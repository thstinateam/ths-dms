var StockCategory = {
	_xhrSave : null,
	_xhrDel: null,	
	_pMap:null,
	_pCheckedMap:null,
	xhrExport: null,
	lastValue: 1,
	_pTotalCheckedMap:null,
	_status:null,
	_lstStockLock: null,
	lockStockStaff:function(){
		var dataModel = {};
		var msg = '';
		var focusId = '#saleStaff';
		var staffCode = $('#saleStaff').combobox('getValue');
		if(staffCode==undefined || staffCode == null || staffCode.trim() ===''){
		msg = 'Chưa chọn Mã NV';
		$('#saleStaff').focus();
		}
		if(msg.length == 0){
			var lstNV = $('#saleStaff').combobox('getData');
			var flag = false;
			if(lstNV!=undefined && lstNV!=null && lstNV.length>1){
				for(var i=1; i<lstNV.length; i++){
					if(lstNV[i].staffCode.toLowerCase().trim()===staffCode.toLowerCase().trim()){
						flag = true;
						break;
					}
				}
				if(!flag){
					msg = 'Mã NV không hợp lệ';
				}
			}else{
				msg = 'Không tìm thấy nhân viên nào';
			}
		}
		if(msg.length > 0){
			$('#errMsg').html(msg).change().show();
			return false;
		}
		
		dataModel.staffCode = staffCode;
		Utils.addOrSaveData(dataModel, "/catalog/utility/closing-day/lock-stock-staff-change", null, 'errMsg', function(data) {
			if (data.error) {
				$('#errMsg').html(data.errMsg).show();
			}else {
				$('#successMsg').html('Khóa kho thành công').change().show();
				setTimeout(function(){
					$('.SuccessMsgStyle').html('').change().show();
					window.location.reload();
				}, 700);
			}
		}, null, null);
		return false;
		
	},
	processAccordingToCycleType:function(){
		if($('#lsttype').val()=='0'){
			$('#btnStockCategorySave').show();
			disabled('addProduct');
			var message = "<font color=red>*<strong>Lưu ý:</strong></font><span style='color:#707070'> Hệ thống sẽ lấy tất cả mặt hàng trong kho để kiểm kê.</span>";
			$('#messageWarning').html(message).show();				
			StockCategory.getAllProductFromServerToGrid();
		}else{
			var message = "<font color=red>*<strong>Lưu ý:</strong></font> <span style='color:#707070'>Bạn phải chọn danh sách mặt hàng để kiểm kho.</span>";
			$('#messageWarning').html(message).show();
			enable('addProduct');
			StockCategory.getResultFromServer($('#cycleCountCode').val());
		}
	},
	getMaxActiveSockCardOfMap:function(map){
		if(map==null){
			return parseInt($('#firstNumber').val().trim())-1;
		}
		if(map.size()==0){
			return parseInt($('#firstNumber').val().trim())-1;
		}
		var max = map.valArray[0].stockCardNumber;
		for(var i=1;i<map.size();i++){
			if(map.valArray[i].isDelete == 0){
				if(max <= map.valArray[i].stockCardNumber){
					max = map.valArray[i].stockCardNumber;
				}
			}
		}
		return max;
	},
	getParams:function(){
		var shopId = $('#shopId').val().trim();
		var cycleCountCode = $('#cycleCountCode').val().trim();
		var firstNumber = $('#firstNumber').val().trim();
		var data = new Object();
		if(shopId>0){
			data.shopId = shopId;
		}
		if(cycleCountCode.length>0){
			data.cycleCountCode = cycleCountCode;
		}
		if(firstNumber>=0){
			data.firstNumber = firstNumber;
		}		
		data.startDate = $('#startDate').val().trim();
		data.cycleCountDes = $('#cycleCountDes').val().trim();
		data.cycleType = $('#lsttype').val().trim();
		return data;
	},
	openSelectProductDialog: function(){
		
		var msg = '';
		$('.ErrorMsgStyle').html('').hide();
		msg = Utils.getMessageOfRequireCheck('cycleCountCode','Mã kiểm kê');
		if(msg.length == 0){
			msg = Utils.getMessageOfSpecialCharactersValidate('cycleCountCode','Mã kiểm kê');
		}		
		if(msg.length == 0){
			msg = Utils.getMessageOfRequireCheck('shopCode','Mã đơn vị');
		}			
		if(msg.length > 0){
			$('#errMsg').html(msg).show();
			return false;
		}
		if(!StockCategory.checkFirstNumber()){
			return false;
		}
		$('td[field=id] div input[type=checkbox]').each(function(){	
			$(this).attr('checked',false);
		});
		var listProduct = new Array();
		for(var i=0;i<StockCategory._pMap.size();i++){			
			var obj = StockCategory._pMap.get(StockCategory._pMap.keyArray[i]);
			if(obj!=null && obj.id!=null){
				listProduct.push(obj.id);
			}
		}
		var listProductEx = listProduct.toString();
		CommonSearch._arrParams = null;
		$('#productDialog').css("visibility", "visible");
		var html = $('#productDialog').html();
		$('#productCode').val('');
		$('#productName').val('');
		$('#fAmnt').val('');
		$('#tAmnt').val('');
		$('#category').val('Chọn nghành hàng').change(function(){
			var params  = new Object();
			params.idParentCat = $('#category').val();	
			Utils.getHtmlDataByAjax(params,'/stock/category/getListSubCat',
					function(data) {
						try {
							var _data = JSON.parse(data);
						} catch (e) {

							$('#sub_category').html(data);
							$('#subcat .CustomStyleSelectBoxInner').html("Chọn ngành hàng con");
							$('#sub_category option[value=0]').attr('selected','selected');
						}
					}, '', 'GET');
			
			
		});
		$('#productEasyUIDialog').dialog({  	       
	        closed: false,  
	        cache: false,  
	        modal: true,
	        width : 750,
	        onOpen: function(){
	        	if(!$('#category').next().hasClass('CustomStyleSelectBox')){
	        		$('#category').customStyle();
	        	}
	        	if(!$('#sub_category').next().hasClass('CustomStyleSelectBox')){
	        		$('#sub_category').customStyle();
	        	}	        		        	
	        	$('#productDialog').html('');
	        	StockCategory._pCheckedMap = new Map();
	        	StockCategory._pTotalCheckedMap = new Map();
	        	
	        	$('.easyui-dialog #productGrid').datagrid({
					url : '/stock/category/search-product',
					autoRowHeight : true,
					rownumbers : true,					
					pagination:true,
					checkOnSelect : true,
					rowNum : 10,
					pageNumber:1,
					scrollbarSize : 0,
					fitColumns:true,
					cache: false,
					queryParams:{
						listProductEx:listProductEx,						
						cycleCountCode: $("#cycleCountCode").val().trim()
					},
					pageList  : [10,20,30],
					width : ($('.easyui-dialog #productGridContainer').width()),
					columns:[[
					    {field: 'id', checkbox: true},
						{field: 'productCode',title:'Mã sản phẩm', width: 150, sortable:false,resizable:false, align: 'left' },
						{field: 'productName',title:'Tên sản phẩm',width: 320,sortable:false,resizable:false, align: 'left' },
						{field: 'cat',index:'cat', title: 'Ngành hàng', width: 100, sortable:false,resizable:false,align: 'left' },
						{field: 'subCat',index:'subCat', title: 'Ngành hàng con', width: 150, sortable:false,resizable:true,align: 'left' },
						{field: 'quantity',index:'quantity', title: 'Tồn kho',width: 100, sortable:false,resizable:false, align: 'right',formatter: CountingFormatter.quantityConvert},							    
						{field: 'productId', index:'id', hidden: true},
						{field: 'convfact', index:'convfact', hidden: true}						       
					 ]],
					 onBeforeLoad:function(param){											 
					 },
					 onLoadSuccess :function(data){   
						 	$('td[field=id] div.datagrid-header-check input[type=checkbox]').attr('checked',false);
					    	$('.datagrid-header-rownumber').html('STT');				    	
					    		
					    	var index = 0;				    
					    	$('td[field=id] div.datagrid-cell-check input[type=checkbox]').each(function(){					    		
					    		$(this).attr('index',index);
					    		var product = $('#productGrid').datagrid('getRows')[index];
					    		$(this).attr('productCode',product.productCode);
					    		if(StockCategory._pCheckedMap.get(product.productCode)!=null){
					    			$('#productGrid').datagrid('selectRow',index);
					    		}					    		
					    		++index;
					    		
					    	});
					    	$(window).resize();
					 },
					 onCheck: function(i, r) {
						var obj = new Object();
						obj.id = r.id;
						obj.code = r.productCode;
						obj.name = r.productName;
						StockCategory._pCheckedMap.put(obj.code,obj);
					 },
					 onUncheck: function(i, r) {
						 StockCategory._pCheckedMap.remove(r.productCode);
					 },
					 onCheckAll: function(rows) {
						 for (var i = 0, sz = rows.length; i < sz; i++) {
							var obj = new Object();
							obj.id = rows[i].id;
							obj.code = rows[i].productCode;
							obj.name = rows[i].productName;
							StockCategory._pCheckedMap.put(obj.code, obj);
						 }
					 },
					 onUncheckAll: function(rows) {
						 for (var i = 0, sz = rows.length; i < sz; i++) {
							StockCategory._pCheckedMap.remove(rows[i].productCode);
						 }
					 }
				});  
	        	$('#productCode').focus();
	        	Utils.bindAutoSearch();
	    		Utils.bindAutoButtonEx('.PopupContentMid','btnSearch');
		     },	        
		     onClose : function(){
		        	$('#productDialog').html(html);
		        	$('#productDialog').css("visibility", "hidden");
		        	$('#cat .CustomStyleSelectBoxInner').html('Chọn ngành hàng');
		    		$('#category option[value=0]').attr('selected','selected');		    		
		    		$('#sub_category').html('<option value="0"></option>');
		    		$('#subcat .CustomStyleSelectBoxInner').html("Chọn ngành hàng con");
		    }
		});	
	},
	searchProduct: function(){		
		var params = new Object();
		params.productCode = $('#productCode').val().trim();
		params.productName = $('#productName').val().trim();
		params.shopId = $('#shopId').val().trim();	
		params.category = $('#category').val().trim();	
		params.sub_category = $('#sub_category').val().trim();	
		params.fromAmnt = $('#fAmnt').val().trim();	
		params.toAmnt = $('#tAmnt').val().trim();
		params.cycleCountCode = $("#cycleCountCode").val().trim();
		var msg = Utils.getMessageOfSpecialCharactersValidate('productCode', 'Mã sản phẩm', Utils._CODE);
		if(msg.length == 0) {
			msg = Utils.getMessageOfSpecialCharactersValidate('productName', 'Tên sản phẩm', Utils._NAME);
		}
		if(msg.length > 0) {
			$('#errMsg').html(msg).show();
			return;
		}
		if (params.fromAmnt.length > 0) {
			if (!/\d$/.test(params.fromAmnt)) {
				$('#productEasyUIDialog #errMsg').html("Số lượng từ không hợp lệ!").show();
				$('#fAmnt').focus();
				return false;
			}
		}
		if (params.toAmnt.length > 0) {
			if (!/\d$/.test(params.toAmnt)) {
				$('#productEasyUIDialog #errMsg').html("Số lượng đến không hợp lệ!").show();
				$('#tAmnt').focus();
				return false;
			}
		}
		
		if (params.fromAmnt.length > 0 && params.toAmnt.length > 0) {
			var fAmnt = parseInt(params.fromAmnt);
			var tAmnt = parseInt(params.toAmnt);
			if (fAmnt > tAmnt) {
				$('#productEasyUIDialog #errMsg').html("Số lượng từ không được lớn hơn số lượng đến!").show();
				$('#tAmnt').focus();
				return false;
			}
		}		
		$("#productGrid").datagrid('load',params);		
		return false;
	},
	getProductNoPagingUrl: function(){
		var shopId = $('#shopId').val().trim();			
		var noPaging = 1;		
		var url = "/stock/category/search-product?shopId="+shopId+"&noPaging="+noPaging+"&cycleCountCode="+$("#cycleCountCode").val().trim();
		return url;
	},
	addProduct:function(id,code,name){
		var html = new Array();		
		html.push('<tr id="row_'+ id +'">');
		html.push('<td class="ColsTd1 AlignCCols OrderNumber" style="border-left: 1px solid #C6D5DB;">01</td>');
		html.push('<td class="AlignLCols">'+ Utils.XSSEncode(code) +'</td>');
		html.push('<td class="AlignLCols">'+ Utils.XSSEncode(name) +'</td>');
		html.push('<td class="EndCols AlignCCols" style="padding: 4px 5px;"><a href="javascript:void(0)" onclick="return StockCategory.delSelectedRow('+id+');">' +
				'<img src="/resources/images/icon-delete.png" width="19" height="20" /></a></td>');
		var num = 0;
		$('#lstProducts').append(html.join(""));
		$('.OrderNumber').each(function(){
			num++;
			$(this).html(num);
		});		
	},
	getAllProductFromServerToGrid:function(){		
		$.getJSON(StockCategory.getProductNoPagingUrl(),function(result){
			StockCategory._pMap = new Map();
			if(result.error == false){
				StockCategory._pMap = new Map();
				var list = result.list;	
				var nextStockCard = parseInt($('#firstNumber').val().trim());
				for(var i=0;i<list.length;++i){
					var obj = list[i];
					var temp  = {};
					temp.code = obj.productCode;
					temp.name = obj.productName;
					temp.isNewSelectProduct = 1;
					temp.isDelete=0;
					temp.id = obj.id;
					temp.stockCardNumber = nextStockCard++;
					StockCategory._pMap.put(temp.code,temp);
				}
			}
			StockCategory.fillTable();
		});		
		return false;
	},
	getResultFromServer:function(cycleCode){	
		$('#divOverlay').show();	
		$.getJSON(encodeURI('/stock/category/search-detail?cycleCountCode='+cycleCode),function(result){
			$('#divOverlay').hide();		
			StockCategory._pMap = new Map();
			if(!result.error){
				StockCategory._pMap = new Map();
				var list = result.list;
				for(var i=0;i<list.length;++i){
					var obj = list[i];
					var temp  = {};
					temp.id = obj.product.id;
					temp.code = obj.product.productCode;
					temp.name = obj.product.productName;
					temp.stockCardNumber = obj.stockCardNumber;
					temp.isDelete=0;
					temp.isNewSelectProduct=0;
					StockCategory._pMap.put(temp.code,temp);
				}
			}
			StockCategory.fillTable();			
		});		
		return false;
	},
	selectListProducts: function(){		
		var _gridSelection = $('#productGrid').datagrid('getSelections');
		for(var i=0;i<_gridSelection.length;++i){
			var obj = {};
			obj.id = _gridSelection[i].id;
			obj.code = _gridSelection[i].productCode;
			obj.name = _gridSelection[i].productName;
			if(StockCategory._pCheckedMap.get(obj.code)==null){
				StockCategory._pCheckedMap.put(obj.code,obj);
			}			
		}		
		var nextStockCard = 1 + StockCategory.getMaxActiveSockCardOfMap(StockCategory._pMap);
		for(var i=0;i<StockCategory._pCheckedMap.size();i++){			
			var obj = StockCategory._pMap.get(StockCategory._pCheckedMap.keyArray[i]);
			if(obj==null || obj.isDelete==1){
				var cycleCountMapProduct = {};
				var temp = StockCategory._pCheckedMap.get(StockCategory._pCheckedMap.keyArray[i]);
				cycleCountMapProduct.id = temp.id;
				cycleCountMapProduct.code = temp.code;
				cycleCountMapProduct.name = temp.name;
				cycleCountMapProduct.isDelete = 0;
				cycleCountMapProduct.stockCardNumber = nextStockCard++;
				cycleCountMapProduct.isNewSelectProduct = 1;
				StockCategory._pMap.put(StockCategory._pCheckedMap.keyArray[i],cycleCountMapProduct);
			}
		}
		if(StockCategory._pCheckedMap.size() <= 0){
			$('#addProductError').html('Vui lòng chọn sản phẩm để thêm vào phiếu').show();
			return false;
		}else{
			StockCategory.fillTable();			
		}		
		$('#firstNumber').change();
		$('.easyui-dialog').dialog('close');
		return false;
	},
	fillTable:function(){
		var html = '';	
		var stt =1;		
		var arraySortRecordForGrid =  new Array();
		for(var i=0;i<StockCategory._pMap.size();++i){
			var obj = StockCategory._pMap.get(StockCategory._pMap.keyArray[i]);
			if(obj!=null && obj.isDelete == 0){
				arraySortRecordForGrid.push(obj);
			}
		}
		for(var i=0; i<arraySortRecordForGrid.length;++i){
			var obj = arraySortRecordForGrid[i];
			html+='<tr id="row_'+obj.code+'">';
			html+='<td class="AlignCCols ColsThFirst" style="border-left: 1px solid #C6D5DB;">'+ (stt++) +'</td>';
			html+='<td class="AlignRCols">'+Utils.XSSEncode(obj.stockCardNumber)+'</td>';			
			html+='<td class="AlignLCols">'+Utils.XSSEncode(obj.code)+'</td>';
			html+='<td class="AlignLCols">'+Utils.XSSEncode(obj.name)+'</td>';
			html+='<td class="AlignCCols ColsTdEnd permissionDelete" style="padding: 4px 5px;">';			
			if(StockCategory._status==0 && $('#lsttype').val()!='0'){
				html+='<a href="javascript:void(0)" onclick="return StockCategory.delSelectedRow(\''+Utils.XSSEncode(obj.code)+'\')" >';
				html+='<img src="/resources/images/icon-delete.png"	width="19" height="20" /></a>';
			}	
			html+='</td>';
			html+='</tr>';	
		}
		$('#lstProducts').html(html);		
	},	
	delSelectedRow:function(code){			
		var obj =  StockCategory._pMap.get(code);
		$.messager.confirm('Xác nhận', 'Bạn có muốn xóa sản phẩm này', function(r){
			if (r){
				if($('#lstProducts #row_' + obj.code) != null && $('#lstProducts #row_' + obj.code).html()!= null 
						&& $('#lstProducts #row_' + obj.code).html().length > 0){					
					if(obj.isNewSelectProduct==1){						
						var stockCardOfDeletedElement = obj.stockCardNumber;
						for(var i=0;i<StockCategory._pMap.size();++i){
							var itemI = StockCategory._pMap.get(StockCategory._pMap.keyArray[i]);
							if(itemI!=null && itemI.isDelete == 0 && itemI.stockCardNumber > stockCardOfDeletedElement){
								itemI.stockCardNumber = itemI.stockCardNumber -1;
								StockCategory._pMap.put(StockCategory._pMap.keyArray[i],itemI);
							}
						}
						StockCategory._pMap.remove(code);
					}else{
						obj.isDelete = 1;
						StockCategory._pMap.put(code,obj);
					}	
					if($('#firstNumber').val()==obj.stockCardNumber){
						if(StockCategory._pMap!=null &&  StockCategory._pMap.size()>0){
							for(var i=0;i<StockCategory._pMap.size();++i){
								var itemI = StockCategory._pMap.get(StockCategory._pMap.keyArray[i]);
								if(itemI!=null && itemI.isDelete != 1){// tuc la isDelete == 0
									var minStockCardAfterDelete = itemI.stockCardNumber;
									$('#firstNumber').val(minStockCardAfterDelete);
									break; //Lay thang dau tien thoi.
								}
							}
						}
					}
					StockCategory.fillTable();
					$('#issStockScrollBody').jScrollPane();
				}
			}
		});			
		return false;
	},
	save:function(){
		var msg = '';
		$('.ErrorMsgStyle').html('').hide();
		msg = Utils.getMessageOfRequireCheck('cycleCountCode','Mã kiểm kê');
		if(msg.length == 0){
			msg = Utils.getMessageOfSpecialCharactersValidate('cycleCountCode','Mã kiểm kê');
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfRequireCheck('startDate','Ngày bắt đầu');
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfRequireCheck('shopCode','Mã đơn vị');
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfRequireCheck('firstNumber','Số thẻ kiểm kê đầu tiên');
		}
		if(msg.length > 0){
			$('#errMsgInfor').html(msg).show();
			return false;
		}
		var data = StockCategory.getParams();
		if($('#lsttype').val()=='1'){
			if(StockCategory._pMap.size()==0){
				$('#errMsg').html('Vui lòng chọn sản phẩm để thêm vào phiếu').show();
				return false;
			}else{
				var lstProduct = new Array(),lstIsDelete = new Array(), lstStockCardNumber = new Array();				 
				for(var i=0;i<StockCategory._pMap.size();i++){
					var obj = StockCategory._pMap.get(StockCategory._pMap.keyArray[i]);
					lstProduct.push(obj.code);
					lstIsDelete.push(obj.isDelete);
					lstStockCardNumber.push(obj.stockCardNumber);
				}
				data.lstProduct = lstProduct;
				data.lstIsDelete = lstIsDelete;
				data.lstStockCardNumber = lstStockCardNumber;
			}
		}
		if($('#lsttype').val()=='0'){
			if(StockCategory._pMap.size()==0){
				$('#errMsg').html('Không tồn tại sản phẩm trong kho').show();
				return false;
			}else{
				var lstProduct = new Array(),lstIsDelete = new Array(), lstStockCardNumber = new Array();				 
				for(var i=0;i<StockCategory._pMap.size();i++){
					var obj = StockCategory._pMap.get(StockCategory._pMap.keyArray[i]);
					lstProduct.push(obj.code);
					lstIsDelete.push(obj.isDelete);
					lstStockCardNumber.push(obj.stockCardNumber);
				}
				data.lstProduct = lstProduct;
				data.lstIsDelete = lstIsDelete;
				data.lstStockCardNumber = lstStockCardNumber;
			}
		}
		var error = '';
		Utils.addOrSaveData(data, '/stock/category/addProduct', StockCategory._xhrSave, 'errMsg', function(d){
			if(!d.error){
				$("#succMsg").html('Bạn đã thêm thành công').show();				
				var tm = setTimeout(function(){
					$('#succMsg').html('').hide();
					clearTimeout(tm);					
				}, 3000);
				StockCategory.getResultFromServer($('#cycleCountCode').val().trim());
			}else{
				$('#errMsg').html(d.errMsg).show();
				error = d.errMsg;
			}
		}, 'loading2', '', null, '');
		if(error.length>0){
			$('#errMsg').html(error).show();
		}
		return false;
	},	
	showDialogPrint:function(){
		$('.ErrorMsgStyle').html('').hide();
		var msg = Utils.getMessageOfRequireCheck('cycleCountCode','Mã kiểm kê');
		if(msg.length == 0){
			msg = Utils.getMessageOfSpecialCharactersValidate('cycleCountCode','Mã kiểm kê');
		}
		if(msg.length > 0){
			$('#errMsg').html(msg).show();
			return false;
		}
		$('#displayChooseCycleCountMapProduct').css("visibility", "visible");
		var html = $('#displayChooseCycleCountMapProduct').html();
		$('#CycleCountMapEasyUIDialog').dialog({  	       
	        closed: false,  
	        cache: false,  
	        modal: true,
	        width : 450,
	        height :170,
	        onOpen: function(){	        	
	        	Utils.bindFormatOnTextfield('fffirstNumber', Utils._TF_NUMBER);
				 Utils.bindFormatOnTextfield('lastNumber', Utils._TF_NUMBER);
				 $('.easyui-dialog #errMsg').html('').hide();
				 $('.easyui-dialog #fffirstNumber').val('');
				 $('.easyui-dialog #lastNumber').val('');
	        },
	        onClose : function(){
	        	$('#displayChooseCycleCountMapProduct').html(html);
	        	$('#displayChooseCycleCountMapProduct').css("visibility", "hidden");  	        	
	        }
		});	
	},
	printExecute:function(){	
		$('.easyui-dialog #fffirstNumber').focus();
		$('.ErrorMsgStyle').html('').hide();
		var firstNumber = $('.easyui-dialog #fffirstNumber').val().trim();
		var lastNumber = $('.easyui-dialog #lastNumber').val().trim();
		var cycleCountCode = $('#cycleCountCode').val().trim();
		var vdata = new Object();
		vdata.firstNumber = firstNumber;
		vdata.lastNumber = lastNumber;
		vdata.cycleCountCode = cycleCountCode;
		if(StockCategory.xhrExport != null){
			return false;
		}		
		var msg = '';
		if(firstNumber.length>0 && isNaN(firstNumber)){	
			msg = 'Số thẻ kiểm kho không hợp lệ. Vui lòng nhập lại';
			$('.easyui-dialog #fffirstNumber').focus();
		}
		if(msg.length==0 && lastNumber.length>0 && isNaN(lastNumber) ){
			msg = 'Số thẻ kiểm kho không hợp lệ. Vui lòng nhập lại';
			$('.easyui-dialog #lastNumber').focus();	
		}
		if (msg.length == 0 && firstNumber.length>0 && lastNumber.length>0) {
			if (parseInt(firstNumber) > parseInt(lastNumber)) {
				msg = "Số thẻ kiểm kho từ không được lớn hơn đến!";
				$('.easyui-dialog #fffirstNumber').focus();
			}
		}
		if(firstNumber == "" || lastNumber == ""){
			msg="Chưa nhập vào số thẻ";
		}
		if(msg.length>0){
			$('.easyui-dialog #errMsg').html(msg).show();
			return false;
		}
		$('.easyui-dialog #errMsg').hide();//
		$('#divOverlay').show();		
		StockCategory.xhrExport = $.ajax({
			type : "POST",
			url : "/stock/category/export-stock-category",
			data :vdata,
			dataType: "json",
			success : function(data) {
				$('#divOverlay').hide();				
				StockCategory.xhrExport = null;
				if(!data.error) {
					window.location.href = data.view;
					setTimeout(function() {
	                    CommonSearch.deleteFileExcelExport(data.view);
					},2000);
				} else {
					$('#errMsg').html(data.errMsg).show();
				}
			},
			error:function(XMLHttpRequest, textStatus, errorDivThrown) {				
			}
		});	
		$('.easyui-dialog').dialog('close');
		return false;		
	},
	checkFirstNumber:function(){
		var msg = Utils.getMessageOfRequireCheck('firstNumber','Số thẻ kiểm kê đầu tiên');
		var fist = parseInt($('#firstNumber').val().trim());		
		if(isNaN(fist) && msg.length==0){			
			msg = 'Số thẻ kiểm kê đầu tiên không hợp lệ. Vui lòng nhập lại';			
		}
		if(msg.length>0){
			$('#errMsgInfor').html(msg).show();
			$('#firstNumber').focus();
			return false;
		}
		return true;		
	}
};
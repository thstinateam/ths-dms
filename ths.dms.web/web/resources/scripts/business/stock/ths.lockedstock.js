var LockedStock = {
	_xhrSave : null,
	_xhrDel: null,	
	_pMap:null,
	_phCheckedMap:null,
	xhrExport: null,
	lastValue: 1,
	_pTotalCheckedMap:null,
	_status:null,
	_lstStockLock: null,
	_currentShopCode : '',
	_shopCodeFilter: "",
	
	lockStockStaff:function(){			
		var dataModel = new Object();
		var lstStaffId = new Array();
		var vanLockStatus = 1;
		var obj = LockedStock._phCheckedMap.valArray;
		for(var i=0;i<obj.length;i++){
			lstStaffId.push(obj[i]["id"]);			
		}
		dataModel.lstStaffLock = lstStaffId;
		dataModel.vanLockStatus = vanLockStatus;
		dataModel.shopCodeFilter=LockedStock._shopCodeFilter;
		Utils.addOrSaveData(dataModel, "/catalog/utility/closing-day/lock-stock-staff-change", null, 'errMsg', function(data) {
			
			if (data.error) {				
				$('#errMsg').html(data.errMsg.replace(/\|/g, '<br>')).show();				
			}else {
				$('#successMsg').html('Khóa kho nhân viên thành công').change().show();				
				setTimeout(function(){
					$('#successMsg').html('').change().show();	
					$('#grid').datagrid('reload');
					LockedStock._phCheckedMap.clear();
				}, 700);
			  }
			
			}, null, null, null, null, function(data) {
				if (data.error) {				
					$('#errMsg').html(data.errMsg.replace(/\|/g, '<br>')).show();
					$('#grid').datagrid('reload');
					LockedStock._phCheckedMap.clear();
				}
			});		
		return false;		
	},
	unlockStockStaff:function(){			
		var dataModel = new Object();
		var lstStaffId = new Array();
		var vanLockStatus = 0;
		var obj = LockedStock._phCheckedMap.valArray;
		for(var i=0;i<obj.length;i++){
			lstStaffId.push(obj[i]["id"]);
		}
		dataModel.startDate = $('#startDate').val();
		dataModel.lstStaffLock = lstStaffId;
		dataModel.vanLockStatus = vanLockStatus;
		dataModel.shopCodeFilter=LockedStock._shopCodeFilter;
		Utils.addOrSaveData(dataModel, "/catalog/utility/closing-day/lock-stock-staff-change", null, 'errMsg', function(data) {
			if (data.error) {
				$('#errMsg').html(data.errMsg.replace(/\|/g, '<br>')).show();
			}else {
				$('#successMsg').html('Mở khóa kho nhân viên thành công').change().show();				
				setTimeout(function(){
					$('#successMsg').html('').change().show();
					$('#grid').datagrid('reload');
					LockedStock._phCheckedMap.clear();
				}, 700);
			  }
			
			},  null, null, null, null, function(data) {
				if (data.error) {				
					$('#errMsg').html(data.errMsg.replace(/\|/g, '<br>')).show();
					$('#grid').datagrid('reload');
					LockedStock._phCheckedMap.clear();
				}
			});		
		return false;		
	},	
	initUnitCbx: function(){				
		var params = {};
		$.ajax({
			type : "POST",
			url:'/commons/list-child-shop',
			data :($.param(params, true)),
			dataType : "json",
			success : function(data) {
				listNVBH = data.lstNVBH;
				$('#shopCodeCB').combobox({
					valueField: 'shopCode',
					textField:  'shopName',
					data: data.rows,
					panelWidth: '206',
					formatter: function(row) {
						return '<span style="font-weight:bold">' + Utils.XSSEncode(row.shopCode) + '</span><br/>' + '<span style="color:#888">' + Utils.XSSEncode(row.shopName) + '</span>';
					},
					filter: function(q, row){
						q = new String(q).toUpperCase().trim();
						var opts = $(this).combobox('options');
						return ((unicodeToEnglish(row[opts.textField]).indexOf(unicodeToEnglish(q)) == 0) || unicodeToEnglish(row[opts.valueField]).indexOf(unicodeToEnglish(q)) == 0) ;
					},	
			        onSelect: function(rec){
			        	if (rec != null && rec.shopCode != LockedStock._currentShopCode){
			        		$("#errMsg").html("").show();
			        		LockedStock.resetChangeShop(rec);
			        	}
			        },
			        onLoadSuccess: function(){
			        	var arr = $('#shopCodeCB').combobox('getData');
			        	if (arr != null && arr.length > 0){
			        		$('#shopCodeCB').combobox('select', arr[0].shopCode);
			        		LockedStock._currentShopCode = Utils.XSSEncode(arr[0].shopCode);
			        	}
			        	
			        }
				});
			}
		});
	},
	resetChangeShop: function(rec){
		if (rec != undefined && rec != null){
			LockedStock._phCheckedMap = new Map();
			LockedStock._currentShopCode = rec.shopCode;
			LockedStock._shopCodeFilter = rec.shopCode;
			var params = new Object();	
			if(LockedStock._shopCodeFilter != null){
					params.shopCodeFilter = LockedStock._shopCodeFilter;
			}	
			params.type = -1;
			$('#grid').datagrid({
				url : '/catalog/utility/closing-day/search-locked-stock',
				autoRowHeight : true,
				rownumbers : true, 
				checkOnSelect :true,
				selectOnCheck: true,
				//singleSelect: true,
				pagination:true,
				rowNum : 10,
				fitColumns:true,
				pageList  : [10,20,30],
				scrollbarSize:0,
				width: $('#gridContainer').width(),
				autoWidth: true,
				queryParams:params,
				columns:[[
				    {field: 'id', index:'id', hidden: true},		
				    {field: 'staffId', checkbox: true},
					{field: 'staffCode',title:'Mã nhân viên', width: 150, sortable:false,resizable:false, align: 'left', formatter: function(value, row, index) {
						return Utils.XSSEncode(value);
					}},
					{field: 'staffName',title:'Tên nhân viên',width: 320,sortable:false,resizable:false, align: 'left' , formatter: function(value, row, index) {
						return Utils.XSSEncode(value);
					}},
					{field: 'vanLock', title: 'Trạng thái', width: 100, sortable:false,resizable:false,align: 'left', formatter:UntilityStockedLock.vanLock },
					{field: 'lockDate', title:'Ngày khóa/mở khóa', width:160, align: 'left'}
				 ]],
				 onBeforeLoad:function(param){											 
				 },
				 onLoadSuccess :function(data){   
					 	$('td[field=staffId] div.datagrid-header-check input[type=checkbox]').attr('checked',false);
				    	$('.datagrid-header-rownumber').html('STT');  		    	
				    	$(window).resize();
				    	 if (data == null || data.total == 0) {
							 $("#errMsg").html("Không có nhân viên bán hàng Vansale trong ngày").show();
						 }
				    	 var n = data.rows.length;
				    	 var r = null;
				 },
				 onCheck: function(i, r) {			
					var obj = new Object();			
					obj.id = r.staffId;		
					obj.code = r.staffCode;
					obj.name = r.staffName;				
					LockedStock._phCheckedMap.put(obj.code,obj);				
				 },
				 onUncheck: function(i, r) {
					 LockedStock._phCheckedMap.remove(r.staffCode);
				 },
				 onCheckAll: function(rows) {
					 for (var i = 0, sz = rows.length; i < sz; i++) {
						var obj = new Object();
						obj.id = rows[i].staffId;		
						obj.code = rows[i].staffCode;
						obj.name = rows[i].staffName;			
						LockedStock._phCheckedMap.put(obj.code, obj);			
					 }
				 },
				  onUncheckAll: function(rows) {		
					  for (var i = 0, sz = rows.length; i < sz; i++) {
						  LockedStock._phCheckedMap.remove(rows[i].staffCode);
						 }
				 } 
			});
			
		}
	},
	getParams:function(){	
		var size = LockedStock._phCheckedMap.size();
		for(var i = 0; i < size; ++ i){
		    var staffCode = LockedStock._phCheckedMap.keyArray[i];
		    console.log(staffCode);
		}
		var shopId = $('#shopId').val().trim();
		var staffCode =$('#staffCode').val();	
		var data = new Object();
		if(shopId>0){
			data.shopId = shopId;
		}
		if(staffCode.length>0){
			data.staffCode = staffCode;
		}		
		return data;
	},
	
};
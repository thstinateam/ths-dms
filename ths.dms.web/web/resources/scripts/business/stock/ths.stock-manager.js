var StockManager = {
	_arrRreeShopTocken: null,
	_mapWarehouse: null,
	_arrShopCopyCheked: null,
	_paramWarehouseSearch: null,
	_warehouseType: "",
	_shopRoot: null,
	_shopChoose: null,
	_shopChooseChange: null,
	_warehouseId: null,
	_rowIndexByTree: 0,
	_flagG:null,
	_flagDl:0,
	_activeStatus: 1,
	_stopedStatus: 0,
	///@author hunglm16; @since: August 15,2014; @description Tim kiem danh sach don vi tren Dialog
	searchListShopByCMS:function(){
		var params = {};
		params.code = $('#txtCmnShopCode').val().trim();
		params.name = $('#txtCmnShopName').val().trim();
		params.isShowAllShop = true;
		$('#common-dialog-grid-search').datagrid("load", params);
	},
	///@author hunglm16; @since: August 15,2014; @description Hien thi Dialog tim kiem danh sach don vi
	showSearchListShopByCMS:function(flag, url){
		$('#common-dialog-search-2-textbox').dialog({
			title: 'Thông tin tìm kiếm',
			width: 600, 
			height: 'auto',
			closed: false,
			cache: false,
			modal: true,
			onOpen: function() {
				$('#txtCmnShopCode').val("").show();
				$('#txtCmnShopName').val("").show();
				var params = {};
				params.code = $('#txtCmnShopCode').val();
				params.name = $('#txtCmnShopName').val();
				params.isShowAllShop = true;
				$('#common-dialog-grid-search').datagrid({
					url : url,
					autoRowHeight : true,
					rownumbers : true,
					checkOnSelect :true,
					pagination:true,
					rowNum : 10,
					pageSize: 10,
					scrollbarSize : 0,
					singleSelect:true,
					pageNumber:1,
					queryParams:params,
					fitColumns:true,
					width : ($('#common-dialog-search-2-textbox').width() - 25),
					columns : [[
						{field:'shopCode', title: stock_manage_ma_don_vi, align:'left', width: 110, sortable:false, resizable:false, formatter: function (value, row, index) {
							return Utils.XSSEncode(value);
						}},
						{field:'shopName', title: stock_manage_ten_don_vi, align:'left', width: 150, sortable:false, resizable:false, formatter: function (value, row, index) {
							return Utils.XSSEncode(value);
						}},
						{field:'choose', title:'', align:'center', width:40, sortable:false, resizable:false, formatter:function(value,row,index) {
							return '<a href="javascript:void(0)" onclick="return StockManager.callSelectF9SialogShopSearchWarehouse(' +flag+', ' +row.shopId+',\'' +row.shopCode+'\',\'' +row.shopName+'\');">chọn</a>';         
						}}
					]],
					onLoadSuccess :function(){
						$('#common-dialog-search-2-textbox .datagrid-header-rownumber').html(area_tree_stt);
						var dHeight = $('#common-dialog-search-2-textbox').height();
						var wHeight = $(window).height();
						var top = (wHeight - dHeight) / 2 > 180 ? (wHeight - dHeight) / 2 - 180 : (wHeight - dHeight) / 2;
					}
				});
				
			},
			onClose: function() {
				$('#txtCmnShopCode').val("").change();
				$('#txtCmnShopName').val("").change();
			}
		});
	},
	///@author hunglm16; @since: August 15,2014; @description Tao tham so tim kiem Warehouse
	createWarehouseParamaterSearch:function(){
		StockManager._paramWarehouseSearch = {};
		StockManager._paramWarehouseSearch.longG = StockManager._shopChoose.shopId;
		if ($('#txtShopCode').val() != null && $('#txtShopCode').length > 0) {
			var arrShopCode = $('#txtShopCode').val().trim().split(" - "); 
			if (arrShopCode.length > 0) {
				StockManager._paramWarehouseSearch.shopCode = arrShopCode[0];
			}
		}
		//StockManager._paramWarehouseSearch.shopCode = $('#txtShopCode').val().trim();
		StockManager._paramWarehouseSearch.code = $('#txtWarehouseCode').val().trim();
//		StockManager._paramWarehouseSearch.name = $('#txtWarehouseName').val().trim();
		StockManager._paramWarehouseSearch.status = $('#ddlStatus').val().trim();
		//StockManager._paramWarehouseSearch.textG = $('#ddlWareHouseType').val().trim();
		StockManager._paramWarehouseSearch.fromDateStr = $('#txtFromDate').val().trim();
		StockManager._paramWarehouseSearch.toDateStr = $('#txtToDate').val().trim();
		StockManager._paramWarehouseSearch.warehouseType = $('#cbWarehouseType').val();
	},
	///@author hunglm16; @since: August 15,2014; @description Tim kiem Warehouse
	searchWarehouse:function(){
		$('.ErrorMsgStyle').html('').hide();
		var msg = '';
		var fDate = $('#txtFromDate').val();
		var tDate = $('#txtToDate').val();
		if(fDate.length >0){
			msg = Utils.getMessageOfInvalidFormatDate('txtFromDate', stock_manage_tu_ngay);
		}
		if(msg.length == 0 && tDate.length >0){
			msg = Utils.getMessageOfInvalidFormatDate('txtToDate', stock_manage_den_ngay);
		}
		if(fDate.length >0 && tDate.length >0 && !Utils.compareDate(fDate, tDate)){
				msg = stock_manage_tungay_denngay;
				$('#fromDate').focus();
		}
		if(msg.length > 0){
			$('#errMsgStockManage').html(msg).show();
			return false;
		}
		StockManager.createWarehouseParamaterSearch();
		$('#dgWarehouse').datagrid('load', StockManager._paramWarehouseSearch);
	},
	///@author hunglm16; @since: August 15,2014; @description Goi nhan tham so gui ve luc chon Don vi tu Dialog tim kiem
	callSelectF9SialogShopSearchWarehouse: function(flag, shopId, shopCode, shopName){
		if(flag==0){
			StockManager._shopChoose = {};
			StockManager._shopChoose.shopId = shopId;
			if(shopCode!=undefined && shopCode!=null){
				StockManager._shopChoose.shopCode = shopCode;
				$('#txtShopCode').val(Utils.XSSEncode(shopCode.trim()+ " - " + shopName));
			}
			if(shopName!=undefined && shopName!=null){
				StockManager._shopChoose.shopName = shopName;
				$('#lblShopName').html(Utils.XSSEncode(shopName.trim())).change();
			}
			$('#common-dialog-search-tree-in-grid-choose-single').dialog("close");
		}else if(flag == 1){
			StockManager._shopChooseChange = {};
			StockManager._shopChooseChange.shopId = shopId;
			if(shopCode!=undefined && shopCode!=null){
				StockManager._shopChooseChange.shopCode = shopCode;
				$('#txtShopCodeDl').val(Utils.XSSEncode(shopCode.trim()+ " - " + shopName));
				$('#common-dialog-search-tree-in-grid-choose-single').dialog("close");
			}
			if(shopName!=undefined && shopName!=null){
				StockManager._shopChooseChange.shopName = shopName;
				$('#lblShopNameDl').html(shopName.trim()).change();
			}
			$('#common-dialog-search-2-textbox').dialog("close");
		}else if(flag == 2){
			StockManager._shopChooseChange = {};
			StockManager._shopChooseChange.shopId = shopId;
			if(shopCode!=undefined && shopCode!=null){
				StockManager._shopChooseChange.shopCode = shopCode;
				$('#txtShopCodeDlCp').val(shopCode.trim());
			}
			if(shopName!=undefined && shopName!=null){
				StockManager._shopChooseChange.shopName = shopName;
				$('#lblShopNameCopy').html(shopName.trim()).change();
			}
			$('#common-dialog-search-2-textbox').dialog("close");
		}
	},
	///@author hunglm16; @since: August 15,2014; @description Bat su kien thay doi shopCode
	txtShopCodeOnchange: function(txt){
		var txtShopCode = $(txt).val().trim();
		StockManager._flagG = {};
		StockManager._flagG.isTrue = false;
		StockManager._flagG.shopName = '';
		if (StockManager._arrRreeShopTocken.length > 0) {
			for (var i = 0; i < StockManager._arrRreeShopTocken.length; i++) {
				var root = StockManager._arrRreeShopTocken[i];
				StockManager.checkInofShopCodeInTreeCMS(root, txtShopCode.trim());
				if (StockManager._flagG) {
					break;
				}
			}
			if (!StockManager._flagG.isTrue) {
				$('#lblShopName').html('');
			} else {
				$('#lblShopName').html(StockManager._flagG.shopName);
			}
		} else {
			$('#lblShopName').html('');
		}
	},
	checkInofShopCodeInTreeCMS : function(node, code) {
		if (node != undefined && node != null && !StockManager._flagG.isTrue) {
			if (node.attribute != undefined && node.attribute != null && node.attribute.shopCode != undefined && node.attribute.shopCode != null && code.toUpperCase() === node.attribute.shopCode.trim().toUpperCase()) {
				StockManager._flagG.isTrue = true;
				StockManager._flagG.shopName = node.attribute.shopName;
				return;
			} else {
				var arrChidren = node.children;
				if (arrChidren != undefined && arrChidren != null && arrChidren.length > 0) {
					for (var i = 0; i < arrChidren.length; i++) {
						StockManager.checkInofShopCodeInTreeCMS(arrChidren[i], code);
					}
				}
			}
		}
	},
	
	///@author hunglm16; @since: August 15,2014; @description mo pupup them(sua) kho
	dialogChangeStockManager : function(isEvent, id) { // phuocdh2 them tham so
		$(".ErrorMsgStyle").html('').hide();
		$('input[id$="Dl"]').val("").change();
		var msg = "";
		if (isEvent == undefined || isEvent == null) {
			return false;
		}
		var titleView = "";
		StockManager._shopChooseChange = {};
		if (isEvent == 0) {
			StockManager._warehouseId = -1;
			titleView = stock_manage_them_moi_kho;
			if(StockManager._shopRoot!=null){
				$('#txtShopCodeDl').val(Utils.XSSEncode(StockManager._shopRoot.shopCode +' - ' + StockManager._shopRoot.shopName));
				$('#lblShopNameDl').html(Utils.XSSEncode(StockManager._shopRoot.shopName));
				StockManager._shopChooseChange = {};
				StockManager._shopChooseChange.shopId = StockManager._shopRoot.shopId;
				StockManager._shopChooseChange.shopCode = StockManager._shopRoot.shopCode;
				StockManager._shopChooseChange.shopName = StockManager._shopRoot.shopName;
			}
			$('#txtWarehouseCodeDl').val("").change();
			$('#txtWarehouseNameDl').val("").change();
			$('#txtDescriptionDl').val("").change();
			selectedDropdowlist('ddlStatusDl', activeType.RUNNING);
			selectedDropdowlist('cbWarehouseTypeDl', warehouseType.SALES);
			enable('txtShopCodeDl');
			enable('txtWarehouseCodeDl');
			enable('cbWarehouseTypeDl');
			$('#txtSeqDl').val("1");
		} else if (isEvent == 1) {
			StockManager._warehouseId = id;
			titleView = stock_manage_chinh_sua_kho;
			var warehouse = StockManager._mapWarehouse.get(id);
			if(warehouse!=undefined && warehouse!=null){
				StockManager._warehouseId = id;
				titleView =titleView+'<span style="color:#199700;">' +' ' + Utils.XSSEncode(warehouse.warehouseCode.trim()) +' - ' + Utils.XSSEncode(warehouse.warehouseName) + ' ' + '</span>';
				titleView = titleView.trim();
				StockManager._shopChooseChange = {};
				StockManager._shopChooseChange.shopId = warehouse.shopId;
				StockManager._shopChooseChange.shopCode = warehouse.shopCode;
				StockManager._shopChooseChange.shopName = warehouse.shopName;
				$('#txtShopCodeDl').val(Utils.XSSEncode(warehouse.shopCode + ' - ' + warehouse.shopName));
				$('#lblShopNameDl').html(Utils.XSSEncode(warehouse.shopName));
				$('#txtWarehouseCodeDl').val(Utils.XSSEncode(warehouse.warehouseCode));
				$('#txtWarehouseNameDl').val(Utils.XSSEncode(warehouse.warehouseName));
				$('#txtDescriptionDl').val(Utils.XSSEncode(warehouse.description));
				selectedDropdowlist('ddlStatusDl', warehouse.status);
				selectedDropdowlist('cbWarehouseTypeDl', warehouse.warehouseType);
				var seqStr = warehouse.seq;
				if (seqStr != undefined && seqStr != null) {
					$('#txtSeqDl').val(Utils.XSSEncode(warehouse.seq.toString()));					
				}
				disabled('txtShopCodeDl');
				disabled('txtWarehouseCodeDl');
				disabled('cbWarehouseTypeDl');
			}
		}
		
		$('#changeWarehouseDialog').dialog({
			title: titleView,
			closed: false,  
	        cache: false,  
	        modal: true,
	        width: 680,
	        onOpen : function() {
				if (isEvent == 0) {
					$('#txtWarehouseCodeDl').focus();
				} else {
					$('#txtWarehouseNameDl').focus();
				}
			},
			onClose : function() {
				StockManager._shopChooseChange = {};
			}
		});
		return false;
	},
	//@author hunglm16; @since: August 15,2014; @description Mo pupup sao chep Kho
	dialogCopyStockManager : function() {
		$(".ErrorMsgStyle").html('').hide();
		StockManager._arrShopCopyCheked = [];
		StockManager._rowIndexByTree = -1;
		StockManager._shopChooseChange = {};
		StockManager._shopChooseChange.shopId = StockManager._shopRoot.shopId;
		StockManager._shopChooseChange.shopCode = StockManager._shopRoot.shopCode;
		StockManager._shopChooseChange.shopName = StockManager._shopRoot.shopName;
		$('#txtShopCodeDlCp').val(StockManager._shopRoot.shopCode);
		$('#lblShopNameCopy').html(Utils.XSSEncode(StockManager._shopRoot.shopName)).show();
		selectedDropdowlist('ddlStatusDlCp', activeType.ALL);
		selectedDropdowlist('ddlWareHouseTypeCpDl', StockManager._warehouseType);
		$('#warehouseContainerCopyDgDiv').html('<table id="dgWarehouse"></table>').change();
		
		$('#copyWarehouseDialog').dialog({
			title: stock_manage_sao_chep_kho,
			closed: false,
	        cache: false, 
	        modal: true,
	        width: 850,
	        height: 490,
	        onOpen: function(){
	        	$('#txtShopCodeDlCp').focus();
	        	//Tao cay don vi
	        	$('#dgTreeShopCopy').treegrid({  
	    		    //url:  '/cms/searchTreeShop',
	        		data: StockManager._arrRreeShopTocken,
	    		    width: $('#shopTreeByCopyContainerGrid').width(),
	    		    height: 322,
	    		    //queryParams: {},
	    			fitColumns : true,
	    			checkOnSelect: false,
	    			selectOnCheck: false,
	    		    rownumbers : true,
	    	        idField: 'id',
	    	        treeField: 'code',
	    		    columns:[[
	    		        {field:'id',title:'',resizable:false, width:40, align:'center', fixed:true, formatter:function(value, row, index){
	    		        	StockManager._rowIndexByTree++;
	    		        	var html = '<input type="checkbox" onchange="StockManager.onchangeCbxDgTreeShopCopy(this);" name=' +row.id+' id="cbx_DgTreeShopDlCp_' +row.id+'" value="' +StockManager._rowIndexByTree+'">';
	    		        	return html;
	    		        }},
	    		        {field:'code',title:stock_manage_ma_don_vi,resizable:false, width:180, align:'left', formatter:function(value, row, index){
	    		        	return Utils.XSSEncode(value);
	    		        }},
	    		        {field:'name',title:stock_manage_ten_don_vi,resizable:false, width:150, align:'left', formatter:function(value, row, index){
	    		        	return Utils.XSSEncode(value);
	    		        }}
	    		    ]],
	    	        onLoadSuccess :function(data){
	    				$('.datagrid-header-rownumber').html(area_tree_stt);
	    				Utils.updateRownumWidthAndHeightForDataGrid('dgTreeShopCopy');
	    				StockManager._rowIndexByTree=-1;
	    	        }
	    		});
	        },
	        onClose:function() {
	        	selectedDropdowlist('ddlStatusDl', activeType.RUNNING);
	        }
		});
	},
	///@author hunglm16; @since: August 15,2014; @description Mo pupup sao chep Kho
	dialogSeachShopCommon: function(){
		$(".ErrorMsgStyle").html('').hide();
		$('#common-dialog-dgTreeShopGContainerGS').html('<table id="commonDialogTreeShopG"></table>').change();
		$('#common-dialog-search-tree-in-grid-choose-single').dialog({
			title: stock_manage_tim_kiem_don_vi,
			closed: false,
	        cache: false, 
	        modal: true,
	        width: 680,
	        height: 550,
	        onOpen: function(){
	        	$('#txtShopCodeDlg').val("");
	        	$('#txtShopNameDlg').val("");
	        	//Tao cay don vi
	        	$('#commonDialogTreeShopG').treegrid({  
	        		data: StockManager._arrRreeShopTocken,
	        		url : '/cms/searchTreeShop',
	    		    width: 650,
	    		    height: 325,
	    		    queryParams: {code: $('#txtShopCodeDlg').val().trim(), name: $('#txtShopNameDlg').val().trim()},
	    			fitColumns : true,
	    			checkOnSelect: false,
	    			selectOnCheck: false,
	    		    rownumbers : true,
	    	        idField: 'id',
	    	        treeField: 'code',
	    		    columns:[[
	    		        {field:'code',title:stock_manage_ma_don_vi,resizable:false, width:200, align:'left', formatter:function(value, row, index){
	    		        	return Utils.XSSEncode(value);
	    		        }},
	    		        {field:'name',title:stock_manage_ten_don_vi,resizable:false, width:200, align:'left', formatter:function(value, row, index){
	    		        	return Utils.XSSEncode(value);
	    		        }},
	    		        {field:'choose',title:'',resizable:false, width:60, align:'center', fixed:true, formatter:function(value, row, index){
	    		        	return '<a href="javascript:void(0)" onclick="return StockManager.callSelectF9SialogShopSearchWarehouse(0, ' +row.id+',\'' +row.code+'\',\'' +row.name+'\');">' +stock_manage_chon+'</a>';
	    		        }}
	    		    ]],
	    	        onLoadSuccess :function(data){
	    				$('.datagrid-header-rownumber').html(area_tree_stt);
	    				Utils.updateRownumWidthAndHeightForDataGrid('commonDialogTreeShopG');
	    	        }
	    		});
	        	$('#txtShopCodeDlg').focus();
	        },
	        onClose:function() {
	        	
	        }
		});
	},
	onchangeCbxDgTreeShopCopy:function(cbx){
		if($(cbx)[0].checked){
			var node = $('#dgTreeShopCopy').treegrid('find', parseInt($(cbx)[0].name.trim()));
			StockManager.uncheckFullParentToNode(node, node.id);
			StockManager.uncheckNodeGroup(node, node.id);
			var children = node.children;
			if(StockManager._arrShopInOrgAccessCheked!=undefined && StockManager._arrShopInOrgAccessCheked!=null && StockManager._arrShopInOrgAccessCheked.length>0){
				if(StockManager._arrShopInOrgAccessCheked.indexOf(node.id)<0){
		    		StockManager._arrShopCopyCheked.push(node.id);
		    	}
			}else{
				StockManager._arrShopCopyCheked.push(node.id);
			}
		}else{
			var node = $('#dgTreeShopCopy').treegrid('find', parseInt($(cbx)[0].name.trim()));
			StockManager._arrShopCopyCheked = jQuery.grep(StockManager._arrShopCopyCheked, function( a ) {
				return a !== node.id;
			});
			StockManager.uncheckFullParentToNode(node, node.id);
		}
	},
	///@author hunglm16; @since: August 18,2014; @description uncheckParent and uncheck Childrent
	uncheckFullParentToNode: function(node, id){
		if(node != undefined && node != null){
			StockManager._arrShopCopyCheked = jQuery.grep(StockManager._arrShopCopyCheked, function( a ) {
				return a !== node.id;
			});
			var cbx = $('#cbx_DgTreeShopDlCp_' + node.id);
			if (node.id != id) {
				$(cbx).attr('checked', false);
			}
			if (node.attribute != undefined && node.attribute != null && node.attribute.parentId != undefined && node.attribute.parentId != null) {
				var parNode = $('#dgTreeShopCopy').treegrid('find', node.attribute.parentId);
				if (parNode != undefined && parNode != null) {
					StockManager.uncheckFullParentToNode(parNode);
				} else {
					return;
				}
			}
		}else{
			return;
		}
	},
	///@author HungLM16; @since JULY 25,2014; @description Mo node dau tien va con chau cua no
	uncheckNodeGroup: function(node, id){
		if(node!=undefined && node!=null){
			if(StockManager._arrShopCopyCheked.indexOf(node.id)<0){
				StockManager._arrShopCopyCheked = jQuery.grep(StockManager._arrShopCopyCheked, function( a ) {
					return a !== node.id;
				});
	    	}
			var cbx = $('#cbx_DgTreeShopDlCp_' + node.id);
			if (node.id != id) {
				$(cbx).attr('checked', false);
			}
			var arrChidren = node.children;
			if (arrChidren != undefined && arrChidren != null && arrChidren.length > 0) {
				for (var i = 0; i < arrChidren.length; i++) {
					StockManager.uncheckNodeGroup(arrChidren[i]);
				}
			}
		}
	},
	///@author hunglm16; @since: August 15,2014; @description them moi hoac cap nhat thong tin kho
	changeWarehouseByDialog: function(){
		$(".ErrorMsgStyle").html('').hide();
		var msg = "";
		if (StockManager._shopChooseChange.shopId == undefined || StockManager._shopChooseChange.shopId==null || StockManager._shopChooseChange.shopId<0) {
			msg = stock_manage_khong_xac_dinh_vi_tri;
		}
		if (msg.length == 0) {
			msg = Utils.getMessageOfRequireCheck("txtWarehouseCodeDl", stock_manage_ma_kho);
		}
		if (msg.length == 0) {
			msg = Utils.getMessageOfSpecialCharactersValidate("txtWarehouseCodeDl", stock_manage_ma_kho, Utils._NAME);
		}
		if (msg.length == 0) {
			msg = Utils.getMessageOfRequireCheck("txtWarehouseNameDl", stock_manage_ten_kho);
		}
		if (msg.length == 0) {
			msg = Utils.getMessageOfSpecialCharactersValidate("txtWarehouseNameDl", stock_manage_ten_kho, Utils._NAME);
		}
		var status = $('#ddlStatusDl').val();
		if(msg.length == 0 && (status==null || status==="" || parseInt(status)<0)){
			msg = stock_manage_khong_xac_dinh_tinh_trang;
		}
		if (msg.length == 0) {
			msg = Utils.getMessageOfSpecialCharactersValidate("txtDescriptionDl", stock_manage_tooltip_ghi_chu, Utils._NAME);
		}
		if (msg.length == 0) {
			msg = Utils.getMessageOfInvaildInteger("txtSeqDl", stock_manage_tooltip_thu_tu);
			if (msg.length > 0) {
				msg = stock_manage_thu_tu_lon_hon_mot;
			}
		}
		if (msg.length > 0) {
			$("#errMsgDialogChangeWarehouse").html(msg).show();
			return false;
		}
		var isFlag = false;
		msg ='';
		if (StockManager._warehouseId != undefined && StockManager._warehouseId != null && StockManager._warehouseId > -1) {
			var warehouse = StockManager._mapWarehouse.get(StockManager._warehouseId);
			msg = Utils.XSSEncode(stock_manage_ban_cap_nhat_kho + ' ' + warehouse.warehouseCode +' - ' + warehouse.warehouseName + ' ? ');
			isFlag = true;
		} else {
			msg = stock_manage_them_kho;
		}
		var params = {
				id: StockManager._warehouseId,
				longG: StockManager._shopChooseChange.shopId,//ShopId
				code: $("#txtWarehouseCodeDl").val().trim(),
				name: $("#txtWarehouseNameDl").val().trim(),
				status: $("#ddlStatusDl").val(),
				description: $("#txtDescriptionDl").val().trim(),
				seq: $('#txtSeqDl').val(),
				warehouseType: $('#cbWarehouseTypeDl').val(),
				isFlag: isFlag
		};
		Utils.addOrSaveData(params, "/stock-manage/addorUpdateWarehouse", null, 'errMsgDialogChangeWarehouse', function(data) {
			StockManager.searchWarehouse();
			$("#successMsgDialogChangeWarehouse").html(msgCommon1).show();
			setTimeout(function(){
				$('#changeWarehouseDialog').dialog("close");
				$('.SuccessMsgStyle').html("").hide();
			 }, 1500);
		}, null, null, null, msg);
		return false;
	},
	///@author hunglm16; @since: August 19,2014; @description Sao chep kho
	copyWeahouse: function(){
		$(".ErrorMsgStyle").html('').hide();
		var msg = "";
		if (StockManager._shopChooseChange.shopId == undefined || StockManager._shopChooseChange.shopId==null || StockManager._shopChooseChange.shopId<0) {
			msg = stock_manage_khong_xac_dinh_vi_tri;
		}
		var status = $('#ddlStatusDl').val();
		if(msg.length == 0 && (status==null || status==="" || parseInt(status)<0)){
			msg = stock_manage_khong_xac_dinh_tinh_trang;
		}
		if(msg.length == 0 && (StockManager._arrShopCopyCheked==null || StockManager._arrShopCopyCheked.length==0)){
			msg = stock_manage_chua_chon_don_vi;
		}
		if (msg.length > 0) {
			$("#errMsgCoppyWarehouseDl").html(msg).show();
			return false;
		}
		msg = stock_manage_sao_chep_kho;
		var params = {
				longG: StockManager._shopChooseChange.shopId,//ShopId
				//textG: $("#ddlWareHouseTypeCpDl").val(),
				status: $("#ddlStatusDlCp").val(),
				arrlongG: StockManager._arrShopCopyCheked
				
		};
		Utils.addOrSaveData(params, "/stock-manage/copy-Warehouse", null, 'errMsgCoppyWarehouseDl', function(data) {
			StockManager.searchWarehouse();
			$("#successCoppyWarehouseDl").html(msgCommon1).show();
			setTimeout(function(){
				$('#copyWarehouseDialog').dialog("close");
				$('.SuccessMsgStyle').html("").hide();
			 }, 1500);
		}, null, null, null, msg);
		return false;
	},
	///@author hunglm16; @since: August 15,2014; @description Xoa Kho
	deleteStockManager: function(id, code){
		$(".ErrorMsgStyle").html('').hide();
		var msg = "";
		if(code==undefined || code ==null){
			code = "";
		}
		code = code.toString().trim();
		var params = {
				id: id
		};
		var msg = stock_manage_xoa_kho+code+"?";
		Utils.addOrSaveData(params, "/stock-manage/deleteWarehouse", null, 'errMsgStockManage', function(data) {
			StockManager.searchWarehouse();
			$("#successMsgStockManage").html(msgCommon1).show();
			setTimeout(function(){
				$('.SuccessMsgStyle').html("").hide();
			 }, 1500);
		}, null, null, null, msg);
	},
	
	///@author hunglm16; @since: August 15,2014; @description Xoa Kho
	updateStatusStockManager: function(id, code, statusStr){
		$(".ErrorMsgStyle").html('').hide();
		var msg = "";
		if(code==undefined || code ==null){
			code = "";
		}
		code = code.toString().trim();
		var params = {
				id: id,
				isEvent : 0
		};
		var msg = Utils.XSSEncode(stock_manage_co_muon+ '  '  +statusStr+ ' ' +stock_manage_title+ ' ' +code+ '?');
		Utils.addOrSaveData(params, "/stock-manage/updateStatusWarehouseByEnvent", null, 'errMsgStockManage', function(data) {
			StockManager.searchWarehouse();
			$("#successMsgStockManage").html(msgCommon1).show();
			setTimeout(function(){
				$('.SuccessMsgStyle').html("").hide();
			 }, 1500);
		}, null, null, null, msg);
	}
	
};
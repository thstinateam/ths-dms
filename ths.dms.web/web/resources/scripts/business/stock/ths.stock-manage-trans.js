var StockManageTrans = {
	_xhrSave : null,
	stockTransCodeGlobal:null,
	fromStockCodeGlobal:null,
	toStockCodeGlobal:null,
	stockTransDateStrGlobal:null,
	stockTransTypeGlobal:null,
	nameTransDialog:null,
	issuedDateDialog:null,
	
	getShopCodeByShopCodeAndName : function(input) {
		var value = $('#'+ input).val().trim().split("-");
		return value[0];
	},
	
	openStockOutTrainsDialog :function() {
		$('.ErrorMsgStyle').html('').hide();
		$('#stockOutVanSalePrintDiv').dialog({
			title : "Phiếu xuất kho kiêm vận chuyển nội bộ",
			closed : false,
			onOpen : function() {
				if(StockManageTrans.nameTransDialog != null){
					$('#ipNameTransDialog').val(Utils.XSSEncode(StockManageTrans.nameTransDialog));
				}
				if(StockManageTrans.issuedDateDialog != null){
					$('#fDateDialog').val(Utils.XSSEncode(StockManageTrans.issuedDateDialog));
				}
				$('#ipContentTransDialog').val('Vận chuyển hàng đi bán lưu động');
				$('#ipNameTransDialog').focus();
			},
			onClose:function(){
			}
		});
		return false;
	},
	
	exportStockOutTrans:function(){		
		$('.ErrorMsgStyle').hide();
		/**
		 * @author hunglm16
		 * @since JUNE 23, 2014
		 * @description Update xuat file giong bao cao 7-8-3.Phieu xuat kho kiem van chuyen noi bo
		 * */
		var msg = Utils.getMessageOfRequireCheck('ipNameTransDialog', 'Tên lệnh điều động');		
		if(msg.length==0){
			msg = Utils.getMessageOfSpecialCharactersValidate('ipNameTransDialog', 'Tên lệnh điều động', Utils._NAME);
		}
		if(msg.length==0){
			msg = Utils.getMessageOfRequireCheck('fDateDialog', 'Ngày lệnh điều động');			
		}
		if(msg.length==0){
			msg = Utils.getMessageOfInvalidFormatDate('fDateDialog', 'Ngày lệnh điều động');			
		}
		var currentTime = new Date();
		var month = currentTime.getMonth() + 1;
		var day = currentTime.getDate();
		var year = currentTime.getFullYear();
		if(msg.length==0&&!Utils.compareDate(day + '/' + month + '/' + year, $('#fDateDialog').val().trim())){
			msg = 'Từ ngày phải lớn hơn bằng ngày hiện tại';
			$('#fDateDialog').focus();
		}
		if(msg.length==0){
			msg = Utils.getMessageOfRequireCheck('ipContentTransDialog', 'Nội dung');			
		}	
		if(msg.length==0){
			msg = Utils.getMessageOfSpecialCharactersValidate('ipContentTransDialog', 'Nội dung', Utils._NAME);
		}
		if(msg.length>0){
			$('#errMsg2').html(msg).show();
			return false;
		}
				
		var data = new Object();
		data.staffCode = '';
		data.issuedDate = $('#fDateDialog').val().trim();
		data.stockTransCode = StockManageTrans.stockTransCodeGlobal;
		data.transacContent = $('#ipContentTransDialog').val().trim();
		data.transacName = $('#ipNameTransDialog').val().trim();

		//Update them tham so xuat file report
		var kData = $.param(data,true);
		$('#divOverlay').addClass('Overlay');
		$('#divOverlay').show();
		$('#imgOverlay').show();
		$.ajax({
			type : "POST",
			url : "/stock/manage-trans/print-pxkkvcnb",
			data :(kData),
			dataType: "json",
			success : function(data) {
				$('#divOverlay').removeClass('Overlay');
				$('#imgOverlay').hide();
				$('#divOverlay').hide();				
				if(!data.error) {
					StockManageTrans.nameTransDialog = $('#ipNameTransDialog').val().trim();
					StockManageTrans.issuedDateDialog = $('#fDateDialog').val().trim();
					window.location.href = data.path;
				} else {
					$('#errMsg2').html(data.errMsg).show();					
				}
			},
			error:function(XMLHttpRequest, textStatus, errorDivThrown) {
				$('#divOverlay').removeClass('Overlay');
				$('#divOverlay').hide();
				hideLoading(loading);
			}
		});
		return false;
		
	},
	
	search : function() {
		$('.ErrorMsgStyle').html('').hide();
		var msg = "";
		if(msg.length ==0){
			msg = Utils.getMessageOfRequireCheck('startDate','Từ ngày' );
		}
		if(msg.length ==0){
			msg = Utils.getMessageOfRequireCheck('endDate','Đến ngày' );
		}
		if(msg.length ==0){
			msg = Utils.getMessageOfInvalidFormatDate('startDate', 'Từ ngày');
		}
		if(msg.length ==0){
			msg = Utils.getMessageOfInvalidFormatDate('endDate', 'Đến ngày');
		}
	
		var startDate = $('#startDate').val().trim();
		var endDate = $('#endDate').val().trim();
		
		if(msg.length ==0 && !Utils.compareDate(startDate,endDate)){
			msg ='Từ ngày không được lớn hơn Đến ngày. Vui lòng nhập lại.';
		}
		
		if(msg.length > 0){
			$('#errMsgSearch').html(msg).show();
			return false;
		}
		var type = $('#type').val();
		var shopId = $('#shopId').val();
		var params = {
			page: 1,
			startDate: startDate,
			endDate: endDate,
			type: type,
			shopId: shopId,
			status: $("#status").val().trim()
		};

		$('#grid').datagrid('load', params);
		$('#stockTransDetailTable').hide();
		return false;
	},
	viewDetail : function(stockTransCode, fromStockCode, toStockCode, stockTransDateStr, stockTransType, stockTransId) {
		StockManageTrans.stockTransCodeGlobal=stockTransCode;
		StockManageTrans.fromStockCodeGlobal=fromStockCode;
		StockManageTrans.toStockCodeGlobal=toStockCode;
		StockManageTrans.stockTransDateStrGlobal=stockTransDateStr;
		StockManageTrans.stockTransTypeGlobal=stockTransType;
		StockManageTrans.stockTransIdGlobal = stockTransId;
		$('#stockTransDetailTable').show();
		var params = new Object();
		params.stockTransCode = stockTransCode;
		params.stockTransId = stockTransId;
		$('#title').html('Thông tin chi tiết giao dịch kho - ' + Utils.XSSEncode(stockTransCode)).show();
		$('#productDetailGrid').html('<table id="gridDetail" class="easyui-datagrid"></table>');
		$('#gridDetail').datagrid({
			url : '/stock/manage-trans/viewdetailstock',
			method: 'GET',
			fitColumns:true,
			width: $('#productDetailGrid').width(),
			autoWidth: true,
			autoRowHeight : true,
			singleSelect: true,
			rownumbers : true, 
			checkOnSelect :true,
			pagination:true,
			pageSize:10,
			pageList  : [10,20,30],
			scrollbarSize:0,
			queryParams:params,
			columns:[[
			        {field:'productCode', title: 'Mã sản phẩm', width: 100, sortable:false,resizable:false , align: 'left', formatter:function(value, row, index) {
			        	  return Utils.XSSEncode(value);
			          }},
			  	    {field:'productName', title: 'Tên sản phẩm', width: 130, sortable:false,resizable:false , align: 'left', formatter:function(value, row, index) {
			        	  return Utils.XSSEncode(value);
			          }},
			        {field:'fromWarehouse', title: 'Kho xuất', width: 100, align: 'left', sortable:false,resizable:false,
				        	formatter: function(val, row) {
				        		if (val != undefined && val != null) {
				        			return Utils.XSSEncode(val);
				        		}
				        		return '';
				        	}
				        },
				  	{field:'toWarehouse', title: 'Kho nhập', width: 80, align: 'left',sortable:false,resizable:false,
				        	formatter: function(val, row) {
				        		if (val != undefined && val != null) {
				        			return Utils.XSSEncode(val);
				        		}
				        		return '';
				        	}
				        },
			  	    {field:'thungLe', title: 'Số lượng', sortable:false,resizable:false, align: 'right'},
			  	    /*{field:'lot', title: 'Số lô', width: 100, align: 'left', sortable:false,resizable:false},*/
			  	    {field:'price', title: 'Giá bán', width: 80, align: 'right',sortable:false,resizable:false,formatter:CommonFormatter.numberFormatter},
			  	    {field:'total', title: 'Thành tiền', width: 120, align: 'right',sortable:false,resizable:false,formatter:CommonFormatter.numberFormatter}			  	       
			]],	    
			onLoadSuccess :function(data){
				$('#btnExportExcel').unbind('click');
				 if(StockManageTrans.stockTransTypeGlobal == 'XUAT_KHO_NHAN_VIEN' || StockManageTrans.stockTransTypeGlobal == 'NHAP_KHO_NHAN_VIEN'){
					 enable('btnExportExcel');
					 $('#btnExportExcel').bind('click',function(event) {
						 StockManageTrans.printStockTrans();
					});
				 }else{
					 disabled('btnExportExcel');
				 }
				 $('.datagrid-header-rownumber').html('STT');	    	
				 updateRownumWidthForJqGrid('.easyui-dialog');
				 $(window).resize();    		
				 $('#totalWeight').html(formatCurrency(data.totalWeight));
				 $('#totalAmount').html(formatCurrency(data.totalAmount));
			}
		});		
		return false;
	},
	printStockTrans:function(){
		var data = new Object();
		//data.shopCodeTextField = StockManageTrans.getShopCodeByShopCodeAndName('shopCodeAndName');
		data.code = StockManageTrans.stockTransCodeGlobal;
		data.stockTransDate = StockManageTrans.stockTransDateStrGlobal;
		data.transType = StockManageTrans.stockTransTypeGlobal;
		data.stockTransId = StockManageTrans.stockTransIdGlobal;
		 if(data.transType == "XUAT_KHO_NHAN_VIEN"){
			 data.vansaleCode = StockManageTrans.toStockCodeGlobal;
		 }else if(data.transType ==   "NHAP_KHO_NHAN_VIEN"){
			 data.vansaleCode = StockManageTrans.fromStockCodeGlobal;	
		}else{
			data.vansaleCode = "";
		}
		try {
			data.reportCode = $('#function_code').val();
		} catch(e) {
			// Oop! Error
		}
		var kData;
		kData = $.param(data,true);
		$('#divOverlay').show();
		$("#imgOverlay").show();
		$.ajax({
				type : "POST",
				url : "/stock/manage-trans/printStockTrans",
				data :(kData),
				dataType: "json",
				success : function(data) {
					$('#divOverlay').hide();				
					if(!data.error) {
						var filePath = ReportUtils.buildReportFilePath(data.path);
						window.location.href = filePath;
					} else {
						$('#errMsg').html(data.errMsg).show();					
					}
				},
				error:function(XMLHttpRequest, textStatus, errorDivThrown) {
					$('#divOverlay').hide();
					StockIssued.xhrExport = null;
				}
		});
		return false;
	},
	exportStockOutVanSale:function(){		
		/*$('#errMsg').hide();
		var code = $('#code').val().trim();		
		var carId = $('#car').val();*/		
		var data = new Object();
		//data.shopCodeTextField = StockManageTrans.getShopCodeByShopCodeAndName('shopCodeAndName');
		data.code = StockManageTrans.stockTransCodeGlobal;
		data.vansaleCode = StockManageTrans.codeStaffGlobal;
		data.stockTransDate = StockManageTrans.stockTransDateStrGlobal;
		data.stockTransId = StockManageTrans.stockTransIdGlobal;
		var kData = $.param(data,true);
		$('#divOverlay').show();
		$("#imgOverlay").show();
		$.ajax({
			type : "POST",
			url : "/stock/issued/exportStockOutVanSale",
			data :(kData),
			dataType: "json",
			success : function(data) {
				$('#divOverlay').hide();				
				if(!data.error) {
					window.location.href = data.path;
				} else {
					$('#errMsg').html(data.errMsg).show();					
				}
			},
			error:function(XMLHttpRequest, textStatus, errorDivThrown) {
				$('#divOverlay').hide();
				StockIssued.xhrExport = null;
			}
		});
		return false;
	},
	exportStockReceivedReport:function(){
//		$('#errMsg').hide();
//		var code = $('#stockTransCode').val().trim();	
		var data = new Object();
		data.code = StockManageTrans.stockTransCodeGlobal;
		data.vansaleCode = StockManageTrans.codeStaffGlobal;
		data.stockTransDate = StockManageTrans.stockTransDateStrGlobal;
		data.stockTransId = StockManageTrans.stockTransIdGlobal;
		var kData = $.param(data,true);
		$('#divOverlay').show();
		$("#imgOverlay").show();
		$.ajax({
			type : "POST",
			url : "/stock/received/exportReport",
			data :(kData),
			dataType: "json",
			success : function(data) {
				$('#divOverlay').hide();				
				if(!data.error) {
					window.location.href = data.path;
				} else {
					$('#errMsg').html(data.errMsg).show();					
				}
			},
			error:function(XMLHttpRequest, textStatus, errorDivThrown) {
				$('#divOverlay').hide();
				StockIssued.xhrExport = null;
			}
		});
		return false;
	}
};
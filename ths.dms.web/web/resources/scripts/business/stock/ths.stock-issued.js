var StockIssued = {
	_xhrSave: null,	
	storeProduct:null,
	mapProductLot:null,
	PRODUCT_ID:null,
	CONVFACT:null,
	PRODUCT_TOTAL:null,
	request:null,
	indexEditing:0,
	lstProductExport: new Array(),
	lstProductArray:new Array(),
	lstProductMap: new Map(), /* Danh sach san pham key=productId */
	lstProductCodeMap : new Map(), /* Danh sach san pham key=productCode */
	lstProductCodeShopMap : new Map(),/* DS san pham ton kho npp*/
	gridData:new Map(),
	selectProductId:null,
	exceptProductIdStr:'',
	isAuthorize : true,
	_hasStaffCode : null,
	nameTransDialog: null,
	issuedDateDialog: null,

	openStockOutVanSalePrintDialog :function() {
		$('.ErrorMsgStyle').html('').hide();
		$('#stockOutVanSalePrintDiv').dialog({
			title : "Phiếu xuất kho kiêm vận chuyển nội bộ",
			closed : false,
			onOpen : function() {
				if(StockIssued.nameTransDialog != null){
					$('#ipNameTransDialog').val(StockIssued.nameTransDialog);
				}
				if(StockIssued.issuedDateDialog != null){
					$('#fDateDialog').val(StockIssued.issuedDateDialog);
				}
				$('#ipContentTransDialog').val('Vận chuyển hàng đi bán lưu động');
				$('#ipNameTransDialog').focus();
			},
			onClose:function(){
				
			}
		});
		return false;
	},
	initGrid:function(){
		$('#dg').datagrid({			
			singleSelect: true,			
			fitColumns:true,		
			scrollbarSize:0,		
			width: $(window).width() - 50,
			autoWidth: true,	
			showFooter: true,
			data:StockIssued.lstProductExport,
		    columns:[[	     
		        {field:'rownum', title: 'STT', width: 40, sortable:false,resizable:true , align: 'center',formatter:StockIssuedGridFmt.rownumFmt},
			    {field:'productId', title: 'Mã sản phẩm (F9)', width: 100, sortable:false,resizable:true , align: 'left',		    	
		          	formatter:StockIssuedGridFmt.productIdFmt,	          	
		         	editor: {
		        	 type:'combobox',
		        	 options: {
		        		valueField:'productId',
		        		textField:'productCode',
		        		data:StockIssued.lstProductArray,
		        		width:200,
		        		formatter: function(row) {
		        			 return '<span style="font-weight:bold">' + Utils.XSSEncode(row.productCode) + '</span><br/>' + '<span style="color:#888">' + Utils.XSSEncode(row.productName) + '</span>';
		        		},
		        		onSelect: function(rec) {	        			
		        			var product = StockIssued.lstProductMap.get(rec.productId);
				    		if(product!=null){
				    			StockIssued.selectProductId = rec.productId;
					    		$('.datagrid-row-editing td[field=productName] div').html(product.productName);
					    		$('.datagrid-row-editing td[field=price] div').html(formatCurrency(product.price));					    		
								$('.datagrid-row-editing td[field=stockQuantity] div').html(formatQuantityEx(product.stockQuantity, product.convfact));					    		
					    		$('.ErrorMsgStyle').html('').hide();	
					    		StockIssued.disabledInputTextFiled(false);
					    		$(this).combobox('setValue',product.productId);
				    		}				    		
		        		},
		        		filter: function(q, row){
						   q = new String(q).toUpperCase();
						   var opts = $(this).combobox('options');
						   return row[opts.textField].indexOf(q)>=0;
						},
						onChange:function(newvalue,oldvalue){
					    	if(newvalue!=undefined && newvalue!=null){
					    		var product = StockIssued.lstProductCodeMap.get(new String(newvalue).toUpperCase());
						    	if(product!=null){
						    		$(this).combobox('setValue',product.productId);
						    	}
					    	}
							$('.combo-panel').each(function(){
								if(!$(this).is(':hidden')){
									$(this).parent().css('width','250px');
									$(this).css('width','248px');
								}			
							});
						}
		        	 }
		         }},
		         {field:'productName', title: 'Tên sản phẩm', width:220,sortable:false,resizable:true , align: 'left',formatter:StockIssuedGridFmt.productNameFmt},
				 {field:'stockQuantity', title: 'Tồn kho', width: 80, align: 'right', sortable:false,resizable:true,formatter: StockIssuedGridFmt.availableQuantityFmt},
				 {field:'lot',title: 'Chọn lô', width: 80, align: 'center',sortable:false,resizable:true,formatter:StockIssuedGridFmt.lotFmt},
				 {field:'quantity', title: 'Số lượng', width: 120, align: 'right',sortable:false,resizable:true,
				  	formatter:StockIssuedGridFmt.quantityFmt,
				   	editor:{type : 'text'}},
				 {field:'price', title: 'Giá bán', width: 100, align: 'right',sortable:false,resizable:true,formatter:StockIssuedGridFmt.priceFmt},
				 {field:'amount', title: 'Thành tiền', width: 100, align: 'right',sortable:false,resizable:true,formatter:StockIssuedGridFmt.amountFmt},
				 {field:'delete', title: 'Xóa', width: 30, align: 'center',sortable:false,resizable:true,formatter:StockIssuedGridFmt.delFmt}
			    ]],	    
		    onLoadSuccess :function(data){		    	
		    },
		    onAfterEdit:function(index,row){	
		    	StockIssued.indexEditing = index;
		    	var productId = row.productId;
		    	var product = StockIssued.lstProductMap.get(productId);
		    	if(product!=null){
		    		row.productCode = product.productCode;
          			row.productName = product.productName;
          			row.stockQuantity = product.stockQuantity;
          			row.grossWeight = product.grossWeight;
          			row.lot = product.checkLot;          			
          			row.price = product.price;          			
          			row.convfact = product.convfact;          			
          			row.quantity = formatQuantityEx(row.quantity,row.convfact);
          			row.quantityValue = getQuantity(row.quantity,row.convfact);
          			if(row.quantityValue==undefined || row.quantityValue==null){
          				row.quantityValue = 0;
          			}
          			if(row.quantityValue<=0){
          				row.quantity = '';
          			}          			
          			row.amount = product.price * row.quantityValue;
          			row.footer = false;
          			$('#dg').datagrid('refreshRow', index);
          			StockIssued.showTotalGrossWeightAndAmount();
		    	}
		    },
		    onSelect:function(index,row){
		    	
		    },
		    onBeforeEdit :function(rowIndex,rowData){
		    	var rows = $('#dg').datagrid('getRows');
		    	for(var i=0;i<rows.length;++i){
		    		if(i==rowIndex)
		    			continue;
		    		$('#dg').datagrid('endEdit',i);
		    	}		    	
		    	$('#dg').datagrid('selectRow', rowIndex);		    	
			},
			onDblClickRow:function(rowIndex, rowData) {
				if(!StockIssued.isAuthorize){
					return false;
				}
				$('.ErrorMsgStyle').html('').hide();
				var msg = Utils.getMessageOfRequireCheck('staffCode', 'Nhân viên', true);
				if(msg.length>0){
					$('#errMsg').html(msg).show();
					return false;
				}
				$('#dg').datagrid('beginEdit', rowIndex);	
				$('.datagrid-row-editing td[field=quantity] input').css('text-align','right');
				var productId = rowData.productId;
				var product = StockIssued.lstProductMap.get(productId);
				if(!isNullOrEmpty(rowData.productId) && product!=null){					
					StockIssued.disabledProductIdCols(rowIndex, product.productCode);
					var color =  StockIssued.addStyleRowSaleProduct(rowIndex,rowData);
					if(color==WEB_COLOR.ORANGE){
						$('#span' +rowIndex).css('color',WEB_COLOR.ORANGE_CODE);
					}else if(color==WEB_COLOR.RED){
						$('#span' +rowIndex).css('color',WEB_COLOR.RED_CODE);
					}else {
						$('#span' +rowIndex).css('color',WEB_COLOR.BLACK_CODE);
					 }					
				}else{
					$('.datagrid-row-editing td[field=productId] input').focus();
				}				
			}
		});
		StockIssued.showTotalGrossWeightAndAmount();
	},
	setEventInput:function(){	
		StockIssued.mapProductLot = new Map();	
		$('#staffCode').focus();
		disabled('btnExportExcel');	
		disabled('btnExportExcelNew');
		//applyDateTimePicker('#issuedDate');
		//$('#issuedDate').val(getCurrentDate());
		$('.MySelectBoxClass').customStyle();	
		$('#staffCode').combobox();
		var data = $('#staffCode').combobox('getData');
		for(var i=0;i<data.length;++i){
			var staff = new Object();
			staff.staffCode = data[i].value;
			staff.staffName = data[i].text;
			StaffUtils.listStaffSale.push(staff);
		}
		$('#staffCode').combobox({valueField : 'staffCode',textField : 'staffCode',panelWidth:206,data : StaffUtils.listStaffSale});
		$('#staffCode').combobox({	 
			filter: function(q, row){
				q = new String(q).toUpperCase();
				var opts = $(this).combobox('options');
				return row[opts.valueField].indexOf(q)>=0;
			},		
			onSelect:function(row){
				$('#dg').datagrid('acceptChanges');
				if(!isNullOrEmpty(row.staffCode)){
					if (StockIssued._hasStaffCode != null) {
						$.messager.confirm('Xác nhận', 'Chọn lại NVBH thì danh sách sẽ bị mất, bạn có muốn chọn lại NVBH không ?', function(r){
							if (r){
								StockIssued._hasStaffCode = row.staffCode;
								StockIssued.synchronization();
								$('#errMsg').html('').hide();
							}else{// lampv-for map product to device
								$('#staffCode').combobox('setValue', StockIssued._hasStaffCode);
							}
						});	
					} else {
						StockIssued._hasStaffCode = row.staffCode;
						StockIssued.synchronization();
						$('#errMsg').html('').hide();
					}
				}
			},
			formatter: function(row) {
				return '<span style="font-weight:bold">' + Utils.XSSEncode(row.staffCode) + '</span><br/>' + '<span style="color:#888">' + Utils.XSSEncode(row.staffName) + '</span>';
			},
			onChange:function(){
			}
		});			
		$('.datagrid-row-editing td[field=quantity] input').live('focus', function(e){
			$(this).attr('id','convfact_value' +StockIssued.indexEditing);			
			$(this).attr('maxlength','8');
			Utils.bindFormatOnTextfield('convfact_value' +StockIssued.indexEditing, Utils._TF_NUMBER_CONVFACT);	
			$(this).css('text-align','right');
		});
		$('.datagrid-row-editing td[field=quantity] input').live('keyup', function(e){			
			var convfact_value = $(this).val().trim();			
			if(!isNullOrEmpty(convfact_value)) {	
				var productCode = $('td[field=productId] input.validatebox-text').val().trim().toUpperCase();
				var product = StockIssued.lstProductCodeMap.get(productCode);
				if(product!=null && StockIssued.validateQuantity(convfact_value, product.convfact)){
					var quantity = getQuantity(convfact_value, product.convfact);
					var totalAmount = quantity * product.price;
					$('.datagrid-row-editing td[field=amount] div').html(formatCurrency(totalAmount));										
				}
			}			
			if(e.keyCode==keyCodes.ENTER) {		    	
		    	$('#dg').datagrid('acceptChanges');
		    	StockIssued.appendOrSelectRow();		    	
			}else {
				StockIssued.bindNextAndPrevTextField(e,'quantity');
		    }
		});		
		$('td[field=productId] input.validatebox-text').live('keypress', function(e) {
			var productCode = $(this).val().trim();
			var errType = StockIssued.validateProduct(productCode);
			if(e.keyCode == keyCode_TAB && !e.shiftKey) {		
				if(errType==ERROR.NOT_ERROR){
					var product = StockIssued.lstProductCodeMap.get(productCode);
					StockIssued.selectProductId = product.productId;
		    		$('.datagrid-row-editing td[field=productName] div').html(product.productName);
		    		$('.datagrid-row-editing td[field=price] div').html(formatCurrency(product.price));					    		
					$('.datagrid-row-editing td[field=stockQuantity] div').html(formatQuantityEx(product.stockQuantity, product.convfact));					    		
		    		$('.ErrorMsgStyle').html('').hide();	
		    		StockIssued.disabledInputTextFiled(false);		    		
				}else if(productCode.length>0){					
					var msg =  '';
					if (errType == ERROR.NOT_EXISTS_SALE_CAT) {
						msg = 'không thuộc ngành hàng bán của nhân viên ';
					} else if (errType == ERROR.NOT_EXISTS) {
						msg = 'không tồn tại trong kho đơn vị ';
					}					
					$('#errMsg').html('Mã sản phẩm ' + Utils.XSSEncode(msg) + ' .Vui lòng chọn lại').show();
					StockIssued.selectProductId = null;		
					$('.datagrid-row-editing td[field=productId] input').focus();	
					StockIssued.disabledInputTextFiled(true);
					return false;
				}				
			}else if(e.keyCode == keyCode_F9 && errType==ERROR.NOT_ERROR) {
				StockIssued.showProductEsyUIDialog();		
			}
			if(errType==ERROR.NOT_ERROR){
				StockIssued.disabledInputTextFiled(false);
			}	
			StockIssued.bindNextAndPrevTextField(e,'productId');
		});		
		$('td[field=productId] input.validatebox-text').live('blur',function(){
			var productCode = $(this).val().trim();
			var errType = StockIssued.validateProduct(productCode);
			var msg =  '';
			if(errType==ERROR.NOT_EXISTS_SALE_CAT){
				msg = 'không thuộc ngành hàng bán của nhân viên.';
			}else if(errType==ERROR.IS_EQUIPMENT){
				msg = 'thuộc ngành hàng thiết bị.';
			}else if(errType==ERROR.NOT_EXISTS){
				msg = 'không tồn tại trong kho đơn vị.';
			}
			if(msg.length>0){
				StockIssued.selectProductId = null;
				$('#saleInputErr').html('Mã sản phẩm ' + Utils.XSSEncode(msg) + ' Vui lòng chọn lại').show();
				$('.datagrid-row-editing td[field=productId] input').focus();
				StockIssued.disabledInputTextFiled(true);
				return false;
			}else{
				StockIssued.disabledInputTextFiled(false);	
			}				
		});
	},
	appendOrSelectRow:function(){
		var editIndex = $('#dg').datagrid('getRows').length;			
    	if(StockIssued.indexEditing == editIndex-1) {    		
    		var row = $('#dg').datagrid('getRows')[editIndex-1];
    		var product = StockIssued.lstProductMap.get(row.productId);
    		if(row==null || isNullOrEmpty(row.productId)){  
    			editIndex = editIndex-1;
    		}else if(!isNullOrEmpty(row.productId)){    			
    			if(product!=null){
    				if(isNullOrEmpty(row.quantity) || StockIssued.validateQuantity(row.quantity,product.convfact)){
    					$('#dg').datagrid('appendRow',new Object());
    				}   				
    			}
    		}	
    	}else{
    		var row = $('#dg').datagrid('getRows')[StockIssued.indexEditing];
    		var product = StockIssued.lstProductMap.get(row.productId);
    		editIndex = StockIssued.indexEditing +1;
    	}    	
    	StockIssued.indexEditing = editIndex;
    	$('#dg').datagrid('selectRow', editIndex).datagrid('beginEdit', editIndex);
    	var productCode = $('.datagrid-row-editing td[field=productId] input.validatebox-text').val();
    	if(!isNullOrEmpty(productCode)){    		
    		StockIssued.disabledProductIdCols(editIndex, productCode);
    	}else{
    		$('.datagrid-row-editing td[field=productId] input').focus();
    	}     	
	},
	bindNextAndPrevTextField:function(e,clazz){
		var editIndexRow = StockIssued.indexEditing;
		var len = $('#dg').datagrid('getRows').length;
		var index = editIndexRow;	
		var shiftKey = e.shiftKey && e.keyCode == keyCodes.TAB;	
		if(e.keyCode == keyCodes.ARROW_DOWN){	
			++index;			
		}else if(e.keyCode == keyCodes.ARROW_UP){			
			--index;							
		}else if(shiftKey){
			var product = $('#dg').datagrid('getRows')[editIndexRow];
			if(product!=null && !isNullOrEmpty(product.productId) && StockIssued.lstProductMap.get(product.productId)!=null){
				--index;	
			}else if(clazz=='productId'){
				--index;
			}
		}	
		if(index>-1 && index<len && index!=editIndexRow){			
			$('#dg').datagrid('beginEdit', index);	
			var productCode = $('.datagrid-row-editing td[field=productId] input.validatebox-text').val();
			StockIssued.indexEditing = index;
			var product = $('#dg').datagrid('getRows')[index];
			if(product!=null && !isNullOrEmpty(product.productId) && StockIssued.lstProductMap.get(product.productId)!=null){
				StockIssued.disabledProductIdCols(index, productCode);
				if(clazz=='productId'){
					if(shiftKey){
						$('.datagrid-row-editing td[field=quantity] input').focus();
					}
				}else if(clazz=='quantity'){
					$('.datagrid-row-editing td[field=quantity] input').focus();
				}		
			}else{	
				$('td[field=productId] input.validatebox-text').attr('disabled',false);
				$('td[field=productId] input.validatebox-text').focus();
			}
		}else if(shiftKey && clazz=='quantity'){
			$('td[field=productId] input.validatebox-text').focus();			
		}
	},
	disabledProductIdCols:function(editIndex,productCode){		
		$('#span' +editIndex).remove();			
		$('td[field=productId] div table tbody tr td input').css('display','none');
		$('td[field=productId] div table tbody tr td span').css('display','none');
		$('td[field=productId] div table tbody tr td').append('<span id="span' +editIndex+'"></span');
		$('#span' +editIndex).html(Utils.XSSEncode(productCode));		
		$('.datagrid-row-editing td[field=quantity] input').focus();
	},
	disabledInputTextFiled:function(flag){
		if(flag){
			disabled('btnStockOut');
			$('.datagrid-row-editing td[field=quantity] input').attr('disabled','disabled');			
		}else{
			$('.datagrid-row-editing td[field=quantity] input').attr('disabled',false);    		
			enable('btnStockOut');	
		}		
	},
	synchronization:function(byShop){
		StockIssued.lstProductExport = new Array();
		var url = '';
		if(byShop!=undefined && byShop){
			url = '/stock/issued/search-product?byShop=true';
		}else {
			var vansaleCode = $('#staffCode').combobox('getValue').trim();
			url = '/stock/issued/search-product?isF9=false&vansaleCode=' + vansaleCode;
		}		
		StockIssued.lstProductExport.push(new Object());		
		$.getJSON(url, function(data) {
			StockIssued.lstProductMap = new Map();
			StockIssued.lstProductCodeMap = new Map();
			if(data.error != undefined && data.error != null && data.error == true){
				$('#errMsg').html(data.errMsg).show();
				StockIssued.lstProductArray = null;
				StockIssued.initGrid();
				disabled('btnStockOut');
			}else{
				enable('btnStockOut');
				var len = 0;
				if(byShop!=undefined && byShop){
					var stockTotalArray = data.stock_total;
					len = data.stock_total.length;				
					for(var i = 0; i < len; i++) {
						obj = stockTotalArray[i];
						StockIssued.lstProductCodeShopMap.put(obj.product.productCode, obj); /*Danh sach san pham theo nganh hang*/
					}
					StockIssued.initGrid();
				}else{
					StockIssued.lstProductArray = data.rows;				
					len = StockIssued.lstProductArray.length;
					for(var i = 0; i < len; i++) {
						var obj = StockIssued.lstProductArray[i];
						StockIssued.lstProductMap.put(obj.productId, obj); /*Danh sach san pham theo nganh hang*/
						StockIssued.lstProductCodeMap.put(obj.productCode,obj); /*Danh sach san pham */
					}			
					StockIssued.initGrid();
					$('#dg').datagrid('beginEdit', 0);
				}
			}
		});
	},
	validateProduct:function(productCode){		
		if(isNullOrEmpty(productCode)){			
			return ERROR.NOT_ERROR;
		}
		productCode = productCode.trim().toUpperCase();		
		product = StockIssued.lstProductCodeShopMap.get(productCode);		
		if(product==null){
			return ERROR.NOT_EXISTS;
		}		
		product = StockIssued.lstProductCodeMap.get(productCode);
		if(product==null){			
			return ERROR.NOT_EXISTS_SALE_CAT;
		}		
		return ERROR.NOT_ERROR;
	},
	validateQuantity:function(value,convfact){
		if(isNullOrEmpty(value) || isNullOrEmpty(convfact)){
			return false;
		}
		var quantity = getQuantity(value,convfact);
		if(isNaN(quantity)){
			return false;
		}
		return true;
	},
	showProductEsyUIDialog:function(_dgIndex){
		$('#issSearchProductDialog').unbind('click');
		StockIssued.storeProduct = new Map();
		$('#productDialog').css("visibility", "visible");
		var html = $('#productDialog').html();		
		$('.SelProduct').each(function(){
			$(this).val('');
		 });			
		var msg = Utils.getMessageOfRequireCheck('staffCode', 'Mã nhân viên',true);	
		if(msg.length>0){
			return false;
		}		
		$('#productEasyUIDialog').dialog({  	       
	        closed: false,  
	        cache: false,  
	        modal: true,
	        width : 750,
	        height :550,
	        onOpen: function(){
	        	$('.easyui-dialog #errMsg').html('').hide();
	        	$('.easyui-dialog #issSearchProductDialog').css('margin-left', 285);
				$('.easyui-dialog #issSearchProductDialog').css('margin-top', 10);	
				$('.easyui-dialog #productCode').val('');
				$('.easyui-dialog #productName').val('');
				Utils.bindAutoSearch();
				$('#issSearchProductDialog').bind('click',StockIssued.searchProduct);
	        	$('.easyui-dialog #productGrid').datagrid({
					url : '/stock/issued/search-product',
					autoRowHeight : true,
					rownumbers : true, 
					singleSelect: true,
					pagination:true,
					rowNum : 10,
					scrollbarSize : 0,
					fitColumns:true,
					cache: false, 
					pageList  : [10,20,30],
					queryParams:{
						isF9 : true,
						vansaleCode : $('#staffCode').combobox('getValue').trim(),
						exceptProductIdStr:StockIssued.exceptProductIdStr
					},
					width : ($('.easyui-dialog #productGridContainer').width()),
				    columns:[[				          
				        {field: 'productCode',title: 'Mã sản phẩm', width: 100, sortable:false,resizable:false,align: 'left' },
					    {field: 'productName',title: 'Tên sản phẩm',  width: 380,sortable:false,resizable:false, align: 'left' },
					    {field: 'stockQuantity', title: 'Tồn kho', width: 80,sortable:false,resizable:false, align: 'right', 
					    	formatter: function(value,row,index){
					    		return formatQuantityEx(row.stockQuantity,row.convfact);
					    	}},
					    {field: 'price', title: 'Giá',width: 70, sortable:false,resizable:false, align: 'right', formatter: GridFormatterUtils.currencyCellFormatter},
					    {field: 'amt', title: 'Số lượng',width: 100, sortable:false,resizable:false,align: 'right',formatter:  function(value,row,index){
					    	return '<input index=' +index+' row-id="' +row.id+'" id="productRow_'	+ row.id
					    	+'" size="5" type="text" maxlength="8" style="text-align:right;margin: 0;width:77px;" onkeypress="return NextAndPrevTextField(event, this,\'SelProduct\');" class="IssuedStock_AmountValue InputTextStyle InputText1Style SelProduct" onblur="StockIssued.amountBlured('
					    	+ index +');" onchange="StockIssued.amountChanged(' + index +'); StockIssued.formatQuantity(' +row.id+',' +row.convfact+');">';
				    	}}					    
				    ]],
				    onBeforeLoad:function(param){
				    	$('.easyui-dialog .SelProduct').each(function(){
							  var index = $(this).attr('index');
							  var obj = StockIssued.getData(index,$(this));										 
							  if(obj!=null){
								  StockIssued.storeProduct.put(obj.productId,obj);  
							  }else {
								  var rows = $('.easyui-dialog #productGrid').datagrid ('getRows')[index];
								  var temp = StockIssued.storeProduct.get(rows.productId);
								  if(temp!=null){
									  StockIssued.storeProduct.remove(temp.productId);
								  }
							  }									 
						  });	
				    },
				    onLoadSuccess :function(data){   	 
				    	$('.datagrid-header-rownumber').html('STT');				    		
				    	Utils.bindFormatOnTextfieldInputCss('SelProduct',Utils._TF_NUMBER_CONVFACT);
						$('.easyui-dialog .SelProduct').each(function(){
						  var productId = $(this).attr('row-id');							 								  
						  var obj = StockIssued.storeProduct.get(productId);
						  if(obj!=null){
							  $(this).val(obj.quantity);
						  }else{
							  $(this).val('');
						  }
						});			
				    }
				});  
	        },	        
	        onClose : function(){
	        	$('#productDialog').html(html);
	        	$('#productDialog').css("visibility", "hidden");
	        	$('.easyui-dialog #errMsg').html('').hide();
	        	$('#dg').datagrid('reload');
	        	$('.AmountProduct');
	        	Utils.bindFormatOnTextfieldInputCss('AmountProduct', Utils._TF_NUMBER_CONVFACT);
	        }
	     });
	},	
	searchProduct: function(){			
		var code = encodeChar($('.easyui-dialog #productCode').val().trim());
//		var name = encodeChar($('.easyui-dialog #productName').val().trim());
		var name = $('.easyui-dialog #productName').val().trim();
		var vansaleCode = $('#staffCode').combobox('getValue').trim();
		$('.easyui-dialog #productGrid').datagrid('load',{productCode :code, productName: name,isF9:true,
			vansaleCode:vansaleCode,exceptProductIdStr:StockIssued.exceptProductIdStr});
		return false;
	},	
	showTotalGrossWeightAndAmount:function(){
		var totalGrossWeight = 0;
		var totalAmount = 0;
		var temp = 0;		
		$('#dg').datagrid('acceptChanges');		
		var PRODUCTS = $('#dg').datagrid('getRows');
		len = PRODUCTS.length;	
		var exceptProductIdArr = new Array();
		for(var i=0;i<len;++i){
			var product = PRODUCTS[i];
			if(product==null || isNaN(product.productId)){
				continue;
			}else if(isNullOrEmpty(product.quantity)){
				continue;
			}else if(isNullOrEmpty(product.convfact) || isNaN(product.convfact)){
				continue;
			}else if(isNullOrEmpty(product.price) || isNaN(product.price)){
				continue;
			}else if(isNullOrEmpty(product.grossWeight) || isNaN(product.grossWeight)){
				continue;
			}
			temp = getQuantity(product.quantity,product.convfact);
			totalGrossWeight += Number(temp*product.grossWeight);
			totalAmount += Number(temp * product.price);
			var color =  StockIssued.addStyleRowSaleProduct(i,product);
			if(color==WEB_COLOR.ORANGE){
				$('span.spStyle' +i).each(function(){ $(this).css('color',WEB_COLOR.ORANGE_CODE);});
			}else if(color==WEB_COLOR.RED){
				$('span.spStyle' +i).each(function(){ $(this).css('color',WEB_COLOR.RED_CODE);});
			}else {
				  $('span.spStyle' +i).each(function(){ $(this).css('color',WEB_COLOR.BLACK_CODE);});
			 }
			exceptProductIdArr.push(product.productId);
		}	
		StockIssued.exceptProductIdStr = exceptProductIdArr.toString();
		var footers = new Array();
		var ft = new Object();
		ft.productName = '<b id="footerProductName">Tổng</b>';
		ft.quantity = '<b>' + formatFloatValue(totalGrossWeight,2) + ' kg</b>';
		ft.amount = '<b>' + formatFloatValue(totalAmount,2) + ' VNĐ</b>';
		ft.footer = true;
		footers.push(ft);
		$('#dg').datagrid('reloadFooter',footers);
		$('#footerProductName').parent().css('text-align','center');
	},
	addStyleRowSaleProduct:function(i,product){
		var color = WEB_COLOR.BLACK;
		if(product==null){
			return color;
		}else{
			var stockQuantity = product.stockQuantity;
			var convfact = product.convfact;			
			if(stockQuantity==null || convfact==null){
				return color;
			}
			if(Number(stockQuantity)<0){
				return WEB_COLOR.RED;
			}
			var quantityValue = product.quantityValue;
			if(isNullOrEmpty(product.quantityValue)){
				return color;
			}			
			if(Number(stockQuantity)<Number(quantityValue)){
				return WEB_COLOR.ORANGE;
			}
		}		
		return color;
	},
	getData:function(index,selector){		
		var product = $('.easyui-dialog #productGrid').datagrid ('getRows')[index];	
		var quantity = selector.val().trim();
		if(quantity.length==0){			
			return null;
		}
		var row = new Object();
		row.productId = product.productId;
		row.productCode = product.productCode;
		row.productName = product.productName;
		row.stockQuantity = product.stockQuantity;
		row.grossWeight = product.grossWeight;
		row.lot = product.checkLot;
		row.price = product.price;
		row.convfact = product.convfact;
		row.quantity = formatQuantityEx(quantity, row.convfact);
		row.quantityValue = getQuantity(row.quantity, row.convfact);
		if(isNaN(row.quantityValue)){
			return null;
		}		
		if (row.quantityValue <= 0) {
			row.quantity = '';
		}		
		row.amount = product.price * row.quantityValue;
		row.footer = false;
		return row;
	},	
	selectListProducts: function(){
		var mapProductSelection = new Map();
		$('.easyui-dialog #errMsg').html('').hide();
		var flag = true;
		var index = 0;
		$('.easyui-dialog .SelProduct').each(function(){
			var index = $(this).attr('index');
			if($(this).val().length>0){				
				var obj = StockIssued.getData(index,$(this));
				if(obj!=null){
					index = obj.productId;
					if(obj.quantity == '0/0' ||obj.quantity == '0'|| obj.quantity==''){										
						$('.easyui-dialog #errMsg').html('Giá trị số lượng không hợp lệ').show();
						flag = false;
						return false;
					}						
					if(obj.stockQuantity < obj.quantityValue){
						$('.easyui-dialog #errMsg').html('Mã sản phẩm ' + Utils.XSSEncode(obj.productCode + ' - ' + obj.productName) + '. Vượt quá giá trị tồn kho').show();
						flag = false;						
						return false;			
					} 				
					mapProductSelection.put(obj.productId, obj);
					var storeObject = StockIssued.storeProduct.get(obj.productId);
					if(storeObject!=null){
						StockIssued.storeProduct.remove(obj.productId);
					}
				}				
			}else{
				var rows = $('.easyui-dialog #productGrid').datagrid ('getRows')[index];
				var temp = StockIssued.storeProduct.get(rows.productId);
				if(temp!=null){
				  StockIssued.storeProduct.remove(temp.productId);
				}
			}
		});
		if(!flag){	
			 $('#productRow_' + index).focus();
			return false;
		}
		for(var i=0;i<StockIssued.storeProduct.size();i++){
			var product = StockIssued.storeProduct.get(StockIssued.storeProduct.keyArray[i]); 
			mapProductSelection.put(product.productId, product);
		}
		if(mapProductSelection.size()==0){
			$('.easyui-dialog #errMsg').html('Vui lòng chọn sản phẩm cần xuất kho.').show();
			return false;
		}	
		for(var i=0;i<mapProductSelection.size();i++){
			var data = mapProductSelection.get(mapProductSelection.keyArray[i]); 
			var len = $('#dg').datagrid('getRows').length-1;
			$('#dg').datagrid('insertRow',{index:len, row:data});
		}		
		StockIssued.showTotalGrossWeightAndAmount();
		$('.easyui-dialog').dialog('close');
	},
	duplicateProducts:function(){
		var arrDelete = new Array();
		var len = $('#dg').datagrid('getRows').length;
		var mapTestingDuplicate = new Map();
		var mapDuplicateIndex = new Map();
		for(var i=0;i<len;++i){			
			var product = $('#dg').datagrid('getRows')[i];
			var productId = product.productId;
			if(mapTestingDuplicate.get(productId)==null){
				mapTestingDuplicate.put(productId,product);		
				mapDuplicateIndex.put(productId,i);
			}else{
				var obj = new Object();
				obj.firstIndex = mapDuplicateIndex.get(productId);
				obj.lastIndex = i;
				arrDelete.push(obj);
			}			
		}
		return arrDelete;
	},
	amountChanged: function(rowId){		
		$('.easyui-dialog #errMsg').html('').hide();
		if(!StockIssued.amountBlured(rowId)){			
			$('#errCont').show();	
		}else{
			$('.easyui-dialog #errMsg').html('').hide();	
		}
	},
	amountBlured: function(index){	
		var rows = $('.easyui-dialog #productGrid').datagrid ('getRows')[index];
		var rowId = rows.id;
		var fAmount = rows.quantity;
		var convfact = rows.convfact;
		var productCode = rows.productCode;
		var productName = rows.productName;
		var amount = $('#productRow_' + rowId).val().trim();	
		if(amount.length==0){			
			return true;
		}
		var format = formatQuantity(amount,convfact);
		if(format.length==0){
			$('.easyui-dialog #errMsg').html('Giá trị số lượng không hợp lệ').show();			
			return false;
		}else{
			amount = format;
			if(amount == '0/0' ||amount == '0'|| amount==''){
				$('.easyui-dialog #errMsg').html('Giá trị số lượng không hợp lệ').show();
				setTimeout(function() {	
					$('#productRow_' + rowId).focus();					
				}, 50);				
				return false;
			}
		}
		var fTotal = parseInt(getQuantity(fAmount, convfact));
		var total = parseInt(getQuantity(amount, convfact));		
		if(total.length>6){
			$('.easyui-dialog #errMsg').html('Giá trị số lượng không hợp lệ').show();	
			return false;
		}
		if(fTotal < total){
			$('.easyui-dialog #errMsg').html('Mã sản phẩm ' + Utils.XSSEncode(productCode + ' - ' + productName) + '. Vượt quá giá trị tồn kho').show();					
			setTimeout(function() {	
				$('#productRow_' + rowId).focus();					
			}, 50);				
			return false;		
		} else {			
			return true;
		}
	},
	delSelectedRow:function(index){	
		$('#errExcelMsg').html('').hide();		
		$.messager.confirm('Xác nhận', 'Bạn có muốn xóa sản phẩm này ?', function(ret){
			if (ret == true) {
				$('#dg').datagrid('deleteRow',index);				
				var rows = $('#dg').datagrid ('getRows');
				if(rows.length == 0){
					$('#dg').datagrid('appendRow',new Object());
				}
				$('#dg').datagrid('loadData',rows);
				StockIssued.showTotalGrossWeightAndAmount();
			}
		});
	},	
	stockOutVanSale:function(){
		if(!StockIssued.isAuthorize){
			return false;
		}
		//var msg = '';		
		$('#dg').datagrid('acceptChanges');
		$('.ErrorMsgStyle').html('').hide();	
		var msg = Utils.getMessageOfRequireCheck('staffCode', 'Mã nhân viên',true);		
		if(msg.length==0){
			msg = Utils.getMessageOfRequireCheck('code', 'Mã phiếu');			
		}	
		if(msg.length==0){
			msg = Utils.getMessageOfSpecialCharactersValidate('code', 'Mã phiếu', Utils._CODE);
		}
		if(msg.length==0){
			msg = Utils.getMessageOfRequireCheck('issuedDate', 'Ngày xuất');			
		}
		if(msg.length==0){
			msg = Utils.getMessageOfInvalidFormatDate('issuedDate', 'Ngày xuất');
		}
		if(msg.length==0){
			msg = Utils.getMessageOfRequireCheck('car', 'Xe');	
		}
		var rows = $('#dg').datagrid('getRows');
		if(rows.length==0){
			msg='Vui lòng chọn sản phẩm cần xuất kho.';
		}else{
			var row = $('#dg').datagrid('getRows')[0];
			if(isNullOrEmpty(row.productId)){
				msg='Vui lòng chọn sản phẩm cần xuất kho.';
			}			
		}		
		if(msg.length>0){
			$('#errMsg').html(msg).show();
			return false;
		}
		var isError = true;		
		var data = new Object();		
		data.vansaleCode = $('#staffCode').combobox('getValue');
		data.code = $('#code').val().trim();		
		data.carId = $('#car').val();
		data.issuedDate = $('#issuedDate').val().trim();		
		var selectedId = new Array();
		var selectedQty = new Array();
		var lstProductLot = new Array();
		
		var MAP = new Map();
		for(var i=0;i<rows.length;++i){
			var product = rows[i];
			if(isNullOrEmpty(product.productId)){
				continue;
			}
			if (product.quantityValue == 0) {
				$('#errMsg').html('Bạn chưa nhập số lượng xuất kho cho sản phẩm ' + Utils.XSSEncode(product.productCode) + '.').show();
				$('#dg').datagrid('selectRow', i);
				return false;
			}
			selectedId.push(product.productId);
			selectedQty.push(product.quantityValue);
			if(MAP.get(product.productCode)==null)
				MAP.put(product.productCode,i);
			else{
				$('#errMsg').html('Mã sản phẩm ' + Utils.XSSEncode(product.productCode) + ' có hơn 1 dòng trong danh sách.').show();
				$('#dg').datagrid('selectRow',i);
				return false;
			}
			
			if(sys_separation_auto==0 && product.lot==1){
				var quantity = product.quantityValue;
				var list_product_lots = StockIssued.mapProductLot.get(product.productId);
				if(list_product_lots==null || list_product_lots.length<=0){
					$('#errMsg').html('Vui lòng chọn lô cần xuất cho sản phẩm ' + Utils.XSSEncode(product.productCode)).show();	
					$('#dg').datagir('selectRow',i);
					return false;
				}else{
					var result = '';
					var totalLot = 0;
					for(var j=0;j<list_product_lots.length;++j){
						var productLot = list_product_lots[j];
						if(productLot!=undefined && productLot.quantityLot>0){
							result += productLot.lot + ':' + productLot.quantityLot + ';';
							totalLot+= Number(productLot.quantityLot);
						}
					}						
					if(totalLot!=quantity){
						$('#errMsg').html('Mã sản phẩm ' + Utils.XSSEncode(product.productCode) + ' có số lượng nhập phải bằng tổng nhập tất cả các lô.').show();	
						$('#dg').datagir('selectRow',i);
						return false;;
					}
					if(result.length>0){
						result = result.substring(0,result.length-1);						
					}
					lstProductLot.push(result);
				}
			}else{
				lstProductLot.push('');
			}			
		}
		data.lstProduct_Id = selectedId;
		data.lstProduct_Qty = selectedQty;
		data.lstProductLot = lstProductLot;		
		Utils.addOrSaveData(data, "/stock/issued/stockOutVanSale", StockIssued._xhrSave, 'errMsg', function(result){
			if(!result.error){
				StockIssued.isAuthorize = false;
				StockIssued.disabledDataOnForm();
			}else{				
				$('#errMsg').html(result.errMsg).show();				
			}
			if(result.stockTransCode!=undefined && result.stockTransCode!=null){
				$('#stockOutVanSale').val(result.stockTransCode);
			}
		}, 'loading', '', false, 'Bạn có muốn xuất kho không?');		
		
		return false;
	},
	disabledDataOnForm:function(){
		$('#btnExportExcel').unbind('click');
		$('.ErrorMsgStyle').html('').hide();	
		$('.CHOOSE_LOT').each(function(){
			var html = '<a style="color:#989898" href="javascript:void(0)">Chọn lô</a>';
			$(this).replaceWith(html);
		});		
		disabled('shopCode');
		disabled('btnStockOut');				
		enable('btnExportExcel');
		enable('btnExportExcelNew');
		disabled('issuedDate');		
		disabled('code');
		$('#car').attr('disabled','disabled');$('#car').parent().addClass('BoxDisSelect');
		$('#staffCode').combobox('disable');		
		$('#staffCode').parent().addClass('BoxDisSelect');
		$('#btnExportExcel').bind('click',StockIssued.exportStockOutVanSale);
		$('td[field=delete] a').removeAttr('onclick');		
	},
	exportStockOutVanSale:function(){		
		$('.ErrorMsgStyle').hide();
		/**
		 * @author hunglm16
		 * @since JUNE 23, 2014
		 * @description Update xuat file giong bao cao 7-8-3.Phieu xuat kho kiem van chuyen noi bo
		 * */
		var msg = Utils.getMessageOfRequireCheck('ipNameTransDialog', 'Tên lệnh điều động');		
		if(msg.length==0){
			msg = Utils.getMessageOfSpecialCharactersValidate('ipNameTransDialog', 'Tên lệnh điều động', Utils._NAME);
		}
		if(msg.length==0){
			msg = Utils.getMessageOfRequireCheck('fDateDialog', 'Ngày lệnh điều động');			
		}
		if(msg.length==0){
			msg = Utils.getMessageOfInvalidFormatDate('fDateDialog', 'Ngày lệnh điều động');			
		}
		var currentTime = new Date();
		var month = currentTime.getMonth() + 1;
		var day = currentTime.getDate();
		var year = currentTime.getFullYear();
		if(msg.length==0&&!Utils.compareDate(day + '/' + month + '/' + year, $('#fDateDialog').val().trim())){
			msg = 'Từ ngày phải lớn hơn bằng ngày hiện tại';
			$('#fDateDialog').focus();
		}
		if(msg.length==0){
			msg = Utils.getMessageOfRequireCheck('ipContentTransDialog', 'Nội dung');			
		}	
		if(msg.length==0){
			msg = Utils.getMessageOfSpecialCharactersValidate('ipContentTransDialog', 'Nội dung', Utils._NAME);
		}
		if(msg.length>0){
			$('#errMsg2').html(msg).show();
			return false;
		}
				
		var data = new Object();
		data.shopId = $('#shopId').val().trim();
		data.staffCode = '';
		data.issuedDate = $('#fDateDialog').val().trim();
		data.stockTransId = $('#stockOutVanSale').val();
		data.transacContent = $('#ipContentTransDialog').val().trim();
		data.transacName = $('#ipNameTransDialog').val().trim();
		
		//Update them tham so xuat file report
		var kData = $.param(data,true);
		$('#divOverlay').addClass('Overlay');
		$('#divOverlay').show();
		$('#imgOverlay').show();
		$.ajax({
			type : "POST",
			url : "/stock/issued/print-pxkkvcnb",
			data :(kData),
			dataType: "json",
			success : function(data) {
				$('#divOverlay').removeClass('Overlay');
				$('#imgOverlay').hide();
				$('#divOverlay').hide();				
				if(!data.error) {
					StockIssued.nameTransDialog = $('#ipNameTransDialog').val().trim();
					StockIssued.issuedDateDialog = $('#fDateDialog').val().trim();
					window.location.href = data.path;
				} else {
					$('#errMsg2').html(data.errMsg).show();					
				}
			},
			error:function(XMLHttpRequest, textStatus, errorDivThrown) {
				$('#divOverlay').removeClass('Overlay');
				$('#divOverlay').hide();
				hideLoading(loading);
			}
		});
		return false;
		
	},
	chooseLotDialog:function(index){
		enable('btnSelectLot');
		$('.ErrorMsgStyle').html('').hide();
		var productSelection = $('#dg').datagrid('getRows')[index];
		if(productSelection==null || isNaN(productSelection.quantityValue)){
			return;
		}		
		StockIssued.PRODUCT_ID = productSelection.productId;
		StockIssued.PRODUCT_TOTAL = productSelection.quantityValue;	
		StockIssued.storeProduct = new Map();		
		$('#productLotDialog').css("visibility", "visible");
		var html = $('#productLotDialog').html();
		$('#productLotEasyUIDialog').dialog({closed: false,cache: false,modal: true,width : 600,height :470,onOpen: function(){	   
	        	StockIssued.request = false;
	        	$('#productLotDialog').html('');
				$('.easyui-dialog #productLotGrid').datagrid({
					view: bufferview,	
				    singleSelect:true,				   
				    height: 250,
				    rownumbers : true,
				    autoRowHeight:true,				   
				    fitColumns:true,				    
				    scrollbarSize:20,
					width: 550,									
				    columns:[[				          
				        {field: 'lot',title: 'Số lô', width: 80, sortable:false,resizable:false,align: 'left' },
					    {field: 'quantity',title: 'Tồn kho',  width: 80,sortable:false,resizable:false, align: 'right',
				        	formatter:function(value,row,opts){
				        		return formatQuantity(row.quantity,StockIssued.CONVFACT);
				        	}
				        },
					    {field: 'quantityLot', title: 'Số lượng', width: 80,sortable:false,resizable:false, align: 'right',
				        	formatter:function(value,row,opts){				        		
				        		var amount =  formatQuantity(row.quantityLot,StockIssued.CONVFACT);
				        		if(StockIssued.request || row.quantityLot==0){
				        			amount = "";
				        		}
				        		return '<input onkeypress="return NextAndPrevTextField(event,this,\'QUANTITY\')" type="text" class="InputTextStyle InputText1Style QUANTITY" onblur="StockIssued.onChange(this);" onchange="return StockIssued.onChange(this);" quantity="' +row.quantity+'"  id="QUANTITY' +row.lot+'" product-lot-id="' +row.id+'" value="' +amount+'" lot="' +row.lot+'" />';
				        	}
					    },					    
					    {field: 'id',hidden: true}
					   
				    ]]
				});
				$.getJSON('/stock/issued/list-product-lots?productId=' +StockIssued.PRODUCT_ID, function(result){					
					var product = result.product;
					if(product!=null){
						StockIssued.CONVFACT = product.convfact;						
						$('#fancyProductCode').html(product.productCode);
						$('#fancyProductName').html(product.productName);
						$('#fancyProductTotalQuantity').html(formatQuantityEx(StockIssued.PRODUCT_TOTAL,StockIssued.CONVFACT));
						if(result.error!=null && result.error){
							$('#fancyboxError').html('Các lô của sản phẩm ' + Utils.XSSEncode(product.productCode) +' đã hết hạn sử dụng.').show();
							disabled('btnSelectLot');
							return false;
						}
					}	
					var lots = null;					
					var list_product_lots = StockIssued.mapProductLot.get(StockIssued.PRODUCT_ID);
					if(list_product_lots!=null){
						lots = list_product_lots;
					}else{
						lots = result.lots;	
						StockIssued.request = true;
					}
					for(var i=0;i<lots.length;++i){								
						var productLot = StockIssued.createProductLot(lots[i]);	
						$('#productLotGrid').datagrid('appendRow',productLot);
					}
					StockIssued.showTotalAndScrollBodyProductLotDialog();
					$('.datagrid-header-rownumber').html('STT');
					Utils.bindFormatOnTextfieldInputCss('QUANTITY',Utils._TF_NUMBER_CONVFACT);
				});
	        },	        
	        onClose : function(){
	        	enable('btnSelectLot');
	        	$('#productLotDialog').html(html);
	        	$('#productLotDialog').css("visibility", "hidden");
	        	$('#productLotGridContainer').html('<table id="productLotGrid" class="easyui-datagrid" ></table>');
	        }
	     });			
		$('#errMsg').hide();
		return false;
	},	
	createProductLot:function(productLot,convfact){
		var newrow = {
				lot:productLot.lot,
				quantity:productLot.quantity,
				quantityLot:productLot.quantityLot,
				id:productLot.id							
		};
		return newrow;		
	},
	onChange:function(selector){
		var isError = false;
		$('.easyui-dialog .QUANTITY').each(function(){
			var selector = $(this);
			var quantityLot = getQuantity($(selector).val().trim(), StockIssued.CONVFACT);
			var quantity = $(selector).attr('quantity');
			var lot = $(selector).attr('lot');
			if(isNaN(quantityLot)){	
				isError = true;
				$('#fancyboxError').html('Số lượng của lô ' +lot+' không hợp lệ.').show();
				setTimeout(function() {$(selector).focus();}, 50);	
				return false;
			}
			if(quantity<quantityLot){	
				isError = true;
				$('#fancyboxError').html('Số lượng của lô ' +lot+' vượt quá giá trị tồn kho.').show();
				setTimeout(function() {$(selector).focus();}, 50);	
				return false;
			}
			$(selector).val(formatQuantity($(selector).val().trim(), StockIssued.CONVFACT));
			if($(selector).val().length==0){			
				$(selector).val('');
			}
		});
		if(isError){
			return false;
		}		
		StockIssued.showTotalAndScrollBodyProductLotDialog();
		return true;
	},
	acceptSeparation:function(){
		var productId = StockIssued.PRODUCT_ID;		
		var list_product_lots = new Array();
		var sum_quantity = 0;
		var validate_error = false;
		$('.easyui-dialog .QUANTITY').each(function(){		
			var quantityDisplay = $(this).val().trim();
			var lot = $(this).attr('lot');
			var convfact = StockIssued.CONVFACT;
			var quantityLot = getQuantity(quantityDisplay, StockIssued.CONVFACT);
			var quantity = $(this).attr('quantity');
			if(!isNullOrEmpty(quantityLot.toString())){
				if(isNaN(quantityLot)){
					$(this).focus();
					validate_error = true;
					$('#fancyboxError').html('Số lượng của lô ' +lot + ' không hợp lệ.').show();
					return false;
				}
				if(quantityLot>quantity){
					$(this).focus();
					validate_error = true;
					$('#fancyboxError').html('Số lượng của lô ' +lot + ' vượt quá giá trị tồn kho.').show();
					return false;
				}
				var productLot = new Object();
				productLot.lot = $(this).attr('lot');	
				productLot.id = $(this).attr('product-lot-id');
				productLot.quantityLot = quantityLot;
				productLot.quantity = quantity;							
				list_product_lots.push(productLot);	
				sum_quantity+=quantityLot;				
			}
		});
		if(validate_error){
			return false;
		}
		var totalQuantity = getQuantity(StockIssued.PRODUCT_TOTAL, StockIssued.CONVFACT);
		if(totalQuantity!=sum_quantity){
			$('#fancyboxError').html('Tổng số lượng nhập tất cả các lô phải bằng ' +formatQuantity(StockIssued.PRODUCT_TOTAL, StockIssued.CONVFACT)).show();
			return false;
		}
		StockIssued.mapProductLot.put(productId,list_product_lots);
		$('.easyui-dialog').dialog('close');
	},
	
	closeDiglogLOT:function() {
		enable('btnSelectLot');
		$('.easyui-dialog').dialog('close');
	},
	
	showTotalAndScrollBodyProductLotDialog:function(){
		var totalQty = 0;
		var totalQtyLot = 0;	
		$('.easyui-dialog .QUANTITY').each(function(){
			var totalHTML = $(this).val().trim();
			totalQtyLot += Number(getQuantity(totalHTML, StockIssued.CONVFACT));
			totalQty += Number($(this).attr('quantity'));
			$(this).css('text-align','right');
			$(this).css('width','147px');
			$(this).css('margin','0px');	
			
		});		
		$('#fancyboxError').html('').hide();		
		$('#totalQuantity').html(formatQuantity(totalQty,StockIssued.CONVFACT));
		$('#totalAvailableQuantity').html(formatQuantity(totalQtyLot,StockIssued.CONVFACT));		
	},
	exportDSBHNVBH: function(){
		var msg = '';
		$('#errMsg').html('').hide();
		if(msg.length == 0){
			msg = Utils.getMessageOfRequireCheck('tDate','Đến ngày');
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfInvalidFormatDate('tDate','Đến ngày');
		}
		if(msg.length > 0){
			$('#errMsg').html(msg).show();
			return false;
		}
		var dataModel = new Object();		
		dataModel.toDate = $('#tDate').val().trim();
		ReportUtils.exportReport('/report/crm/kd3/export',dataModel);
	},
	exportDSBHNVBHSKU: function(){
		var msg = '';
		$('#errMsg').html('').hide();
		if(msg.length == 0){
			msg = Utils.getMessageOfRequireCheck('tDate','Từ ngày');
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfInvalidFormatDate('tDate','Đến ngày');
		}
		if(msg.length > 0){
			$('#errMsg').html(msg).show();
			return false;
		}
		var dataModel = new Object();
		dataModel.toDate = $('#tDate').val().trim();
		dataModel.listNVBHId = SuperviseCustomer.mapNVBH.keyArray;
		dataModel.listNVGSId = SuperviseCustomer.mapNVGS.keyArray;
		dataModel.listTBHVId = SuperviseCustomer.mapTBHV.keyArray;
		dataModel.shopId = $('#shop').combotree('getValue');
		dataModel.lstProductId = SuperviseCustomer.mapProduct.keyArray;
		if ($('#product').val()=="") dataModel.strProductCode = "";
		else dataModel.strProductCode = $('#product').val().trim();
		dataModel.strTBHVCode = $('#staffTBHV').val().trim();
		dataModel.strNVGSCode =  $('#staffSaleCode').val().trim();
		dataModel.strNVBHCode = $('#staffSaleCode').val().trim();
		dataModel.checkKD1 = 0; 
		var kData = $.param(dataModel, true);
		$.ajax({
			type : "POST",
			url : '/report/crm/kd1/export',
			dataType : "json",
			data : (kData),
			success : function(data) {
				if(data.error == false) {
					dataModel.checkKD1 = 1;
					dataModel.listNVBHId = data.tbhv;
					dataModel.listNVGSId = data.nvgs;
					dataModel.listTBHVId = data.nvbh;
					dataModel.lstProductId = data.product;
					ReportUtils.exportReport('/report/crm/kd1/export',dataModel);
				}else{
					$('#errMsg').html(data.errMsg).show();
				}
			},
			error: function(XMLHttpRequest, textStatus, errorThrown) {
			}
		});	
		ReportUtils.exportReport('/report/crm/kd1/export',dataModel);
	},
	//loctt - Oct17, 2013
	formatQuantity: function(id, convfact){
		$('#productRow_' +id).val(StockValidateInput.formatStockQuantity($('#productRow_' +id).val().trim(), convfact));
	}

};
var StockIssuedGridFmt = {
		rownumFmt:function(value,row,index){
			if(!isNullOrEmpty(row.productId) && !row.footer){
				return (index+1);
			}
			return '';
		},
		productIdFmt:function(value,row,index){
			if(row.productId != null && row.productId != undefined && row.productId != ''){
				var product = StockIssued.lstProductMap.get(row.productId);
				if(product!=null && !row.footer)
					return '<span class="spStyle' +index+'" >' + Utils.XSSEncode(product.productCode) +'</span>';
				else if(row.footer){
					return value;
				}
      		} 
			return '';
		},
		productNameFmt:function(value,row,index){
			var productId = row.productId;
			var product = StockIssued.lstProductMap.get(productId);
			if(product != null && !row.footer) {
				return '<span class="spStyle' +index+'" >' + Utils.XSSEncode(product.productName) +'</span>';
			} else if(row.footer){
				return value;				
			}
			return '';
		},
		availableQuantityFmt:function(value,row,index){
			if(!isNullOrEmpty(row.productId) && !row.footer) {
	    		return '<span class="spStyle' +index+'" >' +formatQuantityEx(row.stockQuantity, row.convfact)+'</span>';
	    	}else if(row.footer){
				return value;
			}else {
	    		return '';
	    	}
		},
		lotFmt:function(value,row,index){
			if(!isNullOrEmpty(value) && row.lot==1 && sys_separation_auto=='0' && !row.footer){
    			return '<a href="javascript:void(0);" class="CHOOSE_LOT" onclick="return StockIssued.chooseLotDialog(' +index+')">Chọn lô</a>';
    		}else if(row.footer){
				return value;
			} else {
    			return '';
    		}
		},
		quantityFmt:function(value,row,index){
			var productId = row.productId;
			var product = StockIssued.lstProductMap.get(productId);
			if(product != null && row.quantityValue>0 && !row.footer) {				
				return '<span class="spStyle' +index+'" >' +formatQuantityEx(row.quantity,product.convfact)+'</span>';
			}else if(row.footer){
				return value;
			}		
			return '';
		},
		priceFmt:function(value,row,index){			
			var productId = row.productId;
			var product = StockIssued.lstProductMap.get(productId);
			if(product != null && product.price != null && product.price != undefined && !row.footer) {
				return '<span class="spStyle' +index+'" >' +formatCurrency(product.price)+'</span>';
			}else if(row.footer){
				return value;
			} else {
				return '';
			}
		},
		amountFmt:function(value,row,index){	
			var productId = row.productId;
			var product = StockIssued.lstProductMap.get(productId);
			if(product==null && !row.footer)
				return '';
			else if(row.footer){
				return value;
			}
			return '<span class="spStyle' +index+'" >' +formatFloatValue(row.amount)+'</span>';
		},
		delFmt:function(value,row,index){
			var productId = row.productId;
			var product = StockIssued.lstProductMap.get(productId);
			if(product==null)
				return '';
			return '<a tabindex="-1" href="javascript:void(0)" class="DelProduct" onclick="StockIssued.delSelectedRow(' +index+');" >'
			+'<img src="/resources/images/icon-delete.png" width="19" height="20" /></a>';
    		
		}		
};







var StockUpdateApprove = {
	_isAllowShowPrice: true,
	_isLoad: true,
	xhrSave: null,
	/**
	 * @author vuongmq
	 * @param objectId
	 * @param index
	 * @param shortCode
	 * @param customerName
	 * @description Gan Ma khach hang vao objectId(customerCode)
	 */
	callSelectF9CustomerCode: function (index, shortCode, customerName) {
			$('#customerCode').val(shortCode.trim());
			$('#common-dialog-search-2-textbox').dialog("close");
	},
	searchOrder: function(){
		$('.ErrorMsgStyle').html('').hide();
		var shopId = $('#shopId').val();
		if(shopId == null || shopId.length == 0){
			$('#errMsgInfo').html('Bạn chưa chọn giá trị cho trường Đơn vị').show();
			return false;
		}
		var shopId = $('#shopId').val();
		var orderType = $('#orderType').val().trim();
		var orderNumber = $('#orderNumber').val().trim();
		var params = new Object();
		params.shopId = shopId;
		params.orderType = orderType;
		params.orderNumber = orderNumber;
		if(StockUpdateApprove._isLoad) {
			StockUpdateApprove.initGrid(params);
		} else {
			$('#grid').datagrid('uncheckAll');	
			$('#grid').datagrid('load',params);				
		}
		StockUpdateApprove._isLoad = false;
		return false;
	},

	/**
	 * Duyet dieu chinh kho
	 * @author vuongmq
	 * @since 04/09/2015 
	 */
	approveStockOrder: function(){
		var rows = $('#grid').datagrid('getChecked');
		var msg ='';
		if (rows.length <= 0) {
			msg = 'Không có đơn hàng nào được chọn.';
			$('#errMsg').html(msg).show();
			return false;
		}
		var params = new Object();
		
		var listTemp = [];
		for (var i = 0; i < rows.length; i++) {
			if (rows[i].id != undefined) {
				var oSource = rows[0].orderSource;
				if (rows[0].orderSource == null) {
					oSource = 1; // trường hop cac don GO/DP/DCT/DGC/DC là trên web
				}
				listTemp.push({ id:rows[i].id, shopId: rows[i].shopId, staffId: rows[i].staffId, orderNumber:rows[i].orderNumber, orderType:rows[i].orderType, orderSource: oSource });
			}
		}
		params.listTemp = listTemp;
		var msgDialog = 'Bạn có muốn xác nhận các đơn hàng này không?';
		JSONUtil.saveData2(params, "/stock/update-approve/approveStockOrder", msgDialog, "errMsg", function(data) {
			if(!data.error) {
				$('#btnSearch').click();
				$('#successMsg').html("Lưu dữ liệu thành công").show();
				setTimeout(function(){
					$('.SuccessMsgStyle').html("").hide();
				}, 1500);
			}
		}, "", function (data) { // callBackFail
			StockUpdateApprove.showDialogStockOrderError(data);
			$('#btnSearch').click();
		});
		return false;
	},

	/**
	 * Duyet dieu chinh kho; thong bao loi popup danh sach don hang
	 * @author vuongmq
	 * @since 04/09/2015 
	 */
	showDialogStockOrderError: function (data){
		var html = $('#dialogStockOrder').html();
		$('#dialogStockOrderEasyUIDialog').dialog({
			title: 'CÁC ĐƠN HÀNG BỊ LỖI, KHÔNG THỂ XÁC NHẬN',
			closed: false,
			cache: false,
			position: 'right',
			modal: true,
			width: 600,
			height: 350,
			onOpen: function(){							
				$('#dgError').datagrid({
			        singleSelect: true,
			        data: data.rows,
			        autoRowHeight: true,       
			        width: 550,
			        height: 230,
			        fitColumns: true,
			        scrollbarSize: 0,
			       /* frozenColumns:[[
			           {field: 'orderNumber',title:'Số đơn hàng', width:200,align:'left',sortable : false,resizable : false}
			        ]],*/
			        columns:[[
			           {field: 'orderNumber', title: 'Số đơn hàng', width: 150, align: 'left', sortable: false, resizable: false, formatter: function(value, row, index) {
			           		return VTUtilJS.XSSEncode(value);
			           }},
			           {field: 'stockQuantity', title: 'Không đủ kho sản phẩm', width: 350, align: 'left', sortable: false, resizable: false, formatter: function(value, row, index) {
			           		return VTUtilJS.XSSEncode(value);
			           }},
			           {field: 'errOrder', title:'Lỗi xác nhận đơn hàng', width: 350, align: 'center', sortable: false, resizable: false, formatter: function(value, row, index) {
			           		return VTUtilJS.XSSEncode(value);
			           }},
			        ]],
			        onLoadSuccess: function() {
			        	$('#dialogStockOrderEasyUIDialog .datagrid-header').css('height','20px');
			        	$('#dialogStockOrderEasyUIDialog .datagrid-header-row').css('height','20px');
			        	$('#dialogStockOrderEasyUIDialog .datagrid-header-row div.datagrid-cell').css('word-wrap','break-word').css('height','20px');
			        }
				});
			},	        
	        onClose: function(){
	        	$('#dialogStockOrderEasyUIDialog').dialog("destroy");
	        	$('#dialogStockOrder').html(html);
	        }
	     });
	},

	/**
	 * Huy don DC, DCT, DCG
	 * @author lacnv1
	 */
	cancelStockTrans: function() {
		$(".ErrorMsgStyle").hide();

		var rows = $('#grid').datagrid('getChecked');
		if (rows.length <= 0) {
			$('#errMsg').html("Không có đơn hàng nào được chọn.").show();
			return false;
		}

		var lstId = [];
		var r = null;
		for (var i = 0, sz = rows.length; i < sz; i++) {
			r = rows[i];
			if (r.orderType != "DC" && r.orderType != "DCT" && r.orderType != "DCG") {
				$("#errMsg").html("Chỉ được hủy đơn DC, DCT, DCG").show();
				return false;
			}
			if (r.id != undefined && r.id != null && Number(r.id)) {
				lstId.push(r.id);
			}
		}

		$.messager.confirm("Xác nhận", "Bạn có muốn hủy (những) đơn " + $("#orderType").val().trim() + " này không?", function(y) {
			if (y) {
				var dataModel = { lstId : lstId };
				Utils.saveData(dataModel, "/stock/update-approve/cancel-stock-trans", StockUpdateApprove._xhrSave, "errMsg", function(data) {
					$('#btnSearch').click();
					if (data.lstErr != undefined && data.lstErr != null && data.lstErr.length > 0) {
						StockUpdateApprove.showDialogCancelStockTransErr(data.lstErr);
						setTimeout(function () {
							$("#successMsg").hide();
						}, 200);
					}
				});
			}
		});
	},
	showDialogCancelStockTransErr: function (lstErr){
		var html = $('#dialogStockOrder').html();
		$('#dialogStockOrderEasyUIDialog').dialog({
			title:'Các đơn hàng bị lỗi, không thể hủy',
			closed: false,
			cache: false,
			modal: true,
			width: 500,
			height: 350,
			onOpen: function(){							
				//var html = new Array();
				$('#dgError').datagrid({
			        singleSelect:true,
			        data: { total: lstErr.length, rows: lstErr },
			        autoRowHeight: true,       
			        width: 450,
			        height: 230,
			        fitColumns: true,
			        scrollbarSize: 0,
			        columns:[[
			           {field: 'orderNumber', title:'Số đơn hàng', width: 220, align: 'left', sortable: false, resizable: false, formatter: function(value, row, index) {
			           		return VTUtilJS.XSSEncode(value);
			           }},
			           {field: 'approve', title: 'Trạng thái không hợp lệ', width: 350, align: 'left', sortable: false, resizable: false, formatter: function(value, row, index) {
			           		return VTUtilJS.XSSEncode(value);
			           }},
			           {field: 'quantityErr', title: 'Tồn kho không hợp lệ', width: 350, align: 'center', sortable: false, resizable: false, formatter: function(value, row, index) {
			           		return VTUtilJS.XSSEncode(value);
			           }},
			        ]],
			        onLoadSuccess :function(){
			        	$('#dialogStockOrderEasyUIDialog .datagrid-header').css('height','20px');
			        	$('#dialogStockOrderEasyUIDialog .datagrid-header-row').css('height','20px');
			        	$('#dialogStockOrderEasyUIDialog .datagrid-header-row div.datagrid-cell').css('word-wrap','break-word').css('height','20px');
			        }
				});
			},	        
	        onClose : function(){
	        	$('#dialogStockOrderEasyUIDialog').dialog("destroy");
	        	$('#dialogStockOrder').html(html);
	        }
	     });
	},
	
	initUnitCbx: function(){
		//load combobox don vi
		$('#shopCodeCB').combotree({
			url: '/stock/update-approve/search-unit-tree',
			lines: true,
			formatter: function(node) {
				return Utils.XSSEncode(node.text);
			},
			onSelect: function(data){
	        	if (data != null && data.id > 0) {
	        		$('#shopId').val(data.id);
	        		StockUpdateApprove.getStaffByShopCode();
	        	}
		    },
		    onLoadSuccess: function(node, data) {
				$('#shopCodeCB').combotree('setValue', data[0].id);
			}
		});
	},
	
	/**
	*lay danh sach nhan vien theo shop don vi
	*/
	getStaffByShopCode: function() {
		 var shopId = $('#shopId').val();
		 var params = new Object();
		if(shopId != null){
			params.shopId = shopId;
		}
		var kData = $.param(params, true);
		 $.ajax({
			type : "POST",
			url : '/stock/update-approve/load-staff-by-shop-code',
			data : (kData),
			dataType : "json",
			success : function(data) {
				StockUpdateApprove._isAllowShowPrice = data.isAllowShowPrice;
				SPCreateOrder._isAllowShowPrice = data.isAllowShowPrice;
				if(StockUpdateApprove._isAllowShowPrice){
					$('#valueOrderDiv').show();
					$('#isValueOrder').val(-1);
					$('#isValueOrder').change();
				} else {
					$('#valueOrderDiv').hide();
				}
				//bo sung text box gia tri DH
				var numberValueOrder = data.numberValue;
				if(numberValueOrder != null && numberValueOrder != '' && numberValueOrder != "") {
					SPAdjustmentTax.numberValueOrder = numberValueOrder;
					SPAdjustmentTax._textChangeBySeach.avg = numberValueOrder.replace(/,/g, '');
				}

				var lstNVBHVo = data.lstNVBHVo;
				var lstNVGHVo = data.lstNVGHVo;

				if(lstNVBHVo != undefined && lstNVBHVo != null) {
					Utils.bindStaffCbx('#saleStaffCode', lstNVBHVo);
				}
				
				if(lstNVGHVo != undefined && lstNVGHVo != null) {
					Utils.bindStaffCbx('#deliveryStaffCode', lstNVGHVo);
				}
			},
			error:function(XMLHttpRequest, textStatus, errorThrown) {
				console.log("Error");
			}
		});
	},
	
	initGrid: function(params) {
		$('#grid').datagrid({  
		    url: '/stock/update-approve/search',
		    rownumbers:true,
		    pagination: true,
		    singleSelect:false,
		    autoRowHeight : true,
		    pageNumber : 1,
		    pageSize: 100,
		    pageList  : [10,50,100,200,500],
		    width: $(window).width() - 40,	    		    
		    queryParams: params,
		    columns: [[
				{field:'id', checkbox:true},
				{field:'link', title:'', width:50, fixed:true, align:'center', sortable:false, resizable:false, formatter : function(v,r,i) {
					var orderT = '';
					var orderType = '';
					if(r.orderType != undefined && r.orderType != null){
						orderType = '&orderType=' + r.orderType;
						orderT = r.orderType;
					}
					var str = '<a href="javascript:void(0);" onclick="SPPrintOrder.showDetailInPopup(' +r.id + ',\'' + orderT + '\',\'' + Utils.XSSEncode(r.shopCode) + '\');" title="Xem chi tiết" id="aEdit"><img height="17" src="/resources/images/icon-view.png"></a>';
					return str;
				}},
				{field:'orderNumber', title:'Số đơn hàng', width:200, align:'left', formatter: function(value, row, index) {
					return VTUtilJS.XSSEncode(row.orderNumber);
				}},
				{field:'orderType', title:'Loại đơn', width:100, align: 'left', formatter : function(value,row,index) {
		        	if(row.orderType != null) {
		        		return VTUtilJS.XSSEncode(row.orderType);
		        	} else {
		        		return '';
		        	}
		        }},
				{field:'orderDate', title:'Ngày tạo', width:100, align:'center', formatter: function(value, row, index) {
					return formatDate(row.orderDate);
				}},
		        {field:'quantity', title:'Sản lượng', width:100, align:'right', formatter : function(value,row,index) {
		        	if(row.quantity != null) {
		        		return formatFloatValue(row.quantity);
		        	} else {
		        		return 0;
		        	}
		        }},
		        {field:'fromWarehouse', title:'Xuất từ', width:350, align:'left',formatter:function(value,row,index){
		        	if(row.fromWarehouse!=null || row.fromShop){
		        		return VTUtilJS.XSSEncode(row.fromWarehouse+' - ' +row.fromShop);
		        	}
		        	return '';
		        }},
		        {field:'toWarehouse', title:'Nhập vào', width:350, align:'left',formatter:function(value,row,index){
		        	if(row.toWarehouse!=null || row.toShop){
		        		return VTUtilJS.XSSEncode(row.toWarehouse+' - ' +row.toShop);
		        	}
		        	return '';
		        }}
		       	        
		    ]],
		    onLoadSuccess :function(data){
		    	$('.datagrid-header-rownumber').html('STT');
			   	 updateRownumWidthForJqGrid('.easyui-dialog');			   	 
		   	 	$(window).resize();
		   	 	$(".datagrid-header-check input[type=checkbox]").removeAttr("checked");
		    }		    
		}); 
	}
}
/*
 * JavaScript file created by Rockstarapps Concatenation
*/

/*
 * START OF FILE - /web/web/resources/scripts/business/displaytool/vnm.displaytool-product.js
 */
/**
 * 
 */
var DisplayToolProduct = {
	lstNewDisplayToolId : null,
	lstNewDisplayToolCode : null,
	lstNewDisplayToolName : null,
	getGridUrl : function(displayToolCode, displayToolName) {
		return "/displaytool-product/search-displaytool?displaytoolCode=" + encodeChar(displayToolCode) + "&displayToolName="+ encodeChar(displayToolName);
	},
	myFormatter : function(value,row,index){
		return "<a href=\"/resources/images/icon_close.png\" onClick=\"\" >";
	}, 
	getGridProductUrl:function(displayToolId){
		return "/displaytool-product/search-product?displayToolId="+ encodeChar(displayToolId);
	},
	searchDisplaytool : function() {
		var displayToolCode = $('#displayToolCode').val().trim();
		var displayToolName = $('#displayToolName').val().trim();
		var object = new Object();
		if(displayToolCode != null && displayToolCode != '' && displayToolCode != -1 && displayToolCode != undefined) {
			object.displayToolCode = displayToolCode;
		}
		if(displayToolName != null && displayToolName != '' && displayToolName != -1 && displayToolName != undefined) {
			object.displayToolName = displayToolName;
		}
		$('#dg').datagrid('load', {page : 1, displayToolCode:displayToolCode, displayToolName:displayToolName});
	},
	openAddDToolDialog: function(){
		CommonSearch.searchTuOnEasyUIDialog(function(data){
			var newDisplayToolId = data.id;
			var newDisplayToolCode = data.code;
			var newDisplayToolName = data.name;
			if(DisplayToolProduct.lstNewDisplayToolId.indexOf(newDisplayToolId) == -1) {
				DisplayToolProduct.lstNewDisplayToolId.push(newDisplayToolId);
				DisplayToolProduct.lstNewDisplayToolCode.push(newDisplayToolCode);
				DisplayToolProduct.lstNewDisplayToolName.push(newDisplayToolName);
			}
			
			$('#dg').datagrid('load', {
				page : 1, 
				lstNewDisplayToolId: Utils.implode(DisplayToolProduct.lstNewDisplayToolId), 
				lstNewDisplayToolCode : Utils.implode(DisplayToolProduct.lstNewDisplayToolCode), 
				lstNewDisplayToolName : Utils.implode(DisplayToolProduct.lstNewDisplayToolName), 
				add : 1});
		});
		return false;
	},
	
	deleteDisplayTool : function(id, index){
		var displayToolCode= $('#displayToolCode').val();
		var msg = 'Bạn có muốn xóa tất cả sản phẩm ra khỏi tủ không?';
		$.messager.confirm('Xác nhận', msg, function(r){
			if(r){
				var idx = DisplayToolProduct.lstNewDisplayToolId.indexOf(id);
				if(idx != -1) {
					DisplayToolProduct.lstNewDisplayToolId.splice(idx, 1);
					DisplayToolProduct.lstNewDisplayToolCode.splice(idx, 1);
					DisplayToolProduct.lstNewDisplayToolName.splice(idx, 1);
					$('#dg').datagrid('load', {page : 1, lstNewDisplayToolId: Utils.implode(DisplayToolProduct.lstNewDisplayToolId), lstNewDisplayToolCode : Utils.implode(DisplayToolProduct.lstNewDisplayToolCode), lstNewDisplayToolName : Utils.implode(DisplayToolProduct.lstNewDisplayToolName), add : 1});
					$('#successMsg').html("Xoá dữ liệu thành công").show();
					var tm = setTimeout(function(){
						$('#successMsg').html('').hide();
						clearTimeout(tm);
					}, 2000);
				} else {
					var data = new Object();
					data.id = id;
					Utils.saveData(data, '/displaytool-product/deleteDisplayTool', null, 'errMsg', function(data){
						$('#dg').datagrid('load', {page : 1, lstNewDisplayToolId: Utils.implode(DisplayToolProduct.lstNewDisplayToolId), lstNewDisplayToolCode : Utils.implode(DisplayToolProduct.lstNewDisplayToolCode), lstNewDisplayToolName : Utils.implode(DisplayToolProduct.lstNewDisplayToolName), add : 1});
					}, 'loading2', '', true, '');
				}
			}			
		});
	},
	deleteProductOfDisplayTool: function(disToolId, productId,gridId){
		var productCode= $('#productCode').val();
		var msg = 'Bạn có muốn xóa sản phẩm này ra khỏi tủ không ?';
		$.messager.confirm('Xác nhận', msg, function(r){
			if(r){
				var params = new Object();
				params.id =  disToolId;
				params.productId = productId;
				Utils.saveData(params, '/displaytool-product/deleteProductOfDisplayTool', null, 'errMsg', function(data){
					$('#'+gridId).datagrid('load', {page : 1});
				}, 'loading2', '', true, '');
			}
		});
	},
	addProduct4Tool:function(toolCode, gridId) {
		var arrParam = new Array();
		var object = new Object();
		object.name = 'toolCode';
		object.value = toolCode;
		arrParam.push(object);		
		CommonSearch.searchTuOnEasyUIDialogCheckbox(function(map) {	
			var idx = DisplayToolProduct.lstNewDisplayToolCode.indexOf(toolCode);
			if(idx != -1) {
				DisplayToolProduct.lstNewDisplayToolId.splice(idx, 1);
				DisplayToolProduct.lstNewDisplayToolCode.splice(idx, 1);
				DisplayToolProduct.lstNewDisplayToolName.splice(idx, 1);
				var model = new Object();
			    model.displayToolCode = toolCode;
			    model.lstProductId = new Array();
			    for(var i=0;i<map.length;i++){
			    	model.lstProductId[i] = map[i].id;
			    }
			    Utils.addOrSaveData(model, '/displaytool-product/createProductOfDisplayTool', null, null, function(data) {
			    	$('#'+gridId).datagrid('load', {page : 1});			    	
			    }, null, null, null, null, null);
			}else{
				var model = new Object();
			    model.displayToolCode = toolCode;
			    model.lstProductId = new Array();
			    for(var i=0;i<map.length;i++){
			    	model.lstProductId[i] = map[i].id;
			    }
			    Utils.addOrSaveData(model, '/displaytool-product/createProductOfDisplayTool', null, null, function(data) {
			    	$('#'+gridId).datagrid('load', {page : 1});			    	
			    }, null, null, null, null, null);
			}
		}, arrParam);
		return false;
	}
};

/*
 * END OF FILE - /web/web/resources/scripts/business/displaytool/vnm.displaytool-product.js
 */

/*
 * START OF FILE - /web/web/resources/scripts/business/displaytool/vnm.displaytool-customer.js
 */
/**
 * 
 */
var DisplayToolCustomer = {
	mapDisplayTool:null,
	searchGridUrl:function(shopCode,staffCode,dateStr){
		var url = '/displaytool-customer/search';
		return url;
	},
	search:function(seq){
		$('#errMsg').html('').hide();
		var staffCode = $('#staffCode').val().trim();
		var shopId = $('#shop').combotree('getValue');
		var dateStr =  $('#month').val().trim();
		if(dateStr == "__/____" || dateStr.length <=0) { // kiem tra rỗng
			$('#errMsg').html('Bạn chưa nhập giá trị cho trường Tháng').show();
			return false;
		}
		if(dateStr.length>0 && !Utils.isDate('01/' + dateStr,'/')){
			$('#errMsg').html('Tháng không đúng định dạng mm/yyyy').show();
			return false;
		}
		$('#dv').val(shopId);
		$('#nv').val(Utils.XSSEncode(staffCode));
		$('#th').val(dateStr);
		DisplayToolCustomer.mapDisplayTool = new Map();
		if(seq!=undefined && seq!=null){
			$('#grid').datagrid('load',{page : 1, staffCode : staffCode, shopId: shopId,dateStr:dateStr});
		}else{			
			$('#grid').datagrid('load',{page : 1, staffCode : staffCode, shopId: shopId,dateStr:dateStr});
		}		
	},
	copyDisplayTool:function(){
		var msg = '';
		//msg = Utils.getMessageOfRequireCheck('staffCode','Nhân viên');
		if(msg.length==0){
			msg = Utils.getMessageOfRequireCheck('month','Tháng');
		}		
		if(msg.length>0){
			$('#errMsg').html(msg).show();
			return false;
		}
		var dataModel = new Object();
		dataModel.shopId = $('#shop').combotree('getValue');
		dataModel.staffCode = $('#staffCode').val().trim();		
		Utils.addOrSaveData(dataModel, '/displaytool-customer/copy-displaytools', null, 'errMsg', function(data){
			if(data.successMsg != undefined && data.successMsg != null) {
				$('#successMsg').html(data.successMsg).show();
				DisplayToolCustomer.search(true);
				setTimeout(function(){
					$('#successMsg').html('').hide();
				},5000);
				// vi van co 1 so NV da ton tai cua thang hien tai nen khong copy, bao errMsg
				$('#errMsg').html(data.errMsg).show();
			}
		},null,null,null,'Bạn có muốn sao chép dữ liệu cho tháng hiện tại không?',function(data){
			$('#errMsg').html(data.errMsg).show();
		}, true);
	},
	showStaffByShop:function(shopId){
		$.ajax({
			type : "POST",
			url : "/displaytool-customer/search-staff",
			data : ({shopId:shopId}),
			dataType: "json",
			success : function(data) {
				var html = new Array();	
    			if(data!= null && data!= undefined && data.lstStaff!= null && data.lstStaff.length > 0){
    				html.push("<option value=''>Tất cả</option>");
    				for(var i=0;i<data.lstStaff.length;i++){    					
    					html.push("<option value='"+ Utils.XSSEncode(data.lstStaff[i].staffCode) +"'>"+ Utils.XSSEncode(data.lstStaff[i].staffCode) + " - " + Utils.XSSEncode(data.lstStaff[i].staffName) +"</option>");
    				}
    			}else{
    				html.push("<option value=''>Tất cả</option>");
    			}	        			        	
    			$('#staffCode').html(html.join(""));
    			$('#staffCode').change();
			},
			error:function(XMLHttpRequest, textStatus, errorThrown) {				
			}
		});
	},
	deleteDisplayTool:function(){
		$('#errMsg').html('').hide();
		var displayToolsId = DisplayToolCustomer.mapDisplayTool.keyArray;
		if(displayToolsId.length==0 && !$('#selectAllShop').is(':checked')){
			$('#errMsg').html('Vui lòng chọn dòng cần xóa').show();
			return false;
		}		
		var dataModel = new Object();
		dataModel.displayToolsId = displayToolsId;
		if($('#selectAllShop').is(':checked')){
			dataModel.typeCheck = 1;
		}else{
			dataModel.typeCheck = 0;
		}
		var staffCode = $('#staffCode').val().trim();
		var shopId = $('#shop').combotree('getValue');
		var dateStr =  $('#month').val().trim();
		dataModel.staffCode = staffCode;
		dataModel.shopId=shopId;
		dataModel.dateStr=dateStr;
		$.messager.confirm('Xác nhận','Bạn có muốn xoá không?',function(r){
			if(r){
				Utils.addOrSaveData(dataModel, '/displaytool-customer/del-displaytools', null, 'errMsg', function(){
					DisplayToolCustomer.search(true);
					$('#successMsg').html("Xoá dữ liệu thành công").show();
					var tm = setTimeout(function(){
						$('#successMsg').html('').hide();
					},5000);					
				}, null,null, true);
			}
		});				
	},
	importExcel : function() {
		DisplayToolCustomer.mapDisplayTool = new Map();
		$('#easyuiPopup').show();
		$('#errExcelMsg').html('').hide();
		$('#resultExcelMsg').html('').hide();
		$('#dateStrExcel').val(getCurrentMonth());
		$('#fakefilepc').val('');
		$('#easyuiPopup').dialog({  
	        closed: false,  
	        cache: false,  
	        modal: true,
	        onOpen: function(){
	        	var tabindex = -1;
				$('.InputTextStyle, select, input, button , li , tr, td, th, label, ul, a, img').each(function () {
					if (this.type != 'hidden') {
						$(this).attr("tabindex", tabindex);
						tabindex -=1;
					}
				});
				tabindex = 1;
	    		 $('.easyui-dialog input,.easyui-dialog select,.easyui-dialog button').each(function () {
		    		 if (this.type != 'hidden') {
			    	     $(this).attr("tabindex", '');
						 tabindex++;
		    		 }
				 });
	        }
		});

	},
	upload : function() {
		$('#inmonth').val($('date').val);
		$('#resultExcelMsg').html('').hide();
		if ($('#dateStrExcel').val() ==  "") {
			$('#errExcelMsg').html("Vui lòng chọn tháng cần nhập file").show();
			$('#dateStrExcel').focus();
			return false;
		} else {
			if( !Utils.compareCurrentMonthEx( $('#dateStrExcel').val()) ) {
				$('#errExcelMsg').html("Vui lòng chọn tháng lớn hơn hoặc bằng tháng hiện tại !").show();
				$('#dateStrExcel').focus();
				return false;
			}
		}
		if($('#excelFile').val() ==  ""){
			$('#errExcelMsg').html("Vui lòng chọn file Excel").show();
			return false;
		}
		$('#easyuiPopup #importFrm').ajaxForm(options);
		$.messager.confirm('Xác nhận','Bạn có muốn nhập từ file?',function(r){
			if(r){				
				$('#easyuiPopup #importFrm').submit();
			}
		});		
		return false;
	},
	exportExcel : function() {		
		DisplayToolCustomer.mapDisplayTool = new Map();
		$.messager.confirm('Xác nhận','Bạn có muốn xuất file?',function(r){
			if(r){					
					var dataModel = new Object();
					dataModel.shopId = $('#dv').val();
					dataModel.staffCode = $('#nv').val();	
					dataModel.dateStr = $('#th').val();
					ReportUtils.exportExcel('/displaytool-customer/exportExcel', dataModel, 'errMsg');					
			}
		});		
		return false;
	}
};
/*
 * END OF FILE - /web/web/resources/scripts/business/displaytool/vnm.displaytool-customer.js
 */

/*
 * JavaScript file created by Rockstarapps Concatenation
*/

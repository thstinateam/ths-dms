/**
 * 
 */
var DisplayToolProduct = {
	lstNewDisplayToolId : null,
	lstNewDisplayToolCode : null,
	lstNewDisplayToolName : null,
	getGridUrl : function(displayToolCode, displayToolName) {
		return "/displaytool-product/search-displaytool?displaytoolCode=" + encodeChar(displayToolCode) + "&displayToolName="+ encodeChar(displayToolName);
	},
	myFormatter : function(value,row,index){
		return "<a href=\"/resources/images/icon_close.png\" onClick=\"\" >";
	}, 
	getGridProductUrl:function(displayToolId){
		return "/displaytool-product/search-product?displayToolId="+ encodeChar(displayToolId);
	},
	searchDisplaytool : function() {
		var displayToolCode = $('#displayToolCode').val().trim();
		var displayToolName = $('#displayToolName').val().trim();
		var object = new Object();
		if(displayToolCode != null && displayToolCode != '' && displayToolCode != -1 && displayToolCode != undefined) {
			object.displayToolCode = displayToolCode;
		}
		if(displayToolName != null && displayToolName != '' && displayToolName != -1 && displayToolName != undefined) {
			object.displayToolName = displayToolName;
		}
		$('#dg').datagrid('load', {page : 1, displayToolCode:displayToolCode, displayToolName:displayToolName});
	},
	openAddDToolDialog: function(){
		CommonSearch.searchTuOnEasyUIDialog(function(data){
			var newDisplayToolId = data.id;
			var newDisplayToolCode = data.code;
			var newDisplayToolName = data.name;
			if(DisplayToolProduct.lstNewDisplayToolId.indexOf(newDisplayToolId) == -1) {
				DisplayToolProduct.lstNewDisplayToolId.push(newDisplayToolId);
				DisplayToolProduct.lstNewDisplayToolCode.push(newDisplayToolCode);
				DisplayToolProduct.lstNewDisplayToolName.push(newDisplayToolName);
			}
			
			$('#dg').datagrid('load', {
				page : 1, 
				lstNewDisplayToolId: Utils.implode(DisplayToolProduct.lstNewDisplayToolId), 
				lstNewDisplayToolCode : Utils.implode(DisplayToolProduct.lstNewDisplayToolCode), 
				lstNewDisplayToolName : Utils.implode(DisplayToolProduct.lstNewDisplayToolName), 
				add : 1});
		});
		return false;
	},
	
	deleteDisplayTool : function(id, index){
		var displayToolCode= $('#displayToolCode').val();
		var msg = 'Bạn có muốn xóa tất cả sản phẩm ra khỏi tủ không?';
		$.messager.confirm('Xác nhận', msg, function(r){
			if(r){
				var idx = DisplayToolProduct.lstNewDisplayToolId.indexOf(id);
				if(idx != -1) {
					DisplayToolProduct.lstNewDisplayToolId.splice(idx, 1);
					DisplayToolProduct.lstNewDisplayToolCode.splice(idx, 1);
					DisplayToolProduct.lstNewDisplayToolName.splice(idx, 1);
					$('#dg').datagrid('load', {page : 1, lstNewDisplayToolId: Utils.implode(DisplayToolProduct.lstNewDisplayToolId), lstNewDisplayToolCode : Utils.implode(DisplayToolProduct.lstNewDisplayToolCode), lstNewDisplayToolName : Utils.implode(DisplayToolProduct.lstNewDisplayToolName), add : 1});
					$('#successMsg').html("Xoá dữ liệu thành công").show();
					var tm = setTimeout(function(){
						$('#successMsg').html('').hide();
						clearTimeout(tm);
					}, 2000);
				} else {
					var data = new Object();
					data.id = id;
					Utils.saveData(data, '/displaytool-product/deleteDisplayTool', null, 'errMsg', function(data){
						$('#dg').datagrid('load', {page : 1, lstNewDisplayToolId: Utils.implode(DisplayToolProduct.lstNewDisplayToolId), lstNewDisplayToolCode : Utils.implode(DisplayToolProduct.lstNewDisplayToolCode), lstNewDisplayToolName : Utils.implode(DisplayToolProduct.lstNewDisplayToolName), add : 1});
					}, 'loading2', '', true, '');
				}
			}			
		});
	},
	deleteProductOfDisplayTool: function(disToolId, productId,gridId){
		var productCode= $('#productCode').val();
		var msg = 'Bạn có muốn xóa sản phẩm này ra khỏi tủ không ?';
		$.messager.confirm('Xác nhận', msg, function(r){
			if(r){
				var params = new Object();
				params.id =  disToolId;
				params.productId = productId;
				Utils.saveData(params, '/displaytool-product/deleteProductOfDisplayTool', null, 'errMsg', function(data){
					$('#'+gridId).datagrid('load', {page : 1});
				}, 'loading2', '', true, '');
			}
		});
	},
	addProduct4Tool:function(toolCode, gridId) {
		var arrParam = new Array();
		var object = new Object();
		object.name = 'toolCode';
		object.value = toolCode;
		arrParam.push(object);		
		CommonSearch.searchTuOnEasyUIDialogCheckbox(function(map) {	
			var idx = DisplayToolProduct.lstNewDisplayToolCode.indexOf(toolCode);
			if(idx != -1) {
				DisplayToolProduct.lstNewDisplayToolId.splice(idx, 1);
				DisplayToolProduct.lstNewDisplayToolCode.splice(idx, 1);
				DisplayToolProduct.lstNewDisplayToolName.splice(idx, 1);
				var model = new Object();
			    model.displayToolCode = toolCode;
			    model.lstProductId = new Array();
			    for(var i=0;i<map.length;i++){
			    	model.lstProductId[i] = map[i].id;
			    }
			    Utils.addOrSaveData(model, '/displaytool-product/createProductOfDisplayTool', null, null, function(data) {
			    	$('#'+gridId).datagrid('load', {page : 1});			    	
			    }, null, null, null, null, null);
			}else{
				var model = new Object();
			    model.displayToolCode = toolCode;
			    model.lstProductId = new Array();
			    for(var i=0;i<map.length;i++){
			    	model.lstProductId[i] = map[i].id;
			    }
			    Utils.addOrSaveData(model, '/displaytool-product/createProductOfDisplayTool', null, null, function(data) {
			    	$('#'+gridId).datagrid('load', {page : 1});			    	
			    }, null, null, null, null, null);
			}
		}, arrParam);
		return false;
	}
};

var AsoTarget = {
	_MAX_CYCLE_NUM: 13,
	_lstStaffInfo: [],
	_TYPE_ASO_SKU: 1, 
	_TYPE_ASO_SUB_CAT: 2,
	_paramSearch: null,

	/**
	 * Lay params khi nhan buttom search
	 * @author vuongmq
	 * @since 20/10/2015 
	 */
	getSearchParams: function() {
		var params = {};
		params.shopCode = $('#cbxShop').combobox('getValue');
		params.yearPeriod = $('#yearPeriod').val().trim();
		params.numPeriod = $('#numPeriod').combobox('getValue');
		params.staffCode = $('#staffCode').val();
		params.routingId = $('#routingId').val();
		params.type = $('#typeAso').val();
		AsoTarget._paramSearch = params;
		return params;
	},

	/**
	 * Search danh sach phan bo aso theo loai
	 * @author vuongmq
	 * @since 20/10/2015 
	 */
	search: function() {
		hideAllMessage();
		AsoTarget._lstStaffInfo = [];
		var par = AsoTarget.getSearchParams();
		Utils.getHtmlDataByAjax(par, '/aso/search', function(data){
			$('#gridContainer').html(data);
		});
	},

	/**
	 * Bind input chi tieu cua Aso
	 * @author vuongmq
	 * @since 20/10/2015 
	 */
	bindFormatInput: function(){
		$('#gridContainer input.aso').each(function(index, item){
			var id = $(this).attr("id");
			Utils.bindFormatOntextfieldCurrencyFor(id, Utils._TF_NUMBER_DOT);
			$(this).bind('blur', function(){
				var value = $('#' + id).val();
				if (!Utils.isEmpty(value) && !isNaN(value)) {
					if (Number(value) < 0) {
						$('#errExcelMsg').html('Vui lòng nhập giá trị lớn hơn 0');
					} else {
						hideAllMessage();
					}
				}
			});
		});
	},

	/**
	 * Confirm khi nhan phan bo aso
	 * @author vuongmq
	 * @since 20/10/2015 
	 */
	confirmSaveAso: function(){
		hideAllMessage();
		var errMsg = '';
		var nvbh = $('#staffCodeUnder').val();
		if (Utils.isEmpty(nvbh) || nvbh == activeType.DELETE) {
			errMsg = 'Không có nhân viên bán hàng';
		}
		if (errMsg.length == 0) {
			if (Utils.isEmpty($('#routingIdUnder').val())) {
				errMsg = 'Không có tuyến của nhân viên bán hàng';
			}
		}
		if (errMsg.length > 0) {
			$('#errExcelMsg').html(errMsg).show();
			return false;
		}
		var type = $('#curTypeAso').val();
		var mesDiaLog = '';
		if (type == AsoTarget._TYPE_ASO_SKU) {
			mesDiaLog = 'Bạn có muốn phân bổ aso theo SKU không?';
		} else {
			mesDiaLog = 'Bạn có muốn phân bổ aso theo ngành hàng con không?';
		}
		$.messager.confirm('Xác nhận', mesDiaLog, function(r) {  
			if (r) {
				var params = new Object();
				params.shopCode = $('#curShopCodeAso').val();
				params.yearPeriod = $('#yearPeriodUnder').val().trim();
				params.numPeriod = $('#numPeriodUnder').combobox('getValue');
				params.staffCode = $('#staffCodeUnder').val();
				params.type = type;
				Utils.getJSONDataByAjaxNotOverlay(params, '/aso/checkAsoPlan', function(data) {
					if (data != undefined && data != null && data.flag != null) {
						if (data.flag == activeType.WAITING) {
							$.messager.confirm('Xác nhận', data.errMsg, function(r) {  
								if (r) {
									AsoTarget.setAso();
								}
							});
						} else if (data.flag == activeType.STOPPED) {
							$.messager.alert('Thông báo', data.errMsg, 'info');
						} else {
							AsoTarget.setAso();
						}
					}
				});
			}
		});
	},

	/**
	 * Phan bo aso
	 * @author vuongmq
	 * @since 20/10/2015 
	 */
	setAso: function() {
		hideAllMessage();
		var date = new Date();
		var curYear = $('#curYearPeriod').val().trim();
		var curPeriod = $('#curNumPeriod').val().trim();
		var year = $('#yearPeriodUnder').val().trim();
		var period = $('#numPeriodUnder').combobox('getValue').trim();
		var errMsg = '';
		if (parseInt(period) < 0 || parseInt(period) > AsoTarget._MAX_CYCLE_NUM){
			errMsg = 'Chu kỳ không hợp lệ. Vui lòng chọn lại';
		}
		if (errMsg == '' && !Utils.isEmpty(curYear) && !isNaN(curYear) && parseInt(year) < parseInt(curYear)) {
			errMsg = 'Năm phải lớn hơn hay bằng năm hiện tại. Vui lòng chọn lại';
		}
		if (errMsg == '' && !Utils.isEmpty(curYear) && !isNaN(curYear) && parseInt(year) == parseInt(curYear) && parseInt(period) < parseInt(curPeriod)) {
			errMsg = 'Chu kỳ phải lớn hơn hay bằng chu kỳ hiện tại. Vui lòng chọn lại';
		}
		var type = $('#curTypeAso').val();
		var errType = '';
		if (type == AsoTarget._TYPE_ASO_SKU) {
			errType = 'SKU';
		} else {
			errType = 'Ngành hàng con';
		}
		if (AsoTarget._lstStaffInfo == null || AsoTarget._lstStaffInfo.length == 0) {
			errMsg = 'Danh sách ' + errType + ' rỗng.'
		}
		var lstData = [];
		var i = 1;
		$('#gridContainer input.aso').each(function(index, item){
			var id = $(this).attr("id");
			obj = {};
			obj.key = id;
			var valueInput = Utils.returnMoneyValue($(this).val());
			if (isNaN(valueInput)) {
				errMsg = 'Dòng thứ ' + i + ' giá trị chỉ tiêu không phải là số';
				$('#errExcelMsg').html(errMsg).show();
				return false;
			}
			obj.value = valueInput;
			lstData.push(obj);
			i++;
		});
		if (errMsg.length > 0) {
			$('#errExcelMsg').html(errMsg).show();
			return false;
		}
		
		var dataModel = {};
		convertToSimpleObject(dataModel, lstData, 'lstAsoPlanObject');
		dataModel.shopCode = $('#curShopCodeAso').val();
		dataModel.yearPeriod = $('#yearPeriodUnder').val().trim();
		dataModel.numPeriod = $('#numPeriodUnder').combobox('getValue');
		dataModel.staffCode = $('#staffCodeUnder').val();
		dataModel.routingId = $('#routingIdUnder').val();
		dataModel.type = type;
		Utils.saveData(dataModel, '/aso/save-info', null, 'errExcelMsg');
	},

	/**
	 * reset phan bo loai Aso
	 * @author vuongmq
	 * @since 22/10/2015
	 */
	resetAso: function(){
		hideAllMessage();
		var errMsg = '';
		var nvbh = $('#staffCodeUnder').val();
		if (Utils.isEmpty(nvbh) || nvbh == activeType.DELETE) {
			errMsg = 'Không có nhân viên bán hàng';
		}
		if (errMsg.length == 0) {
			if (Utils.isEmpty($('#routingIdUnder').val())) {
				errMsg = 'Không có tuyến của nhân viên bán hàng';
			}
		}
		var type = $('#curTypeAso').val();
		var errType = '';
		if (type == AsoTarget._TYPE_ASO_SKU) {
			errType = 'SKU';
		} else {
			errType = 'Ngành hàng con';
		}
		if (errMsg.length == 0) {
			if (AsoTarget._lstStaffInfo == null || AsoTarget._lstStaffInfo.length == 0) {
				errMsg = 'Danh sách ' + errType + ' rỗng.'
			}
		}
		if (errMsg.length > 0) {
			$('#errExcelMsg').html(errMsg).show();
			return false;
		}
		var message = 'Bạn có muốn reset phân bổ loại ' + errType + ' về 0?';
		var dataModel = {};
		dataModel.shopCode = $('#curShopCodeAso').val();
		dataModel.yearPeriod = $('#yearPeriodUnder').val().trim();
		dataModel.numPeriod = $('#numPeriodUnder').combobox('getValue');
		dataModel.staffCode = $('#staffCodeUnder').val();
		dataModel.routingId = $('#routingIdUnder').val();
		dataModel.type = type;
		Utils.addOrSaveData(dataModel, '/aso/reset-aso', null, 'errExcelMsg', function(data){
			var tm = setTimeout(function(){
				//Load lai danh sach ASO
				$('#successMsg').html("").hide();
				clearTimeout(tm);
				AsoTarget.search();
			 }, 1500);
		}, null, null, null, message);
	},

	/**
	 * imporrt confirm loai Aso
	 * @author vuongmq
	 * @since 22/10/2015
	 */
	openImportConfirm: function(){
		hideAllMessage();
		var excelURI = $('#excelFile').val();
 		if (excelURI == undefined || excelURI == null || excelURI.trim().length == 0) {
 			$('#errExcelMsg').html('Vui lòng chọn tập tin Excel').show();
 			return false;
 		}
		$('#popupImportAso').dialog({
			title: 'Chọn loại import',
			width: 350,
			height:'auto',
			onOpen: function(){
				$('#rbTypeSku').prop("checked", true);
			}
		});
		$('#popupImportAso').dialog('open');
	},

	/**
	 * import excel ASO
	 * @author vuongmq
	 * @since 20/10/2015 
	 */
 	importExcel: function() {
 		var typeRadio = $('input:radio[name=importRadio]:checked').val();
 		if (typeRadio == null || typeRadio == '') {
 			$('#errExcelMsg').html('Vui lòng chọn loại phân bổ aso').show();
 			return false;
 		}
 		$('#typeId').val(typeRadio);
 		$('#popupImportAso').dialog('close');
 		var params = {};
 		hideAllMessage();
		Utils.importExcelUtils(function(data) {
			$('#errExcelMsg').val("").change();
			$('#fakefilepc').val("").change();
			if (data.message != undefined && data.message != null && data.message.trim().length > 0) {
				$('#errExcelMsg').html(data.message.trim()).change().show();
			} else {
				$('#successMsg').html(jsp_common_save_success).show();
				var tm = setTimeout(function(){
					//Load lai danh sach ASO
					$('#successMsg').html("").hide();
					clearTimeout(tm);
					AsoTarget.search();
				 }, 1500);
			}
		}, 'importFrm', 'excelFile', 'errExcelMsg', true);
 	},

	/**
	 * Export excel ASO
	 * @author vuongmq
	 * @since 20/10/2015 
	 */
 	exportExcel: function() {
 		hideAllMessage();
		var errMsg = '';
		var nvbh = $('#staffCodeUnder').val();
		if (Utils.isEmpty(nvbh) || nvbh == activeType.DELETE) {
			errMsg = 'Không có nhân viên bán hàng';
		}
		if (errMsg.length == 0) {
			if (Utils.isEmpty($('#routingIdUnder').val())) {
				errMsg = 'Không có tuyến của nhân viên bán hàng';
			}
		}
		if (errMsg.length > 0) {
			$('#errExcelMsg').html(errMsg).show();
			return false;
		}
 		var params = AsoTarget._paramSearch;
 		ReportUtils.exportReport('/aso/exportExcel', params, 'errExcelMsg');
 	},

 	/**
	 * Lay danh sach cycle theo nam header
	 * @author vuongmq
	 * @since 20/10/2015 
	 */
 	initCycleInfo: function() {
 		var par = {};
 		par.year = $('#yearPeriod').val();
 		Utils.getJSONDataByAjax(par, '/commons/list-cycle-filter', function(data){
 			if (data != null && data.lstCycle != null) {
 				Utils.bindPeriodCbx('#numPeriod', data.lstCycle, data.currentNum, 113, function(rec) {
 					if (rec != undefined && rec != null && rec.num != undefined && rec.num != null) {
 						AsoTarget.changeNumPeriodAso(rec.num);
 					}
 				});
 				Utils.bindPeriodCbx('#numPeriodUnder', jQuery.extend(true, [], data.lstCycle), data.currentNum, 113);
 			}
 		});
 	},

	/**
	 * khi change yearPeriodUnder Lay chu ky cua combobox Under(numPeriodUnder) 
	 * @author vuongmq
	 * @since 20/10/2015 
	 */
 	initCycleInfoUnder: function() {
 		var par = {};
 		par.year = $('#yearPeriodUnder').val();
 		Utils.getJSONDataByAjax(par, '/commons/list-cycle-filter', function(data){
 			if (data != null && data.lstCycle != null) {
 				Utils.bindPeriodCbx('#numPeriodUnder', data.lstCycle, data.currentNum, 113);
 			}
 		});
 	},

 	/**
	 * set year
	 * @author vuongmq
	 * @since 20/10/2015 
	 */
 	setCurrentYear: function(year) {
 		$('#yearPeriod').val(year);
 		$('#yearPeriod').change();
 		$('#yearPeriodUnder').val(year);
 		$('#yearPeriodUnder').change();
 	},
 	
 	 /**
	 * Xu ly khi thay doi don vi; lay danh sach 
	 * @author vuongmq
	 * @since 19/10/2015
	 */
	changeShop: function(shopId) {
		if (shopId != undefined && shopId != null) {
			$.getJSON('/aso/get-list-staff?shopId=' + shopId, function(list) {
				$('#staffCode').html('');
				if (list != undefined && list != null && list.length > 0) {
					if (list.length > 1) { 
						$('#staffCode').append('<option value = "-1" selected = "selected" >' + Utils.XSSEncode(sale_plan_choose_staff) + '</option>');
					}
					for (var i = 0; i < list.length; i++) {
						$('#staffCode').append('<option value = "' + Utils.XSSEncode(list[i].staffCode) + '">' + Utils.XSSEncode(list[i].staffCode + " - " + list[i].staffName) + '</option>');  
					}
				} else {
					$('#staffCode').append('<option value = "-1">' + Utils.XSSEncode(sale_plan_no_staff) + '</option>');
				}
				$('#staffCode').change();
				$('#staffCodeUnder').html('');
				if (list != undefined && list != null && list.length > 0) {
					if (list.length > 1) { 
						$('#staffCodeUnder').append('<option value = "-1" selected = "selected" >' + Utils.XSSEncode(sale_plan_choose_staff) + '</option>');
					}
					for (var i = 0; i < list.length; i++) {
						$('#staffCodeUnder').append('<option value = "' + Utils.XSSEncode(list[i].staffCode) + '">' + Utils.XSSEncode(list[i].staffName) + '</option>');  
					}
				} else {
					$('#staffCodeUnder').append('<option value = "-1">' + Utils.XSSEncode(sale_plan_no_staff) + '</option>');
				}
				$('#staffCodeUnder').change();
			});
		}
	},
	
	/**
	 * Xu ly khi thay doi nhan vien 
	 * @author vuongmq
	 * @since 19/10/2015
	 */
	changeStaff: function(typeView){
		if (typeView == activeType.STOPPED) {
			var staffCode = $('#staffCode').val();
			$('#staffCodeUnder').val(staffCode);
			$('#staffCodeUnder').change();
			// load tuyen cua NVBH
			$('#routingStaff').html('');
			$('#routingId').val('');
			$.getJSON('/aso/get-routing-staff?staffCode=' + staffCode, function(data) {
				if (data != undefined && data != null && data.routingId > 0) {
					$('#routingStaff').html(Utils.XSSEncode(data.routingStaff));
					$('#routingId').val(data.routingId);
				}
			});
		} else {
			var staffCode = $('#staffCodeUnder').val();
			// load tuyen cua NVBH Under
			$('#routingStaffUnder').html('');
			$('#routingIdUnder').val('');
			$.getJSON('/aso/get-routing-staff?staffCode=' + staffCode, function(data) {
				if (data != undefined && data != null && data.routingId > 0) {
					$('#routingStaffUnder').html(Utils.XSSEncode(data.routingStaff));
					$('#routingIdUnder').val(data.routingId);
				}
			});
		}
	},

	/**
	 * Xu ly khi thay doi numPeriod (numPeriodUnder thay doi theo)
	 * @author vuongmq
	 * @since 26/10/2015
	 */
	changeNumPeriodAso: function(numPeriod){
		if (numPeriod == undefined || numPeriod == null) {
			numPeriod = $('#numPeriod').combobox('getValue');
		}
		$('#numPeriodUnder').combobox('setValue', numPeriod);
	},
}
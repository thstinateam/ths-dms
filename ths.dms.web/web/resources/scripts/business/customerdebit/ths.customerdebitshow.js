/**
 * 
 */
var ShowDebit = {
	_shortCode:'',
	_fromDateS:'',
	_toDateS:'',
	_status:-1,
	_staffDeliverCode: '',
	_staffPaymentCode: '',
	_staffCode: '',
	_isLoad: true,
	totalAmount:0,
	totalAmountPay:0,
	totalAmountRemain:0,
	amountRemain:0,
	amountRemainMap:null,
	statusPay:'', /** vuongmq; 05/06/2015; tranh khong bam nut Xem ma bam export se loi he thong*/
	lstOrderNumbers:'',
	getList: function() {
		$('.ErrorMsgStyle').html('').hide();
		var msg = Utils.getMessageOfRequireCheck('shop', jsp_common_shop_name_lable, true);
		if (msg.length == 0 && $('#shortCode').val().trim() > 0 ) {
			msg = Utils.getMessageOfSpecialCharactersValidate('shortCode','Mã KH', Utils._CODE);
		}				
		if (msg.length > 0) {
			$('#errMsg').html(msg).show();
			return false;
		}	
		var data = new Object();
		data.shopCode = $('#shop').combobox('getValue');
		data.shortCode = $('#shortCode').val().trim();
		data.fromDateS = $('#fromDate').val().trim();
		data.toDateS = $('#toDate').val().trim();
		data.status =  $('#status').val();
		data.staffDeliverCode = $('#transferStaff').combobox('getValue').trim();
		data.staffPaymentCode = $('#crashierStaff').combobox('getValue').trim();
		data.staffCode = $('#staffCode').combobox('getValue').trim();
		if ($('#cboStatusPay').val() != null && $('#cboStatusPay').val() != undefined) {
			data.statusPay = $('#cboStatusPay').val().join(",");
		} else {
			data.statusPay = "";
		}
		/** vuongmq; 05/06/2015; xem danh sach don hang nhieu*/
		data.lstOrderNumbers = $("#orderNumber").val().trim();

		ShowDebit._shortCode = data.shortCode;
		ShowDebit._fromDateS = data.fromDateS;
		ShowDebit._toDateS = data.toDateS;
		ShowDebit._status = data.status;
		ShowDebit._staffDeliverCode = data.staffDeliverCode;
		ShowDebit._staffPaymentCode = data.staffPaymentCode;
		ShowDebit._staffCode = data.staffCode;
		ShowDebit.totalAmount = 0;
		ShowDebit.totalAmountPay = 0;
		ShowDebit.totalAmountRemain = 0;
		ShowDebit.amountRemain = 0;
		ShowDebit.amountRemainMap = null;
		ShowDebit.statusPay = data.statusPay;
		ShowDebit.lstOrderNumbers = data.lstOrderNumbers;
		$('#dg').datagrid('load', data);
	},
	exportExcel: function() {
		$('.ErrorMsgStyle').html('').hide();
		if ($('#dg').datagrid ('getRows').length == 0) {
			msg = 'Không có dữ liệu để xuất!';
			$('#errMsg2').html(msg).show();	
			return false;
		}
		var params = new Object();
		params.shopCode = $('#shopCode').val();
		params.shortCode = ShowDebit._shortCode;
		params.fromDateS = ShowDebit._fromDateS;
		params.toDateS = ShowDebit._toDateS;	
		params.status = ShowDebit._status;
		params.staffDeliverCode = ShowDebit._staffDeliverCode;
		params.staffPaymentCode = ShowDebit._staffPaymentCode;
		params.staffCode = ShowDebit._staffCode;
		params.statusPay = ShowDebit.statusPay;
		params.lstOrderNumbers = ShowDebit.lstOrderNumbers;
		try {
			params.reportCode = $('#function_code').val();
		} catch(e) {
			// Oop! Error
		}
		var kData = $.param(params,true);
		$('#divOverlay').show();		
		$.ajax({
			type : "POST",
			url : "/customerdebit/showdebit/exportdebitcustomer",
			data :(kData),
			dataType: "json",
			success : function(data) {
				$('#divOverlay').hide();								
				if (data != undefined && data != null && !data.error) {
					var filePath = ReportUtils.buildReportFilePath(data.view);
					window.location.href = filePath;
				} else {
					$('#errMsg2').html(Utils.XSSEncode(data.errMsg)).show();					
				}
			}
		});	
		return false;
	},
	
	/**
	 * xu ly khi chon shop
	 * @author trietptm
	 * @since Dec 19, 2015
	 */
	changeShop: function() {
		$('#divOverlay').show();
		var par = new Object() ;
		par.shopCode = $('#shop').combobox('getValue');
		$.ajax({
			type : "POST",
			url: '/customerdebit/showdebit/change-shop',
			data :($.param(par, true)),
			dataType : "json",
			success : function(data) {
				if  (data != undefined && data != null) {
					$('.ErrorMsgStyle').hide();
					var lstSale = data.lstSale;
					var lstDeliver = data.lstDeliver;
					var lstCashier = data.lstCashier;
					
					if (lstSale != undefined && lstSale != null && lstSale.length > 0) {
						Utils.bindStaffCbx('#staffCode', lstSale, null, 206);
					}
					if (lstDeliver != undefined && lstDeliver != null && lstDeliver.length > 0) {
						Utils.bindStaffCbx('#transferStaff', lstDeliver, null, 206);
					}
					if (lstCashier != undefined && lstCashier != null && lstCashier.length > 0) {
						Utils.bindStaffCbx('#crashierStaff', lstCashier, null, 206);
					}
					if (data.fromDate != undefined && data.fromDate != null) {
						$('#fromDate').val(data.fromDate);
					}
					if (data.toDate != undefined && data.toDate != null) {
						$('#toDate').val(data.toDate);
					}
					if (ShowDebit._isLoad) {
						ShowDebit.initGrid();
						ShowDebit._isLoad = false;
					}
					$('#divOverlay').hide();
				}
			}
		});
	},
	
	/**
	 * khoi tao grid
	 * @author trietptm
	 * @since Dec 19, 2015
	 */
	initGrid: function() {
		var params = new Object();
		params.shopCode = $('#shop').combobox('getValue');
		params.fromDateS = $('#fromDate').val().trim();
		params.toDateS = $('#toDate').val().trim();
		params.status =  $('#status').val();
		ShowDebit._fromDateS = params.fromDateS;
		ShowDebit._toDateS = params.toDateS;
		ShowDebit._status = params.status;
		$('#dg').datagrid({
			url:'/customerdebit/showdebit/getinfo',		    	      
			width: $(window).width() - 50,
			//height: 350,
			rownumbers: true,	
			singleSelect: true,
			queryParams: (params),
			pagination: true,
			//showFooter:true,
			frozenColumns: [[
			    {field:'orderNumber', title:'Số đơn hàng/<br/>Số phiếu điều chỉnh công nợ', width: 130, resizable: false, align: 'left', formatter: function(v, r, i) {
					return Utils.XSSEncode(v);
				}},
				{field: 'shortCode', title: 'Khách hàng', width: 150, resizable: false, align:'left', formatter: function(value, row, index) {
					if (row.isFooter) {
						return '';
					}
					return Utils.XSSEncode(row.shortCode + ' - ' + row.customerName);
				}},  
				{field: 'address', title: 'Địa chỉ', width: 250, resizable: false ,align: 'left', formatter: function(v, r, i) {
					return Utils.XSSEncode(v);
				}}
			]],
			columns: [[				
				{field: 'createDate', title: 'Ngày nợ', width: 100, resizable: false, align: 'center', formatter: CommonFormatter.dateTimeFormatter},
				{field: 'payDate', title: 'Ngày phải trả', width: 100, align: 'center', resizable: false, formatter: CommonFormatter.dateTimeFormatter},
				{field: 'staffName', title: 'NVBH', width: 150, resizable:false, align: 'left', formatter: function(v, r, i) {
					return Utils.XSSEncode(v);
				}},
				{field: 'deliveryName', title: 'NVGH', width: 150, resizable: false, align: 'left', formatter: function(v, r, i) {
					return Utils.XSSEncode(v);
				}},
				{field: 'cashierName', title: 'NVTT', width: 150, resizable: false, align: 'left', formatter: function(v, r, i) {
					return Utils.XSSEncode(v);
				}},
				{field: 'totalAmount', title: 'Số tiền', width: 100, resizable: false , align:'right', formatter: function(value, row, index) {
					if (value != undefined && value != null) {
						if (row.isFooter) {
							return '<b>' + formatCurrencyInterger(ShowDebit.totalAmount) + '</b>';
						} else {
							ShowDebit.totalAmount += Number(value);
							return formatCurrencyInterger(value);							
						}
					}
					return 0;
				}},
				{field: 'totalAmountPay', title: 'Đã trả', width: 100, resizable: false, align: 'right', formatter: function(value, row, index) {
					if (value != undefined && value != null) {
						if (row.isFooter) {
							return '<b>' + formatCurrencyInterger(ShowDebit.totalAmountPay) + '</b>';
						} else {
							ShowDebit.totalAmountPay += Number(value);
							return formatCurrencyInterger(value);
						}
					}
					return 0;
				}},
				{field: 'discountAmount', title:'Chiết khấu', width:100, resizable:false, align:'right', formatter:function(value, row, index) {
					if (value != undefined && value != null) {
						if (row.isFooter) {
							return '';
						} else {
							return formatCurrency(Math.abs(value));
						}
					}
					return 0;
				}},
				{field:'totalAmountRemain', title:'Còn nợ', width:100, resizable:false, align:'right', formatter:function(value, row, index) {
					if (value != undefined && value != null){
						if (row.isFooter) {
							return '<b>' + formatCurrencyInterger(ShowDebit.totalAmountRemain) + '</b>';
						} else {
							ShowDebit.totalAmountRemain += Number(value);
							return formatCurrencyInterger(value);							
						}
					} 
					return 0;
				}},
				{field:'payType', title:'Loại', width:($(window).width() - 50 - 100 - 200 - 100 - 100 - 100 - 180 - 100 - 100 - 100-100) < 100 ? 100 : ($(window).width() - 50 - 100 - 200 - 100 - 100 - 100 - 180 - 100 - 100 - 100 -100), resizable:false, align:'left', formatter:function(value, row, index){
					if (row.totalAmountRemain == 0) {
						return "Đã tất toán";
					}
					if (row.payDate == null) {
						return (row.isFooter ? "" : "Chưa tới hạn");
					}
					var payDate = CommonFormatter.dateTimeFormatter(row.payDate);
					if (Utils.compareCurrentDateExReturn(getCurrentDate(),payDate) > 0) {
						return 'Quá hạn';
					} else if (Utils.compareCurrentDateExReturn(getCurrentDate(), payDate) == 0) {
						return 'Tới hạn';
					} else if (Utils.compareCurrentDateExReturn(getCurrentDate(), payDate) < 0) {
						return 'Chưa tới hạn';
					}
				}}
			]],
			rowStyler: function(index, row) {
				if (row.totalAmountRemain == 0) {
					return "color:#00F;";
				}
				if (row.payDate != null) {
					var payDate = CommonFormatter.dateTimeFormatter(row.payDate);
					if (Utils.compareCurrentDateExReturn(getCurrentDate(), payDate) > 0) {
						return 'color:red;font-weight:bold;'; //'Qúa hạn';
					} else if (Utils.compareCurrentDateExReturn(getCurrentDate(), payDate) == 0) {
						return 'color:#FFC000;font-weight:bold;'; //'Tới hạn';
					}	
				}
            },  
			onLoadSuccess: function(data) {
				$('.datagrid-header-rownumber').html('STT');
			   	$('.datagrid-header-row td div').css('text-align','center');		   	
			   	updateRownumWidthForJqGrid('.easyui-dialog');
		   	 	$(window).resize();
		   	 	ShowDebit.amountRemain = 0;
		   	 	if (ShowDebit.amountRemainMap != null) {
			   	 	for (var i = 0; i < ShowDebit.amountRemainMap.size(); i++) {
			   	 		ShowDebit.amountRemain += Number(ShowDebit.amountRemainMap.valArray[i]);
			   	 	}		   	 		
		   	 	}
			}
		});
	}
};
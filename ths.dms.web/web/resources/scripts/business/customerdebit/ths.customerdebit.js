/*
 * JavaScript file created by Rockstarapps Concatenation
*/

/*
 * START OF FILE - /web/web/resources/scripts/business/customerdebit/vnm.customerdebit-batch.js
 */
var CustomerDebitBatch = {
	_xhrSave : null,
	_checkF9 : null,
	_lstOrderCheck:null,
	_lstDebitSelectId:null,
	_curRowId: null,
	_lstNVTT: null,
	_rowIndexValueField: null,
	_arrayList: null,
	_mapRowIndex: null,
	_arrayRow: null,
	_remainMoney:0,
	_totalDebit:0,
	getList : function() {
		$('.ErrorMsgStyle').html('').hide();
		var msg = '';
		//msg = Utils.getMessageOfRequireCheck('shortCode','Mã khách hàng');
		//if(msg.length==0){
			msg = Utils.getMessageOfRequireCheck('type','Loại chứng từ', true);
		//}
		var fromDate = $('#fromDate').val().trim();
		var toDate = $('#toDate').val().trim();
		if(fromDate == '__/__/____'){
			$('#fromDate').val('');
			fromDate = '';
		}
		if(toDate == '__/__/____'){
			$('#toDate').val('');
			toDate = '';
		}
		if(msg.length == 0 && fromDate.length > 0){
			msg = Utils.getMessageOfInvalidFormatDate('fromDate', 'Từ ngày');
		}
		if(msg.length ==0 && toDate.length > 0){
			msg = Utils.getMessageOfInvalidFormatDate('toDate', 'Đến ngày');
		}
//		if(msg.length == 0){
//			msg = Utils.getMessageOfRequireCheck('toDate', 'Đến ngày');
//		}
		if(msg.length == 0 && fromDate.length > 0 && toDate.length > 0 &&  !Utils.compareDate(fromDate, toDate)){
			msg = msgErr_fromdate_greater_todate;
			$('#fromDate').focus();
		}
		if(msg.length>0){
			$('#errMsgTop').html(msg).show();
			return false;
		}
		
		CustomerDebitBatch._lstNVTT = $('#crashierStaff').combobox('getData');
		CustomerDebitBatch._arrayList = new Array();
		CustomerDebitBatch._arrayRow = new Array();
		CustomerDebitBatch._arrayList.push(-1);
		CustomerDebitBatch._mapRowIndex = new Map();
		
		var data = new Object();
//		data.orderNumber = $('#orderNumber').val().trim();
		data.shortCode = $('#shortCode').val().trim();
		data.fromDate = $('#fromDate').val().trim();
		data.toDate = $('#toDate').val().trim();
		data.type = $('#type').val().trim();
		data.staffDeliverCode = $('#transferStaff').combobox('getValue').trim();
		data.staffPaymentCode = $('#crashierStaff').combobox('getValue').trim();
		data.staffCode = $('#staffCode').combobox('getValue').trim();

		$('#dg').datagrid('load',data);
		return false;
	},
	viewExcel:function(){		
		var importFileName = $('#excelFile').val();
		if(importFileName.length > 0){
			$('#btnImport').attr('disabled',false);
			$('#btnImport').removeClass('BtnGeneralDStyle');
		}		
		$('#isView').val(1);
		$('#importFrm').submit();
		return false;
	},
	importExcel:function(){		
		$('#btnImport').attr('disabled',true);
		$('#btnImport').addClass('BtnGeneralDStyle');
		msg = 'Bạn có muốn nhập từ file này không?';
		$.messager.confirm('Xác nhận', msg, function(r){
			if (r){
				$('#isView').val(0);
				$('#importFrm').submit();
			}
		});	
		return false;
	},		
	loadInfo : function(){
		CustomerDebitBatch.getInfo(false,false,true);
	},
	getInfo : function(checkF9,isSave, isSaveExcel) {
		$('.ErrorMsgStyle').html('').hide();
		if($('#shortCode').val().trim().length == 0){			
			return false;
		}	
		$('#payreceiptValue').val('');
		$('#debitPreRemain').val($('#debitPostRemain').val());
		var params = new Object();
		//params.staffCode = $('#staffCode').val().trim();
		params.shortCode = $('#shortCode').val().trim();
		params.type = $('#type').val();
		if($('#type').val() == -1) {
			return false;
		}
		$('#dg').datagrid('load',params);
		if(checkF9) {
			CustomerDebitBatch._checkF9 = false;
		}		
		return false;
	},
	import : function() {
		$('.ErrorMsgStyle').html('').hide();
		var msg = '';
		/*msg = Utils.getMessageOfRequireCheck('staffCode','Mã nhân viên bán hàng');
		if(msg.length ==0){
			msg = Utils.getMessageOfSpecialCharactersValidate('staffCode','Mã nhân viên bán hàng',Utils._CODE);
		}*/
		//msg = Utils.getMessageOfRequireCheck('shortCode','Mã khách hàng' );
		/*if(msg.length ==0){
			msg = Utils.getMessageOfRequireCheck('shortCode','Mã khách hàng' );
		}*/
		//if(msg.length ==0){
			msg = Utils.getMessageOfSpecialCharactersValidate('shortCode','Mã khách hàng',Utils._CODE );
		//}
		if(msg.length==0&& $('#type').val() == -1 ){
			msg = 'Bạn chưa chọn loại chứng từ. Yêu cầu chọn loại chứng từ';
		}
		if(msg.length ==0){
			msg = Utils.getMessageOfRequireCheck('payreceiptCode','Số chứng từ' );
		}
		if(msg.length ==0){
			msg = Utils.getMessageOfSpecialCharactersValidate('payreceiptCode','Số chứng từ',Utils._CODE );
		}
		if(msg.length ==0){
			msg = Utils.getMessageOfRequireCheck('payreceiptValue','Số tiền', undefined, true);
		}
		var payreceiptValue = Utils.returnMoneyValue($('#payreceiptValue').val().trim());
		if(payreceiptValue.length > 20){
			msg = 'Giá trị Số tiền nhập vào chỉ giới hạn 20 ký tự';
		}
		var type = $('#type').val().trim();
		if(msg.length ==0 &&(isNaN(payreceiptValue) || payreceiptValue <=0)){
			if(type==1){
				msg ='Số tiền phiếu thu phải là số nguyên dương';
			} else{
				msg ='Số tiền phiếu chi phải là số nguyên dương';
			}
		}
		
		if(msg.length > 0){
			$('#errMsg').html(msg).show();
			return false;
		}		
		var lstCustomerDebitID = new Array();
		//var rows =  $('#dg').datagrid('getSelections');
		var lstPayAmount = [];
		var lstDiscount = [];
		var lstStaffCode = [];
		var rowCheck = $('[name=debitId]');//$('#dg').datagrid('getChecked');
		var rows = $('#dg').datagrid('getRows');
		if (rows != undefined && rows != null) {
			var dis;
			for(var i=0, sz = rows.length; i < sz; i++){
				if(rowCheck[i].checked){
					lstCustomerDebitID.push(rows[i].debitId);
					lstPayAmount.push(rows[i].payAmount);
					dis = $('#discount-' + rows[i].debitId).val().replace(/,/g, '');
					lstDiscount.push(Number(dis));
					var cbx = $('#dg').datagrid('getEditors', i)[0];
					lstStaffCode.push($(cbx.target).combobox('getValue').trim());
				}
			}
		}
		if(lstCustomerDebitID.length <=0 ){
			$('#errMsg').html("Chưa chọn đơn hàng để thanh toán").show();
			return false;
		}
		var cAmount = Number(Utils.returnMoneyValue($('#cAmount').html().trim()));
		var cPay = Number(payreceiptValue);
//		if(($('#type').val() == 1 && CustomerDebitBatch._remainMoney > 0) || ($('#type').val() == 0 && CustomerDebitBatch._remainMoney < 0)) {
//			$('#errMsg').html('Số tiền phải được thanh toán hết').show();
//			return false;
//		}
		if(cPay - cAmount != 0) {
			//$('#errMsg').html('Số tiền phải được thanh toán hết').show();
			$('#errMsg').html('Giá trị phiếu ' + (type==1 ? 'thu' : 'chi') + ' phải bằng tổng tiền thanh toán.').show();
			return false;
		}
		
		var params = new Object();
		//params.staffCode = $('#staffCode').val().trim();
		params.shortCode = $('#shortCode').val().trim();
		params.type = $('#type').val();
		params.payreceiptCode = $('#payreceiptCode').val().trim();
		params.payreceiptValue = payreceiptValue;
		params.lstCustomerDebitID = lstCustomerDebitID;
		params.lstPayAmount = lstPayAmount;
		params.lstDiscount = lstDiscount;
		params.lstStaffCode = lstStaffCode;
		msg ='Bạn có muốn thanh toán theo các đơn hàng này ?';
		Utils.addOrSaveData(params, '/customerdebit/batch/import', null,  'errMsg', function(data) {			
			//CustomerDebitBatch.getInfo(false,true);
			CustomerDebitBatch.getList();
		}, 'loadingImport',null, null, msg,function(dataErr) {
			if(dataErr.errorReloadPage == true){
				window.location.href = '/customerdebit/batch/info';
			}
		});
		return false;
	},
	changeCheckBox: function(input) {
		var count = 0;
		$("input[name=chkCUSTOMERDEBIT]").each(function(){
	   		++count;
	   	});
		if (count == $(".cb_element:checked").length){
			$('#chkCUSTOMERDEBIT_ALL').attr('checked', 'checked');
		}
		else{
			$('#chkCUSTOMERDEBIT_ALL').attr('checked',false);
		}
		
		var checkbox = $('#' +input);
		
		var payreceiptValueTemp = $('#payreceiptValue').val();
		var payreceiptValue = Number(Utils.returnMoneyValue(payreceiptValueTemp));
		if($('#type').val()== 0){
			payreceiptValue = -payreceiptValue;
		}
		var checked_status = checkbox.is(':checked');
		if((payreceiptValueTemp == "" || payreceiptValueTemp == '') && checked_status){
			var msg = "Hãy nhập số tiền cho số chứng từ";
			Alert('Thông báo',msg);
			checkbox.attr('checked',false);
			return false;
		}
		var sum = 0;
		$(".cb_element:checked").each(function(){
	   		sum = sum + Number($(this).val());
	   	});
		var test = Math.abs(payreceiptValue) -Math.abs(sum);
		var debitPreRemain = Number(Utils.returnMoneyValue($('#debitPreRemain').val()));
		if(test<0) {
			var msg = "Số tiền còn lại của phiếu thu không thể trả đủ cho đơn hàng";
			$('#debitPostRemain').val(formatCurrencyInterger(debitPreRemain -payreceiptValue));
			Alert('Thông báo',msg);
		} else{
			$('#debitPostRemain').val(formatCurrencyInterger(debitPreRemain-sum));
		}
		return false;
	},
	changeCheckBoxALL: function() {
		
	},
	changePayReceiptValue: function() {
		var payreceiptValueTemp = $('#payreceiptValue').val();
		if(payreceiptValueTemp.length != 0) {
			var payreceiptValue = Number(Utils.returnMoneyValue(payreceiptValueTemp));
			var type = $('#type').val();
			var msg ='';
			if(isNaN(payreceiptValue) || payreceiptValue <=0){
				if(type==1){
					msg ='Số tiền phiếu thu phải là số nguyên dương';
				} else{
					msg ='Số tiền phiếu chi phải là số nguyên dương';
				}
				Alert('Thông báo',msg, function() {
					$('#payreceiptValue').val('');
					$('#payreceiptValue').focus();
				});
			} else{
				//var test;
				//var sum = 0;
				//$(".cb_element:checked").each(function(){
			   	//	sum = sum + Number($(this).val());
			   	//});
				var heSo = 1;
				if(type== 0){
					heSo = -1;
					//payreceiptValue = -payreceiptValue;
				}
				//var debitPreRemain = Number(Utils.returnMoneyValue($('#debitPreRemain').val()));
				//test = Math.abs(payreceiptValue) - Math.abs(sum);
				//if(test<0) {
				//	if(type==1){
				//		msg ='Số tiền còn lại của phiếu thu không thể trả đủ cho đơn hàng';
				//	} else{
				//		msg ='Số tiền còn lại của phiếu chi không thể trả đủ cho đơn hàng';
				//	}
				//	$('#debitPostRemain').val(formatCurrencyInterger(debitPreRemain-payreceiptValue));
				//	Alert('Thông báo',msg);
				//}else{
				//	$('#debitPostRemain').val(formatCurrencyInterger(debitPreRemain-payreceiptValue));
//					$('#debitPostRemain').val($('#debitPreRemain').val());
				//}
				CustomerDebitBatch._remainMoney = payreceiptValue;
				var cAmount = Number(Utils.returnMoneyValue($('#cAmount').html().trim()));
				$('#debitPostRemain').val(formatCurrency(CustomerDebitBatch._totalDebit - Math.min(payreceiptValue, cAmount) * heSo));
				//$('#dg').datagrid('uncheckAll');
			}
			return false;
		}
	},
	changeCustomer: function() {
		if($('#shortCode').val().trim().length != 0){
			var url = 'shopCode=' + encodeChar($('#shopCode').val()) + '&shortCode=' + encodeChar($('#shortCode').val());
			$.getJSON('/customerdebit/auto/currentDebit?' +url, function(data){
				if(data.error) {
					$('#errMsg').html(data.errMsg).show();
				} else {
					$('#debitPreRemain').val(formatCurrencyInterger(data.currentDebit) );
					CustomerDebitBatch._totalDebit = data.currentDebit;
					$('#debitPostRemain').val('');
				}
			});
		}
		return false;
	},
	showImportExcelOnDialog:function(){		
		$('#importExcelContent').css("visibility", "visible");
		var html = $('#importExcelContent').html();
		$('#errExcelMsg').html('').hide();
		$('#importExcelContenDialog').dialog({  	       
	        closed: false,  
	        cache: false,  
	        modal: true,
	        width : 850,
	        height :450,
	        onOpen: function(){
	        	$('.easyui-dialog #dgDetail').datagrid({					
					//autoRowHeight : true,
					rownumbers : true, 
					singleSelect: true,					
					//rowNum : 10,
					scrollbarSize : 0,
					fitColumns:true,
					//cache: false, 					
					width : $('#importExcelContenDialog').width()-20,
					//height:auto,
					//autoWidth: true,
				    columns:[[
						{field: 'shortCode',title: 'Mã KH',  width: 100,sortable:false,resizable:false, align: 'left' , formatter:function(value, options, rowObject) {
							return Utils.XSSEncode(value);
						}},
						{field: 'type',title: 'Loại CT',  width: 100,sortable:false,resizable:false, align: 'left' , formatter:function(value, options, rowObject) {
							return Utils.XSSEncode(value);
						}},
						{field: 'payreceiptCode',title: 'Số CT',  width: 100,sortable:false,resizable:false, align: 'left' , formatter:function(value, options, rowObject) {
							return Utils.XSSEncode(value);
						}},
						{field: 'money',title: 'Số tiền',  width: 100,sortable:false,resizable:false, align: 'right', formatter:function(cellValue, options, rowObject) {
							if(cellValue!=undefined && cellValue!=null){
								return formatCurrency(Math.abs(cellValue));
							}
							return '';
						} },
						{field: 'discount',title: 'Chiết khấu',  width: 100,sortable:false,resizable:false, align: 'right', formatter:function(cellValue, options, rowObject) {
							if(cellValue!=undefined && cellValue!=null){
								return formatCurrency(Math.abs(cellValue));
							}
							return '';
						}}
				    ]],
				    onLoadSuccess :function(data){   	 
				    	
				    }
	        	});
	        	$('.datagrid-header-rownumber').html('STT');
	         	$('.datagrid-header-row td div').css('text-align','center');	
	        },
	        onClose:function(){
	        	$('#importExcelContent').html(html);
	        	$('#importExcelContent').css("visibility", "hidden");
	        	$('.easyui-dialog #dgDetail').datagrid('loadData', []);
	        	$('#excelFile').val('');
	        	$('#fakefilepc').val('');
	        }
		});
	},
	beforeImportExcel: function(){
		if(!previewImportExcelFile(document.getElementById("excelFile"))){
			return false;
		}		
		$('#errExcelMsg').html('').hide();		
		showLoadingIcon();
		return true;
	},
	afterImportExcelUpdate: function(responseText, statusText, xhr, $form){
		if($('#btnImport').length != 0) {
			enable('btnImport'); 
		}
		hideLoadingIcon();
		if (statusText == 'success') {				
	    	$("#responseDiv").html(responseText);		 
	    	var newToken = $('#responseDiv #newToken').val();
	    	console.log(newToken);
	    	if(newToken != null && newToken != undefined && newToken != '') {
	    		$('#token').val(newToken);	    		
	    		$('#tokenImport').val(newToken);
	    	}
	    	if($('#errorExcel').html().trim() == 'true' || $('#errorExcelMsg').html().trim().length > 0){
	    		$('#errExcelMsg').html($('#errorExcelMsg').html()).show();
	    	} else {
	    		if($('#typeView').html().trim() == 'false'){
	    			var totalRow = parseInt($('#totalRow').html().trim());
	    			var numFail = parseInt($('#numFail').html().trim());
	    			var fileNameFail = $('#fileNameFail').html();
	    			var mes = format(msgErr_result_import_excel,(totalRow - numFail),numFail);		    		
	    			if(numFail > 0){
	    				mes+= ' <a href="' + fileNameFail +'">Xem chi tiết lỗi</a>';
	    			}
	    			if($('#excelFile').length!=0 && $('#fakefilepc').length!=0){
	    				try{$('#excelFile').val('');$('#fakefilepc').val('');}catch(err){}
	    			}
	    			$('#errExcelMsg').html(mes).show();	    				    			
	    		}else{	    
	    			$('.easyui-dialog #dgDetail').datagrid("loadData", {total:0, rows:[]});
	    			$('.excel-item').each(function(){
	    				var row = new Object();
	    				//row.staffCode = $(this).attr('staffCode');
	    				row.shortCode = $(this).attr('shortCode');
	    				row.type = $(this).attr('typect') == '0' ? 'phiếu thu' : $(this).attr('typect') == '1' ? 'phiếu chi' : $(this).attr('typect');
	    				row.payreceiptCode = $(this).attr('payreceiptCode');
	    				row.money = $(this).attr('money');
	    				row.discount = $(this).attr('discount');
	    				row.errMsg = $(this).attr('errMsg');
	    				$('.easyui-dialog #dgDetail').datagrid('appendRow',row);
	    			});
	    		}
	    	}
	    }	
	},
	exportExcel: function() {
		$('#divOverlay').addClass('Overlay');
		$('#imgOverlay').show();
		$('#errMsg').html('').hide();
		$('#errExcelMsg').html('').hide();
		$('.ErrorMsgStyle').html('').hide();
		var msg = '';
		msg = Utils.getMessageOfRequireCheck('type','Loại chứng từ', true);
		var fromDate = $('#fromDate').val().trim();
		var toDate = $('#toDate').val().trim();
		if(fromDate == '__/__/____'){
			$('#fromDate').val('');
			fromDate = '';
		}
		if(toDate == '__/__/____'){
			$('#toDate').val('');
			toDate = '';
		}
		if(msg.length == 0 && fromDate.length > 0){
			msg = Utils.getMessageOfInvalidFormatDate('fromDate', 'Từ ngày');
		}
		if(msg.length ==0 && toDate.length > 0){
			msg = Utils.getMessageOfInvalidFormatDate('toDate', 'Đến ngày');
		}
		if(msg.length == 0 && fromDate.length > 0 && toDate.length > 0 &&  !Utils.compareDate(fromDate, toDate)){
			msg = msgErr_fromdate_greater_todate;
			$('#fromDate').focus();
		}
		if(msg.length>0){
			$('#errMsgTop').html(msg).show();
			return false;
		}
		var data = new Object();
		data.shortCode = $('#shortCode').val().trim();
		data.fromDate = $('#fromDate').val().trim();
		data.toDate = $('#toDate').val().trim();
		data.type = $('#type').val().trim();
		data.staffDeliverCode = $('#transferStaff').combobox('getValue').trim();
		data.staffPaymentCode = $('#crashierStaff').combobox('getValue').trim();
		data.staffCode = $('#staffCode').combobox('getValue').trim();
//		var lstCustomerDebitID = new Array();
//		var rowCheck = $('[name=debitId]');
//		var rows = $('#dg').datagrid('getRows');
//		if (rows != undefined && rows != null) {
//			for(var i=0, sz = rows.length; i < sz; i++){
//				if(rowCheck[i].checked){
//					lstCustomerDebitID.push(rows[i].debitId);
//				}
//			}
//		}
//		data.lstDebitId = lstCustomerDebitID;
		var url = "/customerdebit/batch/exportExcel";
		CommonSearch.exportExcelData(data,url);
		return false;
	}
};
/*
 * END OF FILE - /web/web/resources/scripts/business/customerdebit/vnm.customerdebit-batch.js
 */

/*
 * START OF FILE - /web/web/resources/scripts/business/customerdebit/vnm.customerdebit-reduce.js
 */
var CustomerDebitReduce = {
	_xhrSave : null,
	_checkF9 : null,
	_lstOrderCheck:null,
	_lstDebitSelectId:null,
	_remainMoney:0,
	_totalDebit:0,
	getList : function() {
		$('.ErrorMsgStyle').html('').hide();
		var msg = '';
		var fromDate = $('#fromDate').val().trim();
		var toDate = $('#toDate').val().trim();
		if(fromDate == '__/__/____'){
			$('#fromDate').val('');
			fromDate = '';
		}
		if(toDate == '__/__/____'){
			$('#toDate').val('');
			toDate = '';
		}
		if(msg.length == 0 && fromDate.length > 0){
			msg = Utils.getMessageOfInvalidFormatDate('fromDate', 'Từ ngày');
		}
		if(msg.length ==0 && toDate.length > 0){
			msg = Utils.getMessageOfInvalidFormatDate('toDate', 'Đến ngày');
		}
		if(msg.length == 0 && fromDate.length > 0 && toDate.length > 0 &&  !Utils.compareDate(fromDate, toDate)){
			msg = msgErr_fromdate_greater_todate;
			$('#fromDate').focus();
		}
		if(msg.length>0){
			$('#errMsgTop').html(msg).show();
			return false;
		}
		var data = new Object();
		data.shortCode = $('#shortCode').val().trim();
		data.fromDate = $('#fromDate').val().trim();
		data.toDate = $('#toDate').val().trim();
		data.type = 1;//Phiếu thu - $('#type').val().trim();
		data.staffDeliverCode = $('#transferStaff').combobox('getValue').trim();
		data.staffPaymentCode = $('#crashierStaff').combobox('getValue').trim();
		data.staffCode = $('#staffCode').combobox('getValue').trim();

		$('#dg').datagrid('load',data);
		return false;
	},
	viewExcel:function(){		
		var importFileName = $('#excelFile').val();
		if(importFileName.length > 0){
			$('#btnImport').attr('disabled',false);
			$('#btnImport').removeClass('BtnGeneralDStyle');
		}		
		$('#isView').val(1);
		$('#importFrm').submit();
		return false;
	},
	importExcel:function(){		
		$('#btnImport').attr('disabled',true);
		$('#btnImport').addClass('BtnGeneralDStyle');
		msg = 'Bạn có muốn nhập từ file này không?';
		$.messager.confirm('Xác nhận', msg, function(r){
			if (r){
				$('#isView').val(0);
				$('#importFrm').submit();
			}
		});	
		return false;
	},		
	loadInfo : function(){
		CustomerDebitReduce.getInfo(false,false,true);
	},
	getInfo : function(checkF9,isSave, isSaveExcel) {
		$('.ErrorMsgStyle').html('').hide();
		if($('#shortCode').val().trim().length == 0){			
			return false;
		}	
		$('#payreceiptValue').val('');
		$('#debitPreRemain').val($('#debitPostRemain').val());
		var params = new Object();
		params.shortCode = $('#shortCode').val().trim();
		params.type = 1;//Phiếu thu - $('#type').val();
		$('#dg').datagrid('load',params);
		if(checkF9) {
			CustomerDebitReduce._checkF9 = false;
		}		
		return false;
	},
	import : function() {
		$('.ErrorMsgStyle').html('').hide();
		var msg = '';
		msg = Utils.getMessageOfSpecialCharactersValidate('shortCode','Mã khách hàng',Utils._CODE );
		if(msg.length ==0){
			msg = Utils.getMessageOfRequireCheck('payreceiptCode','Số chứng từ' );
		}
		if(msg.length ==0){
			msg = Utils.getMessageOfSpecialCharactersValidate('payreceiptCode','Số chứng từ',Utils._CODE );
		}
		if(msg.length ==0){
			msg = Utils.getMessageOfRequireCheck('payreceiptValue','Số tiền', undefined, true);
		}
		var payreceiptValue = Utils.returnMoneyValue($('#payreceiptValue').val().trim());
		if(payreceiptValue.length > 20){
			msg = 'Giá trị Số tiền nhập vào chỉ giới hạn 20 ký tự';
		}
		var type = 1;//Phiếu thu - $('#type').val().trim();
		if(msg.length ==0 &&(isNaN(payreceiptValue) || payreceiptValue <=0)){
			msg ='Số tiền giảm nợ phải là số nguyên dương';	
		}
		
		if(msg.length > 0){
			$('#errMsg').html(msg).show();
			return false;
		}		
		var lstCustomerDebitID = new Array();
		//var rows =  $('#dg').datagrid('getSelections');
		var lstPayAmount = [];
		var lstDiscount = [];
		var rows = $('#dg').datagrid('getChecked');
		if (rows != undefined && rows != null) {
			for(var i=0, sz = rows.length; i < sz; i++){
				lstCustomerDebitID.push(rows[i].debitId);
				lstPayAmount.push(rows[i].payAmount);
			}
		}
		if(lstCustomerDebitID.length <=0 ){
			$('#errMsg').html("Chưa chọn đơn hàng để giảm nợ").show();
			return false;
		}
		var cAmount = Number(Utils.returnMoneyValue($('#cAmount').html().trim()));
		var cPay = Number(payreceiptValue);
		if(cPay - cAmount != 0) {
			$('#errMsg').html('Giá trị phiếu giảm nợ phải bằng tổng tiền đơn hàng chọn.').show();
			return false;
		}
		
		var params = new Object();
		params.shortCode = $('#shortCode').val().trim();
		params.type = 1;//Phiếu thu - $('#type').val();
		params.payreceiptCode = $('#payreceiptCode').val().trim();
		params.payreceiptValue = payreceiptValue;
		params.lstCustomerDebitID = lstCustomerDebitID;
		params.lstPayAmount = lstPayAmount;
		params.lstDiscount = lstDiscount;
		msg ='Bạn có muốn thanh toán theo các đơn hàng này ?';
		Utils.addOrSaveData(params, '/customerdebit/reduce/import', null,  'errMsg', function(data) {
			CustomerDebitReduce.getList();
		}, 'loadingImport',null, null, msg,function(dataErr) {
			if(dataErr.errorReloadPage == true){
				window.location.href = '/customerdebit/reduce/info';
			}
		});
		return false;
	},
	changeCheckBox: function(input) {
		var count = 0;
		$("input[name=chkCUSTOMERDEBIT]").each(function(){
	   		++count;
	   	});
		if (count == $(".cb_element:checked").length){
			$('#chkCUSTOMERDEBIT_ALL').attr('checked', 'checked');
		}
		else{
			$('#chkCUSTOMERDEBIT_ALL').attr('checked',false);
		}
		
		var checkbox = $('#'+input);
		
		var payreceiptValueTemp = $('#payreceiptValue').val();
		var payreceiptValue = Number(Utils.returnMoneyValue(payreceiptValueTemp));
		var checked_status = checkbox.is(':checked');
		if((payreceiptValueTemp == "" || payreceiptValueTemp == '') && checked_status){
			var msg = "Hãy nhập số tiền cho số chứng từ";
			Alert('Thông báo',msg);
			checkbox.attr('checked',false);
			return false;
		}
		var sum = 0;
		$(".cb_element:checked").each(function(){
	   		sum = sum + Number($(this).val());
	   	});
		var test = Math.abs(payreceiptValue) -Math.abs(sum);
		var debitPreRemain = Number(Utils.returnMoneyValue($('#debitPreRemain').val()));
		if(test<0) {
			var msg = "Số tiền còn lại của phiếu thu không thể trả đủ cho đơn hàng";
			$('#debitPostRemain').val(formatCurrencyInterger(debitPreRemain -payreceiptValue));
			Alert('Thông báo',msg);
		} else{
			$('#debitPostRemain').val(formatCurrencyInterger(debitPreRemain-sum));
		}
		return false;
	},
	changeCheckBoxALL: function() {
		
	},
	changePayReceiptValue: function() {
		var payreceiptValueTemp = $('#payreceiptValue').val();
		if(payreceiptValueTemp.length != 0) {
			var payreceiptValue = Number(Utils.returnMoneyValue(payreceiptValueTemp));
			var type = 1;//Phiếu thu - $('#type').val();
			var msg ='';
			if(isNaN(payreceiptValue) || payreceiptValue <=0){
				if(type==1){
					msg ='Số tiền phiếu thu phải là số nguyên dương';
				} else{
					msg ='Số tiền phiếu chi phải là số nguyên dương';
				}
				Alert('Thông báo',msg, function() {
					$('#payreceiptValue').val('');
					$('#payreceiptValue').focus();
				});
			} else{
				var heSo = 1;
				CustomerDebitReduce._remainMoney = payreceiptValue;
				var cAmount = Number(Utils.returnMoneyValue($('#cAmount').html().trim()));
				$('#debitPostRemain').val(formatCurrency(CustomerDebitReduce._totalDebit - Math.min(payreceiptValue, cAmount) * heSo));
				//$('#dg').datagrid('uncheckAll');
			}
			return false;
		}
	},
	changeCustomer: function() {
		if($('#shortCode').val().trim().length != 0){
			var url = 'shopCode='+ encodeChar($('#shopCode').val()) + '&shortCode='+ encodeChar($('#shortCode').val());
			$.getJSON('/customerdebit/auto/currentDebit?'+url, function(data){
				if(data.error) {
					$('#errMsg').html(data.errMsg).show();
				} else {
					$('#debitPreRemain').val(formatCurrencyInterger(data.currentDebit) );
					CustomerDebitReduce._totalDebit = data.currentDebit;
					$('#debitPostRemain').val('');
				}
			});
		}
		return false;
	},
	showImportExcelOnDialog:function(){		
		$('#importExcelContent').css("visibility", "visible");
		var html = $('#importExcelContent').html();
		$('#errExcelMsg').html('').hide();
		$('#importExcelContenDialog').dialog({  	       
	        closed: false,  
	        cache: false,  
	        modal: true,
	        width : 850,
	        height :450,
	        onOpen: function(){
	        	$('.easyui-dialog #dgDetail').datagrid({					
					//autoRowHeight : true,
					rownumbers : true, 
					singleSelect: true,					
					//rowNum : 10,
					scrollbarSize : 0,
					fitColumns:true,
					//cache: false, 					
					width : $('#importExcelContenDialog').width()-20,
					//height:auto,
					//autoWidth: true,
				    columns:[[
						{field: 'shortCode',title: 'Mã KH',  width: 120,sortable:false,resizable:false, align: 'left' , formatter:function(value, options, rowObject) {
							return Utils.XSSEncode(value);
						}},
						{field: 'type',title: 'Loại CT',  width: 100,sortable:false,resizable:false, align: 'right' , formatter:function(value, options, rowObject) {
							return Utils.XSSEncode(value);
						}},
						{field: 'payreceiptCode',title: 'Số CT',  width: 100,sortable:false,resizable:false, align: 'left' , formatter:function(value, options, rowObject) {
							return Utils.XSSEncode(value);
						}},
						{field: 'money',title: 'Số tiền',  width: 120,sortable:false,resizable:false, align: 'right', formatter:function(cellValue, options, rowObject) {
							if(cellValue!=undefined && cellValue!=null){
								return formatCurrency(Math.abs(cellValue));
							}
							return '';
						} },
				    ]],
				    onLoadSuccess :function(data){   	 
				    	
				    }
	        	});
	        	$('.datagrid-header-rownumber').html('STT');
	         	$('.datagrid-header-row td div').css('text-align','center');	
	        },
	        onClose:function(){
	        	$('#importExcelContent').html(html);
	        	$('#importExcelContent').css("visibility", "hidden");
	        	$('.easyui-dialog #dgDetail').datagrid('loadData', []);
	        	$('#excelFile').val('');
	        	$('#fakefilepc').val('');
	        }
		});
	},
	beforeImportExcel: function(){
		if(!previewImportExcelFile(document.getElementById("excelFile"))){
			return false;
		}		
		$('#errExcelMsg').html('').hide();		
		showLoadingIcon();
		return true;
	},
	afterImportExcelUpdate: function(responseText, statusText, xhr, $form){
		if($('#btnImport').length != 0) {
			enable('btnImport'); 
		}
		hideLoadingIcon();
		if (statusText == 'success') {				
	    	$("#responseDiv").html(responseText);		 
	    	var newToken = $('#responseDiv #newToken').val();
	    	console.log(newToken);
	    	if(newToken != null && newToken != undefined && newToken != '') {
	    		$('#token').val(newToken);	    		
	    		$('#tokenImport').val(newToken);
	    	}
	    	if($('#errorExcel').html().trim() == 'true' || $('#errorExcelMsg').html().trim().length > 0){
	    		$('#errExcelMsg').html($('#errorExcelMsg').html()).show();
	    	} else {
	    		if($('#typeView').html().trim() == 'false'){
	    			var totalRow = parseInt($('#totalRow').html().trim());
	    			var numFail = parseInt($('#numFail').html().trim());
	    			var fileNameFail = $('#fileNameFail').html();
	    			var mes = format(msgErr_result_import_excel,(totalRow - numFail),numFail);		    		
	    			if(numFail > 0){
	    				mes+= ' <a href="'+ fileNameFail +'">Xem chi tiết lỗi</a>';
	    			}
	    			if($('#excelFile').length!=0 && $('#fakefilepc').length!=0){
	    				try{$('#excelFile').val('');$('#fakefilepc').val('');}catch(err){}
	    			}
	    			$('#errExcelMsg').html(mes).show();	    				    			
	    		}else{	    
	    			$('.easyui-dialog #dgDetail').datagrid("loadData", {total:0, rows:[]});
	    			$('.excel-item').each(function(){
	    				var row = new Object();
	    				//row.staffCode = $(this).attr('staffCode');
	    				row.shortCode = $(this).attr('shortCode');
	    				row.type = $(this).attr('typect') == '0' ? 'phiếu thu' : $(this).attr('typect') == '1' ? 'phiếu chi' : $(this).attr('typect');
	    				row.payreceiptCode = $(this).attr('payreceiptCode');
	    				row.money = $(this).attr('money');
	    				row.errMsg = $(this).attr('errMsg');
	    				$('.easyui-dialog #dgDetail').datagrid('appendRow',row);
	    			});
	    		}
	    	}
	    }	
	}
};
/*
 * END OF FILE - /web/web/resources/scripts/business/customerdebit/vnm.customerdebit-reduce.js
 */

/*
 * START OF FILE - /web/web/resources/scripts/business/customerdebit/vnm.customerdebit-auto.js
 */
var CustomerDebitAuto = {
	_isLoad: true,
	getList: function() {
		$('.ErrorMsgStyle').hide();
		$('.SuccessMsgStyle').hide();
		var msg = Utils.getMessageOfRequireCheck('shop', jsp_common_shop_name_lable, true);
		var fromDate = $('#fromDate').val().trim();
		var toDate = $('#toDate').val().trim();
		if (fromDate == '__/__/____') {
			$('#fromDate').val('');
		}
		if (toDate == '__/__/____') {
			$('#toDate').val('');
		}
		if (msg.length == 0) {
			msg = Utils.getMessageOfRequireCheck('shortCode','Khách hàng');
		}		
		if (msg.length == 0) {
			msg = Utils.getMessageOfSpecialCharactersValidate('shortCode','Khách hàng',Utils._CODE);
		}
		//
		if (msg.length == 0) {
			msg = Utils.getMessageOfRequireCheck('status','Loại chứng từ', true);
		}
		if (msg.length == 0 && fromDate != null && fromDate.length > 0) {
			msg = Utils.getMessageOfInvalidFormatDate('fromDate', 'Từ ngày');
		}
		//
		if (msg.length == 0){
			msg = Utils.getMessageOfRequireCheck('payReceivedNumber','Số chứng từ');
		}
		if (msg.length == 0) {
			msg = Utils.getMessageOfRequireCheck('valuePay','Số tiền', undefined, true);
		}
		var valuePay = Utils.returnMoneyValue($('#valuePay').val().trim());
		if (valuePay.length > 20) {
			msg = 'Giá trị Số tiền nhập vào chỉ giới hạn 20 ký tự';
		}
		if (valuePay.length == 0) {
			$("#afterPay").val("");
		}
		if (msg.length == 0) {
			msg = Utils.getMessageOfInvaildNumber('valuePay', 'Số tiền');
		}
		if (msg.length == 0) {
			msg = Utils.getMessageOfNegativeNumberCheck('valuePay','Số tiền');
		}
		if (msg.length == 0 && toDate != null && toDate.length > 0) {
			msg = Utils.getMessageOfInvalidFormatDate('toDate', 'Đến ngày');
		}
		
		if (msg.length == 0  && fromDate != null && fromDate.length > 0 && toDate != null && toDate.length > 0 && !Utils.compareDate(fromDate, toDate)) {
			msg = msgErr_fromdate_greater_todate;
			$('#fromDate').focus();
		}
		
		if (msg.length > 0) {
			$('#errMsgTop').html(msg).show();
			return false;
		}
		if (_isFullPrivilege || _MapControl.get("btnExportPOAutoReport") == 2) {
			$('#btnAuto').show();
		}
		var data = new Object();
		data.shopCode = $('#shop').combobox('getValue').trim();
		data.payReceivedNumber = $('#payReceivedNumber').val().trim();
		data.shortCode = $('#shortCode').val().trim();
		//data.staff2Code = $('#staff2Code').val().trim();
		data.valuePay = Utils.returnMoneyValue($('#valuePay').val().trim());
		data.status = $('#status').val().trim();
		data.fromDate = $('#fromDate').val().trim();
		data.toDate = $('#toDate').val().trim();
		data.staffDeliverCode = $('#transferStaff').combobox('getValue').trim();
		data.staffPaymentCode = $('#crashierStaff').combobox('getValue').trim();
		$('#dg').datagrid('load',data);
		return false;
	},	
	savePay: function() {
		$('.ErrorMsgStyle').hide();
		$('.SuccessMsgStyle').hide();
		var msg = Utils.getMessageOfRequireCheck('shop', jsp_common_shop_name_lable, true);
		if (msg.length == 0) {
			msg = Utils.getMessageOfRequireCheck('shortCode','Khách hàng');
		}		
		if (msg.length == 0) {
			msg = Utils.getMessageOfSpecialCharactersValidate('shortCode','Khách hàng',Utils._CODE);
		}
		if (msg.length == 0) {
			msg = Utils.getMessageOfRequireCheck('valuePay','Số tiền');
		}		
		if (msg.length == 0) {
			msg = Utils.getMessageOfRequireCheck('payReceivedNumber','Mã chứng từ');
		}		
		if (msg.length == 0) {
			msg = Utils.getMessageOfSpecialCharactersValidate('payReceivedNumber','Mã chứng từ',Utils._CODE);
		}
		if (msg.length == 0&& $('#status').val() == -1 ) {
			msg = 'Bạn chưa chọn loại chứng từ. Yêu cầu chọn loại chứng từ';
		}
		
		var fromDate = $('#fromDate').val().trim();
		var toDate = $('#toDate').val().trim();
		if (fromDate == '__/__/____') {
			$('#fromDate').val('');
		}
		if (toDate == '__/__/____') {
			$('#toDate').val('');
		}
		if (msg.length == 0 && fromDate.length > 0 && toDate.length > 0 && !Utils.compareDate(fromDate, toDate)) {
			msg = msgErr_fromdate_greater_todate;
			$('#fromDate').focus();
		}
		
		if (msg.length > 0) {
			$('#errMsg').html(msg).show();
			return false;
		}
		var lstDebitId = new Array();
		var lstDebitAmt = new Array();
//		var rows =  $('#dg').datagrid('getSelections');
		var rows =  $('#dg').datagrid('getRows');
		for (var i = 0, n = rows.length; i < n; ++i) {
			lstDebitId.push(rows[i].debitId);
			lstDebitAmt.push(rows[i].payAmt);
		}
		if (lstDebitId.length <= 0) {
			msg = 'Không có đơn hàng nào được chọn.';
		}
		if (msg.length > 0) {
			$('#errMsg').html(msg).show();
			return false;
		}
		var data = new Object();
		data.shopCode = $('#shop').combobox('getValue').trim();
		data.shortCode = $('#shortCode').val().trim();
		data.payReceivedNumber = $('#payReceivedNumber').val().trim();
		data.valuePay = Utils.returnMoneyValue($('#valuePay').val().trim());
		//data.staff2Code = $('#staff2Code').val().trim();
		data.status = $('#status').val().trim();	
		data.lstDebitId = lstDebitId;
		data.lstDebitAmt = lstDebitAmt;
		data.fromDate = $('#fromDate').val().trim();
		data.toDate = $('#toDate').val().trim();
		data.staffDeliverCode = $('#transferStaff').combobox('getValue').trim();
		data.staffPaymentCode = $('#crashierStaff').combobox('getValue').trim();
		Utils.addOrSaveData(data,"/customerdebit/auto/savepay", null, 'errMsg', function(data){
			if(data.error){
				$('#errMsg').html(data.errMsg).show();
			}else{
				$('#shortCode').val('');
				$('#status').val(1);
				$('#status').change();
				$('#payReceivedNumber').val(data.payReceivedNumber);
				$('#valuePay').val('');
				$('#afterPay').val('');
				$('#beforePay').val('');
				$("#debitNPPToKH").val("");
				$("#debitKHToNPP").val("");
				$('#dg').datagrid('loadData',[]);
			}
		},'loading2',null,null,null,function(data) {
			if(data.errMsg != undefined && data.errMsg != null){
				$('#errMsg').html(data.errMsg).show();
			} else {
				$('#errMsg').html('Người dùng này đã được đăng nhập vào máy khác, đề nghị tải lại trang.').show();
			}
		});
	},
	
	/**
	 * xu ly khi chon shop
	 * @author trietptm
	 * @since Dec 19, 2015
	 */
	changeShop: function() {
		$('#divOverlay').show();
		var param = new Object();
		param.shopCode = $('#shop').combobox('getValue');
		$.ajax({
			type : "POST",
			url: '/customerdebit/auto/change-shop',
			data :($.param(param, true)),
			dataType : "json",
			success : function(data) {
				if  (data != undefined && data != null) {
					$('.ErrorMsgStyle').hide();
					var lstSale = data.lstSale;
					var lstDeliver = data.lstDeliver;
					var lstCashier = data.lstCashier;

					if (lstDeliver != undefined && lstDeliver != null && lstDeliver.length > 0) {
						Utils.bindStaffCbx('#transferStaff', lstDeliver, null, 206, function(item) {
							CustomerDebitAuto.getCurrentDebit();
						});
					}
					if (lstCashier != undefined && lstCashier != null && lstCashier.length > 0) {
						Utils.bindStaffCbx('#crashierStaff', lstCashier, null, 206, function(item) {
							CustomerDebitAuto.getCurrentDebit();
						});
					}
					if (data.payReceivedNumber != undefined && data.payReceivedNumber != null) {
						$('#payReceivedNumber').val(data.payReceivedNumber);
					}
					if (CustomerDebitAuto._isLoad) {
						CustomerDebitAuto.initGrid();
						CustomerDebitAuto._isLoad = false;
					}
					CustomerDebitAuto.getCurrentDebit();
					$('#divOverlay').hide();
				}
			}
		});
	},
	
	/**
	 * khoi tao grid
	 * @author trietptm
	 * @since Dec 21, 2015
	 */
	initGrid: function() {
		var params = new Object();
		params.shopCode = $('#shop').combobox('getValue');
		$('#dg').datagrid({
			url:'/customerdebit/auto/getinfo',		    	      
		    width: $(window).width() - 55,
		    height: 350,
		    rownumbers: true,
		    fitColumns: true,
		    singleSelect: true,
		    //scrollbarSize : 0,
		    queryParams: params,	   
		    columns:[[
		        {field: 'debitId', hidden: true},
//	 	        {field: 'debitId',checkbox:true},
				{field: 'shortCode', title: 'Khách hàng', width: 150, align: 'left', resizable: false, formatter: function(value, row, index) {
					return Utils.XSSEncode(row.shortCode + ' - ' + row.customerName);
				}},
				{field: 'orderNumber', title: 'Số đơn hàng/<br/>Số phiếu điều chỉnh công nợ', width: 100 , resizable: false, align: 'left'},
				{field: 'orderDate', title: 'Ngày', width: 80, align: 'center', resizable: false, formatter: CommonFormatter.dateTimeFormatter},
				{field: 'nvbhCodeName', title: 'NVBH', width: 100, resizable: false, align: 'left', formatter: function (v, r, i) {
					if (v != undefined && v != null) {
						return Utils.XSSEncode(v + " - " + r.nvbhName);
					}
					return "";
				}},
				{field: 'nvghCodeName', title: 'NVGH', width: 100, resizable: false, align: 'left', formatter: function (v, r, i) {
					if (v != undefined && v != null) {
						return Utils.XSSEncode(v + " - " + r.nvghName);
					}
					return "";
				}},
				{field: 'nvttCodeName', title: 'NVTT', width: 100, resizable: false, align: 'left', formatter: function (v, r, i) {
					if (v != undefined && v != null) {
						return Utils.XSSEncode(v + " - " + r.nvttName);
					}
					return "";
				}},
				{field: 'total', title: 'Số tiền', width: 100, align: 'right', resizable: false, formatter: CustomerDebitAutoFormatter.signedNumberFormatter},
				{field: 'totalPay', title: 'Đã trả', width: 100, align: 'right', resizable: false, formatter:function(val, row, index) {
					if (row != undefined && row != null) {
						return formatCurrency(Math.abs(row.totalPay)) + '&nbsp;<span style="color:#f00;">(' + formatCurrency(Math.abs(row.discountAmount)) + ')</span>';
					}
					return '';
				}},
				{field: 'payAmt', title: 'Giá trị trả', width: 100, align: 'right', resizable: false, formatter: CustomerDebitAutoFormatter.signedNumberFormatter} 
			]],    
			onLoadSuccess :function(data) {
				GridUtil.updateDefaultRowNumWidth("#gridContainer", "dg");
				$('.ErrorMsgStyle').hide();
				$('.SuccessMsgStyle').hide();
				if (data != null && data.warning != null) {
					var warning = data.warning;
					if (!isFirstLoad) {
						$("#beforePay").val(formatCurrency(parseInt(data.totalIsReceived), 10));
						if (warning == 4) {
							$('#errMsgTop').html('Mã khách hàng không tồn tại trong hệ thống.').show();
							$('#shortCode').focus();
							$('#afterPay').val('');
							return;
						}
						if(warning != 1 && data.rows.length == 0) {
							if ($('#status').val() == 2) {//phieu chi
								$('#errMsgTop').html('Khách hàng không còn đơn hàng trả nào chưa thanh toán').show();
							} else if ($('#status').val() == 1) {//phieu thu
								$('#errMsgTop').html('Khách hàng không còn đơn hàng nợ nào').show();
							}
							return;
						}
						if (warning == 1) {
//	 					 $('#warning1').html("Số tiền phải được thanh toán hết.Gợi ý: có thể trả tối đa <span class=\"myWarning\">"+ <s:property value="totalIsReceived"/> + "</span> VND").show();
						 $('#errMsgTop').html('Số tiền nhập vào lớn hơn số tiền còn nợ của KH .Số tiền còn nợ của KH <b>' + $('#shortCode').val().toUpperCase() + '</b> là: '+ formatCurrency(parseInt(data.totalIsReceived), 10) +' VNĐ').show();
						 $('#afterPay').val('');
						 $('#valuePay').focus();
						} else if (warning == 2) {
							if (data.status != null && data.status == 2) {
								$('#warning2').html('Số tiền còn phải chi: ' + formatCurrency(parseInt(data.ttt), 10) + ' VNĐ').show();
							} else {
								$('#warning2').html('Số tiền còn nợ của KH <b>' + $('#shortCode').val().toUpperCase() + '</b> là: ' + formatCurrency(parseInt(data.ttt), 10) + ' VNĐ').show();	
							}
							$('#btnAuto').show();
							myFun();
						} else if (warning == 3) {
	 						$('#warning3').html('Nợ của KH: <b> ' + $('#shortCode').val().toUpperCase() + ' </b> với NPP đã được thanh toán hết.').show();
							$('#afterPay').val('');
							myFun();
						}
					}
					isFirstLoad = false;
				}

				$('#dg').datagrid('unselectAll');
				$('.datagrid-header-rownumber').html('STT');
			   	$('.datagrid-header-row td div').css('text-align','center');
			   	//updateRownumWidthForJqGrid('.easyui-dialog');
		   	 	$(window).resize();
			}
		});
	},
	
	/**
	 * lay cong no thanh toan
	 * @author trietptm
	 * @since Dec 25, 2015
	 */
	getCurrentDebit: function() {
		$('.ErrorMsgStyle').hide();
		$('.SuccessMsgStyle').hide();
		var msg = Utils.getMessageOfRequireCheck('shop', jsp_common_shop_name_lable, true);
		var fromDate = $('#fromDate').val().trim();
		var toDate = $('#toDate').val().trim();
		if (fromDate == '__/__/____') {
			$('#fromDate').val('');
		}
		if (toDate == '__/__/____') {
			$('#toDate').val('');
		}
		if (msg.length == 0) {
			msg = Utils.getMessageOfRequireCheck('shortCode','Khách hàng');
		}		
		if (msg.length == 0) {
			msg = Utils.getMessageOfSpecialCharactersValidate('shortCode','Khách hàng', Utils._CODE);
		}
		//
		if (msg.length == 0) {
			msg = Utils.getMessageOfRequireCheck('status','Loại chứng từ', true);
		}
		if (msg.length == 0 && fromDate != null && fromDate.length > 0) {
			msg = Utils.getMessageOfInvalidFormatDate('fromDate', 'Từ ngày');
		}
		
		if (msg.length == 0 && toDate != null && toDate.length > 0) {
			msg = Utils.getMessageOfInvalidFormatDate('toDate', 'Đến ngày');
		}
		
		if (msg.length == 0 && fromDate != null && fromDate.length > 0 && toDate != null && toDate.length > 0 && !Utils.compareDate(fromDate, toDate)) {
			msg = msgErr_fromdate_greater_todate;
			$('#fromDate').focus();
		}
		
		if (msg.length > 0) {
//			$('#errMsgTop').html(msg).show();
			return false;
		}
		if (_isFullPrivilege || _MapControl.get("btnExportPOAutoReport") == 2) {
			$('#btnAuto').show();
		}
		var param = new Object();
		param.shopCode = $('#shop').combobox('getValue').trim();
		param.shortCode = $('#shortCode').val().trim();
		param.status = $('#status').val().trim();
		param.fromDate = $('#fromDate').val().trim();
		param.toDate = $('#toDate').val().trim();
		param.staffDeliverCode = $('#transferStaff').combobox('getValue').trim();
		param.staffPaymentCode = $('#crashierStaff').combobox('getValue').trim();
		$.ajax({
			type: "POST",
			url: '/customerdebit/auto/getcurrentDebit',
			data: ($.param(param, true)),
			dataType: "json",
			success: function(data) {
				if (!data.error) {
					$("#beforePay").val(formatCurrency(data.currentDebit));
					$("#afterPay").val("");
				} else {
					$('#errMsg').html(data.errMsg).show();
				}
			}
		});
		return false;
	}
};
/*
 * END OF FILE - /web/web/resources/scripts/business/customerdebit/vnm.customerdebit-auto.js
 */

/*
 * START OF FILE - /web/web/resources/scripts/business/customerdebit/vnm.customerdebit-delsmalldebit.js
 */
var DelSmallDebit = {
	_isLoad: true,
	mapChoose:null,
	getList : function() {
		$('.ErrorMsgStyle').html('').hide();
		var msg = Utils.getMessageOfRequireCheck('shop', jsp_common_shop_name_lable, true);
		var fromDate = $('#fromDate').val().trim();
		var toDate = $('#toDate').val().trim();
		if (fromDate == '__/__/____') {
			$('#fromDate').val('');
			fromDate = '';
		}
		if (toDate == '__/__/____') {
			$('#toDate').val('');
			toDate = '';
		}
		msg = Utils.getMessageOfRequireCheck('fromMoney','Nợ từ (VNĐ)', undefined, true);
		if (msg.length == 0) {
			msg = Utils.getMessageOfRequireCheck('toMoney','Đến (VNĐ)', undefined, true);
		}
		if (msg.length == 0) {
			msg = Utils.getMessageOfInvaildNumber('fromMoney', 'Nợ từ (VNĐ)');
		}
		if (msg.length == 0) {
			msg = Utils.getMessageOfInvaildNumber('toMoney', 'Đến (VNĐ)');
		}
		var fromValue = Utils.returnMoneyValue($('#fromMoney').val().trim());
		var toValue = Utils.returnMoneyValue($('#toMoney').val().trim());
		if (fromValue.length > 20 || toValue.length > 20) {
			msg = 'Giá trị Số tiền nhập vào chỉ giới hạn 20 ký tự';
		}
		if (msg.length == 0) {
			objectName = 'Nợ từ (VNĐ)';
			if ($('#fromMoney').val().trim() != '' && $('#fromMoney').val().trim().length > 0) {
				var msg = Utils.getMessageOfSpecialCharactersValidateEx('fromMoney', objectName, Utils._TF_NUMBER_COMMA_AND_DOT);
				if(msg.length == 0 && $('#fromMoney').val().replace(/,/g,'') < 0) {
					$('#fromMoney').focus();
					msg = objectName + ' phải lớn hơn hoặc bằng 0';
				}
			}
		}
		if (msg.length == 0) {
			msg = Utils.getMessageOfNegativeNumberCheck('toMoney', 'Đến (VNĐ)');
		}
		
		if (msg.length == 0 && parseInt(fromValue) > parseInt(toValue)) {
			msg = 'Nợ từ phải nhỏ hơn Nợ đến.';
			$('#fromMoney').focus();
		}
		if (msg.length == 0 && parseInt(toValue) > 9999999999999) {		
			msg = 'Nợ đến có giá trị quá lớn';
			$('#toMoney').focus();
		}
		
		if (msg.length == 0 && fromDate.length > 0) {
			msg = Utils.getMessageOfInvalidFormatDate('fromDate', 'Từ ngày');
		}
		if (msg.length ==0 && toDate.length > 0) {
			msg = Utils.getMessageOfInvalidFormatDate('toDate', 'Đến ngày');
		}
		if (msg.length == 0 && fromDate.length > 0 && toDate.length > 0 && !Utils.compareDate(fromDate, toDate)) {
			msg = msgErr_fromdate_greater_todate;
			$('#fromDate').focus();
		}
		if (msg.length > 0) {
			$('#errMsg').html(msg).show();
			return false;
		}
		var data = new Object();
		data.shopCode = $('#shop').combobox('getValue');
		data.orderNumber = $('#orderNumber').val().trim();
		data.shortCode = $('#shortCode').val().trim();
		data.fromMoney = fromValue;
		data.toMoney = toValue;
		data.fromDate = fromDate;
		data.toDate = toDate;
		data.staffDeliverCode = $('#transferStaff').combobox('getValue').trim();
		data.staffPaymentCode = $('#crashierStaff').combobox('getValue').trim();
		data.staffCode = $('#staffCode').combobox('getValue').trim();

		$('#dg').datagrid('load',data);
		return false;
	},
	removeDebit: function() {
		$('.ErrorMsgStyle').html('').hide();
		var data = new Object();
		data.shopCode = $('#shop').combobox('getValue');
		data.shortCode = $('#shortCode').val().trim();
		var lstDebitId = new Array();
		var lstDebitAmt = new Array();
		var lstDebitId1 = new Array();
		var lstDebitAmt1 = new Array();
				
		for(var i = 0; i < DelSmallDebit.mapChoose.keyArray.length; ++i){
			var remain = DelSmallDebit.mapChoose.valArray[i].remain;
			if (parseInt(remain) > 0) {
				lstDebitId.push(DelSmallDebit.mapChoose.keyArray[i]);
				lstDebitAmt.push(DelSmallDebit.mapChoose.valArray[i].remain);
			} else {
				lstDebitId1.push(DelSmallDebit.mapChoose.keyArray[i]);
				lstDebitAmt1.push(DelSmallDebit.mapChoose.valArray[i].remain);
			}
		}		
		data.lstDebitId = lstDebitId;
		data.lstDebitAmt = lstDebitAmt;
		data.lstDebitId1 = lstDebitId1;
		data.lstDebitAmt1 = lstDebitAmt1;
		if (data.lstDebitId.length == 0 && data.lstDebitId1.length == 0) {
			$('#errMsg2').html('Bạn chưa chọn đơn hàng nào.').show();
			return;
		}
		Utils.addOrSaveData(data, "/customerdebit/delsmalldebit/removedebit",null, 'errMsg2', function(data) {
			if (data.error) {
				  $('#errMsg2').html(data.errMsg).show();
			} else {
				DelSmallDebit.getList();
			}
		});
		return false;
	},
	
	/**
	 * xu ly khi chon shop
	 * @author trietptm
	 * @since Dec 23, 2015
	 */
	changeShop: function() {
		$('#divOverlay').show();
		var param = new Object() ;
		param.shopCode = $('#shop').combobox('getValue');
		$.ajax({
			type : "POST",
			url: '/customerdebit/delsmalldebit/change-shop',
			data :($.param(param, true)),
			dataType : "json",
			success : function(data) {
				if  (data != undefined && data != null) {
					$('.ErrorMsgStyle').hide();
					var lstSale = data.lstSale;
					var lstDeliver = data.lstDeliver;
					var lstCashier = data.lstCashier;
					
					if (lstSale != undefined && lstSale != null && lstSale.length > 0) {
						Utils.bindStaffCbx('#staffCode', lstSale, null, 206);
					}
					if (lstDeliver != undefined && lstDeliver != null && lstDeliver.length > 0) {
						Utils.bindStaffCbx('#transferStaff', lstDeliver, null, 206);
					}
					if (lstCashier != undefined && lstCashier != null && lstCashier.length > 0) {
						Utils.bindStaffCbx('#crashierStaff', lstCashier, null, 206);
					}
					if (DelSmallDebit._isLoad) {
						DelSmallDebit.initGrid();
						DelSmallDebit._isLoad = false;
					}
					$('#divOverlay').hide();
				}
			}
		});
	},
	
	/**
	 * khoi tao grid
	 * @author trietptm
	 * @since Dec 19, 2015
	 */
	initGrid: function() {
		$('#dg').datagrid({  
 	 		url:'/customerdebit/delsmalldebit/getinfo',		    	      
//  	 	    width: $(window).width() -55,
 	 	  	width: ($('#dg').width() - 20),
 	 	    height: 350,
 	 	    rownumbers: true,
 	 	  	//scrollbarSize: 0,
 	 	    fitColumns: true,
 	 	    queryParams: {},
 	 	    columns: [[
 	 			{field: 'shortCode', title: 'Mã KH', resizable: false, width: 60, align: 'left', formatter: function(value, row, index) {
 			    	return Utils.XSSEncode(value);
 				}},
 	 			{field: 'customerName', title: 'Tên KH', resizable: false, width:120, align: 'left', formatter: function(value, row, index) {
 			    	return Utils.XSSEncode(value);
 				}},
 	 			{field: 'orderNumber', title: 'Số đơn hàng/<br/>Số phiếu điều chỉnh công nợ', resizable: false, width: 100, align: 'left', formatter: function(value, row, index) {
 			    	return Utils.XSSEncode(value);
 				}},
 	 			{field: 'orderDate', title: 'Ngày', width: 60, resizable: false, align: 'center', formatter: CommonFormatter.dateTimeFormatter},
 	 			{field: 'nvbhCodeName', title:'NVBH', resizable: false, width: 130, align: 'left', formatter: function(value, row, index) {
 			    	return Utils.XSSEncode(value);
 				}},
 	 			{field: 'nvghCodeName', title: 'NVGH', resizable: false, width: 130, align: 'left', formatter: function(value, row, index) {
 			    	return Utils.XSSEncode(value);
 				}},
 	 			{field: 'nvttCodeName', title: 'NVTT', resizable: false, width: 130, align: 'left', formatter: function(value, row, index) {
 			    	return Utils.XSSEncode(value);
 				}},
 	 			{field: 'remain', title: 'Số nợ', width: 100, resizable: false, align: 'right', formatter: CommonFormatter.numberFormatter},
 	 			{field: 'debitId', checkbox: true}
 	 		]],
 	 		onCheck: function(rowIndex, rowData) {
 	 			DelSmallDebit.mapChoose.put(rowData.debitId, rowData);
 	 		},
 	 		onUncheck: function(rowIndex, rowData) {
 	 			DelSmallDebit.mapChoose.remove(rowData.debitId);
 	 		},
 	 		onCheckAll: function(rows) {
 	 			for(var i = 0; i < rows.length; i++) {
 	 				var rowData = rows[i];
 	 				DelSmallDebit.mapChoose.put(rowData.debitId, rowData);
 	 			}
 	 		},
 	 		onUncheckAll: function(rows) {
 	 			for(var i = 0; i < rows.length; i++) {
 	 				var rowData = rows[i];
 	 				DelSmallDebit.mapChoose.remove(rowData.debitId);
 	 			}
 	 		},
 	 		onLoadSuccess :function(data) {
 	 			if (data.rows.length > 0) {
 	 				$('#btnRemoveDebit').show();
 	 			}
 	 			DelSmallDebit.mapChoose = new Map();
 	 			$('#dg').datagrid('unselectAll');
 	 			$('.datagrid-header-rownumber').html('STT');
 	 			$('#dg').datagrid('resize');
		      	$('.datagrid-htable .datagrid-cell').addClass('GridHeaderLargeHeight');
 	 		   	$('.datagrid-header-row td div').css('text-align','center');		   	
 	 		}
 	 	});
	}
};
/*
 * END OF FILE - /web/web/resources/scripts/business/customerdebit/vnm.customerdebit-delsmalldebit.js
 */

/*
 * START OF FILE - /web/web/resources/scripts/business/customerdebit/vnm.customerdebitshow.js
 */
/**
 * 
 */
var ShowDebit = {
	_shortCode:'',
	_fromDateS:'',
	_toDateS:'',
	_status:-1,
	_staffDeliverCode: '',
	_staffPaymentCode: '',
	_staffCode: '',
	_isLoad: true,
	totalAmount:0,
	totalAmountPay:0,
	totalAmountRemain:0,
	amountRemain:0,
	amountRemainMap:null,
	statusPay:'', /** vuongmq; 05/06/2015; tranh khong bam nut Xem ma bam export se loi he thong*/
	lstOrderNumbers:'',
	getList: function() {
		$('.ErrorMsgStyle').html('').hide();
		var msg = Utils.getMessageOfRequireCheck('shop', jsp_common_shop_name_lable, true);
		if (msg.length == 0 && $('#shortCode').val().trim() > 0 ) {
			msg = Utils.getMessageOfSpecialCharactersValidate('shortCode','Mã KH', Utils._CODE);
		}				
		if (msg.length > 0) {
			$('#errMsg').html(msg).show();
			return false;
		}	
		var data = new Object();
		data.shopCode = $('#shop').combobox('getValue');
		data.shortCode = $('#shortCode').val().trim();
		data.fromDateS = $('#fromDate').val().trim();
		data.toDateS = $('#toDate').val().trim();
		data.status =  $('#status').val();
		data.staffDeliverCode = $('#transferStaff').combobox('getValue').trim();
		data.staffPaymentCode = $('#crashierStaff').combobox('getValue').trim();
		data.staffCode = $('#staffCode').combobox('getValue').trim();
		if ($('#cboStatusPay').val() != null && $('#cboStatusPay').val() != undefined) {
			data.statusPay = $('#cboStatusPay').val().join(",");
		} else {
			data.statusPay = "";
		}
		/** vuongmq; 05/06/2015; xem danh sach don hang nhieu*/
		data.lstOrderNumbers = $("#orderNumber").val().trim();

		ShowDebit._shortCode = data.shortCode;
		ShowDebit._fromDateS = data.fromDateS;
		ShowDebit._toDateS = data.toDateS;
		ShowDebit._status = data.status;
		ShowDebit._staffDeliverCode = data.staffDeliverCode;
		ShowDebit._staffPaymentCode = data.staffPaymentCode;
		ShowDebit._staffCode = data.staffCode;
		ShowDebit.totalAmount = 0;
		ShowDebit.totalAmountPay = 0;
		ShowDebit.totalAmountRemain = 0;
		ShowDebit.amountRemain = 0;
		ShowDebit.amountRemainMap = null;
		ShowDebit.statusPay = data.statusPay;
		ShowDebit.lstOrderNumbers = data.lstOrderNumbers;
		$('#dg').datagrid('load', data);
	},
	exportExcel: function() {
		$('.ErrorMsgStyle').html('').hide();
		if ($('#dg').datagrid ('getRows').length == 0) {
			msg = 'Không có dữ liệu để xuất!';
			$('#errMsg2').html(msg).show();	
			return false;
		}
		var params = new Object();
		params.shopCode = $('#shopCode').val();
		params.shortCode = ShowDebit._shortCode;
		params.fromDateS = ShowDebit._fromDateS;
		params.toDateS = ShowDebit._toDateS;	
		params.status = ShowDebit._status;
		params.staffDeliverCode = ShowDebit._staffDeliverCode;
		params.staffPaymentCode = ShowDebit._staffPaymentCode;
		params.staffCode = ShowDebit._staffCode;
		params.statusPay = ShowDebit.statusPay;
		params.lstOrderNumbers = ShowDebit.lstOrderNumbers;
		try {
			params.reportCode = $('#function_code').val();
		} catch(e) {
			// Oop! Error
		}
		var kData = $.param(params,true);
		$('#divOverlay').show();		
		$.ajax({
			type : "POST",
			url : "/customerdebit/showdebit/exportdebitcustomer",
			data :(kData),
			dataType: "json",
			success : function(data) {
				$('#divOverlay').hide();								
				if (data != undefined && data != null && !data.error) {
					var filePath = ReportUtils.buildReportFilePath(data.view);
					window.location.href = filePath;
				} else {
					$('#errMsg2').html(Utils.XSSEncode(data.errMsg)).show();					
				}
			}
		});	
		return false;
	},
	
	/**
	 * xu ly khi chon shop
	 * @author trietptm
	 * @since Dec 19, 2015
	 */
	changeShop: function() {
		$('#divOverlay').show();
		var par = new Object() ;
		par.shopCode = $('#shop').combobox('getValue');
		$.ajax({
			type : "POST",
			url: '/customerdebit/showdebit/change-shop',
			data :($.param(par, true)),
			dataType : "json",
			success : function(data) {
				if  (data != undefined && data != null) {
					$('.ErrorMsgStyle').hide();
					var lstSale = data.lstSale;
					var lstDeliver = data.lstDeliver;
					var lstCashier = data.lstCashier;
					
					if (lstSale != undefined && lstSale != null && lstSale.length > 0) {
						Utils.bindStaffCbx('#staffCode', lstSale, null, 206);
					}
					if (lstDeliver != undefined && lstDeliver != null && lstDeliver.length > 0) {
						Utils.bindStaffCbx('#transferStaff', lstDeliver, null, 206);
					}
					if (lstCashier != undefined && lstCashier != null && lstCashier.length > 0) {
						Utils.bindStaffCbx('#crashierStaff', lstCashier, null, 206);
					}
					if (data.fromDate != undefined && data.fromDate != null) {
						$('#fromDate').val(data.fromDate);
					}
					if (data.toDate != undefined && data.toDate != null) {
						$('#toDate').val(data.toDate);
					}
					if (ShowDebit._isLoad) {
						ShowDebit.initGrid();
						ShowDebit._isLoad = false;
					}
					$('#divOverlay').hide();
				}
			}
		});
	},
	
	/**
	 * khoi tao grid
	 * @author trietptm
	 * @since Dec 19, 2015
	 */
	initGrid: function() {
		var params = new Object();
		params.shopCode = $('#shop').combobox('getValue');
		params.fromDateS = $('#fromDate').val().trim();
		params.toDateS = $('#toDate').val().trim();
		params.status =  $('#status').val();
		ShowDebit._fromDateS = params.fromDateS;
		ShowDebit._toDateS = params.toDateS;
		ShowDebit._status = params.status;
		$('#dg').datagrid({
			url:'/customerdebit/showdebit/getinfo',		    	      
			width: $(window).width() - 50,
			//height: 350,
			rownumbers: true,	
			singleSelect: true,
			queryParams: (params),
			pagination: true,
			//showFooter:true,
			frozenColumns: [[
			    {field:'orderNumber', title:'Số đơn hàng/<br/>Số phiếu điều chỉnh công nợ', width: 130, resizable: false, align: 'left', formatter: function(v, r, i) {
					return Utils.XSSEncode(v);
				}},
				{field: 'shortCode', title: 'Khách hàng', width: 150, resizable: false, align:'left', formatter: function(value, row, index) {
					if (row.isFooter) {
						return '';
					}
					return Utils.XSSEncode(row.shortCode + ' - ' + row.customerName);
				}},  
				{field: 'address', title: 'Địa chỉ', width: 250, resizable: false ,align: 'left', formatter: function(v, r, i) {
					return Utils.XSSEncode(v);
				}}
			]],
			columns: [[				
				{field: 'createDate', title: 'Ngày nợ', width: 100, resizable: false, align: 'center', formatter: CommonFormatter.dateTimeFormatter},
				{field: 'payDate', title: 'Ngày phải trả', width: 100, align: 'center', resizable: false, formatter: CommonFormatter.dateTimeFormatter},
				{field: 'staffName', title: 'NVBH', width: 150, resizable:false, align: 'left', formatter: function(v, r, i) {
					return Utils.XSSEncode(v);
				}},
				{field: 'deliveryName', title: 'NVGH', width: 150, resizable: false, align: 'left', formatter: function(v, r, i) {
					return Utils.XSSEncode(v);
				}},
				{field: 'cashierName', title: 'NVTT', width: 150, resizable: false, align: 'left', formatter: function(v, r, i) {
					return Utils.XSSEncode(v);
				}},
				{field: 'totalAmount', title: 'Số tiền', width: 100, resizable: false , align:'right', formatter: function(value, row, index) {
					if (value != undefined && value != null) {
						if (row.isFooter) {
							return '<b>' + formatCurrencyInterger(ShowDebit.totalAmount) + '</b>';
						} else {
							ShowDebit.totalAmount += Number(value);
							return formatCurrencyInterger(value);							
						}
					}
					return 0;
				}},
				{field: 'totalAmountPay', title: 'Đã trả', width: 100, resizable: false, align: 'right', formatter: function(value, row, index) {
					if (value != undefined && value != null) {
						if (row.isFooter) {
							return '<b>' + formatCurrencyInterger(ShowDebit.totalAmountPay) + '</b>';
						} else {
							ShowDebit.totalAmountPay += Number(value);
							return formatCurrencyInterger(value);
						}
					}
					return 0;
				}},
				{field: 'discountAmount', title:'Chiết khấu', width:100, resizable:false, align:'right', formatter:function(value, row, index) {
					if (value != undefined && value != null) {
						if (row.isFooter) {
							return '';
						} else {
							return formatCurrency(Math.abs(value));
						}
					}
					return 0;
				}},
				{field:'totalAmountRemain', title:'Còn nợ', width:100, resizable:false, align:'right', formatter:function(value, row, index) {
					if (value != undefined && value != null){
						if (row.isFooter) {
							return '<b>' + formatCurrencyInterger(ShowDebit.totalAmountRemain) + '</b>';
						} else {
							ShowDebit.totalAmountRemain += Number(value);
							return formatCurrencyInterger(value);							
						}
					} 
					return 0;
				}},
				{field:'payType', title:'Loại', width:($(window).width() - 50 - 100 - 200 - 100 - 100 - 100 - 180 - 100 - 100 - 100-100) < 100 ? 100 : ($(window).width() - 50 - 100 - 200 - 100 - 100 - 100 - 180 - 100 - 100 - 100 -100), resizable:false, align:'left', formatter:function(value, row, index){
					if (row.totalAmountRemain == 0) {
						return "Đã tất toán";
					}
					if (row.payDate == null) {
						return (row.isFooter ? "" : "Chưa tới hạn");
					}
					var payDate = CommonFormatter.dateTimeFormatter(row.payDate);
					if (Utils.compareCurrentDateExReturn(getCurrentDate(),payDate) > 0) {
						return 'Quá hạn';
					} else if (Utils.compareCurrentDateExReturn(getCurrentDate(), payDate) == 0) {
						return 'Tới hạn';
					} else if (Utils.compareCurrentDateExReturn(getCurrentDate(), payDate) < 0) {
						return 'Chưa tới hạn';
					}
				}}
			]],
			rowStyler: function(index, row) {
				if (row.totalAmountRemain == 0) {
					return "color:#00F;";
				}
				if (row.payDate != null) {
					var payDate = CommonFormatter.dateTimeFormatter(row.payDate);
					if (Utils.compareCurrentDateExReturn(getCurrentDate(), payDate) > 0) {
						return 'color:red;font-weight:bold;'; //'Qúa hạn';
					} else if (Utils.compareCurrentDateExReturn(getCurrentDate(), payDate) == 0) {
						return 'color:#FFC000;font-weight:bold;'; //'Tới hạn';
					}	
				}
            },  
			onLoadSuccess: function(data) {
				$('.datagrid-header-rownumber').html('STT');
			   	$('.datagrid-header-row td div').css('text-align','center');		   	
			   	updateRownumWidthForJqGrid('.easyui-dialog');
		   	 	$(window).resize();
		   	 	ShowDebit.amountRemain = 0;
		   	 	if (ShowDebit.amountRemainMap != null) {
			   	 	for (var i = 0; i < ShowDebit.amountRemainMap.size(); i++) {
			   	 		ShowDebit.amountRemain += Number(ShowDebit.amountRemainMap.valArray[i]);
			   	 	}		   	 		
		   	 	}
			}
		});
	}
};
/*
 * END OF FILE - /web/web/resources/scripts/business/customerdebit/vnm.customerdebitshow.js
 */

/*
 * START OF FILE - /web/web/resources/scripts/business/customerdebit/vnm.debit-increment.js
 */
var DebitIncrement = {
		_xhrSave: null,
		
		saveDebit: function() {
			$('.ErrorMsgStyle').hide();
			var msg = "";
			
			msg = Utils.getMessageOfRequireCheck('debitType', "Loại phiếu", true);
			
			if (msg.length == 0) {
				msg = Utils.getMessageOfRequireCheck('type', "Đối tượng", true);
			}
			var type = $('#type').val().trim();
			
			if (msg.length == 0) {
				msg = Utils.getMessageOfRequireCheck('dbNumber', "Số phiếu");
			}
			
			if (msg.length == 0) {
				msg = Utils.getMessageOfSpecialCharactersValidate('dbNumber', "Số phiếu", Utils._CODE);
			}
			
			if (msg.length == 0) {
				msg = Utils.getMessageOfRequireCheck('amount', "Số tiền");
			}
			var amount = $('#amount').val().trim();
			amount = amount.replace(/,/g, '');
			if (msg.length == 0 &&
					(amount.length == 0 || isNaN(amount))) {
				msg = "Số tiền phải là số.";
				$('#amount').focus();
			}
			if (msg.length == 0 && amount <= 0) {
				msg = "Số tiền phải lớn hơn 0.";
				$('#amount').focus();
			}
			
			if (msg.length == 0) {
				msg = Utils.getMessageOfRequireCheck('reason', "Lý do");
			}
			
			if (msg.length == 0 && type == '1') {
				msg = Utils.getMessageOfRequireCheck('shortCode', "Khách hàng");
			}
			
			if (msg.length > 0) {
				$('#errMsg').html(msg).show();
				return false;
			}
			
			var dataModel = new Object();
			dataModel.dbNumber = $('#dbNumber').val().trim();
			dataModel.type = type;
			dataModel.debitType = $('#debitType').val().trim();
			dataModel.amount = amount;
			dataModel.shortCode = $('#shortCode').val().trim();
			dataModel.reason = $('#reason').val().trim();
			$.messager.confirm("Xác nhận", "Bạn có muốn lưu thông tin này", function(r) {
				if (r) {
					Utils.saveData(dataModel, "/debit/increment/save-debit", DebitIncrement._xhrSave, 'errMsg',
							function() {
						setTimeout(function() {
							window.location.reload();
						}, 3000);
					}, "loading2");
				}
			});
			return false;
		},
		
		typeChange: function() {
			var type = $('#type').val().trim();
			if (type == '1') {
				$('#shortCode').removeAttr("disabled");
				$('#shortCode').focus();
			} else {
				$('#shortCode').attr("disabled", "disabled");
				$('#shortCode').val('');
			}
		}
};

function DebitAdjustmentObj() {
		this.debitType = null;
		//this.type = null;
		this.dbNumber = null;
		this.amount = null;
		this.reason = null;
		this.shortCode = null;
}
function ShopDebitAdjustmentObj() {
	this.debitType = null;
	this.dbNumber = null;
	this.amount = null;
	this.reason = null;
}
var DebitAdjustment = {
		_xhrSave: null,
		_lstTypes: ["", "KH - NPP", "NPP - Công ty"],
		_lstDebitTypes: ["", "Phiếu tăng nợ", "Phiếu giảm nợ"],
		_colNames: ["Loại phiếu", "Số phiếu", "Số tiền", "Lý do", "Khách hàng (F9)"],
		
		initGrid: function() {
			$('#gridContainer').html('<table id="dgrid"></table>');
			
			var w = $("#gridContainer").width() - 60;
			var lst1 = [];
			for (var i = 0; i < 30; i++) {
				lst1.push(new DebitAdjustmentObj());
			}
			$('#dgrid').handsontable({
				data: lst1,
				colWidths: [w/6, w/6, w/6, w/3, w/6-27],
				outsideClickDeselects: true,
				multiSelect : false,
				rowHeaders: true,
				manualColumnResize: true,
				fixedRowsTop: 0,
				width: w+70,
				colHeaders: DebitAdjustment._colNames,
				columns: [
					{data: "debitType", allowInvalid: false, type: "dropdown", source: DebitAdjustment._lstDebitTypes},
				    //{data: "type", allowInvalid: false, type: "dropdown", source: DebitAdjustment._lstTypes},
				    {data: "dbNumber"},
				    {data: "amount", type: "numeric", format: "#,##0"/*, allowInvalid: false */},
				    {data: "reason"},
				    {data: "shortCode"}
				],
				beforeKeyDown: function(event){
					$(".ErrorMsgStyle").hide();
					var sl = $('#dgrid').handsontable('getSelected');
					var r = sl[0];
					var c = sl[1];
					if (c ==  1 || c == 4) {
						$(".handsontableInputHolder:not(:hidden) .handsontableInput").attr("maxlength", 40);
					} else if (c == 2) {
						$(".handsontableInputHolder:not(:hidden) .handsontableInput").attr("maxlength", 15);
					} else if (c == 3) {
						$(".handsontableInputHolder:not(:hidden) .handsontableInput").attr("maxlength", 250);
					} else {
						$(".handsontableInputHolder:not(:hidden) .handsontableInput").attr("maxlength", 15);
					}
					var keyCode = event.keyCode;
					if (keyCode == keyCodes.TAB && !event.shiftKey) {
						var htGrid = $('#dgrid').handsontable('getInstance');
						var sl = htGrid.getSelected();
						var r = sl[0];
						var c = sl[1];
						var data = htGrid.getData();
						if (c == 4) {
							event.stopImmediatePropagation();
							event.preventDefault();
							if (r == data.length - 1) {
								for (var i = 0; i < 30; i++) {
									data.push(new DebitAdjustmentObj());
								}
								htGrid.render();
							}
							htGrid.selectCell(r + 1, 0);
						}
					}
					else if (keyCode == keyCode_F9) {
						var htGrid = $('#dgrid').handsontable('getInstance');
						var sl = htGrid.getSelected();
						var r = sl[0];
						var c = sl[1];
						if (c == 4) {
							VCommonJS.showDialogSearch2({
							    inputs : [
							        {id:'code', maxlength:50, label:'Mã KH'},
							        {id:'name', maxlength:250, label:'Tên KH'},
							        {id:'address', maxlength:250, label:'Địa chỉ', width:410}
							    ],
							    url : '/commons/customer-in-shop/search?shopCode='+$('#shopCode').val().trim(),
							    onShow : function() {
						        	$('.easyui-dialog #code').focus();
							    },
							    columns : [[
							        {field:'shortCode', title:'Mã KH', align:'left', width: 110, sortable:false, resizable:false, formatter: function(v, r, i){
							        	if (v) {
							        		return Utils.XSSEncode(v);
							        	}
							        	return "";
							        }},
							        {field:'customerName', title:'Tên KH', align:'left', width: 150, sortable:false, resizable:false, formatter: function(v, r, i){
							        	if (v) {
							        		return Utils.XSSEncode(v);
							        	}
							        	return "";
							        }},
							        {field:'address', title:'Địa chỉ', align:'left', width: 170, sortable:false, resizable:false, formatter: function(v, r, i){
							        	if (v) {
							        		return Utils.XSSEncode(v);
							        	}
							        	return "";
							        }},
							        {field:'choose', title:'', align:'center', width:40, sortable:false, resizable:false, formatter:function(value,row,index) {
							        	return "<a href='javascript:void(0)' onclick='chooseCustomer(\""+ Utils.XSSEncode(row.shortCode) + "\","+r+","+c+");'>Chọn</a>";        
							        }}
							    ]]
							});
						}
					}
				},
				afterChange: function(d, s) {
					if (d != null) {
						var sl = d[0];
						if (sl[1] == "amount") {
							var r = sl[0];
							var v = sl[3];
							var htGrid = $('#dgrid').handsontable('getInstance');
							//var meta = htGrid.getCellMeta(r, 4);
							//meta.readOnly = false;
							if (v && (isNaN(v) || Number(v) <= 0 || Math.floor(Number(v)) - Number(v) != 0)) {
								if (Math.floor(Number(v)) - Number(v) != 0) {
									htGrid.setDataAtCell(r,2,null);
								}
								htGrid.selectCell(r, 2);
								$("#errMsg").html("Số tiền phải là số nguyên dương").show();
								htGrid.render();
							} else if (v && (v.toString().length > 15 || Number(v) > 999999999999999)) {
								//htGrid.setDataAtCell(r,2,null);
								htGrid.selectCell(r, 2);
								$("#errMsg").html("Số tiền chỉ được nhập tối đa 15 chữ số").show();
								htGrid.render();
							}
						}
					}
				}
			});
			$('#dgrid').handsontable('selectCell', 0, 0);
			
			$("#dgrid thead th").css({"background-color":"#56ABC7", "color":"#fff", "font-weight":"bold"});
		},
		saveDebit: function() {
			$(".ErrorMsgStyle").hide();
			
			var htGrid = $("#dgrid").handsontable("getInstance");
			var rows = htGrid.getData();
			if (rows == null || rows.length == 0) {
				$("#errMsg").html("Không có dòng dữ liệu nào để lưu").show();
				return;
			}
			
			var lstDebit = [];
			var dbObj = null;
			var row = null;
			var j = 0;
			var t = 0;
			for (var i = 0, sz = rows.length; i < sz; i++) {
				row = rows[i];
				
				if (DebitAdjustment.isNullOrEmptyRow(row)) {
					continue;
				}
				
				j = 0;
				for (var p in row) {
					if (DebitAdjustment.isNullOrEmptyCell(row[p], p == "amount")) {
						$("#errMsg").html("Bạn chưa nhập giá trị cho trường " + DebitAdjustment._colNames[j].replace(" (F9)", "")).show();
						htGrid.selectCell(i, j);
						return;
					}
					j++;
				}
				
				if (DebitAdjustment._lstDebitTypes.indexOf(row["debitType"]) < 0) {
					$("#errMsg").html("Loại phiếu không hợp lệ").show();
					htGrid.selectCell(i, 0);
					return;
				}
				
				if(!/^[0-9a-zA-Z-_.]+$/.test(row["dbNumber"])){
					$("#errMsg").html(format(msgErr_invalid_format_code, "Số phiếu")).show();
					htGrid.selectCell(i, 1);
					return;
				}
				
				if(row["dbNumber"].trim().length > 40){
					$("#errMsg").html("Số phiếu chỉ được tối đa 40 ký tự").show();
					htGrid.selectCell(i, 1);
					return;
				}
				
				for (j = 0; j < i; j++) {
					if (DebitAdjustment.isNullOrEmptyRow(rows[j])) {
						continue;
					}
					if (rows[j].dbNumber.trim() == row.dbNumber.trim()) {
						$("#errMsg").html("Số phiếu bị trùng với dòng " + (j + 1)).show();
						htGrid.selectCell(i, 1);
						return;
					}
				}
				
				var amt = row["amount"];
				if (isNaN(amt) || Number(amt) <= 0 || Math.floor(Number(amt)) - Number(amt) != 0) {
					$("#errMsg").html("Số tiền phải là số nguyên dương").show();
					htGrid.selectCell(i, 2);
					return;
				}

				if (amt <= 0) {
					$("#errMsg").html("Số tiền phải lớn hơn 0").show();
					htGrid.selectCell(i, 2);
					return;
				}
				
				if (amt.toString().length > 15 || amt > 999999999999999) {
					$("#errMsg").html("Số tiền chỉ được nhập tối đa 15 chữ số").show();
					htGrid.selectCell(i, 2);
					return;
				}
				
				if(row["reason"].trim().length > 250){
					$("#errMsg").html("Lý do chỉ được tối đa 250 ký tự").show();
					htGrid.selectCell(i, 3);
					return;
				}
				
				if(!/^[0-9a-zA-Z-_.]+$/.test(row["shortCode"])){
					$("#errMsg").html(format(msgErr_invalid_format_code, "Khách hàng")).show();
					htGrid.selectCell(i, 4);
					return;
				}
				
				dbObj = {
						debitType: DebitAdjustment._lstDebitTypes.indexOf(row["debitType"]),
						//type: 1, // KH - NPP
						dbNumber: row["dbNumber"].trim(),
						amount: row["amount"],
						reason: row["reason"].trim(),
						shortCode: row["shortCode"].trim()
				};
				
				lstDebit.push(dbObj);
			}
			
			if (lstDebit.length == 0) {
				$("#errMsg").html("Không có dòng dữ liệu nào để lưu").show();
				return;
			}
			
			var dataModel = {type:1, lstDebit:lstDebit, shopCode: $('#shopCode').val().trim()};
			//dataModel[JSONUtil._VAR_NAME]= "debitObject";
			
			JSONUtil.saveData2(dataModel, "/debit/adjustment/save-debit", "Bạn có muốn lưu thông tin này?",
					"errMsg", function() {
//				setTimeout(function() {
//					DebitAdjustment.initGrid();
//				}, 3000);
			});
			return;
		},
		
		isNullOrEmptyRow: function(row) {
			var s = null;
			for (var p in row) {
				s = row[p];
				if (s != undefined && s != null && ((p == "amount" && s.toString().trim().length > 0) || s.trim().length > 0)) {
					return false;
				}
			};
			return true;
		},
		
		isNullOrEmptyCell: function(s, isNo) {
			if (s == undefined || s == null) {
				return true;
			}
			return ((!isNo && s.trim().length == 0) || (s.toString().trim().length == 0));
		},
		
		_colShopNames: ["Loại phiếu", "Số phiếu", "Số tiền", "Lý do"],
		saveShopDebit: function() {
			$(".ErrorMsgStyle").hide();
			
			var htGrid = $("#dgrid").handsontable("getInstance");
			var rows = htGrid.getData();
			if (rows == null || rows.length == 0) {
				$("#errMsg").html("Không có dòng dữ liệu nào để lưu").show();
				return;
			}
			
			var lstDebit = [];
			var dbObj = null;
			var row = null;
			var j = 0;
			for (var i = 0, sz = rows.length; i < sz; i++) {
				row = rows[i];
				
				if (DebitAdjustment.isNullOrEmptyRow(row)) {
					continue;
				}
				
				j = 0;
				for (var p in row) {
					if (DebitAdjustment.isNullOrEmptyCell(row[p], p == "amount")) {
						$("#errMsg").html("Bạn chưa nhập giá trị cho trường " + Utils.XSSEncode(DebitAdjustment._colShopNames[j])).show();
						htGrid.selectCell(i, j);
						return;
					}
					j++;
				}
				
				if (DebitAdjustment._lstDebitTypes.indexOf(row["debitType"]) < 0) {
					$("#errMsg").html("Loại phiếu không hợp lệ").show();
					htGrid.selectCell(i, 0);
					return;
				}
				
				if(!/^[0-9a-zA-Z-_.]+$/.test(row["dbNumber"])){
					$("#errMsg").html(format(msgErr_invalid_format_code, "Số phiếu")).show();
					htGrid.selectCell(i, 1);
					return;
				}
				
				if(row["dbNumber"].trim().length > 40){
					$("#errMsg").html("Số phiếu chỉ được tối đa 40 ký tự").show();
					htGrid.selectCell(i, 1);
					return;
				}
				
				for (j = 0; j < i; j++) {
					if (DebitAdjustment.isNullOrEmptyRow(rows[j])) {
						continue;
					}
					if (rows[j].dbNumber.trim() == row.dbNumber.trim()) {
						$("#errMsg").html("Số phiếu bị trùng với dòng " + (j + 1)).show();
						htGrid.selectCell(i, 1);
						return;
					}
				}
				
				var amt = row["amount"];
				if (isNaN(amt) || Number(amt) <= 0 || Math.floor(Number(amt)) - Number(amt) != 0) {
					$("#errMsg").html("Số tiền phải là số nguyên dương").show();
					htGrid.selectCell(i, 2);
					return;
				}

				if (amt <= 0) {
					$("#errMsg").html("Số tiền phải lớn hơn 0").show();
					htGrid.selectCell(i, 2);
					return;
				}
				
				if (amt.toString().length > 15 || amt > 999999999999999) {
					$("#errMsg").html("Số tiền chỉ được nhập tối đa 15 chữ số").show();
					htGrid.selectCell(i, 2);
					return;
				}
				
				if(row["reason"].trim().length > 250){
					$("#errMsg").html("Lý do chỉ được tối đa 250 ký tự").show();
					htGrid.selectCell(i, 3);
					return;
				}
				
				dbObj = {
						debitType: DebitAdjustment._lstDebitTypes.indexOf(row["debitType"]),
						//type: 2, // NPP - cong ty
						dbNumber: row["dbNumber"].trim(),
						amount: row["amount"],
						reason: row["reason"].trim()
				};
				
				lstDebit.push(dbObj);
			}
			
			if (lstDebit.length == 0) {
				$("#errMsg").html("Không có dòng dữ liệu nào để lưu").show();
				return;
			}
			
			var dataModel = {type:2, lstDebit:lstDebit, shopCode: $('#shopCode').val().trim()};
			//dataModel[JSONUtil._VAR_NAME]= "debitObject";
			JSONUtil.saveData2(dataModel, "/debit/adjustment/save-debit", "Bạn có muốn lưu thông tin này?",
					"errMsg", function() {
				setTimeout(function() {
					window.location.href = "/debit/adjustment/shop";
				}, 3000);
			});
			return;
		}
};
/*
 * END OF FILE - /web/web/resources/scripts/business/customerdebit/vnm.debit-increment.js
 */

/*
 * START OF FILE - /web/web/resources/scripts/business/customerdebit/vnm.customerdebit-batch-new.js
 */
var ColGrid = {
		isCheck: 0, orderNumber: 1, customer: 2,  orderDate: 3, 
		nvbh: 4, nvgh: 5, nvtt: 6, remain: 8, payDiscount: 9,
		paied: 7, payAmount: 10, amount: 11 , bank: 12
};
var CustomerDebitBatchNew = {
	_xhrSave : null,
	_checkF9 : null,
	_lstOrderCheck:null,
	_lstDebitSelectId:null,
	_curRowId: null,
	_lstNVTT: null,
	_mapNVTT: new Map(),
	_rowIndexValueField: null,
	_arrayList: null,
	_mapRowIndex: null,
	_arrayRow: null,
	_remainMoney:0,
	_totalDebit:0,
	_listDataGrid: null,
	_mapDataGrid:null,
	_type:null,
	_lstBank: null,
	_staffCodeNVTTByChange: 'UNCHECKALL',
	_flagCheckAll: true,
	_isLoad: true,
	styleCellRenderer: function (instance, td, row, col, prop, value, cellProperties) {
		if(col == ColGrid.isCheck){
			Handsontable.renderers.CheckboxRenderer.apply(this, arguments);
			$(td).css({
				  "vertical-align": "middle",
				  "text-align": "center"
		    });
			$(td).attr("idx", row);
		} else if (col == ColGrid.orderNumber || col == ColGrid.customer 
				|| col == ColGrid.orderDate || col == ColGrid.nvbh || col == ColGrid.nvgh) {
			Handsontable.renderers.TextRenderer.apply(this, arguments);
		} else if (col == ColGrid.nvtt) {
			Handsontable.renderers.AutocompleteRenderer.apply(this,arguments);
		} else if (col == ColGrid.remain || col == ColGrid.amount || col == ColGrid.payDiscount || col == ColGrid.payAmount) {
			Handsontable.renderers.NumericRenderer.apply(this,arguments);
		} else if (col == ColGrid.paied) {
			var html = '';
			if (value != null) {
				 html = formatCurrency(Math.abs(value.pay)) + '&nbsp;<span style="color:#f00;">(' + formatCurrency(Math.abs(value.discount)) + ')</span>';
			 }
			td.innerHTML = html;
			$(td).css({textAlign: 'right'});
		} else if (col == ColGrid.bank) {
			Handsontable.renderers.AutocompleteRenderer.apply(this,arguments);
		}
		var rowData = $('#dg').handsontable('getInstance').getDataAtRow(row);
		switch (rowData.status) {
		case 0:
			break;
		case 1:
			if (rowData.isCheck) {
				$(td).css({background: '#86CE9D', color: 'black', fontWeight: 'bold'});
			}
			break;
		case 2:
			if (rowData.isCheck) {
				$(td).css({background: '#E78686', color: 'black', fontWeight: 'bold'});
			}
		}
	},
	initGrid: function() {
		$('#dg').handsontable({
			data: [],
			colWidths: [30, 110, 135, 75, 110, 110, 120, 200, 100, 110, 110, 110,130],
			outsideClickDeselects: true,
			multiSelect : false,
			rowHeaders: true,
			manualColumnResize: true,
			width: $(window).width() - 45,
			colHeaders: function (col) {
	           	switch (col) {
                case 0:
                    var txt = "<input type='checkbox' class='checker' id='cbxCheckAll' onchange='CustomerDebitBatchNew.cbxCheckAllChange();' ";
                    txt += CustomerDebitBatchNew.isCheckedAll() ? 'checked="checked"' : '';
                    txt += " />";
                    return txt;
                case 1:
                    return "Số đơn hàng";
                case 2:
                    return "Khách hàng";
                case 3:
                    return "Ngày";
                case 4:
                    return "NVBH";
                case 5:
                    return "NVGH";
                case 6:
                    return "NVTT";
                case 7:
                    return "Đã trả";
                case 8:
                    return "Còn lại";
                case 9:
                    return "Chiết khấu";
                case 10:
                    return "Giá trị thanh toán";
                case 11:
                	return "Tổng tiền ĐH";
                case 12:
                	return "Ngân hàng";
	           	}
			},
			columns: [
			      {type: 'checkbox', data: 'isCheck'},
			      {readOnly: true, data: 'orderNumber'},
			      {readOnly: true, data: 'customer'},
			      {readOnly: true, data: 'orderDate'},
			      {readOnly: true, data: 'nvbhName'},
			      {readOnly: true, data: 'nvghName'},
			      {readOnly: false, allowInvalid: false, strict: true, type: 'dropdown', source: CustomerDebitBatchNew._mapNVTT.keyArray, data: 'nvttCodeName'},
			      {readOnly: true, data: 'paied'},
			      {readOnly: true, type: 'numeric', format: '0,0', data: 'remain'},
			      {readOnly: false, allowInvalid: false, type: 'numeric', format: '0,0', data: 'payDiscount'},
			      {readOnly: false, allowInvalid: false, type: 'numeric', format: '0,0', data: 'payAmount'},
			      {readOnly: true, type: 'numeric', format: '0,0', data: 'total'},
			      {readOnly: false, allowInvalid: false, strict: true, type: 'dropdown', source: CustomerDebitBatchNew._lstBank, data: 'bankCode'},
			 ],
			 cells: function (row, col, prop) {
				 this.renderer = CustomerDebitBatchNew.styleCellRenderer;
		     },
			 afterChange: function(changes, source) {
				  if (changes != null && $.isArray(changes)) {
					  for (var i = 0; i < changes.length; i++) {
						  	var htGrid = $('#dg').handsontable('getInstance');
							var row = changes[i];
							var r = row[0];
							var c = row[1];
							var oldValue = row[2];
							var newValue = row[3];
							var rowData = htGrid.getDataAtRow(r);
							if (c == 'isCheck') {
								CustomerDebitBatchNew.changeWhenCheck(newValue, rowData, r);
							} else if (c == 'payDiscount') {
								if (!isNullOrEmpty(newValue) && (newValue < 0 || newValue >= rowData.remain)) {
									rowData.status = 2;
								} else {
									if(!isNullOrEmpty(newValue)){
										rowData.status = 1;
										rowData.payAmount = rowData.remain - newValue;
									}
								}
							} else if (c == 'payAmount') {
								//rowData.payDiscount = null;
								if (!isNullOrEmpty(newValue) && newValue >0) {
									if (newValue > rowData.remain - rowData.payDiscount) {
										rowData.status = 2;
										rowData.payAmount = rowData.remain - rowData.payDiscount;
									} else {
										if (rowData.payDiscount == null
												|| (rowData.payDiscount>=0 && rowData.payDiscount <= rowData.remain)) {
											rowData.status = 1;
										} else {
											rowData.status = 2;
										}
									}
								} else {
									rowData.status = 2;
								}
							} else if (c == 'nvttCodeName' && rowData.isCheck) {
								CustomerDebitBatchNew.changeNVTT(newValue, rowData.debitId);
							} else if (c == 'bankCode' && rowData.isCheck) {
								CustomerDebitBatchNew.changeBank(newValue, rowData.debitId);
							}
							htGrid.render();
							CustomerDebitBatchNew.setTotalPay();
					  }
				  }
			  },
			  afterRender:function() {
				  if ($('#cbxCheckAll').prop('checked')) {
					  $('#dg').closest('table').find('input.htCheckboxRendererInput').prop("checked", true);
				  }
			  },
			  beforeKeyDown: function(event) {
				  var htGrid = $('#dg').handsontable('getInstance');
					var rowSelect = htGrid.getSelected();
					if (rowSelect != undefined && rowSelect != null) {
						var r = rowSelect[0];
						var c = rowSelect[1];
						if (c == ColGrid.payDiscount) {
						    if (event.keyCode == keyCodes.TAB) {
								event.stopImmediatePropagation();
								if(event.shiftKey){
									htGrid.selectCell(r,ColGrid.isCheck);
								} else {
									htGrid.selectCell(r,ColGrid.payAmount);
								}
								event.preventDefault();
						    }
						} else if (event.keyCode == keyCodes.DELETE && (c == ColGrid.payDiscount || c == ColGrid.payAmount)) {
							event.stopImmediatePropagation();
							htGrid.setDataAtCell(r,ColGrid.payDiscount,null);
							htGrid.setDataAtCell(r,ColGrid.payAmount,null);
							htGrid.selectCell(r,c == ColGrid.payAmount?ColGrid.payAmount:ColGrid.payDiscount);
						} else if (c == ColGrid.payAmount) {
							if (event.keyCode==keyCodes.TAB) {
								event.stopImmediatePropagation();
								if(event.shiftKey){
									htGrid.selectCell(r,ColGrid.payDiscount);
								} else {
									if(htGrid.countRows() > r+1){
										htGrid.selectCell(r+1,ColGrid.isCheck);
									}
								}
								event.preventDefault();
							}
						} else if (c == ColGrid.isCheck) {
							if (event.keyCode == keyCodes.TAB) {
								event.stopImmediatePropagation();
								if (event.shiftKey) {
									htGrid.deselectCell();
									$('#payreceiptValue').focus();
								} else {
									htGrid.selectCell(r,ColGrid.payDiscount);
								}
								event.preventDefault();
							}
						}
					}
			  },
		});
	},
	changeCheckbox : function(a) {
		if (a.checked) {
			$('.checker').on('click', function () {
				$(this).closest('table').find('input.htCheckboxRendererInput').prop("checked", this.checked);
				$.uniform.update();//update UniformJS
			});
		}
	},
	
	isCheckedAll: function() {
		var data = $('#dg').handsontable('getData');
		if (data != null && data.length >0) {
	        for (var i = 0, ilen = data.length; i < ilen; i++) {
	            if (!data[i].isCheck) {
	                return false;
	            }
	        }
	        return true;
		}
		return false;
	},
	changeWhenCheck: function(isCheck, rowData, i) {
		if (isCheck) {
			if (rowData.status != 1) {
				rowData.status = 1;
				rowData.payDiscount = null,
				rowData.payAmount = rowData.remain;
			}
			if (CustomerDebitBatchNew.htCheckboxRendererInputChecked() && CustomerDebitBatchNew._staffCodeNVTTByChange != 'UNCHECKALL') {
				rowData.nvttCodeName = CustomerDebitBatchNew._staffCodeNVTTByChange;//				
			} else {
				rowData.nvttCodeName = CustomerDebitBatchNew.getNVTTCheck();
			}
			CustomerDebitBatchNew.cbxCheckAllUpdate();
			rowData.bankCode = CustomerDebitBatchNew.getBankCheck();
		} else {
			rowData.status = 0;
			rowData.payDiscount = null,
			rowData.payAmount = null;
			rowData.nvttCodeName = rowData.nvttBk;
			rowData.bankCode = rowData.bankCode;
		}
	},
	/**
	 * Ham nay dung de xu ly checkAll
	 * 
	 * @author hunglm16
	 * @since November 20,2014
	 * */
	changeWhenCheckNew: function(isCheck, rowData, i) {
		if (isCheck) {
			if (rowData.status != 1) {
				rowData.status = 1;
				rowData.payDiscount = null,
				rowData.payAmount = rowData.remain;
			}
			if (CustomerDebitBatchNew.htCheckboxRendererInputChecked() && CustomerDebitBatchNew._staffCodeNVTTByChange != 'UNCHECKALL') {
				rowData.nvttCodeName = CustomerDebitBatchNew._staffCodeNVTTByChange;//				
			} else {
				rowData.nvttCodeName = CustomerDebitBatchNew.getNVTTCheck();
			}
			rowData.bankCode = CustomerDebitBatchNew.getBankCheck();
		} else {
			rowData.status = 0;
			rowData.payDiscount = null,
			rowData.payAmount = null;
			rowData.nvttCodeName = rowData.nvttBk;
			rowData.bankCode = rowData.bankCode;
		}
	},
	/**
	 * Kiem tra co su kien check trong danh sach class htCheckboxRendererInput hay khong
	 * 
	 * @author hunglm16
	 * @since November 20,2014
	 * @description dung thuat toan binary quicksearch
	 * */
	htCheckboxRendererInputChecked: function() {
		var htGrid = $('#dg').handsontable('getInstance');
		var rows = htGrid.getData();
		if (rows != undefined && rows != null) {
		   for(var i=0, sz = rows.length; i < sz; i++){
		     if(rows[i].isCheck) {
		          return true;
		     }
		   }
		}
		return false;
	},
	/**
	 * Check All cbxCheckAll
	 * 
	 * @author hunglm16
	 * @since November 20,2014
	 * */
	cbxCheckAllUpdate: function (flag) {
		var htGrid = $('#dg').handsontable('getInstance');
		var rows = htGrid.getData();
		if (rows != undefined && rows != null) {
		   for(var i=0, sz = rows.length; i < sz; i++){
			   if(!rows[i].isCheck) {
				   $('#cbxCheckAll').prop('checked', false);
				   return;
			   }
		   }
		}
		if (flag!=undefined && flag!=null && flag) {
			$('#cbxCheckAll').prop('checked', true).change();
		}
	},
	/**
	 * Check All cbxCheckAll Change
	 * 
	 * @author hunglm16
	 * @since November 20,2014
	 * */
	cbxCheckAllChange: function () {
		var flagCheckAll = $('#cbxCheckAll').prop('checked');
		var htGrid = $('#dg').handsontable('getInstance');
		var rows = htGrid.getData();
		if (rows != undefined && rows != null) {
		   if (!flagCheckAll) {
			   CustomerDebitBatchNew.getDataInGrid();
		   } else {
			   for(var i=0, sz = rows.length; i < sz; i++){
				   rows[i].isCheck = true;
				   CustomerDebitBatchNew.changeWhenCheckNew(true, rows[i], i);
			   }
			   $('#dg').closest('table').find('input.htCheckboxRendererInput').prop("checked", true);
		   }
		}
		CustomerDebitBatchNew.setTotalPay();
	},
	changeNVTT: function(staffCode,debitId) {
		var data = $('#dg').handsontable('getData');
		if (data != null && data.length >0) {
	        for (var i = 0, ilen = data.length; i < ilen; i++) {
	            if (data[i].isCheck && data[i].debitId == debitId) {
	            	data[i].nvttCodeName = staffCode;
	            	CustomerDebitBatchNew._staffCodeNVTTByChange = staffCode;
	            }
	        }
		}
	},
	changeBank: function(bankCode,debitId) {
		var data = $('#dg').handsontable('getData');
		if (data != null && data.length >0) {
	        for (var i = 0, ilen = data.length; i < ilen; i++) {
	            if (data[i].isCheck && data[i].debitId == debitId) {
	            	data[i].bankCode = bankCode;
	            }
	        }
		}
	},
	getNVTTCheck: function(){
		var data = $('#dg').handsontable('getData');
		var staffCode = null;
		for (var i = 0, ilen = data.length; i < ilen; i++) {
            if (data[i].isCheck) {
            	staffCode = data[i].nvttCodeName;
            	if (staffCode!=undefined && staffCode!=null) {            		
            		CustomerDebitBatchNew._staffCodeNVTTByChange = data[i].nvttCodeName;
            	}
            	break;
            }
        }
		return staffCode;
	},
	getBankCheck: function(){
		var data = $('#dg').handsontable('getData');
		var bankCode = null;
		for (var i = 0, ilen = data.length; i < ilen; i++) {
			if (data[i].isCheck) {
				bankCode = data[i].bankCode;
				break;
			}
		}
		return bankCode;
	},
	getTotalPayDiscount: function(){
		var listData = $('#dg').handsontable('getInstance').getData();
		var total = 0;
		for(var i=0, size = listData.length;i<size;i++){
			var dis = listData[i].payDiscount;
			if(listData[i].status == 1 && !isNullOrEmpty(dis) && listData[i].isCheck){
				total += dis;
			}
		}
		return total;
	},
	getTotalPayAmount: function(){
		var listData = $('#dg').handsontable('getInstance').getData();
		var total = 0;
		for(var i=0, size = listData.length;i<size;i++){
			var amo = listData[i].payAmount;
			if(listData[i].status == 1 && !isNullOrEmpty(amo) && listData[i].isCheck){
				total += amo;
			}
		}
		return total;
	},
	setTotalPay: function(){
		var listData = $('#dg').handsontable('getInstance').getData();
		var totalDis = 0;
		var totalAmo = 0;
		for(var i=0, size = listData.length;i<size;i++){
			var dis = listData[i].payDiscount;
			var amo = listData[i].payAmount;
			if(listData[i].status == 1 && listData[i].isCheck){
				totalDis += Number(dis);
				totalAmo += Number(amo);
			}
		}
		$('#cDiscount').html(formatCurrency(totalDis));
		$('#cAmount').html(formatCurrency(totalAmo));
	},
	loadPage: function() {
		$('#dg').on('	', 'input.checker', function (event) {
			var htGrid = $('#dg').handsontable('getInstance');
			var data = htGrid.getData();
			if(data !=null){
		        var current = !$('input.checker').is(':checked'); //returns boolean
	            for (var i = 0, ilen = data.length; i < ilen; i++) {
	            	data[i].isCheck = current;
	            	CustomerDebitBatchNew.changeWhenCheck(current, data[i], i);
	            }
	            htGrid.render();
			}
	    });
		$('#payreceiptValue').bind('keypress',function(event){
			var htGrid = $('#dg').handsontable('getInstance');
			if(!event.shiftKey && event.keyCode == keyCodes.TAB && htGrid.countRows()>0){
				htGrid.selectCell(0, ColGrid.isCheck);
				event.preventDefault();
			}
		});
	},
	getDataInGrid: function(varParams) {
		CustomerDebitBatchNew.initGrid();
		$('.ErrorMsgStyle').html('').hide();
		$("#payerName").val("");
		$("#payerAddress").val("");
		$("#paymentReason").val("");
		$("#payreceiptCode").parent().find("input[type=text]").removeAttr("disabled");
    	enable("btnExportPayment");
    	enable("btnPay");
		if (varParams == undefined || varParams == null) {
			$("#loadingDebit").show();
			var msg = '';
			msg = Utils.getMessageOfRequireCheck('shop', jsp_common_shop_name_lable, true);
			if (msg.length == 0) {
				msg = Utils.getMessageOfRequireCheck('type', 'Loại chứng từ', true);
			}
			var fromDate = $('#fromDate').val().trim();
			var toDate = $('#toDate').val().trim();
			if (fromDate == '__/__/____') {
				$('#fromDate').val('');
				fromDate = '';
			}
			if (toDate == '__/__/____') {
				$('#toDate').val('');
				toDate = '';
			}
			if (msg.length == 0 && fromDate.length > 0) {
				msg = Utils.getMessageOfInvalidFormatDate('fromDate', 'Từ ngày');
			}
			if (msg.length ==0 && toDate.length > 0) {
				msg = Utils.getMessageOfInvalidFormatDate('toDate', 'Đến ngày');
			}
			if (msg.length == 0 && fromDate.length > 0 && toDate.length > 0 &&  !Utils.compareDate(fromDate, toDate)) {
				msg = msgErr_fromdate_greater_todate;
				$('#fromDate').focus();
			}
			if (msg.length > 0) {
				$('#errMsgTop').html(msg).show();
				$("#loadingDebit").hide();
				return false;
			}
			var params = new Object();
			params.shortCode = $('#shortCode').val().trim();
			params.fromDate = $('#fromDate').val().trim();
			params.toDate = $('#toDate').val().trim();
			params.type = $('#type').val().trim();
			CustomerDebitBatchNew._type = $('#type').val().trim();
			params.staffDeliverCode = $('#transferStaff').combobox('getValue').trim();
			params.staffPaymentCode = $('#crashierStaff').combobox('getValue').trim();
			params.staffCode = $('#staffCode').combobox('getValue').trim();
			params.lstOrderNumbers = $("#orderNumber").val().trim();
			params.shopCode = $('#shop').combobox('getValue').trim();
			varParams = params;
		}
		$.ajax({
		    url: "/customerdebit/batch/getinfo",
		    dataType: 'json',
		    data: varParams,
		    type: 'POST',
		    success: function (res) {
		    	if (res.error == undefined || res.error == null || res.error != true) {
			    	var data = res.rows;
			    	if(data != null){
			    		CustomerDebitBatchNew._mapDataGrid = new Map();
			    		var totalPayAmount = 0;
			    		for (var i = 0; i < data.length; i++) {
			    			data[i].status = 0;
			    			data[i].isCheck = false;
			    			data[i].customer = Utils.XSSEncode(data[i].shortCode + ' - ' + data[i].customerName);
			    			data[i].orderDate = $.datepicker.formatDate('dd/mm/yy', new Date(data[i].orderDate));
			    			data[i].paied = {pay: data[i].totalPay, discount: data[i].discountAmount};
			    			data[i].payDiscount = null;
			    			data[i].payAmount = null;
			    			var nvtt = null;
			    			if (data[i].nvttCodeName != null) {
			    				nvtt = Utils.XSSEncode(data[i].nvttCodeName + " - " + data[i].nvttName);
			    			}
			    			data[i].nvttBk = nvtt;
			    			data[i].nvttCodeName = nvtt;
			    			data[i].bankCode = null;
			    			if(CustomerDebitBatchNew._type == 0){
			    				data[i].remain *= -1;
			    				data[i].total *= -1;
			    			}
			    			totalPayAmount += data[i].remain;
			    		}
			    		CustomerDebitBatchNew._listDataGrid = data;
			    		$("#loadingDebit").hide();
		      		$('#dg').handsontable('getInstance').loadData(data);
		      		if (res != null && res.payreceiptCode != null) {
						$('#payreceiptCode').val(res.payreceiptCode);
					}	
		      		$('#cAmountByGrid').html(formatCurrency(totalPayAmount));
		      		$('#payreceiptValue').val('');
		      		$('#payreceiptValue').focus();
		      		$('#cAmount').html(0);
		      		$('#cDiscount').html(0);
			    	} else {
			    		$('#dg').handsontable('getInstance').loadData([]);
			    	}
			    	if (res != null && res.debitPreRemain != undefined && res.debitPreRemain != null) {
					$('#debitPreRemain').val(formatCurrencyInterger(res.debitPreRemain));
					$('#debitPostRemain').val(formatCurrencyInterger(res.debitPreRemain));
					CustomerDebitBatchNew._totalDebit = res.debitPreRemain;
				}
			    	$('#cbxCheckAll').prop('checked', false);
			    	CustomerDebitBatchNew._checkF9 = false;
		    	} else {
		    		$('#errMsg').html(res.errMsg).show();
		    		$('#dg').handsontable('getInstance').loadData([]);
		    	}
		    	$("#loadingDebit").hide();
		    }
		  });
	},
	import: function(printOnly) {
		$('.ErrorMsgStyle').html('').hide();
		var msg = '';
		msg = Utils.getMessageOfRequireCheck('shop', jsp_common_shop_name_lable, true);
		if (msg.length ==0) {
			msg = Utils.getMessageOfSpecialCharactersValidate('shortCode','Mã khách hàng',Utils._CODE );
		}
		if (msg.length == 0 && $('#type').val() == -1 ) {
			msg = 'Bạn chưa chọn loại chứng từ. Yêu cầu chọn loại chứng từ';
		}
		if (msg.length ==0) {
			msg = Utils.getMessageOfRequireCheck('payreceiptCode','Số chứng từ' );
		}
		if (msg.length ==0) {
			msg = Utils.getMessageOfSpecialCharactersValidate('payreceiptCode','Số chứng từ',Utils._CODE );
		}
		if (msg.length ==0) {
			msg = Utils.getMessageOfRequireCheck('payreceiptValue','Số tiền', undefined, true);
		}
		var payreceiptValue = Utils.returnMoneyValue($('#payreceiptValue').val().trim());
		if (payreceiptValue.length > 20) {
			msg = 'Giá trị Số tiền nhập vào chỉ giới hạn 20 ký tự';
		}
		var type = CustomerDebitBatchNew._type;
		if (msg.length ==0 &&(isNaN(payreceiptValue) || payreceiptValue <=0)) {
			if (type==1) {
				msg ='Số tiền phiếu thu phải là số nguyên dương';
			} else {
				msg ='Số tiền phiếu chi phải là số nguyên dương';
			}
		}
		
		if (msg.length > 0) {
			$('#errMsg').html(msg).show();
			return false;
		}		
		var lstCustomerDebitID = new Array();
		var lstPayAmount = [];
		var lstDiscount = [];
		var lstStaffCode = [];
		var lstBankCode = [];
		var htGrid = $('#dg').handsontable('getInstance');
		var rows = htGrid.getData();
		if (rows != undefined && rows != null) {
			for (var i=0, sz = rows.length; i < sz; i++) {
				if (rows[i].isCheck) {
					if (rows[i].status == 2) {
						$('#errMsg').html('Giá trị thanh toán của đơn hàng ' + Utils.XSSEncode(rows[i].orderNumber) + ' lớn hơn số tiền còn nợ của đơn hàng!').show();
						htGrid.selectCell(i,ColGrid.isCheck);
						return false;
					} else if(rows[i].status == 1) {
						lstCustomerDebitID.push(rows[i].debitId);
						if (CustomerDebitBatchNew._type == 0) {
							lstPayAmount.push(rows[i].payAmount*(-1));
						} else {
							lstPayAmount.push(rows[i].payAmount);
						}
						lstDiscount.push(isNullOrEmpty(rows[i].payDiscount) ? 0 :rows[i].payDiscount);
						if (rows[i].nvttCodeName == null) {
							lstStaffCode.push('');
						} else {
							lstStaffCode.push(CustomerDebitBatchNew._mapNVTT.get(rows[i].nvttCodeName));
						}
						if (rows[i].bankCode == null) {
							lstBankCode.push('');
						} else {
							lstBankCode.push(rows[i].bankCode);
						}
					}
				}
			}
		}
		if (lstCustomerDebitID.length <=0 ) {
			$('#errMsg').html("Chưa chọn đơn hàng để thanh toán").show();
			return false;
		}
		var cAmount = Number(Utils.returnMoneyValue($('#cAmount').html().trim()));
		var cPay = Number(payreceiptValue);
		if (cPay - cAmount != 0) {
			$('#errMsg').html('Giá trị phiếu ' + (type==1 ? 'thu' : 'chi') + ' phải bằng tổng tiền thanh toán.').show();
			return false;
		}
		
		var params = new Object();
		params.shopCode = $('#shop').combobox('getValue').trim();
		params.shortCode = $('#shortCode').val().trim();
		params.type = CustomerDebitBatchNew._type;//$('#type').val();
		params.payreceiptCode = $('#payreceiptCode').val().trim();
		params.payreceiptValue = payreceiptValue;
		params.lstCustomerDebitID = lstCustomerDebitID;
		params.lstPayAmount = lstPayAmount;
		params.lstDiscount = lstDiscount;
		params.lstStaffCode = lstStaffCode;
		params.lstBankCode = lstBankCode;
		params.payerName = $("#payerName").val().trim();
		params.payerAddress = $("#payerAddress").val().trim();
		params.paymentReason = $("#paymentReason").val().trim();
		try {
			params.reportCode = $('#function_code').val();
		} catch(e) {
			// Oop! Error
		}
		msg ='Bạn có muốn thanh toán theo các đơn hàng này ?';
		var url = null;
		if (printOnly) {
			url = "/customerdebit/batch/export-payment";
		} else {
			url = "/customerdebit/batch/import";
		}
		Utils.addOrSaveData(params, url, null,  'errMsg', function(data) {
			if (printOnly) {
				if (!data.error && data.path) {
//					var width = $(window).width();
//					var height = $(window).height();
//					window.open('/commons/open-tax-view?windowWidth='+width+'&windowHeight='+height);
					
					var filePath = ReportUtils.buildReportFilePath(data.path);
					ReportUtils.openInNewTab(filePath);
				}
				CustomerDebitBatchNew.reloadPaymentInfo(data.lstDebitVO);
			} else {
				CustomerDebitBatchNew.getDataInGrid();
			}
		}, 'loadingImport',null, null, msg,function(dataErr) {
			if(dataErr.errorReloadPage == true){
				window.location.href = '/customerdebit/batch/info-new';
			}
		});
		return false;
	},
	reloadPaymentInfo: function(data) {
		CustomerDebitBatchNew._mapDataGrid = new Map();
    		var totalPayAmount = 0;
    		for (var i = 0; i < data.length; i++) {
    			data[i].status = 0;
    			data[i].isCheck = false;
    			data[i].customer = Utils.XSSEncode(data[i].shortCode + ' - ' + data[i].customerName);
    			data[i].orderDate = $.datepicker.formatDate('dd/mm/yy', new Date(data[i].orderDate));
    			data[i].paied = {pay: data[i].totalPay, discount: data[i].discountAmount};
    			data[i].payDiscount = data[i].discountAmt;
    			data[i].payAmount = data[i].payAmt;
    			var nvtt = null;
    			if (data[i].nvttCodeName != null) {
    				nvtt = Utils.XSSEncode(data[i].nvttCodeName + " - " + data[i].nvttName);
    			}
    			data[i].nvttBk = nvtt;
    			data[i].nvttCodeName = nvtt;
    			data[i].bankCode = null;
    			if(CustomerDebitBatchNew._type == 0){
    				data[i].remain *= -1;
    				data[i].total *= -1;
    			}
    			totalPayAmount += data[i].remain;
    		}
    		CustomerDebitBatchNew._listDataGrid = data;
    		$("#loadingDebit").hide();
    		$('#dg').handsontable('getInstance').loadData(data);
    		$("#payreceiptCode").parent().find("input[type=text]").attr("disabled", "disabled");
    		disabled("btnExportPayment");
    		disabled("btnPay");
	},
	viewExcel:function(){		
		var importFileName = $('#excelFile').val();
		if(importFileName.length > 0){
			$('#btnImport').attr('disabled',false);
			$('#btnImport').removeClass('BtnGeneralDStyle');
		}		
		$('#isView').val(1);
		$('#importFrm').submit();
		return false;
	},
	importExcel:function(){		
		$('#btnImport').attr('disabled',true);
		$('#btnImport').addClass('BtnGeneralDStyle');
		msg = 'Bạn có muốn nhập từ file này không?';
		$.messager.confirm('Xác nhận', msg, function(r){
			if (r){
				$('#isView').val(0);
				$('#importFrm').submit();
			}
		});	
		return false;
	},		
	loadInfo : function(){
		CustomerDebitBatchNew.getInfo(false,false,true);
	},
	getInfo : function(checkF9,isSave, isSaveExcel) {
		//$('.ErrorMsgStyle').html('').hide();
		if($('#shortCode').val().trim().length == 0){			
			return false;
		}
		$("#payerName").val("");
		$("#payerAddress").val("");
		$("#paymentReason").val("");
		$('#payreceiptValue').val('');
		$('#debitPreRemain').val($('#debitPostRemain').val());
		var params = new Object();
		//params.staffCode = $('#staffCode').val().trim();
		params.shopCode = $('#shop').combobox('getValue').trim();
		params.shortCode = $('#shortCode').val().trim();
		params.type = $('#type').val();
		if($('#type').val() == -1) {
			return false;
		}
		CustomerDebitBatchNew.getDataInGrid(params);
		/*if(checkF9) {
			CustomerDebitBatchNew._checkF9 = false;
		}		*/
		return false;
	},
	
	changePayReceiptValue: function() {
		var payreceiptValueTemp = $('#payreceiptValue').val();
		if(payreceiptValueTemp.length != 0) {
			var payreceiptValue = Number(Utils.returnMoneyValue(payreceiptValueTemp));
			var type = $('#type').val();
			var msg ='';
			if(isNaN(payreceiptValue) || payreceiptValue <=0){
				if(type==1){
					msg ='Số tiền phiếu thu phải là số nguyên dương';
				} else{
					msg ='Số tiền phiếu chi phải là số nguyên dương';
				}
				Alert('Thông báo',msg, function() {
					$('#payreceiptValue').val('');
					$('#payreceiptValue').focus();
				});
			} else{
				var heSo = 1;
				if(type== 0){
					heSo = -1;
				}
				CustomerDebitBatchNew._remainMoney = payreceiptValue;
				var cAmount = Number(Utils.returnMoneyValue($('#cAmount').html().trim()));
				$('#debitPostRemain').val(formatCurrency(CustomerDebitBatchNew._totalDebit - Math.min(payreceiptValue, cAmount) * heSo));
			}
			return false;
		}
	},
	changeCustomer: function() {
		if($('#shortCode').val().trim().length != 0){
			var url = 'shopCode='+ $('#shopCode').val() + '&shortCode='+$('#shortCode').val();
			$.getJSON('/customerdebit/auto/currentDebit?'+url, function(data){
				if(data.error) {
					$('#errMsg').html(data.errMsg).show();
				} else {
					$('#debitPreRemain').val(formatCurrencyInterger(data.currentDebit) );
					CustomerDebitBatchNew._totalDebit = data.currentDebit;
					$('#debitPostRemain').val('');
				}
			});
		}
		return false;
	},
	showImportExcelOnDialog:function(){		
		$('#importExcelContent').css("visibility", "visible");
		var html = $('#importExcelContent').html();
		$('#errExcelMsg').html('').hide();
		var msg = Utils.getMessageOfRequireCheck('shop', jsp_common_shop_name_lable, true);
		if (msg.length > 0) {
			$('#errMsgTop').html(msg).show();
			return false;
		}
		$('#importExcelContenDialog').dialog({  	       
	        closed: false,  
	        cache: false,  
	        modal: true,
	        /*width : 850,
	        height :450,*/
	        width: 470,
	        height: "auto",
	        onOpen: function(){
	        	/*$('.easyui-dialog #dgDetail').datagrid({					
					//autoRowHeight : true,
					rownumbers : true, 
					singleSelect: true,					
					//rowNum : 10,
					scrollbarSize : 0,
					fitColumns:true,
					//cache: false, 					
					width : $('#importExcelContenDialog').width()-20,
					//height:auto,
					//autoWidth: true,
				    columns:[[
						{field: 'shortCode',title: 'Mã KH',  width: 100,sortable:false,resizable:false, align: 'left', formatter:function(value, options, rowObject) {
							return Utils.XSSEncode(value);
						}},
						{field: 'type',title: 'Loại CT',  width: 100,sortable:false,resizable:false, align: 'left' , formatter:function(value, options, rowObject) {
							return Utils.XSSEncode(value);
						}},
						{field: 'payreceiptCode',title: 'Số CT',  width: 100,sortable:false,resizable:false, align: 'left', formatter:function(value, options, rowObject) {
							return Utils.XSSEncode(value);
						}},
						{field: 'money',title: 'Số tiền',  width: 100,sortable:false,resizable:false, align: 'right', formatter:function(cellValue, options, rowObject) {
							if(cellValue!=undefined && cellValue!=null){
								return formatCurrency(Math.abs(cellValue));
							}
							return '';
						} },
						{field: 'discount',title: 'Chiết khấu',  width: 100,sortable:false,resizable:false, align: 'right', formatter:function(cellValue, options, rowObject) {
							if(cellValue!=undefined && cellValue!=null){
								return formatCurrency(Math.abs(cellValue));
							}
							return '';
						}}
				    ]],
				    onLoadSuccess :function(data){   	 
				    	
				    }
	        	});
	        	$('.datagrid-header-rownumber').html('STT');
	         	$('.datagrid-header-row td div').css('text-align','center');*/
	        },
	        onClose:function(){
	        	$('#importExcelContent').html(html);
	        	$('#importExcelContent').css("visibility", "hidden");
	        	$('.easyui-dialog #dgDetail').datagrid('loadData', []);
	        	$('#excelFile').val('');
	        	$('#fakefilepc').val('');
	        }
		});
	},
	beforeImportExcel: function(){
		if(!previewImportExcelFile(document.getElementById("excelFile"))){
			return false;
		}		
		$('#errExcelMsg').html('').hide();		
		showLoadingIcon();
		return true;
	},
	afterImportExcelUpdate: function(responseText, statusText, xhr, $form){
		if($('#btnImport').length != 0) {
			enable('btnImport'); 
		}
		hideLoadingIcon();
		if (statusText == 'success') {				
	    	$("#responseDiv").html(responseText);		 
	    	var newToken = $('#responseDiv #newToken').val();
	    	console.log(newToken);
	    	if(newToken != null && newToken != undefined && newToken != '') {
	    		$('#token').val(newToken);	    		
	    		$('#tokenImport').val(newToken);
	    	}
	    	if(($('#errorExcel').html()!=null && $('#errorExcel').html().trim() == 'true') ||($('#errorExcelMsg').html() && $('#errorExcelMsg').html().trim().length > 0)){
	    		$('#errExcelMsg').html($('#errorExcelMsg').html()).show();
	    	} else {
	    		if($('#typeView').html().trim() == 'false'){
	    			var totalRow = parseInt($('#totalRow').html().trim());
	    			var numFail = parseInt($('#numFail').html().trim());
	    			var fileNameFail = $('#fileNameFail').html();
	    			var mes = format(msgErr_result_import_excel,(totalRow - numFail),numFail);		    		
	    			if(numFail > 0){
	    				mes+= ' <a href="' + fileNameFail +'">Xem chi tiết lỗi</a>';
	    			}
	    			if($('#excelFile').length!=0 && $('#fakefilepc').length!=0){
	    				try{$('#excelFile').val('');$('#fakefilepc').val('');}catch(err){}
	    			}
	    			$('.easyui-dialog').dialog('close');
	    			$('#btnSearch').click();
	    			setTimeout(function(){
	    				$('#errMsg').html(mes).show();
					}, 3000);
	    		}else{	    
	    			$('.easyui-dialog #dgDetail').datagrid("loadData", {total:0, rows:[]});
	    			$('.excel-item').each(function(){
	    				var row = new Object();
	    				//row.staffCode = $(this).attr('staffCode');
	    				row.shortCode = $(this).attr('shortCode');
	    				row.type = $(this).attr('typect') == '0' ? 'phiếu thu' : $(this).attr('typect') == '1' ? 'phiếu chi' : $(this).attr('typect');
	    				row.payreceiptCode = $(this).attr('payreceiptCode');
	    				row.money = $(this).attr('money');
	    				row.discount = $(this).attr('discount');
	    				row.errMsg = $(this).attr('errMsg');
	    				$('.easyui-dialog #dgDetail').datagrid('appendRow',row);
	    			});
	    		}
	    	}
	    }	
	},
	exportExcel: function() {
		$('#divOverlay').addClass('Overlay');
		$('#imgOverlay').show();
		$('#errMsg').html('').hide();
		$('#errExcelMsg').html('').hide();
		$('.ErrorMsgStyle').html('').hide();
		var msg = '';
		msg = Utils.getMessageOfRequireCheck('shop', jsp_common_shop_name_lable, true);
		if(msg.length == 0){
			msg = Utils.getMessageOfRequireCheck('type','Loại chứng từ', true);
		}
		var fromDate = $('#fromDate').val().trim();
		var toDate = $('#toDate').val().trim();
		if(fromDate == '__/__/____'){
			$('#fromDate').val('');
			fromDate = '';
		}
		if(toDate == '__/__/____'){
			$('#toDate').val('');
			toDate = '';
		}
		if(msg.length == 0 && fromDate.length > 0){
			msg = Utils.getMessageOfInvalidFormatDate('fromDate', 'Từ ngày');
		}
		if(msg.length ==0 && toDate.length > 0){
			msg = Utils.getMessageOfInvalidFormatDate('toDate', 'Đến ngày');
		}
		if(msg.length == 0 && fromDate.length > 0 && toDate.length > 0 &&  !Utils.compareDate(fromDate, toDate)){
			msg = msgErr_fromdate_greater_todate;
			$('#fromDate').focus();
		}
		if (msg.length > 0) {
			$('#errMsgTop').html(msg).show();
			return false;
		}
		
		
		var data = new Object();
		data.shopCode = $('#shop').combobox('getValue').trim();
		data.shortCode = $('#shortCode').val().trim();
		data.fromDate = $('#fromDate').val().trim();
		data.toDate = $('#toDate').val().trim();
		data.type = $('#type').val().trim();
		data.staffDeliverCode = $('#transferStaff').combobox('getValue').trim();
		data.staffPaymentCode = $('#crashierStaff').combobox('getValue').trim();
		data.staffCode = $('#staffCode').combobox('getValue').trim();
		var lstCustomerDebitID = new Array();
		var rowCheck = $('[name=debitId]');
		var rows = $('#dg').handsontable('getData');
		
		
		var listPayDiscountStr = [];
		var listPayAmountStr = [];
		var listRemainStr = [];
		var listTotalStr = [];
		var listNVTTStr = [];
		if (rows != undefined && rows != null) {
			for(var i=0, sz = rows.length; i < sz; i++){
				if(rows[i].isCheck) {
			          lstCustomerDebitID.push(rows[i].debitId);
			          if (rows[i].payDiscount!=undefined && rows[i].payDiscount!=null) {
			        	  listPayDiscountStr.push(rows[i].debitId+'_' +rows[i].payDiscount);			        	  
			          }
			          if (rows[i].payAmount!=undefined && rows[i].payAmount!=null) {
			        	  listPayAmountStr.push(rows[i].debitId+'_' +rows[i].payAmount);			        	  
			          }
			          if (rows[i].remain!=undefined && rows[i].remain!=null) {
			        	  listRemainStr.push(rows[i].debitId+'_' +rows[i].remain);
			          }
			          if (rows[i].total!=undefined && rows[i].total!=null) {
			        	  listTotalStr.push(rows[i].debitId+'_' +rows[i].total);
			          }
			          if (rows[i].nvttCodeName!=undefined && rows[i].nvttCodeName!=null && rows[i].nvttCodeName.trim().length > 0) {
			        	  listNVTTStr.push(rows[i].debitId+'_' +rows[i].nvttCodeName.trim());
			          }
			     }
			}
		}
		
		data.lstDebitId = lstCustomerDebitID;
		data.arrPayDiscountStr = listPayDiscountStr.join(",");
		data.arrRemainStr = listRemainStr.join(",");
		data.arrPayAmountStr = listPayAmountStr.join(",");		
		data.arrTotalStr = listTotalStr.join(",");
		data.arrNVTTStr = listNVTTStr.join(",");
		var url = "/customerdebit/batch/exportExcel";
		ReportUtils.exportReport(url, data);
		return false;
	},

	/**
	 * tai file template import thanh toan theo don hang
	 *
	 * @author lacnv1
	 * @since Mar 24, 2015
	 */
	downloadImportTemplate: function() {
		$('.ErrorMsgStyle').html('').hide();

		var params = {};

		var fromDate = $('#fromDate').val().trim();
		var toDate = $('#toDate').val().trim();
		if(fromDate == '__/__/____'){
			$('#fromDate').val('');
			fromDate = '';
		}
		if(toDate == '__/__/____'){
			$('#toDate').val('');
			toDate = '';
		}
		var msg = Utils.getMessageOfInvalidFormatDate('fromDate', 'Từ ngày');
		if (msg.trim().length == 0) {
			params.fromDate = $('#fromDate').val().trim();
		}
		if(msg.length ==0 && toDate.length > 0){
			msg = Utils.getMessageOfInvalidFormatDate('toDate', 'Đến ngày');
			if (msg.trim().length == 0) {
				params.toDate = $('#toDate').val().trim();
			}
		}
		params.shopCode = $('#shop').combobox('getValue').trim();
		params.shortCode = $('#shortCode').val().trim();
		params.type = $('#type').val().trim();
		params.staffDeliverCode = $('#transferStaff').combobox('getValue').trim();
		params.staffPaymentCode = $('#crashierStaff').combobox('getValue').trim();
		params.staffCode = $('#staffCode').combobox('getValue').trim();
		params.lstOrderNumbers = $("#orderNumber").val().trim();
		try {
			params.reportCode = $('#function_code').val();
		} catch(e) {
			// Oop! Error
		}
		Utils.getJSONDataByAjax(params, "/customerdebit/batch/download-template", function(data) {
			if (!data.error) {
				var filePath = ReportUtils.buildReportFilePath(data.path);
				window.location.href = filePath;
				setTimeout(function() {
					CommonSearch.deleteFileExcelExport(filePath);
				}, 10000);
			}
		}, null, "POST");
	},

	/**
	 * import thanh toan bang excel
	 *
	 * @author lacnv1
	 * @since Mar 24, 2015
	 */
	importExcelNew: function() {
		$('.ErrorMsgStyle').hide();
		VTUtilJS.importExcelUtils(function(data) {
			CustomerDebitBatchNew.getDataInGrid();
			$("#errExcelMsg").html(data.message).show();
		}, "importFrm", "excelFile", null, "errExcelMsg", {shopCode: $('#shop').combobox('getValue').trim()});
	},

	/**
 	 * luu NVTT don hang
 	 *
 	 * @author nhutnn
 	 * @since 16/11/2015
 	 */
	updateSaleOrderDebitDetail: function() {
		$('.ErrorMsgStyle').html('').hide();
		var msg = Utils.getMessageOfRequireCheck('shop', jsp_common_shop_name_lable, true);
		var lstCustomerDebitID = new Array();
		if (msg.length == 0) {
			var htGrid = $('#dg').handsontable('getInstance');
			var rows = htGrid.getData();
			if (rows != undefined && rows != null) {
				for (var i = 0, sz = rows.length; i < sz; i++) {
					if (rows[i].isCheck) {
						lstCustomerDebitID.push(rows[i].debitId);
					}
				}
			}
			if (lstCustomerDebitID.length <= 0) {
				$('#errMsg').html("Chưa chọn đơn hàng để thay đổi NVTT.").show();
				return false;
			}
		}
		if (msg.length == 0 && ($("#staffCodeNVTT").combobox("getValue") == null || $("#staffCodeNVTT").combobox("getValue") == "")) {
			msg = "Bạn chưa chọn NVTT.";
		}

		if (msg.length > 0) {
			$('#errMsg').html(msg).show();
			return false;
		}

		var params = new Object();
		params.lstCustomerDebitID = lstCustomerDebitID;
		params.staffCode = $("#staffCodeNVTT").combobox("getValue").trim();
		params.shopCode = $('#shop').combobox('getValue').trim();

		JSONUtil.saveData2(params, "/customerdebit/batch/update-customer-debit-detail", "Bạn có muốn cập nhật dữ liệu?", "errMsg", function(data) {
			if (!data.error) {
				$("#staffCodeNVTT").combobox("clear");
				$("#successMsg").html("Lưu dữ liệu thành công").show();
				setTimeout(function() {
					$("#successMsg").html("").hide();
				}, 2000);

				CustomerDebitBatchNew.getDataInGrid();
			} else {
				$("#errMsg").html(data.errMsg).show();
			}
		}, null, null);
	},
	
	/**
	 * xu ly khi chon shop
	 * @author trietptm
	 * @return String
	 * @since Dec 4, 2015
	 */
	changeShop: function(shopCode) {
		$('#divOverlay').show();
		$.getJSON('/customerdebit/batch/change-shop?shopCode=' + shopCode, function(data) {
			if  (data != undefined && data != null) {
				$('.ErrorMsgStyle').hide();
				var lstSale = data.lstSale;
				var lstDeliver = data.lstDeliver;
				var lstCashier = data.lstCashier;
				var listBank = data.listBank;
				
				if (lstSale != undefined && lstSale != null && lstSale.length > 0) {
					Utils.bindStaffCbx('#staffCode', lstSale, null, 206);
				}
				if (lstDeliver != undefined && lstDeliver != null && lstDeliver.length > 0) {
					Utils.bindStaffCbx('#transferStaff', lstDeliver, null, 206);
				}
				CustomerDebitBatchNew._mapNVTT = new Map();
				CustomerDebitBatchNew._mapNVTT.put('','');
				if (lstCashier != undefined && lstCashier != null && lstCashier.length > 0) {
					Utils.bindStaffCbx('#crashierStaff', lstCashier, null, 206);
					Utils.bindStaffCbx('#staffCodeNVTT', $.extend(true, [], lstCashier), null, 206);
					for (var i = 0, size = lstCashier.length; i < size; i++) {
//						CustomerDebitBatchNew._lstNVTT.push(Utils.XSSEncode(lstCashier[i].staffName + " - " + lstCashier[i].staffCode));		
						CustomerDebitBatchNew._mapNVTT.put(Utils.XSSEncode(lstCashier[i].staffCode + " - " + lstCashier[i].staffName), lstCashier[i].staffCode)
					}
				}
				CustomerDebitBatchNew._lstBank = new Array();
				CustomerDebitBatchNew._lstBank.push("");
				if (listBank != undefined && listBank != null && listBank.length > 0) {
					for (var i = 0, size = listBank.length; i < size; i++) {
						CustomerDebitBatchNew._lstBank.push(Utils.XSSEncode(listBank[i].bankCode));						  
					}
				}
				if (data.fromDate != undefined && data.fromDate != null) {
					$('#fromDate').val(data.fromDate);
				}
				if (data.toDate != undefined && data.toDate != null) {
					$('#toDate').val(data.toDate);
				}
				if (data.payreceiptCode != undefined && data.payreceiptCode != null) {
					$('#payreceiptCode').val(data.payreceiptCode);
				}
				if (CustomerDebitBatchNew._isLoad) {
					CustomerDebitBatchNew.getDataInGrid();
					CustomerDebitBatchNew._isLoad = false;
				}
				$('#divOverlay').hide();
			}
		});
	}
};

/*
 * END OF FILE - /web/web/resources/scripts/business/customerdebit/vnm.customerdebit-batch-new.js
 */

/*
 * JavaScript file created by Rockstarapps Concatenation
*/

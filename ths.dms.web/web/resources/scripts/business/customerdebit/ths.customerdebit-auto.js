var CustomerDebitAuto = {
	_isLoad: true,
	getList: function() {
		$('.ErrorMsgStyle').hide();
		$('.SuccessMsgStyle').hide();
		var msg = Utils.getMessageOfRequireCheck('shop', jsp_common_shop_name_lable, true);
		var fromDate = $('#fromDate').val().trim();
		var toDate = $('#toDate').val().trim();
		if (fromDate == '__/__/____') {
			$('#fromDate').val('');
		}
		if (toDate == '__/__/____') {
			$('#toDate').val('');
		}
		if (msg.length == 0) {
			msg = Utils.getMessageOfRequireCheck('shortCode','Khách hàng');
		}		
		if (msg.length == 0) {
			msg = Utils.getMessageOfSpecialCharactersValidate('shortCode','Khách hàng',Utils._CODE);
		}
		//
		if (msg.length == 0) {
			msg = Utils.getMessageOfRequireCheck('status','Loại chứng từ', true);
		}
		if (msg.length == 0 && fromDate != null && fromDate.length > 0) {
			msg = Utils.getMessageOfInvalidFormatDate('fromDate', 'Từ ngày');
		}
		//
		if (msg.length == 0){
			msg = Utils.getMessageOfRequireCheck('payReceivedNumber','Số chứng từ');
		}
		if (msg.length == 0) {
			msg = Utils.getMessageOfRequireCheck('valuePay','Số tiền', undefined, true);
		}
		var valuePay = Utils.returnMoneyValue($('#valuePay').val().trim());
		if (valuePay.length > 20) {
			msg = 'Giá trị Số tiền nhập vào chỉ giới hạn 20 ký tự';
		}
		if (valuePay.length == 0) {
			$("#afterPay").val("");
		}
		if (msg.length == 0) {
			msg = Utils.getMessageOfInvaildNumber('valuePay', 'Số tiền');
		}
		if (msg.length == 0) {
			msg = Utils.getMessageOfNegativeNumberCheck('valuePay','Số tiền');
		}
		if (msg.length == 0 && toDate != null && toDate.length > 0) {
			msg = Utils.getMessageOfInvalidFormatDate('toDate', 'Đến ngày');
		}
		
		if (msg.length == 0  && fromDate != null && fromDate.length > 0 && toDate != null && toDate.length > 0 && !Utils.compareDate(fromDate, toDate)) {
			msg = msgErr_fromdate_greater_todate;
			$('#fromDate').focus();
		}
		
		if (msg.length > 0) {
			$('#errMsgTop').html(msg).show();
			return false;
		}
		if (_isFullPrivilege || _MapControl.get("btnExportPOAutoReport") == 2) {
			$('#btnAuto').show();
		}
		var data = new Object();
		data.shopCode = $('#shop').combobox('getValue').trim();
		data.payReceivedNumber = $('#payReceivedNumber').val().trim();
		data.shortCode = $('#shortCode').val().trim();
		//data.staff2Code = $('#staff2Code').val().trim();
		data.valuePay = Utils.returnMoneyValue($('#valuePay').val().trim());
		data.status = $('#status').val().trim();
		data.fromDate = $('#fromDate').val().trim();
		data.toDate = $('#toDate').val().trim();
		data.staffDeliverCode = $('#transferStaff').combobox('getValue').trim();
		data.staffPaymentCode = $('#crashierStaff').combobox('getValue').trim();
		$('#dg').datagrid('load',data);
		return false;
	},	
	savePay: function() {
		$('.ErrorMsgStyle').hide();
		$('.SuccessMsgStyle').hide();
		var msg = Utils.getMessageOfRequireCheck('shop', jsp_common_shop_name_lable, true);
		if (msg.length == 0) {
			msg = Utils.getMessageOfRequireCheck('shortCode','Khách hàng');
		}		
		if (msg.length == 0) {
			msg = Utils.getMessageOfSpecialCharactersValidate('shortCode','Khách hàng',Utils._CODE);
		}
		if (msg.length == 0) {
			msg = Utils.getMessageOfRequireCheck('valuePay','Số tiền');
		}		
		if (msg.length == 0) {
			msg = Utils.getMessageOfRequireCheck('payReceivedNumber','Mã chứng từ');
		}		
		if (msg.length == 0) {
			msg = Utils.getMessageOfSpecialCharactersValidate('payReceivedNumber','Mã chứng từ',Utils._CODE);
		}
		if (msg.length == 0&& $('#status').val() == -1 ) {
			msg = 'Bạn chưa chọn loại chứng từ. Yêu cầu chọn loại chứng từ';
		}
		
		var fromDate = $('#fromDate').val().trim();
		var toDate = $('#toDate').val().trim();
		if (fromDate == '__/__/____') {
			$('#fromDate').val('');
		}
		if (toDate == '__/__/____') {
			$('#toDate').val('');
		}
		if (msg.length == 0 && fromDate.length > 0 && toDate.length > 0 && !Utils.compareDate(fromDate, toDate)) {
			msg = msgErr_fromdate_greater_todate;
			$('#fromDate').focus();
		}
		
		if (msg.length > 0) {
			$('#errMsg').html(msg).show();
			return false;
		}
		var lstDebitId = new Array();
		var lstDebitAmt = new Array();
//		var rows =  $('#dg').datagrid('getSelections');
		var rows =  $('#dg').datagrid('getRows');
		for (var i = 0, n = rows.length; i < n; ++i) {
			lstDebitId.push(rows[i].debitId);
			lstDebitAmt.push(rows[i].payAmt);
		}
		if (lstDebitId.length <= 0) {
			msg = 'Không có đơn hàng nào được chọn.';
		}
		if (msg.length > 0) {
			$('#errMsg').html(msg).show();
			return false;
		}
		var data = new Object();
		data.shopCode = $('#shop').combobox('getValue').trim();
		data.shortCode = $('#shortCode').val().trim();
		data.payReceivedNumber = $('#payReceivedNumber').val().trim();
		data.valuePay = Utils.returnMoneyValue($('#valuePay').val().trim());
		//data.staff2Code = $('#staff2Code').val().trim();
		data.status = $('#status').val().trim();	
		data.lstDebitId = lstDebitId;
		data.lstDebitAmt = lstDebitAmt;
		data.fromDate = $('#fromDate').val().trim();
		data.toDate = $('#toDate').val().trim();
		data.staffDeliverCode = $('#transferStaff').combobox('getValue').trim();
		data.staffPaymentCode = $('#crashierStaff').combobox('getValue').trim();
		Utils.addOrSaveData(data,"/customerdebit/auto/savepay", null, 'errMsg', function(data){
			if(data.error){
				$('#errMsg').html(data.errMsg).show();
			}else{
				$('#shortCode').val('');
				$('#status').val(1);
				$('#status').change();
				$('#payReceivedNumber').val(data.payReceivedNumber);
				$('#valuePay').val('');
				$('#afterPay').val('');
				$('#beforePay').val('');
				$("#debitNPPToKH").val("");
				$("#debitKHToNPP").val("");
				$('#dg').datagrid('loadData',[]);
			}
		},'loading2',null,null,null,function(data) {
			if(data.errMsg != undefined && data.errMsg != null){
				$('#errMsg').html(data.errMsg).show();
			} else {
				$('#errMsg').html('Người dùng này đã được đăng nhập vào máy khác, đề nghị tải lại trang.').show();
			}
		});
	},
	
	/**
	 * xu ly khi chon shop
	 * @author trietptm
	 * @since Dec 19, 2015
	 */
	changeShop: function() {
		$('#divOverlay').show();
		var param = new Object();
		param.shopCode = $('#shop').combobox('getValue');
		$.ajax({
			type : "POST",
			url: '/customerdebit/auto/change-shop',
			data :($.param(param, true)),
			dataType : "json",
			success : function(data) {
				if  (data != undefined && data != null) {
					$('.ErrorMsgStyle').hide();
					var lstSale = data.lstSale;
					var lstDeliver = data.lstDeliver;
					var lstCashier = data.lstCashier;

					if (lstDeliver != undefined && lstDeliver != null && lstDeliver.length > 0) {
						Utils.bindStaffCbx('#transferStaff', lstDeliver, null, 206, function(item) {
							CustomerDebitAuto.getCurrentDebit();
						});
					}
					if (lstCashier != undefined && lstCashier != null && lstCashier.length > 0) {
						Utils.bindStaffCbx('#crashierStaff', lstCashier, null, 206, function(item) {
							CustomerDebitAuto.getCurrentDebit();
						});
					}
					if (data.payReceivedNumber != undefined && data.payReceivedNumber != null) {
						$('#payReceivedNumber').val(data.payReceivedNumber);
					}
					if (CustomerDebitAuto._isLoad) {
						CustomerDebitAuto.initGrid();
						CustomerDebitAuto._isLoad = false;
					}
					CustomerDebitAuto.getCurrentDebit();
					$('#divOverlay').hide();
				}
			}
		});
	},
	
	/**
	 * khoi tao grid
	 * @author trietptm
	 * @since Dec 21, 2015
	 */
	initGrid: function() {
		var params = new Object();
		params.shopCode = $('#shop').combobox('getValue');
		$('#dg').datagrid({
			url:'/customerdebit/auto/getinfo',		    	      
		    width: $(window).width() - 55,
		    height: 350,
		    rownumbers: true,
		    fitColumns: true,
		    singleSelect: true,
		    //scrollbarSize : 0,
		    queryParams: params,	   
		    columns:[[
		        {field: 'debitId', hidden: true},
//	 	        {field: 'debitId',checkbox:true},
				{field: 'shortCode', title: 'Khách hàng', width: 150, align: 'left', resizable: false, formatter: function(value, row, index) {
					return Utils.XSSEncode(row.shortCode + ' - ' + row.customerName);
				}},
				{field: 'orderNumber', title: 'Số đơn hàng/<br/>Số phiếu điều chỉnh công nợ', width: 100 , resizable: false, align: 'left'},
				{field: 'orderDate', title: 'Ngày', width: 80, align: 'center', resizable: false, formatter: CommonFormatter.dateTimeFormatter},
				{field: 'nvbhCodeName', title: 'NVBH', width: 100, resizable: false, align: 'left', formatter: function (v, r, i) {
					if (v != undefined && v != null) {
						return Utils.XSSEncode(v + " - " + r.nvbhName);
					}
					return "";
				}},
				{field: 'nvghCodeName', title: 'NVGH', width: 100, resizable: false, align: 'left', formatter: function (v, r, i) {
					if (v != undefined && v != null) {
						return Utils.XSSEncode(v + " - " + r.nvghName);
					}
					return "";
				}},
				{field: 'nvttCodeName', title: 'NVTT', width: 100, resizable: false, align: 'left', formatter: function (v, r, i) {
					if (v != undefined && v != null) {
						return Utils.XSSEncode(v + " - " + r.nvttName);
					}
					return "";
				}},
				{field: 'total', title: 'Số tiền', width: 100, align: 'right', resizable: false, formatter: CustomerDebitAutoFormatter.signedNumberFormatter},
				{field: 'totalPay', title: 'Đã trả', width: 100, align: 'right', resizable: false, formatter:function(val, row, index) {
					if (row != undefined && row != null) {
						return formatCurrency(Math.abs(row.totalPay)) + '&nbsp;<span style="color:#f00;">(' + formatCurrency(Math.abs(row.discountAmount)) + ')</span>';
					}
					return '';
				}},
				{field: 'payAmt', title: 'Giá trị trả', width: 100, align: 'right', resizable: false, formatter: CustomerDebitAutoFormatter.signedNumberFormatter} 
			]],    
			onLoadSuccess :function(data) {
				GridUtil.updateDefaultRowNumWidth("#gridContainer", "dg");
				$('.ErrorMsgStyle').hide();
				$('.SuccessMsgStyle').hide();
				if (data != null && data.warning != null) {
					var warning = data.warning;
					if (!isFirstLoad) {
						$("#beforePay").val(formatCurrency(parseInt(data.totalIsReceived), 10));
						if (warning == 4) {
							$('#errMsgTop').html('Mã khách hàng không tồn tại trong hệ thống.').show();
							$('#shortCode').focus();
							$('#afterPay').val('');
							return;
						}
						if(warning != 1 && data.rows.length == 0) {
							if ($('#status').val() == 2) {//phieu chi
								$('#errMsgTop').html('Khách hàng không còn đơn hàng trả nào chưa thanh toán').show();
							} else if ($('#status').val() == 1) {//phieu thu
								$('#errMsgTop').html('Khách hàng không còn đơn hàng nợ nào').show();
							}
							return;
						}
						if (warning == 1) {
//	 					 $('#warning1').html("Số tiền phải được thanh toán hết.Gợi ý: có thể trả tối đa <span class=\"myWarning\">"+ <s:property value="totalIsReceived"/> + "</span> VND").show();
						 $('#errMsgTop').html('Số tiền nhập vào lớn hơn số tiền còn nợ của KH .Số tiền còn nợ của KH <b>' + $('#shortCode').val().toUpperCase() + '</b> là: '+ formatCurrency(parseInt(data.totalIsReceived), 10) +' VNĐ').show();
						 $('#afterPay').val('');
						 $('#valuePay').focus();
						} else if (warning == 2) {
							if (data.status != null && data.status == 2) {
								$('#warning2').html('Số tiền còn phải chi: ' + formatCurrency(parseInt(data.ttt), 10) + ' VNĐ').show();
							} else {
								$('#warning2').html('Số tiền còn nợ của KH <b>' + $('#shortCode').val().toUpperCase() + '</b> là: ' + formatCurrency(parseInt(data.ttt), 10) + ' VNĐ').show();	
							}
							$('#btnAuto').show();
							myFun();
						} else if (warning == 3) {
	 						$('#warning3').html('Nợ của KH: <b> ' + $('#shortCode').val().toUpperCase() + ' </b> với NPP đã được thanh toán hết.').show();
							$('#afterPay').val('');
							myFun();
						}
					}
					isFirstLoad = false;
				}

				$('#dg').datagrid('unselectAll');
				$('.datagrid-header-rownumber').html('STT');
			   	$('.datagrid-header-row td div').css('text-align','center');
			   	//updateRownumWidthForJqGrid('.easyui-dialog');
		   	 	$(window).resize();
			}
		});
	},
	
	/**
	 * lay cong no thanh toan
	 * @author trietptm
	 * @since Dec 25, 2015
	 */
	getCurrentDebit: function() {
		$('.ErrorMsgStyle').hide();
		$('.SuccessMsgStyle').hide();
		var msg = Utils.getMessageOfRequireCheck('shop', jsp_common_shop_name_lable, true);
		var fromDate = $('#fromDate').val().trim();
		var toDate = $('#toDate').val().trim();
		if (fromDate == '__/__/____') {
			$('#fromDate').val('');
		}
		if (toDate == '__/__/____') {
			$('#toDate').val('');
		}
		if (msg.length == 0) {
			msg = Utils.getMessageOfRequireCheck('shortCode','Khách hàng');
		}		
		if (msg.length == 0) {
			msg = Utils.getMessageOfSpecialCharactersValidate('shortCode','Khách hàng', Utils._CODE);
		}
		//
		if (msg.length == 0) {
			msg = Utils.getMessageOfRequireCheck('status','Loại chứng từ', true);
		}
		if (msg.length == 0 && fromDate != null && fromDate.length > 0) {
			msg = Utils.getMessageOfInvalidFormatDate('fromDate', 'Từ ngày');
		}
		
		if (msg.length == 0 && toDate != null && toDate.length > 0) {
			msg = Utils.getMessageOfInvalidFormatDate('toDate', 'Đến ngày');
		}
		
		if (msg.length == 0 && fromDate != null && fromDate.length > 0 && toDate != null && toDate.length > 0 && !Utils.compareDate(fromDate, toDate)) {
			msg = msgErr_fromdate_greater_todate;
			$('#fromDate').focus();
		}
		
		if (msg.length > 0) {
//			$('#errMsgTop').html(msg).show();
			return false;
		}
		if (_isFullPrivilege || _MapControl.get("btnExportPOAutoReport") == 2) {
			$('#btnAuto').show();
		}
		var param = new Object();
		param.shopCode = $('#shop').combobox('getValue').trim();
		param.shortCode = $('#shortCode').val().trim();
		param.status = $('#status').val().trim();
		param.fromDate = $('#fromDate').val().trim();
		param.toDate = $('#toDate').val().trim();
		param.staffDeliverCode = $('#transferStaff').combobox('getValue').trim();
		param.staffPaymentCode = $('#crashierStaff').combobox('getValue').trim();
		$.ajax({
			type: "POST",
			url: '/customerdebit/auto/getcurrentDebit',
			data: ($.param(param, true)),
			dataType: "json",
			success: function(data) {
				if (!data.error) {
					$("#beforePay").val(formatCurrency(data.currentDebit));
					$("#afterPay").val("");
				} else {
					$('#errMsg').html(data.errMsg).show();
				}
			}
		});
		return false;
	}
};
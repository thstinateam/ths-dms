var ColGrid = {
		isCheck: 0, orderNumber: 1, customer: 2,  orderDate: 3, 
		nvbh: 4, nvgh: 5, nvtt: 6, remain: 8, payDiscount: 9,
		paied: 7, payAmount: 10, amount: 11 , bank: 12
};
var CustomerDebitBatchNew = {
	_xhrSave : null,
	_checkF9 : null,
	_lstOrderCheck:null,
	_lstDebitSelectId:null,
	_curRowId: null,
	_lstNVTT: null,
	_mapNVTT: new Map(),
	_rowIndexValueField: null,
	_arrayList: null,
	_mapRowIndex: null,
	_arrayRow: null,
	_remainMoney:0,
	_totalDebit:0,
	_listDataGrid: null,
	_mapDataGrid:null,
	_type:null,
	_lstBank: null,
	_staffCodeNVTTByChange: 'UNCHECKALL',
	_flagCheckAll: true,
	_isLoad: true,
	styleCellRenderer: function (instance, td, row, col, prop, value, cellProperties) {
		if(col == ColGrid.isCheck){
			Handsontable.renderers.CheckboxRenderer.apply(this, arguments);
			$(td).css({
				  "vertical-align": "middle",
				  "text-align": "center"
		    });
			$(td).attr("idx", row);
		} else if (col == ColGrid.orderNumber || col == ColGrid.customer 
				|| col == ColGrid.orderDate || col == ColGrid.nvbh || col == ColGrid.nvgh) {
			Handsontable.renderers.TextRenderer.apply(this, arguments);
		} else if (col == ColGrid.nvtt) {
			Handsontable.renderers.AutocompleteRenderer.apply(this,arguments);
		} else if (col == ColGrid.remain || col == ColGrid.amount || col == ColGrid.payDiscount || col == ColGrid.payAmount) {
			Handsontable.renderers.NumericRenderer.apply(this,arguments);
		} else if (col == ColGrid.paied) {
			var html = '';
			if (value != null) {
				 html = formatCurrency(Math.abs(value.pay)) + '&nbsp;<span style="color:#f00;">(' + formatCurrency(Math.abs(value.discount)) + ')</span>';
			 }
			td.innerHTML = html;
			$(td).css({textAlign: 'right'});
		} else if (col == ColGrid.bank) {
			Handsontable.renderers.AutocompleteRenderer.apply(this,arguments);
		}
		var rowData = $('#dg').handsontable('getInstance').getDataAtRow(row);
		switch (rowData.status) {
		case 0:
			break;
		case 1:
			if (rowData.isCheck) {
				$(td).css({background: '#86CE9D', color: 'black', fontWeight: 'bold'});
			}
			break;
		case 2:
			if (rowData.isCheck) {
				$(td).css({background: '#E78686', color: 'black', fontWeight: 'bold'});
			}
		}
	},
	initGrid: function() {
		$('#dg').handsontable({
			data: [],
			colWidths: [30, 110, 135, 75, 110, 110, 120, 200, 100, 110, 110, 110,130],
			outsideClickDeselects: true,
			multiSelect : false,
			rowHeaders: true,
			manualColumnResize: true,
			width: $(window).width() - 45,
			colHeaders: function (col) {
	           	switch (col) {
                case 0:
                    var txt = "<input type='checkbox' class='checker' id='cbxCheckAll' onchange='CustomerDebitBatchNew.cbxCheckAllChange();' ";
                    txt += CustomerDebitBatchNew.isCheckedAll() ? 'checked="checked"' : '';
                    txt += " />";
                    return txt;
                case 1:
                    return "Số đơn hàng";
                case 2:
                    return "Khách hàng";
                case 3:
                    return "Ngày";
                case 4:
                    return "NVBH";
                case 5:
                    return "NVGH";
                case 6:
                    return "NVTT";
                case 7:
                    return "Đã trả";
                case 8:
                    return "Còn lại";
                case 9:
                    return "Chiết khấu";
                case 10:
                    return "Giá trị thanh toán";
                case 11:
                	return "Tổng tiền ĐH";
                case 12:
                	return "Ngân hàng";
	           	}
			},
			columns: [
			      {type: 'checkbox', data: 'isCheck'},
			      {readOnly: true, data: 'orderNumber'},
			      {readOnly: true, data: 'customer'},
			      {readOnly: true, data: 'orderDate'},
			      {readOnly: true, data: 'nvbhName'},
			      {readOnly: true, data: 'nvghName'},
			      {readOnly: false, allowInvalid: false, strict: true, type: 'dropdown', source: CustomerDebitBatchNew._mapNVTT.keyArray, data: 'nvttCodeName'},
			      {readOnly: true, data: 'paied'},
			      {readOnly: true, type: 'numeric', format: '0,0', data: 'remain'},
			      {readOnly: false, allowInvalid: false, type: 'numeric', format: '0,0', data: 'payDiscount'},
			      {readOnly: false, allowInvalid: false, type: 'numeric', format: '0,0', data: 'payAmount'},
			      {readOnly: true, type: 'numeric', format: '0,0', data: 'total'},
			      {readOnly: false, allowInvalid: false, strict: true, type: 'dropdown', source: CustomerDebitBatchNew._lstBank, data: 'bankCode'},
			 ],
			 cells: function (row, col, prop) {
				 this.renderer = CustomerDebitBatchNew.styleCellRenderer;
		     },
			 afterChange: function(changes, source) {
				  if (changes != null && $.isArray(changes)) {
					  for (var i = 0; i < changes.length; i++) {
						  	var htGrid = $('#dg').handsontable('getInstance');
							var row = changes[i];
							var r = row[0];
							var c = row[1];
							var oldValue = row[2];
							var newValue = row[3];
							var rowData = htGrid.getDataAtRow(r);
							if (c == 'isCheck') {
								CustomerDebitBatchNew.changeWhenCheck(newValue, rowData, r);
							} else if (c == 'payDiscount') {
								if (!isNullOrEmpty(newValue) && (newValue < 0 || newValue >= rowData.remain)) {
									rowData.status = 2;
								} else {
									if(!isNullOrEmpty(newValue)){
										rowData.status = 1;
										rowData.payAmount = rowData.remain - newValue;
									}
								}
							} else if (c == 'payAmount') {
								//rowData.payDiscount = null;
								if (!isNullOrEmpty(newValue) && newValue >0) {
									if (newValue > rowData.remain - rowData.payDiscount) {
										rowData.status = 2;
										rowData.payAmount = rowData.remain - rowData.payDiscount;
									} else {
										if (rowData.payDiscount == null
												|| (rowData.payDiscount>=0 && rowData.payDiscount <= rowData.remain)) {
											rowData.status = 1;
										} else {
											rowData.status = 2;
										}
									}
								} else {
									rowData.status = 2;
								}
							} else if (c == 'nvttCodeName' && rowData.isCheck) {
								CustomerDebitBatchNew.changeNVTT(newValue, rowData.debitId);
							} else if (c == 'bankCode' && rowData.isCheck) {
								CustomerDebitBatchNew.changeBank(newValue, rowData.debitId);
							}
							htGrid.render();
							CustomerDebitBatchNew.setTotalPay();
					  }
				  }
			  },
			  afterRender:function() {
				  if ($('#cbxCheckAll').prop('checked')) {
					  $('#dg').closest('table').find('input.htCheckboxRendererInput').prop("checked", true);
				  }
			  },
			  beforeKeyDown: function(event) {
				  var htGrid = $('#dg').handsontable('getInstance');
					var rowSelect = htGrid.getSelected();
					if (rowSelect != undefined && rowSelect != null) {
						var r = rowSelect[0];
						var c = rowSelect[1];
						if (c == ColGrid.payDiscount) {
						    if (event.keyCode == keyCodes.TAB) {
								event.stopImmediatePropagation();
								if(event.shiftKey){
									htGrid.selectCell(r,ColGrid.isCheck);
								} else {
									htGrid.selectCell(r,ColGrid.payAmount);
								}
								event.preventDefault();
						    }
						} else if (event.keyCode == keyCodes.DELETE && (c == ColGrid.payDiscount || c == ColGrid.payAmount)) {
							event.stopImmediatePropagation();
							htGrid.setDataAtCell(r,ColGrid.payDiscount,null);
							htGrid.setDataAtCell(r,ColGrid.payAmount,null);
							htGrid.selectCell(r,c == ColGrid.payAmount?ColGrid.payAmount:ColGrid.payDiscount);
						} else if (c == ColGrid.payAmount) {
							if (event.keyCode==keyCodes.TAB) {
								event.stopImmediatePropagation();
								if(event.shiftKey){
									htGrid.selectCell(r,ColGrid.payDiscount);
								} else {
									if(htGrid.countRows() > r+1){
										htGrid.selectCell(r+1,ColGrid.isCheck);
									}
								}
								event.preventDefault();
							}
						} else if (c == ColGrid.isCheck) {
							if (event.keyCode == keyCodes.TAB) {
								event.stopImmediatePropagation();
								if (event.shiftKey) {
									htGrid.deselectCell();
									$('#payreceiptValue').focus();
								} else {
									htGrid.selectCell(r,ColGrid.payDiscount);
								}
								event.preventDefault();
							}
						}
					}
			  },
		});
	},
	changeCheckbox : function(a) {
		if (a.checked) {
			$('.checker').on('click', function () {
				$(this).closest('table').find('input.htCheckboxRendererInput').prop("checked", this.checked);
				$.uniform.update();//update UniformJS
			});
		}
	},
	
	isCheckedAll: function() {
		var data = $('#dg').handsontable('getData');
		if (data != null && data.length >0) {
	        for (var i = 0, ilen = data.length; i < ilen; i++) {
	            if (!data[i].isCheck) {
	                return false;
	            }
	        }
	        return true;
		}
		return false;
	},
	changeWhenCheck: function(isCheck, rowData, i) {
		if (isCheck) {
			if (rowData.status != 1) {
				rowData.status = 1;
				rowData.payDiscount = null,
				rowData.payAmount = rowData.remain;
			}
			if (CustomerDebitBatchNew.htCheckboxRendererInputChecked() && CustomerDebitBatchNew._staffCodeNVTTByChange != 'UNCHECKALL') {
				rowData.nvttCodeName = CustomerDebitBatchNew._staffCodeNVTTByChange;//				
			} else {
				rowData.nvttCodeName = CustomerDebitBatchNew.getNVTTCheck();
			}
			CustomerDebitBatchNew.cbxCheckAllUpdate();
			rowData.bankCode = CustomerDebitBatchNew.getBankCheck();
		} else {
			rowData.status = 0;
			rowData.payDiscount = null,
			rowData.payAmount = null;
			rowData.nvttCodeName = rowData.nvttBk;
			rowData.bankCode = rowData.bankCode;
		}
	},
	/**
	 * Ham nay dung de xu ly checkAll
	 * 
	 * @author hunglm16
	 * @since November 20,2014
	 * */
	changeWhenCheckNew: function(isCheck, rowData, i) {
		if (isCheck) {
			if (rowData.status != 1) {
				rowData.status = 1;
				rowData.payDiscount = null,
				rowData.payAmount = rowData.remain;
			}
			if (CustomerDebitBatchNew.htCheckboxRendererInputChecked() && CustomerDebitBatchNew._staffCodeNVTTByChange != 'UNCHECKALL') {
				rowData.nvttCodeName = CustomerDebitBatchNew._staffCodeNVTTByChange;//				
			} else {
				rowData.nvttCodeName = CustomerDebitBatchNew.getNVTTCheck();
			}
			rowData.bankCode = CustomerDebitBatchNew.getBankCheck();
		} else {
			rowData.status = 0;
			rowData.payDiscount = null,
			rowData.payAmount = null;
			rowData.nvttCodeName = rowData.nvttBk;
			rowData.bankCode = rowData.bankCode;
		}
	},
	/**
	 * Kiem tra co su kien check trong danh sach class htCheckboxRendererInput hay khong
	 * 
	 * @author hunglm16
	 * @since November 20,2014
	 * @description dung thuat toan binary quicksearch
	 * */
	htCheckboxRendererInputChecked: function() {
		var htGrid = $('#dg').handsontable('getInstance');
		var rows = htGrid.getData();
		if (rows != undefined && rows != null) {
		   for(var i=0, sz = rows.length; i < sz; i++){
		     if(rows[i].isCheck) {
		          return true;
		     }
		   }
		}
		return false;
	},
	/**
	 * Check All cbxCheckAll
	 * 
	 * @author hunglm16
	 * @since November 20,2014
	 * */
	cbxCheckAllUpdate: function (flag) {
		var htGrid = $('#dg').handsontable('getInstance');
		var rows = htGrid.getData();
		if (rows != undefined && rows != null) {
		   for(var i=0, sz = rows.length; i < sz; i++){
			   if(!rows[i].isCheck) {
				   $('#cbxCheckAll').prop('checked', false);
				   return;
			   }
		   }
		}
		if (flag!=undefined && flag!=null && flag) {
			$('#cbxCheckAll').prop('checked', true).change();
		}
	},
	/**
	 * Check All cbxCheckAll Change
	 * 
	 * @author hunglm16
	 * @since November 20,2014
	 * */
	cbxCheckAllChange: function () {
		var flagCheckAll = $('#cbxCheckAll').prop('checked');
		var htGrid = $('#dg').handsontable('getInstance');
		var rows = htGrid.getData();
		if (rows != undefined && rows != null) {
		   if (!flagCheckAll) {
			   CustomerDebitBatchNew.getDataInGrid();
		   } else {
			   for(var i=0, sz = rows.length; i < sz; i++){
				   rows[i].isCheck = true;
				   CustomerDebitBatchNew.changeWhenCheckNew(true, rows[i], i);
			   }
			   $('#dg').closest('table').find('input.htCheckboxRendererInput').prop("checked", true);
		   }
		}
		CustomerDebitBatchNew.setTotalPay();
	},
	changeNVTT: function(staffCode,debitId) {
		var data = $('#dg').handsontable('getData');
		if (data != null && data.length >0) {
	        for (var i = 0, ilen = data.length; i < ilen; i++) {
	            if (data[i].isCheck && data[i].debitId == debitId) {
	            	data[i].nvttCodeName = staffCode;
	            	CustomerDebitBatchNew._staffCodeNVTTByChange = staffCode;
	            }
	        }
		}
	},
	changeBank: function(bankCode,debitId) {
		var data = $('#dg').handsontable('getData');
		if (data != null && data.length >0) {
	        for (var i = 0, ilen = data.length; i < ilen; i++) {
	            if (data[i].isCheck && data[i].debitId == debitId) {
	            	data[i].bankCode = bankCode;
	            }
	        }
		}
	},
	getNVTTCheck: function(){
		var data = $('#dg').handsontable('getData');
		var staffCode = null;
		for (var i = 0, ilen = data.length; i < ilen; i++) {
            if (data[i].isCheck) {
            	staffCode = data[i].nvttCodeName;
            	if (staffCode!=undefined && staffCode!=null) {            		
            		CustomerDebitBatchNew._staffCodeNVTTByChange = data[i].nvttCodeName;
            	}
            	break;
            }
        }
		return staffCode;
	},
	getBankCheck: function(){
		var data = $('#dg').handsontable('getData');
		var bankCode = null;
		for (var i = 0, ilen = data.length; i < ilen; i++) {
			if (data[i].isCheck) {
				bankCode = data[i].bankCode;
				break;
			}
		}
		return bankCode;
	},
	getTotalPayDiscount: function(){
		var listData = $('#dg').handsontable('getInstance').getData();
		var total = 0;
		for(var i=0, size = listData.length;i<size;i++){
			var dis = listData[i].payDiscount;
			if(listData[i].status == 1 && !isNullOrEmpty(dis) && listData[i].isCheck){
				total += dis;
			}
		}
		return total;
	},
	getTotalPayAmount: function(){
		var listData = $('#dg').handsontable('getInstance').getData();
		var total = 0;
		for(var i=0, size = listData.length;i<size;i++){
			var amo = listData[i].payAmount;
			if(listData[i].status == 1 && !isNullOrEmpty(amo) && listData[i].isCheck){
				total += amo;
			}
		}
		return total;
	},
	setTotalPay: function(){
		var listData = $('#dg').handsontable('getInstance').getData();
		var totalDis = 0;
		var totalAmo = 0;
		for(var i=0, size = listData.length;i<size;i++){
			var dis = listData[i].payDiscount;
			var amo = listData[i].payAmount;
			if(listData[i].status == 1 && listData[i].isCheck){
				totalDis += Number(dis);
				totalAmo += Number(amo);
			}
		}
		$('#cDiscount').html(formatCurrency(totalDis));
		$('#cAmount').html(formatCurrency(totalAmo));
	},
	loadPage: function() {
		$('#dg').on('	', 'input.checker', function (event) {
			var htGrid = $('#dg').handsontable('getInstance');
			var data = htGrid.getData();
			if(data !=null){
		        var current = !$('input.checker').is(':checked'); //returns boolean
	            for (var i = 0, ilen = data.length; i < ilen; i++) {
	            	data[i].isCheck = current;
	            	CustomerDebitBatchNew.changeWhenCheck(current, data[i], i);
	            }
	            htGrid.render();
			}
	    });
		$('#payreceiptValue').bind('keypress',function(event){
			var htGrid = $('#dg').handsontable('getInstance');
			if(!event.shiftKey && event.keyCode == keyCodes.TAB && htGrid.countRows()>0){
				htGrid.selectCell(0, ColGrid.isCheck);
				event.preventDefault();
			}
		});
	},
	getDataInGrid: function(varParams) {
		CustomerDebitBatchNew.initGrid();
		$('.ErrorMsgStyle').html('').hide();
		$("#payerName").val("");
		$("#payerAddress").val("");
		$("#paymentReason").val("");
		$("#payreceiptCode").parent().find("input[type=text]").removeAttr("disabled");
    	enable("btnExportPayment");
    	enable("btnPay");
		if (varParams == undefined || varParams == null) {
			$("#loadingDebit").show();
			var msg = '';
			msg = Utils.getMessageOfRequireCheck('shop', jsp_common_shop_name_lable, true);
			if (msg.length == 0) {
				msg = Utils.getMessageOfRequireCheck('type', 'Loại chứng từ', true);
			}
			var fromDate = $('#fromDate').val().trim();
			var toDate = $('#toDate').val().trim();
			if (fromDate == '__/__/____') {
				$('#fromDate').val('');
				fromDate = '';
			}
			if (toDate == '__/__/____') {
				$('#toDate').val('');
				toDate = '';
			}
			if (msg.length == 0 && fromDate.length > 0) {
				msg = Utils.getMessageOfInvalidFormatDate('fromDate', 'Từ ngày');
			}
			if (msg.length ==0 && toDate.length > 0) {
				msg = Utils.getMessageOfInvalidFormatDate('toDate', 'Đến ngày');
			}
			if (msg.length == 0 && fromDate.length > 0 && toDate.length > 0 &&  !Utils.compareDate(fromDate, toDate)) {
				msg = msgErr_fromdate_greater_todate;
				$('#fromDate').focus();
			}
			if (msg.length > 0) {
				$('#errMsgTop').html(msg).show();
				$("#loadingDebit").hide();
				return false;
			}
			var params = new Object();
			params.shortCode = $('#shortCode').val().trim();
			params.fromDate = $('#fromDate').val().trim();
			params.toDate = $('#toDate').val().trim();
			params.type = $('#type').val().trim();
			CustomerDebitBatchNew._type = $('#type').val().trim();
			params.staffDeliverCode = $('#transferStaff').combobox('getValue').trim();
			params.staffPaymentCode = $('#crashierStaff').combobox('getValue').trim();
			params.staffCode = $('#staffCode').combobox('getValue').trim();
			params.lstOrderNumbers = $("#orderNumber").val().trim();
			params.shopCode = $('#shop').combobox('getValue').trim();
			varParams = params;
		}
		$.ajax({
		    url: "/customerdebit/batch/getinfo",
		    dataType: 'json',
		    data: varParams,
		    type: 'POST',
		    success: function (res) {
		    	if (res.error == undefined || res.error == null || res.error != true) {
			    	var data = res.rows;
			    	if(data != null){
			    		CustomerDebitBatchNew._mapDataGrid = new Map();
			    		var totalPayAmount = 0;
			    		for (var i = 0; i < data.length; i++) {
			    			data[i].status = 0;
			    			data[i].isCheck = false;
			    			data[i].customer = Utils.XSSEncode(data[i].shortCode + ' - ' + data[i].customerName);
			    			data[i].orderDate = $.datepicker.formatDate('dd/mm/yy', new Date(data[i].orderDate));
			    			data[i].paied = {pay: data[i].totalPay, discount: data[i].discountAmount};
			    			data[i].payDiscount = null;
			    			data[i].payAmount = null;
			    			var nvtt = null;
			    			if (data[i].nvttCodeName != null) {
			    				nvtt = Utils.XSSEncode(data[i].nvttCodeName + " - " + data[i].nvttName);
			    			}
			    			data[i].nvttBk = nvtt;
			    			data[i].nvttCodeName = nvtt;
			    			data[i].bankCode = null;
			    			if(CustomerDebitBatchNew._type == 0){
			    				data[i].remain *= -1;
			    				data[i].total *= -1;
			    			}
			    			totalPayAmount += data[i].remain;
			    		}
			    		CustomerDebitBatchNew._listDataGrid = data;
			    		$("#loadingDebit").hide();
		      		$('#dg').handsontable('getInstance').loadData(data);
		      		if (res != null && res.payreceiptCode != null) {
						$('#payreceiptCode').val(res.payreceiptCode);
					}	
		      		$('#cAmountByGrid').html(formatCurrency(totalPayAmount));
		      		$('#payreceiptValue').val('');
		      		$('#payreceiptValue').focus();
		      		$('#cAmount').html(0);
		      		$('#cDiscount').html(0);
			    	} else {
			    		$('#dg').handsontable('getInstance').loadData([]);
			    	}
			    	if (res != null && res.debitPreRemain != undefined && res.debitPreRemain != null) {
					$('#debitPreRemain').val(formatCurrencyInterger(res.debitPreRemain));
					$('#debitPostRemain').val(formatCurrencyInterger(res.debitPreRemain));
					CustomerDebitBatchNew._totalDebit = res.debitPreRemain;
				}
			    	$('#cbxCheckAll').prop('checked', false);
			    	CustomerDebitBatchNew._checkF9 = false;
		    	} else {
		    		$('#errMsg').html(res.errMsg).show();
		    		$('#dg').handsontable('getInstance').loadData([]);
		    	}
		    	$("#loadingDebit").hide();
		    }
		  });
	},
	import: function(printOnly) {
		$('.ErrorMsgStyle').html('').hide();
		var msg = '';
		msg = Utils.getMessageOfRequireCheck('shop', jsp_common_shop_name_lable, true);
		if (msg.length ==0) {
			msg = Utils.getMessageOfSpecialCharactersValidate('shortCode','Mã khách hàng',Utils._CODE );
		}
		if (msg.length == 0 && $('#type').val() == -1 ) {
			msg = 'Bạn chưa chọn loại chứng từ. Yêu cầu chọn loại chứng từ';
		}
		if (msg.length ==0) {
			msg = Utils.getMessageOfRequireCheck('payreceiptCode','Số chứng từ' );
		}
		if (msg.length ==0) {
			msg = Utils.getMessageOfSpecialCharactersValidate('payreceiptCode','Số chứng từ',Utils._CODE );
		}
		if (msg.length ==0) {
			msg = Utils.getMessageOfRequireCheck('payreceiptValue','Số tiền', undefined, true);
		}
		var payreceiptValue = Utils.returnMoneyValue($('#payreceiptValue').val().trim());
		if (payreceiptValue.length > 20) {
			msg = 'Giá trị Số tiền nhập vào chỉ giới hạn 20 ký tự';
		}
		var type = CustomerDebitBatchNew._type;
		if (msg.length ==0 &&(isNaN(payreceiptValue) || payreceiptValue <=0)) {
			if (type==1) {
				msg ='Số tiền phiếu thu phải là số nguyên dương';
			} else {
				msg ='Số tiền phiếu chi phải là số nguyên dương';
			}
		}
		
		if (msg.length > 0) {
			$('#errMsg').html(msg).show();
			return false;
		}		
		var lstCustomerDebitID = new Array();
		var lstPayAmount = [];
		var lstDiscount = [];
		var lstStaffCode = [];
		var lstBankCode = [];
		var htGrid = $('#dg').handsontable('getInstance');
		var rows = htGrid.getData();
		if (rows != undefined && rows != null) {
			for (var i=0, sz = rows.length; i < sz; i++) {
				if (rows[i].isCheck) {
					if (rows[i].status == 2) {
						$('#errMsg').html('Giá trị thanh toán của đơn hàng ' + Utils.XSSEncode(rows[i].orderNumber) + ' lớn hơn số tiền còn nợ của đơn hàng!').show();
						htGrid.selectCell(i,ColGrid.isCheck);
						return false;
					} else if(rows[i].status == 1) {
						lstCustomerDebitID.push(rows[i].debitId);
						if (CustomerDebitBatchNew._type == 0) {
							lstPayAmount.push(rows[i].payAmount*(-1));
						} else {
							lstPayAmount.push(rows[i].payAmount);
						}
						lstDiscount.push(isNullOrEmpty(rows[i].payDiscount) ? 0 :rows[i].payDiscount);
						if (rows[i].nvttCodeName == null) {
							lstStaffCode.push('');
						} else {
							lstStaffCode.push(CustomerDebitBatchNew._mapNVTT.get(rows[i].nvttCodeName));
						}
						if (rows[i].bankCode == null) {
							lstBankCode.push('');
						} else {
							lstBankCode.push(rows[i].bankCode);
						}
					}
				}
			}
		}
		if (lstCustomerDebitID.length <=0 ) {
			$('#errMsg').html("Chưa chọn đơn hàng để thanh toán").show();
			return false;
		}
		var cAmount = Number(Utils.returnMoneyValue($('#cAmount').html().trim()));
		var cPay = Number(payreceiptValue);
		if (cPay - cAmount != 0) {
			$('#errMsg').html('Giá trị phiếu ' + (type==1 ? 'thu' : 'chi') + ' phải bằng tổng tiền thanh toán.').show();
			return false;
		}
		
		var params = new Object();
		params.shopCode = $('#shop').combobox('getValue').trim();
		params.shortCode = $('#shortCode').val().trim();
		params.type = CustomerDebitBatchNew._type;//$('#type').val();
		params.payreceiptCode = $('#payreceiptCode').val().trim();
		params.payreceiptValue = payreceiptValue;
		params.lstCustomerDebitID = lstCustomerDebitID;
		params.lstPayAmount = lstPayAmount;
		params.lstDiscount = lstDiscount;
		params.lstStaffCode = lstStaffCode;
		params.lstBankCode = lstBankCode;
		params.payerName = $("#payerName").val().trim();
		params.payerAddress = $("#payerAddress").val().trim();
		params.paymentReason = $("#paymentReason").val().trim();
		try {
			params.reportCode = $('#function_code').val();
		} catch(e) {
			// Oop! Error
		}
		msg ='Bạn có muốn thanh toán theo các đơn hàng này ?';
		var url = null;
		if (printOnly) {
			url = "/customerdebit/batch/export-payment";
		} else {
			url = "/customerdebit/batch/import";
		}
		Utils.addOrSaveData(params, url, null,  'errMsg', function(data) {
			if (printOnly) {
				if (!data.error && data.path) {
//					var width = $(window).width();
//					var height = $(window).height();
//					window.open('/commons/open-tax-view?windowWidth='+width+'&windowHeight='+height);
					
					var filePath = ReportUtils.buildReportFilePath(data.path);
					ReportUtils.openInNewTab(filePath);
				}
				CustomerDebitBatchNew.reloadPaymentInfo(data.lstDebitVO);
			} else {
				CustomerDebitBatchNew.getDataInGrid();
			}
		}, 'loadingImport',null, null, msg,function(dataErr) {
			if(dataErr.errorReloadPage == true){
				window.location.href = '/customerdebit/batch/info-new';
			}
		});
		return false;
	},
	reloadPaymentInfo: function(data) {
		CustomerDebitBatchNew._mapDataGrid = new Map();
    		var totalPayAmount = 0;
    		for (var i = 0; i < data.length; i++) {
    			data[i].status = 0;
    			data[i].isCheck = false;
    			data[i].customer = Utils.XSSEncode(data[i].shortCode + ' - ' + data[i].customerName);
    			data[i].orderDate = $.datepicker.formatDate('dd/mm/yy', new Date(data[i].orderDate));
    			data[i].paied = {pay: data[i].totalPay, discount: data[i].discountAmount};
    			data[i].payDiscount = data[i].discountAmt;
    			data[i].payAmount = data[i].payAmt;
    			var nvtt = null;
    			if (data[i].nvttCodeName != null) {
    				nvtt = Utils.XSSEncode(data[i].nvttCodeName + " - " + data[i].nvttName);
    			}
    			data[i].nvttBk = nvtt;
    			data[i].nvttCodeName = nvtt;
    			data[i].bankCode = null;
    			if(CustomerDebitBatchNew._type == 0){
    				data[i].remain *= -1;
    				data[i].total *= -1;
    			}
    			totalPayAmount += data[i].remain;
    		}
    		CustomerDebitBatchNew._listDataGrid = data;
    		$("#loadingDebit").hide();
    		$('#dg').handsontable('getInstance').loadData(data);
    		$("#payreceiptCode").parent().find("input[type=text]").attr("disabled", "disabled");
    		disabled("btnExportPayment");
    		disabled("btnPay");
	},
	viewExcel:function(){		
		var importFileName = $('#excelFile').val();
		if(importFileName.length > 0){
			$('#btnImport').attr('disabled',false);
			$('#btnImport').removeClass('BtnGeneralDStyle');
		}		
		$('#isView').val(1);
		$('#importFrm').submit();
		return false;
	},
	importExcel:function(){		
		$('#btnImport').attr('disabled',true);
		$('#btnImport').addClass('BtnGeneralDStyle');
		msg = 'Bạn có muốn nhập từ file này không?';
		$.messager.confirm('Xác nhận', msg, function(r){
			if (r){
				$('#isView').val(0);
				$('#importFrm').submit();
			}
		});	
		return false;
	},		
	loadInfo : function(){
		CustomerDebitBatchNew.getInfo(false,false,true);
	},
	getInfo : function(checkF9,isSave, isSaveExcel) {
		//$('.ErrorMsgStyle').html('').hide();
		if($('#shortCode').val().trim().length == 0){			
			return false;
		}
		$("#payerName").val("");
		$("#payerAddress").val("");
		$("#paymentReason").val("");
		$('#payreceiptValue').val('');
		$('#debitPreRemain').val($('#debitPostRemain').val());
		var params = new Object();
		//params.staffCode = $('#staffCode').val().trim();
		params.shopCode = $('#shop').combobox('getValue').trim();
		params.shortCode = $('#shortCode').val().trim();
		params.type = $('#type').val();
		if($('#type').val() == -1) {
			return false;
		}
		CustomerDebitBatchNew.getDataInGrid(params);
		/*if(checkF9) {
			CustomerDebitBatchNew._checkF9 = false;
		}		*/
		return false;
	},
	
	changePayReceiptValue: function() {
		var payreceiptValueTemp = $('#payreceiptValue').val();
		if(payreceiptValueTemp.length != 0) {
			var payreceiptValue = Number(Utils.returnMoneyValue(payreceiptValueTemp));
			var type = $('#type').val();
			var msg ='';
			if(isNaN(payreceiptValue) || payreceiptValue <=0){
				if(type==1){
					msg ='Số tiền phiếu thu phải là số nguyên dương';
				} else{
					msg ='Số tiền phiếu chi phải là số nguyên dương';
				}
				Alert('Thông báo',msg, function() {
					$('#payreceiptValue').val('');
					$('#payreceiptValue').focus();
				});
			} else{
				var heSo = 1;
				if(type== 0){
					heSo = -1;
				}
				CustomerDebitBatchNew._remainMoney = payreceiptValue;
				var cAmount = Number(Utils.returnMoneyValue($('#cAmount').html().trim()));
				$('#debitPostRemain').val(formatCurrency(CustomerDebitBatchNew._totalDebit - Math.min(payreceiptValue, cAmount) * heSo));
			}
			return false;
		}
	},
	changeCustomer: function() {
		if($('#shortCode').val().trim().length != 0){
			var url = 'shopCode='+ $('#shopCode').val() + '&shortCode='+$('#shortCode').val();
			$.getJSON('/customerdebit/auto/currentDebit?'+url, function(data){
				if(data.error) {
					$('#errMsg').html(data.errMsg).show();
				} else {
					$('#debitPreRemain').val(formatCurrencyInterger(data.currentDebit) );
					CustomerDebitBatchNew._totalDebit = data.currentDebit;
					$('#debitPostRemain').val('');
				}
			});
		}
		return false;
	},
	showImportExcelOnDialog:function(){		
		$('#importExcelContent').css("visibility", "visible");
		var html = $('#importExcelContent').html();
		$('#errExcelMsg').html('').hide();
		var msg = Utils.getMessageOfRequireCheck('shop', jsp_common_shop_name_lable, true);
		if (msg.length > 0) {
			$('#errMsgTop').html(msg).show();
			return false;
		}
		$('#importExcelContenDialog').dialog({  	       
	        closed: false,  
	        cache: false,  
	        modal: true,
	        /*width : 850,
	        height :450,*/
	        width: 470,
	        height: "auto",
	        onOpen: function(){
	        	/*$('.easyui-dialog #dgDetail').datagrid({					
					//autoRowHeight : true,
					rownumbers : true, 
					singleSelect: true,					
					//rowNum : 10,
					scrollbarSize : 0,
					fitColumns:true,
					//cache: false, 					
					width : $('#importExcelContenDialog').width()-20,
					//height:auto,
					//autoWidth: true,
				    columns:[[
						{field: 'shortCode',title: 'Mã KH',  width: 100,sortable:false,resizable:false, align: 'left', formatter:function(value, options, rowObject) {
							return Utils.XSSEncode(value);
						}},
						{field: 'type',title: 'Loại CT',  width: 100,sortable:false,resizable:false, align: 'left' , formatter:function(value, options, rowObject) {
							return Utils.XSSEncode(value);
						}},
						{field: 'payreceiptCode',title: 'Số CT',  width: 100,sortable:false,resizable:false, align: 'left', formatter:function(value, options, rowObject) {
							return Utils.XSSEncode(value);
						}},
						{field: 'money',title: 'Số tiền',  width: 100,sortable:false,resizable:false, align: 'right', formatter:function(cellValue, options, rowObject) {
							if(cellValue!=undefined && cellValue!=null){
								return formatCurrency(Math.abs(cellValue));
							}
							return '';
						} },
						{field: 'discount',title: 'Chiết khấu',  width: 100,sortable:false,resizable:false, align: 'right', formatter:function(cellValue, options, rowObject) {
							if(cellValue!=undefined && cellValue!=null){
								return formatCurrency(Math.abs(cellValue));
							}
							return '';
						}}
				    ]],
				    onLoadSuccess :function(data){   	 
				    	
				    }
	        	});
	        	$('.datagrid-header-rownumber').html('STT');
	         	$('.datagrid-header-row td div').css('text-align','center');*/
	        },
	        onClose:function(){
	        	$('#importExcelContent').html(html);
	        	$('#importExcelContent').css("visibility", "hidden");
	        	$('.easyui-dialog #dgDetail').datagrid('loadData', []);
	        	$('#excelFile').val('');
	        	$('#fakefilepc').val('');
	        }
		});
	},
	beforeImportExcel: function(){
		if(!previewImportExcelFile(document.getElementById("excelFile"))){
			return false;
		}		
		$('#errExcelMsg').html('').hide();		
		showLoadingIcon();
		return true;
	},
	afterImportExcelUpdate: function(responseText, statusText, xhr, $form){
		if($('#btnImport').length != 0) {
			enable('btnImport'); 
		}
		hideLoadingIcon();
		if (statusText == 'success') {				
	    	$("#responseDiv").html(responseText);		 
	    	var newToken = $('#responseDiv #newToken').val();
	    	console.log(newToken);
	    	if(newToken != null && newToken != undefined && newToken != '') {
	    		$('#token').val(newToken);	    		
	    		$('#tokenImport').val(newToken);
	    	}
	    	if(($('#errorExcel').html()!=null && $('#errorExcel').html().trim() == 'true') ||($('#errorExcelMsg').html() && $('#errorExcelMsg').html().trim().length > 0)){
	    		$('#errExcelMsg').html($('#errorExcelMsg').html()).show();
	    	} else {
	    		if($('#typeView').html().trim() == 'false'){
	    			var totalRow = parseInt($('#totalRow').html().trim());
	    			var numFail = parseInt($('#numFail').html().trim());
	    			var fileNameFail = $('#fileNameFail').html();
	    			var mes = format(msgErr_result_import_excel,(totalRow - numFail),numFail);		    		
	    			if(numFail > 0){
	    				mes+= ' <a href="' + fileNameFail +'">Xem chi tiết lỗi</a>';
	    			}
	    			if($('#excelFile').length!=0 && $('#fakefilepc').length!=0){
	    				try{$('#excelFile').val('');$('#fakefilepc').val('');}catch(err){}
	    			}
	    			$('.easyui-dialog').dialog('close');
	    			$('#btnSearch').click();
	    			setTimeout(function(){
	    				$('#errMsg').html(mes).show();
					}, 3000);
	    		}else{	    
	    			$('.easyui-dialog #dgDetail').datagrid("loadData", {total:0, rows:[]});
	    			$('.excel-item').each(function(){
	    				var row = new Object();
	    				//row.staffCode = $(this).attr('staffCode');
	    				row.shortCode = $(this).attr('shortCode');
	    				row.type = $(this).attr('typect') == '0' ? 'phiếu thu' : $(this).attr('typect') == '1' ? 'phiếu chi' : $(this).attr('typect');
	    				row.payreceiptCode = $(this).attr('payreceiptCode');
	    				row.money = $(this).attr('money');
	    				row.discount = $(this).attr('discount');
	    				row.errMsg = $(this).attr('errMsg');
	    				$('.easyui-dialog #dgDetail').datagrid('appendRow',row);
	    			});
	    		}
	    	}
	    }	
	},
	exportExcel: function() {
		$('#divOverlay').addClass('Overlay');
		$('#imgOverlay').show();
		$('#errMsg').html('').hide();
		$('#errExcelMsg').html('').hide();
		$('.ErrorMsgStyle').html('').hide();
		var msg = '';
		msg = Utils.getMessageOfRequireCheck('shop', jsp_common_shop_name_lable, true);
		if(msg.length == 0){
			msg = Utils.getMessageOfRequireCheck('type','Loại chứng từ', true);
		}
		var fromDate = $('#fromDate').val().trim();
		var toDate = $('#toDate').val().trim();
		if(fromDate == '__/__/____'){
			$('#fromDate').val('');
			fromDate = '';
		}
		if(toDate == '__/__/____'){
			$('#toDate').val('');
			toDate = '';
		}
		if(msg.length == 0 && fromDate.length > 0){
			msg = Utils.getMessageOfInvalidFormatDate('fromDate', 'Từ ngày');
		}
		if(msg.length ==0 && toDate.length > 0){
			msg = Utils.getMessageOfInvalidFormatDate('toDate', 'Đến ngày');
		}
		if(msg.length == 0 && fromDate.length > 0 && toDate.length > 0 &&  !Utils.compareDate(fromDate, toDate)){
			msg = msgErr_fromdate_greater_todate;
			$('#fromDate').focus();
		}
		if (msg.length > 0) {
			$('#errMsgTop').html(msg).show();
			return false;
		}
		
		
		var data = new Object();
		data.shopCode = $('#shop').combobox('getValue').trim();
		data.shortCode = $('#shortCode').val().trim();
		data.fromDate = $('#fromDate').val().trim();
		data.toDate = $('#toDate').val().trim();
		data.type = $('#type').val().trim();
		data.staffDeliverCode = $('#transferStaff').combobox('getValue').trim();
		data.staffPaymentCode = $('#crashierStaff').combobox('getValue').trim();
		data.staffCode = $('#staffCode').combobox('getValue').trim();
		var lstCustomerDebitID = new Array();
		var rowCheck = $('[name=debitId]');
		var rows = $('#dg').handsontable('getData');
		
		
		var listPayDiscountStr = [];
		var listPayAmountStr = [];
		var listRemainStr = [];
		var listTotalStr = [];
		var listNVTTStr = [];
		if (rows != undefined && rows != null) {
			for(var i=0, sz = rows.length; i < sz; i++){
				if(rows[i].isCheck) {
			          lstCustomerDebitID.push(rows[i].debitId);
			          if (rows[i].payDiscount!=undefined && rows[i].payDiscount!=null) {
			        	  listPayDiscountStr.push(rows[i].debitId+'_' +rows[i].payDiscount);			        	  
			          }
			          if (rows[i].payAmount!=undefined && rows[i].payAmount!=null) {
			        	  listPayAmountStr.push(rows[i].debitId+'_' +rows[i].payAmount);			        	  
			          }
			          if (rows[i].remain!=undefined && rows[i].remain!=null) {
			        	  listRemainStr.push(rows[i].debitId+'_' +rows[i].remain);
			          }
			          if (rows[i].total!=undefined && rows[i].total!=null) {
			        	  listTotalStr.push(rows[i].debitId+'_' +rows[i].total);
			          }
			          if (rows[i].nvttCodeName!=undefined && rows[i].nvttCodeName!=null && rows[i].nvttCodeName.trim().length > 0) {
			        	  listNVTTStr.push(rows[i].debitId+'_' +rows[i].nvttCodeName.trim());
			          }
			     }
			}
		}
		
		data.lstDebitId = lstCustomerDebitID;
		data.arrPayDiscountStr = listPayDiscountStr.join(",");
		data.arrRemainStr = listRemainStr.join(",");
		data.arrPayAmountStr = listPayAmountStr.join(",");		
		data.arrTotalStr = listTotalStr.join(",");
		data.arrNVTTStr = listNVTTStr.join(",");
		var url = "/customerdebit/batch/exportExcel";
		ReportUtils.exportReport(url, data);
		return false;
	},

	/**
	 * tai file template import thanh toan theo don hang
	 *
	 * @author lacnv1
	 * @since Mar 24, 2015
	 */
	downloadImportTemplate: function() {
		$('.ErrorMsgStyle').html('').hide();

		var params = {};

		var fromDate = $('#fromDate').val().trim();
		var toDate = $('#toDate').val().trim();
		if(fromDate == '__/__/____'){
			$('#fromDate').val('');
			fromDate = '';
		}
		if(toDate == '__/__/____'){
			$('#toDate').val('');
			toDate = '';
		}
		var msg = Utils.getMessageOfInvalidFormatDate('fromDate', 'Từ ngày');
		if (msg.trim().length == 0) {
			params.fromDate = $('#fromDate').val().trim();
		}
		if(msg.length ==0 && toDate.length > 0){
			msg = Utils.getMessageOfInvalidFormatDate('toDate', 'Đến ngày');
			if (msg.trim().length == 0) {
				params.toDate = $('#toDate').val().trim();
			}
		}
		params.shopCode = $('#shop').combobox('getValue').trim();
		params.shortCode = $('#shortCode').val().trim();
		params.type = $('#type').val().trim();
		params.staffDeliverCode = $('#transferStaff').combobox('getValue').trim();
		params.staffPaymentCode = $('#crashierStaff').combobox('getValue').trim();
		params.staffCode = $('#staffCode').combobox('getValue').trim();
		params.lstOrderNumbers = $("#orderNumber").val().trim();
		try {
			params.reportCode = $('#function_code').val();
		} catch(e) {
			// Oop! Error
		}
		Utils.getJSONDataByAjax(params, "/customerdebit/batch/download-template", function(data) {
			if (!data.error) {
				var filePath = ReportUtils.buildReportFilePath(data.path);
				window.location.href = filePath;
				setTimeout(function() {
					CommonSearch.deleteFileExcelExport(filePath);
				}, 10000);
			}
		}, null, "POST");
	},

	/**
	 * import thanh toan bang excel
	 *
	 * @author lacnv1
	 * @since Mar 24, 2015
	 */
	importExcelNew: function() {
		$('.ErrorMsgStyle').hide();
		VTUtilJS.importExcelUtils(function(data) {
			CustomerDebitBatchNew.getDataInGrid();
			$("#errExcelMsg").html(data.message).show();
		}, "importFrm", "excelFile", null, "errExcelMsg", {shopCode: $('#shop').combobox('getValue').trim()});
	},

	/**
 	 * luu NVTT don hang
 	 *
 	 * @author nhutnn
 	 * @since 16/11/2015
 	 */
	updateSaleOrderDebitDetail: function() {
		$('.ErrorMsgStyle').html('').hide();
		var msg = Utils.getMessageOfRequireCheck('shop', jsp_common_shop_name_lable, true);
		var lstCustomerDebitID = new Array();
		if (msg.length == 0) {
			var htGrid = $('#dg').handsontable('getInstance');
			var rows = htGrid.getData();
			if (rows != undefined && rows != null) {
				for (var i = 0, sz = rows.length; i < sz; i++) {
					if (rows[i].isCheck) {
						lstCustomerDebitID.push(rows[i].debitId);
					}
				}
			}
			if (lstCustomerDebitID.length <= 0) {
				$('#errMsg').html("Chưa chọn đơn hàng để thay đổi NVTT.").show();
				return false;
			}
		}
		if (msg.length == 0 && ($("#staffCodeNVTT").combobox("getValue") == null || $("#staffCodeNVTT").combobox("getValue") == "")) {
			msg = "Bạn chưa chọn NVTT.";
		}

		if (msg.length > 0) {
			$('#errMsg').html(msg).show();
			return false;
		}

		var params = new Object();
		params.lstCustomerDebitID = lstCustomerDebitID;
		params.staffCode = $("#staffCodeNVTT").combobox("getValue").trim();
		params.shopCode = $('#shop').combobox('getValue').trim();

		JSONUtil.saveData2(params, "/customerdebit/batch/update-customer-debit-detail", "Bạn có muốn cập nhật dữ liệu?", "errMsg", function(data) {
			if (!data.error) {
				$("#staffCodeNVTT").combobox("clear");
				$("#successMsg").html("Lưu dữ liệu thành công").show();
				setTimeout(function() {
					$("#successMsg").html("").hide();
				}, 2000);

				CustomerDebitBatchNew.getDataInGrid();
			} else {
				$("#errMsg").html(data.errMsg).show();
			}
		}, null, null);
	},
	
	/**
	 * xu ly khi chon shop
	 * @author trietptm
	 * @return String
	 * @since Dec 4, 2015
	 */
	changeShop: function(shopCode) {
		$('#divOverlay').show();
		$.getJSON('/customerdebit/batch/change-shop?shopCode=' + shopCode, function(data) {
			if  (data != undefined && data != null) {
				$('.ErrorMsgStyle').hide();
				var lstSale = data.lstSale;
				var lstDeliver = data.lstDeliver;
				var lstCashier = data.lstCashier;
				var listBank = data.listBank;
				
				if (lstSale != undefined && lstSale != null && lstSale.length > 0) {
					Utils.bindStaffCbx('#staffCode', lstSale, null, 206);
				}
				if (lstDeliver != undefined && lstDeliver != null && lstDeliver.length > 0) {
					Utils.bindStaffCbx('#transferStaff', lstDeliver, null, 206);
				}
				CustomerDebitBatchNew._mapNVTT = new Map();
				CustomerDebitBatchNew._mapNVTT.put('','');
				if (lstCashier != undefined && lstCashier != null && lstCashier.length > 0) {
					Utils.bindStaffCbx('#crashierStaff', lstCashier, null, 206);
					Utils.bindStaffCbx('#staffCodeNVTT', $.extend(true, [], lstCashier), null, 206);
					for (var i = 0, size = lstCashier.length; i < size; i++) {
//						CustomerDebitBatchNew._lstNVTT.push(Utils.XSSEncode(lstCashier[i].staffName + " - " + lstCashier[i].staffCode));		
						CustomerDebitBatchNew._mapNVTT.put(Utils.XSSEncode(lstCashier[i].staffCode + " - " + lstCashier[i].staffName), lstCashier[i].staffCode)
					}
				}
				CustomerDebitBatchNew._lstBank = new Array();
				CustomerDebitBatchNew._lstBank.push("");
				if (listBank != undefined && listBank != null && listBank.length > 0) {
					for (var i = 0, size = listBank.length; i < size; i++) {
						CustomerDebitBatchNew._lstBank.push(Utils.XSSEncode(listBank[i].bankCode));						  
					}
				}
				if (data.fromDate != undefined && data.fromDate != null) {
					$('#fromDate').val(data.fromDate);
				}
				if (data.toDate != undefined && data.toDate != null) {
					$('#toDate').val(data.toDate);
				}
				if (data.payreceiptCode != undefined && data.payreceiptCode != null) {
					$('#payreceiptCode').val(data.payreceiptCode);
				}
				if (CustomerDebitBatchNew._isLoad) {
					CustomerDebitBatchNew.getDataInGrid();
					CustomerDebitBatchNew._isLoad = false;
				}
				$('#divOverlay').hide();
			}
		});
	}
};

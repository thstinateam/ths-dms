var CustomerDebitBatch = {
	_xhrSave : null,
	_checkF9 : null,
	_lstOrderCheck:null,
	_lstDebitSelectId:null,
	_curRowId: null,
	_lstNVTT: null,
	_rowIndexValueField: null,
	_arrayList: null,
	_mapRowIndex: null,
	_arrayRow: null,
	_remainMoney:0,
	_totalDebit:0,
	getList : function() {
		$('.ErrorMsgStyle').html('').hide();
		var msg = '';
		//msg = Utils.getMessageOfRequireCheck('shortCode','Mã khách hàng');
		//if(msg.length==0){
			msg = Utils.getMessageOfRequireCheck('type','Loại chứng từ', true);
		//}
		var fromDate = $('#fromDate').val().trim();
		var toDate = $('#toDate').val().trim();
		if(fromDate == '__/__/____'){
			$('#fromDate').val('');
			fromDate = '';
		}
		if(toDate == '__/__/____'){
			$('#toDate').val('');
			toDate = '';
		}
		if(msg.length == 0 && fromDate.length > 0){
			msg = Utils.getMessageOfInvalidFormatDate('fromDate', 'Từ ngày');
		}
		if(msg.length ==0 && toDate.length > 0){
			msg = Utils.getMessageOfInvalidFormatDate('toDate', 'Đến ngày');
		}
//		if(msg.length == 0){
//			msg = Utils.getMessageOfRequireCheck('toDate', 'Đến ngày');
//		}
		if(msg.length == 0 && fromDate.length > 0 && toDate.length > 0 &&  !Utils.compareDate(fromDate, toDate)){
			msg = msgErr_fromdate_greater_todate;
			$('#fromDate').focus();
		}
		if(msg.length>0){
			$('#errMsgTop').html(msg).show();
			return false;
		}
		
		CustomerDebitBatch._lstNVTT = $('#crashierStaff').combobox('getData');
		CustomerDebitBatch._arrayList = new Array();
		CustomerDebitBatch._arrayRow = new Array();
		CustomerDebitBatch._arrayList.push(-1);
		CustomerDebitBatch._mapRowIndex = new Map();
		
		var data = new Object();
//		data.orderNumber = $('#orderNumber').val().trim();
		data.shortCode = $('#shortCode').val().trim();
		data.fromDate = $('#fromDate').val().trim();
		data.toDate = $('#toDate').val().trim();
		data.type = $('#type').val().trim();
		data.staffDeliverCode = $('#transferStaff').combobox('getValue').trim();
		data.staffPaymentCode = $('#crashierStaff').combobox('getValue').trim();
		data.staffCode = $('#staffCode').combobox('getValue').trim();

		$('#dg').datagrid('load',data);
		return false;
	},
	viewExcel:function(){		
		var importFileName = $('#excelFile').val();
		if(importFileName.length > 0){
			$('#btnImport').attr('disabled',false);
			$('#btnImport').removeClass('BtnGeneralDStyle');
		}		
		$('#isView').val(1);
		$('#importFrm').submit();
		return false;
	},
	importExcel:function(){		
		$('#btnImport').attr('disabled',true);
		$('#btnImport').addClass('BtnGeneralDStyle');
		msg = 'Bạn có muốn nhập từ file này không?';
		$.messager.confirm('Xác nhận', msg, function(r){
			if (r){
				$('#isView').val(0);
				$('#importFrm').submit();
			}
		});	
		return false;
	},		
	loadInfo : function(){
		CustomerDebitBatch.getInfo(false,false,true);
	},
	getInfo : function(checkF9,isSave, isSaveExcel) {
		$('.ErrorMsgStyle').html('').hide();
		if($('#shortCode').val().trim().length == 0){			
			return false;
		}	
		$('#payreceiptValue').val('');
		$('#debitPreRemain').val($('#debitPostRemain').val());
		var params = new Object();
		//params.staffCode = $('#staffCode').val().trim();
		params.shortCode = $('#shortCode').val().trim();
		params.type = $('#type').val();
		if($('#type').val() == -1) {
			return false;
		}
		$('#dg').datagrid('load',params);
		if(checkF9) {
			CustomerDebitBatch._checkF9 = false;
		}		
		return false;
	},
	import : function() {
		$('.ErrorMsgStyle').html('').hide();
		var msg = '';
		/*msg = Utils.getMessageOfRequireCheck('staffCode','Mã nhân viên bán hàng');
		if(msg.length ==0){
			msg = Utils.getMessageOfSpecialCharactersValidate('staffCode','Mã nhân viên bán hàng',Utils._CODE);
		}*/
		//msg = Utils.getMessageOfRequireCheck('shortCode','Mã khách hàng' );
		/*if(msg.length ==0){
			msg = Utils.getMessageOfRequireCheck('shortCode','Mã khách hàng' );
		}*/
		//if(msg.length ==0){
			msg = Utils.getMessageOfSpecialCharactersValidate('shortCode','Mã khách hàng',Utils._CODE );
		//}
		if(msg.length==0&& $('#type').val() == -1 ){
			msg = 'Bạn chưa chọn loại chứng từ. Yêu cầu chọn loại chứng từ';
		}
		if(msg.length ==0){
			msg = Utils.getMessageOfRequireCheck('payreceiptCode','Số chứng từ' );
		}
		if(msg.length ==0){
			msg = Utils.getMessageOfSpecialCharactersValidate('payreceiptCode','Số chứng từ',Utils._CODE );
		}
		if(msg.length ==0){
			msg = Utils.getMessageOfRequireCheck('payreceiptValue','Số tiền', undefined, true);
		}
		var payreceiptValue = Utils.returnMoneyValue($('#payreceiptValue').val().trim());
		if(payreceiptValue.length > 20){
			msg = 'Giá trị Số tiền nhập vào chỉ giới hạn 20 ký tự';
		}
		var type = $('#type').val().trim();
		if(msg.length ==0 &&(isNaN(payreceiptValue) || payreceiptValue <=0)){
			if(type==1){
				msg ='Số tiền phiếu thu phải là số nguyên dương';
			} else{
				msg ='Số tiền phiếu chi phải là số nguyên dương';
			}
		}
		
		if(msg.length > 0){
			$('#errMsg').html(msg).show();
			return false;
		}		
		var lstCustomerDebitID = new Array();
		//var rows =  $('#dg').datagrid('getSelections');
		var lstPayAmount = [];
		var lstDiscount = [];
		var lstStaffCode = [];
		var rowCheck = $('[name=debitId]');//$('#dg').datagrid('getChecked');
		var rows = $('#dg').datagrid('getRows');
		if (rows != undefined && rows != null) {
			var dis;
			for(var i=0, sz = rows.length; i < sz; i++){
				if(rowCheck[i].checked){
					lstCustomerDebitID.push(rows[i].debitId);
					lstPayAmount.push(rows[i].payAmount);
					dis = $('#discount-' + rows[i].debitId).val().replace(/,/g, '');
					lstDiscount.push(Number(dis));
					var cbx = $('#dg').datagrid('getEditors', i)[0];
					lstStaffCode.push($(cbx.target).combobox('getValue').trim());
				}
			}
		}
		if(lstCustomerDebitID.length <=0 ){
			$('#errMsg').html("Chưa chọn đơn hàng để thanh toán").show();
			return false;
		}
		var cAmount = Number(Utils.returnMoneyValue($('#cAmount').html().trim()));
		var cPay = Number(payreceiptValue);
//		if(($('#type').val() == 1 && CustomerDebitBatch._remainMoney > 0) || ($('#type').val() == 0 && CustomerDebitBatch._remainMoney < 0)) {
//			$('#errMsg').html('Số tiền phải được thanh toán hết').show();
//			return false;
//		}
		if(cPay - cAmount != 0) {
			//$('#errMsg').html('Số tiền phải được thanh toán hết').show();
			$('#errMsg').html('Giá trị phiếu ' + (type==1 ? 'thu' : 'chi') + ' phải bằng tổng tiền thanh toán.').show();
			return false;
		}
		
		var params = new Object();
		//params.staffCode = $('#staffCode').val().trim();
		params.shortCode = $('#shortCode').val().trim();
		params.type = $('#type').val();
		params.payreceiptCode = $('#payreceiptCode').val().trim();
		params.payreceiptValue = payreceiptValue;
		params.lstCustomerDebitID = lstCustomerDebitID;
		params.lstPayAmount = lstPayAmount;
		params.lstDiscount = lstDiscount;
		params.lstStaffCode = lstStaffCode;
		msg ='Bạn có muốn thanh toán theo các đơn hàng này ?';
		Utils.addOrSaveData(params, '/customerdebit/batch/import', null,  'errMsg', function(data) {			
			//CustomerDebitBatch.getInfo(false,true);
			CustomerDebitBatch.getList();
		}, 'loadingImport',null, null, msg,function(dataErr) {
			if(dataErr.errorReloadPage == true){
				window.location.href = '/customerdebit/batch/info';
			}
		});
		return false;
	},
	changeCheckBox: function(input) {
		var count = 0;
		$("input[name=chkCUSTOMERDEBIT]").each(function(){
	   		++count;
	   	});
		if (count == $(".cb_element:checked").length){
			$('#chkCUSTOMERDEBIT_ALL').attr('checked', 'checked');
		}
		else{
			$('#chkCUSTOMERDEBIT_ALL').attr('checked',false);
		}
		
		var checkbox = $('#' +input);
		
		var payreceiptValueTemp = $('#payreceiptValue').val();
		var payreceiptValue = Number(Utils.returnMoneyValue(payreceiptValueTemp));
		if($('#type').val()== 0){
			payreceiptValue = -payreceiptValue;
		}
		var checked_status = checkbox.is(':checked');
		if((payreceiptValueTemp == "" || payreceiptValueTemp == '') && checked_status){
			var msg = "Hãy nhập số tiền cho số chứng từ";
			Alert('Thông báo',msg);
			checkbox.attr('checked',false);
			return false;
		}
		var sum = 0;
		$(".cb_element:checked").each(function(){
	   		sum = sum + Number($(this).val());
	   	});
		var test = Math.abs(payreceiptValue) -Math.abs(sum);
		var debitPreRemain = Number(Utils.returnMoneyValue($('#debitPreRemain').val()));
		if(test<0) {
			var msg = "Số tiền còn lại của phiếu thu không thể trả đủ cho đơn hàng";
			$('#debitPostRemain').val(formatCurrencyInterger(debitPreRemain -payreceiptValue));
			Alert('Thông báo',msg);
		} else{
			$('#debitPostRemain').val(formatCurrencyInterger(debitPreRemain-sum));
		}
		return false;
	},
	changeCheckBoxALL: function() {
		
	},
	changePayReceiptValue: function() {
		var payreceiptValueTemp = $('#payreceiptValue').val();
		if(payreceiptValueTemp.length != 0) {
			var payreceiptValue = Number(Utils.returnMoneyValue(payreceiptValueTemp));
			var type = $('#type').val();
			var msg ='';
			if(isNaN(payreceiptValue) || payreceiptValue <=0){
				if(type==1){
					msg ='Số tiền phiếu thu phải là số nguyên dương';
				} else{
					msg ='Số tiền phiếu chi phải là số nguyên dương';
				}
				Alert('Thông báo',msg, function() {
					$('#payreceiptValue').val('');
					$('#payreceiptValue').focus();
				});
			} else{
				//var test;
				//var sum = 0;
				//$(".cb_element:checked").each(function(){
			   	//	sum = sum + Number($(this).val());
			   	//});
				var heSo = 1;
				if(type== 0){
					heSo = -1;
					//payreceiptValue = -payreceiptValue;
				}
				//var debitPreRemain = Number(Utils.returnMoneyValue($('#debitPreRemain').val()));
				//test = Math.abs(payreceiptValue) - Math.abs(sum);
				//if(test<0) {
				//	if(type==1){
				//		msg ='Số tiền còn lại của phiếu thu không thể trả đủ cho đơn hàng';
				//	} else{
				//		msg ='Số tiền còn lại của phiếu chi không thể trả đủ cho đơn hàng';
				//	}
				//	$('#debitPostRemain').val(formatCurrencyInterger(debitPreRemain-payreceiptValue));
				//	Alert('Thông báo',msg);
				//}else{
				//	$('#debitPostRemain').val(formatCurrencyInterger(debitPreRemain-payreceiptValue));
//					$('#debitPostRemain').val($('#debitPreRemain').val());
				//}
				CustomerDebitBatch._remainMoney = payreceiptValue;
				var cAmount = Number(Utils.returnMoneyValue($('#cAmount').html().trim()));
				$('#debitPostRemain').val(formatCurrency(CustomerDebitBatch._totalDebit - Math.min(payreceiptValue, cAmount) * heSo));
				//$('#dg').datagrid('uncheckAll');
			}
			return false;
		}
	},
	changeCustomer: function() {
		if($('#shortCode').val().trim().length != 0){
			var url = 'shopCode=' + encodeChar($('#shopCode').val()) + '&shortCode=' + encodeChar($('#shortCode').val());
			$.getJSON('/customerdebit/auto/currentDebit?' +url, function(data){
				if(data.error) {
					$('#errMsg').html(data.errMsg).show();
				} else {
					$('#debitPreRemain').val(formatCurrencyInterger(data.currentDebit) );
					CustomerDebitBatch._totalDebit = data.currentDebit;
					$('#debitPostRemain').val('');
				}
			});
		}
		return false;
	},
	showImportExcelOnDialog:function(){		
		$('#importExcelContent').css("visibility", "visible");
		var html = $('#importExcelContent').html();
		$('#errExcelMsg').html('').hide();
		$('#importExcelContenDialog').dialog({  	       
	        closed: false,  
	        cache: false,  
	        modal: true,
	        width : 850,
	        height :450,
	        onOpen: function(){
	        	$('.easyui-dialog #dgDetail').datagrid({					
					//autoRowHeight : true,
					rownumbers : true, 
					singleSelect: true,					
					//rowNum : 10,
					scrollbarSize : 0,
					fitColumns:true,
					//cache: false, 					
					width : $('#importExcelContenDialog').width()-20,
					//height:auto,
					//autoWidth: true,
				    columns:[[
						{field: 'shortCode',title: 'Mã KH',  width: 100,sortable:false,resizable:false, align: 'left' , formatter:function(value, options, rowObject) {
							return Utils.XSSEncode(value);
						}},
						{field: 'type',title: 'Loại CT',  width: 100,sortable:false,resizable:false, align: 'left' , formatter:function(value, options, rowObject) {
							return Utils.XSSEncode(value);
						}},
						{field: 'payreceiptCode',title: 'Số CT',  width: 100,sortable:false,resizable:false, align: 'left' , formatter:function(value, options, rowObject) {
							return Utils.XSSEncode(value);
						}},
						{field: 'money',title: 'Số tiền',  width: 100,sortable:false,resizable:false, align: 'right', formatter:function(cellValue, options, rowObject) {
							if(cellValue!=undefined && cellValue!=null){
								return formatCurrency(Math.abs(cellValue));
							}
							return '';
						} },
						{field: 'discount',title: 'Chiết khấu',  width: 100,sortable:false,resizable:false, align: 'right', formatter:function(cellValue, options, rowObject) {
							if(cellValue!=undefined && cellValue!=null){
								return formatCurrency(Math.abs(cellValue));
							}
							return '';
						}}
				    ]],
				    onLoadSuccess :function(data){   	 
				    	
				    }
	        	});
	        	$('.datagrid-header-rownumber').html('STT');
	         	$('.datagrid-header-row td div').css('text-align','center');	
	        },
	        onClose:function(){
	        	$('#importExcelContent').html(html);
	        	$('#importExcelContent').css("visibility", "hidden");
	        	$('.easyui-dialog #dgDetail').datagrid('loadData', []);
	        	$('#excelFile').val('');
	        	$('#fakefilepc').val('');
	        }
		});
	},
	beforeImportExcel: function(){
		if(!previewImportExcelFile(document.getElementById("excelFile"))){
			return false;
		}		
		$('#errExcelMsg').html('').hide();		
		showLoadingIcon();
		return true;
	},
	afterImportExcelUpdate: function(responseText, statusText, xhr, $form){
		if($('#btnImport').length != 0) {
			enable('btnImport'); 
		}
		hideLoadingIcon();
		if (statusText == 'success') {				
	    	$("#responseDiv").html(responseText);		 
	    	var newToken = $('#responseDiv #newToken').val();
	    	console.log(newToken);
	    	if(newToken != null && newToken != undefined && newToken != '') {
	    		$('#token').val(newToken);	    		
	    		$('#tokenImport').val(newToken);
	    	}
	    	if($('#errorExcel').html().trim() == 'true' || $('#errorExcelMsg').html().trim().length > 0){
	    		$('#errExcelMsg').html($('#errorExcelMsg').html()).show();
	    	} else {
	    		if($('#typeView').html().trim() == 'false'){
	    			var totalRow = parseInt($('#totalRow').html().trim());
	    			var numFail = parseInt($('#numFail').html().trim());
	    			var fileNameFail = $('#fileNameFail').html();
	    			var mes = format(msgErr_result_import_excel,(totalRow - numFail),numFail);		    		
	    			if(numFail > 0){
	    				mes+= ' <a href="' + fileNameFail +'">Xem chi tiết lỗi</a>';
	    			}
	    			if($('#excelFile').length!=0 && $('#fakefilepc').length!=0){
	    				try{$('#excelFile').val('');$('#fakefilepc').val('');}catch(err){}
	    			}
	    			$('#errExcelMsg').html(mes).show();	    				    			
	    		}else{	    
	    			$('.easyui-dialog #dgDetail').datagrid("loadData", {total:0, rows:[]});
	    			$('.excel-item').each(function(){
	    				var row = new Object();
	    				//row.staffCode = $(this).attr('staffCode');
	    				row.shortCode = $(this).attr('shortCode');
	    				row.type = $(this).attr('typect') == '0' ? 'phiếu thu' : $(this).attr('typect') == '1' ? 'phiếu chi' : $(this).attr('typect');
	    				row.payreceiptCode = $(this).attr('payreceiptCode');
	    				row.money = $(this).attr('money');
	    				row.discount = $(this).attr('discount');
	    				row.errMsg = $(this).attr('errMsg');
	    				$('.easyui-dialog #dgDetail').datagrid('appendRow',row);
	    			});
	    		}
	    	}
	    }	
	},
	exportExcel: function() {
		$('#divOverlay').addClass('Overlay');
		$('#imgOverlay').show();
		$('#errMsg').html('').hide();
		$('#errExcelMsg').html('').hide();
		$('.ErrorMsgStyle').html('').hide();
		var msg = '';
		msg = Utils.getMessageOfRequireCheck('type','Loại chứng từ', true);
		var fromDate = $('#fromDate').val().trim();
		var toDate = $('#toDate').val().trim();
		if(fromDate == '__/__/____'){
			$('#fromDate').val('');
			fromDate = '';
		}
		if(toDate == '__/__/____'){
			$('#toDate').val('');
			toDate = '';
		}
		if(msg.length == 0 && fromDate.length > 0){
			msg = Utils.getMessageOfInvalidFormatDate('fromDate', 'Từ ngày');
		}
		if(msg.length ==0 && toDate.length > 0){
			msg = Utils.getMessageOfInvalidFormatDate('toDate', 'Đến ngày');
		}
		if(msg.length == 0 && fromDate.length > 0 && toDate.length > 0 &&  !Utils.compareDate(fromDate, toDate)){
			msg = msgErr_fromdate_greater_todate;
			$('#fromDate').focus();
		}
		if(msg.length>0){
			$('#errMsgTop').html(msg).show();
			return false;
		}
		var data = new Object();
		data.shortCode = $('#shortCode').val().trim();
		data.fromDate = $('#fromDate').val().trim();
		data.toDate = $('#toDate').val().trim();
		data.type = $('#type').val().trim();
		data.staffDeliverCode = $('#transferStaff').combobox('getValue').trim();
		data.staffPaymentCode = $('#crashierStaff').combobox('getValue').trim();
		data.staffCode = $('#staffCode').combobox('getValue').trim();
//		var lstCustomerDebitID = new Array();
//		var rowCheck = $('[name=debitId]');
//		var rows = $('#dg').datagrid('getRows');
//		if (rows != undefined && rows != null) {
//			for(var i=0, sz = rows.length; i < sz; i++){
//				if(rowCheck[i].checked){
//					lstCustomerDebitID.push(rows[i].debitId);
//				}
//			}
//		}
//		data.lstDebitId = lstCustomerDebitID;
		var url = "/customerdebit/batch/exportExcel";
		CommonSearch.exportExcelData(data,url);
		return false;
	}
};
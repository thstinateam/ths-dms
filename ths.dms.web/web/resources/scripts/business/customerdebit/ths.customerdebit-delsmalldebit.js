var DelSmallDebit = {
	_isLoad: true,
	mapChoose:null,
	getList : function() {
		$('.ErrorMsgStyle').html('').hide();
		var msg = Utils.getMessageOfRequireCheck('shop', jsp_common_shop_name_lable, true);
		var fromDate = $('#fromDate').val().trim();
		var toDate = $('#toDate').val().trim();
		if (fromDate == '__/__/____') {
			$('#fromDate').val('');
			fromDate = '';
		}
		if (toDate == '__/__/____') {
			$('#toDate').val('');
			toDate = '';
		}
		msg = Utils.getMessageOfRequireCheck('fromMoney','Nợ từ (VNĐ)', undefined, true);
		if (msg.length == 0) {
			msg = Utils.getMessageOfRequireCheck('toMoney','Đến (VNĐ)', undefined, true);
		}
		if (msg.length == 0) {
			msg = Utils.getMessageOfInvaildNumber('fromMoney', 'Nợ từ (VNĐ)');
		}
		if (msg.length == 0) {
			msg = Utils.getMessageOfInvaildNumber('toMoney', 'Đến (VNĐ)');
		}
		var fromValue = Utils.returnMoneyValue($('#fromMoney').val().trim());
		var toValue = Utils.returnMoneyValue($('#toMoney').val().trim());
		if (fromValue.length > 20 || toValue.length > 20) {
			msg = 'Giá trị Số tiền nhập vào chỉ giới hạn 20 ký tự';
		}
		if (msg.length == 0) {
			objectName = 'Nợ từ (VNĐ)';
			if ($('#fromMoney').val().trim() != '' && $('#fromMoney').val().trim().length > 0) {
				var msg = Utils.getMessageOfSpecialCharactersValidateEx('fromMoney', objectName, Utils._TF_NUMBER_COMMA_AND_DOT);
				if(msg.length == 0 && $('#fromMoney').val().replace(/,/g,'') < 0) {
					$('#fromMoney').focus();
					msg = objectName + ' phải lớn hơn hoặc bằng 0';
				}
			}
		}
		if (msg.length == 0) {
			msg = Utils.getMessageOfNegativeNumberCheck('toMoney', 'Đến (VNĐ)');
		}
		
		if (msg.length == 0 && parseInt(fromValue) > parseInt(toValue)) {
			msg = 'Nợ từ phải nhỏ hơn Nợ đến.';
			$('#fromMoney').focus();
		}
		if (msg.length == 0 && parseInt(toValue) > 9999999999999) {		
			msg = 'Nợ đến có giá trị quá lớn';
			$('#toMoney').focus();
		}
		
		if (msg.length == 0 && fromDate.length > 0) {
			msg = Utils.getMessageOfInvalidFormatDate('fromDate', 'Từ ngày');
		}
		if (msg.length ==0 && toDate.length > 0) {
			msg = Utils.getMessageOfInvalidFormatDate('toDate', 'Đến ngày');
		}
		if (msg.length == 0 && fromDate.length > 0 && toDate.length > 0 && !Utils.compareDate(fromDate, toDate)) {
			msg = msgErr_fromdate_greater_todate;
			$('#fromDate').focus();
		}
		if (msg.length > 0) {
			$('#errMsg').html(msg).show();
			return false;
		}
		var data = new Object();
		data.shopCode = $('#shop').combobox('getValue');
		data.orderNumber = $('#orderNumber').val().trim();
		data.shortCode = $('#shortCode').val().trim();
		data.fromMoney = fromValue;
		data.toMoney = toValue;
		data.fromDate = fromDate;
		data.toDate = toDate;
		data.staffDeliverCode = $('#transferStaff').combobox('getValue').trim();
		data.staffPaymentCode = $('#crashierStaff').combobox('getValue').trim();
		data.staffCode = $('#staffCode').combobox('getValue').trim();

		$('#dg').datagrid('load',data);
		return false;
	},
	removeDebit: function() {
		$('.ErrorMsgStyle').html('').hide();
		var data = new Object();
		data.shopCode = $('#shop').combobox('getValue');
		data.shortCode = $('#shortCode').val().trim();
		var lstDebitId = new Array();
		var lstDebitAmt = new Array();
		var lstDebitId1 = new Array();
		var lstDebitAmt1 = new Array();
				
		for(var i = 0; i < DelSmallDebit.mapChoose.keyArray.length; ++i){
			var remain = DelSmallDebit.mapChoose.valArray[i].remain;
			if (parseInt(remain) > 0) {
				lstDebitId.push(DelSmallDebit.mapChoose.keyArray[i]);
				lstDebitAmt.push(DelSmallDebit.mapChoose.valArray[i].remain);
			} else {
				lstDebitId1.push(DelSmallDebit.mapChoose.keyArray[i]);
				lstDebitAmt1.push(DelSmallDebit.mapChoose.valArray[i].remain);
			}
		}		
		data.lstDebitId = lstDebitId;
		data.lstDebitAmt = lstDebitAmt;
		data.lstDebitId1 = lstDebitId1;
		data.lstDebitAmt1 = lstDebitAmt1;
		if (data.lstDebitId.length == 0 && data.lstDebitId1.length == 0) {
			$('#errMsg2').html('Bạn chưa chọn đơn hàng nào.').show();
			return;
		}
		Utils.addOrSaveData(data, "/customerdebit/delsmalldebit/removedebit",null, 'errMsg2', function(data) {
			if (data.error) {
				  $('#errMsg2').html(data.errMsg).show();
			} else {
				DelSmallDebit.getList();
			}
		});
		return false;
	},
	
	/**
	 * xu ly khi chon shop
	 * @author trietptm
	 * @since Dec 23, 2015
	 */
	changeShop: function() {
		$('#divOverlay').show();
		var param = new Object() ;
		param.shopCode = $('#shop').combobox('getValue');
		$.ajax({
			type : "POST",
			url: '/customerdebit/delsmalldebit/change-shop',
			data :($.param(param, true)),
			dataType : "json",
			success : function(data) {
				if  (data != undefined && data != null) {
					$('.ErrorMsgStyle').hide();
					var lstSale = data.lstSale;
					var lstDeliver = data.lstDeliver;
					var lstCashier = data.lstCashier;
					
					if (lstSale != undefined && lstSale != null && lstSale.length > 0) {
						Utils.bindStaffCbx('#staffCode', lstSale, null, 206);
					}
					if (lstDeliver != undefined && lstDeliver != null && lstDeliver.length > 0) {
						Utils.bindStaffCbx('#transferStaff', lstDeliver, null, 206);
					}
					if (lstCashier != undefined && lstCashier != null && lstCashier.length > 0) {
						Utils.bindStaffCbx('#crashierStaff', lstCashier, null, 206);
					}
					if (DelSmallDebit._isLoad) {
						DelSmallDebit.initGrid();
						DelSmallDebit._isLoad = false;
					}
					$('#divOverlay').hide();
				}
			}
		});
	},
	
	/**
	 * khoi tao grid
	 * @author trietptm
	 * @since Dec 19, 2015
	 */
	initGrid: function() {
		$('#dg').datagrid({  
 	 		url:'/customerdebit/delsmalldebit/getinfo',		    	      
//  	 	    width: $(window).width() -55,
 	 	  	width: ($('#dg').width() - 20),
 	 	    height: 350,
 	 	    rownumbers: true,
 	 	  	//scrollbarSize: 0,
 	 	    fitColumns: true,
 	 	    queryParams: {},
 	 	    columns: [[
 	 			{field: 'shortCode', title: 'Mã KH', resizable: false, width: 60, align: 'left', formatter: function(value, row, index) {
 			    	return Utils.XSSEncode(value);
 				}},
 	 			{field: 'customerName', title: 'Tên KH', resizable: false, width:120, align: 'left', formatter: function(value, row, index) {
 			    	return Utils.XSSEncode(value);
 				}},
 	 			{field: 'orderNumber', title: 'Số đơn hàng/<br/>Số phiếu điều chỉnh công nợ', resizable: false, width: 100, align: 'left', formatter: function(value, row, index) {
 			    	return Utils.XSSEncode(value);
 				}},
 	 			{field: 'orderDate', title: 'Ngày', width: 60, resizable: false, align: 'center', formatter: CommonFormatter.dateTimeFormatter},
 	 			{field: 'nvbhCodeName', title:'NVBH', resizable: false, width: 130, align: 'left', formatter: function(value, row, index) {
 			    	return Utils.XSSEncode(value);
 				}},
 	 			{field: 'nvghCodeName', title: 'NVGH', resizable: false, width: 130, align: 'left', formatter: function(value, row, index) {
 			    	return Utils.XSSEncode(value);
 				}},
 	 			{field: 'nvttCodeName', title: 'NVTT', resizable: false, width: 130, align: 'left', formatter: function(value, row, index) {
 			    	return Utils.XSSEncode(value);
 				}},
 	 			{field: 'remain', title: 'Số nợ', width: 100, resizable: false, align: 'right', formatter: CommonFormatter.numberFormatter},
 	 			{field: 'debitId', checkbox: true}
 	 		]],
 	 		onCheck: function(rowIndex, rowData) {
 	 			DelSmallDebit.mapChoose.put(rowData.debitId, rowData);
 	 		},
 	 		onUncheck: function(rowIndex, rowData) {
 	 			DelSmallDebit.mapChoose.remove(rowData.debitId);
 	 		},
 	 		onCheckAll: function(rows) {
 	 			for(var i = 0; i < rows.length; i++) {
 	 				var rowData = rows[i];
 	 				DelSmallDebit.mapChoose.put(rowData.debitId, rowData);
 	 			}
 	 		},
 	 		onUncheckAll: function(rows) {
 	 			for(var i = 0; i < rows.length; i++) {
 	 				var rowData = rows[i];
 	 				DelSmallDebit.mapChoose.remove(rowData.debitId);
 	 			}
 	 		},
 	 		onLoadSuccess :function(data) {
 	 			if (data.rows.length > 0) {
 	 				$('#btnRemoveDebit').show();
 	 			}
 	 			DelSmallDebit.mapChoose = new Map();
 	 			$('#dg').datagrid('unselectAll');
 	 			$('.datagrid-header-rownumber').html('STT');
 	 			$('#dg').datagrid('resize');
		      	$('.datagrid-htable .datagrid-cell').addClass('GridHeaderLargeHeight');
 	 		   	$('.datagrid-header-row td div').css('text-align','center');		   	
 	 		}
 	 	});
	}
};
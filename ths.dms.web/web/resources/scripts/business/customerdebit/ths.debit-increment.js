var DebitIncrement = {
		_xhrSave: null,
		
		saveDebit: function() {
			$('.ErrorMsgStyle').hide();
			var msg = "";
			
			msg = Utils.getMessageOfRequireCheck('debitType', "Loại phiếu", true);
			
			if (msg.length == 0) {
				msg = Utils.getMessageOfRequireCheck('type', "Đối tượng", true);
			}
			var type = $('#type').val().trim();
			
			if (msg.length == 0) {
				msg = Utils.getMessageOfRequireCheck('dbNumber', "Số phiếu");
			}
			
			if (msg.length == 0) {
				msg = Utils.getMessageOfSpecialCharactersValidate('dbNumber', "Số phiếu", Utils._CODE);
			}
			
			if (msg.length == 0) {
				msg = Utils.getMessageOfRequireCheck('amount', "Số tiền");
			}
			var amount = $('#amount').val().trim();
			amount = amount.replace(/,/g, '');
			if (msg.length == 0 &&
					(amount.length == 0 || isNaN(amount))) {
				msg = "Số tiền phải là số.";
				$('#amount').focus();
			}
			if (msg.length == 0 && amount <= 0) {
				msg = "Số tiền phải lớn hơn 0.";
				$('#amount').focus();
			}
			
			if (msg.length == 0) {
				msg = Utils.getMessageOfRequireCheck('reason', "Lý do");
			}
			
			if (msg.length == 0 && type == '1') {
				msg = Utils.getMessageOfRequireCheck('shortCode', "Khách hàng");
			}
			
			if (msg.length > 0) {
				$('#errMsg').html(msg).show();
				return false;
			}
			
			var dataModel = new Object();
			dataModel.dbNumber = $('#dbNumber').val().trim();
			dataModel.type = type;
			dataModel.debitType = $('#debitType').val().trim();
			dataModel.amount = amount;
			dataModel.shortCode = $('#shortCode').val().trim();
			dataModel.reason = $('#reason').val().trim();
			$.messager.confirm("Xác nhận", "Bạn có muốn lưu thông tin này", function(r) {
				if (r) {
					Utils.saveData(dataModel, "/debit/increment/save-debit", DebitIncrement._xhrSave, 'errMsg',
							function() {
						setTimeout(function() {
							window.location.reload();
						}, 3000);
					}, "loading2");
				}
			});
			return false;
		},
		
		typeChange: function() {
			var type = $('#type').val().trim();
			if (type == '1') {
				$('#shortCode').removeAttr("disabled");
				$('#shortCode').focus();
			} else {
				$('#shortCode').attr("disabled", "disabled");
				$('#shortCode').val('');
			}
		}
};

function DebitAdjustmentObj() {
		this.debitType = null;
		//this.type = null;
		this.dbNumber = null;
		this.amount = null;
		this.reason = null;
		this.shortCode = null;
}
function ShopDebitAdjustmentObj() {
	this.debitType = null;
	this.dbNumber = null;
	this.amount = null;
	this.reason = null;
}
var DebitAdjustment = {
		_xhrSave: null,
		_lstTypes: ["", "KH - NPP", "NPP - Công ty"],
		_lstDebitTypes: ["", "Phiếu tăng nợ", "Phiếu giảm nợ"],
		_colNames: ["Loại phiếu", "Số phiếu", "Số tiền", "Lý do", "Khách hàng (F9)"],
		
		initGrid: function() {
			$('#gridContainer').html('<table id="dgrid"></table>');
			
			var w = $("#gridContainer").width() - 60;
			var lst1 = [];
			for (var i = 0; i < 30; i++) {
				lst1.push(new DebitAdjustmentObj());
			}
			$('#dgrid').handsontable({
				data: lst1,
				colWidths: [w/6, w/6, w/6, w/3, w/6-27],
				outsideClickDeselects: true,
				multiSelect : false,
				rowHeaders: true,
				manualColumnResize: true,
				fixedRowsTop: 0,
				width: w+70,
				colHeaders: DebitAdjustment._colNames,
				columns: [
					{data: "debitType", allowInvalid: false, type: "dropdown", source: DebitAdjustment._lstDebitTypes},
				    //{data: "type", allowInvalid: false, type: "dropdown", source: DebitAdjustment._lstTypes},
				    {data: "dbNumber"},
				    {data: "amount", type: "numeric", format: "#,##0"/*, allowInvalid: false */},
				    {data: "reason"},
				    {data: "shortCode"}
				],
				beforeKeyDown: function(event){
					$(".ErrorMsgStyle").hide();
					var sl = $('#dgrid').handsontable('getSelected');
					var r = sl[0];
					var c = sl[1];
					if (c ==  1 || c == 4) {
						$(".handsontableInputHolder:not(:hidden) .handsontableInput").attr("maxlength", 40);
					} else if (c == 2) {
						$(".handsontableInputHolder:not(:hidden) .handsontableInput").attr("maxlength", 15);
					} else if (c == 3) {
						$(".handsontableInputHolder:not(:hidden) .handsontableInput").attr("maxlength", 250);
					} else {
						$(".handsontableInputHolder:not(:hidden) .handsontableInput").attr("maxlength", 15);
					}
					var keyCode = event.keyCode;
					if (keyCode == keyCodes.TAB && !event.shiftKey) {
						var htGrid = $('#dgrid').handsontable('getInstance');
						var sl = htGrid.getSelected();
						var r = sl[0];
						var c = sl[1];
						var data = htGrid.getData();
						if (c == 4) {
							event.stopImmediatePropagation();
							event.preventDefault();
							if (r == data.length - 1) {
								for (var i = 0; i < 30; i++) {
									data.push(new DebitAdjustmentObj());
								}
								htGrid.render();
							}
							htGrid.selectCell(r + 1, 0);
						}
					}
					else if (keyCode == keyCode_F9) {
						var htGrid = $('#dgrid').handsontable('getInstance');
						var sl = htGrid.getSelected();
						var r = sl[0];
						var c = sl[1];
						if (c == 4) {
							VCommonJS.showDialogSearch2({
							    inputs : [
							        {id:'code', maxlength:50, label:'Mã KH'},
							        {id:'name', maxlength:250, label:'Tên KH'},
							        {id:'address', maxlength:250, label:'Địa chỉ', width:410}
							    ],
							    url : '/commons/customer-in-shop/search?shopCode='+$('#shopCode').val().trim(),
							    onShow : function() {
						        	$('.easyui-dialog #code').focus();
							    },
							    columns : [[
							        {field:'shortCode', title:'Mã KH', align:'left', width: 110, sortable:false, resizable:false, formatter: function(v, r, i){
							        	if (v) {
							        		return Utils.XSSEncode(v);
							        	}
							        	return "";
							        }},
							        {field:'customerName', title:'Tên KH', align:'left', width: 150, sortable:false, resizable:false, formatter: function(v, r, i){
							        	if (v) {
							        		return Utils.XSSEncode(v);
							        	}
							        	return "";
							        }},
							        {field:'address', title:'Địa chỉ', align:'left', width: 170, sortable:false, resizable:false, formatter: function(v, r, i){
							        	if (v) {
							        		return Utils.XSSEncode(v);
							        	}
							        	return "";
							        }},
							        {field:'choose', title:'', align:'center', width:40, sortable:false, resizable:false, formatter:function(value,row,index) {
							        	return "<a href='javascript:void(0)' onclick='chooseCustomer(\""+ Utils.XSSEncode(row.shortCode) + "\","+r+","+c+");'>Chọn</a>";        
							        }}
							    ]]
							});
						}
					}
				},
				afterChange: function(d, s) {
					if (d != null) {
						var sl = d[0];
						if (sl[1] == "amount") {
							var r = sl[0];
							var v = sl[3];
							var htGrid = $('#dgrid').handsontable('getInstance');
							//var meta = htGrid.getCellMeta(r, 4);
							//meta.readOnly = false;
							if (v && (isNaN(v) || Number(v) <= 0 || Math.floor(Number(v)) - Number(v) != 0)) {
								if (Math.floor(Number(v)) - Number(v) != 0) {
									htGrid.setDataAtCell(r,2,null);
								}
								htGrid.selectCell(r, 2);
								$("#errMsg").html("Số tiền phải là số nguyên dương").show();
								htGrid.render();
							} else if (v && (v.toString().length > 15 || Number(v) > 999999999999999)) {
								//htGrid.setDataAtCell(r,2,null);
								htGrid.selectCell(r, 2);
								$("#errMsg").html("Số tiền chỉ được nhập tối đa 15 chữ số").show();
								htGrid.render();
							}
						}
					}
				}
			});
			$('#dgrid').handsontable('selectCell', 0, 0);
			
			$("#dgrid thead th").css({"background-color":"#56ABC7", "color":"#fff", "font-weight":"bold"});
		},
		saveDebit: function() {
			$(".ErrorMsgStyle").hide();
			
			var htGrid = $("#dgrid").handsontable("getInstance");
			var rows = htGrid.getData();
			if (rows == null || rows.length == 0) {
				$("#errMsg").html("Không có dòng dữ liệu nào để lưu").show();
				return;
			}
			
			var lstDebit = [];
			var dbObj = null;
			var row = null;
			var j = 0;
			var t = 0;
			for (var i = 0, sz = rows.length; i < sz; i++) {
				row = rows[i];
				
				if (DebitAdjustment.isNullOrEmptyRow(row)) {
					continue;
				}
				
				j = 0;
				for (var p in row) {
					if (DebitAdjustment.isNullOrEmptyCell(row[p], p == "amount")) {
						$("#errMsg").html("Bạn chưa nhập giá trị cho trường " + DebitAdjustment._colNames[j].replace(" (F9)", "")).show();
						htGrid.selectCell(i, j);
						return;
					}
					j++;
				}
				
				if (DebitAdjustment._lstDebitTypes.indexOf(row["debitType"]) < 0) {
					$("#errMsg").html("Loại phiếu không hợp lệ").show();
					htGrid.selectCell(i, 0);
					return;
				}
				
				if(!/^[0-9a-zA-Z-_.]+$/.test(row["dbNumber"])){
					$("#errMsg").html(format(msgErr_invalid_format_code, "Số phiếu")).show();
					htGrid.selectCell(i, 1);
					return;
				}
				
				if(row["dbNumber"].trim().length > 40){
					$("#errMsg").html("Số phiếu chỉ được tối đa 40 ký tự").show();
					htGrid.selectCell(i, 1);
					return;
				}
				
				for (j = 0; j < i; j++) {
					if (DebitAdjustment.isNullOrEmptyRow(rows[j])) {
						continue;
					}
					if (rows[j].dbNumber.trim() == row.dbNumber.trim()) {
						$("#errMsg").html("Số phiếu bị trùng với dòng " + (j + 1)).show();
						htGrid.selectCell(i, 1);
						return;
					}
				}
				
				var amt = row["amount"];
				if (isNaN(amt) || Number(amt) <= 0 || Math.floor(Number(amt)) - Number(amt) != 0) {
					$("#errMsg").html("Số tiền phải là số nguyên dương").show();
					htGrid.selectCell(i, 2);
					return;
				}

				if (amt <= 0) {
					$("#errMsg").html("Số tiền phải lớn hơn 0").show();
					htGrid.selectCell(i, 2);
					return;
				}
				
				if (amt.toString().length > 15 || amt > 999999999999999) {
					$("#errMsg").html("Số tiền chỉ được nhập tối đa 15 chữ số").show();
					htGrid.selectCell(i, 2);
					return;
				}
				
				if(row["reason"].trim().length > 250){
					$("#errMsg").html("Lý do chỉ được tối đa 250 ký tự").show();
					htGrid.selectCell(i, 3);
					return;
				}
				
				if(!/^[0-9a-zA-Z-_.]+$/.test(row["shortCode"])){
					$("#errMsg").html(format(msgErr_invalid_format_code, "Khách hàng")).show();
					htGrid.selectCell(i, 4);
					return;
				}
				
				dbObj = {
						debitType: DebitAdjustment._lstDebitTypes.indexOf(row["debitType"]),
						//type: 1, // KH - NPP
						dbNumber: row["dbNumber"].trim(),
						amount: row["amount"],
						reason: row["reason"].trim(),
						shortCode: row["shortCode"].trim()
				};
				
				lstDebit.push(dbObj);
			}
			
			if (lstDebit.length == 0) {
				$("#errMsg").html("Không có dòng dữ liệu nào để lưu").show();
				return;
			}
			
			var dataModel = {type:1, lstDebit:lstDebit, shopCode: $('#shopCode').val().trim()};
			//dataModel[JSONUtil._VAR_NAME]= "debitObject";
			
			JSONUtil.saveData2(dataModel, "/debit/adjustment/save-debit", "Bạn có muốn lưu thông tin này?",
					"errMsg", function() {
//				setTimeout(function() {
//					DebitAdjustment.initGrid();
//				}, 3000);
			});
			return;
		},
		
		isNullOrEmptyRow: function(row) {
			var s = null;
			for (var p in row) {
				s = row[p];
				if (s != undefined && s != null && ((p == "amount" && s.toString().trim().length > 0) || s.trim().length > 0)) {
					return false;
				}
			};
			return true;
		},
		
		isNullOrEmptyCell: function(s, isNo) {
			if (s == undefined || s == null) {
				return true;
			}
			return ((!isNo && s.trim().length == 0) || (s.toString().trim().length == 0));
		},
		
		_colShopNames: ["Loại phiếu", "Số phiếu", "Số tiền", "Lý do"],
		saveShopDebit: function() {
			$(".ErrorMsgStyle").hide();
			
			var htGrid = $("#dgrid").handsontable("getInstance");
			var rows = htGrid.getData();
			if (rows == null || rows.length == 0) {
				$("#errMsg").html("Không có dòng dữ liệu nào để lưu").show();
				return;
			}
			
			var lstDebit = [];
			var dbObj = null;
			var row = null;
			var j = 0;
			for (var i = 0, sz = rows.length; i < sz; i++) {
				row = rows[i];
				
				if (DebitAdjustment.isNullOrEmptyRow(row)) {
					continue;
				}
				
				j = 0;
				for (var p in row) {
					if (DebitAdjustment.isNullOrEmptyCell(row[p], p == "amount")) {
						$("#errMsg").html("Bạn chưa nhập giá trị cho trường " + Utils.XSSEncode(DebitAdjustment._colShopNames[j])).show();
						htGrid.selectCell(i, j);
						return;
					}
					j++;
				}
				
				if (DebitAdjustment._lstDebitTypes.indexOf(row["debitType"]) < 0) {
					$("#errMsg").html("Loại phiếu không hợp lệ").show();
					htGrid.selectCell(i, 0);
					return;
				}
				
				if(!/^[0-9a-zA-Z-_.]+$/.test(row["dbNumber"])){
					$("#errMsg").html(format(msgErr_invalid_format_code, "Số phiếu")).show();
					htGrid.selectCell(i, 1);
					return;
				}
				
				if(row["dbNumber"].trim().length > 40){
					$("#errMsg").html("Số phiếu chỉ được tối đa 40 ký tự").show();
					htGrid.selectCell(i, 1);
					return;
				}
				
				for (j = 0; j < i; j++) {
					if (DebitAdjustment.isNullOrEmptyRow(rows[j])) {
						continue;
					}
					if (rows[j].dbNumber.trim() == row.dbNumber.trim()) {
						$("#errMsg").html("Số phiếu bị trùng với dòng " + (j + 1)).show();
						htGrid.selectCell(i, 1);
						return;
					}
				}
				
				var amt = row["amount"];
				if (isNaN(amt) || Number(amt) <= 0 || Math.floor(Number(amt)) - Number(amt) != 0) {
					$("#errMsg").html("Số tiền phải là số nguyên dương").show();
					htGrid.selectCell(i, 2);
					return;
				}

				if (amt <= 0) {
					$("#errMsg").html("Số tiền phải lớn hơn 0").show();
					htGrid.selectCell(i, 2);
					return;
				}
				
				if (amt.toString().length > 15 || amt > 999999999999999) {
					$("#errMsg").html("Số tiền chỉ được nhập tối đa 15 chữ số").show();
					htGrid.selectCell(i, 2);
					return;
				}
				
				if(row["reason"].trim().length > 250){
					$("#errMsg").html("Lý do chỉ được tối đa 250 ký tự").show();
					htGrid.selectCell(i, 3);
					return;
				}
				
				dbObj = {
						debitType: DebitAdjustment._lstDebitTypes.indexOf(row["debitType"]),
						//type: 2, // NPP - cong ty
						dbNumber: row["dbNumber"].trim(),
						amount: row["amount"],
						reason: row["reason"].trim()
				};
				
				lstDebit.push(dbObj);
			}
			
			if (lstDebit.length == 0) {
				$("#errMsg").html("Không có dòng dữ liệu nào để lưu").show();
				return;
			}
			
			var dataModel = {type:2, lstDebit:lstDebit, shopCode: $('#shopCode').val().trim()};
			//dataModel[JSONUtil._VAR_NAME]= "debitObject";
			JSONUtil.saveData2(dataModel, "/debit/adjustment/save-debit", "Bạn có muốn lưu thông tin này?",
					"errMsg", function() {
				setTimeout(function() {
					window.location.href = "/debit/adjustment/shop";
				}, 3000);
			});
			return;
		}
};
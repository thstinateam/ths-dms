/*
 * JavaScript file created by Rockstarapps Concatenation
*/

/*
 * START OF FILE - /web/web/resources/scripts/business/program/vnm.focus-program.js
 */
var FocusProgram = {
	_xhrSave : null,
	_xhrDel: null,
	_mapShop:null,
	_mapProduct:null,
	_listShopId:null,
	_lstSaleTypeData: new Map(),
	_lstTypeData: new Map(),
	_firstCount:true,
	_mapLstProduct:new Map(),
	_searchShopParam: {id:'0',shopCode:'',shopName:''},
	_searchProdParam: {id:'0',code:'',name:''},
	_lstHTBHdb: [],
	hideAllTab: function(){
		$(".ErrorMsgStyle").hide();
		$('#tab1').removeClass('Active');
		$('#tab2').removeClass('Active');
		$('#tab3').removeClass('Active');
		$('#spCTTTTab').hide();
		$('#dvtgCTTTTab').hide();
		$('#htbhCTTTTab').hide();
	},
	showTab1: function(){
		if ($("#tab1.Active").length == 1) {
			return;
		}
		FocusProgram.hideAllTab();
		$('#tab1').addClass('Active');
		$('#htbhCTTTTab').show();
		$('#errExcelMsg').html('').hide();
		$('#idImportDiv').hide();
		$('#codeHTBH').focus();
		Utils.bindAutoButtonEx('.ContentSection','btnSearchSaleType');
		//$("#gridHTBH").datagrid('reload');
	},
	showTab3: function(){
		if ($("#tab3.Active").length == 1) {
			return;
		}
		FocusProgram.hideAllTab();
		$('#tab3').addClass('Active');
		$('#dvtgCTTTTab').show();
		$('#idImportDiv').show();
		$('#shopCode').focus();
		Utils.bindAutoButtonEx('.ContentSection','btnSearchShop');
		$('#downloadTemplate').attr( 'href', excel_template_path + 'catalog/Bieu_mau_danh_muc_NPPThamgiavaoCTTT_import.xls');
		$('#excelType').val(2);
		$('#errExcelMsg').html('').hide();
		var value = $('#divWidth').width()-50;
		var dataWidth = (value * 80)/100;
		var editWidth = (value * 10)/100;
		var deleteWidth = (value * 9)/100+6;
		var statusPermission = $('#statusPermission').val();
		var titleOpenDialogShop ="";
		if(statusPermission == 2){
			titleOpenDialogShop = '<a href="javascript:void(0);" onclick="return FocusProgram.openShopDialog();"><img src="/resources/images/icon-add.png"></a>';
		}
		FocusProgram._searchShopParam.id = $('#progId').val().trim();
		$('#exGrid').treegrid({
		    url:  FocusProgram.getShopGridUrl($('#progId').val(),'',''),
		     width:($('#divWidth').width()-50),
	        height:'auto',  
	        idField: 'id',  
	        treeField: 'data',
		    columns:[[  
		        {field:'data',title:'Đơn vị',resizable:false,width:dataWidth, formatter: function(value, row, index){
			    		return Utils.XSSEncode(value);
	            }},
		        {field:'edit',title:titleOpenDialogShop,width:editWidth,align:'center',resizable:false,formatter:function(value,row,index){
			        	if(row.isShop == 0){
			        		if(statusPermission == 2){
			        			return '<a title="Thêm mới" href="javascript:void(0);" onclick="return FocusProgram.openShopDialog(' +row.id+');"><img src="/resources/images/icon-add.png"></a>';
			        		}
			        	}
		        }},
		        {field:'delete',title:'',width:deleteWidth,align:'center',resizable:false,formatter:function(value,row,index){
		        		if(statusPermission == 2){
		        			return '<a title="Xóa" href="javascript:void(0);" onclick="return FocusProgram.deleteFocusShopMap(' +row.id+');"><img src="/resources/images/icon-delete.png"></a>';
		        		}
		        }}
		    ]] 
		});
	},
	importExcel:function(){
		$(".ErrorMsgStyle").hide();
 		$('#isView').val(0);
		$('#importFrm').ajaxForm(options);
 		$('#importFrm').submit();
 		return false;
 	},
 	afterImportExcelUpdate: function(responseText, statusText, xhr, $form){
		hideLoadingIcon();
		if (statusText == 'success') {				
	    	$("#responseDiv").html(responseText);		 
	    	var newToken = $('#responseDiv #newToken').val();
	    	console.log(newToken);
	    	if(newToken != null && newToken != undefined && newToken != '') {
	    		$('#token').val(newToken);	    		
	    		$('#tokenImport').val(newToken);
	    	}
	    	if($('#errorExcel').html().trim() == 'true' || $('#errorExcelMsg').html().trim().length > 0){
	    		$('#errExcelMsg').html($('#errorExcelMsg').html()).show();
	    	} else {
	    		var totalRow = parseInt($('#totalRow').html().trim());
    			var numFail = parseInt($('#numFail').html().trim());
    			var fileNameFail = $('#fileNameFail').html();
    			var mes = format(msgErr_result_import_excel,(totalRow - numFail),numFail);		    		
    			if(numFail > 0){
    				mes+= ' <a href="' + fileNameFail +'">Xem chi tiết lỗi</a>';
    			}
    			if($('#excelFile').length!=0 && $('#fakefilepc').length!=0){
    				try{$('#excelFile').val('');$('#fakefilepc').val('');}catch(err){}
    			}
    			$('#errExcelMsg').html(mes).show();
    			if ($("#tab2.Active").length == 1) {
    				$("#grid").datagrid("reload");
    			} else if ($("#tab3.Active").length == 1) {
    				$("#exGrid").treegrid("reload");
    			}
	    	}
	    }
	},
	gotoTab: function(tabIndex){
		FocusProgram.deactiveAllMainTab();
		switch (tabIndex) {
		case 0:
			$('#tabContent1').show();
			$('#tab1 a').addClass('Active');
			break;
		case 1:
			$('#tabContent2').show();
			$('#tab2 a').addClass('Active');
			AttributesManager.getAttributesForm('tabContent2',AttributesManager.FOCUS_PROGRAM);
			$('#typeAttribute').val(AttributesManager.FOCUS_PROGRAM);
			break;
		default:
			$('#tabContent1').show();
			break;
		}
	},
	upload : function(){ //upload trong fancybox
		var options = { 					 		
	 		beforeSubmit: ProductLevelCatalog.beforeImportExcel,   
	 		success:      ProductLevelCatalog.afterImportExcelUpdate, 
	 		type: "POST",
	 		dataType: 'html'
	 	}; 
		$('#importShopFrm').ajaxForm(options);
		$.messager.confirm('Xác nhận','Bạn có muốn nhập từ file?',function(r){
			if (r){						
				$('#importShopFrm').submit();						
			}
		});
	},
	deactiveAllMainTab: function(){
		$(".ErrorMsgStyle").hide();
		$('#tab1 a').removeClass('Active');
		$('#tab2 a').removeClass('Active');
		$('#tabContent1').hide();
		$('#tabContent2').hide();
	},
	getGridUrl: function(code,name,shopId,fDate,tDate,status){		
		return "/focus-program/search?code=" + encodeChar(code) + "&name=" + encodeChar(name) + "&shopId=" + shopId + "&fromDate=" + fDate + "&toDate=" + tDate + '&status=' + status;
	},	
	search: function(){
		$(".ErrorMsgStyle").hide();
		$('#errMsg').html('').hide();
		var code = $('#code').val().trim();
		var name = $('#name').val().trim();
		var shopId = $('#shopId').val().trim();
		var fromDate = $('#fDate').val();
		var toDate = $('#tDate').val();	
		var status = 1;
		if($('#permissionUser').val() == 'true'){
			status = $('#status').val();
		}
		var msg = '';
		if(msg.length == 0){
			msg = Utils.getMessageOfSpecialCharactersValidate('code','Mã CTTT',Utils._CODE);
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfSpecialCharactersValidate('name','Tên CTTT');
		}		 
		if(msg.length == 0){
			msg = Utils.getMessageOfInvalidFormatDate('fDate','Từ ngày');
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfInvalidFormatDate('tDate','Đến ngày');
		}
		var fDate = $('#fDate').val();
		var tDate = $('#tDate').val();
		if(msg.length == 0){
			if (fDate != '' && !Utils.isDate(fDate)) {
				msg = 'Từ ngày không hợp lệ, vui lòng nhập theo định dạng dd/MM/yyyy';
				$('#fDate').focus();
			}
		}
		if(msg.length == 0){
			if(tDate != '' && !Utils.isDate(tDate)){
				msg = 'Đến ngày không hợp lệ, vui lòng nhập theo định dạng dd/MM/yyyy';
				$('#tDate').focus();
			}
		}
		//var currentTime = new Date();
		//var month = currentTime.getMonth() + 1;
		//var day = currentTime.getDate();
		//var year = currentTime.getFullYear();
		if(msg.length == 0){
			if(fDate != '' && tDate != ''){
				if(!Utils.compareDate(fDate, tDate)){
					msg = 'Từ ngày phải là ngày trước hoặc cùng ngày với đến ngày';	
					$('#fDate').focus();
				}
			}
		}
		if(msg.length > 0){
			$('#errMsg').html(msg).show();
			return false;
		}
		setTimeout(function() {
			$('#code').focus();
		}, 500);
		var url = FocusProgram.getGridUrl(code,name,shopId,fromDate,toDate,status);
		$("#listCTTTGrid").datagrid({url:url,pageNumber:1});
		//$("#listCTTTGrid").datagrid('load',{page:1,code:code,name:name,shopId:shopId,fromDate:fromDate,toDate:toDate,status:status});
		return false;
	},
	saveInfo: function(){
		$(".ErrorMsgStyle").hide();
		var msg = '';
		$('#errMsg').html('').hide();
		msg = Utils.getMessageOfRequireCheck('code','Mã CTTT');
		if(msg.length == 0){
			msg = Utils.getMessageOfSpecialCharactersValidate('code','Mã CTTT',Utils._CODE);
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfRequireCheck('name','Tên CTTT');
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfSpecialCharactersValidate('name','Tên CTTT');
		}		
		if(msg.length == 0){
			msg = Utils.getMessageOfRequireCheck('fDate','Từ ngày');
		} 
		if(msg.length == 0){
			msg = Utils.getMessageOfInvalidFormatDate('fDate','Từ ngày');
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfInvalidFormatDate('tDate','Đến ngày');
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfRequireCheck('status','Trạng thái',true);
		}
		var fDate = $('#fDate').val();
		var tDate = $('#tDate').val();
		if($('#status').val().trim() == activeType.WAITING){
			if(msg.length == 0){
				if (fDate != '' && !Utils.isDate(fDate)) {
					msg = 'Từ ngày không hợp lệ, vui lòng nhập theo định dạng dd/MM/yyyy';
					$('#fDate').focus();
				}
			}
		}
		if(msg.length == 0){
//			if(($('#stableToDate').val() != '') && ($('#stableToDate').val().trim() != $('#tDate').val().trim())){
			if($('#stableToDate').val().trim() != $('#tDate').val().trim()){
				if(tDate != '' && !Utils.isDate(tDate)){
					msg = 'Đến ngày không hợp lệ, vui lòng nhập theo định dạng dd/MM/yyyy';
					$('#tDate').focus();
				}
			}
		}
		var currentTime = new Date();
		var month = currentTime.getMonth() + 1;
		var day = currentTime.getDate();
		var year = currentTime.getFullYear();
		var proStatus = $('#proStatus').val();
		if($('#status').val().trim() == activeType.RUNNING){
			if(msg.length == 0){
				if(fDate != '' && tDate != ''){
					if(!Utils.compareDate(fDate, tDate)){
						msg = 'Từ ngày phải là ngày trước hoặc cùng ngày với đến ngày';	
						$('#fDate').focus();
					}
				}
			}
		}

		if($('#status').val().trim() == activeType.RUNNING && proStatus == activeType.WAITING){
			if(msg.length == 0){
				if(fDate != ''){
					if(!Utils.compareDate(day + '/' + month + '/' + year,fDate)){
						msg = 'Từ ngày phải lớn hơn hoặc bằng ngày hiện tại.';	
						$('#fDate').focus(); 
					}
				}
			}
		}
		if(msg.length == 0){
			if($('#stableToDate').val().trim() != $('#tDate').val().trim()){
				if(tDate != ''){
					if(!Utils.compareDate(day + '/' + month + '/' + year,tDate)){
						msg = 'Đến ngày phải lớn hơn hoặc bằng ngày hiện tại.';	
						$('#tDate').focus();
					}
				}
			}
		}	
		if(msg.length > 0){
			$('#errMsgInfo').html(msg).show();
			return false;
		}
		var dataModel = new Object();		
		dataModel.id = $('#selId').val().trim();
		dataModel.code = $('#code').val().trim();
		dataModel.name = $('#name').val().trim();
		dataModel.fromDate = $('#fDate').val().trim();
		dataModel.toDate = $('#tDate').val().trim();
		dataModel.status = $('#status').val().trim();
		if(!Utils.validateAttributeData(dataModel, '#errMsgInfo')){
			return false;
		}
		Utils.addOrSaveData(dataModel, "/focus-program/save-info", FocusProgram._xhrSave, 'errMsgInfo', function(data){
			$('#selId').val(data.id);	
			$('#subContentId').val(0);
			$.cookie('proTabOrder', 1);
			window.location.href = '/focus-program/change?id=' + data.id + '&subContentId=0';	
			showSuccessMsg('successMsg1',data);
		});		
		return false;
	},
	getSelectedProgram: function(id){
		if(id!= undefined && id!= null){
			location.href = '/focus-program/change?id=' + id + '&subContentId=1';			
		} else {
			$('#subContentId').val(0);
			location.href = '/focus-program/change?subContentId=0';
		}		
		return false;
	},
	saveShop: function(){
		$(".ErrorMsgStyle").hide();
		var msg = '';
		$('#errMsgShop').html('').hide();
		var areaId = -2;
		var regionId = -2;
		var shopCode = '';
		var shopMapId = $('#selShopMapId').val().trim();
		if(shopMapId.length == 0 || shopMapId == 0){
			if($('#rbArea').is(':checked')){
				msg = Utils.getMessageOfRequireCheck('area','Miền');
				areaId = $('#area').val();
			}
			if($('#rbRegion').is(':checked')){
				msg = Utils.getMessageOfRequireCheck('region','Vùng');
				regionId = $('#region').val();
			}
			if($('#rbShop').is(':checked')){
				msg = Utils.getMessageOfRequireCheck('shopCode','Mã đơn vị');
				if(msg.length == 0){
					msg = Utils.getMessageOfSpecialCharactersValidate('shopCode','Mã đơn vị',Utils._CODE);
				}
				shopCode = $('#shopCode').val();
			}
		}
		
		if(msg.length == 0){
			msg = Utils.getMessageOfRequireCheck('shopStatus','Trạng thái',true);
		}
		if(msg.length > 0){
			$('#errMsgShop').html(msg).show();
			return false;
		}
		var dataModel = new Object();		
		dataModel.id = $('#selId').val().trim();
		dataModel.shopMapId = $('#selShopMapId').val().trim();
		dataModel.areaId = areaId;
		dataModel.regionId = regionId;		
		dataModel.shopCode = shopCode;		
		dataModel.status = $('#shopStatus').val().trim();
		Utils.addOrSaveRowOnGrid(dataModel, "/focus-program/save-shop", FocusProgram._xhrSave, 'shopGrid','errMsgShop', function(data){
			$('#selShopMapId').val(0);	
			$("#shopGrid").trigger("reloadGrid");
			FocusProgram.resetShopForm();
			showSuccessMsg('successMsgShop',data);
		},'shopLoading');		
		return false;
	},
	getSelectedShopRow:function(rowId,status){
		var code = $("#shopGrid").jqGrid ('getCell', rowId, 'shop.shopCode');
		var name = $("#shopGrid").jqGrid ('getCell', rowId, 'shop.shopName');
		var id = $("#shopGrid").jqGrid ('getCell', rowId, 'id');		
		$('#selShopMapId').val(id);
		$('#shopCode').val(code);
		$('#shopName').val(name);
		setSelectBoxValue('shopStatus',Utils.getStatusValue(status));
		disabled('shopCode');
		$('#btnAddShop').hide();
		$('#btnEditShop').show();
		$('#btnCancelShop').show();
		return false;
	},
	resetShopForm: function(){
		$('#btnAddShop').show();
		$('#btnEditShop').hide();
		$('#btnCancelShop').hide();
		enable('shopCode');
		setSelectBoxValue('shopStatus');
		setSelectBoxValue('area');
		setSelectBoxValue('region');
		$('#selShopMapId').val(0);
		$('#shopCode').val('');
		$('#shopName').val('');
	},
	deleteShopRow: function(id){
		var dataModel = new Object();
		dataModel.shopMapId = id;	
		Utils.deleteSelectRowOnGrid(dataModel, 'đơn vị', '/focus-program/delete-shop', FocusProgram._xhrDel, 'shopGrid','errMsgShop',function(data){
			showSuccessMsg('successMsgShop',data);
		},'shopLoading');		
		return false;
	},
	saveProduct: function(){
		$(".ErrorMsgStyle").hide();
		var msg = '';
		$('#errMsgProduct').html('').hide();
		var catCode = '';
		var subCatCode = '';
		var productCode = '';
		var staffTypeId = $('#staffType').val();
		if($('#rbCat').is(':checked')){
			msg = Utils.getMessageOfRequireCheck('catCode','Ngành hàng');
			catCode = $('#catCode').val();
		}
		if($('#rbSubCat').is(':checked')){
			msg = Utils.getMessageOfRequireCheck('subCatCode','Ngành hàng con');
			subCatCode = $('#subCatCode').val();
		}
		if($('#rbProduct').is(':checked')){
			msg = Utils.getMessageOfRequireCheck('productCode','Mã sản phẩm');			
			productCode = $('#productCode').val();
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfRequireCheck('staffType','Loại nhân viên',true);
		}
		
		if(msg.length > 0){
			$('#errMsgProduct').html(msg).show();
			return false;
		}
		var dataModel = new Object();
		dataModel.id = $('#selId').val().trim();
		dataModel.catCode = catCode;
		dataModel.subCatCode = subCatCode;		
		dataModel.productCode = productCode;
		dataModel.staffTypeId = staffTypeId;
		Utils.addOrSaveRowOnGrid(dataModel, "/focus-program/save-product", FocusProgram._xhrSave, 'productGrid','errMsgProduct', function(data){
			$("#productGrid").trigger("reloadGrid");
			$('#catCode').val('');
			$('#subCatCode').val('');
			$('#productCode').val('');
			$('#productName').val('');
			setSelectBoxValue('staffType');		
			showSuccessMsg('successMsgProduct',data);
		},'productLoading');		
		return false;
	},
	showOthersTab: function(){
		$('#tabActive2').removeClass('Disable');
		$('#tabActive3').removeClass('Disable');
		$('#tabActive4').removeClass('Disable');
		$('#tabActive5').removeClass('Disable');
		//$('#tab1').bind('click',FocusProgram.showTab1);
		//$('#tab2').bind('click',FocusProgram.showTab2);
		//$('#tab3').bind('click',FocusProgram.showTab3);
	}
	,deleteProductRow: function(id){
		$(".ErrorMsgStyle").hide();
		var dataModel = new Object();
		dataModel.productId = id;	
		Utils.deleteSelectRowOnGrid(dataModel, 'Sản phẩm', '/focus-program/delete-product', FocusProgram._xhrDel, 'productGrid','errMsgProduct',function(data){
			showSuccessMsg('successMsgProduct',data);
		},'productLoading');		
		return false;
	},
	searchShop: function(){
		$(".ErrorMsgStyle").hide();
		var code = $('#shopCode').val().trim();
		var name = $('#shopName').val().trim();
		var id = $('#progId').val();
		FocusProgram._searchShopParam.id = id;
		FocusProgram._searchShopParam.shopCode = code;
		FocusProgram._searchShopParam.shopName = name;
		var url = FocusProgram.getShopGridUrl(id,code,name);
		$("#exGrid").treegrid({url:url});
		$('#errExcelMsg').html("").hide();
	},
	showDialogCreateShop: function(){
		$(".ErrorMsgStyle").hide();
		var html = $('#shopTreeDialog').html();
		$.fancybox(html,
				{
					modal: true,
					autoResize: false,
					title: 'Thêm đơn vị tham gia CTTT',
					afterShow: function(){
						Utils.resetCheckboxStateOfTree();
						Utils.bindAutoSearch();	
						$('#shopTreeDialog').html('');
						$('#successMsgShopDlg').html('').hide();
						var focusProgramId = $('#selId').val();
						$('#shopCodeDlg').focus();
						var shopCode = encodeURI('');
						var shopName = encodeURI('');	
						FocusProgram.loadShopTreeOnDialog('/rest/catalog/focus-program/shop/list.json?focusProgramId=' + focusProgramId +'&shopCode=' + encodeChar(shopCode) +'&shopName=' + encodeChar(shopName));
						FocusProgram._mapShop = new Map();
						$('#shopTree').bind("loaded.jstree open_node.jstree close_node.jstree", function (event, data) {
							if($('#shopTreeContent').data('jsp') != undefined || $('#shopTreeContent').data('jsp') != null) {
								$('#shopTreeContent').data('jsp').destroy();
								setTimeout(function(){
									$('#shopTreeContent').jScrollPane();
								},500);	
							} else {
								setTimeout(function(){
									$('#shopTreeContent').jScrollPane();
								},500);	
							}						
		    			});
					},
					afterClose: function(){
						$('#shopTreeDialog').html(html);						
					}
				}
			);
		return false;
	},
	getCheckboxStateOfTreeEx:function(treeId,MAP,selector,nodeType){
		for(var i=0;i<MAP.size();++i){
			var _obj = MAP.get(MAP.keyArray[i]);
			var type = true;			
			$('#' + selector).each(function(){
				var _id = $(this).attr('id');
				if(nodeType!=null && nodeType!=undefined){
					var getType = $(this).attr('contentitemid');
					type = (nodeType==getType);
				}
				if(_id==_obj && type){
					$(this).removeClass('jstree-unchecked');
					$(this).addClass('jstree-checked');
				}
			});
		}
	},	
	searchShopOnTree: function(){
		$(".ErrorMsgStyle").hide();
		//Utils.getCheckboxStateOfTree();
		$('#shopTree li').each(function(){
			var _id = $(this).attr('id');
			if($(this).hasClass('jstree-checked')){
				FocusProgram._mapShop.put(_id,_id);	
			}else{
				FocusProgram._mapShop.remove(_id);
			}		
		});
		var focusProgramId = $('#selId').val();
		var shopCode = encodeChar($('#shopCodeDlg').val().trim());
		var shopName = encodeChar($('#shopNameDlg').val().trim());
		$('#shopTreeContent').data('jsp').destroy();
		FocusProgram.loadShopTreeOnDialog('/rest/catalog/focus-program/shop/list.json?focusProgramId=' + focusProgramId +'&shopCode=' + encodeChar(shopCode) +'&shopName=' + encodeChar(shopName));
		setTimeout(function() {
			$('#shopTreeContent').jScrollPane();
			$('#shopTree').bind("loaded.jstree open_node.jstree close_node.jstree", function (event, data) {
				if($('#shopTreeContent').data('jsp') != undefined || $('#shopTreeContent').data('jsp') != null) {
					$('#shopTreeContent').data('jsp').destroy();
					setTimeout(function(){
						$('#shopTreeContent').jScrollPane();
					},500);	
				} else {
					setTimeout(function(){
						$('#shopTreeContent').jScrollPane();
					},500);	
				}
				FocusProgram.getCheckboxStateOfTreeEx('shopTree',FocusProgram._mapShop,'shopTree li');
			});
			FocusProgram.getCheckboxStateOfTreeEx('shopTree',FocusProgram._mapShop,'shopTree li');
		}, 500);
		$('.fancybox-inner #shopCodeDlg').focus();
	},
	selectListShop: function(){
		$('#errMsgShopDlg').html('').hide();
		$('#successMsgShopDlg').html('').hide();
		$.fancybox.update();		
		var arrId = new Array();
		$('.jstree-checked').each(function(){
		    var _id= $(this).attr('id');
		    if($('#' + _id + ' ul li').length >0 && (!$(this).parent().parent().hasClass('jstree-checked'))){
		    	FocusProgram._mapShop.put(_id,_id);	
		    }else if($('#' + _id ).length >0 && (!$(this).parent().parent().hasClass('jstree-checked'))){
		    	FocusProgram._mapShop.put(_id,_id);	
			}else{
		    	FocusProgram._mapShop.remove(_id);	
			}	
		});
//		$('#shopTree li').each(function(){
//			var _id = $(this).attr('id');			
//			if($(this).hasClass('jstree-checked')){
//				FocusProgram._mapShop.put(_id,_id);		
//			}else{
//				FocusProgram._mapShop.remove(_id);	
//			}			
//		});
		for(var i=0;i<FocusProgram._mapShop.size();++i){
			var _obj = FocusProgram._mapShop.get(FocusProgram._mapShop.keyArray[i]);
			arrId.push(_obj);
		}		
		var msg = '';
		if(arrId.length == 0){
			msg = format(msgErr_required_choose_format,'Đơn vị');
		}		
		if(msg.length > 0){
			$('#errMsgShopDlg').html(msg).show();			
			$.fancybox.update();
			return false;
		}		
		var focusProgramId = $('#selId').val();
		var dataModel = new Object();		
		dataModel.lstShopId = arrId;		
		dataModel.id = focusProgramId;
		dataModel.status = -2;
		Utils.addOrSaveData(dataModel, '/focus-program/shop/save', FocusProgram._xhrSave, 'errMsgShopDlg', function(data){
			$('#successMsgShopDlg').html('Lưu dữ liệu thành công').show();
			$('#shopCodeDlg').val('');
			$('#shopNameDlg').val('');
			$('#shopCode').val('');
			$('#shopName').val('');
			setSelectBoxValue('shopStatus', activeType.RUNNING);
			Utils.resetCheckboxStateOfTree();
			FocusProgram.searchShopOnTree();	
			FocusProgram.searchShop();
			$(window).resize();
			$.fancybox.update();
			//$("#shopGrid").trigger("reloadGrid");
			setTimeout(function(){$.fancybox.close();}, 1000);
		}, null, '.fancybox-inner', null, null);
		return false;
	},
	showUpdateShopDetailDialog: function(rowId,status){
		$(".ErrorMsgStyle").hide();
		var html = $('#shopDetailTreeDialog').html();
		$.fancybox(html,
				{
					modal: true,
					autoResize: false,
					title: 'Cập nhật thông tin đơn vị',					
					afterShow: function(){
						$('#shopDetailTreeDialog').html('');
						$('#errMsgShopDetailDlg').html('').hide();
						$('#successMsgShopDetailDlg').html('').hide();						
						
						var name = $("#shopGrid").jqGrid ('getCell', rowId, 'shop.shopName');
						var id = $("#shopGrid").jqGrid ('getCell', rowId, 'shop.id');					    						
						if(status == 'RUNNING'){
							status = 1;
						} else if(status == 'STOPPED'){
							status = 0;
						} else {
							status = -2;
						}						
						$('#shopNameDetailDlg').val(name);
						$('#shopStatusDlgTmp').attr('id','shopStatusDlg');
						$('#shopStatusDlg').val(status);
						$('#shopStatusDlg').addClass('MySelectBoxClass');
						$('#shopStatusDlg').customStyle();						
						$('#shopIdDetailDlg').val(id);
					},
					afterClose: function(){
						$('#shopDetailTreeDialog').html(html);						
					}
				}
			);
		return false;
	},
	saveDetailShop: function(){
		$(".ErrorMsgStyle").hide();
		$('#errMsgShopDetailDlg').html('').hide();
		$('#successMsgShopDetailDlg').html('').hide();
		$.fancybox.update();
		var msg = '';
		msg = Utils.getMessageOfRequireCheck('shopStatusDlg', 'Trạng thái',true);		
		if(msg.length > 0){
			$('#errMsgShopDetailDlg').html(msg).show();
			$.fancybox.update();
			return false;
		}		
		var focusProgramId = $('#selId').val();
		var arrId = new Array();
		arrId.push($('#shopIdDetailDlg').val());
		var dataModel = new Object();
		dataModel.lstShopId = arrId;
		dataModel.id = focusProgramId;
		dataModel.status = $('#shopStatusDlg').val().trim();
		dataModel.numUpdate = 0; 
		Utils.addOrSaveData(dataModel, '/focus-program/shop/save', ProgrammeDisplayCatalog._xhrSave, 'errMsgShopDlg', function(data){
			$('#successMsgShopDlg').html('Lưu dữ liệu thành công').show();						
			$.fancybox.update();
			FocusProgram.searchShop();
			setTimeout(function(){$.fancybox.close();}, 1000);
		}, null, '.fancybox-inner', null, null);
		return false;
	},
	showDialogCreateProduct: function(){
		var html = $('#productTreeDialog').html();
		$.fancybox(html,
				{
					modal: true,
					autoResize: false,
					title: 'Thêm thông tin sản phẩm',
					afterShow: function(){
						Utils.resetCheckboxStateOfTree();
						Utils.bindAutoSearch();	
						$('#productTreeDialog').html('');
						var focusProgramId = $('#selId').val();
						$('#productCodeDlg').focus();
						$('#saleManDlgTmp').attr('id','saleManDlg');
						$('#saleManDlg').addClass('MySelectBoxClass');
						$('#saleManDlg').customStyle();
						$('#typeDialog').addClass('MySelectBoxClass');
						$('#typeDialog').customStyle();
						$.getJSON('/rest/catalog/focus-program/' + focusProgramId +'/staff-type/list.json', function(data){
							var arrHtml = new Array();
							arrHtml.push('<option value="-2">-Chọn hình thức bán hàng-</option>');
							for(var i=0;i<data.length;i++){
								arrHtml.push('<option value="' + data[i].value +'">' + Utils.XSSEncode(data[i].name) +'</option>');
							}
							$('#saleManDlg').html(arrHtml.join(""));
							$('#saleManDlg').change();
						});						
						var productCode = encodeURI('');
						var productName = encodeURI('');						
						FocusProgram.loadDataForTreeWithCheckboxEx('/rest/catalog/focus-program/product/list.json?focusProgramId=' + focusProgramId +'&productCode=' + encodeChar(productCode) +'&productName=' + encodeChar(productName) + '&nodeType=cat&catId=0&subCatId=0','productTree');
						FocusProgram._mapProduct = new Map();
						$('#productTree').bind("loaded.jstree open_node.jstree close_node.jstree", function () {
							if($('#productTreeContent').data('jsp') != undefined || $('#productTreeContent').data('jsp') != null) {
								$('#productTreeContent').data('jsp').destroy();
								setTimeout(function(){
									$('#productTreeContent').jScrollPane();
								},500);	
							} else {
								setTimeout(function(){
									$('#productTreeContent').jScrollPane();
								},500);	
							}
		    			});
					},
					afterClose: function(){
						$('#productTreeDialog').html(html);						
					}
				}
			);
		return false;
	},
	searchProductOnTree: function(){
		$(".ErrorMsgStyle").hide();
		//Utils.getCheckboxStateOfTree();
		$('#productTree li').each(function(){
			var _id = $(this).attr('id');
			var type = $(this).attr('contentitemid');
			if($(this).hasClass('jstree-checked') && type=='product'){
				FocusProgram._mapProduct.put(_id,_id);		
			}else {
				FocusProgram._mapProduct.remove(_id);
			}
		});
		var focusProgramId = $('#selId').val();
		var productCode = encodeChar($('#productCodeDlg').val().trim());
		var productName = encodeChar($('#productNameDlg').val().trim());
		$('#productTreeContent').data('jsp').destroy();
		FocusProgram.loadDataForTreeWithCheckboxEx('/rest/catalog/focus-program/product/list.json?focusProgramId=' + focusProgramId +'&productCode=' + encodeChar(productCode) +'&productName=' + encodeChar(productName) + '&nodeType=cat&catId=0&subCatId=0','productTree');
		setTimeout(function() {
			$('#productTreeContent').jScrollPane();
			$('#productTree').bind("loaded.jstree open_node.jstree close_node.jstree", function (event, data) {
				//Utils.applyCheckboxStateOfTree();
				if($('#productTreeContent').data('jsp') != undefined || $('#productTreeContent').data('jsp') != null) {
					$('#productTreeContent').data('jsp').destroy();
					setTimeout(function(){
						$('#productTreeContent').jScrollPane();
					},500);	
				} else {
					setTimeout(function(){
						$('#productTreeContent').jScrollPane();
					},500);	
				}
				FocusProgram.getCheckboxStateOfTreeEx('productTree',FocusProgram._mapProduct,'productTree li','product');
			});
			FocusProgram.getCheckboxStateOfTreeEx('productTree',FocusProgram._mapProduct,'productTree li','product');
		}, 500);
		$('.fancybox-inner #productCodeDlg').focus();
	},
	selectListProduct: function(){
		$('#errMsgProductDlg').html('').hide();
		$('#successMsgProductDlg').html('').hide();
		$.fancybox.update();
		var arrId = new Array();
/*		$('.jstree-leaf').each(function(){
			if($(this).hasClass('jstree-checked')){
				arrId.push($(this).attr('id'));
			}			
		});
		$('li[classstyle=product]').each(function(){
		    if($(this).hasClass('jstree-checked')){
		    	arrId.push($(this).attr('id'));
		    }
		});*/
		$('#productTree li').each(function(){
			var _id = $(this).attr('id');
			var type = $(this).attr('contentitemid');
			if($(this).hasClass('jstree-checked') && type=='product'){
				FocusProgram._mapProduct.put(_id,_id);		
			}else{
				FocusProgram._mapProduct.remove(_id);	
			}			
		});
		for(var i=0;i<FocusProgram._mapProduct.size();++i){
			var _obj = FocusProgram._mapProduct.get(FocusProgram._mapProduct.keyArray[i]);
			arrId.push(_obj);
		}
		var msg = '';
		if(arrId.length == 0){
			msg = format(msgErr_required_choose_format,'Sản phẩm');
		}	
		if(msg.length ==0){
			msg = Utils.getMessageOfRequireCheck('saleManDlg', 'Hình thức bán hàng',true);
		}
		if(msg.length ==0){
			msg = Utils.getMessageOfRequireCheck('typeDialog', 'Loại MHTT',true);
		}
		if(msg.length > 0){
			$('#errMsgProductDlg').html(msg).show();
			$.fancybox.update();
			return false;
		}		
		var focusProgramId = $('#selId').val();
		var dataModel = new Object();		
		dataModel.lstProductId = arrId;		
		dataModel.id = focusProgramId;
		dataModel.channelTypeId = $('#saleManDlg').val().trim();
		dataModel.type = $('#typeDialog').val().trim();
		Utils.addOrSaveData(dataModel, '/focus-program/product/save', ProgrammeDisplayCatalog._xhrSave, 'errMsgProductDlg', function(data){
			$('#successMsgProductDlg').html('Lưu dữ liệu thành công').show();
			$('#productCodeDlg').val('');
			$('#productNameDlg').val('');	
			$('#productCode').val('');
			$('#productName').val('');
			setSelectBoxValue('saleManDlg');
			setSelectBoxValue('typeDialog');
			setSelectBoxValue('type');
			Utils.resetCheckboxStateOfTree();
			FocusProgram.searchProductOnTree();
			FocusProgram.searchProduct();
			$.fancybox.update();
			//$("#productGrid").trigger("reloadGrid");
			setTimeout(function(){$.fancybox.close();}, 1000);
		}, null, '.fancybox-inner', null, null);
		return false;
	},	
	showUpdateProductDialog: function(rowId){
		var html = $('#productDetailTreeDialog').html();
		$.fancybox(html,
				{
					modal: true,
					autoResize: false,
					title: 'Cập nhật thông tin sản phẩm',					
					afterShow: function(){
						$('#productDetailTreeDialog').html('');
						$('#errMsgProductDetailDlg').html('').hide();
						$('#successMsgProductDetailDlg').html('').hide();
						var focusProgramId = $('#selId').val();
						$('#saleManDetailDlgTmp').attr('id','saleManDetailDlg');
						$('#saleManDetailDlg').addClass('MySelectBoxClass');
						$('#saleManDetailDlg').customStyle();		
						$('#typeDetailDialog').addClass('MySelectBoxClass');
						$('#typeDetailDialog').customStyle();
						$.getJSON('/rest/catalog/focus-program/' + focusProgramId +'/staff-type/list.json', function(data){
							var arrHtml = new Array();
							arrHtml.push('<option value="-2">--- Chọn kiểu bán hàng ---</option>');
							for(var i=0;i<data.length;i++){
								arrHtml.push('<option value="' + data[i].value +'">' + Utils.XSSEncode(data[i].name) +'</option>');
							}
							//Chu y cho nay: vi focusChannelMap.staffSale.id da bi bo di
//							var channelTypeId = $("#productGrid").jqGrid ('getCell', rowId, 'focusChannelMap.staffSale.id');
							$('#saleManDetailDlg').html(arrHtml.join(""));
							var saleStaffType = $("#productGrid").jqGrid ('getCell', rowId, 'focusChannelMap.saleTypeCode');
							$.getJSON('/rest/catalog/focus-program/apparam/' + saleStaffType +'/id.json', function(data){
								$('#saleManDetailDlg').val(data.value);
								$('#saleManDetailDlg').change();
							});
						});
						var name = $("#productGrid").jqGrid ('getCell', rowId, 'product.productName');						
						var productId = $("#productGrid").jqGrid ('getCell', rowId, 'product.id');
						$('#productNameDetailDlg').val(name);
						$('#productIdDetailDlg').val(productId);
						var type = $("#productGrid").jqGrid ('getCell', rowId, 'type');
						//Chu y cho nay vi: type da chuyen sang kieu String//
//						type = focusProductType.parseValue(type);
//						setSelectBoxValue('typeDetailDialog', type);
						$.getJSON('/rest/catalog/focus-program/apparam/' + type +'/id.json', function(data){
							setSelectBoxValue('typeDetailDialog', data.value);
						});						
					},
					afterClose: function(){
						$('#productDetailTreeDialog').html(html);						
					}
				}
			);
		return false;
	},
	saveDetailProduct: function(){
		$('#errMsgProductDetailDlg').html('').hide();
		$('#successMsgProductDetailDlg').html('').hide();
		$.fancybox.update();		
		var msg = '';			
		if(msg.length ==0){
			msg = Utils.getMessageOfRequireCheck('saleManDetailDlg', 'Hình thức bán hàng',true);
		}
		if(msg.length ==0){
			msg = Utils.getMessageOfRequireCheck('typeDetailDialog', 'Loại MHTT',true);
		}
		if(msg.length > 0){
			$('#errMsgProductDetailDlg').html(msg).show();
			$.fancybox.update();
			return false;
		}	
		var arrId = new Array();
		arrId.push($('#productIdDetailDlg').val().trim());
		var focusProgramId = $('#selId').val();
		var dataModel = new Object();		
		dataModel.lstProductId = arrId;		
		dataModel.id = focusProgramId;
		dataModel.channelTypeId = $('#saleManDetailDlg').val().trim();
		dataModel.type = $('#typeDetailDialog').val().trim();
		Utils.addOrSaveData(dataModel, '/focus-program/product/save', ProgrammeDisplayCatalog._xhrSave, 'errMsgProductDetailDlg', function(data){
			$('#successMsgProductDetailDlg').html('Lưu dữ liệu thành công').show();						
			$.fancybox.update();
			$("#productGrid").trigger("reloadGrid");
			setTimeout(function(){$.fancybox.close();}, 1000);
		}, null, '.fancybox-inner', null, null);
		return false;
	},
	loadShopTreeOnDialog: function(url){
		$.getJSON(url, function(data){
			$('#shopTree').bind("loaded.jstree open_node.jstree close_node.jstree", function (event, data) {				
				if($('#shopTreeContent').data('jsp') != undefined || $('#shopTreeContent').data('jsp') != null) {
					$('#shopTreeContent').data('jsp').destroy();
					setTimeout(function(){
						$('#shopTreeContent').jScrollPane();
					},500);	
				} else {
					setTimeout(function(){
						$('#shopTreeContent').jScrollPane();
					},500);	
				}
			}).jstree({
		        "plugins": ["themes", "json_data","ui","checkbox"],
		        "themes": {
		            "theme": "classic",
		            "icons": false,
		            "dots": true
		        },
		        "json_data": {
		        	"data": data,
		        	"ajax" : {
		        		"method": "GET",
		                "url" : '/rest/catalog/sub-shop/list.json?focusProgramId=' +$('#selId').val(),
		                "data" : function (n) {
		                        return { id : n.attr ? n.attr("id") : 0 };
		                    }
		            }
		        }
			});
		});
	},
	loadDataForTreeWithCheckbox:function(url,treeId){
		var tId = 'tree';
		if(treeId!= null && treeId!= undefined){
			tId = treeId;
		}
		$('#' + tId).jstree({
	        "plugins": ["themes", "json_data","ui","checkbox"],
	        "themes": {
	            "theme": "classic",
	            "icons": false,
	            "dots": true
	        },
	        "json_data": {
	        	"ajax" : {
	        		"method": "post",
	                "url" : url,
	                "data" : function (n) {
	                        return { id : n.attr ? n.attr("id") : 0 };
	                    }
	            }
	        }
		});	
	},
	loadDataForTreeWithCheckboxEx:function(url,treeId){
		var tId = 'tree';
		if(treeId!= null && treeId!= undefined){
			tId = treeId;
		}
		$('#' + tId).jstree({
	        "plugins": ["themes", "json_data","ui","checkbox"],
	        "themes": {
	            "theme": "classic",
	            "icons": false,
	            "dots": true
	        },
	        "json_data": {
	        	"ajax" : {
	        		"method": "get",
	                "url" : function( node ){	                	
	                      if(node == -1) {
	                    	  return url;
	                      } else {
	                    	  var nodeType = node.attr("classStyle");
	                    	  var catId = 0;
	                    	  var subCatId = 0;
	                    	  if(nodeType == 'cat') {
	                    		  catId = node.attr("id");
	                    		  var focusProgramId = $('#selId').val();
	                    		  var productCode = encodeURI('');
	                    		  var productName = encodeURI('');	
	                    		  return '/rest/catalog/focus-program/product/list.json?focusProgramId=' + focusProgramId +'&productCode=' + encodeChar(productCode) +'&productName=' + encodeChar(productName) + '&nodeType=sub-cat&catId=' +catId+ '&subCatId=0';
	                    	  } else if(nodeType == 'sub-cat') {
	                    		  subCatId = node.attr("id");
	                    		  catId = $('#' +subCatId).parent().parent().attr('id');
	                    		  var focusProgramId = $('#selId').val();
	                    		  var productCode = encodeURI('');
	                    		  var productName = encodeURI('');
	                    		  return '/rest/catalog/focus-program/product/list.json?focusProgramId=' + focusProgramId +'&productCode=' + encodeChar(productCode) +'&productName=' + encodeChar(productName) + '&nodeType=product&catId=' +catId+ '&subCatId=' + subCatId;
	                    	  } else if(nodeType == 'product') {
	                    		  return '';
	                    	  }
	                      }
	                    },
	                "data" : function (n) {
	                        return { id : n.attr ? n.attr("id") : 0 };
	                    }
	            }
	        }
		});	
	},
	exportExcelData:function(){
		$('#divOverlay').show();
		$('#errMsg').html('').hide();
		$('#errExcelMsg').html('').hide();
		var data = new Object();
		data.id = $('#selId').val().trim();
		data.excelType = $('#excelType').val();
		if($('#excelType').val() == 2){//dvtg
			data.id = FocusProgram._searchShopParam.id;
			data.shopCode = FocusProgram._searchShopParam.shopCode;
			data.shopName = FocusProgram._searchShopParam.shopName;
		}else if($('#excelType').val() == 3){//sp cttt
			data.id = FocusProgram._searchProdParam.id;
			data.code = FocusProgram._searchProdParam.code;
			data.name = FocusProgram._searchProdParam.name;
		}
		var url = "/focus-program/getlistexceldata";
		CommonSearch.exportExcelData(data,url);
	},
	exportActionLog: function(){
		var id = $('#selId').val();
		var fromDate = $('#fromDate').val().trim();
		var toDate = $('#toDate').val().trim();
		ExportActionLog.exportActionLog(id, ExportActionLog.FOCUS_PROGRAM, fromDate, toDate,'errMsgActionLog');
	},
	openCopyFocusProgram:function(){
		CommonSearch.openSearchStyle1EasyUICopyPromotionShop('Mã CTTT <span class="RequireStyle" style="color:red">*</span>',
				'Tên CTTT <span class="RequireStyle" style="color:red">*</span>', "Sao chép CTTT","focusProgramCode",
				"focusProgramName",'searchStyle1EasyUIDialogDivEx');
	},
//	SAN PHAM CHUONG TRINH TRONG TAM[[[[
	getProductGridUrl: function(code,name){
		var selId = $('#selId').val();
		FocusProgram._searchProdParam.id = selId;
		FocusProgram._searchProdParam.code = code;
		FocusProgram._searchProdParam.name = name;
		return '/focus-program/search-product?id=' + encodeChar(selId) + '&code=' + encodeChar(code) + '&name=' + encodeChar(name) /*+ '&type=' + encodeChar(type)*/;
	},
	showTab2: function(){
		if ($("#tab2.Active").length == 1) {
			return;
		}
		$("#htbhCTTTTab #errMsgTmp").hide();
		var rows = FocusProgram._mapHTBH.keyArray;
		var lst1 = FocusProgram._lstHTBHdb;
		if (rows.length != lst1.length) {
			if ($("#tab1.Active").length == 0) {
				FocusProgram.showTab1();
			}
			$("#htbhCTTTTab #errMsgTmp").html("<p id='errMsgTmp' class='ErrorMsgStyle'>Bạn phải lưu thay đổi HTBH trước khi qua tab sản phẩm</p>").show();
			return;
		}
		for (var j = 0, sz = rows.length; j < sz; j++) {
			if (lst1.indexOf(rows[j]) < 0) {
				if ($("#tab1.Active").length == 0) {
					FocusProgram.showTab1();
				}
				$("#htbhCTTTTab #errMsgTmp").html("<p id='errMsgTmp' class='ErrorMsgStyle'>Bạn phải lưu thay đổi HTBH trước khi qua tab sản phẩm</p>").show();
				return;
			}
		}
		//var idx = -1;
		/*$('#tab1').removeClass('Active');
		$('#tab2').removeClass('Active');
		$('#tab3').removeClass('Active');
		$('#spCTTTTab').hide();
		$('#dvtgCTTTTab').hide();
		$('#htbhCTTTTab').hide();*/
		FocusProgram.hideAllTab();
		$('#tab2').addClass('Active');
		$('#spCTTTTab').show();
		$('#idImportDiv').show();
		$('#productCode').focus();
		Utils.bindAutoButtonEx('.ContentSection','btnSearchProduct');
		$('#downloadTemplate').attr('href',excel_template_path + 'catalog/Bieu_mau_danh_muc_cttt_HTBH_TTSP.xls');
		$('#excelType').val(3);
		$('#errExcelMsg').html('').hide();
		var title = '';
		if($('#statusHidden').val().trim() == activeType.WAITING){
			title = '<a title="Thêm mới" href="javascript:void(0);" onclick="FocusProgram.openEasyUIDialog();"><img src="/resources/images/icon-add.png"></a>';
		}
		$("#grid").datagrid({
			  url:FocusProgram.getProductGridUrl('',''),
			  columns:[[		
			    {field:'categoryCode', title: 'Ngành', width: 80, sortable:false,resizable:false, align: 'left',
			    	formatter: function(index,row){
				    	if(row.product != null && row.product != undefined && row.product.cat != null&& row.product.cat != undefined){
				    		return Utils.XSSEncode(row.product.cat.productInfoCode);
				    	}
			    	}
			    },
			    {field:'productCode', title: 'Mã SP', width: 100, sortable:false,resizable:false , align: 'left',
			    	formatter:function(index,row){
				    	if(row.product!=null){
				    		return Utils.XSSEncode(row.product.productCode);
				    	}
				    }
			    },
			    {field:'productName', title: 'Tên SP', width: 100, align: 'left', sortable:false,resizable:false ,  
		            formatter: function(index,row){
		            	if(row.product!=null){
				    		return Utils.XSSEncode(row.product.productName);
				    	}
		            }
			    },
			    {field:'createUser', title: 'Mã HTBH', width:80, align: 'left',sortable:false,resizable:false ,  
	                editor:{  
	                    type:'combobox',
	                    options:{
	                    	data: FocusProgram._lstSaleTypeData.get(0).rows,
	            	        valueField:'saleTypeCode',  
	            	        textField:'saleTypeCode',
//	            	        method:'GET',
//	            	        class:'easyui-combobox',
//	            	        editable:false,
	            	        width :40,
	            	        
//	                        panelHeight :140,        
	                        filter: function(q, row){
	                    		var opts = $(this).combobox('options');
	                    		return row[opts.textField].indexOf(q.toUpperCase()) == 0;
	                    	}
	                    }
	                }
			    },
			    {field:'type', title: 'Loại MHTT', width: 150, align: 'left',fixed:true,sortable:false,resizable:false ,  
                    editor:{  
                        type:'combobox',
                        options:{
                        	data: FocusProgram._lstTypeData.get(0).rows,  
	            	        valueField:'apParamCode',  
	            	        textField:'apParamCode',
//	            	        method:'GET',
//	            	        class:'easyui-combobox',
	            	        width:150,
	            	        panelWidth:150,
	            	        editable:false,
//	                        panelHeight :140, 
	                        filter: function(q, row){
	                    		var opts = $(this).combobox('options');
	                    		return row[opts.textField].indexOf(q.toUpperCase()) == 0;
	                    	}
	                    }
                    }
			    },
			    {field:'edit', title: '', width: 50, align: 'center',sortable:false,resizable:false, formatter: function(value,row,index){
			    	if (row.editing){
		                var s = "<a title='Lưu' href='javascript:void(0)' onclick='return FocusProgram.saveRow(this)'><span style='cursor:pointer'><img width='15' height='15' src='/resources/images/icon-datach.png'/></span></a> ";  
		                var c = "<a title='Quay lại' href='javascript:void(0)' onclick='FocusProgram.cancelRow(this)'><span style='cursor:pointer'><img width='15' height='15' src='/resources/images/icon_esc.png'/></span></a>";  
		                return s + c;  
		            } else {
		            	var e = '';
		            	if($('#statusHidden').val().trim() == activeType.WAITING){
		            		e = "<a title='Sửa' href='javascript:void(0)' onclick='FocusProgram.editRow(this);'><span style='cursor:pointer'><img width='15' height='15' src='/resources/images/icon-edit.png'/></span></a>";  
		            	}
		                return e;  
		            }  
			    }},
			    {field:'delete', title: title, width: 50, align: 'center',sortable:false,resizable:false,
			    	formatter:function(index,row){
			    		if($('#statusHidden').val().trim() == activeType.WAITING){
			    			return "<a title='Xóa' href='javascript:void(0)' onclick=\"return FocusProgram.deleteRow('" + row.id +"')\"><img width='15' height='15' src='/resources/images/icon-delete.png'/></a>";
			    		}else{
			    			return '';
			    		}
			    		
			    	} 
			    },
			  ]],	  
			  pageList  : [10,20,30],
//			  width: 910,
			  height:'auto',
			  scrollbarSize : 0,
			  pagination:true,
			  checkOnSelect :true,
			  fitColumns:true,
			  method : 'GET',
			  rownumbers: true,	  
			  singleSelect:true,
			  pageNumber:1,
			  width: ($('#productFocusGrid').width()),
			  onLoadSuccess:function(){
				  setTimeout(function(){
					  $('#successMsgProduct').html('').hide();
				  },3000);
				  //var angelkid;
				  $('.datagrid-header-rownumber').html('STT');
		      	  $('#grid').datagrid('resize');
		      	  edit = false;
		      },
		  	onBeforeEdit:function(index,row){
		  		//$()
		        row.editing = true;
		        edit =true;
		        idx = index;  
		        FocusProgram.updateActions(index);  
		    },  
		    onAfterEdit:function(index,row){  
		        row.editing = false;
		        edit = false;
		        idx = -1;
		        $('#saleTypeCode').val(row.createUser);
		        $('#type').val(row.type);
		        FocusProgram.updateActions(index);  
		    },  
		    onCancelEdit:function(index,row){  
		        row.editing = false;  
		        edit = false;
		        idx = -1;
		        FocusProgram.updateActions(index);  
		    }  
		});
	},
	getJsonListSaleType:function(){
		$.getJSON('/rest/catalog/focus-program/product/sale-type-code.json?id=' + $('#focusProgramId').val(),function(data){
			if(data != null && data != undefined){
				FocusProgram._lstSaleTypeData.put(0, data);
			}
		});
	},
	getJsonListType:function(){
		$.getJSON('/rest/catalog/focus-program/product/type.json',function(data){
			if(data != null && data != undefined){
				FocusProgram._lstTypeData.put(0, data);
			}
		});
	},
	editRow:function(target){
		$('.ErrorMsgStyle').hide();
		var indexRow = FocusProgram.getRowIndex(target);
	    $('#grid').datagrid('beginEdit', indexRow);
	    var width = 75;
	    $('td[field=saleTypeCode] .datagrid-editable-input').combobox('resize', width);
	    $('td[field=type] .datagrid-editable-input').combobox('resize', width);
	},
	updateActions:function(index){
	    $('#grid').datagrid('updateRow',{
	        index: index,  
	        row:{}  
	    });  
	}, 
	getRowIndex:function(target){  
	    var tr = $(target).closest('tr.datagrid-row');  
	    return parseInt(tr.attr('datagrid-row-index'));  
	},  
	saveRow:function(target){
		$('.ErrorMsgStyle').hide();
		var indexRow = FocusProgram.getRowIndex(target);
		$('#errMsg').html('').hide();
	    $('#grid').datagrid('endEdit', indexRow);
	    var saleTypeCode = $('#grid').datagrid('getData').rows[indexRow].createUser;;
	    var focusProductType = $('#grid').datagrid('getData').rows[indexRow].type;
	    var msg = '';
	    $('#errMsgProduct').html('').hide();
	    if(saleTypeCode == null || saleTypeCode.length == 0 ){
	    	msg = 'Vui lòng chọn mã HTBH';
	    	$('#grid').datagrid('beginEdit', indexRow);
	    	$('td[field=createUser] .combo-text').focus();
//	    	$('tr[datagrid-row-index = indexRow] td[field=createUser] .combo-text').focus();
	    }
	    if(msg.length == 0){
	    	if(focusProductType == null || focusProductType.length == 0 ){
		    	msg = 'Vui lòng chọn loại MHTT';
		    	 $('#grid').datagrid('beginEdit', indexRow);
		    	$('td[field=type] .combo-text').focus();
//		    	$('tr[datagrid-row-index = indexRow] td[field=type] .combo-text').focus();
		    }
	    }
	    if(msg.length > 0){
	    	$('#errMsgProduct').html(msg).show();
	    	return false;
	    }
	    var dataModel = new Object();		
		dataModel.id = $('#selId').val();
		dataModel.focusMapProductId = $('#grid').datagrid('getData').rows[indexRow].id;
		dataModel.staffTypeCode = saleTypeCode;
		dataModel.focusProductType = focusProductType;
		dataModel.isUpdate = true;
		Utils.addOrSaveRowOnGrid(dataModel, '/focus-program/product/save', ProgrammeDisplayCatalog._xhrSave, null,null, function(data){
			$('#successMsgProduct').html('Lưu dữ liệu thành công').show();
			$("#grid").datagrid("reload");
		});
		return false;
	},
	cancelRow:function (target){
		$('.ErrorMsgStyle').hide();
		var indexRow = FocusProgram.getRowIndex(target);
	    $('#grid').datagrid('cancelEdit', indexRow);  
	},
	deleteRow: function(productId){
		$('.ErrorMsgStyle').hide();
		$('#errMsg').html('').hide();
		var dataModel = new Object();
		dataModel.productId = productId;	
		Utils.deleteSelectRowOnGrid(dataModel, 'SP của CTTT', "/focus-program/delete-product", FocusProgram._xhrDel, null, 'errMsgProduct',function(){
			$('#successMsgProduct').html('Xóa dữ liệu thành công').show();
		});
		return false;		
	},
	searchProductTab: function(){
		$('.ErrorMsgStyle').hide();
		$('#errMsg').html('').hide();
		var code = $('#productCode').val().trim();
		var name = $('#productName').val().trim();
		$('#code').focus();
		$("#grid").datagrid({
			url:FocusProgram.getProductGridUrl(code,name),
			pageNumber:1
		});
		return false;
	},
	openEasyUIDialog : function() {
		$('.ErrorMsgStyle').hide();
		$('#divDialogSearch').show();
		var html = $('#productEasyUIDialog').html();
		$('#productEasyUIDialog').dialog({  
//	        title: 'Tìm kiếm sản phẩm' +'<img id="loadingDialog" src="/resources/images/dialog/fancybox_loading.gif" style="display:none;">',  
	        closed: false,  
	        cache: false,  
	        modal: true,
	        position: 'middle',
	        center: true, 
	        onOpen: function(){
	        	FocusProgram._mapLstProduct = new Map();
	        	setTimeout(function(){
	    			CommonSearchEasyUI.fitEasyDialog();
	    		},500);
	        	Utils.bindAutoSearch();
	        	$('#productCodeDlg').focus();
	        	$('#saleTypeCodeDlg').addClass('MySelectBoxClass');
	        	$('#saleTypeCodeDlg').customStyle();
	        	$('#typeDlg').addClass('MySelectBoxClass');
	        	$('#typeDlg').customStyle();
	        	FocusProgram.searchProductTree();
	        },
	        onBeforeClose: function() {
	        },
	        onClose : function(){
	        	$('#divDialogSearch').hide();
	        	$('#productEasyUIDialog').html(html);
	        }
	    });
		return false;
	},
	searchProductTree:function(){
		$('.ErrorMsgStyle').hide();
		$('#loadingDialog').show();
		var code = $("#productCodeDlg").val().trim();
		var name = 	$("#productNameDlg").val().trim();
		var focusProgramId = $('#selId').val();
		var url = '/rest/catalog/group-po/tree/2.json' ;
		$("#tree").tree({
			url: url,  
			method:'GET',
            animate: true,
            checkbox:true,
            onBeforeLoad:function(node,param){
            	$("#tree").css({"visibility":"hidden"});
            	$("#loadding").css({"visibility":"visible"});
            	$("#errMsgDlg").html('');
            	param.code = code;
            	param.name = name;
            	param.id = focusProgramId;
            },
            onCheck:function(node,data){
            	if(node.attributes.productTreeVO.type == "PRODUCT"){
            		if(data){
            			FocusProgram._mapLstProduct.put(node.attributes.productTreeVO.id,node.attributes.productTreeVO.id);
            		}else{
            			FocusProgram._mapLstProduct.remove(node.attributes.productTreeVO.id);
            		}
            	}
            	if(node.attributes.productTreeVO.type == "SUB_CATEGORY"){
            		var sub_category = node.attributes.productTreeVO.listChildren;
            		for(var i = 0;i<sub_category.length;i++){      			
		            		if(data){
		            			FocusProgram._mapLstProduct.put(sub_category[i].id,sub_category[i].id);
		            		}else{
		            			FocusProgram._mapLstProduct.remove(sub_category[i].id);
		            		}
            		}
            	}
            	if(node.attributes.productTreeVO.type == "CATEGORY"){
            		var category = node.attributes.productTreeVO.listChildren;
            		for(var i = 0;i<category.length;i++){
	        			var sub_category = category[i].listChildren;
	        			for(var j = 0;i<sub_category.length;i++){
		            		if(data){
		            			FocusProgram._mapLstProduct.put(sub_category[i].id,sub_category[i].id);
		            		}else{
		            			FocusProgram._mapLstProduct.remove(sub_category[i].id);
		            		}
	        			}
            		}
            	}
            },
            onLoadSuccess:function(node,data){
            	$("#tree").css({"visibility":"visible"});
            	var arr  = FocusProgram._mapLstProduct.keyArray;
            	if(arr.length > 0){
            		for(var i = 0;i < arr.length; i++){
            			var nodes = $('#tree').tree('find', arr[i]);
            			if(nodes != null){
            				$("#tree").tree('check',nodes.target);
            			}
            		}
            	}
            	$("#loadding").css({"visibility":"hidden"});
            	$('#loadingDialog').hide();
            	if(data.length == 0){
            		$("#errMsgDlg").html("Không có kết quả");
            	}
            }
		});
	},
	changeProductTree:function(){
		$('.ErrorMsgStyle').hide();
		$("#errMsgDlg").html("").hide();
		var dataModel = new Object();
		var nodes = $('#tree').tree('getChecked');
		var lstId = [];  
		var arr = $('#tree').children();
		var moreCat = 0;
		arr.each(function(){
			if(!$(this).children().first().children().last().prev().hasClass('tree-checkbox0')){
				moreCat++;
			}
		});
        for(var i=0; i<nodes.length; i++){
        	if(nodes[i].attributes.productTreeVO.type != "PRODUCT"){
        		continue;
        	}
            lstId.push(nodes[i].id.split('_')[0]);  
        }
        if(FocusProgram._mapLstProduct.keyArray.length > 0){
        	for(var j = 0; j < FocusProgram._mapLstProduct.keyArray.length ; j++){
        		if(lstId.indexOf(FocusProgram._mapLstProduct.keyArray[j]) > -1){
        			continue;
        		}else{
        			lstId.push(FocusProgram._mapLstProduct.keyArray[j]);
        		}
        	}
        }
        if(lstId == '' || lstId.length == 0){
        	$("#errMsgDlg").html("Vui lòng chọn sản phẩm").show();
        	return false;
        }
        var saleTypeCode = $('#saleTypeCodeDlg').val().trim();
        var type = $('#typeDlg').val().trim();
        if(saleTypeCode == -1){
        	$("#errMsgDlg").html("Vui lòng chọn loại HTBH").show();
        	return false;
        }
        if(type == -1){
        	$("#errMsgDlg").html("Vui lòng chọn loại MHTT").show();
        	return false;
        }
        dataModel.id = $('#selId').val().trim();
        dataModel.lstProductId = lstId;
		dataModel.channelTypeId = saleTypeCode;
		dataModel.type = type;
		if(moreCat > 1){
			$.messager.confirm('Xác nhận', 'Bạn đang chọn các sản phẩm hơn 1 ngành hàng. Bạn có muốn lưu thông tin không?', function(r){
				if (r){
					Utils.saveData(dataModel, '/focus-program/product/save', FocusProgram._xhrSave, 'errMsgDlg', function(data){
						$('#successMsgProduct').html('Lưu dữ liệu thành công').show();
						$("#productEasyUIDialog").dialog("close");
						if(data.error == false){
							$("#grid").datagrid("reload");
						}
					});		
				}
			});
		}else{
			Utils.addOrSaveData(dataModel, '/focus-program/product/save', FocusProgram._xhrSave, 'errMsgDlg', function(data){
				$('#successMsgProduct').html('Lưu dữ liệu thành công').show();
				$("#productEasyUIDialog").dialog("close");
				if(data.error == false){
					$("#grid").datagrid("reload");
				}
			});
		}
		return false;
	},
//	SAN PHAM CHUONG TRINH TRONG TAM]]]]
	getShopGridUrl: function(focusId,shopCode,shopName){
		return "/focus-program/searchshop?id=" + encodeChar(focusId) + "&shopCode=" + encodeChar(shopCode) + "&shopName=" + encodeChar(shopName);
	},
	openShopDialog:function(id){
		$('.ErrorMsgStyle').hide();
		var focusId = $('#progId').val();
		var arrParam = new Array();
 		var param = new Object();
 		param.name = 'shopId';
 		if(id != null && id != undefined){
 	 		param.value = id;
 		}
 		var param1 = new Object();
 		param1.name = 'focusId';
 		param1.value = focusId;
 		arrParam.push(param);
 		arrParam.push(param1);
 		var param2 = new Object();
 		param2.name = 'stringProgram';
 		param2.value = 'focus';
 		arrParam.push(param2);
 		CommonSearch.searchShopFocusProgram(function(data){
 				//$('#promotionProgramCode').val(data.code);
 		},arrParam);
 		PromotionCatalog.bindFormat();
	},
	onCheckShop:function(id){
		 var isCheck = $('.easyui-dialog #check_' +id).attr('checked');
		    if($('.easyui-dialog #check_' +id).is(':checked')){
		    	FocusProgram._listShopId.push(id);
		    	var classParent = $('.easyui-dialog #check_' +id).attr('class');
				var flag = true;
				var count = 0;
				$('.' +classParent).each(function(){
					if($(this).is(':checked')){
						flag = false;
						count++;
					}
				});
				if(flag || count == 1){
					CommonSearch.recursionFindParentShopCheck(id);
				}
		    	//CommonSearch.recursionFindParentShopCheck(id);
			}else{
				PromotionCatalog.removeArrayByValue(FocusProgram._listShopId,id);
				var classParent = $('.easyui-dialog #check_' +id).attr('class');
				var flag = true;
				var count = 0;
				$('.' +classParent).each(function(){
					if($(this).is(':checked')){
						flag = false;
					}
				});
				if(flag || count == 1){
					CommonSearch.recursionFindParentShopUnCheck(id);
				}
				//CommonSearch.recursionFindParentShopUnCheck(id);
			}
	},
	createFocusShopMap:function(){
		$('.ErrorMsgStyle').hide();
		var params = new Object();
		params.listShopId = FocusProgram._listShopId;
		params.id = $('#progId').val();
		if(FocusProgram._listShopId.length<=0){
			$('.easyui-dialog #errMsgDialog').html('Bạn chưa chọn đơn vị để thêm').show();
			return false;
		}
		Utils.addOrSaveData(params, '/focus-program/createfocusprogram', null, 'errMsgDialog', function(data){
			if(data.error == undefined && data.errMsg == undefined){
				FocusProgram.searchShop();
				FocusProgram._listShopId = new Array();
				$('#searchStyleShopFocus1').dialog('close');
				$('#errExcelMsg').html("").hide();
				$('#successMsg2').html('Lưu dữ liệu thành công.').show();
				var tm = setTimeout(function(){
					$('#successMsg2').html('').hide();
					clearTimeout(tm); 
				}, 3000);
			} 
		}, null, null, null, "Bạn có muốn thêm đơn vị này ?", null);
	},
	deleteFocusShopMap:function(shopId){
		$('.ErrorMsgStyle').hide();
		var params = new Object();
		params.shopId = shopId;
		params.id = $('#progId').val();
		Utils.addOrSaveData(params, '/focus-program/deletefocusprogram', null, 'errExcelMsg', function(data){
			//PromotionCatalog.loadShop();
			FocusProgram.searchShop();
			$('#errExcelMsg').html("").hide();
			$('#successMsg2').html('Xóa dữ liệu thành công.').show();
			var tm = setTimeout(function(){
				$('#successMsg2').html('').hide();
				clearTimeout(tm); 
			}, 3000);
		}, null, null, null, "Bạn có muốn xóa đơn vị này ?", null);
	},
	exportExcelShopMap:function(){
		$('#divOverlay').show();
		$('#errExcelMsg').html('').hide();
	    var focusId = $('#progId').val();
		var shopCode = $('#shopCode').val().trim();
		var shopName = $('#shopName').val().trim();
		
		var data = new Object();
		data.id = focusId;
		data.shopCode = shopCode;
		data.shopName = shopName;
		
		var url = "/focus-program/exportFocusShopMap";
		CommonSearch.exportExcelData(data,url);
	},
	/*****************************	HTBH -THONGNM ****************************************/
	_mapHTBH:new Map(),
	loadHTBH : function() {
		var focusProgramId = $('#focusProgramId').val();	
		var params = new Object();
		params.focusProgramId =focusProgramId;
		if($('#permissionUser').val().trim() == 'true' && $('#statusHidden').val().trim() == activeType.WAITING) {
			FocusProgram._mapHTBH = new Map();
//			Utils.bindAutoSearch();
			$("#gridHTBH").datagrid({
				url:'/focus-program/search-htbh',
				queryParams:params,
//		        pagination : true,
		        rownumbers : true,
		        pageNumber : 1,
		        scrollbarSize: 0,
		        autoWidth: true,
		        autoRowHeight : true,
		        fitColumns : true,
				width : ($('#gridContainerHTBH').width()),
				height:'auto',
				columns:[[
				    {field: 'code', title: 'Mã HTBH', width: 150, sortable:false,resizable:false, align: 'left', formatter:function(value,row,index){
			        	  return Utils.XSSEncode(value);
			         }},
				    {field: 'name', title: 'Tên HTBH', width: 300, sortable:false,resizable:false, align: 'left', formatter:function(value,row,index){
			        	  return Utils.XSSEncode(value);
			         }},
				    {field: 'id',checkbox:true, width: 50, align: 'center',sortable:false,resizable:false},
				    {field: 'isExist',hidden:true,resizable:false,formatter:function(value,rowData,rowIndex){
				    	if(rowData.isExist == 1){ 
				    		FocusProgram._mapHTBH.put(rowData.id, rowData);
				    		if (FocusProgram._lstHTBHdb.indexOf(rowData.id) < 0) {
				    			FocusProgram._lstHTBHdb.push(rowData.id);
				    		}
				    	}
				    }}
				]],
				onCheck : function(rowIndex, rowData) {
					$('.ErrorMsgStyle').hide();
					var selectedId = rowData['id'];
					FocusProgram._mapHTBH.put(selectedId,rowData);
				},
				onUncheck : function(rowIndex, rowData) {
					$('.ErrorMsgStyle').hide();
					var selectedId = rowData['id'];
					FocusProgram._mapHTBH.remove(selectedId);
				},
				onCheckAll : function(rows) {
					$('.ErrorMsgStyle').hide();
					if($.isArray(rows)) {
						for(var i = 0; i < rows.length; i++) {
							var row = rows[i];
							var selectedId = row['id'];
							FocusProgram._mapHTBH.put(selectedId, row);
				    	}
				    }
				},
				onUncheckAll : function(rows) {
					$('.ErrorMsgStyle').hide();
					if($.isArray(rows)) {
						for(var i = 0; i < rows.length; i++) {
							var row = rows[i];
							var selectedId = row['id'];
							FocusProgram._mapHTBH.remove(selectedId);
				    	}
				    }
				},
				onLoadSuccess:function(data){
					$('#gridContainerHTBH .datagrid-header-rownumber').html('STT');
			    	updateRownumWidthForDataGrid('#gridContainerHTBH');
//					  bindAutoHideErrMsg();
			    	setTimeout(function(){
			    		$('#successMsgHTBH').html('').hide();
			    	},3000);
					$('#gridHTBH').datagrid('resize');
					var easyDiv = '#gridContainerHTBH ';
					var runner = 0;
					var isAll = 0;
					$(easyDiv+'input:checkbox[name=id]').each(function() {
						var selectedId = this.value;
						if(FocusProgram._mapHTBH.keyArray.indexOf(parseInt(selectedId)) > -1) {
							isAll++;
							$('#gridHTBH').datagrid('selectRow',runner);
						}
						runner ++;
					});
					/*var rows = data.rows;
					for (runner = 0, sz = rows.length; runner < sz; runner++) {
						if (rows[runner].isExist) {
							isAll++;
							$('#gridHTBH').datagrid('selectRow',runner);
						}
					}*/
					if (isAll > 0 && isAll == runner){
						$(easyDiv+'td[field=id] .datagrid-header-check input:checkbox').attr('checked','checked');
					}else{
						$(easyDiv+'td[field=id] .datagrid-header-check input:checkbox').removeAttr('checked');
					}
				}
			});
		} else {
			$("#gridHTBH").datagrid({
				url:'/focus-program/search-htbh',
				queryParams:params,
		        pagination : true,
		        rownumbers : true,
		        pageNumber : 1,
		        scrollbarSize: 0,
		        autoWidth: true,
		        autoRowHeight : true,
		        fitColumns : true,
				width : ($('#gridContainerHTBH').width()),
				height:'auto',
				columns:[[
				    {field: 'code', title: 'Mã HTBH', width: 150, sortable:false,resizable:false, align: 'left', formatter:function(value,row,index){
			        	  return Utils.XSSEncode(value);
			         }},
				    {field: 'name', title: 'Tên HTBH', width: 300, sortable:false,resizable:false, align: 'left', formatter:function(value,row,index){
			        	  return Utils.XSSEncode(value);
			         }}
				]],
				onLoadSuccess:function(){
					$('#gridContainerHTBH .datagrid-header-rownumber').html('STT');
			    	updateRownumWidthForDataGrid('#gridContainerHTBH');
					$('#gridHTBH').datagrid('resize');
				}
			});
		}
		return false;
	},
	searchHTBH : function () {
		$('.ErrorMsgStyle').hide();
		var params=new Object();
		params.focusProgramId = $('#focusProgramId').val().trim();
		params.code = $('#codeHTBH').val().trim();
		params.name = $('#nameHTBH').val().trim();
		$("#gridHTBH").datagrid('load',params);
		return false;
	},
	saveHTBH: function(){
		//var msg = '';
		$('.ErrorMsgStyle').hide();
		var focusProgramId = $('#focusProgramId').val();	//focusProgramId
		var mapKey = FocusProgram._mapHTBH.keyArray;
		var dataModel = new Object();	
		dataModel.focusProgramId = focusProgramId ;
		dataModel.lstApParamId =  mapKey;
		Utils.addOrSaveData(dataModel, "/focus-program/save-htbh", null, 'errMsg',function(data){
			$('#successMsgHTBH').html('Lưu dữ liệu thành công').show();
			$.getJSON('/rest/catalog/bary-centric/sale-type/list.json?id=' + $('#focusProgramId').val(), function(data){
				var arrHtml = new Array();
				arrHtml.push('<option value="-1">--- Chọn HTBH  ---</option>');
				for(var i=0;i<data.length;i++){
					arrHtml.push('<option value="' + data[i].id +'">' + data[i].saleTypeCode +'</option>');
				}
				$('#saleTypeCodeDlg').html('');
				$('#saleTypeCodeDlg').html(arrHtml.join(""));
				$('#saleTypeCodeDlg').change();
			});
			FocusProgram.getJsonListSaleType();
			$("#gridHTBH").datagrid("reload");
			FocusProgram._lstHTBHdb = [];
		});
		return false;
	}
};
/*
 * END OF FILE - /web/web/resources/scripts/business/program/vnm.focus-program.js
 */

/*
 * START OF FILE - /web/web/resources/scripts/business/program/vnm.programme-display-catalog.js
 */
var ProgrammeDisplayCatalog = {
	_xhrSave : null,
	_xhrDel: null,
	_curGridId: null,
	_curRowId: null,
	_arrSelItems: null,
	_mapCheckSubCat:null,	
	displayProductMap : new Map(),
	displayStaffMap : new Map(),
	_mapCustomer:null,
	_arrListStaffItem:null,
	_mapStaff:null,
	_mapProduct:new Map(),
	_mapShop:null,
	_mapExclusion:null,
	groupIdDS:null,
	groupTypeDS:null,
	_listExclusionDialogSize:null,
	_listExclusion:null,
	_listShopId:null,
	_listShopIdEx:null,
	_lstSize:null,
	_lstProduct:null,
	_lstMapProductsDP: new Map(),
	_params:null,
	_lstCustomerId:null,
	staffRole : null,
	AMOUNT:1,
	SKU:3,
	QUANTITY:2, 
	DISPLAY:4,
	_lstLevelCode: new Map(),
	_lstProductExpand:[],
	checkFromDate:true,
	checkToDate:true,
	isStopped:false,
	fromDate:new Map(),
	toDate:new Map(),
	_listShopId:null,
	_gridDetaiSpCblId:null,
	lstConvfact: null,
	lstProductId: null,
	lstIndex: null,
	searchActionLog: function(){
		$('.ErrorMsgStyle').hide();
		var id = $('#selId').val().trim();
		var fromDate = $('#fromDate').val().trim(); 
		if(fromDate=='__/__/____'){
			fromDate = '';
		}
		var toDate = $('#toDate').val().trim();
		if(toDate=='__/__/____'){
			toDate = '';
		}
		var url = '/catalog/programme-display/search-info-change?id=' + id + '&fromDate=' + fromDate + '&toDate=' + toDate;
		$("#infoChangeGrid").setGridParam({url:url,page:1}).trigger("reloadGrid");
	},	
	showDlgImportExcel : function() {		
		$('#easyuiPopup').show();
		$('.ErrorMsgStyle').hide();
		$('#dateStrExcel').val(getCurrentMonth());
		$('#fakefilepc').val('');
		$('#easyuiPopup').dialog({  
	        closed: false,  
	        cache: false,  
	        modal: true,
	        onOpen: function(){	 
	        	$('.easyui-dialog .BtnGeneralStyle').css('color','');
	        	$('.downloadTemplateNVBH').attr('href',excel_template_path+'/display-program/Bieu_mau_import_so_suat_nvbh_cttb.xls');
	        },
	        onClose: function(){
	        	var month = $('#month').val().trim();
				ProgrammeDisplayCatalog._mapStaff = new Map();
				var params = new Object();
				params.id = $('#displayProgram').val().trim();
				params.strShopIds = $("#shop").data("kendoMultiSelect").value().toString();
				params.month = $('#month').val().trim();
				$("#staffGrid").datagrid('reload',params);
				$("#staffGrid").datagrid('uncheckAll');
				var StaffRole = $('#staffRole').val();
				if(StaffRole!=StaffRoleType.NVGS && StaffRole!=StaffRoleType.NHVNM){
					if(!Utils.compareCurrentMonthEx(month)){
						$('#checkDeleteAll1').attr('disabled',true);
						$('#checkDeleteAll1').attr('checked',false);
					}else{
						$('#checkDeleteAll1').attr('disabled',false);
					}
				}
	        }
		});
	},
	
	openCopyFocusProgram:function(){
		$('.ErrorMsgStyle').hide();
		$('#searchStyle1EasyUIDialogDivEx').css("visibility", "visible");
		var html = $('#searchStyle1EasyUIDialogDivEx').html();
		$('#searchStyle1EasyUIDialogEx').dialog({  
	        title: 'Sao chép CTTB',  
	        closed: false,  
	        cache: false,  
	        modal: true,
	        width : 616,
	        height :'auto',
	        onOpen: function(){	    
	        	$('.easyui-dialog #__btnSaveDisplayProgram').bind('click',function(event){
					var code = $('.easyui-dialog #seachStyle1Code').val().trim();
					var name = $('.easyui-dialog #seachStyle1Name').val().trim();
					if(code == "" || code == undefined || code == null){
						msg = "Bạn chưa nhập giá trị trường Mã CTTB.";
						$('.easyui-dialog #errMsgSearch').html(msg).show();
						$('.easyui-dialog #seachStyle1Code').focus();
						return false;
					}
					var msg = Utils.getMessageOfSpecialCharactersValidateEx1('.easyui-dialog #seachStyle1Code', 'Mã CTTB',Utils._CODE);
					if(msg.length >0){
						$('.easyui-dialog #errMsgSearch').html(msg).show();
						$('.easyui-dialog #seachStyle1Code').focus();
						return false;
					}
					if(name == "" || name == undefined || name == null){
						$('.easyui-dialog #errMsgSearch').html("Bạn chưa nhập giá trị trường Tên CTTB.").show();
						$('.easyui-dialog #seachStyle1Name').focus();
						return false;
					}
					msg = Utils.getMessageOfSpecialCharactersValidateEx1('.easyui-dialog #seachStyle1Name', 'Tên CTTB');
					if(msg.length >0){
						$('.easyui-dialog #errMsgSearch').html(msg).show();
						$('.easyui-dialog #seachStyle1Name').focus();
						return false;
					}
					var id = $('#selId').val();
					var params = new Object();
					params.id = id;
					params.code = code;
					params.name = name;
					Utils.addOrSaveData(params, '/programme-display/copydisplayprogram', null, 'errMsgSearch', function(data){						
						$('.easyui-dialog #successMsgInfo').html("Sao chép dữ liệu thành công.").show();
						var tm = setTimeout(function(){
							$('#searchStyle1EasyUIDialogEx').dialog('close');
							window.location.href = '/programme-display/change?id='+encodeChar(data.displayProgramId);
	                		clearTimeOut(tm);	                		
	                	}, 1500);
						
					}, null, null, null, "Bạn có muốn sao chép CTTB này ?", null);
				});
	        }
		});
	},
	viewDetailActionLog: function(id){
		$('.ErrorMsgStyle').hide();
		$('#detailActionLogDiv').show();
		var url = ProgrammeDisplayCatalog.getGridUrlDetailAction(id);		
		if($('#gbox_infoChangeDetailGrid').html() != null && $('#gbox_infoChangeDetailGrid').html().trim().length > 0){
			$("#infoChangeDetailGrid").setGridParam({url:url,page:1}).trigger("reloadGrid");
		} else {
			$("#infoChangeDetailGrid").jqGrid({
				  url:ProgrammeDisplayCatalog.getGridUrlDetailAction(id),
				  colModel:[
				    {name:'columnName',index : 'columnName', label: 'Thông tin thay đổi', sortable:false,resizable:false, align:'left' },
				    {name:'oldValue',index : 'oldValue', label: 'Giá trị cũ', width: 100, sortable:false,resizable:false, align:'right'},
				    {name:'newValue',index : 'newValue', label: 'Giá trị mới', width: 100, sortable:false,resizable:false, align:'right'},
				    {name:'actionDate',index : 'actionDate', label: 'Ngày thay đổi', width: 80,align:'center', sortable:false,resizable:false, formatter:'date', formatoptions: {srcformat:'Y-m-d', newformat:'d/m/Y'} }			    
				  ],	  
				  pager : '#infoChangeDetailPager',			  	  	  
				  width: ($('#infoChangeDetailContainer').width()),
				  gridComplete: function(){
					  $('#jqgh_infoChangeDetailGrid_rn').html('STT');
					  updateRownumWidthForJqGrid();
				  }
				})
				.navGrid('#infoChangeDetailPager', {edit:false,add:false,del:false, search: false});
		}				
	},
	getGridUrlDetailAction: function(id){
		return '/catalog/programme-display/search-info-change-detail?id=' + id;
	},
	showDisplayProgramChangeTab: function(){
		$('.ErrorMsgStyle').hide();
		ProgrammeDisplayCatalog.hideAllTab();
		$('#tabTitle').html('Xem thông tin thay đổi');
		$('#tabActive6').addClass('Active');
		$('#container6').show();	
		$('#changedInfoDisplayProgramCode').val($('#code').val());
		$('#changedInfoDisplayProgramName').val($('#name').val());
		var id = $('#selId').val();
		var fromDate = $('#fromDate').val();
		var toDate = $('#toDate').val();
		Utils.bindQuicklyAutoSearch();
		var url = '/catalog/programme-display/search-info-change?id=' + id + '&fromDate=' + fromDate + '&toDate=' + toDate;
		$("#infoChangeGrid").jqGrid({
			  url:url,
			  colModel:[
			    {name:'actionUser',index : 'actionUser', label: 'NV thay đổi', sortable:false,resizable:false, align:'left' },
			    {name:'actionType',index : 'actionType', label: 'Loại thay đổi', width: 100, sortable:false,resizable:false, align:'left', formatter: ProgrammeDisplayCatalogFormatter.actionTypeCellFormatter },
			    {name:'actionDate',index : 'actionDate', label: 'Ngày thay đổi', align:'center',width: 80, sortable:false,resizable:false, formatter:'date', formatoptions: {srcformat:'Y-m-d', newformat:'d/m/Y'} },
			    {name:'actionIp',index : 'actionIp', label: 'IP máy thay đổi', width: 100, sortable:false,resizable:false, align:'left' },
			    {name:'viewDetail', label: 'Xem thay đổi', width: 50, align: 'left',sortable:false,resizable:false, formatter: ProgrammeDisplayCatalogFormatter.detailInfoChangeCellFormatter},			    
			    {name:'id', index : 'id', hidden: true }			    
			  ],	  
			  pager : '#infoChangePager',			  	  	  
			  width: ($('#infoChangeContainer').width()),
			  gridComplete: function(){	
				  $('#jqgh_infoChangeGrid_rn').html('STT');
				  var url = ProgrammeDisplayCatalog.getGridUrlDetailAction(-1);
				  $("#infoChangeDetailGrid").setGridParam({url:url,page:1}).trigger("reloadGrid");
				  updateRownumWidthForJqGrid();			  
			  }
			}).navGrid('#infoChangePager', {edit:false,add:false,del:false, search: false});	
	},
	exportActionLog: function(){
		$('.ErrorMsgStyle').hide();
		var id = $('#selId').val();
		var fromDate = $('#fromDate').val().trim();
		var toDate = $('#toDate').val().trim();
		ExportActionLog.exportActionLog(id, ExportActionLog.DISPLAY_PROGRAM, fromDate, toDate,'errMsgActionLog');
	},
	
	getGridUrl: function(code,name,status,fDate,tDate){		
		return "/programme-display/search?code=" + encodeChar(code) + "&name=" + encodeChar(name) + "&status=" + status + "&fromDate=" + encodeChar(fDate) + "&toDate=" + encodeChar(tDate);
	},	
	search: function(){
		$('.ErrorMsgStyle').hide();
		var code = $('#code').val().trim();
		var name = $('#name').val().trim();
		var status = $('#status').val().trim();;
		if($('#permissionUser').val() == 'true' ){
			status = $('#status').val().trim();
		}
		var fDate = $('#fDate').val();
		var tDate = $('#tDate').val();

		var msg=''; 
		var startDate = $('#fDate').val().trim();
		var endDate = $('#tDate').val().trim();
		if (startDate != '' && !Utils.isDate(startDate)) {
			msg = 'Từ ngày không hợp lệ, vui lòng nhập theo định dạng dd/MM/yyyy';
			$('#fDate').focus();
		}
		if(msg.length == 0){
			if(endDate != '' && !Utils.isDate(endDate)){
				msg = 'Đến ngày không hợp lệ, vui lòng nhập theo định dạng dd/MM/yyyy';
				$('#tDate').focus();
			}
		}
		if(msg.length == 0){
			if(startDate != '' && endDate != ''){
				if(!Utils.compareDate(startDate, endDate)){
					msg = 'Giá trị Từ ngày phải nhỏ hơn hoặc bằng giá trị Đến ngày.';				
				}
			}
		}
		if(msg.length > 0){
			$('#errMsg').html(msg).show();
			return false;
		}
		var params = new Object();
		params.code = $('#code').val().trim();
		params.name = $('#name').val().trim();
		params.fDate = $('#fDate').val();
		params.tDate = $('#tDate').val();	
		params.status = $('#status').val();
		var url = ProgrammeDisplayCatalog.getGridUrl(code,name,status,fDate,tDate);
		$("#listCTTBGrid").datagrid('load',{code:code,name:name,status:status,fromDate:fDate,toDate:tDate});
		var tm = setTimeout(function() {
			$('#code').focus();
		}, 500);
		return false;
	},
	saveInfo: function(){
		var msg = '';
		var err = '';
		$('.ErrorMsgStyle').hide();
		var fDate = $('#fDate').val();
		var tDate = $('#tDate').val();
		msg = Utils.getMessageOfRequireCheck('code','Mã CTTB');
		err = '#code';
		if(msg.length == 0){
			msg = Utils.getMessageOfSpecialCharactersValidate('code','Mã CTTB',Utils._CODE);
			err = '#code';
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfRequireCheck('name','Tên CTTB');
			err = '#name';
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfSpecialCharactersValidate('name','Tên CTTB');
			err = '#name';
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfRequireCheck('status','Trạng thái',true);
		}

		//Kiểm tra rỗng
		if(msg.length == 0){
			msg = Utils.getMessageOfRequireCheck('fDate','Từ ngày');
			err = '#fDate';
		}
		if(msg.length == 0 && $('#fDate').val()!='' && !Utils.isDate($('#fDate').val(),'/')){
			msg = 'Từ ngày không tồn tại hoặc không đúng định dạng dd/mm/yyyy.';
			err = '#fDate';
		}
		if(msg.length == 0 && $('#fDate').val()!=null){
			msg = Utils.getMessageOfInvalidFormatDate('fDate', 'Từ ngày');
			err = '#fDate';
		}
		var status = $('#hidStatus').val();
		if(msg.length == 0 && status == 2){
				if(!Utils.compareDate(Utils.currentDate(), $('#fDate').val())){
					msg = 'Từ ngày phải lớn hơn ngày hiện tại';	
				}				
				err = '#fDate';
		}
		
		if(msg.length == 0 && $('#tDate').val()!='' && !Utils.isDate($('#tDate').val(),'/')){
			msg = 'Đến ngày không tồn tại hoặc không đúng định dạng dd/mm/yyyy.';
			err = '#tDate';
		}
		
		if(msg.length == 0 && $('#tDate').val()!=''){
			msg = Utils.getMessageOfInvalidFormatDate('tDate', 'Đến ngày');
			err = '#tDate';
		}
		
		if(msg.length == 0 && $('#startDate').val()!=null && $('#endDate').val()!=null){
			if(!Utils.compareDate($('#startDate').val().trim(), $('#endDate').val().trim())){
				msg = 'Từ ngày phải nhỏ hơn ngày đến ngày';
				err = '#startDate';
			}
		}
		
		if(msg.length == 0 && tDate.length > 0){
			if(!Utils.compareDate(fDate, tDate)){
				msg = msgErr_fromdate_greater_todate;	
				$('#fDate').focus();
			}			
		}
		if(msg.length == 0){
			if(!Utils.compareDate(Utils.currentDate(), $('#tDate').val())){
				msg = 'Đến ngày phải lớn hơn ngày hiện tại';	
			}				
			err = '#fDate';
		}
		if(msg.length > 0){
			$('#errMsg').html(msg).show();
			$(err).focus();
			setTimeout(function(){$('#errMsg').hide();},3500);
			return false;
		}
		

		/*var value = $('#status').val();
		var statusPermission = $('#statusPermission').val();*/
		var dataModel = new Object();		
		dataModel.id = $('#selId').val().trim();
		dataModel.code = $('#code').val().trim();
		dataModel.name = $('#name').val().trim();
		dataModel.status = $('#status').val();
		dataModel.fromDate = $('#fDate').val().trim();
		dataModel.toDate = $('#tDate').val().trim();		
		Utils.addOrSaveData(dataModel, "/programme-display/save-info", ProgrammeDisplayCatalog._xhrSave, null, function(data){
			$('#selId').val(data.id);
			showSuccessMsg('successMsgInfo',data);
			window.location.href = '/programme-display/change?id=' + data.id;						
		});		
		return false;
	},
	getSelectedProgram: function(id){
		$('#errExcelMsg').html('').hide();
		if(id!= undefined && id!= null){
			location.href = '/programme-display/change?id=' + id;
		} else {
			location.href = '/programme-display/change';
		}		
		return false;
	},
	hideAllTab: function(){
		$('#tab1').removeClass('Active');
		$('#tab2').removeClass('Active');
		$('#tab3').removeClass('Active');
		$('#tab4').removeClass('Active');
		$('#tab5').removeClass('Active');
		$('#tab8').removeClass('Active');
		$('#tab10').removeClass('Active');
		$('#amountTab').removeClass('Active');
		$('#container1').hide();
		$('#container2').hide();
		$('#container3').hide();
		$('#container4').hide();
		$('#container5').hide();
		$('#container6').hide();
		$('#container7').hide();
		$('#container8').hide();
		$('#container10').hide();
		$('#amountContainer').hide();
		$('.ErrorMsgStyle').hide();
	},
	showInfoTab: function(){
		ProgrammeDisplayCatalog.hideAllTab();
		$('#tabTitle').html('Thông tin chương trình trưng bày');
		$('#tabActive1').addClass('Active');
		$('#container1').show();
	},
	showQuotaGroupTab: function(){
		ProgrammeDisplayCatalog.hideAllTab();		
		$('#tabTitle').html('Nhóm chi tiêu');
		$('#tabActive2').addClass('Active');	
		$('#quotaPCode').val($('#code').val());
		$('#quotaPName').val($('#name').val());
		$('#container2').show();		
		$('#quotaCode').focus();
		Utils.bindAutoSearch();	
		var selId = $('#selId').val();
		$("#quotaGrid").jqGrid({
			  url:ProgrammeDisplayCatalog.getQuotaGridUrl(selId,'','','','','',$('#quotaStatus').val()),
			  colModel:[		
			    {name:'displayGroupCode', index : 'displayGroupCode', label: 'Mã nhóm', width: 100, sortable:false,resizable:true , align: 'left'},
			    {name:'displayGroupName', index : 'displayGroupName', label: 'Tên nhóm', sortable:false,resizable:true, align: 'left' },
			    {name:'percentMin',index : 'percentMin', label: '% Doanh số đóng góp', width: 105, sortable:false,resizable:false , align: 'right'},
			    {name:'percentMax',index : 'percentMax', label: '% Doanh số tối đa', width: 105, sortable:false,resizable:false , align: 'right'},
			    {name:'status',index : 'status', label: 'Trạng thái', width: 105, align: 'left', sortable:false,resizable:false, formatter: GridFormatterUtils.statusCellIconFormatter},
			    {name:'add',index: 'add', label: 'Thêm', width: 50, align: 'center',sortable:false,resizable:false, formatter: ProgrammeDisplayCatalogFormatter.addCellIconQuotaGroupFormatter},
			    {name:'edit', index:'edit',label: 'Sửa', width: 50, align: 'center',sortable:false,resizable:false, formatter: ProgrammeDisplayCatalogFormatter.editCellIconQuotaGroupFormatter},
			    {name:'delete', index:'delete',label: 'Xóa', width: 50, align: 'center', sortable:false,resizable:false, formatter: ProgrammeDisplayCatalogFormatter.delCellIconQuotaGroupFormatter},
			    {name:'id',index : 'id', hidden:true },
			    {name:'subCatField',index : 'subCatField', hidden:true },
			  ],	  
			  pager : '#quotaPager',			  	  	  
			  width: ($('#quotaGridContainer').width()),
			  subGrid: true,
			  subGridRowExpanded: function(subgrid_id, row_id) {
				  var subgrid_table_id;
				  subgrid_table_id = subgrid_id+"_t";				  
				  jQuery("#"+subgrid_id).html("<table id='"+subgrid_table_id+"' class='scroll'></table>");
				  var quotaGroupId = '0';
				  quotaGroupId = $("#quotaGrid").jqGrid ('getCell', row_id, 'id');				  
				  jQuery("#"+subgrid_table_id).jqGrid({
			          url:"/catalog/programme-display/search-sub-quota-group?quotaId="+quotaGroupId,
			          datatype: "json",			          
			          colModel: [						
						{name:'product.productName', index : 'name', label: 'Tên sản phẩm', width: 633,sortable:false,resizable:false, align: 'center' },									    
						{name:'product.status',index : 'status', label: 'Trạng thái', width: 119, align: 'left', sortable:false,resizable:false, formatter: GridFormatterUtils.statusCellIconFormatter},						
						{name:'delete', index:'delete',label: 'Xóa', width: 50, align: 'center', sortable:false,resizable:false, formatter: ProgrammeDisplayCatalogFormatter.delCellIconSubQuotaGroupFormatter},
						{name:'id',index : 'id', hidden:true }
			          ],
			          height: 'auto',	
			          shrinkToFit: false,
			          rowNum:100,
			          rownumbers: true,
			          gridComplete: function(){						  
						  $('#jqgh_'+ subgrid_table_id +'_rn').html('STT');
						  if(!checkPermissionEdit()){
							  hideColumnOfJqGrid(subgrid_table_id,'delete');
						  }
						  updateRownumWidthForJqGrid();
						  
					  }
			       });
			  },
			  gridComplete: function(){
				  $('#jqgh_quotaGrid_rn').html('STT');	
				  if(!checkPermissionEdit()){
					  hideColumnOfJqGrid('quotaGrid','add');
					  hideColumnOfJqGrid('quotaGrid','edit');
					  hideColumnOfJqGrid('quotaGrid','delete');
				  }
				  updateRownumWidthForJqGrid();
			  }
			})
			.navGrid('#quotaPager', {edit:false,add:false,del:false, search: false});
		
	},
	
	getQuotaGridUrl: function(id,quotaCode,quotaName,fromPercent,toPercent,fieldMap,quotaStatus){
		return '/catalog/programme-display/search-quota-group?id=' + encodeChar(id) + '&quotaCode=' + encodeChar(quotaCode) + '&quotaName=' + encodeChar(quotaName) + '&fromPercent=' + encodeChar(fromPercent) + '&toPercent=' + encodeChar(toPercent) + '&productSubCatCode='+ encodeChar(fieldMap) + '&status=' + encodeChar(quotaStatus);
	},
	searchQuota: function(){
		$('.ErrorMsgStyle').hide();
		var id = $('#selId').val();
		if(fieldMap == '-2'){
			fieldMap = '';
		}
		var quotaCode = $('#quotaCode').val().trim();
		var quotaName = $('#quotaName').val().trim();		
		var fromPercent = $('#fromPercent').val();
		var toPercent = $('#toPercent').val();
		var quotaStatus = $('#quotaStatus').val();
		var fieldMap = fieldMap;
		$('#quotaCode').focus();
		var url = ProgrammeDisplayCatalog.getQuotaGridUrl(id, quotaCode, quotaName, fromPercent,toPercent,fieldMap, quotaStatus);
		$("#quotaGrid").setGridParam({url:url,page:1}).trigger("reloadGrid");
		$('#errMsgQuotaGroup').html('').hide();
	},
	dataGridLevel:function(){
		var selId = $('#selId').val();
		var title = '';
		if($('#statusPermission').val().trim() == activeType.WAITING){
			title = '<a href="javascript:void(0);" onclick="return ProgrammeDisplayCatalog.openLevelDialog();"><img src="/resources/images/icon_add.png"></a>';
		}	
		var url = '/programme-display/search-level?id=' + encodeChar(selId);
		$('#levelGrid').datagrid({
			url : url,
			total:100,
			width:$('#levelGridContainer').width() -20,
			scrollbarSize : 0,
			rownumbers:true,
			singleSelect:true,
			autoRowHeight:true,
			fitColumns:true,
			columns:[[  
			{field: 'levelCode',title: 'Mức', width:150,align:'left',sortable : false,resizable : false,formatter:function(value,row,index){
				return Utils.XSSEncode(row.levelCode);
			}
			},
			{field: 'amount',title: 'Doanh số(VNĐ/thùng)', width:150,align:'right',sortable : false,resizable : false,
				formatter:function(value){
		    		return formatCurrency(value);	
		    	}
			},
			{field: 'quantity',title: 'Số lượng thùng', width:80,align:'right',sortable : false,resizable : false,
				formatter:function(value){
		    		return formatCurrency(value);	
		    	}
			},
			{field: 'numSku',title: 'Số SKU', width:100,align:'right',sortable : false,resizable : false,
				formatter:function(value){
		    		return formatCurrency(value);	
		    	}
			},
			{field: 'edit',title:'CT', width:40,align:'center',sortable : false,resizable : false,
				formatter: function(value,row,index){
					if($('#permissionUser').val() == 'true'){
						var amount = '';
						var sku = '';
						var quantity = '';
						if(row.amount != null && row.amount != undefined){
							amount = row.amount;
						}
						if(row.numSku != null && row.numSku != undefined){
							sku = row.numSku;
						}
						if(row.numSku != null && row.numSku != undefined){
							sku = row.numSku;
						}
						if(row.quantity != null && row.quantity != undefined){
							quantity = row.quantity;
						}
						
						if($('#statusPermission').val().trim() == activeType.WAITING){
							return "<a href='javascript:void(0)' onclick=\"return ProgrammeDisplayCatalog.openLevelDialog('"+row.id+"','"+row.levelCode+"','"+amount+"','"+sku+"','WAITING','"+quantity+"');\"><img  width='15' height='16' src='/resources/images/icon-edit.png'/></a>";
						}
						if($('#statusPermission').val().trim() == activeType.RUNNING){
							return "<a href='javascript:void(0)' onclick=\"return ProgrammeDisplayCatalog.openLevelDialog('"+row.id+"','"+row.levelCode+"','"+amount+"','"+sku+"','RUNNING','"+quantity+"');\"><img  width='15' height='16' src='/resources/images/icon-edit.png'/></a>";
						}
						if($('#statusPermission').val().trim() == activeType.STOPPED){
							return "<a href='javascript:void(0)' onclick=\"return ProgrammeDisplayCatalog.openLevelDialog('"+row.id+"','"+row.levelCode+"','"+amount+"','"+sku+"','STOPPED','"+quantity+"');\"><img  width='15' height='16' src='/resources/images/icon-edit.png'/></a>";
						}
					}else{
						var amount = '';
						var sku = '';
						var quantity = '';
						if(row.amount != null && row.amount != undefined){
							amount = row.amount;
						}
						if(row.numSku != null && row.numSku != undefined){
							sku = row.numSku;
						}
						if(row.quantity != null && row.quantity != undefined){
							quantity = row.quantity;
						}
						if($('#statusPermission').val().trim() == activeType.RUNNING){
							return "<a href='javascript:void(0)' onclick=\"return ProgrammeDisplayCatalog.openLevelDialog('"+row.id+"','"+Utils.XSSEncode(row.levelCode)+"','"+Utils.XSSEncode(amount)+"','"+Utils.XSSEncode(sku)+"','RUNNING','"+Utils.XSSEncode(quantity)+"');\"><img  width='15' height='16' src='/resources/images/icon-edit.png'/></a>";
						}else{
							return "";
						}
					}
        	    }
			},
			{field: 'remove',title:title, width:40,align:'center',sortable : false,resizable : false,
				formatter: function(value,row,index){
					if($('#permissionUser').val() == 'true'){
						if($('#statusPermission').val().trim() == activeType.WAITING){
							return "<a href='javascript:void(0)' onclick=\"return ProgrammeDisplayCatalog.deleteLevelRow('"+row.id+"');\"><img src='/resources/images/icon_delete.png'/></a>";
						}
					}else{
						return "";
					}
        	    }
			}
            ]],
			method : 'GET',
            onLoadSuccess:function(data){
            	ProgrammeDisplayCatalog._lstLevelCode.put(0, data);
            	$('.datagrid-header-rownumber').html('STT');
            	$('#levelGrid').datagrid('resize');
            }
		});
	},
	showLevelTab: function(){
		ProgrammeDisplayCatalog.hideAllTab();
		$('#tab3').addClass('Active');
		$('#container3').show();
		ProgrammeDisplayCatalog.dataGridLevel();
	},
	openLevelDialog : function(levelId,levelCode,amount,numSku,status,quantityLv) {
		$('.ErrorMsgStyle').hide();
		if(levelId != null && levelId != undefined){
			$('#levelId').val(levelId);
		}else{
			$('#levelId').val(0);
			levelId = 0;
		}
		$('#divLevelContainer').show();
		var html = $('#dialogLevel').html();
		$('#dialogLevel').dialog({  
	        closed: false,  
	        cache: false,  
	       // modal: true,
	        position: 'middle',
	        center: true, 
	        onOpen: function(){
		   		$('#dialogLevel').bind('keypress',function(event){
		   			if(event.keyCode == keyCodes.ENTER){		   				
	   					if(!$('#dialogLevel #btnChangeLevel').is(':hidden')){
	   						$('#dialogLevel #btnChangeLevel').click();
	   					}
		   			}
		   		});    
	        	Utils.bindFormatOntextfieldCurrencyFor('amountDlg', Utils._TF_NUMBER);
	        	Utils.bindFormatOntextfieldCurrencyFor('skuDlg', Utils._TF_NUMBER);
	        	Utils.bindFormatOntextfieldCurrencyFor('quantityLv', Utils._TF_NUMBER);
	        	
	        	$('.easyui-dialog #errMsgLevelDlg').html('').hide();
	        	ProgrammeDisplayCatalog.dataGridLevelOnDialog('amountLevelGrid','amountLevelContainer','amount',status,levelId);
	        	ProgrammeDisplayCatalog.dataGridDisplayLevelOnDialog('displayLevelGrid','displayLevelContainer','display',status,levelId);
	        	if(levelId != null && levelId != undefined && Number(levelId)>0){
	        		$('#levelCodeDlg').val(levelCode);
	        		$('#levelCodeDlg').attr('disabled','disabled');
	        		$('#amountDlg').focus();
	        		$('#amountDlg').val(formatFloatValue(amount));
	        		$('#skuDlg').val(formatFloatValue(numSku));
	        		$('#quantityLv').val(formatFloatValue(quantityLv));
	        	}else{
	        		$('#levelCodeDlg').removeAttr('disabled');
	        		$('#levelCodeDlg').val('');
	        		$('#levelCodeDlg').focus();
	        		$('#amountDlg').val('');
	        		$('#skuDlg').val('');
	        		$('#quantityLv').val('');
	        	}
	        	if(status == 'RUNNING' || status == 'STOPPED'){
	        		$('#amountDlg').attr('disabled','disabled');
	        		$('#skuDlg').attr('disabled','disabled');
	        		$('#quantityLv').attr('disabled','disabled');
	        		$('#btnChangeLevel').text('Đóng').bind('click',function(){
	        			$('#dialogLevel').dialog('close'); 
	        		}).attr('onclick','');
	        	}
	        },
	        onBeforeClose: function() {
	        },
	        onClose : function(){
	        	$('#dialogLevel').unbind('keypress');
	        	$('#divLevelContainer').hide();
	        }
	    });
		return false;
	},
	dataGridLevelOnDialog:function(gridId,gridContainerId,type,status,levelId){
		var selId = $('#selId').val().trim();
		var url = '';
		if(type == 'amount'){
			url = '/programme-display/search-level-product-group?id=' + encodeChar(selId) + '&typeGroup=1&levelId='+levelId;
		}else{
			url = '/programme-display/search-level-product-group?id=' + encodeChar(selId) + '&typeGroup=4&levelId='+levelId;
		}
		$('.easyui-dialog #'+gridId).datagrid({
			url : url,
			total:100,
			width:$('.easyui-dialog #'+gridContainerId).width() -20,			
			scrollbarSize : 0,
			rownumbers:true,
			fitColumns:true,			
			rownum:10,
			columns:[[  
			{field: 'displayProductGroupCode',title: 'Mã nhóm', width:130,align:'left',sortable : false,resizable : false,formatter:function(value,row,index){
				return Utils.XSSEncode(row.displayProductGroupCode);
			}
			},
			{field: 'displayProductGroupName',title: 'Tên nhóm', width:130,align:'left',sortable : false,resizable : false,formatter:function(value,row,index){
				return Utils.XSSEncode(row.displayProductGroupName);
			}
			},
			{field: 'type',title: 'Loại nhóm', width:100,align:'left',sortable : false,resizable : false,
				formatter:function(v,r,i){
					if(r.type==null) return '';
					if(r.type=='SL') return 'Sản lượng';
					if(r.type=='DS') return 'Doanh số';
					if(r.type=='SKU') return 'SKU';
				}
			},
			{field: 'min',title: 'Min', width:110,align:'right',sortable : false,resizable : false,
				formatter:function(value,row,index){
					if(status == 'RUNNING' || status == 'STOPPED'){
						return '<input style="width:90px" class="minItem" id="'+type+'_min_'+row.id+'" readonly="readonly" />';
					}else{
						return '<input style="width:90px" class="minItem" id="'+type+'_min_'+row.id+'" onfocus="return Utils.bindFormatOntextfieldCurrencyFor(\''+type+'_min_'+row.id+'\','+Utils._TF_NUMBER+');" onkeypress="EasyUiCommon.eventOnTextField(event,this,\'minItem\')"/>';
					}
				}
			},
			{field: 'max',title: 'Max', width:110,align:'right',sortable : false,resizable : false,
				formatter:function(value,row,index){
					if(status == 'RUNNING' || status == 'STOPPED'){
						return '<input style="width:90px" class="maxItem" id="'+type+'_max_'+row.id+'" readonly="readonly" />';
					}else{
						return '<input style="width:90px" class="maxItem" id="'+type+'_max_'+row.id+'" onfocus="return Utils.bindFormatOntextfieldCurrencyFor(\''+type+'_max_'+row.id+'\','+Utils._TF_NUMBER+');" onkeypress="EasyUiCommon.eventOnTextField(event,this,\'maxItem\')"/>';
					}
				}
			},
			{field: 'percent',title: '% đóng góp', width:110,align:'right',sortable : false,resizable : false,
				formatter:function(value,row,index){
					if(status == 'RUNNING' || status == 'STOPPED'){
						return '<input style="width:90px" class="percentItem" id="'+type+'_percent_'+row.id+'" readonly="readonly" onfocus="return Utils.bindFormatOntextfieldCurrencyFor(\''+type+'_percent_'+row.id+'\','+Utils._TF_NUMBER+');" onkeypress="EasyUiCommon.eventOnTextField(event,this,\'percentItem\')"/>';
					}
					return '<input style="width:90px" class="percentItem" id="'+type+'_percent_'+row.id+'" onfocus="return Utils.bindFormatOntextfieldCurrencyFor(\''+type+'_percent_'+row.id+'\','+Utils._TF_NUMBER+');" onkeypress="EasyUiCommon.eventOnTextField(event,this,\'percentItem\')"/>';
					
				}
			},
            ]],
			method : 'GET',
            onLoadSuccess:function(){
            	$('.easyui-dialog .datagrid-header-rownumber').html('STT');
            	$('.easyui-dialog #'+gridId).datagrid('resize');
            	$('#loadingLevelDialog').hide();
            	if($('#levelId').val() != '' && $('#levelId').val() > 0){
            		$.getJSON('/rest/catalog/display-program/level/detail/' + $('#levelId').val() + ".json",function(data){
	        			for(var i = 0;i<data.length;i++){
	        				if(data[i].displayProductGroup!=null){
	        					if(data[i].displayProductGroup.type == 'SKU' || data[i].displayProductGroup.type == 'DS' || data[i].displayProductGroup.type == 'SL'){
		        					$('#amount_min_'+data[i].displayProductGroup.id).val(data[i].min);
		        					$('#amount_max_'+data[i].displayProductGroup.id).val(data[i].max);
		        					$('#amount_percent_'+data[i].displayProductGroup.id).val(data[i].precent);
		        				}else{
		        					$('#sku_min_'+data[i].displayProductGroup.id).val(data[i].min);
		        					$('#sku_max_'+data[i].displayProductGroup.id).val(data[i].max);
		        					$('#sku_percent_'+data[i].displayProductGroup.id).val(data[i].precent);
		        				}
	        				}
	        			}
	        		});
            	}else{
            		$('.minItem,.maxItem,.percentItem').each(function(){
            			$(this).val('');
            		});            		
            	}
            }
		});
	},
	dataGridDisplayLevelOnDialog:function(gridId,gridContainerId,type,status,levelId){
		var selId = $('#selId').val().trim();
		ProgrammeDisplayCatalog.displayProductMap = new Map();
		var url = '/programme-display/search-level-product-group?id=' + encodeChar(selId) + '&typeGroup=4&levelId='+levelId;
		$('.easyui-dialog #'+gridId).datagrid({
			view: detailview,
			url:url,
			singleSelect:true,
			width:$('.easyui-dialog #'+gridContainerId).width() -20,
			scrollbarSize : 0,
			rownumbers:true,
			fitColumns:true,
			rownum:10,
			columns:[[  
			   {field: 'displayProductGroupName',title: 'Tên nhóm', width:130,align:'left',sortable : false,resizable : false,formatter:function(value,row,index){
				   return Utils.XSSEncode(row.displayProductGroupName);
			   }
			}
            ]],
			method : 'GET',
            onLoadSuccess:function(){
            	$('.easyui-dialog .datagrid-header-rownumber').html('STT');
            	$('.easyui-dialog #'+gridId).datagrid('resize');
            	$('#loadingLevelDialog').hide();            	
            	/** Lay danh sach san pham thuoc nhom */
            	var rows = $('.easyui-dialog #'+gridId).datagrid('getRows');
            	var groupDtlIdLst = new Array();
            	for(var i=0,size = rows.length;i<size;++i){
            		groupDtlIdLst.push(rows[i].id);
            	}           
            	var params = new Object();
            	params.groupDtlIdLst = groupDtlIdLst;
            	params.levelId = levelId;  
            	ProgrammeDisplayCatalog._lstMapProductsDP = new Map();
            	Utils.getJSONDataByAjaxNotOverlay(params,'/programme-display/search-product-detail-ex',function(data){
            		var result = data.rows;
            		for(var i=0,size = result.length;i<size;++i){
            			ProgrammeDisplayCatalog._lstMapProductsDP.put(result[i].groupId.toString(),result[i].displayPDGroupDTLVOs);
                	} 
            		var tmvv = setTimeout(function(){
                		var arrayList = $('.easyui-dialog .datagrid-row-expander');
                    	for(var i=0;i<arrayList.length;++i){
                    		var obj = $(arrayList[i]);
                    		if(!obj.is(':hidden')){                    			
                        		obj.click(); 
                    		}                    		                 		
                    	}
                    	clearTimeout(tmvv);
                	}, 500);
            	});
            	
            	
            }, 
            detailFormatter:function(index,row){  
		     		return '<div style="padding:2px; width:850"><table id="ddt-' + index + '"></table></div>';
		     },
            onExpandRow: function(index,row){
            	var tm = setTimeout(function(){
            		$('.datagrid-header-rownumber').html('STT');
                	var gridDetailId = '';
                	gridDetailId = 'ddt-'+index;
                	ProgrammeDisplayCatalog.dataGridDisplayLevelDetail(gridId,gridDetailId,row.id,index,levelId,status);  
                	var tm = setTimeout(function(){
                		$('.easyui-dialog #'+gridId).datagrid('fixDetailRowHeight',index); 
                		$('.easyui-dialog #'+gridId).datagrid('resize');                   	      	
    				}, 500);   
                	var data = ProgrammeDisplayCatalog._lstMapProductsDP.get(row.id.toString());
                	$('.easyui-dialog #'+gridDetailId).datagrid('loadData',data);
                	clearTimeout(tm);
            	}, 1000);
              } 
		});
	},
	dataGridDisplayLevelDetail:function(gridId,gridDetailId,groupId,index,levelId,status){
		$('#gridDetailId').val(gridDetailId);
		var id = $('#selId').val().trim();		
		$('.easyui-dialog #'+gridDetailId).datagrid({
			width:$('.GridSection').width() -40,		
			scrollbarSize : 0,
			fitColumns:true,
			singleSelect:true,
			rownumbers:true,			
			columns:[[  
			{field: 'productCode',title: 'Mã sản phẩm', width:150,align:'left',sortable : false,resizable : false,formatter:function(value,row,index){
				return Utils.XSSEncode(row.productCode);
			}},
			{field: 'productName',title: 'Tên sản phẩm', width:470,align:'left',sortable : false,resizable : false,formatter:function(value,row,index){
				return Utils.XSSEncode(row.productName);
			}},
			{field: 'quantity',title:"Số lượng", width:150,align:'center',sortable : false,resizable : false,
				formatter: function(value,row,index){
					var type="display";
					if(status == 'RUNNING' || status == 'STOPPED'){
						return '<input  maxlength="8" onkeypress="NextAndPrevTextField(event,this,\'quantityItem\')" prcode="'+ Utils.XSSEncode(row.productCode) +'" grcode="'+ Utils.XSSEncode(row.groupCode) +'" style="width:90px;text-align: right;" class="quantityItem" id="'+type+'_quantity_'+row.id+'" disabled="disabled" />';
					}else{
						return '<input  maxlength="8" onkeypress="NextAndPrevTextField(event,this,\'quantityItem\');" prcode="'+ Utils.XSSEncode(row.productCode) +'" grcode="'+ Utils.XSSEncode(row.groupCode) +'" style="width:90px;text-align: right;" class="quantityItem" id="'+type+'_quantity_'+row.id+'"/>';
					}
				}
			}
	        ]],
			onResize:function(){
	        	$('#'+gridId).datagrid('fixDetailRowHeight',index);  
	        },
	        onLoadSuccess:function(){	        	
	        	var tmBanChar = setTimeout(function(){
					 $('.window').each(function(){
			  			  if(!$(this).is(':hidden')){
			  				  var top = ($(window).height() / 2) - ($(this).outerHeight() / 2);
			  				  var left = ($(window).width() / 2) - ($(this).outerWidth() / 2);
			  				  if(Number(top)<0){
			  					  top = 50;
			  				  }
			  				  $(this).css({ top: top, left: left});
			  			  }
					 });	
					 clearTimeout(tmBanChar);
				 },20); 
	        	Utils.bindFormatOnTextfieldInputCss('quantityItem',Utils._TF_NUMBER);
	        	$('.quantityItem').val('');
	        	$('.datagrid-header-rownumber').html('STT');
	        	setTimeout(function(){
	        		$('.easyui-dialog #'+gridId).datagrid('fixDetailRowHeight',index);
	        		$('.easyui-dialog #'+gridId).datagrid('fixRowHeight',index);
	        	},500);	   
	        	var rows = $('.easyui-dialog #'+gridDetailId).datagrid('getRows');
	        	for(var i=0,sz = rows.length;i<sz;++i){
	        		var obj = new Object();
	        		var grcode = rows[i].groupCode;
	        		var prcode =  rows[i].productCode;	      
	        		obj.groupCode = grcode;
	        		obj.productCode = prcode;
	        		obj.groupId = groupId;
	        		obj.quantity = rows[i].quantity;
	        		var objtmp = ProgrammeDisplayCatalog.displayProductMap.get(grcode+prcode);
	        		if(objtmp!=null){
	        			obj.quantity = objtmp.quantity;
	        		}
	        		ProgrammeDisplayCatalog.displayProductMap.put(grcode+prcode,obj);
	        	}
	        	if($('#levelId').val() != '' && $('#levelId').val() > 0){
	        		if(ProgrammeDisplayCatalog.displayProductMap.size()==0){
	        			$.getJSON('/rest/catalog/display-program/level/product/' + $('#levelId').val() + ".json",function(data){
		        			for(var i = 0;i<data.length;i++){
		        				$('#display_quantity_'+data[i].displayProductGroup.id).val(data[i].min);
		        				var grcode = $('#display_quantity_'+data[i].id).attr('grcode');		        				
		        				var productCode = $('#display_quantity_'+data[i].id).attr('prcode');
		        				var obj = ProgrammeDisplayCatalog.displayProductMap.get(grcode+prcode);
		        				obj.quantity = data[i].quantity;	
		        				ProgrammeDisplayCatalog.displayProductMap.put(grcode+prcode,obj);		        				
		        			}
		        		});
	        		}else{
	        			$('.quantityItem').each(function(){
	        				var grcode = $(this).attr('grcode');
	        				var prcode = $(this).attr('prcode');
	        				var obj = ProgrammeDisplayCatalog.displayProductMap.get(grcode+prcode);	   
	        				if(obj!=null && (obj.quantity!=null || Number(obj.quantity)>0))
	        					$(this).val(obj.quantity);   				
	        			});
	        		}
	        	}
	        }
		});		
		$('.quantityItem').live('change',function(){
			var grcode = $(this).attr('grcode');
			var prcode = $(this).attr('prcode');
			var quantity = Number($(this).val().trim());
			var obj = ProgrammeDisplayCatalog.displayProductMap.get(grcode+prcode);
			if(quantity!=0){
				obj.quantity = quantity;
				ProgrammeDisplayCatalog.displayProductMap.put(grcode+prcode,obj);
			}			
		});		
	},
	
	getLevelGirdUrl: function(id,levelCode,levelAmount,levelStatus){
		return '/programme-display/search-level?id=' + encodeChar(id) + '&levelCode=' + encodeChar(levelCode) + '&levelAmount=' + encodeChar(levelAmount) + '&status=' + encodeChar(levelStatus);
	},
	searchLevel: function(){
		$('.ErrorMsgStyle').hide();
		var msg = Utils.getMessageOfNegativeNumberCheck('levelAmount','Số định mức',Utils._TF_NUMBER_COMMA);
		if(msg.length > 0){
			$('#errMsgLevel').html(msg).show();
			return false;
		}
		var id = $('#selId').val().trim();		
		var levelCode = $('#levelCode').val().trim();
		var levelAmount =  Utils.returnMoneyValue($('#levelAmount').val().trim());	
		var levelStatus = $('#levelStatus').val();
		var url = ProgrammeDisplayCatalog.getLevelGirdUrl(id,levelCode,levelAmount,levelStatus);
		$("#levelGrid").setGridParam({url:url,page:1}).trigger("reloadGrid");
		$('#levelCode').focus();
	},
	/*  locHP 18/09/2013 - showAmountTab */
	showAmountTab: function(){
		ProgrammeDisplayCatalog.hideAllTab();
		$('#tabTitle').html('Sản phẩm của chương trình trưng bày');
		$('#amountTab').addClass('Active');
		$('#amountContainer').show();		
		var selId = $('#selId').val();
		var url = '';
		url = '/programme-display/search-amount-product?id=' + encodeChar(selId) + '&typeGroup=1';
		ProgrammeDisplayCatalog.dataGridProductGroup2('amountGrid',url,ProgrammeDisplayCatalog.AMOUNT);
	}	/*  locHP 18/09/2013 - end showAmountTab */
	,
	showProductTab: function(){
		ProgrammeDisplayCatalog.hideAllTab();
		$('#tabTitle').html('Sản phẩm của chương trình trưng bày');
		$('#tab2').addClass('Active');
		$('#container4').show();		
		var selId = $('#selId').val();
		var url = '';		
		url = '/programme-display/search-product?id=' + encodeChar(selId) + '&typeGroup=4';
		ProgrammeDisplayCatalog.dataGridProductGroup('cbGrid',url,ProgrammeDisplayCatalog.DISPLAY);
	},
	dataGridProductGroup: function(gridId,url,type){
		var title = '';
		if($('#permissionUser').val() == 'true'){
			if($('#statusPermission').val().trim() == activeType.WAITING){
				title = '<a href="javascript:void(0);" onclick=\"return ProgrammeDisplayCatalog.openGroupDialog('+type+');\"><img width="15" height="16" src="/resources/images/icon_add.png"></a>';
			}
		}
		$('#'+gridId).datagrid({
			view: detailview,
			url : url,
	//		pagination : true,
			fitColumns:true,
			width:$('.GridSection').width() -20,
			scrollbarSize : 0,
			rownumbers:true,
			singleSelect : true,
		//	rownum:10,
		//	pageList:[10,20,30],
			autoRowHeight:true,
			columns:[[  
			{field: 'displayProductGroupCode',title: 'Mã nhóm', width:320,align:'left',sortable : false,resizable : false,formatter:function(value,row,index){
				return Utils.XSSEncode(row.displayProductGroupCode);
			}
			},
			{field: 'displayProductGroupName',title: 'Tên nhóm', width:500,align:'left',sortable : false,resizable : false,
				formatter:function(v,r,index){
					return '<div style="word-wrap: break-word;">' + Utils.XSSEncode(v) + '</div>';
				}
				 
			}, 
			{field: 'edit',title:'', width:70,align:'center',sortable : false,resizable : false,
				formatter: function(value,row,index){ /* viet them kiem tra "trang thai"  */
					if($('#permissionUser').val() == 'true'){
						if($('#statusPermission').val().trim() == activeType.WAITING){
							return "<a href='javascript:void(0)' onclick=\"return ProgrammeDisplayCatalog.openGroupDialog('"+type+"','"+row.id+"','"+Utils.XSSEncode(row.displayProductGroupCode)+"','"+Utils.XSSEncode(row.displayProductGroupName)+"');\"><img width='15' height='16' src='/resources/images/icon-edit.png'/></a>";
						}
					}else{
						return "";
					}
        	    }
			},
			{field: 'remove',title:title, width:70,align:'center',sortable : false,resizable : false,
				formatter: function(value,row,index){/* viet them kiem tra "trang thai"  */
					if($('#permissionUser').val() == 'true'){
						if($('#statusPermission').val().trim() == activeType.WAITING){
							return "<a href='javascript:void(0)' onclick=\"return ProgrammeDisplayCatalog.deleteGroupRow('"+row.id+"','"+type+"');\"><img width='15' height='16' src='/resources/images/icon_delete.png'/></a>";
						}
					}else{
						return "";
					}
        	    }
			}
            ]],
			method : 'GET',
		    detailFormatter:function(index,row){  
		    	if(type == ProgrammeDisplayCatalog.AMOUNT){
		    		return '<div style="padding:2px; width:850"><table id="ddv-' + index + '"></table></div>';  
		    	}else{
		    		return '<div style="padding:2px; width:850"><table id="ddt-' + index + '"></table></div>';
		    	}
            },
            onLoadSuccess:function(){
            	$('#'+gridId).datagrid('resize');
            	$('.datagrid-header-rownumber').html('STT');
            },
            onExpandRow: function(index,row){
            	var gridDetailId = '';
            	if(type == ProgrammeDisplayCatalog.AMOUNT){
            		gridDetailId = 'ddv-'+index;
            		if(row.type != 'SL'){
            			ProgrammeDisplayCatalog.dataGridDetailAMOUNT(gridId,gridDetailId,row.id,index);
            		}else{
            			ProgrammeDisplayCatalog.dataGridDetailSKU(gridId,gridDetailId,row.id,index);
            		}
            	}else{
            		gridDetailId = 'ddt-'+index;
            		ProgrammeDisplayCatalog.dataGridDetailCB(gridId,gridDetailId,row.id,index);
            	}
            	$('#'+gridId).datagrid('fixDetailRowHeight',index); 
            	$('#'+gridId).datagrid('resize');
            } 
		});
	},
	dataGridProductGroup2: function(gridId,url,type){
		var title = '';
		if($('#permissionUser').val() == 'true'){
			if($('#statusPermission').val().trim() == activeType.WAITING){
				title = '<a href="javascript:void(0);" onclick=\"return ProgrammeDisplayCatalog.openGroupDialog2('+type+');\"><img width="15" height="16" src="/resources/images/icon_add.png"></a>';
			}
		}
		$('#'+gridId).datagrid({
			view: detailview,
			url : url,
			pagination : false,
			fitColumns:true,
			width:$(window).width() - 55,
			scrollbarSize : 0,
			rownumbers:true,
			
			autoRowHeight:true,
			nowrap : false,
			columns:[[  
			{field: 'displayProductGroupCode',title: 'Mã nhóm', width:273,align:'left',sortable : false,resizable : false,formatter:function(value,row,index){
				return Utils.XSSEncode(row.displayProductGroupCode);
			}
			},
			{field: 'displayProductGroupName',title: 'Tên nhóm', width:500,align:'left',sortable : false,resizable : false,
				formatter:function(v,r,index){
					return '<div style="word-wrap: break-word">' + Utils.XSSEncode(v) +'</div>';
				}
			}, 
			{field: 'type', title: 'Loại nhóm', width:80, align: 'left', sortable: false, resizable: false,
				formatter: function(value, row, index){
					if (row.type== 'DS')
						return 'Doanh số';
					if (row.type== 'SL')
						return 'Sản lượng';
					if (row.type == 'SKU')
						return 'SKU';
				}
			},
			{field: 'edit',title:'', width:70,align:'center',sortable : false,resizable : false,
				formatter: function(value,row,index){ /* viet them kiem tra "trang thai"  */
					if($('#permissionUser').val() == 'true'){
						if($('#statusPermission').val().trim() == activeType.WAITING){
							return "<a href='javascript:void(0)' onclick=\"return ProgrammeDisplayCatalog.openGroupDialog3('"+row.type+"','"+row.id+"','"+Utils.XSSEncode(row.displayProductGroupCode)+"','"+Utils.XSSEncode(row.displayProductGroupName)+"');\"><img width='15' height='16' src='/resources/images/icon-edit.png'/></a>";
						}
					}else{
						return "";
					}
        	    }
			},
			{field: 'remove',title:title, width:70,align:'center',sortable : false,resizable : false,
				formatter: function(value,row,index){/* viet them kiem tra "trang thai"  */
					if($('#permissionUser').val() == 'true'){
						if($('#statusPermission').val().trim() == activeType.WAITING){
							return "<a href='javascript:void(0)' onclick=\"return ProgrammeDisplayCatalog.deleteGroupRow('"+row.id+"','"+type+"');\"><img width='15' height='16' src='/resources/images/icon_delete.png'/></a>";
						}
					}else{
						return "";
					}
        	    }
			}
            ]],
			method : 'GET',
		    detailFormatter:function(index,row){  
		    	if(type == ProgrammeDisplayCatalog.AMOUNT){
		    		return '<div style="padding:2px; width:850"><table id="ddv-' + index + '"></table></div>';  
		    	}else{
		    		return '<div style="padding:2px; width:850"><table id="ddt-' + index + '"></table></div>';
		    	}
            },
            onLoadSuccess:function(){
            	$('#'+gridId).datagrid('resize');
               	$('.datagrid-header-rownumber').html('STT');
            },
            onExpandRow: function(index,row){

            	var gridDetailId = '';
            	if(type == ProgrammeDisplayCatalog.AMOUNT){
            		gridDetailId = 'ddv-'+index;
//            		if(row.type != 'SL'){
//            			ProgrammeDisplayCatalog.dataGridDetailAmount2(gridId,gridDetailId,row.id,index);
//            		}else if(row.type = 'SKU'){
//            			ProgrammeDisplayCatalog.dataGridDetailSKU2(gridId,gridDetailId,row.id,index);
//            		}
            		if (row.type == 'SL'){
            			ProgrammeDisplayCatalog.dataGridDetailSKU2(gridId,gridDetailId,row.id,index);
            		}else if (row.type == 'DS') {
            			ProgrammeDisplayCatalog.dataGridDetailAmount2(gridId,gridDetailId,row.id,index);
            		} else {
            			ProgrammeDisplayCatalog.dataGridDetailAmount3(gridId,gridDetailId,row.id,index);
            		}
            		
            	}
            	$('#'+gridId).datagrid('fixDetailRowHeight',index); 
            	$('#'+gridId).datagrid('resize');
            } 
		});
	},
	dataGridDetailSKU:function(gridId,gridDetailId,groupId,index){
		$('#gridDetailId').val(gridDetailId);
		var id = $('#selId').val().trim();
		var title = '';
		if($('#permissionUser').val() == 'true'){
			if($('#statusPermission').val().trim() == activeType.WAITING){
				title = "<a href='javascript:void(0);' onclick=\"return ProgrammeDisplayCatalog.openProductDialog("+groupId+","+ProgrammeDisplayCatalog.SKU+",null,'"+gridDetailId+"');\"><img src='/resources/images/icon_add.png'></a>";
			}
		}
		/* loc bo catalog  */
		var url = '/programme-display/search-product-detail?id='+ encodeChar(id)+'&groupId=' + encodeChar(groupId);
		$('#'+gridDetailId).datagrid({
			url : url,
			total:100,
			width:$('.GridSection').width() -80,
			scrollbarSize : 0,
			fitColumns:true,
			height:'auto',
			rownumbers:true,
			singleSelect:true,
			columns:[[  
			{field: 'productCode',title: 'Mã sản phẩm', width:350,align:'left',sortable : false,resizable : false,
				formatter:function(value,row,index){
					if(row != null && row != undefined ){
						return Utils.XSSEncode(row.productCode);
					}
				}
			},
			{field: 'productName',title: 'Tên sản phẩm', width:350,align:'left',sortable : false,resizable : false,
				formatter:function(value,row,index){
					if(row != null && row != undefined){
						return Utils.XSSEncode(row.productName);
					}
				}
			},
			{field: 'convfact',title: 'Quy đổi', width:200,align:'right',sortable : false,resizable : false,
				editor:{  
                    type:'numberbox'
				},
				formatter:function(value,row,index){
					if(row != null && row != null){
						if(row.convfact == null || row.convfact == undefined){
							return 0;
						}else{
							return Utils.XSSEncode(row.convfact);
						}
					}
				}
			},
			{field: 'edit',title:'', width:70,align:'center',sortable : false,resizable : false,
				formatter: function(value,row,index){
					if($('#permissionUser').val() == 'true'){
						if($('#statusPermission').val().trim() == activeType.WAITING){
							if (row.editing){  
								var s = "<a href='javascript:void(0)' onclick=\"return ProgrammeDisplayCatalog.saveRowProduct(this,'"+groupId+"','"+gridDetailId+"');\"><span style='cursor:pointer'><img src='/resources/images/icon-datach.png'/></span></a> ";  
								var c = "<a href='javascript:void(0)' onclick=\"ProgrammeDisplayCatalog.cancelRow(this,'"+gridDetailId+"');\"><span style='cursor:pointer'><img src='/resources/images/icon_esc.png'/></span></a>";  
								return s + c;  
							} else {
								return "<a href='javascript:void(0)' onclick=\"ProgrammeDisplayCatalog.editRow(this,'"+gridDetailId+"');\"><span style='cursor:pointer'><img  width='15' height='16'  src='/resources/images/icon-edit.png'/></span></a>";  
							}  
						}
					}else{
						return "";
					}
        	    }
			},
			{field: 'remove',title:title, width:70,align:'center',sortable : false,resizable : false,
				formatter: function(value,row,index){
					if($('#permissionUser').val() == 'true'){
						if($('#statusPermission').val().trim() == activeType.WAITING){
							var productId = '';
							if(row != null && row != undefined && row != undefined){
								productId = row.id;
							}
							return "<a href='javascript:void(0)' onclick=\"return ProgrammeDisplayCatalog.deleteProductRow('"+groupId+"','"+productId+"','"+gridDetailId+"');\"><img src='/resources/images/icon_delete.png'/></a>";
						}
					}else{
						return "";
					}
        	    }
			}
            ]],
			method : 'GET',
			onResize:function(){
            	$('#'+gridId).datagrid('fixDetailRowHeight',index);  
            },
            onLoadSuccess:function(){
            	$('.datagrid-header-rownumber').html('STT');
            	setTimeout(function(){
            		$('#'+gridId).datagrid('fixDetailRowHeight',index);
            		$('#'+gridId).datagrid('fixRowHeight',index);
            	},500);
            	edit = false;
            },
            onBeforeEdit:function(index,row){  
		        row.editing = true;
		        edit =true;
		        idx = index;  
		        ProgrammeDisplayCatalog.updateActions(gridDetailId,index);  
		    },  
		    onAfterEdit:function(index,row){  
		        row.editing = false;
		        edit = false;
		        idx = -1;
		        ProgrammeDisplayCatalog.updateActions(gridDetailId,index);  
		    },  
		    onCancelEdit:function(index,row){  
		        row.editing = false;  
		        edit = false;
		        idx = -1;
		        ProgrammeDisplayCatalog.updateActions(gridDetailId,index);  
		    }  
		});
	},
	/*
	 * author: lochp
	 * ho tro datagridsku
	 */
	dataGridDetailSKU2:function(gridId,gridDetailId,groupId,index){
		$('#gridDetailId').val(gridDetailId);
		var id = $('#selId').val().trim();
		var title = '';
//		if($('#permissionUser').val() == 'true'){
//			if($('#statusPermission').val().trim() == activeType.WAITING){
//				title = "<a href='javascript:void(0);' onclick=\"return ProgrammeDisplayCatalog.openProductDialog("+groupId+","+ProgrammeDisplayCatalog.SKU+",null,'"+gridDetailId+"');\"><img src='/resources/images/icon_add.png'></a>";
//			}
//		}
		if ($('#permissionUser').val() == 'true' && parseInt($('#statusPermission').val().trim()) != activeType.STOPPED){
			title = "<a href='javascript:void(0);' onclick=\"return ProgrammeDisplayCatalog.openSearchProductDialog(2,"+groupId+", '"+gridDetailId+"');\"><img src='/resources/images/icon_add.png'></a>";
		}
		/* loc bo catalog  */
		var url = '/programme-display/search-product-detail?id='+ encodeChar(id)+'&groupId=' + encodeChar(groupId);
		$('#'+gridDetailId).datagrid({
			url : url,
			total:100,
			width:$('.GridSection').width() -40,
			height:'auto',
			scrollbarSize : 0,
			fitColumns:true,			
			rownumbers:true,
			singleSelect:true,
			columns:[[  
			{field: 'productCode',title: 'Mã sản phẩm', width:150,align:'left',sortable : false,resizable : false,
				formatter:function(value,rows,index){
					if(rows != null && rows != undefined ){
						return Utils.XSSEncode(rows.product.productCode);
					}
				}
			},
			{field: 'productName',title: 'Tên sản phẩm', width:357,align:'left',sortable : false,resizable : false,
				formatter:function(value,rows,index){
					if(rows != null && rows != undefined){
						return Utils.XSSEncode(rows.product.productName);
					}
				}
			},
			{field: 'convfact',title: 'Quy đổi', width:150,align:'right',sortable : false,resizable : false,
				editor:{  
                    type:'numberbox',
				},
				formatter:function(value,rows,index){
					if(rows != null && rows != null){
						if(rows.convfact == null || rows.convfact == undefined){
							return 0;
						}else{
							return Utils.XSSEncode(rows.convfact);
						}
					}
				}
			},
			{field: 'edit',title:'', width:50,align:'center',sortable : false,resizable : false,
				formatter: function(value,rows,index){
					if($('#permissionUser').val() == 'true'){
						if($('#statusPermission').val().trim() == activeType.WAITING){
							if (rows != undefined && rows.editing){  
								var s = "<a href='javascript:void(0)' onclick=\"return ProgrammeDisplayCatalog.saveRowProduct2(this,'"+groupId+"','"+gridDetailId+"','"+rows.product.id+"','"+rows.id+"');\"><span style='cursor:pointer'><img src='/resources/images/icon-datach.png'/></span></a> ";  
								var c = "<a href='javascript:void(0)' onclick=\"ProgrammeDisplayCatalog.cancelRow(this,'"+gridDetailId+"');\"><span style='cursor:pointer'><img src='/resources/images/icon_esc.png'/></span></a>";  
								return s + c;  
							} else {
								return "<a href='javascript:void(0)' onclick=\"ProgrammeDisplayCatalog.editRow(this,'"+gridDetailId+"');\"><span style='cursor:pointer'><img  width='15' height='16'  src='/resources/images/icon-edit.png'/></span></a>";  
							}  
						}
					}else{
						return "";
					}
        	    }
			},
			{field: 'remove',title:title, width:50,align:'center',sortable : false,resizable : false,
				formatter: function(value,rows,index){
					if($('#permissionUser').val() == 'true'){
						if($('#statusPermission').val().trim() == activeType.WAITING){
							var productId = '';
							if(rows != null && rows != undefined){
								productId = rows.product.id;
							}
							return "<a href='javascript:void(0)' onclick=\"return ProgrammeDisplayCatalog.deleteProductRow('"+groupId+"','"+productId+"','"+gridDetailId+"');\"><img src='/resources/images/icon_delete.png'/></a>";
						}
					}else{
						return "";
					}
        	    }
			}
            ]],
			method : 'GET',
			onResize:function(){
            	//$('#'+gridId).datagrid('fixDetailRowHeight',index);  
            },
            onLoadSuccess:function(){
            	$('.datagrid-header-rownumber').html('STT');
            	setTimeout(function(){
	        		$('#'+gridId).datagrid('fixDetailRowHeight',index);
	        		$('#'+gridId).datagrid('fixRowHeight',index);
	        	},800);
            	edit = false;
            },
            onBeforeEdit:function(index,row){  
		        row.editing = true;
		        edit =true;
		        idx = index;  
		        ProgrammeDisplayCatalog.updateActions(gridDetailId,index); 		       
		    },  
		    onAfterEdit:function(index,row){  
		        row.editing = false;
		        edit = false;
		        idx = -1;
		        ProgrammeDisplayCatalog.updateActions(gridDetailId,index);  
		    },  
		    onCancelEdit:function(index,row){  
		        row.editing = false;  
		        edit = false;
		        idx = -1;
		        ProgrammeDisplayCatalog.updateActions(gridDetailId,index);  
		    }  
		});		
	},
	dataGridDetailCB:function(gridId,gridDetailId,groupId,index){
		$('#gridDetailId').val(gridDetailId);
		var id = $('#selId').val().trim();
		var title = '';
		var groupIndex = index;
		if($('#permissionUser').val() == 'true'){
			if($('#statusPermission').val().trim() == activeType.WAITING){				
				title = "<a href='javascript:void(0);' onclick=\"return ProgrammeDisplayCatalog.openProductDialog("+groupId+","+ProgrammeDisplayCatalog.DISPLAY+",null,'"+gridDetailId+"');\"><img width='15' height='16' src='/resources/images/icon_add.png'></a>";
			}
		}
		var url = '/programme-display/search-product-detail-cb?id='+ encodeChar(id)+'&groupId=' + encodeChar(groupId);
		$('#'+gridDetailId).datagrid({
			url : url,
			total:100,
			width:$('.GridSection').width() -80,
			height:'auto',
			scrollbarSize : 0,
			//loctt-begin-sep19
			rownum:10,
			pagination:false,
			pageList:[10,20,30],
			//loctt-end-sep19
			fitColumns:true,
			rownumbers:true,
			singleSelect : true,
			columns:[[  
			{field: 'productCode',title: 'Mã sản phẩm', width:233,align:'left',sortable : false,resizable : false,
				formatter:function(value,rows,index){
					if(rows != null && rows != undefined){
						return Utils.XSSEncode(rows.product.productCode);
					}
				}
			},
			{field: 'productName',title: 'Tên sản phẩm', width:461,align:'left',sortable : false,resizable : false,
				formatter:function(value,rows,index){
					if(rows != null && rows != undefined){
						return Utils.XSSEncode(rows.product.productName);
					}
				}
			},
			{field: 'remove',title:title, width:54,align:'center',sortable : false,resizable : false,
				formatter: function(value,rows,index){
					if($('#permissionUser').val() == 'true'){
						if($('#statusPermission').val().trim() == activeType.WAITING){
							var productId = '';
							if(rows != null && rows != undefined){
								productId = rows.product.id;
							}
							return "<a href='javascript:void(0)' onclick=\"return ProgrammeDisplayCatalog.deleteProductCBRow('"+groupId+"','"+productId+"','"+gridDetailId+"', '"+groupIndex+"');\"><img width='15' height='16' src='/resources/images/icon_delete.png'/></a>";
						}
					}else{
						return "";
					}
	    	    }
			}
	        ]],
			method : 'GET',
			onResize:function(){
	        	$('#'+gridId).datagrid('fixDetailRowHeight',index);  
	        },
	        onLoadSuccess:function(){
	        	$('.datagrid-header-rownumber').html('STT');
	        	setTimeout(function(){
	        		$('#'+gridId).datagrid('fixDetailRowHeight',index);
	        		$('#'+gridId).datagrid('fixRowHeight',index);
	        	},500);
	        }
		});
	},
	dataGridDetailAmount:function(gridId,gridDetailId,groupId,index){
		$('#gridDetailId').val(gridDetailId);
		var id = $('#selId').val().trim();
		var title = '';
		if($('#permissionUser').val() == 'true'){
//			if($('#statusPermission').val().trim() == activeType.WAITING){				
				title = "<a href='javascript:void(0);' onclick=\"return ProgrammeDisplayCatalog.openProductDialog("+groupId+","+ProgrammeDisplayCatalog.DISPLAY+",null,'"+gridDetailId+"');\"><img width='15' height='16' src='/resources/images/icon_add.png'></a>";
//			}
		}
		var url = '/programme-display/search-product-detail?id='+ encodeChar(id)+'&groupId=' + encodeChar(groupId);
		$('#'+gridDetailId).datagrid({
			url : url,
			total:100,
			width:$('.GridSection').width() -40,
			height:'auto',
			scrollbarSize : 0,
			rownum:10,
			pagination:true,
			pageList:[10,20,30],
			fitColumns:true,
			rownumbers:true,
			columns:[[  
			{field: 'productCode',title: 'Mã sản phẩm', width:470,align:'left',sortable : false,resizable : false,
				formatter:function(value,rows,index){
					if(rows != null && rows != undefined){
					return Utils.XSSEncode(rows.product.productCode);
					}
				}
			},
			{field: 'productName',title: 'Tên sản phẩm', width:470,align:'left',sortable : false,resizable : false,
				formatter:function(value,rows,index){
					if(rows != null && rows != undefined){
					return Utils.XSSEncode(rows.product.productName);
					}
				}
			},
			{field: 'remove',title:title, width:150,align:'center',sortable : false,resizable : false,
				formatter: function(value,rows,index){
					if($('#permissionUser').val() == 'true'){
						if($('#statusPermission').val().trim() == activeType.WAITING){
							var productId = '';
							if(rows != null && rows != undefined){
								productId = rows.product.id;
							}
							return "<a href='javascript:void(0)' onclick=\"return ProgrammeDisplayCatalog.deleteProductRow('"+groupId+"','"+productId+"','"+gridDetailId+"');\"><img width='15' height='16' src='/resources/images/icon_delete.png'/></a>";
						}
					}else{
						return "";
					}
	    	    }
			}
	        ]],
			method : 'GET',
			onResize:function(){
	        	$('#'+gridId).datagrid('fixDetailRowHeight',index);  
	        },
	        onLoadSuccess:function(){
	        	$('.datagrid-header-rownumber').html('STT');
	        	setTimeout(function(){
	        		$('#'+gridId).datagrid('fixDetailRowHeight',index);
	        		$('#'+gridId).datagrid('fixRowHeight',index);
	        	},500);
	        }
		});
	},
	dataGridDetailAmount2:function(gridId,gridDetailId,groupId,index){
		$('#gridDetailId').val(gridDetailId);
		var id = $('#selId').val().trim();
		var title = '';
		if ($('#permissionUser').val() == 'true' && parseInt($('#statusPermission').val().trim()) != activeType.STOPPED){
			title = "<a href='javascript:void(0);' onclick=\"return ProgrammeDisplayCatalog.openSearchProductDialog(1,"+groupId+", '"+gridDetailId+"');\"><img src='/resources/images/icon_add.png'></a>";
		}
		var url = '/programme-display/search-product-detail?id='+ encodeChar(id)+'&groupId=' + encodeChar(groupId);
		$('#'+gridDetailId).datagrid({
			url : url,
			total:100,
			width:$('.GridSection').width() -80,
			height:'auto',
			scrollbarSize : 0,
			//loctt-begin-sep19
			rownum:10,
			pagination:false,
//			pageList:[10,20,30],
			//loctt-end-sep19
			fitColumns:true,
			rownumbers:true,
			columns:[[  
			{field: 'productCode',title: 'Mã sản phẩm', width:248,align:'left',sortable : false,resizable : false,
				formatter:function(value,rows,index){
					if(rows != null && rows != undefined){
					return Utils.XSSEncode(rows.product.productCode);
					}
				}
			},
			{field: 'productName',title: 'Tên sản phẩm', width:654,align:'left',sortable : false,resizable : false,
				formatter:function(value,rows,index){
					if(rows != null && rows != undefined){
					return Utils.XSSEncode(rows.product.productName);
					}
				}
			},
			{field: 'remove',title:title, width:70,align:'center',sortable : false,resizable : false,
				formatter: function(value,rows,index){
					if($('#permissionUser').val() == 'true'){
						if($('#statusPermission').val().trim() == activeType.WAITING){
							var productId = '';
							if(rows != null && rows != undefined){
								productId = rows.product.id;
							}
							return "<a href='javascript:void(0)' onclick=\"return ProgrammeDisplayCatalog.deleteProductRow('"+groupId+"','"+productId+"','"+gridDetailId+"');\"><img width='15' height='16' src='/resources/images/icon_delete.png'/></a>";
						}
					}else{
						return "";
					}
	    	    }
			}
	        ]],
			method : 'GET',
			onResize:function(){
	        	$('#'+gridId).datagrid('fixDetailRowHeight',index);  
	        },
	        onLoadSuccess:function(){
	        	$('.datagrid-header-rownumber').html('STT');
	        	setTimeout(function(){
	        		$('#'+gridId).datagrid('fixDetailRowHeight',index);
	        		$('#'+gridId).datagrid('fixRowHeight',index);
	        	},500);
	        }
		});
	},
	dataGridDetailAmount3:function(gridId,gridDetailId,groupId,index){
		$('#gridDetailId').val(gridDetailId);
		var id = $('#selId').val().trim();
		var title = '';
		if ($('#permissionUser').val() == 'true' && parseInt($('#statusPermission').val().trim()) != activeType.STOPPED){
			title = "<a href='javascript:void(0);' onclick=\"return ProgrammeDisplayCatalog.openSearchProductDialog(3,"+groupId+", '"+gridDetailId+"');\"><img src='/resources/images/icon_add.png'></a>";
		}
		var url = '/programme-display/search-product-detail?id='+ encodeChar(id)+'&groupId=' + encodeChar(groupId);
		$('#'+gridDetailId).datagrid({
			url : url,
			total:100,
			width:$('.GridSection').width() -80,
			height:'auto',
			scrollbarSize : 0,
			//loctt-begin-sep19
			rownum:10,
			//loctt-end-sep19
			fitColumns:true,
			rownumbers:true,
			columns:[[  
			{field: 'productCode',title: 'Mã sản phẩm', width:248,align:'left',sortable : false,resizable : false,
				formatter:function(value,rows,index){
					if(rows != null && rows != undefined){
					return Utils.XSSEncode(rows.product.productCode);
					}
				}
			},
			{field: 'productName',title: 'Tên sản phẩm', width:500,align:'left',sortable : false,resizable : false,
				formatter:function(value,rows,index){
					if(rows != null && rows != undefined){
					return Utils.XSSEncode(rows.product.productName);
					}
				}
			},
			{field: 'remove',title:title, width:150,align:'center',sortable : false,resizable : false,
				formatter: function(value,rows,index){
					if($('#permissionUser').val() == 'true'){
						if($('#statusPermission').val().trim() == activeType.WAITING){
							var productId = '';
							if(rows != null && rows != undefined){
								productId = rows.product.id;
							}
							return "<a href='javascript:void(0)' onclick=\"return ProgrammeDisplayCatalog.deleteProductRow('"+groupId+"','"+productId+"','"+gridDetailId+"');\"><img width='15' height='16' src='/resources/images/icon_delete.png'/></a>";
						}
					}else{
						return "";
					}
	    	    }
			}
	        ]],
			method : 'GET',
			onResize:function(){
	        	$('#'+gridId).datagrid('fixDetailRowHeight',index);  
	        },
	        onLoadSuccess:function(){
	        	$('.datagrid-header-rownumber').html('STT');
	        	setTimeout(function(){
	        		$('#'+gridId).datagrid('fixDetailRowHeight',index);
	        		$('#'+gridId).datagrid('fixRowHeight',index);
	        	},500);
	        }
		});
	},
	updateActions:function(gridDetailId,index){  
	    $('#'+gridDetailId).datagrid('updateRow',{  
	        index: index,  
	        row:{}  
	    });  
	}, 
	editRow:function(target,gridDetailId){
		var indexRow = ProgrammeDisplayCatalog.getRowIndex(target);
	    $('#'+gridDetailId).datagrid('beginEdit', indexRow);	    
	    var ed = $('#'+gridDetailId).datagrid('getEditor', {index:indexRow,field:'convfact'});
	    $(ed.target).attr('maxlength',7);
	},
	getRowIndex:function(target){  
	    var tr = $(target).closest('tr.datagrid-row');  
	    return parseInt(tr.attr('datagrid-row-index'));  
	},
	saveRowProduct:function(target,groupId,gridDetailId){
		var indexRow = ProgrammeDisplayCatalog.getRowIndex(target);
	    $('#'+gridDetailId).datagrid('endEdit', indexRow);
	    var dataModel = new Object();		
		dataModel.id = $('#selId').val().trim();
		dataModel.groupId = groupId;
		dataModel.productId = $('#'+gridDetailId).datagrid('getData').rows[indexRow].id;
		dataModel.isUpdate = true;
		dataModel.convfact = $('#'+gridDetailId).datagrid('getData').rows[indexRow].convfact;
		Utils.addOrSaveRowOnGrid(dataModel, "/programme-display/change-product", ProgrammeDisplayCatalog._xhrSave, null,null, function(data){
			$('#'+gridDetailId).datagrid('reload');
		},null);
		return false;
	},
	/*
	 * author: lochp
	 * ho tro cho datagriddetailsKU
	 */
	saveRowProduct2:function(target,groupId,gridDetailId, productId, groupDtlId){
		var indexRow = ProgrammeDisplayCatalog.getRowIndex(target);
	    $('#'+gridDetailId).datagrid('endEdit', indexRow);
	    var dataModel = new Object();		
		dataModel.id = $('#selId').val().trim();
		dataModel.groupId = groupId;
		dataModel.productId = productId;
		dataModel.isUpdate = true;
		if (groupDtlId != undefined || groupDtlId != null){
			dataModel.groupDtlId = groupDtlId;
		}
		dataModel.convfact = $('#'+gridDetailId).datagrid('getData').rows[indexRow].convfact;
		Utils.addOrSaveRowOnGrid(dataModel, "/programme-display/change-amount-product", ProgrammeDisplayCatalog._xhrSave, null,null, function(data){
			$('#'+gridDetailId).datagrid('reload');
		},null);
		return false;
	},
	cancelRow:function (target,gridDetailId){  
		var indexRow = ProgrammeDisplayCatalog.getRowIndex(target);
	    $('#'+gridDetailId).datagrid('cancelEdit', indexRow);  
	},
	openGroupDialog : function(type,groupId,groupCode,groupName) {
		$('.ErrorMsgStyle').hide();
		$('#divGroupContainer').show();
		var html = $('#dialogGroup').html();
		$('#dialogGroup').dialog({  
	        closed: false,  
	        cache: false,  
	        modal: true,
	        position: 'middle',
	        center: true, 
	        onOpen: function(){
	        	$('#dialogGroup').bind('keypress',function(event){
		   			if(event.keyCode == keyCodes.ENTER){		   				
	   					if(!$('#dialogGroup #btnChangeGroup').is(':hidden')){
	   						$('#dialogGroup #btnChangeGroup').click();
	   					}
		   			}
		   		});
	        	$('#errMsgGroupDlg').html('').hide();
	        	if(type == ProgrammeDisplayCatalog.DISPLAY){
	        		$('#typeGroup').val(ProgrammeDisplayCatalog.DISPLAY);
	        	}else{
	        		$('#typeGroup').val(ProgrammeDisplayCatalog.SKU);
	        	}
	        	$('#errMsgGroupDlg').html('').hide();	        	
	        	if(groupId != null && groupId != undefined){
	        		$('#groupId').val(groupId);
	        		$('#groupCodeDlg').attr('disabled','disabled');
	        		$('#groupNameDlg').focus();
	        	}else{
	        		$('#groupId').val(0);
	        		$('#groupCodeDlg').removeAttr('disabled');
	        		$('#groupCodeDlg').focus();
	        	}
	        	if(groupCode == null || groupCode == undefined){
	        		groupCode = '';
	        	}
	        	if(groupName == null || groupName == undefined){
	        		groupName = '';
	        	}
	        	$('#groupCodeDlg').val(groupCode);
				$('#groupNameDlg').val(groupName);
	        },
	        onBeforeClose: function() {
	        },
	        onClose : function(){
	        	$('#dialogGroup').unbind('keypress')
	        	$('#divGroupContainer').hide();
				var selId = $('#selId').val();
	    		var url = '';		
	    		url = '/programme-display/search-product?id=' + encodeChar(selId) + '&typeGroup=4';
	    		ProgrammeDisplayCatalog.dataGridProductGroup('cbGrid',url,ProgrammeDisplayCatalog.DISPLAY);	        }
	    });
	},
	openGroupDialog3 : function(type,groupId,groupCode,groupName) {
		$('#divGroupContainer').show();
		var html = $('#dialogGroup').html();
		$('#dialogGroup').dialog({  
	        closed: false,  
	        cache: false,  
	        modal: true,
	        position: 'middle',
	        center: true, 
	        onOpen: function(){
	        	$('#dialogGroup').bind('keypress',function(event){
		   			if(event.keyCode == keyCodes.ENTER){		   				
	   					if(!$('#dialogGroup #btnChangeGroup').is(':hidden')){
	   						$('#dialogGroup #btnChangeGroup').click();
	   					}
		   			}
		   		});
	        	$('#errMsgGroupDlg').html('').hide();
	        	if (type == 'DS')
	        		type = 1;
	        	if (type == 'SL')
	        		type = 2;
	        	if (type == 'SKU')
	        		type = 3;
	        	
	        	$('#typeGroup').val(type);
	        	
	        	$('#errMsgGroupDlg').html('').hide();	        	
	        	if(groupId != null && groupId != undefined){
	        		$('#groupId').val(groupId);
	        		$('#groupCodeDlg').attr('disabled','disabled');
	        		$('#groupNameDlg').focus();
	        	}else{
	        		$('#groupId').val(0);
	        		$('#groupCodeDlg').removeAttr('disabled');
	        		$('#groupCodeDlg').focus();
	        	}
	        	if(groupCode == null || groupCode == undefined){
	        		groupCode = '';
	        	}
	        	if(groupName == null || groupName == undefined){
	        		groupName = '';
	        	}
	        	$('#groupCodeDlg').val(groupCode);
				$('#groupNameDlg').val(groupName);
	        },
	        onBeforeClose: function() {
	        },
	        onClose : function(){
	        	$('#dialogGroup').unbind('keypress')
	        	$('#divGroupContainer').hide();
	        }
	    });
	}
	,
	openGroupDialog2 : function(type,groupId,groupCode,groupName) {
		$('#divGroupContainer2').show();
		var html = $('#dialogGroup2').html();
		$('#dialogGroup2').dialog({  
	        closed: false,  
	        cache: false,  
	        modal: true,
	        position: 'middle',
	        center: true, 
	        onOpen: function(){
	        	$('#dialogGroup2').bind('keypress',function(event){
		   			if(event.keyCode == keyCodes.ENTER){		   				
	   					if(!$('#dialogGroup2 #btnChangeGroup').is(':hidden')){
	   						$('#dialogGroup2 #btnChangeGroup').click();
	   					}
		   			}
		   		});
	        	$('#errMsgGroupDlg2').html('').hide();
	        	$('#errMsgGroupDlg2').html('').hide();	        	
	        	if(groupId != null && groupId != undefined){
	        		$('#groupId2').val(groupId);
	        		$('#groupCodeDlg2').attr('disabled','disabled');
	        		$('#groupNameDlg2').focus();
	        	}else{
	        		$('#groupId2').val(0);
	        		$('#groupCodeDlg2').removeAttr('disabled');
	        		$('#groupCodeDlg2').focus();
	        	}
	        	if(groupCode == null || groupCode == undefined){
	        		groupCode = '';
	        	}
	        	if(groupName == null || groupName == undefined){
	        		groupName = '';
	        	}
	        	$('#groupCodeDlg2').val(groupCode);
				$('#groupNameDlg2').val(groupName);
	        },
	        onBeforeClose: function() {
	        },
	        onClose : function(){
	        	$('#dialogGroup2').unbind('keypress');
	        	$('#divGroupContainer2').hide();
	        }
	    });
	},
	openProductDialog : function(groupId,typeGroup,productId,gridDetailId) {
		$('#gridDetailId').val(gridDetailId);
		if(productId != null && productId != undefined){
			$('#productId').val(productId);
			$('#isUpdate').val(1);
		}else{
			$('#productId').val(0);
			$('#isUpdate').val(0);
		}
		$('#groupId').val(groupId);
		$('#typeGroup').val(typeGroup);
		$('#divProductContainer').show();
		var html = $('#dialogProduct').html();
		$('#dialogProduct').dialog({  
	        closed: false,  
	        cache: false,  
	        modal: true,
	        position: 'middle',
	        center: true, 
	        onOpen: function(){
	        	$('#divProductContainer').html('');
	        	ProgrammeDisplayCatalog._lstProductExpand = [];
	        	ProgrammeDisplayCatalog._mapProduct = new Map();
	        	$('.easyui-dialog #errMsgProductDlg').html('').hide();
	        	$('#productCodeDlg').val('');
	        	$('#productNameDlg').val('');
	        	$('#btnSeachProductTree').bind('click',function(){
	        		ProgrammeDisplayCatalog._lstProductExpand = [];
            		var param = '&code='+encodeChar($('.easyui-dialog #productCodeDlg').val().trim())+'&name='+encodeChar($('.easyui-dialog #productNameDlg').val().trim())+'&type=&parentId=&typeObject=6&isPromotionProduct=false';
            		if(typeGroup == ProgrammeDisplayCatalog.DISPLAY){
    	        		ProgrammeDisplayCatalog.loadProductTreeGridForAmount(param,typeGroup,gridDetailId);
    	        	}else{
    	        		ProgrammeDisplayCatalog.loadProductTreeGridForSKU(param,typeGroup);
    	        	}
        		});
	        	$('#productCodeDlg,#productNameDlg').bind('keyup',function(event){
	        		if(event.keyCode == keyCodes.ENTER){
	        			ProgrammeDisplayCatalog._lstProductExpand = [];
	        			var param = '&code='+encodeChar($('.easyui-dialog #productCodeDlg').val().trim())+'&name='+encodeChar($('.easyui-dialog #productNameDlg').val().trim())+'&type=&parentId=&typeObject=6&isPromotionProduct=false';
	        			if(typeGroup == ProgrammeDisplayCatalog.DISPLAY){
	    	        		ProgrammeDisplayCatalog.loadProductTreeGridForAmount(param,typeGroup,gridDetailId);
	    	        	}else{
	    	        		ProgrammeDisplayCatalog.loadProductTreeGridForSKU(param,typeGroup);
	    	        	}
	        		}
	        	});
	        	$('#productCodeDlg').focus();
	        	if(typeGroup == ProgrammeDisplayCatalog.DISPLAY){
	        		ProgrammeDisplayCatalog.loadProductTreeGridForAmount(null,typeGroup,gridDetailId);
	        	}else{
	        		ProgrammeDisplayCatalog.loadProductTreeGridForSKU(null,typeGroup);
	        	}
	        },
	        onBeforeClose: function() {
	        },
	        onClose : function(){        	
	        	$('#divProductContainer').html(html);
	        	$('#divProductContainer').css("display", "none");	        	
	        	$('#productNameDlg').val('');
	        	$('#productCodeDlg').val('');
	        }
	    });
	},
	loadProductTreeGridForSKU:function(param,typeGroup){
		$('#loadingDialog').show();
		if(param == null || param == undefined || param == ''){
			param = '&code=&name=&type=&parentId=&typeObject=6&isPromotionProduct=false';
		}
		$('.easyui-dialog #treeGrid').treegrid({  
			url:  '/rest/catalog/product/tree-grid/'+typeGroup+'.json?id='+$('#selId').val().trim() + param,
			width:($('.easyui-dialog #treeGridContainer').width()-20),  
			height:400,  
			idField: 'id',  
			treeField: 'text',
			method:'GET',
			checkOnSelect:false,
			singleSelect:false,
			rownumbers:true,
			columns:[[  
	          {field:'text',title:'Sản phẩm',resizable:false,width:400},
	          {field: 'convfact',title: 'Quy đổi', width:180,align:'center',sortable : false,resizable : false,fixed:true,
	        	  formatter:function(value,row){
	        		  if(row.attributes.productTreeVO.type == 'PRODUCT'){
	        			  return '<input style="text-align:right;"  value="'+Utils.XSSEncode(row.attributes.productTreeVO.convfact)+'" class="numberItem" id="quantity_'+row.id+'" onfocus="return Utils.bindFormatOnTextfield(\'quantity_'+row.id+'\',Utils._TF_NUMBER,null);" onkeypress="NextAndPrevTextField(event,this,\'numberItem\')"/>';
	        		  }
	        	  }
	          },
	          {field: 'ck',checkbox:true,
	          }
	          ]],
            onLoadSuccess:function(row,data){
            	$('.easyui-dialog .datagrid-header-rownumber').html('STT');
            	$('#loadingDialog').hide();
            	setTimeout(function(){
            		CommonSearchEasyUI.fitEasyDialog();
        	  	},1000);
            	$('.numberItem').each(function(){
            		EasyUiCommon.hideCheckBoxOnTree(this);
            	});
            },
            onExpand:function(row){
            	var param = '&code=&name=&typeObject=6&isPromotionProduct=false&parentId='+row.id+'&type='+row.attributes.productTreeVO.type;
            	$.getJSON('/rest/catalog/product/tree-grid/'+typeGroup+'.json?id='+$('#selId').val().trim() + param,function(data){
            		var isExist = false;
            		for(var i = 0; i < data.rows.length ; i++){
            			if(ProgrammeDisplayCatalog._lstProductExpand.indexOf(data.rows[i].id) > -1){
            				isExist = true;
            				break;
            			}else{
            				ProgrammeDisplayCatalog._lstProductExpand.push(data.rows[i].id);
            			}
            		}
            		if(!isExist){
            			$('#treeGrid').treegrid('append',{
            				parent: row.id,  
            				data: data.rows
            			});
            		}
        		});
            },
            onCollapse:function(row){
// for(var i = 0; i < ProgrammeDisplayCatalog._lstProductExpand.length ; i++){
// $('#treeGrid').treegrid('remove',ProgrammeDisplayCatalog._lstProductExpand[i]);
// }
            },
            onCheck:function(row){
// var arr = row.attributes.productTreeVO.listChildren;
// EasyUiCommon.recursionCheck(arr,true);
            	 if(row.attributes.productTreeVO.type == 'PRODUCT'){
            		 var id = row.attributes.productTreeVO.id;
            		 var text = row.attributes.productTreeVO.name;
            		 var code = text.split('-')[0];
            		 var name = text.split('-')[1];
            		 var array = [];
            		 array.push(id);
            		 array.push(code);
        			 array.push($('#quantity_'+ id).val());
            		 array.push(name);
            		 ProgrammeDisplayCatalog._mapProduct.put(id,array);
            	 }
		    },
		    onUncheck:function(row){
// var arr = row.attributes.productTreeVO.listChildren;
// EasyUiCommon.recursionUnCheck(arr);
		    	var id = row.attributes.productTreeVO.id;
		    	ProgrammeDisplayCatalog._mapProduct.remove(id);
		    },
		    onCheckAll:function(rows){
		    	for(var i = 0;i<rows.length;i++){
		    		var array = [];
		    		var row = rows[i];
		    		var id = row.attributes.productTreeVO.id;
		    		var text = row.attributes.productTreeVO.name;
	           		var code = text.split('-')[0];
	           		var name = text.split('-')[1];
			    	array.push(id);
		    		array.push(code);
        			array.push($('#quantity_'+ id).val());
			    	array.push(name);
            		ProgrammeDisplayCatalog._mapProduct.put(id,array);
		    	}
		    },
		    onUncheckAll:function(rows){
		    	for(var i = 0;i<rows.length;i++){
		    		var row = rows[i];
		    		var id = row.attributes.productTreeVO.id;
		    		ProgrammeDisplayCatalog._mapProduct.remove(id);
		    	}
		    },
		});
	},
	loadProductTreeGridForAmount:function(param,typeGroup,gridDetailId){
		$('#loadingDialog').show();
		if(param == null || param == undefined || param == ''){
			param = '&code=&name=&isPromotionProduct=false';
		}
		$('.easyui-dialog #treeGrid').datagrid({  
			url:  '/programme-display/load-sanpham?id='+$('#selId').val().trim() + param,
			width:($('.easyui-dialog #treeGridContainer').width()-35),  
			height:'auto',  
			rownum:5,
			pagination:true,
			pageList:[5,10,15],
			method:'GET',
			checkOnSelect:true,
			singleSelect:false,
			rownumbers:true,
			columns:[[  
	          {field:'catCode',title:'Ngành',resizable:false,width:80, formatter : function(value,rowData,rowIndex) {	        	  
	        	  return Utils.XSSEncode(value);
  	        }},
  	        {field:'productCode',title:'Mã sản phẩm',resizable:false,width:250, formatter : function(value,rowData,rowIndex) {
	        	return Utils.XSSEncode(value);
	        }},
	        {field:'productName',title:'Tên sản phẩm',resizable:false,width:250, formatter : function(value,rowData,rowIndex) {
	        	return Utils.XSSEncode(value);
	        }},
	        {field: 'ck',checkbox:true}
	          ]],
            onLoadSuccess:function(data){
            	$('.easyui-dialog .datagrid-header-rownumber').html('STT');
            	$('#loadingDialog').hide();
	    			 $('.datagrid-header-check input').removeAttr('checked');
            	var rows = $('.easyui-dialog #treeGrid').datagrid('getRows');
		    	for(var i = 0; i < rows.length; i++) {
		    		if(ProgrammeDisplayCatalog._mapProduct.get(rows[i].productId)) {
		    			$('.easyui-dialog #treeGrid').datagrid('checkRow', i);
		    		}
		    	}
            },
            onCheck:function(index, row){
            		 var array = [];
            		 array.push(row.productId);
            		 array.push(row.productCode);
            		 array.push(row.productName);
            		 ProgrammeDisplayCatalog._mapProduct.put(row.productId,array);
            		 ProgrammeDisplayCatalog._gridDetaiSpCblId = gridDetailId;
		    },
		    onUncheck:function(index, row){
		    	var arr = row.children;
        		EasyUiCommon.recursionUnCheck(arr);
		    	var id = row.id;
		    	ProgrammeDisplayCatalog._mapProduct.remove(id);
		    },
		    onCheckAll:function(rows){
		    	for(var i = 0;i<rows.length;i++){
		    		var array = [];
		    		var row = rows[i];
		    		var id = row.productId;
			    	array.push(id);
		    		/*array.push(code);
			    	array.push(name);*/
            		ProgrammeDisplayCatalog._mapProduct.put(id,array);
            		ProgrammeDisplayCatalog._gridDetaiSpCblId = gridDetailId;
		    	}
		    },
		    onUncheckAll:function(rows){
		    	
		    	for(var i = 0;i<rows.length;i++){
		    		var row = rows[i];
		    		var id = row.productId;
		    		ProgrammeDisplayCatalog._mapProduct.remove(id);
		    	}
		    }		   
		});
	},
	deleteGroupRow:function(groupId,type){
		var dataModel = new Object();
		dataModel.groupId = groupId;
		var selId = $('#selId').val();
		var text = '';
		var gridId = '';
		var tmpUrl = '';
		var gridType= -1;
		if(type == ProgrammeDisplayCatalog.AMOUNT){
			tmpUrl = '/programme-display/search-amount-product?id=' + encodeChar(selId) + '&typeGroup=1';
			text = 'nhóm doanh số';
			gridId = 'amountGrid';
			gridType = 1;
}		else if(type == ProgrammeDisplayCatalog.DISPLAY){
			tmpUrl = '/programme-display/search-product?id=' + encodeChar(selId) + '&typeGroup=4';
			text = 'nhóm trưng bày';
			gridId = 'cbGrid';
			gridType = 4;
		}else{
			text = 'nhóm SP cho SKU';
			gridId = 'skuGrid';
		}
		Utils.deleteSelectRowOnGrid(dataModel, text , '/programme-display/delete-group', ProgrammeDisplayCatalog._xhrDel, gridId,'errMsgGroup',function(data){
			showSuccessMsg('successMsgGroup',data);
			var selId = $('#selId').val();
			var url = '';		
			url = tmpUrl;
			if (gridType != 1){
				ProgrammeDisplayCatalog.dataGridProductGroup(gridId,url,gridType);
			}
			if (gridType == 1){
				ProgrammeDisplayCatalog.dataGridProductGroup2(gridId,url,gridType);
			}
		},null);		
		return false;
	},
	deleteProductCBRow: function(groupId,productId,gridDetailId, groupIndex){
		var dataModel = new Object();
		dataModel.groupId = groupId;
		dataModel.productId = productId;	
		Utils.deleteSelectRowOnGrid(dataModel, 'sản phẩm trong nhóm', '/programme-display/delete-product', ProgrammeDisplayCatalog._xhrDel, gridDetailId,'errMsgProduct',function(data){
			showSuccessMsg('successMsgProduct',data);
			var selId = $('#selId').val();
			$('#'+gridDetailId).datagrid('reload');
			//var url = '';		
			//url = '/programme-display/search-product?id=' + encodeChar(selId) + '&typeGroup=4';
			//ProgrammeDisplayCatalog.dataGridProductGroup('cbGrid',url,ProgrammeDisplayCatalog.DISPLAY);
			ProgrammeDisplayCatalog.dataGridDetailCB('cbGrid',gridDetailId,groupId,groupIndex);
		},'productLoading');		
		return false;
	},
	deleteProductRow: function(groupId,productId,gridDetailId, index){
		var dataModel = new Object();
		dataModel.groupId = groupId;
		dataModel.productId = productId;	
		Utils.deleteSelectRowOnGrid(dataModel, 'sản phẩm trong nhóm', '/programme-display/delete-product', ProgrammeDisplayCatalog._xhrDel, gridDetailId,'errMsgProduct',function(data){
			showSuccessMsg('successMsgProduct',data);
			var selId = $('#selId').val();
			$('#'+gridDetailId).datagrid('reload');
			//var url = '';		
			//url = '/programme-display/search-product?id=' + encodeChar(selId) + '&typeGroup=4';
			//ProgrammeDisplayCatalog.dataGridProductGroup('cbGrid',url,ProgrammeDisplayCatalog.DISPLAY);
			//ProgrammeDisplayCatalog.dataGridDetailCB('cbGrid',gridDetailId,groupId,index);
		},'productLoading');		
		return false;
		
	},
	changeGroup:function(){
		var msg = '';
		$('.ErrorMsgStyle').hide();
		msg = Utils.getMessageOfRequireCheck('groupCodeDlg','Mã nhóm');
		if(msg.length == 0){
			msg = Utils.getMessageOfSpecialCharactersValidate('groupCodeDlg','Mã nhóm',Utils._CODE);
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfRequireCheck('groupNameDlg','Tên nhóm');
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfSpecialCharactersValidate('groupNameDlg','Tên nhóm');
		}
		if(msg.length > 0){
			$('#errMsgGroupDlg').html(msg).show();
			return false;
		}
		var dataModel = new Object();		
		dataModel.id = $('#selId').val().trim();
		dataModel.groupId = $('#groupId').val().trim();
		dataModel.groupCode = $('#groupCodeDlg').val().trim();
		dataModel.groupName = $('#groupNameDlg').val().trim();	
		dataModel.typeGroup = $('#typeGroup').val();	
		var gridId = '';
		var tmpUrl = '';
		var gridType = -1;
		var selId = $('#selId').val();
		if($('#typeGroup').val() == 4 ){
			tmpUrl = '/programme-display/search-product?id=' + encodeChar(selId) + '&typeGroup=4';
			gridId = 'cbGrid';
			gridType = 4;
		}else{
			tmpUrl = '/programme-display/search-amount-product?id=' + encodeChar(selId) + '&typeGroup=1';
			gridId = 'amountGrid';
			gridType = 1;
		}
		Utils.addOrSaveRowOnGrid(dataModel, "/programme-display/change-group", ProgrammeDisplayCatalog._xhrSave, gridId,'errMsgGroupDlg', function(data){
			$('#groupId').val(data.groupId);
			showSuccessMsg('successMsgGroupDlg',data);
			$('.easyui-dialog').dialog('close');
			
			var url = '';		
			url = tmpUrl;
			if (gridType == 1){
//				ProgrammeDisplayCatalog.dataGridProductGroup2(gridId,url,gridType);
				$('#amountGrid').datagrid('reload');
			}else{
//				ProgrammeDisplayCatalog.dataGridProductGroup(gridId,url,gridType);
				$('#cbGrid').datagrid('reload');
			}
		},null);
		
		return false;
	},
	changeGroup2:function(){
		var msg = '';
		$('.ErrorMsgStyle').hide();
		msg = Utils.getMessageOfRequireCheck('groupCodeDlg2','Mã nhóm');
		if(msg.length == 0){
			msg = Utils.getMessageOfSpecialCharactersValidate('groupCodeDlg2','Mã nhóm',Utils._CODE);
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfRequireCheck('groupNameDlg2','Tên nhóm');
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfSpecialCharactersValidate('groupNameDlg2','Tên nhóm');
		}
		if (msg.length == 0 && $('#typeGroup2').val() == -1){
			msg = "Bạn chưa chọn Loại nhóm.";
		}
		
		if(msg.length > 0){
			$('#errMsgGroupDlg2').html(msg).show();
			return false;
		}
		var dataModel = new Object();		
		dataModel.id = $('#selId').val().trim();
		dataModel.groupId = $('#groupId2').val().trim();
		dataModel.groupCode = $('#groupCodeDlg2').val().trim();
		dataModel.groupName = $('#groupNameDlg2').val().trim();	
		dataModel.typeGroup = $('#typeGroup2').val();	
		var gridId = '';
		if($('#typeGroup').val() == ProgrammeDisplayCatalog.DISPLAY ){
			gridId = 'amountGrid';
		}else{
			gridId = 'skuGrid';
		}
		Utils.addOrSaveRowOnGrid(dataModel, "/programme-display/change-amount-group", ProgrammeDisplayCatalog._xhrSave, gridId,'errMsgGroupDlg2', function(data){
			$('#groupId2').val(data.groupId);
			showSuccessMsg('successMsgGroupDlg',data);
			$("#amountGrid").datagrid('reload');
			$('.easyui-dialog').dialog('close');
		},null);

		
		
		
		
		return false;
	},
	changeProduct:function(){
		var msg = '';
		$('.ErrorMsgStyle').hide();
		var lstProductId = [];
		var lstConvfact = [];
		console.log(ProgrammeDisplayCatalog._mapProduct);
		var key = ProgrammeDisplayCatalog._mapProduct.keyArray;
		if(key != null && key.length > 0){
			var isErr = false;
			if(parseInt($('#isUpdate').val()) == 1){
				
			}else{
				for(var i = 0;i<key.length;i++){
					if(!isErr){
						lstProductId.push(key[i]);
						if($('#typeGroup').val() == ProgrammeDisplayCatalog.SKU ){
							if($('#quantity_'+ key[i]).val() != '' && $('#quantity_'+ key[i]).val() > 0){
								lstConvfact.push($('#quantity_'+ key[i]).val().trim());
							}else{
								msg = 'Vui lòng nhập số lượng quy đổi';
								$('#quantity_'+ key[i]).focus();
								isErr = true;
							}
						}
					}
				}
			}
		}else{
			msg = 'Vui lòng chọn Sản phẩm';
		}
		if(msg.length > 0){
			$('.easyui-dialog #errMsgProductDlg').html(msg).show();
			return false;
		}
		var dataModel = new Object();		
		dataModel.id = $('#selId').val().trim();
		dataModel.groupId = $('#groupId').val().trim();
		dataModel.lstProductId = lstProductId;
		if(parseInt($('#isUpdate').val()) == 1){
			
		}else{
			dataModel.lstConvfact = lstConvfact;	
		}
		var gridId = $('#gridDetailId').val();
		if($('#typeGroup').val() == ProgrammeDisplayCatalog.DISPLAY ){
			gridId = 'amountGrid';
			}else{
				gridId = 'skuGrid';
			}
		gridId = ProgrammeDisplayCatalog._gridDetaiSpCblId;
		Utils.addOrSaveRowOnGrid(dataModel, "/programme-display/change-product", ProgrammeDisplayCatalog._xhrSave, gridId,'errMsgProductDlg', function(data){
			//showSuccessMsg('successMsgProductDlg',data);
			$('.easyui-dialog').dialog('close');			
			$('.easyui-dialog').dialog('close');
			var selId = $('#selId').val();
			var url = '';		
			url = '/programme-display/search-product?id=' + encodeChar(selId) + '&typeGroup=4';
			//$('#'+ProgrammeDisplayCatalog._gridDetaiSpCblId).datagrid('reload');
		},null);
		return false;
	},
	
	changeLevel:function(){
		var msg = '';
		$('.easyui-dialog #errMsgLevelDlg').html('').hide();
		msg = Utils.getMessageOfRequireCheck('levelCodeDlg','Mã mức');
		if(msg.length == 0){
			msg = Utils.getMessageOfSpecialCharactersValidate('levelCodeDlg','Mã mức',Utils._CODE);
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfSpecialCharactersValidateEx('amountDlg','Doanh số',Utils._TF_NUMBER_COMMA);
		}
		if(msg.length == 0){
			var amountDlg = $('#amountDlg').val().trim();
			amountDlg = Utils.returnMoneyValue(amountDlg);
			if($('#amountDlg').val() != '' && Number(amountDlg)<= 0){
				msg = 'Doanh số phải lớn hơn 0';
			}
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfSpecialCharactersValidateEx('skuDlg','Số SKU',Utils._TF_NUMBER_COMMA);
		}
		if(msg.length == 0){
			var skuDlg = $('#skuDlg').val().trim();
			skuDlg = Utils.returnMoneyValue(skuDlg);
			if($('#skuDlg').val() != '' && Number(skuDlg) <= 0){
				msg = 'Số SKU phải lớn hơn 0';
			}
		}
		if(msg.length == 0){
			var quantityLv = $('#quantityLv').val().trim();
			quantityLv = Utils.returnMoneyValue(quantityLv);
			if($('#quantityLv').val() != '' && Number(quantityLv) <= 0){
				msg = 'Số SKU phải lớn hơn 0';
			}
		}
		if(msg.length > 0){
			$('.easyui-dialog #errMsgLevelDlg').html(msg).show();
			return false;
		}
		var dataModel = new Object();		
		dataModel.id = $('#selId').val().trim();
		dataModel.levelId = $('#levelId').val().trim();
		dataModel.levelCode = $('#levelCodeDlg').val().trim();
		dataModel.amount = Utils.returnMoneyValue($('#amountDlg').val().trim());
		dataModel.sku = Utils.returnMoneyValue($('#skuDlg').val().trim());
		dataModel.quantityLv =  Utils.returnMoneyValue(quantityLv);
		var lstMinValue = [];
		var lstMaxValue = [];
		var lstPercentValue = [];
		var lstQuantityValue = [];
		var lstGroupId = [];		
		
		$('.easyui-dialog .minItem').each(function(){
			msg = Utils.getMessageOfSpecialCharactersValidateEx($(this).attr('id'),'Min',Utils._TF_NUMBER_COMMA);
			if(msg.length == 0){
				lstGroupId.push($(this).attr('id').split("_")[2]);
				var value = 0;
				if($(this).val() != null && $(this).val() != '' && Utils.returnMoneyValue($(this).val()) > 0){
					value = Utils.returnMoneyValue($(this).val());
				}
				lstMinValue.push(value);
			}else{
				return false;
			}
		});
		if(msg.length > 0){
			$('.easyui-dialog #errMsgLevelDlg').html(msg).show();
			return false;
		}
		var indexMaxItem = 0;
		$('.easyui-dialog .maxItem').each(function(){
			msg = Utils.getMessageOfSpecialCharactersValidateEx($(this).attr('id'),'Max',Utils._TF_NUMBER_COMMA);
			if(msg.length == 0){
				var value = 0;
				if($(this).val() != null && $(this).val() != '' && Utils.returnMoneyValue($(this).val()) > 0){
					value = Utils.returnMoneyValue($(this).val().trim());
				}
				lstMaxValue.push(value);
				var flag = Number(lstMinValue[indexMaxItem])>0 && Number(lstMaxValue[indexMaxItem])>0;
				if(flag && Number(lstMinValue[indexMaxItem]) > Number(lstMaxValue[indexMaxItem])){
					msg = 'Giá trị Min phải nhỏ hơn giá trị Max.';
					$('.easyui-dialog .minItem').eq(indexMaxItem).focus();
				}
				++indexMaxItem;
			}else{
				return false;
			}
		});
		if(msg.length > 0){
			$('.easyui-dialog #errMsgLevelDlg').html(msg).show();
			return false;
		}
		$('.easyui-dialog .percentItem').each(function(){
			msg = Utils.getMessageOfSpecialCharactersValidateEx($(this).attr('id'),'% đóng góp',Utils._TF_NUMBER_COMMA);
			if(msg.length == 0){
				var value = 0;
				if($(this).val() != null && $(this).val() != '' && Utils.returnMoneyValue($(this).val()) > 0){
					value = Utils.returnMoneyValue($(this).val());
				}
				lstPercentValue.push(value);
			}else{
				return false;
			}
		});
		if(msg.length > 0){
			$('.easyui-dialog #errMsgLevelDlg').html(msg).show();
			return false;
		}
		var lstGroupProductLv = new Array();
		$('.easyui-dialog .quantityItem').each(function(){			
			msg = Utils.getMessageOfSpecialCharactersValidateEx($(this).attr('id'),'Số lượng',Utils._TF_NUMBER_COMMA);
			if(msg.length == 0){
				var value = $(this).val().trim();
				if(!isNullOrEmpty(value)){
					value = Utils.returnMoneyValue(value);
				}			
				var key = $(this).attr('grcode').trim() + $(this).attr('prcode');
				var obj = ProgrammeDisplayCatalog.displayProductMap.get(key);
				var strValue = obj.groupId + ';' + obj.productCode + ';' + value;
				lstGroupProductLv.push(strValue);
				
			}else{
				return false;
			}
		});		
		if(msg.length > 0){
			$('.easyui-dialog #errMsgLevelDlg').html(msg).show();
			return false;
		}
		dataModel.lstGroupId = lstGroupId;
		dataModel.lstMinValue = lstMinValue;
		dataModel.lstMaxValue = lstMaxValue;	
		dataModel.lstPercentValue = lstPercentValue;
		dataModel.lstQuantityValue = lstQuantityValue;
		dataModel.lstGroupProductLv = lstGroupProductLv;
		Utils.addOrSaveRowOnGrid(dataModel, "/programme-display/change-level", ProgrammeDisplayCatalog._xhrSave, 'levelGrid','errMsgLevelDlg', function(data){
			showSuccessMsg('successMsgLevelDlg',data);
			ProgrammeDisplayCatalog.dataGridLevel();			
			setTimeout(function(){
				$('.easyui-dialog').dialog('close');
			}, 1500);				
		},null);
		return false;
	},
	getProductGridUrl: function(id, productCode, productName, quantity){
		return '/programme-display/search-product?id=' + encodeChar(id) + '&productCode=' + encodeChar(productCode) + '&productName=' + encodeChar(productName) + '&quantity=' + encodeChar(quantity);
	},
	searchProduct: function(){
		var id = $('#selId').val().trim();		
		var productCode = $('#productCode').val().trim();
		var productName = $('#productName').val().trim();		
		var quantity = $('#quantity').val();
		$('#productCode').focus();
		var url = ProgrammeDisplayCatalog.getProductGridUrl(id, productCode, productName, quantity);
		$("#productGrid").setGridParam({url:url,page:1}).trigger("reloadGrid");
	},
	showStaffTab: function(){
		//applyDateTimePicker("#month", "mm/yy", null, null, null, null, null, null, null,true,null);
		ProgrammeDisplayCatalog.hideAllTab();
		$('#tab5').addClass('Active');
		$('#container5').show();
		var selId = $('#selId').val().trim();
		var now = new Date();
		var cYear = now.getFullYear();
		var cMonth = now.getMonth() + 1;
		if(cMonth != 10 && cMonth != 11 && cMonth != 12  ){
			cMonth = '0'+cMonth;
		}
		var currentDate = cMonth+'/'+cYear;
		$('#month').val(currentDate);
		//$('#month').val(getCurrentMonth());
		$('#shop').combotree('setValue', Number($('#curShopId').val().trim()));
		/*setTimeout(function(){
			ProgrammeDisplayCatalog.searchStaff();
	  	},1000);*/
		setTimeout(function() {
		$('#container5 #staffGrid').datagrid({
			url : '/programme-display/search-staff',
			autoRowHeight : true,
			rownumbers : true, 
			checkOnSelect :true,
			pagination:true,			
			scrollbarSize : 0,
			pageSize:20,
			pageList  :[10,20,30,40,50],
			width : $(window).width()-60,
			queryParams:{				
				month:getCurrentMonth(),
			    id:  encodeChar(selId)
			},
			fitColumns:true,
			columns:[[  
			{field: 'shopCode',title: 'Mã NPP', width:100,align:'left',sortable : false,resizable : false,
				formatter: function(value,row,index){
					return Utils.XSSEncode(row.staff.shop.shopCode);
				}
			},
			{field: 'staffCode',title: 'Mã NVBH', width:120,align:'left',sortable : false,resizable : false,
				formatter: function(value,row,index){
					return Utils.XSSEncode(row.staff.staffCode); 
				}
			},
			{field: 'staffName',title: 'Tên NVBH', width:250,align:'left',sortable : false,resizable : false,
				formatter: function(value,row,index){
					return Utils.XSSEncode(row.staff.staffName); 
				}
			},
			{field: 'month',title: 'Tháng', width:80,align:'center',sortable : false,resizable : false,
				formatter: function(cellValue,row,index){
					if(cellValue!=undefined && cellValue!=null){				
						return $.datepicker.formatDate('mm/yy', new Date(cellValue));
					} 
				}
			},
			{field: 'quantityMax',title: 'Số suất', width:100,align:'right',sortable : false,resizable : false,
				formatter:function(value){
		    		return formatCurrency(value);	
		    	}
			},	
			{field: 'quantityReceived',title: 'Đã phân bổ', width:100,align:'right',sortable : false,resizable : false,
				formatter:function(value){
		    		return formatCurrency(value);	
		    	}
			},
			{field: 'uploadDate',title: 'Ngày tạo', width:100,align:'center',sortable : false,resizable : false,
				formatter: function(cellValue,row,index){
					if(cellValue!=undefined && cellValue!=null){				
						return $.datepicker.formatDate('dd/mm/yy', new Date(cellValue));
					} 
				}
			},
			{field: 'updateDate',title: 'Ngày điều chỉnh', width:100,align:'center',sortable : false,resizable : false,
				formatter: function(cellValue,row,index){
					if(cellValue!=undefined && cellValue!=null){				
						return $.datepicker.formatDate('dd/mm/yy', new Date(cellValue));
					} 
				}
			},
            ]],			
            onLoadSuccess:function(){            	
            	$('.datagrid-header-rownumber').html('STT'); 
            	//$('#container5 #staffGrid').datagrid('options').fitColumns = true;
            	$('#container5 #staffGrid').datagrid('resize');
            }            
		});
		}, 10);
	},
	unCheckDatagrid: function() {
		var rows = $('#staffGrid').datagrid('getRows');
		var isCheckAll = $('#container5 #checkDeleteAll1').is(':checked');
		if(ProgrammeDisplayCatalog._mapStaff.size() == 0){
			if(isCheckAll == true){
			$('#container5 #checkDeleteAll1').attr('checked','checked');
			}else{
			$('#container5 #checkDeleteAll1').removeAttr('checked');
			}
		}else{
	    	$('#container5 .datagrid-header-check input').removeAttr('checked');
	    	for(var i = 0; i < rows.length; i++) {
	    		if(ProgrammeDisplayCatalog._mapStaff.get(rows[i].id)) {
	    			$('#staffGrid').datagrid('uncheckRow', i);
	    		}
	    	}
	    	$('#container5 #checkDeleteAll1').attr('checked','checked');
		}
    	return true;
	},
	deleteQuantityStaff: function() {
		$('.ErrorMsgStyle').hide();
		var isCheckAll = $('#container5 #checkDeleteAll1').is(':checked');
		var lstChecked = $('#container5 #staffGrid').datagrid('getChecked');
		var rows = $('#container5 #staffGrid').datagrid('getRows');		
		var lstIdStaffMap = new Array();
		var month = '';
		for(var i = 0; i < lstChecked.length; i++) {
			ProgrammeDisplayCatalog._mapStaff.put(lstChecked[i].id,lstChecked[i]);			
		}
		var arra = ProgrammeDisplayCatalog._mapStaff.valArray;		
		if((isCheckAll == false && arra.length == 0)
				|| (isCheckAll && rows.length==0)) {
			$('#container5 #errMsgStaff').html('Bạn chưa chọn nhân viên để xóa. Vui lòng chọn nhân viên').show();
			return;
		}		
		for(var i=0,size=arra.length;i<size;++i){
			lstIdStaffMap.push(arra[i].id);
			var dateMonth = new Date(arra[i].month);
			month = toMonthYearString(dateMonth);					
			if(!Utils.compareCurrentMonthEx(month)){
				$('#container5 #errMsgStaff').html('Không được xóa NVBH trong CTTB của tháng trước tháng hiện tại').show();
				return;
			}
			
		}
		if(month == '' || month == null) {
			var dateMonth = new Date(rows[0].month); 
			month = toMonthYearString(dateMonth);				
			if(!Utils.compareCurrentMonthEx(month)){
				$('#container5 #errMsgStaff').html('Không được xóa NVBH trong CTTB của tháng trước tháng hiện tại').show();
				return;
			}
		}
		var params = new Object();
		params.isCheckAll = isCheckAll;
		params.lstIdStaffMap = lstIdStaffMap;
		params.month = month;
		var code = $('#displayProgram').val();
		if(code !=null && code != undefined)
			params.code = code.trim();
		var id = $('#selId').val();
		if(id!=null && id!=undefined)
			params.id = $('#selId').val().trim();		
		Utils.addOrSaveData(params, '/programme-display/staff-map/delete-staff-map', null, 'errMsgStaff', function() {
			$('#container5 #staffGrid').datagrid('reload');
		}, '#container5 ', true,null, ' Bạn có muốn xóa nhân viên được chọn');
		
	},
	exportStaffMap:function(){
		$('.ErrorMsgStyle').hide();
		var id = $('#selId').val().trim();		
		var shopId =  $('#shop').combotree('getValue');
		var month = $('#month').val().trim();
		var params = new Object();
		params.id = id;
		params.shopId = shopId;
		params.month = month;
		$.messager.confirm('Xác nhận','Bạn có muốn xuất file?',function(r){
			if(r){
				ReportUtils.exportReport('/programme-display/staff-map/export-Excel-Staff', params, 'errMsg');					
			}
		});		
		return false;
	},
	openStaffDialog:function(){
		$('.ErrorMsgStyle').hide();
		$('#divStaffContainer').show();
		var html = $('#dialogStaff').html();
		$('#dialogStaff').dialog({  
	        closed: false,  
	        cache: false,  
	        modal: true,
	        position: 'middle',
	        center: true, 
	        onOpen: function(){
	        	$('#staffCodeDlg').val('');
	        	$('#staffNameDlg').val('');
	        	$('.easyui-dialog #errMsgStaffDlg').html('').hide();
	        	ProgrammeDisplayCatalog._mapStaff = new Map();
	        	ProgrammeDisplayCatalog.dataGridStaffOnDialog();
	        	$('#staffCodeDlg,#staffNameDlg').bind('keyup',function(event){
	        		if(event.keyCode == keyCodes.ENTER){
	        			$('#staffGridDialog').datagrid('load',{page:1,id:$('#selId').val().trim(),code:encodeChar($('#staffCodeDlg').val().trim()),name:encodeChar($('#staffNameDlg').val().trim())});
	        		}
	        	});
	        	$('#btnSeachStaffDlg').bind('click',function(){
        			$('#staffGridDialog').datagrid('load',{page:1,id:$('#selId').val().trim(),code:encodeChar($('#staffCodeDlg').val().trim()),name:encodeChar($('#staffNameDlg').val().trim())});
	        	});
	        },
	        onBeforeClose: function() {
	        },
	        onClose : function(){
	        	$('#divStaffContainer').hide();
	        }
	    });
	},
	openCustomerDialog:function(){
		$('#divCustomerContainer').show();
		var html = $('#dialogCustomer').html();
		$('#dialogCustomer').dialog({  
	        closed: false,  
	        cache: false,  
	        modal: true,
	        position: 'middle',
	        center: true, 
	        onOpen: function(){
	        	$('#levelCodeCustomerDlg').val(-1);
	        	$('#levelCodeCustomerDlg').change();
	        	$('#customerCodeDlg').val('');
	        	$('#customerNameDlg').val('');
	        	$('#addressDlg').val('');
	        	$('#startDateDlg').val('');
	        	$('#endDateDlg').val('');
	        	$('.easyui-dialog #errMsgCustomerDlg').html('').hide();
	        	$('#customerCodeDlg,#customerNameDlg,#addressDlg').bind('keyup',function(event){
	        		if(event.keyCode == keyCodes.ENTER){
	        			var code = encodeChar($('#customerCodeDlg').val().trim());
	    	        	var name =$('#customerNameDlg').val().trim().replace(/\s+/g, ' ');
	    	        	var address = $('#addressDlg').val().trim().replace(/\s+/g, ' ');
	        			$('#customerGridDialog').datagrid('load',{page:1,id:$('#selId').val().trim(),code:code,name:name,address:address});
	        		}
	        	});
	        	$('#btnSeachCustomerDlg').bind('click',function(){
	        		var code = encodeChar($('#customerCodeDlg').val().trim());
		        	var name = $('#customerNameDlg').val().trim().replace(/\s+/g, ' ');
		        	var address = $('#addressDlg').val().trim().replace(/\s+/g, ' ');
	        		$('#customerGridDialog').datagrid('load',{page:1,id:$('#selId').val().trim(),code:code,name:name,address:address});
	        	});
	        	ProgrammeDisplayCatalog._mapCustomer = new Map();
	        	ProgrammeDisplayCatalog.dataGridCustomerOnDialog();
	        },
	        onBeforeClose: function() {
	        },
	        onClose : function(){
	        	$('#customerGridDialog').datagrid('load',{page:1,id:$('#selId').val().trim(),code:'',name:'',address:''});
	        	$('#divCustomerContainer').hide();
	        }
	    });
	},
	dataGridStaffOnDialog:function(){
		var params = new Object();
		params.page = 1;
		params.id = $('#selId').val().trim();
		params.code = $('#staffCodeDlg').val().trim();
		params.name = $('#staffNameDlg').val().trim();
		$('#staffGridDialog').datagrid({
			url : '/catalog/programme-display/search-staff-on-dialog',
			queryParams:params,
			total:100,
			width:$('#staffGridDialogContainer').width() -20,
			pageList  : [10,20,30],
			pagination:true,
			pageSize:10,
			scrollbarSize : 0,
			fitColumns:true,
			pageNumber:1,
			rownumbers:true,
			columns:[[  
				{field: 'staffCode',title: 'Mã nhân viên', width:250,align:'left',sortable : false,resizable : false,formatter:function(value,row,index){
					return Utils.XSSEncode(row.staffCode);
				}
				},
				{field: 'staffName',title: 'Tên nhân viên', width:345,align:'left',sortable : false,resizable : false,formatter:function(value,row,index){
					return Utils.XSSEncode(row.staffName);
				}
				},
				{field: 'ck',checkbox:true},
				{field: 'id',hidden:true}
            ]],
			method : 'GET',
            onLoadSuccess:function(data){
        		var html = $('#staffGridDialogContainer .datagrid-view2 .datagrid-body .datagrid-btable tbody').children();
        		var size = html.length;
        		var count = 0;
        		for(var i = 0; i < size ; i++){
        			if(i > 0){
        				html = html.next();
        			}
        			var idValue = html.first().children().last().children().text();
        			if(ProgrammeDisplayCatalog._mapStaff.keyArray.indexOf(parseInt(idValue)) > -1){
        				html.first().children().last().prev().children().children().attr('checked','checked');
        				count++;
        			}else{
        				html.first().children().last().prev().children().children().removeAttr('checked');
        			}
            	}
        		if(count > 0 && count == size){
        			$('#staffGridDialogContainer .datagrid-view2').children().children().children().children().children().children().first().next().next().children().children().attr('checked','checked');
        		}else{
        			$('#staffGridDialogContainer .datagrid-view2').children().children().children().children().children().children().first().next().next().children().children().removeAttr('checked');
        		}
            	$('.datagrid-header-rownumber').html('STT');
            	$('#staffGridDialog').datagrid('resize');
            	$('#loadingStaffDialog').hide();
            },
            onCheck:function(index,row){
           		ProgrammeDisplayCatalog._mapStaff.put(row.id,row.id);
		    },
		    onUncheck:function(index,row){
		    	ProgrammeDisplayCatalog._mapStaff.remove(row.id);
		    },
		    onCheckAll:function(rows){
		    	for(var i = 0;i<rows.length;i++){
		    		var row = rows[i];
		    		ProgrammeDisplayCatalog._mapStaff.put(row.id,row.id);
		    	}
		    },
		    onUncheckAll:function(rows){
		    	for(var i = 0;i<rows.length;i++){
		    		var row = rows[i];
		    		ProgrammeDisplayCatalog._mapStaff.remove(row.id);
		    	}
		    },
		});		
	},
	changeStaff:function(){
		var msg = '';
		$('.easyui-dialog #errMsgStaffDlg').html('').hide();
		var lstStaffId = [];
		var key = ProgrammeDisplayCatalog._mapStaff.keyArray;
		if(key != null && key.length > 0){
			for(var i = 0;i<key.length;i++){
				lstStaffId.push(key[i]);
			}
		}else{
			msg = 'Vui lòng chọn nhân viên phát triển khách hàng';
		}
		if(msg.length > 0){
			$('.easyui-dialog #errMsgStaffDlg').html(msg).show();
			return false;
		}
		var dataModel = new Object();		
		dataModel.id = $('#selId').val().trim();
		dataModel.lstStaffId = lstStaffId;
		Utils.addOrSaveRowOnGrid(dataModel, "/catalog/programme-display/change-staff", ProgrammeDisplayCatalog._xhrSave, 'staffGrid','errMsgStaffDlg', function(data){
			showSuccessMsg('successMsgStaffDlg',data);
			$('.easyui-dialog').dialog('close');
		},null);
		return false;
	},
	changeCustomer:function(){
		var msg = '';
		$('.easyui-dialog #errMsgCustomerDlg').html('').hide();
		$('#errMsgCustomer').html('').hide();
		msg = Utils.getMessageOfRequireCheck('levelCodeCustomerDlg','Mã mức',true);
		if(msg.length == 0){
			msg = Utils.getMessageOfRequireCheck('startDateDlg','Từ ngày');
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfRequireCheck('endDateDlg','Đến ngày');
		}
		var startDate = $('#startDateDlg').val().trim();
		var endDate = $('#endDateDlg').val().trim();
		if(msg.length == 0){
			if(startDate != '' && endDate != ''){
				if(!Utils.compareDate(startDate, endDate)){
					msg = 'Từ ngày phải là ngày trước hoặc cùng ngày với đến ngày';	
					$('#startDateDlg').focus();
				}
			}
		}
		var currentTime = new Date();
		var month = currentTime.getMonth() + 1;
		var day = currentTime.getDate();
		var year = currentTime.getFullYear();
		if(msg.length == 0){
			if(startDate != ''){
				if(getCurrentDate() == startDate || !Utils.compareDate(day + '/' + month + '/' + year,startDate)){
					msg = 'Từ ngày phải lớn hơn ngày hiện tại.';	
					$('#startDateDlg').focus();
				}
			}
		}
		if(msg.length == 0){
			if(endDate != ''){
				if(getCurrentDate() == endDate || !Utils.compareDate(day + '/' + month + '/' + year,endDate)){
					msg = 'Đến ngày phải lớn hơn ngày hiện tại.';	
					$('#endDateDlg').focus();
				}
			}
		}
		var lstCustomerId = [];
		var key = ProgrammeDisplayCatalog._mapCustomer.keyArray;
		if(key != null && key.length > 0){
			for(var i = 0;i<key.length;i++){
				lstCustomerId.push(key[i]);
			}
		}else{
			msg = 'Vui lòng chọn khách hàng tham gia';
		}
		if(msg.length > 0){
			$('.easyui-dialog #errMsgCustomerDlg').html(msg).show();
			return false;
		}
		var dataModel = new Object();		
		dataModel.id = $('#selId').val().trim();
		dataModel.staffId = $('#staffId').val().trim();
		dataModel.lstCustomerId = lstCustomerId;
		dataModel.levelId = $('#levelCodeCustomerDlg').val();
		dataModel.fromDate = startDate;
		dataModel.toDate = endDate;
		Utils.addOrSaveRowOnGrid(dataModel, "/catalog/programme-display/change-customer", ProgrammeDisplayCatalog._xhrSave, 'customerGrid','errMsgCustomerDlg', function(data){
			$('#staffGrid').datagrid('reload');
			showSuccessMsg('successMsgCustomerDlg',data);
			$('.easyui-dialog').dialog('close');
		},null);
		return false;
	},
	dataGridCustomerOnDialog:function(){
		var params = new Object();
		params.page = 1;
		params.id = $('#selId').val().trim();
		params.code = $('#customerCodeDlg').val().trim();
		params.name = $('#customerNameDlg').val().trim();
		params.address = $('#addressDlg').val().trim();
		var url = '/catalog/programme-display/search-customer-on-dialog';
		$('#customerGridDialog').datagrid({
			url : url,
			total:100,
			queryParams:params,
			width:$('#customerGridDialogContainer').width() -20,
			pageList  : [10,20,30],
			pagination:true,
			pageSize:10,
			scrollbarSize : 0,
			fitColumns:true,
			rownumbers:true,
			columns:[[  
			{field: 'shortCode',title: 'Mã KH', width:150,align:'left',sortable : false,resizable : false,formatter:function(value,row,index){
				return Utils.XSSEncode(row.shortCode);
			}
			},
			{field: 'customerName',title: 'Tên KH', width:150,align:'left',sortable : false,resizable : false,formatter:function(value,row,index){
				return Utils.XSSEncode(row.customerName);
			}
			},
			{field: 'address',title: 'Địa chỉ', width:280,align:'left',sortable : false,resizable : false,formatter:function(value,row,index){
				return Utils.XSSEncode(row.address);
			}
			},
			{field: 'ck',checkbox:true},
			{field: 'id',hidden:true}
            ]],
			method : 'GET',
            onLoadSuccess:function(){
            	var html = $('#customerGridDialogContainer .datagrid-view2 .datagrid-body .datagrid-btable tbody').children();
            	var size = html.length;
            	var count = 0;
        		for(var i = 0; i < size ; i++){
        			if(i > 0){
        				html = html.next();
        			}
        			var idValue = html.first().children().last().children().text();
        			if(ProgrammeDisplayCatalog._mapCustomer.keyArray.indexOf(parseInt(idValue)) > -1){
        				html.first().children().last().prev().children().children().attr('checked','checked');
        				count++;
        			}else{
        				html.first().children().last().prev().children().children().removeAttr('checked');
        			}
            	}
        		if(count > 0 && count == size){
        			$('#customerGridDialogContainer .datagrid-view2').children().children().children().children().children().children().first().next().next().next().children().children().attr('checked','checked');
        		}else{
        			$('#customerGridDialogContainer .datagrid-view2').children().children().children().children().children().children().first().next().next().next().children().children().removeAttr('checked');
        		}
            	$('.datagrid-header-rownumber').html('STT');
            	$('#customerGridDialog').datagrid('resize');
            	$('#loadingCustomerDialog').hide();
            	setTimeout(function(){
            		CommonSearchEasyUI.fitEasyDialog();
        	  	},1000);
            },
            onCheck:function(index,row){
           		 ProgrammeDisplayCatalog._mapCustomer.put(row.id,row.id);
		    },
		    onUncheck:function(index,row){
		    	ProgrammeDisplayCatalog._mapCustomer.remove(row.id);
		    },
		    onCheckAll:function(rows){
		    	for(var i = 0;i<rows.length;i++){
		    		var row = rows[i];
		    		ProgrammeDisplayCatalog._mapCustomer.put(row.id,row.id);
		    	}
		    },
		    onUncheckAll:function(rows){
		    	for(var i = 0;i<rows.length;i++){
		    		var row = rows[i];
		    		ProgrammeDisplayCatalog._mapCustomer.remove(row.id);
		    	}
		    },
		});	
	},
	viewCustomerInfo:function(staffId,staffCode,staffName){
		$('#staffId').val(staffId);
		$('#divCustomerDetail').show();
		$('#toggleCustomer').removeClass('LinkHideStyle').addClass('LinkShowStyle');
		ProgrammeDisplayCatalog.toggleCustomer();
		$('#divStaffInfoId').html(staffCode+'-'+staffName);
		ProgrammeDisplayCatalog.showCustomerTab(staffId);
		$('#customerCode,#customerName,#startDate,#endDate').bind('keyup',function(event){
    		if(event.keyCode == keyCodes.ENTER){
    			var msg = '';
    			$('#errMsgCustomerSearch').html('').hide();
    			msg = Utils.getMessageOfInvalidFormatDate('startDate','Từ ngày');
    			if(msg.length == 0){
    				msg = Utils.getMessageOfInvalidFormatDate('endDate','Đến ngày');
    			}
    			if(msg.length == 0){
    				if($('#startDate').val().trim() != '' && $('#endDate').val().trim() != ''){
    					if(!Utils.compareDate($('#startDate').val().trim(), $('#endDate').val().trim())){
    						msg = 'Từ ngày phải là ngày trước hoặc cùng ngày với đến ngày';				
    					}
    				}
    			}
    			if(msg.length > 0){
    				$('#errMsgCustomerSearch').html(msg).show();
    				return false;
    			}
    			$('#customerGrid').datagrid('load',{page:1,staffId:staffId,code:$('#customerCode').val().trim(),name:$('#customerName').val().trim(),fromDate:$('#startDate').val().trim(),toDate:$('#endDate').val().trim(),levelId:$('#levelCode').val()});
    		}
    	});
		$('#btnSearchCustomer').bind('click',function(){
			var msg = '';
			$('#errMsgCustomerSearch').html('').hide();
			msg = Utils.getMessageOfInvalidFormatDate('startDate','Từ ngày');
			if(msg.length == 0){
				msg = Utils.getMessageOfInvalidFormatDate('endDate','Đến ngày');
			}
			if(msg.length > 0){
				$('#errMsgCustomerSearch').html(msg).show();
				return false;
			}
			$('#customerGrid').datagrid('load',{page:1,staffId:staffId,code:$('#customerCode').val().trim(),name:$('#customerName').val().trim(),fromDate:$('#startDate').val().trim(),toDate:$('#endDate').val().trim(),levelId:$('#levelCode').val()});
		});
		$(window).scrollTop(5000);
	},
	toggleCustomer:function(){
		if($('#toggleCustomer').hasClass('LinkHideStyle')){		
			$('#toggleCustomer').addClass('LinkShowStyle');
			$('#toggleCustomer').removeClass('LinkHideStyle');
			$('#divCustomerSearch').show();
			$('#customerCode').focus();
			$('#toggleCustomer').html("Đóng tìm kiếm &lt;&lt;");
		}else {
			$('#toggleCustomer').addClass('LinkHideStyle');
			$('#toggleCustomer').removeClass('LinkShowStyle');
			$('#divCustomerSearch').hide();
			$('#toggleCustomer').html("Tìm kiếm &lt;&lt;");
		}
	},
	showCustomerTab: function(staffId){
		$('#customerCode').val('');
		$('#customerName').val('');
		$('#startDate').val('');
		$('#endDate').val('');
		$('#levelCode').val(-1);
		$('#levelCode').change();
		var params = new Object();
		params.page = 1;
		params.staffId = staffId;
		params.code = $('#customerCode').val().trim();
		params.name = $('#customerName').val().trim();
		params.fromDate = $('#startDate').val().trim();
		params.toDate = $('#endDate').val().trim();
		params.levelId = -1;
		var title = '<a href="javascript:void(0);" onclick="return ProgrammeDisplayCatalog.openCustomerDialog();"><img src="/resources/images/icon-add.png"></a>';
		if(ProgrammeDisplayCatalog.isStopped){
			title = '';
		}
		var url = '/catalog/programme-display/customer/search';
		$('#customerGrid').datagrid({
			url : url,
			total:100,
			queryParams:params,
			width:$('#customerGridContainer').width() -20,
			pageList  : [10,20,30],
			pagination:true,
			pageSize:10,
			scrollbarSize : 0,
			fitColumns:true,
			rownumbers:true,
			singleSelect:true,
			columns:[[  
			{field: 'shortCode',title: 'Mã KH', width:150,align:'center',sortable : false,resizable : false,formatter:function(value,row,index){
				return Utils.XSSEncode(row.shortCode);
			}
			},
			{field: 'customerName',title: 'Tên KH', width:250,align:'left',sortable : false,resizable : false,formatter:function(value,row,index){
				return Utils.XSSEncode(row.customerName);
			}
			},
			{field: 'levelCode',title: 'Mức', width:160,align:'left',sortable : false,resizable : false,
				editor:{  
                    type:'combobox',
                    options:{
                    	data: ProgrammeDisplayCatalog._lstLevelCode.get(0).rows,
            	        valueField:'levelCode',  
            	        textField:'levelCode',
            	        method:'GET',
            	        class:'easyui-combobox',
            	        filter: function(q, row){
                    		var opts = $(this).combobox('options');
                    		return row[opts.textField].indexOf(q.toUpperCase()) == 0;
                    	}
                    }
				},
			},
			{field: 'fromDateStr',title:'Từ ngày', width:130,align:'center',sortable : false,resizable : false,
				editor:{  
                    type:'text'
				},
        	},
        	{field: 'toDateStr',title:'Đến ngày', width:130,align:'center',sortable : false,resizable : false,
        		editor:{  
                    type:'text'
				},
        	},
			{field: 'edit',title:'', width:80,align:'center',sortable : false,resizable : false,
        		formatter: function(value,row,index){
        			if(!ProgrammeDisplayCatalog.isStopped){
        				if(row != null && row != undefined){
        					var currentTime = new Date();
        					var month = currentTime.getMonth() + 1;
        					var day = currentTime.getDate();
        					var year = currentTime.getFullYear();
        					var fromDate = row.fromDateStr;
        					var toDate = row.toDateStr;
        					if(fromDate != '' && toDate != ''){
        						if(!Utils.compareDate(day + '/' + month + '/' + year,fromDate) && !Utils.compareDate(day + '/' + month + '/' + year,toDate)){
        							if(!ProgrammeDisplayCatalog.checkFromDate || !ProgrammeDisplayCatalog.checkToDate){
        								if (row.editing){
        									var s = "<a title='Lưu' href='javascript:void(0)' onclick=\"return ProgrammeDisplayCatalog.saveRowCustomer(this,'customerGrid');\"><span style='cursor:pointer'><img src='/resources/images/icon-datach.png'/></span></a> ";  
        									var c = "<a title='Hủy' href='javascript:void(0)' onclick=\"ProgrammeDisplayCatalog.cancelRowCustomer(this,'customerGrid');\"><span style='cursor:pointer'><img src='/resources/images/icon_esc.png'/></span></a>";  
        									return s + c;
        								}else {
        									return "<a title='Sửa' href='javascript:void(0)' onclick=\"ProgrammeDisplayCatalog.editRowCustomer(this,'customerGrid');\"><span style='cursor:pointer'><img src='/resources/images/icon-edit.png'/></span></a>";  
        								} 
        							}else{
        								return '';
        							}
        						}else{
        							if (row.editing){  
        								var s = "<a title='Lưu' href='javascript:void(0)' onclick=\"return ProgrammeDisplayCatalog.saveRowCustomer(this,'customerGrid');\"><span style='cursor:pointer'><img src='/resources/images/icon-datach.png'/></span></a> ";  
        								var c = "<a title='Hủy' href='javascript:void(0)' onclick=\"ProgrammeDisplayCatalog.cancelRowCustomer(this,'customerGrid');\"><span style='cursor:pointer'><img src='/resources/images/icon_esc.png'/></span></a>";  
        								return s + c;  
        							} else {
        								return "<a title='Sửa' href='javascript:void(0)' onclick=\"ProgrammeDisplayCatalog.editRowCustomer(this,'customerGrid');\"><span style='cursor:pointer'><img src='/resources/images/icon-edit.png'/></span></a>";  
        							}  
        						}
        					}
        				}
        			}
        	    }
			},
			{field: 'remove',title:title, width:80,align:'center',sortable : false,resizable : false,
				formatter: function(value,row,index){
					if(!ProgrammeDisplayCatalog.isStopped){
						if(row != null && row != undefined){
							var currentTime = new Date();
							var month = currentTime.getMonth() + 1;
							var day = currentTime.getDate();
							var year = currentTime.getFullYear();
							var fromDate = row.fromDateStr;
							var toDate = row.toDateStr;
							if(fromDate != '' && toDate != ''){
								if(!Utils.compareDate(day + '/' + month + '/' + year,fromDate) || !Utils.compareDate(day + '/' + month + '/' + year,toDate)){
									return '';
								}else{
									return "<a title='Xóa' href='javascript:void(0)' onclick=\"return ProgrammeDisplayCatalog.deleteCustomerRow('"+row.displayCustomerMapId+"');\"><img src='/resources/images/icon-delete.png'/></a>";
								}
							}
						}
					}
        	    }
			}
            ]],
			method : 'GET',
            onLoadSuccess:function(data){
            	$('.datagrid-header-rownumber').html('STT');
            	$('#customerGrid').datagrid('resize');
            	for(var i = 0; i < data.rows.length ; i++){
            		ProgrammeDisplayCatalog.toDate.put(data.rows[i].displayCustomerMapId,data.rows[i].toDateStr);
            		ProgrammeDisplayCatalog.fromDate.put(data.rows[i].displayCustomerMapId,data.rows[i].fromDateStr);
	        	}
            	edit = false;
            },
            onBeforeEdit:function(index,row){  
		        row.editing = true;
		        edit =true;
		        idx = index;  
		        ProgrammeDisplayCatalog.updateActions('customerGrid',index);  
		    },  
		    onAfterEdit:function(index,row){  
		        row.editing = false;
		        edit = false;
		        idx = -1;
		        ProgrammeDisplayCatalog.updateActions('customerGrid',index);  
		    },  
		    onCancelEdit:function(index,row){  
		        row.editing = false;  
		        edit = false;
		        idx = -1;
		        ProgrammeDisplayCatalog.updateActions('customerGrid',index);  
		    }  
		});		
	},
	cancelRowCustomer:function (target,gridDetailId){  
		var indexRow = ProgrammeDisplayCatalog.getRowIndex(target);
	    $('#'+gridDetailId).datagrid('cancelEdit', indexRow);  
	    $('#'+gridDetailId).datagrid('beginEdit', indexRow);
	    var ed = $('#'+gridDetailId).datagrid('getEditor', {index:indexRow,field:'toDateStr'});
		if(ed != null && ed != undefined){
			$(ed.target).val(ProgrammeDisplayCatalog.toDate.get($('#'+gridDetailId).datagrid('getData').rows[indexRow].displayCustomerMapId));
		}
		$('#'+gridDetailId).datagrid('endEdit', indexRow); 
	},
	editRowCustomer:function(target,gridDetailId){
		var indexRow = ProgrammeDisplayCatalog.getRowIndex(target);
	    $('#'+gridDetailId).datagrid('beginEdit', indexRow);
	    var fromDate = $('#'+gridDetailId).datagrid('getData').rows[indexRow].fromDateStr;
	    var toDate = $('#'+gridDetailId).datagrid('getData').rows[indexRow].toDateStr;
	    var currentTime = new Date();
		var month = currentTime.getMonth() + 1;
		var day = currentTime.getDate();
		var year = currentTime.getFullYear();
	    if(fromDate != ''){
	    	var ed = $('#'+gridDetailId).datagrid('getEditor', {index:indexRow,field:'fromDateStr'});
			if(!Utils.compareDate(day + '/' + month + '/' + year,fromDate)){
				if(ed != null && ed != undefined){
					$(ed.target).attr('disabled','disabled');
					ProgrammeDisplayCatalog.checkFromDate = false;
				}
			}else{
				applyDateTimePicker(ed.target,null,null,null,null,null,true);
			}
		}
	    if(toDate != ''){
	    	var ed = $('#'+gridDetailId).datagrid('getEditor', {index:indexRow,field:'toDateStr'});
			if(!Utils.compareDate(day + '/' + month + '/' + year,toDate)){
				if(ed != null && ed != undefined){
					$(ed.target).attr('disabled','disabled');
					ProgrammeDisplayCatalog.checkToDate = false;
				}
			}else{
				applyDateTimePicker(ed.target,null,null,null,null,null,true);
			}
		}
	    if(!ProgrammeDisplayCatalog.checkFromDate || !ProgrammeDisplayCatalog.checkToDate){
	    	var ed = $('#'+gridDetailId).datagrid('getEditor', {index:indexRow,field:'levelCode'});
	    	if(ed != null && ed != undefined){
	    		var html = $(ed.target).parent().children().last().children().first();
	    		html.removeClass('combo-text validatebox-text').addClass('datagrid-editable-input');
	    		html.attr('value',html.next().next().val());
	    		html.attr('disabled','disabled');
	    		html.next().children().removeClass('combo-arrow');
			}
	    }
	},
	saveRowCustomer:function(target,gridId){
		var indexRow = ProgrammeDisplayCatalog.getRowIndex(target);
		$('#'+gridId).datagrid('endEdit', indexRow);
		var dataModel = new Object();		
		dataModel.id = $('#selId').val().trim();
		dataModel.staffId = $('#staffId').val().trim();
		dataModel.displayCustomerMapId = $('#'+gridId).datagrid('getData').rows[indexRow].displayCustomerMapId;
		dataModel.isUpdate = true;
		var levelCode = $('#'+gridId).datagrid('getData').rows[indexRow].levelCode;
		var fromDate = $('#'+gridId).datagrid('getData').rows[indexRow].fromDateStr;
		var toDate = $('#'+gridId).datagrid('getData').rows[indexRow].toDateStr;
		var msg = '';
		$('#errMsgCustomer').html('').hide();
		if(levelCode == null || levelCode == undefined || levelCode == ''){
			msg = 'Vui lòng chọn mã mức';
		}
		if(msg.length == 0){
			if (fromDate != '' && !Utils.isDate(fromDate)) {
				msg = 'Từ ngày không hợp lệ, vui lòng nhập theo định dạng dd/MM/yyyy';
			}
		}
		if(msg.length == 0){
			if (toDate != '' && !Utils.isDate(toDate)) {
				msg = 'Đến ngày không hợp lệ, vui lòng nhập theo định dạng dd/MM/yyyy';
			}
		}
		if(msg.length == 0){
			if(fromDate == null || fromDate == undefined || fromDate == ''){
				msg = 'Vui lòng nhập từ ngày';
			}
		}
		if(msg.length == 0){
			if(toDate == null || toDate == undefined || toDate == ''){
				msg = 'Vui lòng nhập đến ngày';
			}
		}
		if(msg.length == 0){
			if(fromDate != '' && toDate != ''){
				if(!Utils.compareDate(fromDate, toDate)){
					msg = 'Từ ngày phải là ngày trước hoặc cùng ngày với đến ngày';	
				}
			}
		}
		var currentTime = new Date();
		var month = currentTime.getMonth() + 1;
		var day = currentTime.getDate();
		var year = currentTime.getFullYear();
		if(msg.length == 0){
			if(fromDate != ''){
				if(ProgrammeDisplayCatalog.checkFromDate){
					if(getCurrentDate() == fromDate || !Utils.compareDate(day + '/' + month + '/' + year,fromDate)){
						msg = 'Từ ngày phải lớn hơn ngày hiện tại.';	
					}
				}
			}
		}
		if(msg.length == 0){
			if(toDate != ''){
				if(getCurrentDate() == toDate || !Utils.compareDate(day + '/' + month + '/' + year,toDate)){
					msg = 'Đến ngày phải lớn hơn ngày hiện tại.';	
				}
			}
		}
		if(msg.length > 0){
			$('#errMsgCustomer').html(msg).show();
			$('#'+gridId).datagrid('beginEdit', indexRow);
			var ed = $('#'+gridId).datagrid('getEditor', {index:indexRow,field:'fromDateStr'});
			if(ed != null && ed != undefined){
				$(ed.target).val(ProgrammeDisplayCatalog.fromDate.get(dataModel.displayCustomerMapId));
				if(!ProgrammeDisplayCatalog.checkFromDate){
					$(ed.target).attr('disabled','disabled');
				}else{
					applyDateTimePicker(ed.target,null,null,null,null,null,true);
				}
			}
			var ed = $('#'+gridId).datagrid('getEditor', {index:indexRow,field:'toDateStr'});
			if(ed != null && ed != undefined){
				$(ed.target).val(ProgrammeDisplayCatalog.toDate.get(dataModel.displayCustomerMapId));
				applyDateTimePicker(ed.target,null,null,null,null,null,true);
			}
			if(!ProgrammeDisplayCatalog.checkFromDate || !ProgrammeDisplayCatalog.checkToDate){
		    	var ed = $('#'+gridId).datagrid('getEditor', {index:indexRow,field:'levelCode'});
		    	if(ed != null && ed != undefined){
		    		var html = $(ed.target).parent().children().last().children().first();
		    		html.removeClass('combo-text validatebox-text').addClass('datagrid-editable-input');
		    		html.attr('value',html.next().next().val());
		    		html.attr('disabled','disabled');
		    		html.next().children().removeClass('combo-arrow');
				}
		    }
			return false;
		}
		dataModel.levelCode = levelCode;
		dataModel.fromDate = fromDate;
		dataModel.toDate = toDate;
		Utils.addOrSaveRowOnGrid(dataModel, "/catalog/programme-display/change-customer", ProgrammeDisplayCatalog._xhrSave, 'customerGrid','errMsgCustomer', function(data){
		});
		return false;
	},
	getStaffGridUrl: function(id,staffCode,staffName){
		return '/catalog/programme-display/search-staff?id=' + encodeChar(id) + '&staffCode=' + encodeChar(staffCode) + '&staffName=' + encodeChar(staffName);   
	},
	getCustomerGridUrl: function(code,name,fromDate,toDate,level){		
		var staffMapId = $('#selStaffId').val().trim();				
		if(level == '-2'){
			level = '';
		}
		return '/catalog/programme-display/customer/search?staffId=' + encodeChar(staffMapId) + '&code=' + encodeChar(code) + '&name=' + encodeChar(name) + '&fromDate=' + encodeChar(fromDate) + '&toDate=' + encodeChar(toDate) + '&levelId=' + encodeChar(level);
	},
	searchStaff: function(){
		ProgrammeDisplayCatalog._mapStaff = new Map();
		var id = $('#selId').val().trim();		
		var shopId = $('#shop').combotree('getValue');
		var month = $('#month').val().trim();		
		var params = new Object();
		params.id = id;
		params.shopId = shopId;
		params.month = month;
		$("#staffGrid").datagrid('reload',params);
		$("#staffGrid").datagrid('uncheckAll');
		if(!Utils.compareCurrentMonthEx(month)){
			$('#checkDeleteAll1').attr('disabled',true);
			$('#checkDeleteAll1').attr('checked',false);
			disabled('staffImportBtn');
		}else{
			$('#checkDeleteAll1').attr('disabled',false);
			enable('staffImportBtn');
		}
		
	},
	deleteLevelRow: function(levelId){
		var dataModel = new Object();
		dataModel.levelId = levelId;	
		Utils.deleteSelectRowOnGrid(dataModel, 'mã mức', '/programme-display/delete-level', ProgrammeDisplayCatalog._xhrDel, 'levelGrid','errMsgLevel',function(data){
			showSuccessMsg('successMsgLevel',data);
			ProgrammeDisplayCatalog.dataGridLevel();
		},'levelLoading');		
		return false;
	},
	addOrSaveLevelTab: function(isEdit){
		var msg = '';
		$('#errMsgLvlDlg').html('').hide();
		msg = Utils.getMessageOfRequireCheck('levelCodeLvlDlg','Mã định mức');
		if(msg.length == 0){
			msg = Utils.getMessageOfSpecialCharactersValidate('levelCodeLvlDlg','Mã định mức',Utils._CODE);
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfRequireCheck('amountLvlDlg','Số định mức');
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfNegativeNumberCheck('amountLvlDlg','Số định mức',Utils._TF_NUMBER_COMMA);
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfInvalidMaxLength('numSKULvlDlg', 'Số SKU');
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfNegativeNumberCheck('numSKULvlDlg','Số SKU',Utils._TF_NUMBER_COMMA);
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfRequireCheck('numProductDisplayLvlDlg','Số mặt trưng bày');
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfNegativeNumberCheck('numProductDisplayLvlDlg','Số mặt trưng bày',Utils._TF_NUMBER_COMMA);
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfInvalidMaxLength('percentDisCountPassLvlDlg', '% đạt DS');
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfNegativeNumberCheck('percentDisCountPassLvlDlg','% đạt DS',Utils._TF_NUMBER_COMMA);
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfInvalidMaxLength('percentDisCountOverLvlDlg', '% vượt DS');
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfNegativeNumberCheck('percentDisCountOverLvlDlg','% vượt DS',Utils._TF_NUMBER_COMMA);
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfInvalidMaxLength('revenueBonusLvlDlg', 'Thưởng DS');
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfNegativeNumberCheck('revenueBonusLvlDlg','Thưởng DS',Utils._TF_NUMBER_COMMA);
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfInvalidMaxLength('maxDisCountLvlDlg', 'Max chiết khấu');
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfNegativeNumberCheck('maxDisCountLvlDlg','Max chiết khấu',Utils._TF_NUMBER_COMMA);
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfInvalidMaxLength('displayBonusLvlDlg', 'Thưởng TB');
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfNegativeNumberCheck('displayBonusLvlDlg','Thưởng TB',Utils._TF_NUMBER_COMMA);
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfInvalidMaxLength('percentSupportDisplayLvlDlg', '% hỗ trợ TB');
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfNegativeNumberCheck('percentSupportDisplayLvlDlg','% hỗ trợ TB',Utils._TF_NUMBER_COMMA);
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfInvalidMaxLength('paidPriceLvlDlg', 'Đơn giá trả');
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfNegativeNumberCheck('paidPriceLvlDlg','Đơn giá trả',Utils._TF_NUMBER_COMMA);
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfInvalidMaxLength('maxSupportDisplayLvlDlg', 'Max hỗ trợ TB');
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfNegativeNumberCheck('maxSupportDisplayLvlDlg','Max hỗ trợ TB',Utils._TF_NUMBER_COMMA);
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfInvalidMaxLength('displayPostion1LvlDlg', 'Vị trí bày bán 1');
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfNegativeNumberCheck('displayPostion1LvlDlg','Vị trí bày bán 1',Utils._TF_NUMBER_COMMA);
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfInvalidMaxLength('displayPostion2LvlDlg', 'Vị trí bày bán 2');
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfNegativeNumberCheck('displayPostion2LvlDlg','Vị trí bày bán 2',Utils._TF_NUMBER_COMMA);
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfInvalidMaxLength('displayPostion3LvlDlg', 'Vị trí bày bán 3');
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfNegativeNumberCheck('displayPostion3LvlDlg','Vị trí bày bán 3',Utils._TF_NUMBER_COMMA);
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfInvalidMaxLength('displayPostion4LvlDlg', 'Vị trí bày bán 4');
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfNegativeNumberCheck('displayPostion4LvlDlg','Vị trí bày bán 4',Utils._TF_NUMBER_COMMA);
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfRequireCheck('statusLvlDlg','Trạng thái',true);
		}		
		if(msg.length > 0){
			$('#errMsgLvlDlg').html(msg).show();
			return false;
		}
		var params = new Object();		
		params.id = $('#selId').val().trim();
		params.levelId = $('#selLevelId').val().trim();
		params.levelCode = $('#levelCodeLvlDlg').val().trim();
		params.levelAmount =  Utils.returnMoneyValue($('#amountLvlDlg').val().trim());
		params.levelNumSKU =  Utils.returnMoneyValue($('#numSKULvlDlg').val().trim());
		params.levelNumProductDisplay =  Utils.returnMoneyValue($('#numProductDisplayLvlDlg').val().trim());
		var checked = $('#chkBoxDisCountDisplayLvlDlg').is(':checked');
		if(checked){
			params.levelDisCountEvenDisplayFailed = 1;
		} else{
			params.levelDisCountEvenDisplayFailed = 0;
		}
		params.levelPercentDisCountPass =  Utils.returnMoneyValue($('#percentDisCountPassLvlDlg').val().trim());
		params.levelPercentDisCountOver =  Utils.returnMoneyValue($('#percentDisCountOverLvlDlg').val().trim());
		params.levelRevenueBonusType = $('#revenueBonusTypeLvlDlg').val();
		params.levelRevenueBonus =  Utils.returnMoneyValue($('#revenueBonusLvlDlg').val().trim());
		params.levelMaxDisCount =  Utils.returnMoneyValue($('#maxDisCountLvlDlg').val().trim());

		params.levelDisplayBonusType = $('#displayBonusTypeLvlDlg').val();
		params.levelDisplayBonus =  Utils.returnMoneyValue($('#displayBonusLvlDlg').val().trim());
		params.levelPercentSupportDisplay =  Utils.returnMoneyValue($('#percentSupportDisplayLvlDlg').val().trim());
		params.levelPaidPrice =  Utils.returnMoneyValue($('#paidPriceLvlDlg').val().trim());
		params.levelMaxSupportDisplay =  Utils.returnMoneyValue($('#maxSupportDisplayLvlDlg').val().trim());
		params.levelDisplayPostion1 =  Utils.returnMoneyValue($('#displayPostion1LvlDlg').val().trim());
		params.levelDisplayPostion2 =  Utils.returnMoneyValue($('#displayPostion2LvlDlg').val().trim());
		params.levelDisplayPostion3 =  Utils.returnMoneyValue($('#displayPostion3LvlDlg').val().trim());
		params.levelDisplayPostion4 =  Utils.returnMoneyValue($('#displayPostion4LvlDlg').val().trim());
		params.levelPaidProductCode = $('#paidProductCodeLvlDlg').val();
		params.levelStatus = $('#statusLvlDlg').val();	
		
		// thuoc tinh mo rong
		if(!Utils.validateAttributeData(params, '#errMsgLvlDlg','.fancybox-inner ')){
			return false;
		}
		var msg = null;
		Utils.addOrSaveData(params, '/catalog/programme-display/save-level', null, 'errMsgLvlDlg', function(data){
			$("#levelGrid").trigger("reloadGrid");
			showSuccessMsg('successMsgLvlDlg',data, function(){
				$.fancybox.close();
			},500);
		}, null, null, null, msg);	
		return false;
	},
	saveProductTab: function(){
		var msg = '';
		$('#errMsgProduct').html('').hide();
		if($('#rbCat').is(':checked')){
			msg = Utils.getMessageOfRequireCheck('catCode','Mã ngành hàng');
			if(msg.length == 0){
				msg = Utils.getMessageOfSpecialCharactersValidate('catCode','Mã ngành hàng');
			}
		}
		if($('#rbSubCat').is(':checked')){
			msg = Utils.getMessageOfRequireCheck('subCatCode','Mã ngành hàng con');
			if(msg.length == 0){
				msg = Utils.getMessageOfSpecialCharactersValidate('subCatCode','Mã ngành hàng con');
			}
		}		
		if($('#rbProduct').is(':checked')){
			msg = Utils.getMessageOfRequireCheck('productCode','Mã sản phẩm');
			if(msg.length == 0){
				msg = Utils.getMessageOfSpecialCharactersValidate('productCode','Mã sản phẩm');
			}
		}	
		if(msg.length > 0){
			$('#errMsgProduct').html(msg).show();
			return false;
		}
		var dataModel = new Object();		
		dataModel.id = $('#selId').val().trim();
		dataModel.productId = $('#selProductId').val().trim();
		dataModel.catCode = $('#catCode').val().trim();
		dataModel.subCatCode = $('#subCatCode').val().trim();		
		dataModel.productCode = $('#productCode').val();		
		Utils.addOrSaveRowOnGrid(dataModel, "/catalog/programme-display/save-product", ProgrammeDisplayCatalog._xhrSave, 'productGrid','errMsgProduct', function(data){	
			$("#productGrid").trigger("reloadGrid");
			$('#catCode').val('');
			$('#subCatCode').val('');
			$('#productCode').val('');
			$('#productName').val('');
			showSuccessMsg('successMsgProduct',data);
		},'productLoading');		
		return false;
	},
	saveQuotaGroup: function(){
		var msg = '';
		$('#errMsgQuotaGroup').html('').hide();
		msg = Utils.getMessageOfRequireCheck('quotaCode','Mã nhóm');
		if(msg.length == 0){
			msg = Utils.getMessageOfSpecialCharactersValidate('quotaCode','Mã nhóm',Utils._CODE);
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfRequireCheck('quotaName','Tên nhóm');
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfSpecialCharactersValidate('quotaName','Tên nhóm');
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfRequireCheck('fromPercent','% Doanh số tối thiểu');// %
																						// Doanh
																						// số
																						// từ
		}
		if(msg.length == 0 && (isNaN($('#fromPercent').val().trim()) || (parseInt($('#fromPercent').val().trim()) < 0 || parseInt($('#fromPercent').val().trim()) > 100))){
			msg = '% Doanh số tối thiểu phải là số nguyên dương <= 100';// %
																		// doanh
																		// số từ
																		// phải
																		// là số
																		// nguyên
																		// dương
																		// <=
																		// 100
			$('#fromPercent').focus(); 
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfRequireCheck('toPercent','% Doanh số tối đa');
		}
		if(msg.length == 0 && (isNaN($('#toPercent').val().trim()) ||parseInt($('#toPercent').val().trim()) <=0)){
			msg = format(msgErr_possitive_integer,'% Doanh số tối đa');
			$('#toPercent').focus();
		}
		if(msg.length == 0 && (isNaN($('#toPercent').val().trim()) || parseInt($('#toPercent').val().trim()) < parseInt($('#fromPercent').val().trim()))){
			msg = "% Doanh số tối thiểu không được lớn hơn % Doanh số tối đa";
			$('#fromPercent').focus();
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfRequireCheck('fieldMap','Chọn trường map',true);
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfRequireCheck('quotaStatus','Trạng thái',true);
		}
		if(msg.length == 0 && $('#toPercent').val().trim().length>8){
			msg = "Chiều dài trường % Doanh số tối đa vượt quá 8 ký tự";
			$('#toPercent').focus();
		}
		if(msg.length == 0 && $('#fromPercent').val().trim().length>8){
			msg = "Chiều dài trường % Doanh số tối thiểu vượt quá 8 ký tự";
			$('#fromPercent').focus();
		}
		if(msg.length > 0){
			$('#errMsgQuotaGroup').html(msg).show();
			return false;
		}
		var dataModel = new Object();		
		dataModel.id = $('#selId').val().trim();
		dataModel.quotaId = $('#selQuotaId').val().trim();
		dataModel.quotaCode = $('#quotaCode').val().trim();
		dataModel.quotaName = $('#quotaName').val().trim();		
		dataModel.fromPercent = $('#fromPercent').val();
		dataModel.toPercent = $('#toPercent').val();
		dataModel.quotaStatus = $('#quotaStatus').val();
		dataModel.productSubCatCode = $('#fieldMap').val();		
		Utils.addOrSaveRowOnGrid(dataModel, "/catalog/programme-display/save-quota-group", ProgrammeDisplayCatalog._xhrSave, 'quotaGrid','errMsgQuotaGroup', function(data){
			$('#selQuotaId').val(data.id);
			ProgrammeDisplayCatalog.cancelQuota();
			showSuccessMsg('successMsgQuotaGroup',data);
		},'quotaLoading');		
		return false;
	},
	getChangedQuotaForm: function(isEdit,groupId){ // them moi' & edit Click
		var params = new Object();
		params.id = groupId;
		Utils.getHtmlDataByAjax(params, '/catalog/programme-display/popupGroup', function(data) {
			var title ='';
			if(isEdit == false){
				title = 'Thêm nhóm chỉ tiêu ';
			} else{
				title = 'Sửa nhóm chỉ tiêu ';
			}
			$.fancybox(data, {
				modal: true,
				title: title,
				afterShow: function () {
					$('.fancybox-inner #quotaStatusF').customStyle();
					$('.fancybox-inner #btnCancelF').bind('click',function(){
						$.fancybox.close();	
					});
					if (isEdit) $('#textA').html("Cập nhật");
					else 
						$('#textA').html("Thêm mới");
					$('#quotaCodeF').focus();
					$('#errMsgQuotaGroupF').html('').hide();
					$(window).resize();
					$.fancybox.update();	
					
					$('.fancybox-inner #quotaPCodeF').val($('#quotaPCode').val().trim());
					$('.fancybox-inner #quotaPNameF').val($('#quotaPName').val().trim());
					$('.fancybox-inner #quotaPCodeF').attr("disabled", "disabled"); 
					$('.fancybox-inner #quotaPNameF').attr("disabled", "disabled"); 
					if (isEdit) $('.fancybox-inner #quotaCodeF').attr("disabled","disabled");
					Utils.loadAttributeData('.fancybox-inner ');
					$('.fancybox-inner .CustomStyleSelectBox').width(115);
					$('.fancybox-inner #btnAddQuotaF').bind('click',function(){
						//
						var msg = ""; var chot = 'quotaCodeF';
						msg = Utils.getMessageOfRequireCheck('quotaCodeF', 'Mã nhóm',false);
						if (msg==""){
							msg = Utils.getMessageOfRequireCheck('quotaNameF','Tên nhóm', false);
							chot = 'quotaNameF';
						}
						if (msg==""){
							msg = Utils.getMessageOfRequireCheck('fromPercentF','%Doanh số đóng góp', false);
							chot = 'fromPercentF';
						}
						if (msg!=""){
							$('.fancybox-inner #errMsgQuotaGroup').html(msg).show();
							$('.fancybox-inner #'+chot).focus();
							return false;
						}
						msg = Utils.getMessageOfSpecialCharactersValidate('quotaCodeF', 'Mã nhóm', Utils._CODE);
						if (msg != ""){
							chot = 'quotaCodeF';
						}
						if (msg ==""){
							msg = Utils.getMessageOfSpecialCharactersValidate('quotaNameF', 'Tên nhóm', Utils._NAME);
							chot = 'quotaNameF';
						}
						if (msg == ""){
							msg = Utils.getMessageOfSpecialCharactersValidateEx('fromPercentF', '%Doanh số đóng góp',Utils._TF_NUMBER_DOT);
							chot = 'fromPercentF';
						}
						if (msg!=""){
							$('.fancybox-inner #errMsgQuotaGroup').html(msg).show();
							$('.fancybox-inner #'+chot).focus();
							return false;
						}
						if (msg==""){
							if (parseInt($('.fancybox-inner #fromPercentF').val().trim()) <= 0) {
								msg = "%Doanh số đóng góp phải lớn hơn 0";
								chot = 'fromPercentF';
							}
						}
						if (msg!=""){
							$('.fancybox-inner #errMsgQuotaGroup').html(msg).show();
							$('.fancybox-inner #'+chot).focus();
							return false;
						}
						if (msg==""){
							msg = Utils.getMessageOfRequireCheck('toPercentF','%Doanh số tối đa', false);
							chot = 'toPercentF';
						}
						if (msg!=""){
							$('.fancybox-inner #errMsgQuotaGroup').html(msg).show();
							$('.fancybox-inner #'+chot).focus();
							return false;
						}
						if (msg == ""){
							msg = Utils.getMessageOfSpecialCharactersValidateEx('toPercentF', '%Doanh số tối đa',Utils._TF_NUMBER_DOT);
							chot = 'toPercentF';
						}
						if (msg!=""){
							$('.fancybox-inner #errMsgQuotaGroup').html(msg).show();
							$('.fancybox-inner #'+chot).focus();
							return false;
						}
						if (msg==""){
							if (parseInt($('.fancybox-inner #toPercentF').val().trim()) <= 0) {
								msg = "%Doanh số tối đa phải lớn hơn 0";
								chot = 'toPercentF';
							}
						}
						if (msg!=""){
							$('.fancybox-inner #errMsgQuotaGroup').html(msg).show();
							$('.fancybox-inner #'+chot).focus();
							return false;
						}
						if ( parseInt($('#fromPercentF').val(),10) > 100)
						{
							$('.fancybox-inner #errMsgQuotaGroup').html("%Doanh số đóng góp lớn 100%").show();
							$('#fromPercentF').focus();
							return false;
						}
						if (parseInt($('#fromPercentF').val(),10) > parseInt($('#toPercentF').val(),10))
						{
							$('.fancybox-inner #errMsgQuotaGroup').html("%Doanh số đóng góp lớn hơn % Doanh số tối đa").show();
							$('#fromPercentF').focus();
							return false;
						}
						if ($('#quotaStatusF').val() == -2) {
							$('.fancybox-inner #errMsgQuotaGroup').html("Mời bạn chọn trạng thái.").show();
							return false;
						}
						var dataModel = new Object();		
						dataModel.id = $('#selId').val().trim();
						dataModel.quotaCode = $('#quotaCodeF').val().trim();
						dataModel.quotaName = $('#quotaNameF').val().trim();		
						dataModel.fromPercent = $('#fromPercentF').val();
						dataModel.toPercent = $('#toPercentF').val();
						dataModel.quotaStatus = $('#quotaStatusF').val();
						dataModel.quotaId = groupId;
						if(!Utils.validateAttributeData(dataModel, '#errMsg' , '.fancybox-inner ')){
							return false;
						}
						Utils.addOrSaveRowOnGrid(dataModel, "/catalog/programme-display/save-quota-group", ProgrammeDisplayCatalog._xhrSave, 'quotaGrid','errMsgQuotaGroup', function(data){
							$('#selQuotaId').val(data.id);
							showSuccessMsg('successMsgQuotaGroup',data);
							$("#quotaGrid").trigger("reloadGrid");
							$.fancybox.close();
						},'quotaLoading');		
					});
				},
				afterClose: function(){
					var curIdFocus = $('#cur_focus').val();
					$('#'+ curIdFocus).focus();
				}
			});
		}, null, 'GET');
	},
	deleteQuotaGroupRow: function(id){
		var dataModel = new Object();
		dataModel.quotaId = id;	
		Utils.deleteSelectRowOnGrid(dataModel, 'nhóm chỉ tiêu', '/catalog/programme-display/delete-quota-group', ProgrammeDisplayCatalog._xhrDel, 'quotaGrid','errMsgQuotaGroup',function(data){
			showSuccessMsg('successMsgQuotaGroup',data);
			ProgrammeDisplayCatalog.cancelQuota();
		},'quotaLoading');
		return false;
	},
	getQuotaGroupSelectedRow: function(rowId){
		var code = $("#quotaGrid").jqGrid ('getCell', rowId, 'displayGroupCode');
		var name = $("#quotaGrid").jqGrid ('getCell', rowId, 'displayGroupName');
		var fromPercent = $("#quotaGrid").jqGrid ('getCell', rowId, 'percentMin');
		var toPercent = $("#quotaGrid").jqGrid ('getCell', rowId, 'percentMax');
		var subCatField = $("#quotaGrid").jqGrid ('getCell', rowId, 'subCatField');		
		var status = $("#quotaGrid").jqGrid ('getCell', rowId, 'status');
		var id = $("#quotaGrid").jqGrid ('getCell', rowId, 'id');
		var statusValue = -2;
		if(status == activeStatusText){
			statusValue = 1;
		} else {
			statusValue = 0;
		}
		setTextboxValue('quotaCode', code);
		setTextboxValue('quotaName', name);
		setTextboxValue('fromPercent', fromPercent);
		setTextboxValue('toPercent', toPercent);
		setSelectBoxValue('fieldMap', subCatField);
		setSelectBoxValue('quotaStatus', statusValue);
		disabled('quotaCode');
		$('#btnSearchQuota').hide();
		$('#btnAddQuota').hide();
		$('#btnEditQuota').show();
		$('#btnCancelQuota').show();
		$('#container2 .RequireStyle').show();
		$('#selQuotaId').val(id);
		$('#quotaName').focus();
		$('#titleQuota').text('Cập nhật nhóm chỉ tiêu');
	},
	cancelQuota: function(){
		$('#quotaCode').removeAttr('disabled','');
		setTextboxValue('quotaCode', '');
		setTextboxValue('quotaName', '');
		setTextboxValue('fromPercent', '');
		setTextboxValue('toPercent', '');
		setSelectBoxValue('fieldMap');
		setSelectBoxValue('quotaStatus', 1);		
		$('#btnEditQuota').hide();
		$('#btnCancelQuota').hide();
		$('#btnSearchQuota').show();
		$('#btnAddQuota').show();
		$('#container2 .RequireStyle').hide();
		$('#selQuotaId').val(0);
		$('#errMsgQuotaGroup').html('').hide();
		ProgrammeDisplayCatalog.searchQuota();
		$('#quotaCode').focus();
		$('#titleQuota').text('Thông tin tìm kiếm nhóm chỉ tiêu');
	},
	saveStaffTab: function(){
		var msg = '';
		$('#errMsgStaff').html('').hide();
		msg = Utils.getMessageOfRequireCheck('shopCode','Mã đơn vị');
		if($('#rbStaff').is(':checked')){
			if(msg.length == 0){
				msg = Utils.getMessageOfRequireCheck('staffCode','Mã nhân viên');
			}
			if(msg.length == 0){
				msg = Utils.getMessageOfSpecialCharactersValidate('staffCode','Mã nhân viên',Utils._CODE);
			}
		}
		if($('#rbStaffType').is(':checked')){
			if(msg.length == 0){
				msg = Utils.getMessageOfRequireCheck('staffType','Loại nhân viên',true);
			}
		}	
		if(msg.length > 0){
			$('#errMsgStaff').html(msg).show();
			return false;
		}
		var dataModel = new Object();		
		dataModel.id = $('#selId').val().trim();
		dataModel.staffTypeId = $('#staffType').val();
		dataModel.staffCode = $('#staffCode').val().trim();
		dataModel.shopCode = $('#shopCode').val().trim();
		Utils.addOrSaveRowOnGrid(dataModel, "/catalog/programme-display/save-staff", ProgrammeDisplayCatalog._xhrSave, 'staffGrid','errMsgStaff', function(data){	
			$("#productGrid").trigger("reloadGrid");
			$('#shopCode').val('');
			$('#shopName').val('');
			$('#staffCode').val('');
			$('#staffName').val('');
			showSuccessMsg('successMsgStaff',data);
			setSelectBoxValue('staffType');
		},'staffLoading');		
		return false;
	},
	deleteStaffRow: function(staffId){
		var dataModel = new Object();
		dataModel.id = $('#selId').val().trim();
		dataModel.staffId = staffId;	
		Utils.deleteSelectRowOnGrid(dataModel, 'nhân viên', '/catalog/programme-display/delete-staff', ProgrammeDisplayCatalog._xhrDel, 'staffGrid','errMsgStaff',function(data){
			showSuccessMsg('successMsgStaff',data);
		},'staffLoading');		
		return false;
	},
	addSubQuotaGroupRow: function(quotaGroupId,rowId, gridId){ // edit CuongND
		var html = $('#productTreeDialog').html();
		$.fancybox(html,
				{
					modal: true,
					title: 'Thêm thông tin sản phẩm',
					afterShow: function(){
						Utils.resetCheckboxStateOfTree();
						Utils.bindAutoSearch();	
						$('.fancybox-inner #treeGroup').hide();
						$('.fancybox-inner #treeQuotaGroup').hide();
						$('.fancybox-inner #btnQ').bind('click',function(){
							ProgrammeDisplayCatalog.selectListProductQuota(quotaGroupId,gridId);
						});
						$('.fancybox-inner #btnSearchProductTree').bind('click',function(){
							ProgrammeDisplayCatalog.searchProductOnTreeQuota();
						});
						$('#productTreeDialog').html('');
						$('.fancybox-inner #tmpNumDisplay').attr('id','numDisplay');
						Utils.bindFormatOnTextfield('numDisplay', Utils._TF_NUMBER);
						var displayId = $('#selId').val();
						var productCode = encodeURI('');
						var productName = encodeURI('');
						ProgrammeDisplayCatalog.loadDataForTreeWithCheckboxExDisplayGroup('/rest/catalog/display-program/productDisplayGroup/list.json?displayProgramId='+ displayId +'&productCode='+ encodeChar(productCode) +'&productName='+ encodeChar(productName) + '&nodeType=cat&catId=0&subCatId=0','productTree');
						ProgrammeDisplayCatalog._mapProduct = new Map();
						$('#productTree').bind("loaded.jstree open_node.jstree close_node.jstree", function (event, data) {
							if($('#productTreeContent').data('jsp') != undefined || $('#productTreeContent').data('jsp') != null) {
								$('#productTreeContent').data('jsp').destroy();
								setTimeout(function(){
									$('#productTreeContent').jScrollPane();
								},500);	
							} else {
								setTimeout(function(){
									$('#productTreeContent').jScrollPane();
								},500);	
							}
						});
						ProgrammeDisplayCatalog.openNodeOnTree('productTree');
					},
					afterClose: function(){
						$('#productTreeDialog').html(html);				
					}
				}
			);
		return false;
	},
	deleteSubQuotaGroupRow: function(id,gridId){
		ProgrammeDisplayCatalog._curGridId = gridId;
		var dataModel = new Object();
		dataModel.subGroupId = id;	
		Utils.deleteSelectRowOnGrid(dataModel, 'sản phẩm', '/catalog/programme-display/delete-sub-display-group', ProgrammeDisplayCatalog._xhrDel, ProgrammeDisplayCatalog._curGridId,'errMsgQuotaGroup',function(data){
			if (data.error == true)
			{
				$('#errMsgQuotaGroup').html(data.errMsg).show();
			}else{
				showSuccessMsg('successMsgQuotaGroup',data);
			}
		},'staffLoading');		
		return false;
	},
	saveCustomer: function(){
		var msg = '';
		$('#errMsg').html('').hide();
		msg = Utils.getMessageOfRequireCheck('customerCode','Mã KH');
		if(msg.length == 0){
			msg = Utils.getMessageOfSpecialCharactersValidate('customerCode','Mã KH');
		}		
		if(msg.length == 0){
			msg = Utils.getMessageOfRequireCheck('level','Mức CTTB',true);
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfRequireCheck('fDate','Từ ngày');
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfInvalidFormatDate('fDate','Từ ngày');
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfRequireCheck('tDate','Đến ngày');
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfInvalidFormatDate('tDate','Đến ngày');
		}
		var fDate = $('#fDate').val();
		var tDate = $('#tDate').val();
		if(msg.length == 0){
			var currentTime = new Date();
			var month = currentTime.getMonth() + 1;
			var day = currentTime.getDate();
			var year = currentTime.getFullYear();		
			if(!Utils.compareDate(fDate, tDate)){
				msg = msgErr_fromdate_greater_todate;				
			}
		}		
		if(msg.length > 0){
			$('#errMsg').html(msg).show();
			return false;
		}
		var dataModel = new Object();		
		dataModel.id = $('#id').val().trim();
		dataModel.displayCustomerId = $('#selId').val().trim();
		dataModel.customerCode = $('#customerCode').val().trim();
		dataModel.staffId = $('#staffId').val().trim();		
		dataModel.levelId = $('#level').val();
		dataModel.shopCode = $('#shopCode').val();
		dataModel.fromDate = $('#fDate').val().trim();
		dataModel.toDate = $('#tDate').val().trim();
		Utils.addOrSaveData(dataModel, "/catalog/programme-display/customer/save", ProgrammeDisplayCatalog._xhrSave, null, function(data){	
			$("#customerGrid").trigger("reloadGrid");
			ProgrammeDisplayCatalog.cancelCustomerForm();			
		});		
		return false;
	},
	getSelectedCustomerRow: function(rowId, level){
		$('#btnAddCustomer').hide();
		$('#btnEditCustomer').show();
		$('#btnCancelCustomer').show();
		var code = $("#customerGrid").jqGrid ('getCell', rowId, 'customer.shortCode');
		var name = $("#customerGrid").jqGrid ('getCell', rowId, 'customer.customerName');
		var fromDate = $("#customerGrid").jqGrid ('getCell', rowId, 'fromDate');
		var toDate = $("#customerGrid").jqGrid ('getCell', rowId, 'toDate');
		var id = $("#customerGrid").jqGrid ('getCell', rowId, 'id');
		$('#selId').val(id);
		setSelectBoxValue('level', level);
		setTextboxValue('customerCode', code);
		setTextboxValue('customerName', name);
		setTextboxValue('fDate', fromDate);
		setTextboxValue('tDate', toDate);		
	},
	cancelCustomerForm: function(){
		$('#selId').val(0);
		$('#btnEditCustomer').hide();
		$('#btnCancelCustomer').hide();
		$('#btnAddCustomer').show();
		setSelectBoxValue('level');
		setTextboxValue('customerCode');
		setTextboxValue('customerName');
		setTextboxValue('fDate');
		setTextboxValue('tDate');
	},
	deleteCustomerRow: function(customerId){
		var dataModel = new Object();
		dataModel.customerId = customerId;	
		Utils.deleteSelectRowOnGrid(dataModel, 'khách hàng', '/catalog/programme-display/customer/delete', ProgrammeDisplayCatalog._xhrDel, 'customerGrid','errMsgCustomer',function(){
			$("#staffGrid").dataGrid("reload");
		});		
		return false;
	},
	activeOthersTab: function(){
		$('#tabActive2').removeClass('Disable');
		$('#tabActive3').removeClass('Disable');
		$('#tabActive4').removeClass('Disable');
		$('#tabActive5').removeClass('Disable');
		$('#tabActive6').removeClass('Disable');
		$('#tabActive7').removeClass('Disable');
		$('#tab2').bind('click',ProgrammeDisplayCatalog.showQuotaGroupTab);
		$('#tab3').bind('click',ProgrammeDisplayCatalog.showLevelTab);
		$('#tab4').bind('click',ProgrammeDisplayCatalog.showProductTab);
		$('#tab5').bind('click',ProgrammeDisplayCatalog.showStaffTab);
		$('#tab6').bind('click',ProgrammeDisplayCatalog.showDisplayProgramChangeTab);
		$('#tab7').bind('click',ProgrammeDisplayCatalog.showShopTab);
		return false;
	},
	showDialogCreateProduct: function(){
		var html = $('#productTreeDialog').html();
		$.fancybox(html,
				{
					modal: true,
					title: 'Thêm thông tin sản phẩm',
					afterShow: function(){
						Utils.resetCheckboxStateOfTree();
						Utils.bindAutoSearch();	
						$('#productTreeDialog').html('');
						$('.fancybox-inner #tmpNumDisplay').attr('id','numDisplay');
						
						$('.fancybox-inner #productToDate').attr('id','dpProductToDate');
						$('.fancybox-inner #productFromDate').attr('id','dpProductFromDate');
						
						setDateTimePicker('dpProductToDate');
						setDateTimePicker('dpProductFromDate');
						
						$('.fancybox-inner #btnSearchProductTree').bind('click',function(){
							ProgrammeDisplayCatalog.searchProductOnTree();
						});
						
						Utils.bindFormatOnTextfield('numDisplay', Utils._TF_NUMBER);
						// Cuong ND edit code
						// -----------------------------------
						$('.fancybox-inner #btnQ').bind('click',function(){
							ProgrammeDisplayCatalog.selectListProduct(false,null);
						});
						// --------------------------------------------------------------
						var displayId = $('#selId').val();
						var productCode = encodeURI('');
						var productName = encodeURI('');
						ProgrammeDisplayCatalog.loadDataForTreeWithCheckboxEx('/rest/catalog/display-program/product/list.json?displayProgramId='+ displayId +'&productCode='+ encodeChar(productCode) +'&productName='+ encodeChar(productName) + '&nodeType=cat&catId=0&subCatId=0','productTree');
						ProgrammeDisplayCatalog._mapProduct = new Map();
						$('#productTree').bind("loaded.jstree open_node.jstree close_node.jstree", function (event, data) {
							if($('#productTreeContent').data('jsp') != undefined || $('#productTreeContent').data('jsp') != null) {
								$('#productTreeContent').data('jsp').destroy();
								setTimeout(function(){
									$('#productTreeContent').jScrollPane();
								},500);	
							} else {
								setTimeout(function(){
									$('#productTreeContent').jScrollPane();
								},500);	
							}
							// $('#productTreeContent
							// .jspContainer').css('width','500px');
						});
						ProgrammeDisplayCatalog.openNodeOnTree('productTree');
					},
					afterClose: function(){
						$('#productTreeDialog').html(html);				
					}
				}
			);
		return false;
	},
	showDialogCreateLevel: function(isEdit,selLevelId){
		var params = new Object();
		params.levelId = selLevelId;
		$('#selLevelId').val(selLevelId);
		Utils.getHtmlDataByAjax(params, '/catalog/programme-display/search-level-detail', function(data) {
			var html ='<div class="GeneralDialog General5Dialog"><div class="DialogAddLevelCTTB" id="contentDetailDiv"></div></div>';
			var title ='';
			if(isEdit == false){
				title = 'Thêm mức cho CTTB ';
			} else{
				title = 'Chi tiết mức CTTB ';
			}
			$.fancybox(html, {
				modal: true,
				title: title,
				autoResize: false,
				afterShow: function () {
					$('.fancybox-inner #contentDetailDiv').html(data);
					ProgrammeDisplayCatalog.onLoadAfterShowLevelDialog();
					
					$(window).resize();
					$.fancybox.update();	
				},
				afterClose: function(){
					$('#selLevelId').val('0');
				}
			});
		}, null, 'GET');
	},
	changePaidProduct: function(){
		var paidProductCode = $('#paidProductCodeLvlDlg').val();
		if(!isNullOrEmpty(paidProductCode)) {
			setSelectBoxValue('revenueBonusTypeLvlDlg',1);
			setSelectBoxValue('displayBonusTypeLvlDlg',1);
			enableSelectbox('revenueBonusTypeLvlDlg');
			enableSelectbox('displayBonusTypeLvlDlg');
		}else{
			setSelectBoxValue('revenueBonusTypeLvlDlg',0);
			setSelectBoxValue('displayBonusTypeLvlDlg',0);
			disableSelectbox('revenueBonusTypeLvlDlg');
			disableSelectbox('displayBonusTypeLvlDlg');
		}
	},
	onLoadAfterShowLevelDialog :function(){
		Utils.bindFormatOntextfieldCurrencyFor('amountLvlDlg',Utils._TF_NUMBER_COMMA);
		Utils.bindFormatOntextfieldCurrencyFor('numSKULvlDlg',Utils._TF_NUMBER_COMMA);
		Utils.bindFormatOntextfieldCurrencyFor('numProductDisplayLvlDlg',Utils._TF_NUMBER_COMMA);
		Utils.bindFormatOntextfieldCurrencyFor('percentDisCountPassLvlDlg',Utils._TF_NUMBER_COMMA);
		Utils.bindFormatOnTextfield('percentDisCountOverLvlDlg',Utils._TF_NUMBER);
		Utils.bindFormatOntextfieldCurrencyFor('revenueBonusLvlDlg',Utils._TF_NUMBER_COMMA);
		Utils.bindFormatOntextfieldCurrencyFor('maxDisCountLvlDlg',Utils._TF_NUMBER_COMMA);
		Utils.bindFormatOntextfieldCurrencyFor('displayBonusLvlDlg',Utils._TF_NUMBER_COMMA);
		Utils.bindFormatOnTextfield('percentSupportDisplayLvlDlg',Utils._TF_NUMBER);
		Utils.bindFormatOntextfieldCurrencyFor('paidPriceLvlDlg',Utils._TF_NUMBER_COMMA);
		Utils.bindFormatOntextfieldCurrencyFor('maxSupportDisplayLvlDlg',Utils._TF_NUMBER_COMMA);
		Utils.bindFormatOntextfieldCurrencyFor('displayPostion1LvlDlg',Utils._TF_NUMBER_COMMA);
		Utils.bindFormatOntextfieldCurrencyFor('displayPostion2LvlDlg',Utils._TF_NUMBER_COMMA);
		Utils.bindFormatOntextfieldCurrencyFor('displayPostion3LvlDlg',Utils._TF_NUMBER_COMMA);
		Utils.bindFormatOntextfieldCurrencyFor('displayPostion4LvlDlg',Utils._TF_NUMBER_COMMA);
		
		Utils.loadAttributeData('.fancybox-inner ','AddLevel1Form');
		var paidProductCode = $('#paidProductCodeLvlDlg').val();
		if(!isNullOrEmpty(paidProductCode)) {
			enableSelectbox('revenueBonusTypeLvlDlg');
			enableSelectbox('displayBonusTypeLvlDlg');
		}else{
			disableSelectbox('revenueBonusTypeLvlDlg');
			disableSelectbox('displayBonusTypeLvlDlg');
		}
		$('#revenueBonusTypeLvlDlg').customStyle();
		$('#displayBonusTypeLvlDlg').customStyle();
		$('#statusLvlDlg').customStyle();
	},
	searchProductOnTree: function(){	
		$('#productTree li').each(function(){
			var _id = $(this).attr('id');
			var type = $(this).attr('contentitemid');
			if($(this).hasClass('jstree-checked') && type=='product'){
				ProgrammeDisplayCatalog._mapProduct.put(_id,_id);		
			}else {
				ProgrammeDisplayCatalog._mapProduct.remove(_id);
			}
		});
		var displayId = $('#selId').val();
		if(displayId==undefined || displayId==null){
			displayId = 0;
		}
		var productCode = encodeChar($('#productCodeDlg').val().trim());
		var productName = encodeChar($('#productNameDlg').val().trim());
		$('#productTreeContent').data('jsp').destroy();
		ProgrammeDisplayCatalog.loadDataForTreeWithCheckboxEx('/rest/catalog/display-program/product/list.json?displayProgramId='+ displayId +'&productCode='+ encodeChar(productCode) +'&productName='+ encodeChar(productName) + '&nodeType=cat&catId=0&subCatId=0','productTree');
		setTimeout(function() {
			$('#productTreeContent').jScrollPane();
			$('#productTree').bind("loaded.jstree open_node.jstree close_node.jstree", function (event, data) {
				// Utils.applyCheckboxStateOfTree();
				if($('#productTreeContent').data('jsp') != undefined || $('#productTreeContent').data('jsp') != null) {
					$('#productTreeContent').data('jsp').destroy();
					setTimeout(function(){
						$('#productTreeContent').jScrollPane();
					},500);	
				} else {
					setTimeout(function(){
						$('#productTreeContent').jScrollPane();
					},500);	
				}
				ProgrammeDisplayCatalog.getCheckboxStateOfTreeEx('productTree',ProgrammeDisplayCatalog._mapProduct,"product",'productTree li');
			});
			ProgrammeDisplayCatalog.getCheckboxStateOfTreeEx('productTree',ProgrammeDisplayCatalog._mapProduct,"product",'productTree li');
		}, 500);
		
	},
	searchProductOnTreeQuota: function(){	
		$('#productTree li').each(function(){
			var _id = $(this).attr('id');
			var type = $(this).attr('contentitemid');
			if($(this).hasClass('jstree-checked') && type=='product'){
				ProgrammeDisplayCatalog._mapProduct.put(_id,_id);		
			}else {
				ProgrammeDisplayCatalog._mapProduct.remove(_id);
			}
		});
		var displayId = $('#selId').val();
		var productCode = encodeChar($('#productCodeDlg').val().trim());
		var productName = encodeChar($('#productNameDlg').val().trim());
		$('#productTreeContent').data('jsp').destroy();
		ProgrammeDisplayCatalog.loadDataForTreeWithCheckboxExDisplayGroup('/rest/catalog/display-program/productDisplayGroup/list.json?displayProgramId='+ displayId +'&productCode='+ encodeChar(productCode) +'&productName='+ encodeChar(productName) + '&nodeType=cat&catId=0&subCatId=0','productTree');
		setTimeout(function() {
			$('#productTreeContent').jScrollPane();
			$('#productTree').bind("loaded.jstree open_node.jstree close_node.jstree", function (event, data) {
				// Utils.applyCheckboxStateOfTree();
				if($('#productTreeContent').data('jsp') != undefined || $('#productTreeContent').data('jsp') != null) {
					$('#productTreeContent').data('jsp').destroy();
					setTimeout(function(){
						$('#productTreeContent').jScrollPane();
					},500);	
				} else {
					setTimeout(function(){
						$('#productTreeContent').jScrollPane();
					},500);	
				}
				ProgrammeDisplayCatalog.getCheckboxStateOfTreeEx('productTree',ProgrammeDisplayCatalog._mapProduct,"product",'productTree li');
			});
			ProgrammeDisplayCatalog.getCheckboxStateOfTreeEx('productTree',ProgrammeDisplayCatalog._mapProduct,"product",'productTree li');
		}, 500);
		
	},
	getCheckboxStateOfTreeEx:function(treeId,MAP,nodeType,selector){
		for(var i=0;i<MAP.size();++i){
			var _obj = MAP.get(MAP.keyArray[i]);					
			$('#' + selector).each(function(){
				var _id = $(this).attr('id');
				var type = $(this).attr('contentitemid');
				if(_id==_obj && nodeType==type ){
					$(this).removeClass('jstree-unchecked');
					$(this).addClass('jstree-checked');
				}
			});
		}
	},	
	selectListProduct: function(flagQouta,groupId){
		$('#errMsgProductDlg').html('').hide();
		$('#successMsgProductDlg').html('').hide();
		$.fancybox.update();
		var arrId = new Array();
		/*
		 * $('.jstree-leaf').each(function(){
		 * if($(this).hasClass('jstree-checked')){
		 * arrId.push($(this).attr('id')); } });
		 * $('li[classstyle=product]').each(function(){
		 * if($(this).hasClass('jstree-checked')){
		 * arrId.push($(this).attr('id')); } });
		 */
		$('#productTree li').each(function(){
			var _id = $(this).attr('id');
			var type = $(this).attr('contentitemid');
			if($(this).hasClass('jstree-checked') && type=='product'){
				ProgrammeDisplayCatalog._mapProduct.put(_id,_id);		
			}else{
				ProgrammeDisplayCatalog._mapProduct.remove(_id);	
			}			
		});
		for(var i=0;i<ProgrammeDisplayCatalog._mapProduct.size();++i){
			var _obj = ProgrammeDisplayCatalog._mapProduct.get(ProgrammeDisplayCatalog._mapProduct.keyArray[i]);
			arrId.push(_obj);
		}
		var msg = '';
		if(arrId.length == 0){
			msg = format(msgErr_required_choose_format,'Sản phẩm ');
		}		
		var	fDate = $('#dpProductFromDate').val().trim();
		var	tDate = $('#dpProductToDate').val().trim();
		if(msg.length == 0){
			msg = Utils.getMessageOfRequireCheck('dpProductFromDate', 'Từ ngày');
		}		
		if(msg.length==0){
			msg = Utils.getMessageOfInvalidFormatDate('dpProductFromDate', 'Từ ngày');
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfRequireCheck('dpProductToDate', 'Đến ngày');
		}
		if(msg.length==0){
			msg = Utils.getMessageOfInvalidFormatDate('dpProductToDate', 'Đến ngày');
		}
		if(msg.length==0){			
			if(!Utils.compareDate(fDate, tDate)){
				msg = msgErr_fromdate_greater_todate;	
				$('#dpProductFromDate').focus();
			}
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfRequireCheck('numDisplay', 'Số lượng trưng bày');
		}
		if(msg.length == 0 && (isNaN($('#numDisplay').val().trim()) || parseInt($('#numDisplay').val().trim()) <=0)){
			msg = format(msgErr_possitive_integer,'Số lượng trưng bày');
			$('#numDisplay').focus();
		}
		if(msg.length > 0){
			$('#errMsgProductDlg').html(msg).show();
			$.fancybox.update();
			return false;
		}
		var moreCat = 0;		
		$('.jstree-no-icons').children().each(function(){
			if($(this).hasClass('jstree-checked') || $(this).hasClass('jstree-undetermined')){
				moreCat++;
			}
		});
		var num = $('#numDisplay').val().trim();
		if(num.length>8){
			$('#errMsgProductDlg').html('Giá trị số lượng quá lớn').show();
			return false;
		}
		var displayId = $('#selId').val();
		var dataModel = new Object();		
		dataModel.lstProductId = arrId;
		dataModel.numDisplay = num;
		dataModel.id = displayId;
		dataModel.fromDate = fDate;
		dataModel.toDate = tDate;
		if(moreCat>1){
			$.messager.confirm('Xác nhận', 'Bạn đang chọn các sản phẩm hơn 1 ngành hàng. Bạn có muốn lưu thông tin không?', function(r){
				if (r){
					Utils.saveData(dataModel, '/catalog/programme-display/product/save', ProgrammeDisplayCatalog._xhrSave, 'errMsgProductDlg', function(data){
						$('#successMsgProductDlg').html('Lưu dữ liệu thành công').show();
						$('#productCodeDlg').val('');
						$('#productNameDlg').val('');
						$('#numDisplay').val('');			
						Utils.resetCheckboxStateOfTree();
						ProgrammeDisplayCatalog.searchProductOnTree();
						$('#productCode').val('');
						$('#productName').val('');
						$('#quantity').val('');
						ProgrammeDisplayCatalog.searchProduct();
						$('#quotaGrid').collapseSubGridRow(ProgrammeDisplayCatalog._curRowId);
						$('#quotaGrid').expandSubGridRow(ProgrammeDisplayCatalog._curRowId);
						$.fancybox.update();
						if (flagQouta==false) setTimeout(function(){$.fancybox.close();}, 1000);
						$("#productGrid").trigger("reloadGrid");
					}, null, '.fancybox-inner');					
				}
			});
		} else {
			Utils.addOrSaveData(dataModel, '/catalog/programme-display/product/save', ProgrammeDisplayCatalog._xhrSave, 'errMsgProductDlg', function(data){
				$('#successMsgProductDlg').html('Lưu dữ liệu thành công').show();
				$('#productCodeDlg').val('');
				$('#productNameDlg').val('');
				$('#numDisplay').val('');
				Utils.resetCheckboxStateOfTree();
				ProgrammeDisplayCatalog.searchProductOnTree();
				$('#productCode').val('');
				$('#productName').val('');
				$('#quantity').val('');
				ProgrammeDisplayCatalog.searchProduct();
				$('#quotaGrid').collapseSubGridRow(ProgrammeDisplayCatalog._curRowId);
				$('#quotaGrid').expandSubGridRow(ProgrammeDisplayCatalog._curRowId);
				$.fancybox.update();
				if (flagQouta==false) setTimeout(function(){$.fancybox.close();}, 1000);
				$("#productGrid").trigger("reloadGrid");
			}, null, '.fancybox-inner', null, null);
		}
		return false;
	},
	showUpdateProductDialog: function(rowId){
		var html = $('#productDetailTreeDialog').html();
		$.fancybox(html,
				{
					modal: true,
					title: 'Cập nhật thông tin sản phẩm',					
					afterShow: function(){
						$('#productDetailTreeDialog').html('');
						$('#errMsgProductDetailDlg').html('').hide();
						$('#successMsgProductDetailDlg').html('').hide();
						Utils.bindAutoSearch();													
						$('.fancybox-inner #tmpNumDisplayDetail').attr('id','numDisplay');
						Utils.bindFormatOnTextfield('numDisplay', Utils._TF_NUMBER);
						$('#numDisplay').focus();
						var name = $("#productGrid").jqGrid ('getCell', rowId, 'product.productName');
						var id = $("#productGrid").jqGrid ('getCell', rowId, 'product.id');						
						var quantity = $("#productGrid").jqGrid ('getCell', rowId, 'quantity');	
						var fromDate = $("#productGrid").jqGrid ('getCell', rowId, 'fromDate');	
						var toDate = $("#productGrid").jqGrid ('getCell', rowId, 'toDate');
						var detailId = $("#productGrid").jqGrid ('getCell', rowId, 'id');
						$('#productNameDetailDlg').val(name);
						$('#productIdDetailDlg').val(id);
						$('#numDisplay').val(quantity);
						
						$('.fancybox-inner #TmproductDetailFromDate').attr('id','productDetailFromDate');
						$('.fancybox-inner #TmproductDetailToDate').attr('id','productDetailToDate');
						$('.fancybox-inner #productDetailFromDate').val(fromDate);
						$('.fancybox-inner #productDetailToDate').val(toDate);
						$('.fancybox-inner #displayProgramDetailId').val(detailId);
						setDateTimePicker('productDetailFromDate');
						setDateTimePicker('productDetailToDate');
					},
					afterClose: function(){
						$('#productDetailTreeDialog').html(html);						
					}
				}
			);
		return false;
	},
	saveDetailProduct: function(){
		$('#errMsgProductDetailDlg').html('').hide();
		$('#successMsgProductDetailDlg').html('').hide();
		$.fancybox.update();
		var msg = '';		
		msg = Utils.getMessageOfRequireCheck('numDisplay', 'Số lượng trưng bày');		
		if(msg.length == 0 && (isNaN($('#numDisplay').val().trim()) || parseInt($('#numDisplay').val().trim()) <=0)){
			msg = format(msgErr_possitive_integer,'Số lượng trưng bày');
			$('#numDisplay').focus();
		}	
		var fromDate = $('#productDetailFromDate').val().trim();
		var toDate = $('#productDetailToDate').val().trim(); 		
		
		if(msg.length == 0){
			msg = Utils.getMessageOfRequireCheck('productDetailFromDate', 'Từ ngày');
		}		
		if(msg.length==0){
			msg = Utils.getMessageOfInvalidFormatDate('productDetailFromDate', 'Từ ngày');
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfRequireCheck('productDetailToDate', 'Đến ngày');
		}
		if(msg.length==0){
			msg = Utils.getMessageOfInvalidFormatDate('productDetailToDate', 'Đến ngày');
		}
		if(msg.length==0){			
			if(!Utils.compareDate(fromDate, toDate)){
				msg = msgErr_fromdate_greater_todate;	
				$('#productDetailFromDate').focus();
			}
		}
		if(msg.length > 0){
			$('#errMsgProductDetailDlg').html(msg).show();
			$.fancybox.update();
			return false;
		}
		var num = $('#numDisplay').val().trim();
		if(num.length>8){
			$('#errMsgProductDetailDlg').html('Giá trị số lượng quá lớn ').show();
			return false;
		}
		var displayId = $('#selId').val();
		var arrId = new Array();
		arrId.push($('#productIdDetailDlg').val());
		var dataModel = new Object();
		dataModel.lstProductId = arrId;
		dataModel.numDisplay = num;
		dataModel.id = displayId;
		
		dataModel.fromDate = fromDate;
		dataModel.toDate = toDate;
		dataModel.detailId = $('#displayProgramDetailId').val().trim();
		Utils.addOrSaveData(dataModel, '/catalog/programme-display/product/save', ProgrammeDisplayCatalog._xhrSave, 'errMsgProductDetailDlg', function(data){			
			$('#successMsgProductDetailDlg').html('Lưu dữ liệu thành công').show();
			$.fancybox.update();
			$('#productCode').val('');
			$('#productName').val('');
			$('#quantity').val('');
			ProgrammeDisplayCatalog.searchProduct();
			setTimeout(function(){$.fancybox.close();}, 1000);
		}, null, '.fancybox-inner', null, null);
		return false;
	},
	showDialogCreateStaff: function(){
		var html = $('#staffTreeDialog').html();
		$.fancybox(html,
				{
					modal: true,
					title: 'Thêm nhân viên phát triển khách hàng',
					afterShow: function(){
						Utils.resetCheckboxStateOfTree();
						Utils.bindAutoSearch();	
						$('#staffTreeDialog').html('');
						$('#productCodeDlg').focus();
						$('#staffCodeDlg').focus();
						var displayId = $('#selId').val();						
						if(displayId==undefined || displayId==null ){
							displayId = 0;
						}
						var staffCode = encodeURI('');
						var staffName = encodeURI('');
						ProgrammeDisplayCatalog.loadDataForTreeWithCheckboxEx('/rest/catalog/display-program/staff/list.json?displayProgramId='+ displayId +'&staffCode='+ encodeChar(staffCode) +'&staffName='+ encodeChar(staffName),'staffTree', true);
						$('#staffTree').bind("loaded.jstree open_node.jstree close_node.jstree", function (event, data) {
							var tm = setTimeout(function() {
								$('#staffTreeContent').jScrollPane();
							}, 500);							
						});
						ProgrammeDisplayCatalog.openNodeOnTree('staffTree');
						ProgrammeDisplayCatalog._mapStaff = new Map();						
						 // $(window).resize();
						  var tm = setTimeout(function(){
							  $('.fancybox-wrap.fancybox-desktop.fancybox-type-html.fancybox-opened').css('center',document.documentElement.scrollTop +  $(window).height()/2);
							  window.scrollTo(0,0);
						  }, 500);
					},					
					afterClose: function(){
						$('#staffTreeDialog').html(html);						
					}
				}
			);
		return false;
	},
	searchStaffOnTree: function(){
		$('#staffTree .jstree-checked').each(function(){
			var _id = $(this).attr('id');
			var type = $(this).attr('contentitemid');
			if(type==2){
				ProgrammeDisplayCatalog._mapStaff.put(_id,_id);	
			}else{				
				ProgrammeDisplayCatalog._mapStaff.remove(_id);		
			}
		});
		var displayId = $('#selId').val();
		var staffCode = encodeChar($('#staffCodeDlg').val().trim());
		var staffName = encodeChar($('#staffNameDlg').val().trim());
		ProgrammeDisplayCatalog.loadDataForTreeWithCheckboxEx('/rest/catalog/display-program/staff/list.json?displayProgramId='+ displayId +'&staffCode='+ encodeChar(staffCode) +'&staffName='+ encodeChar(staffName),'staffTree', true);
		
		setTimeout(function() {	
			$('#staffTreeContent').jScrollPane();
			$('#staffTree').bind("loaded.jstree open_node.jstree close_node.jstree", function (event, data) {
				if($('#staffTreeContent').data('jsp') != undefined || $('#staffTreeContent').data('jsp') != null) {
					$('#staffTreeContent').data('jsp').destroy();
					setTimeout(function(){
						$('#staffTreeContent').jScrollPane();
					},500);	
				} else {
					setTimeout(function(){
						$('#staffTreeContent').jScrollPane();
					},500);	
				}
				ProgrammeDisplayCatalog.getCheckboxStateOfTreeEx('staffTree',ProgrammeDisplayCatalog._mapStaff,2,'staffTree li');
			});
			ProgrammeDisplayCatalog.getCheckboxStateOfTreeEx('staffTree',ProgrammeDisplayCatalog._mapStaff,2,'staffTree li');
			$('#staffTreeContent .jspPane').css('width','486px');
		}, 500);
		ProgrammeDisplayCatalog.openNodeOnTree('staffTree');		
	},
	selectListStaff: function(){
		$('#errMsgStaffDlg').html('').hide();
		$('#successMsgStaffDlg').html('').hide();
		$.fancybox.update();
		var arrId = new Array();
		$('#staffTree li').each(function(){
			var _id = $(this).attr('id');
			var type = $(this).attr('contentitemid');
			if($(this).hasClass('jstree-checked') && type==2){
				ProgrammeDisplayCatalog._mapStaff.put(_id,_id);		
			}else{
				ProgrammeDisplayCatalog._mapStaff.remove(_id);	
			}
		});
		for(var i=0;i<ProgrammeDisplayCatalog._mapStaff.size();++i){
			var _obj = ProgrammeDisplayCatalog._mapStaff.get(ProgrammeDisplayCatalog._mapStaff.keyArray[i]);
			arrId.push(_obj);
		}
		
		var msg = '';
		if(arrId.length == 0){
			msg = format(msgErr_required_choose_format,'Nhân viên theo cây đơn vị');
		}		
		if(msg.length > 0){
			$('#errMsgStaffDlg').html(msg).show();
			$.fancybox.update();
			return false;
		}		
		var displayId = $('#selId').val();
		var dataModel = new Object();		
		dataModel.lstStaffId = arrId;
		// dataModel.lstStaffId = ProgrammeDisplayCatalog._arrListStaffItem;
		dataModel.id = displayId;
		Utils.addOrSaveData(dataModel, '/catalog/programme-display/staff/save', ProgrammeDisplayCatalog._xhrSave, 'errMsgStaffDlg', function(data){
			$('#successMsgStaffDlg').html('Lưu dữ liệu thành công').show();
			$('#staffCodeDlg').val('');
			$('#staffNameDlg').val('');		
			Utils.resetCheckboxStateOfTree();
			ProgrammeDisplayCatalog._arrListStaffItem = new Array();
			ProgrammeDisplayCatalog.searchStaffOnTree();		
			$('#staffCode').val('');
			$('#staffName').val('');
			ProgrammeDisplayCatalog.searchStaff();
			$.fancybox.update();			
			setTimeout(function(){$.fancybox.close();}, 1000);
		}, null, '.fancybox-inner', null, null);
		return false;
	},
	showDialogCreateCustomer: function(){
		var html = $('#customerTreeDialog').html();
		$.fancybox(html,
				{
					modal: true,
					title: 'Thêm khách hàng',
					afterShow: function(){
						Utils.resetCheckboxStateOfTree();
						Utils.bindAutoSearch();	
						$('#customerTreeDialog').html('');
						$('.fancybox-inner #fromDateCusDialogTmp').attr('id','fromDateCusDialog');
						$('.fancybox-inner #toDateCusDialogTmp').attr('id','toDateCusDialog');
						setDateTimePicker('fromDateCusDialog');
						setDateTimePicker('toDateCusDialog');
						$('#customerCodeDlg').focus();
						$('.fancybox-inner #levelProgramDisplayDialogTmp').attr('id','levelProgramDisplayDialog');
						$('#levelProgramDisplayDialog').addClass('MySelectBoxClass');
						$('#levelProgramDisplayDialog').customStyle();						
						var displayId = $('#selId').val();
						$.getJSON('/rest/catalog/display-program/'+ displayId +'/level/list.json', function(data){
							var arrHtml = new Array();
							arrHtml.push('<option value="-2">--- Chọn mức CTTB ---</option>');
							for(var i=0;i<data.length;i++){
								arrHtml.push('<option value="'+ data[i].value +'">'+ Utils.XSSEncode(data[i].name) +'</option>');
							}							
							$('#levelProgramDisplayDialog').html(arrHtml.join(""));							
							$('#levelProgramDisplayDialog').change();
						});
						var staffMapId = $('#selStaffId').val();
						var customerCode = encodeURI('');
						var customerName = encodeURI('');
						var shopCode = encodeChar($('#vnmShopCode').val().trim().trim());
						var URL = '/rest/catalog/display-program/customer/list.json?staffMapId='+ staffMapId +'&customerCode='+ encodeChar(customerCode) +'&customerName='+ encodeChar(customerName)+'&shopCode='+encodeChar(shopCode);
						ProgrammeDisplayCatalog.loadDataForTreeWithCheckbox(URL,'customerTree');
						$('#customerTree').bind("loaded.jstree open_node.jstree close_node.jstree", function (event, data) {							
							var tm = setTimeout(function() {
								$('#customerTreeContent').jScrollPane();
							}, 500);
							
						});		
						ProgrammeDisplayCatalog._mapCustomer = new Map();
						ProgrammeDisplayCatalog.openNodeOnTree('customerTree');							
						var tm = setTimeout(function(){
							$('.fancybox-wrap.fancybox-desktop.fancybox-type-html.fancybox-opened').css('center',document.documentElement.scrollTop +  $(window).height()/2);
							window.scrollTo(0,0);
						}, 500);

					},
					afterClose: function(){
						$('#customerTreeDialog').html(html);						
					}
				}
			);
		return false;
	},
	openNodeOnTree:function(treeId){
		$('#' + treeId).bind("open_node.jstree", function (event, data) {
			var parentId = data.inst._get_node(data.rslt.obj);
			var parentNode = parentId.attr('id');
			var child = data.inst._get_children(data.rslt.obj);
			var childNode = child.attr('id');
			if(childNode==parentNode){     
			   data.inst.delete_node(child,true);
		   }
		});	
	},
	viewCustomerByStaff: function(staffId,rowId){
		$('#selStaffId').val(staffId);				
		var name = $("#staffGrid").jqGrid ('getCell', rowId, 'staff.staffName');	
		var shopCode = $("#staffGrid").jqGrid ('getCell', rowId, 'staff.shop.shopCode');
		$('#vnmShopCode').val(shopCode);
		$('#titleCustomer').text('Thông tin về khách hàng tham gia CTTB do NV ' + Utils.XSSEncode(name) + ' phát triển');
		$('#customerContainer').show();	
		var displayId = $('#selId').val();
		$.getJSON('/rest/catalog/display-program/'+ displayId +'/level/list.json', function(data){
			var arrHtml = new Array();
			arrHtml.push('<option value="-2">--- Chọn mức CTTB ---</option>');
			for(var i=0;i<data.length;i++){
				arrHtml.push('<option value="'+ data[i].value +'">'+ Utils.XSSEncode(data[i].name) +'</option>');
			}							
			$('#levelProgramDisplay').html(arrHtml.join(""));			
			$('#levelProgramDisplay').change();
		});
		$("#customerGrid").jqGrid('GridUnload');
		$("#customerGrid").jqGrid({
			url: ProgrammeDisplayCatalog.getCustomerGridUrl('','','','','-2'),
		  colModel:[		
		    {name:'displayStaffMap.staff.shop.shopCode',index: 'shopCode', label: 'Mã đơn vị', width: 80, sortable:false,resizable:false , align: 'left', formatter: function(value, row, index) {
				return Utils.XSSEncode(value);
			}},		    
		    {name:'displayProgramLevel.levelCode',index: 'levelCode', label: 'Mức CT', width: 80, sortable:false,resizable:false , align: 'left', formatter: function(value, row, index) {
				return Utils.XSSEncode(value);
			}},	    
		    {name:'customer.shortCode',index: 'shortCode', label: 'Mã KH', width: 80, sortable:false,resizable:false , align: 'left', formatter: function(value, row, index) {
				return Utils.XSSEncode(value);
			}},
		    {name:'customer.customerName',index: 'customer.customerName', label: 'Tên KH', sortable:false,resizable:false, align: 'left' , formatter: function(value, row, index) {
				return Utils.XSSEncode(value);
			}},
		    {name:'fromDate',index: 'fromDate', label: 'Từ ngày', width: 80, sortable:false,resizable:false,align:'center', formatter:'date', formatoptions: {srcformat:'Y-m-d', newformat:'d/m/Y'} },
		    {name:'toDate',index: 'toDate', label: 'Đến ngày', width: 80, sortable:false,resizable:false,align:'center', formatter:'date', formatoptions: {srcformat:'Y-m-d', newformat:'d/m/Y'} },
		    {name:'edit', label: 'Sửa', width: 50, align: 'center',sortable:false,resizable:false, formatter: ProgrammeDisplayCatalogFormatter.editCellIconCustomerFormatter},
		    {name:'delete', label: 'Xóa', width: 50, align: 'center',sortable:false,resizable:false, formatter: ProgrammeDisplayCatalogFormatter.deleteCellIconCustomerFormatter},
		    {name:'id', index : 'id', hidden: true },
		    {name:'customer.id', index : 'customer.id', hidden: true },
		    {name:'displayProgramLevel.id', index : 'displayProgramLevel.id', hidden: true },
		  ],	  
		  pager : '#customerPager',	   
		  width: ($('#customerGridContainer').width()),
		  gridComplete: function(){
			  $('#jqgh_customerGrid_rn').html('STT');
			  updateRownumWidthForJqGrid();
		  }
		})
		.navGrid('#customerPager', {edit:false,add:false,del:false, search: false});
	},
	searchCustomer: function(){
		var customerCode = $('#customerCode').val().trim();
		var customerName = $('#customerName').val().trim();
		var fromDate = $('#fromDateCus').val().trim();
		var toDate = $('#toDateCus').val().trim();
		var level = $('#levelProgramDisplay').val().trim();
		var url = ProgrammeDisplayCatalog.getCustomerGridUrl(customerCode, customerName, fromDate, toDate, level);
		$("#customerGrid").setGridParam({url:url,page:1}).trigger("reloadGrid");
		$('#customerCode').focus();
	},	
	getCheckboxStateOfTree:function(treeId,MAP,nodeType){
		for(var i=0;i<MAP.size();++i){
			var _obj = MAP.get(MAP.keyArray[i]);					
			$('.jstree-leaf').each(function(){
				var _id = $(this).attr('id');
				var type = $(this).attr('contentitemid');
				if(_id==_obj && nodeType==type ){
					$(this).removeClass('jstree-unchecked');
					$(this).addClass('jstree-checked');
				}
			});
		}
	},	
	searchCustomerOnTree: function(){
		$('.jstree-leaf').each(function(){
			var _id = $(this).attr('id');
			var type = $(this).attr('contentitemid');
			if($(this).hasClass('jstree-checked') && type==3){
				ProgrammeDisplayCatalog._mapCustomer.put(_id,_id);		
			}
			if($(this).hasClass('jstree-unchecked')){				
				ProgrammeDisplayCatalog._mapCustomer.remove(_id);	
			}
		});
		var staffMapId = $('#selStaffId').val();
		var customerCode = encodeChar($('#customerCodeDlg').val().trim());
		var customerName = encodeChar($('#customerNameDlg').val().trim());
		var shopCode = encodeChar($('#vnmShopCode').val().trim().trim());
		ProgrammeDisplayCatalog.loadDataForTreeWithCheckbox('/rest/catalog/display-program/customer/list.json?staffMapId='+ staffMapId +'&customerCode='+ encodeChar(customerCode) +'&customerName='+ encodeChar(customerName) +'&shopCode='+encodeChar(shopCode),'customerTree');
		
		setTimeout(function() {				
			$('#customerTreeContent').jScrollPane();
			$('#customerTree').bind("loaded.jstree open_node.jstree close_node.jstree", function (event, data) {
				if($('#customerTreeContent').data('jsp') != undefined || $('#customerTreeContent').data('jsp') != null) {
					$('#customerTreeContent').data('jsp').destroy();
					setTimeout(function(){
						$('#customerTreeContent').jScrollPane();
					},500);	
				} else {
					setTimeout(function(){
						$('#customerTreeContent').jScrollPane();
					},500);	
				}
				ProgrammeDisplayCatalog.getCheckboxStateOfTreeEx('customerTree',ProgrammeDisplayCatalog._mapCustomer,3,'customerTree li');
			});
			ProgrammeDisplayCatalog.getCheckboxStateOfTreeEx('customerTree',ProgrammeDisplayCatalog._mapCustomer,3,'customerTree li');
			$('#customerTreeContent .jspPane').css('width','486px');
		}, 500);
		ProgrammeDisplayCatalog.openNodeOnTree('customerTree');		
	},	
	selectListCustomer: function(){
		$('#errMsgCustomerDlg').html('').hide();
		$('#successMsgCustomerDlg').html('').hide();
		$.fancybox.update();
		var arrId = new Array();
		$('#customerTree li').each(function(){
			var _id = $(this).attr('id');
			var type = $(this).attr('contentitemid');
			if($(this).hasClass('jstree-checked') && type==3){
				ProgrammeDisplayCatalog._mapCustomer.put(_id,_id);		
			}else {
				ProgrammeDisplayCatalog._mapCustomer.remove(_id);
			}
		});
		for(var i=0;i<ProgrammeDisplayCatalog._mapCustomer.size();++i){
			var _obj = ProgrammeDisplayCatalog._mapCustomer.get(ProgrammeDisplayCatalog._mapCustomer.keyArray[i]);
			arrId.push(_obj);
		}		
		var msg = '';
		if(arrId.length == 0){
			msg = format(msgErr_required_choose_format,'Khách hàng theo cây đơn vị');
		}		
		if(msg.length == 0){
			msg = Utils.getMessageOfRequireCheck('fromDateCusDialog','Từ ngày');
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfInvalidFormatDate('fromDateCusDialog','Từ ngày');
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfRequireCheck('toDateCusDialog','Đến ngày');
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfInvalidFormatDate('toDateCusDialog','Đến ngày');
		}		
		var fDate = $('#fromDateCusDialog').val();
		var tDate = $('#toDateCusDialog').val();
		if(msg.length == 0){
			var currentTime = new Date();
			var month = currentTime.getMonth() + 1;
			var day = currentTime.getDate();
			var year = currentTime.getFullYear();		
			if(!Utils.compareDate(fDate, tDate)){
				msg = msgErr_fromdate_greater_todate;				
			}
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfRequireCheck('levelProgramDisplayDialog','Mức CTTB',true);
		}
		if(msg.length > 0){
			$('#errMsgCustomerDlg').html(msg).show();
			$.fancybox.update();
			return false;
		}		
		var displayId = $('#selId').val();
		var dataModel = new Object();
		dataModel.lstCustomerId = arrId;		
		dataModel.staffId = $('#selStaffId').val().trim();
		dataModel.fromDate = $('#fromDateCusDialog').val().trim();
		dataModel.toDate = $('#toDateCusDialog').val().trim();
		dataModel.levelId = $('#levelProgramDisplayDialog').val().trim();
		Utils.addOrSaveData(dataModel, '/catalog/programme-display/customer/save', ProgrammeDisplayCatalog._xhrSave, 'errMsgCustomerDlg', function(data){
			$('#successMsgCustomerDlg').html('Lưu dữ liệu thành công').show();
			$('#customerCodeDlg').val('');
			$('#customerNameDlg').val('');	
			Utils.resetCheckboxStateOfTree();
			ProgrammeDisplayCatalog.searchCustomerOnTree();			
			$.fancybox.update();
			
			var url = ProgrammeDisplayCatalog.getCustomerGridUrl('', '', '', '', '');
			$("#customerGrid").setGridParam({url:url,page:1}).trigger("reloadGrid");			
			
			$("#staffGrid").trigger("reloadGrid");
			setTimeout(function(){$.fancybox.close();}, 1000);
		}, null, '.fancybox-inner', null, null);
		return false;
	},
	showUpdateCustomerDetailDialog: function(rowId){
		var html = $('#customerDetailTreeDialog').html();
		$.fancybox(html,
				{
					modal: true,
					title: 'Cập nhật thông tin khách hàng',					
					afterShow: function(){
						$('#customerDetailTreeDialog').html('');
						$('#errMsgCustomerDetailDlg').html('').hide();
						$('#successMsgCustomerDetailDlg').html('').hide();	
						$('#successMsgCustomerDlg').html('').hide();
						
						var name = $("#customerGrid").jqGrid ('getCell', rowId, 'customer.customerName');
					    var customerId = $("#customerGrid").jqGrid ('getCell', rowId, 'customer.id');
					    var fromDate = $("#customerGrid").jqGrid ('getCell', rowId, 'fromDate');
					    var toDate = $("#customerGrid").jqGrid ('getCell', rowId, 'toDate');
						var levelId = $("#customerGrid").jqGrid ('getCell', rowId, 'displayProgramLevel.id');						
						var displayCustomerMapId = $("#customerGrid").jqGrid ('getCell', rowId, 'id');
						$('#displayCustomerMapIdTmp').val(displayCustomerMapId);
						$('#customerNameDetailDlg').val(name);
						$('.fancybox-inner #levelProgramDisplayDetailDialog').attr('id','levelProgramDisplayDetail');
						$('#levelProgramDisplayDetail').val(levelId);
						$('#levelProgramDisplayDetail').addClass('MySelectBoxClass');
						$('#levelProgramDisplayDetail').customStyle();
						var displayId = $('#selId').val();
						$.getJSON('/rest/catalog/display-program/'+ displayId +'/level/list.json', function(data){
							var arrHtml = new Array();
							arrHtml.push('<option value="-2">--- Chọn mức CTTB ---</option>');
							for(var i=0;i<data.length;i++){
								arrHtml.push('<option value="'+ data[i].value +'">'+ data[i].name +'</option>');
							}							
							$('#levelProgramDisplayDetail').html(arrHtml.join(""));
							$('#levelProgramDisplayDetail').val(levelId);
							$('#levelProgramDisplayDetail').change();
						});
						$('.fancybox-inner #fromDateCusDetailDialogTmp').attr('id','fromDateCusDetailDialog');
						$('.fancybox-inner #toDateCusDetailDialogTmp').attr('id','toDateCusDetailDialog');
						setDateTimePicker('fromDateCusDetailDialog');
						setDateTimePicker('toDateCusDetailDialog');
						$('#fromDateCusDetailDialog').focus();
						$('#fromDateCusDetailDialog').val(fromDate);
						$('#toDateCusDetailDialog').val(toDate);
						$('#customerIdDetailDlg').val(customerId);
					},
					afterClose: function(){
						$('#customerDetailTreeDialog').html(html);						
					}
				}
			);
		return false;
	},
	saveDetailCustomer: function(){
		$('#errMsgCustomerDetailDlg').html('').hide();
		$('#successMsgCustomerDetailDlg').html('').hide();
		$.fancybox.update();
		var msg = '';
		msg = Utils.getMessageOfRequireCheck('fromDateCusDetailDialog','Từ ngày');		
		if(msg.length == 0){
			msg = Utils.getMessageOfInvalidFormatDate('fromDateCusDetailDialog','Từ ngày');
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfRequireCheck('toDateCusDetailDialog','Đến ngày');
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfInvalidFormatDate('toDateCusDetailDialog','Đến ngày');
		}		
		var fDate = $('#fromDateCusDetailDialog').val();
		var tDate = $('#toDateCusDetailDialog').val();
		var displayCustomerMapId = $('#displayCustomerMapIdTmp').val().trim();
		if(msg.length == 0){
			var currentTime = new Date();
			var month = currentTime.getMonth() + 1;
			var day = currentTime.getDate();
			var year = currentTime.getFullYear();		
			if(!Utils.compareDate(fDate, tDate)){
				msg = msgErr_fromdate_greater_todate;				
			}
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfRequireCheck('levelProgramDisplayDetail','Mức CTTB',true);
		}
		if(msg.length > 0){
			$('#errMsgCustomerDetailDlg').html(msg).show();
			$.fancybox.update();
			return false;
		}		
		var displayId = $('#selId').val();
		var arrId = new Array();
		arrId.push($('#customerIdDetailDlg').val());
		var dataModel = new Object();
		dataModel.lstCustomerId = arrId;		
		dataModel.staffId = $('#selStaffId').val().trim();
		dataModel.fromDate = $('#fromDateCusDetailDialog').val().trim();
		dataModel.toDate = $('#toDateCusDetailDialog').val().trim();
		dataModel.levelId = $('#levelProgramDisplayDetail').val().trim();
		dataModel.numUpdate = 0;
		dataModel.displayCustomerMapId = displayCustomerMapId;
		Utils.addOrSaveData(dataModel, '/catalog/programme-display/customer/save', ProgrammeDisplayCatalog._xhrSave, 'errMsgCustomerDetailDlg', function(data){
			$('#successMsgCustomerDetailDlg').html('Lưu dữ liệu thành công').show();			
			$.fancybox.update();
			$("#customerGrid").trigger("reloadGrid");
			setTimeout(function(){$.fancybox.close();}, 1500);
		}, null, '.fancybox-inner', null, null);
		return false;
	},
	selectSubCat:function(obj,id){
		if(obj.checked){
			ProgrammeDisplayCatalog._mapCheckSubCat.put(id, $(obj).attr('value'));
		}else{
			ProgrammeDisplayCatalog._mapCheckSubCat.remove(id);
		}
	},
	loadDataForTreeWithCheckbox:function(url,treeId){
		var tId = 'tree';
		if(treeId!= null && treeId!= undefined){
			tId = treeId;
		}
		$('#' + tId).jstree({
	        "plugins": ["themes", "json_data","ui","checkbox"],
	        "themes": {
	            "theme": "classic",
	            "icons": false,
	            "dots": true
	        },
	        "json_data": {
	        	"ajax" : {
	        		"method": "post",
	                "url" : url,
	                "data" : function (n) {
	                        return { id : n.attr ? n.attr("id") : 0 };
	                    }
	            },
	            'complete':function(){
	            	$('#'+treeId).jScrollPane();	            	
	            }
	        }
		});	
	},
	loadDataForTreeWithCheckboxEx:function(url,treeId, forNvptkh){
		var tId = 'tree';
		if(treeId!= null && treeId!= undefined){
			tId = treeId;
		}
		$('#' + tId).jstree({
	        "plugins": ["themes", "json_data","ui","checkbox"],
	        "themes": {
	            "theme": "classic",
	            "icons": false,
	            "dots": true
	        },
	        "json_data": {
	        	"ajax" : {
	        		"method": "get",
	                "url" : function( node ){
	                      if(forNvptkh != undefined || forNvptkh ==true) {
	                    	  if(node == -1) {
	                    		  return url;
	                    	  } else {
	                    		var displayId = $('#selId').val();
	                    		if(displayId==undefined || displayId==null){
	                    			  displayId = 0;
	                    		  }
	                    		var staffCode = encodeURI('');
	      						var staffName = encodeURI('');
	      						return '/rest/catalog/display-program/staff/list.json?displayProgramId='+ displayId +'&staffCode='+ encodeChar(staffCode) +'&staffName='+ encodeChar(staffName);
	                    	  }
	                      } else {
	                    	  if(node == -1) {
		                    	  return url;
		                      } else {
		                    	  var nodeType = node.attr("classStyle");
		                    	  var catId = 0;
		                    	  var subCatId = 0;
		                    	  if(nodeType == 'cat') {
		                    		  catId = node.attr("id");
		                    		  var displayId = $('#selId').val();
		                    		  if(displayId==undefined || displayId==null){
		                    			  displayId = 0;
		                    		  }
		                    		  var productCode = encodeURI('');
		                    		  var productName = encodeURI('');	
		                    		  return '/rest/catalog/display-program/product/list.json?displayProgramId='+ displayId +'&productCode='+ encodeChar(productCode) +'&productName='+ encodeChar(productName) + '&nodeType=sub-cat&catId=' +catId+ '&subCatId=0';
		                    	  } else if(nodeType == 'sub-cat') {
		                    		  subCatId = node.attr("id");
		                    		  catId = $('#'+subCatId).parent().parent().attr('id');
		                    		  var displayId = $('#selId').val();
		                    		  if(displayId==undefined || displayId==null){
		                    			  displayId = 0;
		                    		  }
		                    		  var productCode = encodeURI('');
		                    		  var productName = encodeURI('');	
		                    		  return '/rest/catalog/display-program/product/list.json?displayProgramId='+ displayId +'&productCode='+ encodeChar(productCode) +'&productName='+ encodeChar(productName) + '&nodeType=product&catId=' +catId+ '&subCatId=' + subCatId;
		                    	  } else if(nodeType == 'product') {
		                    		  return '';
		                    	  }
		                      }
	                      }
	                    },
	                "data" : function (n) {
	                        return { id : n.attr ? n.attr("id") : 0 };
	                    }
	            }
	        }
		});	
	},
	loadDataForTreeWithCheckboxExCRM_SKU:function(url,treeId, forNvptkh){
		var tId = 'tree';
		if(treeId!= null && treeId!= undefined){
			tId = treeId;
		}
		$('#' + tId).jstree({
	        "plugins": ["themes", "json_data","ui","checkbox"],
	        "themes": {
	            "theme": "classic",
	            "icons": false,
	            "dots": true
	        },
	        "json_data": {
	        	"ajax" : {
	        		"method": "get",
	                "url" : function( node ){
	                      if(forNvptkh != undefined || forNvptkh ==true) {
	                    	  if(node == -1) {
	                    		  return url;
	                    	  } else {
	                    		var displayId = $('#selId').val();
	                    		var staffCode = encodeURI('');
	      						var staffName = encodeURI('');
	      						return '/rest/catalog/display-program/staff/list.json?displayProgramId='+ 0 +'&staffCode='+ encodeChar(staffCode) +'&staffName='+ encodeChar(staffName);
	                    	  }
	                      } else {
	                    	  if(node == -1) {
		                    	  return url;
		                      } else {
		                    	  var nodeType = node.attr("classStyle");
		                    	  var catId = 0;
		                    	  var subCatId = 0;
		                    	  if(nodeType == 'cat') {
		                    		  catId = node.attr("id");
		                    		  var displayId = $('#selId').val();
		                    		  var productCode = encodeURI('');
		                    		  var productName = encodeURI('');	
		                    		  return '/rest/catalog/display-program/product/list.json?displayProgramId='+ 0 +'&productCode='+ encodeChar(productCode) +'&productName='+ encodeChar(productName) + '&nodeType=sub-cat&catId=' +catId+ '&subCatId=0';
		                    	  } else if(nodeType == 'sub-cat') {
		                    		  subCatId = node.attr("id");
		                    		  catId = $('#'+subCatId).parent().parent().attr('id');
		                    		  var displayId = $('#selId').val();
		                    		  var productCode = encodeURI('');
		                    		  var productName = encodeURI('');	
		                    		  return '/rest/catalog/display-program/product/list.json?displayProgramId='+ 0 +'&productCode='+ encodeChar(productCode) +'&productName='+ encodeChar(productName) + '&nodeType=product&catId=' +catId+ '&subCatId=' + subCatId;
		                    	  } else if(nodeType == 'product') {
		                    		  return '';
		                    	  }
		                      }
	                      }
	                    },
	                "data" : function (n) {
	                        return { id : n.attr ? n.attr("id") : 0 };
	                    }
	            }
	        }
		});	
	},
	loadDataForTreeWithCheckboxExDisplayGroup:function(url,treeId, forNvptkh){
		var tId = 'tree';
		if(treeId!= null && treeId!= undefined){
			tId = treeId;
		}
		$('#' + tId).jstree({
	        "plugins": ["themes", "json_data","ui","checkbox"],
	        "themes": {
	            "theme": "classic",
	            "icons": false,
	            "dots": true
	        },
	        "json_data": {
	        	"ajax" : {
	        		"method": "get",
	                "url" : function( node ){
	                      if(forNvptkh != undefined || forNvptkh ==true) {
	                    	  if(node == -1) {
	                    		  return url;
	                    	  } else {
	                    		var displayId = $('#selId').val();
	                    		var staffCode = encodeURI('');
	      						var staffName = encodeURI('');
	      						return '/rest/catalog/display-program/staff/list.json?displayProgramId='+ displayId +'&staffCode='+ encodeChar(staffCode) +'&staffName='+ encodeChar(staffName);
	                    	  }
	                      } else {
	                    	  if(node == -1) {
		                    	  return url;
		                      } else {
		                    	  var nodeType = node.attr("classStyle");
		                    	  var catId = 0;
		                    	  var subCatId = 0;
		                    	  if(nodeType == 'cat') {
		                    		  catId = node.attr("id");
		                    		  var displayId = $('#selId').val();
		                    		  var productCode = encodeURI('');
		                    		  var productName = encodeURI('');	
		                    		  return '/rest/catalog/display-program/productDisplayGroup/list.json?displayProgramId='+ displayId +'&productCode='+ encodeChar(productCode) +'&productName='+ encodeChar(productName) + '&nodeType=sub-cat&catId=' +catId+ '&subCatId=0';
		                    	  } else if(nodeType == 'sub-cat') {
		                    		  subCatId = node.attr("id");
		                    		  catId = $('#'+subCatId).parent().parent().attr('id');
		                    		  var displayId = $('#selId').val();
		                    		  var productCode = encodeURI('');
		                    		  var productName = encodeURI('');	
		                    		  return '/rest/catalog/display-program/productDisplayGroup/list.json?displayProgramId='+ displayId +'&productCode='+ encodeChar(productCode) +'&productName='+ encodeChar(productName) + '&nodeType=product&catId=' +catId+ '&subCatId=' + subCatId;
		                    	  } else if(nodeType == 'product') {
		                    		  return '';
		                    	  }
		                      }
	                      }
	                    },
	                "data" : function (n) {
	                        return { id : n.attr ? n.attr("id") : 0 };
	                    }
	            }
	        }
		});	
	},
	gotoTab: function(tabIndex){
		ProgrammeDisplayCatalog.deactiveAllMainTab();
		$('#tabContent2').html('');
		$('#tabContent3').html('');
		$('#tabContent4').html('');
		$('.FakeInputFile input').each(function(){
			$(this).val('');
		});
		switch (tabIndex) {
		case 0:
			$('#tabContent1').show();
			$('#tab1 a').addClass('Active');
			break;
		case 1:
			$('#tabContent2').show();
			$('#tab2 a').addClass('Active');
			AttributesManager.getAttributesForm('tabContent2',AttributesManager.DISPLAY_PROGRAM);
			$('#typeAttribute').val(AttributesManager.DISPLAY_PROGRAM);			
			break;
		case 2:
			$('#tabContent3').show();
			$('#tab3 a').addClass('Active');
			AttributesManager.getAttributesForm('tabContent3',AttributesManager.DISPLAY_PROGRAM_LEVEL);
			$('#typeAttribute').val(AttributesManager.DISPLAY_PROGRAM_LEVEL);			
			break;		
		case 3:
			$('#tabContent4').show();
			$('#tab4 a').addClass('Active');
			AttributesManager.getAttributesForm('tabContent4',AttributesManager.DISPLAY_GROUP);
			$('#typeAttribute').val(AttributesManager.DISPLAY_GROUP);			
			break;	
		default:
			$('#tabContent1').show();
			break;
		}
		$('.ErrorMsgStyle').each(function(){
			if(!$(this).is(':hidden')){
				$(this).html('').hide();
			}
		});
	},	
	deactiveAllMainTab: function(){
		$('#tab1 a').removeClass('Active');
		$('#tab2 a').removeClass('Active');
		$('#tab3 a').removeClass('Active');
		$('#tab4 a').removeClass('Active');
		$('#tabContent1').hide();
		$('#tabContent2').hide();
		$('#tabContent3').hide();
		$('#tabContent4').hide();
	},
	exportExcelDisplayPrograme:function(){
		var excelType = $('#excelType').val();
		var code = $('#code').val();
		var name = $('#name').val();
		var status = $('#status').val().trim();
		var fDate = $('#fDate').val();
		var tDate = $('#tDate').val();
		var cont = -1;
		if($('#orCont').is(':checked')){
			cont = 1;
		} else if($('#andCont').is(':checked')){
			cont = 0;
		}
		var params = new Object();
		params.code = code;
		params.name = name;
		params.stt = status;
		params.fromDate = fDate;
		params.toDate = tDate;
		params.cont = cont;
		
		if(excelType == 1){
			Utils.addOrSaveData(params, '/catalog/programme-display/export-Excel-Display-Programe', null, 'errMsg', function(data) {
				window.location.href=data.view;
			}, 'loading', null, null, 'Bạn có muốn xuất file này?');
			return false;
		}else if(excelType == 2){
			Utils.addOrSaveData(params, '/catalog/programme-display/export-Excel-Quota-Group', null, 'errMsg', function(data) {
				window.location.href=data.view;
			}, 'loading', null, null, 'Bạn có muốn xuất file này?');
			return false;
		}else if(excelType == 3){
			Utils.addOrSaveData(params, '/catalog/programme-display/export-Excel-Level', null, 'errMsg', function(data) {
				window.location.href=data.view;
			}, 'loading', null, null, 'Bạn có muốn xuất file này?');
			return false;
		}else if(excelType == 4){
			Utils.addOrSaveData(params, '/catalog/programme-display/export-Excel-Product', null, 'errMsg', function(data) {
				window.location.href=data.view;
			}, 'loading', null, null, 'Bạn có muốn xuất file này?');
			return false;
		}else if(excelType == 5){
			Utils.addOrSaveData(params, '/catalog/programme-display/export-Excel-Staff', null, 'errMsg', function(data) {
				window.location.href=data.view;
			}, 'loading', null, null, 'Bạn có muốn xuất file này?');
			return false;
		}else if(excelType == 6){
			Utils.addOrSaveData(params, '/catalog/programme-display/export-Excel-Shop', null, 'errMsg', function(data) {
				window.location.href=data.view;
			}, 'loading', null, null, 'Bạn có muốn xuất file này?');
			return false;
		}
	},
	// huyNP4
	importExcel:function(){
		$('#isView').val(0);
		$('#importFrm').submit();
		return false;	
	},	
	// huyNP4
	viewExcel:function(){
		$('#isView').val(1);
		$('#importFrm').submit();
		return false;
	},
	// huyNP4
	beforeImportExcel: function(){
		if(!previewImportExcelFile(document.getElementById("excelFile"))){
			return false;
		}
		// showLoading();
		$(".ErrorMsgStyle").hide();
		$('#divOverlay').addClass('Overlay');
		$('#imgOverlay').show();
		return true;
	},
	// huyNP4
	afterImportExcel01: function(responseText, statusText, xhr, $form){// bb HUyNP
		$('#divOverlay').removeClass('Overlay');
		$('#imgOverlay').hide();
		if (statusText == 'success') {				
	    	$("#responseDiv").html(responseText);		   
	    	if($('#errorExcel').html().trim() == 'true' || $('#errorExcelMsg').html().trim().length > 0){
	    		$('#errExcelMsg').html($('#errorExcelMsg').html()).show();
	    	} else {
	    		if($('#typeView').html().trim() == 'false'){
	    			var totalRow = parseInt($('#totalRow').html().trim());
	    			var numFail = parseInt($('#numFail').html().trim());
	    			var fileNameFail = $('#fileNameFail').html();
	    			var mes = format(msgErr_result_import_excel,(totalRow - numFail),numFail);		    		
	    			if(numFail > 0){
	    				mes+= ' <a href="'+ fileNameFail +'">Xem chi tiết lỗi</a>';
	    			}
	    			$('#errExcelMsg').html(mes).show();
	    			$("#grid").trigger("reloadGrid");
	    			if(ProductLevelCatalog._callBackAfterImport != null){
		    			ProductLevelCatalog._callBackAfterImport.call(this);
		    		}
	    		}else{
	    			var obj = '';
	    			var tiles='';
	    			if($('#excelType').val() == '' || $('#excelType').val() == undefined ){
	    				obj = '#popup1';
	    				tiles='Danh sách import Excel CTTB';
	    			}else{
	    				if(parseInt($('#excelType').val()) == 1){
	    					obj = '#popup1';
	    					tiles='Danh sách import Excel CTTB';
	    				}else if(parseInt($('#excelType').val()) == 2){
	    					obj = '#popup2';
	    					tiles='Danh sách import Excel nhóm chỉ tiêu';
	    				}else if(parseInt($('#excelType').val()) == 3){
	    					obj = '#popup3';
	    					tiles='Danh sách import Excel mức';
	    				}else if(parseInt($('#excelType').val()) == 4){
	    					obj = '#popup4';
	    					tiles='Danh sách import Excel sản phẩm';
	    				}else if(parseInt($('#excelType').val()) == 5){
	    					obj = '#popup5';
	    					tiles='Danh sách import Excel Nhân viên phát triển khách hàng';
	    				}else if(parseInt($('#excelType').val()) == 6){
	    					obj = '#popup6';
	    					tiles='Danh sách đơn vị tham gia CTTB';
	    				}
	    			}
	    			var html = $(obj).html();
	    			$.fancybox(html,
    					{
    						modal: true,
    						title: tiles,
    						afterShow: function(){
    							$(obj).html('');
    							$('#scrollExcelDialog').jScrollPane();
    							$('.ScrollSection').jScrollPane().data().jsp;
    							$('.ScrollBodySection').jScrollPane();
    							$(window).resize();
    						},
    						afterClose: function(){
    							$(obj).html(html);
    						}
    					}
    				);
	    		}
	    	}
	    }	
	},
	showShopTab: function(){
		ProgrammeDisplayCatalog.hideAllTab();
		$('#tab4').addClass('Active');		
		$('#container7').show();
		// Utils.bindAutoSearch();
		$('#downloadTemplate').attr( 'href', excel_template_path + 'display-program/Bieu_mau_danh_muc_CTTB_don_vi_import.xls');
		var value = $('#divWidth').width()-50;
		var dataWidth = (value * 95)/100-6;
		var editWidth = (value * 10)/100;
		var deleteWidth = (value * 9)/100+6;
		var statusPermission = $('#statusPermission').val();
		var isOutOfDate = $('#isOutOfDate').val();
		var titleOpenDialogShop ="";
		if($('#permissionUser').val() == 'true'){
			if((statusPermission == 2 || statusPermission == 1) && isOutOfDate == 0){
				titleOpenDialogShop = '<a href="javascript:void(0);" onclick="return ProgrammeDisplayCatalog.openShopDialog();"><img src="/resources/images/icon_add.png"></a>';
			}
		}
		$('#exGrid').treegrid({  
		    url:  ProgrammeDisplayCatalog.getShopGridUrl($('#selId').val(),'',''),
		     width:($('#divWidth').width()-50),  
	        height:'auto',  
	        idField: 'id',  
	        treeField: 'data',
		    columns:[[  
		        {field:'data',title:'Đơn vị',resizable:false,width:dataWidth,formatter:function(value,row,index){
		        	return Utils.XSSEncode(row.data);
		        }},
		        {field:'edit',title:titleOpenDialogShop,width:50,align:'center',resizable:false,formatter:function(value,row,index){
//		        	if(row.isJoin >= 1){
//		        		return '<input type ="checkbox" disabled="disabled">';
//		        	}
		        	var classParent = "";
		        	if(row.parentId != null && row.parentId != undefined){
		        		classParent = "class_"+row.parentId;
		        	}
		        	if($('#permissionUser').val() == 'true'){
		        		if(statusPermission == 2 || statusPermission == 1){
		        			return '<input type ="checkbox" class="'+classParent+'" value="'+row.id+'" id="check_'+row.id+'" onclick="return ProgrammeDisplayCatalog.onCheckShopEx('+row.id+');">';
		        		}
		        	}
		        	
		        }}
		    ]] 
		});
		$('#shopCode').bind('keyup', function(event){
			if(event.keyCode == 13){
				ProgrammeDisplayCatalog.searchShop();
			}
		}).val('');
		$('#shopName').bind('keyup', function(event){
			if(event.keyCode == 13){
				ProgrammeDisplayCatalog.searchShop();
			}
		}).val('');
	},
	showCustomerTab1: function(){
		ProgrammeDisplayCatalog.hideAllTab();
		$('#tab10').addClass('Active');		
		$('#container10').show();
		ReportUtils.loadComboReportTreeEx('shopTree', 'shopCode', $('#curShopId').val(),function(data){
		});
		$('#customerGrid123').datagrid({
			url :'/programme-display/customer/search',
			autoRowHeight : true,
			rownumbers : true, 
			checkOnSelect :true,
			pagination:true,
			//singleSelect:true,			
			queryParams:{
				id:$('#selId').val()
			},
			fitColumns:true, 
			scrollbarSize : 0,
			pageSize:50,
			pageNumber:1,
			width : $(window).width()-48, 
			//width: $('#customerDisplayGrid').width(),
			columns:[[  
//		        	{field: 'displayProgramCode',title:'Mã CTTB', width:100,align:'left',sortable : false,resizable : false,formatter :function(value,row,index) {
//						return Utils.XSSEncode(value);
//					}},
	        	{field: 'levelCode',title:'Mức CTTB', width:50,align:'left',sortable : false,resizable : false,formatter :function(value,row,index) {
					return Utils.XSSEncode(value);
				}},  
	        	{field: 'shopCode',title:'Mã NPP', width:50,align:'left',sortable : false,resizable : false,formatter :function(value,row,index) {
					return Utils.XSSEncode(value);
				}},  
	        	{field: 'staffName',title:'NVBH', width:100,align:'left',sortable : false,resizable : false,formatter :function(value,row,index) {
					return Utils.XSSEncode(value);
				}},  
	        	{field: 'customerName',title:'Khách hàng', width:100,align:'left',sortable : false,resizable : false,formatter :function(value,row,index) {
					return Utils.XSSEncode(value);
				}},  
				{field: 'address',title:'Địa chỉ', width:100,align:'left',sortable : false,resizable : false,formatter :function(value,row,index) {
					return Utils.XSSEncode(row.houseNumber) + " " + Utils.XSSEncode(row.street);
				}},
	        	{field: 'fromDate',title:'Tháng', width:50,align:'center',sortable : false,resizable : false,formatter:function(row,index){
	        		$('#totalRowGrid').val('1');
		        		return Utils.XSSEncode(row);
	        		}
	        	}, 
	        	{field: 'oneMonth',title:'Ngày tạo', width:50,align:'center',sortable : false,resizable : false,formatter:function(row,index){
		        		return Utils.XSSEncode(row);
	        		}
	        	}, 
	        	{field: 'updateDate',title:'Ngày điều chỉnh', width:60,align:'center',sortable : false,resizable : false,formatter:function(row,index){
		        		return Utils.XSSEncode(row);
	        		}
	        	}
//	        	{field: 'id', checkbox:true, align:'center', width:80,sortable : false,resizable : false}
	        ]],
	        onLoadSuccess :function(data){
		    	$('.datagrid-header-rownumber').html('STT');
		    	var originalSize = $('.datagrid-header-row td[field=customerName] div').width();
		    	if(originalSize != null && originalSize != undefined){
		    		ProgrammeDisplayCatalog._lstSize.push(originalSize);
		    	}
		    	ProgrammeDisplayCatalog.updateRownumWidthForJqGridEX1('#customerGrid123','customerName');
//		    	if(data.total <= 50){  
//		    		$('#loadShop').hide(); 
//		    		$('#loadShop').attr('disabled','disabled');
//		    	}else{
//		    		$('#loadShop').show();
//		    		$('#loadShop').removeAttr('disabled');
//		    	}
		    	$('input[name="id"]').each(function(){
					var temp = ProgrammeDisplayCatalog._lstProduct.get($(this).val());
					if(temp!=null) $(this).attr('checked','checked');
				});
		    	var length = 0;
		    	$('input[name="id"]').each(function(){
		    		if($(this).is(':checked')){
		    			++length;
		    		}	    		
		    	});	    	
		    	if(data.rows.length==length){
		    		$('.datagrid-header-check input').attr('checked',true);
		    	}else{
		    		$('.datagrid-header-check input').attr('checked',false);
		    	}
		    },
		    onCheck:function(i,r){
		    	$('#checkDeleteAll').removeAttr('checked');
		    	ProgrammeDisplayCatalog._lstProduct.put(r.id,r);
		    },
		    onUncheck:function(i,r){
		    	ProgrammeDisplayCatalog._lstProduct.remove(r.id);
		    },
		    onCheckAll:function(r){
		    	$('#checkDeleteAll').removeAttr('checked');
		    	for(i=0;i<r.length;i++){
		    		ProgrammeDisplayCatalog._lstProduct.put(r[i].id,r[i]);
		    	}
		    },
		    onUncheckAll:function(r){
		    	for(i=0;i<r.length;i++){
		    		ProgrammeDisplayCatalog._lstProduct.remove(r[i].id);
		    	}
		    }
		});
		
		$('#errCustomerMsg').html('').hide();
		var params = new Object();
		var now = new Date();
		var cYear = now.getFullYear();
		var cMonth = now.getMonth() + 1;
		if(cMonth != 10 && cMonth != 11 && cMonth != 12  ){
			cMonth = '0'+cMonth;
		}
		var currentDate = cMonth+'/'+cYear;
		$('#month1').val(currentDate);
		var month = $('#month1').val();
		params.id = $('#selId').val();
		params.shopId = $('#shopTreeId .combo-value').val();
		params.month = month;
		$('#filterMonth').val(month);
		ProgrammeDisplayCatalog._params = params;
	},
	searchCustomerTab:function(){
		var shopId = $('#shopTreeId .combo-value').val();
		var month = $('#month1').val();
		var params = new Object();
		params.shopId = shopId;
		params.month = month;
		params.id = $('#selId').val();		
		ProgrammeDisplayCatalog._params = params;
		$('#filterMonth').val(month);
		ProgrammeDisplayCatalog._lstProduct = new Map();
		$('#errCustomerMsg').html('').hide();
		$('#customerGrid123').datagrid('reload',params);
	},
	searchCustomerTabEX:function(){
		$('.ErrorMsgStyle').hide();
		$('#checkDeleteAll').removeAttr('checked');
		var shopKendo = $("#shop").data("kendoMultiSelect");
        var lstShopId = new Array();
        lstShopId = shopKendo.value();
		var month = $('#month1').val();
		var params = new Object();
		if(lstShopId != null && lstShopId != undefined){
			params.listShopIdString = lstShopId.toString();
		}

		params.month = month;
		params.id = $('#displayProgram').val();		
		ProgrammeDisplayCatalog._params = params;
		$('#filterMonth').val(month);
		ProgrammeDisplayCatalog._lstProduct = new Map();
		$('#customerGrid123').datagrid('load',params);
//		setTimeout(function(){ //Set timeout để đảm bảo file load lên hoàn tất
//			$('#customerGrid123').datagrid({pageNumber:1});
//      },2000);
	},
	loadDisplayProgram:function(params){
		Utils.getHtmlDataByAjax(params,
				'/programme-display/customer/loadDisplayProgram',
				function(data) {
					try {
						var _data = JSON.parse(data);
					} catch (e) {

						$('#displayProgram').html(data);
						$('.CustomStyleSelectBoxInner').html("Tất cả");
					}
				}, '', 'GET');
	},
	getCustomerTabGridUrl:function(shopId,month){
		var url = '/programme-display/customer/search?id='+$('#selId').val()+"&shopId="+shopId+"&month="+month;
		return url;
	},
// getShopGridUrl: function(code,name,status){
// var selId = $('#selId').val();
// return '/catalog/programme-display/searchshop?id=' + encodeChar(selId) +
// '&code=' + encodeChar(code) + '&name=' + encodeChar(name) + '&status=' +
// status;
// },
	showUpdateShopDetailDialog: function(rowId,status){
		var html = $('#shopDetailTreeDialog').html();
		$.fancybox(html,
				{
					modal: true,
					autoResize: false,
					title: 'Cập nhật thông tin đơn vị',					
					afterShow: function(){
						$('#shopDetailTreeDialog').html('');
						$('#errMsgShopDetailDlg').html('').hide();
						$('#successMsgShopDetailDlg').html('').hide();						
						
						var name = $("#shopGrid").jqGrid ('getCell', rowId, 'shop.shopName');
						var id = $("#shopGrid").jqGrid ('getCell', rowId, 'shop.id');					    						
						if(status == 'RUNNING'){
							status = 1;
						} else if(status == 'STOPPED'){
							status = 0;
						} else {
							status = -2;
						}						
						$('#shopNameDetailDlg').val(name);
						$('#shopStatusDlgTmp').attr('id','shopStatusDlg');
						$('#shopStatusDlg').val(status);
						$('#shopStatusDlg').addClass('MySelectBoxClass');
						$('#shopStatusDlg').customStyle();						
						$('#shopIdDetailDlg').val(id);
					},
					afterClose: function(){
						$('#shopDetailTreeDialog').html(html);						
					}
				}
			);
		return false;
	},
	deleteShopRow: function(id){
		var dataModel = new Object();
		dataModel.shopMapId = id;	
		Utils.deleteSelectRowOnGrid(dataModel, 'đơn vị', '/catalog/programme-display/delete-shop', ProgrammeDisplayCatalog._xhrDel, 'shopGrid','errMsgShop',function(data){
			showSuccessMsg('successMsgShop',data);
		},'shopLoading');		
		return false;
	},
	saveDetailShop: function(){
		$('#errMsgShopDetailDlg').html('').hide();
		$('#successMsgShopDetailDlg').html('').hide();
		$.fancybox.update();
		var msg = '';
		msg = Utils.getMessageOfRequireCheck('shopStatusDlg', 'Trạng thái',true);		
		if(msg.length > 0){
			$('#errMsgShopDetailDlg').html(msg).show();
			$.fancybox.update();
			return false;
		}		
		var displayProgramId = $('#selId').val();
		var arrId = new Array();
		arrId.push($('#shopIdDetailDlg').val());
		var dataModel = new Object();
		dataModel.lstShopId = arrId;
		dataModel.id = displayProgramId;
		dataModel.status = $('#shopStatusDlg').val().trim();
		dataModel.numUpdate = 0; 
		Utils.addOrSaveData(dataModel, '/catalog/programme-display/shop/save', ProgrammeDisplayCatalog._xhrSave, 'errMsgShopDlg', function(data){
			$('#successMsgShopDlg').html('Lưu dữ liệu thành công').show();						
			$.fancybox.update();
			ProgrammeDisplayCatalog.searchShop();
			setTimeout(function(){$.fancybox.close();}, 1000);
		}, null, '.fancybox-inner', null, null);
		return false;
	},
//	searchShop: function(){
//		var code = $('#shopCode').val().trim();
//		var name = $('#shopName').val().trim();
//		var status = $('#shopStatus').val().trim();
//		var url = ProgrammeDisplayCatalog.getShopGridUrl(code,name,status);
//		$("#shopGrid").setGridParam({url:url,page:1}).trigger("reloadGrid");
//	},
	showDialogCreateShop: function(){
		var html = $('#shopTreeDialog').html();
		$.fancybox(html,
				{
					modal: true,
					autoResize: false,
					title: 'Thêm đơn vị tham gia CTTB',
					afterShow: function(){
						Utils.resetCheckboxStateOfTree();
						Utils.bindAutoSearch();	
						$('#shopTreeDialog').html('');
						$('#successMsgShopDlg').html('').hide();
						var displayProgramId = $('#selId').val();
						$('#shopCodeDlg').focus();
						var shopCode = encodeURI('');
						var shopName = encodeURI('');	
						ProgrammeDisplayCatalog.loadShopTreeOnDialog('/rest/catalog/programme-display/shop/list.json?displayProgramId='+ displayProgramId +'&shopCode='+ encodeChar(shopCode) +'&shopName='+ encodeChar(shopName));
						ProgrammeDisplayCatalog._mapShop = new Map();
						$('#shopTree').bind("loaded.jstree open_node.jstree close_node.jstree", function (event, data) {
							if($('#shopTreeContent').data('jsp') != undefined || $('#shopTreeContent').data('jsp') != null) {
								$('#shopTreeContent').data('jsp').destroy();
								setTimeout(function(){
									$('#shopTreeContent').jScrollPane();
								},500);	
							} else {
								setTimeout(function(){
									$('#shopTreeContent').jScrollPane();
								},500);	
							}						
		    			});
					},
					afterClose: function(){
						$('#shopTreeDialog').html(html);						
					}
				}
			);
		return false;
	},
	searchShopOnTree: function(){
		// Utils.getCheckboxStateOfTree();
		$('#shopTree li').each(function(){
			var _id = $(this).attr('id');
			if($(this).hasClass('jstree-checked')){
				ProgrammeDisplayCatalog._mapShop.put(_id,_id);	
			}else{
				ProgrammeDisplayCatalog._mapShop.remove(_id);
			}		
		});
		var displayProgramId = $('#selId').val();
		var shopCode = encodeChar($('#shopCodeDlg').val().trim());
		var shopName = encodeChar($('#shopNameDlg').val().trim());
		$('#shopTreeContent').data('jsp').destroy();
		ProgrammeDisplayCatalog.loadShopTreeOnDialog('/rest/catalog/programme-display/shop/list.json?displayProgramId='+ displayProgramId +'&shopCode='+ encodeChar(shopCode) +'&shopName='+ encodeChar(shopName));
		setTimeout(function() {
			$('#shopTreeContent').jScrollPane();
			$('#shopTree').bind("loaded.jstree open_node.jstree close_node.jstree", function (event, data) {
				if($('#shopTreeContent').data('jsp') != undefined || $('#shopTreeContent').data('jsp') != null) {
					$('#shopTreeContent').data('jsp').destroy();
					setTimeout(function(){
						$('#shopTreeContent').jScrollPane();
					},500);	
				} else {
					setTimeout(function(){
						$('#shopTreeContent').jScrollPane();
					},500);	
				}
				ProgrammeDisplayCatalog.getCheckboxStateOfTreeEx('shopTree',ProgrammeDisplayCatalog._mapShop,'shopTree li');
			});
			ProgrammeDisplayCatalog.getCheckboxStateOfTreeEx('shopTree',ProgrammeDisplayCatalog._mapShop,'shopTree li');
		}, 500);
		$('.fancybox-inner #shopCodeDlg').focus();
	},
	selectListShop: function(){
		$('#errMsgShopDlg').html('').hide();
		$('#successMsgShopDlg').html('').hide();
		$.fancybox.update();		
		var arrId = new Array();
// $('#shopTree li').each(function(){
// var _id = $(this).attr('id');
// if($(this).hasClass('jstree-checked')){
// ProgrammeDisplayCatalog._mapShop.put(_id,_id);
// }else{
// ProgrammeDisplayCatalog._mapShop.remove(_id);
// }
// });
		$('.jstree-checked').each(function(){
		    var _id= $(this).attr('id');
		    if($('#' + _id + ' ul li').length >0 && (!$(this).parent().parent().hasClass('jstree-checked'))){
		    	ProgrammeDisplayCatalog._mapShop.put(_id,_id);
		    }else if($('#' + _id ).length >0 && (!$(this).parent().parent().hasClass('jstree-checked'))){
		    	ProgrammeDisplayCatalog._mapShop.put(_id,_id);
			}else{
				ProgrammeDisplayCatalog._mapShop.remove(_id);
			}	
		});
		for(var i=0;i<ProgrammeDisplayCatalog._mapShop.size();++i){
			var _obj = ProgrammeDisplayCatalog._mapShop.get(ProgrammeDisplayCatalog._mapShop.keyArray[i]);
			arrId.push(_obj);
		}		
		var msg = '';
		if(arrId.length == 0){
			msg = format(msgErr_required_choose_format,'Đơn vị');
		}		
		if(msg.length > 0){
			$('#errMsgShopDlg').html(msg).show();			
			$.fancybox.update();
			return false;
		}		
		var focusProgramId = $('#selId').val();
		var dataModel = new Object();		
		dataModel.lstShopId = arrId;		
		dataModel.id = focusProgramId;
		dataModel.status = -2;
		Utils.addOrSaveData(dataModel, '/catalog/programme-display/shop/save', ProgrammeDisplayCatalog._xhrSave, 'errMsgShopDlg', function(data){
			$('#successMsgShopDlg').html('Lưu dữ liệu thành công').show();
			$('#shopCodeDlg').val('');
			$('#shopNameDlg').val('');
			$('#shopCode').val('');
			$('#shopName').val('');
			setSelectBoxValue('shopStatus', activeType.RUNNING);
			Utils.resetCheckboxStateOfTree();
			ProgrammeDisplayCatalog.searchShopOnTree();	
			ProgrammeDisplayCatalog.searchShop();
			$(window).resize();
			$.fancybox.update();
			// $("#shopGrid").trigger("reloadGrid");
			setTimeout(function(){$.fancybox.close();}, 1000);
		}, null, '.fancybox-inner', null, null);
		return false;
	},
	loadShopTreeOnDialog: function(url){
		$.getJSON(url, function(data){
			$('#shopTree').bind("loaded.jstree open_node.jstree close_node.jstree", function (event, data) {				
				if($('#shopTreeContent').data('jsp') != undefined || $('#shopTreeContent').data('jsp') != null) {
					$('#shopTreeContent').data('jsp').destroy();
					setTimeout(function(){
						$('#shopTreeContent').jScrollPane();
					},500);	
				} else {
					setTimeout(function(){
						$('#shopTreeContent').jScrollPane();
					},500);	
				}
			}).jstree({
		        "plugins": ["themes", "json_data","ui","checkbox"],
		        "themes": {
		            "theme": "classic",
		            "icons": false,
		            "dots": true
		        },
		        "json_data": {
		        	"data": data,
		        	"ajax" : {
		        		"method": "GET",
		                "url" : '/rest/catalog/sub-shop/programme-display/list.json?displayProgramId=' +$('#selId').val(),
		                "data" : function (n) {
		                        return { id : n.attr ? n.attr("id") : 0 };
		                    }
		            }
		        }
			});
		});
	},
	selectListProductQuota: function(groupId,gridId){
		$('#errMsgProductDlg').html('').hide();
		$('#successMsgProductDlg').html('').hide(); 
		$('#errMsgQuotaGroup').html('').hide();
		$.fancybox.update();
		var arrId = new Array();
		var msgError = ""; var strId = "";
		$('#productTree li').each(function(){
			var _id = $(this).attr('id');
			var type = $(this).attr('contentitemid');
			if($(this).hasClass('jstree-checked') && type=='product'){
				ProgrammeDisplayCatalog._mapProduct.put(_id,_id);		
			}else{
				ProgrammeDisplayCatalog._mapProduct.remove(_id);	
			}			
		});
		for(var i=0;i<ProgrammeDisplayCatalog._mapProduct.size();++i){
			var _obj = ProgrammeDisplayCatalog._mapProduct.get(ProgrammeDisplayCatalog._mapProduct.keyArray[i]);
			arrId.push(_obj);
			if (strId == "") strId += _obj;
			else strId +="," + _obj;
		}
		var msg = '';
		if(arrId.length == 0){
			msg = format(msgErr_required_choose_format,'Sản phẩm ');
		}
		if(msg.length > 0){
			$('#errMsgProductDlg').html(msg).show();
			$.fancybox.update();
			return false;
		}
		var moreCat = 0;		
		$('.jstree-no-icons').children().each(function(){
			if($(this).hasClass('jstree-checked') || $(this).hasClass('jstree-undetermined')){
				moreCat++;
			}
		});
		var displayId = $('#selId').val();
		var dataModel = new Object();		
		dataModel.id = groupId;
		dataModel.quota = true;
		var arrIdnew = new Array();
		if(moreCat>1){
			$.messager.confirm('Xác nhận', 'Bạn đang chọn các sản phẩm hơn 1 ngành hàng. Bạn có muốn lưu thông tin không?', function(r){
				if (r){
					// code check here
					$.getJSON('/rest/catalog/display-program/treeQouta/'+ strId +'/'+ $('#selId').val().trim() +'/list.json', function(data){
						if (data != null) 
						{
							dataModel.lstProductId = data[0];
							var productStrTemp = "";
							if (data[1].length > 0){
								for (var i=0; i< data[1].length; i++)
								{
									if (productStrTemp =="") productStrTemp += data[1][i];
									else productStrTemp += "," + data[1][i];
								}
								$.getJSON('/rest/catalog/display-program/treeQoutaShowErrorMsg/'+ productStrTemp +'/list.json', function(result){
									$('.fancybox-inner #errMsgProductDlg').html(result[0] +" đã tồn tại trong nhóm chỉ tiêu khác.").show();
								});
							}
							if (data[0].length > 0){ // co du lieu hop le de
														// luu
								Utils.saveData(dataModel, '/catalog/programme-display/product/save', ProgrammeDisplayCatalog._xhrSave, 'errMsgProductDlg', function(data){
									$('#successMsgProductDlg').html('Lưu dữ liệu thành công').show();
									$('#productCodeDlg').val('');
									$('#productNameDlg').val('');
									$('#numDisplay').val('');			
									Utils.resetCheckboxStateOfTree();
									ProgrammeDisplayCatalog.searchProductOnTreeQuota();
									$('#productCode').val('');
									$('#productName').val('');
									$('#quantity').val('');
									ProgrammeDisplayCatalog.searchProduct();
									$('#quotaGrid').collapseSubGridRow(ProgrammeDisplayCatalog._curRowId);
									$('#quotaGrid').expandSubGridRow(ProgrammeDisplayCatalog._curRowId);
									$.fancybox.update();
									$('#quotaGrid_'+groupId+'_t').trigger("reloadGrid"); 
									$.fancybox.close();
								}, null, '.fancybox-inner');	
							}
						}
					});
				}
			});
		} else {
			// code check here
			$.getJSON('/rest/catalog/display-program/treeQouta/'+ strId +'/'+ $('#selId').val().trim() +'/list.json', function(data){
				if (data != null) 
				{
					dataModel.lstProductId = data[0];
					var productStrTemp = "";
					if (data[1].length > 0){
						for (var i=0; i< data[1].length; i++)
						{
							if (productStrTemp =="") productStrTemp += data[1][i];
							else productStrTemp += "," + data[1][i];
						}
						$.getJSON('/rest/catalog/display-program/treeQoutaShowErrorMsg/'+ productStrTemp +'/list.json', function(result){
							$('.fancybox-inner #errMsgProductDlg').html(result[0] +" đã tồn tại trong các nhóm chỉ tiêu khác.").show();
						});
					}
					if (data[0].length > 0){ // co du lieu hop le de luu
						Utils.addOrSaveData(dataModel, '/catalog/programme-display/product/save', ProgrammeDisplayCatalog._xhrSave, 'errMsgProductDlg', function(data){
							$('#successMsgProductDlg').html('Lưu dữ liệu thành công').show();
							$('#productCodeDlg').val('');
							$('#productNameDlg').val('');
							$('#numDisplay').val('');
							Utils.resetCheckboxStateOfTree();
							ProgrammeDisplayCatalog.searchProductOnTreeQuota();
							$('#productCode').val('');
							$('#productName').val('');
							$('#quantity').val('');
							ProgrammeDisplayCatalog.searchProduct();
							$('#quotaGrid').collapseSubGridRow(ProgrammeDisplayCatalog._curRowId);
							$('#quotaGrid').expandSubGridRow(ProgrammeDisplayCatalog._curRowId);
							$.fancybox.update();
							$('#quotaGrid_'+groupId+'_t').trigger("reloadGrid");
							$.fancybox.close();
						}, null, '.fancybox-inner', null, null);
					}
				}
			});
		}
		return false;
	},
	getDisplayProgramExclusionUrl:function(code,name,displayProgramExclusion){
		code = encodeChar(code);
		name = encodeChar(name);
		return '/catalog/programme-display/program/search?code='+encodeChar(code)+'&name='+encodeChar(name)+'&displayProgramExclusion='+displayProgramExclusion;
	},
	searchDisplayProgramExclusion:function(){
		var code = $('#vnmDisplayProgramCode').val().trim();
		var name = $('#vnmDisplayProgramName').val().trim();
		var textCode = $('#code').val().trim();
		var textExclusion = $('#exclusion').val().trim();
		var displayProgramExclusion = textCode + ',' + textExclusion;
		var url = ProgrammeDisplayCatalog.getDisplayProgramExclusionUrl(code, name,displayProgramExclusion);			
		$("#vnmDisplayProgramGird").setGridParam({url:url,page:1}).trigger("reloadGrid");		
		return false;
	},
	showDisplayProgramExclusion:function(){
		$('#exclustionErr').html('').hide();
		var textCode = $('#code').val().trim();
		var textExclusion = $('#exclusion').val().trim();
		var displayProgramExclusion = textCode + ',' + textExclusion;	
		ProgrammeDisplayCatalog._mapExclusion = new Map();
		var html = $('#displayProgramExclusionDialog').html();		
		$.fancybox(html,{
			modal: true,
			title: 'Danh sách CTTB',
			afterShow: function(){
				$('#displayProgramExclusionDialog').html('');
				$("#vnmDisplayProgramGird").jqGrid({
					  url:ProgrammeDisplayCatalog.getDisplayProgramExclusionUrl('', '',displayProgramExclusion),
					  multiselect: true,
					  colModel:[							    
					    {name: 'displayProgramCode',index:'displayProgramCode', label: 'Mã CTTB', width: 120, sortable:false,resizable:false, align: 'left', formatter: function(value, row, index) {
							return Utils.XSSEncode(value);
						}},
					    {name: 'displayProgramName',index:'displayProgramName', label: 'Tên CTTB', sortable:false,resizable:false, align: 'left', formatter: function(value, row, index) {
							return Utils.XSSEncode(value);
						}},				    							    
					    {name: 'id', index:'id', hidden: true}					    
					  ],
					  pager : $('#vnmDisplayProgramPager'),
					  height: 'auto',
					  rowNum: 10,							 
					  width: ($('#vnmDisplayProgramContainerGrid').width()),
					  beforeRequest:function(){
						  $('.ui-jqgrid-bdiv .cbox').each(function(){							
							var code = $("#vnmDisplayProgramGird").jqGrid ('getCell', $(this).parent().parent().attr('id'), 'displayProgramCode');							
							if($(this).is(':checked')){																
								ProgrammeDisplayCatalog._mapExclusion.put(code,code);
							}else{
								var obj = ProgrammeDisplayCatalog._mapExclusion.get(code);
								if(obj!=null){
									ProgrammeDisplayCatalog._mapExclusion.remove(code);
								}
							}
						  });
					  },
					  gridComplete: function(){
						  $('#jqgh_vnmDisplayProgramGird_rn').html('STT');
						  Utils.bindAutoSearch();
						  $('.ui-jqgrid-bdiv .cbox').live('change', function(){
								if ($(".ui-jqgrid-bdiv .cbox").length == $(".ui-jqgrid-bdiv .cbox:checked").length){
									$('#cb_vnmDisplayProgramGird').attr('checked', 'checked');
								}
								else{
									$('#cb_vnmDisplayProgramGird').attr('checked',false);
								}								
						  });
						  $('.ui-jqgrid-bdiv .cbox').each(function(){
							  var code = $("#vnmDisplayProgramGird").jqGrid ('getCell', $(this).parent().parent().attr('id'), 'displayProgramCode');
							  var obj = ProgrammeDisplayCatalog._mapExclusion.get(code);
							  if(obj!=null){
								  $(this).attr('checked', 'checked');
							  }
							  $(this).parent().css('padding', '3px 3px');
						  });
						  $(window).resize();
						  $.fancybox.update();
						  updateRownumWidthForJqGrid('.fancybox-inner');
					  }
					})
					.navGrid($('#vnmDisplayProgramPager'), {edit:false,add:false,del:false, search: false});
			},
			afterClose: function(){
				$('#displayProgramExclusionDialog').html(html);	
			}
		});
		return false;
	},
	selectListDisplayProgramExclusion:function(){	
		$('.ui-jqgrid-bdiv .cbox').each(function(){							
			var code = $("#vnmDisplayProgramGird").jqGrid ('getCell', $(this).parent().parent().attr('id'), 'displayProgramCode');							
			if($(this).is(':checked')){																
				ProgrammeDisplayCatalog._mapExclusion.put(code,code);
			}
		 });
		if(ProgrammeDisplayCatalog._mapExclusion.size()==0){
			$('#exclustionErr').html('Vui lòng chọn CTTB loại trừ').show();
			return false;
		}
		var lst = '';
		for(var i=0;i<ProgrammeDisplayCatalog._mapExclusion.size();i++){			
			var obj =ProgrammeDisplayCatalog._mapExclusion.get(ProgrammeDisplayCatalog._mapExclusion.keyArray[i]);
			lst += obj + ',';
		}
		lst = lst.substring(0,lst.length-1);
		var exclusion = $('#exclusion').val().trim();
		if(exclusion.length>0){
			$('#exclusion').val(exclusion + ',' + lst);
		}else{
			$('#exclusion').val(lst);
		}
		$.fancybox.close();
	},	
	showArchives: function(){
		ProgrammeDisplayCatalog.hideAllTab();
		$('#tab8').addClass('Active');
		$('#container8').show();
	}
	,
	showExclusionProgram: function(){
		ProgrammeDisplayCatalog.hideAllTab();
		$('#tab1').addClass('Active');
		$('#container2').show();
		$('#displayProgramCode').bind('keyup', function(event){
			if(event.keyCode == 13){
				ProgrammeDisplayCatalog.searchExclusionProgram();
			}
		}).val('');
		$('#displayProgramName').bind('keyup', function(event){
			if(event.keyCode == 13){
				ProgrammeDisplayCatalog.searchExclusionProgram();
			}
		}).val('');
		ProgrammeDisplayCatalog.searchExclusionProgram();
	},
	searchExclusionProgram:function(){
		var selId = $('#selId').val();
		var code = $('#displayProgramCode').val().trim();
		var name = $('#displayProgramName').val().trim();
		var url = '/programme-display/searchExclusionProgram?id=' + encodeChar(selId) + '&code='+encodeChar(code)+'&name='+encodeChar(name);
		$('#exclusionGrid').datagrid({url:url,pageNumber:1});
	},
	openExclusionProgram:function(){
		var selId = $('#selId').val();
		var arrayParam = new Array();
		var param = new Object();
		param.name = 'id';
		param.value = selId;
		arrayParam.push(param);
		
		ProgrammeDisplayCatalog.openSearchStyleExclusionProgramEasyUIDialog("Mã CTTB", "Tên CTTB",
				"Chọn CTTB loại trừ", "/programme-display/searchExclusionProgramPopup", null,
				"displayProgramCode", "displayProgramName", arrayParam);
		return false;
	},
	deleteExclusionProgram:function(id){
		var dataModel = new Object();
		dataModel.id = id;
		Utils.addOrSaveData(dataModel, "/programme-display/deleteExclusionProgram", ProgrammeDisplayCatalog._xhrSave, 'errMsgExclusion', function(data){
			ProgrammeDisplayCatalog.searchExclusionProgram();
		},null,null,null,"Bạn có muốn xóa CTTB loại trừ này không?",function(data){
			$('#errMsgExclusion').html(data.errMsg).show();
			var tm = setTimeout(function(){
				$('#successMsgExclusion').html('').hide();
				clearTimeout(tm); 
			}, 3000);
		});
	},
	getShopGridUrl: function(displayProgramId,shopCode,shopName){
		return "/programme-display/searchshop?id="+ encodeChar(displayProgramId) + "&shopCode=" + encodeChar(shopCode) + "&shopName=" + encodeChar(shopName);
	},
	searchShop:function(){
		var code = $('#shopCode').val().trim();
		var name = $('#shopName').val().trim();
		var id = $('#selId').val();
		var url = ProgrammeDisplayCatalog.getShopGridUrl(id,code,name);
		$("#exGrid").treegrid({url:url});
		$('#errExcelMsg').html("").hide();
		ProgrammeDisplayCatalog._listShopIdEx = new Array();
		ProgrammeDisplayCatalog._listShopId = new Array();
	},
	openShopDialog:function(id){
		var selId = $('#selId').val();
		var arrParam = new Array();
 		var param = new Object();
 		param.name = 'shopId';
 		if(id != null && id != undefined){
 	 		param.value = id;
 		}
 		var param1 = new Object();
 		param1.name = 'focusId';
 		param1.value = selId;
 		arrParam.push(param);
 		arrParam.push(param1);
 		var param2 = new Object();
 		param2.name = 'stringProgram';
 		param2.value = 'display';
 		arrParam.push(param2);
 		CommonSearch.searchShopDisplayProgram(function(data){
 				// $('#promotionProgramCode').val(data.code);
 		},arrParam);
 		ProgrammeDisplayCatalog.bindFormat();
	},
	onCheckShop:function(id){
		 var isCheck = $('.easyui-dialog #check_'+id).attr('checked');
		 if($('.easyui-dialog #check_'+id).is(':checked')){
	    	ProgrammeDisplayCatalog._listShopId.push(id);
	    	var classParent = $('.easyui-dialog #check_'+id).attr('class');
			var flag = true;
			var count = 0;
			ProgrammeDisplayCatalog.recursionFindChildShopCheck(id); 
		}else{
			ProgrammeDisplayCatalog.removeArrayByValue(ProgrammeDisplayCatalog._listShopId,id);
			var classParent = $('.easyui-dialog #check_'+id).attr('class');
			var flag = true;
			var count = 0;
			ProgrammeDisplayCatalog.recursionFindChildShopUnCheck(id);  
			ProgrammeDisplayCatalog.recursionFindParentShopUnCheck(id);
		}
	},
	onCheckShopEx:function(id){
		var isCheck = $('#check_'+id).attr('checked');
	    if($('#check_'+id).is(':checked')){
	    	//ProgrammeDisplayCatalog._listShopIdEx.push(id);
	    	var classParent = $('#check_'+id).attr('class');
			var flag = true;
			var count = 0;
			ProgrammeDisplayCatalog.recursionFindChildShopDeleteCheck(id); 
		}else{
			//ProgrammeDisplayCatalog.removeArrayByValue(ProgrammeDisplayCatalog._listShopIdEx,id);
			var classParent = $('#check_'+id).attr('class');
			var flag = true;
			var count = 0;
			ProgrammeDisplayCatalog.recursionFindChildShopDeleteUnCheck(id);
			ProgrammeDisplayCatalog.recursionFindParentShopUnCheck(id); 
		}
	},
	createDisplayProgramShopMap:function(){
		var params = new Object();
		params.listShopId = ProgrammeDisplayCatalog._listShopId;
		params.id = $('#selId').val();
		if(ProgrammeDisplayCatalog._listShopId.length<=0){
			$('.easyui-dialog #errMsgDialog').html('Bạn chưa chọn đơn vị để thêm').show();
			return false;
		}
		Utils.addOrSaveData(params, '/programme-display/createDisplayProgramShopMap', null, 'errMsgDialog', function(data){
			if(data.error == undefined && data.errMsg == undefined){
				ProgrammeDisplayCatalog.searchShop();
				ProgrammeDisplayCatalog._listShopId = new Array();
				$('#searchStyleShopDisplay1').dialog('close');
				$('#errExcelMsg').html("").hide();
				$('#successMsgShop').html('Lưu dữ liệu thành công.').show();
				var tm = setTimeout(function(){
					$('#successMsgShop').html('').hide();
					clearTimeout(tm); 
				}, 3000);
			} 
		}, null, null, null, "Bạn có muốn thêm đơn vị này ?", null);
	},
	deleteDisplayProgramShopMap:function(shopId){
		var params = new Object();
		params.listShopId = ProgrammeDisplayCatalog._listShopIdEx;
		params.id = $('#selId').val();
		if(ProgrammeDisplayCatalog._listShopIdEx.length<=0){
			$('#errExcelMsg').html('Bạn chưa chọn đơn vị để xóa').show();
			return false;
		}
		Utils.addOrSaveData(params, '/programme-display/deleteDisplayProgramShopMap', null, 'errExcelMsg', function(data){
			ProgrammeDisplayCatalog.searchShop();
			$('#errExcelMsg').html("").hide();
			$('#successMsgShop').html('Xóa dữ liệu thành công.').show();
			var tm = setTimeout(function(){
				$('#successMsgShop').html('').hide();
				clearTimeout(tm); 
			}, 3000);
		}, null, null, null, "Bạn có muốn xóa đơn vị này ?", null);
	},
	exportExcelShopMap:function(){
		$('#divOverlay').show();
		$('#errExcelMsg').html('').hide();
	    var focusId = $('#selId').val();
		var shopCode = $('#shopCode').val().trim();
		var shopName = $('#shopName').val().trim();
		
		var data = new Object();
		data.id = focusId;
		data.shopCode = shopCode;
		data.shopName = shopName;
		
		var url = "/programme-display/exportDisplayProgramShopMap";
		CommonSearch.exportExcelData(data,url);
	},
	importExcelShopMap:function(){
 		$('#isView').val(0);
 		var options = { 
				beforeSubmit: ProgrammeDisplayCatalog.beforeImportExcel,   
		 		success:      ProgrammeDisplayCatalog.afterImportExcelShopMap,  
		 		type: "POST",
		 		dataType: 'html',
		 		data:({excelType:6,id:$('#selId').val()})
		 	}; 
		$('#importFrm').ajaxForm(options);
 		$('#importFrm').submit();
 		return false;
 	},
	bindFormat:function(){
		$('#searchStyleShopContainerGrid input[type=text]').each(function(){
				Utils.bindFormatOnTextfieldEx1($(this).attr('id'),Utils._TF_NUMBER,null,'#errMsgDialog','Số suất phải là số nguyên dương.');
			}); 
	},
	removeArrayByValue:function(arr,val){
		for(var i=0; i<arr.length; i++) {
	        if(arr[i] == val){
	        	arr.splice(i, 1);
		        break;
	        }
	    }
	},
	importDisplayProgram: function(){
		$.messager.confirm('Xác nhận','Bạn có muốn nhập từ file?',function(r){
			if(r){				
		 		$('#importFrm').submit();
			}
		});	
	},
	beforeImportExcel: function(){
		if(!previewImportExcelFile(document.getElementById("excelFile"))){
			return false;
		}
		//showLoading();
		$('#errExcelMsg').html('').hide();
		$('#successMsg1').html('').hide();
		showLoadingIcon();
		return true;
	},
	afterImportExcelDisplayProgram: function(responseText, statusText, xhr, $form){
		hideLoadingIcon();
		if (statusText == 'success') {				
	    	$("#responseDiv").html(responseText);
	    	var tokenHiddenFiled = $('#tokenHiddenFiled').val();
	    	if(tokenHiddenFiled!=null && tokenHiddenFiled!=undefined){
	    		Utils.setToken(tokenHiddenFiled.trim());
	    		$('#importTokenVal').val(tokenHiddenFiled.trim());
	    	}	
	    	if($('#errorExcel').html().trim() == 'true' || $('#errorExcelMsg').html().trim().length > 0){
	    		$('#errExcelMsg').html($('#errorExcelMsg').html()).show();
	    	} else {
	    		if($('#typeView').html().trim() == 'false'){
	    			var totalRow = parseInt($('#totalRow').html().trim());
	    			var numFail = parseInt($('#numFail').html().trim());
	    			var fileNameFail = $('#fileNameFail').html();
	    			var mes = format(msgErr_result_import_excel,(totalRow - numFail),numFail);		    		
	    			if(numFail > 0){
	    				mes+= ' <a href="'+ fileNameFail +'">Xem chi tiết lỗi</a>';
	    			}
	    			if($('#excelFile').length!=0 && $('#fakefilepc').length!=0){//xóa link khi import
	    				try{$('#excelFile').val('');$('#fakefilepc').val('');}catch(err){}
	    			}
	    			$('#errExcelMsg').html(mes).show();
	    			$("#listCTTBGrid").datagrid("reload");

	    		}
	    		}
	    	}
	},
	afterImportExcelShopMap: function(responseText, statusText, xhr, $form){
		hideLoadingIcon();
		if (statusText == 'success') {				
			$("#responseDiv").html(responseText);
			var tokenHiddenFiled = $('#tokenHiddenFiled').val();
			if(tokenHiddenFiled!=null && tokenHiddenFiled!=undefined){
				Utils.setToken(tokenHiddenFiled.trim());
				$('#importTokenVal').val(tokenHiddenFiled.trim());
			}	
			if($('#errorExcel').html().trim() == 'true' || $('#errorExcelMsg').html().trim().length > 0){
				$('#errExcelMsg').html($('#errorExcelMsg').html()).show();
			} else {
				if($('#typeView').html().trim() == 'false'){
					var totalRow = parseInt($('#totalRow').html().trim());
					var numFail = parseInt($('#numFail').html().trim());
					var fileNameFail = $('#fileNameFail').html();
					var mes = format(msgErr_result_import_excel,(totalRow - numFail),numFail);		    		
					if(numFail > 0){
						mes+= ' <a href="'+ fileNameFail +'">Xem chi tiết lỗi</a>';
					}
					if($('#excelFile').length!=0 && $('#fakefilepc').length!=0){//xóa link khi import
						try{$('#excelFile').val('');$('#fakefilepc').val('');}catch(err){}
					}
					$('#errExcelMsg').html(mes).show();
				}
    		}
    	}
	},
	importExcelCustomer : function(){ 
		/*$('.easyui-dialog #errExcelMsg').html('').hide();
		if($('.easyui-dialog #excelFileCus').val() ==  ""){
			$('.easyui-dialog #errExcelMsg').html("Vui lòng chọn file Excel").show();
			return false;
		}*/
		var id = $('#selId').val();
		var month = $('#month1').val();
		var shopId = $('#shopTreeId .combo-value').val();
		/*var options = { 
			beforeSubmit: ProgrammeDisplayCatalog.beforeImportExcel,   
	 		success:      ProgrammeDisplayCatalog.afterImportExcel,
	 		type: "POST",
	 		dataType: 'html',
	 		data:({id:id,month:month,shopId:shopId})
		}; 
		$.messager.confirm('Xác nhận','Bạn có muốn nhập từ file?',function(r){
			if(r){				
				$('.easyui-dialog #importFrmCus').ajaxForm(options);
		 		$('.easyui-dialog #importFrmCus').submit();
			}
		});	
 		return false;*/
		$('.ErrorMsgStyle').hide();
		VTUtilJS.importExcelUtils(function(data) {
			$("#customerGrid123").datagrid("reload");
			$(".easyui-dialog #errExcelMsg").html(data.message).show();
			$("#fakefilepcCustomer").val("");
			$("#excelFileCus").val("");
		}, "importFrmCus", "excelFileCus", null, "errExcelMsg", {id:id,month:month,shopId:shopId});
		return false;
	},
	importExcelStaff : function() {
		$('#inmonth').val($('date').val);
		$('.ErrorMsgStyle').hide();
		if ($('#dateStrExcel').val() ==  "") {
			$('#errExcelMsg').html("Vui lòng chọn tháng cần nhập file").show();
			$('#dateStrExcel').focus();
			return false;
		}
		/*if($('#excelFile').val() ==  ""){
			$('#errExcelMsg').html("Vui lòng chọn file Excel").show();
			return false;
		}
		var options = {
			beforeSubmit : ProgrammeDisplayCatalog.beforeImportExcel,
			success : ProgrammeDisplayCatalog.afterImportExcelStaff,
			type : "POST",
			dataType : 'html'
		};
		$('#easyuiPopup #importFrm').ajaxForm(options);
		$.messager.confirm('Xác nhận','Bạn có muốn nhập từ file?',function(r){
			if(r){				
				$('#easyuiPopup #importFrm').submit();
			}
		});		
		return false;*/
		$('.ErrorMsgStyle').hide();
		VTUtilJS.importExcelUtils(function(data) {
			$("#staffGrid").datagrid("reload");
			$("#errExcelMsg").html(data.message).show();
			$("#fakefilepc").val("");
			$("#excelFile").val("");
		}, "importFrm", "excelFile", null, "errExcelMsg");
		return false;
	},
	updateRownumWidthForJqGridEX1:function(parentId,colReduceWidth){	
		var pId = '';
		if(parentId!= null && parentId!= undefined){
			pId = parentId + ' ';
		}
		var lastValue=$(pId + '.datagrid-cell-rownumber').last().text().trim().length;
		var widthSTT = $('.datagrid-cell-rownumber').width();
		var s = $('.datagrid-header-row td[field='+colReduceWidth+'] div').width();
		if(ProgrammeDisplayCatalog._lstSize != null && ProgrammeDisplayCatalog._lstSize != undefined){ 
			s = ProgrammeDisplayCatalog._lstSize[0];
		}
		if(lastValue > 0){
			var extWidth = 25;
			if(lastValue > 2){
				extWidth += (lastValue - 2) * 9;
			}
			var value = extWidth - widthSTT;
			s = s - value;
			$(pId + '.datagrid-cell-rownumber').css('width',extWidth);
			$(pId + '.datagrid-header-rownumber').css('width',extWidth);
			$(parentId + ' .datagrid-row').each(function(){
				$(parentId + ' .datagrid-header-row td[field='+colReduceWidth+'] div').width(s);
				$(parentId + ' .datagrid-row td[field='+colReduceWidth+'] div').width(s);
			});
			
		}
	},
	exportExcelCTTB: function() {
		$('.ErrorMsgStyle').hide();
		var dataModel = new Object();
		if($('#totalRowGrid').val() == 0){
			$('#errCustomerMsg').html("Không có dữ liệu để xuất").show();
			return false;
		}else{
			$.messager.confirm('Xác nhận', 'Bạn có muốn xuất file này.', function(r){
				if (r){
					var shopKendo = $("#shop").data("kendoMultiSelect");
		            var lstShopId = shopKendo.value();
		            if(lstShopId != null && lstShopId != undefined){
						dataModel.listShopIdString = lstShopId.toString();
		            }

				dataModel.id = $('#displayProgram').val().trim();
				dataModel.month = $('#month1').val().trim();
				ReportUtils.exportReport('/programme-display/customer/export', dataModel, 'errCustomerMsg');
				}
			});
		}
		return false;
	},
	saveProductDlgDS:function(){
		var params = new Object();
		params.id = $('#selId').val().trim();
		var groupId = ProgrammeDisplayCatalog.groupIdDS;
		var groupType = ProgrammeDisplayCatalog.groupTypeDS;
		if (groupType == undefined || groupType == null){
			groupType = $('#tmpGroupType').val();
		}
		if (groupId == undefined || groupId == null){
			groupId = $('#tmpGroupId').val();
		} 
		params.typeGroup = groupType;
		params.groupId = groupId;
		if(groupType==2){
			var listChecked = ProgrammeDisplayCatalog.lstProductId.keyArray;  //$('#searchProductAmountGrid').datagrid('getChecked');
	    	for(var i = 0; i < listChecked.length; i++) {
	    		var convfact = $('#product-'+listChecked[i]).val();
	    		if(convfact == null || convfact == '' || isNaN(convfact)) {
	    			
	    		} else {
	    			ProgrammeDisplayCatalog.lstConvfact.put(listChecked[i], convfact);
	    		}
	    	}
	    	if (ProgrammeDisplayCatalog.lstConvfact == null ||
	    			ProgrammeDisplayCatalog.lstConvfact.keyArray.length == 0){
	    		$('#errProductMsgSearch').html("Bạn chưa chọn sản phẩm nào.").show();
	    		return false;
	    	}
			params.lstConvfact = ProgrammeDisplayCatalog.lstConvfact.valArray;
    		params.lstProductId = ProgrammeDisplayCatalog.lstProductId.keyArray;        		
    		Utils.addOrSaveRowOnGrid(params, "/programme-display/change-amount-product", ProgrammeDisplayCatalog._xhrSave, 'amountGrid','errMsgSearch', function(data){
    			showSuccessMsg('successMsgProductDlg',data);
    			var selId = $('#selId').val();
    			var url = '';		
    			url = '/programme-display/search-product?id=' + encodeChar(selId) + '&typeGroup=4';
//        			ProgrammeDisplayCatalog.dataGridProductGroup('cbGrid',url,ProgrammeDisplayCatalog.DISPLAY);
    			var subGridId = $('#searchProductEasyUIDialog #subGridId').val();
    			ProgrammeDisplayCatalog.lstConvfact = null;
    			$('#tmpCheckedListFlag').val("");
    			$('#searchProductEasyUIDialog').window('close');
    			$('#'+subGridId).datagrid('reload');
    		},null);
		
		}else{
			if (ProgrammeDisplayCatalog.lstProductId == null ||
	    			ProgrammeDisplayCatalog.lstProductId.keyArray.length == 0){
	    		$('#errProductMsgSearch').html("Bạn chưa chọn sản phẩm nào.").show();
	    		return false;
	    	}
			params.lstProductId = ProgrammeDisplayCatalog.lstProductId.keyArray;
			Utils.addOrSaveRowOnGrid(params, "/programme-display/change-amount-product", ProgrammeDisplayCatalog._xhrSave, 'amountGrid','errMsgSearch', function(data){
    			showSuccessMsg('successMsgProductDlg',data);
    			var selId = $('#selId').val();
    			var url = '';		
    			url = '/programme-display/search-product?id=' + encodeChar(selId) + '&typeGroup=4';
//        			ProgrammeDisplayCatalog.dataGridProductGroup('cbGrid',url,ProgrammeDisplayCatalog.DISPLAY);
    			var subGridId = $('#searchProductEasyUIDialog #subGridId').val();
    			ProgrammeDisplayCatalog.lstProductId = null;
    			$('#searchProductEasyUIDialog').window('close');
    			$('#'+subGridId).datagrid('reload');
    		},null);
		}
	},
	openSearchProductDialog: function(groupType, groupId, subGridId){
		ProgrammeDisplayCatalog.groupIdDS = 	groupId;	
		ProgrammeDisplayCatalog.groupTypeDS = groupType;
		var html = $('#searchProductEasyUIDialog').html();
		var productName = $("#seachProductName").val().trim();
		var productCode = $("#seachProductCode").val().trim();
		var displayProgrameId = $("#selId").val().trim();
		
		if (groupType == undefined || groupType == null)
			groupType = $("#tmpGroupType").val();
		$("#tmpGroupType").val(groupType);
		if (groupId == undefined || groupId == null){
			groupId = $('#tmpGroupId').val();
		}
		if (subGridId == undefined || subGridId == null ){
			subGridId = $('#tmpSubGridId').val();
		}
		$('#tmpSubGridId').val(subGridId);
		$("#tmpGroupId").val(groupId);
		$('#searchProductDialogDiv').show();

		if (ProgrammeDisplayCatalog.lstProductId == null || ProgrammeDisplayCatalog.lstProductId.keyArray.length == 0){
			ProgrammeDisplayCatalog.lstProductId = new Map();
		}
		if (ProgrammeDisplayCatalog.lstConvfact == null || ProgrammeDisplayCatalog.lstConvfact.keyArray.length == 0){
			ProgrammeDisplayCatalog.lstConvfact = new Map();
		}
		if ($('#tmpCheckedListFlag').val() == "1"){
			var lstTmpGridRows = $('#searchProductGrid').datagrid('getChecked');
			if (lstTmpGridRows == undefined || lstTmpGridRows == null || lstTmpGridRows.length == 0){
				// do nothing
			}else{
				for(var i = 0; i < lstTmpGridRows.length; i++) {
		    		var convfact = $('#product-'+lstTmpGridRows[i].id).val();
		    		if(convfact == null || convfact == '' || isNaN(convfact)) {
		    			
		    		} else {
		    			ProgrammeDisplayCatalog.lstConvfact.put(lstTmpGridRows[i].id, convfact);
		    		}
		    	}
			}
		}
		lstIndex = new Map();
		$('#searchProductEasyUIDialog').dialog({
			 	title: 'Chọn sản phẩm thêm vào nhóm',  
		        closed: false,  
		        cache: false,  
		        modal: true,
		        width : 580,
		        height :500,
		        onOpen: function(){
		        	$('#searchProductEasyUIDialog #seachProductCode,#searchProductEasyUIDialog #seachProductName').bind('keypress',function(event){
		        		if(event.keyCode == keyCodes.ENTER){
		        			$('#searchProductEasyUIDialog #btnSearchProduct').click();
		        		}
		        	});
		        	$('#searchProductEasyUIDialog #subGridId').val(subGridId);
		        	$('#searchProductContainerGrid').html('<table id="searchProductGrid" style="width: 520px;"></table>');
		        	if(groupType == 2) {
		        		$("#searchProductEasyUIDialog #btnSearchProduct").click(function(){
		        			$('#searchProductAmountGrid').datagrid('load',{
		        				productName: $('#seachProductName').val().trim(),
		        				productCode: $('#seachProductCode').val().trim()
		        				});
		        			});
		        		$('#searchProductAmountGrid').datagrid({
		        			url : '/programme-display/search-group-product?id='+displayProgrameId+'&typeGroup='+groupType,
							autoRowHeight : true,
							rownumbers : true, 
							//checkOnSelect :true,
							pagination:true,
							rowNum : 10,
							scrollbarSize : 0,
							fitColumns:true,
							pageList  : [10,20,30],
							queryParams: {
								productName: productName,
								productCode: productCode
							},
							width : ($('.easyui-dialog #searchProductContainerGrid').width()),
						    columns:[[  
						        {field:'catCode',title:"Ngành",align:'left', width:110, sortable : false,resizable : false,formatter:function(value,row,index){
						        	return Utils.XSSEncode(row.catCode);
						        }},  
						        {field:'productCode',title:"Mã sản phẩm",align:'left', width:350, sortable : false,resizable : false,formatter:function(value,row,index){
						        	return Utils.XSSEncode(row.productCode);
						        }},  
						        {field:'productName', title:'Tên sản phẩm', align:'left',width:350, sortable : false,resizable : false,formatter:function(value,row,index){
						        	return Utils.XSSEncode(row.productName);
						        }},
						        {field:'convfact',title:'Quy đổi', align: 'right', width: 100, sortable : false, resizable: false, formatter: function(value,row,index){
						        	return '<input type="text" id="product-'+row.productId+'" value="'+row.convfact+'"  maxlength="7">';
						        }},
								{field: 'id',checkbox:true}
						    ]],
						    onBeforeLoad : function() {
						    	var listChecked = $('#searchProductAmountGrid').datagrid('getChecked');
						    	for(var i = 0; i < listChecked.length; i++) {
						    		var convfact = $('#product-'+listChecked[i].id).val();
						    		if(convfact == null || convfact == '' || isNaN(convfact)) {
						    			
						    		} else {
						    			ProgrammeDisplayCatalog.lstConvfact.put(listChecked[i].id, convfact);
						    		}
						    	}
						    },
						    onLoadSuccess :function(){
						    	$('#errProductMsgSearch').html("");
						    	var rows = $('#searchProductAmountGrid').datagrid('getRows');
						    	for(var i = 0; i < rows.length; i++) {
						    		//$('#searchProductGrid').datagrid('beginEdit', i);
						    		if(ProgrammeDisplayCatalog.lstConvfact.get(rows[i].id) != null) {
						    			$('#searchProductAmountGrid').datagrid('checkRow', i);
						    			$('#product-'+rows[i].id).val(ProgrammeDisplayCatalog.lstConvfact.get(rows[i].id));;
						    		}
						    	}
						    	$('#seachProductCode').focus();
						    	$('#tmpCheckedListFlag').val("1");
						    	$('.datagrid-header-check input').removeAttr("checked");
						    },
						    onCheck:function(index, row){
						    	$('#errProductMsgSearch').html("");
						    	ProgrammeDisplayCatalog.lstProductId.put(row.productId, row);
						    },
						    onUncheck:function(index, row){
						    	ProgrammeDisplayCatalog.lstProductId.remove(row.productId);
						    },
						    onCheckAll:function(rows){
						    	for(var i = 0;i<rows.length;i++){
						    		var index = i;
						    		var row = rows[i];
						    		ProgrammeDisplayCatalog.lstProductId.put(row.productId, row);
						    	}
						    },
						    onUncheckAll:function(rows){
						    	for(var i = 0;i<rows.length;i++){
						    		var index = i;
						    		var row = rows[i];
						    		ProgrammeDisplayCatalog.lstProductId.remove(row.productId);
						    	}
						    }
		        		});
		        	} else {
		        		$("#searchProductEasyUIDialog #btnSearchProduct").click(function(){
		        			$('#searchProductAmountGrid').datagrid('load',{
		        				productName: $('#seachProductName').val().trim(),
		        				productCode: $('#seachProductCode').val().trim()
		        				});
		        			});
		        		$('#searchProductAmountGrid').datagrid({
		        			url : '/programme-display/search-group-product?id='+displayProgrameId+'&typeGroup='+groupType,
							autoRowHeight : true,
							rownumbers : true, 
							//checkOnSelect :true,
							pagination:true,
							rowNum : 10,
							scrollbarSize : 0,
							fitColumns:true,
							pageList  : [10,20,30],
							queryParams: {
								productName: productName,
								productCode: productCode
							},
							width : ($('.easyui-dialog #searchProductContainerGrid').width()),
						    columns:[[  
						        {field:'catCode',title:"Ngành",align:'left', width:110, sortable : false,resizable : false,formatter:function(value,row,index){
						        	return Utils.XSSEncode(row.catCode);
						        }},  
						        {field:'productCode',title:"Mã sản phẩm",align:'left', width:350, sortable : false,resizable : false,formatter:function(value,row,index){
						        	return Utils.XSSEncode(row.productCode);
						        }},  
						        {field:'productName', title:'Tên sản phẩm', align:'left',width:350, sortable : false,resizable : false,formatter:function(value,row,index){
						        	return Utils.XSSEncode(row.productName);
						        }},
								{field: 'id',checkbox:true}
						    ]],
						    onLoadSuccess :function(){
						    	$('#errProductMsgSearch').html("");
						    	var rows = $('#searchProductAmountGrid').datagrid('getRows');
						    	for(var i = 0; i < rows.length; i++) {
						    		if(ProgrammeDisplayCatalog.lstProductId.get(rows[i].id)) {
						    			$('#searchProductAmountGrid').datagrid('checkRow', i);
						    		}
						    	}
						    	$('#seachProductCode').focus();
						    	$('.datagrid-header-check input').removeAttr("checked");
						    },
						    onCheck:function(index, row){
						    	$('#errProductMsgSearch').html("");
						    	ProgrammeDisplayCatalog.lstProductId.put(row.productId, row);
						    },
						    onUncheck:function(index, row){
						    	ProgrammeDisplayCatalog.lstProductId.remove(row.productId);
						    },
						    onCheckAll:function(rows){
						    	for(var i = 0;i<rows.length;i++){
						    		var index = i;
						    		var row = rows[i];
						    		ProgrammeDisplayCatalog.lstProductId.put(row.productId, row);
						    	}
						    },
						    onUncheckAll:function(rows){
						    	for(var i = 0;i<rows.length;i++){
						    		var index = i;
						    		var row = rows[i];
						    		ProgrammeDisplayCatalog.lstProductId.remove(row.productId);
						    	}
						    }
		        		});
		        	}
		        },
		        onClose: function() {
		        	$('#searchProductEasyUIDialog #seachProductCode,#searchProductEasyUIDialog #seachProductName').unbind('keypress');
		        	$('#searchProductEasyUIDialog').html(html);
		        	$('#searchProductDialogDiv').hide();
		        	ProgrammeDisplayCatalog.lstConvfact = null;
					ProgrammeDisplayCatalog.lstProductId = null;
					$('#tmpCheckedListFlag').val("");
		        }
		});
	},
	openPopupImportCustomer: function(){
		$('#fakefilepcCustomer').val('');
		$('#easyuiPopup1').show();
		$('.easyui-dialog #excelFileCus').val('');
		$('.ErrorMsgStyle').hide();
		var shopId = $('#shopTreeId .combo-value').val();
		var month = $('#month1').val();
		var id = $('#selId').val();
		ReportUtils._callBackAfterImport = function(){
			$('#customerGrid123').datagrid('load',{shopId:shopId,month:month,id:id,pageNumber:1});
			ReportUtils._callBackAfterImport=null;
		};
		$('#easyuiPopup1').dialog('open');
	},
	searchProductPressKey: function(e){
		if (typeof e == undefined && window.event) { e = window.event; }
        if (e.keyCode == 13)
        {
            document.getElementById('btnSearchProduct').click();
        }
	},
	uncheckAll:function(){
		/*if($('#checkDeleteAll').is(':checked')){
			$('#customerGrid123').datagrid('checkAll');
			$('#checkDeleteAll').prop('checked', true);
		}else{
			ProgrammeDisplayCatalog._lstCustomerId = new Map();
			$('#customerGrid123').datagrid('uncheckAll');
		}*/
		if (ProgrammeDisplayCatalog._lstProduct.size() > 0) {
			if($('#checkDeleteAll').is(':checked')){
				$('#customerGrid123').datagrid('uncheckAll');
				$('#checkDeleteAll').attr('checked', "checked");
			}
		}
	},
	deleteCustomerDisplayProgram:function(){
		$('.ErrorMsgStyle').hide();
		var deleteAll = $('#checkDeleteAll').attr('checked');
		var month = $('#filterMonth').val();
		var arrMonth = month.split('/');
		var monthTxt = arrMonth[0];
		var yearTxt = arrMonth[1];
		var now = new Date();
		var cYear = now.getFullYear();
		var cMonth = now.getMonth() + 1;
		if(monthTxt.length == 2){
			  monthTxt = parseInt(monthTxt);
			}
		if(parseInt(yearTxt) > cYear ||( parseInt(monthTxt) < cMonth && parseInt(yearTxt) == cYear ) ||(parseInt(monthTxt) > cMonth && parseInt(yearTxt) < cYear) ){
			msg = "Không được xóa KH tham gia CTTB của tháng trước tháng hiện tại.";
			$('#errCustomerMsg').html(msg).show();
			return false;
		}
		if(deleteAll == "checked"){
			var rows = $('#customerGrid123').datagrid('getRows');
			if(rows != null && rows != undefined && rows.length >0){
				var length = rows.length;
				for(var i =0;i<length;i++){
					var temp = rows[i].id;
					if(temp != null && temp != undefined){
						ProgrammeDisplayCatalog._lstCustomerId.put(temp,temp);
					}
				}
			}else{
				$('#errCustomerMsg').html('Không có dữ liệu để xóa.').show();
				return false;
			}
			var params = new Object();
			var shopKendo = $("#shop").data("kendoMultiSelect");
	        var lstShopId = new Array();
	        lstShopId = shopKendo.value();
			var month = $('#month1').val();
			if(lstShopId != null && lstShopId != undefined){
				params.listShopIdString = lstShopId.toString();
			}

			params.month = month;
			params.id = $('#displayProgram').val();		
			params.isCheckAll = $('#checkDeleteAll').is(':checked');
			
			params.lstCustomerId = ProgrammeDisplayCatalog._lstCustomerId.keyArray;
			Utils.addOrSaveData(params, '/programme-display/customer/deleteCustomer', null, 'errCustomerMsg', function(data){						
				$('#errCustomerMsg').html('').hide();
				$('#successCustomerMsg').html("Xóa dữ liệu thành công").show();
				var tm = setTimeout(function(){
					$('#successCustomerMsg').html('').hide();
            		clearTimeout(tm);	                		
            	}, 3000);
				ProgrammeDisplayCatalog._lstCustomerId = new Map();
				ProgrammeDisplayCatalog.searchCustomerTab();
			}, null, null, null, "Bạn có muốn xóa khách hàng của CTTB này ?", null);
		}else{
			if(ProgrammeDisplayCatalog._lstProduct == null || ProgrammeDisplayCatalog._lstProduct == undefined || ProgrammeDisplayCatalog._lstProduct.size() <= 0)
			{
				$('#errCustomerMsg').html('Bạn chưa chọn khách hàng để xóa.').show();
				return false;
			}
			var params = new Object();
			params.lstCustomerId = ProgrammeDisplayCatalog._lstProduct.keyArray; 
			Utils.addOrSaveData(params, '/programme-display/customer/deleteCustomer', null, 'errCustomerMsg', function(data){						
				$('#errCustomerMsg').html('').hide();
				$('#successCustomerMsg').html("Xóa dữ liệu thành công").show();
				var tm = setTimeout(function(){
					$('#successCustomerMsg').html('').hide();
            		clearTimeout(tm);	                		
            	}, 3000);
				ProgrammeDisplayCatalog._lstProduct = new Map();
				ProgrammeDisplayCatalog.searchCustomerTab();
			}, null, null, null, "Bạn có muốn xóa khách hàng của CTTB này ?", null);
		}
	},
	getGridUrlVNM: function(code,name,status,fDate,tDate){		
		return "/programme-display/searchvnm?code=" + encodeChar(code) + "&name=" + encodeChar(name) + "&status=" + status + "&fromDate=" + encodeChar(fDate) + "&toDate=" + encodeChar(tDate);
	},	
	searchVNM: function(){
		$('#errMsg').html('').hide();
		$('#errExcelMsg').html('').hide();
		var code = $('#code').val().trim();
		var name = $('#name').val().trim();
		var status = $('#status').val().trim();;
		if($('#permissionUser').val() == 'true' ){
			status = $('#status').val().trim();
		}
		
		var msg=''; 
		var startDate = $('#fDate').val().trim();
		var endDate = $('#tDate').val().trim();
		if (startDate == "__/__/____") {
			startDate = "";
		}
		if (endDate == "__/__/____") {
			endDate = "";
		}
		if (startDate.length > 0 && !Utils.isDate(startDate)) {
			msg = 'Từ ngày không hợp lệ, vui lòng nhập theo định dạng dd/MM/yyyy';
			$('#fDate').focus();
		}
		if(msg.length == 0){
			if(endDate.length > 0 && !Utils.isDate(endDate)){
				msg = 'Đến ngày không hợp lệ, vui lòng nhập theo định dạng dd/MM/yyyy';
				$('#tDate').focus();
			}
		}
		if(msg.length == 0){
			if(startDate.length > 0 && endDate.length > 0){
				if(!Utils.compareDate(startDate, endDate)){
					msg = 'Giá trị Từ ngày phải nhỏ hơn hoặc bằng giá trị Đến ngày.';				
				}
			}
		}
		if(msg.length > 0){
			$('#errMsg').html(msg).show();
			return false;
		}
		var params = new Object();
		params.code = $('#code').val().trim();
		params.name = $('#name').val().trim();
		params.fromDate = startDate;
		params.toDate = endDate;
		params.status = $('#status').val().trim();
		//var url = ProgrammeDisplayCatalog.getGridUrlVNM(code,name,status,fDate,tDate);
		$("#listCTTBGridVNM").datagrid('load',params);
		var tm = setTimeout(function() {
			$('#code').focus();
		}, 500);
		return false;
	},
	recursionFindChildShopCheck:function(id){
		if(id == null || id == undefined){
			return;
		}
    	$.ajax({
    		type : "POST",
			url : '/commons/shopMap/getChildShopId',
			data: ({shopId:id}),
			dataType: "json",
			success : function(data) {
				if(data != null && data != undefined && data.listShopId != null && data.listShopId != undefined){
					var length = data.length;
					var arrayShopId = data.listShopId;
					for(var i=0;i<length;i++){
						ProgrammeDisplayCatalog._listShopId.push(data.listShopId[i]);
					}
					ProgrammeDisplayCatalog.loopCheck(arrayShopId);
				} 
			}
    	});
	},
	recursionFindChildShopUnCheck:function(id){
		if(id == null || id == undefined){
			return;
		}
		$.ajax({
    		type : "POST",
			url : '/commons/shopMap/getChildShopId',
			data: ({shopId:id}),
			dataType: "json",
			success : function(data) {
				if(data != null && data != undefined && data.listShopId != null && data.listShopId != undefined){
					var flag = true;
					var length = data.length;
					var arrayShopId = data.listShopId;
					for(var i=0;i<length;i++){
						ProgrammeDisplayCatalog.removeArrayByValue(ProgrammeDisplayCatalog._listShopId,data.listShopId[i]);
					}
					ProgrammeDisplayCatalog.loopUnCheck(arrayShopId); 
				}
			}
    	});
	},
	recursionFindParentShopUnCheck:function(id){
		if(id == null || id == undefined){
			return;
		}
		$.ajax({
    		type : "POST",
			url : '/commons/shopMap/getParentShop',
			data: ({shopId:id}),
			dataType: "json",
			success : function(data) {
				if(data != null && data != undefined && data.parentShopId != null && data.parentShopId != undefined){
					var flag = true;
					$('.class_'+data.parentShopId).each(function(){
						if($(this).is(':checked') || $(this).is(':disabled')){
							flag = false;
						}
					});
					$('#check_'+data.parentShopId).removeAttr('checked');
					$('.easyui-dialog #check_'+data.parentShopId).removeAttr('checked');
					ProgrammeDisplayCatalog.recursionFindParentShopUnCheck(data.parentShopId);
				}
			}
    	});
	},
	loopCheck:function(arrayShopId){
		if(arrayShopId == null || arrayShopId == undefined || arrayShopId.length == 0){
			return;
		}
		$('.easyui-dialog #check_'+arrayShopId[0]).attr('checked','checked');
		ProgrammeDisplayCatalog.removeArrayByValue(arrayShopId,arrayShopId[0]);
		ProgrammeDisplayCatalog.loopCheck(arrayShopId);
	},
	loopUnCheck:function(arrayShopId){
		if(arrayShopId == null || arrayShopId == undefined || arrayShopId.length == 0){
			return;
		}
		$('.easyui-dialog #check_'+arrayShopId[0]).removeAttr('checked');
		ProgrammeDisplayCatalog.removeArrayByValue(arrayShopId,arrayShopId[0]);
		ProgrammeDisplayCatalog.loopUnCheck(arrayShopId);
	},
	loopCheckDelete:function(arrayShopId){
		if(arrayShopId == null || arrayShopId == undefined || arrayShopId.length == 0){
			return;
		}
		$('#check_'+arrayShopId[0]).attr('checked','checked');
		ProgrammeDisplayCatalog.removeArrayByValue(arrayShopId,arrayShopId[0]);
		ProgrammeDisplayCatalog.loopCheckDelete(arrayShopId);
	},
	loopUnCheckDelete:function(arrayShopId){
		if(arrayShopId == null || arrayShopId == undefined || arrayShopId.length == 0){
			return;
		}
		$('#check_'+arrayShopId[0]).removeAttr('checked');
		ProgrammeDisplayCatalog.removeArrayByValue(arrayShopId,arrayShopId[0]);
		ProgrammeDisplayCatalog.loopUnCheckDelete(arrayShopId);
	},
	recursionFindChildShopDeleteCheck:function(id){
		if(id == null || id == undefined){
			return;
		}
    	$.ajax({
    		type : "POST",
			url : '/commons/shopMap/getChildShopId',
			data: ({shopId:id}),
			dataType: "json",
			success : function(data) {
				if(data != null && data != undefined && data.listShopId != null && data.listShopId != undefined){
					var length = data.length;
					var arrayShopId = data.listShopId;
					if(data.listShopId != null && data.listShopId != undefined && data.listShopId.length==0){
						ProgrammeDisplayCatalog._listShopIdEx.push(id);
					}
					for(var i=0;i<length;i++){
						ProgrammeDisplayCatalog._listShopIdEx.push(data.listShopId[i]);
					}
					ProgrammeDisplayCatalog.loopCheckDelete(arrayShopId);
				} 
			}
    	});
	},
	recursionFindChildShopDeleteUnCheck:function(id){
		if(id == null || id == undefined){
			return;
		}
		$.ajax({
    		type : "POST",
			url : '/commons/shopMap/getChildShopId',
			data: ({shopId:id}),
			dataType: "json",
			success : function(data) {
				if(data != null && data != undefined && data.listShopId != null && data.listShopId != undefined){
					var flag = true;
					var length = data.length;
					var arrayShopId = data.listShopId;
					if(data.listShopId != null && data.listShopId != undefined && data.listShopId.length==0){
						ProgrammeDisplayCatalog.removeArrayByValue(ProgrammeDisplayCatalog._listShopIdEx,id);
					}
					for(var i=0;i<length;i++){
						ProgrammeDisplayCatalog.removeArrayByValue(ProgrammeDisplayCatalog._listShopIdEx,data.listShopId[i]);
					}
					ProgrammeDisplayCatalog.loopUnCheckDelete(arrayShopId); 
				}
			}
    	});
	},
	openSearchStyleExclusionProgramEasyUIDialog : function(codeText, nameText, title, url, codeFieldText, nameFieldText, arrParam) {
		$('#searchStyleExclusionDialogDiv').css("visibility", "visible");
		var html = $('#searchStyleExclusionDialogDiv').html();
		$('#searchStyleExclusionDialog').dialog({  
	        title: title,  
	        closed: false,  
	        cache: false,  
	        modal: true,
	        width : 600,
	        height : 'auto',
	        onOpen: function(){	        	
				var tabindex = -1;
				$('.InputTextStyle, select, input, button , li , tr, td, th, label, ul, a, img').each(function () {
					if (this.type != 'hidden') {
						$(this).attr("tabindex", tabindex);
						tabindex -=1;
					}
				});								
				$('.easyui-dialog #seachStyleExclusionCode').focus();
				$('.easyui-dialog #searchStyleExclusionUrl').val(url);
				$('.easyui-dialog #errMsgSearchExclusion').html("").hide();
				
				$('.easyui-dialog #searchStyle1CodeText').val(codeFieldText);
				$('.easyui-dialog #searchStyle1NameText').val(nameFieldText);			
				var codeField = 'code';
				var nameField = 'name';
				if (codeFieldText != null && codeFieldText != undefined && codeFieldText.length > 0) {
					codeField = codeFieldText;
				}
				if (nameFieldText != null&& nameFieldText != undefined && nameFieldText.length > 0) {
					nameField = nameFieldText;
				}

				$('.easyui-dialog #seachStyle1CodeLabel').html(codeText);
				$('.easyui-dialog #seachStyle1NameLabel').html(nameText);
				$('#searchStyleExclusionGrid').show();
				$('#searchStyleExclusionGrid').datagrid({
					url : ProgrammeDisplayCatalog.getSearchStyleExclusionMapGridUrl(url, '', '', arrParam),
					autoRowHeight : true,
					rownumbers : true, 
					checkOnSelect :true,
					pagination:true,
					rowNum : 10,
					pageNumber: 1,
					scrollbarSize : 0,
					fitColumns:true,
					pageList  : [10,20,30],
					width : ($('#searchStyleExclusionContainerGrid').width()),
				    columns:[[  
				        {field:'displayProgramCode',title:codeText,align:'left', width:170, sortable : false,resizable : false,formatter:function(value,row,index){
				        	return Utils.XSSEncode(row.displayProgramCode);
				        }},  
				        {field:'displayProgramName',title:nameText,align:'left', width:370, sortable : false,resizable : false,formatter:function(value,row,index){
				        	return Utils.XSSEncode(row.displayProgramName);
				        }},  
				        {field: 'id', checkbox:true, align:'center', width:80,sortable : false,resizable : false, formatter: function(value, row, index){
				        	return Utils.XSSEncode(row.id);
				        }}
				    ]],
				    onLoadSuccess :function(data){
				    	$('.datagrid-header-rownumber').html('STT');
				    	tabindex = 1;
				    	$('.easyui-dialog input,.easyui-dialog select,.easyui-dialog button').each(function () {
				    		if (this.type != 'hidden') {
				    			$(this).attr("tabindex", '');
				    			tabindex++;
				    		}
				    	});
				    	if(data.rows.length == 0){
				    		$('.datagrid-header-check input').removeAttr('checked');
				    	}
				    	var originalSize = $('.easyui-dialog .datagrid-header-row td[field=displayProgramCode] div').width();
				    	if(originalSize != null && originalSize != undefined){
				    		ProgrammeDisplayCatalog._listExclusionDialogSize.push(originalSize);
				    	}
				    	$('input[name="id"]').each(function(){
				    		var temp = ProgrammeDisplayCatalog._listExclusion.get($(this).val());
				    		if(temp!=null) $(this).attr('checked','checked');
			 			});
			 	    	var length = 0;
			 	    	$('input[name="id"]').each(function(){
			 	    		if($(this).is(':checked')){
			 	    			++length;
			 	    		}	    		
			 	    	});	    	
			 	    	if(data.rows.length==length){
			 	    		$('.datagrid-header-check input').attr('checked','checked');
			 	    	}else{
							$('.datagrid-header-check input').removeAttr('checked');
			 	    	}
			 	    	if(data.rows == null || data.rows == undefined || data.rows.length == 0){
			 	    		$('.datagrid-header-check input').removeAttr('checked');
			 	    	}
				    },
				    onCheck:function(i,r){
				    	ProgrammeDisplayCatalog._listExclusion.put(r.id,r);
				    },
				    onUncheck:function(i,r){
				    	ProgrammeDisplayCatalog._listExclusion.remove(r.id);
				    },
				    onCheckAll:function(r){
				    	for(i=0;i<r.length;i++){
				    		ProgrammeDisplayCatalog._listExclusion.put(r[i].id,r[i]);
				    	}
				    },
				    onUncheckAll:function(r){
				    	for(i=0;i<r.length;i++){
				    		ProgrammeDisplayCatalog._listExclusion.remove(r[i].id);
				    	}
				    }
				}); 
				$('.datagrid-header-rownumber').html('STT');
				$('.easyui-dialog #btnSearchStyleExclusion').bind('click',function(event) {
					$('.easyui-dialog #errMsgSearchExclusion').html('').hide();
					var code = encodeChar($('.easyui-dialog #seachStyleExclusionCode').val().trim());
					var name = encodeChar($('.easyui-dialog #seachStyleExclusionName').val().trim());
					var url = ProgrammeDisplayCatalog.getSearchStyleExclusionMapGridUrl($('.easyui-dialog #searchStyleExclusionUrl').val(), code, name, CommonSearch._arrParams);
					$('.easyui-dialog #searchStyleExclusionGrid').datagrid({url:url,pageNumber:1});						
					$('.easyui-dialog #seachStyleExclusionCode').focus();
				});
				$('.easyui-dialog #btnSaveExclusion').bind('click',function(event) {
					var lstIdCustomer = new Array();
					for(i=0;i<ProgrammeDisplayCatalog._listExclusion.keyArray.length;i++){
						var temp = ProgrammeDisplayCatalog._listExclusion.get(ProgrammeDisplayCatalog._listExclusion.keyArray[i]);
						if(temp!=null){
							lstIdCustomer.push(temp.id);
						}
					}

					var params = new Object();
					params.listExclusionId =lstIdCustomer;
					params.id = $('#selId').val();
					if(lstIdCustomer == undefined || lstIdCustomer == null || lstIdCustomer.length <=0){
						$('.easyui-dialog #errMsgSearchExclusion').html("Bạn chưa chọn CTTB để loại trừ.").show();
						return false;
					}
					Utils.addOrSaveData(params, '/programme-display/createExclusionProgram', null, 'errMsgExclusion', function(data){
						if(data != null && data.error == undefined && data.errMsg == undefined){
							ProgrammeDisplayCatalog.searchExclusionProgram();
							ProgrammeDisplayCatalog._listExclusion = new Map();
							$('#errMsgSearchExclusion').html("").hide();
							$('#searchStyleExclusionDialog').dialog('close');
							$('#errMsgExclusion').html("").hide();
							$('#successMsgExclusion').html('Lưu dữ liệu thành công.').show();
							var tm = setTimeout(function(){
								$('#successMsgExclusion').html('').hide();
								clearTimeout(tm); 
							}, 3000);
						}
					}, null, null, null, "Bạn có muốn thêm CTTB không ?", function(data){
						$('#errMsgSearchExclusion').html(data.errMsg).show();
						var tm = setTimeout(function(){
							$('#successMsgExclusion').html('').hide();
							clearTimeout(tm); 
						}, 3000);
					});
				});
				$('.easyui-dialog #seachStyleExclusionCode').bind('keyup',function(event){
					if(event.keyCode == 13){
						$('.easyui-dialog #errMsgSearchExclusion').html('').hide();
						var code = $('.easyui-dialog #seachStyleExclusionCode').val().trim();
						var name = $('.easyui-dialog #seachStyleExclusionName').val().trim();
						var url = ProgrammeDisplayCatalog.getSearchStyleExclusionMapGridUrl($('.easyui-dialog #searchStyleExclusionUrl').val(), code, name, CommonSearch._arrParams);
						$('.easyui-dialog #searchStyleExclusionGrid').datagrid({url:url,pageNumber:1});
						$('.easyui-dialog #seachStyleExclusionCode').focus();
					}
				});
				$('.easyui-dialog #seachStyleExclusionName').bind('keyup',function(event){
					if(event.keyCode == 13){
						$('.easyui-dialog #errMsgSearchExclusion').html('').hide();
						var code = $('.easyui-dialog #seachStyleExclusionCode').val().trim();
						var name = $('.easyui-dialog #seachStyleExclusionName').val().trim();
						var url = ProgrammeDisplayCatalog.getSearchStyleExclusionMapGridUrl($('.easyui-dialog #searchStyleExclusionUrl').val(), code, name, CommonSearch._arrParams);
						$('.easyui-dialog #searchStyleExclusionGrid').datagrid({url:url,pageNumber:1});
						$('.easyui-dialog #seachStyleExclusionCode').focus();
					}
				});
	        },
	        onBeforeClose: function() {
				var tabindex = 1;
				$(' .InputTextStyle, select, input, button , li , tr, td, th, label, ul, a, img').each(function () {
					if (this.type != 'hidden') {
						$(this).attr("tabindex", '');
					}
					tabindex ++;
				});	
				var curIdFocus = $('#cur_focus').val();
				$('#'+ curIdFocus).focus();				
	        },
	        onClose : function(){
	        	$('#searchStyleExclusionDialogDiv').html(html);
	        	$('#searchStyleExclusionDialogDiv').hide();
	        	$('.easyui-dialog #seachStyleExclusionCode').val('');
	        	$('.easyui-dialog #seachStyleExclusionName').val('');
	        	$('.easyui-dialog #btnSaveExclusion').unbind('click');
	        	$('.easyui-dialog #errMsgSearchExclusion').html('').hide();
	        	ProgrammeDisplayCatalog._listExclusion = new Map(); 
	        }
	    });
		return false;
	},
	getSearchStyleExclusionMapGridUrl : function(url, code, name, arrParam) {		
		var searchUrl = '';
		searchUrl = url;
		var curDate = new Date();
		var curTime = curDate.getTime();
		searchUrl+= '?curTime=' + encodeChar(curTime);
		if (arrParam != null && arrParam != undefined) {
			for ( var i = 0; i < arrParam.length; i++) {
				searchUrl += "&" + arrParam[i].name + "=" + arrParam[i].value;
			}
		}
		searchUrl+= "&code=" + encodeChar(code) + "&name=" + encodeChar(name); 
		return searchUrl;
	},
};
/*
 * END OF FILE - /web/web/resources/scripts/business/program/vnm.programme-display-catalog.js
 */

/*
 * START OF FILE - /web/web/resources/scripts/business/program/vnm.promotion-program.js
 */
/**
 * PromotionProgram
 * xu ly cho chuong trinh khuyen mai
 * 
 * searchProgram: tim kiem CTKM (man hinh danh sach)
 * showTab(tabId, url): mo cac tab tuong ung tren chi tiet CTKM voi id tab, duong dan request lay html
 * searchShop: tim kiem don vi tham gia CTKM tab don vi
 * searchCustomerShopMap: tim kiem KH tham gia CTKM khi click vao chi tiet don vi
 * toggleCustomerSearch: dong/mo thong tin tim kiem KH
 * searchSaler: tim kiem NVBH trong tab so suat NVBH
 * showCustomerDlg: mo popup them KH vao CTKM
 * addCustomerQtt: them KH vao CTKM
 * updateCustomerQuantity: cap nhat so suat KH tren danh sach KH
 * deleteCustomerMap: xoa KH khoi CTKM
 * updateShopQuantity: cap nhat so suat don vi (NPP) tren danh sach don vi
 * deleteShopMap: xoa don vi khoi CTKM
 * showShopDlg: mo popup them don vi vao CTKM
 * addShopQtt: them don vi vao CTKM
 * exportPromotionShop: xuat don vi tham gia CTKM ra excel
 * importPromotionShop: nhap don vi tham gia CTKM tu excel
 * checkSaleLevelCatIdInList: khong biet
 * loadAppliedAttributes: lay danh sach thuoc tinh KH da co trong CTKM
 * selectAttributes: chon thuoc tinh ben trai va dua sang ben phai (dua vao CTKM)
 * removeAttributes: chon thuoc tinh ben phai (trong CTKM) va dua sang ben trai (bo ra khoi CTKM)
 * saveCustomerAttributes: luu thuoc tinh KH tham gia CTKM
 * getTrue_DataOrFalse_MessageOfAutoAttribute: khong biet
 * updateSalerQuantity: cap nhat so suat NVBH
 * showSalerDlg: mo popup them NVBH vao CTKM
 * addSalerQtt: them NVBH vao CTKM
 * exportPromotionStaff: xuat ds so suat NVBH ra excel
 * importPromotionStaff: nhap so suat NVBH CTKM tu excel
 */
var PromotionProgram = {
	shopId: null,
	_xhrSave: null,
	_promotionShopParams: null,
	_promotionStaffParams: null,
	_levelValueType:null,//1 hoac 2
	_listProduct:null,
	_mapGroupMua:null,
	_mapGroupKM:null,
	_mapLevelMua:null,
	_mapLevelKM:null,
	_listMapping:null,
	_hasCustomerType:null,
	_hasSaleLevel:null,
	_hasCustomerCardType:null,
	_flagAjaxTabAttribute:null,
	_listSaleProduct:null,
	_DEFAULT_FROM_LEVEL: 1,
	_DEFAULT_TO_LEVEL: 5,
	fromLevel : 0,
	toLevel : 5,
	isFinishLoad : true,
	isEndLoad : false,
	isButtonLoad : false,
	_mapTabProcess: false,
	_paramCusSearch: null,
	gridRowIconFormatter: function(value,row,index) {
		if(row.type != null && (row.type.substring(0,2) == 'ZM' || row.type.substring(0,2) == 'ZT' || row.type.substring(0,2) == 'ZD'|| row.type.substring(0,2) == 'ZH')){
			return "<a href='/promotion/detail?promotionId="+row.id+"&proType=2'><span style='cursor:pointer'><img src='/resources/images/icon-edit.png'/></span></a>";
		}else{
			return "<a href='/promotion/detail?promotionId="+row.id+"&proType=1'><span style='cursor:pointer'><img src='/resources/images/icon-edit.png'/></span></a>";
		}
	},
	
	update : function() {
		$('.ErrorMsgStyle').html('');
		var msg = '';
		if (msg.length == 0) {
			msg = Utils.getMessageOfRequireCheck('promotionCode', promotionMultiLanguage.code, Utils._CODE);
		}
		if (msg.length == 0) {
			msg = Utils.getMessageOfSpecialCharactersValidate('promotionCode', promotionMultiLanguage.code, Utils._CODE);
		}
		if (msg.length == 0) {
			msg = Utils.getMessageOfRequireCheck('promotionName', promotionMultiLanguage.name, Utils._ADDRESS);
		}
		if (msg.length == 0) {
			msg = Utils.getMessageOfSpecialCharactersValidate('promotionName', promotionMultiLanguage.name, Utils._ADDRESS);
		}
		if (msg.length == 0) {
			msg = Utils.getMessageOfRequireCheck('description', promotionMultiLanguage.description, Utils._ADDRESS);
		}
		if (msg.length == 0) {
			msg = Utils.getMessageOfSpecialCharactersValidate('description', promotionMultiLanguage.description, Utils._ADDRESS);
		}
		if (msg.length > 0) {
			$('#errorMsg').html(msg.trim()).show();
			return;
		}
		var params = null;
		if($('#status').val() == 0) {
			params = new Object();
			params.promotionId = $('#id').val();
			params.promotionName = $('#promotionName').val();
			params.startDate = $('#startDate').val();
			params.endDate = $('#endDate').val();
			params.status = $('#status').val();
			params.canEdit = $('#canEdit').is(':checked');
			params.description = $('#description').val();
		} else {
			params = VTUtilJS.getFormData('update-form');
		}
		if(params == null) {
			return;
		}
		if(params.typeCode == -1) {
			VTUtilJS.showMessageBanner(true, 'Bạn chưa chọn loại chương trình');
			return;
		}
		$('.SuccessMsgStyle').hide();
		if(params != null) {
			var promotionId = $('#id').val();
			if(!VTUtilJS.isNullOrEmpty(promotionId)) {
				params.promotionId = promotionId;
			}
			$.messager.confirm("Xác nhận", "Bạn có muốn cập nhập thông tin CTKM?", function(r) {
				if(r) {
					VTUtilJS.postFormJson(params, '/promotion/update', function(data) {
						if(data.error) {
							VTUtilJS.showMessageBanner(true, data.errMsg);
						} else {
							window.location.href = '/promotion/detail?promotionId='+data.promotionId;
						}
					});
				}
			});
		}
	},

	/**
	 * tai file mau import CTKM
	 * @author  tuannd20
	 * @return void
	 * @date 27/01/2015
	 */
	downloadImportTemplate: function() {
		var selectedProgramTypes = $('#typeCode').val();
		var fileURL = excel_template_path + 'catalog/Bieu_mau_danh_muc_import_CTKM.xlsx';
		if (selectedProgramTypes.length == 1) {
			try {
//				var selectedProgramType = $('select#typeCode option[value=' + selectedProgramTypes[0] + ']').html().split('-')[0];
//				if (selectedProgramType < 'ZV22') {
//				}
				fileURL = excel_template_path + 'catalog/Bieu_mau_danh_muc_import_CTKM_' + selectedProgramType + '.xlsx' ;
			} catch(e) {
				// pass through
			}
		}
		window.location.href = fileURL;
	},
	
	importExcel : function() {
		$(".ErrorMsgStyle").hide();
		$(".SuccessMsgStyle").hide();
		VTUtilJS.importExcelUtils(function(data) {
			if (!VTUtilJS.isNullOrEmpty(data.message)) {
				if (data.fileNameFail) {
					var msg = 'Có lỗi trong file import, không thực hiện nhập dữ liệu. <a href="'+ data.fileNameFail +'">Xem chi tiết lỗi</a>';
					VTUtilJS.showMessageBanner(true, msg);
				} else {
					VTUtilJS.showMessageBanner(true, data.message);
				}
			} else {
				VTUtilJS.showMessageBanner(false, 'Import thành công');
			}
		}, 'importFrm', 'excelFile');
	},
	
	exportExcel:function(){
		$('.ErrorMsgStyle').hide();
		$('#fakefilepc').val('');
		$('#excelFile').val('');
		
		var promotionCode = $('#promotionCode').val().trim();
		var lstApParamId = null;
		if($('#typeCode').val() == null || $('#typeCode').val().length == 0){
			lstApParamId = [];
			lstApParamId.push(-1);
		}else{
			lstApParamId = $('#typeCode').val();
			if (lstApParamId[0] == -1) {
				lstApParamId.splice(1);
			}
		}
		var shopCode = $('#shopCode').val().trim();
		var promotionName = $('#promotionName').val().trim();
		var status = 1;
		//if($('#permissionUser').val() == 'true' ){
			status = $('#status').val().trim();
		//}
		var proType = 1;//$("#proType").val();
		var msg = '';
		var fDate = $('#fromDate').val().trim();
		var tDate = $('#toDate').val().trim();
		if (fDate.length > 0 && !Utils.isDate(fDate)) {
			msg = 'Từ ngày không hợp lệ, vui lòng nhập theo định dạng dd/MM/yyyy';
			$('#fromDate').focus();
		}
		if (msg.length == 0 && tDate.length > 0 && !Utils.isDate(tDate)) {
			msg = 'Đến ngày không hợp lệ, vui lòng nhập theo định dạng dd/MM/yyyy';
			$('#toDate').focus();
		}
		if(msg.length == 0 && fDate.length > 0 && tDate.length > 0 && !Utils.compareDate(fDate, tDate)){
			msg = 'Từ ngày phải là ngày trước hoặc cùng ngày với Đến ngày';
			$('#fromDate').focus();
		}
		if(msg.length > 0){
			$('#errMsg').html(msg).show();
			return false;
		}
		var params = {
				code: promotionCode,
				name: promotionName,
				proType: proType,
				shopCode: shopCode,
				lstTypeId: lstApParamId.join(","),
				fromDate: fDate,
				toDate: tDate,
				status: status
		};
//		try {
//			params.reportCode = $('#function_code').val();
//		} catch(e) {
//			// Oop! Error
//		}
		ReportUtils.exportReport("/promotion/export", params, "errorMsg");
//		Utils.getJSONDataByAjax(params, '/promotion/export',function(data) {
//			if (!data.error) {
//				var filePath = data.path || data.view;
//				if (filePath) {
//					window.location.href = ReportUtils.buildReportFilePath(filePath);
//				}
//			} else {
//				$('#errMsg').html(data.errMsg).show();
//			}
//		});
	},

	searchProgram: function() {
		$('.ErrorMsgStyle').hide();
		$('#fakefilepc').val('');
		$('#excelFile').val('');
		
		var promotionCode = $('#promotionCode').val().trim();
		var lstApParamId = null;
		if($('#typeCode').val() == null || $('#typeCode').val().length == 0){
			lstApParamId = [];
			lstApParamId.push(-1);
		}else{
			lstApParamId = $('#typeCode').val();
			if (lstApParamId[0] == -1) {
				lstApParamId.splice(1);
			}
		}
		var shopCode = $('#shopCode').val().trim();
		var promotionName = $('#promotionName').val().trim();
		var status = 1;
		//if($('#permissionUser').val() == 'true' ){
			status = $('#status').val().trim();
		//}
		var proType = 1;//$("#proType").val();
		var msg = '';
		var fDate = $('#fromDate').val().trim();
		var tDate = $('#toDate').val().trim();
		if (fDate.length > 0 && !Utils.isDate(fDate)) {
			msg = 'Từ ngày không hợp lệ, vui lòng nhập theo định dạng dd/MM/yyyy';
			$('#fromDate').focus();
		}
		if (msg.length == 0 && tDate.length > 0 && !Utils.isDate(tDate)) {
			msg = 'Đến ngày không hợp lệ, vui lòng nhập theo định dạng dd/MM/yyyy';
			$('#toDate').focus();
		}
		if(msg.length == 0 && fDate.length > 0 && tDate.length > 0 && !Utils.compareDate(fDate, tDate)){
			msg = 'Từ ngày phải là ngày trước hoặc cùng ngày với Đến ngày';
			$('#fromDate').focus();
		}
		if(msg.length > 0){
			$('#errorMsgSearch').html(msg).show();
			return false;
		}
		var params = {
				code: promotionCode,
				name: promotionName,
				proType: proType,
				shopCode: shopCode,
				lstTypeId: lstApParamId.join(","),
				fromDate: fDate,
				toDate: tDate,
				status: status
		};
		$('#grid').datagrid("reload", params);

		return false;
	},
	
	showTab: function(tabId, url) {
		if ($("#" + tabId + ".Active").length > 0) {
			return;
		}
		$(".ErrorMsgStyle").hide();
		$("#tabSectionDiv a.Active").removeClass("Active");
		$("#" + tabId).addClass("Active");
		url = url + "?promotionId="+$("#masterDataArea #id").val().trim();
		VTUtilJS.getFormHtml('masterDataArea', url, function(html) {
			$('#divTab').html(html);
			Utils.functionAccessFillControl();
		});
	},
	
	returnGroupTextMua: function(promotionType, listDetail, globalQuantity, globalAmount) {
		if(promotionType == 'ZV01' || promotionType == 'ZV02' || promotionType == 'ZV03') {
			/**
			 * productCode + globalQuantity
			 */
			if(VTUtilJS.isNullOrEmpty(listDetail) || VTUtilJS.isNullOrEmpty(listDetail.length) || listDetail.length == 0 || VTUtilJS.isNullOrEmpty(globalQuantity) || globalQuantity == 0) {
				return '';
			}
			if(!$.isArray(listDetail)) {
				return '';
			} else {
				var text = '(';
				for(var i = 0; i < listDetail.length; i++) {
					var levelDetail = listDetail[i];
					if(text == '(') {
						text += levelDetail.productCode +((levelDetail.isRequired == 1)?'*':'') + '(' + globalQuantity + ')';
					} else {
						text += ', ' + levelDetail.productCode +((levelDetail.isRequired == 1)?'*':'') + '(' + globalQuantity + ')';
					}
				}
				text += ')';
				return text;
			}
		} else if(promotionType == 'ZV04' || promotionType == 'ZV05' || promotionType == 'ZV06') {
			/**
			 * productCode + globalAmount
			 */
			if(VTUtilJS.isNullOrEmpty(listDetail) || VTUtilJS.isNullOrEmpty(listDetail.length) || listDetail.length == 0 || VTUtilJS.isNullOrEmpty(globalAmount) || globalAmount == 0) {
				return '';
			}
			if(!$.isArray(listDetail)) {
				return '';
			} else {
				var text = '(';
				for(var i = 0; i < listDetail.length; i++) {
					var levelDetail = listDetail[i];
					if(text == '(') {
						text += levelDetail.productCode +((levelDetail.isRequired == 1)?'*':'') + '(' + VTUtilJS.formatCurrency(globalAmount) + ')';
					} else {
						text += ', ' + levelDetail.productCode +((levelDetail.isRequired == 1)?'*':'') + '(' + VTUtilJS.formatCurrency(globalAmount) + ')';
					}
				}
				text += ')';
				return text;
			}
		} else if(promotionType == 'ZV07' || promotionType == 'ZV08' || promotionType == 'ZV09' || promotionType == 'ZV10' || promotionType == 'ZV11' || promotionType == 'ZV12') {
			if(VTUtilJS.isNullOrEmpty(listDetail) || VTUtilJS.isNullOrEmpty(listDetail.length) || listDetail.length == 0) {
				return '';
			}
			if(!$.isArray(listDetail)) {
				return '';
			} else {
				var text = '(';
				for(var i = 0; i < listDetail.length; i++) {
					var levelDetail = listDetail[i];
					if(text == '(') {
						text += levelDetail.productCode +((levelDetail.isRequired == 1)?'*':'');
					} else {
						text += ', ' + levelDetail.productCode +((levelDetail.isRequired == 1)?'*':'');
					}
				}
				text += ')';
				return text;
			}
		} else if(promotionType == 'ZV13' || promotionType == 'ZV14' || promotionType == 'ZV15' || promotionType == 'ZV16' || promotionType == 'ZV17' || promotionType == 'ZV18') {
			if(VTUtilJS.isNullOrEmpty(listDetail) || VTUtilJS.isNullOrEmpty(listDetail.length) || listDetail.length == 0) {
				return '';
			}
			if(!$.isArray(listDetail)) {
				return '';
			} else {
				var text = '(';
				for(var i = 0; i < listDetail.length; i++) {
					var levelDetail = listDetail[i];
					if(text == '(') {
						if(VTUtilJS.isNullOrEmpty(levelDetail.value)) {
							return '';
						}
						text += levelDetail.productCode +(levelDetail.isRequired?'*':'') + '(' + levelDetail.value + ')';
					} else {
						if(VTUtilJS.isNullOrEmpty(levelDetail.value)) {
							return '';
						}
						text += ', ' + levelDetail.productCode +(levelDetail.isRequired?'*':'') + '(' + levelDetail.value + ')';
					}
				}
				text += ')';
				return text;
			}
		} else {
			
		}
	},
	
	returnGroupTextKM: function(promotionType, listDetail, globalQuantity, globalAmount) {
		var text = '(';
		for(var i = 0; i < listDetail.length; i++) {
			var levelDetail = listDetail[i];
			if(text == '(') {
				if(VTUtilJS.isNullOrEmpty(levelDetail.value)) {
					return '';
				}
				text += levelDetail.productCode +((levelDetail.isRequired == 1)?'*':'') + '(' + levelDetail.value + ')';
			} else {
				if(VTUtilJS.isNullOrEmpty(levelDetail.value)) {
					return '';
				}
				text += ', ' + levelDetail.productCode +((levelDetail.isRequired == 1)?'*':'') + '(' + levelDetail.value + ')';
			}
		}
		text += ')';
		return text;
	},
		
	editLevelMuaAddProduct: function() {
		if(($('#promotionType').val() == 'ZV01' || $('#promotionType').val() == 'ZV02' || $('#promotionType').val() == 'ZV03' ||
				$('#promotionType').val() == 'ZV04' || $('#promotionType').val() == 'ZV05' || $('#promotionType').val() == 'ZV06')
				&& $('#gridProduct').datagrid('getRows').length == 1) {
			return;
		} else if(($('#promotionType').val() == 'ZV07' || $('#promotionType').val() == 'ZV08' || $('#promotionType').val() == 'ZV09' ||
				$('#promotionType').val() == 'ZV10' || $('#promotionType').val() == 'ZV11' || $('#promotionType').val() == 'ZV12' ||
				$('#promotionType').val() == 'ZV13' || $('#promotionType').val() == 'ZV14' || $('#promotionType').val() == 'ZV15' ||
				$('#promotionType').val() == 'ZV16' || $('#promotionType').val() == 'ZV17' || $('#promotionType').val() == 'ZV18')
				&& $('#gridLevel').datagrid('getRows').length > 1 && $('#gridProduct').datagrid('getRows').length != 0) {
			return;
		}
		$('#gridProduct').datagrid('appendRow', {isRequired : (($('#promotionType').val() == 'ZV01' || $('#promotionType').val() == 'ZV02' || $('#promotionType').val() == 'ZV03' || $('#promotionType').val() == 'ZV04' || $('#promotionType').val() == 'ZV05' || $('#promotionType').val() == 'ZV06' || $('#promotionType').val() == 'ZV13' || $('#promotionType').val() == 'ZV14' || $('#promotionType').val() == 'ZV15' || $('#promotionType').val() == 'ZV16' || $('#promotionType').val() == 'ZV17' || $('#promotionType').val() == 'ZV18') ? 1 : 0)});
		editIndex = $('#gridProduct').datagrid('getRows').length-1;
		$('#gridProduct').datagrid('selectRow', editIndex).datagrid('beginEdit', editIndex);
	},
	
	editLevelKMAddProduct: function() {
		$('#gridProduct').datagrid('appendRow', {});
		editIndex = $('#gridProduct').datagrid('getRows').length-1;
		$('#gridProduct').datagrid('selectRow', editIndex).datagrid('beginEdit', editIndex);
	},
	
	editLevelMuaDeleteProduct: function(index) {
		$('#errAddProductMsg').html('').hide();
		if(($('#promotionType').val() == 'ZV01' || $('#promotionType').val() == 'ZV02' || $('#promotionType').val() == 'ZV03' ||
				$('#promotionType').val() == 'ZV04' || $('#promotionType').val() == 'ZV05' || $('#promotionType').val() == 'ZV06') && $('#gridProduct').datagrid('getRows').length == 1) {
			$('#errAddProductMsg').html('Mức không thể để trống. Bạn không thể xóa sản phẩm').show();
			return;
		} else if(($('#promotionType').val() == 'ZV07' || $('#promotionType').val() == 'ZV08' || $('#promotionType').val() == 'ZV09' ||
				$('#promotionType').val() == 'ZV10' || $('#promotionType').val() == 'ZV11' || $('#promotionType').val() == 'ZV12') && $('#gridLevel').datagrid('getRows').length != 1) {
			$('#errAddProductMsg').html('Nhóm đang có nhiều mức. Bạn không thể xóa sản phẩm').show();
			return;
		}
		
		if(index != $('#gridProduct').datagrid('getRows').length - 1) {
			$('#errAddProductMsg').html('Bạn chỉ được xóa dòng cuối cùng').show();
			return;
		}
		$.messager.confirm("Xác nhận", "Bạn có muốn xóa sản phẩm của mức?", function(r) {
			if(r) {
				var rowProduct = $('#gridProduct').datagrid('getRows')[index];
				if(!VTUtilJS.isNullOrEmpty(rowProduct.id)) {
					var params = new Object();
					params.levelDetailId = rowProduct.id;
					VTUtilJS.postFormJson(params, '/promotion/delete-level-detail', function(data) {
						if(data.error) {
							$('#errAddProductMsg').html(data.errMsg).show();
						} else {
							$('#gridProduct').datagrid('deleteRow', index);
						}
					});
				}
			}
		});
	},
	
	editLevelKMDeleteProduct: function(index) {
		$('#errAddProductMsg').html('').hide();
		if($('#gridLevel').datagrid('getRows').length != 1) {
			$('#errAddProductMsg').html('Nhóm đang có nhiều mức. Bạn không thể xóa sản phẩm').show();
			return;
		}
		
		if(index != $('#gridProduct').datagrid('getRows').length - 1) {
			$('#errAddProductMsg').html('Bạn chỉ được xóa dòng cuối cùng').show();
			return;
		}
		$.messager.confirm("Xác nhận", "Bạn có muốn xóa sản phẩm của mức", function(r) {
			if(r) {
				var rowProduct = $('#gridProduct').datagrid('getRows')[index];
				if(!VTUtilJS.isNullOrEmpty(rowProduct.id)) {
					var params = new Object();
					params.levelDetailId = rowProduct.id;
					VTUtilJS.postFormJson(params, '/promotion/delete-level-detail', function(data) {
						if(data.error) {
							$('#errAddProductMsg').html(data.errMsg).show();
						} else {
							$('#gridProduct').datagrid('deleteRow', index);
						}
					});
				}
			}
		});
	},
	
	editLevelMua: function(rowLeveIndex, groupLevelId) {
		var html = '<div id="popup-product-container">\
			<div id="popup-edit-level">\
			<div class="PopupContentMid">\
			<div class="GeneralForm Search1Form">';
		if($('#promotionType').val() == 'ZV13' || $('#promotionType').val() == 'ZV14' || $('#promotionType').val() == 'ZV15' || $('#promotionType').val() == 'ZV16' || $('#promotionType').val() == 'ZV17' || $('#promotionType').val() == 'ZV18') {
			html +=	'<div class="BoxSelect BoxSelect2">';
			PromotionProgram._levelValueType = ($('#promotionType').val() == 'ZV13' || $('#promotionType').val() == 'ZV14' || $('#promotionType').val() == 'ZV15') ? 1 : 2;
			if(VTUtilJS.isNullOrEmpty(PromotionProgram._levelValueType)) {
				html += '<select id="valueType" class="MySelectBoxClass"><option value="1" selected="selected">Số lượng</option>\
				<option value="2">Số tiền</option>\
				</select>\
				</div>';
			} else if(PromotionProgram._levelValueType == 1) {
				html += '<select id="valueType" disabled="disabled" class="MySelectBoxClass"><option value="1" selected="selected">Số lượng</option>\
					</select>\
					</div>';
			} else if(PromotionProgram._levelValueType == 2) {
				html += '<select id="valueType" disabled="disabled" class="MySelectBoxClass"><option value="2" selected="selected">Số tiền</option>\
					</select>\
					</div>';
			}
		}
		html+= '<div class="Clear"></div>\
			<div id="gridProductContainer">\
			<div id="gridProduct"></div>\
			</div>\
			<div class="Clear"></div>\
			</div>\
			<div class="BtnCenterSection">\
			<button id="btn-update" '+(($('#statusHidden').val() == 0 || $('#statusHidden').val() == 1) ? 'style="display:none;"' : '')+' class="BtnGeneralStyle BtnGeneralMStyle">Cập nhật</button>\
			</div>\
			<div class="Clear"></div>\
			<p class="ErrorMsgStyle SpriteErr" style="display: none;" id="errAddProductMsg"></p></div></div></div>\
			</div>';
		$("body").append(html);
		var row = $('#gridLevel').datagrid('getRows')[rowLeveIndex];
		var detail = [];
		if(row.detail != null && row.detail != undefined && $.isArray(row.detail) && row.detail.length != 0) {
			for(var i = 0; i < row.detail.length; i++) {
				detail.push(row.detail[i]);
			}
		}
		$('#popup-edit-level').dialog({
			title: 'Thông tin sản phẩm của nhóm',
			width: 750,
			heigth: "auto",
			modal: true, close: false, cache: false,
			onOpen: function() {
				var isDisable = false;
				switch ($('#promotionType').val()) {
				case 'ZV01':
					isDisable = true;
					break;
				case 'ZV02':
					isDisable = true;
					break;
				case 'ZV03':
					isDisable = true;
					break;
				case 'ZV04':
					isDisable = true;
					break;
				case 'ZV05':
					isDisable = true;
					break;
				case 'ZV06':
					isDisable = true;
					break;
				case 'ZV07':
					isDisable = true;
					break;
				case 'ZV08':
					isDisable = true;
					break;
				case 'ZV09':
					isDisable = true;
					break;
				case 'ZV10':
					isDisable = true;
					break;
				case 'ZV11':
					isDisable = true;
					break;
				case 'ZV12':
					isDisable = true;
					break;
				default:
					break;
				}
				$('#valueType').customStyle();
				$('#gridProduct').datagrid({
					data : detail,
					width: $("#gridProductContainer").width(),
    				height: 'auto',
    				singleSelect: true,
    				fitColumns: true,
    				scrollbarSize:0,
    				columns: [[
    	    					{field:'id', title:"Mức", width:30,align:'center',sortable:false,resizable:false,formatter:function(value,row,index) {
    	    						return index + 1;
    	    					}},
    	    					{field:'productCode', title:"Sản phẩm mua", width:100,align:'left',sortable:false,resizable:false,formatter:function(value, row, index) {
    	    						return VTUtilJS.XSSEncode(value);
    	    					}, 
    	    						editor: {
    	    							type:'combobox',
    	    							 options:{
	    								 	valueField : 'productCode',
	    								 	textField : 'productCode',
	    								 	//disabled : (rowLeveIndex == 0 && $('#gridLevel').datagrid('getRows').length == 1) ? false : true,
	    								 	disabled : true,
	    								 	data : PromotionProgram._listProduct.valArray,
	    								 	formatter:function(row){
	    			                            return row.productCode + ' - ' +row.productName;
	    			                        },
	    								 	onSelect: function(r) {
	    								 		var row = $(this).parent().parent().parent().parent().parent().parent().parent();
	    								 		row.find('td[field=productName] div').html(r.productName);
	    								 	}
    	    							 }
    	    						}
    	    					},
    	    					{field:'productName', title:"Tên sản phẩm", width:180,align:'left',sortable:false,resizable:false,formatter:CommonFormatter.formatNormalCell},
    	    					{field:'value', title:"Giá trị", width:80,align:'center',sortable:false,resizable:false, editor: {type:'currency', options : {isDisable : isDisable}}},
    	    					{field:'isRequired', title:"Bắt buộc", width:50,align:'center',sortable:false,resizable:false,editor:{type:'checkbox', options:{on:'1',off:'0', isDisable : (($('#gridLevel').datagrid('getRows').length > 1 && detail.length > 0) || $('#promotionType').val() == 'ZV01' || $('#promotionType').val() == 'ZV02' || $('#promotionType').val() == 'ZV03' || $('#promotionType').val() == 'ZV04' || $('#promotionType').val() == 'ZV05' || $('#promotionType').val() == 'ZV06' || $('#promotionType').val() == 'ZV13' || $('#promotionType').val() == 'ZV14' || $('#promotionType').val() == 'ZV15' || $('#promotionType').val() == 'ZV16' || $('#promotionType').val() == 'ZV17' || $('#promotionType').val() == 'ZV18')}}}/*,
    	    					//{field:'isRequired', title:"Bắt buộc", width:50,align:'center',sortable:false,resizable:false,editor:{type:'checkbox', options:{on:'1',off:'0', isDisable : true}}}/*,
    	    					{field:'delete', title:'<a '+(($('#statusHidden').val() == 0 || $('#statusHidden').val() == 1) ? 'style="display:none;"' : '')+' onclick="PromotionProgram.editLevelMuaAddProduct();"><span style="cursor:pointer"><img title="Thêm mới sản phẩm" src="/resources/images/icon_add.png"/></span></a>', width:30,align:'center',sortable:false,resizable:false,formatter:function(value,row,index){
    	    						return '<a '+(($('#statusHidden').val() == 0 || $('#statusHidden').val() == 1) ? 'style="display:none;"' : '')+' onclick="PromotionProgram.editLevelMuaDeleteProduct('+index+')"><span style="cursor:pointer"><img title="Xóa sản phẩm" src="/resources/images/icon_delete.png"/></span></a>';
    	    					}},*/
    	    				]],
    	    				onDblClickRow: function(index,row) {
    	    					if($('#statusHidden').val() == 2) {
    	    						$('#gridProduct').datagrid('beginEdit', index);
    	    					}
    	    				},
    	    				onLoadSuccess: function(data) {
    	    					if(detail == null || detail == undefined || ($.isArray(detail) && detail.length == 0)) {
    	    						if(rowLeveIndex == 0) {
    	    							$('#gridProduct').datagrid('appendRow', {isRequired : (($('#promotionType').val() == 'ZV01' || $('#promotionType').val() == 'ZV02' || $('#promotionType').val() == 'ZV03' || $('#promotionType').val() == 'ZV04' || $('#promotionType').val() == 'ZV05' || $('#promotionType').val() == 'ZV06' || $('#promotionType').val() == 'ZV13' || $('#promotionType').val() == 'ZV14' || $('#promotionType').val() == 'ZV15' || $('#promotionType').val() == 'ZV16' || $('#promotionType').val() == 'ZV17' || $('#promotionType').val() == 'ZV18') ? 1 : 0)});
    	    						} else if($('#promotionType').val() == 'ZV01' || $('#promotionType').val() == 'ZV02' || $('#promotionType').val() == 'ZV03' || $('#promotionType').val() == 'ZV04' || $('#promotionType').val() == 'ZV05' || $('#promotionType').val() == 'ZV06') {
    	    							var listDetail = $('#gridProduct').datagrid('getRows')[0].detail;
    	    							var productCode = listDetail[0].productCode;
    	    						}
    	    						editIndex = $('#gridProduct').datagrid('getRows').length-1;
    	    						$('#gridProduct').datagrid('selectRow', editIndex).datagrid('beginEdit', editIndex);
    	    					}
    	    					/*var rows = $('#gridProduct').datagrid('getRows');
    	    					for(var i = 0; i < rows.length; i++) {
    	    						$('#gridProduct').datagrid('beginEdit', i);
    	    					}*/
    	    				}
				});
				$('#popup-edit-level #btn-update').bind('click', function() {
					$('#errAddProductMsg').html('').hide;
					if($('#valueType').length == 0 && ($('#promotionType').val() == 'ZV01' || $('#promotionType').val() == 'ZV02' || $('#promotionType').val() == 'ZV03' ||
							$('#promotionType').val() == 'ZV07' || $('#promotionType').val() == 'ZV08' || $('#promotionType').val() == 'ZV09')) {
						PromotionProgram._levelValueType = 1;
					} else if($('#valueType').length == 0 && ($('#promotionType').val() == 'ZV04' || $('#promotionType').val() == 'ZV05' || $('#promotionType').val() == 'ZV06' || 
							$('#promotionType').val() == 'ZV10' || $('#promotionType').val() == 'ZV11' || $('#promotionType').val() == 'ZV12')) {
						PromotionProgram._levelValueType = 2;
					} else {
						PromotionProgram._levelValueType = $('#valueType').val();
					}
					
					$('#gridProduct').datagrid('acceptChanges');
					var rows = $('#gridProduct').datagrid('getRows');
					var listDetail = new Array();
					var listProductCode = new Array();
					for(var i = 0; i < rows.length; i++) {
						var row = rows[i];
						var detail = new Object();
						if(VTUtilJS.isNullOrEmpty(row.productCode)) {
							$('#errAddProductMsg').html('Sản phẩm ở dòng ' + (i+1) + ' chưa được nhập').show();
							/*for(var ii = 0; ii < rows.length; ii++) {
								$('#gridProduct').datagrid('beginEdit', ii);
							}*/
							return;
						} else if(listProductCode.indexOf(row.productCode) != -1) {
							$('#errAddProductMsg').html('Sản phẩm ở dòng ' + (i+1) + ' bị trùng mã sản phẩm').show();
							/*for(var ii = 0; ii < rows.length; ii++) {
								$('#gridProduct').datagrid('beginEdit', ii);
							}*/
							return;
						} else {
							if(PromotionProgram._listProduct.get(row.productCode) == null) {
								$('#errAddProductMsg').html('Sản phẩm ở dòng ' + (i+1) + ' không tồn tại trong hệ thống').show();
								/*for(var ii = 0; ii < rows.length; ii++) {
									$('#gridProduct').datagrid('beginEdit', ii);
								}*/
								return;
							}
							detail.productCode = row.productCode;
							detail.productName = PromotionProgram._listProduct.get(row.productCode).productName;
						}
						if($('#promotionType').val() == 'ZV13' || $('#promotionType').val() == 'ZV14' || $('#promotionType').val() == 'ZV15' ||
								$('#promotionType').val() == 'ZV16' || $('#promotionType').val() == 'ZV17' || $('#promotionType').val() == 'ZV18') {
							if(VTUtilJS.isNullOrEmpty(row.value) || row.value == 0) {
								$('#errAddProductMsg').html('Giá trị ở dòng ' + (i+1) + ' chưa được nhập').show();
								/*for(var ii = 0; ii < rows.length; ii++) {
									$('#gridProduct').datagrid('beginEdit', ii);
								}*/
								return;
							} else if(isNaN(row.value) || Number(row.value) <= 0) {
								$('#errAddProductMsg').html('Giá trị ở dòng ' + (i+1) + ' phải là số nguyên lớn hơn 0').show();
								/*for(var ii = 0; ii < rows.length; ii++) {
									$('#gridProduct').datagrid('beginEdit', ii);
								}*/
								return;
							} else {
								detail.value = row.value;
								detail.valueType = 1;
							}
						}
						if($('#promotionType').val() == 'ZV07' || $('#promotionType').val() == 'ZV08' || $('#promotionType').val() == 'ZV09' || $('#promotionType').val() == 'ZV10' || $('#promotionType').val() == 'ZV11' || $('#promotionType').val() == 'ZV12') {
							if(i != 0) {
								var preRow = rows[i-1]; 
								if(preRow.isRequired != row.isRequired) {
									if(preRow.isRequired || preRow.isRequired == 1) {
										$('#errAddProductMsg').html('Dòng ' + (i+1) + ' phải được check cột bắt buộc').show();
										/*for(var ii = 0; ii < rows.length; ii++) {
											$('#gridProduct').datagrid('beginEdit', ii);
										}*/
										return;
									} else {
										$('#errAddProductMsg').html('Dòng ' + (i+1) + ' không được check cột bắt buộc').show();
										/*for(var ii = 0; ii < rows.length; ii++) {
											$('#gridProduct').datagrid('beginEdit', ii);
										}*/
										return;
									}
								}
							}
						}
						detail.isRequired = row.isRequired;
						detail.groupLevelId = groupLevelId;
						listDetail.push(detail);
					}
					$('#gridLevel').datagrid('getRows')[rowLeveIndex].detail = listDetail;
					var orderEd = $('#gridLevel').datagrid('getEditor', {index : 0, field : 'orderNumber'});
					if(orderEd != null) {
						var orderNumber = $(orderEd.target).val();
						$('#gridLevel').datagrid('getRows')[rowLeveIndex].orderNumber = orderNumber;
					}
					$('#gridLevel').datagrid('getRows')[rowLeveIndex].groupText = PromotionProgram.returnGroupTextMua($('#promotionType').val(), listDetail, $('#gridLevel').datagrid('getRows')[rowLeveIndex].minQuantity, $('#gridLevel').datagrid('getRows')[rowLeveIndex].minAmount);
					$("#popup-edit-level").dialog('close');
					$('#gridLevel').datagrid('refreshRow', rowLeveIndex);
					$('#gridLevel').datagrid('beginEdit', rowLeveIndex);
				});
			},
			onClose: function() {
				$("#popup-edit-level").dialog("destroy");
				$("#popup-product-container").remove();
			}
		});
	},
	
	editLevelKM: function(rowLeveIndex, groupLevelId) {
		var html = '<div id="popup-product-container">\
			<div id="popup-edit-level">\
			<div class="PopupContentMid">\
			<div class="GeneralForm Search1Form">\
			<div class="Clear"></div>\
			<div id="gridProductContainer">\
			<div id="gridProduct"></div>\
			</div>\
			<div class="Clear"></div>\
			</div>\
			<div class="BtnCenterSection">\
			<button id="btn-update" '+(($('#statusHidden').val() == 0 || $('#statusHidden').val() == 1) ? 'style="display:none;"' : '')+' class="BtnGeneralStyle BtnGeneralMStyle">Cập nhật</button>\
			</div>\
			<div class="Clear"></div>\
			<p class="ErrorMsgStyle SpriteErr" style="display: none;" id="errAddProductMsg"></p></div></div></div>\
			</div>';
		$("body").append(html);
		var row = $('#gridLevel').datagrid('getRows')[rowLeveIndex];
		var detail = [];
		if(row.detail != null && row.detail != undefined && $.isArray(row.detail) && row.detail.length != 0) {
			for(var i = 0; i < row.detail.length; i++) {
				detail.push(row.detail[i]);
			}
		}
		$('#popup-edit-level').dialog({
			title: 'Thông tin sản phẩm của nhóm',
			width: 750,
			heigth: "auto",
			modal: true, close: false, cache: false,
			onOpen: function() {
				$('#gridProduct').datagrid({
					data : detail,
					width: $("#gridProductContainer").width(),
    				height: 'auto',
    				singleSelect: true,
    				fitColumns: true,
    				scrollbarSize:0,
    				columns: [[
    					{field:'id', title:"Mức", width:30,align:'center',sortable:false,resizable:false,formatter:function(value,row,index) {
    						return index + 1;
    					}},
    					{field:'productCode', title:"Sản phẩm mua", width:100,align:'left',sortable:false,resizable:false,formatter:function(value, row, index) {
    						return VTUtilJS.XSSEncode(value);
    					}, 
    						editor: {
    							type:'combobox',
    							 options:{
								 	valueField : 'productCode',
								 	textField : 'productCode',
								 	disabled : true,
								 	data : PromotionProgram._listProduct.valArray,
								 	formatter:function(row){
			                            return row.productCode + ' - ' +row.productName;
			                        },
								 	onSelect: function(r) {
								 		var row = $(this).parent().parent().parent().parent().parent().parent().parent();
								 		row.find('td[field=productName] div').html(r.productName);
								 	}
    							 }
    						}
    					},
    					{field:'productName', title:"Tên sản phẩm", width:180,align:'left',sortable:false,resizable:false,formatter:CommonFormatter.formatNormalCell},
    					{field:'value', title:"Giá trị", width:80,align:'center',sortable:false,resizable:false, editor: {type:'currency'}},
    					{field:'isRequired', title:"Bắt buộc", width:50,align:'center',sortable:false,resizable:false,editor:{type:'checkbox', options:{on:'1',off:'0'}}}/*,
    					{field:'delete', title:'<a '+(($('#statusHidden').val() == 0 || $('#statusHidden').val() == 1) ? 'style="display:none;"' : '')+' onclick="PromotionProgram.editLevelKMAddProduct();"><span style="cursor:pointer"><img title="Thêm mới sản phẩm" src="/resources/images/icon_add.png"/></span></a>', width:30,align:'center',sortable:false,resizable:false,formatter:function(value,row,index){
    						return '<a '+(($('#statusHidden').val() == 0 || $('#statusHidden').val() == 1) ? 'style="display:none;"' : '')+' onclick="PromotionProgram.editLevelKMDeleteProduct('+index+')"><span style="cursor:pointer"><img title="Xóa sản phẩm" src="/resources/images/icon_delete.png"/></span></a>';
    					}},*/
    				]],
    				onDblClickRow: function(index,row) {
    					if($('#statusHidden').val() == 2) {
    						$('#gridProduct').datagrid('beginEdit', index);
    					}
    				},
    				onLoadSuccess: function(data) {
    					if(detail == null || detail == undefined || ($.isArray(detail) && detail.length == 0)) {
    						$('#gridProduct').datagrid('appendRow', {});
    						editIndex = $('#gridProduct').datagrid('getRows').length-1;
    						$('#gridProduct').datagrid('selectRow', editIndex).datagrid('beginEdit', editIndex);
    					}
    					/*var rows = $('#gridProduct').datagrid('getRows');
    					for(var i = 0; i < rows.length; i++) {
    						$('#gridProduct').datagrid('beginEdit', i);
    					}*/
    				}
				});
				$('#popup-edit-level #btn-update').bind('click', function() {
					$('#errAddProductMsg').html('').hide;
					PromotionProgram._levelValueType = $('#valueType').val();
					$('#gridProduct').datagrid('acceptChanges');
					var rows = $('#gridProduct').datagrid('getRows');
					var listDetail = new Array();
					var listProductCode = new Array();
					for(var i = 0; i < rows.length; i++) {
						var row = rows[i];
						var detail = new Object();
						if(VTUtilJS.isNullOrEmpty(row.productCode)) {
							$('#errAddProductMsg').html('Sản phẩm ở dòng ' + (i+1) + ' chưa được nhập').show();
							/*for(var ii = 0; ii < rows.length; ii++) {
								$('#gridProduct').datagrid('beginEdit', ii);
							}*/
							return;
						} else if(listProductCode.indexOf(row.productCode) != -1) {
							$('#errAddProductMsg').html('Sản phẩm ở dòng ' + (i+1) + ' bị trùng mã sản phẩm').show();
							/*for(var ii = 0; ii < rows.length; ii++) {
								$('#gridProduct').datagrid('beginEdit', ii);
							}*/
							return;
						}  else {
							if(PromotionProgram._listProduct.get(row.productCode) == null) {
								$('#errAddProductMsg').html('Sản phẩm ở dòng ' + (i+1) + ' không tồn tại trong hệ thống').show();
								/*for(var ii = 0; ii < rows.length; ii++) {
									$('#gridProduct').datagrid('beginEdit', ii);
								}*/
								return;
							}
							detail.productCode = row.productCode;
							detail.productName = PromotionProgram._listProduct.get(row.productCode).productName;
						}
						if(VTUtilJS.isNullOrEmpty(row.value) || row.value == 0) {
							$('#errAddProductMsg').html('Giá trị ở dòng ' + (i+1) + ' chưa được nhập').show();
							/*for(var ii = 0; ii < rows.length; ii++) {
								$('#gridProduct').datagrid('beginEdit', ii);
							}*/
							return;
						} else if(isNaN(row.value) || Number(row.value) <= 0) {
							$('#errAddProductMsg').html('Giá trị ở dòng ' + (i+1) + ' phải là số nguyên lớn hơn 0').show();
							/*for(var ii = 0; ii < rows.length; ii++) {
								$('#gridProduct').datagrid('beginEdit', ii);
							}*/
							return;
						} else {
							detail.value = row.value;
							detail.valueType = $('#popup-edit-level #valueType').val();
						}
						detail.isRequired = row.isRequired;
						detail.groupLevelId = groupLevelId;
						listDetail.push(detail);
					}
					$('#gridLevel').datagrid('getRows')[rowLeveIndex].detail = listDetail;
					$('#gridLevel').datagrid('getRows')[rowLeveIndex].groupText = PromotionProgram.returnGroupTextKM($('#promotionType').val(), listDetail, $('#gridLevel').datagrid('getRows')[rowLeveIndex].minQuantity, $('#gridLevel').datagrid('getRows')[rowLeveIndex].minAmount);
					$("#popup-edit-level").dialog('close');
					$('#gridLevel').datagrid('refreshRow', rowLeveIndex);
					$('#gridLevel').datagrid('beginEdit', rowLeveIndex);
				});
			},
			onClose: function() {
				$("#popup-edit-level").dialog("destroy");
				$("#popup-product-container").remove();
			}
		});
	},
	
	addLevelMua: function() {
		if(($('#promotionType').val() == 'ZV01' || $('#promotionType').val() == 'ZV02' || $('#promotionType').val() == 'ZV03' || $('#promotionType').val() == 'ZV04' || $('#promotionType').val() == 'ZV05' || $('#promotionType').val() == 'ZV06' ||
				$('#promotionType').val() == 'ZV07' || $('#promotionType').val() == 'ZV08' || $('#promotionType').val() == 'ZV09' || $('#promotionType').val() == 'ZV10' || $('#promotionType').val() == 'ZV11' || $('#promotionType').val() == 'ZV12') && $('#gridLevel').datagrid('getRows').length != 0) {
			var firstRow = $('#gridLevel').datagrid('getRows')[0];
			if(firstRow.detail == null || ($.isArray(firstRow.detail) && firstRow.detail.length == 0)) {
				return;
			}
			var appendRow = {detail:[]};
			for(var i = 0; i < firstRow.detail.length; i++) {
				appendRow.detail.push(firstRow.detail[i]);
			}
			$('#gridLevel').datagrid('appendRow', appendRow);
			var index = $('#gridLevel').datagrid('getRows').length - 1;
			$('#gridLevel').datagrid('selectRow', index).datagrid('beginEdit', index);
		} else if(($('#promotionType').val() == 'ZV13' || $('#promotionType').val() == 'ZV14' || $('#promotionType').val() == 'ZV15' || $('#promotionType').val() == 'ZV16' || $('#promotionType').val() == 'ZV17' || $('#promotionType').val() == 'ZV18') && $('#gridLevel').datagrid('getRows').length != 0) {
			var firstRow = $('#gridLevel').datagrid('getRows')[0];
			if(firstRow.detail == null || ($.isArray(firstRow.detail) && firstRow.detail.length == 0)) {
				return;
			}
			var appendRow = {detail:[]};
			for(var i = 0; i < firstRow.detail.length; i++) {
				var detail = {groupLevelId : firstRow.detail[i].groupLevelId, isRequired : firstRow.detail[i].isRequired, productCode : firstRow.detail[i].productCode, productName : firstRow.detail[i].productName};
				appendRow.detail.push(detail);
			}
			$('#gridLevel').datagrid('appendRow', appendRow);
			var index = $('#gridLevel').datagrid('getRows').length - 1;
			$('#gridLevel').datagrid('selectRow', index).datagrid('beginEdit', index);
		} else {
			$('#gridLevel').datagrid('appendRow', {});
			var index = $('#gridLevel').datagrid('getRows').length - 1;
			$('#gridLevel').datagrid('selectRow', index).datagrid('beginEdit', index);
		}
	},
	
	addLevelKM: function() {
		$('#gridLevel').datagrid('appendRow', {});
		var index = $('#gridLevel').datagrid('getRows').length - 1;
		$('#gridLevel').datagrid('selectRow', index).datagrid('beginEdit', index);
	},
	
	deleteLevelMua: function(index) {
		$('#errMsgGroupDlg').html('').hide();
		if(index != $('#gridLevel').datagrid('getRows').length - 1) {
			$('#errMsgGroupDlg').html('Bạn chỉ được xóa dòng cuối cùng').show();
			return;
		}
		$.messager.confirm("Xác nhận", "Bạn có muốn xóa mức", function(r) {
			if(r) {
				var rowLevel = $('#gridLevel').datagrid('getRows')[index];
				if(!VTUtilJS.isNullOrEmpty(rowLevel.id)) {
					var params = new Object();
					params.levelMuaId = rowLevel.id;
					VTUtilJS.postFormJson(params, '/promotion/delete-level-mua', function(data) {
						if(data.error) {
							$('#errMsgGroupDlg').html(data.errMsg).show();
						} else {
							$('#gridLevel').datagrid('deleteRow', index);
						}
					});
				}
			}
		});
	},
	
	deleteLevelKM: function(index) {
		$('#errMsgGroupDlg').html('').hide();
		if(index != $('#gridLevel').datagrid('getRows').length - 1) {
			$('#errMsgGroupDlg').html('Bạn chỉ được xóa dòng cuối cùng').show();
			return;
		}
		$.messager.confirm("Xác nhận", "Bạn có muốn xóa mức", function(r) {
			if(r) {
				var rowLevel = $('#gridLevel').datagrid('getRows')[index];
				if(!VTUtilJS.isNullOrEmpty(rowLevel.id)) {
					var params = new Object();
					params.levelKMId = rowLevel.id;
					VTUtilJS.postFormJson(params, '/promotion/delete-level-km', function(data) {
						if(data.error) {
							$('#errMsgGroupDlg').html(data.errMsg).show();
						} else {
							$('#gridLevel').datagrid('deleteRow', index);
						}
					});
				}
			}
		});
	},
	
	popupAddLevelMua : function(productGroupId, groupCode, groupName, minQuantity, minAmount, multiple, recursive, stt) {
		var textDisQuantity = '';
		var textDisAmount = '';
		var isDisableGridQuantity = false;
		var isDisableGridAmount = false;
		if($('#promotionType').val() == 'ZV01' || $('#promotionType').val() == 'ZV02' || $('#promotionType').val() == 'ZV03' || 
				$('#promotionType').val() == 'ZV07' || $('#promotionType').val() == 'ZV08' || $('#promotionType').val() == 'ZV09') {
			textDisQuantity = '';
			textDisAmount = 'disabled="disabled"';
			isDisableGridQuantity = false;
			isDisableGridAmount = true;
		} else if($('#promotionType').val() == 'ZV13' || $('#promotionType').val() == 'ZV14' || $('#promotionType').val() == 'ZV15' || $('#promotionType').val() == 'ZV16' || $('#promotionType').val() == 'ZV17' || $('#promotionType').val() == 'ZV18') {
			isDisableGridQuantity = true;
			isDisableGridAmount = true;
		} else {
			textDisQuantity = 'disabled="disabled"';
			textDisAmount = '';
			isDisableGridQuantity = true;
			isDisableGridAmount = false;
		}
		var html = '<div id="popup-container"><div id="popup-add-level" class="PopupContentMid">\
			<div class="GeneralForm Search1Form" id="form-data">\
			<label style="width: 100px;" class="LabelStyle Label1Style">Mã nhóm<span class="ReqiureStyle">(*)</span></label>\
			<input type="text" call-back="VTValidateUtils.getMessageOfRequireCheck(\'groupCode\', \'Mã Nhóm\', false, true);VTValidateUtils.getMessageOfSpecialCharactersValidate(\'groupCode\', \'Mã Nhóm\', Utils._CODE)" class="InputTextStyle InputText1Style" style="width: 145px;" disabled="disabled" maxlength="50" id="groupCode" value="'+Utils.XSSEncode(groupCode)+'"> \
			<label style="width: 100px;" class="LabelStyle Label1Style">Tên nhóm<span class="ReqiureStyle">(*)</span></label> \
			<input type="text" class="InputTextStyle InputText1Style" style="width: 145px;" call-back="VTValidateUtils.getMessageOfRequireCheck(\'promotionName\', \'Tên Nhóm\', false, true);VTValidateUtils.getMessageOfSpecialCharactersValidate(\'groupName\', \'Tên Nhóm\', Utils._NAME)" maxlength="100" id="groupName" value="'+Utils.XSSEncode(groupName)+'">\
			<div class="Clear"></div>';
			html += '<div class="Clear"></div>';
		if($('#promotionType').val() != 'ZV01' && $('#promotionType').val() != 'ZV04' && $('#promotionType').val() != 'ZV07' && $('#promotionType').val() != 'ZV10' && $('#promotionType').val() != 'ZV19') {
			html +=	'<label class="LabelStyle" style="width:100px;">Bội số</label><input type="checkbox" id="multiple" '+(multiple==1?'checked="checked"':'')+' class="InputCbxStyle"/>\
			<label class="LabelStyle" style="width:131px;">Tối ưu</label><input type="checkbox" id="recursive" '+(recursive==1?'checked="checked"':'')+' class="InputCbxStyle"/>';
		} else {
			html +=	'<label class="LabelStyle" style="width:100px; visibility: hidden;">Bội số</label><input style="visibility: hidden;" type="checkbox" id="multiple" '+(multiple==1?'checked="checked"':'')+' class="InputCbxStyle"/>\
			<label class="LabelStyle" style="width:131px; visibility: hidden;">Tối ưu</label><input style="visibility: hidden;" type="checkbox" id="recursive" '+(recursive==1?'checked="checked"':'')+' class="InputCbxStyle"/>';
		}
		html +=	'<label class="LabelStyle Label1Style">Thứ tự ưu tiên</label> \
			<input type="text" class="InputTextStyle InputText1Style" style="width: 145px;" maxlength="3" id="stt" value="'+stt+'">\
			<input type="hidden" id="groupId" value="'+productGroupId+'">\
			<div class="Clear"></div>\
			<div class="BtnCenterSection">\
			<button class="BtnGeneralStyle" '+(($('#statusHidden').val() == 0 || $('#statusHidden').val() == 1) ? 'style="display:none;"' : '')+' id="btn-update">Cập nhật</button>\
			</div>\
			<p class="ErrorMsgStyle SpriteErr" style="display: none;" id="errMsgGroupDlg"></p>\
			<p class="SuccessMsgStyle" style="display: none;" id="successMsgGroupDlg"></p>\
			<div class="SearchInSection SProduct1Form">\
			<h2 class="Title2Style">Khai báo mức cho nhóm mua: '+groupName+'</h2>\
			<div class="GridSection" id="promotionGrid">\
			<div id="gridLevel"></div>\
			</div>\
			</div>\
			</div>';
		$("body").append(html);
		Utils.bindFormatOnTextfield('stt', Utils._TF_NUMBER);
		$('#popup-add-level').dialog({
			title: 'Thông tin nhóm mua',
			width: 750,
			heigth: "auto",
			modal: true, close: false, cache: false,
			onOpen: function() {
				VTUtilJS.bindFormatOnTextfield('minQuantity', VTUtilJS._TF_NUMBER);
				VTUtilJS.formatCurrencyFor('minQuantity');
				VTUtilJS.bindFormatOnTextfield('minAmount', VTUtilJS._TF_NUMBER);
				VTUtilJS.formatCurrencyFor('minAmount');
				$('#gridLevel').datagrid({
					url: "/promotion/list-level",
    				queryParams: {groupId:productGroupId},
    				width: $("#promotionGrid").width(),
    				height: 'auto',
    				singleSelect: true,
    				fitColumns: true,
    				scrollbarSize:0,
    				pagination:false,
    				columns: [[
    					{field:'id', title:"Mức", width:20,align:'center',sortable:false,resizable:false,formatter:function(value,row,index) {
    						return Number(index) + 1;
    					}},
    					{field:'groupText', title:"Sản phẩm mua", width:100,align:'left',sortable:false,resizable:false,formatter:function(value, row, index) {
    						if(VTUtilJS.isNullOrEmpty(value)) {
    							if(!VTUtilJS.isNullOrEmpty(row.detail) && $.isArray(row.detail)) {
    								if($('#promotionType').val() == 'ZV01' || $('#promotionType').val() == 'ZV02' || $('#promotionType').val() == 'ZV03') {
    									var ed = $('#gridLevel').datagrid('getEditor', {index:index, field:'minQuantity'});
    									if(ed != null) {
    										var val = $(ed.target).val();
        									var text = PromotionProgram.returnGroupTextMua($('#promotionType').val(), row.detail, val, null);
        									return text;
    									} else {
    										return '';
    									}
    								} else if($('#promotionType').val() == 'ZV04' || $('#promotionType').val() == 'ZV05' || $('#promotionType').val() == 'ZV06') {
    									var ed = $('#gridLevel').datagrid('getEditor', {index:index, field:'minAmount'});
    									if(ed != null) {
    										var val = $(ed.target).val();
        									var text = PromotionProgram.returnGroupTextMua($('#promotionType').val(), row.detail, null, val);
        									return text;
    									} else {
    										return '';
    									}
    								} else {
    									var text = PromotionProgram.returnGroupTextMua($('#promotionType').val(), row.detail, null, null);
    									return text;
    								}
    							} else {
    								return '';
    							}
    						} else {
    							if(VTUtilJS.isNullOrEmpty(PromotionProgram._levelValueType)) {
    								if(row.detail != null && row.detail != undefined && $.isArray(row.detail) && row.detail.length > 0) {
    									if(row.detail[0].valueType != null && row.detail[0].valueType != undefined) {
    										PromotionProgram._levelValueType = row.detail[0].valueType;
    									}
    								}
    							}
    							return VTUtilJS.XSSEncode(value);
    						}
    					}},
    					{field:'minQuantity', title:"Số lượng tối thiểu", width:50,align:'right',sortable:false,resizable:false,formatter:function(value, row, index){
    						if(value == 0 || value == 'null' || VTUtilJS.isNullOrEmpty(value)) {
    							return '';
    						} else {
    							return CommonFormatter.formatNormalCell(value, row, index);
    						}
    					}, 
    						editor: {
    							type:'currency', 
    							options : {
    								isDisable: isDisableGridQuantity,
	    							blur: function(ed) {
	    								var val = $(ed).val();
	    								if($('#promotionType').val() == 'ZV01' || $('#promotionType').val() == 'ZV02' || $('#promotionType').val() == 'ZV03' || $('#promotionType').val() == 'ZV07' || $('#promotionType').val() == 'ZV08' || $('#promotionType').val() == 'ZV09') {
	    									var index = $(ed).parent().parent().parent().parent().parent().parent().parent().attr('datagrid-row-index');
	    									var thisRow = $('#gridLevel').datagrid('getRows')[index];
	    									var lstDetail = thisRow.detail;
	    									$('#gridLevel').datagrid('getRows')[index].groupText = PromotionProgram.returnGroupTextMua($('#promotionType').val(), lstDetail, val, null);
	    									$('#gridLevel').datagrid('getRows')[index].minQuantity = val;
	    									$('#gridLevel').datagrid('getRows')[index].orderNumber = $($('#gridLevel').datagrid('getEditor', {index : index, field : 'orderNumber'}).target).val();
	    									$('#gridLevel').datagrid('refreshRow', index);
	    									$('#gridLevel').datagrid('beginEdit', index);
	    									setTimeout(function() {
	    										$($('#gridLevel').datagrid('getEditor', {index:index, field:'minQuantity'}).target).focusEnd();
	    									}, 1);
	    								} else {
	    									
	    								}
	    							}
    							}
    						}
    					},
    					{field:'minAmount', title:"Số tiền tối thiểu", width:50,align:'right',sortable:false,resizable:false,formatter:function(value, row, index){
    						if(value == 0 || value == 'null' || VTUtilJS.isNullOrEmpty(value)) {
    							return '';
    						} else {
    							return CommonFormatter.formatNormalCell(value, row, index);
    						}
    					}, 
    						editor: {
    							type:'currency', 
    							options : {
    								isDisable: isDisableGridAmount,
	    							blur: function(ed) {
	    								var val = $(ed).val();
	    								if($('#promotionType').val() == 'ZV03' || $('#promotionType').val() == 'ZV04' || $('#promotionType').val() == 'ZV05' || 
	    										$('#promotionType').val() == 'ZV10' || $('#promotionType').val() == 'ZV11' || $('#promotionType').val() == 'ZV12') {
	    									var index = $(ed).parent().parent().parent().parent().parent().parent().parent().attr('datagrid-row-index');
	    									var thisRow = $('#gridLevel').datagrid('getRows')[index];
	    									var lstDetail = thisRow.detail;
	    									$('#gridLevel').datagrid('getRows')[index].groupText = PromotionProgram.returnGroupTextMua($('#promotionType').val(), lstDetail, null, val);
	    									$('#gridLevel').datagrid('getRows')[index].minAmount = val;
	    									$('#gridLevel').datagrid('getRows')[index].orderNumber = $($('#gridLevel').datagrid('getEditor', {index : index, field : 'orderNumber'}).target).val();
	    									$('#gridLevel').datagrid('refreshRow', index);
	    									$('#gridLevel').datagrid('beginEdit', index);
	    									setTimeout(function() {
	    										$($('#gridLevel').datagrid('getEditor', {index:index, field:'minAmount'}).target).focusEnd();
	    									}, 1);
	    								} else {
	    									
	    								}
	    							}
    							}
    						}
    					},
    					{field:'orderNumber', title:"Thứ tự", width:30,align:'center',sortable:false,resizable:false, editor: {type:'currency', options : {maxLength:3}}},
    					{field:'edit', title:"", width:30,align:'center',sortable:false,resizable:false,formatter:function(value,row,index){
    						if($('#promotionType').val() == 'ZV19' || $('#promotionType').val() == 'ZV20' || $('#promotionType').val() == 'ZV21') {
    							return '';
    						} else {
    							return '<a onclick="PromotionProgram.editLevelMua('+index+', '+row.id+')"><span style="cursor:pointer"><img title="Thông tin sản phẩm bán" src="/resources/images/icon_edit.png"/></span></a>';
    						}
    					}}
    				]],
    				onDblClickRow: function(index,row) {
    					if($('#statusHidden').val() == 2) {
    						$('#gridLevel').datagrid('beginEdit', index);
    					}
    				},
    				onLoadSuccess: function(data) {
    					/*for(var i = 0; i < data.rows.length; i++) {
    						$('#gridLevel').datagrid('beginEdit', i);
    					}*/
    				}
				});
				$('#btn-update').bind('click', function() {
					$('#errMsgGroupDlg').html('').hide();
					var params = VTUtilJS.getFormData('form-data');
					if($('#errorMsg').html().trim().length > 0 && $('#errorMsg').is(":visible")) {
						$('#errMsgGroupDlg').html($('#errorMsg').html()).show();
						$('#errorMsg').html('').hide();
					}
					if(params == null) {
						return;
					}
					$('#errMsgGroupDlg').html('').hide();
					if(VTUtilJS.isNullOrEmpty(params.groupCode)) {
						$('#errMsgGroupDlg').html('Mã nhóm chưa được khai nhập').show();
						return;
					}
					if(VTUtilJS.isNullOrEmpty(params.groupName)) {
						$('#errMsgGroupDlg').html('Tên nhóm chưa được khai nhập').show();
						return;
					}
					params.promotionId = $('#id').val();
					var rows = $('#gridLevel').datagrid('getRows');
					for(var i = 0; i < rows.length; i++) {
						if(!$('#gridLevel').datagrid('validateRow', i)) {
							$('#errMsgGroupDlg').html('Dữ liệu nhập không hợp lệ ở dòng '+(i+1)).show();
						}
					}
					$('#gridLevel').datagrid('acceptChanges');
					rows = $('#gridLevel').datagrid('getRows');
					var listMinQuantity = new Array();
					var listMinAmount = new Array();
					var listOrder = new Array();
					var listDetail = new Array();
					var listLevelId = new Array();
					for(var i = 0; i < rows.length; i++) {
						var row = rows[i];
						var minQuantity = VTUtilJS.isNullOrEmpty(row.minQuantity) ? 0 : VTUtilJS.returnMoneyValue(row.minQuantity);
						minQuantity = isNaN(minQuantity) ? 0 : Number(minQuantity);
						var minAmount = VTUtilJS.isNullOrEmpty(row.minAmount) ? 0 : VTUtilJS.returnMoneyValue(row.minAmount);
						minAmount = isNaN(minAmount) ? 0 : Number(minAmount);
						var order = VTUtilJS.isNullOrEmpty(row.orderNumber) ? 0 : row.orderNumber;
						order = isNaN(order) ? 0 : Number(order);
						var levelId = VTUtilJS.isNullOrEmpty(row.id) ? 0 : row.id;
						if($('#promotionType').val() == 'ZV01' || $('#promotionType').val() == 'ZV02' || $('#promotionType').val() == 'ZV03' ||
								$('#promotionType').val() == 'ZV07' || $('#promotionType').val() == 'ZV08' || $('#promotionType').val() == 'ZV09') {
							if(VTUtilJS.isNullOrEmpty(minQuantity) || minQuantity <= 0) {
								$('#errMsgGroupDlg').html('Số lượng nhập không đúng định dạng').show();
								for(var ii = 0; ii < rows.length; ii++) {
		    						$('#gridLevel').datagrid('beginEdit', ii);
		    					}
								return;
							}
							/*if(i != 0) {
								var preRow = rows[i-1];
								var preMinQuantity = VTUtilJS.isNullOrEmpty(preRow.minQuantity) ? 0 : VTUtilJS.returnMoneyValue(preRow.minQuantity);
								var preOrder = VTUtilJS.isNullOrEmpty(preRow.orderNumber) ? 0 : preRow.orderNumber;
								if(Number(minQuantity) <= Number(preMinQuantity)) {
									$('#errMsgGroupDlg').html('Giá trị tối thiểu phải tăng dần').show();
									for(var ii = 0; ii < rows.length; ii++) {
			    						$('#gridLevel').datagrid('beginEdit', ii);
			    					}
									return;
								}
								if(Number(order) <= Number(preOrder)) {
									$('#errMsgGroupDlg').html('Số thứ tự phải tăng dần').show();
									for(var ii = 0; ii < rows.length; ii++) {
			    						$('#gridLevel').datagrid('beginEdit', ii);
			    					}
									return;
								}
							}*/
						} else if($('#promotionType').val() == 'ZV04' || $('#promotionType').val() == 'ZV05' || $('#promotionType').val() == 'ZV06' ||
								$('#promotionType').val() == 'ZV10' || $('#promotionType').val() == 'ZV11' || $('#promotionType').val() == 'ZV12') {
							if(VTUtilJS.isNullOrEmpty(minAmount) || minAmount <= 0) {
								$('#errMsgGroupDlg').html('Số lượng nhập không đúng định dạng').show();
								for(var ii = 0; ii < rows.length; ii++) {
		    						$('#gridLevel').datagrid('beginEdit', ii);
		    					}
								return;
							}
							/*if(i != 0) {
								var preRow = rows[i-1];
								var preMinAmount = VTUtilJS.isNullOrEmpty(preRow.minAmount) ? 0 : VTUtilJS.returnMoneyValue(preRow.minAmount);
								preMinAmount = isNaN(preMinAmount) ? 0 : Number(preMinAmount);
								var preOrder = VTUtilJS.isNullOrEmpty(preRow.orderNumber) ? 0 : preRow.orderNumber;
								preOrder = isNaN(preOrder) ? 0 : Number(preOrder);
								if(Number(minAmount) <= Number(preMinAmount)) {
									$('#errMsgGroupDlg').html('Giá trị tối thiểu phải tăng dần').show();
									for(var ii = 0; ii < rows.length; ii++) {
			    						$('#gridLevel').datagrid('beginEdit', ii);
			    					}
									return;
								}
								if(Number(order) <= Number(preOrder)) {
									$('#errMsgGroupDlg').html('Số thứ tự phải tăng dần').show();
									for(var ii = 0; ii < rows.length; ii++) {
			    						$('#gridLevel').datagrid('beginEdit', ii);
			    					}
									return;
								}
							}*/
						}
						if(VTUtilJS.isNullOrEmpty(order) || order <= 0) {
							$('#errMsgGroupDlg').html('Số thứ tự không được để trống').show();
							for(var ii = 0; ii < rows.length; ii++) {
	    						$('#gridLevel').datagrid('beginEdit', ii);
	    					}
							return;
						}
						/*if(i != 0) {
							var preRow = rows[i-1];
							var preOrder = VTUtilJS.isNullOrEmpty(preRow.orderNumber) ? 0 : preRow.orderNumber;
							preOrder = isNaN(preOrder) ? 0 : Number(preOrder);
							if(Number(order) <= Number(preOrder)) {
								$('#errMsgGroupDlg').html('Số thứ tự phải tăng dần').show();
								for(var ii = 0; ii < rows.length; ii++) {
		    						$('#gridLevel').datagrid('beginEdit', ii);
		    					}
								return;
							}
						}*/
						if(minQuantity != 0 && listMinQuantity.indexOf(minQuantity) != -1) {
							$('#errMsgGroupDlg').html('Số lượng tối thiểu trùng nhau').show();
							for(var ii = 0; ii < rows.length; ii++) {
	    						$('#gridLevel').datagrid('beginEdit', ii);
	    					}
							return;
						}
						if(minAmount != 0 && listMinAmount.indexOf(minAmount) != -1) {
							$('#errMsgGroupDlg').html('Số tiền tối thiểu trùng nhau').show();
							for(var ii = 0; ii < rows.length; ii++) {
	    						$('#gridLevel').datagrid('beginEdit', ii);
	    					}
							return;						
						}
						if(listOrder.indexOf(order) != -1) {
							$('#errMsgGroupDlg').html('Số thứ tự trùng nhau').show();
							for(var ii = 0; ii < rows.length; ii++) {
	    						$('#gridLevel').datagrid('beginEdit', ii);
	    					}
							return;	
						}
						listMinQuantity.push(minQuantity);
						listMinAmount.push(minAmount);
						listOrder.push(order);
						listLevelId.push(levelId);
						if($('#promotionType').val() == 'ZV13' || $('#promotionType').val() == 'ZV14' || $('#promotionType').val() == 'ZV15' || $('#promotionType').val() == 'ZV16' || $('#promotionType').val() == 'ZV17' || $('#promotionType').val() == 'ZV18') {
							if(!VTUtilJS.isNullOrEmpty(PromotionProgram._levelValueType) && $.isArray(row.detail)) {
								var detailText = PromotionProgram._levelValueType;
								for(var j = 0; j < row.detail.length; j++) {
									detailText += ';' + row.detail[j].productCode + '-' + (VTUtilJS.isNullOrEmpty(row.detail[j].value) ? 0 : row.detail[j].value) + ','+row.detail[j].isRequired;
								}
								listDetail.push(detailText);
							}
						} else {
							if($.isArray(row.detail)) {
								var detailText = '';
								if(VTUtilJS.isNullOrEmpty(PromotionProgram._levelValueType)) {
									if($('#promotionType').val() == 'ZV01' || $('#promotionType').val() == 'ZV02' || $('#promotionType').val() == 'ZV03' ||
											$('#promotionType').val() == 'ZV07' || $('#promotionType').val() == 'ZV08' || $('#promotionType').val() == 'ZV09') {
										detailText = '1';
									} else if($('#promotionType').val() == 'ZV04' || $('#promotionType').val() == 'ZV05' || $('#promotionType').val() == 'ZV06' || 
											$('#promotionType').val() == 'ZV10' || $('#promotionType').val() == 'ZV11' || $('#promotionType').val() == 'ZV12') {
										detailText = '2';
									}
								} else {
									detailText = VTUtilJS.isNullOrEmpty(PromotionProgram._levelValueType) ? 0 : PromotionProgram._levelValueType;
								}
								for(var j = 0; j < row.detail.length; j++) {
									detailText += ';' + row.detail[j].productCode + '-0' + ','+((row.detail[j].isRequired == true || row.detail[j].isRequired == 1 || row.detail[j].isRequired == '1')?'1':'0');
								}
								listDetail.push(detailText);
							}
						}
					}
					if(!VTUtilJS.isNullOrEmpty(PromotionProgram._levelValueType)) {
						if(listMinQuantity.length != listDetail.length) {
							$('#errMsgGroupDlg').html('Bạn chưa nhập dữ liệu sản phẩm cho mức').show();
							for(var ii = 0; ii < rows.length; ii++) {
	    						$('#gridLevel').datagrid('beginEdit', ii);
	    					}
						}
					}
					params.listMinQuantity = listMinQuantity;
					params.listMinAmount = listMinAmount;
					params.listOrder = listOrder;
					params.listProductDetail = listDetail;
					params.listLevelId = listLevelId;
					$.messager.confirm("Xác nhận", "Bạn có muốn cập nhập mức cho nhóm?", function(r) {
						if(r) {
							VTUtilJS.postFormJson(params, '/promotion/update-group-sale', function(data) {
								if(data.error) {
									$('#errMsgGroupDlg').html(data.errMsg).show();
									for(var ii = 0; ii < rows.length; ii++) {
			    						$('#gridLevel').datagrid('beginEdit', ii);
			    					}
								} else {
									$('#groupMuaGrid').datagrid('reload');
									$('#popup-add-level').dialog('close');
								}
							});
						} else {
							for(var ii = 0; ii < rows.length; ii++) {
	    						$('#gridLevel').datagrid('beginEdit', ii);
	    					}
						}
					});
				});
			},
			onClose: function() {
				PromotionProgram._levelValueType = null;
				$("#popup-add-level").dialog("destroy");
				$("#popup-container").remove();
			}
		});
	},
	
	popupAddLevelKM : function(productGroupId, groupCode, groupName, maxQuantity, maxAmount) {
		var textDisQuantity = '';
		var textDisAmount = '';
		var isDisableGridPercent = false;
		var isDisableGridQuantity = false;
		var isDisableGridAmount = false;
		if($('#promotionType').val() == 'ZV01' || $('#promotionType').val() == 'ZV04' || $('#promotionType').val() == 'ZV07' || 
				$('#promotionType').val() == 'ZV10' || $('#promotionType').val() == 'ZV13' || $('#promotionType').val() == 'ZV16' ||
				$('#promotionType').val() == 'ZV19') {
			textDisQuantity = 'disabled="disabled"';
			textDisAmount = 'disabled="disabled"';
			var isDisableGridPercent = false;
			isDisableGridQuantity = true;
			isDisableGridAmount = true;
		} else if($('#promotionType').val() == 'ZV02' || $('#promotionType').val() == 'ZV05' || $('#promotionType').val() == 'ZV08' || 
				$('#promotionType').val() == 'ZV11' || $('#promotionType').val() == 'ZV14' || $('#promotionType').val() == 'ZV17' ||
				$('#promotionType').val() == 'ZV20'){
			textDisQuantity = 'disabled="disabled"';
			textDisAmount = '';
			var isDisableGridPercent = true;
			isDisableGridQuantity = true;
			isDisableGridAmount = false;
		} else {
			textDisQuantity = 'disabled="disabled"';
			textDisAmount = '';
			var isDisableGridPercent = true;
			isDisableGridQuantity = true;
			isDisableGridAmount = true;
		}
		var html = '<div id="popup-container"><div id="popup-add-level" class="PopupContentMid">\
			<div class="GeneralForm Search1Form" id="form-data">\
			<label style="width: 100px;" class="LabelStyle Label1Style">Mã Nhóm<span class="ReqiureStyle">(*)</span></label>\
			<input type="text" class="InputTextStyle InputText1Style" call-back="VTValidateUtils.getMessageOfRequireCheck(\'groupCode\', \'Mã Nhóm\', false, true);VTValidateUtils.getMessageOfSpecialCharactersValidate(\'groupCode\', \'Mã Nhóm\', Utils._CODE)" style="width: 145px;" disabled="disabled" maxlength="50" id="groupCode" value="'+Utils.XSSEncode(groupCode)+'"> \
			<label style="width: 100px;" class="LabelStyle Label1Style">Tên Nhóm<span class="ReqiureStyle">(*)</span></label> \
			<input type="text" call-back="VTValidateUtils.getMessageOfRequireCheck(\'promotionName\', \'Tên Nhóm\', false, true);VTValidateUtils.getMessageOfSpecialCharactersValidate(\'groupName\', \'Tên Nhóm\', Utils._NAME)" class="InputTextStyle InputText1Style" style="width: 145px;" maxlength="100" id="groupName" value="'+Utils.XSSEncode(groupName)+'">\
			<div class="Clear"></div>';
			html += '<input type="hidden" id="groupId" value="'+productGroupId+'">\
			<div class="Clear"></div>\
			<div class="BtnCenterSection">\
			<button class="BtnGeneralStyle" '+(($('#statusHidden').val() == 0 || $('#statusHidden').val() == 1) ? 'style="display:none;"' : '')+' id="btn-update">Cập nhật</button>\
			</div>\
			<p class="ErrorMsgStyle SpriteErr" style="display: none;" id="errMsgGroupDlg"></p>\
			<p class="SuccessMsgStyle" style="display: none;" id="successMsgGroupDlg"></p>\
			<div class="SearchInSection SProduct1Form">\
			<h2 class="Title2Style">Khai báo mức cho nhóm KM: '+Utils.XSSEncode(groupName)+'</h2>\
			<div class="GridSection" id="promotionGrid">\
			<div id="gridLevel"></div>\
			</div>\
			</div>\
			</div>';
		$("body").append(html);
		bindFormatOnTextfield($('#stt'), _TF_NUMBER);
		$('#popup-add-level').dialog({
			title: 'Thông tin nhóm KM',
			width: 750,
			heigth: "auto",
			modal: true, close: false, cache: false,
			onOpen: function() {
				VTUtilJS.bindFormatOnTextfield('maxQuantity', VTUtilJS._TF_NUMBER);
				VTUtilJS.formatCurrencyFor('maxQuantity');
				VTUtilJS.bindFormatOnTextfield('maxAmount', VTUtilJS._TF_NUMBER);
				VTUtilJS.formatCurrencyFor('maxAmount');
				$('#gridLevel').datagrid({
					url: "/promotion/list-level",
    				queryParams: {groupId:productGroupId},
    				width: $("#promotionGrid").width(),
    				height: 'auto',
    				singleSelect: true,
    				fitColumns: true,
    				scrollbarSize:0,
    				pagination:false,
    				columns: [[
    					{field:'id', title:"Mức", width:20,align:'center',sortable:false,resizable:false,formatter:function(value,row,index) {
    						return Number(index) + 1;
    					}},
    					{field:'groupText', title:"Sản phẩm mua", width:100,align:'left',sortable:false,resizable:false,formatter:function(value, row, index) {
    						if(VTUtilJS.isNullOrEmpty(value)) {
    							if(!VTUtilJS.isNullOrEmpty(row.detail) && $.isArray(row.detail)) {
									var text = PromotionProgram.returnGroupTextMua($('#promotionType').val(), row.detail, null, null);
									return text;
    								var text = '(';
    							} else {
    								return '';
    							}
    						} else {
    							if(VTUtilJS.isNullOrEmpty(PromotionProgram._levelValueType)) {
    								if(row.detail != null && row.detail != undefined && $.isArray(row.detail) && row.detail.length > 0) {
    									if(row.detail[0].valueType != null && row.detail[0].valueType != undefined) {
    										PromotionProgram._levelValueType = row.detail[0].valueType;
    									}
    								}
    							}
    							return VTUtilJS.XSSEncode(value);
    						}
    					}},
    					{field:'promotionPercent', title:"%KM", width:20,align:'right',sortable:false,resizable:false,formatter:CommonFormatter.formatNormalCell, editor: {type:'percent', options : {isDisable: isDisableGridPercent}}},
    					{field:'maxQuantity', title:"Số lượng tối đa", width:50,align:'right',sortable:false,resizable:false,formatter:function(value, row, index){
    						if(value == 0 || value == 'null' || VTUtilJS.isNullOrEmpty(value)) {
    							return '';
    						} else {
    							return CommonFormatter.formatNormalCell(value, row, index);
    						}
    					}, 
    						editor: {
    							type:'currency', 
    							options : {
    								isDisable: isDisableGridQuantity
    							}
    						}
    					},
    					{field:'maxAmount', title:"Số tiền tối đa", width:50,align:'right',sortable:false,resizable:false,formatter:function(value, row, index){
    						if(value == 0 || value == 'null' || VTUtilJS.isNullOrEmpty(value)) {
    							return '';
    						} else {
    							return CommonFormatter.formatNormalCell(value, row, index);
    						}
    					}, 
    						editor: {
    							type:'currency', 
    							options : {
    								isDisable: isDisableGridAmount
    							}
    						}
    					},
    					{field:'orderNumber', title:"Thứ tự", width:30,align:'center',sortable:false,resizable:false, editor: {type:'currency', options : {maxLength:3}}},
    					{field:'edit', title:"", width:30,align:'center',sortable:false,resizable:false,formatter:function(value,row,index){
    						if($('#promotionType').val() == 'ZV03' || $('#promotionType').val() == 'ZV06' || $('#promotionType').val() == 'ZV09' || 
									$('#promotionType').val() == 'ZV12' || $('#promotionType').val() == 'ZV15' || $('#promotionType').val() == 'ZV18' ||
									$('#promotionType').val() == 'ZV21') {
    							return '<a onclick="PromotionProgram.editLevelKM('+index+', '+row.id+')"><span style="cursor:pointer"><img title="Thôn tin sản phẩm KM" src="/resources/images/icon_edit.png"/></span></a>';
    						} else {
    							return '';
    						}
    					}}/*,
    					{field:'delete', title:'<a '+(($('#statusHidden').val() == 0 || $('#statusHidden').val() == 1) ? 'style="display:none;"' : '')+' onclick="PromotionProgram.addLevelKM()"><span style="cursor:pointer"><img title="Thêm mới mức" src="/resources/images/icon_add.png"/></span></a>', width:30,align:'center',sortable:false,resizable:false,formatter:function(value,row,index){
    						return '<a '+(($('#statusHidden').val() == 0 || $('#statusHidden').val() == 1) ? 'style="display:none;"' : '')+' onclick="PromotionProgram.deleteLevelKM('+index+')"><span style="cursor:pointer"><img title="Xóa mức" src="/resources/images/icon_delete.png"/></span></a>';
    					}},*/
    				]],
    				onDblClickRow: function(index,row) {
    					if($('#statusHidden').val() == 2) {
    						$('#gridLevel').datagrid('beginEdit', index);
    					}
    				},
    				onLoadSuccess: function(data) {
    					/*for(var i = 0; i < data.rows.length; i++) {
    						$('#gridLevel').datagrid('beginEdit', i);
    					}*/
    				}
				});
				$('#btn-update').bind('click', function() {
					var params = VTUtilJS.getFormData('form-data');
					if($('#errorMsg').html().trim().length > 0 && $('#errorMsg').is(":visible")) {
						$('#errMsgGroupDlg').html($('#errorMsg').html()).show();
						$('#errorMsg').html('').hide();
					}
					if(params == null) {
						return;
					}
					$('#errMsgGroupDlg').html('').hide();
					if(VTUtilJS.isNullOrEmpty(params.groupCode)) {
						$('#errMsgGroupDlg').html('Mã nhóm chưa được khai nhập').show();
						return;
					}
					if(VTUtilJS.isNullOrEmpty(params.groupName)) {
						$('#errMsgGroupDlg').html('Tên nhóm chưa được khai nhập').show();
						return;
					}
					params.promotionId = $('#id').val();
					var rows = $('#gridLevel').datagrid('getRows');
					for(var i = 0; i < rows.length; i++) {
						if(!$('#gridLevel').datagrid('validateRow', i)) {
							$('#errMsgGroupDlg').html('Dữ liệu nhập không hợp lệ ở dòng '+(i+1)).show();
						}
					}
					$('#gridLevel').datagrid('acceptChanges');
					rows = $('#gridLevel').datagrid('getRows');
					var listPercent = new Array();
					var listMaxQuantity = new Array();
					var listMaxAmount = new Array();
					var listOrder = new Array();
					var listDetail = new Array();
					var listLevelId = new Array();
					for(var i = 0; i < rows.length; i++) {
						var row = rows[i]; 
						var percent = VTUtilJS.isNullOrEmpty(row.promotionPercent) ? 0 : VTUtilJS.returnMoneyValue(row.promotionPercent);
						var maxQuantity = VTUtilJS.isNullOrEmpty(row.maxQuantity) ? 0 : VTUtilJS.returnMoneyValue(row.maxQuantity);
						var maxAmount = VTUtilJS.isNullOrEmpty(row.maxAmount) ? 0 : VTUtilJS.returnMoneyValue(row.maxAmount);
						var order = VTUtilJS.isNullOrEmpty(row.orderNumber) ? 0 : row.orderNumber;
						var levelId = VTUtilJS.isNullOrEmpty(row.id) ? 0 : row.id;
						
						if(!(0 <= Number(percent) && Number(percent) <= 100)) {
							$('#errMsgGroupDlg').html('Số phần phải lớn hơn 0 và nhỏ hơn 100').show();
							for(var ii = 0; ii < rows.length; ii++) {
	    						$('#gridLevel').datagrid('beginEdit', ii);
	    					}
							return;
						}
						
						if(VTUtilJS.isNullOrEmpty(order) || order <= 0) {
							$('#errMsgGroupDlg').html('Số thứ tự không được để trống').show();
							for(var ii = 0; ii < rows.length; ii++) {
	    						$('#gridLevel').datagrid('beginEdit', ii);
	    					}
							return;
						}
						if(row.detail != null && row.detail != undefined && $.isArray(row.detail) && row.detail.length > 0) {
							var detailText = 1;
							for(var j = 0; j < row.detail.length; j++) {
								detailText += ';' + Utils.XSSEncode(row.detail[j].productCode + '-' + row.detail[j].value + ','+row.detail[j].isRequired);
							}
							listDetail.push(detailText);
						} else if($('#promotionType').val() == 'ZV03' || $('#promotionType').val() == 'ZV06' || $('#promotionType').val() == 'ZV09' ||
								$('#promotionType').val() == 'ZV12' || $('#promotionType').val() == 'ZV15' || $('#promotionType').val() == 'ZV18' ||
								$('#promotionType').val() == 'ZV21') {
							$('#errMsgGroupDlg').html('Danh sách sản phẩm khuyến mãi ở mức '+(i+1)+' chưa được nhập').show();
							for(var ii = 0; ii < rows.length; ii++) {
	    						$('#gridLevel').datagrid('beginEdit', ii);
	    					}
							return;
						}
						if($('#promotionType').val() == 'ZV01' || $('#promotionType').val() == 'ZV04' || $('#promotionType').val() == 'ZV07' ||
								$('#promotionType').val() == 'ZV10' || $('#promotionType').val() == 'ZV13' || $('#promotionType').val() == 'ZV16' ||
								$('#promotionType').val() == 'ZV19') {
							if(VTUtilJS.isNullOrEmpty(percent) || percent <= 0) {
								$('#errMsgGroupDlg').html('Phần trăm không được để trống').show();
								for(var ii = 0; ii < rows.length; ii++) {
		    						$('#gridLevel').datagrid('beginEdit', ii);
		    					}
								return;
							}
						} else if($('#promotionType').val() == 'ZV02' || $('#promotionType').val() == 'ZV05' || $('#promotionType').val() == 'ZV08' ||
								$('#promotionType').val() == 'ZV11' || $('#promotionType').val() == 'ZV14' || $('#promotionType').val() == 'ZV17' ||
								$('#promotionType').val() == 'ZV20') {
							if(VTUtilJS.isNullOrEmpty(maxAmount) || maxAmount <= 0) {
								$('#errMsgGroupDlg').html('Số tiền tối đa không đúng định dạng').show();
								for(var ii = 0; ii < rows.length; ii++) {
		    						$('#gridLevel').datagrid('beginEdit', ii);
		    					}
								return;
							}
						} else if($('#promotionType').val() == 'ZV03' || $('#promotionType').val() == 'ZV06' || $('#promotionType').val() == 'ZV09' ||
								$('#promotionType').val() == 'ZV12' || $('#promotionType').val() == 'ZV15' || $('#promotionType').val() == 'ZV18' ||
								$('#promotionType').val() == 'ZV21') {
							if(listDetail.length == 0) {
								$('#errMsgGroupDlg').html('Số tiền tối đa không đúng định dạng').show();
								for(var ii = 0; ii < rows.length; ii++) {
		    						$('#gridLevel').datagrid('beginEdit', ii);
		    					}
								return;
							}
						}
						
						if(listOrder.indexOf(Number(order)) != -1) {
							$('#errMsgGroupDlg').html('Số thứ tự trùng nhau').show();
							for(var ii = 0; ii < rows.length; ii++) {
	    						$('#gridLevel').datagrid('beginEdit', ii);
	    					}
							return;	
						}
						
						/*if(i != 0) {
							var preOrder = VTUtilJS.isNullOrEmpty(rows[i-1].orderNumber) ? 0 : rows[i-1].orderNumber;
							if(preOrder >= order) {
								$('#errMsgGroupDlg').html('Số thứ tự phải tăng dần').show();
								for(var ii = 0; ii < rows.length; ii++) {
		    						$('#gridLevel').datagrid('beginEdit', ii);
		    					}
								return;
							}
						}*/
						
						listPercent.push(percent);
						listMaxQuantity.push(maxQuantity);
						listMaxAmount.push(maxAmount);
						listOrder.push(Number(order));
						listLevelId.push(levelId);
					}
					if($('#promotionType').val() == 'ZV03' || $('#promotionType').val() == 'ZV06' || $('#promotionType').val() == 'ZV09' ||
							$('#promotionType').val() == 'ZV12' || $('#promotionType').val() == 'ZV15' || $('#promotionType').val() == 'ZV18' ||
							$('#promotionType').val() == 'ZV21') {
						if(listMaxQuantity.length != listDetail.length) {
							for(var ii = 0; ii < rows.length; ii++) {
								$('#gridLevel').datagrid('beginEdit', ii);
							}
							$('#errMsgGroupDlg').html('Bạn chưa nhập dữ liệu sản phẩm cho mức').show();
						}
					}
					
					params.listPercent = listPercent;
					params.listMaxQuantity = listMaxQuantity;
					params.listMaxAmount = listMaxAmount;
					params.listOrder = listOrder;
					params.listProductDetail = listDetail;
					params.listLevelId = listLevelId;
					
					$.messager.confirm("Xác nhận", "Bạn có muốn cập nhập mức cho nhóm?", function(r) {
						if(r) {
							VTUtilJS.postFormJson(params, '/promotion/update-group-free', function(data) {
								if(data.error) {
									for(var ii = 0; ii < rows.length; ii++) {
										$('#gridLevel').datagrid('beginEdit', ii);
									}
									$('#errMsgGroupDlg').html(data.errMsg).show();
								} else {
									$('#popup-add-level').dialog('close');
								}
							});
						} else {
							for(var ii = 0; ii < rows.length; ii++) {
	    						$('#gridLevel').datagrid('beginEdit', ii);
	    					}
						}
					});
					
				});
			},
			onClose: function() {
				PromotionProgram._levelValueType = null;
				$("#popup-add-level").dialog("destroy");
				$("#popup-container").remove();
			}
		});
	},
	
	popupEditGroupMua : function(productGroupId, groupCode, groupName, minQuantity, minAmount, multiple, recursive, stt) {
		var textDisQuantity = '';
		var textDisAmount = '';
		if($('#promotionType').val() == 'ZV01' || $('#promotionType').val() == 'ZV02' || $('#promotionType').val() == 'ZV03' || 
				$('#promotionType').val() == 'ZV07' || $('#promotionType').val() == 'ZV08' || $('#promotionType').val() == 'ZV09' ||
				$('#promotionType').val() == 'ZV13' || $('#promotionType').val() == 'ZV14' || $('#promotionType').val() == 'ZV15') {
			textDisQuantity = '';
			textDisAmount = 'disabled="disabled"';
		} else {
			textDisQuantity = 'disabled="disabled"';
			textDisAmount = '';
		}
		var html = '<div id="popup-container"><div id="popup-edit-group-mua" class="PopupContentMid">\
			<div class="GeneralForm Search1Form" id="form-data">\
			<label style="width: 100px;" class="LabelStyle Label1Style">Mã nhóm<span class="ReqiureStyle">(*)</span></label>\
			<input type="text" call-back="VTValidateUtils.getMessageOfRequireCheck(\'groupCode\', \'Mã Nhóm\', false, true);VTValidateUtils.getMessageOfSpecialCharactersValidate(\'groupCode\', \'Mã Nhóm\', Utils._CODE)" class="InputTextStyle InputText1Style" style="width: 145px;" maxlength="50" disabled="disabled" id="groupCode" value="'+Utils.XSSEncode(groupCode)+'"> \
			<label style="width: 100px;" class="LabelStyle Label1Style">Tên nhóm<span class="ReqiureStyle">(*)</span></label> \
			<input type="text" class="InputTextStyle InputText1Style" call-back="VTValidateUtils.getMessageOfRequireCheck(\'promotionName\', \'Tên Nhóm\', false, true);VTValidateUtils.getMessageOfSpecialCharactersValidate(\'groupName\', \'Tên Nhóm\', Utils._NAME)" style="width: 145px;" maxlength="100" id="groupName" value="'+Utils.XSSEncode(groupName)+'">\
			<div class="Clear"></div>';
			html += '<div class="Clear"></div>';
			if($('#promotionType').val() != 'ZV01' && $('#promotionType').val() != 'ZV04' && $('#promotionType').val() != 'ZV07' && $('#promotionType').val() != 'ZV10' && $('#promotionType').val() != 'ZV19') {
				html +=	'<label class="LabelStyle" style="width:100px;">Bội số</label><input type="checkbox" id="multiple" '+(multiple==1?'checked="checked"':'')+' class="InputCbxStyle"/>\
				<label class="LabelStyle" style="width:131px;">Tối ưu</label><input type="checkbox" id="recursive" '+(recursive==1?'checked="checked"':'')+' class="InputCbxStyle"/>';
			} else {
				html +=	'<label class="LabelStyle" style="width:100px; visibility: hidden;">Bội số</label><input style="visibility: hidden;" type="checkbox" id="multiple" '+(multiple==1?'checked="checked"':'')+' class="InputCbxStyle"/>\
				<label class="LabelStyle" style="width:131px; visibility: hidden;">Tối ưu</label><input style="visibility: hidden;" type="checkbox" id="recursive" '+(recursive==1?'checked="checked"':'')+' class="InputCbxStyle"/>';
			}
			html += '<label class="LabelStyle Label1Style">Thứ tự ưu tiên</label> \
			<input type="text" class="InputTextStyle InputText1Style" style="width: 145px;" maxlength="3" id="stt" value="'+stt+'">\
			<input type="hidden" id="groupId" value="'+productGroupId+'">\
			<div class="Clear"></div>\
			<div class="BtnCenterSection">\
			<button class="BtnGeneralStyle" id="btn-update">Cập nhật</button>\
			</div>\
			<p class="ErrorMsgStyle SpriteErr" style="display: none;" id="errMsgGroupDlg"></p>\
			<p class="SuccessMsgStyle" style="display: none;" id="successMsgGroupDlg"></p></div></div></div>';
		$("body").append(html);
		Utils.bindFormatOnTextfield('stt', Utils._TF_NUMBER);
		$('#popup-edit-group-mua').dialog({
			title: 'Thông tin nhóm mua',
			width: 550,
			heigth: "auto",
			modal: true, close: false, cache: false,
			onOpen: function() {
				VTUtilJS.bindFormatOnTextfield('minQuantity', VTUtilJS._TF_NUMBER);
				VTUtilJS.formatCurrencyFor('minQuantity');
				VTUtilJS.bindFormatOnTextfield('minAmount', VTUtilJS._TF_NUMBER);
				VTUtilJS.formatCurrencyFor('minAmount');
				$('#btn-update').bind('click', function() {
					var params = VTUtilJS.getFormData('form-data');
					if($('#errorMsg').html().trim().length > 0 && $('#errorMsg').is(":visible")) {
						$('#errMsgGroupDlg').html($('#errorMsg').html()).show();
						$('#errorMsg').html('').hide();
					}
					if(params == null) {
						return;
					}
					$('.SuccessMsgStyle').hide();
					$('#errMsgGroupDlg').html('').hide();
					if(VTUtilJS.isNullOrEmpty(params.groupCode)) {
						$('#errMsgGroupDlg').html('Mã nhóm chưa được khai nhập').show();
						return;
					}
					if(VTUtilJS.isNullOrEmpty(params.groupName)) {
						$('#errMsgGroupDlg').html('Tên nhóm chưa được khai nhập').show();
						return;
					}
					params.promotionId = $('#id').val();
					$.messager.confirm("Xác nhận", "Bạn có muốn cập nhật nhóm sản phẩm mua?", function(r) {
						if(r) {
							VTUtilJS.postFormJson(params, '/promotion/add-group-sale', function(data) {
								if(data.error) {
									$('#errMsgGroupDlg').html(data.errMsg).show();
								} else {
									$('#popup-edit-group-mua').dialog('destroy');
									$('#popup-container').remove();
									$('#groupMuaGrid').datagrid('reload');
								}
							});
						}
					});
				});
			},
			onClose: function() {
				$("#popup-edit-group-mua").dialog("destroy");
				$("#popup-container").remove();
			}
		});
	},
	
	popupEditGroupKM : function(productGroupId, groupCode, groupName, maxQuantity, maxAmount) {
		var html = '<div id="popup-container"><div id="popup-edit-group-mua" class="PopupContentMid">\
			<div class="GeneralForm Search1Form" id="form-data">\
			<label style="width: 100px;" class="LabelStyle Label1Style">Mã nhóm<span class="ReqiureStyle">(*)</span></label>\
			<input type="text" call-back="VTValidateUtils.getMessageOfRequireCheck(\'groupCode\', \'Mã Nhóm\', false, true);VTValidateUtils.getMessageOfSpecialCharactersValidate(\'groupCode\', \'Mã Nhóm\', Utils._CODE)" disabled="disabled" class="InputTextStyle InputText1Style" style="width: 145px;" maxlength="50" id="groupCode" value="'+Utils.XSSEncode(groupCode)+'"> \
			<label style="width: 100px;" class="LabelStyle Label1Style">Tên nhóm<span class="ReqiureStyle">(*)</span></label> \
			<input type="text" call-back="VTValidateUtils.getMessageOfRequireCheck(\'promotionName\', \'Tên Nhóm\', false, true);VTValidateUtils.getMessageOfSpecialCharactersValidate(\'groupName\', \'Tên Nhóm\', Utils._NAME)" class="InputTextStyle InputText1Style" style="width: 145px;" maxlength="100" id="groupName" value="'+Utils.XSSEncode(groupName)+'">\
			<div class="Clear"></div>';
			/*html += '<label style="width: 100px; visibility: hidden;" class="LabelStyle Label1Style">Số lượng tối đa</label> \
			<input type="text" class="InputTextStyle InputText1Style" style="width: 145px; visibility: hidden;" maxlength="20" id="maxQuantity" value="'+VTUtilJS.formatCurrency(maxQuantity)+'"> \
			<label style="width: 100px; visibility: hidden;" class="LabelStyle Label1Style">Số tiền tối đa</label> \
			<input type="text" class="InputTextStyle InputText1Style" style="width: 145px; visibility: hidden;" maxlength="20" id="maxAmount" value="'+VTUtilJS.formatCurrency(maxAmount)+'">\
			<input type="hidden" id="groupId" value="'+productGroupId+'">';*/
			html += '<div class="Clear"></div>\
			<div class="BtnCenterSection">\
			<button class="BtnGeneralStyle" id="btn-update">Cập nhật</button>\
			</div>\
			<p class="ErrorMsgStyle SpriteErr" style="display: none;" id="errMsgGroupDlg"></p>\
			<p class="SuccessMsgStyle" style="display: none;" id="successMsgGroupDlg"></p></div></div></div>';
		$("body").append(html);
		$('#popup-edit-group-mua').dialog({
			title: 'Thông tin nhóm KM',
			width: 550,
			heigth: "auto",
			modal: true, close: false, cache: false,
			onOpen: function() {
				VTUtilJS.bindFormatOnTextfield('maxQuantity', VTUtilJS._TF_NUMBER);
				VTUtilJS.formatCurrencyFor('maxQuantity');
				VTUtilJS.bindFormatOnTextfield('maxAmount', VTUtilJS._TF_NUMBER);
				VTUtilJS.formatCurrencyFor('maxAmount');
				$('#btn-update').bind('click', function() {
					var params = VTUtilJS.getFormData('form-data');
					if($('#errorMsg').html().trim().length > 0 && $('#errorMsg').is(":visible")) {
						$('#errMsgGroupDlg').html($('#errorMsg').html()).show();
						$('#errorMsg').html('').hide();
					}
					if(params == null) {
						return;
					}
					$('.SuccessMsgStyle').hide();
					$('#errMsgGroupDlg').html('').hide();
					if(VTUtilJS.isNullOrEmpty(params.groupCode)) {
						$('#errMsgGroupDlg').html('Mã nhóm chưa được khai nhập').show();
						return;
					}
					if(VTUtilJS.isNullOrEmpty(params.groupName)) {
						$('#errMsgGroupDlg').html('Tên nhóm chưa được khai nhập').show();
						return;
					}
					params.promotionId = $('#id').val();
					$.messager.confirm("Xác nhận", "Bạn có muốn cập nhật nhóm sản phẩm khuyến mại?", function(r) {
						if(r) {
							VTUtilJS.postFormJson(params, '/promotion/add-group-free', function(data) {
								if(data.error) {
									$('#errMsgGroupDlg').html(data.errMsg).show();
								} else {
									$('#popup-edit-group-mua').dialog('destroy');
									$('#popup-container').remove();
									$('#groupKMGrid').datagrid('reload');
								}
							});
						}
					});
				});
			},
			onClose: function() {
				$("#popup-edit-group-mua").dialog("destroy");
				$("#popup-container").remove();
			}
		});
	},
	
	popupEditGroupNew : function(groupMuaId, groupKMId, groupCode, groupName, stt, multiple, recursive) {
		var textDisQuantity = '';
		var textDisAmount = '';
		var html = '<div id="popup-container"><div id="popup-edit-group-mua" class="PopupContentMid">\
			<div class="GeneralForm Search1Form" id="form-data">\
			<label style="width: 100px;" class="LabelStyle Label1Style">Mã nhóm<span class="ReqiureStyle">(*)</span></label>\
			<input type="text" call-back="VTValidateUtils.getMessageOfRequireCheck(\'groupCode\', \'Mã Nhóm\', false, true);VTValidateUtils.getMessageOfSpecialCharactersValidate(\'groupCode\', \'Mã Nhóm\', Utils._CODE)" class="InputTextStyle InputText1Style" style="width: 145px;" maxlength="50" disabled="disabled" id="groupCode" value="'+Utils.XSSEncode(groupCode)+'"> \
			<label style="width: 100px;" class="LabelStyle Label1Style">Tên nhóm<span class="ReqiureStyle">(*)</span></label> \
			<input type="text" class="InputTextStyle InputText1Style" call-back="VTValidateUtils.getMessageOfRequireCheck(\'promotionName\', \'Tên Nhóm\', false, true);VTValidateUtils.getMessageOfSpecialCharactersValidate(\'groupName\', \'Tên Nhóm\', Utils._NAME)" style="width: 145px;" maxlength="100" id="groupName" value="'+Utils.XSSEncode(groupName)+'">\
			<div class="Clear"></div>';
			if($('#promotionType').val() != 'ZV01' && $('#promotionType').val() != 'ZV04' && $('#promotionType').val() != 'ZV07' && $('#promotionType').val() != 'ZV10' && $('#promotionType').val() != 'ZV19') {
				if( $('#promotionType').val() == 'ZV13' || $('#promotionType').val() == 'ZV16') {
					html +=	'<label class="LabelStyle" style="width:100px;">Bội số</label><input type="checkbox" id="multiple" '+(multiple==1?'checked="checked"':'')+' class="InputCbxStyle"/>\
					<label class="LabelStyle" style="width:128px;"></label>';
				} else {
					html +=	'<label class="LabelStyle" style="width:100px;">Bội số</label><input type="checkbox" id="multiple" '+(multiple==1?'checked="checked"':'')+' class="InputCbxStyle"/>\
					<label class="LabelStyle" style="width:111px;">Tối ưu</label><input type="checkbox" id="recursive" '+(recursive==1?'checked="checked"':'')+' class="InputCbxStyle"/>';
				}
			}
			html += '<label style="width: 100px;" class="LabelStyle Label1Style">Thứ tự ưu tiên</label> \
			<input type="text" class="InputTextStyle InputText1Style" style="width: 145px;" maxlength="3" id="stt" value="'+stt+'">\
			<input type="hidden" id="groupMuaId" value="'+groupMuaId+'">\
			<input type="hidden" id="groupKMId" value="'+groupKMId+'">\
			<div class="Clear"></div>\
			<div class="BtnCenterSection">';
			if($('#statusHidden').val() == 2) {
				html += '<button class="BtnGeneralStyle" id="btn-update">Cập nhật</button>';
			}
			html += '</div>\
			<p class="ErrorMsgStyle SpriteErr" style="display: none;" id="errMsgGroupDlg"></p>\
			<p class="SuccessMsgStyle" style="display: none;" id="successMsgGroupDlg"></p></div></div></div>';
		$("body").append(html);
		Utils.bindFormatOnTextfield('stt', Utils._TF_NUMBER);
		$('#popup-edit-group-mua').dialog({
			title: 'Thông tin nhóm',
			width: 550,
			heigth: "auto",
			modal: true, close: false, cache: false,
			onOpen: function() {
				VTUtilJS.bindFormatOnTextfield('minQuantity', VTUtilJS._TF_NUMBER);
				VTUtilJS.formatCurrencyFor('minQuantity');
				VTUtilJS.bindFormatOnTextfield('minAmount', VTUtilJS._TF_NUMBER);
				VTUtilJS.formatCurrencyFor('minAmount');
				if($('#statusHidden').val() == 2) {
					$('#btn-update').bind('click', function() {
						var params = VTUtilJS.getFormData('form-data');
						if($('#errorMsg').html().trim().length > 0 && $('#errorMsg').is(":visible")) {
							$('#errMsgGroupDlg').html($('#errorMsg').html()).show();
							$('#errorMsg').html('').hide();
						}
						if(params == null) {
							return;
						}
						$('.SuccessMsgStyle').hide();
						$('#errMsgGroupDlg').html('').hide();
						if(VTUtilJS.isNullOrEmpty(params.groupCode)) {
							$('#errMsgGroupDlg').html('Mã nhóm chưa được khai nhập').show();
							return;
						}
						if(VTUtilJS.isNullOrEmpty(params.groupName)) {
							$('#errMsgGroupDlg').html('Tên nhóm chưa được khai nhập').show();
							return;
						}
						params.promotionId = $('#id').val();
						$.messager.confirm("Xác nhận", "Bạn có muốn cập nhật nhóm?", function(r) {
							if(r) {
								VTUtilJS.postFormJson(params, '/promotion/add-group-new', function(data) {
									if(data.error) {
										$('#errMsgGroupDlg').html(data.errMsg).show();
									} else {
										$('#popup-edit-group-mua').dialog('destroy');
										$('#popup-container').remove();
										$('#groupGrid').datagrid('reload');
									}
								});
							}
						});
					});
				}
			},
			onClose: function() {
				$("#popup-edit-group-mua").dialog("destroy");
				$("#popup-container").remove();
			}
		});
	},
	
	popupAddGroupMua : function() {
		var textDisQuantity = '';
		var textDisAmount = '';
		if($('#promotionType').val() == 'ZV01' || $('#promotionType').val() == 'ZV02' || $('#promotionType').val() == 'ZV03' || 
				$('#promotionType').val() == 'ZV07' || $('#promotionType').val() == 'ZV08' || $('#promotionType').val() == 'ZV09' ||
				$('#promotionType').val() == 'ZV13' || $('#promotionType').val() == 'ZV14' || $('#promotionType').val() == 'ZV15') {
			textDisQuantity = '';
			textDisAmount = 'disabled="disabled"';
		} else {
			textDisQuantity = 'disabled="disabled"';
			textDisAmount = '';
		}
		var html = '<div id="popup-container"><div id="popup-add-group-mua" class="PopupContentMid">\
			<div class="GeneralForm Search1Form" id="form-data">\
			<label style="width: 100px;" class="LabelStyle Label1Style">Mã nhóm<span class="ReqiureStyle">(*)</span></label>\
			<input type="text" call-back="VTValidateUtils.getMessageOfSpecialCharactersValidate(\'groupCode\', \'Mã nhóm\', Utils._CODE)" class="InputTextStyle InputText1Style" style="width: 145px;" maxlength="50" id="groupCode"> \
			<label style="width: 100px;" class="LabelStyle Label1Style">Tên nhóm<span class="ReqiureStyle">(*)</span></label> \
			<input type="text" class="InputTextStyle InputText1Style" style="width: 145px;" maxlength="100" id="groupName">\
			<div class="Clear"></div>';
			html += '<div class="Clear"></div>';
			if($('#promotionType').val() != 'ZV01' && $('#promotionType').val() != 'ZV04' && $('#promotionType').val() != 'ZV07' && $('#promotionType').val() != 'ZV10' && $('#promotionType').val() != 'ZV19') {
				html +=	'<label class="LabelStyle" style="width:100px;">Bội số</label><input type="checkbox" id="multiple" class="InputCbxStyle"/>\
				<label class="LabelStyle" style="width:131px;">Tối ưu</label><input type="checkbox" id="recursive" class="InputCbxStyle"/>';
			} else {
				html +=	'<label class="LabelStyle" style="width:100px; visibility: hidden;">Bội số</label><input style="visibility: hidden;" type="checkbox" id="multiple" class="InputCbxStyle"/>\
				<label class="LabelStyle" style="width:131px; visibility: hidden;">Tối ưu</label><input style="visibility: hidden;" type="checkbox" id="recursive" class="InputCbxStyle"/>';
			}
			html += '<label class="LabelStyle Label1Style">Thứ tự ưu tiên</label> \
			<input type="text" class="InputTextStyle InputText1Style" style="width: 145px;" maxlength="3" id="stt">\
			<div class="Clear"></div>\
			<div class="BtnCenterSection">\
			<button class="BtnGeneralStyle" id="btn-update">Cập nhật</button>\
			</div>\
			<p class="ErrorMsgStyle SpriteErr" style="display: none;" id="errMsgGroupDlg"></p>\
			<p class="SuccessMsgStyle" style="display: none;" id="successMsgGroupDlg"></p></div></div></div>';
		$("body").append(html);
		Utils.bindFormatOnTextfield('stt', Utils._TF_NUMBER);
		$('#popup-add-group-mua').dialog({
			title: 'Thông tin nhóm mua',
			width: 550,
			heigth: "auto",
			modal: true, close: false, cache: false,
			onOpen: function() {
				VTUtilJS.bindFormatOnTextfield('minQuantity', VTUtilJS._TF_NUMBER);
				VTUtilJS.formatCurrencyFor('minQuantity');
				VTUtilJS.bindFormatOnTextfield('minAmount', VTUtilJS._TF_NUMBER);
				VTUtilJS.formatCurrencyFor('minAmount');
				$('#btn-update').bind('click', function() {
					var params = VTUtilJS.getFormData('form-data');
					if($('#errorMsg').html().trim().length > 0 && $('#errorMsg').is(":visible")) {
						$('#errMsgGroupDlg').html($('#errorMsg').html()).show();
						$('#errorMsg').html('').hide();
					}
					if(params == null) {
						return;
					}
					$('.SuccessMsgStyle').hide();
					$('#errMsgGroupDlg').html('').hide();
					if(VTUtilJS.isNullOrEmpty(params.groupCode)) {
						$('#errMsgGroupDlg').html('Mã nhóm chưa được khai nhập').show();
						return;
					}
					if(VTUtilJS.isNullOrEmpty(params.groupName)) {
						$('#errMsgGroupDlg').html('Tên nhóm chưa được khai nhập').show();
						return;
					}
					params.promotionId = $('#id').val();
					$.messager.confirm("Xác nhận", "Bạn có muốn thêm nhóm sản phẩm mua?", function(r) {
						if(r) {
							VTUtilJS.postFormJson(params, '/promotion/add-group-sale', function(data) {
								if(data.error) {
									$('#errMsgGroupDlg').html(data.errMsg).show();
								} else {
									$('#popup-add-group-mua').dialog('destroy');
									$('#popup-container').remove();
									$('#groupMuaGrid').datagrid('reload');
								}
							});
						}
					});
				});
			},
			onClose: function() {
				$("#popup-add-group-mua").dialog("destroy");
				$("#popup-container").remove();
			}
		});
	},
	
	popupAddGroupKM : function(promotionId) {
		var html = '<div id="popup-container"><div id="popup-add-group-mua" class="PopupContentMid">\
			<div class="GeneralForm Search1Form" id="form-data">\
			<label style="width: 100px;" class="LabelStyle Label1Style">Mã nhóm<span class="ReqiureStyle">(*)</span></label>\
			<input type="text" call-back="VTValidateUtils.getMessageOfSpecialCharactersValidate(\'groupCode\', \'Mã nhóm\', Utils._CODE)" class="InputTextStyle InputText1Style" style="width: 145px;" maxlength="50" id="groupCode"> \
			<label style="width: 100px;" class="LabelStyle Label1Style">Tên nhóm<span class="ReqiureStyle">(*)</span></label> \
			<input type="text" class="InputTextStyle InputText1Style" style="width: 145px;" maxlength="100" id="groupName">\
			<div class="Clear"></div>';
			/*html += '<label style="width: 100px; visibility: hidden;" class="LabelStyle Label1Style">Số lượng tối đa</label> \
			<input type="text" class="InputTextStyle InputText1Style" style="width: 145px; visibility: hidden;" maxlength="20" id="maxQuantity"> \
			<label style="width: 100px; visibility: hidden;" class="LabelStyle Label1Style">Số tiền tối đa</label> \
			<input type="text" class="InputTextStyle InputText1Style" style="width: 145px; visibility: hidden;" maxlength="20" id="maxAmount">';*/
			html += '<div class="Clear"></div>\
			<div class="BtnCenterSection">\
			<button class="BtnGeneralStyle" id="btn-update">Cập nhật</button>\
			</div>\
			<p class="ErrorMsgStyle SpriteErr" style="display: none;" id="errMsgGroupDlg"></p>\
			<p class="SuccessMsgStyle" style="display: none;" id="successMsgGroupDlg"></p></div></div></div>';
		$("body").append(html);
		$('#popup-add-group-mua').dialog({
			title: 'Thông tin nhóm khuyến mại',
			width: 550,
			heigth: "auto",
			modal: true, close: false, cache: false,
			onOpen: function() {
				VTUtilJS.bindFormatOnTextfield('maxQuantity', VTUtilJS._TF_NUMBER);
				VTUtilJS.formatCurrencyFor('maxQuantity');
				VTUtilJS.bindFormatOnTextfield('maxAmount', VTUtilJS._TF_NUMBER);
				VTUtilJS.formatCurrencyFor('maxAmount');
				$('#btn-update').bind('click', function() {
					var params = VTUtilJS.getFormData('form-data');
					if($('#errorMsg').html().trim().length > 0 && $('#errorMsg').is(":visible")) {
						$('#errMsgGroupDlg').html($('#errorMsg').html()).show();
						$('#errorMsg').html('').hide();
					}
					if(params == null) {
						return;
					}
					$('.SuccessMsgStyle').hide();
					$('#errMsgGroupDlg').html('').hide();
					if(VTUtilJS.isNullOrEmpty(params.groupCode)) {
						$('#errMsgGroupDlg').html('Mã nhóm chưa được khai nhập').show();
						return;
					}
					if(VTUtilJS.isNullOrEmpty(params.groupName)) {
						$('#errMsgGroupDlg').html('Tên nhóm chưa được khai nhập').show();
						return;
					}
					params.promotionId = $('#id').val();
					$.messager.confirm("Xác nhận", "Bạn có muốn thêm nhóm sản phẩm khuyến mại?", function(r) {
						if(r) {
							VTUtilJS.postFormJson(params, '/promotion/add-group-free', function(data) {
								if(data.error) {
									$('#errMsgGroupDlg').html(data.errMsg).show();
								} else {
									$('#popup-add-group-mua').dialog('destroy');
									$('#popup-container').remove();
									$('#groupKMGrid').datagrid('reload');
								}
							});
						}
					});
				});
			},
			onClose: function() {
				$("#popup-add-group-mua").dialog("destroy");
				$("#popup-container").remove();
			}
		});
	},
	
	popupAddGroupNew: function() {
		$('.ErrorMsgStyle').hide();
		if ($("#typeCode").length == 0) {
			return;
		}
		var typeCodeTmp = $("#typeCode").val().trim();
		var lstZVMultGroup = [ "ZV01", "ZV02", "ZV03", "ZV04", "ZV05", "ZV06", "ZV09", "ZV21" ];
		if (lstZVMultGroup.indexOf(typeCodeTmp) < 0 && $("#groupGridContainer .datagrid-row td[field=delete] a").length > 0) {
			$("#groupErrMsg").html("Chỉ được thêm nhiều nhóm cho các ZV " + lstZVMultGroup.join(", ")).show();
			return;
		}
		var textDisQuantity = '';
		var textDisAmount = '';
		var html = '<div id="popup-container"><div id="popup-add-group-mua" class="PopupContentMid">\
			<div class="GeneralForm Search1Form" id="form-data">\
			<label style="width: 100px;" class="LabelStyle Label1Style">Mã nhóm<span class="ReqiureStyle">(*)</span></label>\
			<input type="text" call-back="VTValidateUtils.getMessageOfSpecialCharactersValidate(\'groupCode\', \'Mã nhóm\', Utils._CODE)" class="InputTextStyle InputText1Style" style="width: 145px;" maxlength="50" id="groupCode"> \
			<label style="width: 100px;" class="LabelStyle Label1Style">Tên nhóm<span class="ReqiureStyle">(*)</span></label> \
			<input type="text" call-back="Utils.getMessageOfSpecialCharactersValidate(\'groupName\', \'Tên CTKM\', Utils._SPECIAL)" class="InputTextStyle InputText1Style" style="width: 145px;" maxlength="100" id="groupName">\
			<div class="Clear"></div>';
			if($('#promotionType').val() != 'ZV01' && $('#promotionType').val() != 'ZV04' && $('#promotionType').val() != 'ZV07' && $('#promotionType').val() != 'ZV10' && $('#promotionType').val() != 'ZV19') {
				if( $('#promotionType').val() == 'ZV13' || $('#promotionType').val() == 'ZV16') {
					html +=	'<label class="LabelStyle" style="width:100px;">Bội số</label><input type="checkbox" id="multiple" class="InputCbxStyle"/>\
					<label class="LabelStyle" style="width:128px;"></label>';
				} else {
					html +=	'<label class="LabelStyle" style="width:100px;">Bội số</label><input type="checkbox" id="multiple" class="InputCbxStyle"/>\
					<label class="LabelStyle" style="width:111px;">Tối ưu</label><input type="checkbox" id="recursive" class="InputCbxStyle"/>';
				}
			}
			html += '<label class="LabelStyle Label1Style" style="width: 100px;">Thứ tự ưu tiên</label> \
			<input type="text" class="InputTextStyle InputText1Style" style="width: 145px;" maxlength="3" id="stt" call-back="VTValidateUtils.getMessageOfInvaildInteger(\'stt\', \'Thứ tự ưu tiên\');VTValidateUtils.getMessageOfNegativeNumberCheck(\'stt\', \'Thứ tự ưu tiên\');">\
			<div class="Clear"></div>\
			<div class="BtnCenterSection">\
			<button class="BtnGeneralStyle" id="btn-update">Cập nhật</button>\
			</div>\
			<p class="ErrorMsgStyle SpriteErr" style="display: none;" id="errMsgGroupDlg"></p>\
			<p class="SuccessMsgStyle" style="display: none;" id="successMsgGroupDlg"></p></div></div></div>';
		$("body").append(html);
		Utils.bindFormatOnTextfield('stt', Utils._TF_NUMBER);
		$('#popup-add-group-mua').dialog({
			title: 'Thông tin nhóm',
			width: 550,
			heigth: "auto",
			modal: true, close: false, cache: false,
			onOpen: function() {
				VTUtilJS.bindFormatOnTextfield('minQuantity', VTUtilJS._TF_NUMBER);
				VTUtilJS.formatCurrencyFor('minQuantity');
				VTUtilJS.bindFormatOnTextfield('minAmount', VTUtilJS._TF_NUMBER);
				VTUtilJS.formatCurrencyFor('minAmount');
				$('#btn-update').bind('click', function() {
					var params = VTUtilJS.getFormData('form-data');
					if($('#errorMsg').html().trim().length > 0 && $('#errorMsg').is(":visible")) {
						$('#errMsgGroupDlg').html($('#errorMsg').html()).show();
						$('#errorMsg').html('').hide();
					}
					if(params == null) {
						return;
					}
					$('.SuccessMsgStyle').hide();
					$('#errMsgGroupDlg').html('').hide();
					if(VTUtilJS.isNullOrEmpty(params.groupCode)) {
						$('#errMsgGroupDlg').html('Mã nhóm chưa được khai nhập').show();
						return;
					}
					if(VTUtilJS.isNullOrEmpty(params.groupName)) {
						$('#errMsgGroupDlg').html('Tên nhóm chưa được khai nhập').show();
						return;
					}
					params.promotionId = $('#id').val();
					$.messager.confirm("Xác nhận", "Bạn có muốn thêm nhóm sản phẩm mua?", function(r) {
						if(r) {
							VTUtilJS.postFormJson(params, '/promotion/add-group-new', function(data) {
								if(data.error) {
									$('#errMsgGroupDlg').html(data.errMsg).show();
								} else {
									$('#popup-add-group-mua').dialog('destroy');
									$('#popup-container').remove();
									$('#groupGrid').datagrid('reload');
								}
							});
						}
					});
				});
			},
			onClose: function() {
				$("#popup-add-group-mua").dialog("destroy");
				$("#popup-container").remove();
			}
		});
	},
	
	deleteProductGroup : function(gridDiv, productGroupId) {
		var params = {groupId : productGroupId};
		$.messager.confirm("Xác nhận", "Bạn có muốn xóa nhóm?", function(r) {
			if(r) {
				VTUtilJS.postFormJson(params, '/promotion/delete-group', function(data) {
					if(data.error) {
						VTUtilJS.showMessageBanner(true, data.errMsg);
					} else {
						VTUtilJS.showMessageBanner(false, 'Xóa dữ liệu thành công');
						$('#'+gridDiv).datagrid('reload');
					}
				});
			}
		});
	},
	
	deleteProductGroupNew : function(gridDiv, groupMuaId, groupKMId) {
		$('.ErrorMsgStyle').hide();
		var params = {groupMuaId : groupMuaId, groupKMId : groupKMId};
		$.messager.confirm("Xác nhận", "Bạn có muốn xóa nhóm?", function(r) {
			if(r) {
				VTUtilJS.postFormJson(params, '/promotion/delete-group-new', function(data) {
					if(data.error) {
						VTUtilJS.showMessageBanner(true, data.errMsg);
					} else {
						VTUtilJS.showMessageBanner(false, 'Xóa dữ liệu thành công');
						$('#'+gridDiv).datagrid('reload');
						if ($("#typeCode").length == 0) {
							return;
						}
					}
				});
			}
		});
	},
	
	distributeLevel : function() {
		$('#errorMsgDistribute').html('').hide();
		var cbGroupMuaId = $('#cbGroupMua').val();
		var cbGroupKMId = $('#cbGroupKM').val();
		VTUtilJS.getFormJson({groupMuaId : cbGroupMuaId, groupKMId : cbGroupKMId}, '/promotion/list-level-of-group', function(data) {
			if(!data.error) {
				var listLevelMua = data.listLevelMua;
				var listLevelKM = data.listLevelKM;
				if(!$.isArray(listLevelMua) || !$.isArray(listLevelKM) || listLevelMua.length != listLevelKM.length) {
					$('#errorMsgDistribute').html('Mức của nhóm mua và nhóm khuyến mại chưa phù hợp. không thể tự động phân bổ được').show();
					$('#mappingGrid').datagrid('loadData', []);
					return;
				}
				var listDistribution = new Array();
				for(var i = 0; i < listLevelMua.length; i++) {
					var levelMua = listLevelMua[i];
					var levelKM = listLevelKM[i];
					var groupMua = PromotionProgram._mapGroupMua.get(cbGroupMuaId);
					var groupKM = PromotionProgram._mapGroupKM.get(cbGroupKMId);
					var obj = new Object();
					obj.idLevelMua = groupMua.id;
					obj.groupMuaCode = groupMua.productGroupCode;
					obj.groupMuaName = groupMua.productGroupName;
					obj.groupMuaText = levelMua.groupText;
					obj.orderLevelMua = levelMua.orderNumber;
					obj.minQuantityMua = levelMua.minQuantity;
					obj.minAmountMua = levelMua.minAmount;
					
					obj.idLevelKM = groupKM.id;
					obj.groupKMCode = groupKM.productGroupCode;
					obj.groupKMName = groupKM.productGroupName;
					obj.groupKMText = levelKM.groupText;
					obj.orderLevelKM = levelKM.orderNumber;
					obj.maxQuantityKM = levelKM.maxQuantity;
					obj.maxAmountKM = levelKM.maxAmount;
					obj.percentKM = levelKM.promotionPercent;
					
					listDistribution.push(obj);
				}
				
				$('#mappingGrid').datagrid('loadData', listDistribution);
			}
		});
	},
	
	deleteMapping : function(idx) {
		$('#errorMsg').html('').hide();
		$('#mappingGrid').datagrid('acceptChanges');
		var row = $('#mappingGrid').datagrid('getRows')[idx];
		var mappingId = row.mappingId;
		var groupMuaCode = row.groupMuaCode;
		var orderLevelMua = row.orderLevelMua;
		var groupKMCode = row.groupKMCode;
		var orderLevelKM = row.orderLevelKM;
		var id = $('#id').val();
		var params = new Object();
		if(!VTUtilJS.isNullOrEmpty(mappingId)) {
			params.mappingId = mappingId;
		}
		if(!VTUtilJS.isNullOrEmpty(groupMuaCode)) {
			params.groupMuaCode = groupMuaCode;
		}
		if(!VTUtilJS.isNullOrEmpty(orderLevelMua)) {
			params.orderLevelMua = orderLevelMua;
		}
		if(!VTUtilJS.isNullOrEmpty(groupKMCode)) {
			params.groupKMCode = groupKMCode;
		}
		if(!VTUtilJS.isNullOrEmpty(orderLevelKM)) {
			params.orderLevelKM = orderLevelKM;
		}
		params.id = id;
		$.messager.confirm("Xác nhận", "Bạn có muốn xóa cơ cấu?", function(r) {
			if(r) {
				VTUtilJS.postFormJson(params, '/promotion/delete-mapping', function(data) {
					if(data.error) {
						$('#errorMsg').html(data.errMsg).show();;
					} else {
						$('#mappingGrid').datagrid('getRows').splice(idx, 1);
						$('#mappingGrid').datagrid('loadData', $('#mappingGrid').datagrid('getRows'));
					}
				});
			}
		});
	},
	
	saveMapping : function(idx) {
		$('#errorMsg').html('').hide();
		$('#mappingGrid').datagrid('acceptChanges');
		var row = $('#mappingGrid').datagrid('getRows')[idx];
		var groupMuaCode = row.groupMuaCode;
		var orderLevelMua = row.orderLevelMua;
		var groupKMCode = row.groupKMCode;
		var orderLevelKM = row.orderLevelKM;
		var id = $('#id').val();
		var params = new Object();
		if(!VTUtilJS.isNullOrEmpty(groupMuaCode)) {
			params.groupMuaCode = groupMuaCode;
		}
		if(!VTUtilJS.isNullOrEmpty(orderLevelMua)) {
			params.orderLevelMua = orderLevelMua;
		}
		if(!VTUtilJS.isNullOrEmpty(groupKMCode)) {
			params.groupKMCode = groupKMCode;
		}
		if(!VTUtilJS.isNullOrEmpty(orderLevelKM)) {
			params.orderLevelKM = orderLevelKM;
		}
		params.id = id;
		$.messager.confirm("Xác nhận", "Bạn có muốn lưu cơ cấu?", function(r) {
			if(r) {
				VTUtilJS.postFormJson(params, '/promotion/save-mapping', function(data) {
					if(data.error) {
						$('#errorMsg').html(data.errMsg).show();;
					} else {
						var groupMapping = data.groupMapping;
						$('#mappingGrid').datagrid('getRows')[idx] = groupMapping;
						$('#mappingGrid').datagrid('loadData', $('#mappingGrid').datagrid('getRows'));
					}
				});
			}
		});
	},
	
	addMapping : function() {
		$('#errorMsgDistribute').html('').hide();
		var groupMuaId = $('#cbGroupMua').val();
		var groupKMId = $('#cbGroupKM').val();
		var groupMuaCode = PromotionProgram._mapGroupMua.get(groupMuaId).productGroupCode;
		var groupMuaName = PromotionProgram._mapGroupMua.get(groupMuaId).productGroupName;
		var groupKMCode = PromotionProgram._mapGroupKM.get(groupKMId).productGroupCode;
		var groupKMName = PromotionProgram._mapGroupKM.get(groupKMId).productGroupName;
		var listLevelMua = PromotionProgram._mapLevelMua.get(groupMuaCode);
		if(!$.isArray(listLevelMua) || listLevelMua.length == 0) {
			$('#errorMsgDistribute').html('Nhóm ' + Utils.XSSEncode(groupMuaCode) + ' chưa có mức. Vui lòng qua tab [Khai báo nhóm] để thêm mức').show();
			return;
		}
		var orderLevelMua = listLevelMua[0].orderNumber;
		var minQuantityMua = listLevelMua[0].minQuantity;
		var minAmountMua = listLevelMua[0].minAmount;
		
		var listLevelKM = PromotionProgram._mapLevelKM.get(groupKMCode);
		if(!$.isArray(listLevelKM) || listLevelKM.length == 0) {
			$('#errorMsgDistribute').html('Nhóm ' + Utils.XSSEncode(groupKMCode) + ' chưa có mức. Vui lòng qua tab [Khai báo nhóm] để thêm mức').show();
			return;
		}
		var orderLevelKM = listLevelKM[0].orderNumber;
		var maxQuantityKM = listLevelKM[0].maxQuantity;
		var maxAmountKM = listLevelKM[0].maxAmount;
		var percentKM = listLevelKM[0].promotionPercent;
		var obj = new Object();
		obj.groupMuaCode = groupMuaCode;
		obj.groupMuaName = groupMuaName;
		obj.orderLevelMua = orderLevelMua;
		obj.minQuantityMua = minQuantityMua;
		obj.minAmountMua = minAmountMua;
		
		obj.groupKMCode = groupKMCode;
		obj.groupKMName = groupKMName;
		obj.orderLevelKM = orderLevelKM;
		obj.maxQuantityKM = maxQuantityKM;
		obj.maxAmountKM = maxAmountKM;
		obj.percentKM = percentKM;
		
		$('#mappingGrid').datagrid('appendRow', obj);
		$('#mappingGrid').datagrid('beginEdit', $('#mappingGrid').datagrid('getRows').length-1);
	},
	
	searchShop: function() {
		$(".ErrorMsgStyle").hide();
		$("#labelListCustomer").hide();
		$("#promotionCustomerGrid").hide();
		$("#boxSearch1").hide();
		var params = {
				promotionId: $("#masterDataArea #id").val().trim(),
				code: $("#shopCode").val().trim(),
				name: $("#shopName").val().trim(),
				quantity: isNaN($("#quantityMax").val().trim()) ? "" : $("#quantityMax").val().trim()
		};
		PromotionProgram._promotionShopParams = params;
		$("#exGrid").treegrid("reload", params);
	},
	
	searchCustomerShopMap: function(promotionId, shopId, status) {
		$(".ErrorMsgStyle").hide();
		var r = $("#exGrid").treegrid("find", shopId);
		if (r != null) {
			$("#labelShop").html(r.attr.shopName);
		}
		$("#labelListCustomer").show();
		$("#promotionCustomerGrid").show();
		PromotionProgram.shopId = shopId;
		var params = {
				promotionId: promotionId,
				shopId: shopId,
				code: $("#customerCodeSearch").val().trim(),
				name: $("#customerNameSearch").val().trim(),
				address: $("#customerAddressSearch").val().trim()
		};
		$("#promotionCustomerExGrid").datagrid("load", params);
		$("#btnSearchCustomer").attr("onclick", "PromotionProgram.searchCustomerShopMap("+promotionId+","+shopId+","+status+");");
		$('html, body').animate({ scrollTop: $(document).height() }, 1000);
	},
	
	toggleCustomerSearch: function() {
		var b = $("#boxSearch1").is(":hidden");
		if (b) {
			$("#searchCustomerDiv").html("Đóng tìm kiếm &lt;&lt;");
		} else {
			$("#searchCustomerDiv").html("Tìm kiếm &gt;&gt;");
		}
		$("#boxSearch1").toggle();
	},
	
	searchCustomer: function() {
		$(".ErrorMsgStyle").hide();
		PromotionProgram._promotionStaffParams = {};
		PromotionProgram._promotionStaffParams.promotionId = $("#masterDataArea #id").val().trim();
		PromotionProgram._promotionStaffParams.code = $('#customerCode').val().trim();
		PromotionProgram._promotionStaffParams.name = $('#customerName').val().trim();
		PromotionProgram._promotionStaffParams.address = $('#address').val().trim();
		var params = {
				promotionId: $("#masterDataArea #id").val().trim(),
				code: $("#customerCode").val().trim(),
				name: $("#customerName").val().trim(),
				address: $('#address').val().trim()
		};
		PromotionProgram._promotionStaffParams = params;
		$("#exGrid").treegrid("reload", params);
	},
	
	showCustomerDlg: function() {
		if (isNaN(PromotionProgram.shopId)) {
			return;
		}
		$(".ErrorMsgStyle").hide();
		var html = '<div id="add-customerPopupDiv" style="display:none;">\
			<div id="add-customerPopup">\
			<div class="PopupContentMid">\
				<div class="GeneralForm Search1Form">\
					<label class="LabelStyle" style="width:100px;">Mã KH</label>\
					<input id="codeDlg" class="InputTextStyle" style="width:150px;" maxlength="40" />\
					<label class="LabelStyle" style="width:100px;">Tên KH</label>\
					<input id="nameDlg" class="InputTextStyle" style="width:230px;" maxlength="100" />\
					<div class="Clear"></div>\
					<label class="LabelStyle" style="width:100px;">Địa chỉ</label>\
					<input id="addressDlg" class="InputTextStyle" style="width:500px;" maxlength="250" />\
					<div class="Clear"></div>\
					<div class="BtnCenterSection">\
					    <button class="BtnGeneralStyle" id="btnSearchDlg">Tìm kiếm</button>\
					</div>\
					\
					<div class="GridSection" id="gridDlgDiv">\
						<div id="gridDlg"></div>\
					</div>\
					\
					<label class="LabelStyle" style="width:100px;">Số suất</label>\
					<input id="quantityDlg" class="InputTextStyle vinput-money" style="width:150px;" maxlength="11" />\
					<label class="LabelStyle" style="width:100px;">Số tiền</label>\
					<input id="amtDlg" class="InputTextStyle vinput-money" style="width:150px;" maxlength="11" />\
					<label class="LabelStyle" style="width:100px;">Số lượng</label>\
					<input id="numDlg" class="InputTextStyle vinput-money" style="width:150px;" maxlength="11" />\
					<div class="Clear"></div>\
					<p id="erMsgDlg" class="ErrorMsgStyle" style="padding-left:5px;display:none;"></p>\
					<p id="erMsgDlg" class="ErrorMsgStyle" style="padding-left:5px;display:none;"></p>\
					<div class="Clear"></div>\
					<div class="BtnCenterSection">\
					    <button class="BtnGeneralStyle" id="btnChooseDlg">Chọn</button>\
						<button class="BtnGeneralStyle" id="btnCloseDlg" onclick="javascript:$(\'#add-customerPopup\').dialog(\'close\');">Đóng</button>\
					</div>\
				</div>\
			</div>\
			</div>\
		</div>';
		$("body").append(html);
		//Xu ly hien thi kieu tien
		$('.vinput-money').each(function() {
			VTUtilJS.bindFormatOnTextfield($(this), VTUtilJS._TF_NUMBER_COMMA);
			VTUtilJS.formatCurrencyFor($(this));
		});
		
		var r = $("#exGrid").treegrid("find", PromotionProgram.shopId);
		var tt = (r == null) ? "Chọn Khách hàng" : "Chọn Khách hàng (<span style='color:#3a1;'>" + Utils.XSSEncode(r.attr.shopName) + "</span>)";
		var lstTmp = null;
		$("#add-customerPopup").dialog({
			title: tt,
			width: 900,
			heigth: "auto",
			modal: true, close: false, cache: false,
			onOpen: function() {
				$("#add-customerPopup").addClass("easyui-dialog");
				VTUtilJS.bindFormatOnTextfield("quantityDlg", VTUtilJS._TF_NUMBER);
				$("#add-customerPopup #codeDlg").focus();
				
				lstTmp = new CArray();
				$("#add-customerPopup #gridDlg").datagrid({
					url: "/promotion/search-customer-dlg?promotionId="+$("#masterDataArea #id").val().trim()+"&shopId="+Number(PromotionProgram.shopId),
					rownumbers: false,
					width: $("#add-customerPopup #gridDlgDiv").width(),
					height: "auto",
					pagination: true,
					fitColumns: true,
					idField: "customerId",
					scrollbarSize: 0,
					columns: [[
						{field:"no", title:"STT", sortable: false, resizable: false, width:45, fixed:true, align:"center", formatter:function(v, r, i) {
							var p = $("#add-customerPopup #gridDlg").datagrid("options").pageNumber;
							var n = $("#add-customerPopup #gridDlg").datagrid("options").pageSize;
							return (Number(p) - 1) * Number(n) + i + 1;
						}},
						{field:"customerCode", title:"Mã KH", sortable: false, resizable: false, width:75, align:"left", formatter: function(value, row, index) {
							return VTUtilJS.XSSEncode(value);
						}},
						{field:"customerName", title:"Tên KH", sortable: false, resizable: false, width:120, align:"left", formatter: function(value, row, index) {
							return VTUtilJS.XSSEncode(value);
						}},
						{field:"address", title:"Địa chỉ", sortable: false, resizable: false, width:200, align:"left", formatter: function(value, row, index) {
							return VTUtilJS.XSSEncode(value);
						}},
						{field:"ck", title:"", checkbox:true, sortable: false, resizable: false, align:"center"}
					]],
					onLoadSuccess: function(data) {
						$("#add-customerPopup #gridDlg").datagrid("resize");
						setTimeout(function(){
							var hDlg=parseInt($("#add-customerPopup").parent().height());
							var hW=parseInt($(window).height());
							var d=hW-hDlg;
							d=d/2+document.documentElement.scrollTop;
							if (d < 0) { d = 0; }
							$("#add-customerPopup").parent().css('top',d);
			    		},1000);
					},
					onCheck: function(i, r) {
						lstTmp.push(r.customerId);
					},
					onUncheck: function(i, r) {
						lstTmp.remove(r.customerId);
					},
					onCheckAll: function(rows) {
						for (var i = 0, sz = rows.length; i < sz; i++) {
							lstTmp.push(rows[i].customerId);
						}
					},
					onUncheckAll: function(rows) {
						for (var i = 0, sz = rows.length; i < sz; i++) {
							lstTmp.remove(rows[i].customerId);
						}
					}
				});
				
				$("#add-customerPopup #btnSearchDlg").click(function() {
					var p = {
							code: $("#add-customerPopup #codeDlg").val().trim(),
							name: $("#add-customerPopup #nameDlg").val().trim(),
							address: $("#add-customerPopup #addressDlg").val().trim()
					};
					$("#add-customerPopup #gridDlg").datagrid("reload", p);
				});
				$("#add-customerPopup #codeDlg").parent().keyup(function(event) {
					if (event.keyCode == keyCodes.ENTER) {
						$("#add-customerPopup #btnSearchDlg").click();
					}
				});
				$("#add-customerPopup #btnChooseDlg").click(function() {
					if (lstTmp == null || lstTmp.size() == 0) {
						$("#erMsgDlg").html("Không có khách hàng nào được chọn").show();
						return;
					}
					var qtt = $("#quantityDlg").val().trim();
					qtt = qtt.replace(/,/g,'').trim();
					if (qtt.length > 0 && (isNaN(qtt) || Number(qtt) <= 0)) {
						$("#erMsgDlg").html("Số suất phải là số nguyên dương").show();
						return;
					}
					qtt = $("#amtDlg").val().trim();
					qtt = qtt.replace(/,/g,'').trim();
					if (qtt.length > 0 && (isNaN(qtt) || Number(qtt) <= 0)) {
						$("#erMsgDlg").html("Số tiền phải là số nguyên dương").show();
						return;
					}
					qtt = $("#numDlg").val().trim();
					qtt = qtt.replace(/,/g,'').trim();
					if (qtt.length > 0 && (isNaN(qtt) || Number(qtt) <= 0)) {
						$("#erMsgDlg").html("Số lượng phải là số nguyên dương").show();
						return;
					}
					PromotionProgram.addCustomerQtt(lstTmp.toArray());
				});
			},
			onClose: function() {
				$("#add-customerPopup").dialog("destroy");
				$("#add-customerPopupDiv").remove();
			}
		});
	},
	
	addCustomerQtt: function(lstCustId) {
		$(".ErrorMsgStyle").hide();
		$.messager.confirm("Xác nhận", "Bạn có muốn thêm khách hàng?", function(r) {
			if (r) {
				var params = {
						promotionId: $("#masterDataArea #id").val().trim(),
						quantity: $("#quantityDlg").val().trim().replace(/,/g,'').trim(),
						amount: $("#amtDlg").val().trim().replace(/,/g,'').trim(),
						number: $("#numDlg").val().trim().replace(/,/g,'').trim(),
						lstId: lstCustId
				};
				$("#add-customerPopup").dialog("close");
				Utils.saveData(params, "/promotion/add-customer", PromotionProgram._xhrSave, 'errMsgShop', function(data) {
					$("#successMsgShop").html("Lưu dữ liệu thành công").show();
					setTimeout(function() {$("#successMsgShop").hide();},3000);
					$("#exGrid").treegrid("reload");
				}, "loading2");
			}
		});
	},
	
	updateCustomerQuantity: function(nodeId, pcmId) {
		$(".ErrorMsgStyle").hide();
		var qtt = $("#txt"+nodeId).val().trim();
		qtt = qtt.replace(/,/g, "");
		var amt = $("#txtamt"+nodeId).val().trim();
		amt = amt.replace(/,/g, "");
		var num = $("#txtnum"+nodeId).val().trim();
		num = num.replace(/,/g, "");
		var isEdit = $("#ck"+nodeId).is(':checked');
		if (qtt.length > 0 && (isNaN(qtt) || Number(qtt) <= 0)) {//hungtt
			$("#errMsgShop").html("Số suất khách hàng phải là số").show();
			return;
		}
		if (amt.length > 0 && (isNaN(amt) || Number(amt) <= 0)) {
			$("#errMsgShop").html("Số tiền phải là số dương").show();
			return;
		}
		if (num.length > 0 && (isNaN(num) || Number(num) <= 0)) {
			$("#errMsgShop").html("Số lượng phải là số dương").show();
			return;
		}
		$.messager.confirm("Xác nhận", "Bạn có muốn cập nhật số suất, số tiền, số lượng cho khách hàng?", function(r) {
			if (r) {
				var params = {
						promotionId: $("#masterDataArea #id").val().trim(),
						id: pcmId,
						quantity: qtt.length == 0 ? qtt : parseInt(qtt),
						amount: amt.length == 0 ? amt : parseInt(amt),	
						number: num.length == 0 ? num : parseInt(num),
				};
				Utils.saveData(params, "/promotion/update-customer-quantity", PromotionProgram._xhrSave, 'errMsgShop', function(data) {
					$("#successMsgCustomer").html("Lưu dữ liệu thành công").show();
					setTimeout(function() {$("#successMsgCustomer").hide();},3000);
					//$("#exGrid").treegrid("reload");
					$("#exGrid").treegrid("reload");
				}, "loading2");
			}
		});
	},
	
	deleteCustomerMap: function(pcmId) {
		$(".ErrorMsgStyle").hide();
		var row = $("#exGrid").treegrid("find", pcmId);
		$.messager.confirm("Xác nhận", "Bạn có muốn xóa khách hàng?", function(r) {
			if (r) {
				var params = {
						promotionId: $("#masterDataArea #id").val().trim(),
				};
				
				if (row.attr.isCustomer == 1) {
					params.id = row.attr.mapId;
				} else {
					params.shopId = nodeId.replace("sh", "");
				}
				Utils.saveData(params, "/promotion/delete-customer", PromotionProgram._xhrSave, 'errMsgCustomer', function(data) {
					$("#successMsgShop").html("Lưu dữ liệu thành công").show();
					setTimeout(function() {$("#successMsgShop").hide();},3000);
					$("#exGrid").treegrid("reload");
				}, "loading2");
			}
		});
	},
	
	updateShopQuantity: function(shopId) {
		$(".ErrorMsgStyle").hide();
		var qtt = $("#txt"+shopId).val().trim();
		qtt = qtt.replace(/,/g, "");
		var amt = $("#txtamt"+shopId).val().trim();
		amt = amt.replace(/,/g, "");
		var num = $("#txtnum"+shopId).val().trim();
		num = num.replace(/,/g, "");
		var isEdit = $("#ck"+shopId).is(':checked');
		if (qtt.length > 0 && (isNaN(qtt) || Number(qtt) <= 0)) {
			$("#errMsgShop").html("Số suất đơn vị phải là số dương").show();
			return;
		}
		if (amt.length > 0 && (isNaN(amt) || Number(amt) <= 0)) {
			$("#errMsgShop").html("Số tiền phải là số dương").show();
			return;
		}
		if (num.length > 0 && (isNaN(num) || Number(num) <= 0)) {
			$("#errMsgShop").html("Số lượng phải là số dương").show();
			return;
		}//hic
		$.messager.confirm("Xác nhận", "Bạn có muốn cập nhật số suất, số tiền, số lượng cho đơn vị?", function(r) {
			if (r) {
				var params = {
						promotionId: $("#masterDataArea #id").val().trim(),
						shopId: shopId,
						quantity: qtt.length == 0 ? qtt : parseInt(qtt),
						amount: amt.length == 0 ? amt : parseInt(amt),
						number: num.length == 0 ? num : parseInt(num),
						isEdited:isEdit?1:0
				};
				Utils.saveData(params, "/promotion/update-shop-quantity", PromotionProgram._xhrSave, 'errMsgShop', function(data) {
					$("#successMsgShop").html("Lưu dữ liệu thành công").show();
					setTimeout(function() {$("#successMsgShop").hide();},3000);
					$("#exGrid").treegrid("reload");
					$("#labelListCustomer").hide();
					$("#promotionCustomerGrid").hide();
					$("#boxSearch1").hide();
				}, "loading2");
			}
		});
	},
	
	deleteShopMap: function(shopId) {
		$(".ErrorMsgStyle").hide();
		$.messager.confirm("Xác nhận", "Bạn có muốn xóa đơn vị?", function(r) {
			if (r) {
				var params = {
						promotionId: $("#masterDataArea #id").val().trim(),
						shopId: shopId
				};
				Utils.saveData(params, "/promotion/delete-shop", PromotionProgram._xhrSave, 'errMsgShop', function(data) {
					$("#successMsgShop").html("Lưu dữ liệu thành công").show();
					setTimeout(function() {$("#successMsgShop").hide();},3000);
					$("#exGrid").treegrid("reload");
				}, "loading2");
			}
		});
	},
	
	showShopDlg: function(shopId) {
		if (shopId == undefined || shopId == null) {
			shopId = 0;
		}
		$(".ErrorMsgStyle").hide();
		var html = '<div id="add-shopPopupDiv" style="display:none;">\
			<div id="add-shopPopup">\
			<div class="PopupContentMid">\
				<div class="GeneralForm Search1Form">\
					<label class="LabelStyle" style="width:100px;">Mã đơn vị</label>\
					<input id="codeDlg" class="InputTextStyle" style="width:150px;" maxlength="40" />\
					<label class="LabelStyle" style="width:100px;">Tên đơn vị</label>\
					<input id="nameDlg" class="InputTextStyle" style="width:230px;" maxlength="100" />\
					<div class="Clear"></div>\
					<div class="BtnCenterSection">\
					    <button class="BtnGeneralStyle" id="btnSearchDlg">Tìm kiếm</button>\
					</div>\
					\
					<div class="GridSection" id="gridDlgDiv">\
						<div id="gridDlg"></div>\
					</div>\
					\
					<p id="erMsgDlg" class="ErrorMsgStyle" style="padding-left:5px;display:none;"></p>\
					<div class="Clear"></div>\
					<div class="BtnCenterSection">\
					    <button class="BtnGeneralStyle" id="btnChooseDlg">Chọn</button>\
						<button class="BtnGeneralStyle" id="btnCloseDlg" onclick="javascript:$(\'#add-shopPopup\').dialog(\'close\');">Đóng</button>\
					</div>\
				</div>\
			</div>\
			</div>\
		</div>';
		$("body").append(html);
		Utils.functionAccessFillControl('add-shopPopupDiv', function(data){
			//Xu ly cac su kien lien quan den cac control phan quyen
		});
		$("#add-shopPopup").dialog({
			title: "Chọn đơn vị và Số suất KM",
			width: 800,
			heigth: "auto",
			modal: true, close: false, cache: false,
			onOpen: function() {
				$("#add-shopPopup").addClass("easyui-dialog");
				$("#add-shopPopup #codeDlg").focus();
				
				$("#add-shopPopup #gridDlg").treegrid({
					url: "/promotion/search-shop-dlg?promotionId="+$("#masterDataArea #id").val().trim()+"&shopId="+Number(shopId),
					rownumbers: false,
					width: $("#add-shopPopup #gridDlgDiv").width(),
					height: 350,
					fitColumns: true,
					idField: "nodeId",
					treeField: "text",
					selectOnCheck: false,
					checkOnSelect: false,
					columns: [[
						{field:"text", title:"Mã Đơn vị", sortable: false, resizable: false, width: 250, align:"left", formatter:CommonFormatter.formatNormalCell},
						{field:"quantity", title:"Số suất", sortable: false, resizable: false, width: 90, fixed:true, align:"left", formatter: function(v, r) {
							if (r.attr.isNPP) {
								if (r.attr.isExists) {
									return "<input type='text' id='txt-p"+r.attr.id+"' class='qttClass vinput-money' style='width:75px;' maxlength='11' disabled='disabled' exists='1' />";
								}
								return "<input type='text' id='txt-p"+r.attr.id+"' class='qttClass vinput-money' style='width:75px;' maxlength='11' exists='0' />";
							}
							return "";
						}},
						{field:"isEdit", title:"Sửa số suất", sortable: false, resizable: false, width: 90, fixed:true, align:"center", formatter: function(v, r) {
							if (r.attr.isNPP) {
								if (r.attr.isExists) {
									return "<input type='checkbox' id='edit-p"+r.attr.id+"' class='qttClass vinput-money' style='width:75px;' disabled='disabled' exists='1' />";
								}
								return "<input type='checkbox' id='edit-p"+r.attr.id+"' class='qttClass vinput-money' style='width:75px;' exists='0' />";
							}
							return "";
						}},
						{field:"amountMax", title:"Số tiền", sortable: false, resizable: false, width: 90, fixed:true, align:"left", formatter: function(v, r) {
							if (r.attr.isNPP) {
								if (r.attr.isExists) {
									return "<input type='text' id='txt-a"+r.attr.id+"' class='qttClass vinput-money' maxlength='11' style='width:75px;' disabled='disabled' exists='1' />";
								}
								return "<input type='text' id='txt-a"+r.attr.id+"' class='qttClass vinput-money' maxlength='11' style='width:75px;' exists='0' />";
							}
							return "";
						}},
						{field:"numMax", title:"Số lượng", sortable: false, resizable: false, width: 90, fixed:true, align:"left", formatter: function(v, r) {
							if (r.attr.isNPP) {
								if (r.attr.isExists) {
									return "<input type='text' id='txt-n"+r.attr.id+"' class='qttClass vinput-money' style='width:75px;' maxlength='11' disabled='disabled' exists='1' />";
								}
								return "<input type='text' id='txt-n"+r.attr.id+"' class='qttClass vinput-money' style='width:75px;' maxlength='11' exists='0' />";
							}
							return "";
						}},
						{field:"ck", title:"", sortable: false, resizable: false, width:35, fixed:true, align:"center", formatter: function(v, r) {
							var pId = r.attr.parentId;
							if (pId == undefined || pId == null) {
								pId = 0;
							}
							if (r.attr.isExists) {
								return "<input type='checkbox' id='ck"+r.nodeId+"' value='"+r.attr.id+"' disabled='disabled' exists='1' parentId='"+pId+"' onchange='PromotionProgram.onCheckShop(this);' />";
							}
							return "<input type='checkbox' id='ck"+r.nodeId+"' value='"+r.attr.id+"' exists='0' parentId='"+pId+"' onchange='PromotionProgram.onCheckShop(this);' />";
						}}
					]],
					onLoadSuccess: function(data) {
						$("#add-shopPopup #gridDlg").datagrid("resize");
//						Utils.bindFormatOnTextfieldInputCss('qttClass', Utils._TF_NUMBER, "#add-shopPopup");
//						$('#add-shopPopup .qttClass').each(function() {
//				      		VTUtilJS.formatCurrencyFor($(this).attr('id'));
//				      	});
						$('.vinput-money').each(function() {
							VTUtilJS.bindFormatOnTextfield($(this), VTUtilJS._TF_NUMBER_COMMA);
							VTUtilJS.formatCurrencyFor($(this));
						});
						setTimeout(function() {
							var hDlg = parseInt($("#add-shopPopup").parent().height());
							var hW = parseInt($(window).height());
							var d = hW - hDlg;
							d = d/2 + document.documentElement.scrollTop;
							if (d < 0) { d = 0; }
							$("#add-shopPopup").parent().css('top', d);
			    		}, 1000);
					}
				});
				
				$("#add-shopPopup #btnSearchDlg").click(function() {
					var p = {
							code: $("#add-shopPopup #codeDlg").val().trim(),
							name: $("#add-shopPopup #nameDlg").val().trim()
					};
					$("#add-shopPopup #gridDlg").treegrid("reload", p);
				});
				$("#add-shopPopup #codeDlg").parent().keyup(function(event) {
					if (event.keyCode == keyCodes.ENTER) {
						$("#add-shopPopup #btnSearchDlg").click();
					}
				});
				$("#add-shopPopup #btnChooseDlg").click(function() {
					if ($("#add-shopPopup input[id^=ck]:checked").length == 0) {
						$("#erMsgDlg").html("Không có đơn vị nào được chọn").show();
						return;
					}
					var lstTmp = new Map();
					var lstIsEdit = new Map();
					var lstTmpAmt = new Map();
					var lstTmpNum = new Map();
					var v, qtt, amt, num;
					var isUpdate = true;
					$("#add-shopPopup input[id^=ck]:checked").each(function() {
						v = $(this).val().trim();
						if ($("#txt-p"+v).length == 1) {
							qtt = $("#txt-p"+v).val().trim();
							isEdit = $("#edit-p"+v).is(':checked');
						} else {
							qtt = "";
							isEdit = false;
						}
						qtt = qtt.replace(/,/g, "");
						
						if (qtt.length > 0 && (isNaN(qtt) || Number(qtt) <= 0)) {
							$("#erMsgDlg").html("Số suất phải là số nguyên dương").show();
							lstTmp = null;
							isUpdate = false;
							return;
						}
						lstTmp.put(v, qtt);
						lstIsEdit.put(v, isEdit);
						
						//---Amount
						if ($("#txt-a"+v).length == 1) {
							amt = $("#txt-a"+v).val().trim();
						} else {
							amt = "";
						}
						amt = amt.replace(/,/g, "");
						
						if (amt.length > 0 && (isNaN(amt) || Number(amt) <= 0)) {
							$("#erMsgDlg").html("Số tiền phải là số nguyên dương").show();
							isUpdate = false;
							return;
						}
						lstTmpAmt.put(v, amt);
						
						//---Number
						if ($("#txt-n"+v).length == 1) {
							num = $("#txt-n"+v).val().trim();
						} else {
							num = "";
						}
						num = num.replace(/,/g, "");
						
						if (num.length > 0 && (isNaN(num) || Number(num) <= 0)) {
							$("#erMsgDlg").html("Số lượng phải là số nguyên dương").show();
							isUpdate = false;
							return;
						}
						lstTmpNum.put(v, num);
					});
					if(isUpdate) {
						PromotionProgram.addShopQtt(lstTmp, lstIsEdit, lstTmpAmt, lstTmpNum);						
					}
				});
			},
			onClose: function() {
				$("#add-shopPopup").dialog("destroy");
				$("#add-shopPopupDiv").remove();
			}
		});
	},
	
	onCheckShop: function(t) {
		var ck = $(t).prop("checked");
		if (ck) {
			var p = "#ck" + $(t).attr("parentId");
			PromotionProgram.removeParentShopCheck(p);
		} else {
			var p = "#ck" + $(t).attr("parentId");
			PromotionProgram.enableParentShopCheck(p);
		}
	},
	
	removeParentShopCheck: function(t) {
		if ($(t).length == 0) {
			return;
		}
		$(t).removeAttr("checked");
		$(t).attr("disabled", "disabled");
		var p = "#ck" + $(t).attr("parentId");
		PromotionProgram.removeParentShopCheck(p);
	},
	
	enableParentShopCheck: function(t) {
		if ($(t).length == 0 || $(t).attr("exists") == "1") {
			return;
		}
		$(t).removeAttr("disabled");
		var p = "#ck" + $(t).attr("parentId");
		PromotionProgram.enableParentShopCheck(p);
	},
	
	addShopQtt: function(lstShopTmp, lstIsEdit, lstTmpAmt, lstTmpNum) {
		$(".ErrorMsgStyle").hide();
		$.messager.confirm("Xác nhận", "Bạn có muốn thêm đơn vị?", function(r) {
			if (r) {
				var zezo = 0;
				var arrQttHL = [];
				var arrAmtHL = [];
				var arrNumHL = [];
				var arrIdHl = lstShopTmp.keyArray;
				for (var i = 0, size = arrIdHl.length; i < size; i++) {
					var valMap = lstShopTmp.get(arrIdHl[i]);
					if (valMap != undefined && valMap != null && valMap.length > 0) {
						arrQttHL.push(valMap);
					} else {
						arrQttHL.push(zezo);
					}
					valMap = lstTmpAmt.get(arrIdHl[i]);
					if (valMap != undefined && valMap != null && valMap.length > 0) {
						arrAmtHL.push(valMap);
					} else { 
						arrAmtHL.push(zezo);
					}
					valMap = lstTmpNum.get(arrIdHl[i]);
					if (valMap != undefined && valMap != null && valMap.length > 0) {
						arrNumHL.push(valMap);
					} else {
						arrNumHL.push(zezo);
					}
				}
				var params = {
						promotionId: $("#masterDataArea #id").val().trim(),
						lstId: arrIdHl,
						lstEdit: lstIsEdit.valArray,
						lstQtt: arrQttHL,
						lstAmt: arrAmtHL,
						lstNum: arrNumHL
				};
				$("#add-shopPopup").dialog("close");
				Utils.saveData(params, "/promotion/add-shop", PromotionProgram._xhrSave, 'errMsgShop', function(data) {
					$("#successMsgShop").html("Lưu dữ liệu thành công").show();
					setTimeout(function() {$("#successMsgShop").hide();},3000);
					$("#exGrid").treegrid("reload");
					$("#labelListCustomer").hide();
					$("#promotionCustomerGrid").hide();
					$("#boxSearch1").hide();
				}, "loading2");
			}
		});
	},
	
	exportPromotionShop: function() {
		$(".ErrorMsgStyle").hide();
		if (PromotionProgram._promotionShopParams == null) {
			PromotionProgram._promotionShopParams = {};
			PromotionProgram._promotionShopParams.promotionId = $("#masterDataArea #id").val().trim();
		}
		ReportUtils.exportReport("/promotion/export-promotion-shop", PromotionProgram._promotionShopParams, "errMsgShop");
	},
	
	importPromotionShop: function() {
		$('.ErrorMsgStyle').hide();
		VTUtilJS.importExcelUtils(function(data) {
			$("#exGrid").treegrid("reload");
			$("#labelListCustomer").hide();
			$("#promotionCustomerGrid").hide();
			$("#boxSearch1").hide();
			$("#errExcelMsgShop").html(data.message).show();
		}, "importFrm", "excelFile", null, "errExcelMsgShop");
	},
	
	checkSaleLevelCatIdInList: function(idSaleCatLevel,listData3){
		if(listData3!=null && listData3.length > 0){
			for(var j=0;j<listData3.length;++j){
				if(listData3[j].idSaleCatLevel == idSaleCatLevel){
					return true;
				}
			}
		}else{
			return false;
		}
	},
	loadAppliedAttributes: function(promotionId){
		$('.ErrorMsgStyle').hide();
		if(PromotionProgram._flagAjaxTabAttribute){
			PromotionProgram._flagAjaxTabAttribute = false;
			$.getJSON("/promotion/load-applied-attributes?promotionId="+promotionId, function(result){
				PromotionProgram._flagAjaxTabAttribute = true;
				var promotionStatus = $('#promotionStatus').val();
				var list = result.list;
				if (list != null) {
					for (var k = 0; k < list.length; k++){
						//đoạn này viết chung cho 1 attribute
						var objectType = list[k].objectType;
						var objectId = list[k].objectId;
						var name = list[k].name;
						name = Utils.XSSEncode(name);
						var html = '';
						//end đoạn viết chung.
						if (objectType == 2) {
							html+= '<div>';
							html+= '<div class="Clear"></div>';
							html+= '<li>';
							html+= '<input name="'  + name + '" objectType="'+ objectType +'" objectId="'+objectId +'" type="checkbox"  class="InputCbxStyle checkBoxAttribute"/>';
							html+= '<label class="LabelStyle Label4Style">'+name+'</label>';
							html+= '</li>';
							html+= '<div class="BoxSelect BoxSelect2">';
							html+= '<select id="customerType" style="width: 275px;" class="MySelectBoxClass " multiple="multiple">';
							var listData2 = list[k].listData;	
							var arrHtml = new Array();
							if (listData2 != null) {
								for (var i = 0; i < listData2.length; i++) {
									if (listData2[i].checked == true) {
										arrHtml.push('<option value="'+ listData2[i].idChannelType +'" selected="selected">'+Utils.XSSEncode(listData2[i].codeChannelType) +'-'+Utils.XSSEncode(listData2[i].nameChannelType) +'</option>');
									}else{
										arrHtml.push('<option value="'+ listData2[i].idChannelType +'">'+Utils.XSSEncode(listData2[i].codeChannelType) +'-'+Utils.XSSEncode(listData2[i].nameChannelType) +'</option>');
									}
								}
							}
							html+= arrHtml.join("");
							html+= '</select>';
							html+= '</div>';
							html+= '<div class="Clear"></div>';
							html+= '</div>';
							$('#right').append(html);
						} else if (objectType == 3) {
							var listData3 = list[k].listData;	
							var objectType = 3;
							var objectId = 0;
							var name = "Mức doanh số";
							var html = '';
							//đoạn này hình như là giống nhau trong các trường hợp của objectType:
							html+= '<div id="saleLevel">';//Nhớ đặt id chỗ này để phân biệt, Ví dụ phân biệt: combobox(select)của mức doanh số, của loại khách hàng. 
							//phân biệt checkbox của mức doanh số, hay checkbox của loại khách hàng. Sau này bên hàm loadAppliedAttribute mình cũng đặt id 
							//chỗ objectType=3 giống thế này mới chạy được.
							html+= '<div class="Clear"></div>';
							html+= 	'<li>';
							html+=	'<input name="'  + name + '" objectType="'+ objectType +'" objectId="'+objectId +'" type="checkbox"  class="InputCbxStyle checkBoxAttribute"/>';
							html+=	'<label class="LabelStyle Label4Style">'+name+'</label>';
							html+=	'</li>';
							//end đoạn giống nhau
							/*for qua cái list, add những label ngành hàng vào. Các lable Ngành hàng thì mình đặt chung 1 class "cat", để sau này mình lặp each 
							qua mỗi label, lấy catId(idProductInfoVO)của label đó, và lấy tất cả các mức mà người ta chọn, vậy cái combobox mức nên đặt id theo
							id của ngành hàng.*/
							var listProductInfoVO = list[k].listProductInfoVO;	
							if (listProductInfoVO != null) {
								for (var i = 0; i < listProductInfoVO.length; i++) {
									if (i == 0) {// if else để canh chỉnh html cho thẳng hàng đó mà.
										//label ngành hàng nên đặt 1 thuộc tính là catId
										html+= '<label catId="'+listProductInfoVO[i].idProductInfoVO +'" style="width: 150px;" class="LabelStyle Label5Style cat">'+Utils.XSSEncode(listProductInfoVO[i].codeProductInfoVO)+'</label>';
										//add combobox mức vao label nganh hang, combobox nên đặt id theo ngành hàng.
										html+= '<div class="BoxSelect BoxSelect2">';
										html+= '<select id="saleLevel-'+listProductInfoVO[i].idProductInfoVO +'" class="MySelectBoxClass saleLevelCat" multiple="multiple">';
										var listSaleLevelCat = listProductInfoVO[i].listSaleCatLevelVO;
										var arrHtml = new Array();
										if (listSaleLevelCat != null) {
											for(var t=0;t<listSaleLevelCat.length;++t){
												var checked = PromotionProgram.checkSaleLevelCatIdInList(listSaleLevelCat[t].idSaleCatLevel,listData3);
												if(checked == true){
													arrHtml.push('<option value="'+ listSaleLevelCat[t].idSaleCatLevel +'" selected="selected">'+ Utils.XSSEncode(listSaleLevelCat[t].codeSaleCatLevel) +'-'+Utils.XSSEncode(listSaleLevelCat[t].nameSaleCatLevel) +'</option>');
												}else{
													arrHtml.push('<option value="'+ listSaleLevelCat[t].idSaleCatLevel +'">'+ Utils.XSSEncode(listSaleLevelCat[t].codeSaleCatLevel) +'-'+Utils.XSSEncode(listSaleLevelCat[t].nameSaleCatLevel) +'</option>');
												}
											}
										}
										html+= arrHtml.join("");
										html+= '</select>';
										html+= '</div>';
									}else{
										html+= '<div class="Clear"></div>';
										html+= '<label catId="'+listProductInfoVO[i].idProductInfoVO +'" style="width: 150px;" class="LabelStyle Label6Style">'+Utils.XSSEncode(listProductInfoVO[i].codeProductInfoVO)+'</label>';
										html+= '<div class="BoxSelect BoxSelect2">';
										html+= '<select id="saleLevel-'+listProductInfoVO[i].idProductInfoVO +'" class="MySelectBoxClass saleLevelCat" multiple="multiple">';
										var listSaleLevelCat = listProductInfoVO[i].listSaleCatLevelVO;
										var arrHtml = new Array();
										if(listSaleLevelCat!=null){
											for(var t=0;t<listSaleLevelCat.length;++t){
												var checked = PromotionProgram.checkSaleLevelCatIdInList(listSaleLevelCat[t].idSaleCatLevel,listData3);
												if(checked == true){
													arrHtml.push('<option value="'+ listSaleLevelCat[t].idSaleCatLevel +'" selected="selected">'+ Utils.XSSEncode(listSaleLevelCat[t].codeSaleCatLevel) +'-'+Utils.XSSEncode(listSaleLevelCat[t].nameSaleCatLevel) +'</option>');
												}else{
													arrHtml.push('<option value="'+ listSaleLevelCat[t].idSaleCatLevel +'">'+ Utils.XSSEncode(listSaleLevelCat[t].codeSaleCatLevel) +'-'+Utils.XSSEncode(listSaleLevelCat[t].nameSaleCatLevel) +'</option>');
												}
											}
										}
										html+= arrHtml.join("");
										html+= '</select>';
										html+= '</div>';
									}
								}
							}
							html+= '<div class="Clear"></div>';
							html+= '</div>';
							$('#right').append(html);
						}else if(objectType == 1){
							var valueType = list[k].valueType;
							var listData1 = list[k].listData;
							
							html+= '<div>';
							html+= '<div class="Clear"></div>';
							html+= '<li>';
							//thằng input của thuộc tính động cần bỏ valueType vào để sau này xử lý khi lưu.
							html+= '<input valueType="'+valueType+'"  name="'  + name + '" objectType="'+ objectType +'" objectId="'+objectId +'" type="checkbox"  class="InputCbxStyle checkBoxAttribute"/>';
							html+= '<label class="LabelStyle Label4Style">'+name+'</label>';
							html+= '</li>';
								
							if(listData1!=null){
								if(valueType=="CHARACTER"){
									//Nên đặt id html của text CHARACTER này theo attributeId cộng chuỗi CHARACTER  thì sẽ không bị conflict trong danh sách. Sau này muốn lưu thì
									//select theo attributeId để lấy ra dữ liệu.
									html+= '<input id="'+objectId+'_CHARACTER" type="text" style="width: 377px;" value="'+listData1[0]+'" class="InputTextStyle InputText1Style"/>';
								}else if(valueType=="NUMBER"){
									//Đặt id theo: attributeId_NUMBER_from và attributeId_NUMBER_to:
									var from = '';
									var to = '';
									if(listData1[0]!=null){
										from = listData1[0];
									}
									if(listData1[1]!=null){
										to = listData1[1];
									}
									html+= '<input id="'+objectId+'_NUMBER_from" style="width: 170px;" type="text" value="'+formatCurrency(from)+'" class="InputTextStyle InputText5Style"/>';
									html+= '<label class="LabelStyle Label7Style">-</label>';
									html+= '<input id="'+objectId+'_NUMBER_to" style="width: 170px;" type="text" value="'+formatCurrency(to)+'" class="InputTextStyle InputText5Style"/>';
								}else if(valueType=="DATE_TIME"){
									//Đặt id theo: attributeId_DATETIME_from và attributeId_DATETIME_to. Nhớ applyDateTimePicker applyDateTimePicker("#endDate");
									//Mình đặt chung bọn chúng 1 class rồi,applyDateTimePicker cho class đó được không nhỉ. Tí thử. 
									var from = '';
									var to = '';
									if(listData1[0]!=null){
										from = listData1[0];
									}
									if(listData1[1]!=null){
										to = listData1[1];
									}
									html+= '<input id="'+objectId+'_DATETIME_from"  style="width: 146px;"  type="text" value="'+from+'" class="InputTextStyle InputText6Style"/>';
									html+= '<label class="LabelStyle Label7Style">-</label>';
									html+= '<input id="'+objectId+'_DATETIME_to" style="width: 146px;"  type="text" value="'+to+'" class="InputTextStyle InputText6Style"/>';
								}else if(valueType=="CHOICE" || valueType=="MULTI_CHOICE"){
									var arrHtml = new Array();
									html+= '<div class="BoxSelect BoxSelect2">';
									html+= '<select id="'+objectId+'" class="MySelectBoxClass" multiple="multiple">';
									for(var i=0;i<listData1.length;++i){
										if(listData1[i].checked == true){
											arrHtml.push('<option value="'+ listData1[i].enumId +'" selected="selected">'+Utils.XSSEncode(listData1[i].code)+'-'+Utils.XSSEncode(listData1[i].name) +'</option>');
										}else{
											arrHtml.push('<option value="'+ listData1[i].enumId +'">'+Utils.XSSEncode(listData1[i].code)+'-'+Utils.XSSEncode(listData1[i].name) +'</option>');
										}
									}
									html+= arrHtml.join("");
									html+= '</select>';
									html+= '</div>';
								}
							}
							html+= '<div class="Clear"></div>';
							html+= '</div>';
							$('#right').append(html);
							//dự thảo thì ko disable
							if(valueType=="DATE_TIME"){
								VTUtilJS.applyDateTimePicker(objectId+'_DATETIME_from');
								VTUtilJS.applyDateTimePicker(objectId+'_DATETIME_to');
							}else if(valueType=="NUMBER"){
								Utils.bindFormatOnTextfield(objectId+'_NUMBER_from',Utils._TF_NUMBER);
								Utils.formatCurrencyFor(objectId+'_NUMBER_from');
								Utils.bindFormatOnTextfield(objectId+'_NUMBER_to',Utils._TF_NUMBER);
								Utils.formatCurrencyFor(objectId+'_NUMBER_to');
							}
						}
					}
					//Xong for:
					$('#right .MySelectBoxClass').each(function(){
						var aCombobox = $(this);
						if(!(aCombobox.next().hasClass('ui-dropdownchecklist'))){//Cái này để kiểm tra comboBox đó đã gọi dropdownchecklist lần nào chưa.
							//Nếu nó đã gọi dropdownchecklist 1 lần rồi, thì thể nào cũng gien ra 1 element html có class ui-dropdownchecklist nằm kế bên comboBox.
							aCombobox.dropdownchecklist({ maxDropHeight: 150,width: 277, textFormatFunction: function(options){
								var text = '';
								$('.ui-dropdownchecklist-dropcontainer input[type=checkbox]').each(function(){
									var aCheckBox = $(this);
									if(aCheckBox.parent().parent().parent().prev().prev().attr('id') == aCombobox.attr('id')){//Tức là chỉ xử lý với những checkbox thuộc combobox đó.
										if(aCheckBox.attr('checked')=='checked'){
											text += aCheckBox.next().text().split('-')[0] + ', ';
										}
									}
								});
								if(text != ''){
									text = text.substring(0, text.length-2);//bo dau ', ' cuoi cung.
								}
								return text;
							} });
						}
					});
				}
				//ko dự thảo thì disable.
				if(promotionStatus != 2 /*|| $('#permissionUser').val() == 'false'*/){
					$("#right .MySelectBoxClass").dropdownchecklist("disable");
					$("#right :input").attr('disabled',true);
					$("#left :input").attr('disabled',true);
					$('#group_change_updateCustomerAttribute').hide();
					$('#right .CalendarLink').hide();
				}
			});	
		}
	},
	
	selectAttributes: function() {
		$('.ErrorMsgStyle').hide();
		var listObjectType= new Array();
		var listObjectId= new Array();
		$('#left :checkbox').each(function(){	
			if($(this).is(':checked')){
				//remove ben left, show ben right
				$(this).parent().remove(); 
				var objectType = $(this).attr('objectType');
				var objectId = $(this).attr('objectId');
				listObjectType.push(objectType);
				if(objectType == 2 || objectType == 3){
					listObjectId.push(0);
				}else{
					listObjectId.push(objectId);
				}
			}
		});	
		var data = new Object();
		data.lstObjectType = listObjectType;
		data.lstId = listObjectId;
		var kData = $.param(data, true);
		$.getJSON("/promotion/get-all-data-attributes",kData,function(result){
			var listOut = result.list;	
			if(listOut!=null){
				for(var h=0;h<listOut.length;++h){
					var objectType = listOut[h].objectType;
					var objectId = listOut[h].objectId;
					var valueType = listOut[h].valueType;
					var html = '';
					if(objectType == 2){
						var name = 'Loại khách hàng';
						html+= '<div>';
						html+= '<div class="Clear"></div>';
						html+= '<li>';
						html+= '<input name="'  + name + '" objectType="'+ objectType +'" objectId="'+objectId +'" type="checkbox"  class="InputCbxStyle checkBoxAttribute"/>';
						html+= '<label class="LabelStyle Label4Style">'+Utils.XSSEncode(name)+'</label>';
						html+= '</li>';
						html+= '<div class="BoxSelect BoxSelect2">';
						html+= '<select id="customerType" style="width: 275px;" class="MySelectBoxClass" multiple="multiple">';
						var list = listOut[h].listData;	
						var arrHtml = new Array();
						if(list!=null){
							for(var i=0;i<list.length;++i){
								arrHtml.push('<option value="'+ list[i].idChannelType +'">'+ Utils.XSSEncode(list[i].codeChannelType)+ '-' +Utils.XSSEncode(list[i].nameChannelType)+ '</option>');
							}
						}
						html+= arrHtml.join("");
						html+= '</select>';
						html+= '</div>';
						html+= '<div class="Clear"></div>';
						html+= '</div>';
						$('#right').append(html);
					}else if(objectType == 3){
						var name = 'Mức doanh số';
						html+= '<div id="saleLevel">';
						html+= '<div class="Clear"></div>';
						html+= 	'<li>';
						html+=	'<input name="'  + name + '" objectType="'+ objectType +'" objectId="'+objectId +'" type="checkbox"  class="InputCbxStyle checkBoxAttribute"/>';
						html+=	'<label class="LabelStyle Label4Style">'+Utils.XSSEncode(name)+'</label>';
						html+=	'</li>';
						var list = listOut[h].listProductInfoVO;//result.list;	
						if(list!=null){
							for(var i=0;i<list.length;++i){
								if(i==0){// if else để canh chỉnh html cho thẳng hàng đó mà.
									//label ngành hàng nên đặt 1 thuộc tính là catId
									html+= '<label catId="'+list[i].idProductInfoVO +'" style="width: 150px;" class="LabelStyle Label5Style cat">'+Utils.XSSEncode(list[i].codeProductInfoVO)+'</label>';
									//add combobox mức vao label nganh hang, combobox nên đặt id theo ngành hàng.
									html+= '<div class="BoxSelect BoxSelect2">';
									html+= '<select id="saleLevel-'+list[i].idProductInfoVO +'" class="MySelectBoxClass saleLevelCat" multiple="multiple">';
									var listSaleLevelCat = list[i].listSaleCatLevelVO;
									var arrHtml = new Array();
									if(listSaleLevelCat!=null){
										for(var k=0;k<listSaleLevelCat.length;++k){
											arrHtml.push('<option value="'+ listSaleLevelCat[k].idSaleCatLevel +'">'+ Utils.XSSEncode(listSaleLevelCat[k].codeSaleCatLevel)+'-'+Utils.XSSEncode(listSaleLevelCat[k].nameSaleCatLevel) +'</option>');
										}
									}
									html+= arrHtml.join("");
									html+= '</select>';
									html+= '</div>';
								}else{
									html+= '<div class="Clear"></div>';
									html+= '<label catId="'+list[i].idProductInfoVO +'" style="width: 150px;" class="LabelStyle Label6Style" >'+Utils.XSSEncode(list[i].codeProductInfoVO)+'</label>';
									//add combobox mức vao label nganh hang, combobox nên đặt id theo ngành hàng.
									html+= '<div class="BoxSelect BoxSelect2">';
									html+= '<select id="saleLevel-'+list[i].idProductInfoVO +'" class="MySelectBoxClass saleLevelCat" multiple="multiple">';
									var listSaleLevelCat = list[i].listSaleCatLevelVO;
									var arrHtml = new Array();
									if(listSaleLevelCat!=null){
										for(var k=0;k<listSaleLevelCat.length;++k){
											arrHtml.push('<option value="'+ listSaleLevelCat[k].idSaleCatLevel +'">'+Utils.XSSEncode(listSaleLevelCat[k].codeSaleCatLevel)+'-'+Utils.XSSEncode(listSaleLevelCat[k].nameSaleCatLevel) +'</option>');
										}
									}
									html+= arrHtml.join("");
									html+= '</select>';
									html+= '</div>';
								}
							}
						}
						html+= '<div class="Clear"></div>';
						html+= '</div>';
						$('#right').append(html);
					}else if(objectType == 1){
						var name = listOut[h].name;
						name = Utils.XSSEncode(name);
						html+= '<div>';
						html+= '<div class="Clear"></div>';
						html+= 	'<li>';
						//thằng input của thuộc tính động cần bỏ valueType vào để sau này xử lý khi lưu.
						html+=	'<input valueType="'+valueType+'" name="'  + Utils.XSSEncode(name) + '" objectType="'+ objectType +'" objectId="'+objectId +'" type="checkbox"  class="InputCbxStyle checkBoxAttribute"/>';
						html+=	'<label class="LabelStyle Label4Style">'+Utils.XSSEncode(name)+'</label>';
						html+=  '</li>';
						//Chia các trường hợp theo valueType của attributeId
						if(valueType=="CHARACTER"){
							//Nên đặt id html của text CHARACTER này theo attributeId cộng chuỗi CHARACTER  thì sẽ không bị conflict trong danh sách. Sau này muốn lưu thì
							//select theo attributeId để lấy ra dữ liệu.
							html+= '<input id="'+objectId+'_CHARACTER" type="text" style="width: 377px;" class="InputTextStyle InputText1Style"/>';
						}else if(valueType=="NUMBER"){
							html+= '<input id="'+objectId+'_NUMBER_from" type="text" style="width: 170px;" class="InputTextStyle InputText5Style"/>';
							html+= '<label class="LabelStyle Label7Style">-</label>';
							html+= '<input id="'+objectId+'_NUMBER_to" type="text" style="width: 170px;" class="InputTextStyle InputText5Style"/>';
						}else if(valueType=="DATE_TIME"){
							html+= '<input id="'+objectId+'_DATETIME_from" style="width: 146px;" type="text" class="InputTextStyle InputText6Style "/>';
							html+= '<label class="LabelStyle Label7Style">-</label>';
							html+= '<input id="'+objectId+'_DATETIME_to" style="width: 146px;" type="text" class="InputTextStyle InputText6Style "/>';
						}else if(valueType=="CHOICE" || valueType=="MULTI_CHOICE"){
							var list = listOut[h].listData;	
							var arrHtml = new Array();
							html+= '<div class="BoxSelect BoxSelect2">';
							html+= '<select id="'+objectId+'" class="MySelectBoxClass" multiple="multiple">';
							if(list!=null){
								for(var i=0;i<list.length;++i){
									arrHtml.push('<option value="'+ list[i].enumId +'">'+Utils.XSSEncode(list[i].code)+'-'+Utils.XSSEncode(list[i].name) +'</option>');
								}
							}
							html+= arrHtml.join("");
							html+= '</select>';
							html+= '</div>';
						}
						html+= '<div class="Clear"></div>';
						html+= '</div>';
						$('#right').append(html);
						if(valueType=="DATE_TIME"){
							VTUtilJS.applyDateTimePicker(objectId+'_DATETIME_from');
							VTUtilJS.applyDateTimePicker(objectId+'_DATETIME_to');
						}else if(valueType=="NUMBER"){
							Utils.bindFormatOnTextfield(objectId+'_NUMBER_from',Utils._TF_NUMBER);
							Utils.formatCurrencyFor(objectId+'_NUMBER_from');
							Utils.bindFormatOnTextfield(objectId+'_NUMBER_to',Utils._TF_NUMBER);
							Utils.formatCurrencyFor(objectId+'_NUMBER_to');
						}
					}
				}
				//Xong for:
				$('#right .MySelectBoxClass').each(function(){
					var aCombobox = $(this);
					if(!(aCombobox.next().hasClass('ui-dropdownchecklist'))){//Cái này để kiểm tra comboBox đó đã gọi dropdownchecklist lần nào chưa.
						//Nếu nó đã gọi dropdownchecklist 1 lần rồi, thì thể nào cũng gien ra 1 element html có class ui-dropdownchecklist nằm kế bên comboBox.
						aCombobox.dropdownchecklist({ maxDropHeight: 150,width: 277, textFormatFunction: function(options){
							var text = '';
							$('.ui-dropdownchecklist-dropcontainer input[type=checkbox]').each(function(){
								var aCheckBox = $(this);
								if(aCheckBox.parent().parent().parent().prev().prev().attr('id') == aCombobox.attr('id')){//Tức là chỉ xử lý với những checkbox thuộc combobox đó.
									if(aCheckBox.attr('checked')=='checked'){
										text += aCheckBox.next().text().split('-')[0] + ', ';
									}
								}
							});
							if(text != ''){
								text = text.substring(0, text.length-2);//bo dau ', ' cuoi cung.
							}
							return text;
						} });
					}
				});
				
			}
		});	
	},
	removeAttributes: function(){
		$('.ErrorMsgStyle').hide();
		var html = '';//để bên ngoài vòng for là đúng. Còn bên hàm toSelectAttribute thì phải để bên trong do cuộc gọi bất đồng bộ
		$('#right .checkBoxAttribute').each(function(){
			if($(this).is(':checked')){
				//remove ben right, show ben left
				$(this).parent().parent().remove(); 
				var objectType = $(this).attr('objectType');
				var objectId = $(this).attr('objectId');
				var name = $(this).attr('name');
				html+= '<li> <input name="'  + Utils.XSSEncode(name) + '" objectType="'+ objectType +'" objectId="'+objectId +'" type="checkbox"  class="InputCbxStyle"/><label style="width:150px" class="LabelStyle Label3Style">'+Utils.XSSEncode(name)+'</label></li>';
			}
		});	
		$('#left').append(html);//để bên ngoài vòng for là đúng. Còn bên hàm toSelectAttribute thì phải để bên trong do cuộc gọi bất đồng bộ
	},
	
	saveCustomerAttributes: function(){
		var message = '';
		var messageSaleLevel = '';
		var messageCustomerType = '';
		var messageOther = '';
		$('.ErrorMsgStyle').hide();
		var data = new Object();
		var listCustomerType= new Array();
		var listSaleLevelCatId= new Array();
		var listAttribute= new Array();
		var listAttributeDataInField=new Array();
		var nameAttr = '';
		$('.checkBoxAttribute').each(function(){
			var objectType = $(this).attr('objectType');
			var objectId = $(this).attr('objectId');
			var name = $(this).attr('name');
			
			if(objectType == 2){
				listAttribute.push(-2);//Loai kh cho id = -2 di. De ti nua if else tren action.
				listAttributeDataInField.push('CustomerType');
				messageCustomerType = 'noCheckCustomerType';
				listCustomerType= new Array();
				$('#ddcl-customerType-ddw input[type=checkbox]').each(function(){
					if($(this).attr('checked')=='checked'){
						listCustomerType.push($(this).val());
						messageCustomerType = 'haveCheckCustomerType';
					}
				});
			}else if(objectType == 3){ 
				listAttribute.push(-3);//Muc doanh so cho id = -3 di. De ti nua if else tren action.
				listAttributeDataInField.push('SaleLevel');
				messageSaleLevel = 'noCheckSaleLevel';
				listSaleLevelCatId= new Array();
				$('#saleLevel input[type=checkbox]').each(function(){
					if($(this).attr('checked')=='checked' && !$(this).hasClass('checkBoxAttribute')){
						listSaleLevelCatId.push(this.value);
//						listSaleLevelCatId.push($(this).attr('objectid'));
						//vào được đây tức là mức doanh số có ít nhất 1 combox được check.
						messageSaleLevel = 'haveCheckSaleLevel';
					}
				});
			}else if(objectType == 1){ 
				listAttribute.push(objectId);
				var valueType = $(this).attr('valueType');
				if(valueType=="CHARACTER" || valueType=="NUMBER" || valueType=="DATE_TIME"){
					var boolean = PromotionProgram.getTrue_DataOrFalse_MessageOfAutoAttribute(objectId, name, valueType);
					var arr = boolean.split('__');
					if(arr[0]=='false'){
						message = arr[1];
						return false;
					}else{
						listAttributeDataInField.push(arr[1]);
					}
				}else if(valueType=="CHOICE" || valueType=="MULTI_CHOICE"){
					messageOther = 'noCheckOther';
					var dataOfAutoAttribute = '';
					$('#ddcl-'+objectId+'-ddw input[type=checkbox]').each(function(){
						if($(this).attr('checked')=='checked'){
							dataOfAutoAttribute += $(this).val() +',';//Nhớ chú ý dấu phẩy cuối cùng
							messageOther = 'haveCheckOther';
						}
					});
					nameAttr = name;
					listAttributeDataInField.push(dataOfAutoAttribute);
				}
			}
			if(messageSaleLevel == 'noCheckSaleLevel'){
				message = 'Bạn chưa chọn giá trị cho thuộc tính Mức doanh số';
			}
			if(messageCustomerType == 'noCheckCustomerType'){
				message = 'Bạn chưa chọn giá trị cho thuộc tính Loại khách hàng';
			}
			if(messageOther == 'noCheckOther'){
				message = 'Bạn chưa chọn giá trị cho thuộc tính '+nameAttr;
			}
		});
		if(message.length > 0){
			$('#errMsgSave').html(message).show();
			return false;
		}
		data.lstCustomerType = listCustomerType;
		data.lstSaleLevelCatId = listSaleLevelCatId;
		data.lstId = listAttribute;
		data.lstAttDataInField = listAttributeDataInField;
		data.promotionId=$('#masterDataArea #id').val().trim();
		Utils.addOrSaveData(data, '/promotion/save-promotion-customer-attribute', PromotionProgram._xhrSave,
				'errMsgSave', null, 'loading2', '#customerAttributeMsg ');//Prefix để selector jquery mà truyền thiếu "khoảng trắng" 
		//là có thể lỗi, không xuất ra câu thông báo lỗi, hay câu thông báo thành công đâu.
		return false;
	},
	getTrue_DataOrFalse_MessageOfAutoAttribute: function(objectId, nameAttribute,valueType){
		if(valueType=="CHARACTER"){
			var msg = '';
			var dataOfAutoAttribute = $('#'+objectId+'_CHARACTER').val();
			if(dataOfAutoAttribute==''){
				msg = 'Bạn chưa nhập giá trị cho thuộc tính '+nameAttribute;
				$('#'+objectId+'_CHARACTER').focus();
				return 'false__'+msg;
			}
			msg = Utils.getMessageOfSpecialCharactersValidate(objectId+'_CHARACTER','',Utils._NAME);
			if(msg.length > 0){
				msg = 'Thuộc tính '+nameAttribute+': Giá trị nhập vào không được chứa các ký tự đặc biệt [<>\~#&$%@*()^`\'"'; 
				return 'false__'+msg;
			}
			return 'true__'+dataOfAutoAttribute;
		}else if(valueType=="NUMBER"){
			var msg = '';
			var NUMBER_from = $('#'+objectId+'_NUMBER_from').val();
			var NUMBER_to = $('#'+objectId+'_NUMBER_to').val();
			if (NUMBER_from == '' && NUMBER_to == ''){
				msg = 'Bạn chưa nhập giá trị cho thuộc tính '+nameAttribute;
				$('#'+objectId+'_NUMBER_from').focus();
				return 'false__'+msg;
			}
			if (NUMBER_from != ''){
				NUMBER_from = parseInt(Utils.returnMoneyValue(NUMBER_from.trim()));
			}
			if (NUMBER_to != ''){
				NUMBER_to = parseInt(Utils.returnMoneyValue(NUMBER_to.trim()));
			}
			var dataOfAutoAttribute = NUMBER_from +','+NUMBER_to;//Nếu ở trên không cắt dấu phẩy đi thì sẽ ảnh hưởng!
			
			if(NUMBER_from!=='' &&  NUMBER_to!=='' &&  NUMBER_from > NUMBER_to){
				msg = 'Thuộc tính '+Utils.XSSEncode(nameAttribute)+': Giá trị "Từ" lớn hơn giá trị "Đến"';
				return 'false__'+msg;
			}
			return 'true__'+dataOfAutoAttribute;
		}else if(valueType=="DATE_TIME"){
			var msg = '';
			var DATETIME_from = $('#'+objectId+'_DATETIME_from').val();
			var DATETIME_to = $('#'+objectId+'_DATETIME_to').val();
			var dataOfAutoAttribute = DATETIME_from +','+DATETIME_to;
			if(DATETIME_from == '' && DATETIME_to == ''){
				msg = 'Bạn chưa nhập giá trị cho thuộc tính '+ Utils.XSSEncode(nameAttribute);
				$('#'+objectId+'_DATETIME_from').focus();
				return 'false__'+msg;
			}
			msg = Utils.getMessageOfInvalidFormatDate(objectId+'_DATETIME_from');
			if(msg.length > 0){
				msg = 'Thuộc tính ' + Utils.XSSEncode(nameAttribute) + ': Giá trị "Từ" không tồn tại hoặc không đúng định dạng dd/mm/yyyy.'; 
				return 'false__'+msg;
			}
			msg = Utils.getMessageOfInvalidFormatDate(objectId+'_DATETIME_to');
			if(msg.length > 0){
				msg = 'Thuộc tính ' + Utils.XSSEncode(nameAttribute) + ': Giá trị "Đến" không tồn tại hoặc không đúng định dạng dd/mm/yyyy.'; 
				return 'false__' + msg;
			}
			if(!Utils.compareDate(DATETIME_from, DATETIME_to)){
				msg = msgErr_fromdate_greater_todate;
				msg = 'Thuộc tính ' + Utils.XSSEncode(nameAttribute) + ': '+ msg;
				return 'false__' + msg;
			}
			return 'true__'+dataOfAutoAttribute;
		}
	},
	
	updateSalerQuantity: function(nodeId, mapId) {
		$(".ErrorMsgStyle").hide();
		var qtt = $("#txt"+nodeId).val().trim();
		qtt = qtt.replace(/,/g, "");
		if (qtt.length > 0 && (isNaN(qtt) || Number(qtt) <= 0)) {
			$("#errMsgShop").html("Số suất đơn vị phải là số dương").show();
			return;
		}
		var amt = $("#txtamt"+nodeId).val().trim();
		amt = amt.replace(/,/g, "");
		if (amt.length > 0 && (isNaN(amt) || Number(amt) <= 0)) {
			$("#errMsgShop").html("Số tiền phải là số dương").show();
			return;
		}
		var num = $("#txtnum"+nodeId).val().trim();
		num = num.replace(/,/g, "");
		if (num.length > 0 && (isNaN(num) || Number(num) <= 0)) {
			$("#errMsgShop").html("Số lượng phải là số dương").show();
			return;
		}
		$.messager.confirm("Xác nhận", "Bạn có muốn cập nhật số suất, số tiền, số lượng cho đơn vị?", function(r) {
			if (r) {
				var params = {
						promotionId: $("#masterDataArea #id").val().trim(),
						//shopId: shopId,
						id: mapId,
						quantity: qtt.length == 0 ? qtt : parseInt(qtt),
						amount: amt.length == 0 ? amt : parseInt(amt),
						number: num.length == 0 ? num : parseInt(num)
				};
				Utils.saveData(params, "/promotion/update-saler-quantity", PromotionProgram._xhrSave, 'errMsgShop', function(data) {
					$("#successMsgShop").html("Lưu dữ liệu thành công").show();
					setTimeout(function() {$("#successMsgShop").hide();},3000);
					$("#exGrid").treegrid("reload");
				}, "loading2");
			}
		});
	},
	
	deleteStaffMap: function(nodeId) {
		$(".ErrorMsgStyle").hide();
		var row = $("#exGrid").treegrid("find", nodeId);
		$.messager.confirm("Xác nhận", "Bạn có muốn xóa số suất NVBH?", function(r) {
			if (r) {
				var params = {
						promotionId: $("#masterDataArea #id").val().trim()
				};
				if (row.attr.isSaler == 1) {
					params.id = row.attr.mapId;
				} else {
					params.shopId = nodeId.replace("sh", "");
				}
				Utils.saveData(params, "/promotion/delete-staff-map", PromotionProgram._xhrSave, 'errMsgShop', function(data) {
					$("#successMsgShop").html("Lưu dữ liệu thành công").show();
					setTimeout(function() {$("#successMsgShop").hide();},3000);
					$("#exGrid").treegrid("reload");
				}, "loading2");
			}
		});
	},
	
	showSalerDlg: function(shopId) {
		if (shopId == undefined || shopId == null) {
			shopId = 0;
		}
		$(".ErrorMsgStyle").hide();
		var html = '<div id="add-shopPopupDiv" style="display:none;">\
			<div id="add-shopPopup">\
			<div class="PopupContentMid">\
				<div class="GeneralForm Search1Form">\
					<label class="LabelStyle" style="width:100px;">Mã NVBH</label>\
					<input id="codeDlg" class="InputTextStyle" style="width:150px;" maxlength="40" />\
					<label class="LabelStyle" style="width:100px;">Tên NVBH</label>\
					<input id="nameDlg" class="InputTextStyle" style="width:230px;" maxlength="100" />\
					<div class="Clear"></div>\
					<div class="BtnCenterSection">\
					    <button class="BtnGeneralStyle" id="btnSearchDlg">Tìm kiếm</button>\
					</div>\
					\
					<div class="GridSection" id="gridDlgDiv">\
						<div id="gridDlg"></div>\
					</div>\
					\
					<p id="erMsgDlg" class="ErrorMsgStyle" style="padding-left:5px;display:none;"></p>\
					<div class="Clear"></div>\
					<div class="BtnCenterSection">\
					    <button class="BtnGeneralStyle" id="btnChooseDlg">Chọn</button>\
						<button class="BtnGeneralStyle" id="btnCloseDlg" onclick="javascript:$(\'#add-shopPopup\').dialog(\'close\');">Đóng</button>\
					</div>\
				</div>\
			</div>\
			</div>\
		</div>';
		$("body").append(html);
		
		$("#add-shopPopup").dialog({
			title: "Chọn NVBH và Số suất KM",
			width: 750,
			heigth: "auto",
			modal: true, close: false, cache: false,
			onOpen: function() {
				$("#add-shopPopup").addClass("easyui-dialog");
				$("#add-shopPopup #codeDlg").focus();
				
				$("#add-shopPopup #gridDlg").treegrid({
					url: "/promotion/search-saler-dlg?promotionId="+$("#masterDataArea #id").val().trim()+"&shopId="+Number(shopId),
					rownumbers: false,
					width: $("#add-shopPopup #gridDlgDiv").width(),
					height: 350,
					fitColumns: true,
					idField: "nodeId",
					treeField: "text",
					selectOnCheck: false,
					checkOnSelect: false,
					columns: [[
						{field:"text", title:"Mã Đơn vị", sortable: false, resizable: false, width: 250, align:"left", formatter:CommonFormatter.formatNormalCell},
						{field:"quantity", title:"Số suất", sortable: false, resizable: false, width: 90, fixed:true, align:"left", formatter: function(v, r) {
							if (r.attr.isSaler) {
								/*if (r.attr.isExists) {
									return "<input type='text' id='txt-p"+r.attr.id+"' class='qttClass vinput-money' maxlength='11' disabled='disabled' exists='1' />";
								}*/
								return "<input type='text' id='txt-p"+r.attr.id+"' class='qttClass vinput-money' style='width:75px;' maxlength='11' exists='0' />";
							}
							return "";
						}},
						{field:"amountMax", title:"Số tiền", sortable: false, resizable: false, width: 90, fixed:true, align:"left", formatter: function(v, r) {
							if (r.attr.isSaler) {
								return "<input type='text' id='txt-a"+r.attr.id+"' class='qttClass vinput-money' style='width:75px;' maxlength='11' exists='0' />";
							}
							return "";
						}},
						{field:"numMax", title:"Số lượng", sortable: false, resizable: false, width: 90, fixed:true, align:"left", formatter: function(v, r) {
							if (r.attr.isSaler) {
								return "<input type='text' id='txt-n"+r.attr.id+"' class='qttClass vinput-money' style='width:75px;' maxlength='11' exists='0' />";
							}
							return "";
						}},
						{field:"ck", title:"", sortable: false, resizable: false, width: 30, fixed:true, align:"center", formatter: function(v, r) {
							var pId = r.attr.parentId;
							if (pId == undefined || pId == null) {
								pId = 0;
							}
							/*if (r.attr.isExists) {
								return "<input type='checkbox' id='ck"+r.nodeId+"' value='"+r.attr.id+"' disabled='disabled' exists='1' parentId='"+pId+"' onchange='PromotionProgram.onCheckShop(this);' />";
							}*/
							return "<input type='checkbox' id='ck"+r.nodeId+"' value='"+r.attr.id+"' exists='0' parentId='"+pId+"' onchange='PromotionProgram.onCheckShop(this);' />";
						}}
					]],
					onLoadSuccess: function(data) {
						$("#add-shopPopup #gridDlg").datagrid("resize");
//						Utils.bindFormatOnTextfieldInputCss('qttClass', Utils._TF_NUMBER, "#add-shopPopup");
//						$('#add-shopPopup .qttClass').each(function() {
//				      		VTUtilJS.formatCurrencyFor($(this).attr('id'));
//				      	});
						$('.vinput-money').each(function() {
							VTUtilJS.bindFormatOnTextfield($(this), VTUtilJS._TF_NUMBER_COMMA);
							VTUtilJS.formatCurrencyFor($(this));
						});
						setTimeout(function(){
							var hDlg=parseInt($("#add-shopPopup").parent().height());
							var hW=parseInt($(window).height());
							var d=hW-hDlg;
							d=d/2+document.documentElement.scrollTop;
							if (d < 0) { d = 0; }
							$("#add-shopPopup").parent().css('top',d);
			    		},1000);
					},
					onBeforeLoad: function(r, p) {
						if (p && p.id) {
							p.id = p.id.replace("sh", "");
						}
					}
				});
				
				$("#add-shopPopup #btnSearchDlg").click(function() {
					var p = {
							code: $("#add-shopPopup #codeDlg").val().trim(),
							name: $("#add-shopPopup #nameDlg").val().trim()
					};
					$("#add-shopPopup #gridDlg").treegrid("reload", p);
				});
				$("#add-shopPopup #codeDlg").parent().keyup(function(event) {
					if (event.keyCode == keyCodes.ENTER) {
						$("#add-shopPopup #btnSearchDlg").click();
					}
				});
				$("#add-shopPopup #btnChooseDlg").click(function() {
					if ($("#add-shopPopup input[id^=ckst]:checked").length == 0) {
						$("#erMsgDlg").html("Không có nhân viên nào được chọn").show();
						return;
					}
					var lstTmp = new Map();
					var lstTmpAmt = new Map();
					var lstTmpNum = new Map();
					var v, qtt, amt, num;
					var isUpdate = true;
					$("#add-shopPopup input[id^=ckst]:checked").each(function() {
						v = $(this).val().trim();
						if ($("#txt-p"+v).length == 1) {
							qtt = $("#txt-p"+v).val().trim();
						} else {
							qtt = "";
						}
						qtt = qtt.replace(/,/g, "");
						if (qtt.length > 0 && (isNaN(qtt) || Number(qtt) <= 0)) {
							$("#erMsgDlg").html("Số suất phải là số nguyên dương").show();
							lstTmp = null;
							isUpdate = false;
							return;
						}
						v = v.replace("st", "");
						lstTmp.put(v, qtt);
						
						//---Amount
						if ($("#txt-a"+v).length == 1) {
							amt = $("#txt-a"+v).val().trim();
						} else {
							amt = "";
						}
						amt = amt.replace(/,/g, "");
						
						if (amt.length > 0 && (isNaN(amt) || Number(amt) <= 0)) {
							$("#erMsgDlg").html("Số tiền phải là số nguyên dương").show();
							isUpdate = false;
							return;
						}
						lstTmpAmt.put(v, amt);
						
						//---Number
						if ($("#txt-n"+v).length == 1) {
							num = $("#txt-n"+v).val().trim();
						} else {
							num = "";
						}
						num = num.replace(/,/g, "");
						if (num.length > 0 && (isNaN(num) || Number(num) <= 0)) {
							$("#erMsgDlg").html("Số lượng phải là số nguyên dương").show();
							isUpdate = false;
							return;
						}
						lstTmpNum.put(v, num);
					});
					if (isUpdate) {
						PromotionProgram.addSalerQtt(lstTmp, lstTmpAmt, lstTmpNum);						
					}
				});
			},
			onClose: function() {
				$("#add-shopPopup").dialog("destroy");
				$("#add-shopPopupDiv").remove();
			}
		});
	},
	
	addSalerQtt: function(lstStaffTmp, lstTmpAmt, lstTmpNum) {
		$(".ErrorMsgStyle").hide();
		$.messager.confirm("Xác nhận", "Bạn có muốn thêm NVBH?", function(r) {
			if (r) {
				var zezo = 0;
				var arrQttHL = [];
				var arrAmtHL = [];
				var arrNumHL = [];
				var arrIdHl = lstStaffTmp.keyArray;
				for (var i = 0, size = arrIdHl.length; i < size; i++) {
					var valMap = lstStaffTmp.get(arrIdHl[i]);
					if (valMap != undefined && valMap != null && valMap.length > 0) {
						arrQttHL.push(valMap);
					} else {
						arrQttHL.push(zezo);
					}
					valMap = lstTmpAmt.get(arrIdHl[i]);
					if (valMap != undefined && valMap != null && valMap.length > 0) {
						arrAmtHL.push(valMap);
					} else { 
						arrAmtHL.push(zezo);
					}
					valMap = lstTmpNum.get(arrIdHl[i]);
					if (valMap != undefined && valMap != null && valMap.length > 0) {
						arrNumHL.push(valMap);
					} else {
						arrNumHL.push(zezo);
					}
				}
				
				var params = {
						promotionId: $("#masterDataArea #id").val().trim(),
						lstQtt: arrQttHL,
						lstId: arrIdHl,
						lstAmt: arrAmtHL,
						lstNum: arrNumHL
				};
				$("#add-shopPopup").dialog("close");
				Utils.saveData(params, "/promotion/add-saler", PromotionProgram._xhrSave, 'errMsgShop', function(data) {
					$("#successMsgShop").html("Lưu dữ liệu thành công").show();
					setTimeout(function() {$("#successMsgShop").hide();},3000);
					$("#exGrid").treegrid("reload");
				}, "loading2");
			}
		});
	},
	
	exportPromotionStaff: function() {
		$(".ErrorMsgStyle").hide();
		if (PromotionProgram._promotionStaffParams == null) {
			PromotionProgram._promotionStaffParams = {};
			PromotionProgram._promotionStaffParams.promotionId = $("#masterDataArea #id").val().trim();
		}
		ReportUtils.exportReport("/promotion/export-promotion-staff", PromotionProgram._promotionStaffParams, "errMsgShop");
	},
	
	/**
	 * Export KH tham gia CTKM
	 * 
	 * @author hunglm16
	 * @since 24/09/2015
	 */
	exportPromotionCustomer: function() {
		$(".ErrorMsgStyle").hide();
		if (PromotionProgram._promotionStaffParams == null) {
			PromotionProgram._promotionStaffParams = {};
			PromotionProgram._promotionStaffParams.promotionId = $("#masterDataArea #id").val().trim();
			PromotionProgram._promotionStaffParams.code = $('#customerCode').val().trim();
			PromotionProgram._promotionStaffParams.name = $('#customerName').val().trim();
			PromotionProgram._promotionStaffParams.address = $('#address').val().trim();
		}
		ReportUtils.exportReport("/promotion/export-promotion-customer", PromotionProgram._promotionStaffParams, "errMsgShop");
	},
	
	importPromotionStaff: function() {
		$('.ErrorMsgStyle').hide();
		VTUtilJS.importExcelUtils(function(data) {
			$("#exGrid").treegrid("reload");
			$("#errExcelMsgShop").html(data.message).show();
		}, "importFrm", "excelFile", null, "errExcelMsgShop");
	},
	
	importPromotionCustomer: function() {
		$('.ErrorMsgStyle').hide();
		VTUtilJS.importExcelUtils(function(data) {
			$("#exGrid").treegrid("reload");
			$("#errExcelMsgShop").html(data.message).show();
		}, "importFrm", "excelFile", null, "errExcelMsgShop");
	},
	
	deleteHtmlLevel : function(tableLevelDiv, groupMuaId, groupKMId, mapId, levelMuaId, levelKMId) {
		if($('#statusHidden').val() == 0 || $('#statusHidden').val() == 1){
			return;
		}
		$.messager.confirm("Xác nhận", "Bạn có muốn xóa thông tin mức không?", function(r) {
			if(r) {
				VTUtilJS.postFormJson({groupMuaId : groupMuaId, groupKMId : groupKMId, levelMuaId : levelMuaId, levelKMId : levelKMId, mappingId : mapId}, '/promotion/new-delete-level', function(data){
					if(!data.error) {
						var parentDiv = $('#'+tableLevelDiv).parent().parent().parent();
						$('#'+tableLevelDiv).panel('destroy');
						parentDiv.remove();
						if($($('#mappingGrid tbody')[0]).children().length == 0) {
							$($('#mappingGrid tbody')[0]).append('<tr class="NewLevel">\
											<td>\
											<div class="TableLevel" id="tableLevel" style="width:100%; height:\'auto\';padding:10px;" title="">\
									    </div>\
								</td>\
							</tr>');
							$('#tableLevel').panel({
								width:'95%',
								height:'auto',
								collapsible:true,
								title:' ',
								tools:[{
									iconCls:'iconAdd',
									handler:function(){
										PromotionProgram.addHtmlLevel('tableLevel', groupMuaId, groupKMId);
									}
								}],
								onLoad:function(){
									
								}
							});
						}
					}
				});
			}
		});
	},
	editHtmlLevel : function(tableLevelDiv, groupMuaId, groupKMId, mapId, levelMuaId, levelKMId, levelCode, stt) {
		if($('#statusHidden').val() == 0 || $('#statusHidden').val() == 1){
			return;
		}
		var html = '<div id="popup-container"><div id="popup-add-level" class="PopupContentMid">\
			<div class="GeneralForm Search1Form" id="form-data">\
			<label style="width: 100px;" class="LabelStyle Label1Style">Mã mức<span class="ReqiureStyle">(*)</span></label>\
			<input type="text" value="'+levelCode+'" call-back="VTValidateUtils.getMessageOfSpecialCharactersValidate(\'levelCode\', \'Mã mức\', Utils._CODE)" class="InputTextStyle InputText1Style" style="width: 145px;" maxlength="50" id="levelCode"> \
			<label style="width: 100px;" class="LabelStyle Label1Style">Số thứ tự<span class="ReqiureStyle">(*)</span></label> \
			<input type="text" disabled="disabled" call-back="VTValidateUtils.getMessageOfRequireCheck(\'stt\', \'Số thứ tự\', false, true)" value="'+((stt == null || stt == undefined || stt == 0)?'' : stt)+'" class="InputTextStyle InputText1Style" style="width: 145px;" maxlength="3" id="stt">\
			<input type="hidden" id="groupMuaId" value="'+groupMuaId+'">\
			<input type="hidden" id="groupKMId" value="'+groupKMId+'">\
			<input type="hidden" id="levelMuaId" value="'+levelMuaId+'">\
			<input type="hidden" id="levelKMId" value="'+levelKMId+'">\
			<div class="Clear"></div>\
			<div class="BtnCenterSection">\
			<button class="BtnGeneralStyle" id="btn-update">Cập nhật</button>\
			</div>\
			<p class="ErrorMsgStyle SpriteErr" style="display: none;" id="errMsgLevelDlg"></p>\
			<p class="SuccessMsgStyle" style="display: none;" id="successMsgLevelDlg"></p>\
			</div></div></div>';
		$("body").append(html);
		$('#popup-add-level').dialog({
			title: "Chỉnh sửa mức CTKM",
			width: 600,
			heigth: "auto",
			modal: true, close: false, cache: false,
			onOpen: function() {
				VTUtilJS.bindFormatOnTextfield('stt', VTUtilJS._TF_NUMBER);
				$('#btn-update').bind('click', function() {
					var mm = $('#form-data #levelCode').val();
					if(mm == null || mm == undefined || mm.trim().length == 0) {
						$('#errMsgLevelDlg').html('Mã mức không được để trống').show();
						return;
					}
					VTUtilJS.postFormJson('form-data', '/promotion/new-add-level', function(data){
						if(!data.error) {
							var parentDiv = $('#'+tableLevelDiv).parent().parent().parent();
							$('#popup-add-level').dialog('close');
							$('#'+tableLevelDiv).panel('destroy');
							parentDiv.html('');
							parentDiv.append('<td>\
									<p class="ErrorMsgStyle SpriteErr" style="display: none;" id="errorMsgDistribute'+(VTUtilJS.isNullOrEmpty(data.newMapping.levelCode) ? data.newMapping.mapId : data.newMapping.levelCode+data.newMapping.mapId)+'">\
									<p id="successMsgDistribute'+(VTUtilJS.isNullOrEmpty(data.newMapping.levelCode) ? data.newMapping.mapId : data.newMapping.levelCode+data.newMapping.mapId)+'" class="SuccessMsgStyle" style="display: none"></p>\
									<div class="TableLevel" id="tableLevel'+data.newMapping.mapId+'" style="width:100%; height:\'auto\';padding:10px;" title="">\
								<table style="width: 100%;">\
						    	<tr>\
						    		<td style="width: 50%; vertical-align: top;">\
						    			<table id="tableSubLevelMua'+(VTUtilJS.isNullOrEmpty(data.newMapping.levelCode) ? data.newMapping.mapId : data.newMapping.levelCode+data.newMapping.mapId)+'" style="border: 1px solid black; vertical-align: top;width: 100%">\
						    				<tbody>\
						    				</tbody>\
						    			</table>\
						    		</td>\
						    		<td style="width: 50%; vertical-align: top;">\
						    			<table id="tableSubLevelKM'+(VTUtilJS.isNullOrEmpty(data.newMapping.levelCode) ? data.newMapping.mapId : data.newMapping.levelCode+data.newMapping.mapId)+'" style="border: 1px solid black; vertical-align: top;width: 100%">\
						    				<tbody>\
						    				</tbody>\
						    			</table>\
						    		</td>\
						    	</tr>\
					    	</table>\
								    </div>\
							</td>');
							VTUtilJS.bindFormatOnTextfield('sttMua_'+(VTUtilJS.isNullOrEmpty(data.newMapping.levelCode) ? data.newMapping.mapId : data.newMapping.levelCode+data.newMapping.mapId), VTUtilJS._TF_NUMBER);
							var tools = new Array();
							tools.push({
								iconCls:'iconAdd',
								handler:function(){
									PromotionProgram.addHtmlLevel('tableLevel'+data.newMapping.mapId+'', groupMuaId, groupKMId);
								}
							});
							tools.push({
								iconCls:'iconEdit',
								handler:function(){
									PromotionProgram.editHtmlLevel('tableLevel'+data.newMapping.mapId+'', groupMuaId, groupKMId, data.newMapping.mapId, data.newMapping.levelMuaId, data.newMapping.levelKMId, (VTUtilJS.isNullOrEmpty(data.newMapping.levelCode) ? '' : data.newMapping.levelCode), (data.newMapping.stt == null || data.newMapping.stt == undefined) ? 0 : data.newMapping.stt);
								}
							});
							tools.push({
								iconCls:'iconDelete',
								handler:function(){
									PromotionProgram.deleteHtmlLevel('tableLevel'+data.newMapping.mapId+'', groupMuaId, groupKMId, data.newMapping.mapId, data.newMapping.levelMuaId, data.newMapping.levelKMId);
								}
							});
							tools.push({
								iconCls:'iconCopy',
								handler:function(){
									PromotionProgram.copyHtmlLevel(data.newMapping.mapId, groupMuaId, groupKMId);
								}
							});
							tools.push({
								iconCls:'iconSave',
								handler:function(){
									PromotionProgram.saveHtmlLevel((VTUtilJS.isNullOrEmpty(data.newMapping.levelCode) ? data.newMapping.mapId : data.newMapping.levelCode+data.newMapping.mapId), groupMuaId, groupKMId, data.newMapping.mapId, data.newMapping.levelMuaId, data.newMapping.levelKMId);
								}
							});
							
							$('#tableLevel'+data.newMapping.mapId).panel({
								width:'95%',
								height:'auto',
								collapsible:true,
								title: VTUtilJS.isNullOrEmpty(data.newMapping.levelCode) ? 'STT : ' + data.newMapping.stt : 'Mã : ' + Utils.XSSEncode(data.newMapping.levelCode) + ' - ' + 'STT : ' + data.newMapping.stt,
								tools:tools
							});
							if(data.newMapping.listExLevelMua != null && data.newMapping.listExLevelMua != undefined && $.isArray(data.newMapping.listExLevelMua)) {
								PromotionProgram.addHtmlSubRowMua('tableSubLevelMua'+(VTUtilJS.isNullOrEmpty(data.newMapping.levelCode) ? data.newMapping.mapId : Utils.XSSEncode(data.newMapping.levelCode) + data.newMapping.mapId), VTUtilJS.isNullOrEmpty(data.newMapping.levelCode) ? data.newMapping.mapId : data.newMapping.levelCode+data.newMapping.mapId, data.newMapping.listExLevelMua);
							}
							if(data.newMapping.listExLevelKM != null && data.newMapping.listExLevelKM != undefined && $.isArray(data.newMapping.listExLevelKM)) {
								PromotionProgram.addHtmlSubRowKM('tableSubLevelKM'+(VTUtilJS.isNullOrEmpty(data.newMapping.levelCode) ? data.newMapping.mapId : Utils.XSSEncode(data.newMapping.levelCode) + data.newMapping.mapId), VTUtilJS.isNullOrEmpty(data.newMapping.levelCode) ? data.newMapping.mapId : data.newMapping.levelCode+data.newMapping.mapId, data.newMapping.listExLevelKM);
							}
							Utils.functionAccessFillControl('divTab', function(data) {
								//Xu ly cac su kien lien quan den ctl phan quyen
							});
						} else {
							$('#errMsgLevelDlg').html(data.errMsg).show();
						}
					});
				});
			},
			onClose: function() {
				$("#popup-add-level").dialog("destroy");
				$("#popup-container").remove();
			}
		});
	},
	addHtmlLevel : function(tableLevelDiv, groupMuaId, groupKMId) {
		$('.ErrorMsgStyle').hide();
		VTUtilJS.getFormJson({groupMuaId : groupMuaId}, '/promotion/new-get-max-order', function(__data){
			if(__data.error) {
				return;
			}
			if($('#statusHidden').val() == 0 || $('#statusHidden').val() == 1){
				return;
			}
			var html = '<div id="popup-container"><div id="popup-add-level" class="PopupContentMid">\
				<div class="GeneralForm Search1Form" id="form-data">\
				<label style="width: 100px;" class="LabelStyle Label1Style">Mã mức<span class="ReqiureStyle">(*)</span></label>\
				<input type="text" call-back="VTValidateUtils.getMessageOfSpecialCharactersValidate(\'levelCode\', \'Mã mức\', Utils._CODE)" class="InputTextStyle InputText1Style" style="width: 145px;" maxlength="50" id="levelCode"> \
				<label style="width: 100px;" class="LabelStyle Label1Style">Số thứ tự<span class="ReqiureStyle">(*)</span></label> \
				<input type="text" value="'+__data.stt+'" call-back="VTValidateUtils.getMessageOfRequireCheck(\'stt\', \'Số thứ tự\', false, true);VTValidateUtils.getMessageOfInvaildInteger(\'stt\', \'Số thự tự\');VTValidateUtils.getMessageOfNegativeNumberCheck(\'stt\', \'Số thự tự\');" class="InputTextStyle InputText1Style" style="width: 145px;" maxlength="3" id="stt">\
				<input type="hidden" id="groupMuaId" value="'+groupMuaId+'">\
				<input type="hidden" id="groupKMId" value="'+groupKMId+'">\
				<div class="Clear"></div>\
				<div class="BtnCenterSection">\
				<button class="BtnGeneralStyle" id="btn-update">Cập nhật</button>\
				</div>\
				<p class="ErrorMsgStyle SpriteErr" style="display: none;" id="errMsgLevelDlg"></p>\
				<p class="SuccessMsgStyle" style="display: none;" id="successMsgLevelDlg"></p>\
				</div></div></div>';
			$("body").append(html);
			$('#popup-add-level').dialog({
				title: "Thêm mức CTKM",
				width: 600,
				heigth: "auto",
				modal: true, close: false, cache: false,
				onOpen: function() {
					VTUtilJS.bindFormatOnTextfield('stt', VTUtilJS._TF_NUMBER);
					$('#btn-update').bind('click', function() {
						$('#errMsgLevelDlg').html('').hide();
						var mm = $('#form-data #levelCode').val();
						if(mm == null || mm == undefined || mm.trim().length == 0) {
							$('#errMsgLevelDlg').html('Mã mức không được để trống').show();
							return;
						}
						var stt = $('#form-data #stt').val();
						if(stt == null || stt == undefined || isNaN(stt) || Number(stt) <= 0) {
							if(formData == null) {
								$('#errMsgLevelDlg').html('Số thứ tự không hợp lệ').show();
								return;
							}
						}
						var formData = VTUtilJS.getFormData('form-data');
						if(formData == null) {
							var errMsg = $('#errorMsg').html();
							$('#errorMsg').html('').hide();
							$('#errMsgLevelDlg').html(errMsg).show();
							return;
						}
						$.messager.confirm("Xác nhận", "Bạn có muốn thêm mức?", function(r) {
							if(r) {
								VTUtilJS.postFormJson('form-data', '/promotion/new-add-level', function(data){
									if(!data.error) {
										$('#popup-add-level').dialog('close');
										var trDiv = $('#'+tableLevelDiv).parent().parent().parent();//tr
										var tbodyDiv = $('#'+tableLevelDiv).parent().parent().parent().parent();//tbody
										if(trDiv.hasClass('NewLevel')) {
											$('#'+tableLevelDiv).panel('destroy');
											trDiv.remove();
										}
										tbodyDiv.append('<tr>\
														<td>\
														<p class="ErrorMsgStyle SpriteErr" style="display: none;" id="errorMsgDistribute'+(VTUtilJS.isNullOrEmpty(data.newMapping.levelCode) ? data.newMapping.mapId : Utils.XSSEncode(data.newMapping.levelCode) + data.newMapping.mapId)+'">\
														<p id="successMsgDistribute'+(VTUtilJS.isNullOrEmpty(data.newMapping.levelCode) ? data.newMapping.mapId : Utils.XSSEncode(data.newMapping.levelCode) + data.newMapping.mapId)+'" class="SuccessMsgStyle" style="display: none"></p>\
														<div class="TableLevel" id="tableLevel'+(data.newMapping.mapId)+'" style="width:100%; height:\'auto\';padding:10px;" title="">\
												<table style="width: 100%;">\
										    	<tr>\
										    		<td style="width: 50%; vertical-align: top;">\
										    			<table id="tableSubLevelMua'+(VTUtilJS.isNullOrEmpty(data.newMapping.levelCode) ? data.newMapping.mapId : Utils.XSSEncode(data.newMapping.levelCode) + data.newMapping.mapId)+'" style="border: 1px solid black; vertical-align: top;width: 100%">\
										    				<tbody>\
										    				</tbody>\
										    			</table>\
										    		</td>\
										    		<td style="width: 50%; vertical-align: top;">\
										    			<table id="tableSubLevelKM'+(VTUtilJS.isNullOrEmpty(data.newMapping.levelCode) ? data.newMapping.mapId : Utils.XSSEncode(data.newMapping.levelCode) + data.newMapping.mapId)+'" style="border: 1px solid black; vertical-align: top;width: 100%">\
										    				<tbody>\
										    				</tbody>\
										    			</table>\
										    		</td>\
										    	</tr>\
									    	</table>\
												    </div>\
											</td>\
										</tr>');
										VTUtilJS.bindFormatOnTextfield('sttMua_'+(VTUtilJS.isNullOrEmpty(data.newMapping.levelCode) ? data.newMapping.mapId : Utils.XSSEncode(data.newMapping.levelCode) + data.newMapping.mapId), VTUtilJS._TF_NUMBER);
										
										var tools = new Array();
										tools.push({
											iconCls:'iconAdd ' + data.newMapping.mapId+'',
											handler:function(){
												PromotionProgram.addHtmlLevel('tableLevel'+data.newMapping.mapId+'', groupMuaId, groupKMId);
											}
										});
										tools.push({
											iconCls:'iconEdit '+data.newMapping.mapId+'',
											handler:function(){
												PromotionProgram.editHtmlLevel('tableLevel'+(data.newMapping.mapId), groupMuaId, groupKMId, data.newMapping.mapId, data.newMapping.levelMuaId, data.newMapping.levelKMId, (VTUtilJS.isNullOrEmpty(data.newMapping.levelCode) ? '' : data.newMapping.levelCode), (data.newMapping.stt == null || data.newMapping.stt == undefined) ? 0 : data.newMapping.stt);
											}
										});
										tools.push({
											iconCls:'iconDelete '+data.newMapping.mapId+'',
											handler:function(){
												PromotionProgram.deleteHtmlLevel('tableLevel'+(data.newMapping.mapId), groupMuaId, groupKMId, data.newMapping.mapId, data.newMapping.levelMuaId, data.newMapping.levelKMId);
											}
										});
										tools.push({
											iconCls:'iconCopy '+data.newMapping.mapId+'',
											handler:function(){
												PromotionProgram.copyHtmlLevel(data.newMapping.mapId, groupMuaId, groupKMId);
											}
										});
										tools.push({
											iconCls:'iconSave '+data.newMapping.mapId+'',
											handler:function(){
												PromotionProgram.saveHtmlLevel((VTUtilJS.isNullOrEmpty(data.newMapping.levelCode) ? data.newMapping.mapId : Utils.XSSEncode(data.newMapping.levelCode) + data.newMapping.mapId), groupMuaId, groupKMId, data.newMapping.mapId, data.newMapping.levelMuaId, data.newMapping.levelKMId);
											}
										});
										
										$('#tableLevel'+data.newMapping.mapId).panel({
											width:'95%',
											height:'auto',
											collapsible:true,
											title: VTUtilJS.isNullOrEmpty(data.newMapping.levelCode) ? 'STT : ' + data.newMapping.stt : 'Mã : ' + Utils.XSSEncode(data.newMapping.levelCode) + ' - ' + 'STT : ' + data.newMapping.stt,
											tools:tools,
											onLoad:function(){
											}
										});
										
										if(data.newMapping.listExLevelMua != null && data.newMapping.listExLevelMua != undefined && $.isArray(data.newMapping.listExLevelMua)) {
											PromotionProgram.addHtmlSubRowMua('tableSubLevelMua'+(VTUtilJS.isNullOrEmpty(data.newMapping.levelCode) ? data.newMapping.mapId : Utils.XSSEncode(data.newMapping.levelCode) + data.newMapping.mapId), VTUtilJS.isNullOrEmpty(data.newMapping.levelCode) ? data.newMapping.mapId : data.newMapping.levelCode+data.newMapping.mapId, data.newMapping.listExLevelMua);
										}
										if(data.newMapping.listExLevelKM != null && data.newMapping.listExLevelKM != undefined && $.isArray(data.newMapping.listExLevelKM)) {
											PromotionProgram.addHtmlSubRowKM('tableSubLevelKM'+(VTUtilJS.isNullOrEmpty(data.newMapping.levelCode) ? data.newMapping.mapId : Utils.XSSEncode(data.newMapping.levelCode) + data.newMapping.mapId), VTUtilJS.isNullOrEmpty(data.newMapping.levelCode) ? data.newMapping.mapId : data.newMapping.levelCode+data.newMapping.mapId, data.newMapping.listExLevelKM);
										}
										Utils.functionAccessFillControl('divTab', function(data) {
											//Xu ly cac su kien lien quan den ctl phan quyen
										});
									} else {
										$('#errMsgLevelDlg').html(data.errMsg).show();
									}
								}, "errMsgLevelDlg");
							}
						});
					});
				},
				onClose: function() {
					$("#popup-add-level").dialog("destroy");
					$("#popup-container").remove();
				}
			});
		});
	},
	addSubProductLevel : function(masterGridId, index, detailGridId) {
		if($('#statusHidden').val() == 0 || $('#statusHidden').val() == 1){
			return;
		}
		$('.ErrorMsgStyle').hide();
		var ddv = $('#'+masterGridId).datagrid('getRowDetail', index).find('div.ddv#'+detailGridId);
		if(masterGridId.indexOf('gridSublevelMua') == 0 && ($('#typeCode').val() == 'ZV01' 
			|| $('#typeCode').val() == 'ZV02' 
			|| $('#typeCode').val() == 'ZV03' 
			|| $('#typeCode').val() == 'ZV04' 
			|| $('#typeCode').val() == 'ZV05' 
			|| $('#typeCode').val() == 'ZV06')
			) {
			if(ddv.datagrid('getRows').length == 1) {
				return;
			}
		}
		if(masterGridId.indexOf('gridSublevelMua') == 0 && ($('#typeCode').val() == 'ZV19'
			|| $('#typeCode').val() == 'ZV20'
			|| $('#typeCode').val() == 'ZV21'
			)) {
			return;
		}
		if(masterGridId.indexOf('gridSublevelKM') == 0 && ($('#typeCode').val() == 'ZV01'
			|| $('#typeCode').val() == 'ZV04'
			|| $('#typeCode').val() == 'ZV07'
			|| $('#typeCode').val() == 'ZV10'
			|| $('#typeCode').val() == 'ZV13'
			|| $('#typeCode').val() == 'ZV16'
			|| $('#typeCode').val() == 'ZV19'
			|| $('#typeCode').val() == 'ZV02'
			|| $('#typeCode').val() == 'ZV05'
			|| $('#typeCode').val() == 'ZV08'
			|| $('#typeCode').val() == 'ZV11'
			|| $('#typeCode').val() == 'ZV14'
			|| $('#typeCode').val() == 'ZV17'
			|| $('#typeCode').val() == 'ZV20'
			)) {
			return;
		}
		if(masterGridId.indexOf('gridSublevelMua') == 0) {
			for(var i = 0; i < ddv.datagrid('getRows').length; i++) {
				var row = ddv.datagrid('getRows')[i];
				if(VTUtilJS.isNullOrEmpty(row.productCode)) {
					return;
				}
				if(($('#typeCode').val() != 'ZV07' && $('#typeCode').val() != 'ZV08' && $('#typeCode').val() != 'ZV09' && $('#typeCode').val() != 'ZV10' && $('#typeCode').val() != 'ZV11' && $('#typeCode').val() != 'ZV12') && (VTUtilJS.isNullOrEmpty(row.quantity) || isNaN(row.quantity) || Number(row.quantity) == 0) && (VTUtilJS.isNullOrEmpty(row.amount) || isNaN(row.amount) || Number(row.amount) == 0)) {
					return;
				}
			}
		} else if(masterGridId.indexOf('gridSublevelKM') == 0) {
			for(var i = 0; i < ddv.datagrid('getRows').length; i++) {
				var row = ddv.datagrid('getRows')[i];
				if(VTUtilJS.isNullOrEmpty(row.productCode)) {
					return;
				}
				if(($('#typeCode').val() != 'ZV22' && $('#typeCode').val() != 'ZV23' && $('#typeCode').val() != 'ZV24') && (VTUtilJS.isNullOrEmpty(row.quantity) || isNaN(row.quantity) || Number(row.quantity) == 0) && (VTUtilJS.isNullOrEmpty(row.amount) || isNaN(row.amount) || Number(row.amount) == 0)) {
					return;
				}
			}
		}
		ddv.datagrid('acceptChanges');
		ddv.datagrid('appendRow', {isRequired:0});
		ddv.datagrid('beginEdit', ddv.datagrid('getRows').length-1);
		$('#'+masterGridId).datagrid('getRows')[index].listSubLevel = ddv.datagrid('getRows');
		setTimeout(function() {
			$('#'+masterGridId).datagrid('fixRowHeight',index);
			$('#'+masterGridId).datagrid('fixDetailRowHeight',index);
		}, 500);
	},
	addHtmlSubRowMua: function(divTable, levelCode, subLevel) {
		$('.ErrorMsgStyle').hide();
		if ($('#' + divTable + ' tbody tr').length > 1) {
			if (($('#gridSublevelMua'+levelCode).length > 0 && $('#gridSublevelMua'+levelCode).datagrid('getRows').length > 0) && ($('#typeCode').val() == 'ZV01'
				|| $('#typeCode').val() == 'ZV02'
				|| $('#typeCode').val() == 'ZV03'
				|| $('#typeCode').val() == 'ZV04'
				|| $('#typeCode').val() == 'ZV05'
				|| $('#typeCode').val() == 'ZV06'
				|| $('#typeCode').val() == 'ZV07'
				|| $('#typeCode').val() == 'ZV08'
				|| $('#typeCode').val() == 'ZV09'
				|| $('#typeCode').val() == 'ZV10'
				|| $('#typeCode').val() == 'ZV11'
				|| $('#typeCode').val() == 'ZV12'
				|| $('#typeCode').val() == 'ZV13'
				|| $('#typeCode').val() == 'ZV14'
				|| $('#typeCode').val() == 'ZV15'
				|| $('#typeCode').val() == 'ZV16'
				|| $('#typeCode').val() == 'ZV17'
				|| $('#typeCode').val() == 'ZV18'
				|| $('#typeCode').val() == 'ZV19'
				|| $('#typeCode').val() == 'ZV20'
				|| $('#typeCode').val() == 'ZV21')) { 
				return;
			}
			//da co grid roi
			var rows = $('#gridSublevelMua'+levelCode).datagrid('getRows');
			rows.push({});
			$('#gridSublevelMua'+levelCode).datagrid('loadData', rows);
		} else {
			//chua co grid
			$('#'+divTable+' tbody').append('<tr>\
					<td colspan="5"><div id="gridSublevelMua'+levelCode+'" style="width:100%; height:\'auto\'; padding:10px;"></div></td>\
					</tr>');
			if (subLevel == undefined || subLevel == null || !$.isArray(subLevel) || subLevel.length == 0) {
				subLevel = [{listSubLevel:[]}];
			}
			var disMinQuantityMua = false;
			var disMinAmountMua = false;
			var disQuantityMua = false;
			var disAmountMua = false;
			var disPercentKM = false;
			var disMaxQuantityKM = false;
			var disMaxAmountKM = false;
			var disQuantityKM = false;
			var disAmountKM = false;
			if ($('#typeCode').val() == 'ZV01') {
				disMinQuantityMua = true;
				disMinAmountMua = true;
				disQuantityMua = false;
				disAmountMua = true;
				disPercentKM = false;
				disMaxQuantityKM = true;
				disMaxAmountKM = true;
				disQuantityKM = true;
				disAmountKM = true;
			} else if ($('#typeCode').val() == 'ZV02') {
				disMinQuantityMua = true;
				disMinAmountMua = true;
				disQuantityMua = false;
				disAmountMua = true;
				disPercentKM = true;
				disMaxQuantityKM = true;
				disMaxAmountKM = false;
				disQuantityKM = true;
				disAmountKM = true;
			} else if ($('#typeCode').val() == 'ZV03') {
				disMinQuantityMua = true;
				disMinAmountMua = true;
				disQuantityMua = false;
				disAmountMua = true;
				disPercentKM = true;
				disMaxQuantityKM = true;
				disMaxAmountKM = true;
				disQuantityKM = false;
				disAmountKM = true;
			} else if ($('#typeCode').val() == 'ZV04') {
				disMinQuantityMua = true;
				disMinAmountMua = true;
				disQuantityMua = true;
				disAmountMua = false;
				disPercentKM = false;
				disMaxQuantityKM = true;
				disMaxAmountKM = true;
				disQuantityKM = true;
				disAmountKM = true;
			} else if ($('#typeCode').val() == 'ZV05') {
				disMinQuantityMua = true;
				disMinAmountMua = true;
				disQuantityMua = true;
				disAmountMua = false;
				disPercentKM = true;
				disMaxQuantityKM = true;
				disMaxAmountKM = false;
				disQuantityKM = true;
				disAmountKM = true;
			} else if ($('#typeCode').val() == 'ZV06') {
				disMinQuantityMua = true;
				disMinAmountMua = true;
				disQuantityMua = true;
				disAmountMua = false;
				disPercentKM = true;
				disMaxQuantityKM = true;
				disMaxAmountKM = true;
				disQuantityKM = false;
				disAmountKM = true;
			} else if ($('#typeCode').val() == 'ZV07') {
				disMinQuantityMua = false;
				disMinAmountMua = true;
				disQuantityMua = true;
				disAmountMua = true;
				disPercentKM = false;
				disMaxQuantityKM = true;
				disMaxAmountKM = true;
				disQuantityKM = true;
				disAmountKM = true;
			} else if ($('#typeCode').val() == 'ZV08') {
				disMinQuantityMua = false;
				disMinAmountMua = true;
				disQuantityMua = true;
				disAmountMua = true;
				disPercentKM = true;
				disMaxQuantityKM = true;
				disMaxAmountKM = false;
				disQuantityKM = true;
				disAmountKM = true;
			} else if ($('#typeCode').val() == 'ZV09') {
				disMinQuantityMua = false;
				disMinAmountMua = true;
				disQuantityMua = true;
				disAmountMua = true;
				disPercentKM = true;
				disMaxQuantityKM = true;
				disMaxAmountKM = true;
				disQuantityKM = false;
				disAmountKM = true;
			} else if ($('#typeCode').val() == 'ZV10') {
				disMinQuantityMua = true;
				disMinAmountMua = false;
				disQuantityMua = true;
				disAmountMua = true;
				disPercentKM = false;
				disMaxQuantityKM = true;
				disMaxAmountKM = true;
				disQuantityKM = true;
				disAmountKM = true;
			} else if ($('#typeCode').val() == 'ZV11') {
				disMinQuantityMua = true;
				disMinAmountMua = false;
				disQuantityMua = true;
				disAmountMua = true;
				disPercentKM = true;
				disMaxQuantityKM = true;
				disMaxAmountKM = false;
				disQuantityKM = true;
				disAmountKM = true;
			} else if ($('#typeCode').val() == 'ZV12') {
				disMinQuantityMua = true;
				disMinAmountMua = false;
				disQuantityMua = true;
				disAmountMua = true;
				disPercentKM = true;
				disMaxQuantityKM = true;
				disMaxAmountKM = true;
				disQuantityKM = false;
				disAmountKM = true;
			} else if ($('#typeCode').val() == 'ZV13') {
				disMinQuantityMua = true;
				disMinAmountMua = true;
				disQuantityMua = false;
				disAmountMua = true;
				disPercentKM = false;
				disMaxQuantityKM = true;
				disMaxAmountKM = true;
				disQuantityKM = true;
				disAmountKM = true;
			} else if ($('#typeCode').val() == 'ZV14') {
				disMinQuantityMua = true;
				disMinAmountMua = true;
				disQuantityMua = false;
				disAmountMua = true;
				disPercentKM = true;
				disMaxQuantityKM = true;
				disMaxAmountKM = false;
				disQuantityKM = true;
				disAmountKM = true;
			} else if ($('#typeCode').val() == 'ZV15') {
				disMinQuantityMua = true;
				disMinAmountMua = true;
				disQuantityMua = false;
				disAmountMua = true;
				disPercentKM = true;
				disMaxQuantityKM = true;
				disMaxAmountKM = true;
				disQuantityKM = false;
				disAmountKM = true;
			} else if ($('#typeCode').val() == 'ZV16') {
				disMinQuantityMua = true;
				disMinAmountMua = true;
				disQuantityMua = true;
				disAmountMua = false;
				disPercentKM = false;
				disMaxQuantityKM = true;
				disMaxAmountKM = true;
				disQuantityKM = true;
				disAmountKM = true;
			} else if ($('#typeCode').val() == 'ZV17') {
				disMinQuantityMua = true;
				disMinAmountMua = true;
				disQuantityMua = true;
				disAmountMua = false;
				disPercentKM = true;
				disMaxQuantityKM = true;
				disMaxAmountKM = false;
				disQuantityKM = true;
				disAmountKM = true;
			} else if ($('#typeCode').val() == 'ZV18') {
				disMinQuantityMua = true;
				disMinAmountMua = true;
				disQuantityMua = true;
				disAmountMua = false;
				disPercentKM = true;
				disMaxQuantityKM = true;
				disMaxAmountKM = true;
				disQuantityKM = false;
				disAmountKM = true;
			} else if ($('#typeCode').val() == 'ZV19') {
				disMinQuantityMua = true;
				disMinAmountMua = false;
				disQuantityMua = true;
				disAmountMua = true;
				disPercentKM = false;
				disMaxQuantityKM = true;
				disMaxAmountKM = true;
				disQuantityKM = true;
				disAmountKM = true;
			} else if ($('#typeCode').val() == 'ZV20') {
				disMinQuantityMua = true;
				disMinAmountMua = false;
				disQuantityMua = true;
				disAmountMua = true;
				disPercentKM = true;
				disMaxQuantityKM = true;
				disMaxAmountKM = false;
				disQuantityKM = true;
				disAmountKM = true;
			} else if ($('#typeCode').val() == 'ZV21') {
				disMinQuantityMua = true;
				disMinAmountMua = false;
				disQuantityMua = true;
				disAmountMua = true;
				disPercentKM = true;
				disMaxQuantityKM = true;
				disMaxAmountKM = true;
				disQuantityKM = false;
				disAmountKM = true;
			}
			$('#gridSublevelMua'+levelCode).datagrid({
				data : subLevel,
				view: detailview,
                detailFormatter:function(idx,r){
                    return '<div class="ddv" id="'+levelCode+'_subGridMuaProduct_'+idx+'" style="padding:5px 0"></div>';
                },
                onExpandRow: function(idx,r){
                    var ddv = $(this).datagrid('getRowDetail', idx).find('div.ddv#'+levelCode+'_subGridMuaProduct_'+idx);
                    ddv.datagrid({
                    	data:r.listSubLevel,
                        fitColumns:true,
                        singleSelect:true,
                        rownumbers:true,
                        scrollbarSize:0,
                        height:'auto',
                        onDblClickRow : function(index,row) {
                        	for(var i = 0; i < ddv.datagrid('getRows').length; i++) {
                    			var row = ddv.datagrid('getRows')[i];
                    			if(VTUtilJS.isNullOrEmpty(row.productCode)) {
                    				return;
                    			}
                    			if(($('#typeCode').val() != 'ZV07' && $('#typeCode').val() != 'ZV08' && $('#typeCode').val() != 'ZV09' && $('#typeCode').val() != 'ZV10' && $('#typeCode').val() != 'ZV11' && $('#typeCode').val() != 'ZV12') && (VTUtilJS.isNullOrEmpty(row.quantity) || isNaN(row.quantity) || Number(row.quantity) == 0) && (VTUtilJS.isNullOrEmpty(row.amount) || isNaN(row.amount) || Number(row.amount) == 0)) {
                    				return;
                    			}
                    		}
                    		ddv.datagrid('acceptChanges');
                        	ddv.datagrid('beginEdit', index);
                        },
                        columns:[[
                            {field:'productCode', sortable:false,resizable:false, title:'Mã SP', width:80, align:'left', formatter: function(value, row, index) {
    							return Utils.XSSEncode(value);
    						},
                            	editor: {
	    							type:'combobox',
	    							 options:{
    								 	valueField : 'productCode',
    								 	textField : 'productCode',
    								 	data : PromotionProgram._listProduct.valArray,
    								 	formatter:function(row){
    			                            return Utils.XSSEncode(row.productCode + ' - ' +row.productName);
    			                        },
    								 	onHidePanel: function(){
    								 		var input = $(this).parent().find("input.combo-text.validatebox-text");
    								 		var r = PromotionProgram._listProduct.get(input.val());
    								 		var row = $(this).parent().parent().parent().parent().parent().parent().parent();
    								 		var detail = ddv.datagrid('getRows')[Number(row.attr('datagrid-row-index'))];
    								 		if (r != null && r != undefined) {
	    								 		row.find('td[field=productName] div').html(Utils.XSSEncode(r.productName));
	    								 		//Xu ly them tham so
	    								 		detail.productId = r.productId;
	    								 		detail.productCode = Utils.XSSEncode(r.productCode);
	    								 		detail.productName = Utils.XSSEncode(r.productName);
    								 		}
    								 		ddv.datagrid('getRows').splice(Number(row.attr('datagrid-row-index')), 1, detail);
    								 		var textCode = PromotionProgram.returnTextProductCode(ddv.datagrid('getRows'));
    								 		var subLevel = $('#gridSublevelMua'+Utils.XSSEncode(levelCode)).datagrid('getRows')[idx];
    								 		subLevel.textCode = Utils.XSSEncode(textCode);
    								 		$('#gridSublevelMua'+Utils.XSSEncode(levelCode)).datagrid('getRows').splice(idx, 1, subLevel);
    								 		$('#gridSublevelMua'+Utils.XSSEncode(levelCode)).prev().find('.datagrid-btable tr[datagrid-row-index='+idx+'] td[field=textCode] div').html(Utils.XSSEncode(textCode));
    								 		ddv.datagrid('fixRowHeight', row.attr('datagrid-row-index'));
    								 		$('#gridSublevelMua'+Utils.XSSEncode(levelCode)).datagrid('fixRowHeight', idx);
    								 	}
	    							 }
	    						}
                            },
                            {field:'productName', sortable:false,resizable:false, title:'Tên SP',width:150,align:'left', formatter: function(value, row, index) {
    							return Utils.XSSEncode(value);
    						}},
                            {field:'quantity', sortable:false,resizable:false, title:'Số lượng', width:50, align:'right', formatter:function(value, row, index){
        						if(value == 0 || value == 'null' || VTUtilJS.isNullOrEmpty(value)) {
        							return '';
        						} else {
        							return CommonFormatter.formatNormalCell(value, row, index);
        						}
        					},editor: {
							type: 'currency',
							options: {
								isDisable: disQuantityMua,
								maxLength: 10,
								blur: function(target) {
									var row = $(target).parent().parent().parent().parent().parent().parent().parent();
									var quantity = $(target).val();
									var detail = ddv.datagrid('getRows')[Number(row.attr('datagrid-row-index'))];
									detail.quantity = quantity;
									ddv.datagrid('getRows').splice(Number(row.attr('datagrid-row-index')), 1, detail);
									var textCode = PromotionProgram.returnTextProductCode(ddv.datagrid('getRows'));
									var subLevel = $('#gridSublevelMua' + levelCode).datagrid('getRows')[idx];
									subLevel.textCode = textCode;
									$('#gridSublevelMua' + levelCode).datagrid('getRows').splice(idx, 1, subLevel);
									$('#gridSublevelMua' + levelCode).prev().find('.datagrid-btable tr[datagrid-row-index=' + idx + '] td[field=textCode] div').html(Utils.XSSEncode(textCode));
									ddv.datagrid('fixRowHeight', row.attr('datagrid-row-index'));
									$('#gridSublevelMua' + levelCode).datagrid('fixRowHeight', idx);
									var typeCodeTmp = $('#typeCode').val().trim();
									if (typeCodeTmp == 'ZV01' || typeCodeTmp == 'ZV02' || typeCodeTmp == 'ZV03') {
										subLevel.minQuantityMua = quantity;
										$('#gridSublevelMua' + levelCode).prev().find('.datagrid-btable tr[datagrid-row-index=' + idx + '] td[field=minQuantityMua] input').val(quantity);
									}
								}
							}
    						}},
    						{field:'amount', sortable:false,resizable:false, title:'Số tiền', width:50, align:'right', formatter:function(value, row, index){
        						if(value == 0 || value == 'null' || VTUtilJS.isNullOrEmpty(value)) {
        							return '';
        						} else {
        							return CommonFormatter.formatNormalCell(value, row, index);
        						}
        					},editor: {
							type: 'currency',
							options: {
								isDisable: disAmountMua,
								maxLength: 20,
								blur: function(target) {
									var row = $(target).parent().parent().parent().parent().parent().parent().parent();
									var amount = $(target).val();
									var detail = ddv.datagrid('getRows')[Number(row.attr('datagrid-row-index'))];
									detail.amount = amount;
									ddv.datagrid('getRows').splice(Number(row.attr('datagrid-row-index')), 1, detail);
									var textCode = PromotionProgram.returnTextProductCode(ddv.datagrid('getRows'));
									var subLevel = $('#gridSublevelMua' + levelCode).datagrid('getRows')[idx];
									subLevel.textCode = textCode;
									$('#gridSublevelMua' + levelCode).datagrid('getRows').splice(idx, 1, subLevel);
									$('#gridSublevelMua' + levelCode).prev().find('.datagrid-btable tr[datagrid-row-index=' + idx + '] td[field=textCode] div').html(Utils.XSSEncode(textCode));
									ddv.datagrid('fixRowHeight', row.attr('datagrid-row-index'));
									$('#gridSublevelMua' + levelCode).datagrid('fixRowHeight', idx);
									if (typeCodeTmp == 'ZV04' || typeCodeTmp == 'ZV05' || typeCodeTmp == 'ZV06') {
										subLevel.minAmountMua = amount;
										$('#gridSublevelMua' + levelCode).prev().find('.datagrid-btable tr[datagrid-row-index=' + idx + '] td[field=minAmountMua] input').val(VTUtilJS.formatCurrency(amount));
									}
								}
							}
    						}},
						{ field: 'isRequired', title: "Bắt buộc", width: 50, align: 'center', sortable: false, resizable: false,
							editor: {
								type: 'checkbox',
								options: {
									on: '1',
									off: '0',
									onCheck: function(target) {
										var row = $(target).parent().parent().parent().parent().parent().parent().parent();
										var isRequired = $(target).is(":checked") ? 1 : 0;
										var detail = ddv.datagrid('getRows')[Number(row.attr('datagrid-row-index'))];
										detail.isRequired = isRequired;
										ddv.datagrid('getRows').splice(Number(row.attr('datagrid-row-index')), 1, detail);
										var textCode = PromotionProgram.returnTextProductCode(ddv.datagrid('getRows'));
										var subLevel = $('#gridSublevelMua' + levelCode).datagrid('getRows')[idx];
										subLevel.textCode = Utils.XSSEncode(textCode);
										$('#gridSublevelMua' + levelCode).datagrid('getRows').splice(idx, 1, subLevel);
										$('#gridSublevelMua' + levelCode).prev().find('.datagrid-btable tr[datagrid-row-index=' + idx + '] td[field=textCode] div').html(Utils.XSSEncode(textCode));
										ddv.datagrid('fixRowHeight', row.attr('datagrid-row-index'));
										$('#gridSublevelMua' + levelCode).datagrid('fixRowHeight', idx);
									}
								}
							},
							formatter: function(val, rowData, idx) {
								return (val == 1 ? "x" : "");
							}
						},
						{ field: 'delete', title: '<a ' + (($('#statusHidden').val() == 0 || $('#statusHidden').val() == 1) ? 'style="display:none;"' : '') + ' onclick="PromotionProgram.addSubProductLevel(\'' + ('gridSublevelMua' + levelCode) + '\', ' + idx + ', \'' + (ddv.attr('id')) + '\')"><span style="cursor:pointer"><img title="Thêm mới nhóm mua" src="/resources/images/icon_add.png"/></span></a>',
							width: 30, align: 'center', sortable: false, resizable: false, formatter: function(value, row, index) {
							return '<a ' + (($('#statusHidden').val() == 0 || $('#statusHidden').val() == 1) ? 'style="display:none;"' : '') + ' onclick="PromotionProgram.deleteSubProductLevel(\'' + ('gridSublevelMua' + levelCode) + '\', ' + idx + ', \'' + (ddv.attr('id')) + '\'' + ', ' + index + '' + (!VTUtilJS.isNullOrEmpty(row.levelDetailId) ? ', ' + row.levelDetailId + '' : '') + ')"><span style="cursor:pointer"><img title="Xóa nhóm mua" src="/resources/images/icon_delete.png"/></span></a>';
						}}
                        ]],
                        onLoadSuccess:function(){
                        	setTimeout(function(){
                            	$('#gridSublevelMua'+levelCode).datagrid('fixRowHeight',idx);
                            	$('#gridSublevelMua'+levelCode).datagrid('fixDetailRowHeight',idx);
                            },500);
                        	var arrDelete =  $('#mappingGrid td[field="delete"]');
                			if (arrDelete != undefined && arrDelete != null && arrDelete.length > 0) {
                			  for (var i = 0, size = arrDelete.length; i < size; i++) {
                			    $(arrDelete[i]).prop("id", "tab_product_pn_change_gr_sale_c_td_del_" + i);//Khai bao id danh cho phan quyen
                				$(arrDelete[i]).addClass("cmsiscontrol");
                			  }
                			}
                        	Utils.functionAccessFillControl('mappingGrid', function(data){
        						//Xu ly cac su kien lien quan den cac control phan quyen
                        		if (arrDelete != undefined && arrDelete != null && arrDelete.length > 0) {
	                        		var arrTmpLength =  $('#mappingGrid td[id^="tab_product_pn_change_gr_sale_c_td_del_"]').length;
	                				var invisibleLenght = $('.isCMSInvisible[id^="tab_product_pn_change_gr_sale_c_td_del_"]').length;
	                				if (arrTmpLength > 0 && arrTmpLength != invisibleLenght) {
	                					$(ddv).datagrid("showColumn", "delete");
	                				} else {
	                					$(ddv).datagrid("hideColumn", "delete");
	                				}
                        		}
        					});
                        	
                        }
                    });
                },
                width: '100%',
				height: 'auto',
				singleSelect: true,
				fitColumns: true,
				scrollbarSize:0,
				//showHeader:false,
				columns: [[
					{field:'textCode', title:"Sản phẩm", width:100,align:'left',sortable:false,resizable:false,formatter:function(value, row, index) {
						return PromotionProgram.returnTextProductCode(row.listSubLevel);
					}},
					{field:'minQuantityMua', title:"SL tối thiểu", width:30,align:'right',sortable:false,resizable:false,formatter:function(value, row, index){
						if(value == 0 || value == 'null' || VTUtilJS.isNullOrEmpty(value)) {
							return '';
						} else {
							return CommonFormatter.formatNormalCell(value, row, index);
						}
					},editor: {
						type:'currency', 
						options : {
							isDisable : disMinQuantityMua,
							maxLength:10,
							blur : function(target) {
								var row = $(target).parent().parent().parent().parent().parent().parent().parent();
						 		var minQuantityMua = $(target).val();
						 		$('#gridSublevelMua'+levelCode).datagrid('getRows')[Number(row.attr('datagrid-row-index'))].minQuantityMua = minQuantityMua;
							}
						}
					}},
					{field:'minAmountMua', title:"ST tối thiểu", width:30,align:'right',sortable:false,resizable:false,formatter:function(value, row, index){
						if(value == 0 || value == 'null' || VTUtilJS.isNullOrEmpty(value)) {
							return '';
						} else {
							return CommonFormatter.formatNormalCell(value, row, index);
						}
					},editor: {
						type:'currency', 
						options : {
							isDisable : disMinAmountMua,
							maxLength:20,
							blur : function(target) {
								var row = $(target).parent().parent().parent().parent().parent().parent().parent();
						 		var minAmountMua = $(target).val();
						 		$('#gridSublevelMua'+levelCode).datagrid('getRows')[Number(row.attr('datagrid-row-index'))].minAmountMua = minAmountMua;
							}
						}
					}},
					{field:'delete', title:'<a '+(($('#statusHidden').val() == 0 || $('#statusHidden').val() == 1) ? 'style="display:none;"' : '')+' onclick="PromotionProgram.addHtmlSubRowMua(\'tableSubLevelMua'+levelCode+'\', \''+levelCode+'\')"><span style="cursor:pointer"><img title="Thêm mới mức" src="/resources/images/icon_add.png"/></span></a>', width:10,align:'center',sortable:false,resizable:false,formatter:function(value,row,index){
						return '<a '+(($('#statusHidden').val() == 0 || $('#statusHidden').val() == 1) ? 'style="display:none;"' : '')+' onclick="PromotionProgram.deleteSubLevel(\''+'gridSublevelMua'+levelCode+'\', '+index+''+(!VTUtilJS.isNullOrEmpty(row.levelId) ? ', '+row.levelId+'' : '')+')"><span style="cursor:pointer"><img title="Xóa nhóm mua" src="/resources/images/icon_delete.png"/></span></a>';
					}}
				]],
				onLoadSuccess: function(data) {
					if(data.rows != undefined && $.isArray(data.rows)) {
						for(var i = 0; i < data.rows.length; i++) {
							$('#gridSublevelMua'+levelCode).datagrid('beginEdit', i);
						}
					}
					var arrDelete =  $('#mappingGrid td[field="delete"]');
        			if (arrDelete != undefined && arrDelete != null && arrDelete.length > 0) {
        			  for (var i = 0, size = arrDelete.length; i < size; i++) {
        			    $(arrDelete[i]).prop("id", "tab_product_pn_change_gr_sale_td_del_" + i);//Khai bao id danh cho phan quyen
        				$(arrDelete[i]).addClass("cmsiscontrol");
        			  }
        			}
                	Utils.functionAccessFillControl('mappingGrid', function(data){
						//Xu ly cac su kien lien quan den cac control phan quyen
                		if (arrDelete != undefined && arrDelete != null && arrDelete.length > 0) {
	                		var arrTmpLength =  $('#mappingGrid td[id^="tab_product_pn_change_gr_sale_td_del_"]').length;
	        				var invisibleLenght = $('.isCMSInvisible[id^="tab_product_pn_change_gr_sale_td_del_"]').length;
	        				if (arrTmpLength > 0 && arrTmpLength != invisibleLenght) {
	        					$('#gridSublevelMua'+levelCode).datagrid("showColumn", "delete");
	        				} else {
	        					$('#gridSublevelMua'+levelCode).datagrid("hideColumn", "delete");
	        				}
                		}
					});
				}
			});
		}
	},
	addHtmlSubRowKM : function(divTable, levelCode, subLevel) {
		$('.ErrorMsgStyle').hide();
		if($('#'+divTable+' tbody tr').length > 1) {
			if(($('#gridSublevelKM'+levelCode).length > 0 && $('#gridSublevelKM'+levelCode).datagrid('getRows').length > 0) && ($('#typeCode').val() == 'ZV01'
				|| $('#typeCode').val() == 'ZV02'
					|| $('#typeCode').val() == 'ZV03'
					|| $('#typeCode').val() == 'ZV04'
					|| $('#typeCode').val() == 'ZV05'
					|| $('#typeCode').val() == 'ZV06'
					|| $('#typeCode').val() == 'ZV07'
					|| $('#typeCode').val() == 'ZV08'
					|| $('#typeCode').val() == 'ZV09'
					|| $('#typeCode').val() == 'ZV10'
					|| $('#typeCode').val() == 'ZV11'
					|| $('#typeCode').val() == 'ZV12'
					|| $('#typeCode').val() == 'ZV13'
					|| $('#typeCode').val() == 'ZV14'
					|| $('#typeCode').val() == 'ZV15'
					|| $('#typeCode').val() == 'ZV16'
					|| $('#typeCode').val() == 'ZV17'
					|| $('#typeCode').val() == 'ZV18'
					|| $('#typeCode').val() == 'ZV19'
					|| $('#typeCode').val() == 'ZV20'
					|| $('#typeCode').val() == 'ZV21')) { 
				return;
			}
			//da co grid roi
			var rows = $('#gridSublevelKM'+levelCode).datagrid('getRows');
			rows.push({});
			$('#gridSublevelKM'+levelCode).datagrid('loadData', rows);
		} else {
			//chua co grid
			$('#'+divTable+' tbody').append('<tr>\
					<td colspan="5"><div id="gridSublevelKM'+levelCode+'" style="width:100%; height:\'auto\'; padding:10px;"></div></td>\
					</tr>');
			if(subLevel == undefined || subLevel == null || !$.isArray(subLevel) || subLevel.length == 0) {
				subLevel = [{listSubLevel:[]}];
			}
			var disMinQuantityMua = false;
			var disMinAmountMua = false;
			var disQuantityMua = false;
			var disAmountMua = false;
			var disPercentKM = false;
			var disMaxQuantityKM = false;
			var disMaxAmountKM = false;
			var disQuantityKM = false;
			var disAmountKM = false;
			if($('#typeCode').val() == 'ZV01') {
				disMinQuantityMua = true;
				disMinAmountMua = true;
				disQuantityMua = false;
				disAmountMua = true;
				disPercentKM = false;
				disMaxQuantityKM = true;
				disMaxAmountKM = true;
				disQuantityKM = true;
				disAmountKM = true;
			} else if($('#typeCode').val() == 'ZV02') {
				disMinQuantityMua = true;
				disMinAmountMua = true;
				disQuantityMua = false;
				disAmountMua = true;
				disPercentKM = true;
				disMaxQuantityKM = true;
				disMaxAmountKM = false;
				disQuantityKM = true;
				disAmountKM = true;
			} else if($('#typeCode').val() == 'ZV03') {
				disMinQuantityMua = true;
				disMinAmountMua = true;
				disQuantityMua = false;
				disAmountMua = true;
				disPercentKM = true;
				disMaxQuantityKM = true;
				disMaxAmountKM = true;
				disQuantityKM = false;
				disAmountKM = true;
			} else if($('#typeCode').val() == 'ZV04') {
				disMinQuantityMua = true;
				disMinAmountMua = true;
				disQuantityMua = true;
				disAmountMua = false;
				disPercentKM = false;
				disMaxQuantityKM = true;
				disMaxAmountKM = true;
				disQuantityKM = true;
				disAmountKM = true;
			} else if($('#typeCode').val() == 'ZV05') {
				disMinQuantityMua = true;
				disMinAmountMua = true;
				disQuantityMua = true;
				disAmountMua = false;
				disPercentKM = true;
				disMaxQuantityKM = true;
				disMaxAmountKM = false;
				disQuantityKM = true;
				disAmountKM = true;
			} else if($('#typeCode').val() == 'ZV06') {
				disMinQuantityMua = true;
				disMinAmountMua = true;
				disQuantityMua = true;
				disAmountMua = false;
				disPercentKM = true;
				disMaxQuantityKM = true;
				disMaxAmountKM = true;
				disQuantityKM = false;
				disAmountKM = true;
			} else if($('#typeCode').val() == 'ZV07') {
				disMinQuantityMua = false;
				disMinAmountMua = true;
				disQuantityMua = true;
				disAmountMua = true;
				disPercentKM = false;
				disMaxQuantityKM = true;
				disMaxAmountKM = true;
				disQuantityKM = true;
				disAmountKM = true;
			} else if($('#typeCode').val() == 'ZV08') {
				disMinQuantityMua = false;
				disMinAmountMua = true;
				disQuantityMua = true;
				disAmountMua = true;
				disPercentKM = true;
				disMaxQuantityKM = true;
				disMaxAmountKM = false;
				disQuantityKM = true;
				disAmountKM = true;
			} else if($('#typeCode').val() == 'ZV09') {
				disMinQuantityMua = false;
				disMinAmountMua = true;
				disQuantityMua = true;
				disAmountMua = true;
				disPercentKM = true;
				disMaxQuantityKM = true;
				disMaxAmountKM = true;
				disQuantityKM = false;
				disAmountKM = true;
			} else if($('#typeCode').val() == 'ZV10') {
				disMinQuantityMua = true;
				disMinAmountMua = false;
				disQuantityMua = true;
				disAmountMua = true;
				disPercentKM = false;
				disMaxQuantityKM = true;
				disMaxAmountKM = true;
				disQuantityKM = true;
				disAmountKM = true;
			} else if($('#typeCode').val() == 'ZV11') {
				disMinQuantityMua = true;
				disMinAmountMua = false;
				disQuantityMua = true;
				disAmountMua = true;
				disPercentKM = true;
				disMaxQuantityKM = true;
				disMaxAmountKM = false;
				disQuantityKM = true;
				disAmountKM = true;
			} else if($('#typeCode').val() == 'ZV12') {
				disMinQuantityMua = true;
				disMinAmountMua = false;
				disQuantityMua = true;
				disAmountMua = true;
				disPercentKM = true;
				disMaxQuantityKM = true;
				disMaxAmountKM = true;
				disQuantityKM = false;
				disAmountKM = true;
			} else if($('#typeCode').val() == 'ZV13') {
				disMinQuantityMua = true;
				disMinAmountMua = true;
				disQuantityMua = false;
				disAmountMua = true;
				disPercentKM = false;
				disMaxQuantityKM = true;
				disMaxAmountKM = true;
				disQuantityKM = true;
				disAmountKM = true;
			} else if($('#typeCode').val() == 'ZV14') {
				disMinQuantityMua = true;
				disMinAmountMua = true;
				disQuantityMua = false;
				disAmountMua = true;
				disPercentKM = true;
				disMaxQuantityKM = true;
				disMaxAmountKM = false;
				disQuantityKM = true;
				disAmountKM = true;
			} else if($('#typeCode').val() == 'ZV15') {
				disMinQuantityMua = true;
				disMinAmountMua = true;
				disQuantityMua = false;
				disAmountMua = true;
				disPercentKM = true;
				disMaxQuantityKM = true;
				disMaxAmountKM = true;
				disQuantityKM = false;
				disAmountKM = true;
			} else if($('#typeCode').val() == 'ZV16') {
				disMinQuantityMua = true;
				disMinAmountMua = true;
				disQuantityMua = true;
				disAmountMua = false;
				disPercentKM = false;
				disMaxQuantityKM = true;
				disMaxAmountKM = true;
				disQuantityKM = true;
				disAmountKM = true;
			} else if($('#typeCode').val() == 'ZV17') {
				disMinQuantityMua = true;
				disMinAmountMua = true;
				disQuantityMua = true;
				disAmountMua = false;
				disPercentKM = true;
				disMaxQuantityKM = true;
				disMaxAmountKM = false;
				disQuantityKM = true;
				disAmountKM = true;
			} else if($('#typeCode').val() == 'ZV18') {
				disMinQuantityMua = true;
				disMinAmountMua = true;
				disQuantityMua = true;
				disAmountMua = false;
				disPercentKM = true;
				disMaxQuantityKM = true;
				disMaxAmountKM = true;
				disQuantityKM = false;
				disAmountKM = true;
			} else if($('#typeCode').val() == 'ZV19') {
				disMinQuantityMua = true;
				disMinAmountMua = false;
				disQuantityMua = true;
				disAmountMua = true;
				disPercentKM = false;
				disMaxQuantityKM = true;
				disMaxAmountKM = true;
				disQuantityKM = true;
				disAmountKM = true;
			} else if($('#typeCode').val() == 'ZV20') {
				disMinQuantityMua = true;
				disMinAmountMua = false;
				disQuantityMua = true;
				disAmountMua = true;
				disPercentKM = true;
				disMaxQuantityKM = true;
				disMaxAmountKM = false;
				disQuantityKM = true;
				disAmountKM = true;
			} else if($('#typeCode').val() == 'ZV21') {
				disMinQuantityMua = true;
				disMinAmountMua = false;
				disQuantityMua = true;
				disAmountMua = true;
				disPercentKM = true;
				disMaxQuantityKM = true;
				disMaxAmountKM = true;
				disQuantityKM = false;
				disAmountKM = true;
			}
			$('#gridSublevelKM'+levelCode).datagrid({	
				data : subLevel,
				view: detailview,
                detailFormatter:function(idx,r){
                    return '<div class="ddv" id="'+levelCode+'_subGridKMProduct_'+idx+'" style="padding:5px 0"></div>';
                },
                onExpandRow: function(idx,r){
                    var ddv = $(this).datagrid('getRowDetail', idx).find('div.ddv#'+levelCode+'_subGridKMProduct_'+idx);
                    ddv.datagrid({
                    	data:r.listSubLevel,
                        fitColumns:true,
                        singleSelect:true,
                        rownumbers:true,
                        scrollbarSize:0,
                        height:'auto',
                        onDblClickRow : function(index,row) {
                        	for(var i = 0; i < ddv.datagrid('getRows').length; i++) {
                    			var row = ddv.datagrid('getRows')[i];
                    			if(VTUtilJS.isNullOrEmpty(row.productCode)) {
                    				return;
                    			}
                    			if(($('#typeCode').val() == 'ZV03' || $('#typeCode').val() == 'ZV06' || $('#typeCode').val() == 'ZV09' || $('#typeCode').val() == 'ZV12' || $('#typeCode').val() == 'ZV15' || $('#typeCode').val() == 'ZV18' || $('#typeCode').val() == 'ZV21') &&
                    					(VTUtilJS.isNullOrEmpty(row.quantity) || isNaN(row.quantity) || Number(row.quantity) == 0) && 
                    					(VTUtilJS.isNullOrEmpty(row.amount) || isNaN(row.amount) || Number(row.amount) == 0)
                    					) {
                    				return;
                    			}
                    		}
                    		ddv.datagrid('acceptChanges');
                        	ddv.datagrid('beginEdit', index);
                        },
                        columns:[[
                            {field:'productCode', sortable:false,resizable:false, title:'Mã SP', width:80, align:'left', formatter: function(value, row, index) {
    							return Utils.XSSEncode(value);
    						},
                            	editor: {
	    							type:'combobox',
	    							 options:{
    								 	valueField : 'productCode',
    								 	textField : 'productCode',
    								 	data : PromotionProgram._listProduct.valArray,
    								 	formatter:function(row){
    			                            return Utils.XSSEncode(row.productCode + ' - ' +row.productName);
    			                        },
    			                        onHidePanel: function(){
    			                        	var input = $(this).parent().find("input.combo-text.validatebox-text");
    			                        	var r = PromotionProgram._listProduct.get(input.val());
    			                        	var row = $(this).parent().parent().parent().parent().parent().parent().parent();
    								 		row.find('td[field=productName] div').html(r.productName);
								 			var detail = ddv.datagrid('getRows')[Number(row.attr('datagrid-row-index'))];
								 			detail.productId = Utils.XSSEncode(r.productId);
								 			detail.productCode = Utils.XSSEncode(r.productCode);
								 			detail.productName = Utils.XSSEncode(r.productName);
								 			ddv.datagrid('getRows').splice(Number(row.attr('datagrid-row-index')), 1, detail);
								 			var textCode = PromotionProgram.returnTextProductCode(ddv.datagrid('getRows'));
								 			var subLevel = $('#gridSublevelKM'+levelCode).datagrid('getRows')[idx];
								 			subLevel.textCode = Utils.XSSEncode(textCode);
								 			$('#gridSublevelKM'+levelCode).datagrid('getRows').splice(idx, 1, subLevel);
								 			$('#gridSublevelKM'+levelCode).prev().find('.datagrid-btable tr[datagrid-row-index='+idx+'] td[field=textCode] div').html(Utils.XSSEncode(textCode));
								 			ddv.datagrid('fixRowHeight', row.attr('datagrid-row-index'));
								 			$('#gridSublevelKM'+levelCode).datagrid('fixRowHeight', idx);
								 		}
								 	}    								 	
	    						}
                            },
                            {field:'productName', sortable:false,resizable:false, title:'Tên SP',width:150,align:'left', formatter: function(value, row, index) {
    							return Utils.XSSEncode(value);
    						}},
                            {field:'quantity', sortable:false,resizable:false, title:'Số lượng', width:50, align:'right', formatter:function(value, row, index){
        						if(value == 0 || value == 'null' || VTUtilJS.isNullOrEmpty(value)) {
        							return '';
        						} else {
        							return CommonFormatter.formatNormalCell(value, row, index);
        						}
        					},editor: {
    							type:'currency', 
    							options : {
    								isDisable : disQuantityKM,
    								maxLength:10,
    								blur : function(target) {
    									var row = $(target).parent().parent().parent().parent().parent().parent().parent();
								 		var quantity = $(target).val();
								 		var detail = ddv.datagrid('getRows')[Number(row.attr('datagrid-row-index'))];
								 		detail.quantity = quantity;
								 		ddv.datagrid('getRows').splice(Number(row.attr('datagrid-row-index')), 1, detail);
								 		var textCode = PromotionProgram.returnTextProductCode(ddv.datagrid('getRows'));
								 		var subLevel = $('#gridSublevelKM'+levelCode).datagrid('getRows')[idx];
								 		subLevel.textCode = Utils.XSSEncode(textCode);
								 		$('#gridSublevelKM'+levelCode).datagrid('getRows').splice(idx, 1, subLevel);
								 		$('#gridSublevelKM'+levelCode).prev().find('.datagrid-btable tr[datagrid-row-index='+idx+'] td[field=textCode] div').html(Utils.XSSEncode(textCode));
								 		ddv.datagrid('fixRowHeight', row.attr('datagrid-row-index'));
								 		$('#gridSublevelKM'+levelCode).datagrid('fixRowHeight', idx);
    								}
    							}
    						}},
    						{field:'amount', sortable:false,resizable:false, title:'Số tiền', width:50, align:'right', formatter:function(value, row, index){
        						if(value == 0 || value == 'null' || VTUtilJS.isNullOrEmpty(value)) {
        							return '';
        						} else {
        							return CommonFormatter.formatNormalCell(value, row, index);
        						}
        					},editor: {
    							type:'currency', 
    							options : {
    								isDisable : disAmountKM,
    								maxLength:20,
    								blur : function(target) {
    									var row = $(target).parent().parent().parent().parent().parent().parent().parent();
								 		var amount = $(target).val();
								 		var detail = ddv.datagrid('getRows')[Number(row.attr('datagrid-row-index'))];
								 		detail.amount = amount;
								 		ddv.datagrid('getRows').splice(Number(row.attr('datagrid-row-index')), 1, detail);
								 		var textCode = PromotionProgram.returnTextProductCode(ddv.datagrid('getRows'));
								 		var subLevel = $('#gridSublevelKM'+levelCode).datagrid('getRows')[idx];
								 		subLevel.textCode = Utils.XSSEncode(textCode);
								 		$('#gridSublevelKM'+levelCode).datagrid('getRows').splice(idx, 1, subLevel);
								 		$('#gridSublevelKM'+levelCode).prev().find('.datagrid-btable tr[datagrid-row-index='+idx+'] td[field=textCode] div').html(Utils.XSSEncode(textCode));
								 		ddv.datagrid('fixRowHeight', row.attr('datagrid-row-index'));
								 		$('#gridSublevelKM'+levelCode).datagrid('fixRowHeight', idx);
    								}
    							}
    						}},
    						{field:'isRequired', title:"Bắt buộc", width:50,align:'center',sortable:false,resizable:false,
    							editor:{type:'checkbox', options:{on:'1', off:'0', onCheck:function(target) {
    								var row = $(target).parent().parent().parent().parent().parent().parent().parent();
							 		var isRequired = $(target).is(":checked") ? 1 : 0;
							 		var detail = ddv.datagrid('getRows')[Number(row.attr('datagrid-row-index'))];
							 		detail.isRequired = isRequired;
							 		ddv.datagrid('getRows').splice(Number(row.attr('datagrid-row-index')), 1, detail);
							 		var textCode = PromotionProgram.returnTextProductCode(ddv.datagrid('getRows'));
							 		var subLevel = $('#gridSublevelKM'+levelCode).datagrid('getRows')[idx];
							 		subLevel.textCode = textCode;
							 		$('#gridSublevelKM'+levelCode).datagrid('getRows').splice(idx, 1, subLevel);
							 		$('#gridSublevelKM'+levelCode).prev().find('.datagrid-btable tr[datagrid-row-index='+idx+'] td[field=textCode] div').html(Utils.XSSEncode(textCode));
							 		ddv.datagrid('fixRowHeight', row.attr('datagrid-row-index'));
							 		$('#gridSublevelKM'+levelCode).datagrid('fixRowHeight', idx);
    							}
    						}}},
    						{field:'delete', title:'<a '+(($('#statusHidden').val() == 0 || $('#statusHidden').val() == 1) ? 'style="display:none;"' : '')+' onclick="PromotionProgram.addSubProductLevel(\''+('gridSublevelKM'+levelCode)+'\', '+idx+', \''+(ddv.attr('id'))+'\')"><span style="cursor:pointer"><img title="Thêm mới nhóm mua" src="/resources/images/icon_add.png"/></span></a>', width:30,align:'center',sortable:false,resizable:false,formatter:function(value,row,index){
    							return '<a '+(($('#statusHidden').val() == 0 || $('#statusHidden').val() == 1) ? 'style="display:none;"' : '')+' onclick="PromotionProgram.deleteSubProductLevel(\''+('gridSublevelKM'+levelCode)+'\', '+idx+', \''+(ddv.attr('id'))+'\''+', '+index+''+(!VTUtilJS.isNullOrEmpty(row.levelDetailId) ? ', '+row.levelDetailId+'' : '')+')"><span style="cursor:pointer"><img title="Xóa nhóm mua" src="/resources/images/icon_delete.png"/></span></a>';
    						}}
                        ]],
                        onLoadSuccess:function(){
                        	setTimeout(function(){
                            	$('#gridSublevelKM'+levelCode).datagrid('fixRowHeight',idx);
                            	$('#gridSublevelKM'+levelCode).datagrid('fixDetailRowHeight',idx);
                            }, 500);
                        	var arrDelete =  $('#mappingGrid td[field="delete"]');
                			if (arrDelete != undefined && arrDelete != null && arrDelete.length > 0) {
                			  for (var i = 0, size = arrDelete.length; i < size; i++) {
                			    $(arrDelete[i]).prop("id", "tab_product_pn_change_gr_km_c_td_del_" + i);//Khai bao id danh cho phan quyen
                				$(arrDelete[i]).addClass("cmsiscontrol");
                			  }
                			}
                        	Utils.functionAccessFillControl('mappingGrid', function(data){
                        		if (arrDelete != undefined && arrDelete != null && arrDelete.length > 0) {
                        			//Xu ly cac su kien lien quan den cac control phan quyen
                        			var arrTmpLength =  $('#mappingGrid td[id^="tab_product_pn_change_gr_km_c_td_del_"]').length;
                        			var invisibleLenght = $('.isCMSInvisible[id^="tab_product_pn_change_gr_km_c_td_del_"]').length;
                        			if (arrTmpLength > 0 && arrTmpLength != invisibleLenght) {
                        				$(ddv).datagrid("showColumn", "delete");
                        			} else {
                        				$(ddv).datagrid("hideColumn", "delete");
                        			}
        						}
        					});
                        }
                    });
                },
                width: '100%',
				height: 'auto',
				singleSelect: true,
				fitColumns: true,
				scrollbarSize:0,
				columns: [[
					{field:'textCode', title:"Sản phẩm", width:100,align:'left',sortable:false,resizable:false,formatter:function(value, row, index) {
						return PromotionProgram.returnTextProductCode(row.listSubLevel);
					}},
					{field:'percent', title:"% KM", width:20,align:'right',sortable:false,resizable:false,formatter:function(value, row, index){
						if(value == 0 || value == 'null' || VTUtilJS.isNullOrEmpty(value)) {
							return '';
						} else {
							return CommonFormatter.formatNormalCell(value, row, index);
						}
					},editor: {
						type:'percent',
						options : {
							isDisable : disPercentKM,
							maxLength:5,
							blur : function(target) {
								var row = $(target).parent().parent().parent().parent().parent().parent().parent();
						 		var percent = $(target).val();
						 		var detail = $('#gridSublevelKM'+levelCode).datagrid('getRows')[Number(row.attr('datagrid-row-index'))];
						 		detail.percent = percent;
						 		$('#gridSublevelKM'+levelCode).datagrid('getRows').splice(Number(row.attr('datagrid-row-index')), 1, detail);
							}
						}
					}},
					{field:'maxQuantityKM', title:"SL tối da", width:30,align:'right',sortable:false,resizable:false,formatter:function(value, row, index){
						if(value == 0 || value == 'null' || VTUtilJS.isNullOrEmpty(value)) {
							return '';
						} else {
							return CommonFormatter.formatNormalCell(value, row, index);
						}
					},editor: {
						type:'currency', 
						options : {
							isDisable : disMaxQuantityKM,
							maxLength:10,
							blur : function(target) {
								var row = $(target).parent().parent().parent().parent().parent().parent().parent();
						 		var maxQuantityKM = $(target).val();
						 		var detail = $('#gridSublevelKM'+levelCode).datagrid('getRows')[Number(row.attr('datagrid-row-index'))];
						 		detail.maxQuantityKM = maxQuantityKM;
						 		$('#gridSublevelKM'+levelCode).datagrid('getRows').splice(Number(row.attr('datagrid-row-index')), 1, detail);
							}
						}
					}},
					{field:'maxAmountKM', title:"ST tối da", width:30,align:'right',sortable:false,resizable:false,formatter:function(value, row, index){
						if(value == 0 || value == 'null' || VTUtilJS.isNullOrEmpty(value)) {
							return '';
						} else {
							return CommonFormatter.formatNormalCell(value, row, index);
						}
					},editor: {
						type:'currency', 
						options : {
							isDisable : disMaxAmountKM,
							maxLength:20,
							blur : function(target) {
								var row = $(target).parent().parent().parent().parent().parent().parent().parent();
						 		var maxAmountKM = $(target).val();
						 		var detail = $('#gridSublevelKM'+levelCode).datagrid('getRows')[Number(row.attr('datagrid-row-index'))];
						 		detail.maxAmountKM = maxAmountKM;
						 		$('#gridSublevelKM'+levelCode).datagrid('getRows').splice(Number(row.attr('datagrid-row-index')), 1, detail);
							}
						}
					}},
					{field:'delete', title:'<a '+(($('#statusHidden').val() == 0 || $('#statusHidden').val() == 1) ? 'style="display:none;"' : '')+' onclick="PromotionProgram.addHtmlSubRowKM(\'tableSubLevelKM'+levelCode+'\', \''+levelCode+'\')"><span style="cursor:pointer"><img title="Thêm mới mức" src="/resources/images/icon_add.png"/></span></a>', width:10,align:'center',sortable:false,resizable:false,formatter:function(value,row,index){
						return '<a '+(($('#statusHidden').val() == 0 || $('#statusHidden').val() == 1) ? 'style="display:none;"' : '')+' onclick="PromotionProgram.deleteSubLevel(\''+'gridSublevelKM'+levelCode+'\', '+index+''+(!VTUtilJS.isNullOrEmpty(row.levelId) ? ', '+row.levelId+'' : '')+')"><span style="cursor:pointer"><img title="Xóa nhóm mua" src="/resources/images/icon_delete.png"/></span></a>';
					}}
				]],
				onLoadSuccess: function(data) {
					if(data.rows != null && data.rows != undefined) {
						for(var t = 0; t < data.rows.length; t++) {
							$('#gridSublevelKM'+levelCode).datagrid('beginEdit', t);
						}
					}
					var arrDelete =  $('#mappingGrid td[field="delete"]');
        			if (arrDelete != undefined && arrDelete != null && arrDelete.length > 0) {
        			  for (var i = 0, size = arrDelete.length; i < size; i++) {
        			    $(arrDelete[i]).prop("id", "tab_product_pn_change_gr_km_td_del_" + i);//Khai bao id danh cho phan quyen
        				$(arrDelete[i]).addClass("cmsiscontrol");
        			  }
        			}
                	Utils.functionAccessFillControl('mappingGrid', function(data){
						//Xu ly cac su kien lien quan den cac control phan quyen
                		if (arrDelete != undefined && arrDelete != null && arrDelete.length > 0) {
	                		var arrTmpLength =  $('#mappingGrid td[id^="tab_product_pn_change_gr_km_td_del_"]').length;
	        				var invisibleLenght = $('.isCMSInvisible[id^="tab_product_pn_change_gr_km_td_del_"]').length;
	        				if (arrTmpLength > 0 && arrTmpLength != invisibleLenght) {
	        					$('#gridSublevelKM'+levelCode).datagrid("showColumn", "delete");
	        				} else {
	        					$('#gridSublevelKM'+levelCode).datagrid("hideColumn", "delete");
	        				}
                		}
					});
				}
			});
		}
	},
	renderPannel : function(list, i, groupMuaId, groupKMId) {
		if(list.length == i) {
			PromotionProgram.isFinishLoad = true;
			return;
		} 
		if($('#tableLevel' + (list[i].mapId)).length == 0) {
			$($('#mappingGrid tbody')[0]).append('<tr>\
					<td>\
					<p class="ErrorMsgStyle SpriteErr" style="display: none;" id="errorMsgDistribute'+(VTUtilJS.isNullOrEmpty(list[i].levelCode) ? list[i].mapId : list[i].levelCode+list[i].mapId)+'">\
					<p id="successMsgDistribute'+(VTUtilJS.isNullOrEmpty(list[i].levelCode) ? list[i].mapId : list[i].levelCode+list[i].mapId)+'" class="SuccessMsgStyle" style="display: none"></p>\
					<div class="TableLevel" id="tableLevel'+(list[i].mapId)+'" style="width:100%; height:\'auto\';padding:10px;" title="">\
					<table style="width: 100%;">\
			    	<tr>\
			    		<td id="tdTableSubLevelMua" style="width: 50%; vertical-align: top;">\
			    			<table id="tableSubLevelMua'+(VTUtilJS.isNullOrEmpty(list[i].levelCode) ? list[i].mapId : Utils.XSSEncode(list[i].levelCode) +list[i].mapId)+'" style="border: 1px solid black; vertical-align: top;width: 100%">\
			    				<tbody>\
			    				</tbody>\
			    			</table>\
			    		</td>\
			    		<td id="tdTableSubLevelKM" style="width: 50%; vertical-align: top;">\
			    			<table id="tableSubLevelKM'+(VTUtilJS.isNullOrEmpty(list[i].levelCode) ? list[i].mapId : Utils.XSSEncode(list[i].levelCode) + list[i].mapId)+'" style="border: 1px solid black; vertical-align: top;width: 100%">\
			    				<tbody>\
			    				</tbody>\
			    			</table>\
			    		</td>\
			    	</tr>\
				</table>\
					    </div>\
				</td>\
			</tr>');
			VTUtilJS.bindFormatOnTextfield('sttMua_'+(VTUtilJS.isNullOrEmpty(list[i].levelCode) ? list[i].mapId : Utils.XSSEncode(list[i].levelCode) + list[i].mapId), VTUtilJS._TF_NUMBER);
			
			$('#tableLevel'+(list[i].mapId)).panel({
				width:'95%',
				height:'auto',
				collapsible:true,
				title: VTUtilJS.isNullOrEmpty(list[i].levelCode) ? 'STT : ' +list[i].stt : 'Mã : ' + Utils.XSSEncode(list[i].levelCode) + ' - ' + 'STT : ' + list[i].stt,
				tools:[{
					iconCls:'iconAdd '+i+'',
					handler:function(){
						var i = Number(Number($(this).attr('class').split(' ')[1]));
						PromotionProgram.addHtmlLevel('tableLevel'+(list[i].mapId)+'',groupMuaId, groupKMId);
					}
				}, {
					iconCls:'iconEdit '+i+'',
					handler:function(){
						var i = Number($(this).attr('class').split(' ')[1]);
						PromotionProgram.editHtmlLevel('tableLevel'+(list[i].mapId), groupMuaId, groupKMId, list[i].mapId, list[i].levelMuaId, list[i].levelKMId, (VTUtilJS.isNullOrEmpty(list[i].levelCode) ? '' : list[i].levelCode), (list[i].stt == null || list[i].stt == undefined) ? 0 : list[i].stt);
					}
				}, {
					iconCls:'iconDelete '+i+'',
					handler:function(){
						var i = Number($(this).attr('class').split(' ')[1]);
						PromotionProgram.deleteHtmlLevel('tableLevel'+(list[i].mapId), groupMuaId, groupKMId, list[i].mapId, list[i].levelMuaId, list[i].levelKMId);
					}
				}, {
					iconCls:'iconCopy '+i+'',
					handler:function(){
						var i = Number($(this).attr('class').split(' ')[1]);
						PromotionProgram.copyHtmlLevel(list[i].mapId, groupMuaId, groupKMId);
					}
				}, {
					iconCls:'iconSave '+i+'',
					handler:function(){
						var i = Number($(this).attr('class').split(' ')[1]);
						PromotionProgram.saveHtmlLevel((VTUtilJS.isNullOrEmpty(list[i].levelCode) ? list[i].mapId : list[i].levelCode+list[i].mapId), groupMuaId, groupKMId, list[i].mapId, list[i].levelMuaId, list[i].levelKMId);
					}
				}],
				onOpen: function(){
					$('a.iconAdd').attr('title', 'Thêm mới');
					$('a.iconEdit').attr('title', 'Chỉnh sửa');
					$('a.iconDelete').attr('title', 'Xóa');
					$('a.iconCopy').attr('title', 'Sao chép');
					$('a.iconSave').attr('title', 'Lưu');
				}
			});
			if(list[i].listExLevelMua != null && list[i].listExLevelMua != undefined && $.isArray(list[i].listExLevelMua)) {
				PromotionProgram.addHtmlSubRowMua('tableSubLevelMua'+(VTUtilJS.isNullOrEmpty(list[i].levelCode) ? list[i].mapId : Utils.XSSEncode(list[i].levelCode) + list[i].mapId), VTUtilJS.isNullOrEmpty(list[i].levelCode) ? list[i].mapId : list[i].levelCode+list[i].mapId, list[i].listExLevelMua);
			}
			if(list[i].listExLevelKM != null && list[i].listExLevelKM != undefined && $.isArray(list[i].listExLevelKM)) {
				PromotionProgram.addHtmlSubRowKM('tableSubLevelKM'+(VTUtilJS.isNullOrEmpty(list[i].levelCode) ? list[i].mapId : Utils.XSSEncode(list[i].levelCode) + list[i].mapId), VTUtilJS.isNullOrEmpty(list[i].levelCode) ? list[i].mapId : list[i].levelCode+list[i].mapId, list[i].listExLevelKM);
			}
		}
		PromotionProgram.renderPannel(list, ++i, groupMuaId, groupKMId);	
	},
	clickTabAddLevel : function(groupMuaId, groupKMId, productGroupCode, productGroupName) {
		PromotionProgram.fromLevel = 1;
		PromotionProgram.toLevel = 5;
		PromotionProgram.isEndLoad = false;
		PromotionProgram.isFinishLoad = true;
		PromotionProgram.tabAddLevel(groupMuaId, groupKMId, Utils.XSSEncode(productGroupCode), Utils.XSSEncode(productGroupName));
	},
	tabAddLevel : function(groupMuaId, groupKMId, productGroupCode, productGroupName) {
		$('#kmLevel').show();
		$('#cocauKM').html('Cơ cấu khuyến mại nhóm ' + Utils.XSSEncode(productGroupCode) + ' - ' + Utils.XSSEncode(productGroupName));
		PromotionProgram.groupMuaId = groupMuaId;
		PromotionProgram.groupKMId = groupKMId;
		$('.TableLevel').panel('destroy');
		$('#mappingGrid tbody').html('');
		var params = {
			groupMuaId: groupMuaId,
			groupKMId: groupKMId,
			fromLevel: PromotionProgram.fromLevel,
			toLevel: PromotionProgram.toLevel
		};
		VTUtilJS.getFormJson(params, '/promotion/new-list-map-level', function(data) {
			if (PromotionProgram.isEndLoad || !PromotionProgram.isFinishLoad) {
				return;
			}
			if ((data.isNew != null && data.isNew != undefined && !data.isNew) && data.listNewMapping != null && data.listNewMapping != undefined && $.isArray(data.listNewMapping) && data.listNewMapping.length == 0) {
				PromotionProgram.isEndLoad = true;
				return;
			}
			if (data.listNewMapping != null && data.listNewMapping != undefined && $.isArray(data.listNewMapping) && data.listNewMapping.length > 0) {
				$.messager.show({
			        title:'Cơ cấu CTKM',
			        msg:'Bạn đang xem cơ cấu CTKM từ mức <span style="color:red; font-weight: bold;">' + Utils.XSSEncode(params.fromLevel) + '</span> đến mức <span style="color:red; font-weight: bold;">' + (params.fromLevel + data.listNewMapping.length-1) + '</span>',
			        timeout:4000,
			        showType:'slide'
			    });
				PromotionProgram.fromLevel = Number(params.toLevel) + 1;
				PromotionProgram.toLevel = Number(params.toLevel) + Number(data.listNewMapping.length);
			}
			if (data.isNew) {
				if ($.isArray(data.listNewMapping) && data.listNewMapping.length == 0) {
					$($('#mappingGrid tbody')[0]).append('<tr class="NewLevel">\
								<td>\
								<div class="TableLevel" id="tableLevel" style="width:100%; height:\'auto\';padding:10px;" title="">\
						    </div>\
						</td>\
					</tr>');
					$('#tableLevel').panel({
						width:'95%',
						height:'auto',
						collapsible:true,
						title:' ',
						tools:[{
							iconCls:'iconAdd',
							handler:function(){
								PromotionProgram.addHtmlLevel('tableLevel', groupMuaId, groupKMId);
							}
						}],
						onLoad:function(){
							
						}
					});
				}
			} else {
				PromotionProgram.renderPannel(data.listNewMapping, 0, groupMuaId, groupKMId);
			}
			var arrPanel = $('.panel-tool');
			if (arrPanel != undefined && arrPanel != null && arrPanel.length > 0) {
				for (var i = 0, size = arrPanel.length; i < size; i++) {
					$(arrPanel[i]).prop("id", "tab_product_pn_change_hd_" + i);//Khai bao id danh cho phan quyen
					$(arrPanel[i]).addClass("cmsiscontrol");	
				}
			}
			Utils.functionAccessFillControl('divTab', function(data) {
				//Xu ly cac su kien lien quan den ctl phan quyen
			});
		});
	},
	returnTextProductCode : function(listSubLevel) {
		var textCode = '';
		if(listSubLevel != null && listSubLevel != undefined && $.isArray(listSubLevel) && listSubLevel.length > 0) {
			for(var i = 0; i < listSubLevel.length; i++) {
				if(i == 0) {
					if(!VTUtilJS.isNullOrEmpty(listSubLevel[i].productCode)) {
						textCode += listSubLevel[i].productCode;
					}
					if(listSubLevel[i].isRequired == 1) {
						textCode += '*';
					}
					if(listSubLevel[i].quantity != null && listSubLevel[i].quantity != undefined && !isNaN(listSubLevel[i].quantity) && Number(listSubLevel[i].quantity) > 0) {
						textCode += '('+listSubLevel[i].quantity+')';
					} else if(listSubLevel[i].amount != null && listSubLevel[i].amount != undefined && !isNaN(listSubLevel[i].amount) && Number(listSubLevel[i].amount) > 0) {
						textCode += '('+VTUtilJS.formatCurrency(listSubLevel[i].amount)+')';
					}
				} else {
					textCode += ', ';
					if(!VTUtilJS.isNullOrEmpty(listSubLevel[i].productCode)) {
						textCode += listSubLevel[i].productCode;
					}
					if(listSubLevel[i].isRequired == 1) {
						textCode += '*';
					}
					if(listSubLevel[i].quantity != null && listSubLevel[i].quantity != undefined && !isNaN(listSubLevel[i].quantity) && Number(listSubLevel[i].quantity) > 0) {
						textCode += '('+listSubLevel[i].quantity+')';
					} else if(listSubLevel[i].amount != null && listSubLevel[i].amount != undefined && !isNaN(listSubLevel[i].amount) && Number(listSubLevel[i].amount) > 0) {
						textCode += '('+VTUtilJS.formatCurrency(listSubLevel[i].amount)+')';
					}
				}
			}
		}
		return Utils.XSSEncode(textCode);
	},
	deleteSubLevel : function(gridDiv, idx, subLevelId) {
		if($('#statusHidden').val() == 0 || $('#statusHidden').val() == 1){
			return;
		}
		$.messager.confirm("Xác nhận", "Bạn có muốn xóa mức con không?", function(r) {
			if(r) {
				var rows = $('#'+gridDiv).datagrid('getRows');
				if(subLevelId != undefined && subLevelId != null && subLevelId > 0) {
					VTUtilJS.postFormJson({levelId : subLevelId}, '/promotion/new-delete-sub-level', function(data){
						if(rows != null && $.isArray(rows) && rows.length > 0) {
							rows.splice(idx, 1);
							$('#'+gridDiv).datagrid('loadData', rows);
						}
					});
				} else {
					if(rows != null && $.isArray(rows) && rows.length > 0) {
						rows.splice(idx, 1);
						$('#'+gridDiv).datagrid('loadData', rows);
					}
				}
			}
		});
	},
	deleteSubProductLevel : function(parentDiv, parentIdx, detailGridId, idx, detailId) {
		if($('#statusHidden').val() == 0 || $('#statusHidden').val() == 1){
			return;
		}
		$.messager.confirm("Xác nhận", "Bạn có muốn xóa chi tiết sản phẩm không?", function(r) {
			if(r) {
				var ddv = $('#'+parentDiv).datagrid('getRowDetail', parentIdx).find('div.ddv#'+detailGridId);
				var rows = ddv.datagrid('getRows');
				if(detailId != undefined && detailId != null && detailId > 0) {
					VTUtilJS.postFormJson({levelDetailId : detailId}, '/promotion/new-delete-detail', function(data){
						if(!data.error) {
							if(rows != null && $.isArray(rows) && rows.length > 0) {
								rows.splice(idx, 1);
								ddv.datagrid('loadData', rows);
								var textCode = PromotionProgram.returnTextProductCode(rows);
						 		var subLevel = $('#'+parentDiv).datagrid('getRows')[parentIdx];
						 		subLevel.textCode = textCode;
						 		$('#'+parentDiv).datagrid('getRows').splice(parentIdx, 1, subLevel);
						 		$('#'+parentDiv).prev().find('.datagrid-btable tr[datagrid-row-index='+parentIdx+'] td[field=textCode] div').html(Utils.XSSEncode(Utils.XSSEncode(textCode)));
						 		ddv.datagrid('fixRowHeight', idx);
						 		$('#'+parentDiv).datagrid('fixRowHeight', parentIdx);
							}
						}
					});
				} else {
					if(rows != null && $.isArray(rows) && rows.length > 0) {
						rows.splice(idx, 1);
						ddv.datagrid('loadData', rows);
						var textCode = PromotionProgram.returnTextProductCode(rows);
				 		var subLevel = $('#'+parentDiv).datagrid('getRows')[parentIdx];
				 		subLevel.textCode = textCode;
				 		$('#'+parentDiv).datagrid('getRows').splice(parentIdx, 1, subLevel);
				 		$('#'+parentDiv).prev().find('.datagrid-btable tr[datagrid-row-index='+parentIdx+'] td[field=textCode] div').html(textCode);
				 		ddv.datagrid('fixRowHeight', idx);
				 		$('#'+parentDiv).datagrid('fixRowHeight', parentIdx);
					}
				}
			}
		});
	},
	saveHtmlLevel : function(levelCode, groupMuaId, groupKMId, mapId, levelMuaId, levelKMId) {
		if($('#statusHidden').val() == 0 || $('#statusHidden').val() == 1){
			return;
		}
		$('.ErrorMsgStyle').hide();
		var muaDg = $('#gridSublevelMua'+levelCode);
		var kmDg = $('#gridSublevelKM'+levelCode);
		var dataModel = {};
		dataModel.groupMuaId = groupMuaId;
		dataModel.groupKMId = groupKMId;
		dataModel.mappingId = mapId;
		dataModel.levelMuaId = levelMuaId;
		dataModel.levelKMId = levelKMId;
		if(muaDg.length > 0 && muaDg.datagrid('getRows').length > 0) {
			dataModel.listSubLevelMua = muaDg.datagrid('getRows');
		} else {
			$("#errorMsgDistribute"+levelCode).html('Mức mua chưa được nhập chi tiết. Vui lòng xem lại').show();
			return;
		}
		if(kmDg.length > 0 && kmDg.datagrid('getRows').length > 0) {
			dataModel.listSubLevelKM = kmDg.datagrid('getRows');
		} else {
			$("#errorMsgDistribute"+levelCode).html('Mức khuyến mại chưa được nhập chi tiết. Vui lòng xem lại').show();
			return;
		}
		var listProductCode = new Array();
		for(var i = 0; i < dataModel.listSubLevelKM.length; i++) {
			var isPercentInput = false;
			var isQuantityInput = false;
			var isAmountInput = false;
			if(!VTUtilJS.isNullOrEmpty(dataModel.listSubLevelKM[i].percent) && !isNaN(dataModel.listSubLevelKM[i].percent) && Number(dataModel.listSubLevelKM[i].percent) > 0) {
				isPercentInput = true;
			}
			if(!VTUtilJS.isNullOrEmpty(dataModel.listSubLevelKM[i].maxQuantityKM) && !isNaN(dataModel.listSubLevelKM[i].maxQuantityKM) && Number(dataModel.listSubLevelKM[i].maxQuantityKM) > 0) {
				if(dataModel.listSubLevelKM[i].listSubLevel == null || !$.isArray(dataModel.listSubLevelKM[i].listSubLevel) || dataModel.listSubLevelKM[i].listSubLevel.length == 0) {
					$("#errorMsgDistribute"+levelCode).html('Bạn chưa nhập sản phẩm khuyến mại. Vui lòng nhập sản phẩm khuyến mại').show();
					return;
				}
				isQuantityInput = true;
			}
			if(!VTUtilJS.isNullOrEmpty(dataModel.listSubLevelKM[i].maxAmountKM) && !isNaN(dataModel.listSubLevelKM[i].maxAmountKM) && Number(dataModel.listSubLevelKM[i].maxAmountKM) > 0) {
				isAmountInput = true;
			}
			if(isPercentInput && (isQuantityInput || isAmountInput)) {
				$("#errorMsgDistribute"+levelCode).html('Bạn chỉ được nhập duy nhất một hình thức khuyến mại % tiền hoặc, số lượng, hoặc số tiền').show();
				return;
			}
			if(isQuantityInput && isAmountInput) {
				$("#errorMsgDistribute"+levelCode).html('Bạn chỉ được nhập duy nhất một hình thức khuyến mại % tiền hoặc, số lượng, hoặc số tiền').show();
				return;
			}
			var sumDetailQuantity = 0;
			var sumDetailAmount = 0;
			var totalDetailQuantity = 0;
			var totalDetailAmount = 0;
			if(dataModel.listSubLevelKM[i].listSubLevel != null && $.isArray(dataModel.listSubLevelKM[i].listSubLevel) && dataModel.listSubLevelKM[i].listSubLevel.length > 0) {
				for(var j = 0; j < dataModel.listSubLevelKM[i].listSubLevel.length; j++) {
					if(VTUtilJS.isNullOrEmpty(dataModel.listSubLevelKM[i].listSubLevel[j].productCode)) {
						$("#errorMsgDistribute"+levelCode).html('Bạn chưa nhập sản phẩm khuyến mại. Vui lòng nhập sản phẩm khuyến mại').show();
						return;
					}
					if(!VTUtilJS.isNullOrEmpty(dataModel.listSubLevelKM[i].percent) && !isNaN(dataModel.listSubLevelKM[i].percent) && Number(dataModel.listSubLevelKM[i].percent) > 0) {
						if(!VTUtilJS.isNullOrEmpty(dataModel.listSubLevelKM[i].maxQuantityKM) && !isNaN(dataModel.listSubLevelKM[i].maxQuantityKM) && Number(dataModel.listSubLevelKM[i].maxQuantityKM) > 0) {
							$("#errorMsgDistribute"+levelCode).html('Mức khuyến mãi đang nhập dạng phần trăm').show();
							return;
						} else if(!VTUtilJS.isNullOrEmpty(dataModel.listSubLevelKM[i].maxAmountKM) && !isNaN(dataModel.listSubLevelKM[i].maxAmountKM) && Number(dataModel.listSubLevelKM[i].maxAmountKM) > 0) {
							$("#errorMsgDistribute"+levelCode).html('Mức khuyến mãi đang nhập dạng phần trăm').show();
							return;
						} else if(!VTUtilJS.isNullOrEmpty(dataModel.listSubLevelKM[i].listSubLevel[j].quantity) && !isNaN(dataModel.listSubLevelKM[i].listSubLevel[j].quantity) && Number(dataModel.listSubLevelKM[i].listSubLevel[j].quantity) > 0) {
							$("#errorMsgDistribute"+levelCode).html('Mức khuyến mãi đang nhập dạng phần trăm').show();
							return;
						} else if(!VTUtilJS.isNullOrEmpty(dataModel.listSubLevelKM[i].listSubLevel[j].amount) && !isNaN(dataModel.listSubLevelKM[i].listSubLevel[j].amount) && Number(dataModel.listSubLevelKM[i].listSubLevel[j].amount) > 0) {
							$("#errorMsgDistribute"+levelCode).html('Mức khuyến mãi đang nhập dạng phần trăm').show();
							return;
						}
					} else if(!VTUtilJS.isNullOrEmpty(dataModel.listSubLevelKM[i].maxQuantityKM) && !isNaN(dataModel.listSubLevelKM[i].maxQuantityKM) && Number(dataModel.listSubLevelKM[i].maxQuantityKM) > 0) {
						if(!VTUtilJS.isNullOrEmpty(dataModel.listSubLevelKM[i].percent) && !isNaN(dataModel.listSubLevelKM[i].percent) && Number(dataModel.listSubLevelKM[i].percent) > 0) {
							$("#errorMsgDistribute"+levelCode).html('Mức khuyến mãi đang nhập dạng group-quantity').show();
							return;
						} else if(!VTUtilJS.isNullOrEmpty(dataModel.listSubLevelKM[i].maxAmountKM) && !isNaN(dataModel.listSubLevelKM[i].maxAmountKM) && Number(dataModel.listSubLevelKM[i].maxAmountKM) > 0) {
							$("#errorMsgDistribute"+levelCode).html('Mức khuyến mãi đang nhập dạng group-quantity').show();
							return;
						}
					} else if(!VTUtilJS.isNullOrEmpty(dataModel.listSubLevelKM[i].maxAmountKM) && !isNaN(dataModel.listSubLevelKM[i].maxAmountKM) && Number(dataModel.listSubLevelKM[i].maxAmountKM) > 0) {
						if(!VTUtilJS.isNullOrEmpty(dataModel.listSubLevelKM[i].percent) && !isNaN(dataModel.listSubLevelKM[i].percent) && Number(dataModel.listSubLevelKM[i].percent) > 0) {
							$("#errorMsgDistribute"+levelCode).html('Mức khuyến mãi đang nhập dạng group-amount').show();
							return;
						} else if(!VTUtilJS.isNullOrEmpty(dataModel.listSubLevelKM[i].maxQuantityKM) && !isNaN(dataModel.listSubLevelKM[i].maxQuantityKM) && Number(dataModel.listSubLevelKM[i].maxQuantityKM) > 0) {
							$("#errorMsgDistribute"+levelCode).html('Mức khuyến mãi đang nhập dạng group-amount').show();
							return;
						}
					} else if(!VTUtilJS.isNullOrEmpty(dataModel.listSubLevelKM[i].listSubLevel[j].quantity) && !isNaN(dataModel.listSubLevelKM[i].listSubLevel[j].quantity) && Number(dataModel.listSubLevelKM[i].listSubLevel[j].quantity) > 0) {
						if(!VTUtilJS.isNullOrEmpty(dataModel.listSubLevelKM[i].percent) && !isNaN(dataModel.listSubLevelKM[i].percent) && Number(dataModel.listSubLevelKM[i].percent) > 0) {
							$("#errorMsgDistribute"+levelCode).html('Mức khuyến mãi đang nhập số lượng chi tiết sản phẩm ' + Utils.XSSEncode(dataModel.listSubLevelKM[i].listSubLevel[j].productCode)).show();
							return;
						} else if(!VTUtilJS.isNullOrEmpty(dataModel.listSubLevelKM[i].maxAmountKM) && !isNaN(dataModel.listSubLevelKM[i].maxAmountKM) && Number(dataModel.listSubLevelKM[i].maxAmountKM) > 0) {
							$("#errorMsgDistribute"+levelCode).html('Mức khuyến mãi đang nhập số lượng chi tiết sản phẩm ' + Utils.XSSEncode(dataModel.listSubLevelKM[i].listSubLevel[j].productCode)).show();
							return;
						} else if(!VTUtilJS.isNullOrEmpty(dataModel.listSubLevelKM[i].listSubLevel[j].amount) && !isNaN(dataModel.listSubLevelKM[i].listSubLevel[j].amount) && Number(dataModel.listSubLevelKM[i].listSubLevel[j].amount) > 0) {
							$("#errorMsgDistribute"+levelCode).html('Mức khuyến mãi đang nhập số lượng chi tiết sản phẩm ' + Utils.XSSEncode(dataModel.listSubLevelKM[i].listSubLevel[j].productCode)).show();
							return;
						}
					} else if(!VTUtilJS.isNullOrEmpty(dataModel.listSubLevelKM[i].listSubLevel[j].amount) && !isNaN(dataModel.listSubLevelKM[i].listSubLevel[j].amount) && Number(dataModel.listSubLevelKM[i].listSubLevel[j].amount) > 0) {
						if(!VTUtilJS.isNullOrEmpty(dataModel.listSubLevelKM[i].percent) && !isNaN(dataModel.listSubLevelKM[i].percent) && Number(dataModel.listSubLevelKM[i].percent) > 0) {
							$("#errorMsgDistribute"+levelCode).html('Mức khuyến mãi đang nhập số tiền chi tiết sản phẩm ' + Utils.XSSEncode(dataModel.listSubLevelKM[i].listSubLevel[j].productCode)).show();
							return;
						} else if(!VTUtilJS.isNullOrEmpty(dataModel.listSubLevelKM[i].maxQuantityKM) && !isNaN(dataModel.listSubLevelKM[i].maxQuantityKM) && Number(dataModel.listSubLevelKM[i].maxQuantityKM) > 0) {
							$("#errorMsgDistribute"+levelCode).html('Mức khuyến mãi đang nhập số tiền chi tiết sản phẩm ' + Utils.XSSEncode(dataModel.listSubLevelKM[i].listSubLevel[j].productCode)).show();
							return;
						} else if(!VTUtilJS.isNullOrEmpty(dataModel.listSubLevelKM[i].listSubLevel[j].quantity) && !isNaN(dataModel.listSubLevelKM[i].listSubLevel[j].quantity) && Number(dataModel.listSubLevelKM[i].listSubLevel[j].quantity) > 0) {
							$("#errorMsgDistribute"+levelCode).html('Mức khuyến mãi đang nhập số tiền chi tiết sản phẩm ' + Utils.XSSEncode(dataModel.listSubLevelKM[i].listSubLevel[j].productCode)).show();
							return;
						}
					} else {
						$("#errorMsgDistribute"+levelCode).html('Mức khuyến mãi chưa nhập giá trị cho sản phẩm ' + Utils.XSSEncode(dataModel.listSubLevelKM[i].listSubLevel[j].productCode)).show();
						return;
					}
					
					if(!isQuantityInput && !VTUtilJS.isNullOrEmpty(dataModel.listSubLevelKM[i].listSubLevel[j].quantity) && !isNaN(dataModel.listSubLevelKM[i].listSubLevel[j].quantity) && Number(dataModel.listSubLevelKM[i].listSubLevel[j].quantity) > 0) {
						isQuantityInput = true;
					}
					if(!isAmountInput && !VTUtilJS.isNullOrEmpty(dataModel.listSubLevelKM[i].listSubLevel[j].amount) && !isNaN(dataModel.listSubLevelKM[i].listSubLevel[j].amount) && Number(dataModel.listSubLevelKM[i].listSubLevel[j].amount) > 0) {
						isAmountInput = true;
					}
					if(isQuantityInput && isAmountInput) {
						$("#errorMsgDistribute"+levelCode).html('Một mức chỉ có thể nhập số lượng khuyến mại hoặc số tiền khuyến mại.').show();
						return;
					}
					if(!isQuantityInput && !isAmountInput) {
						$("#errorMsgDistribute"+levelCode).html('Nhóm bắt buộc phải nhập số lượng khuyến mại hoặc số tiền khuyến mại.').show();
						return;
					}
					if(dataModel.listSubLevelKM[i].listSubLevel[j].isRequired == 1 || dataModel.listSubLevelKM[i].listSubLevel[j].isRequired == true || dataModel.listSubLevelKM[i].listSubLevel[j].isRequired == 'true') {
						sumDetailQuantity += VTUtilJS.isNullOrEmpty(dataModel.listSubLevelKM[i].listSubLevel[j].quantity) ? 0 : Number(dataModel.listSubLevelKM[i].listSubLevel[j].quantity);
						sumDetailAmount += VTUtilJS.isNullOrEmpty(dataModel.listSubLevelKM[i].listSubLevel[j].amount) ? 0 : Number(dataModel.listSubLevelKM[i].listSubLevel[j].amount);
					}
					totalDetailQuantity += VTUtilJS.isNullOrEmpty(dataModel.listSubLevelKM[i].listSubLevel[j].quantity) ? 0 : Number(dataModel.listSubLevelKM[i].listSubLevel[j].quantity);
					totalDetailAmount += VTUtilJS.isNullOrEmpty(dataModel.listSubLevelKM[i].listSubLevel[j].amount) ? 0 : Number(dataModel.listSubLevelKM[i].listSubLevel[j].amount);
					if($('#typeCode').val() == 'ZV03' || $('#typeCode').val() == 'ZV06' || $('#typeCode').val() == 'ZV09' || $('#typeCode').val() == 'ZV12' || $('#typeCode').val() == 'ZV15' || $('#typeCode').val() == 'ZV18' || $('#typeCode').val() == 'ZV21') {
						if(listProductCode.indexOf(dataModel.listSubLevelKM[i].listSubLevel[j].productCode) != -1) {
							$("#errorMsgDistribute"+levelCode).html('Sản phẩm ' + Utils.XSSEncode(dataModel.listSubLevelKM[i].listSubLevel[j].productCode) + ' đã tồn tại trong khuyến mại. Vui lòng kiểm tra lại').show();
							return;
						} else {
							listProductCode.push(dataModel.listSubLevelKM[i].listSubLevel[j].productCode);
						}
						if(VTUtilJS.isNullOrEmpty(dataModel.listSubLevelKM[i].listSubLevel[j].productCode)) {
							$("#errorMsgDistribute"+levelCode).html('Bạn chưa nhập sản phẩm khuyến mại. Vui lòng nhập sản phẩm khuyến mại').show();
							return;
						} else if((VTUtilJS.isNullOrEmpty(dataModel.listSubLevelKM[i].listSubLevel[j].quantity) || isNaN(dataModel.listSubLevelKM[i].listSubLevel[j].quantity) || Number(dataModel.listSubLevelKM[i].listSubLevel[j].quantity) ==0)
								&& (VTUtilJS.isNullOrEmpty(dataModel.listSubLevelKM[i].listSubLevel[j].amount) || isNaN(dataModel.listSubLevelKM[i].listSubLevel[j].amount) || Number(dataModel.listSubLevelKM[i].listSubLevel[j].amount) == 0)) {
							$("#errorMsgDistribute"+levelCode).html('Bạn chưa nhập số lượng hoặc số tiền cho sản phẩm khuyến mại ' + Utils.XSSEncode(dataModel.listSubLevelKM[i].listSubLevel[j].productCode)).show();
							return;
						}
					}
				}
			}
			
			if(!isPercentInput && !isQuantityInput && !isAmountInput) {
				$("#errorMsgDistribute"+levelCode).html('Nhóm bắt buộc phải nhập số lượng khuyến mại hoặc số tiền khuyến mại.').show();
				return;
			}
			
			if(isQuantityInput && !VTUtilJS.isNullOrEmpty(dataModel.listSubLevelKM[i].maxQuantityKM) && !isNaN(dataModel.listSubLevelKM[i].maxQuantityKM) && Number(dataModel.listSubLevelKM[i].maxQuantityKM) > 0) {
				if(totalDetailQuantity > Number(dataModel.listSubLevelKM[i].maxQuantityKM)) {
					$("#errorMsgDistribute"+levelCode).html('Số lượng nhóm phải lớn hơn hoặc bằng tổng số lượng các sản phẩm khuyến mại').show();
					return;
				}
			}
			if(isAmountInput && !VTUtilJS.isNullOrEmpty(dataModel.listSubLevelKM[i].maxAmountKM) && !isNaN(dataModel.listSubLevelKM[i].maxAmountKM) && Number(dataModel.listSubLevelKM[i].maxAmountKM) > 0) {
				if(sumDetailAmount > Number(dataModel.listSubLevelKM[i].maxAmountKM)) {
					$("#errorMsgDistribute"+levelCode).html('Số tiền nhóm phải lớn hơn hoặc bằng tổng số tiền các sản phẩm bắt buộc khuyến mại').show();
					return;
				}
			}
		}
		var listProductCode = new Array();
		for(var i = 0; i < dataModel.listSubLevelMua.length; i++) {
			var isQuantityInput = false;
			var isAmountInput = false;
			if(!VTUtilJS.isNullOrEmpty(dataModel.listSubLevelMua[i].minQuantityMua) && !isNaN(dataModel.listSubLevelMua[i].minQuantityMua) && Number(dataModel.listSubLevelMua[i].minQuantityMua) > 0) {
				isQuantityInput = true;
			}
			if(!VTUtilJS.isNullOrEmpty(dataModel.listSubLevelMua[i].minAmountMua) && !isNaN(dataModel.listSubLevelMua[i].minAmountMua) && Number(dataModel.listSubLevelMua[i].minAmountMua) > 0) {
				isAmountInput = true;
			}
			if(isQuantityInput && isAmountInput) {
				$("#errorMsgDistribute"+levelCode).html('Một mức chỉ có thể nhập số lượng mua hoặc số tiền mua.').show();
				return;
			}
			var sumDetailQuantity = 0;
			var sumDetailAmount = 0;
			var totalDetailQuantity = 0;
			var totalDetailAmount = 0;
			if(dataModel.listSubLevelMua[i].listSubLevel != null && $.isArray(dataModel.listSubLevelMua[i].listSubLevel) && dataModel.listSubLevelMua[i].listSubLevel.length > 0) {
				for(var j = 0; j < dataModel.listSubLevelMua[i].listSubLevel.length; j++) {
					if(VTUtilJS.isNullOrEmpty(dataModel.listSubLevelMua[i].listSubLevel[j].productCode)) {
						$("#errorMsgDistribute"+levelCode).html('Bạn chưa nhập sản phẩm mua. Vui lòng nhập sản phẩm mua').show();
						return;
					}
					var promoType = $('#typeCode').val().trim();
					if ((promoType == "ZV01" || promoType == "ZV02" || promoType == "ZV03" || promoType == "ZV04" || promoType == "ZV05" || promoType == "ZV06" ||
						promoType == "ZV13" || promoType == "ZV14" || promoType == "ZV15" || promoType == "ZV16" || promoType == "ZV17" || promoType == "ZV18")
						&& dataModel.listSubLevelMua[i].listSubLevel[j].isRequired != 1) {
						$("#errorMsgDistribute"+levelCode).html('Bạn phải check chọn bắt buộc cho sản phẩm bán đối với KM dạng line, bundle').show();
						return;
					}

					if(!VTUtilJS.isNullOrEmpty(dataModel.listSubLevelMua[i].minQuantityMua) && !isNaN(dataModel.listSubLevelMua[i].minQuantityMua) && Number(dataModel.listSubLevelMua[i].minQuantityMua) > 0) {
						if(!VTUtilJS.isNullOrEmpty(dataModel.listSubLevelMua[i].minAmountMua) && !isNaN(dataModel.listSubLevelMua[i].minAmountMua) && Number(dataModel.listSubLevelMua[i].minAmountMua) > 0) {
							$("#errorMsgDistribute"+levelCode).html('Mức mua đang nhập dạng group-quantity').show();
							return;
						}
					} else if(!VTUtilJS.isNullOrEmpty(dataModel.listSubLevelMua[i].minAmountMua) && !isNaN(dataModel.listSubLevelMua[i].minAmountMua) && Number(dataModel.listSubLevelMua[i].minAmountMua) > 0) {
						if(!VTUtilJS.isNullOrEmpty(dataModel.listSubLevelMua[i].minQuantityMua) && !isNaN(dataModel.listSubLevelMua[i].minQuantityMua) && Number(dataModel.listSubLevelMua[i].minQuantityMua) > 0) {
							$("#errorMsgDistribute"+levelCode).html('Mức mua đang nhập dạng group-amount').show();
							return;
						}
					} else if(!VTUtilJS.isNullOrEmpty(dataModel.listSubLevelMua[i].listSubLevel[j].quantity) && !isNaN(dataModel.listSubLevelMua[i].listSubLevel[j].quantity) && Number(dataModel.listSubLevelMua[i].listSubLevel[j].quantity) > 0) {
						if(!VTUtilJS.isNullOrEmpty(dataModel.listSubLevelMua[i].minAmountMua) && !isNaN(dataModel.listSubLevelMua[i].minAmountMua) && Number(dataModel.listSubLevelMua[i].minAmountMua) > 0) {
							$("#errorMsgDistribute"+levelCode).html('Mức mua đang nhập số lượng chi tiết sản phẩm ' + Utils.XSSEncode(dataModel.listSubLevelMua[i].listSubLevel[j].productCode)).show();
							return;
						} else if(!VTUtilJS.isNullOrEmpty(dataModel.listSubLevelMua[i].listSubLevel[j].amount) && !isNaN(dataModel.listSubLevelMua[i].listSubLevel[j].amount) && Number(dataModel.listSubLevelMua[i].listSubLevel[j].amount) > 0) {
							$("#errorMsgDistribute"+levelCode).html('Mức mua đang nhập số lượng chi tiết sản phẩm ' + Utils.XSSEncode(dataModel.listSubLevelMua[i].listSubLevel[j].productCode)).show();
							return;
						}
					} else if(!VTUtilJS.isNullOrEmpty(dataModel.listSubLevelMua[i].listSubLevel[j].amount) && !isNaN(dataModel.listSubLevelMua[i].listSubLevel[j].amount) && Number(dataModel.listSubLevelMua[i].listSubLevel[j].amount) > 0) {
						if(!VTUtilJS.isNullOrEmpty(dataModel.listSubLevelMua[i].minQuantityMua) && !isNaN(dataModel.listSubLevelMua[i].minQuantityMua) && Number(dataModel.listSubLevelMua[i].minQuantityMua) > 0) {
							$("#errorMsgDistribute"+levelCode).html('Mức mua đang nhập số tiền chi tiết sản phẩm ' + Utils.XSSEncode(dataModel.listSubLevelMua[i].listSubLevel[j].productCode)).show();
							return;
						}else if(!VTUtilJS.isNullOrEmpty(dataModel.listSubLevelMua[i].listSubLevel[j].quantity) && !isNaN(dataModel.listSubLevelMua[i].listSubLevel[j].quantity) && Number(dataModel.listSubLevelMua[i].listSubLevel[j].quantity) > 0) {
							$("#errorMsgDistribute"+levelCode).html('Mức mua đang nhập số tiền chi tiết sản phẩm ' + Utils.XSSEncode(dataModel.listSubLevelMua[i].listSubLevel[j].productCode)).show();
							return;
						}
					} else {
						$("#errorMsgDistribute"+levelCode).html('Mức mua chưa nhập giá trị cho sản phẩm ' + Utils.XSSEncode(dataModel.listSubLevelMua[i].listSubLevel[j].productCode)).show();
						return;
					}
					
					if(!isQuantityInput && !VTUtilJS.isNullOrEmpty(dataModel.listSubLevelMua[i].listSubLevel[j].quantity) && !isNaN(dataModel.listSubLevelMua[i].listSubLevel[j].quantity) && Number(dataModel.listSubLevelMua[i].listSubLevel[j].quantity) > 0) {
						isQuantityInput = true;
					}
					if(!isAmountInput && !VTUtilJS.isNullOrEmpty(dataModel.listSubLevelMua[i].listSubLevel[j].amount) && !isNaN(dataModel.listSubLevelMua[i].listSubLevel[j].amount) && Number(dataModel.listSubLevelMua[i].listSubLevel[j].amount) > 0) {
						isAmountInput = true;
					}
					if(isQuantityInput && isAmountInput) {
						$("#errorMsgDistribute"+levelCode).html('Một mức chỉ có thể nhập số lượng mua hoặc số tiền mua.').show();
						return;
					}
					if(!isQuantityInput && !isAmountInput) {
						$("#errorMsgDistribute"+levelCode).html('Nhóm bắt buộc phải nhập số lượng mua hoặc số tiền mua.').show();
						return;
					}
					/**
					 * 
					 */
					if(dataModel.listSubLevelMua[i].listSubLevel[j].isRequired == 1 || dataModel.listSubLevelMua[i].listSubLevel[j].isRequired == true || dataModel.listSubLevelMua[i].listSubLevel[j].isRequired == 'true') {
						sumDetailQuantity += VTUtilJS.isNullOrEmpty(dataModel.listSubLevelMua[i].listSubLevel[j].quantity) ? 0 : Number(dataModel.listSubLevelMua[i].listSubLevel[j].quantity);
						sumDetailAmount += VTUtilJS.isNullOrEmpty(dataModel.listSubLevelMua[i].listSubLevel[j].amount) ? 0 : Number(dataModel.listSubLevelMua[i].listSubLevel[j].amount);
					}
					totalDetailQuantity += VTUtilJS.isNullOrEmpty(dataModel.listSubLevelMua[i].listSubLevel[j].quantity) ? 0 : Number(dataModel.listSubLevelMua[i].listSubLevel[j].quantity);
					totalDetailAmount += VTUtilJS.isNullOrEmpty(dataModel.listSubLevelMua[i].listSubLevel[j].amount) ? 0 : Number(dataModel.listSubLevelMua[i].listSubLevel[j].amount);
					if($('#typeCode').val() != 'ZV07' && $('#typeCode').val() != 'ZV08' && $('#typeCode').val() != 'ZV09' && $('#typeCode').val() != 'ZV10' && $('#typeCode').val() != 'ZV11' && $('#typeCode').val() != 'ZV12') {
						if(listProductCode.indexOf(dataModel.listSubLevelMua[i].listSubLevel[j].productCode) != -1) {
							$("#errorMsgDistribute"+levelCode).html('Sản phẩm ' + Utils.XSSEncode(dataModel.listSubLevelMua[i].listSubLevel[j].productCode) + ' đã tồn tại trong mức mua. Vui lòng kiểm tra lại').show();
							return;
						} else {
							listProductCode.push(dataModel.listSubLevelMua[i].listSubLevel[j].productCode);
						}
						if(VTUtilJS.isNullOrEmpty(dataModel.listSubLevelMua[i].listSubLevel[j].productCode)) {
							$("#errorMsgDistribute"+levelCode).html('Bạn chưa nhập sản phẩm mua. Vui lòng nhập sản phẩm mua').show();
							return;
						} else if((VTUtilJS.isNullOrEmpty(dataModel.listSubLevelMua[i].listSubLevel[j].quantity) || isNaN(dataModel.listSubLevelMua[i].listSubLevel[j].quantity) || Number(dataModel.listSubLevelMua[i].listSubLevel[j].quantity) ==0)
								&& (VTUtilJS.isNullOrEmpty(dataModel.listSubLevelMua[i].listSubLevel[j].amount) || isNaN(dataModel.listSubLevelMua[i].listSubLevel[j].amount) || Number(dataModel.listSubLevelMua[i].listSubLevel[j].amount) == 0)) {
							$("#errorMsgDistribute"+levelCode).html('Bạn chưa nhập số lượng hoặc số tiền cho sản phẩm mua ' + Utils.XSSEncode(dataModel.listSubLevelMua[i].listSubLevel[j].productCode)).show();
							return;
						}
					} else {
						if(listProductCode.indexOf(dataModel.listSubLevelMua[i].listSubLevel[j].productCode) != -1) {
							$("#errorMsgDistribute"+levelCode).html('Sản phẩm ' + Utils.XSSEncode(dataModel.listSubLevelMua[i].listSubLevel[j].productCode) + ' đã tồn tại trong mức mua. Vui lòng kiểm tra lại').show();
							return;
						} else {
							listProductCode.push(dataModel.listSubLevelMua[i].listSubLevel[j].productCode);
						}
					}
				}
			} else if($('#typeCode').val() != 'ZV19' && $('#typeCode').val() != 'ZV20' && $('#typeCode').val() != 'ZV21') {
				$("#errorMsgDistribute"+levelCode).html('Chương trình khuyến mại nhóm mua bắt buộc phải có sản phẩm').show();
				return;
			}
			
			if(!isQuantityInput && !isAmountInput) {
				$("#errorMsgDistribute"+levelCode).html('Nhóm bắt buộc phải nhập số lượng mua hoặc số tiền mua.').show();
				return;
			}
			
			if(isQuantityInput && !VTUtilJS.isNullOrEmpty(dataModel.listSubLevelMua[i].minQuantityMua) && !isNaN(dataModel.listSubLevelMua[i].minQuantityMua) && Number(dataModel.listSubLevelMua[i].minQuantityMua) > 0) {
				if(totalDetailQuantity > Number(dataModel.listSubLevelMua[i].minQuantityMua)) {
					$("#errorMsgDistribute"+levelCode).html('Số lượng nhóm phải lớn hơn hoặc bằng tổng số lượng các sản phẩm mua').show();
					return;
				}
			}
			if(isAmountInput && !VTUtilJS.isNullOrEmpty(dataModel.listSubLevelMua[i].minAmountMua) && !isNaN(dataModel.listSubLevelMua[i].minAmountMua) && Number(dataModel.listSubLevelMua[i].minAmountMua) > 0) {
				if(sumDetailAmount > Number(dataModel.listSubLevelMua[i].minAmountMua)) {
					$("#errorMsgDistribute"+levelCode).html('Số tiền nhóm phải lớn hơn hoặc bằng tổng số tiền các sản phẩm bắt buộc mua').show();
					return;
				}
			}
		}
		$.messager.confirm("Xác nhận", "Bạn có muốn lưu thông tin?", function(r) {
			if(r) {
				VTUtilJS.postFormJson(VTUtilJS.getSimpleObject(dataModel), '/promotion/new-save-level', function(data){
					if(data.error) {
						$("#errorMsgDistribute"+levelCode).html(data.errMsg).show();
					} else {
						$("#successMsgDistribute"+levelCode).html('Lưu dữ liệu thành công').show();
						setTimeout(function() {
							$("#successMsgDistribute"+levelCode).hide();
						}, 3000);
						loadLevelDetail();
					}
				});
			}
		});
	},
	renderPannelEx : function(list, i, groupMuaId, groupKMId) {
		if(list.length == i) {
			return;
		}
		$($('#mappingGrid tbody')[0]).append('<tr>\
						<td>\
						<p class="ErrorMsgStyle SpriteErr" style="display: none;" id="errorMsgDistribute'+(VTUtilJS.isNullOrEmpty(list[i].levelCode) ? list[i].mapId : list[i].levelCode+list[i].mapId)+'">\
						<p id="successMsgDistribute'+(VTUtilJS.isNullOrEmpty(list[i].levelCode) ? list[i].mapId : list[i].levelCode+list[i].mapId)+'" class="SuccessMsgStyle" style="display: none"></p>\
						<div class="TableLevel" id="tableLevel'+(list[i].mapId)+'" style="width:100%; height:\'auto\';padding:10px;" title="">\
				<table style="width: 100%;">\
		    	<tr>\
		    		<td style="width: 50%; vertical-align: top;">\
		    			<table id="tableSubLevelMua'+(VTUtilJS.isNullOrEmpty(list[i].levelCode) ? list[i].mapId : list[i].levelCode+list[i].mapId)+'" style="border: 1px solid black; vertical-align: top;width: 100%">\
		    				<tbody>\
		    				</tbody>\
		    			</table>\
		    		</td>\
		    		<td style="width: 50%; vertical-align: top;">\
		    			<table id="tableSubLevelKM'+(VTUtilJS.isNullOrEmpty(list[i].levelCode) ? list[i].mapId : list[i].levelCode+list[i].mapId)+'" style="border: 1px solid black; vertical-align: top;width: 100%">\
		    				<tbody>\
		    				</tbody>\
		    			</table>\
		    		</td>\
		    	</tr>\
			</table>\
				    </div>\
			</td>\
		</tr>');
		VTUtilJS.bindFormatOnTextfield('sttMua_'+(VTUtilJS.isNullOrEmpty(list[i].levelCode) ? list[i].mapId : list[i].levelCode+list[i].mapId), VTUtilJS._TF_NUMBER);
		$('#tableLevel'+(list[i].mapId)).panel({
			width:'95%',
			height:'auto',
			collapsible:true,
			title: VTUtilJS.isNullOrEmpty(list[i].levelCode) ? 'STT : ' + list[i].stt : 'Mã : ' + Utils.XSSEncode(list[i].levelCode) + ' - ' + 'STT : ' + list[i].stt,
			tools:[{
				iconCls:'iconAdd '+i+'',
				handler:function(){
					var i = Number(Number($(this).attr('class').split(' ')[1]));
					PromotionProgram.addHtmlLevel('tableLevel'+(list[i].mapId)+'',groupMuaId, groupKMId);
				}
			}, {
				iconCls:'iconEdit '+i+'',
				handler:function(){
					var i = Number($(this).attr('class').split(' ')[1]);
					PromotionProgram.editHtmlLevel('tableLevel'+(list[i].mapId), groupMuaId, groupKMId, list[i].mapId, list[i].levelMuaId, list[i].levelKMId, (VTUtilJS.isNullOrEmpty(list[i].levelCode) ? '' : list[i].levelCode), (list[i].stt == null || list[i].stt == undefined) ? 0 : list[i].stt);
				}
			}, {
				iconCls:'iconDelete '+i+'',
				handler:function(){
					var i = Number($(this).attr('class').split(' ')[1]);
					PromotionProgram.deleteHtmlLevel('tableLevel'+(list[i].mapId), groupMuaId, groupKMId, list[i].mapId, list[i].levelMuaId, list[i].levelKMId);
				}
			}, {
				iconCls:'iconCopy '+i+'',
				handler:function(){
					var i = Number($(this).attr('class').split(' ')[1]);
					PromotionProgram.copyHtmlLevel(list[i].mapId, groupMuaId, groupKMId);
				}
			}, {
				iconCls:'iconSave '+i+'',
				handler:function(){
					var i = Number($(this).attr('class').split(' ')[1]);
					PromotionProgram.saveHtmlLevel((VTUtilJS.isNullOrEmpty(list[i].levelCode) ? list[i].mapId : list[i].levelCode+list[i].mapId), groupMuaId, groupKMId, list[i].mapId, list[i].levelMuaId, list[i].levelKMId);
				}
			}]
		});
		if(list[i].listExLevelMua != null && list[i].listExLevelMua != undefined && $.isArray(list[i].listExLevelMua)) {
			PromotionProgram.addHtmlSubRowMua('tableSubLevelMua'+(VTUtilJS.isNullOrEmpty(list[i].levelCode) ? list[i].mapId : list[i].levelCode+list[i].mapId), VTUtilJS.isNullOrEmpty(list[i].levelCode) ? list[i].mapId : list[i].levelCode+list[i].mapId, list[i].listExLevelMua);
		}
		if(list[i].listExLevelKM != null && list[i].listExLevelKM != undefined && $.isArray(list[i].listExLevelKM)) {
			PromotionProgram.addHtmlSubRowKM('tableSubLevelKM'+(VTUtilJS.isNullOrEmpty(list[i].levelCode) ? list[i].mapId : list[i].levelCode+list[i].mapId), VTUtilJS.isNullOrEmpty(list[i].levelCode) ? list[i].mapId : list[i].levelCode+list[i].mapId, list[i].listExLevelKM);
		}
		setTimeout(function() {
			PromotionProgram.renderPannelEx(list, ++i, groupMuaId, groupKMId);
		}, 100);
	},
	copyHtmlLevel : function(mapId, groupMuaId, groupKMId) {
		$('.ErrorMsgStyle').hide();
		if($('#statusHidden').val() == 0 || $('#statusHidden').val() == 1){
			return;
		}
		var html = '<div id="popup-container"><div id="popup-add-level" class="PopupContentMid">\
			<div class="GeneralForm Search1Form" id="form-data">\
			<div style="padding: 10px 10px 20px;">Bạn có muốn thực hiện sao chép mức?</div>\
			<label style="width: 100px;display:none;" class="LabelStyle Label1Style">Số lần<span class="ReqiureStyle">(*)</span></label> \
			<input type="text" class="InputTextStyle InputText1Style" style="width: 50px;display:none;" maxlength="3" id="copyNum" value="1">\
			<input type="hidden" id="mappingId" value="'+mapId+'">\
			<div class="Clear"></div>\
			<div class="BtnCenterSection">\
			<button class="BtnGeneralStyle" id="btn-update">Đồng ý</button>\
			</div>\
			<p class="ErrorMsgStyle SpriteErr" style="display: none;" id="errMsgLevelDlg"></p>\
			<p class="SuccessMsgStyle" style="display: none;" id="successMsgLevelDlg"></p>\
			</div></div></div>';
		$("body").append(html);
		$('#popup-add-level').dialog({
			title: "Sao chép mức",
			width: 250,
			heigth: "auto",
			modal: true, close: false, cache: false,
			onOpen: function() {
				VTUtilJS.bindFormatOnTextfield('copyNum', VTUtilJS._TF_NUMBER);
				$('#btn-update').bind('click', function() {
					if($('#form-data #copyNum').val() == '') {
						$('#popup-container #errMsgLevelDlg').html('Bạn chưa nhập số lần sao chép').show();
						return;
					}
					PromotionProgram.isEndLoad = true;
					PromotionProgram.isFinishLoad = true;
					VTUtilJS.postFormJson('form-data', '/promotion/new-copy-level', function(data){
						if(data.error) {
							$('#errMsgLevelDlg').html(data.errMsg).show();
						} else {
							$('#popup-add-level').dialog('close');
							var list = data.list;
							if(list != null && list != undefined && $.isArray(list) && list.length > 0) {
								PromotionProgram.renderPannelEx(list, 0, groupMuaId, groupKMId);
							}
						}
					});
				});
			},
			onClose: function() {
				$("#popup-add-level").dialog("destroy");
				$("#popup-container").remove();
			}
		});
	},
	addNewProductConvert: function() {
		var html = '<div id="popup-container"><div id="popup-add-level" class="PopupContentMid">\
			<div class="GeneralForm Search1Form" id="form-data">\
			<label style="width: 100px;" class="LabelStyle Label1Style">Nhóm<span class="ReqiureStyle">(*)</span></label> \
			<input type="text" class="InputTextStyle InputText1Style" style="width: 50px;" call-back="VTValidateUtils.getMessageOfRequireCheck(\'name\', \'Nhóm\', false, true);VTValidateUtils.getMessageOfSpecialCharactersValidate(\'name\', \'Nhóm\', Utils._NAME)" maxlength="100" id="name">\
			<div class="Clear"></div>\
			<div class="BtnCenterSection">\
			<button class="BtnGeneralStyle" id="btn-update">Lưu</button>\
			</div>\
			<p class="ErrorMsgStyle SpriteErr" style="display: none;" id="errMsgLevelDlg"></p>\
			<p class="SuccessMsgStyle" style="display: none;" id="successMsgLevelDlg"></p>\
			</div></div></div>';
		$("body").append(html);
		$('#popup-add-level').dialog({
			title: "Thêm nhóm",
			width: 210,
			heigth: "auto",
			modal: true, close: false, cache: false,
			onOpen: function() {
				$('#btn-update').bind('click', function() {
					var params = VTUtilJS.getFormData('form-data');
					$('#popup-add-level').dialog('close');
					if(!VTUtilJS.isNullOrEmpty(params.name)) {
						var obj = {name : params.name.toUpperCase(), listDetail : []};
						$('#convertGrid').datagrid('appendRow', obj);
						$('#convertGrid').datagrid('loadData', $('#convertGrid').datagrid('getRows'));
					}
				});
			},
			onClose: function() {
				$("#popup-add-level").dialog("destroy");
				$("#popup-container").remove();
			}
		});
	},
	deleteProductConvert: function(index, id) {
		$('#errMsgConvert').html('').hide();
		if(!VTUtilJS.isNullOrEmpty(id)) {
			$.messager.confirm("Xác nhận", "Bạn có muốn xóa nhóm?", function(r) {
				if(r) {
					VTUtilJS.postFormJson({id:id}, '/promotion/product-convert-delete', function(data) {
						if(data.error) {
							$('#errMsgConvert').html(data.errMsg).show();
						} else {
							$('#convertGrid').datagrid('getRows').splice(index, 1);
							$('#convertGrid').datagrid('loadData', $('#convertGrid').datagrid('getRows'));
						}
					});
				}
			});
		} else {
			$('#convertGrid').datagrid('getRows').splice(index, 1);
			$('#convertGrid').datagrid('loadData', $('#convertGrid').datagrid('getRows'));
		}
	},
	addNewProductConvertDetail: function(index) {
		var ddv = $('#convertGrid').datagrid('getRowDetail',index).find('table.ddv');
		var __rows = ddv.datagrid('getRows');
		if(__rows != null && $.isArray(__rows)) {
			for(var ii = 0; ii < __rows.length; ii++) {
				if(VTUtilJS.isNullOrEmpty(__rows[ii].productCode)){
					$('#errMsgConvert').html("Xin nhập mã sản phẩm tại dòng mới thêm vào!").show();
					return;
				} else if (VTUtilJS.isNullOrEmpty(__rows[ii].factor)) {
					$('#errMsgConvert').html("Xin nhập hệ số quy đổi tại dòng mới thêm vào!").show();
					return;
				}
			}
		}
		ddv.datagrid('acceptChanges');
		ddv.datagrid('appendRow', {isSourceProduct:0});
		ddv.datagrid('beginEdit', ddv.datagrid('getRows').length-1);
		$('#convertGrid').datagrid('fixRowHeight', index);
	},
	deleteProductConvertDetail: function(index, id,idxParent) {
		$('#errMsgConvert').html('').hide();
		if(!VTUtilJS.isNullOrEmpty(id)) {
			$.messager.confirm("Xác nhận", "Bạn có muốn xóa sản phẩm của nhóm?", function(r) {
				if(r) {
					VTUtilJS.postFormJson({id:id}, '/promotion/product-convert-detail-delete', function(data) {
						if(data.error) {
							$('#errMsgConvert').html(data.errMsg).show();
						} else {
							var ddv = $('#convertGrid').datagrid('getRowDetail',idxParent).find('table.ddv');
							ddv.datagrid('deleteRow',index);
							ddv.datagrid('loadData', ddv.datagrid('getRows'));
						}
					});
				}
			});
		} else {
			var ddv = $('#convertGrid').datagrid('getRowDetail',idxParent).find('table.ddv');
			ddv.datagrid('deleteRow',index);
			ddv.datagrid('loadData', ddv.datagrid('getRows'));
		}
	},
	saveProductConvert: function() {
		$('#errMsgConvert').html('').hide();
		var params = new Object();
		params.promotionId = $('#id').val();																															
		params.listConvertGroup = $('#convertGrid').datagrid('getRows');
		if(!$.isArray(params.listConvertGroup) || params.listConvertGroup == null || params.listConvertGroup.length == 0) {
			return;
		}
		var listTotalProduct = new Array();
		for(var i = 0; i < params.listConvertGroup.length; i++) {
			var listProductCode = new Array();
			var isHashMore2 = false;
			var isHashRoot = 0;
			var isHashNonRoot = false;
			for(var j = 0; j < params.listConvertGroup[i].listDetail.length; j++) {
				var pp = $("#promotionConvertGrid .datagrid-row-detail .datagrid-wrap")[i];
				var td = $(pp).find(".datagrid-row td[field=productCode]")[j];
				if ($(td).find("input.combo-text.validatebox-text:visible").length > 0) {
					var pcode = $(td).find("input.combo-text.validatebox-text:visible").val().trim();
					if (pcode == ""){
						$('#errMsgConvert').html('Sản phẩm thứ ' +  (j+1) + ' trong nhóm ' + Utils.XSSEncode(params.listConvertGroup[i].name) + ' không được để trống').show();
						return;
					} else if (pcode != null && (params.listConvertGroup[i].listDetail[j].productCode == null || params.listConvertGroup[i].listDetail[j].productCode == "")) {
						$('#errMsgConvert').html('Sản phẩm ' +  Utils.XSSEncode(pcode) + ' trong nhóm ' + Utils.XSSEncode(params.listConvertGroup[i].name) + ' không tồn tại trong hệ thống').show();
						return;
					}
				}
				if(listProductCode.indexOf(params.listConvertGroup[i].listDetail[j].productCode) != -1) {
					$('#errMsgConvert').html('Sản phẩm ' +  Utils.XSSEncode(params.listConvertGroup[i].listDetail[j].productCode) + ' trong nhóm ' + Utils.XSSEncode(params.listConvertGroup[i].name) + ' đang bị trùng').show();
					return;
				} else if(VTUtilJS.isNullOrEmpty(params.listConvertGroup[i].listDetail[j].factor) || isNaN(params.listConvertGroup[i].listDetail[j].factor) || Number(params.listConvertGroup[i].listDetail[j].factor) <= 0) {
					$('#errMsgConvert').html('Dòng sản phẩm ' +  Utils.XSSEncode(params.listConvertGroup[i].listDetail[j].productCode) + ' trong nhóm ' + Utils.XSSEncode(params.listConvertGroup[i].name) + ' chưa nhập hệ số quy đổi').show();
					return;
				} else if(isNaN(params.listConvertGroup[i].listDetail[j].factor) || Number(params.listConvertGroup[i].listDetail[j].factor) <= 0) {
					$('#errMsgConvert').html('Dòng sản phẩm ' +  Utils.XSSEncode(params.listConvertGroup[i].listDetail[j].productCode) + ' trong nhóm ' + Utils.XSSEncode(params.listConvertGroup[i].name) + ' đang có hệ số quy đổi nhỏ hơn hoặc bằng 0').show();
					return;
				} else {
					listProductCode.push(params.listConvertGroup[i].listDetail[j].productCode);
				}
				if(j > 0) {
					isHashMore2 = true;
				}
				if(params.listConvertGroup[i].listDetail[j].isSourceProduct == 1) {
					isHashRoot += 1;
					if(PromotionProgram._listSaleProduct.get(params.listConvertGroup[i].listDetail[j].productCode) == null) {
						$('#errMsgConvert').html('Sản phẩm ' +  Utils.XSSEncode(params.listConvertGroup[i].listDetail[j].productCode) + ' trong nhóm ' + Utils.XSSEncode(params.listConvertGroup[i].name) + ' không phải là sản phẩm trong danh cơ cấu nhóm mua').show();
						return;
					}
				} else {
					isHashNonRoot = false;
				}
				if(listTotalProduct.indexOf(params.listConvertGroup[i].listDetail[j].productCode) != -1) {
					$('#errMsgConvert').html('Sản phẩm ' +  Utils.XSSEncode(params.listConvertGroup[i].listDetail[j].productCode) + ' trong nhóm ' + Utils.XSSEncode(params.listConvertGroup[i].name) + ' không được xuất hiện hai lần').show();
					return;
				} else {
					listTotalProduct.push(params.listConvertGroup[i].listDetail[j].productCode);
				}
			}
			if(!isHashMore2) {
				$('#errMsgConvert').html('Nhóm ' + Utils.XSSEncode(params.listConvertGroup[i].name) + ' phải có ít nhất 2 sản phẩm').show();
				return;
			}
			if(isHashRoot == 0) {
				$('#errMsgConvert').html('Nhóm ' + Utils.XSSEncode(params.listConvertGroup[i].name) + ' phải có sản phẩm gốc').show();
				return;
			} else if(isHashRoot > 1) {
				$('#errMsgConvert').html('Nhóm ' + Utils.XSSEncode(params.listConvertGroup[i].name) + ' đang có 2 sản phẩm gốc').show();
				return;
			}
		}
		$.messager.confirm("Xác nhận", "Bạn có muốn lưu sản phẩm quy đổi?", function(r) {
			if(r) {
				VTUtilJS.postFormJson(VTUtilJS.getSimpleObject(params), '/promotion/product-convert-save', function(data){
					if(data.error) {
						$('#errMsgConvert').html(data.errMsg).show();
					} else {
						$("#successMsgConvert").html('Lưu dữ liệu thành công').show();
						setTimeout(function() {
							$("#successMsgConvert").html('').hide();
							$('#convertGrid').datagrid('reload');
						}, 3000);
					}
				});
			}
		});
	},
	addProductOpenNew: function() {
		$('#openGrid').datagrid('appendRow', {});
		$('#openGrid').datagrid('beginEdit', $('#openGrid').datagrid('getRows').length - 1);
	},
	deleteProductOpenNew: function(index, id) {
		$('#errMsgOpen').html('').hide();
		if(VTUtilJS.isNullOrEmpty(id)) {
			$('#openGrid').datagrid('getRows').splice(index, 1);
			$('#openGrid').datagrid('loadData', $('#openGrid').datagrid('getRows'));
		} else {
			$.messager.confirm("Xác nhận", "Bạn có muốn lưu sản phẩm quy đổi?", function(r) {
				if(r) {
					VTUtilJS.postFormJson({id:id}, '/promotion/product-open-new-delete', function(data) {
						if(data.error) {
							$('#errMsgOpen').html(data.errMsg).show();
						} else {
							$('#openGrid').datagrid('getRows').splice(index, 1);
							$('#openGrid').datagrid('loadData', $('#openGrid').datagrid('getRows'));
						}
					});
				}
			});
		}
	},
	saveProductOpenNew: function() {
		$('#errMsgOpen').html('').hide();
		var params = new Object();
		params.promotionId = $('#id').val();
		params.listProductOpen = $('#openGrid').datagrid('getRows');
		if(!$.isArray(params.listProductOpen) || params.listProductOpen == null || params.listProductOpen.length == 0) {
			return;
		}
		for(var i = 0; i < params.listProductOpen.length; i++) {
			if(VTUtilJS.isNullOrEmpty(params.listProductOpen[i].productCode)) {
				$('#errMsgOpen').html('Dòng '+(i+1)+' chưa chọn sản phẩm').show();
				return;
			} else if((VTUtilJS.isNullOrEmpty(params.listProductOpen[i].quantity) || isNaN(params.listProductOpen[i].quantity) || Number(params.listProductOpen[i].quantity) <= 0) 
					&& (VTUtilJS.isNullOrEmpty(params.listProductOpen[i].amount) || isNaN(params.listProductOpen[i].amount) || Number(params.listProductOpen[i].amount) <= 0)) {
				$('#errMsgOpen').html('Dòng '+(i+1)+' chưa cập nhập giá trị').show();
				return;
			} else if(!VTUtilJS.isNullOrEmpty(params.listProductOpen[i].quantity) && !VTUtilJS.isNullOrEmpty(params.listProductOpen[i].amount)) {
				$('#errMsgOpen').html('Dòng '+(i+1)+' chỉ được nhập một loại giá trị').show();
				return;
			}
		}
		$.messager.confirm("Xác nhận", "Bạn có muốn lưu sản phẩm mở mới?", function(r) {
			if(r) {
				VTUtilJS.postFormJson(VTUtilJS.getSimpleObject(params), '/promotion/product-open-new-save', function(data){
					if(data.error) {
						$('#errMsgOpen').html(data.errMsg).show();
					} else {
						$("#successMsgOpen").html('Lưu dữ liệu thành công').show();
						setTimeout(function() {
							$("#successMsgOpen").html('').hide();
							$('#openGrid').datagrid('reload');
						}, 3000);
					}
				});
			}
		});
	}
};

/**
 * CArray: mang array,
 * Xu ly khi them vao mang, neu chua co indexOf() < 0 thi moi them
 */
function CArray() {
  var arr = [];
  
  this.push = function(a) {
    var idx = arr.indexOf(a);
    if (idx < 0) {
      arr.push(a);
      return a;
    }
    return null;
  }
  
  this.remove = function(a) {
    var idx = arr.indexOf(a);
    if (idx > -1) {
      arr.splice(idx, 1);
    }
    return idx;
  }
  
  this.removeAt = function(i) {
    if (i > -1 && i < arr.length) {
      var a = arr[i];
      arr.splice(i, 1);
      return a;
    }
    return null;
  }
  
  this.get = function(i) {
    if (i > -1 && i < arr.length) {
      return arr[i];
    }
    return null;
  }
  
  this.indexOf = function(a) {
    return arr.indexOf(a);
  }
  
  this.toArray = function() {
    return arr;
  }
  
  this.size = function() {
	  if (arr == null) {
		  return 0;
	  }
	  return arr.length;
  }
};
/*
 * END OF FILE - /web/web/resources/scripts/business/program/vnm.promotion-program.js
 */

/*
 * START OF FILE - /web/web/resources/scripts/business/program/vnm.promotion-support.js
 */
var PromotionSupport = {
	shopId: null,
	_xhrSave: null,
	_promotionShopParams: null,
	_promotionStaffParams: null,
	_hasCustomerType:null,
	_hasSaleLevel:null,
	_hasCustomerCardType:null,
	_flagAjaxTabAttribute:null,
	searchProgram : function() {
		$('.ErrorMsgStyle').hide();
		$('#fakefilepc').val('');
		$('#excelFile').val('');
		
		var promotionCode = $('#promotionCode').val().trim();
		var lstApParamId = null;
		if($('#typeCode').val() == null || $('#typeCode').val().length == 0){
			lstApParamId = [];
			lstApParamId.push(-1);
		}else{
			lstApParamId = $('#typeCode').val();
			if (lstApParamId[0] == -1) {
				lstApParamId.splice(1);
			}
		}
		var shopCode = $('#shopCode').val().trim();
		var promotionName = $('#promotionName').val().trim();
		var status = 1;
		//if($('#permissionUser').val() == 'true' ){
			status = $('#status').val().trim();
		//}
		var proType = 1;//$("#proType").val();
		var msg = '';
		var fDate = $('#fromDate').val().trim();
		var tDate = $('#toDate').val().trim();
		if (fDate.length > 0 && !Utils.isDate(fDate)) {
			msg = 'Từ ngày không hợp lệ, vui lòng nhập theo định dạng dd/MM/yyyy';
			$('#fromDate').focus();
		}
		if (msg.length == 0 && tDate.length > 0 && !Utils.isDate(tDate)) {
			msg = 'Đến ngày không hợp lệ, vui lòng nhập theo định dạng dd/MM/yyyy';
			$('#toDate').focus();
		}
		if(msg.length == 0 && fDate.length > 0 && tDate.length > 0 && !Utils.compareDate(fDate, tDate)){
			msg = 'Từ ngày phải là ngày trước hoặc cùng ngày với Đến ngày';
			$('#fromDate').focus();
		}
		if(msg.length > 0){
			$('#errMsg').html(msg).show();
			return false;
		}
		var params = {
				code: promotionCode,
				name: promotionName,
				proType: proType,
				shopCode: shopCode,
				lstTypeId: lstApParamId.join(","),
				fromDate: fDate,
				toDate: tDate,
				status: status
		};
		$('#grid').datagrid("reload", params);

		return false;
	},
	gridRowIconFormatter: function(value,row,index) {
		return "<a id='permissionEdit_btnEdit_" + row.id + "' class='cmsiscontrol' href='/promotion-support/detail?promotionId="+row.id+"'><span style='cursor:pointer'><img src='/resources/images/icon-edit.png'/></span></a>";
	},
	showTab: function(tabId, url) {
		if ($("#" + tabId + ".Active").length > 0) {
			return;
		}
		$(".ErrorMsgStyle").hide();
		$("#tabSectionDiv a.Active").removeClass("Active");
		$("#" + tabId).addClass("Active");
		url = url + "?promotionId="+$("#masterDataArea #id").val().trim();
		VTUtilJS.getFormHtml('masterDataArea', url, function(html) {
			$('#divTab').html(html);
			Utils.functionAccessFillControl();
		});
	},
	update : function() {
		$('#errorMsg').html('').hide();
		var params = VTUtilJS.getFormData('update-form');
		params.endDate = $('#permissionEditInfo_endDate').val();
		params.status = $('#permissionEditInfo_status').val();
		params.description = $('#description').val();
		if(params.typeCode == -1) {
			VTUtilJS.showMessageBanner(true, 'Bạn chưa chọn loại chương trình');
			return;
		}
		if(params != null) {
			var promotionId = $('#id').val();
			if(!VTUtilJS.isNullOrEmpty(promotionId)) {
				params.promotionId = promotionId;
			}
			$.messager.confirm("Xác nhận", "Bạn có muốn cập nhập thông tin CTKM?", function(r) {
				if(r) {
					VTUtilJS.postFormJson(params, '/promotion-support/update', function(data) {
						if(data.error) {
							VTUtilJS.showMessageBanner(true, data.errMsg);
						} else {
							window.location.href = '/promotion-support/detail?promotionId='+data.promotionId;
						}
					});
				}
			});
		}
	},
	searchShop: function() {
		$(".ErrorMsgStyle").hide();
		$("#labelListCustomer").hide();
		$("#promotionCustomerGrid").hide();
		$("#boxSearch1").hide();
		var params = {
				promotionId: $("#masterDataArea #id").val().trim(),
				code: $("#shopCode").val().trim(),
				name: $("#shopName").val().trim(),
				quantity: isNaN($("#quantityMax").val().trim()) ? "" : $("#quantityMax").val().trim()
		};
		PromotionSupport._promotionShopParams = params;
		$("#exGrid").treegrid("reload", params);
	},
	toggleCustomerSearch: function() {
		var b = $("#boxSearch1").is(":hidden");
		if (b) {
			$("#searchCustomerDiv").html("Đóng tìm kiếm &lt;&lt;");
		} else {
			$("#searchCustomerDiv").html("Tìm kiếm &gt;&gt;");
		}
		$("#boxSearch1").toggle();
	},
	importPromotionShop: function() {
		$('.ErrorMsgStyle').hide();
		VTUtilJS.importExcelUtils(function(data) {
			$("#exGrid").treegrid("reload");
			$("#labelListCustomer").hide();
			$("#promotionCustomerGrid").hide();
			$("#boxSearch1").hide();
			if(data.flag) {
				$("#errExcelMsgShop").html(data.message).show();
			} else {
				$("#successExcelMsgShop").html('Import thành công').show();
			}
		}, "importFrm", "excelFile", null, "errExcelMsgShop");
	},
	exportPromotionShop: function() {
		$(".ErrorMsgStyle").hide();
		if (PromotionSupport._promotionShopParams == null) {
			PromotionSupport._promotionShopParams = {};
			PromotionSupport._promotionShopParams.promotionId = $("#masterDataArea #id").val().trim();
		}
		ReportUtils.exportReport("/promotion-support/export-promotion-shop", PromotionSupport._promotionShopParams, "errMsgShop");
	},
	showCustomerDlg: function() {
		if (isNaN(PromotionSupport.shopId)) {
			return;
		}
		$(".ErrorMsgStyle").hide();
		var html = '<div id="add-customerPopupDiv" style="display:none;">\
			<div id="add-customerPopup">\
			<div class="PopupContentMid">\
				<div class="GeneralForm Search1Form">\
					<label class="LabelStyle" style="width:100px;">Mã KH</label>\
					<input id="codeDlg" class="InputTextStyle" style="width:150px;" maxlength="40" />\
					<label class="LabelStyle" style="width:100px;">Tên KH</label>\
					<input id="nameDlg" class="InputTextStyle" style="width:230px;" maxlength="100" />\
					<div class="Clear"></div>\
					<label class="LabelStyle" style="width:100px;">Địa chỉ</label>\
					<input id="addressDlg" class="InputTextStyle" style="width:500px;" maxlength="250" />\
					<div class="Clear"></div>\
					<div class="BtnCenterSection">\
					    <button class="BtnGeneralStyle" id="btnSearchDlg">Tìm kiếm</button>\
					</div>\
					\
					<div class="GridSection" id="gridDlgDiv">\
						<div id="gridDlg"></div>\
					</div>\
					\
					<label class="LabelStyle" style="width:80px;">Số suất</label>\
					<input id="quantityDlg" class="InputTextStyle vinput-money" style="width:130px;" maxlength="9" />\
					<label class="LabelStyle" style="width:80px;">Số lượng</label>\
					<input id="quantityMaxDlg" class="InputTextStyle vinput-money" style="width:130px;" maxlength="9" />\
					<label class="LabelStyle" style="width:80px;">Số tiền</label>\
					<input id="amountMaxDlg" class="InputTextStyle vinput-money" style="width:130px;" maxlength="9" />\
					<div class="Clear"></div><p id="erMsgDlg" class="ErrorMsgStyle" style="padding-left:5px;display:none;"></p>\
					<div class="Clear"></div>\
					<div class="BtnCenterSection">\
					    <button class="BtnGeneralStyle" id="btnChooseDlg">Chọn</button>\
						<button class="BtnGeneralStyle" id="btnCloseDlg" onclick="javascript:$(\'#add-customerPopup\').dialog(\'close\');">Đóng</button>\
					</div>\
				</div>\
			</div>\
			</div>\
		</div>';
		$("body").append(html);
		$('.vinput-money').each(function() {
			VTUtilJS.bindFormatOnTextfield($(this), VTUtilJS._TF_NUMBER_COMMA);
			VTUtilJS.formatCurrencyFor($(this));
		});
		var r = $("#exGrid").treegrid("find", PromotionSupport.shopId);
		var tt = (r == null) ? "Chọn Khách hàng" : "Chọn Khách hàng (<span style='color:#3a1;'>" + r.attr.shopName + "</span>)";
		
		var lstTmp = null;
		$("#add-customerPopup").dialog({
			title: tt,
			width: 750,
			heigth: "auto",
			modal: true, close: false, cache: false,
			onOpen: function() {
				$("#add-customerPopup").addClass("easyui-dialog");
				VTUtilJS.bindFormatOnTextfield("quantityDlg", VTUtilJS._TF_NUMBER);
				$("#add-customerPopup #codeDlg").focus();
				
				lstTmp = new CArray();
				$("#add-customerPopup #gridDlg").datagrid({
					url: "/promotion-support/search-customer-dlg",
					rownumbers: false,
					width: $("#add-customerPopup #gridDlgDiv").width(),
					height: "auto",
					pagination: true,
					fitColumns: true,
					idField: "customerId",
					scrollbarSize: 0,
					queryParams: {
						promotionId: $("#masterDataArea #id").val(),
						shopId: Number(PromotionSupport.shopId)
					},
					columns: [[
						{field:"no", title:"STT", sortable: false, resizable: false, width:45, fixed:true, align:"center", formatter:function(v, r, i) {
							var p = $("#add-customerPopup #gridDlg").datagrid("options").pageNumber;
							var n = $("#add-customerPopup #gridDlg").datagrid("options").pageSize;
							return (Number(p) - 1) * Number(n) + i + 1;
						}},
						{field:"customerCode", title:"Mã KH", sortable: false, resizable: false, width:75, align:"left" , formatter: function(value, row, index) {
							return Utils.XSSEncode(value);
						}},
						{field:"customerName", title:"Tên KH", sortable: false, resizable: false, width:120, align:"left", formatter:CommonFormatter.formatNormalCell},
						{field:"address", title:"Địa chỉ", sortable: false, resizable: false, width:200, align:"left", formatter:CommonFormatter.formatNormalCell},
						{field:"ck", title:"", checkbox:true, sortable: false, resizable: false, align:"center"}
					]],
					onLoadSuccess: function(data) {
						$("#add-customerPopup #gridDlg").datagrid("resize");
						setTimeout(function(){
							var hDlg=parseInt($("#add-customerPopup").parent().height());
							var hW=parseInt($(window).height());
							var d=hW-hDlg;
							d=d/2+document.documentElement.scrollTop;
							if (d < 0) { d = 0; }
							$("#add-customerPopup").parent().css('top',d);
			    		},1000);
					},
					onCheck: function(i, r) {
						lstTmp.push(r.customerId);
					},
					onUncheck: function(i, r) {
						lstTmp.remove(r.customerId);
					},
					onCheckAll: function(rows) {
						for (var i = 0, sz = rows.length; i < sz; i++) {
							lstTmp.push(rows[i].customerId);
						}
					},
					onUncheckAll: function(rows) {
						for (var i = 0, sz = rows.length; i < sz; i++) {
							lstTmp.remove(rows[i].customerId);
						}
					}
				});
				
				$("#add-customerPopup #btnSearchDlg").click(function() {
					var p = {
							promotionId: $("#masterDataArea #id").val(),
							shopId: Number(PromotionSupport.shopId),
							code: $("#add-customerPopup #codeDlg").val().trim(),
							name: $("#add-customerPopup #nameDlg").val().trim(),
							address: $("#add-customerPopup #addressDlg").val().trim()
					};
					$("#add-customerPopup #gridDlg").datagrid("load", p);
				});
				$("#add-customerPopup #codeDlg").parent().keyup(function(event) {
					if (event.keyCode == keyCodes.ENTER) {
						$("#add-customerPopup #btnSearchDlg").click();
					}
				});
				$("#add-customerPopup #btnChooseDlg").click(function() {
					if (lstTmp == null || lstTmp.size() == 0) {
						$("#erMsgDlg").html("Không có khách hàng nào được chọn").show();
						return;
					}
					var qtt = VTUtilJS.returnMoneyValue($('#quantityDlg').val());
					if (qtt.length > 0 && (isNaN(qtt) || Number(qtt) <= 0)) {
						$("#erMsgDlg").html("Số suất phải lớn hơn 0 và tối đa 9 ký tự số").show();
						return;
					}
					var qttMax = VTUtilJS.returnMoneyValue($('#quantityMaxDlg').val());
					if (qttMax.length > 0 && (isNaN(qttMax) || Number(qttMax) <= 0)) {
						$("#erMsgDlg").html("Số lượng phải lớn hơn 0 và tối đa 9 ký tự số").show();
						return;
					}
					var amountMax = VTUtilJS.returnMoneyValue($('#amountMaxDlg').val());
					if (amountMax.length > 0 && (isNaN(amountMax) || Number(amountMax) <= 0)) {
						$("#erMsgDlg").html("Số tiền phải lớn hơn 0 và tối đa 9 ký tự số").show();
						return;
					}
					PromotionSupport.addCustomerQtt(lstTmp.toArray());
				});
			},
			onClose: function() {
				$("#add-customerPopup").dialog("destroy");
				$("#add-customerPopupDiv").remove();
			}
		});
	},
	addCustomerQtt: function(lstCustId) {
		$(".ErrorMsgStyle").hide();
		$.messager.confirm("Xác nhận", "Bạn có muốn thêm khách hàng?", function(r) {
			if (r) {
				var params = {
						promotionId: $("#masterDataArea #id").val().trim(),
						shopId: PromotionSupport.shopId,
						quantity: VTUtilJS.returnMoneyValue($('#quantityDlg').val()),
						quantityMax: VTUtilJS.returnMoneyValue($('#quantityMaxDlg').val()),
						amountMax: VTUtilJS.returnMoneyValue($('#amountMaxDlg').val()),
						lstId: lstCustId
				};
				$("#add-customerPopup").dialog("close");
				Utils.saveData(params, "/promotion-support/add-customer", PromotionSupport._xhrSave, 'errMsgCustomer', function(data) {
					$("#successMsgCustomer").html("Lưu dữ liệu thành công").show();
					setTimeout(function() {$("#successMsgCustomer").hide();},3000);
					$("#exGrid").treegrid("reload");
					$("#promotionCustomerExGrid").datagrid("reload");
				}, "loading2");
			}
		});
	},
	deleteShopMap: function(shopId) {
		$(".ErrorMsgStyle").hide();
		$.messager.confirm("Xác nhận", "Bạn có muốn xóa đơn vị?", function(r) {
			if (r) {
				var params = {
						promotionId: $("#masterDataArea #id").val().trim(),
						shopId: shopId
				};
				Utils.saveData(params, "/promotion-support/delete-shop", PromotionProgram._xhrSave, 'errMsgShop', function(data) {
					$("#successMsgShop").html("Lưu dữ liệu thành công").show();
					setTimeout(function() {$("#successMsgShop").hide();},3000);
					$("#exGrid").treegrid("reload");
				}, "loading2");
			}
		});
	},
	showShopDlg: function(shopId) {
		if (shopId == undefined || shopId == null) {
			shopId = 0;
		}
		$(".ErrorMsgStyle").hide();
		var html = '<div id="add-shopPopupDiv" style="display:none;">\
			<div id="add-shopPopup">\
			<div class="PopupContentMid">\
				<div class="GeneralForm Search1Form">\
					<label class="LabelStyle" style="width:100px;">Mã đơn vị</label>\
					<input id="codeDlg" class="InputTextStyle" style="width:150px;" maxlength="40" />\
					<label class="LabelStyle" style="width:100px;">Tên đơn vị</label>\
					<input id="nameDlg" class="InputTextStyle" style="width:230px;" maxlength="100" />\
					<div class="Clear"></div>\
					<div class="BtnCenterSection">\
					    <button class="BtnGeneralStyle" id="btnSearchDlg">Tìm kiếm</button>\
					</div>\
					\
					<div class="GridSection" id="gridDlgDiv">\
						<div id="gridDlg"></div>\
					</div>\
					\
					<p id="erMsgDlg" class="ErrorMsgStyle" style="padding-left:5px;display:none;"></p>\
					<div class="Clear"></div>\
					<div class="BtnCenterSection">\
					    <button class="BtnGeneralStyle" id="btnChooseDlg">Chọn</button>\
						<button class="BtnGeneralStyle" id="btnCloseDlg" onclick="javascript:$(\'#add-shopPopup\').dialog(\'close\');">Đóng</button>\
					</div>\
				</div>\
			</div>\
			</div>\
		</div>';
		$("body").append(html);
		
		$("#add-shopPopup").dialog({
			title: "Chọn đơn vị và Số suất KM",
			width: 700,
			heigth: "auto",
			modal: true, close: false, cache: false,
			onOpen: function() {
				$("#add-shopPopup").addClass("easyui-dialog");
				$("#add-shopPopup #codeDlg").focus();
				
				$("#add-shopPopup #gridDlg").treegrid({
					url: "/promotion-support/search-shop-dlg?promotionId="+$("#masterDataArea #id").val().trim()+"&shopId="+Number(shopId),
					rownumbers: false,
					width: $("#add-shopPopup #gridDlgDiv").width(),
					height: 350,
					fitColumns: true,
					idField: "nodeId",
					treeField: "text",
					selectOnCheck: false,
					checkOnSelect: false,
					columns: [[
						{field:"text", title:"Mã Đơn vị", sortable: false, resizable: false, width: 220, align:"left", formatter:CommonFormatter.formatNormalCell},
						{field:"quantity", title:"Số suất", sortable: false, resizable: false, width: 110, fixed:true, align:"left", formatter: function(v, r) {
							if (r.attr.isNPP) {
								if (r.attr.isExists) {
									return "<input type='text' id='quantity-ip"+r.attr.id+"' style='float: right;' class='vinput-money' maxlength='11' disabled='disabled' exists='1' />";
								}
								return "<input type='text' id='txt-p"+r.attr.id+"' style='float: right;' class='vinput-money' maxlength='11' exists='0' />";
							}
							return "";
						}},
						{field:"quantityMax", title:"Số lượng", sortable: false, resizable: false, width: 110, fixed:true, align:"left", formatter: function(v, r) {
							if (r.attr.isNPP) {
								if (r.attr.isExists) {
									return "<input type='text' id='quantity-max-ip-"+r.attr.id+"' style='float: right;' class='vinput-money' maxlength='11' disabled='disabled' exists='1' />";
								}
								return "<input type='text' id='quantity-max-ip-"+r.attr.id+"' style='float: right;' class='vinput-money' maxlength='11' exists='0' />";
							}
							return "";
						}},
						{field:"amountMax", title:"Số tiền", sortable: false, resizable: false, width: 110, fixed:true, align:"left", formatter: function(v, r) {
							if (r.attr.isNPP) {
								if (r.attr.isExists) {
									return "<input type='text' id='amount-max-ip-"+r.attr.id+"' style='float: right;' class='vinput-money' maxlength='11' disabled='disabled' exists='1' />";
								}
								return "<input type='text' id='amount-max-ip-"+r.attr.id+"' style='float: right;' class='vinput-money' maxlength='11' exists='0' />";
							}
							return "";
						}},
						{field:"ck", title:"", sortable: false, resizable: false, width:35, fixed:true, align:"center", formatter: function(v, r) {
							var pId = r.attr.parentId;
							if (pId == undefined || pId == null) {
								pId = 0;
							}
							if (r.attr.isExists) {
								return "<input type='checkbox' id='ck"+r.nodeId+"' value='"+r.attr.id+"' disabled='disabled' exists='1' parentId='"+pId+"' onchange='PromotionSupport.onCheckShop(this);' />";
							}
							return "<input type='checkbox' id='ck"+r.nodeId+"' value='"+r.attr.id+"' exists='0' parentId='"+pId+"' onchange='PromotionSupport.onCheckShop(this);' />";
						}}
					]],
					onLoadSuccess: function(data) {
						$("#add-shopPopup #gridDlg").datagrid("resize");
						$('#add-shopPopup .vinput-money').each(function() {
				      		VTUtilJS.bindFormatOnTextfield($(this), VTUtilJS._TF_NUMBER_COMMA);
							VTUtilJS.formatCurrencyFor($(this).prop('id'));
							$(this).width($(this).parent().width() - 7);
				      	});
						setTimeout(function() {
							var hDlg=parseInt($("#add-shopPopup").parent().height());
							var hW=parseInt($(window).height());
							var d = hW - hDlg;
							d = (d / 2) + document.documentElement.scrollTop;
							if (d < 0) { 
								d = 0; 
							}
							$("#add-shopPopup").parent().css('top', d);
			    		}, 1000);
					}
				});
				
				$("#add-shopPopup #btnSearchDlg").click(function() {
					var p = {
							code: $("#add-shopPopup #codeDlg").val().trim(),
							name: $("#add-shopPopup #nameDlg").val().trim()
					};
					$("#add-shopPopup #gridDlg").treegrid("reload", p);
				});
				$("#add-shopPopup #codeDlg").parent().keyup(function(event) {
					if (event.keyCode == keyCodes.ENTER) {
						$("#add-shopPopup #btnSearchDlg").click();
					}
				});
				$("#add-shopPopup #btnChooseDlg").click(function() {
					if ($("#add-shopPopup input[id^=ck]:checked").length == 0) {
						$("#erMsgDlg").html("Không có đơn vị nào được chọn").show();
						return;
					}
					var mapShopQuantityObj = new Map();
					var arrQuantityObject = [];
					var shopId, qtt, shopText;
					var parentTr;
					var flagError = false;
					$("#add-shopPopup input[id^=ck]:checked").each(function() {
						arrQuantityObject = [];
						shopId = Number($(this).val());
						parentTr = $(this).parent().parent().parent();
						shopText = $($(parentTr).children('td')[0]).text();
						$('#' + $(parentTr).prop('id') + ' .vinput-money').each(function() {
							qtt = VTUtilJS.returnMoneyValue($(this).val());
							if (qtt.length > 9 || isNaN(qtt) || (qtt.length > 0 && Number(qtt) <= 0)) {
								$("#erMsgDlg").html(shopText.trim() + ' không hợp lệ. Số suất, Số lượng, Số tiền phải lớn hơn 0 và tối đa 9 ký tự số').show();
								mapShopQuantityObj = null;
								$(this).focus();
								flagError = true;
								return;
							}
							arrQuantityObject.push(qtt);
						});
						mapShopQuantityObj.put(shopId, arrQuantityObject);//[so suat, so luong, so tien]
					});
					if (flagError) {
						return;
					}
					PromotionSupport.addShopQtt(mapShopQuantityObj);
				});
			},
			onClose: function() {
				$("#add-shopPopup").dialog("destroy");
				$("#add-shopPopupDiv").remove();
			}
		});
	},
	removeParentShopCheck: function(t) {
		if ($(t).length == 0) {
			return;
		}
		$(t).removeAttr("checked");
		$(t).attr("disabled", "disabled");
		var p = "#ck" + $(t).attr("parentId");
		PromotionSupport.removeParentShopCheck(p);
	},
	
	enableParentShopCheck: function(t) {
		if ($(t).length == 0 || $(t).attr("exists") == "1") {
			return;
		}
		$(t).removeAttr("disabled");
		var p = "#ck" + $(t).attr("parentId");
		PromotionSupport.enableParentShopCheck(p);
	},
	onCheckShop: function(t) {
		var ck = $(t).prop("checked");
		if (ck) {
			var p = "#ck" + $(t).attr("parentId");
			PromotionSupport.removeParentShopCheck(p);
		} else {
			var p = "#ck" + $(t).attr("parentId");
			PromotionSupport.enableParentShopCheck(p);
		}
	},
	addShopQtt: function(mapShopTmp) {
		$(".ErrorMsgStyle").hide();
		$.messager.confirm("Xác nhận", "Bạn có muốn thêm đơn vị?", function(r) {
			if (r) {
				var params = {
						promotionId: $("#masterDataArea #id").val().trim(),
						lstId: mapShopTmp.keyArray
				};
				if (mapShopTmp != undefined && mapShopTmp != null && mapShopTmp.size() > 0) {
					var arrKeyVal = mapShopTmp.keyArray;
					//Bo sung tham so
					params.lstId = mapShopTmp.keyArray;
					//Khai bai tham bien dung chung
					var arrQuantity = [];
					var arrQuantityMax = [];
					var arrAmountMax = [];
					for (var i = 0, size = arrKeyVal.length; i < size; i++) {
						var arrQuantityVal = mapShopTmp.get(arrKeyVal[i]);
						if (arrQuantityVal != null && arrQuantityVal != undefined) {
							var lengthArrlength = arrQuantityVal.length;
							if (lengthArrlength > 0 && !isNaN(arrQuantityVal[0]) && Number(arrQuantityVal[0]) > 0) {
								arrQuantity.push(arrKeyVal[i] + ";" + arrQuantityVal[0].trim());
							}
							if (lengthArrlength > 1 && !isNaN(arrQuantityVal[1]) && Number(arrQuantityVal[1]) > 0) {
								arrQuantityMax.push(arrKeyVal[i] + ";" + arrQuantityVal[1].trim());
							}
							if (lengthArrlength > 2 && !isNaN(arrQuantityVal[2]) && Number(arrQuantityVal[2]) > 0) {
								arrAmountMax.push(arrKeyVal[i] + ";" + arrQuantityVal[2].trim());
							}
						}
					}
					//Bo sung tham so So suat, So luong, So tien
					params.quantityObjectStr = arrQuantity.toString();
					params.quantityMaxObjectStr = arrQuantityMax.toString();
					params.amountMaxObjectStr = arrAmountMax.toString();
				}
				
				$("#add-shopPopup").dialog("close");
				Utils.saveData(params, "/promotion-support/add-shop", PromotionSupport._xhrSave, 'errMsgShop', function(data) {
					$("#successMsgShop").html("Lưu dữ liệu thành công").show();
					setTimeout(function() {$("#successMsgShop").hide();},3000);
					$("#exGrid").treegrid("reload");
					$("#labelListCustomer").hide();
					$("#promotionCustomerGrid").hide();
					$("#boxSearch1").hide();
				}, "loading2");
			}
		});
	},
	/**
	 * @modify hunglm16
	 * @param shopId
	 * @returns
	 * @since 25/11/2015
	 */
	updateShopQuantity: function(shopId) {
		$(".ErrorMsgStyle").hide();
		var arrNumberForShop = [];
		var arrVinputMoney = $('#promotionShopGrid .vinput-money[id*="ip-'+shopId+'"]');
		var nodeChange = null;
		var dem = 0;
		var numText = null;
		var numReceivedText = null;
		var numReceived = null;
		for (var i = 0, size = arrVinputMoney.length; i < size; i++) {
			dem++;
			var txtInput = arrVinputMoney[i];
			if (dem == 1) {
				nodeChange = $("#exGrid").treegrid('find', shopId);
				numText = 'Số suất KM';
				numReceivedText = 'Số suất đã KM';
				numReceived = nodeChange.attr.receivedQtt;
			} else if (dem == 2) {
				numText = 'Số lượng KM';
				numReceivedText = 'Số lượng đã KM';
				numReceived = nodeChange.attr.receivedNum;
			} else if (dem == 3) {
				numText = 'Số tiền KM';
				numReceivedText = 'Số tiền đã KM';
				numReceived = nodeChange.attr.receivedAmt;
			}
			var qtt = VTUtilJS.returnMoneyValue($(txtInput).val());
			var objectText = nodeChange.text;
			if (objectText == undefined && objectText == null) {
				objectText = '';
			}
			if (qtt.length > 9 || isNaN(qtt) || (qtt.length > 0 && Number(qtt) <= 0)) {
				$("#errMsgShop").html(objectText.trim() + ' không hợp lệ. '+ numText +' phải lớn hơn 0 và tối đa 9 ký tự số').show();
				$(txtInput).focus();
				return;
			} else if (numReceived != undefined && numReceived != null && numReceived > Number(qtt)) {
				$("#errMsgShop").html(objectText.trim() + ' không hợp lệ. '+ numText +' phải lớn hơn hoặc bằng ' + numReceivedText).show();
				$(txtInput).focus();
				return;
			} else {
				arrNumberForShop.push(qtt);
			}
			if (dem == 3) {
				dem = 0;
			}
			msg = null;
		}
		$.messager.confirm("Xác nhận", "Bạn có muốn cập nhật đơn vị?", function(r) {
			if (r) {
				var params = {
						promotionId: $("#masterDataArea #id").val().trim(),
						shopId: shopId,
						quantity: arrNumberForShop.length > 0 ? arrNumberForShop[0] : Number(arrNumberForShop[0]),
						quantityMax: arrNumberForShop.length > 1 ? arrNumberForShop[1] : Number(arrNumberForShop[1]),
						amountMax: arrNumberForShop.length > 2 ? arrNumberForShop[2] : Number(arrNumberForShop[2])
				};
				Utils.saveData(params, "/promotion-support/update-shop-quantity", PromotionSupport._xhrSave, 'errMsgShop', function(data) {
					$("#successMsgShop").html("Lưu dữ liệu thành công").show();
					setTimeout(function() {
						$(".SuccessMsgStyle").hide();
					}, 1500);
					$("#exGrid").treegrid("reload");
					$("#labelListCustomer").hide();
					$("#promotionCustomerGrid").hide();
					$("#boxSearch1").hide();
				}, "loading2");
			}
		});
	},
	searchCustomerShopMap: function(promotionId, shopId, status) {
		$(".ErrorMsgStyle").hide();
		var r = $("#exGrid").treegrid("find", shopId);
		if (r != null) {
			$("#labelShop").html(r.attr.shopName);
		}
		$("#labelListCustomer").show();
		$("#promotionCustomerGrid").show();
		PromotionSupport.shopId = shopId;
		var params = {
				promotionId: promotionId,
				shopId: shopId,
				code: $("#customerCodeSearch").val().trim(),
				name: $("#customerNameSearch").val().trim(),
				address: $("#customerAddressSearch").val().trim()
		};
		$("#promotionCustomerExGrid").datagrid("load", params);
		$("#btnSearchCustomer").attr("onclick", "PromotionSupport.searchCustomerShopMap("+promotionId+","+shopId+","+status+");");
		$('html, body').animate({ scrollTop: $(document).height() }, 1000);
	},
	updateCustomerQuantity: function (pcmId) {
		$(".ErrorMsgStyle").hide();
		if (pcmId == undefined || pcmId == null || isNaN(pcmId)) {
			$('#errMsgCustomer').text('Không xác định được khách hàng với đơn vị tương ứng').show();
			return;
		}
			
		var rows = $("#promotionCustomerExGrid").datagrid('getRows');
		var row = null;
		for (var i = 0, size = rows.length; i < size; i++) {
			if (pcmId == rows[i].mapId) {
				row = rows[i];
			}
		}
		if (row == null) {
			$('#errMsgCustomer').text('Không xác định được khách hàng với đơn vị tương ứng').show();
			return;
		}
		var arrNumberForCus = [];
		var arrVinputMoney = $('#promotionCustomerGrid .vinput-money[id*="ip-'+pcmId+'"]');
		var dem = 0;
		var numText = null;
		var numReceivedText = null;
		var numReceived = null;
		for (var i = 0, size = arrVinputMoney.length; i < size; i++) {
			dem++;
			var txtInput = arrVinputMoney[i];
			if (dem == 1) {
				numText = 'Số suất KM';
				numReceivedText = 'Số suất đã KM';
				numReceived = row.receivedQtt;
			} else if (dem == 2) {
				numText = 'Số lượng KM';
				numReceivedText = 'Số lượng đã KM';
				numReceived = row.receivedNum;
			} else if (dem == 3) {
				numText = 'Số tiền KM';
				numReceivedText = 'Số tiền đã KM';
				numReceived = row.receivedAmt;
			}
			var qtt = VTUtilJS.returnMoneyValue($(txtInput).val());
			var objectText = 'Khách hàng ' + row.customerCode + ' - ' + row.customerName;
			if (qtt.length > 9 || isNaN(qtt) || (qtt.length > 0 && Number(qtt) <= 0)) {
				$("#errMsgCustomer").html(objectText.trim() + ' không hợp lệ. '+ numText +' phải lớn hơn 0 và tối đa 9 ký tự số').show();
				$(txtInput).focus();
				return;
			} else if (numReceived != undefined && numReceived != null && numReceived > Number(qtt)) {
				$("#errMsgCustomer").html(objectText.trim() + ' không hợp lệ. '+ numText +' phải lớn hơn hoặc bằng ' + numReceivedText).show();
				$(txtInput).focus();
				return;
			} else {
				arrNumberForCus.push(qtt);
			}
			if (dem == 3) {
				dem = 0;
			}
			msg = null;
		}
		$.messager.confirm("Xác nhận", "Bạn có muốn cập nhật khách hàng?", function(r) {
			if (r) {
				var params = {
						promotionId: $("#masterDataArea #id").val().trim(),
						id: pcmId,
						quantity: arrNumberForCus.length > 0 ? arrNumberForCus[0] : Number(arrNumberForCus[0]),
						quantityMax: arrNumberForCus.length > 1 ? arrNumberForCus[1] : Number(arrNumberForCus[1]),
						amountMax: arrNumberForCus.length > 2 ? arrNumberForCus[2] : Number(arrNumberForCus[2])
				};
				Utils.saveData(params, "/promotion-support/update-customer-quantity", PromotionSupport._xhrSave, 'errMsgCustomer', function(data) {
					$("#successMsgCustomer").html("Lưu dữ liệu thành công").show();
					setTimeout(function() {$("#successMsgCustomer").hide();},3000);
					//$("#exGrid").treegrid("reload");
					$("#promotionCustomerExGrid").datagrid("reload");
				}, "loading2");
			}
		});
	},
	deleteCustomerMap: function(pcmId) {
		$(".ErrorMsgStyle").hide();
		$.messager.confirm("Xác nhận", "Bạn có muốn xóa khách hàng?", function(r) {
			if (r) {
				var params = {
						promotionId: $("#masterDataArea #id").val().trim(),
						id: pcmId
				};
				Utils.saveData(params, "/promotion-support/delete-customer", PromotionSupport._xhrSave, 'errMsgCustomer', function(data) {
					$("#successMsgCustomer").html("Lưu dữ liệu thành công").show();
					setTimeout(function() {$("#successMsgCustomer").hide();},3000);
					$("#promotionCustomerExGrid").datagrid("reload");
				}, "loading2");
			}
		});
	},
	selectAttributes: function() {
		$('.ErrorMsgStyle').hide();
		var listObjectType= new Array();
		var listObjectId= new Array();
		$('#left :checkbox').each(function(){	
			if($(this).is(':checked')){
				//remove ben left, show ben right
				$(this).parent().remove(); 
				var objectType = $(this).attr('objectType');
				var objectId = $(this).attr('objectId');
				listObjectType.push(objectType);
				if(objectType == 2 || objectType == 3){
					listObjectId.push(0);
				}else{
					listObjectId.push(objectId);
				}
			}
		});	
		var data = new Object();
		data.lstObjectType = listObjectType;
		data.lstId = listObjectId;
		var kData = $.param(data, true);
		$.getJSON("/promotion-support/get-all-data-attributes",kData,function(result){
			var listOut = result.list;	
			if(listOut!=null){
				for(var h=0;h<listOut.length;++h){
					var objectType = listOut[h].objectType;
					var objectId = listOut[h].objectId;
					var valueType = listOut[h].valueType;
					var html = '';
					if(objectType == 2){
						var name = 'Loại khách hàng';
						html+= '<div>';
						html+= '<div class="Clear"></div>';
						html+= '<li>';
						html+= '<input name="'  + name + '" objectType="'+ objectType +'" objectId="'+objectId +'" type="checkbox"  class="InputCbxStyle checkBoxAttribute"/>';
						html+= '<label class="LabelStyle Label4Style">'+name+'</label>';
						html+= '</li>';
						html+= '<div class="BoxSelect BoxSelect2">';
						html+= '<select id="customerType" style="width: 275px;" class="MySelectBoxClass" multiple="multiple">';
						var list = listOut[h].listData;	
						var arrHtml = new Array();
						if(list!=null){
							for(var i=0;i<list.length;++i){
								arrHtml.push('<option value="'+ list[i].idChannelType +'">'+ Utils.XSSEncode(list[i].codeChannelType)+ '-' +Utils.XSSEncode(list[i].nameChannelType)+ '</option>');
							}
						}
						html+= arrHtml.join("");
						html+= '</select>';
						html+= '</div>';
						html+= '<div class="Clear"></div>';
						html+= '</div>';
						$('#right').append(html);
					}else if(objectType == 3){
						var name = 'Mức doanh số';
						html+= '<div id="saleLevel">';
						html+= '<div class="Clear"></div>';
						html+= 	'<li>';
						html+=	'<input name="'  + name + '" objectType="'+ objectType +'" objectId="'+objectId +'" type="checkbox"  class="InputCbxStyle checkBoxAttribute"/>';
						html+=	'<label class="LabelStyle Label4Style">'+name+'</label>';
						html+=	'</li>';
						var list = listOut[h].listProductInfoVO;//result.list;	
						if(list!=null){
							for(var i=0;i<list.length;++i){
								if(i==0){// if else để canh chỉnh html cho thẳng hàng đó mà.
									//label ngành hàng nên đặt 1 thuộc tính là catId
									html+= '<label catId="'+list[i].idProductInfoVO +'" style="width: 150px;" class="LabelStyle Label5Style cat">'+Utils.XSSEncode(list[i].codeProductInfoVO)+'</label>';
									//add combobox mức vao label nganh hang, combobox nên đặt id theo ngành hàng.
									html+= '<div class="BoxSelect BoxSelect2">';
									html+= '<select id="saleLevel-'+list[i].idProductInfoVO +'" class="MySelectBoxClass saleLevelCat" multiple="multiple">';
									var listSaleLevelCat = list[i].listSaleCatLevelVO;
									var arrHtml = new Array();
									if(listSaleLevelCat!=null){
										for(var k=0;k<listSaleLevelCat.length;++k){
											arrHtml.push('<option value="'+ listSaleLevelCat[k].idSaleCatLevel +'">'+ Utils.XSSEncode(listSaleLevelCat[k].codeSaleCatLevel)+'-'+Utils.XSSEncode(listSaleLevelCat[k].nameSaleCatLevel) +'</option>');
										}
									}
									html+= arrHtml.join("");
									html+= '</select>';
									html+= '</div>';
								}else{
									html+= '<div class="Clear"></div>';
									html+= '<label catId="'+list[i].idProductInfoVO +'" style="width: 150px;" class="LabelStyle Label6Style" >'+Utils.XSSEncode(list[i].codeProductInfoVO)+'</label>';
									//add combobox mức vao label nganh hang, combobox nên đặt id theo ngành hàng.
									html+= '<div class="BoxSelect BoxSelect2">';
									html+= '<select id="saleLevel-'+list[i].idProductInfoVO +'" class="MySelectBoxClass saleLevelCat" multiple="multiple">';
									var listSaleLevelCat = list[i].listSaleCatLevelVO;
									var arrHtml = new Array();
									if(listSaleLevelCat!=null){
										for(var k=0;k<listSaleLevelCat.length;++k){
											arrHtml.push('<option value="'+ listSaleLevelCat[k].idSaleCatLevel +'">'+Utils.XSSEncode(listSaleLevelCat[k].codeSaleCatLevel)+'-'+Utils.XSSEncode(listSaleLevelCat[k].nameSaleCatLevel) +'</option>');
										}
									}
									html+= arrHtml.join("");
									html+= '</select>';
									html+= '</div>';
								}
							}
						}
						html+= '<div class="Clear"></div>';
						html+= '</div>';
						$('#right').append(html);
					}else if(objectType == 1){
						var name = listOut[h].name;
						name = Utils.XSSEncode(name);
						html+= '<div>';
						html+= '<div class="Clear"></div>';
						html+= 	'<li>';
						//thằng input của thuộc tính động cần bỏ valueType vào để sau này xử lý khi lưu.
						html+=	'<input valueType="'+valueType+'" name="'  + name + '" objectType="'+ objectType +'" objectId="'+objectId +'" type="checkbox"  class="InputCbxStyle checkBoxAttribute"/>';
						html+=	'<label class="LabelStyle Label4Style">'+name+'</label>';
						html+=  '</li>';
						//Chia các trường hợp theo valueType của attributeId
						if(valueType=="CHARACTER"){
							//Nên đặt id html của text CHARACTER này theo attributeId cộng chuỗi CHARACTER  thì sẽ không bị conflict trong danh sách. Sau này muốn lưu thì
							//select theo attributeId để lấy ra dữ liệu.
							html+= '<input id="'+objectId+'_CHARACTER" type="text" style="width: 377px;" class="InputTextStyle InputText1Style"/>';
						}else if(valueType=="NUMBER"){
							html+= '<input id="'+objectId+'_NUMBER_from" type="text" style="width: 170px;" class="InputTextStyle InputText5Style"/>';
							html+= '<label class="LabelStyle Label7Style">-</label>';
							html+= '<input id="'+objectId+'_NUMBER_to" type="text" style="width: 170px;" class="InputTextStyle InputText5Style"/>';
						}else if(valueType=="DATE_TIME"){
							html+= '<input id="'+objectId+'_DATETIME_from" style="width: 146px;" type="text" class="InputTextStyle InputText6Style "/>';
							html+= '<label class="LabelStyle Label7Style">-</label>';
							html+= '<input id="'+objectId+'_DATETIME_to" style="width: 146px;" type="text" class="InputTextStyle InputText6Style "/>';
						}else if(valueType=="CHOICE" || valueType=="MULTI_CHOICE"){
							var list = listOut[h].listData;	
							var arrHtml = new Array();
							html+= '<div class="BoxSelect BoxSelect2">';
							html+= '<select id="'+objectId+'" class="MySelectBoxClass" multiple="multiple">';
							if(list!=null){
								for(var i=0;i<list.length;++i){
									arrHtml.push('<option value="'+ list[i].enumId +'">'+Utils.XSSEncode(list[i].code)+'-'+Utils.XSSEncode(list[i].name) +'</option>');
								}
							}
							html+= arrHtml.join("");
							html+= '</select>';
							html+= '</div>';
						}
						html+= '<div class="Clear"></div>';
						html+= '</div>';
						$('#right').append(html);
						if(valueType=="DATE_TIME"){
							VTUtilJS.applyDateTimePicker(objectId+'_DATETIME_from');
							VTUtilJS.applyDateTimePicker(objectId+'_DATETIME_to');
						}else if(valueType=="NUMBER"){
							Utils.bindFormatOnTextfield(objectId+'_NUMBER_from',Utils._TF_NUMBER);
							Utils.formatCurrencyFor(objectId+'_NUMBER_from');
							Utils.bindFormatOnTextfield(objectId+'_NUMBER_to',Utils._TF_NUMBER);
							Utils.formatCurrencyFor(objectId+'_NUMBER_to');
						}
					}
				}
				//Xong for:
				$('#right .MySelectBoxClass').each(function(){
					var aCombobox = $(this);
					if(!(aCombobox.next().hasClass('ui-dropdownchecklist'))){//Cái này để kiểm tra comboBox đó đã gọi dropdownchecklist lần nào chưa.
						//Nếu nó đã gọi dropdownchecklist 1 lần rồi, thì thể nào cũng gien ra 1 element html có class ui-dropdownchecklist nằm kế bên comboBox.
						aCombobox.dropdownchecklist({ maxDropHeight: 150,width: 277, textFormatFunction: function(options){
							var text = '';
							$('.ui-dropdownchecklist-dropcontainer input[type=checkbox]').each(function(){
								var aCheckBox = $(this);
								if(aCheckBox.parent().parent().parent().prev().prev().attr('id') == aCombobox.attr('id')){//Tức là chỉ xử lý với những checkbox thuộc combobox đó.
									if(aCheckBox.attr('checked')=='checked'){
										text += aCheckBox.next().text().split('-')[0] + ', ';
									}
								}
							});
							if(text != ''){
								text = text.substring(0, text.length-2);//bo dau ', ' cuoi cung.
							}
							return text;
						} });
					}
				});
				
			}
		});	
	},
	removeAttributes: function(){
		$('.ErrorMsgStyle').hide();
		var html = '';//để bên ngoài vòng for là đúng. Còn bên hàm toSelectAttribute thì phải để bên trong do cuộc gọi bất đồng bộ
		$('#right .checkBoxAttribute').each(function(){
			if($(this).is(':checked')){
				//remove ben right, show ben left
				$(this).parent().parent().remove(); 
				var objectType = $(this).attr('objectType');
				var objectId = $(this).attr('objectId');
				var name = $(this).attr('name');
				html+= '<li> <input name="'  + name + '" objectType="'+ objectType +'" objectId="'+objectId +'" type="checkbox"  class="InputCbxStyle"/><label style="width:150px" class="LabelStyle Label3Style">'+Utils.XSSEncode(name)+'</label></li>';
			}
		});	
		$('#left').append(html);//để bên ngoài vòng for là đúng. Còn bên hàm toSelectAttribute thì phải để bên trong do cuộc gọi bất đồng bộ
	},
	saveCustomerAttributes: function(){
		var message = '';
		var messageSaleLevel = '';
		var messageCustomerType = '';
		var messageOther = '';
		$('.ErrorMsgStyle').hide();
		var data = new Object();
		var listCustomerType= new Array();
		var listSaleLevelCatId= new Array();
		var listAttribute= new Array();
		var listAttributeDataInField=new Array();
		var nameAttr = '';
		$('.checkBoxAttribute').each(function(){
			var objectType = $(this).attr('objectType');
			var objectId = $(this).attr('objectId');
			var name = $(this).attr('name');
			
			if(objectType == 2){
				listAttribute.push(-2);//Loai kh cho id = -2 di. De ti nua if else tren action.
				listAttributeDataInField.push('CustomerType');
				messageCustomerType = 'noCheckCustomerType';
				listCustomerType= new Array();
				$('#ddcl-customerType-ddw input[type=checkbox]').each(function(){
					if($(this).attr('checked')=='checked'){
						listCustomerType.push($(this).val());
						messageCustomerType = 'haveCheckCustomerType';
					}
				});
			}else if(objectType == 3){ 
				listAttribute.push(-3);//Muc doanh so cho id = -3 di. De ti nua if else tren action.
				listAttributeDataInField.push('SaleLevel');
				messageSaleLevel = 'noCheckSaleLevel';
				listSaleLevelCatId= new Array();
				$('#saleLevel input[type=checkbox]').each(function(){
					if($(this).attr('checked')=='checked' && !$(this).hasClass('checkBoxAttribute')){
						listSaleLevelCatId.push(this.value);
//						listSaleLevelCatId.push($(this).attr('objectid'));
						//vào được đây tức là mức doanh số có ít nhất 1 combox được check.
						messageSaleLevel = 'haveCheckSaleLevel';
					}
				});
			}else if(objectType == 1){ 
				listAttribute.push(objectId);
				var valueType = $(this).attr('valueType');
				if(valueType=="CHARACTER" || valueType=="NUMBER" || valueType=="DATE_TIME"){
					var boolean = PromotionSupport.getTrue_DataOrFalse_MessageOfAutoAttribute(objectId, name, valueType);
					var arr = boolean.split('__');
					if(arr[0]=='false'){
						message = arr[1];
						return false;
					}else{
						listAttributeDataInField.push(arr[1]);
					}
				}else if(valueType=="CHOICE" || valueType=="MULTI_CHOICE"){
					messageOther = 'noCheckOther';
					var dataOfAutoAttribute = '';
					$('#ddcl-'+objectId+'-ddw input[type=checkbox]').each(function(){
						if($(this).attr('checked')=='checked'){
							dataOfAutoAttribute += $(this).val() +',';//Nhớ chú ý dấu phẩy cuối cùng
							messageOther = 'haveCheckOther';
						}
					});
					nameAttr = name;
					listAttributeDataInField.push(dataOfAutoAttribute);
				}
			}
			if(messageSaleLevel == 'noCheckSaleLevel'){
				message = 'Bạn chưa chọn giá trị cho thuộc tính Mức doanh số';
			}
			if(messageCustomerType == 'noCheckCustomerType'){
				message = 'Bạn chưa chọn giá trị cho thuộc tính Loại khách hàng';
			}
			if(messageOther == 'noCheckOther'){
				message = 'Bạn chưa chọn giá trị cho thuộc tính '+ Utils.XSSEncode(nameAttr);
			}
		});
		if(message.length > 0){
			$('#errMsgSave').html(message).show();
			return false;
		}
		data.lstCustomerType = listCustomerType;
		data.lstSaleLevelCatId = listSaleLevelCatId;
		data.lstId = listAttribute;
		data.lstAttDataInField = listAttributeDataInField;
		data.promotionId=$('#masterDataArea #id').val().trim();
		Utils.addOrSaveData(data, '/promotion-support/save-promotion-customer-attribute', PromotionSupport._xhrSave,
				'errMsgSave', null, 'loading2', '#customerAttributeMsg ');//Prefix để selector jquery mà truyền thiếu "khoảng trắng" 
		//là có thể lỗi, không xuất ra câu thông báo lỗi, hay câu thông báo thành công đâu.
		return false;
	},
	getTrue_DataOrFalse_MessageOfAutoAttribute: function(objectId, nameAttribute,valueType){
		if(valueType=="CHARACTER"){
			var msg = '';
			var dataOfAutoAttribute = $('#'+objectId+'_CHARACTER').val();
			if(dataOfAutoAttribute==''){
				msg = 'Bạn chưa nhập giá trị cho thuộc tính '+ Utils.XSSEncode(nameAttribute);
				$('#'+objectId+'_CHARACTER').focus();
				return 'false__'+msg;
			}
			msg = Utils.getMessageOfSpecialCharactersValidate(objectId+'_CHARACTER','',Utils._NAME);
			if(msg.length > 0){
				msg = 'Thuộc tính '+ Utils.XSSEncode(nameAttribute) +': Giá trị nhập vào không được chứa các ký tự đặc biệt [<>\~#&$%@*()^`\'"'; 
				return 'false__'+msg;
			}
			return 'true__'+dataOfAutoAttribute;
		}else if(valueType=="NUMBER"){
			var msg = '';
			var NUMBER_from = $('#'+objectId+'_NUMBER_from').val();
			var NUMBER_to = $('#'+objectId+'_NUMBER_to').val();
			if (NUMBER_from == '' && NUMBER_to == ''){
				msg = 'Bạn chưa nhập giá trị cho thuộc tính '+nameAttribute;
				$('#'+objectId+'_NUMBER_from').focus();
				return 'false__'+msg;
			}
			if (NUMBER_from != ''){
				NUMBER_from = parseInt(Utils.returnMoneyValue(NUMBER_from.trim()));
			}
			if (NUMBER_to != ''){
				NUMBER_to = parseInt(Utils.returnMoneyValue(NUMBER_to.trim()));
			}
			var dataOfAutoAttribute = NUMBER_from +','+NUMBER_to;//Nếu ở trên không cắt dấu phẩy đi thì sẽ ảnh hưởng!
			
			if(NUMBER_from!=='' &&  NUMBER_to!=='' &&  NUMBER_from > NUMBER_to){
				msg = 'Thuộc tính '+ Utils.XSSEncode(nameAttribute) + ': Giá trị "Từ" lớn hơn giá trị "Đến"';
				return 'false__'+msg;
			}
			return 'true__'+dataOfAutoAttribute;
		}else if(valueType=="DATE_TIME"){
			var msg = '';
			var DATETIME_from = $('#'+objectId+'_DATETIME_from').val();
			var DATETIME_to = $('#'+objectId+'_DATETIME_to').val();
			var dataOfAutoAttribute = DATETIME_from +','+DATETIME_to;
			if(DATETIME_from == '' && DATETIME_to == ''){
				msg = 'Bạn chưa nhập giá trị cho thuộc tính '+nameAttribute;
				$('#'+objectId+'_DATETIME_from').focus();
				return 'false__'+msg;
			}
			msg = Utils.getMessageOfInvalidFormatDate(objectId+'_DATETIME_from');
			if(msg.length > 0){
				msg = 'Thuộc tính '+ Utils.XSSEncode(nameAttribute) +': Giá trị "Từ" không tồn tại hoặc không đúng định dạng dd/mm/yyyy.'; 
				return 'false__'+msg;
			}
			msg = Utils.getMessageOfInvalidFormatDate(objectId+'_DATETIME_to');
			if(msg.length > 0){
				msg = 'Thuộc tính ' + Utils.XSSEncode(nameAttribute) + ': Giá trị "Đến" không tồn tại hoặc không đúng định dạng dd/mm/yyyy.'; 
				return 'false__'+msg;
			}
			if(!Utils.compareDate(DATETIME_from, DATETIME_to)){
				msg = msgErr_fromdate_greater_todate;
				msg = 'Thuộc tính '+nameAttribute+': '+msg;
				return 'false__'+msg;
			}
			return 'true__'+dataOfAutoAttribute;
		}
	},
	checkSaleLevelCatIdInList: function(idSaleCatLevel,listData3){
		if(listData3!=null && listData3.length > 0){
			for(var j=0;j<listData3.length;++j){
				if(listData3[j].idSaleCatLevel == idSaleCatLevel){
					return true;
				}
			}
		}else{
			return false;
		}
	},
	loadAppliedAttributes: function(promotionId){
		$('.ErrorMsgStyle').hide();
		if(PromotionSupport._flagAjaxTabAttribute){
			PromotionSupport._flagAjaxTabAttribute = false;
			$.getJSON("/promotion-support/load-applied-attributes?promotionId="+promotionId, function(result){
				PromotionSupport._flagAjaxTabAttribute = true;
				var promotionStatus = $('#promotionStatus').val();
				var list = result.list;
				if (list != null) {
					for (var k = 0; k < list.length; k++){
						//đoạn này viết chung cho 1 attribute
						var objectType = list[k].objectType;
						var objectId = list[k].objectId;
						var name = list[k].name;
						name = Utils.XSSEncode(name);
						var html = '';
						//end đoạn viết chung.
						if (objectType == 2) {
							html+= '<div>';
							html+= '<div class="Clear"></div>';
							html+= '<li>';
							html+= '<input name="'  + name + '" objectType="'+ objectType +'" objectId="'+objectId +'" type="checkbox"  class="InputCbxStyle checkBoxAttribute"/>';
							html+= '<label class="LabelStyle Label4Style">'+name+'</label>';
							html+= '</li>';
							html+= '<div class="BoxSelect BoxSelect2">';
							html+= '<select id="customerType" style="width: 275px;" class="MySelectBoxClass " multiple="multiple">';
							var listData2 = list[k].listData;	
							var arrHtml = new Array();
							if (listData2 != null) {
								for (var i = 0; i < listData2.length; i++) {
									if (listData2[i].checked == true) {
										arrHtml.push('<option value="'+ listData2[i].idChannelType +'" selected="selected">'+Utils.XSSEncode(listData2[i].codeChannelType) +'-'+Utils.XSSEncode(listData2[i].nameChannelType) +'</option>');
									}else{
										arrHtml.push('<option value="'+ listData2[i].idChannelType +'">'+Utils.XSSEncode(listData2[i].codeChannelType) +'-'+Utils.XSSEncode(listData2[i].nameChannelType) +'</option>');
									}
								}
							}
							html+= arrHtml.join("");
							html+= '</select>';
							html+= '</div>';
							html+= '<div class="Clear"></div>';
							html+= '</div>';
							$('#right').append(html);
						} else if (objectType == 3) {
							var listData3 = list[k].listData;	
							var objectType = 3;
							var objectId = 0;
							var name = "Mức doanh số";
							var html = '';
							html+= '<div id="saleLevel">';//Nhớ đặt id chỗ này để phân biệt, Ví dụ phân biệt: combobox(select)của mức doanh số, của loại khách hàng. 
							html+= '<div class="Clear"></div>';
							html+= 	'<li>';
							html+=	'<input name="'  + name + '" objectType="'+ objectType +'" objectId="'+objectId +'" type="checkbox"  class="InputCbxStyle checkBoxAttribute"/>';
							html+=	'<label class="LabelStyle Label4Style">'+Utils.XSSEncode(name)+'</label>';
							html+=	'</li>';
							//end đoạn giống nhau
							/*for qua cái list, add những label ngành hàng vào. Các lable Ngành hàng thì mình đặt chung 1 class "cat", để sau này mình lặp each 
							qua mỗi label, lấy catId(idProductInfoVO)của label đó, và lấy tất cả các mức mà người ta chọn, vậy cái combobox mức nên đặt id theo
							id của ngành hàng.*/
							var listProductInfoVO = list[k].listProductInfoVO;	
							if (listProductInfoVO != null) {
								for (var i = 0; i < listProductInfoVO.length; i++) {
									if (i == 0) {// if else để canh chỉnh html cho thẳng hàng đó mà.
										//label ngành hàng nên đặt 1 thuộc tính là catId
										html+= '<label catId="'+listProductInfoVO[i].idProductInfoVO +'" style="width: 150px;" class="LabelStyle Label5Style cat">'+Utils.XSSEncode(listProductInfoVO[i].codeProductInfoVO)+'</label>';
										//add combobox mức vao label nganh hang, combobox nên đặt id theo ngành hàng.
										html+= '<div class="BoxSelect BoxSelect2">';
										html+= '<select id="saleLevel-'+listProductInfoVO[i].idProductInfoVO +'" class="MySelectBoxClass saleLevelCat" multiple="multiple">';
										var listSaleLevelCat = listProductInfoVO[i].listSaleCatLevelVO;
										var arrHtml = new Array();
										if (listSaleLevelCat != null) {
											for(var t=0;t<listSaleLevelCat.length;++t){
												var checked = PromotionSupport.checkSaleLevelCatIdInList(listSaleLevelCat[t].idSaleCatLevel,listData3);
												if(checked == true){
													arrHtml.push('<option value="'+ listSaleLevelCat[t].idSaleCatLevel +'" selected="selected">'+ Utils.XSSEncode(listSaleLevelCat[t].codeSaleCatLevel) +'-'+Utils.XSSEncode(listSaleLevelCat[t].nameSaleCatLevel) +'</option>');
												}else{
													arrHtml.push('<option value="'+ listSaleLevelCat[t].idSaleCatLevel +'">'+ Utils.XSSEncode(listSaleLevelCat[t].codeSaleCatLevel) +'-'+Utils.XSSEncode(listSaleLevelCat[t].nameSaleCatLevel) +'</option>');
												}
											}
										}
										html+= arrHtml.join("");
										html+= '</select>';
										html+= '</div>';
									}else{
										html+= '<div class="Clear"></div>';
										html+= '<label catId="'+listProductInfoVO[i].idProductInfoVO +'" style="width: 150px;" class="LabelStyle Label6Style">'+Utils.XSSEncode(listProductInfoVO[i].codeProductInfoVO)+'</label>';
										html+= '<div class="BoxSelect BoxSelect2">';
										html+= '<select id="saleLevel-'+listProductInfoVO[i].idProductInfoVO +'" class="MySelectBoxClass saleLevelCat" multiple="multiple">';
										var listSaleLevelCat = listProductInfoVO[i].listSaleCatLevelVO;
										var arrHtml = new Array();
										if(listSaleLevelCat!=null){
											for(var t=0;t<listSaleLevelCat.length;++t){
												var checked = PromotionSupport.checkSaleLevelCatIdInList(listSaleLevelCat[t].idSaleCatLevel,listData3);
												if(checked == true){
													arrHtml.push('<option value="'+ listSaleLevelCat[t].idSaleCatLevel +'" selected="selected">'+ Utils.XSSEncode(listSaleLevelCat[t].codeSaleCatLevel) +'-'+Utils.XSSEncode(listSaleLevelCat[t].nameSaleCatLevel) +'</option>');
												}else{
													arrHtml.push('<option value="'+ listSaleLevelCat[t].idSaleCatLevel +'">'+ Utils.XSSEncode(listSaleLevelCat[t].codeSaleCatLevel) +'-'+Utils.XSSEncode(listSaleLevelCat[t].nameSaleCatLevel) +'</option>');
												}
											}
										}
										html+= arrHtml.join("");
										html+= '</select>';
										html+= '</div>';
									}
								}
							}
							html+= '<div class="Clear"></div>';
							html+= '</div>';
							$('#right').append(html);
						}else if(objectType == 1){
							var valueType = list[k].valueType;
							var listData1 = list[k].listData;
							
							html+= '<div>';
							html+= '<div class="Clear"></div>';
							html+= '<li>';
							//thằng input của thuộc tính động cần bỏ valueType vào để sau này xử lý khi lưu.
							html+= '<input valueType="'+valueType+'"  name="'  + name + '" objectType="'+ objectType +'" objectId="'+objectId +'" type="checkbox"  class="InputCbxStyle checkBoxAttribute"/>';
							html+= '<label class="LabelStyle Label4Style">'+Utils.XSSEncode(name)+'</label>';
							html+= '</li>';
								
							if(listData1!=null){
								if(valueType=="CHARACTER"){
									//Nên đặt id html của text CHARACTER này theo attributeId cộng chuỗi CHARACTER  thì sẽ không bị conflict trong danh sách. Sau này muốn lưu thì
									//select theo attributeId để lấy ra dữ liệu.
									html+= '<input id="'+objectId+'_CHARACTER" type="text" style="width: 377px;" value="'+listData1[0]+'" class="InputTextStyle InputText1Style"/>';
								}else if(valueType=="NUMBER"){
									//Đặt id theo: attributeId_NUMBER_from và attributeId_NUMBER_to:
									var from = '';
									var to = '';
									if(listData1[0]!=null){
										from = listData1[0];
									}
									if(listData1[1]!=null){
										to = listData1[1];
									}
									html+= '<input id="'+objectId+'_NUMBER_from" style="width: 170px;" type="text" value="'+formatCurrency(from)+'" class="InputTextStyle InputText5Style"/>';
									html+= '<label class="LabelStyle Label7Style">-</label>';
									html+= '<input id="'+objectId+'_NUMBER_to" style="width: 170px;" type="text" value="'+formatCurrency(to)+'" class="InputTextStyle InputText5Style"/>';
								}else if(valueType=="DATE_TIME"){
									//Đặt id theo: attributeId_DATETIME_from và attributeId_DATETIME_to. Nhớ applyDateTimePicker applyDateTimePicker("#endDate");
									//Mình đặt chung bọn chúng 1 class rồi,applyDateTimePicker cho class đó được không nhỉ. Tí thử. 
									var from = '';
									var to = '';
									if(listData1[0]!=null){
										from = listData1[0];
									}
									if(listData1[1]!=null){
										to = listData1[1];
									}
									html+= '<input id="'+objectId+'_DATETIME_from"  style="width: 146px;"  type="text" value="'+from+'" class="InputTextStyle InputText6Style"/>';
									html+= '<label class="LabelStyle Label7Style">-</label>';
									html+= '<input id="'+objectId+'_DATETIME_to" style="width: 146px;"  type="text" value="'+to+'" class="InputTextStyle InputText6Style"/>';
								}else if(valueType=="CHOICE" || valueType=="MULTI_CHOICE"){
									var arrHtml = new Array();
									html+= '<div class="BoxSelect BoxSelect2">';
									html+= '<select id="'+objectId+'" class="MySelectBoxClass" multiple="multiple">';
									for(var i=0;i<listData1.length;++i){
										if(listData1[i].checked == true){
											arrHtml.push('<option value="'+ listData1[i].enumId +'" selected="selected">'+Utils.XSSEncode(listData1[i].code)+'-'+Utils.XSSEncode(listData1[i].name) +'</option>');
										}else{
											arrHtml.push('<option value="'+ listData1[i].enumId +'">'+Utils.XSSEncode(listData1[i].code)+'-'+Utils.XSSEncode(listData1[i].name) +'</option>');
										}
									}
									html+= arrHtml.join("");
									html+= '</select>';
									html+= '</div>';
								}
							}
							html+= '<div class="Clear"></div>';
							html+= '</div>';
							$('#right').append(html);
							//dự thảo thì ko disable
							if(valueType=="DATE_TIME"){
								VTUtilJS.applyDateTimePicker(objectId+'_DATETIME_from');
								VTUtilJS.applyDateTimePicker(objectId+'_DATETIME_to');
							}else if(valueType=="NUMBER"){
								Utils.bindFormatOnTextfield(objectId+'_NUMBER_from',Utils._TF_NUMBER);
								Utils.formatCurrencyFor(objectId+'_NUMBER_from');
								Utils.bindFormatOnTextfield(objectId+'_NUMBER_to',Utils._TF_NUMBER);
								Utils.formatCurrencyFor(objectId+'_NUMBER_to');
							}
						}
					}
					//Xong for:
					$('#right .MySelectBoxClass').each(function(){
						var aCombobox = $(this);
						if(!(aCombobox.next().hasClass('ui-dropdownchecklist'))){//Cái này để kiểm tra comboBox đó đã gọi dropdownchecklist lần nào chưa.
							//Nếu nó đã gọi dropdownchecklist 1 lần rồi, thì thể nào cũng gien ra 1 element html có class ui-dropdownchecklist nằm kế bên comboBox.
							aCombobox.dropdownchecklist({ maxDropHeight: 150,width: 277, textFormatFunction: function(options){
								var text = '';
								$('.ui-dropdownchecklist-dropcontainer input[type=checkbox]').each(function(){
									var aCheckBox = $(this);
									if(aCheckBox.parent().parent().parent().prev().prev().attr('id') == aCombobox.attr('id')){//Tức là chỉ xử lý với những checkbox thuộc combobox đó.
										if(aCheckBox.attr('checked')=='checked'){
											text += aCheckBox.next().text().split('-')[0] + ', ';
										}
									}
								});
								if(text != ''){
									text = text.substring(0, text.length-2);//bo dau ', ' cuoi cung.
								}
								return text;
							} });
						}
					});
				}
				//ko dự thảo thì disable.
				if(promotionStatus != 2 /*|| $('#permissionUser').val() == 'false'*/){
					$("#right .MySelectBoxClass").dropdownchecklist("disable");
					$("#right :input").attr('disabled',true);
					$("#left :input").attr('disabled',true);
					$('#updateCustomerAttribute').hide();
					$('#right .CalendarLink').hide();
				}
			});	
		}
	}
}
/*
 * END OF FILE - /web/web/resources/scripts/business/program/vnm.promotion-support.js
 */

/*
 * START OF FILE - /web/web/resources/scripts/business/program/vnm.key-shop.js
 */
var KeyShop = {
	_xhrSave : null,
	_xhrDel: null,
	_mapKSSearch: null,
	_mapKSLevelSearch: null,
	_mapKSCustomerSearch: null,
	_mapKSCustomerLevelSearch: null,
	_widthProductCat: 250,
	_isLoadGridProduct: 1,
	_isLoadGridLevel: 1,
	_isLoadGridReward:1,
	_isLoadGridShop:1,
	_rowIndex:-1,
	_maxLevel: 0,
	_arrShopInOrgAccessCheked: null,
	_arrShopInOrgAccess: null,
	_processType: null,
	_phCheckedMap: null,
	_searchKSParam: null,
	_searchKSCustomerParam: null,
	_searchKSRewardParam: null,
	_isDoneLoadNum: false,
	getUrlSearchKS:function(){
		var obj = new Object();
		obj.shopId = $('#shopId').val().trim();
		obj.code = $('#code').val().trim();
		obj.year = $('#year').val().trim();
		if (KeyShop._isDoneLoadNum) {
			obj.num = $('#num').combobox('getValue').trim();
		} else {
			obj.num = $('#currentNum').val();
		}		
		obj.status = $('#status').val();
		obj.ksType = $('#ksType').val();
		KeyShop._searchKSParam = obj;
		return '/key-shop/searchKS?' + $.param(obj, false);
	},
	getUrlSearchKSCustomer:function(){
		
		var obj = new Object();
		obj.ksId = $('#ksIdCurrent').val();
		obj.shopId = $('#shopId').val().trim();
		obj.code = $('#customerMix').val().trim();
		obj.year = $('#yearBottom').val().trim();
		obj.num = $('#numBottom').combobox('getValue').trim();
		obj.status = $('#statusBottom').val();
		KeyShop._searchKSCustomerParam = obj;
		return '/key-shop/searchKSCustomer?' + $.param(obj, true);
	},
	getUrlSearchKSReward:function(){		
		var obj = new Object();
		obj.ksId = $('#ksIdCurrent').val();
		obj.shopId = $('#shopId').val().trim();
		obj.code = $('#customerReward').val().trim();
		obj.year = $('#yearReward').val().trim();
		obj.num = $('#numReward').combobox('getValue').trim();
		obj.status = $('#statusReward').val();
		obj.resultReward = $('#resultReward').val();
		KeyShop._searchKSRewardParam = obj;
		return '/key-shop/searchKSReward?' + $.param(obj, true);
	},
	getUrlSearchKSProduct:function(){
		var obj = new Object();
		obj.code = $('#productCode').val().trim();
		obj.ksId = $('#ksIdCurrent').val();
		if ($('#category').val() != null) {
			obj.lstProductInfoId = $('#category').val();
		}
		if ($('#categoryChild').val() != null) {
			obj.lstProductInfoChildId = $('#categoryChild').val();
		}
		return '/key-shop/searchKSProduct?' + $.param(obj, true);
	},
	getUrlSearchKSLevel:function(){
		var obj = new Object();
		obj.ksId = $('#ksIdCurrent').val();
		return '/key-shop/searchKSLevel?' + $.param(obj, true);
	},
	getUrlSearchKSCustomerLevel:function(){
		var numPopup = $('#numPopup').combobox('getValue');
		if ($('#numPopup').combobox('getValue')<10) numPopup='0'+$('#numPopup').combobox('getValue');
		else numPopup=$('#numPopup').combobox('getValue');
		var cycleIdPopup = $('#yearPopup').val() +''+ numPopup+''+numPopup;
		var obj = new Object();
		obj.ksId = $('#ksIdCurrent').val();
		obj.customerId = $('#customerId').val();
		obj.cycleId=cycleIdPopup;
		return '/key-shop/searchKSCustomerLevel?' + $.param(obj, true);
	},
	searchKS:function() {
		$('#errMsg').html('').hide();
		var url = KeyShop.getUrlSearchKS();
		$("#gridKS").datagrid({url:url,pageNumber:1});
	},
	lockReward:function(){
		var dataModel = new Object();
		var lstKsCustomerId = new Array();
		var lockStatus = 1;
		var obj = KeyShop._phCheckedMap.valArray;
		for(var i=0;i<obj.length;i++){
			lstKsCustomerId.push(obj[i]["id"]);			
		}
		dataModel.lstKsCustomerId = lstKsCustomerId;
		dataModel.lockStatus = lockStatus;
		Utils.addOrSaveData(dataModel, "/key-shop/lock-customer-reward", null, 'errMsgKeyShop', function(data) {			
			if (data.error) {				
				$('#errMsgKeyShop').html(data.errMsg.replace(/\|/g, '<br>')).show();				
			}else {
				$('#successMsgKeyShop').html('Khóa trả thưởng thành công').change().show();				
				setTimeout(function(){
					$('#successMsg').html('').change().show();
					$('#gridReward').datagrid('reload');
					KeyShop._phCheckedMap.clear();
				}, 700);
			  }
			
			}, null, null, null, null, function(data) {
				if (data.error) {				
					$('#errMsgKeyShop').html(data.errMsg.replace(/\|/g, '<br>')).show();
					$('#gridReward').datagrid('reload');
					KeyShop._phCheckedMap.clear();
				}
			});		
		return false;		
	},
	unlockReward:function(){
		var dataModel = new Object();
		var lstKsCustomerId = new Array();
		var lockStatus = 2;
		var obj = KeyShop._phCheckedMap.valArray;
		for(var i=0;i<obj.length;i++){
			lstKsCustomerId.push(obj[i]["id"]);			
		}
		//dataModel.startDate = $('#startDate').val();
		dataModel.lstKsCustomerId = lstKsCustomerId;
		dataModel.lockStatus = lockStatus;
		Utils.addOrSaveData(dataModel, "/key-shop/lock-customer-reward", null, 'errMsgKeyShop', function(data) {			
			if (data.error) {				
				$('#errMsgKeyShop').html(data.errMsg.replace(/\|/g, '<br>')).show();				
			}else {
				$('#successMsgKeyShop').html('Khóa trả thưởng thành công').change().show();				
				setTimeout(function(){
					$('#successMsg').html('').change().show();	
					$('#gridReward').datagrid('reload');
					KeyShop._phCheckedMap.clear();
				}, 700);
			  }
			
			}, null, null, null, null, function(data) {
				if (data.error) {				
					$('#errMsgKeyShop').html(data.errMsg.replace(/\|/g, '<br>')).show();
					$('#gridreward').datagrid('reload');
					KeyShop._phCheckedMap.clear();
				}
			});		
		return false;		
	},
	unlockReward:function(){
		var dataModel = new Object();
		var lstKsCustomerId = new Array();
		var lockStatus = 2;
		var obj = KeyShop._phCheckedMap.valArray;
		for(var i=0;i<obj.length;i++){
			lstKsCustomerId.push(obj[i]["id"]);			
		}
		//dataModel.startDate = $('#startDate').val();
		dataModel.lstKsCustomerId = lstKsCustomerId;
		dataModel.lockStatus = lockStatus;
		Utils.addOrSaveData(dataModel, "/key-shop/lock-customer-reward", null, 'errMsgKeyShop', function(data) {			
			if (data.error) {				
				$('#errMsgKeyShop').html(data.errMsg.replace(/\|/g, '<br>')).show();				
			} else {
				$('#successMsgKeyShop').html('Mở khóa trả thưởng thành công').change().show();				
				setTimeout(function(){
					$('#successMsgKeyShop').html('').change().show();	
					$('#gridReward').datagrid('reload');
					KeyShop._phCheckedMap.clear();
				}, 700);
			  }
			
			}, null, null, null, null, function(data) {
				if (data.error) {				
					$('#errMsgKeyShop').html(data.errMsg.replace(/\|/g, '<br>')).show();
					$('#gridReward').datagrid('reload');
					KeyShop._phCheckedMap.clear();
				}
			});		
		return false;		
	},
	initPopupEditKS:function(ks) {
		enable('ksCodeKS');
		$('#ksIdKS').val('');
		$('#ksCodeKS').val('');
		$('#ksNameKS').val('');
		$('#minPhotoKS').val('');
		$('#descriptionKS').val('');
		if (ks != undefined && ks != null) {
			disabled('ksCodeKS');
			$('#ksIdKS').val(ks.ksId);
			$('#ksCodeKS').val(Utils.XSSEncode(ks.ksCode));
			$('#ksNameKS').val(Utils.XSSEncode(ks.ksName));
			$('#ksTypeKS').val(ks.ksType).change();
			$('#fromCycleKS').val(ks.fromCycleId).change();
			$('#toCycleKS').val(ks.toCycleId).change();
			$('#minPhotoKS').val(ks.minPhoto);
			$('#descriptionKS').val(Utils.XSSEncode(ks.description));
			$('#statusKS').val(ks.status).change();
		}
	},
	openPopupEditKS:function(id) {
		$('#errMsgKS, #successMsgKS').hide();
		var ks = KeyShop._mapKSSearch.get(id);
		var html = $('#addOrEditKSDiv').html();
		KeyShop.initPopupEditKS(ks);
		$('#addOrEditKS').dialog({
			title : 'Thông tin chương trình HTTM',
			width:800,
			height:'auto',
			onOpen: function(){
				if ($('#ksCodeKS').val() != '') {
					$('#ksNameKS').focus();
				} else {
					$('#ksCodeKS').focus();
				}
			},
			onClose: function(){
				$('#addOrEditKSDiv').html(html);
			}
		});
		$('#addOrEditKS').dialog('open');
	},
	editKS:function() {
		$('#errMsgKS, #successMsgKS').hide();
		var msg = '';
		if (msg.length == 0) {
			msg = Utils.getMessageOfRequireCheck('ksCodeKS', 'Mã CT');
		}
		if (msg.length == 0) {
			msg = Utils.getMessageOfSpecialCharactersValidate('ksCodeKS', 'Mã CT');
		}
		if (msg.length == 0) {
			msg = Utils.getMessageOfRequireCheck('ksNameKS', 'Tên CT');
		}
		if (msg.length == 0) {
			msg = Utils.getMessageOfSpecialCharactersValidate('ksNameKS', 'Tên CT', Utils._SPECIAL);
		}
		if (msg.length == 0) {
			msg = Utils.getMessageOfSpecialCharactersValidate('descriptionKS', 'Mô tả', Utils._SPECIAL);
		}
		if (msg.length > 0) {
			$('#errMsgKS').html(msg).show();
			return;
		}
		var dataModel = new Object();
		dataModel.ksId = $('#ksIdKS').val();
		dataModel.ksCode = $('#ksCodeKS').val().trim();
		dataModel.ksName = $('#ksNameKS').val().trim();
		dataModel.ksType = $('#ksTypeKS').val();
		dataModel.fromCycleId = $('#fromCycleKS').val();
		dataModel.toCycleId = $('#toCycleKS').val();
		dataModel.minPhoto = $('#minPhotoKS').val().trim();
		dataModel.description = $('#descriptionKS').val().trim();
		dataModel.status = $('#statusKS').val();
		Utils.addOrSaveData(dataModel, "/key-shop/editKS", null, 'errMsgKS', function(data) {
			$('#successMsgKS').html('Lưu dữ liệu thành công').show();
			KeyShop.searchKS();
			setTimeout(function() {
				$('.easyui-dialog').dialog('close');
			}, 2000);
		});
		return false;
	},
	initPopupEditKSLevel:function(obj) {
		enable('levelCode');
		$('#levelId').val('');
		$('#levelCode').val('');
		$('#levelName').val('');
		$('#amountLevel').val('');
		$('#quantityLevel').val('');
		if (obj != undefined && obj != null) {
			disabled('levelCode');
			$('#levelId').val(obj.ksLevelId);
			$('#levelCode').val(obj.ksLevelCode);
			$('#levelName').val(obj.ksLevelName);
			$('#amountLevel').val(formatCurrency(obj.amount));
			$('#quantityLevel').val(formatCurrency(obj.quantity));
		}
	},
	openPopupEditKSLevel:function(levelId) {
		$('#errMsgKSLevel, #successMsgKSLevel').hide();
		var kl = KeyShop._mapKSLevelSearch.get(levelId);
		var html = $('#addOrEditKSLevelDiv').html();
		KeyShop.initPopupEditKSLevel(kl);
		$('#addOrEditKSLevel').dialog({
			title : 'Thông tin mức chương trình',
			width:500,
			height:'auto',
			onOpen: function(){
				if ($('#levelCode').val() != '') {
					$('#levelName').focus();
				} else {
					$('#levelCode').focus();
				}
			},
			onClose: function(){
				$('#addOrEditKSLevelDiv').html(html);
			}
		});
		$('#addOrEditKSLevel').dialog('open');
	},
	initPopupEditKSCustomer:function(kc) {
		$('#errMsgKSCustomerLevel').html('').hide();
		if (kc==null) {
			enable('customerCode');
			enable('statusPopup');
			enable('yearPopup');
			enable('numPopup');
			$('#lblTrangThai').hide();
			$('#statusPopupDiv').hide();
			$('#customerCode').val('');
			$('#customerId').val('');
			$('#group_dt_kh_d_tc_btnApproveCustomerLevel').hide();
			$('#group_dt_kh_d_tc_btnDenyCustomerLevel').hide();
			$('#group_edit_ks_btnUpdateCustomerLevel').show();
			
			var yearBottom = $('#yearBottom').val();
			if(!isNullOrEmpty(yearBottom)) {
				$('#yearPopup').val(yearBottom).change();
			}
			
//			var numBottom = $('#numBottom').combobox('getValue');
//			if(!isNullOrEmpty(numBottom)) {
//				$('#numPopup').combobox('setValue', numBottom);
//			}
			
//			$('#numPopup').combobox('setValue', $('#numBottom').combobox('getValue'));
//			$('#yearPopup').val($('#yearBottom').val()).change();
//			$('#numPopup').val($('#numBottom').combobox('getValue')).change();
		} else {
			disabled('customerCode');
			$('#lblTrangThai').show();
			$('#statusPopupDiv').show();
			$('#statusPopup').val(kc.status);
			$('#statusPopup').change();
			disabled('statusPopup');
			disabled('yearPopup');
			disabled('numPopup');
			$('#group_dt_kh_d_tc_btnApproveCustomerLevel').hide();
			$('#group_dt_kh_d_tc_btnDenyCustomerLevel').hide();
			$('#group_edit_ks_btnUpdateCustomerLevel').show();
			
			if (kc.rewardType!=null) {
				$('#group_dt_kh_d_tc_btnApproveCustomerLevel').hide();
				$('#group_dt_kh_d_tc_btnDenyCustomerLevel').hide();
				$('#group_edit_ks_btnUpdateCustomerLevel').hide();
			}
			if (kc.cycleId!=null) {
				$('#yearPopup').val(Number(kc.cycleId.toString().substring(0, 4))).change();
//				$('#numPopup').combobox('setValue', Number(kc.cycleId.toString().substring(6, 8)));
			} else {
//				$('#numPopup').combobox('setValue', $('#numBottom').combobox('getValue'));
//				$('#yearPopup').val($('#yearBottom').val()).change();
				var yearBottom = $('#yearBottom').val();
				if(!isNullOrEmpty(yearBottom)) {
					$('#yearPopup').val(yearBottom).change();
				}
				
//				var numBottom = $('#numBottom').combobox('getValue');
//				if(!isNullOrEmpty(numBottom)) {
//					$('#numPopup').combobox('setValue', numBottom);
//				}
			}
//			var ksId = $('#ksIdCurrent').val().trim();
//			KeyShop.loadGridCustomerLevel(ksId);
		} 
	},
	openPopupEditKSCustomer:function(customerId) { // customerId, customerCode, rewardType
		
		$('#errMsgKSLevel, #successMsgKSCustomer').hide();
		$('#addressPop').html('');
		var ksId = $('#ksIdCurrent').val().trim();
		var ks = KeyShop._mapKSSearch.get(ksId);
		KeyShop.loadKsYearEditCustomer(ks);
		$('#ksCustomerId').val(customerId);
		if (customerId!=null) {
			var kc = KeyShop._mapKSCustomerSearch.get(customerId);
			if (kc!=null) {
				$('#customerCode').val(kc.customerCode);
				$('#customerId').val(kc.customerId);
				$('#addressPop').html(kc.address);
//				var html = $('#addOrEditKSLevelCustomer').html();
				KeyShop.initPopupEditKSCustomer(kc);
			}
		} else {
			$('#customerCode').val('');
			$('#customerId').val('');
			$('#addressPop').val('');
			KeyShop.initPopupEditKSCustomer();
		}
		
//		KeyShop.loadGridCustomerLevel(ksId);
		$('#addOrEditKSCustomer').dialog({
			title : 'Thông tin khách hàng',
			width:900,
			height:'auto',
			onOpen: function() {
				if ($('#customerCode').val() != '') {
					//$('#customerName').focus();
				} else {
					$('#customerCode').focus();
				}
			},
			onClose: function() {
//				$('#addOrEditKSCustomerDiv').html(html);
			}
		});
		$('#addOrEditKSCustomer').dialog('open');
	},
	loadKsYearEditCustomer:function(ks) {
		$('#yearPopup').html('');
		var list = ks.lstYear;
		if (list != undefined && list != null && list.length > 0) {
			for (var i = 0; i < list.length; i++) {
				$('#yearPopup').append('<option value = "' + Utils.XSSEncode(list[i]) + '">' + Utils.XSSEncode(list[i]) + '</option>');  
			}
		}
//		$('#yearPopup').change();
	},
	editKSLevel:function() {
		$('#errMsgKSLevel, #successMsgKSLevel').hide();
		var msg = '';
		if (msg.length == 0) {
			msg = Utils.getMessageOfRequireCheck('levelCode', 'Mã mức');
		}
		if (msg.length == 0) {
			msg = Utils.getMessageOfSpecialCharactersValidate('levelCode', 'Mã mức');
		}
		if (msg.length == 0) {
			msg = Utils.getMessageOfRequireCheck('levelName', 'Tên mức');
		}
		if (msg.length == 0) {
			msg = Utils.getMessageOfSpecialCharactersValidate('levelName', 'Tên mức', Utils._SPECIAL);
		}
		if (msg.length == 0) {
			msg = Utils.getMessageOfRequireCheck('amountLevel', 'Số tiền');
		}
		if (msg.length > 0) {
			$('#errMsgKSLevel').html(msg).show();
			return;
		}
		var dataModel = new Object();
		dataModel.ksId = $('#ksIdCurrent').val()
		dataModel.ksLevelId = $('#levelId').val();
		dataModel.ksLevelCode = $("#levelCode").val().trim();
		dataModel.ksLevelName = $('#levelName').val().trim();
		dataModel.amount = $('#amountLevel').val().trim();
		dataModel.quantity = $('#quantityLevel').val().trim();
		Utils.addOrSaveData(dataModel, "/key-shop/editKSLevel", null, 'errMsgKSLevel', function(data) {
			$('#successMsgKSLevel').html('Lưu dữ liệu thành công').show();
			KeyShop.searchKSLevel();
			setTimeout(function() {
				$('.easyui-dialog').dialog('close');
			}, 2000);
		});
		return false;
	},
	deleteKSLevel:function(levelId){
		var dataModel = new Object();
		dataModel.ksLevelId = levelId;
		Utils.addOrSaveData(dataModel, "/key-shop/deleteKSLevel", null, 'errMsgKeyShop', function(data) {
			KeyShop.searchKSLevel();
			setTimeout(function() {
				$('.easyui-dialog').dialog('close');
			}, 2000);
		},null, null, true);
	},
	hideAllDiv:function() {
		$('#productDiv').hide();
		$('#levelDiv').hide();
		$('#shopDiv').hide();
		$('#customerDiv').hide();
		$('#rewardDiv').hide();
		$('#errMsgKeyShop').html('').hide();
	},
	showProductGrid:function(ksId) {
		KeyShop.hideAllDiv();
		var ks = KeyShop._mapKSSearch.get(ksId);
		if (ks == null) return;
		$('#productDiv').show();
		$('#productTitle').html('Sản phẩm của ' + Utils.XSSEncode(ks.ksName));
		$('#ksIdCurrent').val(ksId);
		if(KeyShop._isLoadGridProduct != 1){
			var url = KeyShop.getUrlSearchKSProduct()
			$("#gridProduct").datagrid({url:url,pageNumber:1});
		}else{
			KeyShop.fillCategoriesToMultiSelect();
			KeyShop._isLoadGridProduct = 0;
			KeyShop.loadGridProduct(ksId);
		}
	},
	showLevelGrid:function(ksId) {
		KeyShop.hideAllDiv();
		var ks = KeyShop._mapKSSearch.get(ksId);
		if (ks == null) return;
		$('#levelDiv').show();
		$('#levelTitle').html('Các mức của ' + Utils.XSSEncode(ks.ksName));
		$('#ksIdCurrent').val(ksId);
		if(KeyShop._isLoadGridLevel != 1){
			KeyShop.searchKSLevel();
		}else{
			KeyShop._isLoadGridLevel = 0;
			KeyShop.loadGridLevel(ksId);
		}
	},
	showCustomerGrid:function(ksId) {
		$('#dgGridContainerCustomer').html('<table id="gridCustomer"></table>');
		KeyShop.hideAllDiv();
		var ks = KeyShop._mapKSSearch.get(ksId);
		if (ks == null) return;
		$('#customerDiv').show();
		$('#customerTitle').html('KHÁCH HÀNG ĐĂNG KÝ THAM GIA ' + Utils.XSSEncode(ks.ksName));
		$('#ksIdCurrent').val(ksId);
		ReportUtils.loadComboTree('shopBottom', 'shopId', $('#curShopId').val(), function(shopId){});
		KeyShop.loadKsYear(ks);
		KeyShop.loadGridCustomer(ksId);
	},
	loadKsYear:function(ks) {
		$('#yearBottom').html('');
		var list = ks.lstYear;
		if (list != undefined && list != null && list.length > 0) {
			for (var i = 0; i < list.length; i++) {
				$('#yearBottom').append('<option value = "' + Utils.XSSEncode(list[i]) + '">' + Utils.XSSEncode(list[i]) + '</option>');  
			}
		}
		$('#yearBottom').change();
	},
	showRewardGrid:function(ksId) {
		KeyShop.hideAllDiv();
		var ks = KeyShop._mapKSSearch.get(ksId);
		if (ks == null) return;
		$('#rewardDiv').show();
		$('#rewardTitle').html('THÔNG TIN TRẢ THƯỞNG KHÁCH HÀNG ' + Utils.XSSEncode(ks.ksName));
		$('#ksIdCurrent').val(ksId);
		ReportUtils.loadComboTree('shopReward', 'shopId', $('#curShopId').val(), function(shopId){});
		KeyShop._phCheckedMap = new Map();
		KeyShop.loadGridReward(ksId);
	},
	searchKSProduct:function(){
		var url = KeyShop.getUrlSearchKSProduct();
		$("#gridProduct").datagrid({url:url,pageNumber:1});
	},
	searchKSLevel:function() {
		var url = KeyShop.getUrlSearchKSLevel()
		$("#gridLevel").datagrid({url:url,pageNumber:1});
	},
	searchKSCustomer:function() {
		$('#errMsgKeyShop').html('').hide();
		var url = KeyShop.getUrlSearchKSCustomer();
		$("#gridCustomer").datagrid({url:url,pageNumber:1});
	},
	searchKSReward:function() {
		$('#errMsgKeyShop').html('').hide();
		var url = KeyShop.getUrlSearchKSReward()
		$("#gridReward").datagrid({url:url,pageNumber:1});
	},
	loadGridProduct:function(ksId) {
		var title = '<a title="Thêm mới sản phẩm" href="javascript:void(0)" onclick=\"return KeyShop.openPopupKSProduct();\"><img src="/resources/images/icon_add.png"/></a>';
		$('#gridProduct').datagrid({
			url: KeyShop.getUrlSearchKSProduct(),
			width : $('#dgGridContainerProduct').width() - 17,
			height: 'auto',
			singleSelect: true,
			rownumbers: true,
			fitColumns: true,
			scrollbarSize: 0,
			pagination: true,
			pageList  : [10,20,30],
			columns:[[  
			          {field:'productCode',title:'Mã sản phẩm',width:70, sortable : true, resizable : false, align: 'left', formatter:function(value,row,index){
			        	  return Utils.XSSEncode(value);
			          }},
			          {field:'productName',title:'Tên sản phẩm',width:150, sortable : true, resizable : false, align: 'left', formatter:function(value,row,index){
			        	  return Utils.XSSEncode(value);
			          }},
			          {field:'cat',title:'Ngành hàng',width:70, sortable : true, resizable : false, align: 'left', formatter:function(value,row,index){
			        	  return Utils.XSSEncode(value);
			          }},
			          {field:'subCat',title:'Ngành hàng con',width:70, sortable : true, resizable : false, align: 'left', formatter:function(value,row,index){
			        	  return Utils.XSSEncode(value);
			          }},
			          {field:'brand',title:'Nhãn hàng',width:70, sortable : true, resizable : false, align: 'left', formatter:function(value,row,index){
			        	  return Utils.XSSEncode(value);
			          }},
			          {field:'flavour',title:'Hương vị',width:70, sortable : true, resizable : false, align: 'left', formatter:function(value,row,index){
			        	  return Utils.XSSEncode(value);
			          }},
			          {field:'packing',title:'Bao bì',width:70, sortable : true, resizable : false, align: 'left', formatter:function(value,row,index){
			        	  return Utils.XSSEncode(value);
			          }},
			          {field:'edit',title:title, width: 30,sortable:false,resizable:false, align: 'center', formatter: function(value, row, index) {
			        	  return "<a title='Xóa' href='javascript:void(0)' onclick=\"return KeyShop.deleteProduct('"+ row.productId +"');\"><img height='17' src='/resources/images/icon-delete.png'></a>";
			          }},
			]],
		  	onLoadSuccess :function(data){	    	
		    	$('.datagrid-header-rownumber').html('STT');
		    	var arrDelete =  $('#dgGridContainerProduct td[field="edit"]');
				if (arrDelete != undefined && arrDelete != null && arrDelete.length > 0) {
				  for (var i = 0, size = arrDelete.length; i < size; i++) {
				    $(arrDelete[i]).prop("id", "group_edit_ks_gr_sp_cl_edit_" + i);//Khai bao id danh cho phan quyen
					$(arrDelete[i]).addClass("cmsiscontrol");
				  }
				}
				Utils.functionAccessFillControl('dgGridContainerProduct', function(data){
					//Xu ly cac su kien lien quan den cac control phan quyen
					var arrTmpLength =  $('#dgGridContainerProduct td[id^="group_edit_ks_gr_sp_cl_edit_"]').length;
					var invisibleLenght = $('.isCMSInvisible[id^="group_edit_ks_gr_sp_cl_edit_"]').length;
					if (arrTmpLength > 0 && arrTmpLength != invisibleLenght) {
						$('#gridProduct').datagrid("showColumn", "edit");
					} else {
						$('#gridProduct').datagrid("hideColumn", "edit");
					}
				});
		    },
		    method : 'GET'
		});
	},
	loadGridLevel:function(ksId) {
		var title = '<a title="Thêm mới mức" href="javascript:void(0)" onclick=\"return KeyShop.openPopupEditKSLevel();\"><img src="/resources/images/icon_add.png"/></a>';
		$('#gridLevel').datagrid({
			url: KeyShop.getUrlSearchKSLevel(),
			width : $('#dgGridContainerLevel').width() - 17,
			height: 'auto',
			singleSelect: true,
			rownumbers: true,
			fitColumns: true,
			scrollbarSize: 0,
			pagination: true,
			pageList  : [10,20,30],
			columns:[[  
			          {field:'ksLevelCode',title:'Mã mức',width:70, sortable : true, resizable : false, align: 'left', formatter:function(value,row,index){
			        	  return Utils.XSSEncode(value);
			          }},
			          {field:'ksLevelName',title:'Tên mức',width:150, sortable : true, resizable : false, align: 'left', formatter:function(value,row,index){
			        	  return Utils.XSSEncode(value);
			          }},
			          {field:'amount',title:'Số tiền',width:70, sortable : true, resizable : false, align: 'left', formatter:function(value,row,index){
			        	  return Utils.XSSEncode(formatCurrency(value));
			          }},
			          {field:'quantity',title:'Số lượng',width:70, sortable : true, resizable : false, align: 'left', formatter:function(value,row,index){
			        	  return Utils.XSSEncode(formatCurrency(value));
			          }},
			          {field:'edit',title:title, width: 30,sortable:false,resizable:false, align: 'center', formatter: function(value, row, index) {
			        	  var str = "<a title='Sửa' href='javascript:void(0)' onclick=\"return KeyShop.openPopupEditKSLevel('"+ row.ksLevelId +"');\"><img height='17' src='/resources/images/icon-edit.png'></a>";
			        	  str += "&nbsp&nbsp&nbsp&nbsp";
			        	  str += "<a title='Xóa' href='javascript:void(0)' onclick=\"return KeyShop.deleteKSLevel('"+ row.ksLevelId +"');\"><img height='17' src='/resources/images/icon-delete.png'></a>";
			        	  return str;
			          }},
			]],
		  	onLoadSuccess :function(data){	    	
		    	$('.datagrid-header-rownumber').html('STT');
		    	KeyShop._mapKSLevelSearch = new Map();
		    	for (var i = 0 ; i < data.rows.length ; i++) {
		    		KeyShop._mapKSLevelSearch.put(data.rows[i].ksLevelId, data.rows[i]);
		    	}
		    	var arrDelete =  $('#dgGridContainerLevel td[field="edit"]');
				if (arrDelete != undefined && arrDelete != null && arrDelete.length > 0) {
				  for (var i = 0, size = arrDelete.length; i < size; i++) {
				    $(arrDelete[i]).prop("id", "group_edit_ks_gr_muc_cl_edit_" + i);//Khai bao id danh cho phan quyen
					$(arrDelete[i]).addClass("cmsiscontrol");
				  }
				}
				Utils.functionAccessFillControl('dgGridContainerLevel', function(data){
					//Xu ly cac su kien lien quan den cac control phan quyen
					var arrTmpLength =  $('#dgGridContainerLevel td[id^="group_edit_ks_gr_muc_cl_edit_"]').length;
					var invisibleLenght = $('.isCMSInvisible[id^="group_edit_ks_gr_muc_cl_edit_"]').length;
					if (arrTmpLength > 0 && arrTmpLength != invisibleLenght) {
						$('#gridLevel').datagrid("showColumn", "edit");
					} else {
						$('#gridLevel').datagrid("hideColumn", "edit");
					}
				});
		    },
		    method : 'GET'
		});
	},
	loadGridReward:function(ksId) {			
			$('#gridReward').datagrid({
			url: KeyShop.getUrlSearchKSReward(),
			width : $('#dgGridContainerReaward').width() - 17,
			height: 'auto',
			checkOnSelect :true,
			selectOnCheck: true,
			//singleSelect: true,
			rownumbers: true,
			//fitColumns: true,
			scrollbarSize: 0,
			pagination: true,
			pageList  : [10,20,30],
			frozenColumns:[[
						{field: 'id', index:'id', hidden: true},		
						{field: 'customerId', checkbox: true},
						{field:'shopCode',title:'NPP',width:150, sortable : true, resizable : false, align: 'left', formatter:function(value,row,index){
							  return Utils.XSSEncode(row.shopCode) + ' - ' + Utils.XSSEncode(row.shopName);
						}},
						{field:'customerCode',title:'Mã KH',width:120, sortable : true, resizable : false, align: 'left', formatter:function(value,row,index){
							  return Utils.XSSEncode(value);
						}},
						{field:'customerName',title:'Tên KH',width:150, sortable : true, resizable : false, align: 'left', formatter:function(value,row,index){
							  return Utils.XSSEncode(value);
						}},       
			]],
			columns:[[  
			         
			          {field:'address',title:'Địa chỉ',width:280, sortable : true, resizable : false, align: 'left', formatter:function(value,row,index){
			        	  return Utils.XSSEncode(value);
			          }},
			          {field:'levelCustomer',title:'Mức đăng ký',width:200, sortable : true, resizable : false, align: 'left', formatter:function(value,row,index){
			        	  return Utils.XSSEncode(value);
			          }},
			          {field:'amountTarget',title:'DS kế hoạch',width:100, sortable : true, resizable : false, align: 'right', formatter:function(value,row,index){
			        	  return Utils.XSSEncode(formatCurrency(value));
			          }},
			          {field:'amount',title:'DS đạt',width:100, sortable : true, resizable : false, align: 'right', formatter:function(value,row,index){
			        	  return Utils.XSSEncode(formatCurrency(value));
			          }},
			          {field:'percentAmount',title:'%',width:80, sortable : true, resizable : false, align: 'right', formatter:function(value,row,index){
			        	  return Utils.XSSEncode(value+'%');
			          }},
			          {field:'quantityTarget',title:'SL kế hoạch',width:100, sortable : true, resizable : false, align: 'right', formatter:function(value,row,index){
			        	  return Utils.XSSEncode(formatCurrency(value));
			          }},
			          {field:'quantity',title:'SL đạt',width:100, sortable : true, resizable : false, align: 'right', formatter:function(value,row,index){
			        	  return Utils.XSSEncode(formatCurrency(value));
			          }},
			          {field:'percentQuantity',title:'%',width:80, sortable : true, resizable : false, align: 'right', formatter:function(value,row,index){
			        	  return Utils.XSSEncode(value+'%');
			          }},
			          {field:'result',title:'Kết quả',width:80, sortable: true, align: 'center',resizable:false , formatter:function(value, row, index) {
			        	  if (value == 5) {
			        		  return 'Không đạt';
			        	  } else if (value == 6) {
			        		  return 'Đạt';
			        	  }
			        	  return '';
			          }},
			          {field:'rewardType',title:'Trạng thái trả thưởng',width:150, sortable: true, align: 'center',resizable:false , formatter:function(value, row, index) {
			        	  if (value == 0) {
			        		  return 'Chuyển khoản';
			        	  } else if (value == 1) {
			        		  return 'Chưa mở khóa trả thưởng';
			        	  }else if (value == 2) {
			        		  return 'Đã mở khóa trả thưởng';
			        	  }else if (value == 3) {
			        		  return 'Đã trả 1 phần';
			        	  }else if (value == 4) {
			        		  return 'Đã trả toàn bộ';
			        	  }else if (value == null) {
			        		  return 'Chưa import trả thưởng';
			        	  }
			        	  return '';
			          }},
			          {field:'rewardType1',title:'Loại trả thưởng',width:100, sortable: true, align: 'center',resizable:false , formatter:function(value, row, index) {
			        	  if (row.rewardType == 0) {
			        		  return 'Chuyển khoản';
			        	  } else if (row.rewardType == 1 || row.rewardType == 2 || row.rewardType == 3 || row.rewardType == 4 ) {
			        		  return 'Coupon';
			        	  }
			        	  return '';
			          }},
			          {field:'totalReward',title:'Số tiền trả thưởng',width:100, sortable : true, resizable : false, align: 'right', formatter:function(value,row,index){
			        	  return Utils.XSSEncode(formatCurrency(value));
			          }},
			          {field:'totalRewardDone',title:'Số tiền đã trả',width:100, sortable : true, resizable : false, align: 'right', formatter:function(value,row,index){
			        	  return Utils.XSSEncode(formatCurrency(value));
			          }},
			          {field:'payReward',title:'Còn lại',width:100, sortable : true, resizable : false, align: 'right', formatter:function(value,row,index){
			        	  return Utils.XSSEncode(formatCurrency(value));
			          }},
			          {field:'productReward',title:'Mặt hàng trả thưởng',width:240, sortable : true, resizable : false, align: 'left', formatter:function(value,row,index){
			        	  return Utils.XSSEncode(value);
			          }},
			          {field:'productRewardDone',title:'Mặt hàng đã trả thưởng',width:240, sortable : true, resizable : false, align: 'left', formatter:function(value,row,index){
			        	  return Utils.XSSEncode(value);
			          }},
			          {field:'amountDiscount',title:'Chiết khấu doanh số',width:100, sortable : true, resizable : false, align: 'right', formatter:function(value,row,index){
			        	  return Utils.XSSEncode(formatCurrency(value));
			          }},
			          {field:'overAmountDiscount',title:'Chiết khấu vượt doanh số',width:100, sortable : true, resizable : false, align: 'right', formatter:function(value,row,index){
			        	  return Utils.XSSEncode(formatCurrency(value));
			          }},
			          {field:'displayMoney',title:'Tiền trưng bày',width:100, sortable : true, resizable : false, align: 'right', formatter:function(value,row,index){
			        	  return Utils.XSSEncode(formatCurrency(value));
			          }},
			          {field:'supportMoney',title:'Tiền hỗ trợ',width:100, sortable : true, resizable : false, align: 'right', formatter:function(value,row,index){
			        	  return Utils.XSSEncode(formatCurrency(value));
			          }},
			          {field:'oddDiscount',title:'Trừ chiết khấu phủ lẻ',width:100, sortable : true, resizable : false, align: 'right', formatter:function(value,row,index){
			        	  return Utils.XSSEncode(formatCurrency(value));
			          }},
			          {field:'addMoney',title:'Tiền thưởng bổ sung',width:100, sortable : true, resizable : false, align: 'right', formatter:function(value,row,index){
			        	  return Utils.XSSEncode(formatCurrency(value));
			          }},
			]],
		  	onLoadSuccess :function(data){
		  		$('td[field=customerId] div.datagrid-header-check input[type=checkbox]').attr('checked',false);
		  		KeyShop._phCheckedMap.clear();
		    	$('.datagrid-header-rownumber').html('STT');
		    	if (data != undefined && data != null && data.rows.length > 0) {
		    		enable('btnExportReward');
		    	} else {
		    		disabled('btnExportReward');
		    	}
		    },
		    onCheck: function(i, r) {			
				var obj = new Object();			
				obj.id = r.id;		
				obj.code = r.customerCode;
				obj.name = r.customerName;				
				KeyShop._phCheckedMap.put(obj.id,obj);				
			 },
			 onUncheck: function(i, r) {
				 KeyShop._phCheckedMap.remove(r.id);
			 },
			 onCheckAll: function(rows) {
				 for (var i = 0, sz = rows.length; i < sz; i++) {
					var obj = new Object();
					obj.id = rows[i].id;		
					obj.code = rows[i].customerCode;
					obj.name = rows[i].customerName;			
					KeyShop._phCheckedMap.put(obj.id, obj);			
				 }
			 },
			 onUncheckAll: function(rows) {		
				  for (var i = 0, sz = rows.length; i < sz; i++) {
					  KeyShop._phCheckedMap.remove(rows[i].id);
					 }
			 }, 
		    method : 'GET'
		});
	},
	loadGridCustomer:function(ksId) {
		var title = '<a title="Thêm mới khách hàng" href="javascript:void(0)" onclick=\"return KeyShop.openPopupEditKSCustomer();\"><img src="/resources/images/icon_add.png"/></a>';
		$('#gridCustomer').datagrid({
			url: KeyShop.getUrlSearchKSCustomer(),
			width : $('#dgGridContainerCustomer').width() - 17,
			height: 'auto',
			singleSelect: true,
			rownumbers: true,
			fitColumns: true,
			scrollbarSize: 0,
			pagination: true,
			pageList  : [10,20,30],
			columns:[[  
			          {field:'shopCode',title:'NPP',width:50, sortable : true, resizable : false, align: 'left', formatter:function(value,row,index){
			        	  return Utils.XSSEncode(row.shopCode) + ' - ' + Utils.XSSEncode(row.shopName);
			          }},
			          {field:'customerCode',title:'Mã KH',width:40, sortable : true, resizable : false, align: 'left', formatter:function(value,row,index){
			        	  return Utils.XSSEncode(value);
			          }},
			          {field:'customerName',title:'Tên KH',width:60, sortable : true, resizable : false, align: 'left', formatter:function(value,row,index){
			        	  return Utils.XSSEncode(value);
			          }},
			          {field:'address',title:'Địa chỉ',width:70, sortable : true, resizable : false, align: 'left', formatter:function(value,row,index){
			        	  return Utils.XSSEncode(value);
			          }},
			          {field:'levelCustomer',title:'Mức đăng ký',width:50, sortable : true, resizable : false, align: 'left', formatter:function(value,row,index){
			        	  return Utils.XSSEncode(value);
			          }},
			          {field:'status',title:'Trạng thái',width:40, sortable: true, align: 'center',resizable:false , formatter:function(value, row, index) {
			        	  if (value == 1) {
			        		  return 'Đã duyệt';
			        	  } else if (value == 2) {
			        		  return 'Chưa duyệt';
			        	  } else if (value == 3) {
			        		  return 'Từ chối';
			        	  } else if (value == 0) {
			        		  return 'Tạm ngưng';
			        	  } 
			        	  return '';
			          }},
			          {field:'approve',title:'Duyệt', width: 30,sortable:false,resizable:false, align: 'center', formatter: function(value, row, index) {
			        	  if (row.status==2 && row.rewardType==null)
			        	  {
			        		  var str = "<a class = 'vt-grid-customer-approve' title='Duyệt' href='javascript:void(0)' onclick=\"return KeyShop.ProcessCustomerLevel(1,"+ row.customerId+","+row.cycleId+");\">Duyệt</a>";
			        		  return str;
			        	  }
			        	  return '';
			          }},
			          {field:'deny',title:'Từ chối', width: 30,sortable:false,resizable:false, align: 'center', formatter: function(value, row, index) {
			        	  if (row.status==2 && row.rewardType==null)
			        	  {
			        		  var str = "<a class = 'vt-grid-customer-deny' title='Từ chối' href='javascript:void(0)' onclick=\"return KeyShop.ProcessCustomerLevel(3,"+ row.customerId+","+row.cycleId+");\">Từ chối</a>";
			        		  return str;
			        	  }
			        	  return '';
			          }},
			          {field: 'ksRegistedHis', title: 'Lịch sử', width: 30, sortable: false, resizable: false, align: 'center', formatter: function(value, row, index) {
			        		return '<a onclick="return KeyShop.showKSRegistedHis('+ ksId +','+ row.ksLevelId +','+ row.customerId +','+ row.cycleId +',\'' +Utils.XSSEncode(row.customerCode)+'\',\'' +Utils.XSSEncode(row.customerName)+'\');" href="javascript:void(0)" title="Lịch sử cập nhật"><img height="17" src="/resources/images/icon-view.png"></a>';
			          }},
			          {field:'edit',title:title, width: 40,sortable:false,resizable:false, align: 'center', formatter: function(value, row, index) {			        	  
			        	  if (row.rewardType==null)
			        	  {
				        	  var str = "<a title='Sửa' href='javascript:void(0)' onclick=\"return KeyShop.openPopupEditKSCustomer("+row.ksCustomerId+");\"><img height='17' src='/resources/images/icon-edit.png'></a>";
				        	  str += "&nbsp&nbsp";
				        	  str += "<a title='Xóa' href='javascript:void(0)' onclick=\"return KeyShop.ProcessCustomerLevel(-1,"+ row.customerId+","+row.cycleId+");\"><img height='17' src='/resources/images/icon-delete.png'></a>";
				        	  return str;
			        	  }
			        	  return '';			        		  
			          }},
			]],
		  	onLoadSuccess :function(data){	    	
		    	$('.datagrid-header-rownumber').html('STT');
		    	KeyShop._mapKSCustomerSearch = new Map();
		    	if (data != undefined && data != null && data.rows.length > 0) {
		    		for (var i = 0 ; i < data.rows.length ; i++) {
			    		KeyShop._mapKSCustomerSearch.put(data.rows[i].ksCustomerId, data.rows[i]);
			    	}
		    		enable('btnExportCustomer');
		    	} else {
		    		disabled('btnExportCustomer');
		    	}
		    	//Xu ly duyet tu choi
		    	var arrApprove =  $('#dgGridContainerCustomer td[field="approve"]');
				if (arrApprove != undefined && arrApprove != null && arrApprove.length > 0) {
				  for (var i = 0, size = arrApprove.length; i < size; i++) {
				    $(arrApprove[i]).prop("id", "group_dt_kh_d_tc_gr_kh_cl_ap" + i);//Khai bao id danh cho phan quyen
					$(arrApprove[i]).addClass("cmsiscontrol");
				  }
				}
				//Xu ly duyet tu choi
		    	var arrDeny =  $('#dgGridContainerCustomer td[field="deny"]');
				if (arrDeny != undefined && arrDeny != null && arrDeny.length > 0) {
				  for (var i = 0, size = arrDeny.length; i < size; i++) {
				    $(arrDeny[i]).prop("id", "group_dt_kh_d_tc_gr_kh_cl_dn" + i);//Khai bao id danh cho phan quyen
					$(arrDeny[i]).addClass("cmsiscontrol");
				  }
				}
				
		    	//Xu ly cho edit
		    	var arrDelete =  $('#dgGridContainerCustomer td[field="edit"]');
				if (arrDelete != undefined && arrDelete != null && arrDelete.length > 0) {
				  for (var i = 0, size = arrDelete.length; i < size; i++) {
				    $(arrDelete[i]).prop("id", "group_edit_ks_gr_kh_cl_edit_" + i);//Khai bao id danh cho phan quyen
					$(arrDelete[i]).addClass("cmsiscontrol");
				  }
				}
				Utils.functionAccessFillControl('dgGridContainerCustomer', function(data){
					//Xu ly cac su kien lien quan den cac control phan quyen
					var arrTmpLength =  $('#dgGridContainerCustomer td[id^="group_dt_kh_d_tc_gr_kh_cl_"]').length;
					var invisibleLenght = $('.isCMSInvisible[id^="group_dt_kh_d_tc_gr_kh_cl_"]').length;
					if (arrTmpLength > 0 && arrTmpLength != invisibleLenght) {
						$('#gridCustomer').datagrid("showColumn", "approve");
						$('#gridCustomer').datagrid("showColumn", "deny");
					} else {
						$('#gridCustomer').datagrid("hideColumn", "approve");
						$('#gridCustomer').datagrid("hideColumn", "deny");
					}
					
					arrTmpLength =  $('#dgGridContainerCustomer td[id^="group_edit_ks_gr_kh_cl_edit_"]').length;
					invisibleLenght = $('.isCMSInvisible[id^="group_edit_ks_gr_kh_cl_edit_"]').length;
					if (arrTmpLength > 0 && arrTmpLength != invisibleLenght) {
						$('#gridCustomer').datagrid("showColumn", "edit");
					} else {
						$('#gridCustomer').datagrid("hideColumn", "edit");
					}
				});
				
				if ($('#dgGridContainerCustomer .vt-grid-customer-approve').length > 0) {
					$('#gridCustomer').datagrid("showColumn", "approve");
				} else {
					$('#gridCustomer').datagrid("hideColumn", "approve");
				}
				if ($('#dgGridContainerCustomer .vt-grid-customer-deny').length > 0) {
					$('#gridCustomer').datagrid("showColumn", "deny");
				} else {
					$('#gridCustomer').datagrid("hideColumn", "deny");
				}
		    },
		    method : 'GET'
		});
	},
	qtyChanged:function(selector){
		$('#errMsgKSCustomerLevel').html('').hide();		
		var dg_index = $(selector).attr('index');
		var rows = $('#gridCustomerLevel').datagrid('getRows')[dg_index];	
		var totalQuantity = 0;
		var totalAmount = 0;
		var amount = $('#amount'+dg_index).html().replace(/,/g, '').trim();
		var quantity = $('#quantity'+dg_index).html().replace(/,/g, '').trim();
		if(rows!=null){								
			var multi = $(selector).val().replace(/,/g, '').trim();
			if(multi != ''){
				if(isNaN(multi)){				
					setTimeout(function(){
						$(selector).focus();
					}, 20);
					$('#errMsgKSCustomerLevel').html('Bội số không đúng định dạng').show();
					$(selector).val('');
					return false;
				}
				totalQuantity = multi * quantity;
				totalAmount = multi * amount;				
				$('#totalAmount'+dg_index).html(formatCurrency(totalAmount));
				$('#totalQuantity'+dg_index).html(formatCurrency(totalQuantity));
			}
		}
		    	
	},
	loadGridCustomerLevel:function(ksId) {
		$('#dgGridContainerCustomerLevel').html('<table id="gridCustomerLevel"></table>');
		$('#gridCustomerLevel').datagrid({
			url: KeyShop.getUrlSearchKSCustomerLevel(),
			width : $('#dgGridContainerCustomerLevel').width() - 17,
			height: 'auto',
			singleSelect: true,
			rownumbers: true,
			fitColumns: true,
			scrollbarSize: 0,
			pagination: true,
			pageList  : [10,20,30],
			columns:[[  
			          {field:'ksLevelCode',title:'Mã mức',width:70, sortable : true, resizable : false, align: 'left', formatter:function(value,row,index){
			        	  return Utils.XSSEncode(value);
			          }},
			          {field:'ksLevelName',title:'Tên mức',width:120, sortable : true, resizable : false, align: 'left', formatter:function(value,row,index){
			        	  return Utils.XSSEncode(value);
			          }},
			          {field:'amount',title:'Số tiền',width:100, sortable : true, resizable : false, align: 'right', formatter:function(value,row,index){
			        	    return '<span id="amount'+index+'">'+Utils.XSSEncode(formatCurrency(value))+'</span>';
			          }},
			          {field:'quantity',title:'Số lượng',width:100, sortable : true, resizable : false, align: 'right', formatter:function(value,row,index){
			        	    return '<span id="quantity'+index+'">'+Utils.XSSEncode(formatCurrency(value))+'</span>';
			          }},
			          {field:'multiplier', title: 'Bội số ĐK', width:80, sortable:false, resizable:false ,align: 'right',formatter:function(value,row,index){
					    	if (row.multiplier == null) value = ""; 
					    	return '<input maxlength="8" index="'+index+'" style="text-align: right; width: 60px; margin: 0;" id="'+row.ksLevelId+'" type="text" class="InputTextStyle InputText1Style skuItem" value="'+formatCurrency(value)+'" onkeypress="NextAndPrevTextField(event,this,\'skuItem\')" onblur="return KeyShop.qtyChanged(this);" />';;
					  }},
					  {field:'totalAmount', title: 'Tổng tiền', width: 120, align: 'right', sortable:false,resizable:false ,align: 'right', formatter:function(value,row,index){
						   	 if(row.multiplier==null){
						   		return '<span id="totalAmount'+index+'">0</span>';
							 }
						   	 else return '<span id="totalAmount'+index+'">' + formatCurrency(row.multiplier*row.amount) + '</span>';
					    	 
					  }},
					  {field:'totalQuantity', title: 'Tổng số lượng', width: 120, align: 'right', sortable:false,resizable:false ,align: 'right', formatter:function(value,row,index){
						   	 if(row.multiplier==null){
						   		return '<span id="totalQuantity'+index+'">0</span>';
							 }
						   	 else return '<span id="totalQuantity'+index+'">' + formatCurrency(row.multiplier*row.quantity) + '</span>';
					  }},
			]],
		  	onLoadSuccess :function(data){	    	
		    	$('.datagrid-header-rownumber').html('STT');
		    	KeyShop._mapKSCustomerLevelSearch = new Map();
		    	for (var i = 0 ; i < data.rows.length ; i++) {
		    		KeyShop._mapKSCustomerLevelSearch.put(data.rows[i].ksLevelId, data.rows[i]);
		    		// vuongmq; 25/08/2015; bind cho nhap so nguyen duong
		    		if (data.rows[i] != undefined && data.rows[i] != null && data.rows[i].ksLevelId != undefined && data.rows[i].ksLevelId != null) {
		    			Utils.bindFormatOntextfieldCurrencyFor(data.rows[i].ksLevelId, Utils._TF_NUMBER);	
		    		}
		    	}
		    },
		    method : 'GET'
		});
	},
	openPopupKSProduct:function(){
		var ksId = $('#ksIdCurrent').val();
		var params = new Array();
		params.push({name:'ksId', value:ksId});
		CommonSearchEasyUI.searchProductDialog(function(data){
			if (data != null && data.length > 0) {
				KeyShop.addKSProduct(data);
			}
		}, params, '/key-shop/searchProduct');
	},
	deleteProduct:function(productId) {
		var dataModel = new Object();
		dataModel.ksId = $('#ksIdCurrent').val();
		dataModel.productId = productId;
		Utils.addOrSaveData(dataModel, "/key-shop/deleteKSProduct", null, 'errMsgKeyShop', function(data) {
			KeyShop.searchKSProduct();
			setTimeout(function() {
				$('.easyui-dialog').dialog('close');
			}, 2000);
		},null, null, true);
	},
	addKSProduct:function(data){
		var lstProductId = new Array();
		for (var i = 0 ; i < data.length ; i++) {
			lstProductId.push(data[i].productId);
		}
		var dataModel = new Object();
		dataModel.lstProductId = lstProductId;
		dataModel.ksId = $("#ksIdCurrent").val();
		Utils.addOrSaveData(dataModel, "/key-shop/addKSProduct", null, 'errMsgKS', function(data) {
			KeyShop.searchKSProduct();
			setTimeout(function() {
				$('.easyui-dialog').dialog('close');
			}, 2000);
		});
	},
	fillCategoriesToMultiSelect: function(isCheck) {
		$.ajax({  
			url: '/key-shop/get-category?isOrderByCode=true',
			method: 'GET',
			success: function(rs) {
				if (rs.error != undefined && !rs.error) {
					var lstCategory = rs.rows;
					var html = '';
					if(lstCategory.length > 0){
						if(isCheck == 1)
							{
								html += '<option value="0" selected="selected">'+Utils.XSSEncode(jsp_common_status_all)+'</option>';
							}else{
								html += '<option value="0" >'+Utils.XSSEncode(jsp_common_status_all)+'</option>';
							}
						for(var i=0; i < lstCategory.length; i++){
							if(isCheck == 1){
							html += '<option selected="selected" value="'+ lstCategory[i].id +'">' +
								Utils.XSSEncode(lstCategory[i].productInfoCode) + ' - ' +
								Utils.XSSEncode(lstCategory[i].productInfoName) + '</option>';
							}else{	
								html += '<option  value="'+ lstCategory[i].id +'">' +
									Utils.XSSEncode(lstCategory[i].productInfoCode) + ' - ' +
									Utils.XSSEncode(lstCategory[i].productInfoName) + '</option>';									
							}
						}
						$('#category').html(html);
						$('#category').change();
						$('#ddcl-category').remove();
						$('#ddcl-category-ddw').remove();
						$('#category').dropdownchecklist({
							emptyText: catalog_product_chon_nganh_hang,
							firstItemChecksAll: true,
							maxDropHeight: KeyShop._widthProductCat
						});
					} else {
						$('#category').html('');
						//$('#category').attr('disabled','disabled');
						$('#category').dropdownchecklist({
							emptyText:'-------------------',
							firstItemChecksAll: true,
							maxDropHeight: KeyShop._widthProductCat
						});
					}
					$('.ui-dropdownchecklist-text').css({"padding-left":"5px"});
				}
			}
		});
	},
	fillCategoryChildToMultiSelect: function(isCheck) {
		var param = '';
		var categories = $('#category').val();
		if (categories != null && categories.length > 0) {
			for (var i = 0; i < categories.length; i++) {
				param += '&lstProductInfoId=' + categories[i];
			}
		}
		$.ajax({
			url: '/key-shop/get-category-child?status=2'+param,
			method: 'GET',
			success: function(rs) {
				if (rs.error != undefined && !rs.error) {
					var lstCategory = rs.rows;
					//$('#categoryChild').removeAttr('disabled');
					var html = '';
					if(lstCategory.length > 0){
						if(isCheck == 1)
							{
								html += '<option value="0" selected="selected">'+jsp_common_status_all+'</option>';
							}else{
								html += '<option value="0" >'+jsp_common_status_all+'</option>';
							}
						for(var i=0; i < lstCategory.length; i++){
							if(isCheck == 1){
								html += '<option selected="selected" value="'+ lstCategory[i].id +'">' +
									Utils.XSSEncode(lstCategory[i].productInfoCode) + ' - ' +
									Utils.XSSEncode(lstCategory[i].productInfoName) + '</option>';
							}else{	
								html += '<option  value="'+ lstCategory[i].id +'">' +
									Utils.XSSEncode(lstCategory[i].productInfoCode) + ' - ' +
									Utils.XSSEncode(lstCategory[i].productInfoName) + '</option>';									
							}
						}
						$('#categoryChild').html(html);
						$('#categoryChild').change();
						$('#ddcl-categoryChild').remove();
						$('#ddcl-categoryChild-ddw').remove();
						$('#categoryChild').dropdownchecklist({
							emptyText: catalog_product_chon_nganh_hang,
							firstItemChecksAll: true,
							maxDropHeight: KeyShop._widthProductCat
						});
					} else {
						$('#categoryChild').html('');
						$('#ddcl-categoryChild').remove();
						$('#ddcl-categoryChild-ddw').remove();
						//$('#category').attr('disabled','disabled');
						$('#categoryChild').dropdownchecklist({
							emptyText:'-------------------',
							firstItemChecksAll: true,
							maxDropHeight: KeyShop._widthProductCat
						});
					}
					$('.ui-dropdownchecklist-text').css({"padding-left":"5px"});
				}
			}
		});
	},
	openImportExcelType:function(){
		$('#popupImport').dialog({
			title : 'Chọn loại import',
			width:250,
			height:'auto',
			onOpen: function(){
				
			}
		});
		$('#popupImport').dialog('open');
	},
	initImport:function() {
		$('#downloadTemplate').attr('href', 'javascript:void(0);');
		Utils._errExcelMsg = 'errExcelMsg';
		var options = {
			beforeSubmit : Utils.beforeImportExcel,
			success : Utils.afterImportExcelNew,
			type : "POST",
			dataType : 'html',
			data : { excelType : $('#excelType').val() }
		};
		$('#importFrm').ajaxForm(options);
		$('#excelFile').val('');
		$('#fakefilepc').val('');
	},
	importExcel : function() {
		var excelType = $('input:radio[name=importRadio]:checked').val();
		Utils._currentSearchCallback = function(){
			KeyShop.searchKS();
			var ksId = $('#ksIdCurrent').val();
			KeyShop.showShopGrid(ksId)
		};
		var options = {
			beforeSubmit : Utils.beforeImportExcel,
			success : Utils.afterImportExcelNew,
			type : "POST",
			dataType : 'html',
			data : { excelType : excelType }
		};
		$('#importFrm').ajaxForm(options);
		$('#importFrm').submit();
		$('#popupImport').dialog('close');
		return false;
	},
	excelTypeChanged:function() {
		$('#excelFile').val('');
		$('#fakefilepc').val('');
	},
	/**
	 * xuat excel danh sach keyshop
	 */
	exportExcelKS:function(){
		ReportUtils.exportReport("/key-shop/export-excel-ks", KeyShop._searchKSParam, "errExcelMsg");
	},
	/**
	 * xuat excel danh sach khach hang
	 */
	exportCustomer:function(){
		ReportUtils.exportReport("/key-shop/export-excel-customer", KeyShop._searchKSCustomerParam, "errMsgKeyShop");
	},
	/**
	 * xuat excel danh sach tra thuong
	 */
	exportReward:function(){
		ReportUtils.exportReport("/key-shop/export-excel-reward", KeyShop._searchKSRewardParam, "errMsgKeyShop");
	},
	exportExcelTemplate:function() {
		ReportUtils.exportReport('/key-shop/export-Template-Excel?excelType='+$('#excelType').val(), {}, 'errMsgKeyShop');
	},
	showShopGrid:function(ksId) {
		KeyShop.hideAllDiv();
		var ks = KeyShop._mapKSSearch.get(ksId);
		if (ks == null) return;
		$('#shopDiv').show();
		$('#shopTitle').html('ĐƠN VỊ PHÂN BỔ CỦA ' + Utils.XSSEncode(ks.ksName));
		$('#ksIdCurrent').val(ksId);
		KeyShop.loadShopGrid();
	},
	///@author HungLM16; @since JULY 21,2014; @description Mo node dau tien va con chau cua no
	expandNodeForLstChirentByTreeShop : function(tree, node) {
		if (node != undefined && node != null) {
			$(tree).treegrid('expand', node.id);
			var nodeEx = node.children;
			if (nodeEx != undefined && nodeEx != null) {
				KeyShop.expandNodeForLstChirentByTreeShop(tree, nodeEx[0]);
			}
		} else {
			return;
		}
	},
	///@author hunglm16; @since: August 18,2014; @description uncheckParent and uncheck Childrent
	uncheckFullParentToNode : function(node, id) {
		if (node != undefined && node != null) {
			var indexArr = KeyShop._arrShopInOrgAccessCheked.indexOf(node.id);
			if (indexArr > -1) {
				KeyShop._arrShopInOrgAccessCheked.splice(indexArr, true);
			}
			var cbx = $('#cbx_DgTreeShopDlCp_' + node.id);
			if (node.id != id) {
				$(cbx).prop('checked', false);
			}
			$('#checkShop_' + node.id.toString().trim()).prop('checked', false);
			if (node.attributes.shop.parentShop != undefined
					&& node.attributes.shop.parentShop != null) {
				var parNode = $('#treeGridShop').treegrid('find', node.attributes.shop.parentShop.id);
				if (parNode != undefined && parNode != null) {
					KeyShop.uncheckFullParentToNode(parNode);
				} else {
					return;
				}
			}
		} else {
			return;
		}
	},
	///@author HungLM16; @since JULY 25,2014; @description Mo node dau tien va con chau cua no
	uncheckNodeGroupAll: function(node, id){
		if (node != undefined && node != null) {
			var indexArr = KeyShop._arrShopInOrgAccessCheked.indexOf(node.id);
			if (indexArr > -1) {
				KeyShop._arrShopInOrgAccessCheked.splice(indexArr, true);
	    	}
			var cbx = $('#cbx_DgTreeShopDlCp_'+ node.id);
			if(node.id!=id){
				$(cbx).attr('checked', false);
			}
			$('#checkShop_' + node.id.toString().trim()).prop('checked', false);
			var arrChidren = node.children;
			if (arrChidren != undefined && arrChidren!=null && arrChidren.length >0){
				for(var i = 0; i < arrChidren.length; i++){
					KeyShop.uncheckNodeGroupAll(arrChidren[i]);
				}
			}
		}
	},
	///@author hunglm16; @since: August 20,2014; @description bat su kien tay doi tren checkbox
	onchangeCbxDgTreeShopOgrAccess:function(cbx){
		var node = $('#treeGridShop').treegrid('find', Number($(cbx).prop("id").replace(/cbx_DgTreeShopDlCp_/g,'').trim()));
		var indexArr = KeyShop._arrShopInOrgAccessCheked.indexOf(node.id);
		if($(cbx).prop("checked")){
			KeyShop.uncheckFullParentToNode(node, node.id);
			KeyShop.uncheckNodeGroupAll(node, node.id);
			if (indexArr < 0) {
				KeyShop._arrShopInOrgAccessCheked.push(node.id);
	    	}
		}else{
			if (indexArr > -1) {
				KeyShop._arrShopInOrgAccessCheked.splice(indexArr, true);
	    	}
			KeyShop.uncheckFullParentToNode(node, node.id);
		}
	},
	///@author HungLM16; @since JULY 17,2014; @description Bat cac su kien sau khi chon tren Tree Grid Shop
	updateChangeByCheckInTreeGridShop: function(){
		$('.ErrorMsgStyle').html('').hide();
		var msg = '';
		var arrDetele = [];//Danh sach xoa
		var arrInsert = [];//Danh sach them moi
		var isAll = false;
		var flag = false;
		var params = {};
		
		var dem = 0;
		if (KeyShop._arrShopInOrgAccess == null) {
			KeyShop._arrShopInOrgAccess = [];
		}
		if (KeyShop._arrShopInOrgAccessCheked == null) { 
			KeyShop._arrShopInOrgAccess = [];
		}
		//Xu ly cho so suat
		var mapShop = new Map();
		$('#shopDiv .vinput-money').each(function() {
			  var quantity = VTUtilJS.returnMoneyValue($(this).val());
			  if (quantity != undefined && quantity != null) {
					if (isNaN(quantity) || (quantity.length > 0 && Number(quantity) <= 0)) {
						var mgsEror = 'Đơn vị ' + $($(this).parent().parent().parent().children()[0]).text();
						msg = 'Số suất là kiểu số nguyên lớn hơn 0. ' + mgsEror.trim() + ' có số suất không hợp lệ';
						$(this).focus();
					} else if (Number(quantity) > 0) {
						var nodeId = Number($(this).prop('id').replace(/txtQuantity-ip-/g, ''));
						if (KeyShop._arrShopInOrgAccessCheked != undefined && KeyShop._arrShopInOrgAccessCheked != null && KeyShop._arrShopInOrgAccessCheked.indexOf(nodeId) > -1) {
							//Neu nhu NPP duoc check
							mapShop.put(nodeId, Number(quantity));
						} else {
							//Nguoc lai, neu nhu cac cap cha duoc check
							var node = $('#treeGridShop').treegrid('find', nodeId);
							if (KeyShop.checkParentCheckSelectTrue(node)) {
								mapShop.put(nodeId, Number(quantity));							
							}							
						}
					}
			  }
		});
		if(msg.length > 0){
			$('#errMsgKeyShop').text(msg).show();
			return false;
		}
		for (var i = 0, sizei = KeyShop._arrShopInOrgAccessCheked.length; i < sizei; i++) {
			flag = false;
			for (var j = 0, sizej = KeyShop._arrShopInOrgAccess.length; j < sizej; j++) {
				if (KeyShop._arrShopInOrgAccessCheked[i] == KeyShop._arrShopInOrgAccess[j]) {
					dem++;
					flag = true;
					break;
				}
			}
			if (!flag) {
				arrInsert.push(KeyShop._arrShopInOrgAccessCheked[i]);
			}
		}
		flag = false;
		params.flagChange = StatusType.ACTIVE;
		if (dem == KeyShop._arrShopInOrgAccess.length && dem == KeyShop._arrShopInOrgAccessCheked.length){
			params.flagChange = StatusType.INACTIVE;
		} else if (KeyShop._arrShopInOrgAccessCheked.length == 0) {
			arrDetele = KeyShop._arrShopInOrgAccess;
			params.shopId = $('#shopId').val().trim();
			params.ksId = $('#ksIdCurrent').val().trim();
			flag = true;
		} else {
			for(var i = 0; i< KeyShop._arrShopInOrgAccess.length; i++){
				flag = false;
				for(var j=0; j < KeyShop._arrShopInOrgAccessCheked.length; j++){
					if (KeyShop._arrShopInOrgAccessCheked[j] == KeyShop._arrShopInOrgAccess[i]) {
						flag = true;
						break;
					}
				}
				if (!flag) {
					arrDetele.push(KeyShop._arrShopInOrgAccess[i]);
				}
			}
		}
		
		if (mapShop != null && mapShop.size() > 0) {
			var arrShopTarget = [];
			var keyArray = mapShop.keyArray;
			for (var i = 0, size = keyArray.length; i < size; i++) {
				arrShopTarget.push(keyArray[i] + ';' + mapShop.get(keyArray[i]));
			}
			params.arrOjectText = arrShopTarget.toString();
		}
		params.lstLong = arrInsert;//Them moi
		params.arrLong = arrDetele;//Xoa
		params.ksId = $('#ksIdCurrent').val().trim();
		Utils.addOrSaveData(params, '/key-shop/update-shop-tree', null,  'errMsgKeyShop', function(data) {
			$('#successMsgKeyShop').html("Lưu dữ liệu thành công").show();
			KeyShop.loadShopGrid();
			var tm = setTimeout(function(){
				//Load lai danh sach quyen
				//$("input[name='checkRightShop']").attr('checked', false);
				$('.SuccessMsgStyle').html("").hide();
				clearTimeout(tm);
			 }, 1500);
		}, null, null, null,msg, function(data){
			if(data.errorType!=undefined && data.errorType!=null && data.errorType == 1){
				//$('#shopCode').focus();
			}
		});
		return false;
	},
	/**
	 * Ham open dialog search khach hang
	 * @param url
	 * @param callback
	 */
	openCustomerOnDialog:function(url,callback){		
		$('.easyui-dialog #btnSearchStyle1').unbind('click');
		$('#searchStyle1EasyUIDialogDiv').show();
		var html = $('#searchStyle1EasyUIDialogDiv').html();		
		$('#searchStyle1EasyUIDialog').dialog({  
	        title: 'Thông tin tìm kiếm',  
	        closed: false,  
	        cache: false,  
	        modal: true,
	        width:750,
	        onOpen: function(){		        	
	        	var tabindex = -1;
	        	$('.InputTextStyle, select, input, button , li , tr, td, th, label, ul, a, img').each(function () {
					if (this.type != 'hidden') {
						$(this).attr("tabindex", tabindex);
						tabindex -=1;
					}
				});	        		        	
	        	$('.easyui-dialog #seachStyle1Code').focus();
				$('.easyui-dialog #seachStyle1AddressLabel').show();
				$('.easyui-dialog #seachStyle1Address').show();
				$('.easyui-dialog #searchStyle1Grid').show();
				$('.easyui-dialog #btnSearchStyle1').css('margin-left', 270);
				$('.easyui-dialog #btnSearchStyle1').css('margin-top', 5);
				$('.easyui-dialog #seachStyle1CodeLabel').html('Mã KH');
				$('.easyui-dialog #seachStyle1NameLabel').html('Tên KH');
				
				Utils.bindAutoSearch();
				CommonSearch._currentSearchCallback = callback;
				$('.easyui-dialog #searchStyle1Grid').datagrid({
					url : url,
					autoRowHeight : true,
					rownumbers : true, 
					checkOnSelect :true,
					pagination:true,
					rowNum : 10,
					pageSize:10,
					scrollbarSize : 0,
//					fitColumns:true,
					queryParams:{
						page:1
					},
					width : 720,
				    columns:[[
				        {field:'shopCode',title:'Mã NPP',align:'left', width:100, sortable : false,resizable : false},
				        {field:'shortCode',title:'Mã KH',align:'left', width:100, sortable : false,resizable : false},  
				        {field:'customerName',title:'Tên KH',align:'left', width:150, sortable : false,resizable : false},				        
				        {field:'address',title:'Địa chỉ',align:'left', width:250, sortable : false,resizable : false},
				        {field:'select', title:'Chọn', width:68, align:'center',sortable : false,resizable : false,formatter : 
				        	function(value, rowData, rowIndex) {
							return "<a href='javascript:void(0)' onclick=\"return KeyShop.getResultSeachStyle1EasyUIDialog("+ rowIndex + ");\">Chọn</a>";
				         }},
				        {field :'id',hidden : true},
				    ]],
				    onLoadSuccess :function(data){
				    	 tabindex = 1;
			    		 $('.easyui-dialog input,.easyui-dialog select,.easyui-dialog button').each(function () {
				    		 if (this.type != 'hidden') {
					    	     $(this).attr("tabindex", '');
								 tabindex++;
				    		 }
						 });
			    		 $('.datagrid-header-rownumber').html('STT');	
			    		 updateRownumWidthForJqGrid('.easyui-dialog');
//			    		 $(window).resize();
			    		 setTimeout(function() {
			    			 CommonSearchEasyUI.fitEasyDialog('searchStyle1EasyUIDialog');
			    		 }, 720);
			    		 var code = $('.easyui-dialog #seachStyle1Code').val().trim();
				    }
				});
				$('.easyui-dialog #btnSearchStyle1').bind('click',function(event) {
					if(!$(this).is(':hidden')){
						var code = $('.easyui-dialog #seachStyle1Code').val().trim();
						var name = $('.easyui-dialog #seachStyle1Name').val().trim();
						var address = $('.easyui-dialog #seachStyle1Address').val().trim();
						$('.easyui-dialog #searchStyle1Grid').datagrid('load',{code :code, name: name,address:address});
						var records = $('.easyui-dialog #searchStyle1Grid').datagrid('getRows').length;
						$('.easyui-dialog #seachStyle1Code').focus();
					}					
				});
				$('.easyui-dialog #btnClose').bind('click',function(event) {
					$('#searchStyle1EasyUIDialogDiv').css("visibility", "hidden");					
					$('.easyui-dialog').dialog('close');
				});
	        },
	        onBeforeClose: function() {
	        	var curIdFocus = $('#cur_focus').val();
				$('#'+ curIdFocus).focus();				
//				$('.easyui-dialog #searchStyle1Grid').datagrid('reload',{	
//					pageSize: 10
//				});
	        },
	        onClose : function(){
	        	$('#searchStyle1EasyUIDialogDiv').html(html);
	        	$('#searchStyle1EasyUIDialogDiv').hide();
	        	$('#searchStyle1ContainerGrid').html('<table id="searchStyle1Grid" class="easyui-datagrid" style="width: 520px;"></table><div id="searchStyle1Pager"></div>');
	        	$('.easyui-dialog #seachStyle1Code').val('');
	        	$('.easyui-dialog #seachStyle1Name').val('');
	        	$('.easyui-dialog #seachStyle1Address').val('');	        	
	        }
		});
	},
	/**
	 * Ham tra ve ket qua khi chon khach hang tren datagrid
	 * @param rowIndex
	 * @returns {Boolean}
	 */
	getResultSeachStyle1EasyUIDialog : function(rowIndex) {
		var row = $('.easyui-dialog #searchStyle1Grid').datagrid('getRows')[rowIndex];			
		var obj = {};
		obj.code = row.shortCode;
		obj.name = row.customerName;
		obj.address = row.address;
		obj.id = row.id;
		obj.rewardType= row.rewardType;
		$('#addressPop').html(row.address);
		$('#customerId').val(row.id);
		
		var ksId = $('#ksIdCurrent').val().trim();
		KeyShop.loadGridCustomerLevel(ksId);
		
		if (CommonSearch._currentSearchCallback != null) {
			$('#searchStyle1EasyUIDialog.easyui-dialog').dialog('close');
			CommonSearch._currentSearchCallback.call(this, obj);
		}	
		return false;
	},
	
	UpdateCustomerLevel:function(){
		var msg = '';
		$('#errMsgKSCustomerLevel').html('').hide();
		$('#successMsgKSCustomerLevel').html('').hide()
		if ($('#numPopup').combobox('getValue') == '')
		{
			msg = "Chưa chọn chu kỳ";
			$('#errMsgKSCustomerLevel').html(msg).show();
			return false;
		}
		if ($('#customerId').val() == '')
		{
			msg = "Chưa chọn khách hàng";
			$('#errMsgKSCustomerLevel').html(msg).show();
			return false;
		}
		var numPopup=0;
		if ($('#numPopup').combobox('getValue')<10) numPopup='0'+$('#numPopup').combobox('getValue');
		else numPopup=$('#numPopup').combobox('getValue');
		var cycleIdPopup = $('#yearPopup').val() +''+ numPopup+''+numPopup;
		var listKsLevelId = new Array();
		var listMultiplier = new Array();
		var customerId = $('#customerId').val();
		var ksId = $('#ksIdCurrent').val().trim();
		var totalRow = 0;
		$('.skuItem').each(function(){
			var index = $(this).attr('index');
			//if($(this).val().trim().length>0){
				var rows = $('#gridCustomerLevel').datagrid('getRows')[index];
				if(rows!=null){
					
					var ksLevelIdRow = rows.ksLevelId;
					var multiRow =$(this).val().trim().replace(/,/g,'');
					
					if(isNaN(multiRow)){
						$(this).focus();
						msg = "Bội số không đúng định dạng";
						return false;
					}
					
					if (multiRow!=null && (parseInt(multiRow) >= 0 || isNullOrEmpty(multiRow)))
					{
						if(!isNullOrEmpty(multiRow)) {
							totalRow = totalRow + parseInt(multiRow);	
						}
						
						listKsLevelId.push(ksLevelIdRow);
						listMultiplier.push(multiRow);
					}
					
				}				
			//}
		});	
		
		if(listKsLevelId.length==0 || totalRow == 0){
			$('#errMsgKSCustomerLevel').html("Chưa chọn bội số cho mức").show();
			return false;
		}
		var data = new Object();
		data.cycleId = cycleIdPopup;
		data.customerId = customerId;
		data.ksId = ksId;
		data.listKsLevelId = listKsLevelId;
		data.listMultiplier = listMultiplier;
		Utils.addOrSaveData(data, '/key-shop/update-customer-level', null,  'errMsgKSCustomerLevel', function(data) {
			$('#successMsgKSCustomerLevel').html("Lưu dữ liệu thành công").show();
			KeyShop.loadGridCustomerLevel(ksId);
			KeyShop.loadGridCustomer(ksId);
			var tm = setTimeout(function() {				
				$('#successMsgKSCustomerLevel').html('').hide();
				$('#addOrEditKSCustomer').dialog('close');
				clearTimeout(tm);
			 }, 1500);
		}, null, null, null, msg, function(data) {
			if (data.errorType!=undefined && data.errorType!=null && data.errorType == 1) {
				//$('#shopCode').focus();
			}
		});
		return false;
	},
	ApproveCustomerLevel:function(processType){
		var customerId = $('#customerId').val();
		var numPopup=0;
		if ($('#numPopup').combobox('getValue')<10) numPopup='0'+$('#numPopup').combobox('getValue');
		else numPopup=$('#numPopup').combobox('getValue');
		var cycleIdPopup = $('#yearPopup').val() +''+ numPopup+''+numPopup;
		KeyShop.ProcessCustomerLevel(processType,customerId,cycleIdPopup);
	},
	ProcessCustomerLevel:function(processType, customerId, cycleId){
		var msg = '';
		if (processType==-1)
		{
			$('#errMsgKeyShop').html('').hide();
			$('#successMsgKeyShop').html('').hide();
		}
		$('#errMsgKSCustomerLevel').html('').hide();
		$('#successMsgKSCustomerLevel').html('').hide()
		
		
		var listKsLevelId = new Array();
		var listMultiplier = new Array();
		//var customerId = $('#customerId').val();
		var ksId = $('#ksIdCurrent').val().trim();
		
		var data = new Object();
		data.cycleId = cycleId;
		data.customerId = customerId;
		data.ksId = ksId;
		data.processType = processType;
		Utils.addOrSaveData(data, '/key-shop/process-customer-level', null,  'errMsgKeyShop', function(data) {
			$('#successMsgKeyShop').html("Lưu dữ liệu thành công").show();
			var tm = setTimeout(function(){
				KeyShop.loadGridCustomerLevel(ksId);
				KeyShop.loadGridCustomer(ksId);
				$('#successMsgKeyShop').html('').hide();
				clearTimeout(tm);
			 }, 1500);
		}, null, null, null,msg, function(data){
			if(data.errorType!=undefined && data.errorType!=null && data.errorType == 1){
				//$('#shopCode').focus();
			}
		});
		
//		$.messager.confirm('Xác nhận', 'Cập nhật thông tin mức khách hàng', function(r){
//			if (r){
//				
//			}
//		});		
		return false;
	},
	loadShopGrid : function(){
		$('#shopContainerGrid').html('<table id="treeGridShop" class="easyui-treegrid"></table>').change().show();
		var orgAccessTreeControlId = '#treeGridShop';
		var params = {};
		params.shopId = $('#shopId').val().trim();
		params.ksId = $('#ksIdCurrent').val();
		params.check = $('#checkAllTreeShop')[0].checked;
		KeyShop._arrShopInOrgAccess=[];
		KeyShop._arrShopInOrgAccessCheked=[];
		$(orgAccessTreeControlId).treegrid({  
		    url: '/key-shop/unit-access-tree', //generalcms/data-permission/org-access-tree
		    width: $('#shopContainerGrid').width(),
		    queryParams: params,
		    scrollbarSize: 0,
			fitColumns: true,
			checkOnSelect: false,
			selectOnCheck: false,
		    rownumbers: true,
	        idField: 'id',
	        treeField: 'shopName',
		    columns:[[	       
		        {field: 'shopName', title: 'Đơn vị', resizable: false, width: 500, formatter: function(value, row, index) {
		        	return Utils.XSSEncode(row.attributes.shop.shopName);
		        }},
		        {field: 'specificType', title: 'Số suất', resizable: true, width: 110, formatter: function(value, row, index) {
		        	if (row.attributes.shop.type != null && row.attributes.shop.type.specificType != undefined && row.attributes.shop.type.specificType != null && row.attributes.shop.type.specificType == 'NPP') {
		        		return '<input type="text" maxlength="9" class="vinput-money" style="text-align: right; width: 120px;" value="'+VTUtilJS.formatCurrency(row.attributes.shop.quantity)+'" id="txtQuantity-ip-'+row.id+'">';
		        	}
		        }},
		        {field: 'shopId', title: '', resizable: false, width: 30, fixed: true, formatter: function(value, row, index) {
		        	var html = '';
		        	if (row.attributes && row.attributes.hasOwnProperty('selected') && row.attributes.selected) {
		        		KeyShop._arrShopInOrgAccess.push(row.id);
		        		html = '<input type="checkbox" checked="checked" onchange="KeyShop.onchangeCbxDgTreeShopOgrAccess(this);" name="cbx_DgTreeShopDlCp_ShopId" id="cbx_DgTreeShopDlCp_'+row.id+'" value="true">';
		        	} else {
		        		html = '<input type="checkbox" onchange="KeyShop.onchangeCbxDgTreeShopOgrAccess(this);" name="cbx_DgTreeShopDlCp_ShopId" id="cbx_DgTreeShopDlCp_'+row.id+'" value="false">';
		        	}
		        	return html;
		        }}
//		        ,		        
//		        {field: 'editCheck', title: '', width: 30, align: 'center', resizable: false, fixed: true, formatter: function(value, row, index) {
//		        	var html = '';
//		        	if (row.children != undefined && row.children != null) {
//		        		html= html + '<input type ="checkbox" value="' + row.id + '" id="checkShop_' + row.attributes.id + '" onclick="return PermissionManagement.onchangeCheckBoxRightGridShop(this);" name = "checkRightShop">';
//					}
//		        	return html;
//		        }}
		    ]],
		    onLoadSuccess: function(n, data) {
		    	$(window).resize();
		    	$('.vinput-money').each(function() {
		    		$(this).width($(this).parent().width());
		    		VTUtilJS.bindFormatOnTextfield($(this), VTUtilJS._TF_NUMBER_COMMA);
		    		VTUtilJS.formatCurrencyFor($(this));
				});
		    	KeyShop._rowIndex = -1;
		    	KeyShop._arrShopInOrgAccessCheked = [];
		    	if (data != undefined && data != null && data.maxLevel != undefined && data.maxLevel > 0) {
		    		KeyShop._maxLevel = data.maxLevel;
		    	}
		    	var root = $('#treeGridShop').treegrid('getRoot');
		    	if (root != undefined && root != null) {
		    		//Check by Flag
		    		//Mo node dau tien va con chau cua no
		    		if ((params.shopId != null && params.shopId.trim().length > 0) || !$('#checkAllTreeShop')[0].checked) {
		    			//$(this).treegrid('expandAll');
		    		} 
		    		else {
				    	var nodeEx = root.children[0];
				    	KeyShop.expandNodeForLstChirentByTreeShop(this, nodeEx);
		    		}
		    	}
		    	if (KeyShop._arrShopInOrgAccess != null && KeyShop._arrShopInOrgAccess.length > 0) {
		    		for (var i = 0; i < KeyShop._arrShopInOrgAccess.length; i++) {
		    			if (KeyShop._arrShopInOrgAccessCheked.indexOf(KeyShop._arrShopInOrgAccess[i]) < 0) {
		    				KeyShop._arrShopInOrgAccessCheked.push(KeyShop._arrShopInOrgAccess[i]);
		    	    	}
		    		}
		    	}
		    }
		});
	},
	yearChange: function(year, num) {
 		var par = {};
 		par.year = $('#' + year).val();
 		Utils.getJSONDataByAjax(par, '/commons/list-cycle-filter?isChoiceAll=true', function(data){
 			if (data != null && data.lstCycle != null) {
 				Utils.bindPeriodCbx('#' + num, data.lstCycle, data.currentNum, 115, function() {
// 					KeyShop.validatePeriod();
 				}, 
 				function() {
 					KeyShop._isDoneLoadNum = true;
 				}, 'num', 'cycleName');
// 				KeyShop.validatePeriod();
 			}
 		});
 	},
 	yearChangePopup: function(year, num) {
 		var par = {};
 		par.year = $('#' + year).val();
 		Utils.getJSONDataByAjax(par, '/commons/list-cycle-filter?isChoiceAll=true', function(data){
 			if (data != null && data.lstCycle != null) {
 				var customerId = $('#ksCustomerId').val().trim();
 				var currentNum = 0;
 				if(!isNullOrEmpty(customerId) && KeyShop._mapKSCustomerSearch != null) {
 					var kc = KeyShop._mapKSCustomerSearch.get(customerId);
 					currentNum = Number(kc.cycleId.toString().substring(6, 8));
 				} else {
 					var currentNum = $('#numBottom').combobox('getValue').trim();
 	 				if (isNullOrEmpty(currentNum)) {
 	 					currentNum = null;
 	 				}	
 				}
 				
 				Utils.bindPeriodCbx('#' + num, data.lstCycle, currentNum, 115, function() {
 					KeyShop.validatePeriod();
 				}, 
 				function() {
 					KeyShop._isDoneLoadNum = true;
 				}, 'num', 'cycleName');
 				KeyShop.validatePeriod();
 			}
 		});
 	},
 	validatePeriod: function (){
 		if ($('#yearPopup').val() != null && $('#numPopup').combobox('getValue') != null) {
 			var curYearPeriod = Number($('#currentYear').val().trim());
 			var curNumPeriod = Number($('#currentCycleId').val().substring(7, 8));
 			var yearPeriodCreate = Number($('#yearPopup').val().trim());
 			var numPeriodCreate = Number($('#numPopup').combobox('getValue').trim());
 			
 			if(yearPeriodCreate < curYearPeriod || (yearPeriodCreate == curYearPeriod && numPeriodCreate < curNumPeriod)){
 				$('#group_dt_kh_d_tc_btnApproveCustomerLevel').hide();
 				$('#group_dt_kh_d_tc_btnDenyCustomerLevel').hide();
 				$('#group_edit_ks_btnUpdateCustomerLevel').hide();
 			}else{
 				$('#group_edit_ks_btnUpdateCustomerLevel').show();
 			}

 			var ksId = $('#ksIdCurrent').val().trim();
 			KeyShop.loadGridCustomerLevel(ksId);
 		}
 	},
 	
 	
 	/**
 	 * Kiem tra node cha duoc check
 	 * @param node thong ton don vi
 	 * @param id Khoa chinh Don vi
 	 * @author hunglm16
 	 * @since 18/11/2015
 	 */
 	checkParentCheckSelectTrue: function (node) {
 		if (node != undefined && node != null) {
			if (KeyShop._arrShopInOrgAccessCheked.indexOf(node.id) > -1) {
				//Kiem tra voi danh sach don vi duoc Check
				return true;
			}
			if (node.attributes.shop.parentShop != undefined && node.attributes.shop.parentShop != null) {
				//Xu ly voi cao hon
				var parNode = $('#treeGridShop').treegrid('find', node.attributes.shop.parentShop.id);
				if (parNode != undefined && parNode != null) {
					return KeyShop.checkParentCheckSelectTrue(parNode);
				}
				return false;
			}
		}
 		return false;
 	},
 	
 	/**
 	* Hien thi thong tin lich su cap nhat khach hang
 	* @author hunglm16
 	* @param 
 	* @since 23/11/2015
 	*/
 	showKSRegistedHis: function(ksId, ksLevelId, customerId, cycleId, customerCode, customerName) {
 		if (customerCode == undefined || customerCode == null) {
 			customerCode = '';
 		}
 		if (customerName == undefined || customerName == null) {
 			customerName = '';
 		}
 		var titleDialog = 'Thông tin lịch sử cập nhật ' + customerCode.trim() + ' - ' + customerName.trim();
 		VCommonJS.showDialogList({
		    url : '/key-shop/getKSRegistedHistory',
		    dialogInfo: {
		    	title: titleDialog.trim(),
		    	width: 780
		    },
		    params: {
		    	ksId: ksId
		    	//, ksLevelId: ksLevelId
		    	, customerId: customerId
		    	, cycleId: cycleId
		    },
		    columns : [[
		        {field: 'keyShopName', title: 'Chương trình', align: 'left', width: 150, sortable: false, resizable: false, formatter: function(value, row, index) {
					return Utils.XSSEncode(value);
				}},
		        {field: 'cycleName', title: 'Chu kỳ', align: 'left', width: 90, sortable: false, resizable: false, formatter: function(value, row, index) {
					return Utils.XSSEncode(value);
				}},
		        {field: 'createDateStr', title: 'Ngày giờ', align: 'center', width: 80, sortable: false, resizable: false, formatter: function(value, row, index) {
					return Utils.XSSEncode(value);
				}},
		        {field: 'userName', title: 'Người cập nhật', align: 'left', width: 110, sortable: false, resizable: false, formatter: function(value, row, index) {
					return Utils.XSSEncode(value);
				}},
		        {field: 'levelName', title: 'Mức', align: 'left', width: 100, sortable: false, resizable: false, formatter: function(value, row, index) {
					return Utils.XSSEncode(value);
				}},
		        {field: 'multiplyer', title: 'Bội số', align: 'right', width: 90, sortable: false, resizable: false, formatter: function(value, row, index) {
					if (value != undefined && value != null) {
						return VTUtilJS.returnMoneyValue(value);
					}
					return '';
				}}
		    ]]
		});
 	}
};
/*
 * END OF FILE - /web/web/resources/scripts/business/program/vnm.key-shop.js
 */

/*
 * JavaScript file created by Rockstarapps Concatenation
*/

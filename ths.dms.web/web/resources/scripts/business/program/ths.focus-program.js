var FocusProgram = {
	_xhrSave : null,
	_xhrDel: null,
	_mapShop:null,
	_mapProduct:null,
	_listShopId:null,
	_lstSaleTypeData: new Map(),
	_lstTypeData: new Map(),
	_firstCount:true,
	_mapLstProduct:new Map(),
	_searchShopParam: {id:'0',shopCode:'',shopName:''},
	_searchProdParam: {id:'0',code:'',name:''},
	_lstHTBHdb: [],
	hideAllTab: function(){
		$(".ErrorMsgStyle").hide();
		$('#tab1').removeClass('Active');
		$('#tab2').removeClass('Active');
		$('#tab3').removeClass('Active');
		$('#spCTTTTab').hide();
		$('#dvtgCTTTTab').hide();
		$('#htbhCTTTTab').hide();
	},
	showTab1: function(){
		if ($("#tab1.Active").length == 1) {
			return;
		}
		FocusProgram.hideAllTab();
		$('#tab1').addClass('Active');
		$('#htbhCTTTTab').show();
		$('#errExcelMsg').html('').hide();
		$('#idImportDiv').hide();
		$('#codeHTBH').focus();
		Utils.bindAutoButtonEx('.ContentSection','btnSearchSaleType');
		//$("#gridHTBH").datagrid('reload');
	},
	showTab3: function(){
		if ($("#tab3.Active").length == 1) {
			return;
		}
		FocusProgram.hideAllTab();
		$('#tab3').addClass('Active');
		$('#dvtgCTTTTab').show();
		$('#idImportDiv').show();
		$('#shopCode').focus();
		Utils.bindAutoButtonEx('.ContentSection','btnSearchShop');
		$('#downloadTemplate').attr( 'href', excel_template_path + 'catalog/Bieu_mau_danh_muc_NPPThamgiavaoCTTT_import.xls');
		$('#excelType').val(2);
		$('#errExcelMsg').html('').hide();
		var value = $('#divWidth').width()-50;
		var dataWidth = (value * 80)/100;
		var editWidth = (value * 10)/100;
		var deleteWidth = (value * 9)/100+6;
		var statusPermission = $('#statusPermission').val();
		var titleOpenDialogShop ="";
		if(statusPermission == 2){
			titleOpenDialogShop = '<a href="javascript:void(0);" onclick="return FocusProgram.openShopDialog();"><img src="/resources/images/icon-add.png"></a>';
		}
		FocusProgram._searchShopParam.id = $('#progId').val().trim();
		$('#exGrid').treegrid({
		    url:  FocusProgram.getShopGridUrl($('#progId').val(),'',''),
		     width:($('#divWidth').width()-50),
	        height:'auto',  
	        idField: 'id',  
	        treeField: 'data',
		    columns:[[  
		        {field:'data',title:'Đơn vị',resizable:false,width:dataWidth, formatter: function(value, row, index){
			    		return Utils.XSSEncode(value);
	            }},
		        {field:'edit',title:titleOpenDialogShop,width:editWidth,align:'center',resizable:false,formatter:function(value,row,index){
			        	if(row.isShop == 0){
			        		if(statusPermission == 2){
			        			return '<a title="Thêm mới" href="javascript:void(0);" onclick="return FocusProgram.openShopDialog(' +row.id+');"><img src="/resources/images/icon-add.png"></a>';
			        		}
			        	}
		        }},
		        {field:'delete',title:'',width:deleteWidth,align:'center',resizable:false,formatter:function(value,row,index){
		        		if(statusPermission == 2){
		        			return '<a title="Xóa" href="javascript:void(0);" onclick="return FocusProgram.deleteFocusShopMap(' +row.id+');"><img src="/resources/images/icon-delete.png"></a>';
		        		}
		        }}
		    ]] 
		});
	},
	importExcel:function(){
		$(".ErrorMsgStyle").hide();
 		$('#isView').val(0);
		$('#importFrm').ajaxForm(options);
 		$('#importFrm').submit();
 		return false;
 	},
 	afterImportExcelUpdate: function(responseText, statusText, xhr, $form){
		hideLoadingIcon();
		if (statusText == 'success') {				
	    	$("#responseDiv").html(responseText);		 
	    	var newToken = $('#responseDiv #newToken').val();
	    	console.log(newToken);
	    	if(newToken != null && newToken != undefined && newToken != '') {
	    		$('#token').val(newToken);	    		
	    		$('#tokenImport').val(newToken);
	    	}
	    	if($('#errorExcel').html().trim() == 'true' || $('#errorExcelMsg').html().trim().length > 0){
	    		$('#errExcelMsg').html($('#errorExcelMsg').html()).show();
	    	} else {
	    		var totalRow = parseInt($('#totalRow').html().trim());
    			var numFail = parseInt($('#numFail').html().trim());
    			var fileNameFail = $('#fileNameFail').html();
    			var mes = format(msgErr_result_import_excel,(totalRow - numFail),numFail);		    		
    			if(numFail > 0){
    				mes+= ' <a href="' + fileNameFail +'">Xem chi tiết lỗi</a>';
    			}
    			if($('#excelFile').length!=0 && $('#fakefilepc').length!=0){
    				try{$('#excelFile').val('');$('#fakefilepc').val('');}catch(err){}
    			}
    			$('#errExcelMsg').html(mes).show();
    			if ($("#tab2.Active").length == 1) {
    				$("#grid").datagrid("reload");
    			} else if ($("#tab3.Active").length == 1) {
    				$("#exGrid").treegrid("reload");
    			}
	    	}
	    }
	},
	gotoTab: function(tabIndex){
		FocusProgram.deactiveAllMainTab();
		switch (tabIndex) {
		case 0:
			$('#tabContent1').show();
			$('#tab1 a').addClass('Active');
			break;
		case 1:
			$('#tabContent2').show();
			$('#tab2 a').addClass('Active');
			AttributesManager.getAttributesForm('tabContent2',AttributesManager.FOCUS_PROGRAM);
			$('#typeAttribute').val(AttributesManager.FOCUS_PROGRAM);
			break;
		default:
			$('#tabContent1').show();
			break;
		}
	},
	upload : function(){ //upload trong fancybox
		var options = { 					 		
	 		beforeSubmit: ProductLevelCatalog.beforeImportExcel,   
	 		success:      ProductLevelCatalog.afterImportExcelUpdate, 
	 		type: "POST",
	 		dataType: 'html'
	 	}; 
		$('#importShopFrm').ajaxForm(options);
		$.messager.confirm('Xác nhận','Bạn có muốn nhập từ file?',function(r){
			if (r){						
				$('#importShopFrm').submit();						
			}
		});
	},
	deactiveAllMainTab: function(){
		$(".ErrorMsgStyle").hide();
		$('#tab1 a').removeClass('Active');
		$('#tab2 a').removeClass('Active');
		$('#tabContent1').hide();
		$('#tabContent2').hide();
	},
	getGridUrl: function(code,name,shopId,fDate,tDate,status){		
		return "/focus-program/search?code=" + encodeChar(code) + "&name=" + encodeChar(name) + "&shopId=" + shopId + "&fromDate=" + fDate + "&toDate=" + tDate + '&status=' + status;
	},	
	search: function(){
		$(".ErrorMsgStyle").hide();
		$('#errMsg').html('').hide();
		var code = $('#code').val().trim();
		var name = $('#name').val().trim();
		var shopId = $('#shopId').val().trim();
		var fromDate = $('#fDate').val();
		var toDate = $('#tDate').val();	
		var status = 1;
		if($('#permissionUser').val() == 'true'){
			status = $('#status').val();
		}
		var msg = '';
		if(msg.length == 0){
			msg = Utils.getMessageOfSpecialCharactersValidate('code','Mã CTTT',Utils._CODE);
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfSpecialCharactersValidate('name','Tên CTTT');
		}		 
		if(msg.length == 0){
			msg = Utils.getMessageOfInvalidFormatDate('fDate','Từ ngày');
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfInvalidFormatDate('tDate','Đến ngày');
		}
		var fDate = $('#fDate').val();
		var tDate = $('#tDate').val();
		if(msg.length == 0){
			if (fDate != '' && !Utils.isDate(fDate)) {
				msg = 'Từ ngày không hợp lệ, vui lòng nhập theo định dạng dd/MM/yyyy';
				$('#fDate').focus();
			}
		}
		if(msg.length == 0){
			if(tDate != '' && !Utils.isDate(tDate)){
				msg = 'Đến ngày không hợp lệ, vui lòng nhập theo định dạng dd/MM/yyyy';
				$('#tDate').focus();
			}
		}
		//var currentTime = new Date();
		//var month = currentTime.getMonth() + 1;
		//var day = currentTime.getDate();
		//var year = currentTime.getFullYear();
		if(msg.length == 0){
			if(fDate != '' && tDate != ''){
				if(!Utils.compareDate(fDate, tDate)){
					msg = 'Từ ngày phải là ngày trước hoặc cùng ngày với đến ngày';	
					$('#fDate').focus();
				}
			}
		}
		if(msg.length > 0){
			$('#errMsg').html(msg).show();
			return false;
		}
		setTimeout(function() {
			$('#code').focus();
		}, 500);
		var url = FocusProgram.getGridUrl(code,name,shopId,fromDate,toDate,status);
		$("#listCTTTGrid").datagrid({url:url,pageNumber:1});
		//$("#listCTTTGrid").datagrid('load',{page:1,code:code,name:name,shopId:shopId,fromDate:fromDate,toDate:toDate,status:status});
		return false;
	},
	saveInfo: function(){
		$(".ErrorMsgStyle").hide();
		var msg = '';
		$('#errMsg').html('').hide();
		msg = Utils.getMessageOfRequireCheck('code','Mã CTTT');
		if(msg.length == 0){
			msg = Utils.getMessageOfSpecialCharactersValidate('code','Mã CTTT',Utils._CODE);
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfRequireCheck('name','Tên CTTT');
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfSpecialCharactersValidate('name','Tên CTTT');
		}		
		if(msg.length == 0){
			msg = Utils.getMessageOfRequireCheck('fDate','Từ ngày');
		} 
		if(msg.length == 0){
			msg = Utils.getMessageOfInvalidFormatDate('fDate','Từ ngày');
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfInvalidFormatDate('tDate','Đến ngày');
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfRequireCheck('status','Trạng thái',true);
		}
		var fDate = $('#fDate').val();
		var tDate = $('#tDate').val();
		if($('#status').val().trim() == activeType.WAITING){
			if(msg.length == 0){
				if (fDate != '' && !Utils.isDate(fDate)) {
					msg = 'Từ ngày không hợp lệ, vui lòng nhập theo định dạng dd/MM/yyyy';
					$('#fDate').focus();
				}
			}
		}
		if(msg.length == 0){
//			if(($('#stableToDate').val() != '') && ($('#stableToDate').val().trim() != $('#tDate').val().trim())){
			if($('#stableToDate').val().trim() != $('#tDate').val().trim()){
				if(tDate != '' && !Utils.isDate(tDate)){
					msg = 'Đến ngày không hợp lệ, vui lòng nhập theo định dạng dd/MM/yyyy';
					$('#tDate').focus();
				}
			}
		}
		var currentTime = new Date();
		var month = currentTime.getMonth() + 1;
		var day = currentTime.getDate();
		var year = currentTime.getFullYear();
		var proStatus = $('#proStatus').val();
		if($('#status').val().trim() == activeType.RUNNING){
			if(msg.length == 0){
				if(fDate != '' && tDate != ''){
					if(!Utils.compareDate(fDate, tDate)){
						msg = 'Từ ngày phải là ngày trước hoặc cùng ngày với đến ngày';	
						$('#fDate').focus();
					}
				}
			}
		}

		if($('#status').val().trim() == activeType.RUNNING && proStatus == activeType.WAITING){
			if(msg.length == 0){
				if(fDate != ''){
					if(!Utils.compareDate(day + '/' + month + '/' + year,fDate)){
						msg = 'Từ ngày phải lớn hơn hoặc bằng ngày hiện tại.';	
						$('#fDate').focus(); 
					}
				}
			}
		}
		if(msg.length == 0){
			if($('#stableToDate').val().trim() != $('#tDate').val().trim()){
				if(tDate != ''){
					if(!Utils.compareDate(day + '/' + month + '/' + year,tDate)){
						msg = 'Đến ngày phải lớn hơn hoặc bằng ngày hiện tại.';	
						$('#tDate').focus();
					}
				}
			}
		}	
		if(msg.length > 0){
			$('#errMsgInfo').html(msg).show();
			return false;
		}
		var dataModel = new Object();		
		dataModel.id = $('#selId').val().trim();
		dataModel.code = $('#code').val().trim();
		dataModel.name = $('#name').val().trim();
		dataModel.fromDate = $('#fDate').val().trim();
		dataModel.toDate = $('#tDate').val().trim();
		dataModel.status = $('#status').val().trim();
		if(!Utils.validateAttributeData(dataModel, '#errMsgInfo')){
			return false;
		}
		Utils.addOrSaveData(dataModel, "/focus-program/save-info", FocusProgram._xhrSave, 'errMsgInfo', function(data){
			$('#selId').val(data.id);	
			$('#subContentId').val(0);
			$.cookie('proTabOrder', 1);
			window.location.href = '/focus-program/change?id=' + data.id + '&subContentId=0';	
			showSuccessMsg('successMsg1',data);
		});		
		return false;
	},
	getSelectedProgram: function(id){
		if(id!= undefined && id!= null){
			location.href = '/focus-program/change?id=' + id + '&subContentId=1';			
		} else {
			$('#subContentId').val(0);
			location.href = '/focus-program/change?subContentId=0';
		}		
		return false;
	},
	saveShop: function(){
		$(".ErrorMsgStyle").hide();
		var msg = '';
		$('#errMsgShop').html('').hide();
		var areaId = -2;
		var regionId = -2;
		var shopCode = '';
		var shopMapId = $('#selShopMapId').val().trim();
		if(shopMapId.length == 0 || shopMapId == 0){
			if($('#rbArea').is(':checked')){
				msg = Utils.getMessageOfRequireCheck('area','Miền');
				areaId = $('#area').val();
			}
			if($('#rbRegion').is(':checked')){
				msg = Utils.getMessageOfRequireCheck('region','Vùng');
				regionId = $('#region').val();
			}
			if($('#rbShop').is(':checked')){
				msg = Utils.getMessageOfRequireCheck('shopCode','Mã đơn vị');
				if(msg.length == 0){
					msg = Utils.getMessageOfSpecialCharactersValidate('shopCode','Mã đơn vị',Utils._CODE);
				}
				shopCode = $('#shopCode').val();
			}
		}
		
		if(msg.length == 0){
			msg = Utils.getMessageOfRequireCheck('shopStatus','Trạng thái',true);
		}
		if(msg.length > 0){
			$('#errMsgShop').html(msg).show();
			return false;
		}
		var dataModel = new Object();		
		dataModel.id = $('#selId').val().trim();
		dataModel.shopMapId = $('#selShopMapId').val().trim();
		dataModel.areaId = areaId;
		dataModel.regionId = regionId;		
		dataModel.shopCode = shopCode;		
		dataModel.status = $('#shopStatus').val().trim();
		Utils.addOrSaveRowOnGrid(dataModel, "/focus-program/save-shop", FocusProgram._xhrSave, 'shopGrid','errMsgShop', function(data){
			$('#selShopMapId').val(0);	
			$("#shopGrid").trigger("reloadGrid");
			FocusProgram.resetShopForm();
			showSuccessMsg('successMsgShop',data);
		},'shopLoading');		
		return false;
	},
	getSelectedShopRow:function(rowId,status){
		var code = $("#shopGrid").jqGrid ('getCell', rowId, 'shop.shopCode');
		var name = $("#shopGrid").jqGrid ('getCell', rowId, 'shop.shopName');
		var id = $("#shopGrid").jqGrid ('getCell', rowId, 'id');		
		$('#selShopMapId').val(id);
		$('#shopCode').val(code);
		$('#shopName').val(name);
		setSelectBoxValue('shopStatus',Utils.getStatusValue(status));
		disabled('shopCode');
		$('#btnAddShop').hide();
		$('#btnEditShop').show();
		$('#btnCancelShop').show();
		return false;
	},
	resetShopForm: function(){
		$('#btnAddShop').show();
		$('#btnEditShop').hide();
		$('#btnCancelShop').hide();
		enable('shopCode');
		setSelectBoxValue('shopStatus');
		setSelectBoxValue('area');
		setSelectBoxValue('region');
		$('#selShopMapId').val(0);
		$('#shopCode').val('');
		$('#shopName').val('');
	},
	deleteShopRow: function(id){
		var dataModel = new Object();
		dataModel.shopMapId = id;	
		Utils.deleteSelectRowOnGrid(dataModel, 'đơn vị', '/focus-program/delete-shop', FocusProgram._xhrDel, 'shopGrid','errMsgShop',function(data){
			showSuccessMsg('successMsgShop',data);
		},'shopLoading');		
		return false;
	},
	saveProduct: function(){
		$(".ErrorMsgStyle").hide();
		var msg = '';
		$('#errMsgProduct').html('').hide();
		var catCode = '';
		var subCatCode = '';
		var productCode = '';
		var staffTypeId = $('#staffType').val();
		if($('#rbCat').is(':checked')){
			msg = Utils.getMessageOfRequireCheck('catCode','Ngành hàng');
			catCode = $('#catCode').val();
		}
		if($('#rbSubCat').is(':checked')){
			msg = Utils.getMessageOfRequireCheck('subCatCode','Ngành hàng con');
			subCatCode = $('#subCatCode').val();
		}
		if($('#rbProduct').is(':checked')){
			msg = Utils.getMessageOfRequireCheck('productCode','Mã sản phẩm');			
			productCode = $('#productCode').val();
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfRequireCheck('staffType','Loại nhân viên',true);
		}
		
		if(msg.length > 0){
			$('#errMsgProduct').html(msg).show();
			return false;
		}
		var dataModel = new Object();
		dataModel.id = $('#selId').val().trim();
		dataModel.catCode = catCode;
		dataModel.subCatCode = subCatCode;		
		dataModel.productCode = productCode;
		dataModel.staffTypeId = staffTypeId;
		Utils.addOrSaveRowOnGrid(dataModel, "/focus-program/save-product", FocusProgram._xhrSave, 'productGrid','errMsgProduct', function(data){
			$("#productGrid").trigger("reloadGrid");
			$('#catCode').val('');
			$('#subCatCode').val('');
			$('#productCode').val('');
			$('#productName').val('');
			setSelectBoxValue('staffType');		
			showSuccessMsg('successMsgProduct',data);
		},'productLoading');		
		return false;
	},
	showOthersTab: function(){
		$('#tabActive2').removeClass('Disable');
		$('#tabActive3').removeClass('Disable');
		$('#tabActive4').removeClass('Disable');
		$('#tabActive5').removeClass('Disable');
		//$('#tab1').bind('click',FocusProgram.showTab1);
		//$('#tab2').bind('click',FocusProgram.showTab2);
		//$('#tab3').bind('click',FocusProgram.showTab3);
	}
	,deleteProductRow: function(id){
		$(".ErrorMsgStyle").hide();
		var dataModel = new Object();
		dataModel.productId = id;	
		Utils.deleteSelectRowOnGrid(dataModel, 'Sản phẩm', '/focus-program/delete-product', FocusProgram._xhrDel, 'productGrid','errMsgProduct',function(data){
			showSuccessMsg('successMsgProduct',data);
		},'productLoading');		
		return false;
	},
	searchShop: function(){
		$(".ErrorMsgStyle").hide();
		var code = $('#shopCode').val().trim();
		var name = $('#shopName').val().trim();
		var id = $('#progId').val();
		FocusProgram._searchShopParam.id = id;
		FocusProgram._searchShopParam.shopCode = code;
		FocusProgram._searchShopParam.shopName = name;
		var url = FocusProgram.getShopGridUrl(id,code,name);
		$("#exGrid").treegrid({url:url});
		$('#errExcelMsg').html("").hide();
	},
	showDialogCreateShop: function(){
		$(".ErrorMsgStyle").hide();
		var html = $('#shopTreeDialog').html();
		$.fancybox(html,
				{
					modal: true,
					autoResize: false,
					title: 'Thêm đơn vị tham gia CTTT',
					afterShow: function(){
						Utils.resetCheckboxStateOfTree();
						Utils.bindAutoSearch();	
						$('#shopTreeDialog').html('');
						$('#successMsgShopDlg').html('').hide();
						var focusProgramId = $('#selId').val();
						$('#shopCodeDlg').focus();
						var shopCode = encodeURI('');
						var shopName = encodeURI('');	
						FocusProgram.loadShopTreeOnDialog('/rest/catalog/focus-program/shop/list.json?focusProgramId=' + focusProgramId +'&shopCode=' + encodeChar(shopCode) +'&shopName=' + encodeChar(shopName));
						FocusProgram._mapShop = new Map();
						$('#shopTree').bind("loaded.jstree open_node.jstree close_node.jstree", function (event, data) {
							if($('#shopTreeContent').data('jsp') != undefined || $('#shopTreeContent').data('jsp') != null) {
								$('#shopTreeContent').data('jsp').destroy();
								setTimeout(function(){
									$('#shopTreeContent').jScrollPane();
								},500);	
							} else {
								setTimeout(function(){
									$('#shopTreeContent').jScrollPane();
								},500);	
							}						
		    			});
					},
					afterClose: function(){
						$('#shopTreeDialog').html(html);						
					}
				}
			);
		return false;
	},
	getCheckboxStateOfTreeEx:function(treeId,MAP,selector,nodeType){
		for(var i=0;i<MAP.size();++i){
			var _obj = MAP.get(MAP.keyArray[i]);
			var type = true;			
			$('#' + selector).each(function(){
				var _id = $(this).attr('id');
				if(nodeType!=null && nodeType!=undefined){
					var getType = $(this).attr('contentitemid');
					type = (nodeType==getType);
				}
				if(_id==_obj && type){
					$(this).removeClass('jstree-unchecked');
					$(this).addClass('jstree-checked');
				}
			});
		}
	},	
	searchShopOnTree: function(){
		$(".ErrorMsgStyle").hide();
		//Utils.getCheckboxStateOfTree();
		$('#shopTree li').each(function(){
			var _id = $(this).attr('id');
			if($(this).hasClass('jstree-checked')){
				FocusProgram._mapShop.put(_id,_id);	
			}else{
				FocusProgram._mapShop.remove(_id);
			}		
		});
		var focusProgramId = $('#selId').val();
		var shopCode = encodeChar($('#shopCodeDlg').val().trim());
		var shopName = encodeChar($('#shopNameDlg').val().trim());
		$('#shopTreeContent').data('jsp').destroy();
		FocusProgram.loadShopTreeOnDialog('/rest/catalog/focus-program/shop/list.json?focusProgramId=' + focusProgramId +'&shopCode=' + encodeChar(shopCode) +'&shopName=' + encodeChar(shopName));
		setTimeout(function() {
			$('#shopTreeContent').jScrollPane();
			$('#shopTree').bind("loaded.jstree open_node.jstree close_node.jstree", function (event, data) {
				if($('#shopTreeContent').data('jsp') != undefined || $('#shopTreeContent').data('jsp') != null) {
					$('#shopTreeContent').data('jsp').destroy();
					setTimeout(function(){
						$('#shopTreeContent').jScrollPane();
					},500);	
				} else {
					setTimeout(function(){
						$('#shopTreeContent').jScrollPane();
					},500);	
				}
				FocusProgram.getCheckboxStateOfTreeEx('shopTree',FocusProgram._mapShop,'shopTree li');
			});
			FocusProgram.getCheckboxStateOfTreeEx('shopTree',FocusProgram._mapShop,'shopTree li');
		}, 500);
		$('.fancybox-inner #shopCodeDlg').focus();
	},
	selectListShop: function(){
		$('#errMsgShopDlg').html('').hide();
		$('#successMsgShopDlg').html('').hide();
		$.fancybox.update();		
		var arrId = new Array();
		$('.jstree-checked').each(function(){
		    var _id= $(this).attr('id');
		    if($('#' + _id + ' ul li').length >0 && (!$(this).parent().parent().hasClass('jstree-checked'))){
		    	FocusProgram._mapShop.put(_id,_id);	
		    }else if($('#' + _id ).length >0 && (!$(this).parent().parent().hasClass('jstree-checked'))){
		    	FocusProgram._mapShop.put(_id,_id);	
			}else{
		    	FocusProgram._mapShop.remove(_id);	
			}	
		});
//		$('#shopTree li').each(function(){
//			var _id = $(this).attr('id');			
//			if($(this).hasClass('jstree-checked')){
//				FocusProgram._mapShop.put(_id,_id);		
//			}else{
//				FocusProgram._mapShop.remove(_id);	
//			}			
//		});
		for(var i=0;i<FocusProgram._mapShop.size();++i){
			var _obj = FocusProgram._mapShop.get(FocusProgram._mapShop.keyArray[i]);
			arrId.push(_obj);
		}		
		var msg = '';
		if(arrId.length == 0){
			msg = format(msgErr_required_choose_format,'Đơn vị');
		}		
		if(msg.length > 0){
			$('#errMsgShopDlg').html(msg).show();			
			$.fancybox.update();
			return false;
		}		
		var focusProgramId = $('#selId').val();
		var dataModel = new Object();		
		dataModel.lstShopId = arrId;		
		dataModel.id = focusProgramId;
		dataModel.status = -2;
		Utils.addOrSaveData(dataModel, '/focus-program/shop/save', FocusProgram._xhrSave, 'errMsgShopDlg', function(data){
			$('#successMsgShopDlg').html('Lưu dữ liệu thành công').show();
			$('#shopCodeDlg').val('');
			$('#shopNameDlg').val('');
			$('#shopCode').val('');
			$('#shopName').val('');
			setSelectBoxValue('shopStatus', activeType.RUNNING);
			Utils.resetCheckboxStateOfTree();
			FocusProgram.searchShopOnTree();	
			FocusProgram.searchShop();
			$(window).resize();
			$.fancybox.update();
			//$("#shopGrid").trigger("reloadGrid");
			setTimeout(function(){$.fancybox.close();}, 1000);
		}, null, '.fancybox-inner', null, null);
		return false;
	},
	showUpdateShopDetailDialog: function(rowId,status){
		$(".ErrorMsgStyle").hide();
		var html = $('#shopDetailTreeDialog').html();
		$.fancybox(html,
				{
					modal: true,
					autoResize: false,
					title: 'Cập nhật thông tin đơn vị',					
					afterShow: function(){
						$('#shopDetailTreeDialog').html('');
						$('#errMsgShopDetailDlg').html('').hide();
						$('#successMsgShopDetailDlg').html('').hide();						
						
						var name = $("#shopGrid").jqGrid ('getCell', rowId, 'shop.shopName');
						var id = $("#shopGrid").jqGrid ('getCell', rowId, 'shop.id');					    						
						if(status == 'RUNNING'){
							status = 1;
						} else if(status == 'STOPPED'){
							status = 0;
						} else {
							status = -2;
						}						
						$('#shopNameDetailDlg').val(name);
						$('#shopStatusDlgTmp').attr('id','shopStatusDlg');
						$('#shopStatusDlg').val(status);
						$('#shopStatusDlg').addClass('MySelectBoxClass');
						$('#shopStatusDlg').customStyle();						
						$('#shopIdDetailDlg').val(id);
					},
					afterClose: function(){
						$('#shopDetailTreeDialog').html(html);						
					}
				}
			);
		return false;
	},
	saveDetailShop: function(){
		$(".ErrorMsgStyle").hide();
		$('#errMsgShopDetailDlg').html('').hide();
		$('#successMsgShopDetailDlg').html('').hide();
		$.fancybox.update();
		var msg = '';
		msg = Utils.getMessageOfRequireCheck('shopStatusDlg', 'Trạng thái',true);		
		if(msg.length > 0){
			$('#errMsgShopDetailDlg').html(msg).show();
			$.fancybox.update();
			return false;
		}		
		var focusProgramId = $('#selId').val();
		var arrId = new Array();
		arrId.push($('#shopIdDetailDlg').val());
		var dataModel = new Object();
		dataModel.lstShopId = arrId;
		dataModel.id = focusProgramId;
		dataModel.status = $('#shopStatusDlg').val().trim();
		dataModel.numUpdate = 0; 
		Utils.addOrSaveData(dataModel, '/focus-program/shop/save', ProgrammeDisplayCatalog._xhrSave, 'errMsgShopDlg', function(data){
			$('#successMsgShopDlg').html('Lưu dữ liệu thành công').show();						
			$.fancybox.update();
			FocusProgram.searchShop();
			setTimeout(function(){$.fancybox.close();}, 1000);
		}, null, '.fancybox-inner', null, null);
		return false;
	},
	showDialogCreateProduct: function(){
		var html = $('#productTreeDialog').html();
		$.fancybox(html,
				{
					modal: true,
					autoResize: false,
					title: 'Thêm thông tin sản phẩm',
					afterShow: function(){
						Utils.resetCheckboxStateOfTree();
						Utils.bindAutoSearch();	
						$('#productTreeDialog').html('');
						var focusProgramId = $('#selId').val();
						$('#productCodeDlg').focus();
						$('#saleManDlgTmp').attr('id','saleManDlg');
						$('#saleManDlg').addClass('MySelectBoxClass');
						$('#saleManDlg').customStyle();
						$('#typeDialog').addClass('MySelectBoxClass');
						$('#typeDialog').customStyle();
						$.getJSON('/rest/catalog/focus-program/' + focusProgramId +'/staff-type/list.json', function(data){
							var arrHtml = new Array();
							arrHtml.push('<option value="-2">-Chọn hình thức bán hàng-</option>');
							for(var i=0;i<data.length;i++){
								arrHtml.push('<option value="' + data[i].value +'">' + Utils.XSSEncode(data[i].name) +'</option>');
							}
							$('#saleManDlg').html(arrHtml.join(""));
							$('#saleManDlg').change();
						});						
						var productCode = encodeURI('');
						var productName = encodeURI('');						
						FocusProgram.loadDataForTreeWithCheckboxEx('/rest/catalog/focus-program/product/list.json?focusProgramId=' + focusProgramId +'&productCode=' + encodeChar(productCode) +'&productName=' + encodeChar(productName) + '&nodeType=cat&catId=0&subCatId=0','productTree');
						FocusProgram._mapProduct = new Map();
						$('#productTree').bind("loaded.jstree open_node.jstree close_node.jstree", function () {
							if($('#productTreeContent').data('jsp') != undefined || $('#productTreeContent').data('jsp') != null) {
								$('#productTreeContent').data('jsp').destroy();
								setTimeout(function(){
									$('#productTreeContent').jScrollPane();
								},500);	
							} else {
								setTimeout(function(){
									$('#productTreeContent').jScrollPane();
								},500);	
							}
		    			});
					},
					afterClose: function(){
						$('#productTreeDialog').html(html);						
					}
				}
			);
		return false;
	},
	searchProductOnTree: function(){
		$(".ErrorMsgStyle").hide();
		//Utils.getCheckboxStateOfTree();
		$('#productTree li').each(function(){
			var _id = $(this).attr('id');
			var type = $(this).attr('contentitemid');
			if($(this).hasClass('jstree-checked') && type=='product'){
				FocusProgram._mapProduct.put(_id,_id);		
			}else {
				FocusProgram._mapProduct.remove(_id);
			}
		});
		var focusProgramId = $('#selId').val();
		var productCode = encodeChar($('#productCodeDlg').val().trim());
		var productName = encodeChar($('#productNameDlg').val().trim());
		$('#productTreeContent').data('jsp').destroy();
		FocusProgram.loadDataForTreeWithCheckboxEx('/rest/catalog/focus-program/product/list.json?focusProgramId=' + focusProgramId +'&productCode=' + encodeChar(productCode) +'&productName=' + encodeChar(productName) + '&nodeType=cat&catId=0&subCatId=0','productTree');
		setTimeout(function() {
			$('#productTreeContent').jScrollPane();
			$('#productTree').bind("loaded.jstree open_node.jstree close_node.jstree", function (event, data) {
				//Utils.applyCheckboxStateOfTree();
				if($('#productTreeContent').data('jsp') != undefined || $('#productTreeContent').data('jsp') != null) {
					$('#productTreeContent').data('jsp').destroy();
					setTimeout(function(){
						$('#productTreeContent').jScrollPane();
					},500);	
				} else {
					setTimeout(function(){
						$('#productTreeContent').jScrollPane();
					},500);	
				}
				FocusProgram.getCheckboxStateOfTreeEx('productTree',FocusProgram._mapProduct,'productTree li','product');
			});
			FocusProgram.getCheckboxStateOfTreeEx('productTree',FocusProgram._mapProduct,'productTree li','product');
		}, 500);
		$('.fancybox-inner #productCodeDlg').focus();
	},
	selectListProduct: function(){
		$('#errMsgProductDlg').html('').hide();
		$('#successMsgProductDlg').html('').hide();
		$.fancybox.update();
		var arrId = new Array();
/*		$('.jstree-leaf').each(function(){
			if($(this).hasClass('jstree-checked')){
				arrId.push($(this).attr('id'));
			}			
		});
		$('li[classstyle=product]').each(function(){
		    if($(this).hasClass('jstree-checked')){
		    	arrId.push($(this).attr('id'));
		    }
		});*/
		$('#productTree li').each(function(){
			var _id = $(this).attr('id');
			var type = $(this).attr('contentitemid');
			if($(this).hasClass('jstree-checked') && type=='product'){
				FocusProgram._mapProduct.put(_id,_id);		
			}else{
				FocusProgram._mapProduct.remove(_id);	
			}			
		});
		for(var i=0;i<FocusProgram._mapProduct.size();++i){
			var _obj = FocusProgram._mapProduct.get(FocusProgram._mapProduct.keyArray[i]);
			arrId.push(_obj);
		}
		var msg = '';
		if(arrId.length == 0){
			msg = format(msgErr_required_choose_format,'Sản phẩm');
		}	
		if(msg.length ==0){
			msg = Utils.getMessageOfRequireCheck('saleManDlg', 'Hình thức bán hàng',true);
		}
		if(msg.length ==0){
			msg = Utils.getMessageOfRequireCheck('typeDialog', 'Loại MHTT',true);
		}
		if(msg.length > 0){
			$('#errMsgProductDlg').html(msg).show();
			$.fancybox.update();
			return false;
		}		
		var focusProgramId = $('#selId').val();
		var dataModel = new Object();		
		dataModel.lstProductId = arrId;		
		dataModel.id = focusProgramId;
		dataModel.channelTypeId = $('#saleManDlg').val().trim();
		dataModel.type = $('#typeDialog').val().trim();
		Utils.addOrSaveData(dataModel, '/focus-program/product/save', ProgrammeDisplayCatalog._xhrSave, 'errMsgProductDlg', function(data){
			$('#successMsgProductDlg').html('Lưu dữ liệu thành công').show();
			$('#productCodeDlg').val('');
			$('#productNameDlg').val('');	
			$('#productCode').val('');
			$('#productName').val('');
			setSelectBoxValue('saleManDlg');
			setSelectBoxValue('typeDialog');
			setSelectBoxValue('type');
			Utils.resetCheckboxStateOfTree();
			FocusProgram.searchProductOnTree();
			FocusProgram.searchProduct();
			$.fancybox.update();
			//$("#productGrid").trigger("reloadGrid");
			setTimeout(function(){$.fancybox.close();}, 1000);
		}, null, '.fancybox-inner', null, null);
		return false;
	},	
	showUpdateProductDialog: function(rowId){
		var html = $('#productDetailTreeDialog').html();
		$.fancybox(html,
				{
					modal: true,
					autoResize: false,
					title: 'Cập nhật thông tin sản phẩm',					
					afterShow: function(){
						$('#productDetailTreeDialog').html('');
						$('#errMsgProductDetailDlg').html('').hide();
						$('#successMsgProductDetailDlg').html('').hide();
						var focusProgramId = $('#selId').val();
						$('#saleManDetailDlgTmp').attr('id','saleManDetailDlg');
						$('#saleManDetailDlg').addClass('MySelectBoxClass');
						$('#saleManDetailDlg').customStyle();		
						$('#typeDetailDialog').addClass('MySelectBoxClass');
						$('#typeDetailDialog').customStyle();
						$.getJSON('/rest/catalog/focus-program/' + focusProgramId +'/staff-type/list.json', function(data){
							var arrHtml = new Array();
							arrHtml.push('<option value="-2">--- Chọn kiểu bán hàng ---</option>');
							for(var i=0;i<data.length;i++){
								arrHtml.push('<option value="' + data[i].value +'">' + Utils.XSSEncode(data[i].name) +'</option>');
							}
							//Chu y cho nay: vi focusChannelMap.staffSale.id da bi bo di
//							var channelTypeId = $("#productGrid").jqGrid ('getCell', rowId, 'focusChannelMap.staffSale.id');
							$('#saleManDetailDlg').html(arrHtml.join(""));
							var saleStaffType = $("#productGrid").jqGrid ('getCell', rowId, 'focusChannelMap.saleTypeCode');
							$.getJSON('/rest/catalog/focus-program/apparam/' + saleStaffType +'/id.json', function(data){
								$('#saleManDetailDlg').val(data.value);
								$('#saleManDetailDlg').change();
							});
						});
						var name = $("#productGrid").jqGrid ('getCell', rowId, 'product.productName');						
						var productId = $("#productGrid").jqGrid ('getCell', rowId, 'product.id');
						$('#productNameDetailDlg').val(name);
						$('#productIdDetailDlg').val(productId);
						var type = $("#productGrid").jqGrid ('getCell', rowId, 'type');
						//Chu y cho nay vi: type da chuyen sang kieu String//
//						type = focusProductType.parseValue(type);
//						setSelectBoxValue('typeDetailDialog', type);
						$.getJSON('/rest/catalog/focus-program/apparam/' + type +'/id.json', function(data){
							setSelectBoxValue('typeDetailDialog', data.value);
						});						
					},
					afterClose: function(){
						$('#productDetailTreeDialog').html(html);						
					}
				}
			);
		return false;
	},
	saveDetailProduct: function(){
		$('#errMsgProductDetailDlg').html('').hide();
		$('#successMsgProductDetailDlg').html('').hide();
		$.fancybox.update();		
		var msg = '';			
		if(msg.length ==0){
			msg = Utils.getMessageOfRequireCheck('saleManDetailDlg', 'Hình thức bán hàng',true);
		}
		if(msg.length ==0){
			msg = Utils.getMessageOfRequireCheck('typeDetailDialog', 'Loại MHTT',true);
		}
		if(msg.length > 0){
			$('#errMsgProductDetailDlg').html(msg).show();
			$.fancybox.update();
			return false;
		}	
		var arrId = new Array();
		arrId.push($('#productIdDetailDlg').val().trim());
		var focusProgramId = $('#selId').val();
		var dataModel = new Object();		
		dataModel.lstProductId = arrId;		
		dataModel.id = focusProgramId;
		dataModel.channelTypeId = $('#saleManDetailDlg').val().trim();
		dataModel.type = $('#typeDetailDialog').val().trim();
		Utils.addOrSaveData(dataModel, '/focus-program/product/save', ProgrammeDisplayCatalog._xhrSave, 'errMsgProductDetailDlg', function(data){
			$('#successMsgProductDetailDlg').html('Lưu dữ liệu thành công').show();						
			$.fancybox.update();
			$("#productGrid").trigger("reloadGrid");
			setTimeout(function(){$.fancybox.close();}, 1000);
		}, null, '.fancybox-inner', null, null);
		return false;
	},
	loadShopTreeOnDialog: function(url){
		$.getJSON(url, function(data){
			$('#shopTree').bind("loaded.jstree open_node.jstree close_node.jstree", function (event, data) {				
				if($('#shopTreeContent').data('jsp') != undefined || $('#shopTreeContent').data('jsp') != null) {
					$('#shopTreeContent').data('jsp').destroy();
					setTimeout(function(){
						$('#shopTreeContent').jScrollPane();
					},500);	
				} else {
					setTimeout(function(){
						$('#shopTreeContent').jScrollPane();
					},500);	
				}
			}).jstree({
		        "plugins": ["themes", "json_data","ui","checkbox"],
		        "themes": {
		            "theme": "classic",
		            "icons": false,
		            "dots": true
		        },
		        "json_data": {
		        	"data": data,
		        	"ajax" : {
		        		"method": "GET",
		                "url" : '/rest/catalog/sub-shop/list.json?focusProgramId=' +$('#selId').val(),
		                "data" : function (n) {
		                        return { id : n.attr ? n.attr("id") : 0 };
		                    }
		            }
		        }
			});
		});
	},
	loadDataForTreeWithCheckbox:function(url,treeId){
		var tId = 'tree';
		if(treeId!= null && treeId!= undefined){
			tId = treeId;
		}
		$('#' + tId).jstree({
	        "plugins": ["themes", "json_data","ui","checkbox"],
	        "themes": {
	            "theme": "classic",
	            "icons": false,
	            "dots": true
	        },
	        "json_data": {
	        	"ajax" : {
	        		"method": "post",
	                "url" : url,
	                "data" : function (n) {
	                        return { id : n.attr ? n.attr("id") : 0 };
	                    }
	            }
	        }
		});	
	},
	loadDataForTreeWithCheckboxEx:function(url,treeId){
		var tId = 'tree';
		if(treeId!= null && treeId!= undefined){
			tId = treeId;
		}
		$('#' + tId).jstree({
	        "plugins": ["themes", "json_data","ui","checkbox"],
	        "themes": {
	            "theme": "classic",
	            "icons": false,
	            "dots": true
	        },
	        "json_data": {
	        	"ajax" : {
	        		"method": "get",
	                "url" : function( node ){	                	
	                      if(node == -1) {
	                    	  return url;
	                      } else {
	                    	  var nodeType = node.attr("classStyle");
	                    	  var catId = 0;
	                    	  var subCatId = 0;
	                    	  if(nodeType == 'cat') {
	                    		  catId = node.attr("id");
	                    		  var focusProgramId = $('#selId').val();
	                    		  var productCode = encodeURI('');
	                    		  var productName = encodeURI('');	
	                    		  return '/rest/catalog/focus-program/product/list.json?focusProgramId=' + focusProgramId +'&productCode=' + encodeChar(productCode) +'&productName=' + encodeChar(productName) + '&nodeType=sub-cat&catId=' +catId+ '&subCatId=0';
	                    	  } else if(nodeType == 'sub-cat') {
	                    		  subCatId = node.attr("id");
	                    		  catId = $('#' +subCatId).parent().parent().attr('id');
	                    		  var focusProgramId = $('#selId').val();
	                    		  var productCode = encodeURI('');
	                    		  var productName = encodeURI('');
	                    		  return '/rest/catalog/focus-program/product/list.json?focusProgramId=' + focusProgramId +'&productCode=' + encodeChar(productCode) +'&productName=' + encodeChar(productName) + '&nodeType=product&catId=' +catId+ '&subCatId=' + subCatId;
	                    	  } else if(nodeType == 'product') {
	                    		  return '';
	                    	  }
	                      }
	                    },
	                "data" : function (n) {
	                        return { id : n.attr ? n.attr("id") : 0 };
	                    }
	            }
	        }
		});	
	},
	exportExcelData:function(){
		$('#divOverlay').show();
		$('#errMsg').html('').hide();
		$('#errExcelMsg').html('').hide();
		var data = new Object();
		data.id = $('#selId').val().trim();
		data.excelType = $('#excelType').val();
		if($('#excelType').val() == 2){//dvtg
			data.id = FocusProgram._searchShopParam.id;
			data.shopCode = FocusProgram._searchShopParam.shopCode;
			data.shopName = FocusProgram._searchShopParam.shopName;
		}else if($('#excelType').val() == 3){//sp cttt
			data.id = FocusProgram._searchProdParam.id;
			data.code = FocusProgram._searchProdParam.code;
			data.name = FocusProgram._searchProdParam.name;
		}
		var url = "/focus-program/getlistexceldata";
		CommonSearch.exportExcelData(data,url);
	},
	exportActionLog: function(){
		var id = $('#selId').val();
		var fromDate = $('#fromDate').val().trim();
		var toDate = $('#toDate').val().trim();
		ExportActionLog.exportActionLog(id, ExportActionLog.FOCUS_PROGRAM, fromDate, toDate,'errMsgActionLog');
	},
	openCopyFocusProgram:function(){
		CommonSearch.openSearchStyle1EasyUICopyPromotionShop('Mã CTTT <span class="RequireStyle" style="color:red">*</span>',
				'Tên CTTT <span class="RequireStyle" style="color:red">*</span>', "Sao chép CTTT","focusProgramCode",
				"focusProgramName",'searchStyle1EasyUIDialogDivEx');
	},
//	SAN PHAM CHUONG TRINH TRONG TAM[[[[
	getProductGridUrl: function(code,name){
		var selId = $('#selId').val();
		FocusProgram._searchProdParam.id = selId;
		FocusProgram._searchProdParam.code = code;
		FocusProgram._searchProdParam.name = name;
		return '/focus-program/search-product?id=' + encodeChar(selId) + '&code=' + encodeChar(code) + '&name=' + encodeChar(name) /*+ '&type=' + encodeChar(type)*/;
	},
	showTab2: function(){
		if ($("#tab2.Active").length == 1) {
			return;
		}
		$("#htbhCTTTTab #errMsgTmp").hide();
		var rows = FocusProgram._mapHTBH.keyArray;
		var lst1 = FocusProgram._lstHTBHdb;
		if (rows.length != lst1.length) {
			if ($("#tab1.Active").length == 0) {
				FocusProgram.showTab1();
			}
			$("#htbhCTTTTab #errMsgTmp").html("<p id='errMsgTmp' class='ErrorMsgStyle'>Bạn phải lưu thay đổi HTBH trước khi qua tab sản phẩm</p>").show();
			return;
		}
		for (var j = 0, sz = rows.length; j < sz; j++) {
			if (lst1.indexOf(rows[j]) < 0) {
				if ($("#tab1.Active").length == 0) {
					FocusProgram.showTab1();
				}
				$("#htbhCTTTTab #errMsgTmp").html("<p id='errMsgTmp' class='ErrorMsgStyle'>Bạn phải lưu thay đổi HTBH trước khi qua tab sản phẩm</p>").show();
				return;
			}
		}
		//var idx = -1;
		/*$('#tab1').removeClass('Active');
		$('#tab2').removeClass('Active');
		$('#tab3').removeClass('Active');
		$('#spCTTTTab').hide();
		$('#dvtgCTTTTab').hide();
		$('#htbhCTTTTab').hide();*/
		FocusProgram.hideAllTab();
		$('#tab2').addClass('Active');
		$('#spCTTTTab').show();
		$('#idImportDiv').show();
		$('#productCode').focus();
		Utils.bindAutoButtonEx('.ContentSection','btnSearchProduct');
		$('#downloadTemplate').attr('href',excel_template_path + 'catalog/Bieu_mau_danh_muc_cttt_HTBH_TTSP.xls');
		$('#excelType').val(3);
		$('#errExcelMsg').html('').hide();
		var title = '';
		if($('#statusHidden').val().trim() == activeType.WAITING){
			title = '<a title="Thêm mới" href="javascript:void(0);" onclick="FocusProgram.openEasyUIDialog();"><img src="/resources/images/icon-add.png"></a>';
		}
		$("#grid").datagrid({
			  url:FocusProgram.getProductGridUrl('',''),
			  columns:[[		
			    {field:'categoryCode', title: 'Ngành', width: 80, sortable:false,resizable:false, align: 'left',
			    	formatter: function(index,row){
				    	if(row.product != null && row.product != undefined && row.product.cat != null&& row.product.cat != undefined){
				    		return Utils.XSSEncode(row.product.cat.productInfoCode);
				    	}
			    	}
			    },
			    {field:'productCode', title: 'Mã SP', width: 100, sortable:false,resizable:false , align: 'left',
			    	formatter:function(index,row){
				    	if(row.product!=null){
				    		return Utils.XSSEncode(row.product.productCode);
				    	}
				    }
			    },
			    {field:'productName', title: 'Tên SP', width: 100, align: 'left', sortable:false,resizable:false ,  
		            formatter: function(index,row){
		            	if(row.product!=null){
				    		return Utils.XSSEncode(row.product.productName);
				    	}
		            }
			    },
			    {field:'createUser', title: 'Mã HTBH', width:80, align: 'left',sortable:false,resizable:false ,  
	                editor:{  
	                    type:'combobox',
	                    options:{
	                    	data: FocusProgram._lstSaleTypeData.get(0).rows,
	            	        valueField:'saleTypeCode',  
	            	        textField:'saleTypeCode',
//	            	        method:'GET',
//	            	        class:'easyui-combobox',
//	            	        editable:false,
	            	        width :40,
	            	        
//	                        panelHeight :140,        
	                        filter: function(q, row){
	                    		var opts = $(this).combobox('options');
	                    		return row[opts.textField].indexOf(q.toUpperCase()) == 0;
	                    	}
	                    }
	                }
			    },
			    {field:'type', title: 'Loại MHTT', width: 150, align: 'left',fixed:true,sortable:false,resizable:false ,  
                    editor:{  
                        type:'combobox',
                        options:{
                        	data: FocusProgram._lstTypeData.get(0).rows,  
	            	        valueField:'apParamCode',  
	            	        textField:'apParamCode',
//	            	        method:'GET',
//	            	        class:'easyui-combobox',
	            	        width:150,
	            	        panelWidth:150,
	            	        editable:false,
//	                        panelHeight :140, 
	                        filter: function(q, row){
	                    		var opts = $(this).combobox('options');
	                    		return row[opts.textField].indexOf(q.toUpperCase()) == 0;
	                    	}
	                    }
                    }
			    },
			    {field:'edit', title: '', width: 50, align: 'center',sortable:false,resizable:false, formatter: function(value,row,index){
			    	if (row.editing){
		                var s = "<a title='Lưu' href='javascript:void(0)' onclick='return FocusProgram.saveRow(this)'><span style='cursor:pointer'><img width='15' height='15' src='/resources/images/icon-datach.png'/></span></a> ";  
		                var c = "<a title='Quay lại' href='javascript:void(0)' onclick='FocusProgram.cancelRow(this)'><span style='cursor:pointer'><img width='15' height='15' src='/resources/images/icon_esc.png'/></span></a>";  
		                return s + c;  
		            } else {
		            	var e = '';
		            	if($('#statusHidden').val().trim() == activeType.WAITING){
		            		e = "<a title='Sửa' href='javascript:void(0)' onclick='FocusProgram.editRow(this);'><span style='cursor:pointer'><img width='15' height='15' src='/resources/images/icon-edit.png'/></span></a>";  
		            	}
		                return e;  
		            }  
			    }},
			    {field:'delete', title: title, width: 50, align: 'center',sortable:false,resizable:false,
			    	formatter:function(index,row){
			    		if($('#statusHidden').val().trim() == activeType.WAITING){
			    			return "<a title='Xóa' href='javascript:void(0)' onclick=\"return FocusProgram.deleteRow('" + row.id +"')\"><img width='15' height='15' src='/resources/images/icon-delete.png'/></a>";
			    		}else{
			    			return '';
			    		}
			    		
			    	} 
			    },
			  ]],	  
			  pageList  : [10,20,30],
//			  width: 910,
			  height:'auto',
			  scrollbarSize : 0,
			  pagination:true,
			  checkOnSelect :true,
			  fitColumns:true,
			  method : 'GET',
			  rownumbers: true,	  
			  singleSelect:true,
			  pageNumber:1,
			  width: ($('#productFocusGrid').width()),
			  onLoadSuccess:function(){
				  setTimeout(function(){
					  $('#successMsgProduct').html('').hide();
				  },3000);
				  //var angelkid;
				  $('.datagrid-header-rownumber').html('STT');
		      	  $('#grid').datagrid('resize');
		      	  edit = false;
		      },
		  	onBeforeEdit:function(index,row){
		  		//$()
		        row.editing = true;
		        edit =true;
		        idx = index;  
		        FocusProgram.updateActions(index);  
		    },  
		    onAfterEdit:function(index,row){  
		        row.editing = false;
		        edit = false;
		        idx = -1;
		        $('#saleTypeCode').val(row.createUser);
		        $('#type').val(row.type);
		        FocusProgram.updateActions(index);  
		    },  
		    onCancelEdit:function(index,row){  
		        row.editing = false;  
		        edit = false;
		        idx = -1;
		        FocusProgram.updateActions(index);  
		    }  
		});
	},
	getJsonListSaleType:function(){
		$.getJSON('/rest/catalog/focus-program/product/sale-type-code.json?id=' + $('#focusProgramId').val(),function(data){
			if(data != null && data != undefined){
				FocusProgram._lstSaleTypeData.put(0, data);
			}
		});
	},
	getJsonListType:function(){
		$.getJSON('/rest/catalog/focus-program/product/type.json',function(data){
			if(data != null && data != undefined){
				FocusProgram._lstTypeData.put(0, data);
			}
		});
	},
	editRow:function(target){
		$('.ErrorMsgStyle').hide();
		var indexRow = FocusProgram.getRowIndex(target);
	    $('#grid').datagrid('beginEdit', indexRow);
	    var width = 75;
	    $('td[field=saleTypeCode] .datagrid-editable-input').combobox('resize', width);
	    $('td[field=type] .datagrid-editable-input').combobox('resize', width);
	},
	updateActions:function(index){
	    $('#grid').datagrid('updateRow',{
	        index: index,  
	        row:{}  
	    });  
	}, 
	getRowIndex:function(target){  
	    var tr = $(target).closest('tr.datagrid-row');  
	    return parseInt(tr.attr('datagrid-row-index'));  
	},  
	saveRow:function(target){
		$('.ErrorMsgStyle').hide();
		var indexRow = FocusProgram.getRowIndex(target);
		$('#errMsg').html('').hide();
	    $('#grid').datagrid('endEdit', indexRow);
	    var saleTypeCode = $('#grid').datagrid('getData').rows[indexRow].createUser;;
	    var focusProductType = $('#grid').datagrid('getData').rows[indexRow].type;
	    var msg = '';
	    $('#errMsgProduct').html('').hide();
	    if(saleTypeCode == null || saleTypeCode.length == 0 ){
	    	msg = 'Vui lòng chọn mã HTBH';
	    	$('#grid').datagrid('beginEdit', indexRow);
	    	$('td[field=createUser] .combo-text').focus();
//	    	$('tr[datagrid-row-index = indexRow] td[field=createUser] .combo-text').focus();
	    }
	    if(msg.length == 0){
	    	if(focusProductType == null || focusProductType.length == 0 ){
		    	msg = 'Vui lòng chọn loại MHTT';
		    	 $('#grid').datagrid('beginEdit', indexRow);
		    	$('td[field=type] .combo-text').focus();
//		    	$('tr[datagrid-row-index = indexRow] td[field=type] .combo-text').focus();
		    }
	    }
	    if(msg.length > 0){
	    	$('#errMsgProduct').html(msg).show();
	    	return false;
	    }
	    var dataModel = new Object();		
		dataModel.id = $('#selId').val();
		dataModel.focusMapProductId = $('#grid').datagrid('getData').rows[indexRow].id;
		dataModel.staffTypeCode = saleTypeCode;
		dataModel.focusProductType = focusProductType;
		dataModel.isUpdate = true;
		Utils.addOrSaveRowOnGrid(dataModel, '/focus-program/product/save', ProgrammeDisplayCatalog._xhrSave, null,null, function(data){
			$('#successMsgProduct').html('Lưu dữ liệu thành công').show();
			$("#grid").datagrid("reload");
		});
		return false;
	},
	cancelRow:function (target){
		$('.ErrorMsgStyle').hide();
		var indexRow = FocusProgram.getRowIndex(target);
	    $('#grid').datagrid('cancelEdit', indexRow);  
	},
	deleteRow: function(productId){
		$('.ErrorMsgStyle').hide();
		$('#errMsg').html('').hide();
		var dataModel = new Object();
		dataModel.productId = productId;	
		Utils.deleteSelectRowOnGrid(dataModel, 'SP của CTTT', "/focus-program/delete-product", FocusProgram._xhrDel, null, 'errMsgProduct',function(){
			$('#successMsgProduct').html('Xóa dữ liệu thành công').show();
		});
		return false;		
	},
	searchProductTab: function(){
		$('.ErrorMsgStyle').hide();
		$('#errMsg').html('').hide();
		var code = $('#productCode').val().trim();
		var name = $('#productName').val().trim();
		$('#code').focus();
		$("#grid").datagrid({
			url:FocusProgram.getProductGridUrl(code,name),
			pageNumber:1
		});
		return false;
	},
	openEasyUIDialog : function() {
		$('.ErrorMsgStyle').hide();
		$('#divDialogSearch').show();
		var html = $('#productEasyUIDialog').html();
		$('#productEasyUIDialog').dialog({  
//	        title: 'Tìm kiếm sản phẩm' +'<img id="loadingDialog" src="/resources/images/dialog/fancybox_loading.gif" style="display:none;">',  
	        closed: false,  
	        cache: false,  
	        modal: true,
	        position: 'middle',
	        center: true, 
	        onOpen: function(){
	        	FocusProgram._mapLstProduct = new Map();
	        	setTimeout(function(){
	    			CommonSearchEasyUI.fitEasyDialog();
	    		},500);
	        	Utils.bindAutoSearch();
	        	$('#productCodeDlg').focus();
	        	$('#saleTypeCodeDlg').addClass('MySelectBoxClass');
	        	$('#saleTypeCodeDlg').customStyle();
	        	$('#typeDlg').addClass('MySelectBoxClass');
	        	$('#typeDlg').customStyle();
	        	FocusProgram.searchProductTree();
	        },
	        onBeforeClose: function() {
	        },
	        onClose : function(){
	        	$('#divDialogSearch').hide();
	        	$('#productEasyUIDialog').html(html);
	        }
	    });
		return false;
	},
	searchProductTree:function(){
		$('.ErrorMsgStyle').hide();
		$('#loadingDialog').show();
		var code = $("#productCodeDlg").val().trim();
		var name = 	$("#productNameDlg").val().trim();
		var focusProgramId = $('#selId').val();
		var url = '/rest/catalog/group-po/tree/2.json' ;
		$("#tree").tree({
			url: url,  
			method:'GET',
            animate: true,
            checkbox:true,
            onBeforeLoad:function(node,param){
            	$("#tree").css({"visibility":"hidden"});
            	$("#loadding").css({"visibility":"visible"});
            	$("#errMsgDlg").html('');
            	param.code = code;
            	param.name = name;
            	param.id = focusProgramId;
            },
            onCheck:function(node,data){
            	if(node.attributes.productTreeVO.type == "PRODUCT"){
            		if(data){
            			FocusProgram._mapLstProduct.put(node.attributes.productTreeVO.id,node.attributes.productTreeVO.id);
            		}else{
            			FocusProgram._mapLstProduct.remove(node.attributes.productTreeVO.id);
            		}
            	}
            	if(node.attributes.productTreeVO.type == "SUB_CATEGORY"){
            		var sub_category = node.attributes.productTreeVO.listChildren;
            		for(var i = 0;i<sub_category.length;i++){      			
		            		if(data){
		            			FocusProgram._mapLstProduct.put(sub_category[i].id,sub_category[i].id);
		            		}else{
		            			FocusProgram._mapLstProduct.remove(sub_category[i].id);
		            		}
            		}
            	}
            	if(node.attributes.productTreeVO.type == "CATEGORY"){
            		var category = node.attributes.productTreeVO.listChildren;
            		for(var i = 0;i<category.length;i++){
	        			var sub_category = category[i].listChildren;
	        			for(var j = 0;i<sub_category.length;i++){
		            		if(data){
		            			FocusProgram._mapLstProduct.put(sub_category[i].id,sub_category[i].id);
		            		}else{
		            			FocusProgram._mapLstProduct.remove(sub_category[i].id);
		            		}
	        			}
            		}
            	}
            },
            onLoadSuccess:function(node,data){
            	$("#tree").css({"visibility":"visible"});
            	var arr  = FocusProgram._mapLstProduct.keyArray;
            	if(arr.length > 0){
            		for(var i = 0;i < arr.length; i++){
            			var nodes = $('#tree').tree('find', arr[i]);
            			if(nodes != null){
            				$("#tree").tree('check',nodes.target);
            			}
            		}
            	}
            	$("#loadding").css({"visibility":"hidden"});
            	$('#loadingDialog').hide();
            	if(data.length == 0){
            		$("#errMsgDlg").html("Không có kết quả");
            	}
            }
		});
	},
	changeProductTree:function(){
		$('.ErrorMsgStyle').hide();
		$("#errMsgDlg").html("").hide();
		var dataModel = new Object();
		var nodes = $('#tree').tree('getChecked');
		var lstId = [];  
		var arr = $('#tree').children();
		var moreCat = 0;
		arr.each(function(){
			if(!$(this).children().first().children().last().prev().hasClass('tree-checkbox0')){
				moreCat++;
			}
		});
        for(var i=0; i<nodes.length; i++){
        	if(nodes[i].attributes.productTreeVO.type != "PRODUCT"){
        		continue;
        	}
            lstId.push(nodes[i].id.split('_')[0]);  
        }
        if(FocusProgram._mapLstProduct.keyArray.length > 0){
        	for(var j = 0; j < FocusProgram._mapLstProduct.keyArray.length ; j++){
        		if(lstId.indexOf(FocusProgram._mapLstProduct.keyArray[j]) > -1){
        			continue;
        		}else{
        			lstId.push(FocusProgram._mapLstProduct.keyArray[j]);
        		}
        	}
        }
        if(lstId == '' || lstId.length == 0){
        	$("#errMsgDlg").html("Vui lòng chọn sản phẩm").show();
        	return false;
        }
        var saleTypeCode = $('#saleTypeCodeDlg').val().trim();
        var type = $('#typeDlg').val().trim();
        if(saleTypeCode == -1){
        	$("#errMsgDlg").html("Vui lòng chọn loại HTBH").show();
        	return false;
        }
        if(type == -1){
        	$("#errMsgDlg").html("Vui lòng chọn loại MHTT").show();
        	return false;
        }
        dataModel.id = $('#selId').val().trim();
        dataModel.lstProductId = lstId;
		dataModel.channelTypeId = saleTypeCode;
		dataModel.type = type;
		if(moreCat > 1){
			$.messager.confirm('Xác nhận', 'Bạn đang chọn các sản phẩm hơn 1 ngành hàng. Bạn có muốn lưu thông tin không?', function(r){
				if (r){
					Utils.saveData(dataModel, '/focus-program/product/save', FocusProgram._xhrSave, 'errMsgDlg', function(data){
						$('#successMsgProduct').html('Lưu dữ liệu thành công').show();
						$("#productEasyUIDialog").dialog("close");
						if(data.error == false){
							$("#grid").datagrid("reload");
						}
					});		
				}
			});
		}else{
			Utils.addOrSaveData(dataModel, '/focus-program/product/save', FocusProgram._xhrSave, 'errMsgDlg', function(data){
				$('#successMsgProduct').html('Lưu dữ liệu thành công').show();
				$("#productEasyUIDialog").dialog("close");
				if(data.error == false){
					$("#grid").datagrid("reload");
				}
			});
		}
		return false;
	},
//	SAN PHAM CHUONG TRINH TRONG TAM]]]]
	getShopGridUrl: function(focusId,shopCode,shopName){
		return "/focus-program/searchshop?id=" + encodeChar(focusId) + "&shopCode=" + encodeChar(shopCode) + "&shopName=" + encodeChar(shopName);
	},
	openShopDialog:function(id){
		$('.ErrorMsgStyle').hide();
		var focusId = $('#progId').val();
		var arrParam = new Array();
 		var param = new Object();
 		param.name = 'shopId';
 		if(id != null && id != undefined){
 	 		param.value = id;
 		}
 		var param1 = new Object();
 		param1.name = 'focusId';
 		param1.value = focusId;
 		arrParam.push(param);
 		arrParam.push(param1);
 		var param2 = new Object();
 		param2.name = 'stringProgram';
 		param2.value = 'focus';
 		arrParam.push(param2);
 		CommonSearch.searchShopFocusProgram(function(data){
 				//$('#promotionProgramCode').val(data.code);
 		},arrParam);
 		PromotionCatalog.bindFormat();
	},
	onCheckShop:function(id){
		 var isCheck = $('.easyui-dialog #check_' +id).attr('checked');
		    if($('.easyui-dialog #check_' +id).is(':checked')){
		    	FocusProgram._listShopId.push(id);
		    	var classParent = $('.easyui-dialog #check_' +id).attr('class');
				var flag = true;
				var count = 0;
				$('.' +classParent).each(function(){
					if($(this).is(':checked')){
						flag = false;
						count++;
					}
				});
				if(flag || count == 1){
					CommonSearch.recursionFindParentShopCheck(id);
				}
		    	//CommonSearch.recursionFindParentShopCheck(id);
			}else{
				PromotionCatalog.removeArrayByValue(FocusProgram._listShopId,id);
				var classParent = $('.easyui-dialog #check_' +id).attr('class');
				var flag = true;
				var count = 0;
				$('.' +classParent).each(function(){
					if($(this).is(':checked')){
						flag = false;
					}
				});
				if(flag || count == 1){
					CommonSearch.recursionFindParentShopUnCheck(id);
				}
				//CommonSearch.recursionFindParentShopUnCheck(id);
			}
	},
	createFocusShopMap:function(){
		$('.ErrorMsgStyle').hide();
		var params = new Object();
		params.listShopId = FocusProgram._listShopId;
		params.id = $('#progId').val();
		if(FocusProgram._listShopId.length<=0){
			$('.easyui-dialog #errMsgDialog').html('Bạn chưa chọn đơn vị để thêm').show();
			return false;
		}
		Utils.addOrSaveData(params, '/focus-program/createfocusprogram', null, 'errMsgDialog', function(data){
			if(data.error == undefined && data.errMsg == undefined){
				FocusProgram.searchShop();
				FocusProgram._listShopId = new Array();
				$('#searchStyleShopFocus1').dialog('close');
				$('#errExcelMsg').html("").hide();
				$('#successMsg2').html('Lưu dữ liệu thành công.').show();
				var tm = setTimeout(function(){
					$('#successMsg2').html('').hide();
					clearTimeout(tm); 
				}, 3000);
			} 
		}, null, null, null, "Bạn có muốn thêm đơn vị này ?", null);
	},
	deleteFocusShopMap:function(shopId){
		$('.ErrorMsgStyle').hide();
		var params = new Object();
		params.shopId = shopId;
		params.id = $('#progId').val();
		Utils.addOrSaveData(params, '/focus-program/deletefocusprogram', null, 'errExcelMsg', function(data){
			//PromotionCatalog.loadShop();
			FocusProgram.searchShop();
			$('#errExcelMsg').html("").hide();
			$('#successMsg2').html('Xóa dữ liệu thành công.').show();
			var tm = setTimeout(function(){
				$('#successMsg2').html('').hide();
				clearTimeout(tm); 
			}, 3000);
		}, null, null, null, "Bạn có muốn xóa đơn vị này ?", null);
	},
	exportExcelShopMap:function(){
		$('#divOverlay').show();
		$('#errExcelMsg').html('').hide();
	    var focusId = $('#progId').val();
		var shopCode = $('#shopCode').val().trim();
		var shopName = $('#shopName').val().trim();
		
		var data = new Object();
		data.id = focusId;
		data.shopCode = shopCode;
		data.shopName = shopName;
		
		var url = "/focus-program/exportFocusShopMap";
		CommonSearch.exportExcelData(data,url);
	},
	/*****************************	HTBH -THONGNM ****************************************/
	_mapHTBH:new Map(),
	loadHTBH : function() {
		var focusProgramId = $('#focusProgramId').val();	
		var params = new Object();
		params.focusProgramId =focusProgramId;
		if($('#permissionUser').val().trim() == 'true' && $('#statusHidden').val().trim() == activeType.WAITING) {
			FocusProgram._mapHTBH = new Map();
//			Utils.bindAutoSearch();
			$("#gridHTBH").datagrid({
				url:'/focus-program/search-htbh',
				queryParams:params,
//		        pagination : true,
		        rownumbers : true,
		        pageNumber : 1,
		        scrollbarSize: 0,
		        autoWidth: true,
		        autoRowHeight : true,
		        fitColumns : true,
				width : ($('#gridContainerHTBH').width()),
				height:'auto',
				columns:[[
				    {field: 'code', title: 'Mã HTBH', width: 150, sortable:false,resizable:false, align: 'left', formatter:function(value,row,index){
			        	  return Utils.XSSEncode(value);
			         }},
				    {field: 'name', title: 'Tên HTBH', width: 300, sortable:false,resizable:false, align: 'left', formatter:function(value,row,index){
			        	  return Utils.XSSEncode(value);
			         }},
				    {field: 'id',checkbox:true, width: 50, align: 'center',sortable:false,resizable:false},
				    {field: 'isExist',hidden:true,resizable:false,formatter:function(value,rowData,rowIndex){
				    	if(rowData.isExist == 1){ 
				    		FocusProgram._mapHTBH.put(rowData.id, rowData);
				    		if (FocusProgram._lstHTBHdb.indexOf(rowData.id) < 0) {
				    			FocusProgram._lstHTBHdb.push(rowData.id);
				    		}
				    	}
				    }}
				]],
				onCheck : function(rowIndex, rowData) {
					$('.ErrorMsgStyle').hide();
					var selectedId = rowData['id'];
					FocusProgram._mapHTBH.put(selectedId,rowData);
				},
				onUncheck : function(rowIndex, rowData) {
					$('.ErrorMsgStyle').hide();
					var selectedId = rowData['id'];
					FocusProgram._mapHTBH.remove(selectedId);
				},
				onCheckAll : function(rows) {
					$('.ErrorMsgStyle').hide();
					if($.isArray(rows)) {
						for(var i = 0; i < rows.length; i++) {
							var row = rows[i];
							var selectedId = row['id'];
							FocusProgram._mapHTBH.put(selectedId, row);
				    	}
				    }
				},
				onUncheckAll : function(rows) {
					$('.ErrorMsgStyle').hide();
					if($.isArray(rows)) {
						for(var i = 0; i < rows.length; i++) {
							var row = rows[i];
							var selectedId = row['id'];
							FocusProgram._mapHTBH.remove(selectedId);
				    	}
				    }
				},
				onLoadSuccess:function(data){
					$('#gridContainerHTBH .datagrid-header-rownumber').html('STT');
			    	updateRownumWidthForDataGrid('#gridContainerHTBH');
//					  bindAutoHideErrMsg();
			    	setTimeout(function(){
			    		$('#successMsgHTBH').html('').hide();
			    	},3000);
					$('#gridHTBH').datagrid('resize');
					var easyDiv = '#gridContainerHTBH ';
					var runner = 0;
					var isAll = 0;
					$(easyDiv+'input:checkbox[name=id]').each(function() {
						var selectedId = this.value;
						if(FocusProgram._mapHTBH.keyArray.indexOf(parseInt(selectedId)) > -1) {
							isAll++;
							$('#gridHTBH').datagrid('selectRow',runner);
						}
						runner ++;
					});
					/*var rows = data.rows;
					for (runner = 0, sz = rows.length; runner < sz; runner++) {
						if (rows[runner].isExist) {
							isAll++;
							$('#gridHTBH').datagrid('selectRow',runner);
						}
					}*/
					if (isAll > 0 && isAll == runner){
						$(easyDiv+'td[field=id] .datagrid-header-check input:checkbox').attr('checked','checked');
					}else{
						$(easyDiv+'td[field=id] .datagrid-header-check input:checkbox').removeAttr('checked');
					}
				}
			});
		} else {
			$("#gridHTBH").datagrid({
				url:'/focus-program/search-htbh',
				queryParams:params,
		        pagination : true,
		        rownumbers : true,
		        pageNumber : 1,
		        scrollbarSize: 0,
		        autoWidth: true,
		        autoRowHeight : true,
		        fitColumns : true,
				width : ($('#gridContainerHTBH').width()),
				height:'auto',
				columns:[[
				    {field: 'code', title: 'Mã HTBH', width: 150, sortable:false,resizable:false, align: 'left', formatter:function(value,row,index){
			        	  return Utils.XSSEncode(value);
			         }},
				    {field: 'name', title: 'Tên HTBH', width: 300, sortable:false,resizable:false, align: 'left', formatter:function(value,row,index){
			        	  return Utils.XSSEncode(value);
			         }}
				]],
				onLoadSuccess:function(){
					$('#gridContainerHTBH .datagrid-header-rownumber').html('STT');
			    	updateRownumWidthForDataGrid('#gridContainerHTBH');
					$('#gridHTBH').datagrid('resize');
				}
			});
		}
		return false;
	},
	searchHTBH : function () {
		$('.ErrorMsgStyle').hide();
		var params=new Object();
		params.focusProgramId = $('#focusProgramId').val().trim();
		params.code = $('#codeHTBH').val().trim();
		params.name = $('#nameHTBH').val().trim();
		$("#gridHTBH").datagrid('load',params);
		return false;
	},
	saveHTBH: function(){
		//var msg = '';
		$('.ErrorMsgStyle').hide();
		var focusProgramId = $('#focusProgramId').val();	//focusProgramId
		var mapKey = FocusProgram._mapHTBH.keyArray;
		var dataModel = new Object();	
		dataModel.focusProgramId = focusProgramId ;
		dataModel.lstApParamId =  mapKey;
		Utils.addOrSaveData(dataModel, "/focus-program/save-htbh", null, 'errMsg',function(data){
			$('#successMsgHTBH').html('Lưu dữ liệu thành công').show();
			$.getJSON('/rest/catalog/bary-centric/sale-type/list.json?id=' + $('#focusProgramId').val(), function(data){
				var arrHtml = new Array();
				arrHtml.push('<option value="-1">--- Chọn HTBH  ---</option>');
				for(var i=0;i<data.length;i++){
					arrHtml.push('<option value="' + data[i].id +'">' + data[i].saleTypeCode +'</option>');
				}
				$('#saleTypeCodeDlg').html('');
				$('#saleTypeCodeDlg').html(arrHtml.join(""));
				$('#saleTypeCodeDlg').change();
			});
			FocusProgram.getJsonListSaleType();
			$("#gridHTBH").datagrid("reload");
			FocusProgram._lstHTBHdb = [];
		});
		return false;
	}
};
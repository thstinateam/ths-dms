var KeyShop = {
	_xhrSave : null,
	_xhrDel: null,
	_mapKSSearch: null,
	_mapKSLevelSearch: null,
	_mapKSCustomerSearch: null,
	_mapKSCustomerLevelSearch: null,
	_widthProductCat: 250,
	_isLoadGridProduct: 1,
	_isLoadGridLevel: 1,
	_isLoadGridReward:1,
	_isLoadGridShop:1,
	_rowIndex:-1,
	_maxLevel: 0,
	_arrShopInOrgAccessCheked: null,
	_arrShopInOrgAccess: null,
	_processType: null,
	_phCheckedMap: null,
	_searchKSParam: null,
	_searchKSCustomerParam: null,
	_searchKSRewardParam: null,
	_isDoneLoadNum: false,
	getUrlSearchKS:function(){
		var obj = new Object();
		obj.shopId = $('#shopId').val().trim();
		obj.code = $('#code').val().trim();
		obj.year = $('#year').val().trim();
		if (KeyShop._isDoneLoadNum) {
			obj.num = $('#num').combobox('getValue').trim();
		} else {
			obj.num = $('#currentNum').val();
		}		
		obj.status = $('#status').val();
		obj.ksType = $('#ksType').val();
		KeyShop._searchKSParam = obj;
		return '/key-shop/searchKS?' + $.param(obj, false);
	},
	getUrlSearchKSCustomer:function(){
		
		var obj = new Object();
		obj.ksId = $('#ksIdCurrent').val();
		obj.shopId = $('#shopId').val().trim();
		obj.code = $('#customerMix').val().trim();
		obj.year = $('#yearBottom').val().trim();
		obj.num = $('#numBottom').combobox('getValue').trim();
		obj.status = $('#statusBottom').val();
		KeyShop._searchKSCustomerParam = obj;
		return '/key-shop/searchKSCustomer?' + $.param(obj, true);
	},
	getUrlSearchKSReward:function(){		
		var obj = new Object();
		obj.ksId = $('#ksIdCurrent').val();
		obj.shopId = $('#shopId').val().trim();
		obj.code = $('#customerReward').val().trim();
		obj.year = $('#yearReward').val().trim();
		obj.num = $('#numReward').combobox('getValue').trim();
		obj.status = $('#statusReward').val();
		obj.resultReward = $('#resultReward').val();
		KeyShop._searchKSRewardParam = obj;
		return '/key-shop/searchKSReward?' + $.param(obj, true);
	},
	getUrlSearchKSProduct:function(){
		var obj = new Object();
		obj.code = $('#productCode').val().trim();
		obj.ksId = $('#ksIdCurrent').val();
		if ($('#category').val() != null) {
			obj.lstProductInfoId = $('#category').val();
		}
		if ($('#categoryChild').val() != null) {
			obj.lstProductInfoChildId = $('#categoryChild').val();
		}
		return '/key-shop/searchKSProduct?' + $.param(obj, true);
	},
	getUrlSearchKSLevel:function(){
		var obj = new Object();
		obj.ksId = $('#ksIdCurrent').val();
		return '/key-shop/searchKSLevel?' + $.param(obj, true);
	},
	getUrlSearchKSCustomerLevel:function(){
		var numPopup = $('#numPopup').combobox('getValue');
		if ($('#numPopup').combobox('getValue')<10) numPopup='0'+$('#numPopup').combobox('getValue');
		else numPopup=$('#numPopup').combobox('getValue');
		var cycleIdPopup = $('#yearPopup').val() +''+ numPopup+''+numPopup;
		var obj = new Object();
		obj.ksId = $('#ksIdCurrent').val();
		obj.customerId = $('#customerId').val();
		obj.cycleId=cycleIdPopup;
		return '/key-shop/searchKSCustomerLevel?' + $.param(obj, true);
	},
	searchKS:function() {
		$('#errMsg').html('').hide();
		var url = KeyShop.getUrlSearchKS();
		$("#gridKS").datagrid({url:url,pageNumber:1});
	},
	lockReward:function(){
		var dataModel = new Object();
		var lstKsCustomerId = new Array();
		var lockStatus = 1;
		var obj = KeyShop._phCheckedMap.valArray;
		for(var i=0;i<obj.length;i++){
			lstKsCustomerId.push(obj[i]["id"]);			
		}
		dataModel.lstKsCustomerId = lstKsCustomerId;
		dataModel.lockStatus = lockStatus;
		Utils.addOrSaveData(dataModel, "/key-shop/lock-customer-reward", null, 'errMsgKeyShop', function(data) {			
			if (data.error) {				
				$('#errMsgKeyShop').html(data.errMsg.replace(/\|/g, '<br>')).show();				
			}else {
				$('#successMsgKeyShop').html('Khóa trả thưởng thành công').change().show();				
				setTimeout(function(){
					$('#successMsg').html('').change().show();
					$('#gridReward').datagrid('reload');
					KeyShop._phCheckedMap.clear();
				}, 700);
			  }
			
			}, null, null, null, null, function(data) {
				if (data.error) {				
					$('#errMsgKeyShop').html(data.errMsg.replace(/\|/g, '<br>')).show();
					$('#gridReward').datagrid('reload');
					KeyShop._phCheckedMap.clear();
				}
			});		
		return false;		
	},
	unlockReward:function(){
		var dataModel = new Object();
		var lstKsCustomerId = new Array();
		var lockStatus = 2;
		var obj = KeyShop._phCheckedMap.valArray;
		for(var i=0;i<obj.length;i++){
			lstKsCustomerId.push(obj[i]["id"]);			
		}
		//dataModel.startDate = $('#startDate').val();
		dataModel.lstKsCustomerId = lstKsCustomerId;
		dataModel.lockStatus = lockStatus;
		Utils.addOrSaveData(dataModel, "/key-shop/lock-customer-reward", null, 'errMsgKeyShop', function(data) {			
			if (data.error) {				
				$('#errMsgKeyShop').html(data.errMsg.replace(/\|/g, '<br>')).show();				
			}else {
				$('#successMsgKeyShop').html('Khóa trả thưởng thành công').change().show();				
				setTimeout(function(){
					$('#successMsg').html('').change().show();	
					$('#gridReward').datagrid('reload');
					KeyShop._phCheckedMap.clear();
				}, 700);
			  }
			
			}, null, null, null, null, function(data) {
				if (data.error) {				
					$('#errMsgKeyShop').html(data.errMsg.replace(/\|/g, '<br>')).show();
					$('#gridreward').datagrid('reload');
					KeyShop._phCheckedMap.clear();
				}
			});		
		return false;		
	},
	unlockReward:function(){
		var dataModel = new Object();
		var lstKsCustomerId = new Array();
		var lockStatus = 2;
		var obj = KeyShop._phCheckedMap.valArray;
		for(var i=0;i<obj.length;i++){
			lstKsCustomerId.push(obj[i]["id"]);			
		}
		//dataModel.startDate = $('#startDate').val();
		dataModel.lstKsCustomerId = lstKsCustomerId;
		dataModel.lockStatus = lockStatus;
		Utils.addOrSaveData(dataModel, "/key-shop/lock-customer-reward", null, 'errMsgKeyShop', function(data) {			
			if (data.error) {				
				$('#errMsgKeyShop').html(data.errMsg.replace(/\|/g, '<br>')).show();				
			} else {
				$('#successMsgKeyShop').html('Mở khóa trả thưởng thành công').change().show();				
				setTimeout(function(){
					$('#successMsgKeyShop').html('').change().show();	
					$('#gridReward').datagrid('reload');
					KeyShop._phCheckedMap.clear();
				}, 700);
			  }
			
			}, null, null, null, null, function(data) {
				if (data.error) {				
					$('#errMsgKeyShop').html(data.errMsg.replace(/\|/g, '<br>')).show();
					$('#gridReward').datagrid('reload');
					KeyShop._phCheckedMap.clear();
				}
			});		
		return false;		
	},
	initPopupEditKS:function(ks) {
		enable('ksCodeKS');
		$('#ksIdKS').val('');
		$('#ksCodeKS').val('');
		$('#ksNameKS').val('');
		$('#minPhotoKS').val('');
		$('#descriptionKS').val('');
		if (ks != undefined && ks != null) {
			disabled('ksCodeKS');
			$('#ksIdKS').val(ks.ksId);
			$('#ksCodeKS').val(Utils.XSSEncode(ks.ksCode));
			$('#ksNameKS').val(Utils.XSSEncode(ks.ksName));
			$('#ksTypeKS').val(ks.ksType).change();
			$('#fromCycleKS').val(ks.fromCycleId).change();
			$('#toCycleKS').val(ks.toCycleId).change();
			$('#minPhotoKS').val(ks.minPhoto);
			$('#descriptionKS').val(Utils.XSSEncode(ks.description));
			$('#statusKS').val(ks.status).change();
		}
	},
	openPopupEditKS:function(id) {
		$('#errMsgKS, #successMsgKS').hide();
		var ks = KeyShop._mapKSSearch.get(id);
		var html = $('#addOrEditKSDiv').html();
		KeyShop.initPopupEditKS(ks);
		$('#addOrEditKS').dialog({
			title : 'Thông tin chương trình HTTM',
			width:800,
			height:'auto',
			onOpen: function(){
				if ($('#ksCodeKS').val() != '') {
					$('#ksNameKS').focus();
				} else {
					$('#ksCodeKS').focus();
				}
			},
			onClose: function(){
				$('#addOrEditKSDiv').html(html);
			}
		});
		$('#addOrEditKS').dialog('open');
	},
	editKS:function() {
		$('#errMsgKS, #successMsgKS').hide();
		var msg = '';
		if (msg.length == 0) {
			msg = Utils.getMessageOfRequireCheck('ksCodeKS', 'Mã CT');
		}
		if (msg.length == 0) {
			msg = Utils.getMessageOfSpecialCharactersValidate('ksCodeKS', 'Mã CT');
		}
		if (msg.length == 0) {
			msg = Utils.getMessageOfRequireCheck('ksNameKS', 'Tên CT');
		}
		if (msg.length == 0) {
			msg = Utils.getMessageOfSpecialCharactersValidate('ksNameKS', 'Tên CT', Utils._SPECIAL);
		}
		if (msg.length == 0) {
			msg = Utils.getMessageOfSpecialCharactersValidate('descriptionKS', 'Mô tả', Utils._SPECIAL);
		}
		if (msg.length > 0) {
			$('#errMsgKS').html(msg).show();
			return;
		}
		var dataModel = new Object();
		dataModel.ksId = $('#ksIdKS').val();
		dataModel.ksCode = $('#ksCodeKS').val().trim();
		dataModel.ksName = $('#ksNameKS').val().trim();
		dataModel.ksType = $('#ksTypeKS').val();
		dataModel.fromCycleId = $('#fromCycleKS').val();
		dataModel.toCycleId = $('#toCycleKS').val();
		dataModel.minPhoto = $('#minPhotoKS').val().trim();
		dataModel.description = $('#descriptionKS').val().trim();
		dataModel.status = $('#statusKS').val();
		Utils.addOrSaveData(dataModel, "/key-shop/editKS", null, 'errMsgKS', function(data) {
			$('#successMsgKS').html('Lưu dữ liệu thành công').show();
			KeyShop.searchKS();
			setTimeout(function() {
				$('.easyui-dialog').dialog('close');
			}, 2000);
		});
		return false;
	},
	initPopupEditKSLevel:function(obj) {
		enable('levelCode');
		$('#levelId').val('');
		$('#levelCode').val('');
		$('#levelName').val('');
		$('#amountLevel').val('');
		$('#quantityLevel').val('');
		if (obj != undefined && obj != null) {
			disabled('levelCode');
			$('#levelId').val(obj.ksLevelId);
			$('#levelCode').val(obj.ksLevelCode);
			$('#levelName').val(obj.ksLevelName);
			$('#amountLevel').val(formatCurrency(obj.amount));
			$('#quantityLevel').val(formatCurrency(obj.quantity));
		}
	},
	openPopupEditKSLevel:function(levelId) {
		$('#errMsgKSLevel, #successMsgKSLevel').hide();
		var kl = KeyShop._mapKSLevelSearch.get(levelId);
		var html = $('#addOrEditKSLevelDiv').html();
		KeyShop.initPopupEditKSLevel(kl);
		$('#addOrEditKSLevel').dialog({
			title : 'Thông tin mức chương trình',
			width:500,
			height:'auto',
			onOpen: function(){
				if ($('#levelCode').val() != '') {
					$('#levelName').focus();
				} else {
					$('#levelCode').focus();
				}
			},
			onClose: function(){
				$('#addOrEditKSLevelDiv').html(html);
			}
		});
		$('#addOrEditKSLevel').dialog('open');
	},
	initPopupEditKSCustomer:function(kc) {
		$('#errMsgKSCustomerLevel').html('').hide();
		if (kc==null) {
			enable('customerCode');
			enable('statusPopup');
			enable('yearPopup');
			enable('numPopup');
			$('#lblTrangThai').hide();
			$('#statusPopupDiv').hide();
			$('#customerCode').val('');
			$('#customerId').val('');
			$('#group_dt_kh_d_tc_btnApproveCustomerLevel').hide();
			$('#group_dt_kh_d_tc_btnDenyCustomerLevel').hide();
			$('#group_edit_ks_btnUpdateCustomerLevel').show();
			
			var yearBottom = $('#yearBottom').val();
			if(!isNullOrEmpty(yearBottom)) {
				$('#yearPopup').val(yearBottom).change();
			}
			
//			var numBottom = $('#numBottom').combobox('getValue');
//			if(!isNullOrEmpty(numBottom)) {
//				$('#numPopup').combobox('setValue', numBottom);
//			}
			
//			$('#numPopup').combobox('setValue', $('#numBottom').combobox('getValue'));
//			$('#yearPopup').val($('#yearBottom').val()).change();
//			$('#numPopup').val($('#numBottom').combobox('getValue')).change();
		} else {
			disabled('customerCode');
			$('#lblTrangThai').show();
			$('#statusPopupDiv').show();
			$('#statusPopup').val(kc.status);
			$('#statusPopup').change();
			disabled('statusPopup');
			disabled('yearPopup');
			disabled('numPopup');
			$('#group_dt_kh_d_tc_btnApproveCustomerLevel').hide();
			$('#group_dt_kh_d_tc_btnDenyCustomerLevel').hide();
			$('#group_edit_ks_btnUpdateCustomerLevel').show();
			
			if (kc.rewardType!=null) {
				$('#group_dt_kh_d_tc_btnApproveCustomerLevel').hide();
				$('#group_dt_kh_d_tc_btnDenyCustomerLevel').hide();
				$('#group_edit_ks_btnUpdateCustomerLevel').hide();
			}
			if (kc.cycleId!=null) {
				$('#yearPopup').val(Number(kc.cycleId.toString().substring(0, 4))).change();
//				$('#numPopup').combobox('setValue', Number(kc.cycleId.toString().substring(6, 8)));
			} else {
//				$('#numPopup').combobox('setValue', $('#numBottom').combobox('getValue'));
//				$('#yearPopup').val($('#yearBottom').val()).change();
				var yearBottom = $('#yearBottom').val();
				if(!isNullOrEmpty(yearBottom)) {
					$('#yearPopup').val(yearBottom).change();
				}
				
//				var numBottom = $('#numBottom').combobox('getValue');
//				if(!isNullOrEmpty(numBottom)) {
//					$('#numPopup').combobox('setValue', numBottom);
//				}
			}
//			var ksId = $('#ksIdCurrent').val().trim();
//			KeyShop.loadGridCustomerLevel(ksId);
		} 
	},
	openPopupEditKSCustomer:function(customerId) { // customerId, customerCode, rewardType
		
		$('#errMsgKSLevel, #successMsgKSCustomer').hide();
		$('#addressPop').html('');
		var ksId = $('#ksIdCurrent').val().trim();
		var ks = KeyShop._mapKSSearch.get(ksId);
		KeyShop.loadKsYearEditCustomer(ks);
		$('#ksCustomerId').val(customerId);
		if (customerId!=null) {
			var kc = KeyShop._mapKSCustomerSearch.get(customerId);
			if (kc!=null) {
				$('#customerCode').val(kc.customerCode);
				$('#customerId').val(kc.customerId);
				$('#addressPop').html(kc.address);
//				var html = $('#addOrEditKSLevelCustomer').html();
				KeyShop.initPopupEditKSCustomer(kc);
			}
		} else {
			$('#customerCode').val('');
			$('#customerId').val('');
			$('#addressPop').val('');
			KeyShop.initPopupEditKSCustomer();
		}
		
//		KeyShop.loadGridCustomerLevel(ksId);
		$('#addOrEditKSCustomer').dialog({
			title : 'Thông tin khách hàng',
			width:900,
			height:'auto',
			onOpen: function() {
				if ($('#customerCode').val() != '') {
					//$('#customerName').focus();
				} else {
					$('#customerCode').focus();
				}
			},
			onClose: function() {
//				$('#addOrEditKSCustomerDiv').html(html);
			}
		});
		$('#addOrEditKSCustomer').dialog('open');
	},
	loadKsYearEditCustomer:function(ks) {
		$('#yearPopup').html('');
		var list = ks.lstYear;
		if (list != undefined && list != null && list.length > 0) {
			for (var i = 0; i < list.length; i++) {
				$('#yearPopup').append('<option value = "' + Utils.XSSEncode(list[i]) + '">' + Utils.XSSEncode(list[i]) + '</option>');  
			}
		}
//		$('#yearPopup').change();
	},
	editKSLevel:function() {
		$('#errMsgKSLevel, #successMsgKSLevel').hide();
		var msg = '';
		if (msg.length == 0) {
			msg = Utils.getMessageOfRequireCheck('levelCode', 'Mã mức');
		}
		if (msg.length == 0) {
			msg = Utils.getMessageOfSpecialCharactersValidate('levelCode', 'Mã mức');
		}
		if (msg.length == 0) {
			msg = Utils.getMessageOfRequireCheck('levelName', 'Tên mức');
		}
		if (msg.length == 0) {
			msg = Utils.getMessageOfSpecialCharactersValidate('levelName', 'Tên mức', Utils._SPECIAL);
		}
		if (msg.length == 0) {
			msg = Utils.getMessageOfRequireCheck('amountLevel', 'Số tiền');
		}
		if (msg.length > 0) {
			$('#errMsgKSLevel').html(msg).show();
			return;
		}
		var dataModel = new Object();
		dataModel.ksId = $('#ksIdCurrent').val()
		dataModel.ksLevelId = $('#levelId').val();
		dataModel.ksLevelCode = $("#levelCode").val().trim();
		dataModel.ksLevelName = $('#levelName').val().trim();
		dataModel.amount = $('#amountLevel').val().trim();
		dataModel.quantity = $('#quantityLevel').val().trim();
		Utils.addOrSaveData(dataModel, "/key-shop/editKSLevel", null, 'errMsgKSLevel', function(data) {
			$('#successMsgKSLevel').html('Lưu dữ liệu thành công').show();
			KeyShop.searchKSLevel();
			setTimeout(function() {
				$('.easyui-dialog').dialog('close');
			}, 2000);
		});
		return false;
	},
	deleteKSLevel:function(levelId){
		var dataModel = new Object();
		dataModel.ksLevelId = levelId;
		Utils.addOrSaveData(dataModel, "/key-shop/deleteKSLevel", null, 'errMsgKeyShop', function(data) {
			KeyShop.searchKSLevel();
			setTimeout(function() {
				$('.easyui-dialog').dialog('close');
			}, 2000);
		},null, null, true);
	},
	hideAllDiv:function() {
		$('#productDiv').hide();
		$('#levelDiv').hide();
		$('#shopDiv').hide();
		$('#customerDiv').hide();
		$('#rewardDiv').hide();
		$('#errMsgKeyShop').html('').hide();
	},
	showProductGrid:function(ksId) {
		KeyShop.hideAllDiv();
		var ks = KeyShop._mapKSSearch.get(ksId);
		if (ks == null) return;
		$('#productDiv').show();
		$('#productTitle').html('Sản phẩm của ' + Utils.XSSEncode(ks.ksName));
		$('#ksIdCurrent').val(ksId);
		if(KeyShop._isLoadGridProduct != 1){
			var url = KeyShop.getUrlSearchKSProduct()
			$("#gridProduct").datagrid({url:url,pageNumber:1});
		}else{
			KeyShop.fillCategoriesToMultiSelect();
			KeyShop._isLoadGridProduct = 0;
			KeyShop.loadGridProduct(ksId);
		}
	},
	showLevelGrid:function(ksId) {
		KeyShop.hideAllDiv();
		var ks = KeyShop._mapKSSearch.get(ksId);
		if (ks == null) return;
		$('#levelDiv').show();
		$('#levelTitle').html('Các mức của ' + Utils.XSSEncode(ks.ksName));
		$('#ksIdCurrent').val(ksId);
		if(KeyShop._isLoadGridLevel != 1){
			KeyShop.searchKSLevel();
		}else{
			KeyShop._isLoadGridLevel = 0;
			KeyShop.loadGridLevel(ksId);
		}
	},
	showCustomerGrid:function(ksId) {
		$('#dgGridContainerCustomer').html('<table id="gridCustomer"></table>');
		KeyShop.hideAllDiv();
		var ks = KeyShop._mapKSSearch.get(ksId);
		if (ks == null) return;
		$('#customerDiv').show();
		$('#customerTitle').html('KHÁCH HÀNG ĐĂNG KÝ THAM GIA ' + Utils.XSSEncode(ks.ksName));
		$('#ksIdCurrent').val(ksId);
		ReportUtils.loadComboTree('shopBottom', 'shopId', $('#curShopId').val(), function(shopId){});
		KeyShop.loadKsYear(ks);
		KeyShop.loadGridCustomer(ksId);
	},
	loadKsYear:function(ks) {
		$('#yearBottom').html('');
		var list = ks.lstYear;
		if (list != undefined && list != null && list.length > 0) {
			for (var i = 0; i < list.length; i++) {
				$('#yearBottom').append('<option value = "' + Utils.XSSEncode(list[i]) + '">' + Utils.XSSEncode(list[i]) + '</option>');  
			}
		}
		$('#yearBottom').change();
	},
	showRewardGrid:function(ksId) {
		KeyShop.hideAllDiv();
		var ks = KeyShop._mapKSSearch.get(ksId);
		if (ks == null) return;
		$('#rewardDiv').show();
		$('#rewardTitle').html('THÔNG TIN TRẢ THƯỞNG KHÁCH HÀNG ' + Utils.XSSEncode(ks.ksName));
		$('#ksIdCurrent').val(ksId);
		ReportUtils.loadComboTree('shopReward', 'shopId', $('#curShopId').val(), function(shopId){});
		KeyShop._phCheckedMap = new Map();
		KeyShop.loadGridReward(ksId);
	},
	searchKSProduct:function(){
		var url = KeyShop.getUrlSearchKSProduct();
		$("#gridProduct").datagrid({url:url,pageNumber:1});
	},
	searchKSLevel:function() {
		var url = KeyShop.getUrlSearchKSLevel()
		$("#gridLevel").datagrid({url:url,pageNumber:1});
	},
	searchKSCustomer:function() {
		$('#errMsgKeyShop').html('').hide();
		var url = KeyShop.getUrlSearchKSCustomer();
		$("#gridCustomer").datagrid({url:url,pageNumber:1});
	},
	searchKSReward:function() {
		$('#errMsgKeyShop').html('').hide();
		var url = KeyShop.getUrlSearchKSReward()
		$("#gridReward").datagrid({url:url,pageNumber:1});
	},
	loadGridProduct:function(ksId) {
		var title = '<a title="Thêm mới sản phẩm" href="javascript:void(0)" onclick=\"return KeyShop.openPopupKSProduct();\"><img src="/resources/images/icon_add.png"/></a>';
		$('#gridProduct').datagrid({
			url: KeyShop.getUrlSearchKSProduct(),
			width : $('#dgGridContainerProduct').width() - 17,
			height: 'auto',
			singleSelect: true,
			rownumbers: true,
			fitColumns: true,
			scrollbarSize: 0,
			pagination: true,
			pageList  : [10,20,30],
			columns:[[  
			          {field:'productCode',title:'Mã sản phẩm',width:70, sortable : true, resizable : false, align: 'left', formatter:function(value,row,index){
			        	  return Utils.XSSEncode(value);
			          }},
			          {field:'productName',title:'Tên sản phẩm',width:150, sortable : true, resizable : false, align: 'left', formatter:function(value,row,index){
			        	  return Utils.XSSEncode(value);
			          }},
			          {field:'cat',title:'Ngành hàng',width:70, sortable : true, resizable : false, align: 'left', formatter:function(value,row,index){
			        	  return Utils.XSSEncode(value);
			          }},
			          {field:'subCat',title:'Ngành hàng con',width:70, sortable : true, resizable : false, align: 'left', formatter:function(value,row,index){
			        	  return Utils.XSSEncode(value);
			          }},
			          {field:'brand',title:'Nhãn hàng',width:70, sortable : true, resizable : false, align: 'left', formatter:function(value,row,index){
			        	  return Utils.XSSEncode(value);
			          }},
			          {field:'flavour',title:'Hương vị',width:70, sortable : true, resizable : false, align: 'left', formatter:function(value,row,index){
			        	  return Utils.XSSEncode(value);
			          }},
			          {field:'packing',title:'Bao bì',width:70, sortable : true, resizable : false, align: 'left', formatter:function(value,row,index){
			        	  return Utils.XSSEncode(value);
			          }},
			          {field:'edit',title:title, width: 30,sortable:false,resizable:false, align: 'center', formatter: function(value, row, index) {
			        	  return "<a title='Xóa' href='javascript:void(0)' onclick=\"return KeyShop.deleteProduct('"+ row.productId +"');\"><img height='17' src='/resources/images/icon-delete.png'></a>";
			          }},
			]],
		  	onLoadSuccess :function(data){	    	
		    	$('.datagrid-header-rownumber').html('STT');
		    	var arrDelete =  $('#dgGridContainerProduct td[field="edit"]');
				if (arrDelete != undefined && arrDelete != null && arrDelete.length > 0) {
				  for (var i = 0, size = arrDelete.length; i < size; i++) {
				    $(arrDelete[i]).prop("id", "group_edit_ks_gr_sp_cl_edit_" + i);//Khai bao id danh cho phan quyen
					$(arrDelete[i]).addClass("cmsiscontrol");
				  }
				}
				Utils.functionAccessFillControl('dgGridContainerProduct', function(data){
					//Xu ly cac su kien lien quan den cac control phan quyen
					var arrTmpLength =  $('#dgGridContainerProduct td[id^="group_edit_ks_gr_sp_cl_edit_"]').length;
					var invisibleLenght = $('.isCMSInvisible[id^="group_edit_ks_gr_sp_cl_edit_"]').length;
					if (arrTmpLength > 0 && arrTmpLength != invisibleLenght) {
						$('#gridProduct').datagrid("showColumn", "edit");
					} else {
						$('#gridProduct').datagrid("hideColumn", "edit");
					}
				});
		    },
		    method : 'GET'
		});
	},
	loadGridLevel:function(ksId) {
		var title = '<a title="Thêm mới mức" href="javascript:void(0)" onclick=\"return KeyShop.openPopupEditKSLevel();\"><img src="/resources/images/icon_add.png"/></a>';
		$('#gridLevel').datagrid({
			url: KeyShop.getUrlSearchKSLevel(),
			width : $('#dgGridContainerLevel').width() - 17,
			height: 'auto',
			singleSelect: true,
			rownumbers: true,
			fitColumns: true,
			scrollbarSize: 0,
			pagination: true,
			pageList  : [10,20,30],
			columns:[[  
			          {field:'ksLevelCode',title:'Mã mức',width:70, sortable : true, resizable : false, align: 'left', formatter:function(value,row,index){
			        	  return Utils.XSSEncode(value);
			          }},
			          {field:'ksLevelName',title:'Tên mức',width:150, sortable : true, resizable : false, align: 'left', formatter:function(value,row,index){
			        	  return Utils.XSSEncode(value);
			          }},
			          {field:'amount',title:'Số tiền',width:70, sortable : true, resizable : false, align: 'left', formatter:function(value,row,index){
			        	  return Utils.XSSEncode(formatCurrency(value));
			          }},
			          {field:'quantity',title:'Số lượng',width:70, sortable : true, resizable : false, align: 'left', formatter:function(value,row,index){
			        	  return Utils.XSSEncode(formatCurrency(value));
			          }},
			          {field:'edit',title:title, width: 30,sortable:false,resizable:false, align: 'center', formatter: function(value, row, index) {
			        	  var str = "<a title='Sửa' href='javascript:void(0)' onclick=\"return KeyShop.openPopupEditKSLevel('"+ row.ksLevelId +"');\"><img height='17' src='/resources/images/icon-edit.png'></a>";
			        	  str += "&nbsp&nbsp&nbsp&nbsp";
			        	  str += "<a title='Xóa' href='javascript:void(0)' onclick=\"return KeyShop.deleteKSLevel('"+ row.ksLevelId +"');\"><img height='17' src='/resources/images/icon-delete.png'></a>";
			        	  return str;
			          }},
			]],
		  	onLoadSuccess :function(data){	    	
		    	$('.datagrid-header-rownumber').html('STT');
		    	KeyShop._mapKSLevelSearch = new Map();
		    	for (var i = 0 ; i < data.rows.length ; i++) {
		    		KeyShop._mapKSLevelSearch.put(data.rows[i].ksLevelId, data.rows[i]);
		    	}
		    	var arrDelete =  $('#dgGridContainerLevel td[field="edit"]');
				if (arrDelete != undefined && arrDelete != null && arrDelete.length > 0) {
				  for (var i = 0, size = arrDelete.length; i < size; i++) {
				    $(arrDelete[i]).prop("id", "group_edit_ks_gr_muc_cl_edit_" + i);//Khai bao id danh cho phan quyen
					$(arrDelete[i]).addClass("cmsiscontrol");
				  }
				}
				Utils.functionAccessFillControl('dgGridContainerLevel', function(data){
					//Xu ly cac su kien lien quan den cac control phan quyen
					var arrTmpLength =  $('#dgGridContainerLevel td[id^="group_edit_ks_gr_muc_cl_edit_"]').length;
					var invisibleLenght = $('.isCMSInvisible[id^="group_edit_ks_gr_muc_cl_edit_"]').length;
					if (arrTmpLength > 0 && arrTmpLength != invisibleLenght) {
						$('#gridLevel').datagrid("showColumn", "edit");
					} else {
						$('#gridLevel').datagrid("hideColumn", "edit");
					}
				});
		    },
		    method : 'GET'
		});
	},
	loadGridReward:function(ksId) {			
			$('#gridReward').datagrid({
			url: KeyShop.getUrlSearchKSReward(),
			width : $('#dgGridContainerReaward').width() - 17,
			height: 'auto',
			checkOnSelect :true,
			selectOnCheck: true,
			//singleSelect: true,
			rownumbers: true,
			//fitColumns: true,
			scrollbarSize: 0,
			pagination: true,
			pageList  : [10,20,30],
			frozenColumns:[[
						{field: 'id', index:'id', hidden: true},		
						{field: 'customerId', checkbox: true},
						{field:'shopCode',title:'NPP',width:150, sortable : true, resizable : false, align: 'left', formatter:function(value,row,index){
							  return Utils.XSSEncode(row.shopCode) + ' - ' + Utils.XSSEncode(row.shopName);
						}},
						{field:'customerCode',title:'Mã KH',width:120, sortable : true, resizable : false, align: 'left', formatter:function(value,row,index){
							  return Utils.XSSEncode(value);
						}},
						{field:'customerName',title:'Tên KH',width:150, sortable : true, resizable : false, align: 'left', formatter:function(value,row,index){
							  return Utils.XSSEncode(value);
						}},       
			]],
			columns:[[  
			         
			          {field:'address',title:'Địa chỉ',width:280, sortable : true, resizable : false, align: 'left', formatter:function(value,row,index){
			        	  return Utils.XSSEncode(value);
			          }},
			          {field:'levelCustomer',title:'Mức đăng ký',width:200, sortable : true, resizable : false, align: 'left', formatter:function(value,row,index){
			        	  return Utils.XSSEncode(value);
			          }},
			          {field:'amountTarget',title:'DS kế hoạch',width:100, sortable : true, resizable : false, align: 'right', formatter:function(value,row,index){
			        	  return Utils.XSSEncode(formatCurrency(value));
			          }},
			          {field:'amount',title:'DS đạt',width:100, sortable : true, resizable : false, align: 'right', formatter:function(value,row,index){
			        	  return Utils.XSSEncode(formatCurrency(value));
			          }},
			          {field:'percentAmount',title:'%',width:80, sortable : true, resizable : false, align: 'right', formatter:function(value,row,index){
			        	  return Utils.XSSEncode(value+'%');
			          }},
			          {field:'quantityTarget',title:'SL kế hoạch',width:100, sortable : true, resizable : false, align: 'right', formatter:function(value,row,index){
			        	  return Utils.XSSEncode(formatCurrency(value));
			          }},
			          {field:'quantity',title:'SL đạt',width:100, sortable : true, resizable : false, align: 'right', formatter:function(value,row,index){
			        	  return Utils.XSSEncode(formatCurrency(value));
			          }},
			          {field:'percentQuantity',title:'%',width:80, sortable : true, resizable : false, align: 'right', formatter:function(value,row,index){
			        	  return Utils.XSSEncode(value+'%');
			          }},
			          {field:'result',title:'Kết quả',width:80, sortable: true, align: 'center',resizable:false , formatter:function(value, row, index) {
			        	  if (value == 5) {
			        		  return 'Không đạt';
			        	  } else if (value == 6) {
			        		  return 'Đạt';
			        	  }
			        	  return '';
			          }},
			          {field:'rewardType',title:'Trạng thái trả thưởng',width:150, sortable: true, align: 'center',resizable:false , formatter:function(value, row, index) {
			        	  if (value == 0) {
			        		  return 'Chuyển khoản';
			        	  } else if (value == 1) {
			        		  return 'Chưa mở khóa trả thưởng';
			        	  }else if (value == 2) {
			        		  return 'Đã mở khóa trả thưởng';
			        	  }else if (value == 3) {
			        		  return 'Đã trả 1 phần';
			        	  }else if (value == 4) {
			        		  return 'Đã trả toàn bộ';
			        	  }else if (value == null) {
			        		  return 'Chưa import trả thưởng';
			        	  }
			        	  return '';
			          }},
			          {field:'rewardType1',title:'Loại trả thưởng',width:100, sortable: true, align: 'center',resizable:false , formatter:function(value, row, index) {
			        	  if (row.rewardType == 0) {
			        		  return 'Chuyển khoản';
			        	  } else if (row.rewardType == 1 || row.rewardType == 2 || row.rewardType == 3 || row.rewardType == 4 ) {
			        		  return 'Coupon';
			        	  }
			        	  return '';
			          }},
			          {field:'totalReward',title:'Số tiền trả thưởng',width:100, sortable : true, resizable : false, align: 'right', formatter:function(value,row,index){
			        	  return Utils.XSSEncode(formatCurrency(value));
			          }},
			          {field:'totalRewardDone',title:'Số tiền đã trả',width:100, sortable : true, resizable : false, align: 'right', formatter:function(value,row,index){
			        	  return Utils.XSSEncode(formatCurrency(value));
			          }},
			          {field:'payReward',title:'Còn lại',width:100, sortable : true, resizable : false, align: 'right', formatter:function(value,row,index){
			        	  return Utils.XSSEncode(formatCurrency(value));
			          }},
			          {field:'productReward',title:'Mặt hàng trả thưởng',width:240, sortable : true, resizable : false, align: 'left', formatter:function(value,row,index){
			        	  return Utils.XSSEncode(value);
			          }},
			          {field:'productRewardDone',title:'Mặt hàng đã trả thưởng',width:240, sortable : true, resizable : false, align: 'left', formatter:function(value,row,index){
			        	  return Utils.XSSEncode(value);
			          }},
			          {field:'amountDiscount',title:'Chiết khấu doanh số',width:100, sortable : true, resizable : false, align: 'right', formatter:function(value,row,index){
			        	  return Utils.XSSEncode(formatCurrency(value));
			          }},
			          {field:'overAmountDiscount',title:'Chiết khấu vượt doanh số',width:100, sortable : true, resizable : false, align: 'right', formatter:function(value,row,index){
			        	  return Utils.XSSEncode(formatCurrency(value));
			          }},
			          {field:'displayMoney',title:'Tiền trưng bày',width:100, sortable : true, resizable : false, align: 'right', formatter:function(value,row,index){
			        	  return Utils.XSSEncode(formatCurrency(value));
			          }},
			          {field:'supportMoney',title:'Tiền hỗ trợ',width:100, sortable : true, resizable : false, align: 'right', formatter:function(value,row,index){
			        	  return Utils.XSSEncode(formatCurrency(value));
			          }},
			          {field:'oddDiscount',title:'Trừ chiết khấu phủ lẻ',width:100, sortable : true, resizable : false, align: 'right', formatter:function(value,row,index){
			        	  return Utils.XSSEncode(formatCurrency(value));
			          }},
			          {field:'addMoney',title:'Tiền thưởng bổ sung',width:100, sortable : true, resizable : false, align: 'right', formatter:function(value,row,index){
			        	  return Utils.XSSEncode(formatCurrency(value));
			          }},
			]],
		  	onLoadSuccess :function(data){
		  		$('td[field=customerId] div.datagrid-header-check input[type=checkbox]').attr('checked',false);
		  		KeyShop._phCheckedMap.clear();
		    	$('.datagrid-header-rownumber').html('STT');
		    	if (data != undefined && data != null && data.rows.length > 0) {
		    		enable('btnExportReward');
		    	} else {
		    		disabled('btnExportReward');
		    	}
		    },
		    onCheck: function(i, r) {			
				var obj = new Object();			
				obj.id = r.id;		
				obj.code = r.customerCode;
				obj.name = r.customerName;				
				KeyShop._phCheckedMap.put(obj.id,obj);				
			 },
			 onUncheck: function(i, r) {
				 KeyShop._phCheckedMap.remove(r.id);
			 },
			 onCheckAll: function(rows) {
				 for (var i = 0, sz = rows.length; i < sz; i++) {
					var obj = new Object();
					obj.id = rows[i].id;		
					obj.code = rows[i].customerCode;
					obj.name = rows[i].customerName;			
					KeyShop._phCheckedMap.put(obj.id, obj);			
				 }
			 },
			 onUncheckAll: function(rows) {		
				  for (var i = 0, sz = rows.length; i < sz; i++) {
					  KeyShop._phCheckedMap.remove(rows[i].id);
					 }
			 }, 
		    method : 'GET'
		});
	},
	loadGridCustomer:function(ksId) {
		var title = '<a title="Thêm mới khách hàng" href="javascript:void(0)" onclick=\"return KeyShop.openPopupEditKSCustomer();\"><img src="/resources/images/icon_add.png"/></a>';
		$('#gridCustomer').datagrid({
			url: KeyShop.getUrlSearchKSCustomer(),
			width : $('#dgGridContainerCustomer').width() - 17,
			height: 'auto',
			singleSelect: true,
			rownumbers: true,
			fitColumns: true,
			scrollbarSize: 0,
			pagination: true,
			pageList  : [10,20,30],
			columns:[[  
			          {field:'shopCode',title:'NPP',width:50, sortable : true, resizable : false, align: 'left', formatter:function(value,row,index){
			        	  return Utils.XSSEncode(row.shopCode) + ' - ' + Utils.XSSEncode(row.shopName);
			          }},
			          {field:'customerCode',title:'Mã KH',width:40, sortable : true, resizable : false, align: 'left', formatter:function(value,row,index){
			        	  return Utils.XSSEncode(value);
			          }},
			          {field:'customerName',title:'Tên KH',width:60, sortable : true, resizable : false, align: 'left', formatter:function(value,row,index){
			        	  return Utils.XSSEncode(value);
			          }},
			          {field:'address',title:'Địa chỉ',width:70, sortable : true, resizable : false, align: 'left', formatter:function(value,row,index){
			        	  return Utils.XSSEncode(value);
			          }},
			          {field:'levelCustomer',title:'Mức đăng ký',width:50, sortable : true, resizable : false, align: 'left', formatter:function(value,row,index){
			        	  return Utils.XSSEncode(value);
			          }},
			          {field:'status',title:'Trạng thái',width:40, sortable: true, align: 'center',resizable:false , formatter:function(value, row, index) {
			        	  if (value == 1) {
			        		  return 'Đã duyệt';
			        	  } else if (value == 2) {
			        		  return 'Chưa duyệt';
			        	  } else if (value == 3) {
			        		  return 'Từ chối';
			        	  } else if (value == 0) {
			        		  return 'Tạm ngưng';
			        	  } 
			        	  return '';
			          }},
			          {field:'approve',title:'Duyệt', width: 30,sortable:false,resizable:false, align: 'center', formatter: function(value, row, index) {
			        	  if (row.status==2 && row.rewardType==null)
			        	  {
			        		  var str = "<a class = 'vt-grid-customer-approve' title='Duyệt' href='javascript:void(0)' onclick=\"return KeyShop.ProcessCustomerLevel(1,"+ row.customerId+","+row.cycleId+");\">Duyệt</a>";
			        		  return str;
			        	  }
			        	  return '';
			          }},
			          {field:'deny',title:'Từ chối', width: 30,sortable:false,resizable:false, align: 'center', formatter: function(value, row, index) {
			        	  if (row.status==2 && row.rewardType==null)
			        	  {
			        		  var str = "<a class = 'vt-grid-customer-deny' title='Từ chối' href='javascript:void(0)' onclick=\"return KeyShop.ProcessCustomerLevel(3,"+ row.customerId+","+row.cycleId+");\">Từ chối</a>";
			        		  return str;
			        	  }
			        	  return '';
			          }},
			          {field: 'ksRegistedHis', title: 'Lịch sử', width: 30, sortable: false, resizable: false, align: 'center', formatter: function(value, row, index) {
			        		return '<a onclick="return KeyShop.showKSRegistedHis('+ ksId +','+ row.ksLevelId +','+ row.customerId +','+ row.cycleId +',\'' +Utils.XSSEncode(row.customerCode)+'\',\'' +Utils.XSSEncode(row.customerName)+'\');" href="javascript:void(0)" title="Lịch sử cập nhật"><img height="17" src="/resources/images/icon-view.png"></a>';
			          }},
			          {field:'edit',title:title, width: 40,sortable:false,resizable:false, align: 'center', formatter: function(value, row, index) {			        	  
			        	  if (row.rewardType==null)
			        	  {
				        	  var str = "<a title='Sửa' href='javascript:void(0)' onclick=\"return KeyShop.openPopupEditKSCustomer("+row.ksCustomerId+");\"><img height='17' src='/resources/images/icon-edit.png'></a>";
				        	  str += "&nbsp&nbsp";
				        	  str += "<a title='Xóa' href='javascript:void(0)' onclick=\"return KeyShop.ProcessCustomerLevel(-1,"+ row.customerId+","+row.cycleId+");\"><img height='17' src='/resources/images/icon-delete.png'></a>";
				        	  return str;
			        	  }
			        	  return '';			        		  
			          }},
			]],
		  	onLoadSuccess :function(data){	    	
		    	$('.datagrid-header-rownumber').html('STT');
		    	KeyShop._mapKSCustomerSearch = new Map();
		    	if (data != undefined && data != null && data.rows.length > 0) {
		    		for (var i = 0 ; i < data.rows.length ; i++) {
			    		KeyShop._mapKSCustomerSearch.put(data.rows[i].ksCustomerId, data.rows[i]);
			    	}
		    		enable('btnExportCustomer');
		    	} else {
		    		disabled('btnExportCustomer');
		    	}
		    	//Xu ly duyet tu choi
		    	var arrApprove =  $('#dgGridContainerCustomer td[field="approve"]');
				if (arrApprove != undefined && arrApprove != null && arrApprove.length > 0) {
				  for (var i = 0, size = arrApprove.length; i < size; i++) {
				    $(arrApprove[i]).prop("id", "group_dt_kh_d_tc_gr_kh_cl_ap" + i);//Khai bao id danh cho phan quyen
					$(arrApprove[i]).addClass("cmsiscontrol");
				  }
				}
				//Xu ly duyet tu choi
		    	var arrDeny =  $('#dgGridContainerCustomer td[field="deny"]');
				if (arrDeny != undefined && arrDeny != null && arrDeny.length > 0) {
				  for (var i = 0, size = arrDeny.length; i < size; i++) {
				    $(arrDeny[i]).prop("id", "group_dt_kh_d_tc_gr_kh_cl_dn" + i);//Khai bao id danh cho phan quyen
					$(arrDeny[i]).addClass("cmsiscontrol");
				  }
				}
				
		    	//Xu ly cho edit
		    	var arrDelete =  $('#dgGridContainerCustomer td[field="edit"]');
				if (arrDelete != undefined && arrDelete != null && arrDelete.length > 0) {
				  for (var i = 0, size = arrDelete.length; i < size; i++) {
				    $(arrDelete[i]).prop("id", "group_edit_ks_gr_kh_cl_edit_" + i);//Khai bao id danh cho phan quyen
					$(arrDelete[i]).addClass("cmsiscontrol");
				  }
				}
				Utils.functionAccessFillControl('dgGridContainerCustomer', function(data){
					//Xu ly cac su kien lien quan den cac control phan quyen
					var arrTmpLength =  $('#dgGridContainerCustomer td[id^="group_dt_kh_d_tc_gr_kh_cl_"]').length;
					var invisibleLenght = $('.isCMSInvisible[id^="group_dt_kh_d_tc_gr_kh_cl_"]').length;
					if (arrTmpLength > 0 && arrTmpLength != invisibleLenght) {
						$('#gridCustomer').datagrid("showColumn", "approve");
						$('#gridCustomer').datagrid("showColumn", "deny");
					} else {
						$('#gridCustomer').datagrid("hideColumn", "approve");
						$('#gridCustomer').datagrid("hideColumn", "deny");
					}
					
					arrTmpLength =  $('#dgGridContainerCustomer td[id^="group_edit_ks_gr_kh_cl_edit_"]').length;
					invisibleLenght = $('.isCMSInvisible[id^="group_edit_ks_gr_kh_cl_edit_"]').length;
					if (arrTmpLength > 0 && arrTmpLength != invisibleLenght) {
						$('#gridCustomer').datagrid("showColumn", "edit");
					} else {
						$('#gridCustomer').datagrid("hideColumn", "edit");
					}
				});
				
				if ($('#dgGridContainerCustomer .vt-grid-customer-approve').length > 0) {
					$('#gridCustomer').datagrid("showColumn", "approve");
				} else {
					$('#gridCustomer').datagrid("hideColumn", "approve");
				}
				if ($('#dgGridContainerCustomer .vt-grid-customer-deny').length > 0) {
					$('#gridCustomer').datagrid("showColumn", "deny");
				} else {
					$('#gridCustomer').datagrid("hideColumn", "deny");
				}
		    },
		    method : 'GET'
		});
	},
	qtyChanged:function(selector){
		$('#errMsgKSCustomerLevel').html('').hide();		
		var dg_index = $(selector).attr('index');
		var rows = $('#gridCustomerLevel').datagrid('getRows')[dg_index];	
		var totalQuantity = 0;
		var totalAmount = 0;
		var amount = $('#amount'+dg_index).html().replace(/,/g, '').trim();
		var quantity = $('#quantity'+dg_index).html().replace(/,/g, '').trim();
		if(rows!=null){								
			var multi = $(selector).val().replace(/,/g, '').trim();
			if(multi != ''){
				if(isNaN(multi)){				
					setTimeout(function(){
						$(selector).focus();
					}, 20);
					$('#errMsgKSCustomerLevel').html('Bội số không đúng định dạng').show();
					$(selector).val('');
					return false;
				}
				totalQuantity = multi * quantity;
				totalAmount = multi * amount;				
				$('#totalAmount'+dg_index).html(formatCurrency(totalAmount));
				$('#totalQuantity'+dg_index).html(formatCurrency(totalQuantity));
			}
		}
		    	
	},
	loadGridCustomerLevel:function(ksId) {
		$('#dgGridContainerCustomerLevel').html('<table id="gridCustomerLevel"></table>');
		$('#gridCustomerLevel').datagrid({
			url: KeyShop.getUrlSearchKSCustomerLevel(),
			width : $('#dgGridContainerCustomerLevel').width() - 17,
			height: 'auto',
			singleSelect: true,
			rownumbers: true,
			fitColumns: true,
			scrollbarSize: 0,
			pagination: true,
			pageList  : [10,20,30],
			columns:[[  
			          {field:'ksLevelCode',title:'Mã mức',width:70, sortable : true, resizable : false, align: 'left', formatter:function(value,row,index){
			        	  return Utils.XSSEncode(value);
			          }},
			          {field:'ksLevelName',title:'Tên mức',width:120, sortable : true, resizable : false, align: 'left', formatter:function(value,row,index){
			        	  return Utils.XSSEncode(value);
			          }},
			          {field:'amount',title:'Số tiền',width:100, sortable : true, resizable : false, align: 'right', formatter:function(value,row,index){
			        	    return '<span id="amount'+index+'">'+Utils.XSSEncode(formatCurrency(value))+'</span>';
			          }},
			          {field:'quantity',title:'Số lượng',width:100, sortable : true, resizable : false, align: 'right', formatter:function(value,row,index){
			        	    return '<span id="quantity'+index+'">'+Utils.XSSEncode(formatCurrency(value))+'</span>';
			          }},
			          {field:'multiplier', title: 'Bội số ĐK', width:80, sortable:false, resizable:false ,align: 'right',formatter:function(value,row,index){
					    	if (row.multiplier == null) value = ""; 
					    	return '<input maxlength="8" index="'+index+'" style="text-align: right; width: 60px; margin: 0;" id="'+row.ksLevelId+'" type="text" class="InputTextStyle InputText1Style skuItem" value="'+formatCurrency(value)+'" onkeypress="NextAndPrevTextField(event,this,\'skuItem\')" onblur="return KeyShop.qtyChanged(this);" />';;
					  }},
					  {field:'totalAmount', title: 'Tổng tiền', width: 120, align: 'right', sortable:false,resizable:false ,align: 'right', formatter:function(value,row,index){
						   	 if(row.multiplier==null){
						   		return '<span id="totalAmount'+index+'">0</span>';
							 }
						   	 else return '<span id="totalAmount'+index+'">' + formatCurrency(row.multiplier*row.amount) + '</span>';
					    	 
					  }},
					  {field:'totalQuantity', title: 'Tổng số lượng', width: 120, align: 'right', sortable:false,resizable:false ,align: 'right', formatter:function(value,row,index){
						   	 if(row.multiplier==null){
						   		return '<span id="totalQuantity'+index+'">0</span>';
							 }
						   	 else return '<span id="totalQuantity'+index+'">' + formatCurrency(row.multiplier*row.quantity) + '</span>';
					  }},
			]],
		  	onLoadSuccess :function(data){	    	
		    	$('.datagrid-header-rownumber').html('STT');
		    	KeyShop._mapKSCustomerLevelSearch = new Map();
		    	for (var i = 0 ; i < data.rows.length ; i++) {
		    		KeyShop._mapKSCustomerLevelSearch.put(data.rows[i].ksLevelId, data.rows[i]);
		    		// vuongmq; 25/08/2015; bind cho nhap so nguyen duong
		    		if (data.rows[i] != undefined && data.rows[i] != null && data.rows[i].ksLevelId != undefined && data.rows[i].ksLevelId != null) {
		    			Utils.bindFormatOntextfieldCurrencyFor(data.rows[i].ksLevelId, Utils._TF_NUMBER);	
		    		}
		    	}
		    },
		    method : 'GET'
		});
	},
	openPopupKSProduct:function(){
		var ksId = $('#ksIdCurrent').val();
		var params = new Array();
		params.push({name:'ksId', value:ksId});
		CommonSearchEasyUI.searchProductDialog(function(data){
			if (data != null && data.length > 0) {
				KeyShop.addKSProduct(data);
			}
		}, params, '/key-shop/searchProduct');
	},
	deleteProduct:function(productId) {
		var dataModel = new Object();
		dataModel.ksId = $('#ksIdCurrent').val();
		dataModel.productId = productId;
		Utils.addOrSaveData(dataModel, "/key-shop/deleteKSProduct", null, 'errMsgKeyShop', function(data) {
			KeyShop.searchKSProduct();
			setTimeout(function() {
				$('.easyui-dialog').dialog('close');
			}, 2000);
		},null, null, true);
	},
	addKSProduct:function(data){
		var lstProductId = new Array();
		for (var i = 0 ; i < data.length ; i++) {
			lstProductId.push(data[i].productId);
		}
		var dataModel = new Object();
		dataModel.lstProductId = lstProductId;
		dataModel.ksId = $("#ksIdCurrent").val();
		Utils.addOrSaveData(dataModel, "/key-shop/addKSProduct", null, 'errMsgKS', function(data) {
			KeyShop.searchKSProduct();
			setTimeout(function() {
				$('.easyui-dialog').dialog('close');
			}, 2000);
		});
	},
	fillCategoriesToMultiSelect: function(isCheck) {
		$.ajax({  
			url: '/key-shop/get-category?isOrderByCode=true',
			method: 'GET',
			success: function(rs) {
				if (rs.error != undefined && !rs.error) {
					var lstCategory = rs.rows;
					var html = '';
					if(lstCategory.length > 0){
						if(isCheck == 1)
							{
								html += '<option value="0" selected="selected">'+Utils.XSSEncode(jsp_common_status_all)+'</option>';
							}else{
								html += '<option value="0" >'+Utils.XSSEncode(jsp_common_status_all)+'</option>';
							}
						for(var i=0; i < lstCategory.length; i++){
							if(isCheck == 1){
							html += '<option selected="selected" value="'+ lstCategory[i].id +'">' +
								Utils.XSSEncode(lstCategory[i].productInfoCode) + ' - ' +
								Utils.XSSEncode(lstCategory[i].productInfoName) + '</option>';
							}else{	
								html += '<option  value="'+ lstCategory[i].id +'">' +
									Utils.XSSEncode(lstCategory[i].productInfoCode) + ' - ' +
									Utils.XSSEncode(lstCategory[i].productInfoName) + '</option>';									
							}
						}
						$('#category').html(html);
						$('#category').change();
						$('#ddcl-category').remove();
						$('#ddcl-category-ddw').remove();
						$('#category').dropdownchecklist({
							emptyText: catalog_product_chon_nganh_hang,
							firstItemChecksAll: true,
							maxDropHeight: KeyShop._widthProductCat
						});
					} else {
						$('#category').html('');
						//$('#category').attr('disabled','disabled');
						$('#category').dropdownchecklist({
							emptyText:'-------------------',
							firstItemChecksAll: true,
							maxDropHeight: KeyShop._widthProductCat
						});
					}
					$('.ui-dropdownchecklist-text').css({"padding-left":"5px"});
				}
			}
		});
	},
	fillCategoryChildToMultiSelect: function(isCheck) {
		var param = '';
		var categories = $('#category').val();
		if (categories != null && categories.length > 0) {
			for (var i = 0; i < categories.length; i++) {
				param += '&lstProductInfoId=' + categories[i];
			}
		}
		$.ajax({
			url: '/key-shop/get-category-child?status=2'+param,
			method: 'GET',
			success: function(rs) {
				if (rs.error != undefined && !rs.error) {
					var lstCategory = rs.rows;
					//$('#categoryChild').removeAttr('disabled');
					var html = '';
					if(lstCategory.length > 0){
						if(isCheck == 1)
							{
								html += '<option value="0" selected="selected">'+jsp_common_status_all+'</option>';
							}else{
								html += '<option value="0" >'+jsp_common_status_all+'</option>';
							}
						for(var i=0; i < lstCategory.length; i++){
							if(isCheck == 1){
								html += '<option selected="selected" value="'+ lstCategory[i].id +'">' +
									Utils.XSSEncode(lstCategory[i].productInfoCode) + ' - ' +
									Utils.XSSEncode(lstCategory[i].productInfoName) + '</option>';
							}else{	
								html += '<option  value="'+ lstCategory[i].id +'">' +
									Utils.XSSEncode(lstCategory[i].productInfoCode) + ' - ' +
									Utils.XSSEncode(lstCategory[i].productInfoName) + '</option>';									
							}
						}
						$('#categoryChild').html(html);
						$('#categoryChild').change();
						$('#ddcl-categoryChild').remove();
						$('#ddcl-categoryChild-ddw').remove();
						$('#categoryChild').dropdownchecklist({
							emptyText: catalog_product_chon_nganh_hang,
							firstItemChecksAll: true,
							maxDropHeight: KeyShop._widthProductCat
						});
					} else {
						$('#categoryChild').html('');
						$('#ddcl-categoryChild').remove();
						$('#ddcl-categoryChild-ddw').remove();
						//$('#category').attr('disabled','disabled');
						$('#categoryChild').dropdownchecklist({
							emptyText:'-------------------',
							firstItemChecksAll: true,
							maxDropHeight: KeyShop._widthProductCat
						});
					}
					$('.ui-dropdownchecklist-text').css({"padding-left":"5px"});
				}
			}
		});
	},
	openImportExcelType:function(){
		$('#popupImport').dialog({
			title : 'Chọn loại import',
			width:250,
			height:'auto',
			onOpen: function(){
				
			}
		});
		$('#popupImport').dialog('open');
	},
	initImport:function() {
		$('#downloadTemplate').attr('href', 'javascript:void(0);');
		Utils._errExcelMsg = 'errExcelMsg';
		var options = {
			beforeSubmit : Utils.beforeImportExcel,
			success : Utils.afterImportExcelNew,
			type : "POST",
			dataType : 'html',
			data : { excelType : $('#excelType').val() }
		};
		$('#importFrm').ajaxForm(options);
		$('#excelFile').val('');
		$('#fakefilepc').val('');
	},
	importExcel : function() {
		var excelType = $('input:radio[name=importRadio]:checked').val();
		Utils._currentSearchCallback = function(){
			KeyShop.searchKS();
			var ksId = $('#ksIdCurrent').val();
			KeyShop.showShopGrid(ksId)
		};
		var options = {
			beforeSubmit : Utils.beforeImportExcel,
			success : Utils.afterImportExcelNew,
			type : "POST",
			dataType : 'html',
			data : { excelType : excelType }
		};
		$('#importFrm').ajaxForm(options);
		$('#importFrm').submit();
		$('#popupImport').dialog('close');
		return false;
	},
	excelTypeChanged:function() {
		$('#excelFile').val('');
		$('#fakefilepc').val('');
	},
	/**
	 * xuat excel danh sach keyshop
	 */
	exportExcelKS:function(){
		ReportUtils.exportReport("/key-shop/export-excel-ks", KeyShop._searchKSParam, "errExcelMsg");
	},
	/**
	 * xuat excel danh sach khach hang
	 */
	exportCustomer:function(){
		ReportUtils.exportReport("/key-shop/export-excel-customer", KeyShop._searchKSCustomerParam, "errMsgKeyShop");
	},
	/**
	 * xuat excel danh sach tra thuong
	 */
	exportReward:function(){
		ReportUtils.exportReport("/key-shop/export-excel-reward", KeyShop._searchKSRewardParam, "errMsgKeyShop");
	},
	exportExcelTemplate:function() {
		ReportUtils.exportReport('/key-shop/export-Template-Excel?excelType='+$('#excelType').val(), {}, 'errMsgKeyShop');
	},
	showShopGrid:function(ksId) {
		KeyShop.hideAllDiv();
		var ks = KeyShop._mapKSSearch.get(ksId);
		if (ks == null) return;
		$('#shopDiv').show();
		$('#shopTitle').html('ĐƠN VỊ PHÂN BỔ CỦA ' + Utils.XSSEncode(ks.ksName));
		$('#ksIdCurrent').val(ksId);
		KeyShop.loadShopGrid();
	},
	///@author HungLM16; @since JULY 21,2014; @description Mo node dau tien va con chau cua no
	expandNodeForLstChirentByTreeShop : function(tree, node) {
		if (node != undefined && node != null) {
			$(tree).treegrid('expand', node.id);
			var nodeEx = node.children;
			if (nodeEx != undefined && nodeEx != null) {
				KeyShop.expandNodeForLstChirentByTreeShop(tree, nodeEx[0]);
			}
		} else {
			return;
		}
	},
	///@author hunglm16; @since: August 18,2014; @description uncheckParent and uncheck Childrent
	uncheckFullParentToNode : function(node, id) {
		if (node != undefined && node != null) {
			var indexArr = KeyShop._arrShopInOrgAccessCheked.indexOf(node.id);
			if (indexArr > -1) {
				KeyShop._arrShopInOrgAccessCheked.splice(indexArr, true);
			}
			var cbx = $('#cbx_DgTreeShopDlCp_' + node.id);
			if (node.id != id) {
				$(cbx).prop('checked', false);
			}
			$('#checkShop_' + node.id.toString().trim()).prop('checked', false);
			if (node.attributes.shop.parentShop != undefined
					&& node.attributes.shop.parentShop != null) {
				var parNode = $('#treeGridShop').treegrid('find', node.attributes.shop.parentShop.id);
				if (parNode != undefined && parNode != null) {
					KeyShop.uncheckFullParentToNode(parNode);
				} else {
					return;
				}
			}
		} else {
			return;
		}
	},
	///@author HungLM16; @since JULY 25,2014; @description Mo node dau tien va con chau cua no
	uncheckNodeGroupAll: function(node, id){
		if (node != undefined && node != null) {
			var indexArr = KeyShop._arrShopInOrgAccessCheked.indexOf(node.id);
			if (indexArr > -1) {
				KeyShop._arrShopInOrgAccessCheked.splice(indexArr, true);
	    	}
			var cbx = $('#cbx_DgTreeShopDlCp_'+ node.id);
			if(node.id!=id){
				$(cbx).attr('checked', false);
			}
			$('#checkShop_' + node.id.toString().trim()).prop('checked', false);
			var arrChidren = node.children;
			if (arrChidren != undefined && arrChidren!=null && arrChidren.length >0){
				for(var i = 0; i < arrChidren.length; i++){
					KeyShop.uncheckNodeGroupAll(arrChidren[i]);
				}
			}
		}
	},
	///@author hunglm16; @since: August 20,2014; @description bat su kien tay doi tren checkbox
	onchangeCbxDgTreeShopOgrAccess:function(cbx){
		var node = $('#treeGridShop').treegrid('find', Number($(cbx).prop("id").replace(/cbx_DgTreeShopDlCp_/g,'').trim()));
		var indexArr = KeyShop._arrShopInOrgAccessCheked.indexOf(node.id);
		if($(cbx).prop("checked")){
			KeyShop.uncheckFullParentToNode(node, node.id);
			KeyShop.uncheckNodeGroupAll(node, node.id);
			if (indexArr < 0) {
				KeyShop._arrShopInOrgAccessCheked.push(node.id);
	    	}
		}else{
			if (indexArr > -1) {
				KeyShop._arrShopInOrgAccessCheked.splice(indexArr, true);
	    	}
			KeyShop.uncheckFullParentToNode(node, node.id);
		}
	},
	///@author HungLM16; @since JULY 17,2014; @description Bat cac su kien sau khi chon tren Tree Grid Shop
	updateChangeByCheckInTreeGridShop: function(){
		$('.ErrorMsgStyle').html('').hide();
		var msg = '';
		var arrDetele = [];//Danh sach xoa
		var arrInsert = [];//Danh sach them moi
		var isAll = false;
		var flag = false;
		var params = {};
		
		var dem = 0;
		if (KeyShop._arrShopInOrgAccess == null) {
			KeyShop._arrShopInOrgAccess = [];
		}
		if (KeyShop._arrShopInOrgAccessCheked == null) { 
			KeyShop._arrShopInOrgAccess = [];
		}
		//Xu ly cho so suat
		var mapShop = new Map();
		$('#shopDiv .vinput-money').each(function() {
			  var quantity = VTUtilJS.returnMoneyValue($(this).val());
			  if (quantity != undefined && quantity != null) {
					if (isNaN(quantity) || (quantity.length > 0 && Number(quantity) <= 0)) {
						var mgsEror = 'Đơn vị ' + $($(this).parent().parent().parent().children()[0]).text();
						msg = 'Số suất là kiểu số nguyên lớn hơn 0. ' + mgsEror.trim() + ' có số suất không hợp lệ';
						$(this).focus();
					} else if (Number(quantity) > 0) {
						var nodeId = Number($(this).prop('id').replace(/txtQuantity-ip-/g, ''));
						if (KeyShop._arrShopInOrgAccessCheked != undefined && KeyShop._arrShopInOrgAccessCheked != null && KeyShop._arrShopInOrgAccessCheked.indexOf(nodeId) > -1) {
							//Neu nhu NPP duoc check
							mapShop.put(nodeId, Number(quantity));
						} else {
							//Nguoc lai, neu nhu cac cap cha duoc check
							var node = $('#treeGridShop').treegrid('find', nodeId);
							if (KeyShop.checkParentCheckSelectTrue(node)) {
								mapShop.put(nodeId, Number(quantity));							
							}							
						}
					}
			  }
		});
		if(msg.length > 0){
			$('#errMsgKeyShop').text(msg).show();
			return false;
		}
		for (var i = 0, sizei = KeyShop._arrShopInOrgAccessCheked.length; i < sizei; i++) {
			flag = false;
			for (var j = 0, sizej = KeyShop._arrShopInOrgAccess.length; j < sizej; j++) {
				if (KeyShop._arrShopInOrgAccessCheked[i] == KeyShop._arrShopInOrgAccess[j]) {
					dem++;
					flag = true;
					break;
				}
			}
			if (!flag) {
				arrInsert.push(KeyShop._arrShopInOrgAccessCheked[i]);
			}
		}
		flag = false;
		params.flagChange = StatusType.ACTIVE;
		if (dem == KeyShop._arrShopInOrgAccess.length && dem == KeyShop._arrShopInOrgAccessCheked.length){
			params.flagChange = StatusType.INACTIVE;
		} else if (KeyShop._arrShopInOrgAccessCheked.length == 0) {
			arrDetele = KeyShop._arrShopInOrgAccess;
			params.shopId = $('#shopId').val().trim();
			params.ksId = $('#ksIdCurrent').val().trim();
			flag = true;
		} else {
			for(var i = 0; i< KeyShop._arrShopInOrgAccess.length; i++){
				flag = false;
				for(var j=0; j < KeyShop._arrShopInOrgAccessCheked.length; j++){
					if (KeyShop._arrShopInOrgAccessCheked[j] == KeyShop._arrShopInOrgAccess[i]) {
						flag = true;
						break;
					}
				}
				if (!flag) {
					arrDetele.push(KeyShop._arrShopInOrgAccess[i]);
				}
			}
		}
		
		if (mapShop != null && mapShop.size() > 0) {
			var arrShopTarget = [];
			var keyArray = mapShop.keyArray;
			for (var i = 0, size = keyArray.length; i < size; i++) {
				arrShopTarget.push(keyArray[i] + ';' + mapShop.get(keyArray[i]));
			}
			params.arrOjectText = arrShopTarget.toString();
		}
		params.lstLong = arrInsert;//Them moi
		params.arrLong = arrDetele;//Xoa
		params.ksId = $('#ksIdCurrent').val().trim();
		Utils.addOrSaveData(params, '/key-shop/update-shop-tree', null,  'errMsgKeyShop', function(data) {
			$('#successMsgKeyShop').html("Lưu dữ liệu thành công").show();
			KeyShop.loadShopGrid();
			var tm = setTimeout(function(){
				//Load lai danh sach quyen
				//$("input[name='checkRightShop']").attr('checked', false);
				$('.SuccessMsgStyle').html("").hide();
				clearTimeout(tm);
			 }, 1500);
		}, null, null, null,msg, function(data){
			if(data.errorType!=undefined && data.errorType!=null && data.errorType == 1){
				//$('#shopCode').focus();
			}
		});
		return false;
	},
	/**
	 * Ham open dialog search khach hang
	 * @param url
	 * @param callback
	 */
	openCustomerOnDialog:function(url,callback){		
		$('.easyui-dialog #btnSearchStyle1').unbind('click');
		$('#searchStyle1EasyUIDialogDiv').show();
		var html = $('#searchStyle1EasyUIDialogDiv').html();		
		$('#searchStyle1EasyUIDialog').dialog({  
	        title: 'Thông tin tìm kiếm',  
	        closed: false,  
	        cache: false,  
	        modal: true,
	        width:750,
	        onOpen: function(){		        	
	        	var tabindex = -1;
	        	$('.InputTextStyle, select, input, button , li , tr, td, th, label, ul, a, img').each(function () {
					if (this.type != 'hidden') {
						$(this).attr("tabindex", tabindex);
						tabindex -=1;
					}
				});	        		        	
	        	$('.easyui-dialog #seachStyle1Code').focus();
				$('.easyui-dialog #seachStyle1AddressLabel').show();
				$('.easyui-dialog #seachStyle1Address').show();
				$('.easyui-dialog #searchStyle1Grid').show();
				$('.easyui-dialog #btnSearchStyle1').css('margin-left', 270);
				$('.easyui-dialog #btnSearchStyle1').css('margin-top', 5);
				$('.easyui-dialog #seachStyle1CodeLabel').html('Mã KH');
				$('.easyui-dialog #seachStyle1NameLabel').html('Tên KH');
				
				Utils.bindAutoSearch();
				CommonSearch._currentSearchCallback = callback;
				$('.easyui-dialog #searchStyle1Grid').datagrid({
					url : url,
					autoRowHeight : true,
					rownumbers : true, 
					checkOnSelect :true,
					pagination:true,
					rowNum : 10,
					pageSize:10,
					scrollbarSize : 0,
//					fitColumns:true,
					queryParams:{
						page:1
					},
					width : 720,
				    columns:[[
				        {field:'shopCode',title:'Mã NPP',align:'left', width:100, sortable : false,resizable : false},
				        {field:'shortCode',title:'Mã KH',align:'left', width:100, sortable : false,resizable : false},  
				        {field:'customerName',title:'Tên KH',align:'left', width:150, sortable : false,resizable : false},				        
				        {field:'address',title:'Địa chỉ',align:'left', width:250, sortable : false,resizable : false},
				        {field:'select', title:'Chọn', width:68, align:'center',sortable : false,resizable : false,formatter : 
				        	function(value, rowData, rowIndex) {
							return "<a href='javascript:void(0)' onclick=\"return KeyShop.getResultSeachStyle1EasyUIDialog("+ rowIndex + ");\">Chọn</a>";
				         }},
				        {field :'id',hidden : true},
				    ]],
				    onLoadSuccess :function(data){
				    	 tabindex = 1;
			    		 $('.easyui-dialog input,.easyui-dialog select,.easyui-dialog button').each(function () {
				    		 if (this.type != 'hidden') {
					    	     $(this).attr("tabindex", '');
								 tabindex++;
				    		 }
						 });
			    		 $('.datagrid-header-rownumber').html('STT');	
			    		 updateRownumWidthForJqGrid('.easyui-dialog');
//			    		 $(window).resize();
			    		 setTimeout(function() {
			    			 CommonSearchEasyUI.fitEasyDialog('searchStyle1EasyUIDialog');
			    		 }, 720);
			    		 var code = $('.easyui-dialog #seachStyle1Code').val().trim();
				    }
				});
				$('.easyui-dialog #btnSearchStyle1').bind('click',function(event) {
					if(!$(this).is(':hidden')){
						var code = $('.easyui-dialog #seachStyle1Code').val().trim();
						var name = $('.easyui-dialog #seachStyle1Name').val().trim();
						var address = $('.easyui-dialog #seachStyle1Address').val().trim();
						$('.easyui-dialog #searchStyle1Grid').datagrid('load',{code :code, name: name,address:address});
						var records = $('.easyui-dialog #searchStyle1Grid').datagrid('getRows').length;
						$('.easyui-dialog #seachStyle1Code').focus();
					}					
				});
				$('.easyui-dialog #btnClose').bind('click',function(event) {
					$('#searchStyle1EasyUIDialogDiv').css("visibility", "hidden");					
					$('.easyui-dialog').dialog('close');
				});
	        },
	        onBeforeClose: function() {
	        	var curIdFocus = $('#cur_focus').val();
				$('#'+ curIdFocus).focus();				
//				$('.easyui-dialog #searchStyle1Grid').datagrid('reload',{	
//					pageSize: 10
//				});
	        },
	        onClose : function(){
	        	$('#searchStyle1EasyUIDialogDiv').html(html);
	        	$('#searchStyle1EasyUIDialogDiv').hide();
	        	$('#searchStyle1ContainerGrid').html('<table id="searchStyle1Grid" class="easyui-datagrid" style="width: 520px;"></table><div id="searchStyle1Pager"></div>');
	        	$('.easyui-dialog #seachStyle1Code').val('');
	        	$('.easyui-dialog #seachStyle1Name').val('');
	        	$('.easyui-dialog #seachStyle1Address').val('');	        	
	        }
		});
	},
	/**
	 * Ham tra ve ket qua khi chon khach hang tren datagrid
	 * @param rowIndex
	 * @returns {Boolean}
	 */
	getResultSeachStyle1EasyUIDialog : function(rowIndex) {
		var row = $('.easyui-dialog #searchStyle1Grid').datagrid('getRows')[rowIndex];			
		var obj = {};
		obj.code = row.shortCode;
		obj.name = row.customerName;
		obj.address = row.address;
		obj.id = row.id;
		obj.rewardType= row.rewardType;
		$('#addressPop').html(row.address);
		$('#customerId').val(row.id);
		
		var ksId = $('#ksIdCurrent').val().trim();
		KeyShop.loadGridCustomerLevel(ksId);
		
		if (CommonSearch._currentSearchCallback != null) {
			$('#searchStyle1EasyUIDialog.easyui-dialog').dialog('close');
			CommonSearch._currentSearchCallback.call(this, obj);
		}	
		return false;
	},
	
	UpdateCustomerLevel:function(){
		var msg = '';
		$('#errMsgKSCustomerLevel').html('').hide();
		$('#successMsgKSCustomerLevel').html('').hide()
		if ($('#numPopup').combobox('getValue') == '')
		{
			msg = "Chưa chọn chu kỳ";
			$('#errMsgKSCustomerLevel').html(msg).show();
			return false;
		}
		if ($('#customerId').val() == '')
		{
			msg = "Chưa chọn khách hàng";
			$('#errMsgKSCustomerLevel').html(msg).show();
			return false;
		}
		var numPopup=0;
		if ($('#numPopup').combobox('getValue')<10) numPopup='0'+$('#numPopup').combobox('getValue');
		else numPopup=$('#numPopup').combobox('getValue');
		var cycleIdPopup = $('#yearPopup').val() +''+ numPopup+''+numPopup;
		var listKsLevelId = new Array();
		var listMultiplier = new Array();
		var customerId = $('#customerId').val();
		var ksId = $('#ksIdCurrent').val().trim();
		var totalRow = 0;
		$('.skuItem').each(function(){
			var index = $(this).attr('index');
			//if($(this).val().trim().length>0){
				var rows = $('#gridCustomerLevel').datagrid('getRows')[index];
				if(rows!=null){
					
					var ksLevelIdRow = rows.ksLevelId;
					var multiRow =$(this).val().trim().replace(/,/g,'');
					
					if(isNaN(multiRow)){
						$(this).focus();
						msg = "Bội số không đúng định dạng";
						return false;
					}
					
					if (multiRow!=null && (parseInt(multiRow) >= 0 || isNullOrEmpty(multiRow)))
					{
						if(!isNullOrEmpty(multiRow)) {
							totalRow = totalRow + parseInt(multiRow);	
						}
						
						listKsLevelId.push(ksLevelIdRow);
						listMultiplier.push(multiRow);
					}
					
				}				
			//}
		});	
		
		if(listKsLevelId.length==0 || totalRow == 0){
			$('#errMsgKSCustomerLevel').html("Chưa chọn bội số cho mức").show();
			return false;
		}
		var data = new Object();
		data.cycleId = cycleIdPopup;
		data.customerId = customerId;
		data.ksId = ksId;
		data.listKsLevelId = listKsLevelId;
		data.listMultiplier = listMultiplier;
		Utils.addOrSaveData(data, '/key-shop/update-customer-level', null,  'errMsgKSCustomerLevel', function(data) {
			$('#successMsgKSCustomerLevel').html("Lưu dữ liệu thành công").show();
			KeyShop.loadGridCustomerLevel(ksId);
			KeyShop.loadGridCustomer(ksId);
			var tm = setTimeout(function() {				
				$('#successMsgKSCustomerLevel').html('').hide();
				$('#addOrEditKSCustomer').dialog('close');
				clearTimeout(tm);
			 }, 1500);
		}, null, null, null, msg, function(data) {
			if (data.errorType!=undefined && data.errorType!=null && data.errorType == 1) {
				//$('#shopCode').focus();
			}
		});
		return false;
	},
	ApproveCustomerLevel:function(processType){
		var customerId = $('#customerId').val();
		var numPopup=0;
		if ($('#numPopup').combobox('getValue')<10) numPopup='0'+$('#numPopup').combobox('getValue');
		else numPopup=$('#numPopup').combobox('getValue');
		var cycleIdPopup = $('#yearPopup').val() +''+ numPopup+''+numPopup;
		KeyShop.ProcessCustomerLevel(processType,customerId,cycleIdPopup);
	},
	ProcessCustomerLevel:function(processType, customerId, cycleId){
		var msg = '';
		if (processType==-1)
		{
			$('#errMsgKeyShop').html('').hide();
			$('#successMsgKeyShop').html('').hide();
		}
		$('#errMsgKSCustomerLevel').html('').hide();
		$('#successMsgKSCustomerLevel').html('').hide()
		
		
		var listKsLevelId = new Array();
		var listMultiplier = new Array();
		//var customerId = $('#customerId').val();
		var ksId = $('#ksIdCurrent').val().trim();
		
		var data = new Object();
		data.cycleId = cycleId;
		data.customerId = customerId;
		data.ksId = ksId;
		data.processType = processType;
		Utils.addOrSaveData(data, '/key-shop/process-customer-level', null,  'errMsgKeyShop', function(data) {
			$('#successMsgKeyShop').html("Lưu dữ liệu thành công").show();
			var tm = setTimeout(function(){
				KeyShop.loadGridCustomerLevel(ksId);
				KeyShop.loadGridCustomer(ksId);
				$('#successMsgKeyShop').html('').hide();
				clearTimeout(tm);
			 }, 1500);
		}, null, null, null,msg, function(data){
			if(data.errorType!=undefined && data.errorType!=null && data.errorType == 1){
				//$('#shopCode').focus();
			}
		});
		
//		$.messager.confirm('Xác nhận', 'Cập nhật thông tin mức khách hàng', function(r){
//			if (r){
//				
//			}
//		});		
		return false;
	},
	loadShopGrid : function(){
		$('#shopContainerGrid').html('<table id="treeGridShop" class="easyui-treegrid"></table>').change().show();
		var orgAccessTreeControlId = '#treeGridShop';
		var params = {};
		params.shopId = $('#shopId').val().trim();
		params.ksId = $('#ksIdCurrent').val();
		params.check = $('#checkAllTreeShop')[0].checked;
		KeyShop._arrShopInOrgAccess=[];
		KeyShop._arrShopInOrgAccessCheked=[];
		$(orgAccessTreeControlId).treegrid({  
		    url: '/key-shop/unit-access-tree', //generalcms/data-permission/org-access-tree
		    width: $('#shopContainerGrid').width(),
		    queryParams: params,
		    scrollbarSize: 0,
			fitColumns: true,
			checkOnSelect: false,
			selectOnCheck: false,
		    rownumbers: true,
	        idField: 'id',
	        treeField: 'shopName',
		    columns:[[	       
		        {field: 'shopName', title: 'Đơn vị', resizable: false, width: 500, formatter: function(value, row, index) {
		        	return Utils.XSSEncode(row.attributes.shop.shopName);
		        }},
		        {field: 'specificType', title: 'Số suất', resizable: true, width: 110, formatter: function(value, row, index) {
		        	if (row.attributes.shop.type != null && row.attributes.shop.type.specificType != undefined && row.attributes.shop.type.specificType != null && row.attributes.shop.type.specificType == 'NPP') {
		        		return '<input type="text" maxlength="9" class="vinput-money" style="text-align: right; width: 120px;" value="'+VTUtilJS.formatCurrency(row.attributes.shop.quantity)+'" id="txtQuantity-ip-'+row.id+'">';
		        	}
		        }},
		        {field: 'shopId', title: '', resizable: false, width: 30, fixed: true, formatter: function(value, row, index) {
		        	var html = '';
		        	if (row.attributes && row.attributes.hasOwnProperty('selected') && row.attributes.selected) {
		        		KeyShop._arrShopInOrgAccess.push(row.id);
		        		html = '<input type="checkbox" checked="checked" onchange="KeyShop.onchangeCbxDgTreeShopOgrAccess(this);" name="cbx_DgTreeShopDlCp_ShopId" id="cbx_DgTreeShopDlCp_'+row.id+'" value="true">';
		        	} else {
		        		html = '<input type="checkbox" onchange="KeyShop.onchangeCbxDgTreeShopOgrAccess(this);" name="cbx_DgTreeShopDlCp_ShopId" id="cbx_DgTreeShopDlCp_'+row.id+'" value="false">';
		        	}
		        	return html;
		        }}
//		        ,		        
//		        {field: 'editCheck', title: '', width: 30, align: 'center', resizable: false, fixed: true, formatter: function(value, row, index) {
//		        	var html = '';
//		        	if (row.children != undefined && row.children != null) {
//		        		html= html + '<input type ="checkbox" value="' + row.id + '" id="checkShop_' + row.attributes.id + '" onclick="return PermissionManagement.onchangeCheckBoxRightGridShop(this);" name = "checkRightShop">';
//					}
//		        	return html;
//		        }}
		    ]],
		    onLoadSuccess: function(n, data) {
		    	$(window).resize();
		    	$('.vinput-money').each(function() {
		    		$(this).width($(this).parent().width());
		    		VTUtilJS.bindFormatOnTextfield($(this), VTUtilJS._TF_NUMBER_COMMA);
		    		VTUtilJS.formatCurrencyFor($(this));
				});
		    	KeyShop._rowIndex = -1;
		    	KeyShop._arrShopInOrgAccessCheked = [];
		    	if (data != undefined && data != null && data.maxLevel != undefined && data.maxLevel > 0) {
		    		KeyShop._maxLevel = data.maxLevel;
		    	}
		    	var root = $('#treeGridShop').treegrid('getRoot');
		    	if (root != undefined && root != null) {
		    		//Check by Flag
		    		//Mo node dau tien va con chau cua no
		    		if ((params.shopId != null && params.shopId.trim().length > 0) || !$('#checkAllTreeShop')[0].checked) {
		    			//$(this).treegrid('expandAll');
		    		} 
		    		else {
				    	var nodeEx = root.children[0];
				    	KeyShop.expandNodeForLstChirentByTreeShop(this, nodeEx);
		    		}
		    	}
		    	if (KeyShop._arrShopInOrgAccess != null && KeyShop._arrShopInOrgAccess.length > 0) {
		    		for (var i = 0; i < KeyShop._arrShopInOrgAccess.length; i++) {
		    			if (KeyShop._arrShopInOrgAccessCheked.indexOf(KeyShop._arrShopInOrgAccess[i]) < 0) {
		    				KeyShop._arrShopInOrgAccessCheked.push(KeyShop._arrShopInOrgAccess[i]);
		    	    	}
		    		}
		    	}
		    }
		});
	},
	yearChange: function(year, num) {
 		var par = {};
 		par.year = $('#' + year).val();
 		Utils.getJSONDataByAjax(par, '/commons/list-cycle-filter?isChoiceAll=true', function(data){
 			if (data != null && data.lstCycle != null) {
 				Utils.bindPeriodCbx('#' + num, data.lstCycle, data.currentNum, 115, function() {
// 					KeyShop.validatePeriod();
 				}, 
 				function() {
 					KeyShop._isDoneLoadNum = true;
 				}, 'num', 'cycleName');
// 				KeyShop.validatePeriod();
 			}
 		});
 	},
 	yearChangePopup: function(year, num) {
 		var par = {};
 		par.year = $('#' + year).val();
 		Utils.getJSONDataByAjax(par, '/commons/list-cycle-filter?isChoiceAll=true', function(data){
 			if (data != null && data.lstCycle != null) {
 				var customerId = $('#ksCustomerId').val().trim();
 				var currentNum = 0;
 				if(!isNullOrEmpty(customerId) && KeyShop._mapKSCustomerSearch != null) {
 					var kc = KeyShop._mapKSCustomerSearch.get(customerId);
 					currentNum = Number(kc.cycleId.toString().substring(6, 8));
 				} else {
 					var currentNum = $('#numBottom').combobox('getValue').trim();
 	 				if (isNullOrEmpty(currentNum)) {
 	 					currentNum = null;
 	 				}	
 				}
 				
 				Utils.bindPeriodCbx('#' + num, data.lstCycle, currentNum, 115, function() {
 					KeyShop.validatePeriod();
 				}, 
 				function() {
 					KeyShop._isDoneLoadNum = true;
 				}, 'num', 'cycleName');
 				KeyShop.validatePeriod();
 			}
 		});
 	},
 	validatePeriod: function (){
 		if ($('#yearPopup').val() != null && $('#numPopup').combobox('getValue') != null) {
 			var curYearPeriod = Number($('#currentYear').val().trim());
 			var curNumPeriod = Number($('#currentCycleId').val().substring(7, 8));
 			var yearPeriodCreate = Number($('#yearPopup').val().trim());
 			var numPeriodCreate = Number($('#numPopup').combobox('getValue').trim());
 			
 			if(yearPeriodCreate < curYearPeriod || (yearPeriodCreate == curYearPeriod && numPeriodCreate < curNumPeriod)){
 				$('#group_dt_kh_d_tc_btnApproveCustomerLevel').hide();
 				$('#group_dt_kh_d_tc_btnDenyCustomerLevel').hide();
 				$('#group_edit_ks_btnUpdateCustomerLevel').hide();
 			}else{
 				$('#group_edit_ks_btnUpdateCustomerLevel').show();
 			}

 			var ksId = $('#ksIdCurrent').val().trim();
 			KeyShop.loadGridCustomerLevel(ksId);
 		}
 	},
 	
 	
 	/**
 	 * Kiem tra node cha duoc check
 	 * @param node thong ton don vi
 	 * @param id Khoa chinh Don vi
 	 * @author hunglm16
 	 * @since 18/11/2015
 	 */
 	checkParentCheckSelectTrue: function (node) {
 		if (node != undefined && node != null) {
			if (KeyShop._arrShopInOrgAccessCheked.indexOf(node.id) > -1) {
				//Kiem tra voi danh sach don vi duoc Check
				return true;
			}
			if (node.attributes.shop.parentShop != undefined && node.attributes.shop.parentShop != null) {
				//Xu ly voi cao hon
				var parNode = $('#treeGridShop').treegrid('find', node.attributes.shop.parentShop.id);
				if (parNode != undefined && parNode != null) {
					return KeyShop.checkParentCheckSelectTrue(parNode);
				}
				return false;
			}
		}
 		return false;
 	},
 	
 	/**
 	* Hien thi thong tin lich su cap nhat khach hang
 	* @author hunglm16
 	* @param 
 	* @since 23/11/2015
 	*/
 	showKSRegistedHis: function(ksId, ksLevelId, customerId, cycleId, customerCode, customerName) {
 		if (customerCode == undefined || customerCode == null) {
 			customerCode = '';
 		}
 		if (customerName == undefined || customerName == null) {
 			customerName = '';
 		}
 		var titleDialog = 'Thông tin lịch sử cập nhật ' + customerCode.trim() + ' - ' + customerName.trim();
 		VCommonJS.showDialogList({
		    url : '/key-shop/getKSRegistedHistory',
		    dialogInfo: {
		    	title: titleDialog.trim(),
		    	width: 780
		    },
		    params: {
		    	ksId: ksId
		    	//, ksLevelId: ksLevelId
		    	, customerId: customerId
		    	, cycleId: cycleId
		    },
		    columns : [[
		        {field: 'keyShopName', title: 'Chương trình', align: 'left', width: 150, sortable: false, resizable: false, formatter: function(value, row, index) {
					return Utils.XSSEncode(value);
				}},
		        {field: 'cycleName', title: 'Chu kỳ', align: 'left', width: 90, sortable: false, resizable: false, formatter: function(value, row, index) {
					return Utils.XSSEncode(value);
				}},
		        {field: 'createDateStr', title: 'Ngày giờ', align: 'center', width: 80, sortable: false, resizable: false, formatter: function(value, row, index) {
					return Utils.XSSEncode(value);
				}},
		        {field: 'userName', title: 'Người cập nhật', align: 'left', width: 110, sortable: false, resizable: false, formatter: function(value, row, index) {
					return Utils.XSSEncode(value);
				}},
		        {field: 'levelName', title: 'Mức', align: 'left', width: 100, sortable: false, resizable: false, formatter: function(value, row, index) {
					return Utils.XSSEncode(value);
				}},
		        {field: 'multiplyer', title: 'Bội số', align: 'right', width: 90, sortable: false, resizable: false, formatter: function(value, row, index) {
					if (value != undefined && value != null) {
						return VTUtilJS.returnMoneyValue(value);
					}
					return '';
				}}
		    ]]
		});
 	}
};
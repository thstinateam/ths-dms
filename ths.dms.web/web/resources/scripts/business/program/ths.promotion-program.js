/**
 * PromotionProgram
 * xu ly cho chuong trinh khuyen mai
 * 
 * searchProgram: tim kiem CTKM (man hinh danh sach)
 * showTab(tabId, url): mo cac tab tuong ung tren chi tiet CTKM voi id tab, duong dan request lay html
 * searchShop: tim kiem don vi tham gia CTKM tab don vi
 * searchCustomerShopMap: tim kiem KH tham gia CTKM khi click vao chi tiet don vi
 * toggleCustomerSearch: dong/mo thong tin tim kiem KH
 * searchSaler: tim kiem NVBH trong tab so suat NVBH
 * showCustomerDlg: mo popup them KH vao CTKM
 * addCustomerQtt: them KH vao CTKM
 * updateCustomerQuantity: cap nhat so suat KH tren danh sach KH
 * deleteCustomerMap: xoa KH khoi CTKM
 * updateShopQuantity: cap nhat so suat don vi (NPP) tren danh sach don vi
 * deleteShopMap: xoa don vi khoi CTKM
 * showShopDlg: mo popup them don vi vao CTKM
 * addShopQtt: them don vi vao CTKM
 * exportPromotionShop: xuat don vi tham gia CTKM ra excel
 * importPromotionShop: nhap don vi tham gia CTKM tu excel
 * checkSaleLevelCatIdInList: khong biet
 * loadAppliedAttributes: lay danh sach thuoc tinh KH da co trong CTKM
 * selectAttributes: chon thuoc tinh ben trai va dua sang ben phai (dua vao CTKM)
 * removeAttributes: chon thuoc tinh ben phai (trong CTKM) va dua sang ben trai (bo ra khoi CTKM)
 * saveCustomerAttributes: luu thuoc tinh KH tham gia CTKM
 * getTrue_DataOrFalse_MessageOfAutoAttribute: khong biet
 * updateSalerQuantity: cap nhat so suat NVBH
 * showSalerDlg: mo popup them NVBH vao CTKM
 * addSalerQtt: them NVBH vao CTKM
 * exportPromotionStaff: xuat ds so suat NVBH ra excel
 * importPromotionStaff: nhap so suat NVBH CTKM tu excel
 */
var PromotionProgram = {
	shopId: null,
	_xhrSave: null,
	_promotionShopParams: null,
	_promotionStaffParams: null,
	_levelValueType:null,//1 hoac 2
	_listProduct:null,
	_mapGroupMua:null,
	_mapGroupKM:null,
	_mapLevelMua:null,
	_mapLevelKM:null,
	_listMapping:null,
	_hasCustomerType:null,
	_hasSaleLevel:null,
	_hasCustomerCardType:null,
	_flagAjaxTabAttribute:null,
	_listSaleProduct:null,
	_DEFAULT_FROM_LEVEL: 1,
	_DEFAULT_TO_LEVEL: 5,
	fromLevel : 0,
	toLevel : 5,
	isFinishLoad : true,
	isEndLoad : false,
	isButtonLoad : false,
	_mapTabProcess: false,
	_paramCusSearch: null,
	gridRowIconFormatter: function(value,row,index) {
		if(row.type != null && (row.type.substring(0,2) == 'ZM' || row.type.substring(0,2) == 'ZT' || row.type.substring(0,2) == 'ZD'|| row.type.substring(0,2) == 'ZH')){
			return "<a href='/promotion/detail?promotionId="+row.id+"&proType=2'><span style='cursor:pointer'><img src='/resources/images/icon-edit.png'/></span></a>";
		}else{
			return "<a href='/promotion/detail?promotionId="+row.id+"&proType=1'><span style='cursor:pointer'><img src='/resources/images/icon-edit.png'/></span></a>";
		}
	},
	
	update : function() {
		$('.ErrorMsgStyle').html('');
		var msg = '';
		if (msg.length == 0) {
			msg = Utils.getMessageOfRequireCheck('promotionCode', promotionMultiLanguage.code, Utils._CODE);
		}
		if (msg.length == 0) {
			msg = Utils.getMessageOfSpecialCharactersValidate('promotionCode', promotionMultiLanguage.code, Utils._CODE);
		}
		if (msg.length == 0) {
			msg = Utils.getMessageOfRequireCheck('promotionName', promotionMultiLanguage.name, Utils._ADDRESS);
		}
		if (msg.length == 0) {
			msg = Utils.getMessageOfSpecialCharactersValidate('promotionName', promotionMultiLanguage.name, Utils._ADDRESS);
		}
		if (msg.length == 0) {
			msg = Utils.getMessageOfRequireCheck('description', promotionMultiLanguage.description, Utils._ADDRESS);
		}
		if (msg.length == 0) {
			msg = Utils.getMessageOfSpecialCharactersValidate('description', promotionMultiLanguage.description, Utils._ADDRESS);
		}
		if (msg.length > 0) {
			$('#errorMsg').html(msg.trim()).show();
			return;
		}
		var params = null;
		if($('#status').val() == 0) {
			params = new Object();
			params.promotionId = $('#id').val();
			params.promotionName = $('#promotionName').val();
			params.startDate = $('#startDate').val();
			params.endDate = $('#endDate').val();
			params.status = $('#status').val();
			params.canEdit = $('#canEdit').is(':checked');
			params.description = $('#description').val();
		} else {
			params = VTUtilJS.getFormData('update-form');
		}
		if(params == null) {
			return;
		}
		if(params.typeCode == -1) {
			VTUtilJS.showMessageBanner(true, 'Bạn chưa chọn loại chương trình');
			return;
		}
		$('.SuccessMsgStyle').hide();
		if(params != null) {
			var promotionId = $('#id').val();
			if(!VTUtilJS.isNullOrEmpty(promotionId)) {
				params.promotionId = promotionId;
			}
			$.messager.confirm("Xác nhận", "Bạn có muốn cập nhập thông tin CTKM?", function(r) {
				if(r) {
					VTUtilJS.postFormJson(params, '/promotion/update', function(data) {
						if(data.error) {
							VTUtilJS.showMessageBanner(true, data.errMsg);
						} else {
							window.location.href = '/promotion/detail?promotionId='+data.promotionId;
						}
					});
				}
			});
		}
	},

	/**
	 * tai file mau import CTKM
	 * @author  tuannd20
	 * @return void
	 * @date 27/01/2015
	 */
	downloadImportTemplate: function() {
		var selectedProgramTypes = $('#typeCode').val();
		var fileURL = excel_template_path + 'catalog/Bieu_mau_danh_muc_import_CTKM.xlsx';
		if (selectedProgramTypes.length == 1) {
			try {
//				var selectedProgramType = $('select#typeCode option[value=' + selectedProgramTypes[0] + ']').html().split('-')[0];
//				if (selectedProgramType < 'ZV22') {
//				}
				fileURL = excel_template_path + 'catalog/Bieu_mau_danh_muc_import_CTKM_' + selectedProgramType + '.xlsx' ;
			} catch(e) {
				// pass through
			}
		}
		window.location.href = fileURL;
	},
	
	importExcel : function() {
		$(".ErrorMsgStyle").hide();
		$(".SuccessMsgStyle").hide();
		VTUtilJS.importExcelUtils(function(data) {
			if (!VTUtilJS.isNullOrEmpty(data.message)) {
				if (data.fileNameFail) {
					var msg = 'Có lỗi trong file import, không thực hiện nhập dữ liệu. <a href="'+ data.fileNameFail +'">Xem chi tiết lỗi</a>';
					VTUtilJS.showMessageBanner(true, msg);
				} else {
					VTUtilJS.showMessageBanner(true, data.message);
				}
			} else {
				VTUtilJS.showMessageBanner(false, 'Import thành công');
			}
		}, 'importFrm', 'excelFile');
	},
	
	exportExcel:function(){
		$('.ErrorMsgStyle').hide();
		$('#fakefilepc').val('');
		$('#excelFile').val('');
		
		var promotionCode = $('#promotionCode').val().trim();
		var lstApParamId = null;
		if($('#typeCode').val() == null || $('#typeCode').val().length == 0){
			lstApParamId = [];
			lstApParamId.push(-1);
		}else{
			lstApParamId = $('#typeCode').val();
			if (lstApParamId[0] == -1) {
				lstApParamId.splice(1);
			}
		}
		var shopCode = $('#shopCode').val().trim();
		var promotionName = $('#promotionName').val().trim();
		var status = 1;
		//if($('#permissionUser').val() == 'true' ){
			status = $('#status').val().trim();
		//}
		var proType = 1;//$("#proType").val();
		var msg = '';
		var fDate = $('#fromDate').val().trim();
		var tDate = $('#toDate').val().trim();
		if (fDate.length > 0 && !Utils.isDate(fDate)) {
			msg = 'Từ ngày không hợp lệ, vui lòng nhập theo định dạng dd/MM/yyyy';
			$('#fromDate').focus();
		}
		if (msg.length == 0 && tDate.length > 0 && !Utils.isDate(tDate)) {
			msg = 'Đến ngày không hợp lệ, vui lòng nhập theo định dạng dd/MM/yyyy';
			$('#toDate').focus();
		}
		if(msg.length == 0 && fDate.length > 0 && tDate.length > 0 && !Utils.compareDate(fDate, tDate)){
			msg = 'Từ ngày phải là ngày trước hoặc cùng ngày với Đến ngày';
			$('#fromDate').focus();
		}
		if(msg.length > 0){
			$('#errMsg').html(msg).show();
			return false;
		}
		var params = {
				code: promotionCode,
				name: promotionName,
				proType: proType,
				shopCode: shopCode,
				lstTypeId: lstApParamId.join(","),
				fromDate: fDate,
				toDate: tDate,
				status: status
		};
//		try {
//			params.reportCode = $('#function_code').val();
//		} catch(e) {
//			// Oop! Error
//		}
		ReportUtils.exportReport("/promotion/export", params, "errorMsg");
//		Utils.getJSONDataByAjax(params, '/promotion/export',function(data) {
//			if (!data.error) {
//				var filePath = data.path || data.view;
//				if (filePath) {
//					window.location.href = ReportUtils.buildReportFilePath(filePath);
//				}
//			} else {
//				$('#errMsg').html(data.errMsg).show();
//			}
//		});
	},

	searchProgram: function() {
		$('.ErrorMsgStyle').hide();
		$('#fakefilepc').val('');
		$('#excelFile').val('');
		
		var promotionCode = $('#promotionCode').val().trim();
		var lstApParamId = null;
		if($('#typeCode').val() == null || $('#typeCode').val().length == 0){
			lstApParamId = [];
			lstApParamId.push(-1);
		}else{
			lstApParamId = $('#typeCode').val();
			if (lstApParamId[0] == -1) {
				lstApParamId.splice(1);
			}
		}
		var shopCode = $('#shopCode').val().trim();
		var promotionName = $('#promotionName').val().trim();
		var status = 1;
		//if($('#permissionUser').val() == 'true' ){
			status = $('#status').val().trim();
		//}
		var proType = 1;//$("#proType").val();
		var msg = '';
		var fDate = $('#fromDate').val().trim();
		var tDate = $('#toDate').val().trim();
		if (fDate.length > 0 && !Utils.isDate(fDate)) {
			msg = 'Từ ngày không hợp lệ, vui lòng nhập theo định dạng dd/MM/yyyy';
			$('#fromDate').focus();
		}
		if (msg.length == 0 && tDate.length > 0 && !Utils.isDate(tDate)) {
			msg = 'Đến ngày không hợp lệ, vui lòng nhập theo định dạng dd/MM/yyyy';
			$('#toDate').focus();
		}
		if(msg.length == 0 && fDate.length > 0 && tDate.length > 0 && !Utils.compareDate(fDate, tDate)){
			msg = 'Từ ngày phải là ngày trước hoặc cùng ngày với Đến ngày';
			$('#fromDate').focus();
		}
		if(msg.length > 0){
			$('#errorMsgSearch').html(msg).show();
			return false;
		}
		var params = {
				code: promotionCode,
				name: promotionName,
				proType: proType,
				shopCode: shopCode,
				lstTypeId: lstApParamId.join(","),
				fromDate: fDate,
				toDate: tDate,
				status: status
		};
		$('#grid').datagrid("reload", params);

		return false;
	},
	
	showTab: function(tabId, url) {
		if ($("#" + tabId + ".Active").length > 0) {
			return;
		}
		$(".ErrorMsgStyle").hide();
		$("#tabSectionDiv a.Active").removeClass("Active");
		$("#" + tabId).addClass("Active");
		url = url + "?promotionId="+$("#masterDataArea #id").val().trim();
		VTUtilJS.getFormHtml('masterDataArea', url, function(html) {
			$('#divTab').html(html);
			Utils.functionAccessFillControl();
		});
	},
	
	returnGroupTextMua: function(promotionType, listDetail, globalQuantity, globalAmount) {
		if(promotionType == 'ZV01' || promotionType == 'ZV02' || promotionType == 'ZV03') {
			/**
			 * productCode + globalQuantity
			 */
			if(VTUtilJS.isNullOrEmpty(listDetail) || VTUtilJS.isNullOrEmpty(listDetail.length) || listDetail.length == 0 || VTUtilJS.isNullOrEmpty(globalQuantity) || globalQuantity == 0) {
				return '';
			}
			if(!$.isArray(listDetail)) {
				return '';
			} else {
				var text = '(';
				for(var i = 0; i < listDetail.length; i++) {
					var levelDetail = listDetail[i];
					if(text == '(') {
						text += levelDetail.productCode +((levelDetail.isRequired == 1)?'*':'') + '(' + globalQuantity + ')';
					} else {
						text += ', ' + levelDetail.productCode +((levelDetail.isRequired == 1)?'*':'') + '(' + globalQuantity + ')';
					}
				}
				text += ')';
				return text;
			}
		} else if(promotionType == 'ZV04' || promotionType == 'ZV05' || promotionType == 'ZV06') {
			/**
			 * productCode + globalAmount
			 */
			if(VTUtilJS.isNullOrEmpty(listDetail) || VTUtilJS.isNullOrEmpty(listDetail.length) || listDetail.length == 0 || VTUtilJS.isNullOrEmpty(globalAmount) || globalAmount == 0) {
				return '';
			}
			if(!$.isArray(listDetail)) {
				return '';
			} else {
				var text = '(';
				for(var i = 0; i < listDetail.length; i++) {
					var levelDetail = listDetail[i];
					if(text == '(') {
						text += levelDetail.productCode +((levelDetail.isRequired == 1)?'*':'') + '(' + VTUtilJS.formatCurrency(globalAmount) + ')';
					} else {
						text += ', ' + levelDetail.productCode +((levelDetail.isRequired == 1)?'*':'') + '(' + VTUtilJS.formatCurrency(globalAmount) + ')';
					}
				}
				text += ')';
				return text;
			}
		} else if(promotionType == 'ZV07' || promotionType == 'ZV08' || promotionType == 'ZV09' || promotionType == 'ZV10' || promotionType == 'ZV11' || promotionType == 'ZV12') {
			if(VTUtilJS.isNullOrEmpty(listDetail) || VTUtilJS.isNullOrEmpty(listDetail.length) || listDetail.length == 0) {
				return '';
			}
			if(!$.isArray(listDetail)) {
				return '';
			} else {
				var text = '(';
				for(var i = 0; i < listDetail.length; i++) {
					var levelDetail = listDetail[i];
					if(text == '(') {
						text += levelDetail.productCode +((levelDetail.isRequired == 1)?'*':'');
					} else {
						text += ', ' + levelDetail.productCode +((levelDetail.isRequired == 1)?'*':'');
					}
				}
				text += ')';
				return text;
			}
		} else if(promotionType == 'ZV13' || promotionType == 'ZV14' || promotionType == 'ZV15' || promotionType == 'ZV16' || promotionType == 'ZV17' || promotionType == 'ZV18') {
			if(VTUtilJS.isNullOrEmpty(listDetail) || VTUtilJS.isNullOrEmpty(listDetail.length) || listDetail.length == 0) {
				return '';
			}
			if(!$.isArray(listDetail)) {
				return '';
			} else {
				var text = '(';
				for(var i = 0; i < listDetail.length; i++) {
					var levelDetail = listDetail[i];
					if(text == '(') {
						if(VTUtilJS.isNullOrEmpty(levelDetail.value)) {
							return '';
						}
						text += levelDetail.productCode +(levelDetail.isRequired?'*':'') + '(' + levelDetail.value + ')';
					} else {
						if(VTUtilJS.isNullOrEmpty(levelDetail.value)) {
							return '';
						}
						text += ', ' + levelDetail.productCode +(levelDetail.isRequired?'*':'') + '(' + levelDetail.value + ')';
					}
				}
				text += ')';
				return text;
			}
		} else {
			
		}
	},
	
	returnGroupTextKM: function(promotionType, listDetail, globalQuantity, globalAmount) {
		var text = '(';
		for(var i = 0; i < listDetail.length; i++) {
			var levelDetail = listDetail[i];
			if(text == '(') {
				if(VTUtilJS.isNullOrEmpty(levelDetail.value)) {
					return '';
				}
				text += levelDetail.productCode +((levelDetail.isRequired == 1)?'*':'') + '(' + levelDetail.value + ')';
			} else {
				if(VTUtilJS.isNullOrEmpty(levelDetail.value)) {
					return '';
				}
				text += ', ' + levelDetail.productCode +((levelDetail.isRequired == 1)?'*':'') + '(' + levelDetail.value + ')';
			}
		}
		text += ')';
		return text;
	},
		
	editLevelMuaAddProduct: function() {
		if(($('#promotionType').val() == 'ZV01' || $('#promotionType').val() == 'ZV02' || $('#promotionType').val() == 'ZV03' ||
				$('#promotionType').val() == 'ZV04' || $('#promotionType').val() == 'ZV05' || $('#promotionType').val() == 'ZV06')
				&& $('#gridProduct').datagrid('getRows').length == 1) {
			return;
		} else if(($('#promotionType').val() == 'ZV07' || $('#promotionType').val() == 'ZV08' || $('#promotionType').val() == 'ZV09' ||
				$('#promotionType').val() == 'ZV10' || $('#promotionType').val() == 'ZV11' || $('#promotionType').val() == 'ZV12' ||
				$('#promotionType').val() == 'ZV13' || $('#promotionType').val() == 'ZV14' || $('#promotionType').val() == 'ZV15' ||
				$('#promotionType').val() == 'ZV16' || $('#promotionType').val() == 'ZV17' || $('#promotionType').val() == 'ZV18')
				&& $('#gridLevel').datagrid('getRows').length > 1 && $('#gridProduct').datagrid('getRows').length != 0) {
			return;
		}
		$('#gridProduct').datagrid('appendRow', {isRequired : (($('#promotionType').val() == 'ZV01' || $('#promotionType').val() == 'ZV02' || $('#promotionType').val() == 'ZV03' || $('#promotionType').val() == 'ZV04' || $('#promotionType').val() == 'ZV05' || $('#promotionType').val() == 'ZV06' || $('#promotionType').val() == 'ZV13' || $('#promotionType').val() == 'ZV14' || $('#promotionType').val() == 'ZV15' || $('#promotionType').val() == 'ZV16' || $('#promotionType').val() == 'ZV17' || $('#promotionType').val() == 'ZV18') ? 1 : 0)});
		editIndex = $('#gridProduct').datagrid('getRows').length-1;
		$('#gridProduct').datagrid('selectRow', editIndex).datagrid('beginEdit', editIndex);
	},
	
	editLevelKMAddProduct: function() {
		$('#gridProduct').datagrid('appendRow', {});
		editIndex = $('#gridProduct').datagrid('getRows').length-1;
		$('#gridProduct').datagrid('selectRow', editIndex).datagrid('beginEdit', editIndex);
	},
	
	editLevelMuaDeleteProduct: function(index) {
		$('#errAddProductMsg').html('').hide();
		if(($('#promotionType').val() == 'ZV01' || $('#promotionType').val() == 'ZV02' || $('#promotionType').val() == 'ZV03' ||
				$('#promotionType').val() == 'ZV04' || $('#promotionType').val() == 'ZV05' || $('#promotionType').val() == 'ZV06') && $('#gridProduct').datagrid('getRows').length == 1) {
			$('#errAddProductMsg').html('Mức không thể để trống. Bạn không thể xóa sản phẩm').show();
			return;
		} else if(($('#promotionType').val() == 'ZV07' || $('#promotionType').val() == 'ZV08' || $('#promotionType').val() == 'ZV09' ||
				$('#promotionType').val() == 'ZV10' || $('#promotionType').val() == 'ZV11' || $('#promotionType').val() == 'ZV12') && $('#gridLevel').datagrid('getRows').length != 1) {
			$('#errAddProductMsg').html('Nhóm đang có nhiều mức. Bạn không thể xóa sản phẩm').show();
			return;
		}
		
		if(index != $('#gridProduct').datagrid('getRows').length - 1) {
			$('#errAddProductMsg').html('Bạn chỉ được xóa dòng cuối cùng').show();
			return;
		}
		$.messager.confirm("Xác nhận", "Bạn có muốn xóa sản phẩm của mức?", function(r) {
			if(r) {
				var rowProduct = $('#gridProduct').datagrid('getRows')[index];
				if(!VTUtilJS.isNullOrEmpty(rowProduct.id)) {
					var params = new Object();
					params.levelDetailId = rowProduct.id;
					VTUtilJS.postFormJson(params, '/promotion/delete-level-detail', function(data) {
						if(data.error) {
							$('#errAddProductMsg').html(data.errMsg).show();
						} else {
							$('#gridProduct').datagrid('deleteRow', index);
						}
					});
				}
			}
		});
	},
	
	editLevelKMDeleteProduct: function(index) {
		$('#errAddProductMsg').html('').hide();
		if($('#gridLevel').datagrid('getRows').length != 1) {
			$('#errAddProductMsg').html('Nhóm đang có nhiều mức. Bạn không thể xóa sản phẩm').show();
			return;
		}
		
		if(index != $('#gridProduct').datagrid('getRows').length - 1) {
			$('#errAddProductMsg').html('Bạn chỉ được xóa dòng cuối cùng').show();
			return;
		}
		$.messager.confirm("Xác nhận", "Bạn có muốn xóa sản phẩm của mức", function(r) {
			if(r) {
				var rowProduct = $('#gridProduct').datagrid('getRows')[index];
				if(!VTUtilJS.isNullOrEmpty(rowProduct.id)) {
					var params = new Object();
					params.levelDetailId = rowProduct.id;
					VTUtilJS.postFormJson(params, '/promotion/delete-level-detail', function(data) {
						if(data.error) {
							$('#errAddProductMsg').html(data.errMsg).show();
						} else {
							$('#gridProduct').datagrid('deleteRow', index);
						}
					});
				}
			}
		});
	},
	
	editLevelMua: function(rowLeveIndex, groupLevelId) {
		var html = '<div id="popup-product-container">\
			<div id="popup-edit-level">\
			<div class="PopupContentMid">\
			<div class="GeneralForm Search1Form">';
		if($('#promotionType').val() == 'ZV13' || $('#promotionType').val() == 'ZV14' || $('#promotionType').val() == 'ZV15' || $('#promotionType').val() == 'ZV16' || $('#promotionType').val() == 'ZV17' || $('#promotionType').val() == 'ZV18') {
			html +=	'<div class="BoxSelect BoxSelect2">';
			PromotionProgram._levelValueType = ($('#promotionType').val() == 'ZV13' || $('#promotionType').val() == 'ZV14' || $('#promotionType').val() == 'ZV15') ? 1 : 2;
			if(VTUtilJS.isNullOrEmpty(PromotionProgram._levelValueType)) {
				html += '<select id="valueType" class="MySelectBoxClass"><option value="1" selected="selected">Số lượng</option>\
				<option value="2">Số tiền</option>\
				</select>\
				</div>';
			} else if(PromotionProgram._levelValueType == 1) {
				html += '<select id="valueType" disabled="disabled" class="MySelectBoxClass"><option value="1" selected="selected">Số lượng</option>\
					</select>\
					</div>';
			} else if(PromotionProgram._levelValueType == 2) {
				html += '<select id="valueType" disabled="disabled" class="MySelectBoxClass"><option value="2" selected="selected">Số tiền</option>\
					</select>\
					</div>';
			}
		}
		html+= '<div class="Clear"></div>\
			<div id="gridProductContainer">\
			<div id="gridProduct"></div>\
			</div>\
			<div class="Clear"></div>\
			</div>\
			<div class="BtnCenterSection">\
			<button id="btn-update" '+(($('#statusHidden').val() == 0 || $('#statusHidden').val() == 1) ? 'style="display:none;"' : '')+' class="BtnGeneralStyle BtnGeneralMStyle">Cập nhật</button>\
			</div>\
			<div class="Clear"></div>\
			<p class="ErrorMsgStyle SpriteErr" style="display: none;" id="errAddProductMsg"></p></div></div></div>\
			</div>';
		$("body").append(html);
		var row = $('#gridLevel').datagrid('getRows')[rowLeveIndex];
		var detail = [];
		if(row.detail != null && row.detail != undefined && $.isArray(row.detail) && row.detail.length != 0) {
			for(var i = 0; i < row.detail.length; i++) {
				detail.push(row.detail[i]);
			}
		}
		$('#popup-edit-level').dialog({
			title: 'Thông tin sản phẩm của nhóm',
			width: 750,
			heigth: "auto",
			modal: true, close: false, cache: false,
			onOpen: function() {
				var isDisable = false;
				switch ($('#promotionType').val()) {
				case 'ZV01':
					isDisable = true;
					break;
				case 'ZV02':
					isDisable = true;
					break;
				case 'ZV03':
					isDisable = true;
					break;
				case 'ZV04':
					isDisable = true;
					break;
				case 'ZV05':
					isDisable = true;
					break;
				case 'ZV06':
					isDisable = true;
					break;
				case 'ZV07':
					isDisable = true;
					break;
				case 'ZV08':
					isDisable = true;
					break;
				case 'ZV09':
					isDisable = true;
					break;
				case 'ZV10':
					isDisable = true;
					break;
				case 'ZV11':
					isDisable = true;
					break;
				case 'ZV12':
					isDisable = true;
					break;
				default:
					break;
				}
				$('#valueType').customStyle();
				$('#gridProduct').datagrid({
					data : detail,
					width: $("#gridProductContainer").width(),
    				height: 'auto',
    				singleSelect: true,
    				fitColumns: true,
    				scrollbarSize:0,
    				columns: [[
    	    					{field:'id', title:"Mức", width:30,align:'center',sortable:false,resizable:false,formatter:function(value,row,index) {
    	    						return index + 1;
    	    					}},
    	    					{field:'productCode', title:"Sản phẩm mua", width:100,align:'left',sortable:false,resizable:false,formatter:function(value, row, index) {
    	    						return VTUtilJS.XSSEncode(value);
    	    					}, 
    	    						editor: {
    	    							type:'combobox',
    	    							 options:{
	    								 	valueField : 'productCode',
	    								 	textField : 'productCode',
	    								 	//disabled : (rowLeveIndex == 0 && $('#gridLevel').datagrid('getRows').length == 1) ? false : true,
	    								 	disabled : true,
	    								 	data : PromotionProgram._listProduct.valArray,
	    								 	formatter:function(row){
	    			                            return row.productCode + ' - ' +row.productName;
	    			                        },
	    								 	onSelect: function(r) {
	    								 		var row = $(this).parent().parent().parent().parent().parent().parent().parent();
	    								 		row.find('td[field=productName] div').html(r.productName);
	    								 	}
    	    							 }
    	    						}
    	    					},
    	    					{field:'productName', title:"Tên sản phẩm", width:180,align:'left',sortable:false,resizable:false,formatter:CommonFormatter.formatNormalCell},
    	    					{field:'value', title:"Giá trị", width:80,align:'center',sortable:false,resizable:false, editor: {type:'currency', options : {isDisable : isDisable}}},
    	    					{field:'isRequired', title:"Bắt buộc", width:50,align:'center',sortable:false,resizable:false,editor:{type:'checkbox', options:{on:'1',off:'0', isDisable : (($('#gridLevel').datagrid('getRows').length > 1 && detail.length > 0) || $('#promotionType').val() == 'ZV01' || $('#promotionType').val() == 'ZV02' || $('#promotionType').val() == 'ZV03' || $('#promotionType').val() == 'ZV04' || $('#promotionType').val() == 'ZV05' || $('#promotionType').val() == 'ZV06' || $('#promotionType').val() == 'ZV13' || $('#promotionType').val() == 'ZV14' || $('#promotionType').val() == 'ZV15' || $('#promotionType').val() == 'ZV16' || $('#promotionType').val() == 'ZV17' || $('#promotionType').val() == 'ZV18')}}}/*,
    	    					//{field:'isRequired', title:"Bắt buộc", width:50,align:'center',sortable:false,resizable:false,editor:{type:'checkbox', options:{on:'1',off:'0', isDisable : true}}}/*,
    	    					{field:'delete', title:'<a '+(($('#statusHidden').val() == 0 || $('#statusHidden').val() == 1) ? 'style="display:none;"' : '')+' onclick="PromotionProgram.editLevelMuaAddProduct();"><span style="cursor:pointer"><img title="Thêm mới sản phẩm" src="/resources/images/icon_add.png"/></span></a>', width:30,align:'center',sortable:false,resizable:false,formatter:function(value,row,index){
    	    						return '<a '+(($('#statusHidden').val() == 0 || $('#statusHidden').val() == 1) ? 'style="display:none;"' : '')+' onclick="PromotionProgram.editLevelMuaDeleteProduct('+index+')"><span style="cursor:pointer"><img title="Xóa sản phẩm" src="/resources/images/icon_delete.png"/></span></a>';
    	    					}},*/
    	    				]],
    	    				onDblClickRow: function(index,row) {
    	    					if($('#statusHidden').val() == 2) {
    	    						$('#gridProduct').datagrid('beginEdit', index);
    	    					}
    	    				},
    	    				onLoadSuccess: function(data) {
    	    					if(detail == null || detail == undefined || ($.isArray(detail) && detail.length == 0)) {
    	    						if(rowLeveIndex == 0) {
    	    							$('#gridProduct').datagrid('appendRow', {isRequired : (($('#promotionType').val() == 'ZV01' || $('#promotionType').val() == 'ZV02' || $('#promotionType').val() == 'ZV03' || $('#promotionType').val() == 'ZV04' || $('#promotionType').val() == 'ZV05' || $('#promotionType').val() == 'ZV06' || $('#promotionType').val() == 'ZV13' || $('#promotionType').val() == 'ZV14' || $('#promotionType').val() == 'ZV15' || $('#promotionType').val() == 'ZV16' || $('#promotionType').val() == 'ZV17' || $('#promotionType').val() == 'ZV18') ? 1 : 0)});
    	    						} else if($('#promotionType').val() == 'ZV01' || $('#promotionType').val() == 'ZV02' || $('#promotionType').val() == 'ZV03' || $('#promotionType').val() == 'ZV04' || $('#promotionType').val() == 'ZV05' || $('#promotionType').val() == 'ZV06') {
    	    							var listDetail = $('#gridProduct').datagrid('getRows')[0].detail;
    	    							var productCode = listDetail[0].productCode;
    	    						}
    	    						editIndex = $('#gridProduct').datagrid('getRows').length-1;
    	    						$('#gridProduct').datagrid('selectRow', editIndex).datagrid('beginEdit', editIndex);
    	    					}
    	    					/*var rows = $('#gridProduct').datagrid('getRows');
    	    					for(var i = 0; i < rows.length; i++) {
    	    						$('#gridProduct').datagrid('beginEdit', i);
    	    					}*/
    	    				}
				});
				$('#popup-edit-level #btn-update').bind('click', function() {
					$('#errAddProductMsg').html('').hide;
					if($('#valueType').length == 0 && ($('#promotionType').val() == 'ZV01' || $('#promotionType').val() == 'ZV02' || $('#promotionType').val() == 'ZV03' ||
							$('#promotionType').val() == 'ZV07' || $('#promotionType').val() == 'ZV08' || $('#promotionType').val() == 'ZV09')) {
						PromotionProgram._levelValueType = 1;
					} else if($('#valueType').length == 0 && ($('#promotionType').val() == 'ZV04' || $('#promotionType').val() == 'ZV05' || $('#promotionType').val() == 'ZV06' || 
							$('#promotionType').val() == 'ZV10' || $('#promotionType').val() == 'ZV11' || $('#promotionType').val() == 'ZV12')) {
						PromotionProgram._levelValueType = 2;
					} else {
						PromotionProgram._levelValueType = $('#valueType').val();
					}
					
					$('#gridProduct').datagrid('acceptChanges');
					var rows = $('#gridProduct').datagrid('getRows');
					var listDetail = new Array();
					var listProductCode = new Array();
					for(var i = 0; i < rows.length; i++) {
						var row = rows[i];
						var detail = new Object();
						if(VTUtilJS.isNullOrEmpty(row.productCode)) {
							$('#errAddProductMsg').html('Sản phẩm ở dòng ' + (i+1) + ' chưa được nhập').show();
							/*for(var ii = 0; ii < rows.length; ii++) {
								$('#gridProduct').datagrid('beginEdit', ii);
							}*/
							return;
						} else if(listProductCode.indexOf(row.productCode) != -1) {
							$('#errAddProductMsg').html('Sản phẩm ở dòng ' + (i+1) + ' bị trùng mã sản phẩm').show();
							/*for(var ii = 0; ii < rows.length; ii++) {
								$('#gridProduct').datagrid('beginEdit', ii);
							}*/
							return;
						} else {
							if(PromotionProgram._listProduct.get(row.productCode) == null) {
								$('#errAddProductMsg').html('Sản phẩm ở dòng ' + (i+1) + ' không tồn tại trong hệ thống').show();
								/*for(var ii = 0; ii < rows.length; ii++) {
									$('#gridProduct').datagrid('beginEdit', ii);
								}*/
								return;
							}
							detail.productCode = row.productCode;
							detail.productName = PromotionProgram._listProduct.get(row.productCode).productName;
						}
						if($('#promotionType').val() == 'ZV13' || $('#promotionType').val() == 'ZV14' || $('#promotionType').val() == 'ZV15' ||
								$('#promotionType').val() == 'ZV16' || $('#promotionType').val() == 'ZV17' || $('#promotionType').val() == 'ZV18') {
							if(VTUtilJS.isNullOrEmpty(row.value) || row.value == 0) {
								$('#errAddProductMsg').html('Giá trị ở dòng ' + (i+1) + ' chưa được nhập').show();
								/*for(var ii = 0; ii < rows.length; ii++) {
									$('#gridProduct').datagrid('beginEdit', ii);
								}*/
								return;
							} else if(isNaN(row.value) || Number(row.value) <= 0) {
								$('#errAddProductMsg').html('Giá trị ở dòng ' + (i+1) + ' phải là số nguyên lớn hơn 0').show();
								/*for(var ii = 0; ii < rows.length; ii++) {
									$('#gridProduct').datagrid('beginEdit', ii);
								}*/
								return;
							} else {
								detail.value = row.value;
								detail.valueType = 1;
							}
						}
						if($('#promotionType').val() == 'ZV07' || $('#promotionType').val() == 'ZV08' || $('#promotionType').val() == 'ZV09' || $('#promotionType').val() == 'ZV10' || $('#promotionType').val() == 'ZV11' || $('#promotionType').val() == 'ZV12') {
							if(i != 0) {
								var preRow = rows[i-1]; 
								if(preRow.isRequired != row.isRequired) {
									if(preRow.isRequired || preRow.isRequired == 1) {
										$('#errAddProductMsg').html('Dòng ' + (i+1) + ' phải được check cột bắt buộc').show();
										/*for(var ii = 0; ii < rows.length; ii++) {
											$('#gridProduct').datagrid('beginEdit', ii);
										}*/
										return;
									} else {
										$('#errAddProductMsg').html('Dòng ' + (i+1) + ' không được check cột bắt buộc').show();
										/*for(var ii = 0; ii < rows.length; ii++) {
											$('#gridProduct').datagrid('beginEdit', ii);
										}*/
										return;
									}
								}
							}
						}
						detail.isRequired = row.isRequired;
						detail.groupLevelId = groupLevelId;
						listDetail.push(detail);
					}
					$('#gridLevel').datagrid('getRows')[rowLeveIndex].detail = listDetail;
					var orderEd = $('#gridLevel').datagrid('getEditor', {index : 0, field : 'orderNumber'});
					if(orderEd != null) {
						var orderNumber = $(orderEd.target).val();
						$('#gridLevel').datagrid('getRows')[rowLeveIndex].orderNumber = orderNumber;
					}
					$('#gridLevel').datagrid('getRows')[rowLeveIndex].groupText = PromotionProgram.returnGroupTextMua($('#promotionType').val(), listDetail, $('#gridLevel').datagrid('getRows')[rowLeveIndex].minQuantity, $('#gridLevel').datagrid('getRows')[rowLeveIndex].minAmount);
					$("#popup-edit-level").dialog('close');
					$('#gridLevel').datagrid('refreshRow', rowLeveIndex);
					$('#gridLevel').datagrid('beginEdit', rowLeveIndex);
				});
			},
			onClose: function() {
				$("#popup-edit-level").dialog("destroy");
				$("#popup-product-container").remove();
			}
		});
	},
	
	editLevelKM: function(rowLeveIndex, groupLevelId) {
		var html = '<div id="popup-product-container">\
			<div id="popup-edit-level">\
			<div class="PopupContentMid">\
			<div class="GeneralForm Search1Form">\
			<div class="Clear"></div>\
			<div id="gridProductContainer">\
			<div id="gridProduct"></div>\
			</div>\
			<div class="Clear"></div>\
			</div>\
			<div class="BtnCenterSection">\
			<button id="btn-update" '+(($('#statusHidden').val() == 0 || $('#statusHidden').val() == 1) ? 'style="display:none;"' : '')+' class="BtnGeneralStyle BtnGeneralMStyle">Cập nhật</button>\
			</div>\
			<div class="Clear"></div>\
			<p class="ErrorMsgStyle SpriteErr" style="display: none;" id="errAddProductMsg"></p></div></div></div>\
			</div>';
		$("body").append(html);
		var row = $('#gridLevel').datagrid('getRows')[rowLeveIndex];
		var detail = [];
		if(row.detail != null && row.detail != undefined && $.isArray(row.detail) && row.detail.length != 0) {
			for(var i = 0; i < row.detail.length; i++) {
				detail.push(row.detail[i]);
			}
		}
		$('#popup-edit-level').dialog({
			title: 'Thông tin sản phẩm của nhóm',
			width: 750,
			heigth: "auto",
			modal: true, close: false, cache: false,
			onOpen: function() {
				$('#gridProduct').datagrid({
					data : detail,
					width: $("#gridProductContainer").width(),
    				height: 'auto',
    				singleSelect: true,
    				fitColumns: true,
    				scrollbarSize:0,
    				columns: [[
    					{field:'id', title:"Mức", width:30,align:'center',sortable:false,resizable:false,formatter:function(value,row,index) {
    						return index + 1;
    					}},
    					{field:'productCode', title:"Sản phẩm mua", width:100,align:'left',sortable:false,resizable:false,formatter:function(value, row, index) {
    						return VTUtilJS.XSSEncode(value);
    					}, 
    						editor: {
    							type:'combobox',
    							 options:{
								 	valueField : 'productCode',
								 	textField : 'productCode',
								 	disabled : true,
								 	data : PromotionProgram._listProduct.valArray,
								 	formatter:function(row){
			                            return row.productCode + ' - ' +row.productName;
			                        },
								 	onSelect: function(r) {
								 		var row = $(this).parent().parent().parent().parent().parent().parent().parent();
								 		row.find('td[field=productName] div').html(r.productName);
								 	}
    							 }
    						}
    					},
    					{field:'productName', title:"Tên sản phẩm", width:180,align:'left',sortable:false,resizable:false,formatter:CommonFormatter.formatNormalCell},
    					{field:'value', title:"Giá trị", width:80,align:'center',sortable:false,resizable:false, editor: {type:'currency'}},
    					{field:'isRequired', title:"Bắt buộc", width:50,align:'center',sortable:false,resizable:false,editor:{type:'checkbox', options:{on:'1',off:'0'}}}/*,
    					{field:'delete', title:'<a '+(($('#statusHidden').val() == 0 || $('#statusHidden').val() == 1) ? 'style="display:none;"' : '')+' onclick="PromotionProgram.editLevelKMAddProduct();"><span style="cursor:pointer"><img title="Thêm mới sản phẩm" src="/resources/images/icon_add.png"/></span></a>', width:30,align:'center',sortable:false,resizable:false,formatter:function(value,row,index){
    						return '<a '+(($('#statusHidden').val() == 0 || $('#statusHidden').val() == 1) ? 'style="display:none;"' : '')+' onclick="PromotionProgram.editLevelKMDeleteProduct('+index+')"><span style="cursor:pointer"><img title="Xóa sản phẩm" src="/resources/images/icon_delete.png"/></span></a>';
    					}},*/
    				]],
    				onDblClickRow: function(index,row) {
    					if($('#statusHidden').val() == 2) {
    						$('#gridProduct').datagrid('beginEdit', index);
    					}
    				},
    				onLoadSuccess: function(data) {
    					if(detail == null || detail == undefined || ($.isArray(detail) && detail.length == 0)) {
    						$('#gridProduct').datagrid('appendRow', {});
    						editIndex = $('#gridProduct').datagrid('getRows').length-1;
    						$('#gridProduct').datagrid('selectRow', editIndex).datagrid('beginEdit', editIndex);
    					}
    					/*var rows = $('#gridProduct').datagrid('getRows');
    					for(var i = 0; i < rows.length; i++) {
    						$('#gridProduct').datagrid('beginEdit', i);
    					}*/
    				}
				});
				$('#popup-edit-level #btn-update').bind('click', function() {
					$('#errAddProductMsg').html('').hide;
					PromotionProgram._levelValueType = $('#valueType').val();
					$('#gridProduct').datagrid('acceptChanges');
					var rows = $('#gridProduct').datagrid('getRows');
					var listDetail = new Array();
					var listProductCode = new Array();
					for(var i = 0; i < rows.length; i++) {
						var row = rows[i];
						var detail = new Object();
						if(VTUtilJS.isNullOrEmpty(row.productCode)) {
							$('#errAddProductMsg').html('Sản phẩm ở dòng ' + (i+1) + ' chưa được nhập').show();
							/*for(var ii = 0; ii < rows.length; ii++) {
								$('#gridProduct').datagrid('beginEdit', ii);
							}*/
							return;
						} else if(listProductCode.indexOf(row.productCode) != -1) {
							$('#errAddProductMsg').html('Sản phẩm ở dòng ' + (i+1) + ' bị trùng mã sản phẩm').show();
							/*for(var ii = 0; ii < rows.length; ii++) {
								$('#gridProduct').datagrid('beginEdit', ii);
							}*/
							return;
						}  else {
							if(PromotionProgram._listProduct.get(row.productCode) == null) {
								$('#errAddProductMsg').html('Sản phẩm ở dòng ' + (i+1) + ' không tồn tại trong hệ thống').show();
								/*for(var ii = 0; ii < rows.length; ii++) {
									$('#gridProduct').datagrid('beginEdit', ii);
								}*/
								return;
							}
							detail.productCode = row.productCode;
							detail.productName = PromotionProgram._listProduct.get(row.productCode).productName;
						}
						if(VTUtilJS.isNullOrEmpty(row.value) || row.value == 0) {
							$('#errAddProductMsg').html('Giá trị ở dòng ' + (i+1) + ' chưa được nhập').show();
							/*for(var ii = 0; ii < rows.length; ii++) {
								$('#gridProduct').datagrid('beginEdit', ii);
							}*/
							return;
						} else if(isNaN(row.value) || Number(row.value) <= 0) {
							$('#errAddProductMsg').html('Giá trị ở dòng ' + (i+1) + ' phải là số nguyên lớn hơn 0').show();
							/*for(var ii = 0; ii < rows.length; ii++) {
								$('#gridProduct').datagrid('beginEdit', ii);
							}*/
							return;
						} else {
							detail.value = row.value;
							detail.valueType = $('#popup-edit-level #valueType').val();
						}
						detail.isRequired = row.isRequired;
						detail.groupLevelId = groupLevelId;
						listDetail.push(detail);
					}
					$('#gridLevel').datagrid('getRows')[rowLeveIndex].detail = listDetail;
					$('#gridLevel').datagrid('getRows')[rowLeveIndex].groupText = PromotionProgram.returnGroupTextKM($('#promotionType').val(), listDetail, $('#gridLevel').datagrid('getRows')[rowLeveIndex].minQuantity, $('#gridLevel').datagrid('getRows')[rowLeveIndex].minAmount);
					$("#popup-edit-level").dialog('close');
					$('#gridLevel').datagrid('refreshRow', rowLeveIndex);
					$('#gridLevel').datagrid('beginEdit', rowLeveIndex);
				});
			},
			onClose: function() {
				$("#popup-edit-level").dialog("destroy");
				$("#popup-product-container").remove();
			}
		});
	},
	
	addLevelMua: function() {
		if(($('#promotionType').val() == 'ZV01' || $('#promotionType').val() == 'ZV02' || $('#promotionType').val() == 'ZV03' || $('#promotionType').val() == 'ZV04' || $('#promotionType').val() == 'ZV05' || $('#promotionType').val() == 'ZV06' ||
				$('#promotionType').val() == 'ZV07' || $('#promotionType').val() == 'ZV08' || $('#promotionType').val() == 'ZV09' || $('#promotionType').val() == 'ZV10' || $('#promotionType').val() == 'ZV11' || $('#promotionType').val() == 'ZV12') && $('#gridLevel').datagrid('getRows').length != 0) {
			var firstRow = $('#gridLevel').datagrid('getRows')[0];
			if(firstRow.detail == null || ($.isArray(firstRow.detail) && firstRow.detail.length == 0)) {
				return;
			}
			var appendRow = {detail:[]};
			for(var i = 0; i < firstRow.detail.length; i++) {
				appendRow.detail.push(firstRow.detail[i]);
			}
			$('#gridLevel').datagrid('appendRow', appendRow);
			var index = $('#gridLevel').datagrid('getRows').length - 1;
			$('#gridLevel').datagrid('selectRow', index).datagrid('beginEdit', index);
		} else if(($('#promotionType').val() == 'ZV13' || $('#promotionType').val() == 'ZV14' || $('#promotionType').val() == 'ZV15' || $('#promotionType').val() == 'ZV16' || $('#promotionType').val() == 'ZV17' || $('#promotionType').val() == 'ZV18') && $('#gridLevel').datagrid('getRows').length != 0) {
			var firstRow = $('#gridLevel').datagrid('getRows')[0];
			if(firstRow.detail == null || ($.isArray(firstRow.detail) && firstRow.detail.length == 0)) {
				return;
			}
			var appendRow = {detail:[]};
			for(var i = 0; i < firstRow.detail.length; i++) {
				var detail = {groupLevelId : firstRow.detail[i].groupLevelId, isRequired : firstRow.detail[i].isRequired, productCode : firstRow.detail[i].productCode, productName : firstRow.detail[i].productName};
				appendRow.detail.push(detail);
			}
			$('#gridLevel').datagrid('appendRow', appendRow);
			var index = $('#gridLevel').datagrid('getRows').length - 1;
			$('#gridLevel').datagrid('selectRow', index).datagrid('beginEdit', index);
		} else {
			$('#gridLevel').datagrid('appendRow', {});
			var index = $('#gridLevel').datagrid('getRows').length - 1;
			$('#gridLevel').datagrid('selectRow', index).datagrid('beginEdit', index);
		}
	},
	
	addLevelKM: function() {
		$('#gridLevel').datagrid('appendRow', {});
		var index = $('#gridLevel').datagrid('getRows').length - 1;
		$('#gridLevel').datagrid('selectRow', index).datagrid('beginEdit', index);
	},
	
	deleteLevelMua: function(index) {
		$('#errMsgGroupDlg').html('').hide();
		if(index != $('#gridLevel').datagrid('getRows').length - 1) {
			$('#errMsgGroupDlg').html('Bạn chỉ được xóa dòng cuối cùng').show();
			return;
		}
		$.messager.confirm("Xác nhận", "Bạn có muốn xóa mức", function(r) {
			if(r) {
				var rowLevel = $('#gridLevel').datagrid('getRows')[index];
				if(!VTUtilJS.isNullOrEmpty(rowLevel.id)) {
					var params = new Object();
					params.levelMuaId = rowLevel.id;
					VTUtilJS.postFormJson(params, '/promotion/delete-level-mua', function(data) {
						if(data.error) {
							$('#errMsgGroupDlg').html(data.errMsg).show();
						} else {
							$('#gridLevel').datagrid('deleteRow', index);
						}
					});
				}
			}
		});
	},
	
	deleteLevelKM: function(index) {
		$('#errMsgGroupDlg').html('').hide();
		if(index != $('#gridLevel').datagrid('getRows').length - 1) {
			$('#errMsgGroupDlg').html('Bạn chỉ được xóa dòng cuối cùng').show();
			return;
		}
		$.messager.confirm("Xác nhận", "Bạn có muốn xóa mức", function(r) {
			if(r) {
				var rowLevel = $('#gridLevel').datagrid('getRows')[index];
				if(!VTUtilJS.isNullOrEmpty(rowLevel.id)) {
					var params = new Object();
					params.levelKMId = rowLevel.id;
					VTUtilJS.postFormJson(params, '/promotion/delete-level-km', function(data) {
						if(data.error) {
							$('#errMsgGroupDlg').html(data.errMsg).show();
						} else {
							$('#gridLevel').datagrid('deleteRow', index);
						}
					});
				}
			}
		});
	},
	
	popupAddLevelMua : function(productGroupId, groupCode, groupName, minQuantity, minAmount, multiple, recursive, stt) {
		var textDisQuantity = '';
		var textDisAmount = '';
		var isDisableGridQuantity = false;
		var isDisableGridAmount = false;
		if($('#promotionType').val() == 'ZV01' || $('#promotionType').val() == 'ZV02' || $('#promotionType').val() == 'ZV03' || 
				$('#promotionType').val() == 'ZV07' || $('#promotionType').val() == 'ZV08' || $('#promotionType').val() == 'ZV09') {
			textDisQuantity = '';
			textDisAmount = 'disabled="disabled"';
			isDisableGridQuantity = false;
			isDisableGridAmount = true;
		} else if($('#promotionType').val() == 'ZV13' || $('#promotionType').val() == 'ZV14' || $('#promotionType').val() == 'ZV15' || $('#promotionType').val() == 'ZV16' || $('#promotionType').val() == 'ZV17' || $('#promotionType').val() == 'ZV18') {
			isDisableGridQuantity = true;
			isDisableGridAmount = true;
		} else {
			textDisQuantity = 'disabled="disabled"';
			textDisAmount = '';
			isDisableGridQuantity = true;
			isDisableGridAmount = false;
		}
		var html = '<div id="popup-container"><div id="popup-add-level" class="PopupContentMid">\
			<div class="GeneralForm Search1Form" id="form-data">\
			<label style="width: 100px;" class="LabelStyle Label1Style">Mã nhóm<span class="ReqiureStyle">(*)</span></label>\
			<input type="text" call-back="VTValidateUtils.getMessageOfRequireCheck(\'groupCode\', \'Mã Nhóm\', false, true);VTValidateUtils.getMessageOfSpecialCharactersValidate(\'groupCode\', \'Mã Nhóm\', Utils._CODE)" class="InputTextStyle InputText1Style" style="width: 145px;" disabled="disabled" maxlength="50" id="groupCode" value="'+Utils.XSSEncode(groupCode)+'"> \
			<label style="width: 100px;" class="LabelStyle Label1Style">Tên nhóm<span class="ReqiureStyle">(*)</span></label> \
			<input type="text" class="InputTextStyle InputText1Style" style="width: 145px;" call-back="VTValidateUtils.getMessageOfRequireCheck(\'promotionName\', \'Tên Nhóm\', false, true);VTValidateUtils.getMessageOfSpecialCharactersValidate(\'groupName\', \'Tên Nhóm\', Utils._NAME)" maxlength="100" id="groupName" value="'+Utils.XSSEncode(groupName)+'">\
			<div class="Clear"></div>';
			html += '<div class="Clear"></div>';
		if($('#promotionType').val() != 'ZV01' && $('#promotionType').val() != 'ZV04' && $('#promotionType').val() != 'ZV07' && $('#promotionType').val() != 'ZV10' && $('#promotionType').val() != 'ZV19') {
			html +=	'<label class="LabelStyle" style="width:100px;">Bội số</label><input type="checkbox" id="multiple" '+(multiple==1?'checked="checked"':'')+' class="InputCbxStyle"/>\
			<label class="LabelStyle" style="width:131px;">Tối ưu</label><input type="checkbox" id="recursive" '+(recursive==1?'checked="checked"':'')+' class="InputCbxStyle"/>';
		} else {
			html +=	'<label class="LabelStyle" style="width:100px; visibility: hidden;">Bội số</label><input style="visibility: hidden;" type="checkbox" id="multiple" '+(multiple==1?'checked="checked"':'')+' class="InputCbxStyle"/>\
			<label class="LabelStyle" style="width:131px; visibility: hidden;">Tối ưu</label><input style="visibility: hidden;" type="checkbox" id="recursive" '+(recursive==1?'checked="checked"':'')+' class="InputCbxStyle"/>';
		}
		html +=	'<label class="LabelStyle Label1Style">Thứ tự ưu tiên</label> \
			<input type="text" class="InputTextStyle InputText1Style" style="width: 145px;" maxlength="3" id="stt" value="'+stt+'">\
			<input type="hidden" id="groupId" value="'+productGroupId+'">\
			<div class="Clear"></div>\
			<div class="BtnCenterSection">\
			<button class="BtnGeneralStyle" '+(($('#statusHidden').val() == 0 || $('#statusHidden').val() == 1) ? 'style="display:none;"' : '')+' id="btn-update">Cập nhật</button>\
			</div>\
			<p class="ErrorMsgStyle SpriteErr" style="display: none;" id="errMsgGroupDlg"></p>\
			<p class="SuccessMsgStyle" style="display: none;" id="successMsgGroupDlg"></p>\
			<div class="SearchInSection SProduct1Form">\
			<h2 class="Title2Style">Khai báo mức cho nhóm mua: '+groupName+'</h2>\
			<div class="GridSection" id="promotionGrid">\
			<div id="gridLevel"></div>\
			</div>\
			</div>\
			</div>';
		$("body").append(html);
		Utils.bindFormatOnTextfield('stt', Utils._TF_NUMBER);
		$('#popup-add-level').dialog({
			title: 'Thông tin nhóm mua',
			width: 750,
			heigth: "auto",
			modal: true, close: false, cache: false,
			onOpen: function() {
				VTUtilJS.bindFormatOnTextfield('minQuantity', VTUtilJS._TF_NUMBER);
				VTUtilJS.formatCurrencyFor('minQuantity');
				VTUtilJS.bindFormatOnTextfield('minAmount', VTUtilJS._TF_NUMBER);
				VTUtilJS.formatCurrencyFor('minAmount');
				$('#gridLevel').datagrid({
					url: "/promotion/list-level",
    				queryParams: {groupId:productGroupId},
    				width: $("#promotionGrid").width(),
    				height: 'auto',
    				singleSelect: true,
    				fitColumns: true,
    				scrollbarSize:0,
    				pagination:false,
    				columns: [[
    					{field:'id', title:"Mức", width:20,align:'center',sortable:false,resizable:false,formatter:function(value,row,index) {
    						return Number(index) + 1;
    					}},
    					{field:'groupText', title:"Sản phẩm mua", width:100,align:'left',sortable:false,resizable:false,formatter:function(value, row, index) {
    						if(VTUtilJS.isNullOrEmpty(value)) {
    							if(!VTUtilJS.isNullOrEmpty(row.detail) && $.isArray(row.detail)) {
    								if($('#promotionType').val() == 'ZV01' || $('#promotionType').val() == 'ZV02' || $('#promotionType').val() == 'ZV03') {
    									var ed = $('#gridLevel').datagrid('getEditor', {index:index, field:'minQuantity'});
    									if(ed != null) {
    										var val = $(ed.target).val();
        									var text = PromotionProgram.returnGroupTextMua($('#promotionType').val(), row.detail, val, null);
        									return text;
    									} else {
    										return '';
    									}
    								} else if($('#promotionType').val() == 'ZV04' || $('#promotionType').val() == 'ZV05' || $('#promotionType').val() == 'ZV06') {
    									var ed = $('#gridLevel').datagrid('getEditor', {index:index, field:'minAmount'});
    									if(ed != null) {
    										var val = $(ed.target).val();
        									var text = PromotionProgram.returnGroupTextMua($('#promotionType').val(), row.detail, null, val);
        									return text;
    									} else {
    										return '';
    									}
    								} else {
    									var text = PromotionProgram.returnGroupTextMua($('#promotionType').val(), row.detail, null, null);
    									return text;
    								}
    							} else {
    								return '';
    							}
    						} else {
    							if(VTUtilJS.isNullOrEmpty(PromotionProgram._levelValueType)) {
    								if(row.detail != null && row.detail != undefined && $.isArray(row.detail) && row.detail.length > 0) {
    									if(row.detail[0].valueType != null && row.detail[0].valueType != undefined) {
    										PromotionProgram._levelValueType = row.detail[0].valueType;
    									}
    								}
    							}
    							return VTUtilJS.XSSEncode(value);
    						}
    					}},
    					{field:'minQuantity', title:"Số lượng tối thiểu", width:50,align:'right',sortable:false,resizable:false,formatter:function(value, row, index){
    						if(value == 0 || value == 'null' || VTUtilJS.isNullOrEmpty(value)) {
    							return '';
    						} else {
    							return CommonFormatter.formatNormalCell(value, row, index);
    						}
    					}, 
    						editor: {
    							type:'currency', 
    							options : {
    								isDisable: isDisableGridQuantity,
	    							blur: function(ed) {
	    								var val = $(ed).val();
	    								if($('#promotionType').val() == 'ZV01' || $('#promotionType').val() == 'ZV02' || $('#promotionType').val() == 'ZV03' || $('#promotionType').val() == 'ZV07' || $('#promotionType').val() == 'ZV08' || $('#promotionType').val() == 'ZV09') {
	    									var index = $(ed).parent().parent().parent().parent().parent().parent().parent().attr('datagrid-row-index');
	    									var thisRow = $('#gridLevel').datagrid('getRows')[index];
	    									var lstDetail = thisRow.detail;
	    									$('#gridLevel').datagrid('getRows')[index].groupText = PromotionProgram.returnGroupTextMua($('#promotionType').val(), lstDetail, val, null);
	    									$('#gridLevel').datagrid('getRows')[index].minQuantity = val;
	    									$('#gridLevel').datagrid('getRows')[index].orderNumber = $($('#gridLevel').datagrid('getEditor', {index : index, field : 'orderNumber'}).target).val();
	    									$('#gridLevel').datagrid('refreshRow', index);
	    									$('#gridLevel').datagrid('beginEdit', index);
	    									setTimeout(function() {
	    										$($('#gridLevel').datagrid('getEditor', {index:index, field:'minQuantity'}).target).focusEnd();
	    									}, 1);
	    								} else {
	    									
	    								}
	    							}
    							}
    						}
    					},
    					{field:'minAmount', title:"Số tiền tối thiểu", width:50,align:'right',sortable:false,resizable:false,formatter:function(value, row, index){
    						if(value == 0 || value == 'null' || VTUtilJS.isNullOrEmpty(value)) {
    							return '';
    						} else {
    							return CommonFormatter.formatNormalCell(value, row, index);
    						}
    					}, 
    						editor: {
    							type:'currency', 
    							options : {
    								isDisable: isDisableGridAmount,
	    							blur: function(ed) {
	    								var val = $(ed).val();
	    								if($('#promotionType').val() == 'ZV03' || $('#promotionType').val() == 'ZV04' || $('#promotionType').val() == 'ZV05' || 
	    										$('#promotionType').val() == 'ZV10' || $('#promotionType').val() == 'ZV11' || $('#promotionType').val() == 'ZV12') {
	    									var index = $(ed).parent().parent().parent().parent().parent().parent().parent().attr('datagrid-row-index');
	    									var thisRow = $('#gridLevel').datagrid('getRows')[index];
	    									var lstDetail = thisRow.detail;
	    									$('#gridLevel').datagrid('getRows')[index].groupText = PromotionProgram.returnGroupTextMua($('#promotionType').val(), lstDetail, null, val);
	    									$('#gridLevel').datagrid('getRows')[index].minAmount = val;
	    									$('#gridLevel').datagrid('getRows')[index].orderNumber = $($('#gridLevel').datagrid('getEditor', {index : index, field : 'orderNumber'}).target).val();
	    									$('#gridLevel').datagrid('refreshRow', index);
	    									$('#gridLevel').datagrid('beginEdit', index);
	    									setTimeout(function() {
	    										$($('#gridLevel').datagrid('getEditor', {index:index, field:'minAmount'}).target).focusEnd();
	    									}, 1);
	    								} else {
	    									
	    								}
	    							}
    							}
    						}
    					},
    					{field:'orderNumber', title:"Thứ tự", width:30,align:'center',sortable:false,resizable:false, editor: {type:'currency', options : {maxLength:3}}},
    					{field:'edit', title:"", width:30,align:'center',sortable:false,resizable:false,formatter:function(value,row,index){
    						if($('#promotionType').val() == 'ZV19' || $('#promotionType').val() == 'ZV20' || $('#promotionType').val() == 'ZV21') {
    							return '';
    						} else {
    							return '<a onclick="PromotionProgram.editLevelMua('+index+', '+row.id+')"><span style="cursor:pointer"><img title="Thông tin sản phẩm bán" src="/resources/images/icon_edit.png"/></span></a>';
    						}
    					}}
    				]],
    				onDblClickRow: function(index,row) {
    					if($('#statusHidden').val() == 2) {
    						$('#gridLevel').datagrid('beginEdit', index);
    					}
    				},
    				onLoadSuccess: function(data) {
    					/*for(var i = 0; i < data.rows.length; i++) {
    						$('#gridLevel').datagrid('beginEdit', i);
    					}*/
    				}
				});
				$('#btn-update').bind('click', function() {
					$('#errMsgGroupDlg').html('').hide();
					var params = VTUtilJS.getFormData('form-data');
					if($('#errorMsg').html().trim().length > 0 && $('#errorMsg').is(":visible")) {
						$('#errMsgGroupDlg').html($('#errorMsg').html()).show();
						$('#errorMsg').html('').hide();
					}
					if(params == null) {
						return;
					}
					$('#errMsgGroupDlg').html('').hide();
					if(VTUtilJS.isNullOrEmpty(params.groupCode)) {
						$('#errMsgGroupDlg').html('Mã nhóm chưa được khai nhập').show();
						return;
					}
					if(VTUtilJS.isNullOrEmpty(params.groupName)) {
						$('#errMsgGroupDlg').html('Tên nhóm chưa được khai nhập').show();
						return;
					}
					params.promotionId = $('#id').val();
					var rows = $('#gridLevel').datagrid('getRows');
					for(var i = 0; i < rows.length; i++) {
						if(!$('#gridLevel').datagrid('validateRow', i)) {
							$('#errMsgGroupDlg').html('Dữ liệu nhập không hợp lệ ở dòng '+(i+1)).show();
						}
					}
					$('#gridLevel').datagrid('acceptChanges');
					rows = $('#gridLevel').datagrid('getRows');
					var listMinQuantity = new Array();
					var listMinAmount = new Array();
					var listOrder = new Array();
					var listDetail = new Array();
					var listLevelId = new Array();
					for(var i = 0; i < rows.length; i++) {
						var row = rows[i];
						var minQuantity = VTUtilJS.isNullOrEmpty(row.minQuantity) ? 0 : VTUtilJS.returnMoneyValue(row.minQuantity);
						minQuantity = isNaN(minQuantity) ? 0 : Number(minQuantity);
						var minAmount = VTUtilJS.isNullOrEmpty(row.minAmount) ? 0 : VTUtilJS.returnMoneyValue(row.minAmount);
						minAmount = isNaN(minAmount) ? 0 : Number(minAmount);
						var order = VTUtilJS.isNullOrEmpty(row.orderNumber) ? 0 : row.orderNumber;
						order = isNaN(order) ? 0 : Number(order);
						var levelId = VTUtilJS.isNullOrEmpty(row.id) ? 0 : row.id;
						if($('#promotionType').val() == 'ZV01' || $('#promotionType').val() == 'ZV02' || $('#promotionType').val() == 'ZV03' ||
								$('#promotionType').val() == 'ZV07' || $('#promotionType').val() == 'ZV08' || $('#promotionType').val() == 'ZV09') {
							if(VTUtilJS.isNullOrEmpty(minQuantity) || minQuantity <= 0) {
								$('#errMsgGroupDlg').html('Số lượng nhập không đúng định dạng').show();
								for(var ii = 0; ii < rows.length; ii++) {
		    						$('#gridLevel').datagrid('beginEdit', ii);
		    					}
								return;
							}
							/*if(i != 0) {
								var preRow = rows[i-1];
								var preMinQuantity = VTUtilJS.isNullOrEmpty(preRow.minQuantity) ? 0 : VTUtilJS.returnMoneyValue(preRow.minQuantity);
								var preOrder = VTUtilJS.isNullOrEmpty(preRow.orderNumber) ? 0 : preRow.orderNumber;
								if(Number(minQuantity) <= Number(preMinQuantity)) {
									$('#errMsgGroupDlg').html('Giá trị tối thiểu phải tăng dần').show();
									for(var ii = 0; ii < rows.length; ii++) {
			    						$('#gridLevel').datagrid('beginEdit', ii);
			    					}
									return;
								}
								if(Number(order) <= Number(preOrder)) {
									$('#errMsgGroupDlg').html('Số thứ tự phải tăng dần').show();
									for(var ii = 0; ii < rows.length; ii++) {
			    						$('#gridLevel').datagrid('beginEdit', ii);
			    					}
									return;
								}
							}*/
						} else if($('#promotionType').val() == 'ZV04' || $('#promotionType').val() == 'ZV05' || $('#promotionType').val() == 'ZV06' ||
								$('#promotionType').val() == 'ZV10' || $('#promotionType').val() == 'ZV11' || $('#promotionType').val() == 'ZV12') {
							if(VTUtilJS.isNullOrEmpty(minAmount) || minAmount <= 0) {
								$('#errMsgGroupDlg').html('Số lượng nhập không đúng định dạng').show();
								for(var ii = 0; ii < rows.length; ii++) {
		    						$('#gridLevel').datagrid('beginEdit', ii);
		    					}
								return;
							}
							/*if(i != 0) {
								var preRow = rows[i-1];
								var preMinAmount = VTUtilJS.isNullOrEmpty(preRow.minAmount) ? 0 : VTUtilJS.returnMoneyValue(preRow.minAmount);
								preMinAmount = isNaN(preMinAmount) ? 0 : Number(preMinAmount);
								var preOrder = VTUtilJS.isNullOrEmpty(preRow.orderNumber) ? 0 : preRow.orderNumber;
								preOrder = isNaN(preOrder) ? 0 : Number(preOrder);
								if(Number(minAmount) <= Number(preMinAmount)) {
									$('#errMsgGroupDlg').html('Giá trị tối thiểu phải tăng dần').show();
									for(var ii = 0; ii < rows.length; ii++) {
			    						$('#gridLevel').datagrid('beginEdit', ii);
			    					}
									return;
								}
								if(Number(order) <= Number(preOrder)) {
									$('#errMsgGroupDlg').html('Số thứ tự phải tăng dần').show();
									for(var ii = 0; ii < rows.length; ii++) {
			    						$('#gridLevel').datagrid('beginEdit', ii);
			    					}
									return;
								}
							}*/
						}
						if(VTUtilJS.isNullOrEmpty(order) || order <= 0) {
							$('#errMsgGroupDlg').html('Số thứ tự không được để trống').show();
							for(var ii = 0; ii < rows.length; ii++) {
	    						$('#gridLevel').datagrid('beginEdit', ii);
	    					}
							return;
						}
						/*if(i != 0) {
							var preRow = rows[i-1];
							var preOrder = VTUtilJS.isNullOrEmpty(preRow.orderNumber) ? 0 : preRow.orderNumber;
							preOrder = isNaN(preOrder) ? 0 : Number(preOrder);
							if(Number(order) <= Number(preOrder)) {
								$('#errMsgGroupDlg').html('Số thứ tự phải tăng dần').show();
								for(var ii = 0; ii < rows.length; ii++) {
		    						$('#gridLevel').datagrid('beginEdit', ii);
		    					}
								return;
							}
						}*/
						if(minQuantity != 0 && listMinQuantity.indexOf(minQuantity) != -1) {
							$('#errMsgGroupDlg').html('Số lượng tối thiểu trùng nhau').show();
							for(var ii = 0; ii < rows.length; ii++) {
	    						$('#gridLevel').datagrid('beginEdit', ii);
	    					}
							return;
						}
						if(minAmount != 0 && listMinAmount.indexOf(minAmount) != -1) {
							$('#errMsgGroupDlg').html('Số tiền tối thiểu trùng nhau').show();
							for(var ii = 0; ii < rows.length; ii++) {
	    						$('#gridLevel').datagrid('beginEdit', ii);
	    					}
							return;						
						}
						if(listOrder.indexOf(order) != -1) {
							$('#errMsgGroupDlg').html('Số thứ tự trùng nhau').show();
							for(var ii = 0; ii < rows.length; ii++) {
	    						$('#gridLevel').datagrid('beginEdit', ii);
	    					}
							return;	
						}
						listMinQuantity.push(minQuantity);
						listMinAmount.push(minAmount);
						listOrder.push(order);
						listLevelId.push(levelId);
						if($('#promotionType').val() == 'ZV13' || $('#promotionType').val() == 'ZV14' || $('#promotionType').val() == 'ZV15' || $('#promotionType').val() == 'ZV16' || $('#promotionType').val() == 'ZV17' || $('#promotionType').val() == 'ZV18') {
							if(!VTUtilJS.isNullOrEmpty(PromotionProgram._levelValueType) && $.isArray(row.detail)) {
								var detailText = PromotionProgram._levelValueType;
								for(var j = 0; j < row.detail.length; j++) {
									detailText += ';' + row.detail[j].productCode + '-' + (VTUtilJS.isNullOrEmpty(row.detail[j].value) ? 0 : row.detail[j].value) + ','+row.detail[j].isRequired;
								}
								listDetail.push(detailText);
							}
						} else {
							if($.isArray(row.detail)) {
								var detailText = '';
								if(VTUtilJS.isNullOrEmpty(PromotionProgram._levelValueType)) {
									if($('#promotionType').val() == 'ZV01' || $('#promotionType').val() == 'ZV02' || $('#promotionType').val() == 'ZV03' ||
											$('#promotionType').val() == 'ZV07' || $('#promotionType').val() == 'ZV08' || $('#promotionType').val() == 'ZV09') {
										detailText = '1';
									} else if($('#promotionType').val() == 'ZV04' || $('#promotionType').val() == 'ZV05' || $('#promotionType').val() == 'ZV06' || 
											$('#promotionType').val() == 'ZV10' || $('#promotionType').val() == 'ZV11' || $('#promotionType').val() == 'ZV12') {
										detailText = '2';
									}
								} else {
									detailText = VTUtilJS.isNullOrEmpty(PromotionProgram._levelValueType) ? 0 : PromotionProgram._levelValueType;
								}
								for(var j = 0; j < row.detail.length; j++) {
									detailText += ';' + row.detail[j].productCode + '-0' + ','+((row.detail[j].isRequired == true || row.detail[j].isRequired == 1 || row.detail[j].isRequired == '1')?'1':'0');
								}
								listDetail.push(detailText);
							}
						}
					}
					if(!VTUtilJS.isNullOrEmpty(PromotionProgram._levelValueType)) {
						if(listMinQuantity.length != listDetail.length) {
							$('#errMsgGroupDlg').html('Bạn chưa nhập dữ liệu sản phẩm cho mức').show();
							for(var ii = 0; ii < rows.length; ii++) {
	    						$('#gridLevel').datagrid('beginEdit', ii);
	    					}
						}
					}
					params.listMinQuantity = listMinQuantity;
					params.listMinAmount = listMinAmount;
					params.listOrder = listOrder;
					params.listProductDetail = listDetail;
					params.listLevelId = listLevelId;
					$.messager.confirm("Xác nhận", "Bạn có muốn cập nhập mức cho nhóm?", function(r) {
						if(r) {
							VTUtilJS.postFormJson(params, '/promotion/update-group-sale', function(data) {
								if(data.error) {
									$('#errMsgGroupDlg').html(data.errMsg).show();
									for(var ii = 0; ii < rows.length; ii++) {
			    						$('#gridLevel').datagrid('beginEdit', ii);
			    					}
								} else {
									$('#groupMuaGrid').datagrid('reload');
									$('#popup-add-level').dialog('close');
								}
							});
						} else {
							for(var ii = 0; ii < rows.length; ii++) {
	    						$('#gridLevel').datagrid('beginEdit', ii);
	    					}
						}
					});
				});
			},
			onClose: function() {
				PromotionProgram._levelValueType = null;
				$("#popup-add-level").dialog("destroy");
				$("#popup-container").remove();
			}
		});
	},
	
	popupAddLevelKM : function(productGroupId, groupCode, groupName, maxQuantity, maxAmount) {
		var textDisQuantity = '';
		var textDisAmount = '';
		var isDisableGridPercent = false;
		var isDisableGridQuantity = false;
		var isDisableGridAmount = false;
		if($('#promotionType').val() == 'ZV01' || $('#promotionType').val() == 'ZV04' || $('#promotionType').val() == 'ZV07' || 
				$('#promotionType').val() == 'ZV10' || $('#promotionType').val() == 'ZV13' || $('#promotionType').val() == 'ZV16' ||
				$('#promotionType').val() == 'ZV19') {
			textDisQuantity = 'disabled="disabled"';
			textDisAmount = 'disabled="disabled"';
			var isDisableGridPercent = false;
			isDisableGridQuantity = true;
			isDisableGridAmount = true;
		} else if($('#promotionType').val() == 'ZV02' || $('#promotionType').val() == 'ZV05' || $('#promotionType').val() == 'ZV08' || 
				$('#promotionType').val() == 'ZV11' || $('#promotionType').val() == 'ZV14' || $('#promotionType').val() == 'ZV17' ||
				$('#promotionType').val() == 'ZV20'){
			textDisQuantity = 'disabled="disabled"';
			textDisAmount = '';
			var isDisableGridPercent = true;
			isDisableGridQuantity = true;
			isDisableGridAmount = false;
		} else {
			textDisQuantity = 'disabled="disabled"';
			textDisAmount = '';
			var isDisableGridPercent = true;
			isDisableGridQuantity = true;
			isDisableGridAmount = true;
		}
		var html = '<div id="popup-container"><div id="popup-add-level" class="PopupContentMid">\
			<div class="GeneralForm Search1Form" id="form-data">\
			<label style="width: 100px;" class="LabelStyle Label1Style">Mã Nhóm<span class="ReqiureStyle">(*)</span></label>\
			<input type="text" class="InputTextStyle InputText1Style" call-back="VTValidateUtils.getMessageOfRequireCheck(\'groupCode\', \'Mã Nhóm\', false, true);VTValidateUtils.getMessageOfSpecialCharactersValidate(\'groupCode\', \'Mã Nhóm\', Utils._CODE)" style="width: 145px;" disabled="disabled" maxlength="50" id="groupCode" value="'+Utils.XSSEncode(groupCode)+'"> \
			<label style="width: 100px;" class="LabelStyle Label1Style">Tên Nhóm<span class="ReqiureStyle">(*)</span></label> \
			<input type="text" call-back="VTValidateUtils.getMessageOfRequireCheck(\'promotionName\', \'Tên Nhóm\', false, true);VTValidateUtils.getMessageOfSpecialCharactersValidate(\'groupName\', \'Tên Nhóm\', Utils._NAME)" class="InputTextStyle InputText1Style" style="width: 145px;" maxlength="100" id="groupName" value="'+Utils.XSSEncode(groupName)+'">\
			<div class="Clear"></div>';
			html += '<input type="hidden" id="groupId" value="'+productGroupId+'">\
			<div class="Clear"></div>\
			<div class="BtnCenterSection">\
			<button class="BtnGeneralStyle" '+(($('#statusHidden').val() == 0 || $('#statusHidden').val() == 1) ? 'style="display:none;"' : '')+' id="btn-update">Cập nhật</button>\
			</div>\
			<p class="ErrorMsgStyle SpriteErr" style="display: none;" id="errMsgGroupDlg"></p>\
			<p class="SuccessMsgStyle" style="display: none;" id="successMsgGroupDlg"></p>\
			<div class="SearchInSection SProduct1Form">\
			<h2 class="Title2Style">Khai báo mức cho nhóm KM: '+Utils.XSSEncode(groupName)+'</h2>\
			<div class="GridSection" id="promotionGrid">\
			<div id="gridLevel"></div>\
			</div>\
			</div>\
			</div>';
		$("body").append(html);
		bindFormatOnTextfield($('#stt'), _TF_NUMBER);
		$('#popup-add-level').dialog({
			title: 'Thông tin nhóm KM',
			width: 750,
			heigth: "auto",
			modal: true, close: false, cache: false,
			onOpen: function() {
				VTUtilJS.bindFormatOnTextfield('maxQuantity', VTUtilJS._TF_NUMBER);
				VTUtilJS.formatCurrencyFor('maxQuantity');
				VTUtilJS.bindFormatOnTextfield('maxAmount', VTUtilJS._TF_NUMBER);
				VTUtilJS.formatCurrencyFor('maxAmount');
				$('#gridLevel').datagrid({
					url: "/promotion/list-level",
    				queryParams: {groupId:productGroupId},
    				width: $("#promotionGrid").width(),
    				height: 'auto',
    				singleSelect: true,
    				fitColumns: true,
    				scrollbarSize:0,
    				pagination:false,
    				columns: [[
    					{field:'id', title:"Mức", width:20,align:'center',sortable:false,resizable:false,formatter:function(value,row,index) {
    						return Number(index) + 1;
    					}},
    					{field:'groupText', title:"Sản phẩm mua", width:100,align:'left',sortable:false,resizable:false,formatter:function(value, row, index) {
    						if(VTUtilJS.isNullOrEmpty(value)) {
    							if(!VTUtilJS.isNullOrEmpty(row.detail) && $.isArray(row.detail)) {
									var text = PromotionProgram.returnGroupTextMua($('#promotionType').val(), row.detail, null, null);
									return text;
    								var text = '(';
    							} else {
    								return '';
    							}
    						} else {
    							if(VTUtilJS.isNullOrEmpty(PromotionProgram._levelValueType)) {
    								if(row.detail != null && row.detail != undefined && $.isArray(row.detail) && row.detail.length > 0) {
    									if(row.detail[0].valueType != null && row.detail[0].valueType != undefined) {
    										PromotionProgram._levelValueType = row.detail[0].valueType;
    									}
    								}
    							}
    							return VTUtilJS.XSSEncode(value);
    						}
    					}},
    					{field:'promotionPercent', title:"%KM", width:20,align:'right',sortable:false,resizable:false,formatter:CommonFormatter.formatNormalCell, editor: {type:'percent', options : {isDisable: isDisableGridPercent}}},
    					{field:'maxQuantity', title:"Số lượng tối đa", width:50,align:'right',sortable:false,resizable:false,formatter:function(value, row, index){
    						if(value == 0 || value == 'null' || VTUtilJS.isNullOrEmpty(value)) {
    							return '';
    						} else {
    							return CommonFormatter.formatNormalCell(value, row, index);
    						}
    					}, 
    						editor: {
    							type:'currency', 
    							options : {
    								isDisable: isDisableGridQuantity
    							}
    						}
    					},
    					{field:'maxAmount', title:"Số tiền tối đa", width:50,align:'right',sortable:false,resizable:false,formatter:function(value, row, index){
    						if(value == 0 || value == 'null' || VTUtilJS.isNullOrEmpty(value)) {
    							return '';
    						} else {
    							return CommonFormatter.formatNormalCell(value, row, index);
    						}
    					}, 
    						editor: {
    							type:'currency', 
    							options : {
    								isDisable: isDisableGridAmount
    							}
    						}
    					},
    					{field:'orderNumber', title:"Thứ tự", width:30,align:'center',sortable:false,resizable:false, editor: {type:'currency', options : {maxLength:3}}},
    					{field:'edit', title:"", width:30,align:'center',sortable:false,resizable:false,formatter:function(value,row,index){
    						if($('#promotionType').val() == 'ZV03' || $('#promotionType').val() == 'ZV06' || $('#promotionType').val() == 'ZV09' || 
									$('#promotionType').val() == 'ZV12' || $('#promotionType').val() == 'ZV15' || $('#promotionType').val() == 'ZV18' ||
									$('#promotionType').val() == 'ZV21') {
    							return '<a onclick="PromotionProgram.editLevelKM('+index+', '+row.id+')"><span style="cursor:pointer"><img title="Thôn tin sản phẩm KM" src="/resources/images/icon_edit.png"/></span></a>';
    						} else {
    							return '';
    						}
    					}}/*,
    					{field:'delete', title:'<a '+(($('#statusHidden').val() == 0 || $('#statusHidden').val() == 1) ? 'style="display:none;"' : '')+' onclick="PromotionProgram.addLevelKM()"><span style="cursor:pointer"><img title="Thêm mới mức" src="/resources/images/icon_add.png"/></span></a>', width:30,align:'center',sortable:false,resizable:false,formatter:function(value,row,index){
    						return '<a '+(($('#statusHidden').val() == 0 || $('#statusHidden').val() == 1) ? 'style="display:none;"' : '')+' onclick="PromotionProgram.deleteLevelKM('+index+')"><span style="cursor:pointer"><img title="Xóa mức" src="/resources/images/icon_delete.png"/></span></a>';
    					}},*/
    				]],
    				onDblClickRow: function(index,row) {
    					if($('#statusHidden').val() == 2) {
    						$('#gridLevel').datagrid('beginEdit', index);
    					}
    				},
    				onLoadSuccess: function(data) {
    					/*for(var i = 0; i < data.rows.length; i++) {
    						$('#gridLevel').datagrid('beginEdit', i);
    					}*/
    				}
				});
				$('#btn-update').bind('click', function() {
					var params = VTUtilJS.getFormData('form-data');
					if($('#errorMsg').html().trim().length > 0 && $('#errorMsg').is(":visible")) {
						$('#errMsgGroupDlg').html($('#errorMsg').html()).show();
						$('#errorMsg').html('').hide();
					}
					if(params == null) {
						return;
					}
					$('#errMsgGroupDlg').html('').hide();
					if(VTUtilJS.isNullOrEmpty(params.groupCode)) {
						$('#errMsgGroupDlg').html('Mã nhóm chưa được khai nhập').show();
						return;
					}
					if(VTUtilJS.isNullOrEmpty(params.groupName)) {
						$('#errMsgGroupDlg').html('Tên nhóm chưa được khai nhập').show();
						return;
					}
					params.promotionId = $('#id').val();
					var rows = $('#gridLevel').datagrid('getRows');
					for(var i = 0; i < rows.length; i++) {
						if(!$('#gridLevel').datagrid('validateRow', i)) {
							$('#errMsgGroupDlg').html('Dữ liệu nhập không hợp lệ ở dòng '+(i+1)).show();
						}
					}
					$('#gridLevel').datagrid('acceptChanges');
					rows = $('#gridLevel').datagrid('getRows');
					var listPercent = new Array();
					var listMaxQuantity = new Array();
					var listMaxAmount = new Array();
					var listOrder = new Array();
					var listDetail = new Array();
					var listLevelId = new Array();
					for(var i = 0; i < rows.length; i++) {
						var row = rows[i]; 
						var percent = VTUtilJS.isNullOrEmpty(row.promotionPercent) ? 0 : VTUtilJS.returnMoneyValue(row.promotionPercent);
						var maxQuantity = VTUtilJS.isNullOrEmpty(row.maxQuantity) ? 0 : VTUtilJS.returnMoneyValue(row.maxQuantity);
						var maxAmount = VTUtilJS.isNullOrEmpty(row.maxAmount) ? 0 : VTUtilJS.returnMoneyValue(row.maxAmount);
						var order = VTUtilJS.isNullOrEmpty(row.orderNumber) ? 0 : row.orderNumber;
						var levelId = VTUtilJS.isNullOrEmpty(row.id) ? 0 : row.id;
						
						if(!(0 <= Number(percent) && Number(percent) <= 100)) {
							$('#errMsgGroupDlg').html('Số phần phải lớn hơn 0 và nhỏ hơn 100').show();
							for(var ii = 0; ii < rows.length; ii++) {
	    						$('#gridLevel').datagrid('beginEdit', ii);
	    					}
							return;
						}
						
						if(VTUtilJS.isNullOrEmpty(order) || order <= 0) {
							$('#errMsgGroupDlg').html('Số thứ tự không được để trống').show();
							for(var ii = 0; ii < rows.length; ii++) {
	    						$('#gridLevel').datagrid('beginEdit', ii);
	    					}
							return;
						}
						if(row.detail != null && row.detail != undefined && $.isArray(row.detail) && row.detail.length > 0) {
							var detailText = 1;
							for(var j = 0; j < row.detail.length; j++) {
								detailText += ';' + Utils.XSSEncode(row.detail[j].productCode + '-' + row.detail[j].value + ','+row.detail[j].isRequired);
							}
							listDetail.push(detailText);
						} else if($('#promotionType').val() == 'ZV03' || $('#promotionType').val() == 'ZV06' || $('#promotionType').val() == 'ZV09' ||
								$('#promotionType').val() == 'ZV12' || $('#promotionType').val() == 'ZV15' || $('#promotionType').val() == 'ZV18' ||
								$('#promotionType').val() == 'ZV21') {
							$('#errMsgGroupDlg').html('Danh sách sản phẩm khuyến mãi ở mức '+(i+1)+' chưa được nhập').show();
							for(var ii = 0; ii < rows.length; ii++) {
	    						$('#gridLevel').datagrid('beginEdit', ii);
	    					}
							return;
						}
						if($('#promotionType').val() == 'ZV01' || $('#promotionType').val() == 'ZV04' || $('#promotionType').val() == 'ZV07' ||
								$('#promotionType').val() == 'ZV10' || $('#promotionType').val() == 'ZV13' || $('#promotionType').val() == 'ZV16' ||
								$('#promotionType').val() == 'ZV19') {
							if(VTUtilJS.isNullOrEmpty(percent) || percent <= 0) {
								$('#errMsgGroupDlg').html('Phần trăm không được để trống').show();
								for(var ii = 0; ii < rows.length; ii++) {
		    						$('#gridLevel').datagrid('beginEdit', ii);
		    					}
								return;
							}
						} else if($('#promotionType').val() == 'ZV02' || $('#promotionType').val() == 'ZV05' || $('#promotionType').val() == 'ZV08' ||
								$('#promotionType').val() == 'ZV11' || $('#promotionType').val() == 'ZV14' || $('#promotionType').val() == 'ZV17' ||
								$('#promotionType').val() == 'ZV20') {
							if(VTUtilJS.isNullOrEmpty(maxAmount) || maxAmount <= 0) {
								$('#errMsgGroupDlg').html('Số tiền tối đa không đúng định dạng').show();
								for(var ii = 0; ii < rows.length; ii++) {
		    						$('#gridLevel').datagrid('beginEdit', ii);
		    					}
								return;
							}
						} else if($('#promotionType').val() == 'ZV03' || $('#promotionType').val() == 'ZV06' || $('#promotionType').val() == 'ZV09' ||
								$('#promotionType').val() == 'ZV12' || $('#promotionType').val() == 'ZV15' || $('#promotionType').val() == 'ZV18' ||
								$('#promotionType').val() == 'ZV21') {
							if(listDetail.length == 0) {
								$('#errMsgGroupDlg').html('Số tiền tối đa không đúng định dạng').show();
								for(var ii = 0; ii < rows.length; ii++) {
		    						$('#gridLevel').datagrid('beginEdit', ii);
		    					}
								return;
							}
						}
						
						if(listOrder.indexOf(Number(order)) != -1) {
							$('#errMsgGroupDlg').html('Số thứ tự trùng nhau').show();
							for(var ii = 0; ii < rows.length; ii++) {
	    						$('#gridLevel').datagrid('beginEdit', ii);
	    					}
							return;	
						}
						
						/*if(i != 0) {
							var preOrder = VTUtilJS.isNullOrEmpty(rows[i-1].orderNumber) ? 0 : rows[i-1].orderNumber;
							if(preOrder >= order) {
								$('#errMsgGroupDlg').html('Số thứ tự phải tăng dần').show();
								for(var ii = 0; ii < rows.length; ii++) {
		    						$('#gridLevel').datagrid('beginEdit', ii);
		    					}
								return;
							}
						}*/
						
						listPercent.push(percent);
						listMaxQuantity.push(maxQuantity);
						listMaxAmount.push(maxAmount);
						listOrder.push(Number(order));
						listLevelId.push(levelId);
					}
					if($('#promotionType').val() == 'ZV03' || $('#promotionType').val() == 'ZV06' || $('#promotionType').val() == 'ZV09' ||
							$('#promotionType').val() == 'ZV12' || $('#promotionType').val() == 'ZV15' || $('#promotionType').val() == 'ZV18' ||
							$('#promotionType').val() == 'ZV21') {
						if(listMaxQuantity.length != listDetail.length) {
							for(var ii = 0; ii < rows.length; ii++) {
								$('#gridLevel').datagrid('beginEdit', ii);
							}
							$('#errMsgGroupDlg').html('Bạn chưa nhập dữ liệu sản phẩm cho mức').show();
						}
					}
					
					params.listPercent = listPercent;
					params.listMaxQuantity = listMaxQuantity;
					params.listMaxAmount = listMaxAmount;
					params.listOrder = listOrder;
					params.listProductDetail = listDetail;
					params.listLevelId = listLevelId;
					
					$.messager.confirm("Xác nhận", "Bạn có muốn cập nhập mức cho nhóm?", function(r) {
						if(r) {
							VTUtilJS.postFormJson(params, '/promotion/update-group-free', function(data) {
								if(data.error) {
									for(var ii = 0; ii < rows.length; ii++) {
										$('#gridLevel').datagrid('beginEdit', ii);
									}
									$('#errMsgGroupDlg').html(data.errMsg).show();
								} else {
									$('#popup-add-level').dialog('close');
								}
							});
						} else {
							for(var ii = 0; ii < rows.length; ii++) {
	    						$('#gridLevel').datagrid('beginEdit', ii);
	    					}
						}
					});
					
				});
			},
			onClose: function() {
				PromotionProgram._levelValueType = null;
				$("#popup-add-level").dialog("destroy");
				$("#popup-container").remove();
			}
		});
	},
	
	popupEditGroupMua : function(productGroupId, groupCode, groupName, minQuantity, minAmount, multiple, recursive, stt) {
		var textDisQuantity = '';
		var textDisAmount = '';
		if($('#promotionType').val() == 'ZV01' || $('#promotionType').val() == 'ZV02' || $('#promotionType').val() == 'ZV03' || 
				$('#promotionType').val() == 'ZV07' || $('#promotionType').val() == 'ZV08' || $('#promotionType').val() == 'ZV09' ||
				$('#promotionType').val() == 'ZV13' || $('#promotionType').val() == 'ZV14' || $('#promotionType').val() == 'ZV15') {
			textDisQuantity = '';
			textDisAmount = 'disabled="disabled"';
		} else {
			textDisQuantity = 'disabled="disabled"';
			textDisAmount = '';
		}
		var html = '<div id="popup-container"><div id="popup-edit-group-mua" class="PopupContentMid">\
			<div class="GeneralForm Search1Form" id="form-data">\
			<label style="width: 100px;" class="LabelStyle Label1Style">Mã nhóm<span class="ReqiureStyle">(*)</span></label>\
			<input type="text" call-back="VTValidateUtils.getMessageOfRequireCheck(\'groupCode\', \'Mã Nhóm\', false, true);VTValidateUtils.getMessageOfSpecialCharactersValidate(\'groupCode\', \'Mã Nhóm\', Utils._CODE)" class="InputTextStyle InputText1Style" style="width: 145px;" maxlength="50" disabled="disabled" id="groupCode" value="'+Utils.XSSEncode(groupCode)+'"> \
			<label style="width: 100px;" class="LabelStyle Label1Style">Tên nhóm<span class="ReqiureStyle">(*)</span></label> \
			<input type="text" class="InputTextStyle InputText1Style" call-back="VTValidateUtils.getMessageOfRequireCheck(\'promotionName\', \'Tên Nhóm\', false, true);VTValidateUtils.getMessageOfSpecialCharactersValidate(\'groupName\', \'Tên Nhóm\', Utils._NAME)" style="width: 145px;" maxlength="100" id="groupName" value="'+Utils.XSSEncode(groupName)+'">\
			<div class="Clear"></div>';
			html += '<div class="Clear"></div>';
			if($('#promotionType').val() != 'ZV01' && $('#promotionType').val() != 'ZV04' && $('#promotionType').val() != 'ZV07' && $('#promotionType').val() != 'ZV10' && $('#promotionType').val() != 'ZV19') {
				html +=	'<label class="LabelStyle" style="width:100px;">Bội số</label><input type="checkbox" id="multiple" '+(multiple==1?'checked="checked"':'')+' class="InputCbxStyle"/>\
				<label class="LabelStyle" style="width:131px;">Tối ưu</label><input type="checkbox" id="recursive" '+(recursive==1?'checked="checked"':'')+' class="InputCbxStyle"/>';
			} else {
				html +=	'<label class="LabelStyle" style="width:100px; visibility: hidden;">Bội số</label><input style="visibility: hidden;" type="checkbox" id="multiple" '+(multiple==1?'checked="checked"':'')+' class="InputCbxStyle"/>\
				<label class="LabelStyle" style="width:131px; visibility: hidden;">Tối ưu</label><input style="visibility: hidden;" type="checkbox" id="recursive" '+(recursive==1?'checked="checked"':'')+' class="InputCbxStyle"/>';
			}
			html += '<label class="LabelStyle Label1Style">Thứ tự ưu tiên</label> \
			<input type="text" class="InputTextStyle InputText1Style" style="width: 145px;" maxlength="3" id="stt" value="'+stt+'">\
			<input type="hidden" id="groupId" value="'+productGroupId+'">\
			<div class="Clear"></div>\
			<div class="BtnCenterSection">\
			<button class="BtnGeneralStyle" id="btn-update">Cập nhật</button>\
			</div>\
			<p class="ErrorMsgStyle SpriteErr" style="display: none;" id="errMsgGroupDlg"></p>\
			<p class="SuccessMsgStyle" style="display: none;" id="successMsgGroupDlg"></p></div></div></div>';
		$("body").append(html);
		Utils.bindFormatOnTextfield('stt', Utils._TF_NUMBER);
		$('#popup-edit-group-mua').dialog({
			title: 'Thông tin nhóm mua',
			width: 550,
			heigth: "auto",
			modal: true, close: false, cache: false,
			onOpen: function() {
				VTUtilJS.bindFormatOnTextfield('minQuantity', VTUtilJS._TF_NUMBER);
				VTUtilJS.formatCurrencyFor('minQuantity');
				VTUtilJS.bindFormatOnTextfield('minAmount', VTUtilJS._TF_NUMBER);
				VTUtilJS.formatCurrencyFor('minAmount');
				$('#btn-update').bind('click', function() {
					var params = VTUtilJS.getFormData('form-data');
					if($('#errorMsg').html().trim().length > 0 && $('#errorMsg').is(":visible")) {
						$('#errMsgGroupDlg').html($('#errorMsg').html()).show();
						$('#errorMsg').html('').hide();
					}
					if(params == null) {
						return;
					}
					$('.SuccessMsgStyle').hide();
					$('#errMsgGroupDlg').html('').hide();
					if(VTUtilJS.isNullOrEmpty(params.groupCode)) {
						$('#errMsgGroupDlg').html('Mã nhóm chưa được khai nhập').show();
						return;
					}
					if(VTUtilJS.isNullOrEmpty(params.groupName)) {
						$('#errMsgGroupDlg').html('Tên nhóm chưa được khai nhập').show();
						return;
					}
					params.promotionId = $('#id').val();
					$.messager.confirm("Xác nhận", "Bạn có muốn cập nhật nhóm sản phẩm mua?", function(r) {
						if(r) {
							VTUtilJS.postFormJson(params, '/promotion/add-group-sale', function(data) {
								if(data.error) {
									$('#errMsgGroupDlg').html(data.errMsg).show();
								} else {
									$('#popup-edit-group-mua').dialog('destroy');
									$('#popup-container').remove();
									$('#groupMuaGrid').datagrid('reload');
								}
							});
						}
					});
				});
			},
			onClose: function() {
				$("#popup-edit-group-mua").dialog("destroy");
				$("#popup-container").remove();
			}
		});
	},
	
	popupEditGroupKM : function(productGroupId, groupCode, groupName, maxQuantity, maxAmount) {
		var html = '<div id="popup-container"><div id="popup-edit-group-mua" class="PopupContentMid">\
			<div class="GeneralForm Search1Form" id="form-data">\
			<label style="width: 100px;" class="LabelStyle Label1Style">Mã nhóm<span class="ReqiureStyle">(*)</span></label>\
			<input type="text" call-back="VTValidateUtils.getMessageOfRequireCheck(\'groupCode\', \'Mã Nhóm\', false, true);VTValidateUtils.getMessageOfSpecialCharactersValidate(\'groupCode\', \'Mã Nhóm\', Utils._CODE)" disabled="disabled" class="InputTextStyle InputText1Style" style="width: 145px;" maxlength="50" id="groupCode" value="'+Utils.XSSEncode(groupCode)+'"> \
			<label style="width: 100px;" class="LabelStyle Label1Style">Tên nhóm<span class="ReqiureStyle">(*)</span></label> \
			<input type="text" call-back="VTValidateUtils.getMessageOfRequireCheck(\'promotionName\', \'Tên Nhóm\', false, true);VTValidateUtils.getMessageOfSpecialCharactersValidate(\'groupName\', \'Tên Nhóm\', Utils._NAME)" class="InputTextStyle InputText1Style" style="width: 145px;" maxlength="100" id="groupName" value="'+Utils.XSSEncode(groupName)+'">\
			<div class="Clear"></div>';
			/*html += '<label style="width: 100px; visibility: hidden;" class="LabelStyle Label1Style">Số lượng tối đa</label> \
			<input type="text" class="InputTextStyle InputText1Style" style="width: 145px; visibility: hidden;" maxlength="20" id="maxQuantity" value="'+VTUtilJS.formatCurrency(maxQuantity)+'"> \
			<label style="width: 100px; visibility: hidden;" class="LabelStyle Label1Style">Số tiền tối đa</label> \
			<input type="text" class="InputTextStyle InputText1Style" style="width: 145px; visibility: hidden;" maxlength="20" id="maxAmount" value="'+VTUtilJS.formatCurrency(maxAmount)+'">\
			<input type="hidden" id="groupId" value="'+productGroupId+'">';*/
			html += '<div class="Clear"></div>\
			<div class="BtnCenterSection">\
			<button class="BtnGeneralStyle" id="btn-update">Cập nhật</button>\
			</div>\
			<p class="ErrorMsgStyle SpriteErr" style="display: none;" id="errMsgGroupDlg"></p>\
			<p class="SuccessMsgStyle" style="display: none;" id="successMsgGroupDlg"></p></div></div></div>';
		$("body").append(html);
		$('#popup-edit-group-mua').dialog({
			title: 'Thông tin nhóm KM',
			width: 550,
			heigth: "auto",
			modal: true, close: false, cache: false,
			onOpen: function() {
				VTUtilJS.bindFormatOnTextfield('maxQuantity', VTUtilJS._TF_NUMBER);
				VTUtilJS.formatCurrencyFor('maxQuantity');
				VTUtilJS.bindFormatOnTextfield('maxAmount', VTUtilJS._TF_NUMBER);
				VTUtilJS.formatCurrencyFor('maxAmount');
				$('#btn-update').bind('click', function() {
					var params = VTUtilJS.getFormData('form-data');
					if($('#errorMsg').html().trim().length > 0 && $('#errorMsg').is(":visible")) {
						$('#errMsgGroupDlg').html($('#errorMsg').html()).show();
						$('#errorMsg').html('').hide();
					}
					if(params == null) {
						return;
					}
					$('.SuccessMsgStyle').hide();
					$('#errMsgGroupDlg').html('').hide();
					if(VTUtilJS.isNullOrEmpty(params.groupCode)) {
						$('#errMsgGroupDlg').html('Mã nhóm chưa được khai nhập').show();
						return;
					}
					if(VTUtilJS.isNullOrEmpty(params.groupName)) {
						$('#errMsgGroupDlg').html('Tên nhóm chưa được khai nhập').show();
						return;
					}
					params.promotionId = $('#id').val();
					$.messager.confirm("Xác nhận", "Bạn có muốn cập nhật nhóm sản phẩm khuyến mại?", function(r) {
						if(r) {
							VTUtilJS.postFormJson(params, '/promotion/add-group-free', function(data) {
								if(data.error) {
									$('#errMsgGroupDlg').html(data.errMsg).show();
								} else {
									$('#popup-edit-group-mua').dialog('destroy');
									$('#popup-container').remove();
									$('#groupKMGrid').datagrid('reload');
								}
							});
						}
					});
				});
			},
			onClose: function() {
				$("#popup-edit-group-mua").dialog("destroy");
				$("#popup-container").remove();
			}
		});
	},
	
	popupEditGroupNew : function(groupMuaId, groupKMId, groupCode, groupName, stt, multiple, recursive) {
		var textDisQuantity = '';
		var textDisAmount = '';
		var html = '<div id="popup-container"><div id="popup-edit-group-mua" class="PopupContentMid">\
			<div class="GeneralForm Search1Form" id="form-data">\
			<label style="width: 100px;" class="LabelStyle Label1Style">Mã nhóm<span class="ReqiureStyle">(*)</span></label>\
			<input type="text" call-back="VTValidateUtils.getMessageOfRequireCheck(\'groupCode\', \'Mã Nhóm\', false, true);VTValidateUtils.getMessageOfSpecialCharactersValidate(\'groupCode\', \'Mã Nhóm\', Utils._CODE)" class="InputTextStyle InputText1Style" style="width: 145px;" maxlength="50" disabled="disabled" id="groupCode" value="'+Utils.XSSEncode(groupCode)+'"> \
			<label style="width: 100px;" class="LabelStyle Label1Style">Tên nhóm<span class="ReqiureStyle">(*)</span></label> \
			<input type="text" class="InputTextStyle InputText1Style" call-back="VTValidateUtils.getMessageOfRequireCheck(\'promotionName\', \'Tên Nhóm\', false, true);VTValidateUtils.getMessageOfSpecialCharactersValidate(\'groupName\', \'Tên Nhóm\', Utils._NAME)" style="width: 145px;" maxlength="100" id="groupName" value="'+Utils.XSSEncode(groupName)+'">\
			<div class="Clear"></div>';
			if($('#promotionType').val() != 'ZV01' && $('#promotionType').val() != 'ZV04' && $('#promotionType').val() != 'ZV07' && $('#promotionType').val() != 'ZV10' && $('#promotionType').val() != 'ZV19') {
				if( $('#promotionType').val() == 'ZV13' || $('#promotionType').val() == 'ZV16') {
					html +=	'<label class="LabelStyle" style="width:100px;">Bội số</label><input type="checkbox" id="multiple" '+(multiple==1?'checked="checked"':'')+' class="InputCbxStyle"/>\
					<label class="LabelStyle" style="width:128px;"></label>';
				} else {
					html +=	'<label class="LabelStyle" style="width:100px;">Bội số</label><input type="checkbox" id="multiple" '+(multiple==1?'checked="checked"':'')+' class="InputCbxStyle"/>\
					<label class="LabelStyle" style="width:111px;">Tối ưu</label><input type="checkbox" id="recursive" '+(recursive==1?'checked="checked"':'')+' class="InputCbxStyle"/>';
				}
			}
			html += '<label style="width: 100px;" class="LabelStyle Label1Style">Thứ tự ưu tiên</label> \
			<input type="text" class="InputTextStyle InputText1Style" style="width: 145px;" maxlength="3" id="stt" value="'+stt+'">\
			<input type="hidden" id="groupMuaId" value="'+groupMuaId+'">\
			<input type="hidden" id="groupKMId" value="'+groupKMId+'">\
			<div class="Clear"></div>\
			<div class="BtnCenterSection">';
			if($('#statusHidden').val() == 2) {
				html += '<button class="BtnGeneralStyle" id="btn-update">Cập nhật</button>';
			}
			html += '</div>\
			<p class="ErrorMsgStyle SpriteErr" style="display: none;" id="errMsgGroupDlg"></p>\
			<p class="SuccessMsgStyle" style="display: none;" id="successMsgGroupDlg"></p></div></div></div>';
		$("body").append(html);
		Utils.bindFormatOnTextfield('stt', Utils._TF_NUMBER);
		$('#popup-edit-group-mua').dialog({
			title: 'Thông tin nhóm',
			width: 550,
			heigth: "auto",
			modal: true, close: false, cache: false,
			onOpen: function() {
				VTUtilJS.bindFormatOnTextfield('minQuantity', VTUtilJS._TF_NUMBER);
				VTUtilJS.formatCurrencyFor('minQuantity');
				VTUtilJS.bindFormatOnTextfield('minAmount', VTUtilJS._TF_NUMBER);
				VTUtilJS.formatCurrencyFor('minAmount');
				if($('#statusHidden').val() == 2) {
					$('#btn-update').bind('click', function() {
						var params = VTUtilJS.getFormData('form-data');
						if($('#errorMsg').html().trim().length > 0 && $('#errorMsg').is(":visible")) {
							$('#errMsgGroupDlg').html($('#errorMsg').html()).show();
							$('#errorMsg').html('').hide();
						}
						if(params == null) {
							return;
						}
						$('.SuccessMsgStyle').hide();
						$('#errMsgGroupDlg').html('').hide();
						if(VTUtilJS.isNullOrEmpty(params.groupCode)) {
							$('#errMsgGroupDlg').html('Mã nhóm chưa được khai nhập').show();
							return;
						}
						if(VTUtilJS.isNullOrEmpty(params.groupName)) {
							$('#errMsgGroupDlg').html('Tên nhóm chưa được khai nhập').show();
							return;
						}
						params.promotionId = $('#id').val();
						$.messager.confirm("Xác nhận", "Bạn có muốn cập nhật nhóm?", function(r) {
							if(r) {
								VTUtilJS.postFormJson(params, '/promotion/add-group-new', function(data) {
									if(data.error) {
										$('#errMsgGroupDlg').html(data.errMsg).show();
									} else {
										$('#popup-edit-group-mua').dialog('destroy');
										$('#popup-container').remove();
										$('#groupGrid').datagrid('reload');
									}
								});
							}
						});
					});
				}
			},
			onClose: function() {
				$("#popup-edit-group-mua").dialog("destroy");
				$("#popup-container").remove();
			}
		});
	},
	
	popupAddGroupMua : function() {
		var textDisQuantity = '';
		var textDisAmount = '';
		if($('#promotionType').val() == 'ZV01' || $('#promotionType').val() == 'ZV02' || $('#promotionType').val() == 'ZV03' || 
				$('#promotionType').val() == 'ZV07' || $('#promotionType').val() == 'ZV08' || $('#promotionType').val() == 'ZV09' ||
				$('#promotionType').val() == 'ZV13' || $('#promotionType').val() == 'ZV14' || $('#promotionType').val() == 'ZV15') {
			textDisQuantity = '';
			textDisAmount = 'disabled="disabled"';
		} else {
			textDisQuantity = 'disabled="disabled"';
			textDisAmount = '';
		}
		var html = '<div id="popup-container"><div id="popup-add-group-mua" class="PopupContentMid">\
			<div class="GeneralForm Search1Form" id="form-data">\
			<label style="width: 100px;" class="LabelStyle Label1Style">Mã nhóm<span class="ReqiureStyle">(*)</span></label>\
			<input type="text" call-back="VTValidateUtils.getMessageOfSpecialCharactersValidate(\'groupCode\', \'Mã nhóm\', Utils._CODE)" class="InputTextStyle InputText1Style" style="width: 145px;" maxlength="50" id="groupCode"> \
			<label style="width: 100px;" class="LabelStyle Label1Style">Tên nhóm<span class="ReqiureStyle">(*)</span></label> \
			<input type="text" class="InputTextStyle InputText1Style" style="width: 145px;" maxlength="100" id="groupName">\
			<div class="Clear"></div>';
			html += '<div class="Clear"></div>';
			if($('#promotionType').val() != 'ZV01' && $('#promotionType').val() != 'ZV04' && $('#promotionType').val() != 'ZV07' && $('#promotionType').val() != 'ZV10' && $('#promotionType').val() != 'ZV19') {
				html +=	'<label class="LabelStyle" style="width:100px;">Bội số</label><input type="checkbox" id="multiple" class="InputCbxStyle"/>\
				<label class="LabelStyle" style="width:131px;">Tối ưu</label><input type="checkbox" id="recursive" class="InputCbxStyle"/>';
			} else {
				html +=	'<label class="LabelStyle" style="width:100px; visibility: hidden;">Bội số</label><input style="visibility: hidden;" type="checkbox" id="multiple" class="InputCbxStyle"/>\
				<label class="LabelStyle" style="width:131px; visibility: hidden;">Tối ưu</label><input style="visibility: hidden;" type="checkbox" id="recursive" class="InputCbxStyle"/>';
			}
			html += '<label class="LabelStyle Label1Style">Thứ tự ưu tiên</label> \
			<input type="text" class="InputTextStyle InputText1Style" style="width: 145px;" maxlength="3" id="stt">\
			<div class="Clear"></div>\
			<div class="BtnCenterSection">\
			<button class="BtnGeneralStyle" id="btn-update">Cập nhật</button>\
			</div>\
			<p class="ErrorMsgStyle SpriteErr" style="display: none;" id="errMsgGroupDlg"></p>\
			<p class="SuccessMsgStyle" style="display: none;" id="successMsgGroupDlg"></p></div></div></div>';
		$("body").append(html);
		Utils.bindFormatOnTextfield('stt', Utils._TF_NUMBER);
		$('#popup-add-group-mua').dialog({
			title: 'Thông tin nhóm mua',
			width: 550,
			heigth: "auto",
			modal: true, close: false, cache: false,
			onOpen: function() {
				VTUtilJS.bindFormatOnTextfield('minQuantity', VTUtilJS._TF_NUMBER);
				VTUtilJS.formatCurrencyFor('minQuantity');
				VTUtilJS.bindFormatOnTextfield('minAmount', VTUtilJS._TF_NUMBER);
				VTUtilJS.formatCurrencyFor('minAmount');
				$('#btn-update').bind('click', function() {
					var params = VTUtilJS.getFormData('form-data');
					if($('#errorMsg').html().trim().length > 0 && $('#errorMsg').is(":visible")) {
						$('#errMsgGroupDlg').html($('#errorMsg').html()).show();
						$('#errorMsg').html('').hide();
					}
					if(params == null) {
						return;
					}
					$('.SuccessMsgStyle').hide();
					$('#errMsgGroupDlg').html('').hide();
					if(VTUtilJS.isNullOrEmpty(params.groupCode)) {
						$('#errMsgGroupDlg').html('Mã nhóm chưa được khai nhập').show();
						return;
					}
					if(VTUtilJS.isNullOrEmpty(params.groupName)) {
						$('#errMsgGroupDlg').html('Tên nhóm chưa được khai nhập').show();
						return;
					}
					params.promotionId = $('#id').val();
					$.messager.confirm("Xác nhận", "Bạn có muốn thêm nhóm sản phẩm mua?", function(r) {
						if(r) {
							VTUtilJS.postFormJson(params, '/promotion/add-group-sale', function(data) {
								if(data.error) {
									$('#errMsgGroupDlg').html(data.errMsg).show();
								} else {
									$('#popup-add-group-mua').dialog('destroy');
									$('#popup-container').remove();
									$('#groupMuaGrid').datagrid('reload');
								}
							});
						}
					});
				});
			},
			onClose: function() {
				$("#popup-add-group-mua").dialog("destroy");
				$("#popup-container").remove();
			}
		});
	},
	
	popupAddGroupKM : function(promotionId) {
		var html = '<div id="popup-container"><div id="popup-add-group-mua" class="PopupContentMid">\
			<div class="GeneralForm Search1Form" id="form-data">\
			<label style="width: 100px;" class="LabelStyle Label1Style">Mã nhóm<span class="ReqiureStyle">(*)</span></label>\
			<input type="text" call-back="VTValidateUtils.getMessageOfSpecialCharactersValidate(\'groupCode\', \'Mã nhóm\', Utils._CODE)" class="InputTextStyle InputText1Style" style="width: 145px;" maxlength="50" id="groupCode"> \
			<label style="width: 100px;" class="LabelStyle Label1Style">Tên nhóm<span class="ReqiureStyle">(*)</span></label> \
			<input type="text" class="InputTextStyle InputText1Style" style="width: 145px;" maxlength="100" id="groupName">\
			<div class="Clear"></div>';
			/*html += '<label style="width: 100px; visibility: hidden;" class="LabelStyle Label1Style">Số lượng tối đa</label> \
			<input type="text" class="InputTextStyle InputText1Style" style="width: 145px; visibility: hidden;" maxlength="20" id="maxQuantity"> \
			<label style="width: 100px; visibility: hidden;" class="LabelStyle Label1Style">Số tiền tối đa</label> \
			<input type="text" class="InputTextStyle InputText1Style" style="width: 145px; visibility: hidden;" maxlength="20" id="maxAmount">';*/
			html += '<div class="Clear"></div>\
			<div class="BtnCenterSection">\
			<button class="BtnGeneralStyle" id="btn-update">Cập nhật</button>\
			</div>\
			<p class="ErrorMsgStyle SpriteErr" style="display: none;" id="errMsgGroupDlg"></p>\
			<p class="SuccessMsgStyle" style="display: none;" id="successMsgGroupDlg"></p></div></div></div>';
		$("body").append(html);
		$('#popup-add-group-mua').dialog({
			title: 'Thông tin nhóm khuyến mại',
			width: 550,
			heigth: "auto",
			modal: true, close: false, cache: false,
			onOpen: function() {
				VTUtilJS.bindFormatOnTextfield('maxQuantity', VTUtilJS._TF_NUMBER);
				VTUtilJS.formatCurrencyFor('maxQuantity');
				VTUtilJS.bindFormatOnTextfield('maxAmount', VTUtilJS._TF_NUMBER);
				VTUtilJS.formatCurrencyFor('maxAmount');
				$('#btn-update').bind('click', function() {
					var params = VTUtilJS.getFormData('form-data');
					if($('#errorMsg').html().trim().length > 0 && $('#errorMsg').is(":visible")) {
						$('#errMsgGroupDlg').html($('#errorMsg').html()).show();
						$('#errorMsg').html('').hide();
					}
					if(params == null) {
						return;
					}
					$('.SuccessMsgStyle').hide();
					$('#errMsgGroupDlg').html('').hide();
					if(VTUtilJS.isNullOrEmpty(params.groupCode)) {
						$('#errMsgGroupDlg').html('Mã nhóm chưa được khai nhập').show();
						return;
					}
					if(VTUtilJS.isNullOrEmpty(params.groupName)) {
						$('#errMsgGroupDlg').html('Tên nhóm chưa được khai nhập').show();
						return;
					}
					params.promotionId = $('#id').val();
					$.messager.confirm("Xác nhận", "Bạn có muốn thêm nhóm sản phẩm khuyến mại?", function(r) {
						if(r) {
							VTUtilJS.postFormJson(params, '/promotion/add-group-free', function(data) {
								if(data.error) {
									$('#errMsgGroupDlg').html(data.errMsg).show();
								} else {
									$('#popup-add-group-mua').dialog('destroy');
									$('#popup-container').remove();
									$('#groupKMGrid').datagrid('reload');
								}
							});
						}
					});
				});
			},
			onClose: function() {
				$("#popup-add-group-mua").dialog("destroy");
				$("#popup-container").remove();
			}
		});
	},
	
	popupAddGroupNew: function() {
		$('.ErrorMsgStyle').hide();
		if ($("#typeCode").length == 0) {
			return;
		}
		var typeCodeTmp = $("#typeCode").val().trim();
		var lstZVMultGroup = [ "ZV01", "ZV02", "ZV03", "ZV04", "ZV05", "ZV06", "ZV09", "ZV21" ];
		if (lstZVMultGroup.indexOf(typeCodeTmp) < 0 && $("#groupGridContainer .datagrid-row td[field=delete] a").length > 0) {
			$("#groupErrMsg").html("Chỉ được thêm nhiều nhóm cho các ZV " + lstZVMultGroup.join(", ")).show();
			return;
		}
		var textDisQuantity = '';
		var textDisAmount = '';
		var html = '<div id="popup-container"><div id="popup-add-group-mua" class="PopupContentMid">\
			<div class="GeneralForm Search1Form" id="form-data">\
			<label style="width: 100px;" class="LabelStyle Label1Style">Mã nhóm<span class="ReqiureStyle">(*)</span></label>\
			<input type="text" call-back="VTValidateUtils.getMessageOfSpecialCharactersValidate(\'groupCode\', \'Mã nhóm\', Utils._CODE)" class="InputTextStyle InputText1Style" style="width: 145px;" maxlength="50" id="groupCode"> \
			<label style="width: 100px;" class="LabelStyle Label1Style">Tên nhóm<span class="ReqiureStyle">(*)</span></label> \
			<input type="text" call-back="Utils.getMessageOfSpecialCharactersValidate(\'groupName\', \'Tên CTKM\', Utils._SPECIAL)" class="InputTextStyle InputText1Style" style="width: 145px;" maxlength="100" id="groupName">\
			<div class="Clear"></div>';
			if($('#promotionType').val() != 'ZV01' && $('#promotionType').val() != 'ZV04' && $('#promotionType').val() != 'ZV07' && $('#promotionType').val() != 'ZV10' && $('#promotionType').val() != 'ZV19') {
				if( $('#promotionType').val() == 'ZV13' || $('#promotionType').val() == 'ZV16') {
					html +=	'<label class="LabelStyle" style="width:100px;">Bội số</label><input type="checkbox" id="multiple" class="InputCbxStyle"/>\
					<label class="LabelStyle" style="width:128px;"></label>';
				} else {
					html +=	'<label class="LabelStyle" style="width:100px;">Bội số</label><input type="checkbox" id="multiple" class="InputCbxStyle"/>\
					<label class="LabelStyle" style="width:111px;">Tối ưu</label><input type="checkbox" id="recursive" class="InputCbxStyle"/>';
				}
			}
			html += '<label class="LabelStyle Label1Style" style="width: 100px;">Thứ tự ưu tiên</label> \
			<input type="text" class="InputTextStyle InputText1Style" style="width: 145px;" maxlength="3" id="stt" call-back="VTValidateUtils.getMessageOfInvaildInteger(\'stt\', \'Thứ tự ưu tiên\');VTValidateUtils.getMessageOfNegativeNumberCheck(\'stt\', \'Thứ tự ưu tiên\');">\
			<div class="Clear"></div>\
			<div class="BtnCenterSection">\
			<button class="BtnGeneralStyle" id="btn-update">Cập nhật</button>\
			</div>\
			<p class="ErrorMsgStyle SpriteErr" style="display: none;" id="errMsgGroupDlg"></p>\
			<p class="SuccessMsgStyle" style="display: none;" id="successMsgGroupDlg"></p></div></div></div>';
		$("body").append(html);
		Utils.bindFormatOnTextfield('stt', Utils._TF_NUMBER);
		$('#popup-add-group-mua').dialog({
			title: 'Thông tin nhóm',
			width: 550,
			heigth: "auto",
			modal: true, close: false, cache: false,
			onOpen: function() {
				VTUtilJS.bindFormatOnTextfield('minQuantity', VTUtilJS._TF_NUMBER);
				VTUtilJS.formatCurrencyFor('minQuantity');
				VTUtilJS.bindFormatOnTextfield('minAmount', VTUtilJS._TF_NUMBER);
				VTUtilJS.formatCurrencyFor('minAmount');
				$('#btn-update').bind('click', function() {
					var params = VTUtilJS.getFormData('form-data');
					if($('#errorMsg').html().trim().length > 0 && $('#errorMsg').is(":visible")) {
						$('#errMsgGroupDlg').html($('#errorMsg').html()).show();
						$('#errorMsg').html('').hide();
					}
					if(params == null) {
						return;
					}
					$('.SuccessMsgStyle').hide();
					$('#errMsgGroupDlg').html('').hide();
					if(VTUtilJS.isNullOrEmpty(params.groupCode)) {
						$('#errMsgGroupDlg').html('Mã nhóm chưa được khai nhập').show();
						return;
					}
					if(VTUtilJS.isNullOrEmpty(params.groupName)) {
						$('#errMsgGroupDlg').html('Tên nhóm chưa được khai nhập').show();
						return;
					}
					params.promotionId = $('#id').val();
					$.messager.confirm("Xác nhận", "Bạn có muốn thêm nhóm sản phẩm mua?", function(r) {
						if(r) {
							VTUtilJS.postFormJson(params, '/promotion/add-group-new', function(data) {
								if(data.error) {
									$('#errMsgGroupDlg').html(data.errMsg).show();
								} else {
									$('#popup-add-group-mua').dialog('destroy');
									$('#popup-container').remove();
									$('#groupGrid').datagrid('reload');
								}
							});
						}
					});
				});
			},
			onClose: function() {
				$("#popup-add-group-mua").dialog("destroy");
				$("#popup-container").remove();
			}
		});
	},
	
	deleteProductGroup : function(gridDiv, productGroupId) {
		var params = {groupId : productGroupId};
		$.messager.confirm("Xác nhận", "Bạn có muốn xóa nhóm?", function(r) {
			if(r) {
				VTUtilJS.postFormJson(params, '/promotion/delete-group', function(data) {
					if(data.error) {
						VTUtilJS.showMessageBanner(true, data.errMsg);
					} else {
						VTUtilJS.showMessageBanner(false, 'Xóa dữ liệu thành công');
						$('#'+gridDiv).datagrid('reload');
					}
				});
			}
		});
	},
	
	deleteProductGroupNew : function(gridDiv, groupMuaId, groupKMId) {
		$('.ErrorMsgStyle').hide();
		var params = {groupMuaId : groupMuaId, groupKMId : groupKMId};
		$.messager.confirm("Xác nhận", "Bạn có muốn xóa nhóm?", function(r) {
			if(r) {
				VTUtilJS.postFormJson(params, '/promotion/delete-group-new', function(data) {
					if(data.error) {
						VTUtilJS.showMessageBanner(true, data.errMsg);
					} else {
						VTUtilJS.showMessageBanner(false, 'Xóa dữ liệu thành công');
						$('#'+gridDiv).datagrid('reload');
						if ($("#typeCode").length == 0) {
							return;
						}
					}
				});
			}
		});
	},
	
	distributeLevel : function() {
		$('#errorMsgDistribute').html('').hide();
		var cbGroupMuaId = $('#cbGroupMua').val();
		var cbGroupKMId = $('#cbGroupKM').val();
		VTUtilJS.getFormJson({groupMuaId : cbGroupMuaId, groupKMId : cbGroupKMId}, '/promotion/list-level-of-group', function(data) {
			if(!data.error) {
				var listLevelMua = data.listLevelMua;
				var listLevelKM = data.listLevelKM;
				if(!$.isArray(listLevelMua) || !$.isArray(listLevelKM) || listLevelMua.length != listLevelKM.length) {
					$('#errorMsgDistribute').html('Mức của nhóm mua và nhóm khuyến mại chưa phù hợp. không thể tự động phân bổ được').show();
					$('#mappingGrid').datagrid('loadData', []);
					return;
				}
				var listDistribution = new Array();
				for(var i = 0; i < listLevelMua.length; i++) {
					var levelMua = listLevelMua[i];
					var levelKM = listLevelKM[i];
					var groupMua = PromotionProgram._mapGroupMua.get(cbGroupMuaId);
					var groupKM = PromotionProgram._mapGroupKM.get(cbGroupKMId);
					var obj = new Object();
					obj.idLevelMua = groupMua.id;
					obj.groupMuaCode = groupMua.productGroupCode;
					obj.groupMuaName = groupMua.productGroupName;
					obj.groupMuaText = levelMua.groupText;
					obj.orderLevelMua = levelMua.orderNumber;
					obj.minQuantityMua = levelMua.minQuantity;
					obj.minAmountMua = levelMua.minAmount;
					
					obj.idLevelKM = groupKM.id;
					obj.groupKMCode = groupKM.productGroupCode;
					obj.groupKMName = groupKM.productGroupName;
					obj.groupKMText = levelKM.groupText;
					obj.orderLevelKM = levelKM.orderNumber;
					obj.maxQuantityKM = levelKM.maxQuantity;
					obj.maxAmountKM = levelKM.maxAmount;
					obj.percentKM = levelKM.promotionPercent;
					
					listDistribution.push(obj);
				}
				
				$('#mappingGrid').datagrid('loadData', listDistribution);
			}
		});
	},
	
	deleteMapping : function(idx) {
		$('#errorMsg').html('').hide();
		$('#mappingGrid').datagrid('acceptChanges');
		var row = $('#mappingGrid').datagrid('getRows')[idx];
		var mappingId = row.mappingId;
		var groupMuaCode = row.groupMuaCode;
		var orderLevelMua = row.orderLevelMua;
		var groupKMCode = row.groupKMCode;
		var orderLevelKM = row.orderLevelKM;
		var id = $('#id').val();
		var params = new Object();
		if(!VTUtilJS.isNullOrEmpty(mappingId)) {
			params.mappingId = mappingId;
		}
		if(!VTUtilJS.isNullOrEmpty(groupMuaCode)) {
			params.groupMuaCode = groupMuaCode;
		}
		if(!VTUtilJS.isNullOrEmpty(orderLevelMua)) {
			params.orderLevelMua = orderLevelMua;
		}
		if(!VTUtilJS.isNullOrEmpty(groupKMCode)) {
			params.groupKMCode = groupKMCode;
		}
		if(!VTUtilJS.isNullOrEmpty(orderLevelKM)) {
			params.orderLevelKM = orderLevelKM;
		}
		params.id = id;
		$.messager.confirm("Xác nhận", "Bạn có muốn xóa cơ cấu?", function(r) {
			if(r) {
				VTUtilJS.postFormJson(params, '/promotion/delete-mapping', function(data) {
					if(data.error) {
						$('#errorMsg').html(data.errMsg).show();;
					} else {
						$('#mappingGrid').datagrid('getRows').splice(idx, 1);
						$('#mappingGrid').datagrid('loadData', $('#mappingGrid').datagrid('getRows'));
					}
				});
			}
		});
	},
	
	saveMapping : function(idx) {
		$('#errorMsg').html('').hide();
		$('#mappingGrid').datagrid('acceptChanges');
		var row = $('#mappingGrid').datagrid('getRows')[idx];
		var groupMuaCode = row.groupMuaCode;
		var orderLevelMua = row.orderLevelMua;
		var groupKMCode = row.groupKMCode;
		var orderLevelKM = row.orderLevelKM;
		var id = $('#id').val();
		var params = new Object();
		if(!VTUtilJS.isNullOrEmpty(groupMuaCode)) {
			params.groupMuaCode = groupMuaCode;
		}
		if(!VTUtilJS.isNullOrEmpty(orderLevelMua)) {
			params.orderLevelMua = orderLevelMua;
		}
		if(!VTUtilJS.isNullOrEmpty(groupKMCode)) {
			params.groupKMCode = groupKMCode;
		}
		if(!VTUtilJS.isNullOrEmpty(orderLevelKM)) {
			params.orderLevelKM = orderLevelKM;
		}
		params.id = id;
		$.messager.confirm("Xác nhận", "Bạn có muốn lưu cơ cấu?", function(r) {
			if(r) {
				VTUtilJS.postFormJson(params, '/promotion/save-mapping', function(data) {
					if(data.error) {
						$('#errorMsg').html(data.errMsg).show();;
					} else {
						var groupMapping = data.groupMapping;
						$('#mappingGrid').datagrid('getRows')[idx] = groupMapping;
						$('#mappingGrid').datagrid('loadData', $('#mappingGrid').datagrid('getRows'));
					}
				});
			}
		});
	},
	
	addMapping : function() {
		$('#errorMsgDistribute').html('').hide();
		var groupMuaId = $('#cbGroupMua').val();
		var groupKMId = $('#cbGroupKM').val();
		var groupMuaCode = PromotionProgram._mapGroupMua.get(groupMuaId).productGroupCode;
		var groupMuaName = PromotionProgram._mapGroupMua.get(groupMuaId).productGroupName;
		var groupKMCode = PromotionProgram._mapGroupKM.get(groupKMId).productGroupCode;
		var groupKMName = PromotionProgram._mapGroupKM.get(groupKMId).productGroupName;
		var listLevelMua = PromotionProgram._mapLevelMua.get(groupMuaCode);
		if(!$.isArray(listLevelMua) || listLevelMua.length == 0) {
			$('#errorMsgDistribute').html('Nhóm ' + Utils.XSSEncode(groupMuaCode) + ' chưa có mức. Vui lòng qua tab [Khai báo nhóm] để thêm mức').show();
			return;
		}
		var orderLevelMua = listLevelMua[0].orderNumber;
		var minQuantityMua = listLevelMua[0].minQuantity;
		var minAmountMua = listLevelMua[0].minAmount;
		
		var listLevelKM = PromotionProgram._mapLevelKM.get(groupKMCode);
		if(!$.isArray(listLevelKM) || listLevelKM.length == 0) {
			$('#errorMsgDistribute').html('Nhóm ' + Utils.XSSEncode(groupKMCode) + ' chưa có mức. Vui lòng qua tab [Khai báo nhóm] để thêm mức').show();
			return;
		}
		var orderLevelKM = listLevelKM[0].orderNumber;
		var maxQuantityKM = listLevelKM[0].maxQuantity;
		var maxAmountKM = listLevelKM[0].maxAmount;
		var percentKM = listLevelKM[0].promotionPercent;
		var obj = new Object();
		obj.groupMuaCode = groupMuaCode;
		obj.groupMuaName = groupMuaName;
		obj.orderLevelMua = orderLevelMua;
		obj.minQuantityMua = minQuantityMua;
		obj.minAmountMua = minAmountMua;
		
		obj.groupKMCode = groupKMCode;
		obj.groupKMName = groupKMName;
		obj.orderLevelKM = orderLevelKM;
		obj.maxQuantityKM = maxQuantityKM;
		obj.maxAmountKM = maxAmountKM;
		obj.percentKM = percentKM;
		
		$('#mappingGrid').datagrid('appendRow', obj);
		$('#mappingGrid').datagrid('beginEdit', $('#mappingGrid').datagrid('getRows').length-1);
	},
	
	searchShop: function() {
		$(".ErrorMsgStyle").hide();
		$("#labelListCustomer").hide();
		$("#promotionCustomerGrid").hide();
		$("#boxSearch1").hide();
		var params = {
				promotionId: $("#masterDataArea #id").val().trim(),
				code: $("#shopCode").val().trim(),
				name: $("#shopName").val().trim(),
				quantity: isNaN($("#quantityMax").val().trim()) ? "" : $("#quantityMax").val().trim()
		};
		PromotionProgram._promotionShopParams = params;
		$("#exGrid").treegrid("reload", params);
	},
	
	searchCustomerShopMap: function(promotionId, shopId, status) {
		$(".ErrorMsgStyle").hide();
		var r = $("#exGrid").treegrid("find", shopId);
		if (r != null) {
			$("#labelShop").html(r.attr.shopName);
		}
		$("#labelListCustomer").show();
		$("#promotionCustomerGrid").show();
		PromotionProgram.shopId = shopId;
		var params = {
				promotionId: promotionId,
				shopId: shopId,
				code: $("#customerCodeSearch").val().trim(),
				name: $("#customerNameSearch").val().trim(),
				address: $("#customerAddressSearch").val().trim()
		};
		$("#promotionCustomerExGrid").datagrid("load", params);
		$("#btnSearchCustomer").attr("onclick", "PromotionProgram.searchCustomerShopMap("+promotionId+","+shopId+","+status+");");
		$('html, body').animate({ scrollTop: $(document).height() }, 1000);
	},
	
	toggleCustomerSearch: function() {
		var b = $("#boxSearch1").is(":hidden");
		if (b) {
			$("#searchCustomerDiv").html("Đóng tìm kiếm &lt;&lt;");
		} else {
			$("#searchCustomerDiv").html("Tìm kiếm &gt;&gt;");
		}
		$("#boxSearch1").toggle();
	},
	
	searchCustomer: function() {
		$(".ErrorMsgStyle").hide();
		PromotionProgram._promotionStaffParams = {};
		PromotionProgram._promotionStaffParams.promotionId = $("#masterDataArea #id").val().trim();
		PromotionProgram._promotionStaffParams.code = $('#customerCode').val().trim();
		PromotionProgram._promotionStaffParams.name = $('#customerName').val().trim();
		PromotionProgram._promotionStaffParams.address = $('#address').val().trim();
		var params = {
				promotionId: $("#masterDataArea #id").val().trim(),
				code: $("#customerCode").val().trim(),
				name: $("#customerName").val().trim(),
				address: $('#address').val().trim()
		};
		PromotionProgram._promotionStaffParams = params;
		$("#exGrid").treegrid("reload", params);
	},
	
	showCustomerDlg: function() {
		if (isNaN(PromotionProgram.shopId)) {
			return;
		}
		$(".ErrorMsgStyle").hide();
		var html = '<div id="add-customerPopupDiv" style="display:none;">\
			<div id="add-customerPopup">\
			<div class="PopupContentMid">\
				<div class="GeneralForm Search1Form">\
					<label class="LabelStyle" style="width:100px;">Mã KH</label>\
					<input id="codeDlg" class="InputTextStyle" style="width:150px;" maxlength="40" />\
					<label class="LabelStyle" style="width:100px;">Tên KH</label>\
					<input id="nameDlg" class="InputTextStyle" style="width:230px;" maxlength="100" />\
					<div class="Clear"></div>\
					<label class="LabelStyle" style="width:100px;">Địa chỉ</label>\
					<input id="addressDlg" class="InputTextStyle" style="width:500px;" maxlength="250" />\
					<div class="Clear"></div>\
					<div class="BtnCenterSection">\
					    <button class="BtnGeneralStyle" id="btnSearchDlg">Tìm kiếm</button>\
					</div>\
					\
					<div class="GridSection" id="gridDlgDiv">\
						<div id="gridDlg"></div>\
					</div>\
					\
					<label class="LabelStyle" style="width:100px;">Số suất</label>\
					<input id="quantityDlg" class="InputTextStyle vinput-money" style="width:150px;" maxlength="11" />\
					<label class="LabelStyle" style="width:100px;">Số tiền</label>\
					<input id="amtDlg" class="InputTextStyle vinput-money" style="width:150px;" maxlength="11" />\
					<label class="LabelStyle" style="width:100px;">Số lượng</label>\
					<input id="numDlg" class="InputTextStyle vinput-money" style="width:150px;" maxlength="11" />\
					<div class="Clear"></div>\
					<p id="erMsgDlg" class="ErrorMsgStyle" style="padding-left:5px;display:none;"></p>\
					<p id="erMsgDlg" class="ErrorMsgStyle" style="padding-left:5px;display:none;"></p>\
					<div class="Clear"></div>\
					<div class="BtnCenterSection">\
					    <button class="BtnGeneralStyle" id="btnChooseDlg">Chọn</button>\
						<button class="BtnGeneralStyle" id="btnCloseDlg" onclick="javascript:$(\'#add-customerPopup\').dialog(\'close\');">Đóng</button>\
					</div>\
				</div>\
			</div>\
			</div>\
		</div>';
		$("body").append(html);
		//Xu ly hien thi kieu tien
		$('.vinput-money').each(function() {
			VTUtilJS.bindFormatOnTextfield($(this), VTUtilJS._TF_NUMBER_COMMA);
			VTUtilJS.formatCurrencyFor($(this));
		});
		
		var r = $("#exGrid").treegrid("find", PromotionProgram.shopId);
		var tt = (r == null) ? "Chọn Khách hàng" : "Chọn Khách hàng (<span style='color:#3a1;'>" + Utils.XSSEncode(r.attr.shopName) + "</span>)";
		var lstTmp = null;
		$("#add-customerPopup").dialog({
			title: tt,
			width: 900,
			heigth: "auto",
			modal: true, close: false, cache: false,
			onOpen: function() {
				$("#add-customerPopup").addClass("easyui-dialog");
				VTUtilJS.bindFormatOnTextfield("quantityDlg", VTUtilJS._TF_NUMBER);
				$("#add-customerPopup #codeDlg").focus();
				
				lstTmp = new CArray();
				$("#add-customerPopup #gridDlg").datagrid({
					url: "/promotion/search-customer-dlg?promotionId="+$("#masterDataArea #id").val().trim()+"&shopId="+Number(PromotionProgram.shopId),
					rownumbers: false,
					width: $("#add-customerPopup #gridDlgDiv").width(),
					height: "auto",
					pagination: true,
					fitColumns: true,
					idField: "customerId",
					scrollbarSize: 0,
					columns: [[
						{field:"no", title:"STT", sortable: false, resizable: false, width:45, fixed:true, align:"center", formatter:function(v, r, i) {
							var p = $("#add-customerPopup #gridDlg").datagrid("options").pageNumber;
							var n = $("#add-customerPopup #gridDlg").datagrid("options").pageSize;
							return (Number(p) - 1) * Number(n) + i + 1;
						}},
						{field:"customerCode", title:"Mã KH", sortable: false, resizable: false, width:75, align:"left", formatter: function(value, row, index) {
							return VTUtilJS.XSSEncode(value);
						}},
						{field:"customerName", title:"Tên KH", sortable: false, resizable: false, width:120, align:"left", formatter: function(value, row, index) {
							return VTUtilJS.XSSEncode(value);
						}},
						{field:"address", title:"Địa chỉ", sortable: false, resizable: false, width:200, align:"left", formatter: function(value, row, index) {
							return VTUtilJS.XSSEncode(value);
						}},
						{field:"ck", title:"", checkbox:true, sortable: false, resizable: false, align:"center"}
					]],
					onLoadSuccess: function(data) {
						$("#add-customerPopup #gridDlg").datagrid("resize");
						setTimeout(function(){
							var hDlg=parseInt($("#add-customerPopup").parent().height());
							var hW=parseInt($(window).height());
							var d=hW-hDlg;
							d=d/2+document.documentElement.scrollTop;
							if (d < 0) { d = 0; }
							$("#add-customerPopup").parent().css('top',d);
			    		},1000);
					},
					onCheck: function(i, r) {
						lstTmp.push(r.customerId);
					},
					onUncheck: function(i, r) {
						lstTmp.remove(r.customerId);
					},
					onCheckAll: function(rows) {
						for (var i = 0, sz = rows.length; i < sz; i++) {
							lstTmp.push(rows[i].customerId);
						}
					},
					onUncheckAll: function(rows) {
						for (var i = 0, sz = rows.length; i < sz; i++) {
							lstTmp.remove(rows[i].customerId);
						}
					}
				});
				
				$("#add-customerPopup #btnSearchDlg").click(function() {
					var p = {
							code: $("#add-customerPopup #codeDlg").val().trim(),
							name: $("#add-customerPopup #nameDlg").val().trim(),
							address: $("#add-customerPopup #addressDlg").val().trim()
					};
					$("#add-customerPopup #gridDlg").datagrid("reload", p);
				});
				$("#add-customerPopup #codeDlg").parent().keyup(function(event) {
					if (event.keyCode == keyCodes.ENTER) {
						$("#add-customerPopup #btnSearchDlg").click();
					}
				});
				$("#add-customerPopup #btnChooseDlg").click(function() {
					if (lstTmp == null || lstTmp.size() == 0) {
						$("#erMsgDlg").html("Không có khách hàng nào được chọn").show();
						return;
					}
					var qtt = $("#quantityDlg").val().trim();
					qtt = qtt.replace(/,/g,'').trim();
					if (qtt.length > 0 && (isNaN(qtt) || Number(qtt) <= 0)) {
						$("#erMsgDlg").html("Số suất phải là số nguyên dương").show();
						return;
					}
					qtt = $("#amtDlg").val().trim();
					qtt = qtt.replace(/,/g,'').trim();
					if (qtt.length > 0 && (isNaN(qtt) || Number(qtt) <= 0)) {
						$("#erMsgDlg").html("Số tiền phải là số nguyên dương").show();
						return;
					}
					qtt = $("#numDlg").val().trim();
					qtt = qtt.replace(/,/g,'').trim();
					if (qtt.length > 0 && (isNaN(qtt) || Number(qtt) <= 0)) {
						$("#erMsgDlg").html("Số lượng phải là số nguyên dương").show();
						return;
					}
					PromotionProgram.addCustomerQtt(lstTmp.toArray());
				});
			},
			onClose: function() {
				$("#add-customerPopup").dialog("destroy");
				$("#add-customerPopupDiv").remove();
			}
		});
	},
	
	addCustomerQtt: function(lstCustId) {
		$(".ErrorMsgStyle").hide();
		$.messager.confirm("Xác nhận", "Bạn có muốn thêm khách hàng?", function(r) {
			if (r) {
				var params = {
						promotionId: $("#masterDataArea #id").val().trim(),
						quantity: $("#quantityDlg").val().trim().replace(/,/g,'').trim(),
						amount: $("#amtDlg").val().trim().replace(/,/g,'').trim(),
						number: $("#numDlg").val().trim().replace(/,/g,'').trim(),
						lstId: lstCustId
				};
				$("#add-customerPopup").dialog("close");
				Utils.saveData(params, "/promotion/add-customer", PromotionProgram._xhrSave, 'errMsgShop', function(data) {
					$("#successMsgShop").html("Lưu dữ liệu thành công").show();
					setTimeout(function() {$("#successMsgShop").hide();},3000);
					$("#exGrid").treegrid("reload");
				}, "loading2");
			}
		});
	},
	
	updateCustomerQuantity: function(nodeId, pcmId) {
		$(".ErrorMsgStyle").hide();
		var qtt = $("#txt"+nodeId).val().trim();
		qtt = qtt.replace(/,/g, "");
		var amt = $("#txtamt"+nodeId).val().trim();
		amt = amt.replace(/,/g, "");
		var num = $("#txtnum"+nodeId).val().trim();
		num = num.replace(/,/g, "");
		var isEdit = $("#ck"+nodeId).is(':checked');
		if (qtt.length > 0 && (isNaN(qtt) || Number(qtt) <= 0)) {//hungtt
			$("#errMsgShop").html("Số suất khách hàng phải là số").show();
			return;
		}
		if (amt.length > 0 && (isNaN(amt) || Number(amt) <= 0)) {
			$("#errMsgShop").html("Số tiền phải là số dương").show();
			return;
		}
		if (num.length > 0 && (isNaN(num) || Number(num) <= 0)) {
			$("#errMsgShop").html("Số lượng phải là số dương").show();
			return;
		}
		$.messager.confirm("Xác nhận", "Bạn có muốn cập nhật số suất, số tiền, số lượng cho khách hàng?", function(r) {
			if (r) {
				var params = {
						promotionId: $("#masterDataArea #id").val().trim(),
						id: pcmId,
						quantity: qtt.length == 0 ? qtt : parseInt(qtt),
						amount: amt.length == 0 ? amt : parseInt(amt),	
						number: num.length == 0 ? num : parseInt(num),
				};
				Utils.saveData(params, "/promotion/update-customer-quantity", PromotionProgram._xhrSave, 'errMsgShop', function(data) {
					$("#successMsgCustomer").html("Lưu dữ liệu thành công").show();
					setTimeout(function() {$("#successMsgCustomer").hide();},3000);
					//$("#exGrid").treegrid("reload");
					$("#exGrid").treegrid("reload");
				}, "loading2");
			}
		});
	},
	
	deleteCustomerMap: function(pcmId) {
		$(".ErrorMsgStyle").hide();
		var row = $("#exGrid").treegrid("find", pcmId);
		$.messager.confirm("Xác nhận", "Bạn có muốn xóa khách hàng?", function(r) {
			if (r) {
				var params = {
						promotionId: $("#masterDataArea #id").val().trim(),
				};
				
				if (row.attr.isCustomer == 1) {
					params.id = row.attr.mapId;
				} else {
					params.shopId = nodeId.replace("sh", "");
				}
				Utils.saveData(params, "/promotion/delete-customer", PromotionProgram._xhrSave, 'errMsgCustomer', function(data) {
					$("#successMsgShop").html("Lưu dữ liệu thành công").show();
					setTimeout(function() {$("#successMsgShop").hide();},3000);
					$("#exGrid").treegrid("reload");
				}, "loading2");
			}
		});
	},
	
	updateShopQuantity: function(shopId) {
		$(".ErrorMsgStyle").hide();
		var qtt = $("#txt"+shopId).val().trim();
		qtt = qtt.replace(/,/g, "");
		var amt = $("#txtamt"+shopId).val().trim();
		amt = amt.replace(/,/g, "");
		var num = $("#txtnum"+shopId).val().trim();
		num = num.replace(/,/g, "");
		var isEdit = $("#ck"+shopId).is(':checked');
		if (qtt.length > 0 && (isNaN(qtt) || Number(qtt) <= 0)) {
			$("#errMsgShop").html("Số suất đơn vị phải là số dương").show();
			return;
		}
		if (amt.length > 0 && (isNaN(amt) || Number(amt) <= 0)) {
			$("#errMsgShop").html("Số tiền phải là số dương").show();
			return;
		}
		if (num.length > 0 && (isNaN(num) || Number(num) <= 0)) {
			$("#errMsgShop").html("Số lượng phải là số dương").show();
			return;
		}//hic
		$.messager.confirm("Xác nhận", "Bạn có muốn cập nhật số suất, số tiền, số lượng cho đơn vị?", function(r) {
			if (r) {
				var params = {
						promotionId: $("#masterDataArea #id").val().trim(),
						shopId: shopId,
						quantity: qtt.length == 0 ? qtt : parseInt(qtt),
						amount: amt.length == 0 ? amt : parseInt(amt),
						number: num.length == 0 ? num : parseInt(num),
						isEdited:isEdit?1:0
				};
				Utils.saveData(params, "/promotion/update-shop-quantity", PromotionProgram._xhrSave, 'errMsgShop', function(data) {
					$("#successMsgShop").html("Lưu dữ liệu thành công").show();
					setTimeout(function() {$("#successMsgShop").hide();},3000);
					$("#exGrid").treegrid("reload");
					$("#labelListCustomer").hide();
					$("#promotionCustomerGrid").hide();
					$("#boxSearch1").hide();
				}, "loading2");
			}
		});
	},
	
	deleteShopMap: function(shopId) {
		$(".ErrorMsgStyle").hide();
		$.messager.confirm("Xác nhận", "Bạn có muốn xóa đơn vị?", function(r) {
			if (r) {
				var params = {
						promotionId: $("#masterDataArea #id").val().trim(),
						shopId: shopId
				};
				Utils.saveData(params, "/promotion/delete-shop", PromotionProgram._xhrSave, 'errMsgShop', function(data) {
					$("#successMsgShop").html("Lưu dữ liệu thành công").show();
					setTimeout(function() {$("#successMsgShop").hide();},3000);
					$("#exGrid").treegrid("reload");
				}, "loading2");
			}
		});
	},
	
	showShopDlg: function(shopId) {
		if (shopId == undefined || shopId == null) {
			shopId = 0;
		}
		$(".ErrorMsgStyle").hide();
		var html = '<div id="add-shopPopupDiv" style="display:none;">\
			<div id="add-shopPopup">\
			<div class="PopupContentMid">\
				<div class="GeneralForm Search1Form">\
					<label class="LabelStyle" style="width:100px;">Mã đơn vị</label>\
					<input id="codeDlg" class="InputTextStyle" style="width:150px;" maxlength="40" />\
					<label class="LabelStyle" style="width:100px;">Tên đơn vị</label>\
					<input id="nameDlg" class="InputTextStyle" style="width:230px;" maxlength="100" />\
					<div class="Clear"></div>\
					<div class="BtnCenterSection">\
					    <button class="BtnGeneralStyle" id="btnSearchDlg">Tìm kiếm</button>\
					</div>\
					\
					<div class="GridSection" id="gridDlgDiv">\
						<div id="gridDlg"></div>\
					</div>\
					\
					<p id="erMsgDlg" class="ErrorMsgStyle" style="padding-left:5px;display:none;"></p>\
					<div class="Clear"></div>\
					<div class="BtnCenterSection">\
					    <button class="BtnGeneralStyle" id="btnChooseDlg">Chọn</button>\
						<button class="BtnGeneralStyle" id="btnCloseDlg" onclick="javascript:$(\'#add-shopPopup\').dialog(\'close\');">Đóng</button>\
					</div>\
				</div>\
			</div>\
			</div>\
		</div>';
		$("body").append(html);
		Utils.functionAccessFillControl('add-shopPopupDiv', function(data){
			//Xu ly cac su kien lien quan den cac control phan quyen
		});
		$("#add-shopPopup").dialog({
			title: "Chọn đơn vị và Số suất KM",
			width: 800,
			heigth: "auto",
			modal: true, close: false, cache: false,
			onOpen: function() {
				$("#add-shopPopup").addClass("easyui-dialog");
				$("#add-shopPopup #codeDlg").focus();
				
				$("#add-shopPopup #gridDlg").treegrid({
					url: "/promotion/search-shop-dlg?promotionId="+$("#masterDataArea #id").val().trim()+"&shopId="+Number(shopId),
					rownumbers: false,
					width: $("#add-shopPopup #gridDlgDiv").width(),
					height: 350,
					fitColumns: true,
					idField: "nodeId",
					treeField: "text",
					selectOnCheck: false,
					checkOnSelect: false,
					columns: [[
						{field:"text", title:"Mã Đơn vị", sortable: false, resizable: false, width: 250, align:"left", formatter:CommonFormatter.formatNormalCell},
						{field:"quantity", title:"Số suất", sortable: false, resizable: false, width: 90, fixed:true, align:"left", formatter: function(v, r) {
							if (r.attr.isNPP) {
								if (r.attr.isExists) {
									return "<input type='text' id='txt-p"+r.attr.id+"' class='qttClass vinput-money' style='width:75px;' maxlength='11' disabled='disabled' exists='1' />";
								}
								return "<input type='text' id='txt-p"+r.attr.id+"' class='qttClass vinput-money' style='width:75px;' maxlength='11' exists='0' />";
							}
							return "";
						}},
						{field:"isEdit", title:"Sửa số suất", sortable: false, resizable: false, width: 90, fixed:true, align:"center", formatter: function(v, r) {
							if (r.attr.isNPP) {
								if (r.attr.isExists) {
									return "<input type='checkbox' id='edit-p"+r.attr.id+"' class='qttClass vinput-money' style='width:75px;' disabled='disabled' exists='1' />";
								}
								return "<input type='checkbox' id='edit-p"+r.attr.id+"' class='qttClass vinput-money' style='width:75px;' exists='0' />";
							}
							return "";
						}},
						{field:"amountMax", title:"Số tiền", sortable: false, resizable: false, width: 90, fixed:true, align:"left", formatter: function(v, r) {
							if (r.attr.isNPP) {
								if (r.attr.isExists) {
									return "<input type='text' id='txt-a"+r.attr.id+"' class='qttClass vinput-money' maxlength='11' style='width:75px;' disabled='disabled' exists='1' />";
								}
								return "<input type='text' id='txt-a"+r.attr.id+"' class='qttClass vinput-money' maxlength='11' style='width:75px;' exists='0' />";
							}
							return "";
						}},
						{field:"numMax", title:"Số lượng", sortable: false, resizable: false, width: 90, fixed:true, align:"left", formatter: function(v, r) {
							if (r.attr.isNPP) {
								if (r.attr.isExists) {
									return "<input type='text' id='txt-n"+r.attr.id+"' class='qttClass vinput-money' style='width:75px;' maxlength='11' disabled='disabled' exists='1' />";
								}
								return "<input type='text' id='txt-n"+r.attr.id+"' class='qttClass vinput-money' style='width:75px;' maxlength='11' exists='0' />";
							}
							return "";
						}},
						{field:"ck", title:"", sortable: false, resizable: false, width:35, fixed:true, align:"center", formatter: function(v, r) {
							var pId = r.attr.parentId;
							if (pId == undefined || pId == null) {
								pId = 0;
							}
							if (r.attr.isExists) {
								return "<input type='checkbox' id='ck"+r.nodeId+"' value='"+r.attr.id+"' disabled='disabled' exists='1' parentId='"+pId+"' onchange='PromotionProgram.onCheckShop(this);' />";
							}
							return "<input type='checkbox' id='ck"+r.nodeId+"' value='"+r.attr.id+"' exists='0' parentId='"+pId+"' onchange='PromotionProgram.onCheckShop(this);' />";
						}}
					]],
					onLoadSuccess: function(data) {
						$("#add-shopPopup #gridDlg").datagrid("resize");
//						Utils.bindFormatOnTextfieldInputCss('qttClass', Utils._TF_NUMBER, "#add-shopPopup");
//						$('#add-shopPopup .qttClass').each(function() {
//				      		VTUtilJS.formatCurrencyFor($(this).attr('id'));
//				      	});
						$('.vinput-money').each(function() {
							VTUtilJS.bindFormatOnTextfield($(this), VTUtilJS._TF_NUMBER_COMMA);
							VTUtilJS.formatCurrencyFor($(this));
						});
						setTimeout(function() {
							var hDlg = parseInt($("#add-shopPopup").parent().height());
							var hW = parseInt($(window).height());
							var d = hW - hDlg;
							d = d/2 + document.documentElement.scrollTop;
							if (d < 0) { d = 0; }
							$("#add-shopPopup").parent().css('top', d);
			    		}, 1000);
					}
				});
				
				$("#add-shopPopup #btnSearchDlg").click(function() {
					var p = {
							code: $("#add-shopPopup #codeDlg").val().trim(),
							name: $("#add-shopPopup #nameDlg").val().trim()
					};
					$("#add-shopPopup #gridDlg").treegrid("reload", p);
				});
				$("#add-shopPopup #codeDlg").parent().keyup(function(event) {
					if (event.keyCode == keyCodes.ENTER) {
						$("#add-shopPopup #btnSearchDlg").click();
					}
				});
				$("#add-shopPopup #btnChooseDlg").click(function() {
					if ($("#add-shopPopup input[id^=ck]:checked").length == 0) {
						$("#erMsgDlg").html("Không có đơn vị nào được chọn").show();
						return;
					}
					var lstTmp = new Map();
					var lstIsEdit = new Map();
					var lstTmpAmt = new Map();
					var lstTmpNum = new Map();
					var v, qtt, amt, num;
					var isUpdate = true;
					$("#add-shopPopup input[id^=ck]:checked").each(function() {
						v = $(this).val().trim();
						if ($("#txt-p"+v).length == 1) {
							qtt = $("#txt-p"+v).val().trim();
							isEdit = $("#edit-p"+v).is(':checked');
						} else {
							qtt = "";
							isEdit = false;
						}
						qtt = qtt.replace(/,/g, "");
						
						if (qtt.length > 0 && (isNaN(qtt) || Number(qtt) <= 0)) {
							$("#erMsgDlg").html("Số suất phải là số nguyên dương").show();
							lstTmp = null;
							isUpdate = false;
							return;
						}
						lstTmp.put(v, qtt);
						lstIsEdit.put(v, isEdit);
						
						//---Amount
						if ($("#txt-a"+v).length == 1) {
							amt = $("#txt-a"+v).val().trim();
						} else {
							amt = "";
						}
						amt = amt.replace(/,/g, "");
						
						if (amt.length > 0 && (isNaN(amt) || Number(amt) <= 0)) {
							$("#erMsgDlg").html("Số tiền phải là số nguyên dương").show();
							isUpdate = false;
							return;
						}
						lstTmpAmt.put(v, amt);
						
						//---Number
						if ($("#txt-n"+v).length == 1) {
							num = $("#txt-n"+v).val().trim();
						} else {
							num = "";
						}
						num = num.replace(/,/g, "");
						
						if (num.length > 0 && (isNaN(num) || Number(num) <= 0)) {
							$("#erMsgDlg").html("Số lượng phải là số nguyên dương").show();
							isUpdate = false;
							return;
						}
						lstTmpNum.put(v, num);
					});
					if(isUpdate) {
						PromotionProgram.addShopQtt(lstTmp, lstIsEdit, lstTmpAmt, lstTmpNum);						
					}
				});
			},
			onClose: function() {
				$("#add-shopPopup").dialog("destroy");
				$("#add-shopPopupDiv").remove();
			}
		});
	},
	
	onCheckShop: function(t) {
		var ck = $(t).prop("checked");
		if (ck) {
			var p = "#ck" + $(t).attr("parentId");
			PromotionProgram.removeParentShopCheck(p);
		} else {
			var p = "#ck" + $(t).attr("parentId");
			PromotionProgram.enableParentShopCheck(p);
		}
	},
	
	removeParentShopCheck: function(t) {
		if ($(t).length == 0) {
			return;
		}
		$(t).removeAttr("checked");
		$(t).attr("disabled", "disabled");
		var p = "#ck" + $(t).attr("parentId");
		PromotionProgram.removeParentShopCheck(p);
	},
	
	enableParentShopCheck: function(t) {
		if ($(t).length == 0 || $(t).attr("exists") == "1") {
			return;
		}
		$(t).removeAttr("disabled");
		var p = "#ck" + $(t).attr("parentId");
		PromotionProgram.enableParentShopCheck(p);
	},
	
	addShopQtt: function(lstShopTmp, lstIsEdit, lstTmpAmt, lstTmpNum) {
		$(".ErrorMsgStyle").hide();
		$.messager.confirm("Xác nhận", "Bạn có muốn thêm đơn vị?", function(r) {
			if (r) {
				var zezo = 0;
				var arrQttHL = [];
				var arrAmtHL = [];
				var arrNumHL = [];
				var arrIdHl = lstShopTmp.keyArray;
				for (var i = 0, size = arrIdHl.length; i < size; i++) {
					var valMap = lstShopTmp.get(arrIdHl[i]);
					if (valMap != undefined && valMap != null && valMap.length > 0) {
						arrQttHL.push(valMap);
					} else {
						arrQttHL.push(zezo);
					}
					valMap = lstTmpAmt.get(arrIdHl[i]);
					if (valMap != undefined && valMap != null && valMap.length > 0) {
						arrAmtHL.push(valMap);
					} else { 
						arrAmtHL.push(zezo);
					}
					valMap = lstTmpNum.get(arrIdHl[i]);
					if (valMap != undefined && valMap != null && valMap.length > 0) {
						arrNumHL.push(valMap);
					} else {
						arrNumHL.push(zezo);
					}
				}
				var params = {
						promotionId: $("#masterDataArea #id").val().trim(),
						lstId: arrIdHl,
						lstEdit: lstIsEdit.valArray,
						lstQtt: arrQttHL,
						lstAmt: arrAmtHL,
						lstNum: arrNumHL
				};
				$("#add-shopPopup").dialog("close");
				Utils.saveData(params, "/promotion/add-shop", PromotionProgram._xhrSave, 'errMsgShop', function(data) {
					$("#successMsgShop").html("Lưu dữ liệu thành công").show();
					setTimeout(function() {$("#successMsgShop").hide();},3000);
					$("#exGrid").treegrid("reload");
					$("#labelListCustomer").hide();
					$("#promotionCustomerGrid").hide();
					$("#boxSearch1").hide();
				}, "loading2");
			}
		});
	},
	
	exportPromotionShop: function() {
		$(".ErrorMsgStyle").hide();
		if (PromotionProgram._promotionShopParams == null) {
			PromotionProgram._promotionShopParams = {};
			PromotionProgram._promotionShopParams.promotionId = $("#masterDataArea #id").val().trim();
		}
		ReportUtils.exportReport("/promotion/export-promotion-shop", PromotionProgram._promotionShopParams, "errMsgShop");
	},
	
	importPromotionShop: function() {
		$('.ErrorMsgStyle').hide();
		VTUtilJS.importExcelUtils(function(data) {
			$("#exGrid").treegrid("reload");
			$("#labelListCustomer").hide();
			$("#promotionCustomerGrid").hide();
			$("#boxSearch1").hide();
			$("#errExcelMsgShop").html(data.message).show();
		}, "importFrm", "excelFile", null, "errExcelMsgShop");
	},
	
	checkSaleLevelCatIdInList: function(idSaleCatLevel,listData3){
		if(listData3!=null && listData3.length > 0){
			for(var j=0;j<listData3.length;++j){
				if(listData3[j].idSaleCatLevel == idSaleCatLevel){
					return true;
				}
			}
		}else{
			return false;
		}
	},
	loadAppliedAttributes: function(promotionId){
		$('.ErrorMsgStyle').hide();
		if(PromotionProgram._flagAjaxTabAttribute){
			PromotionProgram._flagAjaxTabAttribute = false;
			$.getJSON("/promotion/load-applied-attributes?promotionId="+promotionId, function(result){
				PromotionProgram._flagAjaxTabAttribute = true;
				var promotionStatus = $('#promotionStatus').val();
				var list = result.list;
				if (list != null) {
					for (var k = 0; k < list.length; k++){
						//đoạn này viết chung cho 1 attribute
						var objectType = list[k].objectType;
						var objectId = list[k].objectId;
						var name = list[k].name;
						name = Utils.XSSEncode(name);
						var html = '';
						//end đoạn viết chung.
						if (objectType == 2) {
							html+= '<div>';
							html+= '<div class="Clear"></div>';
							html+= '<li>';
							html+= '<input name="'  + name + '" objectType="'+ objectType +'" objectId="'+objectId +'" type="checkbox"  class="InputCbxStyle checkBoxAttribute"/>';
							html+= '<label class="LabelStyle Label4Style">'+name+'</label>';
							html+= '</li>';
							html+= '<div class="BoxSelect BoxSelect2">';
							html+= '<select id="customerType" style="width: 275px;" class="MySelectBoxClass " multiple="multiple">';
							var listData2 = list[k].listData;	
							var arrHtml = new Array();
							if (listData2 != null) {
								for (var i = 0; i < listData2.length; i++) {
									if (listData2[i].checked == true) {
										arrHtml.push('<option value="'+ listData2[i].idChannelType +'" selected="selected">'+Utils.XSSEncode(listData2[i].codeChannelType) +'-'+Utils.XSSEncode(listData2[i].nameChannelType) +'</option>');
									}else{
										arrHtml.push('<option value="'+ listData2[i].idChannelType +'">'+Utils.XSSEncode(listData2[i].codeChannelType) +'-'+Utils.XSSEncode(listData2[i].nameChannelType) +'</option>');
									}
								}
							}
							html+= arrHtml.join("");
							html+= '</select>';
							html+= '</div>';
							html+= '<div class="Clear"></div>';
							html+= '</div>';
							$('#right').append(html);
						} else if (objectType == 3) {
							var listData3 = list[k].listData;	
							var objectType = 3;
							var objectId = 0;
							var name = "Mức doanh số";
							var html = '';
							//đoạn này hình như là giống nhau trong các trường hợp của objectType:
							html+= '<div id="saleLevel">';//Nhớ đặt id chỗ này để phân biệt, Ví dụ phân biệt: combobox(select)của mức doanh số, của loại khách hàng. 
							//phân biệt checkbox của mức doanh số, hay checkbox của loại khách hàng. Sau này bên hàm loadAppliedAttribute mình cũng đặt id 
							//chỗ objectType=3 giống thế này mới chạy được.
							html+= '<div class="Clear"></div>';
							html+= 	'<li>';
							html+=	'<input name="'  + name + '" objectType="'+ objectType +'" objectId="'+objectId +'" type="checkbox"  class="InputCbxStyle checkBoxAttribute"/>';
							html+=	'<label class="LabelStyle Label4Style">'+name+'</label>';
							html+=	'</li>';
							//end đoạn giống nhau
							/*for qua cái list, add những label ngành hàng vào. Các lable Ngành hàng thì mình đặt chung 1 class "cat", để sau này mình lặp each 
							qua mỗi label, lấy catId(idProductInfoVO)của label đó, và lấy tất cả các mức mà người ta chọn, vậy cái combobox mức nên đặt id theo
							id của ngành hàng.*/
							var listProductInfoVO = list[k].listProductInfoVO;	
							if (listProductInfoVO != null) {
								for (var i = 0; i < listProductInfoVO.length; i++) {
									if (i == 0) {// if else để canh chỉnh html cho thẳng hàng đó mà.
										//label ngành hàng nên đặt 1 thuộc tính là catId
										html+= '<label catId="'+listProductInfoVO[i].idProductInfoVO +'" style="width: 150px;" class="LabelStyle Label5Style cat">'+Utils.XSSEncode(listProductInfoVO[i].codeProductInfoVO)+'</label>';
										//add combobox mức vao label nganh hang, combobox nên đặt id theo ngành hàng.
										html+= '<div class="BoxSelect BoxSelect2">';
										html+= '<select id="saleLevel-'+listProductInfoVO[i].idProductInfoVO +'" class="MySelectBoxClass saleLevelCat" multiple="multiple">';
										var listSaleLevelCat = listProductInfoVO[i].listSaleCatLevelVO;
										var arrHtml = new Array();
										if (listSaleLevelCat != null) {
											for(var t=0;t<listSaleLevelCat.length;++t){
												var checked = PromotionProgram.checkSaleLevelCatIdInList(listSaleLevelCat[t].idSaleCatLevel,listData3);
												if(checked == true){
													arrHtml.push('<option value="'+ listSaleLevelCat[t].idSaleCatLevel +'" selected="selected">'+ Utils.XSSEncode(listSaleLevelCat[t].codeSaleCatLevel) +'-'+Utils.XSSEncode(listSaleLevelCat[t].nameSaleCatLevel) +'</option>');
												}else{
													arrHtml.push('<option value="'+ listSaleLevelCat[t].idSaleCatLevel +'">'+ Utils.XSSEncode(listSaleLevelCat[t].codeSaleCatLevel) +'-'+Utils.XSSEncode(listSaleLevelCat[t].nameSaleCatLevel) +'</option>');
												}
											}
										}
										html+= arrHtml.join("");
										html+= '</select>';
										html+= '</div>';
									}else{
										html+= '<div class="Clear"></div>';
										html+= '<label catId="'+listProductInfoVO[i].idProductInfoVO +'" style="width: 150px;" class="LabelStyle Label6Style">'+Utils.XSSEncode(listProductInfoVO[i].codeProductInfoVO)+'</label>';
										html+= '<div class="BoxSelect BoxSelect2">';
										html+= '<select id="saleLevel-'+listProductInfoVO[i].idProductInfoVO +'" class="MySelectBoxClass saleLevelCat" multiple="multiple">';
										var listSaleLevelCat = listProductInfoVO[i].listSaleCatLevelVO;
										var arrHtml = new Array();
										if(listSaleLevelCat!=null){
											for(var t=0;t<listSaleLevelCat.length;++t){
												var checked = PromotionProgram.checkSaleLevelCatIdInList(listSaleLevelCat[t].idSaleCatLevel,listData3);
												if(checked == true){
													arrHtml.push('<option value="'+ listSaleLevelCat[t].idSaleCatLevel +'" selected="selected">'+ Utils.XSSEncode(listSaleLevelCat[t].codeSaleCatLevel) +'-'+Utils.XSSEncode(listSaleLevelCat[t].nameSaleCatLevel) +'</option>');
												}else{
													arrHtml.push('<option value="'+ listSaleLevelCat[t].idSaleCatLevel +'">'+ Utils.XSSEncode(listSaleLevelCat[t].codeSaleCatLevel) +'-'+Utils.XSSEncode(listSaleLevelCat[t].nameSaleCatLevel) +'</option>');
												}
											}
										}
										html+= arrHtml.join("");
										html+= '</select>';
										html+= '</div>';
									}
								}
							}
							html+= '<div class="Clear"></div>';
							html+= '</div>';
							$('#right').append(html);
						}else if(objectType == 1){
							var valueType = list[k].valueType;
							var listData1 = list[k].listData;
							
							html+= '<div>';
							html+= '<div class="Clear"></div>';
							html+= '<li>';
							//thằng input của thuộc tính động cần bỏ valueType vào để sau này xử lý khi lưu.
							html+= '<input valueType="'+valueType+'"  name="'  + name + '" objectType="'+ objectType +'" objectId="'+objectId +'" type="checkbox"  class="InputCbxStyle checkBoxAttribute"/>';
							html+= '<label class="LabelStyle Label4Style">'+name+'</label>';
							html+= '</li>';
								
							if(listData1!=null){
								if(valueType=="CHARACTER"){
									//Nên đặt id html của text CHARACTER này theo attributeId cộng chuỗi CHARACTER  thì sẽ không bị conflict trong danh sách. Sau này muốn lưu thì
									//select theo attributeId để lấy ra dữ liệu.
									html+= '<input id="'+objectId+'_CHARACTER" type="text" style="width: 377px;" value="'+listData1[0]+'" class="InputTextStyle InputText1Style"/>';
								}else if(valueType=="NUMBER"){
									//Đặt id theo: attributeId_NUMBER_from và attributeId_NUMBER_to:
									var from = '';
									var to = '';
									if(listData1[0]!=null){
										from = listData1[0];
									}
									if(listData1[1]!=null){
										to = listData1[1];
									}
									html+= '<input id="'+objectId+'_NUMBER_from" style="width: 170px;" type="text" value="'+formatCurrency(from)+'" class="InputTextStyle InputText5Style"/>';
									html+= '<label class="LabelStyle Label7Style">-</label>';
									html+= '<input id="'+objectId+'_NUMBER_to" style="width: 170px;" type="text" value="'+formatCurrency(to)+'" class="InputTextStyle InputText5Style"/>';
								}else if(valueType=="DATE_TIME"){
									//Đặt id theo: attributeId_DATETIME_from và attributeId_DATETIME_to. Nhớ applyDateTimePicker applyDateTimePicker("#endDate");
									//Mình đặt chung bọn chúng 1 class rồi,applyDateTimePicker cho class đó được không nhỉ. Tí thử. 
									var from = '';
									var to = '';
									if(listData1[0]!=null){
										from = listData1[0];
									}
									if(listData1[1]!=null){
										to = listData1[1];
									}
									html+= '<input id="'+objectId+'_DATETIME_from"  style="width: 146px;"  type="text" value="'+from+'" class="InputTextStyle InputText6Style"/>';
									html+= '<label class="LabelStyle Label7Style">-</label>';
									html+= '<input id="'+objectId+'_DATETIME_to" style="width: 146px;"  type="text" value="'+to+'" class="InputTextStyle InputText6Style"/>';
								}else if(valueType=="CHOICE" || valueType=="MULTI_CHOICE"){
									var arrHtml = new Array();
									html+= '<div class="BoxSelect BoxSelect2">';
									html+= '<select id="'+objectId+'" class="MySelectBoxClass" multiple="multiple">';
									for(var i=0;i<listData1.length;++i){
										if(listData1[i].checked == true){
											arrHtml.push('<option value="'+ listData1[i].enumId +'" selected="selected">'+Utils.XSSEncode(listData1[i].code)+'-'+Utils.XSSEncode(listData1[i].name) +'</option>');
										}else{
											arrHtml.push('<option value="'+ listData1[i].enumId +'">'+Utils.XSSEncode(listData1[i].code)+'-'+Utils.XSSEncode(listData1[i].name) +'</option>');
										}
									}
									html+= arrHtml.join("");
									html+= '</select>';
									html+= '</div>';
								}
							}
							html+= '<div class="Clear"></div>';
							html+= '</div>';
							$('#right').append(html);
							//dự thảo thì ko disable
							if(valueType=="DATE_TIME"){
								VTUtilJS.applyDateTimePicker(objectId+'_DATETIME_from');
								VTUtilJS.applyDateTimePicker(objectId+'_DATETIME_to');
							}else if(valueType=="NUMBER"){
								Utils.bindFormatOnTextfield(objectId+'_NUMBER_from',Utils._TF_NUMBER);
								Utils.formatCurrencyFor(objectId+'_NUMBER_from');
								Utils.bindFormatOnTextfield(objectId+'_NUMBER_to',Utils._TF_NUMBER);
								Utils.formatCurrencyFor(objectId+'_NUMBER_to');
							}
						}
					}
					//Xong for:
					$('#right .MySelectBoxClass').each(function(){
						var aCombobox = $(this);
						if(!(aCombobox.next().hasClass('ui-dropdownchecklist'))){//Cái này để kiểm tra comboBox đó đã gọi dropdownchecklist lần nào chưa.
							//Nếu nó đã gọi dropdownchecklist 1 lần rồi, thì thể nào cũng gien ra 1 element html có class ui-dropdownchecklist nằm kế bên comboBox.
							aCombobox.dropdownchecklist({ maxDropHeight: 150,width: 277, textFormatFunction: function(options){
								var text = '';
								$('.ui-dropdownchecklist-dropcontainer input[type=checkbox]').each(function(){
									var aCheckBox = $(this);
									if(aCheckBox.parent().parent().parent().prev().prev().attr('id') == aCombobox.attr('id')){//Tức là chỉ xử lý với những checkbox thuộc combobox đó.
										if(aCheckBox.attr('checked')=='checked'){
											text += aCheckBox.next().text().split('-')[0] + ', ';
										}
									}
								});
								if(text != ''){
									text = text.substring(0, text.length-2);//bo dau ', ' cuoi cung.
								}
								return text;
							} });
						}
					});
				}
				//ko dự thảo thì disable.
				if(promotionStatus != 2 /*|| $('#permissionUser').val() == 'false'*/){
					$("#right .MySelectBoxClass").dropdownchecklist("disable");
					$("#right :input").attr('disabled',true);
					$("#left :input").attr('disabled',true);
					$('#group_change_updateCustomerAttribute').hide();
					$('#right .CalendarLink').hide();
				}
			});	
		}
	},
	
	selectAttributes: function() {
		$('.ErrorMsgStyle').hide();
		var listObjectType= new Array();
		var listObjectId= new Array();
		$('#left :checkbox').each(function(){	
			if($(this).is(':checked')){
				//remove ben left, show ben right
				$(this).parent().remove(); 
				var objectType = $(this).attr('objectType');
				var objectId = $(this).attr('objectId');
				listObjectType.push(objectType);
				if(objectType == 2 || objectType == 3){
					listObjectId.push(0);
				}else{
					listObjectId.push(objectId);
				}
			}
		});	
		var data = new Object();
		data.lstObjectType = listObjectType;
		data.lstId = listObjectId;
		var kData = $.param(data, true);
		$.getJSON("/promotion/get-all-data-attributes",kData,function(result){
			var listOut = result.list;	
			if(listOut!=null){
				for(var h=0;h<listOut.length;++h){
					var objectType = listOut[h].objectType;
					var objectId = listOut[h].objectId;
					var valueType = listOut[h].valueType;
					var html = '';
					if(objectType == 2){
						var name = 'Loại khách hàng';
						html+= '<div>';
						html+= '<div class="Clear"></div>';
						html+= '<li>';
						html+= '<input name="'  + name + '" objectType="'+ objectType +'" objectId="'+objectId +'" type="checkbox"  class="InputCbxStyle checkBoxAttribute"/>';
						html+= '<label class="LabelStyle Label4Style">'+Utils.XSSEncode(name)+'</label>';
						html+= '</li>';
						html+= '<div class="BoxSelect BoxSelect2">';
						html+= '<select id="customerType" style="width: 275px;" class="MySelectBoxClass" multiple="multiple">';
						var list = listOut[h].listData;	
						var arrHtml = new Array();
						if(list!=null){
							for(var i=0;i<list.length;++i){
								arrHtml.push('<option value="'+ list[i].idChannelType +'">'+ Utils.XSSEncode(list[i].codeChannelType)+ '-' +Utils.XSSEncode(list[i].nameChannelType)+ '</option>');
							}
						}
						html+= arrHtml.join("");
						html+= '</select>';
						html+= '</div>';
						html+= '<div class="Clear"></div>';
						html+= '</div>';
						$('#right').append(html);
					}else if(objectType == 3){
						var name = 'Mức doanh số';
						html+= '<div id="saleLevel">';
						html+= '<div class="Clear"></div>';
						html+= 	'<li>';
						html+=	'<input name="'  + name + '" objectType="'+ objectType +'" objectId="'+objectId +'" type="checkbox"  class="InputCbxStyle checkBoxAttribute"/>';
						html+=	'<label class="LabelStyle Label4Style">'+Utils.XSSEncode(name)+'</label>';
						html+=	'</li>';
						var list = listOut[h].listProductInfoVO;//result.list;	
						if(list!=null){
							for(var i=0;i<list.length;++i){
								if(i==0){// if else để canh chỉnh html cho thẳng hàng đó mà.
									//label ngành hàng nên đặt 1 thuộc tính là catId
									html+= '<label catId="'+list[i].idProductInfoVO +'" style="width: 150px;" class="LabelStyle Label5Style cat">'+Utils.XSSEncode(list[i].codeProductInfoVO)+'</label>';
									//add combobox mức vao label nganh hang, combobox nên đặt id theo ngành hàng.
									html+= '<div class="BoxSelect BoxSelect2">';
									html+= '<select id="saleLevel-'+list[i].idProductInfoVO +'" class="MySelectBoxClass saleLevelCat" multiple="multiple">';
									var listSaleLevelCat = list[i].listSaleCatLevelVO;
									var arrHtml = new Array();
									if(listSaleLevelCat!=null){
										for(var k=0;k<listSaleLevelCat.length;++k){
											arrHtml.push('<option value="'+ listSaleLevelCat[k].idSaleCatLevel +'">'+ Utils.XSSEncode(listSaleLevelCat[k].codeSaleCatLevel)+'-'+Utils.XSSEncode(listSaleLevelCat[k].nameSaleCatLevel) +'</option>');
										}
									}
									html+= arrHtml.join("");
									html+= '</select>';
									html+= '</div>';
								}else{
									html+= '<div class="Clear"></div>';
									html+= '<label catId="'+list[i].idProductInfoVO +'" style="width: 150px;" class="LabelStyle Label6Style" >'+Utils.XSSEncode(list[i].codeProductInfoVO)+'</label>';
									//add combobox mức vao label nganh hang, combobox nên đặt id theo ngành hàng.
									html+= '<div class="BoxSelect BoxSelect2">';
									html+= '<select id="saleLevel-'+list[i].idProductInfoVO +'" class="MySelectBoxClass saleLevelCat" multiple="multiple">';
									var listSaleLevelCat = list[i].listSaleCatLevelVO;
									var arrHtml = new Array();
									if(listSaleLevelCat!=null){
										for(var k=0;k<listSaleLevelCat.length;++k){
											arrHtml.push('<option value="'+ listSaleLevelCat[k].idSaleCatLevel +'">'+Utils.XSSEncode(listSaleLevelCat[k].codeSaleCatLevel)+'-'+Utils.XSSEncode(listSaleLevelCat[k].nameSaleCatLevel) +'</option>');
										}
									}
									html+= arrHtml.join("");
									html+= '</select>';
									html+= '</div>';
								}
							}
						}
						html+= '<div class="Clear"></div>';
						html+= '</div>';
						$('#right').append(html);
					}else if(objectType == 1){
						var name = listOut[h].name;
						name = Utils.XSSEncode(name);
						html+= '<div>';
						html+= '<div class="Clear"></div>';
						html+= 	'<li>';
						//thằng input của thuộc tính động cần bỏ valueType vào để sau này xử lý khi lưu.
						html+=	'<input valueType="'+valueType+'" name="'  + Utils.XSSEncode(name) + '" objectType="'+ objectType +'" objectId="'+objectId +'" type="checkbox"  class="InputCbxStyle checkBoxAttribute"/>';
						html+=	'<label class="LabelStyle Label4Style">'+Utils.XSSEncode(name)+'</label>';
						html+=  '</li>';
						//Chia các trường hợp theo valueType của attributeId
						if(valueType=="CHARACTER"){
							//Nên đặt id html của text CHARACTER này theo attributeId cộng chuỗi CHARACTER  thì sẽ không bị conflict trong danh sách. Sau này muốn lưu thì
							//select theo attributeId để lấy ra dữ liệu.
							html+= '<input id="'+objectId+'_CHARACTER" type="text" style="width: 377px;" class="InputTextStyle InputText1Style"/>';
						}else if(valueType=="NUMBER"){
							html+= '<input id="'+objectId+'_NUMBER_from" type="text" style="width: 170px;" class="InputTextStyle InputText5Style"/>';
							html+= '<label class="LabelStyle Label7Style">-</label>';
							html+= '<input id="'+objectId+'_NUMBER_to" type="text" style="width: 170px;" class="InputTextStyle InputText5Style"/>';
						}else if(valueType=="DATE_TIME"){
							html+= '<input id="'+objectId+'_DATETIME_from" style="width: 146px;" type="text" class="InputTextStyle InputText6Style "/>';
							html+= '<label class="LabelStyle Label7Style">-</label>';
							html+= '<input id="'+objectId+'_DATETIME_to" style="width: 146px;" type="text" class="InputTextStyle InputText6Style "/>';
						}else if(valueType=="CHOICE" || valueType=="MULTI_CHOICE"){
							var list = listOut[h].listData;	
							var arrHtml = new Array();
							html+= '<div class="BoxSelect BoxSelect2">';
							html+= '<select id="'+objectId+'" class="MySelectBoxClass" multiple="multiple">';
							if(list!=null){
								for(var i=0;i<list.length;++i){
									arrHtml.push('<option value="'+ list[i].enumId +'">'+Utils.XSSEncode(list[i].code)+'-'+Utils.XSSEncode(list[i].name) +'</option>');
								}
							}
							html+= arrHtml.join("");
							html+= '</select>';
							html+= '</div>';
						}
						html+= '<div class="Clear"></div>';
						html+= '</div>';
						$('#right').append(html);
						if(valueType=="DATE_TIME"){
							VTUtilJS.applyDateTimePicker(objectId+'_DATETIME_from');
							VTUtilJS.applyDateTimePicker(objectId+'_DATETIME_to');
						}else if(valueType=="NUMBER"){
							Utils.bindFormatOnTextfield(objectId+'_NUMBER_from',Utils._TF_NUMBER);
							Utils.formatCurrencyFor(objectId+'_NUMBER_from');
							Utils.bindFormatOnTextfield(objectId+'_NUMBER_to',Utils._TF_NUMBER);
							Utils.formatCurrencyFor(objectId+'_NUMBER_to');
						}
					}
				}
				//Xong for:
				$('#right .MySelectBoxClass').each(function(){
					var aCombobox = $(this);
					if(!(aCombobox.next().hasClass('ui-dropdownchecklist'))){//Cái này để kiểm tra comboBox đó đã gọi dropdownchecklist lần nào chưa.
						//Nếu nó đã gọi dropdownchecklist 1 lần rồi, thì thể nào cũng gien ra 1 element html có class ui-dropdownchecklist nằm kế bên comboBox.
						aCombobox.dropdownchecklist({ maxDropHeight: 150,width: 277, textFormatFunction: function(options){
							var text = '';
							$('.ui-dropdownchecklist-dropcontainer input[type=checkbox]').each(function(){
								var aCheckBox = $(this);
								if(aCheckBox.parent().parent().parent().prev().prev().attr('id') == aCombobox.attr('id')){//Tức là chỉ xử lý với những checkbox thuộc combobox đó.
									if(aCheckBox.attr('checked')=='checked'){
										text += aCheckBox.next().text().split('-')[0] + ', ';
									}
								}
							});
							if(text != ''){
								text = text.substring(0, text.length-2);//bo dau ', ' cuoi cung.
							}
							return text;
						} });
					}
				});
				
			}
		});	
	},
	removeAttributes: function(){
		$('.ErrorMsgStyle').hide();
		var html = '';//để bên ngoài vòng for là đúng. Còn bên hàm toSelectAttribute thì phải để bên trong do cuộc gọi bất đồng bộ
		$('#right .checkBoxAttribute').each(function(){
			if($(this).is(':checked')){
				//remove ben right, show ben left
				$(this).parent().parent().remove(); 
				var objectType = $(this).attr('objectType');
				var objectId = $(this).attr('objectId');
				var name = $(this).attr('name');
				html+= '<li> <input name="'  + Utils.XSSEncode(name) + '" objectType="'+ objectType +'" objectId="'+objectId +'" type="checkbox"  class="InputCbxStyle"/><label style="width:150px" class="LabelStyle Label3Style">'+Utils.XSSEncode(name)+'</label></li>';
			}
		});	
		$('#left').append(html);//để bên ngoài vòng for là đúng. Còn bên hàm toSelectAttribute thì phải để bên trong do cuộc gọi bất đồng bộ
	},
	
	saveCustomerAttributes: function(){
		var message = '';
		var messageSaleLevel = '';
		var messageCustomerType = '';
		var messageOther = '';
		$('.ErrorMsgStyle').hide();
		var data = new Object();
		var listCustomerType= new Array();
		var listSaleLevelCatId= new Array();
		var listAttribute= new Array();
		var listAttributeDataInField=new Array();
		var nameAttr = '';
		$('.checkBoxAttribute').each(function(){
			var objectType = $(this).attr('objectType');
			var objectId = $(this).attr('objectId');
			var name = $(this).attr('name');
			
			if(objectType == 2){
				listAttribute.push(-2);//Loai kh cho id = -2 di. De ti nua if else tren action.
				listAttributeDataInField.push('CustomerType');
				messageCustomerType = 'noCheckCustomerType';
				listCustomerType= new Array();
				$('#ddcl-customerType-ddw input[type=checkbox]').each(function(){
					if($(this).attr('checked')=='checked'){
						listCustomerType.push($(this).val());
						messageCustomerType = 'haveCheckCustomerType';
					}
				});
			}else if(objectType == 3){ 
				listAttribute.push(-3);//Muc doanh so cho id = -3 di. De ti nua if else tren action.
				listAttributeDataInField.push('SaleLevel');
				messageSaleLevel = 'noCheckSaleLevel';
				listSaleLevelCatId= new Array();
				$('#saleLevel input[type=checkbox]').each(function(){
					if($(this).attr('checked')=='checked' && !$(this).hasClass('checkBoxAttribute')){
						listSaleLevelCatId.push(this.value);
//						listSaleLevelCatId.push($(this).attr('objectid'));
						//vào được đây tức là mức doanh số có ít nhất 1 combox được check.
						messageSaleLevel = 'haveCheckSaleLevel';
					}
				});
			}else if(objectType == 1){ 
				listAttribute.push(objectId);
				var valueType = $(this).attr('valueType');
				if(valueType=="CHARACTER" || valueType=="NUMBER" || valueType=="DATE_TIME"){
					var boolean = PromotionProgram.getTrue_DataOrFalse_MessageOfAutoAttribute(objectId, name, valueType);
					var arr = boolean.split('__');
					if(arr[0]=='false'){
						message = arr[1];
						return false;
					}else{
						listAttributeDataInField.push(arr[1]);
					}
				}else if(valueType=="CHOICE" || valueType=="MULTI_CHOICE"){
					messageOther = 'noCheckOther';
					var dataOfAutoAttribute = '';
					$('#ddcl-'+objectId+'-ddw input[type=checkbox]').each(function(){
						if($(this).attr('checked')=='checked'){
							dataOfAutoAttribute += $(this).val() +',';//Nhớ chú ý dấu phẩy cuối cùng
							messageOther = 'haveCheckOther';
						}
					});
					nameAttr = name;
					listAttributeDataInField.push(dataOfAutoAttribute);
				}
			}
			if(messageSaleLevel == 'noCheckSaleLevel'){
				message = 'Bạn chưa chọn giá trị cho thuộc tính Mức doanh số';
			}
			if(messageCustomerType == 'noCheckCustomerType'){
				message = 'Bạn chưa chọn giá trị cho thuộc tính Loại khách hàng';
			}
			if(messageOther == 'noCheckOther'){
				message = 'Bạn chưa chọn giá trị cho thuộc tính '+nameAttr;
			}
		});
		if(message.length > 0){
			$('#errMsgSave').html(message).show();
			return false;
		}
		data.lstCustomerType = listCustomerType;
		data.lstSaleLevelCatId = listSaleLevelCatId;
		data.lstId = listAttribute;
		data.lstAttDataInField = listAttributeDataInField;
		data.promotionId=$('#masterDataArea #id').val().trim();
		Utils.addOrSaveData(data, '/promotion/save-promotion-customer-attribute', PromotionProgram._xhrSave,
				'errMsgSave', null, 'loading2', '#customerAttributeMsg ');//Prefix để selector jquery mà truyền thiếu "khoảng trắng" 
		//là có thể lỗi, không xuất ra câu thông báo lỗi, hay câu thông báo thành công đâu.
		return false;
	},
	getTrue_DataOrFalse_MessageOfAutoAttribute: function(objectId, nameAttribute,valueType){
		if(valueType=="CHARACTER"){
			var msg = '';
			var dataOfAutoAttribute = $('#'+objectId+'_CHARACTER').val();
			if(dataOfAutoAttribute==''){
				msg = 'Bạn chưa nhập giá trị cho thuộc tính '+nameAttribute;
				$('#'+objectId+'_CHARACTER').focus();
				return 'false__'+msg;
			}
			msg = Utils.getMessageOfSpecialCharactersValidate(objectId+'_CHARACTER','',Utils._NAME);
			if(msg.length > 0){
				msg = 'Thuộc tính '+nameAttribute+': Giá trị nhập vào không được chứa các ký tự đặc biệt [<>\~#&$%@*()^`\'"'; 
				return 'false__'+msg;
			}
			return 'true__'+dataOfAutoAttribute;
		}else if(valueType=="NUMBER"){
			var msg = '';
			var NUMBER_from = $('#'+objectId+'_NUMBER_from').val();
			var NUMBER_to = $('#'+objectId+'_NUMBER_to').val();
			if (NUMBER_from == '' && NUMBER_to == ''){
				msg = 'Bạn chưa nhập giá trị cho thuộc tính '+nameAttribute;
				$('#'+objectId+'_NUMBER_from').focus();
				return 'false__'+msg;
			}
			if (NUMBER_from != ''){
				NUMBER_from = parseInt(Utils.returnMoneyValue(NUMBER_from.trim()));
			}
			if (NUMBER_to != ''){
				NUMBER_to = parseInt(Utils.returnMoneyValue(NUMBER_to.trim()));
			}
			var dataOfAutoAttribute = NUMBER_from +','+NUMBER_to;//Nếu ở trên không cắt dấu phẩy đi thì sẽ ảnh hưởng!
			
			if(NUMBER_from!=='' &&  NUMBER_to!=='' &&  NUMBER_from > NUMBER_to){
				msg = 'Thuộc tính '+Utils.XSSEncode(nameAttribute)+': Giá trị "Từ" lớn hơn giá trị "Đến"';
				return 'false__'+msg;
			}
			return 'true__'+dataOfAutoAttribute;
		}else if(valueType=="DATE_TIME"){
			var msg = '';
			var DATETIME_from = $('#'+objectId+'_DATETIME_from').val();
			var DATETIME_to = $('#'+objectId+'_DATETIME_to').val();
			var dataOfAutoAttribute = DATETIME_from +','+DATETIME_to;
			if(DATETIME_from == '' && DATETIME_to == ''){
				msg = 'Bạn chưa nhập giá trị cho thuộc tính '+ Utils.XSSEncode(nameAttribute);
				$('#'+objectId+'_DATETIME_from').focus();
				return 'false__'+msg;
			}
			msg = Utils.getMessageOfInvalidFormatDate(objectId+'_DATETIME_from');
			if(msg.length > 0){
				msg = 'Thuộc tính ' + Utils.XSSEncode(nameAttribute) + ': Giá trị "Từ" không tồn tại hoặc không đúng định dạng dd/mm/yyyy.'; 
				return 'false__'+msg;
			}
			msg = Utils.getMessageOfInvalidFormatDate(objectId+'_DATETIME_to');
			if(msg.length > 0){
				msg = 'Thuộc tính ' + Utils.XSSEncode(nameAttribute) + ': Giá trị "Đến" không tồn tại hoặc không đúng định dạng dd/mm/yyyy.'; 
				return 'false__' + msg;
			}
			if(!Utils.compareDate(DATETIME_from, DATETIME_to)){
				msg = msgErr_fromdate_greater_todate;
				msg = 'Thuộc tính ' + Utils.XSSEncode(nameAttribute) + ': '+ msg;
				return 'false__' + msg;
			}
			return 'true__'+dataOfAutoAttribute;
		}
	},
	
	updateSalerQuantity: function(nodeId, mapId) {
		$(".ErrorMsgStyle").hide();
		var qtt = $("#txt"+nodeId).val().trim();
		qtt = qtt.replace(/,/g, "");
		if (qtt.length > 0 && (isNaN(qtt) || Number(qtt) <= 0)) {
			$("#errMsgShop").html("Số suất đơn vị phải là số dương").show();
			return;
		}
		var amt = $("#txtamt"+nodeId).val().trim();
		amt = amt.replace(/,/g, "");
		if (amt.length > 0 && (isNaN(amt) || Number(amt) <= 0)) {
			$("#errMsgShop").html("Số tiền phải là số dương").show();
			return;
		}
		var num = $("#txtnum"+nodeId).val().trim();
		num = num.replace(/,/g, "");
		if (num.length > 0 && (isNaN(num) || Number(num) <= 0)) {
			$("#errMsgShop").html("Số lượng phải là số dương").show();
			return;
		}
		$.messager.confirm("Xác nhận", "Bạn có muốn cập nhật số suất, số tiền, số lượng cho đơn vị?", function(r) {
			if (r) {
				var params = {
						promotionId: $("#masterDataArea #id").val().trim(),
						//shopId: shopId,
						id: mapId,
						quantity: qtt.length == 0 ? qtt : parseInt(qtt),
						amount: amt.length == 0 ? amt : parseInt(amt),
						number: num.length == 0 ? num : parseInt(num)
				};
				Utils.saveData(params, "/promotion/update-saler-quantity", PromotionProgram._xhrSave, 'errMsgShop', function(data) {
					$("#successMsgShop").html("Lưu dữ liệu thành công").show();
					setTimeout(function() {$("#successMsgShop").hide();},3000);
					$("#exGrid").treegrid("reload");
				}, "loading2");
			}
		});
	},
	
	deleteStaffMap: function(nodeId) {
		$(".ErrorMsgStyle").hide();
		var row = $("#exGrid").treegrid("find", nodeId);
		$.messager.confirm("Xác nhận", "Bạn có muốn xóa số suất NVBH?", function(r) {
			if (r) {
				var params = {
						promotionId: $("#masterDataArea #id").val().trim()
				};
				if (row.attr.isSaler == 1) {
					params.id = row.attr.mapId;
				} else {
					params.shopId = nodeId.replace("sh", "");
				}
				Utils.saveData(params, "/promotion/delete-staff-map", PromotionProgram._xhrSave, 'errMsgShop', function(data) {
					$("#successMsgShop").html("Lưu dữ liệu thành công").show();
					setTimeout(function() {$("#successMsgShop").hide();},3000);
					$("#exGrid").treegrid("reload");
				}, "loading2");
			}
		});
	},
	
	showSalerDlg: function(shopId) {
		if (shopId == undefined || shopId == null) {
			shopId = 0;
		}
		$(".ErrorMsgStyle").hide();
		var html = '<div id="add-shopPopupDiv" style="display:none;">\
			<div id="add-shopPopup">\
			<div class="PopupContentMid">\
				<div class="GeneralForm Search1Form">\
					<label class="LabelStyle" style="width:100px;">Mã NVBH</label>\
					<input id="codeDlg" class="InputTextStyle" style="width:150px;" maxlength="40" />\
					<label class="LabelStyle" style="width:100px;">Tên NVBH</label>\
					<input id="nameDlg" class="InputTextStyle" style="width:230px;" maxlength="100" />\
					<div class="Clear"></div>\
					<div class="BtnCenterSection">\
					    <button class="BtnGeneralStyle" id="btnSearchDlg">Tìm kiếm</button>\
					</div>\
					\
					<div class="GridSection" id="gridDlgDiv">\
						<div id="gridDlg"></div>\
					</div>\
					\
					<p id="erMsgDlg" class="ErrorMsgStyle" style="padding-left:5px;display:none;"></p>\
					<div class="Clear"></div>\
					<div class="BtnCenterSection">\
					    <button class="BtnGeneralStyle" id="btnChooseDlg">Chọn</button>\
						<button class="BtnGeneralStyle" id="btnCloseDlg" onclick="javascript:$(\'#add-shopPopup\').dialog(\'close\');">Đóng</button>\
					</div>\
				</div>\
			</div>\
			</div>\
		</div>';
		$("body").append(html);
		
		$("#add-shopPopup").dialog({
			title: "Chọn NVBH và Số suất KM",
			width: 750,
			heigth: "auto",
			modal: true, close: false, cache: false,
			onOpen: function() {
				$("#add-shopPopup").addClass("easyui-dialog");
				$("#add-shopPopup #codeDlg").focus();
				
				$("#add-shopPopup #gridDlg").treegrid({
					url: "/promotion/search-saler-dlg?promotionId="+$("#masterDataArea #id").val().trim()+"&shopId="+Number(shopId),
					rownumbers: false,
					width: $("#add-shopPopup #gridDlgDiv").width(),
					height: 350,
					fitColumns: true,
					idField: "nodeId",
					treeField: "text",
					selectOnCheck: false,
					checkOnSelect: false,
					columns: [[
						{field:"text", title:"Mã Đơn vị", sortable: false, resizable: false, width: 250, align:"left", formatter:CommonFormatter.formatNormalCell},
						{field:"quantity", title:"Số suất", sortable: false, resizable: false, width: 90, fixed:true, align:"left", formatter: function(v, r) {
							if (r.attr.isSaler) {
								/*if (r.attr.isExists) {
									return "<input type='text' id='txt-p"+r.attr.id+"' class='qttClass vinput-money' maxlength='11' disabled='disabled' exists='1' />";
								}*/
								return "<input type='text' id='txt-p"+r.attr.id+"' class='qttClass vinput-money' style='width:75px;' maxlength='11' exists='0' />";
							}
							return "";
						}},
						{field:"amountMax", title:"Số tiền", sortable: false, resizable: false, width: 90, fixed:true, align:"left", formatter: function(v, r) {
							if (r.attr.isSaler) {
								return "<input type='text' id='txt-a"+r.attr.id+"' class='qttClass vinput-money' style='width:75px;' maxlength='11' exists='0' />";
							}
							return "";
						}},
						{field:"numMax", title:"Số lượng", sortable: false, resizable: false, width: 90, fixed:true, align:"left", formatter: function(v, r) {
							if (r.attr.isSaler) {
								return "<input type='text' id='txt-n"+r.attr.id+"' class='qttClass vinput-money' style='width:75px;' maxlength='11' exists='0' />";
							}
							return "";
						}},
						{field:"ck", title:"", sortable: false, resizable: false, width: 30, fixed:true, align:"center", formatter: function(v, r) {
							var pId = r.attr.parentId;
							if (pId == undefined || pId == null) {
								pId = 0;
							}
							/*if (r.attr.isExists) {
								return "<input type='checkbox' id='ck"+r.nodeId+"' value='"+r.attr.id+"' disabled='disabled' exists='1' parentId='"+pId+"' onchange='PromotionProgram.onCheckShop(this);' />";
							}*/
							return "<input type='checkbox' id='ck"+r.nodeId+"' value='"+r.attr.id+"' exists='0' parentId='"+pId+"' onchange='PromotionProgram.onCheckShop(this);' />";
						}}
					]],
					onLoadSuccess: function(data) {
						$("#add-shopPopup #gridDlg").datagrid("resize");
//						Utils.bindFormatOnTextfieldInputCss('qttClass', Utils._TF_NUMBER, "#add-shopPopup");
//						$('#add-shopPopup .qttClass').each(function() {
//				      		VTUtilJS.formatCurrencyFor($(this).attr('id'));
//				      	});
						$('.vinput-money').each(function() {
							VTUtilJS.bindFormatOnTextfield($(this), VTUtilJS._TF_NUMBER_COMMA);
							VTUtilJS.formatCurrencyFor($(this));
						});
						setTimeout(function(){
							var hDlg=parseInt($("#add-shopPopup").parent().height());
							var hW=parseInt($(window).height());
							var d=hW-hDlg;
							d=d/2+document.documentElement.scrollTop;
							if (d < 0) { d = 0; }
							$("#add-shopPopup").parent().css('top',d);
			    		},1000);
					},
					onBeforeLoad: function(r, p) {
						if (p && p.id) {
							p.id = p.id.replace("sh", "");
						}
					}
				});
				
				$("#add-shopPopup #btnSearchDlg").click(function() {
					var p = {
							code: $("#add-shopPopup #codeDlg").val().trim(),
							name: $("#add-shopPopup #nameDlg").val().trim()
					};
					$("#add-shopPopup #gridDlg").treegrid("reload", p);
				});
				$("#add-shopPopup #codeDlg").parent().keyup(function(event) {
					if (event.keyCode == keyCodes.ENTER) {
						$("#add-shopPopup #btnSearchDlg").click();
					}
				});
				$("#add-shopPopup #btnChooseDlg").click(function() {
					if ($("#add-shopPopup input[id^=ckst]:checked").length == 0) {
						$("#erMsgDlg").html("Không có nhân viên nào được chọn").show();
						return;
					}
					var lstTmp = new Map();
					var lstTmpAmt = new Map();
					var lstTmpNum = new Map();
					var v, qtt, amt, num;
					var isUpdate = true;
					$("#add-shopPopup input[id^=ckst]:checked").each(function() {
						v = $(this).val().trim();
						if ($("#txt-p"+v).length == 1) {
							qtt = $("#txt-p"+v).val().trim();
						} else {
							qtt = "";
						}
						qtt = qtt.replace(/,/g, "");
						if (qtt.length > 0 && (isNaN(qtt) || Number(qtt) <= 0)) {
							$("#erMsgDlg").html("Số suất phải là số nguyên dương").show();
							lstTmp = null;
							isUpdate = false;
							return;
						}
						v = v.replace("st", "");
						lstTmp.put(v, qtt);
						
						//---Amount
						if ($("#txt-a"+v).length == 1) {
							amt = $("#txt-a"+v).val().trim();
						} else {
							amt = "";
						}
						amt = amt.replace(/,/g, "");
						
						if (amt.length > 0 && (isNaN(amt) || Number(amt) <= 0)) {
							$("#erMsgDlg").html("Số tiền phải là số nguyên dương").show();
							isUpdate = false;
							return;
						}
						lstTmpAmt.put(v, amt);
						
						//---Number
						if ($("#txt-n"+v).length == 1) {
							num = $("#txt-n"+v).val().trim();
						} else {
							num = "";
						}
						num = num.replace(/,/g, "");
						if (num.length > 0 && (isNaN(num) || Number(num) <= 0)) {
							$("#erMsgDlg").html("Số lượng phải là số nguyên dương").show();
							isUpdate = false;
							return;
						}
						lstTmpNum.put(v, num);
					});
					if (isUpdate) {
						PromotionProgram.addSalerQtt(lstTmp, lstTmpAmt, lstTmpNum);						
					}
				});
			},
			onClose: function() {
				$("#add-shopPopup").dialog("destroy");
				$("#add-shopPopupDiv").remove();
			}
		});
	},
	
	addSalerQtt: function(lstStaffTmp, lstTmpAmt, lstTmpNum) {
		$(".ErrorMsgStyle").hide();
		$.messager.confirm("Xác nhận", "Bạn có muốn thêm NVBH?", function(r) {
			if (r) {
				var zezo = 0;
				var arrQttHL = [];
				var arrAmtHL = [];
				var arrNumHL = [];
				var arrIdHl = lstStaffTmp.keyArray;
				for (var i = 0, size = arrIdHl.length; i < size; i++) {
					var valMap = lstStaffTmp.get(arrIdHl[i]);
					if (valMap != undefined && valMap != null && valMap.length > 0) {
						arrQttHL.push(valMap);
					} else {
						arrQttHL.push(zezo);
					}
					valMap = lstTmpAmt.get(arrIdHl[i]);
					if (valMap != undefined && valMap != null && valMap.length > 0) {
						arrAmtHL.push(valMap);
					} else { 
						arrAmtHL.push(zezo);
					}
					valMap = lstTmpNum.get(arrIdHl[i]);
					if (valMap != undefined && valMap != null && valMap.length > 0) {
						arrNumHL.push(valMap);
					} else {
						arrNumHL.push(zezo);
					}
				}
				
				var params = {
						promotionId: $("#masterDataArea #id").val().trim(),
						lstQtt: arrQttHL,
						lstId: arrIdHl,
						lstAmt: arrAmtHL,
						lstNum: arrNumHL
				};
				$("#add-shopPopup").dialog("close");
				Utils.saveData(params, "/promotion/add-saler", PromotionProgram._xhrSave, 'errMsgShop', function(data) {
					$("#successMsgShop").html("Lưu dữ liệu thành công").show();
					setTimeout(function() {$("#successMsgShop").hide();},3000);
					$("#exGrid").treegrid("reload");
				}, "loading2");
			}
		});
	},
	
	exportPromotionStaff: function() {
		$(".ErrorMsgStyle").hide();
		if (PromotionProgram._promotionStaffParams == null) {
			PromotionProgram._promotionStaffParams = {};
			PromotionProgram._promotionStaffParams.promotionId = $("#masterDataArea #id").val().trim();
		}
		ReportUtils.exportReport("/promotion/export-promotion-staff", PromotionProgram._promotionStaffParams, "errMsgShop");
	},
	
	/**
	 * Export KH tham gia CTKM
	 * 
	 * @author hunglm16
	 * @since 24/09/2015
	 */
	exportPromotionCustomer: function() {
		$(".ErrorMsgStyle").hide();
		if (PromotionProgram._promotionStaffParams == null) {
			PromotionProgram._promotionStaffParams = {};
			PromotionProgram._promotionStaffParams.promotionId = $("#masterDataArea #id").val().trim();
			PromotionProgram._promotionStaffParams.code = $('#customerCode').val().trim();
			PromotionProgram._promotionStaffParams.name = $('#customerName').val().trim();
			PromotionProgram._promotionStaffParams.address = $('#address').val().trim();
		}
		ReportUtils.exportReport("/promotion/export-promotion-customer", PromotionProgram._promotionStaffParams, "errMsgShop");
	},
	
	importPromotionStaff: function() {
		$('.ErrorMsgStyle').hide();
		VTUtilJS.importExcelUtils(function(data) {
			$("#exGrid").treegrid("reload");
			$("#errExcelMsgShop").html(data.message).show();
		}, "importFrm", "excelFile", null, "errExcelMsgShop");
	},
	
	importPromotionCustomer: function() {
		$('.ErrorMsgStyle').hide();
		VTUtilJS.importExcelUtils(function(data) {
			$("#exGrid").treegrid("reload");
			$("#errExcelMsgShop").html(data.message).show();
		}, "importFrm", "excelFile", null, "errExcelMsgShop");
	},
	
	deleteHtmlLevel : function(tableLevelDiv, groupMuaId, groupKMId, mapId, levelMuaId, levelKMId) {
		if($('#statusHidden').val() == 0 || $('#statusHidden').val() == 1){
			return;
		}
		$.messager.confirm("Xác nhận", "Bạn có muốn xóa thông tin mức không?", function(r) {
			if(r) {
				VTUtilJS.postFormJson({groupMuaId : groupMuaId, groupKMId : groupKMId, levelMuaId : levelMuaId, levelKMId : levelKMId, mappingId : mapId}, '/promotion/new-delete-level', function(data){
					if(!data.error) {
						var parentDiv = $('#'+tableLevelDiv).parent().parent().parent();
						$('#'+tableLevelDiv).panel('destroy');
						parentDiv.remove();
						if($($('#mappingGrid tbody')[0]).children().length == 0) {
							$($('#mappingGrid tbody')[0]).append('<tr class="NewLevel">\
											<td>\
											<div class="TableLevel" id="tableLevel" style="width:100%; height:\'auto\';padding:10px;" title="">\
									    </div>\
								</td>\
							</tr>');
							$('#tableLevel').panel({
								width:'95%',
								height:'auto',
								collapsible:true,
								title:' ',
								tools:[{
									iconCls:'iconAdd',
									handler:function(){
										PromotionProgram.addHtmlLevel('tableLevel', groupMuaId, groupKMId);
									}
								}],
								onLoad:function(){
									
								}
							});
						}
					}
				});
			}
		});
	},
	editHtmlLevel : function(tableLevelDiv, groupMuaId, groupKMId, mapId, levelMuaId, levelKMId, levelCode, stt) {
		if($('#statusHidden').val() == 0 || $('#statusHidden').val() == 1){
			return;
		}
		var html = '<div id="popup-container"><div id="popup-add-level" class="PopupContentMid">\
			<div class="GeneralForm Search1Form" id="form-data">\
			<label style="width: 100px;" class="LabelStyle Label1Style">Mã mức<span class="ReqiureStyle">(*)</span></label>\
			<input type="text" value="'+levelCode+'" call-back="VTValidateUtils.getMessageOfSpecialCharactersValidate(\'levelCode\', \'Mã mức\', Utils._CODE)" class="InputTextStyle InputText1Style" style="width: 145px;" maxlength="50" id="levelCode"> \
			<label style="width: 100px;" class="LabelStyle Label1Style">Số thứ tự<span class="ReqiureStyle">(*)</span></label> \
			<input type="text" disabled="disabled" call-back="VTValidateUtils.getMessageOfRequireCheck(\'stt\', \'Số thứ tự\', false, true)" value="'+((stt == null || stt == undefined || stt == 0)?'' : stt)+'" class="InputTextStyle InputText1Style" style="width: 145px;" maxlength="3" id="stt">\
			<input type="hidden" id="groupMuaId" value="'+groupMuaId+'">\
			<input type="hidden" id="groupKMId" value="'+groupKMId+'">\
			<input type="hidden" id="levelMuaId" value="'+levelMuaId+'">\
			<input type="hidden" id="levelKMId" value="'+levelKMId+'">\
			<div class="Clear"></div>\
			<div class="BtnCenterSection">\
			<button class="BtnGeneralStyle" id="btn-update">Cập nhật</button>\
			</div>\
			<p class="ErrorMsgStyle SpriteErr" style="display: none;" id="errMsgLevelDlg"></p>\
			<p class="SuccessMsgStyle" style="display: none;" id="successMsgLevelDlg"></p>\
			</div></div></div>';
		$("body").append(html);
		$('#popup-add-level').dialog({
			title: "Chỉnh sửa mức CTKM",
			width: 600,
			heigth: "auto",
			modal: true, close: false, cache: false,
			onOpen: function() {
				VTUtilJS.bindFormatOnTextfield('stt', VTUtilJS._TF_NUMBER);
				$('#btn-update').bind('click', function() {
					var mm = $('#form-data #levelCode').val();
					if(mm == null || mm == undefined || mm.trim().length == 0) {
						$('#errMsgLevelDlg').html('Mã mức không được để trống').show();
						return;
					}
					VTUtilJS.postFormJson('form-data', '/promotion/new-add-level', function(data){
						if(!data.error) {
							var parentDiv = $('#'+tableLevelDiv).parent().parent().parent();
							$('#popup-add-level').dialog('close');
							$('#'+tableLevelDiv).panel('destroy');
							parentDiv.html('');
							parentDiv.append('<td>\
									<p class="ErrorMsgStyle SpriteErr" style="display: none;" id="errorMsgDistribute'+(VTUtilJS.isNullOrEmpty(data.newMapping.levelCode) ? data.newMapping.mapId : data.newMapping.levelCode+data.newMapping.mapId)+'">\
									<p id="successMsgDistribute'+(VTUtilJS.isNullOrEmpty(data.newMapping.levelCode) ? data.newMapping.mapId : data.newMapping.levelCode+data.newMapping.mapId)+'" class="SuccessMsgStyle" style="display: none"></p>\
									<div class="TableLevel" id="tableLevel'+data.newMapping.mapId+'" style="width:100%; height:\'auto\';padding:10px;" title="">\
								<table style="width: 100%;">\
						    	<tr>\
						    		<td style="width: 50%; vertical-align: top;">\
						    			<table id="tableSubLevelMua'+(VTUtilJS.isNullOrEmpty(data.newMapping.levelCode) ? data.newMapping.mapId : data.newMapping.levelCode+data.newMapping.mapId)+'" style="border: 1px solid black; vertical-align: top;width: 100%">\
						    				<tbody>\
						    				</tbody>\
						    			</table>\
						    		</td>\
						    		<td style="width: 50%; vertical-align: top;">\
						    			<table id="tableSubLevelKM'+(VTUtilJS.isNullOrEmpty(data.newMapping.levelCode) ? data.newMapping.mapId : data.newMapping.levelCode+data.newMapping.mapId)+'" style="border: 1px solid black; vertical-align: top;width: 100%">\
						    				<tbody>\
						    				</tbody>\
						    			</table>\
						    		</td>\
						    	</tr>\
					    	</table>\
								    </div>\
							</td>');
							VTUtilJS.bindFormatOnTextfield('sttMua_'+(VTUtilJS.isNullOrEmpty(data.newMapping.levelCode) ? data.newMapping.mapId : data.newMapping.levelCode+data.newMapping.mapId), VTUtilJS._TF_NUMBER);
							var tools = new Array();
							tools.push({
								iconCls:'iconAdd',
								handler:function(){
									PromotionProgram.addHtmlLevel('tableLevel'+data.newMapping.mapId+'', groupMuaId, groupKMId);
								}
							});
							tools.push({
								iconCls:'iconEdit',
								handler:function(){
									PromotionProgram.editHtmlLevel('tableLevel'+data.newMapping.mapId+'', groupMuaId, groupKMId, data.newMapping.mapId, data.newMapping.levelMuaId, data.newMapping.levelKMId, (VTUtilJS.isNullOrEmpty(data.newMapping.levelCode) ? '' : data.newMapping.levelCode), (data.newMapping.stt == null || data.newMapping.stt == undefined) ? 0 : data.newMapping.stt);
								}
							});
							tools.push({
								iconCls:'iconDelete',
								handler:function(){
									PromotionProgram.deleteHtmlLevel('tableLevel'+data.newMapping.mapId+'', groupMuaId, groupKMId, data.newMapping.mapId, data.newMapping.levelMuaId, data.newMapping.levelKMId);
								}
							});
							tools.push({
								iconCls:'iconCopy',
								handler:function(){
									PromotionProgram.copyHtmlLevel(data.newMapping.mapId, groupMuaId, groupKMId);
								}
							});
							tools.push({
								iconCls:'iconSave',
								handler:function(){
									PromotionProgram.saveHtmlLevel((VTUtilJS.isNullOrEmpty(data.newMapping.levelCode) ? data.newMapping.mapId : data.newMapping.levelCode+data.newMapping.mapId), groupMuaId, groupKMId, data.newMapping.mapId, data.newMapping.levelMuaId, data.newMapping.levelKMId);
								}
							});
							
							$('#tableLevel'+data.newMapping.mapId).panel({
								width:'95%',
								height:'auto',
								collapsible:true,
								title: VTUtilJS.isNullOrEmpty(data.newMapping.levelCode) ? 'STT : ' + data.newMapping.stt : 'Mã : ' + Utils.XSSEncode(data.newMapping.levelCode) + ' - ' + 'STT : ' + data.newMapping.stt,
								tools:tools
							});
							if(data.newMapping.listExLevelMua != null && data.newMapping.listExLevelMua != undefined && $.isArray(data.newMapping.listExLevelMua)) {
								PromotionProgram.addHtmlSubRowMua('tableSubLevelMua'+(VTUtilJS.isNullOrEmpty(data.newMapping.levelCode) ? data.newMapping.mapId : Utils.XSSEncode(data.newMapping.levelCode) + data.newMapping.mapId), VTUtilJS.isNullOrEmpty(data.newMapping.levelCode) ? data.newMapping.mapId : data.newMapping.levelCode+data.newMapping.mapId, data.newMapping.listExLevelMua);
							}
							if(data.newMapping.listExLevelKM != null && data.newMapping.listExLevelKM != undefined && $.isArray(data.newMapping.listExLevelKM)) {
								PromotionProgram.addHtmlSubRowKM('tableSubLevelKM'+(VTUtilJS.isNullOrEmpty(data.newMapping.levelCode) ? data.newMapping.mapId : Utils.XSSEncode(data.newMapping.levelCode) + data.newMapping.mapId), VTUtilJS.isNullOrEmpty(data.newMapping.levelCode) ? data.newMapping.mapId : data.newMapping.levelCode+data.newMapping.mapId, data.newMapping.listExLevelKM);
							}
							Utils.functionAccessFillControl('divTab', function(data) {
								//Xu ly cac su kien lien quan den ctl phan quyen
							});
						} else {
							$('#errMsgLevelDlg').html(data.errMsg).show();
						}
					});
				});
			},
			onClose: function() {
				$("#popup-add-level").dialog("destroy");
				$("#popup-container").remove();
			}
		});
	},
	addHtmlLevel : function(tableLevelDiv, groupMuaId, groupKMId) {
		$('.ErrorMsgStyle').hide();
		VTUtilJS.getFormJson({groupMuaId : groupMuaId}, '/promotion/new-get-max-order', function(__data){
			if(__data.error) {
				return;
			}
			if($('#statusHidden').val() == 0 || $('#statusHidden').val() == 1){
				return;
			}
			var html = '<div id="popup-container"><div id="popup-add-level" class="PopupContentMid">\
				<div class="GeneralForm Search1Form" id="form-data">\
				<label style="width: 100px;" class="LabelStyle Label1Style">Mã mức<span class="ReqiureStyle">(*)</span></label>\
				<input type="text" call-back="VTValidateUtils.getMessageOfSpecialCharactersValidate(\'levelCode\', \'Mã mức\', Utils._CODE)" class="InputTextStyle InputText1Style" style="width: 145px;" maxlength="50" id="levelCode"> \
				<label style="width: 100px;" class="LabelStyle Label1Style">Số thứ tự<span class="ReqiureStyle">(*)</span></label> \
				<input type="text" value="'+__data.stt+'" call-back="VTValidateUtils.getMessageOfRequireCheck(\'stt\', \'Số thứ tự\', false, true);VTValidateUtils.getMessageOfInvaildInteger(\'stt\', \'Số thự tự\');VTValidateUtils.getMessageOfNegativeNumberCheck(\'stt\', \'Số thự tự\');" class="InputTextStyle InputText1Style" style="width: 145px;" maxlength="3" id="stt">\
				<input type="hidden" id="groupMuaId" value="'+groupMuaId+'">\
				<input type="hidden" id="groupKMId" value="'+groupKMId+'">\
				<div class="Clear"></div>\
				<div class="BtnCenterSection">\
				<button class="BtnGeneralStyle" id="btn-update">Cập nhật</button>\
				</div>\
				<p class="ErrorMsgStyle SpriteErr" style="display: none;" id="errMsgLevelDlg"></p>\
				<p class="SuccessMsgStyle" style="display: none;" id="successMsgLevelDlg"></p>\
				</div></div></div>';
			$("body").append(html);
			$('#popup-add-level').dialog({
				title: "Thêm mức CTKM",
				width: 600,
				heigth: "auto",
				modal: true, close: false, cache: false,
				onOpen: function() {
					VTUtilJS.bindFormatOnTextfield('stt', VTUtilJS._TF_NUMBER);
					$('#btn-update').bind('click', function() {
						$('#errMsgLevelDlg').html('').hide();
						var mm = $('#form-data #levelCode').val();
						if(mm == null || mm == undefined || mm.trim().length == 0) {
							$('#errMsgLevelDlg').html('Mã mức không được để trống').show();
							return;
						}
						var stt = $('#form-data #stt').val();
						if(stt == null || stt == undefined || isNaN(stt) || Number(stt) <= 0) {
							if(formData == null) {
								$('#errMsgLevelDlg').html('Số thứ tự không hợp lệ').show();
								return;
							}
						}
						var formData = VTUtilJS.getFormData('form-data');
						if(formData == null) {
							var errMsg = $('#errorMsg').html();
							$('#errorMsg').html('').hide();
							$('#errMsgLevelDlg').html(errMsg).show();
							return;
						}
						$.messager.confirm("Xác nhận", "Bạn có muốn thêm mức?", function(r) {
							if(r) {
								VTUtilJS.postFormJson('form-data', '/promotion/new-add-level', function(data){
									if(!data.error) {
										$('#popup-add-level').dialog('close');
										var trDiv = $('#'+tableLevelDiv).parent().parent().parent();//tr
										var tbodyDiv = $('#'+tableLevelDiv).parent().parent().parent().parent();//tbody
										if(trDiv.hasClass('NewLevel')) {
											$('#'+tableLevelDiv).panel('destroy');
											trDiv.remove();
										}
										tbodyDiv.append('<tr>\
														<td>\
														<p class="ErrorMsgStyle SpriteErr" style="display: none;" id="errorMsgDistribute'+(VTUtilJS.isNullOrEmpty(data.newMapping.levelCode) ? data.newMapping.mapId : Utils.XSSEncode(data.newMapping.levelCode) + data.newMapping.mapId)+'">\
														<p id="successMsgDistribute'+(VTUtilJS.isNullOrEmpty(data.newMapping.levelCode) ? data.newMapping.mapId : Utils.XSSEncode(data.newMapping.levelCode) + data.newMapping.mapId)+'" class="SuccessMsgStyle" style="display: none"></p>\
														<div class="TableLevel" id="tableLevel'+(data.newMapping.mapId)+'" style="width:100%; height:\'auto\';padding:10px;" title="">\
												<table style="width: 100%;">\
										    	<tr>\
										    		<td style="width: 50%; vertical-align: top;">\
										    			<table id="tableSubLevelMua'+(VTUtilJS.isNullOrEmpty(data.newMapping.levelCode) ? data.newMapping.mapId : Utils.XSSEncode(data.newMapping.levelCode) + data.newMapping.mapId)+'" style="border: 1px solid black; vertical-align: top;width: 100%">\
										    				<tbody>\
										    				</tbody>\
										    			</table>\
										    		</td>\
										    		<td style="width: 50%; vertical-align: top;">\
										    			<table id="tableSubLevelKM'+(VTUtilJS.isNullOrEmpty(data.newMapping.levelCode) ? data.newMapping.mapId : Utils.XSSEncode(data.newMapping.levelCode) + data.newMapping.mapId)+'" style="border: 1px solid black; vertical-align: top;width: 100%">\
										    				<tbody>\
										    				</tbody>\
										    			</table>\
										    		</td>\
										    	</tr>\
									    	</table>\
												    </div>\
											</td>\
										</tr>');
										VTUtilJS.bindFormatOnTextfield('sttMua_'+(VTUtilJS.isNullOrEmpty(data.newMapping.levelCode) ? data.newMapping.mapId : Utils.XSSEncode(data.newMapping.levelCode) + data.newMapping.mapId), VTUtilJS._TF_NUMBER);
										
										var tools = new Array();
										tools.push({
											iconCls:'iconAdd ' + data.newMapping.mapId+'',
											handler:function(){
												PromotionProgram.addHtmlLevel('tableLevel'+data.newMapping.mapId+'', groupMuaId, groupKMId);
											}
										});
										tools.push({
											iconCls:'iconEdit '+data.newMapping.mapId+'',
											handler:function(){
												PromotionProgram.editHtmlLevel('tableLevel'+(data.newMapping.mapId), groupMuaId, groupKMId, data.newMapping.mapId, data.newMapping.levelMuaId, data.newMapping.levelKMId, (VTUtilJS.isNullOrEmpty(data.newMapping.levelCode) ? '' : data.newMapping.levelCode), (data.newMapping.stt == null || data.newMapping.stt == undefined) ? 0 : data.newMapping.stt);
											}
										});
										tools.push({
											iconCls:'iconDelete '+data.newMapping.mapId+'',
											handler:function(){
												PromotionProgram.deleteHtmlLevel('tableLevel'+(data.newMapping.mapId), groupMuaId, groupKMId, data.newMapping.mapId, data.newMapping.levelMuaId, data.newMapping.levelKMId);
											}
										});
										tools.push({
											iconCls:'iconCopy '+data.newMapping.mapId+'',
											handler:function(){
												PromotionProgram.copyHtmlLevel(data.newMapping.mapId, groupMuaId, groupKMId);
											}
										});
										tools.push({
											iconCls:'iconSave '+data.newMapping.mapId+'',
											handler:function(){
												PromotionProgram.saveHtmlLevel((VTUtilJS.isNullOrEmpty(data.newMapping.levelCode) ? data.newMapping.mapId : Utils.XSSEncode(data.newMapping.levelCode) + data.newMapping.mapId), groupMuaId, groupKMId, data.newMapping.mapId, data.newMapping.levelMuaId, data.newMapping.levelKMId);
											}
										});
										
										$('#tableLevel'+data.newMapping.mapId).panel({
											width:'95%',
											height:'auto',
											collapsible:true,
											title: VTUtilJS.isNullOrEmpty(data.newMapping.levelCode) ? 'STT : ' + data.newMapping.stt : 'Mã : ' + Utils.XSSEncode(data.newMapping.levelCode) + ' - ' + 'STT : ' + data.newMapping.stt,
											tools:tools,
											onLoad:function(){
											}
										});
										
										if(data.newMapping.listExLevelMua != null && data.newMapping.listExLevelMua != undefined && $.isArray(data.newMapping.listExLevelMua)) {
											PromotionProgram.addHtmlSubRowMua('tableSubLevelMua'+(VTUtilJS.isNullOrEmpty(data.newMapping.levelCode) ? data.newMapping.mapId : Utils.XSSEncode(data.newMapping.levelCode) + data.newMapping.mapId), VTUtilJS.isNullOrEmpty(data.newMapping.levelCode) ? data.newMapping.mapId : data.newMapping.levelCode+data.newMapping.mapId, data.newMapping.listExLevelMua);
										}
										if(data.newMapping.listExLevelKM != null && data.newMapping.listExLevelKM != undefined && $.isArray(data.newMapping.listExLevelKM)) {
											PromotionProgram.addHtmlSubRowKM('tableSubLevelKM'+(VTUtilJS.isNullOrEmpty(data.newMapping.levelCode) ? data.newMapping.mapId : Utils.XSSEncode(data.newMapping.levelCode) + data.newMapping.mapId), VTUtilJS.isNullOrEmpty(data.newMapping.levelCode) ? data.newMapping.mapId : data.newMapping.levelCode+data.newMapping.mapId, data.newMapping.listExLevelKM);
										}
										Utils.functionAccessFillControl('divTab', function(data) {
											//Xu ly cac su kien lien quan den ctl phan quyen
										});
									} else {
										$('#errMsgLevelDlg').html(data.errMsg).show();
									}
								}, "errMsgLevelDlg");
							}
						});
					});
				},
				onClose: function() {
					$("#popup-add-level").dialog("destroy");
					$("#popup-container").remove();
				}
			});
		});
	},
	addSubProductLevel : function(masterGridId, index, detailGridId) {
		if($('#statusHidden').val() == 0 || $('#statusHidden').val() == 1){
			return;
		}
		$('.ErrorMsgStyle').hide();
		var ddv = $('#'+masterGridId).datagrid('getRowDetail', index).find('div.ddv#'+detailGridId);
		if(masterGridId.indexOf('gridSublevelMua') == 0 && ($('#typeCode').val() == 'ZV01' 
			|| $('#typeCode').val() == 'ZV02' 
			|| $('#typeCode').val() == 'ZV03' 
			|| $('#typeCode').val() == 'ZV04' 
			|| $('#typeCode').val() == 'ZV05' 
			|| $('#typeCode').val() == 'ZV06')
			) {
			if(ddv.datagrid('getRows').length == 1) {
				return;
			}
		}
		if(masterGridId.indexOf('gridSublevelMua') == 0 && ($('#typeCode').val() == 'ZV19'
			|| $('#typeCode').val() == 'ZV20'
			|| $('#typeCode').val() == 'ZV21'
			)) {
			return;
		}
		if(masterGridId.indexOf('gridSublevelKM') == 0 && ($('#typeCode').val() == 'ZV01'
			|| $('#typeCode').val() == 'ZV04'
			|| $('#typeCode').val() == 'ZV07'
			|| $('#typeCode').val() == 'ZV10'
			|| $('#typeCode').val() == 'ZV13'
			|| $('#typeCode').val() == 'ZV16'
			|| $('#typeCode').val() == 'ZV19'
			|| $('#typeCode').val() == 'ZV02'
			|| $('#typeCode').val() == 'ZV05'
			|| $('#typeCode').val() == 'ZV08'
			|| $('#typeCode').val() == 'ZV11'
			|| $('#typeCode').val() == 'ZV14'
			|| $('#typeCode').val() == 'ZV17'
			|| $('#typeCode').val() == 'ZV20'
			)) {
			return;
		}
		if(masterGridId.indexOf('gridSublevelMua') == 0) {
			for(var i = 0; i < ddv.datagrid('getRows').length; i++) {
				var row = ddv.datagrid('getRows')[i];
				if(VTUtilJS.isNullOrEmpty(row.productCode)) {
					return;
				}
				if(($('#typeCode').val() != 'ZV07' && $('#typeCode').val() != 'ZV08' && $('#typeCode').val() != 'ZV09' && $('#typeCode').val() != 'ZV10' && $('#typeCode').val() != 'ZV11' && $('#typeCode').val() != 'ZV12') && (VTUtilJS.isNullOrEmpty(row.quantity) || isNaN(row.quantity) || Number(row.quantity) == 0) && (VTUtilJS.isNullOrEmpty(row.amount) || isNaN(row.amount) || Number(row.amount) == 0)) {
					return;
				}
			}
		} else if(masterGridId.indexOf('gridSublevelKM') == 0) {
			for(var i = 0; i < ddv.datagrid('getRows').length; i++) {
				var row = ddv.datagrid('getRows')[i];
				if(VTUtilJS.isNullOrEmpty(row.productCode)) {
					return;
				}
				if(($('#typeCode').val() != 'ZV22' && $('#typeCode').val() != 'ZV23' && $('#typeCode').val() != 'ZV24') && (VTUtilJS.isNullOrEmpty(row.quantity) || isNaN(row.quantity) || Number(row.quantity) == 0) && (VTUtilJS.isNullOrEmpty(row.amount) || isNaN(row.amount) || Number(row.amount) == 0)) {
					return;
				}
			}
		}
		ddv.datagrid('acceptChanges');
		ddv.datagrid('appendRow', {isRequired:0});
		ddv.datagrid('beginEdit', ddv.datagrid('getRows').length-1);
		$('#'+masterGridId).datagrid('getRows')[index].listSubLevel = ddv.datagrid('getRows');
		setTimeout(function() {
			$('#'+masterGridId).datagrid('fixRowHeight',index);
			$('#'+masterGridId).datagrid('fixDetailRowHeight',index);
		}, 500);
	},
	addHtmlSubRowMua: function(divTable, levelCode, subLevel) {
		$('.ErrorMsgStyle').hide();
		if ($('#' + divTable + ' tbody tr').length > 1) {
			if (($('#gridSublevelMua'+levelCode).length > 0 && $('#gridSublevelMua'+levelCode).datagrid('getRows').length > 0) && ($('#typeCode').val() == 'ZV01'
				|| $('#typeCode').val() == 'ZV02'
				|| $('#typeCode').val() == 'ZV03'
				|| $('#typeCode').val() == 'ZV04'
				|| $('#typeCode').val() == 'ZV05'
				|| $('#typeCode').val() == 'ZV06'
				|| $('#typeCode').val() == 'ZV07'
				|| $('#typeCode').val() == 'ZV08'
				|| $('#typeCode').val() == 'ZV09'
				|| $('#typeCode').val() == 'ZV10'
				|| $('#typeCode').val() == 'ZV11'
				|| $('#typeCode').val() == 'ZV12'
				|| $('#typeCode').val() == 'ZV13'
				|| $('#typeCode').val() == 'ZV14'
				|| $('#typeCode').val() == 'ZV15'
				|| $('#typeCode').val() == 'ZV16'
				|| $('#typeCode').val() == 'ZV17'
				|| $('#typeCode').val() == 'ZV18'
				|| $('#typeCode').val() == 'ZV19'
				|| $('#typeCode').val() == 'ZV20'
				|| $('#typeCode').val() == 'ZV21')) { 
				return;
			}
			//da co grid roi
			var rows = $('#gridSublevelMua'+levelCode).datagrid('getRows');
			rows.push({});
			$('#gridSublevelMua'+levelCode).datagrid('loadData', rows);
		} else {
			//chua co grid
			$('#'+divTable+' tbody').append('<tr>\
					<td colspan="5"><div id="gridSublevelMua'+levelCode+'" style="width:100%; height:\'auto\'; padding:10px;"></div></td>\
					</tr>');
			if (subLevel == undefined || subLevel == null || !$.isArray(subLevel) || subLevel.length == 0) {
				subLevel = [{listSubLevel:[]}];
			}
			var disMinQuantityMua = false;
			var disMinAmountMua = false;
			var disQuantityMua = false;
			var disAmountMua = false;
			var disPercentKM = false;
			var disMaxQuantityKM = false;
			var disMaxAmountKM = false;
			var disQuantityKM = false;
			var disAmountKM = false;
			if ($('#typeCode').val() == 'ZV01') {
				disMinQuantityMua = true;
				disMinAmountMua = true;
				disQuantityMua = false;
				disAmountMua = true;
				disPercentKM = false;
				disMaxQuantityKM = true;
				disMaxAmountKM = true;
				disQuantityKM = true;
				disAmountKM = true;
			} else if ($('#typeCode').val() == 'ZV02') {
				disMinQuantityMua = true;
				disMinAmountMua = true;
				disQuantityMua = false;
				disAmountMua = true;
				disPercentKM = true;
				disMaxQuantityKM = true;
				disMaxAmountKM = false;
				disQuantityKM = true;
				disAmountKM = true;
			} else if ($('#typeCode').val() == 'ZV03') {
				disMinQuantityMua = true;
				disMinAmountMua = true;
				disQuantityMua = false;
				disAmountMua = true;
				disPercentKM = true;
				disMaxQuantityKM = true;
				disMaxAmountKM = true;
				disQuantityKM = false;
				disAmountKM = true;
			} else if ($('#typeCode').val() == 'ZV04') {
				disMinQuantityMua = true;
				disMinAmountMua = true;
				disQuantityMua = true;
				disAmountMua = false;
				disPercentKM = false;
				disMaxQuantityKM = true;
				disMaxAmountKM = true;
				disQuantityKM = true;
				disAmountKM = true;
			} else if ($('#typeCode').val() == 'ZV05') {
				disMinQuantityMua = true;
				disMinAmountMua = true;
				disQuantityMua = true;
				disAmountMua = false;
				disPercentKM = true;
				disMaxQuantityKM = true;
				disMaxAmountKM = false;
				disQuantityKM = true;
				disAmountKM = true;
			} else if ($('#typeCode').val() == 'ZV06') {
				disMinQuantityMua = true;
				disMinAmountMua = true;
				disQuantityMua = true;
				disAmountMua = false;
				disPercentKM = true;
				disMaxQuantityKM = true;
				disMaxAmountKM = true;
				disQuantityKM = false;
				disAmountKM = true;
			} else if ($('#typeCode').val() == 'ZV07') {
				disMinQuantityMua = false;
				disMinAmountMua = true;
				disQuantityMua = true;
				disAmountMua = true;
				disPercentKM = false;
				disMaxQuantityKM = true;
				disMaxAmountKM = true;
				disQuantityKM = true;
				disAmountKM = true;
			} else if ($('#typeCode').val() == 'ZV08') {
				disMinQuantityMua = false;
				disMinAmountMua = true;
				disQuantityMua = true;
				disAmountMua = true;
				disPercentKM = true;
				disMaxQuantityKM = true;
				disMaxAmountKM = false;
				disQuantityKM = true;
				disAmountKM = true;
			} else if ($('#typeCode').val() == 'ZV09') {
				disMinQuantityMua = false;
				disMinAmountMua = true;
				disQuantityMua = true;
				disAmountMua = true;
				disPercentKM = true;
				disMaxQuantityKM = true;
				disMaxAmountKM = true;
				disQuantityKM = false;
				disAmountKM = true;
			} else if ($('#typeCode').val() == 'ZV10') {
				disMinQuantityMua = true;
				disMinAmountMua = false;
				disQuantityMua = true;
				disAmountMua = true;
				disPercentKM = false;
				disMaxQuantityKM = true;
				disMaxAmountKM = true;
				disQuantityKM = true;
				disAmountKM = true;
			} else if ($('#typeCode').val() == 'ZV11') {
				disMinQuantityMua = true;
				disMinAmountMua = false;
				disQuantityMua = true;
				disAmountMua = true;
				disPercentKM = true;
				disMaxQuantityKM = true;
				disMaxAmountKM = false;
				disQuantityKM = true;
				disAmountKM = true;
			} else if ($('#typeCode').val() == 'ZV12') {
				disMinQuantityMua = true;
				disMinAmountMua = false;
				disQuantityMua = true;
				disAmountMua = true;
				disPercentKM = true;
				disMaxQuantityKM = true;
				disMaxAmountKM = true;
				disQuantityKM = false;
				disAmountKM = true;
			} else if ($('#typeCode').val() == 'ZV13') {
				disMinQuantityMua = true;
				disMinAmountMua = true;
				disQuantityMua = false;
				disAmountMua = true;
				disPercentKM = false;
				disMaxQuantityKM = true;
				disMaxAmountKM = true;
				disQuantityKM = true;
				disAmountKM = true;
			} else if ($('#typeCode').val() == 'ZV14') {
				disMinQuantityMua = true;
				disMinAmountMua = true;
				disQuantityMua = false;
				disAmountMua = true;
				disPercentKM = true;
				disMaxQuantityKM = true;
				disMaxAmountKM = false;
				disQuantityKM = true;
				disAmountKM = true;
			} else if ($('#typeCode').val() == 'ZV15') {
				disMinQuantityMua = true;
				disMinAmountMua = true;
				disQuantityMua = false;
				disAmountMua = true;
				disPercentKM = true;
				disMaxQuantityKM = true;
				disMaxAmountKM = true;
				disQuantityKM = false;
				disAmountKM = true;
			} else if ($('#typeCode').val() == 'ZV16') {
				disMinQuantityMua = true;
				disMinAmountMua = true;
				disQuantityMua = true;
				disAmountMua = false;
				disPercentKM = false;
				disMaxQuantityKM = true;
				disMaxAmountKM = true;
				disQuantityKM = true;
				disAmountKM = true;
			} else if ($('#typeCode').val() == 'ZV17') {
				disMinQuantityMua = true;
				disMinAmountMua = true;
				disQuantityMua = true;
				disAmountMua = false;
				disPercentKM = true;
				disMaxQuantityKM = true;
				disMaxAmountKM = false;
				disQuantityKM = true;
				disAmountKM = true;
			} else if ($('#typeCode').val() == 'ZV18') {
				disMinQuantityMua = true;
				disMinAmountMua = true;
				disQuantityMua = true;
				disAmountMua = false;
				disPercentKM = true;
				disMaxQuantityKM = true;
				disMaxAmountKM = true;
				disQuantityKM = false;
				disAmountKM = true;
			} else if ($('#typeCode').val() == 'ZV19') {
				disMinQuantityMua = true;
				disMinAmountMua = false;
				disQuantityMua = true;
				disAmountMua = true;
				disPercentKM = false;
				disMaxQuantityKM = true;
				disMaxAmountKM = true;
				disQuantityKM = true;
				disAmountKM = true;
			} else if ($('#typeCode').val() == 'ZV20') {
				disMinQuantityMua = true;
				disMinAmountMua = false;
				disQuantityMua = true;
				disAmountMua = true;
				disPercentKM = true;
				disMaxQuantityKM = true;
				disMaxAmountKM = false;
				disQuantityKM = true;
				disAmountKM = true;
			} else if ($('#typeCode').val() == 'ZV21') {
				disMinQuantityMua = true;
				disMinAmountMua = false;
				disQuantityMua = true;
				disAmountMua = true;
				disPercentKM = true;
				disMaxQuantityKM = true;
				disMaxAmountKM = true;
				disQuantityKM = false;
				disAmountKM = true;
			}
			$('#gridSublevelMua'+levelCode).datagrid({
				data : subLevel,
				view: detailview,
                detailFormatter:function(idx,r){
                    return '<div class="ddv" id="'+levelCode+'_subGridMuaProduct_'+idx+'" style="padding:5px 0"></div>';
                },
                onExpandRow: function(idx,r){
                    var ddv = $(this).datagrid('getRowDetail', idx).find('div.ddv#'+levelCode+'_subGridMuaProduct_'+idx);
                    ddv.datagrid({
                    	data:r.listSubLevel,
                        fitColumns:true,
                        singleSelect:true,
                        rownumbers:true,
                        scrollbarSize:0,
                        height:'auto',
                        onDblClickRow : function(index,row) {
                        	for(var i = 0; i < ddv.datagrid('getRows').length; i++) {
                    			var row = ddv.datagrid('getRows')[i];
                    			if(VTUtilJS.isNullOrEmpty(row.productCode)) {
                    				return;
                    			}
                    			if(($('#typeCode').val() != 'ZV07' && $('#typeCode').val() != 'ZV08' && $('#typeCode').val() != 'ZV09' && $('#typeCode').val() != 'ZV10' && $('#typeCode').val() != 'ZV11' && $('#typeCode').val() != 'ZV12') && (VTUtilJS.isNullOrEmpty(row.quantity) || isNaN(row.quantity) || Number(row.quantity) == 0) && (VTUtilJS.isNullOrEmpty(row.amount) || isNaN(row.amount) || Number(row.amount) == 0)) {
                    				return;
                    			}
                    		}
                    		ddv.datagrid('acceptChanges');
                        	ddv.datagrid('beginEdit', index);
                        },
                        columns:[[
                            {field:'productCode', sortable:false,resizable:false, title:'Mã SP', width:80, align:'left', formatter: function(value, row, index) {
    							return Utils.XSSEncode(value);
    						},
                            	editor: {
	    							type:'combobox',
	    							 options:{
    								 	valueField : 'productCode',
    								 	textField : 'productCode',
    								 	data : PromotionProgram._listProduct.valArray,
    								 	formatter:function(row){
    			                            return Utils.XSSEncode(row.productCode + ' - ' +row.productName);
    			                        },
    								 	onHidePanel: function(){
    								 		var input = $(this).parent().find("input.combo-text.validatebox-text");
    								 		var r = PromotionProgram._listProduct.get(input.val());
    								 		var row = $(this).parent().parent().parent().parent().parent().parent().parent();
    								 		var detail = ddv.datagrid('getRows')[Number(row.attr('datagrid-row-index'))];
    								 		if (r != null && r != undefined) {
	    								 		row.find('td[field=productName] div').html(Utils.XSSEncode(r.productName));
	    								 		//Xu ly them tham so
	    								 		detail.productId = r.productId;
	    								 		detail.productCode = Utils.XSSEncode(r.productCode);
	    								 		detail.productName = Utils.XSSEncode(r.productName);
    								 		}
    								 		ddv.datagrid('getRows').splice(Number(row.attr('datagrid-row-index')), 1, detail);
    								 		var textCode = PromotionProgram.returnTextProductCode(ddv.datagrid('getRows'));
    								 		var subLevel = $('#gridSublevelMua'+Utils.XSSEncode(levelCode)).datagrid('getRows')[idx];
    								 		subLevel.textCode = Utils.XSSEncode(textCode);
    								 		$('#gridSublevelMua'+Utils.XSSEncode(levelCode)).datagrid('getRows').splice(idx, 1, subLevel);
    								 		$('#gridSublevelMua'+Utils.XSSEncode(levelCode)).prev().find('.datagrid-btable tr[datagrid-row-index='+idx+'] td[field=textCode] div').html(Utils.XSSEncode(textCode));
    								 		ddv.datagrid('fixRowHeight', row.attr('datagrid-row-index'));
    								 		$('#gridSublevelMua'+Utils.XSSEncode(levelCode)).datagrid('fixRowHeight', idx);
    								 	}
	    							 }
	    						}
                            },
                            {field:'productName', sortable:false,resizable:false, title:'Tên SP',width:150,align:'left', formatter: function(value, row, index) {
    							return Utils.XSSEncode(value);
    						}},
                            {field:'quantity', sortable:false,resizable:false, title:'Số lượng', width:50, align:'right', formatter:function(value, row, index){
        						if(value == 0 || value == 'null' || VTUtilJS.isNullOrEmpty(value)) {
        							return '';
        						} else {
        							return CommonFormatter.formatNormalCell(value, row, index);
        						}
        					},editor: {
							type: 'currency',
							options: {
								isDisable: disQuantityMua,
								maxLength: 10,
								blur: function(target) {
									var row = $(target).parent().parent().parent().parent().parent().parent().parent();
									var quantity = $(target).val();
									var detail = ddv.datagrid('getRows')[Number(row.attr('datagrid-row-index'))];
									detail.quantity = quantity;
									ddv.datagrid('getRows').splice(Number(row.attr('datagrid-row-index')), 1, detail);
									var textCode = PromotionProgram.returnTextProductCode(ddv.datagrid('getRows'));
									var subLevel = $('#gridSublevelMua' + levelCode).datagrid('getRows')[idx];
									subLevel.textCode = textCode;
									$('#gridSublevelMua' + levelCode).datagrid('getRows').splice(idx, 1, subLevel);
									$('#gridSublevelMua' + levelCode).prev().find('.datagrid-btable tr[datagrid-row-index=' + idx + '] td[field=textCode] div').html(Utils.XSSEncode(textCode));
									ddv.datagrid('fixRowHeight', row.attr('datagrid-row-index'));
									$('#gridSublevelMua' + levelCode).datagrid('fixRowHeight', idx);
									var typeCodeTmp = $('#typeCode').val().trim();
									if (typeCodeTmp == 'ZV01' || typeCodeTmp == 'ZV02' || typeCodeTmp == 'ZV03') {
										subLevel.minQuantityMua = quantity;
										$('#gridSublevelMua' + levelCode).prev().find('.datagrid-btable tr[datagrid-row-index=' + idx + '] td[field=minQuantityMua] input').val(quantity);
									}
								}
							}
    						}},
    						{field:'amount', sortable:false,resizable:false, title:'Số tiền', width:50, align:'right', formatter:function(value, row, index){
        						if(value == 0 || value == 'null' || VTUtilJS.isNullOrEmpty(value)) {
        							return '';
        						} else {
        							return CommonFormatter.formatNormalCell(value, row, index);
        						}
        					},editor: {
							type: 'currency',
							options: {
								isDisable: disAmountMua,
								maxLength: 20,
								blur: function(target) {
									var row = $(target).parent().parent().parent().parent().parent().parent().parent();
									var amount = $(target).val();
									var detail = ddv.datagrid('getRows')[Number(row.attr('datagrid-row-index'))];
									detail.amount = amount;
									ddv.datagrid('getRows').splice(Number(row.attr('datagrid-row-index')), 1, detail);
									var textCode = PromotionProgram.returnTextProductCode(ddv.datagrid('getRows'));
									var subLevel = $('#gridSublevelMua' + levelCode).datagrid('getRows')[idx];
									subLevel.textCode = textCode;
									$('#gridSublevelMua' + levelCode).datagrid('getRows').splice(idx, 1, subLevel);
									$('#gridSublevelMua' + levelCode).prev().find('.datagrid-btable tr[datagrid-row-index=' + idx + '] td[field=textCode] div').html(Utils.XSSEncode(textCode));
									ddv.datagrid('fixRowHeight', row.attr('datagrid-row-index'));
									$('#gridSublevelMua' + levelCode).datagrid('fixRowHeight', idx);
									if (typeCodeTmp == 'ZV04' || typeCodeTmp == 'ZV05' || typeCodeTmp == 'ZV06') {
										subLevel.minAmountMua = amount;
										$('#gridSublevelMua' + levelCode).prev().find('.datagrid-btable tr[datagrid-row-index=' + idx + '] td[field=minAmountMua] input').val(VTUtilJS.formatCurrency(amount));
									}
								}
							}
    						}},
						{ field: 'isRequired', title: "Bắt buộc", width: 50, align: 'center', sortable: false, resizable: false,
							editor: {
								type: 'checkbox',
								options: {
									on: '1',
									off: '0',
									onCheck: function(target) {
										var row = $(target).parent().parent().parent().parent().parent().parent().parent();
										var isRequired = $(target).is(":checked") ? 1 : 0;
										var detail = ddv.datagrid('getRows')[Number(row.attr('datagrid-row-index'))];
										detail.isRequired = isRequired;
										ddv.datagrid('getRows').splice(Number(row.attr('datagrid-row-index')), 1, detail);
										var textCode = PromotionProgram.returnTextProductCode(ddv.datagrid('getRows'));
										var subLevel = $('#gridSublevelMua' + levelCode).datagrid('getRows')[idx];
										subLevel.textCode = Utils.XSSEncode(textCode);
										$('#gridSublevelMua' + levelCode).datagrid('getRows').splice(idx, 1, subLevel);
										$('#gridSublevelMua' + levelCode).prev().find('.datagrid-btable tr[datagrid-row-index=' + idx + '] td[field=textCode] div').html(Utils.XSSEncode(textCode));
										ddv.datagrid('fixRowHeight', row.attr('datagrid-row-index'));
										$('#gridSublevelMua' + levelCode).datagrid('fixRowHeight', idx);
									}
								}
							},
							formatter: function(val, rowData, idx) {
								return (val == 1 ? "x" : "");
							}
						},
						{ field: 'delete', title: '<a ' + (($('#statusHidden').val() == 0 || $('#statusHidden').val() == 1) ? 'style="display:none;"' : '') + ' onclick="PromotionProgram.addSubProductLevel(\'' + ('gridSublevelMua' + levelCode) + '\', ' + idx + ', \'' + (ddv.attr('id')) + '\')"><span style="cursor:pointer"><img title="Thêm mới nhóm mua" src="/resources/images/icon_add.png"/></span></a>',
							width: 30, align: 'center', sortable: false, resizable: false, formatter: function(value, row, index) {
							return '<a ' + (($('#statusHidden').val() == 0 || $('#statusHidden').val() == 1) ? 'style="display:none;"' : '') + ' onclick="PromotionProgram.deleteSubProductLevel(\'' + ('gridSublevelMua' + levelCode) + '\', ' + idx + ', \'' + (ddv.attr('id')) + '\'' + ', ' + index + '' + (!VTUtilJS.isNullOrEmpty(row.levelDetailId) ? ', ' + row.levelDetailId + '' : '') + ')"><span style="cursor:pointer"><img title="Xóa nhóm mua" src="/resources/images/icon_delete.png"/></span></a>';
						}}
                        ]],
                        onLoadSuccess:function(){
                        	setTimeout(function(){
                            	$('#gridSublevelMua'+levelCode).datagrid('fixRowHeight',idx);
                            	$('#gridSublevelMua'+levelCode).datagrid('fixDetailRowHeight',idx);
                            },500);
                        	var arrDelete =  $('#mappingGrid td[field="delete"]');
                			if (arrDelete != undefined && arrDelete != null && arrDelete.length > 0) {
                			  for (var i = 0, size = arrDelete.length; i < size; i++) {
                			    $(arrDelete[i]).prop("id", "tab_product_pn_change_gr_sale_c_td_del_" + i);//Khai bao id danh cho phan quyen
                				$(arrDelete[i]).addClass("cmsiscontrol");
                			  }
                			}
                        	Utils.functionAccessFillControl('mappingGrid', function(data){
        						//Xu ly cac su kien lien quan den cac control phan quyen
                        		if (arrDelete != undefined && arrDelete != null && arrDelete.length > 0) {
	                        		var arrTmpLength =  $('#mappingGrid td[id^="tab_product_pn_change_gr_sale_c_td_del_"]').length;
	                				var invisibleLenght = $('.isCMSInvisible[id^="tab_product_pn_change_gr_sale_c_td_del_"]').length;
	                				if (arrTmpLength > 0 && arrTmpLength != invisibleLenght) {
	                					$(ddv).datagrid("showColumn", "delete");
	                				} else {
	                					$(ddv).datagrid("hideColumn", "delete");
	                				}
                        		}
        					});
                        	
                        }
                    });
                },
                width: '100%',
				height: 'auto',
				singleSelect: true,
				fitColumns: true,
				scrollbarSize:0,
				//showHeader:false,
				columns: [[
					{field:'textCode', title:"Sản phẩm", width:100,align:'left',sortable:false,resizable:false,formatter:function(value, row, index) {
						return PromotionProgram.returnTextProductCode(row.listSubLevel);
					}},
					{field:'minQuantityMua', title:"SL tối thiểu", width:30,align:'right',sortable:false,resizable:false,formatter:function(value, row, index){
						if(value == 0 || value == 'null' || VTUtilJS.isNullOrEmpty(value)) {
							return '';
						} else {
							return CommonFormatter.formatNormalCell(value, row, index);
						}
					},editor: {
						type:'currency', 
						options : {
							isDisable : disMinQuantityMua,
							maxLength:10,
							blur : function(target) {
								var row = $(target).parent().parent().parent().parent().parent().parent().parent();
						 		var minQuantityMua = $(target).val();
						 		$('#gridSublevelMua'+levelCode).datagrid('getRows')[Number(row.attr('datagrid-row-index'))].minQuantityMua = minQuantityMua;
							}
						}
					}},
					{field:'minAmountMua', title:"ST tối thiểu", width:30,align:'right',sortable:false,resizable:false,formatter:function(value, row, index){
						if(value == 0 || value == 'null' || VTUtilJS.isNullOrEmpty(value)) {
							return '';
						} else {
							return CommonFormatter.formatNormalCell(value, row, index);
						}
					},editor: {
						type:'currency', 
						options : {
							isDisable : disMinAmountMua,
							maxLength:20,
							blur : function(target) {
								var row = $(target).parent().parent().parent().parent().parent().parent().parent();
						 		var minAmountMua = $(target).val();
						 		$('#gridSublevelMua'+levelCode).datagrid('getRows')[Number(row.attr('datagrid-row-index'))].minAmountMua = minAmountMua;
							}
						}
					}},
					{field:'delete', title:'<a '+(($('#statusHidden').val() == 0 || $('#statusHidden').val() == 1) ? 'style="display:none;"' : '')+' onclick="PromotionProgram.addHtmlSubRowMua(\'tableSubLevelMua'+levelCode+'\', \''+levelCode+'\')"><span style="cursor:pointer"><img title="Thêm mới mức" src="/resources/images/icon_add.png"/></span></a>', width:10,align:'center',sortable:false,resizable:false,formatter:function(value,row,index){
						return '<a '+(($('#statusHidden').val() == 0 || $('#statusHidden').val() == 1) ? 'style="display:none;"' : '')+' onclick="PromotionProgram.deleteSubLevel(\''+'gridSublevelMua'+levelCode+'\', '+index+''+(!VTUtilJS.isNullOrEmpty(row.levelId) ? ', '+row.levelId+'' : '')+')"><span style="cursor:pointer"><img title="Xóa nhóm mua" src="/resources/images/icon_delete.png"/></span></a>';
					}}
				]],
				onLoadSuccess: function(data) {
					if(data.rows != undefined && $.isArray(data.rows)) {
						for(var i = 0; i < data.rows.length; i++) {
							$('#gridSublevelMua'+levelCode).datagrid('beginEdit', i);
						}
					}
					var arrDelete =  $('#mappingGrid td[field="delete"]');
        			if (arrDelete != undefined && arrDelete != null && arrDelete.length > 0) {
        			  for (var i = 0, size = arrDelete.length; i < size; i++) {
        			    $(arrDelete[i]).prop("id", "tab_product_pn_change_gr_sale_td_del_" + i);//Khai bao id danh cho phan quyen
        				$(arrDelete[i]).addClass("cmsiscontrol");
        			  }
        			}
                	Utils.functionAccessFillControl('mappingGrid', function(data){
						//Xu ly cac su kien lien quan den cac control phan quyen
                		if (arrDelete != undefined && arrDelete != null && arrDelete.length > 0) {
	                		var arrTmpLength =  $('#mappingGrid td[id^="tab_product_pn_change_gr_sale_td_del_"]').length;
	        				var invisibleLenght = $('.isCMSInvisible[id^="tab_product_pn_change_gr_sale_td_del_"]').length;
	        				if (arrTmpLength > 0 && arrTmpLength != invisibleLenght) {
	        					$('#gridSublevelMua'+levelCode).datagrid("showColumn", "delete");
	        				} else {
	        					$('#gridSublevelMua'+levelCode).datagrid("hideColumn", "delete");
	        				}
                		}
					});
				}
			});
		}
	},
	addHtmlSubRowKM : function(divTable, levelCode, subLevel) {
		$('.ErrorMsgStyle').hide();
		if($('#'+divTable+' tbody tr').length > 1) {
			if(($('#gridSublevelKM'+levelCode).length > 0 && $('#gridSublevelKM'+levelCode).datagrid('getRows').length > 0) && ($('#typeCode').val() == 'ZV01'
				|| $('#typeCode').val() == 'ZV02'
					|| $('#typeCode').val() == 'ZV03'
					|| $('#typeCode').val() == 'ZV04'
					|| $('#typeCode').val() == 'ZV05'
					|| $('#typeCode').val() == 'ZV06'
					|| $('#typeCode').val() == 'ZV07'
					|| $('#typeCode').val() == 'ZV08'
					|| $('#typeCode').val() == 'ZV09'
					|| $('#typeCode').val() == 'ZV10'
					|| $('#typeCode').val() == 'ZV11'
					|| $('#typeCode').val() == 'ZV12'
					|| $('#typeCode').val() == 'ZV13'
					|| $('#typeCode').val() == 'ZV14'
					|| $('#typeCode').val() == 'ZV15'
					|| $('#typeCode').val() == 'ZV16'
					|| $('#typeCode').val() == 'ZV17'
					|| $('#typeCode').val() == 'ZV18'
					|| $('#typeCode').val() == 'ZV19'
					|| $('#typeCode').val() == 'ZV20'
					|| $('#typeCode').val() == 'ZV21')) { 
				return;
			}
			//da co grid roi
			var rows = $('#gridSublevelKM'+levelCode).datagrid('getRows');
			rows.push({});
			$('#gridSublevelKM'+levelCode).datagrid('loadData', rows);
		} else {
			//chua co grid
			$('#'+divTable+' tbody').append('<tr>\
					<td colspan="5"><div id="gridSublevelKM'+levelCode+'" style="width:100%; height:\'auto\'; padding:10px;"></div></td>\
					</tr>');
			if(subLevel == undefined || subLevel == null || !$.isArray(subLevel) || subLevel.length == 0) {
				subLevel = [{listSubLevel:[]}];
			}
			var disMinQuantityMua = false;
			var disMinAmountMua = false;
			var disQuantityMua = false;
			var disAmountMua = false;
			var disPercentKM = false;
			var disMaxQuantityKM = false;
			var disMaxAmountKM = false;
			var disQuantityKM = false;
			var disAmountKM = false;
			if($('#typeCode').val() == 'ZV01') {
				disMinQuantityMua = true;
				disMinAmountMua = true;
				disQuantityMua = false;
				disAmountMua = true;
				disPercentKM = false;
				disMaxQuantityKM = true;
				disMaxAmountKM = true;
				disQuantityKM = true;
				disAmountKM = true;
			} else if($('#typeCode').val() == 'ZV02') {
				disMinQuantityMua = true;
				disMinAmountMua = true;
				disQuantityMua = false;
				disAmountMua = true;
				disPercentKM = true;
				disMaxQuantityKM = true;
				disMaxAmountKM = false;
				disQuantityKM = true;
				disAmountKM = true;
			} else if($('#typeCode').val() == 'ZV03') {
				disMinQuantityMua = true;
				disMinAmountMua = true;
				disQuantityMua = false;
				disAmountMua = true;
				disPercentKM = true;
				disMaxQuantityKM = true;
				disMaxAmountKM = true;
				disQuantityKM = false;
				disAmountKM = true;
			} else if($('#typeCode').val() == 'ZV04') {
				disMinQuantityMua = true;
				disMinAmountMua = true;
				disQuantityMua = true;
				disAmountMua = false;
				disPercentKM = false;
				disMaxQuantityKM = true;
				disMaxAmountKM = true;
				disQuantityKM = true;
				disAmountKM = true;
			} else if($('#typeCode').val() == 'ZV05') {
				disMinQuantityMua = true;
				disMinAmountMua = true;
				disQuantityMua = true;
				disAmountMua = false;
				disPercentKM = true;
				disMaxQuantityKM = true;
				disMaxAmountKM = false;
				disQuantityKM = true;
				disAmountKM = true;
			} else if($('#typeCode').val() == 'ZV06') {
				disMinQuantityMua = true;
				disMinAmountMua = true;
				disQuantityMua = true;
				disAmountMua = false;
				disPercentKM = true;
				disMaxQuantityKM = true;
				disMaxAmountKM = true;
				disQuantityKM = false;
				disAmountKM = true;
			} else if($('#typeCode').val() == 'ZV07') {
				disMinQuantityMua = false;
				disMinAmountMua = true;
				disQuantityMua = true;
				disAmountMua = true;
				disPercentKM = false;
				disMaxQuantityKM = true;
				disMaxAmountKM = true;
				disQuantityKM = true;
				disAmountKM = true;
			} else if($('#typeCode').val() == 'ZV08') {
				disMinQuantityMua = false;
				disMinAmountMua = true;
				disQuantityMua = true;
				disAmountMua = true;
				disPercentKM = true;
				disMaxQuantityKM = true;
				disMaxAmountKM = false;
				disQuantityKM = true;
				disAmountKM = true;
			} else if($('#typeCode').val() == 'ZV09') {
				disMinQuantityMua = false;
				disMinAmountMua = true;
				disQuantityMua = true;
				disAmountMua = true;
				disPercentKM = true;
				disMaxQuantityKM = true;
				disMaxAmountKM = true;
				disQuantityKM = false;
				disAmountKM = true;
			} else if($('#typeCode').val() == 'ZV10') {
				disMinQuantityMua = true;
				disMinAmountMua = false;
				disQuantityMua = true;
				disAmountMua = true;
				disPercentKM = false;
				disMaxQuantityKM = true;
				disMaxAmountKM = true;
				disQuantityKM = true;
				disAmountKM = true;
			} else if($('#typeCode').val() == 'ZV11') {
				disMinQuantityMua = true;
				disMinAmountMua = false;
				disQuantityMua = true;
				disAmountMua = true;
				disPercentKM = true;
				disMaxQuantityKM = true;
				disMaxAmountKM = false;
				disQuantityKM = true;
				disAmountKM = true;
			} else if($('#typeCode').val() == 'ZV12') {
				disMinQuantityMua = true;
				disMinAmountMua = false;
				disQuantityMua = true;
				disAmountMua = true;
				disPercentKM = true;
				disMaxQuantityKM = true;
				disMaxAmountKM = true;
				disQuantityKM = false;
				disAmountKM = true;
			} else if($('#typeCode').val() == 'ZV13') {
				disMinQuantityMua = true;
				disMinAmountMua = true;
				disQuantityMua = false;
				disAmountMua = true;
				disPercentKM = false;
				disMaxQuantityKM = true;
				disMaxAmountKM = true;
				disQuantityKM = true;
				disAmountKM = true;
			} else if($('#typeCode').val() == 'ZV14') {
				disMinQuantityMua = true;
				disMinAmountMua = true;
				disQuantityMua = false;
				disAmountMua = true;
				disPercentKM = true;
				disMaxQuantityKM = true;
				disMaxAmountKM = false;
				disQuantityKM = true;
				disAmountKM = true;
			} else if($('#typeCode').val() == 'ZV15') {
				disMinQuantityMua = true;
				disMinAmountMua = true;
				disQuantityMua = false;
				disAmountMua = true;
				disPercentKM = true;
				disMaxQuantityKM = true;
				disMaxAmountKM = true;
				disQuantityKM = false;
				disAmountKM = true;
			} else if($('#typeCode').val() == 'ZV16') {
				disMinQuantityMua = true;
				disMinAmountMua = true;
				disQuantityMua = true;
				disAmountMua = false;
				disPercentKM = false;
				disMaxQuantityKM = true;
				disMaxAmountKM = true;
				disQuantityKM = true;
				disAmountKM = true;
			} else if($('#typeCode').val() == 'ZV17') {
				disMinQuantityMua = true;
				disMinAmountMua = true;
				disQuantityMua = true;
				disAmountMua = false;
				disPercentKM = true;
				disMaxQuantityKM = true;
				disMaxAmountKM = false;
				disQuantityKM = true;
				disAmountKM = true;
			} else if($('#typeCode').val() == 'ZV18') {
				disMinQuantityMua = true;
				disMinAmountMua = true;
				disQuantityMua = true;
				disAmountMua = false;
				disPercentKM = true;
				disMaxQuantityKM = true;
				disMaxAmountKM = true;
				disQuantityKM = false;
				disAmountKM = true;
			} else if($('#typeCode').val() == 'ZV19') {
				disMinQuantityMua = true;
				disMinAmountMua = false;
				disQuantityMua = true;
				disAmountMua = true;
				disPercentKM = false;
				disMaxQuantityKM = true;
				disMaxAmountKM = true;
				disQuantityKM = true;
				disAmountKM = true;
			} else if($('#typeCode').val() == 'ZV20') {
				disMinQuantityMua = true;
				disMinAmountMua = false;
				disQuantityMua = true;
				disAmountMua = true;
				disPercentKM = true;
				disMaxQuantityKM = true;
				disMaxAmountKM = false;
				disQuantityKM = true;
				disAmountKM = true;
			} else if($('#typeCode').val() == 'ZV21') {
				disMinQuantityMua = true;
				disMinAmountMua = false;
				disQuantityMua = true;
				disAmountMua = true;
				disPercentKM = true;
				disMaxQuantityKM = true;
				disMaxAmountKM = true;
				disQuantityKM = false;
				disAmountKM = true;
			}
			$('#gridSublevelKM'+levelCode).datagrid({	
				data : subLevel,
				view: detailview,
                detailFormatter:function(idx,r){
                    return '<div class="ddv" id="'+levelCode+'_subGridKMProduct_'+idx+'" style="padding:5px 0"></div>';
                },
                onExpandRow: function(idx,r){
                    var ddv = $(this).datagrid('getRowDetail', idx).find('div.ddv#'+levelCode+'_subGridKMProduct_'+idx);
                    ddv.datagrid({
                    	data:r.listSubLevel,
                        fitColumns:true,
                        singleSelect:true,
                        rownumbers:true,
                        scrollbarSize:0,
                        height:'auto',
                        onDblClickRow : function(index,row) {
                        	for(var i = 0; i < ddv.datagrid('getRows').length; i++) {
                    			var row = ddv.datagrid('getRows')[i];
                    			if(VTUtilJS.isNullOrEmpty(row.productCode)) {
                    				return;
                    			}
                    			if(($('#typeCode').val() == 'ZV03' || $('#typeCode').val() == 'ZV06' || $('#typeCode').val() == 'ZV09' || $('#typeCode').val() == 'ZV12' || $('#typeCode').val() == 'ZV15' || $('#typeCode').val() == 'ZV18' || $('#typeCode').val() == 'ZV21') &&
                    					(VTUtilJS.isNullOrEmpty(row.quantity) || isNaN(row.quantity) || Number(row.quantity) == 0) && 
                    					(VTUtilJS.isNullOrEmpty(row.amount) || isNaN(row.amount) || Number(row.amount) == 0)
                    					) {
                    				return;
                    			}
                    		}
                    		ddv.datagrid('acceptChanges');
                        	ddv.datagrid('beginEdit', index);
                        },
                        columns:[[
                            {field:'productCode', sortable:false,resizable:false, title:'Mã SP', width:80, align:'left', formatter: function(value, row, index) {
    							return Utils.XSSEncode(value);
    						},
                            	editor: {
	    							type:'combobox',
	    							 options:{
    								 	valueField : 'productCode',
    								 	textField : 'productCode',
    								 	data : PromotionProgram._listProduct.valArray,
    								 	formatter:function(row){
    			                            return Utils.XSSEncode(row.productCode + ' - ' +row.productName);
    			                        },
    			                        onHidePanel: function(){
    			                        	var input = $(this).parent().find("input.combo-text.validatebox-text");
    			                        	var r = PromotionProgram._listProduct.get(input.val());
    			                        	var row = $(this).parent().parent().parent().parent().parent().parent().parent();
    								 		row.find('td[field=productName] div').html(r.productName);
								 			var detail = ddv.datagrid('getRows')[Number(row.attr('datagrid-row-index'))];
								 			detail.productId = Utils.XSSEncode(r.productId);
								 			detail.productCode = Utils.XSSEncode(r.productCode);
								 			detail.productName = Utils.XSSEncode(r.productName);
								 			ddv.datagrid('getRows').splice(Number(row.attr('datagrid-row-index')), 1, detail);
								 			var textCode = PromotionProgram.returnTextProductCode(ddv.datagrid('getRows'));
								 			var subLevel = $('#gridSublevelKM'+levelCode).datagrid('getRows')[idx];
								 			subLevel.textCode = Utils.XSSEncode(textCode);
								 			$('#gridSublevelKM'+levelCode).datagrid('getRows').splice(idx, 1, subLevel);
								 			$('#gridSublevelKM'+levelCode).prev().find('.datagrid-btable tr[datagrid-row-index='+idx+'] td[field=textCode] div').html(Utils.XSSEncode(textCode));
								 			ddv.datagrid('fixRowHeight', row.attr('datagrid-row-index'));
								 			$('#gridSublevelKM'+levelCode).datagrid('fixRowHeight', idx);
								 		}
								 	}    								 	
	    						}
                            },
                            {field:'productName', sortable:false,resizable:false, title:'Tên SP',width:150,align:'left', formatter: function(value, row, index) {
    							return Utils.XSSEncode(value);
    						}},
                            {field:'quantity', sortable:false,resizable:false, title:'Số lượng', width:50, align:'right', formatter:function(value, row, index){
        						if(value == 0 || value == 'null' || VTUtilJS.isNullOrEmpty(value)) {
        							return '';
        						} else {
        							return CommonFormatter.formatNormalCell(value, row, index);
        						}
        					},editor: {
    							type:'currency', 
    							options : {
    								isDisable : disQuantityKM,
    								maxLength:10,
    								blur : function(target) {
    									var row = $(target).parent().parent().parent().parent().parent().parent().parent();
								 		var quantity = $(target).val();
								 		var detail = ddv.datagrid('getRows')[Number(row.attr('datagrid-row-index'))];
								 		detail.quantity = quantity;
								 		ddv.datagrid('getRows').splice(Number(row.attr('datagrid-row-index')), 1, detail);
								 		var textCode = PromotionProgram.returnTextProductCode(ddv.datagrid('getRows'));
								 		var subLevel = $('#gridSublevelKM'+levelCode).datagrid('getRows')[idx];
								 		subLevel.textCode = Utils.XSSEncode(textCode);
								 		$('#gridSublevelKM'+levelCode).datagrid('getRows').splice(idx, 1, subLevel);
								 		$('#gridSublevelKM'+levelCode).prev().find('.datagrid-btable tr[datagrid-row-index='+idx+'] td[field=textCode] div').html(Utils.XSSEncode(textCode));
								 		ddv.datagrid('fixRowHeight', row.attr('datagrid-row-index'));
								 		$('#gridSublevelKM'+levelCode).datagrid('fixRowHeight', idx);
    								}
    							}
    						}},
    						{field:'amount', sortable:false,resizable:false, title:'Số tiền', width:50, align:'right', formatter:function(value, row, index){
        						if(value == 0 || value == 'null' || VTUtilJS.isNullOrEmpty(value)) {
        							return '';
        						} else {
        							return CommonFormatter.formatNormalCell(value, row, index);
        						}
        					},editor: {
    							type:'currency', 
    							options : {
    								isDisable : disAmountKM,
    								maxLength:20,
    								blur : function(target) {
    									var row = $(target).parent().parent().parent().parent().parent().parent().parent();
								 		var amount = $(target).val();
								 		var detail = ddv.datagrid('getRows')[Number(row.attr('datagrid-row-index'))];
								 		detail.amount = amount;
								 		ddv.datagrid('getRows').splice(Number(row.attr('datagrid-row-index')), 1, detail);
								 		var textCode = PromotionProgram.returnTextProductCode(ddv.datagrid('getRows'));
								 		var subLevel = $('#gridSublevelKM'+levelCode).datagrid('getRows')[idx];
								 		subLevel.textCode = Utils.XSSEncode(textCode);
								 		$('#gridSublevelKM'+levelCode).datagrid('getRows').splice(idx, 1, subLevel);
								 		$('#gridSublevelKM'+levelCode).prev().find('.datagrid-btable tr[datagrid-row-index='+idx+'] td[field=textCode] div').html(Utils.XSSEncode(textCode));
								 		ddv.datagrid('fixRowHeight', row.attr('datagrid-row-index'));
								 		$('#gridSublevelKM'+levelCode).datagrid('fixRowHeight', idx);
    								}
    							}
    						}},
    						{field:'isRequired', title:"Bắt buộc", width:50,align:'center',sortable:false,resizable:false,
    							editor:{type:'checkbox', options:{on:'1', off:'0', onCheck:function(target) {
    								var row = $(target).parent().parent().parent().parent().parent().parent().parent();
							 		var isRequired = $(target).is(":checked") ? 1 : 0;
							 		var detail = ddv.datagrid('getRows')[Number(row.attr('datagrid-row-index'))];
							 		detail.isRequired = isRequired;
							 		ddv.datagrid('getRows').splice(Number(row.attr('datagrid-row-index')), 1, detail);
							 		var textCode = PromotionProgram.returnTextProductCode(ddv.datagrid('getRows'));
							 		var subLevel = $('#gridSublevelKM'+levelCode).datagrid('getRows')[idx];
							 		subLevel.textCode = textCode;
							 		$('#gridSublevelKM'+levelCode).datagrid('getRows').splice(idx, 1, subLevel);
							 		$('#gridSublevelKM'+levelCode).prev().find('.datagrid-btable tr[datagrid-row-index='+idx+'] td[field=textCode] div').html(Utils.XSSEncode(textCode));
							 		ddv.datagrid('fixRowHeight', row.attr('datagrid-row-index'));
							 		$('#gridSublevelKM'+levelCode).datagrid('fixRowHeight', idx);
    							}
    						}}},
    						{field:'delete', title:'<a '+(($('#statusHidden').val() == 0 || $('#statusHidden').val() == 1) ? 'style="display:none;"' : '')+' onclick="PromotionProgram.addSubProductLevel(\''+('gridSublevelKM'+levelCode)+'\', '+idx+', \''+(ddv.attr('id'))+'\')"><span style="cursor:pointer"><img title="Thêm mới nhóm mua" src="/resources/images/icon_add.png"/></span></a>', width:30,align:'center',sortable:false,resizable:false,formatter:function(value,row,index){
    							return '<a '+(($('#statusHidden').val() == 0 || $('#statusHidden').val() == 1) ? 'style="display:none;"' : '')+' onclick="PromotionProgram.deleteSubProductLevel(\''+('gridSublevelKM'+levelCode)+'\', '+idx+', \''+(ddv.attr('id'))+'\''+', '+index+''+(!VTUtilJS.isNullOrEmpty(row.levelDetailId) ? ', '+row.levelDetailId+'' : '')+')"><span style="cursor:pointer"><img title="Xóa nhóm mua" src="/resources/images/icon_delete.png"/></span></a>';
    						}}
                        ]],
                        onLoadSuccess:function(){
                        	setTimeout(function(){
                            	$('#gridSublevelKM'+levelCode).datagrid('fixRowHeight',idx);
                            	$('#gridSublevelKM'+levelCode).datagrid('fixDetailRowHeight',idx);
                            }, 500);
                        	var arrDelete =  $('#mappingGrid td[field="delete"]');
                			if (arrDelete != undefined && arrDelete != null && arrDelete.length > 0) {
                			  for (var i = 0, size = arrDelete.length; i < size; i++) {
                			    $(arrDelete[i]).prop("id", "tab_product_pn_change_gr_km_c_td_del_" + i);//Khai bao id danh cho phan quyen
                				$(arrDelete[i]).addClass("cmsiscontrol");
                			  }
                			}
                        	Utils.functionAccessFillControl('mappingGrid', function(data){
                        		if (arrDelete != undefined && arrDelete != null && arrDelete.length > 0) {
                        			//Xu ly cac su kien lien quan den cac control phan quyen
                        			var arrTmpLength =  $('#mappingGrid td[id^="tab_product_pn_change_gr_km_c_td_del_"]').length;
                        			var invisibleLenght = $('.isCMSInvisible[id^="tab_product_pn_change_gr_km_c_td_del_"]').length;
                        			if (arrTmpLength > 0 && arrTmpLength != invisibleLenght) {
                        				$(ddv).datagrid("showColumn", "delete");
                        			} else {
                        				$(ddv).datagrid("hideColumn", "delete");
                        			}
        						}
        					});
                        }
                    });
                },
                width: '100%',
				height: 'auto',
				singleSelect: true,
				fitColumns: true,
				scrollbarSize:0,
				columns: [[
					{field:'textCode', title:"Sản phẩm", width:100,align:'left',sortable:false,resizable:false,formatter:function(value, row, index) {
						return PromotionProgram.returnTextProductCode(row.listSubLevel);
					}},
					{field:'percent', title:"% KM", width:20,align:'right',sortable:false,resizable:false,formatter:function(value, row, index){
						if(value == 0 || value == 'null' || VTUtilJS.isNullOrEmpty(value)) {
							return '';
						} else {
							return CommonFormatter.formatNormalCell(value, row, index);
						}
					},editor: {
						type:'percent',
						options : {
							isDisable : disPercentKM,
							maxLength:5,
							blur : function(target) {
								var row = $(target).parent().parent().parent().parent().parent().parent().parent();
						 		var percent = $(target).val();
						 		var detail = $('#gridSublevelKM'+levelCode).datagrid('getRows')[Number(row.attr('datagrid-row-index'))];
						 		detail.percent = percent;
						 		$('#gridSublevelKM'+levelCode).datagrid('getRows').splice(Number(row.attr('datagrid-row-index')), 1, detail);
							}
						}
					}},
					{field:'maxQuantityKM', title:"SL tối da", width:30,align:'right',sortable:false,resizable:false,formatter:function(value, row, index){
						if(value == 0 || value == 'null' || VTUtilJS.isNullOrEmpty(value)) {
							return '';
						} else {
							return CommonFormatter.formatNormalCell(value, row, index);
						}
					},editor: {
						type:'currency', 
						options : {
							isDisable : disMaxQuantityKM,
							maxLength:10,
							blur : function(target) {
								var row = $(target).parent().parent().parent().parent().parent().parent().parent();
						 		var maxQuantityKM = $(target).val();
						 		var detail = $('#gridSublevelKM'+levelCode).datagrid('getRows')[Number(row.attr('datagrid-row-index'))];
						 		detail.maxQuantityKM = maxQuantityKM;
						 		$('#gridSublevelKM'+levelCode).datagrid('getRows').splice(Number(row.attr('datagrid-row-index')), 1, detail);
							}
						}
					}},
					{field:'maxAmountKM', title:"ST tối da", width:30,align:'right',sortable:false,resizable:false,formatter:function(value, row, index){
						if(value == 0 || value == 'null' || VTUtilJS.isNullOrEmpty(value)) {
							return '';
						} else {
							return CommonFormatter.formatNormalCell(value, row, index);
						}
					},editor: {
						type:'currency', 
						options : {
							isDisable : disMaxAmountKM,
							maxLength:20,
							blur : function(target) {
								var row = $(target).parent().parent().parent().parent().parent().parent().parent();
						 		var maxAmountKM = $(target).val();
						 		var detail = $('#gridSublevelKM'+levelCode).datagrid('getRows')[Number(row.attr('datagrid-row-index'))];
						 		detail.maxAmountKM = maxAmountKM;
						 		$('#gridSublevelKM'+levelCode).datagrid('getRows').splice(Number(row.attr('datagrid-row-index')), 1, detail);
							}
						}
					}},
					{field:'delete', title:'<a '+(($('#statusHidden').val() == 0 || $('#statusHidden').val() == 1) ? 'style="display:none;"' : '')+' onclick="PromotionProgram.addHtmlSubRowKM(\'tableSubLevelKM'+levelCode+'\', \''+levelCode+'\')"><span style="cursor:pointer"><img title="Thêm mới mức" src="/resources/images/icon_add.png"/></span></a>', width:10,align:'center',sortable:false,resizable:false,formatter:function(value,row,index){
						return '<a '+(($('#statusHidden').val() == 0 || $('#statusHidden').val() == 1) ? 'style="display:none;"' : '')+' onclick="PromotionProgram.deleteSubLevel(\''+'gridSublevelKM'+levelCode+'\', '+index+''+(!VTUtilJS.isNullOrEmpty(row.levelId) ? ', '+row.levelId+'' : '')+')"><span style="cursor:pointer"><img title="Xóa nhóm mua" src="/resources/images/icon_delete.png"/></span></a>';
					}}
				]],
				onLoadSuccess: function(data) {
					if(data.rows != null && data.rows != undefined) {
						for(var t = 0; t < data.rows.length; t++) {
							$('#gridSublevelKM'+levelCode).datagrid('beginEdit', t);
						}
					}
					var arrDelete =  $('#mappingGrid td[field="delete"]');
        			if (arrDelete != undefined && arrDelete != null && arrDelete.length > 0) {
        			  for (var i = 0, size = arrDelete.length; i < size; i++) {
        			    $(arrDelete[i]).prop("id", "tab_product_pn_change_gr_km_td_del_" + i);//Khai bao id danh cho phan quyen
        				$(arrDelete[i]).addClass("cmsiscontrol");
        			  }
        			}
                	Utils.functionAccessFillControl('mappingGrid', function(data){
						//Xu ly cac su kien lien quan den cac control phan quyen
                		if (arrDelete != undefined && arrDelete != null && arrDelete.length > 0) {
	                		var arrTmpLength =  $('#mappingGrid td[id^="tab_product_pn_change_gr_km_td_del_"]').length;
	        				var invisibleLenght = $('.isCMSInvisible[id^="tab_product_pn_change_gr_km_td_del_"]').length;
	        				if (arrTmpLength > 0 && arrTmpLength != invisibleLenght) {
	        					$('#gridSublevelKM'+levelCode).datagrid("showColumn", "delete");
	        				} else {
	        					$('#gridSublevelKM'+levelCode).datagrid("hideColumn", "delete");
	        				}
                		}
					});
				}
			});
		}
	},
	renderPannel : function(list, i, groupMuaId, groupKMId) {
		if(list.length == i) {
			PromotionProgram.isFinishLoad = true;
			return;
		} 
		if($('#tableLevel' + (list[i].mapId)).length == 0) {
			$($('#mappingGrid tbody')[0]).append('<tr>\
					<td>\
					<p class="ErrorMsgStyle SpriteErr" style="display: none;" id="errorMsgDistribute'+(VTUtilJS.isNullOrEmpty(list[i].levelCode) ? list[i].mapId : list[i].levelCode+list[i].mapId)+'">\
					<p id="successMsgDistribute'+(VTUtilJS.isNullOrEmpty(list[i].levelCode) ? list[i].mapId : list[i].levelCode+list[i].mapId)+'" class="SuccessMsgStyle" style="display: none"></p>\
					<div class="TableLevel" id="tableLevel'+(list[i].mapId)+'" style="width:100%; height:\'auto\';padding:10px;" title="">\
					<table style="width: 100%;">\
			    	<tr>\
			    		<td id="tdTableSubLevelMua" style="width: 50%; vertical-align: top;">\
			    			<table id="tableSubLevelMua'+(VTUtilJS.isNullOrEmpty(list[i].levelCode) ? list[i].mapId : Utils.XSSEncode(list[i].levelCode) +list[i].mapId)+'" style="border: 1px solid black; vertical-align: top;width: 100%">\
			    				<tbody>\
			    				</tbody>\
			    			</table>\
			    		</td>\
			    		<td id="tdTableSubLevelKM" style="width: 50%; vertical-align: top;">\
			    			<table id="tableSubLevelKM'+(VTUtilJS.isNullOrEmpty(list[i].levelCode) ? list[i].mapId : Utils.XSSEncode(list[i].levelCode) + list[i].mapId)+'" style="border: 1px solid black; vertical-align: top;width: 100%">\
			    				<tbody>\
			    				</tbody>\
			    			</table>\
			    		</td>\
			    	</tr>\
				</table>\
					    </div>\
				</td>\
			</tr>');
			VTUtilJS.bindFormatOnTextfield('sttMua_'+(VTUtilJS.isNullOrEmpty(list[i].levelCode) ? list[i].mapId : Utils.XSSEncode(list[i].levelCode) + list[i].mapId), VTUtilJS._TF_NUMBER);
			
			$('#tableLevel'+(list[i].mapId)).panel({
				width:'95%',
				height:'auto',
				collapsible:true,
				title: VTUtilJS.isNullOrEmpty(list[i].levelCode) ? 'STT : ' +list[i].stt : 'Mã : ' + Utils.XSSEncode(list[i].levelCode) + ' - ' + 'STT : ' + list[i].stt,
				tools:[{
					iconCls:'iconAdd '+i+'',
					handler:function(){
						var i = Number(Number($(this).attr('class').split(' ')[1]));
						PromotionProgram.addHtmlLevel('tableLevel'+(list[i].mapId)+'',groupMuaId, groupKMId);
					}
				}, {
					iconCls:'iconEdit '+i+'',
					handler:function(){
						var i = Number($(this).attr('class').split(' ')[1]);
						PromotionProgram.editHtmlLevel('tableLevel'+(list[i].mapId), groupMuaId, groupKMId, list[i].mapId, list[i].levelMuaId, list[i].levelKMId, (VTUtilJS.isNullOrEmpty(list[i].levelCode) ? '' : list[i].levelCode), (list[i].stt == null || list[i].stt == undefined) ? 0 : list[i].stt);
					}
				}, {
					iconCls:'iconDelete '+i+'',
					handler:function(){
						var i = Number($(this).attr('class').split(' ')[1]);
						PromotionProgram.deleteHtmlLevel('tableLevel'+(list[i].mapId), groupMuaId, groupKMId, list[i].mapId, list[i].levelMuaId, list[i].levelKMId);
					}
				}, {
					iconCls:'iconCopy '+i+'',
					handler:function(){
						var i = Number($(this).attr('class').split(' ')[1]);
						PromotionProgram.copyHtmlLevel(list[i].mapId, groupMuaId, groupKMId);
					}
				}, {
					iconCls:'iconSave '+i+'',
					handler:function(){
						var i = Number($(this).attr('class').split(' ')[1]);
						PromotionProgram.saveHtmlLevel((VTUtilJS.isNullOrEmpty(list[i].levelCode) ? list[i].mapId : list[i].levelCode+list[i].mapId), groupMuaId, groupKMId, list[i].mapId, list[i].levelMuaId, list[i].levelKMId);
					}
				}],
				onOpen: function(){
					$('a.iconAdd').attr('title', 'Thêm mới');
					$('a.iconEdit').attr('title', 'Chỉnh sửa');
					$('a.iconDelete').attr('title', 'Xóa');
					$('a.iconCopy').attr('title', 'Sao chép');
					$('a.iconSave').attr('title', 'Lưu');
				}
			});
			if(list[i].listExLevelMua != null && list[i].listExLevelMua != undefined && $.isArray(list[i].listExLevelMua)) {
				PromotionProgram.addHtmlSubRowMua('tableSubLevelMua'+(VTUtilJS.isNullOrEmpty(list[i].levelCode) ? list[i].mapId : Utils.XSSEncode(list[i].levelCode) + list[i].mapId), VTUtilJS.isNullOrEmpty(list[i].levelCode) ? list[i].mapId : list[i].levelCode+list[i].mapId, list[i].listExLevelMua);
			}
			if(list[i].listExLevelKM != null && list[i].listExLevelKM != undefined && $.isArray(list[i].listExLevelKM)) {
				PromotionProgram.addHtmlSubRowKM('tableSubLevelKM'+(VTUtilJS.isNullOrEmpty(list[i].levelCode) ? list[i].mapId : Utils.XSSEncode(list[i].levelCode) + list[i].mapId), VTUtilJS.isNullOrEmpty(list[i].levelCode) ? list[i].mapId : list[i].levelCode+list[i].mapId, list[i].listExLevelKM);
			}
		}
		PromotionProgram.renderPannel(list, ++i, groupMuaId, groupKMId);	
	},
	clickTabAddLevel : function(groupMuaId, groupKMId, productGroupCode, productGroupName) {
		PromotionProgram.fromLevel = 1;
		PromotionProgram.toLevel = 5;
		PromotionProgram.isEndLoad = false;
		PromotionProgram.isFinishLoad = true;
		PromotionProgram.tabAddLevel(groupMuaId, groupKMId, Utils.XSSEncode(productGroupCode), Utils.XSSEncode(productGroupName));
	},
	tabAddLevel : function(groupMuaId, groupKMId, productGroupCode, productGroupName) {
		$('#kmLevel').show();
		$('#cocauKM').html('Cơ cấu khuyến mại nhóm ' + Utils.XSSEncode(productGroupCode) + ' - ' + Utils.XSSEncode(productGroupName));
		PromotionProgram.groupMuaId = groupMuaId;
		PromotionProgram.groupKMId = groupKMId;
		$('.TableLevel').panel('destroy');
		$('#mappingGrid tbody').html('');
		var params = {
			groupMuaId: groupMuaId,
			groupKMId: groupKMId,
			fromLevel: PromotionProgram.fromLevel,
			toLevel: PromotionProgram.toLevel
		};
		VTUtilJS.getFormJson(params, '/promotion/new-list-map-level', function(data) {
			if (PromotionProgram.isEndLoad || !PromotionProgram.isFinishLoad) {
				return;
			}
			if ((data.isNew != null && data.isNew != undefined && !data.isNew) && data.listNewMapping != null && data.listNewMapping != undefined && $.isArray(data.listNewMapping) && data.listNewMapping.length == 0) {
				PromotionProgram.isEndLoad = true;
				return;
			}
			if (data.listNewMapping != null && data.listNewMapping != undefined && $.isArray(data.listNewMapping) && data.listNewMapping.length > 0) {
				$.messager.show({
			        title:'Cơ cấu CTKM',
			        msg:'Bạn đang xem cơ cấu CTKM từ mức <span style="color:red; font-weight: bold;">' + Utils.XSSEncode(params.fromLevel) + '</span> đến mức <span style="color:red; font-weight: bold;">' + (params.fromLevel + data.listNewMapping.length-1) + '</span>',
			        timeout:4000,
			        showType:'slide'
			    });
				PromotionProgram.fromLevel = Number(params.toLevel) + 1;
				PromotionProgram.toLevel = Number(params.toLevel) + Number(data.listNewMapping.length);
			}
			if (data.isNew) {
				if ($.isArray(data.listNewMapping) && data.listNewMapping.length == 0) {
					$($('#mappingGrid tbody')[0]).append('<tr class="NewLevel">\
								<td>\
								<div class="TableLevel" id="tableLevel" style="width:100%; height:\'auto\';padding:10px;" title="">\
						    </div>\
						</td>\
					</tr>');
					$('#tableLevel').panel({
						width:'95%',
						height:'auto',
						collapsible:true,
						title:' ',
						tools:[{
							iconCls:'iconAdd',
							handler:function(){
								PromotionProgram.addHtmlLevel('tableLevel', groupMuaId, groupKMId);
							}
						}],
						onLoad:function(){
							
						}
					});
				}
			} else {
				PromotionProgram.renderPannel(data.listNewMapping, 0, groupMuaId, groupKMId);
			}
			var arrPanel = $('.panel-tool');
			if (arrPanel != undefined && arrPanel != null && arrPanel.length > 0) {
				for (var i = 0, size = arrPanel.length; i < size; i++) {
					$(arrPanel[i]).prop("id", "tab_product_pn_change_hd_" + i);//Khai bao id danh cho phan quyen
					$(arrPanel[i]).addClass("cmsiscontrol");	
				}
			}
			Utils.functionAccessFillControl('divTab', function(data) {
				//Xu ly cac su kien lien quan den ctl phan quyen
			});
		});
	},
	returnTextProductCode : function(listSubLevel) {
		var textCode = '';
		if(listSubLevel != null && listSubLevel != undefined && $.isArray(listSubLevel) && listSubLevel.length > 0) {
			for(var i = 0; i < listSubLevel.length; i++) {
				if(i == 0) {
					if(!VTUtilJS.isNullOrEmpty(listSubLevel[i].productCode)) {
						textCode += listSubLevel[i].productCode;
					}
					if(listSubLevel[i].isRequired == 1) {
						textCode += '*';
					}
					if(listSubLevel[i].quantity != null && listSubLevel[i].quantity != undefined && !isNaN(listSubLevel[i].quantity) && Number(listSubLevel[i].quantity) > 0) {
						textCode += '('+listSubLevel[i].quantity+')';
					} else if(listSubLevel[i].amount != null && listSubLevel[i].amount != undefined && !isNaN(listSubLevel[i].amount) && Number(listSubLevel[i].amount) > 0) {
						textCode += '('+VTUtilJS.formatCurrency(listSubLevel[i].amount)+')';
					}
				} else {
					textCode += ', ';
					if(!VTUtilJS.isNullOrEmpty(listSubLevel[i].productCode)) {
						textCode += listSubLevel[i].productCode;
					}
					if(listSubLevel[i].isRequired == 1) {
						textCode += '*';
					}
					if(listSubLevel[i].quantity != null && listSubLevel[i].quantity != undefined && !isNaN(listSubLevel[i].quantity) && Number(listSubLevel[i].quantity) > 0) {
						textCode += '('+listSubLevel[i].quantity+')';
					} else if(listSubLevel[i].amount != null && listSubLevel[i].amount != undefined && !isNaN(listSubLevel[i].amount) && Number(listSubLevel[i].amount) > 0) {
						textCode += '('+VTUtilJS.formatCurrency(listSubLevel[i].amount)+')';
					}
				}
			}
		}
		return Utils.XSSEncode(textCode);
	},
	deleteSubLevel : function(gridDiv, idx, subLevelId) {
		if($('#statusHidden').val() == 0 || $('#statusHidden').val() == 1){
			return;
		}
		$.messager.confirm("Xác nhận", "Bạn có muốn xóa mức con không?", function(r) {
			if(r) {
				var rows = $('#'+gridDiv).datagrid('getRows');
				if(subLevelId != undefined && subLevelId != null && subLevelId > 0) {
					VTUtilJS.postFormJson({levelId : subLevelId}, '/promotion/new-delete-sub-level', function(data){
						if(rows != null && $.isArray(rows) && rows.length > 0) {
							rows.splice(idx, 1);
							$('#'+gridDiv).datagrid('loadData', rows);
						}
					});
				} else {
					if(rows != null && $.isArray(rows) && rows.length > 0) {
						rows.splice(idx, 1);
						$('#'+gridDiv).datagrid('loadData', rows);
					}
				}
			}
		});
	},
	deleteSubProductLevel : function(parentDiv, parentIdx, detailGridId, idx, detailId) {
		if($('#statusHidden').val() == 0 || $('#statusHidden').val() == 1){
			return;
		}
		$.messager.confirm("Xác nhận", "Bạn có muốn xóa chi tiết sản phẩm không?", function(r) {
			if(r) {
				var ddv = $('#'+parentDiv).datagrid('getRowDetail', parentIdx).find('div.ddv#'+detailGridId);
				var rows = ddv.datagrid('getRows');
				if(detailId != undefined && detailId != null && detailId > 0) {
					VTUtilJS.postFormJson({levelDetailId : detailId}, '/promotion/new-delete-detail', function(data){
						if(!data.error) {
							if(rows != null && $.isArray(rows) && rows.length > 0) {
								rows.splice(idx, 1);
								ddv.datagrid('loadData', rows);
								var textCode = PromotionProgram.returnTextProductCode(rows);
						 		var subLevel = $('#'+parentDiv).datagrid('getRows')[parentIdx];
						 		subLevel.textCode = textCode;
						 		$('#'+parentDiv).datagrid('getRows').splice(parentIdx, 1, subLevel);
						 		$('#'+parentDiv).prev().find('.datagrid-btable tr[datagrid-row-index='+parentIdx+'] td[field=textCode] div').html(Utils.XSSEncode(Utils.XSSEncode(textCode)));
						 		ddv.datagrid('fixRowHeight', idx);
						 		$('#'+parentDiv).datagrid('fixRowHeight', parentIdx);
							}
						}
					});
				} else {
					if(rows != null && $.isArray(rows) && rows.length > 0) {
						rows.splice(idx, 1);
						ddv.datagrid('loadData', rows);
						var textCode = PromotionProgram.returnTextProductCode(rows);
				 		var subLevel = $('#'+parentDiv).datagrid('getRows')[parentIdx];
				 		subLevel.textCode = textCode;
				 		$('#'+parentDiv).datagrid('getRows').splice(parentIdx, 1, subLevel);
				 		$('#'+parentDiv).prev().find('.datagrid-btable tr[datagrid-row-index='+parentIdx+'] td[field=textCode] div').html(textCode);
				 		ddv.datagrid('fixRowHeight', idx);
				 		$('#'+parentDiv).datagrid('fixRowHeight', parentIdx);
					}
				}
			}
		});
	},
	saveHtmlLevel : function(levelCode, groupMuaId, groupKMId, mapId, levelMuaId, levelKMId) {
		if($('#statusHidden').val() == 0 || $('#statusHidden').val() == 1){
			return;
		}
		$('.ErrorMsgStyle').hide();
		var muaDg = $('#gridSublevelMua'+levelCode);
		var kmDg = $('#gridSublevelKM'+levelCode);
		var dataModel = {};
		dataModel.groupMuaId = groupMuaId;
		dataModel.groupKMId = groupKMId;
		dataModel.mappingId = mapId;
		dataModel.levelMuaId = levelMuaId;
		dataModel.levelKMId = levelKMId;
		if(muaDg.length > 0 && muaDg.datagrid('getRows').length > 0) {
			dataModel.listSubLevelMua = muaDg.datagrid('getRows');
		} else {
			$("#errorMsgDistribute"+levelCode).html('Mức mua chưa được nhập chi tiết. Vui lòng xem lại').show();
			return;
		}
		if(kmDg.length > 0 && kmDg.datagrid('getRows').length > 0) {
			dataModel.listSubLevelKM = kmDg.datagrid('getRows');
		} else {
			$("#errorMsgDistribute"+levelCode).html('Mức khuyến mại chưa được nhập chi tiết. Vui lòng xem lại').show();
			return;
		}
		var listProductCode = new Array();
		for(var i = 0; i < dataModel.listSubLevelKM.length; i++) {
			var isPercentInput = false;
			var isQuantityInput = false;
			var isAmountInput = false;
			if(!VTUtilJS.isNullOrEmpty(dataModel.listSubLevelKM[i].percent) && !isNaN(dataModel.listSubLevelKM[i].percent) && Number(dataModel.listSubLevelKM[i].percent) > 0) {
				isPercentInput = true;
			}
			if(!VTUtilJS.isNullOrEmpty(dataModel.listSubLevelKM[i].maxQuantityKM) && !isNaN(dataModel.listSubLevelKM[i].maxQuantityKM) && Number(dataModel.listSubLevelKM[i].maxQuantityKM) > 0) {
				if(dataModel.listSubLevelKM[i].listSubLevel == null || !$.isArray(dataModel.listSubLevelKM[i].listSubLevel) || dataModel.listSubLevelKM[i].listSubLevel.length == 0) {
					$("#errorMsgDistribute"+levelCode).html('Bạn chưa nhập sản phẩm khuyến mại. Vui lòng nhập sản phẩm khuyến mại').show();
					return;
				}
				isQuantityInput = true;
			}
			if(!VTUtilJS.isNullOrEmpty(dataModel.listSubLevelKM[i].maxAmountKM) && !isNaN(dataModel.listSubLevelKM[i].maxAmountKM) && Number(dataModel.listSubLevelKM[i].maxAmountKM) > 0) {
				isAmountInput = true;
			}
			if(isPercentInput && (isQuantityInput || isAmountInput)) {
				$("#errorMsgDistribute"+levelCode).html('Bạn chỉ được nhập duy nhất một hình thức khuyến mại % tiền hoặc, số lượng, hoặc số tiền').show();
				return;
			}
			if(isQuantityInput && isAmountInput) {
				$("#errorMsgDistribute"+levelCode).html('Bạn chỉ được nhập duy nhất một hình thức khuyến mại % tiền hoặc, số lượng, hoặc số tiền').show();
				return;
			}
			var sumDetailQuantity = 0;
			var sumDetailAmount = 0;
			var totalDetailQuantity = 0;
			var totalDetailAmount = 0;
			if(dataModel.listSubLevelKM[i].listSubLevel != null && $.isArray(dataModel.listSubLevelKM[i].listSubLevel) && dataModel.listSubLevelKM[i].listSubLevel.length > 0) {
				for(var j = 0; j < dataModel.listSubLevelKM[i].listSubLevel.length; j++) {
					if(VTUtilJS.isNullOrEmpty(dataModel.listSubLevelKM[i].listSubLevel[j].productCode)) {
						$("#errorMsgDistribute"+levelCode).html('Bạn chưa nhập sản phẩm khuyến mại. Vui lòng nhập sản phẩm khuyến mại').show();
						return;
					}
					if(!VTUtilJS.isNullOrEmpty(dataModel.listSubLevelKM[i].percent) && !isNaN(dataModel.listSubLevelKM[i].percent) && Number(dataModel.listSubLevelKM[i].percent) > 0) {
						if(!VTUtilJS.isNullOrEmpty(dataModel.listSubLevelKM[i].maxQuantityKM) && !isNaN(dataModel.listSubLevelKM[i].maxQuantityKM) && Number(dataModel.listSubLevelKM[i].maxQuantityKM) > 0) {
							$("#errorMsgDistribute"+levelCode).html('Mức khuyến mãi đang nhập dạng phần trăm').show();
							return;
						} else if(!VTUtilJS.isNullOrEmpty(dataModel.listSubLevelKM[i].maxAmountKM) && !isNaN(dataModel.listSubLevelKM[i].maxAmountKM) && Number(dataModel.listSubLevelKM[i].maxAmountKM) > 0) {
							$("#errorMsgDistribute"+levelCode).html('Mức khuyến mãi đang nhập dạng phần trăm').show();
							return;
						} else if(!VTUtilJS.isNullOrEmpty(dataModel.listSubLevelKM[i].listSubLevel[j].quantity) && !isNaN(dataModel.listSubLevelKM[i].listSubLevel[j].quantity) && Number(dataModel.listSubLevelKM[i].listSubLevel[j].quantity) > 0) {
							$("#errorMsgDistribute"+levelCode).html('Mức khuyến mãi đang nhập dạng phần trăm').show();
							return;
						} else if(!VTUtilJS.isNullOrEmpty(dataModel.listSubLevelKM[i].listSubLevel[j].amount) && !isNaN(dataModel.listSubLevelKM[i].listSubLevel[j].amount) && Number(dataModel.listSubLevelKM[i].listSubLevel[j].amount) > 0) {
							$("#errorMsgDistribute"+levelCode).html('Mức khuyến mãi đang nhập dạng phần trăm').show();
							return;
						}
					} else if(!VTUtilJS.isNullOrEmpty(dataModel.listSubLevelKM[i].maxQuantityKM) && !isNaN(dataModel.listSubLevelKM[i].maxQuantityKM) && Number(dataModel.listSubLevelKM[i].maxQuantityKM) > 0) {
						if(!VTUtilJS.isNullOrEmpty(dataModel.listSubLevelKM[i].percent) && !isNaN(dataModel.listSubLevelKM[i].percent) && Number(dataModel.listSubLevelKM[i].percent) > 0) {
							$("#errorMsgDistribute"+levelCode).html('Mức khuyến mãi đang nhập dạng group-quantity').show();
							return;
						} else if(!VTUtilJS.isNullOrEmpty(dataModel.listSubLevelKM[i].maxAmountKM) && !isNaN(dataModel.listSubLevelKM[i].maxAmountKM) && Number(dataModel.listSubLevelKM[i].maxAmountKM) > 0) {
							$("#errorMsgDistribute"+levelCode).html('Mức khuyến mãi đang nhập dạng group-quantity').show();
							return;
						}
					} else if(!VTUtilJS.isNullOrEmpty(dataModel.listSubLevelKM[i].maxAmountKM) && !isNaN(dataModel.listSubLevelKM[i].maxAmountKM) && Number(dataModel.listSubLevelKM[i].maxAmountKM) > 0) {
						if(!VTUtilJS.isNullOrEmpty(dataModel.listSubLevelKM[i].percent) && !isNaN(dataModel.listSubLevelKM[i].percent) && Number(dataModel.listSubLevelKM[i].percent) > 0) {
							$("#errorMsgDistribute"+levelCode).html('Mức khuyến mãi đang nhập dạng group-amount').show();
							return;
						} else if(!VTUtilJS.isNullOrEmpty(dataModel.listSubLevelKM[i].maxQuantityKM) && !isNaN(dataModel.listSubLevelKM[i].maxQuantityKM) && Number(dataModel.listSubLevelKM[i].maxQuantityKM) > 0) {
							$("#errorMsgDistribute"+levelCode).html('Mức khuyến mãi đang nhập dạng group-amount').show();
							return;
						}
					} else if(!VTUtilJS.isNullOrEmpty(dataModel.listSubLevelKM[i].listSubLevel[j].quantity) && !isNaN(dataModel.listSubLevelKM[i].listSubLevel[j].quantity) && Number(dataModel.listSubLevelKM[i].listSubLevel[j].quantity) > 0) {
						if(!VTUtilJS.isNullOrEmpty(dataModel.listSubLevelKM[i].percent) && !isNaN(dataModel.listSubLevelKM[i].percent) && Number(dataModel.listSubLevelKM[i].percent) > 0) {
							$("#errorMsgDistribute"+levelCode).html('Mức khuyến mãi đang nhập số lượng chi tiết sản phẩm ' + Utils.XSSEncode(dataModel.listSubLevelKM[i].listSubLevel[j].productCode)).show();
							return;
						} else if(!VTUtilJS.isNullOrEmpty(dataModel.listSubLevelKM[i].maxAmountKM) && !isNaN(dataModel.listSubLevelKM[i].maxAmountKM) && Number(dataModel.listSubLevelKM[i].maxAmountKM) > 0) {
							$("#errorMsgDistribute"+levelCode).html('Mức khuyến mãi đang nhập số lượng chi tiết sản phẩm ' + Utils.XSSEncode(dataModel.listSubLevelKM[i].listSubLevel[j].productCode)).show();
							return;
						} else if(!VTUtilJS.isNullOrEmpty(dataModel.listSubLevelKM[i].listSubLevel[j].amount) && !isNaN(dataModel.listSubLevelKM[i].listSubLevel[j].amount) && Number(dataModel.listSubLevelKM[i].listSubLevel[j].amount) > 0) {
							$("#errorMsgDistribute"+levelCode).html('Mức khuyến mãi đang nhập số lượng chi tiết sản phẩm ' + Utils.XSSEncode(dataModel.listSubLevelKM[i].listSubLevel[j].productCode)).show();
							return;
						}
					} else if(!VTUtilJS.isNullOrEmpty(dataModel.listSubLevelKM[i].listSubLevel[j].amount) && !isNaN(dataModel.listSubLevelKM[i].listSubLevel[j].amount) && Number(dataModel.listSubLevelKM[i].listSubLevel[j].amount) > 0) {
						if(!VTUtilJS.isNullOrEmpty(dataModel.listSubLevelKM[i].percent) && !isNaN(dataModel.listSubLevelKM[i].percent) && Number(dataModel.listSubLevelKM[i].percent) > 0) {
							$("#errorMsgDistribute"+levelCode).html('Mức khuyến mãi đang nhập số tiền chi tiết sản phẩm ' + Utils.XSSEncode(dataModel.listSubLevelKM[i].listSubLevel[j].productCode)).show();
							return;
						} else if(!VTUtilJS.isNullOrEmpty(dataModel.listSubLevelKM[i].maxQuantityKM) && !isNaN(dataModel.listSubLevelKM[i].maxQuantityKM) && Number(dataModel.listSubLevelKM[i].maxQuantityKM) > 0) {
							$("#errorMsgDistribute"+levelCode).html('Mức khuyến mãi đang nhập số tiền chi tiết sản phẩm ' + Utils.XSSEncode(dataModel.listSubLevelKM[i].listSubLevel[j].productCode)).show();
							return;
						} else if(!VTUtilJS.isNullOrEmpty(dataModel.listSubLevelKM[i].listSubLevel[j].quantity) && !isNaN(dataModel.listSubLevelKM[i].listSubLevel[j].quantity) && Number(dataModel.listSubLevelKM[i].listSubLevel[j].quantity) > 0) {
							$("#errorMsgDistribute"+levelCode).html('Mức khuyến mãi đang nhập số tiền chi tiết sản phẩm ' + Utils.XSSEncode(dataModel.listSubLevelKM[i].listSubLevel[j].productCode)).show();
							return;
						}
					} else {
						$("#errorMsgDistribute"+levelCode).html('Mức khuyến mãi chưa nhập giá trị cho sản phẩm ' + Utils.XSSEncode(dataModel.listSubLevelKM[i].listSubLevel[j].productCode)).show();
						return;
					}
					
					if(!isQuantityInput && !VTUtilJS.isNullOrEmpty(dataModel.listSubLevelKM[i].listSubLevel[j].quantity) && !isNaN(dataModel.listSubLevelKM[i].listSubLevel[j].quantity) && Number(dataModel.listSubLevelKM[i].listSubLevel[j].quantity) > 0) {
						isQuantityInput = true;
					}
					if(!isAmountInput && !VTUtilJS.isNullOrEmpty(dataModel.listSubLevelKM[i].listSubLevel[j].amount) && !isNaN(dataModel.listSubLevelKM[i].listSubLevel[j].amount) && Number(dataModel.listSubLevelKM[i].listSubLevel[j].amount) > 0) {
						isAmountInput = true;
					}
					if(isQuantityInput && isAmountInput) {
						$("#errorMsgDistribute"+levelCode).html('Một mức chỉ có thể nhập số lượng khuyến mại hoặc số tiền khuyến mại.').show();
						return;
					}
					if(!isQuantityInput && !isAmountInput) {
						$("#errorMsgDistribute"+levelCode).html('Nhóm bắt buộc phải nhập số lượng khuyến mại hoặc số tiền khuyến mại.').show();
						return;
					}
					if(dataModel.listSubLevelKM[i].listSubLevel[j].isRequired == 1 || dataModel.listSubLevelKM[i].listSubLevel[j].isRequired == true || dataModel.listSubLevelKM[i].listSubLevel[j].isRequired == 'true') {
						sumDetailQuantity += VTUtilJS.isNullOrEmpty(dataModel.listSubLevelKM[i].listSubLevel[j].quantity) ? 0 : Number(dataModel.listSubLevelKM[i].listSubLevel[j].quantity);
						sumDetailAmount += VTUtilJS.isNullOrEmpty(dataModel.listSubLevelKM[i].listSubLevel[j].amount) ? 0 : Number(dataModel.listSubLevelKM[i].listSubLevel[j].amount);
					}
					totalDetailQuantity += VTUtilJS.isNullOrEmpty(dataModel.listSubLevelKM[i].listSubLevel[j].quantity) ? 0 : Number(dataModel.listSubLevelKM[i].listSubLevel[j].quantity);
					totalDetailAmount += VTUtilJS.isNullOrEmpty(dataModel.listSubLevelKM[i].listSubLevel[j].amount) ? 0 : Number(dataModel.listSubLevelKM[i].listSubLevel[j].amount);
					if($('#typeCode').val() == 'ZV03' || $('#typeCode').val() == 'ZV06' || $('#typeCode').val() == 'ZV09' || $('#typeCode').val() == 'ZV12' || $('#typeCode').val() == 'ZV15' || $('#typeCode').val() == 'ZV18' || $('#typeCode').val() == 'ZV21') {
						if(listProductCode.indexOf(dataModel.listSubLevelKM[i].listSubLevel[j].productCode) != -1) {
							$("#errorMsgDistribute"+levelCode).html('Sản phẩm ' + Utils.XSSEncode(dataModel.listSubLevelKM[i].listSubLevel[j].productCode) + ' đã tồn tại trong khuyến mại. Vui lòng kiểm tra lại').show();
							return;
						} else {
							listProductCode.push(dataModel.listSubLevelKM[i].listSubLevel[j].productCode);
						}
						if(VTUtilJS.isNullOrEmpty(dataModel.listSubLevelKM[i].listSubLevel[j].productCode)) {
							$("#errorMsgDistribute"+levelCode).html('Bạn chưa nhập sản phẩm khuyến mại. Vui lòng nhập sản phẩm khuyến mại').show();
							return;
						} else if((VTUtilJS.isNullOrEmpty(dataModel.listSubLevelKM[i].listSubLevel[j].quantity) || isNaN(dataModel.listSubLevelKM[i].listSubLevel[j].quantity) || Number(dataModel.listSubLevelKM[i].listSubLevel[j].quantity) ==0)
								&& (VTUtilJS.isNullOrEmpty(dataModel.listSubLevelKM[i].listSubLevel[j].amount) || isNaN(dataModel.listSubLevelKM[i].listSubLevel[j].amount) || Number(dataModel.listSubLevelKM[i].listSubLevel[j].amount) == 0)) {
							$("#errorMsgDistribute"+levelCode).html('Bạn chưa nhập số lượng hoặc số tiền cho sản phẩm khuyến mại ' + Utils.XSSEncode(dataModel.listSubLevelKM[i].listSubLevel[j].productCode)).show();
							return;
						}
					}
				}
			}
			
			if(!isPercentInput && !isQuantityInput && !isAmountInput) {
				$("#errorMsgDistribute"+levelCode).html('Nhóm bắt buộc phải nhập số lượng khuyến mại hoặc số tiền khuyến mại.').show();
				return;
			}
			
			if(isQuantityInput && !VTUtilJS.isNullOrEmpty(dataModel.listSubLevelKM[i].maxQuantityKM) && !isNaN(dataModel.listSubLevelKM[i].maxQuantityKM) && Number(dataModel.listSubLevelKM[i].maxQuantityKM) > 0) {
				if(totalDetailQuantity > Number(dataModel.listSubLevelKM[i].maxQuantityKM)) {
					$("#errorMsgDistribute"+levelCode).html('Số lượng nhóm phải lớn hơn hoặc bằng tổng số lượng các sản phẩm khuyến mại').show();
					return;
				}
			}
			if(isAmountInput && !VTUtilJS.isNullOrEmpty(dataModel.listSubLevelKM[i].maxAmountKM) && !isNaN(dataModel.listSubLevelKM[i].maxAmountKM) && Number(dataModel.listSubLevelKM[i].maxAmountKM) > 0) {
				if(sumDetailAmount > Number(dataModel.listSubLevelKM[i].maxAmountKM)) {
					$("#errorMsgDistribute"+levelCode).html('Số tiền nhóm phải lớn hơn hoặc bằng tổng số tiền các sản phẩm bắt buộc khuyến mại').show();
					return;
				}
			}
		}
		var listProductCode = new Array();
		for(var i = 0; i < dataModel.listSubLevelMua.length; i++) {
			var isQuantityInput = false;
			var isAmountInput = false;
			if(!VTUtilJS.isNullOrEmpty(dataModel.listSubLevelMua[i].minQuantityMua) && !isNaN(dataModel.listSubLevelMua[i].minQuantityMua) && Number(dataModel.listSubLevelMua[i].minQuantityMua) > 0) {
				isQuantityInput = true;
			}
			if(!VTUtilJS.isNullOrEmpty(dataModel.listSubLevelMua[i].minAmountMua) && !isNaN(dataModel.listSubLevelMua[i].minAmountMua) && Number(dataModel.listSubLevelMua[i].minAmountMua) > 0) {
				isAmountInput = true;
			}
			if(isQuantityInput && isAmountInput) {
				$("#errorMsgDistribute"+levelCode).html('Một mức chỉ có thể nhập số lượng mua hoặc số tiền mua.').show();
				return;
			}
			var sumDetailQuantity = 0;
			var sumDetailAmount = 0;
			var totalDetailQuantity = 0;
			var totalDetailAmount = 0;
			if(dataModel.listSubLevelMua[i].listSubLevel != null && $.isArray(dataModel.listSubLevelMua[i].listSubLevel) && dataModel.listSubLevelMua[i].listSubLevel.length > 0) {
				for(var j = 0; j < dataModel.listSubLevelMua[i].listSubLevel.length; j++) {
					if(VTUtilJS.isNullOrEmpty(dataModel.listSubLevelMua[i].listSubLevel[j].productCode)) {
						$("#errorMsgDistribute"+levelCode).html('Bạn chưa nhập sản phẩm mua. Vui lòng nhập sản phẩm mua').show();
						return;
					}
					var promoType = $('#typeCode').val().trim();
					if ((promoType == "ZV01" || promoType == "ZV02" || promoType == "ZV03" || promoType == "ZV04" || promoType == "ZV05" || promoType == "ZV06" ||
						promoType == "ZV13" || promoType == "ZV14" || promoType == "ZV15" || promoType == "ZV16" || promoType == "ZV17" || promoType == "ZV18")
						&& dataModel.listSubLevelMua[i].listSubLevel[j].isRequired != 1) {
						$("#errorMsgDistribute"+levelCode).html('Bạn phải check chọn bắt buộc cho sản phẩm bán đối với KM dạng line, bundle').show();
						return;
					}

					if(!VTUtilJS.isNullOrEmpty(dataModel.listSubLevelMua[i].minQuantityMua) && !isNaN(dataModel.listSubLevelMua[i].minQuantityMua) && Number(dataModel.listSubLevelMua[i].minQuantityMua) > 0) {
						if(!VTUtilJS.isNullOrEmpty(dataModel.listSubLevelMua[i].minAmountMua) && !isNaN(dataModel.listSubLevelMua[i].minAmountMua) && Number(dataModel.listSubLevelMua[i].minAmountMua) > 0) {
							$("#errorMsgDistribute"+levelCode).html('Mức mua đang nhập dạng group-quantity').show();
							return;
						}
					} else if(!VTUtilJS.isNullOrEmpty(dataModel.listSubLevelMua[i].minAmountMua) && !isNaN(dataModel.listSubLevelMua[i].minAmountMua) && Number(dataModel.listSubLevelMua[i].minAmountMua) > 0) {
						if(!VTUtilJS.isNullOrEmpty(dataModel.listSubLevelMua[i].minQuantityMua) && !isNaN(dataModel.listSubLevelMua[i].minQuantityMua) && Number(dataModel.listSubLevelMua[i].minQuantityMua) > 0) {
							$("#errorMsgDistribute"+levelCode).html('Mức mua đang nhập dạng group-amount').show();
							return;
						}
					} else if(!VTUtilJS.isNullOrEmpty(dataModel.listSubLevelMua[i].listSubLevel[j].quantity) && !isNaN(dataModel.listSubLevelMua[i].listSubLevel[j].quantity) && Number(dataModel.listSubLevelMua[i].listSubLevel[j].quantity) > 0) {
						if(!VTUtilJS.isNullOrEmpty(dataModel.listSubLevelMua[i].minAmountMua) && !isNaN(dataModel.listSubLevelMua[i].minAmountMua) && Number(dataModel.listSubLevelMua[i].minAmountMua) > 0) {
							$("#errorMsgDistribute"+levelCode).html('Mức mua đang nhập số lượng chi tiết sản phẩm ' + Utils.XSSEncode(dataModel.listSubLevelMua[i].listSubLevel[j].productCode)).show();
							return;
						} else if(!VTUtilJS.isNullOrEmpty(dataModel.listSubLevelMua[i].listSubLevel[j].amount) && !isNaN(dataModel.listSubLevelMua[i].listSubLevel[j].amount) && Number(dataModel.listSubLevelMua[i].listSubLevel[j].amount) > 0) {
							$("#errorMsgDistribute"+levelCode).html('Mức mua đang nhập số lượng chi tiết sản phẩm ' + Utils.XSSEncode(dataModel.listSubLevelMua[i].listSubLevel[j].productCode)).show();
							return;
						}
					} else if(!VTUtilJS.isNullOrEmpty(dataModel.listSubLevelMua[i].listSubLevel[j].amount) && !isNaN(dataModel.listSubLevelMua[i].listSubLevel[j].amount) && Number(dataModel.listSubLevelMua[i].listSubLevel[j].amount) > 0) {
						if(!VTUtilJS.isNullOrEmpty(dataModel.listSubLevelMua[i].minQuantityMua) && !isNaN(dataModel.listSubLevelMua[i].minQuantityMua) && Number(dataModel.listSubLevelMua[i].minQuantityMua) > 0) {
							$("#errorMsgDistribute"+levelCode).html('Mức mua đang nhập số tiền chi tiết sản phẩm ' + Utils.XSSEncode(dataModel.listSubLevelMua[i].listSubLevel[j].productCode)).show();
							return;
						}else if(!VTUtilJS.isNullOrEmpty(dataModel.listSubLevelMua[i].listSubLevel[j].quantity) && !isNaN(dataModel.listSubLevelMua[i].listSubLevel[j].quantity) && Number(dataModel.listSubLevelMua[i].listSubLevel[j].quantity) > 0) {
							$("#errorMsgDistribute"+levelCode).html('Mức mua đang nhập số tiền chi tiết sản phẩm ' + Utils.XSSEncode(dataModel.listSubLevelMua[i].listSubLevel[j].productCode)).show();
							return;
						}
					} else {
						$("#errorMsgDistribute"+levelCode).html('Mức mua chưa nhập giá trị cho sản phẩm ' + Utils.XSSEncode(dataModel.listSubLevelMua[i].listSubLevel[j].productCode)).show();
						return;
					}
					
					if(!isQuantityInput && !VTUtilJS.isNullOrEmpty(dataModel.listSubLevelMua[i].listSubLevel[j].quantity) && !isNaN(dataModel.listSubLevelMua[i].listSubLevel[j].quantity) && Number(dataModel.listSubLevelMua[i].listSubLevel[j].quantity) > 0) {
						isQuantityInput = true;
					}
					if(!isAmountInput && !VTUtilJS.isNullOrEmpty(dataModel.listSubLevelMua[i].listSubLevel[j].amount) && !isNaN(dataModel.listSubLevelMua[i].listSubLevel[j].amount) && Number(dataModel.listSubLevelMua[i].listSubLevel[j].amount) > 0) {
						isAmountInput = true;
					}
					if(isQuantityInput && isAmountInput) {
						$("#errorMsgDistribute"+levelCode).html('Một mức chỉ có thể nhập số lượng mua hoặc số tiền mua.').show();
						return;
					}
					if(!isQuantityInput && !isAmountInput) {
						$("#errorMsgDistribute"+levelCode).html('Nhóm bắt buộc phải nhập số lượng mua hoặc số tiền mua.').show();
						return;
					}
					/**
					 * 
					 */
					if(dataModel.listSubLevelMua[i].listSubLevel[j].isRequired == 1 || dataModel.listSubLevelMua[i].listSubLevel[j].isRequired == true || dataModel.listSubLevelMua[i].listSubLevel[j].isRequired == 'true') {
						sumDetailQuantity += VTUtilJS.isNullOrEmpty(dataModel.listSubLevelMua[i].listSubLevel[j].quantity) ? 0 : Number(dataModel.listSubLevelMua[i].listSubLevel[j].quantity);
						sumDetailAmount += VTUtilJS.isNullOrEmpty(dataModel.listSubLevelMua[i].listSubLevel[j].amount) ? 0 : Number(dataModel.listSubLevelMua[i].listSubLevel[j].amount);
					}
					totalDetailQuantity += VTUtilJS.isNullOrEmpty(dataModel.listSubLevelMua[i].listSubLevel[j].quantity) ? 0 : Number(dataModel.listSubLevelMua[i].listSubLevel[j].quantity);
					totalDetailAmount += VTUtilJS.isNullOrEmpty(dataModel.listSubLevelMua[i].listSubLevel[j].amount) ? 0 : Number(dataModel.listSubLevelMua[i].listSubLevel[j].amount);
					if($('#typeCode').val() != 'ZV07' && $('#typeCode').val() != 'ZV08' && $('#typeCode').val() != 'ZV09' && $('#typeCode').val() != 'ZV10' && $('#typeCode').val() != 'ZV11' && $('#typeCode').val() != 'ZV12') {
						if(listProductCode.indexOf(dataModel.listSubLevelMua[i].listSubLevel[j].productCode) != -1) {
							$("#errorMsgDistribute"+levelCode).html('Sản phẩm ' + Utils.XSSEncode(dataModel.listSubLevelMua[i].listSubLevel[j].productCode) + ' đã tồn tại trong mức mua. Vui lòng kiểm tra lại').show();
							return;
						} else {
							listProductCode.push(dataModel.listSubLevelMua[i].listSubLevel[j].productCode);
						}
						if(VTUtilJS.isNullOrEmpty(dataModel.listSubLevelMua[i].listSubLevel[j].productCode)) {
							$("#errorMsgDistribute"+levelCode).html('Bạn chưa nhập sản phẩm mua. Vui lòng nhập sản phẩm mua').show();
							return;
						} else if((VTUtilJS.isNullOrEmpty(dataModel.listSubLevelMua[i].listSubLevel[j].quantity) || isNaN(dataModel.listSubLevelMua[i].listSubLevel[j].quantity) || Number(dataModel.listSubLevelMua[i].listSubLevel[j].quantity) ==0)
								&& (VTUtilJS.isNullOrEmpty(dataModel.listSubLevelMua[i].listSubLevel[j].amount) || isNaN(dataModel.listSubLevelMua[i].listSubLevel[j].amount) || Number(dataModel.listSubLevelMua[i].listSubLevel[j].amount) == 0)) {
							$("#errorMsgDistribute"+levelCode).html('Bạn chưa nhập số lượng hoặc số tiền cho sản phẩm mua ' + Utils.XSSEncode(dataModel.listSubLevelMua[i].listSubLevel[j].productCode)).show();
							return;
						}
					} else {
						if(listProductCode.indexOf(dataModel.listSubLevelMua[i].listSubLevel[j].productCode) != -1) {
							$("#errorMsgDistribute"+levelCode).html('Sản phẩm ' + Utils.XSSEncode(dataModel.listSubLevelMua[i].listSubLevel[j].productCode) + ' đã tồn tại trong mức mua. Vui lòng kiểm tra lại').show();
							return;
						} else {
							listProductCode.push(dataModel.listSubLevelMua[i].listSubLevel[j].productCode);
						}
					}
				}
			} else if($('#typeCode').val() != 'ZV19' && $('#typeCode').val() != 'ZV20' && $('#typeCode').val() != 'ZV21') {
				$("#errorMsgDistribute"+levelCode).html('Chương trình khuyến mại nhóm mua bắt buộc phải có sản phẩm').show();
				return;
			}
			
			if(!isQuantityInput && !isAmountInput) {
				$("#errorMsgDistribute"+levelCode).html('Nhóm bắt buộc phải nhập số lượng mua hoặc số tiền mua.').show();
				return;
			}
			
			if(isQuantityInput && !VTUtilJS.isNullOrEmpty(dataModel.listSubLevelMua[i].minQuantityMua) && !isNaN(dataModel.listSubLevelMua[i].minQuantityMua) && Number(dataModel.listSubLevelMua[i].minQuantityMua) > 0) {
				if(totalDetailQuantity > Number(dataModel.listSubLevelMua[i].minQuantityMua)) {
					$("#errorMsgDistribute"+levelCode).html('Số lượng nhóm phải lớn hơn hoặc bằng tổng số lượng các sản phẩm mua').show();
					return;
				}
			}
			if(isAmountInput && !VTUtilJS.isNullOrEmpty(dataModel.listSubLevelMua[i].minAmountMua) && !isNaN(dataModel.listSubLevelMua[i].minAmountMua) && Number(dataModel.listSubLevelMua[i].minAmountMua) > 0) {
				if(sumDetailAmount > Number(dataModel.listSubLevelMua[i].minAmountMua)) {
					$("#errorMsgDistribute"+levelCode).html('Số tiền nhóm phải lớn hơn hoặc bằng tổng số tiền các sản phẩm bắt buộc mua').show();
					return;
				}
			}
		}
		$.messager.confirm("Xác nhận", "Bạn có muốn lưu thông tin?", function(r) {
			if(r) {
				VTUtilJS.postFormJson(VTUtilJS.getSimpleObject(dataModel), '/promotion/new-save-level', function(data){
					if(data.error) {
						$("#errorMsgDistribute"+levelCode).html(data.errMsg).show();
					} else {
						$("#successMsgDistribute"+levelCode).html('Lưu dữ liệu thành công').show();
						setTimeout(function() {
							$("#successMsgDistribute"+levelCode).hide();
						}, 3000);
						loadLevelDetail();
					}
				});
			}
		});
	},
	renderPannelEx : function(list, i, groupMuaId, groupKMId) {
		if(list.length == i) {
			return;
		}
		$($('#mappingGrid tbody')[0]).append('<tr>\
						<td>\
						<p class="ErrorMsgStyle SpriteErr" style="display: none;" id="errorMsgDistribute'+(VTUtilJS.isNullOrEmpty(list[i].levelCode) ? list[i].mapId : list[i].levelCode+list[i].mapId)+'">\
						<p id="successMsgDistribute'+(VTUtilJS.isNullOrEmpty(list[i].levelCode) ? list[i].mapId : list[i].levelCode+list[i].mapId)+'" class="SuccessMsgStyle" style="display: none"></p>\
						<div class="TableLevel" id="tableLevel'+(list[i].mapId)+'" style="width:100%; height:\'auto\';padding:10px;" title="">\
				<table style="width: 100%;">\
		    	<tr>\
		    		<td style="width: 50%; vertical-align: top;">\
		    			<table id="tableSubLevelMua'+(VTUtilJS.isNullOrEmpty(list[i].levelCode) ? list[i].mapId : list[i].levelCode+list[i].mapId)+'" style="border: 1px solid black; vertical-align: top;width: 100%">\
		    				<tbody>\
		    				</tbody>\
		    			</table>\
		    		</td>\
		    		<td style="width: 50%; vertical-align: top;">\
		    			<table id="tableSubLevelKM'+(VTUtilJS.isNullOrEmpty(list[i].levelCode) ? list[i].mapId : list[i].levelCode+list[i].mapId)+'" style="border: 1px solid black; vertical-align: top;width: 100%">\
		    				<tbody>\
		    				</tbody>\
		    			</table>\
		    		</td>\
		    	</tr>\
			</table>\
				    </div>\
			</td>\
		</tr>');
		VTUtilJS.bindFormatOnTextfield('sttMua_'+(VTUtilJS.isNullOrEmpty(list[i].levelCode) ? list[i].mapId : list[i].levelCode+list[i].mapId), VTUtilJS._TF_NUMBER);
		$('#tableLevel'+(list[i].mapId)).panel({
			width:'95%',
			height:'auto',
			collapsible:true,
			title: VTUtilJS.isNullOrEmpty(list[i].levelCode) ? 'STT : ' + list[i].stt : 'Mã : ' + Utils.XSSEncode(list[i].levelCode) + ' - ' + 'STT : ' + list[i].stt,
			tools:[{
				iconCls:'iconAdd '+i+'',
				handler:function(){
					var i = Number(Number($(this).attr('class').split(' ')[1]));
					PromotionProgram.addHtmlLevel('tableLevel'+(list[i].mapId)+'',groupMuaId, groupKMId);
				}
			}, {
				iconCls:'iconEdit '+i+'',
				handler:function(){
					var i = Number($(this).attr('class').split(' ')[1]);
					PromotionProgram.editHtmlLevel('tableLevel'+(list[i].mapId), groupMuaId, groupKMId, list[i].mapId, list[i].levelMuaId, list[i].levelKMId, (VTUtilJS.isNullOrEmpty(list[i].levelCode) ? '' : list[i].levelCode), (list[i].stt == null || list[i].stt == undefined) ? 0 : list[i].stt);
				}
			}, {
				iconCls:'iconDelete '+i+'',
				handler:function(){
					var i = Number($(this).attr('class').split(' ')[1]);
					PromotionProgram.deleteHtmlLevel('tableLevel'+(list[i].mapId), groupMuaId, groupKMId, list[i].mapId, list[i].levelMuaId, list[i].levelKMId);
				}
			}, {
				iconCls:'iconCopy '+i+'',
				handler:function(){
					var i = Number($(this).attr('class').split(' ')[1]);
					PromotionProgram.copyHtmlLevel(list[i].mapId, groupMuaId, groupKMId);
				}
			}, {
				iconCls:'iconSave '+i+'',
				handler:function(){
					var i = Number($(this).attr('class').split(' ')[1]);
					PromotionProgram.saveHtmlLevel((VTUtilJS.isNullOrEmpty(list[i].levelCode) ? list[i].mapId : list[i].levelCode+list[i].mapId), groupMuaId, groupKMId, list[i].mapId, list[i].levelMuaId, list[i].levelKMId);
				}
			}]
		});
		if(list[i].listExLevelMua != null && list[i].listExLevelMua != undefined && $.isArray(list[i].listExLevelMua)) {
			PromotionProgram.addHtmlSubRowMua('tableSubLevelMua'+(VTUtilJS.isNullOrEmpty(list[i].levelCode) ? list[i].mapId : list[i].levelCode+list[i].mapId), VTUtilJS.isNullOrEmpty(list[i].levelCode) ? list[i].mapId : list[i].levelCode+list[i].mapId, list[i].listExLevelMua);
		}
		if(list[i].listExLevelKM != null && list[i].listExLevelKM != undefined && $.isArray(list[i].listExLevelKM)) {
			PromotionProgram.addHtmlSubRowKM('tableSubLevelKM'+(VTUtilJS.isNullOrEmpty(list[i].levelCode) ? list[i].mapId : list[i].levelCode+list[i].mapId), VTUtilJS.isNullOrEmpty(list[i].levelCode) ? list[i].mapId : list[i].levelCode+list[i].mapId, list[i].listExLevelKM);
		}
		setTimeout(function() {
			PromotionProgram.renderPannelEx(list, ++i, groupMuaId, groupKMId);
		}, 100);
	},
	copyHtmlLevel : function(mapId, groupMuaId, groupKMId) {
		$('.ErrorMsgStyle').hide();
		if($('#statusHidden').val() == 0 || $('#statusHidden').val() == 1){
			return;
		}
		var html = '<div id="popup-container"><div id="popup-add-level" class="PopupContentMid">\
			<div class="GeneralForm Search1Form" id="form-data">\
			<div style="padding: 10px 10px 20px;">Bạn có muốn thực hiện sao chép mức?</div>\
			<label style="width: 100px;display:none;" class="LabelStyle Label1Style">Số lần<span class="ReqiureStyle">(*)</span></label> \
			<input type="text" class="InputTextStyle InputText1Style" style="width: 50px;display:none;" maxlength="3" id="copyNum" value="1">\
			<input type="hidden" id="mappingId" value="'+mapId+'">\
			<div class="Clear"></div>\
			<div class="BtnCenterSection">\
			<button class="BtnGeneralStyle" id="btn-update">Đồng ý</button>\
			</div>\
			<p class="ErrorMsgStyle SpriteErr" style="display: none;" id="errMsgLevelDlg"></p>\
			<p class="SuccessMsgStyle" style="display: none;" id="successMsgLevelDlg"></p>\
			</div></div></div>';
		$("body").append(html);
		$('#popup-add-level').dialog({
			title: "Sao chép mức",
			width: 250,
			heigth: "auto",
			modal: true, close: false, cache: false,
			onOpen: function() {
				VTUtilJS.bindFormatOnTextfield('copyNum', VTUtilJS._TF_NUMBER);
				$('#btn-update').bind('click', function() {
					if($('#form-data #copyNum').val() == '') {
						$('#popup-container #errMsgLevelDlg').html('Bạn chưa nhập số lần sao chép').show();
						return;
					}
					PromotionProgram.isEndLoad = true;
					PromotionProgram.isFinishLoad = true;
					VTUtilJS.postFormJson('form-data', '/promotion/new-copy-level', function(data){
						if(data.error) {
							$('#errMsgLevelDlg').html(data.errMsg).show();
						} else {
							$('#popup-add-level').dialog('close');
							var list = data.list;
							if(list != null && list != undefined && $.isArray(list) && list.length > 0) {
								PromotionProgram.renderPannelEx(list, 0, groupMuaId, groupKMId);
							}
						}
					});
				});
			},
			onClose: function() {
				$("#popup-add-level").dialog("destroy");
				$("#popup-container").remove();
			}
		});
	},
	addNewProductConvert: function() {
		var html = '<div id="popup-container"><div id="popup-add-level" class="PopupContentMid">\
			<div class="GeneralForm Search1Form" id="form-data">\
			<label style="width: 100px;" class="LabelStyle Label1Style">Nhóm<span class="ReqiureStyle">(*)</span></label> \
			<input type="text" class="InputTextStyle InputText1Style" style="width: 50px;" call-back="VTValidateUtils.getMessageOfRequireCheck(\'name\', \'Nhóm\', false, true);VTValidateUtils.getMessageOfSpecialCharactersValidate(\'name\', \'Nhóm\', Utils._NAME)" maxlength="100" id="name">\
			<div class="Clear"></div>\
			<div class="BtnCenterSection">\
			<button class="BtnGeneralStyle" id="btn-update">Lưu</button>\
			</div>\
			<p class="ErrorMsgStyle SpriteErr" style="display: none;" id="errMsgLevelDlg"></p>\
			<p class="SuccessMsgStyle" style="display: none;" id="successMsgLevelDlg"></p>\
			</div></div></div>';
		$("body").append(html);
		$('#popup-add-level').dialog({
			title: "Thêm nhóm",
			width: 210,
			heigth: "auto",
			modal: true, close: false, cache: false,
			onOpen: function() {
				$('#btn-update').bind('click', function() {
					var params = VTUtilJS.getFormData('form-data');
					$('#popup-add-level').dialog('close');
					if(!VTUtilJS.isNullOrEmpty(params.name)) {
						var obj = {name : params.name.toUpperCase(), listDetail : []};
						$('#convertGrid').datagrid('appendRow', obj);
						$('#convertGrid').datagrid('loadData', $('#convertGrid').datagrid('getRows'));
					}
				});
			},
			onClose: function() {
				$("#popup-add-level").dialog("destroy");
				$("#popup-container").remove();
			}
		});
	},
	deleteProductConvert: function(index, id) {
		$('#errMsgConvert').html('').hide();
		if(!VTUtilJS.isNullOrEmpty(id)) {
			$.messager.confirm("Xác nhận", "Bạn có muốn xóa nhóm?", function(r) {
				if(r) {
					VTUtilJS.postFormJson({id:id}, '/promotion/product-convert-delete', function(data) {
						if(data.error) {
							$('#errMsgConvert').html(data.errMsg).show();
						} else {
							$('#convertGrid').datagrid('getRows').splice(index, 1);
							$('#convertGrid').datagrid('loadData', $('#convertGrid').datagrid('getRows'));
						}
					});
				}
			});
		} else {
			$('#convertGrid').datagrid('getRows').splice(index, 1);
			$('#convertGrid').datagrid('loadData', $('#convertGrid').datagrid('getRows'));
		}
	},
	addNewProductConvertDetail: function(index) {
		var ddv = $('#convertGrid').datagrid('getRowDetail',index).find('table.ddv');
		var __rows = ddv.datagrid('getRows');
		if(__rows != null && $.isArray(__rows)) {
			for(var ii = 0; ii < __rows.length; ii++) {
				if(VTUtilJS.isNullOrEmpty(__rows[ii].productCode)){
					$('#errMsgConvert').html("Xin nhập mã sản phẩm tại dòng mới thêm vào!").show();
					return;
				} else if (VTUtilJS.isNullOrEmpty(__rows[ii].factor)) {
					$('#errMsgConvert').html("Xin nhập hệ số quy đổi tại dòng mới thêm vào!").show();
					return;
				}
			}
		}
		ddv.datagrid('acceptChanges');
		ddv.datagrid('appendRow', {isSourceProduct:0});
		ddv.datagrid('beginEdit', ddv.datagrid('getRows').length-1);
		$('#convertGrid').datagrid('fixRowHeight', index);
	},
	deleteProductConvertDetail: function(index, id,idxParent) {
		$('#errMsgConvert').html('').hide();
		if(!VTUtilJS.isNullOrEmpty(id)) {
			$.messager.confirm("Xác nhận", "Bạn có muốn xóa sản phẩm của nhóm?", function(r) {
				if(r) {
					VTUtilJS.postFormJson({id:id}, '/promotion/product-convert-detail-delete', function(data) {
						if(data.error) {
							$('#errMsgConvert').html(data.errMsg).show();
						} else {
							var ddv = $('#convertGrid').datagrid('getRowDetail',idxParent).find('table.ddv');
							ddv.datagrid('deleteRow',index);
							ddv.datagrid('loadData', ddv.datagrid('getRows'));
						}
					});
				}
			});
		} else {
			var ddv = $('#convertGrid').datagrid('getRowDetail',idxParent).find('table.ddv');
			ddv.datagrid('deleteRow',index);
			ddv.datagrid('loadData', ddv.datagrid('getRows'));
		}
	},
	saveProductConvert: function() {
		$('#errMsgConvert').html('').hide();
		var params = new Object();
		params.promotionId = $('#id').val();																															
		params.listConvertGroup = $('#convertGrid').datagrid('getRows');
		if(!$.isArray(params.listConvertGroup) || params.listConvertGroup == null || params.listConvertGroup.length == 0) {
			return;
		}
		var listTotalProduct = new Array();
		for(var i = 0; i < params.listConvertGroup.length; i++) {
			var listProductCode = new Array();
			var isHashMore2 = false;
			var isHashRoot = 0;
			var isHashNonRoot = false;
			for(var j = 0; j < params.listConvertGroup[i].listDetail.length; j++) {
				var pp = $("#promotionConvertGrid .datagrid-row-detail .datagrid-wrap")[i];
				var td = $(pp).find(".datagrid-row td[field=productCode]")[j];
				if ($(td).find("input.combo-text.validatebox-text:visible").length > 0) {
					var pcode = $(td).find("input.combo-text.validatebox-text:visible").val().trim();
					if (pcode == ""){
						$('#errMsgConvert').html('Sản phẩm thứ ' +  (j+1) + ' trong nhóm ' + Utils.XSSEncode(params.listConvertGroup[i].name) + ' không được để trống').show();
						return;
					} else if (pcode != null && (params.listConvertGroup[i].listDetail[j].productCode == null || params.listConvertGroup[i].listDetail[j].productCode == "")) {
						$('#errMsgConvert').html('Sản phẩm ' +  Utils.XSSEncode(pcode) + ' trong nhóm ' + Utils.XSSEncode(params.listConvertGroup[i].name) + ' không tồn tại trong hệ thống').show();
						return;
					}
				}
				if(listProductCode.indexOf(params.listConvertGroup[i].listDetail[j].productCode) != -1) {
					$('#errMsgConvert').html('Sản phẩm ' +  Utils.XSSEncode(params.listConvertGroup[i].listDetail[j].productCode) + ' trong nhóm ' + Utils.XSSEncode(params.listConvertGroup[i].name) + ' đang bị trùng').show();
					return;
				} else if(VTUtilJS.isNullOrEmpty(params.listConvertGroup[i].listDetail[j].factor) || isNaN(params.listConvertGroup[i].listDetail[j].factor) || Number(params.listConvertGroup[i].listDetail[j].factor) <= 0) {
					$('#errMsgConvert').html('Dòng sản phẩm ' +  Utils.XSSEncode(params.listConvertGroup[i].listDetail[j].productCode) + ' trong nhóm ' + Utils.XSSEncode(params.listConvertGroup[i].name) + ' chưa nhập hệ số quy đổi').show();
					return;
				} else if(isNaN(params.listConvertGroup[i].listDetail[j].factor) || Number(params.listConvertGroup[i].listDetail[j].factor) <= 0) {
					$('#errMsgConvert').html('Dòng sản phẩm ' +  Utils.XSSEncode(params.listConvertGroup[i].listDetail[j].productCode) + ' trong nhóm ' + Utils.XSSEncode(params.listConvertGroup[i].name) + ' đang có hệ số quy đổi nhỏ hơn hoặc bằng 0').show();
					return;
				} else {
					listProductCode.push(params.listConvertGroup[i].listDetail[j].productCode);
				}
				if(j > 0) {
					isHashMore2 = true;
				}
				if(params.listConvertGroup[i].listDetail[j].isSourceProduct == 1) {
					isHashRoot += 1;
					if(PromotionProgram._listSaleProduct.get(params.listConvertGroup[i].listDetail[j].productCode) == null) {
						$('#errMsgConvert').html('Sản phẩm ' +  Utils.XSSEncode(params.listConvertGroup[i].listDetail[j].productCode) + ' trong nhóm ' + Utils.XSSEncode(params.listConvertGroup[i].name) + ' không phải là sản phẩm trong danh cơ cấu nhóm mua').show();
						return;
					}
				} else {
					isHashNonRoot = false;
				}
				if(listTotalProduct.indexOf(params.listConvertGroup[i].listDetail[j].productCode) != -1) {
					$('#errMsgConvert').html('Sản phẩm ' +  Utils.XSSEncode(params.listConvertGroup[i].listDetail[j].productCode) + ' trong nhóm ' + Utils.XSSEncode(params.listConvertGroup[i].name) + ' không được xuất hiện hai lần').show();
					return;
				} else {
					listTotalProduct.push(params.listConvertGroup[i].listDetail[j].productCode);
				}
			}
			if(!isHashMore2) {
				$('#errMsgConvert').html('Nhóm ' + Utils.XSSEncode(params.listConvertGroup[i].name) + ' phải có ít nhất 2 sản phẩm').show();
				return;
			}
			if(isHashRoot == 0) {
				$('#errMsgConvert').html('Nhóm ' + Utils.XSSEncode(params.listConvertGroup[i].name) + ' phải có sản phẩm gốc').show();
				return;
			} else if(isHashRoot > 1) {
				$('#errMsgConvert').html('Nhóm ' + Utils.XSSEncode(params.listConvertGroup[i].name) + ' đang có 2 sản phẩm gốc').show();
				return;
			}
		}
		$.messager.confirm("Xác nhận", "Bạn có muốn lưu sản phẩm quy đổi?", function(r) {
			if(r) {
				VTUtilJS.postFormJson(VTUtilJS.getSimpleObject(params), '/promotion/product-convert-save', function(data){
					if(data.error) {
						$('#errMsgConvert').html(data.errMsg).show();
					} else {
						$("#successMsgConvert").html('Lưu dữ liệu thành công').show();
						setTimeout(function() {
							$("#successMsgConvert").html('').hide();
							$('#convertGrid').datagrid('reload');
						}, 3000);
					}
				});
			}
		});
	},
	addProductOpenNew: function() {
		$('#openGrid').datagrid('appendRow', {});
		$('#openGrid').datagrid('beginEdit', $('#openGrid').datagrid('getRows').length - 1);
	},
	deleteProductOpenNew: function(index, id) {
		$('#errMsgOpen').html('').hide();
		if(VTUtilJS.isNullOrEmpty(id)) {
			$('#openGrid').datagrid('getRows').splice(index, 1);
			$('#openGrid').datagrid('loadData', $('#openGrid').datagrid('getRows'));
		} else {
			$.messager.confirm("Xác nhận", "Bạn có muốn lưu sản phẩm quy đổi?", function(r) {
				if(r) {
					VTUtilJS.postFormJson({id:id}, '/promotion/product-open-new-delete', function(data) {
						if(data.error) {
							$('#errMsgOpen').html(data.errMsg).show();
						} else {
							$('#openGrid').datagrid('getRows').splice(index, 1);
							$('#openGrid').datagrid('loadData', $('#openGrid').datagrid('getRows'));
						}
					});
				}
			});
		}
	},
	saveProductOpenNew: function() {
		$('#errMsgOpen').html('').hide();
		var params = new Object();
		params.promotionId = $('#id').val();
		params.listProductOpen = $('#openGrid').datagrid('getRows');
		if(!$.isArray(params.listProductOpen) || params.listProductOpen == null || params.listProductOpen.length == 0) {
			return;
		}
		for(var i = 0; i < params.listProductOpen.length; i++) {
			if(VTUtilJS.isNullOrEmpty(params.listProductOpen[i].productCode)) {
				$('#errMsgOpen').html('Dòng '+(i+1)+' chưa chọn sản phẩm').show();
				return;
			} else if((VTUtilJS.isNullOrEmpty(params.listProductOpen[i].quantity) || isNaN(params.listProductOpen[i].quantity) || Number(params.listProductOpen[i].quantity) <= 0) 
					&& (VTUtilJS.isNullOrEmpty(params.listProductOpen[i].amount) || isNaN(params.listProductOpen[i].amount) || Number(params.listProductOpen[i].amount) <= 0)) {
				$('#errMsgOpen').html('Dòng '+(i+1)+' chưa cập nhập giá trị').show();
				return;
			} else if(!VTUtilJS.isNullOrEmpty(params.listProductOpen[i].quantity) && !VTUtilJS.isNullOrEmpty(params.listProductOpen[i].amount)) {
				$('#errMsgOpen').html('Dòng '+(i+1)+' chỉ được nhập một loại giá trị').show();
				return;
			}
		}
		$.messager.confirm("Xác nhận", "Bạn có muốn lưu sản phẩm mở mới?", function(r) {
			if(r) {
				VTUtilJS.postFormJson(VTUtilJS.getSimpleObject(params), '/promotion/product-open-new-save', function(data){
					if(data.error) {
						$('#errMsgOpen').html(data.errMsg).show();
					} else {
						$("#successMsgOpen").html('Lưu dữ liệu thành công').show();
						setTimeout(function() {
							$("#successMsgOpen").html('').hide();
							$('#openGrid').datagrid('reload');
						}, 3000);
					}
				});
			}
		});
	}
};

/**
 * CArray: mang array,
 * Xu ly khi them vao mang, neu chua co indexOf() < 0 thi moi them
 */
function CArray() {
  var arr = [];
  
  this.push = function(a) {
    var idx = arr.indexOf(a);
    if (idx < 0) {
      arr.push(a);
      return a;
    }
    return null;
  }
  
  this.remove = function(a) {
    var idx = arr.indexOf(a);
    if (idx > -1) {
      arr.splice(idx, 1);
    }
    return idx;
  }
  
  this.removeAt = function(i) {
    if (i > -1 && i < arr.length) {
      var a = arr[i];
      arr.splice(i, 1);
      return a;
    }
    return null;
  }
  
  this.get = function(i) {
    if (i > -1 && i < arr.length) {
      return arr[i];
    }
    return null;
  }
  
  this.indexOf = function(a) {
    return arr.indexOf(a);
  }
  
  this.toArray = function() {
    return arr;
  }
  
  this.size = function() {
	  if (arr == null) {
		  return 0;
	  }
	  return arr.length;
  }
};
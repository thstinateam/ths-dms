var ProgrammeDisplayCatalog = {
	_xhrSave : null,
	_xhrDel: null,
	_curGridId: null,
	_curRowId: null,
	_arrSelItems: null,
	_mapCheckSubCat:null,	
	displayProductMap : new Map(),
	displayStaffMap : new Map(),
	_mapCustomer:null,
	_arrListStaffItem:null,
	_mapStaff:null,
	_mapProduct:new Map(),
	_mapShop:null,
	_mapExclusion:null,
	groupIdDS:null,
	groupTypeDS:null,
	_listExclusionDialogSize:null,
	_listExclusion:null,
	_listShopId:null,
	_listShopIdEx:null,
	_lstSize:null,
	_lstProduct:null,
	_lstMapProductsDP: new Map(),
	_params:null,
	_lstCustomerId:null,
	staffRole : null,
	AMOUNT:1,
	SKU:3,
	QUANTITY:2, 
	DISPLAY:4,
	_lstLevelCode: new Map(),
	_lstProductExpand:[],
	checkFromDate:true,
	checkToDate:true,
	isStopped:false,
	fromDate:new Map(),
	toDate:new Map(),
	_listShopId:null,
	_gridDetaiSpCblId:null,
	lstConvfact: null,
	lstProductId: null,
	lstIndex: null,
	searchActionLog: function(){
		$('.ErrorMsgStyle').hide();
		var id = $('#selId').val().trim();
		var fromDate = $('#fromDate').val().trim(); 
		if(fromDate=='__/__/____'){
			fromDate = '';
		}
		var toDate = $('#toDate').val().trim();
		if(toDate=='__/__/____'){
			toDate = '';
		}
		var url = '/catalog/programme-display/search-info-change?id=' + id + '&fromDate=' + fromDate + '&toDate=' + toDate;
		$("#infoChangeGrid").setGridParam({url:url,page:1}).trigger("reloadGrid");
	},	
	showDlgImportExcel : function() {		
		$('#easyuiPopup').show();
		$('.ErrorMsgStyle').hide();
		$('#dateStrExcel').val(getCurrentMonth());
		$('#fakefilepc').val('');
		$('#easyuiPopup').dialog({  
	        closed: false,  
	        cache: false,  
	        modal: true,
	        onOpen: function(){	 
	        	$('.easyui-dialog .BtnGeneralStyle').css('color','');
	        	$('.downloadTemplateNVBH').attr('href',excel_template_path+'/display-program/Bieu_mau_import_so_suat_nvbh_cttb.xls');
	        },
	        onClose: function(){
	        	var month = $('#month').val().trim();
				ProgrammeDisplayCatalog._mapStaff = new Map();
				var params = new Object();
				params.id = $('#displayProgram').val().trim();
				params.strShopIds = $("#shop").data("kendoMultiSelect").value().toString();
				params.month = $('#month').val().trim();
				$("#staffGrid").datagrid('reload',params);
				$("#staffGrid").datagrid('uncheckAll');
				var StaffRole = $('#staffRole').val();
				if(StaffRole!=StaffRoleType.NVGS && StaffRole!=StaffRoleType.NHVNM){
					if(!Utils.compareCurrentMonthEx(month)){
						$('#checkDeleteAll1').attr('disabled',true);
						$('#checkDeleteAll1').attr('checked',false);
					}else{
						$('#checkDeleteAll1').attr('disabled',false);
					}
				}
	        }
		});
	},
	
	openCopyFocusProgram:function(){
		$('.ErrorMsgStyle').hide();
		$('#searchStyle1EasyUIDialogDivEx').css("visibility", "visible");
		var html = $('#searchStyle1EasyUIDialogDivEx').html();
		$('#searchStyle1EasyUIDialogEx').dialog({  
	        title: 'Sao chép CTTB',  
	        closed: false,  
	        cache: false,  
	        modal: true,
	        width : 616,
	        height :'auto',
	        onOpen: function(){	    
	        	$('.easyui-dialog #__btnSaveDisplayProgram').bind('click',function(event){
					var code = $('.easyui-dialog #seachStyle1Code').val().trim();
					var name = $('.easyui-dialog #seachStyle1Name').val().trim();
					if(code == "" || code == undefined || code == null){
						msg = "Bạn chưa nhập giá trị trường Mã CTTB.";
						$('.easyui-dialog #errMsgSearch').html(msg).show();
						$('.easyui-dialog #seachStyle1Code').focus();
						return false;
					}
					var msg = Utils.getMessageOfSpecialCharactersValidateEx1('.easyui-dialog #seachStyle1Code', 'Mã CTTB',Utils._CODE);
					if(msg.length >0){
						$('.easyui-dialog #errMsgSearch').html(msg).show();
						$('.easyui-dialog #seachStyle1Code').focus();
						return false;
					}
					if(name == "" || name == undefined || name == null){
						$('.easyui-dialog #errMsgSearch').html("Bạn chưa nhập giá trị trường Tên CTTB.").show();
						$('.easyui-dialog #seachStyle1Name').focus();
						return false;
					}
					msg = Utils.getMessageOfSpecialCharactersValidateEx1('.easyui-dialog #seachStyle1Name', 'Tên CTTB');
					if(msg.length >0){
						$('.easyui-dialog #errMsgSearch').html(msg).show();
						$('.easyui-dialog #seachStyle1Name').focus();
						return false;
					}
					var id = $('#selId').val();
					var params = new Object();
					params.id = id;
					params.code = code;
					params.name = name;
					Utils.addOrSaveData(params, '/programme-display/copydisplayprogram', null, 'errMsgSearch', function(data){						
						$('.easyui-dialog #successMsgInfo').html("Sao chép dữ liệu thành công.").show();
						var tm = setTimeout(function(){
							$('#searchStyle1EasyUIDialogEx').dialog('close');
							window.location.href = '/programme-display/change?id='+encodeChar(data.displayProgramId);
	                		clearTimeOut(tm);	                		
	                	}, 1500);
						
					}, null, null, null, "Bạn có muốn sao chép CTTB này ?", null);
				});
	        }
		});
	},
	viewDetailActionLog: function(id){
		$('.ErrorMsgStyle').hide();
		$('#detailActionLogDiv').show();
		var url = ProgrammeDisplayCatalog.getGridUrlDetailAction(id);		
		if($('#gbox_infoChangeDetailGrid').html() != null && $('#gbox_infoChangeDetailGrid').html().trim().length > 0){
			$("#infoChangeDetailGrid").setGridParam({url:url,page:1}).trigger("reloadGrid");
		} else {
			$("#infoChangeDetailGrid").jqGrid({
				  url:ProgrammeDisplayCatalog.getGridUrlDetailAction(id),
				  colModel:[
				    {name:'columnName',index : 'columnName', label: 'Thông tin thay đổi', sortable:false,resizable:false, align:'left' },
				    {name:'oldValue',index : 'oldValue', label: 'Giá trị cũ', width: 100, sortable:false,resizable:false, align:'right'},
				    {name:'newValue',index : 'newValue', label: 'Giá trị mới', width: 100, sortable:false,resizable:false, align:'right'},
				    {name:'actionDate',index : 'actionDate', label: 'Ngày thay đổi', width: 80,align:'center', sortable:false,resizable:false, formatter:'date', formatoptions: {srcformat:'Y-m-d', newformat:'d/m/Y'} }			    
				  ],	  
				  pager : '#infoChangeDetailPager',			  	  	  
				  width: ($('#infoChangeDetailContainer').width()),
				  gridComplete: function(){
					  $('#jqgh_infoChangeDetailGrid_rn').html('STT');
					  updateRownumWidthForJqGrid();
				  }
				})
				.navGrid('#infoChangeDetailPager', {edit:false,add:false,del:false, search: false});
		}				
	},
	getGridUrlDetailAction: function(id){
		return '/catalog/programme-display/search-info-change-detail?id=' + id;
	},
	showDisplayProgramChangeTab: function(){
		$('.ErrorMsgStyle').hide();
		ProgrammeDisplayCatalog.hideAllTab();
		$('#tabTitle').html('Xem thông tin thay đổi');
		$('#tabActive6').addClass('Active');
		$('#container6').show();	
		$('#changedInfoDisplayProgramCode').val($('#code').val());
		$('#changedInfoDisplayProgramName').val($('#name').val());
		var id = $('#selId').val();
		var fromDate = $('#fromDate').val();
		var toDate = $('#toDate').val();
		Utils.bindQuicklyAutoSearch();
		var url = '/catalog/programme-display/search-info-change?id=' + id + '&fromDate=' + fromDate + '&toDate=' + toDate;
		$("#infoChangeGrid").jqGrid({
			  url:url,
			  colModel:[
			    {name:'actionUser',index : 'actionUser', label: 'NV thay đổi', sortable:false,resizable:false, align:'left' },
			    {name:'actionType',index : 'actionType', label: 'Loại thay đổi', width: 100, sortable:false,resizable:false, align:'left', formatter: ProgrammeDisplayCatalogFormatter.actionTypeCellFormatter },
			    {name:'actionDate',index : 'actionDate', label: 'Ngày thay đổi', align:'center',width: 80, sortable:false,resizable:false, formatter:'date', formatoptions: {srcformat:'Y-m-d', newformat:'d/m/Y'} },
			    {name:'actionIp',index : 'actionIp', label: 'IP máy thay đổi', width: 100, sortable:false,resizable:false, align:'left' },
			    {name:'viewDetail', label: 'Xem thay đổi', width: 50, align: 'left',sortable:false,resizable:false, formatter: ProgrammeDisplayCatalogFormatter.detailInfoChangeCellFormatter},			    
			    {name:'id', index : 'id', hidden: true }			    
			  ],	  
			  pager : '#infoChangePager',			  	  	  
			  width: ($('#infoChangeContainer').width()),
			  gridComplete: function(){	
				  $('#jqgh_infoChangeGrid_rn').html('STT');
				  var url = ProgrammeDisplayCatalog.getGridUrlDetailAction(-1);
				  $("#infoChangeDetailGrid").setGridParam({url:url,page:1}).trigger("reloadGrid");
				  updateRownumWidthForJqGrid();			  
			  }
			}).navGrid('#infoChangePager', {edit:false,add:false,del:false, search: false});	
	},
	exportActionLog: function(){
		$('.ErrorMsgStyle').hide();
		var id = $('#selId').val();
		var fromDate = $('#fromDate').val().trim();
		var toDate = $('#toDate').val().trim();
		ExportActionLog.exportActionLog(id, ExportActionLog.DISPLAY_PROGRAM, fromDate, toDate,'errMsgActionLog');
	},
	
	getGridUrl: function(code,name,status,fDate,tDate){		
		return "/programme-display/search?code=" + encodeChar(code) + "&name=" + encodeChar(name) + "&status=" + status + "&fromDate=" + encodeChar(fDate) + "&toDate=" + encodeChar(tDate);
	},	
	search: function(){
		$('.ErrorMsgStyle').hide();
		var code = $('#code').val().trim();
		var name = $('#name').val().trim();
		var status = $('#status').val().trim();;
		if($('#permissionUser').val() == 'true' ){
			status = $('#status').val().trim();
		}
		var fDate = $('#fDate').val();
		var tDate = $('#tDate').val();

		var msg=''; 
		var startDate = $('#fDate').val().trim();
		var endDate = $('#tDate').val().trim();
		if (startDate != '' && !Utils.isDate(startDate)) {
			msg = 'Từ ngày không hợp lệ, vui lòng nhập theo định dạng dd/MM/yyyy';
			$('#fDate').focus();
		}
		if(msg.length == 0){
			if(endDate != '' && !Utils.isDate(endDate)){
				msg = 'Đến ngày không hợp lệ, vui lòng nhập theo định dạng dd/MM/yyyy';
				$('#tDate').focus();
			}
		}
		if(msg.length == 0){
			if(startDate != '' && endDate != ''){
				if(!Utils.compareDate(startDate, endDate)){
					msg = 'Giá trị Từ ngày phải nhỏ hơn hoặc bằng giá trị Đến ngày.';				
				}
			}
		}
		if(msg.length > 0){
			$('#errMsg').html(msg).show();
			return false;
		}
		var params = new Object();
		params.code = $('#code').val().trim();
		params.name = $('#name').val().trim();
		params.fDate = $('#fDate').val();
		params.tDate = $('#tDate').val();	
		params.status = $('#status').val();
		var url = ProgrammeDisplayCatalog.getGridUrl(code,name,status,fDate,tDate);
		$("#listCTTBGrid").datagrid('load',{code:code,name:name,status:status,fromDate:fDate,toDate:tDate});
		var tm = setTimeout(function() {
			$('#code').focus();
		}, 500);
		return false;
	},
	saveInfo: function(){
		var msg = '';
		var err = '';
		$('.ErrorMsgStyle').hide();
		var fDate = $('#fDate').val();
		var tDate = $('#tDate').val();
		msg = Utils.getMessageOfRequireCheck('code','Mã CTTB');
		err = '#code';
		if(msg.length == 0){
			msg = Utils.getMessageOfSpecialCharactersValidate('code','Mã CTTB',Utils._CODE);
			err = '#code';
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfRequireCheck('name','Tên CTTB');
			err = '#name';
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfSpecialCharactersValidate('name','Tên CTTB');
			err = '#name';
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfRequireCheck('status','Trạng thái',true);
		}

		//Kiểm tra rỗng
		if(msg.length == 0){
			msg = Utils.getMessageOfRequireCheck('fDate','Từ ngày');
			err = '#fDate';
		}
		if(msg.length == 0 && $('#fDate').val()!='' && !Utils.isDate($('#fDate').val(),'/')){
			msg = 'Từ ngày không tồn tại hoặc không đúng định dạng dd/mm/yyyy.';
			err = '#fDate';
		}
		if(msg.length == 0 && $('#fDate').val()!=null){
			msg = Utils.getMessageOfInvalidFormatDate('fDate', 'Từ ngày');
			err = '#fDate';
		}
		var status = $('#hidStatus').val();
		if(msg.length == 0 && status == 2){
				if(!Utils.compareDate(Utils.currentDate(), $('#fDate').val())){
					msg = 'Từ ngày phải lớn hơn ngày hiện tại';	
				}				
				err = '#fDate';
		}
		
		if(msg.length == 0 && $('#tDate').val()!='' && !Utils.isDate($('#tDate').val(),'/')){
			msg = 'Đến ngày không tồn tại hoặc không đúng định dạng dd/mm/yyyy.';
			err = '#tDate';
		}
		
		if(msg.length == 0 && $('#tDate').val()!=''){
			msg = Utils.getMessageOfInvalidFormatDate('tDate', 'Đến ngày');
			err = '#tDate';
		}
		
		if(msg.length == 0 && $('#startDate').val()!=null && $('#endDate').val()!=null){
			if(!Utils.compareDate($('#startDate').val().trim(), $('#endDate').val().trim())){
				msg = 'Từ ngày phải nhỏ hơn ngày đến ngày';
				err = '#startDate';
			}
		}
		
		if(msg.length == 0 && tDate.length > 0){
			if(!Utils.compareDate(fDate, tDate)){
				msg = msgErr_fromdate_greater_todate;	
				$('#fDate').focus();
			}			
		}
		if(msg.length == 0){
			if(!Utils.compareDate(Utils.currentDate(), $('#tDate').val())){
				msg = 'Đến ngày phải lớn hơn ngày hiện tại';	
			}				
			err = '#fDate';
		}
		if(msg.length > 0){
			$('#errMsg').html(msg).show();
			$(err).focus();
			setTimeout(function(){$('#errMsg').hide();},3500);
			return false;
		}
		

		/*var value = $('#status').val();
		var statusPermission = $('#statusPermission').val();*/
		var dataModel = new Object();		
		dataModel.id = $('#selId').val().trim();
		dataModel.code = $('#code').val().trim();
		dataModel.name = $('#name').val().trim();
		dataModel.status = $('#status').val();
		dataModel.fromDate = $('#fDate').val().trim();
		dataModel.toDate = $('#tDate').val().trim();		
		Utils.addOrSaveData(dataModel, "/programme-display/save-info", ProgrammeDisplayCatalog._xhrSave, null, function(data){
			$('#selId').val(data.id);
			showSuccessMsg('successMsgInfo',data);
			window.location.href = '/programme-display/change?id=' + data.id;						
		});		
		return false;
	},
	getSelectedProgram: function(id){
		$('#errExcelMsg').html('').hide();
		if(id!= undefined && id!= null){
			location.href = '/programme-display/change?id=' + id;
		} else {
			location.href = '/programme-display/change';
		}		
		return false;
	},
	hideAllTab: function(){
		$('#tab1').removeClass('Active');
		$('#tab2').removeClass('Active');
		$('#tab3').removeClass('Active');
		$('#tab4').removeClass('Active');
		$('#tab5').removeClass('Active');
		$('#tab8').removeClass('Active');
		$('#tab10').removeClass('Active');
		$('#amountTab').removeClass('Active');
		$('#container1').hide();
		$('#container2').hide();
		$('#container3').hide();
		$('#container4').hide();
		$('#container5').hide();
		$('#container6').hide();
		$('#container7').hide();
		$('#container8').hide();
		$('#container10').hide();
		$('#amountContainer').hide();
		$('.ErrorMsgStyle').hide();
	},
	showInfoTab: function(){
		ProgrammeDisplayCatalog.hideAllTab();
		$('#tabTitle').html('Thông tin chương trình trưng bày');
		$('#tabActive1').addClass('Active');
		$('#container1').show();
	},
	showQuotaGroupTab: function(){
		ProgrammeDisplayCatalog.hideAllTab();		
		$('#tabTitle').html('Nhóm chi tiêu');
		$('#tabActive2').addClass('Active');	
		$('#quotaPCode').val($('#code').val());
		$('#quotaPName').val($('#name').val());
		$('#container2').show();		
		$('#quotaCode').focus();
		Utils.bindAutoSearch();	
		var selId = $('#selId').val();
		$("#quotaGrid").jqGrid({
			  url:ProgrammeDisplayCatalog.getQuotaGridUrl(selId,'','','','','',$('#quotaStatus').val()),
			  colModel:[		
			    {name:'displayGroupCode', index : 'displayGroupCode', label: 'Mã nhóm', width: 100, sortable:false,resizable:true , align: 'left'},
			    {name:'displayGroupName', index : 'displayGroupName', label: 'Tên nhóm', sortable:false,resizable:true, align: 'left' },
			    {name:'percentMin',index : 'percentMin', label: '% Doanh số đóng góp', width: 105, sortable:false,resizable:false , align: 'right'},
			    {name:'percentMax',index : 'percentMax', label: '% Doanh số tối đa', width: 105, sortable:false,resizable:false , align: 'right'},
			    {name:'status',index : 'status', label: 'Trạng thái', width: 105, align: 'left', sortable:false,resizable:false, formatter: GridFormatterUtils.statusCellIconFormatter},
			    {name:'add',index: 'add', label: 'Thêm', width: 50, align: 'center',sortable:false,resizable:false, formatter: ProgrammeDisplayCatalogFormatter.addCellIconQuotaGroupFormatter},
			    {name:'edit', index:'edit',label: 'Sửa', width: 50, align: 'center',sortable:false,resizable:false, formatter: ProgrammeDisplayCatalogFormatter.editCellIconQuotaGroupFormatter},
			    {name:'delete', index:'delete',label: 'Xóa', width: 50, align: 'center', sortable:false,resizable:false, formatter: ProgrammeDisplayCatalogFormatter.delCellIconQuotaGroupFormatter},
			    {name:'id',index : 'id', hidden:true },
			    {name:'subCatField',index : 'subCatField', hidden:true },
			  ],	  
			  pager : '#quotaPager',			  	  	  
			  width: ($('#quotaGridContainer').width()),
			  subGrid: true,
			  subGridRowExpanded: function(subgrid_id, row_id) {
				  var subgrid_table_id;
				  subgrid_table_id = subgrid_id+"_t";				  
				  jQuery("#"+subgrid_id).html("<table id='"+subgrid_table_id+"' class='scroll'></table>");
				  var quotaGroupId = '0';
				  quotaGroupId = $("#quotaGrid").jqGrid ('getCell', row_id, 'id');				  
				  jQuery("#"+subgrid_table_id).jqGrid({
			          url:"/catalog/programme-display/search-sub-quota-group?quotaId="+quotaGroupId,
			          datatype: "json",			          
			          colModel: [						
						{name:'product.productName', index : 'name', label: 'Tên sản phẩm', width: 633,sortable:false,resizable:false, align: 'center' },									    
						{name:'product.status',index : 'status', label: 'Trạng thái', width: 119, align: 'left', sortable:false,resizable:false, formatter: GridFormatterUtils.statusCellIconFormatter},						
						{name:'delete', index:'delete',label: 'Xóa', width: 50, align: 'center', sortable:false,resizable:false, formatter: ProgrammeDisplayCatalogFormatter.delCellIconSubQuotaGroupFormatter},
						{name:'id',index : 'id', hidden:true }
			          ],
			          height: 'auto',	
			          shrinkToFit: false,
			          rowNum:100,
			          rownumbers: true,
			          gridComplete: function(){						  
						  $('#jqgh_'+ subgrid_table_id +'_rn').html('STT');
						  if(!checkPermissionEdit()){
							  hideColumnOfJqGrid(subgrid_table_id,'delete');
						  }
						  updateRownumWidthForJqGrid();
						  
					  }
			       });
			  },
			  gridComplete: function(){
				  $('#jqgh_quotaGrid_rn').html('STT');	
				  if(!checkPermissionEdit()){
					  hideColumnOfJqGrid('quotaGrid','add');
					  hideColumnOfJqGrid('quotaGrid','edit');
					  hideColumnOfJqGrid('quotaGrid','delete');
				  }
				  updateRownumWidthForJqGrid();
			  }
			})
			.navGrid('#quotaPager', {edit:false,add:false,del:false, search: false});
		
	},
	
	getQuotaGridUrl: function(id,quotaCode,quotaName,fromPercent,toPercent,fieldMap,quotaStatus){
		return '/catalog/programme-display/search-quota-group?id=' + encodeChar(id) + '&quotaCode=' + encodeChar(quotaCode) + '&quotaName=' + encodeChar(quotaName) + '&fromPercent=' + encodeChar(fromPercent) + '&toPercent=' + encodeChar(toPercent) + '&productSubCatCode='+ encodeChar(fieldMap) + '&status=' + encodeChar(quotaStatus);
	},
	searchQuota: function(){
		$('.ErrorMsgStyle').hide();
		var id = $('#selId').val();
		if(fieldMap == '-2'){
			fieldMap = '';
		}
		var quotaCode = $('#quotaCode').val().trim();
		var quotaName = $('#quotaName').val().trim();		
		var fromPercent = $('#fromPercent').val();
		var toPercent = $('#toPercent').val();
		var quotaStatus = $('#quotaStatus').val();
		var fieldMap = fieldMap;
		$('#quotaCode').focus();
		var url = ProgrammeDisplayCatalog.getQuotaGridUrl(id, quotaCode, quotaName, fromPercent,toPercent,fieldMap, quotaStatus);
		$("#quotaGrid").setGridParam({url:url,page:1}).trigger("reloadGrid");
		$('#errMsgQuotaGroup').html('').hide();
	},
	dataGridLevel:function(){
		var selId = $('#selId').val();
		var title = '';
		if($('#statusPermission').val().trim() == activeType.WAITING){
			title = '<a href="javascript:void(0);" onclick="return ProgrammeDisplayCatalog.openLevelDialog();"><img src="/resources/images/icon_add.png"></a>';
		}	
		var url = '/programme-display/search-level?id=' + encodeChar(selId);
		$('#levelGrid').datagrid({
			url : url,
			total:100,
			width:$('#levelGridContainer').width() -20,
			scrollbarSize : 0,
			rownumbers:true,
			singleSelect:true,
			autoRowHeight:true,
			fitColumns:true,
			columns:[[  
			{field: 'levelCode',title: 'Mức', width:150,align:'left',sortable : false,resizable : false,formatter:function(value,row,index){
				return Utils.XSSEncode(row.levelCode);
			}
			},
			{field: 'amount',title: 'Doanh số(VNĐ/thùng)', width:150,align:'right',sortable : false,resizable : false,
				formatter:function(value){
		    		return formatCurrency(value);	
		    	}
			},
			{field: 'quantity',title: 'Số lượng thùng', width:80,align:'right',sortable : false,resizable : false,
				formatter:function(value){
		    		return formatCurrency(value);	
		    	}
			},
			{field: 'numSku',title: 'Số SKU', width:100,align:'right',sortable : false,resizable : false,
				formatter:function(value){
		    		return formatCurrency(value);	
		    	}
			},
			{field: 'edit',title:'CT', width:40,align:'center',sortable : false,resizable : false,
				formatter: function(value,row,index){
					if($('#permissionUser').val() == 'true'){
						var amount = '';
						var sku = '';
						var quantity = '';
						if(row.amount != null && row.amount != undefined){
							amount = row.amount;
						}
						if(row.numSku != null && row.numSku != undefined){
							sku = row.numSku;
						}
						if(row.numSku != null && row.numSku != undefined){
							sku = row.numSku;
						}
						if(row.quantity != null && row.quantity != undefined){
							quantity = row.quantity;
						}
						
						if($('#statusPermission').val().trim() == activeType.WAITING){
							return "<a href='javascript:void(0)' onclick=\"return ProgrammeDisplayCatalog.openLevelDialog('"+row.id+"','"+row.levelCode+"','"+amount+"','"+sku+"','WAITING','"+quantity+"');\"><img  width='15' height='16' src='/resources/images/icon-edit.png'/></a>";
						}
						if($('#statusPermission').val().trim() == activeType.RUNNING){
							return "<a href='javascript:void(0)' onclick=\"return ProgrammeDisplayCatalog.openLevelDialog('"+row.id+"','"+row.levelCode+"','"+amount+"','"+sku+"','RUNNING','"+quantity+"');\"><img  width='15' height='16' src='/resources/images/icon-edit.png'/></a>";
						}
						if($('#statusPermission').val().trim() == activeType.STOPPED){
							return "<a href='javascript:void(0)' onclick=\"return ProgrammeDisplayCatalog.openLevelDialog('"+row.id+"','"+row.levelCode+"','"+amount+"','"+sku+"','STOPPED','"+quantity+"');\"><img  width='15' height='16' src='/resources/images/icon-edit.png'/></a>";
						}
					}else{
						var amount = '';
						var sku = '';
						var quantity = '';
						if(row.amount != null && row.amount != undefined){
							amount = row.amount;
						}
						if(row.numSku != null && row.numSku != undefined){
							sku = row.numSku;
						}
						if(row.quantity != null && row.quantity != undefined){
							quantity = row.quantity;
						}
						if($('#statusPermission').val().trim() == activeType.RUNNING){
							return "<a href='javascript:void(0)' onclick=\"return ProgrammeDisplayCatalog.openLevelDialog('"+row.id+"','"+Utils.XSSEncode(row.levelCode)+"','"+Utils.XSSEncode(amount)+"','"+Utils.XSSEncode(sku)+"','RUNNING','"+Utils.XSSEncode(quantity)+"');\"><img  width='15' height='16' src='/resources/images/icon-edit.png'/></a>";
						}else{
							return "";
						}
					}
        	    }
			},
			{field: 'remove',title:title, width:40,align:'center',sortable : false,resizable : false,
				formatter: function(value,row,index){
					if($('#permissionUser').val() == 'true'){
						if($('#statusPermission').val().trim() == activeType.WAITING){
							return "<a href='javascript:void(0)' onclick=\"return ProgrammeDisplayCatalog.deleteLevelRow('"+row.id+"');\"><img src='/resources/images/icon_delete.png'/></a>";
						}
					}else{
						return "";
					}
        	    }
			}
            ]],
			method : 'GET',
            onLoadSuccess:function(data){
            	ProgrammeDisplayCatalog._lstLevelCode.put(0, data);
            	$('.datagrid-header-rownumber').html('STT');
            	$('#levelGrid').datagrid('resize');
            }
		});
	},
	showLevelTab: function(){
		ProgrammeDisplayCatalog.hideAllTab();
		$('#tab3').addClass('Active');
		$('#container3').show();
		ProgrammeDisplayCatalog.dataGridLevel();
	},
	openLevelDialog : function(levelId,levelCode,amount,numSku,status,quantityLv) {
		$('.ErrorMsgStyle').hide();
		if(levelId != null && levelId != undefined){
			$('#levelId').val(levelId);
		}else{
			$('#levelId').val(0);
			levelId = 0;
		}
		$('#divLevelContainer').show();
		var html = $('#dialogLevel').html();
		$('#dialogLevel').dialog({  
	        closed: false,  
	        cache: false,  
	       // modal: true,
	        position: 'middle',
	        center: true, 
	        onOpen: function(){
		   		$('#dialogLevel').bind('keypress',function(event){
		   			if(event.keyCode == keyCodes.ENTER){		   				
	   					if(!$('#dialogLevel #btnChangeLevel').is(':hidden')){
	   						$('#dialogLevel #btnChangeLevel').click();
	   					}
		   			}
		   		});    
	        	Utils.bindFormatOntextfieldCurrencyFor('amountDlg', Utils._TF_NUMBER);
	        	Utils.bindFormatOntextfieldCurrencyFor('skuDlg', Utils._TF_NUMBER);
	        	Utils.bindFormatOntextfieldCurrencyFor('quantityLv', Utils._TF_NUMBER);
	        	
	        	$('.easyui-dialog #errMsgLevelDlg').html('').hide();
	        	ProgrammeDisplayCatalog.dataGridLevelOnDialog('amountLevelGrid','amountLevelContainer','amount',status,levelId);
	        	ProgrammeDisplayCatalog.dataGridDisplayLevelOnDialog('displayLevelGrid','displayLevelContainer','display',status,levelId);
	        	if(levelId != null && levelId != undefined && Number(levelId)>0){
	        		$('#levelCodeDlg').val(levelCode);
	        		$('#levelCodeDlg').attr('disabled','disabled');
	        		$('#amountDlg').focus();
	        		$('#amountDlg').val(formatFloatValue(amount));
	        		$('#skuDlg').val(formatFloatValue(numSku));
	        		$('#quantityLv').val(formatFloatValue(quantityLv));
	        	}else{
	        		$('#levelCodeDlg').removeAttr('disabled');
	        		$('#levelCodeDlg').val('');
	        		$('#levelCodeDlg').focus();
	        		$('#amountDlg').val('');
	        		$('#skuDlg').val('');
	        		$('#quantityLv').val('');
	        	}
	        	if(status == 'RUNNING' || status == 'STOPPED'){
	        		$('#amountDlg').attr('disabled','disabled');
	        		$('#skuDlg').attr('disabled','disabled');
	        		$('#quantityLv').attr('disabled','disabled');
	        		$('#btnChangeLevel').text('Đóng').bind('click',function(){
	        			$('#dialogLevel').dialog('close'); 
	        		}).attr('onclick','');
	        	}
	        },
	        onBeforeClose: function() {
	        },
	        onClose : function(){
	        	$('#dialogLevel').unbind('keypress');
	        	$('#divLevelContainer').hide();
	        }
	    });
		return false;
	},
	dataGridLevelOnDialog:function(gridId,gridContainerId,type,status,levelId){
		var selId = $('#selId').val().trim();
		var url = '';
		if(type == 'amount'){
			url = '/programme-display/search-level-product-group?id=' + encodeChar(selId) + '&typeGroup=1&levelId='+levelId;
		}else{
			url = '/programme-display/search-level-product-group?id=' + encodeChar(selId) + '&typeGroup=4&levelId='+levelId;
		}
		$('.easyui-dialog #'+gridId).datagrid({
			url : url,
			total:100,
			width:$('.easyui-dialog #'+gridContainerId).width() -20,			
			scrollbarSize : 0,
			rownumbers:true,
			fitColumns:true,			
			rownum:10,
			columns:[[  
			{field: 'displayProductGroupCode',title: 'Mã nhóm', width:130,align:'left',sortable : false,resizable : false,formatter:function(value,row,index){
				return Utils.XSSEncode(row.displayProductGroupCode);
			}
			},
			{field: 'displayProductGroupName',title: 'Tên nhóm', width:130,align:'left',sortable : false,resizable : false,formatter:function(value,row,index){
				return Utils.XSSEncode(row.displayProductGroupName);
			}
			},
			{field: 'type',title: 'Loại nhóm', width:100,align:'left',sortable : false,resizable : false,
				formatter:function(v,r,i){
					if(r.type==null) return '';
					if(r.type=='SL') return 'Sản lượng';
					if(r.type=='DS') return 'Doanh số';
					if(r.type=='SKU') return 'SKU';
				}
			},
			{field: 'min',title: 'Min', width:110,align:'right',sortable : false,resizable : false,
				formatter:function(value,row,index){
					if(status == 'RUNNING' || status == 'STOPPED'){
						return '<input style="width:90px" class="minItem" id="'+type+'_min_'+row.id+'" readonly="readonly" />';
					}else{
						return '<input style="width:90px" class="minItem" id="'+type+'_min_'+row.id+'" onfocus="return Utils.bindFormatOntextfieldCurrencyFor(\''+type+'_min_'+row.id+'\','+Utils._TF_NUMBER+');" onkeypress="EasyUiCommon.eventOnTextField(event,this,\'minItem\')"/>';
					}
				}
			},
			{field: 'max',title: 'Max', width:110,align:'right',sortable : false,resizable : false,
				formatter:function(value,row,index){
					if(status == 'RUNNING' || status == 'STOPPED'){
						return '<input style="width:90px" class="maxItem" id="'+type+'_max_'+row.id+'" readonly="readonly" />';
					}else{
						return '<input style="width:90px" class="maxItem" id="'+type+'_max_'+row.id+'" onfocus="return Utils.bindFormatOntextfieldCurrencyFor(\''+type+'_max_'+row.id+'\','+Utils._TF_NUMBER+');" onkeypress="EasyUiCommon.eventOnTextField(event,this,\'maxItem\')"/>';
					}
				}
			},
			{field: 'percent',title: '% đóng góp', width:110,align:'right',sortable : false,resizable : false,
				formatter:function(value,row,index){
					if(status == 'RUNNING' || status == 'STOPPED'){
						return '<input style="width:90px" class="percentItem" id="'+type+'_percent_'+row.id+'" readonly="readonly" onfocus="return Utils.bindFormatOntextfieldCurrencyFor(\''+type+'_percent_'+row.id+'\','+Utils._TF_NUMBER+');" onkeypress="EasyUiCommon.eventOnTextField(event,this,\'percentItem\')"/>';
					}
					return '<input style="width:90px" class="percentItem" id="'+type+'_percent_'+row.id+'" onfocus="return Utils.bindFormatOntextfieldCurrencyFor(\''+type+'_percent_'+row.id+'\','+Utils._TF_NUMBER+');" onkeypress="EasyUiCommon.eventOnTextField(event,this,\'percentItem\')"/>';
					
				}
			},
            ]],
			method : 'GET',
            onLoadSuccess:function(){
            	$('.easyui-dialog .datagrid-header-rownumber').html('STT');
            	$('.easyui-dialog #'+gridId).datagrid('resize');
            	$('#loadingLevelDialog').hide();
            	if($('#levelId').val() != '' && $('#levelId').val() > 0){
            		$.getJSON('/rest/catalog/display-program/level/detail/' + $('#levelId').val() + ".json",function(data){
	        			for(var i = 0;i<data.length;i++){
	        				if(data[i].displayProductGroup!=null){
	        					if(data[i].displayProductGroup.type == 'SKU' || data[i].displayProductGroup.type == 'DS' || data[i].displayProductGroup.type == 'SL'){
		        					$('#amount_min_'+data[i].displayProductGroup.id).val(data[i].min);
		        					$('#amount_max_'+data[i].displayProductGroup.id).val(data[i].max);
		        					$('#amount_percent_'+data[i].displayProductGroup.id).val(data[i].precent);
		        				}else{
		        					$('#sku_min_'+data[i].displayProductGroup.id).val(data[i].min);
		        					$('#sku_max_'+data[i].displayProductGroup.id).val(data[i].max);
		        					$('#sku_percent_'+data[i].displayProductGroup.id).val(data[i].precent);
		        				}
	        				}
	        			}
	        		});
            	}else{
            		$('.minItem,.maxItem,.percentItem').each(function(){
            			$(this).val('');
            		});            		
            	}
            }
		});
	},
	dataGridDisplayLevelOnDialog:function(gridId,gridContainerId,type,status,levelId){
		var selId = $('#selId').val().trim();
		ProgrammeDisplayCatalog.displayProductMap = new Map();
		var url = '/programme-display/search-level-product-group?id=' + encodeChar(selId) + '&typeGroup=4&levelId='+levelId;
		$('.easyui-dialog #'+gridId).datagrid({
			view: detailview,
			url:url,
			singleSelect:true,
			width:$('.easyui-dialog #'+gridContainerId).width() -20,
			scrollbarSize : 0,
			rownumbers:true,
			fitColumns:true,
			rownum:10,
			columns:[[  
			   {field: 'displayProductGroupName',title: 'Tên nhóm', width:130,align:'left',sortable : false,resizable : false,formatter:function(value,row,index){
				   return Utils.XSSEncode(row.displayProductGroupName);
			   }
			}
            ]],
			method : 'GET',
            onLoadSuccess:function(){
            	$('.easyui-dialog .datagrid-header-rownumber').html('STT');
            	$('.easyui-dialog #'+gridId).datagrid('resize');
            	$('#loadingLevelDialog').hide();            	
            	/** Lay danh sach san pham thuoc nhom */
            	var rows = $('.easyui-dialog #'+gridId).datagrid('getRows');
            	var groupDtlIdLst = new Array();
            	for(var i=0,size = rows.length;i<size;++i){
            		groupDtlIdLst.push(rows[i].id);
            	}           
            	var params = new Object();
            	params.groupDtlIdLst = groupDtlIdLst;
            	params.levelId = levelId;  
            	ProgrammeDisplayCatalog._lstMapProductsDP = new Map();
            	Utils.getJSONDataByAjaxNotOverlay(params,'/programme-display/search-product-detail-ex',function(data){
            		var result = data.rows;
            		for(var i=0,size = result.length;i<size;++i){
            			ProgrammeDisplayCatalog._lstMapProductsDP.put(result[i].groupId.toString(),result[i].displayPDGroupDTLVOs);
                	} 
            		var tmvv = setTimeout(function(){
                		var arrayList = $('.easyui-dialog .datagrid-row-expander');
                    	for(var i=0;i<arrayList.length;++i){
                    		var obj = $(arrayList[i]);
                    		if(!obj.is(':hidden')){                    			
                        		obj.click(); 
                    		}                    		                 		
                    	}
                    	clearTimeout(tmvv);
                	}, 500);
            	});
            	
            	
            }, 
            detailFormatter:function(index,row){  
		     		return '<div style="padding:2px; width:850"><table id="ddt-' + index + '"></table></div>';
		     },
            onExpandRow: function(index,row){
            	var tm = setTimeout(function(){
            		$('.datagrid-header-rownumber').html('STT');
                	var gridDetailId = '';
                	gridDetailId = 'ddt-'+index;
                	ProgrammeDisplayCatalog.dataGridDisplayLevelDetail(gridId,gridDetailId,row.id,index,levelId,status);  
                	var tm = setTimeout(function(){
                		$('.easyui-dialog #'+gridId).datagrid('fixDetailRowHeight',index); 
                		$('.easyui-dialog #'+gridId).datagrid('resize');                   	      	
    				}, 500);   
                	var data = ProgrammeDisplayCatalog._lstMapProductsDP.get(row.id.toString());
                	$('.easyui-dialog #'+gridDetailId).datagrid('loadData',data);
                	clearTimeout(tm);
            	}, 1000);
              } 
		});
	},
	dataGridDisplayLevelDetail:function(gridId,gridDetailId,groupId,index,levelId,status){
		$('#gridDetailId').val(gridDetailId);
		var id = $('#selId').val().trim();		
		$('.easyui-dialog #'+gridDetailId).datagrid({
			width:$('.GridSection').width() -40,		
			scrollbarSize : 0,
			fitColumns:true,
			singleSelect:true,
			rownumbers:true,			
			columns:[[  
			{field: 'productCode',title: 'Mã sản phẩm', width:150,align:'left',sortable : false,resizable : false,formatter:function(value,row,index){
				return Utils.XSSEncode(row.productCode);
			}},
			{field: 'productName',title: 'Tên sản phẩm', width:470,align:'left',sortable : false,resizable : false,formatter:function(value,row,index){
				return Utils.XSSEncode(row.productName);
			}},
			{field: 'quantity',title:"Số lượng", width:150,align:'center',sortable : false,resizable : false,
				formatter: function(value,row,index){
					var type="display";
					if(status == 'RUNNING' || status == 'STOPPED'){
						return '<input  maxlength="8" onkeypress="NextAndPrevTextField(event,this,\'quantityItem\')" prcode="'+ Utils.XSSEncode(row.productCode) +'" grcode="'+ Utils.XSSEncode(row.groupCode) +'" style="width:90px;text-align: right;" class="quantityItem" id="'+type+'_quantity_'+row.id+'" disabled="disabled" />';
					}else{
						return '<input  maxlength="8" onkeypress="NextAndPrevTextField(event,this,\'quantityItem\');" prcode="'+ Utils.XSSEncode(row.productCode) +'" grcode="'+ Utils.XSSEncode(row.groupCode) +'" style="width:90px;text-align: right;" class="quantityItem" id="'+type+'_quantity_'+row.id+'"/>';
					}
				}
			}
	        ]],
			onResize:function(){
	        	$('#'+gridId).datagrid('fixDetailRowHeight',index);  
	        },
	        onLoadSuccess:function(){	        	
	        	var tmBanChar = setTimeout(function(){
					 $('.window').each(function(){
			  			  if(!$(this).is(':hidden')){
			  				  var top = ($(window).height() / 2) - ($(this).outerHeight() / 2);
			  				  var left = ($(window).width() / 2) - ($(this).outerWidth() / 2);
			  				  if(Number(top)<0){
			  					  top = 50;
			  				  }
			  				  $(this).css({ top: top, left: left});
			  			  }
					 });	
					 clearTimeout(tmBanChar);
				 },20); 
	        	Utils.bindFormatOnTextfieldInputCss('quantityItem',Utils._TF_NUMBER);
	        	$('.quantityItem').val('');
	        	$('.datagrid-header-rownumber').html('STT');
	        	setTimeout(function(){
	        		$('.easyui-dialog #'+gridId).datagrid('fixDetailRowHeight',index);
	        		$('.easyui-dialog #'+gridId).datagrid('fixRowHeight',index);
	        	},500);	   
	        	var rows = $('.easyui-dialog #'+gridDetailId).datagrid('getRows');
	        	for(var i=0,sz = rows.length;i<sz;++i){
	        		var obj = new Object();
	        		var grcode = rows[i].groupCode;
	        		var prcode =  rows[i].productCode;	      
	        		obj.groupCode = grcode;
	        		obj.productCode = prcode;
	        		obj.groupId = groupId;
	        		obj.quantity = rows[i].quantity;
	        		var objtmp = ProgrammeDisplayCatalog.displayProductMap.get(grcode+prcode);
	        		if(objtmp!=null){
	        			obj.quantity = objtmp.quantity;
	        		}
	        		ProgrammeDisplayCatalog.displayProductMap.put(grcode+prcode,obj);
	        	}
	        	if($('#levelId').val() != '' && $('#levelId').val() > 0){
	        		if(ProgrammeDisplayCatalog.displayProductMap.size()==0){
	        			$.getJSON('/rest/catalog/display-program/level/product/' + $('#levelId').val() + ".json",function(data){
		        			for(var i = 0;i<data.length;i++){
		        				$('#display_quantity_'+data[i].displayProductGroup.id).val(data[i].min);
		        				var grcode = $('#display_quantity_'+data[i].id).attr('grcode');		        				
		        				var productCode = $('#display_quantity_'+data[i].id).attr('prcode');
		        				var obj = ProgrammeDisplayCatalog.displayProductMap.get(grcode+prcode);
		        				obj.quantity = data[i].quantity;	
		        				ProgrammeDisplayCatalog.displayProductMap.put(grcode+prcode,obj);		        				
		        			}
		        		});
	        		}else{
	        			$('.quantityItem').each(function(){
	        				var grcode = $(this).attr('grcode');
	        				var prcode = $(this).attr('prcode');
	        				var obj = ProgrammeDisplayCatalog.displayProductMap.get(grcode+prcode);	   
	        				if(obj!=null && (obj.quantity!=null || Number(obj.quantity)>0))
	        					$(this).val(obj.quantity);   				
	        			});
	        		}
	        	}
	        }
		});		
		$('.quantityItem').live('change',function(){
			var grcode = $(this).attr('grcode');
			var prcode = $(this).attr('prcode');
			var quantity = Number($(this).val().trim());
			var obj = ProgrammeDisplayCatalog.displayProductMap.get(grcode+prcode);
			if(quantity!=0){
				obj.quantity = quantity;
				ProgrammeDisplayCatalog.displayProductMap.put(grcode+prcode,obj);
			}			
		});		
	},
	
	getLevelGirdUrl: function(id,levelCode,levelAmount,levelStatus){
		return '/programme-display/search-level?id=' + encodeChar(id) + '&levelCode=' + encodeChar(levelCode) + '&levelAmount=' + encodeChar(levelAmount) + '&status=' + encodeChar(levelStatus);
	},
	searchLevel: function(){
		$('.ErrorMsgStyle').hide();
		var msg = Utils.getMessageOfNegativeNumberCheck('levelAmount','Số định mức',Utils._TF_NUMBER_COMMA);
		if(msg.length > 0){
			$('#errMsgLevel').html(msg).show();
			return false;
		}
		var id = $('#selId').val().trim();		
		var levelCode = $('#levelCode').val().trim();
		var levelAmount =  Utils.returnMoneyValue($('#levelAmount').val().trim());	
		var levelStatus = $('#levelStatus').val();
		var url = ProgrammeDisplayCatalog.getLevelGirdUrl(id,levelCode,levelAmount,levelStatus);
		$("#levelGrid").setGridParam({url:url,page:1}).trigger("reloadGrid");
		$('#levelCode').focus();
	},
	/*  locHP 18/09/2013 - showAmountTab */
	showAmountTab: function(){
		ProgrammeDisplayCatalog.hideAllTab();
		$('#tabTitle').html('Sản phẩm của chương trình trưng bày');
		$('#amountTab').addClass('Active');
		$('#amountContainer').show();		
		var selId = $('#selId').val();
		var url = '';
		url = '/programme-display/search-amount-product?id=' + encodeChar(selId) + '&typeGroup=1';
		ProgrammeDisplayCatalog.dataGridProductGroup2('amountGrid',url,ProgrammeDisplayCatalog.AMOUNT);
	}	/*  locHP 18/09/2013 - end showAmountTab */
	,
	showProductTab: function(){
		ProgrammeDisplayCatalog.hideAllTab();
		$('#tabTitle').html('Sản phẩm của chương trình trưng bày');
		$('#tab2').addClass('Active');
		$('#container4').show();		
		var selId = $('#selId').val();
		var url = '';		
		url = '/programme-display/search-product?id=' + encodeChar(selId) + '&typeGroup=4';
		ProgrammeDisplayCatalog.dataGridProductGroup('cbGrid',url,ProgrammeDisplayCatalog.DISPLAY);
	},
	dataGridProductGroup: function(gridId,url,type){
		var title = '';
		if($('#permissionUser').val() == 'true'){
			if($('#statusPermission').val().trim() == activeType.WAITING){
				title = '<a href="javascript:void(0);" onclick=\"return ProgrammeDisplayCatalog.openGroupDialog('+type+');\"><img width="15" height="16" src="/resources/images/icon_add.png"></a>';
			}
		}
		$('#'+gridId).datagrid({
			view: detailview,
			url : url,
	//		pagination : true,
			fitColumns:true,
			width:$('.GridSection').width() -20,
			scrollbarSize : 0,
			rownumbers:true,
			singleSelect : true,
		//	rownum:10,
		//	pageList:[10,20,30],
			autoRowHeight:true,
			columns:[[  
			{field: 'displayProductGroupCode',title: 'Mã nhóm', width:320,align:'left',sortable : false,resizable : false,formatter:function(value,row,index){
				return Utils.XSSEncode(row.displayProductGroupCode);
			}
			},
			{field: 'displayProductGroupName',title: 'Tên nhóm', width:500,align:'left',sortable : false,resizable : false,
				formatter:function(v,r,index){
					return '<div style="word-wrap: break-word;">' + Utils.XSSEncode(v) + '</div>';
				}
				 
			}, 
			{field: 'edit',title:'', width:70,align:'center',sortable : false,resizable : false,
				formatter: function(value,row,index){ /* viet them kiem tra "trang thai"  */
					if($('#permissionUser').val() == 'true'){
						if($('#statusPermission').val().trim() == activeType.WAITING){
							return "<a href='javascript:void(0)' onclick=\"return ProgrammeDisplayCatalog.openGroupDialog('"+type+"','"+row.id+"','"+Utils.XSSEncode(row.displayProductGroupCode)+"','"+Utils.XSSEncode(row.displayProductGroupName)+"');\"><img width='15' height='16' src='/resources/images/icon-edit.png'/></a>";
						}
					}else{
						return "";
					}
        	    }
			},
			{field: 'remove',title:title, width:70,align:'center',sortable : false,resizable : false,
				formatter: function(value,row,index){/* viet them kiem tra "trang thai"  */
					if($('#permissionUser').val() == 'true'){
						if($('#statusPermission').val().trim() == activeType.WAITING){
							return "<a href='javascript:void(0)' onclick=\"return ProgrammeDisplayCatalog.deleteGroupRow('"+row.id+"','"+type+"');\"><img width='15' height='16' src='/resources/images/icon_delete.png'/></a>";
						}
					}else{
						return "";
					}
        	    }
			}
            ]],
			method : 'GET',
		    detailFormatter:function(index,row){  
		    	if(type == ProgrammeDisplayCatalog.AMOUNT){
		    		return '<div style="padding:2px; width:850"><table id="ddv-' + index + '"></table></div>';  
		    	}else{
		    		return '<div style="padding:2px; width:850"><table id="ddt-' + index + '"></table></div>';
		    	}
            },
            onLoadSuccess:function(){
            	$('#'+gridId).datagrid('resize');
            	$('.datagrid-header-rownumber').html('STT');
            },
            onExpandRow: function(index,row){
            	var gridDetailId = '';
            	if(type == ProgrammeDisplayCatalog.AMOUNT){
            		gridDetailId = 'ddv-'+index;
            		if(row.type != 'SL'){
            			ProgrammeDisplayCatalog.dataGridDetailAMOUNT(gridId,gridDetailId,row.id,index);
            		}else{
            			ProgrammeDisplayCatalog.dataGridDetailSKU(gridId,gridDetailId,row.id,index);
            		}
            	}else{
            		gridDetailId = 'ddt-'+index;
            		ProgrammeDisplayCatalog.dataGridDetailCB(gridId,gridDetailId,row.id,index);
            	}
            	$('#'+gridId).datagrid('fixDetailRowHeight',index); 
            	$('#'+gridId).datagrid('resize');
            } 
		});
	},
	dataGridProductGroup2: function(gridId,url,type){
		var title = '';
		if($('#permissionUser').val() == 'true'){
			if($('#statusPermission').val().trim() == activeType.WAITING){
				title = '<a href="javascript:void(0);" onclick=\"return ProgrammeDisplayCatalog.openGroupDialog2('+type+');\"><img width="15" height="16" src="/resources/images/icon_add.png"></a>';
			}
		}
		$('#'+gridId).datagrid({
			view: detailview,
			url : url,
			pagination : false,
			fitColumns:true,
			width:$(window).width() - 55,
			scrollbarSize : 0,
			rownumbers:true,
			
			autoRowHeight:true,
			nowrap : false,
			columns:[[  
			{field: 'displayProductGroupCode',title: 'Mã nhóm', width:273,align:'left',sortable : false,resizable : false,formatter:function(value,row,index){
				return Utils.XSSEncode(row.displayProductGroupCode);
			}
			},
			{field: 'displayProductGroupName',title: 'Tên nhóm', width:500,align:'left',sortable : false,resizable : false,
				formatter:function(v,r,index){
					return '<div style="word-wrap: break-word">' + Utils.XSSEncode(v) +'</div>';
				}
			}, 
			{field: 'type', title: 'Loại nhóm', width:80, align: 'left', sortable: false, resizable: false,
				formatter: function(value, row, index){
					if (row.type== 'DS')
						return 'Doanh số';
					if (row.type== 'SL')
						return 'Sản lượng';
					if (row.type == 'SKU')
						return 'SKU';
				}
			},
			{field: 'edit',title:'', width:70,align:'center',sortable : false,resizable : false,
				formatter: function(value,row,index){ /* viet them kiem tra "trang thai"  */
					if($('#permissionUser').val() == 'true'){
						if($('#statusPermission').val().trim() == activeType.WAITING){
							return "<a href='javascript:void(0)' onclick=\"return ProgrammeDisplayCatalog.openGroupDialog3('"+row.type+"','"+row.id+"','"+Utils.XSSEncode(row.displayProductGroupCode)+"','"+Utils.XSSEncode(row.displayProductGroupName)+"');\"><img width='15' height='16' src='/resources/images/icon-edit.png'/></a>";
						}
					}else{
						return "";
					}
        	    }
			},
			{field: 'remove',title:title, width:70,align:'center',sortable : false,resizable : false,
				formatter: function(value,row,index){/* viet them kiem tra "trang thai"  */
					if($('#permissionUser').val() == 'true'){
						if($('#statusPermission').val().trim() == activeType.WAITING){
							return "<a href='javascript:void(0)' onclick=\"return ProgrammeDisplayCatalog.deleteGroupRow('"+row.id+"','"+type+"');\"><img width='15' height='16' src='/resources/images/icon_delete.png'/></a>";
						}
					}else{
						return "";
					}
        	    }
			}
            ]],
			method : 'GET',
		    detailFormatter:function(index,row){  
		    	if(type == ProgrammeDisplayCatalog.AMOUNT){
		    		return '<div style="padding:2px; width:850"><table id="ddv-' + index + '"></table></div>';  
		    	}else{
		    		return '<div style="padding:2px; width:850"><table id="ddt-' + index + '"></table></div>';
		    	}
            },
            onLoadSuccess:function(){
            	$('#'+gridId).datagrid('resize');
               	$('.datagrid-header-rownumber').html('STT');
            },
            onExpandRow: function(index,row){

            	var gridDetailId = '';
            	if(type == ProgrammeDisplayCatalog.AMOUNT){
            		gridDetailId = 'ddv-'+index;
//            		if(row.type != 'SL'){
//            			ProgrammeDisplayCatalog.dataGridDetailAmount2(gridId,gridDetailId,row.id,index);
//            		}else if(row.type = 'SKU'){
//            			ProgrammeDisplayCatalog.dataGridDetailSKU2(gridId,gridDetailId,row.id,index);
//            		}
            		if (row.type == 'SL'){
            			ProgrammeDisplayCatalog.dataGridDetailSKU2(gridId,gridDetailId,row.id,index);
            		}else if (row.type == 'DS') {
            			ProgrammeDisplayCatalog.dataGridDetailAmount2(gridId,gridDetailId,row.id,index);
            		} else {
            			ProgrammeDisplayCatalog.dataGridDetailAmount3(gridId,gridDetailId,row.id,index);
            		}
            		
            	}
            	$('#'+gridId).datagrid('fixDetailRowHeight',index); 
            	$('#'+gridId).datagrid('resize');
            } 
		});
	},
	dataGridDetailSKU:function(gridId,gridDetailId,groupId,index){
		$('#gridDetailId').val(gridDetailId);
		var id = $('#selId').val().trim();
		var title = '';
		if($('#permissionUser').val() == 'true'){
			if($('#statusPermission').val().trim() == activeType.WAITING){
				title = "<a href='javascript:void(0);' onclick=\"return ProgrammeDisplayCatalog.openProductDialog("+groupId+","+ProgrammeDisplayCatalog.SKU+",null,'"+gridDetailId+"');\"><img src='/resources/images/icon_add.png'></a>";
			}
		}
		/* loc bo catalog  */
		var url = '/programme-display/search-product-detail?id='+ encodeChar(id)+'&groupId=' + encodeChar(groupId);
		$('#'+gridDetailId).datagrid({
			url : url,
			total:100,
			width:$('.GridSection').width() -80,
			scrollbarSize : 0,
			fitColumns:true,
			height:'auto',
			rownumbers:true,
			singleSelect:true,
			columns:[[  
			{field: 'productCode',title: 'Mã sản phẩm', width:350,align:'left',sortable : false,resizable : false,
				formatter:function(value,row,index){
					if(row != null && row != undefined ){
						return Utils.XSSEncode(row.productCode);
					}
				}
			},
			{field: 'productName',title: 'Tên sản phẩm', width:350,align:'left',sortable : false,resizable : false,
				formatter:function(value,row,index){
					if(row != null && row != undefined){
						return Utils.XSSEncode(row.productName);
					}
				}
			},
			{field: 'convfact',title: 'Quy đổi', width:200,align:'right',sortable : false,resizable : false,
				editor:{  
                    type:'numberbox'
				},
				formatter:function(value,row,index){
					if(row != null && row != null){
						if(row.convfact == null || row.convfact == undefined){
							return 0;
						}else{
							return Utils.XSSEncode(row.convfact);
						}
					}
				}
			},
			{field: 'edit',title:'', width:70,align:'center',sortable : false,resizable : false,
				formatter: function(value,row,index){
					if($('#permissionUser').val() == 'true'){
						if($('#statusPermission').val().trim() == activeType.WAITING){
							if (row.editing){  
								var s = "<a href='javascript:void(0)' onclick=\"return ProgrammeDisplayCatalog.saveRowProduct(this,'"+groupId+"','"+gridDetailId+"');\"><span style='cursor:pointer'><img src='/resources/images/icon-datach.png'/></span></a> ";  
								var c = "<a href='javascript:void(0)' onclick=\"ProgrammeDisplayCatalog.cancelRow(this,'"+gridDetailId+"');\"><span style='cursor:pointer'><img src='/resources/images/icon_esc.png'/></span></a>";  
								return s + c;  
							} else {
								return "<a href='javascript:void(0)' onclick=\"ProgrammeDisplayCatalog.editRow(this,'"+gridDetailId+"');\"><span style='cursor:pointer'><img  width='15' height='16'  src='/resources/images/icon-edit.png'/></span></a>";  
							}  
						}
					}else{
						return "";
					}
        	    }
			},
			{field: 'remove',title:title, width:70,align:'center',sortable : false,resizable : false,
				formatter: function(value,row,index){
					if($('#permissionUser').val() == 'true'){
						if($('#statusPermission').val().trim() == activeType.WAITING){
							var productId = '';
							if(row != null && row != undefined && row != undefined){
								productId = row.id;
							}
							return "<a href='javascript:void(0)' onclick=\"return ProgrammeDisplayCatalog.deleteProductRow('"+groupId+"','"+productId+"','"+gridDetailId+"');\"><img src='/resources/images/icon_delete.png'/></a>";
						}
					}else{
						return "";
					}
        	    }
			}
            ]],
			method : 'GET',
			onResize:function(){
            	$('#'+gridId).datagrid('fixDetailRowHeight',index);  
            },
            onLoadSuccess:function(){
            	$('.datagrid-header-rownumber').html('STT');
            	setTimeout(function(){
            		$('#'+gridId).datagrid('fixDetailRowHeight',index);
            		$('#'+gridId).datagrid('fixRowHeight',index);
            	},500);
            	edit = false;
            },
            onBeforeEdit:function(index,row){  
		        row.editing = true;
		        edit =true;
		        idx = index;  
		        ProgrammeDisplayCatalog.updateActions(gridDetailId,index);  
		    },  
		    onAfterEdit:function(index,row){  
		        row.editing = false;
		        edit = false;
		        idx = -1;
		        ProgrammeDisplayCatalog.updateActions(gridDetailId,index);  
		    },  
		    onCancelEdit:function(index,row){  
		        row.editing = false;  
		        edit = false;
		        idx = -1;
		        ProgrammeDisplayCatalog.updateActions(gridDetailId,index);  
		    }  
		});
	},
	/*
	 * author: lochp
	 * ho tro datagridsku
	 */
	dataGridDetailSKU2:function(gridId,gridDetailId,groupId,index){
		$('#gridDetailId').val(gridDetailId);
		var id = $('#selId').val().trim();
		var title = '';
//		if($('#permissionUser').val() == 'true'){
//			if($('#statusPermission').val().trim() == activeType.WAITING){
//				title = "<a href='javascript:void(0);' onclick=\"return ProgrammeDisplayCatalog.openProductDialog("+groupId+","+ProgrammeDisplayCatalog.SKU+",null,'"+gridDetailId+"');\"><img src='/resources/images/icon_add.png'></a>";
//			}
//		}
		if ($('#permissionUser').val() == 'true' && parseInt($('#statusPermission').val().trim()) != activeType.STOPPED){
			title = "<a href='javascript:void(0);' onclick=\"return ProgrammeDisplayCatalog.openSearchProductDialog(2,"+groupId+", '"+gridDetailId+"');\"><img src='/resources/images/icon_add.png'></a>";
		}
		/* loc bo catalog  */
		var url = '/programme-display/search-product-detail?id='+ encodeChar(id)+'&groupId=' + encodeChar(groupId);
		$('#'+gridDetailId).datagrid({
			url : url,
			total:100,
			width:$('.GridSection').width() -40,
			height:'auto',
			scrollbarSize : 0,
			fitColumns:true,			
			rownumbers:true,
			singleSelect:true,
			columns:[[  
			{field: 'productCode',title: 'Mã sản phẩm', width:150,align:'left',sortable : false,resizable : false,
				formatter:function(value,rows,index){
					if(rows != null && rows != undefined ){
						return Utils.XSSEncode(rows.product.productCode);
					}
				}
			},
			{field: 'productName',title: 'Tên sản phẩm', width:357,align:'left',sortable : false,resizable : false,
				formatter:function(value,rows,index){
					if(rows != null && rows != undefined){
						return Utils.XSSEncode(rows.product.productName);
					}
				}
			},
			{field: 'convfact',title: 'Quy đổi', width:150,align:'right',sortable : false,resizable : false,
				editor:{  
                    type:'numberbox',
				},
				formatter:function(value,rows,index){
					if(rows != null && rows != null){
						if(rows.convfact == null || rows.convfact == undefined){
							return 0;
						}else{
							return Utils.XSSEncode(rows.convfact);
						}
					}
				}
			},
			{field: 'edit',title:'', width:50,align:'center',sortable : false,resizable : false,
				formatter: function(value,rows,index){
					if($('#permissionUser').val() == 'true'){
						if($('#statusPermission').val().trim() == activeType.WAITING){
							if (rows != undefined && rows.editing){  
								var s = "<a href='javascript:void(0)' onclick=\"return ProgrammeDisplayCatalog.saveRowProduct2(this,'"+groupId+"','"+gridDetailId+"','"+rows.product.id+"','"+rows.id+"');\"><span style='cursor:pointer'><img src='/resources/images/icon-datach.png'/></span></a> ";  
								var c = "<a href='javascript:void(0)' onclick=\"ProgrammeDisplayCatalog.cancelRow(this,'"+gridDetailId+"');\"><span style='cursor:pointer'><img src='/resources/images/icon_esc.png'/></span></a>";  
								return s + c;  
							} else {
								return "<a href='javascript:void(0)' onclick=\"ProgrammeDisplayCatalog.editRow(this,'"+gridDetailId+"');\"><span style='cursor:pointer'><img  width='15' height='16'  src='/resources/images/icon-edit.png'/></span></a>";  
							}  
						}
					}else{
						return "";
					}
        	    }
			},
			{field: 'remove',title:title, width:50,align:'center',sortable : false,resizable : false,
				formatter: function(value,rows,index){
					if($('#permissionUser').val() == 'true'){
						if($('#statusPermission').val().trim() == activeType.WAITING){
							var productId = '';
							if(rows != null && rows != undefined){
								productId = rows.product.id;
							}
							return "<a href='javascript:void(0)' onclick=\"return ProgrammeDisplayCatalog.deleteProductRow('"+groupId+"','"+productId+"','"+gridDetailId+"');\"><img src='/resources/images/icon_delete.png'/></a>";
						}
					}else{
						return "";
					}
        	    }
			}
            ]],
			method : 'GET',
			onResize:function(){
            	//$('#'+gridId).datagrid('fixDetailRowHeight',index);  
            },
            onLoadSuccess:function(){
            	$('.datagrid-header-rownumber').html('STT');
            	setTimeout(function(){
	        		$('#'+gridId).datagrid('fixDetailRowHeight',index);
	        		$('#'+gridId).datagrid('fixRowHeight',index);
	        	},800);
            	edit = false;
            },
            onBeforeEdit:function(index,row){  
		        row.editing = true;
		        edit =true;
		        idx = index;  
		        ProgrammeDisplayCatalog.updateActions(gridDetailId,index); 		       
		    },  
		    onAfterEdit:function(index,row){  
		        row.editing = false;
		        edit = false;
		        idx = -1;
		        ProgrammeDisplayCatalog.updateActions(gridDetailId,index);  
		    },  
		    onCancelEdit:function(index,row){  
		        row.editing = false;  
		        edit = false;
		        idx = -1;
		        ProgrammeDisplayCatalog.updateActions(gridDetailId,index);  
		    }  
		});		
	},
	dataGridDetailCB:function(gridId,gridDetailId,groupId,index){
		$('#gridDetailId').val(gridDetailId);
		var id = $('#selId').val().trim();
		var title = '';
		var groupIndex = index;
		if($('#permissionUser').val() == 'true'){
			if($('#statusPermission').val().trim() == activeType.WAITING){				
				title = "<a href='javascript:void(0);' onclick=\"return ProgrammeDisplayCatalog.openProductDialog("+groupId+","+ProgrammeDisplayCatalog.DISPLAY+",null,'"+gridDetailId+"');\"><img width='15' height='16' src='/resources/images/icon_add.png'></a>";
			}
		}
		var url = '/programme-display/search-product-detail-cb?id='+ encodeChar(id)+'&groupId=' + encodeChar(groupId);
		$('#'+gridDetailId).datagrid({
			url : url,
			total:100,
			width:$('.GridSection').width() -80,
			height:'auto',
			scrollbarSize : 0,
			//loctt-begin-sep19
			rownum:10,
			pagination:false,
			pageList:[10,20,30],
			//loctt-end-sep19
			fitColumns:true,
			rownumbers:true,
			singleSelect : true,
			columns:[[  
			{field: 'productCode',title: 'Mã sản phẩm', width:233,align:'left',sortable : false,resizable : false,
				formatter:function(value,rows,index){
					if(rows != null && rows != undefined){
						return Utils.XSSEncode(rows.product.productCode);
					}
				}
			},
			{field: 'productName',title: 'Tên sản phẩm', width:461,align:'left',sortable : false,resizable : false,
				formatter:function(value,rows,index){
					if(rows != null && rows != undefined){
						return Utils.XSSEncode(rows.product.productName);
					}
				}
			},
			{field: 'remove',title:title, width:54,align:'center',sortable : false,resizable : false,
				formatter: function(value,rows,index){
					if($('#permissionUser').val() == 'true'){
						if($('#statusPermission').val().trim() == activeType.WAITING){
							var productId = '';
							if(rows != null && rows != undefined){
								productId = rows.product.id;
							}
							return "<a href='javascript:void(0)' onclick=\"return ProgrammeDisplayCatalog.deleteProductCBRow('"+groupId+"','"+productId+"','"+gridDetailId+"', '"+groupIndex+"');\"><img width='15' height='16' src='/resources/images/icon_delete.png'/></a>";
						}
					}else{
						return "";
					}
	    	    }
			}
	        ]],
			method : 'GET',
			onResize:function(){
	        	$('#'+gridId).datagrid('fixDetailRowHeight',index);  
	        },
	        onLoadSuccess:function(){
	        	$('.datagrid-header-rownumber').html('STT');
	        	setTimeout(function(){
	        		$('#'+gridId).datagrid('fixDetailRowHeight',index);
	        		$('#'+gridId).datagrid('fixRowHeight',index);
	        	},500);
	        }
		});
	},
	dataGridDetailAmount:function(gridId,gridDetailId,groupId,index){
		$('#gridDetailId').val(gridDetailId);
		var id = $('#selId').val().trim();
		var title = '';
		if($('#permissionUser').val() == 'true'){
//			if($('#statusPermission').val().trim() == activeType.WAITING){				
				title = "<a href='javascript:void(0);' onclick=\"return ProgrammeDisplayCatalog.openProductDialog("+groupId+","+ProgrammeDisplayCatalog.DISPLAY+",null,'"+gridDetailId+"');\"><img width='15' height='16' src='/resources/images/icon_add.png'></a>";
//			}
		}
		var url = '/programme-display/search-product-detail?id='+ encodeChar(id)+'&groupId=' + encodeChar(groupId);
		$('#'+gridDetailId).datagrid({
			url : url,
			total:100,
			width:$('.GridSection').width() -40,
			height:'auto',
			scrollbarSize : 0,
			rownum:10,
			pagination:true,
			pageList:[10,20,30],
			fitColumns:true,
			rownumbers:true,
			columns:[[  
			{field: 'productCode',title: 'Mã sản phẩm', width:470,align:'left',sortable : false,resizable : false,
				formatter:function(value,rows,index){
					if(rows != null && rows != undefined){
					return Utils.XSSEncode(rows.product.productCode);
					}
				}
			},
			{field: 'productName',title: 'Tên sản phẩm', width:470,align:'left',sortable : false,resizable : false,
				formatter:function(value,rows,index){
					if(rows != null && rows != undefined){
					return Utils.XSSEncode(rows.product.productName);
					}
				}
			},
			{field: 'remove',title:title, width:150,align:'center',sortable : false,resizable : false,
				formatter: function(value,rows,index){
					if($('#permissionUser').val() == 'true'){
						if($('#statusPermission').val().trim() == activeType.WAITING){
							var productId = '';
							if(rows != null && rows != undefined){
								productId = rows.product.id;
							}
							return "<a href='javascript:void(0)' onclick=\"return ProgrammeDisplayCatalog.deleteProductRow('"+groupId+"','"+productId+"','"+gridDetailId+"');\"><img width='15' height='16' src='/resources/images/icon_delete.png'/></a>";
						}
					}else{
						return "";
					}
	    	    }
			}
	        ]],
			method : 'GET',
			onResize:function(){
	        	$('#'+gridId).datagrid('fixDetailRowHeight',index);  
	        },
	        onLoadSuccess:function(){
	        	$('.datagrid-header-rownumber').html('STT');
	        	setTimeout(function(){
	        		$('#'+gridId).datagrid('fixDetailRowHeight',index);
	        		$('#'+gridId).datagrid('fixRowHeight',index);
	        	},500);
	        }
		});
	},
	dataGridDetailAmount2:function(gridId,gridDetailId,groupId,index){
		$('#gridDetailId').val(gridDetailId);
		var id = $('#selId').val().trim();
		var title = '';
		if ($('#permissionUser').val() == 'true' && parseInt($('#statusPermission').val().trim()) != activeType.STOPPED){
			title = "<a href='javascript:void(0);' onclick=\"return ProgrammeDisplayCatalog.openSearchProductDialog(1,"+groupId+", '"+gridDetailId+"');\"><img src='/resources/images/icon_add.png'></a>";
		}
		var url = '/programme-display/search-product-detail?id='+ encodeChar(id)+'&groupId=' + encodeChar(groupId);
		$('#'+gridDetailId).datagrid({
			url : url,
			total:100,
			width:$('.GridSection').width() -80,
			height:'auto',
			scrollbarSize : 0,
			//loctt-begin-sep19
			rownum:10,
			pagination:false,
//			pageList:[10,20,30],
			//loctt-end-sep19
			fitColumns:true,
			rownumbers:true,
			columns:[[  
			{field: 'productCode',title: 'Mã sản phẩm', width:248,align:'left',sortable : false,resizable : false,
				formatter:function(value,rows,index){
					if(rows != null && rows != undefined){
					return Utils.XSSEncode(rows.product.productCode);
					}
				}
			},
			{field: 'productName',title: 'Tên sản phẩm', width:654,align:'left',sortable : false,resizable : false,
				formatter:function(value,rows,index){
					if(rows != null && rows != undefined){
					return Utils.XSSEncode(rows.product.productName);
					}
				}
			},
			{field: 'remove',title:title, width:70,align:'center',sortable : false,resizable : false,
				formatter: function(value,rows,index){
					if($('#permissionUser').val() == 'true'){
						if($('#statusPermission').val().trim() == activeType.WAITING){
							var productId = '';
							if(rows != null && rows != undefined){
								productId = rows.product.id;
							}
							return "<a href='javascript:void(0)' onclick=\"return ProgrammeDisplayCatalog.deleteProductRow('"+groupId+"','"+productId+"','"+gridDetailId+"');\"><img width='15' height='16' src='/resources/images/icon_delete.png'/></a>";
						}
					}else{
						return "";
					}
	    	    }
			}
	        ]],
			method : 'GET',
			onResize:function(){
	        	$('#'+gridId).datagrid('fixDetailRowHeight',index);  
	        },
	        onLoadSuccess:function(){
	        	$('.datagrid-header-rownumber').html('STT');
	        	setTimeout(function(){
	        		$('#'+gridId).datagrid('fixDetailRowHeight',index);
	        		$('#'+gridId).datagrid('fixRowHeight',index);
	        	},500);
	        }
		});
	},
	dataGridDetailAmount3:function(gridId,gridDetailId,groupId,index){
		$('#gridDetailId').val(gridDetailId);
		var id = $('#selId').val().trim();
		var title = '';
		if ($('#permissionUser').val() == 'true' && parseInt($('#statusPermission').val().trim()) != activeType.STOPPED){
			title = "<a href='javascript:void(0);' onclick=\"return ProgrammeDisplayCatalog.openSearchProductDialog(3,"+groupId+", '"+gridDetailId+"');\"><img src='/resources/images/icon_add.png'></a>";
		}
		var url = '/programme-display/search-product-detail?id='+ encodeChar(id)+'&groupId=' + encodeChar(groupId);
		$('#'+gridDetailId).datagrid({
			url : url,
			total:100,
			width:$('.GridSection').width() -80,
			height:'auto',
			scrollbarSize : 0,
			//loctt-begin-sep19
			rownum:10,
			//loctt-end-sep19
			fitColumns:true,
			rownumbers:true,
			columns:[[  
			{field: 'productCode',title: 'Mã sản phẩm', width:248,align:'left',sortable : false,resizable : false,
				formatter:function(value,rows,index){
					if(rows != null && rows != undefined){
					return Utils.XSSEncode(rows.product.productCode);
					}
				}
			},
			{field: 'productName',title: 'Tên sản phẩm', width:500,align:'left',sortable : false,resizable : false,
				formatter:function(value,rows,index){
					if(rows != null && rows != undefined){
					return Utils.XSSEncode(rows.product.productName);
					}
				}
			},
			{field: 'remove',title:title, width:150,align:'center',sortable : false,resizable : false,
				formatter: function(value,rows,index){
					if($('#permissionUser').val() == 'true'){
						if($('#statusPermission').val().trim() == activeType.WAITING){
							var productId = '';
							if(rows != null && rows != undefined){
								productId = rows.product.id;
							}
							return "<a href='javascript:void(0)' onclick=\"return ProgrammeDisplayCatalog.deleteProductRow('"+groupId+"','"+productId+"','"+gridDetailId+"');\"><img width='15' height='16' src='/resources/images/icon_delete.png'/></a>";
						}
					}else{
						return "";
					}
	    	    }
			}
	        ]],
			method : 'GET',
			onResize:function(){
	        	$('#'+gridId).datagrid('fixDetailRowHeight',index);  
	        },
	        onLoadSuccess:function(){
	        	$('.datagrid-header-rownumber').html('STT');
	        	setTimeout(function(){
	        		$('#'+gridId).datagrid('fixDetailRowHeight',index);
	        		$('#'+gridId).datagrid('fixRowHeight',index);
	        	},500);
	        }
		});
	},
	updateActions:function(gridDetailId,index){  
	    $('#'+gridDetailId).datagrid('updateRow',{  
	        index: index,  
	        row:{}  
	    });  
	}, 
	editRow:function(target,gridDetailId){
		var indexRow = ProgrammeDisplayCatalog.getRowIndex(target);
	    $('#'+gridDetailId).datagrid('beginEdit', indexRow);	    
	    var ed = $('#'+gridDetailId).datagrid('getEditor', {index:indexRow,field:'convfact'});
	    $(ed.target).attr('maxlength',7);
	},
	getRowIndex:function(target){  
	    var tr = $(target).closest('tr.datagrid-row');  
	    return parseInt(tr.attr('datagrid-row-index'));  
	},
	saveRowProduct:function(target,groupId,gridDetailId){
		var indexRow = ProgrammeDisplayCatalog.getRowIndex(target);
	    $('#'+gridDetailId).datagrid('endEdit', indexRow);
	    var dataModel = new Object();		
		dataModel.id = $('#selId').val().trim();
		dataModel.groupId = groupId;
		dataModel.productId = $('#'+gridDetailId).datagrid('getData').rows[indexRow].id;
		dataModel.isUpdate = true;
		dataModel.convfact = $('#'+gridDetailId).datagrid('getData').rows[indexRow].convfact;
		Utils.addOrSaveRowOnGrid(dataModel, "/programme-display/change-product", ProgrammeDisplayCatalog._xhrSave, null,null, function(data){
			$('#'+gridDetailId).datagrid('reload');
		},null);
		return false;
	},
	/*
	 * author: lochp
	 * ho tro cho datagriddetailsKU
	 */
	saveRowProduct2:function(target,groupId,gridDetailId, productId, groupDtlId){
		var indexRow = ProgrammeDisplayCatalog.getRowIndex(target);
	    $('#'+gridDetailId).datagrid('endEdit', indexRow);
	    var dataModel = new Object();		
		dataModel.id = $('#selId').val().trim();
		dataModel.groupId = groupId;
		dataModel.productId = productId;
		dataModel.isUpdate = true;
		if (groupDtlId != undefined || groupDtlId != null){
			dataModel.groupDtlId = groupDtlId;
		}
		dataModel.convfact = $('#'+gridDetailId).datagrid('getData').rows[indexRow].convfact;
		Utils.addOrSaveRowOnGrid(dataModel, "/programme-display/change-amount-product", ProgrammeDisplayCatalog._xhrSave, null,null, function(data){
			$('#'+gridDetailId).datagrid('reload');
		},null);
		return false;
	},
	cancelRow:function (target,gridDetailId){  
		var indexRow = ProgrammeDisplayCatalog.getRowIndex(target);
	    $('#'+gridDetailId).datagrid('cancelEdit', indexRow);  
	},
	openGroupDialog : function(type,groupId,groupCode,groupName) {
		$('.ErrorMsgStyle').hide();
		$('#divGroupContainer').show();
		var html = $('#dialogGroup').html();
		$('#dialogGroup').dialog({  
	        closed: false,  
	        cache: false,  
	        modal: true,
	        position: 'middle',
	        center: true, 
	        onOpen: function(){
	        	$('#dialogGroup').bind('keypress',function(event){
		   			if(event.keyCode == keyCodes.ENTER){		   				
	   					if(!$('#dialogGroup #btnChangeGroup').is(':hidden')){
	   						$('#dialogGroup #btnChangeGroup').click();
	   					}
		   			}
		   		});
	        	$('#errMsgGroupDlg').html('').hide();
	        	if(type == ProgrammeDisplayCatalog.DISPLAY){
	        		$('#typeGroup').val(ProgrammeDisplayCatalog.DISPLAY);
	        	}else{
	        		$('#typeGroup').val(ProgrammeDisplayCatalog.SKU);
	        	}
	        	$('#errMsgGroupDlg').html('').hide();	        	
	        	if(groupId != null && groupId != undefined){
	        		$('#groupId').val(groupId);
	        		$('#groupCodeDlg').attr('disabled','disabled');
	        		$('#groupNameDlg').focus();
	        	}else{
	        		$('#groupId').val(0);
	        		$('#groupCodeDlg').removeAttr('disabled');
	        		$('#groupCodeDlg').focus();
	        	}
	        	if(groupCode == null || groupCode == undefined){
	        		groupCode = '';
	        	}
	        	if(groupName == null || groupName == undefined){
	        		groupName = '';
	        	}
	        	$('#groupCodeDlg').val(groupCode);
				$('#groupNameDlg').val(groupName);
	        },
	        onBeforeClose: function() {
	        },
	        onClose : function(){
	        	$('#dialogGroup').unbind('keypress')
	        	$('#divGroupContainer').hide();
				var selId = $('#selId').val();
	    		var url = '';		
	    		url = '/programme-display/search-product?id=' + encodeChar(selId) + '&typeGroup=4';
	    		ProgrammeDisplayCatalog.dataGridProductGroup('cbGrid',url,ProgrammeDisplayCatalog.DISPLAY);	        }
	    });
	},
	openGroupDialog3 : function(type,groupId,groupCode,groupName) {
		$('#divGroupContainer').show();
		var html = $('#dialogGroup').html();
		$('#dialogGroup').dialog({  
	        closed: false,  
	        cache: false,  
	        modal: true,
	        position: 'middle',
	        center: true, 
	        onOpen: function(){
	        	$('#dialogGroup').bind('keypress',function(event){
		   			if(event.keyCode == keyCodes.ENTER){		   				
	   					if(!$('#dialogGroup #btnChangeGroup').is(':hidden')){
	   						$('#dialogGroup #btnChangeGroup').click();
	   					}
		   			}
		   		});
	        	$('#errMsgGroupDlg').html('').hide();
	        	if (type == 'DS')
	        		type = 1;
	        	if (type == 'SL')
	        		type = 2;
	        	if (type == 'SKU')
	        		type = 3;
	        	
	        	$('#typeGroup').val(type);
	        	
	        	$('#errMsgGroupDlg').html('').hide();	        	
	        	if(groupId != null && groupId != undefined){
	        		$('#groupId').val(groupId);
	        		$('#groupCodeDlg').attr('disabled','disabled');
	        		$('#groupNameDlg').focus();
	        	}else{
	        		$('#groupId').val(0);
	        		$('#groupCodeDlg').removeAttr('disabled');
	        		$('#groupCodeDlg').focus();
	        	}
	        	if(groupCode == null || groupCode == undefined){
	        		groupCode = '';
	        	}
	        	if(groupName == null || groupName == undefined){
	        		groupName = '';
	        	}
	        	$('#groupCodeDlg').val(groupCode);
				$('#groupNameDlg').val(groupName);
	        },
	        onBeforeClose: function() {
	        },
	        onClose : function(){
	        	$('#dialogGroup').unbind('keypress')
	        	$('#divGroupContainer').hide();
	        }
	    });
	}
	,
	openGroupDialog2 : function(type,groupId,groupCode,groupName) {
		$('#divGroupContainer2').show();
		var html = $('#dialogGroup2').html();
		$('#dialogGroup2').dialog({  
	        closed: false,  
	        cache: false,  
	        modal: true,
	        position: 'middle',
	        center: true, 
	        onOpen: function(){
	        	$('#dialogGroup2').bind('keypress',function(event){
		   			if(event.keyCode == keyCodes.ENTER){		   				
	   					if(!$('#dialogGroup2 #btnChangeGroup').is(':hidden')){
	   						$('#dialogGroup2 #btnChangeGroup').click();
	   					}
		   			}
		   		});
	        	$('#errMsgGroupDlg2').html('').hide();
	        	$('#errMsgGroupDlg2').html('').hide();	        	
	        	if(groupId != null && groupId != undefined){
	        		$('#groupId2').val(groupId);
	        		$('#groupCodeDlg2').attr('disabled','disabled');
	        		$('#groupNameDlg2').focus();
	        	}else{
	        		$('#groupId2').val(0);
	        		$('#groupCodeDlg2').removeAttr('disabled');
	        		$('#groupCodeDlg2').focus();
	        	}
	        	if(groupCode == null || groupCode == undefined){
	        		groupCode = '';
	        	}
	        	if(groupName == null || groupName == undefined){
	        		groupName = '';
	        	}
	        	$('#groupCodeDlg2').val(groupCode);
				$('#groupNameDlg2').val(groupName);
	        },
	        onBeforeClose: function() {
	        },
	        onClose : function(){
	        	$('#dialogGroup2').unbind('keypress');
	        	$('#divGroupContainer2').hide();
	        }
	    });
	},
	openProductDialog : function(groupId,typeGroup,productId,gridDetailId) {
		$('#gridDetailId').val(gridDetailId);
		if(productId != null && productId != undefined){
			$('#productId').val(productId);
			$('#isUpdate').val(1);
		}else{
			$('#productId').val(0);
			$('#isUpdate').val(0);
		}
		$('#groupId').val(groupId);
		$('#typeGroup').val(typeGroup);
		$('#divProductContainer').show();
		var html = $('#dialogProduct').html();
		$('#dialogProduct').dialog({  
	        closed: false,  
	        cache: false,  
	        modal: true,
	        position: 'middle',
	        center: true, 
	        onOpen: function(){
	        	$('#divProductContainer').html('');
	        	ProgrammeDisplayCatalog._lstProductExpand = [];
	        	ProgrammeDisplayCatalog._mapProduct = new Map();
	        	$('.easyui-dialog #errMsgProductDlg').html('').hide();
	        	$('#productCodeDlg').val('');
	        	$('#productNameDlg').val('');
	        	$('#btnSeachProductTree').bind('click',function(){
	        		ProgrammeDisplayCatalog._lstProductExpand = [];
            		var param = '&code='+encodeChar($('.easyui-dialog #productCodeDlg').val().trim())+'&name='+encodeChar($('.easyui-dialog #productNameDlg').val().trim())+'&type=&parentId=&typeObject=6&isPromotionProduct=false';
            		if(typeGroup == ProgrammeDisplayCatalog.DISPLAY){
    	        		ProgrammeDisplayCatalog.loadProductTreeGridForAmount(param,typeGroup,gridDetailId);
    	        	}else{
    	        		ProgrammeDisplayCatalog.loadProductTreeGridForSKU(param,typeGroup);
    	        	}
        		});
	        	$('#productCodeDlg,#productNameDlg').bind('keyup',function(event){
	        		if(event.keyCode == keyCodes.ENTER){
	        			ProgrammeDisplayCatalog._lstProductExpand = [];
	        			var param = '&code='+encodeChar($('.easyui-dialog #productCodeDlg').val().trim())+'&name='+encodeChar($('.easyui-dialog #productNameDlg').val().trim())+'&type=&parentId=&typeObject=6&isPromotionProduct=false';
	        			if(typeGroup == ProgrammeDisplayCatalog.DISPLAY){
	    	        		ProgrammeDisplayCatalog.loadProductTreeGridForAmount(param,typeGroup,gridDetailId);
	    	        	}else{
	    	        		ProgrammeDisplayCatalog.loadProductTreeGridForSKU(param,typeGroup);
	    	        	}
	        		}
	        	});
	        	$('#productCodeDlg').focus();
	        	if(typeGroup == ProgrammeDisplayCatalog.DISPLAY){
	        		ProgrammeDisplayCatalog.loadProductTreeGridForAmount(null,typeGroup,gridDetailId);
	        	}else{
	        		ProgrammeDisplayCatalog.loadProductTreeGridForSKU(null,typeGroup);
	        	}
	        },
	        onBeforeClose: function() {
	        },
	        onClose : function(){        	
	        	$('#divProductContainer').html(html);
	        	$('#divProductContainer').css("display", "none");	        	
	        	$('#productNameDlg').val('');
	        	$('#productCodeDlg').val('');
	        }
	    });
	},
	loadProductTreeGridForSKU:function(param,typeGroup){
		$('#loadingDialog').show();
		if(param == null || param == undefined || param == ''){
			param = '&code=&name=&type=&parentId=&typeObject=6&isPromotionProduct=false';
		}
		$('.easyui-dialog #treeGrid').treegrid({  
			url:  '/rest/catalog/product/tree-grid/'+typeGroup+'.json?id='+$('#selId').val().trim() + param,
			width:($('.easyui-dialog #treeGridContainer').width()-20),  
			height:400,  
			idField: 'id',  
			treeField: 'text',
			method:'GET',
			checkOnSelect:false,
			singleSelect:false,
			rownumbers:true,
			columns:[[  
	          {field:'text',title:'Sản phẩm',resizable:false,width:400},
	          {field: 'convfact',title: 'Quy đổi', width:180,align:'center',sortable : false,resizable : false,fixed:true,
	        	  formatter:function(value,row){
	        		  if(row.attributes.productTreeVO.type == 'PRODUCT'){
	        			  return '<input style="text-align:right;"  value="'+Utils.XSSEncode(row.attributes.productTreeVO.convfact)+'" class="numberItem" id="quantity_'+row.id+'" onfocus="return Utils.bindFormatOnTextfield(\'quantity_'+row.id+'\',Utils._TF_NUMBER,null);" onkeypress="NextAndPrevTextField(event,this,\'numberItem\')"/>';
	        		  }
	        	  }
	          },
	          {field: 'ck',checkbox:true,
	          }
	          ]],
            onLoadSuccess:function(row,data){
            	$('.easyui-dialog .datagrid-header-rownumber').html('STT');
            	$('#loadingDialog').hide();
            	setTimeout(function(){
            		CommonSearchEasyUI.fitEasyDialog();
        	  	},1000);
            	$('.numberItem').each(function(){
            		EasyUiCommon.hideCheckBoxOnTree(this);
            	});
            },
            onExpand:function(row){
            	var param = '&code=&name=&typeObject=6&isPromotionProduct=false&parentId='+row.id+'&type='+row.attributes.productTreeVO.type;
            	$.getJSON('/rest/catalog/product/tree-grid/'+typeGroup+'.json?id='+$('#selId').val().trim() + param,function(data){
            		var isExist = false;
            		for(var i = 0; i < data.rows.length ; i++){
            			if(ProgrammeDisplayCatalog._lstProductExpand.indexOf(data.rows[i].id) > -1){
            				isExist = true;
            				break;
            			}else{
            				ProgrammeDisplayCatalog._lstProductExpand.push(data.rows[i].id);
            			}
            		}
            		if(!isExist){
            			$('#treeGrid').treegrid('append',{
            				parent: row.id,  
            				data: data.rows
            			});
            		}
        		});
            },
            onCollapse:function(row){
// for(var i = 0; i < ProgrammeDisplayCatalog._lstProductExpand.length ; i++){
// $('#treeGrid').treegrid('remove',ProgrammeDisplayCatalog._lstProductExpand[i]);
// }
            },
            onCheck:function(row){
// var arr = row.attributes.productTreeVO.listChildren;
// EasyUiCommon.recursionCheck(arr,true);
            	 if(row.attributes.productTreeVO.type == 'PRODUCT'){
            		 var id = row.attributes.productTreeVO.id;
            		 var text = row.attributes.productTreeVO.name;
            		 var code = text.split('-')[0];
            		 var name = text.split('-')[1];
            		 var array = [];
            		 array.push(id);
            		 array.push(code);
        			 array.push($('#quantity_'+ id).val());
            		 array.push(name);
            		 ProgrammeDisplayCatalog._mapProduct.put(id,array);
            	 }
		    },
		    onUncheck:function(row){
// var arr = row.attributes.productTreeVO.listChildren;
// EasyUiCommon.recursionUnCheck(arr);
		    	var id = row.attributes.productTreeVO.id;
		    	ProgrammeDisplayCatalog._mapProduct.remove(id);
		    },
		    onCheckAll:function(rows){
		    	for(var i = 0;i<rows.length;i++){
		    		var array = [];
		    		var row = rows[i];
		    		var id = row.attributes.productTreeVO.id;
		    		var text = row.attributes.productTreeVO.name;
	           		var code = text.split('-')[0];
	           		var name = text.split('-')[1];
			    	array.push(id);
		    		array.push(code);
        			array.push($('#quantity_'+ id).val());
			    	array.push(name);
            		ProgrammeDisplayCatalog._mapProduct.put(id,array);
		    	}
		    },
		    onUncheckAll:function(rows){
		    	for(var i = 0;i<rows.length;i++){
		    		var row = rows[i];
		    		var id = row.attributes.productTreeVO.id;
		    		ProgrammeDisplayCatalog._mapProduct.remove(id);
		    	}
		    },
		});
	},
	loadProductTreeGridForAmount:function(param,typeGroup,gridDetailId){
		$('#loadingDialog').show();
		if(param == null || param == undefined || param == ''){
			param = '&code=&name=&isPromotionProduct=false';
		}
		$('.easyui-dialog #treeGrid').datagrid({  
			url:  '/programme-display/load-sanpham?id='+$('#selId').val().trim() + param,
			width:($('.easyui-dialog #treeGridContainer').width()-35),  
			height:'auto',  
			rownum:5,
			pagination:true,
			pageList:[5,10,15],
			method:'GET',
			checkOnSelect:true,
			singleSelect:false,
			rownumbers:true,
			columns:[[  
	          {field:'catCode',title:'Ngành',resizable:false,width:80, formatter : function(value,rowData,rowIndex) {	        	  
	        	  return Utils.XSSEncode(value);
  	        }},
  	        {field:'productCode',title:'Mã sản phẩm',resizable:false,width:250, formatter : function(value,rowData,rowIndex) {
	        	return Utils.XSSEncode(value);
	        }},
	        {field:'productName',title:'Tên sản phẩm',resizable:false,width:250, formatter : function(value,rowData,rowIndex) {
	        	return Utils.XSSEncode(value);
	        }},
	        {field: 'ck',checkbox:true}
	          ]],
            onLoadSuccess:function(data){
            	$('.easyui-dialog .datagrid-header-rownumber').html('STT');
            	$('#loadingDialog').hide();
	    			 $('.datagrid-header-check input').removeAttr('checked');
            	var rows = $('.easyui-dialog #treeGrid').datagrid('getRows');
		    	for(var i = 0; i < rows.length; i++) {
		    		if(ProgrammeDisplayCatalog._mapProduct.get(rows[i].productId)) {
		    			$('.easyui-dialog #treeGrid').datagrid('checkRow', i);
		    		}
		    	}
            },
            onCheck:function(index, row){
            		 var array = [];
            		 array.push(row.productId);
            		 array.push(row.productCode);
            		 array.push(row.productName);
            		 ProgrammeDisplayCatalog._mapProduct.put(row.productId,array);
            		 ProgrammeDisplayCatalog._gridDetaiSpCblId = gridDetailId;
		    },
		    onUncheck:function(index, row){
		    	var arr = row.children;
        		EasyUiCommon.recursionUnCheck(arr);
		    	var id = row.id;
		    	ProgrammeDisplayCatalog._mapProduct.remove(id);
		    },
		    onCheckAll:function(rows){
		    	for(var i = 0;i<rows.length;i++){
		    		var array = [];
		    		var row = rows[i];
		    		var id = row.productId;
			    	array.push(id);
		    		/*array.push(code);
			    	array.push(name);*/
            		ProgrammeDisplayCatalog._mapProduct.put(id,array);
            		ProgrammeDisplayCatalog._gridDetaiSpCblId = gridDetailId;
		    	}
		    },
		    onUncheckAll:function(rows){
		    	
		    	for(var i = 0;i<rows.length;i++){
		    		var row = rows[i];
		    		var id = row.productId;
		    		ProgrammeDisplayCatalog._mapProduct.remove(id);
		    	}
		    }		   
		});
	},
	deleteGroupRow:function(groupId,type){
		var dataModel = new Object();
		dataModel.groupId = groupId;
		var selId = $('#selId').val();
		var text = '';
		var gridId = '';
		var tmpUrl = '';
		var gridType= -1;
		if(type == ProgrammeDisplayCatalog.AMOUNT){
			tmpUrl = '/programme-display/search-amount-product?id=' + encodeChar(selId) + '&typeGroup=1';
			text = 'nhóm doanh số';
			gridId = 'amountGrid';
			gridType = 1;
}		else if(type == ProgrammeDisplayCatalog.DISPLAY){
			tmpUrl = '/programme-display/search-product?id=' + encodeChar(selId) + '&typeGroup=4';
			text = 'nhóm trưng bày';
			gridId = 'cbGrid';
			gridType = 4;
		}else{
			text = 'nhóm SP cho SKU';
			gridId = 'skuGrid';
		}
		Utils.deleteSelectRowOnGrid(dataModel, text , '/programme-display/delete-group', ProgrammeDisplayCatalog._xhrDel, gridId,'errMsgGroup',function(data){
			showSuccessMsg('successMsgGroup',data);
			var selId = $('#selId').val();
			var url = '';		
			url = tmpUrl;
			if (gridType != 1){
				ProgrammeDisplayCatalog.dataGridProductGroup(gridId,url,gridType);
			}
			if (gridType == 1){
				ProgrammeDisplayCatalog.dataGridProductGroup2(gridId,url,gridType);
			}
		},null);		
		return false;
	},
	deleteProductCBRow: function(groupId,productId,gridDetailId, groupIndex){
		var dataModel = new Object();
		dataModel.groupId = groupId;
		dataModel.productId = productId;	
		Utils.deleteSelectRowOnGrid(dataModel, 'sản phẩm trong nhóm', '/programme-display/delete-product', ProgrammeDisplayCatalog._xhrDel, gridDetailId,'errMsgProduct',function(data){
			showSuccessMsg('successMsgProduct',data);
			var selId = $('#selId').val();
			$('#'+gridDetailId).datagrid('reload');
			//var url = '';		
			//url = '/programme-display/search-product?id=' + encodeChar(selId) + '&typeGroup=4';
			//ProgrammeDisplayCatalog.dataGridProductGroup('cbGrid',url,ProgrammeDisplayCatalog.DISPLAY);
			ProgrammeDisplayCatalog.dataGridDetailCB('cbGrid',gridDetailId,groupId,groupIndex);
		},'productLoading');		
		return false;
	},
	deleteProductRow: function(groupId,productId,gridDetailId, index){
		var dataModel = new Object();
		dataModel.groupId = groupId;
		dataModel.productId = productId;	
		Utils.deleteSelectRowOnGrid(dataModel, 'sản phẩm trong nhóm', '/programme-display/delete-product', ProgrammeDisplayCatalog._xhrDel, gridDetailId,'errMsgProduct',function(data){
			showSuccessMsg('successMsgProduct',data);
			var selId = $('#selId').val();
			$('#'+gridDetailId).datagrid('reload');
			//var url = '';		
			//url = '/programme-display/search-product?id=' + encodeChar(selId) + '&typeGroup=4';
			//ProgrammeDisplayCatalog.dataGridProductGroup('cbGrid',url,ProgrammeDisplayCatalog.DISPLAY);
			//ProgrammeDisplayCatalog.dataGridDetailCB('cbGrid',gridDetailId,groupId,index);
		},'productLoading');		
		return false;
		
	},
	changeGroup:function(){
		var msg = '';
		$('.ErrorMsgStyle').hide();
		msg = Utils.getMessageOfRequireCheck('groupCodeDlg','Mã nhóm');
		if(msg.length == 0){
			msg = Utils.getMessageOfSpecialCharactersValidate('groupCodeDlg','Mã nhóm',Utils._CODE);
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfRequireCheck('groupNameDlg','Tên nhóm');
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfSpecialCharactersValidate('groupNameDlg','Tên nhóm');
		}
		if(msg.length > 0){
			$('#errMsgGroupDlg').html(msg).show();
			return false;
		}
		var dataModel = new Object();		
		dataModel.id = $('#selId').val().trim();
		dataModel.groupId = $('#groupId').val().trim();
		dataModel.groupCode = $('#groupCodeDlg').val().trim();
		dataModel.groupName = $('#groupNameDlg').val().trim();	
		dataModel.typeGroup = $('#typeGroup').val();	
		var gridId = '';
		var tmpUrl = '';
		var gridType = -1;
		var selId = $('#selId').val();
		if($('#typeGroup').val() == 4 ){
			tmpUrl = '/programme-display/search-product?id=' + encodeChar(selId) + '&typeGroup=4';
			gridId = 'cbGrid';
			gridType = 4;
		}else{
			tmpUrl = '/programme-display/search-amount-product?id=' + encodeChar(selId) + '&typeGroup=1';
			gridId = 'amountGrid';
			gridType = 1;
		}
		Utils.addOrSaveRowOnGrid(dataModel, "/programme-display/change-group", ProgrammeDisplayCatalog._xhrSave, gridId,'errMsgGroupDlg', function(data){
			$('#groupId').val(data.groupId);
			showSuccessMsg('successMsgGroupDlg',data);
			$('.easyui-dialog').dialog('close');
			
			var url = '';		
			url = tmpUrl;
			if (gridType == 1){
//				ProgrammeDisplayCatalog.dataGridProductGroup2(gridId,url,gridType);
				$('#amountGrid').datagrid('reload');
			}else{
//				ProgrammeDisplayCatalog.dataGridProductGroup(gridId,url,gridType);
				$('#cbGrid').datagrid('reload');
			}
		},null);
		
		return false;
	},
	changeGroup2:function(){
		var msg = '';
		$('.ErrorMsgStyle').hide();
		msg = Utils.getMessageOfRequireCheck('groupCodeDlg2','Mã nhóm');
		if(msg.length == 0){
			msg = Utils.getMessageOfSpecialCharactersValidate('groupCodeDlg2','Mã nhóm',Utils._CODE);
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfRequireCheck('groupNameDlg2','Tên nhóm');
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfSpecialCharactersValidate('groupNameDlg2','Tên nhóm');
		}
		if (msg.length == 0 && $('#typeGroup2').val() == -1){
			msg = "Bạn chưa chọn Loại nhóm.";
		}
		
		if(msg.length > 0){
			$('#errMsgGroupDlg2').html(msg).show();
			return false;
		}
		var dataModel = new Object();		
		dataModel.id = $('#selId').val().trim();
		dataModel.groupId = $('#groupId2').val().trim();
		dataModel.groupCode = $('#groupCodeDlg2').val().trim();
		dataModel.groupName = $('#groupNameDlg2').val().trim();	
		dataModel.typeGroup = $('#typeGroup2').val();	
		var gridId = '';
		if($('#typeGroup').val() == ProgrammeDisplayCatalog.DISPLAY ){
			gridId = 'amountGrid';
		}else{
			gridId = 'skuGrid';
		}
		Utils.addOrSaveRowOnGrid(dataModel, "/programme-display/change-amount-group", ProgrammeDisplayCatalog._xhrSave, gridId,'errMsgGroupDlg2', function(data){
			$('#groupId2').val(data.groupId);
			showSuccessMsg('successMsgGroupDlg',data);
			$("#amountGrid").datagrid('reload');
			$('.easyui-dialog').dialog('close');
		},null);

		
		
		
		
		return false;
	},
	changeProduct:function(){
		var msg = '';
		$('.ErrorMsgStyle').hide();
		var lstProductId = [];
		var lstConvfact = [];
		console.log(ProgrammeDisplayCatalog._mapProduct);
		var key = ProgrammeDisplayCatalog._mapProduct.keyArray;
		if(key != null && key.length > 0){
			var isErr = false;
			if(parseInt($('#isUpdate').val()) == 1){
				
			}else{
				for(var i = 0;i<key.length;i++){
					if(!isErr){
						lstProductId.push(key[i]);
						if($('#typeGroup').val() == ProgrammeDisplayCatalog.SKU ){
							if($('#quantity_'+ key[i]).val() != '' && $('#quantity_'+ key[i]).val() > 0){
								lstConvfact.push($('#quantity_'+ key[i]).val().trim());
							}else{
								msg = 'Vui lòng nhập số lượng quy đổi';
								$('#quantity_'+ key[i]).focus();
								isErr = true;
							}
						}
					}
				}
			}
		}else{
			msg = 'Vui lòng chọn Sản phẩm';
		}
		if(msg.length > 0){
			$('.easyui-dialog #errMsgProductDlg').html(msg).show();
			return false;
		}
		var dataModel = new Object();		
		dataModel.id = $('#selId').val().trim();
		dataModel.groupId = $('#groupId').val().trim();
		dataModel.lstProductId = lstProductId;
		if(parseInt($('#isUpdate').val()) == 1){
			
		}else{
			dataModel.lstConvfact = lstConvfact;	
		}
		var gridId = $('#gridDetailId').val();
		if($('#typeGroup').val() == ProgrammeDisplayCatalog.DISPLAY ){
			gridId = 'amountGrid';
			}else{
				gridId = 'skuGrid';
			}
		gridId = ProgrammeDisplayCatalog._gridDetaiSpCblId;
		Utils.addOrSaveRowOnGrid(dataModel, "/programme-display/change-product", ProgrammeDisplayCatalog._xhrSave, gridId,'errMsgProductDlg', function(data){
			//showSuccessMsg('successMsgProductDlg',data);
			$('.easyui-dialog').dialog('close');			
			$('.easyui-dialog').dialog('close');
			var selId = $('#selId').val();
			var url = '';		
			url = '/programme-display/search-product?id=' + encodeChar(selId) + '&typeGroup=4';
			//$('#'+ProgrammeDisplayCatalog._gridDetaiSpCblId).datagrid('reload');
		},null);
		return false;
	},
	
	changeLevel:function(){
		var msg = '';
		$('.easyui-dialog #errMsgLevelDlg').html('').hide();
		msg = Utils.getMessageOfRequireCheck('levelCodeDlg','Mã mức');
		if(msg.length == 0){
			msg = Utils.getMessageOfSpecialCharactersValidate('levelCodeDlg','Mã mức',Utils._CODE);
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfSpecialCharactersValidateEx('amountDlg','Doanh số',Utils._TF_NUMBER_COMMA);
		}
		if(msg.length == 0){
			var amountDlg = $('#amountDlg').val().trim();
			amountDlg = Utils.returnMoneyValue(amountDlg);
			if($('#amountDlg').val() != '' && Number(amountDlg)<= 0){
				msg = 'Doanh số phải lớn hơn 0';
			}
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfSpecialCharactersValidateEx('skuDlg','Số SKU',Utils._TF_NUMBER_COMMA);
		}
		if(msg.length == 0){
			var skuDlg = $('#skuDlg').val().trim();
			skuDlg = Utils.returnMoneyValue(skuDlg);
			if($('#skuDlg').val() != '' && Number(skuDlg) <= 0){
				msg = 'Số SKU phải lớn hơn 0';
			}
		}
		if(msg.length == 0){
			var quantityLv = $('#quantityLv').val().trim();
			quantityLv = Utils.returnMoneyValue(quantityLv);
			if($('#quantityLv').val() != '' && Number(quantityLv) <= 0){
				msg = 'Số SKU phải lớn hơn 0';
			}
		}
		if(msg.length > 0){
			$('.easyui-dialog #errMsgLevelDlg').html(msg).show();
			return false;
		}
		var dataModel = new Object();		
		dataModel.id = $('#selId').val().trim();
		dataModel.levelId = $('#levelId').val().trim();
		dataModel.levelCode = $('#levelCodeDlg').val().trim();
		dataModel.amount = Utils.returnMoneyValue($('#amountDlg').val().trim());
		dataModel.sku = Utils.returnMoneyValue($('#skuDlg').val().trim());
		dataModel.quantityLv =  Utils.returnMoneyValue(quantityLv);
		var lstMinValue = [];
		var lstMaxValue = [];
		var lstPercentValue = [];
		var lstQuantityValue = [];
		var lstGroupId = [];		
		
		$('.easyui-dialog .minItem').each(function(){
			msg = Utils.getMessageOfSpecialCharactersValidateEx($(this).attr('id'),'Min',Utils._TF_NUMBER_COMMA);
			if(msg.length == 0){
				lstGroupId.push($(this).attr('id').split("_")[2]);
				var value = 0;
				if($(this).val() != null && $(this).val() != '' && Utils.returnMoneyValue($(this).val()) > 0){
					value = Utils.returnMoneyValue($(this).val());
				}
				lstMinValue.push(value);
			}else{
				return false;
			}
		});
		if(msg.length > 0){
			$('.easyui-dialog #errMsgLevelDlg').html(msg).show();
			return false;
		}
		var indexMaxItem = 0;
		$('.easyui-dialog .maxItem').each(function(){
			msg = Utils.getMessageOfSpecialCharactersValidateEx($(this).attr('id'),'Max',Utils._TF_NUMBER_COMMA);
			if(msg.length == 0){
				var value = 0;
				if($(this).val() != null && $(this).val() != '' && Utils.returnMoneyValue($(this).val()) > 0){
					value = Utils.returnMoneyValue($(this).val().trim());
				}
				lstMaxValue.push(value);
				var flag = Number(lstMinValue[indexMaxItem])>0 && Number(lstMaxValue[indexMaxItem])>0;
				if(flag && Number(lstMinValue[indexMaxItem]) > Number(lstMaxValue[indexMaxItem])){
					msg = 'Giá trị Min phải nhỏ hơn giá trị Max.';
					$('.easyui-dialog .minItem').eq(indexMaxItem).focus();
				}
				++indexMaxItem;
			}else{
				return false;
			}
		});
		if(msg.length > 0){
			$('.easyui-dialog #errMsgLevelDlg').html(msg).show();
			return false;
		}
		$('.easyui-dialog .percentItem').each(function(){
			msg = Utils.getMessageOfSpecialCharactersValidateEx($(this).attr('id'),'% đóng góp',Utils._TF_NUMBER_COMMA);
			if(msg.length == 0){
				var value = 0;
				if($(this).val() != null && $(this).val() != '' && Utils.returnMoneyValue($(this).val()) > 0){
					value = Utils.returnMoneyValue($(this).val());
				}
				lstPercentValue.push(value);
			}else{
				return false;
			}
		});
		if(msg.length > 0){
			$('.easyui-dialog #errMsgLevelDlg').html(msg).show();
			return false;
		}
		var lstGroupProductLv = new Array();
		$('.easyui-dialog .quantityItem').each(function(){			
			msg = Utils.getMessageOfSpecialCharactersValidateEx($(this).attr('id'),'Số lượng',Utils._TF_NUMBER_COMMA);
			if(msg.length == 0){
				var value = $(this).val().trim();
				if(!isNullOrEmpty(value)){
					value = Utils.returnMoneyValue(value);
				}			
				var key = $(this).attr('grcode').trim() + $(this).attr('prcode');
				var obj = ProgrammeDisplayCatalog.displayProductMap.get(key);
				var strValue = obj.groupId + ';' + obj.productCode + ';' + value;
				lstGroupProductLv.push(strValue);
				
			}else{
				return false;
			}
		});		
		if(msg.length > 0){
			$('.easyui-dialog #errMsgLevelDlg').html(msg).show();
			return false;
		}
		dataModel.lstGroupId = lstGroupId;
		dataModel.lstMinValue = lstMinValue;
		dataModel.lstMaxValue = lstMaxValue;	
		dataModel.lstPercentValue = lstPercentValue;
		dataModel.lstQuantityValue = lstQuantityValue;
		dataModel.lstGroupProductLv = lstGroupProductLv;
		Utils.addOrSaveRowOnGrid(dataModel, "/programme-display/change-level", ProgrammeDisplayCatalog._xhrSave, 'levelGrid','errMsgLevelDlg', function(data){
			showSuccessMsg('successMsgLevelDlg',data);
			ProgrammeDisplayCatalog.dataGridLevel();			
			setTimeout(function(){
				$('.easyui-dialog').dialog('close');
			}, 1500);				
		},null);
		return false;
	},
	getProductGridUrl: function(id, productCode, productName, quantity){
		return '/programme-display/search-product?id=' + encodeChar(id) + '&productCode=' + encodeChar(productCode) + '&productName=' + encodeChar(productName) + '&quantity=' + encodeChar(quantity);
	},
	searchProduct: function(){
		var id = $('#selId').val().trim();		
		var productCode = $('#productCode').val().trim();
		var productName = $('#productName').val().trim();		
		var quantity = $('#quantity').val();
		$('#productCode').focus();
		var url = ProgrammeDisplayCatalog.getProductGridUrl(id, productCode, productName, quantity);
		$("#productGrid").setGridParam({url:url,page:1}).trigger("reloadGrid");
	},
	showStaffTab: function(){
		//applyDateTimePicker("#month", "mm/yy", null, null, null, null, null, null, null,true,null);
		ProgrammeDisplayCatalog.hideAllTab();
		$('#tab5').addClass('Active');
		$('#container5').show();
		var selId = $('#selId').val().trim();
		var now = new Date();
		var cYear = now.getFullYear();
		var cMonth = now.getMonth() + 1;
		if(cMonth != 10 && cMonth != 11 && cMonth != 12  ){
			cMonth = '0'+cMonth;
		}
		var currentDate = cMonth+'/'+cYear;
		$('#month').val(currentDate);
		//$('#month').val(getCurrentMonth());
		$('#shop').combotree('setValue', Number($('#curShopId').val().trim()));
		/*setTimeout(function(){
			ProgrammeDisplayCatalog.searchStaff();
	  	},1000);*/
		setTimeout(function() {
		$('#container5 #staffGrid').datagrid({
			url : '/programme-display/search-staff',
			autoRowHeight : true,
			rownumbers : true, 
			checkOnSelect :true,
			pagination:true,			
			scrollbarSize : 0,
			pageSize:20,
			pageList  :[10,20,30,40,50],
			width : $(window).width()-60,
			queryParams:{				
				month:getCurrentMonth(),
			    id:  encodeChar(selId)
			},
			fitColumns:true,
			columns:[[  
			{field: 'shopCode',title: 'Mã NPP', width:100,align:'left',sortable : false,resizable : false,
				formatter: function(value,row,index){
					return Utils.XSSEncode(row.staff.shop.shopCode);
				}
			},
			{field: 'staffCode',title: 'Mã NVBH', width:120,align:'left',sortable : false,resizable : false,
				formatter: function(value,row,index){
					return Utils.XSSEncode(row.staff.staffCode); 
				}
			},
			{field: 'staffName',title: 'Tên NVBH', width:250,align:'left',sortable : false,resizable : false,
				formatter: function(value,row,index){
					return Utils.XSSEncode(row.staff.staffName); 
				}
			},
			{field: 'month',title: 'Tháng', width:80,align:'center',sortable : false,resizable : false,
				formatter: function(cellValue,row,index){
					if(cellValue!=undefined && cellValue!=null){				
						return $.datepicker.formatDate('mm/yy', new Date(cellValue));
					} 
				}
			},
			{field: 'quantityMax',title: 'Số suất', width:100,align:'right',sortable : false,resizable : false,
				formatter:function(value){
		    		return formatCurrency(value);	
		    	}
			},	
			{field: 'quantityReceived',title: 'Đã phân bổ', width:100,align:'right',sortable : false,resizable : false,
				formatter:function(value){
		    		return formatCurrency(value);	
		    	}
			},
			{field: 'uploadDate',title: 'Ngày tạo', width:100,align:'center',sortable : false,resizable : false,
				formatter: function(cellValue,row,index){
					if(cellValue!=undefined && cellValue!=null){				
						return $.datepicker.formatDate('dd/mm/yy', new Date(cellValue));
					} 
				}
			},
			{field: 'updateDate',title: 'Ngày điều chỉnh', width:100,align:'center',sortable : false,resizable : false,
				formatter: function(cellValue,row,index){
					if(cellValue!=undefined && cellValue!=null){				
						return $.datepicker.formatDate('dd/mm/yy', new Date(cellValue));
					} 
				}
			},
            ]],			
            onLoadSuccess:function(){            	
            	$('.datagrid-header-rownumber').html('STT'); 
            	//$('#container5 #staffGrid').datagrid('options').fitColumns = true;
            	$('#container5 #staffGrid').datagrid('resize');
            }            
		});
		}, 10);
	},
	unCheckDatagrid: function() {
		var rows = $('#staffGrid').datagrid('getRows');
		var isCheckAll = $('#container5 #checkDeleteAll1').is(':checked');
		if(ProgrammeDisplayCatalog._mapStaff.size() == 0){
			if(isCheckAll == true){
			$('#container5 #checkDeleteAll1').attr('checked','checked');
			}else{
			$('#container5 #checkDeleteAll1').removeAttr('checked');
			}
		}else{
	    	$('#container5 .datagrid-header-check input').removeAttr('checked');
	    	for(var i = 0; i < rows.length; i++) {
	    		if(ProgrammeDisplayCatalog._mapStaff.get(rows[i].id)) {
	    			$('#staffGrid').datagrid('uncheckRow', i);
	    		}
	    	}
	    	$('#container5 #checkDeleteAll1').attr('checked','checked');
		}
    	return true;
	},
	deleteQuantityStaff: function() {
		$('.ErrorMsgStyle').hide();
		var isCheckAll = $('#container5 #checkDeleteAll1').is(':checked');
		var lstChecked = $('#container5 #staffGrid').datagrid('getChecked');
		var rows = $('#container5 #staffGrid').datagrid('getRows');		
		var lstIdStaffMap = new Array();
		var month = '';
		for(var i = 0; i < lstChecked.length; i++) {
			ProgrammeDisplayCatalog._mapStaff.put(lstChecked[i].id,lstChecked[i]);			
		}
		var arra = ProgrammeDisplayCatalog._mapStaff.valArray;		
		if((isCheckAll == false && arra.length == 0)
				|| (isCheckAll && rows.length==0)) {
			$('#container5 #errMsgStaff').html('Bạn chưa chọn nhân viên để xóa. Vui lòng chọn nhân viên').show();
			return;
		}		
		for(var i=0,size=arra.length;i<size;++i){
			lstIdStaffMap.push(arra[i].id);
			var dateMonth = new Date(arra[i].month);
			month = toMonthYearString(dateMonth);					
			if(!Utils.compareCurrentMonthEx(month)){
				$('#container5 #errMsgStaff').html('Không được xóa NVBH trong CTTB của tháng trước tháng hiện tại').show();
				return;
			}
			
		}
		if(month == '' || month == null) {
			var dateMonth = new Date(rows[0].month); 
			month = toMonthYearString(dateMonth);				
			if(!Utils.compareCurrentMonthEx(month)){
				$('#container5 #errMsgStaff').html('Không được xóa NVBH trong CTTB của tháng trước tháng hiện tại').show();
				return;
			}
		}
		var params = new Object();
		params.isCheckAll = isCheckAll;
		params.lstIdStaffMap = lstIdStaffMap;
		params.month = month;
		var code = $('#displayProgram').val();
		if(code !=null && code != undefined)
			params.code = code.trim();
		var id = $('#selId').val();
		if(id!=null && id!=undefined)
			params.id = $('#selId').val().trim();		
		Utils.addOrSaveData(params, '/programme-display/staff-map/delete-staff-map', null, 'errMsgStaff', function() {
			$('#container5 #staffGrid').datagrid('reload');
		}, '#container5 ', true,null, ' Bạn có muốn xóa nhân viên được chọn');
		
	},
	exportStaffMap:function(){
		$('.ErrorMsgStyle').hide();
		var id = $('#selId').val().trim();		
		var shopId =  $('#shop').combotree('getValue');
		var month = $('#month').val().trim();
		var params = new Object();
		params.id = id;
		params.shopId = shopId;
		params.month = month;
		$.messager.confirm('Xác nhận','Bạn có muốn xuất file?',function(r){
			if(r){
				ReportUtils.exportReport('/programme-display/staff-map/export-Excel-Staff', params, 'errMsg');					
			}
		});		
		return false;
	},
	openStaffDialog:function(){
		$('.ErrorMsgStyle').hide();
		$('#divStaffContainer').show();
		var html = $('#dialogStaff').html();
		$('#dialogStaff').dialog({  
	        closed: false,  
	        cache: false,  
	        modal: true,
	        position: 'middle',
	        center: true, 
	        onOpen: function(){
	        	$('#staffCodeDlg').val('');
	        	$('#staffNameDlg').val('');
	        	$('.easyui-dialog #errMsgStaffDlg').html('').hide();
	        	ProgrammeDisplayCatalog._mapStaff = new Map();
	        	ProgrammeDisplayCatalog.dataGridStaffOnDialog();
	        	$('#staffCodeDlg,#staffNameDlg').bind('keyup',function(event){
	        		if(event.keyCode == keyCodes.ENTER){
	        			$('#staffGridDialog').datagrid('load',{page:1,id:$('#selId').val().trim(),code:encodeChar($('#staffCodeDlg').val().trim()),name:encodeChar($('#staffNameDlg').val().trim())});
	        		}
	        	});
	        	$('#btnSeachStaffDlg').bind('click',function(){
        			$('#staffGridDialog').datagrid('load',{page:1,id:$('#selId').val().trim(),code:encodeChar($('#staffCodeDlg').val().trim()),name:encodeChar($('#staffNameDlg').val().trim())});
	        	});
	        },
	        onBeforeClose: function() {
	        },
	        onClose : function(){
	        	$('#divStaffContainer').hide();
	        }
	    });
	},
	openCustomerDialog:function(){
		$('#divCustomerContainer').show();
		var html = $('#dialogCustomer').html();
		$('#dialogCustomer').dialog({  
	        closed: false,  
	        cache: false,  
	        modal: true,
	        position: 'middle',
	        center: true, 
	        onOpen: function(){
	        	$('#levelCodeCustomerDlg').val(-1);
	        	$('#levelCodeCustomerDlg').change();
	        	$('#customerCodeDlg').val('');
	        	$('#customerNameDlg').val('');
	        	$('#addressDlg').val('');
	        	$('#startDateDlg').val('');
	        	$('#endDateDlg').val('');
	        	$('.easyui-dialog #errMsgCustomerDlg').html('').hide();
	        	$('#customerCodeDlg,#customerNameDlg,#addressDlg').bind('keyup',function(event){
	        		if(event.keyCode == keyCodes.ENTER){
	        			var code = encodeChar($('#customerCodeDlg').val().trim());
	    	        	var name =$('#customerNameDlg').val().trim().replace(/\s+/g, ' ');
	    	        	var address = $('#addressDlg').val().trim().replace(/\s+/g, ' ');
	        			$('#customerGridDialog').datagrid('load',{page:1,id:$('#selId').val().trim(),code:code,name:name,address:address});
	        		}
	        	});
	        	$('#btnSeachCustomerDlg').bind('click',function(){
	        		var code = encodeChar($('#customerCodeDlg').val().trim());
		        	var name = $('#customerNameDlg').val().trim().replace(/\s+/g, ' ');
		        	var address = $('#addressDlg').val().trim().replace(/\s+/g, ' ');
	        		$('#customerGridDialog').datagrid('load',{page:1,id:$('#selId').val().trim(),code:code,name:name,address:address});
	        	});
	        	ProgrammeDisplayCatalog._mapCustomer = new Map();
	        	ProgrammeDisplayCatalog.dataGridCustomerOnDialog();
	        },
	        onBeforeClose: function() {
	        },
	        onClose : function(){
	        	$('#customerGridDialog').datagrid('load',{page:1,id:$('#selId').val().trim(),code:'',name:'',address:''});
	        	$('#divCustomerContainer').hide();
	        }
	    });
	},
	dataGridStaffOnDialog:function(){
		var params = new Object();
		params.page = 1;
		params.id = $('#selId').val().trim();
		params.code = $('#staffCodeDlg').val().trim();
		params.name = $('#staffNameDlg').val().trim();
		$('#staffGridDialog').datagrid({
			url : '/catalog/programme-display/search-staff-on-dialog',
			queryParams:params,
			total:100,
			width:$('#staffGridDialogContainer').width() -20,
			pageList  : [10,20,30],
			pagination:true,
			pageSize:10,
			scrollbarSize : 0,
			fitColumns:true,
			pageNumber:1,
			rownumbers:true,
			columns:[[  
				{field: 'staffCode',title: 'Mã nhân viên', width:250,align:'left',sortable : false,resizable : false,formatter:function(value,row,index){
					return Utils.XSSEncode(row.staffCode);
				}
				},
				{field: 'staffName',title: 'Tên nhân viên', width:345,align:'left',sortable : false,resizable : false,formatter:function(value,row,index){
					return Utils.XSSEncode(row.staffName);
				}
				},
				{field: 'ck',checkbox:true},
				{field: 'id',hidden:true}
            ]],
			method : 'GET',
            onLoadSuccess:function(data){
        		var html = $('#staffGridDialogContainer .datagrid-view2 .datagrid-body .datagrid-btable tbody').children();
        		var size = html.length;
        		var count = 0;
        		for(var i = 0; i < size ; i++){
        			if(i > 0){
        				html = html.next();
        			}
        			var idValue = html.first().children().last().children().text();
        			if(ProgrammeDisplayCatalog._mapStaff.keyArray.indexOf(parseInt(idValue)) > -1){
        				html.first().children().last().prev().children().children().attr('checked','checked');
        				count++;
        			}else{
        				html.first().children().last().prev().children().children().removeAttr('checked');
        			}
            	}
        		if(count > 0 && count == size){
        			$('#staffGridDialogContainer .datagrid-view2').children().children().children().children().children().children().first().next().next().children().children().attr('checked','checked');
        		}else{
        			$('#staffGridDialogContainer .datagrid-view2').children().children().children().children().children().children().first().next().next().children().children().removeAttr('checked');
        		}
            	$('.datagrid-header-rownumber').html('STT');
            	$('#staffGridDialog').datagrid('resize');
            	$('#loadingStaffDialog').hide();
            },
            onCheck:function(index,row){
           		ProgrammeDisplayCatalog._mapStaff.put(row.id,row.id);
		    },
		    onUncheck:function(index,row){
		    	ProgrammeDisplayCatalog._mapStaff.remove(row.id);
		    },
		    onCheckAll:function(rows){
		    	for(var i = 0;i<rows.length;i++){
		    		var row = rows[i];
		    		ProgrammeDisplayCatalog._mapStaff.put(row.id,row.id);
		    	}
		    },
		    onUncheckAll:function(rows){
		    	for(var i = 0;i<rows.length;i++){
		    		var row = rows[i];
		    		ProgrammeDisplayCatalog._mapStaff.remove(row.id);
		    	}
		    },
		});		
	},
	changeStaff:function(){
		var msg = '';
		$('.easyui-dialog #errMsgStaffDlg').html('').hide();
		var lstStaffId = [];
		var key = ProgrammeDisplayCatalog._mapStaff.keyArray;
		if(key != null && key.length > 0){
			for(var i = 0;i<key.length;i++){
				lstStaffId.push(key[i]);
			}
		}else{
			msg = 'Vui lòng chọn nhân viên phát triển khách hàng';
		}
		if(msg.length > 0){
			$('.easyui-dialog #errMsgStaffDlg').html(msg).show();
			return false;
		}
		var dataModel = new Object();		
		dataModel.id = $('#selId').val().trim();
		dataModel.lstStaffId = lstStaffId;
		Utils.addOrSaveRowOnGrid(dataModel, "/catalog/programme-display/change-staff", ProgrammeDisplayCatalog._xhrSave, 'staffGrid','errMsgStaffDlg', function(data){
			showSuccessMsg('successMsgStaffDlg',data);
			$('.easyui-dialog').dialog('close');
		},null);
		return false;
	},
	changeCustomer:function(){
		var msg = '';
		$('.easyui-dialog #errMsgCustomerDlg').html('').hide();
		$('#errMsgCustomer').html('').hide();
		msg = Utils.getMessageOfRequireCheck('levelCodeCustomerDlg','Mã mức',true);
		if(msg.length == 0){
			msg = Utils.getMessageOfRequireCheck('startDateDlg','Từ ngày');
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfRequireCheck('endDateDlg','Đến ngày');
		}
		var startDate = $('#startDateDlg').val().trim();
		var endDate = $('#endDateDlg').val().trim();
		if(msg.length == 0){
			if(startDate != '' && endDate != ''){
				if(!Utils.compareDate(startDate, endDate)){
					msg = 'Từ ngày phải là ngày trước hoặc cùng ngày với đến ngày';	
					$('#startDateDlg').focus();
				}
			}
		}
		var currentTime = new Date();
		var month = currentTime.getMonth() + 1;
		var day = currentTime.getDate();
		var year = currentTime.getFullYear();
		if(msg.length == 0){
			if(startDate != ''){
				if(getCurrentDate() == startDate || !Utils.compareDate(day + '/' + month + '/' + year,startDate)){
					msg = 'Từ ngày phải lớn hơn ngày hiện tại.';	
					$('#startDateDlg').focus();
				}
			}
		}
		if(msg.length == 0){
			if(endDate != ''){
				if(getCurrentDate() == endDate || !Utils.compareDate(day + '/' + month + '/' + year,endDate)){
					msg = 'Đến ngày phải lớn hơn ngày hiện tại.';	
					$('#endDateDlg').focus();
				}
			}
		}
		var lstCustomerId = [];
		var key = ProgrammeDisplayCatalog._mapCustomer.keyArray;
		if(key != null && key.length > 0){
			for(var i = 0;i<key.length;i++){
				lstCustomerId.push(key[i]);
			}
		}else{
			msg = 'Vui lòng chọn khách hàng tham gia';
		}
		if(msg.length > 0){
			$('.easyui-dialog #errMsgCustomerDlg').html(msg).show();
			return false;
		}
		var dataModel = new Object();		
		dataModel.id = $('#selId').val().trim();
		dataModel.staffId = $('#staffId').val().trim();
		dataModel.lstCustomerId = lstCustomerId;
		dataModel.levelId = $('#levelCodeCustomerDlg').val();
		dataModel.fromDate = startDate;
		dataModel.toDate = endDate;
		Utils.addOrSaveRowOnGrid(dataModel, "/catalog/programme-display/change-customer", ProgrammeDisplayCatalog._xhrSave, 'customerGrid','errMsgCustomerDlg', function(data){
			$('#staffGrid').datagrid('reload');
			showSuccessMsg('successMsgCustomerDlg',data);
			$('.easyui-dialog').dialog('close');
		},null);
		return false;
	},
	dataGridCustomerOnDialog:function(){
		var params = new Object();
		params.page = 1;
		params.id = $('#selId').val().trim();
		params.code = $('#customerCodeDlg').val().trim();
		params.name = $('#customerNameDlg').val().trim();
		params.address = $('#addressDlg').val().trim();
		var url = '/catalog/programme-display/search-customer-on-dialog';
		$('#customerGridDialog').datagrid({
			url : url,
			total:100,
			queryParams:params,
			width:$('#customerGridDialogContainer').width() -20,
			pageList  : [10,20,30],
			pagination:true,
			pageSize:10,
			scrollbarSize : 0,
			fitColumns:true,
			rownumbers:true,
			columns:[[  
			{field: 'shortCode',title: 'Mã KH', width:150,align:'left',sortable : false,resizable : false,formatter:function(value,row,index){
				return Utils.XSSEncode(row.shortCode);
			}
			},
			{field: 'customerName',title: 'Tên KH', width:150,align:'left',sortable : false,resizable : false,formatter:function(value,row,index){
				return Utils.XSSEncode(row.customerName);
			}
			},
			{field: 'address',title: 'Địa chỉ', width:280,align:'left',sortable : false,resizable : false,formatter:function(value,row,index){
				return Utils.XSSEncode(row.address);
			}
			},
			{field: 'ck',checkbox:true},
			{field: 'id',hidden:true}
            ]],
			method : 'GET',
            onLoadSuccess:function(){
            	var html = $('#customerGridDialogContainer .datagrid-view2 .datagrid-body .datagrid-btable tbody').children();
            	var size = html.length;
            	var count = 0;
        		for(var i = 0; i < size ; i++){
        			if(i > 0){
        				html = html.next();
        			}
        			var idValue = html.first().children().last().children().text();
        			if(ProgrammeDisplayCatalog._mapCustomer.keyArray.indexOf(parseInt(idValue)) > -1){
        				html.first().children().last().prev().children().children().attr('checked','checked');
        				count++;
        			}else{
        				html.first().children().last().prev().children().children().removeAttr('checked');
        			}
            	}
        		if(count > 0 && count == size){
        			$('#customerGridDialogContainer .datagrid-view2').children().children().children().children().children().children().first().next().next().next().children().children().attr('checked','checked');
        		}else{
        			$('#customerGridDialogContainer .datagrid-view2').children().children().children().children().children().children().first().next().next().next().children().children().removeAttr('checked');
        		}
            	$('.datagrid-header-rownumber').html('STT');
            	$('#customerGridDialog').datagrid('resize');
            	$('#loadingCustomerDialog').hide();
            	setTimeout(function(){
            		CommonSearchEasyUI.fitEasyDialog();
        	  	},1000);
            },
            onCheck:function(index,row){
           		 ProgrammeDisplayCatalog._mapCustomer.put(row.id,row.id);
		    },
		    onUncheck:function(index,row){
		    	ProgrammeDisplayCatalog._mapCustomer.remove(row.id);
		    },
		    onCheckAll:function(rows){
		    	for(var i = 0;i<rows.length;i++){
		    		var row = rows[i];
		    		ProgrammeDisplayCatalog._mapCustomer.put(row.id,row.id);
		    	}
		    },
		    onUncheckAll:function(rows){
		    	for(var i = 0;i<rows.length;i++){
		    		var row = rows[i];
		    		ProgrammeDisplayCatalog._mapCustomer.remove(row.id);
		    	}
		    },
		});	
	},
	viewCustomerInfo:function(staffId,staffCode,staffName){
		$('#staffId').val(staffId);
		$('#divCustomerDetail').show();
		$('#toggleCustomer').removeClass('LinkHideStyle').addClass('LinkShowStyle');
		ProgrammeDisplayCatalog.toggleCustomer();
		$('#divStaffInfoId').html(staffCode+'-'+staffName);
		ProgrammeDisplayCatalog.showCustomerTab(staffId);
		$('#customerCode,#customerName,#startDate,#endDate').bind('keyup',function(event){
    		if(event.keyCode == keyCodes.ENTER){
    			var msg = '';
    			$('#errMsgCustomerSearch').html('').hide();
    			msg = Utils.getMessageOfInvalidFormatDate('startDate','Từ ngày');
    			if(msg.length == 0){
    				msg = Utils.getMessageOfInvalidFormatDate('endDate','Đến ngày');
    			}
    			if(msg.length == 0){
    				if($('#startDate').val().trim() != '' && $('#endDate').val().trim() != ''){
    					if(!Utils.compareDate($('#startDate').val().trim(), $('#endDate').val().trim())){
    						msg = 'Từ ngày phải là ngày trước hoặc cùng ngày với đến ngày';				
    					}
    				}
    			}
    			if(msg.length > 0){
    				$('#errMsgCustomerSearch').html(msg).show();
    				return false;
    			}
    			$('#customerGrid').datagrid('load',{page:1,staffId:staffId,code:$('#customerCode').val().trim(),name:$('#customerName').val().trim(),fromDate:$('#startDate').val().trim(),toDate:$('#endDate').val().trim(),levelId:$('#levelCode').val()});
    		}
    	});
		$('#btnSearchCustomer').bind('click',function(){
			var msg = '';
			$('#errMsgCustomerSearch').html('').hide();
			msg = Utils.getMessageOfInvalidFormatDate('startDate','Từ ngày');
			if(msg.length == 0){
				msg = Utils.getMessageOfInvalidFormatDate('endDate','Đến ngày');
			}
			if(msg.length > 0){
				$('#errMsgCustomerSearch').html(msg).show();
				return false;
			}
			$('#customerGrid').datagrid('load',{page:1,staffId:staffId,code:$('#customerCode').val().trim(),name:$('#customerName').val().trim(),fromDate:$('#startDate').val().trim(),toDate:$('#endDate').val().trim(),levelId:$('#levelCode').val()});
		});
		$(window).scrollTop(5000);
	},
	toggleCustomer:function(){
		if($('#toggleCustomer').hasClass('LinkHideStyle')){		
			$('#toggleCustomer').addClass('LinkShowStyle');
			$('#toggleCustomer').removeClass('LinkHideStyle');
			$('#divCustomerSearch').show();
			$('#customerCode').focus();
			$('#toggleCustomer').html("Đóng tìm kiếm &lt;&lt;");
		}else {
			$('#toggleCustomer').addClass('LinkHideStyle');
			$('#toggleCustomer').removeClass('LinkShowStyle');
			$('#divCustomerSearch').hide();
			$('#toggleCustomer').html("Tìm kiếm &lt;&lt;");
		}
	},
	showCustomerTab: function(staffId){
		$('#customerCode').val('');
		$('#customerName').val('');
		$('#startDate').val('');
		$('#endDate').val('');
		$('#levelCode').val(-1);
		$('#levelCode').change();
		var params = new Object();
		params.page = 1;
		params.staffId = staffId;
		params.code = $('#customerCode').val().trim();
		params.name = $('#customerName').val().trim();
		params.fromDate = $('#startDate').val().trim();
		params.toDate = $('#endDate').val().trim();
		params.levelId = -1;
		var title = '<a href="javascript:void(0);" onclick="return ProgrammeDisplayCatalog.openCustomerDialog();"><img src="/resources/images/icon-add.png"></a>';
		if(ProgrammeDisplayCatalog.isStopped){
			title = '';
		}
		var url = '/catalog/programme-display/customer/search';
		$('#customerGrid').datagrid({
			url : url,
			total:100,
			queryParams:params,
			width:$('#customerGridContainer').width() -20,
			pageList  : [10,20,30],
			pagination:true,
			pageSize:10,
			scrollbarSize : 0,
			fitColumns:true,
			rownumbers:true,
			singleSelect:true,
			columns:[[  
			{field: 'shortCode',title: 'Mã KH', width:150,align:'center',sortable : false,resizable : false,formatter:function(value,row,index){
				return Utils.XSSEncode(row.shortCode);
			}
			},
			{field: 'customerName',title: 'Tên KH', width:250,align:'left',sortable : false,resizable : false,formatter:function(value,row,index){
				return Utils.XSSEncode(row.customerName);
			}
			},
			{field: 'levelCode',title: 'Mức', width:160,align:'left',sortable : false,resizable : false,
				editor:{  
                    type:'combobox',
                    options:{
                    	data: ProgrammeDisplayCatalog._lstLevelCode.get(0).rows,
            	        valueField:'levelCode',  
            	        textField:'levelCode',
            	        method:'GET',
            	        class:'easyui-combobox',
            	        filter: function(q, row){
                    		var opts = $(this).combobox('options');
                    		return row[opts.textField].indexOf(q.toUpperCase()) == 0;
                    	}
                    }
				},
			},
			{field: 'fromDateStr',title:'Từ ngày', width:130,align:'center',sortable : false,resizable : false,
				editor:{  
                    type:'text'
				},
        	},
        	{field: 'toDateStr',title:'Đến ngày', width:130,align:'center',sortable : false,resizable : false,
        		editor:{  
                    type:'text'
				},
        	},
			{field: 'edit',title:'', width:80,align:'center',sortable : false,resizable : false,
        		formatter: function(value,row,index){
        			if(!ProgrammeDisplayCatalog.isStopped){
        				if(row != null && row != undefined){
        					var currentTime = new Date();
        					var month = currentTime.getMonth() + 1;
        					var day = currentTime.getDate();
        					var year = currentTime.getFullYear();
        					var fromDate = row.fromDateStr;
        					var toDate = row.toDateStr;
        					if(fromDate != '' && toDate != ''){
        						if(!Utils.compareDate(day + '/' + month + '/' + year,fromDate) && !Utils.compareDate(day + '/' + month + '/' + year,toDate)){
        							if(!ProgrammeDisplayCatalog.checkFromDate || !ProgrammeDisplayCatalog.checkToDate){
        								if (row.editing){
        									var s = "<a title='Lưu' href='javascript:void(0)' onclick=\"return ProgrammeDisplayCatalog.saveRowCustomer(this,'customerGrid');\"><span style='cursor:pointer'><img src='/resources/images/icon-datach.png'/></span></a> ";  
        									var c = "<a title='Hủy' href='javascript:void(0)' onclick=\"ProgrammeDisplayCatalog.cancelRowCustomer(this,'customerGrid');\"><span style='cursor:pointer'><img src='/resources/images/icon_esc.png'/></span></a>";  
        									return s + c;
        								}else {
        									return "<a title='Sửa' href='javascript:void(0)' onclick=\"ProgrammeDisplayCatalog.editRowCustomer(this,'customerGrid');\"><span style='cursor:pointer'><img src='/resources/images/icon-edit.png'/></span></a>";  
        								} 
        							}else{
        								return '';
        							}
        						}else{
        							if (row.editing){  
        								var s = "<a title='Lưu' href='javascript:void(0)' onclick=\"return ProgrammeDisplayCatalog.saveRowCustomer(this,'customerGrid');\"><span style='cursor:pointer'><img src='/resources/images/icon-datach.png'/></span></a> ";  
        								var c = "<a title='Hủy' href='javascript:void(0)' onclick=\"ProgrammeDisplayCatalog.cancelRowCustomer(this,'customerGrid');\"><span style='cursor:pointer'><img src='/resources/images/icon_esc.png'/></span></a>";  
        								return s + c;  
        							} else {
        								return "<a title='Sửa' href='javascript:void(0)' onclick=\"ProgrammeDisplayCatalog.editRowCustomer(this,'customerGrid');\"><span style='cursor:pointer'><img src='/resources/images/icon-edit.png'/></span></a>";  
        							}  
        						}
        					}
        				}
        			}
        	    }
			},
			{field: 'remove',title:title, width:80,align:'center',sortable : false,resizable : false,
				formatter: function(value,row,index){
					if(!ProgrammeDisplayCatalog.isStopped){
						if(row != null && row != undefined){
							var currentTime = new Date();
							var month = currentTime.getMonth() + 1;
							var day = currentTime.getDate();
							var year = currentTime.getFullYear();
							var fromDate = row.fromDateStr;
							var toDate = row.toDateStr;
							if(fromDate != '' && toDate != ''){
								if(!Utils.compareDate(day + '/' + month + '/' + year,fromDate) || !Utils.compareDate(day + '/' + month + '/' + year,toDate)){
									return '';
								}else{
									return "<a title='Xóa' href='javascript:void(0)' onclick=\"return ProgrammeDisplayCatalog.deleteCustomerRow('"+row.displayCustomerMapId+"');\"><img src='/resources/images/icon-delete.png'/></a>";
								}
							}
						}
					}
        	    }
			}
            ]],
			method : 'GET',
            onLoadSuccess:function(data){
            	$('.datagrid-header-rownumber').html('STT');
            	$('#customerGrid').datagrid('resize');
            	for(var i = 0; i < data.rows.length ; i++){
            		ProgrammeDisplayCatalog.toDate.put(data.rows[i].displayCustomerMapId,data.rows[i].toDateStr);
            		ProgrammeDisplayCatalog.fromDate.put(data.rows[i].displayCustomerMapId,data.rows[i].fromDateStr);
	        	}
            	edit = false;
            },
            onBeforeEdit:function(index,row){  
		        row.editing = true;
		        edit =true;
		        idx = index;  
		        ProgrammeDisplayCatalog.updateActions('customerGrid',index);  
		    },  
		    onAfterEdit:function(index,row){  
		        row.editing = false;
		        edit = false;
		        idx = -1;
		        ProgrammeDisplayCatalog.updateActions('customerGrid',index);  
		    },  
		    onCancelEdit:function(index,row){  
		        row.editing = false;  
		        edit = false;
		        idx = -1;
		        ProgrammeDisplayCatalog.updateActions('customerGrid',index);  
		    }  
		});		
	},
	cancelRowCustomer:function (target,gridDetailId){  
		var indexRow = ProgrammeDisplayCatalog.getRowIndex(target);
	    $('#'+gridDetailId).datagrid('cancelEdit', indexRow);  
	    $('#'+gridDetailId).datagrid('beginEdit', indexRow);
	    var ed = $('#'+gridDetailId).datagrid('getEditor', {index:indexRow,field:'toDateStr'});
		if(ed != null && ed != undefined){
			$(ed.target).val(ProgrammeDisplayCatalog.toDate.get($('#'+gridDetailId).datagrid('getData').rows[indexRow].displayCustomerMapId));
		}
		$('#'+gridDetailId).datagrid('endEdit', indexRow); 
	},
	editRowCustomer:function(target,gridDetailId){
		var indexRow = ProgrammeDisplayCatalog.getRowIndex(target);
	    $('#'+gridDetailId).datagrid('beginEdit', indexRow);
	    var fromDate = $('#'+gridDetailId).datagrid('getData').rows[indexRow].fromDateStr;
	    var toDate = $('#'+gridDetailId).datagrid('getData').rows[indexRow].toDateStr;
	    var currentTime = new Date();
		var month = currentTime.getMonth() + 1;
		var day = currentTime.getDate();
		var year = currentTime.getFullYear();
	    if(fromDate != ''){
	    	var ed = $('#'+gridDetailId).datagrid('getEditor', {index:indexRow,field:'fromDateStr'});
			if(!Utils.compareDate(day + '/' + month + '/' + year,fromDate)){
				if(ed != null && ed != undefined){
					$(ed.target).attr('disabled','disabled');
					ProgrammeDisplayCatalog.checkFromDate = false;
				}
			}else{
				applyDateTimePicker(ed.target,null,null,null,null,null,true);
			}
		}
	    if(toDate != ''){
	    	var ed = $('#'+gridDetailId).datagrid('getEditor', {index:indexRow,field:'toDateStr'});
			if(!Utils.compareDate(day + '/' + month + '/' + year,toDate)){
				if(ed != null && ed != undefined){
					$(ed.target).attr('disabled','disabled');
					ProgrammeDisplayCatalog.checkToDate = false;
				}
			}else{
				applyDateTimePicker(ed.target,null,null,null,null,null,true);
			}
		}
	    if(!ProgrammeDisplayCatalog.checkFromDate || !ProgrammeDisplayCatalog.checkToDate){
	    	var ed = $('#'+gridDetailId).datagrid('getEditor', {index:indexRow,field:'levelCode'});
	    	if(ed != null && ed != undefined){
	    		var html = $(ed.target).parent().children().last().children().first();
	    		html.removeClass('combo-text validatebox-text').addClass('datagrid-editable-input');
	    		html.attr('value',html.next().next().val());
	    		html.attr('disabled','disabled');
	    		html.next().children().removeClass('combo-arrow');
			}
	    }
	},
	saveRowCustomer:function(target,gridId){
		var indexRow = ProgrammeDisplayCatalog.getRowIndex(target);
		$('#'+gridId).datagrid('endEdit', indexRow);
		var dataModel = new Object();		
		dataModel.id = $('#selId').val().trim();
		dataModel.staffId = $('#staffId').val().trim();
		dataModel.displayCustomerMapId = $('#'+gridId).datagrid('getData').rows[indexRow].displayCustomerMapId;
		dataModel.isUpdate = true;
		var levelCode = $('#'+gridId).datagrid('getData').rows[indexRow].levelCode;
		var fromDate = $('#'+gridId).datagrid('getData').rows[indexRow].fromDateStr;
		var toDate = $('#'+gridId).datagrid('getData').rows[indexRow].toDateStr;
		var msg = '';
		$('#errMsgCustomer').html('').hide();
		if(levelCode == null || levelCode == undefined || levelCode == ''){
			msg = 'Vui lòng chọn mã mức';
		}
		if(msg.length == 0){
			if (fromDate != '' && !Utils.isDate(fromDate)) {
				msg = 'Từ ngày không hợp lệ, vui lòng nhập theo định dạng dd/MM/yyyy';
			}
		}
		if(msg.length == 0){
			if (toDate != '' && !Utils.isDate(toDate)) {
				msg = 'Đến ngày không hợp lệ, vui lòng nhập theo định dạng dd/MM/yyyy';
			}
		}
		if(msg.length == 0){
			if(fromDate == null || fromDate == undefined || fromDate == ''){
				msg = 'Vui lòng nhập từ ngày';
			}
		}
		if(msg.length == 0){
			if(toDate == null || toDate == undefined || toDate == ''){
				msg = 'Vui lòng nhập đến ngày';
			}
		}
		if(msg.length == 0){
			if(fromDate != '' && toDate != ''){
				if(!Utils.compareDate(fromDate, toDate)){
					msg = 'Từ ngày phải là ngày trước hoặc cùng ngày với đến ngày';	
				}
			}
		}
		var currentTime = new Date();
		var month = currentTime.getMonth() + 1;
		var day = currentTime.getDate();
		var year = currentTime.getFullYear();
		if(msg.length == 0){
			if(fromDate != ''){
				if(ProgrammeDisplayCatalog.checkFromDate){
					if(getCurrentDate() == fromDate || !Utils.compareDate(day + '/' + month + '/' + year,fromDate)){
						msg = 'Từ ngày phải lớn hơn ngày hiện tại.';	
					}
				}
			}
		}
		if(msg.length == 0){
			if(toDate != ''){
				if(getCurrentDate() == toDate || !Utils.compareDate(day + '/' + month + '/' + year,toDate)){
					msg = 'Đến ngày phải lớn hơn ngày hiện tại.';	
				}
			}
		}
		if(msg.length > 0){
			$('#errMsgCustomer').html(msg).show();
			$('#'+gridId).datagrid('beginEdit', indexRow);
			var ed = $('#'+gridId).datagrid('getEditor', {index:indexRow,field:'fromDateStr'});
			if(ed != null && ed != undefined){
				$(ed.target).val(ProgrammeDisplayCatalog.fromDate.get(dataModel.displayCustomerMapId));
				if(!ProgrammeDisplayCatalog.checkFromDate){
					$(ed.target).attr('disabled','disabled');
				}else{
					applyDateTimePicker(ed.target,null,null,null,null,null,true);
				}
			}
			var ed = $('#'+gridId).datagrid('getEditor', {index:indexRow,field:'toDateStr'});
			if(ed != null && ed != undefined){
				$(ed.target).val(ProgrammeDisplayCatalog.toDate.get(dataModel.displayCustomerMapId));
				applyDateTimePicker(ed.target,null,null,null,null,null,true);
			}
			if(!ProgrammeDisplayCatalog.checkFromDate || !ProgrammeDisplayCatalog.checkToDate){
		    	var ed = $('#'+gridId).datagrid('getEditor', {index:indexRow,field:'levelCode'});
		    	if(ed != null && ed != undefined){
		    		var html = $(ed.target).parent().children().last().children().first();
		    		html.removeClass('combo-text validatebox-text').addClass('datagrid-editable-input');
		    		html.attr('value',html.next().next().val());
		    		html.attr('disabled','disabled');
		    		html.next().children().removeClass('combo-arrow');
				}
		    }
			return false;
		}
		dataModel.levelCode = levelCode;
		dataModel.fromDate = fromDate;
		dataModel.toDate = toDate;
		Utils.addOrSaveRowOnGrid(dataModel, "/catalog/programme-display/change-customer", ProgrammeDisplayCatalog._xhrSave, 'customerGrid','errMsgCustomer', function(data){
		});
		return false;
	},
	getStaffGridUrl: function(id,staffCode,staffName){
		return '/catalog/programme-display/search-staff?id=' + encodeChar(id) + '&staffCode=' + encodeChar(staffCode) + '&staffName=' + encodeChar(staffName);   
	},
	getCustomerGridUrl: function(code,name,fromDate,toDate,level){		
		var staffMapId = $('#selStaffId').val().trim();				
		if(level == '-2'){
			level = '';
		}
		return '/catalog/programme-display/customer/search?staffId=' + encodeChar(staffMapId) + '&code=' + encodeChar(code) + '&name=' + encodeChar(name) + '&fromDate=' + encodeChar(fromDate) + '&toDate=' + encodeChar(toDate) + '&levelId=' + encodeChar(level);
	},
	searchStaff: function(){
		ProgrammeDisplayCatalog._mapStaff = new Map();
		var id = $('#selId').val().trim();		
		var shopId = $('#shop').combotree('getValue');
		var month = $('#month').val().trim();		
		var params = new Object();
		params.id = id;
		params.shopId = shopId;
		params.month = month;
		$("#staffGrid").datagrid('reload',params);
		$("#staffGrid").datagrid('uncheckAll');
		if(!Utils.compareCurrentMonthEx(month)){
			$('#checkDeleteAll1').attr('disabled',true);
			$('#checkDeleteAll1').attr('checked',false);
			disabled('staffImportBtn');
		}else{
			$('#checkDeleteAll1').attr('disabled',false);
			enable('staffImportBtn');
		}
		
	},
	deleteLevelRow: function(levelId){
		var dataModel = new Object();
		dataModel.levelId = levelId;	
		Utils.deleteSelectRowOnGrid(dataModel, 'mã mức', '/programme-display/delete-level', ProgrammeDisplayCatalog._xhrDel, 'levelGrid','errMsgLevel',function(data){
			showSuccessMsg('successMsgLevel',data);
			ProgrammeDisplayCatalog.dataGridLevel();
		},'levelLoading');		
		return false;
	},
	addOrSaveLevelTab: function(isEdit){
		var msg = '';
		$('#errMsgLvlDlg').html('').hide();
		msg = Utils.getMessageOfRequireCheck('levelCodeLvlDlg','Mã định mức');
		if(msg.length == 0){
			msg = Utils.getMessageOfSpecialCharactersValidate('levelCodeLvlDlg','Mã định mức',Utils._CODE);
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfRequireCheck('amountLvlDlg','Số định mức');
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfNegativeNumberCheck('amountLvlDlg','Số định mức',Utils._TF_NUMBER_COMMA);
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfInvalidMaxLength('numSKULvlDlg', 'Số SKU');
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfNegativeNumberCheck('numSKULvlDlg','Số SKU',Utils._TF_NUMBER_COMMA);
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfRequireCheck('numProductDisplayLvlDlg','Số mặt trưng bày');
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfNegativeNumberCheck('numProductDisplayLvlDlg','Số mặt trưng bày',Utils._TF_NUMBER_COMMA);
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfInvalidMaxLength('percentDisCountPassLvlDlg', '% đạt DS');
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfNegativeNumberCheck('percentDisCountPassLvlDlg','% đạt DS',Utils._TF_NUMBER_COMMA);
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfInvalidMaxLength('percentDisCountOverLvlDlg', '% vượt DS');
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfNegativeNumberCheck('percentDisCountOverLvlDlg','% vượt DS',Utils._TF_NUMBER_COMMA);
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfInvalidMaxLength('revenueBonusLvlDlg', 'Thưởng DS');
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfNegativeNumberCheck('revenueBonusLvlDlg','Thưởng DS',Utils._TF_NUMBER_COMMA);
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfInvalidMaxLength('maxDisCountLvlDlg', 'Max chiết khấu');
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfNegativeNumberCheck('maxDisCountLvlDlg','Max chiết khấu',Utils._TF_NUMBER_COMMA);
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfInvalidMaxLength('displayBonusLvlDlg', 'Thưởng TB');
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfNegativeNumberCheck('displayBonusLvlDlg','Thưởng TB',Utils._TF_NUMBER_COMMA);
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfInvalidMaxLength('percentSupportDisplayLvlDlg', '% hỗ trợ TB');
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfNegativeNumberCheck('percentSupportDisplayLvlDlg','% hỗ trợ TB',Utils._TF_NUMBER_COMMA);
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfInvalidMaxLength('paidPriceLvlDlg', 'Đơn giá trả');
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfNegativeNumberCheck('paidPriceLvlDlg','Đơn giá trả',Utils._TF_NUMBER_COMMA);
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfInvalidMaxLength('maxSupportDisplayLvlDlg', 'Max hỗ trợ TB');
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfNegativeNumberCheck('maxSupportDisplayLvlDlg','Max hỗ trợ TB',Utils._TF_NUMBER_COMMA);
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfInvalidMaxLength('displayPostion1LvlDlg', 'Vị trí bày bán 1');
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfNegativeNumberCheck('displayPostion1LvlDlg','Vị trí bày bán 1',Utils._TF_NUMBER_COMMA);
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfInvalidMaxLength('displayPostion2LvlDlg', 'Vị trí bày bán 2');
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfNegativeNumberCheck('displayPostion2LvlDlg','Vị trí bày bán 2',Utils._TF_NUMBER_COMMA);
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfInvalidMaxLength('displayPostion3LvlDlg', 'Vị trí bày bán 3');
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfNegativeNumberCheck('displayPostion3LvlDlg','Vị trí bày bán 3',Utils._TF_NUMBER_COMMA);
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfInvalidMaxLength('displayPostion4LvlDlg', 'Vị trí bày bán 4');
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfNegativeNumberCheck('displayPostion4LvlDlg','Vị trí bày bán 4',Utils._TF_NUMBER_COMMA);
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfRequireCheck('statusLvlDlg','Trạng thái',true);
		}		
		if(msg.length > 0){
			$('#errMsgLvlDlg').html(msg).show();
			return false;
		}
		var params = new Object();		
		params.id = $('#selId').val().trim();
		params.levelId = $('#selLevelId').val().trim();
		params.levelCode = $('#levelCodeLvlDlg').val().trim();
		params.levelAmount =  Utils.returnMoneyValue($('#amountLvlDlg').val().trim());
		params.levelNumSKU =  Utils.returnMoneyValue($('#numSKULvlDlg').val().trim());
		params.levelNumProductDisplay =  Utils.returnMoneyValue($('#numProductDisplayLvlDlg').val().trim());
		var checked = $('#chkBoxDisCountDisplayLvlDlg').is(':checked');
		if(checked){
			params.levelDisCountEvenDisplayFailed = 1;
		} else{
			params.levelDisCountEvenDisplayFailed = 0;
		}
		params.levelPercentDisCountPass =  Utils.returnMoneyValue($('#percentDisCountPassLvlDlg').val().trim());
		params.levelPercentDisCountOver =  Utils.returnMoneyValue($('#percentDisCountOverLvlDlg').val().trim());
		params.levelRevenueBonusType = $('#revenueBonusTypeLvlDlg').val();
		params.levelRevenueBonus =  Utils.returnMoneyValue($('#revenueBonusLvlDlg').val().trim());
		params.levelMaxDisCount =  Utils.returnMoneyValue($('#maxDisCountLvlDlg').val().trim());

		params.levelDisplayBonusType = $('#displayBonusTypeLvlDlg').val();
		params.levelDisplayBonus =  Utils.returnMoneyValue($('#displayBonusLvlDlg').val().trim());
		params.levelPercentSupportDisplay =  Utils.returnMoneyValue($('#percentSupportDisplayLvlDlg').val().trim());
		params.levelPaidPrice =  Utils.returnMoneyValue($('#paidPriceLvlDlg').val().trim());
		params.levelMaxSupportDisplay =  Utils.returnMoneyValue($('#maxSupportDisplayLvlDlg').val().trim());
		params.levelDisplayPostion1 =  Utils.returnMoneyValue($('#displayPostion1LvlDlg').val().trim());
		params.levelDisplayPostion2 =  Utils.returnMoneyValue($('#displayPostion2LvlDlg').val().trim());
		params.levelDisplayPostion3 =  Utils.returnMoneyValue($('#displayPostion3LvlDlg').val().trim());
		params.levelDisplayPostion4 =  Utils.returnMoneyValue($('#displayPostion4LvlDlg').val().trim());
		params.levelPaidProductCode = $('#paidProductCodeLvlDlg').val();
		params.levelStatus = $('#statusLvlDlg').val();	
		
		// thuoc tinh mo rong
		if(!Utils.validateAttributeData(params, '#errMsgLvlDlg','.fancybox-inner ')){
			return false;
		}
		var msg = null;
		Utils.addOrSaveData(params, '/catalog/programme-display/save-level', null, 'errMsgLvlDlg', function(data){
			$("#levelGrid").trigger("reloadGrid");
			showSuccessMsg('successMsgLvlDlg',data, function(){
				$.fancybox.close();
			},500);
		}, null, null, null, msg);	
		return false;
	},
	saveProductTab: function(){
		var msg = '';
		$('#errMsgProduct').html('').hide();
		if($('#rbCat').is(':checked')){
			msg = Utils.getMessageOfRequireCheck('catCode','Mã ngành hàng');
			if(msg.length == 0){
				msg = Utils.getMessageOfSpecialCharactersValidate('catCode','Mã ngành hàng');
			}
		}
		if($('#rbSubCat').is(':checked')){
			msg = Utils.getMessageOfRequireCheck('subCatCode','Mã ngành hàng con');
			if(msg.length == 0){
				msg = Utils.getMessageOfSpecialCharactersValidate('subCatCode','Mã ngành hàng con');
			}
		}		
		if($('#rbProduct').is(':checked')){
			msg = Utils.getMessageOfRequireCheck('productCode','Mã sản phẩm');
			if(msg.length == 0){
				msg = Utils.getMessageOfSpecialCharactersValidate('productCode','Mã sản phẩm');
			}
		}	
		if(msg.length > 0){
			$('#errMsgProduct').html(msg).show();
			return false;
		}
		var dataModel = new Object();		
		dataModel.id = $('#selId').val().trim();
		dataModel.productId = $('#selProductId').val().trim();
		dataModel.catCode = $('#catCode').val().trim();
		dataModel.subCatCode = $('#subCatCode').val().trim();		
		dataModel.productCode = $('#productCode').val();		
		Utils.addOrSaveRowOnGrid(dataModel, "/catalog/programme-display/save-product", ProgrammeDisplayCatalog._xhrSave, 'productGrid','errMsgProduct', function(data){	
			$("#productGrid").trigger("reloadGrid");
			$('#catCode').val('');
			$('#subCatCode').val('');
			$('#productCode').val('');
			$('#productName').val('');
			showSuccessMsg('successMsgProduct',data);
		},'productLoading');		
		return false;
	},
	saveQuotaGroup: function(){
		var msg = '';
		$('#errMsgQuotaGroup').html('').hide();
		msg = Utils.getMessageOfRequireCheck('quotaCode','Mã nhóm');
		if(msg.length == 0){
			msg = Utils.getMessageOfSpecialCharactersValidate('quotaCode','Mã nhóm',Utils._CODE);
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfRequireCheck('quotaName','Tên nhóm');
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfSpecialCharactersValidate('quotaName','Tên nhóm');
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfRequireCheck('fromPercent','% Doanh số tối thiểu');// %
																						// Doanh
																						// số
																						// từ
		}
		if(msg.length == 0 && (isNaN($('#fromPercent').val().trim()) || (parseInt($('#fromPercent').val().trim()) < 0 || parseInt($('#fromPercent').val().trim()) > 100))){
			msg = '% Doanh số tối thiểu phải là số nguyên dương <= 100';// %
																		// doanh
																		// số từ
																		// phải
																		// là số
																		// nguyên
																		// dương
																		// <=
																		// 100
			$('#fromPercent').focus(); 
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfRequireCheck('toPercent','% Doanh số tối đa');
		}
		if(msg.length == 0 && (isNaN($('#toPercent').val().trim()) ||parseInt($('#toPercent').val().trim()) <=0)){
			msg = format(msgErr_possitive_integer,'% Doanh số tối đa');
			$('#toPercent').focus();
		}
		if(msg.length == 0 && (isNaN($('#toPercent').val().trim()) || parseInt($('#toPercent').val().trim()) < parseInt($('#fromPercent').val().trim()))){
			msg = "% Doanh số tối thiểu không được lớn hơn % Doanh số tối đa";
			$('#fromPercent').focus();
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfRequireCheck('fieldMap','Chọn trường map',true);
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfRequireCheck('quotaStatus','Trạng thái',true);
		}
		if(msg.length == 0 && $('#toPercent').val().trim().length>8){
			msg = "Chiều dài trường % Doanh số tối đa vượt quá 8 ký tự";
			$('#toPercent').focus();
		}
		if(msg.length == 0 && $('#fromPercent').val().trim().length>8){
			msg = "Chiều dài trường % Doanh số tối thiểu vượt quá 8 ký tự";
			$('#fromPercent').focus();
		}
		if(msg.length > 0){
			$('#errMsgQuotaGroup').html(msg).show();
			return false;
		}
		var dataModel = new Object();		
		dataModel.id = $('#selId').val().trim();
		dataModel.quotaId = $('#selQuotaId').val().trim();
		dataModel.quotaCode = $('#quotaCode').val().trim();
		dataModel.quotaName = $('#quotaName').val().trim();		
		dataModel.fromPercent = $('#fromPercent').val();
		dataModel.toPercent = $('#toPercent').val();
		dataModel.quotaStatus = $('#quotaStatus').val();
		dataModel.productSubCatCode = $('#fieldMap').val();		
		Utils.addOrSaveRowOnGrid(dataModel, "/catalog/programme-display/save-quota-group", ProgrammeDisplayCatalog._xhrSave, 'quotaGrid','errMsgQuotaGroup', function(data){
			$('#selQuotaId').val(data.id);
			ProgrammeDisplayCatalog.cancelQuota();
			showSuccessMsg('successMsgQuotaGroup',data);
		},'quotaLoading');		
		return false;
	},
	getChangedQuotaForm: function(isEdit,groupId){ // them moi' & edit Click
		var params = new Object();
		params.id = groupId;
		Utils.getHtmlDataByAjax(params, '/catalog/programme-display/popupGroup', function(data) {
			var title ='';
			if(isEdit == false){
				title = 'Thêm nhóm chỉ tiêu ';
			} else{
				title = 'Sửa nhóm chỉ tiêu ';
			}
			$.fancybox(data, {
				modal: true,
				title: title,
				afterShow: function () {
					$('.fancybox-inner #quotaStatusF').customStyle();
					$('.fancybox-inner #btnCancelF').bind('click',function(){
						$.fancybox.close();	
					});
					if (isEdit) $('#textA').html("Cập nhật");
					else 
						$('#textA').html("Thêm mới");
					$('#quotaCodeF').focus();
					$('#errMsgQuotaGroupF').html('').hide();
					$(window).resize();
					$.fancybox.update();	
					
					$('.fancybox-inner #quotaPCodeF').val($('#quotaPCode').val().trim());
					$('.fancybox-inner #quotaPNameF').val($('#quotaPName').val().trim());
					$('.fancybox-inner #quotaPCodeF').attr("disabled", "disabled"); 
					$('.fancybox-inner #quotaPNameF').attr("disabled", "disabled"); 
					if (isEdit) $('.fancybox-inner #quotaCodeF').attr("disabled","disabled");
					Utils.loadAttributeData('.fancybox-inner ');
					$('.fancybox-inner .CustomStyleSelectBox').width(115);
					$('.fancybox-inner #btnAddQuotaF').bind('click',function(){
						//
						var msg = ""; var chot = 'quotaCodeF';
						msg = Utils.getMessageOfRequireCheck('quotaCodeF', 'Mã nhóm',false);
						if (msg==""){
							msg = Utils.getMessageOfRequireCheck('quotaNameF','Tên nhóm', false);
							chot = 'quotaNameF';
						}
						if (msg==""){
							msg = Utils.getMessageOfRequireCheck('fromPercentF','%Doanh số đóng góp', false);
							chot = 'fromPercentF';
						}
						if (msg!=""){
							$('.fancybox-inner #errMsgQuotaGroup').html(msg).show();
							$('.fancybox-inner #'+chot).focus();
							return false;
						}
						msg = Utils.getMessageOfSpecialCharactersValidate('quotaCodeF', 'Mã nhóm', Utils._CODE);
						if (msg != ""){
							chot = 'quotaCodeF';
						}
						if (msg ==""){
							msg = Utils.getMessageOfSpecialCharactersValidate('quotaNameF', 'Tên nhóm', Utils._NAME);
							chot = 'quotaNameF';
						}
						if (msg == ""){
							msg = Utils.getMessageOfSpecialCharactersValidateEx('fromPercentF', '%Doanh số đóng góp',Utils._TF_NUMBER_DOT);
							chot = 'fromPercentF';
						}
						if (msg!=""){
							$('.fancybox-inner #errMsgQuotaGroup').html(msg).show();
							$('.fancybox-inner #'+chot).focus();
							return false;
						}
						if (msg==""){
							if (parseInt($('.fancybox-inner #fromPercentF').val().trim()) <= 0) {
								msg = "%Doanh số đóng góp phải lớn hơn 0";
								chot = 'fromPercentF';
							}
						}
						if (msg!=""){
							$('.fancybox-inner #errMsgQuotaGroup').html(msg).show();
							$('.fancybox-inner #'+chot).focus();
							return false;
						}
						if (msg==""){
							msg = Utils.getMessageOfRequireCheck('toPercentF','%Doanh số tối đa', false);
							chot = 'toPercentF';
						}
						if (msg!=""){
							$('.fancybox-inner #errMsgQuotaGroup').html(msg).show();
							$('.fancybox-inner #'+chot).focus();
							return false;
						}
						if (msg == ""){
							msg = Utils.getMessageOfSpecialCharactersValidateEx('toPercentF', '%Doanh số tối đa',Utils._TF_NUMBER_DOT);
							chot = 'toPercentF';
						}
						if (msg!=""){
							$('.fancybox-inner #errMsgQuotaGroup').html(msg).show();
							$('.fancybox-inner #'+chot).focus();
							return false;
						}
						if (msg==""){
							if (parseInt($('.fancybox-inner #toPercentF').val().trim()) <= 0) {
								msg = "%Doanh số tối đa phải lớn hơn 0";
								chot = 'toPercentF';
							}
						}
						if (msg!=""){
							$('.fancybox-inner #errMsgQuotaGroup').html(msg).show();
							$('.fancybox-inner #'+chot).focus();
							return false;
						}
						if ( parseInt($('#fromPercentF').val(),10) > 100)
						{
							$('.fancybox-inner #errMsgQuotaGroup').html("%Doanh số đóng góp lớn 100%").show();
							$('#fromPercentF').focus();
							return false;
						}
						if (parseInt($('#fromPercentF').val(),10) > parseInt($('#toPercentF').val(),10))
						{
							$('.fancybox-inner #errMsgQuotaGroup').html("%Doanh số đóng góp lớn hơn % Doanh số tối đa").show();
							$('#fromPercentF').focus();
							return false;
						}
						if ($('#quotaStatusF').val() == -2) {
							$('.fancybox-inner #errMsgQuotaGroup').html("Mời bạn chọn trạng thái.").show();
							return false;
						}
						var dataModel = new Object();		
						dataModel.id = $('#selId').val().trim();
						dataModel.quotaCode = $('#quotaCodeF').val().trim();
						dataModel.quotaName = $('#quotaNameF').val().trim();		
						dataModel.fromPercent = $('#fromPercentF').val();
						dataModel.toPercent = $('#toPercentF').val();
						dataModel.quotaStatus = $('#quotaStatusF').val();
						dataModel.quotaId = groupId;
						if(!Utils.validateAttributeData(dataModel, '#errMsg' , '.fancybox-inner ')){
							return false;
						}
						Utils.addOrSaveRowOnGrid(dataModel, "/catalog/programme-display/save-quota-group", ProgrammeDisplayCatalog._xhrSave, 'quotaGrid','errMsgQuotaGroup', function(data){
							$('#selQuotaId').val(data.id);
							showSuccessMsg('successMsgQuotaGroup',data);
							$("#quotaGrid").trigger("reloadGrid");
							$.fancybox.close();
						},'quotaLoading');		
					});
				},
				afterClose: function(){
					var curIdFocus = $('#cur_focus').val();
					$('#'+ curIdFocus).focus();
				}
			});
		}, null, 'GET');
	},
	deleteQuotaGroupRow: function(id){
		var dataModel = new Object();
		dataModel.quotaId = id;	
		Utils.deleteSelectRowOnGrid(dataModel, 'nhóm chỉ tiêu', '/catalog/programme-display/delete-quota-group', ProgrammeDisplayCatalog._xhrDel, 'quotaGrid','errMsgQuotaGroup',function(data){
			showSuccessMsg('successMsgQuotaGroup',data);
			ProgrammeDisplayCatalog.cancelQuota();
		},'quotaLoading');
		return false;
	},
	getQuotaGroupSelectedRow: function(rowId){
		var code = $("#quotaGrid").jqGrid ('getCell', rowId, 'displayGroupCode');
		var name = $("#quotaGrid").jqGrid ('getCell', rowId, 'displayGroupName');
		var fromPercent = $("#quotaGrid").jqGrid ('getCell', rowId, 'percentMin');
		var toPercent = $("#quotaGrid").jqGrid ('getCell', rowId, 'percentMax');
		var subCatField = $("#quotaGrid").jqGrid ('getCell', rowId, 'subCatField');		
		var status = $("#quotaGrid").jqGrid ('getCell', rowId, 'status');
		var id = $("#quotaGrid").jqGrid ('getCell', rowId, 'id');
		var statusValue = -2;
		if(status == activeStatusText){
			statusValue = 1;
		} else {
			statusValue = 0;
		}
		setTextboxValue('quotaCode', code);
		setTextboxValue('quotaName', name);
		setTextboxValue('fromPercent', fromPercent);
		setTextboxValue('toPercent', toPercent);
		setSelectBoxValue('fieldMap', subCatField);
		setSelectBoxValue('quotaStatus', statusValue);
		disabled('quotaCode');
		$('#btnSearchQuota').hide();
		$('#btnAddQuota').hide();
		$('#btnEditQuota').show();
		$('#btnCancelQuota').show();
		$('#container2 .RequireStyle').show();
		$('#selQuotaId').val(id);
		$('#quotaName').focus();
		$('#titleQuota').text('Cập nhật nhóm chỉ tiêu');
	},
	cancelQuota: function(){
		$('#quotaCode').removeAttr('disabled','');
		setTextboxValue('quotaCode', '');
		setTextboxValue('quotaName', '');
		setTextboxValue('fromPercent', '');
		setTextboxValue('toPercent', '');
		setSelectBoxValue('fieldMap');
		setSelectBoxValue('quotaStatus', 1);		
		$('#btnEditQuota').hide();
		$('#btnCancelQuota').hide();
		$('#btnSearchQuota').show();
		$('#btnAddQuota').show();
		$('#container2 .RequireStyle').hide();
		$('#selQuotaId').val(0);
		$('#errMsgQuotaGroup').html('').hide();
		ProgrammeDisplayCatalog.searchQuota();
		$('#quotaCode').focus();
		$('#titleQuota').text('Thông tin tìm kiếm nhóm chỉ tiêu');
	},
	saveStaffTab: function(){
		var msg = '';
		$('#errMsgStaff').html('').hide();
		msg = Utils.getMessageOfRequireCheck('shopCode','Mã đơn vị');
		if($('#rbStaff').is(':checked')){
			if(msg.length == 0){
				msg = Utils.getMessageOfRequireCheck('staffCode','Mã nhân viên');
			}
			if(msg.length == 0){
				msg = Utils.getMessageOfSpecialCharactersValidate('staffCode','Mã nhân viên',Utils._CODE);
			}
		}
		if($('#rbStaffType').is(':checked')){
			if(msg.length == 0){
				msg = Utils.getMessageOfRequireCheck('staffType','Loại nhân viên',true);
			}
		}	
		if(msg.length > 0){
			$('#errMsgStaff').html(msg).show();
			return false;
		}
		var dataModel = new Object();		
		dataModel.id = $('#selId').val().trim();
		dataModel.staffTypeId = $('#staffType').val();
		dataModel.staffCode = $('#staffCode').val().trim();
		dataModel.shopCode = $('#shopCode').val().trim();
		Utils.addOrSaveRowOnGrid(dataModel, "/catalog/programme-display/save-staff", ProgrammeDisplayCatalog._xhrSave, 'staffGrid','errMsgStaff', function(data){	
			$("#productGrid").trigger("reloadGrid");
			$('#shopCode').val('');
			$('#shopName').val('');
			$('#staffCode').val('');
			$('#staffName').val('');
			showSuccessMsg('successMsgStaff',data);
			setSelectBoxValue('staffType');
		},'staffLoading');		
		return false;
	},
	deleteStaffRow: function(staffId){
		var dataModel = new Object();
		dataModel.id = $('#selId').val().trim();
		dataModel.staffId = staffId;	
		Utils.deleteSelectRowOnGrid(dataModel, 'nhân viên', '/catalog/programme-display/delete-staff', ProgrammeDisplayCatalog._xhrDel, 'staffGrid','errMsgStaff',function(data){
			showSuccessMsg('successMsgStaff',data);
		},'staffLoading');		
		return false;
	},
	addSubQuotaGroupRow: function(quotaGroupId,rowId, gridId){ // edit CuongND
		var html = $('#productTreeDialog').html();
		$.fancybox(html,
				{
					modal: true,
					title: 'Thêm thông tin sản phẩm',
					afterShow: function(){
						Utils.resetCheckboxStateOfTree();
						Utils.bindAutoSearch();	
						$('.fancybox-inner #treeGroup').hide();
						$('.fancybox-inner #treeQuotaGroup').hide();
						$('.fancybox-inner #btnQ').bind('click',function(){
							ProgrammeDisplayCatalog.selectListProductQuota(quotaGroupId,gridId);
						});
						$('.fancybox-inner #btnSearchProductTree').bind('click',function(){
							ProgrammeDisplayCatalog.searchProductOnTreeQuota();
						});
						$('#productTreeDialog').html('');
						$('.fancybox-inner #tmpNumDisplay').attr('id','numDisplay');
						Utils.bindFormatOnTextfield('numDisplay', Utils._TF_NUMBER);
						var displayId = $('#selId').val();
						var productCode = encodeURI('');
						var productName = encodeURI('');
						ProgrammeDisplayCatalog.loadDataForTreeWithCheckboxExDisplayGroup('/rest/catalog/display-program/productDisplayGroup/list.json?displayProgramId='+ displayId +'&productCode='+ encodeChar(productCode) +'&productName='+ encodeChar(productName) + '&nodeType=cat&catId=0&subCatId=0','productTree');
						ProgrammeDisplayCatalog._mapProduct = new Map();
						$('#productTree').bind("loaded.jstree open_node.jstree close_node.jstree", function (event, data) {
							if($('#productTreeContent').data('jsp') != undefined || $('#productTreeContent').data('jsp') != null) {
								$('#productTreeContent').data('jsp').destroy();
								setTimeout(function(){
									$('#productTreeContent').jScrollPane();
								},500);	
							} else {
								setTimeout(function(){
									$('#productTreeContent').jScrollPane();
								},500);	
							}
						});
						ProgrammeDisplayCatalog.openNodeOnTree('productTree');
					},
					afterClose: function(){
						$('#productTreeDialog').html(html);				
					}
				}
			);
		return false;
	},
	deleteSubQuotaGroupRow: function(id,gridId){
		ProgrammeDisplayCatalog._curGridId = gridId;
		var dataModel = new Object();
		dataModel.subGroupId = id;	
		Utils.deleteSelectRowOnGrid(dataModel, 'sản phẩm', '/catalog/programme-display/delete-sub-display-group', ProgrammeDisplayCatalog._xhrDel, ProgrammeDisplayCatalog._curGridId,'errMsgQuotaGroup',function(data){
			if (data.error == true)
			{
				$('#errMsgQuotaGroup').html(data.errMsg).show();
			}else{
				showSuccessMsg('successMsgQuotaGroup',data);
			}
		},'staffLoading');		
		return false;
	},
	saveCustomer: function(){
		var msg = '';
		$('#errMsg').html('').hide();
		msg = Utils.getMessageOfRequireCheck('customerCode','Mã KH');
		if(msg.length == 0){
			msg = Utils.getMessageOfSpecialCharactersValidate('customerCode','Mã KH');
		}		
		if(msg.length == 0){
			msg = Utils.getMessageOfRequireCheck('level','Mức CTTB',true);
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfRequireCheck('fDate','Từ ngày');
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfInvalidFormatDate('fDate','Từ ngày');
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfRequireCheck('tDate','Đến ngày');
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfInvalidFormatDate('tDate','Đến ngày');
		}
		var fDate = $('#fDate').val();
		var tDate = $('#tDate').val();
		if(msg.length == 0){
			var currentTime = new Date();
			var month = currentTime.getMonth() + 1;
			var day = currentTime.getDate();
			var year = currentTime.getFullYear();		
			if(!Utils.compareDate(fDate, tDate)){
				msg = msgErr_fromdate_greater_todate;				
			}
		}		
		if(msg.length > 0){
			$('#errMsg').html(msg).show();
			return false;
		}
		var dataModel = new Object();		
		dataModel.id = $('#id').val().trim();
		dataModel.displayCustomerId = $('#selId').val().trim();
		dataModel.customerCode = $('#customerCode').val().trim();
		dataModel.staffId = $('#staffId').val().trim();		
		dataModel.levelId = $('#level').val();
		dataModel.shopCode = $('#shopCode').val();
		dataModel.fromDate = $('#fDate').val().trim();
		dataModel.toDate = $('#tDate').val().trim();
		Utils.addOrSaveData(dataModel, "/catalog/programme-display/customer/save", ProgrammeDisplayCatalog._xhrSave, null, function(data){	
			$("#customerGrid").trigger("reloadGrid");
			ProgrammeDisplayCatalog.cancelCustomerForm();			
		});		
		return false;
	},
	getSelectedCustomerRow: function(rowId, level){
		$('#btnAddCustomer').hide();
		$('#btnEditCustomer').show();
		$('#btnCancelCustomer').show();
		var code = $("#customerGrid").jqGrid ('getCell', rowId, 'customer.shortCode');
		var name = $("#customerGrid").jqGrid ('getCell', rowId, 'customer.customerName');
		var fromDate = $("#customerGrid").jqGrid ('getCell', rowId, 'fromDate');
		var toDate = $("#customerGrid").jqGrid ('getCell', rowId, 'toDate');
		var id = $("#customerGrid").jqGrid ('getCell', rowId, 'id');
		$('#selId').val(id);
		setSelectBoxValue('level', level);
		setTextboxValue('customerCode', code);
		setTextboxValue('customerName', name);
		setTextboxValue('fDate', fromDate);
		setTextboxValue('tDate', toDate);		
	},
	cancelCustomerForm: function(){
		$('#selId').val(0);
		$('#btnEditCustomer').hide();
		$('#btnCancelCustomer').hide();
		$('#btnAddCustomer').show();
		setSelectBoxValue('level');
		setTextboxValue('customerCode');
		setTextboxValue('customerName');
		setTextboxValue('fDate');
		setTextboxValue('tDate');
	},
	deleteCustomerRow: function(customerId){
		var dataModel = new Object();
		dataModel.customerId = customerId;	
		Utils.deleteSelectRowOnGrid(dataModel, 'khách hàng', '/catalog/programme-display/customer/delete', ProgrammeDisplayCatalog._xhrDel, 'customerGrid','errMsgCustomer',function(){
			$("#staffGrid").dataGrid("reload");
		});		
		return false;
	},
	activeOthersTab: function(){
		$('#tabActive2').removeClass('Disable');
		$('#tabActive3').removeClass('Disable');
		$('#tabActive4').removeClass('Disable');
		$('#tabActive5').removeClass('Disable');
		$('#tabActive6').removeClass('Disable');
		$('#tabActive7').removeClass('Disable');
		$('#tab2').bind('click',ProgrammeDisplayCatalog.showQuotaGroupTab);
		$('#tab3').bind('click',ProgrammeDisplayCatalog.showLevelTab);
		$('#tab4').bind('click',ProgrammeDisplayCatalog.showProductTab);
		$('#tab5').bind('click',ProgrammeDisplayCatalog.showStaffTab);
		$('#tab6').bind('click',ProgrammeDisplayCatalog.showDisplayProgramChangeTab);
		$('#tab7').bind('click',ProgrammeDisplayCatalog.showShopTab);
		return false;
	},
	showDialogCreateProduct: function(){
		var html = $('#productTreeDialog').html();
		$.fancybox(html,
				{
					modal: true,
					title: 'Thêm thông tin sản phẩm',
					afterShow: function(){
						Utils.resetCheckboxStateOfTree();
						Utils.bindAutoSearch();	
						$('#productTreeDialog').html('');
						$('.fancybox-inner #tmpNumDisplay').attr('id','numDisplay');
						
						$('.fancybox-inner #productToDate').attr('id','dpProductToDate');
						$('.fancybox-inner #productFromDate').attr('id','dpProductFromDate');
						
						setDateTimePicker('dpProductToDate');
						setDateTimePicker('dpProductFromDate');
						
						$('.fancybox-inner #btnSearchProductTree').bind('click',function(){
							ProgrammeDisplayCatalog.searchProductOnTree();
						});
						
						Utils.bindFormatOnTextfield('numDisplay', Utils._TF_NUMBER);
						// Cuong ND edit code
						// -----------------------------------
						$('.fancybox-inner #btnQ').bind('click',function(){
							ProgrammeDisplayCatalog.selectListProduct(false,null);
						});
						// --------------------------------------------------------------
						var displayId = $('#selId').val();
						var productCode = encodeURI('');
						var productName = encodeURI('');
						ProgrammeDisplayCatalog.loadDataForTreeWithCheckboxEx('/rest/catalog/display-program/product/list.json?displayProgramId='+ displayId +'&productCode='+ encodeChar(productCode) +'&productName='+ encodeChar(productName) + '&nodeType=cat&catId=0&subCatId=0','productTree');
						ProgrammeDisplayCatalog._mapProduct = new Map();
						$('#productTree').bind("loaded.jstree open_node.jstree close_node.jstree", function (event, data) {
							if($('#productTreeContent').data('jsp') != undefined || $('#productTreeContent').data('jsp') != null) {
								$('#productTreeContent').data('jsp').destroy();
								setTimeout(function(){
									$('#productTreeContent').jScrollPane();
								},500);	
							} else {
								setTimeout(function(){
									$('#productTreeContent').jScrollPane();
								},500);	
							}
							// $('#productTreeContent
							// .jspContainer').css('width','500px');
						});
						ProgrammeDisplayCatalog.openNodeOnTree('productTree');
					},
					afterClose: function(){
						$('#productTreeDialog').html(html);				
					}
				}
			);
		return false;
	},
	showDialogCreateLevel: function(isEdit,selLevelId){
		var params = new Object();
		params.levelId = selLevelId;
		$('#selLevelId').val(selLevelId);
		Utils.getHtmlDataByAjax(params, '/catalog/programme-display/search-level-detail', function(data) {
			var html ='<div class="GeneralDialog General5Dialog"><div class="DialogAddLevelCTTB" id="contentDetailDiv"></div></div>';
			var title ='';
			if(isEdit == false){
				title = 'Thêm mức cho CTTB ';
			} else{
				title = 'Chi tiết mức CTTB ';
			}
			$.fancybox(html, {
				modal: true,
				title: title,
				autoResize: false,
				afterShow: function () {
					$('.fancybox-inner #contentDetailDiv').html(data);
					ProgrammeDisplayCatalog.onLoadAfterShowLevelDialog();
					
					$(window).resize();
					$.fancybox.update();	
				},
				afterClose: function(){
					$('#selLevelId').val('0');
				}
			});
		}, null, 'GET');
	},
	changePaidProduct: function(){
		var paidProductCode = $('#paidProductCodeLvlDlg').val();
		if(!isNullOrEmpty(paidProductCode)) {
			setSelectBoxValue('revenueBonusTypeLvlDlg',1);
			setSelectBoxValue('displayBonusTypeLvlDlg',1);
			enableSelectbox('revenueBonusTypeLvlDlg');
			enableSelectbox('displayBonusTypeLvlDlg');
		}else{
			setSelectBoxValue('revenueBonusTypeLvlDlg',0);
			setSelectBoxValue('displayBonusTypeLvlDlg',0);
			disableSelectbox('revenueBonusTypeLvlDlg');
			disableSelectbox('displayBonusTypeLvlDlg');
		}
	},
	onLoadAfterShowLevelDialog :function(){
		Utils.bindFormatOntextfieldCurrencyFor('amountLvlDlg',Utils._TF_NUMBER_COMMA);
		Utils.bindFormatOntextfieldCurrencyFor('numSKULvlDlg',Utils._TF_NUMBER_COMMA);
		Utils.bindFormatOntextfieldCurrencyFor('numProductDisplayLvlDlg',Utils._TF_NUMBER_COMMA);
		Utils.bindFormatOntextfieldCurrencyFor('percentDisCountPassLvlDlg',Utils._TF_NUMBER_COMMA);
		Utils.bindFormatOnTextfield('percentDisCountOverLvlDlg',Utils._TF_NUMBER);
		Utils.bindFormatOntextfieldCurrencyFor('revenueBonusLvlDlg',Utils._TF_NUMBER_COMMA);
		Utils.bindFormatOntextfieldCurrencyFor('maxDisCountLvlDlg',Utils._TF_NUMBER_COMMA);
		Utils.bindFormatOntextfieldCurrencyFor('displayBonusLvlDlg',Utils._TF_NUMBER_COMMA);
		Utils.bindFormatOnTextfield('percentSupportDisplayLvlDlg',Utils._TF_NUMBER);
		Utils.bindFormatOntextfieldCurrencyFor('paidPriceLvlDlg',Utils._TF_NUMBER_COMMA);
		Utils.bindFormatOntextfieldCurrencyFor('maxSupportDisplayLvlDlg',Utils._TF_NUMBER_COMMA);
		Utils.bindFormatOntextfieldCurrencyFor('displayPostion1LvlDlg',Utils._TF_NUMBER_COMMA);
		Utils.bindFormatOntextfieldCurrencyFor('displayPostion2LvlDlg',Utils._TF_NUMBER_COMMA);
		Utils.bindFormatOntextfieldCurrencyFor('displayPostion3LvlDlg',Utils._TF_NUMBER_COMMA);
		Utils.bindFormatOntextfieldCurrencyFor('displayPostion4LvlDlg',Utils._TF_NUMBER_COMMA);
		
		Utils.loadAttributeData('.fancybox-inner ','AddLevel1Form');
		var paidProductCode = $('#paidProductCodeLvlDlg').val();
		if(!isNullOrEmpty(paidProductCode)) {
			enableSelectbox('revenueBonusTypeLvlDlg');
			enableSelectbox('displayBonusTypeLvlDlg');
		}else{
			disableSelectbox('revenueBonusTypeLvlDlg');
			disableSelectbox('displayBonusTypeLvlDlg');
		}
		$('#revenueBonusTypeLvlDlg').customStyle();
		$('#displayBonusTypeLvlDlg').customStyle();
		$('#statusLvlDlg').customStyle();
	},
	searchProductOnTree: function(){	
		$('#productTree li').each(function(){
			var _id = $(this).attr('id');
			var type = $(this).attr('contentitemid');
			if($(this).hasClass('jstree-checked') && type=='product'){
				ProgrammeDisplayCatalog._mapProduct.put(_id,_id);		
			}else {
				ProgrammeDisplayCatalog._mapProduct.remove(_id);
			}
		});
		var displayId = $('#selId').val();
		if(displayId==undefined || displayId==null){
			displayId = 0;
		}
		var productCode = encodeChar($('#productCodeDlg').val().trim());
		var productName = encodeChar($('#productNameDlg').val().trim());
		$('#productTreeContent').data('jsp').destroy();
		ProgrammeDisplayCatalog.loadDataForTreeWithCheckboxEx('/rest/catalog/display-program/product/list.json?displayProgramId='+ displayId +'&productCode='+ encodeChar(productCode) +'&productName='+ encodeChar(productName) + '&nodeType=cat&catId=0&subCatId=0','productTree');
		setTimeout(function() {
			$('#productTreeContent').jScrollPane();
			$('#productTree').bind("loaded.jstree open_node.jstree close_node.jstree", function (event, data) {
				// Utils.applyCheckboxStateOfTree();
				if($('#productTreeContent').data('jsp') != undefined || $('#productTreeContent').data('jsp') != null) {
					$('#productTreeContent').data('jsp').destroy();
					setTimeout(function(){
						$('#productTreeContent').jScrollPane();
					},500);	
				} else {
					setTimeout(function(){
						$('#productTreeContent').jScrollPane();
					},500);	
				}
				ProgrammeDisplayCatalog.getCheckboxStateOfTreeEx('productTree',ProgrammeDisplayCatalog._mapProduct,"product",'productTree li');
			});
			ProgrammeDisplayCatalog.getCheckboxStateOfTreeEx('productTree',ProgrammeDisplayCatalog._mapProduct,"product",'productTree li');
		}, 500);
		
	},
	searchProductOnTreeQuota: function(){	
		$('#productTree li').each(function(){
			var _id = $(this).attr('id');
			var type = $(this).attr('contentitemid');
			if($(this).hasClass('jstree-checked') && type=='product'){
				ProgrammeDisplayCatalog._mapProduct.put(_id,_id);		
			}else {
				ProgrammeDisplayCatalog._mapProduct.remove(_id);
			}
		});
		var displayId = $('#selId').val();
		var productCode = encodeChar($('#productCodeDlg').val().trim());
		var productName = encodeChar($('#productNameDlg').val().trim());
		$('#productTreeContent').data('jsp').destroy();
		ProgrammeDisplayCatalog.loadDataForTreeWithCheckboxExDisplayGroup('/rest/catalog/display-program/productDisplayGroup/list.json?displayProgramId='+ displayId +'&productCode='+ encodeChar(productCode) +'&productName='+ encodeChar(productName) + '&nodeType=cat&catId=0&subCatId=0','productTree');
		setTimeout(function() {
			$('#productTreeContent').jScrollPane();
			$('#productTree').bind("loaded.jstree open_node.jstree close_node.jstree", function (event, data) {
				// Utils.applyCheckboxStateOfTree();
				if($('#productTreeContent').data('jsp') != undefined || $('#productTreeContent').data('jsp') != null) {
					$('#productTreeContent').data('jsp').destroy();
					setTimeout(function(){
						$('#productTreeContent').jScrollPane();
					},500);	
				} else {
					setTimeout(function(){
						$('#productTreeContent').jScrollPane();
					},500);	
				}
				ProgrammeDisplayCatalog.getCheckboxStateOfTreeEx('productTree',ProgrammeDisplayCatalog._mapProduct,"product",'productTree li');
			});
			ProgrammeDisplayCatalog.getCheckboxStateOfTreeEx('productTree',ProgrammeDisplayCatalog._mapProduct,"product",'productTree li');
		}, 500);
		
	},
	getCheckboxStateOfTreeEx:function(treeId,MAP,nodeType,selector){
		for(var i=0;i<MAP.size();++i){
			var _obj = MAP.get(MAP.keyArray[i]);					
			$('#' + selector).each(function(){
				var _id = $(this).attr('id');
				var type = $(this).attr('contentitemid');
				if(_id==_obj && nodeType==type ){
					$(this).removeClass('jstree-unchecked');
					$(this).addClass('jstree-checked');
				}
			});
		}
	},	
	selectListProduct: function(flagQouta,groupId){
		$('#errMsgProductDlg').html('').hide();
		$('#successMsgProductDlg').html('').hide();
		$.fancybox.update();
		var arrId = new Array();
		/*
		 * $('.jstree-leaf').each(function(){
		 * if($(this).hasClass('jstree-checked')){
		 * arrId.push($(this).attr('id')); } });
		 * $('li[classstyle=product]').each(function(){
		 * if($(this).hasClass('jstree-checked')){
		 * arrId.push($(this).attr('id')); } });
		 */
		$('#productTree li').each(function(){
			var _id = $(this).attr('id');
			var type = $(this).attr('contentitemid');
			if($(this).hasClass('jstree-checked') && type=='product'){
				ProgrammeDisplayCatalog._mapProduct.put(_id,_id);		
			}else{
				ProgrammeDisplayCatalog._mapProduct.remove(_id);	
			}			
		});
		for(var i=0;i<ProgrammeDisplayCatalog._mapProduct.size();++i){
			var _obj = ProgrammeDisplayCatalog._mapProduct.get(ProgrammeDisplayCatalog._mapProduct.keyArray[i]);
			arrId.push(_obj);
		}
		var msg = '';
		if(arrId.length == 0){
			msg = format(msgErr_required_choose_format,'Sản phẩm ');
		}		
		var	fDate = $('#dpProductFromDate').val().trim();
		var	tDate = $('#dpProductToDate').val().trim();
		if(msg.length == 0){
			msg = Utils.getMessageOfRequireCheck('dpProductFromDate', 'Từ ngày');
		}		
		if(msg.length==0){
			msg = Utils.getMessageOfInvalidFormatDate('dpProductFromDate', 'Từ ngày');
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfRequireCheck('dpProductToDate', 'Đến ngày');
		}
		if(msg.length==0){
			msg = Utils.getMessageOfInvalidFormatDate('dpProductToDate', 'Đến ngày');
		}
		if(msg.length==0){			
			if(!Utils.compareDate(fDate, tDate)){
				msg = msgErr_fromdate_greater_todate;	
				$('#dpProductFromDate').focus();
			}
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfRequireCheck('numDisplay', 'Số lượng trưng bày');
		}
		if(msg.length == 0 && (isNaN($('#numDisplay').val().trim()) || parseInt($('#numDisplay').val().trim()) <=0)){
			msg = format(msgErr_possitive_integer,'Số lượng trưng bày');
			$('#numDisplay').focus();
		}
		if(msg.length > 0){
			$('#errMsgProductDlg').html(msg).show();
			$.fancybox.update();
			return false;
		}
		var moreCat = 0;		
		$('.jstree-no-icons').children().each(function(){
			if($(this).hasClass('jstree-checked') || $(this).hasClass('jstree-undetermined')){
				moreCat++;
			}
		});
		var num = $('#numDisplay').val().trim();
		if(num.length>8){
			$('#errMsgProductDlg').html('Giá trị số lượng quá lớn').show();
			return false;
		}
		var displayId = $('#selId').val();
		var dataModel = new Object();		
		dataModel.lstProductId = arrId;
		dataModel.numDisplay = num;
		dataModel.id = displayId;
		dataModel.fromDate = fDate;
		dataModel.toDate = tDate;
		if(moreCat>1){
			$.messager.confirm('Xác nhận', 'Bạn đang chọn các sản phẩm hơn 1 ngành hàng. Bạn có muốn lưu thông tin không?', function(r){
				if (r){
					Utils.saveData(dataModel, '/catalog/programme-display/product/save', ProgrammeDisplayCatalog._xhrSave, 'errMsgProductDlg', function(data){
						$('#successMsgProductDlg').html('Lưu dữ liệu thành công').show();
						$('#productCodeDlg').val('');
						$('#productNameDlg').val('');
						$('#numDisplay').val('');			
						Utils.resetCheckboxStateOfTree();
						ProgrammeDisplayCatalog.searchProductOnTree();
						$('#productCode').val('');
						$('#productName').val('');
						$('#quantity').val('');
						ProgrammeDisplayCatalog.searchProduct();
						$('#quotaGrid').collapseSubGridRow(ProgrammeDisplayCatalog._curRowId);
						$('#quotaGrid').expandSubGridRow(ProgrammeDisplayCatalog._curRowId);
						$.fancybox.update();
						if (flagQouta==false) setTimeout(function(){$.fancybox.close();}, 1000);
						$("#productGrid").trigger("reloadGrid");
					}, null, '.fancybox-inner');					
				}
			});
		} else {
			Utils.addOrSaveData(dataModel, '/catalog/programme-display/product/save', ProgrammeDisplayCatalog._xhrSave, 'errMsgProductDlg', function(data){
				$('#successMsgProductDlg').html('Lưu dữ liệu thành công').show();
				$('#productCodeDlg').val('');
				$('#productNameDlg').val('');
				$('#numDisplay').val('');
				Utils.resetCheckboxStateOfTree();
				ProgrammeDisplayCatalog.searchProductOnTree();
				$('#productCode').val('');
				$('#productName').val('');
				$('#quantity').val('');
				ProgrammeDisplayCatalog.searchProduct();
				$('#quotaGrid').collapseSubGridRow(ProgrammeDisplayCatalog._curRowId);
				$('#quotaGrid').expandSubGridRow(ProgrammeDisplayCatalog._curRowId);
				$.fancybox.update();
				if (flagQouta==false) setTimeout(function(){$.fancybox.close();}, 1000);
				$("#productGrid").trigger("reloadGrid");
			}, null, '.fancybox-inner', null, null);
		}
		return false;
	},
	showUpdateProductDialog: function(rowId){
		var html = $('#productDetailTreeDialog').html();
		$.fancybox(html,
				{
					modal: true,
					title: 'Cập nhật thông tin sản phẩm',					
					afterShow: function(){
						$('#productDetailTreeDialog').html('');
						$('#errMsgProductDetailDlg').html('').hide();
						$('#successMsgProductDetailDlg').html('').hide();
						Utils.bindAutoSearch();													
						$('.fancybox-inner #tmpNumDisplayDetail').attr('id','numDisplay');
						Utils.bindFormatOnTextfield('numDisplay', Utils._TF_NUMBER);
						$('#numDisplay').focus();
						var name = $("#productGrid").jqGrid ('getCell', rowId, 'product.productName');
						var id = $("#productGrid").jqGrid ('getCell', rowId, 'product.id');						
						var quantity = $("#productGrid").jqGrid ('getCell', rowId, 'quantity');	
						var fromDate = $("#productGrid").jqGrid ('getCell', rowId, 'fromDate');	
						var toDate = $("#productGrid").jqGrid ('getCell', rowId, 'toDate');
						var detailId = $("#productGrid").jqGrid ('getCell', rowId, 'id');
						$('#productNameDetailDlg').val(name);
						$('#productIdDetailDlg').val(id);
						$('#numDisplay').val(quantity);
						
						$('.fancybox-inner #TmproductDetailFromDate').attr('id','productDetailFromDate');
						$('.fancybox-inner #TmproductDetailToDate').attr('id','productDetailToDate');
						$('.fancybox-inner #productDetailFromDate').val(fromDate);
						$('.fancybox-inner #productDetailToDate').val(toDate);
						$('.fancybox-inner #displayProgramDetailId').val(detailId);
						setDateTimePicker('productDetailFromDate');
						setDateTimePicker('productDetailToDate');
					},
					afterClose: function(){
						$('#productDetailTreeDialog').html(html);						
					}
				}
			);
		return false;
	},
	saveDetailProduct: function(){
		$('#errMsgProductDetailDlg').html('').hide();
		$('#successMsgProductDetailDlg').html('').hide();
		$.fancybox.update();
		var msg = '';		
		msg = Utils.getMessageOfRequireCheck('numDisplay', 'Số lượng trưng bày');		
		if(msg.length == 0 && (isNaN($('#numDisplay').val().trim()) || parseInt($('#numDisplay').val().trim()) <=0)){
			msg = format(msgErr_possitive_integer,'Số lượng trưng bày');
			$('#numDisplay').focus();
		}	
		var fromDate = $('#productDetailFromDate').val().trim();
		var toDate = $('#productDetailToDate').val().trim(); 		
		
		if(msg.length == 0){
			msg = Utils.getMessageOfRequireCheck('productDetailFromDate', 'Từ ngày');
		}		
		if(msg.length==0){
			msg = Utils.getMessageOfInvalidFormatDate('productDetailFromDate', 'Từ ngày');
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfRequireCheck('productDetailToDate', 'Đến ngày');
		}
		if(msg.length==0){
			msg = Utils.getMessageOfInvalidFormatDate('productDetailToDate', 'Đến ngày');
		}
		if(msg.length==0){			
			if(!Utils.compareDate(fromDate, toDate)){
				msg = msgErr_fromdate_greater_todate;	
				$('#productDetailFromDate').focus();
			}
		}
		if(msg.length > 0){
			$('#errMsgProductDetailDlg').html(msg).show();
			$.fancybox.update();
			return false;
		}
		var num = $('#numDisplay').val().trim();
		if(num.length>8){
			$('#errMsgProductDetailDlg').html('Giá trị số lượng quá lớn ').show();
			return false;
		}
		var displayId = $('#selId').val();
		var arrId = new Array();
		arrId.push($('#productIdDetailDlg').val());
		var dataModel = new Object();
		dataModel.lstProductId = arrId;
		dataModel.numDisplay = num;
		dataModel.id = displayId;
		
		dataModel.fromDate = fromDate;
		dataModel.toDate = toDate;
		dataModel.detailId = $('#displayProgramDetailId').val().trim();
		Utils.addOrSaveData(dataModel, '/catalog/programme-display/product/save', ProgrammeDisplayCatalog._xhrSave, 'errMsgProductDetailDlg', function(data){			
			$('#successMsgProductDetailDlg').html('Lưu dữ liệu thành công').show();
			$.fancybox.update();
			$('#productCode').val('');
			$('#productName').val('');
			$('#quantity').val('');
			ProgrammeDisplayCatalog.searchProduct();
			setTimeout(function(){$.fancybox.close();}, 1000);
		}, null, '.fancybox-inner', null, null);
		return false;
	},
	showDialogCreateStaff: function(){
		var html = $('#staffTreeDialog').html();
		$.fancybox(html,
				{
					modal: true,
					title: 'Thêm nhân viên phát triển khách hàng',
					afterShow: function(){
						Utils.resetCheckboxStateOfTree();
						Utils.bindAutoSearch();	
						$('#staffTreeDialog').html('');
						$('#productCodeDlg').focus();
						$('#staffCodeDlg').focus();
						var displayId = $('#selId').val();						
						if(displayId==undefined || displayId==null ){
							displayId = 0;
						}
						var staffCode = encodeURI('');
						var staffName = encodeURI('');
						ProgrammeDisplayCatalog.loadDataForTreeWithCheckboxEx('/rest/catalog/display-program/staff/list.json?displayProgramId='+ displayId +'&staffCode='+ encodeChar(staffCode) +'&staffName='+ encodeChar(staffName),'staffTree', true);
						$('#staffTree').bind("loaded.jstree open_node.jstree close_node.jstree", function (event, data) {
							var tm = setTimeout(function() {
								$('#staffTreeContent').jScrollPane();
							}, 500);							
						});
						ProgrammeDisplayCatalog.openNodeOnTree('staffTree');
						ProgrammeDisplayCatalog._mapStaff = new Map();						
						 // $(window).resize();
						  var tm = setTimeout(function(){
							  $('.fancybox-wrap.fancybox-desktop.fancybox-type-html.fancybox-opened').css('center',document.documentElement.scrollTop +  $(window).height()/2);
							  window.scrollTo(0,0);
						  }, 500);
					},					
					afterClose: function(){
						$('#staffTreeDialog').html(html);						
					}
				}
			);
		return false;
	},
	searchStaffOnTree: function(){
		$('#staffTree .jstree-checked').each(function(){
			var _id = $(this).attr('id');
			var type = $(this).attr('contentitemid');
			if(type==2){
				ProgrammeDisplayCatalog._mapStaff.put(_id,_id);	
			}else{				
				ProgrammeDisplayCatalog._mapStaff.remove(_id);		
			}
		});
		var displayId = $('#selId').val();
		var staffCode = encodeChar($('#staffCodeDlg').val().trim());
		var staffName = encodeChar($('#staffNameDlg').val().trim());
		ProgrammeDisplayCatalog.loadDataForTreeWithCheckboxEx('/rest/catalog/display-program/staff/list.json?displayProgramId='+ displayId +'&staffCode='+ encodeChar(staffCode) +'&staffName='+ encodeChar(staffName),'staffTree', true);
		
		setTimeout(function() {	
			$('#staffTreeContent').jScrollPane();
			$('#staffTree').bind("loaded.jstree open_node.jstree close_node.jstree", function (event, data) {
				if($('#staffTreeContent').data('jsp') != undefined || $('#staffTreeContent').data('jsp') != null) {
					$('#staffTreeContent').data('jsp').destroy();
					setTimeout(function(){
						$('#staffTreeContent').jScrollPane();
					},500);	
				} else {
					setTimeout(function(){
						$('#staffTreeContent').jScrollPane();
					},500);	
				}
				ProgrammeDisplayCatalog.getCheckboxStateOfTreeEx('staffTree',ProgrammeDisplayCatalog._mapStaff,2,'staffTree li');
			});
			ProgrammeDisplayCatalog.getCheckboxStateOfTreeEx('staffTree',ProgrammeDisplayCatalog._mapStaff,2,'staffTree li');
			$('#staffTreeContent .jspPane').css('width','486px');
		}, 500);
		ProgrammeDisplayCatalog.openNodeOnTree('staffTree');		
	},
	selectListStaff: function(){
		$('#errMsgStaffDlg').html('').hide();
		$('#successMsgStaffDlg').html('').hide();
		$.fancybox.update();
		var arrId = new Array();
		$('#staffTree li').each(function(){
			var _id = $(this).attr('id');
			var type = $(this).attr('contentitemid');
			if($(this).hasClass('jstree-checked') && type==2){
				ProgrammeDisplayCatalog._mapStaff.put(_id,_id);		
			}else{
				ProgrammeDisplayCatalog._mapStaff.remove(_id);	
			}
		});
		for(var i=0;i<ProgrammeDisplayCatalog._mapStaff.size();++i){
			var _obj = ProgrammeDisplayCatalog._mapStaff.get(ProgrammeDisplayCatalog._mapStaff.keyArray[i]);
			arrId.push(_obj);
		}
		
		var msg = '';
		if(arrId.length == 0){
			msg = format(msgErr_required_choose_format,'Nhân viên theo cây đơn vị');
		}		
		if(msg.length > 0){
			$('#errMsgStaffDlg').html(msg).show();
			$.fancybox.update();
			return false;
		}		
		var displayId = $('#selId').val();
		var dataModel = new Object();		
		dataModel.lstStaffId = arrId;
		// dataModel.lstStaffId = ProgrammeDisplayCatalog._arrListStaffItem;
		dataModel.id = displayId;
		Utils.addOrSaveData(dataModel, '/catalog/programme-display/staff/save', ProgrammeDisplayCatalog._xhrSave, 'errMsgStaffDlg', function(data){
			$('#successMsgStaffDlg').html('Lưu dữ liệu thành công').show();
			$('#staffCodeDlg').val('');
			$('#staffNameDlg').val('');		
			Utils.resetCheckboxStateOfTree();
			ProgrammeDisplayCatalog._arrListStaffItem = new Array();
			ProgrammeDisplayCatalog.searchStaffOnTree();		
			$('#staffCode').val('');
			$('#staffName').val('');
			ProgrammeDisplayCatalog.searchStaff();
			$.fancybox.update();			
			setTimeout(function(){$.fancybox.close();}, 1000);
		}, null, '.fancybox-inner', null, null);
		return false;
	},
	showDialogCreateCustomer: function(){
		var html = $('#customerTreeDialog').html();
		$.fancybox(html,
				{
					modal: true,
					title: 'Thêm khách hàng',
					afterShow: function(){
						Utils.resetCheckboxStateOfTree();
						Utils.bindAutoSearch();	
						$('#customerTreeDialog').html('');
						$('.fancybox-inner #fromDateCusDialogTmp').attr('id','fromDateCusDialog');
						$('.fancybox-inner #toDateCusDialogTmp').attr('id','toDateCusDialog');
						setDateTimePicker('fromDateCusDialog');
						setDateTimePicker('toDateCusDialog');
						$('#customerCodeDlg').focus();
						$('.fancybox-inner #levelProgramDisplayDialogTmp').attr('id','levelProgramDisplayDialog');
						$('#levelProgramDisplayDialog').addClass('MySelectBoxClass');
						$('#levelProgramDisplayDialog').customStyle();						
						var displayId = $('#selId').val();
						$.getJSON('/rest/catalog/display-program/'+ displayId +'/level/list.json', function(data){
							var arrHtml = new Array();
							arrHtml.push('<option value="-2">--- Chọn mức CTTB ---</option>');
							for(var i=0;i<data.length;i++){
								arrHtml.push('<option value="'+ data[i].value +'">'+ Utils.XSSEncode(data[i].name) +'</option>');
							}							
							$('#levelProgramDisplayDialog').html(arrHtml.join(""));							
							$('#levelProgramDisplayDialog').change();
						});
						var staffMapId = $('#selStaffId').val();
						var customerCode = encodeURI('');
						var customerName = encodeURI('');
						var shopCode = encodeChar($('#vnmShopCode').val().trim().trim());
						var URL = '/rest/catalog/display-program/customer/list.json?staffMapId='+ staffMapId +'&customerCode='+ encodeChar(customerCode) +'&customerName='+ encodeChar(customerName)+'&shopCode='+encodeChar(shopCode);
						ProgrammeDisplayCatalog.loadDataForTreeWithCheckbox(URL,'customerTree');
						$('#customerTree').bind("loaded.jstree open_node.jstree close_node.jstree", function (event, data) {							
							var tm = setTimeout(function() {
								$('#customerTreeContent').jScrollPane();
							}, 500);
							
						});		
						ProgrammeDisplayCatalog._mapCustomer = new Map();
						ProgrammeDisplayCatalog.openNodeOnTree('customerTree');							
						var tm = setTimeout(function(){
							$('.fancybox-wrap.fancybox-desktop.fancybox-type-html.fancybox-opened').css('center',document.documentElement.scrollTop +  $(window).height()/2);
							window.scrollTo(0,0);
						}, 500);

					},
					afterClose: function(){
						$('#customerTreeDialog').html(html);						
					}
				}
			);
		return false;
	},
	openNodeOnTree:function(treeId){
		$('#' + treeId).bind("open_node.jstree", function (event, data) {
			var parentId = data.inst._get_node(data.rslt.obj);
			var parentNode = parentId.attr('id');
			var child = data.inst._get_children(data.rslt.obj);
			var childNode = child.attr('id');
			if(childNode==parentNode){     
			   data.inst.delete_node(child,true);
		   }
		});	
	},
	viewCustomerByStaff: function(staffId,rowId){
		$('#selStaffId').val(staffId);				
		var name = $("#staffGrid").jqGrid ('getCell', rowId, 'staff.staffName');	
		var shopCode = $("#staffGrid").jqGrid ('getCell', rowId, 'staff.shop.shopCode');
		$('#vnmShopCode').val(shopCode);
		$('#titleCustomer').text('Thông tin về khách hàng tham gia CTTB do NV ' + Utils.XSSEncode(name) + ' phát triển');
		$('#customerContainer').show();	
		var displayId = $('#selId').val();
		$.getJSON('/rest/catalog/display-program/'+ displayId +'/level/list.json', function(data){
			var arrHtml = new Array();
			arrHtml.push('<option value="-2">--- Chọn mức CTTB ---</option>');
			for(var i=0;i<data.length;i++){
				arrHtml.push('<option value="'+ data[i].value +'">'+ Utils.XSSEncode(data[i].name) +'</option>');
			}							
			$('#levelProgramDisplay').html(arrHtml.join(""));			
			$('#levelProgramDisplay').change();
		});
		$("#customerGrid").jqGrid('GridUnload');
		$("#customerGrid").jqGrid({
			url: ProgrammeDisplayCatalog.getCustomerGridUrl('','','','','-2'),
		  colModel:[		
		    {name:'displayStaffMap.staff.shop.shopCode',index: 'shopCode', label: 'Mã đơn vị', width: 80, sortable:false,resizable:false , align: 'left', formatter: function(value, row, index) {
				return Utils.XSSEncode(value);
			}},		    
		    {name:'displayProgramLevel.levelCode',index: 'levelCode', label: 'Mức CT', width: 80, sortable:false,resizable:false , align: 'left', formatter: function(value, row, index) {
				return Utils.XSSEncode(value);
			}},	    
		    {name:'customer.shortCode',index: 'shortCode', label: 'Mã KH', width: 80, sortable:false,resizable:false , align: 'left', formatter: function(value, row, index) {
				return Utils.XSSEncode(value);
			}},
		    {name:'customer.customerName',index: 'customer.customerName', label: 'Tên KH', sortable:false,resizable:false, align: 'left' , formatter: function(value, row, index) {
				return Utils.XSSEncode(value);
			}},
		    {name:'fromDate',index: 'fromDate', label: 'Từ ngày', width: 80, sortable:false,resizable:false,align:'center', formatter:'date', formatoptions: {srcformat:'Y-m-d', newformat:'d/m/Y'} },
		    {name:'toDate',index: 'toDate', label: 'Đến ngày', width: 80, sortable:false,resizable:false,align:'center', formatter:'date', formatoptions: {srcformat:'Y-m-d', newformat:'d/m/Y'} },
		    {name:'edit', label: 'Sửa', width: 50, align: 'center',sortable:false,resizable:false, formatter: ProgrammeDisplayCatalogFormatter.editCellIconCustomerFormatter},
		    {name:'delete', label: 'Xóa', width: 50, align: 'center',sortable:false,resizable:false, formatter: ProgrammeDisplayCatalogFormatter.deleteCellIconCustomerFormatter},
		    {name:'id', index : 'id', hidden: true },
		    {name:'customer.id', index : 'customer.id', hidden: true },
		    {name:'displayProgramLevel.id', index : 'displayProgramLevel.id', hidden: true },
		  ],	  
		  pager : '#customerPager',	   
		  width: ($('#customerGridContainer').width()),
		  gridComplete: function(){
			  $('#jqgh_customerGrid_rn').html('STT');
			  updateRownumWidthForJqGrid();
		  }
		})
		.navGrid('#customerPager', {edit:false,add:false,del:false, search: false});
	},
	searchCustomer: function(){
		var customerCode = $('#customerCode').val().trim();
		var customerName = $('#customerName').val().trim();
		var fromDate = $('#fromDateCus').val().trim();
		var toDate = $('#toDateCus').val().trim();
		var level = $('#levelProgramDisplay').val().trim();
		var url = ProgrammeDisplayCatalog.getCustomerGridUrl(customerCode, customerName, fromDate, toDate, level);
		$("#customerGrid").setGridParam({url:url,page:1}).trigger("reloadGrid");
		$('#customerCode').focus();
	},	
	getCheckboxStateOfTree:function(treeId,MAP,nodeType){
		for(var i=0;i<MAP.size();++i){
			var _obj = MAP.get(MAP.keyArray[i]);					
			$('.jstree-leaf').each(function(){
				var _id = $(this).attr('id');
				var type = $(this).attr('contentitemid');
				if(_id==_obj && nodeType==type ){
					$(this).removeClass('jstree-unchecked');
					$(this).addClass('jstree-checked');
				}
			});
		}
	},	
	searchCustomerOnTree: function(){
		$('.jstree-leaf').each(function(){
			var _id = $(this).attr('id');
			var type = $(this).attr('contentitemid');
			if($(this).hasClass('jstree-checked') && type==3){
				ProgrammeDisplayCatalog._mapCustomer.put(_id,_id);		
			}
			if($(this).hasClass('jstree-unchecked')){				
				ProgrammeDisplayCatalog._mapCustomer.remove(_id);	
			}
		});
		var staffMapId = $('#selStaffId').val();
		var customerCode = encodeChar($('#customerCodeDlg').val().trim());
		var customerName = encodeChar($('#customerNameDlg').val().trim());
		var shopCode = encodeChar($('#vnmShopCode').val().trim().trim());
		ProgrammeDisplayCatalog.loadDataForTreeWithCheckbox('/rest/catalog/display-program/customer/list.json?staffMapId='+ staffMapId +'&customerCode='+ encodeChar(customerCode) +'&customerName='+ encodeChar(customerName) +'&shopCode='+encodeChar(shopCode),'customerTree');
		
		setTimeout(function() {				
			$('#customerTreeContent').jScrollPane();
			$('#customerTree').bind("loaded.jstree open_node.jstree close_node.jstree", function (event, data) {
				if($('#customerTreeContent').data('jsp') != undefined || $('#customerTreeContent').data('jsp') != null) {
					$('#customerTreeContent').data('jsp').destroy();
					setTimeout(function(){
						$('#customerTreeContent').jScrollPane();
					},500);	
				} else {
					setTimeout(function(){
						$('#customerTreeContent').jScrollPane();
					},500);	
				}
				ProgrammeDisplayCatalog.getCheckboxStateOfTreeEx('customerTree',ProgrammeDisplayCatalog._mapCustomer,3,'customerTree li');
			});
			ProgrammeDisplayCatalog.getCheckboxStateOfTreeEx('customerTree',ProgrammeDisplayCatalog._mapCustomer,3,'customerTree li');
			$('#customerTreeContent .jspPane').css('width','486px');
		}, 500);
		ProgrammeDisplayCatalog.openNodeOnTree('customerTree');		
	},	
	selectListCustomer: function(){
		$('#errMsgCustomerDlg').html('').hide();
		$('#successMsgCustomerDlg').html('').hide();
		$.fancybox.update();
		var arrId = new Array();
		$('#customerTree li').each(function(){
			var _id = $(this).attr('id');
			var type = $(this).attr('contentitemid');
			if($(this).hasClass('jstree-checked') && type==3){
				ProgrammeDisplayCatalog._mapCustomer.put(_id,_id);		
			}else {
				ProgrammeDisplayCatalog._mapCustomer.remove(_id);
			}
		});
		for(var i=0;i<ProgrammeDisplayCatalog._mapCustomer.size();++i){
			var _obj = ProgrammeDisplayCatalog._mapCustomer.get(ProgrammeDisplayCatalog._mapCustomer.keyArray[i]);
			arrId.push(_obj);
		}		
		var msg = '';
		if(arrId.length == 0){
			msg = format(msgErr_required_choose_format,'Khách hàng theo cây đơn vị');
		}		
		if(msg.length == 0){
			msg = Utils.getMessageOfRequireCheck('fromDateCusDialog','Từ ngày');
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfInvalidFormatDate('fromDateCusDialog','Từ ngày');
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfRequireCheck('toDateCusDialog','Đến ngày');
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfInvalidFormatDate('toDateCusDialog','Đến ngày');
		}		
		var fDate = $('#fromDateCusDialog').val();
		var tDate = $('#toDateCusDialog').val();
		if(msg.length == 0){
			var currentTime = new Date();
			var month = currentTime.getMonth() + 1;
			var day = currentTime.getDate();
			var year = currentTime.getFullYear();		
			if(!Utils.compareDate(fDate, tDate)){
				msg = msgErr_fromdate_greater_todate;				
			}
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfRequireCheck('levelProgramDisplayDialog','Mức CTTB',true);
		}
		if(msg.length > 0){
			$('#errMsgCustomerDlg').html(msg).show();
			$.fancybox.update();
			return false;
		}		
		var displayId = $('#selId').val();
		var dataModel = new Object();
		dataModel.lstCustomerId = arrId;		
		dataModel.staffId = $('#selStaffId').val().trim();
		dataModel.fromDate = $('#fromDateCusDialog').val().trim();
		dataModel.toDate = $('#toDateCusDialog').val().trim();
		dataModel.levelId = $('#levelProgramDisplayDialog').val().trim();
		Utils.addOrSaveData(dataModel, '/catalog/programme-display/customer/save', ProgrammeDisplayCatalog._xhrSave, 'errMsgCustomerDlg', function(data){
			$('#successMsgCustomerDlg').html('Lưu dữ liệu thành công').show();
			$('#customerCodeDlg').val('');
			$('#customerNameDlg').val('');	
			Utils.resetCheckboxStateOfTree();
			ProgrammeDisplayCatalog.searchCustomerOnTree();			
			$.fancybox.update();
			
			var url = ProgrammeDisplayCatalog.getCustomerGridUrl('', '', '', '', '');
			$("#customerGrid").setGridParam({url:url,page:1}).trigger("reloadGrid");			
			
			$("#staffGrid").trigger("reloadGrid");
			setTimeout(function(){$.fancybox.close();}, 1000);
		}, null, '.fancybox-inner', null, null);
		return false;
	},
	showUpdateCustomerDetailDialog: function(rowId){
		var html = $('#customerDetailTreeDialog').html();
		$.fancybox(html,
				{
					modal: true,
					title: 'Cập nhật thông tin khách hàng',					
					afterShow: function(){
						$('#customerDetailTreeDialog').html('');
						$('#errMsgCustomerDetailDlg').html('').hide();
						$('#successMsgCustomerDetailDlg').html('').hide();	
						$('#successMsgCustomerDlg').html('').hide();
						
						var name = $("#customerGrid").jqGrid ('getCell', rowId, 'customer.customerName');
					    var customerId = $("#customerGrid").jqGrid ('getCell', rowId, 'customer.id');
					    var fromDate = $("#customerGrid").jqGrid ('getCell', rowId, 'fromDate');
					    var toDate = $("#customerGrid").jqGrid ('getCell', rowId, 'toDate');
						var levelId = $("#customerGrid").jqGrid ('getCell', rowId, 'displayProgramLevel.id');						
						var displayCustomerMapId = $("#customerGrid").jqGrid ('getCell', rowId, 'id');
						$('#displayCustomerMapIdTmp').val(displayCustomerMapId);
						$('#customerNameDetailDlg').val(name);
						$('.fancybox-inner #levelProgramDisplayDetailDialog').attr('id','levelProgramDisplayDetail');
						$('#levelProgramDisplayDetail').val(levelId);
						$('#levelProgramDisplayDetail').addClass('MySelectBoxClass');
						$('#levelProgramDisplayDetail').customStyle();
						var displayId = $('#selId').val();
						$.getJSON('/rest/catalog/display-program/'+ displayId +'/level/list.json', function(data){
							var arrHtml = new Array();
							arrHtml.push('<option value="-2">--- Chọn mức CTTB ---</option>');
							for(var i=0;i<data.length;i++){
								arrHtml.push('<option value="'+ data[i].value +'">'+ data[i].name +'</option>');
							}							
							$('#levelProgramDisplayDetail').html(arrHtml.join(""));
							$('#levelProgramDisplayDetail').val(levelId);
							$('#levelProgramDisplayDetail').change();
						});
						$('.fancybox-inner #fromDateCusDetailDialogTmp').attr('id','fromDateCusDetailDialog');
						$('.fancybox-inner #toDateCusDetailDialogTmp').attr('id','toDateCusDetailDialog');
						setDateTimePicker('fromDateCusDetailDialog');
						setDateTimePicker('toDateCusDetailDialog');
						$('#fromDateCusDetailDialog').focus();
						$('#fromDateCusDetailDialog').val(fromDate);
						$('#toDateCusDetailDialog').val(toDate);
						$('#customerIdDetailDlg').val(customerId);
					},
					afterClose: function(){
						$('#customerDetailTreeDialog').html(html);						
					}
				}
			);
		return false;
	},
	saveDetailCustomer: function(){
		$('#errMsgCustomerDetailDlg').html('').hide();
		$('#successMsgCustomerDetailDlg').html('').hide();
		$.fancybox.update();
		var msg = '';
		msg = Utils.getMessageOfRequireCheck('fromDateCusDetailDialog','Từ ngày');		
		if(msg.length == 0){
			msg = Utils.getMessageOfInvalidFormatDate('fromDateCusDetailDialog','Từ ngày');
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfRequireCheck('toDateCusDetailDialog','Đến ngày');
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfInvalidFormatDate('toDateCusDetailDialog','Đến ngày');
		}		
		var fDate = $('#fromDateCusDetailDialog').val();
		var tDate = $('#toDateCusDetailDialog').val();
		var displayCustomerMapId = $('#displayCustomerMapIdTmp').val().trim();
		if(msg.length == 0){
			var currentTime = new Date();
			var month = currentTime.getMonth() + 1;
			var day = currentTime.getDate();
			var year = currentTime.getFullYear();		
			if(!Utils.compareDate(fDate, tDate)){
				msg = msgErr_fromdate_greater_todate;				
			}
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfRequireCheck('levelProgramDisplayDetail','Mức CTTB',true);
		}
		if(msg.length > 0){
			$('#errMsgCustomerDetailDlg').html(msg).show();
			$.fancybox.update();
			return false;
		}		
		var displayId = $('#selId').val();
		var arrId = new Array();
		arrId.push($('#customerIdDetailDlg').val());
		var dataModel = new Object();
		dataModel.lstCustomerId = arrId;		
		dataModel.staffId = $('#selStaffId').val().trim();
		dataModel.fromDate = $('#fromDateCusDetailDialog').val().trim();
		dataModel.toDate = $('#toDateCusDetailDialog').val().trim();
		dataModel.levelId = $('#levelProgramDisplayDetail').val().trim();
		dataModel.numUpdate = 0;
		dataModel.displayCustomerMapId = displayCustomerMapId;
		Utils.addOrSaveData(dataModel, '/catalog/programme-display/customer/save', ProgrammeDisplayCatalog._xhrSave, 'errMsgCustomerDetailDlg', function(data){
			$('#successMsgCustomerDetailDlg').html('Lưu dữ liệu thành công').show();			
			$.fancybox.update();
			$("#customerGrid").trigger("reloadGrid");
			setTimeout(function(){$.fancybox.close();}, 1500);
		}, null, '.fancybox-inner', null, null);
		return false;
	},
	selectSubCat:function(obj,id){
		if(obj.checked){
			ProgrammeDisplayCatalog._mapCheckSubCat.put(id, $(obj).attr('value'));
		}else{
			ProgrammeDisplayCatalog._mapCheckSubCat.remove(id);
		}
	},
	loadDataForTreeWithCheckbox:function(url,treeId){
		var tId = 'tree';
		if(treeId!= null && treeId!= undefined){
			tId = treeId;
		}
		$('#' + tId).jstree({
	        "plugins": ["themes", "json_data","ui","checkbox"],
	        "themes": {
	            "theme": "classic",
	            "icons": false,
	            "dots": true
	        },
	        "json_data": {
	        	"ajax" : {
	        		"method": "post",
	                "url" : url,
	                "data" : function (n) {
	                        return { id : n.attr ? n.attr("id") : 0 };
	                    }
	            },
	            'complete':function(){
	            	$('#'+treeId).jScrollPane();	            	
	            }
	        }
		});	
	},
	loadDataForTreeWithCheckboxEx:function(url,treeId, forNvptkh){
		var tId = 'tree';
		if(treeId!= null && treeId!= undefined){
			tId = treeId;
		}
		$('#' + tId).jstree({
	        "plugins": ["themes", "json_data","ui","checkbox"],
	        "themes": {
	            "theme": "classic",
	            "icons": false,
	            "dots": true
	        },
	        "json_data": {
	        	"ajax" : {
	        		"method": "get",
	                "url" : function( node ){
	                      if(forNvptkh != undefined || forNvptkh ==true) {
	                    	  if(node == -1) {
	                    		  return url;
	                    	  } else {
	                    		var displayId = $('#selId').val();
	                    		if(displayId==undefined || displayId==null){
	                    			  displayId = 0;
	                    		  }
	                    		var staffCode = encodeURI('');
	      						var staffName = encodeURI('');
	      						return '/rest/catalog/display-program/staff/list.json?displayProgramId='+ displayId +'&staffCode='+ encodeChar(staffCode) +'&staffName='+ encodeChar(staffName);
	                    	  }
	                      } else {
	                    	  if(node == -1) {
		                    	  return url;
		                      } else {
		                    	  var nodeType = node.attr("classStyle");
		                    	  var catId = 0;
		                    	  var subCatId = 0;
		                    	  if(nodeType == 'cat') {
		                    		  catId = node.attr("id");
		                    		  var displayId = $('#selId').val();
		                    		  if(displayId==undefined || displayId==null){
		                    			  displayId = 0;
		                    		  }
		                    		  var productCode = encodeURI('');
		                    		  var productName = encodeURI('');	
		                    		  return '/rest/catalog/display-program/product/list.json?displayProgramId='+ displayId +'&productCode='+ encodeChar(productCode) +'&productName='+ encodeChar(productName) + '&nodeType=sub-cat&catId=' +catId+ '&subCatId=0';
		                    	  } else if(nodeType == 'sub-cat') {
		                    		  subCatId = node.attr("id");
		                    		  catId = $('#'+subCatId).parent().parent().attr('id');
		                    		  var displayId = $('#selId').val();
		                    		  if(displayId==undefined || displayId==null){
		                    			  displayId = 0;
		                    		  }
		                    		  var productCode = encodeURI('');
		                    		  var productName = encodeURI('');	
		                    		  return '/rest/catalog/display-program/product/list.json?displayProgramId='+ displayId +'&productCode='+ encodeChar(productCode) +'&productName='+ encodeChar(productName) + '&nodeType=product&catId=' +catId+ '&subCatId=' + subCatId;
		                    	  } else if(nodeType == 'product') {
		                    		  return '';
		                    	  }
		                      }
	                      }
	                    },
	                "data" : function (n) {
	                        return { id : n.attr ? n.attr("id") : 0 };
	                    }
	            }
	        }
		});	
	},
	loadDataForTreeWithCheckboxExCRM_SKU:function(url,treeId, forNvptkh){
		var tId = 'tree';
		if(treeId!= null && treeId!= undefined){
			tId = treeId;
		}
		$('#' + tId).jstree({
	        "plugins": ["themes", "json_data","ui","checkbox"],
	        "themes": {
	            "theme": "classic",
	            "icons": false,
	            "dots": true
	        },
	        "json_data": {
	        	"ajax" : {
	        		"method": "get",
	                "url" : function( node ){
	                      if(forNvptkh != undefined || forNvptkh ==true) {
	                    	  if(node == -1) {
	                    		  return url;
	                    	  } else {
	                    		var displayId = $('#selId').val();
	                    		var staffCode = encodeURI('');
	      						var staffName = encodeURI('');
	      						return '/rest/catalog/display-program/staff/list.json?displayProgramId='+ 0 +'&staffCode='+ encodeChar(staffCode) +'&staffName='+ encodeChar(staffName);
	                    	  }
	                      } else {
	                    	  if(node == -1) {
		                    	  return url;
		                      } else {
		                    	  var nodeType = node.attr("classStyle");
		                    	  var catId = 0;
		                    	  var subCatId = 0;
		                    	  if(nodeType == 'cat') {
		                    		  catId = node.attr("id");
		                    		  var displayId = $('#selId').val();
		                    		  var productCode = encodeURI('');
		                    		  var productName = encodeURI('');	
		                    		  return '/rest/catalog/display-program/product/list.json?displayProgramId='+ 0 +'&productCode='+ encodeChar(productCode) +'&productName='+ encodeChar(productName) + '&nodeType=sub-cat&catId=' +catId+ '&subCatId=0';
		                    	  } else if(nodeType == 'sub-cat') {
		                    		  subCatId = node.attr("id");
		                    		  catId = $('#'+subCatId).parent().parent().attr('id');
		                    		  var displayId = $('#selId').val();
		                    		  var productCode = encodeURI('');
		                    		  var productName = encodeURI('');	
		                    		  return '/rest/catalog/display-program/product/list.json?displayProgramId='+ 0 +'&productCode='+ encodeChar(productCode) +'&productName='+ encodeChar(productName) + '&nodeType=product&catId=' +catId+ '&subCatId=' + subCatId;
		                    	  } else if(nodeType == 'product') {
		                    		  return '';
		                    	  }
		                      }
	                      }
	                    },
	                "data" : function (n) {
	                        return { id : n.attr ? n.attr("id") : 0 };
	                    }
	            }
	        }
		});	
	},
	loadDataForTreeWithCheckboxExDisplayGroup:function(url,treeId, forNvptkh){
		var tId = 'tree';
		if(treeId!= null && treeId!= undefined){
			tId = treeId;
		}
		$('#' + tId).jstree({
	        "plugins": ["themes", "json_data","ui","checkbox"],
	        "themes": {
	            "theme": "classic",
	            "icons": false,
	            "dots": true
	        },
	        "json_data": {
	        	"ajax" : {
	        		"method": "get",
	                "url" : function( node ){
	                      if(forNvptkh != undefined || forNvptkh ==true) {
	                    	  if(node == -1) {
	                    		  return url;
	                    	  } else {
	                    		var displayId = $('#selId').val();
	                    		var staffCode = encodeURI('');
	      						var staffName = encodeURI('');
	      						return '/rest/catalog/display-program/staff/list.json?displayProgramId='+ displayId +'&staffCode='+ encodeChar(staffCode) +'&staffName='+ encodeChar(staffName);
	                    	  }
	                      } else {
	                    	  if(node == -1) {
		                    	  return url;
		                      } else {
		                    	  var nodeType = node.attr("classStyle");
		                    	  var catId = 0;
		                    	  var subCatId = 0;
		                    	  if(nodeType == 'cat') {
		                    		  catId = node.attr("id");
		                    		  var displayId = $('#selId').val();
		                    		  var productCode = encodeURI('');
		                    		  var productName = encodeURI('');	
		                    		  return '/rest/catalog/display-program/productDisplayGroup/list.json?displayProgramId='+ displayId +'&productCode='+ encodeChar(productCode) +'&productName='+ encodeChar(productName) + '&nodeType=sub-cat&catId=' +catId+ '&subCatId=0';
		                    	  } else if(nodeType == 'sub-cat') {
		                    		  subCatId = node.attr("id");
		                    		  catId = $('#'+subCatId).parent().parent().attr('id');
		                    		  var displayId = $('#selId').val();
		                    		  var productCode = encodeURI('');
		                    		  var productName = encodeURI('');	
		                    		  return '/rest/catalog/display-program/productDisplayGroup/list.json?displayProgramId='+ displayId +'&productCode='+ encodeChar(productCode) +'&productName='+ encodeChar(productName) + '&nodeType=product&catId=' +catId+ '&subCatId=' + subCatId;
		                    	  } else if(nodeType == 'product') {
		                    		  return '';
		                    	  }
		                      }
	                      }
	                    },
	                "data" : function (n) {
	                        return { id : n.attr ? n.attr("id") : 0 };
	                    }
	            }
	        }
		});	
	},
	gotoTab: function(tabIndex){
		ProgrammeDisplayCatalog.deactiveAllMainTab();
		$('#tabContent2').html('');
		$('#tabContent3').html('');
		$('#tabContent4').html('');
		$('.FakeInputFile input').each(function(){
			$(this).val('');
		});
		switch (tabIndex) {
		case 0:
			$('#tabContent1').show();
			$('#tab1 a').addClass('Active');
			break;
		case 1:
			$('#tabContent2').show();
			$('#tab2 a').addClass('Active');
			AttributesManager.getAttributesForm('tabContent2',AttributesManager.DISPLAY_PROGRAM);
			$('#typeAttribute').val(AttributesManager.DISPLAY_PROGRAM);			
			break;
		case 2:
			$('#tabContent3').show();
			$('#tab3 a').addClass('Active');
			AttributesManager.getAttributesForm('tabContent3',AttributesManager.DISPLAY_PROGRAM_LEVEL);
			$('#typeAttribute').val(AttributesManager.DISPLAY_PROGRAM_LEVEL);			
			break;		
		case 3:
			$('#tabContent4').show();
			$('#tab4 a').addClass('Active');
			AttributesManager.getAttributesForm('tabContent4',AttributesManager.DISPLAY_GROUP);
			$('#typeAttribute').val(AttributesManager.DISPLAY_GROUP);			
			break;	
		default:
			$('#tabContent1').show();
			break;
		}
		$('.ErrorMsgStyle').each(function(){
			if(!$(this).is(':hidden')){
				$(this).html('').hide();
			}
		});
	},	
	deactiveAllMainTab: function(){
		$('#tab1 a').removeClass('Active');
		$('#tab2 a').removeClass('Active');
		$('#tab3 a').removeClass('Active');
		$('#tab4 a').removeClass('Active');
		$('#tabContent1').hide();
		$('#tabContent2').hide();
		$('#tabContent3').hide();
		$('#tabContent4').hide();
	},
	exportExcelDisplayPrograme:function(){
		var excelType = $('#excelType').val();
		var code = $('#code').val();
		var name = $('#name').val();
		var status = $('#status').val().trim();
		var fDate = $('#fDate').val();
		var tDate = $('#tDate').val();
		var cont = -1;
		if($('#orCont').is(':checked')){
			cont = 1;
		} else if($('#andCont').is(':checked')){
			cont = 0;
		}
		var params = new Object();
		params.code = code;
		params.name = name;
		params.stt = status;
		params.fromDate = fDate;
		params.toDate = tDate;
		params.cont = cont;
		
		if(excelType == 1){
			Utils.addOrSaveData(params, '/catalog/programme-display/export-Excel-Display-Programe', null, 'errMsg', function(data) {
				window.location.href=data.view;
			}, 'loading', null, null, 'Bạn có muốn xuất file này?');
			return false;
		}else if(excelType == 2){
			Utils.addOrSaveData(params, '/catalog/programme-display/export-Excel-Quota-Group', null, 'errMsg', function(data) {
				window.location.href=data.view;
			}, 'loading', null, null, 'Bạn có muốn xuất file này?');
			return false;
		}else if(excelType == 3){
			Utils.addOrSaveData(params, '/catalog/programme-display/export-Excel-Level', null, 'errMsg', function(data) {
				window.location.href=data.view;
			}, 'loading', null, null, 'Bạn có muốn xuất file này?');
			return false;
		}else if(excelType == 4){
			Utils.addOrSaveData(params, '/catalog/programme-display/export-Excel-Product', null, 'errMsg', function(data) {
				window.location.href=data.view;
			}, 'loading', null, null, 'Bạn có muốn xuất file này?');
			return false;
		}else if(excelType == 5){
			Utils.addOrSaveData(params, '/catalog/programme-display/export-Excel-Staff', null, 'errMsg', function(data) {
				window.location.href=data.view;
			}, 'loading', null, null, 'Bạn có muốn xuất file này?');
			return false;
		}else if(excelType == 6){
			Utils.addOrSaveData(params, '/catalog/programme-display/export-Excel-Shop', null, 'errMsg', function(data) {
				window.location.href=data.view;
			}, 'loading', null, null, 'Bạn có muốn xuất file này?');
			return false;
		}
	},
	// huyNP4
	importExcel:function(){
		$('#isView').val(0);
		$('#importFrm').submit();
		return false;	
	},	
	// huyNP4
	viewExcel:function(){
		$('#isView').val(1);
		$('#importFrm').submit();
		return false;
	},
	// huyNP4
	beforeImportExcel: function(){
		if(!previewImportExcelFile(document.getElementById("excelFile"))){
			return false;
		}
		// showLoading();
		$(".ErrorMsgStyle").hide();
		$('#divOverlay').addClass('Overlay');
		$('#imgOverlay').show();
		return true;
	},
	// huyNP4
	afterImportExcel01: function(responseText, statusText, xhr, $form){// bb HUyNP
		$('#divOverlay').removeClass('Overlay');
		$('#imgOverlay').hide();
		if (statusText == 'success') {				
	    	$("#responseDiv").html(responseText);		   
	    	if($('#errorExcel').html().trim() == 'true' || $('#errorExcelMsg').html().trim().length > 0){
	    		$('#errExcelMsg').html($('#errorExcelMsg').html()).show();
	    	} else {
	    		if($('#typeView').html().trim() == 'false'){
	    			var totalRow = parseInt($('#totalRow').html().trim());
	    			var numFail = parseInt($('#numFail').html().trim());
	    			var fileNameFail = $('#fileNameFail').html();
	    			var mes = format(msgErr_result_import_excel,(totalRow - numFail),numFail);		    		
	    			if(numFail > 0){
	    				mes+= ' <a href="'+ fileNameFail +'">Xem chi tiết lỗi</a>';
	    			}
	    			$('#errExcelMsg').html(mes).show();
	    			$("#grid").trigger("reloadGrid");
	    			if(ProductLevelCatalog._callBackAfterImport != null){
		    			ProductLevelCatalog._callBackAfterImport.call(this);
		    		}
	    		}else{
	    			var obj = '';
	    			var tiles='';
	    			if($('#excelType').val() == '' || $('#excelType').val() == undefined ){
	    				obj = '#popup1';
	    				tiles='Danh sách import Excel CTTB';
	    			}else{
	    				if(parseInt($('#excelType').val()) == 1){
	    					obj = '#popup1';
	    					tiles='Danh sách import Excel CTTB';
	    				}else if(parseInt($('#excelType').val()) == 2){
	    					obj = '#popup2';
	    					tiles='Danh sách import Excel nhóm chỉ tiêu';
	    				}else if(parseInt($('#excelType').val()) == 3){
	    					obj = '#popup3';
	    					tiles='Danh sách import Excel mức';
	    				}else if(parseInt($('#excelType').val()) == 4){
	    					obj = '#popup4';
	    					tiles='Danh sách import Excel sản phẩm';
	    				}else if(parseInt($('#excelType').val()) == 5){
	    					obj = '#popup5';
	    					tiles='Danh sách import Excel Nhân viên phát triển khách hàng';
	    				}else if(parseInt($('#excelType').val()) == 6){
	    					obj = '#popup6';
	    					tiles='Danh sách đơn vị tham gia CTTB';
	    				}
	    			}
	    			var html = $(obj).html();
	    			$.fancybox(html,
    					{
    						modal: true,
    						title: tiles,
    						afterShow: function(){
    							$(obj).html('');
    							$('#scrollExcelDialog').jScrollPane();
    							$('.ScrollSection').jScrollPane().data().jsp;
    							$('.ScrollBodySection').jScrollPane();
    							$(window).resize();
    						},
    						afterClose: function(){
    							$(obj).html(html);
    						}
    					}
    				);
	    		}
	    	}
	    }	
	},
	showShopTab: function(){
		ProgrammeDisplayCatalog.hideAllTab();
		$('#tab4').addClass('Active');		
		$('#container7').show();
		// Utils.bindAutoSearch();
		$('#downloadTemplate').attr( 'href', excel_template_path + 'display-program/Bieu_mau_danh_muc_CTTB_don_vi_import.xls');
		var value = $('#divWidth').width()-50;
		var dataWidth = (value * 95)/100-6;
		var editWidth = (value * 10)/100;
		var deleteWidth = (value * 9)/100+6;
		var statusPermission = $('#statusPermission').val();
		var isOutOfDate = $('#isOutOfDate').val();
		var titleOpenDialogShop ="";
		if($('#permissionUser').val() == 'true'){
			if((statusPermission == 2 || statusPermission == 1) && isOutOfDate == 0){
				titleOpenDialogShop = '<a href="javascript:void(0);" onclick="return ProgrammeDisplayCatalog.openShopDialog();"><img src="/resources/images/icon_add.png"></a>';
			}
		}
		$('#exGrid').treegrid({  
		    url:  ProgrammeDisplayCatalog.getShopGridUrl($('#selId').val(),'',''),
		     width:($('#divWidth').width()-50),  
	        height:'auto',  
	        idField: 'id',  
	        treeField: 'data',
		    columns:[[  
		        {field:'data',title:'Đơn vị',resizable:false,width:dataWidth,formatter:function(value,row,index){
		        	return Utils.XSSEncode(row.data);
		        }},
		        {field:'edit',title:titleOpenDialogShop,width:50,align:'center',resizable:false,formatter:function(value,row,index){
//		        	if(row.isJoin >= 1){
//		        		return '<input type ="checkbox" disabled="disabled">';
//		        	}
		        	var classParent = "";
		        	if(row.parentId != null && row.parentId != undefined){
		        		classParent = "class_"+row.parentId;
		        	}
		        	if($('#permissionUser').val() == 'true'){
		        		if(statusPermission == 2 || statusPermission == 1){
		        			return '<input type ="checkbox" class="'+classParent+'" value="'+row.id+'" id="check_'+row.id+'" onclick="return ProgrammeDisplayCatalog.onCheckShopEx('+row.id+');">';
		        		}
		        	}
		        	
		        }}
		    ]] 
		});
		$('#shopCode').bind('keyup', function(event){
			if(event.keyCode == 13){
				ProgrammeDisplayCatalog.searchShop();
			}
		}).val('');
		$('#shopName').bind('keyup', function(event){
			if(event.keyCode == 13){
				ProgrammeDisplayCatalog.searchShop();
			}
		}).val('');
	},
	showCustomerTab1: function(){
		ProgrammeDisplayCatalog.hideAllTab();
		$('#tab10').addClass('Active');		
		$('#container10').show();
		ReportUtils.loadComboReportTreeEx('shopTree', 'shopCode', $('#curShopId').val(),function(data){
		});
		$('#customerGrid123').datagrid({
			url :'/programme-display/customer/search',
			autoRowHeight : true,
			rownumbers : true, 
			checkOnSelect :true,
			pagination:true,
			//singleSelect:true,			
			queryParams:{
				id:$('#selId').val()
			},
			fitColumns:true, 
			scrollbarSize : 0,
			pageSize:50,
			pageNumber:1,
			width : $(window).width()-48, 
			//width: $('#customerDisplayGrid').width(),
			columns:[[  
//		        	{field: 'displayProgramCode',title:'Mã CTTB', width:100,align:'left',sortable : false,resizable : false,formatter :function(value,row,index) {
//						return Utils.XSSEncode(value);
//					}},
	        	{field: 'levelCode',title:'Mức CTTB', width:50,align:'left',sortable : false,resizable : false,formatter :function(value,row,index) {
					return Utils.XSSEncode(value);
				}},  
	        	{field: 'shopCode',title:'Mã NPP', width:50,align:'left',sortable : false,resizable : false,formatter :function(value,row,index) {
					return Utils.XSSEncode(value);
				}},  
	        	{field: 'staffName',title:'NVBH', width:100,align:'left',sortable : false,resizable : false,formatter :function(value,row,index) {
					return Utils.XSSEncode(value);
				}},  
	        	{field: 'customerName',title:'Khách hàng', width:100,align:'left',sortable : false,resizable : false,formatter :function(value,row,index) {
					return Utils.XSSEncode(value);
				}},  
				{field: 'address',title:'Địa chỉ', width:100,align:'left',sortable : false,resizable : false,formatter :function(value,row,index) {
					return Utils.XSSEncode(row.houseNumber) + " " + Utils.XSSEncode(row.street);
				}},
	        	{field: 'fromDate',title:'Tháng', width:50,align:'center',sortable : false,resizable : false,formatter:function(row,index){
	        		$('#totalRowGrid').val('1');
		        		return Utils.XSSEncode(row);
	        		}
	        	}, 
	        	{field: 'oneMonth',title:'Ngày tạo', width:50,align:'center',sortable : false,resizable : false,formatter:function(row,index){
		        		return Utils.XSSEncode(row);
	        		}
	        	}, 
	        	{field: 'updateDate',title:'Ngày điều chỉnh', width:60,align:'center',sortable : false,resizable : false,formatter:function(row,index){
		        		return Utils.XSSEncode(row);
	        		}
	        	}
//	        	{field: 'id', checkbox:true, align:'center', width:80,sortable : false,resizable : false}
	        ]],
	        onLoadSuccess :function(data){
		    	$('.datagrid-header-rownumber').html('STT');
		    	var originalSize = $('.datagrid-header-row td[field=customerName] div').width();
		    	if(originalSize != null && originalSize != undefined){
		    		ProgrammeDisplayCatalog._lstSize.push(originalSize);
		    	}
		    	ProgrammeDisplayCatalog.updateRownumWidthForJqGridEX1('#customerGrid123','customerName');
//		    	if(data.total <= 50){  
//		    		$('#loadShop').hide(); 
//		    		$('#loadShop').attr('disabled','disabled');
//		    	}else{
//		    		$('#loadShop').show();
//		    		$('#loadShop').removeAttr('disabled');
//		    	}
		    	$('input[name="id"]').each(function(){
					var temp = ProgrammeDisplayCatalog._lstProduct.get($(this).val());
					if(temp!=null) $(this).attr('checked','checked');
				});
		    	var length = 0;
		    	$('input[name="id"]').each(function(){
		    		if($(this).is(':checked')){
		    			++length;
		    		}	    		
		    	});	    	
		    	if(data.rows.length==length){
		    		$('.datagrid-header-check input').attr('checked',true);
		    	}else{
		    		$('.datagrid-header-check input').attr('checked',false);
		    	}
		    },
		    onCheck:function(i,r){
		    	$('#checkDeleteAll').removeAttr('checked');
		    	ProgrammeDisplayCatalog._lstProduct.put(r.id,r);
		    },
		    onUncheck:function(i,r){
		    	ProgrammeDisplayCatalog._lstProduct.remove(r.id);
		    },
		    onCheckAll:function(r){
		    	$('#checkDeleteAll').removeAttr('checked');
		    	for(i=0;i<r.length;i++){
		    		ProgrammeDisplayCatalog._lstProduct.put(r[i].id,r[i]);
		    	}
		    },
		    onUncheckAll:function(r){
		    	for(i=0;i<r.length;i++){
		    		ProgrammeDisplayCatalog._lstProduct.remove(r[i].id);
		    	}
		    }
		});
		
		$('#errCustomerMsg').html('').hide();
		var params = new Object();
		var now = new Date();
		var cYear = now.getFullYear();
		var cMonth = now.getMonth() + 1;
		if(cMonth != 10 && cMonth != 11 && cMonth != 12  ){
			cMonth = '0'+cMonth;
		}
		var currentDate = cMonth+'/'+cYear;
		$('#month1').val(currentDate);
		var month = $('#month1').val();
		params.id = $('#selId').val();
		params.shopId = $('#shopTreeId .combo-value').val();
		params.month = month;
		$('#filterMonth').val(month);
		ProgrammeDisplayCatalog._params = params;
	},
	searchCustomerTab:function(){
		var shopId = $('#shopTreeId .combo-value').val();
		var month = $('#month1').val();
		var params = new Object();
		params.shopId = shopId;
		params.month = month;
		params.id = $('#selId').val();		
		ProgrammeDisplayCatalog._params = params;
		$('#filterMonth').val(month);
		ProgrammeDisplayCatalog._lstProduct = new Map();
		$('#errCustomerMsg').html('').hide();
		$('#customerGrid123').datagrid('reload',params);
	},
	searchCustomerTabEX:function(){
		$('.ErrorMsgStyle').hide();
		$('#checkDeleteAll').removeAttr('checked');
		var shopKendo = $("#shop").data("kendoMultiSelect");
        var lstShopId = new Array();
        lstShopId = shopKendo.value();
		var month = $('#month1').val();
		var params = new Object();
		if(lstShopId != null && lstShopId != undefined){
			params.listShopIdString = lstShopId.toString();
		}

		params.month = month;
		params.id = $('#displayProgram').val();		
		ProgrammeDisplayCatalog._params = params;
		$('#filterMonth').val(month);
		ProgrammeDisplayCatalog._lstProduct = new Map();
		$('#customerGrid123').datagrid('load',params);
//		setTimeout(function(){ //Set timeout để đảm bảo file load lên hoàn tất
//			$('#customerGrid123').datagrid({pageNumber:1});
//      },2000);
	},
	loadDisplayProgram:function(params){
		Utils.getHtmlDataByAjax(params,
				'/programme-display/customer/loadDisplayProgram',
				function(data) {
					try {
						var _data = JSON.parse(data);
					} catch (e) {

						$('#displayProgram').html(data);
						$('.CustomStyleSelectBoxInner').html("Tất cả");
					}
				}, '', 'GET');
	},
	getCustomerTabGridUrl:function(shopId,month){
		var url = '/programme-display/customer/search?id='+$('#selId').val()+"&shopId="+shopId+"&month="+month;
		return url;
	},
// getShopGridUrl: function(code,name,status){
// var selId = $('#selId').val();
// return '/catalog/programme-display/searchshop?id=' + encodeChar(selId) +
// '&code=' + encodeChar(code) + '&name=' + encodeChar(name) + '&status=' +
// status;
// },
	showUpdateShopDetailDialog: function(rowId,status){
		var html = $('#shopDetailTreeDialog').html();
		$.fancybox(html,
				{
					modal: true,
					autoResize: false,
					title: 'Cập nhật thông tin đơn vị',					
					afterShow: function(){
						$('#shopDetailTreeDialog').html('');
						$('#errMsgShopDetailDlg').html('').hide();
						$('#successMsgShopDetailDlg').html('').hide();						
						
						var name = $("#shopGrid").jqGrid ('getCell', rowId, 'shop.shopName');
						var id = $("#shopGrid").jqGrid ('getCell', rowId, 'shop.id');					    						
						if(status == 'RUNNING'){
							status = 1;
						} else if(status == 'STOPPED'){
							status = 0;
						} else {
							status = -2;
						}						
						$('#shopNameDetailDlg').val(name);
						$('#shopStatusDlgTmp').attr('id','shopStatusDlg');
						$('#shopStatusDlg').val(status);
						$('#shopStatusDlg').addClass('MySelectBoxClass');
						$('#shopStatusDlg').customStyle();						
						$('#shopIdDetailDlg').val(id);
					},
					afterClose: function(){
						$('#shopDetailTreeDialog').html(html);						
					}
				}
			);
		return false;
	},
	deleteShopRow: function(id){
		var dataModel = new Object();
		dataModel.shopMapId = id;	
		Utils.deleteSelectRowOnGrid(dataModel, 'đơn vị', '/catalog/programme-display/delete-shop', ProgrammeDisplayCatalog._xhrDel, 'shopGrid','errMsgShop',function(data){
			showSuccessMsg('successMsgShop',data);
		},'shopLoading');		
		return false;
	},
	saveDetailShop: function(){
		$('#errMsgShopDetailDlg').html('').hide();
		$('#successMsgShopDetailDlg').html('').hide();
		$.fancybox.update();
		var msg = '';
		msg = Utils.getMessageOfRequireCheck('shopStatusDlg', 'Trạng thái',true);		
		if(msg.length > 0){
			$('#errMsgShopDetailDlg').html(msg).show();
			$.fancybox.update();
			return false;
		}		
		var displayProgramId = $('#selId').val();
		var arrId = new Array();
		arrId.push($('#shopIdDetailDlg').val());
		var dataModel = new Object();
		dataModel.lstShopId = arrId;
		dataModel.id = displayProgramId;
		dataModel.status = $('#shopStatusDlg').val().trim();
		dataModel.numUpdate = 0; 
		Utils.addOrSaveData(dataModel, '/catalog/programme-display/shop/save', ProgrammeDisplayCatalog._xhrSave, 'errMsgShopDlg', function(data){
			$('#successMsgShopDlg').html('Lưu dữ liệu thành công').show();						
			$.fancybox.update();
			ProgrammeDisplayCatalog.searchShop();
			setTimeout(function(){$.fancybox.close();}, 1000);
		}, null, '.fancybox-inner', null, null);
		return false;
	},
//	searchShop: function(){
//		var code = $('#shopCode').val().trim();
//		var name = $('#shopName').val().trim();
//		var status = $('#shopStatus').val().trim();
//		var url = ProgrammeDisplayCatalog.getShopGridUrl(code,name,status);
//		$("#shopGrid").setGridParam({url:url,page:1}).trigger("reloadGrid");
//	},
	showDialogCreateShop: function(){
		var html = $('#shopTreeDialog').html();
		$.fancybox(html,
				{
					modal: true,
					autoResize: false,
					title: 'Thêm đơn vị tham gia CTTB',
					afterShow: function(){
						Utils.resetCheckboxStateOfTree();
						Utils.bindAutoSearch();	
						$('#shopTreeDialog').html('');
						$('#successMsgShopDlg').html('').hide();
						var displayProgramId = $('#selId').val();
						$('#shopCodeDlg').focus();
						var shopCode = encodeURI('');
						var shopName = encodeURI('');	
						ProgrammeDisplayCatalog.loadShopTreeOnDialog('/rest/catalog/programme-display/shop/list.json?displayProgramId='+ displayProgramId +'&shopCode='+ encodeChar(shopCode) +'&shopName='+ encodeChar(shopName));
						ProgrammeDisplayCatalog._mapShop = new Map();
						$('#shopTree').bind("loaded.jstree open_node.jstree close_node.jstree", function (event, data) {
							if($('#shopTreeContent').data('jsp') != undefined || $('#shopTreeContent').data('jsp') != null) {
								$('#shopTreeContent').data('jsp').destroy();
								setTimeout(function(){
									$('#shopTreeContent').jScrollPane();
								},500);	
							} else {
								setTimeout(function(){
									$('#shopTreeContent').jScrollPane();
								},500);	
							}						
		    			});
					},
					afterClose: function(){
						$('#shopTreeDialog').html(html);						
					}
				}
			);
		return false;
	},
	searchShopOnTree: function(){
		// Utils.getCheckboxStateOfTree();
		$('#shopTree li').each(function(){
			var _id = $(this).attr('id');
			if($(this).hasClass('jstree-checked')){
				ProgrammeDisplayCatalog._mapShop.put(_id,_id);	
			}else{
				ProgrammeDisplayCatalog._mapShop.remove(_id);
			}		
		});
		var displayProgramId = $('#selId').val();
		var shopCode = encodeChar($('#shopCodeDlg').val().trim());
		var shopName = encodeChar($('#shopNameDlg').val().trim());
		$('#shopTreeContent').data('jsp').destroy();
		ProgrammeDisplayCatalog.loadShopTreeOnDialog('/rest/catalog/programme-display/shop/list.json?displayProgramId='+ displayProgramId +'&shopCode='+ encodeChar(shopCode) +'&shopName='+ encodeChar(shopName));
		setTimeout(function() {
			$('#shopTreeContent').jScrollPane();
			$('#shopTree').bind("loaded.jstree open_node.jstree close_node.jstree", function (event, data) {
				if($('#shopTreeContent').data('jsp') != undefined || $('#shopTreeContent').data('jsp') != null) {
					$('#shopTreeContent').data('jsp').destroy();
					setTimeout(function(){
						$('#shopTreeContent').jScrollPane();
					},500);	
				} else {
					setTimeout(function(){
						$('#shopTreeContent').jScrollPane();
					},500);	
				}
				ProgrammeDisplayCatalog.getCheckboxStateOfTreeEx('shopTree',ProgrammeDisplayCatalog._mapShop,'shopTree li');
			});
			ProgrammeDisplayCatalog.getCheckboxStateOfTreeEx('shopTree',ProgrammeDisplayCatalog._mapShop,'shopTree li');
		}, 500);
		$('.fancybox-inner #shopCodeDlg').focus();
	},
	selectListShop: function(){
		$('#errMsgShopDlg').html('').hide();
		$('#successMsgShopDlg').html('').hide();
		$.fancybox.update();		
		var arrId = new Array();
// $('#shopTree li').each(function(){
// var _id = $(this).attr('id');
// if($(this).hasClass('jstree-checked')){
// ProgrammeDisplayCatalog._mapShop.put(_id,_id);
// }else{
// ProgrammeDisplayCatalog._mapShop.remove(_id);
// }
// });
		$('.jstree-checked').each(function(){
		    var _id= $(this).attr('id');
		    if($('#' + _id + ' ul li').length >0 && (!$(this).parent().parent().hasClass('jstree-checked'))){
		    	ProgrammeDisplayCatalog._mapShop.put(_id,_id);
		    }else if($('#' + _id ).length >0 && (!$(this).parent().parent().hasClass('jstree-checked'))){
		    	ProgrammeDisplayCatalog._mapShop.put(_id,_id);
			}else{
				ProgrammeDisplayCatalog._mapShop.remove(_id);
			}	
		});
		for(var i=0;i<ProgrammeDisplayCatalog._mapShop.size();++i){
			var _obj = ProgrammeDisplayCatalog._mapShop.get(ProgrammeDisplayCatalog._mapShop.keyArray[i]);
			arrId.push(_obj);
		}		
		var msg = '';
		if(arrId.length == 0){
			msg = format(msgErr_required_choose_format,'Đơn vị');
		}		
		if(msg.length > 0){
			$('#errMsgShopDlg').html(msg).show();			
			$.fancybox.update();
			return false;
		}		
		var focusProgramId = $('#selId').val();
		var dataModel = new Object();		
		dataModel.lstShopId = arrId;		
		dataModel.id = focusProgramId;
		dataModel.status = -2;
		Utils.addOrSaveData(dataModel, '/catalog/programme-display/shop/save', ProgrammeDisplayCatalog._xhrSave, 'errMsgShopDlg', function(data){
			$('#successMsgShopDlg').html('Lưu dữ liệu thành công').show();
			$('#shopCodeDlg').val('');
			$('#shopNameDlg').val('');
			$('#shopCode').val('');
			$('#shopName').val('');
			setSelectBoxValue('shopStatus', activeType.RUNNING);
			Utils.resetCheckboxStateOfTree();
			ProgrammeDisplayCatalog.searchShopOnTree();	
			ProgrammeDisplayCatalog.searchShop();
			$(window).resize();
			$.fancybox.update();
			// $("#shopGrid").trigger("reloadGrid");
			setTimeout(function(){$.fancybox.close();}, 1000);
		}, null, '.fancybox-inner', null, null);
		return false;
	},
	loadShopTreeOnDialog: function(url){
		$.getJSON(url, function(data){
			$('#shopTree').bind("loaded.jstree open_node.jstree close_node.jstree", function (event, data) {				
				if($('#shopTreeContent').data('jsp') != undefined || $('#shopTreeContent').data('jsp') != null) {
					$('#shopTreeContent').data('jsp').destroy();
					setTimeout(function(){
						$('#shopTreeContent').jScrollPane();
					},500);	
				} else {
					setTimeout(function(){
						$('#shopTreeContent').jScrollPane();
					},500);	
				}
			}).jstree({
		        "plugins": ["themes", "json_data","ui","checkbox"],
		        "themes": {
		            "theme": "classic",
		            "icons": false,
		            "dots": true
		        },
		        "json_data": {
		        	"data": data,
		        	"ajax" : {
		        		"method": "GET",
		                "url" : '/rest/catalog/sub-shop/programme-display/list.json?displayProgramId=' +$('#selId').val(),
		                "data" : function (n) {
		                        return { id : n.attr ? n.attr("id") : 0 };
		                    }
		            }
		        }
			});
		});
	},
	selectListProductQuota: function(groupId,gridId){
		$('#errMsgProductDlg').html('').hide();
		$('#successMsgProductDlg').html('').hide(); 
		$('#errMsgQuotaGroup').html('').hide();
		$.fancybox.update();
		var arrId = new Array();
		var msgError = ""; var strId = "";
		$('#productTree li').each(function(){
			var _id = $(this).attr('id');
			var type = $(this).attr('contentitemid');
			if($(this).hasClass('jstree-checked') && type=='product'){
				ProgrammeDisplayCatalog._mapProduct.put(_id,_id);		
			}else{
				ProgrammeDisplayCatalog._mapProduct.remove(_id);	
			}			
		});
		for(var i=0;i<ProgrammeDisplayCatalog._mapProduct.size();++i){
			var _obj = ProgrammeDisplayCatalog._mapProduct.get(ProgrammeDisplayCatalog._mapProduct.keyArray[i]);
			arrId.push(_obj);
			if (strId == "") strId += _obj;
			else strId +="," + _obj;
		}
		var msg = '';
		if(arrId.length == 0){
			msg = format(msgErr_required_choose_format,'Sản phẩm ');
		}
		if(msg.length > 0){
			$('#errMsgProductDlg').html(msg).show();
			$.fancybox.update();
			return false;
		}
		var moreCat = 0;		
		$('.jstree-no-icons').children().each(function(){
			if($(this).hasClass('jstree-checked') || $(this).hasClass('jstree-undetermined')){
				moreCat++;
			}
		});
		var displayId = $('#selId').val();
		var dataModel = new Object();		
		dataModel.id = groupId;
		dataModel.quota = true;
		var arrIdnew = new Array();
		if(moreCat>1){
			$.messager.confirm('Xác nhận', 'Bạn đang chọn các sản phẩm hơn 1 ngành hàng. Bạn có muốn lưu thông tin không?', function(r){
				if (r){
					// code check here
					$.getJSON('/rest/catalog/display-program/treeQouta/'+ strId +'/'+ $('#selId').val().trim() +'/list.json', function(data){
						if (data != null) 
						{
							dataModel.lstProductId = data[0];
							var productStrTemp = "";
							if (data[1].length > 0){
								for (var i=0; i< data[1].length; i++)
								{
									if (productStrTemp =="") productStrTemp += data[1][i];
									else productStrTemp += "," + data[1][i];
								}
								$.getJSON('/rest/catalog/display-program/treeQoutaShowErrorMsg/'+ productStrTemp +'/list.json', function(result){
									$('.fancybox-inner #errMsgProductDlg').html(result[0] +" đã tồn tại trong nhóm chỉ tiêu khác.").show();
								});
							}
							if (data[0].length > 0){ // co du lieu hop le de
														// luu
								Utils.saveData(dataModel, '/catalog/programme-display/product/save', ProgrammeDisplayCatalog._xhrSave, 'errMsgProductDlg', function(data){
									$('#successMsgProductDlg').html('Lưu dữ liệu thành công').show();
									$('#productCodeDlg').val('');
									$('#productNameDlg').val('');
									$('#numDisplay').val('');			
									Utils.resetCheckboxStateOfTree();
									ProgrammeDisplayCatalog.searchProductOnTreeQuota();
									$('#productCode').val('');
									$('#productName').val('');
									$('#quantity').val('');
									ProgrammeDisplayCatalog.searchProduct();
									$('#quotaGrid').collapseSubGridRow(ProgrammeDisplayCatalog._curRowId);
									$('#quotaGrid').expandSubGridRow(ProgrammeDisplayCatalog._curRowId);
									$.fancybox.update();
									$('#quotaGrid_'+groupId+'_t').trigger("reloadGrid"); 
									$.fancybox.close();
								}, null, '.fancybox-inner');	
							}
						}
					});
				}
			});
		} else {
			// code check here
			$.getJSON('/rest/catalog/display-program/treeQouta/'+ strId +'/'+ $('#selId').val().trim() +'/list.json', function(data){
				if (data != null) 
				{
					dataModel.lstProductId = data[0];
					var productStrTemp = "";
					if (data[1].length > 0){
						for (var i=0; i< data[1].length; i++)
						{
							if (productStrTemp =="") productStrTemp += data[1][i];
							else productStrTemp += "," + data[1][i];
						}
						$.getJSON('/rest/catalog/display-program/treeQoutaShowErrorMsg/'+ productStrTemp +'/list.json', function(result){
							$('.fancybox-inner #errMsgProductDlg').html(result[0] +" đã tồn tại trong các nhóm chỉ tiêu khác.").show();
						});
					}
					if (data[0].length > 0){ // co du lieu hop le de luu
						Utils.addOrSaveData(dataModel, '/catalog/programme-display/product/save', ProgrammeDisplayCatalog._xhrSave, 'errMsgProductDlg', function(data){
							$('#successMsgProductDlg').html('Lưu dữ liệu thành công').show();
							$('#productCodeDlg').val('');
							$('#productNameDlg').val('');
							$('#numDisplay').val('');
							Utils.resetCheckboxStateOfTree();
							ProgrammeDisplayCatalog.searchProductOnTreeQuota();
							$('#productCode').val('');
							$('#productName').val('');
							$('#quantity').val('');
							ProgrammeDisplayCatalog.searchProduct();
							$('#quotaGrid').collapseSubGridRow(ProgrammeDisplayCatalog._curRowId);
							$('#quotaGrid').expandSubGridRow(ProgrammeDisplayCatalog._curRowId);
							$.fancybox.update();
							$('#quotaGrid_'+groupId+'_t').trigger("reloadGrid");
							$.fancybox.close();
						}, null, '.fancybox-inner', null, null);
					}
				}
			});
		}
		return false;
	},
	getDisplayProgramExclusionUrl:function(code,name,displayProgramExclusion){
		code = encodeChar(code);
		name = encodeChar(name);
		return '/catalog/programme-display/program/search?code='+encodeChar(code)+'&name='+encodeChar(name)+'&displayProgramExclusion='+displayProgramExclusion;
	},
	searchDisplayProgramExclusion:function(){
		var code = $('#vnmDisplayProgramCode').val().trim();
		var name = $('#vnmDisplayProgramName').val().trim();
		var textCode = $('#code').val().trim();
		var textExclusion = $('#exclusion').val().trim();
		var displayProgramExclusion = textCode + ',' + textExclusion;
		var url = ProgrammeDisplayCatalog.getDisplayProgramExclusionUrl(code, name,displayProgramExclusion);			
		$("#vnmDisplayProgramGird").setGridParam({url:url,page:1}).trigger("reloadGrid");		
		return false;
	},
	showDisplayProgramExclusion:function(){
		$('#exclustionErr').html('').hide();
		var textCode = $('#code').val().trim();
		var textExclusion = $('#exclusion').val().trim();
		var displayProgramExclusion = textCode + ',' + textExclusion;	
		ProgrammeDisplayCatalog._mapExclusion = new Map();
		var html = $('#displayProgramExclusionDialog').html();		
		$.fancybox(html,{
			modal: true,
			title: 'Danh sách CTTB',
			afterShow: function(){
				$('#displayProgramExclusionDialog').html('');
				$("#vnmDisplayProgramGird").jqGrid({
					  url:ProgrammeDisplayCatalog.getDisplayProgramExclusionUrl('', '',displayProgramExclusion),
					  multiselect: true,
					  colModel:[							    
					    {name: 'displayProgramCode',index:'displayProgramCode', label: 'Mã CTTB', width: 120, sortable:false,resizable:false, align: 'left', formatter: function(value, row, index) {
							return Utils.XSSEncode(value);
						}},
					    {name: 'displayProgramName',index:'displayProgramName', label: 'Tên CTTB', sortable:false,resizable:false, align: 'left', formatter: function(value, row, index) {
							return Utils.XSSEncode(value);
						}},				    							    
					    {name: 'id', index:'id', hidden: true}					    
					  ],
					  pager : $('#vnmDisplayProgramPager'),
					  height: 'auto',
					  rowNum: 10,							 
					  width: ($('#vnmDisplayProgramContainerGrid').width()),
					  beforeRequest:function(){
						  $('.ui-jqgrid-bdiv .cbox').each(function(){							
							var code = $("#vnmDisplayProgramGird").jqGrid ('getCell', $(this).parent().parent().attr('id'), 'displayProgramCode');							
							if($(this).is(':checked')){																
								ProgrammeDisplayCatalog._mapExclusion.put(code,code);
							}else{
								var obj = ProgrammeDisplayCatalog._mapExclusion.get(code);
								if(obj!=null){
									ProgrammeDisplayCatalog._mapExclusion.remove(code);
								}
							}
						  });
					  },
					  gridComplete: function(){
						  $('#jqgh_vnmDisplayProgramGird_rn').html('STT');
						  Utils.bindAutoSearch();
						  $('.ui-jqgrid-bdiv .cbox').live('change', function(){
								if ($(".ui-jqgrid-bdiv .cbox").length == $(".ui-jqgrid-bdiv .cbox:checked").length){
									$('#cb_vnmDisplayProgramGird').attr('checked', 'checked');
								}
								else{
									$('#cb_vnmDisplayProgramGird').attr('checked',false);
								}								
						  });
						  $('.ui-jqgrid-bdiv .cbox').each(function(){
							  var code = $("#vnmDisplayProgramGird").jqGrid ('getCell', $(this).parent().parent().attr('id'), 'displayProgramCode');
							  var obj = ProgrammeDisplayCatalog._mapExclusion.get(code);
							  if(obj!=null){
								  $(this).attr('checked', 'checked');
							  }
							  $(this).parent().css('padding', '3px 3px');
						  });
						  $(window).resize();
						  $.fancybox.update();
						  updateRownumWidthForJqGrid('.fancybox-inner');
					  }
					})
					.navGrid($('#vnmDisplayProgramPager'), {edit:false,add:false,del:false, search: false});
			},
			afterClose: function(){
				$('#displayProgramExclusionDialog').html(html);	
			}
		});
		return false;
	},
	selectListDisplayProgramExclusion:function(){	
		$('.ui-jqgrid-bdiv .cbox').each(function(){							
			var code = $("#vnmDisplayProgramGird").jqGrid ('getCell', $(this).parent().parent().attr('id'), 'displayProgramCode');							
			if($(this).is(':checked')){																
				ProgrammeDisplayCatalog._mapExclusion.put(code,code);
			}
		 });
		if(ProgrammeDisplayCatalog._mapExclusion.size()==0){
			$('#exclustionErr').html('Vui lòng chọn CTTB loại trừ').show();
			return false;
		}
		var lst = '';
		for(var i=0;i<ProgrammeDisplayCatalog._mapExclusion.size();i++){			
			var obj =ProgrammeDisplayCatalog._mapExclusion.get(ProgrammeDisplayCatalog._mapExclusion.keyArray[i]);
			lst += obj + ',';
		}
		lst = lst.substring(0,lst.length-1);
		var exclusion = $('#exclusion').val().trim();
		if(exclusion.length>0){
			$('#exclusion').val(exclusion + ',' + lst);
		}else{
			$('#exclusion').val(lst);
		}
		$.fancybox.close();
	},	
	showArchives: function(){
		ProgrammeDisplayCatalog.hideAllTab();
		$('#tab8').addClass('Active');
		$('#container8').show();
	}
	,
	showExclusionProgram: function(){
		ProgrammeDisplayCatalog.hideAllTab();
		$('#tab1').addClass('Active');
		$('#container2').show();
		$('#displayProgramCode').bind('keyup', function(event){
			if(event.keyCode == 13){
				ProgrammeDisplayCatalog.searchExclusionProgram();
			}
		}).val('');
		$('#displayProgramName').bind('keyup', function(event){
			if(event.keyCode == 13){
				ProgrammeDisplayCatalog.searchExclusionProgram();
			}
		}).val('');
		ProgrammeDisplayCatalog.searchExclusionProgram();
	},
	searchExclusionProgram:function(){
		var selId = $('#selId').val();
		var code = $('#displayProgramCode').val().trim();
		var name = $('#displayProgramName').val().trim();
		var url = '/programme-display/searchExclusionProgram?id=' + encodeChar(selId) + '&code='+encodeChar(code)+'&name='+encodeChar(name);
		$('#exclusionGrid').datagrid({url:url,pageNumber:1});
	},
	openExclusionProgram:function(){
		var selId = $('#selId').val();
		var arrayParam = new Array();
		var param = new Object();
		param.name = 'id';
		param.value = selId;
		arrayParam.push(param);
		
		ProgrammeDisplayCatalog.openSearchStyleExclusionProgramEasyUIDialog("Mã CTTB", "Tên CTTB",
				"Chọn CTTB loại trừ", "/programme-display/searchExclusionProgramPopup", null,
				"displayProgramCode", "displayProgramName", arrayParam);
		return false;
	},
	deleteExclusionProgram:function(id){
		var dataModel = new Object();
		dataModel.id = id;
		Utils.addOrSaveData(dataModel, "/programme-display/deleteExclusionProgram", ProgrammeDisplayCatalog._xhrSave, 'errMsgExclusion', function(data){
			ProgrammeDisplayCatalog.searchExclusionProgram();
		},null,null,null,"Bạn có muốn xóa CTTB loại trừ này không?",function(data){
			$('#errMsgExclusion').html(data.errMsg).show();
			var tm = setTimeout(function(){
				$('#successMsgExclusion').html('').hide();
				clearTimeout(tm); 
			}, 3000);
		});
	},
	getShopGridUrl: function(displayProgramId,shopCode,shopName){
		return "/programme-display/searchshop?id="+ encodeChar(displayProgramId) + "&shopCode=" + encodeChar(shopCode) + "&shopName=" + encodeChar(shopName);
	},
	searchShop:function(){
		var code = $('#shopCode').val().trim();
		var name = $('#shopName').val().trim();
		var id = $('#selId').val();
		var url = ProgrammeDisplayCatalog.getShopGridUrl(id,code,name);
		$("#exGrid").treegrid({url:url});
		$('#errExcelMsg').html("").hide();
		ProgrammeDisplayCatalog._listShopIdEx = new Array();
		ProgrammeDisplayCatalog._listShopId = new Array();
	},
	openShopDialog:function(id){
		var selId = $('#selId').val();
		var arrParam = new Array();
 		var param = new Object();
 		param.name = 'shopId';
 		if(id != null && id != undefined){
 	 		param.value = id;
 		}
 		var param1 = new Object();
 		param1.name = 'focusId';
 		param1.value = selId;
 		arrParam.push(param);
 		arrParam.push(param1);
 		var param2 = new Object();
 		param2.name = 'stringProgram';
 		param2.value = 'display';
 		arrParam.push(param2);
 		CommonSearch.searchShopDisplayProgram(function(data){
 				// $('#promotionProgramCode').val(data.code);
 		},arrParam);
 		ProgrammeDisplayCatalog.bindFormat();
	},
	onCheckShop:function(id){
		 var isCheck = $('.easyui-dialog #check_'+id).attr('checked');
		 if($('.easyui-dialog #check_'+id).is(':checked')){
	    	ProgrammeDisplayCatalog._listShopId.push(id);
	    	var classParent = $('.easyui-dialog #check_'+id).attr('class');
			var flag = true;
			var count = 0;
			ProgrammeDisplayCatalog.recursionFindChildShopCheck(id); 
		}else{
			ProgrammeDisplayCatalog.removeArrayByValue(ProgrammeDisplayCatalog._listShopId,id);
			var classParent = $('.easyui-dialog #check_'+id).attr('class');
			var flag = true;
			var count = 0;
			ProgrammeDisplayCatalog.recursionFindChildShopUnCheck(id);  
			ProgrammeDisplayCatalog.recursionFindParentShopUnCheck(id);
		}
	},
	onCheckShopEx:function(id){
		var isCheck = $('#check_'+id).attr('checked');
	    if($('#check_'+id).is(':checked')){
	    	//ProgrammeDisplayCatalog._listShopIdEx.push(id);
	    	var classParent = $('#check_'+id).attr('class');
			var flag = true;
			var count = 0;
			ProgrammeDisplayCatalog.recursionFindChildShopDeleteCheck(id); 
		}else{
			//ProgrammeDisplayCatalog.removeArrayByValue(ProgrammeDisplayCatalog._listShopIdEx,id);
			var classParent = $('#check_'+id).attr('class');
			var flag = true;
			var count = 0;
			ProgrammeDisplayCatalog.recursionFindChildShopDeleteUnCheck(id);
			ProgrammeDisplayCatalog.recursionFindParentShopUnCheck(id); 
		}
	},
	createDisplayProgramShopMap:function(){
		var params = new Object();
		params.listShopId = ProgrammeDisplayCatalog._listShopId;
		params.id = $('#selId').val();
		if(ProgrammeDisplayCatalog._listShopId.length<=0){
			$('.easyui-dialog #errMsgDialog').html('Bạn chưa chọn đơn vị để thêm').show();
			return false;
		}
		Utils.addOrSaveData(params, '/programme-display/createDisplayProgramShopMap', null, 'errMsgDialog', function(data){
			if(data.error == undefined && data.errMsg == undefined){
				ProgrammeDisplayCatalog.searchShop();
				ProgrammeDisplayCatalog._listShopId = new Array();
				$('#searchStyleShopDisplay1').dialog('close');
				$('#errExcelMsg').html("").hide();
				$('#successMsgShop').html('Lưu dữ liệu thành công.').show();
				var tm = setTimeout(function(){
					$('#successMsgShop').html('').hide();
					clearTimeout(tm); 
				}, 3000);
			} 
		}, null, null, null, "Bạn có muốn thêm đơn vị này ?", null);
	},
	deleteDisplayProgramShopMap:function(shopId){
		var params = new Object();
		params.listShopId = ProgrammeDisplayCatalog._listShopIdEx;
		params.id = $('#selId').val();
		if(ProgrammeDisplayCatalog._listShopIdEx.length<=0){
			$('#errExcelMsg').html('Bạn chưa chọn đơn vị để xóa').show();
			return false;
		}
		Utils.addOrSaveData(params, '/programme-display/deleteDisplayProgramShopMap', null, 'errExcelMsg', function(data){
			ProgrammeDisplayCatalog.searchShop();
			$('#errExcelMsg').html("").hide();
			$('#successMsgShop').html('Xóa dữ liệu thành công.').show();
			var tm = setTimeout(function(){
				$('#successMsgShop').html('').hide();
				clearTimeout(tm); 
			}, 3000);
		}, null, null, null, "Bạn có muốn xóa đơn vị này ?", null);
	},
	exportExcelShopMap:function(){
		$('#divOverlay').show();
		$('#errExcelMsg').html('').hide();
	    var focusId = $('#selId').val();
		var shopCode = $('#shopCode').val().trim();
		var shopName = $('#shopName').val().trim();
		
		var data = new Object();
		data.id = focusId;
		data.shopCode = shopCode;
		data.shopName = shopName;
		
		var url = "/programme-display/exportDisplayProgramShopMap";
		CommonSearch.exportExcelData(data,url);
	},
	importExcelShopMap:function(){
 		$('#isView').val(0);
 		var options = { 
				beforeSubmit: ProgrammeDisplayCatalog.beforeImportExcel,   
		 		success:      ProgrammeDisplayCatalog.afterImportExcelShopMap,  
		 		type: "POST",
		 		dataType: 'html',
		 		data:({excelType:6,id:$('#selId').val()})
		 	}; 
		$('#importFrm').ajaxForm(options);
 		$('#importFrm').submit();
 		return false;
 	},
	bindFormat:function(){
		$('#searchStyleShopContainerGrid input[type=text]').each(function(){
				Utils.bindFormatOnTextfieldEx1($(this).attr('id'),Utils._TF_NUMBER,null,'#errMsgDialog','Số suất phải là số nguyên dương.');
			}); 
	},
	removeArrayByValue:function(arr,val){
		for(var i=0; i<arr.length; i++) {
	        if(arr[i] == val){
	        	arr.splice(i, 1);
		        break;
	        }
	    }
	},
	importDisplayProgram: function(){
		$.messager.confirm('Xác nhận','Bạn có muốn nhập từ file?',function(r){
			if(r){				
		 		$('#importFrm').submit();
			}
		});	
	},
	beforeImportExcel: function(){
		if(!previewImportExcelFile(document.getElementById("excelFile"))){
			return false;
		}
		//showLoading();
		$('#errExcelMsg').html('').hide();
		$('#successMsg1').html('').hide();
		showLoadingIcon();
		return true;
	},
	afterImportExcelDisplayProgram: function(responseText, statusText, xhr, $form){
		hideLoadingIcon();
		if (statusText == 'success') {				
	    	$("#responseDiv").html(responseText);
	    	var tokenHiddenFiled = $('#tokenHiddenFiled').val();
	    	if(tokenHiddenFiled!=null && tokenHiddenFiled!=undefined){
	    		Utils.setToken(tokenHiddenFiled.trim());
	    		$('#importTokenVal').val(tokenHiddenFiled.trim());
	    	}	
	    	if($('#errorExcel').html().trim() == 'true' || $('#errorExcelMsg').html().trim().length > 0){
	    		$('#errExcelMsg').html($('#errorExcelMsg').html()).show();
	    	} else {
	    		if($('#typeView').html().trim() == 'false'){
	    			var totalRow = parseInt($('#totalRow').html().trim());
	    			var numFail = parseInt($('#numFail').html().trim());
	    			var fileNameFail = $('#fileNameFail').html();
	    			var mes = format(msgErr_result_import_excel,(totalRow - numFail),numFail);		    		
	    			if(numFail > 0){
	    				mes+= ' <a href="'+ fileNameFail +'">Xem chi tiết lỗi</a>';
	    			}
	    			if($('#excelFile').length!=0 && $('#fakefilepc').length!=0){//xóa link khi import
	    				try{$('#excelFile').val('');$('#fakefilepc').val('');}catch(err){}
	    			}
	    			$('#errExcelMsg').html(mes).show();
	    			$("#listCTTBGrid").datagrid("reload");

	    		}
	    		}
	    	}
	},
	afterImportExcelShopMap: function(responseText, statusText, xhr, $form){
		hideLoadingIcon();
		if (statusText == 'success') {				
			$("#responseDiv").html(responseText);
			var tokenHiddenFiled = $('#tokenHiddenFiled').val();
			if(tokenHiddenFiled!=null && tokenHiddenFiled!=undefined){
				Utils.setToken(tokenHiddenFiled.trim());
				$('#importTokenVal').val(tokenHiddenFiled.trim());
			}	
			if($('#errorExcel').html().trim() == 'true' || $('#errorExcelMsg').html().trim().length > 0){
				$('#errExcelMsg').html($('#errorExcelMsg').html()).show();
			} else {
				if($('#typeView').html().trim() == 'false'){
					var totalRow = parseInt($('#totalRow').html().trim());
					var numFail = parseInt($('#numFail').html().trim());
					var fileNameFail = $('#fileNameFail').html();
					var mes = format(msgErr_result_import_excel,(totalRow - numFail),numFail);		    		
					if(numFail > 0){
						mes+= ' <a href="'+ fileNameFail +'">Xem chi tiết lỗi</a>';
					}
					if($('#excelFile').length!=0 && $('#fakefilepc').length!=0){//xóa link khi import
						try{$('#excelFile').val('');$('#fakefilepc').val('');}catch(err){}
					}
					$('#errExcelMsg').html(mes).show();
				}
    		}
    	}
	},
	importExcelCustomer : function(){ 
		/*$('.easyui-dialog #errExcelMsg').html('').hide();
		if($('.easyui-dialog #excelFileCus').val() ==  ""){
			$('.easyui-dialog #errExcelMsg').html("Vui lòng chọn file Excel").show();
			return false;
		}*/
		var id = $('#selId').val();
		var month = $('#month1').val();
		var shopId = $('#shopTreeId .combo-value').val();
		/*var options = { 
			beforeSubmit: ProgrammeDisplayCatalog.beforeImportExcel,   
	 		success:      ProgrammeDisplayCatalog.afterImportExcel,
	 		type: "POST",
	 		dataType: 'html',
	 		data:({id:id,month:month,shopId:shopId})
		}; 
		$.messager.confirm('Xác nhận','Bạn có muốn nhập từ file?',function(r){
			if(r){				
				$('.easyui-dialog #importFrmCus').ajaxForm(options);
		 		$('.easyui-dialog #importFrmCus').submit();
			}
		});	
 		return false;*/
		$('.ErrorMsgStyle').hide();
		VTUtilJS.importExcelUtils(function(data) {
			$("#customerGrid123").datagrid("reload");
			$(".easyui-dialog #errExcelMsg").html(data.message).show();
			$("#fakefilepcCustomer").val("");
			$("#excelFileCus").val("");
		}, "importFrmCus", "excelFileCus", null, "errExcelMsg", {id:id,month:month,shopId:shopId});
		return false;
	},
	importExcelStaff : function() {
		$('#inmonth').val($('date').val);
		$('.ErrorMsgStyle').hide();
		if ($('#dateStrExcel').val() ==  "") {
			$('#errExcelMsg').html("Vui lòng chọn tháng cần nhập file").show();
			$('#dateStrExcel').focus();
			return false;
		}
		/*if($('#excelFile').val() ==  ""){
			$('#errExcelMsg').html("Vui lòng chọn file Excel").show();
			return false;
		}
		var options = {
			beforeSubmit : ProgrammeDisplayCatalog.beforeImportExcel,
			success : ProgrammeDisplayCatalog.afterImportExcelStaff,
			type : "POST",
			dataType : 'html'
		};
		$('#easyuiPopup #importFrm').ajaxForm(options);
		$.messager.confirm('Xác nhận','Bạn có muốn nhập từ file?',function(r){
			if(r){				
				$('#easyuiPopup #importFrm').submit();
			}
		});		
		return false;*/
		$('.ErrorMsgStyle').hide();
		VTUtilJS.importExcelUtils(function(data) {
			$("#staffGrid").datagrid("reload");
			$("#errExcelMsg").html(data.message).show();
			$("#fakefilepc").val("");
			$("#excelFile").val("");
		}, "importFrm", "excelFile", null, "errExcelMsg");
		return false;
	},
	updateRownumWidthForJqGridEX1:function(parentId,colReduceWidth){	
		var pId = '';
		if(parentId!= null && parentId!= undefined){
			pId = parentId + ' ';
		}
		var lastValue=$(pId + '.datagrid-cell-rownumber').last().text().trim().length;
		var widthSTT = $('.datagrid-cell-rownumber').width();
		var s = $('.datagrid-header-row td[field='+colReduceWidth+'] div').width();
		if(ProgrammeDisplayCatalog._lstSize != null && ProgrammeDisplayCatalog._lstSize != undefined){ 
			s = ProgrammeDisplayCatalog._lstSize[0];
		}
		if(lastValue > 0){
			var extWidth = 25;
			if(lastValue > 2){
				extWidth += (lastValue - 2) * 9;
			}
			var value = extWidth - widthSTT;
			s = s - value;
			$(pId + '.datagrid-cell-rownumber').css('width',extWidth);
			$(pId + '.datagrid-header-rownumber').css('width',extWidth);
			$(parentId + ' .datagrid-row').each(function(){
				$(parentId + ' .datagrid-header-row td[field='+colReduceWidth+'] div').width(s);
				$(parentId + ' .datagrid-row td[field='+colReduceWidth+'] div').width(s);
			});
			
		}
	},
	exportExcelCTTB: function() {
		$('.ErrorMsgStyle').hide();
		var dataModel = new Object();
		if($('#totalRowGrid').val() == 0){
			$('#errCustomerMsg').html("Không có dữ liệu để xuất").show();
			return false;
		}else{
			$.messager.confirm('Xác nhận', 'Bạn có muốn xuất file này.', function(r){
				if (r){
					var shopKendo = $("#shop").data("kendoMultiSelect");
		            var lstShopId = shopKendo.value();
		            if(lstShopId != null && lstShopId != undefined){
						dataModel.listShopIdString = lstShopId.toString();
		            }

				dataModel.id = $('#displayProgram').val().trim();
				dataModel.month = $('#month1').val().trim();
				ReportUtils.exportReport('/programme-display/customer/export', dataModel, 'errCustomerMsg');
				}
			});
		}
		return false;
	},
	saveProductDlgDS:function(){
		var params = new Object();
		params.id = $('#selId').val().trim();
		var groupId = ProgrammeDisplayCatalog.groupIdDS;
		var groupType = ProgrammeDisplayCatalog.groupTypeDS;
		if (groupType == undefined || groupType == null){
			groupType = $('#tmpGroupType').val();
		}
		if (groupId == undefined || groupId == null){
			groupId = $('#tmpGroupId').val();
		} 
		params.typeGroup = groupType;
		params.groupId = groupId;
		if(groupType==2){
			var listChecked = ProgrammeDisplayCatalog.lstProductId.keyArray;  //$('#searchProductAmountGrid').datagrid('getChecked');
	    	for(var i = 0; i < listChecked.length; i++) {
	    		var convfact = $('#product-'+listChecked[i]).val();
	    		if(convfact == null || convfact == '' || isNaN(convfact)) {
	    			
	    		} else {
	    			ProgrammeDisplayCatalog.lstConvfact.put(listChecked[i], convfact);
	    		}
	    	}
	    	if (ProgrammeDisplayCatalog.lstConvfact == null ||
	    			ProgrammeDisplayCatalog.lstConvfact.keyArray.length == 0){
	    		$('#errProductMsgSearch').html("Bạn chưa chọn sản phẩm nào.").show();
	    		return false;
	    	}
			params.lstConvfact = ProgrammeDisplayCatalog.lstConvfact.valArray;
    		params.lstProductId = ProgrammeDisplayCatalog.lstProductId.keyArray;        		
    		Utils.addOrSaveRowOnGrid(params, "/programme-display/change-amount-product", ProgrammeDisplayCatalog._xhrSave, 'amountGrid','errMsgSearch', function(data){
    			showSuccessMsg('successMsgProductDlg',data);
    			var selId = $('#selId').val();
    			var url = '';		
    			url = '/programme-display/search-product?id=' + encodeChar(selId) + '&typeGroup=4';
//        			ProgrammeDisplayCatalog.dataGridProductGroup('cbGrid',url,ProgrammeDisplayCatalog.DISPLAY);
    			var subGridId = $('#searchProductEasyUIDialog #subGridId').val();
    			ProgrammeDisplayCatalog.lstConvfact = null;
    			$('#tmpCheckedListFlag').val("");
    			$('#searchProductEasyUIDialog').window('close');
    			$('#'+subGridId).datagrid('reload');
    		},null);
		
		}else{
			if (ProgrammeDisplayCatalog.lstProductId == null ||
	    			ProgrammeDisplayCatalog.lstProductId.keyArray.length == 0){
	    		$('#errProductMsgSearch').html("Bạn chưa chọn sản phẩm nào.").show();
	    		return false;
	    	}
			params.lstProductId = ProgrammeDisplayCatalog.lstProductId.keyArray;
			Utils.addOrSaveRowOnGrid(params, "/programme-display/change-amount-product", ProgrammeDisplayCatalog._xhrSave, 'amountGrid','errMsgSearch', function(data){
    			showSuccessMsg('successMsgProductDlg',data);
    			var selId = $('#selId').val();
    			var url = '';		
    			url = '/programme-display/search-product?id=' + encodeChar(selId) + '&typeGroup=4';
//        			ProgrammeDisplayCatalog.dataGridProductGroup('cbGrid',url,ProgrammeDisplayCatalog.DISPLAY);
    			var subGridId = $('#searchProductEasyUIDialog #subGridId').val();
    			ProgrammeDisplayCatalog.lstProductId = null;
    			$('#searchProductEasyUIDialog').window('close');
    			$('#'+subGridId).datagrid('reload');
    		},null);
		}
	},
	openSearchProductDialog: function(groupType, groupId, subGridId){
		ProgrammeDisplayCatalog.groupIdDS = 	groupId;	
		ProgrammeDisplayCatalog.groupTypeDS = groupType;
		var html = $('#searchProductEasyUIDialog').html();
		var productName = $("#seachProductName").val().trim();
		var productCode = $("#seachProductCode").val().trim();
		var displayProgrameId = $("#selId").val().trim();
		
		if (groupType == undefined || groupType == null)
			groupType = $("#tmpGroupType").val();
		$("#tmpGroupType").val(groupType);
		if (groupId == undefined || groupId == null){
			groupId = $('#tmpGroupId').val();
		}
		if (subGridId == undefined || subGridId == null ){
			subGridId = $('#tmpSubGridId').val();
		}
		$('#tmpSubGridId').val(subGridId);
		$("#tmpGroupId").val(groupId);
		$('#searchProductDialogDiv').show();

		if (ProgrammeDisplayCatalog.lstProductId == null || ProgrammeDisplayCatalog.lstProductId.keyArray.length == 0){
			ProgrammeDisplayCatalog.lstProductId = new Map();
		}
		if (ProgrammeDisplayCatalog.lstConvfact == null || ProgrammeDisplayCatalog.lstConvfact.keyArray.length == 0){
			ProgrammeDisplayCatalog.lstConvfact = new Map();
		}
		if ($('#tmpCheckedListFlag').val() == "1"){
			var lstTmpGridRows = $('#searchProductGrid').datagrid('getChecked');
			if (lstTmpGridRows == undefined || lstTmpGridRows == null || lstTmpGridRows.length == 0){
				// do nothing
			}else{
				for(var i = 0; i < lstTmpGridRows.length; i++) {
		    		var convfact = $('#product-'+lstTmpGridRows[i].id).val();
		    		if(convfact == null || convfact == '' || isNaN(convfact)) {
		    			
		    		} else {
		    			ProgrammeDisplayCatalog.lstConvfact.put(lstTmpGridRows[i].id, convfact);
		    		}
		    	}
			}
		}
		lstIndex = new Map();
		$('#searchProductEasyUIDialog').dialog({
			 	title: 'Chọn sản phẩm thêm vào nhóm',  
		        closed: false,  
		        cache: false,  
		        modal: true,
		        width : 580,
		        height :500,
		        onOpen: function(){
		        	$('#searchProductEasyUIDialog #seachProductCode,#searchProductEasyUIDialog #seachProductName').bind('keypress',function(event){
		        		if(event.keyCode == keyCodes.ENTER){
		        			$('#searchProductEasyUIDialog #btnSearchProduct').click();
		        		}
		        	});
		        	$('#searchProductEasyUIDialog #subGridId').val(subGridId);
		        	$('#searchProductContainerGrid').html('<table id="searchProductGrid" style="width: 520px;"></table>');
		        	if(groupType == 2) {
		        		$("#searchProductEasyUIDialog #btnSearchProduct").click(function(){
		        			$('#searchProductAmountGrid').datagrid('load',{
		        				productName: $('#seachProductName').val().trim(),
		        				productCode: $('#seachProductCode').val().trim()
		        				});
		        			});
		        		$('#searchProductAmountGrid').datagrid({
		        			url : '/programme-display/search-group-product?id='+displayProgrameId+'&typeGroup='+groupType,
							autoRowHeight : true,
							rownumbers : true, 
							//checkOnSelect :true,
							pagination:true,
							rowNum : 10,
							scrollbarSize : 0,
							fitColumns:true,
							pageList  : [10,20,30],
							queryParams: {
								productName: productName,
								productCode: productCode
							},
							width : ($('.easyui-dialog #searchProductContainerGrid').width()),
						    columns:[[  
						        {field:'catCode',title:"Ngành",align:'left', width:110, sortable : false,resizable : false,formatter:function(value,row,index){
						        	return Utils.XSSEncode(row.catCode);
						        }},  
						        {field:'productCode',title:"Mã sản phẩm",align:'left', width:350, sortable : false,resizable : false,formatter:function(value,row,index){
						        	return Utils.XSSEncode(row.productCode);
						        }},  
						        {field:'productName', title:'Tên sản phẩm', align:'left',width:350, sortable : false,resizable : false,formatter:function(value,row,index){
						        	return Utils.XSSEncode(row.productName);
						        }},
						        {field:'convfact',title:'Quy đổi', align: 'right', width: 100, sortable : false, resizable: false, formatter: function(value,row,index){
						        	return '<input type="text" id="product-'+row.productId+'" value="'+row.convfact+'"  maxlength="7">';
						        }},
								{field: 'id',checkbox:true}
						    ]],
						    onBeforeLoad : function() {
						    	var listChecked = $('#searchProductAmountGrid').datagrid('getChecked');
						    	for(var i = 0; i < listChecked.length; i++) {
						    		var convfact = $('#product-'+listChecked[i].id).val();
						    		if(convfact == null || convfact == '' || isNaN(convfact)) {
						    			
						    		} else {
						    			ProgrammeDisplayCatalog.lstConvfact.put(listChecked[i].id, convfact);
						    		}
						    	}
						    },
						    onLoadSuccess :function(){
						    	$('#errProductMsgSearch').html("");
						    	var rows = $('#searchProductAmountGrid').datagrid('getRows');
						    	for(var i = 0; i < rows.length; i++) {
						    		//$('#searchProductGrid').datagrid('beginEdit', i);
						    		if(ProgrammeDisplayCatalog.lstConvfact.get(rows[i].id) != null) {
						    			$('#searchProductAmountGrid').datagrid('checkRow', i);
						    			$('#product-'+rows[i].id).val(ProgrammeDisplayCatalog.lstConvfact.get(rows[i].id));;
						    		}
						    	}
						    	$('#seachProductCode').focus();
						    	$('#tmpCheckedListFlag').val("1");
						    	$('.datagrid-header-check input').removeAttr("checked");
						    },
						    onCheck:function(index, row){
						    	$('#errProductMsgSearch').html("");
						    	ProgrammeDisplayCatalog.lstProductId.put(row.productId, row);
						    },
						    onUncheck:function(index, row){
						    	ProgrammeDisplayCatalog.lstProductId.remove(row.productId);
						    },
						    onCheckAll:function(rows){
						    	for(var i = 0;i<rows.length;i++){
						    		var index = i;
						    		var row = rows[i];
						    		ProgrammeDisplayCatalog.lstProductId.put(row.productId, row);
						    	}
						    },
						    onUncheckAll:function(rows){
						    	for(var i = 0;i<rows.length;i++){
						    		var index = i;
						    		var row = rows[i];
						    		ProgrammeDisplayCatalog.lstProductId.remove(row.productId);
						    	}
						    }
		        		});
		        	} else {
		        		$("#searchProductEasyUIDialog #btnSearchProduct").click(function(){
		        			$('#searchProductAmountGrid').datagrid('load',{
		        				productName: $('#seachProductName').val().trim(),
		        				productCode: $('#seachProductCode').val().trim()
		        				});
		        			});
		        		$('#searchProductAmountGrid').datagrid({
		        			url : '/programme-display/search-group-product?id='+displayProgrameId+'&typeGroup='+groupType,
							autoRowHeight : true,
							rownumbers : true, 
							//checkOnSelect :true,
							pagination:true,
							rowNum : 10,
							scrollbarSize : 0,
							fitColumns:true,
							pageList  : [10,20,30],
							queryParams: {
								productName: productName,
								productCode: productCode
							},
							width : ($('.easyui-dialog #searchProductContainerGrid').width()),
						    columns:[[  
						        {field:'catCode',title:"Ngành",align:'left', width:110, sortable : false,resizable : false,formatter:function(value,row,index){
						        	return Utils.XSSEncode(row.catCode);
						        }},  
						        {field:'productCode',title:"Mã sản phẩm",align:'left', width:350, sortable : false,resizable : false,formatter:function(value,row,index){
						        	return Utils.XSSEncode(row.productCode);
						        }},  
						        {field:'productName', title:'Tên sản phẩm', align:'left',width:350, sortable : false,resizable : false,formatter:function(value,row,index){
						        	return Utils.XSSEncode(row.productName);
						        }},
								{field: 'id',checkbox:true}
						    ]],
						    onLoadSuccess :function(){
						    	$('#errProductMsgSearch').html("");
						    	var rows = $('#searchProductAmountGrid').datagrid('getRows');
						    	for(var i = 0; i < rows.length; i++) {
						    		if(ProgrammeDisplayCatalog.lstProductId.get(rows[i].id)) {
						    			$('#searchProductAmountGrid').datagrid('checkRow', i);
						    		}
						    	}
						    	$('#seachProductCode').focus();
						    	$('.datagrid-header-check input').removeAttr("checked");
						    },
						    onCheck:function(index, row){
						    	$('#errProductMsgSearch').html("");
						    	ProgrammeDisplayCatalog.lstProductId.put(row.productId, row);
						    },
						    onUncheck:function(index, row){
						    	ProgrammeDisplayCatalog.lstProductId.remove(row.productId);
						    },
						    onCheckAll:function(rows){
						    	for(var i = 0;i<rows.length;i++){
						    		var index = i;
						    		var row = rows[i];
						    		ProgrammeDisplayCatalog.lstProductId.put(row.productId, row);
						    	}
						    },
						    onUncheckAll:function(rows){
						    	for(var i = 0;i<rows.length;i++){
						    		var index = i;
						    		var row = rows[i];
						    		ProgrammeDisplayCatalog.lstProductId.remove(row.productId);
						    	}
						    }
		        		});
		        	}
		        },
		        onClose: function() {
		        	$('#searchProductEasyUIDialog #seachProductCode,#searchProductEasyUIDialog #seachProductName').unbind('keypress');
		        	$('#searchProductEasyUIDialog').html(html);
		        	$('#searchProductDialogDiv').hide();
		        	ProgrammeDisplayCatalog.lstConvfact = null;
					ProgrammeDisplayCatalog.lstProductId = null;
					$('#tmpCheckedListFlag').val("");
		        }
		});
	},
	openPopupImportCustomer: function(){
		$('#fakefilepcCustomer').val('');
		$('#easyuiPopup1').show();
		$('.easyui-dialog #excelFileCus').val('');
		$('.ErrorMsgStyle').hide();
		var shopId = $('#shopTreeId .combo-value').val();
		var month = $('#month1').val();
		var id = $('#selId').val();
		ReportUtils._callBackAfterImport = function(){
			$('#customerGrid123').datagrid('load',{shopId:shopId,month:month,id:id,pageNumber:1});
			ReportUtils._callBackAfterImport=null;
		};
		$('#easyuiPopup1').dialog('open');
	},
	searchProductPressKey: function(e){
		if (typeof e == undefined && window.event) { e = window.event; }
        if (e.keyCode == 13)
        {
            document.getElementById('btnSearchProduct').click();
        }
	},
	uncheckAll:function(){
		/*if($('#checkDeleteAll').is(':checked')){
			$('#customerGrid123').datagrid('checkAll');
			$('#checkDeleteAll').prop('checked', true);
		}else{
			ProgrammeDisplayCatalog._lstCustomerId = new Map();
			$('#customerGrid123').datagrid('uncheckAll');
		}*/
		if (ProgrammeDisplayCatalog._lstProduct.size() > 0) {
			if($('#checkDeleteAll').is(':checked')){
				$('#customerGrid123').datagrid('uncheckAll');
				$('#checkDeleteAll').attr('checked', "checked");
			}
		}
	},
	deleteCustomerDisplayProgram:function(){
		$('.ErrorMsgStyle').hide();
		var deleteAll = $('#checkDeleteAll').attr('checked');
		var month = $('#filterMonth').val();
		var arrMonth = month.split('/');
		var monthTxt = arrMonth[0];
		var yearTxt = arrMonth[1];
		var now = new Date();
		var cYear = now.getFullYear();
		var cMonth = now.getMonth() + 1;
		if(monthTxt.length == 2){
			  monthTxt = parseInt(monthTxt);
			}
		if(parseInt(yearTxt) > cYear ||( parseInt(monthTxt) < cMonth && parseInt(yearTxt) == cYear ) ||(parseInt(monthTxt) > cMonth && parseInt(yearTxt) < cYear) ){
			msg = "Không được xóa KH tham gia CTTB của tháng trước tháng hiện tại.";
			$('#errCustomerMsg').html(msg).show();
			return false;
		}
		if(deleteAll == "checked"){
			var rows = $('#customerGrid123').datagrid('getRows');
			if(rows != null && rows != undefined && rows.length >0){
				var length = rows.length;
				for(var i =0;i<length;i++){
					var temp = rows[i].id;
					if(temp != null && temp != undefined){
						ProgrammeDisplayCatalog._lstCustomerId.put(temp,temp);
					}
				}
			}else{
				$('#errCustomerMsg').html('Không có dữ liệu để xóa.').show();
				return false;
			}
			var params = new Object();
			var shopKendo = $("#shop").data("kendoMultiSelect");
	        var lstShopId = new Array();
	        lstShopId = shopKendo.value();
			var month = $('#month1').val();
			if(lstShopId != null && lstShopId != undefined){
				params.listShopIdString = lstShopId.toString();
			}

			params.month = month;
			params.id = $('#displayProgram').val();		
			params.isCheckAll = $('#checkDeleteAll').is(':checked');
			
			params.lstCustomerId = ProgrammeDisplayCatalog._lstCustomerId.keyArray;
			Utils.addOrSaveData(params, '/programme-display/customer/deleteCustomer', null, 'errCustomerMsg', function(data){						
				$('#errCustomerMsg').html('').hide();
				$('#successCustomerMsg').html("Xóa dữ liệu thành công").show();
				var tm = setTimeout(function(){
					$('#successCustomerMsg').html('').hide();
            		clearTimeout(tm);	                		
            	}, 3000);
				ProgrammeDisplayCatalog._lstCustomerId = new Map();
				ProgrammeDisplayCatalog.searchCustomerTab();
			}, null, null, null, "Bạn có muốn xóa khách hàng của CTTB này ?", null);
		}else{
			if(ProgrammeDisplayCatalog._lstProduct == null || ProgrammeDisplayCatalog._lstProduct == undefined || ProgrammeDisplayCatalog._lstProduct.size() <= 0)
			{
				$('#errCustomerMsg').html('Bạn chưa chọn khách hàng để xóa.').show();
				return false;
			}
			var params = new Object();
			params.lstCustomerId = ProgrammeDisplayCatalog._lstProduct.keyArray; 
			Utils.addOrSaveData(params, '/programme-display/customer/deleteCustomer', null, 'errCustomerMsg', function(data){						
				$('#errCustomerMsg').html('').hide();
				$('#successCustomerMsg').html("Xóa dữ liệu thành công").show();
				var tm = setTimeout(function(){
					$('#successCustomerMsg').html('').hide();
            		clearTimeout(tm);	                		
            	}, 3000);
				ProgrammeDisplayCatalog._lstProduct = new Map();
				ProgrammeDisplayCatalog.searchCustomerTab();
			}, null, null, null, "Bạn có muốn xóa khách hàng của CTTB này ?", null);
		}
	},
	getGridUrlVNM: function(code,name,status,fDate,tDate){		
		return "/programme-display/searchvnm?code=" + encodeChar(code) + "&name=" + encodeChar(name) + "&status=" + status + "&fromDate=" + encodeChar(fDate) + "&toDate=" + encodeChar(tDate);
	},	
	searchVNM: function(){
		$('#errMsg').html('').hide();
		$('#errExcelMsg').html('').hide();
		var code = $('#code').val().trim();
		var name = $('#name').val().trim();
		var status = $('#status').val().trim();;
		if($('#permissionUser').val() == 'true' ){
			status = $('#status').val().trim();
		}
		
		var msg=''; 
		var startDate = $('#fDate').val().trim();
		var endDate = $('#tDate').val().trim();
		if (startDate == "__/__/____") {
			startDate = "";
		}
		if (endDate == "__/__/____") {
			endDate = "";
		}
		if (startDate.length > 0 && !Utils.isDate(startDate)) {
			msg = 'Từ ngày không hợp lệ, vui lòng nhập theo định dạng dd/MM/yyyy';
			$('#fDate').focus();
		}
		if(msg.length == 0){
			if(endDate.length > 0 && !Utils.isDate(endDate)){
				msg = 'Đến ngày không hợp lệ, vui lòng nhập theo định dạng dd/MM/yyyy';
				$('#tDate').focus();
			}
		}
		if(msg.length == 0){
			if(startDate.length > 0 && endDate.length > 0){
				if(!Utils.compareDate(startDate, endDate)){
					msg = 'Giá trị Từ ngày phải nhỏ hơn hoặc bằng giá trị Đến ngày.';				
				}
			}
		}
		if(msg.length > 0){
			$('#errMsg').html(msg).show();
			return false;
		}
		var params = new Object();
		params.code = $('#code').val().trim();
		params.name = $('#name').val().trim();
		params.fromDate = startDate;
		params.toDate = endDate;
		params.status = $('#status').val().trim();
		//var url = ProgrammeDisplayCatalog.getGridUrlVNM(code,name,status,fDate,tDate);
		$("#listCTTBGridVNM").datagrid('load',params);
		var tm = setTimeout(function() {
			$('#code').focus();
		}, 500);
		return false;
	},
	recursionFindChildShopCheck:function(id){
		if(id == null || id == undefined){
			return;
		}
    	$.ajax({
    		type : "POST",
			url : '/commons/shopMap/getChildShopId',
			data: ({shopId:id}),
			dataType: "json",
			success : function(data) {
				if(data != null && data != undefined && data.listShopId != null && data.listShopId != undefined){
					var length = data.length;
					var arrayShopId = data.listShopId;
					for(var i=0;i<length;i++){
						ProgrammeDisplayCatalog._listShopId.push(data.listShopId[i]);
					}
					ProgrammeDisplayCatalog.loopCheck(arrayShopId);
				} 
			}
    	});
	},
	recursionFindChildShopUnCheck:function(id){
		if(id == null || id == undefined){
			return;
		}
		$.ajax({
    		type : "POST",
			url : '/commons/shopMap/getChildShopId',
			data: ({shopId:id}),
			dataType: "json",
			success : function(data) {
				if(data != null && data != undefined && data.listShopId != null && data.listShopId != undefined){
					var flag = true;
					var length = data.length;
					var arrayShopId = data.listShopId;
					for(var i=0;i<length;i++){
						ProgrammeDisplayCatalog.removeArrayByValue(ProgrammeDisplayCatalog._listShopId,data.listShopId[i]);
					}
					ProgrammeDisplayCatalog.loopUnCheck(arrayShopId); 
				}
			}
    	});
	},
	recursionFindParentShopUnCheck:function(id){
		if(id == null || id == undefined){
			return;
		}
		$.ajax({
    		type : "POST",
			url : '/commons/shopMap/getParentShop',
			data: ({shopId:id}),
			dataType: "json",
			success : function(data) {
				if(data != null && data != undefined && data.parentShopId != null && data.parentShopId != undefined){
					var flag = true;
					$('.class_'+data.parentShopId).each(function(){
						if($(this).is(':checked') || $(this).is(':disabled')){
							flag = false;
						}
					});
					$('#check_'+data.parentShopId).removeAttr('checked');
					$('.easyui-dialog #check_'+data.parentShopId).removeAttr('checked');
					ProgrammeDisplayCatalog.recursionFindParentShopUnCheck(data.parentShopId);
				}
			}
    	});
	},
	loopCheck:function(arrayShopId){
		if(arrayShopId == null || arrayShopId == undefined || arrayShopId.length == 0){
			return;
		}
		$('.easyui-dialog #check_'+arrayShopId[0]).attr('checked','checked');
		ProgrammeDisplayCatalog.removeArrayByValue(arrayShopId,arrayShopId[0]);
		ProgrammeDisplayCatalog.loopCheck(arrayShopId);
	},
	loopUnCheck:function(arrayShopId){
		if(arrayShopId == null || arrayShopId == undefined || arrayShopId.length == 0){
			return;
		}
		$('.easyui-dialog #check_'+arrayShopId[0]).removeAttr('checked');
		ProgrammeDisplayCatalog.removeArrayByValue(arrayShopId,arrayShopId[0]);
		ProgrammeDisplayCatalog.loopUnCheck(arrayShopId);
	},
	loopCheckDelete:function(arrayShopId){
		if(arrayShopId == null || arrayShopId == undefined || arrayShopId.length == 0){
			return;
		}
		$('#check_'+arrayShopId[0]).attr('checked','checked');
		ProgrammeDisplayCatalog.removeArrayByValue(arrayShopId,arrayShopId[0]);
		ProgrammeDisplayCatalog.loopCheckDelete(arrayShopId);
	},
	loopUnCheckDelete:function(arrayShopId){
		if(arrayShopId == null || arrayShopId == undefined || arrayShopId.length == 0){
			return;
		}
		$('#check_'+arrayShopId[0]).removeAttr('checked');
		ProgrammeDisplayCatalog.removeArrayByValue(arrayShopId,arrayShopId[0]);
		ProgrammeDisplayCatalog.loopUnCheckDelete(arrayShopId);
	},
	recursionFindChildShopDeleteCheck:function(id){
		if(id == null || id == undefined){
			return;
		}
    	$.ajax({
    		type : "POST",
			url : '/commons/shopMap/getChildShopId',
			data: ({shopId:id}),
			dataType: "json",
			success : function(data) {
				if(data != null && data != undefined && data.listShopId != null && data.listShopId != undefined){
					var length = data.length;
					var arrayShopId = data.listShopId;
					if(data.listShopId != null && data.listShopId != undefined && data.listShopId.length==0){
						ProgrammeDisplayCatalog._listShopIdEx.push(id);
					}
					for(var i=0;i<length;i++){
						ProgrammeDisplayCatalog._listShopIdEx.push(data.listShopId[i]);
					}
					ProgrammeDisplayCatalog.loopCheckDelete(arrayShopId);
				} 
			}
    	});
	},
	recursionFindChildShopDeleteUnCheck:function(id){
		if(id == null || id == undefined){
			return;
		}
		$.ajax({
    		type : "POST",
			url : '/commons/shopMap/getChildShopId',
			data: ({shopId:id}),
			dataType: "json",
			success : function(data) {
				if(data != null && data != undefined && data.listShopId != null && data.listShopId != undefined){
					var flag = true;
					var length = data.length;
					var arrayShopId = data.listShopId;
					if(data.listShopId != null && data.listShopId != undefined && data.listShopId.length==0){
						ProgrammeDisplayCatalog.removeArrayByValue(ProgrammeDisplayCatalog._listShopIdEx,id);
					}
					for(var i=0;i<length;i++){
						ProgrammeDisplayCatalog.removeArrayByValue(ProgrammeDisplayCatalog._listShopIdEx,data.listShopId[i]);
					}
					ProgrammeDisplayCatalog.loopUnCheckDelete(arrayShopId); 
				}
			}
    	});
	},
	openSearchStyleExclusionProgramEasyUIDialog : function(codeText, nameText, title, url, codeFieldText, nameFieldText, arrParam) {
		$('#searchStyleExclusionDialogDiv').css("visibility", "visible");
		var html = $('#searchStyleExclusionDialogDiv').html();
		$('#searchStyleExclusionDialog').dialog({  
	        title: title,  
	        closed: false,  
	        cache: false,  
	        modal: true,
	        width : 600,
	        height : 'auto',
	        onOpen: function(){	        	
				var tabindex = -1;
				$('.InputTextStyle, select, input, button , li , tr, td, th, label, ul, a, img').each(function () {
					if (this.type != 'hidden') {
						$(this).attr("tabindex", tabindex);
						tabindex -=1;
					}
				});								
				$('.easyui-dialog #seachStyleExclusionCode').focus();
				$('.easyui-dialog #searchStyleExclusionUrl').val(url);
				$('.easyui-dialog #errMsgSearchExclusion').html("").hide();
				
				$('.easyui-dialog #searchStyle1CodeText').val(codeFieldText);
				$('.easyui-dialog #searchStyle1NameText').val(nameFieldText);			
				var codeField = 'code';
				var nameField = 'name';
				if (codeFieldText != null && codeFieldText != undefined && codeFieldText.length > 0) {
					codeField = codeFieldText;
				}
				if (nameFieldText != null&& nameFieldText != undefined && nameFieldText.length > 0) {
					nameField = nameFieldText;
				}

				$('.easyui-dialog #seachStyle1CodeLabel').html(codeText);
				$('.easyui-dialog #seachStyle1NameLabel').html(nameText);
				$('#searchStyleExclusionGrid').show();
				$('#searchStyleExclusionGrid').datagrid({
					url : ProgrammeDisplayCatalog.getSearchStyleExclusionMapGridUrl(url, '', '', arrParam),
					autoRowHeight : true,
					rownumbers : true, 
					checkOnSelect :true,
					pagination:true,
					rowNum : 10,
					pageNumber: 1,
					scrollbarSize : 0,
					fitColumns:true,
					pageList  : [10,20,30],
					width : ($('#searchStyleExclusionContainerGrid').width()),
				    columns:[[  
				        {field:'displayProgramCode',title:codeText,align:'left', width:170, sortable : false,resizable : false,formatter:function(value,row,index){
				        	return Utils.XSSEncode(row.displayProgramCode);
				        }},  
				        {field:'displayProgramName',title:nameText,align:'left', width:370, sortable : false,resizable : false,formatter:function(value,row,index){
				        	return Utils.XSSEncode(row.displayProgramName);
				        }},  
				        {field: 'id', checkbox:true, align:'center', width:80,sortable : false,resizable : false, formatter: function(value, row, index){
				        	return Utils.XSSEncode(row.id);
				        }}
				    ]],
				    onLoadSuccess :function(data){
				    	$('.datagrid-header-rownumber').html('STT');
				    	tabindex = 1;
				    	$('.easyui-dialog input,.easyui-dialog select,.easyui-dialog button').each(function () {
				    		if (this.type != 'hidden') {
				    			$(this).attr("tabindex", '');
				    			tabindex++;
				    		}
				    	});
				    	if(data.rows.length == 0){
				    		$('.datagrid-header-check input').removeAttr('checked');
				    	}
				    	var originalSize = $('.easyui-dialog .datagrid-header-row td[field=displayProgramCode] div').width();
				    	if(originalSize != null && originalSize != undefined){
				    		ProgrammeDisplayCatalog._listExclusionDialogSize.push(originalSize);
				    	}
				    	$('input[name="id"]').each(function(){
				    		var temp = ProgrammeDisplayCatalog._listExclusion.get($(this).val());
				    		if(temp!=null) $(this).attr('checked','checked');
			 			});
			 	    	var length = 0;
			 	    	$('input[name="id"]').each(function(){
			 	    		if($(this).is(':checked')){
			 	    			++length;
			 	    		}	    		
			 	    	});	    	
			 	    	if(data.rows.length==length){
			 	    		$('.datagrid-header-check input').attr('checked','checked');
			 	    	}else{
							$('.datagrid-header-check input').removeAttr('checked');
			 	    	}
			 	    	if(data.rows == null || data.rows == undefined || data.rows.length == 0){
			 	    		$('.datagrid-header-check input').removeAttr('checked');
			 	    	}
				    },
				    onCheck:function(i,r){
				    	ProgrammeDisplayCatalog._listExclusion.put(r.id,r);
				    },
				    onUncheck:function(i,r){
				    	ProgrammeDisplayCatalog._listExclusion.remove(r.id);
				    },
				    onCheckAll:function(r){
				    	for(i=0;i<r.length;i++){
				    		ProgrammeDisplayCatalog._listExclusion.put(r[i].id,r[i]);
				    	}
				    },
				    onUncheckAll:function(r){
				    	for(i=0;i<r.length;i++){
				    		ProgrammeDisplayCatalog._listExclusion.remove(r[i].id);
				    	}
				    }
				}); 
				$('.datagrid-header-rownumber').html('STT');
				$('.easyui-dialog #btnSearchStyleExclusion').bind('click',function(event) {
					$('.easyui-dialog #errMsgSearchExclusion').html('').hide();
					var code = encodeChar($('.easyui-dialog #seachStyleExclusionCode').val().trim());
					var name = encodeChar($('.easyui-dialog #seachStyleExclusionName').val().trim());
					var url = ProgrammeDisplayCatalog.getSearchStyleExclusionMapGridUrl($('.easyui-dialog #searchStyleExclusionUrl').val(), code, name, CommonSearch._arrParams);
					$('.easyui-dialog #searchStyleExclusionGrid').datagrid({url:url,pageNumber:1});						
					$('.easyui-dialog #seachStyleExclusionCode').focus();
				});
				$('.easyui-dialog #btnSaveExclusion').bind('click',function(event) {
					var lstIdCustomer = new Array();
					for(i=0;i<ProgrammeDisplayCatalog._listExclusion.keyArray.length;i++){
						var temp = ProgrammeDisplayCatalog._listExclusion.get(ProgrammeDisplayCatalog._listExclusion.keyArray[i]);
						if(temp!=null){
							lstIdCustomer.push(temp.id);
						}
					}

					var params = new Object();
					params.listExclusionId =lstIdCustomer;
					params.id = $('#selId').val();
					if(lstIdCustomer == undefined || lstIdCustomer == null || lstIdCustomer.length <=0){
						$('.easyui-dialog #errMsgSearchExclusion').html("Bạn chưa chọn CTTB để loại trừ.").show();
						return false;
					}
					Utils.addOrSaveData(params, '/programme-display/createExclusionProgram', null, 'errMsgExclusion', function(data){
						if(data != null && data.error == undefined && data.errMsg == undefined){
							ProgrammeDisplayCatalog.searchExclusionProgram();
							ProgrammeDisplayCatalog._listExclusion = new Map();
							$('#errMsgSearchExclusion').html("").hide();
							$('#searchStyleExclusionDialog').dialog('close');
							$('#errMsgExclusion').html("").hide();
							$('#successMsgExclusion').html('Lưu dữ liệu thành công.').show();
							var tm = setTimeout(function(){
								$('#successMsgExclusion').html('').hide();
								clearTimeout(tm); 
							}, 3000);
						}
					}, null, null, null, "Bạn có muốn thêm CTTB không ?", function(data){
						$('#errMsgSearchExclusion').html(data.errMsg).show();
						var tm = setTimeout(function(){
							$('#successMsgExclusion').html('').hide();
							clearTimeout(tm); 
						}, 3000);
					});
				});
				$('.easyui-dialog #seachStyleExclusionCode').bind('keyup',function(event){
					if(event.keyCode == 13){
						$('.easyui-dialog #errMsgSearchExclusion').html('').hide();
						var code = $('.easyui-dialog #seachStyleExclusionCode').val().trim();
						var name = $('.easyui-dialog #seachStyleExclusionName').val().trim();
						var url = ProgrammeDisplayCatalog.getSearchStyleExclusionMapGridUrl($('.easyui-dialog #searchStyleExclusionUrl').val(), code, name, CommonSearch._arrParams);
						$('.easyui-dialog #searchStyleExclusionGrid').datagrid({url:url,pageNumber:1});
						$('.easyui-dialog #seachStyleExclusionCode').focus();
					}
				});
				$('.easyui-dialog #seachStyleExclusionName').bind('keyup',function(event){
					if(event.keyCode == 13){
						$('.easyui-dialog #errMsgSearchExclusion').html('').hide();
						var code = $('.easyui-dialog #seachStyleExclusionCode').val().trim();
						var name = $('.easyui-dialog #seachStyleExclusionName').val().trim();
						var url = ProgrammeDisplayCatalog.getSearchStyleExclusionMapGridUrl($('.easyui-dialog #searchStyleExclusionUrl').val(), code, name, CommonSearch._arrParams);
						$('.easyui-dialog #searchStyleExclusionGrid').datagrid({url:url,pageNumber:1});
						$('.easyui-dialog #seachStyleExclusionCode').focus();
					}
				});
	        },
	        onBeforeClose: function() {
				var tabindex = 1;
				$(' .InputTextStyle, select, input, button , li , tr, td, th, label, ul, a, img').each(function () {
					if (this.type != 'hidden') {
						$(this).attr("tabindex", '');
					}
					tabindex ++;
				});	
				var curIdFocus = $('#cur_focus').val();
				$('#'+ curIdFocus).focus();				
	        },
	        onClose : function(){
	        	$('#searchStyleExclusionDialogDiv').html(html);
	        	$('#searchStyleExclusionDialogDiv').hide();
	        	$('.easyui-dialog #seachStyleExclusionCode').val('');
	        	$('.easyui-dialog #seachStyleExclusionName').val('');
	        	$('.easyui-dialog #btnSaveExclusion').unbind('click');
	        	$('.easyui-dialog #errMsgSearchExclusion').html('').hide();
	        	ProgrammeDisplayCatalog._listExclusion = new Map(); 
	        }
	    });
		return false;
	},
	getSearchStyleExclusionMapGridUrl : function(url, code, name, arrParam) {		
		var searchUrl = '';
		searchUrl = url;
		var curDate = new Date();
		var curTime = curDate.getTime();
		searchUrl+= '?curTime=' + encodeChar(curTime);
		if (arrParam != null && arrParam != undefined) {
			for ( var i = 0; i < arrParam.length; i++) {
				searchUrl += "&" + arrParam[i].name + "=" + arrParam[i].value;
			}
		}
		searchUrl+= "&code=" + encodeChar(code) + "&name=" + encodeChar(name); 
		return searchUrl;
	},
};
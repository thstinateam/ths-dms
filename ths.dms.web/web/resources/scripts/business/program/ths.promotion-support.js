var PromotionSupport = {
	shopId: null,
	_xhrSave: null,
	_promotionShopParams: null,
	_promotionStaffParams: null,
	_hasCustomerType:null,
	_hasSaleLevel:null,
	_hasCustomerCardType:null,
	_flagAjaxTabAttribute:null,
	searchProgram : function() {
		$('.ErrorMsgStyle').hide();
		$('#fakefilepc').val('');
		$('#excelFile').val('');
		
		var promotionCode = $('#promotionCode').val().trim();
		var lstApParamId = null;
		if($('#typeCode').val() == null || $('#typeCode').val().length == 0){
			lstApParamId = [];
			lstApParamId.push(-1);
		}else{
			lstApParamId = $('#typeCode').val();
			if (lstApParamId[0] == -1) {
				lstApParamId.splice(1);
			}
		}
		var shopCode = $('#shopCode').val().trim();
		var promotionName = $('#promotionName').val().trim();
		var status = 1;
		//if($('#permissionUser').val() == 'true' ){
			status = $('#status').val().trim();
		//}
		var proType = 1;//$("#proType").val();
		var msg = '';
		var fDate = $('#fromDate').val().trim();
		var tDate = $('#toDate').val().trim();
		if (fDate.length > 0 && !Utils.isDate(fDate)) {
			msg = 'Từ ngày không hợp lệ, vui lòng nhập theo định dạng dd/MM/yyyy';
			$('#fromDate').focus();
		}
		if (msg.length == 0 && tDate.length > 0 && !Utils.isDate(tDate)) {
			msg = 'Đến ngày không hợp lệ, vui lòng nhập theo định dạng dd/MM/yyyy';
			$('#toDate').focus();
		}
		if(msg.length == 0 && fDate.length > 0 && tDate.length > 0 && !Utils.compareDate(fDate, tDate)){
			msg = 'Từ ngày phải là ngày trước hoặc cùng ngày với Đến ngày';
			$('#fromDate').focus();
		}
		if(msg.length > 0){
			$('#errMsg').html(msg).show();
			return false;
		}
		var params = {
				code: promotionCode,
				name: promotionName,
				proType: proType,
				shopCode: shopCode,
				lstTypeId: lstApParamId.join(","),
				fromDate: fDate,
				toDate: tDate,
				status: status
		};
		$('#grid').datagrid("reload", params);

		return false;
	},
	gridRowIconFormatter: function(value,row,index) {
		return "<a id='permissionEdit_btnEdit_" + row.id + "' class='cmsiscontrol' href='/promotion-support/detail?promotionId="+row.id+"'><span style='cursor:pointer'><img src='/resources/images/icon-edit.png'/></span></a>";
	},
	showTab: function(tabId, url) {
		if ($("#" + tabId + ".Active").length > 0) {
			return;
		}
		$(".ErrorMsgStyle").hide();
		$("#tabSectionDiv a.Active").removeClass("Active");
		$("#" + tabId).addClass("Active");
		url = url + "?promotionId="+$("#masterDataArea #id").val().trim();
		VTUtilJS.getFormHtml('masterDataArea', url, function(html) {
			$('#divTab').html(html);
			Utils.functionAccessFillControl();
		});
	},
	update : function() {
		$('#errorMsg').html('').hide();
		var params = VTUtilJS.getFormData('update-form');
		params.endDate = $('#permissionEditInfo_endDate').val();
		params.status = $('#permissionEditInfo_status').val();
		params.description = $('#description').val();
		if(params.typeCode == -1) {
			VTUtilJS.showMessageBanner(true, 'Bạn chưa chọn loại chương trình');
			return;
		}
		if(params != null) {
			var promotionId = $('#id').val();
			if(!VTUtilJS.isNullOrEmpty(promotionId)) {
				params.promotionId = promotionId;
			}
			$.messager.confirm("Xác nhận", "Bạn có muốn cập nhập thông tin CTKM?", function(r) {
				if(r) {
					VTUtilJS.postFormJson(params, '/promotion-support/update', function(data) {
						if(data.error) {
							VTUtilJS.showMessageBanner(true, data.errMsg);
						} else {
							window.location.href = '/promotion-support/detail?promotionId='+data.promotionId;
						}
					});
				}
			});
		}
	},
	searchShop: function() {
		$(".ErrorMsgStyle").hide();
		$("#labelListCustomer").hide();
		$("#promotionCustomerGrid").hide();
		$("#boxSearch1").hide();
		var params = {
				promotionId: $("#masterDataArea #id").val().trim(),
				code: $("#shopCode").val().trim(),
				name: $("#shopName").val().trim(),
				quantity: isNaN($("#quantityMax").val().trim()) ? "" : $("#quantityMax").val().trim()
		};
		PromotionSupport._promotionShopParams = params;
		$("#exGrid").treegrid("reload", params);
	},
	toggleCustomerSearch: function() {
		var b = $("#boxSearch1").is(":hidden");
		if (b) {
			$("#searchCustomerDiv").html("Đóng tìm kiếm &lt;&lt;");
		} else {
			$("#searchCustomerDiv").html("Tìm kiếm &gt;&gt;");
		}
		$("#boxSearch1").toggle();
	},
	importPromotionShop: function() {
		$('.ErrorMsgStyle').hide();
		VTUtilJS.importExcelUtils(function(data) {
			$("#exGrid").treegrid("reload");
			$("#labelListCustomer").hide();
			$("#promotionCustomerGrid").hide();
			$("#boxSearch1").hide();
			if(data.flag) {
				$("#errExcelMsgShop").html(data.message).show();
			} else {
				$("#successExcelMsgShop").html('Import thành công').show();
			}
		}, "importFrm", "excelFile", null, "errExcelMsgShop");
	},
	exportPromotionShop: function() {
		$(".ErrorMsgStyle").hide();
		if (PromotionSupport._promotionShopParams == null) {
			PromotionSupport._promotionShopParams = {};
			PromotionSupport._promotionShopParams.promotionId = $("#masterDataArea #id").val().trim();
		}
		ReportUtils.exportReport("/promotion-support/export-promotion-shop", PromotionSupport._promotionShopParams, "errMsgShop");
	},
	showCustomerDlg: function() {
		if (isNaN(PromotionSupport.shopId)) {
			return;
		}
		$(".ErrorMsgStyle").hide();
		var html = '<div id="add-customerPopupDiv" style="display:none;">\
			<div id="add-customerPopup">\
			<div class="PopupContentMid">\
				<div class="GeneralForm Search1Form">\
					<label class="LabelStyle" style="width:100px;">Mã KH</label>\
					<input id="codeDlg" class="InputTextStyle" style="width:150px;" maxlength="40" />\
					<label class="LabelStyle" style="width:100px;">Tên KH</label>\
					<input id="nameDlg" class="InputTextStyle" style="width:230px;" maxlength="100" />\
					<div class="Clear"></div>\
					<label class="LabelStyle" style="width:100px;">Địa chỉ</label>\
					<input id="addressDlg" class="InputTextStyle" style="width:500px;" maxlength="250" />\
					<div class="Clear"></div>\
					<div class="BtnCenterSection">\
					    <button class="BtnGeneralStyle" id="btnSearchDlg">Tìm kiếm</button>\
					</div>\
					\
					<div class="GridSection" id="gridDlgDiv">\
						<div id="gridDlg"></div>\
					</div>\
					\
					<label class="LabelStyle" style="width:80px;">Số suất</label>\
					<input id="quantityDlg" class="InputTextStyle vinput-money" style="width:130px;" maxlength="9" />\
					<label class="LabelStyle" style="width:80px;">Số lượng</label>\
					<input id="quantityMaxDlg" class="InputTextStyle vinput-money" style="width:130px;" maxlength="9" />\
					<label class="LabelStyle" style="width:80px;">Số tiền</label>\
					<input id="amountMaxDlg" class="InputTextStyle vinput-money" style="width:130px;" maxlength="9" />\
					<div class="Clear"></div><p id="erMsgDlg" class="ErrorMsgStyle" style="padding-left:5px;display:none;"></p>\
					<div class="Clear"></div>\
					<div class="BtnCenterSection">\
					    <button class="BtnGeneralStyle" id="btnChooseDlg">Chọn</button>\
						<button class="BtnGeneralStyle" id="btnCloseDlg" onclick="javascript:$(\'#add-customerPopup\').dialog(\'close\');">Đóng</button>\
					</div>\
				</div>\
			</div>\
			</div>\
		</div>';
		$("body").append(html);
		$('.vinput-money').each(function() {
			VTUtilJS.bindFormatOnTextfield($(this), VTUtilJS._TF_NUMBER_COMMA);
			VTUtilJS.formatCurrencyFor($(this));
		});
		var r = $("#exGrid").treegrid("find", PromotionSupport.shopId);
		var tt = (r == null) ? "Chọn Khách hàng" : "Chọn Khách hàng (<span style='color:#3a1;'>" + r.attr.shopName + "</span>)";
		
		var lstTmp = null;
		$("#add-customerPopup").dialog({
			title: tt,
			width: 750,
			heigth: "auto",
			modal: true, close: false, cache: false,
			onOpen: function() {
				$("#add-customerPopup").addClass("easyui-dialog");
				VTUtilJS.bindFormatOnTextfield("quantityDlg", VTUtilJS._TF_NUMBER);
				$("#add-customerPopup #codeDlg").focus();
				
				lstTmp = new CArray();
				$("#add-customerPopup #gridDlg").datagrid({
					url: "/promotion-support/search-customer-dlg",
					rownumbers: false,
					width: $("#add-customerPopup #gridDlgDiv").width(),
					height: "auto",
					pagination: true,
					fitColumns: true,
					idField: "customerId",
					scrollbarSize: 0,
					queryParams: {
						promotionId: $("#masterDataArea #id").val(),
						shopId: Number(PromotionSupport.shopId)
					},
					columns: [[
						{field:"no", title:"STT", sortable: false, resizable: false, width:45, fixed:true, align:"center", formatter:function(v, r, i) {
							var p = $("#add-customerPopup #gridDlg").datagrid("options").pageNumber;
							var n = $("#add-customerPopup #gridDlg").datagrid("options").pageSize;
							return (Number(p) - 1) * Number(n) + i + 1;
						}},
						{field:"customerCode", title:"Mã KH", sortable: false, resizable: false, width:75, align:"left" , formatter: function(value, row, index) {
							return Utils.XSSEncode(value);
						}},
						{field:"customerName", title:"Tên KH", sortable: false, resizable: false, width:120, align:"left", formatter:CommonFormatter.formatNormalCell},
						{field:"address", title:"Địa chỉ", sortable: false, resizable: false, width:200, align:"left", formatter:CommonFormatter.formatNormalCell},
						{field:"ck", title:"", checkbox:true, sortable: false, resizable: false, align:"center"}
					]],
					onLoadSuccess: function(data) {
						$("#add-customerPopup #gridDlg").datagrid("resize");
						setTimeout(function(){
							var hDlg=parseInt($("#add-customerPopup").parent().height());
							var hW=parseInt($(window).height());
							var d=hW-hDlg;
							d=d/2+document.documentElement.scrollTop;
							if (d < 0) { d = 0; }
							$("#add-customerPopup").parent().css('top',d);
			    		},1000);
					},
					onCheck: function(i, r) {
						lstTmp.push(r.customerId);
					},
					onUncheck: function(i, r) {
						lstTmp.remove(r.customerId);
					},
					onCheckAll: function(rows) {
						for (var i = 0, sz = rows.length; i < sz; i++) {
							lstTmp.push(rows[i].customerId);
						}
					},
					onUncheckAll: function(rows) {
						for (var i = 0, sz = rows.length; i < sz; i++) {
							lstTmp.remove(rows[i].customerId);
						}
					}
				});
				
				$("#add-customerPopup #btnSearchDlg").click(function() {
					var p = {
							promotionId: $("#masterDataArea #id").val(),
							shopId: Number(PromotionSupport.shopId),
							code: $("#add-customerPopup #codeDlg").val().trim(),
							name: $("#add-customerPopup #nameDlg").val().trim(),
							address: $("#add-customerPopup #addressDlg").val().trim()
					};
					$("#add-customerPopup #gridDlg").datagrid("load", p);
				});
				$("#add-customerPopup #codeDlg").parent().keyup(function(event) {
					if (event.keyCode == keyCodes.ENTER) {
						$("#add-customerPopup #btnSearchDlg").click();
					}
				});
				$("#add-customerPopup #btnChooseDlg").click(function() {
					if (lstTmp == null || lstTmp.size() == 0) {
						$("#erMsgDlg").html("Không có khách hàng nào được chọn").show();
						return;
					}
					var qtt = VTUtilJS.returnMoneyValue($('#quantityDlg').val());
					if (qtt.length > 0 && (isNaN(qtt) || Number(qtt) <= 0)) {
						$("#erMsgDlg").html("Số suất phải lớn hơn 0 và tối đa 9 ký tự số").show();
						return;
					}
					var qttMax = VTUtilJS.returnMoneyValue($('#quantityMaxDlg').val());
					if (qttMax.length > 0 && (isNaN(qttMax) || Number(qttMax) <= 0)) {
						$("#erMsgDlg").html("Số lượng phải lớn hơn 0 và tối đa 9 ký tự số").show();
						return;
					}
					var amountMax = VTUtilJS.returnMoneyValue($('#amountMaxDlg').val());
					if (amountMax.length > 0 && (isNaN(amountMax) || Number(amountMax) <= 0)) {
						$("#erMsgDlg").html("Số tiền phải lớn hơn 0 và tối đa 9 ký tự số").show();
						return;
					}
					PromotionSupport.addCustomerQtt(lstTmp.toArray());
				});
			},
			onClose: function() {
				$("#add-customerPopup").dialog("destroy");
				$("#add-customerPopupDiv").remove();
			}
		});
	},
	addCustomerQtt: function(lstCustId) {
		$(".ErrorMsgStyle").hide();
		$.messager.confirm("Xác nhận", "Bạn có muốn thêm khách hàng?", function(r) {
			if (r) {
				var params = {
						promotionId: $("#masterDataArea #id").val().trim(),
						shopId: PromotionSupport.shopId,
						quantity: VTUtilJS.returnMoneyValue($('#quantityDlg').val()),
						quantityMax: VTUtilJS.returnMoneyValue($('#quantityMaxDlg').val()),
						amountMax: VTUtilJS.returnMoneyValue($('#amountMaxDlg').val()),
						lstId: lstCustId
				};
				$("#add-customerPopup").dialog("close");
				Utils.saveData(params, "/promotion-support/add-customer", PromotionSupport._xhrSave, 'errMsgCustomer', function(data) {
					$("#successMsgCustomer").html("Lưu dữ liệu thành công").show();
					setTimeout(function() {$("#successMsgCustomer").hide();},3000);
					$("#exGrid").treegrid("reload");
					$("#promotionCustomerExGrid").datagrid("reload");
				}, "loading2");
			}
		});
	},
	deleteShopMap: function(shopId) {
		$(".ErrorMsgStyle").hide();
		$.messager.confirm("Xác nhận", "Bạn có muốn xóa đơn vị?", function(r) {
			if (r) {
				var params = {
						promotionId: $("#masterDataArea #id").val().trim(),
						shopId: shopId
				};
				Utils.saveData(params, "/promotion-support/delete-shop", PromotionProgram._xhrSave, 'errMsgShop', function(data) {
					$("#successMsgShop").html("Lưu dữ liệu thành công").show();
					setTimeout(function() {$("#successMsgShop").hide();},3000);
					$("#exGrid").treegrid("reload");
				}, "loading2");
			}
		});
	},
	showShopDlg: function(shopId) {
		if (shopId == undefined || shopId == null) {
			shopId = 0;
		}
		$(".ErrorMsgStyle").hide();
		var html = '<div id="add-shopPopupDiv" style="display:none;">\
			<div id="add-shopPopup">\
			<div class="PopupContentMid">\
				<div class="GeneralForm Search1Form">\
					<label class="LabelStyle" style="width:100px;">Mã đơn vị</label>\
					<input id="codeDlg" class="InputTextStyle" style="width:150px;" maxlength="40" />\
					<label class="LabelStyle" style="width:100px;">Tên đơn vị</label>\
					<input id="nameDlg" class="InputTextStyle" style="width:230px;" maxlength="100" />\
					<div class="Clear"></div>\
					<div class="BtnCenterSection">\
					    <button class="BtnGeneralStyle" id="btnSearchDlg">Tìm kiếm</button>\
					</div>\
					\
					<div class="GridSection" id="gridDlgDiv">\
						<div id="gridDlg"></div>\
					</div>\
					\
					<p id="erMsgDlg" class="ErrorMsgStyle" style="padding-left:5px;display:none;"></p>\
					<div class="Clear"></div>\
					<div class="BtnCenterSection">\
					    <button class="BtnGeneralStyle" id="btnChooseDlg">Chọn</button>\
						<button class="BtnGeneralStyle" id="btnCloseDlg" onclick="javascript:$(\'#add-shopPopup\').dialog(\'close\');">Đóng</button>\
					</div>\
				</div>\
			</div>\
			</div>\
		</div>';
		$("body").append(html);
		
		$("#add-shopPopup").dialog({
			title: "Chọn đơn vị và Số suất KM",
			width: 700,
			heigth: "auto",
			modal: true, close: false, cache: false,
			onOpen: function() {
				$("#add-shopPopup").addClass("easyui-dialog");
				$("#add-shopPopup #codeDlg").focus();
				
				$("#add-shopPopup #gridDlg").treegrid({
					url: "/promotion-support/search-shop-dlg?promotionId="+$("#masterDataArea #id").val().trim()+"&shopId="+Number(shopId),
					rownumbers: false,
					width: $("#add-shopPopup #gridDlgDiv").width(),
					height: 350,
					fitColumns: true,
					idField: "nodeId",
					treeField: "text",
					selectOnCheck: false,
					checkOnSelect: false,
					columns: [[
						{field:"text", title:"Mã Đơn vị", sortable: false, resizable: false, width: 220, align:"left", formatter:CommonFormatter.formatNormalCell},
						{field:"quantity", title:"Số suất", sortable: false, resizable: false, width: 110, fixed:true, align:"left", formatter: function(v, r) {
							if (r.attr.isNPP) {
								if (r.attr.isExists) {
									return "<input type='text' id='quantity-ip"+r.attr.id+"' style='float: right;' class='vinput-money' maxlength='11' disabled='disabled' exists='1' />";
								}
								return "<input type='text' id='txt-p"+r.attr.id+"' style='float: right;' class='vinput-money' maxlength='11' exists='0' />";
							}
							return "";
						}},
						{field:"quantityMax", title:"Số lượng", sortable: false, resizable: false, width: 110, fixed:true, align:"left", formatter: function(v, r) {
							if (r.attr.isNPP) {
								if (r.attr.isExists) {
									return "<input type='text' id='quantity-max-ip-"+r.attr.id+"' style='float: right;' class='vinput-money' maxlength='11' disabled='disabled' exists='1' />";
								}
								return "<input type='text' id='quantity-max-ip-"+r.attr.id+"' style='float: right;' class='vinput-money' maxlength='11' exists='0' />";
							}
							return "";
						}},
						{field:"amountMax", title:"Số tiền", sortable: false, resizable: false, width: 110, fixed:true, align:"left", formatter: function(v, r) {
							if (r.attr.isNPP) {
								if (r.attr.isExists) {
									return "<input type='text' id='amount-max-ip-"+r.attr.id+"' style='float: right;' class='vinput-money' maxlength='11' disabled='disabled' exists='1' />";
								}
								return "<input type='text' id='amount-max-ip-"+r.attr.id+"' style='float: right;' class='vinput-money' maxlength='11' exists='0' />";
							}
							return "";
						}},
						{field:"ck", title:"", sortable: false, resizable: false, width:35, fixed:true, align:"center", formatter: function(v, r) {
							var pId = r.attr.parentId;
							if (pId == undefined || pId == null) {
								pId = 0;
							}
							if (r.attr.isExists) {
								return "<input type='checkbox' id='ck"+r.nodeId+"' value='"+r.attr.id+"' disabled='disabled' exists='1' parentId='"+pId+"' onchange='PromotionSupport.onCheckShop(this);' />";
							}
							return "<input type='checkbox' id='ck"+r.nodeId+"' value='"+r.attr.id+"' exists='0' parentId='"+pId+"' onchange='PromotionSupport.onCheckShop(this);' />";
						}}
					]],
					onLoadSuccess: function(data) {
						$("#add-shopPopup #gridDlg").datagrid("resize");
						$('#add-shopPopup .vinput-money').each(function() {
				      		VTUtilJS.bindFormatOnTextfield($(this), VTUtilJS._TF_NUMBER_COMMA);
							VTUtilJS.formatCurrencyFor($(this).prop('id'));
							$(this).width($(this).parent().width() - 7);
				      	});
						setTimeout(function() {
							var hDlg=parseInt($("#add-shopPopup").parent().height());
							var hW=parseInt($(window).height());
							var d = hW - hDlg;
							d = (d / 2) + document.documentElement.scrollTop;
							if (d < 0) { 
								d = 0; 
							}
							$("#add-shopPopup").parent().css('top', d);
			    		}, 1000);
					}
				});
				
				$("#add-shopPopup #btnSearchDlg").click(function() {
					var p = {
							code: $("#add-shopPopup #codeDlg").val().trim(),
							name: $("#add-shopPopup #nameDlg").val().trim()
					};
					$("#add-shopPopup #gridDlg").treegrid("reload", p);
				});
				$("#add-shopPopup #codeDlg").parent().keyup(function(event) {
					if (event.keyCode == keyCodes.ENTER) {
						$("#add-shopPopup #btnSearchDlg").click();
					}
				});
				$("#add-shopPopup #btnChooseDlg").click(function() {
					if ($("#add-shopPopup input[id^=ck]:checked").length == 0) {
						$("#erMsgDlg").html("Không có đơn vị nào được chọn").show();
						return;
					}
					var mapShopQuantityObj = new Map();
					var arrQuantityObject = [];
					var shopId, qtt, shopText;
					var parentTr;
					var flagError = false;
					$("#add-shopPopup input[id^=ck]:checked").each(function() {
						arrQuantityObject = [];
						shopId = Number($(this).val());
						parentTr = $(this).parent().parent().parent();
						shopText = $($(parentTr).children('td')[0]).text();
						$('#' + $(parentTr).prop('id') + ' .vinput-money').each(function() {
							qtt = VTUtilJS.returnMoneyValue($(this).val());
							if (qtt.length > 9 || isNaN(qtt) || (qtt.length > 0 && Number(qtt) <= 0)) {
								$("#erMsgDlg").html(shopText.trim() + ' không hợp lệ. Số suất, Số lượng, Số tiền phải lớn hơn 0 và tối đa 9 ký tự số').show();
								mapShopQuantityObj = null;
								$(this).focus();
								flagError = true;
								return;
							}
							arrQuantityObject.push(qtt);
						});
						mapShopQuantityObj.put(shopId, arrQuantityObject);//[so suat, so luong, so tien]
					});
					if (flagError) {
						return;
					}
					PromotionSupport.addShopQtt(mapShopQuantityObj);
				});
			},
			onClose: function() {
				$("#add-shopPopup").dialog("destroy");
				$("#add-shopPopupDiv").remove();
			}
		});
	},
	removeParentShopCheck: function(t) {
		if ($(t).length == 0) {
			return;
		}
		$(t).removeAttr("checked");
		$(t).attr("disabled", "disabled");
		var p = "#ck" + $(t).attr("parentId");
		PromotionSupport.removeParentShopCheck(p);
	},
	
	enableParentShopCheck: function(t) {
		if ($(t).length == 0 || $(t).attr("exists") == "1") {
			return;
		}
		$(t).removeAttr("disabled");
		var p = "#ck" + $(t).attr("parentId");
		PromotionSupport.enableParentShopCheck(p);
	},
	onCheckShop: function(t) {
		var ck = $(t).prop("checked");
		if (ck) {
			var p = "#ck" + $(t).attr("parentId");
			PromotionSupport.removeParentShopCheck(p);
		} else {
			var p = "#ck" + $(t).attr("parentId");
			PromotionSupport.enableParentShopCheck(p);
		}
	},
	addShopQtt: function(mapShopTmp) {
		$(".ErrorMsgStyle").hide();
		$.messager.confirm("Xác nhận", "Bạn có muốn thêm đơn vị?", function(r) {
			if (r) {
				var params = {
						promotionId: $("#masterDataArea #id").val().trim(),
						lstId: mapShopTmp.keyArray
				};
				if (mapShopTmp != undefined && mapShopTmp != null && mapShopTmp.size() > 0) {
					var arrKeyVal = mapShopTmp.keyArray;
					//Bo sung tham so
					params.lstId = mapShopTmp.keyArray;
					//Khai bai tham bien dung chung
					var arrQuantity = [];
					var arrQuantityMax = [];
					var arrAmountMax = [];
					for (var i = 0, size = arrKeyVal.length; i < size; i++) {
						var arrQuantityVal = mapShopTmp.get(arrKeyVal[i]);
						if (arrQuantityVal != null && arrQuantityVal != undefined) {
							var lengthArrlength = arrQuantityVal.length;
							if (lengthArrlength > 0 && !isNaN(arrQuantityVal[0]) && Number(arrQuantityVal[0]) > 0) {
								arrQuantity.push(arrKeyVal[i] + ";" + arrQuantityVal[0].trim());
							}
							if (lengthArrlength > 1 && !isNaN(arrQuantityVal[1]) && Number(arrQuantityVal[1]) > 0) {
								arrQuantityMax.push(arrKeyVal[i] + ";" + arrQuantityVal[1].trim());
							}
							if (lengthArrlength > 2 && !isNaN(arrQuantityVal[2]) && Number(arrQuantityVal[2]) > 0) {
								arrAmountMax.push(arrKeyVal[i] + ";" + arrQuantityVal[2].trim());
							}
						}
					}
					//Bo sung tham so So suat, So luong, So tien
					params.quantityObjectStr = arrQuantity.toString();
					params.quantityMaxObjectStr = arrQuantityMax.toString();
					params.amountMaxObjectStr = arrAmountMax.toString();
				}
				
				$("#add-shopPopup").dialog("close");
				Utils.saveData(params, "/promotion-support/add-shop", PromotionSupport._xhrSave, 'errMsgShop', function(data) {
					$("#successMsgShop").html("Lưu dữ liệu thành công").show();
					setTimeout(function() {$("#successMsgShop").hide();},3000);
					$("#exGrid").treegrid("reload");
					$("#labelListCustomer").hide();
					$("#promotionCustomerGrid").hide();
					$("#boxSearch1").hide();
				}, "loading2");
			}
		});
	},
	/**
	 * @modify hunglm16
	 * @param shopId
	 * @returns
	 * @since 25/11/2015
	 */
	updateShopQuantity: function(shopId) {
		$(".ErrorMsgStyle").hide();
		var arrNumberForShop = [];
		var arrVinputMoney = $('#promotionShopGrid .vinput-money[id*="ip-'+shopId+'"]');
		var nodeChange = null;
		var dem = 0;
		var numText = null;
		var numReceivedText = null;
		var numReceived = null;
		for (var i = 0, size = arrVinputMoney.length; i < size; i++) {
			dem++;
			var txtInput = arrVinputMoney[i];
			if (dem == 1) {
				nodeChange = $("#exGrid").treegrid('find', shopId);
				numText = 'Số suất KM';
				numReceivedText = 'Số suất đã KM';
				numReceived = nodeChange.attr.receivedQtt;
			} else if (dem == 2) {
				numText = 'Số lượng KM';
				numReceivedText = 'Số lượng đã KM';
				numReceived = nodeChange.attr.receivedNum;
			} else if (dem == 3) {
				numText = 'Số tiền KM';
				numReceivedText = 'Số tiền đã KM';
				numReceived = nodeChange.attr.receivedAmt;
			}
			var qtt = VTUtilJS.returnMoneyValue($(txtInput).val());
			var objectText = nodeChange.text;
			if (objectText == undefined && objectText == null) {
				objectText = '';
			}
			if (qtt.length > 9 || isNaN(qtt) || (qtt.length > 0 && Number(qtt) <= 0)) {
				$("#errMsgShop").html(objectText.trim() + ' không hợp lệ. '+ numText +' phải lớn hơn 0 và tối đa 9 ký tự số').show();
				$(txtInput).focus();
				return;
			} else if (numReceived != undefined && numReceived != null && numReceived > Number(qtt)) {
				$("#errMsgShop").html(objectText.trim() + ' không hợp lệ. '+ numText +' phải lớn hơn hoặc bằng ' + numReceivedText).show();
				$(txtInput).focus();
				return;
			} else {
				arrNumberForShop.push(qtt);
			}
			if (dem == 3) {
				dem = 0;
			}
			msg = null;
		}
		$.messager.confirm("Xác nhận", "Bạn có muốn cập nhật đơn vị?", function(r) {
			if (r) {
				var params = {
						promotionId: $("#masterDataArea #id").val().trim(),
						shopId: shopId,
						quantity: arrNumberForShop.length > 0 ? arrNumberForShop[0] : Number(arrNumberForShop[0]),
						quantityMax: arrNumberForShop.length > 1 ? arrNumberForShop[1] : Number(arrNumberForShop[1]),
						amountMax: arrNumberForShop.length > 2 ? arrNumberForShop[2] : Number(arrNumberForShop[2])
				};
				Utils.saveData(params, "/promotion-support/update-shop-quantity", PromotionSupport._xhrSave, 'errMsgShop', function(data) {
					$("#successMsgShop").html("Lưu dữ liệu thành công").show();
					setTimeout(function() {
						$(".SuccessMsgStyle").hide();
					}, 1500);
					$("#exGrid").treegrid("reload");
					$("#labelListCustomer").hide();
					$("#promotionCustomerGrid").hide();
					$("#boxSearch1").hide();
				}, "loading2");
			}
		});
	},
	searchCustomerShopMap: function(promotionId, shopId, status) {
		$(".ErrorMsgStyle").hide();
		var r = $("#exGrid").treegrid("find", shopId);
		if (r != null) {
			$("#labelShop").html(r.attr.shopName);
		}
		$("#labelListCustomer").show();
		$("#promotionCustomerGrid").show();
		PromotionSupport.shopId = shopId;
		var params = {
				promotionId: promotionId,
				shopId: shopId,
				code: $("#customerCodeSearch").val().trim(),
				name: $("#customerNameSearch").val().trim(),
				address: $("#customerAddressSearch").val().trim()
		};
		$("#promotionCustomerExGrid").datagrid("load", params);
		$("#btnSearchCustomer").attr("onclick", "PromotionSupport.searchCustomerShopMap("+promotionId+","+shopId+","+status+");");
		$('html, body').animate({ scrollTop: $(document).height() }, 1000);
	},
	updateCustomerQuantity: function (pcmId) {
		$(".ErrorMsgStyle").hide();
		if (pcmId == undefined || pcmId == null || isNaN(pcmId)) {
			$('#errMsgCustomer').text('Không xác định được khách hàng với đơn vị tương ứng').show();
			return;
		}
			
		var rows = $("#promotionCustomerExGrid").datagrid('getRows');
		var row = null;
		for (var i = 0, size = rows.length; i < size; i++) {
			if (pcmId == rows[i].mapId) {
				row = rows[i];
			}
		}
		if (row == null) {
			$('#errMsgCustomer').text('Không xác định được khách hàng với đơn vị tương ứng').show();
			return;
		}
		var arrNumberForCus = [];
		var arrVinputMoney = $('#promotionCustomerGrid .vinput-money[id*="ip-'+pcmId+'"]');
		var dem = 0;
		var numText = null;
		var numReceivedText = null;
		var numReceived = null;
		for (var i = 0, size = arrVinputMoney.length; i < size; i++) {
			dem++;
			var txtInput = arrVinputMoney[i];
			if (dem == 1) {
				numText = 'Số suất KM';
				numReceivedText = 'Số suất đã KM';
				numReceived = row.receivedQtt;
			} else if (dem == 2) {
				numText = 'Số lượng KM';
				numReceivedText = 'Số lượng đã KM';
				numReceived = row.receivedNum;
			} else if (dem == 3) {
				numText = 'Số tiền KM';
				numReceivedText = 'Số tiền đã KM';
				numReceived = row.receivedAmt;
			}
			var qtt = VTUtilJS.returnMoneyValue($(txtInput).val());
			var objectText = 'Khách hàng ' + row.customerCode + ' - ' + row.customerName;
			if (qtt.length > 9 || isNaN(qtt) || (qtt.length > 0 && Number(qtt) <= 0)) {
				$("#errMsgCustomer").html(objectText.trim() + ' không hợp lệ. '+ numText +' phải lớn hơn 0 và tối đa 9 ký tự số').show();
				$(txtInput).focus();
				return;
			} else if (numReceived != undefined && numReceived != null && numReceived > Number(qtt)) {
				$("#errMsgCustomer").html(objectText.trim() + ' không hợp lệ. '+ numText +' phải lớn hơn hoặc bằng ' + numReceivedText).show();
				$(txtInput).focus();
				return;
			} else {
				arrNumberForCus.push(qtt);
			}
			if (dem == 3) {
				dem = 0;
			}
			msg = null;
		}
		$.messager.confirm("Xác nhận", "Bạn có muốn cập nhật khách hàng?", function(r) {
			if (r) {
				var params = {
						promotionId: $("#masterDataArea #id").val().trim(),
						id: pcmId,
						quantity: arrNumberForCus.length > 0 ? arrNumberForCus[0] : Number(arrNumberForCus[0]),
						quantityMax: arrNumberForCus.length > 1 ? arrNumberForCus[1] : Number(arrNumberForCus[1]),
						amountMax: arrNumberForCus.length > 2 ? arrNumberForCus[2] : Number(arrNumberForCus[2])
				};
				Utils.saveData(params, "/promotion-support/update-customer-quantity", PromotionSupport._xhrSave, 'errMsgCustomer', function(data) {
					$("#successMsgCustomer").html("Lưu dữ liệu thành công").show();
					setTimeout(function() {$("#successMsgCustomer").hide();},3000);
					//$("#exGrid").treegrid("reload");
					$("#promotionCustomerExGrid").datagrid("reload");
				}, "loading2");
			}
		});
	},
	deleteCustomerMap: function(pcmId) {
		$(".ErrorMsgStyle").hide();
		$.messager.confirm("Xác nhận", "Bạn có muốn xóa khách hàng?", function(r) {
			if (r) {
				var params = {
						promotionId: $("#masterDataArea #id").val().trim(),
						id: pcmId
				};
				Utils.saveData(params, "/promotion-support/delete-customer", PromotionSupport._xhrSave, 'errMsgCustomer', function(data) {
					$("#successMsgCustomer").html("Lưu dữ liệu thành công").show();
					setTimeout(function() {$("#successMsgCustomer").hide();},3000);
					$("#promotionCustomerExGrid").datagrid("reload");
				}, "loading2");
			}
		});
	},
	selectAttributes: function() {
		$('.ErrorMsgStyle').hide();
		var listObjectType= new Array();
		var listObjectId= new Array();
		$('#left :checkbox').each(function(){	
			if($(this).is(':checked')){
				//remove ben left, show ben right
				$(this).parent().remove(); 
				var objectType = $(this).attr('objectType');
				var objectId = $(this).attr('objectId');
				listObjectType.push(objectType);
				if(objectType == 2 || objectType == 3){
					listObjectId.push(0);
				}else{
					listObjectId.push(objectId);
				}
			}
		});	
		var data = new Object();
		data.lstObjectType = listObjectType;
		data.lstId = listObjectId;
		var kData = $.param(data, true);
		$.getJSON("/promotion-support/get-all-data-attributes",kData,function(result){
			var listOut = result.list;	
			if(listOut!=null){
				for(var h=0;h<listOut.length;++h){
					var objectType = listOut[h].objectType;
					var objectId = listOut[h].objectId;
					var valueType = listOut[h].valueType;
					var html = '';
					if(objectType == 2){
						var name = 'Loại khách hàng';
						html+= '<div>';
						html+= '<div class="Clear"></div>';
						html+= '<li>';
						html+= '<input name="'  + name + '" objectType="'+ objectType +'" objectId="'+objectId +'" type="checkbox"  class="InputCbxStyle checkBoxAttribute"/>';
						html+= '<label class="LabelStyle Label4Style">'+name+'</label>';
						html+= '</li>';
						html+= '<div class="BoxSelect BoxSelect2">';
						html+= '<select id="customerType" style="width: 275px;" class="MySelectBoxClass" multiple="multiple">';
						var list = listOut[h].listData;	
						var arrHtml = new Array();
						if(list!=null){
							for(var i=0;i<list.length;++i){
								arrHtml.push('<option value="'+ list[i].idChannelType +'">'+ Utils.XSSEncode(list[i].codeChannelType)+ '-' +Utils.XSSEncode(list[i].nameChannelType)+ '</option>');
							}
						}
						html+= arrHtml.join("");
						html+= '</select>';
						html+= '</div>';
						html+= '<div class="Clear"></div>';
						html+= '</div>';
						$('#right').append(html);
					}else if(objectType == 3){
						var name = 'Mức doanh số';
						html+= '<div id="saleLevel">';
						html+= '<div class="Clear"></div>';
						html+= 	'<li>';
						html+=	'<input name="'  + name + '" objectType="'+ objectType +'" objectId="'+objectId +'" type="checkbox"  class="InputCbxStyle checkBoxAttribute"/>';
						html+=	'<label class="LabelStyle Label4Style">'+name+'</label>';
						html+=	'</li>';
						var list = listOut[h].listProductInfoVO;//result.list;	
						if(list!=null){
							for(var i=0;i<list.length;++i){
								if(i==0){// if else để canh chỉnh html cho thẳng hàng đó mà.
									//label ngành hàng nên đặt 1 thuộc tính là catId
									html+= '<label catId="'+list[i].idProductInfoVO +'" style="width: 150px;" class="LabelStyle Label5Style cat">'+Utils.XSSEncode(list[i].codeProductInfoVO)+'</label>';
									//add combobox mức vao label nganh hang, combobox nên đặt id theo ngành hàng.
									html+= '<div class="BoxSelect BoxSelect2">';
									html+= '<select id="saleLevel-'+list[i].idProductInfoVO +'" class="MySelectBoxClass saleLevelCat" multiple="multiple">';
									var listSaleLevelCat = list[i].listSaleCatLevelVO;
									var arrHtml = new Array();
									if(listSaleLevelCat!=null){
										for(var k=0;k<listSaleLevelCat.length;++k){
											arrHtml.push('<option value="'+ listSaleLevelCat[k].idSaleCatLevel +'">'+ Utils.XSSEncode(listSaleLevelCat[k].codeSaleCatLevel)+'-'+Utils.XSSEncode(listSaleLevelCat[k].nameSaleCatLevel) +'</option>');
										}
									}
									html+= arrHtml.join("");
									html+= '</select>';
									html+= '</div>';
								}else{
									html+= '<div class="Clear"></div>';
									html+= '<label catId="'+list[i].idProductInfoVO +'" style="width: 150px;" class="LabelStyle Label6Style" >'+Utils.XSSEncode(list[i].codeProductInfoVO)+'</label>';
									//add combobox mức vao label nganh hang, combobox nên đặt id theo ngành hàng.
									html+= '<div class="BoxSelect BoxSelect2">';
									html+= '<select id="saleLevel-'+list[i].idProductInfoVO +'" class="MySelectBoxClass saleLevelCat" multiple="multiple">';
									var listSaleLevelCat = list[i].listSaleCatLevelVO;
									var arrHtml = new Array();
									if(listSaleLevelCat!=null){
										for(var k=0;k<listSaleLevelCat.length;++k){
											arrHtml.push('<option value="'+ listSaleLevelCat[k].idSaleCatLevel +'">'+Utils.XSSEncode(listSaleLevelCat[k].codeSaleCatLevel)+'-'+Utils.XSSEncode(listSaleLevelCat[k].nameSaleCatLevel) +'</option>');
										}
									}
									html+= arrHtml.join("");
									html+= '</select>';
									html+= '</div>';
								}
							}
						}
						html+= '<div class="Clear"></div>';
						html+= '</div>';
						$('#right').append(html);
					}else if(objectType == 1){
						var name = listOut[h].name;
						name = Utils.XSSEncode(name);
						html+= '<div>';
						html+= '<div class="Clear"></div>';
						html+= 	'<li>';
						//thằng input của thuộc tính động cần bỏ valueType vào để sau này xử lý khi lưu.
						html+=	'<input valueType="'+valueType+'" name="'  + name + '" objectType="'+ objectType +'" objectId="'+objectId +'" type="checkbox"  class="InputCbxStyle checkBoxAttribute"/>';
						html+=	'<label class="LabelStyle Label4Style">'+name+'</label>';
						html+=  '</li>';
						//Chia các trường hợp theo valueType của attributeId
						if(valueType=="CHARACTER"){
							//Nên đặt id html của text CHARACTER này theo attributeId cộng chuỗi CHARACTER  thì sẽ không bị conflict trong danh sách. Sau này muốn lưu thì
							//select theo attributeId để lấy ra dữ liệu.
							html+= '<input id="'+objectId+'_CHARACTER" type="text" style="width: 377px;" class="InputTextStyle InputText1Style"/>';
						}else if(valueType=="NUMBER"){
							html+= '<input id="'+objectId+'_NUMBER_from" type="text" style="width: 170px;" class="InputTextStyle InputText5Style"/>';
							html+= '<label class="LabelStyle Label7Style">-</label>';
							html+= '<input id="'+objectId+'_NUMBER_to" type="text" style="width: 170px;" class="InputTextStyle InputText5Style"/>';
						}else if(valueType=="DATE_TIME"){
							html+= '<input id="'+objectId+'_DATETIME_from" style="width: 146px;" type="text" class="InputTextStyle InputText6Style "/>';
							html+= '<label class="LabelStyle Label7Style">-</label>';
							html+= '<input id="'+objectId+'_DATETIME_to" style="width: 146px;" type="text" class="InputTextStyle InputText6Style "/>';
						}else if(valueType=="CHOICE" || valueType=="MULTI_CHOICE"){
							var list = listOut[h].listData;	
							var arrHtml = new Array();
							html+= '<div class="BoxSelect BoxSelect2">';
							html+= '<select id="'+objectId+'" class="MySelectBoxClass" multiple="multiple">';
							if(list!=null){
								for(var i=0;i<list.length;++i){
									arrHtml.push('<option value="'+ list[i].enumId +'">'+Utils.XSSEncode(list[i].code)+'-'+Utils.XSSEncode(list[i].name) +'</option>');
								}
							}
							html+= arrHtml.join("");
							html+= '</select>';
							html+= '</div>';
						}
						html+= '<div class="Clear"></div>';
						html+= '</div>';
						$('#right').append(html);
						if(valueType=="DATE_TIME"){
							VTUtilJS.applyDateTimePicker(objectId+'_DATETIME_from');
							VTUtilJS.applyDateTimePicker(objectId+'_DATETIME_to');
						}else if(valueType=="NUMBER"){
							Utils.bindFormatOnTextfield(objectId+'_NUMBER_from',Utils._TF_NUMBER);
							Utils.formatCurrencyFor(objectId+'_NUMBER_from');
							Utils.bindFormatOnTextfield(objectId+'_NUMBER_to',Utils._TF_NUMBER);
							Utils.formatCurrencyFor(objectId+'_NUMBER_to');
						}
					}
				}
				//Xong for:
				$('#right .MySelectBoxClass').each(function(){
					var aCombobox = $(this);
					if(!(aCombobox.next().hasClass('ui-dropdownchecklist'))){//Cái này để kiểm tra comboBox đó đã gọi dropdownchecklist lần nào chưa.
						//Nếu nó đã gọi dropdownchecklist 1 lần rồi, thì thể nào cũng gien ra 1 element html có class ui-dropdownchecklist nằm kế bên comboBox.
						aCombobox.dropdownchecklist({ maxDropHeight: 150,width: 277, textFormatFunction: function(options){
							var text = '';
							$('.ui-dropdownchecklist-dropcontainer input[type=checkbox]').each(function(){
								var aCheckBox = $(this);
								if(aCheckBox.parent().parent().parent().prev().prev().attr('id') == aCombobox.attr('id')){//Tức là chỉ xử lý với những checkbox thuộc combobox đó.
									if(aCheckBox.attr('checked')=='checked'){
										text += aCheckBox.next().text().split('-')[0] + ', ';
									}
								}
							});
							if(text != ''){
								text = text.substring(0, text.length-2);//bo dau ', ' cuoi cung.
							}
							return text;
						} });
					}
				});
				
			}
		});	
	},
	removeAttributes: function(){
		$('.ErrorMsgStyle').hide();
		var html = '';//để bên ngoài vòng for là đúng. Còn bên hàm toSelectAttribute thì phải để bên trong do cuộc gọi bất đồng bộ
		$('#right .checkBoxAttribute').each(function(){
			if($(this).is(':checked')){
				//remove ben right, show ben left
				$(this).parent().parent().remove(); 
				var objectType = $(this).attr('objectType');
				var objectId = $(this).attr('objectId');
				var name = $(this).attr('name');
				html+= '<li> <input name="'  + name + '" objectType="'+ objectType +'" objectId="'+objectId +'" type="checkbox"  class="InputCbxStyle"/><label style="width:150px" class="LabelStyle Label3Style">'+Utils.XSSEncode(name)+'</label></li>';
			}
		});	
		$('#left').append(html);//để bên ngoài vòng for là đúng. Còn bên hàm toSelectAttribute thì phải để bên trong do cuộc gọi bất đồng bộ
	},
	saveCustomerAttributes: function(){
		var message = '';
		var messageSaleLevel = '';
		var messageCustomerType = '';
		var messageOther = '';
		$('.ErrorMsgStyle').hide();
		var data = new Object();
		var listCustomerType= new Array();
		var listSaleLevelCatId= new Array();
		var listAttribute= new Array();
		var listAttributeDataInField=new Array();
		var nameAttr = '';
		$('.checkBoxAttribute').each(function(){
			var objectType = $(this).attr('objectType');
			var objectId = $(this).attr('objectId');
			var name = $(this).attr('name');
			
			if(objectType == 2){
				listAttribute.push(-2);//Loai kh cho id = -2 di. De ti nua if else tren action.
				listAttributeDataInField.push('CustomerType');
				messageCustomerType = 'noCheckCustomerType';
				listCustomerType= new Array();
				$('#ddcl-customerType-ddw input[type=checkbox]').each(function(){
					if($(this).attr('checked')=='checked'){
						listCustomerType.push($(this).val());
						messageCustomerType = 'haveCheckCustomerType';
					}
				});
			}else if(objectType == 3){ 
				listAttribute.push(-3);//Muc doanh so cho id = -3 di. De ti nua if else tren action.
				listAttributeDataInField.push('SaleLevel');
				messageSaleLevel = 'noCheckSaleLevel';
				listSaleLevelCatId= new Array();
				$('#saleLevel input[type=checkbox]').each(function(){
					if($(this).attr('checked')=='checked' && !$(this).hasClass('checkBoxAttribute')){
						listSaleLevelCatId.push(this.value);
//						listSaleLevelCatId.push($(this).attr('objectid'));
						//vào được đây tức là mức doanh số có ít nhất 1 combox được check.
						messageSaleLevel = 'haveCheckSaleLevel';
					}
				});
			}else if(objectType == 1){ 
				listAttribute.push(objectId);
				var valueType = $(this).attr('valueType');
				if(valueType=="CHARACTER" || valueType=="NUMBER" || valueType=="DATE_TIME"){
					var boolean = PromotionSupport.getTrue_DataOrFalse_MessageOfAutoAttribute(objectId, name, valueType);
					var arr = boolean.split('__');
					if(arr[0]=='false'){
						message = arr[1];
						return false;
					}else{
						listAttributeDataInField.push(arr[1]);
					}
				}else if(valueType=="CHOICE" || valueType=="MULTI_CHOICE"){
					messageOther = 'noCheckOther';
					var dataOfAutoAttribute = '';
					$('#ddcl-'+objectId+'-ddw input[type=checkbox]').each(function(){
						if($(this).attr('checked')=='checked'){
							dataOfAutoAttribute += $(this).val() +',';//Nhớ chú ý dấu phẩy cuối cùng
							messageOther = 'haveCheckOther';
						}
					});
					nameAttr = name;
					listAttributeDataInField.push(dataOfAutoAttribute);
				}
			}
			if(messageSaleLevel == 'noCheckSaleLevel'){
				message = 'Bạn chưa chọn giá trị cho thuộc tính Mức doanh số';
			}
			if(messageCustomerType == 'noCheckCustomerType'){
				message = 'Bạn chưa chọn giá trị cho thuộc tính Loại khách hàng';
			}
			if(messageOther == 'noCheckOther'){
				message = 'Bạn chưa chọn giá trị cho thuộc tính '+ Utils.XSSEncode(nameAttr);
			}
		});
		if(message.length > 0){
			$('#errMsgSave').html(message).show();
			return false;
		}
		data.lstCustomerType = listCustomerType;
		data.lstSaleLevelCatId = listSaleLevelCatId;
		data.lstId = listAttribute;
		data.lstAttDataInField = listAttributeDataInField;
		data.promotionId=$('#masterDataArea #id').val().trim();
		Utils.addOrSaveData(data, '/promotion-support/save-promotion-customer-attribute', PromotionSupport._xhrSave,
				'errMsgSave', null, 'loading2', '#customerAttributeMsg ');//Prefix để selector jquery mà truyền thiếu "khoảng trắng" 
		//là có thể lỗi, không xuất ra câu thông báo lỗi, hay câu thông báo thành công đâu.
		return false;
	},
	getTrue_DataOrFalse_MessageOfAutoAttribute: function(objectId, nameAttribute,valueType){
		if(valueType=="CHARACTER"){
			var msg = '';
			var dataOfAutoAttribute = $('#'+objectId+'_CHARACTER').val();
			if(dataOfAutoAttribute==''){
				msg = 'Bạn chưa nhập giá trị cho thuộc tính '+ Utils.XSSEncode(nameAttribute);
				$('#'+objectId+'_CHARACTER').focus();
				return 'false__'+msg;
			}
			msg = Utils.getMessageOfSpecialCharactersValidate(objectId+'_CHARACTER','',Utils._NAME);
			if(msg.length > 0){
				msg = 'Thuộc tính '+ Utils.XSSEncode(nameAttribute) +': Giá trị nhập vào không được chứa các ký tự đặc biệt [<>\~#&$%@*()^`\'"'; 
				return 'false__'+msg;
			}
			return 'true__'+dataOfAutoAttribute;
		}else if(valueType=="NUMBER"){
			var msg = '';
			var NUMBER_from = $('#'+objectId+'_NUMBER_from').val();
			var NUMBER_to = $('#'+objectId+'_NUMBER_to').val();
			if (NUMBER_from == '' && NUMBER_to == ''){
				msg = 'Bạn chưa nhập giá trị cho thuộc tính '+nameAttribute;
				$('#'+objectId+'_NUMBER_from').focus();
				return 'false__'+msg;
			}
			if (NUMBER_from != ''){
				NUMBER_from = parseInt(Utils.returnMoneyValue(NUMBER_from.trim()));
			}
			if (NUMBER_to != ''){
				NUMBER_to = parseInt(Utils.returnMoneyValue(NUMBER_to.trim()));
			}
			var dataOfAutoAttribute = NUMBER_from +','+NUMBER_to;//Nếu ở trên không cắt dấu phẩy đi thì sẽ ảnh hưởng!
			
			if(NUMBER_from!=='' &&  NUMBER_to!=='' &&  NUMBER_from > NUMBER_to){
				msg = 'Thuộc tính '+ Utils.XSSEncode(nameAttribute) + ': Giá trị "Từ" lớn hơn giá trị "Đến"';
				return 'false__'+msg;
			}
			return 'true__'+dataOfAutoAttribute;
		}else if(valueType=="DATE_TIME"){
			var msg = '';
			var DATETIME_from = $('#'+objectId+'_DATETIME_from').val();
			var DATETIME_to = $('#'+objectId+'_DATETIME_to').val();
			var dataOfAutoAttribute = DATETIME_from +','+DATETIME_to;
			if(DATETIME_from == '' && DATETIME_to == ''){
				msg = 'Bạn chưa nhập giá trị cho thuộc tính '+nameAttribute;
				$('#'+objectId+'_DATETIME_from').focus();
				return 'false__'+msg;
			}
			msg = Utils.getMessageOfInvalidFormatDate(objectId+'_DATETIME_from');
			if(msg.length > 0){
				msg = 'Thuộc tính '+ Utils.XSSEncode(nameAttribute) +': Giá trị "Từ" không tồn tại hoặc không đúng định dạng dd/mm/yyyy.'; 
				return 'false__'+msg;
			}
			msg = Utils.getMessageOfInvalidFormatDate(objectId+'_DATETIME_to');
			if(msg.length > 0){
				msg = 'Thuộc tính ' + Utils.XSSEncode(nameAttribute) + ': Giá trị "Đến" không tồn tại hoặc không đúng định dạng dd/mm/yyyy.'; 
				return 'false__'+msg;
			}
			if(!Utils.compareDate(DATETIME_from, DATETIME_to)){
				msg = msgErr_fromdate_greater_todate;
				msg = 'Thuộc tính '+nameAttribute+': '+msg;
				return 'false__'+msg;
			}
			return 'true__'+dataOfAutoAttribute;
		}
	},
	checkSaleLevelCatIdInList: function(idSaleCatLevel,listData3){
		if(listData3!=null && listData3.length > 0){
			for(var j=0;j<listData3.length;++j){
				if(listData3[j].idSaleCatLevel == idSaleCatLevel){
					return true;
				}
			}
		}else{
			return false;
		}
	},
	loadAppliedAttributes: function(promotionId){
		$('.ErrorMsgStyle').hide();
		if(PromotionSupport._flagAjaxTabAttribute){
			PromotionSupport._flagAjaxTabAttribute = false;
			$.getJSON("/promotion-support/load-applied-attributes?promotionId="+promotionId, function(result){
				PromotionSupport._flagAjaxTabAttribute = true;
				var promotionStatus = $('#promotionStatus').val();
				var list = result.list;
				if (list != null) {
					for (var k = 0; k < list.length; k++){
						//đoạn này viết chung cho 1 attribute
						var objectType = list[k].objectType;
						var objectId = list[k].objectId;
						var name = list[k].name;
						name = Utils.XSSEncode(name);
						var html = '';
						//end đoạn viết chung.
						if (objectType == 2) {
							html+= '<div>';
							html+= '<div class="Clear"></div>';
							html+= '<li>';
							html+= '<input name="'  + name + '" objectType="'+ objectType +'" objectId="'+objectId +'" type="checkbox"  class="InputCbxStyle checkBoxAttribute"/>';
							html+= '<label class="LabelStyle Label4Style">'+name+'</label>';
							html+= '</li>';
							html+= '<div class="BoxSelect BoxSelect2">';
							html+= '<select id="customerType" style="width: 275px;" class="MySelectBoxClass " multiple="multiple">';
							var listData2 = list[k].listData;	
							var arrHtml = new Array();
							if (listData2 != null) {
								for (var i = 0; i < listData2.length; i++) {
									if (listData2[i].checked == true) {
										arrHtml.push('<option value="'+ listData2[i].idChannelType +'" selected="selected">'+Utils.XSSEncode(listData2[i].codeChannelType) +'-'+Utils.XSSEncode(listData2[i].nameChannelType) +'</option>');
									}else{
										arrHtml.push('<option value="'+ listData2[i].idChannelType +'">'+Utils.XSSEncode(listData2[i].codeChannelType) +'-'+Utils.XSSEncode(listData2[i].nameChannelType) +'</option>');
									}
								}
							}
							html+= arrHtml.join("");
							html+= '</select>';
							html+= '</div>';
							html+= '<div class="Clear"></div>';
							html+= '</div>';
							$('#right').append(html);
						} else if (objectType == 3) {
							var listData3 = list[k].listData;	
							var objectType = 3;
							var objectId = 0;
							var name = "Mức doanh số";
							var html = '';
							html+= '<div id="saleLevel">';//Nhớ đặt id chỗ này để phân biệt, Ví dụ phân biệt: combobox(select)của mức doanh số, của loại khách hàng. 
							html+= '<div class="Clear"></div>';
							html+= 	'<li>';
							html+=	'<input name="'  + name + '" objectType="'+ objectType +'" objectId="'+objectId +'" type="checkbox"  class="InputCbxStyle checkBoxAttribute"/>';
							html+=	'<label class="LabelStyle Label4Style">'+Utils.XSSEncode(name)+'</label>';
							html+=	'</li>';
							//end đoạn giống nhau
							/*for qua cái list, add những label ngành hàng vào. Các lable Ngành hàng thì mình đặt chung 1 class "cat", để sau này mình lặp each 
							qua mỗi label, lấy catId(idProductInfoVO)của label đó, và lấy tất cả các mức mà người ta chọn, vậy cái combobox mức nên đặt id theo
							id của ngành hàng.*/
							var listProductInfoVO = list[k].listProductInfoVO;	
							if (listProductInfoVO != null) {
								for (var i = 0; i < listProductInfoVO.length; i++) {
									if (i == 0) {// if else để canh chỉnh html cho thẳng hàng đó mà.
										//label ngành hàng nên đặt 1 thuộc tính là catId
										html+= '<label catId="'+listProductInfoVO[i].idProductInfoVO +'" style="width: 150px;" class="LabelStyle Label5Style cat">'+Utils.XSSEncode(listProductInfoVO[i].codeProductInfoVO)+'</label>';
										//add combobox mức vao label nganh hang, combobox nên đặt id theo ngành hàng.
										html+= '<div class="BoxSelect BoxSelect2">';
										html+= '<select id="saleLevel-'+listProductInfoVO[i].idProductInfoVO +'" class="MySelectBoxClass saleLevelCat" multiple="multiple">';
										var listSaleLevelCat = listProductInfoVO[i].listSaleCatLevelVO;
										var arrHtml = new Array();
										if (listSaleLevelCat != null) {
											for(var t=0;t<listSaleLevelCat.length;++t){
												var checked = PromotionSupport.checkSaleLevelCatIdInList(listSaleLevelCat[t].idSaleCatLevel,listData3);
												if(checked == true){
													arrHtml.push('<option value="'+ listSaleLevelCat[t].idSaleCatLevel +'" selected="selected">'+ Utils.XSSEncode(listSaleLevelCat[t].codeSaleCatLevel) +'-'+Utils.XSSEncode(listSaleLevelCat[t].nameSaleCatLevel) +'</option>');
												}else{
													arrHtml.push('<option value="'+ listSaleLevelCat[t].idSaleCatLevel +'">'+ Utils.XSSEncode(listSaleLevelCat[t].codeSaleCatLevel) +'-'+Utils.XSSEncode(listSaleLevelCat[t].nameSaleCatLevel) +'</option>');
												}
											}
										}
										html+= arrHtml.join("");
										html+= '</select>';
										html+= '</div>';
									}else{
										html+= '<div class="Clear"></div>';
										html+= '<label catId="'+listProductInfoVO[i].idProductInfoVO +'" style="width: 150px;" class="LabelStyle Label6Style">'+Utils.XSSEncode(listProductInfoVO[i].codeProductInfoVO)+'</label>';
										html+= '<div class="BoxSelect BoxSelect2">';
										html+= '<select id="saleLevel-'+listProductInfoVO[i].idProductInfoVO +'" class="MySelectBoxClass saleLevelCat" multiple="multiple">';
										var listSaleLevelCat = listProductInfoVO[i].listSaleCatLevelVO;
										var arrHtml = new Array();
										if(listSaleLevelCat!=null){
											for(var t=0;t<listSaleLevelCat.length;++t){
												var checked = PromotionSupport.checkSaleLevelCatIdInList(listSaleLevelCat[t].idSaleCatLevel,listData3);
												if(checked == true){
													arrHtml.push('<option value="'+ listSaleLevelCat[t].idSaleCatLevel +'" selected="selected">'+ Utils.XSSEncode(listSaleLevelCat[t].codeSaleCatLevel) +'-'+Utils.XSSEncode(listSaleLevelCat[t].nameSaleCatLevel) +'</option>');
												}else{
													arrHtml.push('<option value="'+ listSaleLevelCat[t].idSaleCatLevel +'">'+ Utils.XSSEncode(listSaleLevelCat[t].codeSaleCatLevel) +'-'+Utils.XSSEncode(listSaleLevelCat[t].nameSaleCatLevel) +'</option>');
												}
											}
										}
										html+= arrHtml.join("");
										html+= '</select>';
										html+= '</div>';
									}
								}
							}
							html+= '<div class="Clear"></div>';
							html+= '</div>';
							$('#right').append(html);
						}else if(objectType == 1){
							var valueType = list[k].valueType;
							var listData1 = list[k].listData;
							
							html+= '<div>';
							html+= '<div class="Clear"></div>';
							html+= '<li>';
							//thằng input của thuộc tính động cần bỏ valueType vào để sau này xử lý khi lưu.
							html+= '<input valueType="'+valueType+'"  name="'  + name + '" objectType="'+ objectType +'" objectId="'+objectId +'" type="checkbox"  class="InputCbxStyle checkBoxAttribute"/>';
							html+= '<label class="LabelStyle Label4Style">'+Utils.XSSEncode(name)+'</label>';
							html+= '</li>';
								
							if(listData1!=null){
								if(valueType=="CHARACTER"){
									//Nên đặt id html của text CHARACTER này theo attributeId cộng chuỗi CHARACTER  thì sẽ không bị conflict trong danh sách. Sau này muốn lưu thì
									//select theo attributeId để lấy ra dữ liệu.
									html+= '<input id="'+objectId+'_CHARACTER" type="text" style="width: 377px;" value="'+listData1[0]+'" class="InputTextStyle InputText1Style"/>';
								}else if(valueType=="NUMBER"){
									//Đặt id theo: attributeId_NUMBER_from và attributeId_NUMBER_to:
									var from = '';
									var to = '';
									if(listData1[0]!=null){
										from = listData1[0];
									}
									if(listData1[1]!=null){
										to = listData1[1];
									}
									html+= '<input id="'+objectId+'_NUMBER_from" style="width: 170px;" type="text" value="'+formatCurrency(from)+'" class="InputTextStyle InputText5Style"/>';
									html+= '<label class="LabelStyle Label7Style">-</label>';
									html+= '<input id="'+objectId+'_NUMBER_to" style="width: 170px;" type="text" value="'+formatCurrency(to)+'" class="InputTextStyle InputText5Style"/>';
								}else if(valueType=="DATE_TIME"){
									//Đặt id theo: attributeId_DATETIME_from và attributeId_DATETIME_to. Nhớ applyDateTimePicker applyDateTimePicker("#endDate");
									//Mình đặt chung bọn chúng 1 class rồi,applyDateTimePicker cho class đó được không nhỉ. Tí thử. 
									var from = '';
									var to = '';
									if(listData1[0]!=null){
										from = listData1[0];
									}
									if(listData1[1]!=null){
										to = listData1[1];
									}
									html+= '<input id="'+objectId+'_DATETIME_from"  style="width: 146px;"  type="text" value="'+from+'" class="InputTextStyle InputText6Style"/>';
									html+= '<label class="LabelStyle Label7Style">-</label>';
									html+= '<input id="'+objectId+'_DATETIME_to" style="width: 146px;"  type="text" value="'+to+'" class="InputTextStyle InputText6Style"/>';
								}else if(valueType=="CHOICE" || valueType=="MULTI_CHOICE"){
									var arrHtml = new Array();
									html+= '<div class="BoxSelect BoxSelect2">';
									html+= '<select id="'+objectId+'" class="MySelectBoxClass" multiple="multiple">';
									for(var i=0;i<listData1.length;++i){
										if(listData1[i].checked == true){
											arrHtml.push('<option value="'+ listData1[i].enumId +'" selected="selected">'+Utils.XSSEncode(listData1[i].code)+'-'+Utils.XSSEncode(listData1[i].name) +'</option>');
										}else{
											arrHtml.push('<option value="'+ listData1[i].enumId +'">'+Utils.XSSEncode(listData1[i].code)+'-'+Utils.XSSEncode(listData1[i].name) +'</option>');
										}
									}
									html+= arrHtml.join("");
									html+= '</select>';
									html+= '</div>';
								}
							}
							html+= '<div class="Clear"></div>';
							html+= '</div>';
							$('#right').append(html);
							//dự thảo thì ko disable
							if(valueType=="DATE_TIME"){
								VTUtilJS.applyDateTimePicker(objectId+'_DATETIME_from');
								VTUtilJS.applyDateTimePicker(objectId+'_DATETIME_to');
							}else if(valueType=="NUMBER"){
								Utils.bindFormatOnTextfield(objectId+'_NUMBER_from',Utils._TF_NUMBER);
								Utils.formatCurrencyFor(objectId+'_NUMBER_from');
								Utils.bindFormatOnTextfield(objectId+'_NUMBER_to',Utils._TF_NUMBER);
								Utils.formatCurrencyFor(objectId+'_NUMBER_to');
							}
						}
					}
					//Xong for:
					$('#right .MySelectBoxClass').each(function(){
						var aCombobox = $(this);
						if(!(aCombobox.next().hasClass('ui-dropdownchecklist'))){//Cái này để kiểm tra comboBox đó đã gọi dropdownchecklist lần nào chưa.
							//Nếu nó đã gọi dropdownchecklist 1 lần rồi, thì thể nào cũng gien ra 1 element html có class ui-dropdownchecklist nằm kế bên comboBox.
							aCombobox.dropdownchecklist({ maxDropHeight: 150,width: 277, textFormatFunction: function(options){
								var text = '';
								$('.ui-dropdownchecklist-dropcontainer input[type=checkbox]').each(function(){
									var aCheckBox = $(this);
									if(aCheckBox.parent().parent().parent().prev().prev().attr('id') == aCombobox.attr('id')){//Tức là chỉ xử lý với những checkbox thuộc combobox đó.
										if(aCheckBox.attr('checked')=='checked'){
											text += aCheckBox.next().text().split('-')[0] + ', ';
										}
									}
								});
								if(text != ''){
									text = text.substring(0, text.length-2);//bo dau ', ' cuoi cung.
								}
								return text;
							} });
						}
					});
				}
				//ko dự thảo thì disable.
				if(promotionStatus != 2 /*|| $('#permissionUser').val() == 'false'*/){
					$("#right .MySelectBoxClass").dropdownchecklist("disable");
					$("#right :input").attr('disabled',true);
					$("#left :input").attr('disabled',true);
					$('#updateCustomerAttribute').hide();
					$('#right .CalendarLink').hide();
				}
			});	
		}
	}
}
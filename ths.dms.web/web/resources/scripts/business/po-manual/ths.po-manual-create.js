/**
 * POManualCreate
 * @author vuongmq
 * @since Dec 04, 2015
 */
var POManualCreate = {
	_poNumberParent: null,
	_mapPoParent: null,
	_warehouseIdOld: null,
	_TYPE: null, // loai don, 
	_LOAD_POP_FIRST: true,
	_isvalidQuantityPO: true,
	/**
	 * saveOrderManual
	 * @author vuongmq
	 * @params action
	 * @since Dec 08, 2015
	 */
	saveOrderManual: function(action) {
		hideAllMessage();
		var msg = '';
		var shopCode = '';
		if (action == ActionType.INSERT) {
			shopCode = $('#cbxShop').combobox('getValue');
		} else {
			shopCode = $('#shopCode').val();
		}
		if (isNullOrEmpty(shopCode)) {
			msg = 'Vui lòng chọn đơn vị';
		}
		var type = $('#type').val();
		if (msg.length == 0) {
			if (isNullOrEmpty(type) || (PoManualType.PO_IMPORT != type && PoManualType.PO_RETURN != type)) {
				msg = 'Loại đơn không hợp lệ. Vui lòng chọn loại đơn';
			}
		}
		if (msg.length == 0) {
			msg = Utils.getMessageOfRequireCheck('poDate', 'Ngày lập đơn');
		}
		if (msg.length == 0){
			msg = Utils.getMessageOfInvalidFormatDate('poDate', 'Ngày lập đơn');
		}
		var poDate = $('#poDate').val();
		var curDate = ReportUtils.getCurrentDateString();
		if (msg.length == 0 && !Utils.compareDate(poDate, curDate)) {
			msg = 'Ngày lập đơn không được lớn hơn ngày hiện tại. Vui lòng nhập lại';
			$('#poDate').focus();
		}
		var requestDate = $('#requestDate').val();
		if (msg.length == 0 && !Utils.compareDate(curDate, requestDate)) {
			msg = 'Ngày yêu cầu giao hàng phải lớn hơn hoặc bằng ngày hiện tại. Vui lòng nhập lại';
			$('#poDate').focus();
		}
		var warehouseId = null;
		var poNumberParent = null;
		var lstSaleProduct = [];
		var tmp = $('#gridSalePOData').handsontable('getInstance');
		var rows = tmp.getData();
		if (type == PoManualType.PO_IMPORT) {
			if (msg.length == 0) {
				if (action == ActionType.INSERT) {
					warehouseId = $('#warehouse').combobox('getValue');
				} else {
					warehouseId = $('#warehouse').val();
				}
				if (isNullOrEmpty(warehouseId) || warehouseId < activeType.STOPPED) {
					msg = 'Vui lòng chọn kho';
				}
			}
			if (msg.length == 0) {
				// kiem tra dong khong nhap so luong bao loi
				var mapProductQuantity = new Map(); // su dung validate SP trung
				for (var i = 0, sz = rows.length; i < sz; i++) {
					var row = rows[i];
					var poLine = i + 1;
					if (row != null && !isNullOrEmpty(row[COLSALEPO.PRO_CODE])) {
						var productCode = row[COLSALEPO.PRO_CODE];
						var soLuong = row[COLSALEPO.QUANTITY];
						if (!GridSalePOBusiness.isValidQuantityInputPO(soLuong)) {
							msg = format('{0} sản phẩm {1} phải đúng định dạng và số nguyên dương. Vui lòng nhập lại', GridSalePOBusiness._quantityView, productCode);
							break;
						} else {
							//soLuong = Utils.returnMoneyValue(soLuong);
							var rowTmp = GridSalePOBusiness._mapSaleProduct.get(productCode);
							if (rowTmp != null) {
								soLuong = getQuantity(soLuong, rowTmp.convfact);
								if (isNullOrEmpty(soLuong) || isNaN(Number(soLuong)) || Number(soLuong) <= 0) {
									msg = format('Số lượng đặt sản phẩm {0} phải là số nguyên dương. Vui lòng nhập lại', productCode);
									break;
								}
								if (mapProductQuantity.findIt(productCode) > -1) {
									msg = format('Tồn tại sản phẩm {0} nhiều hơn một dòng. Vui lòng nhập lại', productCode);
									break;
								} else {
									mapProductQuantity.put(productCode, soLuong);
									var productVO = new Object();
									productVO.productId = rowTmp.productId;
									productVO.productCode = productCode;
									productVO.quantity = soLuong;
									productVO.poLineNumber = poLine;
									lstSaleProduct.push(productVO);
								}
							}
						}
					}
				}
			}
		} else {
			if (msg.length == 0) {
				poNumberParent = $('#poNumberParent').val();
				if (isNullOrEmpty(poNumberParent)) {
					msg = 'Vui lòng F9 chọn đơn gốc';
				}
			}
			if (msg.length == 0) {
				var flagReturn = true; // dong nao khong nhap so luong thi bo qua; van luu san pham khong luu so luong
				for (var i = 0, sz = rows.length; i < sz; i++) {
					var row = rows[i];
					var poLine = i + 1;
					if (row != null && !isNullOrEmpty(row[COLSALEPO.PRO_CODE])) {
						var productCode = row[COLSALEPO.PRO_CODE];
						var soLuong = row[COLSALEPO.QUANTITY];
						if (!GridSalePOBusiness.isValidQuantityInputPO(soLuong)) {
							msg = format('{0} sản phẩm {1} phải đúng định dạng và số nguyên dương. Vui lòng nhập lại', GridSalePOBusiness._quantityView, productCode);
							break;
						} else {
							var rowTmp = GridSalePOBusiness._mapSaleProduct.get(productCode);
							if (rowTmp != null && rowTmp.convfact != null) {
								if (!isNullOrEmpty(soLuong)) {
									//soLuong = Number(Utils.returnMoneyValue(soLuong));
									soLuong = getQuantity(soLuong, rowTmp.convfact);
									if (isNaN(soLuong) || soLuong <= 0) {
										msg = format('Số lượng trả sản phẩm {0} phải là số nguyên dương. Vui lòng nhập lại', productCode);
										break;
									}
									//var soLuongLe = soLuong * rowTmp.convfact;
									if (rowTmp.availableQuantity != null) {
										if (soLuong > rowTmp.availableQuantity) {
											msg = format('Số lượng trả sản phẩm {0} vượt quá tồn kho đáp ứng. Vui lòng nhập lại', productCode);
											break;
										}
									} else {
										msg = format('Số lượng trả sản phẩm {0} vượt quá tồn kho đáp ứng. Vui lòng nhập lại', productCode);
										break;
									}
									if (rowTmp.oldQuantity == null || soLuong > rowTmp.oldQuantity) {
										// oldQuantity: so luong le: quantity
										msg = format('Số lượng trả sản phẩm {0} vượt quá số lượng đơn trả. Vui lòng nhập lại', productCode);
										break;
									}
									flagReturn = false;
								}
								var productVO = new Object();
								productVO.productId = rowTmp.productId;
								productVO.productCode = productCode;
								productVO.quantity = soLuong;
								productVO.poLineNumber = poLine;
								lstSaleProduct.push(productVO);
							} else {
								msg = format('Sản phẩm {0} không có số lượng quy đổi thùng/lẻ', productCode);
								break;
							}
						}
					}
				}
				if (msg.length == 0) {
					if (rows.length == 0) {
						msg = 'Không tồn tại sản phẩm trả. Vui lòng F9 chọn đơn gốc';
					}
				}
				if (msg.length == 0) {
					if (flagReturn) {
						msg = 'Không tồn tại số lượng sản phẩm trả. Vui lòng nhập số lượng sản phẩm trả';
					}
				}
			}
		}
		if (msg.length > 0) {
			$('#errorMsg').html(msg).show();
			return false;
		}
		var params = new Object();
		params.valueEdit = action;
		params.shopCode = shopCode;
		params.type = type;
		//params.poDate = poDate; // lay ngay hien tai cua he thong
		params.requestDate = requestDate;
		params.note = $('#poNote').val();
		params.lstSaleProduct = lstSaleProduct;
		var message = '';
		if (action == ActionType.INSERT) {
			if (type == PoManualType.PO_IMPORT) {
				message = 'Bạn có muốn tạo đơn đặt hàng này?';
				params.warehouseId = warehouseId;
			} else {
				message = 'Bạn có muốn tạo đơn trả hàng này?';
				params.poAutoNumber = poNumberParent;
			}
		} else {
			params.poId = $('#poId').val();
			if (type == PoManualType.PO_IMPORT) {
				message = 'Bạn có muốn cập nhật đơn đặt hàng này?';
			} else {
				message = 'Bạn có muốn cập nhật đơn trả hàng này?';
			}
		}
		JSONUtil.saveData2(params, '/po-manual/save-order-manual', message, "errorMsg", function(data) {
			if (data != null) {
				if (action == ActionType.INSERT) {
					$('#poNumber').val(data.poNumber);
					disabled('btnSaveCreate');
				} else {
					setTimeout(function() { 
						window.location.href = '/po-manual-manage/info'; 
					}, 1500);
				}
			}
			var tm = setTimeout(function() {
				$('#successMsg').html("").hide();
				clearTimeout(tm);
				//window.location.href = '';
			 }, 1500);
		});
	},

	/**
	 * redirectOrderManual
	 * @author vuongmq
	 * @since Dec 08, 2015
	 */
	redirectOrderManual: function() {
		$.messager.confirm(jsp_common_xacnhan, 'Bạn có muốn <b>tạo lại 1 đơn hàng mới</b>?', function(r) {
			if (r) {
				window.location.href = '/po-manual/create-manual';
			}
		});
	},

	/**
	 * getAmountProduct
	 * @author vuongmq
	 * @params pkPrice
	 * @params pkQuantity
	 * @params price
	 * @params quantity
	 * @since Dec 08, 2015
	 */
	getAmountProduct: function(pkPrice, pkQuantity, price, quantity) {
		var amount = 0;
		if (pkPrice != null && pkQuantity != null && price != null && quantity != null) {
			var pkPriceNum =  Utils.returnMoneyValue(pkPrice);
			var pkQuantityNum =  Utils.returnMoneyValue(pkQuantity);
			var priceNum =  Utils.returnMoneyValue(price);
			var quantityNum =  Utils.returnMoneyValue(quantity);
			amount = (Number(pkPriceNum) * Number(pkQuantityNum)) + (Number(priceNum) * Number(quantityNum));
		} else if (pkPrice != null && pkQuantity != null) {
			var pkPriceNum =  Utils.returnMoneyValue(pkPrice);
			var pkQuantityNum =  Utils.returnMoneyValue(pkQuantity);
			amount = Number(pkPriceNum) * Number(pkQuantityNum);
		} else if (price != null && quantity != null) {
			var priceNum =  Utils.returnMoneyValue(price);
			var quantityNum =  Utils.returnMoneyValue(quantity);
			amount = Number(priceNum) * Number(quantityNum);
		}
		return amount;
	},

	/**
	 * showValidateError
	 * @author vuongmq
	 * @params errText
	 * @since Dec 08, 2015
	 */
	showValidateError: function(errText) {
		$('#errorMsg').html(errText).show();											
		setTimeout(function() {
			$('#errorMsg').html('').hide();
		}, 3000);
	},

	/**
	 * changeType
	 * @author vuongmq
	 * @since Dec 14, 2015
	 */
	changeType: function() {
		hideAllMessage();
		var type = $('#type').val();
		if (type == PoManualType.PO_RETURN) {
			var warehouseIdOld = $('#warehouse').combobox('getValue');
			POManualCreate._warehouseIdOld = warehouseIdOld;
			$('#warehouse').combobox('setValue', '');
			disableCombo('warehouse');
			enable('poNumberParent');
			$('#poNote').val('');
			POManualCreate._TYPE = PoManualType.PO_RETURN;
			GridSalePOBusiness.disableSalePOGrid();
		} else {
			enableCombo('warehouse');
			$('#warehouse').combobox('setValue', POManualCreate._warehouseIdOld);
			$('#poNumberParent').val('');
			$('#poNote').val('');
			disabled('poNumberParent');
			POManualCreate._TYPE = PoManualType.PO_IMPORT;
			var shopId = $('#shopId').val();
			var warehouseId = $('#warehouse').combobox('getValue');
			GridSalePOBusiness.getProductPO(shopId, warehouseId);
		}
		GridSalePOBusiness.initSalePOGrid();
	},

	/**
	 * changePoNumberParent
	 * @author vuongmq
	 * @param idx
	 * @since Dec 14, 2015 
	 */
	changePoNumberParent: function(idx) {
		hideAllMessage();
		var url = '/po-manual/change-po-number-parent';
		var params = {};
		params.shopId = $('#shopId').val();
		params.poAutoNumber = $('#poNumberParent').val().trim();
    	Utils.getJSONDataByAjaxNotOverlay(params, url, function(data) {
			/** Lay record po asn*/
			if (data != null && data.poParent != null && data.id != null) {
				POManualCreate.selectPoParent(null, data.poParent, data.id);
			} else {
				GridSalePOBusiness.disableSalePOGrid();
				GridSalePOBusiness.resetSalePOGrid(PoManualType.PO_RETURN);
				$('#errMsgInfo').html('Số đơn gốc không tồn tại. Vui lòng nhập lại').show();
			}
		});
	},

	/**
	 * changeType
	 * @author vuongmq
	 * @since Dec 14, 2015
	 */
	openPopupPoParent: function() {
		hideAllMessage();
		var html = $('#popupPoNumberParentDiv').html();
		$('#popupPoNumberParent').dialog({
			title: 'Danh sách đơn trả hàng',
			width: 800,
			height: 'auto',
			onOpen: function() {
				$('#popOrderNumber').focus();
				$('#popOrderNumber').val('');
				$('#popAsn').val('');
				setDateTimePicker('popFromDate');	
				setDateTimePicker('popToDate');
				$('#popFromDate').val(getLastWeek());
				ReportUtils.setCurrentDateForCalendar('popToDate');
				POManualCreate.initGridPoParent();
				if (POManualCreate._LOAD_POP_FIRST) {
					Utils.bindAutoButtonEx('#popupPoNumberParent','popBtnSearch');
					POManualCreate._LOAD_POP_FIRST = false;
				}
			},
			onClose: function() {
				$('#popupPoNumberParentDiv').html(html);
			}
		});
		$('#popupPoNumberParent').dialog('open');
	},
	
	/**
	 * Search danh sach don hang goc
	 * @author vuongmq
	 * @since Dec 14, 2015 
	 */
	searchPoParent: function() {
		var params = POManualCreate.getSearchPoParentParams();
		$('#gridPoParent').datagrid('load', params);
	},

	/**
	 * Lay param tim kiem danh sach don hang goc
	 * @author vuongmq
	 * @since Dec 14, 2015 
	 */
	getSearchPoParentParams: function() {
		var params = {};
		params.shopId = $('#shopId').val().trim();
		params.orderNumber = $('#popOrderNumber').val().trim();
		params.poAutoNumber = $('#popAsn').val().trim();
		params.fromDate = $('#popFromDate').val().trim();
		params.toDate = $('#popToDate').val().trim();
		return params;
	},

	/**
	 * Init danh sach don hang goc
	 * @author vuongmq
	 * @since Dec 14, 2015 
	 */
	initGridPoParent: function() {
		var params = POManualCreate.getSearchPoParentParams();
		$('#gridPoParent').datagrid({  
		    url: '/po-manual/get-po-vnm-return',
		    rownumbers: true,
		    pagination: true,
		    singleSelect: false,
		    autoRowHeight: true,
		    pageNumber: 1,
		    pageSize: 10,
		    pageList: [10, 50, 100],
		    width: 740,	    		    
		    queryParams: params,
		    fitColumns: true,
		    scrollbarSize: 0,
		    /*data: [{id: 1, orderNumber: 'PO23123', asn: 'ASN01023', dateAsn: '11/12/2015', productCode: '410002', productName: 'sản phẩm a'
					, convfact: 24, availableQuantity: 1234, quantity: 2345, packagePrice: 40000, price: 2000}],*/
		    columns: [[
				{field: 'orderNumber', title: 'Số đơn hàng', width: 130, align: 'left', formatter: function(value, row, index) {
					return VTUtilJS.XSSEncode(value);
				}},
				{field: 'asn', title: 'Số ASN', width: 130, align: 'left', formatter: function(value, row, index) {
		        	return VTUtilJS.XSSEncode(value);
		        }},
				{field: 'poDateStr', title: 'Ngày tạo ASN', width: 60, align: 'center', formatter: function(value, row, index) {
					return VTUtilJS.XSSEncode(value);
				}},
		        {field: 'change', title: '', width: 50, align: 'center', formatter: function(value, row, index) {
			    	var html = '<a href="javascript:void(0)" onclick="return POManualCreate.selectPoParent(' + index + ', \'' + VTUtilJS.XSSEncode(row.asn) + '\', ' + row.id + ');"> Chọn</a>';
			    	return html;
			    }}, 
		    ]],
		    onLoadSuccess: function(data) {
		    	$('.datagrid-header-rownumber').html('STT');
			    updateRownumWidthForJqGrid('#searchPoParentResult');			   	 
		   	 	$(window).resize();
		    }		    
		});
	},

	/**
	 * selectPoParent
	 * @author vuongmq
	 * @param index
	 * @param poParent
	 * @param id
	 * @since Dec 14, 2015 
	 */
	selectPoParent: function(index, poParent, id) {
		//alert('index:' + index);
		if (id != null && poParent != null) {
			$('#poNumberParent').val(poParent);
			var url = '/po-manual/get-po-vnm-detail-return';
			var params = {};
			params.shopId = $('#shopId').val();
			params.poVnmId = id;
	    	Utils.getJSONDataByAjaxNotOverlay(params, url, function(data) {
				/** Lay danh sach san pham ban*/
				GridSalePOBusiness._listSaleProduct = data.listPoVNMDetail;
				GridSalePOBusiness._mapSaleProduct = new Map();
				GridSalePOBusiness._listSaleProductCode = new Array();
				if (GridSalePOBusiness._listSaleProduct != null && GridSalePOBusiness._listSaleProduct != undefined) {
					for (var i = 0, sz = GridSalePOBusiness._listSaleProduct.length; i < sz; i++) {
						var saleProduct = GridSalePOBusiness._mapSaleProduct.get(GridSalePOBusiness._listSaleProduct[i].productCode); 
						if (saleProduct == null) {
							GridSalePOBusiness._mapSaleProduct.put(GridSalePOBusiness._listSaleProduct[i].productCode, GridSalePOBusiness._listSaleProduct[i]);
							GridSalePOBusiness._listSaleProductCode.push(Utils.XSSEncode(GridSalePOBusiness._listSaleProduct[i].productCode + ' - ' + GridSalePOBusiness._listSaleProduct[i].productName));
						}
					}
					GridSalePOBusiness.resetSalePOGrid(PoManualType.PO_RETURN);
				} else {
					GridSalePOBusiness.disableSalePOGrid();
				}
				$('#popupPoNumberParent').dialog('close');
			});
		}
	},

	/**
	 * Ham Lay changeColumnIndex
	 * @author vuongmq
	 * @since 24/12/2015
	 * */
	changeColumnIndex: function() {
		if (POManualCreate._TYPE == PoManualType.PO_IMPORT) {
			COLSALEPO.PRO_CODE = 0;
			COLSALEPO.PRO_NAME = 1;
			COLSALEPO.CONVFACT = 2; 
			COLSALEPO.PACKING_PRICE = 3; 
			COLSALEPO.AVAILABLE_QUANTITY = 4; 
			COLSALEPO.STOCK = 5; 
			COLSALEPO.QUANTITY = 6; 
			COLSALEPO.AMOUNT = 7; 
			COLSALEPO.DELETE = 8;
		} else {
			COLSALEPO.PRO_CODE = 0;
			COLSALEPO.PRO_NAME = 1;
			COLSALEPO.CONVFACT = 2; 
			COLSALEPO.PACKING_PRICE = 3; 
			COLSALEPO.AVAILABLE_QUANTITY = 4; 
			COLSALEPO.STOCK = 5; 
			COLSALEPO.OLD_QUANTITY = 6;
			COLSALEPO.QUANTITY = 7; 
			COLSALEPO.AMOUNT = 8; 
			COLSALEPO.DELETE = 9;
		}
	},
};

var COLSALEPO = {
	PRO_CODE: 0, PRO_NAME: 1, CONVFACT: 2, PACKING_PRICE: 3, AVAILABLE_QUANTITY: 4, STOCK: 5, QUANTITY: 6, AMOUNT: 7, DELETE: 8
};

////////////////////////////////// BEGIN GridSalePOBusiness ///////////////////////////////////////////////
var GridSalePOBusiness = {
	_LOAD_DATA: 'loadData',
	_listSaleProduct: new Array(), //danh sach san pham
	_listSaleProductCode: new Array(), //danh sach ma san pham
	_mapSaleProduct: new Map(), //map danh sach san pham
	_mapSaleProductGrid: new Map(), //map danh sach san pham ban
	_mapPoProductEdit: new Map(), //map danh sach san pham po Edit de view
	_stt: 'STT',
	_productCode: 'Mã sản phẩm',
	_productName: 'Tên sản phẩm',
	_convfact: 'Quy cách',
	_packing_price: 'Đơn giá',
	_priceOUM1: 'Giá thùng',
	_priceOUM2: 'Giá lẻ',
	_warehouse: 'Kho',
	_availableQuantity: 'Tồn kho đáp ứng',
	_stock: 'Tồn kho thực tế',
	_old_quantity: 'Số lượng đơn trả',
    _quantityInput: 'Số lượng thực đặt',
    _quantityOutput: 'Số lượng trả',
    _quantityView: '',
    _amount: 'Thành tiền',
    _deleteSaleProduct: 'Xóa',

    /**
	 * Ham Lay danh sach San pham
	 * @author vuongmq
	 * @param shopId
	 * @since 07/12/2015
	 * */
    getProductPO: function(shopId, warehouseId) {
    	var url = '/po-manual/get-product';
		var params = {};
		if (shopId != null && warehouseId != null) {
			params.shopId = shopId;
			params.warehouseId = warehouseId;
		} else {
			GridSalePOBusiness.initSalePOGrid();
			GridSalePOBusiness.disableSalePOGrid();
			return false;
		}
    	Utils.getJSONDataByAjaxNotOverlay(params, url, function(data) {
    		/** Lay danh sach san pham ban*/
			GridSalePOBusiness._listSaleProduct = data.listSaleProduct;
			GridSalePOBusiness._mapSaleProduct = new Map();
			GridSalePOBusiness._listSaleProductCode = new Array();
			if (GridSalePOBusiness._listSaleProduct != null && GridSalePOBusiness._listSaleProduct != undefined) {
				for (var i = 0, sz = GridSalePOBusiness._listSaleProduct.length; i < sz; i++) {
					var saleProduct = GridSalePOBusiness._mapSaleProduct.get(GridSalePOBusiness._listSaleProduct[i].productCode); 
					if (saleProduct == null) {
						GridSalePOBusiness._mapSaleProduct.put(GridSalePOBusiness._listSaleProduct[i].productCode, GridSalePOBusiness._listSaleProduct[i]);
						GridSalePOBusiness._listSaleProductCode.push(Utils.XSSEncode(GridSalePOBusiness._listSaleProduct[i].productCode + ' - ' + GridSalePOBusiness._listSaleProduct[i].productName));
					}
				}
				GridSalePOBusiness.resetSalePOGrid(PoManualType.PO_IMPORT);
			} else {
				GridSalePOBusiness.disableSalePOGrid();
			}
    	});
    },

    /**
	 * Ham formatter cho cac column
	 * @author vuongmq
	 * @param instance: gia tri hien hanh khi thao tac grid
	 * @param td: table td cu ly cac thuoc tinh cua td 
	 * @param row: thu tu cua row
	 * @param col: thu tu cua column
	 * @param prop: cac thuoc tinh cua grid
	 * @param value: gia tri cua cell
	 * @param cellProperties: thuoc tinh cua cell khi format
	 * @since 07/12/2015
	 * */
	styleTextStockNegative: function (instance, td, row, col, prop, value, cellProperties) {
		Handsontable.renderers.TextRenderer.apply(this, arguments);
		var proCode = $('#gridSalePOData').handsontable('getInstance').getDataAtCell(row, COLSALEPO.PRO_CODE);
		var pro = GridSalePOBusiness._mapSaleProduct.get(proCode);
		if (col == COLSALEPO.PRO_NAME || col == COLSALEPO.CONVFACT || col == COLSALEPO.PACKING_PRICE || COLSALEPO.AVAILABLE_QUANTITY || COLSALEPO.STOCK || col == COLSALEPO.QUANTITY || col == COLSALEPO.AMOUNT) {
			if (!isNullOrEmpty(proCode)) {
				if (col == COLSALEPO.CONVFACT || col == COLSALEPO.PACKING_PRICE || col == COLSALEPO.AVAILABLE_QUANTITY || col == COLSALEPO.STOCK || col == COLSALEPO.OLD_QUANTITY || col == COLSALEPO.QUANTITY  || col == COLSALEPO.AMOUNT) {
					td.style.textAlign = "right";
				} else if (col == COLSALEPO.PRO_NAME) {
					td.style.textAlign = "left";
				}
			}
		}
	},

	/**
	 * Ham lay thong tin cac cot cua grid
	 * @author vuongmq
	 * @since Dec 08, 2015
	 */
	getColumnProperty: function() {
		var col = [{}];
		if (POManualCreate._TYPE == PoManualType.PO_IMPORT) {
			col = [
	           // 1 product code
				{
					type : 'autocomplete',
					source: GridSalePOBusiness._listSaleProductCode,
					strict: false,
					allowInvalid: false,
					renderer: GridSalePOBusiness.productCodeRenderer
				},
				// 2 product name
				{
					readOnly: true,
					renderer: GridSalePOBusiness.styleTextStockNegative
				},
				// 3 convfact
				{
					readOnly: true,
					renderer: GridSalePOBusiness.styleTextStockNegative
				},
				// 4 available quantity
				{
					readOnly: true,
					renderer: GridSalePOBusiness.styleTextStockNegative
				},
				// 5 stock (quantity)
				{
					readOnly: true,
					renderer: GridSalePOBusiness.styleTextStockNegative
				},
				// 6 price thung/le 
				{
					readOnly: true,
	//				type: 'numeric',
	//				format: '0,0.00',
					renderer: GridSalePOBusiness.styleTextStockNegative
				},
				// 7 quantity (thuc dat)
				{
					renderer: GridSalePOBusiness.styleTextStockNegative,
	//				validator: SPCreateOrder.convfactValidation,
					allowInvalid: false
				},
				// 8 amount
				{
					readOnly: true,
	//				type: 'numeric',
	//			    format: '0,0.00',
					renderer: GridSalePOBusiness.styleTextStockNegative
				},
				// 9 delete
				{
					renderer: GridSalePOBusiness.deleteItemRenderer
				},
				
			];
		} else {
			col = [
	           // 1 product code
				{
					readOnly: true,
					strict: false,
					allowInvalid: false,
					renderer: GridSalePOBusiness.productCodeRenderer
				},
				// 2 product name
				{
					readOnly: true,
					renderer: GridSalePOBusiness.styleTextStockNegative
				},
				// 3 convfact
				{
					readOnly: true,
					renderer: GridSalePOBusiness.styleTextStockNegative
				},
				// 4 available quantity
				{
					readOnly: true,
					renderer: GridSalePOBusiness.styleTextStockNegative
				},
				// 5 stock (quantity)
				{
					readOnly: true,
					renderer: GridSalePOBusiness.styleTextStockNegative
				},
				// 5.1 oldQuantity (oldQuantity) // so luong don tra
				{
					readOnly: true,
					renderer: GridSalePOBusiness.styleTextStockNegative
				},
				// 6 price thung/le
				{
					readOnly: true,
	//				type: 'numeric',
	//				format: '0,0.00',
					renderer: GridSalePOBusiness.styleTextStockNegative
				},
				// 7 quantity (thuc dat)
				{
					renderer: GridSalePOBusiness.styleTextStockNegative,
	//				validator: SPCreateOrder.convfactValidation,
					allowInvalid: false
				},
				// 8 amount
				{
					readOnly: true,
	//				type: 'numeric',
	//			    format: '0,0.00',
					renderer: GridSalePOBusiness.styleTextStockNegative
				},				
			];
		}
		return col;
	},
	
	/**
	 * Ham set property cell trong hansontable
	 * @author vuongmq
	 * @params instance
	 * @params td
	 * @params row
	 * @params col
	 * @params prop
	 * @params value
	 * @params cellProperties
	 * @since Dec 08, 2015
	 */
	deleteItemRenderer: function(instance, td, row, col, prop, value, cellProperties) {
		if ($("#gridSalePOData").handsontable("getCellMeta", 0, COLSALEPO.DELETE).readOnly) {
			Handsontable.renderers.TextRenderer.apply(this, arguments);
			td.innerHTML = '';
			td.align = 'center';
		} else {
			Handsontable.renderers.TextRenderer.apply(this, arguments);
			var btnRemoveProductIdStr = "btnRemoveProduct_" + row;
			html = '<a class="delIcons" id="' + btnRemoveProductIdStr + '"' + ' onclick="return GridSalePOBusiness.deleteSaleProduct(\''+row+'\',\''+col+'\',\''+instance+'\');" href="javascript:void(0)" title="Xóa sản phẩm"><img src="/resources/images/icon-delete.png" width="19" height="20" /></a>'
			td.innerHTML = html;
			td.align = 'center';
		}
	},
	
	/**
	 *  Ham delete sp ban
	 * @author vuongmq
	 * @params r
	 * @params c
	 * @params htSale
	 * @since Dec 08, 2015
	 */
	deleteSaleProduct: function(r, c, htSale) {
		r = Number(r);
		c = Number(c);
		htSale = $('#gridSalePOData').handsontable('getInstance');
		var proCode = htSale.getDataAtCell(r, COLSALEPO.PRO_CODE);
		var pro = GridSalePOBusiness._mapSaleProduct.get(proCode);
		htSale.deselectCell();
		$.messager.confirm(jsp_common_xacnhan, 'Bạn có muốn xóa sản phẩm ' + Utils.XSSEncode(proCode) + '?', function(is) {  
		    if (is) {
		    	// restore quantity to de-select warehouse
		    	//GridSalePOBusiness.restoreWarehouseAvailableQuantityWhenDeleteRow(r, proCode);
		    	// if there isn't product with 'productCode' anymore, deleting product in 'SPCreateOrder._mapPRODUCTSbyWarehouseOnGrid' also.
		    	var count = 0;	// count product with 'productCode' (except row 'r') on sale grid
		    	for (var i = 0, row = htSale.getData(); i < row.length; row[i][COLSALEPO.PRO_CODE] == proCode && i != r ? count++ : null, i++);
		    	if (!count && pro) {
		    		//SPCreateOrder._mapPRODUCTSbyWarehouseOnGrid.remove(pro.productId);
		    	}
				htSale.alter('remove_row', r);	
		    	GridSalePOBusiness.showTotalGrossWeightAndAmount();
		    	htSale.selectCell(0,0);
		    	htSale.render();
		    	enable('payment');
				//disabled('btnOrderSave');
				//disabled('btnOrderReset');
		    } else {
	    		htSale.selectCell(r,c);
		    }
		});
	},

	/**
	 * Ham set product code 
	 * @author vuongmq
	 * @params instance
	 * @params td
	 * @params row
	 * @params col
	 * @params prop
	 * @params value
	 * @params cellProperties
	 * @since Dec 08, 2015
	 */
	productCodeRenderer: function (instance, td, row, col, prop, value, cellProperties) {
		Handsontable.renderers.TextRenderer.apply(this, arguments);
		var html = '';
		if (value != null) {
			html = Utils.XSSEncode(value);
			/*if (SPCreateOrder._isViewOrder == true) {
				html = '<a id="product_'+ row + '" onclick="return SPSearchSale.viewProductDetails(\''+ Utils.XSSEncode(value)+'\');" href="javascript:void(0)">'+Utils.XSSEncode(value)+'</a>'
			} else {
				html = '<a id="product_'+ row + '" onclick="return SPCreateOrder.viewProductDetails(\''+Utils.XSSEncode(value)+'\');" href="javascript:void(0)">'+Utils.XSSEncode(value)+'</a>'
			}*/
		}
		td.innerHTML = html;
	},

	/**
	 *  Ham tinh toan tong tien , tong kluong va tong ckhau don hang
	 * @author vuongmq
	 * @params type
	 * @since Dec 08, 2015
	 */
	showTotalGrossWeightAndAmount: function(type) {
		var htSale = $('#gridSalePOData').handsontable('getInstance');
		var quantityCol = htSale.getDataAtCol(COLSALEPO.QUANTITY);
		var totalWeight = 0;
		var totalQuantity = 0;
		var totalAmount = 0;
		for (var i = 0, sz = htSale.getData().length; i < sz; i++) {
			if (quantityCol[i] != null) {
				var product = GridSalePOBusiness._mapSaleProduct.get(htSale.getDataAtCell(i, COLSALEPO.PRO_CODE));
				if (product != null) {
					var quantity = quantityCol[i];
					//quantity = Number(Utils.returnMoneyValue(quantity));
					var quanArr = quantity.split('/');
					var quanPk = (quanArr[0] != undefined) ? Utils.returnMoneyValue(quanArr[0].trim()) : 0;
					var quanRe = (quanArr[1] != undefined) ? Utils.returnMoneyValue(quanArr[1].trim()) : 0;
					var pkPrice = product.packagePrice;
					var price = product.price;
					if (!isNullOrEmpty(quantity) && !isNullOrEmpty(pkPrice) && !isNullOrEmpty(price)) {
						pkPrice = Number(Utils.returnMoneyValue(pkPrice));
						price = Number(Utils.returnMoneyValue(price));
						totalAmount = totalAmount + POManualCreate.getAmountProduct(pkPrice, quanPk, price, quanRe);
					}
					quantity = Number(getQuantity(quantity, product.convfact));
					totalWeight += quantity * product.grossWeight; // so luong la thung
					totalQuantity += quantity;
				}
			}
		}
		$('#totalWeightSale').html(formatFloatValue(totalWeight, 3));
		$('#totalQuantitySale').html(formatCurrency(totalQuantity));
		$('#totalAmountSale').html(formatCurrency(totalAmount));
	},

	/**
	 *  Ham reset grid them moi san pham ban
	 * @author vuongmq
	 * @params type
	 * @since Dec 08, 2015
	 */
	resetSalePOGrid: function(type) {
		GridSalePOBusiness._mapSaleProductGrid = new Map();
		var cols = GridSalePOBusiness.getColumnProperty();
		var data = new Array();
		var minSpareRows = 0;
		if (type == PoManualType.PO_IMPORT) {
			for (var i = 0; i < 1; i++) {
				var row = new Array();
				for (var j = 0, size = cols.length; j < size; j++) {
					row.push(null);
				}
				data.push(row);
			}
			minSpareRows = 1;
		} else {
			if (GridSalePOBusiness._mapSaleProduct != null) {
				var len = GridSalePOBusiness._mapSaleProduct.keyArray.length;
				for (var i = 0; i < len; i++) {
					var row = new Array();
					var key = GridSalePOBusiness._mapSaleProduct.keyArray[i];
					var dataMap = GridSalePOBusiness._mapSaleProduct.get(key);
					for (var j = 0, size = cols.length; j < size; j++) {
						if (j == COLSALEPO.PRO_CODE) {
							row.push(dataMap.productCode);
						} else if (j == COLSALEPO.PRO_NAME) {
							row.push(dataMap.productName);
						} else if (j == COLSALEPO.CONVFACT) {
							row.push(formatCurrency(dataMap.convfact));
						} else if (j == COLSALEPO.PACKING_PRICE) {
							row.push(formatCurrency(dataMap.packagePrice) + '/' + formatCurrency(dataMap.price));
						} else if (j == COLSALEPO.AVAILABLE_QUANTITY) {
							row.push(formatQuantity(dataMap.availableQuantity, dataMap.convfact));
						} else if (j == COLSALEPO.STOCK) {
							row.push(formatQuantity(dataMap.quantity, dataMap.convfact));
						} else if (j == COLSALEPO.OLD_QUANTITY) {
							row.push(formatQuantity(dataMap.oldQuantity, dataMap.convfact));
						}
					}
					data.push(row);
				}
			}
		}
		var hsSale = $('#gridSalePOData').handsontable('getInstance');
//		hsSale.alter('insert_row');
//		hsSale.updateSettings({data: data, columns: GridSalePOBusiness.getColumnProperty(), minSpareRows: minSpareRows});
		hsSale.updateSettings({data: data, columns: cols, minSpareRows: minSpareRows});
		$('#totalWeightSale').html(0);
		$('#totalQuantitySale').html(0);
		$('#totalAmountSale').html(0);
	},

	/**
	 * Ham disble grid sp ban
	 * @author vuongmq
	 * @since Dec 08, 2015
	 */
	disableSalePOGrid: function() {
		GridSalePOBusiness._listSaleProduct = new Array();
		GridSalePOBusiness._mapSaleProduct = new Map();
		GridSalePOBusiness._listSaleProductCode = new Array();
		var valueEdit = $('#valueEdit').val();
		if (valueEdit != undefined && valueEdit == ActionType.UPDATE) {
			GridSalePOBusiness._mapPoProductEdit = new Map();
		}
		var hsSale = $('#gridSalePOData').handsontable('getInstance');
		hsSale.updateSettings({data: []});
		$('#totalWeightSale').html(0);
		$('#totalQuantitySale').html(0);
		$('#totalAmountSale').html(0);
//		$('#gridSaleContainer').css('height', 70);
	},

	/**
	 * check if quantity-input is valid
	 * @author tuannd20
	 * @param  {String}  inputStr quantity input string
	 * @return {Boolean}          true: valid, false: invalid
	 * @date 17/10/2014
	 * @since 03/02/2016
	 */
	isValidQuantityInputPO: function(inputStr) {
		inputStr = (inputStr || "").toString();
		return !((!/^[0-9\/]+$/.test(inputStr)) || (String(inputStr).indexOf('/') != -1 && String(inputStr).split('/').length > 2));		
	},

	/**
	 * Ham khoi tao grid san pham ban
	 * @author vuongmq
	 * @since Dec 08, 2015
	 */
	initSalePOGrid: function() {
		var columnWidths = new Object();
		var columnsHeader = new Object();
		if (POManualCreate._TYPE == PoManualType.PO_IMPORT) {
			GridSalePOBusiness._quantityView = GridSalePOBusiness._quantityInput;
			columnWidths = [200, 250, 80, 120, 120, 120, 115, 130, 50];
			columnsHeader = [GridSalePOBusiness._productCode, GridSalePOBusiness._productName,
						 GridSalePOBusiness._convfact, GridSalePOBusiness._packing_price,
						 GridSalePOBusiness._availableQuantity, GridSalePOBusiness._stock,
			             GridSalePOBusiness._quantityInput, GridSalePOBusiness._amount,
			             GridSalePOBusiness._deleteSaleProduct];
		} else {
			GridSalePOBusiness._quantityView = GridSalePOBusiness._quantityOutput;
			columnWidths = [200, 250, 80, 120, 120, 120, 110, 110, 130];
			columnsHeader = [GridSalePOBusiness._productCode, GridSalePOBusiness._productName,
						 GridSalePOBusiness._convfact, GridSalePOBusiness._packing_price,
						 GridSalePOBusiness._availableQuantity, GridSalePOBusiness._stock,
						 GridSalePOBusiness._old_quantity,
			             GridSalePOBusiness._quantityOutput, GridSalePOBusiness._amount];
		}
		$('#gridSalePOData').handsontable({
			data: [],
			colWidths: columnWidths,
			width: $(window).width() - 55,
			rowHeaders: true,
			outsideClickDeselects: true,
			multiSelect : false,
			fillHandle: true,
			comments: true,
			/*contextMenu: {
			},
			cells: function (row, col, prop) {
			},*/
			colHeaders : columnsHeader,
			columns: GridSalePOBusiness.getColumnProperty(),
			afterChange: function(changes, source) {//SPCreateOrder
				POManualCreate.changeColumnIndex();
				if (changes != null && $.isArray(changes)) {
					hideAllMessage();
					var htSale = $('#gridSalePOData').handsontable('getInstance');
					for (var i = 0, sz = changes.length; i < sz; i++) {
						var row = changes[i];
						var r = row[0];
						var c = row[1];
						var oldValue = row[2];
						var newValue = row[3];
						//Tu dong fill ten sp, don gia, ton kho dap ung, ctkm khi chon sp
						if (newValue != oldValue && newValue != null) {
							switch (c) {
								//Khi nhap gia tri vao cot Ma SP
								case COLSALEPO.PRO_CODE: {
									var oldProduct = GridSalePOBusiness._mapSaleProduct.get(oldValue);
									var newProduct = GridSalePOBusiness._mapSaleProduct.get(newValue);
									if (newProduct != null) {
										// truong hop khong co lo
										htSale.setDataAtCell(r, COLSALEPO.PRO_NAME, newProduct.productName, source);
										htSale.setDataAtCell(r, COLSALEPO.CONVFACT, formatCurrency(newProduct.convfact), source);
										var priceView = formatCurrency(newProduct.packagePrice) + '/' + formatCurrency(newProduct.price);
										htSale.setDataAtCell(r, COLSALEPO.PACKING_PRICE, priceView, source);
										htSale.setDataAtCell(r, COLSALEPO.AVAILABLE_QUANTITY, formatQuantity(newProduct.availableQuantity, newProduct.convfact), source);
										htSale.setDataAtCell(r, COLSALEPO.STOCK, formatQuantity(newProduct.quantity, newProduct.convfact), source);
										if (htSale.getDataAtCell(r, COLSALEPO.QUANTITY) != null) {
											htSale.setDataAtCell(r, COLSALEPO.QUANTITY, null, source);
										}
										htSale.setDataAtCell(r, COLSALEPO.DELETE, 0, source);
										htSale.selectCell(r, COLSALEPO.QUANTITY);
									} else {
										htSale.setDataAtCell(r, COLSALEPO.PRO_CODE, null, source);
									}
									GridSalePOBusiness.showTotalGrossWeightAndAmount();
								}
								break;
								// Khi nhap so luong
								case COLSALEPO.QUANTITY: {
									var newProductCode = htSale.getDataAtCell(r, COLSALEPO.PRO_CODE);
									var newProduct = GridSalePOBusiness._mapSaleProduct.get(newProductCode);
									if (newProduct != null) {
										var newQuantity = newValue;
										if (isNaN(Number(newQuantity)) || Number(newQuantity) < 0) {
											htSale.selectCell(r, COLSALEPO.QUANTITY);
											htSale.getActiveEditor().beginEditing();
											return;
										}
										GridSalePOBusiness.showTotalGrossWeightAndAmount();
									}
								}
							}
						}
					}
				}
			},
			//Xu ly du lieu truoc khi thay doi
			onBeforeChange: function(changes, source) {//SPCreateOrder
				POManualCreate.changeColumnIndex();
				if (changes != null && $.isArray(changes)) {
					for (var i = 0, sz = changes.length; i < sz; i++) {
						var htSale = $('#gridSalePOData').handsontable('getInstance');
						var row = changes[i];
						var r = row[0];
						var c = row[1];
						var oldValue = row[2];
						var newValue = row[3];
						var productCode = htSale.getDataAtCell(r, COLSALEPO.PRO_CODE);
						switch (c) {
							// truoc khi thay doi ma San pham
							case COLSALEPO.PRO_CODE:
								var proCode = "";
								var products = GridSalePOBusiness._mapSaleProduct.valArray;
								for (var i = 0, n = products.length; i < n; i++) {
									if (newValue == Utils.XSSEncode(products[i].productCode + " - " + products[i].productName).trim()) {
										proCode = products[i].productCode;
										break;
									}
								}
								if (proCode.length > 0) {
									row[3] = proCode;
									// xoa trong GridSalePOBusiness._listSaleProductCode dong productCode da them vao
									/*var idx = $.inArray(proCheck, GridSalePOBusiness._listSaleProductCode);
									GridSalePOBusiness._listSaleProductCode.splice(idx, 1);*/
								}
							break;
							// truoc khi thay doi so luong
							case COLSALEPO.QUANTITY:  
								if (productCode != null) {
									var product = GridSalePOBusiness._mapSaleProduct.get(productCode.trim());
									if (product != null) {
										if (newValue != null) {
											if (newValue == '') {
												newValue = '0';
											}
											/*newValue = Utils.returnMoneyValue(newValue);
											var strValidNewValue =  Utils.getMessageOfInvaildNumberNew(newValue);
											if (strValidNewValue.length > 0 || newValue <= 0) {
												var msg = format('Số lượng đặt sản phẩm {0} phải là số nguyên dương. Vui lòng nhập lại', productCode);
												POManualCreate.showValidateError(msg);
											} else {
												// tinh so tien
												amount = POManualCreate.getAmountProduct(product.packagePrice, newValue);
												htSale.setDataAtCell(r, COLSALEPO.AMOUNT, formatFloatValue(amount, apConfig.sysDigitDecimal), source);
												row[3] = formatCurrency(newValue);
											}*/
											if (!GridSalePOBusiness.isValidQuantityInputPO(newValue)) {
												htSale.setDataAtCell(r, COLSALEPO.AMOUNT, 0, source);
												htSale.selectCell(r, COLSALEPO.QUANTITY);
												setTimeout(function() {
													htSale.selectCell(r, COLSALEPO.QUANTITY);
												}, 100);
												var msg = format('{0} sản phẩm {1} phải đúng định dạng và số nguyên dương. Vui lòng nhập lại', GridSalePOBusiness._quantityView, productCode);
												POManualCreate.showValidateError(msg);
												GridSalePOBusiness._isvalidQuantityPO = false;
											} else {
												var pkPriceNum = product.packagePrice;
												var priceNum = product.price;
												var quanArr = newValue.split('/');
												var amount = 0;
												if ($.isArray(quanArr)) {
													if (quanArr.length == 2) {
														var quantity = getQuantity(newValue, product.convfact);
														newValue = formatQuantity(quantity, product.convfact);
														amount = POManualCreate.getAmountProduct(product.packagePrice, Utils.returnMoneyValue(quanArr[0].trim()), product.price, Utils.returnMoneyValue(quanArr[1].trim()));
													} else {
														newValue = formatQuantity(Utils.returnMoneyValue(quanArr[0].trim()), product.convfact);
														quanArr = newValue.split('/');
														amount = POManualCreate.getAmountProduct(product.packagePrice, Utils.returnMoneyValue(quanArr[0].trim()), product.price, Utils.returnMoneyValue(quanArr[1].trim()));
													}
													htSale.setDataAtCell(r, COLSALEPO.AMOUNT, formatFloatValue(amount, apConfig.sysDigitDecimal), source);
												}
												GridSalePOBusiness._isvalidQuantityPO = true;
											}
											row[3] = newValue;
										} else {
											htSale.setDataAtCell(r, COLSALEPO.AMOUNT, null, source);
											row[3] = null;
										}
									} else {
										return false;
									}
								} else {
									return false;
								}
							break;
						}
					}
				}
			}
			///
		});
	},
	
	 /**
	 * Ham Lay danh sach San pham edit
	 * @author vuongmq
	 * @param type
	 * @since 26/12/2015
	 * */
    editProductPO: function(type) {
    	var shopId = $('#shopId').val();
		var warehouseId = $('#warehouse').val();
		var params = {};
    	if (type == PoManualType.PO_IMPORT) {
	    	var url = '/po-manual/get-product';
			if (shopId != null && warehouseId != null) {
				params.shopId = shopId;
				params.warehouseId = warehouseId;
			} else {
				GridSalePOBusiness.disableSalePOGrid();
				return false;
			}
	    	Utils.getJSONDataByAjaxNotOverlay(params, url, function(data) {
	    		/** Lay danh sach san pham ban*/
				GridSalePOBusiness._listSaleProduct = data.listSaleProduct;
				GridSalePOBusiness._mapSaleProduct = new Map();
				GridSalePOBusiness._listSaleProductCode = new Array();
				if (GridSalePOBusiness._listSaleProduct != null && GridSalePOBusiness._listSaleProduct != undefined) {
					for (var i = 0, sz = GridSalePOBusiness._listSaleProduct.length; i < sz; i++) {
						var saleProduct = GridSalePOBusiness._mapSaleProduct.get(GridSalePOBusiness._listSaleProduct[i].productCode); 
						if (saleProduct == null) {
							GridSalePOBusiness._mapSaleProduct.put(GridSalePOBusiness._listSaleProduct[i].productCode, GridSalePOBusiness._listSaleProduct[i]);
							GridSalePOBusiness._listSaleProductCode.push(Utils.XSSEncode(GridSalePOBusiness._listSaleProduct[i].productCode + ' - ' + GridSalePOBusiness._listSaleProduct[i].productName));
						}
					}
					//GridSalePOBusiness.editResetSalePOGrid(type);
				} else {
					GridSalePOBusiness.disableSalePOGrid();
				}
	    	});
		}
		// lay danh sach edit
		var urlEdit = '/po-manual-manage/get-po-product-edit';
		var params = {};
		var poId = $('#poId').val();
		if (type != null && shopId != null && warehouseId != null && poId != null) {
			params.shopId = shopId;
			params.warehouseId = warehouseId;
			params.poId = poId;
			params.type = type;
		} else {
			GridSalePOBusiness.disableSalePOGrid();
			return false;
		}
    	Utils.getJSONDataByAjaxNotOverlay(params, urlEdit, function(data) {
			/** Lay danh sach san pham ban*/
			GridSalePOBusiness._listSaleProduct = data.listPoProductEdit;
			GridSalePOBusiness._mapPoProductEdit = new Map();
			if (GridSalePOBusiness._listSaleProduct != null && GridSalePOBusiness._listSaleProduct != undefined) {
				if (type == PoManualType.PO_IMPORT) {
					for (var i = 0, sz = GridSalePOBusiness._listSaleProduct.length; i < sz; i++) {
						var saleProduct = GridSalePOBusiness._mapPoProductEdit.get(GridSalePOBusiness._listSaleProduct[i].productCode); 
						if (saleProduct == null) {
							GridSalePOBusiness._mapPoProductEdit.put(GridSalePOBusiness._listSaleProduct[i].productCode, GridSalePOBusiness._listSaleProduct[i]);
						}
					}
				} else {
					GridSalePOBusiness._mapSaleProduct = new Map(); // voi PO_RETURN; GridSalePOBusiness._mapSaleProduct bang voi map GridSalePOBusiness._mapPoProductEdit
					GridSalePOBusiness._listSaleProductCode = new Array();
					for (var i = 0, sz = GridSalePOBusiness._listSaleProduct.length; i < sz; i++) {
						var saleProduct = GridSalePOBusiness._mapSaleProduct.get(GridSalePOBusiness._listSaleProduct[i].productCode); 
						if (saleProduct == null) {
							GridSalePOBusiness._mapPoProductEdit.put(GridSalePOBusiness._listSaleProduct[i].productCode, GridSalePOBusiness._listSaleProduct[i]);
							GridSalePOBusiness._mapSaleProduct.put(GridSalePOBusiness._listSaleProduct[i].productCode, GridSalePOBusiness._listSaleProduct[i]);
							GridSalePOBusiness._listSaleProductCode.push(Utils.XSSEncode(GridSalePOBusiness._listSaleProduct[i].productCode + ' - ' + GridSalePOBusiness._listSaleProduct[i].productName));
						}
					}
				}
				GridSalePOBusiness.editResetSalePOGrid(type);
			} else {
				GridSalePOBusiness.disableSalePOGrid();
			}
		});
    },

    /**
	 *  Ham reset grid edit san pham
	 * @author vuongmq
	 * @params type
	 * @since Dec 26, 2015
	 */
	editResetSalePOGrid: function(type) {
		GridSalePOBusiness._mapSaleProductGrid = new Map();
		var cols = GridSalePOBusiness.getColumnProperty();
		var data = new Array();
		var minSpareRows = 0;
		if (GridSalePOBusiness._mapPoProductEdit != null) {
			var len = GridSalePOBusiness._mapPoProductEdit.keyArray.length;
			for (var i = 0; i < len; i++) {
				var row = new Array();
				var key = GridSalePOBusiness._mapPoProductEdit.keyArray[i];
				var dataMap = GridSalePOBusiness._mapPoProductEdit.get(key);
				for (var j = 0, size = cols.length; j < size; j++) {
					if (j == COLSALEPO.PRO_CODE) {
						row.push(dataMap.productCode);
					} else if (j == COLSALEPO.PRO_NAME) {
						row.push(dataMap.productName);
					} else if (j == COLSALEPO.CONVFACT) {
						row.push(formatCurrency(dataMap.convfact));
					} else if (j == COLSALEPO.PACKING_PRICE) {
						row.push(formatCurrency(dataMap.packagePrice) + '/' + formatCurrency(dataMap.price));
					} else if (j == COLSALEPO.AVAILABLE_QUANTITY) {
						row.push(formatQuantity(dataMap.availableQuantity, dataMap.convfact));
					} else if (j == COLSALEPO.STOCK) {
						row.push(formatQuantity(dataMap.quantity, dataMap.convfact));
					} else if (j == COLSALEPO.OLD_QUANTITY) {
						row.push(formatQuantity(dataMap.oldQuantity, dataMap.convfact)); // don tra: hien len so luong don tra
					} else if (j == COLSALEPO.QUANTITY) {
						//row.push(formatCurrency(dataMap.packageQuantity)); // vi nhap so luong thung; nen edit cung so luong thung
						row.push(formatQuantity(dataMap.poDetailQuantity, dataMap.convfact)); // vi nhap so luong thung/le; nen edit cung so luong thung/le
					} else if (j == COLSALEPO.AMOUNT) {
						row.push(formatCurrency(dataMap.amount));
					}
				}
				data.push(row);
			}
		}
		if (type == PoManualType.PO_IMPORT) {
			minSpareRows = 1;
		}
		var hsSale = $('#gridSalePOData').handsontable('getInstance');
//		hsSale.alter('insert_row');
//		hsSale.updateSettings({data: data, columns: GridSalePOBusiness.getColumnProperty(), minSpareRows: minSpareRows});
		hsSale.updateSettings({data: data, columns: cols, minSpareRows: minSpareRows});
		/*$('#totalWeightSale').html(0);
		$('#totalQuantitySale').html(0);
		$('#totalAmountSale').html(0);*/
		GridSalePOBusiness.showTotalGrossWeightAndAmount();
	},
};

/////////////////////////////////  END GridSalePOBusiness ///////////////////////////////////////////////
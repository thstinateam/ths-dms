/**
 * POManualManage
 * @author vuongmq
 * @since Dec 18, 2015
 */
var POManualManage = {
	_ERR_X: "X",
	/**
	 * getSearchParams po
	 * @author vuongmq
	 * @since 18/12/2015 
	 */
	getSearchParams: function() {
		var params = new Object();
		params.shopId = $('#shopId').val();
		params.type = $('#type').val();
		params.approvedStep = $('#approvedStep').val();
		params.poNumber = $('#poNumber').val().trim();
		params.fromDate = $('#fDate').val();
		params.toDate = $('#tDate').val();
		return params;
	},

	/**
	 * hideAnhShowButtomPo	 
	 * huy va Duyet: hien khi don hang tra
	 * @author vuongmq
	 * @since 21/12/2015 
	 */
	hideAnhShowButtomPo: function() {
		var type = $('#type').val();
		if (type == PoManualType.PO_RETURN) {
			enable('btnApproved');
			$('#btnApproved').show()
			enable('btnRejected');
			$('#btnRejected').show();
		} else {
			$('#btnApproved').hide();
			disabled('btnApproved');
			$('#btnRejected').hide()
			disabled('btnRejected');
		}
		POManualManage.searchPo();
	},

	/**
	 * search po
	 * @author vuongmq
	 * @since 18/12/2015 
	 */
	searchPo: function() {
		var params = POManualManage.getSearchParams();
		$('#grid').datagrid('load', params);
	},

	/**
	 * Init danh sach don hang po
	 * @author vuongmq
	 * @since 18/12/2015 
	 */
	initGridPo: function() {
		var params = POManualManage.getSearchParams();
		$('#grid').datagrid({  
		    url: '/po-manual-manage/search',
		    rownumbers: true,
		    pagination: true,
		    singleSelect: false,
		    autoRowHeight: true,
		    pageNumber: 1,
		    pageSize: 10,
		    pageList: [10, 50, 100, 200, 500],
		    width: $(window).width() - 70,	    		    
		    queryParams: params,
		    fitColumns: true,
		    scrollbarSize: 0,
		    columns: [[
				{field: 'id', checkbox: true, width: 50, align: 'center'},
				{field: 'shopCode', title: 'Đơn vị', width: 300, align: 'left', formatter: function(value, row, index) {
					return VTUtilJS.XSSEncode(row.shopCode + ' - ' + row.shopName);
				}},
				{field: 'poNumber', title: 'Số đơn hàng', width: 200, align: 'left', formatter: function(value, row, index) {
		        	return VTUtilJS.XSSEncode(value);
		        }},
				{field: 'type', title: 'Loại đơn hàng', width: 130, align: 'left', formatter: function(value, row, index) {
					return PoManualType.parseValue(value);
				}},
		        {field: 'approvedStep', title: 'Trạng thái', width: 130, align: 'left', formatter: function(value, row, index) {
		        	var html = '';
		        	if (ApprovalStepPo.NEW == value && activeType.RUNNING == row.status) {
		        		//status = 1, approve_step = 1
		        		html = 'Chờ gửi';
		        	} else if (ApprovalStepPo.WAITING == value && activeType.RUNNING == row.status) {
		        		//status = 1, approve_step = 2
		        		html = 'Chờ duyệt';
		        	} else if (ApprovalStepPo.APPROVED == value && activeType.RUNNING == row.status) {
		        		//status = 1, approve_step = 3
		        		html = 'Đã duyệt';
		        	} else if (ApprovalStepPo.COMPLETED == value && activeType.RUNNING == row.status) {
		        		//status = 1, approve_step = 4
		        		if (PoManualType.PO_IMPORT == row.type) {
		        			html = 'Đã nhập hàng';	
		        		} else {
		        			html = 'Đã xuất kho';
		        		}
		        	} else if (ApprovalStepPo.NEW == value && activeType.STOPPED == row.status) {
		        		//status = 0, approve_step = 1
		        		html = 'Hủy';
		        	}  else if (ApprovalStepPo.WAITING == value && activeType.STOPPED == row.status) {
		        		//status = 0, approve_step = 2
		        		html = 'Từ chối';
		        	}
		        	return html;
		        }},
		        {field: 'poDateStr', title: 'Ngày tạo', width: 130, align: 'center', formatter: function(value, row, index) {
		        	return VTUtilJS.XSSEncode(value);
		        }},
		        {field: 'amount', title: 'Tổng tiền', width: 140, align: 'right', formatter: function(value, row, index) {
		        	return formatCurrency(value);
		        }},
		        {field: 'change', title: '<a id="add_grid_po" class="cmsiscontrol" href="/po-manual/create-manual"><img title="Thêm đơn hàng" src="/resources/images/icon_add.png"/></a>', width: 70, align: 'center', formatter: function(value, row, index) {
			    	var html = '<a href="javascript:void(0)" onclick="return POManualManage.viewDetailPo(' + index + ', ' + row.id + ', \'' + Utils.XSSEncode(row.poNumber) + '\');"><img title="Thông tin đơn hàng" src="/resources/images/icon-view.png" width="16" heigh="16"></a>';
			    	var htmlEdit = '';
			    	if (ApprovalStepPo.NEW == row.approvedStep && activeType.RUNNING == row.status) {
		        		//status = 1, approve_step = 1; html = 'Chờ gửi';
		        		htmlEdit += '&nbsp &nbsp &nbsp<a id="group_edit_grid_po' + index + '" class="cmsiscontrol" href="javascript:void(0)" onclick="return POManualManage.editPo(' + index + ', ' + row.id + ');"><img title="Chỉnh sửa đơn hàng" src="/resources/images/icon-edit.png" width="16" heigh="16"></a>';
		        	} else {
		        		htmlEdit += '&nbsp &nbsp &nbsp<a id="group_edit_grid_po' + index + '" class="cmsiscontrol" href="javascript:void(0)" ><img title="Không được chỉnh sửa" src="/resources/images/icon-edit_disable.png" width="16" heigh="16"></a>';
		        	}
			    	return html + htmlEdit;
			    }}, 
		    ]],
		    onCheckAll: function(r) {
		    	
	    	},
		    onLoadSuccess: function(data) {
		    	$('.datagrid-header-rownumber').html('STT');
			    updateRownumWidthForJqGrid('#searchPoResult');			   	 
		   	 	$(window).resize();
		   	 	$(".datagrid-header-check input[type=checkbox]").removeAttr("checked");
		   	 	$('#divPOCSConfirmTable').html('').hide(); // an grid detail
		   	 	Utils.functionAccessFillControl('dgGridContainer', function(data) {
					//Xu ly cac su kien lien quan den control duoc phan quyen
				});
		    }		    
		});
	},

	/**
	 * view chi tiet don hang Po
	 * @author vuongmq
	 * @since 18/12/2015 
	 */
	viewDetailPo: function(index, id, poNumber) {
		//alert('viewDetailPo');
		var params = new Object();
		params.poId = id;
		Utils.getHtmlDataByAjax(params, "/po-manual-manage/detail-po-manual", function(data) {
			try {
				var _data = JSON.parse(data);
				if(_data.error && _data.errMsg != undefined) {
					$('#errMsg').html(_data.errMsg).show();
					var tm = setTimeout(function(){
						$('#errMsg').html('').hide();
					}, 3000);
				}
			} catch(e) {
				$("#divPOCSConfirmTable").html(data).show();
			}
		});		
		$('html, body').animate({ scrollTop: $(document).height() }, 1000);
		return false;
	},
	
	/**
	 * chinh sua don hang Po
	 * @author vuongmq
	 * @since 18/12/2015 
	 */
	editPo: function(index, id) {
		if (id != null) {
			window.location.href = '/po-manual-manage/edit-manual?poId=' + id;	
		}
	},

	/**
	 * quay lai danh sach don hang Po
	 * @author vuongmq
	 * @since 28/12/2015 
	 */
	backPoManage: function() {
		$.messager.confirm(jsp_common_xacnhan, 'Bạn có muốn <b>quay lại</b> quản lý danh sách đơn hàng?', function(r) {
			if (r) {
				window.location.href = '/po-manual-manage/info';
			}
		});
	},

	/**
	 * Cap nhat trang thai cua don hang Po
	 * @author vuongmq
	 * @param approvedStep
	 * @since 18/12/2015 
	 */
	updateStatusOrderManual: function(approvedStep) {
		hideAllMessage();
		var errMsg = '';
		var data = $('#grid').datagrid('getChecked');
		if (data == null || data.length == 0) {
			errMsg = 'Vui lòng chọn đơn hàng cần xử lý.';
		}
		var lstId = new Array();
		for (var i = 0, sz = data.length; i < sz; i++) {
			if (data[i] != null && data[i].id != null) {
				lstId.push(data[i].id);
			}
		}
		if (errMsg.length > 0) {
			$('#errMsg').html(errMsg).show();
			return false;
		}
		var params = new Object();
		params.lstId = lstId;
		params.approvedStep = approvedStep;
		var message = 'Bạn có muốn {0} danh sách đơn hàng đã chọn?';
		var typeName = '';
		if (approvedStep == ApprovalStepPo.DESTROY) {
			typeName = 'hủy';
		} else if (approvedStep == ApprovalStepPo.NEW) {
			typeName = 'gửi';
		}  else if (approvedStep == ApprovalStepPo.REJECTED) {
			typeName = 'từ chối';
		} else if (approvedStep == ApprovalStepPo.APPROVED) {
			typeName = 'duyệt';
		}
		message = format(message, typeName);
		Utils.addOrSaveData(params, '/po-manual-manage/updateStatusOrderManual', null, 'errMsg', function(data) {
			var tm = setTimeout(function() {
				//Load lai danh sach don hang po
				$('#successMsg').html("").hide();
				clearTimeout(tm);
				POManualManage.searchPo();
			 }, 1500);
		}, null, null, null, message, function (data) {
			POManualManage.searchPo();
			if (data != null && data.error && data.lstError != null && data.lstError.length > 0) {
				POManualManage.viewPopupErrUpdateStatus(data.lstError, data.errMsg);
			}
			/*var tm = setTimeout(function() {
				//Load lai danh sach don hang po
				clearTimeout(tm);
				POManualManage.searchPo();
			}, 1500);*/
		});
	},

	/**
	 * Cap nhat trang thai cua don hang Po
	 * @author vuongmq
	 * @param listError
	 * @since 21/12/2015 
	 */
	viewPopupErrUpdateStatus: function(listError, errMsg) {
		hideAllMessage();
		/*var html = '<div id="popup-error-container">\
			<div id="popup-error">\
				<div class="PopupContentMid">\
					<div class="GeneralForm Search1Form">\
						<div id="gridErrorContainer">\
						<div id="gridError"></div>\
						</div>\
						<div class="Clear"></div>\
					</div>\
				<div class="BtnCenterSection">\
			</div>\
			</div>';
		$("body").append(html);*/
		$('#popupErrorStatus').addClass('easyui-dialog');
		$('#popupErrorStatus').dialog({
			title: 'Cảnh báo',
			width: 700,
			heigth: "auto",
			modal: true, close: false, cache: false,
			onOpen: function() {
				$('#errMsgPopup').html(errMsg).show();
				$('#gridError').datagrid({
					data: listError,
					width: $("#gridErrorContainer").width(),
    				height: 'auto',
    				singleSelect: true,
    				fitColumns: true,
    				scrollbarSize: 0,
    				rownumbers: true,
    				columns: [[
    					{field: 'poNumber', title: "Số đơn hàng", width: 180, align: 'left', sortable: false, resizable: false, formatter: function(value,row,index) {
    						return VTUtilJS.XSSEncode(value);
    					}},
    					{field: 'failure', title: "Trạng thái", width: 70, align: 'center', sortable: false, resizable: false, formatter: function(value, row, index) {
    						var html = '';
    						if (!isNullOrEmpty(value) && value == POManualManage._ERR_X) {
    							html = '<img title="Thất bại" src="/resources/images/dialog/error-icon.png" width="16" heigh="16">';
    						} else {
    							html = '<img title="Thành công" src="/resources/images/dialog/icon-success.gif" width="16" heigh="16">';
    						}
    						return html;
    					}},
    					{field:  'reason', title:  "Lý do", width: 350, align: 'left', sortable: false, resizable: false, formatter: function(value, row, index) {
    						return VTUtilJS.XSSEncode(value);
    					}},
    				]],
    				onLoadSuccess: function(data) {
    					$('.datagrid-header-rownumber').html('STT');
    					//updateRownumWidthForJqGrid('#gridErrorContainer');
    					//setTimeout(function() {CommonSearchEasyUI.fitEasyDialog("popupErrorStatus");}, 500);
    					$('#popupErrorStatus .datagrid-htable td div').css('text-align', 'center');
    				}
				});
			},
			onClose: function() {
				//$("#popupErrorStatus").dialog("destroy");
			}
		});
	},
};
/*
 * Copyright 2016 Viettel ICT. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
var SaleInPriceManager = {
	_arrId: null,
	_priceId: null,
	_paramPriceSearch: null,
	_shopChoose: null,
	_valueTmp: 0,
	_paramSearch: null,
	_flagChangeWaitting : {
		isdelete: 0, //Xoa
		isrunning: 1 //Duyet
	},	
	
	/**
	 * xu ly format cot VAT
	 * @author trietptm
	 * @since Jan 20, 2016
	 */
	processVATInPrice: function(vat) {
		if (vat == undefined || vat == null) {
			return 0;
		}
		var arrNum = vat.toString().trim().split(".");
		if (arrNum.length > 2) {
			vat = parseFloat(arrNum[0].trim() + "." + arrNum[1].trim());
			SaleInPriceManager.processVATInPrice(vat);
		}
		return parseFloat(vat).toFixed(2);
	},

	/**
	 * Tao tham so tim kiem Price sale in
	 * @author trietptm
	 * @since Jan 20, 2016
	 */
	createSaleInPriceManagerParamaterSearch: function() {
		SaleInPriceManager._paramSearch = {};
		SaleInPriceManager._paramSearch.productCode = $('#txtProductCode').val().trim();
		SaleInPriceManager._paramSearch.productName = $('#txtProductName').val().trim();
		var shopCode = $('#cbxShop').combobox('getValue');
		if (shopCode.length == 0) {
			shopCode = $('#shopCode').val();
		}
		SaleInPriceManager._paramSearch.shopCode = shopCode;
		SaleInPriceManager._paramSearch.fromDateStr = $('#txtFromDate').val().trim();
		SaleInPriceManager._paramSearch.toDateStr = $('#txtToDate').val().trim();	
		SaleInPriceManager._paramSearch.status = $('#ddlStatus').val();
	},

	/**
	 * tim kiem gia sale in
	 * @author trietptm
	 * @since Jan 20, 2016
	 */
	searchProductPrice:function() {
		$('.ErrorMsgStyle').html('').hide();
		var msg = '';
		var firstLoadPage = true;
		var fDate = $('#txtFromDate').val();
		var tDate = $('#txtToDate').val();
		if (fDate.length > 0) {
			msg = Utils.getMessageOfInvalidFormatDate('txtFromDate', price_grid_column7);
		}
		if (msg.length == 0 && tDate.length > 0) {
			msg = Utils.getMessageOfInvalidFormatDate('txtToDate', price_grid_column8);
		}
		if (fDate.length >0 && tDate.length > 0 && !Utils.compareDate(fDate, tDate)) {
			msg = price_search_error_01;
			$('#fromDate').focus();
		}
		if (msg.length > 0) {
			$('#errMsgSaleInPriceManager').html(msg).show();
			return false;
		}
		SaleInPriceManager.createSaleInPriceManagerParamaterSearch();
		$('#productPriceDgDiv').html('<table id="dgProductPrice"></table>').change().show();	
		$('#dgProductPrice').datagrid({
			url: '/sale-in/search',
			width: $('#productPriceDgDiv').width() - 15,
			queryParams: SaleInPriceManager._paramSearch,
			pagination: true,
	        rownumbers: true,
	        pageNumber: 1,
	        singleSelect: true,
	        scrollbarSize: 0,
	        autoWidth: true,
	        pageList: [10, 20 , 30, 50],
	        autoRowHeight: true,
	        fitColumns: true,
			columns:[[ 
			    {field: 'shopText', title: price_grid_column2, width: 90, sortable: false, align: 'left', formatter: function(value, row, index) {
			    	var valueS = '';
			    	if (row.shopCode != undefined && row.shopCode != null) {
			    		valueS = valueS + row.shopCode + " - ";
			    	}
			    	if (row.shopName != undefined && row.shopName != null) {
			    		valueS = valueS + row.shopName;
			    	}
			    	return Utils.XSSEncode(valueS);
			    }},
			    {field: 'productCode', title: catalog_product_com, width: 140, sortable: false, align: 'left', formatter: function(value, row, index) {
			    	return Utils.XSSEncode(row.productCode + " - " +  row.productName);
			    }},
			    {field: 'fromDateStr', title: price_grid_column7, width: 40, sortable: false, align: 'center', formatter: function(value, row, index) {
			    	return Utils.XSSEncode(value);
			    }},
			    {field: 'toDateStr', title: price_grid_column8, width: 40, sortable: false, align: 'center', formatter: function(value, row, index) {
			    	return Utils.XSSEncode(value);
			    }},
			    {field: 'pricePackage', title: price_grid_column14, width: 60, sortable: false, align: 'right', formatter: function(value, row, index) {
			    	return VTUtilJS.formatCurrency(Utils.formatDoubleValue(value));
			    }},
			    {field: 'price', title: price_grid_column10, width: 50, sortable: false, align: 'right', formatter: function(value, row, index) {
			    	return VTUtilJS.formatCurrency(Utils.formatDoubleValue(value));
			    }},
			    {field: 'vat', title: price_grid_column11, width: 30, sortable: false, align: 'right', formatter: function(value, row, index) {
			    	return SaleInPriceManager.processVATInPrice(value);
			    }},
			    {field: 'pricePackageNotVAT', title: price_grid_column13, width: 60, sortable: false, align: 'right', formatter: function(value, row, index) {
			    	return VTUtilJS.formatCurrency(Utils.formatDoubleValue(value));
			    }},
			    {field: 'priceNotVAT', title: price_grid_column9, width: 50, sortable: false, align: 'right', formatter: function(value, row, index) {
			    	return VTUtilJS.formatCurrency(Utils.formatDoubleValue(value));
			    }},
			    {field: 'status', title: price_grid_column12, width: 40, sortable: false, align: 'left', formatter: function(value, row, index) {
			    	return statusTypeText.parseValue(value);
			    }},
			    {field: 'change', title: '', width: 20, align: 'center', formatter: function(value, row, index) {
			    	var html = '';
			    	if (row.status != undefined && row.status != null && row.status == 1) {
			    		html += '<a href="javascript:void(0)" id="dg_priceManage_stop" onclick="return SaleInPriceManager.stopPriceManager(' + row.id + ',' + row.status + ',\'' + row.productCode + '\');"><img title="' + common_status_stopped + '" src="/resources/images/icon-stop.png" width="16" heigh="16"></a>';
			    	}
			    	return html;
			    }}
			]],
	        onLoadSuccess: function(data) {
				$('.datagrid-header-rownumber').html(price_stt);
				Utils.updateRownumWidthAndHeightForDataGrid('dgProductPrice');
				if($('td[field="change"] a').length == 0) {
			 		$('#dgProductPrice').datagrid("hideColumn", "change");
			 	}else{
			 		$('#dgProductPrice').datagrid("showColumn", "change");
			 	}
				//Phan quyen control
			 	var arrDelete =  $('#productPriceDgDiv td[field="change"]');
				if (arrDelete != undefined && arrDelete != null && arrDelete.length > 0) {
				  for (var i = 0, size = arrDelete.length; i < size; i++) {
				    $(arrDelete[i]).prop("id", "group_edit_pr_gr_row_change_" + i);
					$(arrDelete[i]).addClass("cmsiscontrol");
				  }
				}
				Utils.functionAccessFillControl('productPriceDgDiv', function(data) {
					//Xu ly cac su kien lien quan den cac control phan quyen
					var arrTmpLength =  $('#productPriceDgDiv td[id^="group_edit_pr_gr_row_change_"]').length;
					var invisibleLenght = $('.isCMSInvisible[id^="group_edit_pr_gr_row_change_"]').length;
					if (arrTmpLength > 0 && arrTmpLength != invisibleLenght) {
						$('#dgProductPrice').datagrid("showColumn", "change");
					} else {
						$('#dgProductPrice').datagrid("hideColumn", "change");
					}
				});
	        }
		});
	},
	
	/**
	 * tam ngung gia sale in
	 * @author trietptm
	 * @since Jan 20, 2016
	 */
	stopPriceManager: function(id, status, productCode) {
		if(status != undefined && status != null && (status == 2 || status == 1)) {
			$(".ErrorMsgStyle").html('').hide();
			var msg = "";
			if (productCode == undefined || productCode == null) {
				productCode = "";
			}
			productCode = productCode.toString().trim();
			var params = {
				id: id				
			};
			var msg = sale_in_price_do_you_want_stop + productCode + "?";
			Utils.addOrSaveData(params, "/sale-in/stopPrice", null, 'errMsgPriceManager', function(data) {
				SaleInPriceManager.searchProductPrice();
				$("#successMsgPriceManager").html(price_search_error_03).show();
				setTimeout(function() {
					$('.SuccessMsgStyle').html("").hide();
				 }, 1500);
			}, null, null, null, msg);
		}
	},
	
	/**
	 * import excel
	 * @author trietptm
	 * @since Jan 20, 2016
	 */
	importPriceProduct: function() {
		$('.SuccessMsgStyle').html('').hide();
		$('.ErrorMsgStyle').html('').hide();
		
		Utils.importExcelUtils(function(data) {
			SaleInPriceManager.searchProductPrice();
			$('#excelFileFormPrice').val("").change();
			$('#fakefilepc').val("").change();
			if (data.message != undefined && data.message != null && data.message.trim().length > 0) {
				$('#errMsgImportPrice').html(data.message.trim()).change().show();
			} else {
				$('#successMsgImportPrice').html(price_search_error_03).show();
				var tm = setTimeout(function() {
					//Load lai danh sach quyen
					$('#successMsgImportPrice').html("").hide();
					clearTimeout(tm);
				 }, 1500);
			}
		}, 'importFrmPrice', 'excelFileFormPrice', 'errMsgImportPrice');
	},
	
	/**
	 * export excel
	 * @author trietptm
	 * @since Jan 20, 2016
	 */
	exportBySearchPrice: function() {
		$('.ErrorMsgStyle').html('').hide();
		var msg = '';
		if (SaleInPriceManager._paramSearch != null) {
			var rows = $('#dgProductPrice').datagrid("getData");
			if (rows == null || rows.length == 0) {
				msg = price_search_error_09;
			}
		} else {
			msg = price_search_error_09;
		}
		if (msg.length > 0) {
			$('#errMsgImportPrice').html(msg).show();
			return false;
		}
		var msg = price_search_error_10;
		$.messager.confirm(price_grid_column18, msg, function(r) {
			if (r) {
				var params = SaleInPriceManager._paramSearch;
				ReportUtils.exportReport('/sale-in/exportPriceProduct', params, 'errMsgPermission');
			}
		});
	},
	
	/**
	 * xu ly sau khi chon sp
	 * @author trietptm
	 * @since Jan 20, 2016
	 */
	fillTxtProductCodeByF9 : function(code,name) {
		$('#txtProductCode').val(code).show();
		$('#txtProductName').val(name).show();
		$('#common-dialog-search-2-textbox').dialog('close');
		$('#txtProductCode').focus();
	},

	/**
	 * download file template import gia sale in
	 * @author trietptm
	 * @since Jan 20, 2016
	 */
	downloadImportPriceTemplateFile: function() {
		var url = "/sale-in/download-import-price-template-file";
		ReportUtils.exportReport(url, {}, 'errMsg');
		return false;
	},
};
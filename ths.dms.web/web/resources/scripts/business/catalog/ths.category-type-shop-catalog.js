var CategoryTypeCatalog ={
	_xhrSave : null,
	_xhrDel: null,
	lstCategory : null,
	iload:1,
	indexedit:0,
	getGridUrl: function(shopCode,shopName,status,categoryType){
		var url = "/cargo-brand-distributer/search?shopCode=" + encodeChar(shopCode) 
		+ "&shopName=" + encodeChar(shopName) 
		+ "&status=" + status;
		if($.isArray(categoryType)) {
			for(var i = 0; i < categoryType.length; i++) {
				url = url + "&lstcategory=" + categoryType[i];
			}
		}
		return url;
	},
	fillTxtShopCodeByF9 : function(code){
		$('#shopCode').val(Utils.XSSEncode(code)).show();
		$('#common-dialog-search-2-textbox').dialog('close');
		$('#shopCode').focus();
	},
	search: function(){
		$('#errMsg').html('').hide();
		var shopCode = $('#shopCode').val().trim();
		var arrayCategory = new Object();
		var shopName = '';
		var categoryType = $('#category').val();
		CategoryTypeCatalog.lstCategory = categoryType;
		arrayCategory.listCategory = CategoryTypeCatalog.lstCategory;
		var status = $('#status').val();
		var tm = setTimeout(function() {
			$('#shopCode').focus();
		}, 500);
		var url = CategoryTypeCatalog.getGridUrl(shopCode,shopName,status,arrayCategory.listCategory);
		$("#grid").datagrid({url:url,pageNumber:1});
		if(CategoryTypeCatalog.iload==1){
			CategoryTypeCatalog.iload += 3;
		}else{
			CategoryTypeCatalog.iload +=1;
		}
		return false;
	},
	exportExcel: function() {
		var shopCode = $('#shopCode').val().trim();
		var shopName = '';
		var status = $('#status').val();
		var params = new Object();
		params.shopCode = shopCode;
		params.shopName = shopName;
		if(CategoryTypeCatalog.lstCategory!=null){
			params.lstcategory = CategoryTypeCatalog.lstCategory;
		}
		params.status = status;
		
		var url = "/cargo-brand-distributer/export-excel";
//		CommonSearch.exportExcelData(params,url,'errExcelMsg');
		ReportUtils.exportReport(url, params, 'errExcelMsg');
	},
	change: function(){
		var msg = '';
		$('.ErrorMsgStyle').html('').hide();
		var shopObjectType = $('#shopObjectType').val();
		if(shopObjectType!=undefined && shopObjectType!=null 
				&& parseInt(shopObjectType)!= ShopObjectType.VNM && parseInt(shopObjectType)!= ShopObjectType.GT){
			CategoryTypeProductCatalog.search();
			msg = "Không được quyền thực hiện chức năng";
			$('#errMsg').html(msg).show();
			return false;
		}
		msg = Utils.getMessageOfRequireCheck('shopCode1','Mã đơn vị');
		if(msg.length == 0){
			msg = Utils.getMessageOfSpecialCharactersValidate('shopCode1','Mã đơn vị',Utils._CODE);
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfRequireCheck('stockMin','Tồn kho Min');
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfSpecialCharactersValidate('stockMin','Tồn kho Min');
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfInvaildNumber('stockMin', 'Tồn kho Min');
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfNegativeNumberCheck('stockMin','Tồn kho Min');
		}
		if(msg.length == 0 && $('#stockMin').val().replace(/,/g,'')>9999){
			msg='Tồn kho min không được vượt quá 9999';
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfRequireCheck('stockMax','Tồn kho Max');
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfSpecialCharactersValidate('stockMax','Tồn kho Max');
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfInvaildNumber('stockMax', 'Tồn kho Max');
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfNegativeNumberCheck('stockMax','Tồn kho Max');
		}
		if(msg.length == 0 && $('#stockMax').val().replace(/,/g,'')>9999){
			msg='Tồn kho max không được vượt quá 9999';
		}
		if(msg.length == 0){
			if($('#stockMin').val().trim() != '' && $('#stockMax').val().trim() != ''){
				if(parseInt($('#stockMin').val().trim()) >= parseInt($('#stockMax').val().trim())){
					msg = "Tồn kho Min phải bé hơn tồn kho Max";
					$('#errMsg').html(msg).show();
					$('#stockMin').focus();
					return false;
				}
			}
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfRequireCheck('dateGo','Ngày đi đường');
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfSpecialCharactersValidate('dateGo','Ngày đi đường');
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfInvaildNumber('dateGo', 'Ngày đi đường');
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfNegativeNumberCheck('dateGo','Ngày đi đường');
		}
		if(msg.length == 0 && $('#dateGo').val().replace(/,/g,'')>999){
			msg='Ngày đi đường không được vượt quá 999';
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfRequireCheck('dateSale','Ngày bán hàng');
		}
		if(msg.length == 0){
			if(CategoryTypeCatalog.parseCharToValue($('#dateSale').val().trim()) == '0000000'){
				msg = 'Bạn chưa nhập giá trị cho trường Ngày bán hàng. Yêu cầu nhập giá trị';
				//$('#dateSale').focus();
			}
		}
		if(msg.length > 0){
			$('#errMsg').html(msg).show();
			return false;
		}
		var dataModel = new Object();
		dataModel.categoryId = $('#categoryId').val();
		dataModel.shopCode = $('#shopCode1').val().trim();
		dataModel.categoryType = $('#categoryTypeId').val();
		dataModel.stockMin = $('#stockMin').val().replace(/,/g,'').trim();
		dataModel.stockMax = $('#stockMax').val().replace(/,/g,'').trim();
		dataModel.dateGo = $('#dateGo').val().replace(/,/g,'').trim();
		dataModel.dateSale = $('#dateSale').val().trim();
		dataModel.growth = $('#growth').val().trim();
		dataModel.status = $('#status1').val();
		Utils.addOrSaveData(dataModel, "/cargo-brand-distributer/save", CategoryTypeCatalog._xhrSave, null,function(data){			
			if(data.error == false){
				$("#grid").datagrid('reload');
				return true;
			}
		},null,null,null,null,function(data){
			if(data.error == true){
				//$('#shopCode').focus();
			}
		});
		$('#grid').datagrid('beginEdit', CategoryTypeCatalog.indexedit);
		return true;
	},
	deleteRow: function(categoryId){
		$('#errMsg').html('').hide();
		var dataModel = new Object();
		dataModel.categoryId = categoryId;	
		Utils.deleteSelectRowOnGrid(dataModel, 'ngành hàng bán cho đơn vị', "/cargo-brand-distributer/remove", CategoryTypeCatalog._xhrDel, null, null);
		return false;		
	},
	mapValue:function(){
		var num =0;
		var value = "0000000";
		if($('#monday').is(':checked')){
			num++;
			value+=($('#monday').val());
		}
		if($('#tuesday').is(':checked')){
			num++;
			value+=($('#tuesday').val());
		}
		if($('#wednesday').is(':checked')){
			num++;
			value+=($('#wednesday').val());
		}
		if($('#thursday').is(':checked')){
			num++;
			value+=($('#thursday').val());
		}
		if($('#friday').is(':checked')){
			num++;
			value+=($('#friday').val());
		}
		if($('#saturday').is(':checked')){
			num++;
			value+=($('#saturday').val());
		}
		if($('#sunday').is(':checked')){
			num++;
			value+=($('#sunday').val());
		}
		value = value.substring(num);
		if(value == "0000000"){
			$('#errMsg1').html('Vui lòng chọn ngày làm việc').show();
			var tm = setTimeout(function(){
				$('#errMsg1').html('').hide();
				clearTimeout(tm); }, 3000);
			return false;
		}
		$('#dateSale').val(value);
		//$('#dateSale1').val(CategoryTypeCatalog.parseValueToChar(value));
		$('#dateSale1').val(value);
		$(".datagrid-btable td[field='calendarD'] table td input[type=text]").val($('#dateSale1').val());
		//$('.ToolTipCalandar').hide();
		$("#popup1").dialog("close");
	},
	parseValueToChar:function(value){
		var char = "";
		if(value != null || value != undefined){
			var i = value.length;
			for(var j=0;j<i;j++){
				if(value.charAt(j) == 2){
					char += "T2";
				}else if(value.charAt(j) == 3){
					char += "T3";
				}else if(value.charAt(j) == 4){
					char += "T4";
				}else if(value.charAt(j) == 5){
					char += "T5";
				}else if(value.charAt(j) == 6){
					char += "T6";
				}else if(value.charAt(j) == 7){
					char += "T7";
				}else if(value.charAt(j) == 1){
					char += "CN";
				}
				if(char != ""){
					char += " ";
				}
			}
			return char;
		}else{
			return char;
		}
	},
	parseCharToValue:function(char){
		var value = "0000000";
		if(char != null || char != undefined){
			var i = char.trim().split(" ").length;
			var ch = char.trim().split(" ");
			for(var j=0;j<i;j++){
				if(ch[j] == "T2"){
					value += "2";
				}else if(ch[j] == "T3"){
					value += "3";
				}else if(ch[j] == "T4"){
					value += "4";
				}else if(ch[j] == "T5"){
					value += "5";
				}else if(ch[j] == "T6"){
					value += "6";
				}else if(ch[j] == "T7"){
					value += "7";
				}else if(ch[j] == "CN"){
					value += "1";
				}
			}
			return value.substring(i);
		}else{
			return value;
		}
	},
	ViewFileUpload: function(typeView){
		$('#importFrm').submit();
	}
};
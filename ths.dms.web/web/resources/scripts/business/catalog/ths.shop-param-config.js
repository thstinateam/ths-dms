/**
 * ShopParamConfig
 * @author vuongmq
 * @since Nov 27, 2015
 */
var ShopParamConfig  = {
	_lstDayStartDelete: null,
	_lstDayEndDelete: null,
	_mapCloseDayStart: null,
	_MAX_LENGTH_3: 3,
	_INSERT: 1,
	_EDIT: 2,
	_DELETE: 3,
	_HH: (1 * 60 * 60 * 1000), //1 gio: (1 * 60 * 60 * 1000): phut
	_MM:      (1 * 60 * 1000), //1phut:      (1 * 60 * 1000): phut

	/**
	 * setInputIsEmpty shop param
	 * @author vuongmq
	 * @since Nov 27, 2015
	 */
	setInputIsEmpty: function() {
		$('#shopDistanceOrder').val(''); // ShopId chinh la id cua combobox shop chon

		$('#shopDTStart').val('');
		$('#shopDTMiddle').val('');
		$('#shopDTEnd').val('');
		
		$('#shopCCDistance').val('');
		$('#shopCCStart').val('');
		$('#shopCCEnd').val('');

		//danh sach Id cua ShopParam
		$('#hdShopDTStart').val('');
		$('#hdShopDTMiddle').val('');
		$('#hdShopDTEnd').val('');
		
		$('#hdShopCCDistance').val('');
		$('#hdShopCCStart').val('');
		$('#hdShopCCEnd').val('');

		ShopParamConfig._lstDayStartDelete = [];
		ShopParamConfig._lstDayEndDelete = [];
		ShopParamConfig.initGridCloseDayStart();
		ShopParamConfig.initGridCloseDayEnd();
		ShopParamConfig.initGridProductStockDay();
	},

	/**
	 * search shop param
	 * @author vuongmq
	 * @param shopId
	 * @since Nov 27, 2015
	 */
	search: function(shopId) {
		hideAllMessage();
		var shopCode = $('#cbxShop').combobox('getValue');
		if (shopCode.length == 0) {
			$('#errMsgSearch').html('Vui lòng chọn nhà phân phối').show();
			return false;
		}
		var params = {shopId: shopId};
		var url = '/catalog/shop-param/search';
		ShopParamConfig.setInputIsEmpty();
		Utils.getJSONDataByAjax(params, url, function(data) {
			if (data != undefined && data != null && data.shopParamVOSave != null) {
				var row = data.shopParamVOSave;
				var shopDistanceOrder = Utils.XSSEncode(formatCurrency(row.shopDistanceOrder));
				$('#shopDistanceOrder').val(shopDistanceOrder); // ShopId chinh la id cua combobox shop chon

				var shopDTStart = Utils.XSSEncode(row.shopDTStart);
				var shopDTMiddle = Utils.XSSEncode(row.shopDTMiddle);
				var shopDTEnd = Utils.XSSEncode(row.shopDTEnd);
				
				var shopCCDistance = Utils.XSSEncode(formatCurrency(row.shopCCDistance));
				var shopCCStart = Utils.XSSEncode(row.shopCCStart);
				var shopCCEnd = Utils.XSSEncode(row.shopCCEnd);
				
				$('#shopDTStart').val(shopDTStart);
				$('#shopDTMiddle').val(shopDTMiddle);
				$('#shopDTEnd').val(shopDTEnd);
				
				$('#shopCCDistance').val(shopCCDistance);
				$('#shopCCStart').val(shopCCStart);
				$('#shopCCEnd').val(shopCCEnd);

				//danh sach Id cua ShopParam
				var shopDTStartId = Utils.XSSEncode(row.shopDTStartId);
				var shopDTMiddleId = Utils.XSSEncode(row.shopDTMiddleId);
				var shopDTEndId = Utils.XSSEncode(row.shopDTEndId);
				
				var shopCCDistanceId = Utils.XSSEncode(row.shopCCDistanceId);
				var shopCCStartId = Utils.XSSEncode(row.shopCCStartId);
				var shopCCEndId = Utils.XSSEncode(row.shopCCEndId);
				
				$('#hdShopDTStart').val(shopDTStartId);
				$('#hdShopDTMiddle').val(shopDTMiddleId);
				$('#hdShopDTEnd').val(shopDTEndId);
				
				$('#hdShopCCDistance').val(shopCCDistanceId);
				$('#hdShopCCStart').val(shopCCStartId);
				$('#hdShopCCEnd').val(shopCCEndId);

				$('#closeDayHM').val(data.closeDayHM);
				$('#beforeMinute').val(data.beforeMinute);
			}
		});
	},

	/**
	 * getInputValue shop param
	 * @author vuongmq
	 * @since Nov 27, 2015
	 */
	getInputValue: function() {
		var paramSearch = new Object();
		paramSearch.shopDistanceOrder = Utils.returnMoneyValue($('#shopDistanceOrder').val()); // ShopId chinh la id cua combobox shop chon

		paramSearch.shopDTStart = $('#shopDTStart').val();
		paramSearch.shopDTMiddle = $('#shopDTMiddle').val();
		paramSearch.shopDTEnd = $('#shopDTEnd').val();
		
		paramSearch.shopCCDistance = Utils.returnMoneyValue($('#shopCCDistance').val());
		paramSearch.shopCCStart = $('#shopCCStart').val();
		paramSearch.shopCCEnd = $('#shopCCEnd').val();

		//danh sach Id cua ShopParam
		paramSearch.shopDTStartId = $('#hdShopDTStart').val();
		paramSearch.shopDTMiddleId = $('#hdShopDTMiddle').val();
		paramSearch.shopDTEndId = $('#hdShopDTEnd').val();
		
		paramSearch.shopCCDistanceId = $('#hdShopCCDistance').val();
		paramSearch.shopCCStartId = $('#hdShopCCStart').val();
		paramSearch.shopCCEndId = $('#hdShopCCEnd').val();
		return paramSearch;
	},

	/**
	 * save info shop param
	 * @author vuongmq
	 * @since Nov 27, 2015
	 */
	saveInfo: function() {
		hideAllMessage();
		var msg = Utils.getMessageOfRequireCheck('shopId', 'Nhà phân phối');
		if (msg.length == 0) {
			var valueShop = $('#cbxShop').combobox('getValue');
			if (isNullOrEmpty(valueShop)) {
				msg = 'Vui lòng chọn nhà phân phối.';
			}
		}
		if (msg.length > 0) {
			$('#errorMsg').html(msg).show();
			return false;
		}
		var lstDayStart = [];
		var rowStart = $('#gridCloseDayStart').datagrid('getRows');
		if (rowStart != null && rowStart.length > 0) {
			for (var i = 0, sz = rowStart.length; i < sz; i++) {
				var row = rowStart[i];
				if (row.updateFlag != activeType.RUNNING) {
					var newData = new Object();
					newData.valueDateStr = row.valueDate;
					newData.valueTimeStr = row.valueTime;
					lstDayStart.push(newData);
				}
			}
		}
		var lstDayEnd = [];
		var rowEnd = $('#gridCloseDayEnd').datagrid('getRows');
		if (rowEnd != null && rowEnd.length > 0) {
			for (var i = 0, sz = rowEnd.length; i < sz; i++) {
				var row = rowEnd[i];
				var newData = new Object();
				newData.fromDateStr = row.valueFromDateStr;
				newData.toDateStr = row.valueToDateStr;
				if (row.id != undefined && row.id != null) {
					newData.id = row.id;
				}
				if (row.updateFlag != undefined && row.updateFlag != null) {
					newData.updateFlag = row.updateFlag;
				}
				lstDayEnd.push(newData);
			}
		}
		var lstProductShopMapVO = [];
		var errStock = '';
		var rowStock = $('#gridProductStockDay').datagrid('getRows');
		$('.stockDayClazz').each(function(i) {
			var value = $(this).val();
			if (!isNullOrEmpty(value)) {
				if (isNaN(value)) {
					errStock = format('Số ngày tồn kho dòng {0} không phải định dạng số', (i + 1));
					return false;
				}
				if (errStock.length == 0 && Number(value) < 0) {
					errStock = format('Số ngày tồn kho dòng {0} nhập vào phải là số nguyên.', (i + 1));
					return false;
				}
				if (errStock.length == 0 && value.trim().length > ShopParamConfig._MAX_LENGTH_3) {
					errStock = format('Số ngày tồn kho dòng {0} nhập vào phải nhỏ hơn hoặc bằng {1} ký tự.', (i + 1), ShopParamConfig._MAX_LENGTH_3);
					return false;
				}
			}
			var newData = new Object();
			newData.productId = rowStock[i].productId;
		    newData.stockDay = value;
		    if (rowStock[i].shopId != undefined && rowStock[i].shopId != null) {
		    	newData.shopId = rowStock[i].shopId;
		    }
		    if (rowStock[i].id != undefined && rowStock[i].id != null) {
		    	newData.id = rowStock[i].id;
		    }
		    lstProductShopMapVO.push(newData);
		})
		if (errStock.length > 0) {
			$('#errorMsg').html(errStock).show();
			return false;
		}
		var shopId = $('#curShopId').val();
		var paramVOSave = ShopParamConfig.getInputValue(); // danh sach value co ban
		var params = new Object();
		params.shopId = shopId;
		if (ShopParamConfig._lstDayStartDelete != null && ShopParamConfig._lstDayStartDelete.length > 0) {
			paramVOSave.lstDayStartDelete = ShopParamConfig._lstDayStartDelete;
		}
		paramVOSave.lstDayStart = lstDayStart;
		if (ShopParamConfig._lstDayEndDelete != null && ShopParamConfig._lstDayEndDelete.length > 0) {
			paramVOSave.lstDayEndDelete = ShopParamConfig._lstDayEndDelete;
		}
		paramVOSave.lstDayEnd = lstDayEnd;
		params.shopParamVOSave = paramVOSave;
		params.lstProductVO = lstProductShopMapVO;
		var url = "/catalog/shop-param/saveInfo";
		var message = "Bạn có muốn lưu thông tin cấu hình này?";
		JSONUtil.saveData2(params, url, message, "errorMsg", function() {
			var tm = setTimeout(function() {
				$('#successMsg').html("").hide();
				clearTimeout(tm);
				//window.location.href = '/catalog/shop-param/info';
				var shopId = $('#curShopId').val();
				ShopParamConfig.search(shopId);
			}, 2000);
		});	
		return false;
	},
	
	/**
	 * Delete row grid
	 * @author vuongmq
	 * @param grid
	 * @param index
	 * @since 20/11/2015 
	 */
	deleteRowGrid: function(grid, index) {
		$('#' + grid).datagrid('deleteRow', index);
		$('#' + grid).datagrid('loadData', $('#' + grid).datagrid('getRows'));

	},

	/**
	 * Init grid danh sach close day start
	 * @author vuongmq
	 * @since 27/11/2015 
	 */
	initGridCloseDayStart: function() {
		var params = new Object();
		params.shopId = $('#curShopId').val();
		var wd = $(window).width() - 500;
		$('#gridCloseDayStart').datagrid({  
		    url: '/catalog/shop-param/getShopLockExecTime',
		    rownumbers: true,
		    pagination: true,
		    singleSelect: false,
		    autoRowHeight: true,
		    pageNumber: 1,
		    pageSize: 10,
		    pageList: [10, 20, 50, 100, 500],
		    width: wd,
		    //width: 800,
		    queryParams: params,
		    //fitColumns: true,
		    scrollbarSize: 0,
		    columns: [[
				{field: 'valueDate', title: 'Ngày', width: wd - 300 - 100 - 31, align: 'center', resizable: false, formatter: function(value, row, index) {
					return Utils.XSSEncode(value);
				}},
				{field: 'valueTime', title: 'Thời gian', width: 300, align: 'center', resizable: false, formatter: function(value, row, index) {
		        	return Utils.XSSEncode(value);
		        }},
		        {field:'changeStart', title: '<a href="javaScript:void(0);" onclick="return ShopParamConfig.viewDialogAddCloseDayStart();"><img title="Thêm thời gian chốt trong ngày" src="/resources/images/icon_add.png" width="18" heigh="18"/></a>', width: 100, align: 'center', resizable: false, formatter: function(value, row, index) {
			    	var html = '<a href="javascript:void(0)" onclick="return ShopParamConfig.deleteCloseDayStart(' + index + ');"><img title="Xóa thời gian chốt trong ngày" src="/resources/images/icon_delete.png" width="15" heigh="15"></a>';
			    	return html;
			    }}, 
		    ]],
		    onLoadSuccess: function(data) {
		    	$('.datagrid-header-rownumber').html('STT');
			    updateRownumWidthForJqGrid('#closeDayStartResult');
		   	 	$(window).resize();
		   	 	ShopParamConfig._mapCloseDayStart = new Map();
		   	 	if (data != null && data.rows != null && data.rows.length > 0) {
		   	 		for (var i = 0, sz = data.rows.length; i < sz; i++) {
		   	 			var row = data.rows[i];
		   	 			if (row != null && !isNullOrEmpty(row.valueDate)) {
		   	 				ShopParamConfig._mapCloseDayStart.put(row.valueDate, row.valueTime);
		   	 			}
		   	 		}
		   	 	}
		   	 	
		    }		    
		});
	},

	/**
	 * View popup add close day start
	 * @author vuongmq
	 * @since 01/12/2015 
	 */
	viewDialogAddCloseDayStart: function() {
		hideAllMessage();
		$('#popupCloseDayStart').dialog({
			title: 'Thêm ngày thời gian chốt ngày',
			closed: false,
			cache: false,
			modal: true,
			width: 370,
			height: 170,
			onOpen: function() {
				/*setDateTimePicker('popDate');
				$('#popDate').val(getNextBySysDateForNumber(1));
				var inputPopTime = $('#popupCloseDayStart #popTime').clockpicker({
				    placement: 'bottom',
				    align: 'left',
				    autoclose: true
				});*/
				$('#popDateTime').datetimepicker({
			 		lang:'vi',
			 		format:'d/m/Y H:i'
				});
				var currentTime = new Date();
				var month = currentTime.getMonth() + 1;
				var day = currentTime.getDate();
				var year = currentTime.getFullYear();
				var hour = currentTime.getHours();
				var minute = currentTime.getMinutes();
				if (month < 10) {
					month = '0' + month;
				}
				if (day < 10) {
					day = '0' + day;
				}
				if (hour < 10) {
					hour = '0' + hour;
				}
				if (minute < 10) {
					minute = '0' + minute;
				}
				$('#popDateTime').val(day + '/' + month + '/' + year + ' 00:00');
				Utils.bindFormatOnTextfield('popDateTime', Utils._TF_NUMBER);
			},
    	});
	},

	/**
	 * checkCloseDayStartBeforeMinute
	 * // thoi gian endTime truoc startTime (minuteBefore) la true
	 * @author vuongmq
	 * @param startTime (gio cho tu dong)
	 * @param endTime (gio chon)
	 * @param minuteBefore (thoi gian cau hinh, mac dinh 10p)
	 * @param type (INSERT, DELETE); default undefined: INSERT
	 * @return String (chuoi loi)
	 * @since 02/12/2015 
	 */
	checkCloseDayStartBeforeMinute: function(startTime, endTime, minuteBefore, type) {
		var msg = '';
		var hh = ShopParamConfig._HH;
		var mm = ShopParamConfig._MM;
		var startTimeStr = startTime.split(':');
		var hStart = Number(startTimeStr[0]);
		var mStart = Number(startTimeStr[1]);
		var endTimeStr = endTime.split(':');
		var hEnd = Number(endTimeStr[0]);
		var mEnd = Number(endTimeStr[1]);
		minuteBefore = Number(minuteBefore);
		var startTimeTotal = (hStart * hh) + (mStart * mm) - (minuteBefore * mm);
		var d = new Date();
		var hCur = d.getHours();
		var mCur = d.getMinutes();
		var curTimeTotal = (hCur * hh) + (mCur * mm);
		// gio hien tai <= gio chot tu dong - 10p; 
		if (msg.length == 0) {
			if (curTimeTotal > startTimeTotal) {
				if (type == undefined || type != ShopParamConfig._DELETE) {
					msg = format('Chỉ cho thêm dòng của ngày hiện tại nếu giờ phút hiện tại trước giờ phút tự động chốt {0} phút.', minuteBefore);
				} else {
					msg = format('Xóa dòng của ngày hiện tại nếu giờ phút hiện tại trước giờ phút tự động chốt {0} phút.', minuteBefore);
				}
			}
		}
		var endTimeTotal = (hEnd * hh) + (mEnd * mm);
		curTimeTotal = curTimeTotal + (minuteBefore * mm);
		// gio chon >= gio hien tai + 10p
		if (msg.length == 0) {
			if (endTimeTotal < curTimeTotal) {
				if (type == undefined || type != ShopParamConfig._DELETE) {
					msg = format('Chỉ cho thêm dòng của ngày hiện tại nếu giờ phút chọn sau giờ phút hiện tại {0} phút.', minuteBefore);
				} else {
					msg = format('Xóa dòng của ngày hiện tại nếu giờ phút chọn sau giờ phút hiện tại {0} phút.', minuteBefore);
				}
			}
		}
		return msg;
	},

	/**
	 * grid addCloseDayStart
	 * @author vuongmq
	 * @since 30/11/2015 
	 */
	addCloseDayStart: function() {
		hideAllMessage();
		var msg = '';
		var popDate = $('#popDateTime').val();
		if (popDate.trim().length < 16 || !Utils.isDateNew(popDate)) {
			msg = 'Ngày thời gian không hợp lệ. Vui lòng chọn lại';
		}
		var valueDateStr = popDate.substring(0, 10).trim();
		var valueTimeStr = popDate.substring(11, 16).trim();
		if (msg.length == 0) {
			if (ShopParamConfig._mapCloseDayStart != null && ShopParamConfig._mapCloseDayStart.findIt(valueDateStr) >= 0) {
				msg = 'Ngày thêm đã tồn tại trong đổi thời gian chốt ngày, Vui lòng chọn lại';
			}
		}
		var curDate = ReportUtils.getCurrentDateString();
		if (msg.length == 0) {
			if (Utils.compareDate1(valueDateStr, curDate)) {
				msg = 'Ngày thêm phải lớn hơn hoặc bằng ngày hiện tại, Vui lòng chọn lại';
			}
		}
		if (msg.length == 0) {
			if (Utils.compareDateInt(valueDateStr, curDate) == 0) {
				var startTime = $('#closeDayHM').val();
				var minuteBefore = $('#beforeMinute').val();
				msg = ShopParamConfig.checkCloseDayStartBeforeMinute(startTime, valueTimeStr, minuteBefore);
			}
		}
		if (msg.length > 0) {
			$('#errMsgPopDayStart').html(msg).show();
			return false;
		}
		var numRow = $('#gridCloseDayStart').datagrid('getRows').length;
		var newData = new Object();
		newData.valueDate = valueDateStr;
		newData.valueTime = valueTimeStr;
		$('#gridCloseDayStart').datagrid('insertRow', {index: numRow, row: newData});
		ShopParamConfig._mapCloseDayStart.put(valueDateStr, valueTimeStr);
		$('#popupCloseDayStart').dialog('close');
	},

	/**
	 * grid deleteCloseDayStart
	 * @author vuongmq
	 * @param index
	 * @since 30/11/2015 
	 */
	deleteCloseDayStart: function(index) {
		var row = $('#gridCloseDayStart').datagrid('getRows')[index];
		if (row != null) {
			if (row.updateFlag == activeType.RUNNING) {
				var msg = '';
				var valueDate = row.valueDate;
				if (!isNullOrEmpty(valueDate)) {
					var curDate = ReportUtils.getCurrentDateString();
					if (Utils.compareDate1(valueDate, curDate)) {
						msg = 'Ngày xóa không được nhỏ ngày hiện tại.';
						$.messager.alert('Cảnh báo', msg, 'warning');
						return false;
					}
					// Neu ngay xoa bang ngay hien tai
					var valueTimeStr = row.valueTime;
					if (Utils.compareDateInt(valueDate, curDate) == 0 && !isNullOrEmpty(valueTimeStr)) {
						var startTime = $('#closeDayHM').val();
						var minuteBefore = $('#beforeMinute').val();
						msg = ShopParamConfig.checkCloseDayStartBeforeMinute(startTime, valueTimeStr, minuteBefore, ShopParamConfig._DELETE);
						if (msg.length > 0) {
							$.messager.alert('Cảnh báo', msg, 'warning');
							return false;	
						}
					}
				}
			}
			var message = 'Bạn có muốn xóa dòng đổi thời gian chốt ngày này? <br>';
			message += 'Thay đổi được lưu khi nhấn nút Cập nhật.';
			$.messager.confirm(jsp_common_xacnhan, message, function(r) {
				if (r) {
					if (row.updateFlag == activeType.RUNNING) {
						if (ShopParamConfig._lstDayStartDelete != null && ShopParamConfig._lstDayStartDelete.length > 0) {
								ShopParamConfig._lstDayStartDelete.push(row.id);
						} else {
							ShopParamConfig._lstDayStartDelete = [];
							ShopParamConfig._lstDayStartDelete.push(row.id);
						}
					}
					ShopParamConfig.deleteRowGrid('gridCloseDayStart', index);
				}
			});
		}
	},

	/**
	 * Init grid danh sach close day end
	 * @author vuongmq
	 * @since 27/11/2015 
	 */
	initGridCloseDayEnd: function() {
		var params = new Object();
		params.shopId = $('#curShopId').val();
		var wd = $(window).width() - 500;
		$('#gridCloseDayEnd').datagrid({  
		    url: '/catalog/shop-param/getShopLockAbortDuration',
		    rownumbers: true,
		    pagination: true,
		    singleSelect: false,
		    autoRowHeight: true,
		    pageNumber: 1,
		    pageSize: 10,
		    pageList: [10, 20, 50, 100, 500],
		    width: wd,  		    
		    //width: 800,
		    queryParams: params,
		    //fitColumns: true,
		    scrollbarSize: 0,
		    columns: [[
				{field: 'valueFromDateStr', title: 'Từ ngày', width: wd - 250 - 150 - 31, align: 'center', resizable: false, formatter: function(value, row, index) {
					return Utils.XSSEncode(value);
				}},
				{field: 'valueToDateStr', title: 'Đến ngày', width: 250, align: 'center', resizable: false, formatter: function(value, row, index) {
		        	return Utils.XSSEncode(value);
		        }},
		        {field: 'changeEnd', title: '<a href="javaScript:void(0);" onclick="return ShopParamConfig.viewDialogAddCloseDayEnd(' + ShopParamConfig._INSERT + ');"><img title="Thêm ngày chốt tự động" src="/resources/images/icon_add.png" width="18" heigh="18"/></a>', width: 150, align: 'center', resizable: false, formatter: function(value, row, index) {
			    	var html = '<a href="javascript:void(0)" onclick="return ShopParamConfig.viewDialogAddCloseDayEnd(' + ShopParamConfig._EDIT + ',' + index + ');"><img title="Chỉnh sửa ngày chốt tự động" src="/resources/images/icon_edit.png" width="15" heigh="15"></a> &nbsp &nbsp';
			    		html += '<a href="javascript:void(0)" onclick="return ShopParamConfig.deleteCloseDayEnd(' + index + ');"><img title="Xóa ngày chốt tự động" src="/resources/images/icon_delete.png" width="15" heigh="15"></a>';
			    	return html;
			    }}, 
		    ]],
		    onLoadSuccess: function(data) {
		    	$('.datagrid-header-rownumber').html('STT');
			    updateRownumWidthForJqGrid('#closeDayEndResult');
		   	 	$(window).resize();
		    }		    
		});
	},

	/**
	 * View popup add close day end
	 * @author vuongmq
	 * @param type
	 * @param index
	 * @since 01/12/2015 
	 */
	viewDialogAddCloseDayEnd: function(type, index) {
		hideAllMessage();
		var row = null;
		var titlePop = 'Thêm thời gian tạm dừng chốt tự động';
		if (type == ShopParamConfig._EDIT) {
			titlePop = 'Chỉnh sửa thời gian tạm dừng chốt tự động';
			row = $('#gridCloseDayEnd').datagrid('getRows')[index];
			if (row != null) {
				var curDate = ReportUtils.getCurrentDateString();
				if (Utils.compareDate(row.valueToDateStr, curDate)) {
					var msg = 'Đến ngày nhỏ hơn hoặc bằng ngày hiện tại không được chỉnh sửa.';
					$.messager.alert('Cảnh báo', msg, 'warning');
					return false;
				}
			}
		}
		$('#popupCloseDayEnd').dialog({
			title: titlePop,
			closed: false,
			cache: false,
			modal: true,
			width: 480,
			height: 160,
			onOpen: function() {
				setDateTimePicker('popFromDate');
				setDateTimePicker('popToDate');
				if (type == ShopParamConfig._INSERT) {
					$('#popFromDate').val(getNextBySysDateForNumber(1));
					$('#popToDate').val(getNextBySysDateForNumber(1));
					$('#btnSaveDayEnd').html('Thêm');
					$('#btnSaveDayEnd').attr('onclick', 'return ShopParamConfig.addCloseDayEnd(' + type + ');');
				} else {
					if (row != null) {
						var valueFromDate = row.valueFromDateStr;
						var valueToDate = row.valueToDateStr;
						$('#popFromDate').val(valueFromDate);
						$('#popToDate').val(valueToDate);
					}
					$('#btnSaveDayEnd').html('Chỉnh sửa');
					$('#btnSaveDayEnd').attr('onclick', 'return ShopParamConfig.addCloseDayEnd(' + type + ',' + index + ');');
				}
				
			},
    	});
	},

	/**
	 * add close day end
	 * @author vuongmq
	 * @param type
	 * @param index
	 * @since 01/12/2015 
	 */
	addCloseDayEnd: function(type, index) {
		hideAllMessage();
		var msg = '';
		if (msg.length == 0) {
			msg = Utils.getMessageOfRequireCheck('popFromDate', 'Từ ngày');
		}
		if (msg.length == 0) {
			msg = Utils.getMessageOfInvalidFormatDate('popFromDate', 'Từ ngày');
		}
		if (msg.length == 0) {
			msg = Utils.getMessageOfRequireCheck('popToDate', 'Đến ngày');
		}
		if (msg.length == 0) {
			msg = Utils.getMessageOfInvalidFormatDate('popToDate', 'Đến ngày');
		}
		var fDate = $('#popFromDate').val();
		var tDate = $('#popToDate').val();
		var curDate = ReportUtils.getCurrentDateString();
		if (msg.length == 0 && !Utils.compareDate(fDate, tDate)) {
			msg ='Từ ngày không được lớn hơn đến ngày. Vui lòng nhập lại';
			$('#popFromDate').focus();
		}
		if (msg.length > 0) {
			$('#errMsgPopDayEnd').html(msg).show();
			return false;
		}
		if (type == ShopParamConfig._INSERT) {
			if (Utils.compareDate(fDate, curDate)) {
				msg = 'Từ ngày không được nhỏ hơn hoặc bằng ngày hiện tại. Vui lòng nhập lại';
				$('#popFromDate').focus();
			}
			// validate them moi khong trung
			if (msg.length == 0) {
				msg = ShopParamConfig.checkCloseDayEndExistsDay('gridCloseDayEnd', fDate, tDate, type);
				$('#popFromDate').focus();
			}
			if (msg.length > 0) {
				$('#errMsgPopDayEnd').html(msg).show();
				return false;
			}
			var numRow = $('#gridCloseDayEnd').datagrid('getRows').length;
			var newData = new Object();
			newData.valueFromDateStr = fDate;
			newData.valueToDateStr = tDate;
			$('#gridCloseDayEnd').datagrid('insertRow', {index: numRow, row: newData});
		} else {
			if (index != null) {
				var row = $('#gridCloseDayEnd').datagrid('getRows')[index];
				if (Utils.compareDateInt(row.valueFromDateStr, curDate) <= 0 && Utils.compareDateInt(fDate, row.valueFromDateStr) != 0) {
					msg = 'Từ ngày nhỏ hơn hoặc bằng ngày hiện tại không cho đổi từ ngày';
					$('#popFromDate').focus();
				}
				if (Utils.compareDateInt(row.valueFromDateStr, curDate) > 0 && Utils.compareDate(fDate, curDate)) {
					msg = 'Từ ngày không được nhỏ hơn hoặc bằng ngày hiện tại';
					$('#popFromDate').focus();
				}
				if (msg.length == 0 && Utils.compareDate(tDate, curDate)) {
					msg = 'Đến ngày không được nhỏ hơn hoặc bằng ngày hiện tại';
					$('#popFromDate').focus();
				}
				// validate cap nhat khong trung
				if (msg.length == 0) {
					msg = ShopParamConfig.checkCloseDayEndExistsDay('gridCloseDayEnd', fDate, tDate, type, index);
					$('#popFromDate').focus();
				}
				if (msg.length > 0) {
					$('#errMsgPopDayEnd').html(msg).show();
					return false;
				}
				row.valueFromDateStr = fDate;
				row.valueToDateStr = tDate;
				if (row.updateFlag == activeType.RUNNING) {
					row.updateFlag = activeType.WAITING;
				}
				$('#gridCloseDayEnd').datagrid('updateRow', {index: index, row: row});	
			}
		}
		$('#popupCloseDayEnd').dialog('close');
	},

	/**
	 * checkCloseDayEndExistsDay
	 * @author vuongmq
	 * @param grid
	 * @param fromDate
	 * @param toDate
	 * @param type
	 * @param index
	 * @since 03/12/2015 
	 */
	checkCloseDayEndExistsDay: function(grid, fromDate, toDate, type, index) {
		var msg = '';
		var rows = $('#' + grid).datagrid('getRows');
		for (var i = 0, sz = rows.length; i < sz; i++) { 
			if (type == ShopParamConfig._EDIT && index != undefined && index == i) {
				continue;
			}
			var row = rows[i];
			if (Utils.compareDateInt(fromDate, row.valueFromDateStr) == 0 || Utils.compareDateInt(fromDate, row.valueToDateStr) == 0) {
				msg = format('Từ ngày trùng với dòng {0}', (i + 1));
				break;
			}
			if (Utils.compareDateInt(toDate, row.valueFromDateStr) == 0 || Utils.compareDateInt(toDate, row.valueToDateStr) == 0) {
				msg = format('Đến ngày trùng với dòng {0}', (i + 1));
				break;
			}
			if (Utils.compareDateInt(fromDate, row.valueFromDateStr) < 0 && Utils.compareDateInt(toDate, row.valueFromDateStr) > 0) {
				msg = format('Từ ngày và đến ngày trùng với dòng {0}', (i + 1));
				break;
			}
			if (Utils.compareDateInt(fromDate, row.valueToDateStr) < 0 && Utils.compareDateInt(toDate, row.valueToDateStr) > 0) {
				msg = format('Từ ngày và đến ngày trùng với dòng {0}', (i + 1));
				break;
			}
			if (Utils.compareDateInt(fromDate, row.valueFromDateStr) > 0 && Utils.compareDateInt(toDate, row.valueToDateStr) < 0) {
				msg = format('Từ ngày và đến ngày trùng với dòng {0}', (i + 1));
				break;
			}
		}
		return msg;
	},

	/**
	 * delete close day end
	 * @author vuongmq
	 * @param index
	 * @since 02/12/2015 
	 */
	deleteCloseDayEnd: function(index) {
		var row = $('#gridCloseDayEnd').datagrid('getRows')[index];
		if (row != null) {
			if (row.updateFlag == activeType.RUNNING || row.updateFlag == activeType.WAITING) {
				var valueFromDate = row.valueFromDateStr;
				var curDate = ReportUtils.getCurrentDateString();
				if (!isNullOrEmpty(valueFromDate) && Utils.compareDate(valueFromDate, curDate)) {
					var msg = 'Từ ngày và đến ngày xóa đều phải lớn hơn ngày hiện tại.';
					$.messager.alert('Cảnh báo', msg, 'warning');
					return false;
				}
			}
			var message = 'Bạn có muốn xóa dòng tạm dừng chốt tự động này? <br>';
			message += 'Thay đổi được lưu khi nhấn nút Cập nhật.';
			$.messager.confirm(jsp_common_xacnhan, message, function(r) {
				if (r) {
					if (row.updateFlag == activeType.RUNNING || row.updateFlag == activeType.WAITING) {
						if (ShopParamConfig._lstDayEndDelete != null && ShopParamConfig._lstDayEndDelete.length > 0) {
							ShopParamConfig._lstDayEndDelete.push(row.id);
						} else {
							ShopParamConfig._lstDayEndDelete = [];
							ShopParamConfig._lstDayEndDelete.push(row.id);
						}
					}
					ShopParamConfig.deleteRowGrid('gridCloseDayEnd', index);
				}
			});
		}
	},


	/**
	 * Init grid danh sach ProductStockDay
	 * @author vuongmq
	 * @since 27/11/2015 
	 */
	initGridProductStockDay: function() {
		var params = new Object();
		params.shopId = $('#curShopId').val();
		var wd = $(window).width() - 500;
		$('#gridProductStockDay').datagrid({  
		    url: '/catalog/shop-param/getProductShopMap',
		    rownumbers: true,
		    //pagination: true,
		    singleSelect: false,
		    autoRowHeight: true,
		    //pageNumber: 1,
		    //pageSize: 10,
		    //pageList: [10, 20, 50, 100, 500],
		    width: wd,
		    //width: 800,
		    queryParams: params,
		    height: 280,
		    //fitColumns: true,
		    scrollbarSize: 15,
		    columns: [[
				{field: 'productCode', title: 'Mã sản phẩm', width: 200, align: 'left', resizable: false, formatter: function(value, row, index) {
					return VTUtilJS.XSSEncode(value);
				}},
				{field: 'productName', title: 'Tên sản phẩm', width: wd - 200 - 150 - 48, align: 'left', resizable: false, formatter: function(value, row, index) {
		        	return VTUtilJS.XSSEncode(value);
		        }},
		        {field:'stockDay', title: 'Số ngày', width: 150, align: 'center', resizable: false, formatter: function(value, row, index) {
		        	if (isNullOrEmpty(value)) {
		        		value = '';
		        	}
			    	return '<input id="stockDay' + index + '" class="InputShopText stockDayClazz" autocomplete="off" maxlength="3" value="' + value + '">';
			    }}, 
		    ]],
		    onLoadSuccess: function(data) {
		    	$('.datagrid-header-rownumber').html('STT');
			    updateRownumWidthForJqGrid('#closeDayEndResult');
		   	 	$(window).resize(); 
		   	 	$('.stockDayClazz').each(function() {
				    var id = $(this).attr('id');
				    Utils.bindFormatOnTextfield(id, Utils._TF_NUMBER_COMMA);
				    Utils.formatCurrencyFor(id);
				});
		    }		    
		});
	},

};

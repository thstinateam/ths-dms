var PackingCatalog = {
	_xhrSave : null,
	_xhrDel: null,
	getGridUrl: function(packingCode,packingName,note,status){
		return "/catalog/packing/search?code=" + encodeChar(packingCode) + "&name=" + encodeChar(packingName) + "&note=" + encodeChar(note) + "&status=" + status;
	},	
	search: function(){
		$('#errMsg').html('').hide();
		var code = $('#packingCode').val().trim();
		var name = $('#packingName').val().trim();
		var note = $('#note').val().trim();
		var status = 1;
		var tm = setTimeout(function() {
			$('#packingCode').focus();
		}, 500);
		var url = PackingCatalog.getGridUrl(code,name,note,status);
		$("#grid").datagrid({url:url,page:1});
		return false;
	},
	change: function(){
		var msg = '';
		$('#errMsg').html('').hide();
		msg = Utils.getMessageOfRequireCheck('packingCodePop','Mã packing');
		if(msg.length == 0){
			msg = Utils.getMessageOfSpecialCharactersValidate('packingCodePop','Mã packing',Utils._CODE);
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfRequireCheck('packingNamePop','Tên packing');
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfSpecialCharactersValidate('packingNamePop','Tên packing');
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfSpecialCharactersValidate('packingNotePop','Ghi chú');
		}
		if(msg.length > 0){
			$('#errMsg').html(msg).show();
			return false;
		}
		var dataModel = new Object();
		dataModel.id = $('#packingId').val().trim();
		dataModel.code = $('#packingCodePop').val().trim();
		dataModel.name = $('#packingNamePop').val().trim();
		dataModel.note = $('#packingNotePop').val().trim();
		dataModel.status = 1;
		Utils.addOrSaveRowOnGrid(dataModel, "/catalog/packing/save", PackingCatalog._xhrSave, null,null, function(data){
			if(!data.error){
				$('#popup1').dialog('close');
				PackingCatalog.search();
			}
		});		
		return false;
	},
	getSelectedPacking: function(rowId, code, name, note, status){	
		
		if(rowId != null && rowId != 0 && rowId != undefined){
			$('#packingId').val(rowId);
		} else {
			$('#packingId').val(0);
		}
		if(note == 'null'){
			note= '';
		}
		$('#packingCodePop').val(code);
		$('#packingNamePop').val(name);
		$('#packingNotePop').val(note);
		$('#divDialog').css('visibility','visible');
		$('#popup1').dialog('open');
		return false;
	},
	resetForm: function(){
		$('.RequireStyle').hide();
		setTitleSearch();
		$('#packingId').val(0);
		$('#errMsg').html('').hide();
		$('#code').removeAttr('disabled');
		$('#btnUpdate').hide();
		$('#btnDismiss').hide();
		$('#btnCreate').show();
		$('#btnSearch').show();
		$('#code').val('');
		$('#code').focus();
		$('#name').val('');
		$('#note').val('');
		setSelectBoxValue('status', 1);		
		PackingCatalog.search();
		return false;
	},
	deletePacking: function(packingCode){
		$('#errMsg').html('').hide();
		var dataModel = new Object();
		dataModel.code = packingCode;	
		Utils.deleteSelectRowOnGrid(dataModel, 'packing', '/catalog/packing/remove', PackingCatalog._xhrDel, null, null,function(data){
			PackingCatalog.resetForm();
		});
		return false;
	}
};
var AttributeCustomerManager = {
	loadlandau:true,
	loadlandau2:true,
	search: function(){
		$('#errMsg').html('').hide();
		var attributeCode = $('#attributeCode').val().trim();
		var attributeName = $('#attributeName').val().trim();
//		var valueType = $('#valueType').val().trim();
		var lstAttributeCM= '';
		$('#ddcl-valueType-ddw input[type=checkbox]').each(function(){
			if($(this).is(':checked')){
				if($(this).val() == -1){
					lstAttributeCM += $(this).val() + ',';
					return false;
				}else{
					lstAttributeCM += $(this).val() + ',';
				}
			}
		});
		var lstObject = '';
		$('#ddcl-object-ddw input[type=checkbox]').each(function(){
			if($(this).is(':checked')){
				if ($(this).val() == -1) {
					lstObject += $(this).val() + ',';
					return false;
				} else {
					lstObject += $(this).val() + ',';
				}
			}
		});
		var status = $('#status').val().trim();		
		$('#attributeDetailDataDiv').hide();
		var params = new Object();
		params.attributeCode = attributeCode;
		params.attributeName = attributeName;
		params.lstAttributeCM = lstAttributeCM;
		params.lstObject = lstObject;
		params.status = status;
		$('#attributeDatagrid').datagrid('load',params);
			return false;
	},	
	
	saveAttribute : function() {
		var msg = '';
		var valueTypePop = $('#valueTypePop').val().trim();
		$('#errMsg').html('').hide();
		if (msg.length == 0) {
			msg = Utils.getMessageOfRequireCheck('attributeCodePop', 'Mã thuộc tính');
		}
		if(msg.length ==0){
			msg = Utils.getMessageOfSpecialCharactersValidate('attributeCodePop', 'Mã thuộc tính',Utils._CODE);
		}
		if (msg.length == 0){
			msg = Utils.getMessageOfRequireCheck('attributeNamePop', 'Tên thuộc tính');
		}
		if(msg.length ==0){
			msg = Utils.getMessageOfSpecialCharactersValidate('attributeNamePop', 'Tên thuộc tính');
		}
		if (msg.length == 0) {
			msg = Utils.getMessageOfRequireCheck('valueTypePop', 'Loại giá trị');
		}
		if (msg.length == 0) {
			msg = Utils.getMessageOfRequireCheck('statusPop', 'Trạng thái');
		}
		if(msg.length == 0 && valueTypePop == -1){
			msg = 'Bạn chưa chọn Loại giá trị';
		}
		if (msg.length > 0) {
			$('#errMsgPop').html(msg).show();
			return false;
		}
		var id = $('#id').val().trim();
		var attributeCodePop = $('#attributeCodePop').val().trim();
		var attributeNamePop = $('#attributeNamePop').val().trim();
		var valueTypePop = $('#valueTypePop').val().trim();
		var statusPop = $('#statusPop').val().trim();
		var notePop = $('#notePop').val().trim();
				
		var dataModel = new Object();
		dataModel.id = id; //$('#id').val().trim();
		dataModel.attributeCodePop = attributeCodePop;
		dataModel.attributeNamePop = attributeNamePop;
		dataModel.valueTypePop = valueTypePop;
		dataModel.statusPop = statusPop;
		dataModel.notePop = notePop;
		Utils.addOrSaveData(dataModel, "/attribute-customer-manager/save-attribute",null, 'errMsgPop', function(data){
			
				var tm = setTimeout(function(){
					$('.easyui-dialog').dialog('close'); 
					$("#attributeDatagrid").datagrid("reload");
					clearTimeout(tm);
				}, 2000);
			
		},null,'#popupAttribute',null,null,function(data){
			if(data.error != undefined  && data.error && data.errMsg != undefined){
				$('#errMsgPop').html(data.errMsg).show();
			}
		});
		return false;
	},
	
	loadDetail : function(attributeId) {
		$('#attributeDetailDataDiv').show();
		var params = new Object();
		$('#attributeId').val(attributeId);
		params.attributeId = attributeId;
		$('#attributeDetailDatagrid').datagrid({
			url: '/attribute-customer-manager/load-attribute-detail',
			autoRowHeight : true,
			rownumbers : true, 
			checkOnSelect :true,
			pagination:true,
			rowNum : 10,
			fitColumns:true,
			pageList  : [10,20,30],
			scrollbarSize:0,
			width: $('#attributeDetailDataGridSection2').width(),
			queryParams:params,
		    columns:[[	        
			    {field:'code', title: 'Mã GT', width: 100, sortable:false,resizable:false , align: 'left', formatter: function(value, row, index){
			    	return Utils.XSSEncode(value);
			    }},
			    {field:'name', title: 'Tên GT', width:150,sortable:false,resizable:false , align: 'left', formatter: function(value, row, index){
			    	return Utils.XSSEncode(value);
			    }},
			    {field:'statusDetail', title: 'Trạng thái', width: 60, align: 'left',sortable:false,resizable:false,formatter:AttributeCustemerCatalogFormatter.statusFormat},
			    {field:'edit', title:'<a href= "javascript:void(0);" id="btnGridDetailAdd" onclick= "AttributeCustomerManager.openAttributeDetailCustomer();"><img src="/resources/images/icon_add.png" /></a>',
			    	width: 15, align: 'center',sortable:false,resizable:false, formatter: AttributeCustemerCatalogFormatter.editCellDetailIconFormatter},
		    ]],
		    onLoadSuccess :function(data){	    	
		    	$('.datagrid-header-rownumber').html('STT');	    	
		    	 updateRownumWidthForJqGrid('.easyui-dialog');
		    	 CommonFormatter.checkPermissionHideGridButton("btnGridDetailAdd", false, "attributeDetailDataGridSection2");
		    	 CommonFormatter.checkPermissionHideGridButton("btnGridDetailEdit", true, "attributeDetailDataGridSection2");
		    	 $(window).resize();    		 
		    }
		});
		Utils.bindAutoSearchForEasyUI();
	},
		
		
	openAttributeCustomer : function(index) {
		$('#popupAttribute #successMsg').html('').hide();
		$('#popupAttributeDetail #successMsg').html('').hide();
		$('#attributeDetailDataDiv').hide();
		var rows = null;
		if(index!=undefined && index!=null && index>=0){
			rows = $('#attributeDatagrid').datagrid('getRows')[index];
			console.log(rows);
		}
		$('#popupAttribute').dialog({
			closed : false,
			cache : false,
			modal : true,
			width : 550,
			height : 250,
			onOpen: function(){	
				if(AttributeCustomerManager.loadlandau){
					Utils.bindAutoButtonEx('#popupAttribute','btnSaveAttr');
					AttributeCustomerManager.loadlandau = false;
				}
				var tabindex = -1;
				$('.InputTextStyle, select, input, button , li , tr, td, th, label, ul, a, img').each(function () {
					if (this.type != 'hidden') {
						$(this).attr("tabindex", tabindex);
						tabindex -=1;
					}
				});
				tabindex = 1;
	    		 $('.easyui-dialog input,.easyui-dialog select,.easyui-dialog button').each(function () {
		    		 if (this.type != 'hidden') {
			    	     $(this).attr("tabindex", '');
						 tabindex++;
		    		 }
				 });
				if(rows!=null){
					disabled('attributeCodePop');
					disabled('valueTypePop');
					$('#valueTypePopDiv').addClass('BoxDisSelect');
					$('#id').val(rows.attributeId);
					setTextboxValue('attributeCodePop',rows.attributeCode);
					setTextboxValue('attributeNamePop',rows.attributeName);
					setSelectBoxValue('valueTypePop',rows.valueType);
					setSelectBoxValue('statusPop',rows.status);
					setTextboxValue('notePop',rows.note);
					var tm = setTimeout(function(){
						$('#attributeNamePop').focus();
						clearTimeout(tm);
					}, 2000);
				}else{
					$('#id').val(0);
					var tm = setTimeout(function(){
						$('#attributeCodePop').focus();
						clearTimeout(tm);
					}, 2000);
				}
			},
		 	onBeforeClose: function() {
				var tabindex = 1;
				$(' .InputTextStyle, select, input, button , li , tr, td, th, label, ul, a, img').each(function () {
					if (this.type != 'hidden') {
						$(this).attr("tabindex", '');
					}
					tabindex ++;
				});	
				var curIdFocus = $('#cur_focus').val();
				$('#'+ curIdFocus).focus();				
	        },
	        onClose : function(){
	        	$('.easyui-dialog #attributeCodePop').val('');
	        	$('.easyui-dialog #attributeNamePop').val('');
	        	$('.easyui-dialog #valueTypePop').val(-1);
	        	$('.easyui-dialog #valueTypePop').change();
	        	$('.easyui-dialog #statusPop').val(1);
	        	$('.easyui-dialog #statusPop').change();
	        	$('.easyui-dialog #notePop').val('');
	        	$('.easyui-dialog #errMsgPop').hide();
	        	enable('attributeCodePop');
	        	enable('valueTypePop');
				$('#valueTypePopDiv').removeClass('BoxDisSelect');
	        }
		});
	},
	saveAttributeDetail : function() {
		var msg = '';
		$('#errMsgDetailPop').html('').hide();
		if (msg.length == 0) {
			msg = Utils.getMessageOfRequireCheck('attributeDetailCodePop', 'Mã GT');
		}
		if(msg.length ==0){
			msg = Utils.getMessageOfSpecialCharactersValidate('attributeDetailCodePop', 'Mã GT',Utils._CODE);
		}
		if (msg.length == 0){
			msg = Utils.getMessageOfRequireCheck('attributeDetailNamePop', 'Tên GT');
		}
		if(msg.length ==0){
			msg = Utils.getMessageOfSpecialCharactersValidate('attributeDetailNamePop', 'Tên GT');
		}
		if (msg.length == 0) {
			msg = Utils.getMessageOfRequireCheck('statusDetailPop', 'Trạng thái');
		}
		if (msg.length > 0) {
			$('#errMsgDetailPop').html(msg).show();
			return false;
		}
		var idDetail = $('#idDetail').val().trim();
		var attributeId = $('#attributeId').val().trim();
		var attributeDetailCodePop = $('#attributeDetailCodePop').val().trim();
		var attributeDetailNamePop = $('#attributeDetailNamePop').val().trim();
		var statusDetailPop = $('#statusDetailPop').val().trim();
		var dataModel = new Object();
		dataModel.idDetail = idDetail; //$('#id').val().trim();
		dataModel.attributeId = attributeId;
		dataModel.attributeDetailCodePop = attributeDetailCodePop;
		dataModel.attributeDetailNamePop = attributeDetailNamePop;
		dataModel.statusDetailPop = statusDetailPop;
		Utils.addOrSaveData(dataModel, "/attribute-customer-manager/save-attribute-detail",null, 'errMsgDetailPop', function(data){
			if(data.error != undefined  && data.error && data.errMsg != undefined){
				$('#errMsgDetailPop').html(data.errMsg).show();
			}else{
				var tm = setTimeout(function(){
					$('.easyui-dialog').dialog('close'); 
					$("#attributeDetailDatagrid").datagrid("reload");					
					clearTimeout(tm);
				}, 2000);				
			}			
		},null,'#popupAttributeDetail');
		return false;
	},
	openAttributeDetailCustomer : function(index) {
		$('#popupAttribute #successMsg').html('').hide();
		$('#popupAttributeDetail #successMsg').html('').hide();
		$('#errMsgDetailPop').html('').hide();
		var rows = null;
		if(index!=undefined && index!=null && index>=0){
			rows = $('#attributeDetailDatagrid').datagrid('getRows')[index];
		}
		$('#popupAttributeDetail').dialog({
			closed : false,
			cache : false,
			modal : true,
			width : 550,
			height : 230,
			onOpen: function(){	
				if(AttributeCustomerManager.loadlandau2){
					Utils.bindAutoButtonEx('#popupAttributeDetail','btnSaveAttrDetail');
					AttributeCustomerManager.loadlandau2 = false;
				}
				var tabindex = -1;
				$('.InputTextStyle, select, input, button , li , tr, td, th, label, ul, a, img').each(function () {
					if (this.type != 'hidden') {
						$(this).attr("tabindex", tabindex);
						tabindex -=1;
					}
				});
				tabindex = 1;
	    		 $('.easyui-dialog input,.easyui-dialog select,.easyui-dialog button').each(function () {
		    		 if (this.type != 'hidden') {
			    	     $(this).attr("tabindex", '');
						 tabindex++;
		    		 }
				 });
				if(rows!=null){
					disabled('attributeDetailCodePop');
					$('#idDetail').val(rows.id);
					setTextboxValue('attributeDetailCodePop',rows.code);
					setTextboxValue('attributeDetailNamePop',rows.name);
					setSelectBoxValue('statusDetailPop',rows.status);
					var tm = setTimeout(function(){
						$('#attributeDetailNamePop').focus();
						clearTimeout(tm);
					}, 2000);
				}else{
					$('#idDetail').val(0);
					var tm = setTimeout(function(){
						$('#attributeDetailCodePop').focus();
						clearTimeout(tm);
					}, 2000);
				}
//				$('#saveAttributeDetail').bind('click',function(event) {
//					if(!$(this).is(':hidden')){
//						AttributeCustomerManager.saveAttributeDetail();
//					}
//				});
			},
		 	onBeforeClose: function() {
				var tabindex = 1;
				$(' .InputTextStyle, select, input, button , li , tr, td, th, label, ul, a, img').each(function () {
					if (this.type != 'hidden') {
						$(this).attr("tabindex", '');
					}
					tabindex ++;
				});	
				var curIdFocus = $('#cur_focus').val();
				$('#'+ curIdFocus).focus();				
	        },
	        onClose : function(){
	        	$('.easyui-dialog #attributeDetailCodePop').val('');
	        	$('.easyui-dialog #attributeDetailNamePop').val('');
	        	$('.easyui-dialog #statusDetailPop').val(1);
	        	$('.easyui-dialog #statusDetailPop').change();
	        	enable('attributeDetailCodePop');
	        }
		});
	}
};
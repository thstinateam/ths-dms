var SalesDayCatalog ={
	_xhrSave : null,
	_xhrDel: null,
	getGridUrl: function(fromYear,toYear,status){
		return "/catalog/sales-day/search?fromYear=" + encodeChar(fromYear) + "&toYear=" + encodeChar(toYear) + "&status=" + status;
	},
	search: function(){
		$('#errMsg').html('').hide();
		var fromYear = $('#fromYear').val().trim();
		var toYear = $('#toYear').val().trim();
		if(fromYear != '' && toYear != ''){
			if(parseInt(fromYear) > parseInt(toYear)){
				$('#errMsg').html('Thời gian tìm kiếm không hợp lệ').show();
				return false;
			}
		}
		var status = $('#status').val();
		var url = SalesDayCatalog.getGridUrl(fromYear,toYear,status);
		$("#grid").setGridParam({url:url,page:1}).trigger("reloadGrid");
		return false;
	},
	change: function(){
		var msg = '';
		$('#errMsg').html('').hide();
		var error = false;
		var tmp = '';
		msg = Utils.getMessageOfRequireCheck('year','Năm');
		if(msg.length == 0){
			if($('#year').val().trim() != ''){
				if($('#january').val().trim() != '' && parseInt($('#january').val().trim()) > SalesDayCatalog.daysInMonth(0,$('#year').val().trim())){
					tmp = 'tháng 1';
					$('#january').focus();
					error = true;
				}
				if($('#february').val().trim() != '' && parseInt($('#february').val().trim()) > SalesDayCatalog.daysInMonth(1,$('#year').val().trim())){
					tmp = 'tháng 2';
					$('#february').focus();
					error = true;
				}
				if($('#march').val().trim() != '' && parseInt($('#march').val().trim()) > SalesDayCatalog.daysInMonth(2,$('#year').val().trim())){
					tmp = 'tháng 3';
					$('#march').focus();
					error = true;
				}
				if($('#april').val().trim() != '' && parseInt($('#april').val().trim()) > SalesDayCatalog.daysInMonth(3,$('#year').val().trim())){
					tmp = 'tháng 4';
					$('#april').focus();
					error = true;
				}
				if($('#may').val().trim() != '' && parseInt($('#may').val().trim()) > SalesDayCatalog.daysInMonth(4,$('#year').val().trim())){
					tmp = 'tháng 5';
					$('#may').focus();
					error = true;
				}
				if($('#june').val().trim() != '' && parseInt($('#june').val().trim()) > SalesDayCatalog.daysInMonth(5,$('#year').val().trim())){
					tmp = 'tháng 6';
					$('#june').focus();
					error = true;
				}
				if($('#july').val().trim() != '' && parseInt($('#july').val().trim()) > SalesDayCatalog.daysInMonth(6,$('#year').val().trim())){
					tmp = 'tháng 7';
					$('#july').focus();
					error = true;
				}
				if($('#august').val().trim() != '' && parseInt($('#august').val().trim()) > SalesDayCatalog.daysInMonth(7,$('#year').val().trim())){
					tmp = 'tháng 8';
					$('#august').focus();
					error = true;
				}
				if($('#september').val().trim() != '' && parseInt($('#september').val().trim()) > SalesDayCatalog.daysInMonth(8,$('#year').val().trim())){
					tmp = 'tháng 9';
					$('#september').focus();
					error = true;
				}
				if($('#october').val().trim() != '' && parseInt($('#october').val().trim()) > SalesDayCatalog.daysInMonth(9,$('#year').val().trim())){
					tmp = 'tháng 10';
					$('#october').focus();
					error = true;
				}
				if($('#november').val().trim() != '' && parseInt($('#november').val().trim()) > SalesDayCatalog.daysInMonth(10,$('#year').val().trim())){
					tmp = 'tháng 11';
					$('#november').focus();
					error = true;
				}
				if($('#december').val().trim() != '' && parseInt($('#december').val().trim()) > SalesDayCatalog.daysInMonth(11,$('#year').val().trim())){
					tmp = 'tháng 12';
					$('#december').focus();
					error = true;
				}
			}
			if(error){
				msg = 'Số ngày trong ' + Utils.XSSEncode(tmp) + ' không hợp lệ';
			}
		}
		if(msg.length > 0){
			$('#errMsg').html(msg).show();
			return false;
		}
		var dataModel = new Object();
		dataModel.salesDayId = $('#salesDayId').val();
		dataModel.year = $('#year').val();
		dataModel.january = $('#january').val().trim();
		dataModel.february = $('#february').val().trim();
		dataModel.march = $('#march').val().trim();
		dataModel.april = $('#april').val().trim();
		dataModel.may = $('#may').val().trim();
		dataModel.june = $('#june').val().trim();
		dataModel.july = $('#july').val().trim();
		dataModel.august = $('#august').val().trim();
		dataModel.september = $('#september').val().trim();
		dataModel.october = $('#october').val().trim();
		dataModel.november = $('#november').val().trim();
		dataModel.december = $('#december').val().trim();
		dataModel.status = $('#status').val();
		Utils.addOrSaveData(dataModel, "/catalog/sales-day/save", SalesDayCatalog._xhrSave, null,function(data){
			if(data.error == false){
				window.location.href = '/catalog/working-date-in-year';
			}
		});
		return false;
	},
	deleteRow: function(salesDayId){
		$('#errMsg').html('').hide();
		var dataModel = new Object();
		dataModel.salesDayId = salesDayId;	
		Utils.deleteSelectRowOnGrid(dataModel, 'Sản phẩm', "/catalog/sales-day/remove", SalesDayCatalog._xhrDel, null, null);		
		return false;		
	},
	daysInMonth:function (m, y) { // m is 0 indexed: 0-11
	    switch (m) {
	        case 1 :
	            return (y % 4 == 0 && y % 100) || y % 400 == 0 ? 29 : 28;
	        case 8 : case 3 : case 5 : case 10 :
	            return 30;
	        default :
	            return 31;
	    }
	}

};
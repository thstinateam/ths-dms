var UnitTreeCatalog = {
	_xhrSave : null,
	_listNodeTree:null,
	_lstStaffAdd:null,
	_lstStaffDelete:null,
	_typeScreen: 0,
	_zoom : 18,
	_nodeSelect:null,
	_callBack:null,
	_idSearch:null,
	_idSearchStaff:0,
	_idShopAddNewStaff:null,
	_isChange:false,
	_isLoadArea:true,
	_isBkLatLng :false,
	latbk : '',
	lngbk : '',
	_lstContextMenu: null,
	_contextMenuHTML : null,
	_MAX_LENGTH_6: 6,
	//Tungmt-------------------------------------------------------------------------------------------------
	getNodeFromListNode:function(node){
		if(node==null || node.attributes==null) return null;
		if(node.attributes.shop!=null){//shop
			return UnitTreeCatalog._listNodeTree.get("shop" +node.attributes.shop.id);
		}else if(node.attributes.staff!=null){//staff
			return UnitTreeCatalog._listNodeTree.get("staff" +node.attributes.staff.id);
		}else return null;
	},
	putMapListNodeTree:function(parent,data){
		for(var i=0;i<data.length;i++){
			var node=data[i];
			if(parent!=null && parent.attributes!=null)
				node.attributes.parent=parent.attributes;
			else node.attributes.parent=null;
			var nodeTemp = jQuery.extend(true, {}, node);
			nodeTemp.__proto__=null;
			if(nodeTemp.attributes!=null){
				if(nodeTemp.attributes.shop!=null){//shop
					UnitTreeCatalog._listNodeTree.put("shop" +nodeTemp.attributes.shop.id,nodeTemp);
				}else{//staff
					UnitTreeCatalog._listNodeTree.put("staff" +nodeTemp.attributes.staff.id,nodeTemp);
				}
			}
			if(nodeTemp.children!=null && nodeTemp.children.length>0){// nếu có con thì đệ quy put tiếp
				UnitTreeCatalog.putMapListNodeTree(nodeTemp,nodeTemp.children);
			}
		}
	},
	cbEditChange:function(){
		if($('#cbEdit').is(':checked')){
			$('#tree').tree('enableDnd');
		}else{
			$('#tree').tree('disableDnd');
			if (UnitTreeCatalog._typeScreen < 0) {
				UnitTreeCatalog._typeScreen = 0;
				UnitTreeCatalog.closeCollapseTab();
			}
		}
	},
	cbStopChange:function(){
		if($('#cbStop').is(':checked')){
			for(var i=0;i<UnitTreeCatalog._listNodeTree.keyArray.length;i++){
				var temp = UnitTreeCatalog._listNodeTree.get(UnitTreeCatalog._listNodeTree.keyArray[i]);
				if(temp!=null && temp.attributes!=null && temp.attributes.shop!=null && temp.attributes.shop.status!='RUNNING'){
					$($("#tree").tree('find',temp.attributes.shop.id).target).show();
				}
				if(temp!=null && temp.attributes!=null && temp.attributes.staff!=null && temp.attributes.staff.status!='RUNNING'){
					$($("#tree").tree('find','s' +temp.attributes.staff.id).target).show();
				}
			}
		}else{
			for(var i=0;i<UnitTreeCatalog._listNodeTree.keyArray.length;i++){
				var temp = UnitTreeCatalog._listNodeTree.get(UnitTreeCatalog._listNodeTree.keyArray[i]);
				if(temp!=null && temp.attributes!=null && temp.attributes.shop!=null && temp.attributes.shop.status!='RUNNING'){
					$($("#tree").tree('find',temp.attributes.shop.id).target).hide();
				}
				if(temp!=null && temp.attributes!=null && temp.attributes.staff!=null && temp.attributes.staff.status!='RUNNING'){
					$($("#tree").tree('find','s' +temp.attributes.staff.id).target).hide();
				}
			}
		}
	},
	hideAllContextMenu:function(){
		$('#cmChuyenDonVi').hide();
		$('#cmTamNgungNhanVien').hide();
	},
	/**
	 * push html context menu
	 * @author liemtpt
	 * @params list context menu: danh sach context menu theo role la SHOP hay STAFF 
	 * @params type: SHOP:1 , STAFF:2
	 * @description Tao context menu cho loai don vi 
	 * @createDate 26/01/2015
	 **/
	pushHtmlContextMenu:function(lstContextMenu,type){
		var html = new Array();
		html.push('<div class="menu-line" style="height: 100%;"></div>');
		/***type với 1:SHOP, 2:STAFF ***/
		if(type == 1){
			html.push('<span style="padding-left:25px;font-size: 13px;font-weight: bold;display:block;height:25px;margin-left: 5px">' + Utils.XSSEncode(catalog_unit_tao_don_vi_con) + '</span>');
		}else if(type == 2){
			html.push('<div class="menu-sep" id="cmLine" style="display: block;"></div>');
			html.push('<span style="padding-left:25px;font-size: 13px;font-weight: bold;display:block;height:25px; margin-left: 5px">' + Utils.XSSEncode(catalog_unit_tao_chuc_vu) + '</span>');
		}
		for(var i = 0,size = lstContextMenu.length  ; i < size ; i++){					
			html.push('<div class="menu-item" style="font-size: 12px;display:block;height:20px" '
					+ 'id="cm' +lstContextMenu[i].typeId + '"'  
					+ 'onclick="UnitTreeCatalog.addNewDataMenu(' +lstContextMenu[i].nodeType+',' +lstContextMenu[i].typeId+',' +lstContextMenu[i].id+');"> '							
					+ '<span style="padding-left:3px;"><img src="' +imgServerPath+lstContextMenu[i].iconUrl+'" width="15px" height="15px">' 
					+ '<span style="padding-left:25px;">' + Utils.XSSEncode(lstContextMenu[i].name) + '</span> '
					+ '</div>');
		}
		return html;
	},
	/**
	 * show context menu
	 * @author liemtpt
	 * @params type: SHOP:1 , STAFF:2
	 * @description Hien thi context menu len cay 
	 * @createDate 26/01/2015
	 **/
	showContextMenu:function(type){
		$('#group_tree_dbl_right_contextMenu').html('');
		var node = $('#tree').tree('getSelected');
		/***type với 1:đơn vị, 2:nhân viên***/
		if(type == nodeType.SHOP){//shop
			node=UnitTreeCatalog.getNodeFromListNode(node);
			// output: id(organization_id),code, name, nodeType, typeId,iconUrl
			var lstContextMenu = UnitTreeCatalog._lstContextMenu;
			if(lstContextMenu != null){
				var lstRoleStaff = new Array();
				var lstRoleShop = new Array();
				var htmlShop = new Array();
				var htmlStaff = new Array();
				for(var i = 0,size = lstContextMenu.length  ; i < size ; i++){					
					if(lstContextMenu[i].nodeType == nodeType.SHOP){
						lstRoleShop.push(lstContextMenu[i]);
					}else if(lstContextMenu[i].nodeType == nodeType.STAFF){
						lstRoleStaff.push(lstContextMenu[i]);
					}
				}
				if(lstRoleShop != null && lstRoleShop.length > 0){
					htmlShop = UnitTreeCatalog.pushHtmlContextMenu(lstRoleShop,nodeType.SHOP);
				}
				if(lstRoleStaff != null && lstRoleStaff.length > 0){
					htmlStaff = UnitTreeCatalog.pushHtmlContextMenu(lstRoleStaff,nodeType.STAFF);
				}
				
			}
			$('#group_tree_dbl_right_contextMenu').html(htmlShop.join("") + htmlStaff.join(""));			
			$(".menu-item").hover(function () {
				$(this).toggleClass("menu-active");
			 });
		}else if(type == nodeType.STAFF){//staff
			var html = new Array();
			html.push('<div class="menu-line" style="height: 100%;"></div>');
			html.push('<div id="cmChuyenDonVi" class="menu-item" style="font-size: 12px;display:block;height:20px" onclick="UnitTreeCatalog.openPoppupMovedStaff();"><span style="padding-left:3px;"><img src="'
					+imgServerPath+'/iconOrganization/male-user-remove.png" width="15px" height="15px"><span style="padding-left:20px;">' + Utils.XSSEncode(catalog_unit_tree_chuyen_don_vi) + '</div>');
			html.push('<div id="cmTamNgungNhanVien" class="menu-item" style="font-size: 12px;display:block;height:20px" onclick="UnitTreeCatalog.suspendedStaff();"><span style="padding-left:3px;"><img src="'
					+imgServerPath+'/iconOrganization/home-go.png" width="15px" height="15px"><span style="padding-left:20px;">' + Utils.XSSEncode(catalog_unit_tree_tam_ngung_doi_tuong_nay) + '</div>');
			$('#group_tree_dbl_right_contextMenu').html(html.join(""));
			$(".menu-item").hover(function () {
				$(this).toggleClass("menu-active");
			 });
		} else {
			return false;
		}
		Utils.functionAccessFillControl ('idDivTreeShop', function() {
			//Xu ly cac su kien lien quan den control phan quyen 
		});
		return true;
	},
	addNewDataMenu: function(nodeType,nodeTypeId,orgId){
		// xu ly them tren cay
		//alert('shop haha');
		var node = $('#tree').tree('getSelected'); // luong luon la don vi
		if(nodeType == 1){
			// them shop tren cay phuocDH2
			//UnitTreeCatalog.showCollapseTab(2,node.attributes.shop.id,node.attributes.shop.parentShop);
			UnitTreeCatalog.openCollapseTab();
			if(node.attributes.shop != null && node.attributes.shop!= undefined){
				UnitTreeCatalog.showShopInfoScreen(node.attributes.shop.id,nodeTypeId,nodeTypeId,orgId);
			} else {
				UnitTreeCatalog.showShopInfoScreen(node.attributes.shop,nodeTypeId,nodeTypeId,orgId);
			}
		} else if(nodeType == 2){
			// them staff tren cay VUONGMQ
			var shopId = '';
			var nameShop = '';
			if(node != null){
				shopId = node.id;
				nameShop = Utils.XSSEncode(node.attributes.shop.shopName);
			}
			UnitTreeCatalog.openCollapseTab();
			UnitTreeCatalog.showStaffDetailScreen(nodeType,nodeTypeId, orgId, nameShop, shopId);
		}
		$('#group_tree_dbl_right_contextMenu').hide();
	},
	/**
	 * filter context menu
	 * @author liemtpt
	 * @params node voi nodeType la SHOP:1 hay STAFF:2 
	 * @description hien thi context menu tuong ung voi tung loai node 
	 * @createDate 26/01/2015
	 **/
	filterContextMenu:function(node){ // show context menu tuong ung voi tung loai node
		$('#group_tree_dbl_right_contextMenu').html('');
		if(node.attributes==null){
			return false;
		}
		UnitTreeCatalog.hideAllContextMenu();
		if(node.attributes.shop!=null){//shop
			if(node.attributes.shop.organizationId == null){
				return false;
			}
			var params = new Object();
			params.orgId = node.attributes.shop.organizationId;
			Utils.getJSONDataByAjaxNotOverlay(params, "/catalog/unit-tree/get-list-context-menu-by-org-id", function(data) {
				if(data.listMenuContext!=null){
					UnitTreeCatalog._lstContextMenu = data.listMenuContext;
					UnitTreeCatalog.showContextMenu(nodeType.SHOP);
				}
				Utils.functionAccessFillControl ('idDivTreeShop', function() {
					//Xu ly cac su kien lien quan den control phan quyen 
				});
			});
			return true;
		}else if(node.attributes.staff!=null){//staff
			return UnitTreeCatalog.showContextMenu(nodeType.STAFF);
		}else{
			return false;
		}
	},
	/**
	 * suspended staff
	 * @author liemtpt
	 * @description tam ngung nhan vien 
	 * @createDate 02/03/2015
	 **/
	suspendedStaff:function(){
		var node = $('#tree').tree('getSelected');
		var params = new Object();
		var staffId = node.attributes.staff.id;
		params.id = staffId;
		//Gui yeu cau len server de luu du lieu
		var url = "/catalog/unit-tree/suspended-staff";
		params[JSONUtil._VAR_NAME] = 'staffTreeVO';
		var dataModel = JSONUtil.getSimpleObject(params);
		Utils.addOrSaveData(dataModel,url,null, 'errMsg', function(data){
			var tm = setTimeout(function(){
				$('.easyui-dialog').dialog('close'); 
				//load lai cay
				UnitTreeCatalog.reloadUnitTree();
				clearTimeout(tm);
			}, 1500);
		},null,'#treeDiv',null,null,function(data){
			if(data.error != undefined  && data.error && data.errMsg != undefined){
				$('#errMsg').html(data.errMsg).show();
				setTimeout(function(){$('#errMsg').hide();},4000);
			}
		});
		$('#group_tree_dbl_right_contextMenu').hide();
		return false;
	},
	/**
	 * open attribute detail dynamic
	 * @author liemtpt
	 * @description: mo popup chuyen don vi
	 * @createDate 22/01/2015
	 */
	openPoppupMovedStaff : function() {
		$('#popupMovedStaff #successMsgPop').html('').hide();
		$('#popupMovedStaff #errMsg').html('').show();
		$('#popupMovedStaff').dialog({
			closed : false,
			cache : false,
			modal : true,
			width : 450,
			height : 210,
			onOpen: function(){	
				
			},
			onBeforeClose: function() {
								
			},
			onClose : function(){
				
			}
		});
		$('#group_tree_dbl_right_contextMenu').hide();
	},
	/**
	 * moved staff 
	 * @author liemtpt
	 * @description Chuyen don vị: chuyen nhan vien tu don vi nay sang don vi khac
	 * @createDate 02/03/2015
	 **/
	movedStaff:function(){
		var msg = '';
		var node = $('#tree').tree('getSelected');
		var choseShopId = $('#idShopCodeChose').combotree('getValue');
		var curStaff = node.attributes.staff;
		if(curStaff != null && curStaff != undefined){
			if(curStaff.shop != null && curStaff.shop.id >0){
				//kiem tra don vi dich co la don vi hien tai dang chon khong
				if(curStaff.shop.id == choseShopId){
					if (msg.length == 0) {
						msg = catalog_unit_tree_change_unit_don_vi_dich_la_don_vi_hien_tai;
					}
				}
				if (msg.length > 0) {
					$('#popupMovedStaff #errMsg').html(msg).show();
					return false;
				}
				var params = new Object();
				params.shopId = choseShopId;
				params.staffId = curStaff.id;
				Utils.getJSONDataByAjaxNotOverlay(params, "/catalog/unit-tree/check-chose-shop-exits-staff-type-id", function(data) {
					if(data.listExits != null && data.listExits.length > 0){
						params.shopId = choseShopId;
						var url = "/catalog/unit-tree/moved-staff";
						Utils.addOrSaveData(params,url,null, 'errMsg', function(data){
							$('#popupMovedStaff #successMsgPop').html(catalog_unit_tree_change_unit_phan_quyen).show();
							var tm = setTimeout(function(){
								$('.easyui-dialog').dialog('close');
								//load lai cay
								UnitTreeCatalog.reloadUnitTree();
								clearTimeout(tm);
							}, 3000);
							
						},null,'#popupMovedStaff',null,null,function(data){
							if(data.error != undefined  && data.error && data.errMsg != undefined){
								$('#popupMovedStaff #errMsg').html(data.errMsg).show();
							}
						});
					}else{
						if (msg.length == 0) {
							msg = catalog_unit_tree_chang_unit_don_vi_dich_khong_cho_tao;
						}
						if (msg.length > 0) {
							$('#popupMovedStaff #errMsg').html(msg).show();
							return false;
						}
					}
				});
				
			}
		}
		return false;
	},
	checkDropEnter:function(node,target){//kiem tra có cho thả xuống hay ko
		if(node.attributes==undefined || node.attributes==null || target.attributes==undefined || target.attributes==null){
			return false;
		}
		if(target.attributes.staff!=null){// chỉ cho thả vào shop
			return false;
		}
		if(node.attributes.staff.shop.id==target.attributes.shop.id)//nếu cùng shopid với shop muốn thả thì ko cho
			return false;
		if(node.attributes.staff.staffType.objectType==StaffRoleType.NVGS){//gsnpp
			if(target.attributes.shop.type.objectType!=3)//shop  ko phải npp thì ko cho
				return false;
		}else if(node.attributes.staff.staffType.objectType==StaffRoleType.TBHM){//gdm
			if(target.attributes.shop.type.objectType!=1)//gdm chỉ dc chuyển qua shop có shop là mien
				return false;
		}else if(node.attributes.staff.staffType.objectType==StaffRoleType.TBHV){//tbhv
			if(target.attributes.shop.type.objectType!=2)//tbhv chỉ dc chuyển qua nhóm có shop là vùng
				return false;
		}else if(node.attributes.staff.staffType.objectType==8){//vnm
			return false;
		}else{//nvbh
			if(target.attributes.shop.type.objectType!=3)//nvbh chỉ dc chuyển qua nhóm có shop là npp
				return false;
		}
		return true;
	},
	fillMapStaffForDND:function(target,nodeSource){
		var source = new Object();
		var newNode = new Object();
		source = jQuery.extend(true, {}, nodeSource);
		source.attributes.staff.shop=jQuery.extend(true, {}, target.attributes.shop);
		newNode = jQuery.extend(true, {}, source);
		newNode.attributes.parent=jQuery.extend(true, {}, target.attributes);
		UnitTreeCatalog._lstStaffAdd.put("staff" +source.attributes.staff.id,source);
		UnitTreeCatalog._listNodeTree.put("staff" +newNode.attributes.staff.id,newNode);
		return newNode;
	},
	checkChangeOnTree:function(){
		if(UnitTreeCatalog._lstStaffAdd.keyArray.length>0 ||
				UnitTreeCatalog._lstStaffDelete.keyArray.length>0){
			$('#btnSave').removeAttr('disabled');
			$('#btnCancel').removeAttr('disabled');
			$('#btnSave').removeClass('BtnGeneralDStyle');
			$('#btnCancel').removeClass('BtnGeneralDStyle');
			return true;
		}else{
			$('#btnSave').attr('disabled','disabled');
			$('#btnCancel').attr('disabled','disabled');
			$('#btnSave').addClass('BtnGeneralDStyle');
			$('#btnCancel').addClass('BtnGeneralDStyle');
			return false;
		}
	},
	checkChangeTree:function(callBack,noCall){//nếu noCall=1 thì không cho callback khi bấm hủy
		if(UnitTreeCatalog.checkChangeOnTree()==true){
			$.messager.confirm('Xác nhận', 'Bạn có muốn lưu các thay đổi.', function(r){
				if (r){
					if(callBack!=undefined && callBack!=null){
						UnitTreeCatalog._callBack=callBack;
					}
					UnitTreeCatalog.saveTree();
				}else if(noCall==undefined || noCall==null){//hủy
					UnitTreeCatalog.reloadUnitTree();
					if(callBack!=undefined && callBack!=null){
						callBack.call(this);
					}
				}
			});
		}else{
			if(callBack!=undefined && callBack!=null){
				callBack.call(this);
			}
		}
	},
	saveTree:function(confirm){
		var lstStaffAddId=new Array();//danh sach nhan vien them vao group moi
		var lstStaffShopAddId=new Array();//danh sach nhan vien xoa khoi nhom
		var flagUpdate=false;
		for(var i=0;i<UnitTreeCatalog._lstStaffAdd.keyArray.length;i++){
			var temp = UnitTreeCatalog._lstStaffAdd.get(UnitTreeCatalog._lstStaffAdd.keyArray[i]);
			if(temp!=null && temp.attributes.staff!=null){
				lstStaffAddId.push(temp.attributes.staff.id);
				lstStaffShopAddId.push(temp.attributes.staff.shop.id);
				flagUpdate=true;
			}
		}
		var param = new Object();
		param.lstStaffShopAddId=lstStaffShopAddId;
		param.lstStaffAddId=lstStaffAddId;
		if(!flagUpdate){//không có thay đổi
			$('#successMsg').html('Bạn chưa có thay đổi nào').show();
			setTimeout(function(){$('#successMsg').html('').hide();},4000);
			return;
		}
		if(confirm!=undefined && confirm!=null && confirm==1){
			Utils.addOrSaveData(param, '/catalog/unit-tree/save-tree', null, 'errMsg', function(result){
				UnitTreeCatalog.saveTreeSuccess(result);
			});
		}else{
			Utils.saveData(param, '/catalog/unit-tree/save-tree', null, 'errMsg', function(result){
				UnitTreeCatalog.saveTreeSuccess(result);
			});
		}
	},
	saveTreeSuccess:function(result){
		if(!result.error){
			$('#successMsg').html('Lưu dữ liệu thành công').show();
			setTimeout(function(){$('#successMsg').html('').hide();},4000);
			UnitTreeCatalog._lstStaffAdd=new Map();
			UnitTreeCatalog._lstStaffDelete=new Map();
			UnitTreeCatalog.checkChangeOnTree();//disabled 2 button lưu và hủy
			if(UnitTreeCatalog._callBack!=null){
				UnitTreeCatalog._callBack.call(this);
			} else {
				UnitTreeCatalog.reloadUnitTree();
			}
		}
	},
	reloadUnitTree:function(){
		if(UnitTreeCatalog._idShopAddNewStaff!=null){
			UnitTreeCatalog.loadUnitTree(UnitTreeCatalog._idShopAddNewStaff);
		}else{
			var node = $('#tree').tree('getSelected');
			if(node!=null && node.attributes!=null){
				var attr=node.attributes;
				if(attr.shop!=null) UnitTreeCatalog.loadUnitTree(attr.shop.id);
				else if(attr.staff!=null) UnitTreeCatalog.loadUnitTree(attr.staff.shop.id);
				else UnitTreeCatalog.loadUnitTree();
			}else{
				UnitTreeCatalog.loadUnitTree();
			}
		}
	},
	cancelOrganization:function(){
		UnitTreeCatalog.openCollapseTab();
		UnitTreeCatalog.showCollapseTab(1);
		
	}
	,
	cancelAction:function(){
		$.messager.confirm('Xác nhận', 'Bạn có muốn hủy các thao tác đã làm.', function(r){
			if (r){
				UnitTreeCatalog.loadUnitTree();
			}
		});
	},
	loadUnitTree:function(idSearch,idSearchStaff){
		UnitTreeCatalog._listNodeTree = new Map();
		UnitTreeCatalog._lstStaffAdd = new Map();
		UnitTreeCatalog._lstStaffDelete = new Map();
		UnitTreeCatalog._idShopAddNewStaff = null;
		UnitTreeCatalog.checkChangeOnTree();//disabled 2 button lưu và hủy
		if (idSearch != undefined && idSearch != null && idSearch != 0) {
			UnitTreeCatalog._idSearch = idSearch;
			if (idSearchStaff != undefined && idSearchStaff != null
					&& idSearchStaff != 0)
				UnitTreeCatalog._idSearchStaff = idSearchStaff;
		}
		$('#tree').tree({  
			url:'/catalog/unit-tree/get-list-unit-tree' ,
			dnd:true,
			lines:true,
			onBeforeLoad: function(n,p){
				if(UnitTreeCatalog._idSearch!=null && UnitTreeCatalog._idSearch!=0){
					p.idSearch=UnitTreeCatalog._idSearch;
					if (idSearchStaff && Number(idSearchStaff)) {
						p.staffId = idSearchStaff;
					}
					$('#divOverlay').show();
				}
			},
			 formatter: function(node){
					var tmpText = Utils.XSSEncode(node.text);
					if (node.attributes.shop != null && node.attributes.shop.status == 'RUNNING'){
						if(node.iconCls != null){
							return ('<img src="' +node.iconCls+'" width="15px" height="15px"> ' + Utils.XSSEncode(tmpText) + '<span style="color: dodgerblue;">&nbsp;</span>');
						}else{
							return (tmpText + '<span style="color: dodgerblue;">&nbsp;</span>');
						}
					}
					if (node.attributes.shop != null && node.attributes.shop.status == 'STOPPED'){
						if(node.iconCls != null){
							return ('<img src="' +node.iconCls+'" width="15px" height="15px"> ' + Utils.XSSEncode(tmpText) + '<span style="color: red;">&nbsp;(I)</span>');
						}else{
							return (tmpText + '<span style="color: red;">&nbsp;(I)</span>');
						}
					}
					if (node.attributes.staff != null && node.attributes.staff.status == 'RUNNING'){
						if(node.iconCls != null){
							return ('<img src="' +node.iconCls+'" width="15px" height="15px"> ' + tmpText + '<span style="color: dodgerblue;">&nbsp;</span>');
						}else{
							return (tmpText + '<span style="color: dodgerblue;">&nbsp;</span>');
						}
					}
					if (node.attributes.staff != null && node.attributes.staff.status == 'STOPPED'){
						if(node.iconCls != null){
							return ('<img src="' +node.iconCls+'" width="15px" height="15px"> ' + tmpText + '<span style="color: red;">&nbsp;(I)</span>');
						}else{
							return (tmpText + '<span style="color: red;">&nbsp;(I)</span>');
						}
					}
					if (node.iconCls != null) {
						return '<img src="' +node.iconCls+'" width="15px" height="15px"> ' + Utils.XSSEncode(node.text);
					} else {
						return Utils.XSSEncode(node.text);
					}
			 },
			 onLoadSuccess : function(parent, data) {
				if (UnitTreeCatalog._idSearch != null) {
					try {
						var id=UnitTreeCatalog._idSearch;
						if(UnitTreeCatalog._idSearchStaff!=undefined && UnitTreeCatalog._idSearchStaff!=null && UnitTreeCatalog._idSearchStaff!=0){
							id='s' +UnitTreeCatalog._idSearchStaff;
						}
						var node = $('#tree').tree('find', id);
						if(node!=null){
							$('#tree').tree('select', node.target);
						}
					}catch(e){}
					UnitTreeCatalog._idSearch=null;
					UnitTreeCatalog._idSearchStaff=0;
					$('#divOverlay').hide();
				}
				UnitTreeCatalog.putMapListNodeTree(parent,data);
				UnitTreeCatalog.cbStopChange();
				UnitTreeCatalog.resize();
				//Xu ly phan quyen ctrl
				Utils.functionAccessFillControl ('idDivTreeShop', function() {
					//Xu ly cac su kien lien quan den control phan quyen 
				});
			},
			onContextMenu:function(e, node){
				e.preventDefault();
				$('#tree').tree('select', node.target);
				if(UnitTreeCatalog.filterContextMenu(node)){
					$('#group_tree_dbl_right_contextMenu').addClass('easyui-menu');
					$('#group_tree_dbl_right_contextMenu').menu('show', {left: e.pageX,top: e.pageY});
				}
				$('#group_tree_dbl_right_contextMenu').css({top: e.pageY});
			},
			onCollapse:function(node){
				UnitTreeCatalog.resize();
			},
			onExpand :function(node) {
				UnitTreeCatalog.resize();
			},
			onClick:function(node){
				if($('#searchStaffUnitTreeGrid').length==1 && node!=null && node.attributes!=null && node.attributes.shop!=null){
					UnitTreeCatalog.search(0);
				}
			},
			onDblClick:function(node){
				//if($('#cbEdit').is(':checked')){
					if(node!=null && node.attributes!=null){
						if(node.attributes.shop!=null && node.attributes.shop.id > 0){
							UnitTreeCatalog.openCollapseTab();
							if (node.attributes.shop.parentShop != null && node.attributes.shop.parentShop !=undefined ) {
								UnitTreeCatalog.showShopInfoScreen(node.attributes.shop.parentShop.id,node.attributes.shop.id);
							}else {
								UnitTreeCatalog.showShopInfoScreen(node.attributes.shop.parentShop,node.attributes.shop.id);
							}	
						} else if(node.attributes.staff!= null) {
							UnitTreeCatalog.openCollapseTab();
							UnitTreeCatalog.showStaffEditScreen(node.attributes.staff.id,node.attributes.staff.shop.id);
						}
					}
				//}
			},
			onBeforeDrag:function(node){
				if(!$('#cbEdit').is(':checked')) return false;
				if(node.attributes==null || node.attributes.staff==null)
					return false;
				return true;
			},
			onBeforeDrop:function(target,source,point){
				if(point!=undefined && point != "append") {
					return false;
				}
				var nodeTarget=$('#tree').tree('getNode', target);
				if(nodeTarget==null) {
					return false;
				}
				return UnitTreeCatalog.doDrop(nodeTarget, source);
			}
		});
	},
	doDrop: function(nodeTarget, source) {
		if(!UnitTreeCatalog.checkDropEnter(source,nodeTarget)) {
			return false;
		}
		UnitTreeCatalog.fillMapStaffForDND(nodeTarget,source);
		UnitTreeCatalog.checkChangeOnTree();//disabled 2 button lưu và hủy
		UnitTreeCatalog._callBack = function() {
			UnitTreeCatalog._callBack = null;
			var st = source.attributes.staff;
			UnitTreeCatalog.loadUnitTree(st.shop.id, st.id);
		};
		UnitTreeCatalog.saveTree(); // luu truc tiep
		return true;
	},
	checkStaffOnTree:function(staffId){
		for(var i=0;i<UnitTreeCatalog._listNodeTree.keyArray.length;i++){
			var temp = UnitTreeCatalog._listNodeTree.get(UnitTreeCatalog._listNodeTree.keyArray[i]);
			if(temp!=null && temp.attributes!=null && temp.attributes.staff!=null && temp.attributes.staff.id==staffId){
					return true;
			}
		}
		return false;
	},
	addStaffForTree:function(staff,target){
		var iconCls="tricon6";
		var attributes = new Object();
		attributes.shop=null;
		attributes.staff=staff;
		attributes.parent=target.attributes;
		var strAlias= Utils.XSSEncode(attributes.staff.staffCode+' - ' +attributes.staff.staffName);
		if(attributes.staff.staffType.objectType==StaffRoleType.NVGS){
			iconCls="tricon4";
		}else if(attributes.staff.staffType.objectType==StaffRoleType.TBHM){
			strAlias=attributes.staff.staffName;
			iconCls="tricon2";
		}else if(attributes.staff.staffType.objectType==StaffRoleType.TBHV){
			strAlias=attributes.staff.staffName;
			iconCls="tricon7";
		}
		strAlias = Utils.XSSEncode(strAlias);
		var node = new Object();
		node.text= strAlias;
		node.id=attributes.staff.id;
		node.attributes=attribute;
		node.state='close';
		var nodeTemp = jQuery.extend(true, {}, node);
		var nodeTemp1 = jQuery.extend(true, {}, node);
		UnitTreeCatalog._listNodeTree.put("staff" +nodeTemp.attributes.staff.id,nodeTemp);
		UnitTreeCatalog._lstStaffAdd.put("staff" +nodeTemp1.attributes.staff.id,nodeTemp1);
		$('#tree').tree('append', {  
			parent: (target?target.target:null),  
			data: [{  
				text: Utils.XSSEncode(nodeTemp.text),
				id: nodeTemp.id,
				attributes: nodeTemp.attributes,
				state: nodeTemp.state,
				iconCls:iconCls
			}]  
		});
		UnitTreeCatalog.checkChangeOnTree();//disabled 2 button lưu và hủy
		UnitTreeCatalog._callBack=null;
	},
	addStaff: function(type){
		var nodeSelect = $('#tree').tree('getSelected');
		if(nodeSelect==null || nodeSelect.attributes.staff!=null) return false;
		var shopId=nodeSelect.attributes.shop.id;
		var arrParam = new Array();
		var param = new Object();
		param.name = 'shopId';
		param.value = shopId;
		arrParam.push(param);
		var param1 = new Object();
		param1.name = 'objectType';
		param1.value = type;
		arrParam.push(param1);
		CommonSearchEasyUI.searchNVUnitTreeDialog(function(data){
			if(data!=null && data.length>0){
				for(var i=0;i<data.length;i++){
					var staff=new Object();
					staff.attributes={staff:{id:data[i].id,shop:{}}};
					staff.attributes.staff.shop.id=shopId;
					UnitTreeCatalog._lstStaffAdd.put("staff" +data[i].id,staff);
				}
				UnitTreeCatalog.saveTree(); // luu truc tiep
			}
		}, arrParam);
	},
	addNewStaff:function(type,typeCheck){
		UnitTreeCatalog._nodeSelect=$('#tree').tree('getSelected');
		var node = UnitTreeCatalog._nodeSelect;
		if(node.attributes!=null && node.attributes.shop!=null){
			UnitTreeCatalog._idShopAddNewStaff=node.attributes.shop.id;
			UnitTreeCatalog.checkChangeTree(function(){
				var shopCode=Utils.XSSEncode(node.attributes.shop.shopCode);
				var shopName=Utils.XSSEncode(node.attributes.shop.shopName);
				UnitTreeCatalog.showCollapseTab(-3,null,null,shopCode,shopName,type);
			},1);
		}
	},
	addNewShop:function(){
		var node=$('#tree').tree('getSelected');
		if(node.attributes!=null && node.attributes.shop!=null){
			UnitTreeCatalog.checkChangeTree(function(){
				UnitTreeCatalog.showCollapseTab(-2,null,node.attributes.shop.id);
			});
		}
	},
	updateShop:function(){
		var node=$('#tree').tree('getSelected');
		if(node.attributes!=null && node.attributes.shop!=null){//shop
			UnitTreeCatalog.checkChangeTree(function(){
				UnitTreeCatalog.showCollapseTab(-2,node.attributes.shop.id,null);
			});
		}
	},
	//-------------------------------------------------------------------------------------------------------
	resize: function() {
		var hSidebar3=$('#tree').height();
		var hSidebar1=$('.Content3Section').height();
		
		if(hSidebar3> 650) {
			$('.ReportTree2Section').css('height', 'auto');	
		} else {
			if(hSidebar3 <  hSidebar1) {
				if(hSidebar1 < 700) {
					$('.ReportTree2Section').height(hSidebar1-60);
				} else {
					$('.ReportTree2Section').height(650);
				}
			} else {
				if(hSidebar3 < 300) {
					$('.ReportTree2Section').height(300);
				} else {
					$('.ReportTree2Section').css('height', 'auto');	
				}
			}
		}
		var hSidebar2=$('.Sidebar1Section').height();
		if( hSidebar1 > hSidebar2 ) {
			$('.Content1Section').height(hSidebar1);
		}
		else $('.Content1Section').height(hSidebar2);
		return ;
	},
	openCollapseTab: function() {
		$('#collapseTab').addClass('Item1Close');
		$('#collapseTab').removeClass('Item1');
		$('.Sidebar1Section').css('width', '24.9%');
		$('#unitTreeContent').show();
	},
	closeCollapseTab: function(){
		$('#collapseTab').addClass('Item1');
		$('#collapseTab').removeClass('Item1Close');
		$('.Sidebar1Section').css('width', '95.8%');
		$('#unitTreeContent').hide();
	},
	removeActiveCollapseTab: function(){
		$('#collapseTab').removeClass('Active');
		$('#collapseTab1').removeClass('Active');
		$('#collapseTab2').removeClass('Active');
		/*if(!$('#collapseTab2').hasClass('Active')){
			$('#btnSave').addClass('BtnGeneralDStyle');
			$('#btnSave').attr('disabled', 'disabled');
		}*/
		$('#collapseTab3').removeClass('Active');
	},
	showCollapseTab: function(type,shopId,parentShopId,shopStaffCode, shopStaffName,staffType,staffId){
		if(UnitTreeCatalog._isChange){
			//VUONGBD MULTILANGUAGE DIALOG
			$.messager.confirm(jsp_common_xacnhan,catalog_unit_view_thong_tin_nhan_vien_back, function(r){
				if (r){
					UnitTreeCatalog._isChange=false;
					//UnitTreeCatalog.openCollapseTab();
					UnitTreeCatalog.showSearchScreen();
					//UnitTreeCatalog.showCollapseTab(type,shopId,parentShopId,shopStaffCode, shopStaffName,staffType,staffId);
			}});
			return false;
		}
		UnitTreeCatalog.removeActiveCollapseTab();
		if(type>0) {
			UnitTreeCatalog.checkChangeTree(function(){
				var checkLoad = false;
				if($('#collapseTab').hasClass('Item1')){
					UnitTreeCatalog.openCollapseTab();
					checkLoad = true;
				}
				else if($('#collapseTab').hasClass('Item1Close')){
					if(type == UnitTreeCatalog._typeScreen) {
						//UnitTreeCatalog.closeCollapseTab();
						checkLoad = true;
					} else {
						checkLoad = true;
					}
				}
				if(type == 1) {
					$('#collapseTab1').addClass('Active');
					if(checkLoad) UnitTreeCatalog.showSearchScreen();
				} else if(type ==2 ) {
					$('#collapseTab2').addClass('Active');
					/*$('#btnSave').removeClass('BtnGeneralDStyle');
					$('#btnSave').removeAttr('disabled');*/
					//p if(checkLoad) UnitTreeCatalog.showShopInfoScreen();
					UnitTreeCatalog.showShopInfoScreen(parentShopId,shopId);
				} else if(type ==3) {
					$('#collapseTab3').addClass('Active');
					if(checkLoad) UnitTreeCatalog.showStaffSearchScreen();
				}
				if(type != 0 ) UnitTreeCatalog._typeScreen = type ;
				UnitTreeCatalog.resize();
				// 1man hinh search
				// 2man hinh them moi hoac cap nhat don vi
				// 3man hinh them moi hoac cap nhat nhan vien
			});
		} else {
			// 0 (Open and Close binh thuong )
			// -1(Close va load lai cay theo shopId)
			// -2(Them moi hoac cap nhat shop)
			// -3(Man hinh cap nhat nhan vien)
			if(type ==0) {
				if($('#collapseTab').hasClass('Item1')){
					UnitTreeCatalog.openCollapseTab();
					$('#collapseTab').addClass('Active');
					if(UnitTreeCatalog._typeScreen == 0) {
						UnitTreeCatalog.showSearchScreen();
						UnitTreeCatalog._typeScreen = 4;
					}
				}
				else if($('#collapseTab').hasClass('Item1Close')){
					UnitTreeCatalog.closeCollapseTab();
				}
			}else if(type == -1) {
				UnitTreeCatalog.closeCollapseTab();
				if( shopId != undefined && shopId != null) {
					UnitTreeCatalog.loadUnitTree(shopId,staffId);
				}
				UnitTreeCatalog._typeScreen = type ;
			}else if(type == -2) {
				UnitTreeCatalog.openCollapseTab();
				UnitTreeCatalog.showShopInfoScreen(parentShopId,shopId);
			}else if(type == -3) {
				UnitTreeCatalog.openCollapseTab();
				UnitTreeCatalog.showStaffCreateScreen(staffType,shopStaffCode,shopStaffName);
			}
			if(type != 0 ) UnitTreeCatalog._typeScreen = type ;
			UnitTreeCatalog.resize();
		}
		return false;
	},
	showSearchScreen: function(tabId){
		var params = new Object();
		Utils.getHtmlDataByAjax(params, '/catalog/unit-tree/show-search',function(data) {
			$("#unitTreeContent").html(data).show();
			if (tabId) {
				$("#tabId").click();
			}
		}, null, 'POST');
		return false;
	},
	//ThongNM-------------------------------------------------------------------------------------------------
	
	//Search UnitTree
	hideAllTab: function(){
		$('#tabActive1').removeClass('Active');
		$('#tabActive2').removeClass('Active');
		$('#container1').hide();
		$('#container2').hide();
	},
	showTab1: function(){
		$( "#importFrm" ).prop( "disabled", false );
		UnitTreeCatalog.hideAllTab();
		$('#tabActive1').addClass('Active');
		$('#container1').show();
		Utils.bindAutoSearchEx('.SProduct1Form');
		$('#errExcelMsg').html('').hide();
	},
	showTab2: function(){
		$( "#importShopFrm" ).prop( "disabled", false );
		UnitTreeCatalog.hideAllTab();
		$('#tabActive2').addClass('Active');
		$('#container2').show();
		$('#searchShopUnitTreeGrid').show();
		$('#staffCode').val('');
		$('#staffName').val('');
		Utils.bindAutoSearchEx('.SProduct1Form');
		$('#errExcelMsg').html('').hide();
	},
	searchShop: function() {
		$('#errMsgSearchShop').html('').hide();
		var rawSearchParam = {
			unitCode: $('#shopCode').val(),
			unitName: $('#shopName').val(),
			unitType: $('#unitType').val(),
			status: $('#shopStatus').val(),
			searchObjectType: 'SHOP'
		}
		
		try {
			var selectedNodeId = $('#manageShop').combotree('getValue');
			var tree = $('#manageShop').combotree('tree');
			var node = $(tree).tree('find', selectedNodeId);
			rawSearchParam.manageUnitCode = node.attributes.shop.shopCode;
		} catch (e) {
			// pass through
		}

		if (rawSearchParam.unitType === "-1") {
			delete rawSearchParam.unitType;
		}
		if (rawSearchParam.status === 'ALL') {
			delete rawSearchParam.status;
		}

		var searchParamPrefix = 'unitFilter';
		var searchParam = {};
		convertToSimpleObject(searchParam, rawSearchParam, searchParamPrefix);
		$('#searchShopUnitTreeGrid').datagrid('load', searchParam);
		$('#shopCode').focus();
	},
	/**
	 * export don vi de xuat ra file excel : gom tim kiem va export
	 * @author phuocdh2
	 * @return void
	 * @since  24-Mar-2015
	 */
	exportShop: function() {
		$('#errMsg').html('').hide();
		var rawSearchParam = {
			unitCode: $('#shopCode').val(),
			unitName: $('#shopName').val(),
			unitType: $('#unitType').val(),
			status: $('#shopStatus').val(),
			searchObjectType: 'SHOP'
		}
		
		try {
			var selectedNodeId = $('#manageShop').combotree('getValue');
			var tree = $('#manageShop').combotree('tree');
			var node = $(tree).tree('find', selectedNodeId);
			rawSearchParam.manageUnitCode = node.attributes.shop.shopCode;
		} catch (e) {
			// pass through
		}

		if (rawSearchParam.unitType === "-1") {
			delete rawSearchParam.unitType;
		}
		if (rawSearchParam.status === 'ALL') {
			delete rawSearchParam.status;
		}

		var searchParamPrefix = 'unitFilter';
		var searchParam = {};
		convertToSimpleObject(searchParam, rawSearchParam, searchParamPrefix);
		var url = "/catalog/unit-tree/export-shop-excel";
		ReportUtils.exportReport(url, searchParam, 'errExcelMsg');
		return false;
	},
	/**
	 * tim kiem nhan vien o tab nhan vien
	 * @author tuannd20
	 * @return void
	 * @since  13/03/2015
	 */
	searchStaff: function() {
		$('#errMsgSearchShop').html('').hide();
		var rawSearchParam = {
			unitCode: $('#staffCode').val(),
			unitName: $('#staffName').val(),
			unitType: $('#staffType').val(),
			status: $('#staffStatus').val(),
			searchObjectType: 'STAFF'
		}
		
		try {
			var selectedNodeId = $('#manageShopStaff').combotree('getValue');
			var tree = $('#manageShopStaff').combotree('tree');
			var node = $(tree).tree('find', selectedNodeId);
			rawSearchParam.manageUnitCode = node.attributes.shop.shopCode;
		} catch(e) {
			// pass through
		}

		if (rawSearchParam.unitType === "-1") {
			delete rawSearchParam.unitType;
		}
		if (rawSearchParam.status === 'ALL') {
			delete rawSearchParam.status;
		}

		var searchParamPrefix = 'unitFilter';
		var searchParam = {};
		convertToSimpleObject(searchParam, rawSearchParam, searchParamPrefix);
		$('#searchStaffUnitGrid').datagrid('load', searchParam);
		$('#staffCode').focus();
	},
	showShopInfoScreen: function(parentShopId,shopId,nodeTypeId,orgId){
		var params = new Object();
		if(parentShopId !=  undefined && parentShopId != null) {
			params.parentShopId = parentShopId;
		}
		if(shopId !=  undefined && shopId != null) {
			params.shId = shopId;
		} else {
			params.shId = '';
		}
		var node = $('#tree').tree('getSelected');//p+
		if (nodeTypeId == null) {
			var shopType= '';
			if (node.attributes.shop.type != undefined && node.attributes.shop.type != null) {
				 shopType = node.attributes.shop.type.id;
			}	
			if (shopType != undefined && shopType != null) {
				params.shopType = shopType;
			}//p+
		} else {
			params.shopType = nodeTypeId;
		}
		if(orgId !=  undefined && orgId != null) {
			//params.shopId = shopId;
			params.organizationId = orgId;
		} else {
			params.organizationId = '';
		}
		Utils.getHtmlDataByAjax(params, '/catalog/unit-tree/show-shopinfo',function(data) {
			$("#unitTreeContent").html(data).show();
			UnitTreeCatalog.resize();
		}, null, 'POST');
		return false;
	},
	updateShopInfoScreenWhenLoad: function(isUpdate,allowSelectParentShop,shopTypeOfShop,statusOfShop,areaIdOfShop,shopTypeOfParentShop){
		isUpdate = (isUpdate =='true');
		
		allowSelectParentShop = (allowSelectParentShop =='true');
		
		if(shopTypeOfShop == undefined || shopTypeOfShop == null || shopTypeOfShop=='') shopTypeOfShop = -1;
		else shopTypeOfShop = Number(shopTypeOfShop);
		
		if(statusOfShop == undefined || statusOfShop == null || statusOfShop=='') statusOfShop = 1;
		
		if(areaIdOfShop == undefined || areaIdOfShop == null || areaIdOfShop=='') areaIdOfShop = null;
		else areaIdOfShop = Number(areaIdOfShop);
		
		if(shopTypeOfParentShop == undefined || shopTypeOfParentShop == null || shopTypeOfParentShop=='') shopTypeOfParentShop = -1;
		else shopTypeOfParentShop = Number(shopTypeOfParentShop);
		
		$('.RequireStypeParentShopCode').hide();
		var isDisableAreaTree = false;
		if(isUpdate) {
			$('.RequireStypeShopCode').hide();
			disabled('shopCode');
			setSelectBoxValue('status',statusOfShop);
			if(allowSelectParentShop) {
				$('.RequireStypeParentShopCode').show();
				UnitTreeCatalog.loadComboTreeShop('parentShopTree', 'parentShopCode', $("#parentShopIdHd").val().trim(), function(data) {
					if(data.id> 0) {
						var type = data.type.objectType;
						if (ShopObjectType.NPP == shopTypeOfShop && ShopObjectType.VUNG != type) {
							Alert('Thông báo','Mã đơn vị cha phải là cấp Vùng',function(){
								$("#parentShopTree").combotree("setValue", $("#parentShopIdHd").val().trim());
								$('#parentShopName').val($("#parentShopNameHd").val().trim());
							});
						} else  if (ShopObjectType.VUNG == shopTypeOfShop && ShopObjectType.MIEN != type) {
							Alert('Thông báo','Mã đơn vị cha phải là cấp Miền',function(){
								$("#parentShopTree").combotree("setValue", $("#parentShopIdHd").val().trim());
								$('#parentShopName').val($("#parentShopNameHd").val().trim());
							});
						} else {
							setSelectBoxValue('shopType',type+1);
							$('#parentShopName').val(Utils.XSSEncode(data.shopName));
						}
					} else {
						if (ShopObjectType.NPP == shopTypeOfShop) {
							Alert('Thông báo','Mã đơn vị cha phải là cấp Vùng');
						} else  if (ShopObjectType.VUNG == shopTypeOfShop) {
							Alert('Thông báo','Mã đơn vị cha phải là cấp Miền');
						}
						$("#parentShopTree").combotree("setValue", $("#parentShopIdHd").val().trim());
						$('#parentShopName').val($("#parentShopNameHd").val().trim());
					}
					UnitTreeCatalog._isChange=true;
				}, null, true);
			}
			
		} else {
			
			if(allowSelectParentShop) {
				$('.RequireStypeParentShopCode').show();
				TreeUtils.loadComboTreeShopHasTitle('parentShopTree', 'parentShopCode',function(data) {
					if(data.id> 0) {
						var type = data.type.objectType;
						if(type == ShopObjectType.NPP || type == ShopObjectType.NPP_KA || type == ShopObjectType.MIEN_ST || type == 14) {
							Alert('Thông báo','Mã đơn vị cha phải là cấp GT(KA, ST), Miền, Vùng',function(){
								$('#parentShopTree').combotree('setValue',activeType.ALL);
								setSelectBoxValue('shopType',-1);
							});
						} else {
							setSelectBoxValue('shopType',type+1);
							UnitTreeCatalog.updateShopInfoScreenWhenSelectParentShop(type+1);
						}
						UnitTreeCatalog._isChange=true;
					} else {
						var shType = data.type;
						if ((shType == undefined || shType == null) && data.id != activeType.ALL) {
							Alert('Thông báo','Mã đơn vị cha phải là cấp GT(KA, ST), Miền, Vùng');
							$('#parentShopTree').combotree('setValue',activeType.ALL);
						}
						setSelectBoxValue('shopType',-1);
					}
					$('#parentShopName').val(Utils.XSSEncode(data.shopName));
				}, true);
			} else {
				shopTypeOfShop = shopTypeOfParentShop+1;
				
			}
		}
		TreeUtils.loadComboTreeAreaForUnitTreeHasTitle('areaTree', 'areaId',areaIdOfShop,function(data) {
			if(data.id != activeType.ALL && !isDisableAreaTree) {
				if(data.type != 'WARD') {
					Alert('Thông báo','Địa bàn chọn phải là phường/xã.',function(){
						$('#areaTree').combotree('setValue',activeType.ALL);
						$('#provinceName').val('');
						$('#districtName').val('');
						$('#precinctName').val('');
						//ViettelMap.clearOverlays();
					});
				} else {
					UnitTreeCatalog.loadAreaInfoAndMap(data);
				}
			}
		}, function() {
			if(isDisableAreaTree) { 
				disableComboTree('areaTree');
				$('#areaTree').combotree('setValue',activeType.ALL);
			}
		});
		//disableSelectbox('shopType');
		//setSelectBoxValue('shopType',shopTypeOfShop);
		return false;
	},
	loadComboTreeShop: function(controlId, storeCode,defaultValue,callback,params,returnCode){		 
		var _url= '/rest/catalog/parentshop/combotree/{0}.json';
		_url = _url.replace("{0}", $("#parentShopIdHd").val().trim());
		$('#' + controlId).combotree({url: _url,
			formatter: function(node) {
				return Utils.XSSEncode(node.text);
			},
			onChange: function(newValue,oldValue){
				if(params != null && params.length >0){
					for(var i=0;i<params.length;i++){
						$('#' +params[i]).val('');
					}
				}
			}
		});
		var t = $('#' + controlId).combotree('tree');
		t.tree('options').url = '';
		t.tree({			
			onSelect:function(node){
				var data = new Object();
				if(node.attributes != null) {
					data = node.attributes.shop;
				} else {
					data.id= node.id;
				}
				if(callback!=undefined && callback!=null){
					callback.call(this, data);
				}
				if(returnCode) {
					$('#' +storeCode).val(data.shopCode);
				} else {
					$('#' +storeCode).val(node.id);	
				}	
				$('#' +controlId).combotree('setValue', Utils.XSSEncode(data.shopCode));
			},
			onBeforeExpand:function(result){	            					
				var nodes = t.tree('getChildren',result.target);
				/*for(var i=0;i<nodes.length;++i){
					t.tree('remove',nodes[i].target);
				}*/
				if (nodes.length > 0) {
					return;
				}
				var ___jaxurl = '/rest/catalog/shop/combotree/2/';
				$.ajax({
					type : "POST",
					url : ___jaxurl + result.id +'.json',
					dataType : "json",
					success : function(r) {
						$(r).each(function(i,e){
							e.text = Utils.XSSEncode(e.text);
						});
						t.tree('append',{
							parent:result.target,
							data:r
						});
						var node=t.tree('getNode',result.target);
						$(node.target).next().css('display','block');
					}
				});
			},
			onExpand: function(result){	
			}
		});
		if(defaultValue == undefined || defaultValue == null){
			$('#' + controlId).combotree('setValue',activeType.ALL);
		} else {
			$('#' + controlId).combotree('setValue',defaultValue);
		}
	},
	resetScreen:function() {
		$('#phoneNumber').val('');
		$('#mobileNumber').val('');
		$('#faxNumber').val('');
		$('#email').val('');
		//$('#accountNumber').val('');
		//$('#bankName').val('');
		$('#taxNumber').val('');
		$('#addressBillTo').val('');
		$('#contactName').val('');
		$('#addressShipTo').val('');
		$('#address').val('');
	},
	updateShopInfoScreenWhenSelectParentShop: function(type) {
		UnitTreeCatalog.resetScreen();
		if(type == 3) {
			enable('phoneNumber');
			enable('mobileNumber');
			enable('phoneNumber');
			enable('faxNumber');
			enable('email');
			enable('taxNumber');
			enable('addressBillTo');
			enable('contactName');
			enable('addressShipTo');
			enable('address');
			enableComboTree('areaTree');
			$('.RequireStypeOfParentShopCode').show();
		} else {
			disabled('phoneNumber');
			disabled('mobileNumber');
			disabled('phoneNumber');
			disabled('faxNumber');
			disabled('email');
			disabled('taxNumber');
			disabled('addressBillTo');
			disabled('contactName');
			disabled('addressShipTo');
			disabled('address');
			$('.RequireStypeOfParentShopCode').hide();
			$('#areaTree').combotree('setValue',activeType.ALL);
			disableComboTree('areaTree');
		}
	},
	addOrUpdateShopInfo:function(){
		$('#errMsgAddOrUpdate').html('').hide();
		var msg = '';
		//var msg = Utils.getMessageOfRequireCheck('parentShopCode','Mã đơn vị cha');
		/*if(msg.length==0){
			msg = Utils.getMessageOfSpecialCharactersValidate('parentShopCode','Mã đơn vị cha',Utils._CODE);
		}*/
		if (msg.length == 0) {
			msg = Utils.getMessageOfRequireCheck('shopCode', 'Mã đơn vị');
		}
		if (msg.length == 0) {
			msg = Utils.getMessageOfSpecialCharactersValidate('shopCode', 'Mã đơn vị', Utils._CODE);
		}
		if (msg.length == 0) {
			msg = Utils.getMessageOfRequireCheck('shopName', 'Tên đơn vị');
		}
		if (msg.length == 0) {
			msg = Utils.getMessageOfSpecialCharactersValidate('shopName', 'Tên đơn vị', Utils._SPECIAL);
		}
		if (msg.length == 0) {
			msg = Utils.getMessageOfRequireCheck('shopType', 'Loại đơn vị', true);
		}
		if (msg.length == 0) {
			msg = Utils.getMessageOfRequireCheck('status', 'Trạng thái', true);
		}
		var shopType = $('#shopType').val().trim();
		var areaId = null;
		//p if(shopType ==3) {
		/** validate giong truong ten*/
		if (msg.length == 0 && $('#phoneNumber').val().trim().length > 0) {
			msg = Utils.getMessageOfSpecialCharactersValidate('phoneNumber', 'Phone', Utils._SPECIAL);
		}
		if (msg.length == 0 && $('#mobileNumber').val().trim().length > 0) {
			msg = Utils.getMessageOfSpecialCharactersValidate('mobileNumber', 'Mobile', Utils._SPECIAL);
		}
		if (msg.length == 0 && $('#faxNumber').val().trim().length > 0) {
			msg = Utils.getMessageOfSpecialCharactersValidate('faxNumber', 'Số Fax', Utils._SPECIAL);
		}
		if (msg.length == 0) {
				msg = Utils.getMessageOfInvalidEmailFormat('email','Email');
		}
		/*if(msg.length == 0 && $('#accountNumber').val().trim().length>0){
			msg = Utils.getMessageOfSpecialCharactersValidateEx('accountNumber','Số tài khoản',Utils._TF_NUMBER);
		}*/
		if (msg.length == 0 && $('#abbreviation').val().trim().length > 0) {
			msg = Utils.getMessageOfSpecialCharactersValidate('abbreviation', 'Tên viết tắt', Utils._SPECIAL);
		}
		if (msg.length == 0) {
			msg = Utils.getMessageOfSpecialCharactersValidate('addressBillTo', 'Địa chỉ giao HĐ', Utils._ADDRESS);
		}
		if (msg.length == 0 && $('#contactName').val().trim().length > 0) {
			msg = Utils.getMessageOfSpecialCharactersValidate('contactName', 'Tên người liên hệ', Utils._NAME);
		}
		if (msg.length == 0) {
			msg = Utils.getMessageOfSpecialCharactersValidate('addressShipTo', 'Địa chỉ giao hàng', Utils._ADDRESS);
		}
		/*if(msg.length == 0){
			msg = Utils.getMessageOfRequireCheck('address','Địa chỉ');
		}*/
		if (msg.length == 0) {
			msg = Utils.getMessageOfSpecialCharactersValidate('address', 'Địa chỉ', Utils._ADDRESS);
		}
		areaId = $('#areaId').val().trim();
		if(msg.length > 0){
			$('#errMsgAddOrUpdate').html(msg).show();
			return false;
		}
		var node = $('#tree').tree('getSelected');//p+
		var organizationId = '';
		organizationId = $('#organizationId').val().trim();
		var dataModel = new Object();
		dataModel.parentShopCode = $('#parentShopCode').val().trim();
		dataModel.shopId = $('#shopIdInfo').val().trim();
		dataModel.shopCode = $('#shopCode').val().trim();
		dataModel.shopName = $('#shopName').val().trim();
		dataModel.abbreviation = $('#abbreviation').val().trim();
		dataModel.status = $('#status').val();
		dataModel.shopType = shopType;//p+
		if (organizationId != null) {
			dataModel.organizationId = organizationId;
		}
		dataModel.phoneNumber = $('#phoneNumber').val().trim();
		dataModel.mobileNumber = $('#mobileNumber').val().trim();
		dataModel.faxNumber = $('#faxNumber').val().trim();
		dataModel.email = $('#email').val().trim();
		dataModel.taxNumber = $('#taxNumber').val().trim();
		dataModel.addressBillTo = $('#addressBillTo').val().trim();
		dataModel.contactName = $('#contactName').val().trim();
		dataModel.addressShipTo = $('#addressShipTo').val().trim();
		dataModel.areaId = areaId;
		dataModel.address = $('#address').val().trim();
		dataModel.lat = $('#lat').val().trim();
		dataModel.lng = $('#lng').val().trim();
		UnitTreeCatalog._isChange=false;
		Utils.addOrSaveData(dataModel, "/catalog/unit-tree/addorupdate-shop", UnitTreeCatalog._xhrSave, 'errMsgAddOrUpdate',function(data){
			$('#shopIdInfo').val(data.shopId);
			if( data.shopId != undefined && data.shopId != null) {
				UnitTreeCatalog.loadUnitTree(data.shopId);
			}
			setTimeout(function(){
				$('#successMsg').html(msgCommon1).show();
				//UnitTreeCatalog.openCollapseTab();
				//UnitTreeCatalog.showCollapseTab(1);
				/*UnitTreeCatalog.showCollapseTab(0);
				UnitTreeCatalog.openCollapseTab();
				UnitTreeCatalog.showSearchScreen();*/
			/*	UnitTreeCatalog.showShopInfoScreen(data.parentShopId,data.shopId);
				UnitTreeCatalog.openCollapseTab();
				UnitTreeCatalog.showCollapseTab(0);*/
				//UnitTreeCatalog.showCollapseTab(-1, data.shopId,data.parentShopId);
			}, 2000);
			/*UnitTreeCatalog.openCollapseTab();
			UnitTreeCatalog.showShopInfoScreen(data.parentShopId,data.shopId);
			UnitTreeCatalog.showCollapseTab(0);*/
			
		},null,'.ReportCtnSection');
		return false;
	},
	loadAreaInfoAndMap:function(area) {
		$('#provinceName').val('');
		$('#districtName').val('');
		$('#precinctName').val('');
		if (area != undefined && area != null) {
			$('#provinceName').val(area.provinceName);
			$('#districtName').val(area.districtName);
			$('#precinctName').val(area.precinctName);
		}
		return false;
	},
	viewBigMap : function() {
		var params = new Object();
		params.lat = $('#lat').val().trim();
		params.lng = $('#lng').val().trim();
		//CommonSearchEasyUI.viewBigMap(params, catalog_unit_view_ban_do_don_vi); //'Bản đồ vị trí đơn vị'
		CommonSearchEasyUI.viewBigMapShopTree(params, catalog_unit_view_ban_do_don_vi); //'Bản đồ vị trí đơn vị'
	},
	//-------------------------------------------------------------------------------------------------------
		
	//NhanLT-------------------------------------------------------------------------------------------------
	/** vuongmq; 26/02/2015; Quay lai view danh sach staff*/
	showStaffSearchScreenBack: function(){
		$.messager.confirm(jsp_common_xacnhan, catalog_unit_view_thong_tin_nhan_vien_back, function(r){
			if (r){
				/*UnitTreeCatalog.showCollapseTab(0);
				UnitTreeCatalog.showTab2();
*/				//alert('confirm: chưa có tab nhân viên (khi nào có thì trả về trang này).OK TESTER');
				//UnitTreeCatalog.showStaffSearchScreen();
				//UnitTreeCatalog.showCollapseTab(1);
				//UnitTreeCatalog.openCollapseTab();
				UnitTreeCatalog.removeActiveCollapseTab();
				$('#collapseTab1').addClass('Active');
				UnitTreeCatalog.showSearchScreen();
			}
		});
		return false;
	},
	/** vuongmq; 26/02/2015; view danh sach staff*/
	showStaffSearchScreen: function(confirm){
		UnitTreeCatalog._isChange=false;
		var params = new Object();
		Utils.getHtmlDataByAjax(params, '/catalog/unit-tree/manage-staff',function(data) {
			$("#unitTreeContent").html(data).show();
			
			UnitTreeCatalog.resize();
		}, null, 'POST');
		return false;
	},
	/** vuongmq; 26/02/2015; goi ham view ban do cho thuoc tinh staff*/
	viewBigMapOnDlg : function(id) {
		var selector = $('#' + id);
		if(UnitTreeCatalog._isBkLatLng == false){
			//var latbk = '';
			//var lngbk = '';
			var latLngbk = selector.val().trim();
			if(!isNullOrEmpty(latLngbk) && latLngbk.indexOf(';')>=0){
				UnitTreeCatalog.latbk = latLngbk.split(';')[0];
				UnitTreeCatalog.lngbk = latLngbk.split(';')[1];
			}
			UnitTreeCatalog._isBkLatLng = true;		
		}
		//$('#viewBigMap').css('visibility','visible');
		var html = $('#viewBigMap').html();
		$('#viewBigMap').dialog({
			closed: false,  
			title : xem_ban_do,
			cache: false,  
			modal: true,
			onOpen: function(){
				var tm = setTimeout(function(){					
					var lat = '';
					var lng = '';
					var latLng = selector.val().trim();
					if(!isNullOrEmpty(latLng) && latLng.indexOf(';')>=0){
						lat = latLng.split(';')[0];
						lng = latLng.split(';')[1];
					}
					var zoom = 12;
					if(lat.trim().length==0 || lng.trim().length==0){
						zoom = ViettelMap._DEF_ALL_ZOOM;
					}
					VTMapUtil.loadMapResource(function(){
						ViettelMap.loadBigMapEx('bigMapContainer', lat, lng, zoom, function(latitude, longtitude) {
							selector.val(latitude + ';' + longtitude);
						}, true);
					});
				}, 1500);
				$('.easyui-dialog #imgBtnDeleteLatLng').unbind('click');
				$('.easyui-dialog #imgBtnDeleteLatLng').bind('click',function(){
					selector.val('');
					ViettelMap.clearOverlays();
					ViettelMap._listOverlay = new Array(); 
					if (ViettelMap._marker != null){
						ViettelMap._marker.setMap(null);
					}
					
				});
				$('.easyui-dialog #imgBtnUpdateLatLng').unbind('click');
				$('.easyui-dialog #imgBtnUpdateLatLng').bind('click',function(){
					var lat = '';
					var lng = '';
					var latLng = selector.val().trim();
					if(!isNullOrEmpty(latLng) && latLng.indexOf(';')>=0){
						lat = latLng.split(';')[0];
						lng = latLng.split(';')[1];
					}
					var zoom = 12;
					if(lat.trim().length==0 || lng.trim().length==0){
						zoom = ViettelMap._DEF_ALL_ZOOM;
					}
					VTMapUtil.loadMapResource(function(){
						ViettelMap.loadBigMapEx('bigMapContainer', lat, lng, zoom, function(latitude, longtitude) {
							selector.val(latitude + ';' + longtitude);
						}, true);
					});
					$('#viewBigMap').dialog('close');
				});
				$('.easyui-dialog #imgBtnCancelLatLng').unbind('click');
				$('.easyui-dialog #imgBtnCancelLatLng').bind('click',function(){
					var zoom = 12;
					if(UnitTreeCatalog.latbk.trim().length==0 || UnitTreeCatalog.lngbk.trim().length==0){
						zoom = ViettelMap._DEF_ALL_ZOOM;
					}
					VTMapUtil.loadMapResource(function(){
						ViettelMap.loadBigMapEx('bigMapContainer', UnitTreeCatalog.latbk, UnitTreeCatalog.lngbk, zoom, function(latitude, longtitude) {
							selector.val(latitude + ';' + longtitude);
						}, true);
					});
					//$('#viewBigMap').onClose();
				});
				
			},
			onClose:function() {
				$('#viewBigMap').html(html);
				//
			}
		});
		
	},
	/** vuongmq; 26/02/2015; goi ham view danh sach chi tiet staff; khi click  them moi tren don vi*/
	showStaffDetailScreen: function(nodeType,nodeTypeId, orgId, nameShop, shopId, staffId,callBack){
		var params = new Object();
		if(staffId != null){
			params.staffId = staffId;
		}
		Utils.getHtmlDataByAjax(params, '/catalog/unit-tree/view-staff',function(data) {
			$("#unitTreeContent").html(data).show();
			/**vuongmq; 27/02/2015; view Selected loai nhan vien*/
			$('#staffType').find('option[value=' +nodeTypeId+']').attr('selected','selected');
			/**vuongmq; 27/02/2015; view textbox nhan vien tren cay*/
			$('#staffShopCode').val(nameShop);
			$('#nodeTypeId').val(nodeTypeId);
			$('#nodeType').val(nodeType);
			$('#nodeShopId').val(shopId);
			$('#orgId').val(orgId);
			UnitTreeCatalog.resize();
			UnitTreeCatalog._typeScreen = -3;
		}, null, 'POST');
		return false;
	},
	showStaffCreateScreen: function(staffCreateType, shopCode, shopName, callBack){
		var params = new Object();
		params.staffCreateType = staffCreateType;
		params.shopCode = shopCode;
		params.shopName = shopName;
		Utils.getHtmlDataByAjax(params, '/catalog/unit-tree/view-staff', function(data) {
			$("#unitTreeContent").html(data).show();
			UnitTreeCatalog.resize();
			UnitTreeCatalog._typeScreen = -3;
		}, null, 'POST');
		return false;
	},
	/** vuongmq; 26/02/2015; goi ham view danh sach chi tiet staff; khi dubclick staff tren cay (edit nhan vien)*/
	showStaffEditScreen: function(staffId,shopId){
		var params = new Object();
		params.staffId = staffId;
		//params.staffObjectType = staffObjectType;
		if(shopId!=undefined && shopId!=null){
			params.shopId=shopId;
			//params.shId = shopId;
		}
		Utils.getHtmlDataByAjax(params, '/catalog/unit-tree/view-staff',function(data) {
			$("#unitTreeContent").html(data).show();
			UnitTreeCatalog.resize();
			UnitTreeCatalog._typeScreen = -3;
		}, null, 'POST');
		return false;
	},
	openPopupImportStaff : function(){
		$('#file').html('');
		$('.easyui-dialog #resultExcelMsg').html('').hide();
		$('#easyuiPopup').show();
		$('#errExcelMsg').html('').hide();
		ReportUtils._callBackAfterImport = function(){
			$('#gridStaffType').datagrid('load', {page : 1, shopId:$('#shopIdHidden').val()});
			ReportUtils._callBackAfterImport=null;
			hideLoadingIcon();
		};
		$('#easyuiPopup').dialog('open');
	},
	/**
	 * @author phuocdh2 upload shop
	 * @description ham import ds  don vi
	 */ 
	uploadShop : function(){ //upload trong fancybox
		$('#isViewShop').val(0);
		Utils._inputText = 'fakefilepc_shop';
		Utils.importExcelUtils(function(data){
			if(data.message!=undefined && data.message!=null && data.message.trim().length >0){
				$('#errExcelMsgShop').html(data.message.trim()).change().show();
				var tm = setTimeout(function(){
					$('#errExcelMsgShop').html(data.message.trim()).change().show();
					clearTimeout(tm);

				 }, 10000);
			}else{
				UnitTreeCatalog.loadUnitTree();
				$('#successMsgImportShop').html(msgCommon1).show();
				var tm = setTimeout(function(){
					$('#successMsgImportShop').html("").hide();
					clearTimeout(tm);
				 }, 4000);
			}
			$('#excelFileShop').val(""); //longnh15 add - xoa duong dan excel
			$('#fakefilepc_shop').val(""); //longnh15 add - xoa text
		}, 'importShopFrm', 'excelFileShop');
		
		return false;
	},
	upload : function(){ //upload trong fancybox
		$('#isViewStaff').val(0);
		Utils._inputText = 'fakefilepc_staff';
		Utils.importExcelUtils(function(data){
			if(data.message!=undefined && data.message!=null && data.message.trim().length >0){
				$('#errExcelMsg').html(data.message.trim()).change().show();
				var tm = setTimeout(function(){
					$('#errExcelMsg').html(data.message.trim()).change().show();
					clearTimeout(tm);
				 }, 10000);
			}else{
				UnitTreeCatalog.loadUnitTree();
				$('#successMsgImportStaff').html(msgCommon1).show();
				var tm = setTimeout(function(){
					$('#successMsgImportStaff').html("").hide();
					clearTimeout(tm);
				 }, 4000);
			}
		}, 'importFrm', 'excelFileStaff');
		return false;
	},
	
	search: function(independentSearch){
		$('#errMsg').html('').hide();
		if (independentSearch != 1) {
			var staffCode = $('#staffCode').val().trim();
			var staffName = $('#staffName').val().trim();
			var shopId = 0;
			var node = $('#tree').tree('getSelected');
			var status = $('#status').val();
			if (node != null) {
				if(node.attributes.shop!=null) shopId= node.attributes.shop.id;
				else if(node.attributes.staff!=null) shopId= node.attributes.staff.shop.id;
			}
			$('#searchStaffUnitTreeGrid').datagrid('load',{page : 1,shopId:shopId,status:status, staffCode: staffCode, staffName: staffName});
		} else {
			var staffCode = $('#staffCode').val().trim();
			var staffName = $('#staffName').val().trim();
			$('#searchStaffUnitGrid').datagrid('load',{page : 1, staffCode: staffCode, staffName: staffName});
		}
		return false;
	},
	exportExcel:function(){
		$('#errMsg').html('').hide();
		var shopId= 0;
		var staffCode = $('#staffCode').val();
		var staffName = $('#staffName').val();
		var status = activeType.parseValue($('#staffStatus').val().trim());
		var staffTypeName = $('#staffType').val();
		
		var params = new Object();
		//params.shopId = shopId;
		shopId = $('#manageShopStaff').combotree('getValue');
		params.shId = shopId;
		params.staffCode = staffCode;
		params.staffName = staffName;
		params.status = status;
		params.staffTypeName = staffTypeName;
		var url = "/catalog/unit-tree/export-excel";
		ReportUtils.exportReport(url, params, 'errExcelMsg');
		return false;
	},
	/** vuongmq; 26/02/2015; luu thong tin chi tiet staff*/
	changeStaffInfo:function(isEdit, staffId){
		var msg = '';
		$('.SuccessMsgStyle').html('').hide();
		$('.ErrorMsgStyle').html('').hide();
		//$('#errMsg').html('').hide();
		//$('errMsgStaff').html('').hide();
		////////////////////////////////////////////// thong tin co ban
		msg = Utils.getMessageOfRequireCheck('staffCode', catalog_staff_code); //'Mã nhân viên'
		if(msg.length==0){
			msg = Utils.getMessageOfSpecialCharactersValidate('staffCode', catalog_staff_code,Utils._CODE); //'Mã nhân viên'
		}	
		
		if(msg.length == 0){
			msg = Utils.getMessageOfRequireCheck('staffName', catalog_staff_name); //'Tên nhân viên'
		}		
		/*if(msg.length == 0){
			msg = Utils.getMessageOfSpecialCharactersValidate('staffName', catalog_staff_name,Utils._NAME); //'Tên nhân viên'
		}*/		
		if(msg.length == 0){
			msg = Utils.getMessageOfRequireCheck('staffShopCode', catalog_unit_tree,true); //'Đơn vị'
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfRequireCheck('staffType', catalog_staff_type,true); //'Loại nhân viên'
		}
		/*if(msg.length == 0){
			msg = Utils.getMessageOfRequireCheck('gender', catalog_gender_value,true); //'Giới tính'
		}*/
		if(msg.length == 0){
			if ($('#workStartDate').val().trim() != '' && !Utils.isDate($('#workStartDate').val().trim())) {
				//msg = 'Ngày bắt đầu làm việc không hợp lệ, vui lòng nhập theo định dạng dd/MM/yyyy';
				msg = catalog_unit_ngay_bat_dau_lam_viec_khong_hop_le;
				$('#workStartDate').focus();
			}
		}
		if (msg.length == 0 && $('#workStartDate').val().trim() != '' && !Utils.isDate($('#workStartDate').val().trim())) {
			//msg = 'Ngày bắt đầu làm việc không hợp lệ, vui lòng nhập theo định dạng dd/MM/yyyy';
			msg = catalog_unit_ngay_bat_dau_lam_viec_khong_hop_le;
			$('#workStartDate').focus();
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfRequireCheck('status', jsp_common_status,true); //'Trạng thái'
		}
		////////////////////////////////////////////// thong tin lien lac
		/*if(msg.length == 0){
			msg = Utils.getMessageOfSpecialCharactersValidate('staffPhone', catalog_mobilephone); //'Số di động'
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfSpecialCharactersValidate('staffTelephone', catalog_telephone); //'Số cố định'
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfInvaildNumber('staffPhone', catalog_mobilephone); //'Số di động'
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfInvaildNumber('staffTelephone', catalog_telephone); //'Số cố định'
		}*/
		if(msg.length == 0){
			msg = Utils.getMessageOfInvalidEmailFormat('email', catalog_email_code); //'Email'
		}
		/*if(msg.length == 0){
			var area = $('#areaId').val().trim();
			if (area == null || area == -2) {
				msg = 'Địa bàn là bắt buộc. Xin vui lòng chọn địa bàn';
				$('#areaTree').focus();
			}
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfSpecialCharactersValidate('provinceName','Tỉnh thành');
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfSpecialCharactersValidate('districtName','Quận/ Huyện');
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfSpecialCharactersValidate('precinctName','Phường/ Xã');
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfSpecialCharactersValidate('houseNumber','Số nhà, đường');
		}*/
		
		////////////////
		/*if(msg.length == 0){
			msg = Utils.getMessageOfSpecialCharactersValidate('street','Đường');
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfSpecialCharactersValidate('idPlace','Nơi cấp');
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfSpecialCharactersValidate('education','Trình độ');
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfSpecialCharactersValidate('position','Vị trí chức danh');
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfSpecialCharactersValidate('address','Địa chỉ');
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfSpecialCharactersValidate('houseNumber','Số nhà');
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfSpecialCharactersValidate('street','Đường');
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfSpecialCharactersValidate('idCard','CMND');
		}		
		if (msg.length == 0 && $('#idDate').val().trim() != '' && !Utils.isDate($('#idDate').val().trim())) {
			msg = 'Ngày cấp không hợp lệ, vui lòng nhập theo định dạng dd/MM/yyyy';
			$('#idDate').focus();
		}*/
		if(msg.length > 0){
			$('#errMsgStaff').html(msg).show();
			return false;
		}
		/*var multiStaff = $("#parentStaff").data("kendoMultiSelect");
		var lstParentStaffId=new Array();
		if (multiStaff != null) {
			var dataStaff = multiStaff.dataItems();
			if(dataStaff.length>0){
				for(var i = 0;i<dataStaff.length;i++){
					lstParentStaffId.push(dataStaff[i].id);
				}
			}
			
		}*/
		
		var dataModel = new Object();
		if (isEdit == 1) { // 1:cap nhat staff // 0:tao moi staff
			dataModel.staffId = staffId;
		}
		//dataModel.shopCode = 'VNM_COM';
		dataModel.nodeTypeId = $('#nodeTypeId').val();
		dataModel.nodeType = $('#nodeType').val();
		dataModel.shopId = $('#nodeShopId').val();
		dataModel.orgId = $('#orgId').val();

		dataModel.staffCode = $('#staffCode').val().trim();
		dataModel.staffName = $('#staffName').val().trim();				
		dataModel.gender = $('#gender').val();
		dataModel.workStartDate = $('#workStartDate').val().trim();
		dataModel.status = $('#status').val();

		dataModel.staffPhone = $('#staffPhone').val().trim();
		dataModel.staffTelephone = $('#staffTelephone').val().trim();
		dataModel.email = $('#email').val().trim();
		dataModel.areaId = $('#areaId').val().trim();
		dataModel.houseNumber = $('#houseNumber').val().trim();
		if ( $('#orderProduct').val() != undefined && $('#orderProduct').val() != null &&  $('#orderProduct').val() != '' ) {
			dataModel.orderProduct = $('#orderProduct').val();
		}	
		if ($('#manageSubStaff').combobox('getValues') != undefined && $('#manageSubStaff').combobox('getValues') != null  && $('#manageSubStaff').combobox('getValues') != '') {
			dataModel.subStaff = $('#manageSubStaff').combobox('getValues');
		}	
		/*dataModel.shopName = $('#staffShopName').val().trim();
		dataModel.idCard = $('#idCard').val().trim();
		dataModel.idDate = $('#idDate').val().trim();
		dataModel.idPlace = $('#idPlace').val().trim();
		dataModel.street = $('#street').val().trim();
		dataModel.address = $('#address').val().trim();
		dataModel.staffType = $('#staffType').val();
		dataModel.saleTypeCode = $('#saleType').val();
		dataModel.education = $('#education').val().trim();
		dataModel.position = $('#position').val().trim();
		dataModel.staffCreateType = staffCreateType;
		dataModel.lstParentStaffId=lstParentStaffId;*/
		
		/** luu thuoc tinh dong*/
		if(!Utils.validateAttributeData(dataModel, '#errMsgStaff', '#propertyTabContainer ')) {
			return false;
		}
		UnitTreeCatalog._isChange=false;
		Utils.addOrSaveData(dataModel, "/catalog/unit-tree/change-Staff-Info", UnitTreeCatalog._xhrSave, 'errMsgStaff',function(data){
			if(data.error != null && data.error == false){
				//$('#successMsgStaff').html('Lưu dữ liệu thành công').show();
				$('#successMsgStaff').html(msgCommon1).show();
				if (isEdit == 0) {
					//UnitTreeCatalog.loadUnitTree(data.shopId);
					//UnitTreeCatalog.showCollapseTab(3);
					//UnitTreeCatalog.loadUnitTree(data.shopId);
					//UnitTreeCatalog.showCollapseTab(3);
					UnitTreeCatalog.loadUnitTree(data.shopId, data.staff.id);
					/** them moi nhan vien xong chuyen ve la cap nhat nhan vien*/
					$('#btnUpdateStaff').attr('onclick', 'return UnitTreeCatalog.changeStaffInfo(1,' +data.staff.id+');');
					$('#staffCode').attr('disabled',true);
					UnitTreeCatalog.showStaffEditScreen(data.staffId, data.shopId);
				} else {
					UnitTreeCatalog.loadUnitTree(data.shopId, data.staff.id);
					/*setTimeout(function(){
						UnitTreeCatalog.resetStaff();
					}, 3000);*/
				}
						
				//UnitTreeCatalog.showSearchScreen(1);
			}
			
		},null,'#staff ');
	},
	
	loadTreeEx:function(areaIdOfShop,staffId){
		TreeUtils.loadComboTreeAreaForUnitTreeHasTitle('areaTree', 'areaId',areaIdOfShop,function(data) {
				UnitTreeCatalog.loadAreaInfoAndMap(data);
		});
	},
	resetStaff: function() {
		$('#staffShopCode').val('');		
		$('#staffShopName').val('');
		$('#staffId').val('');		
		$('#staffCode').val('');
		$('#staffName').val('');				
		$('#idCard').val('');
		$('#idDate').val('');
		$('#idPlace').val('');
		$('#provinceName').val('');
		$('#districtName').val('');
		$('#precintName').val('');
		$('#street').val('');
		$('#address').val('');
		$('#email').val('');
		$('#saleGroup').val('');
		$('#staffPhone').val('');
		$('#staffType').val(-1);
		$('#staffType').change();
		$('#workStartDate').val('');
		$('#education').val('');
		$('#position').val('');
		$('#successMsgStaff').hide();
		$('#staffTelephone').val('');	
		$('#gender').val(-1);
		$('#gender').change();
		$('#areaTree').combotree('reload');
		UnitTreeCatalog.showCollapseTab(0);
	},
	showArea:function(selector,type,codeValue){		
		var html = new Array();	
		html.push("<option value=''>" + Utils.XSSEncode(jsp_common_status_all) + "</option>"); //Tất cả
		var areaCode = $(selector).val().trim();		
		if(areaCode!=undefined && areaCode!=null && areaCode.trim().length==0){
			if($(selector).attr('id')=='provinceCode'){
				$('#districtCode').html(html.join(""));
				$('#wardCode').html(html.join("")); 
				$('#districtCode').val('');
				$('#wardCode').val('');
				disableSelectbox('districtCode');
				disableSelectbox('wardCode');
				
			}
			if($(selector).attr('id')=='districtCode'){
				$('#wardCode').html(html.join(""));
				$('#wardCode').val('');
				disableSelectbox('wardCode');
			}
			
			return false;
		}
		$.ajax({
			type : "POST",
			url : "/catalog_staff_manager/show-area",
			data : ({areaCode:areaCode}),
			dataType: "json",
			success : function(data) {				
				if(data!= null && data!= undefined && data.areas!= null && data.areas.length > 0){    				
					for(var i=0;i<data.areas.length;i++){    					
						html.push("<option value='" + Utils.XSSEncode(data.areas[i].areaCode) +"'>" + Utils.XSSEncode(data.areas[i].areaName) +"</option>");
					}
					if(type==1){
						enable('districtCode');
						$('#districtCode').parent().removeClass('BoxDisSelect');
					}else if(type==2){    					
						enable('wardCode');
						$('#wardCode').parent().removeClass('BoxDisSelect');
					}    				
				}    			
				if(type==1){    				
					$('#districtCode').html(html.join(""));  
					if(codeValue!=undefined && codeValue!=null && codeValue.trim().length>0){
						$('#districtCode').val(codeValue);
						disableSelectbox('districtCode');
					}
				}else if(type==2){    				
					$('#wardCode').html(html.join("")); 
					if(codeValue!=undefined && codeValue!=null && codeValue.trim().length>0){
						$('#wardCode').val(codeValue);
						disableSelectbox('wardCode');
					}     				    				
				}    			
			},
			error:function(XMLHttpRequest, textStatus, errorThrown) {				
			}
		});
	},
	//-------------------------------------------------------------------------------------------------------
	loadParentStaff:function(){
		var parentStaffId=$('#parentStaffId').val().trim();
		var lstParentStaff=[];
		if(parentStaffId!=''){
			eval('lstParentStaff=[' +parentStaffId+']');
		}
		$("#parentStaff").kendoMultiSelect({
			dataTextField: "staffCode",
			dataValueField: "staffId",
			filter: "contains",
			itemTemplate: function(data, e, s, h, q) {
				return Utils.XSSEncode(data.staffCode) + ' - ' + Utils.XSSEncode(data.staffName);
			},
			tagTemplate:  '#: data.staffCode #',
			change: function(e) {
			},
			dataSource: {
				transport: {
					read: {
						dataType: "json",
						url: "/rest/catalog/staff-unit-tree/kendo-ui-combobox.json"
					}
				}
			},
			value: lstParentStaff
		});

		var staffKendo = $("#parentStaff").data("kendoMultiSelect");
		staffKendo.wrapper.attr("id", "parentShop-wrapper");
	},
	/**
	 * download file template import nhan vien
	 * @author tuannd20
	 * @since 28/03/2015
	 */
	downloadImportStaffTemplateFile: function() {
		ReportUtils.exportReport('/catalog/unit-tree/download-import-staff-template-file?excelType=' +$('#excelType').val(), {}, 'errExcelMsg');
		return false;
	},
	/**
	 * imporrt file template import nhan vien
	 * @author hoanv25
	 * @since 14/08/2015
	 */
	openImportExcelType:function(){
		Utils._inputTextFileExcel = null;
		$('#popupImportUnit').dialog({
			title : 'Chọn loại import',
			width:250,
			height:'auto',
			onOpen: function(){
				
			}
		});
		$('#popupImportUnit').dialog('open');
	},
	/**
	 * imporrt file template import nhan vien
	 * @author hoanv25
	 * @since 14/08/2015
	 */
	importExcel : function() {
		var excelType = $('input:radio[name=importRadio]:checked').val();
	/*	Utils._currentSearchCallback = function(){
			KeyShop.searchKS();
		};*/
	/*	Utils._inputTextFileExcel = "excelFileShop";*/
		var options = {
			beforeSubmit : Utils.beforeImportExcel,
			success : Utils.afterImportExcelNew,
			type : "POST",
			dataType : 'html',
			data : { excelType : excelType }
		};
		$('#importFrm').ajaxForm(options);
		$('#importFrm').submit();
		$('#popupImportUnit').dialog('close');
		return false;
	},
	/**
	 * download file template import nhan vien
	 * @author tuannd20
	 * @since 28/03/2015
	 */
	downloadImportShopTemplateFile: function() {
		var url = "/catalog/unit-tree/download-import-shop-template-file";
		ReportUtils.exportReport(url, {}, 'errExcelMsg');
		return false;
	},
	initImport:function() {
		$('#downloadTemplate2').attr('href', 'javascript:void(0);');
		Utils._errExcelMsg = 'errExcelMsg';
		var options = {
			beforeSubmit : Utils.beforeImportExcel,
			success : Utils.afterImportExcelNew,
			type : "POST",
			dataType : 'html',
			data : { excelType : $('#excelType').val() }
		};
		$('#importFrm').ajaxForm(options);
		$('#excelFile').val('');
		$('#fakefilepc_staff').val('');
	},
	excelTypeChanged:function() {
		$('#excelFile').val('');
		$('#fakefilepc_staff').val('');
	},

	/**
	 * change pass popup user khi da dang nhap thanh cong
	 * @author vuongmq
	 * @params typeLogin (dng nhap pass mac dinh doi mat khau hoac doi mat khau)
	 * @since 25/08/2015 
	 */
	changePassPopupUser: function(typeLogin) {
		$('.ErrorMsgStyle').html('').hide();
		var msg = '';
		if (msg.length == 0) {
			msg = Utils.getMessageOfRequireCheck('seachStyle1PassOld', 'Mật khẩu cũ');
			cus = 'seachStyle1PassOld';
		}
		if (msg.length == 0) {
			msg = Utils.getMessageOfRequireCheck('seachStyle1PassNew', 'Mật khẩu mới');
			cus = 'seachStyle1PassNew';
		}
		if (msg.length == 0) {
			msg = Utils.getMessageOfPolicyPassWord('seachStyle1PassNew', 'Mật khẩu mới', UnitTreeCatalog._MAX_LENGTH_6);
			cus = 'seachStyle1PassNew';
		}
		if (msg.length == 0) {
			msg = Utils.getMessageOfRequireCheck('seachStyle1PassRetype', 'Nhập lại mật khẩu mới');
			cus = 'seachStyle1PassRetype';
		}
		if (msg.length == 0) {
			msg = Utils.getMessageOfPolicyPassWord('seachStyle1PassRetype', 'Nhập lại mật khẩu mới', UnitTreeCatalog._MAX_LENGTH_6);
			cus = 'seachStyle1PassRetype';
		}
		if (msg.length > 0) {
			$('#errMsgChangePass').html(msg).show();
			$('#' + cus).focus();
			return false;
		}
		var params = new Object();
		params.password = $('#searchStyle1EasyUIDialogChangePass #seachStyle1PassOld').val();
		params.newPass = $('#searchStyle1EasyUIDialogChangePass #seachStyle1PassNew').val();
		params.retypedPass = $('#searchStyle1EasyUIDialogChangePass #seachStyle1PassRetype').val();
		var url = '/changePassword';
		Utils.saveData(params, url, null, 'errMsgChangePass', function(data) {
			if (data.error != null && data.error == false && data.errMsg != undefined && data.errMsg != "") {
				$('#errMsgChangePass').html(data.errMsg).show();
			} else {
				$('#successMsgChangePass').html('Đổi mật khẩu thành công !').show();
				setTimeout(function() {
					$('#successMsgChangePass').html('').hide();
					if (typeLogin != undefined && typeLogin != undefined && typeLogin == StatusType.ACTIVE) {
						window.location.assign(window.location.origin + '/home');
						$('#searchStyle1EasyUIDialogChangePassDivChangePass').css("visibility", "hidden");
					} else {
						$('.easyui-dialog').dialog('close');
					}
				}, 2500);
			}
		}, null, null, null, null);
	},

	/**
	 * Open dia log change pass default
	 * @author vuongmq
	 * @since 30/10/2015 
	 */
	openDialogChangePassDefault: function() {
		$('.ErrorMsgStyle').html('').hide();
		$('#searchStyle1EasyUIDialogChangePassDivChangePass').css('visibility','visible');
		$('#searchStyle1EasyUIDialogChangePass').dialog({
			title: 'Đổi mật khẩu',
			width: 426,
			height: 280,
			closed: false,
			cache: false,
			modal: true,
			onOpen: function() {
				$('#searchStyle1EasyUIDialogChangePass #seachStyle1PassOld').focus();
				$('#searchStyle1EasyUIDialogChangePass #seachStyle1PassOld').val('');
				$('#searchStyle1EasyUIDialogChangePass #seachStyle1PassNew').val('');
				$('#searchStyle1EasyUIDialogChangePass #seachStyle1PassRetype').val('');
				//$('.easyui-dialog #seachStyle1Code')
				$(window).bind('keyup', function(event) {
					if (event.keyCode == 13) {
						$('#__btnSaveChangePass').click();
					}
				});
				$('#__btnSaveChangePass').attr('onclick', 'return UnitTreeCatalog.changePassPopupUser('+ StatusType.ACTIVE +');');
				$('#__btnCancelChangePass').html('Thoát');
				$('#__btnCancelChangePass').attr('onclick', 'return General.logoutSystem();');
				UnitTreeCatalog._flagNext = false;
			},
			onClose: function() {
	        	$('.ErrorMsgStyle').html('').hide();
	        	if (!UnitTreeCatalog._flagNext) {
	        		setTimeout(function() {
	        			UnitTreeCatalog.openDialogChangePassDefault();
	    			}, 500);
	        	}
	        }
		});
	},
};
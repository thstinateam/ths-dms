var ProgramDocument = {
	_listShopId:null,
	_listShopIdMien:null,
	_listShopIdCheck:null,
	_listFull:null,
	_listQuantity:null,
	_arrParams : null,
	_demtang : null,
	search:function(){
		var typeDocument = $('#typeDocument').val().trim();
		var msg = '';
		var err = '';
		var documentCode =$('#documentCode').val().trim();
		var documentName = $('#documentName').val().trim();
		var fromDate = $('#fromDate').val().trim();
		var toDate =$('#toDate').val().trim();
		
		if(msg.length == 0 && $('#fromDate').val().trim().length>0 && !Utils.isDate($('#fromDate').val(),'/')){
			msg = 'Từ ngày không tồn tại hoặc không đúng định dạng dd/mm/yyyy.';
			err = '#fromDate';
		}
		if(msg.length == 0 && $('#toDate').val().trim().length>0 && !Utils.isDate($('#toDate').val(),'/')){
			msg = 'Đến ngày không tồn tại hoặc không đúng định dạng dd/mm/yyyy.';
			err = '#toDate';
		}
		if(msg.length==0 && fromDate.length>0 && toDate.length>0 && !Utils.compareDate(fromDate, toDate)){
			msg = 'Từ ngày phải là ngày trước hoặc cùng ngày với đến ngày';
		}
		if(msg.length>0){
			$('#errMsg').html(msg).show();
			$(err).focus();
			setTimeout(function(){$('#errMsg').hide();},3000);
			return false;
		}
		
		$('#typeDocumentHide').val($('#typeDocument').val().trim());
		$('#documentCodeHide').val($('#documentCode').val().trim());
		$('#documentNameHide').val($('#documentName').val().trim());
		$('#fromDateHide').val($('#fromDate').val().trim());
		$('#toDateHide').val($('#toDate').val().trim());
		
		$('#dg').datagrid('load',{
			typeDocument:typeDocument,
			/*documentCode:encodeChar(documentCode),
			documentName:encodeChar(documentName),*/
			documentCode: documentCode,
			documentName: documentName,
			fromDate:fromDate,
			toDate:toDate
		});
		return false;
	},
	
	exportExcelOfficeDocument:function(){		
		var typeDocument = $('#typeDocumentHide').val().trim();
		var documentCode =$('#documentCodeHide').val().trim();
		var documentName = $('#documentNameHide').val().trim();
		var fromDate = $('#fromDateHide').val().trim();
		var toDate =$('#toDateHide').val().trim();
		$.messager.confirm('Xác nhận','Bạn có muốn xuất Excel không?',function(r){
			if(r){
				var params = new Object();
				params.typeDocument = typeDocument;
				params.documentCode = documentCode;
				params.documentName = documentName;
				params.fromDate = fromDate;
				params.toDate = toDate;
				
				var url = '/document/exportExcelOficedocument';
				ProgramDocument.exportExcelData(params,url,'errExcelMsg');
				return false; 
			}
		});	
		return false;
	},
	importPDFFile: function(){
		var msg = '';
		var err = '';
		
		//Check is Null
		if(msg.length == 0){
			msg = Utils.getMessageOfRequireCheck('documentCodeDlg','Mã CV');
			err = '#documentCodeDlg';
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfRequireCheck('documentNameDlg','Tên CV');
			err = '#documentNameDlg';
		}
		if(msg.length == 0 && $('#typeDocumentDlg').val().trim().length >0){
			var typeInt = parseInt($('#typeDocumentDlg').val().trim());
			if( typeInt == -2){
				msg = 'Chưa chọn loại CV';
				err = '#typeDocumentDlg';
			}
		}
		
		if(msg.length == 0 && $('#fromDateDlg').val().trim().length>0){
			msg = Utils.getMessageOfEmptyDate('fromDateDlg', 'Từ ngày');
			err = '#fromDateDlg';
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfRequireCheck('fromDateDlg','Từ ngày');
			err = '#fromDateDlg';
		}
		if(msg.length == 0 && $('#fromDateDlg').val().trim().length>0 && !Utils.isDate($('#fromDateDlg').val(),'/')){
			msg = 'Từ ngày không tồn tại hoặc không đúng định dạng dd/mm/yyyy.';
			err = '#fromDateDlg';
		}
		if(msg.length == 0 && $('#fromDateDlg').val().trim().length>0){
			msg = Utils.getMessageOfInvalidFormatDate('fromDateDlg', 'Từ ngày');
			err = '#fromDateDlg';
		}
		
		if(msg.length == 0 && $('#toDateDlg').val().trim().length>0 && !Utils.isDate($('#toDateDlg').val(),'/')){
			msg = 'Đến ngày không tồn tại hoặc không đúng định dạng dd/mm/yyyy.';
			err = '#toDateDlg';
		}
		
		if(msg.length == 0 && $('#toDateDlg').val().trim().length>0){
			msg = Utils.getMessageOfInvalidFormatDate('fromDateDlg', 'Đến ngày');
			err = '#toDateDlg';
		}
		
		if(msg.length == 0 && $('#fromDateDlg').val()!=null && $('#toDateDlg').val()!=null){
			if(!Utils.compareDate($('#fromDateDlg').val().trim(), $('#toDateDlg').val().trim())){
				msg = 'Từ ngày phải nhỏ hơn ngày đến ngày';
				err = '#fromDateDlg';
			}
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfRequireCheck('fakePdffilepc','Chọn File');
			if(msg.length>0){
				msg = 'Chưa chọn tập tin PDF';
			}
			else{
				var str = $('#fakePdffilepc').val().trim();
				var n = str.substring(str.length-3,str.length).toUpperCase();
				if(n!=='PDF'){
					msg = 'Không đúng loại định dạng PDF';
				}
			}
			
		}
		//Check validate
		if(msg.length == 0){
			msg = Utils.getMessageOfSpecialCharactersValidateEx1('#documentCodeDlg','Mã CV',Utils._CODE);
			err = '#documentCodeDlg';
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfSpecialCharactersValidateEx1('#documentNameDlg','Tên CV',Utils._NAME);
			err = '#documentNameDlg';
		}
		if(msg.length>0){
			$('#errPdfMsg').html(msg).show();
			$(err).focus();
			setTimeout(function(){$('#errPdfMsg').hide();},4000);
			return false;
		}
		
		if(!uploadPdfFile(document.getElementById("pdfFile"))){
			return false;
		}
		
		var options = {
				beforeSubmit : ProgramDocument.beforeUploadPdf,
				success : ProgramDocument.afterImportExcel,
				type : "POST",
				dataType : 'html'
			};
			$('#easyuiPopupUpload #importFrm').ajaxForm(options);
			$.messager.confirm('Xác nhận', 'Bạn có muốn lưu thông tin này ?',	function(r) {
				if (r) {
					try{
						$('#importFrm').submit();}
					catch(e) {
					}
				}
			});
 		return false;
	},
	deleteOfficeDocument:function(id){
		$.messager.confirm('Xác nhận', 'Bạn có muốn xoá công văn không?', function(r){
			if (r){
				var param = new Object();
				param.idOfficeDoc = id;
				Utils.saveData(param, '/document/delete-OfficeDocument', null, 'errMsg', function(result){
					$('#dg').datagrid('reload');
					$('#successExcelMsg').html('Xóa dữ liệu thành công').show();
					setTimeout(function(){
						$('#successExcelMsg').html('');
						$('#successExcelMsg').hide();
		    		},2000);
				});
			}
		});
	},
	importPdfFileAction:function(dataModel,url,errMsg,typeErr){
		var kData = $.param(dataModel,true);
		if(CommonSearch._xhrReport != null){
			CommonSearch._xhrReport.abort();
			CommonSearch._xhrReport = null;
		}
		$('#divOverlay').show();
		CommonSearch._xhrReport = $.ajax({
			type : "POST",
			url : url,
			data: (kData),
			dataType: "json",
			success : function(data) {
				$('#divOverlay').hide();
				if(data.error && data.errMsg!= undefined){
					$('#' + errMsg).html(Utils.XSSEncode(data.errMsg)).show();
					setTimeout(function(){
						$('#' + errMsg).hide();
					}, 4000);
				} else{
					if(data.hasData!= undefined && !data.hasData) {
						$('#errPdfMsg').html('Không tải được tập tin').show();
						setTimeout(function(){
							$('#errPdfMsg').hide();
						},3500);
					} else {
						$('#resultPdfMsg').html('Lưu dữ liệu thành công').show();
						$('#dg').datagrid('reload');
						setTimeout(function() { // Set timeout để
							$('#resultPdfMsg').hide();
							$('#easyuiPopupUpload').dialog('close');
						},2000);
					}
				}
			},
			error:function(XMLHttpRequest, textStatus, errorDivThrown) {
				$.ajax({
					type : "POST",
					url : '/check-session',
					dataType : "json",
					success : function(data) {
						CommonSearch._xhrReport = null;
					},
					error: function(XMLHttpRequest, textStatus, errorThrown) {
						window.location.href = '/home';
					}
				});
			}
		});
		return false;
	},
	viewDetail:function(documentId){
//		var html = $('#easyuiPopupUpload').html();
		var params = new Object();
		var width = 800;
		
		params.idOfficeDoc = documentId;
		Utils.getJSONDataByAjax(params, '/document/viewDetail-OfficeDocument',function(data) {
			if(data.widthImage > width && data.widthImage<980){
				width =data.widthImage + 20; 
			}
			if(data.urlImage!=null && data.urlImage!= undefined && data.urlImage.length>0){
				$('#imgDocument').attr('src',data.urlImage);
			} else{
//				$('#easyuiPopup').dialog('close');
			}
		}, null, 'POST');
		$('#easyuiPopup').dialog({  
	        closed: false,  
	        cache: false,
	        modal: true,
	        width:width,
	        height:600,
	        onOpen: function(){	        	
				$('#rotateButton').unbind('click');
				$('#rotateButton').bind('click', function() {
					ProgramDocument.rotateDoc(documentId);
				});
	        },
	        buttons:'#buttonViewDocument',
			onClose:function(){
	        	$('#contentImgDocument').html('<img id="imgDocument" src="" />');
	        }
		});
	},
	rotateDoc: function(docId) {
		var url = '/document/rotate-doc';
		var params = new Object();
		params.docId = docId;
		kData = $.param(params);
		$.ajax({
			type : "POST",
			url : url,
			data :(kData),
			dataType : "json",
			success : function(data) {
				var width = 800;
				var curDate = new Date;
				var curTime = curDate.getTime();
				if(data.widthImage > width && data.widthImage<980){
					width =data.widthImage + 20; 
				}
				if(data.urlImage!=null && data.urlImage!= undefined && data.urlImage.length>0){
					$('#imgDocument').attr('src',data.urlImage+'?v=' +curTime);
				}
				$('#easyuiPopup').dialog({width : width});
			},
			error:function(XMLHttpRequest, textStatus, errorThrown) {
				$.ajax({
					type : "POST",
					url : '/check-session',
					dataType : "json",
					success : function(data, textStatus, jqXHR) {
					},
					error: function(XMLHttpRequest, textStatus, errorThrown) {
						window.location.href = '/home';
					}
				});
			}
		});
	},
	deleteRow:function(){
		return false;
	},
	exportExcel:function(){
		
	},
	uploadDoc:function(){	
		$('#easyuiPopupUpload').css("visibility", "visible");
		var html = $('#easyuiPopupUpload').html();
		
		ProgramDocument.refreshPopupUpload();
		$('#easyuiPopupUpload').dialog({  
	        closed: false,  
	        cache: false,  
	        modal: true,
	        width:550,
	        height:300,
	        onOpen: function(){
	        	$('#documentCodeDlg').focus();
	        	setDateTimePicker('fromDateDlg'); 
	        	setDateTimePicker('toDateDlg');
	        	//ReportUtils.setCurrentDateForCalendar('fromDateDlg');
	        	//ReportUtils.setCurrentDateForCalendar('toDateDlg');
	        },onClose:function(){
	        	$('#easyuiPopupUpload').html(html);
	        	$('#easyuiPopupUpload').css("visibility", "hidden");
	        	//$('#documentCodeDlg').focus();
	        	$('#documentCodeDlg').val('').show();
	        	$('#documentNameDlg').val('').show();
	        	$('#fromDateDlg').val('').show();
	        	$('#toDateDlg').val('').show();
	        	$('#fakePdffilepc').val('').show();
	        }
		});
	},	
	refreshPopupUpload : function(){
		var typeValue = -2;
		$('#documentCodeDlg').val('').show();
		$('#documentNameDlg').val('').show();
		$('#typeDocumentDlg').find("option[value=" + typeValue +']').attr('selected','selected');
//		$('#fromDateDlg').val(ProgramDocument.currentDate()).show();
//		$('#toDateDlg').val(ProgramDocument.currentDate()).show();
		$('#fromDateDlg').val('').show();
		$('#toDateDlg').val('').show();
		$('#fakePdffilepc').val('').show();
		$('#errPdfMsg').hide();
		$('#resultPdfMsg').hide();
//		$('#fakePdffilepc').attr('disabled','disabled');
		$('#documentNameDlg').focus();
	},
	uploadExecute : function() {
		var options = {
			beforeSubmit : ReportUtils.beforeImportExcel,
			success : ReportUtils.afterImportExcel,
			type : "POST",
			dataType : 'html'
		};
		$('#easyuiPopupUpload #importFrm').ajaxForm(options);
		$.messager.confirm('Xác nhận', 'Bạn có muốn lưu thông tin này ?',	function(r) {
			if (r) {
				$('#easyuiPopupUpload #importFrm').submit();
			}
		});
	},
	beforeUploadPdf:function(){
//		if(!uploadPdfFile(document.getElementById("pdfFile"))){
//			return false;
//		}
		try{
			$('#checkSubmit').val(1);
			$('#errPdfMsg').html('').hide();
			$('#resultPdfMsg').html('').hide();		
			$('#divOverlay').show();
		}catch(e){}
	},
	afterImportExcel: function(responseText, statusText, xhr, $form){
		$('#divOverlay').hide();
		if (statusText == 'success') {
			$("#responseDiv").html(responseText);
			updateTokenImport();
			var errMsg = $('#sverrMsg').val().trim();
			if(errMsg!=undefined && errMsg.length>0){
				$('#errPdfMsg').html(errMsg).show();
				setTimeout(function(){$('#errPdfMsg').hide();},3500);
				return false;
			}
			else{
				$('#resultPdfMsg').html('Lưu dữ liệu thành công').show();
				$('#dg').datagrid('reload');			
				setTimeout(function(){
					$('#resultPdfMsg').hide();
					$('#easyuiPopupUpload').dialog('close');
				},4000);				
			}
			
		}
	},
	closeWindow:function(){
		$('#easyuiPopupUpload').dialog('close');
	},	
	currentDate:function() {
		var now = new Date();
		var cYear = now.getFullYear();
		var cMonth = now.getMonth() + 1;
		var cDate = now.getDate();
		var currentDate = cDate+'/' +cMonth+'/' +cYear;
		return currentDate;
	},
	
	onchangeType:function() {
		$('#fromDateDlg').focus();
		return false;
	},
	
	exportExcelData:function(dataModel,url,errMsg){
		if(errMsg == null || errMsg == undefined){
			errMsg = 'errExcelMsg';
		}
		var kData = $.param(dataModel,true);
		if(CommonSearch._xhrReport != null){
			CommonSearch._xhrReport.abort();
			CommonSearch._xhrReport = null;
		}
		CommonSearch._xhrReport = $.ajax({
			type : "POST",
			url : url,
			data: (kData),
			dataType: "json",
			success : function(data) {
				hideLoadingIcon();
				//window.location.href = data.view;
				if(data.error!=false && data.errMsg!= undefined){
					$('#errExcelMsg').html('Xuất báo cáo không thành công. Lỗi: ' + Utils.XSSEncode(data.errMsg)).show();
					setTimeout(function(){$('#errExcelMsg').hide();},3000);
				} else{
					if(data.hasData!= undefined && data.hasData!=true) {
						$('#errExcelMsg').html('Không có dữ liệu để xuất báo cáo').show();
						setTimeout(function(){$('#errExcelMsg').hide();},3000);
					} else{
						window.location.href = data.view;
					}
				}
			},
			error:function(XMLHttpRequest, textStatus, errorDivThrown) {
				$.ajax({
					type : "POST",
					url : '/check-session',
					dataType : "json",
					success : function(data) {
						CommonSearch._xhrReport = null;
					},
					error: function(XMLHttpRequest, textStatus, errorThrown) {
						window.location.href = '/home';
					}
				});
			}
		});
		return false;
	},
	editOfficeDocumentShopMap:function(id){
//		var arrParam = new Array();
		ProgramDocument._listShopId = new Array();
		ProgramDocument._listShopIdCheck = new Array();
		ProgramDocument._listFull = new Array();
		ProgramDocument._listShopIdMien = new Array();
		ProgramDocument._demtang = 0;
		var shopCode = $('#seachStyleShopCode').val().trim();
		var shopName = $('#seachStyleShopName').val().trim();
// 		var param3 = new Object();
// 		param3.promotionProgramCode = shopCode;
// 		param3.promotionProgramName = shopName;
// 		param3.idOfficeDoc = id;
 		$('#idDocumentIDGrid').val(id);
// 		arrParam.push(param3);
 		ProgramDocument.searchShopDialog(function(data){});
 		ProgramDocument.bindFormat();
 		
 		
	},
	searchShopDialog:function(callback) {
		ProgramDocument.openDialogSearchShop("Mã đơn vị",
				"Tên đơn vị", "Chọn đơn vị",
				"/document/searchShopDialog", callback, "promotionProgramCode",
				"promotionProgramName",'searchStyleShopDisplay','searchStyleShopDisplay1','display');
		return false;
	},
	bindFormat:function(){
		$('#searchStyleShopContainerGrid input[type=text]').each(function(){
				Utils.bindFormatOnTextfieldEx1($(this).attr('id'),Utils._TF_NUMBER,null,'#errMsgDialog','Số suất phải là số nguyên dương.');
			}); 
	},
	openDialogSearchShop : function(codeText, nameText, title, url, callback, codeFieldText, nameFieldText,searchDiv,searchDialog,program) {
		CommonSearchEasyUI._arrParams = null;
		CommonSearchEasyUI._currentSearchCallback = null;
		var html = $('#' +searchDiv).html();
		$('#' +searchDialog).dialog({  
	        title: title,  
	        closed: false,  
	        cache: false,  
	        modal: true,
	        width : 616,
	        height :'auto',
	        onOpen: function(){
	        	Utils.bindAutoSearch();
				$('.easyui-dialog #seachStyleShopCode').focus();
				$('.easyui-dialog #searchStyleShopUrl').val(url);
				$('.easyui-dialog #searchStyleShopCodeText').val(codeFieldText);
				$('.easyui-dialog #searchStyleShopNameText').val(nameFieldText);
				CommonSearch._currentSearchCallback = callback;
				// CommonSearch._arrParams = arrParam;
				//$('#loadingTree').show();
				$('#searchStyleShopContainerGrid').html('<table id="searchStyleShopGrid" class="easyui-treegrid"></table>').show();
				var codeField = 'code';
				var nameField = 'name';
				if (codeFieldText != null && codeFieldText != undefined && codeFieldText.length > 0) {
					codeField = codeFieldText;
				}
				if (nameFieldText != null&& nameFieldText != undefined && nameFieldText.length > 0) {
					nameField = nameFieldText;
				}
				
				var shopCode = $('#seachStyleShopCode').val().trim();
				var shopName = $('#seachStyleShopName').val().trim();
				var uRL = ProgramDocument.getSearchStyleDocShopMapGridUrl('/document/searchShopDialog',shopCode,shopName);
				$('.easyui-dialog #seachStyleShopCodeLabel').html(codeText);
				$('.easyui-dialog #seachStyleShopNameLabel').html(nameText);
				$('.easyui-dialog #searchStyleShopGrid').show();
				$('.easyui-dialog #searchStyleShopGrid').treegrid({  
				    url:  uRL,
				    width:($('#searchStyleShopContainerGrid').width()-20),  
			        height:400,
			        fitColumns : true,
			        idField: 'id',
			        treeField: 'data',
				    columns:[[  
				        {field:'data',title:'Đơn vị',resizable:false,width:530, formatter: function(data){
				        	return Utils.XSSEncode(data);
				        }}, 
				        {field:'edit',title:'',width:30,align:'center',resizable:false,formatter:function(value,row,index){
				        	var classParent = "";
				        	var parentNodeG = $('#shopIDAd').val();
				        	var parentIDTemp = null;
				        	if(row.parentId != null && row.parentId != undefined){
				        		parentIDTemp = row.parentId;
				        		classParent = "class_" +row.parentId;
				        		if(row.parentId == parentNodeG){
				        			ProgramDocument._listShopIdMien.push(row.id);
				        		}
				        	}
				        	if(row.isJoin==1 || (row.isChildJoin==1 && row.isJoin>1)){
				        		ProgramDocument._listShopId.push(row.id);
				        		return '<input type ="checkbox" class="' +Utils.XSSEncode(classParent)+'" checked="true" value="' +row.id+'" id="check_' +row.id+'" onclick="return ProgramDocument.onCheckShop(' +row.id+',' + parentIDTemp+');">';
				        	}
				        	return '<input type ="checkbox" class="' +Utils.XSSEncode(classParent)+'" value="' +row.id+'" id="check_' +row.id+'" onclick="return ProgramDocument.onCheckShop(' +row.id+',' + parentIDTemp+');">';
				        }}
				    ]],
				    onLoadSuccess:function(row,data){
				    	$(window).resize();
				    	if(ProgramDocument._listShopIdMien!=null && ProgramDocument._listShopIdMien.length>0){
				    		for(var i=0; i< ProgramDocument._listShopIdMien.length; i++){
				    			ProgramDocument.oncheckedClickMouse(ProgramDocument._listShopIdMien[i]);
				    		}
				    	}
				    	//Uncheck NodeG
				    	var idShopG = $('#shopIDAd').val().trim();
				    	if(idShopG!=undefined && idShopG!=null){
				    		$('.easyui-dialog #check_' +idShopG).removeAttr('checked');
				    	}
				    	$(this).treegrid('expandAll');
				    }
				});
				$('.easyui-dialog #btnSearchStyleShop').bind('click',function(event) {
						$('.easyui-dialog #errMsgDialog').html('').hide();
						var code = $('.easyui-dialog #seachStyleShopCode').val().trim();
						var name = $('.easyui-dialog #seachStyleShopName').val().trim();
						var url = '/document/searchShopDialog';
						if(ProgramDocument._listShopId != null && ProgramDocument._listShopId != undefined && ProgramDocument._listShopId.length > 0){
							var listSize = ProgramDocument._listShopId.length;
							for(var i=0;i<listSize;i++){
								var value = $('.easyui-dialog #txtQuantity_' +ProgramDocument._listShopId[i]).val();
								if(value != null && value != undefined){
									ProgramDocument._listQuantity.push(value);	
								}
							}
						}
						var shopCode = $('#seachStyleShopCode').val().trim();
						var shopName = $('#seachStyleShopName').val().trim();
						var idOfficeDoc = $('#idDocumentIDGrid').val().trim();
						var arrParam = new Array();
						var param = new Object();
				 		param.promotionProgramCode = shopCode;
				 		param.promotionProgramName = shopName;
				 		param.idOfficeDoc = idOfficeDoc;
				 		arrParam.push(param);
				 		var urlSearch =  ProgramDocument.getSearchStyleDocShopMapGridUrl('/document/searchShopDialog', shopCode, shopName);
				 		$('.easyui-dialog #searchStyleShopGrid').treegrid({url:urlSearch});
				 		
				});
//				setTimeout(function(){$('#loadingTree').hide();},1000);
				
	        },
	        onBeforeClose: function() {
				var tabindex = 1;
				$(' .InputTextStyle, select, input, button , li , tr, td, th, label, ul, a, img').each(function () {
					if (this.type != 'hidden') {
						$(this).attr("tabindex", '');
					}
					tabindex ++;
				});	
				var curIdFocus = $('#cur_focus').val();
				$('#' + curIdFocus).focus();				
	        },
	        onClose : function(){
	        	$('#' +searchDiv).html(html);
	        	$('.easyui-dialog #seachStyleShopCode').val('');
	        	$('.easyui-dialog #seachStyleShopName').val('');
	        	$('.easyui-dialog #errMsgDialog').html('').hide();
	        	ProgramDocument._listShopId = new Array();
	        	ProgramDocument._listQuantity = new Array();
	        	ProgramDocument._listShopIdMien = new Array();
	        }
	    });
		return false;
	},
	
	onCheckShop:function(id, parentId){
		if(parentId==null || parentId==undefined){
			parentId = $('#shopIDAd').val();
		}
		 var isCheck = $('.easyui-dialog #check_' +id).attr('checked');
		    if($('.easyui-dialog #check_' +id).is(':checked')){
		    	
		    	var flag = false;
				for(var j=0;j<ProgramDocument._listShopId.length;j++){
					if(ProgramDocument._listShopId[j] == id){
						flag = true;
						break;
					}
				}
				if(flag!=true){
					ProgramDocument._listShopId.push(id);
				}
		    	
		    	var classParent = $('.easyui-dialog #check_' +id).attr('class');
				var count = 0;
				ProgramDocument.recursionFindChildShopCheck(id);
				ProgramDocument.oncheckedClickMouse(parentId);
			}else{
				ProgramDocument.removeArrayByValue(ProgramDocument._listShopId,id);
				var classParent = $('.easyui-dialog #check_' +id).attr('class');
				var flag = true;
				var count = 0;
				ProgramDocument.recursionFindChildShopUnCheck(id); 
				$('.easyui-dialog #check_' +parentId).removeAttr('checked');
			}
	},
	
	oncheckedClickMouse:function(parentId){
		var rows = $('.easyui-dialog .class_' +parentId);
		var flag =1;
		for(var i=0; i<rows.length; i++){
			if(rows[i].checked!=true){
				flag =0;
				break;
			}
		}
		if(flag!=0){
			$('.easyui-dialog #check_' +parentId).attr('checked', 'checked');
		}
	},
	recursionFindChildShopCheck:function(id){
		if(id == null || id == undefined){
			return;
		}
    	$.ajax({
    		type : "POST",
			url : '/document/getChildShopId',
			data: ({shopId:id}),
			dataType: "json",
			success : function(data) {
				if(data != null && data != undefined && data.listShopId != null && data.listShopId != undefined){
					var length = data.length;
					var arrayShopId = data.listShopId;
					if(ProgramDocument._listShopId==null || ProgramDocument._listShopId.length <=0){
						for(var i=0;i<length;i++){
								ProgramDocument._listShopId.push(data.listShopId[i]);
						}
					}else{
						//Thuc hien kiem tra truoc khi them moi
						for(var i=0;i<length;i++){
							var flag = false;
							for(var j=0;j<ProgramDocument._listShopId.length;j++){
								if(ProgramDocument._listShopId[j] == data.listShopId[i]){
									flag = true;
									break;
								}
							}
							if(flag!=true){
								ProgramDocument._listShopId.push(data.listShopId[i]);
							}
						}
					}
					ProgramDocument.loopCheck(arrayShopId);
				} 
			}
    	});
	},
	recursionFindChildShopUnCheck:function(id){
		if(id == null || id == undefined){
			return;
		}
		$.ajax({
    		type : "POST",
			url : '/document/getChildShopId',
			data: ({shopId:id}),
			dataType: "json",
			success : function(data) {
				if(data != null && data != undefined && data.listShopId != null && data.listShopId != undefined){
					var flag = true;
					var length = data.length;
					var arrayShopId = data.listShopId;
					for(var i=0;i<length;i++){
						ProgramDocument.removeArrayByValue(ProgramDocument._listShopId,data.listShopId[i]);
					}
					ProgramDocument.loopUnCheck(arrayShopId);
				}
			}
    	});
	},
	removeArrayByValue:function(arr,val){
		for(var i=0; i<arr.length; i++) {
	        if(arr[i] == val){
	        	arr.splice(i, 1);
		        break;
	        }
	    }
	},
	updateDocumentShopMap:function(){
		var params = new Object();
		params.listShopId = ProgramDocument._listShopId;
//		params.ProgramDocument._listShopIdCheck;
		params.idOfficeDoc = $('#idDocumentIDGrid').val().trim();
		if(ProgramDocument._listShopId.length<=0){
			$('.easyui-dialog #errMsgDialog').html('Bạn chưa chọn đơn vị để thêm').show();
			return false;
		}
		try{
			Utils.addOrSaveData(params, '/document/updateAndCreOffDocShopMap', null, 'errMsgDialog', function(data){
				$('#divOverlay').show();
				if(data.error != undefined && data.errMsg != true){
					$('#divOverlay').hide();
					$('#errExcelMsg').html("").hide();
					$('#resultInsertShop').html('Lưu dữ liệu thành công.').show();
					var tm = setTimeout(function(){
						$('#resultInsertShop').html('').hide();
						clearTimeout(tm); 
						$('#searchStyleShopDisplay1').dialog('close');
					}, 2500);
				} else {
					$('#divOverlay').hide();
					$('#resultInsertShop').html('Chưa Lưu dữ liệu được').show();
				}
			}, null, null, null, "Bạn có muốn thêm đơn vị này ?", null);
		}
		catch(e) {
			$('#divOverlay').hide();
			$('#errMsgDialog').html('Cập nhật dữ liệu thất bại').show();
			setTimeout(function(){$('#errMsgDialog').hide();},3500);
		}
	},
	searchShop:function(){
		var code = $('#shopCode').val().trim();
		var name = $('#shopName').val().trim();
//		var id = $('#selId').val();
		var url = ProgrammeDisplayCatalog.getShopGridUrl(id,code,name);
		$("#exGrid").treegrid({url:url});
		$('#errExcelMsg').html("").hide();
		ProgrammeDisplayCatalog._listShopIdEx = new Array();
		ProgrammeDisplayCatalog._listShopId = new Array();
	},
	
	getSearchStyleDocShopMapGridUrl : function(url, code, name) {		
		var searchUrl = '';
		searchUrl = url;
		searchUrl+= "?promotionProgramCode=" + Utils.XSSEncode(code) + "&promotionProgramName=" + Utils.XSSEncode(name);
		var idOfficeDoc = $('#idDocumentIDGrid').val().trim();
		
		searchUrl += "&idOfficeDoc=" + idOfficeDoc;
		return searchUrl;
	},
	loopCheck:function(arrayShopId){
		if(arrayShopId == null || arrayShopId == undefined || arrayShopId.length == 0){
			return;
		}
		$('.easyui-dialog #check_' +arrayShopId[0]).attr('checked','checked');
		ProgramDocument.removeArrayByValue(arrayShopId,arrayShopId[0]);
		ProgramDocument.loopCheck(arrayShopId);
	},
	loopUnCheck:function(arrayShopId){
		if(arrayShopId == null || arrayShopId == undefined || arrayShopId.length == 0){
			return;
		}
		$('.easyui-dialog #check_' +arrayShopId[0]).removeAttr('checked');
		ProgramDocument.removeArrayByValue(arrayShopId,arrayShopId[0]);
		ProgramDocument.loopUnCheck(arrayShopId);
	}
};
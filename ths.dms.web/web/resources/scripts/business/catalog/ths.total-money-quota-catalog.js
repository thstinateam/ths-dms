var TotalMoneyQuotaCatalog = {
	_xhrSave : null,
	_xhrDel: null,
	getGridUrl: function(channelTypeCode,channelTypeName,status,saleAmount){		
		return "/catalog/total-money-quota/search?code=" + encodeChar(channelTypeCode) + "&name=" + encodeChar(channelTypeName) + "&status=" + status + '&saleAmount=' + encodeChar(saleAmount);
	},	
	search: function(){		
		$('#errMsg').html('').hide();
		$('#errExcelMsg').html('').hide();
		var channelTypeCode = $('#code').val().trim();
		var channelTypeName =  $('#name').val().trim();		
		var status = $('#status').val().trim();
		var saleAmount =  Utils.returnMoneyValue($('#saleAmount').val().trim());
		var url = TotalMoneyQuotaCatalog.getGridUrl(channelTypeCode,channelTypeName,status,saleAmount);
		$("#grid").setGridParam({url:url,page:1}).trigger("reloadGrid");
		return false;
	},
	save: function(){
		var msg = '';
		$('#errMsg').html('').hide();
		$('#errExcelMsg').html('').hide();
		msg = Utils.getMessageOfRequireCheck('code','Mã loại KH',true);
		if(msg.length == 0){
			msg = Utils.getMessageOfRequireCheck('saleAmount','Mức DS');
		}
		var skuAmount = $('#saleAmount').val().trim().replace(/,/g,'');
		
		if(isNaN(skuAmount) || parseInt(skuAmount) <=0){
			msg = 'Mức DS phải là số nguyên dương';
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfRequireCheck('status','Trạng thái',true);
		}		
		if(msg.length > 0){
			$('#errMsg').html(msg).show();
			return false;
		}
		var dataModel = new Object();
		dataModel.id = $('#selId').val().trim();
		dataModel.code = $('#code').val().trim();		
		dataModel.saleAmount = skuAmount;
		dataModel.status = $('#status').val();
		Utils.addOrSaveRowOnGrid(dataModel, "/catalog/total-money-quota/save", TotalMoneyQuotaCatalog._xhrSave, null, null, function(data){
			if(!data.error){
				TotalMoneyQuotaCatalog.resetForm();
				TotalMoneyQuotaCatalog.search();
			}			
		});			
		return false;
	},
	getSelectedRow: function(rowId){
		disabled('name');
		var code = $("#grid").jqGrid ('getCell', rowId, 'channelTypeCode');
		var name = $("#grid").jqGrid ('getCell', rowId, 'channelTypeName');
		var status = $("#grid").jqGrid ('getCell', rowId, 'statusAmount');
		var skuAmount = $("#grid").jqGrid ('getCell', rowId, 'saleAmount');
		var id = $("#grid").jqGrid ('getCell', rowId, 'id');
		
		if(id!= null && id!= 0 && id!=undefined){
			$('#selId').val(id);
		} else {
			$('#selId').val(0);
		}
		setSelectBoxValue('code',code);
		setTextboxValue('name',name);
		setTextboxValue('saleAmount',skuAmount);
		if(status == activeStatusText){
			status = 1;
		} else {
			status = 0;
		}
		setSelectBoxValue('status', status);
		disabled('code');
		$('#code').change();
		TotalMoneyQuotaCatalog.getChangedForm();	
		setTitleUpdate();
		$('#saleAmount').focus();
		$('#errExcelMsg').html('').hide();
		return false;
	},
	resetForm: function(){
		setTitleSearch();
		$('.RequireStyle').hide();
		$('#selId').val(0);
		$('#errMsg').html('').hide();
		$('#errExcelMsg').html('').hide();
		$('#name').removeAttr('disabled');
		$('#btnEdit').hide();
		$('#btnCancel').hide();
		$('#btnSearch').show();
		$('#btnAdd').show();		
		$('#name').val('');
		$('#saleAmount').val('');
		setSelectBoxValue('code', -2);
		setSelectBoxValue('status', 1);		
		enable('code');
		$('#code').change();
		TotalMoneyQuotaCatalog.search();
		$('#saleAmount').focus();
		return false;
	},
	deleteSale: function(id){
		var dataModel = new Object();
		dataModel.id = id;	
		Utils.deleteSelectRowOnGrid(dataModel, 'mức DS của loại KH', '/catalog/total-money-quota/delete', TotalMoneyQuotaCatalog._xhrDel, null, null,function(data){
			if (!data.error){
				TotalMoneyQuotaCatalog.resetForm();
			}
		},null);
		$('#errExcelMsg').html('').hide();
		return false;
	},
	getChangedForm: function(){
		$('#saleAmount').focus();
		$('.RequireStyle').show();
		$('#btnSearch').hide();
		$('#btnAdd').hide();
		$('#btnEdit').show();
		$('#btnCancel').show();	
		disabled('name');
		setTitleAdd();
		$('#code').change();
	}
};
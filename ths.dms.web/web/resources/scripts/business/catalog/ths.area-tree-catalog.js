var AreaTreeCatalog = {
	parentsId:null,
	searchFilter:null,
	selectNodeTree:-1,
	isRefresh:false,
	isUpdate:false,
	updateArea: function(){
		var params = new Object();
		params.areaId=$('#areaId').val();
		params.status=$('#status').val();
		Utils.addOrSaveData(params, '/catalog/area-tree/update-area', null,  'errMsg', function(data) {
			$('#successMsgImport').html(msgCommon1).show();
			setTimeout(function(){
				$('#areaParentCode').val("");
				$('#areaNameParent').val("");
				$('#areaCode').val("");
				$('#areaName').val("");
				AreaTreeCatalog.resetSearch();
				AreaTreeCatalog.search();
				AreaTreeCatalog.loadAreaTree();
				$('.SuccessMsgStyle').hide();
			 }, 3000);
		});
		return false;
	},
	search:function(parentId){
		//AreaTreeCatalog.resetSearch();
		var param = new Object();
		param.areaCode=$('#areaCode').val().trim();
		param.areaName=$('#areaName').val().trim();
		param.status=$('#status').val();
		if($('#parentId').val()!='' && $('#parentId').val()!='-1'){
			param.parentId=$('#parentId').val();
		}
		$("#grid").datagrid('load',param);
	},
	loadAreaTree:function(){		
		$('#tree').jstree({
	        "plugins": ["themes", "json_data","ui","contextmenu"],
	        "themes": {
	            "theme": "classic",
	            "icons": false,
	            "dots": true
	        },
	        "json_data": {
	        	"ajax" : {
	        		"url": function(node){
	                      if( node == -1 ){
	                    	  return "/rest/catalog/area-tree-ex/0/tree.json";
	                      } else {
	                    	  return "/rest/catalog/area-tree-ex/" +node.attr("id")+"/tree.json";
	                      }
	                    },
	                "data" : function (n) {
	                        return { id : n.attr ? n.attr("id") : 0 };
	                    },
	                "complete":function(){
	                	AreaTreeCatalog.setSelectedNode(AreaTreeCatalog.selectNodeTree);
	    	        }
	            }        	
	        },
	        "contextmenu" : {
                items : {
                    "chinhsua" : {
                        "separator_before"  : false,
                        "separator_after"   : true,
                        "label"             : "<span style='font-size:13px;'>" +Utils.XSSEncode(common_information_detail)+"</span>",
                        "icon"				:"/resources/images/icon_edit.png",
                        "action"            : function(obj){
                        	$('#errExcelMsg').hide();
                        	$('#findInfo').html(area_tree_area_update);
                        	var nodeId = obj.attr('id').trim();
                        	AreaTreeCatalog.setSelectedNode(nodeId);	
                        	if(nodeId != '-1'){
                        		AreaTreeCatalog.parentsId = $.jstree._reference('#tree')._get_parent(obj).attr("id");
							}else{
								AreaTreeCatalog.parentsId = '-1';
								return false;
							}
                        	$.ajax({
                    			type : "POST",
                    			url : '/catalog/area-tree/get-AreaDetail-ById',
                    			dataType : "json",
                    			data :$.param({areaId : nodeId}, true),
                    			success : function(result) {
                    				//hidden div chua grid
                    				$('#divContentEdit').show();
                    				$('#GridSection').hide();
                    				$('#hideSupport').hide();
                    				$('#titleGrid').hide();		
                    				$('#btnUpdate').show();
                    				$('#btnSearch').hide();
                    				$('#title').html(area_tree_information_area);
                    				$('#areaId').val(nodeId.toString());
                    				var area = result.area;
                    				$('#areaCode').val(Utils.XSSEncode(area.areaCode));
                    				$('#areaName').val(Utils.XSSEncode(area.areaName));
                    				if(area.parentArea != null){
                    					$('#areaNameParent').val(Utils.XSSEncode(area.parentArea.areaName));
                    					$('#areaParentCode').val(Utils.XSSEncode(area.parentArea.areaCode));
                    				}
                    				else{
                    					$('#areaNameParent').val('');
                    					$('#areaParentCode').val('');
                    				}
                    				$('#areaCode').attr("disabled","disabled");
                    				$('#areaName').attr("disabled","disabled");
                    				$('#areaParentCode').attr("disabled","disabled");
                    				$('#areaNameParent').attr("disabled","disabled");
                    				if(area.status=='STOPPED'){
                    					$('#status').val(0).change();
                    				}else{
                    					$('#status').val(1).change();
                    				}
                    				AreaTreeCatalog.isUpdate = true;
                    				jQuery('#status').children('option[value="-2"]').hide();
                    				$('#status').focus();
                    				//Xu ly phan quyen control
                    				Utils.functionAccessFillControl('searchForm', function (data) {
                    					
                    				});
                    			}
                        	});
                        }
                    }
                }
            }

		});	
		$('#tree').bind("loaded.jstree", function(event, data){			
			AreaTreeCatalog.setSelectedNode("-1");
		});
		
		$('#tree').bind('before.jstree',
				function(event, data) {
					if (data.func == 'refresh') {
						AreaTreeCatalog.isRefresh = true;
					}
			});
		
		$('#tree').bind("select_node.jstree", function (event, data) {
        	$('#errExcelMsg').html('').hide();
			var name = data.inst.get_text(data.rslt.obj);
			var id = data.rslt.obj.attr("id");
			AreaTreeCatalog.selectNodeTree = id;
			$('#parentId').val(id);
			if(id == -1){
				id = '';
			}else{
				AreaTreeCatalog.parentsId = data.inst._get_parent(data.rslt.obj).attr("id"); // lay id node cha
			}
			AreaTreeCatalog.isUpdate = false;
			if(AreaTreeCatalog.isRefresh){
				AreaTreeCatalog.isRefresh = false;
				return false;
			}
			AreaTreeCatalog.search(id);
			AreaTreeCatalog.resetOnLoad();
			AreaTreeCatalog.searchFilter == null;
	    });
	},
	/**phuocdh2 get download file **/
	download:function(){
		var dataModel = new Object();
		try {
			dataModel.reportCode = $('#function_code').val();
		} catch(e) {
			// Oop! Error
		}
		var kData = $.param(dataModel,true);
//		var path = '';
		$.ajax({
			type : "POST",
			url : '/catalog/area-tree/get-template-excel',
			data : (kData),
			dataType : "json",
			success : function(result) {
				var filePath = ReportUtils.buildReportFilePath(result.path);
				window.location.href = filePath;
//				path = filePath;
//				$('#downloadTemplate').attr('href', path);
			}
    	});
		//$('#downloadTemplate').attr('href', excel_template_path + 'catalog/Bieu_mau_danh_muc_cay_dia_ban.xls');
	}
	,
	exportExcelData:function(){
		$('#divOverlay').addClass('Overlay');
		$('#imgOverlay').show();
		$('#errMsg').html('').hide();
		$('#errExcelMsg').html('').hide();
		var areaCode = $('#areaCode').val().trim();
		var areaName = $('#areaName').val().trim();
		var status = $('#status').val().trim();
		var isAll = false;
		if($('#isAll').attr('checked') == 'checked') {
			isAll = true;
		}
		var data = new Object();
		data.areaCode = areaCode;
		data.areaName = areaName;
		data.status = status;
		data.isAll = isAll;
		if($('#parentId').val()!='' && $('#parentId').val()!='-1'){
			data.parentId=$('#parentId').val();
		}
		var url = "/catalog/area-tree/getlistexceldata";
		ReportUtils.exportReport(url,data,'errMsg');
	},
	importExcel:function(){	
		Utils.importExcelUtils(function(data){
			if(data.message!=undefined && data.message!=null && data.message.trim().length >0){
				$('#errExcelMsg').html(data.message.trim()).change().show();
			}else{
				AreaTreeCatalog.search();
				AreaTreeCatalog.loadAreaTree();
				$('#successMsgImport').html(msgCommon1).show();
				var tm = setTimeout(function(){
					$('#successMsgImport').html("").hide();
					clearTimeout(tm);
				 }, 4000);
			}
		}, 'importFrm', 'excelFile');
		return false;
	},	
	setSelectedNode:function(nodeId){
		$('#tree a').removeClass('jstree-hovered jstree-clicked');
		$('#tree #' +nodeId+' >a').addClass('jstree-clicked');
	},
	resetOnLoad:function(){
		setSelectBoxValue('status',1);				
		$('#areaCode').val('');
		$('#areaName').val('');
		$('#divContentEdit').hide();
		$('#GridSection').show();
		$('#btnUpdate').hide();
		$('#btnSearch').show();
		$('#areaCode').removeAttr('disabled');
		$('#areaName').removeAttr('disabled');
	},
	resetSearch:function(){
		setSelectBoxValue('status',1);				
		$('#divContentEdit').hide();
		$('#GridSection').show();
		$('#btnUpdate').hide();
		$('#btnSearch').show();
		$('#areaCode').removeAttr('disabled');
		$('#areaName').removeAttr('disabled');
	}
};
var DisplayToolCatalog = {
	_xhrSave : null,
	_xhrDel: null,
	_selObjType: null,	
	getGridUrl: function(shopCode,supervisorStaffCode,saleStaffCode,customerCode,usedMonth){
		return "/catalog_display_tool/search?shopCode=" + encodeChar(shopCode) + "&supervisorStaffCode=" + encodeChar(supervisorStaffCode) + "&saleStaffCode=" + encodeChar(saleStaffCode)+ "&customerCode=" + encodeChar(customerCode) +"&usedMonth=" + encodeChar(usedMonth);
	},	
	search: function(){
		$('#errMsg').html('').hide();
		var shopCode = $('#shopCode').val().trim();
		var supervisorStaffCode = $('#supervisorStaffCode').val().trim();
		var saleStaffCode = $('#saleStaffCode').val().trim();
		var customerCode = $('#customerCode').val().trim();
		var usedMonth = $('#usedMonth').val().trim();
		var tm = setTimeout(function() {
			$('#supervisorStaffCode').focus();
		}, 500);
		var url = DisplayToolCatalog.getGridUrl(shopCode,supervisorStaffCode,saleStaffCode,customerCode,usedMonth);
		$("#grid").setGridParam({url:url,page:1}).trigger("reloadGrid");
		return false;
	},
	change: function(){
		var msg = '';
		$('#errMsg').html('').hide();
		msg = Utils.getMessageOfRequireCheck('shopCode','Mã đơn vị');
		if(msg.length == 0){
			msg = Utils.getMessageOfSpecialCharactersValidate('shopCode','Mã đơn vị',Utils._CODE);
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfRequireCheck('saleStaffCode','Mã NVBH');
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfSpecialCharactersValidate('saleStaffCode','Mã NVBH',Utils._CODE);
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfRequireCheck('customerCode','Mã KH');
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfSpecialCharactersValidate('customerCode','Mã KH',Utils._CODE);
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfRequireCheck('displayToolCode','Mã tủ');
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfSpecialCharactersValidate('displayToolCode','Mã tủ',Utils._CODE);
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfRequireCheck('quantity','Số lượng');
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfRequireCheck('income','Doanh số');
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfRequireCheck('usedMonth','Tháng sử dụng');
		}
		if(msg.length > 0){
			$('#errMsg').html(msg).show();
			return false;
		}
		var dataModel = new Object();
		dataModel.displayToolId = $('#displayToolId').val().trim();
		dataModel.shopCode = $('#shopCode').val().trim();
		dataModel.supervisorStaffCode = $('#supervisorStaffCode').val().trim();
		dataModel.saleStaffCode = $('#saleStaffCode').val().trim();
		dataModel.customerCode = $('#customerCode').val().trim();
		dataModel.displayToolCode = $('#displayToolCode').val().trim();
		dataModel.quantity = $('#quantity').val().trim();
		dataModel.income = Utils.returnMoneyValue($('#income').val().trim());
		dataModel.usedMonth = $('#usedMonth').val().trim();
		Utils.addOrSaveRowOnGrid(dataModel, "/catalog_display_tool/save", DisplayToolCatalog._xhrSave, null, null,function(){
			DisplayToolCatalog.resetForm();
		});
		return false;
	},
	getSelectedDisplayTool: function(displayToolId,shopCode,supervisorStaffCode,saleStaffCode,customerCode,displayToolCode,quantity,income,usedMonth){
		if(displayToolId!= null && displayToolId!= 0 && displayToolId!=undefined){
			$('#displayToolId').val(displayToolId);
		} else {
			$('#displayToolId').val(0);
		}
		setTextboxValue('shopCode',shopCode);
//		setTextboxValue('supervisorStaffCode',supervisorStaffCode);
		if(displayToolId != null && displayToolId > 0){
//			$('#saleStaffCode').attr('disabled','disabled');
			$('#customerCode').attr('disabled','disabled');
		}
		$('#shopCode').attr('disabled','disabled');
		$('#supervisorStaffCode').attr('disabled','disabled');
		setTextboxValue('saleStaffCode',saleStaffCode);
		setTextboxValue('customerCode',customerCode);	
		setTextboxValue('displayToolCode',displayToolCode);
		setTextboxValue('quantity',quantity);	
		setTextboxValue('income',income);
		Utils.formatCurrencyFor('income');
		setTextboxValue('usedMonth',usedMonth);	
		Utils.getChangedForm(true);
		setTitleUpdate();
		$('#btnSearch,#btnCreate').hide();
		$('#btnUpdate,#btnDismiss').show();
	},
	resetForm: function(){
		$('#errMsg').html('').hide();
		setTitleSearch();
		$('.RequireStyle').hide();
		$('.divBankHidden').hide();
//		$('#saleStaffCode').removeAttr('disabled');
		$('#customerCode').removeAttr('disabled');
		setTextboxValue('saleStaffCode','');
		setTextboxValue('customerCode','');	
		setTextboxValue('displayToolCode','');
		setTextboxValue('quantity','');	
		setTextboxValue('income','');
		setTextboxValue('usedMonth',getCurrentMonth());	
		$('#btnUpdate,#btnDismiss').hide();
		$('#btnCreate,#btnSearch').show();
		if(isBoth == 'false'){
			if(isShopEnable == 'true'){
				$('#shopCode').removeAttr('disabled','disabled');
			}else{
				$('#supervisorStaffCode').removeAttr('disabled','disabled');
			}
		}else{
			$('#shopCode').removeAttr('disabled','disabled');
			$('#supervisorStaffCode').removeAttr('disabled','disabled');
		}
		var url = DisplayToolCatalog.getGridUrl($('#shopCode').val(),$('#supervisorStaffCode').val(),'','',$('#usedMonth').val());
		$("#grid").setGridParam({url:url,page:1}).trigger("reloadGrid");
		return false;
	},
	deleteDisplayTool: function(displayToolId){
		$('#errMsg').html('').hide();
		var dataModel = new Object();
		dataModel.displayToolId = displayToolId;	
		Utils.deleteSelectRowOnGrid(dataModel, 'khách hàng sử dụng tủ', "/catalog_display_tool/remove", DisplayToolCatalog._xhrDel, null, null,function(){
			DisplayToolCatalog.resetForm();
		});		
		return false;		
	}
};
var StaffTypeCatalog = {
	_xhrSave : null,
	_xhrDel: null,
	isContextMenu:0,
	parentsId:null,
	_selObjType: null,	
	getGridUrl: function(staffTypeCode,parentCode,staffTypeName,status,staffObjectType,isGetParent){
		return "/catalog/staff-type/search?staffTypeCode=" + encodeChar(staffTypeCode) + "&parentCode=" + encodeChar(parentCode) + "&staffTypeName=" + encodeChar(staffTypeName) + "&status=" + status+ "&staffObjectType=" + encodeChar(staffObjectType) +"&isGetParent=" + encodeChar(isGetParent);
	},	
	search: function(){
		$('#errMsg').html('').hide();
		var staffTypeCode = $('#staffTypeCode').val().trim();
		var parentCode = $('#parentCode').combotree('getValue');
		if(parentCode == '' || parentCode == null){
			parentCode = -1;
		}
		var staffTypeName = $('#staffTypeName').val().trim();
		var status = $('#status').val().trim();
		var staffObjectType = $('#staffObjectType').val().trim();
		var url = StaffTypeCatalog.getGridUrl(staffTypeCode,parentCode,staffTypeName,status,staffObjectType,false);
		$("#grid").datagrid({url:url,page:1});
		var tm = setTimeout(function() {
			$('#staffTypeCode').focus();
		}, 500);
		return false;
	},
	change: function(){
		var msg = '';
		$('#errMsg').html('').hide();
		msg = Utils.getMessageOfRequireCheck('staffTypeCode','Mã loại nhân viên');
		if(msg.length == 0){
			msg = Utils.getMessageOfSpecialCharactersValidate('staffTypeCode','Mã loại nhân viên',Utils._CODE);
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfRequireCheck('staffTypeName','Tên loại');
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfSpecialCharactersValidate('staffTypeName','Tên loại');
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfRequireCheck('status','Trạng thái',true);
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfRequireCheck('staffObjectType','Mã phân loại',true);
		}
		if($('#status').val() != 1 && $('#status').val() != 0 && msg.length == 0){
			msg = "Bạn chưa nhập giá trị cho trường Trạng thái. Yêu cầu nhập giá trị";
		}
		if(msg.length > 0){
			$('#errMsg').html(msg).show();
			return false;
		}
		var dataModel = new Object();
		dataModel.id = $('#staffTypeId').val().trim();
		dataModel.staffTypeCode = $('#staffTypeCode').val().trim();
		dataModel.staffTypeName = $('#staffTypeName').val().trim();
		dataModel.parentCode = $('#parentCode').combotree('getValue');
		dataModel.status = $('#status').val().trim();
		dataModel.staffObjectType = $('#staffObjectType').val().trim();
		Utils.addOrSaveRowOnGrid(dataModel, "/catalog/staff-type/save", StaffTypeCatalog._xhrSave, null, null,function(){
			$('#contentGrid').show();
			StaffTypeCatalog.loadTree();
			StaffTypeCatalog.resetForm();
			var url = StaffTypeCatalog.getGridUrl('',-1,'',1,-1,false);
			$("#grid").datagrid({url:url,page:1});
		});
		return false;
	},
	getSelectedStaffType: function(id,staffTypeCode,parentCode,staffTypeName,status,staffObjectType){
		$('#errMsg').html('').hide();
		$('#selObjType').val(staffObjectType);
		var url = 'channelTypeId='+ parentCode;
		$.getJSON('/rest/catalog/catalog/staff-type/channeltype.json?'+url, function(data){
			if(data != null && data.status != 'RUNNING'){
				$('#errMsg').html('Bạn phải thay đổi mã cha về trạng thái hoạt động trước khi chỉnh sửa được đối tượng này').show();
			} else {
				$('#parentCode').combotree("reload",'/rest/catalog/staff-type/combotree/1/'+id+'.json');
				if(id!= null && id!= 0 && id!=undefined){
					hideOption('parentCode',id);
					$('#staffTypeId').val(id);
				} else {
					$('#staffTypeId').val(0);
				}
				$('#staffTypeCode').attr('disabled',true);
				setTextboxValue('staffTypeCode',staffTypeCode);
				setTextboxValue('staffTypeName',staffTypeName);				
				setSelectBoxValue('status', status);
				setSelectBoxValue('staffObjectType', staffObjectType);
				$('#parentCode').combotree('setValue',parentCode);
				Utils.getChangedForm();
				//setTitleUpdate();
				$('#divParentCode').show();
				$('#staffObjectType').attr('disabled', true);
				$('#parentCode').combotree('disable');
				$('#title').html('Thông tin loại nhân viên');
				$('#title1').html('Thông tin loại nhân viên');
				$('#btnSearch,#btnCreate').hide();
				$('#btnUpdate,#btnDismiss').show();
			}
		});
	},
	resetForm: function(){
		$('#errMsg').html('').hide();
		setTitleSearch();
		$('#title1').html('Danh sách loại nhân viên');
		$('.ReqiureStyle').hide();
		$('#divParentCode').hide(); //an ma cha
		$('#btnUpdate,#btnDismiss').hide();
		$('#btnSearch').show();
		$('#parentCode').combotree('enable');
		$('#staffTypeCode').val('');
		$('#staffTypeCode').removeAttr('disabled');
		$('#staffTypeCode').focus();
		$('#staffTypeName').val('');
		resetAllOptions('parentCode');
		$('#parentCode').combotree('clear');
		$('#parentCode').combotree("reload",'/rest/catalog/staff-type/combotree/0/0.json');
		$('#parentCode').combotree("setValue",-1);
		setSelectBoxValue('status',1);
		setSelectBoxValue('staffObjectType',-1);
		$('#staffObjectType').removeAttr('disabled');
		$('#staffTypeId').val(0);
		$('#selObjType').val(-1);
		var url = StaffTypeCatalog.getGridUrl('',-1,'',1,-1,false);
		//$("#grid").datagrid({url:url,page:1});
		return false;
	},
	deleteStaffType: function(staffTypeCode){
		$('#errMsg').html('').hide();
		var dataModel = new Object();
		dataModel.staffTypeCode = staffTypeCode;	
		Utils.deleteSelectRowOnGrid(dataModel, 'loại nhân viên', "/catalog/staff-type/remove", StaffTypeCatalog._xhrDel, null, null,function(){
			StaffTypeCatalog.loadTree();
			StaffTypeCatalog.resetForm();
		});		
		return false;		
	},
	loadTree: function(){
		//loadDataForTree('/rest/catalog/staff-type/tree.json');
		$('#tree').jstree({
	        "plugins": ["themes", "json_data","ui","contextmenu"],
	        "themes": {
	            "theme": "classic",
	            "icons": false,
	            "dots": true
	        },
	        "json_data": {
	        	"ajax" : {
	                "url" : '/rest/catalog/staff-type/tree.json',
	                "data" : function (n) {
	                        return { id : n.attr ? n.attr("id") : 0 };
	                    }
	            }        	
	        },
	        'onExpand':function(){
	        	console.log(0);
	        },
	        "contextmenu" : {
	            items : { // Could be a function that should return an object like this one
	                "create" : {
	                    "separator_before"  : false,
	                    "separator_after"   : true,
	                    "_class"			:"Item Item 1",
	                    "label"             : "Thêm loại nhân viên",
	                    "action"            : function(obj){
	                    						var nodeId = obj.attr('id').trim();
	                    						StaffTypeCatalog.setSelectedNode(nodeId);
	                    						StaffTypeCatalog.isContextMenu = 1;
	                    						StaffTypeCatalog.parentsId = nodeId;
	                    						StaffTypeCatalog.newCusType();
	                    }
	                },
					"update" : {
						"separator_before"	: false,
						"separator_after"	: false,
						"_class"			:"Item Item 2",
						"label"				: "Chỉnh sửa",
						"action"			: function (obj) { 
							var nodeId = obj.attr('id').trim();
							StaffTypeCatalog.setSelectedNode(nodeId);
							StaffTypeCatalog.isContextMenu = 1;
							if(nodeId != '-1'){
								StaffTypeCatalog.parentsId = $.jstree._reference('#tree')._get_parent(obj).attr("id");
							}else{
								StaffTypeCatalog.parentsId = '-1';
								return false;
							}
							
                        	$.ajax({
                				type : "POST",
                				url : '/catalog/staff-type/update',
                				dataType : "json",
                				data :$.param({id : nodeId}, true),
                				success : function(result) {
                					//hidden div chua grid
                					$('#contentGrid').hide();
                					//$('#divGeralMilkBox').show();
                					var channelType = result.channelType;
                					var status = 1;
                					if(channelType.status != activeStatus){
                						status = 0;
                					}
                					$('#selObjType').val(channelType.objectType);
                					$('#staffTypeId').val(nodeId.toString());
                					$('#staffTypeCode').attr('disabled','disabled');
                					$('#parentCode').combotree('disable');
                					setTextboxValue('staffTypeCode',channelType.channelTypeCode.toString());
                					setTextboxValue('staffTypeName',channelType.channelTypeName.toString());
                					setSelectBoxValue('status',status);
                					setSelectBoxValue('staffObjectType',channelType.objectType);
                					StaffTypeCatalog.getChangedForm();
                					StaffTypeCatalog.isContextMenu = 0;
                					$('#divParentCode').show();
                					$('#staffObjectType').attr('disabled', true);
                					$('#title').html('Thông tin loại nhân viên');
                					$('#title1').html('Thông tin loại nhân viên');
                					$('#staffTypeName').focus();
                					
                				}
                        	});
						}
					}
	            }
	        }
		});	
		$('#tree').bind("loaded.jstree", function(event, data){
			if($('#treeContainer').data("jsp")!= undefined){
				$('#treeContainer').data("jsp").destroy();
			}
			$('#treeContainer').jScrollPane();
			StaffTypeCatalog.setSelectedNode("-1");
		});
		$('#tree').bind("select_node.jstree", function (event, data) {
			var name = data.inst.get_text(data.rslt.obj);
			var id = data.rslt.obj.attr("id");
			$('#contentGrid').show();
			if(id == -1){
				id = '-1';
			}else{
				StaffTypeCatalog.parentsId = data.inst._get_parent(data.rslt.obj).attr("id"); // lay id node cha
			}
			var url = StaffTypeCatalog.getGridUrl('',id,'',1,-1,true);
			$("#grid").datagrid({url:url,page:1});
			StaffTypeCatalog.resetForm();
	    });
		$("#tree").bind("open_node.jstree", function (event, data) { 
			$('.jspContainer').css('height','auto');
		}); 
	},
	getChangedForm: function(){
		if(StaffTypeCatalog.isContextMenu == 1){
			if(StaffTypeCatalog.parentsId == null || StaffTypeCatalog.parentsId=='' ){
				StaffTypeCatalog.parentsId = '-1';
			}
			$('#parentCode').combotree('setValue',StaffTypeCatalog.parentsId);
		}
		$('#errMsg').hide();
		$('#divParentCode').show();
		$('.ReqiureStyle').show();		
		$('#btnSearch').hide();
		$('#btnUpdate').show();
		$('#title').html('Thông tin loại nhân viên');
		$('#title1').html('Thông tin loại nhân viên');
	},
	getAppramCode : function(parentId){
		if(parentId ==-1){	
			var selObjType = $('#selObjType').val().trim();
			if(selObjType.length == 0){
				selObjType = -1;
			}
			setSelectBoxValue('staffObjectType',selObjType);
		} else{
			var url = 'channelTypeId='+ parentId;
			$.getJSON('/rest/catalog/catalog/staff-type/channeltype.json?'+url, function(data){
				$('#staffObjectType').attr('disabled', true);
				if(data !=null || data != undefined) {
					setSelectBoxValue('staffObjectType',data.objectType);
				} else {					
					setSelectBoxValue('staffObjectType',-1);
					
				}
			});
		}
	},
	getListParentCode:function(parentId,objParentCode){
		$.ajax({
			type : "POST",
			url : "/catalog/staff-type/getlistparentcode",
			data :({parentCode:parentId}),
			dataType : "json",
			success : function(data) {
				var html = new Array();
    			if(data!= null && data!= undefined && data.lstParentCode!= null && data.lstParentCode.length > 0){
    				html.push("<option value='-1'>---Chọn mã cha---</option>");
    				for(var i=0;i<data.lstParentCode.length;i++){
    					html.push("<option value='"+ data.lstParentCode[i].id +"'>"+ data.lstParentCode[i].channelTypeName +"</option>");
    				}
    			}else{
    				html.push("<option value='-1'>---Chọn mã cha---</option>");
    			}	        			        	
    			$(objParentCode).html(html.join(""));
    			$(objParentCode).val(parentId);
    			$(objParentCode).change();
			},
			error:function(XMLHttpRequest, textStatus, errorThrown) {
				$.ajax({
					type : "POST",
					url : '/check-session',
					dataType : "json",
					success : function(data) {
					},
					error: function(XMLHttpRequest, textStatus, errorThrown) {
						window.location.href = '/home';
					}
				});
			}
		});
	},
	newCusType:function(){
		$('#contentGrid').hide();
		StaffTypeCatalog.resetForm();
		StaffTypeCatalog.getChangedForm();
	},
	setSelectedNode:function(nodeId){
		$('#tree a').removeClass('jstree-hovered jstree-clicked');
		$('#tree #'+nodeId+' >a').addClass('jstree-clicked');
	}
};
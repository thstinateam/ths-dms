var DeliveryGroupCatalog = {
	_xhrSave : null,
	_xhrDel: null,
	getGridUrl: function(code,name,staffCode,customerCode,shopId,status,shopCode, allChild){
		if(shopId == undefined || shopId == ''){
			shopId = -2;
		}
		if(shopCode == null || shopCode == undefined){
			shopCode = '';
		}
		return "/catalog/delivery-group/search?code=" + encodeChar(code) + "&name=" + encodeChar(name) + "&staffCode=" + encodeChar(staffCode) + "&customerCode=" + encodeChar(customerCode) + "&shopId=" + encodeChar(shopId) + '&status=' + status + '&shopCode=' + encodeChar(shopCode) + '&getAll=' + encodeChar(allChild);
	},	
	search: function(){
		$('#errMsg').html('').hide();
		$('#errExcelMsg').html('').hide();
		var code = $('#code').val().trim();
		var name = $('#name').val().trim();
		var staffCode = $('#staffCode').val().trim();
		var customerCode = $('#customerCode').val().trim();
		var status = $('#status').val().trim();
		var shopCode = $('#shopCode').val().trim();		
		var shopId = -2;
		var url = DeliveryGroupCatalog.getGridUrl(code,name,staffCode,customerCode,shopId,status,shopCode,true);
		$("#grid").setGridParam({url:url,page:1}).trigger("reloadGrid");
		return false;
	},
	saveInfo: function(){
		var msg = '';
		$('#errMsg').html('').hide();
		msg = Utils.getMessageOfRequireCheck('code','Mã nhóm');
		if(msg.length == 0){
			msg = Utils.getMessageOfSpecialCharactersValidate('code','Mã nhóm',Utils._CODE);
		}
		if(msg.length == 0) {
			msg = Utils.getMessageOfSpecialCharactersValidate('name', 'Tên nhóm', Utils._NAME);
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfRequireCheck('name','Tên nhóm');
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfSpecialCharactersValidate('name','Tên nhóm');
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfRequireCheck('shopCode','Mã đơn vị');
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfSpecialCharactersValidate('shopCode','Mã đơn vị',Utils._CODE);
		}		
		if(msg.length == 0){
			msg = Utils.getMessageOfRequireCheck('staffCode','Mã NVGH');
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfSpecialCharactersValidate('staffCode','Mã NVGH',Utils._CODE);
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfRequireCheck('status','Trạng thái',true);
		}
		if(msg.length > 0){
			$('#errMsg').html(msg).show();
			return false;
		}
		var dataModel = new Object();
		dataModel.id = $('#selId').val().trim();
		dataModel.code = $('#code').val().trim();
		dataModel.name = $('#name').val().trim();
		dataModel.shopCode = $('#shopCode').val().trim();
		dataModel.staffCode = $('#staffCode').val().trim();
		dataModel.status = $('#status').val().trim();
		Utils.addOrSaveData(dataModel, "/catalog/delivery-group/save-info", DeliveryGroupCatalog._xhrSave, null, function(data){			
			$('#selId').val(data.id);
			$('#hidShopCodeTmp').val(Utils.XSSEncode(data.shopCode));
			$('#code').val($('#code').val().trim());
			$('#name').val($('#name').val().trim());
			$('#shopCode').val($('#shopCode').val().trim());
			$('#shopName').val($('#shopName').val().trim());
			DeliveryGroupCatalog.showCustomerTab($('#code').val().trim(), $('#name').val().trim(), $('#shopCode').val().trim(), $('#shopName').val().trim());
		});		
		return false;
	},
	getSelectedRow: function(id){
		location.href = '/catalog/delivery-group/change?id=' + id;
		return false;
	},
	resetForm: function(){
		$('#errMsg').html('').hide();			
		$('#code').val('');
		$('#name').val('');
		$('#staffCode').val('');
		$('#customerCode').val('');
		setSelectBoxValue('status',1);
		$("#grid").trigger("reloadGrid");
		return false;
	},
	deleteRow: function(id){
		var dataModel = new Object();
		dataModel.id = id;	
		Utils.deleteSelectRowOnGrid(dataModel, 'nhóm', '/catalog/delivery-group/delete', DeliveryGroupCatalog._xhrDel, null, 'errExcelMsg');		
		return false;
	},
	openAddForm: function(){
		var selCode = $('#tree').jstree('get_selected').attr('contentItemId');
		if(selCode == undefined){
			selCode = '';			
		}
		location.href = '/catalog/delivery-group/change?shopCode=' + encodeChar(selCode);
		return false;
	},
	shopCodeChanged: function(){
		var code = $('#shopCode').val().trim();
		$.getJSON('/rest/catalog/delivery-group/shop/'+ encodeChar(code) +'/name.json', function(data){
			if(data!=null){
				setTextboxValue('shopName',data.name);					
			}				
		});		
		return false;
	},
	staffCodeChanged: function(){
		var code = $('#staffCode').val().trim();
		if(code != $('#hidStaffCode').val().trim()){
			$.getJSON('/rest/catalog/delivery-group/staff/'+ encodeChar(code) +'/name.json', function(data){
				setTextboxValue('staffName',data.name);
				$('#hidStaffCode').val($('#staffCode').val());
			});
		}
		return false;
	},
	customerCodeChanged: function(){
		var code = $('#customerCode').val().trim();
		var shopCode = $('#shopCode').val().trim();
		if(code != $('#hidCustomerCode').val().trim()){
			$.getJSON('/rest/catalog/delivery-group/customer/'+ encodeChar(code) +'/'+ encodeChar(shopCode) +'/name.json', function(data){
				setTextboxValue('customerName',data.name);
				$('#hidCustomerCode').val($('#customerCode').val());
			});
		}
		return false;
	},
	showInfoTab: function(groupCode, groupName, shopCode, shopName){
		$('#errMsg').html('').hide();
		$('#customerDiv').hide();
		$('#staffDiv').show();	
		$('#statusDiv').show();
		
		$('#tabCusActive').removeClass('Active');
		$('#tabInfoActive').addClass('Active');
		$('#name').val(groupName);
		$('#shopCode').val(shopCode);
		$('#shopName').val(shopName);
		disabled('shopCode');
		if($('#code').val().length = 0 || groupCode.length == 0 || groupCode.length == null) {
			$('#code').removeAttr('disabled');
		}
		$('#name').removeAttr('disabled');
		$('#name').val(groupName);
		$('#shopCode').val(shopCode);
		$('#shopName').val(shopName);
		
		$('#btnAddCustomer').hide();
		$('#gridContainer').hide();
		
		if($('#selId').val().trim().length > 0){
			$('#btnUpdateInfo').show();				
		} else{
			$('#btnAddInfo').show();
		}
		//$('#btnCancel').show();
		return false;
	},
	showCustomerTab: function(groupCode, groupName, shopCode, shopName){
		$('#errMsg').html('').hide();
		$('#tabCusActive').removeClass('Disable');
		$('#staffDiv').hide();
		$('#customerDiv').show();
		$('#statusDiv').hide();
		$('#tabCus').bind('click', function() {
			DeliveryGroupCatalog.showCustomerTab(groupCode, groupName, shopCode, shopName);
		});
				
		$('#tabInfoActive').removeClass('Active');
		$('#tabCusActive').addClass('Active');
		$('#code').val(groupCode);
		$('#code').attr('disabled','disabled');
		$('#name').val(groupName);
		$('#shopCode').val(shopCode);
		$('#shopName').val(shopName);
		$('#name').attr('disabled','disabled');
		$('#shopCode').attr('disabled','disabled');
		//$('#hidShopCode').val($('shopCode').val());
		//$('#shopCode').val($('#hidShopCodeTmp').val());
		
		$('#btnAddInfo').hide();
		$('#btnUpdateInfo').hide();
		//$('#btnCancel').hide();	
		$('#btnAddCustomer').show();
		$('#gridContainer').show();
		
		var id=$('#selId').val().trim();
		var shopCode=$('#shopCode').val().trim();
		$("#grid").jqGrid({
		  url:'/catalog/delivery-group/customer-list?id=' + id + '&shopCode=' + shopCode,
		  colModel:[		
		    {name:'groupTransfer.shop.shopCode', label: 'Mã đơn vị', width: 100, sortable:false,resizable:false, align: 'left' },
		    {name:'groupTransfer.staff.staffCode', label: 'Mã NVGH', width: 100, sortable:false,resizable:false , align: 'left'},
		    {name:'shortCode', label: 'Mã KH', width: 100,sortable:false,resizable:false , align: 'left'},
		    {name:'customerName', label: 'Tên KH', sortable:false,resizable:false, align:'left'},	    
		    {name:'delete', label: 'Xóa', width: 50, align: 'center', sortable:false,resizable:false, formatter: DeliveryGroupCatalogFormatter.delCellIconCustomerFormatter}
		  ],	  
		  pager : '#pager',
		  width: ($('#cusGrid').width())	  
		})
		.navGrid('#pager', {edit:false,add:false,del:false, search: false});
		return false;
	},
	addCustomer: function(){
		var msg = '';
		$('#errMsg').html('').hide();
		msg = Utils.getMessageOfRequireCheck('customerCode','Mã KH');
		if(msg.length == 0){
			msg = Utils.getMessageOfSpecialCharactersValidate('customerCode','Mã KH',Utils._CODE);
		}		
		if(msg.length > 0){
			$('#errMsg').html(msg).show();
			return false;
		}
		var shopCode=$('#shopCode').val().trim();
		var dataModel = new Object();
		dataModel.id = $('#selId').val().trim();
		dataModel.customerCode = $('#customerCode').val().trim();
		dataModel.shopCode = shopCode;
		Utils.addOrSaveRowOnGrid(dataModel, "/catalog/delivery-group/add-customer", DeliveryGroupCatalog._xhrSave, null,null, function(data){
			if(!data.error){
				$('#customerCode').val('');
				$('#customerName').val('');
				$("#grid").trigger("reloadGrid");
			}
		});		
		return false;
	},
	deleteCustomerRow: function(id){
		var dataModel = new Object();
		//dataModel.id = $('#selId').val().trim();
		dataModel.id = id;
		Utils.deleteSelectRowOnGrid(dataModel, "khách hàng","/catalog/delivery-group/delete-customer", DeliveryGroupCatalog._xhrDel, null,null);
		$('#customerCode').val('');
		$('#customerName').val('');
		return false;
	},
	backToParentPage: function(){
		location.href = '/catalog/delivery-group';
		return false;
	},
	loadTreeEx:function(){
		DeliveryGroupCatalog.loadShopTree();
		$('#tree').bind("loaded.jstree", function(event, data){
        	if($('#treeContainer').data("jsp")!= undefined){
        		$('#treeContainer').data("jsp").destroy();
        	}
        	$('#treeContainer').jScrollPane();
        });
	},
	loadShopTree:function(){
		$('#tree').jstree({
	        "plugins": ["themes", "json_data","ui"],
	        "themes": {
	            "theme": "classic",
	            "icons": false,
	            "dots": true
	        },
	        "json_data": {
	            "ajax": {
	            	"url": function( node ){
	                      if( node == -1 ){
	                    	  return "/rest/catalog/unit-tree-ex/"+$('#shopIdHidden').val().trim()+"/0/tree.json";
	                      } else {
	                    	  return "/rest/catalog/unit-tree-ex/"+node.data("attr").id+"/1/tree.json";
	                      }
	                    },
	                "type": 'GET',
	                "success": function(ops) {
	                    data = [];
	                    for( opnum in ops ){
	                      var op = ops[opnum];
	                      node = {
	                          "data" : op.data,
	                          "attr" : op.attr,
	                          "metadata" :  op ,
	                          "state" : op.state
	                      };
	                      data.push( node );
	                    }
	                    return data;
	                },
	                'complete':function(){
	                	$('#tree').bind("select_node.jstree", function (event, data) {
	                		var name = data.inst.get_text(data.rslt.obj);
	                		var id = data.rslt.obj.attr("id");
	                		var code = data.rslt.obj.attr("contentItemId");
	                		$('#selId').val(id);
	                		$('#selCode').val(Utils.XSSEncode(code));
	                		$('#selName').val(Utils.XSSEncode(name));
	                		var allChild = true;		
	                		if(!$('#chkAllChild').is(':checked')){
	                			allChild = false;
	                		}
	                		var url = DeliveryGroupCatalog.getGridUrl('','','','',id,$('#status').val(),'',allChild);
	                		$("#grid").setGridParam({url:url,page:1}).trigger("reloadGrid");
	            	    });
	                	$('#tree').bind("loaded.jstree", function(event, data){
	                		if($('#treeContainer').data("jsp")!= undefined){
	                			$('#treeContainer').data("jsp").destroy();
	                		}
	                		$('#treeContainer').jScrollPane();
	                    });
	                	var tm = setTimeout(function() {
		                    if($('#treeContainer').data("jsp")!= undefined){
		                		$('#treeContainer').data("jsp").destroy();
		                	}
		                    $('#treeContainer').css('height',$('.Content').height()-75);
		  				  	$('#treeContainer').jScrollPane();
	                    }, 500);
	                }
	            }
	        },
	        "core" : {"load_open" : true }
		});	
	},
	exportExcelData:function(){
		$('#divOverlay').addClass('Overlay');
		$('#imgOverlay').show();
		$('#errMsg').html('').hide();
		$('#errExcelMsg').html('').hide();
		var code = $('#code').val().trim();
		var name = $('#name').val().trim();
		var staffCode = $('#staffCode').val().trim();
		var customerCode = $('#customerCode').val().trim();
		var status = $('#status').val().trim();
		var shopCode = $('#shopCode').val().trim();		
		var shopId = -2;
		var data = new Object();
		data.code = code;
		data.name = name;
		data.staffCode = staffCode;
		data.customerCode = customerCode;
		data.status = status;
		data.shopCode = shopCode;
		data.shopId = shopId;
		data.getAll = true;
		data.excelType = $('#excelType').val();
		var url = "/catalog/delivery-group/getlistexceldata";
		CommonSearch.exportExcelData(data,url);
	},
	importExcel:function(){
		$('#isView').val(0);
		var options = { 
				beforeSubmit: ProductLevelCatalog.beforeImportExcel,   
		 		success:      ProductLevelCatalog.afterImportExcelUpdate, 
		 		type: "POST",
		 		dataType: 'html',
		 		data:({excelType:$('#excelType').val()})
		 	}; 
		$('#importFrm').ajaxForm(options);
 		$('#importFrm').submit();
		return false;
	},	
	viewExcel:function(){
		$('#isView').val(1);
		var options = { 
				beforeSubmit: ProductLevelCatalog.beforeImportExcel,   
		 		success:      ProductLevelCatalog.afterImportExcelUpdate, 
		 		type: "POST",
		 		dataType: 'html',
		 		data:({excelType:$('#excelType').val()})
		 	}; 
		$('#importFrm').ajaxForm(options);
 		$('#importFrm').submit();
		return false;
	}
};
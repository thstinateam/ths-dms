var BrandCatalog = {
	_xhrSave : null,
	_xhrDel: null,
	getGridUrl: function(brandCode,brandName,description,status){
		return "/catalog/brand/search?code=" + encodeChar(brandCode) + "&name=" + encodeChar(brandName) + "&note=" + encodeChar(description) + "&status=" + status;
	},	
	searchBrand: function(){
		$('#productContainerDiv').hide();
		$('#errMsg').html('').hide();
		var params = new Object();		
		params.code = $('#brandCode').val().trim();
		params.name = $('#brandName').val().trim();
		params.status = 1;
		var tm = setTimeout(function() {
			$('#brandCode').focus();
		}, 500);
		$("#grid").datagrid('load',params);	
		return false;
	},
	saveBrand: function(){
		var msg = '';
		$('.ErrorMsgStyle').hide();
		msg = Utils.getMessageOfRequireCheck('brandCodePop','Mã nhãn hàng');
		if(msg.length == 0){
			msg = Utils.getMessageOfSpecialCharactersValidate('brandCodePop','Mã nhãn hàng',Utils._CODE);
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfRequireCheck('brandNamePop','Tên nhãn hàng');
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfSpecialCharactersValidate('brandNamePop','Tên nhãn hàng');
		}
		if(msg.length > 0){
			$('#errMsg').html(msg).show();
			return false;
		}
		var dataModel = new Object();
		dataModel.id = $('#selBrandId').val().trim();
		dataModel.code = $('#brandCodePop').val().trim();
		dataModel.name = $('#brandNamePop').val().trim();
		dataModel.status = 1;
		Utils.addOrSaveRowOnGrid(dataModel, "/catalog/brand/save", BrandCatalog._xhrSave, null, null, function(data){
			if(!data.error){
				$('#popup1').dialog('close');
				BrandCatalog.searchBrand();
			}
		});		
		return false;
	},
	getSelectedBrand: function(index){
		$('.ErrorMsgStyle').hide();
		$('.easyui-dialog input').val('');
		$('#brandCodePop').removeAttr('disabled');
		if(index != null && index !=undefined){
			var row = $('#grid').datagrid('getRows')[index];
			$('#selBrandId').val(row.id);			
			$('#brandCodePop').val(row.productInfoCode);
			$('#brandNamePop').val(row.productInfoName);
			$('#brandCodePop').attr('disabled','disabled');
		}else{
			$('#selBrandId').val(0);
		}
        $('#popup1').dialog('open');
		return false;
	},
	resetForm: function(){
		BrandCatalog.clearData();	
		$("#grid").trigger("reloadGrid");
		return false;
	},
	deleteBrand: function(brandCode){
		var dataModel = new Object();
		dataModel.code = brandCode;	
		Utils.deleteSelectRowOnGrid(dataModel, 'brand', '/catalog/brand/delete', BrandCatalog._xhrDel, null, null,function(data){
			BrandCatalog.clearData();
		});
		return false;
	},
	clearData: function(){
		setTitleSearch();
		$('.RequireStyle').hide();
		$('#errMsg').html('').hide();
		$('#selBrandId').val(0);
		$('#brandCode').removeAttr('disabled');
		$('#btnCancel').hide();
		$('#btnSearch').show();
		$('#btnAdd').show();
		$('#brandCode').val('');
		$('#brandName').val('');
		focusFirstTextbox();
		setSelectBoxValue('status', 1);	
		BrandCatalog.searchBrand();
	},
	getChangedForm: function(){
		$('.RequireStyle').show();
		$('#btnSearch').hide();
		$('#btnAdd').hide();
		$('#btnCancel').show();	
		focusFirstTextbox();
	},
	loadProductOfBrand : function(index){
		$('.ErrorMsgStyle').hide();
		$('#productContainerDiv').show();
		var row = $('#grid').datagrid('getRows')[index];
		$('#productTitle').html('Danh sách sản phẩm thuộc nhãn hàng : s' + Utils.XSSEncode(row.productInfoCode));
		$("#productGrid").datagrid({
			  url : '/catalog/brand/product',
			  queryParams : {
				  id : row.id
			  },
			  width: $(window).width() - 50,
			  pageSize : 10,
			  height:'auto',
			  scrollbarSize : 0,
			  pagination:true,
			  singleSelect:true,
			  fitColumns:true,		  
			  rownumbers: true,
			  columns:[[	
				{field: 'productCode', title: 'Mã sản phẩm', width: 150, sortable:false,resizable:false, align: 'left', formatter: function(value, row, index) {
					return Utils.XSSEncode(value);
				}},
			    {field: 'productName', title: 'Tên sản phẩm', width: $(window).width() - 350, sortable:false,resizable:false, align: 'left' , formatter: function(value, row, index) {
					return Utils.XSSEncode(value);
				}},
			    {field: 'categoryCode', title: 'Ngành hàng', width: 100, align: 'left',sortable:false,resizable:false, formatter: function(value, row, index) {
					return Utils.XSSEncode(value);
				}},						   					    
			    {field: 'del', title: '<img src="/resources/images/icon_add.png" style="cursor:pointer" alt="Thêm mới" title="Thêm mới" onclick="BrandCatalog.selectListProduct('+row.id+')" />', width: 50, align: 'center',sortable:false,resizable:false, 
			    	formatter:function(value, row,index){
			    		var html = '<a onclick="BrandCatalog.resetBrand({0})" href="javascript:void(0)"><img title="Xóa" src="/resources/images/icon_delete.png"></a>';
			    		return format(html,row.id);
			    	}
			   	},
			  ]],
			 
			  onLoadSuccess:function(){
			      	$('#productGrid').datagrid('resize');
			      	var tm = setTimeout(function(){
			      		var hg = $('.ContentSection').height() - 100; 
				      	$('.ReportTreeSection').css('height',hg);
			      	},500);
			      }
			});
	},
	selectListProduct : function(brandId){
		$('.ErrorMsgStyle').hide();
		var arrParam = new Array();
		var params = new Object();
		params.name = 'checkBrand';
		params.value = true;
		arrParam.push(params);
		CommonSearchEasyUI.openSearchStyle2EasyUIMutilSelectDialog("Mã sản phẩm", "Tên sản phẩm", "Thêm sản phẩm","/commons/search-product-brand",function(data){
			var lstProducts = new Array();
			for(var i = 0 ; i < data.length; ++i){
				lstProducts.push(data[i].id);
			}
			var params = new Object();
			params.id = brandId;
			params.lstProducts = lstProducts;
			Utils.addOrSaveData(params,'/catalog/brand/add-product',null,'errMsgId',function(result){
				$("#productGrid").datagrid('reload');
			});			
		},"productCode", "productName", arrParam);
	},
	resetBrand : function(productId){
		$('.ErrorMsgStyle').hide();
		var params = new Object();
		params.productId = productId;
		Utils.addOrSaveData(params,'/catalog/brand/reset-brand',null,'errMsgId',function(result){
			$("#productGrid").datagrid('reload');
		});		
	}
};
var BaryCentricProgrammeCatalog = {
	_xhrSave : null,
	_xhrDel: null,
	_mapShop:null,
	_mapProduct:null,
	_listShopId:null,
	_lstSaleTypeData: new Map(),
	_lstTypeData: new Map(),
	_firstCount: true,
	_importHtml: '',
	_mapLstProduct:new Map(),
	hideAllTab: function() {
		$('.ErrorMsgStyle').hide();
		$('#tab1').removeClass('Active');
		$('#tab2').removeClass('Active');
		$('#tab3').removeClass('Active');
		$('#spCTTTTab').hide();
		$('#dvtgCTTTTab').hide();
		$('#htbhCTTTTab').hide();
	},
	showTab1: function() {
		BaryCentricProgrammeCatalog.hideAllTab();
		$('#tab1').addClass('Active');
		$('#htbhCTTTTab').show();
		$('#errExcelMsg').html('').hide();
		$('.IdImportDivTpm').hide();
		$('#codeHTBH').focus();
		Utils.bindAutoButtonEx('.ContentSection', 'btnSearchSaleType');
		$('#codeHTBH').val('').change();
		$('#nameHTBH').val('').change();
		BaryCentricProgrammeCatalog.searchHTBH();
	},
	showTab3: function() {
		BaryCentricProgrammeCatalog.hideAllTab();
		$('#tab3').addClass('Active');
		$('#dvtgCTTTTab').show();
		$('#divImportGrroupSpDv').html(BaryCentricProgrammeCatalog._importHtml).change();
		$('.IdImportDivTpm').each(function() {
			$(this).prop('id', 'tab_dv_group_edit_import').addClass('cmsiscontrol');
		});
		Utils.functionAccessFillControl('divImportGrroupSpDv');
		$('.IdImportDivTpm').show();
		$('#shopCode').focus();
		Utils.bindAutoButtonEx('.ContentSection', 'btnSearchShop');
		if ($('#downloadTemplate').length > 0) {
			$('#downloadTemplate').attr( 'href', excel_template_path + 'catalog/Bieu_mau_danh_muc_NPPThamgiavaoCTTT_import.xls');
		}
		$('#excelType').val(2);
		$('#errExcelMsg').html('').hide();
		var value = $('#divWidth').width() - 50;
		$('#shopCode').val('').change();
		$('#shopName').val('').change();
		var dataWidth = (value * 80)/100;
		var editWidth = (value * 10)/100;
		var deleteWidth = (value * 9)/100+6;
		var statusPermission = $('#statusPermission').val();
		var titleOpenDialogShop = "";
		if (statusPermission == 2) {
			titleOpenDialogShop = '<a id="btnAddGridS" href="javascript:void(0);" onclick="return BaryCentricProgrammeCatalog.openShopDialog();"><img src="/resources/images/icon-add.png"></a>';
		}
		$('#exGrid').treegrid({
		    url:  BaryCentricProgrammeCatalog.getShopGridUrl($('#progId').val(), '', ''),
		    width:($('#divWidth').width()-50),  
	        height: 'auto', 
	        fitColumns: true,
	        idField: 'id',  
	        treeField: 'data',
		    columns:[[  
		        {field: 'data', title: 'Đơn vị', resizable: false, width: dataWidth, formatter: function(value, row, index) {
			    		return Utils.XSSEncode(value);
	            }},
		        {field: 'edit', title: titleOpenDialogShop, hidden: true, width: editWidth, align: 'center', resizable: false, formatter: function(value, row, index) {
			        	if (row.isShop == 0) {
			        		if (statusPermission == 2) {
			        			return '<a id="btnEditGridS"' + index+ ' title="Thêm mới" href="javascript:void(0);" onclick="BaryCentricProgrammeCatalog.openShopDialog('+row.id+ ');"><img src="/resources/images/icon-add.png"></a>';
			        		}
			        	}
		        }},
		        {field: 'delete', title: '', width: deleteWidth, hidden: true, align: 'center', resizable: false, formatter: function(value, row, index) {
		        		if (statusPermission == 2) {
		        			return '<a id="btnDelGridS"' +index+ ' title="Xóa" href="javascript:void(0);" onclick="BaryCentricProgrammeCatalog.deleteFocusShopMap('+row.id+ ');"><img src="/resources/images/icon-delete.png"></a>';
		        		}
		        }}
		    ]],
		    onLoadSuccess: function() {
		    	$('#exGrid').datagrid('resize');
		    	
		    	var arrEdit =  $('#shopGridContainer td[field="edit"]');
				if (arrEdit != undefined && arrEdit != null && arrEdit.length > 0) {
				for (var i = 0, size = arrEdit.length; i < size; i++) {
					$(arrEdit[i]).prop("id", "tab_dv_group_edit_" + i).addClass("cmsiscontrol");//Khai bao id danh cho phan quyen
				  }
				}
				var arrDelete =  $('#shopGridContainer td[field="delete"]');
				if (arrDelete != undefined && arrDelete != null && arrDelete.length > 0) {
				  for (var i = 0, size = arrDelete.length; i < size; i++) {
					$(arrDelete[i]).prop("id", "tab_dv_group_edit_" + i).addClass("cmsiscontrol");//Khai bao id danh cho phan quyen
				  }
				}
				Utils.functionAccessFillControl('shopGridContainer', function(data) {
				//Xu ly cac su kien lien quan den cac control phan quyen
				var arrTmpLength =  $('#shopGridContainer td[id^="tab_dv_group_edit_"]').length;
				var invisibleLenght = $('.isCMSInvisible[id^="tab_dv_group_edit_"]').length;
				if (arrTmpLength > 0 && arrTmpLength != invisibleLenght) {
					$('#grid').datagrid("showColumn", "delete");
				} else {
					$('#grid').datagrid("hideColumn", "delete");
				}
				arrTmpLength =  $('#shopGridContainer td[id^="tab_dv_group_edit_"]').length;
				invisibleLenght = $('.isCMSInvisible[id^="tab_dv_group_edit_"]').length;
				if (arrTmpLength > 0 && arrTmpLength != invisibleLenght) {
					$('#grid').datagrid("showColumn", "edit");
				} else {
					$('#grid').datagrid("hideColumn", "edit");
					}
				});
		    }
		});
	},
	/**
	 * Import san pham CTTT
	 * @modify hunglm16
	 * @since 03/11/2015
	 * */
	importExcel: function() {
		$('.ErrorMsgStyle').hide();
 		$('#isView').val(0);
 		var options = { 
				beforeSubmit: BaryCentricProgrammeCatalog.beforeImportExcel,   
		 		success:      BaryCentricProgrammeCatalog.afterImportExcelUpdate, 
		 		type: "POST",
		 		dataType: 'html',
		 		data:({excelType:$('#excelType').val(), id:$('#selId').val()})
		 	}; 
		$('#importFrm').ajaxForm(options);
 		$('#importFrm').submit();
 		return false;
 	},
 	/**
	 * Before - Import san pham CTTT
	 * @modify hunglm16
	 * @since 03/11/2015
	 * @description chi dung cho tab import san pham, khong ke thua
	 * , yeu cau chuyen qua dung ham chung khi phat trien chuc nang moi 
	 * */
 	beforeImportExcel: function() {
		if (!previewImportExcelFile(document.getElementById("excelFile"))) {
			return false;
		}
		$('#errExcelMsg').hide();
		$('#divOverlay').show();
		$('#imgOverlay').show();
		return true;
	},
	/**
	 * After - Import san pham CTTT
	 * @modify hunglm16
	 * @since 03/11/2015
	 * @description chi dung cho tab import san pham, khong ke thua
	 * , yeu cau chuyen qua dung ham chung khi phat trien chuc nang moi 
	 * */
	afterImportExcelUpdate: function(responseText, statusText, xhr, $form) {
		hideLoadingIcon();
		if (statusText == 'success') {
			$("#responseDiv").html(responseText);
			var newToken = $('#responseDiv #newToken').val();
			//console.log(newToken);
			if (newToken != null && newToken != undefined && newToken != '') {
				$('#token').val(newToken);
				$('#tokenImport').val(newToken);
			}
			if ($('#errorExcel').html().trim() == 'true' || $('#errorExcelMsg').html().trim().length > 0) {
				$('#errExcelMsg').html($('#errorExcelMsg').html()).show();
			} else {
				var totalRow = parseInt($('#totalRow').html().trim());
				var numFail = parseInt($('#numFail').html().trim());
				var fileNameFail = $('#fileNameFail').html();
				var mes = format(msgErr_result_import_excel, (totalRow - numFail), numFail);
				if (numFail > 0) {
					mes += ' <a href="' + fileNameFail + '">Xem chi tiết lỗi</a>';
				}
				if ($('#excelFile').length != 0 && $('#fakefilepc').length != 0) {
					try {
						$('#excelFile').val('');
						$('#fakefilepc').val('');
					} catch (err) {
					}
				}
				$('#errExcelMsg').html(mes).show();
			}
			//Reload lai grid
			$("#grid").datagrid("reload");
		}
	},
	gotoTab: function(tabIndex) {
		$('.ErrorMsgStyle').hide();
		BaryCentricProgrammeCatalog.deactiveAllMainTab();
		switch (tabIndex) {
		case 0:
			$('#tabContent1').show();
			$('#tab1 a').addClass('Active');
			break;
		case 1:
			$('#tabContent2').show();
			$('#tab2 a').addClass('Active');
			AttributesManager.getAttributesForm('tabContent2',AttributesManager.FOCUS_PROGRAM);
			$('#typeAttribute').val(AttributesManager.FOCUS_PROGRAM);
			break;
		default:
			$('#tabContent1').show();
			break;
		}
	},
	upload : function() { //upload trong fancybox
		$('.ErrorMsgStyle').hide();
		var options = { 					 		
	 		beforeSubmit: ProductLevelCatalog.beforeImportExcel,   
	 		success:      ProductLevelCatalog.afterImportExcelUpdate, 
	 		type: "POST",
	 		dataType: 'html'
	 	}; 
		$('#importShopFrm').ajaxForm(options);
		$.messager.confirm('Xác nhận', 'Bạn có muốn nhập từ file?',function(r) {
			if (r) {						
				$('#importShopFrm').submit();						
			}
		});
	},
	deactiveAllMainTab: function() {
		$('.ErrorMsgStyle').hide();
		$('#tab1 a').removeClass('Active');
		$('#tab2 a').removeClass('Active');
		$('#tabContent1').hide();
		$('#tabContent2').hide();
	},
	getGridUrl: function(code,name,shopId,fDate,tDate,status) {		
		return "/catalog/bary-centric-programme/search?code=" + encodeChar(code) + "&name=" + encodeChar(name) + "&shopId=" + shopId + "&fromDate=" + fDate + "&toDate=" + tDate + '&status=' + status;
	},	
	search: function() {
		$('.ErrorMsgStyle').hide();
		var code = $('#code').val().trim();
		var name = $('#name').val().trim();
		var shopId = $('#shopTree').combotree('getValue');//$('#shopId').val().trim();
		var fromDate = $('#fDate').val();
		var toDate = $('#tDate').val();	
		var status = 1;
		if ($('#status').length > 0) {
			status = $('#status').val().trim();
		}
		var msg = '';
//		if (msg.length == 0) {
//			msg = Utils.getMessageOfSpecialCharactersValidate('code', 'Mã CTTT',Utils._CODE);
//		}
//		if (msg.length == 0) {
//			msg = Utils.getMessageOfSpecialCharactersValidate('name', 'Tên CTTT');
//		}		 
		if (msg.length == 0) {
			msg = Utils.getMessageOfInvalidFormatDate('fDate', 'Từ ngày');
		}
		if (msg.length == 0) {
			msg = Utils.getMessageOfInvalidFormatDate('tDate', 'Đến ngày');
		}
		var fDate = $('#fDate').val();
		var tDate = $('#tDate').val();
		if (msg.length == 0) {
			if (fDate != '' && !Utils.isDate(fDate)) {
				msg = 'Từ ngày không hợp lệ, vui lòng nhập theo định dạng dd/MM/yyyy';
				$('#fDate').focus();
			}
		}
		if (msg.length == 0) {
			if (tDate != '' && !Utils.isDate(tDate)) {
				msg = 'Đến ngày không hợp lệ, vui lòng nhập theo định dạng dd/MM/yyyy';
				$('#tDate').focus();
			}
		}
		var currentTime = new Date();
		var month = currentTime.getMonth() + 1;
		var day = currentTime.getDate();
		var year = currentTime.getFullYear();
		if (msg.length == 0) {
			if (fDate != '' && tDate != '') {
				if (!Utils.compareDate(fDate, tDate)) {
					msg = 'Từ ngày phải là ngày trước hoặc cùng ngày với đến ngày';	
					$('#fDate').focus();
				}
			}
		}

		if (msg.length > 0) {
			$('#errMsg').html(msg).show();
			return false;
		}
		var tm = setTimeout(function() {
			$('#code').focus();
		}, 500);
		var url = BaryCentricProgrammeCatalog.getGridUrl(code,name,shopId,fromDate,toDate,status);
		$("#listCTTTGrid").datagrid({url:url,pageNumber:1});
		return false;
	},
	saveInfo: function() {
		var msg = '';
		$('.ErrorMsgStyle').hide();
		msg = Utils.getMessageOfRequireCheck('code', 'Mã CTTT');
		if (msg.length == 0) {
			msg = Utils.getMessageOfSpecialCharactersValidate('code', 'Mã CTTT',Utils._CODE);
		}
		if (msg.length == 0) {
			msg = Utils.getMessageOfRequireCheck('name', 'Tên CTTT');
		}
		if (msg.length == 0) {
			msg = Utils.getMessageOfSpecialCharactersValidate('name', 'Tên CTTT');
		}		
		if (msg.length == 0) {
			msg = Utils.getMessageOfRequireCheck('fDate', 'Từ ngày');
		} 
		if (msg.length == 0) {
			msg = Utils.getMessageOfInvalidFormatDate('fDate', 'Từ ngày');
		}
		if (msg.length == 0) {
			msg = Utils.getMessageOfInvalidFormatDate('tDate', 'Đến ngày');
		}
		if (msg.length == 0) {
			msg = Utils.getMessageOfRequireCheck('status', 'Trạng thái',true);
		}
		var fDate = $('#fDate').val();
		var tDate = $('#tDate').val();
		if ($('#status').val().trim() == activeType.WAITING) {
			if (msg.length == 0) {
				if (fDate != '' && !Utils.isDate(fDate)) {
					msg = 'Từ ngày không hợp lệ, vui lòng nhập theo định dạng dd/MM/yyyy';
					$('#fDate').focus();
				}
			}
		}
		if (msg.length == 0) {
//			if (($('#stableToDate').val() != '') && ($('#stableToDate').val().trim() != $('#tDate').val().trim())) {
			if ($('#stableToDate').val().trim() != $('#tDate').val().trim()) {
				if (tDate != '' && !Utils.isDate(tDate)) {
					msg = 'Đến ngày không hợp lệ, vui lòng nhập theo định dạng dd/MM/yyyy';
					$('#tDate').focus();
				}
			}
		}
		var currentTime = new Date();
		var month = currentTime.getMonth() + 1;
		var day = currentTime.getDate();
		var year = currentTime.getFullYear();
		var proStatus = $('#proStatus').val();
		if ($('#status').val().trim() == activeType.RUNNING) {
			if (msg.length == 0) {
				if (fDate != '' && tDate != '') {
					if (!Utils.compareDate(fDate, tDate)) {
						msg = 'Từ ngày phải là ngày trước hoặc cùng ngày với đến ngày';	
						$('#fDate').focus();
					}
				}
			}
		}

		/*if ($('#status').val().trim() == activeType.RUNNING && proStatus == activeType.WAITING) {
			if (msg.length == 0) {
				if (fDate != '') {
					if (!Utils.compareDate(day + '/' + month + '/' + year,fDate)) {
						msg = 'Từ ngày phải lớn hơn hoặc bằng ngày hiện tại.';	
						$('#fDate').focus(); 
					}
				}
			}
		}*/
		if (msg.length == 0) {
//			if (($('#stableToDate').val() != '') && ($('#stableToDate').val().trim() != $('#tDate').val().trim())) {
			if ($('#stableToDate').val().trim() != $('#tDate').val().trim()) {
				if (tDate != '') {
					if (!Utils.compareDate(day + '/' + month + '/' + year,tDate)) {
						msg = 'Đến ngày phải lớn hơn hoặc bằng ngày hiện tại.';	
						$('#tDate').focus();
					}
				}
			}
		}	
		if (msg.length > 0) {
			$('#errMsgInfo').html(msg).show();
			return false;
		}
		var dataModel = new Object();		
		dataModel.id = $('#selId').val().trim();
		dataModel.code = $('#code').val().trim();
		dataModel.name = $('#name').val().trim();
		dataModel.fromDate = $('#fDate').val().trim();
		dataModel.toDate = $('#tDate').val().trim();
		dataModel.status = $('#status').val().trim();
		if (!Utils.validateAttributeData(dataModel, '#errMsgInfo')) {
			return false;
		}
		Utils.addOrSaveData(dataModel, "/catalog/bary-centric-programme/save-info", BaryCentricProgrammeCatalog._xhrSave, 'errMsgInfo', function(data) {
			$('#selId').val(data.id);	
			$('#subContentId').val(0);
			$.cookie('proTabOrder', 1);
			window.location.href = '/catalog/bary-centric-programme/change?id=' + data.id + '&subContentId=0';	
//			$('#subContent').hide();
			showSuccessMsg('successMsg1',data);
		});		
		return false;
	},
	getSelectedProgram: function(id) {
		if (id!= undefined && id!= null) {
			location.href = '/catalog/bary-centric-programme/change?id=' + id + '&subContentId=1';			
		} else {
			$('#subContentId').val(0);
			location.href = '/catalog/bary-centric-programme/change?subContentId=0';
		}		
		return false;
	},
	saveShop: function() {
		var msg = '';
		$('.ErrorMsgStyle').hide();
		var areaId = -2;
		var regionId = -2;
		var shopCode = '';
		var shopMapId = $('#selShopMapId').val().trim();
		if (shopMapId.length == 0 || shopMapId == 0) {
			if ($('#rbArea').is(':checked')) {
				msg = Utils.getMessageOfRequireCheck('area', 'Miền');
				areaId = $('#area').val();
			}
			if ($('#rbRegion').is(':checked')) {
				msg = Utils.getMessageOfRequireCheck('region', 'Vùng');
				regionId = $('#region').val();
			}
			if ($('#rbShop').is(':checked')) {
				msg = Utils.getMessageOfRequireCheck('shopCode', 'Mã đơn vị');
				if (msg.length == 0) {
					msg = Utils.getMessageOfSpecialCharactersValidate('shopCode', 'Mã đơn vị',Utils._CODE);
				}
				shopCode = $('#shopCode').val();
			}
		}
		
		if (msg.length == 0) {
			msg = Utils.getMessageOfRequireCheck('shopStatus', 'Trạng thái',true);
		}
		if (msg.length > 0) {
			$('#errMsgShop').html(msg).show();
			return false;
		}
		var dataModel = new Object();		
		dataModel.id = $('#selId').val().trim();
		dataModel.shopMapId = $('#selShopMapId').val().trim();
		dataModel.areaId = areaId;
		dataModel.regionId = regionId;		
		dataModel.shopCode = shopCode;		
		dataModel.status = $('#shopStatus').val().trim();
		Utils.addOrSaveRowOnGrid(dataModel, "/catalog/bary-centric-programme/save-shop", BaryCentricProgrammeCatalog._xhrSave, 'shopGrid', 'errMsgShop', function(data) {
			$('#selShopMapId').val(0);	
			$("#shopGrid").trigger("reloadGrid");
			BaryCentricProgrammeCatalog.resetShopForm();
			showSuccessMsg('successMsgShop',data);
		}, 'shopLoading');		
		return false;
	},
	getSelectedShopRow: function(rowId,status) {
		var code = $("#shopGrid").jqGrid ('getCell', rowId, 'shop.shopCode');
		var name = $("#shopGrid").jqGrid ('getCell', rowId, 'shop.shopName');
		var id = $("#shopGrid").jqGrid ('getCell', rowId, 'id');		
		$('#selShopMapId').val(id);
		$('#shopCode').val(code);
		$('#shopName').val(name);
		setSelectBoxValue('shopStatus',Utils.getStatusValue(status));
		disabled('shopCode');
		$('#btnAddShop').hide();
		$('#btnEditShop').show();
		$('#btnCancelShop').show();
		return false;
	},
	resetShopForm: function() {
		$('.ErrorMsgStyle').hide();
		$('#btnAddShop').show();
		$('#btnEditShop').hide();
		$('#btnCancelShop').hide();
		enable('shopCode');
		setSelectBoxValue('shopStatus');
		setSelectBoxValue('area');
		setSelectBoxValue('region');
		$('#selShopMapId').val(0);
		$('#shopCode').val('');
		$('#shopName').val('');
	},
	deleteShopRow: function(id) {
		$('.ErrorMsgStyle').hide();
		var dataModel = new Object();
		dataModel.shopMapId = id;	
		Utils.deleteSelectRowOnGrid(dataModel, 'đơn vị', '/catalog/bary-centric-programme/delete-shop', BaryCentricProgrammeCatalog._xhrDel, 'shopGrid', 'errMsgShop',function(data) {
			showSuccessMsg('successMsgShop',data);
		}, 'shopLoading');		
		return false;
	},
	saveProduct: function() {
		var msg = '';
		$('.ErrorMsgStyle').hide();
		var catCode = '';
		var subCatCode = '';
		var productCode = '';
		var staffTypeId = $('#staffType').val();
		if ($('#rbCat').is(':checked')) {
			msg = Utils.getMessageOfRequireCheck('catCode', 'Ngành hàng');
			catCode = $('#catCode').val();
		}
		if ($('#rbSubCat').is(':checked')) {
			msg = Utils.getMessageOfRequireCheck('subCatCode', 'Ngành hàng con');
			subCatCode = $('#subCatCode').val();
		}
		if ($('#rbProduct').is(':checked')) {
			msg = Utils.getMessageOfRequireCheck('productCode', 'Mã sản phẩm');			
			productCode = $('#productCode').val();
		}
		if (msg.length == 0) {
			msg = Utils.getMessageOfRequireCheck('staffType', 'Loại nhân viên',true);
		}
		
		if (msg.length > 0) {
			$('#errMsgProduct').html(msg).show();
			return false;
		}
		var dataModel = new Object();
		dataModel.id = $('#selId').val().trim();
		dataModel.catCode = catCode;
		dataModel.subCatCode = subCatCode;		
		dataModel.productCode = productCode;
		dataModel.staffTypeId = staffTypeId;
		Utils.addOrSaveRowOnGrid(dataModel, "/catalog/bary-centric-programme/save-product", BaryCentricProgrammeCatalog._xhrSave, 'productGrid', 'errMsgProduct', function(data) {
			$("#productGrid").trigger("reloadGrid");
			$('#catCode').val('');
			$('#subCatCode').val('');
			$('#productCode').val('');
			$('#productName').val('');
			setSelectBoxValue('staffType');		
			showSuccessMsg('successMsgProduct',data);
		}, 'productLoading');		
		return false;
	},
	showOthersTab: function() {
		$('#tabActive2').removeClass('Disable');
		$('#tabActive3').removeClass('Disable');
		$('#tabActive4').removeClass('Disable');
		$('#tabActive5').removeClass('Disable');
		$('#tab1').bind('click',BaryCentricProgrammeCatalog.showTab1);
		$('#tab2').bind('click',BaryCentricProgrammeCatalog.showTab2);
		$('#tab3').bind('click',BaryCentricProgrammeCatalog.showTab3);
	}
	,deleteProductRow: function(id) {
		$('.ErrorMsgStyle').hide();
		var dataModel = new Object();
		dataModel.productId = id;	
		Utils.deleteSelectRowOnGrid(dataModel, 'Sản phẩm', '/catalog/bary-centric-programme/delete-product', BaryCentricProgrammeCatalog._xhrDel, 'productGrid', 'errMsgProduct',function(data) {
			showSuccessMsg('successMsgProduct',data);
		}, 'productLoading');		
		return false;
	},
	searchShop: function() {
		$('.ErrorMsgStyle').hide();
		var code = $('#shopCode').val().trim();
		var name = $('#shopName').val().trim();
		var id = $('#progId').val();
		var url = BaryCentricProgrammeCatalog.getShopGridUrl(id, code,name);
		$("#exGrid").treegrid({url:url});
	},
	showDialogCreateShop: function() {
		$('.ErrorMsgStyle').hide();
		var html = $('#shopTreeDialog').html();
		$.fancybox(html,
				{
					modal: true,
					autoResize: false,
					title: 'Thêm đơn vị tham gia CTTT',
					afterShow: function() {
						Utils.resetCheckboxStateOfTree();
						Utils.bindAutoSearch();	
						$('#shopTreeDialog').html('');
						$('#successMsgShopDlg').html('').hide();
						var focusProgramId = $('#selId').val();
						$('#shopCodeDlg').focus();
						var shopCode = encodeURI('');
						var shopName = encodeURI('');	
						BaryCentricProgrammeCatalog.loadShopTreeOnDialog('/rest/catalog/focus-program/shop/list.json?focusProgramId=' + focusProgramId + '&shopCode=' + encodeChar(shopCode) + '&shopName=' + encodeChar(shopName));
						BaryCentricProgrammeCatalog._mapShop = new Map();
						$('#shopTree').bind("loaded.jstree open_node.jstree close_node.jstree", function (event, data) {
							if ($('#shopTreeContent').data('jsp') != undefined || $('#shopTreeContent').data('jsp') != null) {
								$('#shopTreeContent').data('jsp').destroy();
								setTimeout(function() {
									$('#shopTreeContent').jScrollPane();
								},500);	
							} else {
								setTimeout(function() {
									$('#shopTreeContent').jScrollPane();
								},500);	
							}						
		    			});
					},
					afterClose: function() {
						$('#shopTreeDialog').html(html);						
					}
				}
			);
		return false;
	},
	getCheckboxStateOfTreeEx: function(treeId,MAP,selector,nodeType) {
		for (var i=0;i<MAP.size();++i) {
			var _obj = MAP.get(MAP.keyArray[i]);
			var type = true;			
			$('#' + selector).each(function() {
				var _id = $(this).attr('id');
				if (nodeType!=null && nodeType!=undefined) {
					var getType = $(this).attr('contentitemid');
					type = (nodeType==getType);
				}
				if (_id==_obj && type) {
					$(this).removeClass('jstree-unchecked');
					$(this).addClass('jstree-checked');
				}
			});
		}
	},	
	searchShopOnTree: function() {
		$('.ErrorMsgStyle').hide();
		//Utils.getCheckboxStateOfTree();
		$('#shopTree li').each(function() {
			var _id = $(this).attr('id');
			if ($(this).hasClass('jstree-checked')) {
				BaryCentricProgrammeCatalog._mapShop.put(_id,_id);	
			} else {
				BaryCentricProgrammeCatalog._mapShop.remove(_id);
			}		
		});
		var focusProgramId = $('#selId').val();
		var shopCode = encodeChar($('#shopCodeDlg').val().trim());
		var shopName = encodeChar($('#shopNameDlg').val().trim());
		$('#shopTreeContent').data('jsp').destroy();
		BaryCentricProgrammeCatalog.loadShopTreeOnDialog('/rest/catalog/focus-program/shop/list.json?focusProgramId=' + focusProgramId + '&shopCode=' + encodeChar(shopCode) + '&shopName=' + encodeChar(shopName));
		setTimeout(function() {
			$('#shopTreeContent').jScrollPane();
			$('#shopTree').bind("loaded.jstree open_node.jstree close_node.jstree", function (event, data) {
				if ($('#shopTreeContent').data('jsp') != undefined || $('#shopTreeContent').data('jsp') != null) {
					$('#shopTreeContent').data('jsp').destroy();
					setTimeout(function() {
						$('#shopTreeContent').jScrollPane();
					},500);	
				} else {
					setTimeout(function() {
						$('#shopTreeContent').jScrollPane();
					},500);	
				}
				BaryCentricProgrammeCatalog.getCheckboxStateOfTreeEx('shopTree',BaryCentricProgrammeCatalog._mapShop, 'shopTree li');
			});
			BaryCentricProgrammeCatalog.getCheckboxStateOfTreeEx('shopTree',BaryCentricProgrammeCatalog._mapShop, 'shopTree li');
		}, 500);
		$('.fancybox-inner #shopCodeDlg').focus();
	},
	selectListShop: function() {
		$('.ErrorMsgStyle').hide();
		$('#successMsgShopDlg').html('').hide();
		$.fancybox.update();		
		var arrId = new Array();
		$('.jstree-checked').each(function() {
		    var _id= $(this).attr('id');
		    if ($('#' + _id + ' ul li').length >0 && (!$(this).parent().parent().hasClass('jstree-checked'))) {
		    	BaryCentricProgrammeCatalog._mapShop.put(_id,_id);	
		    } else if ($('#' + _id ).length >0 && (!$(this).parent().parent().hasClass('jstree-checked'))) {
		    	BaryCentricProgrammeCatalog._mapShop.put(_id,_id);	
			} else {
		    	BaryCentricProgrammeCatalog._mapShop.remove(_id);	
			}	
		});
//		$('#shopTree li').each(function() {
//			var _id = $(this).attr('id');			
//			if ($(this).hasClass('jstree-checked')) {
//				BaryCentricProgrammeCatalog._mapShop.put(_id,_id);		
//			} else {
//				BaryCentricProgrammeCatalog._mapShop.remove(_id);	
//			}			
//		});
		for (var i=0;i<BaryCentricProgrammeCatalog._mapShop.size();++i) {
			var _obj = BaryCentricProgrammeCatalog._mapShop.get(BaryCentricProgrammeCatalog._mapShop.keyArray[i]);
			arrId.push(_obj);
		}		
		var msg = '';
		if (arrId.length == 0) {
			msg = format(msgErr_required_choose_format, 'Đơn vị');
		}		
		if (msg.length > 0) {
			$('#errMsgShopDlg').html(msg).show();			
			$.fancybox.update();
			return false;
		}		
		var focusProgramId = $('#selId').val();
		var dataModel = new Object();		
		dataModel.lstShopId = arrId;		
		dataModel.id = focusProgramId;
		dataModel.status = -2;
		Utils.addOrSaveData(dataModel, '/catalog/bary-centric-programme/shop/save', BaryCentricProgrammeCatalog._xhrSave, 'errMsgShopDlg', function(data) {
			$('#successMsgShopDlg').html('Lưu dữ liệu thành công').show();
			$('#shopCodeDlg').val('');
			$('#shopNameDlg').val('');
			$('#shopCode').val('');
			$('#shopName').val('');
			setSelectBoxValue('shopStatus', activeType.RUNNING);
			Utils.resetCheckboxStateOfTree();
			BaryCentricProgrammeCatalog.searchShopOnTree();	
			BaryCentricProgrammeCatalog.searchShop();
			$(window).resize();
			$.fancybox.update();
			//$("#shopGrid").trigger("reloadGrid");
			setTimeout(function() {$.fancybox.close();}, 1000);
		}, null, '.fancybox-inner', null, null);
		return false;
	},
	showUpdateShopDetailDialog: function(rowId,status) {
		$('.ErrorMsgStyle').hide();
		var html = $('#shopDetailTreeDialog').html();
		$.fancybox(html,
				{
					modal: true,
					autoResize: false,
					title: 'Cập nhật thông tin đơn vị',					
					afterShow: function() {
						$('#shopDetailTreeDialog').html('');
						$('#errMsgShopDetailDlg').html('').hide();
						$('#successMsgShopDetailDlg').html('').hide();						
						
						var name = $("#shopGrid").jqGrid ('getCell', rowId, 'shop.shopName');
						var id = $("#shopGrid").jqGrid ('getCell', rowId, 'shop.id');					    						
						if (status == 'RUNNING') {
							status = 1;
						} else if (status == 'STOPPED') {
							status = 0;
						} else {
							status = -2;
						}						
						$('#shopNameDetailDlg').val(Utils.XSSEncode(name));
						$('#shopStatusDlgTmp').attr('id', 'shopStatusDlg');
						$('#shopStatusDlg').val(Utils.XSSEncode(status));
						$('#shopStatusDlg').addClass('MySelectBoxClass');
						$('#shopStatusDlg').customStyle();						
						$('#shopIdDetailDlg').val(id);
					},
					afterClose: function() {
						$('#shopDetailTreeDialog').html(html);						
					}
				}
			);
		return false;
	},
	saveDetailShop: function() {
		$('.ErrorMsgStyle').hide();
		$('#successMsgShopDetailDlg').html('').hide();
		$.fancybox.update();
		var msg = '';
		msg = Utils.getMessageOfRequireCheck('shopStatusDlg', 'Trạng thái',true);		
		if (msg.length > 0) {
			$('#errMsgShopDetailDlg').html(msg).show();
			$.fancybox.update();
			return false;
		}		
		var focusProgramId = $('#selId').val();
		var arrId = new Array();
		arrId.push($('#shopIdDetailDlg').val());
		var dataModel = new Object();
		dataModel.lstShopId = arrId;
		dataModel.id = focusProgramId;
		dataModel.status = $('#shopStatusDlg').val().trim();
		dataModel.numUpdate = 0; 
		Utils.addOrSaveData(dataModel, '/catalog/bary-centric-programme/shop/save', ProgrammeDisplayCatalog._xhrSave, 'errMsgShopDlg', function(data) {
			$('#successMsgShopDlg').html('Lưu dữ liệu thành công').show();						
			$.fancybox.update();
			BaryCentricProgrammeCatalog.searchShop();
			setTimeout(function() {$.fancybox.close();}, 1000);
		}, null, '.fancybox-inner', null, null);
		return false;
	},
	showDialogCreateProduct: function() {
		$('.ErrorMsgStyle').hide();
		var html = $('#productTreeDialog').html();
		$.fancybox(html,
				{
					modal: true,
					autoResize: false,
					title: 'Thêm thông tin sản phẩm',
					afterShow: function() {
						Utils.resetCheckboxStateOfTree();
						Utils.bindAutoSearch();	
						$('#productTreeDialog').html('');
						var focusProgramId = $('#selId').val();
						$('#productCodeDlg').focus();
						$('#saleManDlgTmp').attr('id', 'saleManDlg');
						$('#saleManDlg').addClass('MySelectBoxClass');
						$('#saleManDlg').customStyle();
						$('#typeDialog').addClass('MySelectBoxClass');
						$('#typeDialog').customStyle();
						$.getJSON('/rest/catalog/focus-program/' + focusProgramId + '/staff-type/list.json', function(data) {
							var arrHtml = new Array();
							arrHtml.push('<option value="-2">-Chọn hình thức bán hàng-</option>');
							for (var i=0;i<data.length;i++) {
								arrHtml.push('<option value="' + Utils.XSSEncode(data[i].value) + '">' + Utils.XSSEncode(data[i].name) + '</option>');
							}
							$('#saleManDlg').html(arrHtml.join(""));
							$('#saleManDlg').change();
						});						
						var productCode = encodeURI('');
						var productName = encodeURI('');						
						BaryCentricProgrammeCatalog.loadDataForTreeWithCheckboxEx('/rest/catalog/focus-program/product/list.json?focusProgramId=' + focusProgramId + '&productCode=' + encodeChar(productCode) + '&productName=' + encodeChar(productName) + '&nodeType=cat&catId=0&subCatId=0', 'productTree');
						BaryCentricProgrammeCatalog._mapProduct = new Map();
						$('#productTree').bind("loaded.jstree open_node.jstree close_node.jstree", function () {
							if ($('#productTreeContent').data('jsp') != undefined || $('#productTreeContent').data('jsp') != null) {
								$('#productTreeContent').data('jsp').destroy();
								setTimeout(function() {
									$('#productTreeContent').jScrollPane();
								},500);	
							} else {
								setTimeout(function() {
									$('#productTreeContent').jScrollPane();
								},500);	
							}
		    			});
					},
					afterClose: function() {
						$('#productTreeDialog').html(html);						
					}
				}
			);
		return false;
	},
	searchProductOnTree: function() {
		$('.ErrorMsgStyle').hide();
		//Utils.getCheckboxStateOfTree();
		$('#productTree li').each(function() {
			var _id = $(this).attr('id');
			var type = $(this).attr('contentitemid');
			if ($(this).hasClass('jstree-checked') && type=='product') {
				BaryCentricProgrammeCatalog._mapProduct.put(_id,_id);		
			} else {
				BaryCentricProgrammeCatalog._mapProduct.remove(_id);
			}
		});
		var focusProgramId = $('#selId').val();
		var productCode = encodeChar($('#productCodeDlg').val().trim());
		var productName = encodeChar($('#productNameDlg').val().trim());
		$('#productTreeContent').data('jsp').destroy();
		BaryCentricProgrammeCatalog.loadDataForTreeWithCheckboxEx('/rest/catalog/focus-program/product/list.json?focusProgramId=' + focusProgramId + '&productCode=' + encodeChar(productCode) + '&productName=' + encodeChar(productName) + '&nodeType=cat&catId=0&subCatId=0', 'productTree');
		setTimeout(function() {
			$('#productTreeContent').jScrollPane();
			$('#productTree').bind("loaded.jstree open_node.jstree close_node.jstree", function (event, data) {
				//Utils.applyCheckboxStateOfTree();
				if ($('#productTreeContent').data('jsp') != undefined || $('#productTreeContent').data('jsp') != null) {
					$('#productTreeContent').data('jsp').destroy();
					setTimeout(function() {
						$('#productTreeContent').jScrollPane();
					},500);	
				} else {
					setTimeout(function() {
						$('#productTreeContent').jScrollPane();
					},500);	
				}
				BaryCentricProgrammeCatalog.getCheckboxStateOfTreeEx('productTree',BaryCentricProgrammeCatalog._mapProduct, 'productTree li', 'product');
			});
			BaryCentricProgrammeCatalog.getCheckboxStateOfTreeEx('productTree',BaryCentricProgrammeCatalog._mapProduct, 'productTree li', 'product');
		}, 500);
		$('.fancybox-inner #productCodeDlg').focus();
	},
	selectListProduct: function() {
		$('.ErrorMsgStyle').hide();
		$('#successMsgProductDlg').html('').hide();
		$.fancybox.update();
		var arrId = new Array();
/*		$('.jstree-leaf').each(function() {
			if ($(this).hasClass('jstree-checked')) {
				arrId.push($(this).attr('id'));
			}			
		});
		$('li[classstyle=product]').each(function() {
		    if ($(this).hasClass('jstree-checked')) {
		    	arrId.push($(this).attr('id'));
		    }
		});*/
		$('#productTree li').each(function() {
			var _id = $(this).attr('id');
			var type = $(this).attr('contentitemid');
			if ($(this).hasClass('jstree-checked') && type=='product') {
				BaryCentricProgrammeCatalog._mapProduct.put(_id,_id);		
			} else {
				BaryCentricProgrammeCatalog._mapProduct.remove(_id);	
			}			
		});
		for (var i=0;i<BaryCentricProgrammeCatalog._mapProduct.size();++i) {
			var _obj = BaryCentricProgrammeCatalog._mapProduct.get(BaryCentricProgrammeCatalog._mapProduct.keyArray[i]);
			arrId.push(_obj);
		}
		var msg = '';
		if (arrId.length == 0) {
			msg = format(msgErr_required_choose_format, 'Sản phẩm');
		}	
		if (msg.length ==0) {
			msg = Utils.getMessageOfRequireCheck('saleManDlg', 'Hình thức bán hàng',true);
		}
		if (msg.length ==0) {
			msg = Utils.getMessageOfRequireCheck('typeDialog', 'Loại MHTT',true);
		}
		if (msg.length > 0) {
			$('#errMsgProductDlg').html(msg).show();
			$.fancybox.update();
			return false;
		}		
		var focusProgramId = $('#selId').val();
		var dataModel = new Object();		
		dataModel.lstProductId = arrId;		
		dataModel.id = focusProgramId;
		dataModel.channelTypeId = $('#saleManDlg').val().trim();
		dataModel.type = $('#typeDialog').val().trim();
		Utils.addOrSaveData(dataModel, '/catalog/bary-centric-programme/product/save', ProgrammeDisplayCatalog._xhrSave, 'errMsgProductDlg', function(data) {
			$('#successMsgProductDlg').html('Lưu dữ liệu thành công').show();
			$('#productCodeDlg').val('');
			$('#productNameDlg').val('');	
			$('#productCode').val('');
			$('#productName').val('');
			setSelectBoxValue('saleManDlg');
			setSelectBoxValue('typeDialog');
			setSelectBoxValue('type');
			Utils.resetCheckboxStateOfTree();
			BaryCentricProgrammeCatalog.searchProductOnTree();
			BaryCentricProgrammeCatalog.searchProduct();
			$.fancybox.update();
			//$("#productGrid").trigger("reloadGrid");
			setTimeout(function() {$.fancybox.close();}, 1000);
		}, null, '.fancybox-inner', null, null);
		return false;
	},	
	showUpdateProductDialog: function(rowId) {
		$('.ErrorMsgStyle').hide();
		var html = $('#productDetailTreeDialog').html();
		$.fancybox(html,
				{
					modal: true,
					autoResize: false,
					title: 'Cập nhật thông tin sản phẩm',					
					afterShow: function() {
						$('#productDetailTreeDialog').html('');
						$('#errMsgProductDetailDlg').html('').hide();
						$('#successMsgProductDetailDlg').html('').hide();
						var focusProgramId = $('#selId').val();
						$('#saleManDetailDlgTmp').attr('id', 'saleManDetailDlg');
						$('#saleManDetailDlg').addClass('MySelectBoxClass');
						$('#saleManDetailDlg').customStyle();		
						$('#typeDetailDialog').addClass('MySelectBoxClass');
						$('#typeDetailDialog').customStyle();
						$.getJSON('/rest/catalog/focus-program/' + focusProgramId + '/staff-type/list.json', function(data) {
							var arrHtml = new Array();
							arrHtml.push('<option value="-2">--- Chọn kiểu bán hàng ---</option>');
							for (var i=0;i<data.length;i++) {
								arrHtml.push('<option value="' + Utils.XSSEncode(data[i].value) + '">' + Utils.XSSEncode(data[i].name) + '</option>');
							}
							//Chu y cho nay: vi focusChannelMap.staffSale.id da bi bo di
//							var channelTypeId = $("#productGrid").jqGrid ('getCell', rowId, 'focusChannelMap.staffSale.id');
							$('#saleManDetailDlg').html(arrHtml.join(""));
							var saleStaffType = $("#productGrid").jqGrid ('getCell', rowId, 'focusChannelMap.saleTypeCode');
							$.getJSON('/rest/catalog/focus-program/apparam/' + saleStaffType + '/id.json', function(data) {
								$('#saleManDetailDlg').val(Utils.XSSEncode(data.value));
								$('#saleManDetailDlg').change();
							});
						});
						var name = $("#productGrid").jqGrid ('getCell', rowId, 'product.productName');						
						var productId = $("#productGrid").jqGrid ('getCell', rowId, 'product.id');
						$('#productNameDetailDlg').val(name);
						$('#productIdDetailDlg').val(productId);
						var type = $("#productGrid").jqGrid ('getCell', rowId, 'type');
						//Chu y cho nay vi: type da chuyen sang kieu String//
//						type = focusProductType.parseValue(type);
//						setSelectBoxValue('typeDetailDialog', type);
						$.getJSON('/rest/catalog/focus-program/apparam/' + type + '/id.json', function(data) {
							setSelectBoxValue('typeDetailDialog', data.value);
						});						
					},
					afterClose: function() {
						$('#productDetailTreeDialog').html(html);						
					}
				}
			);
		return false;
	},
	saveDetailProduct: function() {
		$('.ErrorMsgStyle').hide();
		$('#successMsgProductDetailDlg').html('').hide();
		$.fancybox.update();		
		var msg = '';			
		if (msg.length ==0) {
			msg = Utils.getMessageOfRequireCheck('saleManDetailDlg', 'Hình thức bán hàng',true);
		}
		if (msg.length ==0) {
			msg = Utils.getMessageOfRequireCheck('typeDetailDialog', 'Loại MHTT',true);
		}
		if (msg.length > 0) {
			$('#errMsgProductDetailDlg').html(msg).show();
			$.fancybox.update();
			return false;
		}	
		var arrId = new Array();
		arrId.push($('#productIdDetailDlg').val().trim());
		var focusProgramId = $('#selId').val();
		var dataModel = new Object();		
		dataModel.lstProductId = arrId;		
		dataModel.id = focusProgramId;
		dataModel.channelTypeId = $('#saleManDetailDlg').val().trim();
		dataModel.type = $('#typeDetailDialog').val().trim();
		Utils.addOrSaveData(dataModel, '/catalog/bary-centric-programme/product/save', ProgrammeDisplayCatalog._xhrSave, 'errMsgProductDetailDlg', function(data) {
			$('#successMsgProductDetailDlg').html('Lưu dữ liệu thành công').show();						
			$.fancybox.update();
			$("#productGrid").trigger("reloadGrid");
			setTimeout(function() {$.fancybox.close();}, 1000);
		}, null, '.fancybox-inner', null, null);
		return false;
	},
	loadShopTreeOnDialog: function(url) {
		$('.ErrorMsgStyle').hide();
		$.getJSON(url, function(data) {
			$('#shopTree').bind("loaded.jstree open_node.jstree close_node.jstree", function (event, data) {				
				if ($('#shopTreeContent').data('jsp') != undefined || $('#shopTreeContent').data('jsp') != null) {
					$('#shopTreeContent').data('jsp').destroy();
					setTimeout(function() {
						$('#shopTreeContent').jScrollPane();
					},500);	
				} else {
					setTimeout(function() {
						$('#shopTreeContent').jScrollPane();
					},500);	
				}
			}).jstree({
		        "plugins": ["themes", "json_data","ui","checkbox"],
		        "themes": {
		            "theme": "classic",
		            "icons": false,
		            "dots": true
		        },
		        "json_data": {
		        	"data": data,
		        	"ajax" : {
		        		"method": "GET",
		                "url" : '/rest/catalog/sub-shop/list.json?focusProgramId=' +$('#selId').val(),
		                "data" : function (n) {
		                        return { id : n.attr ? n.attr("id") : 0 };
		                    }
		            }
		        }
			});
		});
	},
	loadDataForTreeWithCheckbox: function(url,treeId) {
		var tId = 'tree';
		if (treeId!= null && treeId!= undefined) {
			tId = treeId;
		}
		$('#' + tId).jstree({
	        "plugins": ["themes", "json_data","ui","checkbox"],
	        "themes": {
	            "theme": "classic",
	            "icons": false,
	            "dots": true
	        },
	        "json_data": {
	        	"ajax" : {
	        		"method": "post",
	                "url" : url,
	                "data" : function (n) {
	                        return { id : n.attr ? n.attr("id") : 0 };
	                    }
	            }
	        }
		});	
	},
	loadDataForTreeWithCheckboxEx: function(url,treeId) {
		var tId = 'tree';
		if (treeId!= null && treeId!= undefined) {
			tId = treeId;
		}
		$('#' + tId).jstree({
	        "plugins": ["themes", "json_data","ui","checkbox"],
	        "themes": {
	            "theme": "classic",
	            "icons": false,
	            "dots": true
	        },
	        "json_data": {
	        	"ajax" : {
	        		"method": "get",
	                "url" : function( node ) {	                	
	                      if (node == -1) {
	                    	  return url;
	                      } else {
	                    	  var nodeType = node.attr("classStyle");
	                    	  var catId = 0;
	                    	  var subCatId = 0;
	                    	  if (nodeType == 'cat') {
	                    		  catId = node.attr("id");
	                    		  var focusProgramId = $('#selId').val();
	                    		  var productCode = encodeURI('');
	                    		  var productName = encodeURI('');	
	                    		  return '/rest/catalog/focus-program/product/list.json?focusProgramId=' + focusProgramId + '&productCode=' + encodeChar(productCode) + '&productName=' + encodeChar(productName) + '&nodeType=sub-cat&catId=' +catId+ '&subCatId=0';
	                    	  } else if (nodeType == 'sub-cat') {
	                    		  subCatId = node.attr("id");
	                    		  catId = $('#' +subCatId).parent().parent().attr('id');
	                    		  var focusProgramId = $('#selId').val();
	                    		  var productCode = encodeURI('');
	                    		  var productName = encodeURI('');
	                    		  return '/rest/catalog/focus-program/product/list.json?focusProgramId=' + focusProgramId + '&productCode=' + encodeChar(productCode) + '&productName=' + encodeChar(productName) + '&nodeType=product&catId=' +catId+ '&subCatId=' + subCatId;
	                    	  } else if (nodeType == 'product') {
	                    		  return '';
	                    	  }
	                      }
	                    },
	                "data" : function (n) {
	                        return { id : n.attr ? n.attr("id") : 0 };
	                    }
	            }
	        }
		});	
	},
	exportExcelData: function() {
		$('#divOverlay').show();
		$('.ErrorMsgStyle').hide();
		var data = new Object();
		var code = '';
		var name = '';
		if ($('#excelType').val() == 2) {//dvtg
			code = $('#shopCode').val().trim();
			name = $('#shopName').val().trim();
			data.shopCode = code;
			data.shopName = name;
		} else if ($('#excelType').val() == 3) {//sp cttt
			code = $('#productCode').val().trim();
			name = $('#productName').val().trim();
			data.code = code;
			data.name = name;
		}
		data.id = $('#selId').val();
		data.excelType = $('#excelType').val();
		var url = "/catalog/bary-centric-programme/getlistexceldata";
		ReportUtils.exportReport(url, data);
	},
	exportActionLog: function() {
		var id = $('#selId').val();
		var fromDate = $('#fromDate').val().trim();
		var toDate = $('#toDate').val().trim();
		ExportActionLog.exportActionLog(id, ExportActionLog.FOCUS_PROGRAM, fromDate, toDate, 'errMsgActionLog');
	},
	openCopyFocusProgram: function() {
		$('.ErrorMsgStyle').hide();
		var codeText = 'Mã CTTT <span class="RequireStyle" style="color:red">*</span>';
		var nameText = 'Tên CTTT <span class="RequireStyle" style="color:red">*</span>';
		var title = "Sao chép CTTT";
		var codeFieldText = "focusProgramCode";
		var nameFieldText = "focusProgramName";
		var searchDiv = "searchStyle1EasyUIDialogDivEx";
		var arrParam = null;

		CommonSearchEasyUI._arrParams = null;
		CommonSearchEasyUI._currentSearchCallback = null;
		$('#searchStyle1EasyUIDialogDivEx').css("visibility", "visible");
		var html = $('#searchStyle1EasyUIDialogDivEx').html();
		$('#searchStyle1EasyUIDialogEx').dialog({  
	        title: title,  
	        closed: false,  
	        cache: false,  
	        modal: true,
	        width : 616,
	        height : 'auto',
	        onOpen: function() {
	        	//@CuongND : my try Code <reset tabindex>
	        	$('.easyui-dialog #__btnSaveIncentiveProgram').unbind('click');
	        	var tabindex = -1;
				$('.InputTextStyle, select, input, button , li , tr, td, th, label, ul, a, img').each(function () {
					if (this.type != 'hidden') {
						$(this).attr("tabindex", tabindex);
						tabindex -=1;
					}
				});
				tabindex = 1;
	    		 $('.easyui-dialog input,.easyui-dialog select,.easyui-dialog button').each(function () {
		    		 if (this.type != 'hidden') {
			    	     $(this).attr("tabindex", '');
						 tabindex++;
		    		 }
				 });
				//end <reset tabindex>
				$('.easyui-dialog #seachStyle1Code').focus();
				$('.easyui-dialog #errMsgSearch').html("").hide();
				$('.easyui-dialog #successMsgInfo').html("").hide();
				$('.easyui-dialog #searchStyle1CodeText').val(codeFieldText);
				$('.easyui-dialog #searchStyle1NameText').val(nameFieldText);
				
				var codeField = 'code';
				var nameField = 'name';
				if (codeFieldText != null && codeFieldText != undefined && codeFieldText.length > 0) {
					codeField = codeFieldText;
				}
				if (nameFieldText != null&& nameFieldText != undefined && nameFieldText.length > 0) {
					nameField = nameFieldText;
				}
				
				$('.easyui-dialog #seachStyle1CodeLabel').html(codeText);
				$('.easyui-dialog #seachStyle1NameLabel').html(nameText);
				
				$('.easyui-dialog #__btnSaveFocusProgram').unbind('click');
				$('.easyui-dialog #__btnSaveFocusProgram').bind('click',function(event) {
					var code = $('.easyui-dialog #seachStyle1Code').val().trim();
					var name = $('.easyui-dialog #seachStyle1Name').val().trim();
					if (code == "" || code == undefined || code == null) {
						$('.easyui-dialog #errMsgSearch').html("Bạn chưa nhập giá trị trường Mã CTTT.").show();
						$('.easyui-dialog #seachStyle1Code').focus();
						return false;
					}
					var msg = Utils.getMessageOfSpecialCharactersValidateEx1('.easyui-dialog #seachStyle1Code', 'Mã CTTT',Utils._CODE);
					if (msg.length >0) {
						$('.easyui-dialog #errMsgSearch').html(msg).show();
						$('.easyui-dialog #seachStyle1Code').focus();
						return false;
					}
					if (name == "" || name == undefined || name == null) {
						$('.easyui-dialog #errMsgSearch').html("Bạn chưa nhập giá trị trường Tên CTTT.").show();
						$('.easyui-dialog #seachStyle1Name').focus();
						return false;
					}
					msg = Utils.getMessageOfSpecialCharactersValidateEx1('.easyui-dialog #seachStyle1Name', 'Tên CTTT');
					if (msg.length >0) {
						$('.easyui-dialog #errMsgSearch').html(msg).show();
						$('.easyui-dialog #seachStyle1Name').focus();
						return false;
					}
					var focusId = $('#progId').val();
					var params = new Object();
					params.id = focusId;
					params.code = code;
					params.name = name;
					Utils.addOrSaveData(params, '/catalog/bary-centric-programme/copyfocusprogram', null, 'errMsgSearch', function(data) {
						//PromotionCatalog.loadShop();
						$('.easyui-dialog #successMsgInfo').html("Sao chép dữ liệu thành công.").show();
						$('#searchStyle1EasyUIDialogEx').dialog('close');
						//$('#promotionId').val(data.promotionId);
						window.location.href = '/catalog/bary-centric-programme/change?id=' +encodeChar(data.focusId);
					}, null, null, null, "Bạn có muốn sao chép CTTT này ?", null);
				});
	        },	       
	        onClose : function() {
	        	var tabindex = 1;
				$('.InputTextStyle, select, input, button , li , tr, td, th, label, ul, a, img').each(function () {
					if (this.type != 'hidden') {
						$(this).attr("tabindex", '');
					}
					tabindex ++;
				});	
	        	$('#searchStyle1EasyUIDialogDivEx').html(html);
	        	$('#searchStyle1EasyUIDialogDivEx').css("visibility", "hidden");
	        	$('.easyui-dialog #seachStyle1Code').val('');
	        	$('.easyui-dialog #seachStyle1Name').val('');
	        	$('.easyui-dialog #__btnSaveFocusProgram').unbind('click');
	        	$('.easyui-dialog #__btnSave').unbind('click');
	        }
	    });
		return false;
	},
//	SAN PHAM CHUONG TRINH TRONG TAM[[[[
	getProductGridUrl: function(code,name) {
		var selId = $('#selId').val();
		return '/catalog/bary-centric-programme/search-product?id=' + encodeChar(selId) + '&code=' + encodeChar(code) + '&name=' + encodeChar(name) /*+ '&type=' + encodeChar(type)*/;
	},
	showTab2: function() {
		var idx = -1;
		$('#tab1').removeClass('Active');
		$('#tab2').removeClass('Active');
		$('#tab3').removeClass('Active');
		$('#spCTTTTab').hide();
		$('#dvtgCTTTTab').hide();
		$('#htbhCTTTTab').hide();
		$('#tab2').addClass('Active');
		$('#spCTTTTab').show();
		$('#divImportGrroupSpDv').html(BaryCentricProgrammeCatalog._importHtml).change();
		$('.IdImportDivTpm').each(function() {
			$(this).prop('id', 'tab_sp_group_edit_import').addClass('cmsiscontrol');
		});
		Utils.functionAccessFillControl('divImportGrroupSpDv');
		$('.IdImportDivTpm').show();
		$('#productCode').focus();
		Utils.bindAutoButtonEx('.ContentSection', 'btnSearchProduct');
		if ($('#downloadTemplate').length > 0) {
			$('#downloadTemplate').attr('href',excel_template_path + 'catalog/Bieu_mau_danh_muc_cttt_HTBH_TTSP.xls');
		}
		$('#excelType').val(3);
		$('.ErrorMsgStyle').hide();
		var title = '';
		if ($('#statusHidden').val().trim() == activeType.WAITING) {
			title = '<a id="btnAddGridP" title="Thêm mới" href="javascript:void(0);" onclick="BaryCentricProgrammeCatalog.openEasyUIDialog();"><img src="/resources/images/icon-add.png"></a>';
		}
		$('.ErrorMsgStyle').hide();
		$('#productCode').val('').change();
		$('#productName').val('').change();
		$("#grid").datagrid({
			  url:BaryCentricProgrammeCatalog.getProductGridUrl('', ''),
			  columns:[[		
			    {field: 'categoryCode', title: 'Mã ngành hàng', width: 80, sortable: false, resizable: false, align: 'left',
			    	formatter: function(index, row) {
				    	if (row.product != null && row.product != undefined && row.product.cat != null&& row.product.cat != undefined) {
				    		return Utils.XSSEncode(row.product.cat.productInfoCode);
				    	}
			    	}
			    },
			    {field: 'productCode', title: 'Mã SP', width: 100, sortable: false, resizable: false , align: 'left',
			    	formatter: function(index, row) {
				    	if (row.product!=null) {
				    		return Utils.XSSEncode(row.product.productCode);
				    	}
				    }
			    },
			    {field: 'productName', title: 'Tên SP', width: 100, align: 'left', sortable: false, resizable: false ,  
		            formatter: function(index, row) {
		            	if (row.product!=null) {
				    		return Utils.XSSEncode(row.product.productName);
				    	}
		            }
			    },
			    {field: 'createUser', title: 'Mã HTBH', width:80, align: 'left', sortable: false, resizable: false ,  
	                editor:{  
	                    type: 'combobox',
	                    options:{
	                    	data: BaryCentricProgrammeCatalog._lstSaleTypeData.get(0).rows,
	            	        valueField: 'saleTypeCode',  
	            	        textField: 'saleTypeCode',
	            	        width :40,
	                        filter: function(q, row) {
	                    		var opts = $(this).combobox('options');
	                    		return row[opts.textField].indexOf(q.toUpperCase()) == 0;
	                    	}
	                    }
	                }
			    },
			    {field: 'type', title: 'Loại MHTT', width: 150, align: 'left',fixed: true, sortable: false, resizable: false ,  
                    editor:{  
                        type: 'combobox',
                        options:{
                        	data: BaryCentricProgrammeCatalog._lstTypeData.get(0).rows,  
	            	        valueField: 'apParamCode',  
	            	        textField: 'apParamCode',
//	            	        method: 'GET',
//	            	        class: 'easyui-combobox',
	            	        width:150,
	            	        panelWidth:150,
	            	        editable: false,
//	                        panelHeight :140, 
	                        filter: function(q, row) {
	                    		var opts = $(this).combobox('options');
	                    		return row[opts.textField].indexOf(q.toUpperCase()) == 0;
	                    	}
	                    }
                    }
			    },
			    {field: 'edit', title: '', hidden: true, width: 50, align: 'center', sortable: false, resizable: false, formatter: function(value, row, index) {
			    	if (row.editing) {
		                var s = "<a title='Lưu' href='javascript:void(0)' onclick='return BaryCentricProgrammeCatalog.saveRow(this)'><span style='cursor:pointer'><img src='/resources/images/icon-datach.png'/></span></a> ";  
		                var c = "<a title='Quay lại' href='javascript:void(0)' onclick='BaryCentricProgrammeCatalog.cancelRow(this)'><span style='cursor:pointer'><img src='/resources/images/icon_esc.png'/></span></a>";  
		                return s + c;  
		            } else {
		            	var e = '';
		            	if ($('#statusHidden').val().trim() == activeType.WAITING) {
		            		e = "<a id='btnEditGridP'"+index+" title='Sửa' href='javascript:void(0)' onclick='BaryCentricProgrammeCatalog.editRow(this);'><span style='cursor:pointer'><img src='/resources/images/icon-edit.png'/></span></a>";
		            	}
		                return e;
		            }
			    }},
			    {field: 'delete', hidden: true, title: title, width: 50, align: 'center', sortable: false, resizable: false,
			    	formatter: function(index, row) {
			    		if ($('#statusHidden').val().trim() == activeType.WAITING) {
			    			return "<a id='btnDelGridP'"+index+" title='Xóa' href='javascript:void(0)' onclick=\"BaryCentricProgrammeCatalog.deleteRow('"+ row.id +"')\"><img src='/resources/images/icon-delete.png'/></a>";
			    		}
			    		return '';
			    		
			    	} 
			    },
			  ]],	  
			  pageList  : [10,20,30],
			  height: 'auto',
			  scrollbarSize : 0,
			  pagination: true,
			  checkOnSelect : true,
			  fitColumns: true,
			  method : 'GET',
			  rownumbers: true,	  
			  singleSelect: true,
			  pageNumber:1,
			  width: ($('#productFocusGrid').width()),
			  onLoadSuccess: function() {
					setTimeout(function() {
					  $('#successMsgProduct').html('').hide();
					}, 1500);
					var angelkid;
					$('.datagrid-header-rownumber').html('STT');
					$('#grid').datagrid('resize');
					edit = false;
					  
					var arrEdit =  $('#productFocusGrid td[field="edit"]');
					if (arrEdit != undefined && arrEdit != null && arrEdit.length > 0) {
						for (var i = 0, size = arrEdit.length; i < size; i++) {
							$(arrEdit[i]).prop("id", "tab_sp_group_edit_" + i).addClass("cmsiscontrol");//Khai bao id danh cho phan quyen
						}
					}
					var arrDelete =  $('#productFocusGrid td[field="delete"]');
					if (arrDelete != undefined && arrDelete != null && arrDelete.length > 0) {
						for (var i = 0, size = arrDelete.length; i < size; i++) {
							$(arrDelete[i]).prop("id", "tab_sp_group_edit_" + i).addClass("cmsiscontrol");//Khai bao id danh cho phan quyen
						}
					}
					Utils.functionAccessFillControl('productFocusGrid', function(data) {
					//Xu ly cac su kien lien quan den cac control phan quyen
					var arrTmpLength =  $('#productFocusGrid td[id^="tab_sp_group_edit_"]').length;
					var invisibleLenght = $('.isCMSInvisible[id^="tab_sp_group_edit_"]').length;
					if (arrTmpLength > 0 && arrTmpLength != invisibleLenght) {
						$('#grid').datagrid("showColumn", "delete");
					} else {
						$('#grid').datagrid("hideColumn", "delete");
					}
					arrTmpLength =  $('#productFocusGrid td[id^="tab_sp_group_edit_"]').length;
					invisibleLenght = $('.isCMSInvisible[id^="tab_sp_group_edit_"]').length;
					if (arrTmpLength > 0 && arrTmpLength != invisibleLenght) {
						$('#grid').datagrid("showColumn", "edit");
					} else {
						$('#grid').datagrid("hideColumn", "edit");
						}
					});
		      },
		  	onBeforeEdit: function(index, row) {
		  		//$()
		        row.editing = true;
		        edit =true;
		        idx = index;  
		        BaryCentricProgrammeCatalog.updateActions(index);  
		    },  
		    onAfterEdit: function(index, row) {  
		        row.editing = false;
		        edit = false;
		        idx = -1;
		        $('#saleTypeCode').val(Utils.XSSEncode(row.createUser));
		        $('#type').val(Utils.XSSEncode(row.type));
		        BaryCentricProgrammeCatalog.updateActions(index);  
		    },  
		    onCancelEdit: function(index, row) {  
		        row.editing = false;  
		        edit = false;
		        idx = -1;
		        BaryCentricProgrammeCatalog.updateActions(index);  
		    }  
		});
	},
	getJsonListSaleType: function() {
		$.getJSON('/rest/catalog/focus-program/product/sale-type-code.json?id=' + $('#focusProgramId').val(),function(data) {
			if (data != null && data != undefined) {
				BaryCentricProgrammeCatalog._lstSaleTypeData.put(0, data);
			}
		});
	},
	getJsonListType: function() {
		$.getJSON('/rest/catalog/focus-program/product/type.json',function(data) {
			if (data != null && data != undefined) {
				BaryCentricProgrammeCatalog._lstTypeData.put(0, data);
			}
		});
	},
	editRow: function(target) {
		var indexRow = BaryCentricProgrammeCatalog.getRowIndex(target);
	    $('#grid').datagrid('beginEdit', indexRow);
	    var width = 75;
	    $('td[field=saleTypeCode] .datagrid-editable-input').combobox('resize', width);
	    $('td[field=type] .datagrid-editable-input').combobox('resize', width);
	},
	updateActions: function(index) {  
	    $('#grid').datagrid('updateRow',{  
	        index: index,  
	        row:{}  
	    });  
	}, 
	getRowIndex: function(target) {  
	    var tr = $(target).closest('tr.datagrid-row');  
	    return parseInt(tr.attr('datagrid-row-index'));  
	},  
	saveRow: function(target) {
		var indexRow = BaryCentricProgrammeCatalog.getRowIndex(target);
		$('#errMsg').html('').hide();
	    $('#grid').datagrid('endEdit', indexRow);
	    var saleTypeCode = $('#grid').datagrid('getData').rows[indexRow].createUser;;
	    var focusProductType = $('#grid').datagrid('getData').rows[indexRow].type;
	    var msg = '';
	    $('#errMsgProduct').html('').hide();
	    if (saleTypeCode == null || saleTypeCode.length == 0 ) {
	    	msg = 'Vui lòng chọn mã HTBH';
	    	$('#grid').datagrid('beginEdit', indexRow);
	    	$('td[field=createUser] .combo-text').focus();
	    }
	    if (msg.length == 0) {
	    	if (focusProductType == null || focusProductType.length == 0 ) {
		    	msg = 'Vui lòng chọn loại MHTT';
		    	 $('#grid').datagrid('beginEdit', indexRow);
		    	$('td[field=type] .combo-text').focus();
		    }
	    }
	    if (msg.length > 0) {
	    	$('#errMsgProduct').html(msg).show();
	    	return false;
	    }
	    var dataModel = new Object();		
		dataModel.id = $('#selId').val();
		dataModel.focusMapProductId = $('#grid').datagrid('getData').rows[indexRow].id;
		dataModel.staffTypeCode = saleTypeCode;
		dataModel.focusProductType = focusProductType;
		dataModel.isUpdate = true;
		Utils.addOrSaveRowOnGrid(dataModel, '/catalog/bary-centric-programme/product/save', ProgrammeDisplayCatalog._xhrSave, null, null, function(data) {
			$('#successMsgProduct').html('Lưu dữ liệu thành công').show();
			$("#grid").datagrid("reload");
		});
		return false;
	},
	cancelRow: function (target) {  
		var indexRow = BaryCentricProgrammeCatalog.getRowIndex(target);
	    $('#grid').datagrid('cancelEdit', indexRow);  
	},
	deleteRow: function(productId) {
		$('#errMsg').html('').hide();
		var dataModel = new Object();
		dataModel.productId = productId;	
		Utils.deleteSelectRowOnGrid(dataModel, 'SP của CTTT', "/catalog/bary-centric-programme/delete-product", BaryCentricProgrammeCatalog._xhrDel, null, 'errMsgProduct',function() {
			$('#successMsgProduct').html('Xóa dữ liệu thành công').show();
		});
		return false;		
	},
	searchProductTab: function() {
		$('.ErrorMsgStyle').hide();
		var code = $('#productCode').val().trim();
		var name = $('#productName').val().trim();
		$('#code').focus();
		$("#grid").datagrid({
			url:BaryCentricProgrammeCatalog.getProductGridUrl(code,name),
			pageNumber:1
		});
		return false;
	},
	openEasyUIDialog : function() {
		$('.ErrorMsgStyle').hide();
		$('#divDialogSearch').show();
		var html = $('#productEasyUIDialog').html();
		$('#productEasyUIDialog').dialog({  
	        closed: false,  
	        cache: false,  
	        modal: true,
	        position: 'middle',
	        center: true, 
	        onOpen: function() {
	        	BaryCentricProgrammeCatalog._mapLstProduct = new Map();
	        	setTimeout(function() {
	    			CommonSearchEasyUI.fitEasyDialog();
	    		},500);
	        	Utils.bindAutoSearch();
	        	$('#productCodeDlg').focus();
	        	$('#saleTypeCodeDlg').addClass('MySelectBoxClass');
	        	$('#saleTypeCodeDlg').customStyle();
	        	$('#typeDlg').addClass('MySelectBoxClass');
	        	$('#typeDlg').customStyle();
	        	BaryCentricProgrammeCatalog.searchProductTree();
	        },
	        onBeforeClose: function() {
	        },
	        onClose : function() {
	        	$('#divDialogSearch').hide();
	        	$('#productEasyUIDialog').html(html);
	        }
	    });
		return false;
	},
	searchProductTree: function() {
		$('.ErrorMsgStyle').hide();
		$('#loadingDialog').show();
		var code = $("#productCodeDlg").val().trim();
		var name = 	$("#productNameDlg").val().trim();
		var focusProgramId = $('#selId').val();
		var url = '/rest/catalog/group-po/tree/2.json' ;
		$("#tree").tree({
			url: url,  
			method: 'GET',
            animate: true,
            checkbox: true,
            onBeforeLoad: function(node,param) {
            	$("#tree").css({"visibility":"hidden"});
            	$("#loadding").css({"visibility":"visible"});
            	$("#errMsgDlg").html('');
            	param.code = code;
            	param.name = name;
            	param.id = focusProgramId;
            },
            onCheck: function(node,data) {
            	if (node.attributes.productTreeVO.type == "PRODUCT") {
            		if (data) {
            			BaryCentricProgrammeCatalog._mapLstProduct.put(node.attributes.productTreeVO.id,node.attributes.productTreeVO.id);
            		} else {
            			BaryCentricProgrammeCatalog._mapLstProduct.remove(node.attributes.productTreeVO.id);
            		}
            	}
            	if (node.attributes.productTreeVO.type == "SUB_CATEGORY") {
            		var sub_category = node.attributes.productTreeVO.listChildren;
            		for (var i = 0;i<sub_category.length;i++) {      			
		            		if (data) {
		            			BaryCentricProgrammeCatalog._mapLstProduct.put(sub_category[i].id,sub_category[i].id);
		            		} else {
		            			BaryCentricProgrammeCatalog._mapLstProduct.remove(sub_category[i].id);
		            		}
            		}
            	}
            	if (node.attributes.productTreeVO.type == "CATEGORY") {
            		var category = node.attributes.productTreeVO.listChildren;
            		for (var i = 0;i<category.length;i++) {
	        			var sub_category = category[i].listChildren;
	        			for (var j = 0;i<sub_category.length;i++) {
		            		if (data) {
		            			BaryCentricProgrammeCatalog._mapLstProduct.put(sub_category[i].id,sub_category[i].id);
		            		} else {
		            			BaryCentricProgrammeCatalog._mapLstProduct.remove(sub_category[i].id);
		            		}
	        			}
            		}
            	}
            },
            onLoadSuccess: function(node,data) {
            	$("#tree").css({"visibility":"visible"});
            	var arr  = BaryCentricProgrammeCatalog._mapLstProduct.keyArray;
            	if (arr.length > 0) {
            		for (var i = 0;i < arr.length; i++) {
            			var nodes = $('#tree').tree('find', arr[i]);
            			if (nodes != null) {
            				$("#tree").tree('check',nodes.target);
            			}
            		}
            	}
            	$("#loadding").css({"visibility":"hidden"});
            	$('#loadingDialog').hide();
            	if (data.length == 0) {
            		$("#errMsgDlg").html("Không có kết quả");
            	}
            	setTimeout(function() {
            		CommonSearchEasyUI.fitEasyDialog("productEasyUIDialog");
            	}, 500);
            }
		});
	},
	changeProductTree: function() {
		$('.ErrorMsgStyle').hide();
		var dataModel = new Object();
		var nodes = $('#tree').tree('getChecked');
		var lstId = [];  
		var arr = $('#tree').children();
		var moreCat = 0;
		arr.each(function() {
			if (!$(this).children().first().children().last().prev().hasClass('tree-checkbox0')) {
				moreCat++;
			}
		});
        for (var i=0; i<nodes.length; i++) {
        	if (nodes[i].attributes.productTreeVO.type != "PRODUCT") {
        		continue;
        	}
            lstId.push(nodes[i].id.split('_')[0]);  
        }
        if (BaryCentricProgrammeCatalog._mapLstProduct.keyArray.length > 0) {
        	for (var j = 0; j < BaryCentricProgrammeCatalog._mapLstProduct.keyArray.length ; j++) {
        		if (lstId.indexOf(BaryCentricProgrammeCatalog._mapLstProduct.keyArray[j]) > -1) {
        			continue;
        		} else {
        			lstId.push(BaryCentricProgrammeCatalog._mapLstProduct.keyArray[j]);
        		}
        	}
        }
        if (lstId == '' || lstId.length == 0) {
        	$("#errMsgDlg").html("Vui lòng chọn sản phẩm").show();
        	return false;
        }
        var saleTypeCode = $('#saleTypeCodeDlg').val().trim();
        var type = $('#typeDlg').val().trim();
        if (saleTypeCode == -1) {
        	$("#errMsgDlg").html("Vui lòng chọn loại HTBH").show();
        	return false;
        }
        if (type == -1) {
        	$("#errMsgDlg").html("Vui lòng chọn loại MHTT").show();
        	return false;
        }
        dataModel.id = $('#selId').val().trim();
        dataModel.lstProductId = lstId;
		dataModel.channelTypeId = saleTypeCode;
		dataModel.type = type;
		if (moreCat > 1) {
			$.messager.confirm('Xác nhận', 'Bạn đang chọn các sản phẩm hơn 1 ngành hàng. Bạn có muốn lưu thông tin không?', function(r) {
				if (r) {
					Utils.saveData(dataModel, '/catalog/bary-centric-programme/product/save', BaryCentricProgrammeCatalog._xhrSave, 'errMsgDlg', function(data) {
						$('#successMsgProduct').html('Lưu dữ liệu thành công').show();
						$("#productEasyUIDialog").dialog("close");
						if (data.error == false) {
							$("#grid").datagrid("reload");
						}
					});		
				}
			});
		} else {
			Utils.addOrSaveData(dataModel, '/catalog/bary-centric-programme/product/save', BaryCentricProgrammeCatalog._xhrSave, 'errMsgDlg', function(data) {
				$('#successMsgProduct').html('Lưu dữ liệu thành công').show();
				$("#productEasyUIDialog").dialog("close");
				if (data.error == false) {
					$("#grid").datagrid("reload");
				}
			});
		}
		return false;
	},
	getShopGridUrl: function(focusId,shopCode,shopName) {
		return "/catalog/bary-centric-programme/searchshop?id="+ encodeChar(focusId) + "&shopCode=" + encodeChar(shopCode) + "&shopName=" + encodeChar(shopName);
	},
	openShopDialog: function(id) {
		$('.ErrorMsgStyle').hide();
		var focusId = $('#progId').val();
		var arrParam = new Array();
 		var param = new Object();
 		param.name = 'shopId';
 		if (id != null && id != undefined) {
 	 		param.value = id;
 		}
 		var param1 = new Object();
 		param1.name = 'focusId';
 		param1.value = focusId;
 		arrParam.push(param);
 		arrParam.push(param1);
 		var param2 = new Object();
 		param2.name = 'stringProgram';
 		param2.value = 'focus';
 		arrParam.push(param2);
 		BaryCentricProgrammeCatalog.searchShopFocusProgram(function(data) {
 				//$('#promotionProgramCode').val(data.code);
 		}, arrParam);
 		$('#searchStyleShopContainerGrid input[type=text]').each(function() {
			Utils.bindFormatOnTextfieldEx1($(this).attr('id'),Utils._TF_NUMBER, null, '#errMsgDialog', 'Số suất phải là số nguyên dương.');
		}); 
	},
	searchShopFocusProgram: function(callback,arrParam) {
		var codeText = "Mã đơn vị";
		var nameText = "Tên đơn vị";
		var title = "Chọn đơn vị";
		var url = "/catalog/bary-centric-programme/searchShopDialog";
		var codeFieldText = "promotionProgramCode";
		var nameFieldText = "promotionProgramName";
		var searchDiv = "searchStyleShopFocus";
		var searchDialog = "searchStyleShopFocus1";
		var program = "focus";

		CommonSearchEasyUI._arrParams = null;
		CommonSearchEasyUI._currentSearchCallback = null;
		var html = $('#' +searchDiv).html();
		$('#' +searchDialog).dialog({  
	        title: title,  
	        closed: false,  
	        cache: false,  
	        modal: true,
	        width : 616,
	        height : 'auto',
	        onOpen: function() {
				$('.easyui-dialog #seachStyleShopCode').focus();
				$('.easyui-dialog #searchStyleShopUrl').val(url);
				$('.easyui-dialog #searchStyleShopCodeText').val(codeFieldText);
				$('.easyui-dialog #searchStyleShopNameText').val(nameFieldText);
				CommonSearch._currentSearchCallback = callback;
				CommonSearch._arrParams = arrParam;
				
				var codeField = 'code';
				var nameField = 'name';
				if (codeFieldText != null && codeFieldText != undefined && codeFieldText.length > 0) {
					codeField = codeFieldText;
				}
				if (nameFieldText != null&& nameFieldText != undefined && nameFieldText.length > 0) {
					nameField = nameFieldText;
				}
				
				$('.easyui-dialog #seachStyleShopCodeLabel').html(codeText);
				$('.easyui-dialog #seachStyleShopNameLabel').html(nameText);
				
				$('.easyui-dialog #searchStyleShopGrid').show();
				$('.easyui-dialog #searchStyleShopGrid').treegrid({  
				    url:  CommonSearch.getSearchStyleShopGridUrl(url, '', '', arrParam),
				    width:($('#searchStyleShopContainerGrid').width()-20),  
			        height:400,  
			        idField: 'id',  
			        treeField: 'data',
				    columns:[[  
				        {field: 'data', title: 'Đơn vị', resizable: false, width:516, formatter: function(value, row, index) {
				        	  return Utils.XSSEncode(value);
				          }},
				        {field: 'edit', title: '', width:50, align: 'center', resizable: false, formatter: function(value, row, index) {
				        	if (row.isJoin >= 1) {
				        		return '<input type ="checkbox" disabled="disabled">';
				        	}
				        	var classParent = "";
				        	if (row.parentId != null && row.parentId != undefined) {
				        		classParent = "class_"+row.parentId;
				        	}
				        	return '<input type ="checkbox" class="' +classParent+ '" value="' +row.id+ '" id="check_' +row.id+ '" onclick="return BaryCentricProgrammeCatalog.onCheckShop('+row.id+ ');">';
				        }}
				    ]],
				    onLoadSuccess: function(row,data) {
				    	$(window).resize();
				    	if (data.rows != null && data.rows != undefined) {
					    	var length = parseInt(data.solg);
					    	$('#solg').val(length);
					    	var s = $('#width').val();
					    	if (s == null || s == "" || s == undefined) {
					    		s = $('.easyui-dialog .datagrid-header-row td[field=data] div').width();
					    		$('#width').val(s);
					    	}
					    	if (length > 15) {
					    		$('#searchStyleShopContainerGrid .datagrid-header-row td[field=data] div').width(s-17);
				    			$('#searchStyleShopContainerGrid .datagrid-row td[field=data] div').width(s-17);
					    	}
					    	for (var ij = 0, szj = BaryCentricProgrammeCatalog._listShopId.length; ij < szj; ij++) {
					    		$("#check_"+BaryCentricProgrammeCatalog._listShopId[ij]).attr("checked", "checked");
					    	}
				    	}
				    	setTimeout(function() {
			    			CommonSearchEasyUI.fitEasyDialog(searchDialog);
			    		},1000);
				    },
				    onExpand: function(row) {
				    	var value = parseInt($('#solg').val());
				    	var width = parseInt($('#width').val());
				    	var countNode = parseInt(row.countNode);
				    	value = value + countNode;
				    	$('#solg').val(value);
				    	if (value > 15) {
				    		var s = width;
				    		$('#searchStyleShopContainerGrid .datagrid-header-row td[field=data] div').width(s-17);
			    			$('#searchStyleShopContainerGrid .datagrid-row td[field=data] div').width(s-17);
				    	}
				    	for (var ij = 0, szj = BaryCentricProgrammeCatalog._listShopId.length; ij < szj; ij++) {
				    		$("#check_"+BaryCentricProgrammeCatalog._listShopId[ij]).attr("checked", "checked");
				    	}
				    },
				    onCollapse: function(row) {
				    	var value = parseInt($('#solg').val());
				    	var width = parseInt($('#width').val());
				    	var countNode = parseInt(row.countNode);
				    	console.log(countNode);
				    	value = value - countNode;
				    	$('#solg').val(value);
				    	if (value < 15) {
				    		var s = width;
				    		$('#searchStyleShopContainerGrid .datagrid-header-row td[field=data] div').width(s);
			    			$('#searchStyleShopContainerGrid .datagrid-row td[field=data] div').width(s);
				    	}
				    }
				});
				
				$('.easyui-dialog #btnSearchStyleShop').unbind('click');
				$('.easyui-dialog #btnSearchStyleShop').bind('click',function(event) {
					BaryCentricProgrammeCatalog.searchShopDlg();
				});
				$('.easyui-dialog #seachStyleShopCode').unbind('keyup');
				$('.easyui-dialog #seachStyleShopCode').bind('keyup',function(event) {
					if (event.keyCode == keyCodes.ENTER) {
						BaryCentricProgrammeCatalog.searchShopDlg();
					}
				});
				$('.easyui-dialog #seachStyleShopName').unbind('keyup');
				$('.easyui-dialog #seachStyleShopName').bind('keyup',function(event) {
					if (event.keyCode == keyCodes.ENTER) {
						BaryCentricProgrammeCatalog.searchShopDlg();
					}
				});
	        },
	        onBeforeClose: function() {
				var tabindex = 1;
				$('.InputTextStyle, select, input, button , li , tr, td, th, label, ul, a, img').each(function () {
					if (this.type != 'hidden') {
						$(this).attr("tabindex", '');
					}
					tabindex ++;
				});	
				var curIdFocus = $('#cur_focus').val();
				$('#' + curIdFocus).focus();				
	        },
	        onClose : function() {
	        	$('#' +searchDiv).html(html);
	        	$('.easyui-dialog #seachStyleShopCode').val('');
	        	$('.easyui-dialog #seachStyleShopName').val('');
	        	$('.easyui-dialog #errMsgDialog').html('').hide();
	        	BaryCentricProgrammeCatalog._listShopId = new Array();
	        }
	    });
	},
	searchShopDlg: function() {
		$('.easyui-dialog #errMsgDialog').html('').hide();
		var code = $('.easyui-dialog #seachStyleShopCode').val().trim();
		var name = $('.easyui-dialog #seachStyleShopName').val().trim();
		var arr = $.extend(true, [], CommonSearch._arrParams);
		var url = CommonSearch.getSearchStyleShopGridUrl($('.easyui-dialog #searchStyleShopUrl').val().trim(), code, name, arr);
		$('.easyui-dialog #searchStyleShopGrid').treegrid({url:url});
	},
	onCheckShop: function(id) {
		$('.ErrorMsgStyle').hide();
		var isCheck = $('.easyui-dialog #check_' + id).attr('checked');
	    	if ($('.easyui-dialog #check_' + id).is(':checked')) {
		    	if (BaryCentricProgrammeCatalog._listShopId.indexOf(id) < 0) {
		    		BaryCentricProgrammeCatalog._listShopId.push(id);
		    	}
		    	var classParent = $('.easyui-dialog #check_' + id).attr('class');
			var flag = true;
			var count = 0;
			$('.' +classParent).each(function() {
				if ($(this).is(':checked')) {
					flag = false;
					count++;
				}
			});
			if (flag || count == 1) {
				CommonSearch.recursionFindParentShopCheck(id);
			}
		} else {
			var idx = BaryCentricProgrammeCatalog._listShopId.indexOf(id);
			if (idx > -1) {
		    		BaryCentricProgrammeCatalog._listShopId.splice(idx, 1);
		    	}
			var classParent = $('.easyui-dialog #check_' +id).attr('class');
			var flag = true;
			var count = 0;
			$('.' +classParent).each(function() {
				if ($(this).is(':checked')) {
					flag = false;
				}
			});
			if (flag || count == 1) {
				CommonSearch.recursionFindParentShopUnCheck(id);
			}
			//CommonSearch.recursionFindParentShopUnCheck(id);
		}
	},
	createFocusShopMap: function() {
		$('.ErrorMsgStyle').hide();
		var params = new Object();
		params.listShopId = BaryCentricProgrammeCatalog._listShopId;
		params.id = $('#progId').val();
		if (BaryCentricProgrammeCatalog._listShopId.length<=0) {
			$('.easyui-dialog #errMsgDialog').html('Bạn chưa chọn đơn vị để thêm').show();
			return false;
		}
		Utils.addOrSaveData(params, '/catalog/bary-centric-programme/createfocusprogram', null, 'errMsgDialog', function(data) {
			if (data.error == undefined && data.errMsg == undefined) {
				BaryCentricProgrammeCatalog.searchShop();
				BaryCentricProgrammeCatalog._listShopId = new Array();
				$('#searchStyleShopFocus1').dialog('close');
				$('#errExcelMsg').html("").hide();
				$('#successMsg2').html('Lưu dữ liệu thành công.').show();
				var tm = setTimeout(function() {
					$('#successMsg2').html('').hide();
					clearTimeout(tm); 
				}, 3000);
			} 
		}, null, null, null, "Bạn có muốn thêm đơn vị này ?", null);
	},
	deleteFocusShopMap: function(shopId) {
		$('.ErrorMsgStyle').hide();
		var params = new Object();
		params.shopId = shopId;
		params.id = $('#progId').val();
		Utils.addOrSaveData(params, '/catalog/bary-centric-programme/deletefocusprogram', null, 'errExcelMsg', function(data) {
			BaryCentricProgrammeCatalog.searchShop();
			$('#errExcelMsg').html("").hide();
			$('#successMsg2').html('Xóa dữ liệu thành công.').show();
			var tm = setTimeout(function() {
				$('#successMsg2').html('').hide();
				clearTimeout(tm); 
			}, 3000);
		}, null, null, null, "Bạn có muốn xóa đơn vị này ?", null);
	},
	exportExcelShopMap: function() {
		$('#divOverlay').show();
		$('.ErrorMsgStyle').hide();
	    var focusId = $('#progId').val();
		var shopCode = $('#shopCode').val().trim();
		var shopName = $('#shopName').val().trim();
		
		var data = new Object();
		data.id = focusId;
		data.shopCode = shopCode;
		data.shopName = shopName;
		
		var url = "/catalog/bary-centric-programme/exportFocusShopMap";
		CommonSearch.exportExcelData(data,url);
	},
	/*****************************	HTBH ****************************************/
	_mapHTBH:null,
	/**
	 * @modify hunglm16
	 * @since 03/11/2015
	 * @description thuc hien phan quyen lai control
	 * */
	loadHTBH : function() {
		$('.ErrorMsgStyle').hide();
		var focusProgramId = $('#focusProgramId').val();	
		var params = new Object();
		params.focusProgramId =focusProgramId;
		BaryCentricProgrammeCatalog._beforeUpdateHTBH = [];
		BaryCentricProgrammeCatalog._mapHTBH = new Map();
		$("#gridHTBH").datagrid({
			url: '/catalog/bary-centric-programme/search-htbh?permissionEdit=false',
			queryParams:params,
	        pagination : true,
	        rownumbers : true,
	        pageNumber : 1,
	        scrollbarSize: 0,
	        autoWidth: true,
	        autoRowHeight : true,
	        fitColumns : true,
			width : ($('#gridContainerHTBH').width()),
			height: 'auto',
			columns:[[
			    	{field: 'code', title: 'Mã HTBH', width: 150, sortable: false, resizable: false, align: 'left', formatter: function(value, row, index) {
		        	  	return Utils.XSSEncode(value);
		         	}},
			    	{field: 'name', title: 'Tên HTBH', width: 300, sortable: false, resizable: false, align: 'left', formatter: function(value, row, index) {
		        	  	return Utils.XSSEncode(value);
		         	}},
			    	{field: 'id', checkbox: true, width: 50, align: 'center', sortable: false, resizable: false},
			    	{field: 'isExist', hidden: true, resizable: false, formatter: function(value, rowData, rowIndex) {
				    	if (rowData.isExist == 1) { 
				    		BaryCentricProgrammeCatalog._mapHTBH.put(rowData.id, rowData);
				    		if (BaryCentricProgrammeCatalog._beforeUpdateHTBH.indexOf(rowData.id) < 0) {
					    		BaryCentricProgrammeCatalog._beforeUpdateHTBH.push(rowData.id);
					    	}
				    	}
			    	}}
			]],
			onCheck : function(rowIndex, rowData) {
				$('.ErrorMsgStyle').hide();
				var selectedId = rowData['id'];
				BaryCentricProgrammeCatalog._mapHTBH.put(selectedId, rowData);
			},
			onUncheck : function(rowIndex, rowData) {
				$('.ErrorMsgStyle').hide();
				var selectedId = rowData['id'];
				BaryCentricProgrammeCatalog._mapHTBH.remove(selectedId);
			},
			onCheckAll : function(rows) {
				$('.ErrorMsgStyle').hide();
				if ($.isArray(rows)) {
					for (var i = 0, size = rows.length; i < size; i++) {
						var row = rows[i];
						var selectedId = row['id'];
						BaryCentricProgrammeCatalog._mapHTBH.put(selectedId, row);
			    	}
			    }
			},
			onUncheckAll : function(rows) {
				$('.ErrorMsgStyle').hide();
				if ($.isArray(rows)) {
					for (var i = 0, size = rows.length; i < size; i++) {
						var row = rows[i];
						var selectedId = row['id'];
						BaryCentricProgrammeCatalog._mapHTBH.remove(selectedId);
			    	}
			    }
			},
			onLoadSuccess: function() {
				$('#gridContainerHTBH .datagrid-header-rownumber').html('STT');
		    	updateRownumWidthForDataGrid('#gridContainerHTBH');
				$('.ErrorMsgStyle').hide();
		    	setTimeout(function() {
		    		$('#successMsgHTBH').html('').hide();
		    	}, 1500);
				$('#gridHTBH').datagrid('resize');
				
				//Thuc hien phan quyen control
//				var arrDelete =  $('#gridContainerHTBH td[field="id"]');
//				if (arrDelete != undefined && arrDelete != null && arrDelete.length > 0) {
//				  for (var i = 0, size = arrDelete.length; i < size; i++) {
//				    $(arrDelete[i]).prop("id", "btnSaveHTBH_table_gr_id_" + i);//Khai bao id danh cho phan quyen
//					$(arrDelete[i]).addClass("cmsiscontrol");
//				  }
//				}
//				Utils.functionAccessFillControl('gridContainerHTBH', function(data) {
//					//Xu ly cac su kien lien quan den cac control phan quyen
////					if ($('#btnSaveHTBH').length > 0) {
////						$('#gridHTBH').datagrid("showColumn", "id");
////					}
//				});
				$('#gridHTBH').datagrid("showColumn", "id");
				var runner = 0;
				var isAll = 0;
				$('#gridContainerHTBH input:checkbox[name=id]').each(function() {
					var selectedId = this.value;
					if (BaryCentricProgrammeCatalog._mapHTBH.keyArray.indexOf(parseInt(selectedId)) > -1) {
						isAll++;
						$('#gridHTBH').datagrid('selectRow', runner);
					} 
					runner ++; 
				});
				if (isAll > 0 && isAll == runner) {
					$('#gridContainerHTBH td[field=id] .datagrid-header-check input:checkbox').attr('checked', 'checked');
				} else {
					$('#gridContainerHTBH td[field=id] .datagrid-header-check input:checkbox').removeAttr('checked');
				}
			}
		});
		return false;
	},
	searchHTBH : function () {
		$('.ErrorMsgStyle').hide();
		var params = new Object();
		params.focusProgramId = $('#focusProgramId').val().trim();
		params.code = $('#codeHTBH').val().trim();
		params.name = $('#nameHTBH').val().trim();
		BaryCentricProgrammeCatalog._beforeUpdateHTBH = [];
		BaryCentricProgrammeCatalog._mapHTBH = new Map();
		$("#gridHTBH").datagrid('load', params);
		return false;
	},
	saveHTBH: function() {
		var msg = '';
		$('.ErrorMsgStyle').hide();
		var focusProgramId = $('#focusProgramId').val();	//focusProgramId
		var mapKey = BaryCentricProgrammeCatalog._mapHTBH.keyArray;
		if (mapKey.length == BaryCentricProgrammeCatalog._beforeUpdateHTBH.length) {
			var c = 0;
			var sz = mapKey.length;
			for (var i = 0; i < sz; i++) {
				if (BaryCentricProgrammeCatalog._beforeUpdateHTBH.indexOf(mapKey[i]) < 0) {
					break;
				}
				c++;
			}
			if (c == sz) {
				$("#errMsg").html("Dữ liệu không có thay đổi để lưu").show();
				return false;
			}
		}
		var dataModel = new Object();
		dataModel.focusProgramId = focusProgramId;
		dataModel.lstApParamId =  mapKey;
		Utils.addOrSaveData(dataModel, "/catalog/bary-centric-programme/save-htbh", null, 'errMsg',function(data) {
			$('#successMsgHTBH').html('Lưu dữ liệu thành công').show();
			$.getJSON('/rest/catalog/bary-centric/sale-type/list.json?id=' + $('#focusProgramId').val(), function(data) {
				var arrHtml = new Array();
				arrHtml.push('<option value="-1">--- Chọn HTBH  ---</option>');
				for (var i=0;i<data.length;i++) {
					arrHtml.push('<option value="' + data[i].id + '">' + Utils.XSSEncode(data[i].saleTypeCode) + '</option>');
				}
				$('#saleTypeCodeDlg').html('');
				$('#saleTypeCodeDlg').html(arrHtml.join(""));
				$('#saleTypeCodeDlg').change();
			});
			BaryCentricProgrammeCatalog._beforeUpdateHTBH = [];
			BaryCentricProgrammeCatalog._mapHTBH = new Map();
			$("#gridHTBH").datagrid("reload");
			BaryCentricProgrammeCatalog.getJsonListSaleType();
		});
		return false;
	}
};
var SettingsConfig  = {
		_xhrUpdate : null,
		_xhrDel: null,
	/** duongdt3 27/11/2015 click buttom cap nhat tham so*/
	update: function(index, idParam){
		hideAllMessage();
		//get data from view
		var dataModel = new Object();
		var settingsVo = new Object();
		settingsVo.hourAutoCloseDateId = $('#hdHourAutoCloseDate').val();
		settingsVo.hourAutoCloseDateValue = $('#hourAutoCloseDate').val();
		settingsVo.distanceCheckCustomerId = $('#hdDistanceCheckCustomer').val();
		settingsVo.distanceCheckCustomerValue = $('#distanceCheckCustomer').val();
		settingsVo.problemAttachSizeMaxId = $('#hdProblemAttachSizeMax').val();
		settingsVo.problemAttachSizeMaxValue = $('#problemAttachSizeMax').val();
		settingsVo.problemFileTypeUploadId = $('#hdProblemFileTypeUpload').val();
		settingsVo.problemFileTypeUploadValue = $('#problemFileTypeUpload').val();
		settingsVo.timeRequestPositionId = $('#hdTimeRequestPosition').val();
		settingsVo.timeRequestPositionValue = $('#timeRequestPosition').val();
		settingsVo.timeRequestSyncDataId = $('#hdTimeRequestSyncData').val();
		settingsVo.timeRequestSyncDataValue = $('#timeRequestSyncData').val();
		var msg = '';
		if (msg.length == 0) {
			msg = Utils.getMessageOfRequireCheck('distanceCheckCustomer', jsp_settings_system_hour_auto_close_date.toLowerCase());
		}
		if (msg.length == 0) {
			msg = Utils.getMessageOfRequireCheck('distanceCheckCustomer', jsp_settings_system_distance_check_customer.toLowerCase());
		}
		if (msg.length == 0) {
			msg = Utils.getMessageOfRequireCheck('problemAttachSizeMax', jsp_settings_system_problem_attach_size_max.toLowerCase());
		}
		if (msg.length == 0) {
			msg = Utils.getMessageOfRequireCheck('problemFileTypeUpload', jsp_settings_system_problem_file_type_upload.toLowerCase());
		}
		if (msg.length == 0) {
			msg = Utils.getMessageOfSpecialCharactersValidate('problemFileTypeUpload', jsp_settings_system_problem_file_type_upload, Utils._SPECIAL);
		}
		if (msg.length == 0) {
			msg = Utils.getMessageOfRequireCheck('timeRequestPosition', jsp_settings_system_tablet_time_request_position.toLowerCase());
		}
		if (msg.length == 0) {
			msg = Utils.getMessageOfRequireCheck('timeRequestSyncData', jsp_settings_system_tablet_time_request_sync_data.toLowerCase());
		}
		if (msg.length > 0 ) {
			$('#errorMsg').html(msg).show();
			return false;
		}
		
		dataModel.settingsVo = settingsVo;
		JSONUtil.saveData2(dataModel, '/catalog/settings/update', ss_settings_system_confirm_update_data, "errorMsg", function() {
			var tm = setTimeout(function() {
				$('#successMsg').html("").hide();
				clearTimeout(tm);
				window.location.href = '/catalog/settings/info';
			 }, 1500);
		});
		return false;
	}
};

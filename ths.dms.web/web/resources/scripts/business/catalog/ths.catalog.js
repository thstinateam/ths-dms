/*
 * JavaScript file created by Rockstarapps Concatenation
*/

/*
 * START OF FILE - /web/web/resources/scripts/business/catalog/vnm.area-tree-catalog.js
 */
var AreaTreeCatalog = {
	parentsId:null,
	searchFilter:null,
	selectNodeTree:-1,
	isRefresh:false,
	isUpdate:false,
	updateArea: function(){
		var params = new Object();
		params.areaId=$('#areaId').val();
		params.status=$('#status').val();
		Utils.addOrSaveData(params, '/catalog/area-tree/update-area', null,  'errMsg', function(data) {
			$('#successMsgImport').html(msgCommon1).show();
			setTimeout(function(){
				$('#areaParentCode').val("");
				$('#areaNameParent').val("");
				$('#areaCode').val("");
				$('#areaName').val("");
				AreaTreeCatalog.resetSearch();
				AreaTreeCatalog.search();
				AreaTreeCatalog.loadAreaTree();
				$('.SuccessMsgStyle').hide();
			 }, 3000);
		});
		return false;
	},
	search:function(parentId){
		//AreaTreeCatalog.resetSearch();
		var param = new Object();
		param.areaCode=$('#areaCode').val().trim();
		param.areaName=$('#areaName').val().trim();
		param.status=$('#status').val();
		if($('#parentId').val()!='' && $('#parentId').val()!='-1'){
			param.parentId=$('#parentId').val();
		}
		$("#grid").datagrid('load',param);
	},
	loadAreaTree:function(){		
		$('#tree').jstree({
	        "plugins": ["themes", "json_data","ui","contextmenu"],
	        "themes": {
	            "theme": "classic",
	            "icons": false,
	            "dots": true
	        },
	        "json_data": {
	        	"ajax" : {
	        		"url": function(node){
	                      if( node == -1 ){
	                    	  return "/rest/catalog/area-tree-ex/0/tree.json";
	                      } else {
	                    	  return "/rest/catalog/area-tree-ex/" +node.attr("id")+"/tree.json";
	                      }
	                    },
	                "data" : function (n) {
	                        return { id : n.attr ? n.attr("id") : 0 };
	                    },
	                "complete":function(){
	                	AreaTreeCatalog.setSelectedNode(AreaTreeCatalog.selectNodeTree);
	    	        }
	            }        	
	        },
	        "contextmenu" : {
                items : {
                    "chinhsua" : {
                        "separator_before"  : false,
                        "separator_after"   : true,
                        "label"             : "<span style='font-size:13px;'>" +Utils.XSSEncode(common_information_detail)+"</span>",
                        "icon"				:"/resources/images/icon_edit.png",
                        "action"            : function(obj){
                        	$('#errExcelMsg').hide();
                        	$('#findInfo').html(area_tree_area_update);
                        	var nodeId = obj.attr('id').trim();
                        	AreaTreeCatalog.setSelectedNode(nodeId);	
                        	if(nodeId != '-1'){
                        		AreaTreeCatalog.parentsId = $.jstree._reference('#tree')._get_parent(obj).attr("id");
							}else{
								AreaTreeCatalog.parentsId = '-1';
								return false;
							}
                        	$.ajax({
                    			type : "POST",
                    			url : '/catalog/area-tree/get-AreaDetail-ById',
                    			dataType : "json",
                    			data :$.param({areaId : nodeId}, true),
                    			success : function(result) {
                    				//hidden div chua grid
                    				$('#divContentEdit').show();
                    				$('#GridSection').hide();
                    				$('#hideSupport').hide();
                    				$('#titleGrid').hide();		
                    				$('#btnUpdate').show();
                    				$('#btnSearch').hide();
                    				$('#title').html(area_tree_information_area);
                    				$('#areaId').val(nodeId.toString());
                    				var area = result.area;
                    				$('#areaCode').val(Utils.XSSEncode(area.areaCode));
                    				$('#areaName').val(Utils.XSSEncode(area.areaName));
                    				if(area.parentArea != null){
                    					$('#areaNameParent').val(Utils.XSSEncode(area.parentArea.areaName));
                    					$('#areaParentCode').val(Utils.XSSEncode(area.parentArea.areaCode));
                    				}
                    				else{
                    					$('#areaNameParent').val('');
                    					$('#areaParentCode').val('');
                    				}
                    				$('#areaCode').attr("disabled","disabled");
                    				$('#areaName').attr("disabled","disabled");
                    				$('#areaParentCode').attr("disabled","disabled");
                    				$('#areaNameParent').attr("disabled","disabled");
                    				if(area.status=='STOPPED'){
                    					$('#status').val(0).change();
                    				}else{
                    					$('#status').val(1).change();
                    				}
                    				AreaTreeCatalog.isUpdate = true;
                    				jQuery('#status').children('option[value="-2"]').hide();
                    				$('#status').focus();
                    				//Xu ly phan quyen control
                    				Utils.functionAccessFillControl('searchForm', function (data) {
                    					
                    				});
                    			}
                        	});
                        }
                    }
                }
            }

		});	
		$('#tree').bind("loaded.jstree", function(event, data){			
			AreaTreeCatalog.setSelectedNode("-1");
		});
		
		$('#tree').bind('before.jstree',
				function(event, data) {
					if (data.func == 'refresh') {
						AreaTreeCatalog.isRefresh = true;
					}
			});
		
		$('#tree').bind("select_node.jstree", function (event, data) {
        	$('#errExcelMsg').html('').hide();
			var name = data.inst.get_text(data.rslt.obj);
			var id = data.rslt.obj.attr("id");
			AreaTreeCatalog.selectNodeTree = id;
			$('#parentId').val(id);
			if(id == -1){
				id = '';
			}else{
				AreaTreeCatalog.parentsId = data.inst._get_parent(data.rslt.obj).attr("id"); // lay id node cha
			}
			AreaTreeCatalog.isUpdate = false;
			if(AreaTreeCatalog.isRefresh){
				AreaTreeCatalog.isRefresh = false;
				return false;
			}
			AreaTreeCatalog.search(id);
			AreaTreeCatalog.resetOnLoad();
			AreaTreeCatalog.searchFilter == null;
	    });
	},
	/**phuocdh2 get download file **/
	download:function(){
		var dataModel = new Object();
		try {
			dataModel.reportCode = $('#function_code').val();
		} catch(e) {
			// Oop! Error
		}
		var kData = $.param(dataModel,true);
//		var path = '';
		$.ajax({
			type : "POST",
			url : '/catalog/area-tree/get-template-excel',
			data : (kData),
			dataType : "json",
			success : function(result) {
				var filePath = ReportUtils.buildReportFilePath(result.path);
				window.location.href = filePath;
//				path = filePath;
//				$('#downloadTemplate').attr('href', path);
			}
    	});
		//$('#downloadTemplate').attr('href', excel_template_path + 'catalog/Bieu_mau_danh_muc_cay_dia_ban.xls');
	}
	,
	exportExcelData:function(){
		$('#divOverlay').addClass('Overlay');
		$('#imgOverlay').show();
		$('#errMsg').html('').hide();
		$('#errExcelMsg').html('').hide();
		var areaCode = $('#areaCode').val().trim();
		var areaName = $('#areaName').val().trim();
		var status = $('#status').val().trim();
		var isAll = false;
		if($('#isAll').attr('checked') == 'checked') {
			isAll = true;
		}
		var data = new Object();
		data.areaCode = areaCode;
		data.areaName = areaName;
		data.status = status;
		data.isAll = isAll;
		if($('#parentId').val()!='' && $('#parentId').val()!='-1'){
			data.parentId=$('#parentId').val();
		}
		var url = "/catalog/area-tree/getlistexceldata";
		ReportUtils.exportReport(url,data,'errMsg');
	},
	importExcel:function(){	
		Utils.importExcelUtils(function(data){
			if(data.message!=undefined && data.message!=null && data.message.trim().length >0){
				$('#errExcelMsg').html(data.message.trim()).change().show();
			}else{
				AreaTreeCatalog.search();
				AreaTreeCatalog.loadAreaTree();
				$('#successMsgImport').html(msgCommon1).show();
				var tm = setTimeout(function(){
					$('#successMsgImport').html("").hide();
					clearTimeout(tm);
				 }, 4000);
			}
		}, 'importFrm', 'excelFile');
		return false;
	},	
	setSelectedNode:function(nodeId){
		$('#tree a').removeClass('jstree-hovered jstree-clicked');
		$('#tree #' +nodeId+' >a').addClass('jstree-clicked');
	},
	resetOnLoad:function(){
		setSelectBoxValue('status',1);				
		$('#areaCode').val('');
		$('#areaName').val('');
		$('#divContentEdit').hide();
		$('#GridSection').show();
		$('#btnUpdate').hide();
		$('#btnSearch').show();
		$('#areaCode').removeAttr('disabled');
		$('#areaName').removeAttr('disabled');
	},
	resetSearch:function(){
		setSelectBoxValue('status',1);				
		$('#divContentEdit').hide();
		$('#GridSection').show();
		$('#btnUpdate').hide();
		$('#btnSearch').show();
		$('#areaCode').removeAttr('disabled');
		$('#areaName').removeAttr('disabled');
	}
};
/*
 * END OF FILE - /web/web/resources/scripts/business/catalog/vnm.area-tree-catalog.js
 */

/*
 * START OF FILE - /web/web/resources/scripts/business/catalog/vnm.bary-centric-programme-catalog.js
 */
var BaryCentricProgrammeCatalog = {
	_xhrSave : null,
	_xhrDel: null,
	_mapShop:null,
	_mapProduct:null,
	_listShopId:null,
	_lstSaleTypeData: new Map(),
	_lstTypeData: new Map(),
	_firstCount: true,
	_importHtml: '',
	_mapLstProduct:new Map(),
	hideAllTab: function() {
		$('.ErrorMsgStyle').hide();
		$('#tab1').removeClass('Active');
		$('#tab2').removeClass('Active');
		$('#tab3').removeClass('Active');
		$('#spCTTTTab').hide();
		$('#dvtgCTTTTab').hide();
		$('#htbhCTTTTab').hide();
	},
	showTab1: function() {
		BaryCentricProgrammeCatalog.hideAllTab();
		$('#tab1').addClass('Active');
		$('#htbhCTTTTab').show();
		$('#errExcelMsg').html('').hide();
		$('.IdImportDivTpm').hide();
		$('#codeHTBH').focus();
		Utils.bindAutoButtonEx('.ContentSection', 'btnSearchSaleType');
		$('#codeHTBH').val('').change();
		$('#nameHTBH').val('').change();
		BaryCentricProgrammeCatalog.searchHTBH();
	},
	showTab3: function() {
		BaryCentricProgrammeCatalog.hideAllTab();
		$('#tab3').addClass('Active');
		$('#dvtgCTTTTab').show();
		$('#divImportGrroupSpDv').html(BaryCentricProgrammeCatalog._importHtml).change();
		$('.IdImportDivTpm').each(function() {
			$(this).prop('id', 'tab_dv_group_edit_import').addClass('cmsiscontrol');
		});
		Utils.functionAccessFillControl('divImportGrroupSpDv');
		$('.IdImportDivTpm').show();
		$('#shopCode').focus();
		Utils.bindAutoButtonEx('.ContentSection', 'btnSearchShop');
		if ($('#downloadTemplate').length > 0) {
			$('#downloadTemplate').attr( 'href', excel_template_path + 'catalog/Bieu_mau_danh_muc_NPPThamgiavaoCTTT_import.xls');
		}
		$('#excelType').val(2);
		$('#errExcelMsg').html('').hide();
		var value = $('#divWidth').width() - 50;
		$('#shopCode').val('').change();
		$('#shopName').val('').change();
		var dataWidth = (value * 80)/100;
		var editWidth = (value * 10)/100;
		var deleteWidth = (value * 9)/100+6;
		var statusPermission = $('#statusPermission').val();
		var titleOpenDialogShop = "";
		if (statusPermission == 2) {
			titleOpenDialogShop = '<a id="btnAddGridS" href="javascript:void(0);" onclick="return BaryCentricProgrammeCatalog.openShopDialog();"><img src="/resources/images/icon-add.png"></a>';
		}
		$('#exGrid').treegrid({
		    url:  BaryCentricProgrammeCatalog.getShopGridUrl($('#progId').val(), '', ''),
		    width:($('#divWidth').width()-50),  
	        height: 'auto', 
	        fitColumns: true,
	        idField: 'id',  
	        treeField: 'data',
		    columns:[[  
		        {field: 'data', title: 'Đơn vị', resizable: false, width: dataWidth, formatter: function(value, row, index) {
			    		return Utils.XSSEncode(value);
	            }},
		        {field: 'edit', title: titleOpenDialogShop, hidden: true, width: editWidth, align: 'center', resizable: false, formatter: function(value, row, index) {
			        	if (row.isShop == 0) {
			        		if (statusPermission == 2) {
			        			return '<a id="btnEditGridS"' + index+ ' title="Thêm mới" href="javascript:void(0);" onclick="BaryCentricProgrammeCatalog.openShopDialog('+row.id+ ');"><img src="/resources/images/icon-add.png"></a>';
			        		}
			        	}
		        }},
		        {field: 'delete', title: '', width: deleteWidth, hidden: true, align: 'center', resizable: false, formatter: function(value, row, index) {
		        		if (statusPermission == 2) {
		        			return '<a id="btnDelGridS"' +index+ ' title="Xóa" href="javascript:void(0);" onclick="BaryCentricProgrammeCatalog.deleteFocusShopMap('+row.id+ ');"><img src="/resources/images/icon-delete.png"></a>';
		        		}
		        }}
		    ]],
		    onLoadSuccess: function() {
		    	$('#exGrid').datagrid('resize');
		    	
		    	var arrEdit =  $('#shopGridContainer td[field="edit"]');
				if (arrEdit != undefined && arrEdit != null && arrEdit.length > 0) {
				for (var i = 0, size = arrEdit.length; i < size; i++) {
					$(arrEdit[i]).prop("id", "tab_dv_group_edit_" + i).addClass("cmsiscontrol");//Khai bao id danh cho phan quyen
				  }
				}
				var arrDelete =  $('#shopGridContainer td[field="delete"]');
				if (arrDelete != undefined && arrDelete != null && arrDelete.length > 0) {
				  for (var i = 0, size = arrDelete.length; i < size; i++) {
					$(arrDelete[i]).prop("id", "tab_dv_group_edit_" + i).addClass("cmsiscontrol");//Khai bao id danh cho phan quyen
				  }
				}
				Utils.functionAccessFillControl('shopGridContainer', function(data) {
				//Xu ly cac su kien lien quan den cac control phan quyen
				var arrTmpLength =  $('#shopGridContainer td[id^="tab_dv_group_edit_"]').length;
				var invisibleLenght = $('.isCMSInvisible[id^="tab_dv_group_edit_"]').length;
				if (arrTmpLength > 0 && arrTmpLength != invisibleLenght) {
					$('#grid').datagrid("showColumn", "delete");
				} else {
					$('#grid').datagrid("hideColumn", "delete");
				}
				arrTmpLength =  $('#shopGridContainer td[id^="tab_dv_group_edit_"]').length;
				invisibleLenght = $('.isCMSInvisible[id^="tab_dv_group_edit_"]').length;
				if (arrTmpLength > 0 && arrTmpLength != invisibleLenght) {
					$('#grid').datagrid("showColumn", "edit");
				} else {
					$('#grid').datagrid("hideColumn", "edit");
					}
				});
		    }
		});
	},
	/**
	 * Import san pham CTTT
	 * @modify hunglm16
	 * @since 03/11/2015
	 * */
	importExcel: function() {
		$('.ErrorMsgStyle').hide();
 		$('#isView').val(0);
 		var options = { 
				beforeSubmit: BaryCentricProgrammeCatalog.beforeImportExcel,   
		 		success:      BaryCentricProgrammeCatalog.afterImportExcelUpdate, 
		 		type: "POST",
		 		dataType: 'html',
		 		data:({excelType:$('#excelType').val(), id:$('#selId').val()})
		 	}; 
		$('#importFrm').ajaxForm(options);
 		$('#importFrm').submit();
 		return false;
 	},
 	/**
	 * Before - Import san pham CTTT
	 * @modify hunglm16
	 * @since 03/11/2015
	 * @description chi dung cho tab import san pham, khong ke thua
	 * , yeu cau chuyen qua dung ham chung khi phat trien chuc nang moi 
	 * */
 	beforeImportExcel: function() {
		if (!previewImportExcelFile(document.getElementById("excelFile"))) {
			return false;
		}
		$('#errExcelMsg').hide();
		$('#divOverlay').show();
		$('#imgOverlay').show();
		return true;
	},
	/**
	 * After - Import san pham CTTT
	 * @modify hunglm16
	 * @since 03/11/2015
	 * @description chi dung cho tab import san pham, khong ke thua
	 * , yeu cau chuyen qua dung ham chung khi phat trien chuc nang moi 
	 * */
	afterImportExcelUpdate: function(responseText, statusText, xhr, $form) {
		hideLoadingIcon();
		if (statusText == 'success') {
			$("#responseDiv").html(responseText);
			var newToken = $('#responseDiv #newToken').val();
			//console.log(newToken);
			if (newToken != null && newToken != undefined && newToken != '') {
				$('#token').val(newToken);
				$('#tokenImport').val(newToken);
			}
			if ($('#errorExcel').html().trim() == 'true' || $('#errorExcelMsg').html().trim().length > 0) {
				$('#errExcelMsg').html($('#errorExcelMsg').html()).show();
			} else {
				var totalRow = parseInt($('#totalRow').html().trim());
				var numFail = parseInt($('#numFail').html().trim());
				var fileNameFail = $('#fileNameFail').html();
				var mes = format(msgErr_result_import_excel, (totalRow - numFail), numFail);
				if (numFail > 0) {
					mes += ' <a href="' + fileNameFail + '">Xem chi tiết lỗi</a>';
				}
				if ($('#excelFile').length != 0 && $('#fakefilepc').length != 0) {
					try {
						$('#excelFile').val('');
						$('#fakefilepc').val('');
					} catch (err) {
					}
				}
				$('#errExcelMsg').html(mes).show();
			}
			//Reload lai grid
			$("#grid").datagrid("reload");
		}
	},
	gotoTab: function(tabIndex) {
		$('.ErrorMsgStyle').hide();
		BaryCentricProgrammeCatalog.deactiveAllMainTab();
		switch (tabIndex) {
		case 0:
			$('#tabContent1').show();
			$('#tab1 a').addClass('Active');
			break;
		case 1:
			$('#tabContent2').show();
			$('#tab2 a').addClass('Active');
			AttributesManager.getAttributesForm('tabContent2',AttributesManager.FOCUS_PROGRAM);
			$('#typeAttribute').val(AttributesManager.FOCUS_PROGRAM);
			break;
		default:
			$('#tabContent1').show();
			break;
		}
	},
	upload : function() { //upload trong fancybox
		$('.ErrorMsgStyle').hide();
		var options = { 					 		
	 		beforeSubmit: ProductLevelCatalog.beforeImportExcel,   
	 		success:      ProductLevelCatalog.afterImportExcelUpdate, 
	 		type: "POST",
	 		dataType: 'html'
	 	}; 
		$('#importShopFrm').ajaxForm(options);
		$.messager.confirm('Xác nhận', 'Bạn có muốn nhập từ file?',function(r) {
			if (r) {						
				$('#importShopFrm').submit();						
			}
		});
	},
	deactiveAllMainTab: function() {
		$('.ErrorMsgStyle').hide();
		$('#tab1 a').removeClass('Active');
		$('#tab2 a').removeClass('Active');
		$('#tabContent1').hide();
		$('#tabContent2').hide();
	},
	getGridUrl: function(code,name,shopId,fDate,tDate,status) {		
		return "/catalog/bary-centric-programme/search?code=" + encodeChar(code) + "&name=" + encodeChar(name) + "&shopId=" + shopId + "&fromDate=" + fDate + "&toDate=" + tDate + '&status=' + status;
	},	
	search: function() {
		$('.ErrorMsgStyle').hide();
		var code = $('#code').val().trim();
		var name = $('#name').val().trim();
		var shopId = $('#shopTree').combotree('getValue');//$('#shopId').val().trim();
		var fromDate = $('#fDate').val();
		var toDate = $('#tDate').val();	
		var status = 1;
		if ($('#status').length > 0) {
			status = $('#status').val().trim();
		}
		var msg = '';
//		if (msg.length == 0) {
//			msg = Utils.getMessageOfSpecialCharactersValidate('code', 'Mã CTTT',Utils._CODE);
//		}
//		if (msg.length == 0) {
//			msg = Utils.getMessageOfSpecialCharactersValidate('name', 'Tên CTTT');
//		}		 
		if (msg.length == 0) {
			msg = Utils.getMessageOfInvalidFormatDate('fDate', 'Từ ngày');
		}
		if (msg.length == 0) {
			msg = Utils.getMessageOfInvalidFormatDate('tDate', 'Đến ngày');
		}
		var fDate = $('#fDate').val();
		var tDate = $('#tDate').val();
		if (msg.length == 0) {
			if (fDate != '' && !Utils.isDate(fDate)) {
				msg = 'Từ ngày không hợp lệ, vui lòng nhập theo định dạng dd/MM/yyyy';
				$('#fDate').focus();
			}
		}
		if (msg.length == 0) {
			if (tDate != '' && !Utils.isDate(tDate)) {
				msg = 'Đến ngày không hợp lệ, vui lòng nhập theo định dạng dd/MM/yyyy';
				$('#tDate').focus();
			}
		}
		var currentTime = new Date();
		var month = currentTime.getMonth() + 1;
		var day = currentTime.getDate();
		var year = currentTime.getFullYear();
		if (msg.length == 0) {
			if (fDate != '' && tDate != '') {
				if (!Utils.compareDate(fDate, tDate)) {
					msg = 'Từ ngày phải là ngày trước hoặc cùng ngày với đến ngày';	
					$('#fDate').focus();
				}
			}
		}

		if (msg.length > 0) {
			$('#errMsg').html(msg).show();
			return false;
		}
		var tm = setTimeout(function() {
			$('#code').focus();
		}, 500);
		var url = BaryCentricProgrammeCatalog.getGridUrl(code,name,shopId,fromDate,toDate,status);
		$("#listCTTTGrid").datagrid({url:url,pageNumber:1});
		return false;
	},
	saveInfo: function() {
		var msg = '';
		$('.ErrorMsgStyle').hide();
		msg = Utils.getMessageOfRequireCheck('code', 'Mã CTTT');
		if (msg.length == 0) {
			msg = Utils.getMessageOfSpecialCharactersValidate('code', 'Mã CTTT',Utils._CODE);
		}
		if (msg.length == 0) {
			msg = Utils.getMessageOfRequireCheck('name', 'Tên CTTT');
		}
		if (msg.length == 0) {
			msg = Utils.getMessageOfSpecialCharactersValidate('name', 'Tên CTTT');
		}		
		if (msg.length == 0) {
			msg = Utils.getMessageOfRequireCheck('fDate', 'Từ ngày');
		} 
		if (msg.length == 0) {
			msg = Utils.getMessageOfInvalidFormatDate('fDate', 'Từ ngày');
		}
		if (msg.length == 0) {
			msg = Utils.getMessageOfInvalidFormatDate('tDate', 'Đến ngày');
		}
		if (msg.length == 0) {
			msg = Utils.getMessageOfRequireCheck('status', 'Trạng thái',true);
		}
		var fDate = $('#fDate').val();
		var tDate = $('#tDate').val();
		if ($('#status').val().trim() == activeType.WAITING) {
			if (msg.length == 0) {
				if (fDate != '' && !Utils.isDate(fDate)) {
					msg = 'Từ ngày không hợp lệ, vui lòng nhập theo định dạng dd/MM/yyyy';
					$('#fDate').focus();
				}
			}
		}
		if (msg.length == 0) {
//			if (($('#stableToDate').val() != '') && ($('#stableToDate').val().trim() != $('#tDate').val().trim())) {
			if ($('#stableToDate').val().trim() != $('#tDate').val().trim()) {
				if (tDate != '' && !Utils.isDate(tDate)) {
					msg = 'Đến ngày không hợp lệ, vui lòng nhập theo định dạng dd/MM/yyyy';
					$('#tDate').focus();
				}
			}
		}
		var currentTime = new Date();
		var month = currentTime.getMonth() + 1;
		var day = currentTime.getDate();
		var year = currentTime.getFullYear();
		var proStatus = $('#proStatus').val();
		if ($('#status').val().trim() == activeType.RUNNING) {
			if (msg.length == 0) {
				if (fDate != '' && tDate != '') {
					if (!Utils.compareDate(fDate, tDate)) {
						msg = 'Từ ngày phải là ngày trước hoặc cùng ngày với đến ngày';	
						$('#fDate').focus();
					}
				}
			}
		}

		/*if ($('#status').val().trim() == activeType.RUNNING && proStatus == activeType.WAITING) {
			if (msg.length == 0) {
				if (fDate != '') {
					if (!Utils.compareDate(day + '/' + month + '/' + year,fDate)) {
						msg = 'Từ ngày phải lớn hơn hoặc bằng ngày hiện tại.';	
						$('#fDate').focus(); 
					}
				}
			}
		}*/
		if (msg.length == 0) {
//			if (($('#stableToDate').val() != '') && ($('#stableToDate').val().trim() != $('#tDate').val().trim())) {
			if ($('#stableToDate').val().trim() != $('#tDate').val().trim()) {
				if (tDate != '') {
					if (!Utils.compareDate(day + '/' + month + '/' + year,tDate)) {
						msg = 'Đến ngày phải lớn hơn hoặc bằng ngày hiện tại.';	
						$('#tDate').focus();
					}
				}
			}
		}	
		if (msg.length > 0) {
			$('#errMsgInfo').html(msg).show();
			return false;
		}
		var dataModel = new Object();		
		dataModel.id = $('#selId').val().trim();
		dataModel.code = $('#code').val().trim();
		dataModel.name = $('#name').val().trim();
		dataModel.fromDate = $('#fDate').val().trim();
		dataModel.toDate = $('#tDate').val().trim();
		dataModel.status = $('#status').val().trim();
		if (!Utils.validateAttributeData(dataModel, '#errMsgInfo')) {
			return false;
		}
		Utils.addOrSaveData(dataModel, "/catalog/bary-centric-programme/save-info", BaryCentricProgrammeCatalog._xhrSave, 'errMsgInfo', function(data) {
			$('#selId').val(data.id);	
			$('#subContentId').val(0);
			$.cookie('proTabOrder', 1);
			window.location.href = '/catalog/bary-centric-programme/change?id=' + data.id + '&subContentId=0';	
//			$('#subContent').hide();
			showSuccessMsg('successMsg1',data);
		});		
		return false;
	},
	getSelectedProgram: function(id) {
		if (id!= undefined && id!= null) {
			location.href = '/catalog/bary-centric-programme/change?id=' + id + '&subContentId=1';			
		} else {
			$('#subContentId').val(0);
			location.href = '/catalog/bary-centric-programme/change?subContentId=0';
		}		
		return false;
	},
	saveShop: function() {
		var msg = '';
		$('.ErrorMsgStyle').hide();
		var areaId = -2;
		var regionId = -2;
		var shopCode = '';
		var shopMapId = $('#selShopMapId').val().trim();
		if (shopMapId.length == 0 || shopMapId == 0) {
			if ($('#rbArea').is(':checked')) {
				msg = Utils.getMessageOfRequireCheck('area', 'Miền');
				areaId = $('#area').val();
			}
			if ($('#rbRegion').is(':checked')) {
				msg = Utils.getMessageOfRequireCheck('region', 'Vùng');
				regionId = $('#region').val();
			}
			if ($('#rbShop').is(':checked')) {
				msg = Utils.getMessageOfRequireCheck('shopCode', 'Mã đơn vị');
				if (msg.length == 0) {
					msg = Utils.getMessageOfSpecialCharactersValidate('shopCode', 'Mã đơn vị',Utils._CODE);
				}
				shopCode = $('#shopCode').val();
			}
		}
		
		if (msg.length == 0) {
			msg = Utils.getMessageOfRequireCheck('shopStatus', 'Trạng thái',true);
		}
		if (msg.length > 0) {
			$('#errMsgShop').html(msg).show();
			return false;
		}
		var dataModel = new Object();		
		dataModel.id = $('#selId').val().trim();
		dataModel.shopMapId = $('#selShopMapId').val().trim();
		dataModel.areaId = areaId;
		dataModel.regionId = regionId;		
		dataModel.shopCode = shopCode;		
		dataModel.status = $('#shopStatus').val().trim();
		Utils.addOrSaveRowOnGrid(dataModel, "/catalog/bary-centric-programme/save-shop", BaryCentricProgrammeCatalog._xhrSave, 'shopGrid', 'errMsgShop', function(data) {
			$('#selShopMapId').val(0);	
			$("#shopGrid").trigger("reloadGrid");
			BaryCentricProgrammeCatalog.resetShopForm();
			showSuccessMsg('successMsgShop',data);
		}, 'shopLoading');		
		return false;
	},
	getSelectedShopRow: function(rowId,status) {
		var code = $("#shopGrid").jqGrid ('getCell', rowId, 'shop.shopCode');
		var name = $("#shopGrid").jqGrid ('getCell', rowId, 'shop.shopName');
		var id = $("#shopGrid").jqGrid ('getCell', rowId, 'id');		
		$('#selShopMapId').val(id);
		$('#shopCode').val(code);
		$('#shopName').val(name);
		setSelectBoxValue('shopStatus',Utils.getStatusValue(status));
		disabled('shopCode');
		$('#btnAddShop').hide();
		$('#btnEditShop').show();
		$('#btnCancelShop').show();
		return false;
	},
	resetShopForm: function() {
		$('.ErrorMsgStyle').hide();
		$('#btnAddShop').show();
		$('#btnEditShop').hide();
		$('#btnCancelShop').hide();
		enable('shopCode');
		setSelectBoxValue('shopStatus');
		setSelectBoxValue('area');
		setSelectBoxValue('region');
		$('#selShopMapId').val(0);
		$('#shopCode').val('');
		$('#shopName').val('');
	},
	deleteShopRow: function(id) {
		$('.ErrorMsgStyle').hide();
		var dataModel = new Object();
		dataModel.shopMapId = id;	
		Utils.deleteSelectRowOnGrid(dataModel, 'đơn vị', '/catalog/bary-centric-programme/delete-shop', BaryCentricProgrammeCatalog._xhrDel, 'shopGrid', 'errMsgShop',function(data) {
			showSuccessMsg('successMsgShop',data);
		}, 'shopLoading');		
		return false;
	},
	saveProduct: function() {
		var msg = '';
		$('.ErrorMsgStyle').hide();
		var catCode = '';
		var subCatCode = '';
		var productCode = '';
		var staffTypeId = $('#staffType').val();
		if ($('#rbCat').is(':checked')) {
			msg = Utils.getMessageOfRequireCheck('catCode', 'Ngành hàng');
			catCode = $('#catCode').val();
		}
		if ($('#rbSubCat').is(':checked')) {
			msg = Utils.getMessageOfRequireCheck('subCatCode', 'Ngành hàng con');
			subCatCode = $('#subCatCode').val();
		}
		if ($('#rbProduct').is(':checked')) {
			msg = Utils.getMessageOfRequireCheck('productCode', 'Mã sản phẩm');			
			productCode = $('#productCode').val();
		}
		if (msg.length == 0) {
			msg = Utils.getMessageOfRequireCheck('staffType', 'Loại nhân viên',true);
		}
		
		if (msg.length > 0) {
			$('#errMsgProduct').html(msg).show();
			return false;
		}
		var dataModel = new Object();
		dataModel.id = $('#selId').val().trim();
		dataModel.catCode = catCode;
		dataModel.subCatCode = subCatCode;		
		dataModel.productCode = productCode;
		dataModel.staffTypeId = staffTypeId;
		Utils.addOrSaveRowOnGrid(dataModel, "/catalog/bary-centric-programme/save-product", BaryCentricProgrammeCatalog._xhrSave, 'productGrid', 'errMsgProduct', function(data) {
			$("#productGrid").trigger("reloadGrid");
			$('#catCode').val('');
			$('#subCatCode').val('');
			$('#productCode').val('');
			$('#productName').val('');
			setSelectBoxValue('staffType');		
			showSuccessMsg('successMsgProduct',data);
		}, 'productLoading');		
		return false;
	},
	showOthersTab: function() {
		$('#tabActive2').removeClass('Disable');
		$('#tabActive3').removeClass('Disable');
		$('#tabActive4').removeClass('Disable');
		$('#tabActive5').removeClass('Disable');
		$('#tab1').bind('click',BaryCentricProgrammeCatalog.showTab1);
		$('#tab2').bind('click',BaryCentricProgrammeCatalog.showTab2);
		$('#tab3').bind('click',BaryCentricProgrammeCatalog.showTab3);
	}
	,deleteProductRow: function(id) {
		$('.ErrorMsgStyle').hide();
		var dataModel = new Object();
		dataModel.productId = id;	
		Utils.deleteSelectRowOnGrid(dataModel, 'Sản phẩm', '/catalog/bary-centric-programme/delete-product', BaryCentricProgrammeCatalog._xhrDel, 'productGrid', 'errMsgProduct',function(data) {
			showSuccessMsg('successMsgProduct',data);
		}, 'productLoading');		
		return false;
	},
	searchShop: function() {
		$('.ErrorMsgStyle').hide();
		var code = $('#shopCode').val().trim();
		var name = $('#shopName').val().trim();
		var id = $('#progId').val();
		var url = BaryCentricProgrammeCatalog.getShopGridUrl(id, code,name);
		$("#exGrid").treegrid({url:url});
	},
	showDialogCreateShop: function() {
		$('.ErrorMsgStyle').hide();
		var html = $('#shopTreeDialog').html();
		$.fancybox(html,
				{
					modal: true,
					autoResize: false,
					title: 'Thêm đơn vị tham gia CTTT',
					afterShow: function() {
						Utils.resetCheckboxStateOfTree();
						Utils.bindAutoSearch();	
						$('#shopTreeDialog').html('');
						$('#successMsgShopDlg').html('').hide();
						var focusProgramId = $('#selId').val();
						$('#shopCodeDlg').focus();
						var shopCode = encodeURI('');
						var shopName = encodeURI('');	
						BaryCentricProgrammeCatalog.loadShopTreeOnDialog('/rest/catalog/focus-program/shop/list.json?focusProgramId=' + focusProgramId + '&shopCode=' + encodeChar(shopCode) + '&shopName=' + encodeChar(shopName));
						BaryCentricProgrammeCatalog._mapShop = new Map();
						$('#shopTree').bind("loaded.jstree open_node.jstree close_node.jstree", function (event, data) {
							if ($('#shopTreeContent').data('jsp') != undefined || $('#shopTreeContent').data('jsp') != null) {
								$('#shopTreeContent').data('jsp').destroy();
								setTimeout(function() {
									$('#shopTreeContent').jScrollPane();
								},500);	
							} else {
								setTimeout(function() {
									$('#shopTreeContent').jScrollPane();
								},500);	
							}						
		    			});
					},
					afterClose: function() {
						$('#shopTreeDialog').html(html);						
					}
				}
			);
		return false;
	},
	getCheckboxStateOfTreeEx: function(treeId,MAP,selector,nodeType) {
		for (var i=0;i<MAP.size();++i) {
			var _obj = MAP.get(MAP.keyArray[i]);
			var type = true;			
			$('#' + selector).each(function() {
				var _id = $(this).attr('id');
				if (nodeType!=null && nodeType!=undefined) {
					var getType = $(this).attr('contentitemid');
					type = (nodeType==getType);
				}
				if (_id==_obj && type) {
					$(this).removeClass('jstree-unchecked');
					$(this).addClass('jstree-checked');
				}
			});
		}
	},	
	searchShopOnTree: function() {
		$('.ErrorMsgStyle').hide();
		//Utils.getCheckboxStateOfTree();
		$('#shopTree li').each(function() {
			var _id = $(this).attr('id');
			if ($(this).hasClass('jstree-checked')) {
				BaryCentricProgrammeCatalog._mapShop.put(_id,_id);	
			} else {
				BaryCentricProgrammeCatalog._mapShop.remove(_id);
			}		
		});
		var focusProgramId = $('#selId').val();
		var shopCode = encodeChar($('#shopCodeDlg').val().trim());
		var shopName = encodeChar($('#shopNameDlg').val().trim());
		$('#shopTreeContent').data('jsp').destroy();
		BaryCentricProgrammeCatalog.loadShopTreeOnDialog('/rest/catalog/focus-program/shop/list.json?focusProgramId=' + focusProgramId + '&shopCode=' + encodeChar(shopCode) + '&shopName=' + encodeChar(shopName));
		setTimeout(function() {
			$('#shopTreeContent').jScrollPane();
			$('#shopTree').bind("loaded.jstree open_node.jstree close_node.jstree", function (event, data) {
				if ($('#shopTreeContent').data('jsp') != undefined || $('#shopTreeContent').data('jsp') != null) {
					$('#shopTreeContent').data('jsp').destroy();
					setTimeout(function() {
						$('#shopTreeContent').jScrollPane();
					},500);	
				} else {
					setTimeout(function() {
						$('#shopTreeContent').jScrollPane();
					},500);	
				}
				BaryCentricProgrammeCatalog.getCheckboxStateOfTreeEx('shopTree',BaryCentricProgrammeCatalog._mapShop, 'shopTree li');
			});
			BaryCentricProgrammeCatalog.getCheckboxStateOfTreeEx('shopTree',BaryCentricProgrammeCatalog._mapShop, 'shopTree li');
		}, 500);
		$('.fancybox-inner #shopCodeDlg').focus();
	},
	selectListShop: function() {
		$('.ErrorMsgStyle').hide();
		$('#successMsgShopDlg').html('').hide();
		$.fancybox.update();		
		var arrId = new Array();
		$('.jstree-checked').each(function() {
		    var _id= $(this).attr('id');
		    if ($('#' + _id + ' ul li').length >0 && (!$(this).parent().parent().hasClass('jstree-checked'))) {
		    	BaryCentricProgrammeCatalog._mapShop.put(_id,_id);	
		    } else if ($('#' + _id ).length >0 && (!$(this).parent().parent().hasClass('jstree-checked'))) {
		    	BaryCentricProgrammeCatalog._mapShop.put(_id,_id);	
			} else {
		    	BaryCentricProgrammeCatalog._mapShop.remove(_id);	
			}	
		});
//		$('#shopTree li').each(function() {
//			var _id = $(this).attr('id');			
//			if ($(this).hasClass('jstree-checked')) {
//				BaryCentricProgrammeCatalog._mapShop.put(_id,_id);		
//			} else {
//				BaryCentricProgrammeCatalog._mapShop.remove(_id);	
//			}			
//		});
		for (var i=0;i<BaryCentricProgrammeCatalog._mapShop.size();++i) {
			var _obj = BaryCentricProgrammeCatalog._mapShop.get(BaryCentricProgrammeCatalog._mapShop.keyArray[i]);
			arrId.push(_obj);
		}		
		var msg = '';
		if (arrId.length == 0) {
			msg = format(msgErr_required_choose_format, 'Đơn vị');
		}		
		if (msg.length > 0) {
			$('#errMsgShopDlg').html(msg).show();			
			$.fancybox.update();
			return false;
		}		
		var focusProgramId = $('#selId').val();
		var dataModel = new Object();		
		dataModel.lstShopId = arrId;		
		dataModel.id = focusProgramId;
		dataModel.status = -2;
		Utils.addOrSaveData(dataModel, '/catalog/bary-centric-programme/shop/save', BaryCentricProgrammeCatalog._xhrSave, 'errMsgShopDlg', function(data) {
			$('#successMsgShopDlg').html('Lưu dữ liệu thành công').show();
			$('#shopCodeDlg').val('');
			$('#shopNameDlg').val('');
			$('#shopCode').val('');
			$('#shopName').val('');
			setSelectBoxValue('shopStatus', activeType.RUNNING);
			Utils.resetCheckboxStateOfTree();
			BaryCentricProgrammeCatalog.searchShopOnTree();	
			BaryCentricProgrammeCatalog.searchShop();
			$(window).resize();
			$.fancybox.update();
			//$("#shopGrid").trigger("reloadGrid");
			setTimeout(function() {$.fancybox.close();}, 1000);
		}, null, '.fancybox-inner', null, null);
		return false;
	},
	showUpdateShopDetailDialog: function(rowId,status) {
		$('.ErrorMsgStyle').hide();
		var html = $('#shopDetailTreeDialog').html();
		$.fancybox(html,
				{
					modal: true,
					autoResize: false,
					title: 'Cập nhật thông tin đơn vị',					
					afterShow: function() {
						$('#shopDetailTreeDialog').html('');
						$('#errMsgShopDetailDlg').html('').hide();
						$('#successMsgShopDetailDlg').html('').hide();						
						
						var name = $("#shopGrid").jqGrid ('getCell', rowId, 'shop.shopName');
						var id = $("#shopGrid").jqGrid ('getCell', rowId, 'shop.id');					    						
						if (status == 'RUNNING') {
							status = 1;
						} else if (status == 'STOPPED') {
							status = 0;
						} else {
							status = -2;
						}						
						$('#shopNameDetailDlg').val(Utils.XSSEncode(name));
						$('#shopStatusDlgTmp').attr('id', 'shopStatusDlg');
						$('#shopStatusDlg').val(Utils.XSSEncode(status));
						$('#shopStatusDlg').addClass('MySelectBoxClass');
						$('#shopStatusDlg').customStyle();						
						$('#shopIdDetailDlg').val(id);
					},
					afterClose: function() {
						$('#shopDetailTreeDialog').html(html);						
					}
				}
			);
		return false;
	},
	saveDetailShop: function() {
		$('.ErrorMsgStyle').hide();
		$('#successMsgShopDetailDlg').html('').hide();
		$.fancybox.update();
		var msg = '';
		msg = Utils.getMessageOfRequireCheck('shopStatusDlg', 'Trạng thái',true);		
		if (msg.length > 0) {
			$('#errMsgShopDetailDlg').html(msg).show();
			$.fancybox.update();
			return false;
		}		
		var focusProgramId = $('#selId').val();
		var arrId = new Array();
		arrId.push($('#shopIdDetailDlg').val());
		var dataModel = new Object();
		dataModel.lstShopId = arrId;
		dataModel.id = focusProgramId;
		dataModel.status = $('#shopStatusDlg').val().trim();
		dataModel.numUpdate = 0; 
		Utils.addOrSaveData(dataModel, '/catalog/bary-centric-programme/shop/save', ProgrammeDisplayCatalog._xhrSave, 'errMsgShopDlg', function(data) {
			$('#successMsgShopDlg').html('Lưu dữ liệu thành công').show();						
			$.fancybox.update();
			BaryCentricProgrammeCatalog.searchShop();
			setTimeout(function() {$.fancybox.close();}, 1000);
		}, null, '.fancybox-inner', null, null);
		return false;
	},
	showDialogCreateProduct: function() {
		$('.ErrorMsgStyle').hide();
		var html = $('#productTreeDialog').html();
		$.fancybox(html,
				{
					modal: true,
					autoResize: false,
					title: 'Thêm thông tin sản phẩm',
					afterShow: function() {
						Utils.resetCheckboxStateOfTree();
						Utils.bindAutoSearch();	
						$('#productTreeDialog').html('');
						var focusProgramId = $('#selId').val();
						$('#productCodeDlg').focus();
						$('#saleManDlgTmp').attr('id', 'saleManDlg');
						$('#saleManDlg').addClass('MySelectBoxClass');
						$('#saleManDlg').customStyle();
						$('#typeDialog').addClass('MySelectBoxClass');
						$('#typeDialog').customStyle();
						$.getJSON('/rest/catalog/focus-program/' + focusProgramId + '/staff-type/list.json', function(data) {
							var arrHtml = new Array();
							arrHtml.push('<option value="-2">-Chọn hình thức bán hàng-</option>');
							for (var i=0;i<data.length;i++) {
								arrHtml.push('<option value="' + Utils.XSSEncode(data[i].value) + '">' + Utils.XSSEncode(data[i].name) + '</option>');
							}
							$('#saleManDlg').html(arrHtml.join(""));
							$('#saleManDlg').change();
						});						
						var productCode = encodeURI('');
						var productName = encodeURI('');						
						BaryCentricProgrammeCatalog.loadDataForTreeWithCheckboxEx('/rest/catalog/focus-program/product/list.json?focusProgramId=' + focusProgramId + '&productCode=' + encodeChar(productCode) + '&productName=' + encodeChar(productName) + '&nodeType=cat&catId=0&subCatId=0', 'productTree');
						BaryCentricProgrammeCatalog._mapProduct = new Map();
						$('#productTree').bind("loaded.jstree open_node.jstree close_node.jstree", function () {
							if ($('#productTreeContent').data('jsp') != undefined || $('#productTreeContent').data('jsp') != null) {
								$('#productTreeContent').data('jsp').destroy();
								setTimeout(function() {
									$('#productTreeContent').jScrollPane();
								},500);	
							} else {
								setTimeout(function() {
									$('#productTreeContent').jScrollPane();
								},500);	
							}
		    			});
					},
					afterClose: function() {
						$('#productTreeDialog').html(html);						
					}
				}
			);
		return false;
	},
	searchProductOnTree: function() {
		$('.ErrorMsgStyle').hide();
		//Utils.getCheckboxStateOfTree();
		$('#productTree li').each(function() {
			var _id = $(this).attr('id');
			var type = $(this).attr('contentitemid');
			if ($(this).hasClass('jstree-checked') && type=='product') {
				BaryCentricProgrammeCatalog._mapProduct.put(_id,_id);		
			} else {
				BaryCentricProgrammeCatalog._mapProduct.remove(_id);
			}
		});
		var focusProgramId = $('#selId').val();
		var productCode = encodeChar($('#productCodeDlg').val().trim());
		var productName = encodeChar($('#productNameDlg').val().trim());
		$('#productTreeContent').data('jsp').destroy();
		BaryCentricProgrammeCatalog.loadDataForTreeWithCheckboxEx('/rest/catalog/focus-program/product/list.json?focusProgramId=' + focusProgramId + '&productCode=' + encodeChar(productCode) + '&productName=' + encodeChar(productName) + '&nodeType=cat&catId=0&subCatId=0', 'productTree');
		setTimeout(function() {
			$('#productTreeContent').jScrollPane();
			$('#productTree').bind("loaded.jstree open_node.jstree close_node.jstree", function (event, data) {
				//Utils.applyCheckboxStateOfTree();
				if ($('#productTreeContent').data('jsp') != undefined || $('#productTreeContent').data('jsp') != null) {
					$('#productTreeContent').data('jsp').destroy();
					setTimeout(function() {
						$('#productTreeContent').jScrollPane();
					},500);	
				} else {
					setTimeout(function() {
						$('#productTreeContent').jScrollPane();
					},500);	
				}
				BaryCentricProgrammeCatalog.getCheckboxStateOfTreeEx('productTree',BaryCentricProgrammeCatalog._mapProduct, 'productTree li', 'product');
			});
			BaryCentricProgrammeCatalog.getCheckboxStateOfTreeEx('productTree',BaryCentricProgrammeCatalog._mapProduct, 'productTree li', 'product');
		}, 500);
		$('.fancybox-inner #productCodeDlg').focus();
	},
	selectListProduct: function() {
		$('.ErrorMsgStyle').hide();
		$('#successMsgProductDlg').html('').hide();
		$.fancybox.update();
		var arrId = new Array();
/*		$('.jstree-leaf').each(function() {
			if ($(this).hasClass('jstree-checked')) {
				arrId.push($(this).attr('id'));
			}			
		});
		$('li[classstyle=product]').each(function() {
		    if ($(this).hasClass('jstree-checked')) {
		    	arrId.push($(this).attr('id'));
		    }
		});*/
		$('#productTree li').each(function() {
			var _id = $(this).attr('id');
			var type = $(this).attr('contentitemid');
			if ($(this).hasClass('jstree-checked') && type=='product') {
				BaryCentricProgrammeCatalog._mapProduct.put(_id,_id);		
			} else {
				BaryCentricProgrammeCatalog._mapProduct.remove(_id);	
			}			
		});
		for (var i=0;i<BaryCentricProgrammeCatalog._mapProduct.size();++i) {
			var _obj = BaryCentricProgrammeCatalog._mapProduct.get(BaryCentricProgrammeCatalog._mapProduct.keyArray[i]);
			arrId.push(_obj);
		}
		var msg = '';
		if (arrId.length == 0) {
			msg = format(msgErr_required_choose_format, 'Sản phẩm');
		}	
		if (msg.length ==0) {
			msg = Utils.getMessageOfRequireCheck('saleManDlg', 'Hình thức bán hàng',true);
		}
		if (msg.length ==0) {
			msg = Utils.getMessageOfRequireCheck('typeDialog', 'Loại MHTT',true);
		}
		if (msg.length > 0) {
			$('#errMsgProductDlg').html(msg).show();
			$.fancybox.update();
			return false;
		}		
		var focusProgramId = $('#selId').val();
		var dataModel = new Object();		
		dataModel.lstProductId = arrId;		
		dataModel.id = focusProgramId;
		dataModel.channelTypeId = $('#saleManDlg').val().trim();
		dataModel.type = $('#typeDialog').val().trim();
		Utils.addOrSaveData(dataModel, '/catalog/bary-centric-programme/product/save', ProgrammeDisplayCatalog._xhrSave, 'errMsgProductDlg', function(data) {
			$('#successMsgProductDlg').html('Lưu dữ liệu thành công').show();
			$('#productCodeDlg').val('');
			$('#productNameDlg').val('');	
			$('#productCode').val('');
			$('#productName').val('');
			setSelectBoxValue('saleManDlg');
			setSelectBoxValue('typeDialog');
			setSelectBoxValue('type');
			Utils.resetCheckboxStateOfTree();
			BaryCentricProgrammeCatalog.searchProductOnTree();
			BaryCentricProgrammeCatalog.searchProduct();
			$.fancybox.update();
			//$("#productGrid").trigger("reloadGrid");
			setTimeout(function() {$.fancybox.close();}, 1000);
		}, null, '.fancybox-inner', null, null);
		return false;
	},	
	showUpdateProductDialog: function(rowId) {
		$('.ErrorMsgStyle').hide();
		var html = $('#productDetailTreeDialog').html();
		$.fancybox(html,
				{
					modal: true,
					autoResize: false,
					title: 'Cập nhật thông tin sản phẩm',					
					afterShow: function() {
						$('#productDetailTreeDialog').html('');
						$('#errMsgProductDetailDlg').html('').hide();
						$('#successMsgProductDetailDlg').html('').hide();
						var focusProgramId = $('#selId').val();
						$('#saleManDetailDlgTmp').attr('id', 'saleManDetailDlg');
						$('#saleManDetailDlg').addClass('MySelectBoxClass');
						$('#saleManDetailDlg').customStyle();		
						$('#typeDetailDialog').addClass('MySelectBoxClass');
						$('#typeDetailDialog').customStyle();
						$.getJSON('/rest/catalog/focus-program/' + focusProgramId + '/staff-type/list.json', function(data) {
							var arrHtml = new Array();
							arrHtml.push('<option value="-2">--- Chọn kiểu bán hàng ---</option>');
							for (var i=0;i<data.length;i++) {
								arrHtml.push('<option value="' + Utils.XSSEncode(data[i].value) + '">' + Utils.XSSEncode(data[i].name) + '</option>');
							}
							//Chu y cho nay: vi focusChannelMap.staffSale.id da bi bo di
//							var channelTypeId = $("#productGrid").jqGrid ('getCell', rowId, 'focusChannelMap.staffSale.id');
							$('#saleManDetailDlg').html(arrHtml.join(""));
							var saleStaffType = $("#productGrid").jqGrid ('getCell', rowId, 'focusChannelMap.saleTypeCode');
							$.getJSON('/rest/catalog/focus-program/apparam/' + saleStaffType + '/id.json', function(data) {
								$('#saleManDetailDlg').val(Utils.XSSEncode(data.value));
								$('#saleManDetailDlg').change();
							});
						});
						var name = $("#productGrid").jqGrid ('getCell', rowId, 'product.productName');						
						var productId = $("#productGrid").jqGrid ('getCell', rowId, 'product.id');
						$('#productNameDetailDlg').val(name);
						$('#productIdDetailDlg').val(productId);
						var type = $("#productGrid").jqGrid ('getCell', rowId, 'type');
						//Chu y cho nay vi: type da chuyen sang kieu String//
//						type = focusProductType.parseValue(type);
//						setSelectBoxValue('typeDetailDialog', type);
						$.getJSON('/rest/catalog/focus-program/apparam/' + type + '/id.json', function(data) {
							setSelectBoxValue('typeDetailDialog', data.value);
						});						
					},
					afterClose: function() {
						$('#productDetailTreeDialog').html(html);						
					}
				}
			);
		return false;
	},
	saveDetailProduct: function() {
		$('.ErrorMsgStyle').hide();
		$('#successMsgProductDetailDlg').html('').hide();
		$.fancybox.update();		
		var msg = '';			
		if (msg.length ==0) {
			msg = Utils.getMessageOfRequireCheck('saleManDetailDlg', 'Hình thức bán hàng',true);
		}
		if (msg.length ==0) {
			msg = Utils.getMessageOfRequireCheck('typeDetailDialog', 'Loại MHTT',true);
		}
		if (msg.length > 0) {
			$('#errMsgProductDetailDlg').html(msg).show();
			$.fancybox.update();
			return false;
		}	
		var arrId = new Array();
		arrId.push($('#productIdDetailDlg').val().trim());
		var focusProgramId = $('#selId').val();
		var dataModel = new Object();		
		dataModel.lstProductId = arrId;		
		dataModel.id = focusProgramId;
		dataModel.channelTypeId = $('#saleManDetailDlg').val().trim();
		dataModel.type = $('#typeDetailDialog').val().trim();
		Utils.addOrSaveData(dataModel, '/catalog/bary-centric-programme/product/save', ProgrammeDisplayCatalog._xhrSave, 'errMsgProductDetailDlg', function(data) {
			$('#successMsgProductDetailDlg').html('Lưu dữ liệu thành công').show();						
			$.fancybox.update();
			$("#productGrid").trigger("reloadGrid");
			setTimeout(function() {$.fancybox.close();}, 1000);
		}, null, '.fancybox-inner', null, null);
		return false;
	},
	loadShopTreeOnDialog: function(url) {
		$('.ErrorMsgStyle').hide();
		$.getJSON(url, function(data) {
			$('#shopTree').bind("loaded.jstree open_node.jstree close_node.jstree", function (event, data) {				
				if ($('#shopTreeContent').data('jsp') != undefined || $('#shopTreeContent').data('jsp') != null) {
					$('#shopTreeContent').data('jsp').destroy();
					setTimeout(function() {
						$('#shopTreeContent').jScrollPane();
					},500);	
				} else {
					setTimeout(function() {
						$('#shopTreeContent').jScrollPane();
					},500);	
				}
			}).jstree({
		        "plugins": ["themes", "json_data","ui","checkbox"],
		        "themes": {
		            "theme": "classic",
		            "icons": false,
		            "dots": true
		        },
		        "json_data": {
		        	"data": data,
		        	"ajax" : {
		        		"method": "GET",
		                "url" : '/rest/catalog/sub-shop/list.json?focusProgramId=' +$('#selId').val(),
		                "data" : function (n) {
		                        return { id : n.attr ? n.attr("id") : 0 };
		                    }
		            }
		        }
			});
		});
	},
	loadDataForTreeWithCheckbox: function(url,treeId) {
		var tId = 'tree';
		if (treeId!= null && treeId!= undefined) {
			tId = treeId;
		}
		$('#' + tId).jstree({
	        "plugins": ["themes", "json_data","ui","checkbox"],
	        "themes": {
	            "theme": "classic",
	            "icons": false,
	            "dots": true
	        },
	        "json_data": {
	        	"ajax" : {
	        		"method": "post",
	                "url" : url,
	                "data" : function (n) {
	                        return { id : n.attr ? n.attr("id") : 0 };
	                    }
	            }
	        }
		});	
	},
	loadDataForTreeWithCheckboxEx: function(url,treeId) {
		var tId = 'tree';
		if (treeId!= null && treeId!= undefined) {
			tId = treeId;
		}
		$('#' + tId).jstree({
	        "plugins": ["themes", "json_data","ui","checkbox"],
	        "themes": {
	            "theme": "classic",
	            "icons": false,
	            "dots": true
	        },
	        "json_data": {
	        	"ajax" : {
	        		"method": "get",
	                "url" : function( node ) {	                	
	                      if (node == -1) {
	                    	  return url;
	                      } else {
	                    	  var nodeType = node.attr("classStyle");
	                    	  var catId = 0;
	                    	  var subCatId = 0;
	                    	  if (nodeType == 'cat') {
	                    		  catId = node.attr("id");
	                    		  var focusProgramId = $('#selId').val();
	                    		  var productCode = encodeURI('');
	                    		  var productName = encodeURI('');	
	                    		  return '/rest/catalog/focus-program/product/list.json?focusProgramId=' + focusProgramId + '&productCode=' + encodeChar(productCode) + '&productName=' + encodeChar(productName) + '&nodeType=sub-cat&catId=' +catId+ '&subCatId=0';
	                    	  } else if (nodeType == 'sub-cat') {
	                    		  subCatId = node.attr("id");
	                    		  catId = $('#' +subCatId).parent().parent().attr('id');
	                    		  var focusProgramId = $('#selId').val();
	                    		  var productCode = encodeURI('');
	                    		  var productName = encodeURI('');
	                    		  return '/rest/catalog/focus-program/product/list.json?focusProgramId=' + focusProgramId + '&productCode=' + encodeChar(productCode) + '&productName=' + encodeChar(productName) + '&nodeType=product&catId=' +catId+ '&subCatId=' + subCatId;
	                    	  } else if (nodeType == 'product') {
	                    		  return '';
	                    	  }
	                      }
	                    },
	                "data" : function (n) {
	                        return { id : n.attr ? n.attr("id") : 0 };
	                    }
	            }
	        }
		});	
	},
	exportExcelData: function() {
		$('#divOverlay').show();
		$('.ErrorMsgStyle').hide();
		var data = new Object();
		var code = '';
		var name = '';
		if ($('#excelType').val() == 2) {//dvtg
			code = $('#shopCode').val().trim();
			name = $('#shopName').val().trim();
			data.shopCode = code;
			data.shopName = name;
		} else if ($('#excelType').val() == 3) {//sp cttt
			code = $('#productCode').val().trim();
			name = $('#productName').val().trim();
			data.code = code;
			data.name = name;
		}
		data.id = $('#selId').val();
		data.excelType = $('#excelType').val();
		var url = "/catalog/bary-centric-programme/getlistexceldata";
		ReportUtils.exportReport(url, data);
	},
	exportActionLog: function() {
		var id = $('#selId').val();
		var fromDate = $('#fromDate').val().trim();
		var toDate = $('#toDate').val().trim();
		ExportActionLog.exportActionLog(id, ExportActionLog.FOCUS_PROGRAM, fromDate, toDate, 'errMsgActionLog');
	},
	openCopyFocusProgram: function() {
		$('.ErrorMsgStyle').hide();
		var codeText = 'Mã CTTT <span class="RequireStyle" style="color:red">*</span>';
		var nameText = 'Tên CTTT <span class="RequireStyle" style="color:red">*</span>';
		var title = "Sao chép CTTT";
		var codeFieldText = "focusProgramCode";
		var nameFieldText = "focusProgramName";
		var searchDiv = "searchStyle1EasyUIDialogDivEx";
		var arrParam = null;

		CommonSearchEasyUI._arrParams = null;
		CommonSearchEasyUI._currentSearchCallback = null;
		$('#searchStyle1EasyUIDialogDivEx').css("visibility", "visible");
		var html = $('#searchStyle1EasyUIDialogDivEx').html();
		$('#searchStyle1EasyUIDialogEx').dialog({  
	        title: title,  
	        closed: false,  
	        cache: false,  
	        modal: true,
	        width : 616,
	        height : 'auto',
	        onOpen: function() {
	        	//@CuongND : my try Code <reset tabindex>
	        	$('.easyui-dialog #__btnSaveIncentiveProgram').unbind('click');
	        	var tabindex = -1;
				$('.InputTextStyle, select, input, button , li , tr, td, th, label, ul, a, img').each(function () {
					if (this.type != 'hidden') {
						$(this).attr("tabindex", tabindex);
						tabindex -=1;
					}
				});
				tabindex = 1;
	    		 $('.easyui-dialog input,.easyui-dialog select,.easyui-dialog button').each(function () {
		    		 if (this.type != 'hidden') {
			    	     $(this).attr("tabindex", '');
						 tabindex++;
		    		 }
				 });
				//end <reset tabindex>
				$('.easyui-dialog #seachStyle1Code').focus();
				$('.easyui-dialog #errMsgSearch').html("").hide();
				$('.easyui-dialog #successMsgInfo').html("").hide();
				$('.easyui-dialog #searchStyle1CodeText').val(codeFieldText);
				$('.easyui-dialog #searchStyle1NameText').val(nameFieldText);
				
				var codeField = 'code';
				var nameField = 'name';
				if (codeFieldText != null && codeFieldText != undefined && codeFieldText.length > 0) {
					codeField = codeFieldText;
				}
				if (nameFieldText != null&& nameFieldText != undefined && nameFieldText.length > 0) {
					nameField = nameFieldText;
				}
				
				$('.easyui-dialog #seachStyle1CodeLabel').html(codeText);
				$('.easyui-dialog #seachStyle1NameLabel').html(nameText);
				
				$('.easyui-dialog #__btnSaveFocusProgram').unbind('click');
				$('.easyui-dialog #__btnSaveFocusProgram').bind('click',function(event) {
					var code = $('.easyui-dialog #seachStyle1Code').val().trim();
					var name = $('.easyui-dialog #seachStyle1Name').val().trim();
					if (code == "" || code == undefined || code == null) {
						$('.easyui-dialog #errMsgSearch').html("Bạn chưa nhập giá trị trường Mã CTTT.").show();
						$('.easyui-dialog #seachStyle1Code').focus();
						return false;
					}
					var msg = Utils.getMessageOfSpecialCharactersValidateEx1('.easyui-dialog #seachStyle1Code', 'Mã CTTT',Utils._CODE);
					if (msg.length >0) {
						$('.easyui-dialog #errMsgSearch').html(msg).show();
						$('.easyui-dialog #seachStyle1Code').focus();
						return false;
					}
					if (name == "" || name == undefined || name == null) {
						$('.easyui-dialog #errMsgSearch').html("Bạn chưa nhập giá trị trường Tên CTTT.").show();
						$('.easyui-dialog #seachStyle1Name').focus();
						return false;
					}
					msg = Utils.getMessageOfSpecialCharactersValidateEx1('.easyui-dialog #seachStyle1Name', 'Tên CTTT');
					if (msg.length >0) {
						$('.easyui-dialog #errMsgSearch').html(msg).show();
						$('.easyui-dialog #seachStyle1Name').focus();
						return false;
					}
					var focusId = $('#progId').val();
					var params = new Object();
					params.id = focusId;
					params.code = code;
					params.name = name;
					Utils.addOrSaveData(params, '/catalog/bary-centric-programme/copyfocusprogram', null, 'errMsgSearch', function(data) {
						//PromotionCatalog.loadShop();
						$('.easyui-dialog #successMsgInfo').html("Sao chép dữ liệu thành công.").show();
						$('#searchStyle1EasyUIDialogEx').dialog('close');
						//$('#promotionId').val(data.promotionId);
						window.location.href = '/catalog/bary-centric-programme/change?id=' +encodeChar(data.focusId);
					}, null, null, null, "Bạn có muốn sao chép CTTT này ?", null);
				});
	        },	       
	        onClose : function() {
	        	var tabindex = 1;
				$('.InputTextStyle, select, input, button , li , tr, td, th, label, ul, a, img').each(function () {
					if (this.type != 'hidden') {
						$(this).attr("tabindex", '');
					}
					tabindex ++;
				});	
	        	$('#searchStyle1EasyUIDialogDivEx').html(html);
	        	$('#searchStyle1EasyUIDialogDivEx').css("visibility", "hidden");
	        	$('.easyui-dialog #seachStyle1Code').val('');
	        	$('.easyui-dialog #seachStyle1Name').val('');
	        	$('.easyui-dialog #__btnSaveFocusProgram').unbind('click');
	        	$('.easyui-dialog #__btnSave').unbind('click');
	        }
	    });
		return false;
	},
//	SAN PHAM CHUONG TRINH TRONG TAM[[[[
	getProductGridUrl: function(code,name) {
		var selId = $('#selId').val();
		return '/catalog/bary-centric-programme/search-product?id=' + encodeChar(selId) + '&code=' + encodeChar(code) + '&name=' + encodeChar(name) /*+ '&type=' + encodeChar(type)*/;
	},
	showTab2: function() {
		var idx = -1;
		$('#tab1').removeClass('Active');
		$('#tab2').removeClass('Active');
		$('#tab3').removeClass('Active');
		$('#spCTTTTab').hide();
		$('#dvtgCTTTTab').hide();
		$('#htbhCTTTTab').hide();
		$('#tab2').addClass('Active');
		$('#spCTTTTab').show();
		$('#divImportGrroupSpDv').html(BaryCentricProgrammeCatalog._importHtml).change();
		$('.IdImportDivTpm').each(function() {
			$(this).prop('id', 'tab_sp_group_edit_import').addClass('cmsiscontrol');
		});
		Utils.functionAccessFillControl('divImportGrroupSpDv');
		$('.IdImportDivTpm').show();
		$('#productCode').focus();
		Utils.bindAutoButtonEx('.ContentSection', 'btnSearchProduct');
		if ($('#downloadTemplate').length > 0) {
			$('#downloadTemplate').attr('href',excel_template_path + 'catalog/Bieu_mau_danh_muc_cttt_HTBH_TTSP.xls');
		}
		$('#excelType').val(3);
		$('.ErrorMsgStyle').hide();
		var title = '';
		if ($('#statusHidden').val().trim() == activeType.WAITING) {
			title = '<a id="btnAddGridP" title="Thêm mới" href="javascript:void(0);" onclick="BaryCentricProgrammeCatalog.openEasyUIDialog();"><img src="/resources/images/icon-add.png"></a>';
		}
		$('.ErrorMsgStyle').hide();
		$('#productCode').val('').change();
		$('#productName').val('').change();
		$("#grid").datagrid({
			  url:BaryCentricProgrammeCatalog.getProductGridUrl('', ''),
			  columns:[[		
			    {field: 'categoryCode', title: 'Mã ngành hàng', width: 80, sortable: false, resizable: false, align: 'left',
			    	formatter: function(index, row) {
				    	if (row.product != null && row.product != undefined && row.product.cat != null&& row.product.cat != undefined) {
				    		return Utils.XSSEncode(row.product.cat.productInfoCode);
				    	}
			    	}
			    },
			    {field: 'productCode', title: 'Mã SP', width: 100, sortable: false, resizable: false , align: 'left',
			    	formatter: function(index, row) {
				    	if (row.product!=null) {
				    		return Utils.XSSEncode(row.product.productCode);
				    	}
				    }
			    },
			    {field: 'productName', title: 'Tên SP', width: 100, align: 'left', sortable: false, resizable: false ,  
		            formatter: function(index, row) {
		            	if (row.product!=null) {
				    		return Utils.XSSEncode(row.product.productName);
				    	}
		            }
			    },
			    {field: 'createUser', title: 'Mã HTBH', width:80, align: 'left', sortable: false, resizable: false ,  
	                editor:{  
	                    type: 'combobox',
	                    options:{
	                    	data: BaryCentricProgrammeCatalog._lstSaleTypeData.get(0).rows,
	            	        valueField: 'saleTypeCode',  
	            	        textField: 'saleTypeCode',
	            	        width :40,
	                        filter: function(q, row) {
	                    		var opts = $(this).combobox('options');
	                    		return row[opts.textField].indexOf(q.toUpperCase()) == 0;
	                    	}
	                    }
	                }
			    },
			    {field: 'type', title: 'Loại MHTT', width: 150, align: 'left',fixed: true, sortable: false, resizable: false ,  
                    editor:{  
                        type: 'combobox',
                        options:{
                        	data: BaryCentricProgrammeCatalog._lstTypeData.get(0).rows,  
	            	        valueField: 'apParamCode',  
	            	        textField: 'apParamCode',
//	            	        method: 'GET',
//	            	        class: 'easyui-combobox',
	            	        width:150,
	            	        panelWidth:150,
	            	        editable: false,
//	                        panelHeight :140, 
	                        filter: function(q, row) {
	                    		var opts = $(this).combobox('options');
	                    		return row[opts.textField].indexOf(q.toUpperCase()) == 0;
	                    	}
	                    }
                    }
			    },
			    {field: 'edit', title: '', hidden: true, width: 50, align: 'center', sortable: false, resizable: false, formatter: function(value, row, index) {
			    	if (row.editing) {
		                var s = "<a title='Lưu' href='javascript:void(0)' onclick='return BaryCentricProgrammeCatalog.saveRow(this)'><span style='cursor:pointer'><img src='/resources/images/icon-datach.png'/></span></a> ";  
		                var c = "<a title='Quay lại' href='javascript:void(0)' onclick='BaryCentricProgrammeCatalog.cancelRow(this)'><span style='cursor:pointer'><img src='/resources/images/icon_esc.png'/></span></a>";  
		                return s + c;  
		            } else {
		            	var e = '';
		            	if ($('#statusHidden').val().trim() == activeType.WAITING) {
		            		e = "<a id='btnEditGridP'"+index+" title='Sửa' href='javascript:void(0)' onclick='BaryCentricProgrammeCatalog.editRow(this);'><span style='cursor:pointer'><img src='/resources/images/icon-edit.png'/></span></a>";
		            	}
		                return e;
		            }
			    }},
			    {field: 'delete', hidden: true, title: title, width: 50, align: 'center', sortable: false, resizable: false,
			    	formatter: function(index, row) {
			    		if ($('#statusHidden').val().trim() == activeType.WAITING) {
			    			return "<a id='btnDelGridP'"+index+" title='Xóa' href='javascript:void(0)' onclick=\"BaryCentricProgrammeCatalog.deleteRow('"+ row.id +"')\"><img src='/resources/images/icon-delete.png'/></a>";
			    		}
			    		return '';
			    		
			    	} 
			    },
			  ]],	  
			  pageList  : [10,20,30],
			  height: 'auto',
			  scrollbarSize : 0,
			  pagination: true,
			  checkOnSelect : true,
			  fitColumns: true,
			  method : 'GET',
			  rownumbers: true,	  
			  singleSelect: true,
			  pageNumber:1,
			  width: ($('#productFocusGrid').width()),
			  onLoadSuccess: function() {
					setTimeout(function() {
					  $('#successMsgProduct').html('').hide();
					}, 1500);
					var angelkid;
					$('.datagrid-header-rownumber').html('STT');
					$('#grid').datagrid('resize');
					edit = false;
					  
					var arrEdit =  $('#productFocusGrid td[field="edit"]');
					if (arrEdit != undefined && arrEdit != null && arrEdit.length > 0) {
						for (var i = 0, size = arrEdit.length; i < size; i++) {
							$(arrEdit[i]).prop("id", "tab_sp_group_edit_" + i).addClass("cmsiscontrol");//Khai bao id danh cho phan quyen
						}
					}
					var arrDelete =  $('#productFocusGrid td[field="delete"]');
					if (arrDelete != undefined && arrDelete != null && arrDelete.length > 0) {
						for (var i = 0, size = arrDelete.length; i < size; i++) {
							$(arrDelete[i]).prop("id", "tab_sp_group_edit_" + i).addClass("cmsiscontrol");//Khai bao id danh cho phan quyen
						}
					}
					Utils.functionAccessFillControl('productFocusGrid', function(data) {
					//Xu ly cac su kien lien quan den cac control phan quyen
					var arrTmpLength =  $('#productFocusGrid td[id^="tab_sp_group_edit_"]').length;
					var invisibleLenght = $('.isCMSInvisible[id^="tab_sp_group_edit_"]').length;
					if (arrTmpLength > 0 && arrTmpLength != invisibleLenght) {
						$('#grid').datagrid("showColumn", "delete");
					} else {
						$('#grid').datagrid("hideColumn", "delete");
					}
					arrTmpLength =  $('#productFocusGrid td[id^="tab_sp_group_edit_"]').length;
					invisibleLenght = $('.isCMSInvisible[id^="tab_sp_group_edit_"]').length;
					if (arrTmpLength > 0 && arrTmpLength != invisibleLenght) {
						$('#grid').datagrid("showColumn", "edit");
					} else {
						$('#grid').datagrid("hideColumn", "edit");
						}
					});
		      },
		  	onBeforeEdit: function(index, row) {
		  		//$()
		        row.editing = true;
		        edit =true;
		        idx = index;  
		        BaryCentricProgrammeCatalog.updateActions(index);  
		    },  
		    onAfterEdit: function(index, row) {  
		        row.editing = false;
		        edit = false;
		        idx = -1;
		        $('#saleTypeCode').val(Utils.XSSEncode(row.createUser));
		        $('#type').val(Utils.XSSEncode(row.type));
		        BaryCentricProgrammeCatalog.updateActions(index);  
		    },  
		    onCancelEdit: function(index, row) {  
		        row.editing = false;  
		        edit = false;
		        idx = -1;
		        BaryCentricProgrammeCatalog.updateActions(index);  
		    }  
		});
	},
	getJsonListSaleType: function() {
		$.getJSON('/rest/catalog/focus-program/product/sale-type-code.json?id=' + $('#focusProgramId').val(),function(data) {
			if (data != null && data != undefined) {
				BaryCentricProgrammeCatalog._lstSaleTypeData.put(0, data);
			}
		});
	},
	getJsonListType: function() {
		$.getJSON('/rest/catalog/focus-program/product/type.json',function(data) {
			if (data != null && data != undefined) {
				BaryCentricProgrammeCatalog._lstTypeData.put(0, data);
			}
		});
	},
	editRow: function(target) {
		var indexRow = BaryCentricProgrammeCatalog.getRowIndex(target);
	    $('#grid').datagrid('beginEdit', indexRow);
	    var width = 75;
	    $('td[field=saleTypeCode] .datagrid-editable-input').combobox('resize', width);
	    $('td[field=type] .datagrid-editable-input').combobox('resize', width);
	},
	updateActions: function(index) {  
	    $('#grid').datagrid('updateRow',{  
	        index: index,  
	        row:{}  
	    });  
	}, 
	getRowIndex: function(target) {  
	    var tr = $(target).closest('tr.datagrid-row');  
	    return parseInt(tr.attr('datagrid-row-index'));  
	},  
	saveRow: function(target) {
		var indexRow = BaryCentricProgrammeCatalog.getRowIndex(target);
		$('#errMsg').html('').hide();
	    $('#grid').datagrid('endEdit', indexRow);
	    var saleTypeCode = $('#grid').datagrid('getData').rows[indexRow].createUser;;
	    var focusProductType = $('#grid').datagrid('getData').rows[indexRow].type;
	    var msg = '';
	    $('#errMsgProduct').html('').hide();
	    if (saleTypeCode == null || saleTypeCode.length == 0 ) {
	    	msg = 'Vui lòng chọn mã HTBH';
	    	$('#grid').datagrid('beginEdit', indexRow);
	    	$('td[field=createUser] .combo-text').focus();
	    }
	    if (msg.length == 0) {
	    	if (focusProductType == null || focusProductType.length == 0 ) {
		    	msg = 'Vui lòng chọn loại MHTT';
		    	 $('#grid').datagrid('beginEdit', indexRow);
		    	$('td[field=type] .combo-text').focus();
		    }
	    }
	    if (msg.length > 0) {
	    	$('#errMsgProduct').html(msg).show();
	    	return false;
	    }
	    var dataModel = new Object();		
		dataModel.id = $('#selId').val();
		dataModel.focusMapProductId = $('#grid').datagrid('getData').rows[indexRow].id;
		dataModel.staffTypeCode = saleTypeCode;
		dataModel.focusProductType = focusProductType;
		dataModel.isUpdate = true;
		Utils.addOrSaveRowOnGrid(dataModel, '/catalog/bary-centric-programme/product/save', ProgrammeDisplayCatalog._xhrSave, null, null, function(data) {
			$('#successMsgProduct').html('Lưu dữ liệu thành công').show();
			$("#grid").datagrid("reload");
		});
		return false;
	},
	cancelRow: function (target) {  
		var indexRow = BaryCentricProgrammeCatalog.getRowIndex(target);
	    $('#grid').datagrid('cancelEdit', indexRow);  
	},
	deleteRow: function(productId) {
		$('#errMsg').html('').hide();
		var dataModel = new Object();
		dataModel.productId = productId;	
		Utils.deleteSelectRowOnGrid(dataModel, 'SP của CTTT', "/catalog/bary-centric-programme/delete-product", BaryCentricProgrammeCatalog._xhrDel, null, 'errMsgProduct',function() {
			$('#successMsgProduct').html('Xóa dữ liệu thành công').show();
		});
		return false;		
	},
	searchProductTab: function() {
		$('.ErrorMsgStyle').hide();
		var code = $('#productCode').val().trim();
		var name = $('#productName').val().trim();
		$('#code').focus();
		$("#grid").datagrid({
			url:BaryCentricProgrammeCatalog.getProductGridUrl(code,name),
			pageNumber:1
		});
		return false;
	},
	openEasyUIDialog : function() {
		$('.ErrorMsgStyle').hide();
		$('#divDialogSearch').show();
		var html = $('#productEasyUIDialog').html();
		$('#productEasyUIDialog').dialog({  
	        closed: false,  
	        cache: false,  
	        modal: true,
	        position: 'middle',
	        center: true, 
	        onOpen: function() {
	        	BaryCentricProgrammeCatalog._mapLstProduct = new Map();
	        	setTimeout(function() {
	    			CommonSearchEasyUI.fitEasyDialog();
	    		},500);
	        	Utils.bindAutoSearch();
	        	$('#productCodeDlg').focus();
	        	$('#saleTypeCodeDlg').addClass('MySelectBoxClass');
	        	$('#saleTypeCodeDlg').customStyle();
	        	$('#typeDlg').addClass('MySelectBoxClass');
	        	$('#typeDlg').customStyle();
	        	BaryCentricProgrammeCatalog.searchProductTree();
	        },
	        onBeforeClose: function() {
	        },
	        onClose : function() {
	        	$('#divDialogSearch').hide();
	        	$('#productEasyUIDialog').html(html);
	        }
	    });
		return false;
	},
	searchProductTree: function() {
		$('.ErrorMsgStyle').hide();
		$('#loadingDialog').show();
		var code = $("#productCodeDlg").val().trim();
		var name = 	$("#productNameDlg").val().trim();
		var focusProgramId = $('#selId').val();
		var url = '/rest/catalog/group-po/tree/2.json' ;
		$("#tree").tree({
			url: url,  
			method: 'GET',
            animate: true,
            checkbox: true,
            onBeforeLoad: function(node,param) {
            	$("#tree").css({"visibility":"hidden"});
            	$("#loadding").css({"visibility":"visible"});
            	$("#errMsgDlg").html('');
            	param.code = code;
            	param.name = name;
            	param.id = focusProgramId;
            },
            onCheck: function(node,data) {
            	if (node.attributes.productTreeVO.type == "PRODUCT") {
            		if (data) {
            			BaryCentricProgrammeCatalog._mapLstProduct.put(node.attributes.productTreeVO.id,node.attributes.productTreeVO.id);
            		} else {
            			BaryCentricProgrammeCatalog._mapLstProduct.remove(node.attributes.productTreeVO.id);
            		}
            	}
            	if (node.attributes.productTreeVO.type == "SUB_CATEGORY") {
            		var sub_category = node.attributes.productTreeVO.listChildren;
            		for (var i = 0;i<sub_category.length;i++) {      			
		            		if (data) {
		            			BaryCentricProgrammeCatalog._mapLstProduct.put(sub_category[i].id,sub_category[i].id);
		            		} else {
		            			BaryCentricProgrammeCatalog._mapLstProduct.remove(sub_category[i].id);
		            		}
            		}
            	}
            	if (node.attributes.productTreeVO.type == "CATEGORY") {
            		var category = node.attributes.productTreeVO.listChildren;
            		for (var i = 0;i<category.length;i++) {
	        			var sub_category = category[i].listChildren;
	        			for (var j = 0;i<sub_category.length;i++) {
		            		if (data) {
		            			BaryCentricProgrammeCatalog._mapLstProduct.put(sub_category[i].id,sub_category[i].id);
		            		} else {
		            			BaryCentricProgrammeCatalog._mapLstProduct.remove(sub_category[i].id);
		            		}
	        			}
            		}
            	}
            },
            onLoadSuccess: function(node,data) {
            	$("#tree").css({"visibility":"visible"});
            	var arr  = BaryCentricProgrammeCatalog._mapLstProduct.keyArray;
            	if (arr.length > 0) {
            		for (var i = 0;i < arr.length; i++) {
            			var nodes = $('#tree').tree('find', arr[i]);
            			if (nodes != null) {
            				$("#tree").tree('check',nodes.target);
            			}
            		}
            	}
            	$("#loadding").css({"visibility":"hidden"});
            	$('#loadingDialog').hide();
            	if (data.length == 0) {
            		$("#errMsgDlg").html("Không có kết quả");
            	}
            	setTimeout(function() {
            		CommonSearchEasyUI.fitEasyDialog("productEasyUIDialog");
            	}, 500);
            }
		});
	},
	changeProductTree: function() {
		$('.ErrorMsgStyle').hide();
		var dataModel = new Object();
		var nodes = $('#tree').tree('getChecked');
		var lstId = [];  
		var arr = $('#tree').children();
		var moreCat = 0;
		arr.each(function() {
			if (!$(this).children().first().children().last().prev().hasClass('tree-checkbox0')) {
				moreCat++;
			}
		});
        for (var i=0; i<nodes.length; i++) {
        	if (nodes[i].attributes.productTreeVO.type != "PRODUCT") {
        		continue;
        	}
            lstId.push(nodes[i].id.split('_')[0]);  
        }
        if (BaryCentricProgrammeCatalog._mapLstProduct.keyArray.length > 0) {
        	for (var j = 0; j < BaryCentricProgrammeCatalog._mapLstProduct.keyArray.length ; j++) {
        		if (lstId.indexOf(BaryCentricProgrammeCatalog._mapLstProduct.keyArray[j]) > -1) {
        			continue;
        		} else {
        			lstId.push(BaryCentricProgrammeCatalog._mapLstProduct.keyArray[j]);
        		}
        	}
        }
        if (lstId == '' || lstId.length == 0) {
        	$("#errMsgDlg").html("Vui lòng chọn sản phẩm").show();
        	return false;
        }
        var saleTypeCode = $('#saleTypeCodeDlg').val().trim();
        var type = $('#typeDlg').val().trim();
        if (saleTypeCode == -1) {
        	$("#errMsgDlg").html("Vui lòng chọn loại HTBH").show();
        	return false;
        }
        if (type == -1) {
        	$("#errMsgDlg").html("Vui lòng chọn loại MHTT").show();
        	return false;
        }
        dataModel.id = $('#selId').val().trim();
        dataModel.lstProductId = lstId;
		dataModel.channelTypeId = saleTypeCode;
		dataModel.type = type;
		if (moreCat > 1) {
			$.messager.confirm('Xác nhận', 'Bạn đang chọn các sản phẩm hơn 1 ngành hàng. Bạn có muốn lưu thông tin không?', function(r) {
				if (r) {
					Utils.saveData(dataModel, '/catalog/bary-centric-programme/product/save', BaryCentricProgrammeCatalog._xhrSave, 'errMsgDlg', function(data) {
						$('#successMsgProduct').html('Lưu dữ liệu thành công').show();
						$("#productEasyUIDialog").dialog("close");
						if (data.error == false) {
							$("#grid").datagrid("reload");
						}
					});		
				}
			});
		} else {
			Utils.addOrSaveData(dataModel, '/catalog/bary-centric-programme/product/save', BaryCentricProgrammeCatalog._xhrSave, 'errMsgDlg', function(data) {
				$('#successMsgProduct').html('Lưu dữ liệu thành công').show();
				$("#productEasyUIDialog").dialog("close");
				if (data.error == false) {
					$("#grid").datagrid("reload");
				}
			});
		}
		return false;
	},
	getShopGridUrl: function(focusId,shopCode,shopName) {
		return "/catalog/bary-centric-programme/searchshop?id="+ encodeChar(focusId) + "&shopCode=" + encodeChar(shopCode) + "&shopName=" + encodeChar(shopName);
	},
	openShopDialog: function(id) {
		$('.ErrorMsgStyle').hide();
		var focusId = $('#progId').val();
		var arrParam = new Array();
 		var param = new Object();
 		param.name = 'shopId';
 		if (id != null && id != undefined) {
 	 		param.value = id;
 		}
 		var param1 = new Object();
 		param1.name = 'focusId';
 		param1.value = focusId;
 		arrParam.push(param);
 		arrParam.push(param1);
 		var param2 = new Object();
 		param2.name = 'stringProgram';
 		param2.value = 'focus';
 		arrParam.push(param2);
 		BaryCentricProgrammeCatalog.searchShopFocusProgram(function(data) {
 				//$('#promotionProgramCode').val(data.code);
 		}, arrParam);
 		$('#searchStyleShopContainerGrid input[type=text]').each(function() {
			Utils.bindFormatOnTextfieldEx1($(this).attr('id'),Utils._TF_NUMBER, null, '#errMsgDialog', 'Số suất phải là số nguyên dương.');
		}); 
	},
	searchShopFocusProgram: function(callback,arrParam) {
		var codeText = "Mã đơn vị";
		var nameText = "Tên đơn vị";
		var title = "Chọn đơn vị";
		var url = "/catalog/bary-centric-programme/searchShopDialog";
		var codeFieldText = "promotionProgramCode";
		var nameFieldText = "promotionProgramName";
		var searchDiv = "searchStyleShopFocus";
		var searchDialog = "searchStyleShopFocus1";
		var program = "focus";

		CommonSearchEasyUI._arrParams = null;
		CommonSearchEasyUI._currentSearchCallback = null;
		var html = $('#' +searchDiv).html();
		$('#' +searchDialog).dialog({  
	        title: title,  
	        closed: false,  
	        cache: false,  
	        modal: true,
	        width : 616,
	        height : 'auto',
	        onOpen: function() {
				$('.easyui-dialog #seachStyleShopCode').focus();
				$('.easyui-dialog #searchStyleShopUrl').val(url);
				$('.easyui-dialog #searchStyleShopCodeText').val(codeFieldText);
				$('.easyui-dialog #searchStyleShopNameText').val(nameFieldText);
				CommonSearch._currentSearchCallback = callback;
				CommonSearch._arrParams = arrParam;
				
				var codeField = 'code';
				var nameField = 'name';
				if (codeFieldText != null && codeFieldText != undefined && codeFieldText.length > 0) {
					codeField = codeFieldText;
				}
				if (nameFieldText != null&& nameFieldText != undefined && nameFieldText.length > 0) {
					nameField = nameFieldText;
				}
				
				$('.easyui-dialog #seachStyleShopCodeLabel').html(codeText);
				$('.easyui-dialog #seachStyleShopNameLabel').html(nameText);
				
				$('.easyui-dialog #searchStyleShopGrid').show();
				$('.easyui-dialog #searchStyleShopGrid').treegrid({  
				    url:  CommonSearch.getSearchStyleShopGridUrl(url, '', '', arrParam),
				    width:($('#searchStyleShopContainerGrid').width()-20),  
			        height:400,  
			        idField: 'id',  
			        treeField: 'data',
				    columns:[[  
				        {field: 'data', title: 'Đơn vị', resizable: false, width:516, formatter: function(value, row, index) {
				        	  return Utils.XSSEncode(value);
				          }},
				        {field: 'edit', title: '', width:50, align: 'center', resizable: false, formatter: function(value, row, index) {
				        	if (row.isJoin >= 1) {
				        		return '<input type ="checkbox" disabled="disabled">';
				        	}
				        	var classParent = "";
				        	if (row.parentId != null && row.parentId != undefined) {
				        		classParent = "class_"+row.parentId;
				        	}
				        	return '<input type ="checkbox" class="' +classParent+ '" value="' +row.id+ '" id="check_' +row.id+ '" onclick="return BaryCentricProgrammeCatalog.onCheckShop('+row.id+ ');">';
				        }}
				    ]],
				    onLoadSuccess: function(row,data) {
				    	$(window).resize();
				    	if (data.rows != null && data.rows != undefined) {
					    	var length = parseInt(data.solg);
					    	$('#solg').val(length);
					    	var s = $('#width').val();
					    	if (s == null || s == "" || s == undefined) {
					    		s = $('.easyui-dialog .datagrid-header-row td[field=data] div').width();
					    		$('#width').val(s);
					    	}
					    	if (length > 15) {
					    		$('#searchStyleShopContainerGrid .datagrid-header-row td[field=data] div').width(s-17);
				    			$('#searchStyleShopContainerGrid .datagrid-row td[field=data] div').width(s-17);
					    	}
					    	for (var ij = 0, szj = BaryCentricProgrammeCatalog._listShopId.length; ij < szj; ij++) {
					    		$("#check_"+BaryCentricProgrammeCatalog._listShopId[ij]).attr("checked", "checked");
					    	}
				    	}
				    	setTimeout(function() {
			    			CommonSearchEasyUI.fitEasyDialog(searchDialog);
			    		},1000);
				    },
				    onExpand: function(row) {
				    	var value = parseInt($('#solg').val());
				    	var width = parseInt($('#width').val());
				    	var countNode = parseInt(row.countNode);
				    	value = value + countNode;
				    	$('#solg').val(value);
				    	if (value > 15) {
				    		var s = width;
				    		$('#searchStyleShopContainerGrid .datagrid-header-row td[field=data] div').width(s-17);
			    			$('#searchStyleShopContainerGrid .datagrid-row td[field=data] div').width(s-17);
				    	}
				    	for (var ij = 0, szj = BaryCentricProgrammeCatalog._listShopId.length; ij < szj; ij++) {
				    		$("#check_"+BaryCentricProgrammeCatalog._listShopId[ij]).attr("checked", "checked");
				    	}
				    },
				    onCollapse: function(row) {
				    	var value = parseInt($('#solg').val());
				    	var width = parseInt($('#width').val());
				    	var countNode = parseInt(row.countNode);
				    	console.log(countNode);
				    	value = value - countNode;
				    	$('#solg').val(value);
				    	if (value < 15) {
				    		var s = width;
				    		$('#searchStyleShopContainerGrid .datagrid-header-row td[field=data] div').width(s);
			    			$('#searchStyleShopContainerGrid .datagrid-row td[field=data] div').width(s);
				    	}
				    }
				});
				
				$('.easyui-dialog #btnSearchStyleShop').unbind('click');
				$('.easyui-dialog #btnSearchStyleShop').bind('click',function(event) {
					BaryCentricProgrammeCatalog.searchShopDlg();
				});
				$('.easyui-dialog #seachStyleShopCode').unbind('keyup');
				$('.easyui-dialog #seachStyleShopCode').bind('keyup',function(event) {
					if (event.keyCode == keyCodes.ENTER) {
						BaryCentricProgrammeCatalog.searchShopDlg();
					}
				});
				$('.easyui-dialog #seachStyleShopName').unbind('keyup');
				$('.easyui-dialog #seachStyleShopName').bind('keyup',function(event) {
					if (event.keyCode == keyCodes.ENTER) {
						BaryCentricProgrammeCatalog.searchShopDlg();
					}
				});
	        },
	        onBeforeClose: function() {
				var tabindex = 1;
				$('.InputTextStyle, select, input, button , li , tr, td, th, label, ul, a, img').each(function () {
					if (this.type != 'hidden') {
						$(this).attr("tabindex", '');
					}
					tabindex ++;
				});	
				var curIdFocus = $('#cur_focus').val();
				$('#' + curIdFocus).focus();				
	        },
	        onClose : function() {
	        	$('#' +searchDiv).html(html);
	        	$('.easyui-dialog #seachStyleShopCode').val('');
	        	$('.easyui-dialog #seachStyleShopName').val('');
	        	$('.easyui-dialog #errMsgDialog').html('').hide();
	        	BaryCentricProgrammeCatalog._listShopId = new Array();
	        }
	    });
	},
	searchShopDlg: function() {
		$('.easyui-dialog #errMsgDialog').html('').hide();
		var code = $('.easyui-dialog #seachStyleShopCode').val().trim();
		var name = $('.easyui-dialog #seachStyleShopName').val().trim();
		var arr = $.extend(true, [], CommonSearch._arrParams);
		var url = CommonSearch.getSearchStyleShopGridUrl($('.easyui-dialog #searchStyleShopUrl').val().trim(), code, name, arr);
		$('.easyui-dialog #searchStyleShopGrid').treegrid({url:url});
	},
	onCheckShop: function(id) {
		$('.ErrorMsgStyle').hide();
		var isCheck = $('.easyui-dialog #check_' + id).attr('checked');
	    	if ($('.easyui-dialog #check_' + id).is(':checked')) {
		    	if (BaryCentricProgrammeCatalog._listShopId.indexOf(id) < 0) {
		    		BaryCentricProgrammeCatalog._listShopId.push(id);
		    	}
		    	var classParent = $('.easyui-dialog #check_' + id).attr('class');
			var flag = true;
			var count = 0;
			$('.' +classParent).each(function() {
				if ($(this).is(':checked')) {
					flag = false;
					count++;
				}
			});
			if (flag || count == 1) {
				CommonSearch.recursionFindParentShopCheck(id);
			}
		} else {
			var idx = BaryCentricProgrammeCatalog._listShopId.indexOf(id);
			if (idx > -1) {
		    		BaryCentricProgrammeCatalog._listShopId.splice(idx, 1);
		    	}
			var classParent = $('.easyui-dialog #check_' +id).attr('class');
			var flag = true;
			var count = 0;
			$('.' +classParent).each(function() {
				if ($(this).is(':checked')) {
					flag = false;
				}
			});
			if (flag || count == 1) {
				CommonSearch.recursionFindParentShopUnCheck(id);
			}
			//CommonSearch.recursionFindParentShopUnCheck(id);
		}
	},
	createFocusShopMap: function() {
		$('.ErrorMsgStyle').hide();
		var params = new Object();
		params.listShopId = BaryCentricProgrammeCatalog._listShopId;
		params.id = $('#progId').val();
		if (BaryCentricProgrammeCatalog._listShopId.length<=0) {
			$('.easyui-dialog #errMsgDialog').html('Bạn chưa chọn đơn vị để thêm').show();
			return false;
		}
		Utils.addOrSaveData(params, '/catalog/bary-centric-programme/createfocusprogram', null, 'errMsgDialog', function(data) {
			if (data.error == undefined && data.errMsg == undefined) {
				BaryCentricProgrammeCatalog.searchShop();
				BaryCentricProgrammeCatalog._listShopId = new Array();
				$('#searchStyleShopFocus1').dialog('close');
				$('#errExcelMsg').html("").hide();
				$('#successMsg2').html('Lưu dữ liệu thành công.').show();
				var tm = setTimeout(function() {
					$('#successMsg2').html('').hide();
					clearTimeout(tm); 
				}, 3000);
			} 
		}, null, null, null, "Bạn có muốn thêm đơn vị này ?", null);
	},
	deleteFocusShopMap: function(shopId) {
		$('.ErrorMsgStyle').hide();
		var params = new Object();
		params.shopId = shopId;
		params.id = $('#progId').val();
		Utils.addOrSaveData(params, '/catalog/bary-centric-programme/deletefocusprogram', null, 'errExcelMsg', function(data) {
			BaryCentricProgrammeCatalog.searchShop();
			$('#errExcelMsg').html("").hide();
			$('#successMsg2').html('Xóa dữ liệu thành công.').show();
			var tm = setTimeout(function() {
				$('#successMsg2').html('').hide();
				clearTimeout(tm); 
			}, 3000);
		}, null, null, null, "Bạn có muốn xóa đơn vị này ?", null);
	},
	exportExcelShopMap: function() {
		$('#divOverlay').show();
		$('.ErrorMsgStyle').hide();
	    var focusId = $('#progId').val();
		var shopCode = $('#shopCode').val().trim();
		var shopName = $('#shopName').val().trim();
		
		var data = new Object();
		data.id = focusId;
		data.shopCode = shopCode;
		data.shopName = shopName;
		
		var url = "/catalog/bary-centric-programme/exportFocusShopMap";
		CommonSearch.exportExcelData(data,url);
	},
	/*****************************	HTBH ****************************************/
	_mapHTBH:null,
	/**
	 * @modify hunglm16
	 * @since 03/11/2015
	 * @description thuc hien phan quyen lai control
	 * */
	loadHTBH : function() {
		$('.ErrorMsgStyle').hide();
		var focusProgramId = $('#focusProgramId').val();	
		var params = new Object();
		params.focusProgramId =focusProgramId;
		BaryCentricProgrammeCatalog._beforeUpdateHTBH = [];
		BaryCentricProgrammeCatalog._mapHTBH = new Map();
		$("#gridHTBH").datagrid({
			url: '/catalog/bary-centric-programme/search-htbh?permissionEdit=false',
			queryParams:params,
	        pagination : true,
	        rownumbers : true,
	        pageNumber : 1,
	        scrollbarSize: 0,
	        autoWidth: true,
	        autoRowHeight : true,
	        fitColumns : true,
			width : ($('#gridContainerHTBH').width()),
			height: 'auto',
			columns:[[
			    	{field: 'code', title: 'Mã HTBH', width: 150, sortable: false, resizable: false, align: 'left', formatter: function(value, row, index) {
		        	  	return Utils.XSSEncode(value);
		         	}},
			    	{field: 'name', title: 'Tên HTBH', width: 300, sortable: false, resizable: false, align: 'left', formatter: function(value, row, index) {
		        	  	return Utils.XSSEncode(value);
		         	}},
			    	{field: 'id', checkbox: true, width: 50, align: 'center', sortable: false, resizable: false},
			    	{field: 'isExist', hidden: true, resizable: false, formatter: function(value, rowData, rowIndex) {
				    	if (rowData.isExist == 1) { 
				    		BaryCentricProgrammeCatalog._mapHTBH.put(rowData.id, rowData);
				    		if (BaryCentricProgrammeCatalog._beforeUpdateHTBH.indexOf(rowData.id) < 0) {
					    		BaryCentricProgrammeCatalog._beforeUpdateHTBH.push(rowData.id);
					    	}
				    	}
			    	}}
			]],
			onCheck : function(rowIndex, rowData) {
				$('.ErrorMsgStyle').hide();
				var selectedId = rowData['id'];
				BaryCentricProgrammeCatalog._mapHTBH.put(selectedId, rowData);
			},
			onUncheck : function(rowIndex, rowData) {
				$('.ErrorMsgStyle').hide();
				var selectedId = rowData['id'];
				BaryCentricProgrammeCatalog._mapHTBH.remove(selectedId);
			},
			onCheckAll : function(rows) {
				$('.ErrorMsgStyle').hide();
				if ($.isArray(rows)) {
					for (var i = 0, size = rows.length; i < size; i++) {
						var row = rows[i];
						var selectedId = row['id'];
						BaryCentricProgrammeCatalog._mapHTBH.put(selectedId, row);
			    	}
			    }
			},
			onUncheckAll : function(rows) {
				$('.ErrorMsgStyle').hide();
				if ($.isArray(rows)) {
					for (var i = 0, size = rows.length; i < size; i++) {
						var row = rows[i];
						var selectedId = row['id'];
						BaryCentricProgrammeCatalog._mapHTBH.remove(selectedId);
			    	}
			    }
			},
			onLoadSuccess: function() {
				$('#gridContainerHTBH .datagrid-header-rownumber').html('STT');
		    	updateRownumWidthForDataGrid('#gridContainerHTBH');
				$('.ErrorMsgStyle').hide();
		    	setTimeout(function() {
		    		$('#successMsgHTBH').html('').hide();
		    	}, 1500);
				$('#gridHTBH').datagrid('resize');
				
				//Thuc hien phan quyen control
//				var arrDelete =  $('#gridContainerHTBH td[field="id"]');
//				if (arrDelete != undefined && arrDelete != null && arrDelete.length > 0) {
//				  for (var i = 0, size = arrDelete.length; i < size; i++) {
//				    $(arrDelete[i]).prop("id", "btnSaveHTBH_table_gr_id_" + i);//Khai bao id danh cho phan quyen
//					$(arrDelete[i]).addClass("cmsiscontrol");
//				  }
//				}
//				Utils.functionAccessFillControl('gridContainerHTBH', function(data) {
//					//Xu ly cac su kien lien quan den cac control phan quyen
////					if ($('#btnSaveHTBH').length > 0) {
////						$('#gridHTBH').datagrid("showColumn", "id");
////					}
//				});
				$('#gridHTBH').datagrid("showColumn", "id");
				var runner = 0;
				var isAll = 0;
				$('#gridContainerHTBH input:checkbox[name=id]').each(function() {
					var selectedId = this.value;
					if (BaryCentricProgrammeCatalog._mapHTBH.keyArray.indexOf(parseInt(selectedId)) > -1) {
						isAll++;
						$('#gridHTBH').datagrid('selectRow', runner);
					} 
					runner ++; 
				});
				if (isAll > 0 && isAll == runner) {
					$('#gridContainerHTBH td[field=id] .datagrid-header-check input:checkbox').attr('checked', 'checked');
				} else {
					$('#gridContainerHTBH td[field=id] .datagrid-header-check input:checkbox').removeAttr('checked');
				}
			}
		});
		return false;
	},
	searchHTBH : function () {
		$('.ErrorMsgStyle').hide();
		var params = new Object();
		params.focusProgramId = $('#focusProgramId').val().trim();
		params.code = $('#codeHTBH').val().trim();
		params.name = $('#nameHTBH').val().trim();
		BaryCentricProgrammeCatalog._beforeUpdateHTBH = [];
		BaryCentricProgrammeCatalog._mapHTBH = new Map();
		$("#gridHTBH").datagrid('load', params);
		return false;
	},
	saveHTBH: function() {
		var msg = '';
		$('.ErrorMsgStyle').hide();
		var focusProgramId = $('#focusProgramId').val();	//focusProgramId
		var mapKey = BaryCentricProgrammeCatalog._mapHTBH.keyArray;
		if (mapKey.length == BaryCentricProgrammeCatalog._beforeUpdateHTBH.length) {
			var c = 0;
			var sz = mapKey.length;
			for (var i = 0; i < sz; i++) {
				if (BaryCentricProgrammeCatalog._beforeUpdateHTBH.indexOf(mapKey[i]) < 0) {
					break;
				}
				c++;
			}
			if (c == sz) {
				$("#errMsg").html("Dữ liệu không có thay đổi để lưu").show();
				return false;
			}
		}
		var dataModel = new Object();
		dataModel.focusProgramId = focusProgramId;
		dataModel.lstApParamId =  mapKey;
		Utils.addOrSaveData(dataModel, "/catalog/bary-centric-programme/save-htbh", null, 'errMsg',function(data) {
			$('#successMsgHTBH').html('Lưu dữ liệu thành công').show();
			$.getJSON('/rest/catalog/bary-centric/sale-type/list.json?id=' + $('#focusProgramId').val(), function(data) {
				var arrHtml = new Array();
				arrHtml.push('<option value="-1">--- Chọn HTBH  ---</option>');
				for (var i=0;i<data.length;i++) {
					arrHtml.push('<option value="' + data[i].id + '">' + Utils.XSSEncode(data[i].saleTypeCode) + '</option>');
				}
				$('#saleTypeCodeDlg').html('');
				$('#saleTypeCodeDlg').html(arrHtml.join(""));
				$('#saleTypeCodeDlg').change();
			});
			BaryCentricProgrammeCatalog._beforeUpdateHTBH = [];
			BaryCentricProgrammeCatalog._mapHTBH = new Map();
			$("#gridHTBH").datagrid("reload");
			BaryCentricProgrammeCatalog.getJsonListSaleType();
		});
		return false;
	}
};
/*
 * END OF FILE - /web/web/resources/scripts/business/catalog/vnm.bary-centric-programme-catalog.js
 */

/*
 * START OF FILE - /web/web/resources/scripts/business/catalog/vnm.brand-catalog.js
 */
var BrandCatalog = {
	_xhrSave : null,
	_xhrDel: null,
	getGridUrl: function(brandCode,brandName,description,status){
		return "/catalog/brand/search?code=" + encodeChar(brandCode) + "&name=" + encodeChar(brandName) + "&note=" + encodeChar(description) + "&status=" + status;
	},	
	searchBrand: function(){
		$('#productContainerDiv').hide();
		$('#errMsg').html('').hide();
		var params = new Object();		
		params.code = $('#brandCode').val().trim();
		params.name = $('#brandName').val().trim();
		params.status = 1;
		var tm = setTimeout(function() {
			$('#brandCode').focus();
		}, 500);
		$("#grid").datagrid('load',params);	
		return false;
	},
	saveBrand: function(){
		var msg = '';
		$('.ErrorMsgStyle').hide();
		msg = Utils.getMessageOfRequireCheck('brandCodePop','Mã nhãn hàng');
		if(msg.length == 0){
			msg = Utils.getMessageOfSpecialCharactersValidate('brandCodePop','Mã nhãn hàng',Utils._CODE);
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfRequireCheck('brandNamePop','Tên nhãn hàng');
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfSpecialCharactersValidate('brandNamePop','Tên nhãn hàng');
		}
		if(msg.length > 0){
			$('#errMsg').html(msg).show();
			return false;
		}
		var dataModel = new Object();
		dataModel.id = $('#selBrandId').val().trim();
		dataModel.code = $('#brandCodePop').val().trim();
		dataModel.name = $('#brandNamePop').val().trim();
		dataModel.status = 1;
		Utils.addOrSaveRowOnGrid(dataModel, "/catalog/brand/save", BrandCatalog._xhrSave, null, null, function(data){
			if(!data.error){
				$('#popup1').dialog('close');
				BrandCatalog.searchBrand();
			}
		});		
		return false;
	},
	getSelectedBrand: function(index){
		$('.ErrorMsgStyle').hide();
		$('.easyui-dialog input').val('');
		$('#brandCodePop').removeAttr('disabled');
		if(index != null && index !=undefined){
			var row = $('#grid').datagrid('getRows')[index];
			$('#selBrandId').val(row.id);			
			$('#brandCodePop').val(row.productInfoCode);
			$('#brandNamePop').val(row.productInfoName);
			$('#brandCodePop').attr('disabled','disabled');
		}else{
			$('#selBrandId').val(0);
		}
        $('#popup1').dialog('open');
		return false;
	},
	resetForm: function(){
		BrandCatalog.clearData();	
		$("#grid").trigger("reloadGrid");
		return false;
	},
	deleteBrand: function(brandCode){
		var dataModel = new Object();
		dataModel.code = brandCode;	
		Utils.deleteSelectRowOnGrid(dataModel, 'brand', '/catalog/brand/delete', BrandCatalog._xhrDel, null, null,function(data){
			BrandCatalog.clearData();
		});
		return false;
	},
	clearData: function(){
		setTitleSearch();
		$('.RequireStyle').hide();
		$('#errMsg').html('').hide();
		$('#selBrandId').val(0);
		$('#brandCode').removeAttr('disabled');
		$('#btnCancel').hide();
		$('#btnSearch').show();
		$('#btnAdd').show();
		$('#brandCode').val('');
		$('#brandName').val('');
		focusFirstTextbox();
		setSelectBoxValue('status', 1);	
		BrandCatalog.searchBrand();
	},
	getChangedForm: function(){
		$('.RequireStyle').show();
		$('#btnSearch').hide();
		$('#btnAdd').hide();
		$('#btnCancel').show();	
		focusFirstTextbox();
	},
	loadProductOfBrand : function(index){
		$('.ErrorMsgStyle').hide();
		$('#productContainerDiv').show();
		var row = $('#grid').datagrid('getRows')[index];
		$('#productTitle').html('Danh sách sản phẩm thuộc nhãn hàng : s' + Utils.XSSEncode(row.productInfoCode));
		$("#productGrid").datagrid({
			  url : '/catalog/brand/product',
			  queryParams : {
				  id : row.id
			  },
			  width: $(window).width() - 50,
			  pageSize : 10,
			  height:'auto',
			  scrollbarSize : 0,
			  pagination:true,
			  singleSelect:true,
			  fitColumns:true,		  
			  rownumbers: true,
			  columns:[[	
				{field: 'productCode', title: 'Mã sản phẩm', width: 150, sortable:false,resizable:false, align: 'left', formatter: function(value, row, index) {
					return Utils.XSSEncode(value);
				}},
			    {field: 'productName', title: 'Tên sản phẩm', width: $(window).width() - 350, sortable:false,resizable:false, align: 'left' , formatter: function(value, row, index) {
					return Utils.XSSEncode(value);
				}},
			    {field: 'categoryCode', title: 'Ngành hàng', width: 100, align: 'left',sortable:false,resizable:false, formatter: function(value, row, index) {
					return Utils.XSSEncode(value);
				}},						   					    
			    {field: 'del', title: '<img src="/resources/images/icon_add.png" style="cursor:pointer" alt="Thêm mới" title="Thêm mới" onclick="BrandCatalog.selectListProduct('+row.id+')" />', width: 50, align: 'center',sortable:false,resizable:false, 
			    	formatter:function(value, row,index){
			    		var html = '<a onclick="BrandCatalog.resetBrand({0})" href="javascript:void(0)"><img title="Xóa" src="/resources/images/icon_delete.png"></a>';
			    		return format(html,row.id);
			    	}
			   	},
			  ]],
			 
			  onLoadSuccess:function(){
			      	$('#productGrid').datagrid('resize');
			      	var tm = setTimeout(function(){
			      		var hg = $('.ContentSection').height() - 100; 
				      	$('.ReportTreeSection').css('height',hg);
			      	},500);
			      }
			});
	},
	selectListProduct : function(brandId){
		$('.ErrorMsgStyle').hide();
		var arrParam = new Array();
		var params = new Object();
		params.name = 'checkBrand';
		params.value = true;
		arrParam.push(params);
		CommonSearchEasyUI.openSearchStyle2EasyUIMutilSelectDialog("Mã sản phẩm", "Tên sản phẩm", "Thêm sản phẩm","/commons/search-product-brand",function(data){
			var lstProducts = new Array();
			for(var i = 0 ; i < data.length; ++i){
				lstProducts.push(data[i].id);
			}
			var params = new Object();
			params.id = brandId;
			params.lstProducts = lstProducts;
			Utils.addOrSaveData(params,'/catalog/brand/add-product',null,'errMsgId',function(result){
				$("#productGrid").datagrid('reload');
			});			
		},"productCode", "productName", arrParam);
	},
	resetBrand : function(productId){
		$('.ErrorMsgStyle').hide();
		var params = new Object();
		params.productId = productId;
		Utils.addOrSaveData(params,'/catalog/brand/reset-brand',null,'errMsgId',function(result){
			$("#productGrid").datagrid('reload');
		});		
	}
};
/*
 * END OF FILE - /web/web/resources/scripts/business/catalog/vnm.brand-catalog.js
 */

/*
 * START OF FILE - /web/web/resources/scripts/business/catalog/vnm.car-type-catalog.js
 */
var CarTypeCatalog = {
	_xhrSave : null,
	_xhrDel:null,
	getGridUrl: function(shopCode,type,number,label,gross,category,origin,status,isAll){
		isAll = null;
		shopCode = $('#shopCode').val().trim();
		var param = '';
		if(isAll != null || isAll != undefined ){
			param = "&isAll=" + encodeChar(isAll);
		}
		return "/catalog/car-type/search?shopCode=" + encodeChar(shopCode) + "&type=" + encodeChar(type) + "&number=" + encodeChar(number) + "&label=" + encodeChar(label) + "&gross=" + encodeChar(gross) + "&category=" + encodeChar(category) + "&origin=" + encodeChar(origin) + "&status=" + status + param;
	},	
	search: function(){
		$('#errMsg').html('').hide();
		$('#errExcelMsg').html('').hide();
		var type = $('#type').val().trim();
		var number = $('#number').val().trim();
		var label = $('#label').val().trim();
		var gross = $('#gross').val().trim();
		var category = $('#category').val().trim();
		var origin = $('#origin').val().trim();
		var status = $('#status').val().trim();
		var params =  new Array();
		params.type = type;
		params.number =  number;
		params.label = label;
		params.gross = gross;
		params.category = category;
		params.origin = origin;
		params.status = status;
		$('#grid').datagrid('load',params);
		return false;
	},
	change: function(){
		var msg = '';
		$('#errMsg').html('').hide();
		$('#errExcelMsg').html('').hide();
//		msg = Utils.getMessageOfRequireCheck('shopCode','Mã đơn vị');
//		if(msg.length == 0){
//			msg = Utils.getMessageOfSpecialCharactersValidate('shopCode','Mã đơn vị',Utils._CODE);
//		}
		if(msg.length == 0){
			msg = Utils.getMessageOfRequireCheck('number','Số xe');
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfSpecialCharactersValidateEx('number','Số xe',Utils._TF_CAR_NUMBER);
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfRequireCheck('status','Trạng thái',true);
		}
		if(msg.length > 0){
			$('#errMsg').html(msg).show();
			return false;
		}
		var dataModel = new Object();
		dataModel.id = $('#carTypeId').val().trim();
		dataModel.type = $('#type').val().trim();
		dataModel.number = $('#number').val().trim();
		dataModel.label = $('#label').val().trim();
		var gross = parseInt($('#gross').val().trim());
		if(gross <= 0){
			msg = 'Trọng tải(kg) số nguyên lớn hơn 0';
			if(msg.length > 0){
				$('#errMsg').html(msg).show();
				return false;
			}
		}else{
			dataModel.gross = $('#gross').val().trim();
		}
		dataModel.category = $('#category').val().trim();
		dataModel.origin = $('#origin').val().trim();
		dataModel.status = $('#status').val().trim();
		Utils.addOrSaveRowOnGrid(dataModel, "/catalog/car-type/save", CarTypeCatalog._xhrSave, null, null,function(){
			CarTypeCatalog.resetForm();
		});		
		return false;
	},
	getSelectedCarType: function(id,number,category,gross,label,origin,shopCode,status,type){
		Utils.getChangedForm();
		setTitleUpdate();
//		$('#shopCode').attr('disabled','disabled');
		if(id!= null && id!= 0 && id!=undefined){
			$('#carTypeId').val(id);
		} else {
			$('#carTypeId').val(0);
		}
		setTextboxValue('shopCode',shopCode);
		setTextboxValue('number',number);
		$('#number').attr('disabled','disabled');
		setTextboxValue('gross',gross);
		setSelectBoxValue('type',type);
		setSelectBoxValue('label',label);
		setSelectBoxValue('category',category);
		setSelectBoxValue('origin',origin);
		setSelectBoxValue('status', status);
		$('#btnSearch,#btnCreate').hide();
		$('#btnUpdate,#btnDismiss').show();
	},
	resetForm: function(){
		$('span#errIU').html('');
		$('#titleCarId').html('Thông tin tìm kiếm');
		$('.RequireStyle').hide();
		setTitleSearch();
		$('#errMsg').html('').hide();
		$('#errExcelMsg').html('').hide();
		$('#number').removeAttr('disabled');
		$('#btnUpdate,#btnDismiss').hide();
		$('#btnCreate,#btnSearch').show();
		$('#shopCode').val('');
		$('#shopCode').focus();
		$('#number').val('');
		$('#gross').val('');
		$('#type').val(223);
		$('#type').change();
		$('#label').val(-1);
		$('#label').change();
		$('#category').val(-1);
		$('#category').change();
		$('#origin').val(-1);
		$('#origin').change();
		$('#status').val(1);
		$('#status').change();
		$('#carTypeId').val(0);
		Utils.comboboxChange('label','Chọn tất cả');
		Utils.comboboxChange('type','Chọn tất cả');
		Utils.comboboxChange('category','Chọn tất cả');
		Utils.comboboxChange('origin','Chọn tất cả');
		$("#grid").trigger("reloadGrid");
		return false;
	},
	getListParentCode:function(parentId,objParentCode){
		$.ajax({
			type : "POST",
			url : "/catalog/car-type/getlistparentcode",
			data :({id:parentId}),
			dataType : "json",
			success : function(data) {
				var html = new Array();
    			if(data!= null && data!= undefined && data.lstParentCode!= null && data.lstParentCode.length > 0){
    				html.push("<option value='-1'>---Chọn mã cha---</option>");
    				for(var i=0;i<data.lstParentCode.length;i++){
    					html.push("<option value='"+ data.lstParentCode[i].id +"'>"+ Utils.XSSEncode(data.lstParentCode[i].channelTypeCode) +"</option>");
    				}
    			}else{
    				html.push("<option value='-1'>---Chọn mã cha---</option>");
    			}	        			        	
    			$(objParentCode).html(html.join(""));
    			$(objParentCode).val(parentId);
    			$(objParentCode).change();
			},
			error:function(XMLHttpRequest, textStatus, errorThrown) {
				$.ajax({
					type : "POST",
					url : '/check-session',
					dataType : "json",
					success : function(data) {
					},
					error: function(XMLHttpRequest, textStatus, errorThrown) {
						window.location.href = '/home';
					}
				});
			}
		});
	},
	deleteCarType: function(carId){
		$('#errMsg').html('').hide();
		$('#errExcelMsg').html('').hide();
		var dataModel = new Object();
		dataModel.id = carId;	
		Utils.deleteSelectRowOnGrid(dataModel, 'loại xe', "/catalog/car-type/remove", CarTypeCatalog._xhrDel, null, null,function(data){
			if (!data.error){
//				CarTypeCatalog.loadTreeEx();
				CarTypeCatalog.resetForm();
			}
		},null);
		return false;		
	},
	loadTree: function(){
		var shopCode = $('#shopCodeHidden').val().trim();
		loadDataForTree('/rest/catalog/unit-tree/'+encodeChar(shopCode)+'/tree.json');	
		$('#tree').bind("loaded.jstree", function(event, data){
			if($('#treeContainer').data("jsp")!= undefined){
				$('#treeContainer').data("jsp").destroy();
			}
			$('#treeContainer').jScrollPane();
		});
		$('#tree').bind("select_node.jstree", function (event, data) {
			var isAll = false;
			if($('#isAll').is(':checked')){
				isAll = true;
			}
			var name = data.inst.get_text(data.rslt.obj);
			var id = data.rslt.obj.attr("id");
			var code = data.rslt.obj.attr("contentItemId");
			var url = CarTypeCatalog.getGridUrl(code,'','',-1,'',-1,-1,1,isAll);
			$("#grid").setGridParam({url:url,page:1}).trigger("reloadGrid");
	    });
	},
	loadTreeEx:function(){
		CarTypeCatalog.loadShopTree();
	},
	loadShopTree:function(){
		$('#tree').jstree({
	        "plugins": ["themes", "json_data","ui"],
	        "themes": {
	            "theme": "classic",
	            "icons": false,
	            "dots": true
	        },
	        "json_data": {
	            "ajax": {
	            	"url": function( node ){
	                      if( node == -1 ){
	                    	  return "/rest/catalog/unit-tree-ex/"+$('#shopIdHidden').val().trim()+"/0/tree.json";
	                      } else {
	                    	  return "/rest/catalog/unit-tree-ex/"+node.data("attr").id+"/1/tree.json";
	                      }
	                    },
	                "type": 'GET',
	                "success": function(ops) {
	                    data = [];
	                    for( opnum in ops ){
	                      var op = ops[opnum];
	                      node = {
	                          "data" : op.data,
	                          "attr" : op.attr,
	                          "metadata" :  op ,
	                          "state"	: op.state
	                          /*"state" : "closed"*/
	                      };
	                      data.push( node );
	                    }
	                    return data;
	                },
	                'complete':function(){	                	
	                	$('#tree').bind("select_node.jstree", function (event, data) {
	                		var isAll = false;
	            			if($('#isAll').is(':checked')){
	            				isAll = true;
	            			}
	            			var name = data.inst.get_text(data.rslt.obj);
	            			var id = data.rslt.obj.attr("id");
	            			var code = data.rslt.obj.attr("contentItemId");
	            			var url = CarTypeCatalog.getGridUrl(code,'','',-1,'',-1,-1,1,isAll);
	            			$("#grid").setGridParam({url:url,page:1}).trigger("reloadGrid");            			
	            	    });
	                	$('#tree').bind("loaded.jstree", function(event, data){
	                		var tm = setTimeout(function() {
		                		if($('#treeContainer').data("jsp")!= undefined){
		            				$('#treeContainer').data("jsp").destroy();
		            			}
		            			$('#treeContainer').jScrollPane();
		                    }, 500);
	                    });
	                	var tm = setTimeout(function() {
	                		if($('#treeContainer').data("jsp")!= undefined){
	            				$('#treeContainer').data("jsp").destroy();
	            			}
	            			$('#treeContainer').jScrollPane();
	                    }, 500);
	                }
	            }
	        },
	        "core" : {"load_open" : true }
		});	
	},
	exportExcelData:function(){
		showLoadingIcon();
		$('#errMsg').html('').hide();
		$('#errExcelMsg').html('').hide();
		var shopCode = $('#shopCode').val().trim();
		var type = $('#type').val().trim();
		var number = $('#number').val().trim();
		var label = $('#label').val().trim();
		var gross = $('#gross').val().trim();
		var category = $('#category').val().trim();
		var origin = $('#origin').val().trim();
		var status = $('#status').val().trim();
		var data = new Object();
		data.shopCode = shopCode;
		data.type = type;
		data.number = number;
		data.label = label;
		data.gross = gross;
		data.category = category;
		data.origin = origin;
		data.status = status;
		var url = "/catalog/car-type/getlistexceldata";
		CommonSearch.exportExcelData(data,url);
	},
	importExcel:function(){
		$('#isView').val(0);
		$('#importFrm').submit();
		return false;
	},	
	viewExcel:function(){
		$('#isView').val(1);
		$('#importFrm').submit();
		return false;
	}
};
/*
 * END OF FILE - /web/web/resources/scripts/business/catalog/vnm.car-type-catalog.js
 */

/*
 * START OF FILE - /web/web/resources/scripts/business/catalog/vnm.category-catalog.js
 */
var CategoryCatalog = {
	_xhrSave : null,
	_xhrDel: null,
	_stockLock: null,
	getGridUrl: function(code,name,description,status){
		return "/catalog/category/search?code=" + encodeChar(code) + "&name=" + encodeChar(name) + "&note=" + encodeChar(description) + "&status=" + status;
	},
	getStockLockByStaffCode : function(code){
		CategoryCatalog._stockLock = null;
		var params = {};
		params.staffCode = code;
		Utils.getJSONDataByAjax(params,"/catalog/utility/closing-day/get-locked-stock-by-staffCode",function(data){
			if(data!=undefined && data!=null && data.stockLock!=undefined && data.stockLock!=null){
				CategoryCatalog._stockLock = data.stockLock;
			}
		},null,null);
		
	},
	
	search: function(){
		$('#errMsg').html('').hide();
		var code = $('#categoryCode').val().trim();
		var name = $('#categoryName').val().trim();
		var description = $('#description').val().trim();
//		var status = $('#status').val().trim();
		var status = 1;
		var tm = setTimeout(function() {
			$('#categoryCode').focus();
		}, 500);
		var url = CategoryCatalog.getGridUrl(code,name,description,status);
		$("#grid").datagrid({url:url,page:1});
		return false;
	},
	save: function(){
		var msg = '';
		$('#errMsg').html('').hide();
		msg = Utils.getMessageOfRequireCheck('categoryCodePop','Mã category');
		if(msg.length == 0){
			msg = Utils.getMessageOfSpecialCharactersValidate('categoryCodePop','Mã category',Utils._CODE);
			if(msg.length > 0){
				msg = "Giá trị Mã ngành hàng nhập vào chỉ nằm trong các ký tự [A-Z]";
			}
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfSpecialCharactersValidate('categoryNamePop','Tên category');
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfRequireCheck('categoryNamePop','Tên category');
		}
		if(msg.length == 0 && $('#descriptionPop').val().trim().length > 0){
			msg = Utils.getMessageOfSpecialCharactersValidate('descriptionPop','Mô tả');
		}

		if(msg.length > 0){
			$('#errMsg').html(msg).show();
			return false;
		}
		var dataModel = new Object();
		dataModel.id = $('#selId').val().trim();
		dataModel.code = $('#categoryCodePop').val().trim();
		dataModel.name = $('#categoryNamePop').val().trim();
		dataModel.note = $('#descriptionPop').val().trim();
		dataModel.status = 1;
		Utils.addOrSaveRowOnGrid(dataModel, "/catalog/category/save", CategoryCatalog._xhrSave, null, null, function(data){
			if(!data.error){
				$('#popup1').dialog('close');
				CategoryCatalog.search();
			}
		});		
		return false;
	},
	getSelectedCategory: function(rowId, code, name, description, status){	
	
		if(rowId != null && rowId != 0 && rowId != undefined){
			$('#selId').val(rowId);
		} else {
			$('#selId').val(0);
		}
		if(description == 'null'){
			description= '';
		}
		$('#categoryCodePop').val(Utils.XSSEncode(code));
		$('#categoryNamePop').val(Utils.XSSEncode(name));
		$('#descriptionPop').val(Utils.XSSEncode(description));
        $('#popup1').dialog('open');
		return false;
	},
	resetForm: function(){
		CategoryCatalog.clearData();	
		$("#grid").trigger("reloadGrid");
		return false;
	},
	deleteCategory: function(code){
		var dataModel = new Object();
		dataModel.code = code;	
		Utils.deleteSelectRowOnGrid(dataModel, 'category', '/catalog/category/delete', CategoryCatalog._xhrDel, null, null, function(data){
			CategoryCatalog.clearData();
		});
		focusFirstTextbox();
		return false;		
	},
	clearData: function(){
		setTitleSearch();
		$('.RequireStyle').hide();
		$('#selId').val(0);
		$('#errMsg').html('').hide();
		$('#code').removeAttr('disabled');
		$('#btnEdit').hide();
		$('#btnCancel').hide();
		$('#btnSearch').show();
		$('#btnAdd').show();		
		$('#code').val('');
		$('#name').val('');
		$('#description').val('');
		focusFirstTextbox();
		setSelectBoxValue('status', 1);	
		Utils.unbindFormatOnTextfield('code');
		CategoryCatalog.search();
		Utils.bindAutoSearch();
	},
	getChangedForm: function(){
		Utils.bindFormatOnTextfield('code',Utils._TF_A_Z);
		$('#code').val('');
		$('.RequireStyle').show();
		$('#btnSearch').hide();
		$('#btnAdd').hide();
		$('#btnEdit').show();
		$('#btnCancel').show();	
		$('#code').focus();
		focusFirstTextbox();
		setTitleAdd();
	}
};
/*
 * END OF FILE - /web/web/resources/scripts/business/catalog/vnm.category-catalog.js
 */

/*
 * START OF FILE - /web/web/resources/scripts/business/catalog/vnm.category-type-product-catalog.js
 */
var CategoryTypeProductCatalog ={
	_xhrSave : null,
	_xhrDel: null,
	_curRowId: null,
	resultext:null,
	searchFilter:null,
	indexedit:0,
	getGridUrl: function(shopCode,shopName,categoryType,productCode,status){
		return "/product-distributer/search?shopCode=" + encodeChar(shopCode) + "&shopName=" + encodeChar(shopName) + "&categoryType=" + encodeChar(categoryType) +"&productCode=" + encodeChar(productCode) + "&status=" + status;
	},
	
	fillTxtShopCodeByF9 : function(code){
		$('#shopCode').val(Utils.XSSEncode(code)).show();
		$('#common-dialog-search-2-textbox').dialog('close');
		$('#shopCode').focus();
	},
	
	fillTxtProductCodeByF9 : function(code){
		$('#productCode').val(Utils.XSSEncode(code)).show();
		$('#common-dialog-search-2-textbox').dialog('close');
		$('#productCode').focus();
	},
	
	search: function(){
		$('#errMsg').html('').hide();
		var shopCode = $('#shopCode').val().trim();
		var shopName= '';
		var productCode = $('#productCode').val().trim();
		var categoryType = -1;
		var status = $('#status').val();
		var tm = setTimeout(function() {
			$('#shopCode').focus();
		}, 500);
		
		CategoryTypeProductCatalog.searchFilter = new Object();
		CategoryTypeProductCatalog.searchFilter.shopCodeF = shopCode;
		CategoryTypeProductCatalog.searchFilter.productCodeF = productCode;
		CategoryTypeProductCatalog.searchFilter.statusF = status;
		
		var url = CategoryTypeProductCatalog.getGridUrl(shopCode,shopName,categoryType,productCode,status);
		$("#grid").datagrid({url:url,pageNumber:1});
		return false;
	},
	exportExcel: function() {
		var shopCode = $('#shopCode').val().trim();
		var shopName= '';
		var productCode = $('#productCode').val().trim();
		var categoryType = -1;
		var status = $('#status').val();
		var params = new Object();
		params.shopCode = shopCode;
		params.shopName = shopName;
		params.productCode = productCode;
		params.categoryType = categoryType;
		params.status = status;		
		var url = "/product-distributer/export";
//		CommonSearch.exportExcelData(params,url,'errExcelMsg');
		ReportUtils.exportReport(url, params, 'errExcelMsg');
	},
	change: function(){
		var msg = '';
		$('.ErrorMsgStyle').html('').hide();
		var shopObjectType = $('#shopObjectType').val();
		if(shopObjectType!=undefined && shopObjectType!=null  && parseInt(shopObjectType)!= ShopObjectType.VNM &&  parseInt(shopObjectType)!= ShopObjectType.GT){
			CategoryTypeProductCatalog.search();
			msg = "Không được quyền thực hiện chức năng";
			$('#errMsg').html(msg).show();
			return false;
		}
		
		if(msg.length == 0){
			msg = Utils.getMessageOfSpecialCharactersValidate('stockMin','Tồn kho Min');
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfInvaildNumber('stockMin', 'Tồn kho Min');
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfNegativeNumberCheck('stockMin','Tồn kho Min');
		}
		if(msg.length == 0 && $('#stockMin').val().replace(/,/g,'')>9999){
			msg='Tồn kho min không được vượt quá 9999';
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfSpecialCharactersValidate('stockMax','Tồn kho Max');
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfInvaildNumber('stockMax', 'Tồn kho Max');
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfNegativeNumberCheck('stockMax','Tồn kho Max');
		}

		if(msg.length == 0){
			msg = Utils.getMessageOfSpecialCharactersValidate('dateGo','Ngày đi đường');
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfInvaildNumber('dateGo', 'Ngày đi đường');
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfNegativeNumberCheck('dateGo','Ngày đi đường');
		}

		if(msg.length == 0){
			msg = Utils.getMessageOfRequireCheck('dateSale','Ngày bán hàng');
		}
		if(msg.length == 0){
			if(CategoryTypeCatalog.parseCharToValue($('#dateSale').val().trim()) == '0000000'){
				msg = 'Bạn chưa nhập giá trị cho trường Ngày bán hàng. Yêu cầu nhập giá trị';
			}
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfFloatValidate('growth','Tỉ lệ tăng trưởng');
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfSpecialCharactersValidate('status','Trạng thái',Utils._CODE);
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfRequireCheck('status','Trạng thái');
		}

		if(msg.length > 0){
			$('#errMsg').html(msg).show();
			return false;
		}
		var dataModel = new Object();
		dataModel.productId = $('#productId').val();
		dataModel.shopCode = $('#shopCodeGrid').val().trim();		
		dataModel.productCode = $('#productCodeGrid').val().trim();
		dataModel.stockMin = $('#stockMin').val().trim();
		dataModel.stockMax = $('#stockMax').val().trim();
		dataModel.dateGo = $('#dateGo').val().trim();
		dataModel.dateSale = $('#dateSale').val().trim();
		dataModel.growth = $('#growth').val().trim();
		if(parseFloat(dataModel.growth) >9999){
			$('#errMsg').html('Tỉ lệ tăng trưởng phải nhỏ hơn 10,000.00').show();
			return false;
		}
		dataModel.status = $('#status1').val();
		Utils.addOrSaveData(dataModel, "/product-distributer/save", CategoryTypeProductCatalog._xhrSave, null,function(data){
			if(data.error == false){
				CategoryTypeProductCatalog.reset();
				if(CategoryTypeProductCatalog.searchFilter != null){
					setTextboxValue('shopCode',CategoryTypeProductCatalog.searchFilter.shopCodeF);
					setTextboxValue('productCode',CategoryTypeProductCatalog.searchFilter.productCodeF);
					setTextboxValue('status',CategoryTypeProductCatalog.searchFilter.statusF);										
				}				
				CategoryTypeProductCatalog.search();
				return false;
			}
		});
		$('#grid').datagrid('beginEdit', CategoryTypeProductCatalog.indexedit);
		return false;
	},
	deleteRow: function(productId){
		$('#errMsg').html('').hide();
		var dataModel = new Object();
		dataModel.productId = productId;	
		Utils.deleteSelectRowOnGrid(dataModel, 'sản phẩm bán cho đơn vị', "/product-distributer/remove", CategoryTypeProductCatalog._xhrDel, null, null);		
		return false;		
	},
	getCategoryOfProduct: function(status,categoryId,categoryCode){
		$.getJSON('/rest/catalog/category-product/list.json?status=' + status+'&categoryId='+categoryId, function(data){
			var arrHtml = new Array();
			arrHtml.push('<option value="-1">--- Chọn mã ngành hàng ---</option>');
			for(var i=0;i<data.length;i++){
				arrHtml.push('<option value="'+ Utils.XSSEncode(data[i].value) +'">'+ Utils.XSSEncode(data[i].name) +'</option>');
			}
			$('#category').html(arrHtml.join(""));
			$('#category').change();
			setSelectBoxValue('category', categoryId);
		});
	},
	importExcel:function(){
		// tao options
		$('#importFrm').submit();
		
	},
	beforeImportExcel: function(){
		if(!previewImportExcelFile(document.getElementById("excelFile"))){
			return false;
		}
		//showLoading();
		$('.ErrorMsgStyle').html('').hide();
		$('.SuccessMsgStyle').html('').hide();
		showLoadingIcon();
		return true;
	},
	afterImportExcel: function(responseText, statusText, xhr, $form){//ProductLevelCatalog
		hideLoadingIcon();
		CategoryTypeProductCatalog.search();
		
		//Show hien thi thong bao
		if (statusText == 'success') {				
	    	$("#responseDiv").html(responseText);		 
	    	var newToken = $('#responseDiv #newToken').val();
	    	console.log(newToken);
	    	if(newToken != null && newToken != undefined && newToken != '') {
	    		$('#token').val(newToken);	    		
	    		$('#tokenImport').val(newToken);
	    	}
	    	if($('#errorExcel').html().trim() == 'true' || $('#errorExcelMsg').html().trim().length > 0){
	    		$('#errExcelMsg').html($('#errorExcelMsg').html()).show();
	    	} else {
	    		var totalRow = parseInt($('#totalRow').html().trim());
    			var numFail = parseInt($('#numFail').html().trim());
    			var fileNameFail = $('#fileNameFail').html();
    			var mes = format(msgErr_result_import_excel,(totalRow - numFail),numFail);		    		
    			if(numFail > 0){
    				mes+= ' <a href="'+ fileNameFail +'">Xem chi tiết lỗi</a>';
    			}
    			if($('#excelFile').length!=0 && $('#fakefilepc').length!=0){
    				try{$('#excelFile').val('');$('#fakefilepc').val('');}catch(err){}
    			}
    			$('#errExcelMsg').html(mes).show();
	    	}
	    }
	},
	
	afterImportExcelNganh: function(responseText, statusText, xhr, $form){//ProductLevelCatalog
		hideLoadingIcon();
		//Reset form Web
		CategoryTypeCatalog.search();
		//Show hien thi thong bao
		if (statusText == 'success') {				
	    	$("#responseDiv").html(responseText);		 
	    	var newToken = $('#responseDiv #newToken').val();
	    	console.log(newToken);
	    	if(newToken != null && newToken != undefined && newToken != '') {
	    		$('#token').val(newToken);	    		
	    		$('#tokenImport').val(newToken);
	    	}
	    	if($('#errorExcel').html().trim() == 'true' || $('#errorExcelMsg').html().trim().length > 0){
	    		$('#errExcelMsg').html($('#errorExcelMsg').html()).show();
	    	} else {
	    		var totalRow = parseInt($('#totalRow').html().trim());
    			var numFail = parseInt($('#numFail').html().trim());
    			var fileNameFail = $('#fileNameFail').html();
    			var mes = format(msgErr_result_import_excel,(totalRow - numFail),numFail);		    		
    			if(numFail > 0){
    				mes+= ' <a href="'+ fileNameFail +'">Xem chi tiết lỗi</a>';
    			}
    			if($('#excelFile').length!=0 && $('#fakefilepc').length!=0){
    				try{$('#excelFile').val('');$('#fakefilepc').val('');}catch(err){}
    			}
    			$('#errExcelMsg').html(mes).show();
	    	}
	    }
	},
	
	viewExcel:function(){
		$('#isView').val(1);
		$('#importFrm').submit();
		return false;
	},
	editrow:function(target){
		var indexRow = CategoryTypeProductCatalog.getRowIndex(target);
	    $('#grid').datagrid('beginEdit', indexRow);
	    var width = 80;
	    $('td[field=status] .datagrid-editable-input').combobox('resize', width);
	    $('td[field=minsf] .datagrid-editable-input').attr('maxlength','10')
	    $('td[field=maxsf] .datagrid-editable-input').attr('maxlength','10');
	    $('td[field=lead] .datagrid-editable-input').attr('maxlength','10')
	    $('td[field=percentage] .datagrid-editable-input').attr('maxlength','10');
	    $('#minsfOld').val($('td[field=minsf] .datagrid-editable-input').val());    
	    $('td[field=calendarD] .datagrid-editable-input').focus(function(){
      	  CategoryTypeProductCatalog.onClickCell(indexRow,$(this).val());      	
      	});
	},
	updateActions:function(index){  
	    $('#grid').datagrid('updateRow',{  
	        index: index,  
	        row:{}  
	    });  
	},
	reset:function(){
		$('#shopCode').val('');
	    $('#productCode').val('');
	},
	getRowIndex:function(target){  
	    var tr = $(target).closest('tr.datagrid-row');  
	    return parseInt(tr.attr('datagrid-row-index'));  
	},  
	saverow:function(target){
		CategoryTypeProductCatalog.reset();    
		var indexRow = CategoryTypeProductCatalog.getRowIndex(target);
		var msg = '';
		$('#errMsg').html('').hide();
		var minsf = $('td[field=minsf] .datagrid-editable-input').val();			
		if(minsf == ""){
			msg = Utils.getMessageOfRequireCheck('stockMin','Tồn kho Min');	
			$('#errMsg').html(msg).show();			
			$('td[field=minsf] .datagrid-editable-input').focus();
			return false;
		}	
		if(minsf.replace(/,/g,'')>9999){
			msg='Tồn kho min không được vượt quá 9999';
			$('#errMsg').html(msg).show();			
			$('td[field=minsf] .datagrid-editable-input').focus();
			return false;
		}
		var maxsf = $('td[field=maxsf] .datagrid-editable-input').val();		
		if(maxsf == ""){
			msg = Utils.getMessageOfRequireCheck('stockMax','Tồn kho Max');	
			$('#errMsg').html(msg).show();			
			$('td[field=maxsf] .datagrid-editable-input').focus();
			return false;
		}
		if(maxsf.replace(/,/g,'')>9999){
			msg='Tồn kho max không được vượt quá 9999';
			$('#errMsg').html(msg).show();			
			$('td[field=maxsf] .datagrid-editable-input').focus();
			return false;
		}
		var lead = $('td[field=lead] .datagrid-editable-input').val();
		if(lead == ""){
			msg = Utils.getMessageOfRequireCheck('dateGo','Ngày đi đường');	
			$('#errMsg').html(msg).show();			
			$('td[field=lead] .datagrid-editable-input').focus();
			return false;
		}
		if(lead.replace(/,/g,'') > 999){
			msg='Ngày đi đường không được vượt quá 999';
			$('#errMsg').html(msg).show();			
			$('td[field=lead] .datagrid-editable-input').focus();
			return false;
		}
		
		var percentage = $('td[field=percentage] .datagrid-editable-input').val();
		if(percentage == ""){
			msg = Utils.getMessageOfRequireCheck('growth','Tỉ lệ tăng trưởng');
			$('#errMsg').html(msg).show();			
			$('td[field=percentage] .datagrid-editable-input').focus();
			return false;
		}
		var minsfOld = $('#minsfOld').val();
		if(parseInt(minsf.trim()) >= parseInt(maxsf.trim()) && parseInt(minsf) != parseInt(minsfOld.trim())){
			msg = "Tồn kho Min phải bé hơn tồn kho Max";
			$('#errMsg').html(msg).show();
			$('td[field=minsf] .datagrid-editable-input').focus();
			return false;
		}

		if(parseInt(minsf.trim()) >= parseInt(maxsf.trim())){
			msg = "Tồn kho Min phải bé hơn tồn kho Max";
			$('#errMsg').html(msg).show();
			$('td[field=maxsf] .datagrid-editable-input').focus();
			return false;
		}
		
		var leadKT = lead.replace(/,/g,'');
		if( parseInt(leadKT.trim())> parseInt(maxsf.trim())){
			msg='Ngày đi đường không được vượt quá tồn kho Max';
			$('#errMsg').html(msg).show();			
			$('td[field=lead] .datagrid-editable-input').focus();
			return false;
		}
		
	    $('#grid').datagrid('endEdit', indexRow);
	    CategoryTypeProductCatalog.indexedit = indexRow;
	    CategoryTypeProductCatalog.change(target);
	    
	},
	cancelrow:function (target){  
		var indexRow = CategoryTypeProductCatalog.getRowIndex(target);
	    $('#grid').datagrid('cancelEdit', indexRow);  
	},
	onClickCell:function(index,value /*index, field, value*/){		
				  if(value != ''){
						$('#T2,#T3,#T4,#T5,#T6,#T7,#CN').removeAttr('checked');
						for(var i = 0; i < value.length; i++) {
							if(value.charAt(i) == 2) {
								$('#T2').attr('checked', 'checked');
							} else if(value.charAt(i) == 3) {
								$('#T3').attr('checked', 'checked');
							} else if(value.charAt(i) == 4) {
								$('#T4').attr('checked', 'checked');
							} else if(value.charAt(i) == 5) {
								$('#T5').attr('checked', 'checked');
							} else if(value.charAt(i) == 6) {
								$('#T6').attr('checked', 'checked');
							} else if(value.charAt(i) == 7) {
								$('#T7').attr('checked', 'checked');
							} else if(value.charAt(i) == 1){
								$('#CN').attr('checked', 'checked');
							}
						}
					}
				$('#weeks').css('visibility','visible');
				$('#popupDate').dialog({ 			
			        title: 'Ngày làm việc',  
			        width: 400,  
			        height: 130,  
			        closed: false,  
			        cache: false,         
			        modal: true,
			        buttons:[
			                 {text: 'Đồng ý',
			                	 handler:function(){
			                		 var num =0;
			                			var value = "0000000";
			                			if($('#T2').is(':checked')){
			                				num++;
			                				value+=($('#T2').val());
			                			}
			                			if($('#T3').is(':checked')){
			                				num++;
			                				value+=($('#T3').val());
			                			}
			                			if($('#T4').is(':checked')){
			                				num++;
			                				value+=($('#T4').val());
			                			}
			                			if($('#T5').is(':checked')){
			                				num++;
			                				value+=($('#T5').val());
			                			}
			                			if($('#T6').is(':checked')){
			                				num++;
			                				value+=($('#T6').val());
			                			}
			                			if($('#T7').is(':checked')){
			                				num++;
			                				value+=($('#T7').val());
			                			}
			                			if($('#CN').is(':checked')){
			                				num++;
			                				value+=($('#CN').val());
			                			}
			                			value = value.substring(num);
			                			if(value == "0000000"){
			                				$('#errMsg1').html('Vui lòng chọn ngày làm việc').show();
			                				var tm = setTimeout(function(){
			                					$('#errMsg1').html('').hide();
			                					clearTimeout(tm);
			                					}, 3000);
			                				return false;
			                			}
			                			$('#dateSale').val(value);
//			                			$('#dateSale1').val(CategoryTypeProductCatalog.parseValueToChar(value));
			                			$('#dateSale1').val(value);
			                			$('td[field=calendarD] .datagrid-editable-input').val($('#dateSale1').val());
			                			$('#popupDate').dialog("close");
			                	 }
			                 },
			                 {text: 'Hủy bỏ',
			                	 handler:function(){
			                			$('#popupDate').dialog('close');
			                			
			                		}
			                 }
			                ],
			                onOpen:function(){
			                	$('td[field=calendarD] .datagrid-editable-input').blur();
		    					var tabindex = -1;
		    					$('.InputTextStyle, select, input, button , li , tr, td, th, label, ul, a, img').each(function () {
		    						if (this.type != 'hidden') {
		    							$(this).attr("tabindex", tabindex);
		    							tabindex -=1;
		    						}
		    					});
		    					var tabindex = 1;
		    		    		$('#popupDate input,#popupDate select,#popupDate button').each(function () {
		    			    		if (this.type != 'hidden') {
		    				    	    $(this).attr("tabindex", tabindex);
		    							tabindex++;
		    			    		}
		    					});
		    				},
		    				onClose:function(){
		    					var tabindex = 1;
		    					$('.InputTextStyle, select, input, button , li , tr, td, th, label, ul, a, img').each(function () {
		    						if (this.type != 'hidden') {
		    							$(this).attr("tabindex", tabindex);
		    							tabindex +=1;
		    						}
		    					});
		    				}
			    });

		return true;
	  },
	  parseValueToChar:function(value){
			var char = "";
			if(value != null || value != undefined){
				var i = value.length;
				for(var j=0;j<i;j++){
					if(value.charAt(j) == 2){
						char += "T2";
					}else if(value.charAt(j) == 3){
						char += "T3";
					}else if(value.charAt(j) == 4){
						char += "T4";
					}else if(value.charAt(j) == 5){
						char += "T5";
					}else if(value.charAt(j) == 6){
						char += "T6";
					}else if(value.charAt(j) == 7){
						char += "T7";
					}else if(value.charAt(j) == 1){
						char += "CN";
					}
					if(char != ""){
						char += " ";
					}
				}
				return char;
			}else{
				return char;
			}
		},
		parseCharToValue:function(char){
			var value = "0000000";
			if(char != null || char != undefined){
				var i = char.trim().split(" ").length;
				var ch = char.trim().split(" ");
				for(var j=0;j<i;j++){
					if(ch[j] == "T2"){
						value += "2";
					}else if(ch[j] == "T3"){
						value += "3";
					}else if(ch[j] == "T4"){
						value += "4";
					}else if(ch[j] == "T5"){
						value += "5";
					}else if(ch[j] == "T6"){
						value += "6";
					}else if(ch[j] == "T7"){
						value += "7";
					}else if(ch[j] == "CN"){
						value += "1";
					}
				}
				return value.substring(i);
			}else{
				return value;
			}
		}		
};
/*
 * END OF FILE - /web/web/resources/scripts/business/catalog/vnm.category-type-product-catalog.js
 */

/*
 * START OF FILE - /web/web/resources/scripts/business/catalog/vnm.category-type-shop-catalog.js
 */
var CategoryTypeCatalog ={
	_xhrSave : null,
	_xhrDel: null,
	lstCategory : null,
	iload:1,
	indexedit:0,
	getGridUrl: function(shopCode,shopName,status,categoryType){
		var url = "/cargo-brand-distributer/search?shopCode=" + encodeChar(shopCode) 
		+ "&shopName=" + encodeChar(shopName) 
		+ "&status=" + status;
		if($.isArray(categoryType)) {
			for(var i = 0; i < categoryType.length; i++) {
				url = url + "&lstcategory=" + categoryType[i];
			}
		}
		return url;
	},
	fillTxtShopCodeByF9 : function(code){
		$('#shopCode').val(Utils.XSSEncode(code)).show();
		$('#common-dialog-search-2-textbox').dialog('close');
		$('#shopCode').focus();
	},
	search: function(){
		$('#errMsg').html('').hide();
		var shopCode = $('#shopCode').val().trim();
		var arrayCategory = new Object();
		var shopName = '';
		var categoryType = $('#category').val();
		CategoryTypeCatalog.lstCategory = categoryType;
		arrayCategory.listCategory = CategoryTypeCatalog.lstCategory;
		var status = $('#status').val();
		var tm = setTimeout(function() {
			$('#shopCode').focus();
		}, 500);
		var url = CategoryTypeCatalog.getGridUrl(shopCode,shopName,status,arrayCategory.listCategory);
		$("#grid").datagrid({url:url,pageNumber:1});
		if(CategoryTypeCatalog.iload==1){
			CategoryTypeCatalog.iload += 3;
		}else{
			CategoryTypeCatalog.iload +=1;
		}
		return false;
	},
	exportExcel: function() {
		var shopCode = $('#shopCode').val().trim();
		var shopName = '';
		var status = $('#status').val();
		var params = new Object();
		params.shopCode = shopCode;
		params.shopName = shopName;
		if(CategoryTypeCatalog.lstCategory!=null){
			params.lstcategory = CategoryTypeCatalog.lstCategory;
		}
		params.status = status;
		
		var url = "/cargo-brand-distributer/export-excel";
//		CommonSearch.exportExcelData(params,url,'errExcelMsg');
		ReportUtils.exportReport(url, params, 'errExcelMsg');
	},
	change: function(){
		var msg = '';
		$('.ErrorMsgStyle').html('').hide();
		var shopObjectType = $('#shopObjectType').val();
		if(shopObjectType!=undefined && shopObjectType!=null 
				&& parseInt(shopObjectType)!= ShopObjectType.VNM && parseInt(shopObjectType)!= ShopObjectType.GT){
			CategoryTypeProductCatalog.search();
			msg = "Không được quyền thực hiện chức năng";
			$('#errMsg').html(msg).show();
			return false;
		}
		msg = Utils.getMessageOfRequireCheck('shopCode1','Mã đơn vị');
		if(msg.length == 0){
			msg = Utils.getMessageOfSpecialCharactersValidate('shopCode1','Mã đơn vị',Utils._CODE);
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfRequireCheck('stockMin','Tồn kho Min');
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfSpecialCharactersValidate('stockMin','Tồn kho Min');
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfInvaildNumber('stockMin', 'Tồn kho Min');
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfNegativeNumberCheck('stockMin','Tồn kho Min');
		}
		if(msg.length == 0 && $('#stockMin').val().replace(/,/g,'')>9999){
			msg='Tồn kho min không được vượt quá 9999';
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfRequireCheck('stockMax','Tồn kho Max');
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfSpecialCharactersValidate('stockMax','Tồn kho Max');
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfInvaildNumber('stockMax', 'Tồn kho Max');
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfNegativeNumberCheck('stockMax','Tồn kho Max');
		}
		if(msg.length == 0 && $('#stockMax').val().replace(/,/g,'')>9999){
			msg='Tồn kho max không được vượt quá 9999';
		}
		if(msg.length == 0){
			if($('#stockMin').val().trim() != '' && $('#stockMax').val().trim() != ''){
				if(parseInt($('#stockMin').val().trim()) >= parseInt($('#stockMax').val().trim())){
					msg = "Tồn kho Min phải bé hơn tồn kho Max";
					$('#errMsg').html(msg).show();
					$('#stockMin').focus();
					return false;
				}
			}
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfRequireCheck('dateGo','Ngày đi đường');
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfSpecialCharactersValidate('dateGo','Ngày đi đường');
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfInvaildNumber('dateGo', 'Ngày đi đường');
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfNegativeNumberCheck('dateGo','Ngày đi đường');
		}
		if(msg.length == 0 && $('#dateGo').val().replace(/,/g,'')>999){
			msg='Ngày đi đường không được vượt quá 999';
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfRequireCheck('dateSale','Ngày bán hàng');
		}
		if(msg.length == 0){
			if(CategoryTypeCatalog.parseCharToValue($('#dateSale').val().trim()) == '0000000'){
				msg = 'Bạn chưa nhập giá trị cho trường Ngày bán hàng. Yêu cầu nhập giá trị';
				//$('#dateSale').focus();
			}
		}
		if(msg.length > 0){
			$('#errMsg').html(msg).show();
			return false;
		}
		var dataModel = new Object();
		dataModel.categoryId = $('#categoryId').val();
		dataModel.shopCode = $('#shopCode1').val().trim();
		dataModel.categoryType = $('#categoryTypeId').val();
		dataModel.stockMin = $('#stockMin').val().replace(/,/g,'').trim();
		dataModel.stockMax = $('#stockMax').val().replace(/,/g,'').trim();
		dataModel.dateGo = $('#dateGo').val().replace(/,/g,'').trim();
		dataModel.dateSale = $('#dateSale').val().trim();
		dataModel.growth = $('#growth').val().trim();
		dataModel.status = $('#status1').val();
		Utils.addOrSaveData(dataModel, "/cargo-brand-distributer/save", CategoryTypeCatalog._xhrSave, null,function(data){			
			if(data.error == false){
				$("#grid").datagrid('reload');
				return true;
			}
		},null,null,null,null,function(data){
			if(data.error == true){
				//$('#shopCode').focus();
			}
		});
		$('#grid').datagrid('beginEdit', CategoryTypeCatalog.indexedit);
		return true;
	},
	deleteRow: function(categoryId){
		$('#errMsg').html('').hide();
		var dataModel = new Object();
		dataModel.categoryId = categoryId;	
		Utils.deleteSelectRowOnGrid(dataModel, 'ngành hàng bán cho đơn vị', "/cargo-brand-distributer/remove", CategoryTypeCatalog._xhrDel, null, null);
		return false;		
	},
	mapValue:function(){
		var num =0;
		var value = "0000000";
		if($('#monday').is(':checked')){
			num++;
			value+=($('#monday').val());
		}
		if($('#tuesday').is(':checked')){
			num++;
			value+=($('#tuesday').val());
		}
		if($('#wednesday').is(':checked')){
			num++;
			value+=($('#wednesday').val());
		}
		if($('#thursday').is(':checked')){
			num++;
			value+=($('#thursday').val());
		}
		if($('#friday').is(':checked')){
			num++;
			value+=($('#friday').val());
		}
		if($('#saturday').is(':checked')){
			num++;
			value+=($('#saturday').val());
		}
		if($('#sunday').is(':checked')){
			num++;
			value+=($('#sunday').val());
		}
		value = value.substring(num);
		if(value == "0000000"){
			$('#errMsg1').html('Vui lòng chọn ngày làm việc').show();
			var tm = setTimeout(function(){
				$('#errMsg1').html('').hide();
				clearTimeout(tm); }, 3000);
			return false;
		}
		$('#dateSale').val(value);
		//$('#dateSale1').val(CategoryTypeCatalog.parseValueToChar(value));
		$('#dateSale1').val(value);
		$(".datagrid-btable td[field='calendarD'] table td input[type=text]").val($('#dateSale1').val());
		//$('.ToolTipCalandar').hide();
		$("#popup1").dialog("close");
	},
	parseValueToChar:function(value){
		var char = "";
		if(value != null || value != undefined){
			var i = value.length;
			for(var j=0;j<i;j++){
				if(value.charAt(j) == 2){
					char += "T2";
				}else if(value.charAt(j) == 3){
					char += "T3";
				}else if(value.charAt(j) == 4){
					char += "T4";
				}else if(value.charAt(j) == 5){
					char += "T5";
				}else if(value.charAt(j) == 6){
					char += "T6";
				}else if(value.charAt(j) == 7){
					char += "T7";
				}else if(value.charAt(j) == 1){
					char += "CN";
				}
				if(char != ""){
					char += " ";
				}
			}
			return char;
		}else{
			return char;
		}
	},
	parseCharToValue:function(char){
		var value = "0000000";
		if(char != null || char != undefined){
			var i = char.trim().split(" ").length;
			var ch = char.trim().split(" ");
			for(var j=0;j<i;j++){
				if(ch[j] == "T2"){
					value += "2";
				}else if(ch[j] == "T3"){
					value += "3";
				}else if(ch[j] == "T4"){
					value += "4";
				}else if(ch[j] == "T5"){
					value += "5";
				}else if(ch[j] == "T6"){
					value += "6";
				}else if(ch[j] == "T7"){
					value += "7";
				}else if(ch[j] == "CN"){
					value += "1";
				}
			}
			return value.substring(i);
		}else{
			return value;
		}
	},
	ViewFileUpload: function(typeView){
		$('#importFrm').submit();
	}
};
/*
 * END OF FILE - /web/web/resources/scripts/business/catalog/vnm.category-type-shop-catalog.js
 */

/*
 * START OF FILE - /web/web/resources/scripts/business/catalog/vnm.customer-catalog.js
 */
var CustomerCatalog = {
	me: this,
	_xhrSave : null,
	_xhrDel : null,
	_zoom : 18,
	_DEF_ZOOM: 10,
	_curSaleLevel : null,
	latbk : '',
	lngbk : '',
	BUTTON_TYPE_CANCEL_OPERATION: 0,
	BUTTON_TYPE_DELETE: 1,
	BUTTON_TYPE_CONFIRM_LATER: 2,
	BUTTON_TYPE_CREATE: 3,
	BUTTON_TYPE_DENY: 4,
	BUTTON_TYPE_CREATE_WITH_DUPLICATE_CUSTOMER_CODE: 5,
	STATUS_STOPPED: 0,
	STATUS_RUNNING: 1,
	STATUS_WAITING: 2,
	STATUS_REJECTED: 3,
	selectedDuplicateCustomerCode: '',
	duplicateWithCustomers: null,
	search : function() {
		$('#errMsg').html('').hide();
		var searchParam = CustomerCatalog.getSearchParam();
		$("#customerDatagrid").datagrid('load', searchParam);
		return false;
	},
	DUPLICATE_ADDRESS: 1,
	/**
	 * lay thong so tim kiem khach hang
	 * @author tuannd20
	 * @return {Object} doi tuong chua thong tin tim kiem khach hang
	 * @since 04/09/2015
	 */
	getSearchParam: function() {
		var customerCode = $('#customerCode').val().trim();
		var customerName = $('#customerName').val().trim();
		var customerType = $('#customerType').combobox('getValue').trim().split(' - ')[0];
		var address = $('#address').val().trim();
		var phone = $('#phone').val().trim();
		var status = $('#status').val();
		var shopCode = $('#txtShopCode').val().trim();
		var tansuat = $('#tansuat').val().trim();
		$('#customerCode').focus(); 
		return {
			customerCode: customerCode,
			customerName: customerName, 
			customerType: customerType, 
			address: address, 
			phone: phone, 
			status: status, 
			shopCode: shopCode, 
			tansuat: tansuat
		}
	},
	hideAllTab : function() {
		$('#infoTab').removeClass('Active');
		$('#propertyTab').removeClass('Active');
		$('#catInfoTab').removeClass('Active');
		$('#infoTabContainer').hide();
//		$('#propertyTabContainer').hide();
		$('#catInfoTabContainer').hide();
	},
	showTab1 : function() {
		CustomerCatalog.hideAllTab();
		$('#infoTab').addClass('Active');
		$('#infoTabContainer').show();
	},
	showTab2 : function() {
		if($('#id').val().length == 0) {
			return;
		}
		CustomerCatalog.hideAllTab();
		$('#propertyTab').addClass('Active');
		$('#propertyTabContainer').show();
	},
	showTab3 : function() {
		if($('#id').val().length == 0) {
			return;
		}
		CustomerCatalog.hideAllTab();
		$('#catInfoTab').addClass('Active');
		$('#catInfoTabContainer').show();
		var selId = $('#selId').val();
		$('#grid').datagrid({
			url : '/catalog_customer_mng/search-level-cat?id='+ selId,
			autoRowHeight : true,
			rownumbers : true, 
			pagination:true,
			rowNum : 10,
			fitColumns:true,
			pageList  : [10,20,30],
			scrollbarSize:0,
			width: $('#gridContainer').width(),
			autoWidth: true,
		    columns:[[	        
			    {field:'saleLevelCat.cat.productInfoName', title: catalog_customer_cat, width: 100, sortable:false,resizable:true , align: 'left',
			    	formatter:function(value,object,opts){
			    		return Utils.XSSEncode(object.saleLevelCat.cat.productInfoName);
			    	}},
			    {field:'saleLevelCat.saleLevelName', title: catalog_customer_amount_revenue_limit, width:200,sortable:false,resizable:true , align: 'left',
			    		formatter:function(value,object,opts){
				    		return Utils.XSSEncode(object.saleLevelCat.saleLevelName);
				 }},
			    {field:'saleLevelCat.fromAmount', title: catalog_customer_amount_revenue_from, width: 100, align: 'right', sortable:false,resizable:true,
					 formatter:function(value,object,opts){
				    		return object.saleLevelCat.fromAmount;}},
			    {field:'saleLevelCat.toAmount', title: catalog_customer_amount_revenue_to, width: 80, align: 'right', sortable:false,resizable:true,
				    			formatter:function(value,object,opts){ return object.saleLevelCat.toAmount;}},
			    {field:'edit', title:'', width: 20, align: 'center',sortable:false,resizable:true,
			    	formatter : function(val,__row,idx) {
	            		var catId = __row.id;
	            		return '<a id="group_insert_gr_delete_nhc'+catId+'" href=\"javascript:void(0)\" onclick=\"CustomerCatalog.deleteLevelCatRow('+ catId+'\);\"><img style="width:14px;height:15px;border:0px" src=\"/resources/images/icon_delete.png\"></a>';
	            	}
			    },
			    {field:'id',hidden: true}
		    ]],	    
		    onLoadSuccess :function(data){	    	
		    	$('.datagrid-header-rownumber').html(jsp_common_numerical_order);	    	
		    	updateRownumWidthForJqGrid('.easyui-dialog');
		    	 
		    	var arrEdit = $('#catInfoTabContainer td[field="edit"]');
				if (arrEdit != undefined && arrEdit != null && arrEdit.length > 0) {
				  for (var i = 0, size = arrEdit.length; i < size; i++) {
				  	$(arrEdit[i]).prop("id", "group_insert_table_td_edit_" + i).addClass("cmsiscontrol");
				  }
				}
				Utils.functionAccessFillControl('catInfoTabContainer', function(data){
					//Xu ly cac su kien lien quan den cac control phan quyen
					var arrTmpLength =  $('#catInfoTabContainer td[id^="group_insert_table_td_edit_"]').length;
					var invisibleLenght = $('.isCMSInvisible[id^="group_insert_table_td_edit_"]').length;
					if (arrTmpLength > 0 && arrTmpLength != invisibleLenght) {
						$('#grid').datagrid("showColumn", "edit");
					} else {
						$('#grid').datagrid("hideColumn", "edit");
					}
				});
		    	$(window).resize();    		 
		    }
		});
	},
	getSelectedCustomer : function(id) {
		if (id != undefined && id != null) {
			location.href = '/catalog_customer_mng/change?id=' + id;
		} else {
			location.href = '/catalog_customer_mng/change';
		}
		return false;
	},
	addressChange:function(){
		var number=$('#addressNumber').val().trim();
		var street=$('#street').val().trim();
		if(number!='' && street!=''){
			$('#address').val(number+', '+street);
		}else if(number!=''){
			$('#address').val(number);
		}else if(street!=''){
			$('#address').val(street);
		}
	},
	saveInfo : function() {
		var msg = '';
		$('.SuccessMsgStyle').html('').hide();
		$('.ErrorMsgStyle').html('').hide();
		if (msg.length == 0) {
			msg = Utils.getMessageOfRequireCheck('customerName', catalog_customer_name);
		}
		if (msg.length == 0) {
			msg = Utils.getMessageOfSpecialCharactersValidate('customerName', catalog_customer_name);
		}
		if (msg.length == 0) {
			msg = Utils.getMessageOfInvalidMaxLength('idNumber', 'CMND');
		}
		if (msg.length == 0 && $('#email').val().length > 0) {
			msg = Utils.getMessageOfInvalidEmailFormat('email', catalog_customer_email);
		}
//		if (msg.length == 0 && $('#phone').val().trim()!='' && $('#phone').val().trim()[0]!='0') {
//			msg = 'Số điện thoại bàn phải bắt đầu là 0';
//			$('#phone').focus();
//		}
		if (msg.length == 0 && $('#phone').val().trim()!='' 
			&& ($('#phone').val().trim().length<9 || $('#phone').val().trim().length>12)) {
			msg = 'Số điện thoại bàn phải từ 9 đến 12 chữ số.';
			$('#phone').focus();
		}
//		if (msg.length == 0 && $('#mobilePhone').val().trim()!='' && $('#mobilePhone').val().trim()[0]!='0') {
//			msg = 'Số di động phải bắt đầu là 0';
//			$('#mobilePhone').focus();
//		}
		if (msg.length == 0 && $('#mobilePhone').val().trim()!='' 
			&& ($('#mobilePhone').val().trim().length<9 || $('#mobilePhone').val().trim().length>12)) {
			msg = 'Số di động phải từ 9 đến 12 chữ số.';
			$('#mobilePhone').focus();
		}
		if (msg.length == 0) {
			msg = Utils.getMessageOfRequireCheck('status', jsp_common_status);
		}
		
//		if(msg.length == 0){
//			var area = $('#areaId').val().trim();
//			if (area == null || area == -2) {
//				msg = 'Địa bàn là bắt buộc. Xin vui lòng chọn địa bàn';
//				$('#areaTree').focus();
//			}
//		}
		
		if (msg.length == 0) {
			var prov = $('#province').combobox('getValue');
			if (prov == null || prov.length == 0 || prov == '-2') {
				msg = catalog_customer_province_require;
			}
			$('#province').next().find('.combo-text').focus();
		}
		
		if (msg.length == 0) {
			var prov = $('#district').combobox('getValue');
			if (prov == null || prov.length == 0 || prov == '-2') {
				msg = catalog_customer_district_require;
			}
			$('#district').next().find('.combo-text').focus();
		}
		
		if (msg.length == 0) {
			var prov = $('#precinct').combobox('getValue');
			if (prov == null || prov.length == 0 || prov == '-2') {
				msg = catalog_customer_area_require;
			}
			$('#precinct').next().find('.combo-text').focus();
		}
		
//		if (msg.length == 0) {
//			if($('#areaId').val() == -2){
//				msg = 'Bạn chưa nhập giá trị cho trường Địa bàn.';
//			}
//		}
//		if (msg.length == 0) {
//			msg = Utils.getMessageOfSpecialCharactersValidate('address', jsp_common_address, Utils._ADDRESS);
//		}
//		if (msg.length == 0) {
//			msg = Utils.getMessageOfRequireCheck('address', jsp_common_address);
//		}
		if (msg.length == 0) {
			msg = Utils.getMessageOfSpecialCharactersValidate('street', catalog_customer_street_label, Utils._ADDRESS);
		}
		if (msg.length == 0) {
			msg = Utils.getMessageOfSpecialCharactersValidate('addressNumber', catalog_customer_house_number_label, Utils._ADDRESS);
		}
		if (msg.length == 0) {
			msg = Utils.getMessageOfInvalidMaxLength('mobilePhone', catalog_customer_mobile_phone);
		}
		if (msg.length == 0) {
			msg = Utils.getMessageOfInvalidMaxLength('phone', catalog_customer_telephone);
		}
		if (msg.length == 0) {
			msg = Utils.getMessageOfInvalidMaxLength('email', catalog_customer_email);
		}

		if (msg.length > 0) {
			$('#errMsg').html(msg).show();
			return false;
		}
		var customerName = $('#customerName').val().trim();
		//var provinceCode = $('#provinceCode').val().trim();
		//var districtCode = $('#districtCode').val().trim();
		//var wardCode = $('#wardCode').val().trim();
		//var address = $('#address').val().trim();
		var street = $('#street').val().trim();
		var phone = $('#phone').val().trim();
		var mobilePhone = $('#mobilePhone').val().trim();
		var status = $('#status').val().trim();
		var idNumber = $('#idNumber').val().trim();
		var contact = $('#contact').val().trim();
		var email = $('#email').val().trim();
		var addressNumber = $('#addressNumber').val().trim();
		var areaId = $('#areaId').val();
		var lat = $('#lat').val();
		var lng = $('#lng').val();
		
		var dataModel = new Object();
		dataModel.id = $('#id').val().trim();
		dataModel.customerName = customerName;
		dataModel.contact = contact;
		dataModel.idNumber = idNumber;
		dataModel.phone = phone;
		dataModel.mobilePhone = mobilePhone;
		dataModel.email = email;
		dataModel.status = status;
		dataModel.street = street;
		//dataModel.address = address;
		dataModel.areaId = areaId;
		dataModel.addressNumber = addressNumber;
		dataModel.lat = lat;
		dataModel.lng = lng;
		dataModel.shopCode = $('#txtShopCode').val().trim();
		var b = ($("#id").val().trim().length == 0);
		if(!CustomerCatalog.saveProperty(dataModel)) {
			return;
		}
		
		if(dataModel != null) {
			$.ajax({
				type : "POST",
				url : "/catalog_customer_mng/check-sale-order",
				data :($.param(dataModel, true)),
				dataType : "json",
				success : function(data) {				
					if(data.error){
						$.messager.confirm("Thông báo", data.errMsg, function(r){
							Utils.saveData(dataModel, "/catalog_customer_mng/save-info",CustomerCatalog._xhrSave, null, function(data) {
								$('#id').val(data.id);
								$('#selId').val(data.id);
								$('#shortCode').val(Utils.XSSEncode(data.code));
								setTimeout(function() {
									if (b) {
										window.location.href = '/catalog_customer_mng/change?id='+data.id;
									} else {
										CustomerCatalog.showTab3();
									}
								}, 1000);
							}, null, ' #infoTabContainer ');
						});	
					} else {
						Utils.addOrSaveData(dataModel, "/catalog_customer_mng/save-info",CustomerCatalog._xhrSave, null, function(data) {
							$('#id').val(data.id);
							$('#selId').val(data.id);
							$('#shortCode').val(Utils.XSSEncode(data.code));
							setTimeout(function() {
								if (b) {
									window.location.href = '/catalog_customer_mng/change?id='+data.id;
								} else {
									CustomerCatalog.showTab3();
								}
							}, 1000);
						}, null, ' #infoTabContainer ')
					}
				},
				error:function(XMLHttpRequest, textStatus, errorThrown) {
					$('#divOverlay').hide();
//					window.location.href = '/home';
				}
			});
		}
		
		return false;
	},

	/**
	 * Xoa vi tri KH
	 * @author vuongmq
	 * @since 04/09/2015 
	 */
	deleteCustomerPosition: function(){
		$('.SuccessMsgStyle').html('').hide();
		$('.ErrorMsgStyle').html('').hide();
		var dataModel = new Object();
		dataModel.id = $('#id').val().trim();
		dataModel.lat = $('#lat').val();
		dataModel.lng = $('#lng').val();
		Utils.addOrSaveData(dataModel, "/catalog_customer_mng/delete-customer-position", CustomerCatalog._xhrSave, 'errMsgMap', function(data) {
			$('#lat').val('');
    		$('#lng').val('');
    		ViettelMap.clearOverlays();
    		ViettelMap._listOverlay = new Array(); 
    		if (ViettelMap._marker != null) {
            	ViettelMap._marker.setMap(null);
            }
            $('#successMsgMap').html(jsp_common_delete_success).show();
			setTimeout(function() {
				$('#successMsgMap').html('').hide();
			}, 2000);
		}, null, null, true, null, null, true);
		return false;
	},

	saveProperty : function(dataModel) {
		var parentDiv = '#propertyTabContainer ';
		var msg = '';
		$(parentDiv+'#errMsgProperty').html('').hide();
		if (msg.length == 0) {
			msg = Utils.getMessageOfRequireCheck('customerType', catalog_customer_type, true);
		}
		
		if (msg.length > 0) {
			$(parentDiv+'#errMsg').html(msg).show();
			return false;
		}
		var customerType = $('#customerType').combobox('getValue');
		var deliveryCode = $('#deliveryCode').combobox('getValue');
		var cashierCode = $('#cashierCode').combobox('getValue');
		
//		var customerType = CustomerCatalog.editComboboxX("#customerType");
//		var deliveryCode = CustomerCatalog.editComboboxX("#deliveryCode");
//		var cashierCode = CustomerCatalog.editComboboxX("#cashierCode");
		var applyDebitLimited = $('#CheckapplyDebitLimited').is(":checked") ? 1 : 0;
		if(applyDebitLimited == 1) {
			if($('#maxDebit').val().trim()=='' && $('#debitDate').val().trim()=='' ){
				msg = Utils.getMessageOfRequireCheck('maxDebit', catalog_customer_debit_amount);
			}
			if (msg.length > 0) {
				$("#errMsgProperty").html(msg).show();
				return false;
			}
		}
		var maxDebit = Utils.returnMoneyValue($('#maxDebit').val().trim());
		var debitDate = $('#debitDate').val().trim();
		if(maxDebit.length>0 && isNaN(maxDebit)){
			msg = catalog_customer_debit_amount_must_number;
			$('#maxDebit').focus();
		}
		if(debitDate.length>0 && isNaN(debitDate)){
			msg = catalog_customer_debit_limit_must_number;
			$('#debitDate').focus();
		}
		
		if (msg.length == 0) {
			msg = Utils.getMessageOfInvalidMaxLength('invoiceConpanyName', jsp_common_shop_name);
		}
		if (msg.length == 0) {
			msg = Utils.getMessageOfInvalidMaxLength('invoiceOutletName', 'Người đại diện');
		}
		if (msg.length == 0) {
			msg = Utils.getMessageOfInvalidMaxLength('custDeliveryAddr', 'Địa chỉ hóa đơn');
		}
		if (msg.length == 0) {
			msg = Utils.getMessageOfInvalidMaxLength('invoiceTax', catalog_customer_tax_no);
		}
		if (msg.length == 0) {
			msg = Utils.getMessageOfInvalidMaxLength('invoiceNumberAccount', 'Tài khoản');
		}
		if (msg.length == 0) {
			msg = Utils.getMessageOfInvalidMaxLength('invoiceNameBank', 'Ngân hàng');
		}
		
		if (msg.length > 0) {
			$(parentDiv+'#errMsgProperty').html(msg).show();
			return false;
		}
		var invoiceConpanyName = $('#invoiceConpanyName').val().trim();
		var invoiceOutletName = $('#invoiceOutletName').val().trim();
		var custDeliveryAddr = $('#custDeliveryAddr').val().trim();
		var invoiceTax = $('#invoiceTax').val().trim();
		var invoiceNumberAccount = $('#invoiceNumberAccount').val().trim();
		var invoiceNameBank = $('#invoiceNameBank').val().trim();
		var invoiceNameBranchBank = $('#invoiceNameBranchBank').val().trim();
		var bankAccountOwner = $('#bankAccountOwner').val().trim();
		var isVat = $('#cbxIsVat').is(":checked") ? 1 : 0;
		var birthDay = $('#birthDay').val().trim();
		var tansuat = $('#tansuat').val().trim();
		var startDate = $('#startDate').val().trim();
		var endDate = $('#endDate').val().trim();
		var routingId = $('#cbRouting').combobox('getValue');
		var salePosition = $('#salePosition').combobox('getValue');
		
		dataModel.id = $('#id').val().trim();
		dataModel.customerType = customerType;
		dataModel.deliveryCode = deliveryCode;
		dataModel.cashierCode = cashierCode;
		dataModel.applyDebitLimited = applyDebitLimited;
		dataModel.maxDebit = maxDebit;
		dataModel.debitDate = debitDate;
		dataModel.invoiceConpanyName= invoiceConpanyName;
		dataModel.invoiceOutletName = invoiceOutletName;
		dataModel.custDeliveryAddr = custDeliveryAddr;
		dataModel.invoiceTax = invoiceTax;
		dataModel.invoiceNumberAccount = invoiceNumberAccount;
		dataModel.invoiceNameBank = invoiceNameBank;
		dataModel.invoiceNameBranchBank = invoiceNameBranchBank;
		dataModel.bankAccountOwner = bankAccountOwner;
		dataModel.isVat = isVat;
		dataModel.birthDayStr = birthDay;
		dataModel.tansuat = tansuat;
		dataModel.startDateStr = startDate;
		dataModel.endDateStr = endDate;
		dataModel.routingId = routingId;
		dataModel.customerSalePosition = salePosition;
		
		if(!Utils.validateAttributeData(dataModel, '#errMsgProperty', '#propertyTabContainer ')) {
			return false;
		}
//		Utils.addOrSaveData(dataModel, "/catalog_customer_mng/save-property",
//				CustomerCatalog._xhrSave, 'errMsgProperty', function(data) {
//				}, null, ' #propertyTabContainer ');
		return true;
	},
	saveInvoice : function() {
		var msg = '';
		$('#errMsgBill').html('').hide();
		msg = Utils.getMessageOfRequireCheck('code', catalog_customer_code);
		if (msg.length == 0) {
			msg = Utils.getMessageOfSpecialCharactersValidate('code', catalog_customer_code,
					Utils._CODE);
		}
		if (msg.length == 0) {
			msg = Utils.getMessageOfRequireCheck('name', catalog_customer_name);
		}
		if (msg.length == 0) {
			msg = Utils.getMessageOfSpecialCharactersValidate('name', catalog_customer_name);
		}
		if (msg.length == 0) {
			msg = Utils.getMessageOfRequireCheck('invoiceConpanyName',
					jsp_common_shop_name);
		}
		if (msg.length == 0) {
			msg = Utils.getMessageOfSpecialCharactersValidate(
					'invoiceConpanyName', jsp_common_shop_name);
		}
		if (msg.length == 0) {
			msg = Utils.getMessageOfRequireCheck('invoiceOutletName',
					catalog_customer_full_name);
		}
		if (msg.length == 0) {
			msg = Utils.getMessageOfSpecialCharactersValidate(
					'invoiceOutletName', catalog_customer_full_name);
		}
		if (msg.length == 0) {
			msg = Utils.getMessageOfRequireCheck('custDeliveryAddr', jsp_common_address);
		}
		if (msg.length == 0) {
			msg = Utils.getMessageOfSpecialCharactersValidate(
					'custDeliveryAddr', jsp_common_address);
		}
		if (msg.length == 0 && $('#invoiceNameBank').val().length > 0) {
			msg = Utils.getMessageOfSpecialCharactersValidate(
					'invoiceNameBank', common_bank_name);
		}
		if (msg.length == 0 && $('#invoiceTax').val().trim().length > 0
				&& isNaN($('#invoiceTax').val().trim())) {
			msg = format(msgErr_number, catalog_customer_tax_no);
			$('#invoiceTax').focus();
		}
		if (msg.length == 0 && $('#invoiceNumberAccount').val().trim().length > 0
				&& isNaN($('#invoiceNumberAccount').val().trim())) {
			msg = format(msgErr_number, 'Số TK');
			$('#invoiceNumberAccount').focus();
		}

		if (msg.length > 0) {
			$('#errMsgBill').html(msg).show();
			return false;
		}

		var dataModel = new Object();
		dataModel.id = $('#selId').val().trim();
		dataModel.invoiceConpanyName = $('#invoiceConpanyName').val().trim();
		dataModel.invoiceOutletName = $('#invoiceOutletName').val().trim();
		dataModel.custDeliveryAddr = $('#custDeliveryAddr').val().trim();
		dataModel.invoiceTax = $('#invoiceTax').val().trim();
		dataModel.paymentType = $('#paymentType').val();
		dataModel.invoiceNumberAccount = $('#invoiceNumberAccount').val().trim();
		dataModel.invoiceNameBank = $('#invoiceNameBank').val().trim();
		Utils.addOrSaveData(dataModel, "/catalog_customer_mng/save-invoice",
				CustomerCatalog._xhrSave, 'errMsgBill', function(data) {
					if (data.error) {
						$('#errMsgBill').html(data.errMsg).show();
					} else {
						CustomerCatalog.showTab3();
						showSuccessMsg('successMsgBill', data);
					}
				});
		return false;
	},
	getSaleOrderGridUrl : function(id, shopCode, fromDate, toDate) {
		return "/catalog_customer_mng/search-sale-order?id=" + encodeChar(id)
				+ "&shopCode=" + encodeChar(shopCode) + "&fromDate="
				+ encodeChar(fromDate) + "&toDate=" + encodeChar(toDate);
	},
	searchSaleOrder : function() {
		$('#errMsgSaleOrder').html('').hide();
		var id = $('#selId').val();
		var shopCode = $('#shopCode').val();
		var fromDate = $('#saleOrderFDate').val();
		var toDate = $('#saleOrderTDate').val();
		var url = CustomerCatalog.getSaleOrderGridUrl(id, shopCode, fromDate, toDate);
		$("#saleOrderGrid").setGridParam({
			url : url,
			page : 1
		}).trigger("reloadGrid");
		return false;
	},
	saveLevelCat : function() {
		var msg = '';
		$('#errMsgLevel').html('').hide();
		msg = Utils.getMessageOfRequireCheck('cat', catalog_customer_cat, true);
		if (msg.length == 0) {
			msg = Utils.getMessageOfRequireCheck('saleLevelCat', catalog_customer_amount_revenue_limit, true);
		}

		if (msg.length > 0) {
			$('#errMsgLevel').html(msg).show();
			return false;
		}
		var dataModel = new Object();
		dataModel.id = $('#selId').val().trim();
		dataModel.saleLevelCat = $('#saleLevelCat').val().trim();
		dataModel.saleLevelCatId = $('#saleLevelCatId').val().trim();
		Utils.addOrSaveRowOnGrid(dataModel, "/catalog_customer_mng/save-level-cat", CustomerCatalog._xhrSave, 'catGrid', 'errMsgLevel', function(data) {
			if (!data.error) {
				$('#saleLevelCatId').val(0);
//				$("#catGrid").trigger("reloadGrid");
				$('#grid').datagrid('reload');
				CustomerCatalog._curSaleLevel = null;
				showSuccessMsg('successMsgLevel', data);
				// sontt19:
				$("#cat").val('-2');
				$('#cat').change();
				$('#cat').attr('disabled', false);
				$('#cat').parent().removeClass('BoxDisSelect');
				$("#saleLevelCat").val('-2');
				$('#saleLevelCat').change();
				$('#group_insert_btnAddCat').show();
				$('#btnEditCat').hide();
				$('#btnCancelCat').hide();
				// end-sontt19
			}
		}, 'catLoading');
		return false;
	},
	getSelectedLevelCatRow : function(rowId) {
		var catId = $("#catGrid").jqGrid('getCell', rowId,'saleLevelCat.cat.id');
		var saleLevelId = $("#catGrid").jqGrid('getCell', rowId,'saleLevelCat.id');
		var id = $("#catGrid").jqGrid('getCell', rowId, 'id');
		CustomerCatalog._curSaleLevel = saleLevelId;
		$('#saleLevelCatId').val(id);
		$('#cat').val(catId);
		$('#hidCatId').val(catId);
		// disabled('cat');
		// $('#cat').change();
		$('#group_insert_btnAddCat').hide();
		$('#btnEditCat').show();
		$('#btnCancelCat').show();
		CustomerCatalog.loadedCat(catId);
		// sontt19:
		$('#cat').attr('disabled', true);
		$('#cat').parent().addClass('BoxDisSelect');
		// end-sontt19
		return false;
	},
	loadedCat: function(catId) {
		$.getJSON('/rest/catalog/customer-cat/list/' + catId + '.json', function(data) {
			var arrHtml = new Array();
			arrHtml.push('<option value="-2">-- ' + Utils.XSSEncode(catalog_customer_cat_select) + ' --</option>');
			if (data != null && data.length > 0) {
				for ( var i = 0; i < data.length; i++) {
					arrHtml.push('<option value="' + data[i].value + '">' + Utils.XSSEncode(data[i].name) + '</option>');
				}
			}
			$('#cat').html(arrHtml.join(""));
			$('#cat').val(catId);
			$('#cat').change();
		});
	},
	deleteLevelCatRow : function(id) {
		$('#errMsgLevel').html('').hide();
		var dataModel = new Object();
		dataModel.saleLevelCatId = id;
		Utils.deleteSelectRowOnGrid(dataModel, catalog_customer_sale_cat,
				'/catalog_customer_mng/delete-level-cat', CustomerCatalog._xhrDel,
				'catGrid', 'errMsgCat', function(data) {
					showSuccessMsg('successMsgLevel', data,null,3000,jsp_common_delete_success);
					$('#grid').datagrid('reload');
				}, 'catLoading');
		return false;
	},
	activeOthersTab : function() {
		$('#tabActive2').removeClass('Disable');
		$('#tabActive3').removeClass('Disable');
		$('#tabActive4').removeClass('Disable');
		$('#tabActive5').removeClass('Disable');

		$('#tab2').bind('click', CustomerCatalog.showTab2);
		$('#tab3').bind('click', CustomerCatalog.showTab3);
		$('#tab4').bind('click', CustomerCatalog.showTab4);
		$('#tab5').bind('click', CustomerCatalog.showTab5);

		return false;
	},
	activeTabImage : function() {
		$('#tabActive6').removeClass('Disable');
		$('#tab6').bind('click', CustomerCatalog.showTab6);
	},
	searchActionLog : function() {
		$('#errMsgActionLog').html('').hide();
		var id = $('#selId').val().trim();
		// var shopCode = $('#changedInfoShopCode').val();
		var fromDate = $('#fromDate').val().trim();
		var toDate = $('#toDate').val().trim();
		var msg='';
		if(!Utils.compareDate(fromDate, toDate)){
			msg = msgErr_fromdate_greater_todate;		
			$('#fromDate').focus();
		}
		if(msg.length > 0){
			$('#errMsgActionLog').html(msg).show();
			return false;
		}
		var url = '/catalog_customer_mng/search-info-change?id=' + id
				+ '&fromDate=' + fromDate + '&toDate=' + toDate;
		$("#infoChangeGrid").setGridParam({
			url : url,
			page : 1
		}).trigger("reloadGrid");
	},
	viewDetailActionLog : function(id) {
		$('#detailActionLogDiv').show();
		var url = CustomerCatalog.getGridUrlDetailAction(id);
		if ($('#gbox_infoChangeDetailGrid').html() != null
				&& $('#gbox_infoChangeDetailGrid').html().trim().length > 0) {
			$("#infoChangeDetailGrid").setGridParam({
				url : url,
				page : 1
			}).trigger("reloadGrid");
		} else {
			$("#infoChangeDetailGrid").jqGrid({
				url : CustomerCatalog.getGridUrlDetailAction(id),
				colModel : [ {
					name : 'columnName',
					index : 'columnName',
					label : 'Thông tin thay đổi',
					sortable : false,
					resizable : false,
					align : 'left'
				}, {
					name : 'oldValue',
					index : 'oldValue',
					label : 'Giá trị cũ',
					width : 100,
					sortable : false,
					resizable : false,
					align : 'right'
				}, {
					name : 'newValue',
					index : 'newValue',
					label : 'Giá trị mới',
					width : 100,
					sortable : false,
					resizable : false,
					align : 'right'
				}, {
					name : 'actionDate',
					index : 'actionDate',
					label : 'Ngày thay đổi',
					width : 80,
					align : 'center',
					sortable : false,
					resizable : false,
					formatter : 'date',
					formatoptions : {
						srcformat : 'Y-m-d',
						newformat : 'd/m/Y'
					}
				} ],
				pager : '#infoChangeDetailPager',
				width : ($('#infoChangeDetailContainer').width()),
				gridComplete : function() {
					$('#jqgh_infoChangeDetailGrid_rn').html(jsp_common_numerical_order);
					updateRownumWidthForJqGrid();
				}
			}).navGrid('#infoChangeDetailPager', {
				edit : false,
				add : false,
				del : false,
				search : false
			});
		}
	},
	getGridUrlDetailAction : function(id) {
		return '/catalog_customer_mng/search-info-change-detail?id=' + id;
	},
	cancelCat : function() {
		$('#saleLevelCatId').val(0);
		$("#catGrid").trigger("reloadGrid");
		CustomerCatalog._curSaleLevel = null;
		$('#group_insert_btnAddCat').show();
		$('#btnEditCat').hide();
		$('#btnCancelCat').hide();
		enable('cat');
		// sontt19
		$('#cat').parent().removeClass('BoxDisSelect');
		// end-sontt19
		CustomerCatalog.loadedCat(-2);
	},
	bindShopCodeChange : function() {
		$('#shopCode').bind('change', function() {
			var titleGroupTransfer = '<option value="' + activeType.ALL + '">--- Chọn nhóm giao hàng ---</option>';
			var titleCashier = '<option value="' + activeType.ALL + '">--- NVTT ---</option>';
			if ($('#shopCode').val().trim().length > 0) {
				enable('groupTransfer');
				$('#groupTransfer').change();
				$('#groupTransfer').parent().removeClass('BoxDisSelect');
				enable('cashier');
				$('#cashier').change();
				$('#cashier').parent().removeClass('BoxDisSelect');
				$.getJSON('/rest/catalog_customer_mng/shop/group-transfer/list.json?shopCode=' + encodeChar($('#shopCode').val().trim()), function(data) {
						var selectedValue = $('#groupTransfer').val();
						var arrHtml = new Array();
						arrHtml.push(titleGroupTransfer);
						for ( var i = 0; i < data.length; i++) {
							arrHtml.push('<option value="' + Utils.XSSEncode(data[i].value) + '">' + Utils.XSSEncode(data[i].name) + '</option>');
						}
						$('#groupTransfer').html(arrHtml.join(""));
						$('#groupTransfer').val(selectedValue);
						$('#groupTransfer').change();
				});
				var urlCashier = '/rest/catalog_customer_mng/shop/cashier/list.json?shopCode=' + encodeChar($('#shopCode').val().trim());
				var customerCode = $('#code').val();
				if (customerCode != undefined && customerCode != null && customerCode.trim().length > 0) {
					urlCashier += '&customerCode=' + encodeChar($('#code').val().trim());
				} else {
					urlCashier += '&customerCode=';
				}
				$.getJSON(urlCashier, function(data) {
					var selectedValue = $('#cashier').val();
					var arrHtml = new Array();
					arrHtml.push(titleCashier);
					for ( var i = 0; i < data.length; i++) {
						arrHtml.push('<option value="' + Utils.XSSEncode(data[i].value) + '">' + Utils.XSSEncode(data[i].name) + '</option>');
					}
					$('#cashier').html(arrHtml.join(""));
					$('#cashier').val(selectedValue);
					$('#cashier').change();
				});
			} else {
				disabled('groupTransfer');
				$('#groupTransfer').html(titleGroupTransfer);
				$('#groupTransfer').val(activeType.ALL);
				$('#groupTransfer').change();
				$('#groupTransfer').parent().addClass('BoxDisSelect');
				disabled('cashier');
				$('#cashier').html(titleCashier);
				$('#cashier').val(activeType.ALL);
				$('#cashier').change();
				$('#cashier').parent().addClass('BoxDisSelect');
			}
		});
	},
	bindShopCodeChangeDisable : function() {
		$('#shopCode').bind('change', function() {
			var titleGroupTransfer = '<option value="' + activeType.ALL + '">--- Chọn nhóm giao hàng ---</option>';
			var titleCashier = '<option value="' + activeType.ALL + '">--- NVTT ---</option>';
			if ($('#shopCode').val().trim().length > 0) {
				enable('groupTransfer');
				$('#groupTransfer').change();
				$('#groupTransfer').parent().removeClass('BoxDisSelect');
				enable('cashier');
				$('#cashier').change();
				$('#cashier').parent().removeClass('BoxDisSelect');
				$.getJSON('/rest/catalog_customer_mng/shop/group-transfer/list.json?shopCode=' + encodeChar($('#shopCode').val().trim()), function(data) {
					var selectedValue = $('#groupTransfer').val();
					var arrHtml = new Array();
					arrHtml.push(titleGroupTransfer);
					for ( var i = 0; i < data.length; i++) {
						arrHtml.push('<option value="' + Utils.XSSEncode(data[i].value) + '">' + Utils.XSSEncode(data[i].name) + '</option>');
					}
					$('#groupTransfer').html(arrHtml.join(""));
					$('#groupTransfer').val(selectedValue);
					$('#groupTransfer').change();
				});
				var urlCashier = '/rest/catalog_customer_mng/shop/cashier/list.json?shopCode=' + encodeChar($('#shopCode').val().trim());
				var customerCode = $('#code').val();
				if (customerCode != undefined && customerCode != null && customerCode.trim().length > 0) {
					urlCashier += '&customerCode=' + encodeChar($('#code').val().trim());
				} else {
					urlCashier += '&customerCode=';
				}
				$.getJSON(urlCashier, function(data) {
					var selectedValue = $('#cashier').val();
					var arrHtml = new Array();
					arrHtml.push(titleCashier);
					for ( var i = 0; i < data.length; i++) {
						arrHtml.push('<option value="' + data[i].value + '">' + Utils.XSSEncode(data[i].name) + '</option>');
					}
					$('#cashier').html(arrHtml.join(""));
					$('#cashier').val(selectedValue);
					$('#cashier').change();
				});
			} else {
				disabled('groupTransfer');
				$('#groupTransfer').html(titleGroupTransfer);
				$('#groupTransfer').val(activeType.ALL);
				$('#groupTransfer').change();
				$('#groupTransfer').parent().addClass('BoxDisSelect');
				disabled('cashier');
				$('#cashier').html(titleCashier);
				$('#cashier').val(activeType.ALL);
				$('#cashier').change();
				$('#cashier').parent().addClass('BoxDisSelect');
			}
		});
	},
	getDate:function(d){
		var result='';
		try{
			var day = new Date(d);
			result+=day.getDate()+'/'+(day.getMonth()+1)+'/'+day.getFullYear()+' - '+day.getHours()+':'+day.getMinutes();
			return result;
		}catch(e){
			return '';
		}
	},
	showCustomerImageDetail : function(objectType) {
		var custId = $('#selId').val().trim();
		var params = new Object();
		params.customerId = custId;
		params.objectType = objectType;
		CommonSearch.viewCustomerImageInfo(params,function(data) {
			CustomerCatalog.imageLoadingShow();
			if (data.error == undefined || data.error == true) {
				$('#errMsg').html(myData.errMsg);
			} else {
				$('.fancybox-title').html($('.fancybox-title').html()+' ('+data.list.length+')');
				var widhei = CustomerCatalog.calWidthHeight(data.item.width + '-' + data.item.height);
				data.item.width = widhei.split('-')[0];
				data.item.height = widhei.split('-')[1];
				if (document.images) {
					var img = new Image();
					img.src = data.item.url;
					img.onload = function() {
						$('.fancybox-inner #imageBig').attr('src', data.item.url);
						$('.fancybox-inner #imageBig').attr('width', data.item.width);
						$('.fancybox-inner #imageBig').attr('height', data.item.height);
						CustomerCatalog.imageBigShow();
					};
				}
				$('.fancybox-inner #itemImageCreateUser').html(data.item.staff.staffCode + ' &ndash; ' + Utils.XSSEncode(data.item.staff.staffName));
				$('.fancybox-inner #itemImageCreateDate').html(CustomerCatalog.getDate(data.item.createDate));
				var lat = data.item.lat;
				var lng = data.item.lng;
				if (lat > 0 && lng > 0) {
					CustomerCatalog.openMapOnImage(lat, lng);
				} else {
					$('#itemImageMap').html('Chưa cập nhật tọa độ cho đơn vị.');
				}
				var listImage = '<ul class="ResetList PhotoCols2List">';
				for (i = 0; i < data.list.length; i++) {
					if (data.list[i].id == data.item.id) {
						listImage += "<li id='li_" + data.list[i].id + "' class='FocusStyle'>";
					} else {
						listImage += "<li id='li_" + data.list[i].id + "'>";
					}
					listImage += "	<div class='PhotoThumbnails'><span class='BoxFrame'><span class='BoxMiddle'>";
					listImage += "       <img class='imageSelect' data-original='" + data.list[i].thumbUrl
											+ "' src='/resources/images/grey.jpg' width='192' height='126' name='thumbUrl_"
											+ data.list[i].id + "' ></span></span></div>";
					listImage += "	<p id='createDate_"
											+ data.list[i].id + "'>"
											+ CustomerCatalog.getDate(data.list[i].createDate) + "</p>";
					listImage += "	<p id='createUser_" + data.list[i].id + "' style='display:none' >"
											+ data.list[i].staff.staffCode + " &ndash; "
											+ data.list[i].staff.staffName + "</p>";
					listImage += "	<p id='url_" + data.list[i].id + "' style='display:none' >" + data.list[i].url + "</p>";
					listImage += "	<p id='widthheight_" + data.list[i].id
											+ "' style='display:none' >" + data.list[i].width + '-'
											+ data.list[i].height + "</p>";
					listImage += "	<p id='latlng_" + data.list[i].id
											+ "' style='display:none' >" + data.list[i].lat + '-'
											+ data.list[i].lng + "</p>";
					listImage += "</li>";
				}
				listImage += '</ul>';
				$('#listImage').html(listImage);
				$(".imageSelect").lazyload({
					effect : "fadeIn",
					container : $("#listImage")
				});
				$('#listImage').jScrollPane();

				$('.imageSelect').click(CustomerCatalog.imageSelect);
			}
		});
	},
	imageSelect : function() {
		try {
			CustomerCatalog.imageLoadingShow();
			var imageId = $(this).attr('name').split('_')[1];
			if (imageId != null && imageId != '' && imageId != undefined) {
				$('.FocusStyle').removeClass('FocusStyle');
				$('#li_' + imageId).addClass('FocusStyle');
				var wihe = $('.fancybox-inner #widthheight_' + imageId).html();
				wihe = CustomerCatalog.calWidthHeight(wihe);
				var width = wihe.split('-')[0].trim();
				var height = wihe.split('-')[1].trim();
				if (document.images) {
					var img = new Image();
					img.src = $('#url_' + imageId).html();
					img.onload = function() {
						$('.fancybox-inner #imageBig').attr('width', width);
						$('.fancybox-inner #imageBig').attr('height', height);
						$('.fancybox-inner #imageBig').attr('src',
								$('#url_' + imageId).html());
						CustomerCatalog.imageBigShow();
					};
				}
				$('.fancybox-inner #itemImageCreateUser').html(
						$('#createUser_' + imageId).html());
				$('.fancybox-inner #itemImageCreateDate').html(
						$('#createDate_' + imageId).html());
				var latlng = $('.fancybox-inner #latlng_' + imageId).html();
				var lat = latlng.split('-')[0].trim();
				var lng = latlng.split('-')[1].trim();
				CustomerCatalog.openMapOnImage(lat, lng);
			}
		} catch (err) {
			return;
		}
	},
	imageBigShow : function() {
		$('.fancybox-inner #imageLoading').hide();
		$('.fancybox-inner #imageBig').fadeIn(1000);
	},
	imageLoadingShow : function() {
		$('.fancybox-inner #imageBig').hide();
		$('.fancybox-inner #imageLoading').show();
	},
	openMapOnImage : function(lat, lng) {
		MapUtil.loadMapResource(function() {
			ViettelMap.loadMapImage('itemImageMap', lat, lng,
					CustomerCatalog._zoom);
		});
	},
	calWidthHeight : function(wh) {
		var defWidth = 580;
		var defHeight = 426;
		try {
			var width = parseFloat(wh.split('-')[0].trim());
			var height = parseFloat(wh.split('-')[1].trim());
			var percent;
			if (width > defWidth) {
				percent = width / defWidth;
				width = defWidth;
				height = height / percent;
			}
			if (height > defHeight) {
				percent = height / defHeight;
				height = defHeight;
				width = width / percent;
			}
			return width + '-' + height;
		} catch (err) {
			return defWidth + '-' + defHeight;
		}
	},
	extendMap : function(flag) {
		if (flag == 1) {
			$('#extendMapOn').hide();
			$('#itemImageMap').show("slow");
			$('#extendMapOff').show();
		} else {
			$('#extendMapOff').hide();
			$('#itemImageMap').hide("slow");
			$('#extendMapOn').show();
		}
	},
	loadTreeEx : function() {
		CustomerCatalog.loadShopTree();
		$('#tree').bind("loaded.jstree", function(event, data) {
			if ($('#treeContainer').data("jsp") != undefined) {
				$('#treeContainer').data("jsp").destroy();
			}
			$('#treeContainer').jScrollPane();
		});
	},
	loadShopTree : function() {
		$('#tree').jstree({
							"plugins" : [ "themes", "json_data", "ui" ],
							"themes" : {
								"theme" : "classic",
								"icons" : false,
								"dots" : true
							},
							"json_data" : {
								"ajax" : {
									"url" : function(node) {
										if (node == -1) {
											return "/rest/catalog/unit-tree-ex/" + $('#shopIdHidden').val().trim() + "/0/tree.json";
										} else {
											return "/rest/catalog/unit-tree-ex/" + node.data("attr").id + "/1/tree.json";
										}
									},
									"type" : 'GET',
									"success" : function(ops) {
										data = [];
										for (opnum in ops) {
											var op = ops[opnum];
											node = {
												"data" : op.data,
												"attr" : op.attr,
												"metadata" : op,
												"state" : op.state
											};
											data.push(node);
										}
										return data;
									},
									'complete' : function() {
										$('#tree').bind("select_node.jstree", function(event, data) {
											var name = data.inst.get_text(data.rslt.obj);
											var id = data.rslt.obj.attr("id");
											var code = data.rslt.obj.attr("contentItemId");
											var url = CustomerCatalog.getGridUrl('','','','','','','','',activeType.ALL,activeType.RUNNING,activeType.ALL,activeType.ALL,activeType.ALL,activeType.ALL,code,'',activeType.ALL);
											$("#grid").setGridParam({url : url, page : 1}).trigger("reloadGrid");
										});
										$('#tree').bind("loaded.jstree", function(event, data) {
											/*
											 * if($('#treeContainer').data("jsp")!=
											 * undefined){
											 * $('#treeContainer').data("jsp").destroy(); }
											 * $('#treeContainer').jScrollPane();
											 */
											if ($('#treeContainer').data("jsp") != undefined) {
												$('#treeContainer').data("jsp").destroy();
											}
											$('#treeContainer').css('height', $('.Content').height() - 55);
											$('#treeContainer').jScrollPane();
										});
										setTimeout(function() {
											if ($('#treeContainer').data("jsp") != undefined) {
												$('#treeContainer').data("jsp").destroy();
											}
											$('#treeContainer').css('height', $('.Content').height() - 75);
												$('#treeContainer').jScrollPane();
										}, 500);
									}
								}
							},
							"core" : {
								"load_open" : true
							}
						});
	},
	viewBigMap : function() {
		var params = new Object();
		params.lat = $('#lat').val().trim();
		params.lng = $('#lng').val().trim();
		var hasPm = $('#hasEditPermission').val().trim();
		if (hasPm == 'true') {
			CustomerCatalog.viewBigMapOnDlg(params);
		} else {
			$('#imgBtnDeleteLatLng, #imgBtnUpdateLatLng, #imgBtnCancelLatLng').hide();
			CustomerCatalog.viewBigMapDisableClick(params);
		}
	},
	importExcel : function() {
		$('#isView').val(0);
		$('#importFrm').submit();
		return false;
	},
	viewExcel : function() {
		$('#isView').val(1);
		$('#importFrm').submit();
		return false;
	},
	beforeImportExcel: function() {
		if (!previewImportExcelFile(document.getElementById("excelFile"))) {
			return false;
		}
		$('#errExcelMsg').hide();
		$('#divOverlay').addClass('Overlay');
		$('#divOverlay').show();
		$('#imgOverlay').show();
		return true;
	},
	afterImportExcel2 : function(responseText, statusText, xhr, $form) {
		$('#divOverlay').removeClass('Overlay');
		$('#imgOverlay').hide();
		$('#divOverlay').hide();
		if (statusText == 'success') {
			$("#responseDiv").html(responseText);
			var newToken = $('#responseDiv #newToken').val();
	    	if (newToken != null && newToken != undefined && newToken != '') {
	    		$('#token').val(newToken);
	    		$('#tokenImport').val(newToken);
	    	}
			if ($('#errorExcel').html().trim() == 'true' || $('#errorExcelMsg').html().trim().length > 0) {
				$('#errExcelMsg').html($('#errorExcelMsg').html()).show();
			} else {
				if ($('#typeView').html().trim() == 'false') {
					var totalRow = parseInt($('#totalRow').html().trim());
					var numFail = parseInt($('#numFail').html().trim());
					var fileNameFail = $('#fileNameFail').html();
					var mes = format(msgErr_result_import_excel,(totalRow - numFail), numFail);
					if (numFail > 0) {
						mes += ' <a href="' + fileNameFail + '">Xem chi tiết lỗi</a>';
					}
					$('#errExcelMsg').html(mes).show();
					$("#grid").trigger("reloadGrid");
					$('#customerDatagrid').datagrid('reload');
				} else {
					$('#excelDialog').html(responseText);
					var html = $('#excelDialog').html();
					$.fancybox(html, {
						modal : true,
						title : 'Thông tin import KH',
						afterShow : function() {
							//$('#excelDialog').html(responseText);
							$('.fancybox-inner #scrollExcelDialog').jScrollPane();
							$('.fancybox-inner .ScrollSection').jScrollPane().data().jsp;
						},
						afterClose : function() {
							$('#excelDialog').html(html);
						}
					});
				}
			}
			$('#fakefilepc').val('');
			$('#excelFile').val('');
		}
	},
	afterImportExcel3 : function(responseText, statusText, xhr, $form) {
		$('#divOverlay').removeClass('Overlay');
		$('#imgOverlay').hide();
		$('#divOverlay').hide();
		if (statusText == 'success') {
			$("#responseDiv").html(responseText);
			var newToken = $('#responseDiv #newToken').val();
	    	if(newToken != null && newToken != undefined && newToken != '') {
	    		$('#token').val(newToken);
	    		$('#tokenImport').val(newToken);
	    	}
			if ($('#errorExcel').html().trim() == 'true'
					|| $('#errorExcelMsg').html().trim().length > 0) {
				$('#errExcelMsg').html($('#errorExcelMsg').html()).show();
			} else {
				if ($('#typeView').html().trim() == 'false') {
					var totalRow = parseInt($('#totalRow').html().trim());
					var numFail = parseInt($('#numFail').html().trim());
					var fileNameFail = $('#fileNameFail').html();
					var mes = format(msgErr_result_import_excel,
							(totalRow - numFail), numFail);
					if (numFail > 0) {
						mes += ' <a href="' + fileNameFail
								+ '">Xem chi tiết lỗi</a>';
					}
					$('#errExcelMsg').html(mes).show();
					$("#grid").trigger("reloadGrid");
					$('#customerDatagrid').datagrid('reload');
				} else {
					$('#excelDialog').html(responseText);
					var html = $('#excelDialog').html();
					$.fancybox(html, {
						modal : true,
						title : catalog_customer_import_cat_info,
						afterShow : function() {
							//$('#excelDialogSub').html(responseText);
							$('.fancybox-inner #scrollExcelDialog')
									.jScrollPane();
							$('.fancybox-inner .ScrollSection').jScrollPane()
									.data().jsp;
						},
						afterClose : function() {
							$('#excelDialog').html(html);
						}
					});
				}
			}
		}
		$("#file").val("");
		$("#fakefilepc").val("");
	},
	exportExcelCustomer : function() {
		$('#divOverlay').addClass('Overlay');
		$('#imgOverlay').show();
		$('#errMsg').html('').hide();
		$('#errExcelMsg').html('').hide();
		var type = $('#excelType').val().trim();
		$('#errMsg').html('').hide();
		var shopCode = $('#txtShopCode').val().trim();
		var customerCode = $('#customerCode').val().trim();
		var customerName = $('#customerName').val().trim();
		var customerType = $('#customerType').combobox('getValue').trim().split(' - ')[0];
		var address = $('#address').val().trim();
		var phone = $('#phone').val().trim();
		var status = $('#status').val();
		var tansuat = $('#tansuat').val().trim();
		
		var params = {shopCode:shopCode, customerCode:customerCode, customerName:customerName, customerType:customerType, address:address, phone:phone, status:status, tansuat:tansuat};
		if (type == 1) {
			var datagirdOptions = $("#customerDatagrid").datagrid('options');
			if (datagirdOptions.sortName) {
				params.sort = datagirdOptions.sortName;
			}
			if (datagirdOptions.sortOrder) {
				params.order = datagirdOptions.sortOrder;
			}
			var url = "/catalog_customer_mng/export-Excel-Customer";
			ReportUtils.exportReport(url, params);
		}
		if (type == 2) {
			var url = "/catalog_customer_mng/export-Excel-Customer-Category";
			ReportUtils.exportReport(url, params);
		}
		return false;
	},
	getExportExcelCustomerUrl : function(code, name, provinceCode,
			districtCode, address, street, phone, mobilePhone, customerType,
			status, region, loyalty, groupTransfer, cashier, shopCode,
			shopCodeLike, display) {
		if (parseInt(loyalty) == activeType.ALL) {
			loyalty = '';
		}
		if (parseInt(region) == activeType.ALL) {
			region = '';
		}
		if (parseInt(display) == activeType.ALL) {
			display = '';
		}
		return "/catalog_customer_mng/export-Excel-Customer?code="
				+ encodeChar(code) + "&name=" + encodeChar(name)
				+ "&provinceCode=" + encodeChar(provinceCode)
				+ "&districtCode=" + encodeChar(districtCode) + "&address="
				+ encodeChar(address) + "&street=" + encodeChar(street)
				+ "&phone=" + encodeChar(phone) + "&mobilePhone="
				+ encodeChar(mobilePhone) + "&customerType="
				+ encodeChar(customerType) + "&status=" + status + "&region="
				+ encodeChar(region) + "&loyalty=" + encodeChar(loyalty)
				+ "&groupTransfer=" + encodeChar(groupTransfer) + "&cashierId="
				+ encodeChar(cashier) + "&shopCode=" + encodeChar(shopCode)
				+ '&shopCodeLike=' + encodeChar(shopCodeLike) + '&display='
				+ encodeChar(display);
	},
	getExportExcelCustomerCategoryUrl : function(code, name, provinceCode,
			districtCode, address, street, phone, mobilePhone, customerType,
			status, region, loyalty, groupTransfer, cashier, shopCode,
			shopCodeLike, display) {
		if (parseInt(loyalty) == activeType.ALL) {
			loyalty = '';
		}
		if (parseInt(region) == activeType.ALL) {
			region = '';
		}
		if (parseInt(display) == activeType.ALL) {
			display = '';
		}
		return "/catalog_customer_mng/export-Excel-Customer-Category?code="
				+ encodeChar(code) + "&name=" + encodeChar(name)
				+ "&provinceCode=" + encodeChar(provinceCode)
				+ "&districtCode=" + encodeChar(districtCode) + "&address="
				+ encodeChar(address) + "&street=" + encodeChar(street)
				+ "&phone=" + encodeChar(phone) + "&mobilePhone="
				+ encodeChar(mobilePhone) + "&customerType="
				+ encodeChar(customerType) + "&status=" + status + "&region="
				+ encodeChar(region) + "&loyalty=" + encodeChar(loyalty)
				+ "&groupTransfer=" + encodeChar(groupTransfer) + "&cashierId="
				+ encodeChar(cashier) + "&shopCode=" + encodeChar(shopCode)
				+ '&shopCodeLike=' + encodeChar(shopCodeLike) + '&display='
				+ encodeChar(display);
	},
	deactiveAllMainTab: function(){
		$('#tab1 a').removeClass('Active');
		$('#tab2 a').removeClass('Active');
		$('#tabContent1').hide();
		$('#tabContent2').hide();
	},
	exportActionLog: function(){
        var id = $('#selId').val();
        var fromDate = $('#fromDate').val();
        var toDate = $('#toDate').val();
        ExportActionLog.exportActionLog(id, ExportActionLog.CUSTOMER, fromDate, toDate,'errMsgActionLog');
 	},
 	checkAll: function(check){
 		if(check.checked) {
 			$("#checkAll").prop("checked", true);
 			$(".ck").prop("checked", true);
 		} else if(!check.checked) {
 			$(".ck").prop("checked", false);
 			$("#checkAll").prop("checked", false);
 		}
 	},
 	loadAreaInfoAndMap: function(data) {
 		//var provinceCode = data.provinceCode;
		var provinceName = data.provinceName;
		//var districtCode = data.districtCode;
		var districtName = data.districtName;
		//var precinctCode = data.precinctCode;
		var precinctName = data.precinctName;
		$('#provinceName').val(provinceName);
		$('#districtName').val(districtName);
		$('#wardName').val(precinctName);
		var lat = 16.1060393;
		var lng = 107.1976608455;
		var pt = new viettel.LatLng(lat, lng);
		ViettelMap._map.setZoom(ViettelMap._DEF_ALL_ZOOM);
		ViettelMap._map.setCenter(pt);
 	},
 	editComboboxX: function(param){
 		var tmpString = $(param).combobox('getValue');
 		tmpString = tmpString.substring(0, tmpString.indexOf('-')).trim();
 		if (tmpString == ''){
 			tmpString = $(param).combobox('getValue').trim();
 		}
 		if (tmpString.indexOf('--Chọn') >= 0){
 			
 		}
 		return tmpString;
 	},
 	
 	viewBigMapOnDlg : function(arrParam) {
 		CustomerCatalog.latbk = arrParam.lat;
		CustomerCatalog.lngbk = arrParam.lng;
		var html = $('#viewBigMap').html();
		$('#viewBigMap').dialog({
			closed: false, 
			title : xem_ban_do,
	        cache: false,  
	        modal: true,
	        onOpen: function(){
	        	$('.easyui-dialog #imgBtnDeleteLatLng').unbind('click');
	        	$('.easyui-dialog #imgBtnDeleteLatLng').bind('click', function(){
	        		$('#lat').val('');
	        		$('#lng').val('');
	        		ViettelMap.clearOverlays();
	        		ViettelMap._listOverlay = new Array(); 
	        		if (ViettelMap._marker != null) {
	                	ViettelMap._marker.setMap(null);
	                }	                
	        	});
	        	$('.easyui-dialog #imgBtnUpdateLatLng').unbind('click');
	        	$('.easyui-dialog #imgBtnUpdateLatLng').bind('click', function(){
		    		$('#viewBigMap').dialog('close');
	        	});
	        	$('.easyui-dialog #imgBtnCancelLatLng').unbind('click');
	        	$('.easyui-dialog #imgBtnCancelLatLng').bind('click', function(){
	        		var zoom = 12;
					if (CustomerCatalog.latbk.trim().length == 0 || CustomerCatalog.lngbk.trim().length == 0) {
						zoom = ViettelMap._DEF_ALL_ZOOM;
					}
					$('#lat').val(CustomerCatalog.latbk);
					$('#lng').val(CustomerCatalog.lngbk);
					VTMapUtil.loadMapResource(function(){
						ViettelMap.loadBigMapEx('bigMapContainer', CustomerCatalog.latbk, CustomerCatalog.lngbk, zoom, function(latitude, longtitude) {
							$('#lat').val(latitude);
							$('#lng').val(longtitude);
						}, true, true);
		    		});
	        		//$('#viewBigMap').onClose();
	        	});
	        	var tm = setTimeout(function(){//chuyen ban do toi vi tri man hinh
					//load map
					var lat = arrParam.lat;
					var lng = arrParam.lng;
					var zoom = 12;
					if(lat.trim().length==0 || lng.trim().length==0){
						zoom = ViettelMap._DEF_ALL_ZOOM;
					}
					VTMapUtil.loadMapResource(function(){
						ViettelMap.loadBigMapEx('bigMapContainer', lat, lng, zoom, function(latitude, longtitude) {
							$('#lat').val(latitude);
							$('#lng').val(longtitude);
						}, true);
//						if (lat == 'null' || lng == 'null') {
//							var centerCode = $('#precinct option:selected').attr('center_code');
//							var type = 3;
//							if (centerCode == undefined || centerCode == null || centerCode.trim().length == 0) {
//								centerCode = $('#district option:selected').attr('center_code');
//								type = 2;
//								if (centerCode == undefined || centerCode == null || centerCode.trim().length == 0) {
//									centerCode = $('#province option:selected').attr('center_code');
//									type = 1;
//								}
//							}
//							if (centerCode != undefined && centerCode != null && centerCode.trim().length > 0) {
//								CustomerCatalog.loadMapByArea(centerCode.trim(), type);
//							}
//						}
		    		});
					clearTimeout(tm);
				}, 500);
	        },
	        onClose:function() {
	        	CustomerCatalog.showMapContent();
	        	$('#viewBigMap').html(html);
	        }
		});
		
	},
	showMapContent:function(){
		var lat = $('#lat').val().trim();
		var lng = $('#lng').val().trim();
		var zoom = 10;
		if (lat.length == 0 && lng.length == 0) {
			zoom = ViettelMap._DEF_ALL_ZOOM;
		}
		$('#mapContainer').html('');
		VTMapUtil.loadMapResource(function(){
			ViettelMap.loadBigMapEx('mapContainer', lat, lng, zoom, function(latitude, longtitude) {
				$('#lat').val(latitude);
				$('#lng').val(longtitude);
			}, true);
		});
	},
	viewBigMapDisableClick : function(arrParam) {
		var html = $('#viewBigMap').html();
		$('#viewBigMap').dialog({
			closed: false,  
	        cache: false,  
	        modal: true,
	        onOpen: function(){
	        	var tm = setTimeout(function(){//chuyen ban do toi vi tri man hinh
					//load map
					var lat = arrParam.lat;
					var lng = arrParam.lng;
					VTMapUtil.loadMapResource(function(){
						ViettelMap.loadBigMapEx('bigMapContainer', lat, lng, CustomerCatalog._DEF_ZOOM, null);
						/*if (lat == 'null' || lng == 'null') {
							var centerCode = $('#precinct option:selected').attr('center_code');
							var type = 3;
							if (centerCode == undefined || centerCode == null || centerCode.trim().length == 0) {
								centerCode = $('#district option:selected').attr('center_code');
								type = 2;
								if (centerCode == undefined || centerCode == null || centerCode.trim().length == 0) {
									centerCode = $('#province option:selected').attr('center_code');
									type = 1;
								}
							}
							if (centerCode != undefined && centerCode != null && centerCode.trim().length > 0) {
								CustomerCatalog.loadMapByArea(centerCode.trim(), type);
							}
						}*/
		    		});
					clearTimeout(tm);
				}, 500);
	        },
	        onClose:function() {
	        	$('#viewBigMap').html(html);
	        }
		});
	},
 	exportExcelTemplate:function(){
 		$('#divOverlay').show();
		var url='/catalog_customer_mng/export-Template-Excel-Customer';
		if(CommonSearch._xhrReport != null){
			CommonSearch._xhrReport.abort();
			CommonSearch._xhrReport = null;
		}
		var params = new Object();
		try {
			params.reportCode = $('#function_code').val();
		} catch(e) {
			// Oop! Error
		}
		var kData = $.param(params, true);
		CommonSearch._xhrReport = $.ajax({
			type : "POST",
			url : url,
			data :(kData),
			dataType: "json",
			success : function(data) {
				$('#divOverlay').hide();
				if(data.error && data.errMsg!= undefined){
					$('#'+ errMsg).html(formatString(msgCommon5, escapeSpecialChar(data.errMsg))).show();
				} else{
					if(data.hasData!= undefined && !data.hasData) {
						$('#'+ errMsg).html(msgCommon6).show();
					} else{
						var filePath = ReportUtils.buildReportFilePath(data.view);
						window.location.href = filePath;
						setTimeout(function(){ //Set timeout để đảm bảo file load lên hoàn tất
	                        CommonSearch.deleteFileExcelExport(data.view);
	                  },30000);
					}
				}
			},
			error:function(XMLHttpRequest, textStatus, errorDivThrown) {				
			}
		});
		return false;
	},
	exportExcelTemplateCategory:function(){
		$('#divOverlay').show();
		var url='/catalog_customer_mng/export-Template-Excel-Category';
		if(CommonSearch._xhrReport != null){
			CommonSearch._xhrReport.abort();
			CommonSearch._xhrReport = null;
		}
		var params = new Object();
		try {
			params.reportCode = $('#function_code').val();
		} catch(e) {
			// Oop! Error
		}
		var kData = $.param(params, true);
		CommonSearch._xhrReport = $.ajax({
			type : "POST",
			url : url,
			data :(kData),
			dataType: "json",
			success : function(data) {
				$('#divOverlay').hide();
				if(data.error && data.errMsg!= undefined){
					$('#'+ errMsg).html(formatString(msgCommon5, escapeSpecialChar(data.errMsg))).show();
				} else{
					if(data.hasData!= undefined && !data.hasData) {
						$('#'+ errMsg).html(msgCommon6).show();
					} else{
						var filePath = ReportUtils.buildReportFilePath(data.view);
						window.location.href = filePath;
						setTimeout(function(){ //Set timeout để đảm bảo file load lên hoàn tất
							CommonSearch.deleteFileExcelExport(data.view);
						},30000);
					}
				}
			},
			error:function(XMLHttpRequest, textStatus, errorDivThrown) {				
			}
		});
		return false;
	},
	agreeCustomer:function(status){
		$('#errMsg').hide();
		var params = new Object();
		var allVals = [];
		$('.ck').each(function() {
			if ($(this).prop('checked')) {
				allVals.push($(this).val());
			}
		});
		params.lstCustomerId = allVals;
		if (status != undefined && status != null) {
			params.status = status;
		}
		if (params.lstCustomerId.length == 0) {
			$('#errMsg').html(create_route_chua_chon_kh).show();
			return;
		}
		var msgConfirm = '';
		var APPROVE = 1, REJECT = 3, DELETE = -1;
		if (status == APPROVE) {
			msgConfirm = formatString(catalog_customer_confirm, catalog_customer_approve);
			CustomerCatalog.approveNewCustomer(allVals);
		} else if (status == REJECT) {
			msgConfirm = formatString(catalog_customer_confirm, jsp_common_status_reject);
			CustomerCatalog.denyNewCustomer(allVals);
		} else {
			msgConfirm = formatString(catalog_customer_confirm, jsp_common_xoa);
			CustomerCatalog.deleteNewCustomer(allVals);
		}

		return false;
	},
	/**
	 * Duyet khach hang moi
	 * @author tuannd20
	 * @param  {Array} customerIds danh sach id cac khach hang
	 * @return {void}
	 * @since  31/08/2015
	 */
	approveNewCustomer: function(customerIds) {
		CustomerCatalog
		.showConfirmDialog()
		.then(function() {
			return Promise.resolve(customerIds);
		}).each(function(customerId) {
			var param = {
				customerId: customerId
			};
			return CustomerCatalog.tryApproveNewCustomer(param)
				.then(function(result) {
					var isError = result && result.hasOwnProperty('error') && result.error;
					var errMsg = result && result.hasOwnProperty('errMsg') && result.errMsg;
					var hasDuplicateCustomers = result && result.hasOwnProperty('duplicatedCustomer') 
												&& result.duplicatedCustomer instanceof Array && result.duplicatedCustomer.length;
					if (hasDuplicateCustomers) {
						var rows = $('#customerDatagrid').datagrid('getRows');
//						var approvingCustomerInfo = rows.find(function(element, index, array) {
//													return element.id == customerId;
//												});
						var approvingCustomerInfo;
						$.each(rows, function(index, item) {
						    if (item.id == customerId) {
						    	approvingCustomerInfo = item;
						    }
						});
						return CustomerCatalog.confirmDuplicateCustomer(customerId, approvingCustomerInfo, result);
					} else if (isError) {
						return CustomerCatalog.showAlertDialog(errMsg);
					}
					return Promise.resolve();
				}).catch(function(e) {
					console.log(e);
					$('#confirm-duplicate-customer-dialog').dialog('close');
				});
		}).catch(function(e) {
			console.log(e);
		}).finally(function() {
			CustomerCatalog.search();
		});
	},
	/**
	 * duyet khach hang
	 * @author tuannd20
	 * @param  {object} param thong tin khach hang dang duyet
	 * @return {void}
	 * @since 27/08/2015
	 */
	tryApproveNewCustomer: function(param) {
		return new Promise(function(resolve, reject) {
			Utils.saveData(param, "/catalog_customer_mng/approve", CustomerCatalog._xhrSave, null, function(data) {
				resolve(data);
			}, null, ' #infoTabContainer ', null, function(data) {
				resolve(data);
			});
		});
	},
	/**
	 * xac nhan thao tac thuc hien voi khach hang trung
	 * @author tuannd20
	 * @param {long} customerId id khach hang dang duyet
	 * @param {object} approvingCustomerInfo thong tin khach hang dang duyet
	 * @param  {object} duplicateWithCustomers du lieu mo ta khach hang trung
	 * @return {promise}
	 * @since  31/08/2015
	 */
	confirmDuplicateCustomer: function(customerId, approvingCustomerInfo, duplicateWithCustomers) {
		var tmpData = {};
		return CustomerCatalog.showConfirmDuplicateCustomerDialog(approvingCustomerInfo, duplicateWithCustomers)
			.spread(function(actionType, customerCode) {
				tmpData.actionType = actionType;
				tmpData.customerCode = customerCode;
				return CustomerCatalog.showConfirmDialog();
			}).then(function() {
				var result = tmpData;
				if (!result) {
					return Promise.resolve({error: false});
				}
				var actionType = result.actionType;
				var customerCode = result.customerCode;
				var promise = null;
				switch (actionType) {
					case CustomerCatalog.BUTTON_TYPE_DELETE:
						promise = CustomerCatalog.deleteNewCustomerSync(customerId);
						break;
					case CustomerCatalog.BUTTON_TYPE_CREATE:
						promise = CustomerCatalog.createNewCustomer(customerId, null);
						break;
					case CustomerCatalog.BUTTON_TYPE_DENY:
						promise = CustomerCatalog.denyNewCustomerSync(customerId);
						break;
					case CustomerCatalog.BUTTON_TYPE_CREATE_WITH_DUPLICATE_CUSTOMER_CODE:
						promise = CustomerCatalog.createNewCustomer(customerId, customerCode);
						break;
					default:
						promise = Promise.resolve();
				}
				return promise;
			}).then(function(result) {
				if (result && !result.error) {
					$('#confirm-duplicate-customer-dialog').dialog('close');
				}
				if (result && result.error) {
					var errorMsg = result.errMsg || result.errorCustomer[customerId];
					$('#dlgErrMsg').html(errorMsg);
					CustomerCatalog.showElement('#dlgErrMsg', 5000);
					return CustomerCatalog.confirmDuplicateCustomer(customerId, approvingCustomerInfo, duplicateWithCustomers);
				}
			}).catch(function(actionType) {
				if (actionType && actionType === CustomerCatalog.BUTTON_TYPE_CONFIRM_LATER) {
					return Promise.reject(actionType);
				}
				return CustomerCatalog.confirmDuplicateCustomer(customerId, approvingCustomerInfo, duplicateWithCustomers);
			});
	},
	/**
	 * Xac nhan khach hang trung
	 * @author tuannd20
	 * @param {object} approvingCustomerInfo thong tin khach hang dang duyet
	 * @param  {duplicateInfo} du lieu hien thi tren grid khach hang trung
	 * @return {promise}
	 * @since 27/08/2015
	 */
	showConfirmDuplicateCustomerDialog: function(approvingCustomerInfo, duplicateInfo) {
		var duplicateWithCustomers = duplicateInfo.duplicatedCustomer;
		CustomerCatalog.duplicateWithCustomers = duplicateWithCustomers;
		var promise = new Promise(function(resolve, reject) {
			$('#confirm-duplicate-customer-dialog').dialog({
				title: "Xác nhận khách hàng trùng: " + Utils.XSSEncode(approvingCustomerInfo.customerName) + ", " + Utils.XSSEncode(approvingCustomerInfo.address),
				width: 950, 
				height: 'auto',
				closed: false,
				cache: false,
				modal: true,
				onOpen: function() {
					CustomerCatalog.initDuplicateCustomerGrid(approvingCustomerInfo, duplicateWithCustomers);

					$('#confirm-duplicate-customer-dialog button').bind('click', function() {
						var buttonType = $(this).data().buttonType;
						if (CustomerCatalog.BUTTON_TYPE_CONFIRM_LATER == buttonType) {
							reject(buttonType);
						} else {
							resolve([buttonType, CustomerCatalog.selectedDuplicateCustomerCode]);
						}
					});

					$('#duplicate-customer-grid-container td[field=choose] a').unbind('click')
					.bind('click', function() {
						var selectedCustomerCode = $(this).data().customerCode;
						resolve([CustomerCatalog.BUTTON_TYPE_CREATE_WITH_DUPLICATE_CUSTOMER_CODE, selectedCustomerCode]);
					});
					setTimeout(function() {
						CommonSearchEasyUI.fitEasyDialog("confirm-duplicate-customer-dialog");
					}, 200);
				},
				onClose: function() {
					if (promise.isPending()) {
						reject(CustomerCatalog.BUTTON_TYPE_CONFIRM_LATER);
					}
				}
			});
		});
		return promise;
	},
	/**
	 * Khoi tao grid thong tin khach hang trung tren dialog
	 * @author tuannd20
	 * @param {object} approvingCustomerInfo thong tin khach hang dang duyet
	 * @param  {object} gridData du lieu hien thi tren grid
	 * @return {void}
	 * @since  31/08/2015
	 */
	initDuplicateCustomerGrid: function(approvingCustomerInfo, gridData) {
		$('#duplicate-customer-grid').datagrid({
			data: gridData,
			autoRowHeight: true,
			rownumbers: true,
			checkOnSelect: true,
			rowNum: 10,
			scrollbarSize: 0,
			singleSelect: true,
			pageNumber: 1,
			queryParams: {
				page: 1
			},
			fitColumns: true,
			width: ($('#confirm-duplicate-customer-dialog').width() - 40),
			columns: [[
			    {field: 'regionShopName', title: 'Miền', align: 'left', width: 110, sortable: false, resizable: false, 
			    	formatter: function(value, row, index) {
			    		return Utils.XSSEncode(value);
			    	}
				},
			    {field: 'areaShopName', title: 'Vùng', align: 'left', width: 110, sortable: false, resizable: false, 
			    	formatter: function(value, row, index) {
			    		return Utils.XSSEncode(value);
			    	}
			    },
			    {field: 'shopName', title: 'NPP', align: 'left', width: 110, sortable: false, resizable: false, 
			    	formatter: function(value, row, index) {
			    		return Utils.XSSEncode(value);
			    	}
				},
			    {field: 'shortCode', title: 'Mã KH', align: 'left', width: 110, sortable: false, resizable: false, 
			    	formatter: function(value, row, index) {
			    		return Utils.XSSEncode(value);
			    	}
				},
			    {field: 'customerName', title: 'Tên KH', align: 'left', width: 110, sortable: false, resizable: false, 
			    	formatter: function(value, row, index) {
			    		return Utils.XSSEncode(value);
			    	}
				},
			    {field: 'address', title: 'Địa chỉ', align: 'left', width: 110, sortable: false, resizable: false, 
			    	formatter: function(value, row, index) {
			    		if (row.no == CustomerCatalog.DUPLICATE_ADDRESS) {
			    			return '<span style="color: rgb(255, 0, 0);">' + Utils.XSSEncode(value) + '</span>';
			    		}
			    		return Utils.XSSEncode(value);
			    	}
				},
				{field: 'status', title: 'Trạng thái', align: 'left', width: 110, sortable: false, resizable: false, 
			    	formatter: function(value, row, index) {
			    		switch (row.status) {
				    	    case CustomerCatalog.STATUS_RUNNING:
				    	        str = Utils.language_vi('catalog.customer.active');
				    	        break;
				    	    case CustomerCatalog.STATUS_WAITING:
				    	        str = Utils.language_vi('catalog.customer.draf');
				    	        break;
			    	        case CustomerCatalog.STATUS_STOPPED:
			    	            str = Utils.language_vi('catalog.customer.pausing');
			    	            break;
				    	    case CustomerCatalog.STATUS_REJECTED:
				    	        str = Utils.language_vi('catalog.customer.reject');
				    	}
				    	return str;
			    	}
				},
			    {field: 'distanceFromApprovingCustomer', title: 'Khoảng cách (m)', align: 'left', width: 110, sortable: false, resizable: false, 
			    	formatter: function(value, row, index) {
			    		if (row.no != CustomerCatalog.DUPLICATE_ADDRESS) {
			    			return '<span style="color: rgb(255, 0, 0);">' + Math.round(value) + '</span>';
			    		}
			    		return Math.round(value);
			    	}
				},
			    {field: 'choose', title: '', align: 'center', width: 50, sortable: false, resizable: false, formatter: function(value, row, index) {
			    	if (approvingCustomerInfo.shop.shopCode !== row.shopCode) {
			    		return '<a href="javascript:void(0)" data-customer-code="' + Utils.XSSEncode(row.shortCode) + '"">Chọn</a>';
			    	}
			    	return "";			        
			    }}
			]],
			onLoadSuccess: function() {
				
			}
		});
	},
	/**
	 * Hien thi dialog confirm thao tac
	 * @author tuannd20
	 * @param  {string} dialogMessage message hien thi tren dialog
	 * @return {promise}
	 * @since  31/08/2015
	 */
	showConfirmDialog: function(dialogMessage) {
		dialogMessage = dialogMessage || 'Bạn có muốn thực hiện thao tác không?';
		return new Promise(function(resolve, reject) {
			$.messager.confirm(jsp_common_xacnhan, dialogMessage, function(r) {
				if (r) {
					resolve();
				} else {
					reject();
				}
			});
		});
	},
	/**
	 * Hien thi dialog thong bao
	 * @author tuannd20
	 * @param  {string} dialogMessage message hien thi tren dialog
	 * @return {promise}
	 * @since  21/09/2015
	 */
	showAlertDialog: function(dialogMessage) {
		return new Promise(function(resolve, reject) {
			$.messager.alert('Thông báo', dialogMessage, 'warning', function() {
			    resolve();
			});
		});
	},
	/**
	 * Tao khach hang moi voi ma khach hang chi dinh
	 * @author tuannd20
	 * @param  {long} customerId id cua khach hang dang tao
	 * @param  {string} customerCode ma khach hang da chon. NULL neu nhu dang tao moi khach hang
	 * @return {promise}
	 * @since  31/08/2015
	 */
	createNewCustomer: function(customerId, customerCode) {
		return new Promise(function(resolve, reject) {
			var param = {
				customerId: customerId
			};
			if (customerCode) {
				param.customerCode = customerCode;
			}
			Utils.saveData(param, "/catalog_customer_mng/approve/create", CustomerCatalog._xhrSave, null, function(data) {
				resolve(data);
			}, null, ' #infoTabContainer ', null, function(data) {
				resolve(data);
			});
		});
	},
	/**
	 * Huy khach hang do nhan vien tao moi
	 * @author tuannd20
	 * @param  {long} customerId id khach hang can huy
	 * @return {promise}
	 * @since  31/08/2015
	 */
	deleteNewCustomerSync: function(customerId) {
		return CustomerCatalog.rejectNewCustomerSync("/catalog_customer_mng/approve/delete", customerId);
	},
	/**
	 * Tu choi khach hang do nhan vien tao moi
	 * @author tuannd20
	 * @param  {long} customerId id khach hang dang tu choi
	 * @return {promise}
	 * @since  31/08/2015
	 */
	denyNewCustomerSync: function(customerId) {
		return CustomerCatalog.rejectNewCustomerSync("/catalog_customer_mng/approve/deny", customerId);
	},
	/**
	 * Loai bo khach hang
	 * @author tuannd20
	 * @param  {string} url url api
	 * @param  {long} customerId id khach hang
	 * @return {promise}
	 * @since  31/08/2015
	 */
	rejectNewCustomerSync: function(url, customerId) {
		return new Promise(function(resolve, reject) {
			var param = {
				lstCustomerId: [customerId]
			};
			Utils.saveData(param, url, CustomerCatalog._xhrSave, null, function(data) {
				var failCustomerCount = data.hasOwnProperty('errorCustomer') ? data.errorCustomer.length : 0;
				data.error = failCustomerCount != 0
				resolve(data);
			}, null, ' #infoTabContainer ', null, function(data) {
				resolve(data);
			});
		});
	},
	/**
	 * Tu choi khach hang
	 * @author tuannd20
	 * @param  {Array} customerIds danh sach id cua cac khach hang tu choi
	 * @return {void}
	 * @since  31/08/2015
	 */
	denyNewCustomer: function(customerIds) {
		return CustomerCatalog.rejectNewCustomer("/catalog_customer_mng/approve/deny", customerIds);
	},
	/**
	 * Xoa khach hang
	 * @author tuannd20
	 * @param  {Array} customerIds danh sach id cua cac khach hang xoa
	 * @return {void}
	 * @since  31/08/2015
	 */
	deleteNewCustomer: function(customerIds) {
		return CustomerCatalog.rejectNewCustomer("/catalog_customer_mng/approve/delete", customerIds);
	},
	/**
	 * loai bo khach hang moi
	 * @param  {string} url        api's url
	 * @param  {Array} customerIds danh sach id khach hang
	 * @return {void}
	 * @since  31/08/2015
	 */
	rejectNewCustomer: function(url, customerIds) {
		var param = {
			lstCustomerId: customerIds
		};
		Utils.addOrSaveData(param, url, CustomerCatalog._xhrSave, null, function(data) {
			if (data && data.hasOwnProperty('errorCustomer') && !$.isEmptyObject(data.errorCustomer)) {
				var totalCustomer = customerIds.length;
				var failCustomerCount = 0;
				var errMsg = '';
				for (var p in data.errorCustomer) {
					errMsg += Utils.XSSEncode(data.errorCustomer[p]) + '<br>';
					failCustomerCount++;
				}
				$('#successMsg').html('Xử lý thành công ' + (totalCustomer - failCustomerCount) + '/' + totalCustomer);

				if (failCustomerCount != 0) {
					$('#errMsg').html(errMsg);
				}

				CustomerCatalog.showElement('#successMsg', 3000);
				CustomerCatalog.showElement('#errMsg', 3000);
			}
			CustomerCatalog.search();
		}, null, ' #infoTabContainer ', null, null);
	},
	/**
	 * show element
	 * @author tuannd20
	 * @param  {string} elementSelector element's css selector
	 * @param  {integer} timeout        timeout to hide element
	 * @return {void}
	 * @since  31/08/2015
	 */
	showElement: function(elementSelector, timeout) {
		$(elementSelector).show();
		setTimeout(function() {
			$(elementSelector).hide();
		}, timeout);
	},
	fillTxtShopCodeByF9 : function(code, isF9){
		if (code != undefined && code != null && code != '') {
			$('#txtShopCode').val(code).show();
		} else {
			code = $('#txtShopCode').val();
		}
		if (code == '') {
			return;
		}
		if (isF9 == undefined || isF9 == null || !isF9) {
			$('#common-dialog-search-2-textbox').dialog('close');
		}
//		$('#txtShopCode').focus();
		//load lại danh sách NVGH, NVTT
		var params = new Object();
		params.shopCode = code;
		Utils.getJSONDataByAjax(params,'/catalog_customer_mng/get-list-staff-nvgh-nvtt',function(data){
			Utils.bindStaffCbx('#deliveryCode', data.listDeliverStaff);
			Utils.bindStaffCbx('#cashierCode', data.listCashierStaff);
			loadDataComboboxRouting(data.listRouting);
		},null,'POST');
	},
	viewBigMapOnDlgDynamic:function(attr) {
		$('#imgBtnDeleteLatLng,#imgBtnUpdateLatLng,#imgBtnCancelLatLng').show();
		ProductCatalog.viewBigMapOnDlg(attr, function(){
			CustomerCatalog.showMapContent();
		});
	}
};
/*
 * END OF FILE - /web/web/resources/scripts/business/catalog/vnm.customer-catalog.js
 */

/*
 * START OF FILE - /web/web/resources/scripts/business/catalog/vnm.customer-type-catalog.js
 */
var CustomerTypeCatalog = {
	_xhrSave : null,
	_xhrDel: null,
	isContextMenu:0,
	parentsId:null,
	selectnodeId:-2,
	searchModel:null,
	isRefresh:false,
	getGridUrl: function(code,pId,name,status,allItem,skuLevel,saleAmount, isGetParentAndChild){		
		return "/catalog/customer-type/search?code=" + encodeChar(code) 
		+ "&name=" + encodeChar(name) 
		+ "&parentId=" + encodeChar(pId) 
		+ '&status='+ status 
		+ '&allItem=' + encodeChar(allItem)
		+ '&skuLevel=' + skuLevel
		+ '&saleAmount=' + saleAmount
		+ '&isGetParentAndChild=' + (isGetParentAndChild != undefined && isGetParentAndChild != null ? isGetParentAndChild : '');
	},	
	search: function(){
		$('#errMsg').html('').hide();
		var tm = setTimeout(function() {
			$('#code').focus();
		}, 500);
		var code = $('#code').val().trim();
		var name = $('#name').val().trim();
		var status = $('#status').val().trim();
		var pId = CustomerTypeCatalog.selectnodeId;	
		var skuLevel = $('#skuLevel').val().trim();		
		var saleAmount = $('#saleAmount').val().trim();
		CustomerTypeCatalog.searchModel = new Object();
		CustomerTypeCatalog.searchModel.code1 = code;
		CustomerTypeCatalog.searchModel.name1 = name;
		CustomerTypeCatalog.searchModel.status1 = status;
		CustomerTypeCatalog.searchModel.skuLevel1 = skuLevel;
		CustomerTypeCatalog.searchModel.saleAmount1 = saleAmount;
		
		var msg = '';
		var skuLevel = Utils.returnMoneyValue($('#skuLevel').val().trim());
		var saleAmount = Utils.returnMoneyValue($('#saleAmount').val().trim());
		if(skuLevel.length>0 && isNaN(skuLevel)){
			msg = catalog_customer_type_standard_sku_is_not_number ;
			$('#skuLevel').focus();
		}
		if(saleAmount.length>0 && isNaN(saleAmount)){
			msg = catalog_customer_type_target_ds_is_not_number;
			$('#saleAmount').focus();
		}
		if(msg.length > 0){
			$('#errMsg').html(msg).show();
			return false;
		}
		var url = CustomerTypeCatalog.getGridUrl(code,pId,name,status,0,skuLevel,saleAmount);
		if(pId != undefined && pId!=null && pId >0){
			url = null;
			url = CustomerTypeCatalog.getGridUrl(code,pId,name,status,1,skuLevel,saleAmount);
		}
		$("#grid").datagrid({url:url,pageNumber:1});
		return false;
	},
	save: function(){
		var msg = '';
		$('#errMsg').html('').hide();
		msg = Utils.getMessageOfRequireCheck('code',catalog_customer_type_customer_type_code);
		if(msg.length == 0){
			msg = Utils.getMessageOfSpecialCharactersValidate('code',catalog_customer_type_customer_type_code,Utils._CODE);
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfRequireCheck('name',catalog_customer_type_customer_type_name);
		}
//		if(msg.length == 0){
//			msg = Utils.getMessageOfSpecialCharactersValidate('name',catalog_customer_type_customer_type_name,Utils._NAME_CUSTYPE);
//		}	
		if(msg.length == 0){
			msg = Utils.getMessageOfRequireCheck('status',report_status_label,true);
		}
		
		/*if(msg.length == 0){
			msg = Utils.getMessageOfRequireCheck('skuLevel',catalog_customer_type_standard_sku, false, false);
		}
		if (msg.length == 0) {
			var v = $("#skuLevel").val().trim();
			v = v.replace(/,/g, '');
			if (v.length > 15) {
				msg = format(msgErr_invalid_maxlength,catalog_customer_type_standard_sku,15);
			}
		}*/
		
		/*if(msg.length == 0){
			msg = Utils.getMessageOfRequireCheck('saleAmount',catalog_customer_type_target_ds, false, false);
		}
		if (msg.length == 0) {
			var v = $("#saleAmount").val().trim();
			v = v.replace(/,/g, '');
			if (v.length > 15) {
				msg = format(msgErr_invalid_maxlength,catalog_customer_type_target_ds,15);
			}
		}*/
		var skuLevel = Utils.returnMoneyValue($('#skuLevel').val().trim());
		var saleAmount = Utils.returnMoneyValue($('#saleAmount').val().trim());
		if(isNaN(skuLevel) && msg.length == 0){
			msg = catalog_customer_type_standard_sku_is_not_number;
			$('#skuLevel').focus();
		}
		if(isNaN(saleAmount) && msg.length == 0){
			msg = catalog_customer_type_target_ds_is_not_number;
			$('#saleAmount').focus();
		}		
		
		if($('#status').val() != 1 && $('#status').val() != 0 && msg.length == 0){
			msg = catalog_customer_type_chose_status;
		}
		if(msg.length > 0){
			$('#errMsg').html(msg).show();
			return false;
		}
		var dataModel = new Object();
		dataModel.id = $('#selId').val().trim();
		dataModel.code = $('#code').val().trim();
		dataModel.name = $('#name').val().trim();
		dataModel.skuLevel = Utils.returnMoneyValue($('#skuLevel').val().trim());
		dataModel.saleAmount = Utils.returnMoneyValue($('#saleAmount').val().trim());
		dataModel.status = $('#status').val();
		dataModel.parentId = $('#parentCode').combotree('getValue');
		if(dataModel.id != '' && dataModel.status == 0){
			dataModel.changeCus = 0;
		}
		Utils.addOrSaveRowOnGrid(dataModel, "/catalog/customer-type/save", CustomerTypeCatalog._xhrSave, null, null, function(data){
			if(!data.error){
				if(data.hasMessageListCustomer != undefined && data.hasMessageListCustomer != null && data.hasMessageListCustomer !=""){
					return false;
				}
//				CustomerTypeCatalog.selectnodeId = '';
//				$('#contentGrid').show();
//				CustomerTypeCatalog.loadTree();
//				CustomerTypeCatalog.clearData();
//				var url = CustomerTypeCatalog.getGridUrl('','','',1,0,'','');
//				$("#grid").datagrid({url:url,pageNumber:1});
//				CustomerTypeCatalog.loadTree();
				var tree = jQuery.jstree._reference("#tree");
				tree.refresh();
				CustomerTypeCatalog.clearData();
				$('#contentGrid').show();
				if(CustomerTypeCatalog.searchModel != null){
					setTextboxValue('skuLevel',CustomerTypeCatalog.searchModel.skuLevel1);
					setTextboxValue('saleAmount',CustomerTypeCatalog.searchModel.saleAmount1);
					setTextboxValue('code',CustomerTypeCatalog.searchModel.code1);
					setTextboxValue('name',CustomerTypeCatalog.searchModel.name1);
					setSelectBoxValue('status',CustomerTypeCatalog.searchModel.status1);
				}
				CustomerTypeCatalog.search();
				$('#code').focus();
			}			
		});
		
		return false;
	},
	getSelectedRow: function(rowId,parentChannelTypeId){	
		var nodeId = rowId;
		CustomerTypeCatalog.isContextMenu = 1;
		if(nodeId != '-2'){
			CustomerTypeCatalog.parentsId = parentChannelTypeId;
		}else{
			CustomerTypeCatalog.parentsId = '-2';
			return false;
		}
		$.ajax({
			type : "POST",
			url : '/catalog/customer-type/update',
			dataType : "json",
			data :$.param({id : nodeId}, true),
			success : function(result) {
				//hidden div chua grid
				var channelType = result.channelType;
				var status = activeType.STOPPED;
				if(channelType.status === activeStatus){
					status = activeType.RUNNING;
				}
				//Chi co 2 trang thai khi cap nhat laoi khach hang
				$('#status').html('').change();
				$('#status').append('<option value="1">Hoạt động</option>');
				$('#status').append('<option value="0">Tạm ngưng </option>');
				$('#status').change();
				
				$('#selId').val(nodeId.toString());
				$('#code').attr('disabled','disabled');
				$('#parentCode').combotree('disable');
				setTextboxValue('skuLevel',formatCurrency(channelType.sku.toString()));
				setTextboxValue('saleAmount',formatCurrency(channelType.saleAmount.toString()));
				setTextboxValue('code',channelType.channelTypeCode.toString());
				setTextboxValue('name',channelType.channelTypeName.toString());
				setSelectBoxValue('status',status);
				$('#divParentCode').show();
				CustomerTypeCatalog.getChangedForm();
				CustomerTypeCatalog.isContextMenu = 0;
				$('#title').html(catalog_customer_type_information);
				$('#title1').html(catalog_customer_type_information);
				$('#name').focus();
				
			}
    	});
		return false;
	},
	resetForm: function(){
		CustomerTypeCatalog.clearData();	
		$('#code').focus();
		return false;
	},
	deleteRow: function(code){
		var dataModel = new Object();
		dataModel.code = code;	
		Utils.deleteSelectRowOnGrid(dataModel, catalog_customer_type_title, '/catalog/customer-type/delete', CustomerTypeCatalog._xhrDel, null, null,function(){
			CustomerTypeCatalog.loadTree();
			$('#parentCode').combotree('setValue',activeType.ALL);		
			$('#parentCode').combotree("reload",'/rest/catalog/customer-type/combotree/0/0.json');
			$('#parentCode').combotree('setValue',activeType.ALL);
			CustomerTypeCatalog.clearData();
		});
		$('#name').focus();
		return false;
	},
	customMenu : function(node){
//		console.log(node);
		var items =  { // Could be a function that should return an object like this one
            "create" : {
                "separator_before"  : false,
                "separator_after"   : true,
                "label"             : "<span style='font-size:13px'>"+Utils.XSSEncode(catalog_customer_type_add_information)+"</span>",
                "icon"				:"/resources/images/icon_add.png",
                "action"            : function(obj){
                						
                						var nodeId = obj.attr('id').trim();
                						CustomerTypeCatalog.setSelectedNode(nodeId);
                						CustomerTypeCatalog.isContextMenu = 1;
                						CustomerTypeCatalog.parentsId = nodeId;
                						CustomerTypeCatalog.newCusType();
                						disableSelectbox('parentCode');
                						$('#parentCode').combotree('disable', true);
                						var html = "";
                						html += ('<select  class="MySelectBoxClass" style="width:206px">');
                						html += ('<option value="1" selected = "selected">'+Utils.XSSEncode(jsp_common_status_active)+' </option>');
                						html += ('<option value="0">'+Utils.XSSEncode(jsp_common_status_stoped)+' </option>');
                						html += ('</select>');
                						$('#status').html(html);
                						
                }
            },
			"update" : {
				"separator_before"	: false,
				"separator_after"	: false,
				"label"				: "<span style='font-size:13px'>"+Utils.XSSEncode(catalog_customer_type_update_information)+"</span>",
				"icon"				:"/resources/images/icon_edit.png",
				"action"			: function (obj) { 
					var nodeId = obj.attr('id').trim();
					CustomerTypeCatalog.setSelectedNode(nodeId);
					CustomerTypeCatalog.isContextMenu = 1;
					if(nodeId != '-2'){
						CustomerTypeCatalog.parentsId = $.jstree._reference('#tree')._get_parent(obj).attr("id");
					}else{
						CustomerTypeCatalog.parentsId = '-2';
						return false;
					}
					
                	$.ajax({
        				type : "POST",
        				url : '/catalog/customer-type/update',
        				dataType : "json",
        				data :$.param({id : nodeId}, true),
        				success : function(result) {
        					//hidden div chua grid
        					$('#contentGrid').hide();
        					//$('#divGeralMilkBox').show();
        					var channelType = result.channelType;
        					var status = 1;
        					if(channelType.status != activeStatus){
        						status = 0;
        					}
        					$('#selId').val(nodeId.toString());
        					$('#code').attr('disabled','disabled');
        					$('#parentCode').combotree('disable');
        					setTextboxValue('skuLevel',formatCurrency(channelType.sku.toString()));
        					setTextboxValue('saleAmount',formatCurrency(channelType.saleAmount.toString()));
        					setTextboxValue('code',channelType.channelTypeCode.toString());
        					setTextboxValue('name',channelType.channelTypeName.toString());
        					//setSelectBoxValue('parentCode', channelType.parentChannelType.id.toString());
        					setSelectBoxValue('status',status);
        					//disableSelectbox('parentCode');
        					CustomerTypeCatalog.getChangedForm();
        					CustomerTypeCatalog.isContextMenu = 0;
        					$('#divParentCode').show();
        					$('#title').html(catalog_customer_type_information);
        					$('#title1').html(catalog_customer_type_information);
        					$('#name').focus();
        					
        				}
                	});
				}
			}
        };
		if ($(node).attr('id')== '-2') {
			items.update = null;
		}
		return items;
	},
	loadTree: function(){
		//loadDataForTree('/rest/catalog/customer-type/tree.json');
		$('#tree').jstree({
	        "plugins": ["themes", "json_data","ui","contextmenu"],
	        "themes": {
	            "theme": "classic",
	            "icons": false,
	            "dots": true
	        },
	        "json_data": {
	        	"ajax" : {
	                "url" : '/rest/catalog/customer-type/tree.json',
	                "data" : function (n) {
	                        return { id : n.attr ? n.attr("id") : 0 };
	                },
	    	        'complete':function(node){
	    	            	CustomerTypeCatalog.setSelectedNode(CustomerTypeCatalog.selectnodeId);	            	
	    	        }		
	            }        	
	        },
	        "contextmenu" : {
	        	"items" : CustomerTypeCatalog.customMenu
	        }
		});	
		$('#tree').bind("loaded.jstree", function(event, data){
			if($('#treeContainer').data("jsp")!= undefined){
				$('#treeContainer').data("jsp").destroy();
			}
			$('#treeContainer').jScrollPane();
			CustomerTypeCatalog.setSelectedNode("-2");
		});
		
		$('#tree').bind('before.jstree',
			function(event, data) {
				if (data.func == 'refresh') {
					CustomerTypeCatalog.isRefresh = true;
				}
		}); 
		
		$('#tree').bind("show_contextmenu.jstree", function(obj, x,y){
			console.log(obj);
			console.log(x);
			console.log(y);
			
		});

		$('#tree').bind("select_node.jstree",
		function(event, data) {
			$('#status').html('').change();
			$('#status').append('<option selected="selected" value="-2">Tất cả</option>');
			$('#status').append('<option value="1">Hoạt động</option>');
			$('#status').append('<option value="0">Tạm ngưng </option>');
			$('#status').change();
			var name = data.inst.get_text(data.rslt.obj);
			var id = data.rslt.obj.attr("id");
			CustomerTypeCatalog.selectnodeId = id;
			$('#contentGrid').show();
			if (id == -2) {
				id = '';
			} else {
				CustomerTypeCatalog.parentsId = data.inst._get_parent(data.rslt.obj).attr("id"); // lay id node cha
			}
			if (CustomerTypeCatalog.isRefresh) {
				CustomerTypeCatalog.isRefresh = false;
				return false;
			}
			var url = CustomerTypeCatalog.getGridUrl('', id, '', 1, 1,
					'', '', 'true');
			$("#grid").datagrid({
				url : url,
				pageNumber : 1
			});
			CustomerTypeCatalog.resetForm();
			CustomerTypeCatalog.searchModel = null;
		});
	},
	clearData: function(){
		setTitleSearch();
		$('#code').css({'width':'196px'});
		$('#skuLevel').css({'width':'196px'});
		$('#saleAmount').css('width','196px');
		//$('#name').css({'width':'133px'});
		$('#title1').html(catalog_customer_type_title);
		$('.ReqiureStyle').hide();
		$('#divParentCode').hide(); //an ma cha
		$('#selId').val(0);
		$('#errMsg').html('').hide();
		$('#code').removeAttr('disabled');
		$('#parentCode').combotree('enable');
		$('#btnEdit').hide();
		//$('#btnCancel').hide();
		$('#btnSearch').show();
		//$('#btnAdd').show();		
		$('#code').val('');
		$('#name').val('');
		$('#saleAmount').val('');
		$('#skuLevel').val('');
		$('#parentCode').combotree('setValue',activeType.ALL);		
		$('#parentCode').combotree("reload",'/rest/catalog/customer-type/combotree/0/0.json');
		$('#parentCode').combotree('setValue',activeType.ALL);
		setSelectBoxValue('status',1);
	},
	getChangedForm: function(){
		$(".combo-panel .panel-body .panel-body-noheader").css({'width':'197px'});
		$(".panel .combo-p").css({'width':'201px'});
		
		if(CustomerTypeCatalog.isContextMenu == 1){
			if(CustomerTypeCatalog.parentsId == null || CustomerTypeCatalog.parentsId=='' ){
				CustomerTypeCatalog.parentsId = '-2';
			}
			$('#parentCode').combotree('setValue',CustomerTypeCatalog.parentsId);
		}
		$('#divParentCode').show();
		$('.ReqiureStyle').show();
		$('#btnSearch').hide();
		//$('#btnAdd').hide();
		$('#btnEdit').show();
		//$('#btnCancel').show();
		$('#code').focus();
		$('#code').css({'width':'196px'});
		$('#code').attr({'maxLength':'50'});
		$('#name').css({'width':'196px'});
		$('#name').attr({'maxLength':'100'});
		$('#saleAmount').css({'width':'196px'});
		$('#saleAmount').attr({'maxLength':'19'});
		$('#skuLevel').css({'width':'196px'});	
		$('#skuLevel').attr({'maxLength':'19'});	
		$('#title').html(catalog_customer_type_information);
		$('#title1').html(catalog_customer_type_information);
	},
	newCusType:function(){
		enableSelectbox('parentCode');
		$('#contentGrid').hide();
		CustomerTypeCatalog.resetForm();
		CustomerTypeCatalog.getChangedForm();
		$('#parentCode').val("'---"+ Utils.XSSEncode(catalog_customer_type_chose_customer_type) +"---'").change();
	},
	setSelectedNode:function(nodeId){
		$('#tree a').removeClass('jstree-hovered jstree-clicked');
		$('#tree #'+nodeId+' >a').addClass('jstree-clicked');
	}
};
/*
 * END OF FILE - /web/web/resources/scripts/business/catalog/vnm.customer-type-catalog.js
 */

/*
 * START OF FILE - /web/web/resources/scripts/business/catalog/vnm.delivery-group-catalog.js
 */
var DeliveryGroupCatalog = {
	_xhrSave : null,
	_xhrDel: null,
	getGridUrl: function(code,name,staffCode,customerCode,shopId,status,shopCode, allChild){
		if(shopId == undefined || shopId == ''){
			shopId = -2;
		}
		if(shopCode == null || shopCode == undefined){
			shopCode = '';
		}
		return "/catalog/delivery-group/search?code=" + encodeChar(code) + "&name=" + encodeChar(name) + "&staffCode=" + encodeChar(staffCode) + "&customerCode=" + encodeChar(customerCode) + "&shopId=" + encodeChar(shopId) + '&status=' + status + '&shopCode=' + encodeChar(shopCode) + '&getAll=' + encodeChar(allChild);
	},	
	search: function(){
		$('#errMsg').html('').hide();
		$('#errExcelMsg').html('').hide();
		var code = $('#code').val().trim();
		var name = $('#name').val().trim();
		var staffCode = $('#staffCode').val().trim();
		var customerCode = $('#customerCode').val().trim();
		var status = $('#status').val().trim();
		var shopCode = $('#shopCode').val().trim();		
		var shopId = -2;
		var url = DeliveryGroupCatalog.getGridUrl(code,name,staffCode,customerCode,shopId,status,shopCode,true);
		$("#grid").setGridParam({url:url,page:1}).trigger("reloadGrid");
		return false;
	},
	saveInfo: function(){
		var msg = '';
		$('#errMsg').html('').hide();
		msg = Utils.getMessageOfRequireCheck('code','Mã nhóm');
		if(msg.length == 0){
			msg = Utils.getMessageOfSpecialCharactersValidate('code','Mã nhóm',Utils._CODE);
		}
		if(msg.length == 0) {
			msg = Utils.getMessageOfSpecialCharactersValidate('name', 'Tên nhóm', Utils._NAME);
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfRequireCheck('name','Tên nhóm');
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfSpecialCharactersValidate('name','Tên nhóm');
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfRequireCheck('shopCode','Mã đơn vị');
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfSpecialCharactersValidate('shopCode','Mã đơn vị',Utils._CODE);
		}		
		if(msg.length == 0){
			msg = Utils.getMessageOfRequireCheck('staffCode','Mã NVGH');
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfSpecialCharactersValidate('staffCode','Mã NVGH',Utils._CODE);
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfRequireCheck('status','Trạng thái',true);
		}
		if(msg.length > 0){
			$('#errMsg').html(msg).show();
			return false;
		}
		var dataModel = new Object();
		dataModel.id = $('#selId').val().trim();
		dataModel.code = $('#code').val().trim();
		dataModel.name = $('#name').val().trim();
		dataModel.shopCode = $('#shopCode').val().trim();
		dataModel.staffCode = $('#staffCode').val().trim();
		dataModel.status = $('#status').val().trim();
		Utils.addOrSaveData(dataModel, "/catalog/delivery-group/save-info", DeliveryGroupCatalog._xhrSave, null, function(data){			
			$('#selId').val(data.id);
			$('#hidShopCodeTmp').val(Utils.XSSEncode(data.shopCode));
			$('#code').val($('#code').val().trim());
			$('#name').val($('#name').val().trim());
			$('#shopCode').val($('#shopCode').val().trim());
			$('#shopName').val($('#shopName').val().trim());
			DeliveryGroupCatalog.showCustomerTab($('#code').val().trim(), $('#name').val().trim(), $('#shopCode').val().trim(), $('#shopName').val().trim());
		});		
		return false;
	},
	getSelectedRow: function(id){
		location.href = '/catalog/delivery-group/change?id=' + id;
		return false;
	},
	resetForm: function(){
		$('#errMsg').html('').hide();			
		$('#code').val('');
		$('#name').val('');
		$('#staffCode').val('');
		$('#customerCode').val('');
		setSelectBoxValue('status',1);
		$("#grid").trigger("reloadGrid");
		return false;
	},
	deleteRow: function(id){
		var dataModel = new Object();
		dataModel.id = id;	
		Utils.deleteSelectRowOnGrid(dataModel, 'nhóm', '/catalog/delivery-group/delete', DeliveryGroupCatalog._xhrDel, null, 'errExcelMsg');		
		return false;
	},
	openAddForm: function(){
		var selCode = $('#tree').jstree('get_selected').attr('contentItemId');
		if(selCode == undefined){
			selCode = '';			
		}
		location.href = '/catalog/delivery-group/change?shopCode=' + encodeChar(selCode);
		return false;
	},
	shopCodeChanged: function(){
		var code = $('#shopCode').val().trim();
		$.getJSON('/rest/catalog/delivery-group/shop/'+ encodeChar(code) +'/name.json', function(data){
			if(data!=null){
				setTextboxValue('shopName',data.name);					
			}				
		});		
		return false;
	},
	staffCodeChanged: function(){
		var code = $('#staffCode').val().trim();
		if(code != $('#hidStaffCode').val().trim()){
			$.getJSON('/rest/catalog/delivery-group/staff/'+ encodeChar(code) +'/name.json', function(data){
				setTextboxValue('staffName',data.name);
				$('#hidStaffCode').val($('#staffCode').val());
			});
		}
		return false;
	},
	customerCodeChanged: function(){
		var code = $('#customerCode').val().trim();
		var shopCode = $('#shopCode').val().trim();
		if(code != $('#hidCustomerCode').val().trim()){
			$.getJSON('/rest/catalog/delivery-group/customer/'+ encodeChar(code) +'/'+ encodeChar(shopCode) +'/name.json', function(data){
				setTextboxValue('customerName',data.name);
				$('#hidCustomerCode').val($('#customerCode').val());
			});
		}
		return false;
	},
	showInfoTab: function(groupCode, groupName, shopCode, shopName){
		$('#errMsg').html('').hide();
		$('#customerDiv').hide();
		$('#staffDiv').show();	
		$('#statusDiv').show();
		
		$('#tabCusActive').removeClass('Active');
		$('#tabInfoActive').addClass('Active');
		$('#name').val(groupName);
		$('#shopCode').val(shopCode);
		$('#shopName').val(shopName);
		disabled('shopCode');
		if($('#code').val().length = 0 || groupCode.length == 0 || groupCode.length == null) {
			$('#code').removeAttr('disabled');
		}
		$('#name').removeAttr('disabled');
		$('#name').val(groupName);
		$('#shopCode').val(shopCode);
		$('#shopName').val(shopName);
		
		$('#btnAddCustomer').hide();
		$('#gridContainer').hide();
		
		if($('#selId').val().trim().length > 0){
			$('#btnUpdateInfo').show();				
		} else{
			$('#btnAddInfo').show();
		}
		//$('#btnCancel').show();
		return false;
	},
	showCustomerTab: function(groupCode, groupName, shopCode, shopName){
		$('#errMsg').html('').hide();
		$('#tabCusActive').removeClass('Disable');
		$('#staffDiv').hide();
		$('#customerDiv').show();
		$('#statusDiv').hide();
		$('#tabCus').bind('click', function() {
			DeliveryGroupCatalog.showCustomerTab(groupCode, groupName, shopCode, shopName);
		});
				
		$('#tabInfoActive').removeClass('Active');
		$('#tabCusActive').addClass('Active');
		$('#code').val(groupCode);
		$('#code').attr('disabled','disabled');
		$('#name').val(groupName);
		$('#shopCode').val(shopCode);
		$('#shopName').val(shopName);
		$('#name').attr('disabled','disabled');
		$('#shopCode').attr('disabled','disabled');
		//$('#hidShopCode').val($('shopCode').val());
		//$('#shopCode').val($('#hidShopCodeTmp').val());
		
		$('#btnAddInfo').hide();
		$('#btnUpdateInfo').hide();
		//$('#btnCancel').hide();	
		$('#btnAddCustomer').show();
		$('#gridContainer').show();
		
		var id=$('#selId').val().trim();
		var shopCode=$('#shopCode').val().trim();
		$("#grid").jqGrid({
		  url:'/catalog/delivery-group/customer-list?id=' + id + '&shopCode=' + shopCode,
		  colModel:[		
		    {name:'groupTransfer.shop.shopCode', label: 'Mã đơn vị', width: 100, sortable:false,resizable:false, align: 'left' },
		    {name:'groupTransfer.staff.staffCode', label: 'Mã NVGH', width: 100, sortable:false,resizable:false , align: 'left'},
		    {name:'shortCode', label: 'Mã KH', width: 100,sortable:false,resizable:false , align: 'left'},
		    {name:'customerName', label: 'Tên KH', sortable:false,resizable:false, align:'left'},	    
		    {name:'delete', label: 'Xóa', width: 50, align: 'center', sortable:false,resizable:false, formatter: DeliveryGroupCatalogFormatter.delCellIconCustomerFormatter}
		  ],	  
		  pager : '#pager',
		  width: ($('#cusGrid').width())	  
		})
		.navGrid('#pager', {edit:false,add:false,del:false, search: false});
		return false;
	},
	addCustomer: function(){
		var msg = '';
		$('#errMsg').html('').hide();
		msg = Utils.getMessageOfRequireCheck('customerCode','Mã KH');
		if(msg.length == 0){
			msg = Utils.getMessageOfSpecialCharactersValidate('customerCode','Mã KH',Utils._CODE);
		}		
		if(msg.length > 0){
			$('#errMsg').html(msg).show();
			return false;
		}
		var shopCode=$('#shopCode').val().trim();
		var dataModel = new Object();
		dataModel.id = $('#selId').val().trim();
		dataModel.customerCode = $('#customerCode').val().trim();
		dataModel.shopCode = shopCode;
		Utils.addOrSaveRowOnGrid(dataModel, "/catalog/delivery-group/add-customer", DeliveryGroupCatalog._xhrSave, null,null, function(data){
			if(!data.error){
				$('#customerCode').val('');
				$('#customerName').val('');
				$("#grid").trigger("reloadGrid");
			}
		});		
		return false;
	},
	deleteCustomerRow: function(id){
		var dataModel = new Object();
		//dataModel.id = $('#selId').val().trim();
		dataModel.id = id;
		Utils.deleteSelectRowOnGrid(dataModel, "khách hàng","/catalog/delivery-group/delete-customer", DeliveryGroupCatalog._xhrDel, null,null);
		$('#customerCode').val('');
		$('#customerName').val('');
		return false;
	},
	backToParentPage: function(){
		location.href = '/catalog/delivery-group';
		return false;
	},
	loadTreeEx:function(){
		DeliveryGroupCatalog.loadShopTree();
		$('#tree').bind("loaded.jstree", function(event, data){
        	if($('#treeContainer').data("jsp")!= undefined){
        		$('#treeContainer').data("jsp").destroy();
        	}
        	$('#treeContainer').jScrollPane();
        });
	},
	loadShopTree:function(){
		$('#tree').jstree({
	        "plugins": ["themes", "json_data","ui"],
	        "themes": {
	            "theme": "classic",
	            "icons": false,
	            "dots": true
	        },
	        "json_data": {
	            "ajax": {
	            	"url": function( node ){
	                      if( node == -1 ){
	                    	  return "/rest/catalog/unit-tree-ex/"+$('#shopIdHidden').val().trim()+"/0/tree.json";
	                      } else {
	                    	  return "/rest/catalog/unit-tree-ex/"+node.data("attr").id+"/1/tree.json";
	                      }
	                    },
	                "type": 'GET',
	                "success": function(ops) {
	                    data = [];
	                    for( opnum in ops ){
	                      var op = ops[opnum];
	                      node = {
	                          "data" : op.data,
	                          "attr" : op.attr,
	                          "metadata" :  op ,
	                          "state" : op.state
	                      };
	                      data.push( node );
	                    }
	                    return data;
	                },
	                'complete':function(){
	                	$('#tree').bind("select_node.jstree", function (event, data) {
	                		var name = data.inst.get_text(data.rslt.obj);
	                		var id = data.rslt.obj.attr("id");
	                		var code = data.rslt.obj.attr("contentItemId");
	                		$('#selId').val(id);
	                		$('#selCode').val(Utils.XSSEncode(code));
	                		$('#selName').val(Utils.XSSEncode(name));
	                		var allChild = true;		
	                		if(!$('#chkAllChild').is(':checked')){
	                			allChild = false;
	                		}
	                		var url = DeliveryGroupCatalog.getGridUrl('','','','',id,$('#status').val(),'',allChild);
	                		$("#grid").setGridParam({url:url,page:1}).trigger("reloadGrid");
	            	    });
	                	$('#tree').bind("loaded.jstree", function(event, data){
	                		if($('#treeContainer').data("jsp")!= undefined){
	                			$('#treeContainer').data("jsp").destroy();
	                		}
	                		$('#treeContainer').jScrollPane();
	                    });
	                	var tm = setTimeout(function() {
		                    if($('#treeContainer').data("jsp")!= undefined){
		                		$('#treeContainer').data("jsp").destroy();
		                	}
		                    $('#treeContainer').css('height',$('.Content').height()-75);
		  				  	$('#treeContainer').jScrollPane();
	                    }, 500);
	                }
	            }
	        },
	        "core" : {"load_open" : true }
		});	
	},
	exportExcelData:function(){
		$('#divOverlay').addClass('Overlay');
		$('#imgOverlay').show();
		$('#errMsg').html('').hide();
		$('#errExcelMsg').html('').hide();
		var code = $('#code').val().trim();
		var name = $('#name').val().trim();
		var staffCode = $('#staffCode').val().trim();
		var customerCode = $('#customerCode').val().trim();
		var status = $('#status').val().trim();
		var shopCode = $('#shopCode').val().trim();		
		var shopId = -2;
		var data = new Object();
		data.code = code;
		data.name = name;
		data.staffCode = staffCode;
		data.customerCode = customerCode;
		data.status = status;
		data.shopCode = shopCode;
		data.shopId = shopId;
		data.getAll = true;
		data.excelType = $('#excelType').val();
		var url = "/catalog/delivery-group/getlistexceldata";
		CommonSearch.exportExcelData(data,url);
	},
	importExcel:function(){
		$('#isView').val(0);
		var options = { 
				beforeSubmit: ProductLevelCatalog.beforeImportExcel,   
		 		success:      ProductLevelCatalog.afterImportExcelUpdate, 
		 		type: "POST",
		 		dataType: 'html',
		 		data:({excelType:$('#excelType').val()})
		 	}; 
		$('#importFrm').ajaxForm(options);
 		$('#importFrm').submit();
		return false;
	},	
	viewExcel:function(){
		$('#isView').val(1);
		var options = { 
				beforeSubmit: ProductLevelCatalog.beforeImportExcel,   
		 		success:      ProductLevelCatalog.afterImportExcelUpdate, 
		 		type: "POST",
		 		dataType: 'html',
		 		data:({excelType:$('#excelType').val()})
		 	}; 
		$('#importFrm').ajaxForm(options);
 		$('#importFrm').submit();
		return false;
	}
};
/*
 * END OF FILE - /web/web/resources/scripts/business/catalog/vnm.delivery-group-catalog.js
 */

/*
 * START OF FILE - /web/web/resources/scripts/business/catalog/vnm.display-tool-catalog.js
 */
var DisplayToolCatalog = {
	_xhrSave : null,
	_xhrDel: null,
	_selObjType: null,	
	getGridUrl: function(shopCode,supervisorStaffCode,saleStaffCode,customerCode,usedMonth){
		return "/catalog_display_tool/search?shopCode=" + encodeChar(shopCode) + "&supervisorStaffCode=" + encodeChar(supervisorStaffCode) + "&saleStaffCode=" + encodeChar(saleStaffCode)+ "&customerCode=" + encodeChar(customerCode) +"&usedMonth=" + encodeChar(usedMonth);
	},	
	search: function(){
		$('#errMsg').html('').hide();
		var shopCode = $('#shopCode').val().trim();
		var supervisorStaffCode = $('#supervisorStaffCode').val().trim();
		var saleStaffCode = $('#saleStaffCode').val().trim();
		var customerCode = $('#customerCode').val().trim();
		var usedMonth = $('#usedMonth').val().trim();
		var tm = setTimeout(function() {
			$('#supervisorStaffCode').focus();
		}, 500);
		var url = DisplayToolCatalog.getGridUrl(shopCode,supervisorStaffCode,saleStaffCode,customerCode,usedMonth);
		$("#grid").setGridParam({url:url,page:1}).trigger("reloadGrid");
		return false;
	},
	change: function(){
		var msg = '';
		$('#errMsg').html('').hide();
		msg = Utils.getMessageOfRequireCheck('shopCode','Mã đơn vị');
		if(msg.length == 0){
			msg = Utils.getMessageOfSpecialCharactersValidate('shopCode','Mã đơn vị',Utils._CODE);
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfRequireCheck('saleStaffCode','Mã NVBH');
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfSpecialCharactersValidate('saleStaffCode','Mã NVBH',Utils._CODE);
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfRequireCheck('customerCode','Mã KH');
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfSpecialCharactersValidate('customerCode','Mã KH',Utils._CODE);
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfRequireCheck('displayToolCode','Mã tủ');
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfSpecialCharactersValidate('displayToolCode','Mã tủ',Utils._CODE);
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfRequireCheck('quantity','Số lượng');
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfRequireCheck('income','Doanh số');
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfRequireCheck('usedMonth','Tháng sử dụng');
		}
		if(msg.length > 0){
			$('#errMsg').html(msg).show();
			return false;
		}
		var dataModel = new Object();
		dataModel.displayToolId = $('#displayToolId').val().trim();
		dataModel.shopCode = $('#shopCode').val().trim();
		dataModel.supervisorStaffCode = $('#supervisorStaffCode').val().trim();
		dataModel.saleStaffCode = $('#saleStaffCode').val().trim();
		dataModel.customerCode = $('#customerCode').val().trim();
		dataModel.displayToolCode = $('#displayToolCode').val().trim();
		dataModel.quantity = $('#quantity').val().trim();
		dataModel.income = Utils.returnMoneyValue($('#income').val().trim());
		dataModel.usedMonth = $('#usedMonth').val().trim();
		Utils.addOrSaveRowOnGrid(dataModel, "/catalog_display_tool/save", DisplayToolCatalog._xhrSave, null, null,function(){
			DisplayToolCatalog.resetForm();
		});
		return false;
	},
	getSelectedDisplayTool: function(displayToolId,shopCode,supervisorStaffCode,saleStaffCode,customerCode,displayToolCode,quantity,income,usedMonth){
		if(displayToolId!= null && displayToolId!= 0 && displayToolId!=undefined){
			$('#displayToolId').val(displayToolId);
		} else {
			$('#displayToolId').val(0);
		}
		setTextboxValue('shopCode',shopCode);
//		setTextboxValue('supervisorStaffCode',supervisorStaffCode);
		if(displayToolId != null && displayToolId > 0){
//			$('#saleStaffCode').attr('disabled','disabled');
			$('#customerCode').attr('disabled','disabled');
		}
		$('#shopCode').attr('disabled','disabled');
		$('#supervisorStaffCode').attr('disabled','disabled');
		setTextboxValue('saleStaffCode',saleStaffCode);
		setTextboxValue('customerCode',customerCode);	
		setTextboxValue('displayToolCode',displayToolCode);
		setTextboxValue('quantity',quantity);	
		setTextboxValue('income',income);
		Utils.formatCurrencyFor('income');
		setTextboxValue('usedMonth',usedMonth);	
		Utils.getChangedForm(true);
		setTitleUpdate();
		$('#btnSearch,#btnCreate').hide();
		$('#btnUpdate,#btnDismiss').show();
	},
	resetForm: function(){
		$('#errMsg').html('').hide();
		setTitleSearch();
		$('.RequireStyle').hide();
		$('.divBankHidden').hide();
//		$('#saleStaffCode').removeAttr('disabled');
		$('#customerCode').removeAttr('disabled');
		setTextboxValue('saleStaffCode','');
		setTextboxValue('customerCode','');	
		setTextboxValue('displayToolCode','');
		setTextboxValue('quantity','');	
		setTextboxValue('income','');
		setTextboxValue('usedMonth',getCurrentMonth());	
		$('#btnUpdate,#btnDismiss').hide();
		$('#btnCreate,#btnSearch').show();
		if(isBoth == 'false'){
			if(isShopEnable == 'true'){
				$('#shopCode').removeAttr('disabled','disabled');
			}else{
				$('#supervisorStaffCode').removeAttr('disabled','disabled');
			}
		}else{
			$('#shopCode').removeAttr('disabled','disabled');
			$('#supervisorStaffCode').removeAttr('disabled','disabled');
		}
		var url = DisplayToolCatalog.getGridUrl($('#shopCode').val(),$('#supervisorStaffCode').val(),'','',$('#usedMonth').val());
		$("#grid").setGridParam({url:url,page:1}).trigger("reloadGrid");
		return false;
	},
	deleteDisplayTool: function(displayToolId){
		$('#errMsg').html('').hide();
		var dataModel = new Object();
		dataModel.displayToolId = displayToolId;	
		Utils.deleteSelectRowOnGrid(dataModel, 'khách hàng sử dụng tủ', "/catalog_display_tool/remove", DisplayToolCatalog._xhrDel, null, null,function(){
			DisplayToolCatalog.resetForm();
		});		
		return false;		
	}
};
/*
 * END OF FILE - /web/web/resources/scripts/business/catalog/vnm.display-tool-catalog.js
 */

/*
 * START OF FILE - /web/web/resources/scripts/business/catalog/vnm.display-tool-product-catalog.js
 */
var DisplayToolProductCatalog = {
	getGridUrl: function(toolCode, productCode) {
		return "/catalog/display-tool-product/search?toolCode=" + encodeChar(toolCode) + "&productCode=" + encodeChar(productCode);
	},
	search: function() {
		var toolCode = $('#toolCode').val().trim();
		var productCode = $('#productCode').val().trim();
		var tm = setTimeout(function() {
			$('#toolCode').focus();
		}, 3000);
		var url = DisplayToolProductCatalog.getGridUrl(toolCode, productCode);
		$("#grid").setGridParam({url:url,page:1}).trigger("reloadGrid");
	},
	add: function() {
		var msg = '';
		msg = Utils.getMessageOfRequireCheck('toolCode', 'Mã tủ');
		if(msg.length > 0) {
			$('#errMsg').html(msg).show();
			var tm = setTimeout(function() {
				$('#errMsg').html('').hide();
			}, 3000);
			return;
		}
		msg = Utils.getMessageOfRequireCheck('productCode', 'Mã sản phẩm');
		if(msg.length > 0) {
			$('#errMsg').html(msg).show();
			var tm = setTimeout(function() {
				$('#errMsg').html('').hide();
			}, 3000);
			return;
		}
		var toolCode = $('#toolCode').val();
		var productCode = $('#productCode').val();
		var params = new Object();
		params.toolCode = toolCode;
		params.productCode = productCode;
		Utils.addOrSaveData(params, '/catalog/display-tool-product/add', null, 'errMsg', function(data) {
			$("#grid").setGridParam({page:1}).trigger("reloadGrid");
			DisplayToolProductCatalog.changeFormCancel();
		});
	},
	del: function(id) {
		var params = new Object();
		params.id = id;
		$.messager.confirm('Xác nhận', 'Bạn có muốn xóa thông tin này?', function(r){
			if(r) {
				Utils.saveData(params, '/catalog/display-tool-product/delete', null, 'errDelMsg', function(data) {
					$("#grid").trigger("reloadGrid");
				}, null, '', true);
			}
		});
	},
	export: function() {
		var toolCode = $('#toolCode').val();
		var productCode = $('#productCode').val();
		var params = new Object();
		params.toolCode = toolCode;
		params.productCode = productCode;
		var url = "/catalog/display-tool-product/export";
		$.ajax({
			type : "POST",
			url : url,
			data :($.param(params, true)),
			dataType : "json",
			success : function(data) {
				if(data.error && data.errMsg != undefined) {
					$('#errMsg').html(data.errMsg).show();
					var tm = setTimeout(function(){
						$('#errMsg').html('').hide();
						clearTimeout(tm);
					}, 3000);
				} else {
					document.location.href = data.view;
				}
			},
			error:function(XMLHttpRequest, textStatus, errorThrown) {
				$.ajax({
					type : "POST",
					url : '/check-session',
					dataType : "json",
					success : function(data) {
					},
					error: function(XMLHttpRequest, textStatus, errorThrown) {
						window.location.href = '/home';
					}
				});
			}
		});
	},
	changeFormAddNew: function() {
		$('#toolCodeRequire').show();
		$('#productCodeRequire').show();
		$('#btnSearch').hide();
		$('#btnAddNew').hide();
		$('#excelExport').hide();
		$('#btnSave').show();
		$('#btnCancel').show();
	},
	changeFormCancel: function() {
		$('#toolCodeRequire').hide();
		$('#productCodeRequire').hide();
		$('#toolCode').val('');
		$('#productCode').val('');
		$('#btnSearch').show();
		$('#btnAddNew').show();
		$('#excelExport').show();
		$('#btnSave').hide();
		$('#btnCancel').hide();
	}
};
/*
 * END OF FILE - /web/web/resources/scripts/business/catalog/vnm.display-tool-product-catalog.js
 */

/*
 * START OF FILE - /web/web/resources/scripts/business/catalog/vnm.flavour-catalog.js
 */
var FlavourCatalog = {
	_xhrSave : null,
	_xhrDel: null,
	getGridUrl: function(code,name,description,status){
		return "/catalog/flavour/search?code=" + encodeChar(code) + "&name=" + encodeChar(name) + "&note=" + encodeChar(description) + "&status=" + status;
	},	
	search: function(){
		$('#errMsg').html('').hide();
		var code = $('#flavourCode').val().trim();
		var name = $('#flavourName').val().trim();
		var description = $('#description').val().trim();
		var status = 1;
		var tm = setTimeout(function() {
			$('#flavourCode').focus();
		}, 500);
		var url = FlavourCatalog.getGridUrl(code,name,description,status);
		$("#grid").datagrid({url:url,page:1});
		return false;
	},
	save: function(){
		var msg = '';
		$('#errMsg').html('').hide();
		msg = Utils.getMessageOfRequireCheck('flavourCodePop','Mã flavour');
		if(msg.length == 0){
			msg = Utils.getMessageOfSpecialCharactersValidate('flavourCodePop','Mã flavour',Utils._CODE);
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfRequireCheck('flavourNamePop','Tên flavour');
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfSpecialCharactersValidate('flavourNamePop','Tên flavour');
		}

		if(msg.length == 0 && $('#description').val().trim().length > 0){
			msg = Utils.getMessageOfSpecialCharactersValidate('description','Mô tả');
		}

		if(msg.length > 0){
			$('#errMsg').html(msg).show();
			return false;
		}
		var dataModel = new Object();
		dataModel.id = $('#selId').val().trim();
		dataModel.code = $('#flavourCodePop').val().trim();
		dataModel.name = $('#flavourNamePop').val().trim();
		dataModel.note = $('#descriptionPop').val().trim();

		dataModel.status = 1;
		Utils.addOrSaveRowOnGrid(dataModel, "/catalog/flavour/save", FlavourCatalog._xhrSave, null, null, function(data){
			if(!data.error){
				$('#popup1').dialog('close');
				FlavourCatalog.search();
			}
		});		
		return false;
	},
	getSelectedRow: function(rowId, code, name, description,status){	
		
		if(rowId != null && rowId != 0 && rowId !=undefined){
			$('#selId').val(rowId);
		} else {
			$('#selId').val(0);
		}
		if(description == 'null'){
			description= '';
		}
		$('#flavourCodePop').val(code);
		$('#flavourNamePop').val(name);
		$('#descriptionPop').val(description);
        $('#popup1').dialog('open');
		
		return false;
	},
	resetForm: function(){
		FlavourCatalog.clearData();
		$("#grid").trigger("reloadGrid");
		return false;
	},
	deleteRow: function(code){
		var dataModel = new Object();
		dataModel.code = code;	
		Utils.deleteSelectRowOnGrid(dataModel, 'brand', '/catalog/flavour/delete', FlavourCatalog._xhrDel, null, null,function(data){
			FlavourCatalog.clearData();
		});
		return false;
	},
	clearData: function(){
		setTitleSearch();
		$('.RequireStyle').hide();
		$('#selId').val(0);
		$('#errMsg').html('').hide();
		$('#code').removeAttr('disabled');
		$('#btnEdit').hide();
		$('#btnCancel').hide();
		$('#btnSearch').show();
		$('#btnAdd').show();		
		$('#code').val('');
		$('#name').val('');
		$('#description').val('');
		focusFirstTextbox();
		setSelectBoxValue('status', 1);
		FlavourCatalog.search();
	},
	getChangedForm: function(){
		$('.RequireStyle').show();
		$('#btnSearch').hide();
		$('#btnAdd').hide();
		$('#btnEdit').show();
		$('#btnCancel').show();	
		$('#code').focus();
		focusFirstTextbox();
		setTitleAdd();
	}
};
/*
 * END OF FILE - /web/web/resources/scripts/business/catalog/vnm.flavour-catalog.js
 */

/*
 * START OF FILE - /web/web/resources/scripts/business/catalog/vnm.level-catalog.js
 */
var ProductLevelCatalog = {
	_xhrSave : null,
	_xhrDel: null,
	_htmlCat: null,
	_htmlSubCat: null,
	_htmlBrand: null,
	_curRowId: null,
	_lstNVTT: null,
	_rowIndexValueField: null,
	_arrayList: null,
	_mapRowIndex: null,
	_callBackAfterImport: null,
	_xhrReport:null,
	getGridUrl: function(code,name,cat,subCat,status,description){
		return "/catalog_level_mng/search?code=" + encodeChar(code) + "&name=" + encodeChar(name) +"&catId=" + cat + "&subCatId=" + subCat + "&note=" + encodeChar(description) + "&status=" + status;
	},	
	search: function(){
		$('#errMsg').html('').hide();
		$('#errExcelMsg').html('').hide();			
		var code = $('#code').val().trim();	
		var name = $('#name').val().trim();
		var cat = $('#category').val().trim();
		var subCat = $('#subCategory').val().trim();			
		var description = $('#description').val().trim();
		var status = $('#status').val().trim();
		var tm = setTimeout(function() {
			$('#code').focus();
		}, 500);
		
		var url = ProductLevelCatalog.getGridUrl(code,name,cat,subCat,status,description);
		$("#grid").setGridParam({url:url,page:1}).trigger("reloadGrid");
		return false;
	},
	save: function(){
		var msg = '';
		$('#errMsg').html('').hide();
		$('#errExcelMsg').html('').hide();	
//		msg = Utils.getMessageOfRequireCheck('code','Mã Mức');
		if(msg.length == 0){
			msg = Utils.getMessageOfSpecialCharactersValidate('code','Mã Mức',Utils._CODE);
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfRequireCheck('name','Tên Mức');
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfSpecialCharactersValidate('name','Tên Mức',Utils._NAME);
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfRequireCheck('category','Mã category',true);
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfRequireCheck('subCategory','Mã sub category',true);
		}			
		if(msg.length == 0){
			msg = Utils.getMessageOfRequireCheck('status','Trạng thái',true);
		}
		if(msg.length == 0 && $('#description').val().trim().length > 0){
			msg = Utils.getMessageOfSpecialCharactersValidate('description','Mô tả');
		}
		if(msg.length > 0){
			$('#errMsg').html(msg).show();
			return false;
		}
		var dataModel = new Object();
		dataModel.id = $('#selId').val().trim();
		dataModel.code = $('#code').val().trim();
		dataModel.name = $('#name').val().trim();
		dataModel.catId = $('#category').val().trim();
		dataModel.subCatId = $('#subCategory').val().trim();			
		dataModel.note = $('#description').val().trim();
		dataModel.status = $('#status').val();
		Utils.addOrSaveRowOnGrid(dataModel, "/catalog_level_mng/save", ProductLevelCatalog._xhrSave, null, null, function(data){
			if(!data.error){
				ProductLevelCatalog.resetForm();
			}
		});			
		return false;
	},
	getSelectedRow: function(rowId,status){	
		ProductLevelCatalog._curRowId = rowId;
		//ProductLevelCatalog.resetAllCombobox();			
		$('#errExcelMsg').html('').hide();
		$('#errMsg').html('').hide();			
		$('#code').attr('disabled','disabled');
		var id =  $("#grid").jqGrid ('getCell', rowId, 'id');
		var catId =  $("#grid").jqGrid ('getCell', rowId, 'cat.id');
		var catCode =  $("#grid").jqGrid ('getCell', rowId, 'cat.productInfoCode');
		var subCatId =  $("#grid").jqGrid ('getCell', rowId, 'subCat.id');
		var subCatCode =  $("#grid").jqGrid ('getCell', rowId, 'subCat.productInfoCode');			
		var description =  $("#grid").jqGrid ('getCell', rowId, 'description');
		var code = $("#grid").jqGrid ('getCell', rowId, 'productLevelCode');
		var name = $("#grid").jqGrid ('getCell', rowId, 'productLevelName');
		if(id!= null && id!= 0 && id!=undefined){
			$('#selId').val(id);
		} else {
			$('#selId').val(0);
		}	
		ProductLevelCatalog.getProductInfo(1,activeType.RUNNING);
		ProductLevelCatalog.getProductInfo(2,activeType.RUNNING);					
		setTextboxValue('code',code);
		setTextboxValue('name',name);
		setTextboxValue('description',description);
		//setSelectBoxValue('category',catId);			
		//setSelectBoxValue('subCategory',subCatId);
		setSelectBoxValue('status',status);		
		$('.RequireStyle').show();
		$('#btnSearch').hide();
		$('#btnAdd').hide();
		$('#btnEdit').show();
		$('#btnCancel').show();
		$('#code').focus();
		setTitleUpdate();
		focusFirstTextbox();
		return false;
	},
	resetForm: function(){
		ProductLevelCatalog.clearData();
		ProductLevelCatalog.search();
		return false;
	},
	deleteRow: function(code){
		//ProductLevelCatalog.clearData();
		var dataModel = new Object();
		dataModel.code = code;	
		Utils.deleteSelectRowOnGrid(dataModel, 'mức', '/catalog_level_mng/delete', ProductLevelCatalog._xhrDel, 'grid', null, function(data){
			ProductLevelCatalog.resetForm();
		});
		$('#errExcelMsg').html('').hide();	
		return false;
	},
	clearData: function(){
		setTitleSearch();
		ProductLevelCatalog._curRowId = null;
		ProductLevelCatalog.getProductInfo(1,activeType.RUNNING);
		ProductLevelCatalog.getProductInfo(2,activeType.RUNNING);
		$('#selId').val(0);
		setSelectBoxValue('category');
		setSelectBoxValue('subCategory');			
		setSelectBoxValue('status',1);
		$('#errMsg').html('').hide();	
		$('#errExcelMsg').html('').hide();	
		$('#code').removeAttr('disabled');
		$('#btnEdit').hide();
		$('#btnCancel').hide();
		$('#btnSearch').show();
		$('#btnAdd').show();
		//ProductLevelCatalog.resetAllCombobox();
		$('#code').val('');			
		$('#name').val('');		
		$('#description').val('');			
		//ProductLevelCatalog.search();
		$('.RequireStyle').hide();
		focusFirstTextbox();
	},
	resetAllCombobox: function(){
		if(ProductLevelCatalog._htmlCat!= null){
			$('#category').html(ProductLevelCatalog._htmlCat);
			$('#category').change();
		}
		if(ProductLevelCatalog._htmlSubCat!= null){
			$('#subCategory').html(ProductLevelCatalog._htmlSubCat);
			$('#subCategory').change();
		}			
	},
	getChangedForm: function(){
		ProductLevelCatalog._curRowId = null;
		ProductLevelCatalog.getProductInfo(1,activeType.RUNNING);
		ProductLevelCatalog.getProductInfo(2,activeType.RUNNING);
		$('.RequireStyle').show();
		$('#btnSearch').hide();
		$('#btnAdd').hide();
		$('#btnEdit').show();
		$('#btnCancel').show();
		$('#code').attr('disabled','disabled');
		$('#name').focus();
		focusFirstTextbox();
		setTitleAdd();
	},
	getProductInfo: function(type, status){
		$.getJSON('/rest/catalog/product-info/list.json?type='+ type +'&status=' + status, function(data){
			var headerText = '';
			var id = '';
			var colId = '';
			var colName = '';
			if(type == 1){
				headerText = 'mã category';
				id = 'category';
				colId = 'cat.id';
				colName = 'cat.productInfoCode';
			} else if(type = 2){
				headerText = 'mã sub category';
				id = 'subCategory';
				colId = 'subCat.id';
				colName = 'subCat.productInfoCode';
			}
			var arrHtml = new Array();
			arrHtml.push('<option value="'+ activeType.ALL +'">--- Chọn '+ Utils.XSSEncode(headerText) +' ---</option>');
			var j=0;
			for(j=0; j<data.length;j++){
				arrHtml.push('<option value="'+ data[j].value +'">'+ Utils.XSSEncode(data[j].name) +'</option>');
			}			
			$('#' + id).html(arrHtml.join(""));
			$('#' + id).change();			
			if(ProductLevelCatalog._curRowId!= null && ProductLevelCatalog._curRowId!= undefined){				
				var gId =  $("#grid").jqGrid ('getCell', ProductLevelCatalog._curRowId, colId);
				var gName =  $("#grid").jqGrid ('getCell', ProductLevelCatalog._curRowId, colName);				
				if($('#'+ id +' option[value='+ gId +']').html()== null){
					var htmlCat = $('#' + id).html() + '<option value="'+ gId +'">'+ Utils.XSSEncode(gName) +'</option>';
					$('#' + id).html(htmlCat);
					$('#' + id).change();					
				}
				setSelectBoxValue(id, gId);
			}
		});
	},
	exportExcelData:function(){
		showLoadingIcon();
		$('#errMsg').html('').hide();
		$('#errExcelMsg').html('').hide();
		var code = $('#code').val().trim();	
		var name = $('#name').val().trim();
		var cat = $('#category').val().trim();
		var subCat = $('#subCategory').val().trim();			
		var description = $('#description').val().trim();
		var status = $('#status').val().trim();
		var data = new Object();
		data.code = code;
		data.name = name;
		data.catId = cat;
		data.subCatId = subCat;
		data.note = description;
		data.status = status;
		var url = "/catalog_level_mng/getlistexceldata";
		CommonSearch.exportExcelData(data,url);
	},
	importExcel:function(){
		$('#isView').val(0);
		$('#importFrm').submit();
		return false;
	},	
	viewExcel:function(){
		$('#isView').val(1);
		$('#importFrm').submit();
		return false;
	},
	afterImportExcelUpdate: function(responseText, statusText, xhr, $form){
		hideLoadingIcon();
		if (statusText == 'success') {				
	    	$("#responseDiv").html(responseText);		 
	    	var newToken = $('#responseDiv #newToken').val();
	    	console.log(newToken);
	    	if(newToken != null && newToken != undefined && newToken != '') {
	    		$('#token').val(newToken);	    		
	    		$('#tokenImport').val(newToken);
	    	}
	    	if($('#errorExcel').html().trim() == 'true' || $('#errorExcelMsg').html().trim().length > 0){
	    		$('#errExcelMsg').html($('#errorExcelMsg').html()).show();
	    	} else {
	    		var totalRow = parseInt($('#totalRow').html().trim());
    			var numFail = parseInt($('#numFail').html().trim());
    			var fileNameFail = $('#fileNameFail').html();
    			var mes = format(msgErr_result_import_excel,(totalRow - numFail),numFail);		    		
    			if(numFail > 0){
    				mes+= ' <a href="'+ fileNameFail +'">Xem chi tiết lỗi</a>';
    			}
    			if($('#excelFile').length!=0 && $('#fakefilepc').length!=0){
    				try{$('#excelFile').val('');$('#fakefilepc').val('');}catch(err){}
    			}
    			$('#errExcelMsg').html(mes).show();
	    	}
	    }
	},
	afterImportExcelUpdateSaleMT: function(responseText, statusText, xhr, $form){
		hideLoadingIcon();
		if (statusText == 'success') {				
	    	$("#responseDiv").html(responseText);
	    	
	    	//LocTT1 - Dec11, 2013
	    	var token = $('#responseToken').html();
	    	$('#token').val(token);
	    	
	    	if($('#errorExcel').html().trim() == 'true' || $('#errorExcelMsg').html().trim().length > 0){
	    		$('#errExcelMsg').html($('#errorExcelMsg').html()).show();
	    	} else {
	    		if($('#typeView').html().trim() == 'false'){
	    			var totalRow = parseInt($('#totalRow').html().trim());
	    			var numFail = parseInt($('#numFail').html().trim());
	    			var fileNameFail = $('#fileNameFail').html();
	    			var mes = format(msgErr_result_import_excel,(totalRow - numFail),numFail);		    		
	    			if(numFail > 0){
	    				mes+= ' <a href="'+ fileNameFail +'">Xem chi tiết lỗi</a>';
	    			}
	    			if($('#excelFile').length!=0 && $('#fakefilepc').length!=0){//xóa link khi import
	    				try{$('#excelFile').val('');$('#fakefilepc').val('');}catch(err){}
	    			}
	    			$('#errExcelMsg').html(mes).show();
//	    			$("#grid").datagrid("loadData");
	    			$("#grid").datagrid("load");
	    			$("#gridLevel").datagrid("load");
	    			if(ProductLevelCatalog._callBackAfterImport != null){
		    			ProductLevelCatalog._callBackAfterImport.call(this);
		    		}
	    		}else{
	    			var obj = '';
	    			if($('#excelType').val() == '' || $('#excelType').val() == undefined ){
	    				obj = '#popup1';
	    			}else{
	    				if(parseInt($('#excelType').val()) == 1){
	    					obj = '#popup1';
	    				}else if(parseInt($('#excelType').val()) == 2){
	    					obj = '#popup2';
	    				}else if(parseInt($('#excelType').val()) == 3){
	    					obj = '#popup3';
	    				}else if(parseInt($('#excelType').val()) == 4){
	    					obj = '#popup4';
	    				}else if(parseInt($('#excelType').val()) == 5){
	    					obj = '#popup5';
	    				}else{
	    					obj = '#popup6';
	    				}
	    			}
	    			var html = $(obj).html();
	    			$.fancybox(html,
    					{
    						modal: true,
    						title: 'Danh sách dữ liệu từ excel',
    						afterShow: function(){
    							$(obj).html('');
    							updateRownumWidthForJqGrid('.fancybox-inner');
    							$('.ScrollSection').jScrollPane().data().jsp;
    							$('.ScrollBodySection').jScrollPane();
    							
    						},
    						afterClose: function(){
    							$(obj).html(html);
    						}
    					}
    				);
	    		}
	    	}
	    }	
	}
};
/*
 * END OF FILE - /web/web/resources/scripts/business/catalog/vnm.level-catalog.js
 */

/*
 * START OF FILE - /web/web/resources/scripts/business/catalog/vnm.level-income-catalog.js
 */
var LevelIncomeCatalog ={
	_xhrSave : null,
	_xhrDel: null,
	_listCategory:null,
	getGridUrl: function(categoryType,levelCode,levelName,fromIncome,toIncome,status){
		return "/catalog/level-income/search?lstcategory=" + categoryType 
		+ "&levelCode=" + encodeChar(levelCode) 
		+ "&levelName=" + encodeChar(levelName) 
		+ "&fromAmountFilter=" + fromIncome
		+ "&toAmountFilter=" + toIncome
		+ "&status=" + status;
	},
	search: function(){
		$('#errMsg').html('').hide();

		var levelCode = $('#saleLevelCode').val().trim();
		var levelName = $('#saleLevelName').val().trim();
		var status = $('#status').val();
		var tm = setTimeout(function() {
			$('#categoryName').focus();
		}, 500);
		var arrayCategory = new Object();
		arrayCategory.listCategory = LevelIncomeCatalog._listCategory;
		var msg = '';
		var toIncome = Utils.returnMoneyValue($('#toIncome').val().trim());
		var fromIncome = Utils.returnMoneyValue($('#fromIncome').val().trim());
		if(fromIncome.length>0 && isNaN(fromIncome)){
			msg = "Từ DS không phải là giá trị số.";
			$('#fromIncome').focus();
		}
		if(toIncome.length>0 && isNaN(toIncome)){
			msg = "Đến DS không phải là số.";
			$('#toIncome').focus();
		}
		if(msg.length > 0){
			$('#errMsg').html(msg).show();
			return false;
		}
		var url = LevelIncomeCatalog.getGridUrl(arrayCategory.listCategory,levelCode,levelName,fromIncome,toIncome,status);
		$("#grid").datagrid({url:url,pageNumber:1});
		return false;
	},
	getSelectedLevelIncome: function(proInfoId,categoryTypeCode,categoryTypeId,levelCode,levelName,fromIncome,toIncome,status){
		$('#errMsg').html('').hide();
		//$('#selObjType').val(staffObjectType);
		if (proInfoId != null && proInfoId != 0 && proInfoId != undefined) {
			$('#levelIncomeId').val(proInfoId);
		} else {
			$('#levelIncomeId').val(0);
		}
		$('#saleLevelCode1').attr('disabled', true);
		setTextboxValue('saleLevelCode1', levelCode);
		setTextboxValue('saleLevelName1', levelName);
		setTextboxValue('fromIncome1', formatCurrency(fromIncome));
		setTextboxValue('toIncome1', formatCurrency(toIncome));
		setSelectBoxValue('status1', status);
		setSelectBoxValue('categoryType1', categoryTypeId);
		$('#categoryTypeId').val(categoryTypeId);
		$('#popup1').dialog({  
		    title: catalog_sales_brand_update_seasonal_by_industry
		});
		disableSelectbox('categoryType1');
		$('#divDialog').css('visibility','visible');
		$('#popup1').dialog('open');
	},
	change: function(){
		var msg = '';
		$('#errMsg').html('').hide();
		msg = Utils.getMessageOfRequireCheck('categoryType1',catalog_sales_brand_NH_Code,true);
		if(msg.length == 0){
			msg = Utils.getMessageOfRequireCheck('saleLevelCode1',catalog_sales_brand_standard_code);
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfSpecialCharactersValidate('saleLevelCode1',catalog_sales_brand_standard_code,Utils._CODE);
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfRequireCheck('saleLevelName1',catalog_sales_brand_standard_name);
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfSpecialCharactersValidate('saleLevelName1',catalog_sales_brand_standard_name,Utils._NAME);
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfRequireCheck('status1',unit_tree_search_unit_shop_status,true);
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfRequireCheck('fromIncome1',catalog_sales_brand_from_amount);
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfNegativeNumberCheck('fromIncome1',catalog_sales_brand_from_amount);
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfInvaildNumber('fromIncome1',catalog_sales_brand_from_amount);
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfNegativeNumberCheck('toIncome1',catalog_sales_brand_to_amount);
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfInvaildNumber('toIncome1',catalog_sales_brand_to_amount);
		}
		if(msg.length == 0){
			if($('#fromIncome1').val().trim() != '' && $('#toIncome1').val().trim() != ''){
				var fromIncome = Utils.returnMoneyValue($('#fromIncome1').val().trim());
				var toIncome = Utils.returnMoneyValue($('#toIncome1').val().trim());
				if(parseInt(fromIncome) > parseInt(toIncome)){
					msg = catalog_sales_brand_from_amount_to_amount;
				}
			}
		}
		if(msg.length > 0){
			$('#errMsg').html(msg).show();
			return false;
		}
		var dataModel = new Object();
		dataModel.levelIncomeId = $('#levelIncomeId').val();
		dataModel.categoryType = $('#categoryType1').val();
		dataModel.levelCode = $('#saleLevelCode1').val().trim();
		dataModel.levelName = $('#saleLevelName1').val().trim();
		dataModel.status = $('#status1').val();
		dataModel.fromIncome = Utils.returnMoneyValue($('#fromIncome1').val().trim());
		dataModel.toIncome = Utils.returnMoneyValue($('#toIncome1').val().trim());
		Utils.addOrSaveData(dataModel, "/catalog/level-income/save", LevelIncomeCatalog._xhrSave, null,function(data){
			if(data.error == false){
				$('#popup1').dialog('close');
				LevelIncomeCatalog.search();
			}else{
				$('#popup1').dialog('close');
				LevelIncomeCatalog.reset();
			}
			
		});
		return false;
	},
	addLevelIncome:function(){
		LevelIncomeCatalog.reset();
		$('#divDialog').css('visibility','visible');
		$('#popup1').dialog({  
		    title: catalog_customer_add_level_income
		});
		$('#popup1').dialog('open');
	},
	reset:function(){
		$('#levelIncomeId').val(0);
		$('#categoryTypeId').val(0);
		setSelectBoxValue('status1', 1);
		setSelectBoxValue('categoryType1', 0);
		$('#saleLevelCode1').attr('disabled', false);
		$('#saleLevelCode1').val('');
		$('#saleLevelName1').val('');
		$('#fromIncome1').val('');
		$('#toIncome1').val('');
		$('#divDialog').css('visibility','hidden');
		enableSelectbox('categoryType1');
		$('#errMsg').html('').hide();
	},
	deleteRow: function(levelIncomeId){
		var dataModel = new Object();
		dataModel.levelIncomeId = levelIncomeId;	
		Utils.deleteSelectRowOnGrid(dataModel, catalog_sales_brand_target, "/catalog/level-income/remove", LevelIncomeCatalog._xhrDel, null, null);		
		return false;		
	},
	getCategoryName:function(categoryType,obj){
		$.getJSON('/rest/catalog/category/name/' + categoryType + ".json",function(data){
			$(obj).val(categoryType);
		});
	},
	getListLevel:function(categoryType,obj){
		$.getJSON('/rest/catalog/category/' + categoryType + '/level/list'+".json",function(data){
			var arrHtml = new Array();
			arrHtml.push('<option value="-1">--- '+ Utils.XSSEncode(catalog_sales_brand_chose_standard) +'---</option>');
			for(var i=0;i<data.length;i++){
				arrHtml.push('<option value="'+ data[i].name +'">'+ Utils.XSSEncode(data[i].name) +'</option>');
			}
			$(obj).html(arrHtml.join(""));
			$(obj).change();
		});
	},
	ViewFileUpload: function(typeView){
		if ($('#excelFile').val() == '') {
			$('#errExcelMsg').html(msgCommon8).show();
			return false;
		}
		if (typeView == 1)
		{
			var options = { 
			 		beforeSubmit: ProductLevelCatalog.beforeImportExcelA,   
			 		success:      ProductLevelCatalog.afterImportExcelA,  
			 		type: "POST",
			 		dataType: 'html',   
			 		data:({typeView:typeView})
			 	}; 
			$('#importFrm').ajaxForm(options);
			$('#importFrm').submit();
		}
		else
		{
			var options = { 
					beforeSubmit: ProductLevelCatalog.beforeImportExcel,   
			 		success:      ProductLevelCatalog.afterImportExcel,
			 		type: "POST",
			 		dataType: 'html',
			 		data:({typeView:typeView})
			 	}; 
			$('#importFrm').ajaxForm(options);
			$.messager.confirm(jsp_common_xacnhan, msgCommon3, function(r){
				if (r){
					$('#importFrm').submit();
				}
			});
		}
	},
	
	exportExcel: function(){
		var categoryType = $('#categoryType').val().trim();
		var categoryName =  $('#categoryName').val().trim();		
		var status = $('#status').val().trim();		
		var level = $('#level').val().trim();
		
		var data = new Object();
		data.categoryType = categoryType;
		data.categoryName = categoryName;
		data.status = status;
		data.level = level;
		var url = "/catalog/level-income/export-excel";
		ReportUtils.exportReport(url,data);
	}
};
/*
 * END OF FILE - /web/web/resources/scripts/business/catalog/vnm.level-income-catalog.js
 */

/*
 * START OF FILE - /web/web/resources/scripts/business/catalog/vnm.packing-catalog.js
 */
var PackingCatalog = {
	_xhrSave : null,
	_xhrDel: null,
	getGridUrl: function(packingCode,packingName,note,status){
		return "/catalog/packing/search?code=" + encodeChar(packingCode) + "&name=" + encodeChar(packingName) + "&note=" + encodeChar(note) + "&status=" + status;
	},	
	search: function(){
		$('#errMsg').html('').hide();
		var code = $('#packingCode').val().trim();
		var name = $('#packingName').val().trim();
		var note = $('#note').val().trim();
		var status = 1;
		var tm = setTimeout(function() {
			$('#packingCode').focus();
		}, 500);
		var url = PackingCatalog.getGridUrl(code,name,note,status);
		$("#grid").datagrid({url:url,page:1});
		return false;
	},
	change: function(){
		var msg = '';
		$('#errMsg').html('').hide();
		msg = Utils.getMessageOfRequireCheck('packingCodePop','Mã packing');
		if(msg.length == 0){
			msg = Utils.getMessageOfSpecialCharactersValidate('packingCodePop','Mã packing',Utils._CODE);
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfRequireCheck('packingNamePop','Tên packing');
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfSpecialCharactersValidate('packingNamePop','Tên packing');
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfSpecialCharactersValidate('packingNotePop','Ghi chú');
		}
		if(msg.length > 0){
			$('#errMsg').html(msg).show();
			return false;
		}
		var dataModel = new Object();
		dataModel.id = $('#packingId').val().trim();
		dataModel.code = $('#packingCodePop').val().trim();
		dataModel.name = $('#packingNamePop').val().trim();
		dataModel.note = $('#packingNotePop').val().trim();
		dataModel.status = 1;
		Utils.addOrSaveRowOnGrid(dataModel, "/catalog/packing/save", PackingCatalog._xhrSave, null,null, function(data){
			if(!data.error){
				$('#popup1').dialog('close');
				PackingCatalog.search();
			}
		});		
		return false;
	},
	getSelectedPacking: function(rowId, code, name, note, status){	
		
		if(rowId != null && rowId != 0 && rowId != undefined){
			$('#packingId').val(rowId);
		} else {
			$('#packingId').val(0);
		}
		if(note == 'null'){
			note= '';
		}
		$('#packingCodePop').val(code);
		$('#packingNamePop').val(name);
		$('#packingNotePop').val(note);
		$('#divDialog').css('visibility','visible');
		$('#popup1').dialog('open');
		return false;
	},
	resetForm: function(){
		$('.RequireStyle').hide();
		setTitleSearch();
		$('#packingId').val(0);
		$('#errMsg').html('').hide();
		$('#code').removeAttr('disabled');
		$('#btnUpdate').hide();
		$('#btnDismiss').hide();
		$('#btnCreate').show();
		$('#btnSearch').show();
		$('#code').val('');
		$('#code').focus();
		$('#name').val('');
		$('#note').val('');
		setSelectBoxValue('status', 1);		
		PackingCatalog.search();
		return false;
	},
	deletePacking: function(packingCode){
		$('#errMsg').html('').hide();
		var dataModel = new Object();
		dataModel.code = packingCode;	
		Utils.deleteSelectRowOnGrid(dataModel, 'packing', '/catalog/packing/remove', PackingCatalog._xhrDel, null, null,function(data){
			PackingCatalog.resetForm();
		});
		return false;
	}
};
/*
 * END OF FILE - /web/web/resources/scripts/business/catalog/vnm.packing-catalog.js
 */

/*
 * START OF FILE - /web/web/resources/scripts/business/catalog/vnm.product-catalog.js
 */
var ProductCatalog = {
	_xhrSave : null,
	_xhrDel: null,	
	_listCategory:null,
	countArray:null,
	_flagDropzone: false,
	dropzoneView: null,
	_isBkLatLng :false,
	latbk : '',
	lngbk : '',
	gotoTab: function(tabIndex){
		ProductCatalog.deactiveAllMainTab();
		switch (tabIndex) {
		case 0:
			$('#tabContent1').show();
			$('#infoTab').addClass('Active');
			break;
		case 1:
			if ($('#productId').val().trim() == '') {
				$('#tabContent1').show();
				$('#infoTab').addClass('Active');
				break;
			}
			$('#tabContent2').show();
			$('#imageTab').addClass('Active');
			ProductCatalog.loadImage();
			break;
		default:
			$('#tabContent1').show();
			break;
		}
		$('#errExcelMsg').html('').hide();
	},
	deactiveAllMainTab: function(){
		$('#infoTab').removeClass('Active');
		$('#imageTab').removeClass('Active');
		$('#tabContent1').hide();
		$('#tabContent2').hide();
	},
	getGridUrl: function(productCode, productName, categories, subCategories, brandId, flavourId, packingId, uom1, status, isExceptZCat, isPriceValid){
		var param = '';
		if (isExceptZCat != null && isExceptZCat != undefined){
			param += "&isExceptZCat=" +isExceptZCat;
		}
		if (isExceptZCat != null && isExceptZCat != undefined){
			param += "&isPriceValid=" +isPriceValid;
		}
		if (categories != null && categories.length > 0) {
			for (var i = 0; i < categories.length; i++) {
				param += '&lstCategoryId=' + categories[i];
			}
		}		
		if (subCategories != null && subCategories.length > 0) {
			for (var i = 0; i < subCategories.length; i++) {
				param += '&lstSubCategoryId=' + subCategories[i];
			}
		}
		if (brandId != null && brandId != undefined) {
			param += "&brandId=" + brandId;
		}
		if (flavourId != null && flavourId != undefined) {
			param += "&flavourId=" + flavourId;
		}
		if (packingId != null && packingId != undefined) {
			param += "&packingId=" + packingId;
		}
		return "/catalog/product/search?productCode=" + encodeChar(productCode) + "&productName=" + encodeChar(productName) + "&uom1=" + uom1 + "&status=" + status + param;
	},
	
	getImageGridUrl: function(productId){
		return "/catalog/product/searchimage?productId=" + encodeChar(productId);
	},
 	
	search: function(){
		$('#errMsg').html('').hide();
		var productCode = $('#productCode').val().trim();
		var productName = $('#productName').val().trim();
		
		var brand = $('#brand').val().trim();
		if (brand == '-2') {
			brand = '';
		}
		var flavour = $('#flavour').val().trim();
		if (flavour == '-2') {
			flavour = '';
		}
		var packing = $('#packing').val().trim();
		if (packing == '-2') {
			packing = '';
		}
		
		var uom1 = $('#uom1').val().trim();
		if (uom1 == '-2') {
			uom1 = '';
		}
		var status = $('#status').val().trim();
		setTimeout(function() {
			$('#productCode').focus();
		}, 500);
		
		var arrCat = $('#category').val();
		if (arrCat == null || (arrCat.length > 0 && arrCat[0] == 0)) {
			arrCat = [];
		}
		
		var arrSubCat = $('#categoryChild').val();
		if (arrSubCat == null || (arrSubCat.length > 0 && arrSubCat[0] == 0)) {
			arrSubCat = [];
		}
		
		var url = ProductCatalog.getGridUrl(productCode, productName, arrCat, arrSubCat, brand, flavour, packing, uom1, status, true);
		$("#grid").datagrid('options').sortName = null; // search thi load lai sort binh thuong
		$("#grid").datagrid({url:url,pageNumber:1});
		return false;
	},
	
	searchImage: function(){
		$('#errMsg').html('').hide();
		var productId = $('#productId').val().trim();
		var url = ProductCatalog.getImageGridUrl(productId);
		$("#grid").setGridParam({url:url,pageNumber:1}).trigger("reloadGrid");
		return false;
	},
	
	loadInfo:function(){
		var productId = $("#productId").val();
		var data = new Object();
		data.productId = productId;
		var kData = $.param(data, true);		
		$.ajax({
			type : "POST",
			url : "/catalog/product/info",
			data : (kData),
			dataType: "html",
			success : function(data) {
				$("#divGeneral").html(data).show();
				if(productId != null && productId == 0){
					$('#productCode').focus();
				}else if(productId != null && productId >0){
					$('#productName').focus();
				}
				Utils.bindAutoSave();
				Utils.bindAutoCombineKey();
			},
			error:function(XMLHttpRequest, textStatus, errorThrown) {
				$.ajax({
					type : "POST",
					url : '/check-session',
					dataType : "json",
					success : function(data) {
					},
					error: function(XMLHttpRequest, textStatus, errorThrown) {
						window.location.href = '/home';
					}
				});
			}
		});
		
		$('#productName').focus();		
		return false;
	},
	
	loadImage:function(seq){
		var editor = null;
		var productId = $("#productId").val().trim();
		var data = new Object();
		data.productId = productId;
		var kData = $.param(data, true);
		$.ajax({
			type : "POST",
			url : "/catalog/product/image",
			data : (kData),
			dataType: "html",
			success : function(data) {
				$("#divGeneral").html(data).show();
				editor = CKEDITOR.replace('description');
				if(seq!=undefined && seq!=null){
					$('#sucMsgUpload').html(seq).show();
				}
				ProductCatalog.countArray = new Map();
			},
			error:function(XMLHttpRequest, textStatus, errorThrown) {
				$.ajax({
					type : "POST",
					url : '/check-session',
					dataType : "json",
					success : function(data) {
						editor = CKEDITOR.replace('description');
					},
					error: function(XMLHttpRequest, textStatus, errorThrown) {
						window.location.href = '/home';
					}
				});
			}
		});
		return false;
	},
	
	changeProductInfo: function(){
		var msg = '';
		$('#errMsg').html('').hide();
		msg = Utils.getMessageOfRequireCheck('productCode', catalog_display_product_code);
		if(msg.length == 0){
			msg = Utils.getMessageOfSpecialCharactersValidate('productCode', catalog_display_product_code, Utils._CODE);
			$('#productCode').focus();
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfRequireCheck('productName', catalog_product_name);
			$('#productName').focus();
		}
		if (msg.length == 0) {
			msg = Utils.getMessageOfRequireCheck('category', catalog_categoryvn);
			$('#category').focus();
		}
		if (msg.length == 0) {
			msg = Utils.getMessageOfRequireCheck('categoryChild', catalog_categoryvn_child);
			$('#categoryChild').focus();
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfRequireCheck('orderIndex', catalog_product_order_index);
			$('#orderIndex').focus();
		}
		if(msg.length == 0){
			var orderIndex = $('#orderIndex').val().trim();
			if (orderIndex < 1) {
				msg = catalog_product_order_index_more_than_zero;
			}
			$('#orderIndex').focus();
		}
		var barCode = $('#barCode').val().trim();
		if(msg.length == 0 && barCode.length > 0 && !/[0-9]{1,}$/.test(barCode)){
			msg = catalog_product_ma_vach_phai_so_nguyen;
			$('#barCode').focus();
		}
		
		var expiryNo = $('#expireNumber').val().trim();
		expiryNo = expiryNo.replace(/,/g, '');
		if(msg.length == 0 && expiryNo != '' && !/\d$/.test(expiryNo)){
			msg = catalog_product_thsd_nam_trong_khong_chin;
			$('#expireNumber').focus();
		}
		if(msg.length==0 && expiryNo.length > 3){
			msg = catalog_product_thsd_vuot_qua_ba_kytu;
			$('#expireNumber').focus();
		};
		var expiryType = $('#expiryType').val().trim();
		if (msg.length == 0 && expiryNo.length > 0 && expiryType == -1) {
			msg = catalog_product_thsd_chon_loai_hsd;
			$('#expiryType').focus();
		}
		if (msg.length == 0 && expiryNo.length <= 0 && expiryType != -1) {
			msg = catalog_product_thsd_nhap_hsd;
			$('#expireNumber').focus();
		}
		
		if(msg.length == 0){
			msg = Utils.getMessageOfRequireCheck('category',catalog_categoryvn,true);
			$('#category').focus();
		}

		if(msg.length == 0){
			msg = Utils.getMessageOfRequireCheck('categoryChild',catalog_categoryvn_child,true);
			$('#category').focus();
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfRequireCheck('uom1', catalog_product_uom1,true);
			$('#uom1').focus();
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfRequireCheck('uom2', catalog_product_uom2,true);
			$('#uom2').focus();
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfRequireCheck('convfact', catalog_proudct_convfact_sl);
			if(msg.length != 0){
				$('#convfact').focus();
			}
		}
		var regex = /^\d*\.?\d*$/;
		var convfact = $('#convfact').val().trim();
		convfact = convfact.replace(/,/g, '');
		if(msg.length == 0 && convfact.length > 0 && !/\d$/.test(convfact)){
			msg = catalog_product_dong_goi_nam_trong_khong_chin;
			$('#convfact').focus();
		}

		var volume = $('#volumn').val().trim();
		if (msg.length == 0 && volume.length > 9) {
			msg = catalog_product_the_tich_gioi_han_chin_kytu;
			$('#volumn').focus();
		}
		volume = volume.replace(/,/g, '');
		if(msg.length == 0 && !regex.test(volume)){
			msg = catalog_product_the_tich_khong_dung_dinh_dang;
			$('#volumn').focus();
		}
		var netWeight = $('#netWeight').val().trim();
		if (msg.length == 0 && netWeight.length > 20) {
			msg = catalog_product_khoi_luong_tinh_gioi_han_haimuoi_kytu;
			$('#netWeight').focus();
		}
		netWeight = netWeight.replace(/,/g, '');
		if(msg.length == 0 && !regex.test(netWeight)){
			msg = catalog_product_khoi_luong_tinh_khong_dung_dinh_dang;
			$('#netWeight').focus();
		}
		var grossWeight = $('#grossWeight').val().trim();
		if (msg.length == 0 && grossWeight.length > 20) {
			msg = catalog_product_tong_kl_gioi_han_haimuoi_kytu;
			$('#grossWeight').focus();
		}
		grossWeight = grossWeight.replace(/,/g, '');
		if(msg.length == 0 && !regex.test(grossWeight)){
			msg = catalog_product_tong_kl_khong_dung_dinh_dang;
			$('#grossWeight').focus();
		}

		if(msg.length == 0 && parseFloat(netWeight)<=0 ){
			msg = catalog_product_net_weight_minus;
			$('#netWeight').focus();
		}
		if(msg.length == 0 && parseFloat(grossWeight)<=0 ){
			msg = catalog_product_tong_kl_phai_so_nguyen;
			$('#grossWeight').focus();
		}
		if(parseFloat(netWeight)>parseFloat(grossWeight)){
			msg = catalog_product_netgross;
			$('#netWeight').focus();
		}
		
		if(msg.length == 0 && parseFloat(volume)<=0 ){
			msg = catalog_product_volumn_minus;
			$('#volumn').focus();
		}
		var id = $('#productId').val().trim();
		if (id != null && id != '') {
			if(msg.length == 0) {
				msg = Utils.getMessageOfRequireCheck('status', jsp_common_status_all,true);
				$('#status').focus();
			}
		}
		if(msg.length > 0){
			$('#errMsg').html(msg).show();
			return false;
		}
		
		var dataModel = new Object();
		dataModel.orderIndex = $('#orderIndex').val().trim();
		var id = $('#productId').val().trim();
		if (id != null && id != '') {
			dataModel.productId = id;
		}
		Utils.addOrSaveData(dataModel, "/catalog/product/checkDulicateOrderIndex", ProductCatalog._xhrSave, null,function(data) {
			if(data.error != undefined && data.error == false){
				if(data.isDulicate) {
					$.messager.confirm(jsp_common_xacnhan, catalog_product_order_index_duplicate, function(r) {  
						if (r) {
							ProductCatalog.postChangeProductInfo();
						}
					});
				} else {
					ProductCatalog.postChangeProductInfo();
				}			
			} else {
				$('#errMsg').val(data.errMsg);
			}
		},null,null,null,null,null,true);
		
	},
	
	postChangeProductInfo: function(productId){			
		var dataModel = new Object();
		var id = $('#productId').val().trim();
		var isCreate = true;
		if (id != null && id != '') {
			dataModel.productId = id;
			isCreate = false;
			dataModel.status = $('#status').val().trim();
		}
		dataModel.productCode = $('#productCode').val().trim();
		dataModel.productName = $('#productName').val().trim();
		dataModel.shortName = $("#shortName").val().trim();
		dataModel.barCode = $('#barCode').val().trim();
		dataModel.category = $('#category').val();
		dataModel.subCat = $('#categoryChild').val();
		dataModel.orderIndex = $('#orderIndex').val();
		dataModel.flavourId = $('#flavour').val();
		dataModel.brandId = $('#brand').val().trim();
		dataModel.packingId = $('#packing').val();
		dataModel.uom1 = $('#uom1').val().trim();
		dataModel.uom2 = $('#uom2').val().trim();
		var netWeight = $('#netWeight').val().trim();
		netWeight = netWeight.replace(/,/g, '');
		dataModel.netWeight =  netWeight;
		
		var grossWeight = $('#grossWeight').val().trim();
		grossWeight = grossWeight.replace(/,/g, '');
		dataModel.grossWeight = grossWeight;
		
		var volume = $('#volumn').val().trim();
		volume = volume.replace(/,/g, '');
		dataModel.volumn = volume;
		
		var convfact = $('#convfact').val().trim();
		convfact = convfact.replace(/,/g, '');
		dataModel.convfact = convfact;
		dataModel.checkLot = $('#lot').val();
		
		var expiryNo = $('#expireNumber').val().trim();
		expiryNo = expiryNo.replace(/,/g, '');
		dataModel.expireNumber = expiryNo;
		dataModel.expireType = $('#expiryType').val().trim();

		if(!Utils.validateAttributeData(dataModel, '#errMsg', '#propertyTabContainer ')) {
			return false;
		}
		
		Utils.saveData(dataModel, "/catalog/product/changeproductinfo", ProductCatalog._xhrSave, null,function(data) {
			if(data.error != undefined && data.error == false){
				$('#productId').val(data.productId);
				$('#convfactHidden').val(Number(data.product.convfact));
				if (isCreate) {
					setTimeout(function(){
						window.location.href = '/catalog/product/viewdetail?productId=' + data.productId;
					}, 3000);
				}
			} else {
				$('#errMsg').val(data.errMsg);
			}
		});
		return false;
	},
	
	changeProductDescription:function(){
		var msg = '';
		$('#errMsg').html('').hide();
		if(msg.length > 0){
			$('#errMsg').html(msg).show();
			return false;
		}
		var dataModel = new Object();
		dataModel.productId = $('#productId').val();
		dataModel.introductionId = $('#introductionId').val();
		dataModel.description = Utils.XSSEncode(CKEDITOR.instances['description'].getData());
		Utils.addOrSaveData(dataModel, "/catalog/product/changeproductdescription", ProductCatalog._xhrSave, null,
				function(rs) {
					setTimeout(function() {
						$('#tabContent2 .TextEditorInSection #successMsg').html('').hide();
					}, 3000);
		}, null, '#tabContent2 .TextEditorInSection');
		return false;
	},
	/** upload images*/
	changeProductImage:function(productId){
		$('#errMsgUpload').html('').hide();
		var msg = ''; 
		if(msg.lenght==0){
			msg = Utils.getMessageOfSpecialCharactersValidate('imageFile', catalog_product_ten_hinh_anh, Utils._NAME);
		}
		if(msg.lenght==0){
			msg = Utils.getMessageOfSpecialCharactersValidate('videoFile', catalog_product_ten_video,Utils._NAME);
		}
		var size = parseInt($('#vnmTotalImage').val().trim()) + ProductCatalog.countArray.size();
		if( size>20){
			msg = catalog_product_upload_toi_da_hinh_sp;
		}
		if(msg.length==0 && $('#fileQueue').html().length>0 && ProductCatalog.countArray.size()==0){
			msg = catalog_product_ko_chon_hinh_sp;
		}
		if(msg.length>0){
			$('#errMsgUpload').html(msg).show();
			return false;
		}
		
		$('#divOverlay').show();
		$('#imageFile').uploadifySettings('scriptData',{
			'productId':productId,
			'currentUser':currentUser
		});
		if($('#videoMsg').html() != "" && parseInt($('#mediaType').val()) == 1){
			$('#videoMsg').html(catalog_product_sp_mot_video).show();
		}
		$('#imageFile').uploadifyUpload();
		if($('#fileQueue').html() == "" || parseInt($('#successUpload').val().trim()) == 1){
			$('#importFrm').submit();
		}
		return false;
	},
	/**BEGIN UPLOAD IMAGE**/
	initUploadImageElement: function(elementSelector, elementId, clickableElement) {
		// Get the template HTML and remove it from the doumenthe template HTML and remove it from the doument
		var previewNode = document.querySelector("#template");
		previewNode.id = "";
		var previewTemplate = previewNode.parentNode.innerHTML;
		previewNode.parentNode.removeChild(previewNode);

		UploadUtil.initializeUploadElement(elementSelector, elementId, {
			url: "/catalog/product/uploadImageFile",
			paramName: function(fileOrderNumber) {
				return 'images';
			},
			autoProcessQueue: false,
			parallelUploads: 1000,
			uploadMultiple: true,
			thumbnailWidth: 50,
			thumbnailHeight: 50,
			previewTemplate: previewTemplate,
			autoQueue: false,
			previewsContainer: "#previews",
			acceptedFiles: "video/*, image/*",
			maxFilesize: 20,// MB
			clickable: clickableElement,
			successmultiple: function(files, responseData) {
				Utils.updateTokenForJSON(responseData);
				UploadUtil.hideTotalProgressBar();
				if (responseData != undefined) {
					var failCount = 0;
					if (responseData.fail && responseData.fail.length > 0) {
						failCount = responseData.fail.length;
						UploadUtil.readdFailFileForUpload(responseData.fail);
					} else {
						UploadUtil.clearAllSuccessUploadedFile();
					}
					$('#divOverlay').hide();
					var msg = format(catalog_product_video_tai_tc_ha_phim, (files.length - failCount) + '/' + files.length);
					$('#sucMsgUpload').html(msg).show();
					if(responseData.errMsg != null && responseData.errMsg != ""){
						if (responseData.failFormat && responseData.failFormat.length > 0) {
							UploadUtil.readdFailFileForUpload(responseData.failFormat);
						}
						$('#errMsgUpload').html(responseData.errMsg).show();
					} else {
						ProductCatalog.loadImage(msg);
					}
				} else {
					$('#divOverlay').hide();
					if (responseData.failFormat && responseData.failFormat.length > 0) {
						UploadUtil.readdFailFileForUpload(responseData.failFormat);
					}
					$('#errMsgUpload').html(responseData.errMsg).show();
				}
			},
		});
		
	},
	uploadImageFile: function(idProduct) {
		$('.ErrorMsgStyle').html('').hide();
		$('.SuccessMsgStyle').html('').hide();
		var errView = 'errMsgUpload';
		ProductCatalog.updateAdditionalDataForUpload(idProduct);
		UploadUtil.startUpload(errView);
		
	},
	updateAdditionalDataForUpload: function(idProduct) {		
		var data = JSONUtil.getSimpleObject2({
			productId: idProduct
		});
		UploadUtil.updateAdditionalDataForUpload(data);
	},
	/** END UPLOAD IMAGE**/
	deleteMediaItem: function(){
		var dataModel = new Object();
		dataModel.mediaItemId = $('#mediaItemId').val();
		$.messager.confirm(jsp_common_xacnhan, catalog_product_video_ban_co_muon_xoa, function(r){
			if (r){
				Utils.saveData(dataModel, "/catalog/product/removemediaitem", ProductCatalog._xhrDel, null,function(data){
					ProductCatalog.loadImage();
				},null,null,true);
			}
		});
		$('.messager-window').css('border-radius', '0');
		return false;		
	},

	
	viewBigMapOnDlg : function(id, callBack) {
		var selector = $('#' + id);
		if(ProductCatalog._isBkLatLng == false){
			var latLngbk = selector.val().trim();
			if(!isNullOrEmpty(latLngbk) && latLngbk.indexOf(';')>=0){
				ProductCatalog.latbk = latLngbk.split(';')[0];
				ProductCatalog.lngbk = latLngbk.split(';')[1];
			}
			ProductCatalog._isBkLatLng = true;		
		}
		var html = $('#viewBigMap').html();
		$('#viewBigMap').dialog({
			closed: false,  
			title : xem_ban_do,
	        cache: false,  
	        modal: true,
	        onOpen: function(){
	        	$('.easyui-dialog #imgBtnDeleteLatLng').unbind('click');
	        	$('.easyui-dialog #imgBtnDeleteLatLng').bind('click',function(){
	        		selector.val('');
	        		ViettelMap.clearOverlays();
	        		ViettelMap._listOverlay = new Array(); 
	        		if (ViettelMap._marker != null){
	                	ViettelMap._marker.setMap(null);
	                }
	                
	        	});
	        	$('.easyui-dialog #imgBtnUpdateLatLng').unbind('click');
	        	$('.easyui-dialog #imgBtnUpdateLatLng').bind('click',function(){
	        		var lat = '';
					var lng = '';
					var latLng = selector.val().trim();
					if(!isNullOrEmpty(latLng) && latLng.indexOf(';')>=0){
						lat = latLng.split(';')[0];
						lng = latLng.split(';')[1];
					}
					var zoom = 12;
					if(lat.trim().length==0 || lng.trim().length==0){
						zoom = ViettelMap._DEF_ALL_ZOOM;
					}
					VTMapUtil.loadMapResource(function(){
						ViettelMap.loadBigMapEx('bigMapContainer', lat, lng, zoom, function(latitude, longtitude) {
							selector.val(latitude + ';' + longtitude);
						}, true, true);
		    		});
		    		$('#viewBigMap').dialog('close');
	        	});
	        	$('.easyui-dialog #imgBtnCancelLatLng').unbind('click');
	        	$('.easyui-dialog #imgBtnCancelLatLng').bind('click',function(){
	        		var zoom = 12;
					if(ProductCatalog.latbk.trim().length==0 || ProductCatalog.lngbk.trim().length==0){
						zoom = ViettelMap._DEF_ALL_ZOOM;
					}
					VTMapUtil.loadMapResource(function(){
						ViettelMap.loadBigMapEx('bigMapContainer', ProductCatalog.latbk, ProductCatalog.lngbk, zoom, function(latitude, longtitude) {
							selector.val(latitude + ';' + longtitude);
						}, true, true);
		    		});
	        		//$('#viewBigMap').onClose();
	        	});
	        	var tm = setTimeout(function(){					
					var lat = '';
					var lng = '';
					var latLng = selector.val().trim();
					if(!isNullOrEmpty(latLng) && latLng.indexOf(';')>=0){
						lat = latLng.split(';')[0];
						lng = latLng.split(';')[1];
					}
					var zoom = 12;
					if(lat.trim().length==0 || lng.trim().length==0){
						zoom = ViettelMap._DEF_ALL_ZOOM;
					}
					VTMapUtil.loadMapResource(function(){
						ViettelMap.loadBigMapEx('bigMapContainer', lat, lng, zoom, function(latitude, longtitude) {
							selector.val(latitude + ';' + longtitude);
						}, true);
		    		});
					clearTimeout(tm);
				}, 500);
	        },
	        onClose:function() {
	        	$('#viewBigMap').html(html);
	        	if (callBack != undefined && callBack != null) {
	        		callBack.call(this);
	        	}
	        }
		});
		
	},
	
	resetApParam:function(){
		var msg = '';
		$('#errExcelMsg').html('').hide();
		msg = Utils.getMessageOfRequireCheck('apParam','Chọn trường xóa dữ liệu',true);
		if(msg.length > 0){
			$('#errExcelMsg').html(msg).show();
			return false;
		}
		var dataModel = new Object();
		var apParamCode = $('#apParam').val().trim();
		dataModel.apParamCode = apParamCode;
		Utils.deleteSelectRowOnGrid(dataModel, $('#apParam').val().trim(), "/catalog/product/resetapparam", ProductCatalog._xhrDel, null,'errExcelMsg',null);
		return false;
	},
	
	getIntegerFromFloat:function(price){
		var a = Utils.returnMoneyValue(price);
		if((a).indexOf('.') > 0){
			var e= a.split('.');
			var c = e[1].substring(0,1);
			if(c >= 5){
				c =parseInt(e[0])+1;
			}else{
				c=parseInt(e[0]);
			}
			price = c;
		}
		return price;
	},
	getListMediaItem:function(type){
		if(type == 0){
			if($('#numIndex').val() != null && parseInt($('#numIndex').val()) > 0){
				$('#numIndex').val(parseInt($('#numIndex').val())-1);
				if($('#numIndex').val() == 0){
					$('#pageUp').hide();
				}
			}
			$('#pageDown').show();
		}else{
			$('#numIndex').val(parseInt($('#numIndex').val())+1);
			if($('#numIndex').val() == Math.floor($('#numPage').val()/8)){
				$('#pageDown').hide();
			}
			$('#pageUp').show();
		}
		$.ajax({
			type : "POST",
			url : "/catalog/product/getlistmediaitem",
			data : ({productId:$('#productId').val(),numIndex:$('#numIndex').val()}),
			dataType: "html",
			success : function(data) {
				$('#divMI').html(data);
			},
			error:function(XMLHttpRequest, textStatus, errorThrown) {
				$.ajax({
					type : "POST",
					url : '/check-session',
					dataType : "json",
					success : function(data) {
					},
					error: function(XMLHttpRequest, textStatus, errorThrown) {
						window.location.href = '/home';
					}
				});
			}
		});
	},
	
	exportExcelData:function(){
		$('#errMsg').html('').hide();
		$('#errExcelMsg').html('').hide();
		$('#sucExcelMsg').html('').hide();
		$.messager.confirm(jsp_common_xacnhan, catalog_product_export_file, function(r){
			if (r){
				showLoadingIcon();
				var dataModel = new Object();
				
				dataModel.productCode = $('#productCode').val().trim();
				dataModel.productName = $('#productName').val().trim();

				var uom1 = $('#uom1').val().trim();
				if (uom1 == '-2') {
					uom1 = '';
				}
				dataModel.uom1 = uom1;
				
				var brandId = $('#brand').val().trim();
				if (brandId == '-2') {
					brandId = '';
				}
				dataModel.brandId = brandId;
				
				var flavourId = $('#flavour').val().trim();
				if (flavourId == '-2') {
					flavourId = '';
				}
				dataModel.flavourId = flavourId;
				
				var packingId = $('#packing').val().trim();
				if (packingId == '-2') {
					packingId = '';
				}
				dataModel.packingId = packingId;
				
				dataModel.status = $('#status').val().trim();
				var arrCat = $('#category').val();
				if (arrCat == null || (arrCat.length > 0 && arrCat[0] == 0)) {
					arrCat = [];
				}
				dataModel.lstCategoryId =  arrCat;
				var arrSubCat = $('#categoryChild').val();
				if (arrSubCat == null || (arrSubCat.length > 0 && arrSubCat[0] == 0)) {
					arrSubCat = [];
				}
				dataModel.lstSubCategoryId =  arrSubCat;
				var url = "/catalog/product/export-product";
				ReportUtils.exportReport(url, dataModel, 'errExcelMsg');
			}
		});
 		return false;
	},
	searchPackageByConvfact:function(convfact){		
		var obj = new Object();
		obj.productId = Number($('#productId').val());
		obj.convfact = convfact;
		Utils.getJSONDataByAjaxNotOverlay(obj,'/catalog/product/search-package',function(data){
			if(!data.error) {				
				var innerHtml ='<option value="-2" selected="selected" >-- Lựa chọn --</option>';
				$(data.lstPackage).each(function(i,e){
					innerHtml += ' ';
					innerHtml += '<option value="' + Utils.XSSEncode(e.productCode) + '"';
					
					innerHtml += '>';
					innerHtml += Utils.XSSEncode(e.productCode) +' - ' + Utils.XSSEncode(e.productName);
					innerHtml += '</option>';
				});
				$('#baoBi').html(innerHtml);
				$('#baoBi').change();
			}
		});
	},
 	viewExcel:function(){
 		$('#isView').val(1);
 		var options = { 
				beforeSubmit: ProductLevelCatalog.beforeImportExcel,   
		 		success:      ProductLevelCatalog.afterImportExcelUpdate, 
		 		type: "POST",
		 		dataType: 'html',
		 		data:({excelType:$('#excelType').val()})
		 	}; 
		$('#importFrm').ajaxForm(options);
		$('#importFrm').submit();
 		return false;
 	},
	
	/**
	 * LacNV
	 * get category and fill to category select box
	 */
	fillCategoriesToMultiSelect: function(isCheck) {
		$.ajax({
			url: '/catalog/product/get-category?isOrderByCode=true',
			method: 'GET',
			success: function(rs) {
				if (rs.error != undefined && !rs.error) {
					var lstCategory = rs.rows;
					//$('#category').removeAttr('disabled');
					var html = '';
					if(lstCategory.length > 0){
						if(isCheck == 1)
							{
								html += '<option value="0" selected="selected">' + Utils.XSSEncode(jsp_common_status_all) +'</option>';
							}else{
								html += '<option value="0" >' + Utils.XSSEncode(jsp_common_status_all) + '</option>';
							}
						for(var i=0; i < lstCategory.length; i++){
							if(isCheck == 1){
							html += '<option selected="selected" value="' + lstCategory[i].id +'">' +
								Utils.XSSEncode(lstCategory[i].productInfoCode) + ' - ' +
								Utils.XSSEncode(lstCategory[i].productInfoName) + '</option>';
							}else{	
								html += '<option  value="' + lstCategory[i].id +'">' +
									Utils.XSSEncode(lstCategory[i].productInfoCode) + ' - ' +
									Utils.XSSEncode(lstCategory[i].productInfoName) + '</option>';									
							}
						}
						$('#category').html(html);
						$('#category').change();
						$('#ddcl-category').remove();
						$('#ddcl-category-ddw').remove();
						$('#category').dropdownchecklist({
							emptyText: catalog_product_chon_nganh_hang,
							firstItemChecksAll: true,
							maxDropHeight: 350
						});
					} else {
						$('#category').html('');
						//$('#category').attr('disabled','disabled');
						$('#category').dropdownchecklist({
							emptyText:'-------------------',
							firstItemChecksAll: true,
							maxDropHeight: 350
						});
					}
					$('.ui-dropdownchecklist-text').css({"padding-left":"5px"});
				}
			}
		});
	},
	
	fillCategoryChildToMultiSelect: function(isCheck) {
		var param = '';
		var categories = $('#category').val();
		if (categories != null && categories.length > 0) {
			for (var i = 0; i < categories.length; i++) {
				param += '&lstProductInfoId=' + categories[i];
			}
		}
		$.ajax({
			url: '/catalog/product/get-category-child?status=2' +param,
			method: 'GET',
			success: function(rs) {
				if (rs.error != undefined && !rs.error) {
					var lstCategory = rs.rows;
					//$('#categoryChild').removeAttr('disabled');
					var html = '';
					if(lstCategory.length > 0){
						if(isCheck == 1)
							{
								html += '<option value="0" selected="selected">' + Utils.XSSEncode(jsp_common_status_all) + '</option>';
							}else{
								html += '<option value="0" >' + Utils.XSSEncode(jsp_common_status_all) +'</option>';
							}
						for(var i=0; i < lstCategory.length; i++){
							if(isCheck == 1){
								html += '<option selected="selected" value="' + Utils.XSSEncode(lstCategory[i].id) +'">' +
									Utils.XSSEncode(lstCategory[i].productInfoCode) + ' - ' +
									Utils.XSSEncode(lstCategory[i].productInfoName) + '</option>';
							}else{	
								html += '<option  value="' + lstCategory[i].id +'">' +
									Utils.XSSEncode(lstCategory[i].productInfoCode) + ' - ' +
									Utils.XSSEncode(lstCategory[i].productInfoName) + '</option>';									
							}
						}
						$('#categoryChild').html(html);
						$('#categoryChild').change();
						$('#ddcl-categoryChild').remove();
						$('#ddcl-categoryChild-ddw').remove();
						$('#categoryChild').dropdownchecklist({
							emptyText: catalog_product_chon_nganh_hang,
							firstItemChecksAll: true,
							maxDropHeight: 350
						});
					} else {
						$('#categoryChild').html('');
						$('#ddcl-categoryChild').remove();
						$('#ddcl-categoryChild-ddw').remove();
						//$('#category').attr('disabled','disabled');
						$('#categoryChild').dropdownchecklist({
							emptyText:'-------------------',
							firstItemChecksAll: true,
							maxDropHeight: 350
						});
					}
					$('.ui-dropdownchecklist-text').css({"padding-left":"5px"});
				}
			}
		});
	},
	
	/**
	 * LacNV
	 * get uom (1 or 2) and fill to uom select box has id
	 */
	fillUoms: function(val, type, objectId, hasAll) {
		$.ajax({
			url: '/catalog/product/get-uom?uom1=' + type,
			method: 'GET',
			success: function(rs) {
				if (rs.error != undefined && !rs.error) {
					var lstUom = rs.lstUom;
					var html = '';
					if (hasAll != undefined && hasAll==1) {
						html = '<option value="-2" selected="selected">' + Utils.XSSEncode(jsp_common_status_all) + '</option>';
					}
					for(var i=0; i < lstUom.length; i++){
						html += '<option value="' + Utils.XSSEncode(lstUom[i].apParamCode) +'">' +
							Utils.XSSEncode(lstUom[i].apParamName) + '</option>';  
					}
					$('#' + objectId).html(html);
					$('#' + objectId).find("option[value=" + val +']').attr('selected','selected');
					$('#' + objectId).change();
				}
			}
		});
	},
	
	/**
	 * LacNV
	 * get category and fill to category select box
	 */
	fillProductInfoToSelect: function(val, type, objectId, hasAll) {
		if (type == undefined) {
			type = 1; // category
		}
		if (objectId == undefined) {
			objectId = 'category';
		}
		var txt = '';
		if(type == 1){
			//var txt = '-- Chọn ngành hàng --';
			txt = catalog_product_chon_nganh_hang;
		}
		if (type == 3) {
			//txt = '-- Chọn nhãn hiệu --';
			txt = catalog_product_chon_nhan_hieu;
		} else if (type == 4){
			//txt = '-- Chọn hương vị --';
			txt = catalog_product_chon_huong_vi;
		} else if (type == 5){
			//txt = '-- Chọn loại bao bì --';
			txt = catalog_product_chon_bao_bi;
		}
		$.ajax({
			url: '/catalog/product/get-category?status=' + type,
			method: 'GET',
			success: function(rs) {
				if (rs.error != undefined && !rs.error) {
					var lstCategory = rs.rows;
					
					var html = '<option value="-2" selected="selected">' + Utils.XSSEncode(txt) + '</option>';
					if (hasAll != undefined && hasAll==1) {
						html = '<option value="-2" selected="selected">' + Utils.XSSEncode(jsp_common_status_all) + '</option>';
					}
					for(var i=0; i < lstCategory.length; i++){
						html += '<option value="' + lstCategory[i].id +'">' +
							Utils.XSSEncode(lstCategory[i].productInfoCode) + ' - ' +
							Utils.XSSEncode(lstCategory[i].productInfoName) + '</option>';  
					}
					$('#' + objectId).html(html);
					$('#' + objectId).find("option[value=" + val +']').attr('selected','selected');
					$('#' + objectId).change();
					/**vuongmq, 16/01/2015, danh sach nganh hang con**/
					if(type == 1){
						//objectId = 'categoryChild';
						type = 2; // nganh hang conbrandSelectBox
						$('#category').attr('onclick','ProductCatalog.changeCategory(this, null,' +type+',"categoryChild",' +hasAll+')');
					}
				}
			}
		});
	},
	/**
	 * vuongmq, 16/01/2014
	 * view danh sach nganh hang con
	 */
	changeCategory: function(idHtml, val, type, objectId, hasAll, valView){
		var txt ='';
		if (type == 2) { // category child
			//txt = '-- Chọn ngành hàng con --';
			txt = catalog_product_chon_nganh_hang_con;
		}
		if(val == undefined || val == null){
			val = $(idHtml).val().trim();
		}
		$.ajax({
			url: '/catalog/product/get-category-child?status=' + type+'&productInfoId=' +val,
			method: 'GET',
			success: function(rs) {
				if (rs.error != undefined && !rs.error) {
					var lstCategoryChild = rs.rows;
					
					var html = '<option value="-2" selected="selected">' + Utils.XSSEncode(txt) + '</option>';
					if (hasAll != undefined && hasAll==1) {
						html = '<option value="-2" selected="selected">' +jsp_common_status_all+'</option>';
					}
					for(var i=0; i < lstCategoryChild.length; i++){
						html += '<option value="' + lstCategoryChild[i].id +'">' +
							Utils.XSSEncode(lstCategoryChild[i].productInfoCode) + ' - ' +
							Utils.XSSEncode(lstCategoryChild[i].productInfoName) + '</option>';  
					}
					$('#' + objectId).html(html);
					if(valView != undefined && valView != null){
						$('#' + objectId).find("option[value=" + valView +']').attr('selected','selected');
					}
					$('#' + objectId).change();
				}
			}
		});
	},
	/**
	 * LacNV
	 * view product information
	 */
	viewProduct: function(productId) {
		hrefPost('/catalog/product/viewdetail?productId=' + productId, {checkPermission:1});
	}
	,
	
	back: function(){
		sessionStorage.backButton = true;
		location.href = '/catalog/product';
	},
	/**
	 * download file template import san pham
	 * @author trietptm
	 * @since 18/06/2015
	 */
	downloadImportPriceTemplateFile: function() {
		var url = "/catalog/product/download-import-excel-file";
		ReportUtils.exportReport(url, {}, 'errMsg');
		return false;
	},
	importExcel: function() {
		$('.sucExcelMsg').html('').hide();
		$('.errExcelMsg').html('').hide();
		Utils.importExcelUtils(function(data) {
			ProductCatalog.search();
			$('#fakefilepc').val("").change();
			if (data.message != undefined && data.message != null && data.message.trim().length > 0) {
				$('#errExcelMsg').html(data.message.trim()).change().show();
			} else {
				$('#sucExcelMsg').html(jsp_common_save_success).show();
				var tm = setTimeout(function() {
					$('#sucExcelMsg').html("").hide();
					clearTimeout(tm);
				}, 1500);
			}
		}, 'importFrm', 'excelFile', 'errExcelMsg');
		
	},
};
/*
 * END OF FILE - /web/web/resources/scripts/business/catalog/vnm.product-catalog.js
 */

/*
 * START OF FILE - /web/web/resources/scripts/business/catalog/vnm.reason-catalog.js
 */
var ReasonCatalog = {
	_xhrSave : null,
	_xhrDel: null,
	_htmlReasonGroup: null,
	_curCallback: null,
	_curRowId: null,
	getGridUrl: function(reasonCode,reasonName,reasonGroupId,description,status){
		var rgId = 0;
		if(reasonGroupId!= null && reasonGroupId!= undefined){
			rgId = reasonGroupId; 
		}
		return "/catalog/reason/search?code=" + encodeChar(reasonCode) + "&name=" + encodeChar(reasonName) + "&rgId=" + encodeChar(rgId) + "&note=" + encodeChar(description) + "&status=" + status;
	},	
	searchReason: function(){
		$('#errMsg').html('').hide();
		var reasonCode = $('#reasonCode').val().trim();
		var reasonName = $('#reasonName').val().trim();
		var description = $('#description').val().trim();
		var status = $('#status').val().trim();
		var reasonGroupId = $('#reasonGroup').val();
		var url = ReasonCatalog.getGridUrl(reasonCode,reasonName,reasonGroupId,description,status);
		$("#grid").setGridParam({url:url,page:1}).trigger("reloadGrid");
		return false;
	},
	saveReason: function(){
		var msg = '';
		$('#errMsg').html('').hide();
		msg = Utils.getMessageOfRequireCheck('reasonCode','Mã lý do');
		if(msg.length == 0 && !$('#reasonCode').is(':disabled') && $('#reasonCode').val().trim().length != 10){
			msg = "Mã lý do phải đúng 10 ký tự";
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfSpecialCharactersValidate('reasonCode','Mã lý do',Utils._CODE);
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfRequireCheck('reasonName','Tên lý do');
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfSpecialCharactersValidate('reasonName','Tên lý do');
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfRequireCheck('reasonGroup','Nhóm lý do',true);
		}		
		if(msg.length == 0){
			msg = Utils.getMessageOfRequireCheck('status','Trạng thái',true);
		}
		if(msg.length == 0 && $('#description').val().trim().length > 0){
			msg = Utils.getMessageOfSpecialCharactersValidate('description','Mô tả');
		}
		if(msg.length > 0){
			$('#errMsg').html(msg).show();
			return false;
		}
		var dataModel = new Object();
		dataModel.id = $('#selReasonId').val().trim();
		dataModel.code = $('#reasonCode').val().trim();
		dataModel.name = $('#reasonName').val().trim();
		dataModel.rgId = $('#reasonGroup').val().trim();
		dataModel.note = $('#description').val().trim();
		dataModel.status = $('#status').val();
		Utils.addOrSaveRowOnGrid(dataModel, "/catalog/reason/save", ReasonCatalog._xhrSave, null, null, function(data){
			if(!data.error){
				ReasonCatalog.resetAllControl();
			}
		});	
		return false;
	},
	getSelectedReason: function(rowId,status){
		ReasonCatalog._curRowId = rowId;
		$('#reasonCode').attr('disabled','disabled');
		var id =  $("#grid").jqGrid ('getCell', rowId, 'id');
		var code =  $("#grid").jqGrid ('getCell', rowId, 'reasonCode');
		var name =  $("#grid").jqGrid ('getCell', rowId, 'reasonName');
		var description =  $("#grid").jqGrid ('getCell', rowId, 'description');
		var reasonGroupId =  $("#grid").jqGrid ('getCell', rowId, 'reasonGroup.id');
		var reasonGroupName =  $("#grid").jqGrid ('getCell', rowId, 'reasonGroup.reasonGroupName');
		if(id!= null && id!= 0 && id!=undefined){
			$('#selReasonId').val(id);
		} else {
			$('#selReasonId').val(0);
		}		
		ReasonCatalog.getReasonGroup(activeType.RUNNING);
		setTextboxValue('reasonCode',code);
		setTextboxValue('reasonName',name);
		setTextboxValue('description',description);		
		setSelectBoxValue('status', status);	
		//ReasonCatalog.getChangedForm();
		$('.RequireStyle').show();
		$('#btnSearch').hide();
		$('#btnAdd').hide();
		$('#btnEdit').show();
		$('#btnCancel').show();
		$('#code').focus();
		setTitleUpdate();
		return false;
	},
	resetForm: function(){
		ReasonCatalog.resetAllControl();
		$("#grid").trigger("reloadGrid");
		return false;
	},
	deleteReason: function(reasonCode){
		ReasonCatalog.resetCombobox();
		var dataModel = new Object();
		dataModel.code = reasonCode;	
		Utils.deleteSelectRowOnGrid(dataModel, 'lý do', '/catalog/reason/delete', ReasonCatalog._xhrDel, null, null);		
		return false;
	},
	resetAllControl: function(){		
		setTitleSearch();
		ReasonCatalog._curRowId = null;
		$('.RequireStyle').hide();
		$('#selReasonId').val(0);
		$('#errMsg').html('').hide();
		$('#reasonCode').removeAttr('disabled');
		$('#btnEdit').hide();
		$('#btnCancel').hide();
		$('#btnSearch').show();
		$('#btnAdd').show();
		$('#reasonCode').val('');
		$('#reasonName').val('');
		$('#description').val('');
		//ReasonCatalog.resetCombobox();
		ReasonCatalog.getReasonGroup(activeType.ALL);
		setSelectBoxValue('status', 1);	
		setSelectBoxValue('reasonGroup',-2);
		ReasonCatalog.searchReason();
	},
	resetCombobox: function(){
		if(ReasonCatalog._htmlReasonGroup!= null){
			$('#reasonGroup').html(ReasonCatalog._htmlReasonGroup);
			$('#reasonGroup').change();
		}
	},
	getChangedForm: function(){
		ReasonCatalog._curRowId = null;
		ReasonCatalog.getReasonGroup(activeType.RUNNING);
		$('.RequireStyle').show();
		$('#btnSearch').hide();
		$('#btnAdd').hide();
		$('#btnEdit').show();
		$('#btnCancel').show();
		$('#code').focus();
		setTitleAdd();
	},
	getReasonGroup: function(status){
		$.getJSON('/rest/catalog/reason-group/list.json?status=' + status, function(data){
			var arrHtml = new Array();
			arrHtml.push('<option value="'+ activeType.ALL +'">--- Chọn nhóm lý do ---</option>');
			for(var i=0;i<data.length;i++){
				arrHtml.push('<option value="'+ data[i].value +'">'+ Utils.XSSEncode(data[i].name) +'</option>');
			}
			$('#reasonGroup').html(arrHtml.join(""));
			$('#reasonGroup').change();
			if(ReasonCatalog._curRowId!= null && ReasonCatalog._curRowId!= undefined){
				var reasonGroupId =  $("#grid").jqGrid ('getCell', ReasonCatalog._curRowId, 'reasonGroup.id');
				var reasonGroupName =  $("#grid").jqGrid ('getCell', ReasonCatalog._curRowId, 'reasonGroup.reasonGroupName');
				var reasonGroupCode =  $("#grid").jqGrid ('getCell', ReasonCatalog._curRowId, 'reasonGroup.reasonGroupCode');
				if($('#reasonGroup option[value='+ reasonGroupId +']').html()== null){
					var htmlCat = $('#reasonGroup').html() + '<option value="'+ reasonGroupId +'">'+ Utils.XSSEncode(reasonGroupCode) + ' - ' + Utils.XSSEncode(reasonGroupName) + '</option>';
					$('#reasonGroup').html(htmlCat);
					$('#reasonGroup').change();					
				}
				setSelectBoxValue('reasonGroup', reasonGroupId);
			}
		});
	},
	exportExcelData:function(){
		$('#divOverlay').addClass('Overlay');
		$('#imgOverlay').show();
		$('#errMsg').html('').hide();
		$('#errExcelMsg').html('').hide();
		var reasonCode = $('#reasonCode').val().trim();
		var reasonName = $('#reasonName').val().trim();
		var description = $('#description').val().trim();
		var status = $('#status').val().trim();
		var reasonGroupId = $('#reasonGroup').val();
		var data = new Object();
		data.code = reasonCode;
		data.name = reasonName;
		data.note = description;
		data.status = status;
		data.rgId = reasonGroupId;
		var url = "/catalog/reason/getlistexceldata";
		CommonSearch.exportExcelData(data,url);
	}
};
/*
 * END OF FILE - /web/web/resources/scripts/business/catalog/vnm.reason-catalog.js
 */

/*
 * START OF FILE - /web/web/resources/scripts/business/catalog/vnm.reason-group-catalog.js
 */
var ReasonGroupCatalog = {
	_xhrSave : null,
	_xhrDel: null,
	getGridUrl: function(reasonGroupCode,reasonGroupName,description,status){		
		return "/catalog-manager/reason-group/search?code=" + encodeChar(reasonGroupCode) + "&name=" + encodeChar(reasonGroupName) + "&note=" + encodeChar(description) + "&status=" + status;
	},	
	searchReasonGroup: function(){		
		$('#errMsg').html('').hide();
		var reasonGroupCode = $('#reasonGroupCode').val().trim();
		var reasonGroupName =  $('#reasonGroupName').val().trim();
		var description = $('#description').val().trim();
		var status = $('#status').val().trim();		
		var url = ReasonGroupCatalog.getGridUrl(reasonGroupCode,reasonGroupName,description,status);
		$("#grid").setGridParam({url:url,page:1}).trigger("reloadGrid");
		return false;
	},
	saveReasonGroup: function(){
		var msg = '';
		$('#errMsg').html('').hide();
		msg = Utils.getMessageOfRequireCheck('reasonGroupCode','Mã nhóm lý do');
		if(msg.length == 0){
			msg = Utils.getMessageOfSpecialCharactersValidate('reasonGroupCode','Mã nhóm lý do',Utils._CODE);
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfRequireCheck('reasonGroupName','Tên nhóm lý do');
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfSpecialCharactersValidate('reasonGroupName','Tên nhóm lý do');
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfRequireCheck('status','Trạng thái',true);
		}
		if(msg.length == 0 && $('#description').val().trim().length > 0){
			msg = Utils.getMessageOfSpecialCharactersValidate('description','Mô tả');
		}
		if(msg.length > 0){
			$('#errMsg').html(msg).show();
			return false;
		}
		var dataModel = new Object();
		dataModel.id = $('#selReasonId').val().trim();
		dataModel.code = $('#reasonGroupCode').val().trim();
		dataModel.name = $('#reasonGroupName').val().trim();
		dataModel.note = $('#description').val().trim();
		dataModel.status = $('#status').val();
		Utils.addOrSaveRowOnGrid(dataModel, "/catalog-manager/reason-group/save", ReasonGroupCatalog._xhrSave, null,null, function(data){
			if(!data.error){
				ReasonGroupCatalog.clearData();
			}
		});			
		return false;
	},
	getSelectedReasonGroup: function(rowId,status){
		$('#reasonGroupCode').attr('disabled','disabled');
		focusFirstTextbox();
		var id =  $("#grid").jqGrid ('getCell', rowId, 'id');
		var code =  $("#grid").jqGrid ('getCell', rowId, 'reasonGroupCode');
		var name =  $("#grid").jqGrid ('getCell', rowId, 'reasonGroupName');
		var description =  $("#grid").jqGrid ('getCell', rowId, 'description');
		if(id!= null && id!= 0 && id!=undefined){
			$('#selReasonId').val(id);
		} else {
			$('#selReasonId').val(0);
		}
		setTextboxValue('reasonGroupCode',code);
		setTextboxValue('reasonGroupName',name);
		setTextboxValue('description',description);
		setSelectBoxValue('status', status);
		ReasonGroupCatalog.getChangedForm();
		setTitleUpdate();
		return false;
	},
	resetForm: function(){
		ReasonGroupCatalog.clearData();			
		$("#grid").trigger("reloadGrid");
		focusFirstTextbox();
		return false;
	},
	deleteReasonGroup: function(reasonGroupCode){
		var dataModel = new Object();
		dataModel.code = reasonGroupCode;	
		Utils.deleteSelectRowOnGrid(dataModel, 'nhóm lý do', '/catalog-manager/reason-group/delete', ReasonGroupCatalog._xhrDel,null,null, function(data){
			if(!data.error){
				ReasonGroupCatalog.resetForm();
				$('#reasonGroupName').focus();
			}
		}, null);		
		return false;
	},
	clearData: function(){
		setTitleSearch();
		$('.RequireStyle').hide();
		$('#selReasonId').val(0);
		$('#errMsg').html('').hide();
		$('#reasonGroupCode').removeAttr('disabled');
		$('#btnEdit').hide();
		$('#btnCancel').hide();
		$('#btnSearch').show();
		$('#btnAdd').show();
		$('#reasonGroupCode').val('');
		$('#reasonGroupName').val('');
		$('#description').val('');
		setSelectBoxValue('status', 1);
		ReasonGroupCatalog.searchReasonGroup();
	},
	getChangedForm: function(){
		$('.RequireStyle').show();
		$('#btnSearch').hide();
		$('#btnAdd').hide();
		$('#btnEdit').show();
		$('#btnCancel').show();
		//$('#code').focus();
		focusFirstTextbox();
		setTitleAdd();
	}
};
/*
 * END OF FILE - /web/web/resources/scripts/business/catalog/vnm.reason-group-catalog.js
 */

/*
 * START OF FILE - /web/web/resources/scripts/business/catalog/vnm.sales-day-catalog.js
 */
var SalesDayCatalog ={
	_xhrSave : null,
	_xhrDel: null,
	getGridUrl: function(fromYear,toYear,status){
		return "/catalog/sales-day/search?fromYear=" + encodeChar(fromYear) + "&toYear=" + encodeChar(toYear) + "&status=" + status;
	},
	search: function(){
		$('#errMsg').html('').hide();
		var fromYear = $('#fromYear').val().trim();
		var toYear = $('#toYear').val().trim();
		if(fromYear != '' && toYear != ''){
			if(parseInt(fromYear) > parseInt(toYear)){
				$('#errMsg').html('Thời gian tìm kiếm không hợp lệ').show();
				return false;
			}
		}
		var status = $('#status').val();
		var url = SalesDayCatalog.getGridUrl(fromYear,toYear,status);
		$("#grid").setGridParam({url:url,page:1}).trigger("reloadGrid");
		return false;
	},
	change: function(){
		var msg = '';
		$('#errMsg').html('').hide();
		var error = false;
		var tmp = '';
		msg = Utils.getMessageOfRequireCheck('year','Năm');
		if(msg.length == 0){
			if($('#year').val().trim() != ''){
				if($('#january').val().trim() != '' && parseInt($('#january').val().trim()) > SalesDayCatalog.daysInMonth(0,$('#year').val().trim())){
					tmp = 'tháng 1';
					$('#january').focus();
					error = true;
				}
				if($('#february').val().trim() != '' && parseInt($('#february').val().trim()) > SalesDayCatalog.daysInMonth(1,$('#year').val().trim())){
					tmp = 'tháng 2';
					$('#february').focus();
					error = true;
				}
				if($('#march').val().trim() != '' && parseInt($('#march').val().trim()) > SalesDayCatalog.daysInMonth(2,$('#year').val().trim())){
					tmp = 'tháng 3';
					$('#march').focus();
					error = true;
				}
				if($('#april').val().trim() != '' && parseInt($('#april').val().trim()) > SalesDayCatalog.daysInMonth(3,$('#year').val().trim())){
					tmp = 'tháng 4';
					$('#april').focus();
					error = true;
				}
				if($('#may').val().trim() != '' && parseInt($('#may').val().trim()) > SalesDayCatalog.daysInMonth(4,$('#year').val().trim())){
					tmp = 'tháng 5';
					$('#may').focus();
					error = true;
				}
				if($('#june').val().trim() != '' && parseInt($('#june').val().trim()) > SalesDayCatalog.daysInMonth(5,$('#year').val().trim())){
					tmp = 'tháng 6';
					$('#june').focus();
					error = true;
				}
				if($('#july').val().trim() != '' && parseInt($('#july').val().trim()) > SalesDayCatalog.daysInMonth(6,$('#year').val().trim())){
					tmp = 'tháng 7';
					$('#july').focus();
					error = true;
				}
				if($('#august').val().trim() != '' && parseInt($('#august').val().trim()) > SalesDayCatalog.daysInMonth(7,$('#year').val().trim())){
					tmp = 'tháng 8';
					$('#august').focus();
					error = true;
				}
				if($('#september').val().trim() != '' && parseInt($('#september').val().trim()) > SalesDayCatalog.daysInMonth(8,$('#year').val().trim())){
					tmp = 'tháng 9';
					$('#september').focus();
					error = true;
				}
				if($('#october').val().trim() != '' && parseInt($('#october').val().trim()) > SalesDayCatalog.daysInMonth(9,$('#year').val().trim())){
					tmp = 'tháng 10';
					$('#october').focus();
					error = true;
				}
				if($('#november').val().trim() != '' && parseInt($('#november').val().trim()) > SalesDayCatalog.daysInMonth(10,$('#year').val().trim())){
					tmp = 'tháng 11';
					$('#november').focus();
					error = true;
				}
				if($('#december').val().trim() != '' && parseInt($('#december').val().trim()) > SalesDayCatalog.daysInMonth(11,$('#year').val().trim())){
					tmp = 'tháng 12';
					$('#december').focus();
					error = true;
				}
			}
			if(error){
				msg = 'Số ngày trong ' + Utils.XSSEncode(tmp) + ' không hợp lệ';
			}
		}
		if(msg.length > 0){
			$('#errMsg').html(msg).show();
			return false;
		}
		var dataModel = new Object();
		dataModel.salesDayId = $('#salesDayId').val();
		dataModel.year = $('#year').val();
		dataModel.january = $('#january').val().trim();
		dataModel.february = $('#february').val().trim();
		dataModel.march = $('#march').val().trim();
		dataModel.april = $('#april').val().trim();
		dataModel.may = $('#may').val().trim();
		dataModel.june = $('#june').val().trim();
		dataModel.july = $('#july').val().trim();
		dataModel.august = $('#august').val().trim();
		dataModel.september = $('#september').val().trim();
		dataModel.october = $('#october').val().trim();
		dataModel.november = $('#november').val().trim();
		dataModel.december = $('#december').val().trim();
		dataModel.status = $('#status').val();
		Utils.addOrSaveData(dataModel, "/catalog/sales-day/save", SalesDayCatalog._xhrSave, null,function(data){
			if(data.error == false){
				window.location.href = '/catalog/working-date-in-year';
			}
		});
		return false;
	},
	deleteRow: function(salesDayId){
		$('#errMsg').html('').hide();
		var dataModel = new Object();
		dataModel.salesDayId = salesDayId;	
		Utils.deleteSelectRowOnGrid(dataModel, 'Sản phẩm', "/catalog/sales-day/remove", SalesDayCatalog._xhrDel, null, null);		
		return false;		
	},
	daysInMonth:function (m, y) { // m is 0 indexed: 0-11
	    switch (m) {
	        case 1 :
	            return (y % 4 == 0 && y % 100) || y % 400 == 0 ? 29 : 28;
	        case 8 : case 3 : case 5 : case 10 :
	            return 30;
	        default :
	            return 31;
	    }
	}

};
/*
 * END OF FILE - /web/web/resources/scripts/business/catalog/vnm.sales-day-catalog.js
 */

/*
 * START OF FILE - /web/web/resources/scripts/business/catalog/vnm.sku-catalog.js
 */
var SkuCatalog = {
	_xhrSave : null,
	_xhrDel: null,
	getGridUrl: function(channelTypeCode,channelTypeName,status,skuAmount){		
		return "/catalog/sku-customer-type/search?code=" + encodeChar(channelTypeCode) + "&name=" + encodeChar(channelTypeName) + "&status=" + encodeChar(status) + '&skuAmount=' + encodeChar(skuAmount);
	},	
	search: function(){		
		$('#errMsg').html('').hide();
		$('#errExcelMsg').html('').hide();
		$('#name').focus();
		var channelTypeCode = $('#code').val().trim();
		var channelTypeName =  $('#name').val().trim();		
		var status = $('#status').val().trim();		
		var skuAmount = $('#skuAmount').val().trim().replace(/,/g,'');
		var url = SkuCatalog.getGridUrl(channelTypeCode,channelTypeName,status,skuAmount);
		$("#grid").setGridParam({url:url,page:1}).trigger("reloadGrid");
		return false;
	},
	save: function(){
		var msg = '';
		$('#errMsg').html('').hide();
		$('#errExcelMsg').html('').hide();
		msg = Utils.getMessageOfRequireCheck('code','Mã loại KH',true);
		if(msg.length == 0){
			msg = Utils.getMessageOfRequireCheck('skuAmount','Chỉ tiêu SKU');
		}
		var skuAmount = $('#skuAmount').val().trim().replace(/,/g,'');
		if(msg.length == 0){
			if(isNaN(skuAmount) || parseInt(skuAmount) <=0){
				msg = 'Chỉ tiêu SKU phải là số nguyên dương';
			}
		}
		if(msg.length == 0){
			if(parseInt(skuAmount) > 9999999999){
				msg= 'Chỉ tiêu SKU không vượt quá 9,999,999,999';
			}
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfRequireCheck('status','Trạng thái',true);
		}		
		if(msg.length > 0){
			$('#errMsg').html(msg).show();
			return false;
		}
		var dataModel = new Object();
		dataModel.id = $('#selId').val().trim();
		dataModel.code = $('#code').val().trim();		
		dataModel.skuAmount = $('#skuAmount').val().trim().replace(/,/g,'');
		dataModel.status = $('#status').val();
		Utils.addOrSaveRowOnGrid(dataModel, "/catalog/sku-customer-type/save", SkuCatalog._xhrSave, null, null, function(data){
			if(!data.error){
				SkuCatalog.resetForm();
				SkuCatalog.search();
			}			
		});			
		return false;
	},
	getSelectedRow: function(rowId){
		disabled('name');
		$('#skuAmount').focus();
		var code = $("#grid").jqGrid ('getCell', rowId, 'channelTypeCode');
		var name = $("#grid").jqGrid ('getCell', rowId, 'channelTypeName');
		var status = $("#grid").jqGrid ('getCell', rowId, 'statusSku');
		var skuAmount = $("#grid").jqGrid ('getCell', rowId, 'sku');
		var id = $("#grid").jqGrid ('getCell', rowId, 'id');
		
		if(id!= null && id!= 0 && id!=undefined){
			$('#selId').val(id);
		} else {
			$('#selId').val(0);
		}
		setSelectBoxValue('code',code);
		setTextboxValue('name',name);
		setTextboxValue('skuAmount',skuAmount);
		if(status == activeStatusText){
			status = 1;
		} else {
			status = 0;
		}
		setSelectBoxValue('status', status);
		SkuCatalog.getChangedForm();
		disabled('code');
		$('#code').change();
		setTitleUpdate();
		$('#errExcelMsg').html('').hide();
		return false;
	},
	resetForm: function(){
		setTitleSearch();
		$('.RequireStyle').hide();
		$('#selId').val(0);
		$('#errMsg').html('').hide();
		$('#name').removeAttr('disabled');
		$('#btnEdit').hide();
		$('#btnCancel').hide();
		$('#btnSearch').show();
		$('#btnAdd').show();		
		$('#name').val('');
		$('#skuAmount').val('');
		setSelectBoxValue('code', -2);
		setSelectBoxValue('status', 1);
		enable('code');
		$('#name').focus();
		$('#code').change();
		//$("#grid").trigger("reloadGrid");
		SkuCatalog.search();
		$('#errExcelMsg').html('').hide();
		return false;
	},
	deleteSKU: function(id){
		var dataModel = new Object();
		dataModel.id = id;	
		Utils.deleteSelectRowOnGrid(dataModel, 'chỉ tiêu SKU', '/catalog/sku-customer-type/delete', SkuCatalog._xhrDel, null, null,function(data){
			if (!data.error){
				SkuCatalog.resetForm();
			}
		},null);
		$('#errExcelMsg').html('').hide();
		return false;
	},
	getChangedForm: function(){
		$('#skuAmount').focus();
		$('.RequireStyle').show();
		$('#btnSearch').hide();
		$('#btnAdd').hide();
		$('#btnEdit').show();
		$('#btnCancel').show();	
		disabled('name');
		setTitleAdd();
		$('#code').change();
	},
	ViewFileUpload: function(){
		$('#typeButton').val("1");//bang 1 la xem file upload
		var options = { 
		 		beforeSubmit: ProductLevelCatalog.beforeImportExcelA,   
		 		success:      ProductLevelCatalog.afterImportExcelA,  
		 		type: "POST",
		 		dataType: 'html'   
		 	}; 
		$('#importFrm').ajaxForm(options);
		$('#importFrm').submit();
	},
	exportExcel: function(){
		var channelTypeCode = $('#code').val().trim();
		var channelTypeName =  $('#name').val().trim();		
		var status = $('#status').val().trim();		
		var skuAmount = $('#skuAmount').val().trim().replace(/,/g,'');
		var url = "/catalog/sku-customer-type/export-excel?channelTypeCode=" + encodeChar(channelTypeCode) + "&channelTypeName=" + encodeChar(channelTypeName) +"&status=" + status + "&skuAmount=" + encodeChar(skuAmount);
		CommonSearch.exportExcelData(url,'Bieu_mau_export_SKU_CUSTOMER_TYPE.xls');
	}
};
/*
 * END OF FILE - /web/web/resources/scripts/business/catalog/vnm.sku-catalog.js
 */

/*
 * START OF FILE - /web/web/resources/scripts/business/catalog/vnm.sale-type-catalog.js
 */
var SaleTypeCatalog = {
	_xhrSave : null,
	_xhrDel: null,
	getGridUrl: function(staffTypeCode,parentCode,staffTypeName,status,isGetParent){
		return "/catalog/sale-type/search?staffTypeCode=" + encodeChar(staffTypeCode) + "&parentCode=" + encodeChar(parentCode) + "&staffTypeName=" + encodeChar(staffTypeName) + "&status=" + status+ "&isGetParent=" + encodeChar(isGetParent);
	},	
	search: function(){
		$('#errMsg').html('').hide();
		var staffTypeCode = $('#staffTypeCode').val().trim();
		var parentCode = $('#parentCode').combotree('getValue');
		if(parentCode == '' || parentCode == null){
			parentCode = -1;
		}
		var staffTypeName = $('#staffTypeName').val().trim();
		var status = $('#status').val().trim();
		var tm = setTimeout(function() {
			$('#staffTypeCode').focus();
		}, 500);
		var url = SaleTypeCatalog.getGridUrl(staffTypeCode,parentCode,staffTypeName,status,false);
		$("#grid").setGridParam({url:url,page:1}).trigger("reloadGrid");
		return false;
	},
	change: function(){
		var msg = '';
		$('#errMsg').html('').hide();
		msg = Utils.getMessageOfRequireCheck('staffTypeCode','Mã hình thức bán hàng');
		if(msg.length == 0){
			msg = Utils.getMessageOfSpecialCharactersValidate('staffTypeCode','Mã hình thức bán hàng',Utils._CODE);
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfRequireCheck('staffTypeName','Tên hình thức bán hàng');
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfSpecialCharactersValidate('staffTypeName','Tên hình thức bán hàng');
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfRequireCheck('status','Trạng thái',true);
		}
		if($('#status').val() != 1 && $('#status').val() != 0 && msg.length == 0){
			msg = "Bạn chưa nhập giá trị cho trường Trạng thái. Yêu cầu nhập giá trị";
		}
		if(msg.length > 0){
			$('#errMsg').html(msg).show();
			return false;
		}
		var dataModel = new Object();
		dataModel.id = $('#staffTypeId').val().trim();
		dataModel.staffTypeCode = $('#staffTypeCode').val().trim();
		dataModel.staffTypeName = $('#staffTypeName').val().trim();
		dataModel.parentCode = $('#parentCode').combotree('getValue');
		dataModel.status = $('#status').val().trim();
		Utils.addOrSaveRowOnGrid(dataModel, "/catalog/sale-type/save", SaleTypeCatalog._xhrSave, null, null,function(){
			SaleTypeCatalog.loadTree();
			SaleTypeCatalog.resetForm();
			var url = SaleTypeCatalog.getGridUrl('',-1,'',1,false);
			$("#grid").setGridParam({url:url,page:1}).trigger("reloadGrid");
		});
		return false;
	},
	getSelectedStaffType: function(id,staffTypeCode,parentCode,staffTypeName,status){
		$('#errMsg').html('').hide();
		var url = 'channelTypeId='+ parentCode;
		$.getJSON('/rest/catalog/catalog/staff-type/channeltype.json?'+url, function(data){
			if(data != null && data.status != 'RUNNING'){
				$('#errMsg').html('Bạn phải thay đổi mã cha về trạng thái hoạt động trước khi chỉnh sửa được đối tượng này').show();
			} else {
				$('#parentCode').combotree("reload",'/rest/catalog/sale-type/combotree/1/'+id+'.json');
				if(id!= null && id!= 0 && id!=undefined){
					hideOption('parentCode',id);
					$('#staffTypeId').val(id);
				} else {
					$('#staffTypeId').val(0);
				}
				$('#staffTypeCode').attr('disabled',true);
				setTextboxValue('staffTypeCode',staffTypeCode);
				setTextboxValue('staffTypeName',staffTypeName);
				setSelectBoxValue('status', status);
				$('#parentCode').combotree('setValue',parentCode);
				Utils.getChangedForm();
				$('#btnSearch,#btnCreate').hide();
				$('#btnUpdate,#btnDismiss').show();
				setTitleUpdate();
			}
		});
	},
	resetForm: function(){
		$('#errMsg').html('').hide();
		setTitleSearch();
		$('.RequireStyle').hide();
		$('#btnUpdate,#btnDismiss').hide();
		$('#btnCreate,#btnSearch').show();
		$('#staffTypeCode').val('');
		$('#staffTypeCode').removeAttr('disabled');
		$('#staffTypeCode').focus();
		$('#staffTypeName').val('');
		resetAllOptions('parentCode');
		$('#parentCode').combotree('clear');
		$('#parentCode').combotree("reload",'/rest/catalog/sale-type/combotree/0/0.json');
		$('#parentCode').combotree('setValue', -1);
		setSelectBoxValue('status',1);
		$('#staffTypeId').val(0);
		var url = SaleTypeCatalog.getGridUrl('',-1,'',1,false);
		$("#grid").setGridParam({url:url,page:1}).trigger("reloadGrid");
		return false;
	},
	deleteStaffType: function(staffTypeCode){
		$('#errMsg').html('').hide();
		var dataModel = new Object();
		dataModel.staffTypeCode = staffTypeCode;	
		Utils.deleteSelectRowOnGrid(dataModel, 'kiểu bán hàng', "/catalog/sale-type/remove", SaleTypeCatalog._xhrDel, null, null,function(){SaleTypeCatalog.loadTree();});		
		return false;		
	},
	loadTree: function(){
		loadDataForTree('/rest/catalog/sale-type/tree.json');	
		$('#tree').bind("loaded.jstree", function(event, data){
			if($('#treeContainer').data("jsp")!= undefined){
				$('#treeContainer').data("jsp").destroy();
			}
			$('#treeContainer').jScrollPane();
		});
		$('#tree').bind("select_node.jstree", function (event, data) {
			var name = data.inst.get_text(data.rslt.obj);
			var id = data.rslt.obj.attr("id");			 
			var url = SaleTypeCatalog.getGridUrl('',id,'',1,true);
			$("#grid").setGridParam({url:url,page:1}).trigger("reloadGrid");
	    });
	},
	getListParentCode:function(parentId,objParentCode){
		$.ajax({
			type : "POST",
			url : "/catalog/sale-type/getlistparentcode",
			data :({parentCode:parentId}),
			dataType : "json",
			success : function(data) {
				var html = new Array();
    			if(data!= null && data!= undefined && data.lstParentCode!= null && data.lstParentCode.length > 0){
    				html.push("<option value='-1'>---Chọn mã cha---</option>");
    				for(var i=0;i<data.lstParentCode.length;i++){
    					html.push("<option value='"+ data.lstParentCode[i].id +"'>"+ Utils.XSSEncode(data.lstParentCode[i].channelTypeName) +"</option>");
    				}
    			}else{
    				html.push("<option value='-1'>---Chọn mã cha---</option>");
    			}	        			        	
    			$(objParentCode).html(html.join(""));
    			$(objParentCode).val(parentId);
    			$(objParentCode).change();
			},
			error:function(XMLHttpRequest, textStatus, errorThrown) {
				$.ajax({
					type : "POST",
					url : '/check-session',
					dataType : "json",
					success : function(data) {
					},
					error: function(XMLHttpRequest, textStatus, errorThrown) {
						window.location.href = '/home';
					}
				});
			}
		});
	}
};
/*
 * END OF FILE - /web/web/resources/scripts/business/catalog/vnm.sale-type-catalog.js
 */

/*
 * START OF FILE - /web/web/resources/scripts/business/catalog/vnm.staff-catalog.js
 */
var StaffCatalog = {
	_xhrSave : null,
	_xhrDel: null,
	_mapCheckStaff: new Map(),
	_mapStaffCat:new Map(),
	search: function(){
		$('#errMsg').html('').hide();
		var shopCodeHidden = $('#shopCodeHidden').val().trim();
		var staffCode = $('#staffCode').val().trim();
		var staffName = $('#staffName').val().trim();
		var status = $('#status').val();		
		$('#grid').datagrid('load',{page : 1,shopCodeHidden:shopCodeHidden,staffCode:staffCode,staffName:staffName,status:status});
		return false;
	},
	activeTab:function(tab,is_allow){
		$('#tabItem1').bind('click',function(){
			StaffCatalog.activeTab('tab1',is_allow);
		});
		$('#tabItem2').bind('click',function(){
			StaffCatalog.activeTab('tab2',is_allow);
			StaffCatalog.getListSaleCatByStaffId();
		});
		if(tab=='tab1'){
			$('#tabItem1').addClass('Active');
			$('#tabItem2').removeClass('Active');
			$('#tab1').show();
			$('#tab2').hide();			
		}else{
			$('#tabItem2').addClass('Active');
			$('#tabItem1').removeClass('Active');
			$('#tab2').show();
			$('#tab1').hide();			
		}
		if(is_allow!=undefined && is_allow!=null && !is_allow){
			$('#tabItem2').unbind('click');
			$('#tabItem2').removeClass('Active');
		}else if(is_allow==undefined || is_allow==null){
			AttributesManager.getAttributesForm('tab2',AttributesManager.STAFF);
			$('#typeAttribute').val(AttributesManager.STAFF);
		}
		
	},
	changeStaffInfor:function(){
			
		var msg = '';
		$('#errMsg').html('').hide();
		//var staffId = $('#staffId').val();
		
		if(msg.length==0){
			msg = Utils.getMessageOfRequireCheck('staffCode','Mã nhân viên');
		}
		if(msg.length==0){
			msg = Utils.getMessageOfSpecialCharactersValidate('staffCode','Mã nhân viên',Utils._CODE);
		}			
		if(msg.length == 0){
			msg = Utils.getMessageOfRequireCheck('staffName','Tên nhân viên');
		}		
		if(msg.length == 0){
			msg = Utils.getMessageOfSpecialCharactersValidate('staffName','Tên NV',Utils._NAME);
		}		
		if(msg.length == 0){
			msg = Utils.getMessageOfRequireCheck('gender','Giới tính',true);
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfRequireCheck('staffType','Chức vụ',true);
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfSpecialCharactersValidate('idPlace','Nơi cấp');
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfSpecialCharactersValidate('staffPhone','Số di động');
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfSpecialCharactersValidate('staffTelephone','Số cố định');
		}
		if(msg.length == 0){
			//msg = Utils.getMessageOfInvaildNumber('staffPhone','Số di động');
			msg = Utils.getMessageOfInvaildNumberStartWithZero('staffPhone','Số di động');
		}
		if(msg.length == 0){
			//msg = Utils.getMessageOfInvaildNumber('staffTelephone','Số cố định');
			msg = Utils.getMessageOfInvaildNumberStartWithZero('staffTelephone','Số cố định');
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfInvalidEmailFormat('email', 'Email');
		}
		if (msg.length == 0) {
			var prov = $('#province').combobox('getValue');
			if (prov == null || prov.length == 0 || prov == '-2') {
				msg = "Bạn chưa chọn Tỉnh/Thành phố";
			}
			$('#province').next().find('.combo-text').focus();
		}
		
		if (msg.length == 0) {
			var prov = $('#district').combobox('getValue');
			if (prov == null || prov.length == 0 || prov == '-2') {
				msg = "Bạn chưa chọn Quận/Huyện";
			}
			$('#district').next().find('.combo-text').focus();
		}
		
		if (msg.length == 0) {
			var prov = $('#precinct').combobox('getValue');
			if (prov == null || prov.length == 0 || prov == '-2') {
				msg = "Bạn chưa chọn Phường/Xã";
			}
			$('#precinct').next().find('.combo-text').focus();
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfSpecialCharactersValidate('idCard','CMND');
		}		
		if(msg.length == 0){
			if ($('#workStartDate').val().trim() != '' && !Utils.isDate($('#workStartDate').val().trim())) {
				msg = 'Ngày bắt đầu làm việc không hợp lệ, vui lòng nhập theo định dạng dd/MM/yyyy';
				$('#workStartDate').focus();
			}
		}
		if(msg.length == 0){
			var area = $('#areaId').val().trim();
			if (area == null || area == -2) {
				msg = 'Địa bàn là bắt buộc. Xin vui lòng chọn địa bàn';
				$('#areaTree').focus();
			}
		}
		if (msg.length == 0 && $('#idDate').val().trim() != '' && !Utils.isDate($('#idDate').val().trim())) {
			msg = 'Ngày cấp không hợp lệ, vui lòng nhập theo định dạng dd/MM/yyyy';
			$('#idDate').focus();
		}
		if (msg.length == 0 && $('#workStartDate').val().trim() != '' && !Utils.isDate($('#workStartDate').val().trim())) {
			msg = 'Ngày bắt đầu làm việc không hợp lệ, vui lòng nhập theo định dạng dd/MM/yyyy';
			$('#workStartDate').focus();
		}
		if (msg.length == 0 && $('#idDate').val().trim() != '' && !Utils.isDate($('#idDate').val().trim())) {
			msg = 'Ngày bắt đầu làm việc không hợp lệ, vui lòng nhập theo định dạng dd/MM/yyyy';
			$('#idDate').focus();
		}		
		if(msg.length > 0){
			$('#errMsg').html(msg).show();
			return false;
		}
		
		var dataModel = new Object();
		dataModel.staffId = $('#staffId').val().trim();		
		dataModel.staffCode = $('#staffCode').val().trim();
		dataModel.staffName = $('#staffName').val().trim();				
		dataModel.idCard = $('#idCard').val().trim();
		dataModel.idDate = $('#idDate').val().trim();
		dataModel.idPlace = $('#idPlace').val().trim();
		dataModel.areaId = $('#areaId').val().trim();
		dataModel.street = $('#street').val().trim();
		dataModel.address = $('#address').val().trim();
		dataModel.email = $('#email').val().trim();
		dataModel.saleGroup = $('#saleGroup').val().trim();
		dataModel.staffPhone = $('#staffPhone').val().trim();
		dataModel.staffType = $('#staffType').val();
		dataModel.saleTypeCode = $('#saleType').val();
		dataModel.workStateCode = $('#workState').val();
		
		dataModel.status = $('#status').val();
		dataModel.gender = $('#gender').val();
		dataModel.workStartDate = $('#workStartDate').val().trim();
		dataModel.education = $('#education').val().trim();
		dataModel.position = $('#position').val().trim();
		dataModel.staffTelephone = $('#staffTelephone').val().trim();		
		Utils.addOrSaveData(dataModel, "/catalog_staff_manager/saveorupdate", StaffCatalog._xhrSave, 'errMsg',function(data){
			$('#staffId').val(data.staffId);
			if(data.error!=null && !data.error){
				if($('#staffId').val()!= 0){			
					var lstCatId = new Array();
					var lstDelete = new Array();
					$('.InputCbxStyle').each(function(){
						var catId = $(this).val().trim();
						if ($(this).is(':checked') && StaffCatalog._mapStaffCat.get(catId)==null){
							lstCatId.push(catId);
						}else if(!$(this).is(':checked') && StaffCatalog._mapStaffCat.get(catId)!=null){
							lstDelete.push(catId);
						}
					});
					var staffId = $('#infStaffCodeHid').val().trim();
					var params = new Object();		
					params.lstCatId = lstCatId ;
					params.staffId = staffId;
					params.lstDelete = lstDelete;
					Utils.saveData(params, '/catalog_staff_manager/save-staff-sale-cat', null, null, null, null);
				}
				setTimeout(function(){
					window.location.href='/catalog_staff_manager/viewdetail?staffId=' + data.staffId;
				}, 1000);
				$('#imfoCat').hide();
			}
		});		
	},
	showArea:function(selector,type,codeValue){		
		var html = new Array();	
		html.push("<option value=''>Tất cả</option>");
		var areaCode = $(selector).val().trim();		
		if(areaCode!=undefined && areaCode!=null && areaCode.trim().length==0){
			if($(selector).attr('id')=='provinceCode'){
				$('#districtCode').html(html.join(""));
				$('#wardCode').html(html.join("")); 
				$('#districtCode').val('');
				$('#wardCode').val('');
			}
			if($(selector).attr('id')=='districtCode'){
				$('#wardCode').html(html.join(""));
				$('#wardCode').val('');
				/*disableSelectbox('wardCode');*/
			}
			
			return false;
		}
		$.ajax({
			type : "POST",
			url : "/catalog_staff_manager/show-area",
			data : ({areaCode:areaCode}),
			dataType: "json",
			success : function(data) {				
    			if(data!= null && data!= undefined && data.areas!= null && data.areas.length > 0){    				
    				for(var i=0;i<data.areas.length;i++){    					
    					html.push("<option value='"+ Utils.XSSEncode(data.areas[i].areaCode) +"'>" + Utils.XSSEncode(data.areas[i].areaName) +"</option>");
    				}
    				if(type==1){
    					//enable('districtCode');
    					//$('#districtCode').parent().removeClass('BoxDisSelect');
    				}else if(type==2){    					
    					//enable('wardCode');
    					$('#wardCode').parent().removeClass('BoxDisSelect');
    				}    				
    			}    			
    			if(type==1){    				
    				$('#districtCode').html(html.join(""));  
    				if(codeValue!=undefined && codeValue!=null && codeValue.trim().length>0){
    					$('#districtCode').val(codeValue);
    					//disableSelectbox('districtCode');
    				}
    			}else if(type==2){    				
    				$('#wardCode').html(html.join("")); 
    				if(codeValue!=undefined && codeValue!=null && codeValue.trim().length>0){
    					$('#wardCode').val(codeValue);
    					/*disableSelectbox('wardCode');*/
    				}     				    				
    			}    			
			},
			error:function(XMLHttpRequest, textStatus, errorThrown) {				
			}
		});
	},
	getSupervisorGridUrl: function(staffId,shopCode,shopName,staffCode,staffName){
		return "/catalog_staff_manager/searchsupervisor?staffId=" + encodeChar(staffId) + "&shopCode=" + encodeChar(shopCode) + "&shopName=" + encodeChar(shopName) + "&staffCode=" + encodeChar(staffCode) + "&staffName=" + encodeChar(staffName);
	},
	getCategoryGridUrl: function(staffId){
		return "/catalog_staff_manager/searchcategory?staffId="+ encodeChar(staffId);
	},
	getChangedGridUrl: function(staffId,startDate,endDate){
		return "/catalog_staff_manager/searchchanged?staffId="+ encodeChar(staffId) + "&startDate=" + encodeChar(startDate) + "&endDate=" + encodeChar(endDate);
	},
	getDetailChangedGridUrl: function(actionGeneralLogId){
		$('#divDetail').show();
		var url = StaffCatalog.viewDetailChanged(actionGeneralLogId);
		if($('#gbox_detailGrid').html() != null && $('#gbox_detailGrid').html().trim().length > 0){
			$("#detailGrid").setGridParam({url:url,page:1}).trigger("reloadGrid");
		} else {
			$("#detailGrid").jqGrid({
				url:url,
				colModel:[		
				          {name:'columnName', label: 'Thông tin thay đổi', width: 100, sortable:false,resizable:false, align: 'left' },
				          {name:'oldValue', label: 'Giá trị cũ', width: 200, sortable:false,resizable:false, align: 'right' },
				          {name:'newValue', label: 'Giá trị mới', sortable:false,resizable:false, align: 'right'},
				          {name:'actionDate', label: 'Ngày thay đổi', width: 100, align: 'center', sortable:false,resizable:false ,formatter:'date',formatoptions:{srcformat: 'Y-m-d H:i:s',newformat: 'd/m/Y' } },
				          ],	  
				          pager : '#detailPager',
				          height: 'auto',
				          rownumbers: true,	  
				          width: ($('#detailChangedGrid').width()),
				          gridComplete: function(){
							  $('#jqgh_detailGrid_rn').html('STT');
							  updateRownumWidthForJqGrid();
						  }
			})
			.navGrid('#detailPager', {edit:false,add:false,del:false, search: false});
		}
	},
	viewDetailChanged:function(actionGeneralLogId){
		return "/catalog_staff_manager/searchdetailchanged?actionGeneralLogId="+ actionGeneralLogId;
	},
	getHistoryGridUrl: function(staffId,shopCode,startDate,endDate){
		return "/catalog_staff_manager/searchhistory?staffId="+ encodeChar(staffId) + "&shopCode=" + encodeChar(shopCode) + "&startDate=" + encodeChar(startDate) + "&endDate=" + encodeChar(endDate);
	},
	getStaffGridUrlDialog: function(shopCode,shopName,staffCode,staffName, staffId){
		return "/catalog_staff_manager/searchstaffdialog?staffId="+ encodeChar(staffId) + "&shopCode="+ encodeChar(shopCode) + "&shopName=" + encodeChar(shopName) + "&staffCode=" + encodeChar(staffCode) + "&staffName=" + encodeChar(staffName);
	},
	searchSupervisor: function(){
		$('#errMsg').html('').hide();
		var staffId = $('#staffId').val();
		var shopCode = $('#shopCode').val().trim();
		var shopName = $('#shopName').val().trim();
		var staffCode = $('#staffCode').val().trim();
		var staffName = $('#staffName').val().trim();
		var url = StaffCatalog.getSupervisorGridUrl(staffId,shopCode,shopName,staffCode,staffName);
		$("#grid").setGridParam({url:url,page:1}).trigger("reloadGrid");
		$('.ui-paging-info').show();//thong bao cua grid
		return false;
	},
	searchHistory: function(){
		$('#errMsg').html('').hide();
		var staffId = $('#staffId').val().trim();
		var shopCode = $('#shopCode').val().trim();
		var startDate = $('#startDate').val().trim();
		var endDate = $('#endDate').val().trim();
		var url = StaffCatalog.getHistoryGridUrl(staffId,shopCode,startDate,endDate);
		if($('#gbox_grid').html() != null && $('#gbox_grid').html().trim().length > 0){
			$("#grid").setGridParam({url:url,page:1}).trigger("reloadGrid");
		} else {
			$("#grid").jqGrid({
				  url:url,
				  colModel:[		
				    {name:'orderDate', label: 'Ngày bán hàng', width: 100, sortable:false, align: 'center',resizable:false ,formatter:'date',formatoptions:{srcformat: 'Y-m-d H:i:s',newformat: 'd/m/Y' } },
				    {name:'customer.customerName', label: 'KH mua', width: 200, sortable:false,resizable:false, align: 'left' },
				    {name:'orderNumber', label: 'Số đơn hàng', width: 200,sortable:false,resizable:false, align: 'right'},
				    {name:'total', label: 'Số tiền', width: 100, align: 'right', sortable:false,resizable:false, formatter: StaffCatalogFormatter.priceFormat},
				  ],	  
				  pager : '#pager',
				  height: 'auto',
				  rownumbers: true,	  
				  width: ($('#historyGrid').width())	  
				})
				.navGrid('#pager', {edit:false,add:false,del:false, search: false});
				$('#jqgh_grid_rn').prepend('STT');
		}
		return false;
	},
	searchChanged: function(){
		$('#errMsg').html('').hide();
		var staffId = $('#staffId').val().trim();
		var startDate = $('#startDate').val().trim();
		var endDate = $('#endDate').val().trim();
		var url = StaffCatalog.getChangedGridUrl(staffId,startDate,endDate);
		if($('#gbox_grid').html() != null && $('#gbox_grid').html().trim().length > 0){
			$("#grid").setGridParam({url:url,page:1}).trigger("reloadGrid");
		} else {
			$("#grid").jqGrid({
			  url:url,
			  colModel:[		
			    {name:'actionUser', label: 'NV thay đổi', width: 100, sortable:false,resizable:false, align: 'left' },
			    {name:'actionType', label: 'Loại thay đổi', width: 200, sortable:false,resizable:false , align: 'left', formatter: StaffCatalogFormatter.typeFormatter},
			    {name:'actionDate', label: 'Ngày thay đổi', sortable:false,resizable:false, align: 'center' ,formatter:'date',formatoptions:{srcformat: 'Y-m-d H:i:s',newformat: 'd/m/Y' } },
			    {name:'actionIp', label: 'IP máy thay đổi', width: 100, align: 'right', sortable:false,resizable:false},
			    {name:'view', label: 'Xem chi tiết', width: 50, align: 'center',sortable:false,resizable:false, formatter: StaffCatalogFormatter.viewCellIconFormatter},
			  ],	  
			  pager : '#pager',
			  height: 'auto',
			  rownumbers: true,	  
			  width: ($('#changedGrid').width())	  
			})
			.navGrid('#pager', {edit:false,add:false,del:false, search: false});
			$('#jqgh_grid_rn').prepend('STT');
		}
		return false;
	},
	loadInfo:function(){   //thong tin nhan vien tab load info
		$('#tabTitle').html('Thông tin nhân viên');
		$("#infoTab").addClass("Active");
		/*$("#supervisorTab").removeClass("Active");
		$("#categoryTab").removeClass("Active");
		$("#changedTab").removeClass("Active");
		$("#historyTab").removeClass("Active");*/
		var staffId = $("#staffId").val();
		var data = new Object();
		data.staffId = staffId;
		var kData = $.param(data, true);
		$.ajax({
			type : "POST",
			url : "/catalog_staff_manager/info",
			data : (kData),
			dataType: "html",
			success : function(data) {
				$("#divGeneral").html(data).show();
				$('#shopCode').focus();
				Utils.bindAutoSave();
			},
			error:function(XMLHttpRequest, textStatus, errorThrown) {
				$.ajax({
					type : "POST",
					url : '/check-session',
					dataType : "json",
					success : function(data) {
					},
					error: function(XMLHttpRequest, textStatus, errorThrown) {
						window.location.href = '/home';
					}
				});
			}
		});
		return false;
	},
	loadSupervisor:function(){
		$('#tabTitle').html('Giám sát nhân viên bán hàng');
		$("#infoTab").removeClass("Active");
		$("#supervisorTab").addClass("Active");
		$("#categoryTab").removeClass("Active");
		$("#changedTab").removeClass("Active");
		$("#historyTab").removeClass("Active");
		var staffId = $("#staffId").val();
		var data = new Object();
		data.staffId = staffId;
		var kData = $.param(data, true);
		$.ajax({
			type : "POST",
			url : "/catalog_staff_manager/supervisor",
			data : (kData),
			dataType: "html",
			success : function(data) {
				$("#divGeneral").html(data).show();
				$('#shopCode').focus();
				Utils.bindAutoSearch();
			},
			error:function(XMLHttpRequest, textStatus, errorThrown) {
				$.ajax({
					type : "POST",
					url : '/check-session',
					dataType : "json",
					success : function(data) {
					},
					error: function(XMLHttpRequest, textStatus, errorThrown) {
						window.location.href = '/home';
					}
				});
			}
		});
		return false;
	},
	loadCategory:function(flag){
		$('#tabTitle').html('Thông tin ngành hàng');
		$("#infoTab").removeClass("Active");
		$("#supervisorTab").removeClass("Active");
		$("#categoryTab").addClass("Active");
		$("#changedTab").removeClass("Active");
		$("#historyTab").removeClass("Active");
		if (flag) $('#btnCreate').unbind('click');
		var staffId = $("#staffId").val();
		var data = new Object();
		data.staffId = staffId;
		var kData = $.param(data, true);
		$.ajax({
			type : "POST",
			url : "/catalog_staff_manager/category",
			data : (kData),
			dataType: "html",
			success : function(data) {
				$("#divGeneral").html(data).show();
			},
			error:function(XMLHttpRequest, textStatus, errorThrown) {
				$.ajax({
					type : "POST",
					url : '/check-session',
					dataType : "json",
					success : function(data) {
					},
					error: function(XMLHttpRequest, textStatus, errorThrown) {
						window.location.href = '/home';
					}
				});
			}
		});
		return false;
	},
	loadChanged:function(){
		$('#tabTitle').html('Xem thông tin thay đổi');
		$("#infoTab").removeClass("Active");
		$("#supervisorTab").removeClass("Active");
		$("#categoryTab").removeClass("Active");
		$("#changedTab").addClass("Active");
		$("#historyTab").removeClass("Active");
		var staffId = $("#staffId").val();
		var data = new Object();
		data.staffId = staffId;
		var kData = $.param(data, true);
		$.ajax({
			type : "POST",
			url : "/catalog_staff_manager/changed",
			data : (kData),
			dataType: "html",
			success : function(data) {
				$("#divGeneral").html(data).show();
				$('#startDate').focus();
				Utils.bindAutoSearch();
			},
			error:function(XMLHttpRequest, textStatus, errorThrown) {
				$.ajax({
					type : "POST",
					url : '/check-session',
					dataType : "json",
					success : function(data) {
					},
					error: function(XMLHttpRequest, textStatus, errorThrown) {
						window.location.href = '/home';
					}
				});
			}
		});
		return false;
	},
	loadHistory:function(){
		$('#tabTitle').html('Lịch sử bán hàng');
		$("#infoTab").removeClass("Active");
		$("#supervisorTab").removeClass("Active");
		$("#categoryTab").removeClass("Active");
		$("#changedTab").removeClass("Active");
		$("#historyTab").addClass("Active");
		var staffId = $("#staffId").val();
		var data = new Object();
		data.staffId = staffId;
		var kData = $.param(data, true);
		$.ajax({
			type : "POST",
			url : "/catalog_staff_manager/history",
			data : (kData),
			dataType: "html",
			success : function(data) {
				$("#divGeneral").html(data).show();
				$('#startDate').focus();
				Utils.bindAutoSearch();
			},
			error:function(XMLHttpRequest, textStatus, errorThrown) {
				$.ajax({
					type : "POST",
					url : '/check-session',
					dataType : "json",
					success : function(data) {
					},
					error: function(XMLHttpRequest, textStatus, errorThrown) {
						window.location.href = '/home';
					}
				});
			}
		});
		return false;
	},
	changeStaffInfo: function(){
		var msg = '';
		$('#errMsg').html('').hide();
		msg = Utils.getMessageOfRequireCheck('staffCode','Mã nhân viên');
		if(msg.length==0){
			msg = Utils.getMessageOfSpecialCharactersValidate('staffCode','Mã nhân viên',Utils._CODE);
		}		
		if(msg.length == 0){
			msg = Utils.getMessageOfRequireCheck('staffName','Tên nhân viên');
		}		
		if(msg.length == 0){
			msg = Utils.getMessageOfSpecialCharactersValidate('staffName','Tên NV',Utils._NAME);
		}		
		if(msg.length == 0){
			msg = Utils.getMessageOfRequireCheck('gender','Giới tính',true);
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfRequireCheck('provinceCode','Tỉnh',true);
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfRequireCheck('districtCode','Quận/Huyện',true);
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfRequireCheck('wardCode','Phường/Xã',true);
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfRequireCheck('staffType','Loại nhân viên',true);
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfSpecialCharactersValidate('idPlace','Nơi cấp');
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfSpecialCharactersValidate('staffPhone','Số di động');
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfSpecialCharactersValidate('staffTelephone','Số cố định');
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfInvaildNumber('staffPhone','Số di động');
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfInvaildNumber('staffTelephone','Số cố định');
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfInvalidEmailFormat('email', 'Email');
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfSpecialCharactersValidate('saleGroup', 'Nhóm bán hàng');
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfSpecialCharactersValidate('idCard','CMND');
		}		
		if(msg.length == 0){
			if ($('#workStartDate').val().trim() != '' && !Utils.isDate($('#workStartDate').val().trim())) {
				msg = 'Ngày bắt đầu làm việc không hợp lệ, vui lòng nhập theo định dạng dd/MM/yyyy';
				$('#workStartDate').focus();
			}
		}
		if (msg.length == 0 && $('#idDate').val().trim() != '' && !Utils.isDate($('#idDate').val().trim())) {
			msg = 'Ngày cấp không hợp lệ, vui lòng nhập theo định dạng dd/MM/yyyy';
			$('#idDate').focus();
		}
		if (msg.length == 0 && $('#workStartDate').val().trim() != '' && !Utils.isDate($('#workStartDate').val().trim())) {
			msg = 'Ngày bắt đầu làm việc không hợp lệ, vui lòng nhập theo định dạng dd/MM/yyyy';
			$('#workStartDate').focus();
		}
		if (msg.length == 0 && $('#idDate').val().trim() != '' && !Utils.isDate($('#idDate').val().trim())) {
			msg = 'Ngày bắt đầu làm việc không hợp lệ, vui lòng nhập theo định dạng dd/MM/yyyy';
			$('#idDate').focus();
		}		
		if(msg.length > 0){
			$('#errMsg').html(msg).show();
			return false;
		}
		var dataModel = new Object();
		dataModel.staffId = $('#staffId').val();
		dataModel.shopCode = $('#shopCode').val().trim();
		dataModel.staffCode = $('#staffCode').val().trim();
		dataModel.staffName = $('#staffName').val().trim();
		dataModel.staffOwnerCode = $('#staffOwnerCode').val().trim();
		dataModel.staffOwnerName = $('#staffOwnerName').val().trim();
		dataModel.idCard = $('#idCard').val().trim();
		dataModel.idDate = $('#idDate').val().trim();
		dataModel.idPlace = $('#idPlace').val().trim();
		dataModel.provinceCode = $('#provinceCode').val().trim();
		dataModel.districtCode = $('#districtCode').val().trim();
		dataModel.wardCode = $('#wardCode').val().trim();
		dataModel.street = $('#street').val().trim();
		dataModel.address = $('#address').val().trim();
		dataModel.email = $('#email').val().trim();
		dataModel.saleGroup = $('#saleGroup').val().trim();
		dataModel.staffPhone = $('#staffPhone').val().trim();
		dataModel.staffType = $('#staffType').val();
		dataModel.saleTypeCode = $('#saleType').val();
		dataModel.status = $('#status').val();
		dataModel.gender = $('#gender').val();
		dataModel.workStartDate = $('#workStartDate').val().trim();
		dataModel.education = $('#education').val().trim();
		dataModel.position = $('#position').val().trim();
		dataModel.staffTelephone = $('#staffTelephone').val().trim();
		
		Utils.addOrSaveData(dataModel, "/catalog_staff_manager/changestaffinfo", StaffCatalog._xhrSave, null,function(data){
			$('#staffId').val(data.staffId);
			if(data.status == 1){
				if(data.supervisorStaffValue == 5){
					$('#supervisorTab').removeClass('Disable');
					$('#supervisorTab').bind('click',StaffCatalog.loadSupervisor);
					StaffCatalog.loadSupervisor();
				}else{
					$('#supervisorTab').addClass('Disable');
					$('#supervisorTab').unbind('click');
					StaffCatalog.loadCategory();
				}							
			}else{
				if(data.supervisorStaffValue == 1 || data.supervisorStaffValue == 2){
					$('#staffOwnerCode').removeAttr('disabled');
				}else{
					$('#staffOwnerCode').attr('disabled','disabled');
				}
				StaffCatalog.loadInfo();
			}
		});
		
		
		/*if(!Utils.validateAttributeData(dataModel, '#errMsg')){
			return false;
		}
		$.ajax({
			type : "POST",
			url : "/catalog_staff_manager/checkexistsupervisor",
			data : ({staffId:$('#staffId').val(),shopCode:$('#shopCode').val().trim(), staffType:$('#staffType').val()}),
			dataType: "json",
			success : function(data) {
				if(data.error == true){
					if(data.type == 0){//chuyen 
						$.messager.confirm('Xác nhận', 'Nhân viên này đang giám sát NVBH.Sau khi chuyển đơn vị bạn có muốn giữ lại danh sách giám sát này không? ', function(r){
							if (r == true){
								dataModel.isSupervisorStaff = 1;
							}else{
								dataModel.isSupervisorStaff = 0;
							}
							Utils.saveData(dataModel, "/catalog_staff_manager/changestaffinfo", StaffCatalog._xhrSave, null, function(data){
								$('#staffId').val(data.staffId);
								if(data.status == 1){
									if(data.supervisorStaffValue == 5){
										$('#supervisorTab').removeClass('Disable');
										$('#supervisorTab').bind('click',StaffCatalog.loadSupervisor);
										StaffCatalog.loadSupervisor();
									}else{
										$('#supervisorTab').addClass('Disable');
										$('#supervisorTab').unbind('click');
										StaffCatalog.loadCategory();
									}
									$('#categoryTab').removeClass('Disable');
									$('#changedTab').removeClass('Disable');
									$('#historyTab').removeClass('Disable');
									$('#categoryTab').bind('click',StaffCatalog.loadCategory);
									$('#changedTab').bind('click',StaffCatalog.loadChanged);
									$('#historyTab').bind('click',StaffCatalog.loadHistory);
								}else{
									if(data.supervisorStaffValue == 1 || data.supervisorStaffValue == 2){
										$('#staffOwnerCode').removeAttr('disabled');
									}else{
										$('#staffOwnerCode').attr('disabled','disabled');
									}							
								}
							});
						});	
					}else{
						var title = '';
						if(data.type == 1){
							title = 'Nhân viên này đang giám sát NVBH, bạn có muốn chuyển không?';
						}else{
							title = 'Bạn đang thực hiện điều chuyển đơn vị cho NVBH xin vui lòng chọn lại chính xác nhân viên quản lý của nhân viên này. Bạn đã chọn chính xác chưa?';
						}
						$.messager.confirm('Xác nhận',title, function(r) {
							if(r == true) {
								Utils.saveData(dataModel, '/catalog_staff_manager/changestaffinfo', StaffCatalog._xhrSave, null, function(data) {
									$('#staffId').val(data.staffId);
									if(data.status == 1){
										if(data.supervisorStaffValue == 5){
											$('#supervisorTab').removeClass('Disable');
											$('#supervisorTab').bind('click',StaffCatalog.loadSupervisor);
											StaffCatalog.loadSupervisor();
										}else{
											$('#supervisorTab').addClass('Disable');
											$('#supervisorTab').unbind('click');
											StaffCatalog.loadCategory();
										}
										$('#categoryTab').removeClass('Disable');
										$('#changedTab').removeClass('Disable');
										$('#historyTab').removeClass('Disable');
										$('#categoryTab').bind('click',StaffCatalog.loadCategory);
										$('#changedTab').bind('click',StaffCatalog.loadChanged);
										$('#historyTab').bind('click',StaffCatalog.loadHistory);
									}else{
										if(data.supervisorStaffValue == 1 || data.supervisorStaffValue == 2){
											$('#staffOwnerCode').removeAttr('disabled');
										}else{
											$('#staffOwnerCode').attr('disabled','disabled');
										}
										StaffCatalog.loadInfo();
									}
								});
							}
						});
					} 
				}else{
					
				}
			},
			error:function(XMLHttpRequest, textStatus, errorThrown) {
				$.ajax({
					type : "POST",
					url : '/check-session',
					dataType : "json",
					success : function(data) {
						$('#loading').css('visibility','hidden');
					},
					error: function(XMLHttpRequest, textStatus, errorThrown) {
						window.location.href = '/home';
					}
				});
			}
		});*/
		return false;
	},
	getSelectedInfo: function(id,shopCode,quantityMax){
		if(id!= null && id!= 0 && id!=undefined){
			$('#shopId').val(id);
		} else {
			$('#shopId').val(0);
		}
		setTextboxValue('shopCode',shopCode);
		setTextboxValue('quantityMax',quantityMax);
		$('#btnCreate,#btnSearch').hide();
		$('#btnUpdate').show();
	},
	getSelectedCategory: function(id){
		if(id!= null && id!= 0 && id!=undefined){
			$('#categoryId').val(id);
		} else {
			$('#categoryId').val(0);
		}
		setSelectBoxValue('category', id);
		$('#btnCreate').hide();
		$('#btnUpdate').show();
	},
	deleteCategory: function(categoryId){
		var dataModel = new Object();
		dataModel.categoryId = categoryId;	
		Utils.deleteSelectRowOnGrid(dataModel, 'ngành hàng', "/catalog_staff_manager/removecategory", StaffCatalog._xhrDel, null, null);		
		return false;		
	},
	changeStaffCategory: function(){
		var msg = '';
		$('#errMsg').html('').hide();
		var lstSize = $('#lstSize').val();
		var lstSaleCatSize = $('#lstSaleCatSize').val();
		var arrId =new Array();
		var arrDeleteId =new Array();
		var num = 0;
		for(var i=0;i<lstSize;i++){
			if($('#category_'+i).is(':checked')){
				num++;
				var flag = true;
				for(var j=0;j<lstSaleCatSize;j++){
					if(parseInt($('#categorySelected_'+j).val()) == parseInt($('#categoryId_'+i).val())){
						flag = false;
						break;
					}
				}
				if(flag){
					arrId.push($('#categoryId_'+i).val());
				}
			}else{
				if(lstSaleCatSize != null && lstSaleCatSize > 0){
					for(var j=0;j<lstSaleCatSize;j++){
						if(parseInt($('#categorySelected_'+j).val()) == parseInt($('#categoryId_'+i).val())){
							arrDeleteId.push($('#categoryId_'+i).val());
						}
					}
				}
			}
		}
		if(num == 0){
			msg = 'Bạn chưa chọn ngành hàng nào.Hãy chọn ít nhất một ngành hàng để thao tác';
		}
		if(msg.length > 0){
			$('#errMsg').html(msg).show();
			return false;
		}
		var dataModel = new Object();
		dataModel.staffId = $('#staffId').val();
		dataModel.lstCategoryId = arrId;
		dataModel.lstDeleteCategoryId = arrDeleteId;
		Utils.addOrSaveData(dataModel, "/catalog_staff_manager/changestaffcategory", StaffCatalog._xhrSave, null, function(){
			StaffCatalog.loadCategory();
		});
		return false;
	},
	loadTree: function(){
		var shopCode = $('#shopCodeHidden').val().trim();
		loadDataForTree('/rest/catalog/unit-tree/'+shopCode+'/tree.json');
		$('#tree').bind("loaded.jstree", function(event, data){
			if($('#treeContainer').data("jsp")!= undefined){
				$('#treeContainer').data("jsp").destroy();
			}
			$('#treeContainer').jScrollPane();
		});
		$('#tree').bind("select_node.jstree", function (event, data) {
			var name = data.inst.get_text(data.rslt.obj);
			var id = data.rslt.obj.attr("id");
			var code = data.rslt.obj.attr("contentItemId");
			var url = StaffCatalog.getGridUrl(code,'','','','','',-1,'','');
			$("#grid").setGridParam({url:url,page:1}).trigger("reloadGrid");
	    });
	},
	getSupervisorStaffName:function(typeCode,obj){
		$.getJSON('/rest/catalog/supervisor-staff/name/' + encodeChar(typeCode) + ".json",function(data){
			$(obj).val(data.name);
		});
	},
	checkProvinceCode:function(provinceCode,obj){
		$.getJSON('/rest/catalog/province/code/' + encodeChar(provinceCode) + ".json",function(data){
			if(data.value != null && data.value != 0){
				$(obj).removeAttr('disabled');
				$(obj).focus();
				$('#districtCode').bind('keyup', function(event){
					if(event.keyCode == keyCode_F9){
						var lstParam = new Array();
						var obj = {};
						obj.name = 'provinceCode';
						obj.value = $('#provinceCode').val().trim();
						lstParam.push(obj);
						CommonSearch.searchDistrictOnDialog(function(data){
							$('#districtCode').val(data.code);
							$('#wardCode').val("");
							$('#wardCode').removeAttr('disabled');
							$('#wardCode').focus();
						},lstParam);
					}
				});
			}
		});
	},
	checkDistrictCode:function(districtCode,obj){
		$.getJSON('/rest/catalog/district/code/' + encodeChar(districtCode) + ".json",function(data){
			if(data.value != null && data.value != 0){
				$(obj).removeAttr('disabled');
				$(obj).focus();
				$('#wardCode').bind('keyup', function(event){
					if(event.keyCode == keyCode_F9){
						var lstParam = new Array();
						var objP = {};
						objP.name = 'provinceCode';
						objP.value = $('#provinceCode').val().trim();
						var objD = {};
						objD.name = 'districtCode';
						objD.value = $('#districtCode').val().trim();
						lstParam.push(objP);
						lstParam.push(objD);
						CommonSearch.searchWardOnDialog(function(data){
							$('#wardCode').val(data.code);
						},lstParam);
					}
				});
			}
		});
	},
	showDialogStaff: function(){
		var html = $('#staffDialog').html();
		var staffId = $('#staffId').val();
		StaffCatalog._mapCheckStaff = new Map();
		$.fancybox(html,
				{
					modal: true,
					title: 'Thông tin NVBH',
					autoResize: false,
					afterShow: function(){
						$('#staffDialog').html('');
						$("#gridStaff").jqGrid({
							  url:StaffCatalog.getStaffGridUrlDialog('','','','', $('#staffId').val()),
							  colModel:[
					            {name:'staffCode', label: 'Mã NVBH', width: 120, sortable:false,resizable:false , align: 'left'},
					    	    {name:'staffName', label: 'Tên NVBH', width: 150, align: 'left', sortable:false,resizable:false },
					    	    {name:'shop.shopCode', label: 'Mã đơn vị', width: 80, sortable:false,resizable:false , align: 'left'},
					    	    {name:'shop.shopName', label: 'Tên đơn vị', width: 200, align: 'left', sortable:false,resizable:false },
					    	    {name: 'id',index:'id', hidden:true },
							  ],	  
							  pager : $('#pagerStaff'),
							  height: 'auto',
							  multiselect: true,
							  recordpos: 'right',
							  rowNum: 10,	
							  width: ($('#staffInfoGrid').width()),
							  beforeSelectRow: function (id, e) {
								  var _id = 'jqg_gridStaff_' + id;
								  var isCheck = StaffCatalog._mapCheckStaff.get(_id);
								  if(isCheck || isCheck == 'true') {
									  //da co trong map roi->remove no di
									  StaffCatalog._mapCheckStaff.remove(_id);
									  var tm = setTimeout(function(){
										  $('#'+_id).removeAttr('checked');
										  $('#'+id).removeClass('ui-state-highlight');
									  }, 100);
								  } else {
									  //chua co trong map -> put vao
									  StaffCatalog._mapCheckStaff.put(_id, id);
									  var tm = setTimeout(function(){
										  $('#'+_id).attr('checked', 'checked');
										  $('#'+id).addClass('ui-state-highlight');
									  }, 100);
								  }
								  return true;
							  },
							  onSelectAll: function(aRowids, status) {
								  for(var i = 0; i < aRowids.length; i++) {
									  if(status) {
										  var _id = 'jqg_gridStaff_' + aRowids[i];
										  //put vao map
										  StaffCatalog._mapCheckStaff.put(_id, aRowids[i]);
									  } else {
										  var _id = 'jqg_gridStaff_' + aRowids[i];
										  StaffCatalog._mapCheckStaff.remove(_id);
									  }
								  }
							  },
							  gridComplete: function(){
								  $('.cbox').each(function() {
									 var isCheck = StaffCatalog._mapCheckStaff.get(this.id);
									 if(isCheck || isCheck == 'true') {
										 staffId = $("#gridStaff").jqGrid('getCell',$(this).parent().parent().attr('id'),'id');
										 $(this).attr('checked', 'checked');										 
										 $('#'+staffId).addClass('ui-state-highlight');
									 }
								  });
								 
								  $('#jqgh_gridStaff_rn').html('STT');
								  $(window).resize();
								  $.fancybox.update();
								  $(window).resize();
								  //$('#gview_list4>div.ui-jqgrid-bdiv').jScrollPane();
								  updateRownumWidthForJqGrid();
								  
							  }
							})
							.navGrid($('#pagerStaff'), {edit:false,add:false,del:false, search: false});						
						$('#shopCodeDlg').bind('keyup', function(event){
							
							if(event.keyCode == keyCode_F9){
								var arrParams = new Array();
								var params = new Object();
								params.name = 'filterShop';
								params.value = 1;
								arrParams.push(params);
								CommonSearch.searchShopOnEasyUI(function(data){
									$('#shopCodeDlg').val(data.code);
									$('#shopNameDlg').val(data.name);
								}, arrParams,'searchStyle1EasyUI');
							}
						});
						$('#staffCodeDlg').bind('keyup', function(event){
							if(event.keyCode == keyCode_F9){
								var arrParams = new Array();
								var params = new Object();
								params.name = 'shopCode';
								params.value = $('#shopCodeDlg').val().trim();
								arrParams.push(params);
								CommonSearch.searchStaffOnEasyUI(function(data){
									$('#staffCodeDlg').val(data.code);
									$('#staffNameDlg').val(data.name);
								}, arrParams,'searchStyle1EasyUI');
							}
						});
						$('#btnDlgStaff').bind('click',function(event){
							var shopCode = $('#shopCodeDlg').val().trim();
							var shopName = $('#shopNameDlg').val().trim();
							var staffCode = $('#staffCodeDlg').val().trim();
							var staffName = $('#staffNameDlg').val().trim();
							var url = StaffCatalog.getStaffGridUrlDialog(shopCode,shopName,staffCode,staffName, $('#staffId').val());
							$("#gridStaff").setGridParam({url:url,page:1}).trigger("reloadGrid");
						});
					},
					afterClose: function(){
						$('#staffDialog').html(html);
						StaffCatalog._mapCheckStaff = new Map();
					}
				}
			);
		return false;
	},
	selectListStaff: function(){
		$('#errMsgCustomerDlg').html('').hide();
		var arrId = new Array();
		var supervisorId = $('#staffId').val();
		var staffId = '';
		/*$('.fancybox-inner .ui-jqgrid-bdiv .cbox').each(function(){
			if($(this).is(':checked')){
				staffId = $("#gridStaff").jqGrid('getCell',$(this).parent().parent().attr('id'),'id');
				arrId.push(staffId);
			}
		});*/
		var msg = '';
		for(var i = 0; i < StaffCatalog._mapCheckStaff.keyArray.length; i++) {
			arrId.push(StaffCatalog._mapCheckStaff.get(StaffCatalog._mapCheckStaff.keyArray[i]));
		}
		if(arrId.length == 0){
			msg = format(msgErr_required_field,'Thông tin NVBH');
		}
		if(msg.length > 0){
			$('#errMsgCustomerDlg').html(msg).show();
			$.fancybox.update();
			 $(window).resize();
			return false;
		}
		var dataModel = new Object();		
		dataModel.lstStaffId = arrId;
		dataModel.staffId = supervisorId;
		Utils.addOrSaveData(dataModel, '/catalog_staff_manager/changestaffsupervisor', StaffCatalog._xhrSave, 'errMsgCustomerDlg', function(data){
			$('#successMsgCustomerDlg').html('Lưu dữ liệu thành công').show();
			$('#shopCode').val('');
			$('#shopName').val('');
			$('#staffCode').val('');
			$('#staffName').val('');
			StaffCatalog.loadSupervisor();
			$.fancybox.close();
		}, null, '.fancybox-inner', null, null);
		return false;
	},
	deleteSupervisor: function(staffId){
		var dataModel = new Object();
		dataModel.staffId = staffId;
		Utils.deleteSelectRowOnGrid(dataModel, 'NVBH', "/catalog_staff_manager/removesupervisor", StaffCatalog._xhrDel, null, null,function(){
			StaffCatalog.loadSupervisor();
		});		
		return false;		
	},
	loadTreeEx:function(){
		StaffCatalog.loadShopTree();
		$('#tree').bind("loaded.jstree", function(event, data){
        	if($('#treeContainer').data("jsp")!= undefined){
        		$('#treeContainer').data("jsp").destroy();
        	}
        	$('#treeContainer').jScrollPane();
        });
	},
	loadShopTree:function(){
		$('#tree').jstree({
	        "plugins": ["themes", "json_data","ui"],
	        "themes": {
	            "theme": "classic",
	            "icons": false,
	            "dots": true
	        },
	        "json_data": {
	            "ajax": {
	            	"url": function( node ){
	                      if( node == -1 ){
	                    	  return "/rest/catalog/unit-tree-ex/"+$('#shopIdHidden').val().trim()+"/0/tree.json";
	                      } else {
	                    	  return "/rest/catalog/unit-tree-ex/"+node.data("attr").id+"/1/tree.json";
	                      }
	                    },
	                "type": 'GET',
	                "success": function(ops) {
	                    data = [];
	                    for( opnum in ops ){
	                      var op = ops[opnum];
	                      node = {
	                          "data" : op.data,
	                          "attr" : op.attr,
	                          "metadata" :  op ,
	                          "state"	: op.state
	                          /*"state" : "closed"*//*PhuT*/
	                      };
	                      data.push( node );
	                    }
	                    return data;
	                },
	                'complete':function(){
	                	$('#tree').bind("select_node.jstree", function (event, data) {
	            			var isAll = false;
	            			if($('#isAll').is(':checked')){
	            				isAll = true;
	            			}
	            			var name = data.inst.get_text(data.rslt.obj);
	            			var id = data.rslt.obj.attr("id");

	            			var shopCode = data.rslt.obj.attr("contentItemId");
	            			
	            			var url = StaffCatalog.getGridUrl(shopCode,'','','',-1,1,-1,'','');
	            			$("#grid").setGridParam({url:url,page:1}).trigger("reloadGrid"); 
	            			$('#parentCode').val(id);	            			
	            			/*var _url='/rest/catalog/shop-tree-ex/combotree/'+$('#shopCodeHidden').val().trim()+'/1/1/'+id+'.json';
	            			UnitTreeCatalog.generalFunc(_url);
	            			var t = $('#parentCode').combotree('tree');		            			            			
	            			$('#parentCode').combotree('setValue',name);	            			
	            			$('#parentCodeS').val(id);*/
	            			
	            	    });
	                	$('#tree').bind("loaded.jstree", function(event, data){
	                    	if($('#treeContainer').data("jsp")!= undefined){
	                    		$('#treeContainer').data("jsp").destroy();
	                    	}
	                    	$('#treeContainer').jScrollPane();
	                    });
	                	var tm = setTimeout(function() {
		                    if($('#treeContainer').data("jsp")!= undefined){
		                		$('#treeContainer').data("jsp").destroy();
		                	}
		                    $('#treeContainer').css('height',$('.Content').height()-75);
		  				  	$('#treeContainer').jScrollPane();
	                    }, 500);
	                }
	            }
	        },
	        "core" : {"load_open" : true }
		});	
	},
	getStaffType:function(typeCode,obj){
		$.getJSON('/rest/catalog_staff_manager/gettype/' + encodeChar(typeCode) + ".json",function(data){
			if(data.value != '' && data.value > 0){
				$(obj).removeAttr('disabled');
			}else{
				$('#staffOwnerName').val('');
				$(obj).val('');
				$(obj).attr('disabled','disabled');
			}
		});
	},
	gotoTab: function(tabIndex){
		StaffCatalog.deactiveAllMainTab();
		switch (tabIndex) {
		case 0:
			$('#tabContent1').show();
			$('#tab1 a').addClass('Active');
			break;
		case 1:
			$('#tabContent2').show();
			$('#tab2 a').addClass('Active');
			AttributesManager.getAttributesForm('tabContent2',AttributesManager.STAFF);
			$('#typeAttribute').val(AttributesManager.STAFF);
			break;
		default:
			$('#tabContent1').show();
			break;
		}
	},	
	deactiveAllMainTab: function(){
		$('#tab1 a').removeClass('Active');
		$('#tab2 a').removeClass('Active');
		$('#tabContent1').hide();
		$('#tabContent2').hide();
	},
	importExcel:function(){
 		$('#isView').val(0);
 		var options = { 
				beforeSubmit: ProductLevelCatalog.beforeImportExcel,   
		 		success:      ProductLevelCatalog.afterImportExcelUpdate, 
		 		type: "POST",
		 		dataType: 'html',
		 		data:({excelType:$('#excelType').val()})
		 	}; 
		$('#importFrm').ajaxForm(options);
 		$('#importFrm').submit();
 		return false;
 	},	
 	viewExcel:function(){
 		$('#isView').val(1);
 		var options = { 
				beforeSubmit: ProductLevelCatalog.beforeImportExcel,   
		 		success:      ProductLevelCatalog.afterImportExcelUpdate, 
		 		type: "POST",
		 		dataType: 'html',
		 		data:({excelType:$('#excelType').val()})
		 	}; 
		$('#importFrm').ajaxForm(options);
		$('#importFrm').submit();
 		return false;
 	},
 	exportActionLog: function(){
		var staffId = $('#staffId').val().trim();
		var startDate = $('#startDate').val().trim();
		var endDate = $('#endDate').val().trim();
		ExportActionLog.exportActionLog(staffId, ExportActionLog.STAFF, startDate, endDate,'errMsgActionLog');
	},
 	//CuongND
 	exportExcel:function(){
		$('#errMsg').html('').hide();
 		var shopCodeHidden = $('#shopCodeHidden').val().trim();
		var staffCode = $('#staffCode').val().trim();
		var staffName = $('#staffName').val().trim();
		var staffPhone = $('#staffPhone').val().trim();
		var staffType = $('#staffType').val();
		var status = $('#status').val();
		var saleTypeCode = $('#saleType').val();
		var shopCode = $('#shopCode').val().trim();
		var shopName = $('#shopName').val().trim();
		var excelType = $('#excelType').val().trim();
		var saleType = $('#saleType').val().trim();
		
		var params = new Object();
		params.shopCodeHidden = shopCodeHidden;
		params.staffCode = staffCode;
		params.staffName = staffName;
		params.staffPhone = staffPhone;
		params.saleTypeCode = saleTypeCode;
		params.status =status;
		params.saleType =saleType;
		params.shopCode = shopCode;
		params.shopName = shopName;
		params.excelType = excelType;
		params.staffType = staffType;
		
		var url = "/catalog_staff_manager/export-excel";
		CommonSearch.exportExcelData(params,url);
 		return false;
 	},
 	getListSaleCatByStaffId:function(){
 		var data = new Object();
		data.staffId = $('#infStaffCodeHid').val().trim();	
		var kData = $.param(data,true);
 		$.ajax({
			type : "POST",
			url : "/catalog_staff_manager/get-list",
			data :(kData),
			dataType: "json",
			success : function(result) {
				if(result.lst!=null){
					StaffCatalog._mapStaffCat = new Map();
					for(var i=0;i<result.lst.length;i++){
						StaffCatalog._mapStaffCat.put(result.lst[i].cat.id,result.lst[i].cat.id);
						$('.InputCbxStyle').each(function(){
							if($(this).val() == result.lst[i].cat.id){
								$(this).attr('checked',true);
							}
						});
					}
				}
			}
 		});
	},
 	saveStaffSaleCat: function() {
		var msg = '';
		var lstCatId = new Array();
		var lstDelete = new Array();
		$('.InputCbxStyle').each(function(){
			var catId = $(this).val().trim();
			if ($(this).is(':checked') && StaffCatalog._mapStaffCat.get(catId)==null){
				lstCatId.push(catId);
			}else if(!$(this).is(':checked') && StaffCatalog._mapStaffCat.get(catId)!=null){
				lstDelete.push(catId);
			}
		});
		var staffId = $('#infStaffCodeHid').val().trim();
		var params = new Object();		
		params.lstCatId = lstCatId ;
		params.staffId = staffId;
		params.lstDelete = lstDelete;
		Utils.addOrSaveData(params, '/catalog_staff_manager/save-staff-sale-cat', null, 'errMsg', function(data) {		
				$('#successMsg').html('Lưu dữ liệu thành công').show();
				var tm = setTimeout(function(){
					$('#successMsg').html('').hide();
					clearTimeout(tm);
				}, 2000);
		}, 'adjustLoading');
	},
	bindAutoUpdate: function(){
		var check = false;
		if (!check){
			$('.easyui-window .InputTextStyle').each(function(){
			    if(!$(this).is(':hidden')){
			    	check=true;			    			    	
			   		$(this).bind('keyup',function(event){
			   			if(event.keyCode == keyCodes.ENTER){		   				
			   				$('.easyui-window .BtnSearchOnDialog').each(function(){	   					
			   					if(!$(this).is(':hidden')){
			   						$(this).click();
			   					}
			   				});
			   			}
			   		});    
			    }	    
			});
		}
		if (!check){
			$('.easyui-dialog .panel .InputTextStyle').each(function(){
			    if(!$(this).is(':hidden')){
			    	check=true;
			   		$(this).bind('keyup',function(event){
			   			if(event.keyCode == keyCodes.ENTER){		   				
			   				$('.easyui-dialog .BtnSearchOnDialog').each(function(){	   					
			   					if(!$(this).is(':hidden')){
			   						$(this).click();
			   					}
			   				});
			   			}
			   		});    
			    }	    
			});
		}
		if (!check){
			$('.ContentSection .InputTextStyle,.SearchSection .InputTextStyle').each(function(){
			    if(!$(this).is(':hidden')){
			   		$(this).bind('keyup',function(event){
			   			if(event.keyCode == keyCodes.ENTER){		   				
			   				if($('#btnCapNhat')!= null && $('#btnCapNhat').html()!= null && $('#btnCapNhat').html().trim().length > 0 && !$('#btnCapNhat').is(':hidden')){
			   					$('#btnCapNhat').click();
			   				}
			   				$('.btnCapNhat').each(function(){	   					
			   					if(!$(this).is(':hidden')){
			   						$(this).click();
			   					}
			   				});
			   			}
			   		});    
			    }	    
			});
		}		
	}
};
/*
 * END OF FILE - /web/web/resources/scripts/business/catalog/vnm.staff-catalog.js
 */

/*
 * START OF FILE - /web/web/resources/scripts/business/catalog/vnm.staff-type-catalog.js
 */
var StaffTypeCatalog = {
	_xhrSave : null,
	_xhrDel: null,
	isContextMenu:0,
	parentsId:null,
	_selObjType: null,	
	getGridUrl: function(staffTypeCode,parentCode,staffTypeName,status,staffObjectType,isGetParent){
		return "/catalog/staff-type/search?staffTypeCode=" + encodeChar(staffTypeCode) + "&parentCode=" + encodeChar(parentCode) + "&staffTypeName=" + encodeChar(staffTypeName) + "&status=" + status+ "&staffObjectType=" + encodeChar(staffObjectType) +"&isGetParent=" + encodeChar(isGetParent);
	},	
	search: function(){
		$('#errMsg').html('').hide();
		var staffTypeCode = $('#staffTypeCode').val().trim();
		var parentCode = $('#parentCode').combotree('getValue');
		if(parentCode == '' || parentCode == null){
			parentCode = -1;
		}
		var staffTypeName = $('#staffTypeName').val().trim();
		var status = $('#status').val().trim();
		var staffObjectType = $('#staffObjectType').val().trim();
		var url = StaffTypeCatalog.getGridUrl(staffTypeCode,parentCode,staffTypeName,status,staffObjectType,false);
		$("#grid").datagrid({url:url,page:1});
		var tm = setTimeout(function() {
			$('#staffTypeCode').focus();
		}, 500);
		return false;
	},
	change: function(){
		var msg = '';
		$('#errMsg').html('').hide();
		msg = Utils.getMessageOfRequireCheck('staffTypeCode','Mã loại nhân viên');
		if(msg.length == 0){
			msg = Utils.getMessageOfSpecialCharactersValidate('staffTypeCode','Mã loại nhân viên',Utils._CODE);
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfRequireCheck('staffTypeName','Tên loại');
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfSpecialCharactersValidate('staffTypeName','Tên loại');
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfRequireCheck('status','Trạng thái',true);
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfRequireCheck('staffObjectType','Mã phân loại',true);
		}
		if($('#status').val() != 1 && $('#status').val() != 0 && msg.length == 0){
			msg = "Bạn chưa nhập giá trị cho trường Trạng thái. Yêu cầu nhập giá trị";
		}
		if(msg.length > 0){
			$('#errMsg').html(msg).show();
			return false;
		}
		var dataModel = new Object();
		dataModel.id = $('#staffTypeId').val().trim();
		dataModel.staffTypeCode = $('#staffTypeCode').val().trim();
		dataModel.staffTypeName = $('#staffTypeName').val().trim();
		dataModel.parentCode = $('#parentCode').combotree('getValue');
		dataModel.status = $('#status').val().trim();
		dataModel.staffObjectType = $('#staffObjectType').val().trim();
		Utils.addOrSaveRowOnGrid(dataModel, "/catalog/staff-type/save", StaffTypeCatalog._xhrSave, null, null,function(){
			$('#contentGrid').show();
			StaffTypeCatalog.loadTree();
			StaffTypeCatalog.resetForm();
			var url = StaffTypeCatalog.getGridUrl('',-1,'',1,-1,false);
			$("#grid").datagrid({url:url,page:1});
		});
		return false;
	},
	getSelectedStaffType: function(id,staffTypeCode,parentCode,staffTypeName,status,staffObjectType){
		$('#errMsg').html('').hide();
		$('#selObjType').val(staffObjectType);
		var url = 'channelTypeId='+ parentCode;
		$.getJSON('/rest/catalog/catalog/staff-type/channeltype.json?'+url, function(data){
			if(data != null && data.status != 'RUNNING'){
				$('#errMsg').html('Bạn phải thay đổi mã cha về trạng thái hoạt động trước khi chỉnh sửa được đối tượng này').show();
			} else {
				$('#parentCode').combotree("reload",'/rest/catalog/staff-type/combotree/1/'+id+'.json');
				if(id!= null && id!= 0 && id!=undefined){
					hideOption('parentCode',id);
					$('#staffTypeId').val(id);
				} else {
					$('#staffTypeId').val(0);
				}
				$('#staffTypeCode').attr('disabled',true);
				setTextboxValue('staffTypeCode',staffTypeCode);
				setTextboxValue('staffTypeName',staffTypeName);				
				setSelectBoxValue('status', status);
				setSelectBoxValue('staffObjectType', staffObjectType);
				$('#parentCode').combotree('setValue',parentCode);
				Utils.getChangedForm();
				//setTitleUpdate();
				$('#divParentCode').show();
				$('#staffObjectType').attr('disabled', true);
				$('#parentCode').combotree('disable');
				$('#title').html('Thông tin loại nhân viên');
				$('#title1').html('Thông tin loại nhân viên');
				$('#btnSearch,#btnCreate').hide();
				$('#btnUpdate,#btnDismiss').show();
			}
		});
	},
	resetForm: function(){
		$('#errMsg').html('').hide();
		setTitleSearch();
		$('#title1').html('Danh sách loại nhân viên');
		$('.ReqiureStyle').hide();
		$('#divParentCode').hide(); //an ma cha
		$('#btnUpdate,#btnDismiss').hide();
		$('#btnSearch').show();
		$('#parentCode').combotree('enable');
		$('#staffTypeCode').val('');
		$('#staffTypeCode').removeAttr('disabled');
		$('#staffTypeCode').focus();
		$('#staffTypeName').val('');
		resetAllOptions('parentCode');
		$('#parentCode').combotree('clear');
		$('#parentCode').combotree("reload",'/rest/catalog/staff-type/combotree/0/0.json');
		$('#parentCode').combotree("setValue",-1);
		setSelectBoxValue('status',1);
		setSelectBoxValue('staffObjectType',-1);
		$('#staffObjectType').removeAttr('disabled');
		$('#staffTypeId').val(0);
		$('#selObjType').val(-1);
		var url = StaffTypeCatalog.getGridUrl('',-1,'',1,-1,false);
		//$("#grid").datagrid({url:url,page:1});
		return false;
	},
	deleteStaffType: function(staffTypeCode){
		$('#errMsg').html('').hide();
		var dataModel = new Object();
		dataModel.staffTypeCode = staffTypeCode;	
		Utils.deleteSelectRowOnGrid(dataModel, 'loại nhân viên', "/catalog/staff-type/remove", StaffTypeCatalog._xhrDel, null, null,function(){
			StaffTypeCatalog.loadTree();
			StaffTypeCatalog.resetForm();
		});		
		return false;		
	},
	loadTree: function(){
		//loadDataForTree('/rest/catalog/staff-type/tree.json');
		$('#tree').jstree({
	        "plugins": ["themes", "json_data","ui","contextmenu"],
	        "themes": {
	            "theme": "classic",
	            "icons": false,
	            "dots": true
	        },
	        "json_data": {
	        	"ajax" : {
	                "url" : '/rest/catalog/staff-type/tree.json',
	                "data" : function (n) {
	                        return { id : n.attr ? n.attr("id") : 0 };
	                    }
	            }        	
	        },
	        'onExpand':function(){
	        	console.log(0);
	        },
	        "contextmenu" : {
	            items : { // Could be a function that should return an object like this one
	                "create" : {
	                    "separator_before"  : false,
	                    "separator_after"   : true,
	                    "_class"			:"Item Item 1",
	                    "label"             : "Thêm loại nhân viên",
	                    "action"            : function(obj){
	                    						var nodeId = obj.attr('id').trim();
	                    						StaffTypeCatalog.setSelectedNode(nodeId);
	                    						StaffTypeCatalog.isContextMenu = 1;
	                    						StaffTypeCatalog.parentsId = nodeId;
	                    						StaffTypeCatalog.newCusType();
	                    }
	                },
					"update" : {
						"separator_before"	: false,
						"separator_after"	: false,
						"_class"			:"Item Item 2",
						"label"				: "Chỉnh sửa",
						"action"			: function (obj) { 
							var nodeId = obj.attr('id').trim();
							StaffTypeCatalog.setSelectedNode(nodeId);
							StaffTypeCatalog.isContextMenu = 1;
							if(nodeId != '-1'){
								StaffTypeCatalog.parentsId = $.jstree._reference('#tree')._get_parent(obj).attr("id");
							}else{
								StaffTypeCatalog.parentsId = '-1';
								return false;
							}
							
                        	$.ajax({
                				type : "POST",
                				url : '/catalog/staff-type/update',
                				dataType : "json",
                				data :$.param({id : nodeId}, true),
                				success : function(result) {
                					//hidden div chua grid
                					$('#contentGrid').hide();
                					//$('#divGeralMilkBox').show();
                					var channelType = result.channelType;
                					var status = 1;
                					if(channelType.status != activeStatus){
                						status = 0;
                					}
                					$('#selObjType').val(channelType.objectType);
                					$('#staffTypeId').val(nodeId.toString());
                					$('#staffTypeCode').attr('disabled','disabled');
                					$('#parentCode').combotree('disable');
                					setTextboxValue('staffTypeCode',channelType.channelTypeCode.toString());
                					setTextboxValue('staffTypeName',channelType.channelTypeName.toString());
                					setSelectBoxValue('status',status);
                					setSelectBoxValue('staffObjectType',channelType.objectType);
                					StaffTypeCatalog.getChangedForm();
                					StaffTypeCatalog.isContextMenu = 0;
                					$('#divParentCode').show();
                					$('#staffObjectType').attr('disabled', true);
                					$('#title').html('Thông tin loại nhân viên');
                					$('#title1').html('Thông tin loại nhân viên');
                					$('#staffTypeName').focus();
                					
                				}
                        	});
						}
					}
	            }
	        }
		});	
		$('#tree').bind("loaded.jstree", function(event, data){
			if($('#treeContainer').data("jsp")!= undefined){
				$('#treeContainer').data("jsp").destroy();
			}
			$('#treeContainer').jScrollPane();
			StaffTypeCatalog.setSelectedNode("-1");
		});
		$('#tree').bind("select_node.jstree", function (event, data) {
			var name = data.inst.get_text(data.rslt.obj);
			var id = data.rslt.obj.attr("id");
			$('#contentGrid').show();
			if(id == -1){
				id = '-1';
			}else{
				StaffTypeCatalog.parentsId = data.inst._get_parent(data.rslt.obj).attr("id"); // lay id node cha
			}
			var url = StaffTypeCatalog.getGridUrl('',id,'',1,-1,true);
			$("#grid").datagrid({url:url,page:1});
			StaffTypeCatalog.resetForm();
	    });
		$("#tree").bind("open_node.jstree", function (event, data) { 
			$('.jspContainer').css('height','auto');
		}); 
	},
	getChangedForm: function(){
		if(StaffTypeCatalog.isContextMenu == 1){
			if(StaffTypeCatalog.parentsId == null || StaffTypeCatalog.parentsId=='' ){
				StaffTypeCatalog.parentsId = '-1';
			}
			$('#parentCode').combotree('setValue',StaffTypeCatalog.parentsId);
		}
		$('#errMsg').hide();
		$('#divParentCode').show();
		$('.ReqiureStyle').show();		
		$('#btnSearch').hide();
		$('#btnUpdate').show();
		$('#title').html('Thông tin loại nhân viên');
		$('#title1').html('Thông tin loại nhân viên');
	},
	getAppramCode : function(parentId){
		if(parentId ==-1){	
			var selObjType = $('#selObjType').val().trim();
			if(selObjType.length == 0){
				selObjType = -1;
			}
			setSelectBoxValue('staffObjectType',selObjType);
		} else{
			var url = 'channelTypeId='+ parentId;
			$.getJSON('/rest/catalog/catalog/staff-type/channeltype.json?'+url, function(data){
				$('#staffObjectType').attr('disabled', true);
				if(data !=null || data != undefined) {
					setSelectBoxValue('staffObjectType',data.objectType);
				} else {					
					setSelectBoxValue('staffObjectType',-1);
					
				}
			});
		}
	},
	getListParentCode:function(parentId,objParentCode){
		$.ajax({
			type : "POST",
			url : "/catalog/staff-type/getlistparentcode",
			data :({parentCode:parentId}),
			dataType : "json",
			success : function(data) {
				var html = new Array();
    			if(data!= null && data!= undefined && data.lstParentCode!= null && data.lstParentCode.length > 0){
    				html.push("<option value='-1'>---Chọn mã cha---</option>");
    				for(var i=0;i<data.lstParentCode.length;i++){
    					html.push("<option value='"+ data.lstParentCode[i].id +"'>"+ data.lstParentCode[i].channelTypeName +"</option>");
    				}
    			}else{
    				html.push("<option value='-1'>---Chọn mã cha---</option>");
    			}	        			        	
    			$(objParentCode).html(html.join(""));
    			$(objParentCode).val(parentId);
    			$(objParentCode).change();
			},
			error:function(XMLHttpRequest, textStatus, errorThrown) {
				$.ajax({
					type : "POST",
					url : '/check-session',
					dataType : "json",
					success : function(data) {
					},
					error: function(XMLHttpRequest, textStatus, errorThrown) {
						window.location.href = '/home';
					}
				});
			}
		});
	},
	newCusType:function(){
		$('#contentGrid').hide();
		StaffTypeCatalog.resetForm();
		StaffTypeCatalog.getChangedForm();
	},
	setSelectedNode:function(nodeId){
		$('#tree a').removeClass('jstree-hovered jstree-clicked');
		$('#tree #'+nodeId+' >a').addClass('jstree-clicked');
	}
};
/*
 * END OF FILE - /web/web/resources/scripts/business/catalog/vnm.staff-type-catalog.js
 */

/*
 * START OF FILE - /web/web/resources/scripts/business/catalog/vnm.sub-category-catalog.js
 */
var SubCategoryCatalog = {
	_xhrSave : null,
	_xhrDel: null,
	getGridUrl: function(code,name,description,status){
		return "/catalog/sub-category/search?code=" + encodeChar(code) + "&name=" + encodeChar(name) + "&note=" + encodeChar(description) + "&status=" + status;
	},	
	search: function(){
		$('#errMsg').html('').hide();
		var code = $('#subcategoryCode').val().trim();
		var name = $('#subcategoryName').val().trim();
		var description = $('#description').val().trim();
		var status = 1;
		var tm = setTimeout(function() {
			$('#subcategoryCode').focus();
		}, 500);
		var url = SubCategoryCatalog.getGridUrl(code,name,description,status);
		$("#grid").datagrid({url:url,page:1});
		return false;
	},
	save: function(){
		var msg = '';
		$('#errMsg').html('').hide();
		msg = Utils.getMessageOfRequireCheck('subcategoryCodePop','Mã Sub Cat');
		if(msg.length == 0){
			msg = Utils.getMessageOfSpecialCharactersValidate('subcategoryCodePop','Mã Sub Cat',Utils._CODE);
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfRequireCheck('subcategoryNamePop','Tên Sub Cat');
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfSpecialCharactersValidate('subcategoryNamePop','Tên Sub Cat');
		}

		if(msg.length == 0 && $('#subdescriptionPop').val().trim().length > 0){
			msg = Utils.getMessageOfSpecialCharactersValidate('subdescriptionPop','Mô tả');
		}

		if(msg.length > 0){
			$('#errMsg').html(msg).show();
			return false;
		}
		var dataModel = new Object();
		dataModel.id = $('#selId').val().trim();
		dataModel.code = $('#subcategoryCodePop').val().trim();
		dataModel.name = $('#subcategoryNamePop').val().trim();
		dataModel.note = $('#subdescriptionPop').val().trim();
		dataModel.status = 1;
		Utils.addOrSaveRowOnGrid(dataModel, "/catalog/sub-category/save", SubCategoryCatalog._xhrSave, null, null, function(data){
			if(!data.error){
				$('#popup1').dialog('close');
				SubCategoryCatalog.search();
			}
		});		
		return false;
	},
	getSelectedSubCategory: function(rowId, code, name, description, status){
		if(rowId != null && rowId != 0 && rowId !=undefined){
			$('#selId').val(rowId);
		} else {
			$('#selId').val(0);
		}
		if(description == 'null'){
			description= '';
		}
		$('#subcategoryCodePop').val(code);
		$('#subcategoryNamePop').val(name);
		$('#subdescriptionPop').val(description);
        $('#popup1').dialog('open');
		return false;
	},
	resetForm: function(){
		SubCategoryCatalog.clearData();
		$("#grid").trigger("reloadGrid");
		return false;
	},
	deleteSubCategory: function(code){
		var dataModel = new Object();
		dataModel.code = code;	
		Utils.deleteSelectRowOnGrid(dataModel, 'sub category', '/catalog/sub-category/delete', SubCategoryCatalog._xhrDel, null, null,function(data){
			SubCategoryCatalog.clearData();
		});
		return false;		
	},
	clearData: function(){
		setTitleSearch();
		$('.RequireStyle').hide();
		$('#selId').val(0);
		$('#errMsg').html('').hide();
		$('#code').removeAttr('disabled');
		$('#btnEdit').hide();
		$('#btnCancel').hide();
		$('#btnSearch').show();
		$('#btnAdd').show();
		$('#code').val('');
		$('#name').val('');
		$('#description').val('');
		focusFirstTextbox();
		setSelectBoxValue('status', 1);
		SubCategoryCatalog.search();
	},
	getChangedForm: function(){
		$('.RequireStyle').show();
		$('#btnSearch').hide();
		$('#btnAdd').hide();
		$('#btnEdit').show();
		$('#btnCancel').show();	
		$('#code').focus();
		focusFirstTextbox();
		setTitleAdd();
	}
};
/*
 * END OF FILE - /web/web/resources/scripts/business/catalog/vnm.sub-category-catalog.js
 */

/*
 * START OF FILE - /web/web/resources/scripts/business/catalog/vnm.sub-category-secondary.js
 */
var SubCategorySecondary = {
	_xhrSave : null,
	_xhrDel: null,
	
	search: function(){
		$('#errMsg').html('').hide();
		var productInfoMap = $('#productInfoMap').val().trim();
		var productInfoCode = $('#productInfoCode').val().trim();
		var productInfoName = $('#productInfoName').val().trim();
		var status = 1;
		var tm = setTimeout(function() {
			$('#productInfoMap').focus();
		}, 500);
		var params = new Object();
		params.productInfoMap = productInfoMap;
		params.productInfoCode = productInfoCode;
		params.productInfoName = productInfoName;
		params.status = status;
		$("#grid").datagrid('load',params);
		return false;
	},
	save: function(){
		var msg = '';
		$('#errMsgPop').html('').hide();
		msg = Utils.getMessageOfRequireCheck('productInfoMapPop','Mã ngành hàng',true);
		if(msg.length == 0){
			msg = Utils.getMessageOfRequireCheck('productInfoCodePop','Mã ngành hàng con phụ');
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfSpecialCharactersValidate('productInfoCodePop','Mã ngành hàng con phụ',Utils._CODE);
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfRequireCheck('productInfoNamePop','Tên ngành hàng con phụ');
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfSpecialCharactersValidate('productInfoNamePop','Tên ngành hàng con phụ');
		}

		if(msg.length > 0){
			$('#errMsgPop').html(msg).show();
			return false;
		}
		var params = new Object();
		params.productInfoId = $('#selId').val().trim();
		params.productInfoMap = $('#productInfoMapPop').val().trim();
		params.productInfoCode = $('#productInfoCodePop').val().trim();
		params.productInfoName = $('#productInfoNamePop').val().trim();
		params.status = 1;
		Utils.addOrSaveData(params, "/catalog/sub_category_sec_catalog/save", SubCategorySecondary._xhrSave, 'errMsgPop',function(data){
			var tm = setTimeout(function() {
			$('#popup1').dialog('close');
				$("#grid").datagrid("reload");
					clearTimeout(tm);
			}, 800);
		});		
		return false;
	},
	addSubCategorySecondary:function(){
		$('#divDialog').css('visibility','visible');
		$('#popup1').dialog({
			title : 'Thông tin ngành hàng con phụ',
			onOpen: function(){
				SubCategorySecondary.clearPop();
			},
			onClose : function() {
				SubCategorySecondary.clearPop();
			}
		});
		$('#popup1').dialog('open');
	},
	getSelectedSubCategorySecondary: function(rowId,productInfoMap, productInfoCode, productInfoName, status){
		if(rowId != null && rowId != 0 && rowId !=undefined){
			$('#selId').val(rowId);
		} else {
			$('#selId').val(0);
		}
		$('#popup1').dialog({
			title : 'Thông tin ngành hàng con phụ',
			onOpen: function(){
				$('#errMsgPop').html('').hide();
				disableSelectbox('productInfoMapPop');
				disabled('productInfoCodePop');
				$('#productInfoMapPop').val(productInfoMap).change();
				$('#productInfoCodePop').val(productInfoCode);
				$('#productInfoNamePop').val(productInfoName);
				$('#productInfoNamePop').focus();
			},
			onClose : function() {
				SubCategorySecondary.clearPop();
			}
		});
        $('#popup1').dialog('open');
		return false;
	},
	resetForm: function(){
		SubCategorySecondary.clearData();
		$("#grid").trigger("reloadGrid");
		return false;
	},
	
	clearData: function(){
		$('#selId').val(0);
		$('#errMsgPop').html('').hide();
		enableSelectbox('productInfoMapPop');
		enable('productInfoCodePop');
		$('#errMsg').html('').hide();
		$('#productInfoMap').val('').change();
		$('#productInfoCode').val('');
		$('#productInfoName').val('');
		$('#productInfoCode').focus();
		focusFirstTextbox();
	},
	clearPop: function(){
		$('#selId').val(0);
		$('#errMsgPop').html('').hide();
		enableSelectbox('productInfoMapPop');
		enable('productInfoCodePop');
		$('#errMsg').html('').hide();
		$('#productInfoCodePop').focus();
		$('#productInfoMapPop').val('').change();
		$('#productInfoCodePop').val('');
		$('#productInfoNamePop').val('');
	},
	getChangedForm: function(){
		$('.RequireStyle').show();
		$('#btnSearch').hide();
		$('#productCode').focus();
		focusFirstTextbox();
	}
};
/*
 * END OF FILE - /web/web/resources/scripts/business/catalog/vnm.sub-category-secondary.js
 */

/*
 * START OF FILE - /web/web/resources/scripts/business/catalog/vnm.unit-tree-catalog.js
 */
var UnitTreeCatalog = {
	_xhrSave : null,
	_listNodeTree:null,
	_lstStaffAdd:null,
	_lstStaffDelete:null,
	_typeScreen: 0,
	_zoom : 18,
	_nodeSelect:null,
	_callBack:null,
	_idSearch:null,
	_idSearchStaff:0,
	_idShopAddNewStaff:null,
	_isChange:false,
	_isLoadArea:true,
	_isBkLatLng :false,
	latbk : '',
	lngbk : '',
	_lstContextMenu: null,
	_contextMenuHTML : null,
	_MAX_LENGTH_6: 6,
	//Tungmt-------------------------------------------------------------------------------------------------
	getNodeFromListNode:function(node){
		if(node==null || node.attributes==null) return null;
		if(node.attributes.shop!=null){//shop
			return UnitTreeCatalog._listNodeTree.get("shop" +node.attributes.shop.id);
		}else if(node.attributes.staff!=null){//staff
			return UnitTreeCatalog._listNodeTree.get("staff" +node.attributes.staff.id);
		}else return null;
	},
	putMapListNodeTree:function(parent,data){
		for(var i=0;i<data.length;i++){
			var node=data[i];
			if(parent!=null && parent.attributes!=null)
				node.attributes.parent=parent.attributes;
			else node.attributes.parent=null;
			var nodeTemp = jQuery.extend(true, {}, node);
			nodeTemp.__proto__=null;
			if(nodeTemp.attributes!=null){
				if(nodeTemp.attributes.shop!=null){//shop
					UnitTreeCatalog._listNodeTree.put("shop" +nodeTemp.attributes.shop.id,nodeTemp);
				}else{//staff
					UnitTreeCatalog._listNodeTree.put("staff" +nodeTemp.attributes.staff.id,nodeTemp);
				}
			}
			if(nodeTemp.children!=null && nodeTemp.children.length>0){// nếu có con thì đệ quy put tiếp
				UnitTreeCatalog.putMapListNodeTree(nodeTemp,nodeTemp.children);
			}
		}
	},
	cbEditChange:function(){
		if($('#cbEdit').is(':checked')){
			$('#tree').tree('enableDnd');
		}else{
			$('#tree').tree('disableDnd');
			if (UnitTreeCatalog._typeScreen < 0) {
				UnitTreeCatalog._typeScreen = 0;
				UnitTreeCatalog.closeCollapseTab();
			}
		}
	},
	cbStopChange:function(){
		if($('#cbStop').is(':checked')){
			for(var i=0;i<UnitTreeCatalog._listNodeTree.keyArray.length;i++){
				var temp = UnitTreeCatalog._listNodeTree.get(UnitTreeCatalog._listNodeTree.keyArray[i]);
				if(temp!=null && temp.attributes!=null && temp.attributes.shop!=null && temp.attributes.shop.status!='RUNNING'){
					$($("#tree").tree('find',temp.attributes.shop.id).target).show();
				}
				if(temp!=null && temp.attributes!=null && temp.attributes.staff!=null && temp.attributes.staff.status!='RUNNING'){
					$($("#tree").tree('find','s' +temp.attributes.staff.id).target).show();
				}
			}
		}else{
			for(var i=0;i<UnitTreeCatalog._listNodeTree.keyArray.length;i++){
				var temp = UnitTreeCatalog._listNodeTree.get(UnitTreeCatalog._listNodeTree.keyArray[i]);
				if(temp!=null && temp.attributes!=null && temp.attributes.shop!=null && temp.attributes.shop.status!='RUNNING'){
					$($("#tree").tree('find',temp.attributes.shop.id).target).hide();
				}
				if(temp!=null && temp.attributes!=null && temp.attributes.staff!=null && temp.attributes.staff.status!='RUNNING'){
					$($("#tree").tree('find','s' +temp.attributes.staff.id).target).hide();
				}
			}
		}
	},
	hideAllContextMenu:function(){
		$('#cmChuyenDonVi').hide();
		$('#cmTamNgungNhanVien').hide();
	},
	/**
	 * push html context menu
	 * @author liemtpt
	 * @params list context menu: danh sach context menu theo role la SHOP hay STAFF 
	 * @params type: SHOP:1 , STAFF:2
	 * @description Tao context menu cho loai don vi 
	 * @createDate 26/01/2015
	 **/
	pushHtmlContextMenu:function(lstContextMenu,type){
		var html = new Array();
		html.push('<div class="menu-line" style="height: 100%;"></div>');
		/***type với 1:SHOP, 2:STAFF ***/
		if(type == 1){
			html.push('<span style="padding-left:25px;font-size: 13px;font-weight: bold;display:block;height:25px;margin-left: 5px">' + Utils.XSSEncode(catalog_unit_tao_don_vi_con) + '</span>');
		}else if(type == 2){
			html.push('<div class="menu-sep" id="cmLine" style="display: block;"></div>');
			html.push('<span style="padding-left:25px;font-size: 13px;font-weight: bold;display:block;height:25px; margin-left: 5px">' + Utils.XSSEncode(catalog_unit_tao_chuc_vu) + '</span>');
		}
		for(var i = 0,size = lstContextMenu.length  ; i < size ; i++){					
			html.push('<div class="menu-item" style="font-size: 12px;display:block;height:20px" '
					+ 'id="cm' +lstContextMenu[i].typeId + '"'  
					+ 'onclick="UnitTreeCatalog.addNewDataMenu(' +lstContextMenu[i].nodeType+',' +lstContextMenu[i].typeId+',' +lstContextMenu[i].id+');"> '							
					+ '<span style="padding-left:3px;"><img src="' +imgServerPath+lstContextMenu[i].iconUrl+'" width="15px" height="15px">' 
					+ '<span style="padding-left:25px;">' + Utils.XSSEncode(lstContextMenu[i].name) + '</span> '
					+ '</div>');
		}
		return html;
	},
	/**
	 * show context menu
	 * @author liemtpt
	 * @params type: SHOP:1 , STAFF:2
	 * @description Hien thi context menu len cay 
	 * @createDate 26/01/2015
	 **/
	showContextMenu:function(type){
		$('#group_tree_dbl_right_contextMenu').html('');
		var node = $('#tree').tree('getSelected');
		/***type với 1:đơn vị, 2:nhân viên***/
		if(type == nodeType.SHOP){//shop
			node=UnitTreeCatalog.getNodeFromListNode(node);
			// output: id(organization_id),code, name, nodeType, typeId,iconUrl
			var lstContextMenu = UnitTreeCatalog._lstContextMenu;
			if(lstContextMenu != null){
				var lstRoleStaff = new Array();
				var lstRoleShop = new Array();
				var htmlShop = new Array();
				var htmlStaff = new Array();
				for(var i = 0,size = lstContextMenu.length  ; i < size ; i++){					
					if(lstContextMenu[i].nodeType == nodeType.SHOP){
						lstRoleShop.push(lstContextMenu[i]);
					}else if(lstContextMenu[i].nodeType == nodeType.STAFF){
						lstRoleStaff.push(lstContextMenu[i]);
					}
				}
				if(lstRoleShop != null && lstRoleShop.length > 0){
					htmlShop = UnitTreeCatalog.pushHtmlContextMenu(lstRoleShop,nodeType.SHOP);
				}
				if(lstRoleStaff != null && lstRoleStaff.length > 0){
					htmlStaff = UnitTreeCatalog.pushHtmlContextMenu(lstRoleStaff,nodeType.STAFF);
				}
				
			}
			$('#group_tree_dbl_right_contextMenu').html(htmlShop.join("") + htmlStaff.join(""));			
			$(".menu-item").hover(function () {
				$(this).toggleClass("menu-active");
			 });
		}else if(type == nodeType.STAFF){//staff
			var html = new Array();
			html.push('<div class="menu-line" style="height: 100%;"></div>');
			html.push('<div id="cmChuyenDonVi" class="menu-item" style="font-size: 12px;display:block;height:20px" onclick="UnitTreeCatalog.openPoppupMovedStaff();"><span style="padding-left:3px;"><img src="'
					+imgServerPath+'/iconOrganization/male-user-remove.png" width="15px" height="15px"><span style="padding-left:20px;">' + Utils.XSSEncode(catalog_unit_tree_chuyen_don_vi) + '</div>');
			html.push('<div id="cmTamNgungNhanVien" class="menu-item" style="font-size: 12px;display:block;height:20px" onclick="UnitTreeCatalog.suspendedStaff();"><span style="padding-left:3px;"><img src="'
					+imgServerPath+'/iconOrganization/home-go.png" width="15px" height="15px"><span style="padding-left:20px;">' + Utils.XSSEncode(catalog_unit_tree_tam_ngung_doi_tuong_nay) + '</div>');
			$('#group_tree_dbl_right_contextMenu').html(html.join(""));
			$(".menu-item").hover(function () {
				$(this).toggleClass("menu-active");
			 });
		} else {
			return false;
		}
		Utils.functionAccessFillControl ('idDivTreeShop', function() {
			//Xu ly cac su kien lien quan den control phan quyen 
		});
		return true;
	},
	addNewDataMenu: function(nodeType,nodeTypeId,orgId){
		// xu ly them tren cay
		//alert('shop haha');
		var node = $('#tree').tree('getSelected'); // luong luon la don vi
		if(nodeType == 1){
			// them shop tren cay phuocDH2
			//UnitTreeCatalog.showCollapseTab(2,node.attributes.shop.id,node.attributes.shop.parentShop);
			UnitTreeCatalog.openCollapseTab();
			if(node.attributes.shop != null && node.attributes.shop!= undefined){
				UnitTreeCatalog.showShopInfoScreen(node.attributes.shop.id,nodeTypeId,nodeTypeId,orgId);
			} else {
				UnitTreeCatalog.showShopInfoScreen(node.attributes.shop,nodeTypeId,nodeTypeId,orgId);
			}
		} else if(nodeType == 2){
			// them staff tren cay VUONGMQ
			var shopId = '';
			var nameShop = '';
			if(node != null){
				shopId = node.id;
				nameShop = Utils.XSSEncode(node.attributes.shop.shopName);
			}
			UnitTreeCatalog.openCollapseTab();
			UnitTreeCatalog.showStaffDetailScreen(nodeType,nodeTypeId, orgId, nameShop, shopId);
		}
		$('#group_tree_dbl_right_contextMenu').hide();
	},
	/**
	 * filter context menu
	 * @author liemtpt
	 * @params node voi nodeType la SHOP:1 hay STAFF:2 
	 * @description hien thi context menu tuong ung voi tung loai node 
	 * @createDate 26/01/2015
	 **/
	filterContextMenu:function(node){ // show context menu tuong ung voi tung loai node
		$('#group_tree_dbl_right_contextMenu').html('');
		if(node.attributes==null){
			return false;
		}
		UnitTreeCatalog.hideAllContextMenu();
		if(node.attributes.shop!=null){//shop
			if(node.attributes.shop.organizationId == null){
				return false;
			}
			var params = new Object();
			params.orgId = node.attributes.shop.organizationId;
			Utils.getJSONDataByAjaxNotOverlay(params, "/catalog/unit-tree/get-list-context-menu-by-org-id", function(data) {
				if(data.listMenuContext!=null){
					UnitTreeCatalog._lstContextMenu = data.listMenuContext;
					UnitTreeCatalog.showContextMenu(nodeType.SHOP);
				}
				Utils.functionAccessFillControl ('idDivTreeShop', function() {
					//Xu ly cac su kien lien quan den control phan quyen 
				});
			});
			return true;
		}else if(node.attributes.staff!=null){//staff
			return UnitTreeCatalog.showContextMenu(nodeType.STAFF);
		}else{
			return false;
		}
	},
	/**
	 * suspended staff
	 * @author liemtpt
	 * @description tam ngung nhan vien 
	 * @createDate 02/03/2015
	 **/
	suspendedStaff:function(){
		var node = $('#tree').tree('getSelected');
		var params = new Object();
		var staffId = node.attributes.staff.id;
		params.id = staffId;
		//Gui yeu cau len server de luu du lieu
		var url = "/catalog/unit-tree/suspended-staff";
		params[JSONUtil._VAR_NAME] = 'staffTreeVO';
		var dataModel = JSONUtil.getSimpleObject(params);
		Utils.addOrSaveData(dataModel,url,null, 'errMsg', function(data){
			var tm = setTimeout(function(){
				$('.easyui-dialog').dialog('close'); 
				//load lai cay
				UnitTreeCatalog.reloadUnitTree();
				clearTimeout(tm);
			}, 1500);
		},null,'#treeDiv',null,null,function(data){
			if(data.error != undefined  && data.error && data.errMsg != undefined){
				$('#errMsg').html(data.errMsg).show();
				setTimeout(function(){$('#errMsg').hide();},4000);
			}
		});
		$('#group_tree_dbl_right_contextMenu').hide();
		return false;
	},
	/**
	 * open attribute detail dynamic
	 * @author liemtpt
	 * @description: mo popup chuyen don vi
	 * @createDate 22/01/2015
	 */
	openPoppupMovedStaff : function() {
		$('#popupMovedStaff #successMsgPop').html('').hide();
		$('#popupMovedStaff #errMsg').html('').show();
		$('#popupMovedStaff').dialog({
			closed : false,
			cache : false,
			modal : true,
			width : 450,
			height : 210,
			onOpen: function(){	
				
			},
			onBeforeClose: function() {
								
			},
			onClose : function(){
				
			}
		});
		$('#group_tree_dbl_right_contextMenu').hide();
	},
	/**
	 * moved staff 
	 * @author liemtpt
	 * @description Chuyen don vị: chuyen nhan vien tu don vi nay sang don vi khac
	 * @createDate 02/03/2015
	 **/
	movedStaff:function(){
		var msg = '';
		var node = $('#tree').tree('getSelected');
		var choseShopId = $('#idShopCodeChose').combotree('getValue');
		var curStaff = node.attributes.staff;
		if(curStaff != null && curStaff != undefined){
			if(curStaff.shop != null && curStaff.shop.id >0){
				//kiem tra don vi dich co la don vi hien tai dang chon khong
				if(curStaff.shop.id == choseShopId){
					if (msg.length == 0) {
						msg = catalog_unit_tree_change_unit_don_vi_dich_la_don_vi_hien_tai;
					}
				}
				if (msg.length > 0) {
					$('#popupMovedStaff #errMsg').html(msg).show();
					return false;
				}
				var params = new Object();
				params.shopId = choseShopId;
				params.staffId = curStaff.id;
				Utils.getJSONDataByAjaxNotOverlay(params, "/catalog/unit-tree/check-chose-shop-exits-staff-type-id", function(data) {
					if(data.listExits != null && data.listExits.length > 0){
						params.shopId = choseShopId;
						var url = "/catalog/unit-tree/moved-staff";
						Utils.addOrSaveData(params,url,null, 'errMsg', function(data){
							$('#popupMovedStaff #successMsgPop').html(catalog_unit_tree_change_unit_phan_quyen).show();
							var tm = setTimeout(function(){
								$('.easyui-dialog').dialog('close');
								//load lai cay
								UnitTreeCatalog.reloadUnitTree();
								clearTimeout(tm);
							}, 3000);
							
						},null,'#popupMovedStaff',null,null,function(data){
							if(data.error != undefined  && data.error && data.errMsg != undefined){
								$('#popupMovedStaff #errMsg').html(data.errMsg).show();
							}
						});
					}else{
						if (msg.length == 0) {
							msg = catalog_unit_tree_chang_unit_don_vi_dich_khong_cho_tao;
						}
						if (msg.length > 0) {
							$('#popupMovedStaff #errMsg').html(msg).show();
							return false;
						}
					}
				});
				
			}
		}
		return false;
	},
	checkDropEnter:function(node,target){//kiem tra có cho thả xuống hay ko
		if(node.attributes==undefined || node.attributes==null || target.attributes==undefined || target.attributes==null){
			return false;
		}
		if(target.attributes.staff!=null){// chỉ cho thả vào shop
			return false;
		}
		if(node.attributes.staff.shop.id==target.attributes.shop.id)//nếu cùng shopid với shop muốn thả thì ko cho
			return false;
		if(node.attributes.staff.staffType.objectType==StaffRoleType.NVGS){//gsnpp
			if(target.attributes.shop.type.objectType!=3)//shop  ko phải npp thì ko cho
				return false;
		}else if(node.attributes.staff.staffType.objectType==StaffRoleType.TBHM){//gdm
			if(target.attributes.shop.type.objectType!=1)//gdm chỉ dc chuyển qua shop có shop là mien
				return false;
		}else if(node.attributes.staff.staffType.objectType==StaffRoleType.TBHV){//tbhv
			if(target.attributes.shop.type.objectType!=2)//tbhv chỉ dc chuyển qua nhóm có shop là vùng
				return false;
		}else if(node.attributes.staff.staffType.objectType==8){//vnm
			return false;
		}else{//nvbh
			if(target.attributes.shop.type.objectType!=3)//nvbh chỉ dc chuyển qua nhóm có shop là npp
				return false;
		}
		return true;
	},
	fillMapStaffForDND:function(target,nodeSource){
		var source = new Object();
		var newNode = new Object();
		source = jQuery.extend(true, {}, nodeSource);
		source.attributes.staff.shop=jQuery.extend(true, {}, target.attributes.shop);
		newNode = jQuery.extend(true, {}, source);
		newNode.attributes.parent=jQuery.extend(true, {}, target.attributes);
		UnitTreeCatalog._lstStaffAdd.put("staff" +source.attributes.staff.id,source);
		UnitTreeCatalog._listNodeTree.put("staff" +newNode.attributes.staff.id,newNode);
		return newNode;
	},
	checkChangeOnTree:function(){
		if(UnitTreeCatalog._lstStaffAdd.keyArray.length>0 ||
				UnitTreeCatalog._lstStaffDelete.keyArray.length>0){
			$('#btnSave').removeAttr('disabled');
			$('#btnCancel').removeAttr('disabled');
			$('#btnSave').removeClass('BtnGeneralDStyle');
			$('#btnCancel').removeClass('BtnGeneralDStyle');
			return true;
		}else{
			$('#btnSave').attr('disabled','disabled');
			$('#btnCancel').attr('disabled','disabled');
			$('#btnSave').addClass('BtnGeneralDStyle');
			$('#btnCancel').addClass('BtnGeneralDStyle');
			return false;
		}
	},
	checkChangeTree:function(callBack,noCall){//nếu noCall=1 thì không cho callback khi bấm hủy
		if(UnitTreeCatalog.checkChangeOnTree()==true){
			$.messager.confirm('Xác nhận', 'Bạn có muốn lưu các thay đổi.', function(r){
				if (r){
					if(callBack!=undefined && callBack!=null){
						UnitTreeCatalog._callBack=callBack;
					}
					UnitTreeCatalog.saveTree();
				}else if(noCall==undefined || noCall==null){//hủy
					UnitTreeCatalog.reloadUnitTree();
					if(callBack!=undefined && callBack!=null){
						callBack.call(this);
					}
				}
			});
		}else{
			if(callBack!=undefined && callBack!=null){
				callBack.call(this);
			}
		}
	},
	saveTree:function(confirm){
		var lstStaffAddId=new Array();//danh sach nhan vien them vao group moi
		var lstStaffShopAddId=new Array();//danh sach nhan vien xoa khoi nhom
		var flagUpdate=false;
		for(var i=0;i<UnitTreeCatalog._lstStaffAdd.keyArray.length;i++){
			var temp = UnitTreeCatalog._lstStaffAdd.get(UnitTreeCatalog._lstStaffAdd.keyArray[i]);
			if(temp!=null && temp.attributes.staff!=null){
				lstStaffAddId.push(temp.attributes.staff.id);
				lstStaffShopAddId.push(temp.attributes.staff.shop.id);
				flagUpdate=true;
			}
		}
		var param = new Object();
		param.lstStaffShopAddId=lstStaffShopAddId;
		param.lstStaffAddId=lstStaffAddId;
		if(!flagUpdate){//không có thay đổi
			$('#successMsg').html('Bạn chưa có thay đổi nào').show();
			setTimeout(function(){$('#successMsg').html('').hide();},4000);
			return;
		}
		if(confirm!=undefined && confirm!=null && confirm==1){
			Utils.addOrSaveData(param, '/catalog/unit-tree/save-tree', null, 'errMsg', function(result){
				UnitTreeCatalog.saveTreeSuccess(result);
			});
		}else{
			Utils.saveData(param, '/catalog/unit-tree/save-tree', null, 'errMsg', function(result){
				UnitTreeCatalog.saveTreeSuccess(result);
			});
		}
	},
	saveTreeSuccess:function(result){
		if(!result.error){
			$('#successMsg').html('Lưu dữ liệu thành công').show();
			setTimeout(function(){$('#successMsg').html('').hide();},4000);
			UnitTreeCatalog._lstStaffAdd=new Map();
			UnitTreeCatalog._lstStaffDelete=new Map();
			UnitTreeCatalog.checkChangeOnTree();//disabled 2 button lưu và hủy
			if(UnitTreeCatalog._callBack!=null){
				UnitTreeCatalog._callBack.call(this);
			} else {
				UnitTreeCatalog.reloadUnitTree();
			}
		}
	},
	reloadUnitTree:function(){
		if(UnitTreeCatalog._idShopAddNewStaff!=null){
			UnitTreeCatalog.loadUnitTree(UnitTreeCatalog._idShopAddNewStaff);
		}else{
			var node = $('#tree').tree('getSelected');
			if(node!=null && node.attributes!=null){
				var attr=node.attributes;
				if(attr.shop!=null) UnitTreeCatalog.loadUnitTree(attr.shop.id);
				else if(attr.staff!=null) UnitTreeCatalog.loadUnitTree(attr.staff.shop.id);
				else UnitTreeCatalog.loadUnitTree();
			}else{
				UnitTreeCatalog.loadUnitTree();
			}
		}
	},
	cancelOrganization:function(){
		UnitTreeCatalog.openCollapseTab();
		UnitTreeCatalog.showCollapseTab(1);
		
	}
	,
	cancelAction:function(){
		$.messager.confirm('Xác nhận', 'Bạn có muốn hủy các thao tác đã làm.', function(r){
			if (r){
				UnitTreeCatalog.loadUnitTree();
			}
		});
	},
	loadUnitTree:function(idSearch,idSearchStaff){
		UnitTreeCatalog._listNodeTree = new Map();
		UnitTreeCatalog._lstStaffAdd = new Map();
		UnitTreeCatalog._lstStaffDelete = new Map();
		UnitTreeCatalog._idShopAddNewStaff = null;
		UnitTreeCatalog.checkChangeOnTree();//disabled 2 button lưu và hủy
		if (idSearch != undefined && idSearch != null && idSearch != 0) {
			UnitTreeCatalog._idSearch = idSearch;
			if (idSearchStaff != undefined && idSearchStaff != null
					&& idSearchStaff != 0)
				UnitTreeCatalog._idSearchStaff = idSearchStaff;
		}
		$('#tree').tree({  
			url:'/catalog/unit-tree/get-list-unit-tree' ,
			dnd:true,
			lines:true,
			onBeforeLoad: function(n,p){
				if(UnitTreeCatalog._idSearch!=null && UnitTreeCatalog._idSearch!=0){
					p.idSearch=UnitTreeCatalog._idSearch;
					if (idSearchStaff && Number(idSearchStaff)) {
						p.staffId = idSearchStaff;
					}
					$('#divOverlay').show();
				}
			},
			 formatter: function(node){
					var tmpText = Utils.XSSEncode(node.text);
					if (node.attributes.shop != null && node.attributes.shop.status == 'RUNNING'){
						if(node.iconCls != null){
							return ('<img src="' +node.iconCls+'" width="15px" height="15px"> ' + Utils.XSSEncode(tmpText) + '<span style="color: dodgerblue;">&nbsp;</span>');
						}else{
							return (tmpText + '<span style="color: dodgerblue;">&nbsp;</span>');
						}
					}
					if (node.attributes.shop != null && node.attributes.shop.status == 'STOPPED'){
						if(node.iconCls != null){
							return ('<img src="' +node.iconCls+'" width="15px" height="15px"> ' + Utils.XSSEncode(tmpText) + '<span style="color: red;">&nbsp;(I)</span>');
						}else{
							return (tmpText + '<span style="color: red;">&nbsp;(I)</span>');
						}
					}
					if (node.attributes.staff != null && node.attributes.staff.status == 'RUNNING'){
						if(node.iconCls != null){
							return ('<img src="' +node.iconCls+'" width="15px" height="15px"> ' + tmpText + '<span style="color: dodgerblue;">&nbsp;</span>');
						}else{
							return (tmpText + '<span style="color: dodgerblue;">&nbsp;</span>');
						}
					}
					if (node.attributes.staff != null && node.attributes.staff.status == 'STOPPED'){
						if(node.iconCls != null){
							return ('<img src="' +node.iconCls+'" width="15px" height="15px"> ' + tmpText + '<span style="color: red;">&nbsp;(I)</span>');
						}else{
							return (tmpText + '<span style="color: red;">&nbsp;(I)</span>');
						}
					}
					if (node.iconCls != null) {
						return '<img src="' +node.iconCls+'" width="15px" height="15px"> ' + Utils.XSSEncode(node.text);
					} else {
						return Utils.XSSEncode(node.text);
					}
			 },
			 onLoadSuccess : function(parent, data) {
				if (UnitTreeCatalog._idSearch != null) {
					try {
						var id=UnitTreeCatalog._idSearch;
						if(UnitTreeCatalog._idSearchStaff!=undefined && UnitTreeCatalog._idSearchStaff!=null && UnitTreeCatalog._idSearchStaff!=0){
							id='s' +UnitTreeCatalog._idSearchStaff;
						}
						var node = $('#tree').tree('find', id);
						if(node!=null){
							$('#tree').tree('select', node.target);
						}
					}catch(e){}
					UnitTreeCatalog._idSearch=null;
					UnitTreeCatalog._idSearchStaff=0;
					$('#divOverlay').hide();
				}
				UnitTreeCatalog.putMapListNodeTree(parent,data);
				UnitTreeCatalog.cbStopChange();
				UnitTreeCatalog.resize();
				//Xu ly phan quyen ctrl
				Utils.functionAccessFillControl ('idDivTreeShop', function() {
					//Xu ly cac su kien lien quan den control phan quyen 
				});
			},
			onContextMenu:function(e, node){
				e.preventDefault();
				$('#tree').tree('select', node.target);
				if(UnitTreeCatalog.filterContextMenu(node)){
					$('#group_tree_dbl_right_contextMenu').addClass('easyui-menu');
					$('#group_tree_dbl_right_contextMenu').menu('show', {left: e.pageX,top: e.pageY});
				}
				$('#group_tree_dbl_right_contextMenu').css({top: e.pageY});
			},
			onCollapse:function(node){
				UnitTreeCatalog.resize();
			},
			onExpand :function(node) {
				UnitTreeCatalog.resize();
			},
			onClick:function(node){
				if($('#searchStaffUnitTreeGrid').length==1 && node!=null && node.attributes!=null && node.attributes.shop!=null){
					UnitTreeCatalog.search(0);
				}
			},
			onDblClick:function(node){
				//if($('#cbEdit').is(':checked')){
					if(node!=null && node.attributes!=null){
						if(node.attributes.shop!=null && node.attributes.shop.id > 0){
							UnitTreeCatalog.openCollapseTab();
							if (node.attributes.shop.parentShop != null && node.attributes.shop.parentShop !=undefined ) {
								UnitTreeCatalog.showShopInfoScreen(node.attributes.shop.parentShop.id,node.attributes.shop.id);
							}else {
								UnitTreeCatalog.showShopInfoScreen(node.attributes.shop.parentShop,node.attributes.shop.id);
							}	
						} else if(node.attributes.staff!= null) {
							UnitTreeCatalog.openCollapseTab();
							UnitTreeCatalog.showStaffEditScreen(node.attributes.staff.id,node.attributes.staff.shop.id);
						}
					}
				//}
			},
			onBeforeDrag:function(node){
				if(!$('#cbEdit').is(':checked')) return false;
				if(node.attributes==null || node.attributes.staff==null)
					return false;
				return true;
			},
			onBeforeDrop:function(target,source,point){
				if(point!=undefined && point != "append") {
					return false;
				}
				var nodeTarget=$('#tree').tree('getNode', target);
				if(nodeTarget==null) {
					return false;
				}
				return UnitTreeCatalog.doDrop(nodeTarget, source);
			}
		});
	},
	doDrop: function(nodeTarget, source) {
		if(!UnitTreeCatalog.checkDropEnter(source,nodeTarget)) {
			return false;
		}
		UnitTreeCatalog.fillMapStaffForDND(nodeTarget,source);
		UnitTreeCatalog.checkChangeOnTree();//disabled 2 button lưu và hủy
		UnitTreeCatalog._callBack = function() {
			UnitTreeCatalog._callBack = null;
			var st = source.attributes.staff;
			UnitTreeCatalog.loadUnitTree(st.shop.id, st.id);
		};
		UnitTreeCatalog.saveTree(); // luu truc tiep
		return true;
	},
	checkStaffOnTree:function(staffId){
		for(var i=0;i<UnitTreeCatalog._listNodeTree.keyArray.length;i++){
			var temp = UnitTreeCatalog._listNodeTree.get(UnitTreeCatalog._listNodeTree.keyArray[i]);
			if(temp!=null && temp.attributes!=null && temp.attributes.staff!=null && temp.attributes.staff.id==staffId){
					return true;
			}
		}
		return false;
	},
	addStaffForTree:function(staff,target){
		var iconCls="tricon6";
		var attributes = new Object();
		attributes.shop=null;
		attributes.staff=staff;
		attributes.parent=target.attributes;
		var strAlias= Utils.XSSEncode(attributes.staff.staffCode+' - ' +attributes.staff.staffName);
		if(attributes.staff.staffType.objectType==StaffRoleType.NVGS){
			iconCls="tricon4";
		}else if(attributes.staff.staffType.objectType==StaffRoleType.TBHM){
			strAlias=attributes.staff.staffName;
			iconCls="tricon2";
		}else if(attributes.staff.staffType.objectType==StaffRoleType.TBHV){
			strAlias=attributes.staff.staffName;
			iconCls="tricon7";
		}
		strAlias = Utils.XSSEncode(strAlias);
		var node = new Object();
		node.text= strAlias;
		node.id=attributes.staff.id;
		node.attributes=attribute;
		node.state='close';
		var nodeTemp = jQuery.extend(true, {}, node);
		var nodeTemp1 = jQuery.extend(true, {}, node);
		UnitTreeCatalog._listNodeTree.put("staff" +nodeTemp.attributes.staff.id,nodeTemp);
		UnitTreeCatalog._lstStaffAdd.put("staff" +nodeTemp1.attributes.staff.id,nodeTemp1);
		$('#tree').tree('append', {  
			parent: (target?target.target:null),  
			data: [{  
				text: Utils.XSSEncode(nodeTemp.text),
				id: nodeTemp.id,
				attributes: nodeTemp.attributes,
				state: nodeTemp.state,
				iconCls:iconCls
			}]  
		});
		UnitTreeCatalog.checkChangeOnTree();//disabled 2 button lưu và hủy
		UnitTreeCatalog._callBack=null;
	},
	addStaff: function(type){
		var nodeSelect = $('#tree').tree('getSelected');
		if(nodeSelect==null || nodeSelect.attributes.staff!=null) return false;
		var shopId=nodeSelect.attributes.shop.id;
		var arrParam = new Array();
		var param = new Object();
		param.name = 'shopId';
		param.value = shopId;
		arrParam.push(param);
		var param1 = new Object();
		param1.name = 'objectType';
		param1.value = type;
		arrParam.push(param1);
		CommonSearchEasyUI.searchNVUnitTreeDialog(function(data){
			if(data!=null && data.length>0){
				for(var i=0;i<data.length;i++){
					var staff=new Object();
					staff.attributes={staff:{id:data[i].id,shop:{}}};
					staff.attributes.staff.shop.id=shopId;
					UnitTreeCatalog._lstStaffAdd.put("staff" +data[i].id,staff);
				}
				UnitTreeCatalog.saveTree(); // luu truc tiep
			}
		}, arrParam);
	},
	addNewStaff:function(type,typeCheck){
		UnitTreeCatalog._nodeSelect=$('#tree').tree('getSelected');
		var node = UnitTreeCatalog._nodeSelect;
		if(node.attributes!=null && node.attributes.shop!=null){
			UnitTreeCatalog._idShopAddNewStaff=node.attributes.shop.id;
			UnitTreeCatalog.checkChangeTree(function(){
				var shopCode=Utils.XSSEncode(node.attributes.shop.shopCode);
				var shopName=Utils.XSSEncode(node.attributes.shop.shopName);
				UnitTreeCatalog.showCollapseTab(-3,null,null,shopCode,shopName,type);
			},1);
		}
	},
	addNewShop:function(){
		var node=$('#tree').tree('getSelected');
		if(node.attributes!=null && node.attributes.shop!=null){
			UnitTreeCatalog.checkChangeTree(function(){
				UnitTreeCatalog.showCollapseTab(-2,null,node.attributes.shop.id);
			});
		}
	},
	updateShop:function(){
		var node=$('#tree').tree('getSelected');
		if(node.attributes!=null && node.attributes.shop!=null){//shop
			UnitTreeCatalog.checkChangeTree(function(){
				UnitTreeCatalog.showCollapseTab(-2,node.attributes.shop.id,null);
			});
		}
	},
	//-------------------------------------------------------------------------------------------------------
	resize: function() {
		var hSidebar3=$('#tree').height();
		var hSidebar1=$('.Content3Section').height();
		
		if(hSidebar3> 650) {
			$('.ReportTree2Section').css('height', 'auto');	
		} else {
			if(hSidebar3 <  hSidebar1) {
				if(hSidebar1 < 700) {
					$('.ReportTree2Section').height(hSidebar1-60);
				} else {
					$('.ReportTree2Section').height(650);
				}
			} else {
				if(hSidebar3 < 300) {
					$('.ReportTree2Section').height(300);
				} else {
					$('.ReportTree2Section').css('height', 'auto');	
				}
			}
		}
		var hSidebar2=$('.Sidebar1Section').height();
		if( hSidebar1 > hSidebar2 ) {
			$('.Content1Section').height(hSidebar1);
		}
		else $('.Content1Section').height(hSidebar2);
		return ;
	},
	openCollapseTab: function() {
		$('#collapseTab').addClass('Item1Close');
		$('#collapseTab').removeClass('Item1');
		$('.Sidebar1Section').css('width', '24.9%');
		$('#unitTreeContent').show();
	},
	closeCollapseTab: function(){
		$('#collapseTab').addClass('Item1');
		$('#collapseTab').removeClass('Item1Close');
		$('.Sidebar1Section').css('width', '95.8%');
		$('#unitTreeContent').hide();
	},
	removeActiveCollapseTab: function(){
		$('#collapseTab').removeClass('Active');
		$('#collapseTab1').removeClass('Active');
		$('#collapseTab2').removeClass('Active');
		/*if(!$('#collapseTab2').hasClass('Active')){
			$('#btnSave').addClass('BtnGeneralDStyle');
			$('#btnSave').attr('disabled', 'disabled');
		}*/
		$('#collapseTab3').removeClass('Active');
	},
	showCollapseTab: function(type,shopId,parentShopId,shopStaffCode, shopStaffName,staffType,staffId){
		if(UnitTreeCatalog._isChange){
			//VUONGBD MULTILANGUAGE DIALOG
			$.messager.confirm(jsp_common_xacnhan,catalog_unit_view_thong_tin_nhan_vien_back, function(r){
				if (r){
					UnitTreeCatalog._isChange=false;
					//UnitTreeCatalog.openCollapseTab();
					UnitTreeCatalog.showSearchScreen();
					//UnitTreeCatalog.showCollapseTab(type,shopId,parentShopId,shopStaffCode, shopStaffName,staffType,staffId);
			}});
			return false;
		}
		UnitTreeCatalog.removeActiveCollapseTab();
		if(type>0) {
			UnitTreeCatalog.checkChangeTree(function(){
				var checkLoad = false;
				if($('#collapseTab').hasClass('Item1')){
					UnitTreeCatalog.openCollapseTab();
					checkLoad = true;
				}
				else if($('#collapseTab').hasClass('Item1Close')){
					if(type == UnitTreeCatalog._typeScreen) {
						//UnitTreeCatalog.closeCollapseTab();
						checkLoad = true;
					} else {
						checkLoad = true;
					}
				}
				if(type == 1) {
					$('#collapseTab1').addClass('Active');
					if(checkLoad) UnitTreeCatalog.showSearchScreen();
				} else if(type ==2 ) {
					$('#collapseTab2').addClass('Active');
					/*$('#btnSave').removeClass('BtnGeneralDStyle');
					$('#btnSave').removeAttr('disabled');*/
					//p if(checkLoad) UnitTreeCatalog.showShopInfoScreen();
					UnitTreeCatalog.showShopInfoScreen(parentShopId,shopId);
				} else if(type ==3) {
					$('#collapseTab3').addClass('Active');
					if(checkLoad) UnitTreeCatalog.showStaffSearchScreen();
				}
				if(type != 0 ) UnitTreeCatalog._typeScreen = type ;
				UnitTreeCatalog.resize();
				// 1man hinh search
				// 2man hinh them moi hoac cap nhat don vi
				// 3man hinh them moi hoac cap nhat nhan vien
			});
		} else {
			// 0 (Open and Close binh thuong )
			// -1(Close va load lai cay theo shopId)
			// -2(Them moi hoac cap nhat shop)
			// -3(Man hinh cap nhat nhan vien)
			if(type ==0) {
				if($('#collapseTab').hasClass('Item1')){
					UnitTreeCatalog.openCollapseTab();
					$('#collapseTab').addClass('Active');
					if(UnitTreeCatalog._typeScreen == 0) {
						UnitTreeCatalog.showSearchScreen();
						UnitTreeCatalog._typeScreen = 4;
					}
				}
				else if($('#collapseTab').hasClass('Item1Close')){
					UnitTreeCatalog.closeCollapseTab();
				}
			}else if(type == -1) {
				UnitTreeCatalog.closeCollapseTab();
				if( shopId != undefined && shopId != null) {
					UnitTreeCatalog.loadUnitTree(shopId,staffId);
				}
				UnitTreeCatalog._typeScreen = type ;
			}else if(type == -2) {
				UnitTreeCatalog.openCollapseTab();
				UnitTreeCatalog.showShopInfoScreen(parentShopId,shopId);
			}else if(type == -3) {
				UnitTreeCatalog.openCollapseTab();
				UnitTreeCatalog.showStaffCreateScreen(staffType,shopStaffCode,shopStaffName);
			}
			if(type != 0 ) UnitTreeCatalog._typeScreen = type ;
			UnitTreeCatalog.resize();
		}
		return false;
	},
	showSearchScreen: function(tabId){
		var params = new Object();
		Utils.getHtmlDataByAjax(params, '/catalog/unit-tree/show-search',function(data) {
			$("#unitTreeContent").html(data).show();
			if (tabId) {
				$("#tabId").click();
			}
		}, null, 'POST');
		return false;
	},
	//ThongNM-------------------------------------------------------------------------------------------------
	
	//Search UnitTree
	hideAllTab: function(){
		$('#tabActive1').removeClass('Active');
		$('#tabActive2').removeClass('Active');
		$('#container1').hide();
		$('#container2').hide();
	},
	showTab1: function(){
		$( "#importFrm" ).prop( "disabled", false );
		UnitTreeCatalog.hideAllTab();
		$('#tabActive1').addClass('Active');
		$('#container1').show();
		Utils.bindAutoSearchEx('.SProduct1Form');
		$('#errExcelMsg').html('').hide();
	},
	showTab2: function(){
		$( "#importShopFrm" ).prop( "disabled", false );
		UnitTreeCatalog.hideAllTab();
		$('#tabActive2').addClass('Active');
		$('#container2').show();
		$('#searchShopUnitTreeGrid').show();
		$('#staffCode').val('');
		$('#staffName').val('');
		Utils.bindAutoSearchEx('.SProduct1Form');
		$('#errExcelMsg').html('').hide();
	},
	searchShop: function() {
		$('#errMsgSearchShop').html('').hide();
		var rawSearchParam = {
			unitCode: $('#shopCode').val(),
			unitName: $('#shopName').val(),
			unitType: $('#unitType').val(),
			status: $('#shopStatus').val(),
			searchObjectType: 'SHOP'
		}
		
		try {
			var selectedNodeId = $('#manageShop').combotree('getValue');
			var tree = $('#manageShop').combotree('tree');
			var node = $(tree).tree('find', selectedNodeId);
			rawSearchParam.manageUnitCode = node.attributes.shop.shopCode;
		} catch (e) {
			// pass through
		}

		if (rawSearchParam.unitType === "-1") {
			delete rawSearchParam.unitType;
		}
		if (rawSearchParam.status === 'ALL') {
			delete rawSearchParam.status;
		}

		var searchParamPrefix = 'unitFilter';
		var searchParam = {};
		convertToSimpleObject(searchParam, rawSearchParam, searchParamPrefix);
		$('#searchShopUnitTreeGrid').datagrid('load', searchParam);
		$('#shopCode').focus();
	},
	/**
	 * export don vi de xuat ra file excel : gom tim kiem va export
	 * @author phuocdh2
	 * @return void
	 * @since  24-Mar-2015
	 */
	exportShop: function() {
		$('#errMsg').html('').hide();
		var rawSearchParam = {
			unitCode: $('#shopCode').val(),
			unitName: $('#shopName').val(),
			unitType: $('#unitType').val(),
			status: $('#shopStatus').val(),
			searchObjectType: 'SHOP'
		}
		
		try {
			var selectedNodeId = $('#manageShop').combotree('getValue');
			var tree = $('#manageShop').combotree('tree');
			var node = $(tree).tree('find', selectedNodeId);
			rawSearchParam.manageUnitCode = node.attributes.shop.shopCode;
		} catch (e) {
			// pass through
		}

		if (rawSearchParam.unitType === "-1") {
			delete rawSearchParam.unitType;
		}
		if (rawSearchParam.status === 'ALL') {
			delete rawSearchParam.status;
		}

		var searchParamPrefix = 'unitFilter';
		var searchParam = {};
		convertToSimpleObject(searchParam, rawSearchParam, searchParamPrefix);
		var url = "/catalog/unit-tree/export-shop-excel";
		ReportUtils.exportReport(url, searchParam, 'errExcelMsg');
		return false;
	},
	/**
	 * tim kiem nhan vien o tab nhan vien
	 * @author tuannd20
	 * @return void
	 * @since  13/03/2015
	 */
	searchStaff: function() {
		$('#errMsgSearchShop').html('').hide();
		var rawSearchParam = {
			unitCode: $('#staffCode').val(),
			unitName: $('#staffName').val(),
			unitType: $('#staffType').val(),
			status: $('#staffStatus').val(),
			searchObjectType: 'STAFF'
		}
		
		try {
			var selectedNodeId = $('#manageShopStaff').combotree('getValue');
			var tree = $('#manageShopStaff').combotree('tree');
			var node = $(tree).tree('find', selectedNodeId);
			rawSearchParam.manageUnitCode = node.attributes.shop.shopCode;
		} catch(e) {
			// pass through
		}

		if (rawSearchParam.unitType === "-1") {
			delete rawSearchParam.unitType;
		}
		if (rawSearchParam.status === 'ALL') {
			delete rawSearchParam.status;
		}

		var searchParamPrefix = 'unitFilter';
		var searchParam = {};
		convertToSimpleObject(searchParam, rawSearchParam, searchParamPrefix);
		$('#searchStaffUnitGrid').datagrid('load', searchParam);
		$('#staffCode').focus();
	},
	showShopInfoScreen: function(parentShopId,shopId,nodeTypeId,orgId){
		var params = new Object();
		if(parentShopId !=  undefined && parentShopId != null) {
			params.parentShopId = parentShopId;
		}
		if(shopId !=  undefined && shopId != null) {
			params.shId = shopId;
		} else {
			params.shId = '';
		}
		var node = $('#tree').tree('getSelected');//p+
		if (nodeTypeId == null) {
			var shopType= '';
			if (node.attributes.shop.type != undefined && node.attributes.shop.type != null) {
				 shopType = node.attributes.shop.type.id;
			}	
			if (shopType != undefined && shopType != null) {
				params.shopType = shopType;
			}//p+
		} else {
			params.shopType = nodeTypeId;
		}
		if(orgId !=  undefined && orgId != null) {
			//params.shopId = shopId;
			params.organizationId = orgId;
		} else {
			params.organizationId = '';
		}
		Utils.getHtmlDataByAjax(params, '/catalog/unit-tree/show-shopinfo',function(data) {
			$("#unitTreeContent").html(data).show();
			UnitTreeCatalog.resize();
		}, null, 'POST');
		return false;
	},
	updateShopInfoScreenWhenLoad: function(isUpdate,allowSelectParentShop,shopTypeOfShop,statusOfShop,areaIdOfShop,shopTypeOfParentShop){
		isUpdate = (isUpdate =='true');
		
		allowSelectParentShop = (allowSelectParentShop =='true');
		
		if(shopTypeOfShop == undefined || shopTypeOfShop == null || shopTypeOfShop=='') shopTypeOfShop = -1;
		else shopTypeOfShop = Number(shopTypeOfShop);
		
		if(statusOfShop == undefined || statusOfShop == null || statusOfShop=='') statusOfShop = 1;
		
		if(areaIdOfShop == undefined || areaIdOfShop == null || areaIdOfShop=='') areaIdOfShop = null;
		else areaIdOfShop = Number(areaIdOfShop);
		
		if(shopTypeOfParentShop == undefined || shopTypeOfParentShop == null || shopTypeOfParentShop=='') shopTypeOfParentShop = -1;
		else shopTypeOfParentShop = Number(shopTypeOfParentShop);
		
		$('.RequireStypeParentShopCode').hide();
		var isDisableAreaTree = false;
		if(isUpdate) {
			$('.RequireStypeShopCode').hide();
			disabled('shopCode');
			setSelectBoxValue('status',statusOfShop);
			if(allowSelectParentShop) {
				$('.RequireStypeParentShopCode').show();
				UnitTreeCatalog.loadComboTreeShop('parentShopTree', 'parentShopCode', $("#parentShopIdHd").val().trim(), function(data) {
					if(data.id> 0) {
						var type = data.type.objectType;
						if (ShopObjectType.NPP == shopTypeOfShop && ShopObjectType.VUNG != type) {
							Alert('Thông báo','Mã đơn vị cha phải là cấp Vùng',function(){
								$("#parentShopTree").combotree("setValue", $("#parentShopIdHd").val().trim());
								$('#parentShopName').val($("#parentShopNameHd").val().trim());
							});
						} else  if (ShopObjectType.VUNG == shopTypeOfShop && ShopObjectType.MIEN != type) {
							Alert('Thông báo','Mã đơn vị cha phải là cấp Miền',function(){
								$("#parentShopTree").combotree("setValue", $("#parentShopIdHd").val().trim());
								$('#parentShopName').val($("#parentShopNameHd").val().trim());
							});
						} else {
							setSelectBoxValue('shopType',type+1);
							$('#parentShopName').val(Utils.XSSEncode(data.shopName));
						}
					} else {
						if (ShopObjectType.NPP == shopTypeOfShop) {
							Alert('Thông báo','Mã đơn vị cha phải là cấp Vùng');
						} else  if (ShopObjectType.VUNG == shopTypeOfShop) {
							Alert('Thông báo','Mã đơn vị cha phải là cấp Miền');
						}
						$("#parentShopTree").combotree("setValue", $("#parentShopIdHd").val().trim());
						$('#parentShopName').val($("#parentShopNameHd").val().trim());
					}
					UnitTreeCatalog._isChange=true;
				}, null, true);
			}
			
		} else {
			
			if(allowSelectParentShop) {
				$('.RequireStypeParentShopCode').show();
				TreeUtils.loadComboTreeShopHasTitle('parentShopTree', 'parentShopCode',function(data) {
					if(data.id> 0) {
						var type = data.type.objectType;
						if(type == ShopObjectType.NPP || type == ShopObjectType.NPP_KA || type == ShopObjectType.MIEN_ST || type == 14) {
							Alert('Thông báo','Mã đơn vị cha phải là cấp GT(KA, ST), Miền, Vùng',function(){
								$('#parentShopTree').combotree('setValue',activeType.ALL);
								setSelectBoxValue('shopType',-1);
							});
						} else {
							setSelectBoxValue('shopType',type+1);
							UnitTreeCatalog.updateShopInfoScreenWhenSelectParentShop(type+1);
						}
						UnitTreeCatalog._isChange=true;
					} else {
						var shType = data.type;
						if ((shType == undefined || shType == null) && data.id != activeType.ALL) {
							Alert('Thông báo','Mã đơn vị cha phải là cấp GT(KA, ST), Miền, Vùng');
							$('#parentShopTree').combotree('setValue',activeType.ALL);
						}
						setSelectBoxValue('shopType',-1);
					}
					$('#parentShopName').val(Utils.XSSEncode(data.shopName));
				}, true);
			} else {
				shopTypeOfShop = shopTypeOfParentShop+1;
				
			}
		}
		TreeUtils.loadComboTreeAreaForUnitTreeHasTitle('areaTree', 'areaId',areaIdOfShop,function(data) {
			if(data.id != activeType.ALL && !isDisableAreaTree) {
				if(data.type != 'WARD') {
					Alert('Thông báo','Địa bàn chọn phải là phường/xã.',function(){
						$('#areaTree').combotree('setValue',activeType.ALL);
						$('#provinceName').val('');
						$('#districtName').val('');
						$('#precinctName').val('');
						//ViettelMap.clearOverlays();
					});
				} else {
					UnitTreeCatalog.loadAreaInfoAndMap(data);
				}
			}
		}, function() {
			if(isDisableAreaTree) { 
				disableComboTree('areaTree');
				$('#areaTree').combotree('setValue',activeType.ALL);
			}
		});
		//disableSelectbox('shopType');
		//setSelectBoxValue('shopType',shopTypeOfShop);
		return false;
	},
	loadComboTreeShop: function(controlId, storeCode,defaultValue,callback,params,returnCode){		 
		var _url= '/rest/catalog/parentshop/combotree/{0}.json';
		_url = _url.replace("{0}", $("#parentShopIdHd").val().trim());
		$('#' + controlId).combotree({url: _url,
			formatter: function(node) {
				return Utils.XSSEncode(node.text);
			},
			onChange: function(newValue,oldValue){
				if(params != null && params.length >0){
					for(var i=0;i<params.length;i++){
						$('#' +params[i]).val('');
					}
				}
			}
		});
		var t = $('#' + controlId).combotree('tree');
		t.tree('options').url = '';
		t.tree({			
			onSelect:function(node){
				var data = new Object();
				if(node.attributes != null) {
					data = node.attributes.shop;
				} else {
					data.id= node.id;
				}
				if(callback!=undefined && callback!=null){
					callback.call(this, data);
				}
				if(returnCode) {
					$('#' +storeCode).val(data.shopCode);
				} else {
					$('#' +storeCode).val(node.id);	
				}	
				$('#' +controlId).combotree('setValue', Utils.XSSEncode(data.shopCode));
			},
			onBeforeExpand:function(result){	            					
				var nodes = t.tree('getChildren',result.target);
				/*for(var i=0;i<nodes.length;++i){
					t.tree('remove',nodes[i].target);
				}*/
				if (nodes.length > 0) {
					return;
				}
				var ___jaxurl = '/rest/catalog/shop/combotree/2/';
				$.ajax({
					type : "POST",
					url : ___jaxurl + result.id +'.json',
					dataType : "json",
					success : function(r) {
						$(r).each(function(i,e){
							e.text = Utils.XSSEncode(e.text);
						});
						t.tree('append',{
							parent:result.target,
							data:r
						});
						var node=t.tree('getNode',result.target);
						$(node.target).next().css('display','block');
					}
				});
			},
			onExpand: function(result){	
			}
		});
		if(defaultValue == undefined || defaultValue == null){
			$('#' + controlId).combotree('setValue',activeType.ALL);
		} else {
			$('#' + controlId).combotree('setValue',defaultValue);
		}
	},
	resetScreen:function() {
		$('#phoneNumber').val('');
		$('#mobileNumber').val('');
		$('#faxNumber').val('');
		$('#email').val('');
		//$('#accountNumber').val('');
		//$('#bankName').val('');
		$('#taxNumber').val('');
		$('#addressBillTo').val('');
		$('#contactName').val('');
		$('#addressShipTo').val('');
		$('#address').val('');
	},
	updateShopInfoScreenWhenSelectParentShop: function(type) {
		UnitTreeCatalog.resetScreen();
		if(type == 3) {
			enable('phoneNumber');
			enable('mobileNumber');
			enable('phoneNumber');
			enable('faxNumber');
			enable('email');
			enable('taxNumber');
			enable('addressBillTo');
			enable('contactName');
			enable('addressShipTo');
			enable('address');
			enableComboTree('areaTree');
			$('.RequireStypeOfParentShopCode').show();
		} else {
			disabled('phoneNumber');
			disabled('mobileNumber');
			disabled('phoneNumber');
			disabled('faxNumber');
			disabled('email');
			disabled('taxNumber');
			disabled('addressBillTo');
			disabled('contactName');
			disabled('addressShipTo');
			disabled('address');
			$('.RequireStypeOfParentShopCode').hide();
			$('#areaTree').combotree('setValue',activeType.ALL);
			disableComboTree('areaTree');
		}
	},
	addOrUpdateShopInfo:function(){
		$('#errMsgAddOrUpdate').html('').hide();
		var msg = '';
		//var msg = Utils.getMessageOfRequireCheck('parentShopCode','Mã đơn vị cha');
		/*if(msg.length==0){
			msg = Utils.getMessageOfSpecialCharactersValidate('parentShopCode','Mã đơn vị cha',Utils._CODE);
		}*/
		if (msg.length == 0) {
			msg = Utils.getMessageOfRequireCheck('shopCode', 'Mã đơn vị');
		}
		if (msg.length == 0) {
			msg = Utils.getMessageOfSpecialCharactersValidate('shopCode', 'Mã đơn vị', Utils._CODE);
		}
		if (msg.length == 0) {
			msg = Utils.getMessageOfRequireCheck('shopName', 'Tên đơn vị');
		}
		if (msg.length == 0) {
			msg = Utils.getMessageOfSpecialCharactersValidate('shopName', 'Tên đơn vị', Utils._SPECIAL);
		}
		if (msg.length == 0) {
			msg = Utils.getMessageOfRequireCheck('shopType', 'Loại đơn vị', true);
		}
		if (msg.length == 0) {
			msg = Utils.getMessageOfRequireCheck('status', 'Trạng thái', true);
		}
		var shopType = $('#shopType').val().trim();
		var areaId = null;
		//p if(shopType ==3) {
		/** validate giong truong ten*/
		if (msg.length == 0 && $('#phoneNumber').val().trim().length > 0) {
			msg = Utils.getMessageOfSpecialCharactersValidate('phoneNumber', 'Phone', Utils._SPECIAL);
		}
		if (msg.length == 0 && $('#mobileNumber').val().trim().length > 0) {
			msg = Utils.getMessageOfSpecialCharactersValidate('mobileNumber', 'Mobile', Utils._SPECIAL);
		}
		if (msg.length == 0 && $('#faxNumber').val().trim().length > 0) {
			msg = Utils.getMessageOfSpecialCharactersValidate('faxNumber', 'Số Fax', Utils._SPECIAL);
		}
		if (msg.length == 0) {
				msg = Utils.getMessageOfInvalidEmailFormat('email','Email');
		}
		/*if(msg.length == 0 && $('#accountNumber').val().trim().length>0){
			msg = Utils.getMessageOfSpecialCharactersValidateEx('accountNumber','Số tài khoản',Utils._TF_NUMBER);
		}*/
		if (msg.length == 0 && $('#abbreviation').val().trim().length > 0) {
			msg = Utils.getMessageOfSpecialCharactersValidate('abbreviation', 'Tên viết tắt', Utils._SPECIAL);
		}
		if (msg.length == 0) {
			msg = Utils.getMessageOfSpecialCharactersValidate('addressBillTo', 'Địa chỉ giao HĐ', Utils._ADDRESS);
		}
		if (msg.length == 0 && $('#contactName').val().trim().length > 0) {
			msg = Utils.getMessageOfSpecialCharactersValidate('contactName', 'Tên người liên hệ', Utils._NAME);
		}
		if (msg.length == 0) {
			msg = Utils.getMessageOfSpecialCharactersValidate('addressShipTo', 'Địa chỉ giao hàng', Utils._ADDRESS);
		}
		/*if(msg.length == 0){
			msg = Utils.getMessageOfRequireCheck('address','Địa chỉ');
		}*/
		if (msg.length == 0) {
			msg = Utils.getMessageOfSpecialCharactersValidate('address', 'Địa chỉ', Utils._ADDRESS);
		}
		areaId = $('#areaId').val().trim();
		if(msg.length > 0){
			$('#errMsgAddOrUpdate').html(msg).show();
			return false;
		}
		var node = $('#tree').tree('getSelected');//p+
		var organizationId = '';
		organizationId = $('#organizationId').val().trim();
		var dataModel = new Object();
		dataModel.parentShopCode = $('#parentShopCode').val().trim();
		dataModel.shopId = $('#shopIdInfo').val().trim();
		dataModel.shopCode = $('#shopCode').val().trim();
		dataModel.shopName = $('#shopName').val().trim();
		dataModel.abbreviation = $('#abbreviation').val().trim();
		dataModel.status = $('#status').val();
		dataModel.shopType = shopType;//p+
		if (organizationId != null) {
			dataModel.organizationId = organizationId;
		}
		dataModel.phoneNumber = $('#phoneNumber').val().trim();
		dataModel.mobileNumber = $('#mobileNumber').val().trim();
		dataModel.faxNumber = $('#faxNumber').val().trim();
		dataModel.email = $('#email').val().trim();
		dataModel.taxNumber = $('#taxNumber').val().trim();
		dataModel.addressBillTo = $('#addressBillTo').val().trim();
		dataModel.contactName = $('#contactName').val().trim();
		dataModel.addressShipTo = $('#addressShipTo').val().trim();
		dataModel.areaId = areaId;
		dataModel.address = $('#address').val().trim();
		dataModel.lat = $('#lat').val().trim();
		dataModel.lng = $('#lng').val().trim();
		UnitTreeCatalog._isChange=false;
		Utils.addOrSaveData(dataModel, "/catalog/unit-tree/addorupdate-shop", UnitTreeCatalog._xhrSave, 'errMsgAddOrUpdate',function(data){
			$('#shopIdInfo').val(data.shopId);
			if( data.shopId != undefined && data.shopId != null) {
				UnitTreeCatalog.loadUnitTree(data.shopId);
			}
			setTimeout(function(){
				$('#successMsg').html(msgCommon1).show();
				//UnitTreeCatalog.openCollapseTab();
				//UnitTreeCatalog.showCollapseTab(1);
				/*UnitTreeCatalog.showCollapseTab(0);
				UnitTreeCatalog.openCollapseTab();
				UnitTreeCatalog.showSearchScreen();*/
			/*	UnitTreeCatalog.showShopInfoScreen(data.parentShopId,data.shopId);
				UnitTreeCatalog.openCollapseTab();
				UnitTreeCatalog.showCollapseTab(0);*/
				//UnitTreeCatalog.showCollapseTab(-1, data.shopId,data.parentShopId);
			}, 2000);
			/*UnitTreeCatalog.openCollapseTab();
			UnitTreeCatalog.showShopInfoScreen(data.parentShopId,data.shopId);
			UnitTreeCatalog.showCollapseTab(0);*/
			
		},null,'.ReportCtnSection');
		return false;
	},
	loadAreaInfoAndMap:function(area) {
		$('#provinceName').val('');
		$('#districtName').val('');
		$('#precinctName').val('');
		if (area != undefined && area != null) {
			$('#provinceName').val(area.provinceName);
			$('#districtName').val(area.districtName);
			$('#precinctName').val(area.precinctName);
		}
		return false;
	},
	viewBigMap : function() {
		var params = new Object();
		params.lat = $('#lat').val().trim();
		params.lng = $('#lng').val().trim();
		//CommonSearchEasyUI.viewBigMap(params, catalog_unit_view_ban_do_don_vi); //'Bản đồ vị trí đơn vị'
		CommonSearchEasyUI.viewBigMapShopTree(params, catalog_unit_view_ban_do_don_vi); //'Bản đồ vị trí đơn vị'
	},
	//-------------------------------------------------------------------------------------------------------
		
	//NhanLT-------------------------------------------------------------------------------------------------
	/** vuongmq; 26/02/2015; Quay lai view danh sach staff*/
	showStaffSearchScreenBack: function(){
		$.messager.confirm(jsp_common_xacnhan, catalog_unit_view_thong_tin_nhan_vien_back, function(r){
			if (r){
				/*UnitTreeCatalog.showCollapseTab(0);
				UnitTreeCatalog.showTab2();
*/				//alert('confirm: chưa có tab nhân viên (khi nào có thì trả về trang này).OK TESTER');
				//UnitTreeCatalog.showStaffSearchScreen();
				//UnitTreeCatalog.showCollapseTab(1);
				//UnitTreeCatalog.openCollapseTab();
				UnitTreeCatalog.removeActiveCollapseTab();
				$('#collapseTab1').addClass('Active');
				UnitTreeCatalog.showSearchScreen();
			}
		});
		return false;
	},
	/** vuongmq; 26/02/2015; view danh sach staff*/
	showStaffSearchScreen: function(confirm){
		UnitTreeCatalog._isChange=false;
		var params = new Object();
		Utils.getHtmlDataByAjax(params, '/catalog/unit-tree/manage-staff',function(data) {
			$("#unitTreeContent").html(data).show();
			
			UnitTreeCatalog.resize();
		}, null, 'POST');
		return false;
	},
	/** vuongmq; 26/02/2015; goi ham view ban do cho thuoc tinh staff*/
	viewBigMapOnDlg : function(id) {
		var selector = $('#' + id);
		if(UnitTreeCatalog._isBkLatLng == false){
			//var latbk = '';
			//var lngbk = '';
			var latLngbk = selector.val().trim();
			if(!isNullOrEmpty(latLngbk) && latLngbk.indexOf(';')>=0){
				UnitTreeCatalog.latbk = latLngbk.split(';')[0];
				UnitTreeCatalog.lngbk = latLngbk.split(';')[1];
			}
			UnitTreeCatalog._isBkLatLng = true;		
		}
		//$('#viewBigMap').css('visibility','visible');
		var html = $('#viewBigMap').html();
		$('#viewBigMap').dialog({
			closed: false,  
			title : xem_ban_do,
			cache: false,  
			modal: true,
			onOpen: function(){
				var tm = setTimeout(function(){					
					var lat = '';
					var lng = '';
					var latLng = selector.val().trim();
					if(!isNullOrEmpty(latLng) && latLng.indexOf(';')>=0){
						lat = latLng.split(';')[0];
						lng = latLng.split(';')[1];
					}
					var zoom = 12;
					if(lat.trim().length==0 || lng.trim().length==0){
						zoom = ViettelMap._DEF_ALL_ZOOM;
					}
					VTMapUtil.loadMapResource(function(){
						ViettelMap.loadBigMapEx('bigMapContainer', lat, lng, zoom, function(latitude, longtitude) {
							selector.val(latitude + ';' + longtitude);
						}, true);
					});
				}, 1500);
				$('.easyui-dialog #imgBtnDeleteLatLng').unbind('click');
				$('.easyui-dialog #imgBtnDeleteLatLng').bind('click',function(){
					selector.val('');
					ViettelMap.clearOverlays();
					ViettelMap._listOverlay = new Array(); 
					if (ViettelMap._marker != null){
						ViettelMap._marker.setMap(null);
					}
					
				});
				$('.easyui-dialog #imgBtnUpdateLatLng').unbind('click');
				$('.easyui-dialog #imgBtnUpdateLatLng').bind('click',function(){
					var lat = '';
					var lng = '';
					var latLng = selector.val().trim();
					if(!isNullOrEmpty(latLng) && latLng.indexOf(';')>=0){
						lat = latLng.split(';')[0];
						lng = latLng.split(';')[1];
					}
					var zoom = 12;
					if(lat.trim().length==0 || lng.trim().length==0){
						zoom = ViettelMap._DEF_ALL_ZOOM;
					}
					VTMapUtil.loadMapResource(function(){
						ViettelMap.loadBigMapEx('bigMapContainer', lat, lng, zoom, function(latitude, longtitude) {
							selector.val(latitude + ';' + longtitude);
						}, true);
					});
					$('#viewBigMap').dialog('close');
				});
				$('.easyui-dialog #imgBtnCancelLatLng').unbind('click');
				$('.easyui-dialog #imgBtnCancelLatLng').bind('click',function(){
					var zoom = 12;
					if(UnitTreeCatalog.latbk.trim().length==0 || UnitTreeCatalog.lngbk.trim().length==0){
						zoom = ViettelMap._DEF_ALL_ZOOM;
					}
					VTMapUtil.loadMapResource(function(){
						ViettelMap.loadBigMapEx('bigMapContainer', UnitTreeCatalog.latbk, UnitTreeCatalog.lngbk, zoom, function(latitude, longtitude) {
							selector.val(latitude + ';' + longtitude);
						}, true);
					});
					//$('#viewBigMap').onClose();
				});
				
			},
			onClose:function() {
				$('#viewBigMap').html(html);
				//
			}
		});
		
	},
	/** vuongmq; 26/02/2015; goi ham view danh sach chi tiet staff; khi click  them moi tren don vi*/
	showStaffDetailScreen: function(nodeType,nodeTypeId, orgId, nameShop, shopId, staffId,callBack){
		var params = new Object();
		if(staffId != null){
			params.staffId = staffId;
		}
		Utils.getHtmlDataByAjax(params, '/catalog/unit-tree/view-staff',function(data) {
			$("#unitTreeContent").html(data).show();
			/**vuongmq; 27/02/2015; view Selected loai nhan vien*/
			$('#staffType').find('option[value=' +nodeTypeId+']').attr('selected','selected');
			/**vuongmq; 27/02/2015; view textbox nhan vien tren cay*/
			$('#staffShopCode').val(nameShop);
			$('#nodeTypeId').val(nodeTypeId);
			$('#nodeType').val(nodeType);
			$('#nodeShopId').val(shopId);
			$('#orgId').val(orgId);
			UnitTreeCatalog.resize();
			UnitTreeCatalog._typeScreen = -3;
		}, null, 'POST');
		return false;
	},
	showStaffCreateScreen: function(staffCreateType, shopCode, shopName, callBack){
		var params = new Object();
		params.staffCreateType = staffCreateType;
		params.shopCode = shopCode;
		params.shopName = shopName;
		Utils.getHtmlDataByAjax(params, '/catalog/unit-tree/view-staff', function(data) {
			$("#unitTreeContent").html(data).show();
			UnitTreeCatalog.resize();
			UnitTreeCatalog._typeScreen = -3;
		}, null, 'POST');
		return false;
	},
	/** vuongmq; 26/02/2015; goi ham view danh sach chi tiet staff; khi dubclick staff tren cay (edit nhan vien)*/
	showStaffEditScreen: function(staffId,shopId){
		var params = new Object();
		params.staffId = staffId;
		//params.staffObjectType = staffObjectType;
		if(shopId!=undefined && shopId!=null){
			params.shopId=shopId;
			//params.shId = shopId;
		}
		Utils.getHtmlDataByAjax(params, '/catalog/unit-tree/view-staff',function(data) {
			$("#unitTreeContent").html(data).show();
			UnitTreeCatalog.resize();
			UnitTreeCatalog._typeScreen = -3;
		}, null, 'POST');
		return false;
	},
	openPopupImportStaff : function(){
		$('#file').html('');
		$('.easyui-dialog #resultExcelMsg').html('').hide();
		$('#easyuiPopup').show();
		$('#errExcelMsg').html('').hide();
		ReportUtils._callBackAfterImport = function(){
			$('#gridStaffType').datagrid('load', {page : 1, shopId:$('#shopIdHidden').val()});
			ReportUtils._callBackAfterImport=null;
			hideLoadingIcon();
		};
		$('#easyuiPopup').dialog('open');
	},
	/**
	 * @author phuocdh2 upload shop
	 * @description ham import ds  don vi
	 */ 
	uploadShop : function(){ //upload trong fancybox
		$('#isViewShop').val(0);
		Utils._inputText = 'fakefilepc_shop';
		Utils.importExcelUtils(function(data){
			if(data.message!=undefined && data.message!=null && data.message.trim().length >0){
				$('#errExcelMsgShop').html(data.message.trim()).change().show();
				var tm = setTimeout(function(){
					$('#errExcelMsgShop').html(data.message.trim()).change().show();
					clearTimeout(tm);

				 }, 10000);
			}else{
				UnitTreeCatalog.loadUnitTree();
				$('#successMsgImportShop').html(msgCommon1).show();
				var tm = setTimeout(function(){
					$('#successMsgImportShop').html("").hide();
					clearTimeout(tm);
				 }, 4000);
			}
			$('#excelFileShop').val(""); //longnh15 add - xoa duong dan excel
			$('#fakefilepc_shop').val(""); //longnh15 add - xoa text
		}, 'importShopFrm', 'excelFileShop');
		
		return false;
	},
	upload : function(){ //upload trong fancybox
		$('#isViewStaff').val(0);
		Utils._inputText = 'fakefilepc_staff';
		Utils.importExcelUtils(function(data){
			if(data.message!=undefined && data.message!=null && data.message.trim().length >0){
				$('#errExcelMsg').html(data.message.trim()).change().show();
				var tm = setTimeout(function(){
					$('#errExcelMsg').html(data.message.trim()).change().show();
					clearTimeout(tm);
				 }, 10000);
			}else{
				UnitTreeCatalog.loadUnitTree();
				$('#successMsgImportStaff').html(msgCommon1).show();
				var tm = setTimeout(function(){
					$('#successMsgImportStaff').html("").hide();
					clearTimeout(tm);
				 }, 4000);
			}
		}, 'importFrm', 'excelFileStaff');
		return false;
	},
	
	search: function(independentSearch){
		$('#errMsg').html('').hide();
		if (independentSearch != 1) {
			var staffCode = $('#staffCode').val().trim();
			var staffName = $('#staffName').val().trim();
			var shopId = 0;
			var node = $('#tree').tree('getSelected');
			var status = $('#status').val();
			if (node != null) {
				if(node.attributes.shop!=null) shopId= node.attributes.shop.id;
				else if(node.attributes.staff!=null) shopId= node.attributes.staff.shop.id;
			}
			$('#searchStaffUnitTreeGrid').datagrid('load',{page : 1,shopId:shopId,status:status, staffCode: staffCode, staffName: staffName});
		} else {
			var staffCode = $('#staffCode').val().trim();
			var staffName = $('#staffName').val().trim();
			$('#searchStaffUnitGrid').datagrid('load',{page : 1, staffCode: staffCode, staffName: staffName});
		}
		return false;
	},
	exportExcel:function(){
		$('#errMsg').html('').hide();
		var shopId= 0;
		var staffCode = $('#staffCode').val();
		var staffName = $('#staffName').val();
		var status = activeType.parseValue($('#staffStatus').val().trim());
		var staffTypeName = $('#staffType').val();
		
		var params = new Object();
		//params.shopId = shopId;
		shopId = $('#manageShopStaff').combotree('getValue');
		params.shId = shopId;
		params.staffCode = staffCode;
		params.staffName = staffName;
		params.status = status;
		params.staffTypeName = staffTypeName;
		var url = "/catalog/unit-tree/export-excel";
		ReportUtils.exportReport(url, params, 'errExcelMsg');
		return false;
	},
	/** vuongmq; 26/02/2015; luu thong tin chi tiet staff*/
	changeStaffInfo:function(isEdit, staffId){
		var msg = '';
		$('.SuccessMsgStyle').html('').hide();
		$('.ErrorMsgStyle').html('').hide();
		//$('#errMsg').html('').hide();
		//$('errMsgStaff').html('').hide();
		////////////////////////////////////////////// thong tin co ban
		msg = Utils.getMessageOfRequireCheck('staffCode', catalog_staff_code); //'Mã nhân viên'
		if(msg.length==0){
			msg = Utils.getMessageOfSpecialCharactersValidate('staffCode', catalog_staff_code,Utils._CODE); //'Mã nhân viên'
		}	
		
		if(msg.length == 0){
			msg = Utils.getMessageOfRequireCheck('staffName', catalog_staff_name); //'Tên nhân viên'
		}		
		/*if(msg.length == 0){
			msg = Utils.getMessageOfSpecialCharactersValidate('staffName', catalog_staff_name,Utils._NAME); //'Tên nhân viên'
		}*/		
		if(msg.length == 0){
			msg = Utils.getMessageOfRequireCheck('staffShopCode', catalog_unit_tree,true); //'Đơn vị'
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfRequireCheck('staffType', catalog_staff_type,true); //'Loại nhân viên'
		}
		/*if(msg.length == 0){
			msg = Utils.getMessageOfRequireCheck('gender', catalog_gender_value,true); //'Giới tính'
		}*/
		if(msg.length == 0){
			if ($('#workStartDate').val().trim() != '' && !Utils.isDate($('#workStartDate').val().trim())) {
				//msg = 'Ngày bắt đầu làm việc không hợp lệ, vui lòng nhập theo định dạng dd/MM/yyyy';
				msg = catalog_unit_ngay_bat_dau_lam_viec_khong_hop_le;
				$('#workStartDate').focus();
			}
		}
		if (msg.length == 0 && $('#workStartDate').val().trim() != '' && !Utils.isDate($('#workStartDate').val().trim())) {
			//msg = 'Ngày bắt đầu làm việc không hợp lệ, vui lòng nhập theo định dạng dd/MM/yyyy';
			msg = catalog_unit_ngay_bat_dau_lam_viec_khong_hop_le;
			$('#workStartDate').focus();
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfRequireCheck('status', jsp_common_status,true); //'Trạng thái'
		}
		////////////////////////////////////////////// thong tin lien lac
		/*if(msg.length == 0){
			msg = Utils.getMessageOfSpecialCharactersValidate('staffPhone', catalog_mobilephone); //'Số di động'
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfSpecialCharactersValidate('staffTelephone', catalog_telephone); //'Số cố định'
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfInvaildNumber('staffPhone', catalog_mobilephone); //'Số di động'
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfInvaildNumber('staffTelephone', catalog_telephone); //'Số cố định'
		}*/
		if(msg.length == 0){
			msg = Utils.getMessageOfInvalidEmailFormat('email', catalog_email_code); //'Email'
		}
		/*if(msg.length == 0){
			var area = $('#areaId').val().trim();
			if (area == null || area == -2) {
				msg = 'Địa bàn là bắt buộc. Xin vui lòng chọn địa bàn';
				$('#areaTree').focus();
			}
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfSpecialCharactersValidate('provinceName','Tỉnh thành');
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfSpecialCharactersValidate('districtName','Quận/ Huyện');
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfSpecialCharactersValidate('precinctName','Phường/ Xã');
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfSpecialCharactersValidate('houseNumber','Số nhà, đường');
		}*/
		
		////////////////
		/*if(msg.length == 0){
			msg = Utils.getMessageOfSpecialCharactersValidate('street','Đường');
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfSpecialCharactersValidate('idPlace','Nơi cấp');
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfSpecialCharactersValidate('education','Trình độ');
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfSpecialCharactersValidate('position','Vị trí chức danh');
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfSpecialCharactersValidate('address','Địa chỉ');
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfSpecialCharactersValidate('houseNumber','Số nhà');
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfSpecialCharactersValidate('street','Đường');
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfSpecialCharactersValidate('idCard','CMND');
		}		
		if (msg.length == 0 && $('#idDate').val().trim() != '' && !Utils.isDate($('#idDate').val().trim())) {
			msg = 'Ngày cấp không hợp lệ, vui lòng nhập theo định dạng dd/MM/yyyy';
			$('#idDate').focus();
		}*/
		if(msg.length > 0){
			$('#errMsgStaff').html(msg).show();
			return false;
		}
		/*var multiStaff = $("#parentStaff").data("kendoMultiSelect");
		var lstParentStaffId=new Array();
		if (multiStaff != null) {
			var dataStaff = multiStaff.dataItems();
			if(dataStaff.length>0){
				for(var i = 0;i<dataStaff.length;i++){
					lstParentStaffId.push(dataStaff[i].id);
				}
			}
			
		}*/
		
		var dataModel = new Object();
		if (isEdit == 1) { // 1:cap nhat staff // 0:tao moi staff
			dataModel.staffId = staffId;
		}
		//dataModel.shopCode = 'VNM_COM';
		dataModel.nodeTypeId = $('#nodeTypeId').val();
		dataModel.nodeType = $('#nodeType').val();
		dataModel.shopId = $('#nodeShopId').val();
		dataModel.orgId = $('#orgId').val();

		dataModel.staffCode = $('#staffCode').val().trim();
		dataModel.staffName = $('#staffName').val().trim();				
		dataModel.gender = $('#gender').val();
		dataModel.workStartDate = $('#workStartDate').val().trim();
		dataModel.status = $('#status').val();

		dataModel.staffPhone = $('#staffPhone').val().trim();
		dataModel.staffTelephone = $('#staffTelephone').val().trim();
		dataModel.email = $('#email').val().trim();
		dataModel.areaId = $('#areaId').val().trim();
		dataModel.houseNumber = $('#houseNumber').val().trim();
		if ( $('#orderProduct').val() != undefined && $('#orderProduct').val() != null &&  $('#orderProduct').val() != '' ) {
			dataModel.orderProduct = $('#orderProduct').val();
		}	
		if ($('#manageSubStaff').combobox('getValues') != undefined && $('#manageSubStaff').combobox('getValues') != null  && $('#manageSubStaff').combobox('getValues') != '') {
			dataModel.subStaff = $('#manageSubStaff').combobox('getValues');
		}	
		/*dataModel.shopName = $('#staffShopName').val().trim();
		dataModel.idCard = $('#idCard').val().trim();
		dataModel.idDate = $('#idDate').val().trim();
		dataModel.idPlace = $('#idPlace').val().trim();
		dataModel.street = $('#street').val().trim();
		dataModel.address = $('#address').val().trim();
		dataModel.staffType = $('#staffType').val();
		dataModel.saleTypeCode = $('#saleType').val();
		dataModel.education = $('#education').val().trim();
		dataModel.position = $('#position').val().trim();
		dataModel.staffCreateType = staffCreateType;
		dataModel.lstParentStaffId=lstParentStaffId;*/
		
		/** luu thuoc tinh dong*/
		if(!Utils.validateAttributeData(dataModel, '#errMsgStaff', '#propertyTabContainer ')) {
			return false;
		}
		UnitTreeCatalog._isChange=false;
		Utils.addOrSaveData(dataModel, "/catalog/unit-tree/change-Staff-Info", UnitTreeCatalog._xhrSave, 'errMsgStaff',function(data){
			if(data.error != null && data.error == false){
				//$('#successMsgStaff').html('Lưu dữ liệu thành công').show();
				$('#successMsgStaff').html(msgCommon1).show();
				if (isEdit == 0) {
					//UnitTreeCatalog.loadUnitTree(data.shopId);
					//UnitTreeCatalog.showCollapseTab(3);
					//UnitTreeCatalog.loadUnitTree(data.shopId);
					//UnitTreeCatalog.showCollapseTab(3);
					UnitTreeCatalog.loadUnitTree(data.shopId, data.staff.id);
					/** them moi nhan vien xong chuyen ve la cap nhat nhan vien*/
					$('#btnUpdateStaff').attr('onclick', 'return UnitTreeCatalog.changeStaffInfo(1,' +data.staff.id+');');
					$('#staffCode').attr('disabled',true);
					UnitTreeCatalog.showStaffEditScreen(data.staffId, data.shopId);
				} else {
					UnitTreeCatalog.loadUnitTree(data.shopId, data.staff.id);
					/*setTimeout(function(){
						UnitTreeCatalog.resetStaff();
					}, 3000);*/
				}
						
				//UnitTreeCatalog.showSearchScreen(1);
			}
			
		},null,'#staff ');
	},
	
	loadTreeEx:function(areaIdOfShop,staffId){
		TreeUtils.loadComboTreeAreaForUnitTreeHasTitle('areaTree', 'areaId',areaIdOfShop,function(data) {
				UnitTreeCatalog.loadAreaInfoAndMap(data);
		});
	},
	resetStaff: function() {
		$('#staffShopCode').val('');		
		$('#staffShopName').val('');
		$('#staffId').val('');		
		$('#staffCode').val('');
		$('#staffName').val('');				
		$('#idCard').val('');
		$('#idDate').val('');
		$('#idPlace').val('');
		$('#provinceName').val('');
		$('#districtName').val('');
		$('#precintName').val('');
		$('#street').val('');
		$('#address').val('');
		$('#email').val('');
		$('#saleGroup').val('');
		$('#staffPhone').val('');
		$('#staffType').val(-1);
		$('#staffType').change();
		$('#workStartDate').val('');
		$('#education').val('');
		$('#position').val('');
		$('#successMsgStaff').hide();
		$('#staffTelephone').val('');	
		$('#gender').val(-1);
		$('#gender').change();
		$('#areaTree').combotree('reload');
		UnitTreeCatalog.showCollapseTab(0);
	},
	showArea:function(selector,type,codeValue){		
		var html = new Array();	
		html.push("<option value=''>" + Utils.XSSEncode(jsp_common_status_all) + "</option>"); //Tất cả
		var areaCode = $(selector).val().trim();		
		if(areaCode!=undefined && areaCode!=null && areaCode.trim().length==0){
			if($(selector).attr('id')=='provinceCode'){
				$('#districtCode').html(html.join(""));
				$('#wardCode').html(html.join("")); 
				$('#districtCode').val('');
				$('#wardCode').val('');
				disableSelectbox('districtCode');
				disableSelectbox('wardCode');
				
			}
			if($(selector).attr('id')=='districtCode'){
				$('#wardCode').html(html.join(""));
				$('#wardCode').val('');
				disableSelectbox('wardCode');
			}
			
			return false;
		}
		$.ajax({
			type : "POST",
			url : "/catalog_staff_manager/show-area",
			data : ({areaCode:areaCode}),
			dataType: "json",
			success : function(data) {				
				if(data!= null && data!= undefined && data.areas!= null && data.areas.length > 0){    				
					for(var i=0;i<data.areas.length;i++){    					
						html.push("<option value='" + Utils.XSSEncode(data.areas[i].areaCode) +"'>" + Utils.XSSEncode(data.areas[i].areaName) +"</option>");
					}
					if(type==1){
						enable('districtCode');
						$('#districtCode').parent().removeClass('BoxDisSelect');
					}else if(type==2){    					
						enable('wardCode');
						$('#wardCode').parent().removeClass('BoxDisSelect');
					}    				
				}    			
				if(type==1){    				
					$('#districtCode').html(html.join(""));  
					if(codeValue!=undefined && codeValue!=null && codeValue.trim().length>0){
						$('#districtCode').val(codeValue);
						disableSelectbox('districtCode');
					}
				}else if(type==2){    				
					$('#wardCode').html(html.join("")); 
					if(codeValue!=undefined && codeValue!=null && codeValue.trim().length>0){
						$('#wardCode').val(codeValue);
						disableSelectbox('wardCode');
					}     				    				
				}    			
			},
			error:function(XMLHttpRequest, textStatus, errorThrown) {				
			}
		});
	},
	//-------------------------------------------------------------------------------------------------------
	loadParentStaff:function(){
		var parentStaffId=$('#parentStaffId').val().trim();
		var lstParentStaff=[];
		if(parentStaffId!=''){
			eval('lstParentStaff=[' +parentStaffId+']');
		}
		$("#parentStaff").kendoMultiSelect({
			dataTextField: "staffCode",
			dataValueField: "staffId",
			filter: "contains",
			itemTemplate: function(data, e, s, h, q) {
				return Utils.XSSEncode(data.staffCode) + ' - ' + Utils.XSSEncode(data.staffName);
			},
			tagTemplate:  '#: data.staffCode #',
			change: function(e) {
			},
			dataSource: {
				transport: {
					read: {
						dataType: "json",
						url: "/rest/catalog/staff-unit-tree/kendo-ui-combobox.json"
					}
				}
			},
			value: lstParentStaff
		});

		var staffKendo = $("#parentStaff").data("kendoMultiSelect");
		staffKendo.wrapper.attr("id", "parentShop-wrapper");
	},
	/**
	 * download file template import nhan vien
	 * @author tuannd20
	 * @since 28/03/2015
	 */
	downloadImportStaffTemplateFile: function() {
		ReportUtils.exportReport('/catalog/unit-tree/download-import-staff-template-file?excelType=' +$('#excelType').val(), {}, 'errExcelMsg');
		return false;
	},
	/**
	 * imporrt file template import nhan vien
	 * @author hoanv25
	 * @since 14/08/2015
	 */
	openImportExcelType:function(){
		Utils._inputTextFileExcel = null;
		$('#popupImportUnit').dialog({
			title : 'Chọn loại import',
			width:250,
			height:'auto',
			onOpen: function(){
				
			}
		});
		$('#popupImportUnit').dialog('open');
	},
	/**
	 * imporrt file template import nhan vien
	 * @author hoanv25
	 * @since 14/08/2015
	 */
	importExcel : function() {
		var excelType = $('input:radio[name=importRadio]:checked').val();
	/*	Utils._currentSearchCallback = function(){
			KeyShop.searchKS();
		};*/
	/*	Utils._inputTextFileExcel = "excelFileShop";*/
		var options = {
			beforeSubmit : Utils.beforeImportExcel,
			success : Utils.afterImportExcelNew,
			type : "POST",
			dataType : 'html',
			data : { excelType : excelType }
		};
		$('#importFrm').ajaxForm(options);
		$('#importFrm').submit();
		$('#popupImportUnit').dialog('close');
		return false;
	},
	/**
	 * download file template import nhan vien
	 * @author tuannd20
	 * @since 28/03/2015
	 */
	downloadImportShopTemplateFile: function() {
		var url = "/catalog/unit-tree/download-import-shop-template-file";
		ReportUtils.exportReport(url, {}, 'errExcelMsg');
		return false;
	},
	initImport:function() {
		$('#downloadTemplate2').attr('href', 'javascript:void(0);');
		Utils._errExcelMsg = 'errExcelMsg';
		var options = {
			beforeSubmit : Utils.beforeImportExcel,
			success : Utils.afterImportExcelNew,
			type : "POST",
			dataType : 'html',
			data : { excelType : $('#excelType').val() }
		};
		$('#importFrm').ajaxForm(options);
		$('#excelFile').val('');
		$('#fakefilepc_staff').val('');
	},
	excelTypeChanged:function() {
		$('#excelFile').val('');
		$('#fakefilepc_staff').val('');
	},

	/**
	 * change pass popup user khi da dang nhap thanh cong
	 * @author vuongmq
	 * @params typeLogin (dng nhap pass mac dinh doi mat khau hoac doi mat khau)
	 * @since 25/08/2015 
	 */
	changePassPopupUser: function(typeLogin) {
		$('.ErrorMsgStyle').html('').hide();
		var msg = '';
		if (msg.length == 0) {
			msg = Utils.getMessageOfRequireCheck('seachStyle1PassOld', 'Mật khẩu cũ');
			cus = 'seachStyle1PassOld';
		}
		if (msg.length == 0) {
			msg = Utils.getMessageOfRequireCheck('seachStyle1PassNew', 'Mật khẩu mới');
			cus = 'seachStyle1PassNew';
		}
		if (msg.length == 0) {
			msg = Utils.getMessageOfPolicyPassWord('seachStyle1PassNew', 'Mật khẩu mới', UnitTreeCatalog._MAX_LENGTH_6);
			cus = 'seachStyle1PassNew';
		}
		if (msg.length == 0) {
			msg = Utils.getMessageOfRequireCheck('seachStyle1PassRetype', 'Nhập lại mật khẩu mới');
			cus = 'seachStyle1PassRetype';
		}
		if (msg.length == 0) {
			msg = Utils.getMessageOfPolicyPassWord('seachStyle1PassRetype', 'Nhập lại mật khẩu mới', UnitTreeCatalog._MAX_LENGTH_6);
			cus = 'seachStyle1PassRetype';
		}
		if (msg.length > 0) {
			$('#errMsgChangePass').html(msg).show();
			$('#' + cus).focus();
			return false;
		}
		var params = new Object();
		params.password = $('#searchStyle1EasyUIDialogChangePass #seachStyle1PassOld').val();
		params.newPass = $('#searchStyle1EasyUIDialogChangePass #seachStyle1PassNew').val();
		params.retypedPass = $('#searchStyle1EasyUIDialogChangePass #seachStyle1PassRetype').val();
		var url = '/changePassword';
		Utils.saveData(params, url, null, 'errMsgChangePass', function(data) {
			if (data.error != null && data.error == false && data.errMsg != undefined && data.errMsg != "") {
				$('#errMsgChangePass').html(data.errMsg).show();
			} else {
				$('#successMsgChangePass').html('Đổi mật khẩu thành công !').show();
				setTimeout(function() {
					$('#successMsgChangePass').html('').hide();
					if (typeLogin != undefined && typeLogin != undefined && typeLogin == StatusType.ACTIVE) {
						window.location.assign(window.location.origin + '/home');
						$('#searchStyle1EasyUIDialogChangePassDivChangePass').css("visibility", "hidden");
					} else {
						$('.easyui-dialog').dialog('close');
					}
				}, 2500);
			}
		}, null, null, null, null);
	},

	/**
	 * Open dia log change pass default
	 * @author vuongmq
	 * @since 30/10/2015 
	 */
	openDialogChangePassDefault: function() {
		$('.ErrorMsgStyle').html('').hide();
		$('#searchStyle1EasyUIDialogChangePassDivChangePass').css('visibility','visible');
		$('#searchStyle1EasyUIDialogChangePass').dialog({
			title: 'Đổi mật khẩu',
			width: 426,
			height: 280,
			closed: false,
			cache: false,
			modal: true,
			onOpen: function() {
				$('#searchStyle1EasyUIDialogChangePass #seachStyle1PassOld').focus();
				$('#searchStyle1EasyUIDialogChangePass #seachStyle1PassOld').val('');
				$('#searchStyle1EasyUIDialogChangePass #seachStyle1PassNew').val('');
				$('#searchStyle1EasyUIDialogChangePass #seachStyle1PassRetype').val('');
				//$('.easyui-dialog #seachStyle1Code')
				$(window).bind('keyup', function(event) {
					if (event.keyCode == 13) {
						$('#__btnSaveChangePass').click();
					}
				});
				$('#__btnSaveChangePass').attr('onclick', 'return UnitTreeCatalog.changePassPopupUser('+ StatusType.ACTIVE +');');
				$('#__btnCancelChangePass').html('Thoát');
				$('#__btnCancelChangePass').attr('onclick', 'return General.logoutSystem();');
				UnitTreeCatalog._flagNext = false;
			},
			onClose: function() {
	        	$('.ErrorMsgStyle').html('').hide();
	        	if (!UnitTreeCatalog._flagNext) {
	        		setTimeout(function() {
	        			UnitTreeCatalog.openDialogChangePassDefault();
	    			}, 500);
	        	}
	        }
		});
	},
};
/*
 * END OF FILE - /web/web/resources/scripts/business/catalog/vnm.unit-tree-catalog.js
 */

/*
 * START OF FILE - /web/web/resources/scripts/business/catalog/vnm.total-money-quota-catalog.js
 */
var TotalMoneyQuotaCatalog = {
	_xhrSave : null,
	_xhrDel: null,
	getGridUrl: function(channelTypeCode,channelTypeName,status,saleAmount){		
		return "/catalog/total-money-quota/search?code=" + encodeChar(channelTypeCode) + "&name=" + encodeChar(channelTypeName) + "&status=" + status + '&saleAmount=' + encodeChar(saleAmount);
	},	
	search: function(){		
		$('#errMsg').html('').hide();
		$('#errExcelMsg').html('').hide();
		var channelTypeCode = $('#code').val().trim();
		var channelTypeName =  $('#name').val().trim();		
		var status = $('#status').val().trim();
		var saleAmount =  Utils.returnMoneyValue($('#saleAmount').val().trim());
		var url = TotalMoneyQuotaCatalog.getGridUrl(channelTypeCode,channelTypeName,status,saleAmount);
		$("#grid").setGridParam({url:url,page:1}).trigger("reloadGrid");
		return false;
	},
	save: function(){
		var msg = '';
		$('#errMsg').html('').hide();
		$('#errExcelMsg').html('').hide();
		msg = Utils.getMessageOfRequireCheck('code','Mã loại KH',true);
		if(msg.length == 0){
			msg = Utils.getMessageOfRequireCheck('saleAmount','Mức DS');
		}
		var skuAmount = $('#saleAmount').val().trim().replace(/,/g,'');
		
		if(isNaN(skuAmount) || parseInt(skuAmount) <=0){
			msg = 'Mức DS phải là số nguyên dương';
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfRequireCheck('status','Trạng thái',true);
		}		
		if(msg.length > 0){
			$('#errMsg').html(msg).show();
			return false;
		}
		var dataModel = new Object();
		dataModel.id = $('#selId').val().trim();
		dataModel.code = $('#code').val().trim();		
		dataModel.saleAmount = skuAmount;
		dataModel.status = $('#status').val();
		Utils.addOrSaveRowOnGrid(dataModel, "/catalog/total-money-quota/save", TotalMoneyQuotaCatalog._xhrSave, null, null, function(data){
			if(!data.error){
				TotalMoneyQuotaCatalog.resetForm();
				TotalMoneyQuotaCatalog.search();
			}			
		});			
		return false;
	},
	getSelectedRow: function(rowId){
		disabled('name');
		var code = $("#grid").jqGrid ('getCell', rowId, 'channelTypeCode');
		var name = $("#grid").jqGrid ('getCell', rowId, 'channelTypeName');
		var status = $("#grid").jqGrid ('getCell', rowId, 'statusAmount');
		var skuAmount = $("#grid").jqGrid ('getCell', rowId, 'saleAmount');
		var id = $("#grid").jqGrid ('getCell', rowId, 'id');
		
		if(id!= null && id!= 0 && id!=undefined){
			$('#selId').val(id);
		} else {
			$('#selId').val(0);
		}
		setSelectBoxValue('code',code);
		setTextboxValue('name',name);
		setTextboxValue('saleAmount',skuAmount);
		if(status == activeStatusText){
			status = 1;
		} else {
			status = 0;
		}
		setSelectBoxValue('status', status);
		disabled('code');
		$('#code').change();
		TotalMoneyQuotaCatalog.getChangedForm();	
		setTitleUpdate();
		$('#saleAmount').focus();
		$('#errExcelMsg').html('').hide();
		return false;
	},
	resetForm: function(){
		setTitleSearch();
		$('.RequireStyle').hide();
		$('#selId').val(0);
		$('#errMsg').html('').hide();
		$('#errExcelMsg').html('').hide();
		$('#name').removeAttr('disabled');
		$('#btnEdit').hide();
		$('#btnCancel').hide();
		$('#btnSearch').show();
		$('#btnAdd').show();		
		$('#name').val('');
		$('#saleAmount').val('');
		setSelectBoxValue('code', -2);
		setSelectBoxValue('status', 1);		
		enable('code');
		$('#code').change();
		TotalMoneyQuotaCatalog.search();
		$('#saleAmount').focus();
		return false;
	},
	deleteSale: function(id){
		var dataModel = new Object();
		dataModel.id = id;	
		Utils.deleteSelectRowOnGrid(dataModel, 'mức DS của loại KH', '/catalog/total-money-quota/delete', TotalMoneyQuotaCatalog._xhrDel, null, null,function(data){
			if (!data.error){
				TotalMoneyQuotaCatalog.resetForm();
			}
		},null);
		$('#errExcelMsg').html('').hide();
		return false;
	},
	getChangedForm: function(){
		$('#saleAmount').focus();
		$('.RequireStyle').show();
		$('#btnSearch').hide();
		$('#btnAdd').hide();
		$('#btnEdit').show();
		$('#btnCancel').show();	
		disabled('name');
		setTitleAdd();
		$('#code').change();
	}
};
/*
 * END OF FILE - /web/web/resources/scripts/business/catalog/vnm.total-money-quota-catalog.js
 */

/*
 * START OF FILE - /web/web/resources/scripts/business/catalog/vnm.group-po-auto-catalog.js
 */
var GroupPOAuto = {
		xhrSave : null,
		_xhrDel: null,
		id:null,
		_mapMutilSelect : null,
		_mapMultiSelectSub:null,
		_mapNode:null,
		_mapCheck:null,
		getGridUrl: function(groupCode,groupName,status){
			return "/catalog/group-po/search?groupCode=" + encodeChar(groupCode) + "&groupName=" + encodeChar(groupName) +  "&status=" + status;
		},
		getGridProductUrl:function(groupId,objectType){
			return "/catalog/group-po/searchListProduct?idGroup=" + groupId + "&objectType=" +objectType;
		},
		getGridSectorUrl:function(objectType, name, code){
			
			return "/catalog/group-po/searchListSector?objectType=" +objectType + "&name=" + encodeChar(name)+ "&code="+ encodeChar(code);
		},
		search: function(){
			GroupPOAuto.reset();
			$('#errMsg').html('').hide();
			var groupCode = $('#groupCode').val().trim();
			var groupName = $('#groupName').val().trim();
			var status = $('#status').val();
			var tm = setTimeout(function() {
				$('#groupCode').focus();
			}, 500);
			var url = GroupPOAuto.getGridUrl(groupCode,groupName,status);
			$("#grid").datagrid({url:url,pageNumber:1});
			return false;
		},
		getListProduct:function(groupId){
			var objectType =2 ; 
			
		},
		deleteRow: function(groupDetailId){
			$('#errMsg').html('').hide();
			var dataModel = new Object();
			dataModel.groupDetailId = groupDetailId;	
			Utils.deleteSelectRowOnGrid(dataModel, 'sản phẩm', "/catalog/group-po/deleteProduct", GroupPOAuto._xhrDel, null, null,function(data){
				if(data.error == false){
					//var url = GroupPOAuto.getGridUrl(GroupPOAuto.id,2);
					//$("#gridProduct").datagrid({url:url,page:1});
					$("#gridProduct").datagrid("reload");
				}
			});
			return false;		
		},
	
		searchCat:function(id,objectType){
			$('#idCode1').val(id);
			GroupPOAuto.id = id;
			GroupPOAuto.reset();
			$('#menuSecGoods').show();
			var url = GroupPOAuto.getGridProductUrl(id,objectType);
			
			var data = "<a href='javascript:void(0)' onclick='return GroupPOAuto.openDialogUpdateSecGoods();'><img src='/resources/images/icon_add.png'/></a>";
			var title = getFormatterControlGrid('btnAddGrid1', data);
			
			$("#gridSecGoods").datagrid({
				  url:url,
			  	  pageList  : [10,20,30],
			  	  pageNumber : 1, 
				  width: 550,
				  pageSize : 10,
				  checkOnSelect :true,
				  height:'auto',
				  scrollbarSize : 0,
				  pagination:true,			  
				  fitColumns:true,		     
			      singleSelect:true,
				  method : 'GET',
				  rownumbers: true,
				  columns:[[	
					{field: 'code', title: 'Mã NH', width: 150, sortable:false,resizable:false, align: 'left', formatter: function(value, row, index){
				    	return Utils.XSSEncode(value);
				    }},
				    {field: 'name', title: 'Tên NH', width: 300, sortable:false,resizable:false, align: 'left', formatter: function(value, row, index){
				    	return Utils.XSSEncode(value);
				    }},
				    {field:'delete', title: title, width: 50, align: 'center',sortable:false,resizable:false, formatter: POAutoGroupFormatter.delCellIconFormatterCat},	   
				  ]],
				  onLoadSuccess:function(){
						$('.datagrid-header-rownumber').html('STT');  
				      	$('#gridSecGoods').datagrid('resize');
				      }
			});
			return false;
		},
		searchSubCat:function(id,objectType){
			$('#idCode2').val(id);
			GroupPOAuto.id = id;
			GroupPOAuto.reset();
			$('#menuSecGoodsChild').show();
			var url = GroupPOAuto.getGridProductUrl(id,objectType);
			
			var data = '<a href="javascript:void(0)" onclick="return GroupPOAuto.openDialogUpdateSecGoodsBrand();"><img src="/resources/images/icon_add.png"/></a>';
			var title = getFormatterControlGrid('btnAddGrid2', data);
			
			$("#gridSecGoodsBrand").datagrid({
				  url:url,
			  	  pageList  : [10,20,30],
			  	  pageNumber: 1,
				  width: 550,
				  pageSize : 10,
				  checkOnSelect :true,
				  height:'auto',
				  scrollbarSize : 0,
				  pagination:true,			  
				  fitColumns:true,		     
			      singleSelect:true,
				  method : 'GET',
				  rownumbers: true,
				  columns:[[
				            //here
				    {field: 'parentCode', title: 'Mã NH', width: 150, sortable:false,resizable:false, align: 'left', formatter: function(value, row, index){
				    	return Utils.XSSEncode(value);
				    }},
					{field: 'code', title: 'Mã NHC', width: 150, sortable:false,resizable:false, align: 'left', formatter: function(value, row, index){
				    	return Utils.XSSEncode(value);
				    }},
				    {field: 'name', title: 'Tên NHC', width: 300, sortable:false,resizable:false, align: 'left', formatter: function(value, row, index){
				    	return Utils.XSSEncode(value);
				    }},
				    {field:'delete', title: title, width: 50, align: 'center',sortable:false,resizable:false, formatter: POAutoGroupFormatter.delCellIconFormatterSubCat},	   
				  ]],
				  onLoadSuccess:function(){
						$('.datagrid-header-rownumber').html('STT');  
				      	$('#gridSecGoodsBrand').datagrid('resize');
				      }
			});
			return false;
		},
		searchProduct:function(id,objectType){
			GroupPOAuto.id = id;
			GroupPOAuto.reset();
			$('#menuProduct').show();
			var url = GroupPOAuto.getGridProductUrl(id,objectType);
			
			var data = "<a href='javascript:void(0)' onclick=\'return GroupPOAuto.openPopupProduct("+id +","+objectType+");\'><img src='/resources/images/icon_add.png'/></a>";
			var title = getFormatterControlGrid('btnAddGrid3', data);
			
			$("#gridProduct").datagrid({
				  url:url,
				  pageNumber: 1,
			  	  pageList  : [10,20,30],
				  width: 550,
				  pageSize : 10,
				  checkOnSelect :true,
				  height:'auto',
				  scrollbarSize : 0,
				  pagination:true,			  
				  fitColumns:true,		     
			      singleSelect:true,
				  method : 'GET',
				  rownumbers: true,
				  columns:[[	
					{field: 'code', title: 'Mã sản phẩm', width: 150, sortable:false,resizable:false, align: 'left', formatter: function(value, row, index){
				    	return Utils.XSSEncode(value);
				    }},
				    {field: 'name', title: 'Tên sản phẩm', width: 300, sortable:false,resizable:false, align: 'left', formatter: function(value, row, index){
				    	return Utils.XSSEncode(value);
				    }},
				    {field:'delete', title: title, width: 50, align: 'center',sortable:false,resizable:false, formatter: POAutoGroupFormatter.delCellIconFormatter},	   
				  ]],
				  onLoadSuccess:function(){
						$('.datagrid-header-rownumber').html('STT');  
				      	$('#gridProduct').datagrid('resize');
				      }
			});
			return false;
		},
		reset:function(){
			$('#menuSecGoods').hide();
			$('#menuSecGoodsChild').hide();
			$('#menuProduct').hide();
		},
		searchProductTree:function(){
			var code = $("#productCode").val().trim();
			var name = 	$("#productName").val().trim();
			var nodes = $('#tree').tree('getChecked');
			for(var i=0; i<nodes.length; i++){
				if(nodes[i].attributes.productTreeVO.type == "PRODUCT"){
					GroupPOAuto._mapCheck.put(nodes[i].id,nodes[i].id);
		    		}else{
		    			GroupPOAuto._mapCheck.remove(nodes[i].id);
		    		}
			}
			$(".easyui-dialog #errMsgProductDlg").html('').hide();
			var groupPOAutoId = GroupPOAuto.id; //id groupPOAuto
			$("#tree").tree({
				url: '/rest/catalog/group-po/tree/0.json',  
				method:'GET',
	            animate: true,
	            checkbox:true,
	            onBeforeLoad:function(node,param){
	            	$("#tree").css({"visibility":"hidden"});
//	            	$("#loadding").css({"visibility":"visible"});
	            	$("#rpSection").html('<img id="loadding" class="LoadingStyle" src="/resources/images/loadingbig.gif" width="30" height="30" alt="Loadding"/ >');
	            	$("#treeNoResult").html('');
	            	param.code = code;
	            	param.name = name;
	            	param.id = groupPOAutoId;
	            },
	            onLoadSuccess:function(node,data){
	            	$("#tree").css({"visibility":"visible"});
	            	$("#rpSection").html('');
	            	if(data.length == 0){
	            		$(".easyui-dialog #errMsgProductDlg").html('Không có kết quả').show();
	            	}
	            	var arr  = GroupPOAuto._mapCheck.keyArray;
	            	if(arr.length > 0){
	            		for(var i = 0;i < arr.length; i++){
	            			var nodes = $('#tree').tree('find', arr[i]);
	            			if(nodes != null){
	            				$("#tree").tree('check',nodes.target);
	            			}
	            		}
	            	}
	            },
	            onCheck:function(node,check){ 
	            	var nodes = $('#tree').tree('getChecked');
            	if(node.attributes.productTreeVO.type == "PRODUCT"){
            			GroupPOAuto._mapCheck.put(node.id,node.id);
            		}else{
            			GroupPOAuto._mapCheck.remove(node.id);
            		}
	            }
			});
		},
		openPopupProduct:function(id,objectType){
			$("#productCode").val('');
			$("#productName").val('');
			$(".easyui-dialog #errMsgProductDlg").html('').hide();
			//GroupPOAuto.searchProductTree(id);
			GroupPOAuto._mapNode = new Map();
			GroupPOAuto._mapCheck = new Map();
			$("#popupProductSearch").dialog({
				onOpen:function(){
					$("#tree").tree({
						url: '/rest/catalog/group-po/tree/0.json',  
						method:'GET',
			            animate: true,
			            checkbox:true,
			            onBeforeLoad:function(node,param){
			            	$("#tree").css({"visibility":"hidden"});
			            	$("#rpSection").html('<img id="loadding" class="LoadingStyle" src="/resources/images/loadingbig.gif" width="30" height="30" alt="Loadding"/ >');
			            	$("#treeNoResult").html('');
			            	param.code = '';
			            	param.name = '';
			            	param.id = GroupPOAuto.id;
			            },
			            onLoadSuccess:function(node,data){
			            	$("#tree").css({"visibility":"visible"});
			            	$("#rpSection").html('');
			            	$("#productCode").focus();
			            	if(data.length == 0){
			            		$(".easyui-dialog #errMsgProductDlg").html('Không có kết quả').show();
			            	}
			            	var arr  = GroupPOAuto._mapCheck.keyArray;
			            	if(arr.length > 0){
			            		for(var i = 0;i < arr.length; i++){
			            			var nodes = $('#tree').tree('find', arr[i]);
			            			if(nodes != null){
			            				$("#tree").tree('check',nodes.target);
			            			}
			            		}
			            	}
			            },
			            onCheck:function(node,check){ 
			            	var nodes = $('#tree').tree('getChecked');
				            	if(node.attributes.productTreeVO.type == "PRODUCT"){
				            			GroupPOAuto._mapCheck.put(node.id,node.id);
				            		}else{
				            			GroupPOAuto._mapCheck.remove(node.id);
				            		}
			            }
					});
				}
			
			});
			$("#popupProductSearch").dialog('open');
		},
		createGroupPOAutoDetail:function(){
			var dataModel = new Object();
			var nodes = $('#tree').tree('getChecked');
			var lstId = '';  
			var lstIdCustomer = new Array();
			$('#errMsgPopup3').html('').hide();
			if(nodes.length == 0 ) {
				$('#errMsgPopup3').html('Bạn chưa chọn sản phẩm nào.').show();
        		return false;
        	}
	        for(var i=0; i<nodes.length; i++){
	        	if(nodes[i].attributes.productTreeVO.type != "PRODUCT"){
	        		continue;
	        	}
	        	lstIdCustomer.push(nodes[i].attributes.productTreeVO.id);  
	        }
			if(lstIdCustomer == undefined || lstIdCustomer == null || lstIdCustomer.length <=0){
				$(".easyui-dialog #errMsgProductDlg").html('Không có kết quả').show();
				return false;
			}
			dataModel.objectType = 2;
			dataModel.id = GroupPOAuto.id;
			dataModel.listOjectId = lstIdCustomer;
			Utils.addOrSaveRowOnGrid(dataModel, "/catalog/group-po/save", GroupPOAuto._xhrSave, null, null,function(data){
				$("#popupProductSearch").dialog("close");
				if(data.error == false){
					$("#gridProduct").datagrid("reload");
				}
			});
			return false;

		},
		

		openDialogUpdateSecGoods:function(codeText, nameText, title, url, callback,	codeFieldText, nameFieldText, arrParam, objectStaff){
			$('#popupSecFoods').css('visibility','visible');
			$('#popup1').dialog('open');
			$('#codeSecGoods').val('');
			$('#nameSecGoods').val('');
			$('#codeSecGoods').focus();
			GroupPOAuto._mapMutilSelect = new Map();
			$('#errMsgPopup1').hide();
			var url = GroupPOAuto.getGridSectorUrl(0, null, null);
			$("#gridSecGoodsPop").datagrid({
				  url:url,
			  	  pageList  : [10,20,30],
				  width: 550,
				  pageSize : 10,
				  checkOnSelect :true,
				  height:'auto',
				  scrollbarSize : 0,
				  pagination:true,
				  pageNumber:1,
				  fitColumns:true,		     
				  method : 'GET',
				  rownumbers: true,
				  columns:[[	
					{field: 'productInfoCode', title: 'Mã NH', width: 150, sortable:false,resizable:false, align: 'left', formatter: function(value, row, index){
				    	return Utils.XSSEncode(value);
				    }},
				    {field: 'productInfoName', title: 'Tên NH', width: 300, sortable:false,resizable:false, align: 'left', formatter: function(value, row, index){
				    	return Utils.XSSEncode(value);
				    }},
				    {field: 'productInfoId',title: '', checkbox:true, width:50,align:'center',sortable : false,resizable : false},
				    
				  ]],
				  	onCheck : function(rowIndex, rowData) {
				    	var selectedId = rowData.productInfoId;
				    	GroupPOAuto._mapMutilSelect.put(selectedId, rowData);
				    },
				    onUncheck : function(rowIndex, rowData) {
				    	var selectedId = rowData.productInfoId;
				    	GroupPOAuto._mapMutilSelect.remove(selectedId);
				    },
				    onCheckAll : function(rows) {
				    	if($.isArray(rows)) {
				    		for(var i = 0; i < rows.length; i++) {
				    			var row = rows[i];
				    			var selectedId = row.productInfoId;
				    			GroupPOAuto._mapMutilSelect.put(selectedId, row);
				    		}
				    	}
				    },
				    onUncheckAll : function(rows) {
				    	if($.isArray(rows)) {
				    		for(var i = 0; i < rows.length; i++) {
				    			var row = rows[i];
				    			var selectedId = row.productInfoId;
				    			GroupPOAuto._mapMutilSelect.remove(selectedId);
				    		}
				    	}
				    },
				   onLoadSuccess:function(data){
				      	var easyDiv = '#gridSecGoodsPopContainer ';
				    	$(easyDiv+'.datagrid-header-rownumber').html('STT');
				    	var exitsAll = false;
			    		var runner = 0;
			    		$(easyDiv+'input:checkbox[name=productInfoId]').each(function() {
			    			var selectedId = this.value;
			    			if(GroupPOAuto._mapMutilSelect.get(selectedId) != null || GroupPOAuto._mapMutilSelect.get(selectedId) != undefined) {
			    				exitsAll = true;
			    				$('#gridSecGoodsPop').datagrid('selectRow',runner);
			    			} 
			    			runner ++; 
			    		});
			    		if (exitsAll==false){
			    			$(easyDiv+'td[field=productInfoId] .datagrid-header-check input:checkbox').attr('checked', false);
				    	}
				    	
			    		var tabindex = -1;
						$('.InputTextStyle, select, input, button , li , tr, td, th, label, ul, a, img').each(function () {
							if (this.type != 'hidden') {
								$(this).attr("tabindex", tabindex);
								tabindex -=1;
							}
						});
						tabindex = 1;
			    		 $('.easyui-dialog input,.easyui-dialog select,.easyui-dialog button').each(function () {
				    		 if (this.type != 'hidden') {
					    	     $(this).attr("tabindex", '');
								 tabindex++;
				    		 }
						 });
			    		 if(data.rows.length == 0){
			    			 $('.datagrid-header-check input').removeAttr('checked');
			    		 }
			    		 $(easyDiv+'input:checkbox[name=productInfoId]').each(function(){
			    			 	var selectedId = $(this).val();
				 				var temp = GroupPOAuto._mapMutilSelect.get(selectedId);
				 				if(temp!=null) $(this).attr('checked','checked');
				 			});
				 	    	var length = 0;
				 	    	$(easyDiv+'input:checkbox[name=productInfoId]').each(function(){
				 	    		if($(this).is(':checked')){
				 	    			++length;
				 	    		}	    		
				 	    	});	    	
				 	    	if(data.rows.length==length){
				 	    		$(easyDiv+'td[field=productInfoId] .datagrid-header-check input:checkbox').attr('checked','checked');
				 	    	}else{
								$(easyDiv+'td[field=productInfoId] .datagrid-header-check input:checkbox').removeAttr('checked');
				 	    	}
				 	    	if(data.rows == null || data.rows == undefined || data.rows.length == 0){
				 	    		$(easyDiv+'td[field=productInfoId] .datagrid-header-check input:checkbox').removeAttr('checked');
				 	    	}
			    		updateRownumWidthForDataGrid('#gridSecGoodsPop');
			    		$(window).resize();
				      }
			});
			
			return false;
		},
		addCategory:function(){
			$('#errMsgPopup1').html('').hide();
			if(GroupPOAuto._mapMutilSelect == null || GroupPOAuto._mapMutilSelect.size() <= 0) {
				$('#errMsgPopup1').html('Bạn chưa chọn ngành hàng nào. Yêu cầu chọn').show();
        		return false;
        	}
	
			var mapKey = GroupPOAuto._mapMutilSelect.keyArray;
			var mapValue = GroupPOAuto._mapMutilSelect.valArray;
			var params = new Object();
			params.lstCategoryId = mapKey;
			GroupPOAuto.updateCategory(params.lstCategoryId);			
		},
		openDialogUpdateSecGoodsBrand:function(codeText, nameText, title, url, callback,	codeFieldText, nameFieldText, arrParam, objectStaff){
			$('#popupSecFoodsBrand').css('visibility','visible');
			$('#popup2').dialog('open');
			$('#errMsgPopup2').hide();
			GroupPOAuto._mapMutilSelectSub = new Map();
			$('#codeSecGoodsBrand').val('');
			$('#nameSecGoodsBrand').val('');
			$('#codeSecGoodsBrand').focus();
			var url = GroupPOAuto.getGridSectorUrl(1, null, null);
			$("#gridSecGoodsPopBrand").datagrid({
				  url:url,
			  	  pageList  : [10,20,30],
				  width: 550,
				  pageSize : 10,
				  checkOnSelect :true,
				  height:'auto',
				  scrollbarSize : 0,
				  pagination:true,
				  pageNumber:1,
				  fitColumns:true,
				  method : 'GET',
				  rownumbers: true,
				  columns:[[
				            //here
				    {field: 'parentCode', title: 'Mã NH', width: 150, sortable:false,resizable:false, align: 'left', formatter: function(value, row, index){
				    	return Utils.XSSEncode(value);
				    }},
					{field: 'productInfoCode', title: 'Mã NHC', width: 150, sortable:false,resizable:false, align: 'left', formatter: function(value, row, index){
				    	return Utils.XSSEncode(value);
				    }},
				    {field: 'productInfoName', title: 'Tên NHC', width: 300, sortable:false,resizable:false, align: 'left', formatter: function(value, row, index){
				    	return Utils.XSSEncode(value);
				    }},
				    {field: 'productInfoId',title: '', checkbox:true, width:50,align:'center',sortable : false,resizable : false},			  
				  ]],
				  onSelect : function(rowIndex, rowData) {
				    	var selectedId = rowData['productInfoId'];
				    	GroupPOAuto._mapMutilSelectSub.put(selectedId, rowData);
				    },
				    onUnselect : function(rowIndex, rowData) {
				    	var selectedId = rowData['productInfoId'];
				    	GroupPOAuto._mapMutilSelectSub.remove(selectedId);
				    },
				    onSelectAll : function(rows) {
				    	if($.isArray(rows)) {
				    		for(var i = 0; i < rows.length; i++) {
				    			var row = rows[i];
				    			var selectedId = row['productInfoId'];
				    			GroupPOAuto._mapMutilSelectSub.put(selectedId, row);
				    		}
				    	}
				    },
				    onUnselectAll : function(rows) {
				    	if($.isArray(rows)) {
				    		for(var i = 0; i < rows.length; i++) {
				    			var row = rows[i];
				    			var selectedId = row['productInfoId'];
				    			GroupPOAuto._mapMutilSelectSub.remove(selectedId);
				    		}
				    	}
				    },
				   onLoadSuccess:function(){
				      	var easyDiv = '#gridSecGoodsPopBrandContainer ';
				    	$(easyDiv+'.datagrid-header-rownumber').html('STT');
				    	var exitsAll = false;
			    		var runner = 0;
			    		$(easyDiv+'input:checkbox[name=productInfoId]').each(function() {
			    			var selectedId = this.value;
			    			if(GroupPOAuto._mapMutilSelectSub.get(selectedId) != null || GroupPOAuto._mapMutilSelectSub.get(selectedId) != undefined) {
			    				exitsAll = true;
			    				$('#gridSecGoodsPopBrand').datagrid('selectRow',runner);
			    			} 
			    			runner ++; 
			    		});
			    		if (exitsAll==false){
			    			$(easyDiv+'td[field=productInfoId] .datagrid-header-check input:checkbox').attr('checked', false);
				    	}
				    	
			    		var tabindex = -1;
						$('.InputTextStyle, select, input, button , li , tr, td, th, label, ul, a, img').each(function () {
							if (this.type != 'hidden') {
								$(this).attr("tabindex", tabindex);
								tabindex -=1;
							}
						});
						tabindex = 1;
			    		 $('.easyui-dialog input,.easyui-dialog select,.easyui-dialog button').each(function () {
				    		 if (this.type != 'hidden') {
					    	     $(this).attr("tabindex", '');
								 tabindex++;
				    		 }
						 });
			    		updateRownumWidthForDataGrid('#gridSecGoodsPopBrand');
			    		$(window).resize();
				      }
			});
			
			return false;
		},
		addSubCategory:function(){
			$('#errMsgPopup2').html('').hide();
			if(GroupPOAuto._mapMutilSelectSub == null || GroupPOAuto._mapMutilSelectSub.size() <= 0) {
				$('#errMsgPopup2').html('Bạn chưa chọn ngành hàng con nào. Yêu cầu chọn').show();
        		return false;
        	}
	
			var mapKey = GroupPOAuto._mapMutilSelectSub.keyArray;
			var mapValue = GroupPOAuto._mapMutilSelectSub.valArray;
			var params = new Object();
			params.lstCategoryId = mapKey;
			GroupPOAuto.updateSubCategory(params.lstCategoryId);
		},		
		searchSecGoods: function(){
			$('#errMsgPopup1').html('').hide();
			$('#errMsg').html('').hide();
			var code = $('#codeSecGoods').val().trim();
			var name = $('#nameSecGoods').val().trim();
			var tm = setTimeout(function() {
				$('#codeSecGoods').focus();
			}, 500);
			var objectType = 0;
			var url = GroupPOAuto.getGridSectorUrl(objectType, name, code);
			$("#gridSecGoodsPop").datagrid({url:url,pageNumber:1});
//			$("#gridSecGoodsPop").datagrid({
//				  url:url,
//			  	  pageList  : [10,20,30],
//				  width: 550,
//				  pageSize : 10,
//				  checkOnSelect :true,
//				  height:'auto',
//				  scrollbarSize : 0,
//				  pagination:true,			  
//				  fitColumns:true,		     
//			      singleSelect:true,
//				  method : 'GET',
//				  rownumbers: true,
//				  columns:[[	
//					{field: 'productInfoCode', title: 'Mã NH', width: 150, sortable:false,resizable:false, align: 'left'},
//				    {field: 'productInfoName', title: 'Tên NH', width: 300, sortable:false,resizable:false, align: 'left'},
//				    {field: 'status',title: '<a href="javascript:void(0)" onclick="return GroupPOAuto.openDialogUpdateSecGoodsBrand();"></a>', width:50,align:'center',sortable : false,resizable : false,
//		            	formatter:function(value,row){
//		            		if(row.status == 1){
//		            			return '<input type="checkbox" checked="checked" onClick=""/>';
//		            		}else{
//		            			return '<input type="checkbox" onClick="" />';
//		            		}
//		            	}
//				    },	   
//				  ]],
//				  onLoadSuccess:function(){
//						$('.datagrid-header-rownumber').html('STT');  
//				      	$('#gridSecGoodsPop').datagrid('resize');
//				      }
//			});
			return false;
		},
		searchSecGoodsBrand: function(){
			$('#errMsgPopup2').html('').hide();
			$('#errMsg').html('').hide();
			var code = $('#codeSecGoodsBrand').val().trim();
			var name = $('#nameSecGoodsBrand').val().trim();
			var tm = setTimeout(function() {
				$('#codeSecGoodsBrand').focus();
			}, 500);
			var objectType = 1;
			var url = GroupPOAuto.getGridSectorUrl(objectType, name, code);
			$("#gridSecGoodsPopBrand").datagrid({url:url,pageNumber:1});
//			$("#gridSecGoodsPopBrand").datagrid({
//				  url:url,
//			  	  pageList  : [10,20,30],
//				  width: 550,
//				  pageSize : 10,
//				  checkOnSelect :true,
//				  height:'auto',
//				  scrollbarSize : 0,
//				  pagination:true,			  
//				  fitColumns:true,		     
//			      singleSelect:true,
//				  method : 'GET',
//				  rownumbers: true,
//				  columns:[[
//				    {field: 'parentCode', title: 'Mã NH', width: 150, sortable:false,resizable:false, align: 'left'},
//					{field: 'productInfoCode', title: 'Mã NHC', width: 150, sortable:false,resizable:false, align: 'left'},
//				    {field: 'productInfoName', title: 'Tên NHC', width: 300, sortable:false,resizable:false, align: 'left'},
//				    {field: 'status',title: '<a href="javascript:void(0)" onclick="return GroupPOAuto.openDialogUpdateSecGoodsBrand();"></a>', width:50,align:'center',sortable : false,resizable : false,
//		            	formatter:function(value,row){
//		            		if(row.status == 1){
//		            			return '<input type="checkbox" checked="checked" onClick=""/>';
//		            		}else{
//		            			return '<input type="checkbox" onClick="" />';
//		            		}
//		            	}
//				    }	   
//				  ]],
//				  onLoadSuccess:function(){
//						$('.datagrid-header-rownumber').html('STT');  
//				      	$('#gridSecGoodsPopBrand').datagrid('resize');
//				      }
//			});
			return false;

		},
		openDialogCreateGroupPOAuto:function(flag,id,groupCode,groupName,status){
			GroupPOAuto.reset();
			var arrayParam = new Array();
			arrayParam.push(flag);
			arrayParam.push(id);
			arrayParam.push(groupCode);
			arrayParam.push(groupName);
			arrayParam.push(status);
			CommonSearch.openSearchStyle1EasyUICreateUpdateGroupPOAuto(null,
					null, "Thông tin nhóm","groupCode",
					"groupName",'searchStyle1EasyUIDialogCreate','searchStyle1EasyUIDialogDiv',arrayParam);
//			$('#searchStyle1EasyUIDialogDiv #searchStyle1Code').focus();
		},
		openDialogUpdateGroupPOAuto:function(flag,id,groupCode,groupName,status){
			GroupPOAuto.reset();
			var arrayParam = new Array();
			arrayParam.push(flag);
			arrayParam.push(id);
			arrayParam.push(groupCode);
			arrayParam.push(groupName);
			arrayParam.push(status);
			CommonSearch.openSearchStyle1EasyUICreateUpdateGroupPOAuto(null,
					null, "Thông tin nhóm","groupCode",
					"groupName",'searchStyle1EasyUIDialogUpdate','searchStyle1EasyUIDialogDivUpdate',arrayParam);
//			$('#searchStyle1EasyUIDialogDivUpdate #searchStyle1Name').focus();
		},
		updateCategory:function(listCatID){
			var dataModel = new Object();
			dataModel.id = $('#idCode1').val();
			dataModel.lstCategoryId = listCatID;
			Utils.addOrSaveData(dataModel, "/catalog/group-po/addPOAutoGroupDetail", null, 'errMsg',function(data){	
				if(data.errMsg!=undefined && data.errMsg.length==0){
					$('#popup1').dialog('close');
					$("#gridSecGoods").datagrid("reload");
				}
			});
		},
		updateSubCategory:function(listCatID){
			var dataModel = new Object();
			dataModel.id = $('#idCode2').val();
			dataModel.lstCategoryId = listCatID;
			Utils.addOrSaveData(dataModel, "/catalog/group-po/addPOAutoGroupDetailSubCat", null, 'errMsg',function(data){	
				if(data.errMsg!=undefined && data.errMsg.length==0){
					$('#popup2').dialog('close');
					$("#gridSecGoodsBrand").datagrid("reload");
				}
			});
		},
		deleteCategoryRow: function(groupDetailId){
			$('#errMsg').html('').hide();
			var dataModel = new Object();
			dataModel.groupDetailId = groupDetailId;	
			Utils.deleteSelectRowOnGrid(dataModel, 'Ngành hàng', "/catalog/group-po/deleteCategoryRow", GroupPOAuto._xhrDel, null, null,function(data){
				if(data.error == false){
					$("#gridSecGoods").datagrid("reload");
				}
			});
			return false;		
		},
		deleteSubCategoryRow: function(groupDetailId){
			$('#errMsg').html('').hide();
			var dataModel = new Object();
			dataModel.groupDetailId = groupDetailId;	
			Utils.deleteSelectRowOnGrid(dataModel, 'Ngành hàng con', "/catalog/group-po/deleteSubCategoryRow", GroupPOAuto._xhrDel, null, null,function(data){
				if(data.error == false){
					$("#gridSecGoodsBrand").datagrid("reload");
				}
			});
			return false;		
		}
		
};
/*
 * END OF FILE - /web/web/resources/scripts/business/catalog/vnm.group-po-auto-catalog.js
 */

/*
 * START OF FILE - /web/web/resources/scripts/business/catalog/vnm.attributes-manager.js
 */
var AttributesManager = {
	PRODUCT : 0,
	CUSTOMER: 1,
	STAFF: 2,
	PROMOTION_PROGRAM: 3,
	FOCUS_PROGRAM: 4,	
	DISPLAY_PROGRAM: 5,
	DISPLAY_GROUP: 6,
	DISPLAY_PROGRAM_LEVEL:7,
	INCENTIVE_PROGRAM: 8,
	INCENTIVE_PROGRAM_LEVEL: 9,
	DP_PAY_PERIOD_RESULT: 10,
	_xhrSave: null,
	_xhrDelete: null,
	getAttributesForm: function(divId,type){	
		$('#tabAttribute').val(divId);
		var dataModel = new Object();
		dataModel.type = type;
		var kData = $.param(dataModel, true);
		$.ajax({
			type : 'GET',
			url : '/attributes/list',
			data :(kData),
			dataType : "html",
			success : function(data) {
				$('#' + divId).html(data);
				$('#' + divId +' .MySelectBoxClass').customStyle();
				Utils.bindAutoSearch();
			},
			error:function(XMLHttpRequest, textStatus, errorThrown) {
			}});
	},
	getGridUrl: function(code,name,columnType,status, type){
		return "/attributes/search?code=" + encodeChar(code) + "&name=" + encodeChar(name) +"&columnType=" + columnType + "&status=" + status + '&type=' + type;
	},
	getListAttribute: function(){
		var divId = $('#tabAttribute').val();
		var type = $('#typeAttribute').val();
		var url = AttributesManager.getGridUrl('', '', activeType.ALL, activeType.RUNNING, type);
		$("#" + divId + " #attributeGrid").jqGrid({
			  url:url,
			  colModel:[		
			    {name:'attributeCode',index:'attributeCode', label: 'Mã thuộc tính', width: 100, sortable:false,resizable:false , align: 'left'},
			    {name:'attributeName', index:'attributeName',label: 'Tên thuộc tính', sortable:false,resizable:false, align:'left' },
			    {name:'note',index:'note', label: 'Mô tả', sortable:false,resizable:false, align:'left' },
			    {name:'columnName',index:'columnName', label: 'Tên trường', sortable:false,resizable:false, align:'left' },
			    {name:'columnType',index:'columnType', label: 'Loại dữ liệu', sortable:false,resizable:false, align:'left', formatter: AttributeManagerFormatter.columnTypeCellIconFormatter},
			    {name:'columnValueType',index:'columnValueType', label: 'Kiểu dữ liệu', sortable:false,resizable:false, align:'left',formatter: AttributeManagerFormatter.columnValueTypeCellIconFormatter },
			    {name:'status',index:'status', label: 'Trạng thái', sortable:false,resizable:false, align:'left', formatter: GridFormatterUtils.statusCellIconFormatter },
			    {name:'edit', label: 'Sửa', width: 50, align: 'center',sortable:false,resizable:false, formatter: AttributeManagerFormatter.editCellIconFormatter},
			    {name:'listValue', label: 'DS giá trị', width: 50, align: 'center',sortable:false,resizable:false, formatter: AttributeManagerFormatter.listValueCellIconFormatter},
			    {name:'id', index:'id',hidden: true},			    
			  ],	  
			  pager : '#attributePager',
			  width: ($("#" + divId + " #attributeContainer").width())	  
			})
			.navGrid('#attributePager', {edit:false,add:false,del:false, search: false});
		$('#jqgh_attributeGrid_rn').prepend('STT');
	},
	searchAttribute: function(){
		$('#errExcelMsg').html('').hide();
		var divId = $('#tabAttribute').val();
		var type = $('#typeAttribute').val();
		
		var code = $('#' + divId + ' #searchForm #attributeCode').val().trim();
		var name = $('#' + divId + ' #searchForm #attributeName').val().trim();				
		var columnType = $('#' + divId + ' #searchForm #columnType').val().trim();
		var status = $('#' + divId + ' #searchForm #attributeStatus').val().trim();
		var url = AttributesManager.getGridUrl(code, name, columnType, status, type);
		$('#' + divId + ' #attributeGrid').setGridParam({url:url,page:1}).trigger("reloadGrid");
		var dataModel = new Object();
		dataModel.code = code;
		dataModel.name = name;
		dataModel.columnType = columnType;
		dataModel.status = status;
		dataModel.type = type;
		dataModel.listAll = 1;
		var kData = $.param(dataModel, true);
		$.ajax({
			type : 'GET',
			url : '/attributes/search',
			data :(kData),
			dataType : "json",
			success : function(data) {
				if(data!= null && data!= undefined){
					var target = $('#' + divId + " #listAttr"); 
					if (target.hasTemplate() == 0) {
						target.setTemplateURL("/resources/html/catalog/attributeItem.html"+ "?v=" + svn_revision_number);						
					}
					if (target.hasTemplate() != 0) {							
						target.processTemplate(data);
						$('#' + divId + ' #srlLstAttr').jScrollPane();
					}
				}
				
			},
			error:function(XMLHttpRequest, textStatus, errorThrown) {
				
			}});
		return false;
	},
	saveOrUpdate: function(){
		$('#errExcelMsg').html('').hide();
		var divId = $('#tabAttribute').val();
		var type = $('#typeAttribute').val();
		var msg = '';
		$('#errMsgAttr').html('').hide();
		msg = Utils.getMessageOfRequireCheck(divId + ' #createForm #attributeCode','Mã thuộc tính');
		if(msg.trim().length == 0){
			msg = Utils.getMessageOfSpecialCharactersValidate(divId + ' #createForm #attributeCode','Mã thuộc tính',Utils._CODE);
		}
		if(msg.trim().length == 0){
			msg = Utils.getMessageOfRequireCheck(divId + ' #createForm #attributeName','Tên thuộc tính');
		}
		if(msg.trim().length == 0){
			msg = Utils.getMessageOfSpecialCharactersValidate(divId + ' #createForm #attributeName','Tên thuộc tính');
		}
		if(msg.trim().length == 0){
			msg = Utils.getMessageOfRequireCheck(divId + ' #createForm #columnType','Loại dữ liệu',true);
		}
		if(msg.trim().length == 0){
			msg = Utils.getMessageOfRequireCheck(divId + ' #columnName','Tên trường',true);
		}
		if(msg.trim().length == 0){
			msg = Utils.getMessageOfRequireCheck(divId + ' #columnValueType','Kiểu dữ liệu',true);
		}
		if(msg.trim().length == 0){
			msg = Utils.getMessageOfRequireCheck(divId + ' #createForm #attributeStatus','Trạng thái',true);
		}
		if(msg.trim().length == 0){
			msg = Utils.getMessageOfSpecialCharactersValidate(divId + ' #note','Mô tả');
		}
		if(msg.length > 0){
			$('#errMsgAttr').html(msg).show();
			return false;
		}
		var dataModel = new Object();
		dataModel.id = $('#' + divId + ' #selAttrId').val().trim();
		dataModel.code = $('#' + divId + ' #createForm #attributeCode').val().trim();
		dataModel.name = $('#' + divId + ' #createForm #attributeName').val().trim();
		dataModel.note = $('#' + divId + ' #note').val().trim();
		dataModel.status = $('#' + divId + ' #createForm #attributeStatus').val();
		dataModel.columnType = $('#' + divId + ' #createForm #columnType').val();
		dataModel.columnName = $('#' + divId + ' #columnName').val();
		dataModel.columnValueType = $('#' + divId + ' #columnValueType').val();		
		dataModel.type = type;
		Utils.addOrSaveRowOnGrid(dataModel, "/attributes/save", AttributesManager._xhrSave, null, 'errMsgAttr', function(data){
			if(!data.error){
				AttributesManager.clearData();
			}
		});		
		return false;		
	},
	clearData: function(){
		var divId = $('#tabAttribute').val();		
		setTitleSearch();		
		$('#' + divId + ' #createForm').hide();
		$('#' + divId + ' #searchForm').show();
		$('#' + divId + ' #searchForm #attributeCode').focus();
		$('#' + divId + ' #selAttrId').val(0);
		$('#' + divId + ' #attributeCode').removeAttr('disabled');
		$('#' + divId + ' #columnName').removeAttr('disabled');
		$('#' + divId + ' #columnType').removeAttr('disabled');
		setSelectBoxValue(divId + ' #searchForm #attributeStatus',activeType.RUNNING);
		$('.disable').removeClass('BoxDisSelect');
		AttributesManager.searchAttribute();
		Utils.bindAutoSearch();
	},
	showSearchForm: function(){
		$('#errExcelMsg').html('').hide();
		$('#errMsgAttr').html('').hide();
		var divId = $('#tabAttribute').val();		
		$('#' + divId + ' #createForm').hide();
		$('#' + divId + ' #searchForm').show();
		$('#' + divId + ' #attributeCode').removeAttr('disabled');
		$('#' + divId + ' #columnName').removeAttr('disabled');
		$('#' + divId + ' #columnType').removeAttr('disabled');
		setSelectBoxValue(divId + ' #searchForm #attributeStatus',activeType.RUNNING);
		$('.disable').removeClass('BoxDisSelect');
		setTitleSearch('titleAttr');
		AttributesManager.searchAttribute();
	},
	showNewOrUpdateForm: function(){
		$('#errExcelMsg').html('').hide();
		$('#errMsgAttr').html('').hide();
		var divId = $('#tabAttribute').val();		
		$('#' + divId + ' #searchForm').hide();
		$('#' + divId + ' #createForm').show();
		$('#' + divId + ' #createForm #attributeCode').val('');
		$('#' + divId + ' #createForm #attributeName').val('');
		setSelectBoxValue(divId + ' #createForm #columnType',activeType.ALL);
		setSelectBoxValue(divId + ' #createForm #attributeStatus',activeType.RUNNING);
		setSelectBoxValue(divId + ' #columnName',activeType.ALL);
		setSelectBoxValue(divId + ' #columnValueType',activeType.ALL);
		$('#' + divId + ' #note').val('');
		setTitleAdd('titleAttr');
		
	},
	getSelectedRow: function(rowId, columnType, columnValueType, status){		
		var divId = $('#tabAttribute').val();
		setSelectBoxValue(divId + ' #createForm #columnType',columnType);
		var id =  $('#' + divId + ' #attributeGrid').jqGrid ('getCell', rowId, 'id');
		var code =  $('#' + divId + ' #attributeGrid').jqGrid ('getCell', rowId, 'attributeCode');
		var name =  $('#' + divId + ' #attributeGrid').jqGrid ('getCell', rowId, 'attributeName');
		var columnName =  $('#' + divId + ' #attributeGrid').jqGrid ('getCell', rowId, 'columnName');
		var note =  $('#' + divId + ' #attributeGrid').jqGrid ('getCell', rowId, 'note');
		$('#' + divId + ' #selAttrId').val(id);
		$('#errExcelMsg').html('').hide();
		$('#' + divId + ' #searchForm').hide();
		$('#' + divId + ' #createForm').show();
		$('#' + divId + ' #createForm #attributeCode').val(code);
		$('#' + divId + ' #createForm #attributeName').val(name);
		$('#' + divId + ' #createForm #attributeCode').attr('disabled','disabled');		
		$('#' + divId + ' #createForm #columnType').attr('disabled','disabled');
		setSelectBoxValue(divId + ' #createForm #attributeStatus',status);
		setSelectBoxValue(divId + ' #columnValueType',columnValueType);	
		setTimeout(function(){
			AttributesManager.getListColumnName(columnType,'columnName',columnName);
		},300);
		$('#' + divId + ' #createForm #columnName').attr('disabled','disabled');
		$('.disable').addClass('BoxDisSelect');
		$('#' + divId + ' #note').val(note);
		setTitleUpdate('titleAttr');
	},
	getListValue: function(attrId,columnTypeValue){
		$('#dlgColumnTypeValue').val(columnTypeValue);
		var divId = $('#tabAttribute').val();		
		var html = $('#' + divId + ' #dlgListAttributeValue').html();
		var title = 'Danh sách giá trị';
		$.fancybox(html,{
			modal : true,
			title : title,						
			autoResize: false,
			afterShow : function() {				
				$('#' + divId + ' #dlgListAttributeValue').html('');
				$('#dlgSelAttrId').val(attrId);
				$('.fancybox-inner #attrDetailValue').focus();
				var url = '/attributes/list-value?id=' + attrId;
//				if($('#gbox_attrDetailGrid').html() != null && $('#gbox_attrDetailGrid').html().trim().length > 0){
//					$("#attrDetailGrid").setGridParam({url:url,page:1}).trigger("attrDetailGrid");
//				} else {
				$('#attrDetailGrid').jqGrid({
					url : url,
					colModel : [{name : 'attribute.attributeCode',index : 'attribute.attributeCode',align : 'left',label : 'Mã thuộc tính',sortable : false,resizable : false},
					            {name : 'attributeDetailValue',index : 'attributeDetailValue',label : 'Giá trị',sortable : false,resizable : false,align : 'left'},
					            {name : 'attributeDetailName',index : 'attributeDetailName',label : 'Tên',sortable : false,resizable : false,align : 'left'},
					            {name:'edit', label: 'Sửa', width: 50, align: 'center',sortable:false,resizable:false, formatter: AttributeManagerFormatter.editDetailCellIconFormatter},
					            {name:'edit', label: 'Xóa', width: 50, align: 'center',sortable:false,resizable:false, formatter: AttributeManagerFormatter.deleteDetailCellIconFormatter},
					            {name : 'id',index : 'id',hidden : true}
					            ],
					            pager : '#attrDetailPager',
								height : 'auto',rowNum : 10,
								width : ($('#attrDetailContainer').width()),
								gridComplete : function() {
									$('#jqgh_attrDetailGrid_rn').html('STT');
									$('#pg_attrDetailGridPager').css('padding-right','20px');
									updateRownumWidthForJqGrid('.fancybox-inner');
									$(window).resize();
								}												
							}).navGrid('#attrDetailGrid',{edit : false,add : false,del : false,search : false});
//				}
			},
			afterClose : function() {
				$('#' + divId + ' #dlgListAttributeValue').html(html);
			}
		});
	},
	getSelectedDetailRow: function(rowId){
		var divId = $('#tabAttribute').val();
		var id =  $('#attrDetailGrid').jqGrid ('getCell', rowId, 'id');
		var value = $('#attrDetailGrid').jqGrid ('getCell', rowId, 'attributeDetailValue');
		var name = $('#attrDetailGrid').jqGrid ('getCell', rowId, 'attributeDetailName');
		$('#selDetailId').val(id);
		$('#attrDetailValue').val(value);
		$('#attrDetailName').val(name);
		$('#btnSaveDetail span').text('Lưu');
	},
	deleteDetailValue: function(detailId){
		var dataModel = new Object();
		var type = $('#typeAttribute').val();
		dataModel.detailId = detailId;	
		dataModel.type = type;	
		Utils.deleteSelectRowOnGrid(dataModel, 'giá trị thuộc tính', '/attributes/delete-value', AttributesManager._xhrDelete, 'attrDetailGrid', 'errAttrValue',function(data){
			if(!data.error){
				$('#succAttrValue').html('Xóa dữ liệu thành công').show();
				var tm = setTimeout(function(){
					$('#succAttrValue').html('').hide();
					clearTimeout(tm);
				}, 3000);
				AttributesManager.clearDetailForm();
			}else{
				$('.fancybox-inner #errAttrValue').html(data.errMsg).show();
			}
		},null,'.fancybox-inner');
		return false;
	},
	clearDetailForm: function(){
		var divId = $('#tabAttribute').val();
		$('#selDetailId').val(0);
		$('#attrDetailValue').val('');
		$('#attrDetailName').val('');
		$('#btnSaveDetail span').text('Thêm');
		var url = '/attributes/list-value?id=' + $('#dlgSelAttrId').val();
		$('#attrDetailGrid').setGridParam({url:url,page:1}).trigger("reloadGrid");
//		$('#' + divId + '#attrDetailGrid').trigger('reload');
	},
	saveOrUpdateDetail: function(){
		var divId = $('#tabAttribute').val();
		var columnTypeValue = $('#dlgColumnTypeValue').val();
		var msg = '';
		$('#errAttrValue').html('').hide();
		msg = Utils.getMessageOfRequireCheck('attrDetailValue','Giá trị');
		if(msg.trim().length == 0){
			if(columnTypeValue == 1){
				msg = Utils.getMessageOfSpecialCharactersValidateEx('attrDetailValue','Giá trị',Utils._TF_NUMBER);
			}else if(columnTypeValue == 2){
				msg = Utils.getMessageOfSpecialCharactersValidate('attrDetailValue','Giá trị',Utils._CODE);
			}else{
				if(!Utils.isDate($('#attrDetailValue').val().trim())){
					msg = 'Giá trị không hợp lệ, vui lòng nhập theo định dạng dd/MM/yyyy';
					$('#attrDetailValue').focus();
				}
			}
		}
		if(msg.trim().length == 0){
			msg = Utils.getMessageOfRequireCheck('attrDetailName','Tên');
		}
		if(msg.trim().length == 0){
			msg = Utils.getMessageOfSpecialCharactersValidate('attrDetailName','Tên');
		}		
		if(msg.length > 0){
			$('#errAttrValue').html(msg).show();
			return false;
		}
		var dataModel = new Object();
		dataModel.detailId = $('#selDetailId').val().trim();
		dataModel.id = $('#dlgSelAttrId').val();
		dataModel.detailValue = $('#attrDetailValue').val().trim();
		dataModel.detailName = $('#attrDetailName').val().trim();		
		Utils.addOrSaveData(dataModel, "/attributes/save-value", AttributesManager._xhrSave,'errAttrValue', function(data){
			if(!data.error){
				$('#succAttrValue').html('Lưu dữ liệu thành công').show();
				var tm = setTimeout(function(){
					$('#succAttrValue').html('').hide();
					clearTimeout(tm);
				}, 3000);
				AttributesManager.clearDetailForm();
			}
		},null,'.fancybox-inner');		
		return false;
	},
	deleteAttribute: function(){
		$('#errExcelMsg').html('').hide();
		var divId = $('#tabAttribute').val();				
		$('#errMsgAttr').html('').hide();
		var arrId = new Array();
		$('#' + divId + ' #listAttr input[type=checkbox]').each(function(){
			if($(this).is(':checked')){
				var id= $(this).attr('id').replace('chk_attr_','');
				arrId.push(id);
			}
		});
		if(arrId.length == 0){
			$('#errExcelMsg').html('Bạn chưa chọn dữ liệu thuộc tính nào').show();			
		} else {
			var dataModel = new Object();
			dataModel.lstId = arrId;	
			Utils.deleteSelectRowOnGrid(dataModel, 'dữ liệu thuộc tính', '/attributes/delete', AttributesManager._xhrDelete, null, 'errMsgAttr',function(data){
				if(!data.error){
					for(var i=0;i<data.lstDeleted.length;i++){
						$('#tr_attr_' + data.lstDeleted[i]).remove();
						$('#srlLstAttr').jScrollPane();
					}
					AttributesManager.searchAttribute();
				}
			});
		}		
		return false;
	},
	exportAttribute: function(typeExport){
		$('#errExcelMsg').html('').hide();
		var divId = $('#tabAttribute').val();
		var type = $('#typeAttribute').val();
		var arrId = new Array();
		$('#' + divId + ' #listAttr input[type=checkbox]').each(function(){
			if($(this).is(':checked')){
				var id= $(this).attr('id').replace('chk_attr_','');
				arrId.push(id);
			}
		});
		if(arrId.length == 0){
			$('#errExcelMsg').html('Bạn chưa chọn dữ liệu thuộc tính nào').show();			
		} else {
			var dataModel = new Object();
			dataModel.lstId = arrId;
			dataModel.type = type;
			dataModel.listAll = typeExport;
			var kData = $.param(dataModel, true);
			$.messager.confirm('Xác nhận', 'Bạn có muốn xuất file Excel?', function(r){
				if (r){
					$.ajax({
						type : 'GET',
						url : '/attributes/export',
						data :(kData),
						dataType : "json",
						success : function(data) {
							if(data!= null && data!= undefined && data.downloadLink!= undefined){
								document.location.href = data.downloadLink;
								setTimeout(function(){ //Set timeout để đảm bảo file load lên hoàn tất
		                            CommonSearch.deleteFileExcelExport(data.downloadLink);
								},2000);
							}					
						},
						error:function(XMLHttpRequest, textStatus, errorThrown) {}});
				}
			});	
		}
	},
	importExcel: function(typeView){
		var divId = $('#tabAttribute').val();
		var type = $('#typeAttribute').val();
		var options = { 
				beforeSubmit: AttributesManager.beforeImportExcel,   
		 		success:      AttributesManager.afterImportExcel, 
		 		type: "POST",
		 		dataType: 'html',
		 		data:({typeView:typeView,type:type})
		 	}; 
		$('#' + divId + ' #importAttrFrm').ajaxForm(options);
		$('#' + divId + ' #importAttrFrm').submit();
	},	
	beforeImportExcel: function(){
		if(!previewImportExcelFile(document.getElementById("excelFileAttr"))){
			return false;
		}		
		$('#errExcelMsg').html('').hide();
		$('#successMsg').html('').hide();
		$('#divOverlay').show();
		//$('#imgOverlay').show();
		return true;
	},
	afterImportExcel: function(responseText, statusText, xhr, $form){
		$('#divOverlay').hide();
		//$('#imgOverlay').hide();				
		if (statusText == 'success') {
	    	$("#responseDiv").html(responseText);	    	
	    	if($('#errorExcel').html().trim() == 'true' || $('#errorExcelMsg').html().trim().length > 0){
	    		$('#errExcelMsg').html($('#errorExcelMsg').html()).show();
	    	} else {
	    		if($('#typeViewResponse').html().trim()=='1'){
	    			var html = $('#dlgViewAttrExcel').html();
	    			$.fancybox(html,
    					{
    						modal: true,
    						title: 'Danh sách dữ liệu từ excel',
    						afterShow: function(){ 
    							$('#dlgViewAttrExcel').html('');
    							$('.ColsTd2_'+(parseInt($('#lstSize').val())-1)).css('text-align','left');
    							$('.ColsTd2_'+(parseInt($('#lstSize').val())-1)).css('word-break','normal');
    							$('.classTable ').css('width',parseInt($('#lstSize').val())*150 + 50 + 'px');
    							$('.BoxGeneralTTitle ').css('width',parseInt($('#lstSize').val())*150 + 50 + 'px');
    							$('.BoxGeneralTBody ').css('width',parseInt($('#lstSize').val())*150 + 50 + 'px');
    							$('#srlLstAttrExcel').jScrollPane();
    							$('#scrollSectionId').jScrollPane().data().jsp;
    						},
    						afterClose: function(){    	
    							$('#dlgViewAttrExcel').html(html);
    						}
    					}
    				);
	    		} else {
	    			var totalRow = parseInt($('#totalRow').html().trim());
		    		var numFail = parseInt($('#numFail').html().trim());
		    		var fileNameFail = $('#fileNameFail').html();
		    		var mes = format(msgErr_result_import_excel,(totalRow - numFail),numFail);		    		
		    		if(numFail > 0){
		    			mes+= ' <a href="'+ fileNameFail +'">Xem chi tiết lỗi</a>';
		    		}
		    		$('#errExcelMsg').html(mes).show();
	    		}	    		
	    	}
//	    	$('#fakefilepc2').val('');
//			$('#excelFileAttr').val('');
	    }
	},
	getListColumnName: function(value,object,columnValue){
		columnValue = Utils.XSSEncode(columnValue);
		$.ajax({
			type : "POST",
			url : "/attributes/list/column-name",
			data : ({columnType : value,type:$('#tableNameType').val()}),
			dataType: "json",
			success : function(data) {
				var arrHtml = new Array();
				arrHtml.push('<option value="-2">--- Chọn tên trường ---</option>');
				var size = data.lstColumnName.length;
				for(var i=0;i < size; i++){
					arrHtml.push('<option value="'+ Utils.XSSEncode(data.lstColumnName[i].name) +'">'+ Utils.XSSEncode(data.lstColumnName[i].name) +'</option>');
				}
				if(columnValue != -2){
					arrHtml.push('<option value="'+ Utils.XSSEncode(columnValue) +'">'+ Utils.XSSEncode(columnValue) +'</option>');
				}
				$('#createForm #' + object).html(arrHtml.join(""));
				$('#createForm #' + object).val(Utils.XSSEncode(columnValue));
				$('#createForm #' + object).change();
			},
			error:function(XMLHttpRequest, textStatus, errorThrown) {
				$.ajax({
					type : "POST",
					url : '/check-session',
					dataType : "json",
					success : function(data) {
						$('#loading').css('visibility','hidden');
					},
					error: function(XMLHttpRequest, textStatus, errorThrown) {
						window.location.href = '/home';
					}
				});
			}
		});
	}
};
/*
 * END OF FILE - /web/web/resources/scripts/business/catalog/vnm.attributes-manager.js
 */

/*
 * START OF FILE - /web/web/resources/scripts/business/catalog/vnm.attributes-dynamic-manager.js
 */
/**
 * Quan ly thuoc tinh dong
 * @author liemtpt
 * @since 21/01/2015
 */
var AttributesDynamicManager = {
	_xhrDel:null,
	HTML : '',
	APPLY_OBJECT : {
		CUSTOMER:1,
		PRODUCT : 2,
		STAFF : 3
	},
	ATTRIBUTE_TYPE : {
		CHARACTER: 1,
	    NUMBER: 2,
	    DATE_TIME: 3,
		CHOICE: 4,
		MULTI_CHOICE: 5,
		LOCATION: 6
	},
	ACTIVE_TYPE : {
		STOP: 0,
	    RUNNING: 1
	},
	PRODUCT_ATTRIBUTE_ENUM_TYPE : {
		NO: 0,
	    YES: 1
	},
	_applyObjectPop: new Map(),
	/**
	 * load grid
	 * @author liemtpt
	 * @description: load danh sach thuoc tinh dong
	 * @createDate 22/01/2015
	 */
	loadGrid : function() {
		var attributeCode = $('#attributeCode').val().trim();
		var attributeName = $('#attributeName').val().trim();
		var applyObject = $('#applyObject').val().trim();
		var valueType = $('#valueType').val().trim();
		var status = $('#status').val().trim();
		var params = new Object();
		params.attributeCode = attributeCode;
		params.attributeName = attributeName;
		params.valueType = valueType;
		params.status = status;
		params.applyObject = applyObject;
		$("#attributeGrid").datagrid({
		  url:'/attributes/dynamic/search',
		  pageList  : [10,20,30],
		  height:'auto',
		  scrollbarSize : 0,
		  pagination:true,
		  fitColumns:true,
		  singleSelect  :true,
		  method : 'GET',
		  rownumbers: true,	  
		  width:  $(window).width() - 50,
		  queryParams:params,
		  columns:[[		
		    {field:'attributeCode', title: qltt_ma_thuoc_tinh, width: 110, sortable: true,resizable:true , align: 'left',
		    	formatter: function(val, row) {
		    		if (val != undefined && val != null) {
		    			return Utils.XSSEncode(val);
		    		}
		    		return '';
		    	}
		    },
		    {field:'attributeName', title: qltt_ten_thuoc_tinh, width: 180, sortable:true,resizable:true , align: 'left',
		    	formatter: function(val, row) {
		    		if (val != undefined && val != null) {
		    			return Utils.XSSEncode(val);
		    		}
		    		return '';
		    	}
		    },
		    {field:'applyObject', title: qltt_doi_tuong_ap_dung, width: 140, sortable:true,resizable:true , align: 'left',
		    	formatter: function(val, row) {
		    		if ($('#applyObject').val() != undefined && $('#applyObject').val() != null) {
		    			if($('#applyObject').val() == AttributesDynamicManager.APPLY_OBJECT.CUSTOMER){
		    				return qltt_doi_tuong_khachhang;
		    			}else if($('#applyObject').val() == AttributesDynamicManager.APPLY_OBJECT.PRODUCT){
		    				return qltt_doi_tuong_sanpham;
		    			}else if($('#applyObject').val() == AttributesDynamicManager.APPLY_OBJECT.STAFF){
		    				return qltt_doi_tuong_nhanvien;
		    			}
		    		}
		    		return '';
		    	}
		    },
		    {field:'mandatory', title: qltt_bat_buoc, width: 50, sortable:true,resizable:true , align: 'center',
		    	formatter: function(val, row) {
		    		if (val != undefined && val != null) {
		    			if(val == AttributesDynamicManager.PRODUCT_ATTRIBUTE_ENUM_TYPE.YES){
		    				return "<a id='"+row.attributeId+"' ><img src='/resources/images/icon-tick.png'/></a>";
						}else{
						}
		    		}
		    		return '';
		    	}
		    },
		    {field:'type', title: qltt_loai_gia_tri, width: 70, sortable:true,resizable:true , align: 'left',
		    	formatter: function(val, row) {
		    		if (val != undefined && val != null) {
		    			if(val == AttributesDynamicManager.ATTRIBUTE_TYPE.CHARACTER){
							return 'Chữ';
						}else if(val == AttributesDynamicManager.ATTRIBUTE_TYPE.NUMBER){
							return 'Số';
						}else if(val == AttributesDynamicManager.ATTRIBUTE_TYPE.CHOICE){
							return 'Chọn một';
						}else if(val == AttributesDynamicManager.ATTRIBUTE_TYPE.MULTI_CHOICE){
							return 'Chọn nhiều';
						}else if(val == AttributesDynamicManager.ATTRIBUTE_TYPE.DATE_TIME){
							return 'Ngày tháng';
						}else if(val == AttributesDynamicManager.ATTRIBUTE_TYPE.LOCATION){
							return 'Vị trí bản đồ';
						}
		    		}
		    		return '';
		    	}
		    },
		    {field:'displayOrder', title: qltt_thu_tu_hien_thi, width: 40, sortable:true,resizable:true , align: 'center',
		    	formatter: function(val, row) {
		    		if (val != undefined && val != null) {
		    			return Utils.XSSEncode(val);
		    		}
		    		return '';
		    	}
		    },
		    {field:'status', title: qltt_trang_thai, width: 80, sortable:true,resizable:true , align: 'left',
		    	formatter: function(val, row) {
		    		if (val != undefined && val != null) {
		    			if(val == AttributesDynamicManager.ACTIVE_TYPE.RUNNING){
		    				return hoat_dong;
		    			}else{
		    				return tam_ngung;
		    			}
		    		}
		    		return '';
		    	}
		    },
		    {field:'view', title: '', width: 40, align: 'center', sortable: false, resizable: true, formatter: function(val, row, index) {
		    		if (row.type == AttributesDynamicManager.ATTRIBUTE_TYPE.CHOICE || row.type == AttributesDynamicManager.ATTRIBUTE_TYPE.MULTI_CHOICE) {
		    			return '<a id="viewAttrDynamicMng" href="javascript:void(0);" onclick="AttributesDynamicManager.loadDetail('+row.attributeId+',\''+row.attributeCode+'\','+$('#applyObject').val()+','+row.status+')"><img src="/resources/images/icon-view.png"/></a>';
		    		}
		    		return '<img style="visibility: hidden " src="/resources/images/icon-view-space.png"/>';
		    	}
		    },
		    {field:'edit', title: '<a id="btnAddAttribute" title="'+Utils.XSSEncode(msgText2)+'"  href="javascript:void(0)" onclick= "AttributesDynamicManager.openAttributeDynamic();"><img src="/resources/images/icon_add.png"/></a>',width: 40, align: 'center',sortable:false,resizable:true,
		    	formatter: function(val, row, index) {
		    		return '<a href="javascript:void(0)" class="btnEditAttribute" onclick="AttributesDynamicManager.openAttributeDynamic('+index+');"><img src="/resources/images/icon-edit.png"/></a>';
		    	}
		    },
		    {field:'P', hidden : true}
		  ]],
		  onLoadSuccess: function() {
				$('.datagrid-header-rownumber').html(jsp_common_numerical_order);
				var lstHeaderStyle = ["attributeCode", "attributeName", "type", "displayOrder", "status"];
				Utils.updateCellHeaderStyleSort('gridContainer', lstHeaderStyle);
				var arrTmpViewLength =  $('#gridContainer td a[id^="viewAttrDynamicMng"]').length;
				if (arrTmpViewLength == 0) {
					$('#attributeGrid').datagrid("hideColumn", "view");
				}
				//Xu ly phan quyen
				var arrEdit =  $('#gridContainer td[field="edit"]');
				if (arrEdit != undefined && arrEdit != null && arrEdit.length > 0) {
				  for (var i = 0, size = arrEdit.length; i < size; i++) {
				  	$(arrEdit[i]).prop("id", "gr_table_td_edit_dstt_" + i);//Khai bao id danh cho phan quyen
					$(arrEdit[i]).addClass("cmsiscontrol");
				  }
				}
				Utils.functionAccessFillControl('gridContainer', function(data){
					//Xu ly cac su kien lien quan den cac control phan quyen
					var arrTmpLength =  $('#gridContainer td[id^="gr_table_td_edit_dstt_"]').length;
					var invisibleLenght = $('.isCMSInvisible[id^="gr_table_td_edit_dstt_"]').length;
					if (arrTmpLength > 0 && arrTmpLength != invisibleLenght) {
						$('#attributeGrid').datagrid("showColumn", "edit");
					} else {
						$('#attributeGrid').datagrid("hideColumn", "edit");
					}
				});
	      }
		});
	},
	/**
	 * Search
	 * @author liemtpt
	 * @description: Tim kiem lich nghi phep cua nhan vien
	 * @createDate 14/01/2015
	 */
	search: function() {
		$('#errMsg').html('').hide();
		$('#attributeDetailDataDiv').hide();
		var msg = '';
		var attributeCode = $('#attributeCode').val().trim();
		var attributeName = $('#attributeName').val().trim();
		var applyObject = $('#applyObject').val().trim();
		var valueType = $('#valueType').val().trim();
		var status = $('#status').val().trim();
		/*if(msg.length == 0){
			msg = Utils.getMessageOfInvalidFormatDate('fromDate','Từ ngày');
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfInvalidFormatDate('toDate','Đến ngày');
		}
		if(msg.length == 0 && !Utils.compareDate(fromDate, toDate)){
			msg = "Từ ngày phải là ngày trước hoặc cùng ngày với Đến ngày";
			$('#fromDate').focus();
		}
		if(msg.length > 0){
			$('#errMsg').html(msg).show();
			return false;
		}*/
		var params = new Object();
		params.attributeCode = attributeCode;
		params.attributeName = attributeName;
		params.valueType = valueType;
		params.status = status;
		params.applyObject = applyObject;
		$("#attributeGrid").datagrid('options').sortName = null; // search thi load lai sort binh thuong
		$('#attributeGrid').datagrid('load',params);
		return false;
	},	
	/**
	 * open attribute dynamic
	 * @author liemtpt
	 * @description: Mo popup thuoc tinh dong
	 * @createDate 22/01/2015
	 */
	openAttributeDynamic : function(index) {
		$('#popupAttribute #successMsg').html('').hide();
//		$('#popupAttributeDetail #successMsg').html('').hide();
//		$('#attributeDetailDataDiv').hide();
		var row = null;
		if(index!=undefined && index!=null && index>=0){
			row = $('#attributeGrid').datagrid('getRows')[index];
			console.log(row);
		}
		$('#popupAttribute').dialog({
			closed : false,
			cache : false,
			modal : true,
			width : 650,
			height : 350,//400,
			onOpen: function(){	
				var applyObject = $('#applyObject').val();
				$('#applyObjectPop').val(applyObject).change();
				if(row!=null){
					$('#popupAttribute #idDivStatusPop').show();
					//Khong cho cap nhat
					disableSelectbox('applyObjectPop','#popupAttribute');
					disableSelectbox('valueTypePop','#popupAttribute');
					disabled('attributeCodePop','#popupAttribute');
					//view du lieu
					$('#popupAttribute #statusPop').val(row.status).change();
					$('#popupAttribute #valueTypePop').val(row.type).change();
					setTextboxValue('attributeCodePop',row.attributeCode);
					setTextboxValue('attributeNamePop',row.attributeName);
					$('#popupAttribute #mandatoryPop').val(row.mandatory).change();
					setTextboxValue('displayOrderPop',row.displayOrder);
					setTextboxValue('descriptionPop',row.description);
					$('#popupAttribute #statusPop').val(row.status).change();
					if(row.type == AttributesDynamicManager.ATTRIBUTE_TYPE.CHARACTER){
						$('#popupAttribute #idDivChoseText').show();
						setTextboxValue('dataLengthPop',row.dataLength);
						$('#popupAttribute #idDivChoseNumber').hide();
					}else if(row.type == AttributesDynamicManager.ATTRIBUTE_TYPE.NUMBER){
						$('#popupAttribute #idDivChoseText').show();
						$('#popupAttribute #idDivChoseNumber').show();
						setTextboxValue('dataLengthPop',row.dataLength);
						setTextboxValue('minValuePop',row.minValue);
						setTextboxValue('maxValuePop',row.maxValue);
					}else{
						$('#popupAttribute #idDivChoseText').hide();
						$('#popupAttribute #idDivChoseNumber').hide();
					}					
					$('#attributeIdHid').val(row.attributeId);
					$('#attributeNamePop').focus();
				}else{
					$('#attributeIdHid').val(0);
					$('#popupAttribute #idDivStatusPop').hide();
					$('#attributeCodePop').focus();
				}
			},
		 	onBeforeClose: function() {

		 	},
	        onClose : function(){
	        	//Khong cho cap nhat
	        	enableSelectbox('applyObjectPop','#popupAttribute');
				enableSelectbox('valueTypePop','#popupAttribute');
				enable('attributeCodePop','#popupAttribute');
				//view du lieu
				$('#popupAttribute #valueTypePop').val(AttributesDynamicManager.ATTRIBUTE_TYPE.CHOICE).change();
				setTextboxValue('attributeCodePop','');
				setTextboxValue('attributeNamePop','');
				$('#popupAttribute #mandatoryPop').val(AttributesDynamicManager.PRODUCT_ATTRIBUTE_ENUM_TYPE.NO).change();
				setTextboxValue('displayOrderPop','');
				setTextboxValue('descriptionPop','');
				$('#popupAttribute #statusPop').val(AttributesDynamicManager.ACTIVE_TYPE.RUNNING).change();
				setTextboxValue('dataLengthPop','');
				setTextboxValue('minValuePop','');
				setTextboxValue('maxValuePop','');
	        	$('#popupAttribute #errMsg').hide();
	        }
		});
	},
	
	/**
	 * save attribute 
	 * @author liemtpt
	 * @description: tao moi va cap nhat thuoc tinh dong
	 * @createDate 22/01/2015
	 */
	saveAttribute : function() {
		var msg = '';
		var valueTypePop = $('#valueTypePop').val().trim();
		$('#popupAttribute #errMsg').html('').hide();
		if (msg.length == 0) {
			msg = Utils.getMessageOfRequireCheck('attributeCodePop', qltt_ma_thuoc_tinh);
		}
		if(msg.length ==0){
			msg = Utils.getMessageOfSpecialCharactersValidate('attributeCodePop', qltt_ma_thuoc_tinh,Utils._CODE);
		}
		if (msg.length == 0){
			msg = Utils.getMessageOfRequireCheck('attributeNamePop', qltt_ten_thuoc_tinh);
		}
		if(msg.length ==0){
			msg = Utils.getMessageOfSpecialCharactersValidate('attributeNamePop', qltt_ten_thuoc_tinh,Utils._ADDRESS);
		}
		
		var attributeId = $('#popupAttribute #attributeIdHid').val().trim();
		var attributeCode = $('#popupAttribute #attributeCodePop').val().trim();
		var attributeName = $('#popupAttribute #attributeNamePop').val().trim();
		var applyObject = $('#popupAttribute #applyObjectPop').val().trim();
		var valueType = $('#popupAttribute #valueTypePop').val().trim();
		var mandatory = $('#popupAttribute #mandatoryPop').val().trim();
		var displayOrder = $('#popupAttribute #displayOrderPop').val().trim();
		var description = $('#popupAttribute #descriptionPop').val().trim();
		var params = new Object();
//		if(attributeId!= null && attributeId > 0){
			var status = $('#popupAttribute #statusPop').val().trim();
			params.status = status;
			if(valueType == AttributesDynamicManager.ATTRIBUTE_TYPE.CHARACTER){
				var dataLength = $('#popupAttribute #dataLengthPop').val();
				if(dataLength<0 || dataLength >2000){
					msg = qltt_do_dai_toi_da_chu;
				}
				params.dataLength = dataLength;
			}else if(valueType == AttributesDynamicManager.ATTRIBUTE_TYPE.NUMBER){
				var dataLength = $('#popupAttribute #dataLengthPop').val();
				if(dataLength < 0 || dataLength >8){
					msg = qltt_do_dai_toi_da_so;
				}
				var minValue = $('#popupAttribute #minValuePop').val();
				var maxValue = $('#popupAttribute #maxValuePop').val();
				if(Number(minValue) > Number(maxValue) && msg.length ==0){
					msg = qltt_js_msg_tu_den;
					$('#popupAttribute #minValuePop').focus();
				}
				params.dataLength = dataLength;
				params.minValue = minValue;
				params.maxValue = maxValue;
			}
//		}	
		if (msg.length > 0) {
			$('#popupAttribute #errMsg').html(msg).show();
			return false;
		}	
		params.applyObject = applyObject;
		params.attributeId = attributeId;
		params.attributeCode = attributeCode;
		params.attributeName = attributeName;
		params.type = valueType;
		params.mandatory = mandatory;
		params.displayOrder = displayOrder;
		params.description = description;
		//Gui yeu cau len server de luu du lieu
		var url = "/attributes/dynamic/save-attribute";
		params[Utils.VAR_NAME] = 'attributeDynamicVO';
		var dataModel = getSimpleObject(params);
		Utils.addOrSaveData(dataModel,url,null, 'errMsg', function(data){
			var tm = setTimeout(function(){
				$('.easyui-dialog').dialog('close'); 
				$("#attributeGrid").datagrid("reload");
				clearTimeout(tm);
			}, 2000);
			
		},null,'#popupAttribute',null,null,function(data){
			if(data.error != undefined  && data.error && data.errMsg != undefined){
				$('#popupAttribute #errMsg').html(data.errMsg).show();
			}
//			var errorMsg = data.errMsg;
//			setTimeout(function(){$('#errMsg').html('').hide();}, 3000);
		});
		return false;
	},
	
	/**
	 * load detail 
	 * @author liemtpt
	 * @description: load danh sach gia tri cua tung thuoc tinh dong
	 * @createDate 22/01/2015
	 */
	loadDetail : function(attributeId,attributeCode,applyObject,status) {
		$('#attributeDetailDataDiv').html(AttributesDynamicManager.HTML);		
		$('#attributeDetailDataDiv').show();		
		$('#idTitleDetail').html(Utils.XSSEncode(qltt_danh_sach_gia_tri_cua_tt + ' ' + attributeCode));
		var params = new Object();
		params.attributeId = attributeId;
		params.applyObject = applyObject;

		var addTile = '';
		if(status == activeType.RUNNING){
			addTile = '<a href= "javascript:void(0);" id="btnGridDetailAdd" onclick= "AttributesDynamicManager.openAttributeDetailDynamic(null,'+applyObject+','+attributeId+');"><img src="/resources/images/icon_add.png" /></a>';
		}
		
		$('#attributeDetailGrid').datagrid({
			url: '/attributes/dynamic/load-attribute-detail',
			autoRowHeight : true,
			rownumbers : true, 
			checkOnSelect :true,
			pagination:true,
			rowNum : 10,
			fitColumns:true,
			pageList  : [10,20,30],
			scrollbarSize:0,
			width: $(window).width() - 350,
			queryParams:params,
		    columns:[[	        
			    {field:'enumCode', title: qltt_ma_gia_tri, width: 100, sortable:true,resizable:true , align: 'left', formatter: function(value, row, index){
			    	return Utils.XSSEncode(value);
			    }},
			    {field:'enumValue', title: qltt_ten_gia_tri, width:150,sortable:true,resizable:true , align: 'left', formatter: function(value, row, index){
			    	return Utils.XSSEncode(value);
			    }},
			    {field:'edit', title:addTile,width: 30, align: 'center',sortable:false,resizable:true,
			    	formatter: function(val, row, index) {
			    		var value= '';
			    		if(status == activeType.RUNNING){
			    			value += '<a href="javascript:void(0)" <a title="'+msgText6+'" class="btnEditAttribute" onclick="AttributesDynamicManager.openAttributeDetailDynamic('+index+','+applyObject+','+attributeId+');"><img src="/resources/images/icon-edit.png"/></a>';
			    		}		    		
			    		value += '   <a  <a title="'+msgText4+'"  href="javascript:void(0)" class="btnEditAttribute" onclick="AttributesDynamicManager.deleteAttributeDetail('+row.enumId+','+applyObject+');"><img src="/resources/images/icon_delete.png"/></a>';
			    		return value;
			    	}
			    },
		    ]],
		    onLoadSuccess :function(data){	    	
		    	$('.datagrid-header-rownumber').html(jsp_common_numerical_order);	    
		    	var lstHeaderStyle = ["enumCode", "enumValue"];
				Utils.updateCellHeaderStyleSort('attributeDetailDataGridSection2', lstHeaderStyle);
				//Xu ly phan quyen
				var arrEdit =  $('#gridContainer td[field="edit"]');
				if (arrEdit != undefined && arrEdit != null && arrEdit.length > 0) {
				  for (var i = 0, size = arrEdit.length; i < size; i++) {
				  	$(arrEdit[i]).prop("id", "gr_table_td_edit_dstt_ct_" + i);//Khai bao id danh cho phan quyen
					$(arrEdit[i]).addClass("cmsiscontrol");
				  }
				}
				Utils.functionAccessFillControl('gridContainer', function(data){
					//Xu ly cac su kien lien quan den cac control phan quyen
					var arrTmpLength =  $('#gridContainer td[id^="gr_table_td_edit_dstt_ct_"]').length;
					var invisibleLenght = $('.isCMSInvisible[id^="gr_table_td_edit_dstt_ct_"]').length;
					if (arrTmpLength > 0 && arrTmpLength != invisibleLenght) {
						$('#attributeDetailGrid').datagrid("showColumn", "edit");
					} else {
						$('#attributeDetailGrid').datagrid("hideColumn", "edit");
					}
				});
		    }
		});
		Utils.bindAutoSearchForEasyUI();
	},
	/**
	 * open attribute detail dynamic
	 * @author liemtpt
	 * @description: mo popup tao gia tri cho thuoc tinh dong
	 * @createDate 22/01/2015
	 */
	openAttributeDetailDynamic : function(index,applyObject,attributeId) {
		$('#popupAttribute #successMsg').html('').hide();
		$('#popupAttributeDetail #successMsg').html('').hide();
		$('#popupAttributeDetail #errMsg').html('').hide();
		var row = null;
		if(index!=undefined && index!=null && index>=0){
			row = $('#attributeDetailGrid').datagrid('getRows')[index];
		}
		$('#popupAttributeDetail').dialog({
			closed : false,
			cache : false,
			modal : true,
			width : 550,
			height : 200,
			onOpen: function(){	
				$('#popupAttributeDetail  #applyObjectHid').val(applyObject);
				$('#popupAttributeDetail #attributeIdHid').val(attributeId);
				if(row!=null){
					disabled('attributeDetailCodePop');
					$('#popupAttributeDetail #enumIdHid').val(row.enumId);
					$('#popupAttributeDetail #attributeIdHid').val(row.attributeId);
//					$('#popupAttributeDetail  #applyObjectHid').val(applyObject);
					setTextboxValue('attributeDetailCodePop',row.enumCode);
					setTextboxValue('attributeDetailNamePop',row.enumValue);
					$('#attributeDetailNamePop').focus();
				}else{
					$('#popupAttributeDetail #enumIdHid').val(0);
					$('#attributeDetailCodePop').focus();
				}
			},
		 	onBeforeClose: function() {
								
	        },
	        onClose : function(){
	        	$('.easyui-dialog #attributeDetailCodePop').val('');
	        	$('.easyui-dialog #attributeDetailNamePop').val('');
	        	enable('attributeDetailCodePop','#popupAttributeDetail');
	        }
		});
	},
	
	/**
	 * save attribute detail 
	 * @author liemtpt
	 * @description: tao moi va cap nhat gia tri cua thuoc tinh dong
	 * @createDate 22/01/2015
	 */
	saveAttributeDetail : function() {
		var msg = '';
		$('#popupAttributeDetail #errMsg').html('').hide();
		if (msg.length == 0) {
			msg = Utils.getMessageOfRequireCheck('attributeDetailCodePop', qltt_ma_gia_tri);
		}
		if(msg.length ==0){
			msg = Utils.getMessageOfSpecialCharactersValidate('attributeDetailCodePop', qltt_ma_gia_tri,Utils._CODE);
		}
		if (msg.length == 0){
			msg = Utils.getMessageOfRequireCheck('attributeDetailNamePop', qltt_ten_gia_tri);
		}
		if(msg.length ==0){
			msg = Utils.getMessageOfSpecialCharactersValidate('attributeDetailNamePop', qltt_ten_gia_tri,Utils._ADDRESS);
		}
		if (msg.length > 0) {
			$('#popupAttributeDetail #errMsg').html(msg).show();
			return false;
		}
		var applyObject =  $('#popupAttributeDetail  #applyObjectHid').val();
		var enumId = $('#popupAttributeDetail #enumIdHid').val().trim();
		var attributeId = $('#popupAttributeDetail #attributeIdHid').val().trim();
		var enumCode = $('#popupAttributeDetail #attributeDetailCodePop').val().trim();
		var enumValue = $('#popupAttributeDetail #attributeDetailNamePop').val().trim();
		var params = new Object();
		params.applyObject = applyObject;
		params.enumId = enumId;
		params.attributeId = attributeId;
		params.enumCode = enumCode;
		params.enumValue = enumValue;
		//Gui yeu cau len server de luu du lieu
		var url = "/attributes/dynamic/save-attribute-detail";
		params[Utils.VAR_NAME] = 'attributeEnumVO';
		var dataModel = getSimpleObject(params);
		Utils.addOrSaveData(dataModel, url,null, 'errMsg', function(data){
			if(data.error != undefined  && data.error && data.errMsg != undefined){
				$('#errMsg').html(data.errMsg).show();
			}else{
				var tm = setTimeout(function(){
					$('.easyui-dialog').dialog('close'); 
					$("#attributeDetailGrid").datagrid("reload");					
					clearTimeout(tm);
				}, 2000);				
			}			
		},null,'#popupAttributeDetail');
		return false;
	},
	/**
	 * delete attribute detail 
	 * @author liemtpt
	 * @description: xoa gia tri thuoc tinh dong
	 * @createDate 22/01/2015
	 */
	deleteAttributeDetail : function(id,applyObject) {
		$('#errMsg').html('').hide();
		var params = new Object();
		params.enumId = id;	
		params.applyObject = applyObject;
		Utils.deleteSelectRowOnGrid(params, qltt_gia_tri, "/attributes/dynamic/remove", AttributesDynamicManager._xhrDel, null, null,function(data){
			if (!data.error){
				$("#attributeDetailGrid").datagrid("reload");			
			}
		},null);		
		return false;		
	}
};
/*
 * END OF FILE - /web/web/resources/scripts/business/catalog/vnm.attributes-dynamic-manager.js
 */

/*
 * START OF FILE - /web/web/resources/scripts/business/catalog/vnm.attribute-customer-manager.js
 */
var AttributeCustomerManager = {
	loadlandau:true,
	loadlandau2:true,
	search: function(){
		$('#errMsg').html('').hide();
		var attributeCode = $('#attributeCode').val().trim();
		var attributeName = $('#attributeName').val().trim();
//		var valueType = $('#valueType').val().trim();
		var lstAttributeCM= '';
		$('#ddcl-valueType-ddw input[type=checkbox]').each(function(){
			if($(this).is(':checked')){
				if($(this).val() == -1){
					lstAttributeCM += $(this).val() + ',';
					return false;
				}else{
					lstAttributeCM += $(this).val() + ',';
				}
			}
		});
		var lstObject = '';
		$('#ddcl-object-ddw input[type=checkbox]').each(function(){
			if($(this).is(':checked')){
				if ($(this).val() == -1) {
					lstObject += $(this).val() + ',';
					return false;
				} else {
					lstObject += $(this).val() + ',';
				}
			}
		});
		var status = $('#status').val().trim();		
		$('#attributeDetailDataDiv').hide();
		var params = new Object();
		params.attributeCode = attributeCode;
		params.attributeName = attributeName;
		params.lstAttributeCM = lstAttributeCM;
		params.lstObject = lstObject;
		params.status = status;
		$('#attributeDatagrid').datagrid('load',params);
			return false;
	},	
	
	saveAttribute : function() {
		var msg = '';
		var valueTypePop = $('#valueTypePop').val().trim();
		$('#errMsg').html('').hide();
		if (msg.length == 0) {
			msg = Utils.getMessageOfRequireCheck('attributeCodePop', 'Mã thuộc tính');
		}
		if(msg.length ==0){
			msg = Utils.getMessageOfSpecialCharactersValidate('attributeCodePop', 'Mã thuộc tính',Utils._CODE);
		}
		if (msg.length == 0){
			msg = Utils.getMessageOfRequireCheck('attributeNamePop', 'Tên thuộc tính');
		}
		if(msg.length ==0){
			msg = Utils.getMessageOfSpecialCharactersValidate('attributeNamePop', 'Tên thuộc tính');
		}
		if (msg.length == 0) {
			msg = Utils.getMessageOfRequireCheck('valueTypePop', 'Loại giá trị');
		}
		if (msg.length == 0) {
			msg = Utils.getMessageOfRequireCheck('statusPop', 'Trạng thái');
		}
		if(msg.length == 0 && valueTypePop == -1){
			msg = 'Bạn chưa chọn Loại giá trị';
		}
		if (msg.length > 0) {
			$('#errMsgPop').html(msg).show();
			return false;
		}
		var id = $('#id').val().trim();
		var attributeCodePop = $('#attributeCodePop').val().trim();
		var attributeNamePop = $('#attributeNamePop').val().trim();
		var valueTypePop = $('#valueTypePop').val().trim();
		var statusPop = $('#statusPop').val().trim();
		var notePop = $('#notePop').val().trim();
				
		var dataModel = new Object();
		dataModel.id = id; //$('#id').val().trim();
		dataModel.attributeCodePop = attributeCodePop;
		dataModel.attributeNamePop = attributeNamePop;
		dataModel.valueTypePop = valueTypePop;
		dataModel.statusPop = statusPop;
		dataModel.notePop = notePop;
		Utils.addOrSaveData(dataModel, "/attribute-customer-manager/save-attribute",null, 'errMsgPop', function(data){
			
				var tm = setTimeout(function(){
					$('.easyui-dialog').dialog('close'); 
					$("#attributeDatagrid").datagrid("reload");
					clearTimeout(tm);
				}, 2000);
			
		},null,'#popupAttribute',null,null,function(data){
			if(data.error != undefined  && data.error && data.errMsg != undefined){
				$('#errMsgPop').html(data.errMsg).show();
			}
		});
		return false;
	},
	
	loadDetail : function(attributeId) {
		$('#attributeDetailDataDiv').show();
		var params = new Object();
		$('#attributeId').val(attributeId);
		params.attributeId = attributeId;
		$('#attributeDetailDatagrid').datagrid({
			url: '/attribute-customer-manager/load-attribute-detail',
			autoRowHeight : true,
			rownumbers : true, 
			checkOnSelect :true,
			pagination:true,
			rowNum : 10,
			fitColumns:true,
			pageList  : [10,20,30],
			scrollbarSize:0,
			width: $('#attributeDetailDataGridSection2').width(),
			queryParams:params,
		    columns:[[	        
			    {field:'code', title: 'Mã GT', width: 100, sortable:false,resizable:false , align: 'left', formatter: function(value, row, index){
			    	return Utils.XSSEncode(value);
			    }},
			    {field:'name', title: 'Tên GT', width:150,sortable:false,resizable:false , align: 'left', formatter: function(value, row, index){
			    	return Utils.XSSEncode(value);
			    }},
			    {field:'statusDetail', title: 'Trạng thái', width: 60, align: 'left',sortable:false,resizable:false,formatter:AttributeCustemerCatalogFormatter.statusFormat},
			    {field:'edit', title:'<a href= "javascript:void(0);" id="btnGridDetailAdd" onclick= "AttributeCustomerManager.openAttributeDetailCustomer();"><img src="/resources/images/icon_add.png" /></a>',
			    	width: 15, align: 'center',sortable:false,resizable:false, formatter: AttributeCustemerCatalogFormatter.editCellDetailIconFormatter},
		    ]],
		    onLoadSuccess :function(data){	    	
		    	$('.datagrid-header-rownumber').html('STT');	    	
		    	 updateRownumWidthForJqGrid('.easyui-dialog');
		    	 CommonFormatter.checkPermissionHideGridButton("btnGridDetailAdd", false, "attributeDetailDataGridSection2");
		    	 CommonFormatter.checkPermissionHideGridButton("btnGridDetailEdit", true, "attributeDetailDataGridSection2");
		    	 $(window).resize();    		 
		    }
		});
		Utils.bindAutoSearchForEasyUI();
	},
		
		
	openAttributeCustomer : function(index) {
		$('#popupAttribute #successMsg').html('').hide();
		$('#popupAttributeDetail #successMsg').html('').hide();
		$('#attributeDetailDataDiv').hide();
		var rows = null;
		if(index!=undefined && index!=null && index>=0){
			rows = $('#attributeDatagrid').datagrid('getRows')[index];
			console.log(rows);
		}
		$('#popupAttribute').dialog({
			closed : false,
			cache : false,
			modal : true,
			width : 550,
			height : 250,
			onOpen: function(){	
				if(AttributeCustomerManager.loadlandau){
					Utils.bindAutoButtonEx('#popupAttribute','btnSaveAttr');
					AttributeCustomerManager.loadlandau = false;
				}
				var tabindex = -1;
				$('.InputTextStyle, select, input, button , li , tr, td, th, label, ul, a, img').each(function () {
					if (this.type != 'hidden') {
						$(this).attr("tabindex", tabindex);
						tabindex -=1;
					}
				});
				tabindex = 1;
	    		 $('.easyui-dialog input,.easyui-dialog select,.easyui-dialog button').each(function () {
		    		 if (this.type != 'hidden') {
			    	     $(this).attr("tabindex", '');
						 tabindex++;
		    		 }
				 });
				if(rows!=null){
					disabled('attributeCodePop');
					disabled('valueTypePop');
					$('#valueTypePopDiv').addClass('BoxDisSelect');
					$('#id').val(rows.attributeId);
					setTextboxValue('attributeCodePop',rows.attributeCode);
					setTextboxValue('attributeNamePop',rows.attributeName);
					setSelectBoxValue('valueTypePop',rows.valueType);
					setSelectBoxValue('statusPop',rows.status);
					setTextboxValue('notePop',rows.note);
					var tm = setTimeout(function(){
						$('#attributeNamePop').focus();
						clearTimeout(tm);
					}, 2000);
				}else{
					$('#id').val(0);
					var tm = setTimeout(function(){
						$('#attributeCodePop').focus();
						clearTimeout(tm);
					}, 2000);
				}
			},
		 	onBeforeClose: function() {
				var tabindex = 1;
				$(' .InputTextStyle, select, input, button , li , tr, td, th, label, ul, a, img').each(function () {
					if (this.type != 'hidden') {
						$(this).attr("tabindex", '');
					}
					tabindex ++;
				});	
				var curIdFocus = $('#cur_focus').val();
				$('#'+ curIdFocus).focus();				
	        },
	        onClose : function(){
	        	$('.easyui-dialog #attributeCodePop').val('');
	        	$('.easyui-dialog #attributeNamePop').val('');
	        	$('.easyui-dialog #valueTypePop').val(-1);
	        	$('.easyui-dialog #valueTypePop').change();
	        	$('.easyui-dialog #statusPop').val(1);
	        	$('.easyui-dialog #statusPop').change();
	        	$('.easyui-dialog #notePop').val('');
	        	$('.easyui-dialog #errMsgPop').hide();
	        	enable('attributeCodePop');
	        	enable('valueTypePop');
				$('#valueTypePopDiv').removeClass('BoxDisSelect');
	        }
		});
	},
	saveAttributeDetail : function() {
		var msg = '';
		$('#errMsgDetailPop').html('').hide();
		if (msg.length == 0) {
			msg = Utils.getMessageOfRequireCheck('attributeDetailCodePop', 'Mã GT');
		}
		if(msg.length ==0){
			msg = Utils.getMessageOfSpecialCharactersValidate('attributeDetailCodePop', 'Mã GT',Utils._CODE);
		}
		if (msg.length == 0){
			msg = Utils.getMessageOfRequireCheck('attributeDetailNamePop', 'Tên GT');
		}
		if(msg.length ==0){
			msg = Utils.getMessageOfSpecialCharactersValidate('attributeDetailNamePop', 'Tên GT');
		}
		if (msg.length == 0) {
			msg = Utils.getMessageOfRequireCheck('statusDetailPop', 'Trạng thái');
		}
		if (msg.length > 0) {
			$('#errMsgDetailPop').html(msg).show();
			return false;
		}
		var idDetail = $('#idDetail').val().trim();
		var attributeId = $('#attributeId').val().trim();
		var attributeDetailCodePop = $('#attributeDetailCodePop').val().trim();
		var attributeDetailNamePop = $('#attributeDetailNamePop').val().trim();
		var statusDetailPop = $('#statusDetailPop').val().trim();
		var dataModel = new Object();
		dataModel.idDetail = idDetail; //$('#id').val().trim();
		dataModel.attributeId = attributeId;
		dataModel.attributeDetailCodePop = attributeDetailCodePop;
		dataModel.attributeDetailNamePop = attributeDetailNamePop;
		dataModel.statusDetailPop = statusDetailPop;
		Utils.addOrSaveData(dataModel, "/attribute-customer-manager/save-attribute-detail",null, 'errMsgDetailPop', function(data){
			if(data.error != undefined  && data.error && data.errMsg != undefined){
				$('#errMsgDetailPop').html(data.errMsg).show();
			}else{
				var tm = setTimeout(function(){
					$('.easyui-dialog').dialog('close'); 
					$("#attributeDetailDatagrid").datagrid("reload");					
					clearTimeout(tm);
				}, 2000);				
			}			
		},null,'#popupAttributeDetail');
		return false;
	},
	openAttributeDetailCustomer : function(index) {
		$('#popupAttribute #successMsg').html('').hide();
		$('#popupAttributeDetail #successMsg').html('').hide();
		$('#errMsgDetailPop').html('').hide();
		var rows = null;
		if(index!=undefined && index!=null && index>=0){
			rows = $('#attributeDetailDatagrid').datagrid('getRows')[index];
		}
		$('#popupAttributeDetail').dialog({
			closed : false,
			cache : false,
			modal : true,
			width : 550,
			height : 230,
			onOpen: function(){	
				if(AttributeCustomerManager.loadlandau2){
					Utils.bindAutoButtonEx('#popupAttributeDetail','btnSaveAttrDetail');
					AttributeCustomerManager.loadlandau2 = false;
				}
				var tabindex = -1;
				$('.InputTextStyle, select, input, button , li , tr, td, th, label, ul, a, img').each(function () {
					if (this.type != 'hidden') {
						$(this).attr("tabindex", tabindex);
						tabindex -=1;
					}
				});
				tabindex = 1;
	    		 $('.easyui-dialog input,.easyui-dialog select,.easyui-dialog button').each(function () {
		    		 if (this.type != 'hidden') {
			    	     $(this).attr("tabindex", '');
						 tabindex++;
		    		 }
				 });
				if(rows!=null){
					disabled('attributeDetailCodePop');
					$('#idDetail').val(rows.id);
					setTextboxValue('attributeDetailCodePop',rows.code);
					setTextboxValue('attributeDetailNamePop',rows.name);
					setSelectBoxValue('statusDetailPop',rows.status);
					var tm = setTimeout(function(){
						$('#attributeDetailNamePop').focus();
						clearTimeout(tm);
					}, 2000);
				}else{
					$('#idDetail').val(0);
					var tm = setTimeout(function(){
						$('#attributeDetailCodePop').focus();
						clearTimeout(tm);
					}, 2000);
				}
//				$('#saveAttributeDetail').bind('click',function(event) {
//					if(!$(this).is(':hidden')){
//						AttributeCustomerManager.saveAttributeDetail();
//					}
//				});
			},
		 	onBeforeClose: function() {
				var tabindex = 1;
				$(' .InputTextStyle, select, input, button , li , tr, td, th, label, ul, a, img').each(function () {
					if (this.type != 'hidden') {
						$(this).attr("tabindex", '');
					}
					tabindex ++;
				});	
				var curIdFocus = $('#cur_focus').val();
				$('#'+ curIdFocus).focus();				
	        },
	        onClose : function(){
	        	$('.easyui-dialog #attributeDetailCodePop').val('');
	        	$('.easyui-dialog #attributeDetailNamePop').val('');
	        	$('.easyui-dialog #statusDetailPop').val(1);
	        	$('.easyui-dialog #statusDetailPop').change();
	        	enable('attributeDetailCodePop');
	        }
		});
	}
};
/*
 * END OF FILE - /web/web/resources/scripts/business/catalog/vnm.attribute-customer-manager.js
 */

/*
 * START OF FILE - /web/web/resources/scripts/business/catalog/vnm.group-product-po-auto-npp-catalog.js
 */
var GroupProductPOAutoNPP = {
		xhrSave : null,
		_xhrDel: null,
		id:null,
		_groupId:null,
		_mapMutilSelect : null,
		_mapMultiSelectSub:null,
		_mapNode:null,
		_mapCheck:null,
		getGridUrl: function(shopCode,shopName,status){
			return "/catalog/group-product-po-auto-npp/search?shopCode=" + encodeChar(shopCode) + "&shopName=" + encodeChar(shopName) +  "&status=" + status;
		},
		getPopSeachShopMapUrl:function(shopCode,shopName){
			return "/catalog/group-product-po-auto-npp/getListShopNotInPoGroup?shopCode=" + encodeChar(shopCode) + "&shopName=" + encodeChar(shopName);
		},
		getGridGroupProductUrl: function(id,status){
			return "/catalog/group-product-po-auto-npp/searchGroupProduct?id=" + encodeChar(id);
		},
		getGridProductUrl:function(groupId,objectType){
			return "/catalog/group-product-po-auto-npp/searchListProduct?idGroup=" + groupId + "&objectType=" +objectType;
		},
		getGridSectorUrl:function(objectType, name, code){
			return "/catalog/group-product-po-auto-npp/searchListSector?objectType=" +objectType + "&name=" + encodeChar(name)+ "&code="+ encodeChar(code);
		},
		afterCreateUpdatePoAutoGroup:function(){
			//reload lai gridGroupProduct
			$('#gridGroupProduct').datagrid('reload');
		},
		search: function(){
//			GroupProductPOAutoNPP.reset();
			$('#errMsg').html('').hide();
			var shopCode = $('#shopCode').val().trim();
			var shopName = $('#shopName').val().trim();
			var status = $('#status').val();
			var tm = setTimeout(function() {
				$('#shopCode').focus();
			}, 500);
			var url = GroupProductPOAutoNPP.getGridUrl(shopCode,shopName,status);
			$("#gridShopMap").datagrid({url:url,pageNumber:1});
			GroupProductPOAutoNPP._hideGrids();
			return false;
		},
		_hideGrids: function() {
			$("#menuGroupProduct").hide();
			$("#menuSecGoods").hide();
			$("#menuSecGoodsChild").hide();
			$("#menuProduct").hide();
		},
		searchGroupProduct: function(id, shopCode){
//			$('#idCode2').val(id);
			GroupProductPOAutoNPP.id = id;
			GroupProductPOAutoNPP.reset();
			$('#menuGroupProduct').show();
			$('#errMsg').html('').hide();
			var url = GroupProductPOAutoNPP.getGridGroupProductUrl(id);
			var NPPshopCodeLabel = '';
			if (shopCode != undefined && shopCode != null ){
				NPPshopCodeLabel = shopCode;
			}
			$('.Title2Style #NPPshopCodeLabel').text("(NPP: "+NPPshopCodeLabel+")");
			var strTitle = '<a href="javascript:void(0);"  onclick="return GroupProductPOAutoNPP.openDialogCreateGroupProductPOAutoNPP(0);"><img src="/resources/images/icon-add.png"></a>';
			var strTitleAddOnGrid = getFormatterControlGrid('btnAddGrid2', strTitle);
			$("#gridGroupProduct").datagrid({
				  url:url,
			  	  pageList  : [10,20,30],
				  width: ($('#productGrid').width()),
				  pageSize : 10,
				  checkOnSelect :true,
				  height:'auto',
				  scrollbarSize : 0,
				  pagination:true,			  
				  fitColumns:true,		     
			      singleSelect:true,
				  method : 'GET',
				  rownumbers: true,
				  columns:[[	
					{field: 'groupCode', title: 'Mã nhóm', width: 100, sortable:false,resizable:false, align: 'left', formatter: function(value, row, index){
				    	return Utils.XSSEncode(value);
				    }},
				    {field: 'groupName', title: 'Tên nhóm', width: 340, sortable:false,resizable:false, align: 'left', formatter: function(value, row, index){
				    	return Utils.XSSEncode(value);
				    }},
				    {field: 'status', title: 'Trạng thái', width: 80, sortable:false,resizable:false, align: 'center',formatter:function(value,row,index){
				    	if(row.status == 1){
				    		return 'Hoạt động';
				    	}
				    	if(row.status == 0){
				    		return 'Tạm ngưng';
				    	}
				    }},
				    {field: 'cat', title: 'NH', width: 30, sortable:false,resizable:false, align: 'center',formatter:function(value,row,index){
				    	var data = '<a href="javascript:void(0);" onclick="return GroupProductPOAutoNPP.searchCat('+row.id+',0);"><img src="/resources/images/icon-view.png"></a>';
						return getFormatterControlGrid('btnDetailGrid2', data);
				    }},
				    {field: 'subcat', title: 'NHC', width: 30, sortable:false,resizable:false, align: 'center',formatter:function(value,row,index){
				    	return '<a href="javascript:void(0);" onclick="return GroupProductPOAutoNPP.searchSubCat('+row.id+',1);"><img src="/resources/images/icon-view.png"></a>';
				    }},
				    {field: 'product', title: 'SP', width: 30, sortable:false,resizable:false, align: 'center',formatter:function(value,row,index){
				    	return '<a href="javascript:void(0);" onclick="return GroupProductPOAutoNPP.searchProduct('+row.id+',2);"><img src="/resources/images/icon-view.png"></a>';
				    }},
				    {field: 'edit', title: strTitleAddOnGrid, width: 30, align: 'center',sortable:false,resizable:false,
				    formatter:function(value,row,index){
				    	var data = '<a href="javascript:void(0);" onclick="return GroupProductPOAutoNPP.openDialogUpdateGroupProductPOAutoNPP(1,'+row.id+',\''+Utils.XSSEncode(row.groupCode)+'\',\''+Utils.XSSEncode(row.groupName)+'\','+row.status+');"><img src="/resources/images/icon-edit.png"></a>';
						return getFormatterControlGrid('btnEditGrid2', data);
				    }},	
				    {field: 'id', index: 'id', hidden: true},	   
				  ]],
				  onLoadSuccess:function(){
						$('.datagrid-header-rownumber').html('STT');  
				      }
			});
			return false;
		},
		getListProduct:function(groupId){
			var objectType =2 ; 
			
		},
		deleteRow: function(groupDetailId){
			$('#errMsg').html('').hide();
			var dataModel = new Object();
			dataModel.groupDetailId = groupDetailId;	
			Utils.deleteSelectRowOnGrid(dataModel, 'sản phẩm', "/catalog/group-product-po-auto-npp/deleteProduct", GroupProductPOAutoNPP._xhrDel, null, null,function(data){
				if(data.error == false){
					//var url = GroupProductPOAutoNPP.getGridUrl(GroupProductPOAutoNPP.id,2);
					//$("#gridProduct").datagrid({url:url,page:1});
					$("#gridProduct").datagrid("reload");
				}
			});
			return false;		
		},
	
		searchCat:function(id,objectType){
			$('#idCode1').val(id);
			GroupProductPOAutoNPP.id = id;
			GroupProductPOAutoNPP.reset();
			$('#menuSecGoods').show();
			var url = GroupProductPOAutoNPP.getGridProductUrl(id,objectType);
			var strTitle = "<a href='javascript:void(0)' onclick='return GroupProductPOAutoNPP.openDialogUpdateSecGoods();'><img src='/resources/images/icon_add.png'/></a>";
			var strTitleAddOnGrid = getFormatterControlGrid('btnAddGrid3', strTitle);
			$("#gridSecGoods").datagrid({
				  url:url,
			  	  pageList  : [10,20,30],
				  width: 550,
				  pageSize : 10,
				  checkOnSelect :true,
				  height:'auto',
				  scrollbarSize : 0,
				  pagination:true,			  
				  fitColumns:true,		     
			      singleSelect:true,
				  method : 'GET',
				  rownumbers: true,
				  columns:[[	
					{field: 'code', title: 'Mã NH', width: 150, sortable:false,resizable:false, align: 'left', formatter: function(value, row, index){
				    	return Utils.XSSEncode(value);
				    }},
				    {field: 'name', title: 'Tên NH', width: 300, sortable:false,resizable:false, align: 'left', formatter: function(value, row, index){
				    	return Utils.XSSEncode(value);
				    }},
				    {field:'delete', title:strTitleAddOnGrid, width: 50, align: 'center',sortable:false,resizable:false, formatter: function(value, row, index){
				    	var data = "<a href='javascript:void(0)' onclick=\"return GroupPOAuto.deleteCategoryRow('"+ row.id +"')\"><img src='/resources/images/icon-delete.png'/></a>";
						return getFormatterControlGrid('btnDeleteGrid3', data);
				    }},
				  ]],
				  onLoadSuccess:function(){
						$('.datagrid-header-rownumber').html('STT');  
				      	$('#gridSecGoods').datagrid('resize');
				      }
			});
			return false;
		},
		searchSubCat:function(id,objectType){
			$('#idCode2').val(id);
			GroupProductPOAutoNPP.id = id;
			GroupProductPOAutoNPP.reset();
			$('#menuSecGoodsChild').show();
			var url = GroupProductPOAutoNPP.getGridProductUrl(id,objectType);
			var strTitle = '<a href="javascript:void(0)" onclick="return GroupProductPOAutoNPP.openDialogUpdateSecGoodsBrand();"><img src="/resources/images/icon_add.png"/></a>';
			var strTitleAddOnGrid = getFormatterControlGrid('btnAddGrid4', strTitle); 
			$("#gridSecGoodsBrand").datagrid({
				  url:url,
			  	  pageList  : [10,20,30],
				  width: 550,
				  pageSize : 10,
				  checkOnSelect :true,
				  height:'auto',
				  scrollbarSize : 0,
				  pagination:true,			  
				  fitColumns:true,		     
			      singleSelect:true,
				  method : 'GET',
				  rownumbers: true,
				  columns:[[
				            //here
				    {field: 'parentCode', title: 'Mã NH', width: 150, sortable:false,resizable:false, align: 'left', formatter: function(value, row, index){
				    	return Utils.XSSEncode(value);
				    }},
					{field: 'code', title: 'Mã NHC', width: 150, sortable:false,resizable:false, align: 'left', formatter: function(value, row, index){
				    	return Utils.XSSEncode(value);
				    }},
				    {field: 'name', title: 'Tên NHC', width: 300, sortable:false,resizable:false, align: 'left', formatter: function(value, row, index){
				    	return Utils.XSSEncode(value);
				    }},
				    {field:'delete', title:strTitleAddOnGrid, width: 50, align: 'center',sortable:false,resizable:false, formatter: function(value, row, index){
				    	var data = "<a href='javascript:void(0)' onclick=\"return GroupPOAuto.deleteSubCategoryRow('"+ row.id +"')\"><img src='/resources/images/icon-delete.png'/></a>";
						return getFormatterControlGrid('btnDeleteGrid4', data);
				    }},
				  ]],
				  onLoadSuccess:function(){
						$('.datagrid-header-rownumber').html('STT');  
				      	$('#gridSecGoodsBrand').datagrid('resize');
				      }
			});
			return false;
		},
		searchProduct:function(id,objectType){
			GroupProductPOAutoNPP.id = id;
			GroupProductPOAutoNPP.reset();
			$('#menuProduct').show();
			var url = GroupProductPOAutoNPP.getGridProductUrl(id,objectType);
			var strTilte = "<a href='javascript:void(0)' onclick=\'return GroupProductPOAutoNPP.openPopupProduct("+id +","+objectType+");\'><img src='/resources/images/icon_add.png'/></a>";
			var strTitleAddOnGrid = getFormatterControlGrid('btnAddGrid5', strTilte);
			$("#gridProduct").datagrid({
				  url:url,
			  	  pageList  : [10,20,30],
				  width: 550,
				  pageSize : 10,
				  checkOnSelect :true,
				  height:'auto',
				  scrollbarSize : 0,
				  pagination:true,			  
				  fitColumns:true,		     
			      singleSelect:true,
				  method : 'GET',
				  rownumbers: true,
				  columns:[[	
					{field: 'code', title: 'Mã sản phẩm', width: 150, sortable:false,resizable:false, align: 'left', formatter: function(value, row, index){
				    	return Utils.XSSEncode(value);
				    }},
				    {field: 'name', title: 'Tên sản phẩm', width: 300, sortable:false,resizable:false, align: 'left', formatter: function(value, row, index){
				    	return Utils.XSSEncode(value);
				    }},
				    {field:'delete', title:strTitleAddOnGrid, width: 50, align: 'center',sortable:false,resizable:false, formatter: function(value, row, index){
				    	var data = "<a href='javascript:void(0)' onclick=\"return GroupPOAuto.deleteRow('"+ row.id +"')\"><img src='/resources/images/icon-delete.png'/></a>";
						return getFormatterControlGrid('btnDeleteGrid5', data);
				    }},   
				  ]],
				  onLoadSuccess:function(){
						$('.datagrid-header-rownumber').html('STT');  
				      	$('#gridProduct').datagrid('resize');
				      }
			});
			return false;
		},
		reset:function(){
			$('#menuSecGoods').hide();
			$('#menuSecGoodsChild').hide();
			$('#menuProduct').hide();
		},
		searchProductTree:function(){
			var code = $("#productCode").val().trim();
			var name = 	$("#productName").val().trim();
			var nodes = $('#tree').tree('getChecked');
			for(var i=0; i<nodes.length; i++){
				if(nodes[i].attributes.productTreeVO.type == "PRODUCT"){
					GroupProductPOAutoNPP._mapCheck.put(nodes[i].id,nodes[i].id);
		    		}else{
		    			GroupProductPOAutoNPP._mapCheck.remove(nodes[i].id);
		    		}
			}
			$(".easyui-dialog #errMsgProductDlg").html('').hide();
			var GroupProductPOAutoNPPId = GroupProductPOAutoNPP.id; //id GroupProductPOAutoNPP
			$("#tree").tree({
				url: '/rest/catalog/group-po/tree/0.json',  
				method:'GET',
	            animate: true,
	            checkbox:true,
	            onBeforeLoad:function(node,param){
	            	$("#tree").css({"visibility":"hidden"});
//	            	$("#loadding").css({"visibility":"visible"});
	            	$("#rpSection").html('<img id="loadding" class="LoadingStyle" src="/resources/images/loadingbig.gif" width="30" height="30" alt="Loadding"/ >');
	            	$("#treeNoResult").html('');
	            	param.code = code;
	            	param.name = name;
	            	param.id = GroupProductPOAutoNPPId;
	            },
	            onLoadSuccess:function(node,data){
	            	$("#tree").css({"visibility":"visible"});
//	            	$("#loadding").css({"visibility":"hidden"});
	            	$("#rpSection").html('');
	            	if(data.length == 0){
	            		$(".easyui-dialog #errMsgProductDlg").html('Không có kết quả').show();
	            	}
	            	var arr  = GroupProductPOAutoNPP._mapCheck.keyArray;
	            	if(arr.length > 0){
	            		for(var i = 0;i < arr.length; i++){
	            			var nodes = $('#tree').tree('find', arr[i]);
	            			if(nodes != null){
	            				$("#tree").tree('check',nodes.target);
	            			}
	            		}
	            	}
	            },
	            onCheck:function(node,check){ 
	            	var nodes = $('#tree').tree('getChecked');
            	if(node.attributes.productTreeVO.type == "PRODUCT"){
            			GroupProductPOAutoNPP._mapCheck.put(node.id,node.id);
            		}else{
            			GroupProductPOAutoNPP._mapCheck.remove(node.id);
            		}
	            }
			});
		},
		openPopupProduct:function(id,objectType){
			$("#productCode").val('');
			$("#productName").val('');
			$(".easyui-dialog #errMsgProductDlg").html('').hide();
			GroupProductPOAutoNPP._mapNode = new Map();
			GroupProductPOAutoNPP._mapCheck = new Map();
			$("#popupProductSearch").dialog({
				onOpen:function(){
					$("#tree").tree({
						url: '/rest/catalog/group-po/tree/4.json',  
						method:'GET',
			            animate: true,
			            checkbox:true,
			            onBeforeLoad:function(node,param){
			            	$("#tree").css({"visibility":"hidden"});
			            	//$("#loadding").css({"visibility":"visible"});
			            	$("#rpSection").html('<img id="loadding" class="LoadingStyle" src="/resources/images/loadingbig.gif" width="30" height="30" alt="Loadding"/ >');
			            	$("#treeNoResult").html('');
			            	param.code = '';
			            	param.name = '';
			            	param.id = GroupProductPOAutoNPP.id;
			            },
			            onLoadSuccess:function(node,data){
			            	$("#tree").css({"visibility":"visible"});
			            	//$("#loadding").css({"visibility":"hidden"});
			            	$("#rpSection").html('');
			            	$("#productCode").focus();
//			            	if(GroupProductPOAutoNPP._mapNode != null && GroupProductPOAutoNPP._mapNode != undefined && GroupProductPOAutoNPP._mapNode.keyArray.length >0){
//			            		var nodes = $('#tree').tree('getChecked');
//			            		for(var i=0; i<nodes.length; i++){
//			            			var temp = GroupProductPOAutoNPP._mapNode.get(nodes[i].id);
//			            			if(temp != null && temp != undefined){
//			            				$("#tree").tree('check',nodes[i].target);
//			            			}
//			            		}
//			            	}
			            	if(data.length == 0){
			            		$(".easyui-dialog #errMsgProductDlg").html('Không có kết quả').show();
			            	}
			            	var arr  = GroupProductPOAutoNPP._mapCheck.keyArray;
			            	if(arr.length > 0){
			            		for(var i = 0;i < arr.length; i++){
			            			var nodes = $('#tree').tree('find', arr[i]);
			            			if(nodes != null){
			            				$("#tree").tree('check',nodes.target);
			            			}
			            		}
			            	}
			            },
			            onCheck:function(node,check){ 
			            	var nodes = $('#tree').tree('getChecked');
			                
//			            	if(check){
//			            			GroupProductPOAutoNPP._mapNode.put(node.id,node.id);
//			            			for(var i=0; i<nodes.length; i++){
//					                	if(nodes[i].attributes.productTreeVO.type == "PRODUCT"){
//					                		var temp = GroupProductPOAutoNPP._mapCheck.get(nodes[i].id);
//					                		if(temp == null){
//					                			GroupProductPOAutoNPP._mapCheck.put(nodes[i].id,nodes[i].id);
//					                		}
//					                	}
//					                }
//			            	}else{
//			            		GroupProductPOAutoNPP._mapNode.remove(node.id);
//			            		GroupProductPOAutoNPP._mapCheck.remove(node.id);
//			            	}
				            	if(node.attributes.productTreeVO.type == "PRODUCT"){
				            			GroupProductPOAutoNPP._mapCheck.put(node.id,node.id);
				            		}else{
				            			GroupProductPOAutoNPP._mapCheck.remove(node.id);
				            		}
			            }
					});
				}
			
			});
			$("#popupProductSearch").dialog('open');
		},
		createGroupProductPOAutoNPPDetail:function(){
			var dataModel = new Object();
			var nodes = $('#tree').tree('getChecked');
			var lstId = '';  
			var lstIdCustomer = new Array();
			$('#errMsgPopup3').html('').hide();
			if(nodes.length == 0 ) {
				$('#errMsgPopup3').html('Bạn chưa chọn sản phẩm nào.').show();
        		return false;
        	}
	        for(var i=0; i<nodes.length; i++){
	        	if(nodes[i].attributes.productTreeVO.type != "PRODUCT"){
	        		continue;
	        	}
	        	lstIdCustomer.push(nodes[i].attributes.productTreeVO.id); 
	        }
			if(lstIdCustomer == undefined || lstIdCustomer == null || lstIdCustomer.length <=0){
				$(".easyui-dialog #errMsgProductDlg").html('Không có kết quả').show();
				return false;
			}
			dataModel.objectType = 2;
			dataModel.id = GroupProductPOAutoNPP.id;
			dataModel.listOjectId = lstIdCustomer;
			Utils.addOrSaveRowOnGrid(dataModel, "/catalog/group-po/save", GroupProductPOAutoNPP._xhrSave, null, null,function(data){
				$("#popupProductSearch").dialog("close");
				if(data.error == false){
					$("#gridProduct").datagrid("reload");
				}
			});
			return false;

		},
		openDialogAddShopMap:function(codeText, nameText, title, url, callback,	codeFieldText, nameFieldText, arrParam, objectStaff){
			GroupProductPOAutoNPP._hideGrids();
			$('#popupShopMapDiv').css('visibility','visible');
			$('#popupShopMap').dialog('open');
			$('#shopCodePop').val('');
			$('#shopNamePop').val('');
			$('#shopCodePop').focus();
			GroupProductPOAutoNPP._mapMutilSelect = new Map();
			$('#errMsgPopShopMap').hide();
			$("#gridShopMapPop").datagrid({
				  url:'/catalog/group-product-po-auto-npp/getListShopNotInPoGroup',
			  	  pageList  : [10,20,30],
				  width: 550,
				  pageSize : 10,
				  checkOnSelect :true,
				  height:'auto',
				  scrollbarSize : 0,
				  pagination:true,
				  pageNumber:1,
				  fitColumns:true,		     
				  method : 'GET',
				  rownumbers: true,
				  columns:[[	
						{field: 'shopCode', title: 'Mã đơn vị', width: 150, sortable:false,resizable:false, align: 'left', formatter: function(value, row, index){
					    	return Utils.XSSEncode(value);
					    }},
						{field: 'shopName', title: 'Tên đơn vị', width: 300, sortable:false,resizable:false, align: 'left', formatter: function(value, row, index){
					    	return Utils.XSSEncode(value);
					    }},
						{field: 'id',title: '', checkbox:true, width:50,align:'center',sortable : false,resizable : false},
				  ]],
				  	onCheck : function(rowIndex, rowData) {
				    	var selectedId = rowData.id;
				    	GroupProductPOAutoNPP._mapMutilSelect.put(selectedId, rowData);
				    },
				    onUncheck : function(rowIndex, rowData) {
				    	var selectedId = rowData.id;
				    	GroupProductPOAutoNPP._mapMutilSelect.remove(selectedId);
				    },
				    onCheckAll : function(rows) {
				    	if($.isArray(rows)) {
				    		for(var i = 0; i < rows.length; i++) {
				    			var row = rows[i];
				    			var selectedId = row.id;
				    			GroupProductPOAutoNPP._mapMutilSelect.put(selectedId, row);
				    		}
				    	}
				    },
				    onUncheckAll : function(rows) {
				    	if($.isArray(rows)) {
				    		for(var i = 0; i < rows.length; i++) {
				    			var row = rows[i];
				    			var selectedId = row.id;
				    			GroupProductPOAutoNPP._mapMutilSelect.remove(selectedId);
				    		}
				    	}
				    },
				   onLoadSuccess:function(data){
				      	var easyDiv = '#gridShopMapContainer ';
				    	$(easyDiv+'.datagrid-header-rownumber').html('STT');
				    	var exitsAll = false;
			    		var runner = 0;
			    		$(easyDiv+'input:checkbox[name=id]').each(function() {
			    			var selectedId = this.value;
			    			if(GroupProductPOAutoNPP._mapMutilSelect.get(selectedId) != null || GroupProductPOAutoNPP._mapMutilSelect.get(selectedId) != undefined) {
			    				exitsAll = true;
			    				$('#gridShopMapPop').datagrid('selectRow',runner);
			    			} 
			    			runner ++; 
			    		});
			    		if (exitsAll==false){
			    			$(easyDiv+'td[field=id] .datagrid-header-check input:checkbox').attr('checked', false);
				    	}
				    	
			    		var tabindex = -1;
						$('.InputTextStyle, select, input, button , li , tr, td, th, label, ul, a, img').each(function () {
							if (this.type != 'hidden') {
								$(this).attr("tabindex", tabindex);
								tabindex -=1;
							}
						});
						tabindex = 1;
			    		 $('.easyui-dialog input,.easyui-dialog select,.easyui-dialog button').each(function () {
				    		 if (this.type != 'hidden') {
					    	     $(this).attr("tabindex", '');
								 tabindex++;
				    		 }
						 });
				 	    	if(data.rows == null || data.rows == undefined || data.rows.length == 0){
				 	    		$(easyDiv+'td[field=id] .datagrid-header-check input:checkbox').removeAttr('checked');
				 	    	}
			    		updateRownumWidthForDataGrid('#gridShopMapPop');
			    		$(window).resize();
				      }
			});
			
			return false;
		},
		searchShopMapPop: function(){
			$('#errMsgPopShopMap').html('').hide();
			$('#errMsg').html('').hide();
			var code = $('#shopCodePop').val().trim();
			var name = $('#shopNamePop').val().trim();
			var tm = setTimeout(function() {
				$('#shopCodePop').focus();
			}, 500);
			var url = GroupProductPOAutoNPP.getPopSeachShopMapUrl(code, name);
			$("#gridShopMapPop").datagrid({url:url,pageNumber:1});
			return false;
		},
		addPoAutoShopMap:function(){
			$('#errMsgPopShopMap').html('').hide();
			$('#errMsg').html('').hide();
			if(GroupProductPOAutoNPP._mapMutilSelect == null || GroupProductPOAutoNPP._mapMutilSelect.size() <= 0) {
				$('#errMsgPopShopMap').html('Bạn chưa chọn NPP nào. Yêu cầu chọn').show();
        		return false;
        	}
			var mapKey = GroupProductPOAutoNPP._mapMutilSelect.keyArray;
			var mapValue = GroupProductPOAutoNPP._mapMutilSelect.valArray;
			var params = new Object();
			params.listShopId = mapKey;
			GroupProductPOAutoNPP.updatePoAutoShopMap(params.listShopId);			
		},
		updatePoAutoShopMap:function(listShopId){
			var dataModel = new Object();
//			dataModel.id = $('#idCode1').val();
			dataModel.listShopId = listShopId;
			Utils.addOrSaveData(dataModel, "/catalog/group-product-po-auto-npp/addPOAutoGroupShopMap", null, 'errMsg',function(data){	
				if(data.errMsg!=undefined && data.errMsg.length==0){
					$('#popupShopMap').dialog('close');
					$("#gridShopMapPop").datagrid("reload");
					GroupProductPOAutoNPP.search();
				}
			});
		},
		openDialogEditShopMap: function(idPoAutoGroupShopMap,shopCode, shopName, status){
			if(idPoAutoGroupShopMap != null && idPoAutoGroupShopMap != 0 && idPoAutoGroupShopMap !=undefined){
				$('#shopIdHidden').val(idPoAutoGroupShopMap);
			} else {
				$('#shopIdHidden').val(0);
			}
			$('#popupEditShopMap').dialog({
				title : 'Cập nhật NPP với nhóm PO Auto',
				onOpen: function(){
					$('#errMsgPop').html('').hide();
					disabled('_shopCodePop');
					disabled('_shopNamePop');
					$('#_shopCodePop').val(shopCode);
					$('#_shopNamePop').val(shopName);
					$('#_statusPop').val(status).change();
					$('#_statusPop').focus();
					$('.easyui-dialog #__btnSaveShopMap').bind('click',function(event) {
						var params = new Object();
						params.id = idPoAutoGroupShopMap;
						params.statusValue = $('.easyui-dialog #_statusPop').val();
						Utils.addOrSaveData(params, '/catalog/group-product-po-auto-npp/updatePoAutoGroupShopMap', null, 'errMsgEditShopMap', function(data){
//							$('#'+searchDialog).dialog('close');
//								GroupProductPOAutoNPP.afterCreateUpdatePoAutoGroup();
							$('#gridShopMap').datagrid('reload');
						}, null, null, null, "Bạn có muốn cập nhật thông tin nhóm không ?", null);
					});
				},
				onClose : function() {
					SubCategorySecondary.clearPop();
					$('.easyui-dialog #__btnSaveShopMap').unbind('click');
		        	$('.easyui-dialog #errMsgEditShopMap').html('').hide();
				}
			});
	        $('#popupEditShopMap').dialog('open');
			return false;
		},
		
		
		
		
		
		openDialogUpdateSecGoods:function(codeText, nameText, title, url, callback,	codeFieldText, nameFieldText, arrParam, objectStaff){
			$('#popupSecFoods').css('visibility','visible');
			$('#popup1').dialog('open');
			$('#codeSecGoods').val('');
			$('#nameSecGoods').val('');
			$('#codeSecGoods').focus();
			GroupProductPOAutoNPP._mapMutilSelect = new Map();
			$('#errMsgPopup1').hide();
			var url = GroupProductPOAutoNPP.getGridSectorUrl(0, null, null);
//			$("#gridSecGoodsPop").datagrid({url:url,pageNumber:1});
			$("#gridSecGoodsPop").datagrid({
				  url:url,
			  	  pageList  : [10,20,30],
				  width: 550,
				  pageSize : 10,
				  checkOnSelect :true,
				  height:'auto',
				  scrollbarSize : 0,
				  pagination:true,
				  pageNumber:1,
				  fitColumns:true,		     
				  method : 'GET',
				  rownumbers: true,
				  columns:[[	
					{field: 'productInfoCode', title: 'Mã NH', width: 150, sortable:false,resizable:false, align: 'left', formatter: function(value, row, index){
				    	return Utils.XSSEncode(value);
				    }},
				    {field: 'productInfoName', title: 'Tên NH', width: 300, sortable:false,resizable:false, align: 'left', formatter: function(value, row, index){
				    	return Utils.XSSEncode(value);
				    }},
//				    {field: 'status',title: 'Trạng thái', width:100,align:'center',sortable : false,resizable : false , formatter : PoAutoGroupFomatter.statusFormatter},
				    {field: 'productInfoId',title: '', checkbox:true, width:50,align:'center',sortable : false,resizable : false},
				    
				  ]],
				  	onCheck : function(rowIndex, rowData) {
				    	var selectedId = rowData.productInfoId;
				    	GroupProductPOAutoNPP._mapMutilSelect.put(selectedId, rowData);
				    },
				    onUncheck : function(rowIndex, rowData) {
				    	var selectedId = rowData.productInfoId;
				    	GroupProductPOAutoNPP._mapMutilSelect.remove(selectedId);
				    },
				    onCheckAll : function(rows) {
				    	if($.isArray(rows)) {
				    		for(var i = 0; i < rows.length; i++) {
				    			var row = rows[i];
				    			var selectedId = row.productInfoId;
				    			GroupProductPOAutoNPP._mapMutilSelect.put(selectedId, row);
				    		}
				    	}
				    },
				    onUncheckAll : function(rows) {
				    	if($.isArray(rows)) {
				    		for(var i = 0; i < rows.length; i++) {
				    			var row = rows[i];
				    			var selectedId = row.productInfoId;
				    			GroupProductPOAutoNPP._mapMutilSelect.remove(selectedId);
				    		}
				    	}
				    },
				   onLoadSuccess:function(data){
				      	var easyDiv = '#gridSecGoodsPopContainer ';
				    	$(easyDiv+'.datagrid-header-rownumber').html('STT');
				    	var exitsAll = false;
			    		var runner = 0;
			    		$(easyDiv+'input:checkbox[name=productInfoId]').each(function() {
			    			var selectedId = this.value;
			    			if(GroupProductPOAutoNPP._mapMutilSelect.get(selectedId) != null || GroupProductPOAutoNPP._mapMutilSelect.get(selectedId) != undefined) {
			    				exitsAll = true;
			    				$('#gridSecGoodsPop').datagrid('selectRow',runner);
			    			} 
			    			runner ++; 
			    		});
			    		if (exitsAll==false){
			    			$(easyDiv+'td[field=productInfoId] .datagrid-header-check input:checkbox').attr('checked', false);
				    	}
				    	
			    		var tabindex = -1;
						$('.InputTextStyle, select, input, button , li , tr, td, th, label, ul, a, img').each(function () {
							if (this.type != 'hidden') {
								$(this).attr("tabindex", tabindex);
								tabindex -=1;
							}
						});
						tabindex = 1;
			    		 $('.easyui-dialog input,.easyui-dialog select,.easyui-dialog button').each(function () {
				    		 if (this.type != 'hidden') {
					    	     $(this).attr("tabindex", '');
								 tabindex++;
				    		 }
						 });
			    		 if(data.rows.length == 0){
			    			 $('.datagrid-header-check input').removeAttr('checked');
			    		 }
			    		 $(easyDiv+'input:checkbox[name=productInfoId]').each(function(){
			    			 	var selectedId = $(this).val();
				 				var temp = GroupProductPOAutoNPP._mapMutilSelect.get(selectedId);
				 				if(temp!=null) $(this).attr('checked','checked');
				 			});
				 	    	var length = 0;
				 	    	$(easyDiv+'input:checkbox[name=productInfoId]').each(function(){
				 	    		if($(this).is(':checked')){
				 	    			++length;
				 	    		}	    		
				 	    	});	    	
				 	    	if(data.rows.length==length){
				 	    		$(easyDiv+'td[field=productInfoId] .datagrid-header-check input:checkbox').attr('checked','checked');
				 	    	}else{
								$(easyDiv+'td[field=productInfoId] .datagrid-header-check input:checkbox').removeAttr('checked');
				 	    	}
				 	    	if(data.rows == null || data.rows == undefined || data.rows.length == 0){
				 	    		$(easyDiv+'td[field=productInfoId] .datagrid-header-check input:checkbox').removeAttr('checked');
				 	    	}
			    		updateRownumWidthForDataGrid('#gridSecGoodsPop');
			    		$(window).resize();
				      }
			});
			
			return false;
		},
		addCategory:function(){
			$('#errMsgPopup1').html('').hide();
			if(GroupProductPOAutoNPP._mapMutilSelect == null || GroupProductPOAutoNPP._mapMutilSelect.size() <= 0) {
				$('#errMsgPopup1').html('Bạn chưa chọn ngành hàng nào. Yêu cầu chọn').show();
        		return false;
        	}
	
			var mapKey = GroupProductPOAutoNPP._mapMutilSelect.keyArray;
			var mapValue = GroupProductPOAutoNPP._mapMutilSelect.valArray;
			var params = new Object();
			params.lstCategoryId = mapKey;
			GroupProductPOAutoNPP.updateCategory(params.lstCategoryId);			
		},
		openDialogUpdateSecGoodsBrand:function(codeText, nameText, title, url, callback,	codeFieldText, nameFieldText, arrParam, objectStaff){
			$('#popupSecFoodsBrand').css('visibility','visible');
			$('#popup2').dialog('open');
			$('#errMsgPopup2').hide();
			GroupProductPOAutoNPP._mapMutilSelectSub = new Map();
			$('#codeSecGoodsBrand').val('');
			$('#nameSecGoodsBrand').val('');
			$('#codeSecGoodsBrand').focus();
			var url = GroupProductPOAutoNPP.getGridSectorUrl(1, null, null);
			$("#gridSecGoodsPopBrand").datagrid({
				  url:url,
			  	  pageList  : [10,20,30],
				  width: 550,
				  pageSize : 10,
				  checkOnSelect :true,
				  height:'auto',
				  scrollbarSize : 0,
				  pagination:true,
				  pageNumber:1,
				  fitColumns:true,
				  method : 'GET',
				  rownumbers: true,
				  columns:[[
				            //here
				    {field: 'parentCode', title: 'Mã NH', width: 150, sortable:false,resizable:false, align: 'left', formatter: function(value, row, index){
				    	return Utils.XSSEncode(value);
				    }},
					{field: 'productInfoCode', title: 'Mã NHC', width: 150, sortable:false,resizable:false, align: 'left', formatter: function(value, row, index){
				    	return Utils.XSSEncode(value);
				    }},
				    {field: 'productInfoName', title: 'Tên NHC', width: 300, sortable:false,resizable:false, align: 'left', formatter: function(value, row, index){
				    	return Utils.XSSEncode(value);
				    }},
				    {field: 'productInfoId',title: '', checkbox:true, width:50,align:'center',sortable : false,resizable : false},			  
				  ]],
				  onSelect : function(rowIndex, rowData) {
				    	var selectedId = rowData['productInfoId'];
				    	GroupProductPOAutoNPP._mapMutilSelectSub.put(selectedId, rowData);
				    },
				    onUnselect : function(rowIndex, rowData) {
				    	var selectedId = rowData['productInfoId'];
				    	GroupProductPOAutoNPP._mapMutilSelectSub.remove(selectedId);
				    },
				    onSelectAll : function(rows) {
				    	if($.isArray(rows)) {
				    		for(var i = 0; i < rows.length; i++) {
				    			var row = rows[i];
				    			var selectedId = row['productInfoId'];
				    			GroupProductPOAutoNPP._mapMutilSelectSub.put(selectedId, row);
				    		}
				    	}
				    },
				    onUnselectAll : function(rows) {
				    	if($.isArray(rows)) {
				    		for(var i = 0; i < rows.length; i++) {
				    			var row = rows[i];
				    			var selectedId = row['productInfoId'];
				    			GroupProductPOAutoNPP._mapMutilSelectSub.remove(selectedId);
				    		}
				    	}
				    },
				   onLoadSuccess:function(){
				      	var easyDiv = '#gridSecGoodsPopBrandContainer ';
				    	$(easyDiv+'.datagrid-header-rownumber').html('STT');
				    	var exitsAll = false;
			    		var runner = 0;
			    		$(easyDiv+'input:checkbox[name=productInfoId]').each(function() {
			    			var selectedId = this.value;
			    			if(GroupProductPOAutoNPP._mapMutilSelectSub.get(selectedId) != null || GroupProductPOAutoNPP._mapMutilSelectSub.get(selectedId) != undefined) {
			    				exitsAll = true;
			    				$('#gridSecGoodsPopBrand').datagrid('selectRow',runner);
			    			} 
			    			runner ++; 
			    		});
			    		if (exitsAll==false){
			    			$(easyDiv+'td[field=productInfoId] .datagrid-header-check input:checkbox').attr('checked', false);
				    	}
				    	
			    		var tabindex = -1;
						$('.InputTextStyle, select, input, button , li , tr, td, th, label, ul, a, img').each(function () {
							if (this.type != 'hidden') {
								$(this).attr("tabindex", tabindex);
								tabindex -=1;
							}
						});
						tabindex = 1;
			    		 $('.easyui-dialog input,.easyui-dialog select,.easyui-dialog button').each(function () {
				    		 if (this.type != 'hidden') {
					    	     $(this).attr("tabindex", '');
								 tabindex++;
				    		 }
						 });
			    		updateRownumWidthForDataGrid('#gridSecGoodsPopBrand');
			    		$(window).resize();
				      }
			});
			
			return false;
		},
		addSubCategory:function(){
			$('#errMsgPopup2').html('').hide();
			if(GroupProductPOAutoNPP._mapMutilSelectSub == null || GroupProductPOAutoNPP._mapMutilSelectSub.size() <= 0) {
				$('#errMsgPopup2').html('Bạn chưa chọn ngành hàng con nào. Yêu cầu chọn').show();
        		return false;
        	}
	
			var mapKey = GroupProductPOAutoNPP._mapMutilSelectSub.keyArray;
			var mapValue = GroupProductPOAutoNPP._mapMutilSelectSub.valArray;
			var params = new Object();
			params.lstCategoryId = mapKey;
			GroupProductPOAutoNPP.updateSubCategory(params.lstCategoryId);
		},		
		searchSecGoods: function(){
			$('#errMsgPopup1').html('').hide();
			$('#errMsg').html('').hide();
			var code = $('#codeSecGoods').val().trim();
			var name = $('#nameSecGoods').val().trim();
			var tm = setTimeout(function() {
				$('#codeSecGoods').focus();
			}, 500);
			var objectType = 0;
			var url = GroupProductPOAutoNPP.getGridSectorUrl(objectType, name, code);
			$("#gridSecGoodsPop").datagrid({url:url,pageNumber:1});
			return false;
		},
		searchSecGoodsBrand: function(){
			$('#errMsgPopup2').html('').hide();
			$('#errMsg').html('').hide();
			var code = $('#codeSecGoodsBrand').val().trim();
			var name = $('#nameSecGoodsBrand').val().trim();
			var tm = setTimeout(function() {
				$('#codeSecGoodsBrand').focus();
			}, 500);
			var objectType = 1;
			var url = GroupProductPOAutoNPP.getGridSectorUrl(objectType, name, code);
			$("#gridSecGoodsPopBrand").datagrid({url:url,pageNumber:1});
//			$("#gridSecGoodsPopBrand").datagrid({
//				  url:url,
//			  	  pageList  : [10,20,30],
//				  width: 550,
//				  pageSize : 10,
//				  checkOnSelect :true,
//				  height:'auto',
//				  scrollbarSize : 0,
//				  pagination:true,			  
//				  fitColumns:true,		     
//			      singleSelect:true,
//				  method : 'GET',
//				  rownumbers: true,
//				  columns:[[
//				    {field: 'parentCode', title: 'Mã NH', width: 150, sortable:false,resizable:false, align: 'left'},
//					{field: 'productInfoCode', title: 'Mã NHC', width: 150, sortable:false,resizable:false, align: 'left'},
//				    {field: 'productInfoName', title: 'Tên NHC', width: 300, sortable:false,resizable:false, align: 'left'},
//				    {field: 'status',title: '<a href="javascript:void(0)" onclick="return GroupProductPOAutoNPP.openDialogUpdateSecGoodsBrand();"></a>', width:50,align:'center',sortable : false,resizable : false,
//		            	formatter:function(value,row){
//		            		if(row.status == 1){
//		            			return '<input type="checkbox" checked="checked" onClick=""/>';
//		            		}else{
//		            			return '<input type="checkbox" onClick="" />';
//		            		}
//		            	}
//				    }	   
//				  ]],
//				  onLoadSuccess:function(){
//						$('.datagrid-header-rownumber').html('STT');  
//				      	$('#gridSecGoodsPopBrand').datagrid('resize');
//				      }
//			});
			return false;

		},
		openDialogCreateGroupProductPOAutoNPP:function(flag,idPoAutoGroupShopMap,groupCode,groupName,status){
			GroupProductPOAutoNPP.reset();
			var arrayParam = new Array();
			arrayParam.push(flag);
			arrayParam.push(idPoAutoGroupShopMap);
			arrayParam.push(groupCode);
			arrayParam.push(groupName);
			arrayParam.push(status);
			GroupProductPOAutoNPP.openSearchStyle1EasyUICreateUpdateGroupPOAutoNpp(null,
					null, "Thông tin nhóm","groupCode",
					"groupName",'searchStyle1EasyUIDialogCreate','searchStyle1EasyUIDialogDiv',arrayParam);
//			$('#searchStyle1EasyUIDialogDiv #searchStyle1Code').focus();
		},
		openDialogUpdateGroupProductPOAutoNPP:function(flag,idPoAutoGroup,groupCode,groupName,status){
			GroupProductPOAutoNPP.reset();
			var arrayParam = new Array();
			arrayParam.push(flag);
			arrayParam.push(idPoAutoGroup);
			arrayParam.push(groupCode);
			arrayParam.push(groupName);
			arrayParam.push(status);
			GroupProductPOAutoNPP.openSearchStyle1EasyUICreateUpdateGroupPOAutoNpp(null,
					null, "Thông tin nhóm","groupCode",
					"groupName",'searchStyle1EasyUIDialogUpdate','searchStyle1EasyUIDialogDivUpdate',arrayParam);
//			$('#searchStyle1EasyUIDialogDivUpdate #searchStyle1Name').focus();
		},
		updateCategory:function(listCatID){
			var dataModel = new Object();
			dataModel.id = $('#idCode1').val();
			dataModel.lstCategoryId = listCatID;
			Utils.addOrSaveData(dataModel, "/catalog/group-product-po-auto-npp/addPOAutoGroupDetail", null, 'errMsg',function(data){	
				if(data.errMsg!=undefined && data.errMsg.length==0){
					$('#popup1').dialog('close');
					$("#gridSecGoods").datagrid("reload");
				}
			});
		},
		updateSubCategory:function(listCatID){
			var dataModel = new Object();
			dataModel.id = $('#idCode2').val();
			dataModel.lstCategoryId = listCatID;
			Utils.addOrSaveData(dataModel, "/catalog/group-product-po-auto-npp/addPOAutoGroupDetailSubCat", null, 'errMsg',function(data){	
				if(data.errMsg!=undefined && data.errMsg.length==0){
					$('#popup2').dialog('close');
					$("#gridSecGoodsBrand").datagrid("reload");
				}
			});
		},
		deleteCategoryRow: function(groupDetailId){
			$('#errMsg').html('').hide();
			var dataModel = new Object();
			dataModel.groupDetailId = groupDetailId;	
			Utils.deleteSelectRowOnGrid(dataModel, 'Ngành hàng', "/catalog/group-product-po-auto-npp/deleteCategoryRow", GroupProductPOAutoNPP._xhrDel, null, null,function(data){
				if(data.error == false){
					$("#gridSecGoods").datagrid("reload");
				}
			});
			return false;		
		},
		deleteSubCategoryRow: function(groupDetailId){
			$('#errMsg').html('').hide();
			var dataModel = new Object();
			dataModel.groupDetailId = groupDetailId;	
			Utils.deleteSelectRowOnGrid(dataModel, 'Ngành hàng con', "/catalog/group-product-po-auto-npp/deleteSubCategoryRow", GroupProductPOAutoNPP._xhrDel, null, null,function(data){
				if(data.error == false){
					$("#gridSecGoodsBrand").datagrid("reload");
				}
			});
			return false;		
		},
		openSearchStyle1EasyUICreateUpdateGroupPOAutoNpp : function(codeText, nameText, title, codeFieldText, nameFieldText, searchDialog,searchDiv,arrayParam) {
			CommonSearchEasyUI._arrParams = null;
			CommonSearchEasyUI._currentSearchCallback = null;
			$('#'+searchDiv).css("visibility", "visible");
			var html = $('#'+searchDiv).html();
			$('#'+searchDialog).dialog({  
		        title: title,  
		        closed: false,  
		        cache: false,  
		        modal: true,
		        width : 616,
		        height :'auto',
		        onOpen: function(){
		        	//@CuongND : my try Code <reset tabindex>
		        	var tabindex = -1;
					$('.InputTextStyle, select, input, button , li , tr, td, th, label, ul, a, img').each(function () {
						if (this.type != 'hidden') {
							$(this).attr("tabindex", tabindex);
							tabindex -=1;
						}
					});
					tabindex = 1;
		    		 $('.easyui-dialog input,.easyui-dialog select,.easyui-dialog button').each(function () {
			    		 if (this.type != 'hidden') {
				    	     $(this).attr("tabindex", '');
							 tabindex++;
			    		 }
					 });
					//end <reset tabindex>
					//$('.easyui-dialog #searchStyle1Code').focus();
					$('.easyui-dialog #searchStyle1CodeText').val(codeFieldText);
					$('.easyui-dialog #searchStyle1NameText').val(nameFieldText);
					
					var codeField = 'code';
					var nameField = 'name';
					if (codeFieldText != null && codeFieldText != undefined && codeFieldText.length > 0) {
						codeField = codeFieldText;
					}
					if (nameFieldText != null&& nameFieldText != undefined && nameFieldText.length > 0) {
						nameField = nameFieldText;
					}
					
//					$('.easyui-dialog #seachStyle1CodeLabel').html(codeText);
//					$('.easyui-dialog #seachStyle1NameLabel').html(nameText);
					
					if(arrayParam != null && arrayParam[0] == 1){
						var code = arrayParam[2];
						var name = arrayParam[3];
						var status = arrayParam[4];
						$('.easyui-dialog #seachStyle1Code').attr('disabled','disabled');
						$('.easyui-dialog #seachStyle1Code').val(code);
						$('.easyui-dialog #seachStyle1Name').val(name);
						$('.easyui-dialog #seachStyle1Name').focus();
						$('.easyui-dialog #statusDialogUpdate option[value='+status+']').attr('selected','selected');
						var value = $('.easyui-dialog #statusDialogUpdate option[value='+status+']').html();
						$('.easyui-dialog .CustomStyleSelectBoxInner').html(value);
					}
					if(arrayParam != null && arrayParam[0] == 0){
						var value = $('.easyui-dialog #statusDialog option[selected=selected]').html();
						$('.easyui-dialog .CustomStyleSelectBoxInner').html(value);
						$('.easyui-dialog #seachStyle1Code').removeAttr('disabled').focus();
					}
				
					$('.easyui-dialog #__btnSave').bind('click',function(event) {
						var s = -1;
						var code = $('.easyui-dialog #seachStyle1Code').val().trim();
						var name = $('.easyui-dialog #seachStyle1Name').val().trim();
						var msg = "";
						if(code == undefined || code == "" || code == null){
							msg = "Bạn chưa nhập giá trị cho trường Mã nhóm.";
							$('.easyui-dialog #errMsgSearchCreate').html(msg).show();
							$('.easyui-dialog #seachStyle1Code').focus();
							return false;
						}
						if(msg.length == 0){
							msg = Utils.getMessageOfSpecialCharactersValidateEx1('.easyui-dialog #seachStyle1Code', 'Mã nhóm', Utils._CODE, null);
							if(msg.length >0){
								$('.easyui-dialog #errMsgSearchCreate').html(msg).show();
								$('.easyui-dialog #seachStyle1Code').focus();
								return false;
							}
						}
						if(msg.length == 0){
							if(name == undefined || name == "" || name == null){
								msg = "Bạn chưa nhập giá trị cho trường Tên nhóm.";
								$('.easyui-dialog #errMsgSearchCreate').html(msg).show();
								$('.easyui-dialog #seachStyle1Name').focus();
								return false;
							}
						}
						if(msg.length == 0){
							msg = Utils.getMessageOfSpecialCharactersValidateEx1('.easyui-dialog #seachStyle1Name', 'Tên nhóm', Utils._NAME, null);
							if(msg.length >0){
								$('.easyui-dialog #errMsgSearchCreate').html(msg).show();
								$('.easyui-dialog #seachStyle1Name').focus();
								return false;
							}
						}
						if(arrayParam != null){
							s = arrayParam[0];
						}
						if(s == 0){
							var params = new Object();
							params.id = GroupProductPOAutoNPP.id;//idPoAutoGroupShopMap
							params.groupCode = code;
							params.groupName = name;
							params.statusValue = $('.easyui-dialog #statusDialog').val();
							Utils.addOrSaveData(params, '/catalog/group-product-po-auto-npp/creategrouppo', null, 'errMsgSearchCreate', function(data){
								$('#'+searchDialog).dialog('close'); 
								GroupProductPOAutoNPP.afterCreateUpdatePoAutoGroup();
							}, null, null, null, "Bạn có muốn thêm nhóm mới không ?", null);
						}
						
					});
					$('.easyui-dialog #__btnSave1').bind('click',function(event) {
						var s = -1;
						var code = $('#searchStyle1EasyUIDialogUpdate #seachStyle1Code').val().trim();
						var name = $('#searchStyle1EasyUIDialogUpdate #seachStyle1Name').val().trim();
						var msg = "";
						if(code == undefined || code == "" || code == null){
							msg = "Bạn chưa nhập giá trị cho trường Mã nhóm.";
							$('.easyui-dialog #errMsgSearchUpdate').html(msg).show();
							$('#searchStyle1EasyUIDialogUpdate #seachStyle1Code').focus();
							return false;
						}
						if(msg.length == 0){
							msg = Utils.getMessageOfSpecialCharactersValidateEx1('#searchStyle1EasyUIDialogUpdate #seachStyle1Code', 'Mã nhóm', Utils._CODE, null);
							if(msg.length >0){
								$('.easyui-dialog #errMsgSearchUpdate').html(msg).show();
								$('#searchStyle1EasyUIDialogUpdate #seachStyle1Code').focus();
								return false;
							}
						}
						if(msg.length == 0){
							if(name == undefined || name == "" || name == null){
								msg = "Bạn chưa nhập giá trị cho trường Tên nhóm.";
								$('.easyui-dialog #errMsgSearchUpdate').html(msg).show();
								$('#searchStyle1EasyUIDialogUpdate #seachStyle1Name').focus();
								return false;
							}
						}
						if(msg.length == 0){
							msg = Utils.getMessageOfSpecialCharactersValidateEx1('#searchStyle1EasyUIDialogUpdate #seachStyle1Name', 'Tên nhóm', Utils._NAME, null);
							if(msg.length >0){
								$('.easyui-dialog #errMsgSearchUpdate').html(msg).show();
								$('#searchStyle1EasyUIDialogUpdate #seachStyle1Name').focus();
								return false;
							}
						}
						if(arrayParam != null){
							s = arrayParam[0];
						}
						if(s == 1){
							var params = new Object();
							params.id = arrayParam[1];
							var name = $('#searchStyle1EasyUIDialogUpdate #seachStyle1Name').val().trim();
							params.groupName = name;
							params.statusValue = $('.easyui-dialog #statusDialogUpdate').val();
							Utils.addOrSaveData(params, '/catalog/group-product-po-auto-npp/updategrouppo', null, 'errMsgSearchUpdate', function(data){
								$('#'+searchDialog).dialog('close');
									GroupProductPOAutoNPP.afterCreateUpdatePoAutoGroup();
							}, null, null, null, "Bạn có muốn cập nhật thông tin nhóm không ?", null);
						}
						
					});
		        },
		        onBeforeClose: function() {
					var tabindex = 1;
					$(' .InputTextStyle, select, input, button , li , tr, td, th, label, ul, a, img').each(function () {
						if (this.type != 'hidden') {
							$(this).attr("tabindex", '');
						}
						tabindex ++;
					});	
					var curIdFocus = $('#cur_focus').val();
					$('#'+ curIdFocus).focus();
		        },
		        onClose : function(){
		        	$('#'+searchDiv).html(html);
		        	$('#'+searchDiv).css("visibility", "hidden");
		        	$('.easyui-dialog #seachStyle1Code').val('');
		        	$('.easyui-dialog #seachStyle1Name').val('');
		        	$('.easyui-dialog #__btnSave').unbind('click');
		        	$('.easyui-dialog #__btnSave1').unbind('click');
		        	$('.easyui-dialog #errMsgSearchCreate').html('').hide();
		        	$('.easyui-dialog #errMsgSearchUpdate').html('').hide();	
		        }
		    });
			return false;
		}
		
};
/*
 * END OF FILE - /web/web/resources/scripts/business/catalog/vnm.group-product-po-auto-npp-catalog.js
 */

/*
 * START OF FILE - /web/web/resources/scripts/business/catalog/vnm.program-document.js
 */
var ProgramDocument = {
	_listShopId:null,
	_listShopIdMien:null,
	_listShopIdCheck:null,
	_listFull:null,
	_listQuantity:null,
	_arrParams : null,
	_demtang : null,
	search:function(){
		var typeDocument = $('#typeDocument').val().trim();
		var msg = '';
		var err = '';
		var documentCode =$('#documentCode').val().trim();
		var documentName = $('#documentName').val().trim();
		var fromDate = $('#fromDate').val().trim();
		var toDate =$('#toDate').val().trim();
		
		if(msg.length == 0 && $('#fromDate').val().trim().length>0 && !Utils.isDate($('#fromDate').val(),'/')){
			msg = 'Từ ngày không tồn tại hoặc không đúng định dạng dd/mm/yyyy.';
			err = '#fromDate';
		}
		if(msg.length == 0 && $('#toDate').val().trim().length>0 && !Utils.isDate($('#toDate').val(),'/')){
			msg = 'Đến ngày không tồn tại hoặc không đúng định dạng dd/mm/yyyy.';
			err = '#toDate';
		}
		if(msg.length==0 && fromDate.length>0 && toDate.length>0 && !Utils.compareDate(fromDate, toDate)){
			msg = 'Từ ngày phải là ngày trước hoặc cùng ngày với đến ngày';
		}
		if(msg.length>0){
			$('#errMsg').html(msg).show();
			$(err).focus();
			setTimeout(function(){$('#errMsg').hide();},3000);
			return false;
		}
		
		$('#typeDocumentHide').val($('#typeDocument').val().trim());
		$('#documentCodeHide').val($('#documentCode').val().trim());
		$('#documentNameHide').val($('#documentName').val().trim());
		$('#fromDateHide').val($('#fromDate').val().trim());
		$('#toDateHide').val($('#toDate').val().trim());
		
		$('#dg').datagrid('load',{
			typeDocument:typeDocument,
			/*documentCode:encodeChar(documentCode),
			documentName:encodeChar(documentName),*/
			documentCode: documentCode,
			documentName: documentName,
			fromDate:fromDate,
			toDate:toDate
		});
		return false;
	},
	
	exportExcelOfficeDocument:function(){		
		var typeDocument = $('#typeDocumentHide').val().trim();
		var documentCode =$('#documentCodeHide').val().trim();
		var documentName = $('#documentNameHide').val().trim();
		var fromDate = $('#fromDateHide').val().trim();
		var toDate =$('#toDateHide').val().trim();
		$.messager.confirm('Xác nhận','Bạn có muốn xuất Excel không?',function(r){
			if(r){
				var params = new Object();
				params.typeDocument = typeDocument;
				params.documentCode = documentCode;
				params.documentName = documentName;
				params.fromDate = fromDate;
				params.toDate = toDate;
				
				var url = '/document/exportExcelOficedocument';
				ProgramDocument.exportExcelData(params,url,'errExcelMsg');
				return false; 
			}
		});	
		return false;
	},
	importPDFFile: function(){
		var msg = '';
		var err = '';
		
		//Check is Null
		if(msg.length == 0){
			msg = Utils.getMessageOfRequireCheck('documentCodeDlg','Mã CV');
			err = '#documentCodeDlg';
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfRequireCheck('documentNameDlg','Tên CV');
			err = '#documentNameDlg';
		}
		if(msg.length == 0 && $('#typeDocumentDlg').val().trim().length >0){
			var typeInt = parseInt($('#typeDocumentDlg').val().trim());
			if( typeInt == -2){
				msg = 'Chưa chọn loại CV';
				err = '#typeDocumentDlg';
			}
		}
		
		if(msg.length == 0 && $('#fromDateDlg').val().trim().length>0){
			msg = Utils.getMessageOfEmptyDate('fromDateDlg', 'Từ ngày');
			err = '#fromDateDlg';
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfRequireCheck('fromDateDlg','Từ ngày');
			err = '#fromDateDlg';
		}
		if(msg.length == 0 && $('#fromDateDlg').val().trim().length>0 && !Utils.isDate($('#fromDateDlg').val(),'/')){
			msg = 'Từ ngày không tồn tại hoặc không đúng định dạng dd/mm/yyyy.';
			err = '#fromDateDlg';
		}
		if(msg.length == 0 && $('#fromDateDlg').val().trim().length>0){
			msg = Utils.getMessageOfInvalidFormatDate('fromDateDlg', 'Từ ngày');
			err = '#fromDateDlg';
		}
		
		if(msg.length == 0 && $('#toDateDlg').val().trim().length>0 && !Utils.isDate($('#toDateDlg').val(),'/')){
			msg = 'Đến ngày không tồn tại hoặc không đúng định dạng dd/mm/yyyy.';
			err = '#toDateDlg';
		}
		
		if(msg.length == 0 && $('#toDateDlg').val().trim().length>0){
			msg = Utils.getMessageOfInvalidFormatDate('fromDateDlg', 'Đến ngày');
			err = '#toDateDlg';
		}
		
		if(msg.length == 0 && $('#fromDateDlg').val()!=null && $('#toDateDlg').val()!=null){
			if(!Utils.compareDate($('#fromDateDlg').val().trim(), $('#toDateDlg').val().trim())){
				msg = 'Từ ngày phải nhỏ hơn ngày đến ngày';
				err = '#fromDateDlg';
			}
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfRequireCheck('fakePdffilepc','Chọn File');
			if(msg.length>0){
				msg = 'Chưa chọn tập tin PDF';
			}
			else{
				var str = $('#fakePdffilepc').val().trim();
				var n = str.substring(str.length-3,str.length).toUpperCase();
				if(n!=='PDF'){
					msg = 'Không đúng loại định dạng PDF';
				}
			}
			
		}
		//Check validate
		if(msg.length == 0){
			msg = Utils.getMessageOfSpecialCharactersValidateEx1('#documentCodeDlg','Mã CV',Utils._CODE);
			err = '#documentCodeDlg';
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfSpecialCharactersValidateEx1('#documentNameDlg','Tên CV',Utils._NAME);
			err = '#documentNameDlg';
		}
		if(msg.length>0){
			$('#errPdfMsg').html(msg).show();
			$(err).focus();
			setTimeout(function(){$('#errPdfMsg').hide();},4000);
			return false;
		}
		
		if(!uploadPdfFile(document.getElementById("pdfFile"))){
			return false;
		}
		
		var options = {
				beforeSubmit : ProgramDocument.beforeUploadPdf,
				success : ProgramDocument.afterImportExcel,
				type : "POST",
				dataType : 'html'
			};
			$('#easyuiPopupUpload #importFrm').ajaxForm(options);
			$.messager.confirm('Xác nhận', 'Bạn có muốn lưu thông tin này ?',	function(r) {
				if (r) {
					try{
						$('#importFrm').submit();}
					catch(e) {
					}
				}
			});
 		return false;
	},
	deleteOfficeDocument:function(id){
		$.messager.confirm('Xác nhận', 'Bạn có muốn xoá công văn không?', function(r){
			if (r){
				var param = new Object();
				param.idOfficeDoc = id;
				Utils.saveData(param, '/document/delete-OfficeDocument', null, 'errMsg', function(result){
					$('#dg').datagrid('reload');
					$('#successExcelMsg').html('Xóa dữ liệu thành công').show();
					setTimeout(function(){
						$('#successExcelMsg').html('');
						$('#successExcelMsg').hide();
		    		},2000);
				});
			}
		});
	},
	importPdfFileAction:function(dataModel,url,errMsg,typeErr){
		var kData = $.param(dataModel,true);
		if(CommonSearch._xhrReport != null){
			CommonSearch._xhrReport.abort();
			CommonSearch._xhrReport = null;
		}
		$('#divOverlay').show();
		CommonSearch._xhrReport = $.ajax({
			type : "POST",
			url : url,
			data: (kData),
			dataType: "json",
			success : function(data) {
				$('#divOverlay').hide();
				if(data.error && data.errMsg!= undefined){
					$('#' + errMsg).html(Utils.XSSEncode(data.errMsg)).show();
					setTimeout(function(){
						$('#' + errMsg).hide();
					}, 4000);
				} else{
					if(data.hasData!= undefined && !data.hasData) {
						$('#errPdfMsg').html('Không tải được tập tin').show();
						setTimeout(function(){
							$('#errPdfMsg').hide();
						},3500);
					} else {
						$('#resultPdfMsg').html('Lưu dữ liệu thành công').show();
						$('#dg').datagrid('reload');
						setTimeout(function() { // Set timeout để
							$('#resultPdfMsg').hide();
							$('#easyuiPopupUpload').dialog('close');
						},2000);
					}
				}
			},
			error:function(XMLHttpRequest, textStatus, errorDivThrown) {
				$.ajax({
					type : "POST",
					url : '/check-session',
					dataType : "json",
					success : function(data) {
						CommonSearch._xhrReport = null;
					},
					error: function(XMLHttpRequest, textStatus, errorThrown) {
						window.location.href = '/home';
					}
				});
			}
		});
		return false;
	},
	viewDetail:function(documentId){
//		var html = $('#easyuiPopupUpload').html();
		var params = new Object();
		var width = 800;
		
		params.idOfficeDoc = documentId;
		Utils.getJSONDataByAjax(params, '/document/viewDetail-OfficeDocument',function(data) {
			if(data.widthImage > width && data.widthImage<980){
				width =data.widthImage + 20; 
			}
			if(data.urlImage!=null && data.urlImage!= undefined && data.urlImage.length>0){
				$('#imgDocument').attr('src',data.urlImage);
			} else{
//				$('#easyuiPopup').dialog('close');
			}
		}, null, 'POST');
		$('#easyuiPopup').dialog({  
	        closed: false,  
	        cache: false,
	        modal: true,
	        width:width,
	        height:600,
	        onOpen: function(){	        	
				$('#rotateButton').unbind('click');
				$('#rotateButton').bind('click', function() {
					ProgramDocument.rotateDoc(documentId);
				});
	        },
	        buttons:'#buttonViewDocument',
			onClose:function(){
	        	$('#contentImgDocument').html('<img id="imgDocument" src="" />');
	        }
		});
	},
	rotateDoc: function(docId) {
		var url = '/document/rotate-doc';
		var params = new Object();
		params.docId = docId;
		kData = $.param(params);
		$.ajax({
			type : "POST",
			url : url,
			data :(kData),
			dataType : "json",
			success : function(data) {
				var width = 800;
				var curDate = new Date;
				var curTime = curDate.getTime();
				if(data.widthImage > width && data.widthImage<980){
					width =data.widthImage + 20; 
				}
				if(data.urlImage!=null && data.urlImage!= undefined && data.urlImage.length>0){
					$('#imgDocument').attr('src',data.urlImage+'?v=' +curTime);
				}
				$('#easyuiPopup').dialog({width : width});
			},
			error:function(XMLHttpRequest, textStatus, errorThrown) {
				$.ajax({
					type : "POST",
					url : '/check-session',
					dataType : "json",
					success : function(data, textStatus, jqXHR) {
					},
					error: function(XMLHttpRequest, textStatus, errorThrown) {
						window.location.href = '/home';
					}
				});
			}
		});
	},
	deleteRow:function(){
		return false;
	},
	exportExcel:function(){
		
	},
	uploadDoc:function(){	
		$('#easyuiPopupUpload').css("visibility", "visible");
		var html = $('#easyuiPopupUpload').html();
		
		ProgramDocument.refreshPopupUpload();
		$('#easyuiPopupUpload').dialog({  
	        closed: false,  
	        cache: false,  
	        modal: true,
	        width:550,
	        height:300,
	        onOpen: function(){
	        	$('#documentCodeDlg').focus();
	        	setDateTimePicker('fromDateDlg'); 
	        	setDateTimePicker('toDateDlg');
	        	//ReportUtils.setCurrentDateForCalendar('fromDateDlg');
	        	//ReportUtils.setCurrentDateForCalendar('toDateDlg');
	        },onClose:function(){
	        	$('#easyuiPopupUpload').html(html);
	        	$('#easyuiPopupUpload').css("visibility", "hidden");
	        	//$('#documentCodeDlg').focus();
	        	$('#documentCodeDlg').val('').show();
	        	$('#documentNameDlg').val('').show();
	        	$('#fromDateDlg').val('').show();
	        	$('#toDateDlg').val('').show();
	        	$('#fakePdffilepc').val('').show();
	        }
		});
	},	
	refreshPopupUpload : function(){
		var typeValue = -2;
		$('#documentCodeDlg').val('').show();
		$('#documentNameDlg').val('').show();
		$('#typeDocumentDlg').find("option[value=" + typeValue +']').attr('selected','selected');
//		$('#fromDateDlg').val(ProgramDocument.currentDate()).show();
//		$('#toDateDlg').val(ProgramDocument.currentDate()).show();
		$('#fromDateDlg').val('').show();
		$('#toDateDlg').val('').show();
		$('#fakePdffilepc').val('').show();
		$('#errPdfMsg').hide();
		$('#resultPdfMsg').hide();
//		$('#fakePdffilepc').attr('disabled','disabled');
		$('#documentNameDlg').focus();
	},
	uploadExecute : function() {
		var options = {
			beforeSubmit : ReportUtils.beforeImportExcel,
			success : ReportUtils.afterImportExcel,
			type : "POST",
			dataType : 'html'
		};
		$('#easyuiPopupUpload #importFrm').ajaxForm(options);
		$.messager.confirm('Xác nhận', 'Bạn có muốn lưu thông tin này ?',	function(r) {
			if (r) {
				$('#easyuiPopupUpload #importFrm').submit();
			}
		});
	},
	beforeUploadPdf:function(){
//		if(!uploadPdfFile(document.getElementById("pdfFile"))){
//			return false;
//		}
		try{
			$('#checkSubmit').val(1);
			$('#errPdfMsg').html('').hide();
			$('#resultPdfMsg').html('').hide();		
			$('#divOverlay').show();
		}catch(e){}
	},
	afterImportExcel: function(responseText, statusText, xhr, $form){
		$('#divOverlay').hide();
		if (statusText == 'success') {
			$("#responseDiv").html(responseText);
			updateTokenImport();
			var errMsg = $('#sverrMsg').val().trim();
			if(errMsg!=undefined && errMsg.length>0){
				$('#errPdfMsg').html(errMsg).show();
				setTimeout(function(){$('#errPdfMsg').hide();},3500);
				return false;
			}
			else{
				$('#resultPdfMsg').html('Lưu dữ liệu thành công').show();
				$('#dg').datagrid('reload');			
				setTimeout(function(){
					$('#resultPdfMsg').hide();
					$('#easyuiPopupUpload').dialog('close');
				},4000);				
			}
			
		}
	},
	closeWindow:function(){
		$('#easyuiPopupUpload').dialog('close');
	},	
	currentDate:function() {
		var now = new Date();
		var cYear = now.getFullYear();
		var cMonth = now.getMonth() + 1;
		var cDate = now.getDate();
		var currentDate = cDate+'/' +cMonth+'/' +cYear;
		return currentDate;
	},
	
	onchangeType:function() {
		$('#fromDateDlg').focus();
		return false;
	},
	
	exportExcelData:function(dataModel,url,errMsg){
		if(errMsg == null || errMsg == undefined){
			errMsg = 'errExcelMsg';
		}
		var kData = $.param(dataModel,true);
		if(CommonSearch._xhrReport != null){
			CommonSearch._xhrReport.abort();
			CommonSearch._xhrReport = null;
		}
		CommonSearch._xhrReport = $.ajax({
			type : "POST",
			url : url,
			data: (kData),
			dataType: "json",
			success : function(data) {
				hideLoadingIcon();
				//window.location.href = data.view;
				if(data.error!=false && data.errMsg!= undefined){
					$('#errExcelMsg').html('Xuất báo cáo không thành công. Lỗi: ' + Utils.XSSEncode(data.errMsg)).show();
					setTimeout(function(){$('#errExcelMsg').hide();},3000);
				} else{
					if(data.hasData!= undefined && data.hasData!=true) {
						$('#errExcelMsg').html('Không có dữ liệu để xuất báo cáo').show();
						setTimeout(function(){$('#errExcelMsg').hide();},3000);
					} else{
						window.location.href = data.view;
					}
				}
			},
			error:function(XMLHttpRequest, textStatus, errorDivThrown) {
				$.ajax({
					type : "POST",
					url : '/check-session',
					dataType : "json",
					success : function(data) {
						CommonSearch._xhrReport = null;
					},
					error: function(XMLHttpRequest, textStatus, errorThrown) {
						window.location.href = '/home';
					}
				});
			}
		});
		return false;
	},
	editOfficeDocumentShopMap:function(id){
//		var arrParam = new Array();
		ProgramDocument._listShopId = new Array();
		ProgramDocument._listShopIdCheck = new Array();
		ProgramDocument._listFull = new Array();
		ProgramDocument._listShopIdMien = new Array();
		ProgramDocument._demtang = 0;
		var shopCode = $('#seachStyleShopCode').val().trim();
		var shopName = $('#seachStyleShopName').val().trim();
// 		var param3 = new Object();
// 		param3.promotionProgramCode = shopCode;
// 		param3.promotionProgramName = shopName;
// 		param3.idOfficeDoc = id;
 		$('#idDocumentIDGrid').val(id);
// 		arrParam.push(param3);
 		ProgramDocument.searchShopDialog(function(data){});
 		ProgramDocument.bindFormat();
 		
 		
	},
	searchShopDialog:function(callback) {
		ProgramDocument.openDialogSearchShop("Mã đơn vị",
				"Tên đơn vị", "Chọn đơn vị",
				"/document/searchShopDialog", callback, "promotionProgramCode",
				"promotionProgramName",'searchStyleShopDisplay','searchStyleShopDisplay1','display');
		return false;
	},
	bindFormat:function(){
		$('#searchStyleShopContainerGrid input[type=text]').each(function(){
				Utils.bindFormatOnTextfieldEx1($(this).attr('id'),Utils._TF_NUMBER,null,'#errMsgDialog','Số suất phải là số nguyên dương.');
			}); 
	},
	openDialogSearchShop : function(codeText, nameText, title, url, callback, codeFieldText, nameFieldText,searchDiv,searchDialog,program) {
		CommonSearchEasyUI._arrParams = null;
		CommonSearchEasyUI._currentSearchCallback = null;
		var html = $('#' +searchDiv).html();
		$('#' +searchDialog).dialog({  
	        title: title,  
	        closed: false,  
	        cache: false,  
	        modal: true,
	        width : 616,
	        height :'auto',
	        onOpen: function(){
	        	Utils.bindAutoSearch();
				$('.easyui-dialog #seachStyleShopCode').focus();
				$('.easyui-dialog #searchStyleShopUrl').val(url);
				$('.easyui-dialog #searchStyleShopCodeText').val(codeFieldText);
				$('.easyui-dialog #searchStyleShopNameText').val(nameFieldText);
				CommonSearch._currentSearchCallback = callback;
				// CommonSearch._arrParams = arrParam;
				//$('#loadingTree').show();
				$('#searchStyleShopContainerGrid').html('<table id="searchStyleShopGrid" class="easyui-treegrid"></table>').show();
				var codeField = 'code';
				var nameField = 'name';
				if (codeFieldText != null && codeFieldText != undefined && codeFieldText.length > 0) {
					codeField = codeFieldText;
				}
				if (nameFieldText != null&& nameFieldText != undefined && nameFieldText.length > 0) {
					nameField = nameFieldText;
				}
				
				var shopCode = $('#seachStyleShopCode').val().trim();
				var shopName = $('#seachStyleShopName').val().trim();
				var uRL = ProgramDocument.getSearchStyleDocShopMapGridUrl('/document/searchShopDialog',shopCode,shopName);
				$('.easyui-dialog #seachStyleShopCodeLabel').html(codeText);
				$('.easyui-dialog #seachStyleShopNameLabel').html(nameText);
				$('.easyui-dialog #searchStyleShopGrid').show();
				$('.easyui-dialog #searchStyleShopGrid').treegrid({  
				    url:  uRL,
				    width:($('#searchStyleShopContainerGrid').width()-20),  
			        height:400,
			        fitColumns : true,
			        idField: 'id',
			        treeField: 'data',
				    columns:[[  
				        {field:'data',title:'Đơn vị',resizable:false,width:530, formatter: function(data){
				        	return Utils.XSSEncode(data);
				        }}, 
				        {field:'edit',title:'',width:30,align:'center',resizable:false,formatter:function(value,row,index){
				        	var classParent = "";
				        	var parentNodeG = $('#shopIDAd').val();
				        	var parentIDTemp = null;
				        	if(row.parentId != null && row.parentId != undefined){
				        		parentIDTemp = row.parentId;
				        		classParent = "class_" +row.parentId;
				        		if(row.parentId == parentNodeG){
				        			ProgramDocument._listShopIdMien.push(row.id);
				        		}
				        	}
				        	if(row.isJoin==1 || (row.isChildJoin==1 && row.isJoin>1)){
				        		ProgramDocument._listShopId.push(row.id);
				        		return '<input type ="checkbox" class="' +Utils.XSSEncode(classParent)+'" checked="true" value="' +row.id+'" id="check_' +row.id+'" onclick="return ProgramDocument.onCheckShop(' +row.id+',' + parentIDTemp+');">';
				        	}
				        	return '<input type ="checkbox" class="' +Utils.XSSEncode(classParent)+'" value="' +row.id+'" id="check_' +row.id+'" onclick="return ProgramDocument.onCheckShop(' +row.id+',' + parentIDTemp+');">';
				        }}
				    ]],
				    onLoadSuccess:function(row,data){
				    	$(window).resize();
				    	if(ProgramDocument._listShopIdMien!=null && ProgramDocument._listShopIdMien.length>0){
				    		for(var i=0; i< ProgramDocument._listShopIdMien.length; i++){
				    			ProgramDocument.oncheckedClickMouse(ProgramDocument._listShopIdMien[i]);
				    		}
				    	}
				    	//Uncheck NodeG
				    	var idShopG = $('#shopIDAd').val().trim();
				    	if(idShopG!=undefined && idShopG!=null){
				    		$('.easyui-dialog #check_' +idShopG).removeAttr('checked');
				    	}
				    	$(this).treegrid('expandAll');
				    }
				});
				$('.easyui-dialog #btnSearchStyleShop').bind('click',function(event) {
						$('.easyui-dialog #errMsgDialog').html('').hide();
						var code = $('.easyui-dialog #seachStyleShopCode').val().trim();
						var name = $('.easyui-dialog #seachStyleShopName').val().trim();
						var url = '/document/searchShopDialog';
						if(ProgramDocument._listShopId != null && ProgramDocument._listShopId != undefined && ProgramDocument._listShopId.length > 0){
							var listSize = ProgramDocument._listShopId.length;
							for(var i=0;i<listSize;i++){
								var value = $('.easyui-dialog #txtQuantity_' +ProgramDocument._listShopId[i]).val();
								if(value != null && value != undefined){
									ProgramDocument._listQuantity.push(value);	
								}
							}
						}
						var shopCode = $('#seachStyleShopCode').val().trim();
						var shopName = $('#seachStyleShopName').val().trim();
						var idOfficeDoc = $('#idDocumentIDGrid').val().trim();
						var arrParam = new Array();
						var param = new Object();
				 		param.promotionProgramCode = shopCode;
				 		param.promotionProgramName = shopName;
				 		param.idOfficeDoc = idOfficeDoc;
				 		arrParam.push(param);
				 		var urlSearch =  ProgramDocument.getSearchStyleDocShopMapGridUrl('/document/searchShopDialog', shopCode, shopName);
				 		$('.easyui-dialog #searchStyleShopGrid').treegrid({url:urlSearch});
				 		
				});
//				setTimeout(function(){$('#loadingTree').hide();},1000);
				
	        },
	        onBeforeClose: function() {
				var tabindex = 1;
				$(' .InputTextStyle, select, input, button , li , tr, td, th, label, ul, a, img').each(function () {
					if (this.type != 'hidden') {
						$(this).attr("tabindex", '');
					}
					tabindex ++;
				});	
				var curIdFocus = $('#cur_focus').val();
				$('#' + curIdFocus).focus();				
	        },
	        onClose : function(){
	        	$('#' +searchDiv).html(html);
	        	$('.easyui-dialog #seachStyleShopCode').val('');
	        	$('.easyui-dialog #seachStyleShopName').val('');
	        	$('.easyui-dialog #errMsgDialog').html('').hide();
	        	ProgramDocument._listShopId = new Array();
	        	ProgramDocument._listQuantity = new Array();
	        	ProgramDocument._listShopIdMien = new Array();
	        }
	    });
		return false;
	},
	
	onCheckShop:function(id, parentId){
		if(parentId==null || parentId==undefined){
			parentId = $('#shopIDAd').val();
		}
		 var isCheck = $('.easyui-dialog #check_' +id).attr('checked');
		    if($('.easyui-dialog #check_' +id).is(':checked')){
		    	
		    	var flag = false;
				for(var j=0;j<ProgramDocument._listShopId.length;j++){
					if(ProgramDocument._listShopId[j] == id){
						flag = true;
						break;
					}
				}
				if(flag!=true){
					ProgramDocument._listShopId.push(id);
				}
		    	
		    	var classParent = $('.easyui-dialog #check_' +id).attr('class');
				var count = 0;
				ProgramDocument.recursionFindChildShopCheck(id);
				ProgramDocument.oncheckedClickMouse(parentId);
			}else{
				ProgramDocument.removeArrayByValue(ProgramDocument._listShopId,id);
				var classParent = $('.easyui-dialog #check_' +id).attr('class');
				var flag = true;
				var count = 0;
				ProgramDocument.recursionFindChildShopUnCheck(id); 
				$('.easyui-dialog #check_' +parentId).removeAttr('checked');
			}
	},
	
	oncheckedClickMouse:function(parentId){
		var rows = $('.easyui-dialog .class_' +parentId);
		var flag =1;
		for(var i=0; i<rows.length; i++){
			if(rows[i].checked!=true){
				flag =0;
				break;
			}
		}
		if(flag!=0){
			$('.easyui-dialog #check_' +parentId).attr('checked', 'checked');
		}
	},
	recursionFindChildShopCheck:function(id){
		if(id == null || id == undefined){
			return;
		}
    	$.ajax({
    		type : "POST",
			url : '/document/getChildShopId',
			data: ({shopId:id}),
			dataType: "json",
			success : function(data) {
				if(data != null && data != undefined && data.listShopId != null && data.listShopId != undefined){
					var length = data.length;
					var arrayShopId = data.listShopId;
					if(ProgramDocument._listShopId==null || ProgramDocument._listShopId.length <=0){
						for(var i=0;i<length;i++){
								ProgramDocument._listShopId.push(data.listShopId[i]);
						}
					}else{
						//Thuc hien kiem tra truoc khi them moi
						for(var i=0;i<length;i++){
							var flag = false;
							for(var j=0;j<ProgramDocument._listShopId.length;j++){
								if(ProgramDocument._listShopId[j] == data.listShopId[i]){
									flag = true;
									break;
								}
							}
							if(flag!=true){
								ProgramDocument._listShopId.push(data.listShopId[i]);
							}
						}
					}
					ProgramDocument.loopCheck(arrayShopId);
				} 
			}
    	});
	},
	recursionFindChildShopUnCheck:function(id){
		if(id == null || id == undefined){
			return;
		}
		$.ajax({
    		type : "POST",
			url : '/document/getChildShopId',
			data: ({shopId:id}),
			dataType: "json",
			success : function(data) {
				if(data != null && data != undefined && data.listShopId != null && data.listShopId != undefined){
					var flag = true;
					var length = data.length;
					var arrayShopId = data.listShopId;
					for(var i=0;i<length;i++){
						ProgramDocument.removeArrayByValue(ProgramDocument._listShopId,data.listShopId[i]);
					}
					ProgramDocument.loopUnCheck(arrayShopId);
				}
			}
    	});
	},
	removeArrayByValue:function(arr,val){
		for(var i=0; i<arr.length; i++) {
	        if(arr[i] == val){
	        	arr.splice(i, 1);
		        break;
	        }
	    }
	},
	updateDocumentShopMap:function(){
		var params = new Object();
		params.listShopId = ProgramDocument._listShopId;
//		params.ProgramDocument._listShopIdCheck;
		params.idOfficeDoc = $('#idDocumentIDGrid').val().trim();
		if(ProgramDocument._listShopId.length<=0){
			$('.easyui-dialog #errMsgDialog').html('Bạn chưa chọn đơn vị để thêm').show();
			return false;
		}
		try{
			Utils.addOrSaveData(params, '/document/updateAndCreOffDocShopMap', null, 'errMsgDialog', function(data){
				$('#divOverlay').show();
				if(data.error != undefined && data.errMsg != true){
					$('#divOverlay').hide();
					$('#errExcelMsg').html("").hide();
					$('#resultInsertShop').html('Lưu dữ liệu thành công.').show();
					var tm = setTimeout(function(){
						$('#resultInsertShop').html('').hide();
						clearTimeout(tm); 
						$('#searchStyleShopDisplay1').dialog('close');
					}, 2500);
				} else {
					$('#divOverlay').hide();
					$('#resultInsertShop').html('Chưa Lưu dữ liệu được').show();
				}
			}, null, null, null, "Bạn có muốn thêm đơn vị này ?", null);
		}
		catch(e) {
			$('#divOverlay').hide();
			$('#errMsgDialog').html('Cập nhật dữ liệu thất bại').show();
			setTimeout(function(){$('#errMsgDialog').hide();},3500);
		}
	},
	searchShop:function(){
		var code = $('#shopCode').val().trim();
		var name = $('#shopName').val().trim();
//		var id = $('#selId').val();
		var url = ProgrammeDisplayCatalog.getShopGridUrl(id,code,name);
		$("#exGrid").treegrid({url:url});
		$('#errExcelMsg').html("").hide();
		ProgrammeDisplayCatalog._listShopIdEx = new Array();
		ProgrammeDisplayCatalog._listShopId = new Array();
	},
	
	getSearchStyleDocShopMapGridUrl : function(url, code, name) {		
		var searchUrl = '';
		searchUrl = url;
		searchUrl+= "?promotionProgramCode=" + Utils.XSSEncode(code) + "&promotionProgramName=" + Utils.XSSEncode(name);
		var idOfficeDoc = $('#idDocumentIDGrid').val().trim();
		
		searchUrl += "&idOfficeDoc=" + idOfficeDoc;
		return searchUrl;
	},
	loopCheck:function(arrayShopId){
		if(arrayShopId == null || arrayShopId == undefined || arrayShopId.length == 0){
			return;
		}
		$('.easyui-dialog #check_' +arrayShopId[0]).attr('checked','checked');
		ProgramDocument.removeArrayByValue(arrayShopId,arrayShopId[0]);
		ProgramDocument.loopCheck(arrayShopId);
	},
	loopUnCheck:function(arrayShopId){
		if(arrayShopId == null || arrayShopId == undefined || arrayShopId.length == 0){
			return;
		}
		$('.easyui-dialog #check_' +arrayShopId[0]).removeAttr('checked');
		ProgramDocument.removeArrayByValue(arrayShopId,arrayShopId[0]);
		ProgramDocument.loopUnCheck(arrayShopId);
	}
};
/*
 * END OF FILE - /web/web/resources/scripts/business/catalog/vnm.program-document.js
 */

/*
 * START OF FILE - /web/web/resources/scripts/business/catalog/vnm.report-manager.js
 */
var ReportManager = {
	_idUrlGoc: null,
	_rowIndexGoc: null,
	_idDefaultGoc: null,
	_flagEditName: true,
	_shopType: null,
	_isNPP: null,
	refreshGridEdit: function () {
		$('#divReportDetail').css('visibility', 'hidden');
		ReportManager._flagEditName = true;
	},
	changeRadio: function (id) {
		//if($('#isdefault').is(':checked')){
		if (id == undefined || id == null || id == '' || id ==""){
			$('#checkDefault').val(0);
			$('#reportIdDefault').val(ReportManager._idDefaultGoc);
		} else {
			if(ReportManager._idDefaultGoc == id) { // bang -1: thi khong can upDate lai isDefault
				$('#checkDefault').val(-1);
			} else {
				$('#checkDefault').val(id);
				$('#reportIdDefault').val(ReportManager._idDefaultGoc);
			}
		}
	},
	search:function(){
		
		var shopId = '';
		if($('#shopId').val() != undefined && $('#shopId').val() != null && $('#shopId').val() != ""){
			shopId = $('#shop').combobox('getValue').trim();
			var t = $('#shop').combotree('tree');
			var n = t.tree('getSelected');
			$('#divShopName').html(n.text);
		} else {
			shopId = $('#curShopId').val();
		}
		var reportCode =$('#reportCode').val().trim();
		var reportName = $('#reportName').val().trim();
		ReportManager._isNPP = $('#isNPP').val();
		$('#dg').datagrid('load',{
			shopId: shopId,
			reportCode: reportCode,
			reportName: reportName
		});
		ReportManager.refreshGridEdit();
		return false;
	},
	uploadFileReport:function(){	
		$('#easyuiPopupUpload').css("visibility", "visible");
		var html = $('#easyuiPopupUpload').html();
		ReportManager.refreshPopupUpload();
		$('#easyuiPopupUpload').dialog({  
	        closed: false,  
	        cache: false,  
	        modal: true,
	        width:550,
	        height:300,
	        onOpen: function(){
	        	$('#reportCodeDlg').focus();
	        	$("#easyuiPopupUpload #checkDefault").attr("checked", "checked");
	        },onClose:function(){
	        	$('#easyuiPopupUpload').html(html);
	        	$('#easyuiPopupUpload').css("visibility", "hidden");
	        	$('#reportCodeDlg').val('').show();
	        	$('#reportNameDlg').val('').show();
	        	$('#fakefilepcVNM').val('').show();
	        }
		});
	},
	refreshPopupUpload : function(){
		$('#reportCodeDlg').val('').show();
		$('#reportNameDlg').val('').show();
		$('#fakefilepcVNM').val('').show();
		$('#errPdfMsg').hide();
		$('#resultPdfMsg').hide();
//		$('#fakePdffilepc').attr('disabled','disabled');
		$('#reportNameDlg').focus();
	},
	//upload file bao cao vnm.report-manager.js, load ca file .jasper, .jrxml: function uploadJasperFile(fileObj,formId,inputText){
	importReportFile: function(){
		var msg = '';
		var err = '';
		//Check is Null
		if(msg.length == 0){
			msg = Utils.getMessageOfRequireCheck('reportCodeDlg','Mã BC');
			err = '#reportCodeDlg';
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfRequireCheck('reportNameDlg','Tên BC');
			err = '#reportNameDlg';
		}
		var str = $('#fakefilepcVNM').val().trim();
		if(msg.length == 0){
			if(str.length > 0) {
				var n = str.substring(str.length-5,str.length).toUpperCase();
				if(n!=='JRXML'){
					msg = 'Không đúng loại định dạng Report file .jrxml';
				}
			}  else {
				msg = 'Không đúng loại định dạng Report file .jrxml';
			}
		}
		//Check validate
		if(msg.length == 0){
			msg = Utils.getMessageOfSpecialCharactersValidateEx1('#reportCodeDlg','Mã BC',Utils._CODE);
			err = '#reportCodeDlg';
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfSpecialCharactersValidateEx1('#reportNameDlg','Tên BC',Utils._NAME);
			err = '#reportNameDlg';
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfSpecialCharactersValidateEx1('#fakefilepcVNM','Tập tin Report file .jrxml',Utils._NAME);
			err = '#fakefilepcVNM';
		}
		if(msg.length>0){
			$('#errPdfMsg').html(msg).show();
			$(err).focus();
			setTimeout(function(){$('#errPdfMsg').hide();},4000);
			return false;
		}
		if(str != null && str != "") {
			if(!uploadJasperFile(document.getElementById("reportFile"), null, 'fakefilepcVNM')){
				return false;
			}
		} else {
			msg = 'Không đúng loại định dạng Report file .jrxml';
		}
		var options = {
				beforeSubmit : ReportManager.beforeUploadFile,
				success : ReportManager.afterUploadFile,
				type : "POST",
				dataType : 'html'
			};
			$('#easyuiPopupUpload #importFrm').ajaxForm(options);
			$.messager.confirm('Xác nhận', 'Bạn có muốn lưu thông tin này ?',	function(r) {
				if (r) {
					try{
						$('#importFrm').submit();}
					catch(e) {
					}
				}
			});
 		return false;
	},
	importReportFileDetail: function(){
		var msg = '';
		var err = '';
		var str = $('#fakefilepc').val().trim();
		if(msg.length == 0){
			if(str.length > 0) {
				var n = str.substring(str.length-5,str.length).toUpperCase();
				if(n!=='JRXML'){
					msg = 'Không đúng loại định dạng Report file .jrxml';
				}
			} else {
				if($('#checkDefault').val() == -1) {
					msg = 'Không có gì thay đổi' ;
				} else if($('#checkDefault').val() == 0) {
					msg = 'Vui lòng chọn tập tin file .jrxml làm mặc định' ;
				}
			}
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfSpecialCharactersValidateEx1('#fakefilepc','Tập tin Report file .jrxml',Utils._NAME);
			err = '#fakefilepc';
		}
		if(msg.length>0){
			$('#errMsg').html(msg).show();
			$(err).focus();
			setTimeout(function(){$('#errMsg').hide();},4000);
			return false;
		}
		if($('#shopId').val() != undefined && $('#shopId').val() != null && $('#shopId').val() != ""){
			var shopId = $('#shop').combobox('getValue').trim();
			$('#shopIdReport').val(shopId);
			/*var t = $('#shop').combotree('tree');
			var n = t.tree('getSelected');
			$('#divShopName').html(n.text);*/
		} else {
			var shopId = $('#curShopId').val();
			$('#shopIdReport').val(shopId);
		}
		if(str != null && str != "") {
			if(!uploadJasperFile(document.getElementById("reportFileDetail"), null, 'fakefilepc')){
				return false;
			}
		}
		var options = {
				beforeSubmit : ReportManager.beforeUploadFile,
				success : ReportManager.afterUploadFileDetail,
				type : "POST",
				dataType : 'html'
			};
			$('#importFrmReport').ajaxForm(options);
			$.messager.confirm('Xác nhận', 'Bạn có muốn lưu thông tin này ?',	function(r) {
				if (r) {
					try{
						$('#importFrmReport').submit();}
					catch(e) {
					}
				}
			});
 		return false;
	},
	beforeUploadFile:function(){
//		if(!uploadPdfFile(document.getElementById("pdfFile"))){
//			return false;
//		}
		try{
			
			$('#errPdfMsg').html('').hide();
			$('#resultPdfMsg').html('').hide();	
			$('#errMsg').html('').hide();
			$('#successMsg').html('').hide();
			$('#divOverlay').show();
		}catch(e){}
	},
	afterUploadFile: function(responseText, statusText, xhr, $form){
		$('#divOverlay').hide();
		if (statusText == 'success') {
			$("#responseDiv").html(responseText);
			updateTokenImport();
			var errMsg = $('#sverrMsg').val().trim();
			if(errMsg!=undefined && errMsg.length>0){
				$('#errPdfMsg').html(errMsg).show();
				setTimeout(function(){$('#errPdfMsg').hide();},4000);
				return false;
			}
			else{
				$('#resultPdfMsg').html('Lưu dữ liệu thành công').show();
				$('#dg').datagrid('reload');
				setTimeout(function(){
					$('#resultPdfMsg').html('').hide();
					ReportManager.refreshGridEdit();
					$('#easyuiPopupUpload').dialog('close');
				},4000);				
			}
			
		}
	},
	afterUploadFileDetail: function(responseText, statusText, xhr, $form){
		$('#divOverlay').hide();
		if (statusText == 'success') {
			$("#responseDiv").html(responseText);
			updateTokenImport();
			var errMsg = $('#sverrMsg').val().trim();
			var typeReportMsg = $('#typeReportMsg').val().trim();
			if(errMsg!=undefined && errMsg.length>0){
				$('#errMsg').html(errMsg).show();
				setTimeout(function(){$('#errMsg').hide();},4000);
				return false;
			}
			else{
				$('#successMsg').html('Lưu dữ liệu thành công').show();
				//$('#dgReportDetail').datagrid('reload');
				if(typeReportMsg == 0) {
					ReportManager.editReportShopMapVNM(ReportManager._idUrlGoc, ReportManager._rowIndexGoc);
				} else {
					ReportManager.editReportShopMap(ReportManager._idUrlGoc, ReportManager._rowIndexGoc);
				}
				setTimeout(function(){
					$('#successMsg').html('').hide();
				},4000);				
			}
			
		}
	},
	closeWindow:function(){
		$('#easyuiPopupUpload').dialog('close');
	},
	editReportShopMapVNM:function(id, rowIndex){
 		$('#divReportDetail').css('visibility', 'visible');
 		//$('#divReportDetail').css('width', '1245');
 		var code = $('#dg').datagrid('getRows')[rowIndex].reportCode;
 		var name = $('#dg').datagrid('getRows')[rowIndex].reportName;
 		$('#divReportDetail .Title2Style').html('Danh sách file Gốc báo cáo (' + '<span class="RequireStyle" style="text-transform:none">' + Utils.XSSEncode(code) + ' - ' + Utils.XSSEncode(name) + '</span>)');
 		var shopId = '';
		if($('#shopId').val() != undefined && $('#shopId').val() != null && $('#shopId').val() != ""){
			shopId = $('#shop').combobox('getValue').trim();
		} else {
			shopId = $('#curShopId').val();
		}
		var typeFile = 0; // VNM
		$('#typeReport').val(typeFile);
 		loadReportDetailVNM(id, shopId, typeFile);
 		ReportManager._rowIndexGoc = rowIndex;
 		ReportManager._idUrlGoc = id;
 		$('#reportId').val(id);  // id cua Report can chinh sua
 		$('#checkDefault').val(-1); // default chinh no khong can update
	},
	editReportShopMapNPP:function(id, rowIndex){
 		$('#divReportDetail').css('visibility', 'visible');
 		var code = $('#dg').datagrid('getRows')[rowIndex].reportCode;
 		var name = $('#dg').datagrid('getRows')[rowIndex].reportName;
 		//$('#divReportDetail .RequireStyle').html(code + ' - ' +name);
 		$('#divReportDetail .Title2Style').html('Danh sách file Gốc báo cáo (' + '<span class="RequireStyle" style="text-transform:none">' + Utils.XSSEncode(code) + ' - ' + Utils.XSSEncode(name) + '</span>)');
 		var shopId = '';
		if($('#shopId').val() != undefined && $('#shopId').val() != null && $('#shopId').val() != ""){
			shopId = $('#shop').combobox('getValue').trim();
		} else {
			shopId = $('#curShopId').val();
		}
 		var typeFile = 0; // VNM
		$('#typeReport').val(typeFile);
 		loadReportDetailNPP(id, shopId, typeFile);
	},
	editReportShopMap:function(id, rowIndex){
 		$('#divReportDetail').css('visibility', 'visible');
 		var code = $('#dg').datagrid('getRows')[rowIndex].reportCode;
 		var name = $('#dg').datagrid('getRows')[rowIndex].reportName;
 		$('#divReportDetail .Title2Style').html('Danh sách file NPP báo cáo (' + '<span class="RequireStyle" style="text-transform:none">' + Utils.XSSEncode(code) + ' - ' + Utils.XSSEncode(name) + '</span>)');
 		var shopId = '';
		if($('#shopId').val() != undefined && $('#shopId').val() != null && $('#shopId').val() != ""){
			shopId = $('#shop').combobox('getValue').trim();
		} else {
			shopId = $('#curShopId').val();
		}
		var typeFile = 1; // NPP
		$('#typeReport').val(typeFile);
 		loadReportDetail(id, shopId, typeFile);
 		ReportManager._rowIndexGoc = rowIndex;
 		ReportManager._idUrlGoc = id;
 		$('#reportId').val(id);  // id cua Report can chinh sua
 		$('#checkDefault').val(-1); // default chinh no khong can update
	},
	editReportShopMapName: function(id, rowIndex) {
		//edit name
		var row = $('#dg').datagrid('getRows')[rowIndex];
		if(row.editName){
			$.messager.confirm('Xác nhận', 'Bạn có muốn lưu tên báo cáo không?', function(r){
				if(r){
					ReportManager._flagEditName = true;
					$('#dg').datagrid('acceptChanges');
					row.editName = false;
					$('#dg').datagrid('rejectChanges');
					$('#dg').datagrid('refreshRow',rowIndex);
					
					$.ajax({
						 type: "POST",
						 url: "/catalog/report-manager/updateNameReport",
						 data: { reportId: id, reportName: row.reportName},
						 dataType: "json",
						 success: function(data){
							 if(data.error == false){
								 $('#successMsgEdit').html('Lưu thành công!').show();
								 setTimeout(function(){ $('#successMsgEdit').html('').hide();},5000);
							 }else{
								 $('#errMsgEdit').html(data.errMsg).show();
								 setTimeout(function(){ $('#errMsgEdit').html('').hide();},5000);
							 }
						 }
					});
				}
			});
			
		}else{
			if(ReportManager._flagEditName == true) {
				row.editName = true;
				$('#dg').datagrid('refreshRow',rowIndex);
				$('#dg').datagrid('beginEdit', rowIndex);
				ReportManager._flagEditName = false;
			}
		}
	},
	backEditReportShopMapName: function(id, rowIndex){
		ReportManager._flagEditName = true;
		$('#dg').datagrid('getRows')[rowIndex].editName = false;
		$('#dg').datagrid('rejectChanges');
		$('#dg').datagrid('refreshRow',rowIndex);
	},
	deleteReport: function(id) {
		//save name
		$.messager.confirm('Xác nhận', 'Nếu xóa báo cáo thì sẽ xóa toàn bộ các mẫu báo cáo của tất cả đơn vị. <br> Bạn có muốn xóa không?', function(r){
			if(r){
			}
		});
	},
	deleteReportUrl: function(id, index) {
		if(id != undefined && id != null && id != ""){
			//save name
			$.messager.confirm('Xác nhận', 'Bạn có muốn xóa file báo cáo!', function(r){
			});
		} else {
			$.messager.alert('Thông báo', 'File chưa được tải lên. <br> Bạn không thể xóa!', 'info');
		}
		
	}
	///////////////********** END VUONGMQ *********/////////////////////
};
/*
 * END OF FILE - /web/web/resources/scripts/business/catalog/vnm.report-manager.js
 */

/*
 * START OF FILE - /web/web/resources/scripts/business/catalog/vnm.catalog-manager.js
 */
/*
 * Copyright 2015 Viettel ICT. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/**
 * QL danh muc thuoc tinh, huong vi ..
 * @author trietptm
 * @since Nov 17 2014
 * **/
var CatalogType = {
	UNIT : 'UOM',
	CAT : '1',
    SUB_CAT : '2',
    BRAND : '3',
    FLAVOUR : '4',
    PACKING : '5',
    LIST_OF_PROBLEMS : 'FEEDBACK_TYPE',
    EXPIRY_DATE : 'NGAY_THANG_NAM',
    CUSTOMER_SALE_POSITION : 'CUSTOMER_SALE_POSITION',
    STAFF_SALE_TYPE : 'STAFF_SALE_TYPE',
    FOCUS_PRODUCT_TYPE : 'FOCUS_PRODUCT_TYPE',
    STAFF_TYPE_SHOW_SUP : 'STAFF_TYPE_SHOW_SUP',
    SYS_REASON_UPDATE_INVOICE: 'SYS_REASON_UPDATE_INVOICE',
    ORDER_PIRITY: 'ORDER_PIRITY',
    catType : null
//    ,
//    parseValue: function(value) {
//    	if (value.trim() == 0) {
//    		return CatalogType.UNIT;
//    	} else if(value.trim() == 1) {
//    		return CatalogType.CAT;
//    	} else if(value.trim() == 2) {
//    		return CatalogType.SUB_CAT;
//    	} else if(value.trim() == 3) {
//    		return CatalogType.BRAND;
//    	} else if(value.trim() == 4) {
//    		return CatalogType.FLAVOUR;
//    	} else if(value.trim() == 5) {
//    		return CatalogType.PACKING;
//    	} else if(value.trim() == 10) {
//    		return CatalogType.LIST_OF_PROBLEMS;
//    	} else if(value.trim() == 11) {
//    		return CatalogType.EXPIRY_DATE;
//    	} else if(value.trim() == 12) {
//    		return CatalogType.CUSTOMER_SALE_POSITION;
//    	} else if(value.trim() == 13) {
//    		return CatalogType.STAFF_SALE_TYPE;
//    	} else if(value.trim() == 14) {
//    		return CatalogType.FOCUS_PRODUCT_TYPE;
//    	}
//    	return -2;
//    }
};

/**
 * Quan ly danh muc
 * @author trietptm
 * @since Nov 17, 2015
 */
var CatalogManager = {
	isEdit: false,
	poplbName: null,
	oldCode: null,
	orPopLbName: null,
	
	/**
	 * load grid danh muc
	 * @author trietptm
	 * @param params
	 * @since Nov 17, 2015
	 * 
	 */
	loadCatGrid: function(params) {
		var cptype = $('#catType').val().trim();
		var addIcon = '<a title="' + CatalogManager.gridBtTtAdd + '" href="javascript:void(0)" onclick="CatalogManager.showDialogAddOrEdit()" >' +
    	'<img width="15" height="15" src="/resources/images/icon_add.png"/></a>';
		var gridTt = CatalogManager.gridTtCatCode;
		switch (cptype) {
			case CatalogType.UNIT:
				gridTt = CatalogManager.gridTtUnit;
				break;	
			case CatalogType.SUB_CAT:
				gridTt = CatalogManager.gridTtSubCatCode;
				break;
			case CatalogType.BRAND:
				gridTt = CatalogManager.gridTtBrand;
				break;
			case CatalogType.FLAVOUR:
				gridTt = CatalogManager.gridTtFlavour;
				break;
			case CatalogType.PACKING:
				gridTt = CatalogManager.gridTtPackage;	
				break;
			case CatalogType.LIST_OF_PROBLEMS:
				gridTt = CatalogManager.gridTtProblemsListGtt;
				break;
			case CatalogType.EXPIRY_DATE:
				gridTt = CatalogManager.gridTtExpiryDate;
				break;
			case CatalogType.CUSTOMER_SALE_POSITION:
				gridTt = CatalogManager.gridTtCustomerSalePosition;
				break;
			case CatalogType.STAFF_SALE_TYPE:
				gridTt = CatalogManager.gridTtStaffSaleType;
				break;
			case CatalogType.STAFF_TYPE_SHOW_SUP:
				gridTt = CatalogManager.gridTtPositionSuperviseStaffType;
				break;
			case CatalogType.FOCUS_PRODUCT_TYPE:
				gridTt = CatalogManager.gridTtFocusProductType;
				break;
			case CatalogType.SYS_REASON_UPDATE_INVOICE:
				gridTt = CatalogManager.gridTtReasonUpdateInvoice;
				break;
			case CatalogType.ORDER_PIRITY:
				gridTt = CatalogManager.gridTtOrderPriority;
				break;
		}
		
		if (cptype != CatalogType.SUB_CAT) {
			CatalogManager.poplbName = gridTt;
			CatalogManager.orPopLbName = gridTt;
		} else {
			CatalogManager.poplbName = CatalogManager.gridTtSubCatCode;
			CatalogManager.orPopLbName = CatalogManager.gridTtSubCatCode;
		}
					
		$('#dg').datagrid({
			url: '/config/loadShopCat',
			rownumbers: true,
			queryParams: params,
			singleSelect: true,
			fitColumns: true,
			pageSize: 20,
			width: $('#datasourceDiv').width(),
			pageList: [ 20, 30, 40 ],
			height: 'auto',
			scrollbarSize: 0,
			pagination: true,
			pageNumber: 1,
			nowrap: false,
			autoRowHeight: true,
			columns: [[
				{field: 'catCode', title : CatalogManager.gridTtCode, width : 100, align : 'left', sortable : false, resizable : false,
					formatter : function(value, row, index) {
						return Utils.XSSEncode(row.code);
					}
				},
				{field: 'catName', title : CatalogManager.poplbName, width : 150, align : 'left', sortable : false, resizable : false,
					formatter : function(value, row, index) {
						return Utils.XSSEncode(row.name);
					}
				},
				{field: 'value', title : CatalogManager.gridTtStaffType, hidden: true, width : 150, align : 'left', sortable : false, resizable : false,
					formatter : function(value, row, index) {
						return Utils.XSSEncode(row.value);
					}
				},
				{field: 'parentCatName', title : CatalogManager.gridTtParentCat, hidden: true, width : 150, align : 'left', sortable : false, resizable : false,
					formatter : function(value, row, index) {
						return Utils.XSSEncode(row.parentName);
					}
				},
				{field: 'catDescript', title : CatalogManager.gridTtDescript, width : 200, align : 'left', sortable : false, resizable : false,
					formatter : function(value, row, index) {
						return Utils.XSSEncode(row.description);
					}
				},
			    {field:'edit', title: addIcon, width: 50, align: 'center', sortable: false, resizable: false, 
					formatter: function(val, row, index) {
			    		var strFunc = '<a href="javascript:void(0)" title="'+ Utils.XSSEncode(CatalogManager.gridBtTtChange) +'" onclick="CatalogManager.showDialogAddOrEdit('+ index + ');">'+
		    			'<img src="/resources/images/icon-edit.png"/></a>';
			    		if (cptype != CatalogType.UNIT && cptype != CatalogType.EXPIRY_DATE && cptype != CatalogType.LIST_OF_PROBLEMS
			    				&& cptype != CatalogType.CUSTOMER_SALE_POSITION && cptype != CatalogType.STAFF_SALE_TYPE && cptype != CatalogType.FOCUS_PRODUCT_TYPE
			    				&& cptype != CatalogType.SYS_REASON_UPDATE_INVOICE && cptype != CatalogType.ORDER_PIRITY) {
	    					strFunc += '<a href="javascript:void(0)" title="' + Utils.XSSEncode(msgText4) + '"style="margin-left:2px" onclick="CatalogManager.deleteRow('+ index + ');">'+
	    						'<img src="/resources/images/icon_delete.png"/></a>';
			    		}
			    		return strFunc;
			    	}
			    }
			]],
			onLoadSuccess: function(data) {
				if (!data.error) {
					$('#errExcelMsg').html('').hide();
					
					$('#dg').datagrid("hideColumn","value");
					$('#dg').datagrid("hideColumn","parentCatName");
					var catType = $('#catType').val().trim();
					if (catType == CatalogType.SUB_CAT) {
						$('#dg').datagrid("showColumn","parentCatName");
					} else if (catType == CatalogType.STAFF_TYPE_SHOW_SUP) {
						$('#dg').datagrid("showColumn","value");
					}
					
					$('#shopCatEasyUIDialog #popCatType').val(catType);
					//fill popup combo cat
					var lstCat = data.lstCat;
					if (lstCat != undefined && lstCat != null && lstCat.length > 0) {
						$('#shopCatEasyUIDialog #popupParentCat').empty().change();
						for (var i = 0; i < lstCat.length; i++) {
							var obj = new Object();
							obj = lstCat[i];
							$('#shopCatEasyUIDialog #popupParentCat').append($('<option>', {
							    value: obj.id,
							    select: obj.id,
							    id: 'select-'+ obj.id,
							    text: obj.productInfoName
							}));
						}
						$('#shopCatEasyUIDialog #popupParentCat').change();
					}
					//fill popup combo cat
					var lstStaffType = data.lstStaffType;
					if (lstStaffType != undefined && lstStaffType != null && lstStaffType.length > 0) {
						$('#shopCatEasyUIDialog #cbxStaffType').empty().change();
						for (var i = 0; i < lstStaffType.length; i++) {
							var obj = new Object();
							obj = lstStaffType[i];
							$('#shopCatEasyUIDialog #cbxStaffType').append($('<option>', {
							    value: obj.id,
							    select: obj.id,
							    id: 'select-'+ obj.id,
							    text: obj.name
							}));
						}
						$('#shopCatEasyUIDialog #cbxStaffType').change();
					}
					setTimeout(function() {
						$('#pdCodeName').focus();
					}, 500);
					
					// phan quyen control
					var arrEdit =  $('#datasourceDiv td[field="edit"]');
					if (arrEdit != undefined && arrEdit != null && arrEdit.length > 0) {
					  for (var i = 0, size = arrEdit.length; i < size; i++) {
					  	$(arrEdit[i]).prop("id", "column_edit" + i);//Khai bao id danh cho phan quyen
						$(arrEdit[i]).addClass("cmsiscontrol");
					  }
					}
					Utils.functionAccessFillControl('datasourceDiv', function(data) {
						arrTmpLength =  $('#datasourceDiv td[id^="column_edit"]').length;
						invisibleLenght = $('.isCMSInvisible[id^="column_edit"]').length;
						if (arrTmpLength > 0 && arrTmpLength != invisibleLenght) {
							$('#dg').datagrid("showColumn", "edit");
						} else {
							$('#dg').datagrid("hideColumn", "edit");
						}
					});
				} else {
					$('#errExcelMsg').html(data.errMsg).show();
				}
			}
		});
	},
	
	/**
	 * show Dialog Add Edit
	 * @author trietptm
	 * @since Nov 17, 2015
	 */
	showDialogAddOrEdit: function(index) {
		$('#errExcelMsg').html('').hide();
		$('#successMsg').html('').hide();
		$('#errMsgPopup').html('').hide();
		$('#shopCatEasyUIDialog #errMsg').html('').hide();
		
		CatalogManager.changeLbText();
		var catType = $('#catType').val().trim();
		var data = null;
		
		setTimeout(function() {
			if ($('#shopCatEasyUIDialog #catCode').prop('disabled') == false) {
				$('#shopCatEasyUIDialog #catCode').focus();
			} else {
				$('#shopCatEasyUIDialog #catName').focus();
			}
		}, 500);
		
		if (index == undefined) {
			$('#shopCatEasyUIDialog #catCode').val('');
			$('#shopCatEasyUIDialog #catName').val('');
			$('#shopCatEasyUIDialog #catDescript').val('');

			$('#shopCatEasyUIDialog #catCode').removeAttr('disabled');
			$('#shopCatEasyUIDialog #catName').removeAttr('disabled');
	
			CatalogManager.isEdit = false;
			CatalogManager.oldCode = null;
			$('#hideCatCode').val('');
		} else {
			data = $('#dg').datagrid('getRows')[index];
			CatalogManager.oldCode = data.code;
			$('#hideCatCode').val(Utils.XSSEncode(data.code));
			$('#shopCatEasyUIDialog #catCode').val(Utils.XSSEncode(data.code));
			$('#shopCatEasyUIDialog #catName').val(Utils.XSSEncode(data.name));
			$('#shopCatEasyUIDialog #catDescript').val(Utils.XSSEncode(data.description));

			if(catType == CatalogType.UNIT || catType == CatalogType.EXPIRY_DATE || catType == CatalogType.LIST_OF_PROBLEMS
				|| catType == CatalogType.CUSTOMER_SALE_POSITION || catType == CatalogType.STAFF_SALE_TYPE || catType == CatalogType.FOCUS_PRODUCT_TYPE
				|| catType == CatalogType.STAFF_TYPE_SHOW_SUP || catType == CatalogType.SYS_REASON_UPDATE_INVOICE || catType == CatalogType.ORDER_PIRITY) {
				$('#shopCatEasyUIDialog #catCode').attr('disabled','disalbled');
			} else {
				$('#shopCatEasyUIDialog #catCode').removeAttr('disabled');
				$('#shopCatEasyUIDialog #catName').removeAttr('disabled');
			}
			
			CatalogManager.isEdit = true;
			if (data.parentId != undefined && data.parentId != null) {
				$('#shopCatEasyUIDialog #popupParentCat').val(data.parentId).change();
			}
			if (data.staffTypeId != undefined && data.staffTypeId != null) {
				$('#shopCatEasyUIDialog #cbxStaffType').val(data.staffTypeId).change();
			}
		}
		$('#shopCatEasyUIDialog #lblPopCatName').html(CatalogManager.poplbName);
		
		$('#shopCatEasyUIDialog #coverCbParentCat').hide();
		$('#shopCatEasyUIDialog #staffTypeContainer').hide();
		if (catType == CatalogType.SUB_CAT) {
			$('#shopCatEasyUIDialog #coverCbParentCat').show();
		} else if (catType == CatalogType.STAFF_TYPE_SHOW_SUP) {
			$('#shopCatEasyUIDialog #staffTypeContainer').show();
		}
		
		$('#shopCatEasyUIDialog #catCode').focus();
		$('#shopCatEasyUIDialog').dialog('open');
		setTimeout(function() {
			CommonSearchEasyUI.fitEasyDialog('shopCatEasyUIDialog');
		}, 1000);
	},
	
	/**
	 * xu ly luu them moi / thay doi
	 * @author trietptm
	 * @since Nov 17, 2015
	 */
	saveCatalogManager: function() {
		CatalogManager.changeLbText(CatalogType.UNIT);
		$('#errMsgPopup').hide();
		var data = new Object();
		var msg = '';
		var catCode = $('#shopCatEasyUIDialog #catCode').val().trim();
		var cbType = $('#catType').val().trim();

		if ((cbType == CatalogType.LIST_OF_PROBLEMS
				|| cbType == CatalogType.UNIT || cbType == CatalogType.CUSTOMER_SALE_POSITION 
				|| cbType == CatalogType.STAFF_SALE_TYPE || cbType == CatalogType.FOCUS_PRODUCT_TYPE
				) && CatalogManager.isEdit == true) {
			
			//anhhpt:neu ma dvt luc dau khac voi ma dvt moi --> khong cho luu
			if(!isNullOrEmpty(catCode) && !isNullOrEmpty($('#hideCatCode').val()) && catCode.toUpperCase()!= $('#hideCatCode').val().trim().toUpperCase()){
				$('#errMsgPopup').html(msgKhongTheLuu).show();
				return false;
			}
			catCode = $('#hideCatCode').val();
		}
		
		var catName = $('#shopCatEasyUIDialog #catName').val().trim();
		var catDescript = $('#shopCatEasyUIDialog #catDescript').val().trim();
		var type = $('#shopCatEasyUIDialog #popCatType').val().trim();
		
		if (isNullOrEmpty(msg)) {
			msg = Utils.getMessageOfRequireCheck('shopCatEasyUIDialog #catCode', CatalogManager.gridTtCode, false, 20);
		}
		if (isNullOrEmpty(msg)) {
			msg = Utils.getMessageOfSpecialCharactersValidate('shopCatEasyUIDialog #catCode', CatalogManager.gridTtCode, Utils._CODE);
		}
		if (isNullOrEmpty(msg)) {
			msg = Utils.getMessageOfRequireCheck('shopCatEasyUIDialog #catName', CatalogManager.poplbName, false, 500);
		}
		if (isNullOrEmpty(msg)) {
			msg = Utils.getMessageOfSpecialCharactersValidate('shopCatEasyUIDialog #catName', CatalogManager.poplbName);
		}
//		if (isNullOrEmpty(msg)) {
//			//neu la: Nganh hang, nganh hang con, nhan hieu, huong vi, dong goi, loai KH thi validate them dau ","
//			if (cbType == CatalogType.CAT || cbType == CatalogType.SUB_CAT || cbType == CatalogType.BRAND
//					|| cbType == CatalogType.FLAVOUR || cbType == CatalogType.PACKING 
//					|| cbType == CatalogType.CUSTOMER_TYPE || cbType == CatalogType.UNIT) {
//				if (/[<>/\,]/g.test(catName) == true) {
//					msg = CatalogManager.orPopLbName + " " + msgTenDanhMucKTDB;
//				}
//			} else {//Cac danh muc con lai validate 4 ky tu dac biet cho ATTT
//				if (/[<>/\\]/g.test(catName) == true) {
//					msg = CatalogManager.orPopLbName + " " + msgTenDanhMucKTDB;
//				}				
//			}
//		}
		if (isNullOrEmpty(msg)) {
			msg = Utils.getMessageOfSpecialCharactersValidate('shopCatEasyUIDialog #catDescript', CatalogManager.gridTtDescript, Utils._SPECIAL);
		}
		
		if (!isNullOrEmpty(msg)) {
			$('#shopCatEasyUIDialog #errMsg').html(msg).show();
			return false;
		}
		
		data.oldCatCode = CatalogManager.oldCode;
		data.catCode = catCode;
		data.catName = catName;
		data.catDescript = catDescript;
		data.type = type;
		data.isEdit = CatalogManager.isEdit;
		if (cbType == CatalogType.SUB_CAT) {
			data.parentCatId = $('#shopCatEasyUIDialog #popupParentCat').val().trim();
		} else if (cbType == CatalogType.STAFF_TYPE_SHOW_SUP) {
			data.staffTypeId = $('#shopCatEasyUIDialog #cbxStaffType').val().trim();
		}
		Utils.addOrSaveData(data, '/config/save-shop-cat', null, 'serverErrors',function(data) {
			$('#errExcelMsg').html('').hide();
			$('#shopCatEasyUIDialog').dialog('close');
			$('#dg').datagrid('reload');
		}, null, null, null, null, function(data) {
			if (!isNullOrEmpty(data.errMsg)) {
				$('#shopCatEasyUIDialog #errMsg').html(data.errMsg).show();
				return false;
			}
		});
	},
	
	/**
	 * tim kiem
	 * @author trietptm
	 * @since Nov 17, 2015
	 */
	searchCat: function() {
		var params = new Object();
		params.type = $('#catType').val().trim();
		params.pdCodeName = $('#pdCodeName').val().trim();
//		params.inportExportType = $('#cbWhType').val();
		CatalogManager.loadCatGrid(params);
	},
	
	/**
	 * xu ly xoa row tren gird
	 * @author trietptm
	 * @since Nov 17, 2015
	 */
	deleteRow: function(index) {
		var data = $('#dg').datagrid('getRows')[index];
		var tmType = $('#catType').val().trim();
		var dataModel = new Object();
		dataModel.catCode = data.code;
		dataModel.type = tmType;
		Utils.addOrSaveData(dataModel, '/config/delete-shop-cat', null, 'serverErrors', function(data) {
			Utils.updateTokenForJSON(data);
			$('#errExcelMsg').html('').hide();
			$('#dg').datagrid('reload');
		}, null, null, true, msgCommon2, function(data) {
			$('#errExcelMsg').html(data.errMsg).show();
		});
		
	},
	
	/**
	 * xu ly thay doi lable ten
	 * @author trietptm
	 * @since Nov 17, 2015
	 */
	changeLbText: function(forgridTT) {
		var cptype = $('#catType').val().trim();
		var gridTt = CatalogManager.gridTtCatCode;
		switch(cptype){
			case CatalogType.UNIT:
				gridTt = CatalogManager.gridTtUnit;
				break;	
			case CatalogType.SUB_CAT:
				gridTt = CatalogManager.gridTtSubCatCode;
				break;
			case CatalogType.BRAND:
				gridTt = CatalogManager.gridTtBrand;
				break;
			case CatalogType.FLAVOUR:
				gridTt = CatalogManager.gridTtFlavour;
				break;
			case CatalogType.PACKING:
				gridTt = CatalogManager.gridTtPackage;	
				break;
			case CatalogType.LIST_OF_PROBLEMS:
				gridTt = CatalogManager.gridTtProblemsListGtt;
				break;
			case CatalogType.EXPIRY_DATE:
				gridTt = CatalogManager.gridTtExpiryDate;
				break;
			case CatalogType.CUSTOMER_SALE_POSITION:
				gridTt = CatalogManager.gridTtCustomerSalePosition;
				break;
			case CatalogType.STAFF_SALE_TYPE:
				gridTt = CatalogManager.gridTtStaffSaleType;
				break;
			case CatalogType.STAFF_TYPE_SHOW_SUP :
				gridTt = CatalogManager.gridTtPositionSuperviseStaffType;
				break;
			case CatalogType.FOCUS_PRODUCT_TYPE:
				gridTt = CatalogManager.gridTtFocusProductType;
				break;
			case CatalogType.SYS_REASON_UPDATE_INVOICE:
				gridTt = CatalogManager.gridTtReasonUpdateInvoice;
				break;
			case CatalogType.ORDER_PIRITY:
				gridTt = CatalogManager.gridTtOrderPriority;
				break;
		}
		
		if (isNullOrEmpty(forgridTT)) {
			if(cptype != CatalogType.SUB_CAT) {
				CatalogManager.poplbName = gridTt + '<span class="ReqiureStyle"> *</span>';
			} else {
				CatalogManager.poplbName = CatalogManager.gridTtSubCatCode+ '<span class="ReqiureStyle"> *</span>';
			}
		} else {
			CatalogManager.poplbName = gridTt; 
		}
		CatalogManager.orPopLbName = gridTt;
		$('.panel-title').first().html(CatalogManager.thongTin + ' ' + gridTt);
	},
	
	/**
	 * load cay danh muc
	 * @author trietptm
	 * @since Nov 17, 2015
	 */
	loadCatalogTree: function(url) {
		loadDataForTree(url);
		$('#tree').bind("loaded.jstree", function(event, data) {
			$('#tree').jstree('select_node', '#' + CatalogType.UNIT);
		});
		$('#tree').bind("select_node.jstree", function (event, data) {	
			$('#pdCodeName').val('');
			var id = data.rslt.obj.attr("id");
			if (id != null && id != undefined && id.length > 0) {
				CatalogManager.showCatalogForm(id);
			}
	    });
		$('#tree').bind("open_node.jstree", function (event, data) {
	    });
	},
	
	/**
	 * load thong tin tung danh muc
	 * @author trietptm
	 * @since Nov 17, 2015
	 */
	showCatalogForm: function(id) {
		$('#catType').val(id);
//		$('#divOverlay').show();
		var params = new Object();
		params.type = id;
		CatalogManager.loadCatGrid(params);
//		if (id == CatalogType.IMPORT_STOCK || id == CatalogType.EXPORT_STOCK) {
//			$('#whTypeCover').show();
//			$('#lbWhType').show();
////			$('#btSearch').css('margin-left','-161px');
//			$('#cbWhType').val(0).change();
//		} else {
			$('#whTypeCover').hide();
			$('#lbWhType').hide();
//		}
	}
};

/*
 * END OF FILE - /web/web/resources/scripts/business/catalog/vnm.catalog-manager.js
 */

/*
 * START OF FILE - /web/web/resources/scripts/business/catalog/vnm.easy-ui.common.js
 */
var EasyUiCommon = {
	editIndex :undefined,
	editIndexDetail :undefined,
	_xhrDel : null,
	_xhrSave : null,
	_mapCheckProduct: new Map(),
	_mapNewDataSubGrid1: new Map(),
	_mapNewDataSubGrid2: new Map(),
	_lstProductData: new Map(),
	_lstProduct : null,
	inputId:null,
	field:null,
	type:null,
	apParamCode:null,
	productCode : '',
	productName : null,
	_lstSize :null,
	pCode: '',
	pName: '',
	pCodeRep: '',
	pQuantity: 0,
	REQUIRE_PRODUCT: 1,
	QUANTITY_PRODUCT: 2,
	AMOUNT_PRODUCT: 3,
	DETAIL_PRODUCT: 4,
	AUTOCOMPLETE_ONE: 1,
	AUTOCOMPLETE_MUL: 2,
	isMulti:null,
	typeChange:0,
	indexDeleted:[],
	_lstProductSelected:[],
	_mapProductOnSecondDialog: new Map(),
	_mapProductCodeStr1: new Map(),
	_mapProductCodeStr2: new Map(),
	_lstProductExpand:[],
	_lstProductOnChange:[],
	hasChange:false,
	isExceptZ:false,
	totalRowDelete:0,
	_totalRowGrid:0,
	codeByGroup:'',
	endEditing : function(gridId,field){
		if (EasyUiCommon.editIndex == undefined){
			return true;
		} 
		if(gridId != null && gridId != undefined){
			if ($('#' +gridId).datagrid('validateRow', EasyUiCommon.editIndex)){  
				$('#' +gridId).datagrid('endEdit', EasyUiCommon.editIndex);  
				EasyUiCommon.editIndex = undefined;  
				return true;  
			}else{
				return false;
			}
		}else{
			return true;
		}
	},
	endEditingDetail : function(gridId,field){
		if (EasyUiCommon.editIndexDetail == undefined){
			return true;
		} 
		if(gridId != null && gridId != undefined){
			if ($('#' +gridId).datagrid('validateRow', EasyUiCommon.editIndexDetail)){ 
				$('#' +gridId).datagrid('endEdit', EasyUiCommon.editIndexDetail);  
				EasyUiCommon.editIndexDetail = undefined;  
				return true;  
			}else{
				return false;
			}
		}else{
			return true;
		}
	},
	appendRowOnGrid:function(gridId){
		var type = $('#apParamCode').val().trim();
		$('#grid').datagrid('selectRow',0);
		var data = $('#grid').datagrid('getRows');
		var isAppend = false;
		if(data.length == 0){
			$('#' +gridId).datagrid('appendRow',{});
			isAppend = true;
		}else{
				if(type == 'ZV01' ){
					if(data[data.length-1].productCode != null && data[data.length-1].productCode.length > 0
						|| data[data.length-1].quantity != null && data[data.length-1].quantity > 0
						|| data[data.length-1].discountPercent != null && data[data.length-1].discountPercent > 0 ){
						$('#' +gridId).datagrid('appendRow',{});
						isAppend = true;
					}
				}else if(type == 'ZV02' ){
					if(data[data.length-1].productCode != null && data[data.length-1].productCode.length > 0
						|| data[data.length-1].quantity != null && data[data.length-1].quantity > 0
						|| data[data.length-1].discountAmount != null && data[data.length-1].discountAmount > 0){
						$('#' +gridId).datagrid('appendRow',{});
						isAppend = true;
					}
				}else if(type == 'ZV04' ){
					if(data[data.length-1].productCode != null && data[data.length-1].productCode.length > 0
						|| data[data.length-1].amount != null && data[data.length-1].amount > 0
						|| data[data.length-1].discountPercent != null && data[data.length-1].discountPercent > 0){
						$('#' +gridId).datagrid('appendRow',{});
						isAppend = true;
					}
				}else if(type == 'ZV05' ){
					if(data[data.length-1].productCode != null && data[data.length-1].productCode.length > 0
						|| data[data.length-1].amount != null && data[data.length-1].amount > 0
						|| data[data.length-1].discountAmount != null && data[data.length-1].discountAmount > 0){
						$('#' +gridId).datagrid('appendRow',{});
						isAppend = true;
					}
				}else if(type == 'ZV03' ){
					if(data[data.length-1].productCode != null && data[data.length-1].productCode.length > 0
						|| data[data.length-1].quantity != null && data[data.length-1].quantity > 0){
						$('#' +gridId).datagrid('appendRow',{});
						isAppend = true;
					}
				}else if(type == 'ZV06' ){
					if(data[data.length-1].productCode != null && data[data.length-1].productCode.length > 0
						|| data[data.length-1].amount != null && data[data.length-1].amount > 0){
						$('#' +gridId).datagrid('appendRow',{});
						isAppend = true;
					}
				}else if(type == 'ZV07' ){
					if(data[data.length-1].quantity != null && data[data.length-1].quantity > 0
						|| data[data.length-1].discountPercent != null && data[data.length-1].discountPercent > 0
						|| EasyUiCommon._mapProductCodeStr1.get(data.length-1) == ''){
						$('#' +gridId).datagrid('appendRow',{});
						isAppend = true;
						data = $('#grid').datagrid('getRows');
						EasyUiCommon._mapProductCodeStr1.put(data.length-1,EasyUiCommon._mapProductCodeStr1.get(0));
						EasyUiCommon._mapNewDataSubGrid1.put(data.length-1,EasyUiCommon._mapNewDataSubGrid1.get(0));
					}
				}else if(type == 'ZV08' ){
					if(data[data.length-1].quantity != null && data[data.length-1].quantity > 0
						|| data[data.length-1].discountAmount != null && data[data.length-1].discountAmount > 0
						|| EasyUiCommon._mapProductCodeStr1.get(data.length-1) == ''){
						$('#' +gridId).datagrid('appendRow',{});
						isAppend = true;
						data = $('#grid').datagrid('getRows');
						EasyUiCommon._mapProductCodeStr1.put(data.length-1,EasyUiCommon._mapProductCodeStr1.get(0));
						EasyUiCommon._mapNewDataSubGrid1.put(data.length-1,EasyUiCommon._mapNewDataSubGrid1.get(0));
					}
				}else if(type == 'ZV10' ){
					if(data[data.length-1].amount != null && data[data.length-1].amount > 0
						|| data[data.length-1].discountPercent != null && data[data.length-1].discountPercent > 0
						|| EasyUiCommon._mapProductCodeStr1.get(data.length-1) == ''){
						$('#' +gridId).datagrid('appendRow',{});
						isAppend = true;
						data = $('#grid').datagrid('getRows');
						EasyUiCommon._mapProductCodeStr1.put(data.length-1,EasyUiCommon._mapProductCodeStr1.get(0));
						EasyUiCommon._mapNewDataSubGrid1.put(data.length-1,EasyUiCommon._mapNewDataSubGrid1.get(0));
					}
				}else if(type == 'ZV11' ){
					if(data[data.length-1].amount != null && data[data.length-1].amount > 0
						|| data[data.length-1].discountAmount != null && data[data.length-1].discountAmount > 0
						|| EasyUiCommon._mapProductCodeStr1.get(data.length-1) == ''){
						$('#' +gridId).datagrid('appendRow',{});
						isAppend = true;
						data = $('#grid').datagrid('getRows');
						EasyUiCommon._mapProductCodeStr1.put(data.length-1,EasyUiCommon._mapProductCodeStr1.get(0));
						EasyUiCommon._mapNewDataSubGrid1.put(data.length-1,EasyUiCommon._mapNewDataSubGrid1.get(0));
					}
				}else if(type == 'ZV09' || type == 'ZV23'){
					if(data[data.length-1].quantity != null && data[data.length-1].quantity > 0
						||EasyUiCommon._mapProductCodeStr1.get(data.length-1) == ''
						||data.length <= EasyUiCommon._mapProductCodeStr2.keyArray.length){
						$('#' +gridId).datagrid('appendRow',{});
						isAppend = true;
						data = $('#grid').datagrid('getRows');
						EasyUiCommon._mapProductCodeStr1.put(data.length-1,EasyUiCommon._mapProductCodeStr1.get(0));
						EasyUiCommon._mapNewDataSubGrid1.put(data.length-1,EasyUiCommon._mapNewDataSubGrid1.get(0));
						EasyUiCommon._mapNewDataSubGrid2.put(data.length-1,new Array());
					}
				}else if(type == 'ZV12' ){
					if(data[data.length-1].amount != null && data[data.length-1].amount > 0
						||EasyUiCommon._mapProductCodeStr1.get(data.length-1) == ''
						||data.length <= EasyUiCommon._mapProductCodeStr2.keyArray.length){
						$('#' +gridId).datagrid('appendRow',{});
						isAppend = true;
						data = $('#grid').datagrid('getRows');
						EasyUiCommon._mapProductCodeStr1.put(data.length-1,EasyUiCommon._mapProductCodeStr1.get(0));
						EasyUiCommon._mapNewDataSubGrid1.put(data.length-1,EasyUiCommon._mapNewDataSubGrid1.get(0));
						EasyUiCommon._mapNewDataSubGrid2.put(data.length-1,new Array());
					}
				}else if(type == 'ZV15' ){
					if(data.length <= EasyUiCommon._mapProductCodeStr1.keyArray.length
						||data.length <= EasyUiCommon._mapProductCodeStr2.keyArray.length){
						$('#' +gridId).datagrid('appendRow',{});
						isAppend = true;
					}
				}else if(type == 'ZV18' ){
					if(data.length <= EasyUiCommon._mapProductCodeStr1.keyArray.length
						||data.length <= EasyUiCommon._mapProductCodeStr2.keyArray.length){
						$('#' +gridId).datagrid('appendRow',{});
						isAppend = true;
					}
				}else if(type == 'ZV13' || type == 'ZV16' ){
					if(data[data.length-1].discountPercent != null && data[data.length-1].discountPercent > 0
						||data.length <= EasyUiCommon._mapProductCodeStr1.keyArray.length){
						$('#' +gridId).datagrid('appendRow',{});
						isAppend = true;
					}
				}else if(type == 'ZV14' || type == 'ZV17' ){
					if(data[data.length-1].discountAmount != null && data[data.length-1].discountAmount > 0
						||data.length <= EasyUiCommon._mapProductCodeStr1.keyArray.length){
						$('#' +gridId).datagrid('appendRow',{});
						isAppend = true;
					}
				}else if(type == 'ZV19' ){
					if(data[data.length-1].amount != null && data[data.length-1].amount > 0
						|| data[data.length-1].discountPercent != null && data[data.length-1].discountPercent > 0){
						$('#' +gridId).datagrid('appendRow',{});
						isAppend = true;
					}
				}else if(type == 'ZV20' ){
					if(data[data.length-1].amount != null && data[data.length-1].amount > 0
						|| data[data.length-1].discountAmount != null && data[data.length-1].discountAmount > 0){
						$('#' +gridId).datagrid('appendRow',{});
						isAppend = true;
					}
				}else if(type == 'ZV21' ){
					if(data[data.length-1].amount != null && data[data.length-1].amount > 0
						||data.length <= EasyUiCommon._mapProductCodeStr1.keyArray.length){
						$('#' +gridId).datagrid('appendRow',{});
						isAppend = true;
					}
				}else if(type == 'ZV22' ){
					if(data[data.length-1].quantity != null && data[data.length-1].quantity > 0
						||EasyUiCommon._mapProductCodeStr1.get(data.length-1) == ''
						||data.length <= EasyUiCommon._mapProductCodeStr2.keyArray.length){
						$('#' +gridId).datagrid('appendRow',{});
						isAppend = true;
						data = $('#grid').datagrid('getRows');
						EasyUiCommon._mapProductCodeStr1.put(data.length-1,EasyUiCommon._mapProductCodeStr1.get(0));
						EasyUiCommon._mapNewDataSubGrid1.put(data.length-1,EasyUiCommon._mapNewDataSubGrid1.get(0));
						EasyUiCommon._mapNewDataSubGrid2.put(data.length-1,new Array());
					}
				}
			//}
		}
		if(isAppend){
			EasyUiCommon._totalRowGrid++;
		}
		$('#grid').datagrid('selectRow',$('#grid').datagrid('getRows').length-1);
	},
	loadCommonDataGrid:function(type,permission){
		EasyUiCommon.indexDeleted = [];
		EasyUiCommon._mapNewDataSubGrid1 = new Map();
		EasyUiCommon._mapNewDataSubGrid2 = new Map();		
		EasyUiCommon._mapProductCodeStr1 = new Map();
		EasyUiCommon._mapProductCodeStr2 = new Map();
		var urlComboGrid = '';
		var url = '';
		if(type == 'ZV01'){
			url = PromotionCatalog.getGridZV01Url($('#promotionId').val().trim(),type);
			EasyUiCommon.dataGrid01020405('grid',url,'productCode','productName','quantity','discountPercent','Mã SP','Tên SP','SL mua','% KM tiền',urlComboGrid,type,permission);
		}else if(type == 'ZV02'){
			url = PromotionCatalog.getGridZV01Url($('#promotionId').val().trim(),type);
			EasyUiCommon.dataGrid01020405('grid',url,'productCode','productName','quantity','discountAmount','Mã SP','Tên SP','SL mua','TT khuyến mãi',urlComboGrid,type,permission);
		}else if(type == 'ZV04'){
			url = PromotionCatalog.getGridZV01Url($('#promotionId').val().trim(),type);
			EasyUiCommon.dataGrid01020405('grid',url,'productCode','productName','amount','discountPercent','Mã SP','Tên SP','TT mua','% KM tiền',urlComboGrid,type,permission);
		}else if(type == 'ZV05'){
			url = PromotionCatalog.getGridZV01Url($('#promotionId').val().trim(),type);
			EasyUiCommon.dataGrid01020405('grid',url,'productCode','productName','amount','discountAmount','Mã SP','Tên SP','TT mua','TT khuyến mãi',urlComboGrid,type,permission);
		}else if(type == 'ZV03'){
			url = PromotionCatalog.getGridZV03Url($('#promotionId').val().trim(),type);
			EasyUiCommon.dataGrid0306('grid',url,'productCode','productName','quantity','freeProductCodeStr','Mã SP','Tên SP','SL mua','SP khuyến mãi',urlComboGrid,type,'saleQty','Số lượng',permission);
		}else if(type == 'ZV06'){
			url = PromotionCatalog.getGridZV03Url($('#promotionId').val().trim(),type);
			EasyUiCommon.dataGrid0306('grid',url,'productCode','productName','amount','freeProductCodeStr','Mã SP','Tên SP','TT mua','SP khuyến mãi',urlComboGrid,type,'saleQty','Số lượng',permission);
		}else if(type == 'ZV07'){
			url = PromotionCatalog.getGridZV07Url($('#promotionId').val().trim(),type);
			EasyUiCommon.dataGrid07081011('grid',url,'quantity','discountPercent','SL mua','% KM tiền',urlComboGrid,type,permission);
		}else if(type == 'ZV08'){
			url = PromotionCatalog.getGridZV07Url($('#promotionId').val().trim(),type);
			EasyUiCommon.dataGrid07081011('grid',url,'quantity','discountAmount','SL mua','TT khuyến mãi',urlComboGrid,type,permission);
		}else if(type == 'ZV10'){
			url = PromotionCatalog.getGridZV07Url($('#promotionId').val().trim(),type);
			EasyUiCommon.dataGrid07081011('grid',url,'amount','discountPercent','TT mua','% KM tiền',urlComboGrid,type,permission);
		}else if(type == 'ZV11'){
			url = PromotionCatalog.getGridZV07Url($('#promotionId').val().trim(),type);
			EasyUiCommon.dataGrid07081011('grid',url,'amount','discountAmount','TT mua','TT khuyến mãi',urlComboGrid,type,permission);
		}else if(type == 'ZV09'){
			url = PromotionCatalog.getGridZV09Url($('#promotionId').val().trim(),type);
			EasyUiCommon.dataGrid0912('grid',url,'quantity','SL mua',urlComboGrid,type,permission);
		}else if(type == 'ZV12'){
			url = PromotionCatalog.getGridZV09Url($('#promotionId').val().trim(),type);
			EasyUiCommon.dataGrid0912('grid',url,'amount','TT mua',urlComboGrid,type,permission);
		}else if(type == 'ZV15'){
			url = PromotionCatalog.getGridZV09Url($('#promotionId').val().trim(),type);
			EasyUiCommon.dataGrid1518('grid',url,urlComboGrid,type,permission);
		}else if(type == 'ZV18'){
			url = PromotionCatalog.getGridZV09Url($('#promotionId').val().trim(),type);
			EasyUiCommon.dataGrid1518('grid',url,urlComboGrid,type,permission);
		}else if(type == 'ZV13'){
			url = PromotionCatalog.getGridZV13Url($('#promotionId').val().trim(),type);
			EasyUiCommon.dataGrid13141617('grid',url,'discountPercent','% KM tiền',urlComboGrid,type,'quantity','Số lượng',permission);
		}else if(type == 'ZV14'){
			url = PromotionCatalog.getGridZV13Url($('#promotionId').val().trim(),type);
			EasyUiCommon.dataGrid13141617('grid',url,'discountAmount','TT khuyến mại',urlComboGrid,type,'quantity','Số lượng',permission);
		}else if(type == 'ZV16'){
			url = PromotionCatalog.getGridZV13Url($('#promotionId').val().trim(),type);
			EasyUiCommon.dataGrid13141617('grid',url,'discountPercent','% KM tiền',urlComboGrid,type,'amount','TT mua',permission);
		}else if(type == 'ZV17'){
			url = PromotionCatalog.getGridZV13Url($('#promotionId').val().trim(),type);
			EasyUiCommon.dataGrid13141617('grid',url,'discountAmount','TT khuyến mại',urlComboGrid,type,'amount','TT mua',permission);
		}else if(type == 'ZV19'){
			url = PromotionCatalog.getGridZV19Url($('#promotionId').val().trim(),type);
			EasyUiCommon.dataGrid1920('grid',url,'amount','discountPercent','TT mua','% KM tiền',urlComboGrid,type,permission);
		}else if(type == 'ZV20'){
			url = PromotionCatalog.getGridZV19Url($('#promotionId').val().trim(),type);
			EasyUiCommon.dataGrid1920('grid',url,'amount','discountAmount','TT mua','TT khuyến mãi',urlComboGrid,type,permission);
		}else if(type == 'ZV21'){
			url = PromotionCatalog.getGridZV21Url($('#promotionId').val().trim(),type);
			EasyUiCommon.dataGrid21('grid',url,'amount','freeProductCodeStr','TT mua','SP khuyến mãi',urlComboGrid,type,'quantity','SL khuyến mãi',permission);
		}
	},
	changeCommonGrid :function(type,permission){
		$('#divOverlay').show();
		$('#errMsgProduct').html('').hide();
		$('#errExcelMsg').html('').hide();	
		var lstString1 = new Array();
		var lstString2 = new Array();
		var lstString3 = new Array();
		var lstString4 = new Array();
		var lstNumber = [];
		var lstCode = [];
		var size = $('#grid').datagrid('getRows').length;
		EasyUiCommon.totalRowDelete = 0;
		//////DETAIL FOR SUBGRID
		for(var i= 0;i<size;i++){
//			var indexDetailErr = 0;
			var str = i.toString();
			if(type == 'ZV03' || type == 'ZV06'){
				var sizeDetail = 0;
				if(($.inArray(str,EasyUiCommon.indexDeleted) == -1) && (EasyUiCommon._mapNewDataSubGrid1.get(i) != null)){
					sizeDetail = EasyUiCommon._mapNewDataSubGrid1.get(i).length;
					if(sizeDetail > 0){
						lstString2.push(sizeDetail);
					}
					for(var k = 0;k<sizeDetail;k++){
						var objDetail = EasyUiCommon._mapNewDataSubGrid1.get(i);
						var a2 = [];
						if(objDetail[k].productCode == '' || objDetail[k].productCode == undefined){
							objDetail[k].productCode = 0;
						}
						a2.push(objDetail[k].productCode);
						if(objDetail[k].quantity == '' || objDetail[k].quantity == undefined){
							objDetail[k].quantity = 0;
						}
						a2.push(objDetail[k].quantity);
						lstString3.push(a2);
					}
				}
			}else if(type == 'ZV07' || type == 'ZV08' || type == 'ZV10' || type == 'ZV11' ){
				var sizeDetail = 0;
				if(($.inArray(str,EasyUiCommon.indexDeleted) == -1) && (EasyUiCommon._mapNewDataSubGrid1.get(i) != null)){
					sizeDetail = EasyUiCommon._mapNewDataSubGrid1.get(i).length;
					if(sizeDetail > 0){
						lstString2.push(sizeDetail);
					}
					for(var k = 0;k<sizeDetail;k++){
						var objDetail = EasyUiCommon._mapNewDataSubGrid1.get(i);
						var a2 = [];
						if(objDetail[k].productCode == '' || objDetail[k].productCode == undefined){
							objDetail[k].productCode = 0;
						}
						a2.push(objDetail[k].productCode);
						if(objDetail[k].required == '' || objDetail[k].required == undefined){
							objDetail[k].required = 0;
						}
						a2.push(objDetail[k].required);
						lstString3.push(a2);
					}
				}
			}else if(type == 'ZV09' || type == 'ZV12' || type == 'ZV15' || type == 'ZV18' ){
				var sizeDetail1 = 0;
				var sizeDetail2 = 0;
				if(($.inArray(str,EasyUiCommon.indexDeleted) == -1) && EasyUiCommon._mapNewDataSubGrid1.get(i) != null && EasyUiCommon._mapNewDataSubGrid2.get(i) != null){
					sizeDetail1 = EasyUiCommon._mapNewDataSubGrid1.get(i).length;
					sizeDetail2 = EasyUiCommon._mapNewDataSubGrid2.get(i).length;
					var sizeIndex = [];
					if(sizeDetail1 > 0 && sizeDetail2 > 0){
						sizeIndex.push(sizeDetail1);
						sizeIndex.push(sizeDetail2);
					}else{
						sizeIndex.push(0);
						sizeIndex.push(0);
					}
					lstString2.push(sizeIndex);
					for(var k = 0;k<sizeDetail1;k++){
						var objDetail = EasyUiCommon._mapNewDataSubGrid1.get(i);
						var a2 = [];
						if(objDetail[k].productCode == '' || objDetail[k].productCode == undefined){
							objDetail[k].productCode = 0;
						}
						if(type == 'ZV15' || type == 'ZV18'){
							if(i == 0){
								lstCode.push(objDetail[k].productCode.trim());
							}else{
								if(lstCode.indexOf(objDetail[k].productCode.trim()) == -1){
									$('#errMsgProduct').html('Sản phẩm mua ở các dòng bắt buộc phải giống nhau.').show();
									break;
								}
							}
						}
						a2.push(objDetail[k].productCode);
						if(type == 'ZV09' || type == 'ZV12'){
							if(objDetail[k].required == '' || objDetail[k].required == undefined){
								objDetail[k].required = 0;
							}
							a2.push(objDetail[k].required);
						}else if(type == 'ZV15'){
							if(objDetail[k].quantity == '' || objDetail[k].quantity == undefined){
								objDetail[k].quantity = 0;
							}
							a2.push(objDetail[k].quantity);
						}else if(type == 'ZV18'){
							if(objDetail[k].amount == '' || objDetail[k].amount == undefined){
								objDetail[k].amount = 0;
							}
							a2.push(objDetail[k].amount);
						}
						lstString3.push(a2);
					}
					for(var j = 0;j<sizeDetail2;j++){
						var objDetail = EasyUiCommon._mapNewDataSubGrid2.get(i);
						var a2 = [];
						if(objDetail[j].productCode == '' || objDetail[j].productCode == undefined){
							objDetail[j].productCode = 0;
						}
						a2.push(objDetail[j].productCode);
						if(objDetail[j].quantity == '' || objDetail[j].quantity == undefined){
							objDetail[j].quantity = 0;
						}
						a2.push(objDetail[j].quantity);
						lstString4.push(a2);
					}
				}
			}else if(type == 'ZV13' || type == 'ZV14' || type == 'ZV16' || type == 'ZV17' ){
				var sizeDetail = 0;
				if(($.inArray(str,EasyUiCommon.indexDeleted) == -1) && (EasyUiCommon._mapNewDataSubGrid1.get(i) != null)){
					sizeDetail = EasyUiCommon._mapNewDataSubGrid1.get(i).length;
					if(lstString2.length > 0 && (lstString2.indexOf(sizeDetail) == -1 || lstString2.indexOf(parseInt(sizeDetail)) == -1)){
						$('#errMsgProduct').html('Sản phẩm mua ở các dòng bắt buộc phải giống nhau.').show();
						break;
					}
					if(sizeDetail > 0){
						lstString2.push(sizeDetail);
					}
					for(var k = 0;k<sizeDetail;k++){
						var objDetail = EasyUiCommon._mapNewDataSubGrid1.get(i);
						var a2 = [];
						if(objDetail[k].productCode == '' || objDetail[k].productCode == undefined){
							objDetail[k].productCode = 0;
						}
						if(i == 0){
							lstCode.push(objDetail[k].productCode.trim());
						}else{
							if(lstCode.indexOf(objDetail[k].productCode.trim()) == -1){
								$('#errMsgProduct').html('Sản phẩm mua ở các dòng bắt buộc phải giống nhau.').show();
								break;
							}
						}
						a2.push(objDetail[k].productCode);
						if(type == 'ZV13' || type == 'ZV14'){
							if(objDetail[k].quantity == '' || objDetail[k].quantity == undefined){
								objDetail[k].quantity = 0;
							}
							a2.push(objDetail[k].quantity);
						}else{
							if(objDetail[k].amount == '' || objDetail[k].amount == undefined){
								objDetail[k].amount = 0;
							}
							a2.push(objDetail[k].amount);
						} 
						lstString3.push(a2);
					}
				}
			}else if(type == 'ZV21'){
				var sizeDetail = 0;
				if(($.inArray(str,EasyUiCommon.indexDeleted) == -1) && EasyUiCommon._mapNewDataSubGrid1.get(i) != null){
					sizeDetail = EasyUiCommon._mapNewDataSubGrid1.get(i).length;
					if(sizeDetail > 0){
						lstString2.push(sizeDetail);
					}
					for(var k = 0;k<sizeDetail;k++){
						var objDetail = EasyUiCommon._mapNewDataSubGrid1.get(i);
						var a2 = [];
						if(objDetail[k].productCode == '' || objDetail[k].productCode == undefined){
							objDetail[k].productCode = 0;
						}
						a2.push(objDetail[k].productCode);
						if(objDetail[k].quantity == '' || objDetail[k].quantity == undefined){
							objDetail[k].quantity = 0;
						}
						a2.push(objDetail[k].quantity);
						lstString3.push(a2);
					}
				}
			}
		}
		
		//////HEADER FOR GRID
		if($('#errMsgProduct').html() != '' && $('#errMsgProduct').html().length > 0 ){
			$('#divOverlay').hide();
			return false;
		}
		var indexErr = 0;
		var indexConfirm = 0;
		var isConfirm = false;
		for(var i= 0;i<size;i++){
			var str = i.toString();
			if(i == 0){
				$('#grid').datagrid('acceptChanges');
				EasyUiCommon.formatLabelAfterChange(type);
			}
			var objHeader = $('#grid').datagrid('getData').rows;
			if($('#errMsgProduct').html() == '' || $('#errMsgProduct').html().length == 0 ){
				indexErr = i +1;
				if(type == 'ZV01' || type == 'ZV02' || type == 'ZV04' || type == 'ZV05'){
					if($.inArray(str,EasyUiCommon.indexDeleted) == -1){
						if(objHeader[i].productCode != '' && objHeader[i].productCode != undefined ){
							var numExist = 0;
							if(lstString1.indexOf(objHeader[i].productCode) > -1){
								numExist = 1;
							}
							lstString1.push(objHeader[i].productCode);
							if(type == 'ZV01'){
								if(numExist == 1){
									if(lstString2.indexOf(parseInt(objHeader[i].quantity)) > -1 || lstString2.indexOf(objHeader[i].quantity) > -1){
										numExist = 2;
									}
								}
								if(numExist == 2){
									$('#errMsgProduct').html(' Sản phẩm ' + Utils.XSSEncode(objHeader[i].productCode) + ' với số lượng mua này đã tồn tại.').show();
								}
								lstString2.push(objHeader[i].quantity);
								lstString3.push(objHeader[i].discountPercent);
								if(objHeader[i].quantity == ''|| objHeader[i].quantity == undefined || objHeader[i].quantity <= 0){
									$('#errMsgProduct').html('Số lượng mua ở dòng ' + Utils.XSSEncode(indexErr) + ' phải lớn hơn 0').show();
								}
								if(objHeader[i].discountPercent == ''|| objHeader[i].discountPercent == undefined || objHeader[i].discountPercent <= 0){
									$('#errMsgProduct').html('%KM tiền ở dòng ' + Utils.XSSEncode(indexErr) + ' phải lớn hơn 0').show();
								}
								if(objHeader[i].discountPercent == ''|| objHeader[i].discountPercent == undefined || objHeader[i].discountPercent > 100){
									$('#errMsgProduct').html('%KM tiền ở dòng ' + Utils.XSSEncode(indexErr) + ' phải nhỏ hơn 100').show();
								}
							}else if(type == 'ZV02'){
								if(numExist == 1){
									if(lstString2.indexOf(parseInt(objHeader[i].quantity)) > -1 || lstString2.indexOf(objHeader[i].quantity) > -1){
										numExist = 2;
									}
								}
								if(numExist == 2){
									$('#errMsgProduct').html(' Sản phẩm ' + Utils.XSSEncode(objHeader[i].productCode) +' với số lượng mua này đã tồn tại.').show();
								}
								lstString2.push(objHeader[i].quantity);
								lstString3.push(objHeader[i].discountAmount);
								if(objHeader[i].quantity == ''|| objHeader[i].quantity == undefined || objHeader[i].quantity <= 0){
									$('#errMsgProduct').html('Số lượng mua ở dòng ' + Utils.XSSEncode(indexErr) + ' phải lớn hơn 0').show();
								}
								if(objHeader[i].discountAmount == ''|| objHeader[i].discountAmount == undefined || objHeader[i].discountAmount <= 0){
									$('#errMsgProduct').html('Tổng tiền khuyến mãi ở dòng ' + Utils.XSSEncode(indexErr) + ' phải lớn hơn 0').show();
								}
							}else if(type == 'ZV04'){
								if(numExist == 1){
									if(lstString2.indexOf(parseInt(objHeader[i].amount)) > -1 || lstString2.indexOf(objHeader[i].amount) > -1){
										numExist = 2;
									}
								}
								if(numExist == 2){
									$('#errMsgProduct').html(' Sản phẩm ' + Utils.XSSEncode(objHeader[i].productCode) +' với TT mua này đã tồn tại.').show();
								}
								lstString2.push(objHeader[i].amount);
								lstString3.push(objHeader[i].discountPercent);
								if(objHeader[i].amount == ''|| objHeader[i].amount == undefined || objHeader[i].amount <= 0){
									$('#errMsgProduct').html('Tổng tiền mua ở dòng ' + Utils.XSSEncode(indexErr) + ' phải lớn hơn 0').show();
								}
								if(objHeader[i].discountPercent == ''|| objHeader[i].discountPercent == undefined || objHeader[i].discountPercent <= 0){
									$('#errMsgProduct').html('%KM tiền ở dòng ' + Utils.XSSEncode(indexErr) + ' phải lớn hơn 0').show();
								}
								if(objHeader[i].discountPercent == ''|| objHeader[i].discountPercent == undefined || objHeader[i].discountPercent > 100){
									$('#errMsgProduct').html('%KM tiền ở dòng ' + Utils.XSSEncode(indexErr) + ' phải nhỏ hơn 100').show();
								}
							}else if(type == 'ZV05'){
								if(numExist == 1){
									if(lstString2.indexOf(parseInt(objHeader[i].amount)) > -1 || lstString2.indexOf(objHeader[i].amount) > -1){
										numExist = 2;
									}
								}
								if(numExist == 2){
									$('#errMsgProduct').html(' Sản phẩm ' + Utils.XSSEncode(objHeader[i].productCode) + ' với TT mua này đã tồn tại.').show();
								}
								lstString2.push(objHeader[i].amount);
								lstString3.push(objHeader[i].discountAmount);
								if(objHeader[i].amount == ''|| objHeader[i].amount == undefined || objHeader[i].amount <= 0){
									$('#errMsgProduct').html('Tổng tiền mua ở dòng ' + Utils.XSSEncode(indexErr) + ' phải lớn hơn 0').show();
								}
								if(objHeader[i].discountAmount == ''|| objHeader[i].discountAmount == undefined || objHeader[i].discountAmount <= 0){
									$('#errMsgProduct').html('Tổng tiền khuyến mãi ở dòng ' + Utils.XSSEncode(indexErr) + ' phải lớn hơn 0').show();
								}
								if(objHeader[i].amount != '' &&  objHeader[i].discountAmount != '' && parseInt(objHeader[i].amount) < parseInt(objHeader[i].discountAmount)){
									isConfirm = true;
									indexConfirm = indexErr;
								}
							}
						}else{
							if(type == 'ZV01'){
								if((objHeader[i].quantity != '' && objHeader[i].quantity > 0) || (objHeader[i].discountPercent != '' && objHeader[i].discountPercent > 0)){
									$('#errMsgProduct').html('Vui lòng nhập mã SP mua ở dòng ' + Utils.XSSEncode(indexErr)).show();
								}
							}else if(type == 'ZV02'){
								if((objHeader[i].quantity != '' && objHeader[i].quantity > 0) || (objHeader[i].discountAmount != '' && objHeader[i].discountAmount > 0)){
									$('#errMsgProduct').html('Vui lòng nhập mã SP mua ở dòng ' + Utils.XSSEncode(indexErr)).show();
								}
							}else if(type == 'ZV04'){
								if((objHeader[i].quantity != '' && objHeader[i].quantity > 0) || (objHeader[i].discountPercent != '' && objHeader[i].discountPercent > 0)){
									$('#errMsgProduct').html('Vui lòng nhập mã SP mua ở dòng ' + Utils.XSSEncode(indexErr)).show();
								}
							}else if(type == 'ZV05'){
								if((objHeader[i].amount != '' && objHeader[i].amount > 0) || (objHeader[i].discountAmount != '' && objHeader[i].discountAmount > 0)){
									$('#errMsgProduct').html('Vui lòng nhập mã SP mua ở dòng ' + Utils.XSSEncode(indexErr)).show();
								}
							}
						}
					}
				}else if(type == 'ZV03' || type == 'ZV06'){
					if(($.inArray(str,EasyUiCommon.indexDeleted) == -1)){// && (EasyUiCommon._mapNewDataSubGrid1.get(i) != null)){
						if(objHeader[i].productCode != '' && objHeader[i].productCode != undefined ){
							var a1= [];
							a1.push(objHeader[i].productCode);
							if(type == 'ZV03'){
								a1.push(objHeader[i].quantity);
								if(objHeader[i].quantity == ''|| objHeader[i].quantity == undefined || objHeader[i].quantity <= 0){
									$('#errMsgProduct').html('Số lượng mua ở dòng ' + Utils.XSSEncode(indexErr) +' phải lớn hơn 0').show();
								}
								if(EasyUiCommon._mapProductCodeStr1.get(i) == null || EasyUiCommon._mapProductCodeStr1.get(i).length == 0){
									$('#errMsgProduct').html('Vui lòng nhập SP khuyến mãi ở dòng ' + Utils.XSSEncode(indexErr)).show();
								}
								for(var j=0;j<lstString1.length;j++){
									if(lstString1[j][0] == a1[0]&&lstString1[j][1] == a1[1]){
										$('#errMsgProduct').html('SP khuyến mãi và số lượng ở dòng ' + Utils.XSSEncode(indexErr) +' đã bị trùng').show();
									}
								}
							}else if(type == 'ZV06'){
								a1.push(objHeader[i].amount);
								if(objHeader[i].amount == ''|| objHeader[i].amount == undefined || objHeader[i].amount <= 0){
									$('#errMsgProduct').html('Tổng tiền mua ở dòng ' + Utils.XSSEncode(indexErr) +' phải lớn hơn 0').show();
								}
								if(EasyUiCommon._mapProductCodeStr1.get(i) == null || EasyUiCommon._mapProductCodeStr1.get(i).length == 0){
									$('#errMsgProduct').html('Vui lòng nhập SP khuyến mãi ở dòng ' + indexErr).show();
								}
								for(var j=0;j<lstString1.length;j++){
									if(lstString1[j][0] == a1[0]&&lstString1[j][1] == a1[1]){
										$('#errMsgProduct').html('SP khuyến mãi và tổng tiền ở dòng ' + Utils.XSSEncode(indexErr) +' đã bị trùng').show();
									}
								}
							}
							lstString1.push(a1);
						}else{
							if(type == 'ZV03'){
								if((objHeader[i].quantity != '' && objHeader[i].quantity > 0) || (EasyUiCommon._mapProductCodeStr1.get(i) != null && EasyUiCommon._mapProductCodeStr1.get(i).length > 0)){
									$('#errMsgProduct').html('Vui lòng nhập mã SP mua ở dòng ' + Utils.XSSEncode(indexErr)).show();
								}
							}else if(type == 'ZV06'){
								if((objHeader[i].amount != '' && objHeader[i].amount > 0) || (EasyUiCommon._mapProductCodeStr1.get(i) != null && EasyUiCommon._mapProductCodeStr1.get(i).length > 0)){
									$('#errMsgProduct').html('Vui lòng nhập mã SP mua ở dòng ' + Utils.XSSEncode(indexErr)).show();
								}
							}
						}
					}
				}else if(type == 'ZV07' || type == 'ZV08' ||type == 'ZV10' || type == 'ZV11'){
					if(($.inArray(str,EasyUiCommon.indexDeleted) == -1) && (EasyUiCommon._mapNewDataSubGrid1.get(i) != null)){
						if(EasyUiCommon._mapNewDataSubGrid1.get(i) != null && EasyUiCommon._mapNewDataSubGrid1.get(i).length > 0){
							var a1= [];
							if(type == 'ZV07'){
								var numExist = 0;
								if(lstNumber.indexOf(parseInt(objHeader[i].quantity)) > -1 || lstNumber.indexOf(objHeader[i].quantity) > -1){
									numExist = 1;
								}
								if(numExist == 1){
									$('#errMsgProduct').html('Số lượng mua ở dòng ' + Utils.XSSEncode(indexErr) +' đã tồn tại').show();
								}
								lstNumber.push(objHeader[i].quantity);
								a1.push(objHeader[i].quantity);
								a1.push(objHeader[i].discountPercent);
								if(objHeader[i].quantity == ''|| objHeader[i].quantity == undefined || objHeader[i].quantity <= 0){
									$('#errMsgProduct').html('Số lượng mua ở dòng ' + Utils.XSSEncode(indexErr) +' phải lớn hơn 0').show();
								}
								if(objHeader[i].discountPercent == ''|| objHeader[i].discountPercent == undefined || objHeader[i].discountPercent <= 0){
									$('#errMsgProduct').html('%KM tiền ở dòng ' + Utils.XSSEncode(indexErr) +' phải lớn hơn 0').show();
								}
								if(objHeader[i].discountPercent == ''|| objHeader[i].discountPercent == undefined || objHeader[i].discountPercent > 100){
									$('#errMsgProduct').html('%KM tiền ở dòng ' + Utils.XSSEncode(indexErr) +' phải nhỏ hơn 100').show();
								}
							}else if(type == 'ZV08'){
								var numExist = 0;
								if(lstNumber.indexOf(parseInt(objHeader[i].quantity)) > -1 || lstNumber.indexOf(objHeader[i].quantity) > -1){
									numExist = 1;
								}
								if(numExist == 1){
									$('#errMsgProduct').html('Số lượng mua ở dòng ' + Utils.XSSEncode(indexErr) +' đã tồn tại').show();
								}
								lstNumber.push(objHeader[i].quantity);
								a1.push(objHeader[i].quantity);
								a1.push(objHeader[i].discountAmount);
								if(objHeader[i].quantity == ''|| objHeader[i].quantity == undefined || objHeader[i].quantity <= 0){
									$('#errMsgProduct').html('Số lượng mua ở dòng ' + Utils.XSSEncode(indexErr) +' phải lớn hơn 0').show();
								}
								if(objHeader[i].discountAmount == ''|| objHeader[i].discountAmount == undefined || objHeader[i].discountAmount <= 0){
									$('#errMsgProduct').html('Tổng tiền khuyến mãi ở dòng ' + Utils.XSSEncode(indexErr) +' phải lớn hơn 0').show();
								}
							}else if(type == 'ZV10'){
								var numExist = 0;
								if(lstNumber.indexOf(parseInt(objHeader[i].amount)) > -1 || lstNumber.indexOf(objHeader[i].amount) > -1){
									numExist = 1;
								}
								if(numExist == 1){
									$('#errMsgProduct').html('Tổng tiền mua ở dòng ' + Utils.XSSEncode(indexErr) +' đã tồn tại').show();
								}
								lstNumber.push(objHeader[i].amount);
								a1.push(objHeader[i].amount);
								a1.push(objHeader[i].discountPercent);
								if(objHeader[i].amount == ''|| objHeader[i].amount == undefined || objHeader[i].amount <= 0){
									$('#errMsgProduct').html('Tổng tiền mua ở dòng ' + Utils.XSSEncode(indexErr) +' phải lớn hơn 0').show();
								}
								if(objHeader[i].discountPercent == ''|| objHeader[i].discountPercent == undefined || objHeader[i].discountPercent <= 0){
									$('#errMsgProduct').html('%KM tiền ở dòng ' + Utils.XSSEncode(indexErr) +' phải lớn hơn 0').show();
								}
								if(objHeader[i].discountPercent == ''|| objHeader[i].discountPercent == undefined || objHeader[i].discountPercent > 100){
									$('#errMsgProduct').html('%KM tiền ở dòng ' + Utils.XSSEncode(indexErr) +' phải nhỏ hơn 100').show();
								}
							}else if(type == 'ZV11'){
								var numExist = 0;
								if(lstNumber.indexOf(parseInt(objHeader[i].amount)) > -1 || lstNumber.indexOf(objHeader[i].amount) > -1){
									numExist = 1;
								}
								if(numExist == 1){
									$('#errMsgProduct').html('Tổng tiền mua ở dòng ' + Utils.XSSEncode(indexErr) +' đã tồn tại').show();
								}
								lstNumber.push(objHeader[i].amount);
								a1.push(objHeader[i].amount);
								a1.push(objHeader[i].discountAmount);
								if(objHeader[i].amount == ''|| objHeader[i].amount == undefined || objHeader[i].amount <= 0){
									$('#errMsgProduct').html('Tổng tiền mua ở dòng ' + Utils.XSSEncode(indexErr) +' phải lớn hơn 0').show();
								}
								if(objHeader[i].discountAmount == ''|| objHeader[i].discountAmount == undefined || objHeader[i].discountAmount <= 0){
									$('#errMsgProduct').html('Tổng tiền khuyến mãi ở dòng ' + Utils.XSSEncode(indexErr) +' phải lớn hơn 0').show();
								}
							}
							lstString1.push(a1);
						}else{
							if(type == 'ZV07'){
								if((objHeader[i].quantity != '' && objHeader[i].quantity > 0) || (objHeader[i].discountPercent != '' && objHeader[i].discountPercent > 0)){
									$('#errMsgProduct').html('Vui lòng nhập mã SP mua ở dòng ' + Utils.XSSEncode(indexErr)).show();
								}
							}else if(type == 'ZV08'){
								if((objHeader[i].quantity != '' && objHeader[i].quantity > 0) || (objHeader[i].discountAmount != '' && objHeader[i].discountAmount > 0)){
									$('#errMsgProduct').html('Vui lòng nhập mã SP mua ở dòng ' + Utils.XSSEncode(indexErr)).show();
								}
							}else if(type == 'ZV10'){
								if((objHeader[i].amount != '' && objHeader[i].amount > 0) || (objHeader[i].discountPercent != '' && objHeader[i].discountPercent > 0)){
									$('#errMsgProduct').html('Vui lòng nhập mã SP mua ở dòng ' + Utils.XSSEncode(indexErr)).show();
								}
							}else if(type == 'ZV11'){
								if((objHeader[i].amount != '' && objHeader[i].amount > 0) || (objHeader[i].discountAmount != '' && objHeader[i].discountAmount > 0)){
									$('#errMsgProduct').html('Vui lòng nhập mã SP mua ở dòng ' + Utils.XSSEncode(indexErr)).show();
								}
							}
						}
					}
				}else if(type == 'ZV09' || type == 'ZV12' ||type == 'ZV15' || type == 'ZV18'){
					if(($.inArray(str,EasyUiCommon.indexDeleted) == -1)){
						if(EasyUiCommon._mapNewDataSubGrid1.get(i) != null && EasyUiCommon._mapNewDataSubGrid1.get(i).length > 0){
							var a1= [];
							if(type == 'ZV09'){
								var numExist = 0;
								if(lstNumber.indexOf(parseInt(objHeader[i].quantity)) > -1 || lstNumber.indexOf(objHeader[i].quantity) > -1){
									numExist = 1;
								}
								if(numExist == 1){
									$('#errMsgProduct').html('Số lượng mua ở dòng ' + Utils.XSSEncode(indexErr) +' đã tồn tại').show();
								}
								lstNumber.push(objHeader[i].quantity);
								a1.push(objHeader[i].quantity);
								if(objHeader[i].quantity == ''|| objHeader[i].quantity == undefined || objHeader[i].quantity <= 0){
									$('#errMsgProduct').html('Số lượng mua ở dòng ' + Utils.XSSEncode(indexErr) +' phải lớn hơn 0').show();
								}
								if(EasyUiCommon._mapNewDataSubGrid2.get(i) == null || EasyUiCommon._mapNewDataSubGrid2.get(i).length == 0){
									$('#errMsgProduct').html('Vui lòng nhập SP khuyến mãi ở dòng ' + Utils.XSSEncode(indexErr)).show();
								}
								lstString1.push(a1);
							}else if(type == 'ZV12'){
								var numExist = 0;
								if(lstNumber.indexOf(parseInt(objHeader[i].amount)) > -1 || lstNumber.indexOf(objHeader[i].amount) > -1){
									numExist = 1;
								}
								if(numExist == 1){
									$('#errMsgProduct').html('Tổng tiền mua ở dòng ' + Utils.XSSEncode(indexErr) +' đã tồn tại').show();
								}
								lstNumber.push(objHeader[i].amount);
								a1.push(objHeader[i].amount);
								if(objHeader[i].amount == ''|| objHeader[i].amount == undefined || objHeader[i].amount <= 0){
									$('#errMsgProduct').html('Tổng tiền mua ở dòng ' + Utils.XSSEncode(indexErr) +' phải lớn hơn 0').show();
								}
								if(EasyUiCommon._mapNewDataSubGrid2.get(i) == null || EasyUiCommon._mapNewDataSubGrid2.get(i).length == 0){
									$('#errMsgProduct').html('Vui lòng nhập SP khuyến mãi ở dòng ' + Utils.XSSEncode(indexErr)).show();
								}
								lstString1.push(a1);
							}else if(type == 'ZV15'){
								if(EasyUiCommon._mapNewDataSubGrid2.get(i) == null || EasyUiCommon._mapNewDataSubGrid2.get(i).length == 0){
									$('#errMsgProduct').html('Vui lòng nhập SP khuyến mãi ở dòng ' + Utils.XSSEncode(indexErr)).show();
								}
							}else if(type == 'ZV18'){
								if(EasyUiCommon._mapNewDataSubGrid2.get(i) == null || EasyUiCommon._mapNewDataSubGrid2.get(i).length == 0){
									$('#errMsgProduct').html('Vui lòng nhập SP khuyến mãi ở dòng ' + Utils.XSSEncode(indexErr)).show();
								}
							}
						}else{
							if(type == 'ZV09'){
								if((objHeader[i].quantity != '' && objHeader[i].quantity > 0) || (EasyUiCommon._mapNewDataSubGrid2.get(i) != null && EasyUiCommon._mapNewDataSubGrid2.get(i).length > 0)){
									$('#errMsgProduct').html('Vui lòng nhập mã SP mua ở dòng ' + Utils.XSSEncode(indexErr)).show();
								}
							}else if(type == 'ZV12'){
								if((objHeader[i].amount != '' && objHeader[i].amount > 0) || (EasyUiCommon._mapNewDataSubGrid2.get(i) != null && EasyUiCommon._mapNewDataSubGrid2.get(i).length > 0)){
									$('#errMsgProduct').html('Vui lòng nhập mã SP mua ở dòng ' + Utils.XSSEncode(indexErr)).show();
								}
							}else if(type == 'ZV15'){
								if(EasyUiCommon._mapNewDataSubGrid1.get(i) == null || EasyUiCommon._mapNewDataSubGrid1.get(i).length == 0){
									$('#errMsgProduct').html('Vui lòng nhập mã SP mua ở dòng ' + Utils.XSSEncode(indexErr)).show();
								}
							}else if(type == 'ZV18'){
								if(EasyUiCommon._mapNewDataSubGrid1.get(i) == null || EasyUiCommon._mapNewDataSubGrid1.get(i).length == 0){
									$('#errMsgProduct').html('Vui lòng nhập mã SP mua ở dòng ' + Utils.XSSEncode(indexErr)).show();
								}
							}
						}
					}
				}else if(type == 'ZV13' || type == 'ZV14' ||type == 'ZV16' || type == 'ZV17'){
					if(($.inArray(str,EasyUiCommon.indexDeleted) == -1)){
						if(EasyUiCommon._mapNewDataSubGrid1.get(i) != null && EasyUiCommon._mapNewDataSubGrid1.get(i).length > 0){
							var a1= [];
							if(type == 'ZV13'){
								a1.push(objHeader[i].discountPercent);
								if(objHeader[i].discountPercent == ''|| objHeader[i].discountPercent == undefined || objHeader[i].discountPercent <= 0){
									$('#errMsgProduct').html('%KM tiền ở dòng ' + Utils.XSSEncode(indexErr) +' phải lớn hơn 0').show();
								}
								if(objHeader[i].discountPercent == ''|| objHeader[i].discountPercent == undefined || objHeader[i].discountPercent > 100){
									$('#errMsgProduct').html('%KM tiền ở dòng ' + Utils.XSSEncode(indexErr) +' phải nhỏ hơn 100').show();
								}
							}else if(type == 'ZV14'){
								a1.push(objHeader[i].discountAmount);
								if(objHeader[i].discountAmount == ''|| objHeader[i].discountAmount == undefined || objHeader[i].discountAmount <= 0){
									$('#errMsgProduct').html('Tổng tiền khuyến mãi ở dòng ' + Utils.XSSEncode(indexErr) +' phải lớn hơn 0').show();
								}
							}else if(type == 'ZV16'){
								a1.push(objHeader[i].discountPercent);
								if(objHeader[i].discountPercent == ''|| objHeader[i].discountPercent == undefined || objHeader[i].discountPercent <= 0){
									$('#errMsgProduct').html('%KM tiền ở dòng ' + Utils.XSSEncode(indexErr) +' phải lớn hơn 0').show();
								}
								if(objHeader[i].discountPercent == ''|| objHeader[i].discountPercent == undefined || objHeader[i].discountPercent > 100){
									$('#errMsgProduct').html('%KM tiền ở dòng ' + Utils.XSSEncode(indexErr) +' phải nhỏ hơn 100').show();
								}
							}else if(type == 'ZV17'){
								a1.push(objHeader[i].discountAmount);
								if(objHeader[i].discountAmount == ''|| objHeader[i].discountAmount == undefined || objHeader[i].discountAmount <= 0){
									$('#errMsgProduct').html('Tổng tiền khuyến mãi ở dòng ' + Utils.XSSEncode(indexErr) +' phải lớn hơn 0').show();
								}
							}
							lstString1.push(a1);
						}else{
							if(type == 'ZV13'){
								if(objHeader[i].discountPercent != '' && objHeader[i].discountPercent > 0){
									$('#errMsgProduct').html('Vui lòng nhập mã SP mua ở dòng ' + Utils.XSSEncode(indexErr)).show();
								}
							}else if(type == 'ZV14'){
								if(objHeader[i].discountAmount != '' && objHeader[i].discountAmount > 0){
									$('#errMsgProduct').html('Vui lòng nhập mã SP mua ở dòng ' + Utils.XSSEncode(indexErr)).show();
								}
							}else if(type == 'ZV16'){
								if(objHeader[i].discountPercent != '' && objHeader[i].discountPercent > 0){
									$('#errMsgProduct').html('Vui lòng nhập mã SP mua ở dòng ' + Utils.XSSEncode(indexErr)).show();
								}
							}else if(type == 'ZV17'){
								if(objHeader[i].discountAmount != '' && objHeader[i].discountAmount > 0){
									$('#errMsgProduct').html('Vui lòng nhập mã SP mua ở dòng ' + Utils.XSSEncode(indexErr)).show();
								}
							}
						}
					}
				}else if(type == 'ZV19' || type == 'ZV20'){
					if($.inArray(str,EasyUiCommon.indexDeleted) == -1){
						if(objHeader[i].amount != '' && objHeader[i].amount > 0){
							lstString1.push(objHeader[i].amount);
							if(type == 'ZV19'){
								lstString2.push(objHeader[i].discountPercent);
								if(objHeader[i].discountPercent == ''|| objHeader[i].discountPercent == undefined || objHeader[i].discountPercent <= 0){
									$('#errMsgProduct').html('%KM tiền ở dòng ' + Utils.XSSEncode(indexErr) +' phải lớn hơn 0').show();
								}
								if(objHeader[i].discountPercent == ''|| objHeader[i].discountPercent == undefined || objHeader[i].discountPercent > 100){
									$('#errMsgProduct').html('%KM tiền ở dòng ' + Utils.XSSEncode(indexErr) +' phải nhỏ hơn 100').show();
								}
								for(var j=i+1;j<size;j++){
									if(objHeader[i].amount == objHeader[j].amount){
										$('#errMsgProduct').html('Tổng tiền mua ở dòng ' + (i+1)+' trùng với tổng tiền mua ở dòng ' +(j+1)).show();
										$('#successMsgProduct').html('').hide();
									}
								}
							}else if(type == 'ZV20'){
								lstString2.push(objHeader[i].discountAmount);
								if(objHeader[i].amount == ''|| objHeader[i].amount == undefined || objHeader[i].amount <= 0){
									$('#errMsgProduct').html('Tổng tiền mua ở dòng ' + Utils.XSSEncode(indexErr) +' phải lớn hơn 0').show();
								}
								if(objHeader[i].discountAmount == ''|| objHeader[i].discountAmount == undefined || objHeader[i].discountAmount <= 0){
									$('#errMsgProduct').html('Tổng tiền khuyến mãi ở dòng ' + Utils.XSSEncode(indexErr) +' phải lớn hơn 0').show();
								}
								for(var j=i+1;j<size;j++){
									if(objHeader[i].amount == objHeader[j].amount){
										$('#errMsgProduct').html('Tổng tiền mua ở dòng ' + (i+1)+' trùng với tổng tiền mua ở dòng ' +(j+1)).show();
										$('#successMsgProduct').html('').hide();
									}
								}
								if(objHeader[i].amount != '' &&  objHeader[i].discountAmount != '' && parseInt(objHeader[i].amount) < parseInt(objHeader[i].discountAmount)){
									isConfirm = true;
									indexConfirm = indexErr;
								}
							}
						}else{
							if(type == 'ZV19'){
								if(objHeader[i].discountPercent != '' && objHeader[i].discountPercent > 0){
									$('#errMsgProduct').html('Vui lòng nhập tổng tiền mua ở dòng ' + Utils.XSSEncode(indexErr)).show();
								}
							}else if(type == 'ZV20'){
								if(objHeader[i].discountAmount != '' && objHeader[i].discountAmount > 0){
									$('#errMsgProduct').html('Vui lòng nhập tổng tiền mua ở dòng ' + Utils.XSSEncode(indexErr)).show();
								}
							}
						}
					}
				}else if(type == 'ZV21'){
					if(($.inArray(str,EasyUiCommon.indexDeleted) == -1) && (EasyUiCommon._mapNewDataSubGrid1.get(i) != null)){
						if(objHeader[i].amount != '' && objHeader[i].amount > 0){
							lstString1.push(objHeader[i].amount);
							if(EasyUiCommon._mapNewDataSubGrid1.get(i) == null || EasyUiCommon._mapNewDataSubGrid1.get(i).length == 0){
								$('#errMsgProduct').html('Vui lòng nhập SP khuyến mãi ở dòng ' + Utils.XSSEncode(indexErr)).show();
							}
						}else{
							if(EasyUiCommon._mapNewDataSubGrid1.get(i) != null && EasyUiCommon._mapNewDataSubGrid1.get(i).length > 0){
								$('#errMsgProduct').html('Vui lòng nhập TT mua ở dòng ' + Utils.XSSEncode(indexErr)).show();
							}
						}
						for(var j=i+1;j<size;j++){
							if(objHeader[i].amount == objHeader[j].amount){
								$('#errMsgProduct').html('Tổng tiền mua ở dòng ' + (i+1)+' trùng với tổng tiền mua ở dòng ' +(j+1)).show();
								$('#successMsgProduct').html('').hide();
							}
						}
					}
				}
			}
		}
		if($('#errMsgProduct').html() != '' && $('#errMsgProduct').html().length > 0 ){
			$('#grid').datagrid('selectRow',indexErr - 1);
			$('#divOverlay').hide();
			return false;
		}
//		console.log(lstString1);
//		console.log(lstString2);
//		console.log(lstString3);
//		console.log(lstString4);
//		if(type == 'ZV13' ||type == 'ZV14' ||type == 'ZV15' || type == 'ZV16' ||type == 'ZV17' ||type == 'ZV18'){
//		}
		var dataModel = new Object();		
		dataModel.promotionId = $('#promotionId').val().trim();
		dataModel.lstString1 = lstString1;
		dataModel.lstString2 = lstString2;
		dataModel.lstString3 = lstString3;
		if(type == 'ZV09' ||type == 'ZV12' ||type == 'ZV15' ||type == 'ZV18'){
			dataModel.lstString4 = lstString4;
		}
		$('#divOverlay').hide();
		if(isConfirm){
			$.messager.confirm('Xác nhận', 'Bạn nhập tổng tiền khuyến mại lớn hơn tổng tiền mua ở dòng ' + Utils.XSSEncode(indexConfirm) + '. Bạn có muốn cập nhật hay không?', function(r){
				if (r){
					Utils.saveData(dataModel, "/catalog/promotion/change-common-data-grid", EasyUiCommon._xhrSave, null,function(data){
						EasyUiCommon.loadCommonDataGrid(type,permission);
						$('#successMsgProduct').html('Cập nhật thành công.').show();
						setTimeout(function() { $("#successMsgProduct").hide(); }, 3000);
					});
				}
			});	
		}else{
			Utils.addOrSaveData(dataModel, '/catalog/promotion/change-common-data-grid', EasyUiCommon._xhrSave,null, function(data){
				EasyUiCommon.loadCommonDataGrid(type,permission);
				$('#successMsgProduct').html('Cập nhật thành công.').show();
				setTimeout(function() { $("#successMsgProduct").hide(); }, 3000);
			});
		}
		return false;
	},
	reloadNewDataForSubGrid: function(gridId, gridIndex,gridDetailId){
		var size = $('#' +gridId).datagrid('getRows').length;
		for(var m = 0;m<size;m++){
			$('#' +gridId).datagrid('collapseRow',m);
			$('#' +gridId).datagrid('expandRow',m);
		}
		EasyUiCommon.loadLocalDataForSubGrid(gridDetailId,gridIndex);		
	},
	loadLocalDataForSubGrid: function(gridDetailId,gridIndex){
		var newData = {};
		if(gridDetailId != undefined){
			if(gridDetailId.substring(0,3) == 'ddv'){
				newData = EasyUiCommon._mapNewDataSubGrid1.get(gridIndex);
			}else{
				newData = EasyUiCommon._mapNewDataSubGrid2.get(gridIndex);
			}
		}else{
			newData = EasyUiCommon._mapNewDataSubGrid1.get(gridIndex);
		}
		if(newData!= undefined && newData!= null && newData.length > 0){
			for(var i=0;i<newData.length;i++){
				var numRow = $('#' +gridDetailId).datagrid('getRows').length;
				$('#' +gridDetailId).datagrid('insertRow',{index: numRow-1, row:newData[i]});
			}
		}
	},
	clearTmpRowInSubGrid: function(datagridView){
		$('.datagrid-row-detail .datagrid-view' + datagridView +' .datagrid-body table tbody').each(function(){
			var arrSubChild = $(this).children();
			var rowIndex = -1;
			arrSubChild.each(function(){
			    if(!$(this).hasClass('datagrid-row') || $(this).attr('id').split('-')[3]!=datagridView || $(this).attr('datagrid-row-index')== rowIndex){
			        $(this).remove();
			    } else {
			        rowIndex = $(this).attr('datagrid-row-index');
			    }    
			});
		});		
		
	},
	updateRownumWidthForJqGridEX1:function(parentId,colReduceWidth){	
		var pId = '';
		if(parentId!= null && parentId!= undefined){
			pId = parentId + ' ';
		}
		var lastValue=$(pId + '.datagrid-cell-rownumber').last().text().trim().length;
		var widthSTT = $('.easyui-dialog .datagrid-cell-rownumber').width();
		var s = $('.easyui-dialog .datagrid-header-row td[field=' +colReduceWidth+'] div').width();
		if(EasyUiCommon._lstSize != null && EasyUiCommon._lstSize != undefined){ 
			s = EasyUiCommon._lstSize[0];
		}
		if(lastValue > 0){
			var extWidth = 25;
			if(lastValue > 2){
				extWidth += (lastValue - 2) * 9;
			}
			var value = extWidth - widthSTT;
			s = s - value;
			$(pId + '.datagrid-cell-rownumber').css('width',extWidth);
			$(pId + '.datagrid-header-rownumber').css('width',extWidth);
			$(parentId + ' .datagrid-row').each(function(){
				$(parentId + ' .datagrid-header-row td[field=' +colReduceWidth+'] div').width(s);
				$(parentId + ' .datagrid-row td[field=' +colReduceWidth+'] div').width(s);
			});
			
		}
	},
	loadAutoCompleteForProduct: function(filter){	//filter: desObj, pType, type	
		var choosePro = 0;
		var desObj = filter.desObj;
		var template = '';
		var gridDetailId = '';
		if(filter.gridId != null && filter.gridId != undefined){
			gridDetailId = filter.gridId;
		}
		if(filter.pType == EasyUiCommon.REQUIRE_PRODUCT){
			template = '<h3 style="float:left">${productCode}</h3><span style="float:right"><label style="font-size:10px;font-style:italic">BB mua:</label><input style="margin-top:7px" type="checkbox"/></span></br><p>${productName}</p>';
		} else if(filter.pType == EasyUiCommon.QUANTITY_PRODUCT){
			template = '<h3 style="float:left">${productCode}</h3><span style="float:right"><label style="font-size:10px;font-style:italic">SL mua:</label><input style="float:right !important;width:30px" class="InputTextStyle " type="text"/></span></br><p>${productName}</p>';
		} else if(filter.pType == EasyUiCommon.AMOUNT_PRODUCT){
			template = '<h3 style="float:left">${productCode}</h3><span style="float:right"><label style="font-size:10px;font-style:italic">TT mua:</label><input style="float:right !important;width:30px" class="InputTextStyle " type="text"/></span></br><p>${productName}</p>';
		} else if(filter.pType == EasyUiCommon.DETAIL_PRODUCT){
			template = '<h3 style="float:left">${productCode}</h3></br><p>${productName}</p>';
		}
		if(desObj != null && desObj != undefined){
			desObj.kendoAutoComplete({	 
				highlightFirst: true,
                filter: "startswith",
                placeholder: "Chọn sản phẩm...",
                separator: ", ",
                dataSource: {
                    type: "json",
                    serverFiltering: true,
                    serverPaging: true,
                    pageSize: 20,
                    transport: {
                        read: "/catalog/product/autocomplete?isExceptZCat=true"
                    }
                },
                dataTextField: "productCode",
                template: template,
                change: function(e){        
            		var value = this.element.val();
            		var arrVal = value.split(',');
            		var fVal = '';
            		if(filter.type == EasyUiCommon.AUTOCOMPLETE_MUL && filter.pType == EasyUiCommon.REQUIRE_PRODUCT){
            			if(arrVal!= null && arrVal.length  > 0){
            				for(var i=0;i<arrVal.length;i++){
            					if(arrVal[i] != undefined){
            						var fSubVal = arrVal[i].trim().replace(/\*/g,'');
            						if(pCodeRep!= null && fSubVal == pCode && fVal.indexOf(fSubVal) == -1){
            							fVal += pCodeRep + ', ';
            						} else if(arrVal[i].trim().length > 0 && fVal.indexOf(fSubVal) == -1) {
            							fVal += arrVal[i].trim() + ', ';
            						}
            					}
            				}
            				if(gridDetailId != ''){
            					EasyUiCommon.appendRowKendo(gridDetailId,filter.pType,filter.index);
            				}
            				this.element.val(fVal);
            			}
            		} else 
            			if(filter.type == EasyUiCommon.AUTOCOMPLETE_MUL && (filter.pType == EasyUiCommon.QUANTITY_PRODUCT || filter.pType == EasyUiCommon.AMOUNT_PRODUCT))
            		{
            			if(arrVal!= null && arrVal.length  > 0){
            				for(var i=0;i<arrVal.length;i++){
            					if(arrVal[i] != undefined){
            						var fSubVal = arrVal[i].replace(/\(|\)/g,' ').trim().split(' ')[0];                    			
            						if(pCodeRep!= null && fSubVal == pCode && fVal.indexOf(fSubVal) == -1){
            							fVal += pCodeRep + ', ';
            						} else if(arrVal[i].trim().length > 0 && fVal.indexOf(fSubVal) == -1) {
            							if(arrVal[i].trim().indexOf("(") > -1 && arrVal[i].trim().indexOf(")") > -1){
            								fVal += arrVal[i].trim() + ', ';
            							}
            						}
            					}
            				}
            				if(gridDetailId != ''){
            					EasyUiCommon.appendRowKendo(gridDetailId,filter.pType,filter.index);
            				}
        					this.element.val(fVal);
            			}
            		} 
            			else{
            				if(choosePro == 1){
            					this.element.val(pCode);
                    			this.element.parent().parent().parent().parent().parent().parent().parent().parent().children().first().next().children().children().children().children().children().children().val(pName);
                    			this.element.parent().parent().parent().parent().parent().parent().parent().parent().children().first().next().children().children().children().children().children().children().attr('disabled','disabled');
                    			$('#errMsgProduct').html('Mã sản phẩm không hợp lệ').hide();
            				}
            				else{
            					this.element.val('');
            					this.element.parent().parent().parent().parent().parent().parent().parent().parent().children().first().next().children().children().children().children().children().children().val('');
                    			this.element.parent().parent().parent().parent().parent().parent().parent().parent().children().first().next().children().children().children().children().children().children().attr('enabled','enabled');
            					var html = $('#errMsgProduct').html();
            					if(html!=undefined && html!=null)
            						$('#errMsgProduct').html('Mã sản phẩm không hợp lệ').show();                   			
            				}           				
            		}
                },
                dataBound: function(e) {
                	$(window).bind('keyup', function(evt){
                		if(evt.keyCode == 17){
                			if(filter.type == EasyUiCommon.AUTOCOMPLETE_MUL && filter.pType == EasyUiCommon.REQUIRE_PRODUCT){
                				var chkItem =  $('.k-state-focused input[type=checkbox]');
                    			if(chkItem.is(':checked')){
                    				chkItem.removeAttr('checked');
                    			} else {
                    				chkItem.attr('checked','checked');
                    			}
                			} else if(filter.type == EasyUiCommon.AUTOCOMPLETE_MUL && (filter.pType == EasyUiCommon.QUANTITY_PRODUCT || filter.pType == EasyUiCommon.AMOUNT_PRODUCT)){
                				setTimeout(function(){
                					$('.k-state-focused input').focus();
                				},500);
                			}               			
                		}
                	
                	});  
                },
                close: function(e){
                	$(window).unbind('keyup');	                            	
                },	                             
                select: function(e){
                	choosePro = 1;
                	pCode = $('.k-state-focused h3').html();
                	pName = $('.k-state-focused p').html();
                		pCodeRep = null;
                    	if(filter.type == EasyUiCommon.AUTOCOMPLETE_MUL && filter.pType == EasyUiCommon.REQUIRE_PRODUCT){
                    		if($('.k-state-focused input[type=checkbox]').is(':checked')){
                    			EasyUiCommon.pQuantity = 1;
                        		pCodeRep = pCode + '*';
                        	}else{
                        		EasyUiCommon.pQuantity = 0;
                        	}
                    	} else if(filter.type == EasyUiCommon.AUTOCOMPLETE_MUL && (filter.pType == EasyUiCommon.QUANTITY_PRODUCT || filter.pType == EasyUiCommon.AMOUNT_PRODUCT)){
                    		pCode = e.item.children().first().html();
                    		//var numProduct = $('.k-state-focused input').val().trim();
                    		var numProduct = $(e.item.children()[1]).children().last().val().trim();
                    		if(numProduct.length == 0){
                    			numProduct = 0;
                    		}
                    		EasyUiCommon.pQuantity = numProduct;
                    		pCodeRep = Utils.XSSEncode(pCode) + '(' + numProduct + ')';
                    	} else{
                    		pCode = Utils.XSSEncode(e.item.children().first().text());
                        	pName = Utils.XSSEncode(e.item.children().last().text());
                    	}
                	
                }                
            });			
			setTimeout(function(){
				desObj.css('width',desObj.parent().width()-6);
				desObj.parent().css('padding','0px !important');	
			},500);
		}
	},
	appendRowKendo:function(gridDetailId,type,index){
		var row = {};
		var i = $('#' +gridDetailId).datagrid('getRows').length-1; 
		$('#' +gridDetailId).datagrid('deleteRow', i); 
		if(type == EasyUiCommon.REQUIRE_PRODUCT){
			row = {productCode:pCode,
					productName:pName,
					required:EasyUiCommon.pQuantity};    			
		}else if(type == EasyUiCommon.QUANTITY_PRODUCT){
			row = {productCode:pCode,
					productName:pName,
					quantity:EasyUiCommon.pQuantity}; 
		}else if(type == EasyUiCommon.AMOUNT_PRODUCT){
			row = {productCode:pCode,
					productName:pName,
					amount:EasyUiCommon.pQuantity}; 
		}
		if(pCode != '' && pCode != null && pCode != undefined){
			var size = 0;
			var productCode = '';
			var isExist = false;
			if(gridDetailId.substring(0,3) == 'ddv'){
				if(EasyUiCommon._mapNewDataSubGrid1.size() > 0){
					size = EasyUiCommon._mapNewDataSubGrid1.get(index).length;
					for(var count = 0;count< size;count++){
						productCode = EasyUiCommon._mapNewDataSubGrid1.get(index)[count].productCode;
						if(pCode == productCode){
							isExist = true;
						}
					}
				}
			}else{
				if(EasyUiCommon._mapNewDataSubGrid2.size() > 0){
					size = EasyUiCommon._mapNewDataSubGrid2.get(index).length;
					for(var count = 0;count< size;count++){
						productCode = EasyUiCommon._mapNewDataSubGrid2.get(index)[count].productCode;
						if(pCode == productCode){
							isExist = true;
						}
					}
				}
			}
			if(!isExist){
				$('#' +gridDetailId).datagrid('appendRow',row);
				if(gridDetailId.substring(0,3) == 'ddv'){
					var arrValues = EasyUiCommon._mapNewDataSubGrid1.get(index);
					if(arrValues == undefined || arrValues == null || arrValues.length == 0){
						arrValues = new Array();
					}
					arrValues.push(row);
					EasyUiCommon._mapNewDataSubGrid1.put(index,arrValues);
				}else{
					var arrValues = EasyUiCommon._mapNewDataSubGrid2.get(index);
					if(arrValues == undefined || arrValues == null || arrValues.length == 0){
						arrValues = new Array();
					}
					arrValues.push(row);
					EasyUiCommon._mapNewDataSubGrid2.put(index,arrValues);
				}
			}
		}
		$('#' +gridDetailId).datagrid('appendRow',{});
		setTimeout(function(){
			$('#' +gridDetailId).datagrid('resize');
			$('#grid').datagrid('resize');
		},500);
	},
	dataGrid01020405 : function(gridId,url,field1,field2,field3,field4,title1,title2,title3,title4,urlComboGrid,type,permission){
		var _field1 = 'productCode';
		var _field2 = 'productName';
		var _title1 = 'Mã SP';
		var _title2 = 'Tên SP';
		if(field1 != null && field1 != undefined){
			_field1 = field1;
		}
		if(field2 != null && field2 != undefined){
			_field2 = field2;
		}
		if(title1 != null && title1 != undefined){
			_title1 = title1;
		}
		if(title2 != null && title2 != undefined){
			_title2 = title2;
		}
		var addRow = '';
		if(permission == 'true'){
			addRow = "<a href='javascript:void(0)' onclick=\"return EasyUiCommon.appendRowOnGrid('grid')\"><img title='Thêm mới' src='/resources/images/icon-add.png'/></a>"; 
		}
		if(field3 != null && field3 != undefined && field4 != null && field4 != undefined ){
			$('#' +gridId).datagrid({
				url : url,
				pageList  : [10,20,30],
				width:$('.GridSection').width() -20,
				height:'auto',
				scrollbarSize : 18,
				rownumbers:true,
				columns:[[  
			          {field: _field1,title: _title1, width:200,align:'left',sortable : false,resizable : false,
			        	  editor:{
				        	  type:'text'
			          	  },
			          	formatter:function(value){
				    		return Utils.XSSEncode(value);	
				    	}
			          },
			          {field: _field2,title: _title2, width:300,align:'left',sortable : false,resizable : false,
			        	  editor:{
				        	  type:'text'
			          	  },
			          	  styler: function(value,row,index){
 							  return 'font-size:14;font-weight:bold';
			          	  },
			          	  formatter:function(value){
				    		return Utils.XSSEncode(value);	
			          	  }
			          },
			          {field: field3,title: title3, width:150,align:'right',sortable : false,resizable : false,
			        	  editor:{ 
								type:'numberbox',
					    		options:{
					    			groupSeparator: ',',
					    			max:'5'
					    		}
				    		}, 
				    		formatter:function(value){
					    		return formatCurrency(value);	
					    	}
			          },
			          {field: field4,title: title4, width:150,align:'right',sortable : false,resizable : false,
			        	  editor:{ 
								type:'numberbox',
					    		options:{
					    			groupSeparator: ',',
					    			max:'5'
					    		}
				    		}, 
				    		formatter:function(value){
					    		return formatCurrency(value);	
					    	}
			          },
			          {field: 'remove',title:addRow, width:50,align:'center',sortable : false,resizable : false,
			        	  formatter: function(value,row,index){
			        		  if(permission == 'true'){
			        			  return "<a href='javascript:void(0)' onclick=\"return EasyUiCommon.removeRowOnGrid('" + index + "',false)\"><img title='Xóa' src='/resources/images/icon-delete.png'/></a>";
			        		  }else{
			        			  return '';
			        		  }
			        	  }
			          },
			          ]],
			      onSelect:function(index,row){
			    	  if(permission == 'true'){
		    			  if (EasyUiCommon.endEditing(gridId)){
		    				  $('#' +gridId).datagrid('beginEdit', index);  
		    				  EasyUiCommon.editIndex = index;
		    				  $('#' +gridId).datagrid('resize');
		    			  }
			        	  var ed = $('#' +gridId).datagrid('getEditor', {index:EasyUiCommon.editIndex,field: _field1});
			        	  var ed2 = $('#' +gridId).datagrid('getEditor', {index:EasyUiCommon.editIndex,field: _field2});
			        	  var ed3 = $('#' +gridId).datagrid('getEditor', {index:EasyUiCommon.editIndex,field: field3});
			        	  var ed4 = $('#' +gridId).datagrid('getEditor', {index:EasyUiCommon.editIndex,field: field4});
			        	  if(ed != null && ed != undefined){
		        			  $(ed2.target).attr('disabled','disabled');
		        			  var filter = {};
		        			  filter.desObj = $(ed.target);
		        			  filter.type = EasyUiCommon.AUTOCOMPLETE_MUL;
		        			  filter.pType = EasyUiCommon.DETAIL_PRODUCT;
		        			  EasyUiCommon.loadAutoCompleteForProduct(filter);	
			        	  }
			        	  if(ed3 != null && ed3 != undefined){
			        		  $(ed3.target).bind('keyup',function(event){
			        			  if(event.keyCode == keyCodes.ENTER && $(ed.target).val() != ""){	
			        				  EasyUiCommon.appendRowOnGrid(gridId);
			        			  }
			        		  });
			        	  }
			        	  if(ed4 != null && ed4 != undefined){
			        		  $(ed4.target).bind('keyup',function(event){
			        			  if(event.keyCode == keyCodes.ENTER && $(ed.target).val() != ""){		
			        				  EasyUiCommon.appendRowOnGrid(gridId);
			        			  }
			        		  });
			        	  }
			        	  $('.datagrid-view td[field="quantity"] input').attr('maxlength', '9');
				    	  $('.datagrid-view td[field="amount"] input').attr('maxlength','17');
				    	  $('.datagrid-view td[field="discountAmount"] input').attr('maxlength','17');
				    	  $('.datagrid-view td[field="discountPercent"] input').attr('maxlength', '9');
		        	  }
			      },
		          method : 'GET',
		          onLoadSuccess:function(){
		        	  $('.datagrid-header-rownumber').html(jsp_common_numerical_order);
		        	  if(permission == 'true'){
		        		  var size = $('#' +gridId).datagrid('getRows').length; 
		        		  EasyUiCommon._totalRowGrid = size;
		        		  EasyUiCommon.appendRowOnGrid(gridId);
		        	  }
		        	  $('#' +gridId).datagrid('resize');
		          }
			});
		}
	},
	dataGrid0306 : function(gridId,url,field1,field2,field3,field4,title1,title2,title3,title4,urlComboGrid,type,fieldDetail,titleDetail,permission){
		var _field1 = 'productCode';
		var _field2 = 'productName';
		var _title1 = 'Mã SP';
		var _title2 = 'Tên SP';
		if(field1 != null && field1 != undefined){
			_field1 = field1;
		}
		if(field2 != null && field2 != undefined){
			_field2 = field2;
		}
		if(title1 != null && title1 != undefined){
			_title1 = title1;
		}
		if(title2 != null && title2 != undefined){
			_title2 = title2;
		}
		var addRow = '';
		if(permission == 'true'){
			addRow = "<a href='javascript:void(0)' onclick=\"return EasyUiCommon.appendRowOnGrid('grid')\"><img title='Thêm mới' src='/resources/images/icon-add.png'/></a>";
		}
		if(field3 != null && field3 != undefined && field4 != null && field4 != undefined ){
			$('#' +gridId).datagrid({
				url : url,
				total:100,
				width:$('.GridSection').width() -20,
				scrollbarSize : 0,
				rownumbers:true,
				columns:[[  
				{field: _field1,title: _title1, width:200,align:'left',sortable : false,resizable : false,
					  editor:{
						  type:'text'
					  },
					  formatter:function(value){
				    		return Utils.XSSEncode(value);	
			          }
				},
				{field: _field2,title: _title2, width:200,align:'left',sortable : false,resizable : false,
					  editor:{
			        	  type:'text',
		          	  },
		          	  styler: function(value,row,index){
						  return 'font-size:14;font-weight:bold';
		          	  },
		          	  formatter:function(value){
		          		  	return Utils.XSSEncode(value);	
		          	  }
				},
				{field: field3,title: title3, width:100,align:'right',sortable : false,resizable : false,
					editor:{ 
						type:'numberbox',
			    		options:{
			    			groupSeparator: ','
			    		}
		    		}, 
		    		formatter:function(value){
			    		return formatCurrency(value);	
			    	}
				},
				{field: field4,title: title4, width:300,align:'left',sortable : false,resizable : false,
					formatter: function(value,row,index){
						return EasyUiCommon.stylerOnRow(index,value,type);
					}
				},
				{field: 'remove',title:addRow, width:50,align:'center',sortable : false,resizable : false,
					formatter: function(value,row,index){
	        		  if(permission == 'true'){
	        			  return "<a href='javascript:void(0)' onclick=\"return EasyUiCommon.removeRowOnGrid('" + index + "',false)\"><img title='Xóa' src='/resources/images/icon-delete.png'/></a>";
	        		  }else{
	        			  return '';
	        		  }
	        	  }
				}
	            ]],
	            onSelect:function(index,row){
	            	$('#divDialogSearch').hide();
	            	if(permission == 'true'){
	        		  if (EasyUiCommon.endEditing(gridId)){  
	        			  $('#' +gridId).datagrid('beginEdit', index);  
	        			  EasyUiCommon.editIndex = index;
	        			  $('#' +gridId).datagrid('resize');
	        		  } 
	        		  EasyUiCommon.formatLabelAfterChange(type);
		        	  var ed = $('#' +gridId).datagrid('getEditor', {index:EasyUiCommon.editIndex,field: _field1});
		        	  var ed2 = $('#' +gridId).datagrid('getEditor', {index:EasyUiCommon.editIndex,field: _field2});
		        	  if(ed != null && ed != undefined){
		        		  $(ed2.target).attr('disabled','disabled');
	        			  var filter = {};
	        			  filter.desObj = $(ed.target);
	        			  filter.type = EasyUiCommon.AUTOCOMPLETE_MUL;
	        			  filter.pType = EasyUiCommon.DETAIL_PRODUCT;
	        			  EasyUiCommon.loadAutoCompleteForProduct(filter);
		        	  }
		        	  var ed3 = $('#' +gridId).datagrid('getEditor', {index:EasyUiCommon.editIndex,field: field3});
		        	  var ed4 = $('#' +gridId).datagrid('getEditor', {index:EasyUiCommon.editIndex,field: field4});
		        	  if(ed3 != null && ed3 != undefined){
		        		  $(ed3.target).bind('keyup',function(event){
		        			  if(event.keyCode == keyCodes.ENTER){		   				
		        				  EasyUiCommon.appendRowOnGrid(gridId);
		        			  }
		        		  });
		        	  }
		        	  if(ed4 != null && ed4 != undefined){
		        		  $(ed4.target).bind('focus',function(event){
		        			  EasyUiCommon.showGridDialog(index,'saleQty','Số lượng','numberbox',false);
		        		  });
		        	  }
		        	  $('.datagrid-view td[field="quantity"] input').attr('maxlength', '9');
		              $('.datagrid-view td[field="amount"] input').attr('maxlength','17');
		              $('.datagrid-view td[field="discountAmount"] input').attr('maxlength','17');
		              $('.datagrid-view td[field="discountPercent"] input').attr('maxlength', '9');
	            	}
	            },
				method : 'GET',
	            onLoadSuccess:function(){
	            	$('.datagrid-header-rownumber').html(jsp_common_numerical_order);
	            	if(permission == 'true'){
	            		var size = $('#' +gridId).datagrid('getRows').length; 
	            		EasyUiCommon._totalRowGrid = size;
	            		EasyUiCommon.loadDataOnGridSuccess(gridId,type);
	            	} else {
	            		EasyUiCommon.loadDataOnGridSuccess(gridId,type);
	            	}
	            	$('#' +gridId).datagrid('resize');
	            }
			});
		}
	},
	dataGrid07081011 : function(gridId,url,field2,field3,title2,title3,urlComboGrid,type,permission){
		var _field1 = 'productCodeStr';
		var _title1 = 'Sản phẩm mua';
		var addRow = '';
		if(permission == 'true'){
			addRow = "<a href='javascript:void(0)' onclick=\"return EasyUiCommon.appendRowOnGrid('grid')\"><img title='Thêm mới' src='/resources/images/icon-add.png'/></a>";
		}
		if(field2 != null && field2 != undefined && field3 != null && field3 != undefined ){
			$('#' +gridId).datagrid({
				url : url,
				total:100,
				width: $('.GridSection').width() -20,
				scrollbarSize : 0,
				rownumbers:true,
				columns:[[  
				{field: _field1,title: _title1, width:200,align:'left',sortable : false,resizable : false,
					  formatter: function(value,row,index){
						  return EasyUiCommon.stylerOnRow(index,value,type);
					  }
				},
				{field: field2,title: title2, width:200,align:'left',sortable : false,resizable : false,
					editor:{ 
						type:'numberbox',
			    		options:{
			    			groupSeparator: ','
			    		}
		    		}, 
		    		formatter:function(value){
			    		return formatCurrency(value);	
			    	}
				},
				{field: field3,title: title3, width:100,align:'right',sortable : false,resizable : false,
					editor:{ 
						type:'numberbox',
			    		options:{
			    			groupSeparator: ','
			    		}
		    		}, 
		    		formatter:function(value){
			    		return formatCurrency(value);	
			    	}
				},
				{field: 'remove',title:addRow, width:50,align:'center',sortable : false,resizable : false,
					formatter: function(value,row,index){
	        		  if(permission == 'true'){
	        			  return "<a href='javascript:void(0)' onclick=\"return EasyUiCommon.removeRowOnGrid('" + index + "',false)\"><img title='Xóa' src='/resources/images/icon-delete.png'/></a>";
	        		  }else{
	        			  return '';
	        		  }
	        	  }
				},
	            ]],
	            onSelect:function(index,row){
	            	$('#divDialogSearch').hide();
	            	if(permission == 'true'){
		        		  if (EasyUiCommon.endEditing(gridId)){  
		        			  $('#' +gridId).datagrid('beginEdit', index);  
		        			  EasyUiCommon.editIndex = index;  
		        			  $('#' +gridId).datagrid('resize');
		        		  } 
		        		  EasyUiCommon.formatLabelAfterChange(type);
			        	  var ed = $('#' +gridId).datagrid('getEditor', {index:EasyUiCommon.editIndex,field: _field1});
			        	  if(ed != null && ed != undefined){
			        		  $(ed.target).bind('focus',function(event){
			        			  EasyUiCommon.showGridDialog(index,'required','BB mua','checkbox',false);
			        		  });
			        	  }
			        	  var ed2 = $('#' +gridId).datagrid('getEditor', {index:EasyUiCommon.editIndex,field: field2});
			        	  var ed3 = $('#' +gridId).datagrid('getEditor', {index:EasyUiCommon.editIndex,field: field3});
			        	  if(ed2 != null && ed2 != undefined){
			        		  $(ed2.target).bind('keyup',function(event){
			        			  if(event.keyCode == keyCodes.ENTER){		   				
			        				  EasyUiCommon.appendRowOnGrid(gridId);
			        			  }
			        		  });
			        	  }
			        	  if(ed3 != null && ed3 != undefined){
			        		  $(ed3.target).bind('keyup',function(event){
			        			  if(event.keyCode == keyCodes.ENTER){		   				
			        				  EasyUiCommon.appendRowOnGrid(gridId);
			        			  }
			        		  });
			        	  }
			        	  $('.datagrid-view td[field="quantity"] input').attr('maxlength', '9');
			        	  $('.datagrid-view td[field="amount"] input').attr('maxlength','17');
			        	  $('.datagrid-view td[field="discountAmount"] input').attr('maxlength','17');
			        	  $('.datagrid-view td[field="discountPercent"] input').attr('maxlength', '9');
	            	}
	            },
				method : 'GET',
	            onLoadSuccess:function(){
	            	$('.datagrid-header-rownumber').html(jsp_common_numerical_order);
	            	if(permission == 'true'){
	            		var size = $('#' +gridId).datagrid('getRows').length; 
	            		EasyUiCommon._totalRowGrid = size;
	            		EasyUiCommon.loadDataOnGridSuccess(gridId,type);
	            	} else {
	            		EasyUiCommon.loadDataOnGridSuccess(gridId,type);
	            	}
	            	$('#' +gridId).datagrid('resize');
	            }
			});
		}
	},
	dataGrid0912 : function(gridId,url,field,title,urlComboGrid,type,permission){
		var addRow = '';
		if(permission == 'true'){
			addRow = "<a href='javascript:void(0)' onclick=\"return EasyUiCommon.appendRowOnGrid('grid')\"><img title='Thêm mới' src='/resources/images/icon-add.png'/></a>";
		}
		if(field != null && field != undefined){
			$('#' +gridId).datagrid({
				url : url,
				total:100,
				width:$('.GridSection').width() -20,
				fitColumns:true,
				rownumbers:true,
				scrollbarSize : 0,
				columns:[[  
				{field: 'productCodeStr',title: 'Sản phẩm mua', width:200,align:'left',sortable : false,resizable : false,
					  formatter: function(value,row,index){
						  return EasyUiCommon.stylerOnRow(index,value,type);
					  }
				},
				{field: field,title: title, width:100,align:'right',sortable : false,resizable : false,
					editor:{ 
						type:'numberbox',
			    		options:{
			    			groupSeparator: ','
			    		}
		    		}, 
		    		formatter:function(value){
			    		return formatCurrency(value);	
			    	}
				},
				{field: 'freeProductCodeStr',title: 'Sản phẩm khuyến mãi', width:200,align:'left',sortable : false,resizable : false,
					  formatter: function(value,row,index){
						  return EasyUiCommon.stylerOnRow(index,value,type,true);
					  }
				},
				{field: 'remove',title:addRow, width:50,align:'center',sortable : false,resizable : false,
					formatter: function(value,row,index){
		        		  if(permission == 'true'){
		        			  return "<a href='javascript:void(0)' onclick=\"return EasyUiCommon.removeRowOnGrid('" +index+"',true)\"><img title ='Xóa' src='/resources/images/icon-delete.png'/></a>";
		        		  }else{
		        			  return '';
		        		  }
		        	  }
				},
	            ]],
	            onSelect : function (index,row){
	            	$('#divDialogSearch').hide();
	            	if(permission == 'true'){
	        		    if (EasyUiCommon.endEditing(gridId)){  
	            			$('#' +gridId).datagrid('beginEdit', index);  
	            			EasyUiCommon.editIndex = index; 
	            			$('#' +gridId).datagrid('resize');
	            		}  
	        		    EasyUiCommon.formatLabelAfterChange(type);
	            		var ed2 = $('#' +gridId).datagrid('getEditor', {index:EasyUiCommon.editIndex,field: field});
	            		if(ed2 != null && ed2 != undefined){
	            			$(ed2.target).bind('keyup',function(event){
		        			    if(event.keyCode == keyCodes.ENTER){		   				
		        			    	EasyUiCommon.appendRowOnGrid(gridId);
		        			    }
		        		    });
	            		}
	            		$('.datagrid-view td[field="quantity"] input').attr('maxlength', '9');
				    	$('.datagrid-view td[field="amount"] input').attr('maxlength','17');
				    	$('.datagrid-view td[field="discountAmount"] input').attr('maxlength','17');
				    	$('.datagrid-view td[field="discountPercent"] input').attr('maxlength', '9');
	            	}
	            } ,
				method : 'GET',
	            onLoadSuccess:function(){
	            	$('.datagrid-header-rownumber').html(jsp_common_numerical_order);
	            	if(permission == 'true'){
	            		var size = $('#' +gridId).datagrid('getRows').length; 
	            		EasyUiCommon._totalRowGrid = size;
	            		EasyUiCommon.loadDataOnGridSuccess(gridId,type);
	            	} else {
	            		EasyUiCommon.loadDataOnGridSuccess(gridId,type);
	            	}
            		$('#' +gridId).datagrid('resize');
            		$('.datagrid-body td[field="productCodeStr"] div').css('position','relative');
            		$('.datagrid-body td[field="freeProductCodeStr"] div').css('position','relative');
	            },
	            onAfterEdit: function(){
	            	$('.datagrid-body td[field="productCodeStr"] div').css('position','relative');
            		$('.datagrid-body td[field="freeProductCodeStr"] div').css('position','relative');
	            },
	            onCancelEdit: function(){
	            	$('.datagrid-body td[field="productCodeStr"] div').css('position','relative');
            		$('.datagrid-body td[field="freeProductCodeStr"] div').css('position','relative');
	            },
	            onBeforeEdit: function(){
	            	$('.datagrid-body td[field="productCodeStr"] div').css('position','relative');
            		$('.datagrid-body td[field="freeProductCodeStr"] div').css('position','relative');
	            }
			});
		}
	},
	dataGrid1518 : function(gridId,url,urlComboGrid,type,permission){
		var addRow = '';
		if(permission == 'true'){
			addRow = "<a href='javascript:void(0)' onclick=\"return EasyUiCommon.appendRowOnGrid('grid')\"><img title='Thêm mới' src='/resources/images/icon-add.png'/></a>";
		}
		$('#' +gridId).datagrid({
			url : url,
			total:100,
			width: $('.GridSection').width() -20,
			scrollbarSize : 0,
			rownumbers:true,
			columns:[[  
			{field: 'productCodeStr',title: 'Sản phẩm mua', width:200,align:'left',sortable : false,resizable : false,
				  formatter: function(value,row,index){
					  return EasyUiCommon.stylerOnRow(index,value,type);
				  }
			},
			{field: 'freeProductCodeStr',title: 'Sản phẩm khuyến mãi', width:200,align:'left',sortable : false,resizable : false,
				  formatter: function(value,row,index){
					  return EasyUiCommon.stylerOnRow(index,value,type,true);
				  }
			},
			{field: 'hiddenField', hidden:true,
				editor:{
					type:'text'
				}
			},
			{field: 'remove',title:addRow, width:50,align:'center',sortable : false,resizable : false,
				formatter: function(value,row,index){
	        		  if(permission == 'true'){
	        			  return "<a href='javascript:void(0)' onclick=\"return EasyUiCommon.removeRowOnGrid('" + index + "',false)\"><img title='Xóa' src='/resources/images/icon-delete.png'/></a>";
	        		  }else{
	        			  return '';
	        		  }
	        	  }
			},
            ]],
            onSelect : function (index,row){ 
            	$('#divDialogSearch').hide();
            	if(permission == 'true'){
        		    if (EasyUiCommon.endEditing(gridId)){  
            			$('#' +gridId).datagrid('beginEdit', index);  
            			EasyUiCommon.editIndex = index; 
            			$('#' +gridId).datagrid('resize');
            		}  
        		    EasyUiCommon.formatLabelAfterChange(type);
            		var ed = $('#' +gridId).datagrid('getEditor', {index:EasyUiCommon.editIndex,field: 'productCodeStr'});
            		if(ed != null && ed != undefined){
            			if(type != null && type == 'ZV15'){
            				$(ed.target).bind('focus',function(event){
            					EasyUiCommon.showGridDialog(index,'saleQty','Số lượng','numberbox',false);
	            			});
            			}else{
            				$(ed.target).bind('focus',function(event){
            					EasyUiCommon.showGridDialog(index,'amount','TT mua','numberbox',false);
            				});
            			}
            		}
            		var ed2 = $('#' +gridId).datagrid('getEditor', {index:EasyUiCommon.editIndex,field: 'freeProductCodeStr'});
            		if(ed2 != null && ed2 != undefined){
            			$(ed2.target).bind('focus',function(event){
            				EasyUiCommon.showGridDialog(index,'saleQty','Số lượng','numberbox',true);
            			});
            		}
            		$('.datagrid-view td[field="quantity"] input').attr('maxlength', '9');
			    	$('.datagrid-view td[field="amount"] input').attr('maxlength','17');
			    	$('.datagrid-view td[field="discountAmount"] input').attr('maxlength','17');
			    	$('.datagrid-view td[field="discountPercent"] input').attr('maxlength', '9');
            	}
            } ,
			method : 'GET',
            onLoadSuccess:function(){
            	$('.datagrid-header-rownumber').html(jsp_common_numerical_order);
            	if(permission == 'true'){
            		var size = $('#' +gridId).datagrid('getRows').length; 
            		EasyUiCommon._totalRowGrid = size;
            		EasyUiCommon.loadDataOnGridSuccess(gridId,type);
            	} else {
            		EasyUiCommon.loadDataOnGridSuccess(gridId,type);
            	}
        		$('#' +gridId).datagrid('resize');
            }
		});
	},
	dataGrid13141617 : function(gridId,url,field2,title2,urlComboGrid,type,fieldDetail,titleDetail,permission){
		var addRow = '';
		if(permission == 'true'){
			addRow = "<a href='javascript:void(0)' onclick=\"return EasyUiCommon.appendRowOnGrid('grid')\"><img title='Thêm mới' src='/resources/images/icon-add.png'/></a>";
		}
		var _field1 = 'productCodeStr';
		var _title1 = 'Sản phẩm mua';
		if(field2 != null && field2 != undefined){
			$('#' +gridId).datagrid({
				url : url,
				total:100,
				width:$('.GridSection').width() -20,
				scrollbarSize : 18,
				rownumbers:true,
				columns:[[  
				{field: _field1,title: _title1, width:200,align:'left',sortable : false,resizable : false,
					  formatter: function(value,row,index){
						  return EasyUiCommon.stylerOnRow(index,value,type);
					  }
				},
				{field: field2,title: title2, width:200,align:'left',sortable : false,resizable : false,
					editor:{ 
						type:'numberbox',
			    		options:{
			    			groupSeparator: ','
			    		}
		    		}, 
		    		formatter:function(value){
			    		return formatCurrency(value);	
			    	}
				},
				{field: 'remove',title:addRow, width:50,align:'center',sortable : false,resizable : false,
					formatter: function(value,row,index){
	        		  if(permission == 'true'){
	        			  return "<a href='javascript:void(0)' onclick=\"return EasyUiCommon.removeRowOnGrid('" + index + "',false)\"><img title='Xóa' src='/resources/images/icon-delete.png'/></a>";
	        		  }else{
	        			  return '';
	        		  }
	        	  }
				},
	            ]],
	            onSelect:function(index,row){
	            	$('#divDialogSearch').hide();
	            	if(permission == 'true'){
		        		  if (EasyUiCommon.endEditing(gridId)){  
		        			  $('#' +gridId).datagrid('beginEdit', index);  
		        			  EasyUiCommon.editIndex = index; 
		        			  $('#' +gridId).datagrid('resize');
		        		  } 
		        		  EasyUiCommon.formatLabelAfterChange(type);
			        	  var ed = $('#' +gridId).datagrid('getEditor', {index:EasyUiCommon.editIndex,field: _field1});
			        	  if(ed != null && ed != undefined){
			        		  EasyUiCommon.eventWhenPressTAB('numberItem');
			        		  if(type == 'ZV13' || type == 'ZV14'){
			        			  $(ed.target).bind('focus',function(event){
			        				  EasyUiCommon.showGridDialog(index,'saleQty','Số lượng','numberbox',false);
			        			  });
			        		  }else if(type == 'ZV16' || type == 'ZV17'){
			        			  $(ed.target).bind('focus',function(event){
			        				  EasyUiCommon.showGridDialog(index,'amount','TT mua','numberbox',false);
			        			  });
			        		  }
			        	  }
			        	  var ed2 = $('#' +gridId).datagrid('getEditor', {index:EasyUiCommon.editIndex,field: field2});
			        	  if(ed2 != null && ed2 != undefined){
			        		  $(ed2.target).bind('keyup',function(event){
			        			  if(event.keyCode == keyCodes.ENTER){		   				
			        				  EasyUiCommon.appendRowOnGrid(gridId);
			        			  }
			        		  });
			        	  }
			        	  $('.datagrid-view td[field="quantity"] input').attr('maxlength', '9');
				    	  $('.datagrid-view td[field="amount"] input').attr('maxlength','17');
				    	  $('.datagrid-view td[field="discountAmount"] input').attr('maxlength','17');
				    	  $('.datagrid-view td[field="discountPercent"] input').attr('maxlength', '9');
	            	}
	            },
				method : 'GET',
	            onLoadSuccess:function(){
	            	$('.datagrid-header-rownumber').html(jsp_common_numerical_order);
	            	if(permission == 'true'){
	            		var size = $('#' +gridId).datagrid('getRows').length; 
	            		EasyUiCommon._totalRowGrid = size;
	            		EasyUiCommon.loadDataOnGridSuccess(gridId,type);
	            	} else {
	            		EasyUiCommon.loadDataOnGridSuccess(gridId,type);
	            	}
	        		$('#' +gridId).datagrid('resize');
	            }
			});
		}
	},
	dataGrid1920 : function(gridId,url,field1,field2,title1,title2,urlComboGrid,type,permission){
		var addRow = '';
		if(permission == 'true'){
			addRow = "<a href='javascript:void(0)' onclick=\"return EasyUiCommon.appendRowOnGrid('grid')\"><img title='Thêm mới' src='/resources/images/icon-add.png'/></a>";
		}
		if(field1 != null && field1 != undefined && field2 != null && field2 != undefined){
			$('#' +gridId).datagrid({
				url : url,
				total:100,
				width:$('.GridSection').width() -20,
				//height:'auto',
				rownumbers:true,
				scrollbarSize : 18,
				columns:[[  
				{field: field1,title: title1, width:200,align:'left',sortable : false,resizable : false,
					editor:{ 
						type:'numberbox',
			    		options:{
			    			groupSeparator: ',',
			    		}
		    		}, 
		    		formatter:function(value){
			    		return formatCurrency(value);	
			    	}
				},
				{field: field2,title: title2, width:200,align:'left',sortable : false,resizable : false,
					editor:{ 
						type:'numberbox',
			    		options:{
			    			groupSeparator: ','
			    		}
		    		}, 
		    		formatter:function(value){
			    		return formatCurrency(value);	
			    	}
				},
				{field: 'remove',title:addRow, width:50,align:'center',sortable : false,resizable : false,
					formatter: function(value,row,index){
	        		  if(permission == 'true'){
	        			  return "<a href='javascript:void(0)' onclick=\"return EasyUiCommon.removeRowOnGrid('" + index + "',false)\"><img title='Xóa' src='/resources/images/icon-delete.png'/></a>";
	        		  }else{
	        			  return '';
	        		  }
	        	  }},
	            ]],
	            onSelect : function (index,row){
	            	if(permission == 'true'){
		        		  if (EasyUiCommon.endEditing(gridId)){  
		        			  $('#' +gridId).datagrid('beginEdit', index);  
		        			  EasyUiCommon.editIndex = index;  
		        			  $('#' +gridId).datagrid('resize');
		        		  } 
			        	  var ed = $('#' +gridId).datagrid('getEditor', {index:EasyUiCommon.editIndex,field: field1});
			        	  if(ed != null && ed != undefined){
			        		  $(ed.target).bind('keyup',function(event){
			        			  if(event.keyCode == keyCodes.ENTER){		   				
			        				  EasyUiCommon.appendRowOnGrid(gridId);
			        			  }
			        		  });
			        	  }
			        	  var ed2 = $('#' +gridId).datagrid('getEditor', {index:EasyUiCommon.editIndex,field: field2});
			        	  if(ed2 != null && ed2 != undefined){
			        		  $(ed2.target).bind('keyup',function(event){
			        			  if(event.keyCode == keyCodes.ENTER){		   				
			        				  EasyUiCommon.appendRowOnGrid(gridId);
			        			  }
			        		  });
			        	  }
			        	  $('.datagrid-view td[field="quantity"] input').attr('maxlength', '9');
				    	  $('.datagrid-view td[field="amount"] input').attr('maxlength','17');
				    	  $('.datagrid-view td[field="discountAmount"] input').attr('maxlength','17');
				    	  $('.datagrid-view td[field="discountPercent"] input').attr('maxlength', '9');
	            	}
	            },
				method : 'GET',
	            onLoadSuccess:function(){
	            	$('.datagrid-header-rownumber').html(jsp_common_numerical_order);
	            	if(permission == 'true'){
	            		$('#' +gridId).datagrid('appendRow',{});
	            	}
	            	$('#' +gridId).datagrid('resize');
	            },
			});
		}
	},
	dataGrid21 : function(gridId,url,field1,field2,title1,title2,urlComboGrid,type,fieldDetail,titleDetail,permission){
		var addRow = '';
		if(permission == 'true'){
			addRow = "<a href='javascript:void(0)' onclick=\"return EasyUiCommon.appendRowOnGrid('grid')\"><img title='Thêm mới' src='/resources/images/icon-add.png'/></a>";
		}
		if(field1 != null && field1 != undefined && field2 != null && field2 != undefined){
			$('#' +gridId).datagrid({
				url : url,
				total:100,
				width:$('.GridSection').width() -20,
				scrollbarSize : 18,
				rownumbers:true,
				columns:[[  
				{field: field1,title: title1, width:200,align:'left',sortable : false,resizable : false,
					editor:{ 
						type:'numberbox',
			    		options:{
			    			groupSeparator: ','
			    		}
		    		}, 
		    		formatter:function(value){
			    		return formatCurrency(value);	
			    	}
				},
				{field: field2,title: title2, width:200,align:'left',sortable : false,resizable : false,
					  formatter: function(value,row,index){
						  return EasyUiCommon.stylerOnRow(index,value,type);
					  }
				},
				{field: 'remove',title:addRow, width:50,align:'center',sortable : false,resizable : false,
					formatter: function(value,row,index){
	        		  if(permission == 'true'){
	        			  return "<a href='javascript:void(0)' onclick=\"return EasyUiCommon.removeRowOnGrid('" + index + "',false)\"><img title='Xóa' src='/resources/images/icon-delete.png'/></a>";
	        		  }else{
	        			  return '';
	        		  }
	        	  }},
	            ]],
	            onSelect : function (index,row){ 
	            	$('#divDialogSearch').hide();
	            	if(permission == 'true'){
	        		  if (EasyUiCommon.endEditing(gridId)){  
	        			  $('#' +gridId).datagrid('beginEdit', index);  
	        			  EasyUiCommon.editIndex = index; 
	        			  $('#' +gridId).datagrid('resize');
	        		  }  
	        		  EasyUiCommon.formatLabelAfterChange(type);
		        	  var ed = $('#' +gridId).datagrid('getEditor', {index:EasyUiCommon.editIndex,field: field1});
		        	  if(ed != null && ed != undefined){
		        		  $(ed.target).focusToEnd();
		        		  $(ed.target).bind('keyup',function(event){
		        			  if(event.keyCode == keyCodes.ENTER){		   				
		        				  EasyUiCommon.appendRowOnGrid(gridId);
		        			  }
		        		  });
		        	  }
		        	  var ed2 = $('#' +gridId).datagrid('getEditor', {index:EasyUiCommon.editIndex,field: field2});
		        	  if(ed2 != null && ed2 != undefined){
		        		  $(ed2.target).bind('focus',function(event){
	        				  EasyUiCommon.showGridDialog(index,'saleQty','Số lượng','numberbox',false);
	        			  });
		        	  }
		        	  $('.datagrid-view td[field="quantity"] input').attr('maxlength', '9');
			    	  $('.datagrid-view td[field="amount"] input').attr('maxlength','17');
			    	  $('.datagrid-view td[field="discountAmount"] input').attr('maxlength','17');
			    	  $('.datagrid-view td[field="discountPercent"] input').attr('maxlength', '9');
	            	}
	            } ,
				method : 'GET',
	            onLoadSuccess:function(){
	            	$('.datagrid-header-rownumber').html(jsp_common_numerical_order);
	            	if(permission == 'true'){
	            		EasyUiCommon.loadDataOnGridSuccess(gridId,type);
	            	} else {
	            		EasyUiCommon.loadDataOnGridSuccess(gridId,type);
	            	}
	        		$('#' +gridId).datagrid('resize');
	            }
			});
		}
	},
	showGridAutocomplete : function(gridId,indexGrid,inputId,_comboGridField3,_comboGridTitle3,type,apParamCode,inputValue,isTwoDetail) {
		$('#indexDialog').val(indexGrid);
		EasyUiCommon.inputId = inputId;
		EasyUiCommon.field = _comboGridField3;
		EasyUiCommon.title = _comboGridTitle3;
		EasyUiCommon.type = type;
		EasyUiCommon.apParamCode = apParamCode;
		EasyUiCommon.isMulti = isTwoDetail;
		if(inputValue == undefined){
			inputValue = '';
		}
		$('#errMsgUIDialog').html('').hide();
		$('#searchGrid').datagrid({
			url : '/catalog/product/search?status=1&isExceptZCat=true&isPriceValid=true&productCode=' +inputValue+'&lstProductCode=' +EasyUiCommon._lstProductSelected,
			autoRowHeight : false,
			view: bufferview,
			pageSize:10,  
			rownumbers : true, 
			height: 150,
			fitColumns:true,
			singleSelect:true,
			queryParams:{
		 	},
		 	scrollbarSize : 18,
			width : ($('#gridDialog').width()),
		    columns:[[  
		        {field:'productCode',title:'Mã SP',align:'left', width:150, sortable : false,resizable : false,fixed:true, formatter:function(value){
		        	return Utils.XSSEncode(value);
		        }},  
		        {field:'productName',title:'Tên SP',align:'left', width:250, sortable : false,resizable : false,fixed:true, formatter:function(value){
		        	return Utils.XSSEncode(value);
		        }},
		        {field: EasyUiCommon.field,title: EasyUiCommon.title, width:100,align:'left',sortable : false,resizable : false,fixed:true,
		        	editor:{
		        		type:EasyUiCommon.type
		        	},
	            	formatter:function(value,row){
	            		if(type == 'checkbox'){
	            			return '<input class="checkItem" type="checkbox" id="' +EasyUiCommon.field+'_' +row.id+'" onkeypress="EasyUiCommon.eventOnTextField(event,this,\'checkItem\',\'searchGrid\')"/>';
	            		}else{
	            			return '<input class="numberItem" id="' +EasyUiCommon.field+'_' +row.id+'" onfocus="return Utils.bindFormatOnTextfield(\'' +_comboGridField3+'_' +row.id+'\',Utils._TF_NUMBER,null);" onkeypress="EasyUiCommon.eventOnTextField(event,this,\'numberItem\',\'searchGrid\')"/>';
	            		}
	            	}},
		        {field :'id',hidden : true},
		    ]],
		    onLoadSuccess :function(){
		    	$('#searchGrid').datagrid('selectRow', 0);
		    	$('.datagrid-pager').html('').hide();
		    	$('.datagrid-header-rownumber').html(jsp_common_numerical_order);
		    	var originalSize = $('.datagrid-header-row td[field=productCode] div').width();
		    	if(originalSize != null && originalSize != undefined){
		    		EasyUiCommon._lstSize.push(originalSize);
		    	}
	    		$(window).bind('keyup',function(event){
	    			if(event.keyCode == keyCodes.ENTER){
	    				var fVal = '';
	    				var data = $('#searchGrid').datagrid('getSelected');
	    				var arrVal = $(EasyUiCommon.inputId).val().split(",");
	    				if(EasyUiCommon.type == 'checkbox'){
							if(arrVal!= null && arrVal.length  > 0){
								for(var i=0;i<arrVal.length;i++){
									if(arrVal[i] != undefined){
										if(arrVal[i].trim() != '' && arrVal[i].trim().length >= 6){
											fVal += arrVal[i].trim() + ', ';
										}else{
											if(fVal.indexOf(data.productCode) == -1){
												arrVal[i] = '';
												arrVal[i] += data.productCode;
												var required = 0;
												if($('#' +EasyUiCommon.field+'_' +data.id).is(":checked")){
													arrVal[i] += "*";
													required = 1;
												}else{
													required = 0;	
												}
												fVal += arrVal[i].trim() + ', ';
												var row = {};
												row = {productCode:data.productCode,
														productName:data.productName,
														required:required
												}; 
												EasyUiCommon._lstProductSelected.push(data.productCode);
												if(EasyUiCommon.isMulti){
													var arrValues = EasyUiCommon._mapNewDataSubGrid2.get($('#indexDialog').val());
													if(arrValues == undefined || arrValues == null || arrValues.length == 0){
														arrValues = new Array();
													}
													arrValues.push(row);
													EasyUiCommon._mapNewDataSubGrid2.put($('#indexDialog').val(),arrValues);
												}else{
													var arrValues = EasyUiCommon._mapNewDataSubGrid1.get($('#indexDialog').val());
													if(arrValues == undefined || arrValues == null || arrValues.length == 0){
														arrValues = new Array();
													}
													arrValues.push(row);
													EasyUiCommon._mapNewDataSubGrid1.put($('#indexDialog').val(),arrValues);
												}
											}
										}
									}
								}
							}
	    				}else{
							if(arrVal!= null && arrVal.length  > 0){
								for(var i=0;i<arrVal.length;i++){
									if(arrVal[i] != undefined){
										if(arrVal[i].trim().indexOf("(") > -1 && arrVal[i].trim().indexOf(")") > -1){
											fVal += arrVal[i].trim() + ', ';
										}else{
											if(fVal.indexOf(data.productCode) == -1){
												arrVal[i] = '';
												arrVal[i] += data.productCode;
												arrVal[i] += "(" +$('#' +EasyUiCommon.field+'_' +data.id).val()+")";
												fVal += arrVal[i].trim() + ', ';
												var row = {};
												if(EasyUiCommon.field == 'saleQty'){
													row = {productCode:data.productCode,
															productName:data.productName,
															quantity:$('#' +EasyUiCommon.field+'_' +data.id).val()
													}; 
												}else{
													row = {productCode:data.productCode,
															productName:data.productName,
															amount:$('#' +EasyUiCommon.field+'_' +data.id).val()
													};
												}
												EasyUiCommon._lstProductSelected.push(data.productCode);
												if(EasyUiCommon.isMulti){
													var arrValues = EasyUiCommon._mapNewDataSubGrid2.get($('#indexDialog').val());
													if(arrValues == undefined || arrValues == null || arrValues.length == 0){
														arrValues = new Array();
													}
													arrValues.push(row);
													EasyUiCommon._mapNewDataSubGrid2.put($('#indexDialog').val(),arrValues);
												}else{
													var arrValues = EasyUiCommon._mapNewDataSubGrid1.get($('#indexDialog').val());
													if(arrValues == undefined || arrValues == null || arrValues.length == 0){
														arrValues = new Array();
													}
													arrValues.push(row);
													EasyUiCommon._mapNewDataSubGrid1.put($('#indexDialog').val(),arrValues);
												}
											}
										}
									}
								}
							}
	    				}
	    				$(EasyUiCommon.inputId).val(fVal);
	    				$(EasyUiCommon.inputId).focusToEnd();
	    				$('#divDialogSearch').hide();
	    				return false;
	    			}else if(event.keyCode == keyCodes.ESCAPE){
	    				$(EasyUiCommon.inputId).focusToEnd();
	    				$('#divDialogSearch').hide();
	    				return false;
	    			}
	    		});
		    }
		});  
		return false;
	},
	showGridDialog:function(gridIndex,_comboGridField3,_comboGridTitle3,type,isTwoDetail,isPromotionProduct){
		EasyUiCommon.isExceptZ = !isPromotionProduct;
		$('#indexDialog').val(gridIndex);
		$('#errMsgUIDialogUpdate').html('').hide();
		EasyUiCommon.isMulti = isTwoDetail;
		var title = '';
		
		var permissionUser = $('#permissionValue').val().trim();  //thachnn them 2 dong nay de fix loi
		var promotionStatus = $('#promotionStatus').val().trim(); //
		
		if(_comboGridField3 == 'saleQty'){
			EasyUiCommon.typeChange = EasyUiCommon.QUANTITY_PRODUCT;
		}else if(_comboGridField3 == 'amount'){
			EasyUiCommon.typeChange = EasyUiCommon.AMOUNT_PRODUCT;
		}else if(_comboGridField3 == 'required'){
			EasyUiCommon.typeChange = EasyUiCommon.REQUIRE_PRODUCT;
		}
		
			if(promotionStatus != 2){
				$('#productDialogUpdate .BtnCenterSection').html('<button id="btnChange" class="BtnGeneralStyle" onclick="$(\'#productDialogUpdate\').window(\'close\');">Đóng</button>');
			}
		
		if(permissionUser == 'true'){
			if(promotionStatus == 2){
				title = '<a href="javascript:void(0);" onclick="EasyUiCommon.showTreeGridDialog(' +isPromotionProduct+');"><img title="Thêm mới" src="/resources/images/icon-add.png"></a>';
			}else{
				title = '';
			}
		}else{
			title = '';
		}
		$('#productDialogUpdate').dialog({  
	        title: 'Thông tin SPKM',  
	        closed: false,  
	        cache: false,  
	        modal: true,
	        onOpen: function(){
	        	EasyUiCommon._lstProductOnChange = [];
	        	EasyUiCommon.hasChange = false;
	        	$('.k-list-container').each(function(){
            	    $(this).removeAttr('id');
            	});
	        	$('.easyui-dialog #searchGridUpdate').datagrid({
					url : '/catalog/product/search?status=1&isExceptZCat=' +EasyUiCommon.isExceptZ+'&isPriceValid=true&productCode=01AB03' +'&lstProductCode=' +EasyUiCommon._lstProductSelected,
					autoRowHeight : false,
					pageSize:10,  
					rownumbers : true, 
					height: 160,
					fitColumns:true,
					queryParams:{
				 	},
					width : ($('.easyui-dialog #gridDialogUpdate').width()),
				    columns:[[  
				        {field:'productCode',title:'Mã SP',align:'left', width:100, sortable : false,resizable : false,fixed:true,
				        	editor:{
				        		type:'text'
				        	},
				        	formatter:function(value,row,index){
				        		if(value != null && value != undefined && value.length > 0){
				        			return '<input id="productCode_' +index+'" value="' +Utils.XSSEncode(value)+'" disabled="disabled" style="width:75px"/>';
				        		}else{
				        			return '<input id="productCode_' +index+'" style="width:75px" />';
				        		}
				        	}
				        },  
				        {field:'productName',title:'Tên SP',align:'left', width:250, sortable : false,resizable : false,fixed:true, formatter: function(value){
				        	return Utils.XSSEncode(value);
				        }},
				        {field: _comboGridField3,title: _comboGridTitle3, width:80,align:'left',sortable : false,resizable : false,fixed:true,
				        	editor:{
				        		type:type
				        	},
			            	formatter:function(value,row,index){
			            		if(type == 'checkbox'){
			            			if(row.required == 1){
			            				return '<input class="checkDialog" type="checkbox" checked="checked"/>';
			            			}else{
			            				return '<input class="checkDialog" type="checkbox" />';
			            			}
			            		}else{
			            			if(_comboGridField3 == 'amount'){
			            				return '<input id="numberItem_' +index+'" value="' +row.amount+'" style="width:80px;" class="numberDialog" onkeypress="NextAndPrevTextField(event,this,\'numberDialog\')" onfocus="return Utils.bindFormatOnTextfield(\'numberItem_' +index+'\',' +Utils._TF_NUMBER+');" maxlength="17"/>';
			            			}else{
			            				return '<input id="numberItem_' +index+'" value="' +row.quantity+'" style="width:80px;" class="numberDialog" onkeypress="NextAndPrevTextField(event,this,\'numberDialog\')" onfocus="return Utils.bindFormatOnTextfield(\'numberItem_' +index+'\',' +Utils._TF_NUMBER+');" maxlength="9"/>';
			            			}
			            		}
			            	}
				        },
				        {field: 'remove',title:title, width:50,align:'center',sortable : false,resizable : false,
							formatter: function(value,row,index){
								if(permissionUser == 'true'){
									if(promotionStatus == 2){
										return "<a href='javascript:void(0)' onclick=\"return EasyUiCommon.removeRowOnDialog('.easyui-dialog #searchGridUpdate','" + index + "','" +isTwoDetail+"')\"><img title ='Xóa' src='/resources/images/icon-delete.png'/></a>";
									}else{
										return '';
									}
								}else{
									return '';
								}
							}
						},
				    ]],
				    onLoadSuccess :function(){
				    	var newData = {};
				    	if(isTwoDetail){
				    		newData = EasyUiCommon._mapNewDataSubGrid2.get(gridIndex);
				    	}else{
				    		newData = EasyUiCommon._mapNewDataSubGrid1.get(gridIndex);
				    	}
			    		if(newData!= undefined && newData!= null && newData.length > 0){
			    			for(var i=0;i<newData.length;i++){
			    				$('.easyui-dialog #searchGridUpdate').datagrid('insertRow',{index: i, row:newData[i]});
			    			}
			    		}
				    	$('.datagrid-pager').html('').hide();
				    	$('.datagrid-header-rownumber').html(jsp_common_numerical_order);
				    	var originalSize = $('.easyui-dialog .datagrid-header-row td[field=productCode] div').width();
				    	if(originalSize != null && originalSize != undefined){
				    		EasyUiCommon._lstSize.push(originalSize);
				    	}
				    	if(newData!= undefined && newData!= null && newData.length > 0){
				    		EasyUiCommon.insertRowOnDialog(newData.length);
				    	}else{
				    		EasyUiCommon.insertRowOnDialog(0);
				    	}
				    	if(_comboGridField3 == 'required'){
				    		$('.checkDialog').each(function(){
				    			$(this).focus();
				    			return false;
				    		});
				    	}else{
				    		$('.numberDialog').each(function(){
				    			$(this).focusToEnd();
				    			return false;
				    		});
				    	}
				    	EasyUiCommon.updateRownumWidthForJqGridEX1('.easyui-dialog #gridDialog','productCode');
				    }
				});  
	        },
	        onBeforeClose: function() {
	        },
	        onClose : function(){
	        	if(!EasyUiCommon.hasChange){
	        		for(var i = 0; i < EasyUiCommon._lstProductOnChange.length ; i++){
	        			if(EasyUiCommon.isMulti){
	        				for(var j = 0;j < EasyUiCommon._mapNewDataSubGrid2.get(gridIndex).length ; j++){
	        					if(EasyUiCommon._lstProductOnChange[i] == EasyUiCommon._mapNewDataSubGrid2.get(gridIndex)[j].productCode){
	        						EasyUiCommon._mapNewDataSubGrid2.get(gridIndex).splice(j);
	        					}
	        				}
	        			}else{
	        				for(var j = 0;j < EasyUiCommon._mapNewDataSubGrid1.get(gridIndex).length ; j++){
	        					if(EasyUiCommon._lstProductOnChange[i] == EasyUiCommon._mapNewDataSubGrid1.get(gridIndex)[j].productCode){
	        						EasyUiCommon._mapNewDataSubGrid1.get(gridIndex).splice(j);
	        					}
	        				}
	        			}
	        		}
	        	}
	        	EasyUiCommon._mapCheckProduct = new Map();
	        	$('#gridDialogUpdate').html('<table id="searchGridUpdate" class="easyui-datagrid"></table><div class="Clear"></div>');
	        }
	    });
		return false;
	},
	autocompleteOnDialog:function(inputId,index){
		var gridIndex = $('#indexDialog').val();
		var template = '<h3 style="float:left">${productCode}</h3></br><p>${productName}</p>';
		inputId.kendoAutoComplete({	 
			highlightFirst: true,
            filter: "startswith",
            placeholder: "Nhập SP...",
            dataSource: {
                type: "json",
                serverFiltering: true,
                serverPaging: true,
                pageSize: 20,
                transport: {
                    read: "/catalog/product/autocomplete?isExceptZCat=" +EasyUiCommon.isExceptZ
                }
            },
            dataTextField: "productCode",
            template: template,
            change: function(e){      
        		this.element.val(pCode);
        		this.element.parent().parent().parent().parent().children().first().next().children().html(pName);
        		var row = {};
        		if(EasyUiCommon.typeChange == EasyUiCommon.QUANTITY_PRODUCT){
        			row = {
        					productCode : pCode,
        					productName : pName,
        					quantity : 0
        			};
        		}else if(EasyUiCommon.typeChange == EasyUiCommon.AMOUNT_PRODUCT){
        			row = {
        					productCode : pCode,
        					productName : pName,
        					amount : 0
        			};
        		}else{
        			row = {
        					productCode : pCode,
        					productName : pName,
        					required : 0
        			};
        		}
        		if(EasyUiCommon.isMulti){
        			var isExist = false;
        			if(EasyUiCommon._mapNewDataSubGrid2.get(gridIndex) != null){
        				for(var i = 0;i < EasyUiCommon._mapNewDataSubGrid2.get(gridIndex).length ; i++){
        					if(pCode == EasyUiCommon._mapNewDataSubGrid2.get(gridIndex)[i].productCode){
        						isExist = true;
        						break;
        					}
        				}
        			}
        			if(!isExist){
        				if(EasyUiCommon._mapNewDataSubGrid2.get(gridIndex) != null){
        					EasyUiCommon._mapNewDataSubGrid2.get(gridIndex).push(row);
        				}else{
        					var arrValue = new Array();
        					arrValue.push(row);
        					EasyUiCommon._mapNewDataSubGrid2.put(gridIndex,arrValue);
        				}				
        			}
        		}else{
        			var isExist = false;
        			if(EasyUiCommon._mapNewDataSubGrid1.get(gridIndex) != null){
        				for(var i = 0;i < EasyUiCommon._mapNewDataSubGrid1.get(gridIndex).length ; i++){
        					if(pCode == EasyUiCommon._mapNewDataSubGrid1.get(gridIndex)[i].productCode){
        						isExist = true;
        						break;
        					}
        				}
        			}
        			if(!isExist){
        				if(EasyUiCommon._mapNewDataSubGrid1.get(gridIndex) != null){
        					EasyUiCommon._mapNewDataSubGrid1.get(gridIndex).push(row);
        				}else{
        					var arrValue = new Array();
        					arrValue.push(row);
        					EasyUiCommon._mapNewDataSubGrid1.put(gridIndex,arrValue);
        				}
        			}
        		}
        		EasyUiCommon._lstProductOnChange.push(pCode);
            },
            dataBound: function(e) {
            },
            close: function(e){
            	$(window).unbind('keyup');	
            },	  
            open: function(e) {
    			var text = inputId.attr('id')+'-list';
    			$('#' +text).css('width','350px');
    			inputId.parent().css('padding','0px !important');	
            },
            select: function(e){
            	pCode = e.item.children().first().text();
            	pName = e.item.children().last().text();
            	var lstIndex = [];
        		$('.easyui-dialog .k-autocomplete').each(function(){
        			lstIndex.push($(this).children().first().attr('id').split("_")[1]);
            	});
        		var hasNewRow = true;
        		for(var i = 0 ; i < lstIndex.length ; i++){
        			if(parseInt(lstIndex[i]) == index + 1){
        				hasNewRow = false;
        				break;
        			}
        		}
        		if(hasNewRow){
        			EasyUiCommon.insertRowOnDialog(index+1);
        		}
            }                
        });
	},
	changeQuantity:function(){
		EasyUiCommon.hasChange = true;
		$('#errMsgUIDialogUpdate').html('').hide();
		var gridIndex = $('#indexDialog').val();
		var ed = null;
		var type = $('#apParamCode').val().trim();
		if(type == 'ZV03' || type == 'ZV06'){
			ed = $('#grid').datagrid('getEditor', {index:gridIndex,field: 'productCode'});
		}else if(type == 'ZV07' || type == 'ZV08'){
			ed = $('#grid').datagrid('getEditor', {index:gridIndex,field: 'quantity'});
		}else if(type == 'ZV10' || type == 'ZV11'){
			ed = $('#grid').datagrid('getEditor', {index:gridIndex,field: 'amount'});
		}else if(type == 'ZV09'){
			ed = $('#grid').datagrid('getEditor', {index:gridIndex,field: 'quantity'});
		}else if(type == 'ZV12'){
			ed = $('#grid').datagrid('getEditor', {index:gridIndex,field: 'amount'});
		}else if(type == 'ZV15' || type == 'ZV18'){
			ed = $('#grid').datagrid('getEditor', {index:gridIndex,field: 'hiddenField'});
		}else if(type == 'ZV13' || type == 'ZV16'){
			ed = $('#grid').datagrid('getEditor', {index:gridIndex,field: 'discountPercent'});
		}else if(type == 'ZV14' || type == 'ZV17'){
			ed = $('#grid').datagrid('getEditor', {index:gridIndex,field: 'discountAmount'});
		}else if(type == 'ZV21'){
			ed = $('#grid').datagrid('getEditor', {index:gridIndex,field: 'amount'});
		}
		var lstTmp = null;
		var number = 0;
		var checkType = 0;
		if(EasyUiCommon.typeChange == EasyUiCommon.REQUIRE_PRODUCT){
			lstTmp = new Array();
			checkType = 1;
			$('.checkDialog').each(function(){
				if($('#productCode_' +number).val() != ''&& $('#productCode_' +number).val().length > 0 && $(this).parent().parent().parent().children().first().next().children().text() != ''){
					lstTmp.push($('#productCode_' +number).val());
				}
				number++;
			});
		}else{
			checkType = 2;
			lstTmp = new Array();
			$('.numberDialog').each(function(){
				if($('#productCode_' +number).val() != ''&& $('#productCode_' +number).val().length > 0 && $(this).parent().parent().parent().children().first().next().children().text() != ''){
					lstTmp.push($('#productCode_' +number).val());
				}
				number++;
			});
		}
		var xx = 0;
		var flag = false;
		while(xx < lstTmp.length){
			for (var i = 0; i< lstTmp.length; i++){
				if (xx != i){
					if (lstTmp[xx] == lstTmp[i]){
						flag = true;
					}
				}
			}
			xx++;
		}
		if (flag == true){
			if (checkType == 1){
				$('#errMsgUIDialogUpdate').html(' SP là duy nhất trong nhóm SP mua.').show();
				return false;
			}else{
				$('#errMsgUIDialogUpdate').html(' SP là duy nhất trong nhóm SPKM.').show();
				return false;
			}
		}
		if(ed != null && ed != undefined){
			EasyUiCommon.inputId = ed.target;
			var lstProductCode = '';
			var first = true;
			var isErr = false;
			var num = 0;
			if(EasyUiCommon.typeChange == EasyUiCommon.REQUIRE_PRODUCT){
				$('.checkDialog').each(function(){
					if($('#productCode_' +num).val() != ''&& $('#productCode_' +num).val().length > 0 && $(this).parent().parent().parent().children().first().next().children().text() != ''){
						var required = 0;
						if(!first){
							lstProductCode += ','; 
						}
						lstProductCode += $('#productCode_' +num).val();
						if($(this).is(":checked")){
							lstProductCode += "*";
							required = 1;
						}else{
							required = 0;
						}
						var row = {};
						row = {
								productCode:$('#productCode_' +num).val(),
								productName:$(this).parent().parent().parent().children().first().next().children().text(),
								required:required
						};
						var arrValues = null;
						if(!first){
							if(EasyUiCommon.isMulti){
								arrValues = EasyUiCommon._mapNewDataSubGrid2.get(gridIndex);
							}else{
								arrValues = EasyUiCommon._mapNewDataSubGrid1.get(gridIndex);
							}
						}else{
							arrValues = new Array();
						}
						arrValues.push(row);
						if(EasyUiCommon.isMulti){
							EasyUiCommon._mapNewDataSubGrid2.put(gridIndex,arrValues);					
						}else{
							for(var i = 0;i < EasyUiCommon._mapNewDataSubGrid1.keyArray.length;i++){
								if(EasyUiCommon._mapNewDataSubGrid1.get(i).length > 0){
									EasyUiCommon._mapNewDataSubGrid1.put(i,arrValues);
								}
							}
						}
						first = false;
					}
					num++;
				});
			}else{
				$('.numberDialog').each(function(){
					if($('#productCode_' +num).val() != ''&& $('#productCode_' +num).val().length > 0 && $(this).parent().parent().parent().children().first().next().children().text() != ''){
						if(this.value != '' && this.value > 0){
							if(!first){
								lstProductCode += ','; 
							}
							lstProductCode += $('#productCode_' +num).val();
							lstProductCode += "(" +this.value+ ")";
							var row = {};
							if(EasyUiCommon.typeChange == EasyUiCommon.QUANTITY_PRODUCT){
								row = {
										productCode:$('#productCode_' +num).val(),
										productName:$(this).parent().parent().parent().children().first().next().children().text(),
										quantity:this.value
								}; 
							}else if(EasyUiCommon.typeChange == EasyUiCommon.AMOUNT_PRODUCT){
								row = {
										productCode:$('#productCode_' +num).val(),
										productName:$(this).parent().parent().parent().children().first().next().children().text(),
										amount:this.value
								}; 
							}
							var arrValues = null;
							if(!first){
								if(EasyUiCommon.isMulti){
									arrValues = EasyUiCommon._mapNewDataSubGrid2.get(gridIndex);
								}else{
									arrValues = EasyUiCommon._mapNewDataSubGrid1.get(gridIndex);
								}
							}else{
								arrValues = new Array();
							}
							arrValues.push(row);
							if(EasyUiCommon.isMulti){
								EasyUiCommon._mapNewDataSubGrid2.put(gridIndex,arrValues);					
							}else{
								EasyUiCommon._mapNewDataSubGrid1.put(gridIndex,arrValues);
							}
							first = false;
						}else{
							isErr = true;
							$('#errMsgUIDialogUpdate').html('Vui lòng nhập số lượng KM').show();
							return false;
						}
					}
					num++;
				});
				if(EasyUiCommon.isMulti){
					if(EasyUiCommon._mapNewDataSubGrid2.get(gridIndex).length == 0){
						isErr = true;
						$('#errMsgUIDialogUpdate').html('Vui lòng chọn sản phẩm').show();
						return false;
					}
				}else{
					if(EasyUiCommon._mapNewDataSubGrid1.get(gridIndex).length == 0){
						isErr = true;
						$('#errMsgUIDialogUpdate').html('Vui lòng chọn sản phẩm').show();
						return false;
					}
				}
			}
			if(!isErr){
				if(EasyUiCommon.isMulti){
					lstProductCode = EasyUiCommon.stylerOnRow(gridIndex,lstProductCode,type,true);
					EasyUiCommon._mapProductCodeStr2.put(gridIndex,lstProductCode);
					if(type == 'ZV09' || type == 'ZV12'){
						$(ed.target).parent().parent().parent().parent().parent().parent().parent().children().last().prev().children().html(lstProductCode);
					}else{
						$(ed.target).parent().parent().parent().parent().parent().parent().parent().children().last().prev().prev().children().html(lstProductCode);
					}
				}else{
					lstProductCode = EasyUiCommon.stylerOnRow(gridIndex,lstProductCode,type);
					EasyUiCommon._mapProductCodeStr1.put(gridIndex,lstProductCode);
					if(type == 'ZV03' || type == 'ZV06'){
						$(ed.target).parent().parent().parent().parent().parent().parent().parent().parent().children().last().prev().children().html(lstProductCode);
					}else if(type == 'ZV07' || type == 'ZV08' || type == 'ZV09' || type == 'ZV10' || type == 'ZV11' || type == 'ZV12'){
						for(var i = 0;i < EasyUiCommon._mapNewDataSubGrid1.keyArray.length;i++){
							if(EasyUiCommon._mapNewDataSubGrid1.get(i).length > 0){
								$('#grid').datagrid('beginEdit',i);
								if(type == 'ZV07' || type == 'ZV08'){
									ed = $('#grid').datagrid('getEditor', {index:i,field: 'quantity'});
								}else if(type == 'ZV10' || type == 'ZV11'){
									ed = $('#grid').datagrid('getEditor', {index:i,field: 'amount'});
								}else if(type == 'ZV09'){
									ed = $('#grid').datagrid('getEditor', {index:i,field: 'quantity'});
								}else if(type == 'ZV12'){
									ed = $('#grid').datagrid('getEditor', {index:i,field: 'amount'});
								}
								$(ed.target).parent().parent().parent().parent().parent().parent().parent().children().first().children().html(lstProductCode);
							}
						}
						for(var i = 0; i < EasyUiCommon._mapProductCodeStr1.keyArray.length ; i++){
							EasyUiCommon._mapProductCodeStr1.put(parseInt(EasyUiCommon._mapProductCodeStr1.keyArray[i]),lstProductCode);
						}
					}else if(type == 'ZV15' || type == 'ZV18'){
						$(ed.target).parent().parent().parent().parent().parent().parent().parent().children().first().children().html(lstProductCode);
					}else if(type == 'ZV13' || type == 'ZV16'){
						$(ed.target).parent().parent().parent().parent().parent().parent().parent().children().first().children().html(lstProductCode);
					}else if(type == 'ZV14' || type == 'ZV17'){
						$(ed.target).parent().parent().parent().parent().parent().parent().parent().children().first().children().html(lstProductCode);
					}else if(type == 'ZV21'){
						$(ed.target).parent().parent().parent().parent().parent().parent().parent().children().first().next().children().html(lstProductCode);
					}
				}
				$('#productDialogUpdate').dialog('close');
			}
		}
	},
	autocompleteOnTextbox:function(event,value,gridId,index,inputId,type,typeAutocomplete,isTwoDetail){
		if(isTwoDetail == null || isTwoDetail == undefined){
			isTwoDetail = false;
		}
		var keyChar = String.fromCharCode(event.which);
		var namePattern = /^[A-Za-z0-9]*$/ ;
		if(namePattern.test(keyChar)){
			  $('#divDialogSearch').show();
			  var left = inputId.position().left;
			  var top = inputId.position().top;
			  var height_box = inputId.height();
			  left = left+$('.GridSection').position().left;
			  top = top+ height_box + $('.GridSection').position().top;
			  $('#productEasyUIDialog').css({'left':left,'top':top});
			  $('#typeProductValue').val(0);
			  var inputValue = '';
			  if(value == null || value == undefined){
				  value = '';
			  }
			  inputValue =  value + keyChar;
			  var size = inputValue.split(",").length;
			  if(size > 0){
				  inputValue = inputValue.split(",")[size-1];
			  }
			  if(typeAutocomplete == EasyUiCommon.QUANTITY_PRODUCT){
				  EasyUiCommon.showGridAutocomplete(gridId,index,inputId,'saleQty','SL mua','numberbox',type,inputValue.trim(),isTwoDetail);
			  }else if(typeAutocomplete == EasyUiCommon.AMOUNT_PRODUCT){
				  EasyUiCommon.showGridAutocomplete(gridId,index,inputId,'amount','TT mua','numberbox',type,inputValue.trim(),isTwoDetail);
			  }else if(typeAutocomplete == EasyUiCommon.REQUIRE_PRODUCT){
				  EasyUiCommon.showGridAutocomplete(gridId,index,inputId,'required','BB mua','checkbox',type,inputValue.trim(),isTwoDetail);
			  }
		}
	},
	loadDataOnGridSuccess:function(gridId,apParamCode){
		$('.datagrid-header-rownumber').html(jsp_common_numerical_order);
		if(PromotionCatalog._isExcel == true){
			EasyUiCommon._lstProductSelected = new Array();
		}
		var size = $('#' +gridId).datagrid('getRows').length; 
    	if(size == 0){
    		$('#' +gridId).datagrid('appendRow',{});
    		EasyUiCommon._totalRowGrid++;
    	}else{
//    		$('#' +gridId).datagrid('appendRow',{});//Sontt comment dong nay khi fix bug! Khi list co du lieu thi ko add dong rong vao. Chi add dong
    		//rong vao khi grid ko co du lieu, hoac khi nguoi dung bam nut them moi cho grid.
    		if(apParamCode ==  'ZV07' || apParamCode ==  'ZV08' || apParamCode ==  'ZV10' || apParamCode ==  'ZV11' || apParamCode ==  'ZV09' || apParamCode ==  'ZV12'){
    			size = $('#' +gridId).datagrid('getRows').length;
    		}
    		for(var i = 0;i<size;i++){
    			var codeStr = '';
    			var nameStr = '';
    			var freeCodeStr = '';
    			var freeNameStr = '';
    			if(apParamCode ==  'ZV03' || apParamCode ==  'ZV06'){
    				codeStr = $('#' +gridId).datagrid('getRows')[i].freeProductCodeStr;
    				nameStr = $('#' +gridId).datagrid('getRows')[i].freeProductNameStr;
    				EasyUiCommon.putDataForMap(codeStr, nameStr, EasyUiCommon.QUANTITY_PRODUCT, EasyUiCommon._mapNewDataSubGrid1, i);
    			}else if(apParamCode ==  'ZV07' || apParamCode ==  'ZV08' || apParamCode ==  'ZV10' || apParamCode ==  'ZV11'){
    				codeStr = $('#' +gridId).datagrid('getRows')[i].productCodeStr;
    				nameStr = $('#' +gridId).datagrid('getRows')[i].productNameStr;
    				EasyUiCommon.putDataForMap(codeStr, nameStr, EasyUiCommon.REQUIRE_PRODUCT, EasyUiCommon._mapNewDataSubGrid1, i);
    			}else if(apParamCode ==  'ZV09' || apParamCode ==  'ZV12'){
    				codeStr = $('#' +gridId).datagrid('getRows')[i].productCodeStr;
    				nameStr = $('#' +gridId).datagrid('getRows')[i].productNameStr;
    				freeCodeStr = $('#' +gridId).datagrid('getRows')[i].freeProductCodeStr;
    				freeNameStr = $('#' +gridId).datagrid('getRows')[i].freeProductNameStr;
    				EasyUiCommon.putDataForMap(codeStr, nameStr, EasyUiCommon.REQUIRE_PRODUCT, EasyUiCommon._mapNewDataSubGrid1, i);
    				EasyUiCommon.putDataForMap(freeCodeStr, freeNameStr, EasyUiCommon.QUANTITY_PRODUCT, EasyUiCommon._mapNewDataSubGrid2, i,true);
    			}else if(apParamCode ==  'ZV15' || apParamCode ==  'ZV18'){
    				codeStr = $('#' +gridId).datagrid('getRows')[i].productCodeStr;
    				nameStr = $('#' +gridId).datagrid('getRows')[i].productNameStr;
    				freeCodeStr = $('#' +gridId).datagrid('getRows')[i].freeProductCodeStr;
    				freeNameStr = $('#' +gridId).datagrid('getRows')[i].freeProductNameStr;
    				if(apParamCode ==  'ZV15'){
    					EasyUiCommon.putDataForMap(codeStr, nameStr, EasyUiCommon.QUANTITY_PRODUCT, EasyUiCommon._mapNewDataSubGrid1, i);
    				}else{
    					EasyUiCommon.putDataForMap(codeStr, nameStr, EasyUiCommon.AMOUNT_PRODUCT, EasyUiCommon._mapNewDataSubGrid1, i);
    				}
    				EasyUiCommon.putDataForMap(freeCodeStr, freeNameStr, EasyUiCommon.QUANTITY_PRODUCT, EasyUiCommon._mapNewDataSubGrid2, i,true);
    			}else if(apParamCode ==  'ZV13' || apParamCode ==  'ZV14' || apParamCode ==  'ZV16' || apParamCode ==  'ZV17'){
    				codeStr = $('#' +gridId).datagrid('getRows')[i].productCodeStr;
    				nameStr = $('#' +gridId).datagrid('getRows')[i].productNameStr;
    				if(apParamCode ==  'ZV13' || apParamCode ==  'ZV14'){
    					EasyUiCommon.putDataForMap(codeStr, nameStr, EasyUiCommon.QUANTITY_PRODUCT, EasyUiCommon._mapNewDataSubGrid1, i);
    				}else{
    					EasyUiCommon.putDataForMap(codeStr, nameStr, EasyUiCommon.AMOUNT_PRODUCT, EasyUiCommon._mapNewDataSubGrid1, i);
    				}
    			}else if(apParamCode ==  'ZV21'){
    				codeStr = $('#' +gridId).datagrid('getRows')[i].freeProductCodeStr;
    				nameStr = $('#' +gridId).datagrid('getRows')[i].freeProductNameStr;
    				EasyUiCommon.putDataForMap(codeStr, nameStr, EasyUiCommon.QUANTITY_PRODUCT, EasyUiCommon._mapNewDataSubGrid1, i);
    			} 
    		}
    	}
	},
	insertRowOnDialog:function(index){
		var row = {};
		if(EasyUiCommon.typeChange == EasyUiCommon.QUANTITY_PRODUCT){
			row = {
					productCode : '',
					productName : '',
					quantity : '',
			};
		}else if(EasyUiCommon.typeChange == EasyUiCommon.AMOUNT_PRODUCT){
			row = {
					productCode : '',
					productName : '',
					amount : '',
			};
		}else {
			row = {
					productCode : '',
					productName : '',
					required : 0,
			};
		}
		$('.easyui-dialog #searchGridUpdate').datagrid('insertRow',{index: index, row:row});
		EasyUiCommon.autocompleteOnDialog($('#productCode_' +index),index);
	},
	eventOnTextField:function(e,selector,clazz,gridId){	
		var index = $('input.' +clazz).index(selector);	
		if((e.keyCode == keyCodes.ARROW_DOWN) ){	
			++index;
			var nextSelector = $('input.' +clazz).eq(index);
			if($(nextSelector).hasClass(clazz)){				
				setTimeout(function(){
					$('input.' +clazz).eq(index).focus();
				}, 30);
			}
		}else if(e.keyCode == keyCodes.ARROW_UP ){
			--index;
			var nextSelector = $('input.' +clazz).eq(index);
			if($(nextSelector).hasClass(clazz)){	
				setTimeout(function(){
					$('input.' +clazz).eq(index).focus();
				}, 30);
			}			
		}
		if(gridId != null && gridId != undefined){
			$('#' +gridId).datagrid('selectRow',index);
		}
	},
	eventWhenPressTAB:function(clazz){
		$(window).bind('keyup', function(evt){
    		if(evt.keyCode == keyCodes.TAB){
    			$('.' +clazz).each(function(){
    				$(this).focus();
    				return false;
    			});
    		}
	  });
	},
	putDataForMap:function(codeStr,nameStr,typeAutocomplete,_lstProductData,index,isTwoDetail){
		var type = $('#apParamCode').val().trim();
		var lstProductCode = '';
		if(codeStr != undefined ){
			if(isTwoDetail != undefined && isTwoDetail){
				lstProductCode = EasyUiCommon.stylerOnRow(index,codeStr,type,true);
				EasyUiCommon._mapProductCodeStr2.put(index,lstProductCode);
			}else{
				lstProductCode = EasyUiCommon.stylerOnRow(index,codeStr,type);
				EasyUiCommon._mapProductCodeStr1.put(index,lstProductCode);
				EasyUiCommon.productCode = lstProductCode;
			}
			var arrLst = codeStr.split(",");
			var arrLstName = nameStr.split(",");
			for(var k = 0;k < arrLst.length ;k++){
				var row = {};
				if(typeAutocomplete ==  EasyUiCommon.QUANTITY_PRODUCT){
					var x = arrLst[k].indexOf("(");
					var y = arrLst[k].indexOf(")");
					row = {productCode:arrLst[k].substring(0,x),
							productName:arrLstName[k],
							quantity:arrLst[k].substring(x+1,y)
					};
				}else if(typeAutocomplete ==  EasyUiCommon.AMOUNT_PRODUCT){
					var x = arrLst[k].indexOf("(");
					var y = arrLst[k].indexOf(")");
					row = {productCode:arrLst[k].substring(0,x),
							productName:arrLstName[k],
							amount:arrLst[k].substring(x+1,y)
					};
				}else if(typeAutocomplete ==  EasyUiCommon.REQUIRE_PRODUCT){
					var required = 0;
					var productCode = '';
					if(arrLst[k].indexOf("*") == -1){
						required = 0;
						productCode = arrLst[k]; 
					}else{
						required = 1;
						productCode = arrLst[k].split("*")[0];
					}
					row = {productCode:productCode,
							productName:arrLstName[k],
							required:required
					};
				}
				EasyUiCommon._lstProductSelected.push(row.productCode);
				var arrValues = _lstProductData.get(index);
				if(arrValues == undefined || arrValues == null || arrValues.length == 0){
					arrValues = new Array();
				}
				arrValues.push(row);
				_lstProductData.put(index,arrValues);
			}
		}else{
			if(type == 'ZV07' || type == 'ZV08' || type == 'ZV09' || type == 'ZV10' || type == 'ZV11' || type == 'ZV12'){
				if(isTwoDetail == undefined || !isTwoDetail){
					if(EasyUiCommon._mapProductCodeStr1.keyArray.length > 0){
						lstProductCode = EasyUiCommon.stylerOnRow(index,EasyUiCommon.codeByGroup,type);
						EasyUiCommon._mapProductCodeStr1.put(index,lstProductCode);
					} 
					if(_lstProductData.keyArray.length > 0){
						_lstProductData.put(index,_lstProductData.get(0));
					}
				}else{
					_lstProductData.put(index,new Array());
				}
			}else{
				var arrValues = new Array();
				_lstProductData.put(index,arrValues);
			}
		}
	},
	removeRowOnDialog:function(gridId,index,isTwoDetail){
		var tmp = new Map();
    	$.messager.confirm('Xác nhận', 'Bạn có muốn xóa dòng dữ liệu này?', function(r){
			if (r){
				$('#' +gridId).datagrid('deleteRow', index);
				var size = $('#' +gridId).datagrid('getRows').length; 
            	if(size == 0){
            		$('#' +gridId).datagrid('appendRow',{});
            	} else {
            		if(isTwoDetail == 'true'){
            			tmp = EasyUiCommon._mapNewDataSubGrid2.get($('#indexDialog').val());
            			if(tmp != null){
            				tmp.splice(index,1);
            			}
            			EasyUiCommon.putValueOnRemove(tmp);
            			EasyUiCommon._mapNewDataSubGrid2.put($('#indexDialog').val(),tmp);
            		}else{
            			tmp = EasyUiCommon._mapNewDataSubGrid1.get($('#indexDialog').val());
            			console.log(tmp[index]);
            			if(tmp != null){
            				tmp.splice(index,1);
            			}
            			EasyUiCommon.putValueOnRemove(tmp);
            			EasyUiCommon._mapNewDataSubGrid1.put($('#indexDialog').val(),tmp);
            		}
            	}
            	$('#' +gridId).datagrid('reload');
            	$('.k-list-container').each(function(){
            	    $(this).removeAttr('id');
            	});
			}
    	});
    },
    putValueOnRemove:function(tmp){
    	var i = 0;
		if(EasyUiCommon.typeChange == EasyUiCommon.QUANTITY_PRODUCT){
			$('.numberDialog').each(function(){
				if(tmp != null && tmp[i] != undefined){
					tmp[i].quantity = this.value;
				}
				i++;
			});
		}else if(EasyUiCommon.typeChange == EasyUiCommon.AMOUNT_PRODUCT){
			$('.numberDialog').each(function(){
				if(tmp != null && tmp[i] != undefined){
					tmp[i].amount = this.value;
				}
				i++;
			});
		}else if(EasyUiCommon.typeChange == EasyUiCommon.REQUIRE_PRODUCT){
			$('.checkDialog').each(function(){
				if(tmp != null && tmp[i] != undefined){
					if($(this).is(":checked")){
						tmp[i].required = 1;
					}else{
						tmp[i].required = 0;
					}
				}
				i++;
			});
		}
    },
    removeRowOnGrid:function(index,isTwoDetail){
    	var type = $('#apParamCode').val().trim();
    	var tmp = new Map();
    	$('#errMsgProduct').html('').hide();
    	if(type == 'ZV01' || type == 'ZV02' ||type == 'ZV04' ||type == 'ZV05' ||type == 'ZV03' ||type == 'ZV06'){
			rows = $('#grid').datagrid('getRows');
			if(rows.length > 0){
				if(rows.length == 1||EasyUiCommon._totalRowGrid ==1){
					return;
				}
				if(rows[index].productCode == undefined || rows[index].productCode == null || rows[index].productCode == ''){
					return;
				}
			}
		}
    	$.messager.confirm('Xác nhận', 'Bạn có muốn xóa dòng dữ liệu này?', function(r){
			if (r){
				$('#grid').datagrid('deleteRow', index);
				EasyUiCommon._totalRowGrid--;
				//var data = $('#grid').datagrid('getRows');
				//$('#grid').datagrid('loadData',data);
				var obj = new Object();
				$('#grid').datagrid('getRows').splice(index, 0, obj);
				EasyUiCommon.indexDeleted.push(index);
				var size = $('#grid').datagrid('getRows').length; 
            	if(size == 0){
            		EasyUiCommon.appendRowOnGrid('grid');
            	} else {
            		if(!(type == 'ZV01' || type == 'ZV02' ||type == 'ZV04' ||type == 'ZV05' ||type == 'ZV19' ||type == 'ZV20')){
            			if(isTwoDetail){
            				tmp = EasyUiCommon._mapNewDataSubGrid2.get(index);
            				if(tmp != null){
//            					tmp.splice(0);
            					EasyUiCommon._mapNewDataSubGrid2.put(index,new Array());
            					//EasyUiCommon._mapNewDataSubGrid2.remove(index);
            				}
            				EasyUiCommon._mapProductCodeStr2.put(index,"");
            				//EasyUiCommon._mapProductCodeStr1.remove(index);
            				//EasyUiCommon.resetMapCodeStrAfterRemoveGrid(EasyUiCommon._mapNewDataSubGrid2
            					//	, EasyUiCommon._mapProductCodeStr1, type, isTwoDetail);
            			}
            			tmp = EasyUiCommon._mapNewDataSubGrid1.get(index);
            			if(tmp != null){ 
//            				tmp.splice(0);
            				EasyUiCommon._mapNewDataSubGrid1.put(index,new Array());
            				//EasyUiCommon._mapNewDataSubGrid1.remove(index);
            			}
            			EasyUiCommon._mapProductCodeStr1.put(index,"");
            			//EasyUiCommon._mapProductCodeStr1.remove(index);
            			//EasyUiCommon.resetMapCodeStrAfterRemoveGrid(EasyUiCommon._mapNewDataSubGrid1,
            				//	EasyUiCommon._mapProductCodeStr1, type, isTwoDetail);
            		}
            	} 
			}
    	});
    },
    /**
     * reset index cua MapSubGridData va MapProductCodeStringData khi xoa tren dataGrid
     * @param mapSubData
     * @param mapCodeData
     * @param type
     * @param isMulti
     * @author vuonghn
     */
    resetMapCodeStrAfterRemoveGrid:function(mapSubData,mapCodeData,type,isMulti){
    	var keySub = mapSubData.keySet();
    	var keyCode = mapCodeData.keySet();
    	var size = keySub.length;
    	mapSubData.keyArray = new Array();
    	mapCodeData.keyArray = new Array();
    	for(var i=0;i<size;i++){
    		mapSubData.keyArray.push(i);
    		mapCodeData.keyArray.push(i);
    		var first = true;
    		var valueSub = mapSubData.valSet()[i];
    		var lstProductCode = '';
    		for(var j=0;j<valueSub.length;j++){
    			if(!first){
        			lstProductCode+=',';
        		}
    			lstProductCode+=valueSub[j].productCode;
    			if(valueSub[j].quantity != undefined && valueSub[j].quantity !=null){
    				lstProductCode += "(" +valueSub[j].quantity+")";
    			}else{
    				if(valueSub[j].required != undefined && valueSub[j].required !=null){
    					if(valueSub[j].required == 1){
    						lstProductCode += '*';
    					}
    				}
    			}
    			first = false;
    		}
    		lstProductCode = EasyUiCommon.stylerOnRow(i, lstProductCode, type, isMulti);
    		mapCodeData.put(i,lstProductCode);
    	}
    },
    cssForNumberBox:function(value){
    	var lstStr = '';
	    for(var k = 0;k<value.split(",").length;k++){
		   if(lstStr != ''){
			   lstStr += ", ";
		   }
		   var x = value.split(",")[k].indexOf("(");
		   var y = value.split(",")[k].indexOf(")");
		   if(value.split(",")[k].substring(0,x) != ''){
			   lstStr += value.split(",")[k].substring(0,x) + "(<span style='color:red'>" + EasyUiCommon.formatCurrency(value.split(",")[k].substring(x+1,y)) + "</span>)";
		   }
	    }
	    if(lstStr != '' && lstStr.split(",").length > 3){
			var len = lstStr.split(",")[0].length + lstStr.split(",")[1].length +lstStr.split(",")[2].length;
			lstStr = lstStr.substring(0,len+2) + " , <strong>...</strong>";
		}
	    return lstStr;
    },
    cssForCheckBox:function(value){
    	var lstStr = '';
	    for(var k = 0;k<value.split(",").length;k++){
		   if(lstStr != ''){
			   lstStr += ", ";
		   }
		   if(value.split(",")[k].indexOf("*") > -1){
			   x = value.split(",")[k].indexOf("*");
			   if(value.split(",")[k].substring(0,x) != ''){
				   lstStr += value.split(",")[k].substring(0,x) + "<span style='color:red'>*</span>";
			   }
		   }else{
			   lstStr += value.split(",")[k];
		   }
	    }
	    if(lstStr != '' && lstStr.split(",").length > 3){
			var len = lstStr.split(",")[0].length + lstStr.split(",")[1].length +lstStr.split(",")[2].length;
			lstStr = lstStr.substring(0,len+2) + " , <strong>...</strong>";
		}
	    return lstStr;
    },
    loadProductTreeGrid:function(param,isPromotionProduct){
		$('#loadingDialog').show();
		if(param == null || param == undefined || param == ''){
			param = '&code=&name=&type=&parentId=&typeObject=2&isPromotionProduct=' +isPromotionProduct;
		}
		var title = 'Số lượng';
		var typeBox = 'numberbox';
		if(EasyUiCommon.typeChange == EasyUiCommon.QUANTITY_PRODUCT){
			title = 'Số lượng';
			typeBox = 'numberbox';
		}else if(EasyUiCommon.typeChange == EasyUiCommon.AMOUNT_PRODUCT){
			title = 'Tổng tiền';
			typeBox = 'numberbox';
		}else if(EasyUiCommon.typeChange == EasyUiCommon.REQUIRE_PRODUCT){
			title = 'Bắt buộc mua';
			typeBox = 'checkbox';
		}
		var field = 'quantity';
		$('.easyui-dialog #treeGrid').treegrid({  
			url:  '/rest/catalog/product/tree-grid/0.json?id=' +$('#promotionId').val() + param,
			width:($('.easyui-dialog #treeGridContainer').width()-20),  
			height:400,  
			idField: 'id',  
			treeField: 'text',
			method:'GET',
			checkOnSelect:false,
			singleSelect:false,
			rownumbers:true,
			columns:[[  
	          {field: 'text', title: 'Sản phẩm', resizable: false, width: 400, formatter: function(value, row, index){
			    	return Utils.XSSEncode(value);
			  }},
	          {field: field,title: title, width:180,align:'center',sortable : false,resizable : false,fixed:true,
	        	  editor:{
	        		  type : typeBox
	        	  },
	        	  formatter:function(value,row){
	        		  if(row.attributes.productTreeVO.type == 'PRODUCT'){
		        		  if(typeBox == 'numberbox'){
		        			  return '<input style="width:140px" class="numberSecondDialog" id="quantity_' +row.id+'" onfocus="return Utils.bindFormatOnTextfield(\'quantity_' +row.id+'\',Utils._TF_NUMBER,null);" onkeypress="NextAndPrevTextField(event,this,\'numberSecondDialog\')"/>';
		        		  }else{
		        			  return '<input style="width:140px" type="checkbox" class="checkSecondDialog" id="quantity_' +row.id+'" onkeypress="NextAndPrevTextField(event,this,\'checkSecondDialog\')"/>';
		        		  }
	        		  }
	        	  }
	          },
	          {field: 'ck',checkbox:true}
	          ]],
            onLoadSuccess:function(row,data){
            	$('.easyui-dialog .datagrid-header-rownumber').html(jsp_common_numerical_order);
            	$('#loadingDialog').hide();
            	setTimeout(function(){
            		CommonSearchEasyUI.fitEasyDialog();
        	  	},1000);
            	$('.numberSecondDialog').each(function(){
            		EasyUiCommon.hideCheckBoxOnTree(this);
	        	    $(this).bind('keyup',function(event){
	        	        if(event.keyCode == keyCodes.ENTER){
	        	        	EasyUiCommon.selectOnSecondDialog();
	        	        }
	        	    });
	        	});
            	$('.checkSecondDialog').each(function(){
	        	    EasyUiCommon.hideCheckBoxOnTree(this);
	        	});
            },
            onExpand:function(row){
            	var param = '&code=&name=&typeObject=2&parentId=' +row.id+'&type=' +row.attributes.productTreeVO.type+'&isPromotionProduct=' +isPromotionProduct;
            	setTimeout(function(){
	            	$.getJSON('/rest/catalog/product/tree-grid/0.json?id=' +$('#promotionId').val() + param,function(data){
	            		var isExist = false;
	            		for(var i = 0; i < data.rows.length ; i++){
	            			if(EasyUiCommon._lstProductExpand.indexOf(data.rows[i].id) > -1){
	            				isExist = true;
	            				break;
	            			}else{
	            				EasyUiCommon._lstProductExpand.push(data.rows[i].id);
	            			}
	            		}
	            		if(!isExist){
            				$('#treeGrid').treegrid('append',{
            					parent: row.id,  
            					data: data.rows
            				});
	            		}
	        		});
            	},500);
            },
            onCollapse:function(row){
            },
            onCheck:function(row){
//        		var arr = row.attributes.productTreeVO.listChildren;
//        		EasyUiCommon.recursionCheck(arr,true);
            	if(row.attributes.productTreeVO.type == 'PRODUCT'){
            		 var id = row.attributes.productTreeVO.id;
            		 var text = row.attributes.productTreeVO.name;
            		 var code = text.split('-')[0];
            		 var name = text.split('-')[1];
            		 var insRow = {
    					 productCode:code,
    					 productName:name,
    					 quantity:$('#quantity_' + id).val()
        			 };  
            		 var array = new Array();
            		 array.push(insRow);
            		 EasyUiCommon._mapProductOnSecondDialog.put(id,array);
            	}
		    },
		    onUncheck:function(row){
//		    	var arr = row.attributes.productTreeVO.listChildren;
//		    	EasyUiCommon.recursionUnCheck(arr);
		    	var id = row.attributes.productTreeVO.id;
		    	EasyUiCommon._mapProductOnSecondDialog.remove(id);
		    },
		    onCheckAll:function(rows){
		    	for(var i = 0;i<rows.length;i++){
		    		var row = rows[i];
        			var id = row.attributes.productTreeVO.id;
	           		var text = row.attributes.productTreeVO.name;
	           		var code = text.split('-')[0];
	           		var name = text.split('-')[1];
	           		var insRow = {
	       					productCode:code,
	       					productName:name,
	       					quantity:$('#quantity_' + id).val()
	           			};  
	           		var array = new Array();
	           		array.push(insRow);
	           		EasyUiCommon._mapProductOnSecondDialog.put(id,array);
		    	}
		    },
		    onUncheckAll:function(rows){
		    	for(var i = 0;i<rows.length;i++){
		    		var row = rows[i];
		    		var id = row.attributes.productTreeVO.id;
		    		EasyUiCommon._mapProductOnSecondDialog.remove(id);
		    	}
		    },
		});
	},
	hideCheckBoxOnTree:function(input){
		var html = $(input).parent().parent().parent().parent().parent().parent().parent().parent().parent().children();
	    var htmlCat = html;
	    htmlCat = html.parent().parent().parent().parent().parent().parent().children();
	    var size = html.length;
	    var sizeCat = htmlCat.length;
	    for(var i = 0; i < size ; i++){
	    	if(i > 0){
	    		html = html.next();
	    	}
	    	html = html.first();
	    	var id = html.attr('id');
	    	if(id != undefined){
	    		var htmlSub = html;
	    		htmlSub.children().last().children().html('');
	    	}
	    }
	    for(var i = 0; i < sizeCat ; i++){
	    	if(i > 0){
	    		htmlCat = htmlCat.next();
	    	}
	    	htmlCat = htmlCat.first();
	    	var id = htmlCat.attr('id');
	    	if(id != undefined){
	    		var htmlCat1 = htmlCat;
	    		htmlCat1.children().last().children().html('');
	    	}
	    }
	},
	recursionCheck:function(arr,isHasTextBox){
		var size = 0;
		if(arr != null && arr != undefined){
			size = arr.length;
		}
		var lstArr = [];
		if(size == 0){
			return;
		}else{
			for(var i=0;i<size;i++){
				if(i < size){
					if(arr[i].type != 'PRODUCT'){
						$('.easyui-dialog #treeGrid').treegrid('select',arr[i].id);
						lstArr = arr[i].listChildren;
						EasyUiCommon.recursionCheck(lstArr,true);
					}else{
						if(isHasTextBox){
							if(i == 0){
								$('#quantity_' +arr[i].id).focus();
							}
						}
						$('.easyui-dialog #treeGrid').treegrid('select',arr[i].id);
					}
				}
			}
		}
	},
	recursionUnCheck:function(arr){
		var size = 0;
		if(arr != null && arr != undefined){
			size = arr.length;
		}
		var lstArr = [];
		if(size == 0){
			return;
		}else{
			for(var i=0;i<size;i++){
				if(i < size){
					if(arr[i].type != 'PRODUCT'){
						$('.easyui-dialog #treeGrid').treegrid('unselect',arr[i].id);
						lstArr = arr[i].listChildren;
						EasyUiCommon.recursionUnCheck(lstArr);
					}else{
						$('.easyui-dialog #treeGrid').treegrid('unselect',arr[i].id);
					}
				}
			}
		}
	},
	showTreeGridDialog:function(isPromotionProduct){
		$('#dialogProduct').dialog({  
	        closed: false,  
	        cache: false,  
	        modal: true,
	        position: 'middle',
	        center: true, 
	        onOpen: function(){
	        	$('#errMsgProductDlg').html('').hide();
	        	EasyUiCommon._lstProductExpand = [];
	        	EasyUiCommon._mapProductOnSecondDialog = new Map();
	        	$('#productCodeDlg').focus();
	        	$('#productCodeDlg').val('');
	        	$('#productNameDlg').val('');
	        	$('#btnSeachProductTree').bind('click',function(){
	        		EasyUiCommon._lstProductExpand = [];
            		var param = '&code=' +encodeChar($('.easyui-dialog #productCodeDlg').val().trim())+'&name=' +encodeChar($('.easyui-dialog #productNameDlg').val().trim())+'&type=&parentId=&typeObject=2&isPromotionProduct=' +isPromotionProduct;
        			EasyUiCommon.loadProductTreeGrid(param,isPromotionProduct);
        		});
            	$('#productCodeDlg,#productNameDlg').each(function(){
	        	    $(this).bind('keyup',function(event){
	        	        if(event.keyCode == keyCodes.ENTER){
	        	        	EasyUiCommon._lstProductExpand = [];
	        	        	var param = '&code=' +encodeChar($('.easyui-dialog #productCodeDlg').val().trim())+'&name=' +encodeChar($('.easyui-dialog #productNameDlg').val().trim())+'&type=&parentId=&typeObject=2&isPromotionProduct=' +isPromotionProduct;
	            			EasyUiCommon.loadProductTreeGrid(param,isPromotionProduct);
	        	        }
	        	    });
	        	});
        		EasyUiCommon.loadProductTreeGrid(null,isPromotionProduct);
	        },
	        onBeforeClose: function() {
	        },
	        onClose : function(){
	        	$('#divProductContainer').hide();
	        }
	    });
	},
	selectOnSecondDialog:function(){
		$('#errMsgProductDlg').html('').hide();
		var msg = '';
		var newData = {};
    	if(EasyUiCommon.isMulti){
    		newData = EasyUiCommon._mapNewDataSubGrid2.get($('#indexDialog').val());
    	}else{
    		newData = EasyUiCommon._mapNewDataSubGrid1.get($('#indexDialog').val());
    	}
		var size = 0;
		if(newData!= undefined && newData!= null && newData.length > 0){
			size = newData.length;
		}
		var newDataOnSecondDialog = EasyUiCommon._mapProductOnSecondDialog.keyArray;
		var isErr = false;
		var id = 0;
		var lstArr = new Array();
		$('.easyui-dialog #searchGridUpdate').datagrid('deleteRow',size);
		if(newDataOnSecondDialog.length == 0){
			msg = 'Vui lòng chọn sản phẩm';
		}else{
			for(var i=size;i< size + newDataOnSecondDialog.length;i++){
				if(!isErr){
					var obj = EasyUiCommon._mapProductOnSecondDialog.get(newDataOnSecondDialog[i-size])[0];
					if(EasyUiCommon.typeChange == EasyUiCommon.QUANTITY_PRODUCT){
						if($('#quantity_' +newDataOnSecondDialog[i-size]).val() != '' && $('#quantity_' +newDataOnSecondDialog[i-size]).val().trim() > 0){
							obj.quantity = $('#quantity_' +newDataOnSecondDialog[i-size]).val().trim();
						}else{
							msg = 'Vui lòng nhập số lượng';
							isErr = true;
							id = newDataOnSecondDialog[i-size];
							break;
						}
					}else if(EasyUiCommon.typeChange == EasyUiCommon.AMOUNT_PRODUCT){
						if($('#quantity_' +newDataOnSecondDialog[i-size]).val() != '' && $('#quantity_' +newDataOnSecondDialog[i-size]).val().trim() > 0){
							obj.amount = $('#quantity_' +newDataOnSecondDialog[i-size]).val().trim();
						}else{
							msg = 'Vui lòng nhập tổng tiền';
							isErr = true;
							id = newDataOnSecondDialog[i-size];
							break;
						}
					}else if(EasyUiCommon.typeChange == EasyUiCommon.REQUIRE_PRODUCT){
						if($('#quantity_' +newDataOnSecondDialog[i-size]).is(":checked")){
							obj.required = 1;
						}else{
							obj.required = 0;
						}
					}
					lstArr.push(obj);
				}
			}
		}
		if(msg.length > 0){
			$('#errMsgProductDlg').html(msg).show();
			EasyUiCommon.insertRowOnDialog(size);
			$('#quantity_' +id).focus();
		}else{
			for(var i = size; i< size + lstArr.length ; i++){
				$('.easyui-dialog #searchGridUpdate').datagrid('insertRow',{index: i, row:lstArr[i-size]});
				var arrValues = null;
				if(EasyUiCommon.isMulti){
					arrValues = EasyUiCommon._mapNewDataSubGrid2.get($('#indexDialog').val());
				}else{
					arrValues = EasyUiCommon._mapNewDataSubGrid1.get($('#indexDialog').val());
				}
				if(arrValues == undefined || arrValues == null || arrValues.length == 0){
					arrValues = new Array();
				}
				arrValues.push(lstArr[i-size]);
				if(EasyUiCommon.isMulti){
					EasyUiCommon._mapNewDataSubGrid2.put($('#indexDialog').val(),arrValues);
				}else{
					EasyUiCommon._mapNewDataSubGrid1.put($('#indexDialog').val(),arrValues);
				}
				EasyUiCommon._lstProductOnChange.push(lstArr[i-size].productCode);
			}
			if(EasyUiCommon.typeChange == EasyUiCommon.REQUIRE_PRODUCT){
				$('.checkDialog').each(function(){
					$(this).focus();
				});
			}else{
				$('.numberDialog').each(function(){
					$(this).focusToEnd();
				});
			}
			if(EasyUiCommon.isMulti){
				EasyUiCommon.insertRowOnDialog(EasyUiCommon._mapNewDataSubGrid2.get($('#indexDialog').val()).length);
			}else{
				EasyUiCommon.insertRowOnDialog(EasyUiCommon._mapNewDataSubGrid1.get($('#indexDialog').val()).length);
			}
			$('#dialogProduct').dialog('close'); 
		}
	},
	stylerOnRow:function(index,value,type,isMulti){
		value = Utils.XSSEncode(value);
		var text = '<span style="color:white">.</span>';
		var img = '';
		var left = 0;
		if(type == 'ZV03' || type == 'ZV06'){
			left = $('.datagrid-view .datagrid-view2').width() - 118;
			img = "<a style=\"position: absolute; left: " +left+"px;\" href='javascript:void(0)' onclick=\"return EasyUiCommon.showGridDialog('" + index + "','saleQty','Số lượng','numberbox',false,true)\"><img title='Thêm SP KM' src='/resources/images/icon-view.png'/></a>";
			if(value != '' && value != null && value != undefined){
				return EasyUiCommon.cssForNumberBox(value) + img;
			}
		}else if(type == 'ZV07' || type == 'ZV08'|| type == 'ZV10'|| type == 'ZV11'){
			left = $('.datagrid-view .datagrid-view2').children().first().children().children().children().children().children().first().children().width() - 18;
			img = "<a style=\"position: absolute; left: " +left+"px;\" href='javascript:void(0)' onclick=\"return EasyUiCommon.showGridDialog('" + index + "','required','BB mua','checkbox',false,false)\"><img title='Thêm SP mua' src='/resources/images/icon-view.png'/></a>";
			if(value != '' && value != null && value != undefined){
				EasyUiCommon.codeByGroup = value;
				return EasyUiCommon.cssForCheckBox(value) + img;
			}
			if(index != 0 && EasyUiCommon.codeByGroup != ''){
				return EasyUiCommon.cssForCheckBox(EasyUiCommon.codeByGroup) + img;
			}
		}else if(type == 'ZV09' || type == 'ZV12'){
			if(isMulti != null && isMulti != undefined && isMulti){
//				left = $('.datagrid-view .datagrid-view2').width() - 118;
//				img = "<a style=\"position: absolute; left: " +left+"px;\" href='javascript:void(0)' onclick=\"return EasyUiCommon.showGridDialog('" + index + "','saleQty','Số lượng','numberbox',true,true)\"><img src='/resources/images/icon-view.png'/></a>";
				var right = 10;
				img = "<a style=\"position: absolute; right: " +right+"px;\" href='javascript:void(0)' onclick=\"return EasyUiCommon.showGridDialog('" + index + "','saleQty','Số lượng','numberbox',true,true)\"><img title='Thêm SP KM' src='/resources/images/icon-view.png'/></a>";
//				$('.datagrid-body td[field="productCodeStr"] div').css('position','relative');
				$('.datagrid-body td[field="freeProductCodeStr"] div').css('position','relative');
				if(value != '' && value != null && value != undefined){
					return EasyUiCommon.cssForNumberBox(value) + img;
				}
			}else{
//				left = $('.datagrid-view .datagrid-view2').children().first().children().children().children().children().children().first().children().width() - 18;
//				img = "<a style=\"position: absolute; left: " +left+"px;\" href='javascript:void(0)' onclick=\"return EasyUiCommon.showGridDialog('" + index + "','required','BB mua','checkbox',false,false)\"><img src='/resources/images/icon-view.png'/></a>";
				var right = 10;
				img = "<a style=\"position: absolute; right: " +right+"px;\" href='javascript:void(0)' onclick=\"return EasyUiCommon.showGridDialog('" + index + "','required','BB mua','checkbox',false,false)\"><img title ='Thêm SP mua' src='/resources/images/icon-view.png'/></a>";
				$('.datagrid-body td[field="productCodeStr"] div').css('position','relative');
				$('.datagrid-body td[field="freeProductCodeStr"] div').css('position','relative');
				if(value != '' && value != null && value != undefined){
					//EasyUiCommon.codeByGroup = value; //thachnn bo dong nay de fix bug
					return EasyUiCommon.cssForCheckBox(value) + img;
				}
				if(index != 0 && EasyUiCommon.codeByGroup != ''){
					return EasyUiCommon.cssForCheckBox(EasyUiCommon.codeByGroup) + img;
				}
			}
		}else if(type == 'ZV15' || type == 'ZV18'){
			if(isMulti != null && isMulti != undefined && isMulti){
				left = $('.datagrid-view .datagrid-view2').width() - 168;
				img = "<a style=\"position: absolute; left: " +left+"px;\" href='javascript:void(0)' onclick=\"return EasyUiCommon.showGridDialog('" + index + "','saleQty','Số lượng','numberbox',true,true)\"><img title='Thêm SP KM' src='/resources/images/icon-view.png'/></a>";
			}else{
				left = $('.datagrid-view .datagrid-view2').children().first().children().children().children().children().children().first().children().width() - 18;
				if(type == 'ZV15'){
					img = "<a style=\"position: absolute; left: " +left+"px;\" href='javascript:void(0)' onclick=\"return EasyUiCommon.showGridDialog('" + index + "','saleQty','Số lượng','numberbox',false,false)\"><img title='Thêm SP mua' src='/resources/images/icon-view.png'/></a>";
				}else{
					img = "<a style=\"position: absolute; left: " +left+"px;\" href='javascript:void(0)' onclick=\"return EasyUiCommon.showGridDialog('" + index + "','amount','TT mua','numberbox',false,false)\"><img title='Thêm SP mua' src='/resources/images/icon-view.png'/></a>";
				}
			}
			if(value != '' && value != null && value != undefined){
				return EasyUiCommon.cssForNumberBox(value) + img;
			}
		}else if(type == 'ZV13' || type == 'ZV14'|| type == 'ZV16'|| type == 'ZV17'){
			left = $('.datagrid-view .datagrid-view2').children().first().children().children().children().children().children().first().children().width() - 18;
			if(type == 'ZV13' || type == 'ZV14'){
				img = "<a style=\"position: absolute; left: " +left+"px;\" href='javascript:void(0)' onclick=\"return EasyUiCommon.showGridDialog('" + index + "','saleQty','Số lượng','numberbox',false,false)\"><img title='Thêm SP mua'  src='/resources/images/icon-view.png'/></a>";
			}else{
				img = "<a style=\"position: absolute; left: " +left+"px;\" href='javascript:void(0)' onclick=\"return EasyUiCommon.showGridDialog('" + index + "','amount','TT mua','numberbox',false,false)\"><img title='Thêm SP mua'  src='/resources/images/icon-view.png'/></a>";
			}
			if(value != '' && value != null && value != undefined){
				return EasyUiCommon.cssForNumberBox(value) + img;
			}
		}else if(type == 'ZV21'){
			left = $('.datagrid-view .datagrid-view2').width() - 178;
			img = "<a style=\"position: absolute; left: " +left+"px;\" href='javascript:void(0)' onclick=\"return EasyUiCommon.showGridDialog('" + index + "','saleQty','Số lượng','numberbox',false,true)\"><img  title='Thêm SP KM' src='/resources/images/icon-view.png'/></a>";
			if(value != '' && value != null && value != undefined){
				return EasyUiCommon.cssForNumberBox(value) + img;
			}
		}
		return text + img;
	},
	formatLabelAfterChange:function(type){
		var count = 0;
		for(var x = 0; x < EasyUiCommon._mapProductCodeStr1.keyArray.length ; x++){
			if(EasyUiCommon._mapProductCodeStr1.get(x) != '' && EasyUiCommon._mapProductCodeStr1.get(x).length > 0){
				var html = $('.datagrid-view2 .datagrid-body table tbody').children();
				var indexValue = EasyUiCommon._mapProductCodeStr1.keyArray[x];
				for(var y = 0;y <= count ; y++){
					if(y > 0 ){
						html = html.next();
					}
				}
				if(type == 'ZV03' || type == 'ZV06'){
					html.first().children().last().prev().children().html(EasyUiCommon._mapProductCodeStr1.get(indexValue));
				}else if(type == 'ZV07' || type == 'ZV08' || type == 'ZV09' || type == 'ZV10' || type == 'ZV11' || type == 'ZV12'){
					html.first().children().first().children().html(EasyUiCommon._mapProductCodeStr1.get(indexValue));
				}else if(type == 'ZV15' || type == 'ZV18'){
					html.first().children().first().children().html(EasyUiCommon._mapProductCodeStr1.get(indexValue));
				}else if(type == 'ZV13' || type == 'ZV16'){
					html.first().children().first().children().html(EasyUiCommon._mapProductCodeStr1.get(indexValue));
				}else if(type == 'ZV14' || type == 'ZV17'){
					html.first().children().first().children().html(EasyUiCommon._mapProductCodeStr1.get(indexValue));
				}else if(type == 'ZV21'){
					html.first().children().first().next().children().html(EasyUiCommon._mapProductCodeStr1.get(indexValue));
				}
				count++;
			}
		}
		var count = 0;
		for(var x = 0; x < EasyUiCommon._mapProductCodeStr2.keyArray.length ; x++){
			if(EasyUiCommon._mapProductCodeStr2.get(x) != '' && EasyUiCommon._mapProductCodeStr2.get(x).length > 0){
				var html = $('.datagrid-view2 .datagrid-body table tbody').children();
				var indexValue = EasyUiCommon._mapProductCodeStr2.keyArray[x];
				for(var y = 0;y <= count ; y++){
					if(y > 0){
						html = html.next();
					}
				}
				if(type == 'ZV09' || type == 'ZV12'){
					html.first().children().last().prev().children().html(EasyUiCommon._mapProductCodeStr2.get(indexValue));
				}else{
					html.first().children().last().prev().prev().children().html(EasyUiCommon._mapProductCodeStr2.get(indexValue));
				}
				count++;
			}
		}
	},
	formatCurrency:function(num) {
		if(num == undefined || num == null) {
			return '';
		}
		num = num.toString().split('.');
		var ints = num[0].split('').reverse();
		for (var out=[],len=ints.length,i=0; i < len; i++) {
			if (i > 0 && (i % 3) === 0){
				out.push('.');	
			}
			out.push(ints[i]);
		}
		out = out.reverse() && out.join('');
		return out;
	}
};
/*
 * END OF FILE - /web/web/resources/scripts/business/catalog/vnm.easy-ui.common.js
 */

/*
 * START OF FILE - /web/web/resources/scripts/business/catalog/vnm.sale-in-price-manager.js
 */
/*
 * Copyright 2016 Viettel ICT. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
var SaleInPriceManager = {
	_arrId: null,
	_priceId: null,
	_paramPriceSearch: null,
	_shopChoose: null,
	_valueTmp: 0,
	_paramSearch: null,
	_flagChangeWaitting : {
		isdelete: 0, //Xoa
		isrunning: 1 //Duyet
	},	
	
	/**
	 * xu ly format cot VAT
	 * @author trietptm
	 * @since Jan 20, 2016
	 */
	processVATInPrice: function(vat) {
		if (vat == undefined || vat == null) {
			return 0;
		}
		var arrNum = vat.toString().trim().split(".");
		if (arrNum.length > 2) {
			vat = parseFloat(arrNum[0].trim() + "." + arrNum[1].trim());
			SaleInPriceManager.processVATInPrice(vat);
		}
		return parseFloat(vat).toFixed(2);
	},

	/**
	 * Tao tham so tim kiem Price sale in
	 * @author trietptm
	 * @since Jan 20, 2016
	 */
	createSaleInPriceManagerParamaterSearch: function() {
		SaleInPriceManager._paramSearch = {};
		SaleInPriceManager._paramSearch.productCode = $('#txtProductCode').val().trim();
		SaleInPriceManager._paramSearch.productName = $('#txtProductName').val().trim();
		var shopCode = $('#cbxShop').combobox('getValue');
		if (shopCode.length == 0) {
			shopCode = $('#shopCode').val();
		}
		SaleInPriceManager._paramSearch.shopCode = shopCode;
		SaleInPriceManager._paramSearch.fromDateStr = $('#txtFromDate').val().trim();
		SaleInPriceManager._paramSearch.toDateStr = $('#txtToDate').val().trim();	
		SaleInPriceManager._paramSearch.status = $('#ddlStatus').val();
	},

	/**
	 * tim kiem gia sale in
	 * @author trietptm
	 * @since Jan 20, 2016
	 */
	searchProductPrice:function() {
		$('.ErrorMsgStyle').html('').hide();
		var msg = '';
		var firstLoadPage = true;
		var fDate = $('#txtFromDate').val();
		var tDate = $('#txtToDate').val();
		if (fDate.length > 0) {
			msg = Utils.getMessageOfInvalidFormatDate('txtFromDate', price_grid_column7);
		}
		if (msg.length == 0 && tDate.length > 0) {
			msg = Utils.getMessageOfInvalidFormatDate('txtToDate', price_grid_column8);
		}
		if (fDate.length >0 && tDate.length > 0 && !Utils.compareDate(fDate, tDate)) {
			msg = price_search_error_01;
			$('#fromDate').focus();
		}
		if (msg.length > 0) {
			$('#errMsgSaleInPriceManager').html(msg).show();
			return false;
		}
		SaleInPriceManager.createSaleInPriceManagerParamaterSearch();
		$('#productPriceDgDiv').html('<table id="dgProductPrice"></table>').change().show();	
		$('#dgProductPrice').datagrid({
			url: '/sale-in/search',
			width: $('#productPriceDgDiv').width() - 15,
			queryParams: SaleInPriceManager._paramSearch,
			pagination: true,
	        rownumbers: true,
	        pageNumber: 1,
	        singleSelect: true,
	        scrollbarSize: 0,
	        autoWidth: true,
	        pageList: [10, 20 , 30, 50],
	        autoRowHeight: true,
	        fitColumns: true,
			columns:[[ 
			    {field: 'shopText', title: price_grid_column2, width: 90, sortable: false, align: 'left', formatter: function(value, row, index) {
			    	var valueS = '';
			    	if (row.shopCode != undefined && row.shopCode != null) {
			    		valueS = valueS + row.shopCode + " - ";
			    	}
			    	if (row.shopName != undefined && row.shopName != null) {
			    		valueS = valueS + row.shopName;
			    	}
			    	return Utils.XSSEncode(valueS);
			    }},
			    {field: 'productCode', title: catalog_product_com, width: 140, sortable: false, align: 'left', formatter: function(value, row, index) {
			    	return Utils.XSSEncode(row.productCode + " - " +  row.productName);
			    }},
			    {field: 'fromDateStr', title: price_grid_column7, width: 40, sortable: false, align: 'center', formatter: function(value, row, index) {
			    	return Utils.XSSEncode(value);
			    }},
			    {field: 'toDateStr', title: price_grid_column8, width: 40, sortable: false, align: 'center', formatter: function(value, row, index) {
			    	return Utils.XSSEncode(value);
			    }},
			    {field: 'pricePackage', title: price_grid_column14, width: 60, sortable: false, align: 'right', formatter: function(value, row, index) {
			    	return VTUtilJS.formatCurrency(Utils.formatDoubleValue(value));
			    }},
			    {field: 'price', title: price_grid_column10, width: 50, sortable: false, align: 'right', formatter: function(value, row, index) {
			    	return VTUtilJS.formatCurrency(Utils.formatDoubleValue(value));
			    }},
			    {field: 'vat', title: price_grid_column11, width: 30, sortable: false, align: 'right', formatter: function(value, row, index) {
			    	return SaleInPriceManager.processVATInPrice(value);
			    }},
			    {field: 'pricePackageNotVAT', title: price_grid_column13, width: 60, sortable: false, align: 'right', formatter: function(value, row, index) {
			    	return VTUtilJS.formatCurrency(Utils.formatDoubleValue(value));
			    }},
			    {field: 'priceNotVAT', title: price_grid_column9, width: 50, sortable: false, align: 'right', formatter: function(value, row, index) {
			    	return VTUtilJS.formatCurrency(Utils.formatDoubleValue(value));
			    }},
			    {field: 'status', title: price_grid_column12, width: 40, sortable: false, align: 'left', formatter: function(value, row, index) {
			    	return statusTypeText.parseValue(value);
			    }},
			    {field: 'change', title: '', width: 20, align: 'center', formatter: function(value, row, index) {
			    	var html = '';
			    	if (row.status != undefined && row.status != null && row.status == 1) {
			    		html += '<a href="javascript:void(0)" id="dg_priceManage_stop" onclick="return SaleInPriceManager.stopPriceManager(' + row.id + ',' + row.status + ',\'' + row.productCode + '\');"><img title="' + common_status_stopped + '" src="/resources/images/icon-stop.png" width="16" heigh="16"></a>';
			    	}
			    	return html;
			    }}
			]],
	        onLoadSuccess: function(data) {
				$('.datagrid-header-rownumber').html(price_stt);
				Utils.updateRownumWidthAndHeightForDataGrid('dgProductPrice');
				if($('td[field="change"] a').length == 0) {
			 		$('#dgProductPrice').datagrid("hideColumn", "change");
			 	}else{
			 		$('#dgProductPrice').datagrid("showColumn", "change");
			 	}
				//Phan quyen control
			 	var arrDelete =  $('#productPriceDgDiv td[field="change"]');
				if (arrDelete != undefined && arrDelete != null && arrDelete.length > 0) {
				  for (var i = 0, size = arrDelete.length; i < size; i++) {
				    $(arrDelete[i]).prop("id", "group_edit_pr_gr_row_change_" + i);
					$(arrDelete[i]).addClass("cmsiscontrol");
				  }
				}
				Utils.functionAccessFillControl('productPriceDgDiv', function(data) {
					//Xu ly cac su kien lien quan den cac control phan quyen
					var arrTmpLength =  $('#productPriceDgDiv td[id^="group_edit_pr_gr_row_change_"]').length;
					var invisibleLenght = $('.isCMSInvisible[id^="group_edit_pr_gr_row_change_"]').length;
					if (arrTmpLength > 0 && arrTmpLength != invisibleLenght) {
						$('#dgProductPrice').datagrid("showColumn", "change");
					} else {
						$('#dgProductPrice').datagrid("hideColumn", "change");
					}
				});
	        }
		});
	},
	
	/**
	 * tam ngung gia sale in
	 * @author trietptm
	 * @since Jan 20, 2016
	 */
	stopPriceManager: function(id, status, productCode) {
		if(status != undefined && status != null && (status == 2 || status == 1)) {
			$(".ErrorMsgStyle").html('').hide();
			var msg = "";
			if (productCode == undefined || productCode == null) {
				productCode = "";
			}
			productCode = productCode.toString().trim();
			var params = {
				id: id				
			};
			var msg = sale_in_price_do_you_want_stop + productCode + "?";
			Utils.addOrSaveData(params, "/sale-in/stopPrice", null, 'errMsgPriceManager', function(data) {
				SaleInPriceManager.searchProductPrice();
				$("#successMsgPriceManager").html(price_search_error_03).show();
				setTimeout(function() {
					$('.SuccessMsgStyle').html("").hide();
				 }, 1500);
			}, null, null, null, msg);
		}
	},
	
	/**
	 * import excel
	 * @author trietptm
	 * @since Jan 20, 2016
	 */
	importPriceProduct: function() {
		$('.SuccessMsgStyle').html('').hide();
		$('.ErrorMsgStyle').html('').hide();
		
		Utils.importExcelUtils(function(data) {
			SaleInPriceManager.searchProductPrice();
			$('#excelFileFormPrice').val("").change();
			$('#fakefilepc').val("").change();
			if (data.message != undefined && data.message != null && data.message.trim().length > 0) {
				$('#errMsgImportPrice').html(data.message.trim()).change().show();
			} else {
				$('#successMsgImportPrice').html(price_search_error_03).show();
				var tm = setTimeout(function() {
					//Load lai danh sach quyen
					$('#successMsgImportPrice').html("").hide();
					clearTimeout(tm);
				 }, 1500);
			}
		}, 'importFrmPrice', 'excelFileFormPrice', 'errMsgImportPrice');
	},
	
	/**
	 * export excel
	 * @author trietptm
	 * @since Jan 20, 2016
	 */
	exportBySearchPrice: function() {
		$('.ErrorMsgStyle').html('').hide();
		var msg = '';
		if (SaleInPriceManager._paramSearch != null) {
			var rows = $('#dgProductPrice').datagrid("getData");
			if (rows == null || rows.length == 0) {
				msg = price_search_error_09;
			}
		} else {
			msg = price_search_error_09;
		}
		if (msg.length > 0) {
			$('#errMsgImportPrice').html(msg).show();
			return false;
		}
		var msg = price_search_error_10;
		$.messager.confirm(price_grid_column18, msg, function(r) {
			if (r) {
				var params = SaleInPriceManager._paramSearch;
				ReportUtils.exportReport('/sale-in/exportPriceProduct', params, 'errMsgPermission');
			}
		});
	},
	
	/**
	 * xu ly sau khi chon sp
	 * @author trietptm
	 * @since Jan 20, 2016
	 */
	fillTxtProductCodeByF9 : function(code,name) {
		$('#txtProductCode').val(code).show();
		$('#txtProductName').val(name).show();
		$('#common-dialog-search-2-textbox').dialog('close');
		$('#txtProductCode').focus();
	},

	/**
	 * download file template import gia sale in
	 * @author trietptm
	 * @since Jan 20, 2016
	 */
	downloadImportPriceTemplateFile: function() {
		var url = "/sale-in/download-import-price-template-file";
		ReportUtils.exportReport(url, {}, 'errMsg');
		return false;
	},
};
/*
 * END OF FILE - /web/web/resources/scripts/business/catalog/vnm.sale-in-price-manager.js
 */

/*
 * START OF FILE - /web/web/resources/scripts/business/catalog/vnm.bank-catalog-manager.js
 */
var BankCatalogManager = {
	search: function(){
		var msg ="";
		if(msg.length == 0){
			var shopId = $('#shopTree').combobox('getValue');
			if(shopId <= 0){
				$('#errorMsg').html('Bạn chưa nhập giá trị trường Mã đơn vị.').show();
				$('#grid').datagrid('loadData', new Array());
				return false;
			}
		}
		var params = new Object();			
		params.shopCode = $('#shopCode').val().trim();
		params.bankCode = $('#bankCode').val().trim();
		params.bankName = $('#bankName').val().trim();
		params.bankPhone = $('#bankPhone').val().trim();
		params.bankAddress = $('#bankAddress').val().trim();
		params.status = $('#status').val().trim();
		$('#grid').datagrid('load', params);
	},
	/** vuongmq; 23- September, 2014  opendialog them moi ngan hang */
	openDialogBankChangeAdd: function(){
		$(".ErrorMsgStyle").html('').hide();
		//$('#gridSearchBillContGrid').html('<table id="gridSearchBill"></table>').change();
		$('#dialogBankChange').dialog({
			title: "Thêm mới ngân hàng",
			closed: false,
			cache: false,
			modal: true,
	        width: 910,
	        height: 'auto',
	        onOpen: function(){
	        	$('.easyui-dialog .InputTextStyle').val('');
	    		$('.easyui-dialog #errorSearchMsg').hide();
	    		
	    		/**shopCodeDl don vị*/
	    		$('#shopCodeDl').addClass('BoxSelect');
	    		$('#shopCodeDl').html('<input type="text" id="shopTreeDialog" class="InputTextStyle InputText5Style" />');
	    		$('#shopCodeDl').append('<input type="hidden" id="shopCodeDialog"/>');
	    		/*TreeUtils.loadComboTreeShopHasTitle('shopTreeDialog', 'shopCodeDialog',function(data) {
	    		}, true);*/
	    		var shopCodeLoad = $('#shopCodeLogin').val().trim();
	    		if(shopCodeLoad != undefined && shopCodeLoad != null){
	    			$('#shopCodeDialog').val(shopCodeLoad);
	    		}
	    		TreeUtils.loadComboTreeShop('shopTreeDialog', 'shopCodeDialog', $('#shopCodeLogin').val().trim(), function(data) {
	    			$('.combo-panel').css('width','205');
	    			$('.combo-p').css('width','205');
	    		}, null, true, false);
	    		//ReportUtils.loadComboTreeAuthorizeEx('shopTreeDialog','shopCodeDialog',$('#shopCodeLogin').val().trim());
	    		/*ReportUtils.loadComboTreeAuthorizeEx('shopTreeDialog','shopCodeDialog', $('#shopCodeLogin').val().trim());*/
	    		$('.BoxSelect .combo').css('width','170');
	    		
	    		$('#bankCodeDl').removeAttr('disabled');
	    		/**combobox status*/
	    		$('#dialogBankChange .MySelectBoxClass').css('width', '173');
	    		$('#dialogBankChange .CustomStyleSelectBox').css('width', '140');
	    		$('#statusDl').val(1).change();
	    		$('#dialogBankChange #btnSaveBank').html('Thêm mới');
	    		
	    		$('#bankCodeDl').focus();
	    		
	    		$('#dialogBankChange #btnSaveBank').unbind('click');
	    		$('#dialogBankChange #btnSaveBank').bind('click',function(event) {
	    			BankCatalogManager.addBank();
				});
	        },
	        onClose:function() {
	        	
	        }
		});
	},
	/** vuongmq; 23- September, 2014 click buttom them moi ngan hang */
	addBank: function(){
		$(".ErrorMsgStyle").html('').hide();
		var msg ="";
		if(msg.length == 0){
			var shopId = $('#shopTreeDialog').combobox('getValue');
			if(shopId <= 0){
				msg = 'Bạn chưa nhập giá trị trường Mã đơn vị.';
			}
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfRequireCheck('bankCodeDl', 'Mã ngân hàng');
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfSpecialCharactersValidate('bankCodeDl', 'Mã ngân hàng', Utils._CODE);
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfInvaildInteger('bankPhoneDl', 'Số điện thoại');
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfMaxlengthValidate('bankPhoneDl', 'Số điện thoại', 11);
		}
		if(msg.length >0){
			$('#errorMsgDl').html(msg).show();
			return false;
		}
		var params = new Object();			
		params.shopCode = $('#shopCodeDialog').val().trim();
		params.bankCode = $('#bankCodeDl').val().trim();
		params.bankName = $('#bankNameDl').val().trim();
		params.bankAccount = $('#bankAccountDl').val().trim();
		params.bankPhone = $('#bankPhoneDl').val().trim();
		params.bankAddress = $('#bankAddressDl').val().trim();
		params.status = $('#statusDl').val().trim();
		var msg = 'Bạn có muốn thêm mới ngân hàng?';
		Utils.addOrSaveData(params, '/catalog/bank/save', null,  'errorMsgDl', function(data) {
			$('#successMsgDl').html("Lưu dữ liệu thành công").show();
			var tm = setTimeout(function(){
				//Load lai danh sach vai tro
				//$('#btnSearchPermission').click();
				$('#dialogBankChange').dialog('close');
				//$('#btnSearch').click();
				$('#grid').datagrid('reload');
				$('.SuccessMsgStyle').html("").hide();
				clearTimeout(tm);
			 }, 1500);
		}, null, null, null,msg);
		return false;
	},
	/** vuongmq; 23- September, 2014 opendialog cap nhat ngan hang */
	openDialogBankChangeUpdate: function(index, id){
		$(".ErrorMsgStyle").html('').hide();
		var data = $('#grid').datagrid('getRows')[index];
		$('#dialogBankChange').dialog({
			title: "Cập nhật ngân hàng "+ '<span style="color:#199700">' + Utils.XSSEncode(data.bankCode) + '</span>',
			closed: false,
			cache: false,
			modal: true,
	        width: 910,
	        height: 'auto',
	        onOpen: function(){
	        	$('.easyui-dialog .InputTextStyle').val('');
	    		$('.easyui-dialog #errorSearchMsg').hide();
	    		/**shopCodeDl don vị*/
	    		$('#shopCodeDl').removeClass('BoxSelect');
	    		$('#shopCodeDl').html('<input id="shopCodeDialog" type="text" class="InputTextStyle InputText5Style" value='+ data.shopCode + '></input>');
	    		
	    		/**combobox status*/
	    		$('#dialogBankChange .MySelectBoxClass').css('width', '173');
	    		$('#dialogBankChange .CustomStyleSelectBox').css('width', '140');
	    		$('#dialogBankChange #btnSaveBank').html('Cập nhật');
	    		
	    		/** set cac gia tri vao dialog*/
	    		$('#shopCodeDialog').attr('disabled', "disabled");
	    		$('#bankCodeDl').attr('disabled', "disabled");
	    		
	    		$('#bankCodeDl').val(Utils.XSSEncode(data.bankCode));
	    		$('#bankNameDl').val(Utils.XSSEncode(data.bankName));
	    		$('#bankPhoneDl').val(Utils.XSSEncode(data.bankPhone));
	    		$('#bankAddressDl').val(Utils.XSSEncode(data.bankAddress));
	    		$('#bankAccountDl').val(Utils.XSSEncode(data.bankAccount));
	    		$('#statusDl').val(Utils.XSSEncode(data.status)).change();
	    		
	    		$('#bankNameDl').focus();
	    		
	    		$('#dialogBankChange #btnSaveBank').unbind('click');
	    		$('#dialogBankChange #btnSaveBank').bind('click',function(event) {
	    			BankCatalogManager.updateBank(index, id);
				});
	        },
	        onClose:function() {
	        	/*$("input[id$='Dl']").val("");
	        	var txtIsJoin = $('#txtIsJoinOderNumber').val().trim();
	        	if(txtIsJoin.length==0){
	        		$('#txtIsJoinOderNumber').val(AdjustedRevenueManage._txtIsJoinOderNumberC);	        		
	        	}*/
	        }
		});
	},
	/** vuongmq; 23- September, 2014 click buttom cap nhat ngan hang */
	updateBank: function(index, idBank){
		$(".ErrorMsgStyle").html('').hide();
		var params = new Object();			
		params.shopCode = $('#shopCodeDialog').val().trim();
		params.bankCode = $('#bankCodeDl').val().trim();
		params.bankName = $('#bankNameDl').val().trim();
		params.bankAccount = $('#bankAccountDl').val().trim();
		params.bankPhone = $('#bankPhoneDl').val().trim();
		params.bankAddress = $('#bankAddressDl').val().trim();
		params.status = $('#statusDl').val().trim();
		params.bankId = idBank;
		var data = $('#grid').datagrid('getRows')[index];
		/** kiem tra khi cap nhat khong co gi thay doi*/
		if(data.bankName== null){
			data.bankName = '';
		}
		if(data.bankAccount== null){
			data.bankAccount = '';
		}
		if(data.bankPhone== null){
			data.bankPhone = '';
		}
		if(data.bankAddress== null){
			data.bankAddress = '';
		}
		var msg = '';
		if (data.bankName == params.bankName && data.bankAccount == params.bankAccount && data.bankPhone == params.bankPhone 
		 && data.bankAddress == params.bankAddress && data.status == params.status ) {
			/*$('#errorMsgDl').html('Không có gì thay đổi cập nhật.').show();
			return false;*/
			msg = 'Không có gì thay đổi cập nhật.';
		}
		
		if(msg.length == 0){
			msg = Utils.getMessageOfInvaildInteger('bankPhoneDl', 'Số điện thoại');
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfMaxlengthValidate('bankPhoneDl', 'Số điện thoại', 11);
		}
		if(msg.length >0){
			$('#errorMsgDl').html(msg).show();
			return false;
		}
		var msgDialog = 'Bạn có muốn cập nhật ngân hàng?';
		Utils.addOrSaveData(params, '/catalog/bank/save', null,  'errorMsgDl', function(data) {
			$('#successMsgDl').html("Lưu dữ liệu thành công").show();
			var tm = setTimeout(function(){
				//Load lai danh sach vai tro
				//$('#btnSearchPermission').click();
				$('#dialogBankChange').dialog('close');
				//$('#btnSearch').click();
				$('#grid').datagrid('reload');
				$('.SuccessMsgStyle').html("").hide();
				clearTimeout(tm);
			 }, 1500);
		}, null, null, null,msgDialog);
		return false;
	}
}

/*
 * END OF FILE - /web/web/resources/scripts/business/catalog/vnm.bank-catalog-manager.js
 */

/*
 * START OF FILE - /web/web/resources/scripts/business/catalog/vnm.param-setting.js
 */
var ApParamSetting  = {
	search: function(){
		//aabb
		var params = new Object();			
		params.apParamCode = $('#apParamCode').val().trim();
		params.apParamName = $('#apParamName').val().trim();
		params.type = $('#apParamType').val().trim();
		params.status = $('#status').val().trim();
		$("#grid").datagrid('options').sortName = null; // search thi load lai sort binh thuong
		$('#grid').datagrid('load', params);
	},
	openDialogChangeAdd: function(){
		$(".ErrorMsgStyle").hide();
		$('#dialogParam').dialog({
			title: jsp_apparam_tao_moi_tham_so,
			closed: false,
			cache: false,
			modal: true,
	        width: 910,
	        height: 'auto',
	        onOpen: function(){
	        	$('.easyui-dialog .InputTextStyle').val('');
	    		$('.easyui-dialog #errorSearchMsg').hide();
	    		$('.easyui-dialog #statusDl').val(1).change();
	    		$('#codeDl').attr('disabled', false);
	    		/**combobox status*/
	    		$('#dialogParam .MySelectBoxClass').css('width', '173');
	    		$('#dialogParam .CustomStyleSelectBox').css('width', '140');
	    		$('#dialogParam #btnSaveBank').html(jsp_common_taomoi);
	    		
	    		$('#dialogParam #btnSaveBank').unbind('click');
	    		$('#dialogParam #btnSaveBank').bind('click',function(event) {
	    			ApParamSetting.add();
				});
	        },
	        onClose:function() {
	        	
	        }
		});
	},
	/** phuongvm; 07/10/2014 click buttom them moi */
	add: function(){
		var msg ="";
		if(msg.length == 0){
			msg = Utils.getMessageOfRequireCheck('codeDl', jsp_apparam_ma_tham_so);
		}
		if(msg.length >0){
			$('#errorMsgDl').html(msg).show();
			return false;
		}
		var params = new Object();		
		params.apParamCode = $('#codeDl').val().trim();
		params.apParamName = $('#nameDl').val().trim();
		params.type = $('#typeDl').val().trim();
		params.description = $('#descDl').val().trim();
		params.value = $('#valueDl').val().trim();
		params.status = $('#statusDl').val().trim();
		var msg = jsp_apparam_ban_tao_moi_tham_so;
		Utils.addOrSaveData(params, '/catalog/apparam/save', null,  'errorMsgDl', function(data) {
			$('#successMsgDl').html(msgCommon1).show();
			var tm = setTimeout(function(){
				$('#dialogParam').dialog('close');
				$('#btnSearch').click();
				$('.SuccessMsgStyle').hide();
				clearTimeout(tm);
			 }, 1500);
		}, null, null, null,msg);
		return false;
	},
	/** phuongvm 07/10/2014 opendialog cap nhat tham so */
	openDialogChangeUpdate: function(index, id){
		$(".ErrorMsgStyle").html('').hide();
		var data = $('#grid').datagrid('getRows')[index];
		$('#dialogParam').dialog({
			title: jsp_apparam_cap_nhat_tham_so +" "+ '<span style="color:#199700">' + Utils.XSSEncode(data.apParamCode) + '</span>',
			closed: false,
			cache: false,
			modal: true,
	        width: 910,
	        height: 'auto',
	        onOpen: function(){
	        	$('.easyui-dialog .InputTextStyle').val('');
	    		$('.easyui-dialog #errorSearchMsg').hide();
	    		/**combobox status*/
	    		$('#dialogParam .MySelectBoxClass').css('width', '173');
	    		$('#dialogParam .CustomStyleSelectBox').css('width', '140');
	    		$('#dialogParam #btnSaveBank').html(jsp_common_capnhat);

	    		$('#codeDl').attr('disabled', true);
	    		
	    		$('#codeDl').val(data.apParamCode);
	    		$('#nameDl').val(data.apParamName);
	    		$('#typeDl').val(data.type);
	    		$('#descDl').val(data.description);
	    		$('#valueDl').val(data.value);
	    		$('#statusDl').val(data.status).change();
	    		
	    		$('#dialogParam #btnSaveBank').unbind('click');
	    		$('#dialogParam #btnSaveBank').bind('click',function(event) {
	    			ApParamSetting.update(index, id);
				});
	        },
	        onClose:function() {
	        	
	        }
		});
	},
	/** phuongvm 07/10/2014 click buttom cap nhat tham so*/
	update: function(index, idParam){
		var params = new Object();			
		params.apParamCode = $('#codeDl').val().trim();
		params.apParamName = $('#nameDl').val().trim();
		params.value = $('#valueDl').val().trim();
		params.type = $('#typeDl').val().trim();
		params.description = $('#descDl').val().trim();
		params.status = $('#statusDl').val().trim();
		params.idParam = idParam;
		var data = $('#grid').datagrid('getRows')[index];
		
		if(data.apParamName== null){
			data.apParamName = '';
		}
		if(data.value== null){
			data.value = '';
		}
		if(data.type== null){
			data.type = '';
		}
		if(data.description== null){
			data.description = '';
		}
		if (data.apParamName == params.apParamName && data.value == params.value && data.type == params.type 
		 && data.description == params.description && data.status == params.status ) {
			$('#errorMsgDl').html(jsp_apparam_khong_doi_cap_nhat).show();
			return false;
		}
		var msg = jsp_apparam_ban_cap_nhat_tham_so;
		Utils.addOrSaveData(params, '/catalog/apparam/save', null,  'errorMsgDl', function(data) {
			$('#successMsgDl').html(msgCommon1).show();
			var tm = setTimeout(function(){
				$('#dialogParam').dialog('close');
				//$('#btnSearch').click();
				$('#grid').datagrid('reload');
				$('.SuccessMsgStyle').hide();
				clearTimeout(tm);
			 }, 1500);
		}, null, null, null,msg);
		return false;
	}
};

/*
 * END OF FILE - /web/web/resources/scripts/business/catalog/vnm.param-setting.js
 */

/*
 * START OF FILE - /web/web/resources/scripts/business/catalog/vnm.shop-param-config.js
 */
/**
 * ShopParamConfig
 * @author vuongmq
 * @since Nov 27, 2015
 */
var ShopParamConfig  = {
	_lstDayStartDelete: null,
	_lstDayEndDelete: null,
	_mapCloseDayStart: null,
	_MAX_LENGTH_3: 3,
	_INSERT: 1,
	_EDIT: 2,
	_DELETE: 3,
	_HH: (1 * 60 * 60 * 1000), //1 gio: (1 * 60 * 60 * 1000): phut
	_MM:      (1 * 60 * 1000), //1phut:      (1 * 60 * 1000): phut

	/**
	 * setInputIsEmpty shop param
	 * @author vuongmq
	 * @since Nov 27, 2015
	 */
	setInputIsEmpty: function() {
		$('#shopDistanceOrder').val(''); // ShopId chinh la id cua combobox shop chon

		$('#shopDTStart').val('');
		$('#shopDTMiddle').val('');
		$('#shopDTEnd').val('');
		
		$('#shopCCDistance').val('');
		$('#shopCCStart').val('');
		$('#shopCCEnd').val('');

		//danh sach Id cua ShopParam
		$('#hdShopDTStart').val('');
		$('#hdShopDTMiddle').val('');
		$('#hdShopDTEnd').val('');
		
		$('#hdShopCCDistance').val('');
		$('#hdShopCCStart').val('');
		$('#hdShopCCEnd').val('');

		ShopParamConfig._lstDayStartDelete = [];
		ShopParamConfig._lstDayEndDelete = [];
		ShopParamConfig.initGridCloseDayStart();
		ShopParamConfig.initGridCloseDayEnd();
		ShopParamConfig.initGridProductStockDay();
	},

	/**
	 * search shop param
	 * @author vuongmq
	 * @param shopId
	 * @since Nov 27, 2015
	 */
	search: function(shopId) {
		hideAllMessage();
		var shopCode = $('#cbxShop').combobox('getValue');
		if (shopCode.length == 0) {
			$('#errMsgSearch').html('Vui lòng chọn nhà phân phối').show();
			return false;
		}
		var params = {shopId: shopId};
		var url = '/catalog/shop-param/search';
		ShopParamConfig.setInputIsEmpty();
		Utils.getJSONDataByAjax(params, url, function(data) {
			if (data != undefined && data != null && data.shopParamVOSave != null) {
				var row = data.shopParamVOSave;
				var shopDistanceOrder = Utils.XSSEncode(formatCurrency(row.shopDistanceOrder));
				$('#shopDistanceOrder').val(shopDistanceOrder); // ShopId chinh la id cua combobox shop chon

				var shopDTStart = Utils.XSSEncode(row.shopDTStart);
				var shopDTMiddle = Utils.XSSEncode(row.shopDTMiddle);
				var shopDTEnd = Utils.XSSEncode(row.shopDTEnd);
				
				var shopCCDistance = Utils.XSSEncode(formatCurrency(row.shopCCDistance));
				var shopCCStart = Utils.XSSEncode(row.shopCCStart);
				var shopCCEnd = Utils.XSSEncode(row.shopCCEnd);
				
				$('#shopDTStart').val(shopDTStart);
				$('#shopDTMiddle').val(shopDTMiddle);
				$('#shopDTEnd').val(shopDTEnd);
				
				$('#shopCCDistance').val(shopCCDistance);
				$('#shopCCStart').val(shopCCStart);
				$('#shopCCEnd').val(shopCCEnd);

				//danh sach Id cua ShopParam
				var shopDTStartId = Utils.XSSEncode(row.shopDTStartId);
				var shopDTMiddleId = Utils.XSSEncode(row.shopDTMiddleId);
				var shopDTEndId = Utils.XSSEncode(row.shopDTEndId);
				
				var shopCCDistanceId = Utils.XSSEncode(row.shopCCDistanceId);
				var shopCCStartId = Utils.XSSEncode(row.shopCCStartId);
				var shopCCEndId = Utils.XSSEncode(row.shopCCEndId);
				
				$('#hdShopDTStart').val(shopDTStartId);
				$('#hdShopDTMiddle').val(shopDTMiddleId);
				$('#hdShopDTEnd').val(shopDTEndId);
				
				$('#hdShopCCDistance').val(shopCCDistanceId);
				$('#hdShopCCStart').val(shopCCStartId);
				$('#hdShopCCEnd').val(shopCCEndId);

				$('#closeDayHM').val(data.closeDayHM);
				$('#beforeMinute').val(data.beforeMinute);
			}
		});
	},

	/**
	 * getInputValue shop param
	 * @author vuongmq
	 * @since Nov 27, 2015
	 */
	getInputValue: function() {
		var paramSearch = new Object();
		paramSearch.shopDistanceOrder = Utils.returnMoneyValue($('#shopDistanceOrder').val()); // ShopId chinh la id cua combobox shop chon

		paramSearch.shopDTStart = $('#shopDTStart').val();
		paramSearch.shopDTMiddle = $('#shopDTMiddle').val();
		paramSearch.shopDTEnd = $('#shopDTEnd').val();
		
		paramSearch.shopCCDistance = Utils.returnMoneyValue($('#shopCCDistance').val());
		paramSearch.shopCCStart = $('#shopCCStart').val();
		paramSearch.shopCCEnd = $('#shopCCEnd').val();

		//danh sach Id cua ShopParam
		paramSearch.shopDTStartId = $('#hdShopDTStart').val();
		paramSearch.shopDTMiddleId = $('#hdShopDTMiddle').val();
		paramSearch.shopDTEndId = $('#hdShopDTEnd').val();
		
		paramSearch.shopCCDistanceId = $('#hdShopCCDistance').val();
		paramSearch.shopCCStartId = $('#hdShopCCStart').val();
		paramSearch.shopCCEndId = $('#hdShopCCEnd').val();
		return paramSearch;
	},

	/**
	 * save info shop param
	 * @author vuongmq
	 * @since Nov 27, 2015
	 */
	saveInfo: function() {
		hideAllMessage();
		var msg = Utils.getMessageOfRequireCheck('shopId', 'Nhà phân phối');
		if (msg.length == 0) {
			var valueShop = $('#cbxShop').combobox('getValue');
			if (isNullOrEmpty(valueShop)) {
				msg = 'Vui lòng chọn nhà phân phối.';
			}
		}
		if (msg.length > 0) {
			$('#errorMsg').html(msg).show();
			return false;
		}
		var lstDayStart = [];
		var rowStart = $('#gridCloseDayStart').datagrid('getRows');
		if (rowStart != null && rowStart.length > 0) {
			for (var i = 0, sz = rowStart.length; i < sz; i++) {
				var row = rowStart[i];
				if (row.updateFlag != activeType.RUNNING) {
					var newData = new Object();
					newData.valueDateStr = row.valueDate;
					newData.valueTimeStr = row.valueTime;
					lstDayStart.push(newData);
				}
			}
		}
		var lstDayEnd = [];
		var rowEnd = $('#gridCloseDayEnd').datagrid('getRows');
		if (rowEnd != null && rowEnd.length > 0) {
			for (var i = 0, sz = rowEnd.length; i < sz; i++) {
				var row = rowEnd[i];
				var newData = new Object();
				newData.fromDateStr = row.valueFromDateStr;
				newData.toDateStr = row.valueToDateStr;
				if (row.id != undefined && row.id != null) {
					newData.id = row.id;
				}
				if (row.updateFlag != undefined && row.updateFlag != null) {
					newData.updateFlag = row.updateFlag;
				}
				lstDayEnd.push(newData);
			}
		}
		var lstProductShopMapVO = [];
		var errStock = '';
		var rowStock = $('#gridProductStockDay').datagrid('getRows');
		$('.stockDayClazz').each(function(i) {
			var value = $(this).val();
			if (!isNullOrEmpty(value)) {
				if (isNaN(value)) {
					errStock = format('Số ngày tồn kho dòng {0} không phải định dạng số', (i + 1));
					return false;
				}
				if (errStock.length == 0 && Number(value) < 0) {
					errStock = format('Số ngày tồn kho dòng {0} nhập vào phải là số nguyên.', (i + 1));
					return false;
				}
				if (errStock.length == 0 && value.trim().length > ShopParamConfig._MAX_LENGTH_3) {
					errStock = format('Số ngày tồn kho dòng {0} nhập vào phải nhỏ hơn hoặc bằng {1} ký tự.', (i + 1), ShopParamConfig._MAX_LENGTH_3);
					return false;
				}
			}
			var newData = new Object();
			newData.productId = rowStock[i].productId;
		    newData.stockDay = value;
		    if (rowStock[i].shopId != undefined && rowStock[i].shopId != null) {
		    	newData.shopId = rowStock[i].shopId;
		    }
		    if (rowStock[i].id != undefined && rowStock[i].id != null) {
		    	newData.id = rowStock[i].id;
		    }
		    lstProductShopMapVO.push(newData);
		})
		if (errStock.length > 0) {
			$('#errorMsg').html(errStock).show();
			return false;
		}
		var shopId = $('#curShopId').val();
		var paramVOSave = ShopParamConfig.getInputValue(); // danh sach value co ban
		var params = new Object();
		params.shopId = shopId;
		if (ShopParamConfig._lstDayStartDelete != null && ShopParamConfig._lstDayStartDelete.length > 0) {
			paramVOSave.lstDayStartDelete = ShopParamConfig._lstDayStartDelete;
		}
		paramVOSave.lstDayStart = lstDayStart;
		if (ShopParamConfig._lstDayEndDelete != null && ShopParamConfig._lstDayEndDelete.length > 0) {
			paramVOSave.lstDayEndDelete = ShopParamConfig._lstDayEndDelete;
		}
		paramVOSave.lstDayEnd = lstDayEnd;
		params.shopParamVOSave = paramVOSave;
		params.lstProductVO = lstProductShopMapVO;
		var url = "/catalog/shop-param/saveInfo";
		var message = "Bạn có muốn lưu thông tin cấu hình này?";
		JSONUtil.saveData2(params, url, message, "errorMsg", function() {
			var tm = setTimeout(function() {
				$('#successMsg').html("").hide();
				clearTimeout(tm);
				//window.location.href = '/catalog/shop-param/info';
				var shopId = $('#curShopId').val();
				ShopParamConfig.search(shopId);
			}, 2000);
		});	
		return false;
	},
	
	/**
	 * Delete row grid
	 * @author vuongmq
	 * @param grid
	 * @param index
	 * @since 20/11/2015 
	 */
	deleteRowGrid: function(grid, index) {
		$('#' + grid).datagrid('deleteRow', index);
		$('#' + grid).datagrid('loadData', $('#' + grid).datagrid('getRows'));

	},

	/**
	 * Init grid danh sach close day start
	 * @author vuongmq
	 * @since 27/11/2015 
	 */
	initGridCloseDayStart: function() {
		var params = new Object();
		params.shopId = $('#curShopId').val();
		var wd = $(window).width() - 500;
		$('#gridCloseDayStart').datagrid({  
		    url: '/catalog/shop-param/getShopLockExecTime',
		    rownumbers: true,
		    pagination: true,
		    singleSelect: false,
		    autoRowHeight: true,
		    pageNumber: 1,
		    pageSize: 10,
		    pageList: [10, 20, 50, 100, 500],
		    width: wd,
		    //width: 800,
		    queryParams: params,
		    //fitColumns: true,
		    scrollbarSize: 0,
		    columns: [[
				{field: 'valueDate', title: 'Ngày', width: wd - 300 - 100 - 31, align: 'center', resizable: false, formatter: function(value, row, index) {
					return Utils.XSSEncode(value);
				}},
				{field: 'valueTime', title: 'Thời gian', width: 300, align: 'center', resizable: false, formatter: function(value, row, index) {
		        	return Utils.XSSEncode(value);
		        }},
		        {field:'changeStart', title: '<a href="javaScript:void(0);" onclick="return ShopParamConfig.viewDialogAddCloseDayStart();"><img title="Thêm thời gian chốt trong ngày" src="/resources/images/icon_add.png" width="18" heigh="18"/></a>', width: 100, align: 'center', resizable: false, formatter: function(value, row, index) {
			    	var html = '<a href="javascript:void(0)" onclick="return ShopParamConfig.deleteCloseDayStart(' + index + ');"><img title="Xóa thời gian chốt trong ngày" src="/resources/images/icon_delete.png" width="15" heigh="15"></a>';
			    	return html;
			    }}, 
		    ]],
		    onLoadSuccess: function(data) {
		    	$('.datagrid-header-rownumber').html('STT');
			    updateRownumWidthForJqGrid('#closeDayStartResult');
		   	 	$(window).resize();
		   	 	ShopParamConfig._mapCloseDayStart = new Map();
		   	 	if (data != null && data.rows != null && data.rows.length > 0) {
		   	 		for (var i = 0, sz = data.rows.length; i < sz; i++) {
		   	 			var row = data.rows[i];
		   	 			if (row != null && !isNullOrEmpty(row.valueDate)) {
		   	 				ShopParamConfig._mapCloseDayStart.put(row.valueDate, row.valueTime);
		   	 			}
		   	 		}
		   	 	}
		   	 	
		    }		    
		});
	},

	/**
	 * View popup add close day start
	 * @author vuongmq
	 * @since 01/12/2015 
	 */
	viewDialogAddCloseDayStart: function() {
		hideAllMessage();
		$('#popupCloseDayStart').dialog({
			title: 'Thêm ngày thời gian chốt ngày',
			closed: false,
			cache: false,
			modal: true,
			width: 370,
			height: 170,
			onOpen: function() {
				/*setDateTimePicker('popDate');
				$('#popDate').val(getNextBySysDateForNumber(1));
				var inputPopTime = $('#popupCloseDayStart #popTime').clockpicker({
				    placement: 'bottom',
				    align: 'left',
				    autoclose: true
				});*/
				$('#popDateTime').datetimepicker({
			 		lang:'vi',
			 		format:'d/m/Y H:i'
				});
				var currentTime = new Date();
				var month = currentTime.getMonth() + 1;
				var day = currentTime.getDate();
				var year = currentTime.getFullYear();
				var hour = currentTime.getHours();
				var minute = currentTime.getMinutes();
				if (month < 10) {
					month = '0' + month;
				}
				if (day < 10) {
					day = '0' + day;
				}
				if (hour < 10) {
					hour = '0' + hour;
				}
				if (minute < 10) {
					minute = '0' + minute;
				}
				$('#popDateTime').val(day + '/' + month + '/' + year + ' 00:00');
				Utils.bindFormatOnTextfield('popDateTime', Utils._TF_NUMBER);
			},
    	});
	},

	/**
	 * checkCloseDayStartBeforeMinute
	 * // thoi gian endTime truoc startTime (minuteBefore) la true
	 * @author vuongmq
	 * @param startTime (gio cho tu dong)
	 * @param endTime (gio chon)
	 * @param minuteBefore (thoi gian cau hinh, mac dinh 10p)
	 * @param type (INSERT, DELETE); default undefined: INSERT
	 * @return String (chuoi loi)
	 * @since 02/12/2015 
	 */
	checkCloseDayStartBeforeMinute: function(startTime, endTime, minuteBefore, type) {
		var msg = '';
		var hh = ShopParamConfig._HH;
		var mm = ShopParamConfig._MM;
		var startTimeStr = startTime.split(':');
		var hStart = Number(startTimeStr[0]);
		var mStart = Number(startTimeStr[1]);
		var endTimeStr = endTime.split(':');
		var hEnd = Number(endTimeStr[0]);
		var mEnd = Number(endTimeStr[1]);
		minuteBefore = Number(minuteBefore);
		var startTimeTotal = (hStart * hh) + (mStart * mm) - (minuteBefore * mm);
		var d = new Date();
		var hCur = d.getHours();
		var mCur = d.getMinutes();
		var curTimeTotal = (hCur * hh) + (mCur * mm);
		// gio hien tai <= gio chot tu dong - 10p; 
		if (msg.length == 0) {
			if (curTimeTotal > startTimeTotal) {
				if (type == undefined || type != ShopParamConfig._DELETE) {
					msg = format('Chỉ cho thêm dòng của ngày hiện tại nếu giờ phút hiện tại trước giờ phút tự động chốt {0} phút.', minuteBefore);
				} else {
					msg = format('Xóa dòng của ngày hiện tại nếu giờ phút hiện tại trước giờ phút tự động chốt {0} phút.', minuteBefore);
				}
			}
		}
		var endTimeTotal = (hEnd * hh) + (mEnd * mm);
		curTimeTotal = curTimeTotal + (minuteBefore * mm);
		// gio chon >= gio hien tai + 10p
		if (msg.length == 0) {
			if (endTimeTotal < curTimeTotal) {
				if (type == undefined || type != ShopParamConfig._DELETE) {
					msg = format('Chỉ cho thêm dòng của ngày hiện tại nếu giờ phút chọn sau giờ phút hiện tại {0} phút.', minuteBefore);
				} else {
					msg = format('Xóa dòng của ngày hiện tại nếu giờ phút chọn sau giờ phút hiện tại {0} phút.', minuteBefore);
				}
			}
		}
		return msg;
	},

	/**
	 * grid addCloseDayStart
	 * @author vuongmq
	 * @since 30/11/2015 
	 */
	addCloseDayStart: function() {
		hideAllMessage();
		var msg = '';
		var popDate = $('#popDateTime').val();
		if (popDate.trim().length < 16 || !Utils.isDateNew(popDate)) {
			msg = 'Ngày thời gian không hợp lệ. Vui lòng chọn lại';
		}
		var valueDateStr = popDate.substring(0, 10).trim();
		var valueTimeStr = popDate.substring(11, 16).trim();
		if (msg.length == 0) {
			if (ShopParamConfig._mapCloseDayStart != null && ShopParamConfig._mapCloseDayStart.findIt(valueDateStr) >= 0) {
				msg = 'Ngày thêm đã tồn tại trong đổi thời gian chốt ngày, Vui lòng chọn lại';
			}
		}
		var curDate = ReportUtils.getCurrentDateString();
		if (msg.length == 0) {
			if (Utils.compareDate1(valueDateStr, curDate)) {
				msg = 'Ngày thêm phải lớn hơn hoặc bằng ngày hiện tại, Vui lòng chọn lại';
			}
		}
		if (msg.length == 0) {
			if (Utils.compareDateInt(valueDateStr, curDate) == 0) {
				var startTime = $('#closeDayHM').val();
				var minuteBefore = $('#beforeMinute').val();
				msg = ShopParamConfig.checkCloseDayStartBeforeMinute(startTime, valueTimeStr, minuteBefore);
			}
		}
		if (msg.length > 0) {
			$('#errMsgPopDayStart').html(msg).show();
			return false;
		}
		var numRow = $('#gridCloseDayStart').datagrid('getRows').length;
		var newData = new Object();
		newData.valueDate = valueDateStr;
		newData.valueTime = valueTimeStr;
		$('#gridCloseDayStart').datagrid('insertRow', {index: numRow, row: newData});
		ShopParamConfig._mapCloseDayStart.put(valueDateStr, valueTimeStr);
		$('#popupCloseDayStart').dialog('close');
	},

	/**
	 * grid deleteCloseDayStart
	 * @author vuongmq
	 * @param index
	 * @since 30/11/2015 
	 */
	deleteCloseDayStart: function(index) {
		var row = $('#gridCloseDayStart').datagrid('getRows')[index];
		if (row != null) {
			if (row.updateFlag == activeType.RUNNING) {
				var msg = '';
				var valueDate = row.valueDate;
				if (!isNullOrEmpty(valueDate)) {
					var curDate = ReportUtils.getCurrentDateString();
					if (Utils.compareDate1(valueDate, curDate)) {
						msg = 'Ngày xóa không được nhỏ ngày hiện tại.';
						$.messager.alert('Cảnh báo', msg, 'warning');
						return false;
					}
					// Neu ngay xoa bang ngay hien tai
					var valueTimeStr = row.valueTime;
					if (Utils.compareDateInt(valueDate, curDate) == 0 && !isNullOrEmpty(valueTimeStr)) {
						var startTime = $('#closeDayHM').val();
						var minuteBefore = $('#beforeMinute').val();
						msg = ShopParamConfig.checkCloseDayStartBeforeMinute(startTime, valueTimeStr, minuteBefore, ShopParamConfig._DELETE);
						if (msg.length > 0) {
							$.messager.alert('Cảnh báo', msg, 'warning');
							return false;	
						}
					}
				}
			}
			var message = 'Bạn có muốn xóa dòng đổi thời gian chốt ngày này? <br>';
			message += 'Thay đổi được lưu khi nhấn nút Cập nhật.';
			$.messager.confirm(jsp_common_xacnhan, message, function(r) {
				if (r) {
					if (row.updateFlag == activeType.RUNNING) {
						if (ShopParamConfig._lstDayStartDelete != null && ShopParamConfig._lstDayStartDelete.length > 0) {
								ShopParamConfig._lstDayStartDelete.push(row.id);
						} else {
							ShopParamConfig._lstDayStartDelete = [];
							ShopParamConfig._lstDayStartDelete.push(row.id);
						}
					}
					ShopParamConfig.deleteRowGrid('gridCloseDayStart', index);
				}
			});
		}
	},

	/**
	 * Init grid danh sach close day end
	 * @author vuongmq
	 * @since 27/11/2015 
	 */
	initGridCloseDayEnd: function() {
		var params = new Object();
		params.shopId = $('#curShopId').val();
		var wd = $(window).width() - 500;
		$('#gridCloseDayEnd').datagrid({  
		    url: '/catalog/shop-param/getShopLockAbortDuration',
		    rownumbers: true,
		    pagination: true,
		    singleSelect: false,
		    autoRowHeight: true,
		    pageNumber: 1,
		    pageSize: 10,
		    pageList: [10, 20, 50, 100, 500],
		    width: wd,  		    
		    //width: 800,
		    queryParams: params,
		    //fitColumns: true,
		    scrollbarSize: 0,
		    columns: [[
				{field: 'valueFromDateStr', title: 'Từ ngày', width: wd - 250 - 150 - 31, align: 'center', resizable: false, formatter: function(value, row, index) {
					return Utils.XSSEncode(value);
				}},
				{field: 'valueToDateStr', title: 'Đến ngày', width: 250, align: 'center', resizable: false, formatter: function(value, row, index) {
		        	return Utils.XSSEncode(value);
		        }},
		        {field: 'changeEnd', title: '<a href="javaScript:void(0);" onclick="return ShopParamConfig.viewDialogAddCloseDayEnd(' + ShopParamConfig._INSERT + ');"><img title="Thêm ngày chốt tự động" src="/resources/images/icon_add.png" width="18" heigh="18"/></a>', width: 150, align: 'center', resizable: false, formatter: function(value, row, index) {
			    	var html = '<a href="javascript:void(0)" onclick="return ShopParamConfig.viewDialogAddCloseDayEnd(' + ShopParamConfig._EDIT + ',' + index + ');"><img title="Chỉnh sửa ngày chốt tự động" src="/resources/images/icon_edit.png" width="15" heigh="15"></a> &nbsp &nbsp';
			    		html += '<a href="javascript:void(0)" onclick="return ShopParamConfig.deleteCloseDayEnd(' + index + ');"><img title="Xóa ngày chốt tự động" src="/resources/images/icon_delete.png" width="15" heigh="15"></a>';
			    	return html;
			    }}, 
		    ]],
		    onLoadSuccess: function(data) {
		    	$('.datagrid-header-rownumber').html('STT');
			    updateRownumWidthForJqGrid('#closeDayEndResult');
		   	 	$(window).resize();
		    }		    
		});
	},

	/**
	 * View popup add close day end
	 * @author vuongmq
	 * @param type
	 * @param index
	 * @since 01/12/2015 
	 */
	viewDialogAddCloseDayEnd: function(type, index) {
		hideAllMessage();
		var row = null;
		var titlePop = 'Thêm thời gian tạm dừng chốt tự động';
		if (type == ShopParamConfig._EDIT) {
			titlePop = 'Chỉnh sửa thời gian tạm dừng chốt tự động';
			row = $('#gridCloseDayEnd').datagrid('getRows')[index];
			if (row != null) {
				var curDate = ReportUtils.getCurrentDateString();
				if (Utils.compareDate(row.valueToDateStr, curDate)) {
					var msg = 'Đến ngày nhỏ hơn hoặc bằng ngày hiện tại không được chỉnh sửa.';
					$.messager.alert('Cảnh báo', msg, 'warning');
					return false;
				}
			}
		}
		$('#popupCloseDayEnd').dialog({
			title: titlePop,
			closed: false,
			cache: false,
			modal: true,
			width: 480,
			height: 160,
			onOpen: function() {
				setDateTimePicker('popFromDate');
				setDateTimePicker('popToDate');
				if (type == ShopParamConfig._INSERT) {
					$('#popFromDate').val(getNextBySysDateForNumber(1));
					$('#popToDate').val(getNextBySysDateForNumber(1));
					$('#btnSaveDayEnd').html('Thêm');
					$('#btnSaveDayEnd').attr('onclick', 'return ShopParamConfig.addCloseDayEnd(' + type + ');');
				} else {
					if (row != null) {
						var valueFromDate = row.valueFromDateStr;
						var valueToDate = row.valueToDateStr;
						$('#popFromDate').val(valueFromDate);
						$('#popToDate').val(valueToDate);
					}
					$('#btnSaveDayEnd').html('Chỉnh sửa');
					$('#btnSaveDayEnd').attr('onclick', 'return ShopParamConfig.addCloseDayEnd(' + type + ',' + index + ');');
				}
				
			},
    	});
	},

	/**
	 * add close day end
	 * @author vuongmq
	 * @param type
	 * @param index
	 * @since 01/12/2015 
	 */
	addCloseDayEnd: function(type, index) {
		hideAllMessage();
		var msg = '';
		if (msg.length == 0) {
			msg = Utils.getMessageOfRequireCheck('popFromDate', 'Từ ngày');
		}
		if (msg.length == 0) {
			msg = Utils.getMessageOfInvalidFormatDate('popFromDate', 'Từ ngày');
		}
		if (msg.length == 0) {
			msg = Utils.getMessageOfRequireCheck('popToDate', 'Đến ngày');
		}
		if (msg.length == 0) {
			msg = Utils.getMessageOfInvalidFormatDate('popToDate', 'Đến ngày');
		}
		var fDate = $('#popFromDate').val();
		var tDate = $('#popToDate').val();
		var curDate = ReportUtils.getCurrentDateString();
		if (msg.length == 0 && !Utils.compareDate(fDate, tDate)) {
			msg ='Từ ngày không được lớn hơn đến ngày. Vui lòng nhập lại';
			$('#popFromDate').focus();
		}
		if (msg.length > 0) {
			$('#errMsgPopDayEnd').html(msg).show();
			return false;
		}
		if (type == ShopParamConfig._INSERT) {
			if (Utils.compareDate(fDate, curDate)) {
				msg = 'Từ ngày không được nhỏ hơn hoặc bằng ngày hiện tại. Vui lòng nhập lại';
				$('#popFromDate').focus();
			}
			// validate them moi khong trung
			if (msg.length == 0) {
				msg = ShopParamConfig.checkCloseDayEndExistsDay('gridCloseDayEnd', fDate, tDate, type);
				$('#popFromDate').focus();
			}
			if (msg.length > 0) {
				$('#errMsgPopDayEnd').html(msg).show();
				return false;
			}
			var numRow = $('#gridCloseDayEnd').datagrid('getRows').length;
			var newData = new Object();
			newData.valueFromDateStr = fDate;
			newData.valueToDateStr = tDate;
			$('#gridCloseDayEnd').datagrid('insertRow', {index: numRow, row: newData});
		} else {
			if (index != null) {
				var row = $('#gridCloseDayEnd').datagrid('getRows')[index];
				if (Utils.compareDateInt(row.valueFromDateStr, curDate) <= 0 && Utils.compareDateInt(fDate, row.valueFromDateStr) != 0) {
					msg = 'Từ ngày nhỏ hơn hoặc bằng ngày hiện tại không cho đổi từ ngày';
					$('#popFromDate').focus();
				}
				if (Utils.compareDateInt(row.valueFromDateStr, curDate) > 0 && Utils.compareDate(fDate, curDate)) {
					msg = 'Từ ngày không được nhỏ hơn hoặc bằng ngày hiện tại';
					$('#popFromDate').focus();
				}
				if (msg.length == 0 && Utils.compareDate(tDate, curDate)) {
					msg = 'Đến ngày không được nhỏ hơn hoặc bằng ngày hiện tại';
					$('#popFromDate').focus();
				}
				// validate cap nhat khong trung
				if (msg.length == 0) {
					msg = ShopParamConfig.checkCloseDayEndExistsDay('gridCloseDayEnd', fDate, tDate, type, index);
					$('#popFromDate').focus();
				}
				if (msg.length > 0) {
					$('#errMsgPopDayEnd').html(msg).show();
					return false;
				}
				row.valueFromDateStr = fDate;
				row.valueToDateStr = tDate;
				if (row.updateFlag == activeType.RUNNING) {
					row.updateFlag = activeType.WAITING;
				}
				$('#gridCloseDayEnd').datagrid('updateRow', {index: index, row: row});	
			}
		}
		$('#popupCloseDayEnd').dialog('close');
	},

	/**
	 * checkCloseDayEndExistsDay
	 * @author vuongmq
	 * @param grid
	 * @param fromDate
	 * @param toDate
	 * @param type
	 * @param index
	 * @since 03/12/2015 
	 */
	checkCloseDayEndExistsDay: function(grid, fromDate, toDate, type, index) {
		var msg = '';
		var rows = $('#' + grid).datagrid('getRows');
		for (var i = 0, sz = rows.length; i < sz; i++) { 
			if (type == ShopParamConfig._EDIT && index != undefined && index == i) {
				continue;
			}
			var row = rows[i];
			if (Utils.compareDateInt(fromDate, row.valueFromDateStr) == 0 || Utils.compareDateInt(fromDate, row.valueToDateStr) == 0) {
				msg = format('Từ ngày trùng với dòng {0}', (i + 1));
				break;
			}
			if (Utils.compareDateInt(toDate, row.valueFromDateStr) == 0 || Utils.compareDateInt(toDate, row.valueToDateStr) == 0) {
				msg = format('Đến ngày trùng với dòng {0}', (i + 1));
				break;
			}
			if (Utils.compareDateInt(fromDate, row.valueFromDateStr) < 0 && Utils.compareDateInt(toDate, row.valueFromDateStr) > 0) {
				msg = format('Từ ngày và đến ngày trùng với dòng {0}', (i + 1));
				break;
			}
			if (Utils.compareDateInt(fromDate, row.valueToDateStr) < 0 && Utils.compareDateInt(toDate, row.valueToDateStr) > 0) {
				msg = format('Từ ngày và đến ngày trùng với dòng {0}', (i + 1));
				break;
			}
			if (Utils.compareDateInt(fromDate, row.valueFromDateStr) > 0 && Utils.compareDateInt(toDate, row.valueToDateStr) < 0) {
				msg = format('Từ ngày và đến ngày trùng với dòng {0}', (i + 1));
				break;
			}
		}
		return msg;
	},

	/**
	 * delete close day end
	 * @author vuongmq
	 * @param index
	 * @since 02/12/2015 
	 */
	deleteCloseDayEnd: function(index) {
		var row = $('#gridCloseDayEnd').datagrid('getRows')[index];
		if (row != null) {
			if (row.updateFlag == activeType.RUNNING || row.updateFlag == activeType.WAITING) {
				var valueFromDate = row.valueFromDateStr;
				var curDate = ReportUtils.getCurrentDateString();
				if (!isNullOrEmpty(valueFromDate) && Utils.compareDate(valueFromDate, curDate)) {
					var msg = 'Từ ngày và đến ngày xóa đều phải lớn hơn ngày hiện tại.';
					$.messager.alert('Cảnh báo', msg, 'warning');
					return false;
				}
			}
			var message = 'Bạn có muốn xóa dòng tạm dừng chốt tự động này? <br>';
			message += 'Thay đổi được lưu khi nhấn nút Cập nhật.';
			$.messager.confirm(jsp_common_xacnhan, message, function(r) {
				if (r) {
					if (row.updateFlag == activeType.RUNNING || row.updateFlag == activeType.WAITING) {
						if (ShopParamConfig._lstDayEndDelete != null && ShopParamConfig._lstDayEndDelete.length > 0) {
							ShopParamConfig._lstDayEndDelete.push(row.id);
						} else {
							ShopParamConfig._lstDayEndDelete = [];
							ShopParamConfig._lstDayEndDelete.push(row.id);
						}
					}
					ShopParamConfig.deleteRowGrid('gridCloseDayEnd', index);
				}
			});
		}
	},


	/**
	 * Init grid danh sach ProductStockDay
	 * @author vuongmq
	 * @since 27/11/2015 
	 */
	initGridProductStockDay: function() {
		var params = new Object();
		params.shopId = $('#curShopId').val();
		var wd = $(window).width() - 500;
		$('#gridProductStockDay').datagrid({  
		    url: '/catalog/shop-param/getProductShopMap',
		    rownumbers: true,
		    //pagination: true,
		    singleSelect: false,
		    autoRowHeight: true,
		    //pageNumber: 1,
		    //pageSize: 10,
		    //pageList: [10, 20, 50, 100, 500],
		    width: wd,
		    //width: 800,
		    queryParams: params,
		    height: 280,
		    //fitColumns: true,
		    scrollbarSize: 15,
		    columns: [[
				{field: 'productCode', title: 'Mã sản phẩm', width: 200, align: 'left', resizable: false, formatter: function(value, row, index) {
					return VTUtilJS.XSSEncode(value);
				}},
				{field: 'productName', title: 'Tên sản phẩm', width: wd - 200 - 150 - 48, align: 'left', resizable: false, formatter: function(value, row, index) {
		        	return VTUtilJS.XSSEncode(value);
		        }},
		        {field:'stockDay', title: 'Số ngày', width: 150, align: 'center', resizable: false, formatter: function(value, row, index) {
		        	if (isNullOrEmpty(value)) {
		        		value = '';
		        	}
			    	return '<input id="stockDay' + index + '" class="InputShopText stockDayClazz" autocomplete="off" maxlength="3" value="' + value + '">';
			    }}, 
		    ]],
		    onLoadSuccess: function(data) {
		    	$('.datagrid-header-rownumber').html('STT');
			    updateRownumWidthForJqGrid('#closeDayEndResult');
		   	 	$(window).resize(); 
		   	 	$('.stockDayClazz').each(function() {
				    var id = $(this).attr('id');
				    Utils.bindFormatOnTextfield(id, Utils._TF_NUMBER_COMMA);
				    Utils.formatCurrencyFor(id);
				});
		    }		    
		});
	},

};

/*
 * END OF FILE - /web/web/resources/scripts/business/catalog/vnm.shop-param-config.js
 */

/*
 * JavaScript file created by Rockstarapps Concatenation
*/

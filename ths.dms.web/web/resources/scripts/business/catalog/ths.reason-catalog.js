var ReasonCatalog = {
	_xhrSave : null,
	_xhrDel: null,
	_htmlReasonGroup: null,
	_curCallback: null,
	_curRowId: null,
	getGridUrl: function(reasonCode,reasonName,reasonGroupId,description,status){
		var rgId = 0;
		if(reasonGroupId!= null && reasonGroupId!= undefined){
			rgId = reasonGroupId; 
		}
		return "/catalog/reason/search?code=" + encodeChar(reasonCode) + "&name=" + encodeChar(reasonName) + "&rgId=" + encodeChar(rgId) + "&note=" + encodeChar(description) + "&status=" + status;
	},	
	searchReason: function(){
		$('#errMsg').html('').hide();
		var reasonCode = $('#reasonCode').val().trim();
		var reasonName = $('#reasonName').val().trim();
		var description = $('#description').val().trim();
		var status = $('#status').val().trim();
		var reasonGroupId = $('#reasonGroup').val();
		var url = ReasonCatalog.getGridUrl(reasonCode,reasonName,reasonGroupId,description,status);
		$("#grid").setGridParam({url:url,page:1}).trigger("reloadGrid");
		return false;
	},
	saveReason: function(){
		var msg = '';
		$('#errMsg').html('').hide();
		msg = Utils.getMessageOfRequireCheck('reasonCode','Mã lý do');
		if(msg.length == 0 && !$('#reasonCode').is(':disabled') && $('#reasonCode').val().trim().length != 10){
			msg = "Mã lý do phải đúng 10 ký tự";
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfSpecialCharactersValidate('reasonCode','Mã lý do',Utils._CODE);
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfRequireCheck('reasonName','Tên lý do');
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfSpecialCharactersValidate('reasonName','Tên lý do');
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfRequireCheck('reasonGroup','Nhóm lý do',true);
		}		
		if(msg.length == 0){
			msg = Utils.getMessageOfRequireCheck('status','Trạng thái',true);
		}
		if(msg.length == 0 && $('#description').val().trim().length > 0){
			msg = Utils.getMessageOfSpecialCharactersValidate('description','Mô tả');
		}
		if(msg.length > 0){
			$('#errMsg').html(msg).show();
			return false;
		}
		var dataModel = new Object();
		dataModel.id = $('#selReasonId').val().trim();
		dataModel.code = $('#reasonCode').val().trim();
		dataModel.name = $('#reasonName').val().trim();
		dataModel.rgId = $('#reasonGroup').val().trim();
		dataModel.note = $('#description').val().trim();
		dataModel.status = $('#status').val();
		Utils.addOrSaveRowOnGrid(dataModel, "/catalog/reason/save", ReasonCatalog._xhrSave, null, null, function(data){
			if(!data.error){
				ReasonCatalog.resetAllControl();
			}
		});	
		return false;
	},
	getSelectedReason: function(rowId,status){
		ReasonCatalog._curRowId = rowId;
		$('#reasonCode').attr('disabled','disabled');
		var id =  $("#grid").jqGrid ('getCell', rowId, 'id');
		var code =  $("#grid").jqGrid ('getCell', rowId, 'reasonCode');
		var name =  $("#grid").jqGrid ('getCell', rowId, 'reasonName');
		var description =  $("#grid").jqGrid ('getCell', rowId, 'description');
		var reasonGroupId =  $("#grid").jqGrid ('getCell', rowId, 'reasonGroup.id');
		var reasonGroupName =  $("#grid").jqGrid ('getCell', rowId, 'reasonGroup.reasonGroupName');
		if(id!= null && id!= 0 && id!=undefined){
			$('#selReasonId').val(id);
		} else {
			$('#selReasonId').val(0);
		}		
		ReasonCatalog.getReasonGroup(activeType.RUNNING);
		setTextboxValue('reasonCode',code);
		setTextboxValue('reasonName',name);
		setTextboxValue('description',description);		
		setSelectBoxValue('status', status);	
		//ReasonCatalog.getChangedForm();
		$('.RequireStyle').show();
		$('#btnSearch').hide();
		$('#btnAdd').hide();
		$('#btnEdit').show();
		$('#btnCancel').show();
		$('#code').focus();
		setTitleUpdate();
		return false;
	},
	resetForm: function(){
		ReasonCatalog.resetAllControl();
		$("#grid").trigger("reloadGrid");
		return false;
	},
	deleteReason: function(reasonCode){
		ReasonCatalog.resetCombobox();
		var dataModel = new Object();
		dataModel.code = reasonCode;	
		Utils.deleteSelectRowOnGrid(dataModel, 'lý do', '/catalog/reason/delete', ReasonCatalog._xhrDel, null, null);		
		return false;
	},
	resetAllControl: function(){		
		setTitleSearch();
		ReasonCatalog._curRowId = null;
		$('.RequireStyle').hide();
		$('#selReasonId').val(0);
		$('#errMsg').html('').hide();
		$('#reasonCode').removeAttr('disabled');
		$('#btnEdit').hide();
		$('#btnCancel').hide();
		$('#btnSearch').show();
		$('#btnAdd').show();
		$('#reasonCode').val('');
		$('#reasonName').val('');
		$('#description').val('');
		//ReasonCatalog.resetCombobox();
		ReasonCatalog.getReasonGroup(activeType.ALL);
		setSelectBoxValue('status', 1);	
		setSelectBoxValue('reasonGroup',-2);
		ReasonCatalog.searchReason();
	},
	resetCombobox: function(){
		if(ReasonCatalog._htmlReasonGroup!= null){
			$('#reasonGroup').html(ReasonCatalog._htmlReasonGroup);
			$('#reasonGroup').change();
		}
	},
	getChangedForm: function(){
		ReasonCatalog._curRowId = null;
		ReasonCatalog.getReasonGroup(activeType.RUNNING);
		$('.RequireStyle').show();
		$('#btnSearch').hide();
		$('#btnAdd').hide();
		$('#btnEdit').show();
		$('#btnCancel').show();
		$('#code').focus();
		setTitleAdd();
	},
	getReasonGroup: function(status){
		$.getJSON('/rest/catalog/reason-group/list.json?status=' + status, function(data){
			var arrHtml = new Array();
			arrHtml.push('<option value="'+ activeType.ALL +'">--- Chọn nhóm lý do ---</option>');
			for(var i=0;i<data.length;i++){
				arrHtml.push('<option value="'+ data[i].value +'">'+ Utils.XSSEncode(data[i].name) +'</option>');
			}
			$('#reasonGroup').html(arrHtml.join(""));
			$('#reasonGroup').change();
			if(ReasonCatalog._curRowId!= null && ReasonCatalog._curRowId!= undefined){
				var reasonGroupId =  $("#grid").jqGrid ('getCell', ReasonCatalog._curRowId, 'reasonGroup.id');
				var reasonGroupName =  $("#grid").jqGrid ('getCell', ReasonCatalog._curRowId, 'reasonGroup.reasonGroupName');
				var reasonGroupCode =  $("#grid").jqGrid ('getCell', ReasonCatalog._curRowId, 'reasonGroup.reasonGroupCode');
				if($('#reasonGroup option[value='+ reasonGroupId +']').html()== null){
					var htmlCat = $('#reasonGroup').html() + '<option value="'+ reasonGroupId +'">'+ Utils.XSSEncode(reasonGroupCode) + ' - ' + Utils.XSSEncode(reasonGroupName) + '</option>';
					$('#reasonGroup').html(htmlCat);
					$('#reasonGroup').change();					
				}
				setSelectBoxValue('reasonGroup', reasonGroupId);
			}
		});
	},
	exportExcelData:function(){
		$('#divOverlay').addClass('Overlay');
		$('#imgOverlay').show();
		$('#errMsg').html('').hide();
		$('#errExcelMsg').html('').hide();
		var reasonCode = $('#reasonCode').val().trim();
		var reasonName = $('#reasonName').val().trim();
		var description = $('#description').val().trim();
		var status = $('#status').val().trim();
		var reasonGroupId = $('#reasonGroup').val();
		var data = new Object();
		data.code = reasonCode;
		data.name = reasonName;
		data.note = description;
		data.status = status;
		data.rgId = reasonGroupId;
		var url = "/catalog/reason/getlistexceldata";
		CommonSearch.exportExcelData(data,url);
	}
};
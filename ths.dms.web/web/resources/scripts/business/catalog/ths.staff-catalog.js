var StaffCatalog = {
	_xhrSave : null,
	_xhrDel: null,
	_mapCheckStaff: new Map(),
	_mapStaffCat:new Map(),
	search: function(){
		$('#errMsg').html('').hide();
		var shopCodeHidden = $('#shopCodeHidden').val().trim();
		var staffCode = $('#staffCode').val().trim();
		var staffName = $('#staffName').val().trim();
		var status = $('#status').val();		
		$('#grid').datagrid('load',{page : 1,shopCodeHidden:shopCodeHidden,staffCode:staffCode,staffName:staffName,status:status});
		return false;
	},
	activeTab:function(tab,is_allow){
		$('#tabItem1').bind('click',function(){
			StaffCatalog.activeTab('tab1',is_allow);
		});
		$('#tabItem2').bind('click',function(){
			StaffCatalog.activeTab('tab2',is_allow);
			StaffCatalog.getListSaleCatByStaffId();
		});
		if(tab=='tab1'){
			$('#tabItem1').addClass('Active');
			$('#tabItem2').removeClass('Active');
			$('#tab1').show();
			$('#tab2').hide();			
		}else{
			$('#tabItem2').addClass('Active');
			$('#tabItem1').removeClass('Active');
			$('#tab2').show();
			$('#tab1').hide();			
		}
		if(is_allow!=undefined && is_allow!=null && !is_allow){
			$('#tabItem2').unbind('click');
			$('#tabItem2').removeClass('Active');
		}else if(is_allow==undefined || is_allow==null){
			AttributesManager.getAttributesForm('tab2',AttributesManager.STAFF);
			$('#typeAttribute').val(AttributesManager.STAFF);
		}
		
	},
	changeStaffInfor:function(){
			
		var msg = '';
		$('#errMsg').html('').hide();
		//var staffId = $('#staffId').val();
		
		if(msg.length==0){
			msg = Utils.getMessageOfRequireCheck('staffCode','Mã nhân viên');
		}
		if(msg.length==0){
			msg = Utils.getMessageOfSpecialCharactersValidate('staffCode','Mã nhân viên',Utils._CODE);
		}			
		if(msg.length == 0){
			msg = Utils.getMessageOfRequireCheck('staffName','Tên nhân viên');
		}		
		if(msg.length == 0){
			msg = Utils.getMessageOfSpecialCharactersValidate('staffName','Tên NV',Utils._NAME);
		}		
		if(msg.length == 0){
			msg = Utils.getMessageOfRequireCheck('gender','Giới tính',true);
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfRequireCheck('staffType','Chức vụ',true);
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfSpecialCharactersValidate('idPlace','Nơi cấp');
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfSpecialCharactersValidate('staffPhone','Số di động');
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfSpecialCharactersValidate('staffTelephone','Số cố định');
		}
		if(msg.length == 0){
			//msg = Utils.getMessageOfInvaildNumber('staffPhone','Số di động');
			msg = Utils.getMessageOfInvaildNumberStartWithZero('staffPhone','Số di động');
		}
		if(msg.length == 0){
			//msg = Utils.getMessageOfInvaildNumber('staffTelephone','Số cố định');
			msg = Utils.getMessageOfInvaildNumberStartWithZero('staffTelephone','Số cố định');
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfInvalidEmailFormat('email', 'Email');
		}
		if (msg.length == 0) {
			var prov = $('#province').combobox('getValue');
			if (prov == null || prov.length == 0 || prov == '-2') {
				msg = "Bạn chưa chọn Tỉnh/Thành phố";
			}
			$('#province').next().find('.combo-text').focus();
		}
		
		if (msg.length == 0) {
			var prov = $('#district').combobox('getValue');
			if (prov == null || prov.length == 0 || prov == '-2') {
				msg = "Bạn chưa chọn Quận/Huyện";
			}
			$('#district').next().find('.combo-text').focus();
		}
		
		if (msg.length == 0) {
			var prov = $('#precinct').combobox('getValue');
			if (prov == null || prov.length == 0 || prov == '-2') {
				msg = "Bạn chưa chọn Phường/Xã";
			}
			$('#precinct').next().find('.combo-text').focus();
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfSpecialCharactersValidate('idCard','CMND');
		}		
		if(msg.length == 0){
			if ($('#workStartDate').val().trim() != '' && !Utils.isDate($('#workStartDate').val().trim())) {
				msg = 'Ngày bắt đầu làm việc không hợp lệ, vui lòng nhập theo định dạng dd/MM/yyyy';
				$('#workStartDate').focus();
			}
		}
		if(msg.length == 0){
			var area = $('#areaId').val().trim();
			if (area == null || area == -2) {
				msg = 'Địa bàn là bắt buộc. Xin vui lòng chọn địa bàn';
				$('#areaTree').focus();
			}
		}
		if (msg.length == 0 && $('#idDate').val().trim() != '' && !Utils.isDate($('#idDate').val().trim())) {
			msg = 'Ngày cấp không hợp lệ, vui lòng nhập theo định dạng dd/MM/yyyy';
			$('#idDate').focus();
		}
		if (msg.length == 0 && $('#workStartDate').val().trim() != '' && !Utils.isDate($('#workStartDate').val().trim())) {
			msg = 'Ngày bắt đầu làm việc không hợp lệ, vui lòng nhập theo định dạng dd/MM/yyyy';
			$('#workStartDate').focus();
		}
		if (msg.length == 0 && $('#idDate').val().trim() != '' && !Utils.isDate($('#idDate').val().trim())) {
			msg = 'Ngày bắt đầu làm việc không hợp lệ, vui lòng nhập theo định dạng dd/MM/yyyy';
			$('#idDate').focus();
		}		
		if(msg.length > 0){
			$('#errMsg').html(msg).show();
			return false;
		}
		
		var dataModel = new Object();
		dataModel.staffId = $('#staffId').val().trim();		
		dataModel.staffCode = $('#staffCode').val().trim();
		dataModel.staffName = $('#staffName').val().trim();				
		dataModel.idCard = $('#idCard').val().trim();
		dataModel.idDate = $('#idDate').val().trim();
		dataModel.idPlace = $('#idPlace').val().trim();
		dataModel.areaId = $('#areaId').val().trim();
		dataModel.street = $('#street').val().trim();
		dataModel.address = $('#address').val().trim();
		dataModel.email = $('#email').val().trim();
		dataModel.saleGroup = $('#saleGroup').val().trim();
		dataModel.staffPhone = $('#staffPhone').val().trim();
		dataModel.staffType = $('#staffType').val();
		dataModel.saleTypeCode = $('#saleType').val();
		dataModel.workStateCode = $('#workState').val();
		
		dataModel.status = $('#status').val();
		dataModel.gender = $('#gender').val();
		dataModel.workStartDate = $('#workStartDate').val().trim();
		dataModel.education = $('#education').val().trim();
		dataModel.position = $('#position').val().trim();
		dataModel.staffTelephone = $('#staffTelephone').val().trim();		
		Utils.addOrSaveData(dataModel, "/catalog_staff_manager/saveorupdate", StaffCatalog._xhrSave, 'errMsg',function(data){
			$('#staffId').val(data.staffId);
			if(data.error!=null && !data.error){
				if($('#staffId').val()!= 0){			
					var lstCatId = new Array();
					var lstDelete = new Array();
					$('.InputCbxStyle').each(function(){
						var catId = $(this).val().trim();
						if ($(this).is(':checked') && StaffCatalog._mapStaffCat.get(catId)==null){
							lstCatId.push(catId);
						}else if(!$(this).is(':checked') && StaffCatalog._mapStaffCat.get(catId)!=null){
							lstDelete.push(catId);
						}
					});
					var staffId = $('#infStaffCodeHid').val().trim();
					var params = new Object();		
					params.lstCatId = lstCatId ;
					params.staffId = staffId;
					params.lstDelete = lstDelete;
					Utils.saveData(params, '/catalog_staff_manager/save-staff-sale-cat', null, null, null, null);
				}
				setTimeout(function(){
					window.location.href='/catalog_staff_manager/viewdetail?staffId=' + data.staffId;
				}, 1000);
				$('#imfoCat').hide();
			}
		});		
	},
	showArea:function(selector,type,codeValue){		
		var html = new Array();	
		html.push("<option value=''>Tất cả</option>");
		var areaCode = $(selector).val().trim();		
		if(areaCode!=undefined && areaCode!=null && areaCode.trim().length==0){
			if($(selector).attr('id')=='provinceCode'){
				$('#districtCode').html(html.join(""));
				$('#wardCode').html(html.join("")); 
				$('#districtCode').val('');
				$('#wardCode').val('');
			}
			if($(selector).attr('id')=='districtCode'){
				$('#wardCode').html(html.join(""));
				$('#wardCode').val('');
				/*disableSelectbox('wardCode');*/
			}
			
			return false;
		}
		$.ajax({
			type : "POST",
			url : "/catalog_staff_manager/show-area",
			data : ({areaCode:areaCode}),
			dataType: "json",
			success : function(data) {				
    			if(data!= null && data!= undefined && data.areas!= null && data.areas.length > 0){    				
    				for(var i=0;i<data.areas.length;i++){    					
    					html.push("<option value='"+ Utils.XSSEncode(data.areas[i].areaCode) +"'>" + Utils.XSSEncode(data.areas[i].areaName) +"</option>");
    				}
    				if(type==1){
    					//enable('districtCode');
    					//$('#districtCode').parent().removeClass('BoxDisSelect');
    				}else if(type==2){    					
    					//enable('wardCode');
    					$('#wardCode').parent().removeClass('BoxDisSelect');
    				}    				
    			}    			
    			if(type==1){    				
    				$('#districtCode').html(html.join(""));  
    				if(codeValue!=undefined && codeValue!=null && codeValue.trim().length>0){
    					$('#districtCode').val(codeValue);
    					//disableSelectbox('districtCode');
    				}
    			}else if(type==2){    				
    				$('#wardCode').html(html.join("")); 
    				if(codeValue!=undefined && codeValue!=null && codeValue.trim().length>0){
    					$('#wardCode').val(codeValue);
    					/*disableSelectbox('wardCode');*/
    				}     				    				
    			}    			
			},
			error:function(XMLHttpRequest, textStatus, errorThrown) {				
			}
		});
	},
	getSupervisorGridUrl: function(staffId,shopCode,shopName,staffCode,staffName){
		return "/catalog_staff_manager/searchsupervisor?staffId=" + encodeChar(staffId) + "&shopCode=" + encodeChar(shopCode) + "&shopName=" + encodeChar(shopName) + "&staffCode=" + encodeChar(staffCode) + "&staffName=" + encodeChar(staffName);
	},
	getCategoryGridUrl: function(staffId){
		return "/catalog_staff_manager/searchcategory?staffId="+ encodeChar(staffId);
	},
	getChangedGridUrl: function(staffId,startDate,endDate){
		return "/catalog_staff_manager/searchchanged?staffId="+ encodeChar(staffId) + "&startDate=" + encodeChar(startDate) + "&endDate=" + encodeChar(endDate);
	},
	getDetailChangedGridUrl: function(actionGeneralLogId){
		$('#divDetail').show();
		var url = StaffCatalog.viewDetailChanged(actionGeneralLogId);
		if($('#gbox_detailGrid').html() != null && $('#gbox_detailGrid').html().trim().length > 0){
			$("#detailGrid").setGridParam({url:url,page:1}).trigger("reloadGrid");
		} else {
			$("#detailGrid").jqGrid({
				url:url,
				colModel:[		
				          {name:'columnName', label: 'Thông tin thay đổi', width: 100, sortable:false,resizable:false, align: 'left' },
				          {name:'oldValue', label: 'Giá trị cũ', width: 200, sortable:false,resizable:false, align: 'right' },
				          {name:'newValue', label: 'Giá trị mới', sortable:false,resizable:false, align: 'right'},
				          {name:'actionDate', label: 'Ngày thay đổi', width: 100, align: 'center', sortable:false,resizable:false ,formatter:'date',formatoptions:{srcformat: 'Y-m-d H:i:s',newformat: 'd/m/Y' } },
				          ],	  
				          pager : '#detailPager',
				          height: 'auto',
				          rownumbers: true,	  
				          width: ($('#detailChangedGrid').width()),
				          gridComplete: function(){
							  $('#jqgh_detailGrid_rn').html('STT');
							  updateRownumWidthForJqGrid();
						  }
			})
			.navGrid('#detailPager', {edit:false,add:false,del:false, search: false});
		}
	},
	viewDetailChanged:function(actionGeneralLogId){
		return "/catalog_staff_manager/searchdetailchanged?actionGeneralLogId="+ actionGeneralLogId;
	},
	getHistoryGridUrl: function(staffId,shopCode,startDate,endDate){
		return "/catalog_staff_manager/searchhistory?staffId="+ encodeChar(staffId) + "&shopCode=" + encodeChar(shopCode) + "&startDate=" + encodeChar(startDate) + "&endDate=" + encodeChar(endDate);
	},
	getStaffGridUrlDialog: function(shopCode,shopName,staffCode,staffName, staffId){
		return "/catalog_staff_manager/searchstaffdialog?staffId="+ encodeChar(staffId) + "&shopCode="+ encodeChar(shopCode) + "&shopName=" + encodeChar(shopName) + "&staffCode=" + encodeChar(staffCode) + "&staffName=" + encodeChar(staffName);
	},
	searchSupervisor: function(){
		$('#errMsg').html('').hide();
		var staffId = $('#staffId').val();
		var shopCode = $('#shopCode').val().trim();
		var shopName = $('#shopName').val().trim();
		var staffCode = $('#staffCode').val().trim();
		var staffName = $('#staffName').val().trim();
		var url = StaffCatalog.getSupervisorGridUrl(staffId,shopCode,shopName,staffCode,staffName);
		$("#grid").setGridParam({url:url,page:1}).trigger("reloadGrid");
		$('.ui-paging-info').show();//thong bao cua grid
		return false;
	},
	searchHistory: function(){
		$('#errMsg').html('').hide();
		var staffId = $('#staffId').val().trim();
		var shopCode = $('#shopCode').val().trim();
		var startDate = $('#startDate').val().trim();
		var endDate = $('#endDate').val().trim();
		var url = StaffCatalog.getHistoryGridUrl(staffId,shopCode,startDate,endDate);
		if($('#gbox_grid').html() != null && $('#gbox_grid').html().trim().length > 0){
			$("#grid").setGridParam({url:url,page:1}).trigger("reloadGrid");
		} else {
			$("#grid").jqGrid({
				  url:url,
				  colModel:[		
				    {name:'orderDate', label: 'Ngày bán hàng', width: 100, sortable:false, align: 'center',resizable:false ,formatter:'date',formatoptions:{srcformat: 'Y-m-d H:i:s',newformat: 'd/m/Y' } },
				    {name:'customer.customerName', label: 'KH mua', width: 200, sortable:false,resizable:false, align: 'left' },
				    {name:'orderNumber', label: 'Số đơn hàng', width: 200,sortable:false,resizable:false, align: 'right'},
				    {name:'total', label: 'Số tiền', width: 100, align: 'right', sortable:false,resizable:false, formatter: StaffCatalogFormatter.priceFormat},
				  ],	  
				  pager : '#pager',
				  height: 'auto',
				  rownumbers: true,	  
				  width: ($('#historyGrid').width())	  
				})
				.navGrid('#pager', {edit:false,add:false,del:false, search: false});
				$('#jqgh_grid_rn').prepend('STT');
		}
		return false;
	},
	searchChanged: function(){
		$('#errMsg').html('').hide();
		var staffId = $('#staffId').val().trim();
		var startDate = $('#startDate').val().trim();
		var endDate = $('#endDate').val().trim();
		var url = StaffCatalog.getChangedGridUrl(staffId,startDate,endDate);
		if($('#gbox_grid').html() != null && $('#gbox_grid').html().trim().length > 0){
			$("#grid").setGridParam({url:url,page:1}).trigger("reloadGrid");
		} else {
			$("#grid").jqGrid({
			  url:url,
			  colModel:[		
			    {name:'actionUser', label: 'NV thay đổi', width: 100, sortable:false,resizable:false, align: 'left' },
			    {name:'actionType', label: 'Loại thay đổi', width: 200, sortable:false,resizable:false , align: 'left', formatter: StaffCatalogFormatter.typeFormatter},
			    {name:'actionDate', label: 'Ngày thay đổi', sortable:false,resizable:false, align: 'center' ,formatter:'date',formatoptions:{srcformat: 'Y-m-d H:i:s',newformat: 'd/m/Y' } },
			    {name:'actionIp', label: 'IP máy thay đổi', width: 100, align: 'right', sortable:false,resizable:false},
			    {name:'view', label: 'Xem chi tiết', width: 50, align: 'center',sortable:false,resizable:false, formatter: StaffCatalogFormatter.viewCellIconFormatter},
			  ],	  
			  pager : '#pager',
			  height: 'auto',
			  rownumbers: true,	  
			  width: ($('#changedGrid').width())	  
			})
			.navGrid('#pager', {edit:false,add:false,del:false, search: false});
			$('#jqgh_grid_rn').prepend('STT');
		}
		return false;
	},
	loadInfo:function(){   //thong tin nhan vien tab load info
		$('#tabTitle').html('Thông tin nhân viên');
		$("#infoTab").addClass("Active");
		/*$("#supervisorTab").removeClass("Active");
		$("#categoryTab").removeClass("Active");
		$("#changedTab").removeClass("Active");
		$("#historyTab").removeClass("Active");*/
		var staffId = $("#staffId").val();
		var data = new Object();
		data.staffId = staffId;
		var kData = $.param(data, true);
		$.ajax({
			type : "POST",
			url : "/catalog_staff_manager/info",
			data : (kData),
			dataType: "html",
			success : function(data) {
				$("#divGeneral").html(data).show();
				$('#shopCode').focus();
				Utils.bindAutoSave();
			},
			error:function(XMLHttpRequest, textStatus, errorThrown) {
				$.ajax({
					type : "POST",
					url : '/check-session',
					dataType : "json",
					success : function(data) {
					},
					error: function(XMLHttpRequest, textStatus, errorThrown) {
						window.location.href = '/home';
					}
				});
			}
		});
		return false;
	},
	loadSupervisor:function(){
		$('#tabTitle').html('Giám sát nhân viên bán hàng');
		$("#infoTab").removeClass("Active");
		$("#supervisorTab").addClass("Active");
		$("#categoryTab").removeClass("Active");
		$("#changedTab").removeClass("Active");
		$("#historyTab").removeClass("Active");
		var staffId = $("#staffId").val();
		var data = new Object();
		data.staffId = staffId;
		var kData = $.param(data, true);
		$.ajax({
			type : "POST",
			url : "/catalog_staff_manager/supervisor",
			data : (kData),
			dataType: "html",
			success : function(data) {
				$("#divGeneral").html(data).show();
				$('#shopCode').focus();
				Utils.bindAutoSearch();
			},
			error:function(XMLHttpRequest, textStatus, errorThrown) {
				$.ajax({
					type : "POST",
					url : '/check-session',
					dataType : "json",
					success : function(data) {
					},
					error: function(XMLHttpRequest, textStatus, errorThrown) {
						window.location.href = '/home';
					}
				});
			}
		});
		return false;
	},
	loadCategory:function(flag){
		$('#tabTitle').html('Thông tin ngành hàng');
		$("#infoTab").removeClass("Active");
		$("#supervisorTab").removeClass("Active");
		$("#categoryTab").addClass("Active");
		$("#changedTab").removeClass("Active");
		$("#historyTab").removeClass("Active");
		if (flag) $('#btnCreate').unbind('click');
		var staffId = $("#staffId").val();
		var data = new Object();
		data.staffId = staffId;
		var kData = $.param(data, true);
		$.ajax({
			type : "POST",
			url : "/catalog_staff_manager/category",
			data : (kData),
			dataType: "html",
			success : function(data) {
				$("#divGeneral").html(data).show();
			},
			error:function(XMLHttpRequest, textStatus, errorThrown) {
				$.ajax({
					type : "POST",
					url : '/check-session',
					dataType : "json",
					success : function(data) {
					},
					error: function(XMLHttpRequest, textStatus, errorThrown) {
						window.location.href = '/home';
					}
				});
			}
		});
		return false;
	},
	loadChanged:function(){
		$('#tabTitle').html('Xem thông tin thay đổi');
		$("#infoTab").removeClass("Active");
		$("#supervisorTab").removeClass("Active");
		$("#categoryTab").removeClass("Active");
		$("#changedTab").addClass("Active");
		$("#historyTab").removeClass("Active");
		var staffId = $("#staffId").val();
		var data = new Object();
		data.staffId = staffId;
		var kData = $.param(data, true);
		$.ajax({
			type : "POST",
			url : "/catalog_staff_manager/changed",
			data : (kData),
			dataType: "html",
			success : function(data) {
				$("#divGeneral").html(data).show();
				$('#startDate').focus();
				Utils.bindAutoSearch();
			},
			error:function(XMLHttpRequest, textStatus, errorThrown) {
				$.ajax({
					type : "POST",
					url : '/check-session',
					dataType : "json",
					success : function(data) {
					},
					error: function(XMLHttpRequest, textStatus, errorThrown) {
						window.location.href = '/home';
					}
				});
			}
		});
		return false;
	},
	loadHistory:function(){
		$('#tabTitle').html('Lịch sử bán hàng');
		$("#infoTab").removeClass("Active");
		$("#supervisorTab").removeClass("Active");
		$("#categoryTab").removeClass("Active");
		$("#changedTab").removeClass("Active");
		$("#historyTab").addClass("Active");
		var staffId = $("#staffId").val();
		var data = new Object();
		data.staffId = staffId;
		var kData = $.param(data, true);
		$.ajax({
			type : "POST",
			url : "/catalog_staff_manager/history",
			data : (kData),
			dataType: "html",
			success : function(data) {
				$("#divGeneral").html(data).show();
				$('#startDate').focus();
				Utils.bindAutoSearch();
			},
			error:function(XMLHttpRequest, textStatus, errorThrown) {
				$.ajax({
					type : "POST",
					url : '/check-session',
					dataType : "json",
					success : function(data) {
					},
					error: function(XMLHttpRequest, textStatus, errorThrown) {
						window.location.href = '/home';
					}
				});
			}
		});
		return false;
	},
	changeStaffInfo: function(){
		var msg = '';
		$('#errMsg').html('').hide();
		msg = Utils.getMessageOfRequireCheck('staffCode','Mã nhân viên');
		if(msg.length==0){
			msg = Utils.getMessageOfSpecialCharactersValidate('staffCode','Mã nhân viên',Utils._CODE);
		}		
		if(msg.length == 0){
			msg = Utils.getMessageOfRequireCheck('staffName','Tên nhân viên');
		}		
		if(msg.length == 0){
			msg = Utils.getMessageOfSpecialCharactersValidate('staffName','Tên NV',Utils._NAME);
		}		
		if(msg.length == 0){
			msg = Utils.getMessageOfRequireCheck('gender','Giới tính',true);
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfRequireCheck('provinceCode','Tỉnh',true);
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfRequireCheck('districtCode','Quận/Huyện',true);
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfRequireCheck('wardCode','Phường/Xã',true);
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfRequireCheck('staffType','Loại nhân viên',true);
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfSpecialCharactersValidate('idPlace','Nơi cấp');
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfSpecialCharactersValidate('staffPhone','Số di động');
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfSpecialCharactersValidate('staffTelephone','Số cố định');
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfInvaildNumber('staffPhone','Số di động');
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfInvaildNumber('staffTelephone','Số cố định');
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfInvalidEmailFormat('email', 'Email');
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfSpecialCharactersValidate('saleGroup', 'Nhóm bán hàng');
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfSpecialCharactersValidate('idCard','CMND');
		}		
		if(msg.length == 0){
			if ($('#workStartDate').val().trim() != '' && !Utils.isDate($('#workStartDate').val().trim())) {
				msg = 'Ngày bắt đầu làm việc không hợp lệ, vui lòng nhập theo định dạng dd/MM/yyyy';
				$('#workStartDate').focus();
			}
		}
		if (msg.length == 0 && $('#idDate').val().trim() != '' && !Utils.isDate($('#idDate').val().trim())) {
			msg = 'Ngày cấp không hợp lệ, vui lòng nhập theo định dạng dd/MM/yyyy';
			$('#idDate').focus();
		}
		if (msg.length == 0 && $('#workStartDate').val().trim() != '' && !Utils.isDate($('#workStartDate').val().trim())) {
			msg = 'Ngày bắt đầu làm việc không hợp lệ, vui lòng nhập theo định dạng dd/MM/yyyy';
			$('#workStartDate').focus();
		}
		if (msg.length == 0 && $('#idDate').val().trim() != '' && !Utils.isDate($('#idDate').val().trim())) {
			msg = 'Ngày bắt đầu làm việc không hợp lệ, vui lòng nhập theo định dạng dd/MM/yyyy';
			$('#idDate').focus();
		}		
		if(msg.length > 0){
			$('#errMsg').html(msg).show();
			return false;
		}
		var dataModel = new Object();
		dataModel.staffId = $('#staffId').val();
		dataModel.shopCode = $('#shopCode').val().trim();
		dataModel.staffCode = $('#staffCode').val().trim();
		dataModel.staffName = $('#staffName').val().trim();
		dataModel.staffOwnerCode = $('#staffOwnerCode').val().trim();
		dataModel.staffOwnerName = $('#staffOwnerName').val().trim();
		dataModel.idCard = $('#idCard').val().trim();
		dataModel.idDate = $('#idDate').val().trim();
		dataModel.idPlace = $('#idPlace').val().trim();
		dataModel.provinceCode = $('#provinceCode').val().trim();
		dataModel.districtCode = $('#districtCode').val().trim();
		dataModel.wardCode = $('#wardCode').val().trim();
		dataModel.street = $('#street').val().trim();
		dataModel.address = $('#address').val().trim();
		dataModel.email = $('#email').val().trim();
		dataModel.saleGroup = $('#saleGroup').val().trim();
		dataModel.staffPhone = $('#staffPhone').val().trim();
		dataModel.staffType = $('#staffType').val();
		dataModel.saleTypeCode = $('#saleType').val();
		dataModel.status = $('#status').val();
		dataModel.gender = $('#gender').val();
		dataModel.workStartDate = $('#workStartDate').val().trim();
		dataModel.education = $('#education').val().trim();
		dataModel.position = $('#position').val().trim();
		dataModel.staffTelephone = $('#staffTelephone').val().trim();
		
		Utils.addOrSaveData(dataModel, "/catalog_staff_manager/changestaffinfo", StaffCatalog._xhrSave, null,function(data){
			$('#staffId').val(data.staffId);
			if(data.status == 1){
				if(data.supervisorStaffValue == 5){
					$('#supervisorTab').removeClass('Disable');
					$('#supervisorTab').bind('click',StaffCatalog.loadSupervisor);
					StaffCatalog.loadSupervisor();
				}else{
					$('#supervisorTab').addClass('Disable');
					$('#supervisorTab').unbind('click');
					StaffCatalog.loadCategory();
				}							
			}else{
				if(data.supervisorStaffValue == 1 || data.supervisorStaffValue == 2){
					$('#staffOwnerCode').removeAttr('disabled');
				}else{
					$('#staffOwnerCode').attr('disabled','disabled');
				}
				StaffCatalog.loadInfo();
			}
		});
		
		
		/*if(!Utils.validateAttributeData(dataModel, '#errMsg')){
			return false;
		}
		$.ajax({
			type : "POST",
			url : "/catalog_staff_manager/checkexistsupervisor",
			data : ({staffId:$('#staffId').val(),shopCode:$('#shopCode').val().trim(), staffType:$('#staffType').val()}),
			dataType: "json",
			success : function(data) {
				if(data.error == true){
					if(data.type == 0){//chuyen 
						$.messager.confirm('Xác nhận', 'Nhân viên này đang giám sát NVBH.Sau khi chuyển đơn vị bạn có muốn giữ lại danh sách giám sát này không? ', function(r){
							if (r == true){
								dataModel.isSupervisorStaff = 1;
							}else{
								dataModel.isSupervisorStaff = 0;
							}
							Utils.saveData(dataModel, "/catalog_staff_manager/changestaffinfo", StaffCatalog._xhrSave, null, function(data){
								$('#staffId').val(data.staffId);
								if(data.status == 1){
									if(data.supervisorStaffValue == 5){
										$('#supervisorTab').removeClass('Disable');
										$('#supervisorTab').bind('click',StaffCatalog.loadSupervisor);
										StaffCatalog.loadSupervisor();
									}else{
										$('#supervisorTab').addClass('Disable');
										$('#supervisorTab').unbind('click');
										StaffCatalog.loadCategory();
									}
									$('#categoryTab').removeClass('Disable');
									$('#changedTab').removeClass('Disable');
									$('#historyTab').removeClass('Disable');
									$('#categoryTab').bind('click',StaffCatalog.loadCategory);
									$('#changedTab').bind('click',StaffCatalog.loadChanged);
									$('#historyTab').bind('click',StaffCatalog.loadHistory);
								}else{
									if(data.supervisorStaffValue == 1 || data.supervisorStaffValue == 2){
										$('#staffOwnerCode').removeAttr('disabled');
									}else{
										$('#staffOwnerCode').attr('disabled','disabled');
									}							
								}
							});
						});	
					}else{
						var title = '';
						if(data.type == 1){
							title = 'Nhân viên này đang giám sát NVBH, bạn có muốn chuyển không?';
						}else{
							title = 'Bạn đang thực hiện điều chuyển đơn vị cho NVBH xin vui lòng chọn lại chính xác nhân viên quản lý của nhân viên này. Bạn đã chọn chính xác chưa?';
						}
						$.messager.confirm('Xác nhận',title, function(r) {
							if(r == true) {
								Utils.saveData(dataModel, '/catalog_staff_manager/changestaffinfo', StaffCatalog._xhrSave, null, function(data) {
									$('#staffId').val(data.staffId);
									if(data.status == 1){
										if(data.supervisorStaffValue == 5){
											$('#supervisorTab').removeClass('Disable');
											$('#supervisorTab').bind('click',StaffCatalog.loadSupervisor);
											StaffCatalog.loadSupervisor();
										}else{
											$('#supervisorTab').addClass('Disable');
											$('#supervisorTab').unbind('click');
											StaffCatalog.loadCategory();
										}
										$('#categoryTab').removeClass('Disable');
										$('#changedTab').removeClass('Disable');
										$('#historyTab').removeClass('Disable');
										$('#categoryTab').bind('click',StaffCatalog.loadCategory);
										$('#changedTab').bind('click',StaffCatalog.loadChanged);
										$('#historyTab').bind('click',StaffCatalog.loadHistory);
									}else{
										if(data.supervisorStaffValue == 1 || data.supervisorStaffValue == 2){
											$('#staffOwnerCode').removeAttr('disabled');
										}else{
											$('#staffOwnerCode').attr('disabled','disabled');
										}
										StaffCatalog.loadInfo();
									}
								});
							}
						});
					} 
				}else{
					
				}
			},
			error:function(XMLHttpRequest, textStatus, errorThrown) {
				$.ajax({
					type : "POST",
					url : '/check-session',
					dataType : "json",
					success : function(data) {
						$('#loading').css('visibility','hidden');
					},
					error: function(XMLHttpRequest, textStatus, errorThrown) {
						window.location.href = '/home';
					}
				});
			}
		});*/
		return false;
	},
	getSelectedInfo: function(id,shopCode,quantityMax){
		if(id!= null && id!= 0 && id!=undefined){
			$('#shopId').val(id);
		} else {
			$('#shopId').val(0);
		}
		setTextboxValue('shopCode',shopCode);
		setTextboxValue('quantityMax',quantityMax);
		$('#btnCreate,#btnSearch').hide();
		$('#btnUpdate').show();
	},
	getSelectedCategory: function(id){
		if(id!= null && id!= 0 && id!=undefined){
			$('#categoryId').val(id);
		} else {
			$('#categoryId').val(0);
		}
		setSelectBoxValue('category', id);
		$('#btnCreate').hide();
		$('#btnUpdate').show();
	},
	deleteCategory: function(categoryId){
		var dataModel = new Object();
		dataModel.categoryId = categoryId;	
		Utils.deleteSelectRowOnGrid(dataModel, 'ngành hàng', "/catalog_staff_manager/removecategory", StaffCatalog._xhrDel, null, null);		
		return false;		
	},
	changeStaffCategory: function(){
		var msg = '';
		$('#errMsg').html('').hide();
		var lstSize = $('#lstSize').val();
		var lstSaleCatSize = $('#lstSaleCatSize').val();
		var arrId =new Array();
		var arrDeleteId =new Array();
		var num = 0;
		for(var i=0;i<lstSize;i++){
			if($('#category_'+i).is(':checked')){
				num++;
				var flag = true;
				for(var j=0;j<lstSaleCatSize;j++){
					if(parseInt($('#categorySelected_'+j).val()) == parseInt($('#categoryId_'+i).val())){
						flag = false;
						break;
					}
				}
				if(flag){
					arrId.push($('#categoryId_'+i).val());
				}
			}else{
				if(lstSaleCatSize != null && lstSaleCatSize > 0){
					for(var j=0;j<lstSaleCatSize;j++){
						if(parseInt($('#categorySelected_'+j).val()) == parseInt($('#categoryId_'+i).val())){
							arrDeleteId.push($('#categoryId_'+i).val());
						}
					}
				}
			}
		}
		if(num == 0){
			msg = 'Bạn chưa chọn ngành hàng nào.Hãy chọn ít nhất một ngành hàng để thao tác';
		}
		if(msg.length > 0){
			$('#errMsg').html(msg).show();
			return false;
		}
		var dataModel = new Object();
		dataModel.staffId = $('#staffId').val();
		dataModel.lstCategoryId = arrId;
		dataModel.lstDeleteCategoryId = arrDeleteId;
		Utils.addOrSaveData(dataModel, "/catalog_staff_manager/changestaffcategory", StaffCatalog._xhrSave, null, function(){
			StaffCatalog.loadCategory();
		});
		return false;
	},
	loadTree: function(){
		var shopCode = $('#shopCodeHidden').val().trim();
		loadDataForTree('/rest/catalog/unit-tree/'+shopCode+'/tree.json');
		$('#tree').bind("loaded.jstree", function(event, data){
			if($('#treeContainer').data("jsp")!= undefined){
				$('#treeContainer').data("jsp").destroy();
			}
			$('#treeContainer').jScrollPane();
		});
		$('#tree').bind("select_node.jstree", function (event, data) {
			var name = data.inst.get_text(data.rslt.obj);
			var id = data.rslt.obj.attr("id");
			var code = data.rslt.obj.attr("contentItemId");
			var url = StaffCatalog.getGridUrl(code,'','','','','',-1,'','');
			$("#grid").setGridParam({url:url,page:1}).trigger("reloadGrid");
	    });
	},
	getSupervisorStaffName:function(typeCode,obj){
		$.getJSON('/rest/catalog/supervisor-staff/name/' + encodeChar(typeCode) + ".json",function(data){
			$(obj).val(data.name);
		});
	},
	checkProvinceCode:function(provinceCode,obj){
		$.getJSON('/rest/catalog/province/code/' + encodeChar(provinceCode) + ".json",function(data){
			if(data.value != null && data.value != 0){
				$(obj).removeAttr('disabled');
				$(obj).focus();
				$('#districtCode').bind('keyup', function(event){
					if(event.keyCode == keyCode_F9){
						var lstParam = new Array();
						var obj = {};
						obj.name = 'provinceCode';
						obj.value = $('#provinceCode').val().trim();
						lstParam.push(obj);
						CommonSearch.searchDistrictOnDialog(function(data){
							$('#districtCode').val(data.code);
							$('#wardCode').val("");
							$('#wardCode').removeAttr('disabled');
							$('#wardCode').focus();
						},lstParam);
					}
				});
			}
		});
	},
	checkDistrictCode:function(districtCode,obj){
		$.getJSON('/rest/catalog/district/code/' + encodeChar(districtCode) + ".json",function(data){
			if(data.value != null && data.value != 0){
				$(obj).removeAttr('disabled');
				$(obj).focus();
				$('#wardCode').bind('keyup', function(event){
					if(event.keyCode == keyCode_F9){
						var lstParam = new Array();
						var objP = {};
						objP.name = 'provinceCode';
						objP.value = $('#provinceCode').val().trim();
						var objD = {};
						objD.name = 'districtCode';
						objD.value = $('#districtCode').val().trim();
						lstParam.push(objP);
						lstParam.push(objD);
						CommonSearch.searchWardOnDialog(function(data){
							$('#wardCode').val(data.code);
						},lstParam);
					}
				});
			}
		});
	},
	showDialogStaff: function(){
		var html = $('#staffDialog').html();
		var staffId = $('#staffId').val();
		StaffCatalog._mapCheckStaff = new Map();
		$.fancybox(html,
				{
					modal: true,
					title: 'Thông tin NVBH',
					autoResize: false,
					afterShow: function(){
						$('#staffDialog').html('');
						$("#gridStaff").jqGrid({
							  url:StaffCatalog.getStaffGridUrlDialog('','','','', $('#staffId').val()),
							  colModel:[
					            {name:'staffCode', label: 'Mã NVBH', width: 120, sortable:false,resizable:false , align: 'left'},
					    	    {name:'staffName', label: 'Tên NVBH', width: 150, align: 'left', sortable:false,resizable:false },
					    	    {name:'shop.shopCode', label: 'Mã đơn vị', width: 80, sortable:false,resizable:false , align: 'left'},
					    	    {name:'shop.shopName', label: 'Tên đơn vị', width: 200, align: 'left', sortable:false,resizable:false },
					    	    {name: 'id',index:'id', hidden:true },
							  ],	  
							  pager : $('#pagerStaff'),
							  height: 'auto',
							  multiselect: true,
							  recordpos: 'right',
							  rowNum: 10,	
							  width: ($('#staffInfoGrid').width()),
							  beforeSelectRow: function (id, e) {
								  var _id = 'jqg_gridStaff_' + id;
								  var isCheck = StaffCatalog._mapCheckStaff.get(_id);
								  if(isCheck || isCheck == 'true') {
									  //da co trong map roi->remove no di
									  StaffCatalog._mapCheckStaff.remove(_id);
									  var tm = setTimeout(function(){
										  $('#'+_id).removeAttr('checked');
										  $('#'+id).removeClass('ui-state-highlight');
									  }, 100);
								  } else {
									  //chua co trong map -> put vao
									  StaffCatalog._mapCheckStaff.put(_id, id);
									  var tm = setTimeout(function(){
										  $('#'+_id).attr('checked', 'checked');
										  $('#'+id).addClass('ui-state-highlight');
									  }, 100);
								  }
								  return true;
							  },
							  onSelectAll: function(aRowids, status) {
								  for(var i = 0; i < aRowids.length; i++) {
									  if(status) {
										  var _id = 'jqg_gridStaff_' + aRowids[i];
										  //put vao map
										  StaffCatalog._mapCheckStaff.put(_id, aRowids[i]);
									  } else {
										  var _id = 'jqg_gridStaff_' + aRowids[i];
										  StaffCatalog._mapCheckStaff.remove(_id);
									  }
								  }
							  },
							  gridComplete: function(){
								  $('.cbox').each(function() {
									 var isCheck = StaffCatalog._mapCheckStaff.get(this.id);
									 if(isCheck || isCheck == 'true') {
										 staffId = $("#gridStaff").jqGrid('getCell',$(this).parent().parent().attr('id'),'id');
										 $(this).attr('checked', 'checked');										 
										 $('#'+staffId).addClass('ui-state-highlight');
									 }
								  });
								 
								  $('#jqgh_gridStaff_rn').html('STT');
								  $(window).resize();
								  $.fancybox.update();
								  $(window).resize();
								  //$('#gview_list4>div.ui-jqgrid-bdiv').jScrollPane();
								  updateRownumWidthForJqGrid();
								  
							  }
							})
							.navGrid($('#pagerStaff'), {edit:false,add:false,del:false, search: false});						
						$('#shopCodeDlg').bind('keyup', function(event){
							
							if(event.keyCode == keyCode_F9){
								var arrParams = new Array();
								var params = new Object();
								params.name = 'filterShop';
								params.value = 1;
								arrParams.push(params);
								CommonSearch.searchShopOnEasyUI(function(data){
									$('#shopCodeDlg').val(data.code);
									$('#shopNameDlg').val(data.name);
								}, arrParams,'searchStyle1EasyUI');
							}
						});
						$('#staffCodeDlg').bind('keyup', function(event){
							if(event.keyCode == keyCode_F9){
								var arrParams = new Array();
								var params = new Object();
								params.name = 'shopCode';
								params.value = $('#shopCodeDlg').val().trim();
								arrParams.push(params);
								CommonSearch.searchStaffOnEasyUI(function(data){
									$('#staffCodeDlg').val(data.code);
									$('#staffNameDlg').val(data.name);
								}, arrParams,'searchStyle1EasyUI');
							}
						});
						$('#btnDlgStaff').bind('click',function(event){
							var shopCode = $('#shopCodeDlg').val().trim();
							var shopName = $('#shopNameDlg').val().trim();
							var staffCode = $('#staffCodeDlg').val().trim();
							var staffName = $('#staffNameDlg').val().trim();
							var url = StaffCatalog.getStaffGridUrlDialog(shopCode,shopName,staffCode,staffName, $('#staffId').val());
							$("#gridStaff").setGridParam({url:url,page:1}).trigger("reloadGrid");
						});
					},
					afterClose: function(){
						$('#staffDialog').html(html);
						StaffCatalog._mapCheckStaff = new Map();
					}
				}
			);
		return false;
	},
	selectListStaff: function(){
		$('#errMsgCustomerDlg').html('').hide();
		var arrId = new Array();
		var supervisorId = $('#staffId').val();
		var staffId = '';
		/*$('.fancybox-inner .ui-jqgrid-bdiv .cbox').each(function(){
			if($(this).is(':checked')){
				staffId = $("#gridStaff").jqGrid('getCell',$(this).parent().parent().attr('id'),'id');
				arrId.push(staffId);
			}
		});*/
		var msg = '';
		for(var i = 0; i < StaffCatalog._mapCheckStaff.keyArray.length; i++) {
			arrId.push(StaffCatalog._mapCheckStaff.get(StaffCatalog._mapCheckStaff.keyArray[i]));
		}
		if(arrId.length == 0){
			msg = format(msgErr_required_field,'Thông tin NVBH');
		}
		if(msg.length > 0){
			$('#errMsgCustomerDlg').html(msg).show();
			$.fancybox.update();
			 $(window).resize();
			return false;
		}
		var dataModel = new Object();		
		dataModel.lstStaffId = arrId;
		dataModel.staffId = supervisorId;
		Utils.addOrSaveData(dataModel, '/catalog_staff_manager/changestaffsupervisor', StaffCatalog._xhrSave, 'errMsgCustomerDlg', function(data){
			$('#successMsgCustomerDlg').html('Lưu dữ liệu thành công').show();
			$('#shopCode').val('');
			$('#shopName').val('');
			$('#staffCode').val('');
			$('#staffName').val('');
			StaffCatalog.loadSupervisor();
			$.fancybox.close();
		}, null, '.fancybox-inner', null, null);
		return false;
	},
	deleteSupervisor: function(staffId){
		var dataModel = new Object();
		dataModel.staffId = staffId;
		Utils.deleteSelectRowOnGrid(dataModel, 'NVBH', "/catalog_staff_manager/removesupervisor", StaffCatalog._xhrDel, null, null,function(){
			StaffCatalog.loadSupervisor();
		});		
		return false;		
	},
	loadTreeEx:function(){
		StaffCatalog.loadShopTree();
		$('#tree').bind("loaded.jstree", function(event, data){
        	if($('#treeContainer').data("jsp")!= undefined){
        		$('#treeContainer').data("jsp").destroy();
        	}
        	$('#treeContainer').jScrollPane();
        });
	},
	loadShopTree:function(){
		$('#tree').jstree({
	        "plugins": ["themes", "json_data","ui"],
	        "themes": {
	            "theme": "classic",
	            "icons": false,
	            "dots": true
	        },
	        "json_data": {
	            "ajax": {
	            	"url": function( node ){
	                      if( node == -1 ){
	                    	  return "/rest/catalog/unit-tree-ex/"+$('#shopIdHidden').val().trim()+"/0/tree.json";
	                      } else {
	                    	  return "/rest/catalog/unit-tree-ex/"+node.data("attr").id+"/1/tree.json";
	                      }
	                    },
	                "type": 'GET',
	                "success": function(ops) {
	                    data = [];
	                    for( opnum in ops ){
	                      var op = ops[opnum];
	                      node = {
	                          "data" : op.data,
	                          "attr" : op.attr,
	                          "metadata" :  op ,
	                          "state"	: op.state
	                          /*"state" : "closed"*//*PhuT*/
	                      };
	                      data.push( node );
	                    }
	                    return data;
	                },
	                'complete':function(){
	                	$('#tree').bind("select_node.jstree", function (event, data) {
	            			var isAll = false;
	            			if($('#isAll').is(':checked')){
	            				isAll = true;
	            			}
	            			var name = data.inst.get_text(data.rslt.obj);
	            			var id = data.rslt.obj.attr("id");

	            			var shopCode = data.rslt.obj.attr("contentItemId");
	            			
	            			var url = StaffCatalog.getGridUrl(shopCode,'','','',-1,1,-1,'','');
	            			$("#grid").setGridParam({url:url,page:1}).trigger("reloadGrid"); 
	            			$('#parentCode').val(id);	            			
	            			/*var _url='/rest/catalog/shop-tree-ex/combotree/'+$('#shopCodeHidden').val().trim()+'/1/1/'+id+'.json';
	            			UnitTreeCatalog.generalFunc(_url);
	            			var t = $('#parentCode').combotree('tree');		            			            			
	            			$('#parentCode').combotree('setValue',name);	            			
	            			$('#parentCodeS').val(id);*/
	            			
	            	    });
	                	$('#tree').bind("loaded.jstree", function(event, data){
	                    	if($('#treeContainer').data("jsp")!= undefined){
	                    		$('#treeContainer').data("jsp").destroy();
	                    	}
	                    	$('#treeContainer').jScrollPane();
	                    });
	                	var tm = setTimeout(function() {
		                    if($('#treeContainer').data("jsp")!= undefined){
		                		$('#treeContainer').data("jsp").destroy();
		                	}
		                    $('#treeContainer').css('height',$('.Content').height()-75);
		  				  	$('#treeContainer').jScrollPane();
	                    }, 500);
	                }
	            }
	        },
	        "core" : {"load_open" : true }
		});	
	},
	getStaffType:function(typeCode,obj){
		$.getJSON('/rest/catalog_staff_manager/gettype/' + encodeChar(typeCode) + ".json",function(data){
			if(data.value != '' && data.value > 0){
				$(obj).removeAttr('disabled');
			}else{
				$('#staffOwnerName').val('');
				$(obj).val('');
				$(obj).attr('disabled','disabled');
			}
		});
	},
	gotoTab: function(tabIndex){
		StaffCatalog.deactiveAllMainTab();
		switch (tabIndex) {
		case 0:
			$('#tabContent1').show();
			$('#tab1 a').addClass('Active');
			break;
		case 1:
			$('#tabContent2').show();
			$('#tab2 a').addClass('Active');
			AttributesManager.getAttributesForm('tabContent2',AttributesManager.STAFF);
			$('#typeAttribute').val(AttributesManager.STAFF);
			break;
		default:
			$('#tabContent1').show();
			break;
		}
	},	
	deactiveAllMainTab: function(){
		$('#tab1 a').removeClass('Active');
		$('#tab2 a').removeClass('Active');
		$('#tabContent1').hide();
		$('#tabContent2').hide();
	},
	importExcel:function(){
 		$('#isView').val(0);
 		var options = { 
				beforeSubmit: ProductLevelCatalog.beforeImportExcel,   
		 		success:      ProductLevelCatalog.afterImportExcelUpdate, 
		 		type: "POST",
		 		dataType: 'html',
		 		data:({excelType:$('#excelType').val()})
		 	}; 
		$('#importFrm').ajaxForm(options);
 		$('#importFrm').submit();
 		return false;
 	},	
 	viewExcel:function(){
 		$('#isView').val(1);
 		var options = { 
				beforeSubmit: ProductLevelCatalog.beforeImportExcel,   
		 		success:      ProductLevelCatalog.afterImportExcelUpdate, 
		 		type: "POST",
		 		dataType: 'html',
		 		data:({excelType:$('#excelType').val()})
		 	}; 
		$('#importFrm').ajaxForm(options);
		$('#importFrm').submit();
 		return false;
 	},
 	exportActionLog: function(){
		var staffId = $('#staffId').val().trim();
		var startDate = $('#startDate').val().trim();
		var endDate = $('#endDate').val().trim();
		ExportActionLog.exportActionLog(staffId, ExportActionLog.STAFF, startDate, endDate,'errMsgActionLog');
	},
 	//CuongND
 	exportExcel:function(){
		$('#errMsg').html('').hide();
 		var shopCodeHidden = $('#shopCodeHidden').val().trim();
		var staffCode = $('#staffCode').val().trim();
		var staffName = $('#staffName').val().trim();
		var staffPhone = $('#staffPhone').val().trim();
		var staffType = $('#staffType').val();
		var status = $('#status').val();
		var saleTypeCode = $('#saleType').val();
		var shopCode = $('#shopCode').val().trim();
		var shopName = $('#shopName').val().trim();
		var excelType = $('#excelType').val().trim();
		var saleType = $('#saleType').val().trim();
		
		var params = new Object();
		params.shopCodeHidden = shopCodeHidden;
		params.staffCode = staffCode;
		params.staffName = staffName;
		params.staffPhone = staffPhone;
		params.saleTypeCode = saleTypeCode;
		params.status =status;
		params.saleType =saleType;
		params.shopCode = shopCode;
		params.shopName = shopName;
		params.excelType = excelType;
		params.staffType = staffType;
		
		var url = "/catalog_staff_manager/export-excel";
		CommonSearch.exportExcelData(params,url);
 		return false;
 	},
 	getListSaleCatByStaffId:function(){
 		var data = new Object();
		data.staffId = $('#infStaffCodeHid').val().trim();	
		var kData = $.param(data,true);
 		$.ajax({
			type : "POST",
			url : "/catalog_staff_manager/get-list",
			data :(kData),
			dataType: "json",
			success : function(result) {
				if(result.lst!=null){
					StaffCatalog._mapStaffCat = new Map();
					for(var i=0;i<result.lst.length;i++){
						StaffCatalog._mapStaffCat.put(result.lst[i].cat.id,result.lst[i].cat.id);
						$('.InputCbxStyle').each(function(){
							if($(this).val() == result.lst[i].cat.id){
								$(this).attr('checked',true);
							}
						});
					}
				}
			}
 		});
	},
 	saveStaffSaleCat: function() {
		var msg = '';
		var lstCatId = new Array();
		var lstDelete = new Array();
		$('.InputCbxStyle').each(function(){
			var catId = $(this).val().trim();
			if ($(this).is(':checked') && StaffCatalog._mapStaffCat.get(catId)==null){
				lstCatId.push(catId);
			}else if(!$(this).is(':checked') && StaffCatalog._mapStaffCat.get(catId)!=null){
				lstDelete.push(catId);
			}
		});
		var staffId = $('#infStaffCodeHid').val().trim();
		var params = new Object();		
		params.lstCatId = lstCatId ;
		params.staffId = staffId;
		params.lstDelete = lstDelete;
		Utils.addOrSaveData(params, '/catalog_staff_manager/save-staff-sale-cat', null, 'errMsg', function(data) {		
				$('#successMsg').html('Lưu dữ liệu thành công').show();
				var tm = setTimeout(function(){
					$('#successMsg').html('').hide();
					clearTimeout(tm);
				}, 2000);
		}, 'adjustLoading');
	},
	bindAutoUpdate: function(){
		var check = false;
		if (!check){
			$('.easyui-window .InputTextStyle').each(function(){
			    if(!$(this).is(':hidden')){
			    	check=true;			    			    	
			   		$(this).bind('keyup',function(event){
			   			if(event.keyCode == keyCodes.ENTER){		   				
			   				$('.easyui-window .BtnSearchOnDialog').each(function(){	   					
			   					if(!$(this).is(':hidden')){
			   						$(this).click();
			   					}
			   				});
			   			}
			   		});    
			    }	    
			});
		}
		if (!check){
			$('.easyui-dialog .panel .InputTextStyle').each(function(){
			    if(!$(this).is(':hidden')){
			    	check=true;
			   		$(this).bind('keyup',function(event){
			   			if(event.keyCode == keyCodes.ENTER){		   				
			   				$('.easyui-dialog .BtnSearchOnDialog').each(function(){	   					
			   					if(!$(this).is(':hidden')){
			   						$(this).click();
			   					}
			   				});
			   			}
			   		});    
			    }	    
			});
		}
		if (!check){
			$('.ContentSection .InputTextStyle,.SearchSection .InputTextStyle').each(function(){
			    if(!$(this).is(':hidden')){
			   		$(this).bind('keyup',function(event){
			   			if(event.keyCode == keyCodes.ENTER){		   				
			   				if($('#btnCapNhat')!= null && $('#btnCapNhat').html()!= null && $('#btnCapNhat').html().trim().length > 0 && !$('#btnCapNhat').is(':hidden')){
			   					$('#btnCapNhat').click();
			   				}
			   				$('.btnCapNhat').each(function(){	   					
			   					if(!$(this).is(':hidden')){
			   						$(this).click();
			   					}
			   				});
			   			}
			   		});    
			    }	    
			});
		}		
	}
};
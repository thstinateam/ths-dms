/*
 * Copyright 2015 Viettel ICT. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/**
 * QL danh muc thuoc tinh, huong vi ..
 * @author trietptm
 * @since Nov 17 2014
 * **/
var CatalogType = {
	UNIT : 'UOM',
	CAT : '1',
    SUB_CAT : '2',
    BRAND : '3',
    FLAVOUR : '4',
    PACKING : '5',
    LIST_OF_PROBLEMS : 'FEEDBACK_TYPE',
    EXPIRY_DATE : 'NGAY_THANG_NAM',
    CUSTOMER_SALE_POSITION : 'CUSTOMER_SALE_POSITION',
    STAFF_SALE_TYPE : 'STAFF_SALE_TYPE',
    FOCUS_PRODUCT_TYPE : 'FOCUS_PRODUCT_TYPE',
    STAFF_TYPE_SHOW_SUP : 'STAFF_TYPE_SHOW_SUP',
    SYS_REASON_UPDATE_INVOICE: 'SYS_REASON_UPDATE_INVOICE',
    ORDER_PIRITY: 'ORDER_PIRITY',
    catType : null
//    ,
//    parseValue: function(value) {
//    	if (value.trim() == 0) {
//    		return CatalogType.UNIT;
//    	} else if(value.trim() == 1) {
//    		return CatalogType.CAT;
//    	} else if(value.trim() == 2) {
//    		return CatalogType.SUB_CAT;
//    	} else if(value.trim() == 3) {
//    		return CatalogType.BRAND;
//    	} else if(value.trim() == 4) {
//    		return CatalogType.FLAVOUR;
//    	} else if(value.trim() == 5) {
//    		return CatalogType.PACKING;
//    	} else if(value.trim() == 10) {
//    		return CatalogType.LIST_OF_PROBLEMS;
//    	} else if(value.trim() == 11) {
//    		return CatalogType.EXPIRY_DATE;
//    	} else if(value.trim() == 12) {
//    		return CatalogType.CUSTOMER_SALE_POSITION;
//    	} else if(value.trim() == 13) {
//    		return CatalogType.STAFF_SALE_TYPE;
//    	} else if(value.trim() == 14) {
//    		return CatalogType.FOCUS_PRODUCT_TYPE;
//    	}
//    	return -2;
//    }
};

/**
 * Quan ly danh muc
 * @author trietptm
 * @since Nov 17, 2015
 */
var CatalogManager = {
	isEdit: false,
	poplbName: null,
	oldCode: null,
	orPopLbName: null,
	
	/**
	 * load grid danh muc
	 * @author trietptm
	 * @param params
	 * @since Nov 17, 2015
	 * 
	 */
	loadCatGrid: function(params) {
		var cptype = $('#catType').val().trim();
		var addIcon = '<a title="' + CatalogManager.gridBtTtAdd + '" href="javascript:void(0)" onclick="CatalogManager.showDialogAddOrEdit()" >' +
    	'<img width="15" height="15" src="/resources/images/icon_add.png"/></a>';
		var gridTt = CatalogManager.gridTtCatCode;
		switch (cptype) {
			case CatalogType.UNIT:
				gridTt = CatalogManager.gridTtUnit;
				break;	
			case CatalogType.SUB_CAT:
				gridTt = CatalogManager.gridTtSubCatCode;
				break;
			case CatalogType.BRAND:
				gridTt = CatalogManager.gridTtBrand;
				break;
			case CatalogType.FLAVOUR:
				gridTt = CatalogManager.gridTtFlavour;
				break;
			case CatalogType.PACKING:
				gridTt = CatalogManager.gridTtPackage;	
				break;
			case CatalogType.LIST_OF_PROBLEMS:
				gridTt = CatalogManager.gridTtProblemsListGtt;
				break;
			case CatalogType.EXPIRY_DATE:
				gridTt = CatalogManager.gridTtExpiryDate;
				break;
			case CatalogType.CUSTOMER_SALE_POSITION:
				gridTt = CatalogManager.gridTtCustomerSalePosition;
				break;
			case CatalogType.STAFF_SALE_TYPE:
				gridTt = CatalogManager.gridTtStaffSaleType;
				break;
			case CatalogType.STAFF_TYPE_SHOW_SUP:
				gridTt = CatalogManager.gridTtPositionSuperviseStaffType;
				break;
			case CatalogType.FOCUS_PRODUCT_TYPE:
				gridTt = CatalogManager.gridTtFocusProductType;
				break;
			case CatalogType.SYS_REASON_UPDATE_INVOICE:
				gridTt = CatalogManager.gridTtReasonUpdateInvoice;
				break;
			case CatalogType.ORDER_PIRITY:
				gridTt = CatalogManager.gridTtOrderPriority;
				break;
		}
		
		if (cptype != CatalogType.SUB_CAT) {
			CatalogManager.poplbName = gridTt;
			CatalogManager.orPopLbName = gridTt;
		} else {
			CatalogManager.poplbName = CatalogManager.gridTtSubCatCode;
			CatalogManager.orPopLbName = CatalogManager.gridTtSubCatCode;
		}
					
		$('#dg').datagrid({
			url: '/config/loadShopCat',
			rownumbers: true,
			queryParams: params,
			singleSelect: true,
			fitColumns: true,
			pageSize: 20,
			width: $('#datasourceDiv').width(),
			pageList: [ 20, 30, 40 ],
			height: 'auto',
			scrollbarSize: 0,
			pagination: true,
			pageNumber: 1,
			nowrap: false,
			autoRowHeight: true,
			columns: [[
				{field: 'catCode', title : CatalogManager.gridTtCode, width : 100, align : 'left', sortable : false, resizable : false,
					formatter : function(value, row, index) {
						return Utils.XSSEncode(row.code);
					}
				},
				{field: 'catName', title : CatalogManager.poplbName, width : 150, align : 'left', sortable : false, resizable : false,
					formatter : function(value, row, index) {
						return Utils.XSSEncode(row.name);
					}
				},
				{field: 'value', title : CatalogManager.gridTtStaffType, hidden: true, width : 150, align : 'left', sortable : false, resizable : false,
					formatter : function(value, row, index) {
						return Utils.XSSEncode(row.value);
					}
				},
				{field: 'parentCatName', title : CatalogManager.gridTtParentCat, hidden: true, width : 150, align : 'left', sortable : false, resizable : false,
					formatter : function(value, row, index) {
						return Utils.XSSEncode(row.parentName);
					}
				},
				{field: 'catDescript', title : CatalogManager.gridTtDescript, width : 200, align : 'left', sortable : false, resizable : false,
					formatter : function(value, row, index) {
						return Utils.XSSEncode(row.description);
					}
				},
			    {field:'edit', title: addIcon, width: 50, align: 'center', sortable: false, resizable: false, 
					formatter: function(val, row, index) {
			    		var strFunc = '<a href="javascript:void(0)" title="'+ Utils.XSSEncode(CatalogManager.gridBtTtChange) +'" onclick="CatalogManager.showDialogAddOrEdit('+ index + ');">'+
		    			'<img src="/resources/images/icon-edit.png"/></a>';
			    		if (cptype != CatalogType.UNIT && cptype != CatalogType.EXPIRY_DATE && cptype != CatalogType.LIST_OF_PROBLEMS
			    				&& cptype != CatalogType.CUSTOMER_SALE_POSITION && cptype != CatalogType.STAFF_SALE_TYPE && cptype != CatalogType.FOCUS_PRODUCT_TYPE
			    				&& cptype != CatalogType.SYS_REASON_UPDATE_INVOICE && cptype != CatalogType.ORDER_PIRITY) {
	    					strFunc += '<a href="javascript:void(0)" title="' + Utils.XSSEncode(msgText4) + '"style="margin-left:2px" onclick="CatalogManager.deleteRow('+ index + ');">'+
	    						'<img src="/resources/images/icon_delete.png"/></a>';
			    		}
			    		return strFunc;
			    	}
			    }
			]],
			onLoadSuccess: function(data) {
				if (!data.error) {
					$('#errExcelMsg').html('').hide();
					
					$('#dg').datagrid("hideColumn","value");
					$('#dg').datagrid("hideColumn","parentCatName");
					var catType = $('#catType').val().trim();
					if (catType == CatalogType.SUB_CAT) {
						$('#dg').datagrid("showColumn","parentCatName");
					} else if (catType == CatalogType.STAFF_TYPE_SHOW_SUP) {
						$('#dg').datagrid("showColumn","value");
					}
					
					$('#shopCatEasyUIDialog #popCatType').val(catType);
					//fill popup combo cat
					var lstCat = data.lstCat;
					if (lstCat != undefined && lstCat != null && lstCat.length > 0) {
						$('#shopCatEasyUIDialog #popupParentCat').empty().change();
						for (var i = 0; i < lstCat.length; i++) {
							var obj = new Object();
							obj = lstCat[i];
							$('#shopCatEasyUIDialog #popupParentCat').append($('<option>', {
							    value: obj.id,
							    select: obj.id,
							    id: 'select-'+ obj.id,
							    text: obj.productInfoName
							}));
						}
						$('#shopCatEasyUIDialog #popupParentCat').change();
					}
					//fill popup combo cat
					var lstStaffType = data.lstStaffType;
					if (lstStaffType != undefined && lstStaffType != null && lstStaffType.length > 0) {
						$('#shopCatEasyUIDialog #cbxStaffType').empty().change();
						for (var i = 0; i < lstStaffType.length; i++) {
							var obj = new Object();
							obj = lstStaffType[i];
							$('#shopCatEasyUIDialog #cbxStaffType').append($('<option>', {
							    value: obj.id,
							    select: obj.id,
							    id: 'select-'+ obj.id,
							    text: obj.name
							}));
						}
						$('#shopCatEasyUIDialog #cbxStaffType').change();
					}
					setTimeout(function() {
						$('#pdCodeName').focus();
					}, 500);
					
					// phan quyen control
					var arrEdit =  $('#datasourceDiv td[field="edit"]');
					if (arrEdit != undefined && arrEdit != null && arrEdit.length > 0) {
					  for (var i = 0, size = arrEdit.length; i < size; i++) {
					  	$(arrEdit[i]).prop("id", "column_edit" + i);//Khai bao id danh cho phan quyen
						$(arrEdit[i]).addClass("cmsiscontrol");
					  }
					}
					Utils.functionAccessFillControl('datasourceDiv', function(data) {
						arrTmpLength =  $('#datasourceDiv td[id^="column_edit"]').length;
						invisibleLenght = $('.isCMSInvisible[id^="column_edit"]').length;
						if (arrTmpLength > 0 && arrTmpLength != invisibleLenght) {
							$('#dg').datagrid("showColumn", "edit");
						} else {
							$('#dg').datagrid("hideColumn", "edit");
						}
					});
				} else {
					$('#errExcelMsg').html(data.errMsg).show();
				}
			}
		});
	},
	
	/**
	 * show Dialog Add Edit
	 * @author trietptm
	 * @since Nov 17, 2015
	 */
	showDialogAddOrEdit: function(index) {
		$('#errExcelMsg').html('').hide();
		$('#successMsg').html('').hide();
		$('#errMsgPopup').html('').hide();
		$('#shopCatEasyUIDialog #errMsg').html('').hide();
		
		CatalogManager.changeLbText();
		var catType = $('#catType').val().trim();
		var data = null;
		
		setTimeout(function() {
			if ($('#shopCatEasyUIDialog #catCode').prop('disabled') == false) {
				$('#shopCatEasyUIDialog #catCode').focus();
			} else {
				$('#shopCatEasyUIDialog #catName').focus();
			}
		}, 500);
		
		if (index == undefined) {
			$('#shopCatEasyUIDialog #catCode').val('');
			$('#shopCatEasyUIDialog #catName').val('');
			$('#shopCatEasyUIDialog #catDescript').val('');

			$('#shopCatEasyUIDialog #catCode').removeAttr('disabled');
			$('#shopCatEasyUIDialog #catName').removeAttr('disabled');
	
			CatalogManager.isEdit = false;
			CatalogManager.oldCode = null;
			$('#hideCatCode').val('');
		} else {
			data = $('#dg').datagrid('getRows')[index];
			CatalogManager.oldCode = data.code;
			$('#hideCatCode').val(Utils.XSSEncode(data.code));
			$('#shopCatEasyUIDialog #catCode').val(Utils.XSSEncode(data.code));
			$('#shopCatEasyUIDialog #catName').val(Utils.XSSEncode(data.name));
			$('#shopCatEasyUIDialog #catDescript').val(Utils.XSSEncode(data.description));

			if(catType == CatalogType.UNIT || catType == CatalogType.EXPIRY_DATE || catType == CatalogType.LIST_OF_PROBLEMS
				|| catType == CatalogType.CUSTOMER_SALE_POSITION || catType == CatalogType.STAFF_SALE_TYPE || catType == CatalogType.FOCUS_PRODUCT_TYPE
				|| catType == CatalogType.STAFF_TYPE_SHOW_SUP || catType == CatalogType.SYS_REASON_UPDATE_INVOICE || catType == CatalogType.ORDER_PIRITY) {
				$('#shopCatEasyUIDialog #catCode').attr('disabled','disalbled');
			} else {
				$('#shopCatEasyUIDialog #catCode').removeAttr('disabled');
				$('#shopCatEasyUIDialog #catName').removeAttr('disabled');
			}
			
			CatalogManager.isEdit = true;
			if (data.parentId != undefined && data.parentId != null) {
				$('#shopCatEasyUIDialog #popupParentCat').val(data.parentId).change();
			}
			if (data.staffTypeId != undefined && data.staffTypeId != null) {
				$('#shopCatEasyUIDialog #cbxStaffType').val(data.staffTypeId).change();
			}
		}
		$('#shopCatEasyUIDialog #lblPopCatName').html(CatalogManager.poplbName);
		
		$('#shopCatEasyUIDialog #coverCbParentCat').hide();
		$('#shopCatEasyUIDialog #staffTypeContainer').hide();
		if (catType == CatalogType.SUB_CAT) {
			$('#shopCatEasyUIDialog #coverCbParentCat').show();
		} else if (catType == CatalogType.STAFF_TYPE_SHOW_SUP) {
			$('#shopCatEasyUIDialog #staffTypeContainer').show();
		}
		
		$('#shopCatEasyUIDialog #catCode').focus();
		$('#shopCatEasyUIDialog').dialog('open');
		setTimeout(function() {
			CommonSearchEasyUI.fitEasyDialog('shopCatEasyUIDialog');
		}, 1000);
	},
	
	/**
	 * xu ly luu them moi / thay doi
	 * @author trietptm
	 * @since Nov 17, 2015
	 */
	saveCatalogManager: function() {
		CatalogManager.changeLbText(CatalogType.UNIT);
		$('#errMsgPopup').hide();
		var data = new Object();
		var msg = '';
		var catCode = $('#shopCatEasyUIDialog #catCode').val().trim();
		var cbType = $('#catType').val().trim();

		if ((cbType == CatalogType.LIST_OF_PROBLEMS
				|| cbType == CatalogType.UNIT || cbType == CatalogType.CUSTOMER_SALE_POSITION 
				|| cbType == CatalogType.STAFF_SALE_TYPE || cbType == CatalogType.FOCUS_PRODUCT_TYPE
				) && CatalogManager.isEdit == true) {
			
			//anhhpt:neu ma dvt luc dau khac voi ma dvt moi --> khong cho luu
			if(!isNullOrEmpty(catCode) && !isNullOrEmpty($('#hideCatCode').val()) && catCode.toUpperCase()!= $('#hideCatCode').val().trim().toUpperCase()){
				$('#errMsgPopup').html(msgKhongTheLuu).show();
				return false;
			}
			catCode = $('#hideCatCode').val();
		}
		
		var catName = $('#shopCatEasyUIDialog #catName').val().trim();
		var catDescript = $('#shopCatEasyUIDialog #catDescript').val().trim();
		var type = $('#shopCatEasyUIDialog #popCatType').val().trim();
		
		if (isNullOrEmpty(msg)) {
			msg = Utils.getMessageOfRequireCheck('shopCatEasyUIDialog #catCode', CatalogManager.gridTtCode, false, 20);
		}
		if (isNullOrEmpty(msg)) {
			msg = Utils.getMessageOfSpecialCharactersValidate('shopCatEasyUIDialog #catCode', CatalogManager.gridTtCode, Utils._CODE);
		}
		if (isNullOrEmpty(msg)) {
			msg = Utils.getMessageOfRequireCheck('shopCatEasyUIDialog #catName', CatalogManager.poplbName, false, 500);
		}
		if (isNullOrEmpty(msg)) {
			msg = Utils.getMessageOfSpecialCharactersValidate('shopCatEasyUIDialog #catName', CatalogManager.poplbName);
		}
//		if (isNullOrEmpty(msg)) {
//			//neu la: Nganh hang, nganh hang con, nhan hieu, huong vi, dong goi, loai KH thi validate them dau ","
//			if (cbType == CatalogType.CAT || cbType == CatalogType.SUB_CAT || cbType == CatalogType.BRAND
//					|| cbType == CatalogType.FLAVOUR || cbType == CatalogType.PACKING 
//					|| cbType == CatalogType.CUSTOMER_TYPE || cbType == CatalogType.UNIT) {
//				if (/[<>/\,]/g.test(catName) == true) {
//					msg = CatalogManager.orPopLbName + " " + msgTenDanhMucKTDB;
//				}
//			} else {//Cac danh muc con lai validate 4 ky tu dac biet cho ATTT
//				if (/[<>/\\]/g.test(catName) == true) {
//					msg = CatalogManager.orPopLbName + " " + msgTenDanhMucKTDB;
//				}				
//			}
//		}
		if (isNullOrEmpty(msg)) {
			msg = Utils.getMessageOfSpecialCharactersValidate('shopCatEasyUIDialog #catDescript', CatalogManager.gridTtDescript, Utils._SPECIAL);
		}
		
		if (!isNullOrEmpty(msg)) {
			$('#shopCatEasyUIDialog #errMsg').html(msg).show();
			return false;
		}
		
		data.oldCatCode = CatalogManager.oldCode;
		data.catCode = catCode;
		data.catName = catName;
		data.catDescript = catDescript;
		data.type = type;
		data.isEdit = CatalogManager.isEdit;
		if (cbType == CatalogType.SUB_CAT) {
			data.parentCatId = $('#shopCatEasyUIDialog #popupParentCat').val().trim();
		} else if (cbType == CatalogType.STAFF_TYPE_SHOW_SUP) {
			data.staffTypeId = $('#shopCatEasyUIDialog #cbxStaffType').val().trim();
		}
		Utils.addOrSaveData(data, '/config/save-shop-cat', null, 'serverErrors',function(data) {
			$('#errExcelMsg').html('').hide();
			$('#shopCatEasyUIDialog').dialog('close');
			$('#dg').datagrid('reload');
		}, null, null, null, null, function(data) {
			if (!isNullOrEmpty(data.errMsg)) {
				$('#shopCatEasyUIDialog #errMsg').html(data.errMsg).show();
				return false;
			}
		});
	},
	
	/**
	 * tim kiem
	 * @author trietptm
	 * @since Nov 17, 2015
	 */
	searchCat: function() {
		var params = new Object();
		params.type = $('#catType').val().trim();
		params.pdCodeName = $('#pdCodeName').val().trim();
//		params.inportExportType = $('#cbWhType').val();
		CatalogManager.loadCatGrid(params);
	},
	
	/**
	 * xu ly xoa row tren gird
	 * @author trietptm
	 * @since Nov 17, 2015
	 */
	deleteRow: function(index) {
		var data = $('#dg').datagrid('getRows')[index];
		var tmType = $('#catType').val().trim();
		var dataModel = new Object();
		dataModel.catCode = data.code;
		dataModel.type = tmType;
		Utils.addOrSaveData(dataModel, '/config/delete-shop-cat', null, 'serverErrors', function(data) {
			Utils.updateTokenForJSON(data);
			$('#errExcelMsg').html('').hide();
			$('#dg').datagrid('reload');
		}, null, null, true, msgCommon2, function(data) {
			$('#errExcelMsg').html(data.errMsg).show();
		});
		
	},
	
	/**
	 * xu ly thay doi lable ten
	 * @author trietptm
	 * @since Nov 17, 2015
	 */
	changeLbText: function(forgridTT) {
		var cptype = $('#catType').val().trim();
		var gridTt = CatalogManager.gridTtCatCode;
		switch(cptype){
			case CatalogType.UNIT:
				gridTt = CatalogManager.gridTtUnit;
				break;	
			case CatalogType.SUB_CAT:
				gridTt = CatalogManager.gridTtSubCatCode;
				break;
			case CatalogType.BRAND:
				gridTt = CatalogManager.gridTtBrand;
				break;
			case CatalogType.FLAVOUR:
				gridTt = CatalogManager.gridTtFlavour;
				break;
			case CatalogType.PACKING:
				gridTt = CatalogManager.gridTtPackage;	
				break;
			case CatalogType.LIST_OF_PROBLEMS:
				gridTt = CatalogManager.gridTtProblemsListGtt;
				break;
			case CatalogType.EXPIRY_DATE:
				gridTt = CatalogManager.gridTtExpiryDate;
				break;
			case CatalogType.CUSTOMER_SALE_POSITION:
				gridTt = CatalogManager.gridTtCustomerSalePosition;
				break;
			case CatalogType.STAFF_SALE_TYPE:
				gridTt = CatalogManager.gridTtStaffSaleType;
				break;
			case CatalogType.STAFF_TYPE_SHOW_SUP :
				gridTt = CatalogManager.gridTtPositionSuperviseStaffType;
				break;
			case CatalogType.FOCUS_PRODUCT_TYPE:
				gridTt = CatalogManager.gridTtFocusProductType;
				break;
			case CatalogType.SYS_REASON_UPDATE_INVOICE:
				gridTt = CatalogManager.gridTtReasonUpdateInvoice;
				break;
			case CatalogType.ORDER_PIRITY:
				gridTt = CatalogManager.gridTtOrderPriority;
				break;
		}
		
		if (isNullOrEmpty(forgridTT)) {
			if(cptype != CatalogType.SUB_CAT) {
				CatalogManager.poplbName = gridTt + '<span class="ReqiureStyle"> *</span>';
			} else {
				CatalogManager.poplbName = CatalogManager.gridTtSubCatCode+ '<span class="ReqiureStyle"> *</span>';
			}
		} else {
			CatalogManager.poplbName = gridTt; 
		}
		CatalogManager.orPopLbName = gridTt;
		$('.panel-title').first().html(CatalogManager.thongTin + ' ' + gridTt);
	},
	
	/**
	 * load cay danh muc
	 * @author trietptm
	 * @since Nov 17, 2015
	 */
	loadCatalogTree: function(url) {
		loadDataForTree(url);
		$('#tree').bind("loaded.jstree", function(event, data) {
			$('#tree').jstree('select_node', '#' + CatalogType.UNIT);
		});
		$('#tree').bind("select_node.jstree", function (event, data) {	
			$('#pdCodeName').val('');
			var id = data.rslt.obj.attr("id");
			if (id != null && id != undefined && id.length > 0) {
				CatalogManager.showCatalogForm(id);
			}
	    });
		$('#tree').bind("open_node.jstree", function (event, data) {
	    });
	},
	
	/**
	 * load thong tin tung danh muc
	 * @author trietptm
	 * @since Nov 17, 2015
	 */
	showCatalogForm: function(id) {
		$('#catType').val(id);
//		$('#divOverlay').show();
		var params = new Object();
		params.type = id;
		CatalogManager.loadCatGrid(params);
//		if (id == CatalogType.IMPORT_STOCK || id == CatalogType.EXPORT_STOCK) {
//			$('#whTypeCover').show();
//			$('#lbWhType').show();
////			$('#btSearch').css('margin-left','-161px');
//			$('#cbWhType').val(0).change();
//		} else {
			$('#whTypeCover').hide();
			$('#lbWhType').hide();
//		}
	}
};

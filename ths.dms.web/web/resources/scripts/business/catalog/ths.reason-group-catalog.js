var ReasonGroupCatalog = {
	_xhrSave : null,
	_xhrDel: null,
	getGridUrl: function(reasonGroupCode,reasonGroupName,description,status){		
		return "/catalog-manager/reason-group/search?code=" + encodeChar(reasonGroupCode) + "&name=" + encodeChar(reasonGroupName) + "&note=" + encodeChar(description) + "&status=" + status;
	},	
	searchReasonGroup: function(){		
		$('#errMsg').html('').hide();
		var reasonGroupCode = $('#reasonGroupCode').val().trim();
		var reasonGroupName =  $('#reasonGroupName').val().trim();
		var description = $('#description').val().trim();
		var status = $('#status').val().trim();		
		var url = ReasonGroupCatalog.getGridUrl(reasonGroupCode,reasonGroupName,description,status);
		$("#grid").setGridParam({url:url,page:1}).trigger("reloadGrid");
		return false;
	},
	saveReasonGroup: function(){
		var msg = '';
		$('#errMsg').html('').hide();
		msg = Utils.getMessageOfRequireCheck('reasonGroupCode','Mã nhóm lý do');
		if(msg.length == 0){
			msg = Utils.getMessageOfSpecialCharactersValidate('reasonGroupCode','Mã nhóm lý do',Utils._CODE);
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfRequireCheck('reasonGroupName','Tên nhóm lý do');
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfSpecialCharactersValidate('reasonGroupName','Tên nhóm lý do');
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfRequireCheck('status','Trạng thái',true);
		}
		if(msg.length == 0 && $('#description').val().trim().length > 0){
			msg = Utils.getMessageOfSpecialCharactersValidate('description','Mô tả');
		}
		if(msg.length > 0){
			$('#errMsg').html(msg).show();
			return false;
		}
		var dataModel = new Object();
		dataModel.id = $('#selReasonId').val().trim();
		dataModel.code = $('#reasonGroupCode').val().trim();
		dataModel.name = $('#reasonGroupName').val().trim();
		dataModel.note = $('#description').val().trim();
		dataModel.status = $('#status').val();
		Utils.addOrSaveRowOnGrid(dataModel, "/catalog-manager/reason-group/save", ReasonGroupCatalog._xhrSave, null,null, function(data){
			if(!data.error){
				ReasonGroupCatalog.clearData();
			}
		});			
		return false;
	},
	getSelectedReasonGroup: function(rowId,status){
		$('#reasonGroupCode').attr('disabled','disabled');
		focusFirstTextbox();
		var id =  $("#grid").jqGrid ('getCell', rowId, 'id');
		var code =  $("#grid").jqGrid ('getCell', rowId, 'reasonGroupCode');
		var name =  $("#grid").jqGrid ('getCell', rowId, 'reasonGroupName');
		var description =  $("#grid").jqGrid ('getCell', rowId, 'description');
		if(id!= null && id!= 0 && id!=undefined){
			$('#selReasonId').val(id);
		} else {
			$('#selReasonId').val(0);
		}
		setTextboxValue('reasonGroupCode',code);
		setTextboxValue('reasonGroupName',name);
		setTextboxValue('description',description);
		setSelectBoxValue('status', status);
		ReasonGroupCatalog.getChangedForm();
		setTitleUpdate();
		return false;
	},
	resetForm: function(){
		ReasonGroupCatalog.clearData();			
		$("#grid").trigger("reloadGrid");
		focusFirstTextbox();
		return false;
	},
	deleteReasonGroup: function(reasonGroupCode){
		var dataModel = new Object();
		dataModel.code = reasonGroupCode;	
		Utils.deleteSelectRowOnGrid(dataModel, 'nhóm lý do', '/catalog-manager/reason-group/delete', ReasonGroupCatalog._xhrDel,null,null, function(data){
			if(!data.error){
				ReasonGroupCatalog.resetForm();
				$('#reasonGroupName').focus();
			}
		}, null);		
		return false;
	},
	clearData: function(){
		setTitleSearch();
		$('.RequireStyle').hide();
		$('#selReasonId').val(0);
		$('#errMsg').html('').hide();
		$('#reasonGroupCode').removeAttr('disabled');
		$('#btnEdit').hide();
		$('#btnCancel').hide();
		$('#btnSearch').show();
		$('#btnAdd').show();
		$('#reasonGroupCode').val('');
		$('#reasonGroupName').val('');
		$('#description').val('');
		setSelectBoxValue('status', 1);
		ReasonGroupCatalog.searchReasonGroup();
	},
	getChangedForm: function(){
		$('.RequireStyle').show();
		$('#btnSearch').hide();
		$('#btnAdd').hide();
		$('#btnEdit').show();
		$('#btnCancel').show();
		//$('#code').focus();
		focusFirstTextbox();
		setTitleAdd();
	}
};
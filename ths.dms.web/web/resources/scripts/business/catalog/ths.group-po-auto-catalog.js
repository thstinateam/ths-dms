var GroupPOAuto = {
		xhrSave : null,
		_xhrDel: null,
		id:null,
		_mapMutilSelect : null,
		_mapMultiSelectSub:null,
		_mapNode:null,
		_mapCheck:null,
		getGridUrl: function(groupCode,groupName,status){
			return "/catalog/group-po/search?groupCode=" + encodeChar(groupCode) + "&groupName=" + encodeChar(groupName) +  "&status=" + status;
		},
		getGridProductUrl:function(groupId,objectType){
			return "/catalog/group-po/searchListProduct?idGroup=" + groupId + "&objectType=" +objectType;
		},
		getGridSectorUrl:function(objectType, name, code){
			
			return "/catalog/group-po/searchListSector?objectType=" +objectType + "&name=" + encodeChar(name)+ "&code="+ encodeChar(code);
		},
		search: function(){
			GroupPOAuto.reset();
			$('#errMsg').html('').hide();
			var groupCode = $('#groupCode').val().trim();
			var groupName = $('#groupName').val().trim();
			var status = $('#status').val();
			var tm = setTimeout(function() {
				$('#groupCode').focus();
			}, 500);
			var url = GroupPOAuto.getGridUrl(groupCode,groupName,status);
			$("#grid").datagrid({url:url,pageNumber:1});
			return false;
		},
		getListProduct:function(groupId){
			var objectType =2 ; 
			
		},
		deleteRow: function(groupDetailId){
			$('#errMsg').html('').hide();
			var dataModel = new Object();
			dataModel.groupDetailId = groupDetailId;	
			Utils.deleteSelectRowOnGrid(dataModel, 'sản phẩm', "/catalog/group-po/deleteProduct", GroupPOAuto._xhrDel, null, null,function(data){
				if(data.error == false){
					//var url = GroupPOAuto.getGridUrl(GroupPOAuto.id,2);
					//$("#gridProduct").datagrid({url:url,page:1});
					$("#gridProduct").datagrid("reload");
				}
			});
			return false;		
		},
	
		searchCat:function(id,objectType){
			$('#idCode1').val(id);
			GroupPOAuto.id = id;
			GroupPOAuto.reset();
			$('#menuSecGoods').show();
			var url = GroupPOAuto.getGridProductUrl(id,objectType);
			
			var data = "<a href='javascript:void(0)' onclick='return GroupPOAuto.openDialogUpdateSecGoods();'><img src='/resources/images/icon_add.png'/></a>";
			var title = getFormatterControlGrid('btnAddGrid1', data);
			
			$("#gridSecGoods").datagrid({
				  url:url,
			  	  pageList  : [10,20,30],
			  	  pageNumber : 1, 
				  width: 550,
				  pageSize : 10,
				  checkOnSelect :true,
				  height:'auto',
				  scrollbarSize : 0,
				  pagination:true,			  
				  fitColumns:true,		     
			      singleSelect:true,
				  method : 'GET',
				  rownumbers: true,
				  columns:[[	
					{field: 'code', title: 'Mã NH', width: 150, sortable:false,resizable:false, align: 'left', formatter: function(value, row, index){
				    	return Utils.XSSEncode(value);
				    }},
				    {field: 'name', title: 'Tên NH', width: 300, sortable:false,resizable:false, align: 'left', formatter: function(value, row, index){
				    	return Utils.XSSEncode(value);
				    }},
				    {field:'delete', title: title, width: 50, align: 'center',sortable:false,resizable:false, formatter: POAutoGroupFormatter.delCellIconFormatterCat},	   
				  ]],
				  onLoadSuccess:function(){
						$('.datagrid-header-rownumber').html('STT');  
				      	$('#gridSecGoods').datagrid('resize');
				      }
			});
			return false;
		},
		searchSubCat:function(id,objectType){
			$('#idCode2').val(id);
			GroupPOAuto.id = id;
			GroupPOAuto.reset();
			$('#menuSecGoodsChild').show();
			var url = GroupPOAuto.getGridProductUrl(id,objectType);
			
			var data = '<a href="javascript:void(0)" onclick="return GroupPOAuto.openDialogUpdateSecGoodsBrand();"><img src="/resources/images/icon_add.png"/></a>';
			var title = getFormatterControlGrid('btnAddGrid2', data);
			
			$("#gridSecGoodsBrand").datagrid({
				  url:url,
			  	  pageList  : [10,20,30],
			  	  pageNumber: 1,
				  width: 550,
				  pageSize : 10,
				  checkOnSelect :true,
				  height:'auto',
				  scrollbarSize : 0,
				  pagination:true,			  
				  fitColumns:true,		     
			      singleSelect:true,
				  method : 'GET',
				  rownumbers: true,
				  columns:[[
				            //here
				    {field: 'parentCode', title: 'Mã NH', width: 150, sortable:false,resizable:false, align: 'left', formatter: function(value, row, index){
				    	return Utils.XSSEncode(value);
				    }},
					{field: 'code', title: 'Mã NHC', width: 150, sortable:false,resizable:false, align: 'left', formatter: function(value, row, index){
				    	return Utils.XSSEncode(value);
				    }},
				    {field: 'name', title: 'Tên NHC', width: 300, sortable:false,resizable:false, align: 'left', formatter: function(value, row, index){
				    	return Utils.XSSEncode(value);
				    }},
				    {field:'delete', title: title, width: 50, align: 'center',sortable:false,resizable:false, formatter: POAutoGroupFormatter.delCellIconFormatterSubCat},	   
				  ]],
				  onLoadSuccess:function(){
						$('.datagrid-header-rownumber').html('STT');  
				      	$('#gridSecGoodsBrand').datagrid('resize');
				      }
			});
			return false;
		},
		searchProduct:function(id,objectType){
			GroupPOAuto.id = id;
			GroupPOAuto.reset();
			$('#menuProduct').show();
			var url = GroupPOAuto.getGridProductUrl(id,objectType);
			
			var data = "<a href='javascript:void(0)' onclick=\'return GroupPOAuto.openPopupProduct("+id +","+objectType+");\'><img src='/resources/images/icon_add.png'/></a>";
			var title = getFormatterControlGrid('btnAddGrid3', data);
			
			$("#gridProduct").datagrid({
				  url:url,
				  pageNumber: 1,
			  	  pageList  : [10,20,30],
				  width: 550,
				  pageSize : 10,
				  checkOnSelect :true,
				  height:'auto',
				  scrollbarSize : 0,
				  pagination:true,			  
				  fitColumns:true,		     
			      singleSelect:true,
				  method : 'GET',
				  rownumbers: true,
				  columns:[[	
					{field: 'code', title: 'Mã sản phẩm', width: 150, sortable:false,resizable:false, align: 'left', formatter: function(value, row, index){
				    	return Utils.XSSEncode(value);
				    }},
				    {field: 'name', title: 'Tên sản phẩm', width: 300, sortable:false,resizable:false, align: 'left', formatter: function(value, row, index){
				    	return Utils.XSSEncode(value);
				    }},
				    {field:'delete', title: title, width: 50, align: 'center',sortable:false,resizable:false, formatter: POAutoGroupFormatter.delCellIconFormatter},	   
				  ]],
				  onLoadSuccess:function(){
						$('.datagrid-header-rownumber').html('STT');  
				      	$('#gridProduct').datagrid('resize');
				      }
			});
			return false;
		},
		reset:function(){
			$('#menuSecGoods').hide();
			$('#menuSecGoodsChild').hide();
			$('#menuProduct').hide();
		},
		searchProductTree:function(){
			var code = $("#productCode").val().trim();
			var name = 	$("#productName").val().trim();
			var nodes = $('#tree').tree('getChecked');
			for(var i=0; i<nodes.length; i++){
				if(nodes[i].attributes.productTreeVO.type == "PRODUCT"){
					GroupPOAuto._mapCheck.put(nodes[i].id,nodes[i].id);
		    		}else{
		    			GroupPOAuto._mapCheck.remove(nodes[i].id);
		    		}
			}
			$(".easyui-dialog #errMsgProductDlg").html('').hide();
			var groupPOAutoId = GroupPOAuto.id; //id groupPOAuto
			$("#tree").tree({
				url: '/rest/catalog/group-po/tree/0.json',  
				method:'GET',
	            animate: true,
	            checkbox:true,
	            onBeforeLoad:function(node,param){
	            	$("#tree").css({"visibility":"hidden"});
//	            	$("#loadding").css({"visibility":"visible"});
	            	$("#rpSection").html('<img id="loadding" class="LoadingStyle" src="/resources/images/loadingbig.gif" width="30" height="30" alt="Loadding"/ >');
	            	$("#treeNoResult").html('');
	            	param.code = code;
	            	param.name = name;
	            	param.id = groupPOAutoId;
	            },
	            onLoadSuccess:function(node,data){
	            	$("#tree").css({"visibility":"visible"});
	            	$("#rpSection").html('');
	            	if(data.length == 0){
	            		$(".easyui-dialog #errMsgProductDlg").html('Không có kết quả').show();
	            	}
	            	var arr  = GroupPOAuto._mapCheck.keyArray;
	            	if(arr.length > 0){
	            		for(var i = 0;i < arr.length; i++){
	            			var nodes = $('#tree').tree('find', arr[i]);
	            			if(nodes != null){
	            				$("#tree").tree('check',nodes.target);
	            			}
	            		}
	            	}
	            },
	            onCheck:function(node,check){ 
	            	var nodes = $('#tree').tree('getChecked');
            	if(node.attributes.productTreeVO.type == "PRODUCT"){
            			GroupPOAuto._mapCheck.put(node.id,node.id);
            		}else{
            			GroupPOAuto._mapCheck.remove(node.id);
            		}
	            }
			});
		},
		openPopupProduct:function(id,objectType){
			$("#productCode").val('');
			$("#productName").val('');
			$(".easyui-dialog #errMsgProductDlg").html('').hide();
			//GroupPOAuto.searchProductTree(id);
			GroupPOAuto._mapNode = new Map();
			GroupPOAuto._mapCheck = new Map();
			$("#popupProductSearch").dialog({
				onOpen:function(){
					$("#tree").tree({
						url: '/rest/catalog/group-po/tree/0.json',  
						method:'GET',
			            animate: true,
			            checkbox:true,
			            onBeforeLoad:function(node,param){
			            	$("#tree").css({"visibility":"hidden"});
			            	$("#rpSection").html('<img id="loadding" class="LoadingStyle" src="/resources/images/loadingbig.gif" width="30" height="30" alt="Loadding"/ >');
			            	$("#treeNoResult").html('');
			            	param.code = '';
			            	param.name = '';
			            	param.id = GroupPOAuto.id;
			            },
			            onLoadSuccess:function(node,data){
			            	$("#tree").css({"visibility":"visible"});
			            	$("#rpSection").html('');
			            	$("#productCode").focus();
			            	if(data.length == 0){
			            		$(".easyui-dialog #errMsgProductDlg").html('Không có kết quả').show();
			            	}
			            	var arr  = GroupPOAuto._mapCheck.keyArray;
			            	if(arr.length > 0){
			            		for(var i = 0;i < arr.length; i++){
			            			var nodes = $('#tree').tree('find', arr[i]);
			            			if(nodes != null){
			            				$("#tree").tree('check',nodes.target);
			            			}
			            		}
			            	}
			            },
			            onCheck:function(node,check){ 
			            	var nodes = $('#tree').tree('getChecked');
				            	if(node.attributes.productTreeVO.type == "PRODUCT"){
				            			GroupPOAuto._mapCheck.put(node.id,node.id);
				            		}else{
				            			GroupPOAuto._mapCheck.remove(node.id);
				            		}
			            }
					});
				}
			
			});
			$("#popupProductSearch").dialog('open');
		},
		createGroupPOAutoDetail:function(){
			var dataModel = new Object();
			var nodes = $('#tree').tree('getChecked');
			var lstId = '';  
			var lstIdCustomer = new Array();
			$('#errMsgPopup3').html('').hide();
			if(nodes.length == 0 ) {
				$('#errMsgPopup3').html('Bạn chưa chọn sản phẩm nào.').show();
        		return false;
        	}
	        for(var i=0; i<nodes.length; i++){
	        	if(nodes[i].attributes.productTreeVO.type != "PRODUCT"){
	        		continue;
	        	}
	        	lstIdCustomer.push(nodes[i].attributes.productTreeVO.id);  
	        }
			if(lstIdCustomer == undefined || lstIdCustomer == null || lstIdCustomer.length <=0){
				$(".easyui-dialog #errMsgProductDlg").html('Không có kết quả').show();
				return false;
			}
			dataModel.objectType = 2;
			dataModel.id = GroupPOAuto.id;
			dataModel.listOjectId = lstIdCustomer;
			Utils.addOrSaveRowOnGrid(dataModel, "/catalog/group-po/save", GroupPOAuto._xhrSave, null, null,function(data){
				$("#popupProductSearch").dialog("close");
				if(data.error == false){
					$("#gridProduct").datagrid("reload");
				}
			});
			return false;

		},
		

		openDialogUpdateSecGoods:function(codeText, nameText, title, url, callback,	codeFieldText, nameFieldText, arrParam, objectStaff){
			$('#popupSecFoods').css('visibility','visible');
			$('#popup1').dialog('open');
			$('#codeSecGoods').val('');
			$('#nameSecGoods').val('');
			$('#codeSecGoods').focus();
			GroupPOAuto._mapMutilSelect = new Map();
			$('#errMsgPopup1').hide();
			var url = GroupPOAuto.getGridSectorUrl(0, null, null);
			$("#gridSecGoodsPop").datagrid({
				  url:url,
			  	  pageList  : [10,20,30],
				  width: 550,
				  pageSize : 10,
				  checkOnSelect :true,
				  height:'auto',
				  scrollbarSize : 0,
				  pagination:true,
				  pageNumber:1,
				  fitColumns:true,		     
				  method : 'GET',
				  rownumbers: true,
				  columns:[[	
					{field: 'productInfoCode', title: 'Mã NH', width: 150, sortable:false,resizable:false, align: 'left', formatter: function(value, row, index){
				    	return Utils.XSSEncode(value);
				    }},
				    {field: 'productInfoName', title: 'Tên NH', width: 300, sortable:false,resizable:false, align: 'left', formatter: function(value, row, index){
				    	return Utils.XSSEncode(value);
				    }},
				    {field: 'productInfoId',title: '', checkbox:true, width:50,align:'center',sortable : false,resizable : false},
				    
				  ]],
				  	onCheck : function(rowIndex, rowData) {
				    	var selectedId = rowData.productInfoId;
				    	GroupPOAuto._mapMutilSelect.put(selectedId, rowData);
				    },
				    onUncheck : function(rowIndex, rowData) {
				    	var selectedId = rowData.productInfoId;
				    	GroupPOAuto._mapMutilSelect.remove(selectedId);
				    },
				    onCheckAll : function(rows) {
				    	if($.isArray(rows)) {
				    		for(var i = 0; i < rows.length; i++) {
				    			var row = rows[i];
				    			var selectedId = row.productInfoId;
				    			GroupPOAuto._mapMutilSelect.put(selectedId, row);
				    		}
				    	}
				    },
				    onUncheckAll : function(rows) {
				    	if($.isArray(rows)) {
				    		for(var i = 0; i < rows.length; i++) {
				    			var row = rows[i];
				    			var selectedId = row.productInfoId;
				    			GroupPOAuto._mapMutilSelect.remove(selectedId);
				    		}
				    	}
				    },
				   onLoadSuccess:function(data){
				      	var easyDiv = '#gridSecGoodsPopContainer ';
				    	$(easyDiv+'.datagrid-header-rownumber').html('STT');
				    	var exitsAll = false;
			    		var runner = 0;
			    		$(easyDiv+'input:checkbox[name=productInfoId]').each(function() {
			    			var selectedId = this.value;
			    			if(GroupPOAuto._mapMutilSelect.get(selectedId) != null || GroupPOAuto._mapMutilSelect.get(selectedId) != undefined) {
			    				exitsAll = true;
			    				$('#gridSecGoodsPop').datagrid('selectRow',runner);
			    			} 
			    			runner ++; 
			    		});
			    		if (exitsAll==false){
			    			$(easyDiv+'td[field=productInfoId] .datagrid-header-check input:checkbox').attr('checked', false);
				    	}
				    	
			    		var tabindex = -1;
						$('.InputTextStyle, select, input, button , li , tr, td, th, label, ul, a, img').each(function () {
							if (this.type != 'hidden') {
								$(this).attr("tabindex", tabindex);
								tabindex -=1;
							}
						});
						tabindex = 1;
			    		 $('.easyui-dialog input,.easyui-dialog select,.easyui-dialog button').each(function () {
				    		 if (this.type != 'hidden') {
					    	     $(this).attr("tabindex", '');
								 tabindex++;
				    		 }
						 });
			    		 if(data.rows.length == 0){
			    			 $('.datagrid-header-check input').removeAttr('checked');
			    		 }
			    		 $(easyDiv+'input:checkbox[name=productInfoId]').each(function(){
			    			 	var selectedId = $(this).val();
				 				var temp = GroupPOAuto._mapMutilSelect.get(selectedId);
				 				if(temp!=null) $(this).attr('checked','checked');
				 			});
				 	    	var length = 0;
				 	    	$(easyDiv+'input:checkbox[name=productInfoId]').each(function(){
				 	    		if($(this).is(':checked')){
				 	    			++length;
				 	    		}	    		
				 	    	});	    	
				 	    	if(data.rows.length==length){
				 	    		$(easyDiv+'td[field=productInfoId] .datagrid-header-check input:checkbox').attr('checked','checked');
				 	    	}else{
								$(easyDiv+'td[field=productInfoId] .datagrid-header-check input:checkbox').removeAttr('checked');
				 	    	}
				 	    	if(data.rows == null || data.rows == undefined || data.rows.length == 0){
				 	    		$(easyDiv+'td[field=productInfoId] .datagrid-header-check input:checkbox').removeAttr('checked');
				 	    	}
			    		updateRownumWidthForDataGrid('#gridSecGoodsPop');
			    		$(window).resize();
				      }
			});
			
			return false;
		},
		addCategory:function(){
			$('#errMsgPopup1').html('').hide();
			if(GroupPOAuto._mapMutilSelect == null || GroupPOAuto._mapMutilSelect.size() <= 0) {
				$('#errMsgPopup1').html('Bạn chưa chọn ngành hàng nào. Yêu cầu chọn').show();
        		return false;
        	}
	
			var mapKey = GroupPOAuto._mapMutilSelect.keyArray;
			var mapValue = GroupPOAuto._mapMutilSelect.valArray;
			var params = new Object();
			params.lstCategoryId = mapKey;
			GroupPOAuto.updateCategory(params.lstCategoryId);			
		},
		openDialogUpdateSecGoodsBrand:function(codeText, nameText, title, url, callback,	codeFieldText, nameFieldText, arrParam, objectStaff){
			$('#popupSecFoodsBrand').css('visibility','visible');
			$('#popup2').dialog('open');
			$('#errMsgPopup2').hide();
			GroupPOAuto._mapMutilSelectSub = new Map();
			$('#codeSecGoodsBrand').val('');
			$('#nameSecGoodsBrand').val('');
			$('#codeSecGoodsBrand').focus();
			var url = GroupPOAuto.getGridSectorUrl(1, null, null);
			$("#gridSecGoodsPopBrand").datagrid({
				  url:url,
			  	  pageList  : [10,20,30],
				  width: 550,
				  pageSize : 10,
				  checkOnSelect :true,
				  height:'auto',
				  scrollbarSize : 0,
				  pagination:true,
				  pageNumber:1,
				  fitColumns:true,
				  method : 'GET',
				  rownumbers: true,
				  columns:[[
				            //here
				    {field: 'parentCode', title: 'Mã NH', width: 150, sortable:false,resizable:false, align: 'left', formatter: function(value, row, index){
				    	return Utils.XSSEncode(value);
				    }},
					{field: 'productInfoCode', title: 'Mã NHC', width: 150, sortable:false,resizable:false, align: 'left', formatter: function(value, row, index){
				    	return Utils.XSSEncode(value);
				    }},
				    {field: 'productInfoName', title: 'Tên NHC', width: 300, sortable:false,resizable:false, align: 'left', formatter: function(value, row, index){
				    	return Utils.XSSEncode(value);
				    }},
				    {field: 'productInfoId',title: '', checkbox:true, width:50,align:'center',sortable : false,resizable : false},			  
				  ]],
				  onSelect : function(rowIndex, rowData) {
				    	var selectedId = rowData['productInfoId'];
				    	GroupPOAuto._mapMutilSelectSub.put(selectedId, rowData);
				    },
				    onUnselect : function(rowIndex, rowData) {
				    	var selectedId = rowData['productInfoId'];
				    	GroupPOAuto._mapMutilSelectSub.remove(selectedId);
				    },
				    onSelectAll : function(rows) {
				    	if($.isArray(rows)) {
				    		for(var i = 0; i < rows.length; i++) {
				    			var row = rows[i];
				    			var selectedId = row['productInfoId'];
				    			GroupPOAuto._mapMutilSelectSub.put(selectedId, row);
				    		}
				    	}
				    },
				    onUnselectAll : function(rows) {
				    	if($.isArray(rows)) {
				    		for(var i = 0; i < rows.length; i++) {
				    			var row = rows[i];
				    			var selectedId = row['productInfoId'];
				    			GroupPOAuto._mapMutilSelectSub.remove(selectedId);
				    		}
				    	}
				    },
				   onLoadSuccess:function(){
				      	var easyDiv = '#gridSecGoodsPopBrandContainer ';
				    	$(easyDiv+'.datagrid-header-rownumber').html('STT');
				    	var exitsAll = false;
			    		var runner = 0;
			    		$(easyDiv+'input:checkbox[name=productInfoId]').each(function() {
			    			var selectedId = this.value;
			    			if(GroupPOAuto._mapMutilSelectSub.get(selectedId) != null || GroupPOAuto._mapMutilSelectSub.get(selectedId) != undefined) {
			    				exitsAll = true;
			    				$('#gridSecGoodsPopBrand').datagrid('selectRow',runner);
			    			} 
			    			runner ++; 
			    		});
			    		if (exitsAll==false){
			    			$(easyDiv+'td[field=productInfoId] .datagrid-header-check input:checkbox').attr('checked', false);
				    	}
				    	
			    		var tabindex = -1;
						$('.InputTextStyle, select, input, button , li , tr, td, th, label, ul, a, img').each(function () {
							if (this.type != 'hidden') {
								$(this).attr("tabindex", tabindex);
								tabindex -=1;
							}
						});
						tabindex = 1;
			    		 $('.easyui-dialog input,.easyui-dialog select,.easyui-dialog button').each(function () {
				    		 if (this.type != 'hidden') {
					    	     $(this).attr("tabindex", '');
								 tabindex++;
				    		 }
						 });
			    		updateRownumWidthForDataGrid('#gridSecGoodsPopBrand');
			    		$(window).resize();
				      }
			});
			
			return false;
		},
		addSubCategory:function(){
			$('#errMsgPopup2').html('').hide();
			if(GroupPOAuto._mapMutilSelectSub == null || GroupPOAuto._mapMutilSelectSub.size() <= 0) {
				$('#errMsgPopup2').html('Bạn chưa chọn ngành hàng con nào. Yêu cầu chọn').show();
        		return false;
        	}
	
			var mapKey = GroupPOAuto._mapMutilSelectSub.keyArray;
			var mapValue = GroupPOAuto._mapMutilSelectSub.valArray;
			var params = new Object();
			params.lstCategoryId = mapKey;
			GroupPOAuto.updateSubCategory(params.lstCategoryId);
		},		
		searchSecGoods: function(){
			$('#errMsgPopup1').html('').hide();
			$('#errMsg').html('').hide();
			var code = $('#codeSecGoods').val().trim();
			var name = $('#nameSecGoods').val().trim();
			var tm = setTimeout(function() {
				$('#codeSecGoods').focus();
			}, 500);
			var objectType = 0;
			var url = GroupPOAuto.getGridSectorUrl(objectType, name, code);
			$("#gridSecGoodsPop").datagrid({url:url,pageNumber:1});
//			$("#gridSecGoodsPop").datagrid({
//				  url:url,
//			  	  pageList  : [10,20,30],
//				  width: 550,
//				  pageSize : 10,
//				  checkOnSelect :true,
//				  height:'auto',
//				  scrollbarSize : 0,
//				  pagination:true,			  
//				  fitColumns:true,		     
//			      singleSelect:true,
//				  method : 'GET',
//				  rownumbers: true,
//				  columns:[[	
//					{field: 'productInfoCode', title: 'Mã NH', width: 150, sortable:false,resizable:false, align: 'left'},
//				    {field: 'productInfoName', title: 'Tên NH', width: 300, sortable:false,resizable:false, align: 'left'},
//				    {field: 'status',title: '<a href="javascript:void(0)" onclick="return GroupPOAuto.openDialogUpdateSecGoodsBrand();"></a>', width:50,align:'center',sortable : false,resizable : false,
//		            	formatter:function(value,row){
//		            		if(row.status == 1){
//		            			return '<input type="checkbox" checked="checked" onClick=""/>';
//		            		}else{
//		            			return '<input type="checkbox" onClick="" />';
//		            		}
//		            	}
//				    },	   
//				  ]],
//				  onLoadSuccess:function(){
//						$('.datagrid-header-rownumber').html('STT');  
//				      	$('#gridSecGoodsPop').datagrid('resize');
//				      }
//			});
			return false;
		},
		searchSecGoodsBrand: function(){
			$('#errMsgPopup2').html('').hide();
			$('#errMsg').html('').hide();
			var code = $('#codeSecGoodsBrand').val().trim();
			var name = $('#nameSecGoodsBrand').val().trim();
			var tm = setTimeout(function() {
				$('#codeSecGoodsBrand').focus();
			}, 500);
			var objectType = 1;
			var url = GroupPOAuto.getGridSectorUrl(objectType, name, code);
			$("#gridSecGoodsPopBrand").datagrid({url:url,pageNumber:1});
//			$("#gridSecGoodsPopBrand").datagrid({
//				  url:url,
//			  	  pageList  : [10,20,30],
//				  width: 550,
//				  pageSize : 10,
//				  checkOnSelect :true,
//				  height:'auto',
//				  scrollbarSize : 0,
//				  pagination:true,			  
//				  fitColumns:true,		     
//			      singleSelect:true,
//				  method : 'GET',
//				  rownumbers: true,
//				  columns:[[
//				    {field: 'parentCode', title: 'Mã NH', width: 150, sortable:false,resizable:false, align: 'left'},
//					{field: 'productInfoCode', title: 'Mã NHC', width: 150, sortable:false,resizable:false, align: 'left'},
//				    {field: 'productInfoName', title: 'Tên NHC', width: 300, sortable:false,resizable:false, align: 'left'},
//				    {field: 'status',title: '<a href="javascript:void(0)" onclick="return GroupPOAuto.openDialogUpdateSecGoodsBrand();"></a>', width:50,align:'center',sortable : false,resizable : false,
//		            	formatter:function(value,row){
//		            		if(row.status == 1){
//		            			return '<input type="checkbox" checked="checked" onClick=""/>';
//		            		}else{
//		            			return '<input type="checkbox" onClick="" />';
//		            		}
//		            	}
//				    }	   
//				  ]],
//				  onLoadSuccess:function(){
//						$('.datagrid-header-rownumber').html('STT');  
//				      	$('#gridSecGoodsPopBrand').datagrid('resize');
//				      }
//			});
			return false;

		},
		openDialogCreateGroupPOAuto:function(flag,id,groupCode,groupName,status){
			GroupPOAuto.reset();
			var arrayParam = new Array();
			arrayParam.push(flag);
			arrayParam.push(id);
			arrayParam.push(groupCode);
			arrayParam.push(groupName);
			arrayParam.push(status);
			CommonSearch.openSearchStyle1EasyUICreateUpdateGroupPOAuto(null,
					null, "Thông tin nhóm","groupCode",
					"groupName",'searchStyle1EasyUIDialogCreate','searchStyle1EasyUIDialogDiv',arrayParam);
//			$('#searchStyle1EasyUIDialogDiv #searchStyle1Code').focus();
		},
		openDialogUpdateGroupPOAuto:function(flag,id,groupCode,groupName,status){
			GroupPOAuto.reset();
			var arrayParam = new Array();
			arrayParam.push(flag);
			arrayParam.push(id);
			arrayParam.push(groupCode);
			arrayParam.push(groupName);
			arrayParam.push(status);
			CommonSearch.openSearchStyle1EasyUICreateUpdateGroupPOAuto(null,
					null, "Thông tin nhóm","groupCode",
					"groupName",'searchStyle1EasyUIDialogUpdate','searchStyle1EasyUIDialogDivUpdate',arrayParam);
//			$('#searchStyle1EasyUIDialogDivUpdate #searchStyle1Name').focus();
		},
		updateCategory:function(listCatID){
			var dataModel = new Object();
			dataModel.id = $('#idCode1').val();
			dataModel.lstCategoryId = listCatID;
			Utils.addOrSaveData(dataModel, "/catalog/group-po/addPOAutoGroupDetail", null, 'errMsg',function(data){	
				if(data.errMsg!=undefined && data.errMsg.length==0){
					$('#popup1').dialog('close');
					$("#gridSecGoods").datagrid("reload");
				}
			});
		},
		updateSubCategory:function(listCatID){
			var dataModel = new Object();
			dataModel.id = $('#idCode2').val();
			dataModel.lstCategoryId = listCatID;
			Utils.addOrSaveData(dataModel, "/catalog/group-po/addPOAutoGroupDetailSubCat", null, 'errMsg',function(data){	
				if(data.errMsg!=undefined && data.errMsg.length==0){
					$('#popup2').dialog('close');
					$("#gridSecGoodsBrand").datagrid("reload");
				}
			});
		},
		deleteCategoryRow: function(groupDetailId){
			$('#errMsg').html('').hide();
			var dataModel = new Object();
			dataModel.groupDetailId = groupDetailId;	
			Utils.deleteSelectRowOnGrid(dataModel, 'Ngành hàng', "/catalog/group-po/deleteCategoryRow", GroupPOAuto._xhrDel, null, null,function(data){
				if(data.error == false){
					$("#gridSecGoods").datagrid("reload");
				}
			});
			return false;		
		},
		deleteSubCategoryRow: function(groupDetailId){
			$('#errMsg').html('').hide();
			var dataModel = new Object();
			dataModel.groupDetailId = groupDetailId;	
			Utils.deleteSelectRowOnGrid(dataModel, 'Ngành hàng con', "/catalog/group-po/deleteSubCategoryRow", GroupPOAuto._xhrDel, null, null,function(data){
				if(data.error == false){
					$("#gridSecGoodsBrand").datagrid("reload");
				}
			});
			return false;		
		}
		
};
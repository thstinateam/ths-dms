var ProductLevelCatalog = {
	_xhrSave : null,
	_xhrDel: null,
	_htmlCat: null,
	_htmlSubCat: null,
	_htmlBrand: null,
	_curRowId: null,
	_lstNVTT: null,
	_rowIndexValueField: null,
	_arrayList: null,
	_mapRowIndex: null,
	_callBackAfterImport: null,
	_xhrReport:null,
	getGridUrl: function(code,name,cat,subCat,status,description){
		return "/catalog_level_mng/search?code=" + encodeChar(code) + "&name=" + encodeChar(name) +"&catId=" + cat + "&subCatId=" + subCat + "&note=" + encodeChar(description) + "&status=" + status;
	},	
	search: function(){
		$('#errMsg').html('').hide();
		$('#errExcelMsg').html('').hide();			
		var code = $('#code').val().trim();	
		var name = $('#name').val().trim();
		var cat = $('#category').val().trim();
		var subCat = $('#subCategory').val().trim();			
		var description = $('#description').val().trim();
		var status = $('#status').val().trim();
		var tm = setTimeout(function() {
			$('#code').focus();
		}, 500);
		
		var url = ProductLevelCatalog.getGridUrl(code,name,cat,subCat,status,description);
		$("#grid").setGridParam({url:url,page:1}).trigger("reloadGrid");
		return false;
	},
	save: function(){
		var msg = '';
		$('#errMsg').html('').hide();
		$('#errExcelMsg').html('').hide();	
//		msg = Utils.getMessageOfRequireCheck('code','Mã Mức');
		if(msg.length == 0){
			msg = Utils.getMessageOfSpecialCharactersValidate('code','Mã Mức',Utils._CODE);
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfRequireCheck('name','Tên Mức');
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfSpecialCharactersValidate('name','Tên Mức',Utils._NAME);
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfRequireCheck('category','Mã category',true);
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfRequireCheck('subCategory','Mã sub category',true);
		}			
		if(msg.length == 0){
			msg = Utils.getMessageOfRequireCheck('status','Trạng thái',true);
		}
		if(msg.length == 0 && $('#description').val().trim().length > 0){
			msg = Utils.getMessageOfSpecialCharactersValidate('description','Mô tả');
		}
		if(msg.length > 0){
			$('#errMsg').html(msg).show();
			return false;
		}
		var dataModel = new Object();
		dataModel.id = $('#selId').val().trim();
		dataModel.code = $('#code').val().trim();
		dataModel.name = $('#name').val().trim();
		dataModel.catId = $('#category').val().trim();
		dataModel.subCatId = $('#subCategory').val().trim();			
		dataModel.note = $('#description').val().trim();
		dataModel.status = $('#status').val();
		Utils.addOrSaveRowOnGrid(dataModel, "/catalog_level_mng/save", ProductLevelCatalog._xhrSave, null, null, function(data){
			if(!data.error){
				ProductLevelCatalog.resetForm();
			}
		});			
		return false;
	},
	getSelectedRow: function(rowId,status){	
		ProductLevelCatalog._curRowId = rowId;
		//ProductLevelCatalog.resetAllCombobox();			
		$('#errExcelMsg').html('').hide();
		$('#errMsg').html('').hide();			
		$('#code').attr('disabled','disabled');
		var id =  $("#grid").jqGrid ('getCell', rowId, 'id');
		var catId =  $("#grid").jqGrid ('getCell', rowId, 'cat.id');
		var catCode =  $("#grid").jqGrid ('getCell', rowId, 'cat.productInfoCode');
		var subCatId =  $("#grid").jqGrid ('getCell', rowId, 'subCat.id');
		var subCatCode =  $("#grid").jqGrid ('getCell', rowId, 'subCat.productInfoCode');			
		var description =  $("#grid").jqGrid ('getCell', rowId, 'description');
		var code = $("#grid").jqGrid ('getCell', rowId, 'productLevelCode');
		var name = $("#grid").jqGrid ('getCell', rowId, 'productLevelName');
		if(id!= null && id!= 0 && id!=undefined){
			$('#selId').val(id);
		} else {
			$('#selId').val(0);
		}	
		ProductLevelCatalog.getProductInfo(1,activeType.RUNNING);
		ProductLevelCatalog.getProductInfo(2,activeType.RUNNING);					
		setTextboxValue('code',code);
		setTextboxValue('name',name);
		setTextboxValue('description',description);
		//setSelectBoxValue('category',catId);			
		//setSelectBoxValue('subCategory',subCatId);
		setSelectBoxValue('status',status);		
		$('.RequireStyle').show();
		$('#btnSearch').hide();
		$('#btnAdd').hide();
		$('#btnEdit').show();
		$('#btnCancel').show();
		$('#code').focus();
		setTitleUpdate();
		focusFirstTextbox();
		return false;
	},
	resetForm: function(){
		ProductLevelCatalog.clearData();
		ProductLevelCatalog.search();
		return false;
	},
	deleteRow: function(code){
		//ProductLevelCatalog.clearData();
		var dataModel = new Object();
		dataModel.code = code;	
		Utils.deleteSelectRowOnGrid(dataModel, 'mức', '/catalog_level_mng/delete', ProductLevelCatalog._xhrDel, 'grid', null, function(data){
			ProductLevelCatalog.resetForm();
		});
		$('#errExcelMsg').html('').hide();	
		return false;
	},
	clearData: function(){
		setTitleSearch();
		ProductLevelCatalog._curRowId = null;
		ProductLevelCatalog.getProductInfo(1,activeType.RUNNING);
		ProductLevelCatalog.getProductInfo(2,activeType.RUNNING);
		$('#selId').val(0);
		setSelectBoxValue('category');
		setSelectBoxValue('subCategory');			
		setSelectBoxValue('status',1);
		$('#errMsg').html('').hide();	
		$('#errExcelMsg').html('').hide();	
		$('#code').removeAttr('disabled');
		$('#btnEdit').hide();
		$('#btnCancel').hide();
		$('#btnSearch').show();
		$('#btnAdd').show();
		//ProductLevelCatalog.resetAllCombobox();
		$('#code').val('');			
		$('#name').val('');		
		$('#description').val('');			
		//ProductLevelCatalog.search();
		$('.RequireStyle').hide();
		focusFirstTextbox();
	},
	resetAllCombobox: function(){
		if(ProductLevelCatalog._htmlCat!= null){
			$('#category').html(ProductLevelCatalog._htmlCat);
			$('#category').change();
		}
		if(ProductLevelCatalog._htmlSubCat!= null){
			$('#subCategory').html(ProductLevelCatalog._htmlSubCat);
			$('#subCategory').change();
		}			
	},
	getChangedForm: function(){
		ProductLevelCatalog._curRowId = null;
		ProductLevelCatalog.getProductInfo(1,activeType.RUNNING);
		ProductLevelCatalog.getProductInfo(2,activeType.RUNNING);
		$('.RequireStyle').show();
		$('#btnSearch').hide();
		$('#btnAdd').hide();
		$('#btnEdit').show();
		$('#btnCancel').show();
		$('#code').attr('disabled','disabled');
		$('#name').focus();
		focusFirstTextbox();
		setTitleAdd();
	},
	getProductInfo: function(type, status){
		$.getJSON('/rest/catalog/product-info/list.json?type='+ type +'&status=' + status, function(data){
			var headerText = '';
			var id = '';
			var colId = '';
			var colName = '';
			if(type == 1){
				headerText = 'mã category';
				id = 'category';
				colId = 'cat.id';
				colName = 'cat.productInfoCode';
			} else if(type = 2){
				headerText = 'mã sub category';
				id = 'subCategory';
				colId = 'subCat.id';
				colName = 'subCat.productInfoCode';
			}
			var arrHtml = new Array();
			arrHtml.push('<option value="'+ activeType.ALL +'">--- Chọn '+ Utils.XSSEncode(headerText) +' ---</option>');
			var j=0;
			for(j=0; j<data.length;j++){
				arrHtml.push('<option value="'+ data[j].value +'">'+ Utils.XSSEncode(data[j].name) +'</option>');
			}			
			$('#' + id).html(arrHtml.join(""));
			$('#' + id).change();			
			if(ProductLevelCatalog._curRowId!= null && ProductLevelCatalog._curRowId!= undefined){				
				var gId =  $("#grid").jqGrid ('getCell', ProductLevelCatalog._curRowId, colId);
				var gName =  $("#grid").jqGrid ('getCell', ProductLevelCatalog._curRowId, colName);				
				if($('#'+ id +' option[value='+ gId +']').html()== null){
					var htmlCat = $('#' + id).html() + '<option value="'+ gId +'">'+ Utils.XSSEncode(gName) +'</option>';
					$('#' + id).html(htmlCat);
					$('#' + id).change();					
				}
				setSelectBoxValue(id, gId);
			}
		});
	},
	exportExcelData:function(){
		showLoadingIcon();
		$('#errMsg').html('').hide();
		$('#errExcelMsg').html('').hide();
		var code = $('#code').val().trim();	
		var name = $('#name').val().trim();
		var cat = $('#category').val().trim();
		var subCat = $('#subCategory').val().trim();			
		var description = $('#description').val().trim();
		var status = $('#status').val().trim();
		var data = new Object();
		data.code = code;
		data.name = name;
		data.catId = cat;
		data.subCatId = subCat;
		data.note = description;
		data.status = status;
		var url = "/catalog_level_mng/getlistexceldata";
		CommonSearch.exportExcelData(data,url);
	},
	importExcel:function(){
		$('#isView').val(0);
		$('#importFrm').submit();
		return false;
	},	
	viewExcel:function(){
		$('#isView').val(1);
		$('#importFrm').submit();
		return false;
	},
	afterImportExcelUpdate: function(responseText, statusText, xhr, $form){
		hideLoadingIcon();
		if (statusText == 'success') {				
	    	$("#responseDiv").html(responseText);		 
	    	var newToken = $('#responseDiv #newToken').val();
	    	console.log(newToken);
	    	if(newToken != null && newToken != undefined && newToken != '') {
	    		$('#token').val(newToken);	    		
	    		$('#tokenImport').val(newToken);
	    	}
	    	if($('#errorExcel').html().trim() == 'true' || $('#errorExcelMsg').html().trim().length > 0){
	    		$('#errExcelMsg').html($('#errorExcelMsg').html()).show();
	    	} else {
	    		var totalRow = parseInt($('#totalRow').html().trim());
    			var numFail = parseInt($('#numFail').html().trim());
    			var fileNameFail = $('#fileNameFail').html();
    			var mes = format(msgErr_result_import_excel,(totalRow - numFail),numFail);		    		
    			if(numFail > 0){
    				mes+= ' <a href="'+ fileNameFail +'">Xem chi tiết lỗi</a>';
    			}
    			if($('#excelFile').length!=0 && $('#fakefilepc').length!=0){
    				try{$('#excelFile').val('');$('#fakefilepc').val('');}catch(err){}
    			}
    			$('#errExcelMsg').html(mes).show();
	    	}
	    }
	},
	afterImportExcelUpdateSaleMT: function(responseText, statusText, xhr, $form){
		hideLoadingIcon();
		if (statusText == 'success') {				
	    	$("#responseDiv").html(responseText);
	    	
	    	//LocTT1 - Dec11, 2013
	    	var token = $('#responseToken').html();
	    	$('#token').val(token);
	    	
	    	if($('#errorExcel').html().trim() == 'true' || $('#errorExcelMsg').html().trim().length > 0){
	    		$('#errExcelMsg').html($('#errorExcelMsg').html()).show();
	    	} else {
	    		if($('#typeView').html().trim() == 'false'){
	    			var totalRow = parseInt($('#totalRow').html().trim());
	    			var numFail = parseInt($('#numFail').html().trim());
	    			var fileNameFail = $('#fileNameFail').html();
	    			var mes = format(msgErr_result_import_excel,(totalRow - numFail),numFail);		    		
	    			if(numFail > 0){
	    				mes+= ' <a href="'+ fileNameFail +'">Xem chi tiết lỗi</a>';
	    			}
	    			if($('#excelFile').length!=0 && $('#fakefilepc').length!=0){//xóa link khi import
	    				try{$('#excelFile').val('');$('#fakefilepc').val('');}catch(err){}
	    			}
	    			$('#errExcelMsg').html(mes).show();
//	    			$("#grid").datagrid("loadData");
	    			$("#grid").datagrid("load");
	    			$("#gridLevel").datagrid("load");
	    			if(ProductLevelCatalog._callBackAfterImport != null){
		    			ProductLevelCatalog._callBackAfterImport.call(this);
		    		}
	    		}else{
	    			var obj = '';
	    			if($('#excelType').val() == '' || $('#excelType').val() == undefined ){
	    				obj = '#popup1';
	    			}else{
	    				if(parseInt($('#excelType').val()) == 1){
	    					obj = '#popup1';
	    				}else if(parseInt($('#excelType').val()) == 2){
	    					obj = '#popup2';
	    				}else if(parseInt($('#excelType').val()) == 3){
	    					obj = '#popup3';
	    				}else if(parseInt($('#excelType').val()) == 4){
	    					obj = '#popup4';
	    				}else if(parseInt($('#excelType').val()) == 5){
	    					obj = '#popup5';
	    				}else{
	    					obj = '#popup6';
	    				}
	    			}
	    			var html = $(obj).html();
	    			$.fancybox(html,
    					{
    						modal: true,
    						title: 'Danh sách dữ liệu từ excel',
    						afterShow: function(){
    							$(obj).html('');
    							updateRownumWidthForJqGrid('.fancybox-inner');
    							$('.ScrollSection').jScrollPane().data().jsp;
    							$('.ScrollBodySection').jScrollPane();
    							
    						},
    						afterClose: function(){
    							$(obj).html(html);
    						}
    					}
    				);
	    		}
	    	}
	    }	
	}
};
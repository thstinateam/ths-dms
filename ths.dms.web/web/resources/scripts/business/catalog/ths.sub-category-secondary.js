var SubCategorySecondary = {
	_xhrSave : null,
	_xhrDel: null,
	
	search: function(){
		$('#errMsg').html('').hide();
		var productInfoMap = $('#productInfoMap').val().trim();
		var productInfoCode = $('#productInfoCode').val().trim();
		var productInfoName = $('#productInfoName').val().trim();
		var status = 1;
		var tm = setTimeout(function() {
			$('#productInfoMap').focus();
		}, 500);
		var params = new Object();
		params.productInfoMap = productInfoMap;
		params.productInfoCode = productInfoCode;
		params.productInfoName = productInfoName;
		params.status = status;
		$("#grid").datagrid('load',params);
		return false;
	},
	save: function(){
		var msg = '';
		$('#errMsgPop').html('').hide();
		msg = Utils.getMessageOfRequireCheck('productInfoMapPop','Mã ngành hàng',true);
		if(msg.length == 0){
			msg = Utils.getMessageOfRequireCheck('productInfoCodePop','Mã ngành hàng con phụ');
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfSpecialCharactersValidate('productInfoCodePop','Mã ngành hàng con phụ',Utils._CODE);
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfRequireCheck('productInfoNamePop','Tên ngành hàng con phụ');
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfSpecialCharactersValidate('productInfoNamePop','Tên ngành hàng con phụ');
		}

		if(msg.length > 0){
			$('#errMsgPop').html(msg).show();
			return false;
		}
		var params = new Object();
		params.productInfoId = $('#selId').val().trim();
		params.productInfoMap = $('#productInfoMapPop').val().trim();
		params.productInfoCode = $('#productInfoCodePop').val().trim();
		params.productInfoName = $('#productInfoNamePop').val().trim();
		params.status = 1;
		Utils.addOrSaveData(params, "/catalog/sub_category_sec_catalog/save", SubCategorySecondary._xhrSave, 'errMsgPop',function(data){
			var tm = setTimeout(function() {
			$('#popup1').dialog('close');
				$("#grid").datagrid("reload");
					clearTimeout(tm);
			}, 800);
		});		
		return false;
	},
	addSubCategorySecondary:function(){
		$('#divDialog').css('visibility','visible');
		$('#popup1').dialog({
			title : 'Thông tin ngành hàng con phụ',
			onOpen: function(){
				SubCategorySecondary.clearPop();
			},
			onClose : function() {
				SubCategorySecondary.clearPop();
			}
		});
		$('#popup1').dialog('open');
	},
	getSelectedSubCategorySecondary: function(rowId,productInfoMap, productInfoCode, productInfoName, status){
		if(rowId != null && rowId != 0 && rowId !=undefined){
			$('#selId').val(rowId);
		} else {
			$('#selId').val(0);
		}
		$('#popup1').dialog({
			title : 'Thông tin ngành hàng con phụ',
			onOpen: function(){
				$('#errMsgPop').html('').hide();
				disableSelectbox('productInfoMapPop');
				disabled('productInfoCodePop');
				$('#productInfoMapPop').val(productInfoMap).change();
				$('#productInfoCodePop').val(productInfoCode);
				$('#productInfoNamePop').val(productInfoName);
				$('#productInfoNamePop').focus();
			},
			onClose : function() {
				SubCategorySecondary.clearPop();
			}
		});
        $('#popup1').dialog('open');
		return false;
	},
	resetForm: function(){
		SubCategorySecondary.clearData();
		$("#grid").trigger("reloadGrid");
		return false;
	},
	
	clearData: function(){
		$('#selId').val(0);
		$('#errMsgPop').html('').hide();
		enableSelectbox('productInfoMapPop');
		enable('productInfoCodePop');
		$('#errMsg').html('').hide();
		$('#productInfoMap').val('').change();
		$('#productInfoCode').val('');
		$('#productInfoName').val('');
		$('#productInfoCode').focus();
		focusFirstTextbox();
	},
	clearPop: function(){
		$('#selId').val(0);
		$('#errMsgPop').html('').hide();
		enableSelectbox('productInfoMapPop');
		enable('productInfoCodePop');
		$('#errMsg').html('').hide();
		$('#productInfoCodePop').focus();
		$('#productInfoMapPop').val('').change();
		$('#productInfoCodePop').val('');
		$('#productInfoNamePop').val('');
	},
	getChangedForm: function(){
		$('.RequireStyle').show();
		$('#btnSearch').hide();
		$('#productCode').focus();
		focusFirstTextbox();
	}
};
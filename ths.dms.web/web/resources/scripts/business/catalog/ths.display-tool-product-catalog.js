var DisplayToolProductCatalog = {
	getGridUrl: function(toolCode, productCode) {
		return "/catalog/display-tool-product/search?toolCode=" + encodeChar(toolCode) + "&productCode=" + encodeChar(productCode);
	},
	search: function() {
		var toolCode = $('#toolCode').val().trim();
		var productCode = $('#productCode').val().trim();
		var tm = setTimeout(function() {
			$('#toolCode').focus();
		}, 3000);
		var url = DisplayToolProductCatalog.getGridUrl(toolCode, productCode);
		$("#grid").setGridParam({url:url,page:1}).trigger("reloadGrid");
	},
	add: function() {
		var msg = '';
		msg = Utils.getMessageOfRequireCheck('toolCode', 'Mã tủ');
		if(msg.length > 0) {
			$('#errMsg').html(msg).show();
			var tm = setTimeout(function() {
				$('#errMsg').html('').hide();
			}, 3000);
			return;
		}
		msg = Utils.getMessageOfRequireCheck('productCode', 'Mã sản phẩm');
		if(msg.length > 0) {
			$('#errMsg').html(msg).show();
			var tm = setTimeout(function() {
				$('#errMsg').html('').hide();
			}, 3000);
			return;
		}
		var toolCode = $('#toolCode').val();
		var productCode = $('#productCode').val();
		var params = new Object();
		params.toolCode = toolCode;
		params.productCode = productCode;
		Utils.addOrSaveData(params, '/catalog/display-tool-product/add', null, 'errMsg', function(data) {
			$("#grid").setGridParam({page:1}).trigger("reloadGrid");
			DisplayToolProductCatalog.changeFormCancel();
		});
	},
	del: function(id) {
		var params = new Object();
		params.id = id;
		$.messager.confirm('Xác nhận', 'Bạn có muốn xóa thông tin này?', function(r){
			if(r) {
				Utils.saveData(params, '/catalog/display-tool-product/delete', null, 'errDelMsg', function(data) {
					$("#grid").trigger("reloadGrid");
				}, null, '', true);
			}
		});
	},
	export: function() {
		var toolCode = $('#toolCode').val();
		var productCode = $('#productCode').val();
		var params = new Object();
		params.toolCode = toolCode;
		params.productCode = productCode;
		var url = "/catalog/display-tool-product/export";
		$.ajax({
			type : "POST",
			url : url,
			data :($.param(params, true)),
			dataType : "json",
			success : function(data) {
				if(data.error && data.errMsg != undefined) {
					$('#errMsg').html(data.errMsg).show();
					var tm = setTimeout(function(){
						$('#errMsg').html('').hide();
						clearTimeout(tm);
					}, 3000);
				} else {
					document.location.href = data.view;
				}
			},
			error:function(XMLHttpRequest, textStatus, errorThrown) {
				$.ajax({
					type : "POST",
					url : '/check-session',
					dataType : "json",
					success : function(data) {
					},
					error: function(XMLHttpRequest, textStatus, errorThrown) {
						window.location.href = '/home';
					}
				});
			}
		});
	},
	changeFormAddNew: function() {
		$('#toolCodeRequire').show();
		$('#productCodeRequire').show();
		$('#btnSearch').hide();
		$('#btnAddNew').hide();
		$('#excelExport').hide();
		$('#btnSave').show();
		$('#btnCancel').show();
	},
	changeFormCancel: function() {
		$('#toolCodeRequire').hide();
		$('#productCodeRequire').hide();
		$('#toolCode').val('');
		$('#productCode').val('');
		$('#btnSearch').show();
		$('#btnAddNew').show();
		$('#excelExport').show();
		$('#btnSave').hide();
		$('#btnCancel').hide();
	}
};
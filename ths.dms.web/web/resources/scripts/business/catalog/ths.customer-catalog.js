var CustomerCatalog = {
	me: this,
	_xhrSave : null,
	_xhrDel : null,
	_zoom : 18,
	_DEF_ZOOM: 10,
	_curSaleLevel : null,
	latbk : '',
	lngbk : '',
	BUTTON_TYPE_CANCEL_OPERATION: 0,
	BUTTON_TYPE_DELETE: 1,
	BUTTON_TYPE_CONFIRM_LATER: 2,
	BUTTON_TYPE_CREATE: 3,
	BUTTON_TYPE_DENY: 4,
	BUTTON_TYPE_CREATE_WITH_DUPLICATE_CUSTOMER_CODE: 5,
	STATUS_STOPPED: 0,
	STATUS_RUNNING: 1,
	STATUS_WAITING: 2,
	STATUS_REJECTED: 3,
	selectedDuplicateCustomerCode: '',
	duplicateWithCustomers: null,
	search : function() {
		$('#errMsg').html('').hide();
		var searchParam = CustomerCatalog.getSearchParam();
		$("#customerDatagrid").datagrid('load', searchParam);
		return false;
	},
	DUPLICATE_ADDRESS: 1,
	/**
	 * lay thong so tim kiem khach hang
	 * @author tuannd20
	 * @return {Object} doi tuong chua thong tin tim kiem khach hang
	 * @since 04/09/2015
	 */
	getSearchParam: function() {
		var customerCode = $('#customerCode').val().trim();
		var customerName = $('#customerName').val().trim();
		var customerType = $('#customerType').combobox('getValue').trim().split(' - ')[0];
		var address = $('#address').val().trim();
		var phone = $('#phone').val().trim();
		var status = $('#status').val();
		var shopCode = $('#txtShopCode').val().trim();
		var tansuat = $('#tansuat').val().trim();
		$('#customerCode').focus(); 
		return {
			customerCode: customerCode,
			customerName: customerName, 
			customerType: customerType, 
			address: address, 
			phone: phone, 
			status: status, 
			shopCode: shopCode, 
			tansuat: tansuat
		}
	},
	hideAllTab : function() {
		$('#infoTab').removeClass('Active');
		$('#propertyTab').removeClass('Active');
		$('#catInfoTab').removeClass('Active');
		$('#infoTabContainer').hide();
//		$('#propertyTabContainer').hide();
		$('#catInfoTabContainer').hide();
	},
	showTab1 : function() {
		CustomerCatalog.hideAllTab();
		$('#infoTab').addClass('Active');
		$('#infoTabContainer').show();
	},
	showTab2 : function() {
		if($('#id').val().length == 0) {
			return;
		}
		CustomerCatalog.hideAllTab();
		$('#propertyTab').addClass('Active');
		$('#propertyTabContainer').show();
	},
	showTab3 : function() {
		if($('#id').val().length == 0) {
			return;
		}
		CustomerCatalog.hideAllTab();
		$('#catInfoTab').addClass('Active');
		$('#catInfoTabContainer').show();
		var selId = $('#selId').val();
		$('#grid').datagrid({
			url : '/catalog_customer_mng/search-level-cat?id='+ selId,
			autoRowHeight : true,
			rownumbers : true, 
			pagination:true,
			rowNum : 10,
			fitColumns:true,
			pageList  : [10,20,30],
			scrollbarSize:0,
			width: $('#gridContainer').width(),
			autoWidth: true,
		    columns:[[	        
			    {field:'saleLevelCat.cat.productInfoName', title: catalog_customer_cat, width: 100, sortable:false,resizable:true , align: 'left',
			    	formatter:function(value,object,opts){
			    		return Utils.XSSEncode(object.saleLevelCat.cat.productInfoName);
			    	}},
			    {field:'saleLevelCat.saleLevelName', title: catalog_customer_amount_revenue_limit, width:200,sortable:false,resizable:true , align: 'left',
			    		formatter:function(value,object,opts){
				    		return Utils.XSSEncode(object.saleLevelCat.saleLevelName);
				 }},
			    {field:'saleLevelCat.fromAmount', title: catalog_customer_amount_revenue_from, width: 100, align: 'right', sortable:false,resizable:true,
					 formatter:function(value,object,opts){
				    		return object.saleLevelCat.fromAmount;}},
			    {field:'saleLevelCat.toAmount', title: catalog_customer_amount_revenue_to, width: 80, align: 'right', sortable:false,resizable:true,
				    			formatter:function(value,object,opts){ return object.saleLevelCat.toAmount;}},
			    {field:'edit', title:'', width: 20, align: 'center',sortable:false,resizable:true,
			    	formatter : function(val,__row,idx) {
	            		var catId = __row.id;
	            		return '<a id="group_insert_gr_delete_nhc'+catId+'" href=\"javascript:void(0)\" onclick=\"CustomerCatalog.deleteLevelCatRow('+ catId+'\);\"><img style="width:14px;height:15px;border:0px" src=\"/resources/images/icon_delete.png\"></a>';
	            	}
			    },
			    {field:'id',hidden: true}
		    ]],	    
		    onLoadSuccess :function(data){	    	
		    	$('.datagrid-header-rownumber').html(jsp_common_numerical_order);	    	
		    	updateRownumWidthForJqGrid('.easyui-dialog');
		    	 
		    	var arrEdit = $('#catInfoTabContainer td[field="edit"]');
				if (arrEdit != undefined && arrEdit != null && arrEdit.length > 0) {
				  for (var i = 0, size = arrEdit.length; i < size; i++) {
				  	$(arrEdit[i]).prop("id", "group_insert_table_td_edit_" + i).addClass("cmsiscontrol");
				  }
				}
				Utils.functionAccessFillControl('catInfoTabContainer', function(data){
					//Xu ly cac su kien lien quan den cac control phan quyen
					var arrTmpLength =  $('#catInfoTabContainer td[id^="group_insert_table_td_edit_"]').length;
					var invisibleLenght = $('.isCMSInvisible[id^="group_insert_table_td_edit_"]').length;
					if (arrTmpLength > 0 && arrTmpLength != invisibleLenght) {
						$('#grid').datagrid("showColumn", "edit");
					} else {
						$('#grid').datagrid("hideColumn", "edit");
					}
				});
		    	$(window).resize();    		 
		    }
		});
	},
	getSelectedCustomer : function(id) {
		if (id != undefined && id != null) {
			location.href = '/catalog_customer_mng/change?id=' + id;
		} else {
			location.href = '/catalog_customer_mng/change';
		}
		return false;
	},
	addressChange:function(){
		var number=$('#addressNumber').val().trim();
		var street=$('#street').val().trim();
		if(number!='' && street!=''){
			$('#address').val(number+', '+street);
		}else if(number!=''){
			$('#address').val(number);
		}else if(street!=''){
			$('#address').val(street);
		}
	},
	saveInfo : function() {
		var msg = '';
		$('.SuccessMsgStyle').html('').hide();
		$('.ErrorMsgStyle').html('').hide();
		if (msg.length == 0) {
			msg = Utils.getMessageOfRequireCheck('customerName', catalog_customer_name);
		}
		if (msg.length == 0) {
			msg = Utils.getMessageOfSpecialCharactersValidate('customerName', catalog_customer_name);
		}
		if (msg.length == 0) {
			msg = Utils.getMessageOfInvalidMaxLength('idNumber', 'CMND');
		}
		if (msg.length == 0 && $('#email').val().length > 0) {
			msg = Utils.getMessageOfInvalidEmailFormat('email', catalog_customer_email);
		}
//		if (msg.length == 0 && $('#phone').val().trim()!='' && $('#phone').val().trim()[0]!='0') {
//			msg = 'Số điện thoại bàn phải bắt đầu là 0';
//			$('#phone').focus();
//		}
		if (msg.length == 0 && $('#phone').val().trim()!='' 
			&& ($('#phone').val().trim().length<9 || $('#phone').val().trim().length>12)) {
			msg = 'Số điện thoại bàn phải từ 9 đến 12 chữ số.';
			$('#phone').focus();
		}
//		if (msg.length == 0 && $('#mobilePhone').val().trim()!='' && $('#mobilePhone').val().trim()[0]!='0') {
//			msg = 'Số di động phải bắt đầu là 0';
//			$('#mobilePhone').focus();
//		}
		if (msg.length == 0 && $('#mobilePhone').val().trim()!='' 
			&& ($('#mobilePhone').val().trim().length<9 || $('#mobilePhone').val().trim().length>12)) {
			msg = 'Số di động phải từ 9 đến 12 chữ số.';
			$('#mobilePhone').focus();
		}
		if (msg.length == 0) {
			msg = Utils.getMessageOfRequireCheck('status', jsp_common_status);
		}
		
//		if(msg.length == 0){
//			var area = $('#areaId').val().trim();
//			if (area == null || area == -2) {
//				msg = 'Địa bàn là bắt buộc. Xin vui lòng chọn địa bàn';
//				$('#areaTree').focus();
//			}
//		}
		
		if (msg.length == 0) {
			var prov = $('#province').combobox('getValue');
			if (prov == null || prov.length == 0 || prov == '-2') {
				msg = catalog_customer_province_require;
			}
			$('#province').next().find('.combo-text').focus();
		}
		
		if (msg.length == 0) {
			var prov = $('#district').combobox('getValue');
			if (prov == null || prov.length == 0 || prov == '-2') {
				msg = catalog_customer_district_require;
			}
			$('#district').next().find('.combo-text').focus();
		}
		
		if (msg.length == 0) {
			var prov = $('#precinct').combobox('getValue');
			if (prov == null || prov.length == 0 || prov == '-2') {
				msg = catalog_customer_area_require;
			}
			$('#precinct').next().find('.combo-text').focus();
		}
		
//		if (msg.length == 0) {
//			if($('#areaId').val() == -2){
//				msg = 'Bạn chưa nhập giá trị cho trường Địa bàn.';
//			}
//		}
//		if (msg.length == 0) {
//			msg = Utils.getMessageOfSpecialCharactersValidate('address', jsp_common_address, Utils._ADDRESS);
//		}
//		if (msg.length == 0) {
//			msg = Utils.getMessageOfRequireCheck('address', jsp_common_address);
//		}
		if (msg.length == 0) {
			msg = Utils.getMessageOfSpecialCharactersValidate('street', catalog_customer_street_label, Utils._ADDRESS);
		}
		if (msg.length == 0) {
			msg = Utils.getMessageOfSpecialCharactersValidate('addressNumber', catalog_customer_house_number_label, Utils._ADDRESS);
		}
		if (msg.length == 0) {
			msg = Utils.getMessageOfInvalidMaxLength('mobilePhone', catalog_customer_mobile_phone);
		}
		if (msg.length == 0) {
			msg = Utils.getMessageOfInvalidMaxLength('phone', catalog_customer_telephone);
		}
		if (msg.length == 0) {
			msg = Utils.getMessageOfInvalidMaxLength('email', catalog_customer_email);
		}

		if (msg.length > 0) {
			$('#errMsg').html(msg).show();
			return false;
		}
		var customerName = $('#customerName').val().trim();
		//var provinceCode = $('#provinceCode').val().trim();
		//var districtCode = $('#districtCode').val().trim();
		//var wardCode = $('#wardCode').val().trim();
		//var address = $('#address').val().trim();
		var street = $('#street').val().trim();
		var phone = $('#phone').val().trim();
		var mobilePhone = $('#mobilePhone').val().trim();
		var status = $('#status').val().trim();
		var idNumber = $('#idNumber').val().trim();
		var contact = $('#contact').val().trim();
		var email = $('#email').val().trim();
		var addressNumber = $('#addressNumber').val().trim();
		var areaId = $('#areaId').val();
		var lat = $('#lat').val();
		var lng = $('#lng').val();
		
		var dataModel = new Object();
		dataModel.id = $('#id').val().trim();
		dataModel.customerName = customerName;
		dataModel.contact = contact;
		dataModel.idNumber = idNumber;
		dataModel.phone = phone;
		dataModel.mobilePhone = mobilePhone;
		dataModel.email = email;
		dataModel.status = status;
		dataModel.street = street;
		//dataModel.address = address;
		dataModel.areaId = areaId;
		dataModel.addressNumber = addressNumber;
		dataModel.lat = lat;
		dataModel.lng = lng;
		dataModel.shopCode = $('#txtShopCode').val().trim();
		var b = ($("#id").val().trim().length == 0);
		if(!CustomerCatalog.saveProperty(dataModel)) {
			return;
		}
		
		if(dataModel != null) {
			$.ajax({
				type : "POST",
				url : "/catalog_customer_mng/check-sale-order",
				data :($.param(dataModel, true)),
				dataType : "json",
				success : function(data) {				
					if(data.error){
						$.messager.confirm("Thông báo", data.errMsg, function(r){
							Utils.saveData(dataModel, "/catalog_customer_mng/save-info",CustomerCatalog._xhrSave, null, function(data) {
								$('#id').val(data.id);
								$('#selId').val(data.id);
								$('#shortCode').val(Utils.XSSEncode(data.code));
								setTimeout(function() {
									if (b) {
										window.location.href = '/catalog_customer_mng/change?id='+data.id;
									} else {
										CustomerCatalog.showTab3();
									}
								}, 1000);
							}, null, ' #infoTabContainer ');
						});	
					} else {
						Utils.addOrSaveData(dataModel, "/catalog_customer_mng/save-info",CustomerCatalog._xhrSave, null, function(data) {
							$('#id').val(data.id);
							$('#selId').val(data.id);
							$('#shortCode').val(Utils.XSSEncode(data.code));
							setTimeout(function() {
								if (b) {
									window.location.href = '/catalog_customer_mng/change?id='+data.id;
								} else {
									CustomerCatalog.showTab3();
								}
							}, 1000);
						}, null, ' #infoTabContainer ')
					}
				},
				error:function(XMLHttpRequest, textStatus, errorThrown) {
					$('#divOverlay').hide();
//					window.location.href = '/home';
				}
			});
		}
		
		return false;
	},

	/**
	 * Xoa vi tri KH
	 * @author vuongmq
	 * @since 04/09/2015 
	 */
	deleteCustomerPosition: function(){
		$('.SuccessMsgStyle').html('').hide();
		$('.ErrorMsgStyle').html('').hide();
		var dataModel = new Object();
		dataModel.id = $('#id').val().trim();
		dataModel.lat = $('#lat').val();
		dataModel.lng = $('#lng').val();
		Utils.addOrSaveData(dataModel, "/catalog_customer_mng/delete-customer-position", CustomerCatalog._xhrSave, 'errMsgMap', function(data) {
			$('#lat').val('');
    		$('#lng').val('');
    		ViettelMap.clearOverlays();
    		ViettelMap._listOverlay = new Array(); 
    		if (ViettelMap._marker != null) {
            	ViettelMap._marker.setMap(null);
            }
            $('#successMsgMap').html(jsp_common_delete_success).show();
			setTimeout(function() {
				$('#successMsgMap').html('').hide();
			}, 2000);
		}, null, null, true, null, null, true);
		return false;
	},

	saveProperty : function(dataModel) {
		var parentDiv = '#propertyTabContainer ';
		var msg = '';
		$(parentDiv+'#errMsgProperty').html('').hide();
		if (msg.length == 0) {
			msg = Utils.getMessageOfRequireCheck('customerType', catalog_customer_type, true);
		}
		
		if (msg.length > 0) {
			$(parentDiv+'#errMsg').html(msg).show();
			return false;
		}
		var customerType = $('#customerType').combobox('getValue');
		var deliveryCode = $('#deliveryCode').combobox('getValue');
		var cashierCode = $('#cashierCode').combobox('getValue');
		
//		var customerType = CustomerCatalog.editComboboxX("#customerType");
//		var deliveryCode = CustomerCatalog.editComboboxX("#deliveryCode");
//		var cashierCode = CustomerCatalog.editComboboxX("#cashierCode");
		var applyDebitLimited = $('#CheckapplyDebitLimited').is(":checked") ? 1 : 0;
		if(applyDebitLimited == 1) {
			if($('#maxDebit').val().trim()=='' && $('#debitDate').val().trim()=='' ){
				msg = Utils.getMessageOfRequireCheck('maxDebit', catalog_customer_debit_amount);
			}
			if (msg.length > 0) {
				$("#errMsgProperty").html(msg).show();
				return false;
			}
		}
		var maxDebit = Utils.returnMoneyValue($('#maxDebit').val().trim());
		var debitDate = $('#debitDate').val().trim();
		if(maxDebit.length>0 && isNaN(maxDebit)){
			msg = catalog_customer_debit_amount_must_number;
			$('#maxDebit').focus();
		}
		if(debitDate.length>0 && isNaN(debitDate)){
			msg = catalog_customer_debit_limit_must_number;
			$('#debitDate').focus();
		}
		
		if (msg.length == 0) {
			msg = Utils.getMessageOfInvalidMaxLength('invoiceConpanyName', jsp_common_shop_name);
		}
		if (msg.length == 0) {
			msg = Utils.getMessageOfInvalidMaxLength('invoiceOutletName', 'Người đại diện');
		}
		if (msg.length == 0) {
			msg = Utils.getMessageOfInvalidMaxLength('custDeliveryAddr', 'Địa chỉ hóa đơn');
		}
		if (msg.length == 0) {
			msg = Utils.getMessageOfInvalidMaxLength('invoiceTax', catalog_customer_tax_no);
		}
		if (msg.length == 0) {
			msg = Utils.getMessageOfInvalidMaxLength('invoiceNumberAccount', 'Tài khoản');
		}
		if (msg.length == 0) {
			msg = Utils.getMessageOfInvalidMaxLength('invoiceNameBank', 'Ngân hàng');
		}
		
		if (msg.length > 0) {
			$(parentDiv+'#errMsgProperty').html(msg).show();
			return false;
		}
		var invoiceConpanyName = $('#invoiceConpanyName').val().trim();
		var invoiceOutletName = $('#invoiceOutletName').val().trim();
		var custDeliveryAddr = $('#custDeliveryAddr').val().trim();
		var invoiceTax = $('#invoiceTax').val().trim();
		var invoiceNumberAccount = $('#invoiceNumberAccount').val().trim();
		var invoiceNameBank = $('#invoiceNameBank').val().trim();
		var invoiceNameBranchBank = $('#invoiceNameBranchBank').val().trim();
		var bankAccountOwner = $('#bankAccountOwner').val().trim();
		var isVat = $('#cbxIsVat').is(":checked") ? 1 : 0;
		var birthDay = $('#birthDay').val().trim();
		var tansuat = $('#tansuat').val().trim();
		var startDate = $('#startDate').val().trim();
		var endDate = $('#endDate').val().trim();
		var routingId = $('#cbRouting').combobox('getValue');
		var salePosition = $('#salePosition').combobox('getValue');
		
		dataModel.id = $('#id').val().trim();
		dataModel.customerType = customerType;
		dataModel.deliveryCode = deliveryCode;
		dataModel.cashierCode = cashierCode;
		dataModel.applyDebitLimited = applyDebitLimited;
		dataModel.maxDebit = maxDebit;
		dataModel.debitDate = debitDate;
		dataModel.invoiceConpanyName= invoiceConpanyName;
		dataModel.invoiceOutletName = invoiceOutletName;
		dataModel.custDeliveryAddr = custDeliveryAddr;
		dataModel.invoiceTax = invoiceTax;
		dataModel.invoiceNumberAccount = invoiceNumberAccount;
		dataModel.invoiceNameBank = invoiceNameBank;
		dataModel.invoiceNameBranchBank = invoiceNameBranchBank;
		dataModel.bankAccountOwner = bankAccountOwner;
		dataModel.isVat = isVat;
		dataModel.birthDayStr = birthDay;
		dataModel.tansuat = tansuat;
		dataModel.startDateStr = startDate;
		dataModel.endDateStr = endDate;
		dataModel.routingId = routingId;
		dataModel.customerSalePosition = salePosition;
		
		if(!Utils.validateAttributeData(dataModel, '#errMsgProperty', '#propertyTabContainer ')) {
			return false;
		}
//		Utils.addOrSaveData(dataModel, "/catalog_customer_mng/save-property",
//				CustomerCatalog._xhrSave, 'errMsgProperty', function(data) {
//				}, null, ' #propertyTabContainer ');
		return true;
	},
	saveInvoice : function() {
		var msg = '';
		$('#errMsgBill').html('').hide();
		msg = Utils.getMessageOfRequireCheck('code', catalog_customer_code);
		if (msg.length == 0) {
			msg = Utils.getMessageOfSpecialCharactersValidate('code', catalog_customer_code,
					Utils._CODE);
		}
		if (msg.length == 0) {
			msg = Utils.getMessageOfRequireCheck('name', catalog_customer_name);
		}
		if (msg.length == 0) {
			msg = Utils.getMessageOfSpecialCharactersValidate('name', catalog_customer_name);
		}
		if (msg.length == 0) {
			msg = Utils.getMessageOfRequireCheck('invoiceConpanyName',
					jsp_common_shop_name);
		}
		if (msg.length == 0) {
			msg = Utils.getMessageOfSpecialCharactersValidate(
					'invoiceConpanyName', jsp_common_shop_name);
		}
		if (msg.length == 0) {
			msg = Utils.getMessageOfRequireCheck('invoiceOutletName',
					catalog_customer_full_name);
		}
		if (msg.length == 0) {
			msg = Utils.getMessageOfSpecialCharactersValidate(
					'invoiceOutletName', catalog_customer_full_name);
		}
		if (msg.length == 0) {
			msg = Utils.getMessageOfRequireCheck('custDeliveryAddr', jsp_common_address);
		}
		if (msg.length == 0) {
			msg = Utils.getMessageOfSpecialCharactersValidate(
					'custDeliveryAddr', jsp_common_address);
		}
		if (msg.length == 0 && $('#invoiceNameBank').val().length > 0) {
			msg = Utils.getMessageOfSpecialCharactersValidate(
					'invoiceNameBank', common_bank_name);
		}
		if (msg.length == 0 && $('#invoiceTax').val().trim().length > 0
				&& isNaN($('#invoiceTax').val().trim())) {
			msg = format(msgErr_number, catalog_customer_tax_no);
			$('#invoiceTax').focus();
		}
		if (msg.length == 0 && $('#invoiceNumberAccount').val().trim().length > 0
				&& isNaN($('#invoiceNumberAccount').val().trim())) {
			msg = format(msgErr_number, 'Số TK');
			$('#invoiceNumberAccount').focus();
		}

		if (msg.length > 0) {
			$('#errMsgBill').html(msg).show();
			return false;
		}

		var dataModel = new Object();
		dataModel.id = $('#selId').val().trim();
		dataModel.invoiceConpanyName = $('#invoiceConpanyName').val().trim();
		dataModel.invoiceOutletName = $('#invoiceOutletName').val().trim();
		dataModel.custDeliveryAddr = $('#custDeliveryAddr').val().trim();
		dataModel.invoiceTax = $('#invoiceTax').val().trim();
		dataModel.paymentType = $('#paymentType').val();
		dataModel.invoiceNumberAccount = $('#invoiceNumberAccount').val().trim();
		dataModel.invoiceNameBank = $('#invoiceNameBank').val().trim();
		Utils.addOrSaveData(dataModel, "/catalog_customer_mng/save-invoice",
				CustomerCatalog._xhrSave, 'errMsgBill', function(data) {
					if (data.error) {
						$('#errMsgBill').html(data.errMsg).show();
					} else {
						CustomerCatalog.showTab3();
						showSuccessMsg('successMsgBill', data);
					}
				});
		return false;
	},
	getSaleOrderGridUrl : function(id, shopCode, fromDate, toDate) {
		return "/catalog_customer_mng/search-sale-order?id=" + encodeChar(id)
				+ "&shopCode=" + encodeChar(shopCode) + "&fromDate="
				+ encodeChar(fromDate) + "&toDate=" + encodeChar(toDate);
	},
	searchSaleOrder : function() {
		$('#errMsgSaleOrder').html('').hide();
		var id = $('#selId').val();
		var shopCode = $('#shopCode').val();
		var fromDate = $('#saleOrderFDate').val();
		var toDate = $('#saleOrderTDate').val();
		var url = CustomerCatalog.getSaleOrderGridUrl(id, shopCode, fromDate, toDate);
		$("#saleOrderGrid").setGridParam({
			url : url,
			page : 1
		}).trigger("reloadGrid");
		return false;
	},
	saveLevelCat : function() {
		var msg = '';
		$('#errMsgLevel').html('').hide();
		msg = Utils.getMessageOfRequireCheck('cat', catalog_customer_cat, true);
		if (msg.length == 0) {
			msg = Utils.getMessageOfRequireCheck('saleLevelCat', catalog_customer_amount_revenue_limit, true);
		}

		if (msg.length > 0) {
			$('#errMsgLevel').html(msg).show();
			return false;
		}
		var dataModel = new Object();
		dataModel.id = $('#selId').val().trim();
		dataModel.saleLevelCat = $('#saleLevelCat').val().trim();
		dataModel.saleLevelCatId = $('#saleLevelCatId').val().trim();
		Utils.addOrSaveRowOnGrid(dataModel, "/catalog_customer_mng/save-level-cat", CustomerCatalog._xhrSave, 'catGrid', 'errMsgLevel', function(data) {
			if (!data.error) {
				$('#saleLevelCatId').val(0);
//				$("#catGrid").trigger("reloadGrid");
				$('#grid').datagrid('reload');
				CustomerCatalog._curSaleLevel = null;
				showSuccessMsg('successMsgLevel', data);
				// sontt19:
				$("#cat").val('-2');
				$('#cat').change();
				$('#cat').attr('disabled', false);
				$('#cat').parent().removeClass('BoxDisSelect');
				$("#saleLevelCat").val('-2');
				$('#saleLevelCat').change();
				$('#group_insert_btnAddCat').show();
				$('#btnEditCat').hide();
				$('#btnCancelCat').hide();
				// end-sontt19
			}
		}, 'catLoading');
		return false;
	},
	getSelectedLevelCatRow : function(rowId) {
		var catId = $("#catGrid").jqGrid('getCell', rowId,'saleLevelCat.cat.id');
		var saleLevelId = $("#catGrid").jqGrid('getCell', rowId,'saleLevelCat.id');
		var id = $("#catGrid").jqGrid('getCell', rowId, 'id');
		CustomerCatalog._curSaleLevel = saleLevelId;
		$('#saleLevelCatId').val(id);
		$('#cat').val(catId);
		$('#hidCatId').val(catId);
		// disabled('cat');
		// $('#cat').change();
		$('#group_insert_btnAddCat').hide();
		$('#btnEditCat').show();
		$('#btnCancelCat').show();
		CustomerCatalog.loadedCat(catId);
		// sontt19:
		$('#cat').attr('disabled', true);
		$('#cat').parent().addClass('BoxDisSelect');
		// end-sontt19
		return false;
	},
	loadedCat: function(catId) {
		$.getJSON('/rest/catalog/customer-cat/list/' + catId + '.json', function(data) {
			var arrHtml = new Array();
			arrHtml.push('<option value="-2">-- ' + Utils.XSSEncode(catalog_customer_cat_select) + ' --</option>');
			if (data != null && data.length > 0) {
				for ( var i = 0; i < data.length; i++) {
					arrHtml.push('<option value="' + data[i].value + '">' + Utils.XSSEncode(data[i].name) + '</option>');
				}
			}
			$('#cat').html(arrHtml.join(""));
			$('#cat').val(catId);
			$('#cat').change();
		});
	},
	deleteLevelCatRow : function(id) {
		$('#errMsgLevel').html('').hide();
		var dataModel = new Object();
		dataModel.saleLevelCatId = id;
		Utils.deleteSelectRowOnGrid(dataModel, catalog_customer_sale_cat,
				'/catalog_customer_mng/delete-level-cat', CustomerCatalog._xhrDel,
				'catGrid', 'errMsgCat', function(data) {
					showSuccessMsg('successMsgLevel', data,null,3000,jsp_common_delete_success);
					$('#grid').datagrid('reload');
				}, 'catLoading');
		return false;
	},
	activeOthersTab : function() {
		$('#tabActive2').removeClass('Disable');
		$('#tabActive3').removeClass('Disable');
		$('#tabActive4').removeClass('Disable');
		$('#tabActive5').removeClass('Disable');

		$('#tab2').bind('click', CustomerCatalog.showTab2);
		$('#tab3').bind('click', CustomerCatalog.showTab3);
		$('#tab4').bind('click', CustomerCatalog.showTab4);
		$('#tab5').bind('click', CustomerCatalog.showTab5);

		return false;
	},
	activeTabImage : function() {
		$('#tabActive6').removeClass('Disable');
		$('#tab6').bind('click', CustomerCatalog.showTab6);
	},
	searchActionLog : function() {
		$('#errMsgActionLog').html('').hide();
		var id = $('#selId').val().trim();
		// var shopCode = $('#changedInfoShopCode').val();
		var fromDate = $('#fromDate').val().trim();
		var toDate = $('#toDate').val().trim();
		var msg='';
		if(!Utils.compareDate(fromDate, toDate)){
			msg = msgErr_fromdate_greater_todate;		
			$('#fromDate').focus();
		}
		if(msg.length > 0){
			$('#errMsgActionLog').html(msg).show();
			return false;
		}
		var url = '/catalog_customer_mng/search-info-change?id=' + id
				+ '&fromDate=' + fromDate + '&toDate=' + toDate;
		$("#infoChangeGrid").setGridParam({
			url : url,
			page : 1
		}).trigger("reloadGrid");
	},
	viewDetailActionLog : function(id) {
		$('#detailActionLogDiv').show();
		var url = CustomerCatalog.getGridUrlDetailAction(id);
		if ($('#gbox_infoChangeDetailGrid').html() != null
				&& $('#gbox_infoChangeDetailGrid').html().trim().length > 0) {
			$("#infoChangeDetailGrid").setGridParam({
				url : url,
				page : 1
			}).trigger("reloadGrid");
		} else {
			$("#infoChangeDetailGrid").jqGrid({
				url : CustomerCatalog.getGridUrlDetailAction(id),
				colModel : [ {
					name : 'columnName',
					index : 'columnName',
					label : 'Thông tin thay đổi',
					sortable : false,
					resizable : false,
					align : 'left'
				}, {
					name : 'oldValue',
					index : 'oldValue',
					label : 'Giá trị cũ',
					width : 100,
					sortable : false,
					resizable : false,
					align : 'right'
				}, {
					name : 'newValue',
					index : 'newValue',
					label : 'Giá trị mới',
					width : 100,
					sortable : false,
					resizable : false,
					align : 'right'
				}, {
					name : 'actionDate',
					index : 'actionDate',
					label : 'Ngày thay đổi',
					width : 80,
					align : 'center',
					sortable : false,
					resizable : false,
					formatter : 'date',
					formatoptions : {
						srcformat : 'Y-m-d',
						newformat : 'd/m/Y'
					}
				} ],
				pager : '#infoChangeDetailPager',
				width : ($('#infoChangeDetailContainer').width()),
				gridComplete : function() {
					$('#jqgh_infoChangeDetailGrid_rn').html(jsp_common_numerical_order);
					updateRownumWidthForJqGrid();
				}
			}).navGrid('#infoChangeDetailPager', {
				edit : false,
				add : false,
				del : false,
				search : false
			});
		}
	},
	getGridUrlDetailAction : function(id) {
		return '/catalog_customer_mng/search-info-change-detail?id=' + id;
	},
	cancelCat : function() {
		$('#saleLevelCatId').val(0);
		$("#catGrid").trigger("reloadGrid");
		CustomerCatalog._curSaleLevel = null;
		$('#group_insert_btnAddCat').show();
		$('#btnEditCat').hide();
		$('#btnCancelCat').hide();
		enable('cat');
		// sontt19
		$('#cat').parent().removeClass('BoxDisSelect');
		// end-sontt19
		CustomerCatalog.loadedCat(-2);
	},
	bindShopCodeChange : function() {
		$('#shopCode').bind('change', function() {
			var titleGroupTransfer = '<option value="' + activeType.ALL + '">--- Chọn nhóm giao hàng ---</option>';
			var titleCashier = '<option value="' + activeType.ALL + '">--- NVTT ---</option>';
			if ($('#shopCode').val().trim().length > 0) {
				enable('groupTransfer');
				$('#groupTransfer').change();
				$('#groupTransfer').parent().removeClass('BoxDisSelect');
				enable('cashier');
				$('#cashier').change();
				$('#cashier').parent().removeClass('BoxDisSelect');
				$.getJSON('/rest/catalog_customer_mng/shop/group-transfer/list.json?shopCode=' + encodeChar($('#shopCode').val().trim()), function(data) {
						var selectedValue = $('#groupTransfer').val();
						var arrHtml = new Array();
						arrHtml.push(titleGroupTransfer);
						for ( var i = 0; i < data.length; i++) {
							arrHtml.push('<option value="' + Utils.XSSEncode(data[i].value) + '">' + Utils.XSSEncode(data[i].name) + '</option>');
						}
						$('#groupTransfer').html(arrHtml.join(""));
						$('#groupTransfer').val(selectedValue);
						$('#groupTransfer').change();
				});
				var urlCashier = '/rest/catalog_customer_mng/shop/cashier/list.json?shopCode=' + encodeChar($('#shopCode').val().trim());
				var customerCode = $('#code').val();
				if (customerCode != undefined && customerCode != null && customerCode.trim().length > 0) {
					urlCashier += '&customerCode=' + encodeChar($('#code').val().trim());
				} else {
					urlCashier += '&customerCode=';
				}
				$.getJSON(urlCashier, function(data) {
					var selectedValue = $('#cashier').val();
					var arrHtml = new Array();
					arrHtml.push(titleCashier);
					for ( var i = 0; i < data.length; i++) {
						arrHtml.push('<option value="' + Utils.XSSEncode(data[i].value) + '">' + Utils.XSSEncode(data[i].name) + '</option>');
					}
					$('#cashier').html(arrHtml.join(""));
					$('#cashier').val(selectedValue);
					$('#cashier').change();
				});
			} else {
				disabled('groupTransfer');
				$('#groupTransfer').html(titleGroupTransfer);
				$('#groupTransfer').val(activeType.ALL);
				$('#groupTransfer').change();
				$('#groupTransfer').parent().addClass('BoxDisSelect');
				disabled('cashier');
				$('#cashier').html(titleCashier);
				$('#cashier').val(activeType.ALL);
				$('#cashier').change();
				$('#cashier').parent().addClass('BoxDisSelect');
			}
		});
	},
	bindShopCodeChangeDisable : function() {
		$('#shopCode').bind('change', function() {
			var titleGroupTransfer = '<option value="' + activeType.ALL + '">--- Chọn nhóm giao hàng ---</option>';
			var titleCashier = '<option value="' + activeType.ALL + '">--- NVTT ---</option>';
			if ($('#shopCode').val().trim().length > 0) {
				enable('groupTransfer');
				$('#groupTransfer').change();
				$('#groupTransfer').parent().removeClass('BoxDisSelect');
				enable('cashier');
				$('#cashier').change();
				$('#cashier').parent().removeClass('BoxDisSelect');
				$.getJSON('/rest/catalog_customer_mng/shop/group-transfer/list.json?shopCode=' + encodeChar($('#shopCode').val().trim()), function(data) {
					var selectedValue = $('#groupTransfer').val();
					var arrHtml = new Array();
					arrHtml.push(titleGroupTransfer);
					for ( var i = 0; i < data.length; i++) {
						arrHtml.push('<option value="' + Utils.XSSEncode(data[i].value) + '">' + Utils.XSSEncode(data[i].name) + '</option>');
					}
					$('#groupTransfer').html(arrHtml.join(""));
					$('#groupTransfer').val(selectedValue);
					$('#groupTransfer').change();
				});
				var urlCashier = '/rest/catalog_customer_mng/shop/cashier/list.json?shopCode=' + encodeChar($('#shopCode').val().trim());
				var customerCode = $('#code').val();
				if (customerCode != undefined && customerCode != null && customerCode.trim().length > 0) {
					urlCashier += '&customerCode=' + encodeChar($('#code').val().trim());
				} else {
					urlCashier += '&customerCode=';
				}
				$.getJSON(urlCashier, function(data) {
					var selectedValue = $('#cashier').val();
					var arrHtml = new Array();
					arrHtml.push(titleCashier);
					for ( var i = 0; i < data.length; i++) {
						arrHtml.push('<option value="' + data[i].value + '">' + Utils.XSSEncode(data[i].name) + '</option>');
					}
					$('#cashier').html(arrHtml.join(""));
					$('#cashier').val(selectedValue);
					$('#cashier').change();
				});
			} else {
				disabled('groupTransfer');
				$('#groupTransfer').html(titleGroupTransfer);
				$('#groupTransfer').val(activeType.ALL);
				$('#groupTransfer').change();
				$('#groupTransfer').parent().addClass('BoxDisSelect');
				disabled('cashier');
				$('#cashier').html(titleCashier);
				$('#cashier').val(activeType.ALL);
				$('#cashier').change();
				$('#cashier').parent().addClass('BoxDisSelect');
			}
		});
	},
	getDate:function(d){
		var result='';
		try{
			var day = new Date(d);
			result+=day.getDate()+'/'+(day.getMonth()+1)+'/'+day.getFullYear()+' - '+day.getHours()+':'+day.getMinutes();
			return result;
		}catch(e){
			return '';
		}
	},
	showCustomerImageDetail : function(objectType) {
		var custId = $('#selId').val().trim();
		var params = new Object();
		params.customerId = custId;
		params.objectType = objectType;
		CommonSearch.viewCustomerImageInfo(params,function(data) {
			CustomerCatalog.imageLoadingShow();
			if (data.error == undefined || data.error == true) {
				$('#errMsg').html(myData.errMsg);
			} else {
				$('.fancybox-title').html($('.fancybox-title').html()+' ('+data.list.length+')');
				var widhei = CustomerCatalog.calWidthHeight(data.item.width + '-' + data.item.height);
				data.item.width = widhei.split('-')[0];
				data.item.height = widhei.split('-')[1];
				if (document.images) {
					var img = new Image();
					img.src = data.item.url;
					img.onload = function() {
						$('.fancybox-inner #imageBig').attr('src', data.item.url);
						$('.fancybox-inner #imageBig').attr('width', data.item.width);
						$('.fancybox-inner #imageBig').attr('height', data.item.height);
						CustomerCatalog.imageBigShow();
					};
				}
				$('.fancybox-inner #itemImageCreateUser').html(data.item.staff.staffCode + ' &ndash; ' + Utils.XSSEncode(data.item.staff.staffName));
				$('.fancybox-inner #itemImageCreateDate').html(CustomerCatalog.getDate(data.item.createDate));
				var lat = data.item.lat;
				var lng = data.item.lng;
				if (lat > 0 && lng > 0) {
					CustomerCatalog.openMapOnImage(lat, lng);
				} else {
					$('#itemImageMap').html('Chưa cập nhật tọa độ cho đơn vị.');
				}
				var listImage = '<ul class="ResetList PhotoCols2List">';
				for (i = 0; i < data.list.length; i++) {
					if (data.list[i].id == data.item.id) {
						listImage += "<li id='li_" + data.list[i].id + "' class='FocusStyle'>";
					} else {
						listImage += "<li id='li_" + data.list[i].id + "'>";
					}
					listImage += "	<div class='PhotoThumbnails'><span class='BoxFrame'><span class='BoxMiddle'>";
					listImage += "       <img class='imageSelect' data-original='" + data.list[i].thumbUrl
											+ "' src='/resources/images/grey.jpg' width='192' height='126' name='thumbUrl_"
											+ data.list[i].id + "' ></span></span></div>";
					listImage += "	<p id='createDate_"
											+ data.list[i].id + "'>"
											+ CustomerCatalog.getDate(data.list[i].createDate) + "</p>";
					listImage += "	<p id='createUser_" + data.list[i].id + "' style='display:none' >"
											+ data.list[i].staff.staffCode + " &ndash; "
											+ data.list[i].staff.staffName + "</p>";
					listImage += "	<p id='url_" + data.list[i].id + "' style='display:none' >" + data.list[i].url + "</p>";
					listImage += "	<p id='widthheight_" + data.list[i].id
											+ "' style='display:none' >" + data.list[i].width + '-'
											+ data.list[i].height + "</p>";
					listImage += "	<p id='latlng_" + data.list[i].id
											+ "' style='display:none' >" + data.list[i].lat + '-'
											+ data.list[i].lng + "</p>";
					listImage += "</li>";
				}
				listImage += '</ul>';
				$('#listImage').html(listImage);
				$(".imageSelect").lazyload({
					effect : "fadeIn",
					container : $("#listImage")
				});
				$('#listImage').jScrollPane();

				$('.imageSelect').click(CustomerCatalog.imageSelect);
			}
		});
	},
	imageSelect : function() {
		try {
			CustomerCatalog.imageLoadingShow();
			var imageId = $(this).attr('name').split('_')[1];
			if (imageId != null && imageId != '' && imageId != undefined) {
				$('.FocusStyle').removeClass('FocusStyle');
				$('#li_' + imageId).addClass('FocusStyle');
				var wihe = $('.fancybox-inner #widthheight_' + imageId).html();
				wihe = CustomerCatalog.calWidthHeight(wihe);
				var width = wihe.split('-')[0].trim();
				var height = wihe.split('-')[1].trim();
				if (document.images) {
					var img = new Image();
					img.src = $('#url_' + imageId).html();
					img.onload = function() {
						$('.fancybox-inner #imageBig').attr('width', width);
						$('.fancybox-inner #imageBig').attr('height', height);
						$('.fancybox-inner #imageBig').attr('src',
								$('#url_' + imageId).html());
						CustomerCatalog.imageBigShow();
					};
				}
				$('.fancybox-inner #itemImageCreateUser').html(
						$('#createUser_' + imageId).html());
				$('.fancybox-inner #itemImageCreateDate').html(
						$('#createDate_' + imageId).html());
				var latlng = $('.fancybox-inner #latlng_' + imageId).html();
				var lat = latlng.split('-')[0].trim();
				var lng = latlng.split('-')[1].trim();
				CustomerCatalog.openMapOnImage(lat, lng);
			}
		} catch (err) {
			return;
		}
	},
	imageBigShow : function() {
		$('.fancybox-inner #imageLoading').hide();
		$('.fancybox-inner #imageBig').fadeIn(1000);
	},
	imageLoadingShow : function() {
		$('.fancybox-inner #imageBig').hide();
		$('.fancybox-inner #imageLoading').show();
	},
	openMapOnImage : function(lat, lng) {
		MapUtil.loadMapResource(function() {
			ViettelMap.loadMapImage('itemImageMap', lat, lng,
					CustomerCatalog._zoom);
		});
	},
	calWidthHeight : function(wh) {
		var defWidth = 580;
		var defHeight = 426;
		try {
			var width = parseFloat(wh.split('-')[0].trim());
			var height = parseFloat(wh.split('-')[1].trim());
			var percent;
			if (width > defWidth) {
				percent = width / defWidth;
				width = defWidth;
				height = height / percent;
			}
			if (height > defHeight) {
				percent = height / defHeight;
				height = defHeight;
				width = width / percent;
			}
			return width + '-' + height;
		} catch (err) {
			return defWidth + '-' + defHeight;
		}
	},
	extendMap : function(flag) {
		if (flag == 1) {
			$('#extendMapOn').hide();
			$('#itemImageMap').show("slow");
			$('#extendMapOff').show();
		} else {
			$('#extendMapOff').hide();
			$('#itemImageMap').hide("slow");
			$('#extendMapOn').show();
		}
	},
	loadTreeEx : function() {
		CustomerCatalog.loadShopTree();
		$('#tree').bind("loaded.jstree", function(event, data) {
			if ($('#treeContainer').data("jsp") != undefined) {
				$('#treeContainer').data("jsp").destroy();
			}
			$('#treeContainer').jScrollPane();
		});
	},
	loadShopTree : function() {
		$('#tree').jstree({
							"plugins" : [ "themes", "json_data", "ui" ],
							"themes" : {
								"theme" : "classic",
								"icons" : false,
								"dots" : true
							},
							"json_data" : {
								"ajax" : {
									"url" : function(node) {
										if (node == -1) {
											return "/rest/catalog/unit-tree-ex/" + $('#shopIdHidden').val().trim() + "/0/tree.json";
										} else {
											return "/rest/catalog/unit-tree-ex/" + node.data("attr").id + "/1/tree.json";
										}
									},
									"type" : 'GET',
									"success" : function(ops) {
										data = [];
										for (opnum in ops) {
											var op = ops[opnum];
											node = {
												"data" : op.data,
												"attr" : op.attr,
												"metadata" : op,
												"state" : op.state
											};
											data.push(node);
										}
										return data;
									},
									'complete' : function() {
										$('#tree').bind("select_node.jstree", function(event, data) {
											var name = data.inst.get_text(data.rslt.obj);
											var id = data.rslt.obj.attr("id");
											var code = data.rslt.obj.attr("contentItemId");
											var url = CustomerCatalog.getGridUrl('','','','','','','','',activeType.ALL,activeType.RUNNING,activeType.ALL,activeType.ALL,activeType.ALL,activeType.ALL,code,'',activeType.ALL);
											$("#grid").setGridParam({url : url, page : 1}).trigger("reloadGrid");
										});
										$('#tree').bind("loaded.jstree", function(event, data) {
											/*
											 * if($('#treeContainer').data("jsp")!=
											 * undefined){
											 * $('#treeContainer').data("jsp").destroy(); }
											 * $('#treeContainer').jScrollPane();
											 */
											if ($('#treeContainer').data("jsp") != undefined) {
												$('#treeContainer').data("jsp").destroy();
											}
											$('#treeContainer').css('height', $('.Content').height() - 55);
											$('#treeContainer').jScrollPane();
										});
										setTimeout(function() {
											if ($('#treeContainer').data("jsp") != undefined) {
												$('#treeContainer').data("jsp").destroy();
											}
											$('#treeContainer').css('height', $('.Content').height() - 75);
												$('#treeContainer').jScrollPane();
										}, 500);
									}
								}
							},
							"core" : {
								"load_open" : true
							}
						});
	},
	viewBigMap : function() {
		var params = new Object();
		params.lat = $('#lat').val().trim();
		params.lng = $('#lng').val().trim();
		var hasPm = $('#hasEditPermission').val().trim();
		if (hasPm == 'true') {
			CustomerCatalog.viewBigMapOnDlg(params);
		} else {
			$('#imgBtnDeleteLatLng, #imgBtnUpdateLatLng, #imgBtnCancelLatLng').hide();
			CustomerCatalog.viewBigMapDisableClick(params);
		}
	},
	importExcel : function() {
		$('#isView').val(0);
		$('#importFrm').submit();
		return false;
	},
	viewExcel : function() {
		$('#isView').val(1);
		$('#importFrm').submit();
		return false;
	},
	beforeImportExcel: function() {
		if (!previewImportExcelFile(document.getElementById("excelFile"))) {
			return false;
		}
		$('#errExcelMsg').hide();
		$('#divOverlay').addClass('Overlay');
		$('#divOverlay').show();
		$('#imgOverlay').show();
		return true;
	},
	afterImportExcel2 : function(responseText, statusText, xhr, $form) {
		$('#divOverlay').removeClass('Overlay');
		$('#imgOverlay').hide();
		$('#divOverlay').hide();
		if (statusText == 'success') {
			$("#responseDiv").html(responseText);
			var newToken = $('#responseDiv #newToken').val();
	    	if (newToken != null && newToken != undefined && newToken != '') {
	    		$('#token').val(newToken);
	    		$('#tokenImport').val(newToken);
	    	}
			if ($('#errorExcel').html().trim() == 'true' || $('#errorExcelMsg').html().trim().length > 0) {
				$('#errExcelMsg').html($('#errorExcelMsg').html()).show();
			} else {
				if ($('#typeView').html().trim() == 'false') {
					var totalRow = parseInt($('#totalRow').html().trim());
					var numFail = parseInt($('#numFail').html().trim());
					var fileNameFail = $('#fileNameFail').html();
					var mes = format(msgErr_result_import_excel,(totalRow - numFail), numFail);
					if (numFail > 0) {
						mes += ' <a href="' + fileNameFail + '">Xem chi tiết lỗi</a>';
					}
					$('#errExcelMsg').html(mes).show();
					$("#grid").trigger("reloadGrid");
					$('#customerDatagrid').datagrid('reload');
				} else {
					$('#excelDialog').html(responseText);
					var html = $('#excelDialog').html();
					$.fancybox(html, {
						modal : true,
						title : 'Thông tin import KH',
						afterShow : function() {
							//$('#excelDialog').html(responseText);
							$('.fancybox-inner #scrollExcelDialog').jScrollPane();
							$('.fancybox-inner .ScrollSection').jScrollPane().data().jsp;
						},
						afterClose : function() {
							$('#excelDialog').html(html);
						}
					});
				}
			}
			$('#fakefilepc').val('');
			$('#excelFile').val('');
		}
	},
	afterImportExcel3 : function(responseText, statusText, xhr, $form) {
		$('#divOverlay').removeClass('Overlay');
		$('#imgOverlay').hide();
		$('#divOverlay').hide();
		if (statusText == 'success') {
			$("#responseDiv").html(responseText);
			var newToken = $('#responseDiv #newToken').val();
	    	if(newToken != null && newToken != undefined && newToken != '') {
	    		$('#token').val(newToken);
	    		$('#tokenImport').val(newToken);
	    	}
			if ($('#errorExcel').html().trim() == 'true'
					|| $('#errorExcelMsg').html().trim().length > 0) {
				$('#errExcelMsg').html($('#errorExcelMsg').html()).show();
			} else {
				if ($('#typeView').html().trim() == 'false') {
					var totalRow = parseInt($('#totalRow').html().trim());
					var numFail = parseInt($('#numFail').html().trim());
					var fileNameFail = $('#fileNameFail').html();
					var mes = format(msgErr_result_import_excel,
							(totalRow - numFail), numFail);
					if (numFail > 0) {
						mes += ' <a href="' + fileNameFail
								+ '">Xem chi tiết lỗi</a>';
					}
					$('#errExcelMsg').html(mes).show();
					$("#grid").trigger("reloadGrid");
					$('#customerDatagrid').datagrid('reload');
				} else {
					$('#excelDialog').html(responseText);
					var html = $('#excelDialog').html();
					$.fancybox(html, {
						modal : true,
						title : catalog_customer_import_cat_info,
						afterShow : function() {
							//$('#excelDialogSub').html(responseText);
							$('.fancybox-inner #scrollExcelDialog')
									.jScrollPane();
							$('.fancybox-inner .ScrollSection').jScrollPane()
									.data().jsp;
						},
						afterClose : function() {
							$('#excelDialog').html(html);
						}
					});
				}
			}
		}
		$("#file").val("");
		$("#fakefilepc").val("");
	},
	exportExcelCustomer : function() {
		$('#divOverlay').addClass('Overlay');
		$('#imgOverlay').show();
		$('#errMsg').html('').hide();
		$('#errExcelMsg').html('').hide();
		var type = $('#excelType').val().trim();
		$('#errMsg').html('').hide();
		var shopCode = $('#txtShopCode').val().trim();
		var customerCode = $('#customerCode').val().trim();
		var customerName = $('#customerName').val().trim();
		var customerType = $('#customerType').combobox('getValue').trim().split(' - ')[0];
		var address = $('#address').val().trim();
		var phone = $('#phone').val().trim();
		var status = $('#status').val();
		var tansuat = $('#tansuat').val().trim();
		
		var params = {shopCode:shopCode, customerCode:customerCode, customerName:customerName, customerType:customerType, address:address, phone:phone, status:status, tansuat:tansuat};
		if (type == 1) {
			var datagirdOptions = $("#customerDatagrid").datagrid('options');
			if (datagirdOptions.sortName) {
				params.sort = datagirdOptions.sortName;
			}
			if (datagirdOptions.sortOrder) {
				params.order = datagirdOptions.sortOrder;
			}
			var url = "/catalog_customer_mng/export-Excel-Customer";
			ReportUtils.exportReport(url, params);
		}
		if (type == 2) {
			var url = "/catalog_customer_mng/export-Excel-Customer-Category";
			ReportUtils.exportReport(url, params);
		}
		return false;
	},
	getExportExcelCustomerUrl : function(code, name, provinceCode,
			districtCode, address, street, phone, mobilePhone, customerType,
			status, region, loyalty, groupTransfer, cashier, shopCode,
			shopCodeLike, display) {
		if (parseInt(loyalty) == activeType.ALL) {
			loyalty = '';
		}
		if (parseInt(region) == activeType.ALL) {
			region = '';
		}
		if (parseInt(display) == activeType.ALL) {
			display = '';
		}
		return "/catalog_customer_mng/export-Excel-Customer?code="
				+ encodeChar(code) + "&name=" + encodeChar(name)
				+ "&provinceCode=" + encodeChar(provinceCode)
				+ "&districtCode=" + encodeChar(districtCode) + "&address="
				+ encodeChar(address) + "&street=" + encodeChar(street)
				+ "&phone=" + encodeChar(phone) + "&mobilePhone="
				+ encodeChar(mobilePhone) + "&customerType="
				+ encodeChar(customerType) + "&status=" + status + "&region="
				+ encodeChar(region) + "&loyalty=" + encodeChar(loyalty)
				+ "&groupTransfer=" + encodeChar(groupTransfer) + "&cashierId="
				+ encodeChar(cashier) + "&shopCode=" + encodeChar(shopCode)
				+ '&shopCodeLike=' + encodeChar(shopCodeLike) + '&display='
				+ encodeChar(display);
	},
	getExportExcelCustomerCategoryUrl : function(code, name, provinceCode,
			districtCode, address, street, phone, mobilePhone, customerType,
			status, region, loyalty, groupTransfer, cashier, shopCode,
			shopCodeLike, display) {
		if (parseInt(loyalty) == activeType.ALL) {
			loyalty = '';
		}
		if (parseInt(region) == activeType.ALL) {
			region = '';
		}
		if (parseInt(display) == activeType.ALL) {
			display = '';
		}
		return "/catalog_customer_mng/export-Excel-Customer-Category?code="
				+ encodeChar(code) + "&name=" + encodeChar(name)
				+ "&provinceCode=" + encodeChar(provinceCode)
				+ "&districtCode=" + encodeChar(districtCode) + "&address="
				+ encodeChar(address) + "&street=" + encodeChar(street)
				+ "&phone=" + encodeChar(phone) + "&mobilePhone="
				+ encodeChar(mobilePhone) + "&customerType="
				+ encodeChar(customerType) + "&status=" + status + "&region="
				+ encodeChar(region) + "&loyalty=" + encodeChar(loyalty)
				+ "&groupTransfer=" + encodeChar(groupTransfer) + "&cashierId="
				+ encodeChar(cashier) + "&shopCode=" + encodeChar(shopCode)
				+ '&shopCodeLike=' + encodeChar(shopCodeLike) + '&display='
				+ encodeChar(display);
	},
	deactiveAllMainTab: function(){
		$('#tab1 a').removeClass('Active');
		$('#tab2 a').removeClass('Active');
		$('#tabContent1').hide();
		$('#tabContent2').hide();
	},
	exportActionLog: function(){
        var id = $('#selId').val();
        var fromDate = $('#fromDate').val();
        var toDate = $('#toDate').val();
        ExportActionLog.exportActionLog(id, ExportActionLog.CUSTOMER, fromDate, toDate,'errMsgActionLog');
 	},
 	checkAll: function(check){
 		if(check.checked) {
 			$("#checkAll").prop("checked", true);
 			$(".ck").prop("checked", true);
 		} else if(!check.checked) {
 			$(".ck").prop("checked", false);
 			$("#checkAll").prop("checked", false);
 		}
 	},
 	loadAreaInfoAndMap: function(data) {
 		//var provinceCode = data.provinceCode;
		var provinceName = data.provinceName;
		//var districtCode = data.districtCode;
		var districtName = data.districtName;
		//var precinctCode = data.precinctCode;
		var precinctName = data.precinctName;
		$('#provinceName').val(provinceName);
		$('#districtName').val(districtName);
		$('#wardName').val(precinctName);
		var lat = 16.1060393;
		var lng = 107.1976608455;
		var pt = new viettel.LatLng(lat, lng);
		ViettelMap._map.setZoom(ViettelMap._DEF_ALL_ZOOM);
		ViettelMap._map.setCenter(pt);
 	},
 	editComboboxX: function(param){
 		var tmpString = $(param).combobox('getValue');
 		tmpString = tmpString.substring(0, tmpString.indexOf('-')).trim();
 		if (tmpString == ''){
 			tmpString = $(param).combobox('getValue').trim();
 		}
 		if (tmpString.indexOf('--Chọn') >= 0){
 			
 		}
 		return tmpString;
 	},
 	
 	viewBigMapOnDlg : function(arrParam) {
 		CustomerCatalog.latbk = arrParam.lat;
		CustomerCatalog.lngbk = arrParam.lng;
		var html = $('#viewBigMap').html();
		$('#viewBigMap').dialog({
			closed: false, 
			title : xem_ban_do,
	        cache: false,  
	        modal: true,
	        onOpen: function(){
	        	$('.easyui-dialog #imgBtnDeleteLatLng').unbind('click');
	        	$('.easyui-dialog #imgBtnDeleteLatLng').bind('click', function(){
	        		$('#lat').val('');
	        		$('#lng').val('');
	        		ViettelMap.clearOverlays();
	        		ViettelMap._listOverlay = new Array(); 
	        		if (ViettelMap._marker != null) {
	                	ViettelMap._marker.setMap(null);
	                }	                
	        	});
	        	$('.easyui-dialog #imgBtnUpdateLatLng').unbind('click');
	        	$('.easyui-dialog #imgBtnUpdateLatLng').bind('click', function(){
		    		$('#viewBigMap').dialog('close');
	        	});
	        	$('.easyui-dialog #imgBtnCancelLatLng').unbind('click');
	        	$('.easyui-dialog #imgBtnCancelLatLng').bind('click', function(){
	        		var zoom = 12;
					if (CustomerCatalog.latbk.trim().length == 0 || CustomerCatalog.lngbk.trim().length == 0) {
						zoom = ViettelMap._DEF_ALL_ZOOM;
					}
					$('#lat').val(CustomerCatalog.latbk);
					$('#lng').val(CustomerCatalog.lngbk);
					VTMapUtil.loadMapResource(function(){
						ViettelMap.loadBigMapEx('bigMapContainer', CustomerCatalog.latbk, CustomerCatalog.lngbk, zoom, function(latitude, longtitude) {
							$('#lat').val(latitude);
							$('#lng').val(longtitude);
						}, true, true);
		    		});
	        		//$('#viewBigMap').onClose();
	        	});
	        	var tm = setTimeout(function(){//chuyen ban do toi vi tri man hinh
					//load map
					var lat = arrParam.lat;
					var lng = arrParam.lng;
					var zoom = 12;
					if(lat.trim().length==0 || lng.trim().length==0){
						zoom = ViettelMap._DEF_ALL_ZOOM;
					}
					VTMapUtil.loadMapResource(function(){
						ViettelMap.loadBigMapEx('bigMapContainer', lat, lng, zoom, function(latitude, longtitude) {
							$('#lat').val(latitude);
							$('#lng').val(longtitude);
						}, true);
//						if (lat == 'null' || lng == 'null') {
//							var centerCode = $('#precinct option:selected').attr('center_code');
//							var type = 3;
//							if (centerCode == undefined || centerCode == null || centerCode.trim().length == 0) {
//								centerCode = $('#district option:selected').attr('center_code');
//								type = 2;
//								if (centerCode == undefined || centerCode == null || centerCode.trim().length == 0) {
//									centerCode = $('#province option:selected').attr('center_code');
//									type = 1;
//								}
//							}
//							if (centerCode != undefined && centerCode != null && centerCode.trim().length > 0) {
//								CustomerCatalog.loadMapByArea(centerCode.trim(), type);
//							}
//						}
		    		});
					clearTimeout(tm);
				}, 500);
	        },
	        onClose:function() {
	        	CustomerCatalog.showMapContent();
	        	$('#viewBigMap').html(html);
	        }
		});
		
	},
	showMapContent:function(){
		var lat = $('#lat').val().trim();
		var lng = $('#lng').val().trim();
		var zoom = 10;
		if (lat.length == 0 && lng.length == 0) {
			zoom = ViettelMap._DEF_ALL_ZOOM;
		}
		$('#mapContainer').html('');
		VTMapUtil.loadMapResource(function(){
			ViettelMap.loadBigMapEx('mapContainer', lat, lng, zoom, function(latitude, longtitude) {
				$('#lat').val(latitude);
				$('#lng').val(longtitude);
			}, true);
		});
	},
	viewBigMapDisableClick : function(arrParam) {
		var html = $('#viewBigMap').html();
		$('#viewBigMap').dialog({
			closed: false,  
	        cache: false,  
	        modal: true,
	        onOpen: function(){
	        	var tm = setTimeout(function(){//chuyen ban do toi vi tri man hinh
					//load map
					var lat = arrParam.lat;
					var lng = arrParam.lng;
					VTMapUtil.loadMapResource(function(){
						ViettelMap.loadBigMapEx('bigMapContainer', lat, lng, CustomerCatalog._DEF_ZOOM, null);
						/*if (lat == 'null' || lng == 'null') {
							var centerCode = $('#precinct option:selected').attr('center_code');
							var type = 3;
							if (centerCode == undefined || centerCode == null || centerCode.trim().length == 0) {
								centerCode = $('#district option:selected').attr('center_code');
								type = 2;
								if (centerCode == undefined || centerCode == null || centerCode.trim().length == 0) {
									centerCode = $('#province option:selected').attr('center_code');
									type = 1;
								}
							}
							if (centerCode != undefined && centerCode != null && centerCode.trim().length > 0) {
								CustomerCatalog.loadMapByArea(centerCode.trim(), type);
							}
						}*/
		    		});
					clearTimeout(tm);
				}, 500);
	        },
	        onClose:function() {
	        	$('#viewBigMap').html(html);
	        }
		});
	},
 	exportExcelTemplate:function(){
 		$('#divOverlay').show();
		var url='/catalog_customer_mng/export-Template-Excel-Customer';
		if(CommonSearch._xhrReport != null){
			CommonSearch._xhrReport.abort();
			CommonSearch._xhrReport = null;
		}
		var params = new Object();
		try {
			params.reportCode = $('#function_code').val();
		} catch(e) {
			// Oop! Error
		}
		var kData = $.param(params, true);
		CommonSearch._xhrReport = $.ajax({
			type : "POST",
			url : url,
			data :(kData),
			dataType: "json",
			success : function(data) {
				$('#divOverlay').hide();
				if(data.error && data.errMsg!= undefined){
					$('#'+ errMsg).html(formatString(msgCommon5, escapeSpecialChar(data.errMsg))).show();
				} else{
					if(data.hasData!= undefined && !data.hasData) {
						$('#'+ errMsg).html(msgCommon6).show();
					} else{
						var filePath = ReportUtils.buildReportFilePath(data.view);
						window.location.href = filePath;
						setTimeout(function(){ //Set timeout để đảm bảo file load lên hoàn tất
	                        CommonSearch.deleteFileExcelExport(data.view);
	                  },30000);
					}
				}
			},
			error:function(XMLHttpRequest, textStatus, errorDivThrown) {				
			}
		});
		return false;
	},
	exportExcelTemplateCategory:function(){
		$('#divOverlay').show();
		var url='/catalog_customer_mng/export-Template-Excel-Category';
		if(CommonSearch._xhrReport != null){
			CommonSearch._xhrReport.abort();
			CommonSearch._xhrReport = null;
		}
		var params = new Object();
		try {
			params.reportCode = $('#function_code').val();
		} catch(e) {
			// Oop! Error
		}
		var kData = $.param(params, true);
		CommonSearch._xhrReport = $.ajax({
			type : "POST",
			url : url,
			data :(kData),
			dataType: "json",
			success : function(data) {
				$('#divOverlay').hide();
				if(data.error && data.errMsg!= undefined){
					$('#'+ errMsg).html(formatString(msgCommon5, escapeSpecialChar(data.errMsg))).show();
				} else{
					if(data.hasData!= undefined && !data.hasData) {
						$('#'+ errMsg).html(msgCommon6).show();
					} else{
						var filePath = ReportUtils.buildReportFilePath(data.view);
						window.location.href = filePath;
						setTimeout(function(){ //Set timeout để đảm bảo file load lên hoàn tất
							CommonSearch.deleteFileExcelExport(data.view);
						},30000);
					}
				}
			},
			error:function(XMLHttpRequest, textStatus, errorDivThrown) {				
			}
		});
		return false;
	},
	agreeCustomer:function(status){
		$('#errMsg').hide();
		var params = new Object();
		var allVals = [];
		$('.ck').each(function() {
			if ($(this).prop('checked')) {
				allVals.push($(this).val());
			}
		});
		params.lstCustomerId = allVals;
		if (status != undefined && status != null) {
			params.status = status;
		}
		if (params.lstCustomerId.length == 0) {
			$('#errMsg').html(create_route_chua_chon_kh).show();
			return;
		}
		var msgConfirm = '';
		var APPROVE = 1, REJECT = 3, DELETE = -1;
		if (status == APPROVE) {
			msgConfirm = formatString(catalog_customer_confirm, catalog_customer_approve);
			CustomerCatalog.approveNewCustomer(allVals);
		} else if (status == REJECT) {
			msgConfirm = formatString(catalog_customer_confirm, jsp_common_status_reject);
			CustomerCatalog.denyNewCustomer(allVals);
		} else {
			msgConfirm = formatString(catalog_customer_confirm, jsp_common_xoa);
			CustomerCatalog.deleteNewCustomer(allVals);
		}

		return false;
	},
	/**
	 * Duyet khach hang moi
	 * @author tuannd20
	 * @param  {Array} customerIds danh sach id cac khach hang
	 * @return {void}
	 * @since  31/08/2015
	 */
	approveNewCustomer: function(customerIds) {
		CustomerCatalog
		.showConfirmDialog()
		.then(function() {
			return Promise.resolve(customerIds);
		}).each(function(customerId) {
			var param = {
				customerId: customerId
			};
			return CustomerCatalog.tryApproveNewCustomer(param)
				.then(function(result) {
					var isError = result && result.hasOwnProperty('error') && result.error;
					var errMsg = result && result.hasOwnProperty('errMsg') && result.errMsg;
					var hasDuplicateCustomers = result && result.hasOwnProperty('duplicatedCustomer') 
												&& result.duplicatedCustomer instanceof Array && result.duplicatedCustomer.length;
					if (hasDuplicateCustomers) {
						var rows = $('#customerDatagrid').datagrid('getRows');
//						var approvingCustomerInfo = rows.find(function(element, index, array) {
//													return element.id == customerId;
//												});
						var approvingCustomerInfo;
						$.each(rows, function(index, item) {
						    if (item.id == customerId) {
						    	approvingCustomerInfo = item;
						    }
						});
						return CustomerCatalog.confirmDuplicateCustomer(customerId, approvingCustomerInfo, result);
					} else if (isError) {
						return CustomerCatalog.showAlertDialog(errMsg);
					}
					return Promise.resolve();
				}).catch(function(e) {
					console.log(e);
					$('#confirm-duplicate-customer-dialog').dialog('close');
				});
		}).catch(function(e) {
			console.log(e);
		}).finally(function() {
			CustomerCatalog.search();
		});
	},
	/**
	 * duyet khach hang
	 * @author tuannd20
	 * @param  {object} param thong tin khach hang dang duyet
	 * @return {void}
	 * @since 27/08/2015
	 */
	tryApproveNewCustomer: function(param) {
		return new Promise(function(resolve, reject) {
			Utils.saveData(param, "/catalog_customer_mng/approve", CustomerCatalog._xhrSave, null, function(data) {
				resolve(data);
			}, null, ' #infoTabContainer ', null, function(data) {
				resolve(data);
			});
		});
	},
	/**
	 * xac nhan thao tac thuc hien voi khach hang trung
	 * @author tuannd20
	 * @param {long} customerId id khach hang dang duyet
	 * @param {object} approvingCustomerInfo thong tin khach hang dang duyet
	 * @param  {object} duplicateWithCustomers du lieu mo ta khach hang trung
	 * @return {promise}
	 * @since  31/08/2015
	 */
	confirmDuplicateCustomer: function(customerId, approvingCustomerInfo, duplicateWithCustomers) {
		var tmpData = {};
		return CustomerCatalog.showConfirmDuplicateCustomerDialog(approvingCustomerInfo, duplicateWithCustomers)
			.spread(function(actionType, customerCode) {
				tmpData.actionType = actionType;
				tmpData.customerCode = customerCode;
				return CustomerCatalog.showConfirmDialog();
			}).then(function() {
				var result = tmpData;
				if (!result) {
					return Promise.resolve({error: false});
				}
				var actionType = result.actionType;
				var customerCode = result.customerCode;
				var promise = null;
				switch (actionType) {
					case CustomerCatalog.BUTTON_TYPE_DELETE:
						promise = CustomerCatalog.deleteNewCustomerSync(customerId);
						break;
					case CustomerCatalog.BUTTON_TYPE_CREATE:
						promise = CustomerCatalog.createNewCustomer(customerId, null);
						break;
					case CustomerCatalog.BUTTON_TYPE_DENY:
						promise = CustomerCatalog.denyNewCustomerSync(customerId);
						break;
					case CustomerCatalog.BUTTON_TYPE_CREATE_WITH_DUPLICATE_CUSTOMER_CODE:
						promise = CustomerCatalog.createNewCustomer(customerId, customerCode);
						break;
					default:
						promise = Promise.resolve();
				}
				return promise;
			}).then(function(result) {
				if (result && !result.error) {
					$('#confirm-duplicate-customer-dialog').dialog('close');
				}
				if (result && result.error) {
					var errorMsg = result.errMsg || result.errorCustomer[customerId];
					$('#dlgErrMsg').html(errorMsg);
					CustomerCatalog.showElement('#dlgErrMsg', 5000);
					return CustomerCatalog.confirmDuplicateCustomer(customerId, approvingCustomerInfo, duplicateWithCustomers);
				}
			}).catch(function(actionType) {
				if (actionType && actionType === CustomerCatalog.BUTTON_TYPE_CONFIRM_LATER) {
					return Promise.reject(actionType);
				}
				return CustomerCatalog.confirmDuplicateCustomer(customerId, approvingCustomerInfo, duplicateWithCustomers);
			});
	},
	/**
	 * Xac nhan khach hang trung
	 * @author tuannd20
	 * @param {object} approvingCustomerInfo thong tin khach hang dang duyet
	 * @param  {duplicateInfo} du lieu hien thi tren grid khach hang trung
	 * @return {promise}
	 * @since 27/08/2015
	 */
	showConfirmDuplicateCustomerDialog: function(approvingCustomerInfo, duplicateInfo) {
		var duplicateWithCustomers = duplicateInfo.duplicatedCustomer;
		CustomerCatalog.duplicateWithCustomers = duplicateWithCustomers;
		var promise = new Promise(function(resolve, reject) {
			$('#confirm-duplicate-customer-dialog').dialog({
				title: "Xác nhận khách hàng trùng: " + Utils.XSSEncode(approvingCustomerInfo.customerName) + ", " + Utils.XSSEncode(approvingCustomerInfo.address),
				width: 950, 
				height: 'auto',
				closed: false,
				cache: false,
				modal: true,
				onOpen: function() {
					CustomerCatalog.initDuplicateCustomerGrid(approvingCustomerInfo, duplicateWithCustomers);

					$('#confirm-duplicate-customer-dialog button').bind('click', function() {
						var buttonType = $(this).data().buttonType;
						if (CustomerCatalog.BUTTON_TYPE_CONFIRM_LATER == buttonType) {
							reject(buttonType);
						} else {
							resolve([buttonType, CustomerCatalog.selectedDuplicateCustomerCode]);
						}
					});

					$('#duplicate-customer-grid-container td[field=choose] a').unbind('click')
					.bind('click', function() {
						var selectedCustomerCode = $(this).data().customerCode;
						resolve([CustomerCatalog.BUTTON_TYPE_CREATE_WITH_DUPLICATE_CUSTOMER_CODE, selectedCustomerCode]);
					});
					setTimeout(function() {
						CommonSearchEasyUI.fitEasyDialog("confirm-duplicate-customer-dialog");
					}, 200);
				},
				onClose: function() {
					if (promise.isPending()) {
						reject(CustomerCatalog.BUTTON_TYPE_CONFIRM_LATER);
					}
				}
			});
		});
		return promise;
	},
	/**
	 * Khoi tao grid thong tin khach hang trung tren dialog
	 * @author tuannd20
	 * @param {object} approvingCustomerInfo thong tin khach hang dang duyet
	 * @param  {object} gridData du lieu hien thi tren grid
	 * @return {void}
	 * @since  31/08/2015
	 */
	initDuplicateCustomerGrid: function(approvingCustomerInfo, gridData) {
		$('#duplicate-customer-grid').datagrid({
			data: gridData,
			autoRowHeight: true,
			rownumbers: true,
			checkOnSelect: true,
			rowNum: 10,
			scrollbarSize: 0,
			singleSelect: true,
			pageNumber: 1,
			queryParams: {
				page: 1
			},
			fitColumns: true,
			width: ($('#confirm-duplicate-customer-dialog').width() - 40),
			columns: [[
			    {field: 'regionShopName', title: 'Miền', align: 'left', width: 110, sortable: false, resizable: false, 
			    	formatter: function(value, row, index) {
			    		return Utils.XSSEncode(value);
			    	}
				},
			    {field: 'areaShopName', title: 'Vùng', align: 'left', width: 110, sortable: false, resizable: false, 
			    	formatter: function(value, row, index) {
			    		return Utils.XSSEncode(value);
			    	}
			    },
			    {field: 'shopName', title: 'NPP', align: 'left', width: 110, sortable: false, resizable: false, 
			    	formatter: function(value, row, index) {
			    		return Utils.XSSEncode(value);
			    	}
				},
			    {field: 'shortCode', title: 'Mã KH', align: 'left', width: 110, sortable: false, resizable: false, 
			    	formatter: function(value, row, index) {
			    		return Utils.XSSEncode(value);
			    	}
				},
			    {field: 'customerName', title: 'Tên KH', align: 'left', width: 110, sortable: false, resizable: false, 
			    	formatter: function(value, row, index) {
			    		return Utils.XSSEncode(value);
			    	}
				},
			    {field: 'address', title: 'Địa chỉ', align: 'left', width: 110, sortable: false, resizable: false, 
			    	formatter: function(value, row, index) {
			    		if (row.no == CustomerCatalog.DUPLICATE_ADDRESS) {
			    			return '<span style="color: rgb(255, 0, 0);">' + Utils.XSSEncode(value) + '</span>';
			    		}
			    		return Utils.XSSEncode(value);
			    	}
				},
				{field: 'status', title: 'Trạng thái', align: 'left', width: 110, sortable: false, resizable: false, 
			    	formatter: function(value, row, index) {
			    		switch (row.status) {
				    	    case CustomerCatalog.STATUS_RUNNING:
				    	        str = Utils.language_vi('catalog.customer.active');
				    	        break;
				    	    case CustomerCatalog.STATUS_WAITING:
				    	        str = Utils.language_vi('catalog.customer.draf');
				    	        break;
			    	        case CustomerCatalog.STATUS_STOPPED:
			    	            str = Utils.language_vi('catalog.customer.pausing');
			    	            break;
				    	    case CustomerCatalog.STATUS_REJECTED:
				    	        str = Utils.language_vi('catalog.customer.reject');
				    	}
				    	return str;
			    	}
				},
			    {field: 'distanceFromApprovingCustomer', title: 'Khoảng cách (m)', align: 'left', width: 110, sortable: false, resizable: false, 
			    	formatter: function(value, row, index) {
			    		if (row.no != CustomerCatalog.DUPLICATE_ADDRESS) {
			    			return '<span style="color: rgb(255, 0, 0);">' + Math.round(value) + '</span>';
			    		}
			    		return Math.round(value);
			    	}
				},
			    {field: 'choose', title: '', align: 'center', width: 50, sortable: false, resizable: false, formatter: function(value, row, index) {
			    	if (approvingCustomerInfo.shop.shopCode !== row.shopCode) {
			    		return '<a href="javascript:void(0)" data-customer-code="' + Utils.XSSEncode(row.shortCode) + '"">Chọn</a>';
			    	}
			    	return "";			        
			    }}
			]],
			onLoadSuccess: function() {
				
			}
		});
	},
	/**
	 * Hien thi dialog confirm thao tac
	 * @author tuannd20
	 * @param  {string} dialogMessage message hien thi tren dialog
	 * @return {promise}
	 * @since  31/08/2015
	 */
	showConfirmDialog: function(dialogMessage) {
		dialogMessage = dialogMessage || 'Bạn có muốn thực hiện thao tác không?';
		return new Promise(function(resolve, reject) {
			$.messager.confirm(jsp_common_xacnhan, dialogMessage, function(r) {
				if (r) {
					resolve();
				} else {
					reject();
				}
			});
		});
	},
	/**
	 * Hien thi dialog thong bao
	 * @author tuannd20
	 * @param  {string} dialogMessage message hien thi tren dialog
	 * @return {promise}
	 * @since  21/09/2015
	 */
	showAlertDialog: function(dialogMessage) {
		return new Promise(function(resolve, reject) {
			$.messager.alert('Thông báo', dialogMessage, 'warning', function() {
			    resolve();
			});
		});
	},
	/**
	 * Tao khach hang moi voi ma khach hang chi dinh
	 * @author tuannd20
	 * @param  {long} customerId id cua khach hang dang tao
	 * @param  {string} customerCode ma khach hang da chon. NULL neu nhu dang tao moi khach hang
	 * @return {promise}
	 * @since  31/08/2015
	 */
	createNewCustomer: function(customerId, customerCode) {
		return new Promise(function(resolve, reject) {
			var param = {
				customerId: customerId
			};
			if (customerCode) {
				param.customerCode = customerCode;
			}
			Utils.saveData(param, "/catalog_customer_mng/approve/create", CustomerCatalog._xhrSave, null, function(data) {
				resolve(data);
			}, null, ' #infoTabContainer ', null, function(data) {
				resolve(data);
			});
		});
	},
	/**
	 * Huy khach hang do nhan vien tao moi
	 * @author tuannd20
	 * @param  {long} customerId id khach hang can huy
	 * @return {promise}
	 * @since  31/08/2015
	 */
	deleteNewCustomerSync: function(customerId) {
		return CustomerCatalog.rejectNewCustomerSync("/catalog_customer_mng/approve/delete", customerId);
	},
	/**
	 * Tu choi khach hang do nhan vien tao moi
	 * @author tuannd20
	 * @param  {long} customerId id khach hang dang tu choi
	 * @return {promise}
	 * @since  31/08/2015
	 */
	denyNewCustomerSync: function(customerId) {
		return CustomerCatalog.rejectNewCustomerSync("/catalog_customer_mng/approve/deny", customerId);
	},
	/**
	 * Loai bo khach hang
	 * @author tuannd20
	 * @param  {string} url url api
	 * @param  {long} customerId id khach hang
	 * @return {promise}
	 * @since  31/08/2015
	 */
	rejectNewCustomerSync: function(url, customerId) {
		return new Promise(function(resolve, reject) {
			var param = {
				lstCustomerId: [customerId]
			};
			Utils.saveData(param, url, CustomerCatalog._xhrSave, null, function(data) {
				var failCustomerCount = data.hasOwnProperty('errorCustomer') ? data.errorCustomer.length : 0;
				data.error = failCustomerCount != 0
				resolve(data);
			}, null, ' #infoTabContainer ', null, function(data) {
				resolve(data);
			});
		});
	},
	/**
	 * Tu choi khach hang
	 * @author tuannd20
	 * @param  {Array} customerIds danh sach id cua cac khach hang tu choi
	 * @return {void}
	 * @since  31/08/2015
	 */
	denyNewCustomer: function(customerIds) {
		return CustomerCatalog.rejectNewCustomer("/catalog_customer_mng/approve/deny", customerIds);
	},
	/**
	 * Xoa khach hang
	 * @author tuannd20
	 * @param  {Array} customerIds danh sach id cua cac khach hang xoa
	 * @return {void}
	 * @since  31/08/2015
	 */
	deleteNewCustomer: function(customerIds) {
		return CustomerCatalog.rejectNewCustomer("/catalog_customer_mng/approve/delete", customerIds);
	},
	/**
	 * loai bo khach hang moi
	 * @param  {string} url        api's url
	 * @param  {Array} customerIds danh sach id khach hang
	 * @return {void}
	 * @since  31/08/2015
	 */
	rejectNewCustomer: function(url, customerIds) {
		var param = {
			lstCustomerId: customerIds
		};
		Utils.addOrSaveData(param, url, CustomerCatalog._xhrSave, null, function(data) {
			if (data && data.hasOwnProperty('errorCustomer') && !$.isEmptyObject(data.errorCustomer)) {
				var totalCustomer = customerIds.length;
				var failCustomerCount = 0;
				var errMsg = '';
				for (var p in data.errorCustomer) {
					errMsg += Utils.XSSEncode(data.errorCustomer[p]) + '<br>';
					failCustomerCount++;
				}
				$('#successMsg').html('Xử lý thành công ' + (totalCustomer - failCustomerCount) + '/' + totalCustomer);

				if (failCustomerCount != 0) {
					$('#errMsg').html(errMsg);
				}

				CustomerCatalog.showElement('#successMsg', 3000);
				CustomerCatalog.showElement('#errMsg', 3000);
			}
			CustomerCatalog.search();
		}, null, ' #infoTabContainer ', null, null);
	},
	/**
	 * show element
	 * @author tuannd20
	 * @param  {string} elementSelector element's css selector
	 * @param  {integer} timeout        timeout to hide element
	 * @return {void}
	 * @since  31/08/2015
	 */
	showElement: function(elementSelector, timeout) {
		$(elementSelector).show();
		setTimeout(function() {
			$(elementSelector).hide();
		}, timeout);
	},
	fillTxtShopCodeByF9 : function(code, isF9){
		if (code != undefined && code != null && code != '') {
			$('#txtShopCode').val(code).show();
		} else {
			code = $('#txtShopCode').val();
		}
		if (code == '') {
			return;
		}
		if (isF9 == undefined || isF9 == null || !isF9) {
			$('#common-dialog-search-2-textbox').dialog('close');
		}
//		$('#txtShopCode').focus();
		//load lại danh sách NVGH, NVTT
		var params = new Object();
		params.shopCode = code;
		Utils.getJSONDataByAjax(params,'/catalog_customer_mng/get-list-staff-nvgh-nvtt',function(data){
			Utils.bindStaffCbx('#deliveryCode', data.listDeliverStaff);
			Utils.bindStaffCbx('#cashierCode', data.listCashierStaff);
			loadDataComboboxRouting(data.listRouting);
		},null,'POST');
	},
	viewBigMapOnDlgDynamic:function(attr) {
		$('#imgBtnDeleteLatLng,#imgBtnUpdateLatLng,#imgBtnCancelLatLng').show();
		ProductCatalog.viewBigMapOnDlg(attr, function(){
			CustomerCatalog.showMapContent();
		});
	}
};
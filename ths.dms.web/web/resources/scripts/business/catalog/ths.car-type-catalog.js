var CarTypeCatalog = {
	_xhrSave : null,
	_xhrDel:null,
	getGridUrl: function(shopCode,type,number,label,gross,category,origin,status,isAll){
		isAll = null;
		shopCode = $('#shopCode').val().trim();
		var param = '';
		if(isAll != null || isAll != undefined ){
			param = "&isAll=" + encodeChar(isAll);
		}
		return "/catalog/car-type/search?shopCode=" + encodeChar(shopCode) + "&type=" + encodeChar(type) + "&number=" + encodeChar(number) + "&label=" + encodeChar(label) + "&gross=" + encodeChar(gross) + "&category=" + encodeChar(category) + "&origin=" + encodeChar(origin) + "&status=" + status + param;
	},	
	search: function(){
		$('#errMsg').html('').hide();
		$('#errExcelMsg').html('').hide();
		var type = $('#type').val().trim();
		var number = $('#number').val().trim();
		var label = $('#label').val().trim();
		var gross = $('#gross').val().trim();
		var category = $('#category').val().trim();
		var origin = $('#origin').val().trim();
		var status = $('#status').val().trim();
		var params =  new Array();
		params.type = type;
		params.number =  number;
		params.label = label;
		params.gross = gross;
		params.category = category;
		params.origin = origin;
		params.status = status;
		$('#grid').datagrid('load',params);
		return false;
	},
	change: function(){
		var msg = '';
		$('#errMsg').html('').hide();
		$('#errExcelMsg').html('').hide();
//		msg = Utils.getMessageOfRequireCheck('shopCode','Mã đơn vị');
//		if(msg.length == 0){
//			msg = Utils.getMessageOfSpecialCharactersValidate('shopCode','Mã đơn vị',Utils._CODE);
//		}
		if(msg.length == 0){
			msg = Utils.getMessageOfRequireCheck('number','Số xe');
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfSpecialCharactersValidateEx('number','Số xe',Utils._TF_CAR_NUMBER);
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfRequireCheck('status','Trạng thái',true);
		}
		if(msg.length > 0){
			$('#errMsg').html(msg).show();
			return false;
		}
		var dataModel = new Object();
		dataModel.id = $('#carTypeId').val().trim();
		dataModel.type = $('#type').val().trim();
		dataModel.number = $('#number').val().trim();
		dataModel.label = $('#label').val().trim();
		var gross = parseInt($('#gross').val().trim());
		if(gross <= 0){
			msg = 'Trọng tải(kg) số nguyên lớn hơn 0';
			if(msg.length > 0){
				$('#errMsg').html(msg).show();
				return false;
			}
		}else{
			dataModel.gross = $('#gross').val().trim();
		}
		dataModel.category = $('#category').val().trim();
		dataModel.origin = $('#origin').val().trim();
		dataModel.status = $('#status').val().trim();
		Utils.addOrSaveRowOnGrid(dataModel, "/catalog/car-type/save", CarTypeCatalog._xhrSave, null, null,function(){
			CarTypeCatalog.resetForm();
		});		
		return false;
	},
	getSelectedCarType: function(id,number,category,gross,label,origin,shopCode,status,type){
		Utils.getChangedForm();
		setTitleUpdate();
//		$('#shopCode').attr('disabled','disabled');
		if(id!= null && id!= 0 && id!=undefined){
			$('#carTypeId').val(id);
		} else {
			$('#carTypeId').val(0);
		}
		setTextboxValue('shopCode',shopCode);
		setTextboxValue('number',number);
		$('#number').attr('disabled','disabled');
		setTextboxValue('gross',gross);
		setSelectBoxValue('type',type);
		setSelectBoxValue('label',label);
		setSelectBoxValue('category',category);
		setSelectBoxValue('origin',origin);
		setSelectBoxValue('status', status);
		$('#btnSearch,#btnCreate').hide();
		$('#btnUpdate,#btnDismiss').show();
	},
	resetForm: function(){
		$('span#errIU').html('');
		$('#titleCarId').html('Thông tin tìm kiếm');
		$('.RequireStyle').hide();
		setTitleSearch();
		$('#errMsg').html('').hide();
		$('#errExcelMsg').html('').hide();
		$('#number').removeAttr('disabled');
		$('#btnUpdate,#btnDismiss').hide();
		$('#btnCreate,#btnSearch').show();
		$('#shopCode').val('');
		$('#shopCode').focus();
		$('#number').val('');
		$('#gross').val('');
		$('#type').val(223);
		$('#type').change();
		$('#label').val(-1);
		$('#label').change();
		$('#category').val(-1);
		$('#category').change();
		$('#origin').val(-1);
		$('#origin').change();
		$('#status').val(1);
		$('#status').change();
		$('#carTypeId').val(0);
		Utils.comboboxChange('label','Chọn tất cả');
		Utils.comboboxChange('type','Chọn tất cả');
		Utils.comboboxChange('category','Chọn tất cả');
		Utils.comboboxChange('origin','Chọn tất cả');
		$("#grid").trigger("reloadGrid");
		return false;
	},
	getListParentCode:function(parentId,objParentCode){
		$.ajax({
			type : "POST",
			url : "/catalog/car-type/getlistparentcode",
			data :({id:parentId}),
			dataType : "json",
			success : function(data) {
				var html = new Array();
    			if(data!= null && data!= undefined && data.lstParentCode!= null && data.lstParentCode.length > 0){
    				html.push("<option value='-1'>---Chọn mã cha---</option>");
    				for(var i=0;i<data.lstParentCode.length;i++){
    					html.push("<option value='"+ data.lstParentCode[i].id +"'>"+ Utils.XSSEncode(data.lstParentCode[i].channelTypeCode) +"</option>");
    				}
    			}else{
    				html.push("<option value='-1'>---Chọn mã cha---</option>");
    			}	        			        	
    			$(objParentCode).html(html.join(""));
    			$(objParentCode).val(parentId);
    			$(objParentCode).change();
			},
			error:function(XMLHttpRequest, textStatus, errorThrown) {
				$.ajax({
					type : "POST",
					url : '/check-session',
					dataType : "json",
					success : function(data) {
					},
					error: function(XMLHttpRequest, textStatus, errorThrown) {
						window.location.href = '/home';
					}
				});
			}
		});
	},
	deleteCarType: function(carId){
		$('#errMsg').html('').hide();
		$('#errExcelMsg').html('').hide();
		var dataModel = new Object();
		dataModel.id = carId;	
		Utils.deleteSelectRowOnGrid(dataModel, 'loại xe', "/catalog/car-type/remove", CarTypeCatalog._xhrDel, null, null,function(data){
			if (!data.error){
//				CarTypeCatalog.loadTreeEx();
				CarTypeCatalog.resetForm();
			}
		},null);
		return false;		
	},
	loadTree: function(){
		var shopCode = $('#shopCodeHidden').val().trim();
		loadDataForTree('/rest/catalog/unit-tree/'+encodeChar(shopCode)+'/tree.json');	
		$('#tree').bind("loaded.jstree", function(event, data){
			if($('#treeContainer').data("jsp")!= undefined){
				$('#treeContainer').data("jsp").destroy();
			}
			$('#treeContainer').jScrollPane();
		});
		$('#tree').bind("select_node.jstree", function (event, data) {
			var isAll = false;
			if($('#isAll').is(':checked')){
				isAll = true;
			}
			var name = data.inst.get_text(data.rslt.obj);
			var id = data.rslt.obj.attr("id");
			var code = data.rslt.obj.attr("contentItemId");
			var url = CarTypeCatalog.getGridUrl(code,'','',-1,'',-1,-1,1,isAll);
			$("#grid").setGridParam({url:url,page:1}).trigger("reloadGrid");
	    });
	},
	loadTreeEx:function(){
		CarTypeCatalog.loadShopTree();
	},
	loadShopTree:function(){
		$('#tree').jstree({
	        "plugins": ["themes", "json_data","ui"],
	        "themes": {
	            "theme": "classic",
	            "icons": false,
	            "dots": true
	        },
	        "json_data": {
	            "ajax": {
	            	"url": function( node ){
	                      if( node == -1 ){
	                    	  return "/rest/catalog/unit-tree-ex/"+$('#shopIdHidden').val().trim()+"/0/tree.json";
	                      } else {
	                    	  return "/rest/catalog/unit-tree-ex/"+node.data("attr").id+"/1/tree.json";
	                      }
	                    },
	                "type": 'GET',
	                "success": function(ops) {
	                    data = [];
	                    for( opnum in ops ){
	                      var op = ops[opnum];
	                      node = {
	                          "data" : op.data,
	                          "attr" : op.attr,
	                          "metadata" :  op ,
	                          "state"	: op.state
	                          /*"state" : "closed"*/
	                      };
	                      data.push( node );
	                    }
	                    return data;
	                },
	                'complete':function(){	                	
	                	$('#tree').bind("select_node.jstree", function (event, data) {
	                		var isAll = false;
	            			if($('#isAll').is(':checked')){
	            				isAll = true;
	            			}
	            			var name = data.inst.get_text(data.rslt.obj);
	            			var id = data.rslt.obj.attr("id");
	            			var code = data.rslt.obj.attr("contentItemId");
	            			var url = CarTypeCatalog.getGridUrl(code,'','',-1,'',-1,-1,1,isAll);
	            			$("#grid").setGridParam({url:url,page:1}).trigger("reloadGrid");            			
	            	    });
	                	$('#tree').bind("loaded.jstree", function(event, data){
	                		var tm = setTimeout(function() {
		                		if($('#treeContainer').data("jsp")!= undefined){
		            				$('#treeContainer').data("jsp").destroy();
		            			}
		            			$('#treeContainer').jScrollPane();
		                    }, 500);
	                    });
	                	var tm = setTimeout(function() {
	                		if($('#treeContainer').data("jsp")!= undefined){
	            				$('#treeContainer').data("jsp").destroy();
	            			}
	            			$('#treeContainer').jScrollPane();
	                    }, 500);
	                }
	            }
	        },
	        "core" : {"load_open" : true }
		});	
	},
	exportExcelData:function(){
		showLoadingIcon();
		$('#errMsg').html('').hide();
		$('#errExcelMsg').html('').hide();
		var shopCode = $('#shopCode').val().trim();
		var type = $('#type').val().trim();
		var number = $('#number').val().trim();
		var label = $('#label').val().trim();
		var gross = $('#gross').val().trim();
		var category = $('#category').val().trim();
		var origin = $('#origin').val().trim();
		var status = $('#status').val().trim();
		var data = new Object();
		data.shopCode = shopCode;
		data.type = type;
		data.number = number;
		data.label = label;
		data.gross = gross;
		data.category = category;
		data.origin = origin;
		data.status = status;
		var url = "/catalog/car-type/getlistexceldata";
		CommonSearch.exportExcelData(data,url);
	},
	importExcel:function(){
		$('#isView').val(0);
		$('#importFrm').submit();
		return false;
	},	
	viewExcel:function(){
		$('#isView').val(1);
		$('#importFrm').submit();
		return false;
	}
};
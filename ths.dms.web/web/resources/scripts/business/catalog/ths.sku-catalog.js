var SkuCatalog = {
	_xhrSave : null,
	_xhrDel: null,
	getGridUrl: function(channelTypeCode,channelTypeName,status,skuAmount){		
		return "/catalog/sku-customer-type/search?code=" + encodeChar(channelTypeCode) + "&name=" + encodeChar(channelTypeName) + "&status=" + encodeChar(status) + '&skuAmount=' + encodeChar(skuAmount);
	},	
	search: function(){		
		$('#errMsg').html('').hide();
		$('#errExcelMsg').html('').hide();
		$('#name').focus();
		var channelTypeCode = $('#code').val().trim();
		var channelTypeName =  $('#name').val().trim();		
		var status = $('#status').val().trim();		
		var skuAmount = $('#skuAmount').val().trim().replace(/,/g,'');
		var url = SkuCatalog.getGridUrl(channelTypeCode,channelTypeName,status,skuAmount);
		$("#grid").setGridParam({url:url,page:1}).trigger("reloadGrid");
		return false;
	},
	save: function(){
		var msg = '';
		$('#errMsg').html('').hide();
		$('#errExcelMsg').html('').hide();
		msg = Utils.getMessageOfRequireCheck('code','Mã loại KH',true);
		if(msg.length == 0){
			msg = Utils.getMessageOfRequireCheck('skuAmount','Chỉ tiêu SKU');
		}
		var skuAmount = $('#skuAmount').val().trim().replace(/,/g,'');
		if(msg.length == 0){
			if(isNaN(skuAmount) || parseInt(skuAmount) <=0){
				msg = 'Chỉ tiêu SKU phải là số nguyên dương';
			}
		}
		if(msg.length == 0){
			if(parseInt(skuAmount) > 9999999999){
				msg= 'Chỉ tiêu SKU không vượt quá 9,999,999,999';
			}
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfRequireCheck('status','Trạng thái',true);
		}		
		if(msg.length > 0){
			$('#errMsg').html(msg).show();
			return false;
		}
		var dataModel = new Object();
		dataModel.id = $('#selId').val().trim();
		dataModel.code = $('#code').val().trim();		
		dataModel.skuAmount = $('#skuAmount').val().trim().replace(/,/g,'');
		dataModel.status = $('#status').val();
		Utils.addOrSaveRowOnGrid(dataModel, "/catalog/sku-customer-type/save", SkuCatalog._xhrSave, null, null, function(data){
			if(!data.error){
				SkuCatalog.resetForm();
				SkuCatalog.search();
			}			
		});			
		return false;
	},
	getSelectedRow: function(rowId){
		disabled('name');
		$('#skuAmount').focus();
		var code = $("#grid").jqGrid ('getCell', rowId, 'channelTypeCode');
		var name = $("#grid").jqGrid ('getCell', rowId, 'channelTypeName');
		var status = $("#grid").jqGrid ('getCell', rowId, 'statusSku');
		var skuAmount = $("#grid").jqGrid ('getCell', rowId, 'sku');
		var id = $("#grid").jqGrid ('getCell', rowId, 'id');
		
		if(id!= null && id!= 0 && id!=undefined){
			$('#selId').val(id);
		} else {
			$('#selId').val(0);
		}
		setSelectBoxValue('code',code);
		setTextboxValue('name',name);
		setTextboxValue('skuAmount',skuAmount);
		if(status == activeStatusText){
			status = 1;
		} else {
			status = 0;
		}
		setSelectBoxValue('status', status);
		SkuCatalog.getChangedForm();
		disabled('code');
		$('#code').change();
		setTitleUpdate();
		$('#errExcelMsg').html('').hide();
		return false;
	},
	resetForm: function(){
		setTitleSearch();
		$('.RequireStyle').hide();
		$('#selId').val(0);
		$('#errMsg').html('').hide();
		$('#name').removeAttr('disabled');
		$('#btnEdit').hide();
		$('#btnCancel').hide();
		$('#btnSearch').show();
		$('#btnAdd').show();		
		$('#name').val('');
		$('#skuAmount').val('');
		setSelectBoxValue('code', -2);
		setSelectBoxValue('status', 1);
		enable('code');
		$('#name').focus();
		$('#code').change();
		//$("#grid").trigger("reloadGrid");
		SkuCatalog.search();
		$('#errExcelMsg').html('').hide();
		return false;
	},
	deleteSKU: function(id){
		var dataModel = new Object();
		dataModel.id = id;	
		Utils.deleteSelectRowOnGrid(dataModel, 'chỉ tiêu SKU', '/catalog/sku-customer-type/delete', SkuCatalog._xhrDel, null, null,function(data){
			if (!data.error){
				SkuCatalog.resetForm();
			}
		},null);
		$('#errExcelMsg').html('').hide();
		return false;
	},
	getChangedForm: function(){
		$('#skuAmount').focus();
		$('.RequireStyle').show();
		$('#btnSearch').hide();
		$('#btnAdd').hide();
		$('#btnEdit').show();
		$('#btnCancel').show();	
		disabled('name');
		setTitleAdd();
		$('#code').change();
	},
	ViewFileUpload: function(){
		$('#typeButton').val("1");//bang 1 la xem file upload
		var options = { 
		 		beforeSubmit: ProductLevelCatalog.beforeImportExcelA,   
		 		success:      ProductLevelCatalog.afterImportExcelA,  
		 		type: "POST",
		 		dataType: 'html'   
		 	}; 
		$('#importFrm').ajaxForm(options);
		$('#importFrm').submit();
	},
	exportExcel: function(){
		var channelTypeCode = $('#code').val().trim();
		var channelTypeName =  $('#name').val().trim();		
		var status = $('#status').val().trim();		
		var skuAmount = $('#skuAmount').val().trim().replace(/,/g,'');
		var url = "/catalog/sku-customer-type/export-excel?channelTypeCode=" + encodeChar(channelTypeCode) + "&channelTypeName=" + encodeChar(channelTypeName) +"&status=" + status + "&skuAmount=" + encodeChar(skuAmount);
		CommonSearch.exportExcelData(url,'Bieu_mau_export_SKU_CUSTOMER_TYPE.xls');
	}
};
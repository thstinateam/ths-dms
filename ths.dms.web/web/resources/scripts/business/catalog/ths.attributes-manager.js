var AttributesManager = {
	PRODUCT : 0,
	CUSTOMER: 1,
	STAFF: 2,
	PROMOTION_PROGRAM: 3,
	FOCUS_PROGRAM: 4,	
	DISPLAY_PROGRAM: 5,
	DISPLAY_GROUP: 6,
	DISPLAY_PROGRAM_LEVEL:7,
	INCENTIVE_PROGRAM: 8,
	INCENTIVE_PROGRAM_LEVEL: 9,
	DP_PAY_PERIOD_RESULT: 10,
	_xhrSave: null,
	_xhrDelete: null,
	getAttributesForm: function(divId,type){	
		$('#tabAttribute').val(divId);
		var dataModel = new Object();
		dataModel.type = type;
		var kData = $.param(dataModel, true);
		$.ajax({
			type : 'GET',
			url : '/attributes/list',
			data :(kData),
			dataType : "html",
			success : function(data) {
				$('#' + divId).html(data);
				$('#' + divId +' .MySelectBoxClass').customStyle();
				Utils.bindAutoSearch();
			},
			error:function(XMLHttpRequest, textStatus, errorThrown) {
			}});
	},
	getGridUrl: function(code,name,columnType,status, type){
		return "/attributes/search?code=" + encodeChar(code) + "&name=" + encodeChar(name) +"&columnType=" + columnType + "&status=" + status + '&type=' + type;
	},
	getListAttribute: function(){
		var divId = $('#tabAttribute').val();
		var type = $('#typeAttribute').val();
		var url = AttributesManager.getGridUrl('', '', activeType.ALL, activeType.RUNNING, type);
		$("#" + divId + " #attributeGrid").jqGrid({
			  url:url,
			  colModel:[		
			    {name:'attributeCode',index:'attributeCode', label: 'Mã thuộc tính', width: 100, sortable:false,resizable:false , align: 'left'},
			    {name:'attributeName', index:'attributeName',label: 'Tên thuộc tính', sortable:false,resizable:false, align:'left' },
			    {name:'note',index:'note', label: 'Mô tả', sortable:false,resizable:false, align:'left' },
			    {name:'columnName',index:'columnName', label: 'Tên trường', sortable:false,resizable:false, align:'left' },
			    {name:'columnType',index:'columnType', label: 'Loại dữ liệu', sortable:false,resizable:false, align:'left', formatter: AttributeManagerFormatter.columnTypeCellIconFormatter},
			    {name:'columnValueType',index:'columnValueType', label: 'Kiểu dữ liệu', sortable:false,resizable:false, align:'left',formatter: AttributeManagerFormatter.columnValueTypeCellIconFormatter },
			    {name:'status',index:'status', label: 'Trạng thái', sortable:false,resizable:false, align:'left', formatter: GridFormatterUtils.statusCellIconFormatter },
			    {name:'edit', label: 'Sửa', width: 50, align: 'center',sortable:false,resizable:false, formatter: AttributeManagerFormatter.editCellIconFormatter},
			    {name:'listValue', label: 'DS giá trị', width: 50, align: 'center',sortable:false,resizable:false, formatter: AttributeManagerFormatter.listValueCellIconFormatter},
			    {name:'id', index:'id',hidden: true},			    
			  ],	  
			  pager : '#attributePager',
			  width: ($("#" + divId + " #attributeContainer").width())	  
			})
			.navGrid('#attributePager', {edit:false,add:false,del:false, search: false});
		$('#jqgh_attributeGrid_rn').prepend('STT');
	},
	searchAttribute: function(){
		$('#errExcelMsg').html('').hide();
		var divId = $('#tabAttribute').val();
		var type = $('#typeAttribute').val();
		
		var code = $('#' + divId + ' #searchForm #attributeCode').val().trim();
		var name = $('#' + divId + ' #searchForm #attributeName').val().trim();				
		var columnType = $('#' + divId + ' #searchForm #columnType').val().trim();
		var status = $('#' + divId + ' #searchForm #attributeStatus').val().trim();
		var url = AttributesManager.getGridUrl(code, name, columnType, status, type);
		$('#' + divId + ' #attributeGrid').setGridParam({url:url,page:1}).trigger("reloadGrid");
		var dataModel = new Object();
		dataModel.code = code;
		dataModel.name = name;
		dataModel.columnType = columnType;
		dataModel.status = status;
		dataModel.type = type;
		dataModel.listAll = 1;
		var kData = $.param(dataModel, true);
		$.ajax({
			type : 'GET',
			url : '/attributes/search',
			data :(kData),
			dataType : "json",
			success : function(data) {
				if(data!= null && data!= undefined){
					var target = $('#' + divId + " #listAttr"); 
					if (target.hasTemplate() == 0) {
						target.setTemplateURL("/resources/html/catalog/attributeItem.html"+ "?v=" + svn_revision_number);						
					}
					if (target.hasTemplate() != 0) {							
						target.processTemplate(data);
						$('#' + divId + ' #srlLstAttr').jScrollPane();
					}
				}
				
			},
			error:function(XMLHttpRequest, textStatus, errorThrown) {
				
			}});
		return false;
	},
	saveOrUpdate: function(){
		$('#errExcelMsg').html('').hide();
		var divId = $('#tabAttribute').val();
		var type = $('#typeAttribute').val();
		var msg = '';
		$('#errMsgAttr').html('').hide();
		msg = Utils.getMessageOfRequireCheck(divId + ' #createForm #attributeCode','Mã thuộc tính');
		if(msg.trim().length == 0){
			msg = Utils.getMessageOfSpecialCharactersValidate(divId + ' #createForm #attributeCode','Mã thuộc tính',Utils._CODE);
		}
		if(msg.trim().length == 0){
			msg = Utils.getMessageOfRequireCheck(divId + ' #createForm #attributeName','Tên thuộc tính');
		}
		if(msg.trim().length == 0){
			msg = Utils.getMessageOfSpecialCharactersValidate(divId + ' #createForm #attributeName','Tên thuộc tính');
		}
		if(msg.trim().length == 0){
			msg = Utils.getMessageOfRequireCheck(divId + ' #createForm #columnType','Loại dữ liệu',true);
		}
		if(msg.trim().length == 0){
			msg = Utils.getMessageOfRequireCheck(divId + ' #columnName','Tên trường',true);
		}
		if(msg.trim().length == 0){
			msg = Utils.getMessageOfRequireCheck(divId + ' #columnValueType','Kiểu dữ liệu',true);
		}
		if(msg.trim().length == 0){
			msg = Utils.getMessageOfRequireCheck(divId + ' #createForm #attributeStatus','Trạng thái',true);
		}
		if(msg.trim().length == 0){
			msg = Utils.getMessageOfSpecialCharactersValidate(divId + ' #note','Mô tả');
		}
		if(msg.length > 0){
			$('#errMsgAttr').html(msg).show();
			return false;
		}
		var dataModel = new Object();
		dataModel.id = $('#' + divId + ' #selAttrId').val().trim();
		dataModel.code = $('#' + divId + ' #createForm #attributeCode').val().trim();
		dataModel.name = $('#' + divId + ' #createForm #attributeName').val().trim();
		dataModel.note = $('#' + divId + ' #note').val().trim();
		dataModel.status = $('#' + divId + ' #createForm #attributeStatus').val();
		dataModel.columnType = $('#' + divId + ' #createForm #columnType').val();
		dataModel.columnName = $('#' + divId + ' #columnName').val();
		dataModel.columnValueType = $('#' + divId + ' #columnValueType').val();		
		dataModel.type = type;
		Utils.addOrSaveRowOnGrid(dataModel, "/attributes/save", AttributesManager._xhrSave, null, 'errMsgAttr', function(data){
			if(!data.error){
				AttributesManager.clearData();
			}
		});		
		return false;		
	},
	clearData: function(){
		var divId = $('#tabAttribute').val();		
		setTitleSearch();		
		$('#' + divId + ' #createForm').hide();
		$('#' + divId + ' #searchForm').show();
		$('#' + divId + ' #searchForm #attributeCode').focus();
		$('#' + divId + ' #selAttrId').val(0);
		$('#' + divId + ' #attributeCode').removeAttr('disabled');
		$('#' + divId + ' #columnName').removeAttr('disabled');
		$('#' + divId + ' #columnType').removeAttr('disabled');
		setSelectBoxValue(divId + ' #searchForm #attributeStatus',activeType.RUNNING);
		$('.disable').removeClass('BoxDisSelect');
		AttributesManager.searchAttribute();
		Utils.bindAutoSearch();
	},
	showSearchForm: function(){
		$('#errExcelMsg').html('').hide();
		$('#errMsgAttr').html('').hide();
		var divId = $('#tabAttribute').val();		
		$('#' + divId + ' #createForm').hide();
		$('#' + divId + ' #searchForm').show();
		$('#' + divId + ' #attributeCode').removeAttr('disabled');
		$('#' + divId + ' #columnName').removeAttr('disabled');
		$('#' + divId + ' #columnType').removeAttr('disabled');
		setSelectBoxValue(divId + ' #searchForm #attributeStatus',activeType.RUNNING);
		$('.disable').removeClass('BoxDisSelect');
		setTitleSearch('titleAttr');
		AttributesManager.searchAttribute();
	},
	showNewOrUpdateForm: function(){
		$('#errExcelMsg').html('').hide();
		$('#errMsgAttr').html('').hide();
		var divId = $('#tabAttribute').val();		
		$('#' + divId + ' #searchForm').hide();
		$('#' + divId + ' #createForm').show();
		$('#' + divId + ' #createForm #attributeCode').val('');
		$('#' + divId + ' #createForm #attributeName').val('');
		setSelectBoxValue(divId + ' #createForm #columnType',activeType.ALL);
		setSelectBoxValue(divId + ' #createForm #attributeStatus',activeType.RUNNING);
		setSelectBoxValue(divId + ' #columnName',activeType.ALL);
		setSelectBoxValue(divId + ' #columnValueType',activeType.ALL);
		$('#' + divId + ' #note').val('');
		setTitleAdd('titleAttr');
		
	},
	getSelectedRow: function(rowId, columnType, columnValueType, status){		
		var divId = $('#tabAttribute').val();
		setSelectBoxValue(divId + ' #createForm #columnType',columnType);
		var id =  $('#' + divId + ' #attributeGrid').jqGrid ('getCell', rowId, 'id');
		var code =  $('#' + divId + ' #attributeGrid').jqGrid ('getCell', rowId, 'attributeCode');
		var name =  $('#' + divId + ' #attributeGrid').jqGrid ('getCell', rowId, 'attributeName');
		var columnName =  $('#' + divId + ' #attributeGrid').jqGrid ('getCell', rowId, 'columnName');
		var note =  $('#' + divId + ' #attributeGrid').jqGrid ('getCell', rowId, 'note');
		$('#' + divId + ' #selAttrId').val(id);
		$('#errExcelMsg').html('').hide();
		$('#' + divId + ' #searchForm').hide();
		$('#' + divId + ' #createForm').show();
		$('#' + divId + ' #createForm #attributeCode').val(code);
		$('#' + divId + ' #createForm #attributeName').val(name);
		$('#' + divId + ' #createForm #attributeCode').attr('disabled','disabled');		
		$('#' + divId + ' #createForm #columnType').attr('disabled','disabled');
		setSelectBoxValue(divId + ' #createForm #attributeStatus',status);
		setSelectBoxValue(divId + ' #columnValueType',columnValueType);	
		setTimeout(function(){
			AttributesManager.getListColumnName(columnType,'columnName',columnName);
		},300);
		$('#' + divId + ' #createForm #columnName').attr('disabled','disabled');
		$('.disable').addClass('BoxDisSelect');
		$('#' + divId + ' #note').val(note);
		setTitleUpdate('titleAttr');
	},
	getListValue: function(attrId,columnTypeValue){
		$('#dlgColumnTypeValue').val(columnTypeValue);
		var divId = $('#tabAttribute').val();		
		var html = $('#' + divId + ' #dlgListAttributeValue').html();
		var title = 'Danh sách giá trị';
		$.fancybox(html,{
			modal : true,
			title : title,						
			autoResize: false,
			afterShow : function() {				
				$('#' + divId + ' #dlgListAttributeValue').html('');
				$('#dlgSelAttrId').val(attrId);
				$('.fancybox-inner #attrDetailValue').focus();
				var url = '/attributes/list-value?id=' + attrId;
//				if($('#gbox_attrDetailGrid').html() != null && $('#gbox_attrDetailGrid').html().trim().length > 0){
//					$("#attrDetailGrid").setGridParam({url:url,page:1}).trigger("attrDetailGrid");
//				} else {
				$('#attrDetailGrid').jqGrid({
					url : url,
					colModel : [{name : 'attribute.attributeCode',index : 'attribute.attributeCode',align : 'left',label : 'Mã thuộc tính',sortable : false,resizable : false},
					            {name : 'attributeDetailValue',index : 'attributeDetailValue',label : 'Giá trị',sortable : false,resizable : false,align : 'left'},
					            {name : 'attributeDetailName',index : 'attributeDetailName',label : 'Tên',sortable : false,resizable : false,align : 'left'},
					            {name:'edit', label: 'Sửa', width: 50, align: 'center',sortable:false,resizable:false, formatter: AttributeManagerFormatter.editDetailCellIconFormatter},
					            {name:'edit', label: 'Xóa', width: 50, align: 'center',sortable:false,resizable:false, formatter: AttributeManagerFormatter.deleteDetailCellIconFormatter},
					            {name : 'id',index : 'id',hidden : true}
					            ],
					            pager : '#attrDetailPager',
								height : 'auto',rowNum : 10,
								width : ($('#attrDetailContainer').width()),
								gridComplete : function() {
									$('#jqgh_attrDetailGrid_rn').html('STT');
									$('#pg_attrDetailGridPager').css('padding-right','20px');
									updateRownumWidthForJqGrid('.fancybox-inner');
									$(window).resize();
								}												
							}).navGrid('#attrDetailGrid',{edit : false,add : false,del : false,search : false});
//				}
			},
			afterClose : function() {
				$('#' + divId + ' #dlgListAttributeValue').html(html);
			}
		});
	},
	getSelectedDetailRow: function(rowId){
		var divId = $('#tabAttribute').val();
		var id =  $('#attrDetailGrid').jqGrid ('getCell', rowId, 'id');
		var value = $('#attrDetailGrid').jqGrid ('getCell', rowId, 'attributeDetailValue');
		var name = $('#attrDetailGrid').jqGrid ('getCell', rowId, 'attributeDetailName');
		$('#selDetailId').val(id);
		$('#attrDetailValue').val(value);
		$('#attrDetailName').val(name);
		$('#btnSaveDetail span').text('Lưu');
	},
	deleteDetailValue: function(detailId){
		var dataModel = new Object();
		var type = $('#typeAttribute').val();
		dataModel.detailId = detailId;	
		dataModel.type = type;	
		Utils.deleteSelectRowOnGrid(dataModel, 'giá trị thuộc tính', '/attributes/delete-value', AttributesManager._xhrDelete, 'attrDetailGrid', 'errAttrValue',function(data){
			if(!data.error){
				$('#succAttrValue').html('Xóa dữ liệu thành công').show();
				var tm = setTimeout(function(){
					$('#succAttrValue').html('').hide();
					clearTimeout(tm);
				}, 3000);
				AttributesManager.clearDetailForm();
			}else{
				$('.fancybox-inner #errAttrValue').html(data.errMsg).show();
			}
		},null,'.fancybox-inner');
		return false;
	},
	clearDetailForm: function(){
		var divId = $('#tabAttribute').val();
		$('#selDetailId').val(0);
		$('#attrDetailValue').val('');
		$('#attrDetailName').val('');
		$('#btnSaveDetail span').text('Thêm');
		var url = '/attributes/list-value?id=' + $('#dlgSelAttrId').val();
		$('#attrDetailGrid').setGridParam({url:url,page:1}).trigger("reloadGrid");
//		$('#' + divId + '#attrDetailGrid').trigger('reload');
	},
	saveOrUpdateDetail: function(){
		var divId = $('#tabAttribute').val();
		var columnTypeValue = $('#dlgColumnTypeValue').val();
		var msg = '';
		$('#errAttrValue').html('').hide();
		msg = Utils.getMessageOfRequireCheck('attrDetailValue','Giá trị');
		if(msg.trim().length == 0){
			if(columnTypeValue == 1){
				msg = Utils.getMessageOfSpecialCharactersValidateEx('attrDetailValue','Giá trị',Utils._TF_NUMBER);
			}else if(columnTypeValue == 2){
				msg = Utils.getMessageOfSpecialCharactersValidate('attrDetailValue','Giá trị',Utils._CODE);
			}else{
				if(!Utils.isDate($('#attrDetailValue').val().trim())){
					msg = 'Giá trị không hợp lệ, vui lòng nhập theo định dạng dd/MM/yyyy';
					$('#attrDetailValue').focus();
				}
			}
		}
		if(msg.trim().length == 0){
			msg = Utils.getMessageOfRequireCheck('attrDetailName','Tên');
		}
		if(msg.trim().length == 0){
			msg = Utils.getMessageOfSpecialCharactersValidate('attrDetailName','Tên');
		}		
		if(msg.length > 0){
			$('#errAttrValue').html(msg).show();
			return false;
		}
		var dataModel = new Object();
		dataModel.detailId = $('#selDetailId').val().trim();
		dataModel.id = $('#dlgSelAttrId').val();
		dataModel.detailValue = $('#attrDetailValue').val().trim();
		dataModel.detailName = $('#attrDetailName').val().trim();		
		Utils.addOrSaveData(dataModel, "/attributes/save-value", AttributesManager._xhrSave,'errAttrValue', function(data){
			if(!data.error){
				$('#succAttrValue').html('Lưu dữ liệu thành công').show();
				var tm = setTimeout(function(){
					$('#succAttrValue').html('').hide();
					clearTimeout(tm);
				}, 3000);
				AttributesManager.clearDetailForm();
			}
		},null,'.fancybox-inner');		
		return false;
	},
	deleteAttribute: function(){
		$('#errExcelMsg').html('').hide();
		var divId = $('#tabAttribute').val();				
		$('#errMsgAttr').html('').hide();
		var arrId = new Array();
		$('#' + divId + ' #listAttr input[type=checkbox]').each(function(){
			if($(this).is(':checked')){
				var id= $(this).attr('id').replace('chk_attr_','');
				arrId.push(id);
			}
		});
		if(arrId.length == 0){
			$('#errExcelMsg').html('Bạn chưa chọn dữ liệu thuộc tính nào').show();			
		} else {
			var dataModel = new Object();
			dataModel.lstId = arrId;	
			Utils.deleteSelectRowOnGrid(dataModel, 'dữ liệu thuộc tính', '/attributes/delete', AttributesManager._xhrDelete, null, 'errMsgAttr',function(data){
				if(!data.error){
					for(var i=0;i<data.lstDeleted.length;i++){
						$('#tr_attr_' + data.lstDeleted[i]).remove();
						$('#srlLstAttr').jScrollPane();
					}
					AttributesManager.searchAttribute();
				}
			});
		}		
		return false;
	},
	exportAttribute: function(typeExport){
		$('#errExcelMsg').html('').hide();
		var divId = $('#tabAttribute').val();
		var type = $('#typeAttribute').val();
		var arrId = new Array();
		$('#' + divId + ' #listAttr input[type=checkbox]').each(function(){
			if($(this).is(':checked')){
				var id= $(this).attr('id').replace('chk_attr_','');
				arrId.push(id);
			}
		});
		if(arrId.length == 0){
			$('#errExcelMsg').html('Bạn chưa chọn dữ liệu thuộc tính nào').show();			
		} else {
			var dataModel = new Object();
			dataModel.lstId = arrId;
			dataModel.type = type;
			dataModel.listAll = typeExport;
			var kData = $.param(dataModel, true);
			$.messager.confirm('Xác nhận', 'Bạn có muốn xuất file Excel?', function(r){
				if (r){
					$.ajax({
						type : 'GET',
						url : '/attributes/export',
						data :(kData),
						dataType : "json",
						success : function(data) {
							if(data!= null && data!= undefined && data.downloadLink!= undefined){
								document.location.href = data.downloadLink;
								setTimeout(function(){ //Set timeout để đảm bảo file load lên hoàn tất
		                            CommonSearch.deleteFileExcelExport(data.downloadLink);
								},2000);
							}					
						},
						error:function(XMLHttpRequest, textStatus, errorThrown) {}});
				}
			});	
		}
	},
	importExcel: function(typeView){
		var divId = $('#tabAttribute').val();
		var type = $('#typeAttribute').val();
		var options = { 
				beforeSubmit: AttributesManager.beforeImportExcel,   
		 		success:      AttributesManager.afterImportExcel, 
		 		type: "POST",
		 		dataType: 'html',
		 		data:({typeView:typeView,type:type})
		 	}; 
		$('#' + divId + ' #importAttrFrm').ajaxForm(options);
		$('#' + divId + ' #importAttrFrm').submit();
	},	
	beforeImportExcel: function(){
		if(!previewImportExcelFile(document.getElementById("excelFileAttr"))){
			return false;
		}		
		$('#errExcelMsg').html('').hide();
		$('#successMsg').html('').hide();
		$('#divOverlay').show();
		//$('#imgOverlay').show();
		return true;
	},
	afterImportExcel: function(responseText, statusText, xhr, $form){
		$('#divOverlay').hide();
		//$('#imgOverlay').hide();				
		if (statusText == 'success') {
	    	$("#responseDiv").html(responseText);	    	
	    	if($('#errorExcel').html().trim() == 'true' || $('#errorExcelMsg').html().trim().length > 0){
	    		$('#errExcelMsg').html($('#errorExcelMsg').html()).show();
	    	} else {
	    		if($('#typeViewResponse').html().trim()=='1'){
	    			var html = $('#dlgViewAttrExcel').html();
	    			$.fancybox(html,
    					{
    						modal: true,
    						title: 'Danh sách dữ liệu từ excel',
    						afterShow: function(){ 
    							$('#dlgViewAttrExcel').html('');
    							$('.ColsTd2_'+(parseInt($('#lstSize').val())-1)).css('text-align','left');
    							$('.ColsTd2_'+(parseInt($('#lstSize').val())-1)).css('word-break','normal');
    							$('.classTable ').css('width',parseInt($('#lstSize').val())*150 + 50 + 'px');
    							$('.BoxGeneralTTitle ').css('width',parseInt($('#lstSize').val())*150 + 50 + 'px');
    							$('.BoxGeneralTBody ').css('width',parseInt($('#lstSize').val())*150 + 50 + 'px');
    							$('#srlLstAttrExcel').jScrollPane();
    							$('#scrollSectionId').jScrollPane().data().jsp;
    						},
    						afterClose: function(){    	
    							$('#dlgViewAttrExcel').html(html);
    						}
    					}
    				);
	    		} else {
	    			var totalRow = parseInt($('#totalRow').html().trim());
		    		var numFail = parseInt($('#numFail').html().trim());
		    		var fileNameFail = $('#fileNameFail').html();
		    		var mes = format(msgErr_result_import_excel,(totalRow - numFail),numFail);		    		
		    		if(numFail > 0){
		    			mes+= ' <a href="'+ fileNameFail +'">Xem chi tiết lỗi</a>';
		    		}
		    		$('#errExcelMsg').html(mes).show();
	    		}	    		
	    	}
//	    	$('#fakefilepc2').val('');
//			$('#excelFileAttr').val('');
	    }
	},
	getListColumnName: function(value,object,columnValue){
		columnValue = Utils.XSSEncode(columnValue);
		$.ajax({
			type : "POST",
			url : "/attributes/list/column-name",
			data : ({columnType : value,type:$('#tableNameType').val()}),
			dataType: "json",
			success : function(data) {
				var arrHtml = new Array();
				arrHtml.push('<option value="-2">--- Chọn tên trường ---</option>');
				var size = data.lstColumnName.length;
				for(var i=0;i < size; i++){
					arrHtml.push('<option value="'+ Utils.XSSEncode(data.lstColumnName[i].name) +'">'+ Utils.XSSEncode(data.lstColumnName[i].name) +'</option>');
				}
				if(columnValue != -2){
					arrHtml.push('<option value="'+ Utils.XSSEncode(columnValue) +'">'+ Utils.XSSEncode(columnValue) +'</option>');
				}
				$('#createForm #' + object).html(arrHtml.join(""));
				$('#createForm #' + object).val(Utils.XSSEncode(columnValue));
				$('#createForm #' + object).change();
			},
			error:function(XMLHttpRequest, textStatus, errorThrown) {
				$.ajax({
					type : "POST",
					url : '/check-session',
					dataType : "json",
					success : function(data) {
						$('#loading').css('visibility','hidden');
					},
					error: function(XMLHttpRequest, textStatus, errorThrown) {
						window.location.href = '/home';
					}
				});
			}
		});
	}
};
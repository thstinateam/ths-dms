var ProductCatalog = {
	_xhrSave : null,
	_xhrDel: null,	
	_listCategory:null,
	countArray:null,
	_flagDropzone: false,
	dropzoneView: null,
	_isBkLatLng :false,
	latbk : '',
	lngbk : '',
	gotoTab: function(tabIndex){
		ProductCatalog.deactiveAllMainTab();
		switch (tabIndex) {
		case 0:
			$('#tabContent1').show();
			$('#infoTab').addClass('Active');
			break;
		case 1:
			if ($('#productId').val().trim() == '') {
				$('#tabContent1').show();
				$('#infoTab').addClass('Active');
				break;
			}
			$('#tabContent2').show();
			$('#imageTab').addClass('Active');
			ProductCatalog.loadImage();
			break;
		default:
			$('#tabContent1').show();
			break;
		}
		$('#errExcelMsg').html('').hide();
	},
	deactiveAllMainTab: function(){
		$('#infoTab').removeClass('Active');
		$('#imageTab').removeClass('Active');
		$('#tabContent1').hide();
		$('#tabContent2').hide();
	},
	getGridUrl: function(productCode, productName, categories, subCategories, brandId, flavourId, packingId, uom1, status, isExceptZCat, isPriceValid){
		var param = '';
		if (isExceptZCat != null && isExceptZCat != undefined){
			param += "&isExceptZCat=" +isExceptZCat;
		}
		if (isExceptZCat != null && isExceptZCat != undefined){
			param += "&isPriceValid=" +isPriceValid;
		}
		if (categories != null && categories.length > 0) {
			for (var i = 0; i < categories.length; i++) {
				param += '&lstCategoryId=' + categories[i];
			}
		}		
		if (subCategories != null && subCategories.length > 0) {
			for (var i = 0; i < subCategories.length; i++) {
				param += '&lstSubCategoryId=' + subCategories[i];
			}
		}
		if (brandId != null && brandId != undefined) {
			param += "&brandId=" + brandId;
		}
		if (flavourId != null && flavourId != undefined) {
			param += "&flavourId=" + flavourId;
		}
		if (packingId != null && packingId != undefined) {
			param += "&packingId=" + packingId;
		}
		return "/catalog/product/search?productCode=" + encodeChar(productCode) + "&productName=" + encodeChar(productName) + "&uom1=" + uom1 + "&status=" + status + param;
	},
	
	getImageGridUrl: function(productId){
		return "/catalog/product/searchimage?productId=" + encodeChar(productId);
	},
 	
	search: function(){
		$('#errMsg').html('').hide();
		var productCode = $('#productCode').val().trim();
		var productName = $('#productName').val().trim();
		
		var brand = $('#brand').val().trim();
		if (brand == '-2') {
			brand = '';
		}
		var flavour = $('#flavour').val().trim();
		if (flavour == '-2') {
			flavour = '';
		}
		var packing = $('#packing').val().trim();
		if (packing == '-2') {
			packing = '';
		}
		
		var uom1 = $('#uom1').val().trim();
		if (uom1 == '-2') {
			uom1 = '';
		}
		var status = $('#status').val().trim();
		setTimeout(function() {
			$('#productCode').focus();
		}, 500);
		
		var arrCat = $('#category').val();
		if (arrCat == null || (arrCat.length > 0 && arrCat[0] == 0)) {
			arrCat = [];
		}
		
		var arrSubCat = $('#categoryChild').val();
		if (arrSubCat == null || (arrSubCat.length > 0 && arrSubCat[0] == 0)) {
			arrSubCat = [];
		}
		
		var url = ProductCatalog.getGridUrl(productCode, productName, arrCat, arrSubCat, brand, flavour, packing, uom1, status, true);
		$("#grid").datagrid('options').sortName = null; // search thi load lai sort binh thuong
		$("#grid").datagrid({url:url,pageNumber:1});
		return false;
	},
	
	searchImage: function(){
		$('#errMsg').html('').hide();
		var productId = $('#productId').val().trim();
		var url = ProductCatalog.getImageGridUrl(productId);
		$("#grid").setGridParam({url:url,pageNumber:1}).trigger("reloadGrid");
		return false;
	},
	
	loadInfo:function(){
		var productId = $("#productId").val();
		var data = new Object();
		data.productId = productId;
		var kData = $.param(data, true);		
		$.ajax({
			type : "POST",
			url : "/catalog/product/info",
			data : (kData),
			dataType: "html",
			success : function(data) {
				$("#divGeneral").html(data).show();
				if(productId != null && productId == 0){
					$('#productCode').focus();
				}else if(productId != null && productId >0){
					$('#productName').focus();
				}
				Utils.bindAutoSave();
				Utils.bindAutoCombineKey();
			},
			error:function(XMLHttpRequest, textStatus, errorThrown) {
				$.ajax({
					type : "POST",
					url : '/check-session',
					dataType : "json",
					success : function(data) {
					},
					error: function(XMLHttpRequest, textStatus, errorThrown) {
						window.location.href = '/home';
					}
				});
			}
		});
		
		$('#productName').focus();		
		return false;
	},
	
	loadImage:function(seq){
		var editor = null;
		var productId = $("#productId").val().trim();
		var data = new Object();
		data.productId = productId;
		var kData = $.param(data, true);
		$.ajax({
			type : "POST",
			url : "/catalog/product/image",
			data : (kData),
			dataType: "html",
			success : function(data) {
				$("#divGeneral").html(data).show();
				editor = CKEDITOR.replace('description');
				if(seq!=undefined && seq!=null){
					$('#sucMsgUpload').html(seq).show();
				}
				ProductCatalog.countArray = new Map();
			},
			error:function(XMLHttpRequest, textStatus, errorThrown) {
				$.ajax({
					type : "POST",
					url : '/check-session',
					dataType : "json",
					success : function(data) {
						editor = CKEDITOR.replace('description');
					},
					error: function(XMLHttpRequest, textStatus, errorThrown) {
						window.location.href = '/home';
					}
				});
			}
		});
		return false;
	},
	
	changeProductInfo: function(){
		var msg = '';
		$('#errMsg').html('').hide();
		msg = Utils.getMessageOfRequireCheck('productCode', catalog_display_product_code);
		if(msg.length == 0){
			msg = Utils.getMessageOfSpecialCharactersValidate('productCode', catalog_display_product_code, Utils._CODE);
			$('#productCode').focus();
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfRequireCheck('productName', catalog_product_name);
			$('#productName').focus();
		}
		if (msg.length == 0) {
			msg = Utils.getMessageOfRequireCheck('category', catalog_categoryvn);
			$('#category').focus();
		}
		if (msg.length == 0) {
			msg = Utils.getMessageOfRequireCheck('categoryChild', catalog_categoryvn_child);
			$('#categoryChild').focus();
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfRequireCheck('orderIndex', catalog_product_order_index);
			$('#orderIndex').focus();
		}
		if(msg.length == 0){
			var orderIndex = $('#orderIndex').val().trim();
			if (orderIndex < 1) {
				msg = catalog_product_order_index_more_than_zero;
			}
			$('#orderIndex').focus();
		}
		var barCode = $('#barCode').val().trim();
		if(msg.length == 0 && barCode.length > 0 && !/[0-9]{1,}$/.test(barCode)){
			msg = catalog_product_ma_vach_phai_so_nguyen;
			$('#barCode').focus();
		}
		
		var expiryNo = $('#expireNumber').val().trim();
		expiryNo = expiryNo.replace(/,/g, '');
		if(msg.length == 0 && expiryNo != '' && !/\d$/.test(expiryNo)){
			msg = catalog_product_thsd_nam_trong_khong_chin;
			$('#expireNumber').focus();
		}
		if(msg.length==0 && expiryNo.length > 3){
			msg = catalog_product_thsd_vuot_qua_ba_kytu;
			$('#expireNumber').focus();
		};
		var expiryType = $('#expiryType').val().trim();
		if (msg.length == 0 && expiryNo.length > 0 && expiryType == -1) {
			msg = catalog_product_thsd_chon_loai_hsd;
			$('#expiryType').focus();
		}
		if (msg.length == 0 && expiryNo.length <= 0 && expiryType != -1) {
			msg = catalog_product_thsd_nhap_hsd;
			$('#expireNumber').focus();
		}
		
		if(msg.length == 0){
			msg = Utils.getMessageOfRequireCheck('category',catalog_categoryvn,true);
			$('#category').focus();
		}

		if(msg.length == 0){
			msg = Utils.getMessageOfRequireCheck('categoryChild',catalog_categoryvn_child,true);
			$('#category').focus();
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfRequireCheck('uom1', catalog_product_uom1,true);
			$('#uom1').focus();
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfRequireCheck('uom2', catalog_product_uom2,true);
			$('#uom2').focus();
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfRequireCheck('convfact', catalog_proudct_convfact_sl);
			if(msg.length != 0){
				$('#convfact').focus();
			}
		}
		var regex = /^\d*\.?\d*$/;
		var convfact = $('#convfact').val().trim();
		convfact = convfact.replace(/,/g, '');
		if(msg.length == 0 && convfact.length > 0 && !/\d$/.test(convfact)){
			msg = catalog_product_dong_goi_nam_trong_khong_chin;
			$('#convfact').focus();
		}

		var volume = $('#volumn').val().trim();
		if (msg.length == 0 && volume.length > 9) {
			msg = catalog_product_the_tich_gioi_han_chin_kytu;
			$('#volumn').focus();
		}
		volume = volume.replace(/,/g, '');
		if(msg.length == 0 && !regex.test(volume)){
			msg = catalog_product_the_tich_khong_dung_dinh_dang;
			$('#volumn').focus();
		}
		var netWeight = $('#netWeight').val().trim();
		if (msg.length == 0 && netWeight.length > 20) {
			msg = catalog_product_khoi_luong_tinh_gioi_han_haimuoi_kytu;
			$('#netWeight').focus();
		}
		netWeight = netWeight.replace(/,/g, '');
		if(msg.length == 0 && !regex.test(netWeight)){
			msg = catalog_product_khoi_luong_tinh_khong_dung_dinh_dang;
			$('#netWeight').focus();
		}
		var grossWeight = $('#grossWeight').val().trim();
		if (msg.length == 0 && grossWeight.length > 20) {
			msg = catalog_product_tong_kl_gioi_han_haimuoi_kytu;
			$('#grossWeight').focus();
		}
		grossWeight = grossWeight.replace(/,/g, '');
		if(msg.length == 0 && !regex.test(grossWeight)){
			msg = catalog_product_tong_kl_khong_dung_dinh_dang;
			$('#grossWeight').focus();
		}

		if(msg.length == 0 && parseFloat(netWeight)<=0 ){
			msg = catalog_product_net_weight_minus;
			$('#netWeight').focus();
		}
		if(msg.length == 0 && parseFloat(grossWeight)<=0 ){
			msg = catalog_product_tong_kl_phai_so_nguyen;
			$('#grossWeight').focus();
		}
		if(parseFloat(netWeight)>parseFloat(grossWeight)){
			msg = catalog_product_netgross;
			$('#netWeight').focus();
		}
		
		if(msg.length == 0 && parseFloat(volume)<=0 ){
			msg = catalog_product_volumn_minus;
			$('#volumn').focus();
		}
		var id = $('#productId').val().trim();
		if (id != null && id != '') {
			if(msg.length == 0) {
				msg = Utils.getMessageOfRequireCheck('status', jsp_common_status_all,true);
				$('#status').focus();
			}
		}
		if(msg.length > 0){
			$('#errMsg').html(msg).show();
			return false;
		}
		
		var dataModel = new Object();
		dataModel.orderIndex = $('#orderIndex').val().trim();
		var id = $('#productId').val().trim();
		if (id != null && id != '') {
			dataModel.productId = id;
		}
		Utils.addOrSaveData(dataModel, "/catalog/product/checkDulicateOrderIndex", ProductCatalog._xhrSave, null,function(data) {
			if(data.error != undefined && data.error == false){
				if(data.isDulicate) {
					$.messager.confirm(jsp_common_xacnhan, catalog_product_order_index_duplicate, function(r) {  
						if (r) {
							ProductCatalog.postChangeProductInfo();
						}
					});
				} else {
					ProductCatalog.postChangeProductInfo();
				}			
			} else {
				$('#errMsg').val(data.errMsg);
			}
		},null,null,null,null,null,true);
		
	},
	
	postChangeProductInfo: function(productId){			
		var dataModel = new Object();
		var id = $('#productId').val().trim();
		var isCreate = true;
		if (id != null && id != '') {
			dataModel.productId = id;
			isCreate = false;
			dataModel.status = $('#status').val().trim();
		}
		dataModel.productCode = $('#productCode').val().trim();
		dataModel.productName = $('#productName').val().trim();
		dataModel.shortName = $("#shortName").val().trim();
		dataModel.barCode = $('#barCode').val().trim();
		dataModel.category = $('#category').val();
		dataModel.subCat = $('#categoryChild').val();
		dataModel.orderIndex = $('#orderIndex').val();
		dataModel.flavourId = $('#flavour').val();
		dataModel.brandId = $('#brand').val().trim();
		dataModel.packingId = $('#packing').val();
		dataModel.uom1 = $('#uom1').val().trim();
		dataModel.uom2 = $('#uom2').val().trim();
		var netWeight = $('#netWeight').val().trim();
		netWeight = netWeight.replace(/,/g, '');
		dataModel.netWeight =  netWeight;
		
		var grossWeight = $('#grossWeight').val().trim();
		grossWeight = grossWeight.replace(/,/g, '');
		dataModel.grossWeight = grossWeight;
		
		var volume = $('#volumn').val().trim();
		volume = volume.replace(/,/g, '');
		dataModel.volumn = volume;
		
		var convfact = $('#convfact').val().trim();
		convfact = convfact.replace(/,/g, '');
		dataModel.convfact = convfact;
		dataModel.checkLot = $('#lot').val();
		
		var expiryNo = $('#expireNumber').val().trim();
		expiryNo = expiryNo.replace(/,/g, '');
		dataModel.expireNumber = expiryNo;
		dataModel.expireType = $('#expiryType').val().trim();

		if(!Utils.validateAttributeData(dataModel, '#errMsg', '#propertyTabContainer ')) {
			return false;
		}
		
		Utils.saveData(dataModel, "/catalog/product/changeproductinfo", ProductCatalog._xhrSave, null,function(data) {
			if(data.error != undefined && data.error == false){
				$('#productId').val(data.productId);
				$('#convfactHidden').val(Number(data.product.convfact));
				if (isCreate) {
					setTimeout(function(){
						window.location.href = '/catalog/product/viewdetail?productId=' + data.productId;
					}, 3000);
				}
			} else {
				$('#errMsg').val(data.errMsg);
			}
		});
		return false;
	},
	
	changeProductDescription:function(){
		var msg = '';
		$('#errMsg').html('').hide();
		if(msg.length > 0){
			$('#errMsg').html(msg).show();
			return false;
		}
		var dataModel = new Object();
		dataModel.productId = $('#productId').val();
		dataModel.introductionId = $('#introductionId').val();
		dataModel.description = Utils.XSSEncode(CKEDITOR.instances['description'].getData());
		Utils.addOrSaveData(dataModel, "/catalog/product/changeproductdescription", ProductCatalog._xhrSave, null,
				function(rs) {
					setTimeout(function() {
						$('#tabContent2 .TextEditorInSection #successMsg').html('').hide();
					}, 3000);
		}, null, '#tabContent2 .TextEditorInSection');
		return false;
	},
	/** upload images*/
	changeProductImage:function(productId){
		$('#errMsgUpload').html('').hide();
		var msg = ''; 
		if(msg.lenght==0){
			msg = Utils.getMessageOfSpecialCharactersValidate('imageFile', catalog_product_ten_hinh_anh, Utils._NAME);
		}
		if(msg.lenght==0){
			msg = Utils.getMessageOfSpecialCharactersValidate('videoFile', catalog_product_ten_video,Utils._NAME);
		}
		var size = parseInt($('#vnmTotalImage').val().trim()) + ProductCatalog.countArray.size();
		if( size>20){
			msg = catalog_product_upload_toi_da_hinh_sp;
		}
		if(msg.length==0 && $('#fileQueue').html().length>0 && ProductCatalog.countArray.size()==0){
			msg = catalog_product_ko_chon_hinh_sp;
		}
		if(msg.length>0){
			$('#errMsgUpload').html(msg).show();
			return false;
		}
		
		$('#divOverlay').show();
		$('#imageFile').uploadifySettings('scriptData',{
			'productId':productId,
			'currentUser':currentUser
		});
		if($('#videoMsg').html() != "" && parseInt($('#mediaType').val()) == 1){
			$('#videoMsg').html(catalog_product_sp_mot_video).show();
		}
		$('#imageFile').uploadifyUpload();
		if($('#fileQueue').html() == "" || parseInt($('#successUpload').val().trim()) == 1){
			$('#importFrm').submit();
		}
		return false;
	},
	/**BEGIN UPLOAD IMAGE**/
	initUploadImageElement: function(elementSelector, elementId, clickableElement) {
		// Get the template HTML and remove it from the doumenthe template HTML and remove it from the doument
		var previewNode = document.querySelector("#template");
		previewNode.id = "";
		var previewTemplate = previewNode.parentNode.innerHTML;
		previewNode.parentNode.removeChild(previewNode);

		UploadUtil.initializeUploadElement(elementSelector, elementId, {
			url: "/catalog/product/uploadImageFile",
			paramName: function(fileOrderNumber) {
				return 'images';
			},
			autoProcessQueue: false,
			parallelUploads: 1000,
			uploadMultiple: true,
			thumbnailWidth: 50,
			thumbnailHeight: 50,
			previewTemplate: previewTemplate,
			autoQueue: false,
			previewsContainer: "#previews",
			acceptedFiles: "video/*, image/*",
			maxFilesize: 20,// MB
			clickable: clickableElement,
			successmultiple: function(files, responseData) {
				Utils.updateTokenForJSON(responseData);
				UploadUtil.hideTotalProgressBar();
				if (responseData != undefined) {
					var failCount = 0;
					if (responseData.fail && responseData.fail.length > 0) {
						failCount = responseData.fail.length;
						UploadUtil.readdFailFileForUpload(responseData.fail);
					} else {
						UploadUtil.clearAllSuccessUploadedFile();
					}
					$('#divOverlay').hide();
					var msg = format(catalog_product_video_tai_tc_ha_phim, (files.length - failCount) + '/' + files.length);
					$('#sucMsgUpload').html(msg).show();
					if(responseData.errMsg != null && responseData.errMsg != ""){
						if (responseData.failFormat && responseData.failFormat.length > 0) {
							UploadUtil.readdFailFileForUpload(responseData.failFormat);
						}
						$('#errMsgUpload').html(responseData.errMsg).show();
					} else {
						ProductCatalog.loadImage(msg);
					}
				} else {
					$('#divOverlay').hide();
					if (responseData.failFormat && responseData.failFormat.length > 0) {
						UploadUtil.readdFailFileForUpload(responseData.failFormat);
					}
					$('#errMsgUpload').html(responseData.errMsg).show();
				}
			},
		});
		
	},
	uploadImageFile: function(idProduct) {
		$('.ErrorMsgStyle').html('').hide();
		$('.SuccessMsgStyle').html('').hide();
		var errView = 'errMsgUpload';
		ProductCatalog.updateAdditionalDataForUpload(idProduct);
		UploadUtil.startUpload(errView);
		
	},
	updateAdditionalDataForUpload: function(idProduct) {		
		var data = JSONUtil.getSimpleObject2({
			productId: idProduct
		});
		UploadUtil.updateAdditionalDataForUpload(data);
	},
	/** END UPLOAD IMAGE**/
	deleteMediaItem: function(){
		var dataModel = new Object();
		dataModel.mediaItemId = $('#mediaItemId').val();
		$.messager.confirm(jsp_common_xacnhan, catalog_product_video_ban_co_muon_xoa, function(r){
			if (r){
				Utils.saveData(dataModel, "/catalog/product/removemediaitem", ProductCatalog._xhrDel, null,function(data){
					ProductCatalog.loadImage();
				},null,null,true);
			}
		});
		$('.messager-window').css('border-radius', '0');
		return false;		
	},

	
	viewBigMapOnDlg : function(id, callBack) {
		var selector = $('#' + id);
		if(ProductCatalog._isBkLatLng == false){
			var latLngbk = selector.val().trim();
			if(!isNullOrEmpty(latLngbk) && latLngbk.indexOf(';')>=0){
				ProductCatalog.latbk = latLngbk.split(';')[0];
				ProductCatalog.lngbk = latLngbk.split(';')[1];
			}
			ProductCatalog._isBkLatLng = true;		
		}
		var html = $('#viewBigMap').html();
		$('#viewBigMap').dialog({
			closed: false,  
			title : xem_ban_do,
	        cache: false,  
	        modal: true,
	        onOpen: function(){
	        	$('.easyui-dialog #imgBtnDeleteLatLng').unbind('click');
	        	$('.easyui-dialog #imgBtnDeleteLatLng').bind('click',function(){
	        		selector.val('');
	        		ViettelMap.clearOverlays();
	        		ViettelMap._listOverlay = new Array(); 
	        		if (ViettelMap._marker != null){
	                	ViettelMap._marker.setMap(null);
	                }
	                
	        	});
	        	$('.easyui-dialog #imgBtnUpdateLatLng').unbind('click');
	        	$('.easyui-dialog #imgBtnUpdateLatLng').bind('click',function(){
	        		var lat = '';
					var lng = '';
					var latLng = selector.val().trim();
					if(!isNullOrEmpty(latLng) && latLng.indexOf(';')>=0){
						lat = latLng.split(';')[0];
						lng = latLng.split(';')[1];
					}
					var zoom = 12;
					if(lat.trim().length==0 || lng.trim().length==0){
						zoom = ViettelMap._DEF_ALL_ZOOM;
					}
					VTMapUtil.loadMapResource(function(){
						ViettelMap.loadBigMapEx('bigMapContainer', lat, lng, zoom, function(latitude, longtitude) {
							selector.val(latitude + ';' + longtitude);
						}, true, true);
		    		});
		    		$('#viewBigMap').dialog('close');
	        	});
	        	$('.easyui-dialog #imgBtnCancelLatLng').unbind('click');
	        	$('.easyui-dialog #imgBtnCancelLatLng').bind('click',function(){
	        		var zoom = 12;
					if(ProductCatalog.latbk.trim().length==0 || ProductCatalog.lngbk.trim().length==0){
						zoom = ViettelMap._DEF_ALL_ZOOM;
					}
					VTMapUtil.loadMapResource(function(){
						ViettelMap.loadBigMapEx('bigMapContainer', ProductCatalog.latbk, ProductCatalog.lngbk, zoom, function(latitude, longtitude) {
							selector.val(latitude + ';' + longtitude);
						}, true, true);
		    		});
	        		//$('#viewBigMap').onClose();
	        	});
	        	var tm = setTimeout(function(){					
					var lat = '';
					var lng = '';
					var latLng = selector.val().trim();
					if(!isNullOrEmpty(latLng) && latLng.indexOf(';')>=0){
						lat = latLng.split(';')[0];
						lng = latLng.split(';')[1];
					}
					var zoom = 12;
					if(lat.trim().length==0 || lng.trim().length==0){
						zoom = ViettelMap._DEF_ALL_ZOOM;
					}
					VTMapUtil.loadMapResource(function(){
						ViettelMap.loadBigMapEx('bigMapContainer', lat, lng, zoom, function(latitude, longtitude) {
							selector.val(latitude + ';' + longtitude);
						}, true);
		    		});
					clearTimeout(tm);
				}, 500);
	        },
	        onClose:function() {
	        	$('#viewBigMap').html(html);
	        	if (callBack != undefined && callBack != null) {
	        		callBack.call(this);
	        	}
	        }
		});
		
	},
	
	resetApParam:function(){
		var msg = '';
		$('#errExcelMsg').html('').hide();
		msg = Utils.getMessageOfRequireCheck('apParam','Chọn trường xóa dữ liệu',true);
		if(msg.length > 0){
			$('#errExcelMsg').html(msg).show();
			return false;
		}
		var dataModel = new Object();
		var apParamCode = $('#apParam').val().trim();
		dataModel.apParamCode = apParamCode;
		Utils.deleteSelectRowOnGrid(dataModel, $('#apParam').val().trim(), "/catalog/product/resetapparam", ProductCatalog._xhrDel, null,'errExcelMsg',null);
		return false;
	},
	
	getIntegerFromFloat:function(price){
		var a = Utils.returnMoneyValue(price);
		if((a).indexOf('.') > 0){
			var e= a.split('.');
			var c = e[1].substring(0,1);
			if(c >= 5){
				c =parseInt(e[0])+1;
			}else{
				c=parseInt(e[0]);
			}
			price = c;
		}
		return price;
	},
	getListMediaItem:function(type){
		if(type == 0){
			if($('#numIndex').val() != null && parseInt($('#numIndex').val()) > 0){
				$('#numIndex').val(parseInt($('#numIndex').val())-1);
				if($('#numIndex').val() == 0){
					$('#pageUp').hide();
				}
			}
			$('#pageDown').show();
		}else{
			$('#numIndex').val(parseInt($('#numIndex').val())+1);
			if($('#numIndex').val() == Math.floor($('#numPage').val()/8)){
				$('#pageDown').hide();
			}
			$('#pageUp').show();
		}
		$.ajax({
			type : "POST",
			url : "/catalog/product/getlistmediaitem",
			data : ({productId:$('#productId').val(),numIndex:$('#numIndex').val()}),
			dataType: "html",
			success : function(data) {
				$('#divMI').html(data);
			},
			error:function(XMLHttpRequest, textStatus, errorThrown) {
				$.ajax({
					type : "POST",
					url : '/check-session',
					dataType : "json",
					success : function(data) {
					},
					error: function(XMLHttpRequest, textStatus, errorThrown) {
						window.location.href = '/home';
					}
				});
			}
		});
	},
	
	exportExcelData:function(){
		$('#errMsg').html('').hide();
		$('#errExcelMsg').html('').hide();
		$('#sucExcelMsg').html('').hide();
		$.messager.confirm(jsp_common_xacnhan, catalog_product_export_file, function(r){
			if (r){
				showLoadingIcon();
				var dataModel = new Object();
				
				dataModel.productCode = $('#productCode').val().trim();
				dataModel.productName = $('#productName').val().trim();

				var uom1 = $('#uom1').val().trim();
				if (uom1 == '-2') {
					uom1 = '';
				}
				dataModel.uom1 = uom1;
				
				var brandId = $('#brand').val().trim();
				if (brandId == '-2') {
					brandId = '';
				}
				dataModel.brandId = brandId;
				
				var flavourId = $('#flavour').val().trim();
				if (flavourId == '-2') {
					flavourId = '';
				}
				dataModel.flavourId = flavourId;
				
				var packingId = $('#packing').val().trim();
				if (packingId == '-2') {
					packingId = '';
				}
				dataModel.packingId = packingId;
				
				dataModel.status = $('#status').val().trim();
				var arrCat = $('#category').val();
				if (arrCat == null || (arrCat.length > 0 && arrCat[0] == 0)) {
					arrCat = [];
				}
				dataModel.lstCategoryId =  arrCat;
				var arrSubCat = $('#categoryChild').val();
				if (arrSubCat == null || (arrSubCat.length > 0 && arrSubCat[0] == 0)) {
					arrSubCat = [];
				}
				dataModel.lstSubCategoryId =  arrSubCat;
				var url = "/catalog/product/export-product";
				ReportUtils.exportReport(url, dataModel, 'errExcelMsg');
			}
		});
 		return false;
	},
	searchPackageByConvfact:function(convfact){		
		var obj = new Object();
		obj.productId = Number($('#productId').val());
		obj.convfact = convfact;
		Utils.getJSONDataByAjaxNotOverlay(obj,'/catalog/product/search-package',function(data){
			if(!data.error) {				
				var innerHtml ='<option value="-2" selected="selected" >-- Lựa chọn --</option>';
				$(data.lstPackage).each(function(i,e){
					innerHtml += ' ';
					innerHtml += '<option value="' + Utils.XSSEncode(e.productCode) + '"';
					
					innerHtml += '>';
					innerHtml += Utils.XSSEncode(e.productCode) +' - ' + Utils.XSSEncode(e.productName);
					innerHtml += '</option>';
				});
				$('#baoBi').html(innerHtml);
				$('#baoBi').change();
			}
		});
	},
 	viewExcel:function(){
 		$('#isView').val(1);
 		var options = { 
				beforeSubmit: ProductLevelCatalog.beforeImportExcel,   
		 		success:      ProductLevelCatalog.afterImportExcelUpdate, 
		 		type: "POST",
		 		dataType: 'html',
		 		data:({excelType:$('#excelType').val()})
		 	}; 
		$('#importFrm').ajaxForm(options);
		$('#importFrm').submit();
 		return false;
 	},
	
	/**
	 * LacNV
	 * get category and fill to category select box
	 */
	fillCategoriesToMultiSelect: function(isCheck) {
		$.ajax({
			url: '/catalog/product/get-category?isOrderByCode=true',
			method: 'GET',
			success: function(rs) {
				if (rs.error != undefined && !rs.error) {
					var lstCategory = rs.rows;
					//$('#category').removeAttr('disabled');
					var html = '';
					if(lstCategory.length > 0){
						if(isCheck == 1)
							{
								html += '<option value="0" selected="selected">' + Utils.XSSEncode(jsp_common_status_all) +'</option>';
							}else{
								html += '<option value="0" >' + Utils.XSSEncode(jsp_common_status_all) + '</option>';
							}
						for(var i=0; i < lstCategory.length; i++){
							if(isCheck == 1){
							html += '<option selected="selected" value="' + lstCategory[i].id +'">' +
								Utils.XSSEncode(lstCategory[i].productInfoCode) + ' - ' +
								Utils.XSSEncode(lstCategory[i].productInfoName) + '</option>';
							}else{	
								html += '<option  value="' + lstCategory[i].id +'">' +
									Utils.XSSEncode(lstCategory[i].productInfoCode) + ' - ' +
									Utils.XSSEncode(lstCategory[i].productInfoName) + '</option>';									
							}
						}
						$('#category').html(html);
						$('#category').change();
						$('#ddcl-category').remove();
						$('#ddcl-category-ddw').remove();
						$('#category').dropdownchecklist({
							emptyText: catalog_product_chon_nganh_hang,
							firstItemChecksAll: true,
							maxDropHeight: 350
						});
					} else {
						$('#category').html('');
						//$('#category').attr('disabled','disabled');
						$('#category').dropdownchecklist({
							emptyText:'-------------------',
							firstItemChecksAll: true,
							maxDropHeight: 350
						});
					}
					$('.ui-dropdownchecklist-text').css({"padding-left":"5px"});
				}
			}
		});
	},
	
	fillCategoryChildToMultiSelect: function(isCheck) {
		var param = '';
		var categories = $('#category').val();
		if (categories != null && categories.length > 0) {
			for (var i = 0; i < categories.length; i++) {
				param += '&lstProductInfoId=' + categories[i];
			}
		}
		$.ajax({
			url: '/catalog/product/get-category-child?status=2' +param,
			method: 'GET',
			success: function(rs) {
				if (rs.error != undefined && !rs.error) {
					var lstCategory = rs.rows;
					//$('#categoryChild').removeAttr('disabled');
					var html = '';
					if(lstCategory.length > 0){
						if(isCheck == 1)
							{
								html += '<option value="0" selected="selected">' + Utils.XSSEncode(jsp_common_status_all) + '</option>';
							}else{
								html += '<option value="0" >' + Utils.XSSEncode(jsp_common_status_all) +'</option>';
							}
						for(var i=0; i < lstCategory.length; i++){
							if(isCheck == 1){
								html += '<option selected="selected" value="' + Utils.XSSEncode(lstCategory[i].id) +'">' +
									Utils.XSSEncode(lstCategory[i].productInfoCode) + ' - ' +
									Utils.XSSEncode(lstCategory[i].productInfoName) + '</option>';
							}else{	
								html += '<option  value="' + lstCategory[i].id +'">' +
									Utils.XSSEncode(lstCategory[i].productInfoCode) + ' - ' +
									Utils.XSSEncode(lstCategory[i].productInfoName) + '</option>';									
							}
						}
						$('#categoryChild').html(html);
						$('#categoryChild').change();
						$('#ddcl-categoryChild').remove();
						$('#ddcl-categoryChild-ddw').remove();
						$('#categoryChild').dropdownchecklist({
							emptyText: catalog_product_chon_nganh_hang,
							firstItemChecksAll: true,
							maxDropHeight: 350
						});
					} else {
						$('#categoryChild').html('');
						$('#ddcl-categoryChild').remove();
						$('#ddcl-categoryChild-ddw').remove();
						//$('#category').attr('disabled','disabled');
						$('#categoryChild').dropdownchecklist({
							emptyText:'-------------------',
							firstItemChecksAll: true,
							maxDropHeight: 350
						});
					}
					$('.ui-dropdownchecklist-text').css({"padding-left":"5px"});
				}
			}
		});
	},
	
	/**
	 * LacNV
	 * get uom (1 or 2) and fill to uom select box has id
	 */
	fillUoms: function(val, type, objectId, hasAll) {
		$.ajax({
			url: '/catalog/product/get-uom?uom1=' + type,
			method: 'GET',
			success: function(rs) {
				if (rs.error != undefined && !rs.error) {
					var lstUom = rs.lstUom;
					var html = '';
					if (hasAll != undefined && hasAll==1) {
						html = '<option value="-2" selected="selected">' + Utils.XSSEncode(jsp_common_status_all) + '</option>';
					}
					for(var i=0; i < lstUom.length; i++){
						html += '<option value="' + Utils.XSSEncode(lstUom[i].apParamCode) +'">' +
							Utils.XSSEncode(lstUom[i].apParamName) + '</option>';  
					}
					$('#' + objectId).html(html);
					$('#' + objectId).find("option[value=" + val +']').attr('selected','selected');
					$('#' + objectId).change();
				}
			}
		});
	},
	
	/**
	 * LacNV
	 * get category and fill to category select box
	 */
	fillProductInfoToSelect: function(val, type, objectId, hasAll) {
		if (type == undefined) {
			type = 1; // category
		}
		if (objectId == undefined) {
			objectId = 'category';
		}
		var txt = '';
		if(type == 1){
			//var txt = '-- Chọn ngành hàng --';
			txt = catalog_product_chon_nganh_hang;
		}
		if (type == 3) {
			//txt = '-- Chọn nhãn hiệu --';
			txt = catalog_product_chon_nhan_hieu;
		} else if (type == 4){
			//txt = '-- Chọn hương vị --';
			txt = catalog_product_chon_huong_vi;
		} else if (type == 5){
			//txt = '-- Chọn loại bao bì --';
			txt = catalog_product_chon_bao_bi;
		}
		$.ajax({
			url: '/catalog/product/get-category?status=' + type,
			method: 'GET',
			success: function(rs) {
				if (rs.error != undefined && !rs.error) {
					var lstCategory = rs.rows;
					
					var html = '<option value="-2" selected="selected">' + Utils.XSSEncode(txt) + '</option>';
					if (hasAll != undefined && hasAll==1) {
						html = '<option value="-2" selected="selected">' + Utils.XSSEncode(jsp_common_status_all) + '</option>';
					}
					for(var i=0; i < lstCategory.length; i++){
						html += '<option value="' + lstCategory[i].id +'">' +
							Utils.XSSEncode(lstCategory[i].productInfoCode) + ' - ' +
							Utils.XSSEncode(lstCategory[i].productInfoName) + '</option>';  
					}
					$('#' + objectId).html(html);
					$('#' + objectId).find("option[value=" + val +']').attr('selected','selected');
					$('#' + objectId).change();
					/**vuongmq, 16/01/2015, danh sach nganh hang con**/
					if(type == 1){
						//objectId = 'categoryChild';
						type = 2; // nganh hang conbrandSelectBox
						$('#category').attr('onclick','ProductCatalog.changeCategory(this, null,' +type+',"categoryChild",' +hasAll+')');
					}
				}
			}
		});
	},
	/**
	 * vuongmq, 16/01/2014
	 * view danh sach nganh hang con
	 */
	changeCategory: function(idHtml, val, type, objectId, hasAll, valView){
		var txt ='';
		if (type == 2) { // category child
			//txt = '-- Chọn ngành hàng con --';
			txt = catalog_product_chon_nganh_hang_con;
		}
		if(val == undefined || val == null){
			val = $(idHtml).val().trim();
		}
		$.ajax({
			url: '/catalog/product/get-category-child?status=' + type+'&productInfoId=' +val,
			method: 'GET',
			success: function(rs) {
				if (rs.error != undefined && !rs.error) {
					var lstCategoryChild = rs.rows;
					
					var html = '<option value="-2" selected="selected">' + Utils.XSSEncode(txt) + '</option>';
					if (hasAll != undefined && hasAll==1) {
						html = '<option value="-2" selected="selected">' +jsp_common_status_all+'</option>';
					}
					for(var i=0; i < lstCategoryChild.length; i++){
						html += '<option value="' + lstCategoryChild[i].id +'">' +
							Utils.XSSEncode(lstCategoryChild[i].productInfoCode) + ' - ' +
							Utils.XSSEncode(lstCategoryChild[i].productInfoName) + '</option>';  
					}
					$('#' + objectId).html(html);
					if(valView != undefined && valView != null){
						$('#' + objectId).find("option[value=" + valView +']').attr('selected','selected');
					}
					$('#' + objectId).change();
				}
			}
		});
	},
	/**
	 * LacNV
	 * view product information
	 */
	viewProduct: function(productId) {
		hrefPost('/catalog/product/viewdetail?productId=' + productId, {checkPermission:1});
	}
	,
	
	back: function(){
		sessionStorage.backButton = true;
		location.href = '/catalog/product';
	},
	/**
	 * download file template import san pham
	 * @author trietptm
	 * @since 18/06/2015
	 */
	downloadImportPriceTemplateFile: function() {
		var url = "/catalog/product/download-import-excel-file";
		ReportUtils.exportReport(url, {}, 'errMsg');
		return false;
	},
	importExcel: function() {
		$('.sucExcelMsg').html('').hide();
		$('.errExcelMsg').html('').hide();
		Utils.importExcelUtils(function(data) {
			ProductCatalog.search();
			$('#fakefilepc').val("").change();
			if (data.message != undefined && data.message != null && data.message.trim().length > 0) {
				$('#errExcelMsg').html(data.message.trim()).change().show();
			} else {
				$('#sucExcelMsg').html(jsp_common_save_success).show();
				var tm = setTimeout(function() {
					$('#sucExcelMsg').html("").hide();
					clearTimeout(tm);
				}, 1500);
			}
		}, 'importFrm', 'excelFile', 'errExcelMsg');
		
	},
};
var ReportManager = {
	_idUrlGoc: null,
	_rowIndexGoc: null,
	_idDefaultGoc: null,
	_flagEditName: true,
	_shopType: null,
	_isNPP: null,
	refreshGridEdit: function () {
		$('#divReportDetail').css('visibility', 'hidden');
		ReportManager._flagEditName = true;
	},
	changeRadio: function (id) {
		//if($('#isdefault').is(':checked')){
		if (id == undefined || id == null || id == '' || id ==""){
			$('#checkDefault').val(0);
			$('#reportIdDefault').val(ReportManager._idDefaultGoc);
		} else {
			if(ReportManager._idDefaultGoc == id) { // bang -1: thi khong can upDate lai isDefault
				$('#checkDefault').val(-1);
			} else {
				$('#checkDefault').val(id);
				$('#reportIdDefault').val(ReportManager._idDefaultGoc);
			}
		}
	},
	search:function(){
		
		var shopId = '';
		if($('#shopId').val() != undefined && $('#shopId').val() != null && $('#shopId').val() != ""){
			shopId = $('#shop').combobox('getValue').trim();
			var t = $('#shop').combotree('tree');
			var n = t.tree('getSelected');
			$('#divShopName').html(n.text);
		} else {
			shopId = $('#curShopId').val();
		}
		var reportCode =$('#reportCode').val().trim();
		var reportName = $('#reportName').val().trim();
		ReportManager._isNPP = $('#isNPP').val();
		$('#dg').datagrid('load',{
			shopId: shopId,
			reportCode: reportCode,
			reportName: reportName
		});
		ReportManager.refreshGridEdit();
		return false;
	},
	uploadFileReport:function(){	
		$('#easyuiPopupUpload').css("visibility", "visible");
		var html = $('#easyuiPopupUpload').html();
		ReportManager.refreshPopupUpload();
		$('#easyuiPopupUpload').dialog({  
	        closed: false,  
	        cache: false,  
	        modal: true,
	        width:550,
	        height:300,
	        onOpen: function(){
	        	$('#reportCodeDlg').focus();
	        	$("#easyuiPopupUpload #checkDefault").attr("checked", "checked");
	        },onClose:function(){
	        	$('#easyuiPopupUpload').html(html);
	        	$('#easyuiPopupUpload').css("visibility", "hidden");
	        	$('#reportCodeDlg').val('').show();
	        	$('#reportNameDlg').val('').show();
	        	$('#fakefilepcVNM').val('').show();
	        }
		});
	},
	refreshPopupUpload : function(){
		$('#reportCodeDlg').val('').show();
		$('#reportNameDlg').val('').show();
		$('#fakefilepcVNM').val('').show();
		$('#errPdfMsg').hide();
		$('#resultPdfMsg').hide();
//		$('#fakePdffilepc').attr('disabled','disabled');
		$('#reportNameDlg').focus();
	},
	//upload file bao cao vnm.report-manager.js, load ca file .jasper, .jrxml: function uploadJasperFile(fileObj,formId,inputText){
	importReportFile: function(){
		var msg = '';
		var err = '';
		//Check is Null
		if(msg.length == 0){
			msg = Utils.getMessageOfRequireCheck('reportCodeDlg','Mã BC');
			err = '#reportCodeDlg';
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfRequireCheck('reportNameDlg','Tên BC');
			err = '#reportNameDlg';
		}
		var str = $('#fakefilepcVNM').val().trim();
		if(msg.length == 0){
			if(str.length > 0) {
				var n = str.substring(str.length-5,str.length).toUpperCase();
				if(n!=='JRXML'){
					msg = 'Không đúng loại định dạng Report file .jrxml';
				}
			}  else {
				msg = 'Không đúng loại định dạng Report file .jrxml';
			}
		}
		//Check validate
		if(msg.length == 0){
			msg = Utils.getMessageOfSpecialCharactersValidateEx1('#reportCodeDlg','Mã BC',Utils._CODE);
			err = '#reportCodeDlg';
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfSpecialCharactersValidateEx1('#reportNameDlg','Tên BC',Utils._NAME);
			err = '#reportNameDlg';
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfSpecialCharactersValidateEx1('#fakefilepcVNM','Tập tin Report file .jrxml',Utils._NAME);
			err = '#fakefilepcVNM';
		}
		if(msg.length>0){
			$('#errPdfMsg').html(msg).show();
			$(err).focus();
			setTimeout(function(){$('#errPdfMsg').hide();},4000);
			return false;
		}
		if(str != null && str != "") {
			if(!uploadJasperFile(document.getElementById("reportFile"), null, 'fakefilepcVNM')){
				return false;
			}
		} else {
			msg = 'Không đúng loại định dạng Report file .jrxml';
		}
		var options = {
				beforeSubmit : ReportManager.beforeUploadFile,
				success : ReportManager.afterUploadFile,
				type : "POST",
				dataType : 'html'
			};
			$('#easyuiPopupUpload #importFrm').ajaxForm(options);
			$.messager.confirm('Xác nhận', 'Bạn có muốn lưu thông tin này ?',	function(r) {
				if (r) {
					try{
						$('#importFrm').submit();}
					catch(e) {
					}
				}
			});
 		return false;
	},
	importReportFileDetail: function(){
		var msg = '';
		var err = '';
		var str = $('#fakefilepc').val().trim();
		if(msg.length == 0){
			if(str.length > 0) {
				var n = str.substring(str.length-5,str.length).toUpperCase();
				if(n!=='JRXML'){
					msg = 'Không đúng loại định dạng Report file .jrxml';
				}
			} else {
				if($('#checkDefault').val() == -1) {
					msg = 'Không có gì thay đổi' ;
				} else if($('#checkDefault').val() == 0) {
					msg = 'Vui lòng chọn tập tin file .jrxml làm mặc định' ;
				}
			}
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfSpecialCharactersValidateEx1('#fakefilepc','Tập tin Report file .jrxml',Utils._NAME);
			err = '#fakefilepc';
		}
		if(msg.length>0){
			$('#errMsg').html(msg).show();
			$(err).focus();
			setTimeout(function(){$('#errMsg').hide();},4000);
			return false;
		}
		if($('#shopId').val() != undefined && $('#shopId').val() != null && $('#shopId').val() != ""){
			var shopId = $('#shop').combobox('getValue').trim();
			$('#shopIdReport').val(shopId);
			/*var t = $('#shop').combotree('tree');
			var n = t.tree('getSelected');
			$('#divShopName').html(n.text);*/
		} else {
			var shopId = $('#curShopId').val();
			$('#shopIdReport').val(shopId);
		}
		if(str != null && str != "") {
			if(!uploadJasperFile(document.getElementById("reportFileDetail"), null, 'fakefilepc')){
				return false;
			}
		}
		var options = {
				beforeSubmit : ReportManager.beforeUploadFile,
				success : ReportManager.afterUploadFileDetail,
				type : "POST",
				dataType : 'html'
			};
			$('#importFrmReport').ajaxForm(options);
			$.messager.confirm('Xác nhận', 'Bạn có muốn lưu thông tin này ?',	function(r) {
				if (r) {
					try{
						$('#importFrmReport').submit();}
					catch(e) {
					}
				}
			});
 		return false;
	},
	beforeUploadFile:function(){
//		if(!uploadPdfFile(document.getElementById("pdfFile"))){
//			return false;
//		}
		try{
			
			$('#errPdfMsg').html('').hide();
			$('#resultPdfMsg').html('').hide();	
			$('#errMsg').html('').hide();
			$('#successMsg').html('').hide();
			$('#divOverlay').show();
		}catch(e){}
	},
	afterUploadFile: function(responseText, statusText, xhr, $form){
		$('#divOverlay').hide();
		if (statusText == 'success') {
			$("#responseDiv").html(responseText);
			updateTokenImport();
			var errMsg = $('#sverrMsg').val().trim();
			if(errMsg!=undefined && errMsg.length>0){
				$('#errPdfMsg').html(errMsg).show();
				setTimeout(function(){$('#errPdfMsg').hide();},4000);
				return false;
			}
			else{
				$('#resultPdfMsg').html('Lưu dữ liệu thành công').show();
				$('#dg').datagrid('reload');
				setTimeout(function(){
					$('#resultPdfMsg').html('').hide();
					ReportManager.refreshGridEdit();
					$('#easyuiPopupUpload').dialog('close');
				},4000);				
			}
			
		}
	},
	afterUploadFileDetail: function(responseText, statusText, xhr, $form){
		$('#divOverlay').hide();
		if (statusText == 'success') {
			$("#responseDiv").html(responseText);
			updateTokenImport();
			var errMsg = $('#sverrMsg').val().trim();
			var typeReportMsg = $('#typeReportMsg').val().trim();
			if(errMsg!=undefined && errMsg.length>0){
				$('#errMsg').html(errMsg).show();
				setTimeout(function(){$('#errMsg').hide();},4000);
				return false;
			}
			else{
				$('#successMsg').html('Lưu dữ liệu thành công').show();
				//$('#dgReportDetail').datagrid('reload');
				if(typeReportMsg == 0) {
					ReportManager.editReportShopMapVNM(ReportManager._idUrlGoc, ReportManager._rowIndexGoc);
				} else {
					ReportManager.editReportShopMap(ReportManager._idUrlGoc, ReportManager._rowIndexGoc);
				}
				setTimeout(function(){
					$('#successMsg').html('').hide();
				},4000);				
			}
			
		}
	},
	closeWindow:function(){
		$('#easyuiPopupUpload').dialog('close');
	},
	editReportShopMapVNM:function(id, rowIndex){
 		$('#divReportDetail').css('visibility', 'visible');
 		//$('#divReportDetail').css('width', '1245');
 		var code = $('#dg').datagrid('getRows')[rowIndex].reportCode;
 		var name = $('#dg').datagrid('getRows')[rowIndex].reportName;
 		$('#divReportDetail .Title2Style').html('Danh sách file Gốc báo cáo (' + '<span class="RequireStyle" style="text-transform:none">' + Utils.XSSEncode(code) + ' - ' + Utils.XSSEncode(name) + '</span>)');
 		var shopId = '';
		if($('#shopId').val() != undefined && $('#shopId').val() != null && $('#shopId').val() != ""){
			shopId = $('#shop').combobox('getValue').trim();
		} else {
			shopId = $('#curShopId').val();
		}
		var typeFile = 0; // VNM
		$('#typeReport').val(typeFile);
 		loadReportDetailVNM(id, shopId, typeFile);
 		ReportManager._rowIndexGoc = rowIndex;
 		ReportManager._idUrlGoc = id;
 		$('#reportId').val(id);  // id cua Report can chinh sua
 		$('#checkDefault').val(-1); // default chinh no khong can update
	},
	editReportShopMapNPP:function(id, rowIndex){
 		$('#divReportDetail').css('visibility', 'visible');
 		var code = $('#dg').datagrid('getRows')[rowIndex].reportCode;
 		var name = $('#dg').datagrid('getRows')[rowIndex].reportName;
 		//$('#divReportDetail .RequireStyle').html(code + ' - ' +name);
 		$('#divReportDetail .Title2Style').html('Danh sách file Gốc báo cáo (' + '<span class="RequireStyle" style="text-transform:none">' + Utils.XSSEncode(code) + ' - ' + Utils.XSSEncode(name) + '</span>)');
 		var shopId = '';
		if($('#shopId').val() != undefined && $('#shopId').val() != null && $('#shopId').val() != ""){
			shopId = $('#shop').combobox('getValue').trim();
		} else {
			shopId = $('#curShopId').val();
		}
 		var typeFile = 0; // VNM
		$('#typeReport').val(typeFile);
 		loadReportDetailNPP(id, shopId, typeFile);
	},
	editReportShopMap:function(id, rowIndex){
 		$('#divReportDetail').css('visibility', 'visible');
 		var code = $('#dg').datagrid('getRows')[rowIndex].reportCode;
 		var name = $('#dg').datagrid('getRows')[rowIndex].reportName;
 		$('#divReportDetail .Title2Style').html('Danh sách file NPP báo cáo (' + '<span class="RequireStyle" style="text-transform:none">' + Utils.XSSEncode(code) + ' - ' + Utils.XSSEncode(name) + '</span>)');
 		var shopId = '';
		if($('#shopId').val() != undefined && $('#shopId').val() != null && $('#shopId').val() != ""){
			shopId = $('#shop').combobox('getValue').trim();
		} else {
			shopId = $('#curShopId').val();
		}
		var typeFile = 1; // NPP
		$('#typeReport').val(typeFile);
 		loadReportDetail(id, shopId, typeFile);
 		ReportManager._rowIndexGoc = rowIndex;
 		ReportManager._idUrlGoc = id;
 		$('#reportId').val(id);  // id cua Report can chinh sua
 		$('#checkDefault').val(-1); // default chinh no khong can update
	},
	editReportShopMapName: function(id, rowIndex) {
		//edit name
		var row = $('#dg').datagrid('getRows')[rowIndex];
		if(row.editName){
			$.messager.confirm('Xác nhận', 'Bạn có muốn lưu tên báo cáo không?', function(r){
				if(r){
					ReportManager._flagEditName = true;
					$('#dg').datagrid('acceptChanges');
					row.editName = false;
					$('#dg').datagrid('rejectChanges');
					$('#dg').datagrid('refreshRow',rowIndex);
					
					$.ajax({
						 type: "POST",
						 url: "/catalog/report-manager/updateNameReport",
						 data: { reportId: id, reportName: row.reportName},
						 dataType: "json",
						 success: function(data){
							 if(data.error == false){
								 $('#successMsgEdit').html('Lưu thành công!').show();
								 setTimeout(function(){ $('#successMsgEdit').html('').hide();},5000);
							 }else{
								 $('#errMsgEdit').html(data.errMsg).show();
								 setTimeout(function(){ $('#errMsgEdit').html('').hide();},5000);
							 }
						 }
					});
				}
			});
			
		}else{
			if(ReportManager._flagEditName == true) {
				row.editName = true;
				$('#dg').datagrid('refreshRow',rowIndex);
				$('#dg').datagrid('beginEdit', rowIndex);
				ReportManager._flagEditName = false;
			}
		}
	},
	backEditReportShopMapName: function(id, rowIndex){
		ReportManager._flagEditName = true;
		$('#dg').datagrid('getRows')[rowIndex].editName = false;
		$('#dg').datagrid('rejectChanges');
		$('#dg').datagrid('refreshRow',rowIndex);
	},
	deleteReport: function(id) {
		//save name
		$.messager.confirm('Xác nhận', 'Nếu xóa báo cáo thì sẽ xóa toàn bộ các mẫu báo cáo của tất cả đơn vị. <br> Bạn có muốn xóa không?', function(r){
			if(r){
			}
		});
	},
	deleteReportUrl: function(id, index) {
		if(id != undefined && id != null && id != ""){
			//save name
			$.messager.confirm('Xác nhận', 'Bạn có muốn xóa file báo cáo!', function(r){
			});
		} else {
			$.messager.alert('Thông báo', 'File chưa được tải lên. <br> Bạn không thể xóa!', 'info');
		}
		
	}
	///////////////********** END VUONGMQ *********/////////////////////
};
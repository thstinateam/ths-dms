var BankCatalogManager = {
	search: function(){
		var msg ="";
		if(msg.length == 0){
			var shopId = $('#shopTree').combobox('getValue');
			if(shopId <= 0){
				$('#errorMsg').html('Bạn chưa nhập giá trị trường Mã đơn vị.').show();
				$('#grid').datagrid('loadData', new Array());
				return false;
			}
		}
		var params = new Object();			
		params.shopCode = $('#shopCode').val().trim();
		params.bankCode = $('#bankCode').val().trim();
		params.bankName = $('#bankName').val().trim();
		params.bankPhone = $('#bankPhone').val().trim();
		params.bankAddress = $('#bankAddress').val().trim();
		params.status = $('#status').val().trim();
		$('#grid').datagrid('load', params);
	},
	/** vuongmq; 23- September, 2014  opendialog them moi ngan hang */
	openDialogBankChangeAdd: function(){
		$(".ErrorMsgStyle").html('').hide();
		//$('#gridSearchBillContGrid').html('<table id="gridSearchBill"></table>').change();
		$('#dialogBankChange').dialog({
			title: "Thêm mới ngân hàng",
			closed: false,
			cache: false,
			modal: true,
	        width: 910,
	        height: 'auto',
	        onOpen: function(){
	        	$('.easyui-dialog .InputTextStyle').val('');
	    		$('.easyui-dialog #errorSearchMsg').hide();
	    		
	    		/**shopCodeDl don vị*/
	    		$('#shopCodeDl').addClass('BoxSelect');
	    		$('#shopCodeDl').html('<input type="text" id="shopTreeDialog" class="InputTextStyle InputText5Style" />');
	    		$('#shopCodeDl').append('<input type="hidden" id="shopCodeDialog"/>');
	    		/*TreeUtils.loadComboTreeShopHasTitle('shopTreeDialog', 'shopCodeDialog',function(data) {
	    		}, true);*/
	    		var shopCodeLoad = $('#shopCodeLogin').val().trim();
	    		if(shopCodeLoad != undefined && shopCodeLoad != null){
	    			$('#shopCodeDialog').val(shopCodeLoad);
	    		}
	    		TreeUtils.loadComboTreeShop('shopTreeDialog', 'shopCodeDialog', $('#shopCodeLogin').val().trim(), function(data) {
	    			$('.combo-panel').css('width','205');
	    			$('.combo-p').css('width','205');
	    		}, null, true, false);
	    		//ReportUtils.loadComboTreeAuthorizeEx('shopTreeDialog','shopCodeDialog',$('#shopCodeLogin').val().trim());
	    		/*ReportUtils.loadComboTreeAuthorizeEx('shopTreeDialog','shopCodeDialog', $('#shopCodeLogin').val().trim());*/
	    		$('.BoxSelect .combo').css('width','170');
	    		
	    		$('#bankCodeDl').removeAttr('disabled');
	    		/**combobox status*/
	    		$('#dialogBankChange .MySelectBoxClass').css('width', '173');
	    		$('#dialogBankChange .CustomStyleSelectBox').css('width', '140');
	    		$('#statusDl').val(1).change();
	    		$('#dialogBankChange #btnSaveBank').html('Thêm mới');
	    		
	    		$('#bankCodeDl').focus();
	    		
	    		$('#dialogBankChange #btnSaveBank').unbind('click');
	    		$('#dialogBankChange #btnSaveBank').bind('click',function(event) {
	    			BankCatalogManager.addBank();
				});
	        },
	        onClose:function() {
	        	
	        }
		});
	},
	/** vuongmq; 23- September, 2014 click buttom them moi ngan hang */
	addBank: function(){
		$(".ErrorMsgStyle").html('').hide();
		var msg ="";
		if(msg.length == 0){
			var shopId = $('#shopTreeDialog').combobox('getValue');
			if(shopId <= 0){
				msg = 'Bạn chưa nhập giá trị trường Mã đơn vị.';
			}
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfRequireCheck('bankCodeDl', 'Mã ngân hàng');
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfSpecialCharactersValidate('bankCodeDl', 'Mã ngân hàng', Utils._CODE);
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfInvaildInteger('bankPhoneDl', 'Số điện thoại');
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfMaxlengthValidate('bankPhoneDl', 'Số điện thoại', 11);
		}
		if(msg.length >0){
			$('#errorMsgDl').html(msg).show();
			return false;
		}
		var params = new Object();			
		params.shopCode = $('#shopCodeDialog').val().trim();
		params.bankCode = $('#bankCodeDl').val().trim();
		params.bankName = $('#bankNameDl').val().trim();
		params.bankAccount = $('#bankAccountDl').val().trim();
		params.bankPhone = $('#bankPhoneDl').val().trim();
		params.bankAddress = $('#bankAddressDl').val().trim();
		params.status = $('#statusDl').val().trim();
		var msg = 'Bạn có muốn thêm mới ngân hàng?';
		Utils.addOrSaveData(params, '/catalog/bank/save', null,  'errorMsgDl', function(data) {
			$('#successMsgDl').html("Lưu dữ liệu thành công").show();
			var tm = setTimeout(function(){
				//Load lai danh sach vai tro
				//$('#btnSearchPermission').click();
				$('#dialogBankChange').dialog('close');
				//$('#btnSearch').click();
				$('#grid').datagrid('reload');
				$('.SuccessMsgStyle').html("").hide();
				clearTimeout(tm);
			 }, 1500);
		}, null, null, null,msg);
		return false;
	},
	/** vuongmq; 23- September, 2014 opendialog cap nhat ngan hang */
	openDialogBankChangeUpdate: function(index, id){
		$(".ErrorMsgStyle").html('').hide();
		var data = $('#grid').datagrid('getRows')[index];
		$('#dialogBankChange').dialog({
			title: "Cập nhật ngân hàng "+ '<span style="color:#199700">' + Utils.XSSEncode(data.bankCode) + '</span>',
			closed: false,
			cache: false,
			modal: true,
	        width: 910,
	        height: 'auto',
	        onOpen: function(){
	        	$('.easyui-dialog .InputTextStyle').val('');
	    		$('.easyui-dialog #errorSearchMsg').hide();
	    		/**shopCodeDl don vị*/
	    		$('#shopCodeDl').removeClass('BoxSelect');
	    		$('#shopCodeDl').html('<input id="shopCodeDialog" type="text" class="InputTextStyle InputText5Style" value='+ data.shopCode + '></input>');
	    		
	    		/**combobox status*/
	    		$('#dialogBankChange .MySelectBoxClass').css('width', '173');
	    		$('#dialogBankChange .CustomStyleSelectBox').css('width', '140');
	    		$('#dialogBankChange #btnSaveBank').html('Cập nhật');
	    		
	    		/** set cac gia tri vao dialog*/
	    		$('#shopCodeDialog').attr('disabled', "disabled");
	    		$('#bankCodeDl').attr('disabled', "disabled");
	    		
	    		$('#bankCodeDl').val(Utils.XSSEncode(data.bankCode));
	    		$('#bankNameDl').val(Utils.XSSEncode(data.bankName));
	    		$('#bankPhoneDl').val(Utils.XSSEncode(data.bankPhone));
	    		$('#bankAddressDl').val(Utils.XSSEncode(data.bankAddress));
	    		$('#bankAccountDl').val(Utils.XSSEncode(data.bankAccount));
	    		$('#statusDl').val(Utils.XSSEncode(data.status)).change();
	    		
	    		$('#bankNameDl').focus();
	    		
	    		$('#dialogBankChange #btnSaveBank').unbind('click');
	    		$('#dialogBankChange #btnSaveBank').bind('click',function(event) {
	    			BankCatalogManager.updateBank(index, id);
				});
	        },
	        onClose:function() {
	        	/*$("input[id$='Dl']").val("");
	        	var txtIsJoin = $('#txtIsJoinOderNumber').val().trim();
	        	if(txtIsJoin.length==0){
	        		$('#txtIsJoinOderNumber').val(AdjustedRevenueManage._txtIsJoinOderNumberC);	        		
	        	}*/
	        }
		});
	},
	/** vuongmq; 23- September, 2014 click buttom cap nhat ngan hang */
	updateBank: function(index, idBank){
		$(".ErrorMsgStyle").html('').hide();
		var params = new Object();			
		params.shopCode = $('#shopCodeDialog').val().trim();
		params.bankCode = $('#bankCodeDl').val().trim();
		params.bankName = $('#bankNameDl').val().trim();
		params.bankAccount = $('#bankAccountDl').val().trim();
		params.bankPhone = $('#bankPhoneDl').val().trim();
		params.bankAddress = $('#bankAddressDl').val().trim();
		params.status = $('#statusDl').val().trim();
		params.bankId = idBank;
		var data = $('#grid').datagrid('getRows')[index];
		/** kiem tra khi cap nhat khong co gi thay doi*/
		if(data.bankName== null){
			data.bankName = '';
		}
		if(data.bankAccount== null){
			data.bankAccount = '';
		}
		if(data.bankPhone== null){
			data.bankPhone = '';
		}
		if(data.bankAddress== null){
			data.bankAddress = '';
		}
		var msg = '';
		if (data.bankName == params.bankName && data.bankAccount == params.bankAccount && data.bankPhone == params.bankPhone 
		 && data.bankAddress == params.bankAddress && data.status == params.status ) {
			/*$('#errorMsgDl').html('Không có gì thay đổi cập nhật.').show();
			return false;*/
			msg = 'Không có gì thay đổi cập nhật.';
		}
		
		if(msg.length == 0){
			msg = Utils.getMessageOfInvaildInteger('bankPhoneDl', 'Số điện thoại');
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfMaxlengthValidate('bankPhoneDl', 'Số điện thoại', 11);
		}
		if(msg.length >0){
			$('#errorMsgDl').html(msg).show();
			return false;
		}
		var msgDialog = 'Bạn có muốn cập nhật ngân hàng?';
		Utils.addOrSaveData(params, '/catalog/bank/save', null,  'errorMsgDl', function(data) {
			$('#successMsgDl').html("Lưu dữ liệu thành công").show();
			var tm = setTimeout(function(){
				//Load lai danh sach vai tro
				//$('#btnSearchPermission').click();
				$('#dialogBankChange').dialog('close');
				//$('#btnSearch').click();
				$('#grid').datagrid('reload');
				$('.SuccessMsgStyle').html("").hide();
				clearTimeout(tm);
			 }, 1500);
		}, null, null, null,msgDialog);
		return false;
	}
}

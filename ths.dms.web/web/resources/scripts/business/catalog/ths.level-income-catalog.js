var LevelIncomeCatalog ={
	_xhrSave : null,
	_xhrDel: null,
	_listCategory:null,
	getGridUrl: function(categoryType,levelCode,levelName,fromIncome,toIncome,status){
		return "/catalog/level-income/search?lstcategory=" + categoryType 
		+ "&levelCode=" + encodeChar(levelCode) 
		+ "&levelName=" + encodeChar(levelName) 
		+ "&fromAmountFilter=" + fromIncome
		+ "&toAmountFilter=" + toIncome
		+ "&status=" + status;
	},
	search: function(){
		$('#errMsg').html('').hide();

		var levelCode = $('#saleLevelCode').val().trim();
		var levelName = $('#saleLevelName').val().trim();
		var status = $('#status').val();
		var tm = setTimeout(function() {
			$('#categoryName').focus();
		}, 500);
		var arrayCategory = new Object();
		arrayCategory.listCategory = LevelIncomeCatalog._listCategory;
		var msg = '';
		var toIncome = Utils.returnMoneyValue($('#toIncome').val().trim());
		var fromIncome = Utils.returnMoneyValue($('#fromIncome').val().trim());
		if(fromIncome.length>0 && isNaN(fromIncome)){
			msg = "Từ DS không phải là giá trị số.";
			$('#fromIncome').focus();
		}
		if(toIncome.length>0 && isNaN(toIncome)){
			msg = "Đến DS không phải là số.";
			$('#toIncome').focus();
		}
		if(msg.length > 0){
			$('#errMsg').html(msg).show();
			return false;
		}
		var url = LevelIncomeCatalog.getGridUrl(arrayCategory.listCategory,levelCode,levelName,fromIncome,toIncome,status);
		$("#grid").datagrid({url:url,pageNumber:1});
		return false;
	},
	getSelectedLevelIncome: function(proInfoId,categoryTypeCode,categoryTypeId,levelCode,levelName,fromIncome,toIncome,status){
		$('#errMsg').html('').hide();
		//$('#selObjType').val(staffObjectType);
		if (proInfoId != null && proInfoId != 0 && proInfoId != undefined) {
			$('#levelIncomeId').val(proInfoId);
		} else {
			$('#levelIncomeId').val(0);
		}
		$('#saleLevelCode1').attr('disabled', true);
		setTextboxValue('saleLevelCode1', levelCode);
		setTextboxValue('saleLevelName1', levelName);
		setTextboxValue('fromIncome1', formatCurrency(fromIncome));
		setTextboxValue('toIncome1', formatCurrency(toIncome));
		setSelectBoxValue('status1', status);
		setSelectBoxValue('categoryType1', categoryTypeId);
		$('#categoryTypeId').val(categoryTypeId);
		$('#popup1').dialog({  
		    title: catalog_sales_brand_update_seasonal_by_industry
		});
		disableSelectbox('categoryType1');
		$('#divDialog').css('visibility','visible');
		$('#popup1').dialog('open');
	},
	change: function(){
		var msg = '';
		$('#errMsg').html('').hide();
		msg = Utils.getMessageOfRequireCheck('categoryType1',catalog_sales_brand_NH_Code,true);
		if(msg.length == 0){
			msg = Utils.getMessageOfRequireCheck('saleLevelCode1',catalog_sales_brand_standard_code);
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfSpecialCharactersValidate('saleLevelCode1',catalog_sales_brand_standard_code,Utils._CODE);
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfRequireCheck('saleLevelName1',catalog_sales_brand_standard_name);
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfSpecialCharactersValidate('saleLevelName1',catalog_sales_brand_standard_name,Utils._NAME);
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfRequireCheck('status1',unit_tree_search_unit_shop_status,true);
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfRequireCheck('fromIncome1',catalog_sales_brand_from_amount);
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfNegativeNumberCheck('fromIncome1',catalog_sales_brand_from_amount);
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfInvaildNumber('fromIncome1',catalog_sales_brand_from_amount);
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfNegativeNumberCheck('toIncome1',catalog_sales_brand_to_amount);
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfInvaildNumber('toIncome1',catalog_sales_brand_to_amount);
		}
		if(msg.length == 0){
			if($('#fromIncome1').val().trim() != '' && $('#toIncome1').val().trim() != ''){
				var fromIncome = Utils.returnMoneyValue($('#fromIncome1').val().trim());
				var toIncome = Utils.returnMoneyValue($('#toIncome1').val().trim());
				if(parseInt(fromIncome) > parseInt(toIncome)){
					msg = catalog_sales_brand_from_amount_to_amount;
				}
			}
		}
		if(msg.length > 0){
			$('#errMsg').html(msg).show();
			return false;
		}
		var dataModel = new Object();
		dataModel.levelIncomeId = $('#levelIncomeId').val();
		dataModel.categoryType = $('#categoryType1').val();
		dataModel.levelCode = $('#saleLevelCode1').val().trim();
		dataModel.levelName = $('#saleLevelName1').val().trim();
		dataModel.status = $('#status1').val();
		dataModel.fromIncome = Utils.returnMoneyValue($('#fromIncome1').val().trim());
		dataModel.toIncome = Utils.returnMoneyValue($('#toIncome1').val().trim());
		Utils.addOrSaveData(dataModel, "/catalog/level-income/save", LevelIncomeCatalog._xhrSave, null,function(data){
			if(data.error == false){
				$('#popup1').dialog('close');
				LevelIncomeCatalog.search();
			}else{
				$('#popup1').dialog('close');
				LevelIncomeCatalog.reset();
			}
			
		});
		return false;
	},
	addLevelIncome:function(){
		LevelIncomeCatalog.reset();
		$('#divDialog').css('visibility','visible');
		$('#popup1').dialog({  
		    title: catalog_customer_add_level_income
		});
		$('#popup1').dialog('open');
	},
	reset:function(){
		$('#levelIncomeId').val(0);
		$('#categoryTypeId').val(0);
		setSelectBoxValue('status1', 1);
		setSelectBoxValue('categoryType1', 0);
		$('#saleLevelCode1').attr('disabled', false);
		$('#saleLevelCode1').val('');
		$('#saleLevelName1').val('');
		$('#fromIncome1').val('');
		$('#toIncome1').val('');
		$('#divDialog').css('visibility','hidden');
		enableSelectbox('categoryType1');
		$('#errMsg').html('').hide();
	},
	deleteRow: function(levelIncomeId){
		var dataModel = new Object();
		dataModel.levelIncomeId = levelIncomeId;	
		Utils.deleteSelectRowOnGrid(dataModel, catalog_sales_brand_target, "/catalog/level-income/remove", LevelIncomeCatalog._xhrDel, null, null);		
		return false;		
	},
	getCategoryName:function(categoryType,obj){
		$.getJSON('/rest/catalog/category/name/' + categoryType + ".json",function(data){
			$(obj).val(categoryType);
		});
	},
	getListLevel:function(categoryType,obj){
		$.getJSON('/rest/catalog/category/' + categoryType + '/level/list'+".json",function(data){
			var arrHtml = new Array();
			arrHtml.push('<option value="-1">--- '+ Utils.XSSEncode(catalog_sales_brand_chose_standard) +'---</option>');
			for(var i=0;i<data.length;i++){
				arrHtml.push('<option value="'+ data[i].name +'">'+ Utils.XSSEncode(data[i].name) +'</option>');
			}
			$(obj).html(arrHtml.join(""));
			$(obj).change();
		});
	},
	ViewFileUpload: function(typeView){
		if ($('#excelFile').val() == '') {
			$('#errExcelMsg').html(msgCommon8).show();
			return false;
		}
		if (typeView == 1)
		{
			var options = { 
			 		beforeSubmit: ProductLevelCatalog.beforeImportExcelA,   
			 		success:      ProductLevelCatalog.afterImportExcelA,  
			 		type: "POST",
			 		dataType: 'html',   
			 		data:({typeView:typeView})
			 	}; 
			$('#importFrm').ajaxForm(options);
			$('#importFrm').submit();
		}
		else
		{
			var options = { 
					beforeSubmit: ProductLevelCatalog.beforeImportExcel,   
			 		success:      ProductLevelCatalog.afterImportExcel,
			 		type: "POST",
			 		dataType: 'html',
			 		data:({typeView:typeView})
			 	}; 
			$('#importFrm').ajaxForm(options);
			$.messager.confirm(jsp_common_xacnhan, msgCommon3, function(r){
				if (r){
					$('#importFrm').submit();
				}
			});
		}
	},
	
	exportExcel: function(){
		var categoryType = $('#categoryType').val().trim();
		var categoryName =  $('#categoryName').val().trim();		
		var status = $('#status').val().trim();		
		var level = $('#level').val().trim();
		
		var data = new Object();
		data.categoryType = categoryType;
		data.categoryName = categoryName;
		data.status = status;
		data.level = level;
		var url = "/catalog/level-income/export-excel";
		ReportUtils.exportReport(url,data);
	}
};
var SubCategoryCatalog = {
	_xhrSave : null,
	_xhrDel: null,
	getGridUrl: function(code,name,description,status){
		return "/catalog/sub-category/search?code=" + encodeChar(code) + "&name=" + encodeChar(name) + "&note=" + encodeChar(description) + "&status=" + status;
	},	
	search: function(){
		$('#errMsg').html('').hide();
		var code = $('#subcategoryCode').val().trim();
		var name = $('#subcategoryName').val().trim();
		var description = $('#description').val().trim();
		var status = 1;
		var tm = setTimeout(function() {
			$('#subcategoryCode').focus();
		}, 500);
		var url = SubCategoryCatalog.getGridUrl(code,name,description,status);
		$("#grid").datagrid({url:url,page:1});
		return false;
	},
	save: function(){
		var msg = '';
		$('#errMsg').html('').hide();
		msg = Utils.getMessageOfRequireCheck('subcategoryCodePop','Mã Sub Cat');
		if(msg.length == 0){
			msg = Utils.getMessageOfSpecialCharactersValidate('subcategoryCodePop','Mã Sub Cat',Utils._CODE);
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfRequireCheck('subcategoryNamePop','Tên Sub Cat');
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfSpecialCharactersValidate('subcategoryNamePop','Tên Sub Cat');
		}

		if(msg.length == 0 && $('#subdescriptionPop').val().trim().length > 0){
			msg = Utils.getMessageOfSpecialCharactersValidate('subdescriptionPop','Mô tả');
		}

		if(msg.length > 0){
			$('#errMsg').html(msg).show();
			return false;
		}
		var dataModel = new Object();
		dataModel.id = $('#selId').val().trim();
		dataModel.code = $('#subcategoryCodePop').val().trim();
		dataModel.name = $('#subcategoryNamePop').val().trim();
		dataModel.note = $('#subdescriptionPop').val().trim();
		dataModel.status = 1;
		Utils.addOrSaveRowOnGrid(dataModel, "/catalog/sub-category/save", SubCategoryCatalog._xhrSave, null, null, function(data){
			if(!data.error){
				$('#popup1').dialog('close');
				SubCategoryCatalog.search();
			}
		});		
		return false;
	},
	getSelectedSubCategory: function(rowId, code, name, description, status){
		if(rowId != null && rowId != 0 && rowId !=undefined){
			$('#selId').val(rowId);
		} else {
			$('#selId').val(0);
		}
		if(description == 'null'){
			description= '';
		}
		$('#subcategoryCodePop').val(code);
		$('#subcategoryNamePop').val(name);
		$('#subdescriptionPop').val(description);
        $('#popup1').dialog('open');
		return false;
	},
	resetForm: function(){
		SubCategoryCatalog.clearData();
		$("#grid").trigger("reloadGrid");
		return false;
	},
	deleteSubCategory: function(code){
		var dataModel = new Object();
		dataModel.code = code;	
		Utils.deleteSelectRowOnGrid(dataModel, 'sub category', '/catalog/sub-category/delete', SubCategoryCatalog._xhrDel, null, null,function(data){
			SubCategoryCatalog.clearData();
		});
		return false;		
	},
	clearData: function(){
		setTitleSearch();
		$('.RequireStyle').hide();
		$('#selId').val(0);
		$('#errMsg').html('').hide();
		$('#code').removeAttr('disabled');
		$('#btnEdit').hide();
		$('#btnCancel').hide();
		$('#btnSearch').show();
		$('#btnAdd').show();
		$('#code').val('');
		$('#name').val('');
		$('#description').val('');
		focusFirstTextbox();
		setSelectBoxValue('status', 1);
		SubCategoryCatalog.search();
	},
	getChangedForm: function(){
		$('.RequireStyle').show();
		$('#btnSearch').hide();
		$('#btnAdd').hide();
		$('#btnEdit').show();
		$('#btnCancel').show();	
		$('#code').focus();
		focusFirstTextbox();
		setTitleAdd();
	}
};
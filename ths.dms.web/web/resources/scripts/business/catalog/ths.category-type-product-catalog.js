var CategoryTypeProductCatalog ={
	_xhrSave : null,
	_xhrDel: null,
	_curRowId: null,
	resultext:null,
	searchFilter:null,
	indexedit:0,
	getGridUrl: function(shopCode,shopName,categoryType,productCode,status){
		return "/product-distributer/search?shopCode=" + encodeChar(shopCode) + "&shopName=" + encodeChar(shopName) + "&categoryType=" + encodeChar(categoryType) +"&productCode=" + encodeChar(productCode) + "&status=" + status;
	},
	
	fillTxtShopCodeByF9 : function(code){
		$('#shopCode').val(Utils.XSSEncode(code)).show();
		$('#common-dialog-search-2-textbox').dialog('close');
		$('#shopCode').focus();
	},
	
	fillTxtProductCodeByF9 : function(code){
		$('#productCode').val(Utils.XSSEncode(code)).show();
		$('#common-dialog-search-2-textbox').dialog('close');
		$('#productCode').focus();
	},
	
	search: function(){
		$('#errMsg').html('').hide();
		var shopCode = $('#shopCode').val().trim();
		var shopName= '';
		var productCode = $('#productCode').val().trim();
		var categoryType = -1;
		var status = $('#status').val();
		var tm = setTimeout(function() {
			$('#shopCode').focus();
		}, 500);
		
		CategoryTypeProductCatalog.searchFilter = new Object();
		CategoryTypeProductCatalog.searchFilter.shopCodeF = shopCode;
		CategoryTypeProductCatalog.searchFilter.productCodeF = productCode;
		CategoryTypeProductCatalog.searchFilter.statusF = status;
		
		var url = CategoryTypeProductCatalog.getGridUrl(shopCode,shopName,categoryType,productCode,status);
		$("#grid").datagrid({url:url,pageNumber:1});
		return false;
	},
	exportExcel: function() {
		var shopCode = $('#shopCode').val().trim();
		var shopName= '';
		var productCode = $('#productCode').val().trim();
		var categoryType = -1;
		var status = $('#status').val();
		var params = new Object();
		params.shopCode = shopCode;
		params.shopName = shopName;
		params.productCode = productCode;
		params.categoryType = categoryType;
		params.status = status;		
		var url = "/product-distributer/export";
//		CommonSearch.exportExcelData(params,url,'errExcelMsg');
		ReportUtils.exportReport(url, params, 'errExcelMsg');
	},
	change: function(){
		var msg = '';
		$('.ErrorMsgStyle').html('').hide();
		var shopObjectType = $('#shopObjectType').val();
		if(shopObjectType!=undefined && shopObjectType!=null  && parseInt(shopObjectType)!= ShopObjectType.VNM &&  parseInt(shopObjectType)!= ShopObjectType.GT){
			CategoryTypeProductCatalog.search();
			msg = "Không được quyền thực hiện chức năng";
			$('#errMsg').html(msg).show();
			return false;
		}
		
		if(msg.length == 0){
			msg = Utils.getMessageOfSpecialCharactersValidate('stockMin','Tồn kho Min');
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfInvaildNumber('stockMin', 'Tồn kho Min');
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfNegativeNumberCheck('stockMin','Tồn kho Min');
		}
		if(msg.length == 0 && $('#stockMin').val().replace(/,/g,'')>9999){
			msg='Tồn kho min không được vượt quá 9999';
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfSpecialCharactersValidate('stockMax','Tồn kho Max');
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfInvaildNumber('stockMax', 'Tồn kho Max');
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfNegativeNumberCheck('stockMax','Tồn kho Max');
		}

		if(msg.length == 0){
			msg = Utils.getMessageOfSpecialCharactersValidate('dateGo','Ngày đi đường');
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfInvaildNumber('dateGo', 'Ngày đi đường');
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfNegativeNumberCheck('dateGo','Ngày đi đường');
		}

		if(msg.length == 0){
			msg = Utils.getMessageOfRequireCheck('dateSale','Ngày bán hàng');
		}
		if(msg.length == 0){
			if(CategoryTypeCatalog.parseCharToValue($('#dateSale').val().trim()) == '0000000'){
				msg = 'Bạn chưa nhập giá trị cho trường Ngày bán hàng. Yêu cầu nhập giá trị';
			}
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfFloatValidate('growth','Tỉ lệ tăng trưởng');
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfSpecialCharactersValidate('status','Trạng thái',Utils._CODE);
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfRequireCheck('status','Trạng thái');
		}

		if(msg.length > 0){
			$('#errMsg').html(msg).show();
			return false;
		}
		var dataModel = new Object();
		dataModel.productId = $('#productId').val();
		dataModel.shopCode = $('#shopCodeGrid').val().trim();		
		dataModel.productCode = $('#productCodeGrid').val().trim();
		dataModel.stockMin = $('#stockMin').val().trim();
		dataModel.stockMax = $('#stockMax').val().trim();
		dataModel.dateGo = $('#dateGo').val().trim();
		dataModel.dateSale = $('#dateSale').val().trim();
		dataModel.growth = $('#growth').val().trim();
		if(parseFloat(dataModel.growth) >9999){
			$('#errMsg').html('Tỉ lệ tăng trưởng phải nhỏ hơn 10,000.00').show();
			return false;
		}
		dataModel.status = $('#status1').val();
		Utils.addOrSaveData(dataModel, "/product-distributer/save", CategoryTypeProductCatalog._xhrSave, null,function(data){
			if(data.error == false){
				CategoryTypeProductCatalog.reset();
				if(CategoryTypeProductCatalog.searchFilter != null){
					setTextboxValue('shopCode',CategoryTypeProductCatalog.searchFilter.shopCodeF);
					setTextboxValue('productCode',CategoryTypeProductCatalog.searchFilter.productCodeF);
					setTextboxValue('status',CategoryTypeProductCatalog.searchFilter.statusF);										
				}				
				CategoryTypeProductCatalog.search();
				return false;
			}
		});
		$('#grid').datagrid('beginEdit', CategoryTypeProductCatalog.indexedit);
		return false;
	},
	deleteRow: function(productId){
		$('#errMsg').html('').hide();
		var dataModel = new Object();
		dataModel.productId = productId;	
		Utils.deleteSelectRowOnGrid(dataModel, 'sản phẩm bán cho đơn vị', "/product-distributer/remove", CategoryTypeProductCatalog._xhrDel, null, null);		
		return false;		
	},
	getCategoryOfProduct: function(status,categoryId,categoryCode){
		$.getJSON('/rest/catalog/category-product/list.json?status=' + status+'&categoryId='+categoryId, function(data){
			var arrHtml = new Array();
			arrHtml.push('<option value="-1">--- Chọn mã ngành hàng ---</option>');
			for(var i=0;i<data.length;i++){
				arrHtml.push('<option value="'+ Utils.XSSEncode(data[i].value) +'">'+ Utils.XSSEncode(data[i].name) +'</option>');
			}
			$('#category').html(arrHtml.join(""));
			$('#category').change();
			setSelectBoxValue('category', categoryId);
		});
	},
	importExcel:function(){
		// tao options
		$('#importFrm').submit();
		
	},
	beforeImportExcel: function(){
		if(!previewImportExcelFile(document.getElementById("excelFile"))){
			return false;
		}
		//showLoading();
		$('.ErrorMsgStyle').html('').hide();
		$('.SuccessMsgStyle').html('').hide();
		showLoadingIcon();
		return true;
	},
	afterImportExcel: function(responseText, statusText, xhr, $form){//ProductLevelCatalog
		hideLoadingIcon();
		CategoryTypeProductCatalog.search();
		
		//Show hien thi thong bao
		if (statusText == 'success') {				
	    	$("#responseDiv").html(responseText);		 
	    	var newToken = $('#responseDiv #newToken').val();
	    	console.log(newToken);
	    	if(newToken != null && newToken != undefined && newToken != '') {
	    		$('#token').val(newToken);	    		
	    		$('#tokenImport').val(newToken);
	    	}
	    	if($('#errorExcel').html().trim() == 'true' || $('#errorExcelMsg').html().trim().length > 0){
	    		$('#errExcelMsg').html($('#errorExcelMsg').html()).show();
	    	} else {
	    		var totalRow = parseInt($('#totalRow').html().trim());
    			var numFail = parseInt($('#numFail').html().trim());
    			var fileNameFail = $('#fileNameFail').html();
    			var mes = format(msgErr_result_import_excel,(totalRow - numFail),numFail);		    		
    			if(numFail > 0){
    				mes+= ' <a href="'+ fileNameFail +'">Xem chi tiết lỗi</a>';
    			}
    			if($('#excelFile').length!=0 && $('#fakefilepc').length!=0){
    				try{$('#excelFile').val('');$('#fakefilepc').val('');}catch(err){}
    			}
    			$('#errExcelMsg').html(mes).show();
	    	}
	    }
	},
	
	afterImportExcelNganh: function(responseText, statusText, xhr, $form){//ProductLevelCatalog
		hideLoadingIcon();
		//Reset form Web
		CategoryTypeCatalog.search();
		//Show hien thi thong bao
		if (statusText == 'success') {				
	    	$("#responseDiv").html(responseText);		 
	    	var newToken = $('#responseDiv #newToken').val();
	    	console.log(newToken);
	    	if(newToken != null && newToken != undefined && newToken != '') {
	    		$('#token').val(newToken);	    		
	    		$('#tokenImport').val(newToken);
	    	}
	    	if($('#errorExcel').html().trim() == 'true' || $('#errorExcelMsg').html().trim().length > 0){
	    		$('#errExcelMsg').html($('#errorExcelMsg').html()).show();
	    	} else {
	    		var totalRow = parseInt($('#totalRow').html().trim());
    			var numFail = parseInt($('#numFail').html().trim());
    			var fileNameFail = $('#fileNameFail').html();
    			var mes = format(msgErr_result_import_excel,(totalRow - numFail),numFail);		    		
    			if(numFail > 0){
    				mes+= ' <a href="'+ fileNameFail +'">Xem chi tiết lỗi</a>';
    			}
    			if($('#excelFile').length!=0 && $('#fakefilepc').length!=0){
    				try{$('#excelFile').val('');$('#fakefilepc').val('');}catch(err){}
    			}
    			$('#errExcelMsg').html(mes).show();
	    	}
	    }
	},
	
	viewExcel:function(){
		$('#isView').val(1);
		$('#importFrm').submit();
		return false;
	},
	editrow:function(target){
		var indexRow = CategoryTypeProductCatalog.getRowIndex(target);
	    $('#grid').datagrid('beginEdit', indexRow);
	    var width = 80;
	    $('td[field=status] .datagrid-editable-input').combobox('resize', width);
	    $('td[field=minsf] .datagrid-editable-input').attr('maxlength','10')
	    $('td[field=maxsf] .datagrid-editable-input').attr('maxlength','10');
	    $('td[field=lead] .datagrid-editable-input').attr('maxlength','10')
	    $('td[field=percentage] .datagrid-editable-input').attr('maxlength','10');
	    $('#minsfOld').val($('td[field=minsf] .datagrid-editable-input').val());    
	    $('td[field=calendarD] .datagrid-editable-input').focus(function(){
      	  CategoryTypeProductCatalog.onClickCell(indexRow,$(this).val());      	
      	});
	},
	updateActions:function(index){  
	    $('#grid').datagrid('updateRow',{  
	        index: index,  
	        row:{}  
	    });  
	},
	reset:function(){
		$('#shopCode').val('');
	    $('#productCode').val('');
	},
	getRowIndex:function(target){  
	    var tr = $(target).closest('tr.datagrid-row');  
	    return parseInt(tr.attr('datagrid-row-index'));  
	},  
	saverow:function(target){
		CategoryTypeProductCatalog.reset();    
		var indexRow = CategoryTypeProductCatalog.getRowIndex(target);
		var msg = '';
		$('#errMsg').html('').hide();
		var minsf = $('td[field=minsf] .datagrid-editable-input').val();			
		if(minsf == ""){
			msg = Utils.getMessageOfRequireCheck('stockMin','Tồn kho Min');	
			$('#errMsg').html(msg).show();			
			$('td[field=minsf] .datagrid-editable-input').focus();
			return false;
		}	
		if(minsf.replace(/,/g,'')>9999){
			msg='Tồn kho min không được vượt quá 9999';
			$('#errMsg').html(msg).show();			
			$('td[field=minsf] .datagrid-editable-input').focus();
			return false;
		}
		var maxsf = $('td[field=maxsf] .datagrid-editable-input').val();		
		if(maxsf == ""){
			msg = Utils.getMessageOfRequireCheck('stockMax','Tồn kho Max');	
			$('#errMsg').html(msg).show();			
			$('td[field=maxsf] .datagrid-editable-input').focus();
			return false;
		}
		if(maxsf.replace(/,/g,'')>9999){
			msg='Tồn kho max không được vượt quá 9999';
			$('#errMsg').html(msg).show();			
			$('td[field=maxsf] .datagrid-editable-input').focus();
			return false;
		}
		var lead = $('td[field=lead] .datagrid-editable-input').val();
		if(lead == ""){
			msg = Utils.getMessageOfRequireCheck('dateGo','Ngày đi đường');	
			$('#errMsg').html(msg).show();			
			$('td[field=lead] .datagrid-editable-input').focus();
			return false;
		}
		if(lead.replace(/,/g,'') > 999){
			msg='Ngày đi đường không được vượt quá 999';
			$('#errMsg').html(msg).show();			
			$('td[field=lead] .datagrid-editable-input').focus();
			return false;
		}
		
		var percentage = $('td[field=percentage] .datagrid-editable-input').val();
		if(percentage == ""){
			msg = Utils.getMessageOfRequireCheck('growth','Tỉ lệ tăng trưởng');
			$('#errMsg').html(msg).show();			
			$('td[field=percentage] .datagrid-editable-input').focus();
			return false;
		}
		var minsfOld = $('#minsfOld').val();
		if(parseInt(minsf.trim()) >= parseInt(maxsf.trim()) && parseInt(minsf) != parseInt(minsfOld.trim())){
			msg = "Tồn kho Min phải bé hơn tồn kho Max";
			$('#errMsg').html(msg).show();
			$('td[field=minsf] .datagrid-editable-input').focus();
			return false;
		}

		if(parseInt(minsf.trim()) >= parseInt(maxsf.trim())){
			msg = "Tồn kho Min phải bé hơn tồn kho Max";
			$('#errMsg').html(msg).show();
			$('td[field=maxsf] .datagrid-editable-input').focus();
			return false;
		}
		
		var leadKT = lead.replace(/,/g,'');
		if( parseInt(leadKT.trim())> parseInt(maxsf.trim())){
			msg='Ngày đi đường không được vượt quá tồn kho Max';
			$('#errMsg').html(msg).show();			
			$('td[field=lead] .datagrid-editable-input').focus();
			return false;
		}
		
	    $('#grid').datagrid('endEdit', indexRow);
	    CategoryTypeProductCatalog.indexedit = indexRow;
	    CategoryTypeProductCatalog.change(target);
	    
	},
	cancelrow:function (target){  
		var indexRow = CategoryTypeProductCatalog.getRowIndex(target);
	    $('#grid').datagrid('cancelEdit', indexRow);  
	},
	onClickCell:function(index,value /*index, field, value*/){		
				  if(value != ''){
						$('#T2,#T3,#T4,#T5,#T6,#T7,#CN').removeAttr('checked');
						for(var i = 0; i < value.length; i++) {
							if(value.charAt(i) == 2) {
								$('#T2').attr('checked', 'checked');
							} else if(value.charAt(i) == 3) {
								$('#T3').attr('checked', 'checked');
							} else if(value.charAt(i) == 4) {
								$('#T4').attr('checked', 'checked');
							} else if(value.charAt(i) == 5) {
								$('#T5').attr('checked', 'checked');
							} else if(value.charAt(i) == 6) {
								$('#T6').attr('checked', 'checked');
							} else if(value.charAt(i) == 7) {
								$('#T7').attr('checked', 'checked');
							} else if(value.charAt(i) == 1){
								$('#CN').attr('checked', 'checked');
							}
						}
					}
				$('#weeks').css('visibility','visible');
				$('#popupDate').dialog({ 			
			        title: 'Ngày làm việc',  
			        width: 400,  
			        height: 130,  
			        closed: false,  
			        cache: false,         
			        modal: true,
			        buttons:[
			                 {text: 'Đồng ý',
			                	 handler:function(){
			                		 var num =0;
			                			var value = "0000000";
			                			if($('#T2').is(':checked')){
			                				num++;
			                				value+=($('#T2').val());
			                			}
			                			if($('#T3').is(':checked')){
			                				num++;
			                				value+=($('#T3').val());
			                			}
			                			if($('#T4').is(':checked')){
			                				num++;
			                				value+=($('#T4').val());
			                			}
			                			if($('#T5').is(':checked')){
			                				num++;
			                				value+=($('#T5').val());
			                			}
			                			if($('#T6').is(':checked')){
			                				num++;
			                				value+=($('#T6').val());
			                			}
			                			if($('#T7').is(':checked')){
			                				num++;
			                				value+=($('#T7').val());
			                			}
			                			if($('#CN').is(':checked')){
			                				num++;
			                				value+=($('#CN').val());
			                			}
			                			value = value.substring(num);
			                			if(value == "0000000"){
			                				$('#errMsg1').html('Vui lòng chọn ngày làm việc').show();
			                				var tm = setTimeout(function(){
			                					$('#errMsg1').html('').hide();
			                					clearTimeout(tm);
			                					}, 3000);
			                				return false;
			                			}
			                			$('#dateSale').val(value);
//			                			$('#dateSale1').val(CategoryTypeProductCatalog.parseValueToChar(value));
			                			$('#dateSale1').val(value);
			                			$('td[field=calendarD] .datagrid-editable-input').val($('#dateSale1').val());
			                			$('#popupDate').dialog("close");
			                	 }
			                 },
			                 {text: 'Hủy bỏ',
			                	 handler:function(){
			                			$('#popupDate').dialog('close');
			                			
			                		}
			                 }
			                ],
			                onOpen:function(){
			                	$('td[field=calendarD] .datagrid-editable-input').blur();
		    					var tabindex = -1;
		    					$('.InputTextStyle, select, input, button , li , tr, td, th, label, ul, a, img').each(function () {
		    						if (this.type != 'hidden') {
		    							$(this).attr("tabindex", tabindex);
		    							tabindex -=1;
		    						}
		    					});
		    					var tabindex = 1;
		    		    		$('#popupDate input,#popupDate select,#popupDate button').each(function () {
		    			    		if (this.type != 'hidden') {
		    				    	    $(this).attr("tabindex", tabindex);
		    							tabindex++;
		    			    		}
		    					});
		    				},
		    				onClose:function(){
		    					var tabindex = 1;
		    					$('.InputTextStyle, select, input, button , li , tr, td, th, label, ul, a, img').each(function () {
		    						if (this.type != 'hidden') {
		    							$(this).attr("tabindex", tabindex);
		    							tabindex +=1;
		    						}
		    					});
		    				}
			    });

		return true;
	  },
	  parseValueToChar:function(value){
			var char = "";
			if(value != null || value != undefined){
				var i = value.length;
				for(var j=0;j<i;j++){
					if(value.charAt(j) == 2){
						char += "T2";
					}else if(value.charAt(j) == 3){
						char += "T3";
					}else if(value.charAt(j) == 4){
						char += "T4";
					}else if(value.charAt(j) == 5){
						char += "T5";
					}else if(value.charAt(j) == 6){
						char += "T6";
					}else if(value.charAt(j) == 7){
						char += "T7";
					}else if(value.charAt(j) == 1){
						char += "CN";
					}
					if(char != ""){
						char += " ";
					}
				}
				return char;
			}else{
				return char;
			}
		},
		parseCharToValue:function(char){
			var value = "0000000";
			if(char != null || char != undefined){
				var i = char.trim().split(" ").length;
				var ch = char.trim().split(" ");
				for(var j=0;j<i;j++){
					if(ch[j] == "T2"){
						value += "2";
					}else if(ch[j] == "T3"){
						value += "3";
					}else if(ch[j] == "T4"){
						value += "4";
					}else if(ch[j] == "T5"){
						value += "5";
					}else if(ch[j] == "T6"){
						value += "6";
					}else if(ch[j] == "T7"){
						value += "7";
					}else if(ch[j] == "CN"){
						value += "1";
					}
				}
				return value.substring(i);
			}else{
				return value;
			}
		}		
};
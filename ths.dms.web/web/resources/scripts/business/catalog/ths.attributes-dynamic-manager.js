/**
 * Quan ly thuoc tinh dong
 * @author liemtpt
 * @since 21/01/2015
 */
var AttributesDynamicManager = {
	_xhrDel:null,
	HTML : '',
	APPLY_OBJECT : {
		CUSTOMER:1,
		PRODUCT : 2,
		STAFF : 3
	},
	ATTRIBUTE_TYPE : {
		CHARACTER: 1,
	    NUMBER: 2,
	    DATE_TIME: 3,
		CHOICE: 4,
		MULTI_CHOICE: 5,
		LOCATION: 6
	},
	ACTIVE_TYPE : {
		STOP: 0,
	    RUNNING: 1
	},
	PRODUCT_ATTRIBUTE_ENUM_TYPE : {
		NO: 0,
	    YES: 1
	},
	_applyObjectPop: new Map(),
	/**
	 * load grid
	 * @author liemtpt
	 * @description: load danh sach thuoc tinh dong
	 * @createDate 22/01/2015
	 */
	loadGrid : function() {
		var attributeCode = $('#attributeCode').val().trim();
		var attributeName = $('#attributeName').val().trim();
		var applyObject = $('#applyObject').val().trim();
		var valueType = $('#valueType').val().trim();
		var status = $('#status').val().trim();
		var params = new Object();
		params.attributeCode = attributeCode;
		params.attributeName = attributeName;
		params.valueType = valueType;
		params.status = status;
		params.applyObject = applyObject;
		$("#attributeGrid").datagrid({
		  url:'/attributes/dynamic/search',
		  pageList  : [10,20,30],
		  height:'auto',
		  scrollbarSize : 0,
		  pagination:true,
		  fitColumns:true,
		  singleSelect  :true,
		  method : 'GET',
		  rownumbers: true,	  
		  width:  $(window).width() - 50,
		  queryParams:params,
		  columns:[[		
		    {field:'attributeCode', title: qltt_ma_thuoc_tinh, width: 110, sortable: true,resizable:true , align: 'left',
		    	formatter: function(val, row) {
		    		if (val != undefined && val != null) {
		    			return Utils.XSSEncode(val);
		    		}
		    		return '';
		    	}
		    },
		    {field:'attributeName', title: qltt_ten_thuoc_tinh, width: 180, sortable:true,resizable:true , align: 'left',
		    	formatter: function(val, row) {
		    		if (val != undefined && val != null) {
		    			return Utils.XSSEncode(val);
		    		}
		    		return '';
		    	}
		    },
		    {field:'applyObject', title: qltt_doi_tuong_ap_dung, width: 140, sortable:true,resizable:true , align: 'left',
		    	formatter: function(val, row) {
		    		if ($('#applyObject').val() != undefined && $('#applyObject').val() != null) {
		    			if($('#applyObject').val() == AttributesDynamicManager.APPLY_OBJECT.CUSTOMER){
		    				return qltt_doi_tuong_khachhang;
		    			}else if($('#applyObject').val() == AttributesDynamicManager.APPLY_OBJECT.PRODUCT){
		    				return qltt_doi_tuong_sanpham;
		    			}else if($('#applyObject').val() == AttributesDynamicManager.APPLY_OBJECT.STAFF){
		    				return qltt_doi_tuong_nhanvien;
		    			}
		    		}
		    		return '';
		    	}
		    },
		    {field:'mandatory', title: qltt_bat_buoc, width: 50, sortable:true,resizable:true , align: 'center',
		    	formatter: function(val, row) {
		    		if (val != undefined && val != null) {
		    			if(val == AttributesDynamicManager.PRODUCT_ATTRIBUTE_ENUM_TYPE.YES){
		    				return "<a id='"+row.attributeId+"' ><img src='/resources/images/icon-tick.png'/></a>";
						}else{
						}
		    		}
		    		return '';
		    	}
		    },
		    {field:'type', title: qltt_loai_gia_tri, width: 70, sortable:true,resizable:true , align: 'left',
		    	formatter: function(val, row) {
		    		if (val != undefined && val != null) {
		    			if(val == AttributesDynamicManager.ATTRIBUTE_TYPE.CHARACTER){
							return 'Chữ';
						}else if(val == AttributesDynamicManager.ATTRIBUTE_TYPE.NUMBER){
							return 'Số';
						}else if(val == AttributesDynamicManager.ATTRIBUTE_TYPE.CHOICE){
							return 'Chọn một';
						}else if(val == AttributesDynamicManager.ATTRIBUTE_TYPE.MULTI_CHOICE){
							return 'Chọn nhiều';
						}else if(val == AttributesDynamicManager.ATTRIBUTE_TYPE.DATE_TIME){
							return 'Ngày tháng';
						}else if(val == AttributesDynamicManager.ATTRIBUTE_TYPE.LOCATION){
							return 'Vị trí bản đồ';
						}
		    		}
		    		return '';
		    	}
		    },
		    {field:'displayOrder', title: qltt_thu_tu_hien_thi, width: 40, sortable:true,resizable:true , align: 'center',
		    	formatter: function(val, row) {
		    		if (val != undefined && val != null) {
		    			return Utils.XSSEncode(val);
		    		}
		    		return '';
		    	}
		    },
		    {field:'status', title: qltt_trang_thai, width: 80, sortable:true,resizable:true , align: 'left',
		    	formatter: function(val, row) {
		    		if (val != undefined && val != null) {
		    			if(val == AttributesDynamicManager.ACTIVE_TYPE.RUNNING){
		    				return hoat_dong;
		    			}else{
		    				return tam_ngung;
		    			}
		    		}
		    		return '';
		    	}
		    },
		    {field:'view', title: '', width: 40, align: 'center', sortable: false, resizable: true, formatter: function(val, row, index) {
		    		if (row.type == AttributesDynamicManager.ATTRIBUTE_TYPE.CHOICE || row.type == AttributesDynamicManager.ATTRIBUTE_TYPE.MULTI_CHOICE) {
		    			return '<a id="viewAttrDynamicMng" href="javascript:void(0);" onclick="AttributesDynamicManager.loadDetail('+row.attributeId+',\''+row.attributeCode+'\','+$('#applyObject').val()+','+row.status+')"><img src="/resources/images/icon-view.png"/></a>';
		    		}
		    		return '<img style="visibility: hidden " src="/resources/images/icon-view-space.png"/>';
		    	}
		    },
		    {field:'edit', title: '<a id="btnAddAttribute" title="'+Utils.XSSEncode(msgText2)+'"  href="javascript:void(0)" onclick= "AttributesDynamicManager.openAttributeDynamic();"><img src="/resources/images/icon_add.png"/></a>',width: 40, align: 'center',sortable:false,resizable:true,
		    	formatter: function(val, row, index) {
		    		return '<a href="javascript:void(0)" class="btnEditAttribute" onclick="AttributesDynamicManager.openAttributeDynamic('+index+');"><img src="/resources/images/icon-edit.png"/></a>';
		    	}
		    },
		    {field:'P', hidden : true}
		  ]],
		  onLoadSuccess: function() {
				$('.datagrid-header-rownumber').html(jsp_common_numerical_order);
				var lstHeaderStyle = ["attributeCode", "attributeName", "type", "displayOrder", "status"];
				Utils.updateCellHeaderStyleSort('gridContainer', lstHeaderStyle);
				var arrTmpViewLength =  $('#gridContainer td a[id^="viewAttrDynamicMng"]').length;
				if (arrTmpViewLength == 0) {
					$('#attributeGrid').datagrid("hideColumn", "view");
				}
				//Xu ly phan quyen
				var arrEdit =  $('#gridContainer td[field="edit"]');
				if (arrEdit != undefined && arrEdit != null && arrEdit.length > 0) {
				  for (var i = 0, size = arrEdit.length; i < size; i++) {
				  	$(arrEdit[i]).prop("id", "gr_table_td_edit_dstt_" + i);//Khai bao id danh cho phan quyen
					$(arrEdit[i]).addClass("cmsiscontrol");
				  }
				}
				Utils.functionAccessFillControl('gridContainer', function(data){
					//Xu ly cac su kien lien quan den cac control phan quyen
					var arrTmpLength =  $('#gridContainer td[id^="gr_table_td_edit_dstt_"]').length;
					var invisibleLenght = $('.isCMSInvisible[id^="gr_table_td_edit_dstt_"]').length;
					if (arrTmpLength > 0 && arrTmpLength != invisibleLenght) {
						$('#attributeGrid').datagrid("showColumn", "edit");
					} else {
						$('#attributeGrid').datagrid("hideColumn", "edit");
					}
				});
	      }
		});
	},
	/**
	 * Search
	 * @author liemtpt
	 * @description: Tim kiem lich nghi phep cua nhan vien
	 * @createDate 14/01/2015
	 */
	search: function() {
		$('#errMsg').html('').hide();
		$('#attributeDetailDataDiv').hide();
		var msg = '';
		var attributeCode = $('#attributeCode').val().trim();
		var attributeName = $('#attributeName').val().trim();
		var applyObject = $('#applyObject').val().trim();
		var valueType = $('#valueType').val().trim();
		var status = $('#status').val().trim();
		/*if(msg.length == 0){
			msg = Utils.getMessageOfInvalidFormatDate('fromDate','Từ ngày');
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfInvalidFormatDate('toDate','Đến ngày');
		}
		if(msg.length == 0 && !Utils.compareDate(fromDate, toDate)){
			msg = "Từ ngày phải là ngày trước hoặc cùng ngày với Đến ngày";
			$('#fromDate').focus();
		}
		if(msg.length > 0){
			$('#errMsg').html(msg).show();
			return false;
		}*/
		var params = new Object();
		params.attributeCode = attributeCode;
		params.attributeName = attributeName;
		params.valueType = valueType;
		params.status = status;
		params.applyObject = applyObject;
		$("#attributeGrid").datagrid('options').sortName = null; // search thi load lai sort binh thuong
		$('#attributeGrid').datagrid('load',params);
		return false;
	},	
	/**
	 * open attribute dynamic
	 * @author liemtpt
	 * @description: Mo popup thuoc tinh dong
	 * @createDate 22/01/2015
	 */
	openAttributeDynamic : function(index) {
		$('#popupAttribute #successMsg').html('').hide();
//		$('#popupAttributeDetail #successMsg').html('').hide();
//		$('#attributeDetailDataDiv').hide();
		var row = null;
		if(index!=undefined && index!=null && index>=0){
			row = $('#attributeGrid').datagrid('getRows')[index];
			console.log(row);
		}
		$('#popupAttribute').dialog({
			closed : false,
			cache : false,
			modal : true,
			width : 650,
			height : 350,//400,
			onOpen: function(){	
				var applyObject = $('#applyObject').val();
				$('#applyObjectPop').val(applyObject).change();
				if(row!=null){
					$('#popupAttribute #idDivStatusPop').show();
					//Khong cho cap nhat
					disableSelectbox('applyObjectPop','#popupAttribute');
					disableSelectbox('valueTypePop','#popupAttribute');
					disabled('attributeCodePop','#popupAttribute');
					//view du lieu
					$('#popupAttribute #statusPop').val(row.status).change();
					$('#popupAttribute #valueTypePop').val(row.type).change();
					setTextboxValue('attributeCodePop',row.attributeCode);
					setTextboxValue('attributeNamePop',row.attributeName);
					$('#popupAttribute #mandatoryPop').val(row.mandatory).change();
					setTextboxValue('displayOrderPop',row.displayOrder);
					setTextboxValue('descriptionPop',row.description);
					$('#popupAttribute #statusPop').val(row.status).change();
					if(row.type == AttributesDynamicManager.ATTRIBUTE_TYPE.CHARACTER){
						$('#popupAttribute #idDivChoseText').show();
						setTextboxValue('dataLengthPop',row.dataLength);
						$('#popupAttribute #idDivChoseNumber').hide();
					}else if(row.type == AttributesDynamicManager.ATTRIBUTE_TYPE.NUMBER){
						$('#popupAttribute #idDivChoseText').show();
						$('#popupAttribute #idDivChoseNumber').show();
						setTextboxValue('dataLengthPop',row.dataLength);
						setTextboxValue('minValuePop',row.minValue);
						setTextboxValue('maxValuePop',row.maxValue);
					}else{
						$('#popupAttribute #idDivChoseText').hide();
						$('#popupAttribute #idDivChoseNumber').hide();
					}					
					$('#attributeIdHid').val(row.attributeId);
					$('#attributeNamePop').focus();
				}else{
					$('#attributeIdHid').val(0);
					$('#popupAttribute #idDivStatusPop').hide();
					$('#attributeCodePop').focus();
				}
			},
		 	onBeforeClose: function() {

		 	},
	        onClose : function(){
	        	//Khong cho cap nhat
	        	enableSelectbox('applyObjectPop','#popupAttribute');
				enableSelectbox('valueTypePop','#popupAttribute');
				enable('attributeCodePop','#popupAttribute');
				//view du lieu
				$('#popupAttribute #valueTypePop').val(AttributesDynamicManager.ATTRIBUTE_TYPE.CHOICE).change();
				setTextboxValue('attributeCodePop','');
				setTextboxValue('attributeNamePop','');
				$('#popupAttribute #mandatoryPop').val(AttributesDynamicManager.PRODUCT_ATTRIBUTE_ENUM_TYPE.NO).change();
				setTextboxValue('displayOrderPop','');
				setTextboxValue('descriptionPop','');
				$('#popupAttribute #statusPop').val(AttributesDynamicManager.ACTIVE_TYPE.RUNNING).change();
				setTextboxValue('dataLengthPop','');
				setTextboxValue('minValuePop','');
				setTextboxValue('maxValuePop','');
	        	$('#popupAttribute #errMsg').hide();
	        }
		});
	},
	
	/**
	 * save attribute 
	 * @author liemtpt
	 * @description: tao moi va cap nhat thuoc tinh dong
	 * @createDate 22/01/2015
	 */
	saveAttribute : function() {
		var msg = '';
		var valueTypePop = $('#valueTypePop').val().trim();
		$('#popupAttribute #errMsg').html('').hide();
		if (msg.length == 0) {
			msg = Utils.getMessageOfRequireCheck('attributeCodePop', qltt_ma_thuoc_tinh);
		}
		if(msg.length ==0){
			msg = Utils.getMessageOfSpecialCharactersValidate('attributeCodePop', qltt_ma_thuoc_tinh,Utils._CODE);
		}
		if (msg.length == 0){
			msg = Utils.getMessageOfRequireCheck('attributeNamePop', qltt_ten_thuoc_tinh);
		}
		if(msg.length ==0){
			msg = Utils.getMessageOfSpecialCharactersValidate('attributeNamePop', qltt_ten_thuoc_tinh,Utils._ADDRESS);
		}
		
		var attributeId = $('#popupAttribute #attributeIdHid').val().trim();
		var attributeCode = $('#popupAttribute #attributeCodePop').val().trim();
		var attributeName = $('#popupAttribute #attributeNamePop').val().trim();
		var applyObject = $('#popupAttribute #applyObjectPop').val().trim();
		var valueType = $('#popupAttribute #valueTypePop').val().trim();
		var mandatory = $('#popupAttribute #mandatoryPop').val().trim();
		var displayOrder = $('#popupAttribute #displayOrderPop').val().trim();
		var description = $('#popupAttribute #descriptionPop').val().trim();
		var params = new Object();
//		if(attributeId!= null && attributeId > 0){
			var status = $('#popupAttribute #statusPop').val().trim();
			params.status = status;
			if(valueType == AttributesDynamicManager.ATTRIBUTE_TYPE.CHARACTER){
				var dataLength = $('#popupAttribute #dataLengthPop').val();
				if(dataLength<0 || dataLength >2000){
					msg = qltt_do_dai_toi_da_chu;
				}
				params.dataLength = dataLength;
			}else if(valueType == AttributesDynamicManager.ATTRIBUTE_TYPE.NUMBER){
				var dataLength = $('#popupAttribute #dataLengthPop').val();
				if(dataLength < 0 || dataLength >8){
					msg = qltt_do_dai_toi_da_so;
				}
				var minValue = $('#popupAttribute #minValuePop').val();
				var maxValue = $('#popupAttribute #maxValuePop').val();
				if(Number(minValue) > Number(maxValue) && msg.length ==0){
					msg = qltt_js_msg_tu_den;
					$('#popupAttribute #minValuePop').focus();
				}
				params.dataLength = dataLength;
				params.minValue = minValue;
				params.maxValue = maxValue;
			}
//		}	
		if (msg.length > 0) {
			$('#popupAttribute #errMsg').html(msg).show();
			return false;
		}	
		params.applyObject = applyObject;
		params.attributeId = attributeId;
		params.attributeCode = attributeCode;
		params.attributeName = attributeName;
		params.type = valueType;
		params.mandatory = mandatory;
		params.displayOrder = displayOrder;
		params.description = description;
		//Gui yeu cau len server de luu du lieu
		var url = "/attributes/dynamic/save-attribute";
		params[Utils.VAR_NAME] = 'attributeDynamicVO';
		var dataModel = getSimpleObject(params);
		Utils.addOrSaveData(dataModel,url,null, 'errMsg', function(data){
			var tm = setTimeout(function(){
				$('.easyui-dialog').dialog('close'); 
				$("#attributeGrid").datagrid("reload");
				clearTimeout(tm);
			}, 2000);
			
		},null,'#popupAttribute',null,null,function(data){
			if(data.error != undefined  && data.error && data.errMsg != undefined){
				$('#popupAttribute #errMsg').html(data.errMsg).show();
			}
//			var errorMsg = data.errMsg;
//			setTimeout(function(){$('#errMsg').html('').hide();}, 3000);
		});
		return false;
	},
	
	/**
	 * load detail 
	 * @author liemtpt
	 * @description: load danh sach gia tri cua tung thuoc tinh dong
	 * @createDate 22/01/2015
	 */
	loadDetail : function(attributeId,attributeCode,applyObject,status) {
		$('#attributeDetailDataDiv').html(AttributesDynamicManager.HTML);		
		$('#attributeDetailDataDiv').show();		
		$('#idTitleDetail').html(Utils.XSSEncode(qltt_danh_sach_gia_tri_cua_tt + ' ' + attributeCode));
		var params = new Object();
		params.attributeId = attributeId;
		params.applyObject = applyObject;

		var addTile = '';
		if(status == activeType.RUNNING){
			addTile = '<a href= "javascript:void(0);" id="btnGridDetailAdd" onclick= "AttributesDynamicManager.openAttributeDetailDynamic(null,'+applyObject+','+attributeId+');"><img src="/resources/images/icon_add.png" /></a>';
		}
		
		$('#attributeDetailGrid').datagrid({
			url: '/attributes/dynamic/load-attribute-detail',
			autoRowHeight : true,
			rownumbers : true, 
			checkOnSelect :true,
			pagination:true,
			rowNum : 10,
			fitColumns:true,
			pageList  : [10,20,30],
			scrollbarSize:0,
			width: $(window).width() - 350,
			queryParams:params,
		    columns:[[	        
			    {field:'enumCode', title: qltt_ma_gia_tri, width: 100, sortable:true,resizable:true , align: 'left', formatter: function(value, row, index){
			    	return Utils.XSSEncode(value);
			    }},
			    {field:'enumValue', title: qltt_ten_gia_tri, width:150,sortable:true,resizable:true , align: 'left', formatter: function(value, row, index){
			    	return Utils.XSSEncode(value);
			    }},
			    {field:'edit', title:addTile,width: 30, align: 'center',sortable:false,resizable:true,
			    	formatter: function(val, row, index) {
			    		var value= '';
			    		if(status == activeType.RUNNING){
			    			value += '<a href="javascript:void(0)" <a title="'+msgText6+'" class="btnEditAttribute" onclick="AttributesDynamicManager.openAttributeDetailDynamic('+index+','+applyObject+','+attributeId+');"><img src="/resources/images/icon-edit.png"/></a>';
			    		}		    		
			    		value += '   <a  <a title="'+msgText4+'"  href="javascript:void(0)" class="btnEditAttribute" onclick="AttributesDynamicManager.deleteAttributeDetail('+row.enumId+','+applyObject+');"><img src="/resources/images/icon_delete.png"/></a>';
			    		return value;
			    	}
			    },
		    ]],
		    onLoadSuccess :function(data){	    	
		    	$('.datagrid-header-rownumber').html(jsp_common_numerical_order);	    
		    	var lstHeaderStyle = ["enumCode", "enumValue"];
				Utils.updateCellHeaderStyleSort('attributeDetailDataGridSection2', lstHeaderStyle);
				//Xu ly phan quyen
				var arrEdit =  $('#gridContainer td[field="edit"]');
				if (arrEdit != undefined && arrEdit != null && arrEdit.length > 0) {
				  for (var i = 0, size = arrEdit.length; i < size; i++) {
				  	$(arrEdit[i]).prop("id", "gr_table_td_edit_dstt_ct_" + i);//Khai bao id danh cho phan quyen
					$(arrEdit[i]).addClass("cmsiscontrol");
				  }
				}
				Utils.functionAccessFillControl('gridContainer', function(data){
					//Xu ly cac su kien lien quan den cac control phan quyen
					var arrTmpLength =  $('#gridContainer td[id^="gr_table_td_edit_dstt_ct_"]').length;
					var invisibleLenght = $('.isCMSInvisible[id^="gr_table_td_edit_dstt_ct_"]').length;
					if (arrTmpLength > 0 && arrTmpLength != invisibleLenght) {
						$('#attributeDetailGrid').datagrid("showColumn", "edit");
					} else {
						$('#attributeDetailGrid').datagrid("hideColumn", "edit");
					}
				});
		    }
		});
		Utils.bindAutoSearchForEasyUI();
	},
	/**
	 * open attribute detail dynamic
	 * @author liemtpt
	 * @description: mo popup tao gia tri cho thuoc tinh dong
	 * @createDate 22/01/2015
	 */
	openAttributeDetailDynamic : function(index,applyObject,attributeId) {
		$('#popupAttribute #successMsg').html('').hide();
		$('#popupAttributeDetail #successMsg').html('').hide();
		$('#popupAttributeDetail #errMsg').html('').hide();
		var row = null;
		if(index!=undefined && index!=null && index>=0){
			row = $('#attributeDetailGrid').datagrid('getRows')[index];
		}
		$('#popupAttributeDetail').dialog({
			closed : false,
			cache : false,
			modal : true,
			width : 550,
			height : 200,
			onOpen: function(){	
				$('#popupAttributeDetail  #applyObjectHid').val(applyObject);
				$('#popupAttributeDetail #attributeIdHid').val(attributeId);
				if(row!=null){
					disabled('attributeDetailCodePop');
					$('#popupAttributeDetail #enumIdHid').val(row.enumId);
					$('#popupAttributeDetail #attributeIdHid').val(row.attributeId);
//					$('#popupAttributeDetail  #applyObjectHid').val(applyObject);
					setTextboxValue('attributeDetailCodePop',row.enumCode);
					setTextboxValue('attributeDetailNamePop',row.enumValue);
					$('#attributeDetailNamePop').focus();
				}else{
					$('#popupAttributeDetail #enumIdHid').val(0);
					$('#attributeDetailCodePop').focus();
				}
			},
		 	onBeforeClose: function() {
								
	        },
	        onClose : function(){
	        	$('.easyui-dialog #attributeDetailCodePop').val('');
	        	$('.easyui-dialog #attributeDetailNamePop').val('');
	        	enable('attributeDetailCodePop','#popupAttributeDetail');
	        }
		});
	},
	
	/**
	 * save attribute detail 
	 * @author liemtpt
	 * @description: tao moi va cap nhat gia tri cua thuoc tinh dong
	 * @createDate 22/01/2015
	 */
	saveAttributeDetail : function() {
		var msg = '';
		$('#popupAttributeDetail #errMsg').html('').hide();
		if (msg.length == 0) {
			msg = Utils.getMessageOfRequireCheck('attributeDetailCodePop', qltt_ma_gia_tri);
		}
		if(msg.length ==0){
			msg = Utils.getMessageOfSpecialCharactersValidate('attributeDetailCodePop', qltt_ma_gia_tri,Utils._CODE);
		}
		if (msg.length == 0){
			msg = Utils.getMessageOfRequireCheck('attributeDetailNamePop', qltt_ten_gia_tri);
		}
		if(msg.length ==0){
			msg = Utils.getMessageOfSpecialCharactersValidate('attributeDetailNamePop', qltt_ten_gia_tri,Utils._ADDRESS);
		}
		if (msg.length > 0) {
			$('#popupAttributeDetail #errMsg').html(msg).show();
			return false;
		}
		var applyObject =  $('#popupAttributeDetail  #applyObjectHid').val();
		var enumId = $('#popupAttributeDetail #enumIdHid').val().trim();
		var attributeId = $('#popupAttributeDetail #attributeIdHid').val().trim();
		var enumCode = $('#popupAttributeDetail #attributeDetailCodePop').val().trim();
		var enumValue = $('#popupAttributeDetail #attributeDetailNamePop').val().trim();
		var params = new Object();
		params.applyObject = applyObject;
		params.enumId = enumId;
		params.attributeId = attributeId;
		params.enumCode = enumCode;
		params.enumValue = enumValue;
		//Gui yeu cau len server de luu du lieu
		var url = "/attributes/dynamic/save-attribute-detail";
		params[Utils.VAR_NAME] = 'attributeEnumVO';
		var dataModel = getSimpleObject(params);
		Utils.addOrSaveData(dataModel, url,null, 'errMsg', function(data){
			if(data.error != undefined  && data.error && data.errMsg != undefined){
				$('#errMsg').html(data.errMsg).show();
			}else{
				var tm = setTimeout(function(){
					$('.easyui-dialog').dialog('close'); 
					$("#attributeDetailGrid").datagrid("reload");					
					clearTimeout(tm);
				}, 2000);				
			}			
		},null,'#popupAttributeDetail');
		return false;
	},
	/**
	 * delete attribute detail 
	 * @author liemtpt
	 * @description: xoa gia tri thuoc tinh dong
	 * @createDate 22/01/2015
	 */
	deleteAttributeDetail : function(id,applyObject) {
		$('#errMsg').html('').hide();
		var params = new Object();
		params.enumId = id;	
		params.applyObject = applyObject;
		Utils.deleteSelectRowOnGrid(params, qltt_gia_tri, "/attributes/dynamic/remove", AttributesDynamicManager._xhrDel, null, null,function(data){
			if (!data.error){
				$("#attributeDetailGrid").datagrid("reload");			
			}
		},null);		
		return false;		
	}
};
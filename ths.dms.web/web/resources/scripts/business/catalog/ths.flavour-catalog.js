var FlavourCatalog = {
	_xhrSave : null,
	_xhrDel: null,
	getGridUrl: function(code,name,description,status){
		return "/catalog/flavour/search?code=" + encodeChar(code) + "&name=" + encodeChar(name) + "&note=" + encodeChar(description) + "&status=" + status;
	},	
	search: function(){
		$('#errMsg').html('').hide();
		var code = $('#flavourCode').val().trim();
		var name = $('#flavourName').val().trim();
		var description = $('#description').val().trim();
		var status = 1;
		var tm = setTimeout(function() {
			$('#flavourCode').focus();
		}, 500);
		var url = FlavourCatalog.getGridUrl(code,name,description,status);
		$("#grid").datagrid({url:url,page:1});
		return false;
	},
	save: function(){
		var msg = '';
		$('#errMsg').html('').hide();
		msg = Utils.getMessageOfRequireCheck('flavourCodePop','Mã flavour');
		if(msg.length == 0){
			msg = Utils.getMessageOfSpecialCharactersValidate('flavourCodePop','Mã flavour',Utils._CODE);
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfRequireCheck('flavourNamePop','Tên flavour');
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfSpecialCharactersValidate('flavourNamePop','Tên flavour');
		}

		if(msg.length == 0 && $('#description').val().trim().length > 0){
			msg = Utils.getMessageOfSpecialCharactersValidate('description','Mô tả');
		}

		if(msg.length > 0){
			$('#errMsg').html(msg).show();
			return false;
		}
		var dataModel = new Object();
		dataModel.id = $('#selId').val().trim();
		dataModel.code = $('#flavourCodePop').val().trim();
		dataModel.name = $('#flavourNamePop').val().trim();
		dataModel.note = $('#descriptionPop').val().trim();

		dataModel.status = 1;
		Utils.addOrSaveRowOnGrid(dataModel, "/catalog/flavour/save", FlavourCatalog._xhrSave, null, null, function(data){
			if(!data.error){
				$('#popup1').dialog('close');
				FlavourCatalog.search();
			}
		});		
		return false;
	},
	getSelectedRow: function(rowId, code, name, description,status){	
		
		if(rowId != null && rowId != 0 && rowId !=undefined){
			$('#selId').val(rowId);
		} else {
			$('#selId').val(0);
		}
		if(description == 'null'){
			description= '';
		}
		$('#flavourCodePop').val(code);
		$('#flavourNamePop').val(name);
		$('#descriptionPop').val(description);
        $('#popup1').dialog('open');
		
		return false;
	},
	resetForm: function(){
		FlavourCatalog.clearData();
		$("#grid").trigger("reloadGrid");
		return false;
	},
	deleteRow: function(code){
		var dataModel = new Object();
		dataModel.code = code;	
		Utils.deleteSelectRowOnGrid(dataModel, 'brand', '/catalog/flavour/delete', FlavourCatalog._xhrDel, null, null,function(data){
			FlavourCatalog.clearData();
		});
		return false;
	},
	clearData: function(){
		setTitleSearch();
		$('.RequireStyle').hide();
		$('#selId').val(0);
		$('#errMsg').html('').hide();
		$('#code').removeAttr('disabled');
		$('#btnEdit').hide();
		$('#btnCancel').hide();
		$('#btnSearch').show();
		$('#btnAdd').show();		
		$('#code').val('');
		$('#name').val('');
		$('#description').val('');
		focusFirstTextbox();
		setSelectBoxValue('status', 1);
		FlavourCatalog.search();
	},
	getChangedForm: function(){
		$('.RequireStyle').show();
		$('#btnSearch').hide();
		$('#btnAdd').hide();
		$('#btnEdit').show();
		$('#btnCancel').show();	
		$('#code').focus();
		focusFirstTextbox();
		setTitleAdd();
	}
};
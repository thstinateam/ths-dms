var SaleTypeCatalog = {
	_xhrSave : null,
	_xhrDel: null,
	getGridUrl: function(staffTypeCode,parentCode,staffTypeName,status,isGetParent){
		return "/catalog/sale-type/search?staffTypeCode=" + encodeChar(staffTypeCode) + "&parentCode=" + encodeChar(parentCode) + "&staffTypeName=" + encodeChar(staffTypeName) + "&status=" + status+ "&isGetParent=" + encodeChar(isGetParent);
	},	
	search: function(){
		$('#errMsg').html('').hide();
		var staffTypeCode = $('#staffTypeCode').val().trim();
		var parentCode = $('#parentCode').combotree('getValue');
		if(parentCode == '' || parentCode == null){
			parentCode = -1;
		}
		var staffTypeName = $('#staffTypeName').val().trim();
		var status = $('#status').val().trim();
		var tm = setTimeout(function() {
			$('#staffTypeCode').focus();
		}, 500);
		var url = SaleTypeCatalog.getGridUrl(staffTypeCode,parentCode,staffTypeName,status,false);
		$("#grid").setGridParam({url:url,page:1}).trigger("reloadGrid");
		return false;
	},
	change: function(){
		var msg = '';
		$('#errMsg').html('').hide();
		msg = Utils.getMessageOfRequireCheck('staffTypeCode','Mã hình thức bán hàng');
		if(msg.length == 0){
			msg = Utils.getMessageOfSpecialCharactersValidate('staffTypeCode','Mã hình thức bán hàng',Utils._CODE);
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfRequireCheck('staffTypeName','Tên hình thức bán hàng');
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfSpecialCharactersValidate('staffTypeName','Tên hình thức bán hàng');
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfRequireCheck('status','Trạng thái',true);
		}
		if($('#status').val() != 1 && $('#status').val() != 0 && msg.length == 0){
			msg = "Bạn chưa nhập giá trị cho trường Trạng thái. Yêu cầu nhập giá trị";
		}
		if(msg.length > 0){
			$('#errMsg').html(msg).show();
			return false;
		}
		var dataModel = new Object();
		dataModel.id = $('#staffTypeId').val().trim();
		dataModel.staffTypeCode = $('#staffTypeCode').val().trim();
		dataModel.staffTypeName = $('#staffTypeName').val().trim();
		dataModel.parentCode = $('#parentCode').combotree('getValue');
		dataModel.status = $('#status').val().trim();
		Utils.addOrSaveRowOnGrid(dataModel, "/catalog/sale-type/save", SaleTypeCatalog._xhrSave, null, null,function(){
			SaleTypeCatalog.loadTree();
			SaleTypeCatalog.resetForm();
			var url = SaleTypeCatalog.getGridUrl('',-1,'',1,false);
			$("#grid").setGridParam({url:url,page:1}).trigger("reloadGrid");
		});
		return false;
	},
	getSelectedStaffType: function(id,staffTypeCode,parentCode,staffTypeName,status){
		$('#errMsg').html('').hide();
		var url = 'channelTypeId='+ parentCode;
		$.getJSON('/rest/catalog/catalog/staff-type/channeltype.json?'+url, function(data){
			if(data != null && data.status != 'RUNNING'){
				$('#errMsg').html('Bạn phải thay đổi mã cha về trạng thái hoạt động trước khi chỉnh sửa được đối tượng này').show();
			} else {
				$('#parentCode').combotree("reload",'/rest/catalog/sale-type/combotree/1/'+id+'.json');
				if(id!= null && id!= 0 && id!=undefined){
					hideOption('parentCode',id);
					$('#staffTypeId').val(id);
				} else {
					$('#staffTypeId').val(0);
				}
				$('#staffTypeCode').attr('disabled',true);
				setTextboxValue('staffTypeCode',staffTypeCode);
				setTextboxValue('staffTypeName',staffTypeName);
				setSelectBoxValue('status', status);
				$('#parentCode').combotree('setValue',parentCode);
				Utils.getChangedForm();
				$('#btnSearch,#btnCreate').hide();
				$('#btnUpdate,#btnDismiss').show();
				setTitleUpdate();
			}
		});
	},
	resetForm: function(){
		$('#errMsg').html('').hide();
		setTitleSearch();
		$('.RequireStyle').hide();
		$('#btnUpdate,#btnDismiss').hide();
		$('#btnCreate,#btnSearch').show();
		$('#staffTypeCode').val('');
		$('#staffTypeCode').removeAttr('disabled');
		$('#staffTypeCode').focus();
		$('#staffTypeName').val('');
		resetAllOptions('parentCode');
		$('#parentCode').combotree('clear');
		$('#parentCode').combotree("reload",'/rest/catalog/sale-type/combotree/0/0.json');
		$('#parentCode').combotree('setValue', -1);
		setSelectBoxValue('status',1);
		$('#staffTypeId').val(0);
		var url = SaleTypeCatalog.getGridUrl('',-1,'',1,false);
		$("#grid").setGridParam({url:url,page:1}).trigger("reloadGrid");
		return false;
	},
	deleteStaffType: function(staffTypeCode){
		$('#errMsg').html('').hide();
		var dataModel = new Object();
		dataModel.staffTypeCode = staffTypeCode;	
		Utils.deleteSelectRowOnGrid(dataModel, 'kiểu bán hàng', "/catalog/sale-type/remove", SaleTypeCatalog._xhrDel, null, null,function(){SaleTypeCatalog.loadTree();});		
		return false;		
	},
	loadTree: function(){
		loadDataForTree('/rest/catalog/sale-type/tree.json');	
		$('#tree').bind("loaded.jstree", function(event, data){
			if($('#treeContainer').data("jsp")!= undefined){
				$('#treeContainer').data("jsp").destroy();
			}
			$('#treeContainer').jScrollPane();
		});
		$('#tree').bind("select_node.jstree", function (event, data) {
			var name = data.inst.get_text(data.rslt.obj);
			var id = data.rslt.obj.attr("id");			 
			var url = SaleTypeCatalog.getGridUrl('',id,'',1,true);
			$("#grid").setGridParam({url:url,page:1}).trigger("reloadGrid");
	    });
	},
	getListParentCode:function(parentId,objParentCode){
		$.ajax({
			type : "POST",
			url : "/catalog/sale-type/getlistparentcode",
			data :({parentCode:parentId}),
			dataType : "json",
			success : function(data) {
				var html = new Array();
    			if(data!= null && data!= undefined && data.lstParentCode!= null && data.lstParentCode.length > 0){
    				html.push("<option value='-1'>---Chọn mã cha---</option>");
    				for(var i=0;i<data.lstParentCode.length;i++){
    					html.push("<option value='"+ data.lstParentCode[i].id +"'>"+ Utils.XSSEncode(data.lstParentCode[i].channelTypeName) +"</option>");
    				}
    			}else{
    				html.push("<option value='-1'>---Chọn mã cha---</option>");
    			}	        			        	
    			$(objParentCode).html(html.join(""));
    			$(objParentCode).val(parentId);
    			$(objParentCode).change();
			},
			error:function(XMLHttpRequest, textStatus, errorThrown) {
				$.ajax({
					type : "POST",
					url : '/check-session',
					dataType : "json",
					success : function(data) {
					},
					error: function(XMLHttpRequest, textStatus, errorThrown) {
						window.location.href = '/home';
					}
				});
			}
		});
	}
};
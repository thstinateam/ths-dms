var CategoryCatalog = {
	_xhrSave : null,
	_xhrDel: null,
	_stockLock: null,
	getGridUrl: function(code,name,description,status){
		return "/catalog/category/search?code=" + encodeChar(code) + "&name=" + encodeChar(name) + "&note=" + encodeChar(description) + "&status=" + status;
	},
	getStockLockByStaffCode : function(code){
		CategoryCatalog._stockLock = null;
		var params = {};
		params.staffCode = code;
		Utils.getJSONDataByAjax(params,"/catalog/utility/closing-day/get-locked-stock-by-staffCode",function(data){
			if(data!=undefined && data!=null && data.stockLock!=undefined && data.stockLock!=null){
				CategoryCatalog._stockLock = data.stockLock;
			}
		},null,null);
		
	},
	
	search: function(){
		$('#errMsg').html('').hide();
		var code = $('#categoryCode').val().trim();
		var name = $('#categoryName').val().trim();
		var description = $('#description').val().trim();
//		var status = $('#status').val().trim();
		var status = 1;
		var tm = setTimeout(function() {
			$('#categoryCode').focus();
		}, 500);
		var url = CategoryCatalog.getGridUrl(code,name,description,status);
		$("#grid").datagrid({url:url,page:1});
		return false;
	},
	save: function(){
		var msg = '';
		$('#errMsg').html('').hide();
		msg = Utils.getMessageOfRequireCheck('categoryCodePop','Mã category');
		if(msg.length == 0){
			msg = Utils.getMessageOfSpecialCharactersValidate('categoryCodePop','Mã category',Utils._CODE);
			if(msg.length > 0){
				msg = "Giá trị Mã ngành hàng nhập vào chỉ nằm trong các ký tự [A-Z]";
			}
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfSpecialCharactersValidate('categoryNamePop','Tên category');
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfRequireCheck('categoryNamePop','Tên category');
		}
		if(msg.length == 0 && $('#descriptionPop').val().trim().length > 0){
			msg = Utils.getMessageOfSpecialCharactersValidate('descriptionPop','Mô tả');
		}

		if(msg.length > 0){
			$('#errMsg').html(msg).show();
			return false;
		}
		var dataModel = new Object();
		dataModel.id = $('#selId').val().trim();
		dataModel.code = $('#categoryCodePop').val().trim();
		dataModel.name = $('#categoryNamePop').val().trim();
		dataModel.note = $('#descriptionPop').val().trim();
		dataModel.status = 1;
		Utils.addOrSaveRowOnGrid(dataModel, "/catalog/category/save", CategoryCatalog._xhrSave, null, null, function(data){
			if(!data.error){
				$('#popup1').dialog('close');
				CategoryCatalog.search();
			}
		});		
		return false;
	},
	getSelectedCategory: function(rowId, code, name, description, status){	
	
		if(rowId != null && rowId != 0 && rowId != undefined){
			$('#selId').val(rowId);
		} else {
			$('#selId').val(0);
		}
		if(description == 'null'){
			description= '';
		}
		$('#categoryCodePop').val(Utils.XSSEncode(code));
		$('#categoryNamePop').val(Utils.XSSEncode(name));
		$('#descriptionPop').val(Utils.XSSEncode(description));
        $('#popup1').dialog('open');
		return false;
	},
	resetForm: function(){
		CategoryCatalog.clearData();	
		$("#grid").trigger("reloadGrid");
		return false;
	},
	deleteCategory: function(code){
		var dataModel = new Object();
		dataModel.code = code;	
		Utils.deleteSelectRowOnGrid(dataModel, 'category', '/catalog/category/delete', CategoryCatalog._xhrDel, null, null, function(data){
			CategoryCatalog.clearData();
		});
		focusFirstTextbox();
		return false;		
	},
	clearData: function(){
		setTitleSearch();
		$('.RequireStyle').hide();
		$('#selId').val(0);
		$('#errMsg').html('').hide();
		$('#code').removeAttr('disabled');
		$('#btnEdit').hide();
		$('#btnCancel').hide();
		$('#btnSearch').show();
		$('#btnAdd').show();		
		$('#code').val('');
		$('#name').val('');
		$('#description').val('');
		focusFirstTextbox();
		setSelectBoxValue('status', 1);	
		Utils.unbindFormatOnTextfield('code');
		CategoryCatalog.search();
		Utils.bindAutoSearch();
	},
	getChangedForm: function(){
		Utils.bindFormatOnTextfield('code',Utils._TF_A_Z);
		$('#code').val('');
		$('.RequireStyle').show();
		$('#btnSearch').hide();
		$('#btnAdd').hide();
		$('#btnEdit').show();
		$('#btnCancel').show();	
		$('#code').focus();
		focusFirstTextbox();
		setTitleAdd();
	}
};
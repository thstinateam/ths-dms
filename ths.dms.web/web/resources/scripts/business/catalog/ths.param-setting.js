var ApParamSetting  = {
	search: function(){
		//aabb
		var params = new Object();			
		params.apParamCode = $('#apParamCode').val().trim();
		params.apParamName = $('#apParamName').val().trim();
		params.type = $('#apParamType').val().trim();
		params.status = $('#status').val().trim();
		$("#grid").datagrid('options').sortName = null; // search thi load lai sort binh thuong
		$('#grid').datagrid('load', params);
	},
	openDialogChangeAdd: function(){
		$(".ErrorMsgStyle").hide();
		$('#dialogParam').dialog({
			title: jsp_apparam_tao_moi_tham_so,
			closed: false,
			cache: false,
			modal: true,
	        width: 910,
	        height: 'auto',
	        onOpen: function(){
	        	$('.easyui-dialog .InputTextStyle').val('');
	    		$('.easyui-dialog #errorSearchMsg').hide();
	    		$('.easyui-dialog #statusDl').val(1).change();
	    		$('#codeDl').attr('disabled', false);
	    		/**combobox status*/
	    		$('#dialogParam .MySelectBoxClass').css('width', '173');
	    		$('#dialogParam .CustomStyleSelectBox').css('width', '140');
	    		$('#dialogParam #btnSaveBank').html(jsp_common_taomoi);
	    		
	    		$('#dialogParam #btnSaveBank').unbind('click');
	    		$('#dialogParam #btnSaveBank').bind('click',function(event) {
	    			ApParamSetting.add();
				});
	        },
	        onClose:function() {
	        	
	        }
		});
	},
	/** phuongvm; 07/10/2014 click buttom them moi */
	add: function(){
		var msg ="";
		if(msg.length == 0){
			msg = Utils.getMessageOfRequireCheck('codeDl', jsp_apparam_ma_tham_so);
		}
		if(msg.length >0){
			$('#errorMsgDl').html(msg).show();
			return false;
		}
		var params = new Object();		
		params.apParamCode = $('#codeDl').val().trim();
		params.apParamName = $('#nameDl').val().trim();
		params.type = $('#typeDl').val().trim();
		params.description = $('#descDl').val().trim();
		params.value = $('#valueDl').val().trim();
		params.status = $('#statusDl').val().trim();
		var msg = jsp_apparam_ban_tao_moi_tham_so;
		Utils.addOrSaveData(params, '/catalog/apparam/save', null,  'errorMsgDl', function(data) {
			$('#successMsgDl').html(msgCommon1).show();
			var tm = setTimeout(function(){
				$('#dialogParam').dialog('close');
				$('#btnSearch').click();
				$('.SuccessMsgStyle').hide();
				clearTimeout(tm);
			 }, 1500);
		}, null, null, null,msg);
		return false;
	},
	/** phuongvm 07/10/2014 opendialog cap nhat tham so */
	openDialogChangeUpdate: function(index, id){
		$(".ErrorMsgStyle").html('').hide();
		var data = $('#grid').datagrid('getRows')[index];
		$('#dialogParam').dialog({
			title: jsp_apparam_cap_nhat_tham_so +" "+ '<span style="color:#199700">' + Utils.XSSEncode(data.apParamCode) + '</span>',
			closed: false,
			cache: false,
			modal: true,
	        width: 910,
	        height: 'auto',
	        onOpen: function(){
	        	$('.easyui-dialog .InputTextStyle').val('');
	    		$('.easyui-dialog #errorSearchMsg').hide();
	    		/**combobox status*/
	    		$('#dialogParam .MySelectBoxClass').css('width', '173');
	    		$('#dialogParam .CustomStyleSelectBox').css('width', '140');
	    		$('#dialogParam #btnSaveBank').html(jsp_common_capnhat);

	    		$('#codeDl').attr('disabled', true);
	    		
	    		$('#codeDl').val(data.apParamCode);
	    		$('#nameDl').val(data.apParamName);
	    		$('#typeDl').val(data.type);
	    		$('#descDl').val(data.description);
	    		$('#valueDl').val(data.value);
	    		$('#statusDl').val(data.status).change();
	    		
	    		$('#dialogParam #btnSaveBank').unbind('click');
	    		$('#dialogParam #btnSaveBank').bind('click',function(event) {
	    			ApParamSetting.update(index, id);
				});
	        },
	        onClose:function() {
	        	
	        }
		});
	},
	/** phuongvm 07/10/2014 click buttom cap nhat tham so*/
	update: function(index, idParam){
		var params = new Object();			
		params.apParamCode = $('#codeDl').val().trim();
		params.apParamName = $('#nameDl').val().trim();
		params.value = $('#valueDl').val().trim();
		params.type = $('#typeDl').val().trim();
		params.description = $('#descDl').val().trim();
		params.status = $('#statusDl').val().trim();
		params.idParam = idParam;
		var data = $('#grid').datagrid('getRows')[index];
		
		if(data.apParamName== null){
			data.apParamName = '';
		}
		if(data.value== null){
			data.value = '';
		}
		if(data.type== null){
			data.type = '';
		}
		if(data.description== null){
			data.description = '';
		}
		if (data.apParamName == params.apParamName && data.value == params.value && data.type == params.type 
		 && data.description == params.description && data.status == params.status ) {
			$('#errorMsgDl').html(jsp_apparam_khong_doi_cap_nhat).show();
			return false;
		}
		var msg = jsp_apparam_ban_cap_nhat_tham_so;
		Utils.addOrSaveData(params, '/catalog/apparam/save', null,  'errorMsgDl', function(data) {
			$('#successMsgDl').html(msgCommon1).show();
			var tm = setTimeout(function(){
				$('#dialogParam').dialog('close');
				//$('#btnSearch').click();
				$('#grid').datagrid('reload');
				$('.SuccessMsgStyle').hide();
				clearTimeout(tm);
			 }, 1500);
		}, null, null, null,msg);
		return false;
	}
};

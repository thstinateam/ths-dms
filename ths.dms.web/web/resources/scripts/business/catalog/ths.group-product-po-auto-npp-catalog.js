var GroupProductPOAutoNPP = {
		xhrSave : null,
		_xhrDel: null,
		id:null,
		_groupId:null,
		_mapMutilSelect : null,
		_mapMultiSelectSub:null,
		_mapNode:null,
		_mapCheck:null,
		getGridUrl: function(shopCode,shopName,status){
			return "/catalog/group-product-po-auto-npp/search?shopCode=" + encodeChar(shopCode) + "&shopName=" + encodeChar(shopName) +  "&status=" + status;
		},
		getPopSeachShopMapUrl:function(shopCode,shopName){
			return "/catalog/group-product-po-auto-npp/getListShopNotInPoGroup?shopCode=" + encodeChar(shopCode) + "&shopName=" + encodeChar(shopName);
		},
		getGridGroupProductUrl: function(id,status){
			return "/catalog/group-product-po-auto-npp/searchGroupProduct?id=" + encodeChar(id);
		},
		getGridProductUrl:function(groupId,objectType){
			return "/catalog/group-product-po-auto-npp/searchListProduct?idGroup=" + groupId + "&objectType=" +objectType;
		},
		getGridSectorUrl:function(objectType, name, code){
			return "/catalog/group-product-po-auto-npp/searchListSector?objectType=" +objectType + "&name=" + encodeChar(name)+ "&code="+ encodeChar(code);
		},
		afterCreateUpdatePoAutoGroup:function(){
			//reload lai gridGroupProduct
			$('#gridGroupProduct').datagrid('reload');
		},
		search: function(){
//			GroupProductPOAutoNPP.reset();
			$('#errMsg').html('').hide();
			var shopCode = $('#shopCode').val().trim();
			var shopName = $('#shopName').val().trim();
			var status = $('#status').val();
			var tm = setTimeout(function() {
				$('#shopCode').focus();
			}, 500);
			var url = GroupProductPOAutoNPP.getGridUrl(shopCode,shopName,status);
			$("#gridShopMap").datagrid({url:url,pageNumber:1});
			GroupProductPOAutoNPP._hideGrids();
			return false;
		},
		_hideGrids: function() {
			$("#menuGroupProduct").hide();
			$("#menuSecGoods").hide();
			$("#menuSecGoodsChild").hide();
			$("#menuProduct").hide();
		},
		searchGroupProduct: function(id, shopCode){
//			$('#idCode2').val(id);
			GroupProductPOAutoNPP.id = id;
			GroupProductPOAutoNPP.reset();
			$('#menuGroupProduct').show();
			$('#errMsg').html('').hide();
			var url = GroupProductPOAutoNPP.getGridGroupProductUrl(id);
			var NPPshopCodeLabel = '';
			if (shopCode != undefined && shopCode != null ){
				NPPshopCodeLabel = shopCode;
			}
			$('.Title2Style #NPPshopCodeLabel').text("(NPP: "+NPPshopCodeLabel+")");
			var strTitle = '<a href="javascript:void(0);"  onclick="return GroupProductPOAutoNPP.openDialogCreateGroupProductPOAutoNPP(0);"><img src="/resources/images/icon-add.png"></a>';
			var strTitleAddOnGrid = getFormatterControlGrid('btnAddGrid2', strTitle);
			$("#gridGroupProduct").datagrid({
				  url:url,
			  	  pageList  : [10,20,30],
				  width: ($('#productGrid').width()),
				  pageSize : 10,
				  checkOnSelect :true,
				  height:'auto',
				  scrollbarSize : 0,
				  pagination:true,			  
				  fitColumns:true,		     
			      singleSelect:true,
				  method : 'GET',
				  rownumbers: true,
				  columns:[[	
					{field: 'groupCode', title: 'Mã nhóm', width: 100, sortable:false,resizable:false, align: 'left', formatter: function(value, row, index){
				    	return Utils.XSSEncode(value);
				    }},
				    {field: 'groupName', title: 'Tên nhóm', width: 340, sortable:false,resizable:false, align: 'left', formatter: function(value, row, index){
				    	return Utils.XSSEncode(value);
				    }},
				    {field: 'status', title: 'Trạng thái', width: 80, sortable:false,resizable:false, align: 'center',formatter:function(value,row,index){
				    	if(row.status == 1){
				    		return 'Hoạt động';
				    	}
				    	if(row.status == 0){
				    		return 'Tạm ngưng';
				    	}
				    }},
				    {field: 'cat', title: 'NH', width: 30, sortable:false,resizable:false, align: 'center',formatter:function(value,row,index){
				    	var data = '<a href="javascript:void(0);" onclick="return GroupProductPOAutoNPP.searchCat('+row.id+',0);"><img src="/resources/images/icon-view.png"></a>';
						return getFormatterControlGrid('btnDetailGrid2', data);
				    }},
				    {field: 'subcat', title: 'NHC', width: 30, sortable:false,resizable:false, align: 'center',formatter:function(value,row,index){
				    	return '<a href="javascript:void(0);" onclick="return GroupProductPOAutoNPP.searchSubCat('+row.id+',1);"><img src="/resources/images/icon-view.png"></a>';
				    }},
				    {field: 'product', title: 'SP', width: 30, sortable:false,resizable:false, align: 'center',formatter:function(value,row,index){
				    	return '<a href="javascript:void(0);" onclick="return GroupProductPOAutoNPP.searchProduct('+row.id+',2);"><img src="/resources/images/icon-view.png"></a>';
				    }},
				    {field: 'edit', title: strTitleAddOnGrid, width: 30, align: 'center',sortable:false,resizable:false,
				    formatter:function(value,row,index){
				    	var data = '<a href="javascript:void(0);" onclick="return GroupProductPOAutoNPP.openDialogUpdateGroupProductPOAutoNPP(1,'+row.id+',\''+Utils.XSSEncode(row.groupCode)+'\',\''+Utils.XSSEncode(row.groupName)+'\','+row.status+');"><img src="/resources/images/icon-edit.png"></a>';
						return getFormatterControlGrid('btnEditGrid2', data);
				    }},	
				    {field: 'id', index: 'id', hidden: true},	   
				  ]],
				  onLoadSuccess:function(){
						$('.datagrid-header-rownumber').html('STT');  
				      }
			});
			return false;
		},
		getListProduct:function(groupId){
			var objectType =2 ; 
			
		},
		deleteRow: function(groupDetailId){
			$('#errMsg').html('').hide();
			var dataModel = new Object();
			dataModel.groupDetailId = groupDetailId;	
			Utils.deleteSelectRowOnGrid(dataModel, 'sản phẩm', "/catalog/group-product-po-auto-npp/deleteProduct", GroupProductPOAutoNPP._xhrDel, null, null,function(data){
				if(data.error == false){
					//var url = GroupProductPOAutoNPP.getGridUrl(GroupProductPOAutoNPP.id,2);
					//$("#gridProduct").datagrid({url:url,page:1});
					$("#gridProduct").datagrid("reload");
				}
			});
			return false;		
		},
	
		searchCat:function(id,objectType){
			$('#idCode1').val(id);
			GroupProductPOAutoNPP.id = id;
			GroupProductPOAutoNPP.reset();
			$('#menuSecGoods').show();
			var url = GroupProductPOAutoNPP.getGridProductUrl(id,objectType);
			var strTitle = "<a href='javascript:void(0)' onclick='return GroupProductPOAutoNPP.openDialogUpdateSecGoods();'><img src='/resources/images/icon_add.png'/></a>";
			var strTitleAddOnGrid = getFormatterControlGrid('btnAddGrid3', strTitle);
			$("#gridSecGoods").datagrid({
				  url:url,
			  	  pageList  : [10,20,30],
				  width: 550,
				  pageSize : 10,
				  checkOnSelect :true,
				  height:'auto',
				  scrollbarSize : 0,
				  pagination:true,			  
				  fitColumns:true,		     
			      singleSelect:true,
				  method : 'GET',
				  rownumbers: true,
				  columns:[[	
					{field: 'code', title: 'Mã NH', width: 150, sortable:false,resizable:false, align: 'left', formatter: function(value, row, index){
				    	return Utils.XSSEncode(value);
				    }},
				    {field: 'name', title: 'Tên NH', width: 300, sortable:false,resizable:false, align: 'left', formatter: function(value, row, index){
				    	return Utils.XSSEncode(value);
				    }},
				    {field:'delete', title:strTitleAddOnGrid, width: 50, align: 'center',sortable:false,resizable:false, formatter: function(value, row, index){
				    	var data = "<a href='javascript:void(0)' onclick=\"return GroupPOAuto.deleteCategoryRow('"+ row.id +"')\"><img src='/resources/images/icon-delete.png'/></a>";
						return getFormatterControlGrid('btnDeleteGrid3', data);
				    }},
				  ]],
				  onLoadSuccess:function(){
						$('.datagrid-header-rownumber').html('STT');  
				      	$('#gridSecGoods').datagrid('resize');
				      }
			});
			return false;
		},
		searchSubCat:function(id,objectType){
			$('#idCode2').val(id);
			GroupProductPOAutoNPP.id = id;
			GroupProductPOAutoNPP.reset();
			$('#menuSecGoodsChild').show();
			var url = GroupProductPOAutoNPP.getGridProductUrl(id,objectType);
			var strTitle = '<a href="javascript:void(0)" onclick="return GroupProductPOAutoNPP.openDialogUpdateSecGoodsBrand();"><img src="/resources/images/icon_add.png"/></a>';
			var strTitleAddOnGrid = getFormatterControlGrid('btnAddGrid4', strTitle); 
			$("#gridSecGoodsBrand").datagrid({
				  url:url,
			  	  pageList  : [10,20,30],
				  width: 550,
				  pageSize : 10,
				  checkOnSelect :true,
				  height:'auto',
				  scrollbarSize : 0,
				  pagination:true,			  
				  fitColumns:true,		     
			      singleSelect:true,
				  method : 'GET',
				  rownumbers: true,
				  columns:[[
				            //here
				    {field: 'parentCode', title: 'Mã NH', width: 150, sortable:false,resizable:false, align: 'left', formatter: function(value, row, index){
				    	return Utils.XSSEncode(value);
				    }},
					{field: 'code', title: 'Mã NHC', width: 150, sortable:false,resizable:false, align: 'left', formatter: function(value, row, index){
				    	return Utils.XSSEncode(value);
				    }},
				    {field: 'name', title: 'Tên NHC', width: 300, sortable:false,resizable:false, align: 'left', formatter: function(value, row, index){
				    	return Utils.XSSEncode(value);
				    }},
				    {field:'delete', title:strTitleAddOnGrid, width: 50, align: 'center',sortable:false,resizable:false, formatter: function(value, row, index){
				    	var data = "<a href='javascript:void(0)' onclick=\"return GroupPOAuto.deleteSubCategoryRow('"+ row.id +"')\"><img src='/resources/images/icon-delete.png'/></a>";
						return getFormatterControlGrid('btnDeleteGrid4', data);
				    }},
				  ]],
				  onLoadSuccess:function(){
						$('.datagrid-header-rownumber').html('STT');  
				      	$('#gridSecGoodsBrand').datagrid('resize');
				      }
			});
			return false;
		},
		searchProduct:function(id,objectType){
			GroupProductPOAutoNPP.id = id;
			GroupProductPOAutoNPP.reset();
			$('#menuProduct').show();
			var url = GroupProductPOAutoNPP.getGridProductUrl(id,objectType);
			var strTilte = "<a href='javascript:void(0)' onclick=\'return GroupProductPOAutoNPP.openPopupProduct("+id +","+objectType+");\'><img src='/resources/images/icon_add.png'/></a>";
			var strTitleAddOnGrid = getFormatterControlGrid('btnAddGrid5', strTilte);
			$("#gridProduct").datagrid({
				  url:url,
			  	  pageList  : [10,20,30],
				  width: 550,
				  pageSize : 10,
				  checkOnSelect :true,
				  height:'auto',
				  scrollbarSize : 0,
				  pagination:true,			  
				  fitColumns:true,		     
			      singleSelect:true,
				  method : 'GET',
				  rownumbers: true,
				  columns:[[	
					{field: 'code', title: 'Mã sản phẩm', width: 150, sortable:false,resizable:false, align: 'left', formatter: function(value, row, index){
				    	return Utils.XSSEncode(value);
				    }},
				    {field: 'name', title: 'Tên sản phẩm', width: 300, sortable:false,resizable:false, align: 'left', formatter: function(value, row, index){
				    	return Utils.XSSEncode(value);
				    }},
				    {field:'delete', title:strTitleAddOnGrid, width: 50, align: 'center',sortable:false,resizable:false, formatter: function(value, row, index){
				    	var data = "<a href='javascript:void(0)' onclick=\"return GroupPOAuto.deleteRow('"+ row.id +"')\"><img src='/resources/images/icon-delete.png'/></a>";
						return getFormatterControlGrid('btnDeleteGrid5', data);
				    }},   
				  ]],
				  onLoadSuccess:function(){
						$('.datagrid-header-rownumber').html('STT');  
				      	$('#gridProduct').datagrid('resize');
				      }
			});
			return false;
		},
		reset:function(){
			$('#menuSecGoods').hide();
			$('#menuSecGoodsChild').hide();
			$('#menuProduct').hide();
		},
		searchProductTree:function(){
			var code = $("#productCode").val().trim();
			var name = 	$("#productName").val().trim();
			var nodes = $('#tree').tree('getChecked');
			for(var i=0; i<nodes.length; i++){
				if(nodes[i].attributes.productTreeVO.type == "PRODUCT"){
					GroupProductPOAutoNPP._mapCheck.put(nodes[i].id,nodes[i].id);
		    		}else{
		    			GroupProductPOAutoNPP._mapCheck.remove(nodes[i].id);
		    		}
			}
			$(".easyui-dialog #errMsgProductDlg").html('').hide();
			var GroupProductPOAutoNPPId = GroupProductPOAutoNPP.id; //id GroupProductPOAutoNPP
			$("#tree").tree({
				url: '/rest/catalog/group-po/tree/0.json',  
				method:'GET',
	            animate: true,
	            checkbox:true,
	            onBeforeLoad:function(node,param){
	            	$("#tree").css({"visibility":"hidden"});
//	            	$("#loadding").css({"visibility":"visible"});
	            	$("#rpSection").html('<img id="loadding" class="LoadingStyle" src="/resources/images/loadingbig.gif" width="30" height="30" alt="Loadding"/ >');
	            	$("#treeNoResult").html('');
	            	param.code = code;
	            	param.name = name;
	            	param.id = GroupProductPOAutoNPPId;
	            },
	            onLoadSuccess:function(node,data){
	            	$("#tree").css({"visibility":"visible"});
//	            	$("#loadding").css({"visibility":"hidden"});
	            	$("#rpSection").html('');
	            	if(data.length == 0){
	            		$(".easyui-dialog #errMsgProductDlg").html('Không có kết quả').show();
	            	}
	            	var arr  = GroupProductPOAutoNPP._mapCheck.keyArray;
	            	if(arr.length > 0){
	            		for(var i = 0;i < arr.length; i++){
	            			var nodes = $('#tree').tree('find', arr[i]);
	            			if(nodes != null){
	            				$("#tree").tree('check',nodes.target);
	            			}
	            		}
	            	}
	            },
	            onCheck:function(node,check){ 
	            	var nodes = $('#tree').tree('getChecked');
            	if(node.attributes.productTreeVO.type == "PRODUCT"){
            			GroupProductPOAutoNPP._mapCheck.put(node.id,node.id);
            		}else{
            			GroupProductPOAutoNPP._mapCheck.remove(node.id);
            		}
	            }
			});
		},
		openPopupProduct:function(id,objectType){
			$("#productCode").val('');
			$("#productName").val('');
			$(".easyui-dialog #errMsgProductDlg").html('').hide();
			GroupProductPOAutoNPP._mapNode = new Map();
			GroupProductPOAutoNPP._mapCheck = new Map();
			$("#popupProductSearch").dialog({
				onOpen:function(){
					$("#tree").tree({
						url: '/rest/catalog/group-po/tree/4.json',  
						method:'GET',
			            animate: true,
			            checkbox:true,
			            onBeforeLoad:function(node,param){
			            	$("#tree").css({"visibility":"hidden"});
			            	//$("#loadding").css({"visibility":"visible"});
			            	$("#rpSection").html('<img id="loadding" class="LoadingStyle" src="/resources/images/loadingbig.gif" width="30" height="30" alt="Loadding"/ >');
			            	$("#treeNoResult").html('');
			            	param.code = '';
			            	param.name = '';
			            	param.id = GroupProductPOAutoNPP.id;
			            },
			            onLoadSuccess:function(node,data){
			            	$("#tree").css({"visibility":"visible"});
			            	//$("#loadding").css({"visibility":"hidden"});
			            	$("#rpSection").html('');
			            	$("#productCode").focus();
//			            	if(GroupProductPOAutoNPP._mapNode != null && GroupProductPOAutoNPP._mapNode != undefined && GroupProductPOAutoNPP._mapNode.keyArray.length >0){
//			            		var nodes = $('#tree').tree('getChecked');
//			            		for(var i=0; i<nodes.length; i++){
//			            			var temp = GroupProductPOAutoNPP._mapNode.get(nodes[i].id);
//			            			if(temp != null && temp != undefined){
//			            				$("#tree").tree('check',nodes[i].target);
//			            			}
//			            		}
//			            	}
			            	if(data.length == 0){
			            		$(".easyui-dialog #errMsgProductDlg").html('Không có kết quả').show();
			            	}
			            	var arr  = GroupProductPOAutoNPP._mapCheck.keyArray;
			            	if(arr.length > 0){
			            		for(var i = 0;i < arr.length; i++){
			            			var nodes = $('#tree').tree('find', arr[i]);
			            			if(nodes != null){
			            				$("#tree").tree('check',nodes.target);
			            			}
			            		}
			            	}
			            },
			            onCheck:function(node,check){ 
			            	var nodes = $('#tree').tree('getChecked');
			                
//			            	if(check){
//			            			GroupProductPOAutoNPP._mapNode.put(node.id,node.id);
//			            			for(var i=0; i<nodes.length; i++){
//					                	if(nodes[i].attributes.productTreeVO.type == "PRODUCT"){
//					                		var temp = GroupProductPOAutoNPP._mapCheck.get(nodes[i].id);
//					                		if(temp == null){
//					                			GroupProductPOAutoNPP._mapCheck.put(nodes[i].id,nodes[i].id);
//					                		}
//					                	}
//					                }
//			            	}else{
//			            		GroupProductPOAutoNPP._mapNode.remove(node.id);
//			            		GroupProductPOAutoNPP._mapCheck.remove(node.id);
//			            	}
				            	if(node.attributes.productTreeVO.type == "PRODUCT"){
				            			GroupProductPOAutoNPP._mapCheck.put(node.id,node.id);
				            		}else{
				            			GroupProductPOAutoNPP._mapCheck.remove(node.id);
				            		}
			            }
					});
				}
			
			});
			$("#popupProductSearch").dialog('open');
		},
		createGroupProductPOAutoNPPDetail:function(){
			var dataModel = new Object();
			var nodes = $('#tree').tree('getChecked');
			var lstId = '';  
			var lstIdCustomer = new Array();
			$('#errMsgPopup3').html('').hide();
			if(nodes.length == 0 ) {
				$('#errMsgPopup3').html('Bạn chưa chọn sản phẩm nào.').show();
        		return false;
        	}
	        for(var i=0; i<nodes.length; i++){
	        	if(nodes[i].attributes.productTreeVO.type != "PRODUCT"){
	        		continue;
	        	}
	        	lstIdCustomer.push(nodes[i].attributes.productTreeVO.id); 
	        }
			if(lstIdCustomer == undefined || lstIdCustomer == null || lstIdCustomer.length <=0){
				$(".easyui-dialog #errMsgProductDlg").html('Không có kết quả').show();
				return false;
			}
			dataModel.objectType = 2;
			dataModel.id = GroupProductPOAutoNPP.id;
			dataModel.listOjectId = lstIdCustomer;
			Utils.addOrSaveRowOnGrid(dataModel, "/catalog/group-po/save", GroupProductPOAutoNPP._xhrSave, null, null,function(data){
				$("#popupProductSearch").dialog("close");
				if(data.error == false){
					$("#gridProduct").datagrid("reload");
				}
			});
			return false;

		},
		openDialogAddShopMap:function(codeText, nameText, title, url, callback,	codeFieldText, nameFieldText, arrParam, objectStaff){
			GroupProductPOAutoNPP._hideGrids();
			$('#popupShopMapDiv').css('visibility','visible');
			$('#popupShopMap').dialog('open');
			$('#shopCodePop').val('');
			$('#shopNamePop').val('');
			$('#shopCodePop').focus();
			GroupProductPOAutoNPP._mapMutilSelect = new Map();
			$('#errMsgPopShopMap').hide();
			$("#gridShopMapPop").datagrid({
				  url:'/catalog/group-product-po-auto-npp/getListShopNotInPoGroup',
			  	  pageList  : [10,20,30],
				  width: 550,
				  pageSize : 10,
				  checkOnSelect :true,
				  height:'auto',
				  scrollbarSize : 0,
				  pagination:true,
				  pageNumber:1,
				  fitColumns:true,		     
				  method : 'GET',
				  rownumbers: true,
				  columns:[[	
						{field: 'shopCode', title: 'Mã đơn vị', width: 150, sortable:false,resizable:false, align: 'left', formatter: function(value, row, index){
					    	return Utils.XSSEncode(value);
					    }},
						{field: 'shopName', title: 'Tên đơn vị', width: 300, sortable:false,resizable:false, align: 'left', formatter: function(value, row, index){
					    	return Utils.XSSEncode(value);
					    }},
						{field: 'id',title: '', checkbox:true, width:50,align:'center',sortable : false,resizable : false},
				  ]],
				  	onCheck : function(rowIndex, rowData) {
				    	var selectedId = rowData.id;
				    	GroupProductPOAutoNPP._mapMutilSelect.put(selectedId, rowData);
				    },
				    onUncheck : function(rowIndex, rowData) {
				    	var selectedId = rowData.id;
				    	GroupProductPOAutoNPP._mapMutilSelect.remove(selectedId);
				    },
				    onCheckAll : function(rows) {
				    	if($.isArray(rows)) {
				    		for(var i = 0; i < rows.length; i++) {
				    			var row = rows[i];
				    			var selectedId = row.id;
				    			GroupProductPOAutoNPP._mapMutilSelect.put(selectedId, row);
				    		}
				    	}
				    },
				    onUncheckAll : function(rows) {
				    	if($.isArray(rows)) {
				    		for(var i = 0; i < rows.length; i++) {
				    			var row = rows[i];
				    			var selectedId = row.id;
				    			GroupProductPOAutoNPP._mapMutilSelect.remove(selectedId);
				    		}
				    	}
				    },
				   onLoadSuccess:function(data){
				      	var easyDiv = '#gridShopMapContainer ';
				    	$(easyDiv+'.datagrid-header-rownumber').html('STT');
				    	var exitsAll = false;
			    		var runner = 0;
			    		$(easyDiv+'input:checkbox[name=id]').each(function() {
			    			var selectedId = this.value;
			    			if(GroupProductPOAutoNPP._mapMutilSelect.get(selectedId) != null || GroupProductPOAutoNPP._mapMutilSelect.get(selectedId) != undefined) {
			    				exitsAll = true;
			    				$('#gridShopMapPop').datagrid('selectRow',runner);
			    			} 
			    			runner ++; 
			    		});
			    		if (exitsAll==false){
			    			$(easyDiv+'td[field=id] .datagrid-header-check input:checkbox').attr('checked', false);
				    	}
				    	
			    		var tabindex = -1;
						$('.InputTextStyle, select, input, button , li , tr, td, th, label, ul, a, img').each(function () {
							if (this.type != 'hidden') {
								$(this).attr("tabindex", tabindex);
								tabindex -=1;
							}
						});
						tabindex = 1;
			    		 $('.easyui-dialog input,.easyui-dialog select,.easyui-dialog button').each(function () {
				    		 if (this.type != 'hidden') {
					    	     $(this).attr("tabindex", '');
								 tabindex++;
				    		 }
						 });
				 	    	if(data.rows == null || data.rows == undefined || data.rows.length == 0){
				 	    		$(easyDiv+'td[field=id] .datagrid-header-check input:checkbox').removeAttr('checked');
				 	    	}
			    		updateRownumWidthForDataGrid('#gridShopMapPop');
			    		$(window).resize();
				      }
			});
			
			return false;
		},
		searchShopMapPop: function(){
			$('#errMsgPopShopMap').html('').hide();
			$('#errMsg').html('').hide();
			var code = $('#shopCodePop').val().trim();
			var name = $('#shopNamePop').val().trim();
			var tm = setTimeout(function() {
				$('#shopCodePop').focus();
			}, 500);
			var url = GroupProductPOAutoNPP.getPopSeachShopMapUrl(code, name);
			$("#gridShopMapPop").datagrid({url:url,pageNumber:1});
			return false;
		},
		addPoAutoShopMap:function(){
			$('#errMsgPopShopMap').html('').hide();
			$('#errMsg').html('').hide();
			if(GroupProductPOAutoNPP._mapMutilSelect == null || GroupProductPOAutoNPP._mapMutilSelect.size() <= 0) {
				$('#errMsgPopShopMap').html('Bạn chưa chọn NPP nào. Yêu cầu chọn').show();
        		return false;
        	}
			var mapKey = GroupProductPOAutoNPP._mapMutilSelect.keyArray;
			var mapValue = GroupProductPOAutoNPP._mapMutilSelect.valArray;
			var params = new Object();
			params.listShopId = mapKey;
			GroupProductPOAutoNPP.updatePoAutoShopMap(params.listShopId);			
		},
		updatePoAutoShopMap:function(listShopId){
			var dataModel = new Object();
//			dataModel.id = $('#idCode1').val();
			dataModel.listShopId = listShopId;
			Utils.addOrSaveData(dataModel, "/catalog/group-product-po-auto-npp/addPOAutoGroupShopMap", null, 'errMsg',function(data){	
				if(data.errMsg!=undefined && data.errMsg.length==0){
					$('#popupShopMap').dialog('close');
					$("#gridShopMapPop").datagrid("reload");
					GroupProductPOAutoNPP.search();
				}
			});
		},
		openDialogEditShopMap: function(idPoAutoGroupShopMap,shopCode, shopName, status){
			if(idPoAutoGroupShopMap != null && idPoAutoGroupShopMap != 0 && idPoAutoGroupShopMap !=undefined){
				$('#shopIdHidden').val(idPoAutoGroupShopMap);
			} else {
				$('#shopIdHidden').val(0);
			}
			$('#popupEditShopMap').dialog({
				title : 'Cập nhật NPP với nhóm PO Auto',
				onOpen: function(){
					$('#errMsgPop').html('').hide();
					disabled('_shopCodePop');
					disabled('_shopNamePop');
					$('#_shopCodePop').val(shopCode);
					$('#_shopNamePop').val(shopName);
					$('#_statusPop').val(status).change();
					$('#_statusPop').focus();
					$('.easyui-dialog #__btnSaveShopMap').bind('click',function(event) {
						var params = new Object();
						params.id = idPoAutoGroupShopMap;
						params.statusValue = $('.easyui-dialog #_statusPop').val();
						Utils.addOrSaveData(params, '/catalog/group-product-po-auto-npp/updatePoAutoGroupShopMap', null, 'errMsgEditShopMap', function(data){
//							$('#'+searchDialog).dialog('close');
//								GroupProductPOAutoNPP.afterCreateUpdatePoAutoGroup();
							$('#gridShopMap').datagrid('reload');
						}, null, null, null, "Bạn có muốn cập nhật thông tin nhóm không ?", null);
					});
				},
				onClose : function() {
					SubCategorySecondary.clearPop();
					$('.easyui-dialog #__btnSaveShopMap').unbind('click');
		        	$('.easyui-dialog #errMsgEditShopMap').html('').hide();
				}
			});
	        $('#popupEditShopMap').dialog('open');
			return false;
		},
		
		
		
		
		
		openDialogUpdateSecGoods:function(codeText, nameText, title, url, callback,	codeFieldText, nameFieldText, arrParam, objectStaff){
			$('#popupSecFoods').css('visibility','visible');
			$('#popup1').dialog('open');
			$('#codeSecGoods').val('');
			$('#nameSecGoods').val('');
			$('#codeSecGoods').focus();
			GroupProductPOAutoNPP._mapMutilSelect = new Map();
			$('#errMsgPopup1').hide();
			var url = GroupProductPOAutoNPP.getGridSectorUrl(0, null, null);
//			$("#gridSecGoodsPop").datagrid({url:url,pageNumber:1});
			$("#gridSecGoodsPop").datagrid({
				  url:url,
			  	  pageList  : [10,20,30],
				  width: 550,
				  pageSize : 10,
				  checkOnSelect :true,
				  height:'auto',
				  scrollbarSize : 0,
				  pagination:true,
				  pageNumber:1,
				  fitColumns:true,		     
				  method : 'GET',
				  rownumbers: true,
				  columns:[[	
					{field: 'productInfoCode', title: 'Mã NH', width: 150, sortable:false,resizable:false, align: 'left', formatter: function(value, row, index){
				    	return Utils.XSSEncode(value);
				    }},
				    {field: 'productInfoName', title: 'Tên NH', width: 300, sortable:false,resizable:false, align: 'left', formatter: function(value, row, index){
				    	return Utils.XSSEncode(value);
				    }},
//				    {field: 'status',title: 'Trạng thái', width:100,align:'center',sortable : false,resizable : false , formatter : PoAutoGroupFomatter.statusFormatter},
				    {field: 'productInfoId',title: '', checkbox:true, width:50,align:'center',sortable : false,resizable : false},
				    
				  ]],
				  	onCheck : function(rowIndex, rowData) {
				    	var selectedId = rowData.productInfoId;
				    	GroupProductPOAutoNPP._mapMutilSelect.put(selectedId, rowData);
				    },
				    onUncheck : function(rowIndex, rowData) {
				    	var selectedId = rowData.productInfoId;
				    	GroupProductPOAutoNPP._mapMutilSelect.remove(selectedId);
				    },
				    onCheckAll : function(rows) {
				    	if($.isArray(rows)) {
				    		for(var i = 0; i < rows.length; i++) {
				    			var row = rows[i];
				    			var selectedId = row.productInfoId;
				    			GroupProductPOAutoNPP._mapMutilSelect.put(selectedId, row);
				    		}
				    	}
				    },
				    onUncheckAll : function(rows) {
				    	if($.isArray(rows)) {
				    		for(var i = 0; i < rows.length; i++) {
				    			var row = rows[i];
				    			var selectedId = row.productInfoId;
				    			GroupProductPOAutoNPP._mapMutilSelect.remove(selectedId);
				    		}
				    	}
				    },
				   onLoadSuccess:function(data){
				      	var easyDiv = '#gridSecGoodsPopContainer ';
				    	$(easyDiv+'.datagrid-header-rownumber').html('STT');
				    	var exitsAll = false;
			    		var runner = 0;
			    		$(easyDiv+'input:checkbox[name=productInfoId]').each(function() {
			    			var selectedId = this.value;
			    			if(GroupProductPOAutoNPP._mapMutilSelect.get(selectedId) != null || GroupProductPOAutoNPP._mapMutilSelect.get(selectedId) != undefined) {
			    				exitsAll = true;
			    				$('#gridSecGoodsPop').datagrid('selectRow',runner);
			    			} 
			    			runner ++; 
			    		});
			    		if (exitsAll==false){
			    			$(easyDiv+'td[field=productInfoId] .datagrid-header-check input:checkbox').attr('checked', false);
				    	}
				    	
			    		var tabindex = -1;
						$('.InputTextStyle, select, input, button , li , tr, td, th, label, ul, a, img').each(function () {
							if (this.type != 'hidden') {
								$(this).attr("tabindex", tabindex);
								tabindex -=1;
							}
						});
						tabindex = 1;
			    		 $('.easyui-dialog input,.easyui-dialog select,.easyui-dialog button').each(function () {
				    		 if (this.type != 'hidden') {
					    	     $(this).attr("tabindex", '');
								 tabindex++;
				    		 }
						 });
			    		 if(data.rows.length == 0){
			    			 $('.datagrid-header-check input').removeAttr('checked');
			    		 }
			    		 $(easyDiv+'input:checkbox[name=productInfoId]').each(function(){
			    			 	var selectedId = $(this).val();
				 				var temp = GroupProductPOAutoNPP._mapMutilSelect.get(selectedId);
				 				if(temp!=null) $(this).attr('checked','checked');
				 			});
				 	    	var length = 0;
				 	    	$(easyDiv+'input:checkbox[name=productInfoId]').each(function(){
				 	    		if($(this).is(':checked')){
				 	    			++length;
				 	    		}	    		
				 	    	});	    	
				 	    	if(data.rows.length==length){
				 	    		$(easyDiv+'td[field=productInfoId] .datagrid-header-check input:checkbox').attr('checked','checked');
				 	    	}else{
								$(easyDiv+'td[field=productInfoId] .datagrid-header-check input:checkbox').removeAttr('checked');
				 	    	}
				 	    	if(data.rows == null || data.rows == undefined || data.rows.length == 0){
				 	    		$(easyDiv+'td[field=productInfoId] .datagrid-header-check input:checkbox').removeAttr('checked');
				 	    	}
			    		updateRownumWidthForDataGrid('#gridSecGoodsPop');
			    		$(window).resize();
				      }
			});
			
			return false;
		},
		addCategory:function(){
			$('#errMsgPopup1').html('').hide();
			if(GroupProductPOAutoNPP._mapMutilSelect == null || GroupProductPOAutoNPP._mapMutilSelect.size() <= 0) {
				$('#errMsgPopup1').html('Bạn chưa chọn ngành hàng nào. Yêu cầu chọn').show();
        		return false;
        	}
	
			var mapKey = GroupProductPOAutoNPP._mapMutilSelect.keyArray;
			var mapValue = GroupProductPOAutoNPP._mapMutilSelect.valArray;
			var params = new Object();
			params.lstCategoryId = mapKey;
			GroupProductPOAutoNPP.updateCategory(params.lstCategoryId);			
		},
		openDialogUpdateSecGoodsBrand:function(codeText, nameText, title, url, callback,	codeFieldText, nameFieldText, arrParam, objectStaff){
			$('#popupSecFoodsBrand').css('visibility','visible');
			$('#popup2').dialog('open');
			$('#errMsgPopup2').hide();
			GroupProductPOAutoNPP._mapMutilSelectSub = new Map();
			$('#codeSecGoodsBrand').val('');
			$('#nameSecGoodsBrand').val('');
			$('#codeSecGoodsBrand').focus();
			var url = GroupProductPOAutoNPP.getGridSectorUrl(1, null, null);
			$("#gridSecGoodsPopBrand").datagrid({
				  url:url,
			  	  pageList  : [10,20,30],
				  width: 550,
				  pageSize : 10,
				  checkOnSelect :true,
				  height:'auto',
				  scrollbarSize : 0,
				  pagination:true,
				  pageNumber:1,
				  fitColumns:true,
				  method : 'GET',
				  rownumbers: true,
				  columns:[[
				            //here
				    {field: 'parentCode', title: 'Mã NH', width: 150, sortable:false,resizable:false, align: 'left', formatter: function(value, row, index){
				    	return Utils.XSSEncode(value);
				    }},
					{field: 'productInfoCode', title: 'Mã NHC', width: 150, sortable:false,resizable:false, align: 'left', formatter: function(value, row, index){
				    	return Utils.XSSEncode(value);
				    }},
				    {field: 'productInfoName', title: 'Tên NHC', width: 300, sortable:false,resizable:false, align: 'left', formatter: function(value, row, index){
				    	return Utils.XSSEncode(value);
				    }},
				    {field: 'productInfoId',title: '', checkbox:true, width:50,align:'center',sortable : false,resizable : false},			  
				  ]],
				  onSelect : function(rowIndex, rowData) {
				    	var selectedId = rowData['productInfoId'];
				    	GroupProductPOAutoNPP._mapMutilSelectSub.put(selectedId, rowData);
				    },
				    onUnselect : function(rowIndex, rowData) {
				    	var selectedId = rowData['productInfoId'];
				    	GroupProductPOAutoNPP._mapMutilSelectSub.remove(selectedId);
				    },
				    onSelectAll : function(rows) {
				    	if($.isArray(rows)) {
				    		for(var i = 0; i < rows.length; i++) {
				    			var row = rows[i];
				    			var selectedId = row['productInfoId'];
				    			GroupProductPOAutoNPP._mapMutilSelectSub.put(selectedId, row);
				    		}
				    	}
				    },
				    onUnselectAll : function(rows) {
				    	if($.isArray(rows)) {
				    		for(var i = 0; i < rows.length; i++) {
				    			var row = rows[i];
				    			var selectedId = row['productInfoId'];
				    			GroupProductPOAutoNPP._mapMutilSelectSub.remove(selectedId);
				    		}
				    	}
				    },
				   onLoadSuccess:function(){
				      	var easyDiv = '#gridSecGoodsPopBrandContainer ';
				    	$(easyDiv+'.datagrid-header-rownumber').html('STT');
				    	var exitsAll = false;
			    		var runner = 0;
			    		$(easyDiv+'input:checkbox[name=productInfoId]').each(function() {
			    			var selectedId = this.value;
			    			if(GroupProductPOAutoNPP._mapMutilSelectSub.get(selectedId) != null || GroupProductPOAutoNPP._mapMutilSelectSub.get(selectedId) != undefined) {
			    				exitsAll = true;
			    				$('#gridSecGoodsPopBrand').datagrid('selectRow',runner);
			    			} 
			    			runner ++; 
			    		});
			    		if (exitsAll==false){
			    			$(easyDiv+'td[field=productInfoId] .datagrid-header-check input:checkbox').attr('checked', false);
				    	}
				    	
			    		var tabindex = -1;
						$('.InputTextStyle, select, input, button , li , tr, td, th, label, ul, a, img').each(function () {
							if (this.type != 'hidden') {
								$(this).attr("tabindex", tabindex);
								tabindex -=1;
							}
						});
						tabindex = 1;
			    		 $('.easyui-dialog input,.easyui-dialog select,.easyui-dialog button').each(function () {
				    		 if (this.type != 'hidden') {
					    	     $(this).attr("tabindex", '');
								 tabindex++;
				    		 }
						 });
			    		updateRownumWidthForDataGrid('#gridSecGoodsPopBrand');
			    		$(window).resize();
				      }
			});
			
			return false;
		},
		addSubCategory:function(){
			$('#errMsgPopup2').html('').hide();
			if(GroupProductPOAutoNPP._mapMutilSelectSub == null || GroupProductPOAutoNPP._mapMutilSelectSub.size() <= 0) {
				$('#errMsgPopup2').html('Bạn chưa chọn ngành hàng con nào. Yêu cầu chọn').show();
        		return false;
        	}
	
			var mapKey = GroupProductPOAutoNPP._mapMutilSelectSub.keyArray;
			var mapValue = GroupProductPOAutoNPP._mapMutilSelectSub.valArray;
			var params = new Object();
			params.lstCategoryId = mapKey;
			GroupProductPOAutoNPP.updateSubCategory(params.lstCategoryId);
		},		
		searchSecGoods: function(){
			$('#errMsgPopup1').html('').hide();
			$('#errMsg').html('').hide();
			var code = $('#codeSecGoods').val().trim();
			var name = $('#nameSecGoods').val().trim();
			var tm = setTimeout(function() {
				$('#codeSecGoods').focus();
			}, 500);
			var objectType = 0;
			var url = GroupProductPOAutoNPP.getGridSectorUrl(objectType, name, code);
			$("#gridSecGoodsPop").datagrid({url:url,pageNumber:1});
			return false;
		},
		searchSecGoodsBrand: function(){
			$('#errMsgPopup2').html('').hide();
			$('#errMsg').html('').hide();
			var code = $('#codeSecGoodsBrand').val().trim();
			var name = $('#nameSecGoodsBrand').val().trim();
			var tm = setTimeout(function() {
				$('#codeSecGoodsBrand').focus();
			}, 500);
			var objectType = 1;
			var url = GroupProductPOAutoNPP.getGridSectorUrl(objectType, name, code);
			$("#gridSecGoodsPopBrand").datagrid({url:url,pageNumber:1});
//			$("#gridSecGoodsPopBrand").datagrid({
//				  url:url,
//			  	  pageList  : [10,20,30],
//				  width: 550,
//				  pageSize : 10,
//				  checkOnSelect :true,
//				  height:'auto',
//				  scrollbarSize : 0,
//				  pagination:true,			  
//				  fitColumns:true,		     
//			      singleSelect:true,
//				  method : 'GET',
//				  rownumbers: true,
//				  columns:[[
//				    {field: 'parentCode', title: 'Mã NH', width: 150, sortable:false,resizable:false, align: 'left'},
//					{field: 'productInfoCode', title: 'Mã NHC', width: 150, sortable:false,resizable:false, align: 'left'},
//				    {field: 'productInfoName', title: 'Tên NHC', width: 300, sortable:false,resizable:false, align: 'left'},
//				    {field: 'status',title: '<a href="javascript:void(0)" onclick="return GroupProductPOAutoNPP.openDialogUpdateSecGoodsBrand();"></a>', width:50,align:'center',sortable : false,resizable : false,
//		            	formatter:function(value,row){
//		            		if(row.status == 1){
//		            			return '<input type="checkbox" checked="checked" onClick=""/>';
//		            		}else{
//		            			return '<input type="checkbox" onClick="" />';
//		            		}
//		            	}
//				    }	   
//				  ]],
//				  onLoadSuccess:function(){
//						$('.datagrid-header-rownumber').html('STT');  
//				      	$('#gridSecGoodsPopBrand').datagrid('resize');
//				      }
//			});
			return false;

		},
		openDialogCreateGroupProductPOAutoNPP:function(flag,idPoAutoGroupShopMap,groupCode,groupName,status){
			GroupProductPOAutoNPP.reset();
			var arrayParam = new Array();
			arrayParam.push(flag);
			arrayParam.push(idPoAutoGroupShopMap);
			arrayParam.push(groupCode);
			arrayParam.push(groupName);
			arrayParam.push(status);
			GroupProductPOAutoNPP.openSearchStyle1EasyUICreateUpdateGroupPOAutoNpp(null,
					null, "Thông tin nhóm","groupCode",
					"groupName",'searchStyle1EasyUIDialogCreate','searchStyle1EasyUIDialogDiv',arrayParam);
//			$('#searchStyle1EasyUIDialogDiv #searchStyle1Code').focus();
		},
		openDialogUpdateGroupProductPOAutoNPP:function(flag,idPoAutoGroup,groupCode,groupName,status){
			GroupProductPOAutoNPP.reset();
			var arrayParam = new Array();
			arrayParam.push(flag);
			arrayParam.push(idPoAutoGroup);
			arrayParam.push(groupCode);
			arrayParam.push(groupName);
			arrayParam.push(status);
			GroupProductPOAutoNPP.openSearchStyle1EasyUICreateUpdateGroupPOAutoNpp(null,
					null, "Thông tin nhóm","groupCode",
					"groupName",'searchStyle1EasyUIDialogUpdate','searchStyle1EasyUIDialogDivUpdate',arrayParam);
//			$('#searchStyle1EasyUIDialogDivUpdate #searchStyle1Name').focus();
		},
		updateCategory:function(listCatID){
			var dataModel = new Object();
			dataModel.id = $('#idCode1').val();
			dataModel.lstCategoryId = listCatID;
			Utils.addOrSaveData(dataModel, "/catalog/group-product-po-auto-npp/addPOAutoGroupDetail", null, 'errMsg',function(data){	
				if(data.errMsg!=undefined && data.errMsg.length==0){
					$('#popup1').dialog('close');
					$("#gridSecGoods").datagrid("reload");
				}
			});
		},
		updateSubCategory:function(listCatID){
			var dataModel = new Object();
			dataModel.id = $('#idCode2').val();
			dataModel.lstCategoryId = listCatID;
			Utils.addOrSaveData(dataModel, "/catalog/group-product-po-auto-npp/addPOAutoGroupDetailSubCat", null, 'errMsg',function(data){	
				if(data.errMsg!=undefined && data.errMsg.length==0){
					$('#popup2').dialog('close');
					$("#gridSecGoodsBrand").datagrid("reload");
				}
			});
		},
		deleteCategoryRow: function(groupDetailId){
			$('#errMsg').html('').hide();
			var dataModel = new Object();
			dataModel.groupDetailId = groupDetailId;	
			Utils.deleteSelectRowOnGrid(dataModel, 'Ngành hàng', "/catalog/group-product-po-auto-npp/deleteCategoryRow", GroupProductPOAutoNPP._xhrDel, null, null,function(data){
				if(data.error == false){
					$("#gridSecGoods").datagrid("reload");
				}
			});
			return false;		
		},
		deleteSubCategoryRow: function(groupDetailId){
			$('#errMsg').html('').hide();
			var dataModel = new Object();
			dataModel.groupDetailId = groupDetailId;	
			Utils.deleteSelectRowOnGrid(dataModel, 'Ngành hàng con', "/catalog/group-product-po-auto-npp/deleteSubCategoryRow", GroupProductPOAutoNPP._xhrDel, null, null,function(data){
				if(data.error == false){
					$("#gridSecGoodsBrand").datagrid("reload");
				}
			});
			return false;		
		},
		openSearchStyle1EasyUICreateUpdateGroupPOAutoNpp : function(codeText, nameText, title, codeFieldText, nameFieldText, searchDialog,searchDiv,arrayParam) {
			CommonSearchEasyUI._arrParams = null;
			CommonSearchEasyUI._currentSearchCallback = null;
			$('#'+searchDiv).css("visibility", "visible");
			var html = $('#'+searchDiv).html();
			$('#'+searchDialog).dialog({  
		        title: title,  
		        closed: false,  
		        cache: false,  
		        modal: true,
		        width : 616,
		        height :'auto',
		        onOpen: function(){
		        	//@CuongND : my try Code <reset tabindex>
		        	var tabindex = -1;
					$('.InputTextStyle, select, input, button , li , tr, td, th, label, ul, a, img').each(function () {
						if (this.type != 'hidden') {
							$(this).attr("tabindex", tabindex);
							tabindex -=1;
						}
					});
					tabindex = 1;
		    		 $('.easyui-dialog input,.easyui-dialog select,.easyui-dialog button').each(function () {
			    		 if (this.type != 'hidden') {
				    	     $(this).attr("tabindex", '');
							 tabindex++;
			    		 }
					 });
					//end <reset tabindex>
					//$('.easyui-dialog #searchStyle1Code').focus();
					$('.easyui-dialog #searchStyle1CodeText').val(codeFieldText);
					$('.easyui-dialog #searchStyle1NameText').val(nameFieldText);
					
					var codeField = 'code';
					var nameField = 'name';
					if (codeFieldText != null && codeFieldText != undefined && codeFieldText.length > 0) {
						codeField = codeFieldText;
					}
					if (nameFieldText != null&& nameFieldText != undefined && nameFieldText.length > 0) {
						nameField = nameFieldText;
					}
					
//					$('.easyui-dialog #seachStyle1CodeLabel').html(codeText);
//					$('.easyui-dialog #seachStyle1NameLabel').html(nameText);
					
					if(arrayParam != null && arrayParam[0] == 1){
						var code = arrayParam[2];
						var name = arrayParam[3];
						var status = arrayParam[4];
						$('.easyui-dialog #seachStyle1Code').attr('disabled','disabled');
						$('.easyui-dialog #seachStyle1Code').val(code);
						$('.easyui-dialog #seachStyle1Name').val(name);
						$('.easyui-dialog #seachStyle1Name').focus();
						$('.easyui-dialog #statusDialogUpdate option[value='+status+']').attr('selected','selected');
						var value = $('.easyui-dialog #statusDialogUpdate option[value='+status+']').html();
						$('.easyui-dialog .CustomStyleSelectBoxInner').html(value);
					}
					if(arrayParam != null && arrayParam[0] == 0){
						var value = $('.easyui-dialog #statusDialog option[selected=selected]').html();
						$('.easyui-dialog .CustomStyleSelectBoxInner').html(value);
						$('.easyui-dialog #seachStyle1Code').removeAttr('disabled').focus();
					}
				
					$('.easyui-dialog #__btnSave').bind('click',function(event) {
						var s = -1;
						var code = $('.easyui-dialog #seachStyle1Code').val().trim();
						var name = $('.easyui-dialog #seachStyle1Name').val().trim();
						var msg = "";
						if(code == undefined || code == "" || code == null){
							msg = "Bạn chưa nhập giá trị cho trường Mã nhóm.";
							$('.easyui-dialog #errMsgSearchCreate').html(msg).show();
							$('.easyui-dialog #seachStyle1Code').focus();
							return false;
						}
						if(msg.length == 0){
							msg = Utils.getMessageOfSpecialCharactersValidateEx1('.easyui-dialog #seachStyle1Code', 'Mã nhóm', Utils._CODE, null);
							if(msg.length >0){
								$('.easyui-dialog #errMsgSearchCreate').html(msg).show();
								$('.easyui-dialog #seachStyle1Code').focus();
								return false;
							}
						}
						if(msg.length == 0){
							if(name == undefined || name == "" || name == null){
								msg = "Bạn chưa nhập giá trị cho trường Tên nhóm.";
								$('.easyui-dialog #errMsgSearchCreate').html(msg).show();
								$('.easyui-dialog #seachStyle1Name').focus();
								return false;
							}
						}
						if(msg.length == 0){
							msg = Utils.getMessageOfSpecialCharactersValidateEx1('.easyui-dialog #seachStyle1Name', 'Tên nhóm', Utils._NAME, null);
							if(msg.length >0){
								$('.easyui-dialog #errMsgSearchCreate').html(msg).show();
								$('.easyui-dialog #seachStyle1Name').focus();
								return false;
							}
						}
						if(arrayParam != null){
							s = arrayParam[0];
						}
						if(s == 0){
							var params = new Object();
							params.id = GroupProductPOAutoNPP.id;//idPoAutoGroupShopMap
							params.groupCode = code;
							params.groupName = name;
							params.statusValue = $('.easyui-dialog #statusDialog').val();
							Utils.addOrSaveData(params, '/catalog/group-product-po-auto-npp/creategrouppo', null, 'errMsgSearchCreate', function(data){
								$('#'+searchDialog).dialog('close'); 
								GroupProductPOAutoNPP.afterCreateUpdatePoAutoGroup();
							}, null, null, null, "Bạn có muốn thêm nhóm mới không ?", null);
						}
						
					});
					$('.easyui-dialog #__btnSave1').bind('click',function(event) {
						var s = -1;
						var code = $('#searchStyle1EasyUIDialogUpdate #seachStyle1Code').val().trim();
						var name = $('#searchStyle1EasyUIDialogUpdate #seachStyle1Name').val().trim();
						var msg = "";
						if(code == undefined || code == "" || code == null){
							msg = "Bạn chưa nhập giá trị cho trường Mã nhóm.";
							$('.easyui-dialog #errMsgSearchUpdate').html(msg).show();
							$('#searchStyle1EasyUIDialogUpdate #seachStyle1Code').focus();
							return false;
						}
						if(msg.length == 0){
							msg = Utils.getMessageOfSpecialCharactersValidateEx1('#searchStyle1EasyUIDialogUpdate #seachStyle1Code', 'Mã nhóm', Utils._CODE, null);
							if(msg.length >0){
								$('.easyui-dialog #errMsgSearchUpdate').html(msg).show();
								$('#searchStyle1EasyUIDialogUpdate #seachStyle1Code').focus();
								return false;
							}
						}
						if(msg.length == 0){
							if(name == undefined || name == "" || name == null){
								msg = "Bạn chưa nhập giá trị cho trường Tên nhóm.";
								$('.easyui-dialog #errMsgSearchUpdate').html(msg).show();
								$('#searchStyle1EasyUIDialogUpdate #seachStyle1Name').focus();
								return false;
							}
						}
						if(msg.length == 0){
							msg = Utils.getMessageOfSpecialCharactersValidateEx1('#searchStyle1EasyUIDialogUpdate #seachStyle1Name', 'Tên nhóm', Utils._NAME, null);
							if(msg.length >0){
								$('.easyui-dialog #errMsgSearchUpdate').html(msg).show();
								$('#searchStyle1EasyUIDialogUpdate #seachStyle1Name').focus();
								return false;
							}
						}
						if(arrayParam != null){
							s = arrayParam[0];
						}
						if(s == 1){
							var params = new Object();
							params.id = arrayParam[1];
							var name = $('#searchStyle1EasyUIDialogUpdate #seachStyle1Name').val().trim();
							params.groupName = name;
							params.statusValue = $('.easyui-dialog #statusDialogUpdate').val();
							Utils.addOrSaveData(params, '/catalog/group-product-po-auto-npp/updategrouppo', null, 'errMsgSearchUpdate', function(data){
								$('#'+searchDialog).dialog('close');
									GroupProductPOAutoNPP.afterCreateUpdatePoAutoGroup();
							}, null, null, null, "Bạn có muốn cập nhật thông tin nhóm không ?", null);
						}
						
					});
		        },
		        onBeforeClose: function() {
					var tabindex = 1;
					$(' .InputTextStyle, select, input, button , li , tr, td, th, label, ul, a, img').each(function () {
						if (this.type != 'hidden') {
							$(this).attr("tabindex", '');
						}
						tabindex ++;
					});	
					var curIdFocus = $('#cur_focus').val();
					$('#'+ curIdFocus).focus();
		        },
		        onClose : function(){
		        	$('#'+searchDiv).html(html);
		        	$('#'+searchDiv).css("visibility", "hidden");
		        	$('.easyui-dialog #seachStyle1Code').val('');
		        	$('.easyui-dialog #seachStyle1Name').val('');
		        	$('.easyui-dialog #__btnSave').unbind('click');
		        	$('.easyui-dialog #__btnSave1').unbind('click');
		        	$('.easyui-dialog #errMsgSearchCreate').html('').hide();
		        	$('.easyui-dialog #errMsgSearchUpdate').html('').hide();	
		        }
		    });
			return false;
		}
		
};
var CustomerTypeCatalog = {
	_xhrSave : null,
	_xhrDel: null,
	isContextMenu:0,
	parentsId:null,
	selectnodeId:-2,
	searchModel:null,
	isRefresh:false,
	getGridUrl: function(code,pId,name,status,allItem,skuLevel,saleAmount, isGetParentAndChild){		
		return "/catalog/customer-type/search?code=" + encodeChar(code) 
		+ "&name=" + encodeChar(name) 
		+ "&parentId=" + encodeChar(pId) 
		+ '&status='+ status 
		+ '&allItem=' + encodeChar(allItem)
		+ '&skuLevel=' + skuLevel
		+ '&saleAmount=' + saleAmount
		+ '&isGetParentAndChild=' + (isGetParentAndChild != undefined && isGetParentAndChild != null ? isGetParentAndChild : '');
	},	
	search: function(){
		$('#errMsg').html('').hide();
		var tm = setTimeout(function() {
			$('#code').focus();
		}, 500);
		var code = $('#code').val().trim();
		var name = $('#name').val().trim();
		var status = $('#status').val().trim();
		var pId = CustomerTypeCatalog.selectnodeId;	
		var skuLevel = $('#skuLevel').val().trim();		
		var saleAmount = $('#saleAmount').val().trim();
		CustomerTypeCatalog.searchModel = new Object();
		CustomerTypeCatalog.searchModel.code1 = code;
		CustomerTypeCatalog.searchModel.name1 = name;
		CustomerTypeCatalog.searchModel.status1 = status;
		CustomerTypeCatalog.searchModel.skuLevel1 = skuLevel;
		CustomerTypeCatalog.searchModel.saleAmount1 = saleAmount;
		
		var msg = '';
		var skuLevel = Utils.returnMoneyValue($('#skuLevel').val().trim());
		var saleAmount = Utils.returnMoneyValue($('#saleAmount').val().trim());
		if(skuLevel.length>0 && isNaN(skuLevel)){
			msg = catalog_customer_type_standard_sku_is_not_number ;
			$('#skuLevel').focus();
		}
		if(saleAmount.length>0 && isNaN(saleAmount)){
			msg = catalog_customer_type_target_ds_is_not_number;
			$('#saleAmount').focus();
		}
		if(msg.length > 0){
			$('#errMsg').html(msg).show();
			return false;
		}
		var url = CustomerTypeCatalog.getGridUrl(code,pId,name,status,0,skuLevel,saleAmount);
		if(pId != undefined && pId!=null && pId >0){
			url = null;
			url = CustomerTypeCatalog.getGridUrl(code,pId,name,status,1,skuLevel,saleAmount);
		}
		$("#grid").datagrid({url:url,pageNumber:1});
		return false;
	},
	save: function(){
		var msg = '';
		$('#errMsg').html('').hide();
		msg = Utils.getMessageOfRequireCheck('code',catalog_customer_type_customer_type_code);
		if(msg.length == 0){
			msg = Utils.getMessageOfSpecialCharactersValidate('code',catalog_customer_type_customer_type_code,Utils._CODE);
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfRequireCheck('name',catalog_customer_type_customer_type_name);
		}
//		if(msg.length == 0){
//			msg = Utils.getMessageOfSpecialCharactersValidate('name',catalog_customer_type_customer_type_name,Utils._NAME_CUSTYPE);
//		}	
		if(msg.length == 0){
			msg = Utils.getMessageOfRequireCheck('status',report_status_label,true);
		}
		
		/*if(msg.length == 0){
			msg = Utils.getMessageOfRequireCheck('skuLevel',catalog_customer_type_standard_sku, false, false);
		}
		if (msg.length == 0) {
			var v = $("#skuLevel").val().trim();
			v = v.replace(/,/g, '');
			if (v.length > 15) {
				msg = format(msgErr_invalid_maxlength,catalog_customer_type_standard_sku,15);
			}
		}*/
		
		/*if(msg.length == 0){
			msg = Utils.getMessageOfRequireCheck('saleAmount',catalog_customer_type_target_ds, false, false);
		}
		if (msg.length == 0) {
			var v = $("#saleAmount").val().trim();
			v = v.replace(/,/g, '');
			if (v.length > 15) {
				msg = format(msgErr_invalid_maxlength,catalog_customer_type_target_ds,15);
			}
		}*/
		var skuLevel = Utils.returnMoneyValue($('#skuLevel').val().trim());
		var saleAmount = Utils.returnMoneyValue($('#saleAmount').val().trim());
		if(isNaN(skuLevel) && msg.length == 0){
			msg = catalog_customer_type_standard_sku_is_not_number;
			$('#skuLevel').focus();
		}
		if(isNaN(saleAmount) && msg.length == 0){
			msg = catalog_customer_type_target_ds_is_not_number;
			$('#saleAmount').focus();
		}		
		
		if($('#status').val() != 1 && $('#status').val() != 0 && msg.length == 0){
			msg = catalog_customer_type_chose_status;
		}
		if(msg.length > 0){
			$('#errMsg').html(msg).show();
			return false;
		}
		var dataModel = new Object();
		dataModel.id = $('#selId').val().trim();
		dataModel.code = $('#code').val().trim();
		dataModel.name = $('#name').val().trim();
		dataModel.skuLevel = Utils.returnMoneyValue($('#skuLevel').val().trim());
		dataModel.saleAmount = Utils.returnMoneyValue($('#saleAmount').val().trim());
		dataModel.status = $('#status').val();
		dataModel.parentId = $('#parentCode').combotree('getValue');
		if(dataModel.id != '' && dataModel.status == 0){
			dataModel.changeCus = 0;
		}
		Utils.addOrSaveRowOnGrid(dataModel, "/catalog/customer-type/save", CustomerTypeCatalog._xhrSave, null, null, function(data){
			if(!data.error){
				if(data.hasMessageListCustomer != undefined && data.hasMessageListCustomer != null && data.hasMessageListCustomer !=""){
					return false;
				}
//				CustomerTypeCatalog.selectnodeId = '';
//				$('#contentGrid').show();
//				CustomerTypeCatalog.loadTree();
//				CustomerTypeCatalog.clearData();
//				var url = CustomerTypeCatalog.getGridUrl('','','',1,0,'','');
//				$("#grid").datagrid({url:url,pageNumber:1});
//				CustomerTypeCatalog.loadTree();
				var tree = jQuery.jstree._reference("#tree");
				tree.refresh();
				CustomerTypeCatalog.clearData();
				$('#contentGrid').show();
				if(CustomerTypeCatalog.searchModel != null){
					setTextboxValue('skuLevel',CustomerTypeCatalog.searchModel.skuLevel1);
					setTextboxValue('saleAmount',CustomerTypeCatalog.searchModel.saleAmount1);
					setTextboxValue('code',CustomerTypeCatalog.searchModel.code1);
					setTextboxValue('name',CustomerTypeCatalog.searchModel.name1);
					setSelectBoxValue('status',CustomerTypeCatalog.searchModel.status1);
				}
				CustomerTypeCatalog.search();
				$('#code').focus();
			}			
		});
		
		return false;
	},
	getSelectedRow: function(rowId,parentChannelTypeId){	
		var nodeId = rowId;
		CustomerTypeCatalog.isContextMenu = 1;
		if(nodeId != '-2'){
			CustomerTypeCatalog.parentsId = parentChannelTypeId;
		}else{
			CustomerTypeCatalog.parentsId = '-2';
			return false;
		}
		$.ajax({
			type : "POST",
			url : '/catalog/customer-type/update',
			dataType : "json",
			data :$.param({id : nodeId}, true),
			success : function(result) {
				//hidden div chua grid
				var channelType = result.channelType;
				var status = activeType.STOPPED;
				if(channelType.status === activeStatus){
					status = activeType.RUNNING;
				}
				//Chi co 2 trang thai khi cap nhat laoi khach hang
				$('#status').html('').change();
				$('#status').append('<option value="1">Hoạt động</option>');
				$('#status').append('<option value="0">Tạm ngưng </option>');
				$('#status').change();
				
				$('#selId').val(nodeId.toString());
				$('#code').attr('disabled','disabled');
				$('#parentCode').combotree('disable');
				setTextboxValue('skuLevel',formatCurrency(channelType.sku.toString()));
				setTextboxValue('saleAmount',formatCurrency(channelType.saleAmount.toString()));
				setTextboxValue('code',channelType.channelTypeCode.toString());
				setTextboxValue('name',channelType.channelTypeName.toString());
				setSelectBoxValue('status',status);
				$('#divParentCode').show();
				CustomerTypeCatalog.getChangedForm();
				CustomerTypeCatalog.isContextMenu = 0;
				$('#title').html(catalog_customer_type_information);
				$('#title1').html(catalog_customer_type_information);
				$('#name').focus();
				
			}
    	});
		return false;
	},
	resetForm: function(){
		CustomerTypeCatalog.clearData();	
		$('#code').focus();
		return false;
	},
	deleteRow: function(code){
		var dataModel = new Object();
		dataModel.code = code;	
		Utils.deleteSelectRowOnGrid(dataModel, catalog_customer_type_title, '/catalog/customer-type/delete', CustomerTypeCatalog._xhrDel, null, null,function(){
			CustomerTypeCatalog.loadTree();
			$('#parentCode').combotree('setValue',activeType.ALL);		
			$('#parentCode').combotree("reload",'/rest/catalog/customer-type/combotree/0/0.json');
			$('#parentCode').combotree('setValue',activeType.ALL);
			CustomerTypeCatalog.clearData();
		});
		$('#name').focus();
		return false;
	},
	customMenu : function(node){
//		console.log(node);
		var items =  { // Could be a function that should return an object like this one
            "create" : {
                "separator_before"  : false,
                "separator_after"   : true,
                "label"             : "<span style='font-size:13px'>"+Utils.XSSEncode(catalog_customer_type_add_information)+"</span>",
                "icon"				:"/resources/images/icon_add.png",
                "action"            : function(obj){
                						
                						var nodeId = obj.attr('id').trim();
                						CustomerTypeCatalog.setSelectedNode(nodeId);
                						CustomerTypeCatalog.isContextMenu = 1;
                						CustomerTypeCatalog.parentsId = nodeId;
                						CustomerTypeCatalog.newCusType();
                						disableSelectbox('parentCode');
                						$('#parentCode').combotree('disable', true);
                						var html = "";
                						html += ('<select  class="MySelectBoxClass" style="width:206px">');
                						html += ('<option value="1" selected = "selected">'+Utils.XSSEncode(jsp_common_status_active)+' </option>');
                						html += ('<option value="0">'+Utils.XSSEncode(jsp_common_status_stoped)+' </option>');
                						html += ('</select>');
                						$('#status').html(html);
                						
                }
            },
			"update" : {
				"separator_before"	: false,
				"separator_after"	: false,
				"label"				: "<span style='font-size:13px'>"+Utils.XSSEncode(catalog_customer_type_update_information)+"</span>",
				"icon"				:"/resources/images/icon_edit.png",
				"action"			: function (obj) { 
					var nodeId = obj.attr('id').trim();
					CustomerTypeCatalog.setSelectedNode(nodeId);
					CustomerTypeCatalog.isContextMenu = 1;
					if(nodeId != '-2'){
						CustomerTypeCatalog.parentsId = $.jstree._reference('#tree')._get_parent(obj).attr("id");
					}else{
						CustomerTypeCatalog.parentsId = '-2';
						return false;
					}
					
                	$.ajax({
        				type : "POST",
        				url : '/catalog/customer-type/update',
        				dataType : "json",
        				data :$.param({id : nodeId}, true),
        				success : function(result) {
        					//hidden div chua grid
        					$('#contentGrid').hide();
        					//$('#divGeralMilkBox').show();
        					var channelType = result.channelType;
        					var status = 1;
        					if(channelType.status != activeStatus){
        						status = 0;
        					}
        					$('#selId').val(nodeId.toString());
        					$('#code').attr('disabled','disabled');
        					$('#parentCode').combotree('disable');
        					setTextboxValue('skuLevel',formatCurrency(channelType.sku.toString()));
        					setTextboxValue('saleAmount',formatCurrency(channelType.saleAmount.toString()));
        					setTextboxValue('code',channelType.channelTypeCode.toString());
        					setTextboxValue('name',channelType.channelTypeName.toString());
        					//setSelectBoxValue('parentCode', channelType.parentChannelType.id.toString());
        					setSelectBoxValue('status',status);
        					//disableSelectbox('parentCode');
        					CustomerTypeCatalog.getChangedForm();
        					CustomerTypeCatalog.isContextMenu = 0;
        					$('#divParentCode').show();
        					$('#title').html(catalog_customer_type_information);
        					$('#title1').html(catalog_customer_type_information);
        					$('#name').focus();
        					
        				}
                	});
				}
			}
        };
		if ($(node).attr('id')== '-2') {
			items.update = null;
		}
		return items;
	},
	loadTree: function(){
		//loadDataForTree('/rest/catalog/customer-type/tree.json');
		$('#tree').jstree({
	        "plugins": ["themes", "json_data","ui","contextmenu"],
	        "themes": {
	            "theme": "classic",
	            "icons": false,
	            "dots": true
	        },
	        "json_data": {
	        	"ajax" : {
	                "url" : '/rest/catalog/customer-type/tree.json',
	                "data" : function (n) {
	                        return { id : n.attr ? n.attr("id") : 0 };
	                },
	    	        'complete':function(node){
	    	            	CustomerTypeCatalog.setSelectedNode(CustomerTypeCatalog.selectnodeId);	            	
	    	        }		
	            }        	
	        },
	        "contextmenu" : {
	        	"items" : CustomerTypeCatalog.customMenu
	        }
		});	
		$('#tree').bind("loaded.jstree", function(event, data){
			if($('#treeContainer').data("jsp")!= undefined){
				$('#treeContainer').data("jsp").destroy();
			}
			$('#treeContainer').jScrollPane();
			CustomerTypeCatalog.setSelectedNode("-2");
		});
		
		$('#tree').bind('before.jstree',
			function(event, data) {
				if (data.func == 'refresh') {
					CustomerTypeCatalog.isRefresh = true;
				}
		}); 
		
		$('#tree').bind("show_contextmenu.jstree", function(obj, x,y){
			console.log(obj);
			console.log(x);
			console.log(y);
			
		});

		$('#tree').bind("select_node.jstree",
		function(event, data) {
			$('#status').html('').change();
			$('#status').append('<option selected="selected" value="-2">Tất cả</option>');
			$('#status').append('<option value="1">Hoạt động</option>');
			$('#status').append('<option value="0">Tạm ngưng </option>');
			$('#status').change();
			var name = data.inst.get_text(data.rslt.obj);
			var id = data.rslt.obj.attr("id");
			CustomerTypeCatalog.selectnodeId = id;
			$('#contentGrid').show();
			if (id == -2) {
				id = '';
			} else {
				CustomerTypeCatalog.parentsId = data.inst._get_parent(data.rslt.obj).attr("id"); // lay id node cha
			}
			if (CustomerTypeCatalog.isRefresh) {
				CustomerTypeCatalog.isRefresh = false;
				return false;
			}
			var url = CustomerTypeCatalog.getGridUrl('', id, '', 1, 1,
					'', '', 'true');
			$("#grid").datagrid({
				url : url,
				pageNumber : 1
			});
			CustomerTypeCatalog.resetForm();
			CustomerTypeCatalog.searchModel = null;
		});
	},
	clearData: function(){
		setTitleSearch();
		$('#code').css({'width':'196px'});
		$('#skuLevel').css({'width':'196px'});
		$('#saleAmount').css('width','196px');
		//$('#name').css({'width':'133px'});
		$('#title1').html(catalog_customer_type_title);
		$('.ReqiureStyle').hide();
		$('#divParentCode').hide(); //an ma cha
		$('#selId').val(0);
		$('#errMsg').html('').hide();
		$('#code').removeAttr('disabled');
		$('#parentCode').combotree('enable');
		$('#btnEdit').hide();
		//$('#btnCancel').hide();
		$('#btnSearch').show();
		//$('#btnAdd').show();		
		$('#code').val('');
		$('#name').val('');
		$('#saleAmount').val('');
		$('#skuLevel').val('');
		$('#parentCode').combotree('setValue',activeType.ALL);		
		$('#parentCode').combotree("reload",'/rest/catalog/customer-type/combotree/0/0.json');
		$('#parentCode').combotree('setValue',activeType.ALL);
		setSelectBoxValue('status',1);
	},
	getChangedForm: function(){
		$(".combo-panel .panel-body .panel-body-noheader").css({'width':'197px'});
		$(".panel .combo-p").css({'width':'201px'});
		
		if(CustomerTypeCatalog.isContextMenu == 1){
			if(CustomerTypeCatalog.parentsId == null || CustomerTypeCatalog.parentsId=='' ){
				CustomerTypeCatalog.parentsId = '-2';
			}
			$('#parentCode').combotree('setValue',CustomerTypeCatalog.parentsId);
		}
		$('#divParentCode').show();
		$('.ReqiureStyle').show();
		$('#btnSearch').hide();
		//$('#btnAdd').hide();
		$('#btnEdit').show();
		//$('#btnCancel').show();
		$('#code').focus();
		$('#code').css({'width':'196px'});
		$('#code').attr({'maxLength':'50'});
		$('#name').css({'width':'196px'});
		$('#name').attr({'maxLength':'100'});
		$('#saleAmount').css({'width':'196px'});
		$('#saleAmount').attr({'maxLength':'19'});
		$('#skuLevel').css({'width':'196px'});	
		$('#skuLevel').attr({'maxLength':'19'});	
		$('#title').html(catalog_customer_type_information);
		$('#title1').html(catalog_customer_type_information);
	},
	newCusType:function(){
		enableSelectbox('parentCode');
		$('#contentGrid').hide();
		CustomerTypeCatalog.resetForm();
		CustomerTypeCatalog.getChangedForm();
		$('#parentCode').val("'---"+ Utils.XSSEncode(catalog_customer_type_chose_customer_type) +"---'").change();
	},
	setSelectedNode:function(nodeId){
		$('#tree a').removeClass('jstree-hovered jstree-clicked');
		$('#tree #'+nodeId+' >a').addClass('jstree-clicked');
	}
};
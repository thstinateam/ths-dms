/**
 * Chỉ định nhân viên giao hàng
 */
var SPAssignTransferStaff = {
	_lstSaleOrderSelect:null,
	/**
	 * @author vuongmq
	 * @since 25 - August, 2014
	 * @description: gán mã khách hàng vào text book
	 */
	callSelectF9CustomerCode: function(index, shortCode, customerName){
		$('#customerCode').val(Utils.XSSEncode(shortCode.trim()));
		$('#common-dialog-search-2-textbox').dialog("close");
	},
	searchSaleOrderForDelivery : function() {
		$('.ErrorMsgStyle').html('').hide();
		var params = new Object();
		var shopCode = $('#cbxShop').combobox('getValue');
		var	msg = Utils.getMessageOfInvalidFormatDate('date', 'Ngày');
		if(msg.length > 0) {
			$('#errMsgInfo').html(msg).show();
			return;
		}
		msg = Utils.getMessageOfRequireCheck('date', 'Ngày');
		// vuongmq; 09/06/2015 kiem tra gia tri DH
		if (msg.length == 0) {
			msg = Utils.getMessageOfSpecialCharactersValidateEx('numberValueOrder','ĐH', Utils._TF_NUMBER_COMMA);
			$('#numberValueOrder').focus();
		}
		if(msg.length > 0) {
			$('#errMsgInfo').html(msg).show();
			return;
		}
		var orderNumber = $('#orderNumber').val().trim();
		var saleStaffCode = $('#saleStaffCode').combobox('getValue').trim();
		var transferStaffCode = $('#transferStaffCode').combobox('getValue').trim();
		var cashierStaffCode = $('#cashierStaffCode').combobox('getValue').trim();
		var carId = $('#carId').combobox('getValue');
		var date = $('#date').val();
		var customerCode = $('#customerCode').val().trim();
		var isValueOrder = $('#isValueOrder').val();
		var priorityCode = $('#priorityId').val();
		var numberOrder = $('#numberValueOrder').val().trim();
		var numberValueOrder = "";
		if ( $('#numberValueOrder').val() != undefined &&  $('#numberValueOrder').val() != null &&  $('#numberValueOrder').val() !='') {
			numberValueOrder = numberOrder.replace(/,/g, '');
		}

		var params = {shopCode: shopCode, orderNumber : orderNumber, saleStaffCode : saleStaffCode, transferStaffCode : transferStaffCode, cashierStaffCode: cashierStaffCode, carId: carId, startDate : date, customerCode : customerCode, isValueOrder: isValueOrder, numberValueOrder: numberValueOrder, priorityCode:priorityCode};
		var orderTypes = $('#orderType').val();
		if (orderTypes != null) {
			if (orderTypes instanceof Array) {
				while (orderTypes.length > 0 && isNullOrEmpty(orderTypes[0])) {
					orderTypes.splice(0, 1);
				}
				var out = {};
				convertToSimpleObject(out, orderTypes, 'orderTypes');
				$.extend(params, out);
			} else {
				params.orderTypes = orderTypes.trim();
			}
		}
//		$('#grid').datagrid('reload', params);
		SPAssignTransferStaff.bindDataGrid(params);
	},
	showMessage : function(code, codeMaxLength, name) {
		var message = " không được phép vượt quá ";
		var nullMessage = " không được phép để trống.";

		if (code.length > codeMaxLength) {
			errMsg = name + message + codeMaxLength + " ký tự.";
			$('#errMsg').html(errMsg).show();			
			return;
		}
		if (code.length <= 0 && name == "Mã NVGH") {
			nullMessage = name + nullMessage;
			$('#errMsg').html(nullMessage).show();			
			return;
		}
	},
	getListCheckbox: function() {
		var data = new Object();
		var listSaleOrderId = new Array();
		var len =  $('.datagrid-row input[type=checkbox]:checked').length;
		var selectors = $('.datagrid-row input[type=checkbox]:checked');
		for(var i=0;i<len;++i){
			var id = $(selectors[i]).val().trim();
			listSaleOrderId.push(id);
		}		
		data.listSaleOrderId = listSaleOrderId;		
		var shopCode = $('#cbxShop').combobox('getValue');
		var deliveryId = $('#listStaff').combobox('getValue');
		var cashierId = $('#listCashierStaff').combobox('getValue');
		var carId = $('#listCar').combobox('getValue');	
		data.shopCode = shopCode;
		if (!isNullOrEmpty(deliveryId) && $('#checkSelectTransferStaff').is(':checked')) {
			data.deliveryId = deliveryId;
		}
		if (!isNullOrEmpty(cashierId) && $('#checkSelectCashierStaff').is(':checked')) {
			data.cashierId = cashierId;
		}
		if (!isNullOrEmpty(carId) && $('#checkSelectCar').is(':checked')) {
			data.carId = carId;
		}
		return data;
	},
	updateAssignTransfer : function() {
		$('#errMsg').html('').hide();
		$('#errExcelMsg').hide();
		
		var paramsSaleOrderForDelivery = new Object();
		$('.ErrorMsgStyle').html('').hide();
		paramsSaleOrderForDelivery = SPAssignTransferStaff.getListCheckbox();	
		
		//var msg = '';
		if (paramsSaleOrderForDelivery.listSaleOrderId == null || paramsSaleOrderForDelivery.listSaleOrderId.length==0) {			
			$('#errMsg').html('Bạn phải chọn đơn hàng để cập nhật.').show();			
			return;
		}
		if($('#checkSelectTransferStaff').is(':checked') && (paramsSaleOrderForDelivery.deliveryId == undefined	|| paramsSaleOrderForDelivery.deliveryId == null)){
			$('#errMsg').html('Bạn chưa chọn mã NVGH.').show();			
			return;
		}
		if($('#checkSelectCashierStaff').is(':checked') && (paramsSaleOrderForDelivery.cashierId == undefined	|| paramsSaleOrderForDelivery.cashierId == null)){
			$('#errMsg').html('Bạn chưa chọn mã NVTT.').show();			
			return;
		}
		if($('#checkSelectCar').is(':checked') && (paramsSaleOrderForDelivery.carId == undefined || paramsSaleOrderForDelivery.carId == null)){
			$('#errMsg').html('Bạn chưa chọn xe.').show();			
			return;
		}
		if(paramsSaleOrderForDelivery.listSaleOrderId.length>0 &&	!$('#checkSelectTransferStaff').is(':checked') 
				&& !$('#checkSelectCashierStaff').is(':checked')
				&& !$('#checkSelectCar').is(':checked')){
			$('#errMsg').html('Bạn cần chọn nhân viên giao hàng hoặc xe ').show();
			return false;
		}
		Utils.addOrSaveData(paramsSaleOrderForDelivery,'/sale-product/assign-transfer-staff/updateassigntransfer',
				null, 'errMsg', function(data) {
					$('input[type=checkbox]').attr('checked',false);
					SPAssignTransferStaff.searchSaleOrderForDelivery();
					$('#listCar').val('');
					$('#listStaff').val('');
					$('#listCashierStaff').val('');
					disableSelectbox('listCar');
					disableSelectbox('listStaff');
					disableSelectbox('listCashierStaff');
				}, 'loadingSearch', null, null,'Bạn có muốn cập nhật đơn hàng không?');
			
		
	},
	/** vuongmq Sep 29, 2014, xuat file excel*/
	exportAssignTransfer : function(){
		$('#errMsg').html('').hide();
		$('#errExcelMsg').hide();
		var data = new Object();		
		data = SPAssignTransferStaff.getListCheckbox();
		if (data.listSaleOrderId == undefined || data.listSaleOrderId.length == 0) {
			$('#errMsg').html('Bạn chưa chọn đơn hàng để xuất.').show();
			return;
		} else {
			ReportUtils.exportReport("/sale-product/assign-transfer-staff/export", data, 'errMsg');
		}
		
		return false;
	},
	/** vuongmq Sep 29, 2014, import file excel*/
	importAssignTransfer: function(){
 		$('.ErrorMsgStyle').hide();
		VTUtilJS.importExcelUtils(function(data) {
			//$("#exGrid").treegrid("reload");
			if(data.message!=undefined && data.message!=null && data.message.trim().length >0){
				$("#errExcelMsg").html(data.message).show();
			}else{
				$('#successMsg').html("Lưu dữ liệu thành công").show();
				$('.ErrorMsgStyle').html('').hide();
				SPAssignTransferStaff.searchSaleOrderForDelivery();
				var tm = setTimeout(function(){
					//Load lai danh sach quyen
					$('#successMsg').html("").hide();
					clearTimeout(tm);
				 }, 1500);
			}
			// cai nay nguy hiem, loi se khong nhin thay message loi SPAssignTransferStaff.searchSaleOrderForDelivery(); // import thanh cong full, thanh công 1 phần diều load lai HAM SEARCH
		}, "importFrm", "excelFile", 'isView', "errExcelMsg");
	},
	initInfo: function() {
		Utils.initUnitCbx('cbxShop', {}, null, function(){
			SPAssignTransferStaff.handleSelectShop();
		});
		var ti = setTimeout(function() {
			SPAssignTransferStaff.searchSaleOrderForDelivery();
			SPAssignTransferStaff.disableUnderCbx();
			
			$('#checkSelectTransferStaff').click(function() {			
				if(!$(this).is(':checked')){
					$('#listStaff').val('');
					disableCombo('listStaff');	
					
				}else{
					enableCombo('listStaff');
				}
			});		
			$('#checkSelectCashierStaff').click(function() {			
				if(!$(this).is(':checked')){
					$('#listCashierStaff').val('');
					disableCombo('listCashierStaff');				
				}else{
					enableCombo('listCashierStaff');
				}
			});		
			$('#checkSelectCar').click(function() {			
				if(!$(this).is(':checked')){
					$('#listCar').val('');
					disableCombo('listCar');				
				}else{
					enableCombo('listCar');				
				}
			});
		}, 3000);
	},
	disableUnderCbx: function() {
		/*disableSelectbox('listCar');
		disableSelectbox('listStaff');
		disableSelectbox('listCashierStaff');*/
		
		disableCombo('listCar');
		disableCombo('listStaff');
		disableCombo('listCashierStaff');
		
	},
	handleSelectShop: function() {
		/*$('#saleStaffCode').focus();
		Utils.bindComboboxStaffEasyUI('saleStaffCode');
		Utils.bindComboboxStaffEasyUI('transferStaffCode');
		Utils.bindComboboxStaffEasyUI('cashierStaffCode');*/
		var params= {};
		params.shopCode = $('#cbxShop').combobox('getValue');
		Utils.getJSONDataByAjax(params, '/sale-product/assign-transfer-staff/lst-staff-cbx' , function(data) {
			if (data != null) {
				$('#strLockDate').html(data.lockDate);
				$('#date').val(data.lockDate);
				Utils.bindStaffCbx('#saleStaffCode', data.lstSale, null, 206);
				Utils.bindStaffCbx('#transferStaffCode', data.lstDeliver, null, 206);
				Utils.bindStaffCbx('#cashierStaffCode', data.lstCashier, null, 206);
				Utils.bindCarCbx('#carId', data.lstCar, null, 206);				
				//combobox under
				Utils.bindStaffCbxId('#listStaff', jQuery.extend(true, [], data.lstDeliver), null, 206);
				Utils.bindStaffCbxId('#listCashierStaff', jQuery.extend(true, [], data.lstCashier), null, 206);
				Utils.bindCarCbx('#listCar', jQuery.extend(true, [], data.lstCar), null, 206);
				
				if(!$('#checkSelectTransferStaff').is(':checked')){
					$('#listStaff').val('');
					disableCombo('listStaff');	
					
				}else{
					enableCombo('listStaff');
				}
				
				if(!$('#checkSelectCashierStaff').is(':checked')){
					$('#listCashierStaff').val('');
					disableCombo('listCashierStaff');				
				}else{
					enableCombo('listCashierStaff');
				}
				
				if(!$('#checkSelectCar').is(':checked')){
					$('#listCar').val('');
					disableCombo('listCar');				
				}else{
					enableCombo('listCar');				
				}
			}
		});
		
	},
	bindDataGrid: function(par) {
		var wd = $("#gridContainer").width() -5;		
		$('#grid').datagrid({  
		    url:'/sale-product/assign-transfer-staff/search',
	        rownumbers : true,
		    pagination : true,
	        pageNumber : 1,
	        scrollbarSize: 0,
	        autoWidth: true,
	        pageList: [10, 20, 30, 50, 100, 200, 500],
	        autoRowHeight : true,
	        fitColumns : (wd > 1440),
		    //selectOnCheck:false,
		    singleSelect: false,
		    scrollbarSize: 0,
		    autoRowHeight : true,
		    width: wd,
		    queryParams: par,
		    frozenColumns:[[
				{field:'id', checkbox:true, width:50, align:'center'},      
				{field:'orderNumber', title:'Số đơn hàng', width:120, align:'left', formatter: function(value,row,index){
					return VTUtilJS.XSSEncode(row.orderNumber);
				}},
				{field:'orderDate', title:'Ngày', width:70, align:'left', formatter: function(value, row, index) {
					return formatDate(row.orderDate);
				}},
				{field:'customer', title:'Khách hàng', width:180, align: 'left', formatter : function(value,row,index) {
					if(row.customer != null) {
						return VTUtilJS.XSSEncode(row.customer.shortCode + ' - ' + row.customer.customerName);
					} else {
						return '';
					}
				}},
				{field:'total', title:'Số tiền', width:100, align:'right', formatter : function(value,row,index) {
		        	if(row.total != null) {
		        		return formatCurrencyInterger(row.total);
		        	} else {
		        		return 0;
		        	}
		        }}
			]],               
		    columns:[[
				{field:'staff.staffCode', title:'NVBH', width:140, align:'left',formatter:function(value,row,index){
					if(row.staff!=null){
						return VTUtilJS.XSSEncode(row.staff.staffCode + ' - ' + row.staff.staffName);
					}
					return '';
				}},
				{field:'delivery.staffCode', title:'NVGH', width:140, align:'left',formatter:function(value,row,index){
					if(row.delivery!=null){
						return VTUtilJS.XSSEncode(row.delivery.staffCode+ ' - ' + row.delivery.staffName);
					}
					return '';
				}},
				{field:'cashier.staffCode', title:'NVTT', width:140, align:'left',formatter:function(value,row,index){
					if(row.cashier!=null){
						return VTUtilJS.XSSEncode(row.cashier.staffCode+ ' - '+ row.cashier.staffName);
					}
					return '';
				}},
		        {field:'address', title:'Địa chỉ', width:300, align: 'left', formatter : function(value,row,index) {
		        	if(row.customer != null) {
		        		return VTUtilJS.XSSEncode(row.customer.address);
		        	} else {
		        		return '';
		        	}
		        }},		        
		        {field:'car.carNumber', title:'Xe GH', width:100, align:'left',formatter:function(value,row,index){
		        	if(row.car!=null)
		        		return VTUtilJS.XSSEncode(row.car.carNumber);
		        	return '';
		        }},		        
		        {field:'totalWeight', title:'Trọng lượng', width:100, align:'right',formatter:CommonFormatter.numberFormatter}
		    ]],
		    onLoadSuccess :function(data){
		    	$('.datagrid-header-rownumber').html('STT');
			   	$('.datagrid-header-row td div').css('text-align','center');		   	
			   	 updateRownumWidthForJqGrid('.easyui-dialog');
		   	 	$(window).resize();
		   	 	$(".datagrid-header-check input[type=checkbox]").removeAttr("checked");
		    }		    
		});
	}
	
	

};
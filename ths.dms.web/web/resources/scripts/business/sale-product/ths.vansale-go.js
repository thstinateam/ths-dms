/*
 * Copyright YYYY Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/**
 * Lap don tra hang vansale
 * 
 * @author lacnv1
 */
var VansaleGO = {
	_curSaler: null,
	_pageLocked: false,
	_mapProducts: new Map(),
	_lstProducts: [],
	_currentShopCode: '',
	/**
	 * load trang
	 * 
	 * @author lacnv1
	 */
	loadPage: function() {
		var gridData = {
			total: 1,
			rows: [{}],
			footer: [{isFooter: true, weight:0, amount:0}]
		};
		VansaleGO.initGrid(gridData);
		VansaleGO.loadComboboxShop();
	},
	
	/**
	 * Khoi tao datagrid
	 * 
	 * @author lacnv1
	 */
	initGrid: function(gridData) {
		$("#dgrid").datagrid({
			url: "",
			data: gridData,
			width: $("#gridContainer").width() - 10,
			height: "auto",
			scrollbarSize: 0,
			singleSelect: true,
			checkOnSelect: false,
			selectOnCheck: false,
			rownumbers: true,
			fitColumns: true,
			showFooter: true,
			columns: [[
			    {field: "productCode", title:"Mã sản phẩm", width: 100, sortable:false, resizable:false, align: "left", formatter: function(v, r, i) {
			    	if (r.isFooter) {
			    		return "";
			    	}
			    	return VTUtilJS.XSSEncode(v);
			    }, editor: {
			    	type: "combobox",
			    	options: {
				    	valueField: 'productCode',
						textField: 'productCode',
						//panelWidth: 110,
						formatter: function(r) {
							var productName = r['productName'];
							return "<div style='white-space: nowrap;overflow:hidden;text-overflow: ellipsis;'>" + Utils.XSSEncode(productName) + "</div>";
						},
						onHidePanel: function() {
							$(".ErrorMsgStyle").hide();
							VansaleGO.chooseProduct();
						},
						filter: function(q, row){
							q = new String(q).toUpperCase().trim();
							return row['productCode'].indexOf(q)>=0;
						}
			    	}
			    }},
				{field: "productName", title:"Tên sản phẩm", width: 200, sortable:false, resizable:false, align: "left", formatter: function(v, r, i) {
			    	if (r.isFooter) {
			    		return "";
			    	}
			    	return VTUtilJS.XSSEncode(v);
			    }},
				{field: "warehouseId", title:"Kho", width: 150, sortable:false, resizable:false, align: "left", formatter: function(v, r, i) {
			    	if (r.isFooter) {
			    		return "<b style='float:right;'>Trọng lượng</b><div class='Clear'></div>";
			    	}
			    	return VTUtilJS.XSSEncode(r.warehouseName);
			    }, editor: {
			    	type: "combobox",
			    	options: {
			    		valueField: 'warehouseId',
						textField: 'warehouseName',
						formatter: function(r) {
							return Utils.XSSEncode(r['warehouseName']);
						},
						editable: false,
						onHidePanel: function() {
							$(".ErrorMsgStyle").hide();
							VansaleGO.chooseWarehouse();
						}
			    	}
			    }},
				{field: "quantity", title:"Số lượng", width: 90, fixed:true, sortable:false, resizable:false, align: "right", formatter: function(v, r, i) {
					if (r.isFooter) {
			    		return "<b>" + formatFloatValue(r.weight, 2) + " kg</b>";
			    	}
					if (VansaleGO._pageLocked) {
						return "";
					}
					var inp = '<input type="text" id="qtt'+i+'" class="InputTextStyle qttTextBox" style="margin:0;width:75px;text-align:right;" \
							maxlength="10" value="{0}" onchange="VansaleGO.changeQuantity('+i+');" onkeypress="return VansaleDP.quantityTextBoxKeypress(event);" />';
					if (v != undefined && v != null) {
						inp = inp.replace("{0}", formatQuantity(v, r.convfact));
					} else {
						inp = inp.replace("{0}", "");
					}
					return inp;
			    }},
				{field: "price", title:"Giá bán lẻ", width: 100, sortable:false, resizable:false, align: "right", formatter: function(v, r, i) {
					if (r.isFooter) {
			    		return "<b>Tổng tiền</b>";
			    	}
					return formatCurrency(v);
			    }},
				{field: "amount", title:"Thành tiền", width: 120, sortable:false, resizable:false, align: "right", formatter: function(v, r, i) {
					if (r.isFooter) {
			    		return "<b>" + formatCurrency(v) + " VNĐ</b>";
			    	}
					return formatCurrency(v);
			    }},
				{field: "del", title:"", width: 35, fixed:true, sortable:false, resizable:false, align: "center", formatter: function(v, r, i) {
					if (r.isFooter) {
			    		return "";
			    	}
					return '<a id="del'+i+'" href="javascript:void(0);" class="delA" onclick="VansaleGO.removeProduct('+i+');"><img width="15" height="15" alt="del" src="/resources/images/icon_delete.png" title="Xóa" /></a>';
			    }}
			]],
			onLoadSuccess: function(data) {
				$('.datagrid-header-rownumber').html('STT');
				GridUtil.updateDefaultRowNumWidth("#gridContainer", "dgrid");
				VansaleGO.refreshFooter();
				if (VansaleGO._pageLocked) {
					$(".delA").removeAttr("onclick");
				}
			},
			onSelect: function(i) {
				if (VansaleGO._pageLocked) {
					return;
				}
				if (VansaleGO._editIdx != i) {
					if (VansaleGO.endEditing()) {
						$('#dgrid').datagrid('beginEdit', i);
						VansaleGO._editIdx = i;
					}
					VansaleGO.loadOptionsForRow();
				}
			}
		});
	},
	
	/**
	 * Chon sp trong combobox
	 * 
	 * @author lacnv1
	 */
	chooseProduct: function(qtt) {
		var rs = $("#dgrid").datagrid("getRows");
		if (rs.length > VansaleGO._editIdx) {
			var r = rs[VansaleGO._editIdx];
			var cbs = $("#dgrid").datagrid("getEditors", VansaleGO._editIdx);
			var v = $(cbs[0].target).combobox("getValue").split(" - ")[0];
			if (v !== r.productCode) {
				var arrp = VansaleGO._mapProducts.get(v);
				if (arrp && arrp.length > 0) {
					var idx = 0;
					var b = true;
					for (var sz = arrp.length; idx < sz;) {
						b = true;
						for (var i = 0, szi = rs.length; i < szi; i++) {
							if (i !== VansaleGO._editIdx && rs[i].productCode === v && rs[i].warehouseId === arrp[idx].warehouseId) {
								idx++;
								b = false;
								break;
							}
						}
						if (b) {
							break;
						}
					}
					if (idx >= arrp.length) {
						$("#errMsg").html("Bạn đã chọn tất cả các kho của sản phẩm <b><i>" + Utils.XSSEncode(v) + "</i></b>").show();
						$(cbs[0].target).combobox("setValue", "");
						return;
					}
					r.productId = arrp[idx].productId;
					r.productCode = v;
					r.productName = arrp[idx].productName;
					r.warehouseName = arrp[idx].warehouseName;
					r.warehouseId = arrp[idx].warehouseId;
					$(cbs[1].target).combobox("loadData", [].concat(arrp));
					$(cbs[1].target).combobox("setValue", arrp[idx].warehouseId);
					r.approvedQuantity = arrp[idx].approvedQuantity;
					r.price = arrp[idx].price;
					if (qtt > 0) {
						r.quantity = qtt;
						r.amount = r.quantity * r.price;
					} else {
						r.quantity = null;
						r.amount = 0;
					}
					r.convfact = arrp[idx].convfact;
					r.grossWeight = arrp[idx].grossWeight;
					$('#dgrid').datagrid('endEdit', VansaleGO._editIdx);
					$('#dgrid').datagrid('beginEdit', VansaleGO._editIdx);
					VansaleGO.loadOptionsForRow();
					$("#qtt"+VansaleGO._editIdx).focus();
					if (rs.length > 0 && (rs.length - 1) == VansaleGO._editIdx && rs[rs.length - 1].productCode) {
						$('#dgrid').datagrid("appendRow", {});
					}
				} else {
					$(cbs[0].target).combobox("setValue", Utils.XSSEncode(r.productCode));
				}
			}
		}
	},
	
	/**
	 * Chon sp trong combobox
	 * 
	 * @author lacnv1
	 */
	chooseWarehouse: function() {
		var rs = $("#dgrid").datagrid("getRows");
		if (rs.length > VansaleGO._editIdx) {
			var r = rs[VansaleGO._editIdx];
			var cbs = $("#dgrid").datagrid("getEditors", VansaleGO._editIdx);
			var pCode = $(cbs[0].target).combobox("getValue");
			var v = Number($(cbs[1].target).combobox("getValue"));
			if (v !== r.warehouseId) {
				var rs = $("#dgrid").datagrid("getRows");
				for (var i = 0, sz = rs.length - 1; i < sz; i++) {
					if (i == VansaleGO._editIdx) {
						continue;
					}
					if (pCode == rs[i].productCode && v == rs[i].warehouseId) {
						$(cbs[1].target).combobox("setValue", r.warehouseId);
						$("#errMsg").html("Sản phẩm <b><i>" + Utils.XSSEncode(pCode) + "</i></b> đã chọn kho <b><i>" + Utils.XSSEncode(rs[i].warehouseName) + "</i></b>").show();
						return;
					}
				}
				
				var arrp = VansaleGO._mapProducts.get(pCode);
				if (arrp && arrp.length > 0) {
					var i = 0;
					for (var sz = arrp.length; i < sz; i++) {
						if (arrp[i].warehouseId === v) {
							break;
						}
					}
					r.warehouseName = arrp[i].warehouseName;
					r.warehouseId = arrp[i].warehouseId;
					r.approvedQuantity = arrp[i].approvedQuantity;
					$('#dgrid').datagrid('endEdit', VansaleGO._editIdx);
					$('#dgrid').datagrid('beginEdit', VansaleGO._editIdx);
					VansaleGO.loadOptionsForRow();
					$("#qtt"+VansaleGO._editIdx).focus();
				}
			}
		}
	},
	
	/**
	 * Lay ds sp cho combobox cua dong dang sua
	 * 
	 * @author lacnv1
	 */
	loadOptionsForRow: function() {
		var cbs = $("#dgrid").datagrid("getEditors", VansaleGO._editIdx);
		$(cbs[0].target).combobox("loadData", VansaleGO.getProductData());
		var r = $("#dgrid").datagrid("getRows")[VansaleGO._editIdx];
		if (r && r.productCode) {
			var arrp = VansaleGO._mapProducts.get(r.productCode);
			$(cbs[1].target).combobox("loadData", [].concat(arrp));
		}
	},
	
	/**
	 * Lay ds sp ban cho combobox
	 * 
	 * @author lacnv1
	 */
	getProductData: function() {
		var rows = VansaleGO._lstProducts;
		lst = [];
		for (i = 0, sz = rows.length; i < sz; i++) {
			var p = VansaleGO._mapProducts.get(rows[i]);
			lst.push({productCode: rows[i], productName: rows[i] + " - " + (p[0].productName)});
		}
		return lst;
	},
	
	/**
	 * Tim kiem
	 * 
	 * @author lacnv1
	 */
	search: function() {
		$("#loadingProduct").show();
		$("#GONumber").val("");
		$("#btnSave").removeAttr("disabled");
		$("#btnSave").attr("onclick", "VansaleGO.saveOrder();");
		$("#btnSave").removeClass("BtnGeneralDStyle");
		VansaleGO._editIdx = undefined;
		var gridData = {
				footer: [{isFooter: true, weight:0, amount:0}]
			};
		var params = new Object();
		params.staffCode = $("#saler").combobox("getValue").trim();
		params.shopCodeFilter = VansaleGO._currentShopCode; 
		VansaleGO._curSaler = params.staffCode;
		$('#lstDP').val('');
		$('#lstDPView').html('');
		$('#lstDPViewDiv').hide();
		Utils.getJSONDataByAjaxNotOverlay(params, "/sale-product/vansale/search-go", function(data) {
			if (!data.error) {
				if(data.lstDP != undefined && data.lstDP != null && data.lstDP != ''){
					$('#lstDPViewDiv').show();
					$('#lstDP').val(data.lstDP);// dùng để lưu
					$('#lstDPView').html(data.lstDP);// dùng để view
				}
				var r = null;
				var i, sz;
				VansaleGO._lstProducts = [];
				VansaleGO._mapProducts = new Map();
				
				var rows = data.rows;
				var rs = [];
				var r1 = null;
				for (i = 0, sz = rows.length; i < sz; i++) {
					r = rows[i];
					r.approvedQuantity = r.quantity;
					if (VansaleGO._lstProducts.indexOf(r.productCode) < 0) {
						VansaleGO._lstProducts.push(r.productCode);
						VansaleGO._mapProducts.put(r.productCode, [r]);
						
						r1 = {productId: r.productId, productCode: r.productCode,
								productName: r.productName, warehouseId: r.warehouseId,
								warehouseName: r.warehouseName, quantity: r.quantity, approvedQuantity: r.quantity,
								price: r.price, convfact: r.convfact, grossWeight: r.grossWeight};
						r1.amount = r.quantity * r.price;
						rs.push(r1);
					} else {
						VansaleGO._mapProducts.get(r.productCode).push(r);
					}
				}
				
				rs.push({});
				gridData.rows = rs;
				gridData.total = data.total;
				$("#dgrid").datagrid("loadData", gridData);
				$("#loadingProduct").hide();
				$("#btnSave").removeAttr("disabled");
				$("#btnSave").removeClass("BtnGeneralDStyle");
			} else {
				$("#errMsg").html(data.errMsg).show();
				gridData.rows = [];
				gridData.total = 0;
				$("#dgrid").datagrid("loadData", gridData);
				$("#loadingProduct").hide();
				$("#btnSave").attr("disabled", "disabled");
				$("#btnSave").addClass("BtnGeneralDStyle");
			}
		});
	},
	
	/**
	 * Luu don hang
	 * 
	 * @author lacnv1
	 */
	saveOrder: function() {
		$(".ErrorMsgStyle").hide();
		
		var shopCode = VansaleGO._currentShopCode;
		if (shopCode == null || shopCode.length == 0) {
			$("#errMsg").html("Bạn chưa chọn giá trị cho trường Đơn vị").show();
			$("#shop + .combo .combo-text").focus();
			return;
		}
		
		var staffCode = VansaleGO._curSaler;
		if (staffCode == null || staffCode.length == 0) {
			$("#errMsg").html("Bạn chưa chọn giá trị cho trường Mã NV").show();
			$("#saler + .combo .combo-text").focus();
			return;
		}
		
		var rs = $("#dgrid").datagrid("getRows");
		if (rs.length == 0 || (rs.length == 1 && !rs[0].productCode)) {
			$("#errMsg").html("Bạn chưa chọn mặt hàng nào").show();
			return;
		}
		var lst = [];
		var lstCode = [];
		var qtt = 0;
		for (var i = 0, sz = rs.length - 1; i < sz; i++) {
			if (!rs[i].productCode) {
				continue;
			}
			if (VansaleGO._lstProducts.indexOf(rs[i].productCode) < 0) {
				$("#errMsg").html("Chỉ trả hàng cho sản phẩm còn lại trong kho nhân viên").show();
				$("#dgrid").datagrid("unselectAll");
				return;
			}
			if (!rs[i].warehouseId) {
				$("#errMsg").html("Bạn chưa chọn kho cho sản phẩm").show();
				$("#dgrid").datagrid("selectRow", i);
				return;
			}
			for (j = 0; j < i; j++) {
				if (!rs[j].productCode) {
					continue;
				}
				if (rs[i].productCode == rs[j].productCode && rs[i].warehouseId == rs[j].warehouseId) {
					$("#errMsg").html("Sản phẩm <b><i>" + Utils.XSSEncode(rs[i].productCode) + "</i></b> với kho <b><i>" + Utils.XSSEncode(rs[i].warehouseName) + "</i></b> có nhiều hơn 1 dòng").show();
					$("#dgrid").datagrid("selectRow", i);
					return;
				}
			}
			if (!rs[i].quantity) {
				$("#errMsg").html("Bạn chưa nhập số lượng cho sản phẩm").show();
				$("#dgrid").datagrid("selectRow", i);
				$("#qtt"+i).focus();
				return;
			}
			if (rs[i].quantity <= 0) {
				$("#errMsg").html("Số lượng phải lớn hơn 0").show();
				$("#dgrid").datagrid("selectRow", i);
				$("#qtt"+i).focus();
				return;
			}
			qtt = rs[i].quantity;
			for (var j = 0, szj = rs.length; j < szj; j++) {
				if (j !== i && rs[j].productCode === rs[i].productCode) {
					qtt = qtt + rs[j].quantity;
				}
			}
			if (qtt != rs[i].approvedQuantity) {
				$("#errMsg").html("Tổng số lượng sản phẩm <b><i>"+Utils.XSSEncode(rs[i].productCode)+"</i></b> phải bằng với số lượng còn lại trong kho NV").show();
				$("#dgrid").datagrid("selectRow", i);
				$("#qtt"+i).focus();
				return;
			}
			
			lst.push({productId: rs[i].productId, warehouseId: rs[i].warehouseId,
						quantity: rs[i].quantity, price: rs[i].price});
			lstCode.push(rs[i].productCode);
		}
		
		var c = 0;
		for (var i = 0, sz = VansaleGO._lstProducts.length; i < sz; i++) {
			if (lstCode.indexOf(VansaleGO._lstProducts[i]) > -1) {
				c++;
			}
		}
		if (c !== VansaleGO._lstProducts.length) {
			$("#errMsg").html("Bạn phải trả tất cả các mặt hàng còn lại trong kho NV").show();
			$("#dgrid").datagrid("unselectAll");
			return;
		}
		var lstDP = $('#lstDP').val();
		if(lstDP != ''){
			lstDP = lstDP.split(', ');
		}
		var dataModel = {shopCodeFilter:shopCode, staffCode:staffCode, lstProducts:lst, lstDP: lstDP};
		JSONUtil.saveData2(dataModel, "/sale-product/vansale/save-go-order", "Bạn có muốn lưu thông tin này ?", "errMsg", function(data) {
			$("#btnSave").attr("disabled", "disabled");
			$("#btnSave").removeAttr("onclick");
			$("#btnSave").addClass("BtnGeneralDStyle");
			$("#GONumber").val(Utils.XSSEncode(data.stockTransCode));
		}, null, function(data) {
			data.errMsg = data.errMsg.replace(/\[b\]/g, "<b><i>");
			data.errMsg = data.errMsg.replace(/\[\/\]/g, "</i></b>");
			$("#errMsg").html(data.errMsg).show();
		});
	},
	
	/**
	 * Load lai footer cua grid
	 * 
	 * @author lacnv1
	 */
	refreshFooter: function() {
		var rs = $("#dgrid").datagrid("getRows");
		var r = null;
		var weight = 0;
		var amount = 0;
		for (var i = 0, sz = rs.length; i < sz; i++) {
			r = rs[i];
			if (r.productCode && r.quantity) {
				weight = weight + Number(r.quantity) * r.grossWeight;
				amount = amount + r.amount;
			}
		}
		var ft = $("#dgrid").datagrid("getData").footer[0];
		ft.weight = weight;
		ft.amount = amount;
		$("#dgrid").datagrid("reloadFooter");
	},
	
	/**
	 * Ket thuc chinh sua grid
	 * 
	 * @author lacnv1
	 */
	endEditing: function() {
		if (VansaleGO._editIdx == undefined) {
			return true;
		}
		if ($('#dgrid').datagrid('validateRow', VansaleGO._editIdx)) {
			$('#dgrid').datagrid('endEdit', VansaleGO._editIdx);
			VansaleGO._editIdx = undefined;
			return true;
		} else {
			return false;
		}
	},
	
	/**
	 * Cap nhat lai id control theo index cua cac dong
	 * 
	 * @author lacnv1
	 */
	updateIndex: function() {
		var i = 0;
		$(".qttTextBox").each(function() {
			$(this).attr("id", "qtt"+i);
			$(this).attr("onchange", "VansaleGO.changeQuantity("+i+");");
			i++;
		});
		i = 0;
		$(".delA").each(function() {
			$(this).attr("id", "del"+i);
			$(this).attr("onclick", "VansaleGO.removeProduct("+i+");");
			i++;
		});
	},
	
	/**
	 * Xoa sp
	 * 
	 * @author lacnv1
	 */
	removeProduct: function(i) {
		$.messager.confirm("Xác nhận", "Bạn có muốn xóa sản phẩm khỏi danh sách bán?", function(y) {
			if (y) {
				var rs = $("#dgrid").datagrid("getRows");
				if (i < rs.length - 1) {
					$("#dgrid").datagrid("deleteRow", i);
					VansaleGO.endEditing();
					VansaleGO.updateIndex();
					VansaleGO.refreshFooter();
				}
			}
		});
	},
	
	/**
	 * Thay doi so luong ban sp
	 * 
	 * @author lacnv1
	 */
	changeQuantity: function(i) {
		VansaleGO._idxTmp = null; // dung de bat enter
		$(".ErrorMsgStyle").hide();
		var rs = $("#dgrid").datagrid("getRows");
		if (rs && rs.length > i) {
			var v1 = getQuantity($("#qtt"+i).val().trim(), rs[i].convfact);
			if (isNaN(v1)) {
				rs[i].quantity = null;
			} else {
				rs[i].quantity = v1;
			}
			if (rs[i].productCode && rs[i].price) {
				rs[i].amount = Number(rs[i].quantity) * rs[i].price;
				VansaleGO.endEditing();
			}
		}
		VansaleGO.refreshFooter();
		VansaleGO._idxTmp = i; // dung de bat enter
	},
	initGridView: function(orderId, shopCode) {
		$("#dgrid").datagrid({
			url: '/commons/get-order-detail',
			width: $("#gridContainer").width() - 10,
			height: "auto",
			scrollbarSize: 0,
			singleSelect: true,
			checkOnSelect: false,
			selectOnCheck: false,
			rownumbers: true,
			fitColumns: true,
			showFooter: true,
			queryParams:{orderId:orderId, shopCodeFilter: shopCode},
			columns: [[
			    {field: "productCode", title:"Mã sản phẩm", width: 100, sortable:false, resizable:false, align: "left", formatter: function(v, r, i) {
			    	if (r.isFooter) {
			    		return "";
			    	}
			    	return VTUtilJS.XSSEncode(v);
			    }},
				{field: "productName", title:"Tên sản phẩm", width: 200, sortable:false, resizable:false, align: "left", formatter: function(v, r, i) {
			    	if (r.isFooter) {
			    		return "";
			    	}
			    	return VTUtilJS.XSSEncode(v);
			    }},
				{field: "warehouseId", title:"Kho", width: 150, sortable:false, resizable:false, align: "left", formatter: function(v, r, i) {
			    	if (r.isFooter) {
			    		return "<b>Trọng lượng</b>";
			    	}
			    	return VTUtilJS.XSSEncode(r.warehouseName);
			    }},
				{field: "quantity", title:"Số lượng", width: 90, fixed:true, sortable:false, resizable:false, align: "right", formatter: function(v, r, i) {
					if (r.isFooter) {
			    		return "<b>" + formatFloatValue(r.weight, 2) + " kg</b>";
			    	}
					var inp = '';
					if (v != undefined && v != null) {
						inp = formatQuantity(v, r.convfact);
					}
					return inp;
			    }},
				{field: "price", title:"Giá bán", width: 100, sortable:false, resizable:false, align: "right", formatter: function(v, r, i) {
					if (r.isFooter) {
			    		return "<b>Tổng tiền</b>";
			    	}
					return formatCurrency(v);
			    }},
				{field: "amount", title:"Thành tiền", width: 120, sortable:false, resizable:false, align: "right", formatter: function(v, r, i) {
					if (r.isFooter) {
			    		return "<b>" + formatCurrency(v) + " VNĐ</b>";
			    	}
					return formatCurrency(v);
			    }}
			]],
			onLoadSuccess: function(data) {
				$('.datagrid-header-rownumber').html('STT');
				GridUtil.updateDefaultRowNumWidth("#gridContainer", "dgrid");
			}
		});
	},
	loadPageView:function(id, shopCode){
		VansaleGO.initGridView(id, shopCode);
	},
	//load combobox don vi
	loadComboboxShop: function() {
		var params = {};
		$.ajax({
			type : "POST",
			url:'/commons/list-child-shop',
			data :($.param(params, true)),
			dataType : "json",
			success : function(data) {
				$('#shop').combobox({
					valueField: 'shopCode',
					textField:  'shopName',
					data: data.rows,
					panelWidth: '206',
					formatter: function(row) {
						return '<span style="font-weight:bold">' + Utils.XSSEncode(row.shopCode) + '</span><br/>' + '<span style="color:#888">' + Utils.XSSEncode(row.shopName) + '</span>';
					},
					filter: function(q, row){
						q = new String(q).toUpperCase().trim();
						var opts = $(this).combobox('options');
						return ((unicodeToEnglish(row[opts.textField]).indexOf(unicodeToEnglish(q)) == 0) || unicodeToEnglish(row[opts.valueField]).indexOf(unicodeToEnglish(q)) == 0) ;
					},	
			        onSelect: function(rec){
			        	if (rec != null && rec.shopCode != VansaleGO._currentShopCode){
			        		VansaleGO._currentShopCode = rec.shopCode;
			        		VansaleGO.changeShop();
			        	}
			        },
			        onLoadSuccess: function(){
			        	var arr = $('#shop').combobox('getData');
			        	if (arr != null && arr.length > 0){			        		
			        		$('#shop').combobox('select', arr[0].shopCode);
			        		VansaleGO._currentShopCode = arr[0].shopCode;
			        	}
			        }
				});
			}
		});	
	},
	changeShop: function(){
		var params = new Object();	
		if(VansaleGO._currentShopCode != null){
			params.shopCodeFilter = VansaleGO._currentShopCode;
		}					
		var kData = $.param(params, true);
		 $.ajax({
			type : "POST",
			url : '/sale-product/vansale/change-shop',
			data : (kData),
			dataType : "json",
			success : function(data) {
				$("#saler").combobox({
					valueField: 'staffCode',
					textField: 'staffName',
					data: data.lstStaff,
					panelWidth: 206,
					formatter: function(row) {
						return '<span style="font-weight:bold">' + Utils.XSSEncode(row.staffCode) + '</span><br/>' + '<span style="color:#888">' + Utils.XSSEncode(row.staffName) + '</span>';
					},
					filter: function(q, row){
						q = new String(q).toUpperCase().trim();
						var opts = $(this).combobox('options');
						return ((unicodeToEnglish(row[opts.textField]).indexOf(unicodeToEnglish(q)) == 0) || unicodeToEnglish(row[opts.valueField]).indexOf(unicodeToEnglish(q)) == 0) ;
					},
					onSelect: function(r) {
						$("#salerName").val(Utils.XSSEncode(r.staffName));
					}
				});
				$("#saler + .combo .combo-text").focus();
				
				$('#fDate').val(data.lockDate);
				
				$("#errMsg").html('').hide();
				$('td[field=quantity] .qttTextBox').live('keypress', function(event) {
					if (event.keyCode == keyCodes.ENTER) {
						var rs = $("#dgrid").datagrid("getRows");
						var r = rs[VansaleGO._editIdx];
						if (formatQuantity(r.quantity, r.convfact) == formatQuantity($(this).val().trim(), r.convfact)) {
							if (VansaleGO._editIdx < rs.length-1) {
								$("#dgrid").datagrid("selectRow", VansaleGO._editIdx+1);
								var cbs = $("#dgrid").datagrid("getEditors", VansaleGO._editIdx);
								$(cbs[0].target).next().find(".combo-text").focus();
							}
						} else {
							VansaleGO._itv = setInterval(function() {
								if (VansaleGO._idxTmp != null && VansaleGO._idxTmp != undefined) {
									if (VansaleGO._idxTmp < rs.length-1) {
										$("#dgrid").datagrid("selectRow", VansaleGO._idxTmp+1);
										var cbs = $("#dgrid").datagrid("getEditors", VansaleGO._idxTmp+1);
										$(cbs[0].target).next().find(".combo-text").focus();
									}
									VansaleGO._idxTmp = null;
									clearInterval(VansaleGO._itv);
								}
							}, 100);
						}
					}
				});
				
			},
			error:function(XMLHttpRequest, textStatus, errorThrown) {
				console.log("Error");
			}
		});
	}
};
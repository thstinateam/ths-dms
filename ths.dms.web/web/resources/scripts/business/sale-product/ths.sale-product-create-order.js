/**
 * vnm.sale-product-create-order.js
 * File js xu ly cac thao tac lien quan den chuc nang tao don hang , tinh tien KM va import don hang
 * Gom cac bien chung
 * 	- SPCreateOrder : xu ly cac thao tac lien quan tao don hang ( tinh tien , tao don hang , import ,..)
 *  - GridSaleBusiness : khoi tao grid don hang ban
 *  - COLSALE : luu thu tu cac cot trong don hang ban
 *  - ERROR  : cac thong tin ma loi validate san pham
 *  - PromotionType : thong tin loai hinh KM
 */
var SPCreateOrder = {
	_errorCommercialSupport:0,
	_isViewOrder: false,
	_xhrSave : null,
	_xhrDel : null,
	_mapProduct:null,//map luu cac sp khi F9 chon sp ban
	_mapCommercialSupport:null,//map luu CTHTTM hoac CTKM khi F9 chon sp ban
	_mapDiscountAmountOrder:new Map(),
	_mapPromoDetail:null,
	_mapPromoDetailForOrder:null,
	_mapDiscountAmount:new Map(),
	_mapDiscountPercent:new Map(),
	_mapHasPromotion:new Map(),
	_lstKeyShop: new Array(),
	_lstDiscountKeyShop:new Map(),
	_lstProgramDatSaleProduct:new Array(),
	_lstProduct:null,
	_lstPromotionProductIsAuto:null,
	_lstPromotionProductForOrderIsAuto:new Map(),
	_lstPromotionProductForOrder:null,
	_lstPromotionOpenProduct: null,
	_saleOrderQuantityReceivedList:null,
	_promotionOrderReceivedList:null,
	_promotionPortionReceivedList:null,
	_lstDiscount:null,
	_lstDisper:null,
	_lstPromotionProductIsAutoChange:null,
	_currentCustomerCode : '',
	_currentSaleStaff : '',
	_currentShopCode : '',
	_isUsingZVOrder : 0,
	_isUsingZVCode : "",
	_programes: new Array(),
	_PRODUCTS : new Map(),
	_EQUIPMENT : new Map(),
	_mapPRODUCTSbyWarehouse: new Map(),
	_confirmQuantityAcc: true,
	_currentCustomerCodeFromSearch :'',
	_isAllowShowPrice : true,
	_isAllowModifyPrice : false,//true
	_shopCodeFilter: "",
	_lockDate:'',
	_isLoadGridDiscountDetail:1,
	_SALE_ORDER_PROMOTION_OBJECT_SHOP: 1,
	_SALE_ORDER_PROMOTION_OBJECT_STAFF: 2,
	_SALE_ORDER_PROMOTION_OBJECT_CUSTOMER: 3,
	_SALE_ORDER_PROMOTION_UNIT_QUANTITY: 1,
	_SALE_ORDER_PROMOTION_UNIT_NUM: 2,
	_SALE_ORDER_PROMOTION_UNIT_AMOUNT: 3,
	_DAT_KHONG_NHAN_KM: 1,
	_DAT_NHAN_MOT_PHAN_KM: 2,
	_mapErrProgram: null,
	/*
	 * Map(san pham, ds kho voi so luong thay doi thuc te tren grid).
	 * Chua thong tin la cac san pham dang chon tren grid san pham ban, clone tu _mapPRODUCTSbyWarehouse,
	 * so luong available_quantity thay doi tren map nay.
	 * Muc dich: 	- bao toan du lieu cho _mapPRODUCTSbyWarehouse, vi ko ro _mapPRODUCTSbyWarehouse duoc su dung o nhung noi nao
	 * 				- han che phat sinh bo nho
	 */
	_mapPRODUCTSbyWarehouseOnGrid: new Map(),
	_lstCTTL: new Array(),
	_lstCTTLMoneyQueue: new Array(),//luu cac CTKM tich luy tien nhưng chưa được phân bổ vào đơn hàng do total không đủ
	_totalAcc: new Array(),//tong tien KM tich luy 
	_editingProductCode: null,
	_selectingIndex: null,
	/**
	 * true: neu nhu dang thuc hien them san pham tu dialog kho vao grid
	 */
	_isInsertProductFromDialogToGrid: false,
	/**
	 * true: neu nhu dang xoa 1 dong khoi grid
	 */
	_isRemovingRow: false,
	/**
	 * danh sach cac kho chon de them san pham tu dialog
	 */
	_selectedWarehouse: new Map(),
	/*
	 * danh sach cac dong san pham cap nhat vao grid.
	 * dung khi tach kho
	 */
	_tmpRowUpdateToGrid: new Map(),
	/*
	 * cac kieu nhap san pham tu dialog chon kho
	 */
	productWarehouseType: {
		SALE_PRODUCT: 1,
		PROMOTION_PRODUCT: 2,
		SALE_ORDER_PROMOTION: 3,
		PROMOTION_OPEN_PRODUCT: 8,
		KEY_SHOP: 9
	},
	/*
	 * loai tra thuong cua CTKM
	 */
	PROMOTION_GET_TYPE: {
		PRODUCT: 1,	// tra thuong san pham
		AMOUNT: 2,	// tra thuong tien
		PERCENT: 3,	// tra thuong %
	},
	/*
	 * kieu nhap san pham tu dialog chon kho dang chon
	 */
	_selectingProductWarehouseType: null,
	/*
	 * cho biet trang thai xu ly la dang tu dong tach kho hay la chon san pham
	 */
	_isSplitingWarehouse: false,
	/*
	 * true: neu nhu dang tao don hang, false: neu nhu dang sua don hang
	 */
	_isCreatingSaleOrder: true,
	/*
	 * phan nhom (CTKM, co duoc doi san pham km hay ko) tren grid san pham KM
	 */
	_promotionProgramGroup: null,
	/*
	 * cho biet co thay doi tren sale grid hay ko, dung de set trang thai cho button 'tinh tien', button 'luu'
	 */
	_isChangeOnSaleGrid: false,
	/*
	 * use to synchronize the process updating data on sale grid
	 */
	_syncFlag: {
		/*
		 * true: validate if quantity exceed available quanity
		 * false: just set data, not validate
		 */
		isCheckInputQuantityValid: true,
		/*
		 * true: setting common data (product code, product name, ...) finished. Otherwise, false
		 */
		isSettingProductCodeDone: false,
		/*
		 * true: setting quantity finished. Otherwise, false
		 */
		isSettingQuantityDone: false,
		/*
		 * true: setting program finished. Otherwise, false
		 */
		isSettingProgramDone: false,
		/*
		 * Spliting warehouse will insert more rows into handsontable. 
		 * If 'processNextItem' is true, new row will be inserted. Otherwise, the processing of current row hasn't finished yet.
		 */
		processNextItem: true,
		/*
		 * Whenever choosing a product, it will be filled with default program. => isSetDefaultProgram = true
		 * But when spliting warehouse, each newly inserted row's program field will be set with value of root (editting) row's program field. => isSetDefaultProgram = false
		 */
		isSetDefaultProgram: true,
		/*
		 * true if payment-button is clickable.
		 * Meaning: while spliting warehouse of product, restrict user click payment-button
		 */
		isPaymentButtonClickable: true
	},
	/*
	 * cau hinh phan biet loai kho
	 */
	_isDividualWarehouse: true,
	/*
	 * cau hinh chuyen doi quantity nhap vao
	 */
	SYS_CONVERT_QUANTITY_CONFIG: {
		ZEROQUANTITY: '0',	// 0/quantity
		QUANTITYZERO: '1',	// quantity/0
	},
	_sysConvertQuantityConfig: '0',
	/**
	 * Ham load page createOrderInfo.jsp
	 */
	pageLoad:function(){
		SPCreateOrder.initUnitCbx();
		SPCreateOrder.initKeyShopGrid();
		$('#deliveryDate').datepicker('setDate', new Date());
		disabled('btnOrderReset');
		disabled('payment');
		$('.cls_Convfact_Value').live('keypress',Utils.unitNumberOnly);
		SPCreateOrder.setEventInput();
		SPCreateOrder.bindAutoComplete();
		/*
		 * init and create datagrids
		 */
		GridSaleBusiness.initSaleGrid();
		SPCreateOrder.loadGridFree();
		OpenProductPromotion.initOpenProductGrid();
		SPCreateOrder.createAccumulativePromotionGrid();

//		var shopCodeFilter = $('#shopCodeCB').combobox('getValue');
		SPCreateOrder.loadProductForSale(SPCreateOrder._shopCodeFilter);
		var optionsDisplay = { 
			beforeSubmit: SPCreateOrder.beforeImportExcel,   
	 		success:      ProductLevelCatalog.afterImportExcelUpdate,
	 		type: "POST",
	 		dataType: 'html'
	 	};
		$('#importDisplayFrm').ajaxForm(optionsDisplay);
		
		$('#customerCode').bind('keyup', function(event) {					
			if (event.keyCode == keyCode_F9) {
				var shopId = $('#shopId').val();
				var shopCodeFilter = $('#shopCodeCB').combobox('getValue');
				var params = new Object();
				params.shopId = shopId;
				if (shopCodeFilter.length > 0) {
					params.shopCode = shopCodeFilter;
				};
				VCommonJS.showDialogSearch2({
				    inputs : [
				        {id:'code', maxlength:50, label:create_order_ma_kh},
				        {id:'name', maxlength:250, label:create_order_ten_kh},
				        {id:'address', maxlength:250, label:create_order_dia_chi, width:410}
				    ],
				    // params:{shopId:shopId},
				    params : params,
				    url : '/commons/customer-in-shop/search',
				    onShow : function() {
				    	var tabindex = -1;
			        	$('.InputTextStyle, select, input, button , li , tr, td, th, label, ul, a, img').each(function () {
							if (this.type != 'hidden') {
								$(this).attr("tabindex", tabindex);
								tabindex -=1;
							}
						});	        		        	
			        	$('.easyui-dialog #seachStyle1Code').focus();
						$('.easyui-dialog #seachStyle1AddressLabel').show();
						$('.easyui-dialog #seachStyle1Address').show();
						$('.easyui-dialog #searchStyle1Grid').show();
						$('.easyui-dialog #btnSearchStyle1').css('margin-left', 270);
						$('.easyui-dialog #btnSearchStyle1').css('margin-top', 5);
						$('.easyui-dialog #seachStyle1CodeLabel').html(create_order_ma_kh);
						$('.easyui-dialog #seachStyle1NameLabel').html(create_order_ten_kh);
						
						Utils.bindAutoSearch();
						$('.easyui-dialog #btnSearchStyle1').bind('click',function(event) {
							if(!$(this).is(':hidden')){
								var code = $('.easyui-dialog #seachStyle1Code').val().trim();
								var name = $('.easyui-dialog #seachStyle1Name').val().trim();
								var address = $('.easyui-dialog #seachStyle1Address').val().trim();
								$('.easyui-dialog #searchStyle1Grid').datagrid('load',{code :code, name: name,address:address});
								var records = $('.easyui-dialog #searchStyle1Grid').datagrid('getRows').length;
								$('.easyui-dialog #seachStyle1Code').focus();
								if (records != null && records == 1 && !isNullOrEmpty(code)) {
									$('#customerCode').val($('.easyui-dialog #searchStyle1Grid').datagrid('getRows')[0].shortCode);
									$('#customerName').val($('.easyui-dialog #searchStyle1Grid').datagrid('getRows')[0].customerName);
									$('#searchStyle1EasyUIDialogDiv').css("visibility", "hidden");					
									$('.easyui-dialog').dialog('close');
									$('#customerCode').change();
								}
							}
						});
				    },
				    columns : [[
				    	{field:'customerCode', title:create_order_ma_kh, align:'left', width: 110, sortable:false, resizable:false, hidden:true, formatter: function(v, r, i) {
				    		return Utils.XSSEncode(v);
				    	}},
				        {field:'shortCode', title:create_order_ma_kh, align:'left', width: 110, sortable:false, resizable:false, formatter: function(v, r, i) {
				    		return Utils.XSSEncode(v);
				    	}},
				        {field:'customerName', title:create_order_ten_kh, align:'left', width: 150, sortable:false, resizable:false, formatter: function(v, r, i) {
				    		return Utils.XSSEncode(v);
				    	}},
				        {field:'address', title:create_order_dia_chi, align:'left', width: 170, sortable:false, resizable:false, formatter: function(v, r, i) {
				    		return Utils.XSSEncode(v);
				    	}},
				        {field:'choose', title:'', align:'center', width:40, sortable:false, resizable:false, formatter:function(value,row,index) {
				        	return "<a href='javascript:void(0)' onclick=\"return SPCreateOrder.getResultSeachStyle1EasyUIDialog("+ index + ");\">"+create_order_chon +"</a>";        
				        }}
				    ]]
				});
			} 
		});		
		$('#customerCode').live('change', function() {
			SPCreateOrder.isShowDialogNVBHConfirm = false;
			var shortCode = $(this).val().trim().toUpperCase();	
			if (isNullOrEmpty(shortCode)) {
				$('#generalInfoDiv #GeneralInforErrors').html('Chọn khách hàng trước khi chọn NVBH,NVGH,NVTT.').show();
				SPCreateOrder.resetStaff();
				return false;
			}
			var htSale = $('#gridSaleData').handsontable('getInstance');
			var data = htSale.getData();
			if (SPCreateOrder._currentCustomerCode.length > 0 && SPCreateOrder._currentCustomerCode != shortCode) {
				var len = data.length;
				if (len >= 1) {
					var row = data[0];
					if (!isNullOrEmpty(row[0])) {
						$.messager.confirm(jsp_common_xacnhan,'Danh sách sản phẩm đã chọn sẽ xóa nếu chọn lại KH. <br /><b>Bạn có muốn chọn KH khác không?</b>.', function(r) {  
							if (r) {
								SPCreateOrder.customerDetails(shortCode, null);
								SPCreateOrder._currentCustomerCode = shortCode;
								SPCreateOrder._tmpRowUpdateToGrid = new Map();
							} else {
								$('#customerCode').val(Utils.XSSEncode(SPCreateOrder._currentCustomerCode));
							}
						});	
					} else {
						SPCreateOrder.customerDetails(shortCode,null);
						SPCreateOrder._currentCustomerCode = shortCode;
					}
				} else {
					SPCreateOrder.customerDetails(shortCode,null);
					SPCreateOrder._currentCustomerCode = shortCode;
				}							
			} else {
				SPCreateOrder.customerDetails(shortCode,null);
				SPCreateOrder._currentCustomerCode = shortCode;
			}			
		});		
		var tm = setTimeout(function(){
			SPCreateOrder.disabledStaff();
			$('#customerCode').focus();	
			clearTimeout(tm);
		},1000);	
		/*
		 * init some other data
		 */
		SPAdjustmentOrder.PROGRAM_TYPE_MAP = new Map([
													{key: "ZM", value: "1"},
													{key: "DISPLAY_SCORE", value: "2"},
													{key: "ZD", value: "3"},
													{key: "ZH", value: "4"},
													{key: "ZT", value: "5"},
													{key: "MANUAL_PROM", value: "dummy"}
												]);
	},
	
	/**
	 * Hủy đơn hàng
	 * @author nhanlt
	 * @since 29/08/2014
	 */
	cancelOrder: function() {
		$(".ErrorMsgStyle").hide();
//		SPConfirmOrder.hideAllMsg();
		var lstOrderId=new Array();
		if (!isNullOrEmpty(SPAdjustmentOrder._orderId)) {
			lstOrderId.push(SPAdjustmentOrder._orderId);
		}
		if (lstOrderId.length == 0) {
			$('#errMsgOrder').html('Bạn chưa chọn đơn hàng để hủy.').show();
			return ;
		}
		var params=new Object();
		params.lstOrderId=lstOrderId;
		params.approvedStep = 1;
		Utils.addOrSaveData(params, '/sale-product/confirm-order/cancel-order', null, 'serverErrors', function(data) {
			if (!data.error) {
				$('#serverErrors').html('Hủy đơn hàng thành công').show();
				var tm = setTimeout(function() {
					$('#successMsgOrder').hide();
					SPCreateOrder.resetOrder();
					clearTimeout(tm);
				}, 2000);
				
			}
		});
	},
	
	checkQuantityAmountKeyShop:function() {
		var rows = $('#gridKeyShop').datagrid('getRows');
		for (var i = 0 ; i < rows.length ; i++) {
			var r = rows[i]
			if (r.productId != null) {//dòng sản phẩm
				var quantityAmount = 0;
				//for qua lấy tổng nhận của sản phẩm trong 1 chương trình theo tất cả các kho
				for (var j = i ; j < rows.length ; j++) {
					if (rows[j].productId != null && r.productId == rows[j].productId && r.ksId  == rows[j].ksId) {
						quantityAmount += StockValidateInput.getQuantity(rows[j].quantityAmount, r.convfact);
					}
				}
				if (quantityAmount > r.total - r.totalDone) {
					return 'Mã sản phẩm ' + Utils.XSSEncode(r.productCode) + ' vượt quá số lượng trả thưởng chương trình hỗ trợ thương mại. Vui lòng nhập lại.';
				}
			}
		}
		return '';
	},
	
	initChooseWarehouseKeyShop: function(data, index) {
		var rows = $('#gridKeyShop').datagrid('getRows');
		var row = rows[index];
		data.rows.forEach(function(item, index){
			var selector = '#productWarehouseDialog input[id*=warehouseRow][warehouseid="' + item.warehouseId + '"]';
			var warehousesOfProduct = GridSaleBusiness.getWarehouseByType(row.productId) || [];
			var processingWarehouse = warehousesOfProduct.filter(function(item2){
										return item2.warehouseId == item.warehouseId;
									});
			var qtt = 0;
			//tính tổng sản phẩm theo kho
			for (var ij = 0, szj = rows.length; ij < szj; ij++) {
				var r = rows[ij];
				if (r.productId != null) {//dòng san phẩm
					if (row.productCode == r.productCode && row.ksCode == r.ksCode && r.warehouse.warehouseId == processingWarehouse[0].warehouseId) {
						//ứng với 1 sp, 1 ks, 1 kho thì chỉ có 1 dòng
						qtt = r.quantityAmount;
						break;
					}
				}
			}
			if (qtt != 0) {
				$(selector).val(qtt);
			}
			var remainQuantity = row.total - row.totalDone;
			$(selector).parent().append('<span>(' + GridSaleBusiness.formatStockQuantity(remainQuantity, row.convfact).toString() + ')</span>');
			// custom display style
			setTimeout(function(){
				$(selector).parent().css('line-height', $(selector).parent().height() + 'px');
			}, 200);
		});
	},
	
	chooseWarehouseKeyShop: function(mapWarehouseQuantity) {
		var index = $('#indexTmpToAddRow').val();
		var rows = $('#gridKeyShop').datagrid('getRows');
		var row = rows[index];
		var lst = new Array();
		//Bỏ qua các dòng cũ và thêm dòng mới
		var flag = true;
		var weight = 0;
		for (var i = 0 ; i < rows.length ; i++) {
			var r = rows[i];
			if (rows[i].productId != null && row.productId == r.productId && row.ksCode == r.ksCode) {
				//các dòng cùng sp cùng ks thì bỏ qua không thêm vào lst
				var product = GridSaleBusiness.mapSaleProduct.get(rows[i].productCode);
				weight -= getQuantity(rows[i].quantityAmount, rows[i].convfact) * product.grossWeight;//trừ weight của các dòng cũ mà sẽ bị thay thế
				if (flag) {
					flag = false;//thêm các dòng mới vào 1 lần và các lần sau ko thêm nữa
					for (var k = 0 ; k < mapWarehouseQuantity.valArray.length ; k++) {
						var newRow = jQuery.extend(true, {}, row);
						newRow.warehouse = mapWarehouseQuantity.valArray[k].warehouse;
						newRow.quantityAmount = mapWarehouseQuantity.valArray[k].convfact_value;
						if (newRow.quantityAmount.indexOf('/') == -1) {//nếu ko có / thì là số lẻ
							newRow.quantityAmount = '0/' + newRow.quantityAmount;
						} else if (newRow.quantityAmount.indexOf('/') == 0) {
							newRow.quantityAmount = '0' + newRow.quantityAmount;
						}
						lst.push(newRow);
						weight += Number(getQuantity(newRow.quantityAmount, newRow.convfact)) * product.grossWeight;//thêm weight các dòng mới
					}
				}
			} else {
				lst.push(rows[i]);
			}
		}
		//set data vao grid keyshop
		$('#gridKeyShop').datagrid('loadData',lst);
		
		//set lại total_weight mới
		var totalWeight = Number($('#totalWeight').html().replace(/,/g,''));
		totalWeight += weight;
		$('#totalWeight').html(formatCurrency(Math.round10(totalWeight, -3)));
	},
	
	chkKeyShopChange:function() {
		if ($('#chkKeyshop').is(':checked')) {
			var lst = SPCreateOrder.calKeyShop();
			$('#gridKeyShop').datagrid('loadData',lst);
		} else {
			$('#gridKeyShop').datagrid('loadData',[]);
			SPCreateOrder._lstDiscountKeyShop = new Map();
		}
		//sau khi tính toán và load lại grid keyshop thì cho tự động tính tiền lại để khớp giá trị trọng lượng
		setTimeout(function(){
			SPCreateOrder.payment();
		},300);
	},
	
	/*
	 * tungmt
	 * Load keyshop khi edit đơn hàng (Màn hình tạo đơn hàng)
	 */
	loadKeyShopEdit:function(data) {
		var lst = new Array();
		for (var i = 0 ; i < data.order_detail_promotion.length ; i++) {
			var detail = data.order_detail_promotion[i];
			if (detail.promotionType == 'KS') {
				var obj = new Object();
				obj.ksCode = detail.programCode;
				obj.isView = true;
				if (detail.productId != null) {
					obj.productId = detail.productId;
					obj.productCode = detail.productCode;
					obj.productName = detail.productName;
					obj.convfact = detail.convfact;
					obj.warehouse = detail.warehouse;
					obj.warehouse.availableQuantity = detail.availableQuantity; 
					obj.quantityAmount = detail.quantityPackage + '/' + detail.quantityRetail;
				} else {
					obj.quantityAmount = detail.discountAmount; 
				}
				//set tổng thưởng, tổng trả
				obj.total = 0;
				obj.totalDone = 0;
				for (var j = 0 ; j < SPCreateOrder._lstKeyShop.length ; j++) {
					var ks = SPCreateOrder._lstKeyShop[j];
					//nếu trùng ksCode và cùng là KM SP or KM tiền
					if (obj.ksCode == ks.ksCode && ((obj.productId == null && ks.productId == null)
							|| (obj.productId != null && ks.productId != null && obj.productId == ks.productId))) {
						obj.total = ks.total;
						obj.totalDone = ks.totalDone;
						break;
					}
				}
				lst.push(obj);
			}
		}
		SPCreateOrder.loadKeyShopGrid(lst);
	},
	
	/*
	 * tungmt
	 * Parse danh sách key shop lấy từ server thành list
	 */
	parseKeyShop2List:function(data) {
		SPCreateOrder._lstKeyShop = new Array();
		var lst = data.lstKeyShop;
		if (lst != undefined && lst != null && lst.length > 0) {
			for (var i = 0 ; i < lst.length ; i++) {
				lst[i].quantityAmount = lst[i].total - lst[i].totalDone;
				SPCreateOrder._lstKeyShop.push(lst[i]);
			}
		}
	},
	
	/*
	 * tungmt
	 * Lấy danh sách trả thưởng keyshop cho khách hàng
	 */
	getKeyShopData:function(data) {
		SPCreateOrder.parseKeyShop2List(data);
		SPCreateOrder._lstKeyShop = SPCreateOrder.splitWarehouseForKeyShop();
	},
	
	/*
	 * tungmt
	 * Tách kho trả thưởng keyshop
	 */
	splitWarehouseForKeyShop:function() {
		var lst = SPCreateOrder._lstKeyShop;
		if (lst == null || lst.length == 0) {
			return lst;
		}
		var lstResult = new Array();
		var lstProductCodeNotInWarehouse = '';//Danh sách sản phẩm không có trong kho KM
		for (var i = 0 ; i < lst.length ; i++) {
			var obj = jQuery.extend(true, {}, lst[i]);
			if (obj.productCode == null || obj.productCode.length == 0) {
				lstResult.push(obj);
				continue;//nếu là dòng KM tiền thì bỏ qua ko tách kho
			}
			var quantityAmount = obj.quantityAmount;
			var lstWH = GridSaleBusiness.getWarehouseOnGridByType(obj.productId, WarehouseType.PROMO);
			if (lstWH != null && lstWH.length > 0) {
				for (var k = 0 ; k < lstWH.length && quantityAmount > 0; k++) {
					if (lstWH[k].availableQuantity >= quantityAmount) {
						obj.quantityAmount = GridSaleBusiness.formatStockQuantity(quantityAmount, obj.convfact);
					} else {
						obj.quantityAmount = GridSaleBusiness.formatStockQuantity(lstWH[k].availableQuantity, obj.convfact); 
					}
					quantityAmount = quantityAmount - lstWH[k].availableQuantity;
					obj.warehouse = lstWH[k];
					lstResult.push(jQuery.extend(true, {}, obj));
				}
			} else {
				lstProductCodeNotInWarehouse += lstProductCodeNotInWarehouse != '' ? ', ' + obj.productCode : Utils.XSSEncode(obj.productCode);
			}
		}
		if (lstProductCodeNotInWarehouse.length > 0) {
			$('#GeneralInforErrors2').html('Các sản phẩm được trả thưởng chương trình hỗ trợ thương mại không có trong kho ' + Utils.XSSEncode(lstProductCodeNotInWarehouse)).show();
			setTimeout(function() { $('#GeneralInforErrors2').hide(); },3000);
		}
		return lstResult;
	},
	
	/*
	 * tungmt : Tính toán khuyến mãi tiền để chiết khấu
	 */
	calKeyShop:function() {
		var lst = SPCreateOrder._lstKeyShop;
		var lstResult = new Array();
		var totalAmount = Number($('#total').html().replace(/,/g,''));
		for (var i = 0 ; i < lst.length ; i++) {
			var obj = jQuery.extend(true, {}, lst[i]);
			if (obj.productId != null) {//sản phẩm thì lấy ko cần tính
				lstResult.push(obj);
			} else if (totalAmount > 0){
				var discount = obj.total - obj.totalDone;
				if (discount > 0) {
					if (totalAmount > discount) {
						totalAmount = totalAmount - discount;
					} else {
						discount = totalAmount;
						totalAmount = 0;
					}
					obj.quantityAmount = discount;
					lstResult.push(obj);
				}
			}
		}
		return lstResult;
	},
	
	/*
	 * tungmt: gọi khi load lại khách hàng or đổi ngày đơn hàng
	 */
	loadKeyShopGrid:function(lst) {
		if (lst != null) {
			$("#h2KS").next().show();
			$("#h2KS").show();
			setTimeout(function(){
				if ($('#chkKeyshop').is(':checked')) {
					$('#gridKeyShop').datagrid('loadData',lst);
				}
			},200);
		}
	},
	
	/*
	 * tungmt
	 * Khởi tạo grid keyshop
	 */
	initKeyShopGrid:function() {
		$("#h2KS").next().show();
		$("#h2KS").show();
		var isLoadKeyShopGrid = 1;
		$('#gridKeyShop').datagrid({
			data: [],
			singleSelect: true,
			width:$(window).width() - 40,				
			fitColumns:true,		
			scrollbarSize:0,
			autoRowHeight : true,
			columns: [[
			    {field:'ksCode',title:'CT HTTM',width:100,resizable:false, sortable:false,align:'left', formatter: function(v,r,i){
			    	return '<a class="delIcons" onclick="return SPCreateOrder.showKeyShopDetail(\'' + Utils.XSSEncode(v) + '\');" href="javascript:void(0)">' + Utils.XSSEncode(v) + '</a>';
			    }},
			    {field:'productCode',title:'Mã sản phẩm',width:100,resizable:false, sortable:false,align:'left', formatter: function(v,r,i){
			    	v = Utils.XSSEncode(v);
			    	return '<a href="javascript:void(0)" class="ppCodeStyle" id="promotionProduct7" index="7" onclick="return SPCreateOrder.viewProductPromotionDetail(\'' + Utils.XSSEncode(v) + '\')">' + Utils.XSSEncode(v)+ '</a>';
			    }},
			    {field:'productName', title:'Tên sản phẩm', align:'left', resizable:false, sortable:false, width: 150, formatter:function(v,r,i){
			    	return Utils.XSSEncode(v);
			    }},
			    {field:'quantityAmount', title:'Số lượng/Tiền', align:'right', resizable:false, sortable:false, width: 100, formatter:function(v,r,i){
			    	v = Utils.XSSEncode(v);
			    	if (r.productCode != null && r.productCode != '') {//sản phẩm
			    		return '<input id="quantityAmount' + i + '" value="' + v + '" onchange="SPCreateOrder.onChangeQuantityKeyShop(this,\'' + i + '\')" type="text" class="InputTextStyle InputText1Style">';
			    	} else {
			    		return formatCurrency(v);
			    	}
			    }},
			    {field:'warehouse', title:'Kho', align:'left', resizable:false, sortable:false, width: 100, formatter:function(v,r,i){
			    	if (r.productCode != null && r.productCode != '' && r.warehouse != undefined && r.warehouse != null) {//sản phẩm
			    		return '<a onclick="return SPCreateOrder.openSelectProductWarehouseDialog(\''+r.productCode+'\',\''+i+'\', SPCreateOrder.productWarehouseType.KEY_SHOP);" href="javascript:void(0)">'+Utils.XSSEncode(r.warehouse.warehouseName)+'</a>';
			    	}
			    	return '';
			    }},
			    {field:'availableQuantity', title:'Tồn kho', align:'right', resizable:false, sortable:false, width: 100, formatter:function(v,r,i){
			    	if (r.productCode != null && r.productCode != '') {//sản phẩm
				    	return GridSaleBusiness.formatStockQuantity(Utils.XSSEncode(r.warehouse.availableQuantity), r.convfact);
			    	}
			    	return '';
			    }},
			    {field:'totalDone', title:'Tổng trả', align:'right', resizable:false, sortable:false, width: 100, formatter:function(v,r,i){
			    	if (r.productCode != null && r.productCode != '') {//sản phẩm
			    		return GridSaleBusiness.formatStockQuantity(Utils.XSSEncode(v), r.convfact);
			    	} else {
			    		return formatCurrency(Utils.XSSEncode(v));
			    	}
			    }},
			    {field:'total', title:'Tổng thưởng', align:'right', resizable:false, sortable:false, width: 100, formatter:function(v,r,i){
			    	if (r.productCode != null && r.productCode != '') {//sản phẩm
			    		return GridSaleBusiness.formatStockQuantity(Utils.XSSEncode(v), r.convfact);
			    	} else {
			    		return formatCurrency(Utils.XSSEncode(v));
			    	}
			    }},
			]],	
			onLoadSuccess: function(data){
				if (data == null || !data.rows || data.rows.length == 0) {
					if ($('#chkKeyshop').is(':checked') && isLoadKeyShopGrid == 1) {
						//lần đầu load keyshop của khách hàng mà ko có thì đóng luôn keyshop
						setTimeout(function() {
							$("#h2KS").next().hide();
							$("#h2KS").hide();
						}, 100);
					}
				} else {
					$('.vinput-money').each(function() {
						VTUtilJS.bindFormatOnTextfield($(this), VTUtilJS._TF_NUMBER_COMMA);
						VTUtilJS.formatCurrencyFor($(this));
					});
				}
				isLoad = 0;
			},
			rowStyler: function(i,r){
				if(r.productId != undefined && r.productId != null){//KM sản phẩm
					if (StockValidateInput.getQuantity(r.quantityAmount, r.convfact) > StockValidateInput.getQuantity(r.warehouse.availableQuantity, r.convfact))
					return 'color:' + WEB_COLOR.ORANGE_CODE + ';'; 
				}
				return '';
			},
		});
	},
	
	/*
	 * tungmt
	 * show thông tin chi tiết của chương trình key shop bao gồm các mức của chương trình
	 */
	showKeyShopDetail:function(ksCode) {
		var html = $('#createOrderKSDetailContainer').html();
		$('#createOrderKSDetailContainer').show();
		$('#createOrderKSDetail').dialog({
			closed: false,  
			cache: false,  
		    modal: true,
		    onOpen: function() {
		    	$.getJSON('/commons/ks-detail?ksCode='+ksCode, function(data) {
					if (data != null && data.ksInfo != null) {
						var info = data.ksInfo;
						var lstLevel = data.lstLevel;
						$('.easyui-dialog #ksCodeKSDetail').html(Utils.XSSEncode(info.ksCode));
						$('.easyui-dialog #ksNameKSDetail').html(Utils.XSSEncode(info.ksName));
						$('.easyui-dialog #fromCycleKSDetail').html(Utils.XSSEncode(info.fromCycle));
						$('.easyui-dialog #toCycleKSDetail').html(Utils.XSSEncode(info.toCycle));
						$('.easyui-dialog #skuKSDetail').html(Utils.XSSEncode(info.productCode));
						$('.easyui-dialog #descriptionKSDetail').html(Utils.XSSEncode(info.description));
						$('#gridKSLevelDetail').datagrid({
							data: lstLevel,
							singleSelect: true,
							width:$('#createOrderKSDetail').width() - 20,				
							fitColumns:true,		
							scrollbarSize:0,
							autoRowHeight : true,
							columns: [[
							    {field:'ksLevelCode',title:'Mã mức',width:100,resizable:false, sortable:false,align:'left', formatter: function(v,r,i){
							    	return Utils.XSSEncode(v);
							    }},
							    {field:'ksLevelName',title:'Tên mức',width:200,resizable:false, sortable:false,align:'left', formatter: function(v,r,i){
							    	return Utils.XSSEncode(v);
							    }},
							    {field:'amount', title:'Số tiền', align:'right', resizable:false, sortable:false, width: 100, formatter:function(v,r,i){
							    	return Utils.XSSEncode(formatCurrency(v));
							    }},
							    {field:'quantity', title:'Số lượng', align:'right', resizable:false, sortable:false, width: 70, formatter:function(v,r,i){
							    	return Utils.XSSEncode(formatCurrency(v));
							    }},
							]],	
							onLoadSuccess: function(data){
							},
						});
					} else {
						$('.easyui-dialog #ksCodeKSDetail').html('');
						$('.easyui-dialog #ksNameKSDetail').html('');
						$('.easyui-dialog #fromCycleKSDetail').html('');
						$('.easyui-dialog #toCycleKSDetail').html('');
						$('.easyui-dialog #skuKSDetail').html('');
						$('.easyui-dialog #descriptionKSDetail').html('');
					}
				});
		    },
		    onClose : function(){
		    	$('#createOrderKSDetailContainer').html(html);
	        	$('#createOrderKSDetailContainer').hide();
		    }
		});
	},
	
	/*
	 * Thay đổi số lượng sản phẩm trả thưởng Keyshop - Nếu sai thì cho về 0
	 */
	onChangeQuantityKeyShop: function(t, index) {
		var newQuantity = $(t).val();
		if (!GridSaleBusiness.isValidQuantityInput(newQuantity)) {
			$(t).val(0);
			newQuantity = 0;
		}
		var row = $('#gridKeyShop').datagrid('getRows')[index];
		var oldValue = getQuantity(row.quantityAmount, row.convfact);
		var product = GridSaleBusiness.mapSaleProduct.get(row.productCode);
		if (product != null) {
			var newWeight = product.grossWeight * Number(getQuantity(newQuantity, row.convfact));
			var oldWeight = product.grossWeight * oldValue;
			var totalWeight = Number($('#totalWeight').html().replace(/,/g,''));
			totalWeight += newWeight - oldWeight;
			$('#totalWeight').html(formatCurrency(Math.round10(totalWeight, -3)));
			//set lai giá trị cho grid
			row.quantityAmount = newQuantity;
		}
	},
	
	/*
	 * tungmt - lấy grossweight của các SP trả thưởng keyshop
	 */
	getWeightOfKeyShop:function() {
		var rows = $('#gridKeyShop').datagrid('getRows');
		var totalWeight = 0;
		if (rows != null && rows.length > 0) {
			for (var i = 0 ; i < rows.length ; i++) {
				var row = rows[i];
				var quantity = getQuantity(row.quantityAmount, row.convfact);
				var product = GridSaleBusiness.mapSaleProduct.get(row.productCode);
				if (product != null) {
					totalWeight += product.grossWeight * quantity;
				}
			}
		}
		return totalWeight;
	},
	
	/**
	 * lay ds sp ban, ton kho
	 */
	loadProductForSale: function(shopCodeFilter) {
		$.getJSON('/sale-product/create-order/load-product-for-sale?shopCodeFilter='+shopCodeFilter, function(data) {
			if (data != null) {
				if($.isArray(data.productStocks) && data.productStocks.length > 0) {
					for (var i=0;i<data.productStocks.length;i++) {
						var tmp = data.productStocks[i];
						SPCreateOrder._PRODUCTS.put(tmp.productCode,tmp);
					}
				}
				if ($.isArray(data.products) && data.products.length > 0) {
					for (var j=0;j<data.products.length;j++) {
						var tmp2 = data.products[j];
						SPCreateOrder._EQUIPMENT.put(tmp2.productCode,tmp2.productCode);
					}
				}
				if ($.isArray(data.lstWarehouse) && data.lstWarehouse.length > 0) {
					for (var k=0;k<data.lstWarehouse.length;k++) {
						var tmp3 = data.lstWarehouse[k];
						var obj =  new Object();
						obj.warehouseId = tmp3.warehouseId;
						obj.warehouseName = tmp3.warehouseName;
						obj.warehouseCode = tmp3.warehouseCode;
						obj.seq = tmp3.seq;
						obj.warehouseType = tmp3.warehouseType;
						GridSaleBusiness._mapWarehouse.put(tmp3.warehouseId,obj);
					}
				}
				setTimeout(function() {
					if (SPCreateOrder._PRODUCTS.size() > 0) {
						for (var l=0;l<SPCreateOrder._PRODUCTS.keyArray.length;l++) {
							var tmp = SPCreateOrder._PRODUCTS.valArray[l];
							var productId = tmp.productId;
							SPCreateOrder._mapPRODUCTSbyWarehouse.put(productId,data.mapProductByWarehouse[productId]);
						}
					}
				},300);
			}
		});
	},
	/**
	*lay danh sach nhan vien theo shop don vi
	*/
	getStaffByShopCode: function(shopCodeFilter) {
		var params = new Object();	
		if(SPCreateOrder._shopCodeFilter != null){
				params.shopCodeFilter = SPCreateOrder._shopCodeFilter;
		}					
		SPCreateOrder.loadProductForSale(SPCreateOrder._shopCodeFilter);	
		SPCreateOrder.loadStaffCbxs(params);
	},
	loadStaffCbxs: function(params){
		var kData = $.param(params, true);
		 $.ajax({
			type : "POST",
			url : '/sale-product/create-order/load-staff-by-shop-code',
			data : (kData),
			dataType : "json",
			success : function(data) {
				SPCreateOrder._isAllowShowPrice = data.isAllowShowPrice;
				if(SPCreateOrder._isAllowShowPrice){
					$('#sumPrice').show();
					$('#sumPrice1').show();
				} else {
					$('#sumPrice').hide();
					$('#sumPrice1').hide();
				}
				SPCreateOrder._isAllowModifyPrice = data.isAllowModifyPrice;
				GridSaleBusiness.initSaleGrid();
				var htSale = $('#gridSaleData').handsontable('getInstance');
				htSale.render();
				var lstNVBHVo = data.lstNVBHVo;
				var lstNVTTVo = data.lstNVTTVo;
				var lstNVGHVo = data.lstNVGHVo;
				 
				 /**
				  * Xu ly Dialog tim kiem don
				  */
				 if ($('#searchWebOrderPopup').is(':visible')){
					if(lstNVBHVo != undefined && lstNVBHVo != null) {
						Utils.bindStaffCbx('#staffCodeDlg', lstNVBHVo, null, 210);
					 }
					 if(lstNVGHVo != undefined && lstNVGHVo != null) {
						 Utils.bindStaffCbx('#deliveryCodeDlg', lstNVGHVo, null, 210);
					 }
				 } else {
					 if(lstNVTTVo != undefined && lstNVTTVo != null) {
						Utils.bindStaffCbx('#cashierStaffCode', lstNVTTVo);
					 }
					 if(lstNVBHVo != undefined && lstNVBHVo != null) {
						Utils.bindStaffCbx('#staffSaleCode', lstNVBHVo);
					 }
					 if(lstNVGHVo != undefined && lstNVGHVo != null) {
						 Utils.bindStaffCbx('#transferStaffCode', lstNVGHVo);
					 }
					 if (data.lstCar != undefined && data.lstCar != null){
						 $('#carId').combobox({			
							valueField : 'id',
							textField : 'carNumber',
							data : data.lstCar,
							formatter: function(row) {
								return '<span style="font-weight:bold">' + Utils.XSSEncode(row.carNumber) + '</span>';
							},
							filter: function(q, row){
								q = new String(q).toUpperCase().trim();
								var opts = $(this).combobox('options');
								return unicodeToEnglish(row[opts.textField]).indexOf(unicodeToEnglish(q)) >= 0;
							}									
						});
					 }
					 SPCreateOrder.resetStaff();
				 }
			},
			error:function(XMLHttpRequest, textStatus, errorThrown) {
				console.log("Error");
			}
		});
	},

	
	/**
	 * Ham luu thong tin khach hang
	 * @param rowIndex 
	 */
	getResultSeachStyle1EasyUIDialog : function(rowIndex) {
		var row = $('.easyui-dialog #common-dialog-grid-search').datagrid('getRows')[rowIndex];			
		var obj = {};
		SPCreateOrder._currentCustomerCodeFromSearch = row.customerCode;
		obj.code = row.shortCode;
		obj.name = row.customerName;
		obj.address = row.address;
		obj.id = row.id;
		$('#customerCode').val(Utils.XSSEncode(obj.code));
		$('#customerName').val(Utils.XSSEncode(obj.name));
		$('#customerCode').change();
		$('.easyui-dialog').dialog('close');
		return false;
	},
	
	/**
	 * Ham disable cac combobox staff
	 */
	disabledStaff: function() {
		$('#staffSaleCode').combobox('disable');		
		$('#staffSaleCode').parent().addClass('BoxDisSelect');		
		$('#cashierStaffCode').combobox('disable');
		$('#cashierStaffCode').parent().addClass('BoxDisSelect');		
		$('#transferStaffCode').combobox('disable');
		$('#transferStaffCode').parent().addClass('BoxDisSelect');
	},
	
	/**
	 * Ham enable cac combobox staff
	 */
	enabledStaff: function() {
		$('#staffSaleCode').combobox('enable');		
		$('#staffSaleCode').parent().removeClass('BoxDisSelect');			
		$('#cashierStaffCode').combobox('enable');
		$('#cashierStaffCode').parent().removeClass('BoxDisSelect');			
		$('#transferStaffCode').combobox('enable');
		$('#transferStaffCode').parent().removeClass('BoxDisSelect');
	},
	
	/**
	 * Ham reset cac combobox staff
	 */
	resetStaff: function() {
		$('#staffSaleCode').combobox('setValue','');
		$('#staffSaleCode').combobox('disable');		
		$('#staffSaleCode').parent().addClass('BoxDisSelect');				
		$('#cashierStaffCode').combobox('setValue','');
		$('#cashierStaffCode').combobox('disable');
		$('#cashierStaffCode').parent().addClass('BoxDisSelect');
		$('#transferStaffCode').combobox('setValue','');
		$('#transferStaffCode').combobox('disable');
		$('#transferStaffCode').parent().addClass('BoxDisSelect');
	},
	
	/**
	 * Ham validation khi nhap so luong san pham
	 * @param value
	 * @param callback
	 */
	convfactValidation: function(value, callback) {
		if(!/^[0-9\/]+$/.test(value)){
			callback(false);
		} else if(value.indexOf('/')!=-1 && value.split('/').length > 2){
			callback(false);
		} else {
			value = 0;
			callback(true);
		}
	},
	
	/**
	 * Ham load grid free product
	 */
	loadGridFree:function(){
		$('.ErrorMsgStyle').html('').hide();
		SPCreateOrder._lstPromotionProductIsAuto = new Map();
		SPCreateOrder._lstPromotionProductForOrderIsAuto = new Map();
		SPCreateOrder._lstPromotionProductForOrder = new Map();
		$('#spSaleProductInfor').show();
		$('#spPromotionProductInfor').show();
		SPAdjustmentOrder.initPromotionProductGrid();
		SPAdjustmentOrder.initSalePromotionProductGrid();
		enable('payment');
		disabled('btnOrderSave');
		disabled('btnOrderReset');
	},
	
	/**
	 * Ham prepare truoc khi tinh tien
	 */
	preparePayment:function(deleteRow) {
		var obj = new Object();
		var arrDelete = new Array();
		var rows = $('#gridSaleData').handsontable('getInstance').getData();
		if($.isArray(rows) && rows.length > 0) {
			SPCreateOrder._lstProduct = new Map();
			var mapTmp = new Map();			
			for(var i = 0; i < rows.length; i++) {
				var productCode = rows[i][COLSALE.PRO_CODE];		
				if (!isNullOrEmpty(productCode)) {
					var product = SPCreateOrder._PRODUCTS.get(productCode);
					if(!isNullOrEmpty(product.commercialSupport)) {
						if(SPCreateOrder._lstProduct.get(productId+'-'+product.commercialSupport) == null) {
							SPCreateOrder._lstProduct.put(productId+'-'+product.commercialSupport, product);
							mapTmp.put(product.productId+'-'+product.commercialSupport,i);
						} else{
							obj.firstIndex = mapTmp.get(product.productId+'-'+product.commercialSupport);
							obj.lastIndex = i;
							if(deleteRow==undefined){
								return obj;
							}
							arrDelete.push(obj);
						}
					} else {
						if(SPCreateOrder._lstProduct.get(product.productId) == null) {
							SPCreateOrder._lstProduct.put(product.productId, product);
							mapTmp.put(product.productId,i);
						} else{
							obj.firstIndex = mapTmp.get(product.productId);
							obj.lastIndex = i;
							if(deleteRow==undefined){
								return obj;
							}
							arrDelete.push(obj);
						}
					}
				}
			}
		}
		if(deleteRow==undefined){
			return null;
		}
		return arrDelete;
	},	

	/**
	 * Ham validate product voi map all product
	 * @param productCode
	 */
	validateProduct:function(productCode){		
		if(isNullOrEmpty(productCode)){			
			return ERROR.NOT_ERROR;
		}
		productCode = productCode.trim().toUpperCase();		
		product = SPCreateOrder._PRODUCTS.get(productCode);		
		if(product==null){
			return ERROR.NOT_EXISTS;
		}		
		product = SPCreateOrder.findProductCodeInMap(productCode);
		if(product==null){			
			return ERROR.NOT_EXISTS_SALE_CAT;
		}		
		return ERROR.NOT_ERROR;
	},

	/**
	 * Ham validate quantity nhap cua grid sp
	 * @param quantity value
	 * @param convfact
	 */
	validateQuantity:function(value,convfact){
		if(isNullOrEmpty(value) || isNullOrEmpty(convfact)){
			return false;
		}
		var quantity = getQuantity(value,convfact);
		if(isNaN(quantity)){
			return false;
		}
		return true;
	},
	

	/**
	 * Ham set su kien input cho cac control
	 */
	setEventInput:function() {
		$('.clsProductVOQuantity').each(function(){
			var productId = $(this).attr('productid').trim();
			var object = SPCreateOrder._mapProduct.get(productId);
			if (object != null) {
				$(this).val(Utils.XSSEncode(object.convfact_value));
				var index = $(this).attr('index');
				if (object.commercialSupportType.indexOf('ZV') == -1 && !isNullOrEmpty(object.commercialSupportType)) {
					$('#commercialSupport_'+ index).val(Utils.XSSEncode(object.commercialSupport));
					$('#commercialSupport_'+ index).attr('name', Utils.XSSEncode(object.commercialSupportType));
				}								
			}
		});
		/** show dialog san pham */
		$('.ccSupportCode').live('keypress',function(event) {
		  	if (event.keyCode == keyCodes.BACK_SPACE || event.keyCode == keyCodes.DELETE) {
		  		$(this).val('');
		  		$(this).removeAttr('name');
		  	} else if (event.keyCode == keyCode_F9) {
		  		return true;
		  	}
			return false;
		});
		$('.ccSupportCode').live('keyup', function(event){							  
			  var param = new Object();				
			  var arrParams = new Array();
			  param = new Object();
			  param.name='customerCode';			
			  param.value=$('#customerCode').val().trim();				
			  arrParams.push(param);
			  var selector = $(this);
			  if(event.keyCode == keyCode_F9){
				  CommonSearch.searchCommercialSupportOnDialog(function(data){
					  selector.val(Utils.XSSEncode(data.code));
					  selector.attr('name', Utils.XSSEncode(data.promotionType));
					  selector.focus();
				}, arrParams); 
				setTimeout(function() { $('.combo-panel').show(); }, 400);
			}else{
				return false;
			}
		 });		
	},	

	/**
	 * Ham binding auto complete cho combobox
	 */
	bindAutoComplete: function() {
		Utils.bindComboboxStaffEasyUI('staffSaleCode');
		Utils.bindComboboxStaffEasyUI('cashierStaffCode');		
		Utils.bindComboboxStaffEasyUI('transferStaffCode');
		Utils.bindComboboxCarEasyUI('carId');
		setTimeout(function() {
			$('#carId').combobox({
				onChange:function(newvalue,oldvalue){
					SPCreateOrder.orderChanged = true;
				}
			});
			$('#transferStaffCode').combobox({			
				onChange:function(newvalue,oldvalue){
					//$('.combo-panel').each(function(){if(!$(this).is(':hidden')) {$(this).parent().css('width','250px');$(this).css('width','248px');}});
					SPCreateOrder.orderChanged = true;
					if ($("#btnApproveOrderIN").length > 0) {
						if (newvalue == null || newvalue.trim().length == 0) {
							$("#btnApproveOrderIN").removeAttr("onclick");
							disabled("btnApproveOrderIN");
						} else {
							$("#btnApproveOrderIN").attr("onclick", "SPPrintOrder.approveOrder();");
							if ($("#payment:disabled").length > 0) {
								enable("btnApproveOrderIN");
							}
						}
					}
				}
			});
			$('#cashierStaffCode').combobox({
				onChange:function(newvalue,oldvalue){
					SPCreateOrder.orderChanged = true;
				},
				onSelect:function(val){
					var tCode = $('#transferStaffCode').combobox('getValue');
					var nvbhCode = $('#staffSaleCode').combobox('getValue');
					if ($("#saleProductGrid").length > 0 && !isNullOrEmpty(tCode) && !isNullOrEmpty(val.staffCode) && !isNullOrEmpty(nvbhCode) && isNullOrEmpty(SPAdjustmentOrder.orderId)) {
						var len = $('#saleProductGrid').datagrid('getRows').length-1;
						if (len > -1 && len <=0 ) {
							$('#saleProductGrid').datagrid('selectRow',len).datagrid('beginEdit',len);
						}
					}
				}
			});
		}, 1000);
		$('#staffSaleCode').combobox({
			onChange: function(newvalue, oldvalue){
				if (newvalue.length > 0 && SPCreateOrder.isShowDialogNVBHConfirm) {
					var htSale = $('#gridSaleData').handsontable('getInstance');
					var data = htSale.getData();
					var shortCode = $('#customerCode').val().trim()
					if (data.length > 0) {
						var row = data[0];
						if (!isNullOrEmpty(row[0])) {
							$.messager.confirm(jsp_common_xacnhan,'Danh sách sản phẩm sẽ bị xóa khi chọn lại NVBH. <br /> <b>Bạn có muốn chọn nhân viên mới?</b>',function(r){  
							    if (r){ 					    	
							    		SPCreateOrder.staffDetails(shortCode, newvalue);
								    	SPCreateOrder._currentSaleStaff = newvalue;
								    	SPCreateOrder.clearTempData();
							    } else {
							    		SPCreateOrder.isShowDialogNVBHConfirm = false;
							    		$('#staffSaleCode').combobox('setValue', Utils.XSSEncode(SPCreateOrder._currentSaleStaff));
							    }
							});
							$(".panel.window.messager-window:not(:hidden)").find(".panel-tool-close").hide();
						} else {
							SPCreateOrder.staffDetails(shortCode, newvalue);
							SPCreateOrder._currentSaleStaff = newvalue;
						}
					} else {
						SPCreateOrder.staffDetails(shortCode, newvalue);
						SPCreateOrder._currentSaleStaff = newvalue;
					}
				}
				SPCreateOrder.isShowDialogNVBHConfirm = true;
				$('.combo-panel').each(function() {
					if (!$(this).is(':hidden')) {
						$(this).parent().css('width','250px');
						$(this).css('width','248px');
					}
				});
			}
		});
	},
	
	/**
	 * Ham load param tim kiem thong tin sp
	 */
	getParamsSearchProduct:function(){
		var obj = new Object();		
		var spCreateOrderSearchProductCode = $('#spCreateOrderSearchProductCode').val().trim();
		var spCreateOrderSearchProductName = $('#spCreateOrderSearchProductName').val().trim();
		var spCreateOrderSearchPPCode = $('#spCreateOrderSearchPPCode').val().trim();	
		obj.code = spCreateOrderSearchProductCode;
		obj.name = spCreateOrderSearchProductName;
		obj.ppCode = spCreateOrderSearchPPCode;
		obj.shopCode = $('#shopCode').val().trim();
		obj.salerCode=$('#staffSaleCode').combobox('getValue').trim();
		obj.customerCode=$('#customerCode').val().trim();
		obj.orderId=$('#orderId').val();		
		return obj; 
	},

	/**
	 * Ham mo dialog tim kiem sp
	 */
	openSelectProductDialog: function(){
		SPCreateOrder._mapProduct = new Map();
		SPCreateOrder._mapCommercialSupport = new Map();
		$('#productDialogContainer').show();
		$('.clsProductVOQuantity').bind('keydown',function(e) {
			var index = $(this).attr('index');
			var isPressed = false;
			if (e.keyCode == keyCodes.ARROW_DOWN) {
				++index;
				isPressed = true;
			} else if (e.keyCode == keyCodes.ARROW_UP) {
				--index;
				isPressed = true;
			} else if (e.keyCode == keyCodes.ARROW_LEFT) {
				$('#commercialSupport_'+index).focus();
			}
			if (index > -1 && isPressed == true) {
				$('#productRow_'+index).focus();
			}
		});
		var html = $('#productDialog').html();
		$('#productDialog').window('refresh');
		$('#productDialog').dialog({closed: false,modal: true,onOpen: function() {			
			$('#spCreateOrderSearchProductCode').focus();
			$('.easyui-dialog #GeneralInforErrors').html('').hide();
			Utils.bindAutoSearch();
	        	$("#productGrids").datagrid({
	        		url: '/sale-product/create-order/search-product',        		
	        		fitColumns:true,
	        		pagination : true,
	        		width: $('#productDialog').width()-25,
	        		scrollbarSize:0,
	        		singleSelect:true,
	        		rownumbers:true,cache: false,
	        		queryParams:SPCreateOrder.getParamsSearchProduct(),
	        		columns:[[  
	        		          {field:'productCode',title:'Sản phẩm',width:$('#productDialog').width() - 35 - 120 - 80 - 100 - 100, align:'left', formatter: function(value, row, index) {
	        		        	  if(row.isFocus=='0'|| row.isFocus==0){
	        		        		  return '<span class="prStyle'+index+'">'+Utils.XSSEncode(row.productCode+'-'+row.productName)+'</span>';
	        		        	  } else {
	        		        		  return '<span class="prStyle'+index+'">'+Utils.XSSEncode(row.productCode+'-'+row.productName)+'(*)'+'</span>';
	        		        	  }
	        		          }},  
	        		          {field:'availableQuantity',
	        		        	  title:isNullOrEmpty(SPAdjustmentOrder.orderId)? 'Tồn kho đáp ứng':'Tồn kho thực tế',
	        		        	  align:'right',width:130, formatter: function(value, row, index) {
	        		        		  var value = Number(row.availableQuantity);	  
	        		        		  if(!isNullOrEmpty(SPAdjustmentOrder.orderId)){
	        		        			  value = Number(row.stock);
	        		        		  }
	        		        		  return '<span class="prStyle'+index+'">'+formatQuantityEx(value,row.convfact)+'</span>';
	        		          }},  
	        		          {field:'price',title:'Giá',width:80,align:'right', formatter: function(value, row, index) {
	        		        	  	if(value != null && value != undefined ){
	        		        	  		return '<span class="prStyle'+index+'">'+formatCurrency(value)+'</span>';
	        		      			}else{
	        		      				return "";
	        		      			}
	        		          }},
	        		          {field:'promotionProgramCode',title:'CTKM',width:100,align:'left',formatter:function(value,row,index){
	        		        	  if(value != null && value != undefined ){
	        		        		  return '<span class="prStyle'+index+'">'+Utils.XSSEncode(value)+'</span>';
	        		        	  }
	        		        	  return '';
	        		          }},
	        		          {field:'quantity',title:'Số lượng',width:100, algin:'center', formatter: function(value, row, index) {
	        		        	  return '<input index="'+index+'" productid="'+Utils.XSSEncode(row.productId)+'" maxlength="8" type="text" style="width: 65px; margin-bottom: 0px;" onkeypress="NextAndPrevTextField(event,this,\'clsProductVOQuantity\')" class="clsProductVOQuantity InputTextStyle InputText1Style prStyle'+index+'" size="10" id="productRow_'+index +'" onchange="SPCreateOrder.amountBlured('+ index+');" onblur="SPCreateOrder.amountBlured('+ index+');" />';
	        		          }}, 
	        		          {field:'commercial',title:'CT HTTM(F9)',width:100, align:'center', formatter: function(value, row, index) {	        		        	  	
	        		        	  	return '<input index="'+index+'" productid="'+Utils.XSSEncode(row.productId)+'" onkeypress="NextAndPrevTextField(event,this,\'ccSupportCode\')" type="text" style="width: 70px;margin-bottom: 0px;" class="ccSupportCode InputTextStyle InputText1Style prStyle'+index+'" size="10" id="commercialSupport_'+ index +'" />';
	        		          }}
	        		]],	        		
	        		onBeforeLoad: function() {
	        			$('.easyui-dialog .clsProductVOQuantity').each(function(){
							  var obj = $(this);
							  var data = {};
							  var rowId = obj.attr('id').split('_')[1];
							  var productId = $("#productGrids").datagrid('getRows')[rowId]['productId'];
							  var productCode= $("#productGrids").datagrid('getRows')[rowId]['productCode'];
							  var productName  = $("#productGrids").datagrid('getRows')[rowId]['productName'];
							  var availableQuantity = $("#productGrids").datagrid('getRows')[rowId]['availableQuantity'];
							  var price = $("#productGrids").datagrid('getRows')[rowId]['price'];
							  var commercialSupport = $('.easyui-dialog #commercialSupport_' + rowId).val();
							  var commercialSupportType = $('.easyui-dialog #commercialSupport_' + rowId).attr('name');
							  var convfact =  $("#productGrids").datagrid('getRows')[rowId]['convfact']; 
							  var grossWeight = $("#productGrids").datagrid('getRows')[rowId]['grossWeight']; 
							  var checkLot = $("#productGrids").datagrid('getRows')[rowId]['checkLot'];
							  if(isNullOrEmpty(commercialSupport)){
								  var autoPromotionSupport = $("#productGrids").datagrid('getRows')[rowId]['promotionProgramCode'];
								  if(autoPromotionSupport != null && autoPromotionSupport != undefined && autoPromotionSupport != '') {
									  commercialSupport = autoPromotionSupport;
									  commercialSupportType = 'ZV';
								  }
							  }	
							  if(!isNullOrEmpty(obj.val())) {
								  var quantity = obj.val();								  
								  data.rowId = productId;
								  data.productCode = productCode;
								  data.productName = productName;
								  data.availableQuantity = availableQuantity;
								  data.price = price;				
								  data.commercialSupport = commercialSupport;
								  data.convfact = convfact;
								  data.productId = productId;
								  data.quantity_input = quantity;
								  data.quantity= getQuantity(quantity,convfact);
								  data.convfact_value = formatQuantity(quantity,convfact);
								  data.grossWeight = grossWeight;
								  data.checkLot = checkLot;
								  if(commercialSupportType==null || commercialSupportType == undefined){
									  commercialSupportType = '';
								  }
								  data.commercialSupportType = commercialSupportType;
								  if(isNaN(data.quantity) || Number(data.quantity)<=0){
									  return false;
								  }
								  SPCreateOrder._mapProduct.put(productId,data);
							  } else {								 
								  var object = SPCreateOrder._mapProduct.get(productId);
								  if(object!=null){
									  SPCreateOrder._mapProduct.remove(productId);
								  }
							  }								
						  });
	        		},
	        		onLoadSuccess: function() {
	        			$('.clsProductVOQuantity').css('text-align','right').css('padding-right','5px');
						$('.ccSupportCode').css('text-align','left').css('padding-left','5px');
						Utils.bindFormatOnTextfieldInputCss('clsProductVOQuantity',Utils._TF_NUMBER_CONVFACT);
						$('.datagrid-header-rownumber').html('STT');
						Utils.bindAutoSearch();
						var PRODUCTS = $("#productGrids").datagrid('getRows');
						var len = PRODUCTS.length;
						for(var i=0;i<len;++i){
							var product = PRODUCTS [i];
							var color =  SPAdjustmentOrder.addStyleRowProductDialog(i,product);
							if(color==WEB_COLOR.ORANGE){
								$('span.prStyle'+i).each(function(){ $(this).css('color',WEB_COLOR.ORANGE_CODE);});
							}else if(color==WEB_COLOR.RED){
								$('span.prStyle'+i).each(function(){ $(this).css('color',WEB_COLOR.RED_CODE);});
							}else {
							  $('span.prStyle'+i).each(function(){ $(this).css('color',WEB_COLOR.BLACK_CODE);});
							}
							var pp = SPCreateOrder._mapProduct.get(product.productId);
							if(pp!=null){
								$('.easyui-dialog #productRow_'+i).val(Utils.XSSEncode(pp.convfact_value));
								if(!isNullOrEmpty(pp.commercialSupportType) && pp.commercialSupportType.indexOf('ZV')!=0){
									$('.easyui-dialog #commercialSupport_' + i).val(Utils.XSSEncode(pp.commercialSupport));
									$('.easyui-dialog #commercialSupport_' + i).attr('name', Utils.XSSEncode(pp.commercialSupportType));
								}								
							}
						}
						$(window).resize();
	        		}
	        	});
	        },
	        onClose : function() {
	        	$('#productDialog').html(html);
	        	$('#productDialogContainer').hide();
	        	$('#productDialogContainer .GridSection').html('<table id="productGrids"></table>');
				SPCreateOrder._mapProduct = null;				
	        }
		});
	},
	
	/**
	 * Ham chon sp
	 */
	selectListProducts: function(){
		$('#GeneralInforErrors').html('').hide();	
		var isErrors = false;
		var flag = true;
		var index = 0;
		$('.easyui-dialog .clsProductVOQuantity').each(function(){
			var obj = $(this);
			var data = {};			
			var rowId = obj.attr('id').split('_')[1];	
			var productId = $("#productGrids").datagrid('getRows')[rowId]['productId'];
			var productCode= $("#productGrids").datagrid('getRows')[rowId]['productCode'];
			var productName  = $("#productGrids").datagrid('getRows')[rowId]['productName'];
			var availableQuantity = $("#productGrids").datagrid('getRows')[rowId]['availableQuantity'];
			var price = $("#productGrids").datagrid('getRows')[rowId]['price'];
			var commercialSupport = $('.easyui-dialog #commercialSupport_' + rowId).val();
			var commercialSupportType = $('.easyui-dialog #commercialSupport_' + rowId).attr('name');
			if(isNullOrEmpty(commercialSupport)) {
				var autoPromotionSupport = $("#productGrids").datagrid('getRows')[rowId]['promotionProgramCode'];
				if(autoPromotionSupport != null && autoPromotionSupport != undefined && autoPromotionSupport != '') {
					commercialSupport = autoPromotionSupport;
					commercialSupportType = 'ZV';
				}
			}
			var convfact =  $("#productGrids").datagrid('getRows')[rowId]['convfact']; 
			var grossWeight = $("#productGrids").datagrid('getRows')[rowId]['grossWeight']; 
			var checkLot = $("#productGrids").datagrid('getRows')[rowId]['checkLot'];
			if(obj.val().length>0){				 
				var quantity = obj.val();
				data.rowId = productId;
				data.productCode = productCode;
				data.productName = productName;
				data.availableQuantity = availableQuantity;
				data.price = price;				
				data.commercialSupport = commercialSupport;
				data.convfact = convfact;
				data.productId = productId;	
				data.quantity= getQuantity(quantity,convfact);
				data.convfact_value = formatQuantity(quantity,convfact);
				data.grossWeight = grossWeight;
				data.checkLot = checkLot;
				if(isNullOrEmpty(commercialSupportType)){
					commercialSupportType = '';
				}
				data.commercialSupportType = commercialSupportType;
				 if(isNaN(data.quantity) || Number(data.quantity)<=0){
					flag = false;index = rowId;
					$('.easyui-dialog #GeneralInforErrors').html('Giá trị số lượng sản phẩm '+ Utils.XSSEncode(productCode +' - '+productName)+' không hợp lệ').show();
					return false;
				}		
				var tmp = SPCreateOrder._mapProduct.get(productId);
				if(tmp!=null){
					SPCreateOrder._mapProduct.remove(productId);
				}													
			}	
		});	
		for(var i = 0; i < SPCreateOrder._mapProduct.size(); i++) {			
			var obj = SPCreateOrder._mapProduct.get(SPCreateOrder._mapProduct.keyArray[i]);	
			if (obj != null) {
				
			}
		}
		if(!flag){
			$('#productRow_'+index).focus();
			return false;
		}
		var arrDelete = SPCreateOrder.preparePayment(true);
		for(var i=0;i<arrDelete.length;++i){
			var obj = arrDelete[i];
		}	
		enable('payment');			
		disabled('btnOrderSave');
		disabled('btnOrderReset');
		SPCreateOrder.showTotalGrossWeightAndAmount();
		$('.easyui-dialog').dialog('close');		
		return false;
	},	
	
	/**
	 * tungmt
	 * Kiểm tra dòng trên grid có CTHTTM là KM tay hay ko
	 */
	checkCTHTTM:function(r){
		var htSale = $('#gridSaleData').handsontable('getInstance');
		var cthttm = htSale.getDataAtCell(r, COLSALE.PROGRAM);
		if (cthttm != null && cthttm.length > 0 && GridSaleBusiness.listCSCode != null && GridSaleBusiness.listCSCode.length > 0) {
			for (var i = 0 ; i < GridSaleBusiness.listCSCode.length ; i++) {//duyệt qua lst CT HTTM nếu có thì chuyển qua kho KM
				if (GridSaleBusiness.listCSCode[i] == cthttm) {
					return true;
				}
			}
		}
		return false;
	},
	/**
	 * tungmt
	 * Kiểm tra dòng trên grid có CTHTTM là KM tay hay ko
	 */
	checkCTHTTMValue:function(cthttm){
		if (cthttm == undefined || cthttm == null || cthttm == '') {
			return false;
		}
		if (cthttm != null && cthttm.length > 0 && GridSaleBusiness.listCSCode != null && GridSaleBusiness.listCSCode.length > 0) {
			for (var i = 0 ; i < GridSaleBusiness.listCSCode.length ; i++) {//duyệt qua lst CT HTTM nếu có thì chuyển qua kho KM
				if (GridSaleBusiness.listCSCode[i] == cthttm) {
					return true;
				}
			}
		}
		return false;
	},
	/**
	 * Ham mo dialog goi y ton kho dap ung sp theo Kho
	 */
	openSelectProductWarehouseDialog: function(productCode, index, productWarehouse){
		var orderNumber = $('#orderNumber').val();
		// if (orderNumber !== undefined && !isNullOrEmpty(orderNumber)) {
		// 	return;
		// }
		$('#productWarehouseDialogContainer').show();
		$('.clsProductVOQuantity').bind('keydown',function(e) {
			var index = $(this).attr('index');
			var isPressed = false;
			if (e.keyCode == keyCodes.ARROW_DOWN) {
				++index;
				isPressed = true;
			} else if (e.keyCode == keyCodes.ARROW_UP) {
				--index;
				isPressed = true;
			} 
			if (index > -1 && isPressed == true) {
				$('#productRow_'+index).focus();
			}
		});
		$('#indexTmpToAddRow').val(index);
		var product = GridSaleBusiness.mapSaleProduct.get(productCode);
		var warehouseType = WarehouseType.SALE;
		if (productWarehouse === SPCreateOrder.productWarehouseType.PROMOTION_PRODUCT) {
			product = $('#gridPromotionProduct').datagrid('getRows')[index];
			warehouseType = WarehouseType.PROMO;
		} else if (productWarehouse == SPCreateOrder.productWarehouseType.SALE_ORDER_PROMOTION) {
			product = $('#gridSaleOrderPromotion').datagrid('getRows')[index];
			warehouseType = WarehouseType.PROMO;
		} else if (productWarehouse === SPCreateOrder.productWarehouseType.SALE_PRODUCT && SPCreateOrder.checkCTHTTM(index)) {
			warehouseType = WarehouseType.PROMO;
		} else if (productWarehouse === SPCreateOrder.productWarehouseType.KEY_SHOP) {
			product = $('#gridKeyShop').datagrid('getRows')[index];
			warehouseType = WarehouseType.PROMO;
		}
		var lstData = GridSaleBusiness.getWarehouseByType(product.productId, warehouseType);;
		var html = $('#productWarehouseDialog').html();
		$('#productWarehouseDialog').window('refresh');
		var title = "Tồn kho đáp ứng của sản phẩm " + Utils.XSSEncode(product.productCode + " - " + product.productName) + ' tương ứng các kho';
		$('#productWarehouseDialog').dialog({
			closed: false,
			title: title,
			modal: true,
			zIndex: 10000,
			onOpen: function() {
				$('#spProductWarehouseCode').html(Utils.XSSEncode(product.productCode));
				$('#spProductWarehouseName').html(Utils.XSSEncode(product.productName));
				$('.easyui-dialog #GeneralInforErrors').html('').hide();
				SPCreateOrder._editingProductCode = product.productCode;
				try {
					SPCreateOrder._selectingIndex = Number(index);
				} catch (e) {
					// pass through
				}
				Utils.bindAutoSearch();
	        	$("#productWarehouseGrids").datagrid({
	        		//url: '/sale-product/create-order/search-product-by-warehouse',  
	        		data: lstData,
	        		fitColumns:true,
	        		pagination : true,
	        		width: $('#productWarehouseDialog').width()-25,
	        		scrollbarSize:0,
	        		singleSelect:true,
	        		rownumbers:true,cache: false,
	        		columns:[[
	        			{field:'warehouseCode',title:'Kho',width:$('#productWarehouseDialog').width() - 35 - 120 - 80 - 100 - 130, align:'left', formatter: function(value, row, index) {
	        				return '<span class="prStyle'+index+'">'+Utils.XSSEncode(row.warehouseName)+'</span>';
	        			}},
	        			{field:'availableQuantity',title: 'Tồn kho đáp ứng' /*isNullOrEmpty(SPAdjustmentOrder.orderId)? 'Tồn kho đáp ứng':'Tồn kho thực tế'*/,
	        				align:'right',width:130, formatter: function(value, row, index) {
	        					var value = Number(row.availableQuantity);	  
	        					/*
	        					if(!isNullOrEmpty(SPAdjustmentOrder.orderId)){
	        						value = Number(row.stockQuantity);
	        					}
	        					*/
	        					var displayStr = formatQuantityEx(value, row.convfact);	        					
	        					return '<span class="prStyle'+index+'">' + displayStr + '</span>';
	        				}
	        			},  
	        			{field:'quantity',title:'Số lượng',width:130, algin:'center', formatter: function(value, row, index) {
	        					var id = "warehouseRow_" + index + "_" + productWarehouse;
	        					return '<input id=\"' + id + '\" index="'+index+'" warehouseId="'+row.warehouseId+'" maxlength="8" type="text" style="width: 65px; margin-bottom: 0px;" onkeypress="NextAndPrevTextField(event,this,\'clsProductVOQuantity\')" class="clsProductVOQuantity InputTextStyle InputText1Style prStyle'+index+'" size="10"/>&nbsp;';
	        				}
	        			}, 
	        		]],	        		
	        		onBeforeLoad: function() {
	        			
	        		},
	        		onLoadSuccess: function(data) {
	        			$('.clsProductVOQuantity').css('text-align','right').css('padding-right','5px');
						$('.ccSupportCode').css('text-align','left').css('padding-left','5px');
						Utils.bindFormatOnTextfieldInputCss('clsProductVOQuantity',Utils._TF_NUMBER_CONVFACT);
						$('.datagrid-header-rownumber').html('STT');
//						$(window).resize();
						$('#warehouseRow_0' + productWarehouse).focus();
						setTimeout(function(){
							CommonSearchEasyUI.fitEasyDialog("productWarehouseDialog");
						}, 100);
						/*
						 * restore data from main grid to dialog's grid
						 */
						SPCreateOrder._selectingProductWarehouseType = productWarehouse;
						var p = GridSaleBusiness.mapSaleProduct.get(SPCreateOrder._editingProductCode);
						if (productWarehouse === SPCreateOrder.productWarehouseType.SALE_PRODUCT) {
							var htSale = $('#gridSaleData').handsontable('getInstance');
							var htData = htSale.getData();
							var programAtSelectingRow = htData[SPCreateOrder._selectingIndex][COLSALE.PROGRAM];
							var convfact = GridSaleBusiness.mapSaleProduct.get(SPCreateOrder._editingProductCode).convfact || 0;
							var _inputQuantityMap = new Map();							
							for (var i=0; i < htData.length; i++) {
								var productCode = htSale.getDataAtCell(i, COLSALE.PRO_CODE);
								var program = htSale.getDataAtCell(i, COLSALE.PROGRAM);
								if (productCode !== null && productCode !== undefined 
									&& SPCreateOrder._editingProductCode !== null && SPCreateOrder._editingProductCode !== undefined
									&& productCode === SPCreateOrder._editingProductCode
									&& program == programAtSelectingRow) {
									var quantity = htSale.getDataAtCell(i,COLSALE.QUANTITY);
									var retailQuantity = 0, packageQuantity = 0;
									if (quantity != null && quantity != '') {
										if (quantity.indexOf('/') == -1) {
											retailQuantity = Number(quantity);
										} else {
											var arrQuantity = quantity.split('/');
											packageQuantity = Number(arrQuantity[0]);
											retailQuantity = Number(arrQuantity[1]);
										}
									}
									var warehouseId = GridSaleBusiness._mapWarehouseIdSelect.get(i);
									if (_inputQuantityMap.get(warehouseId) == null) {
										_inputQuantityMap.put(warehouseId, '0/0');
									}
									var currentQuantity = _inputQuantityMap.get(warehouseId).split('/');//biến nội bộ nên sẽ có '/'
									currentQuantity[0] = Number(currentQuantity[0]) + Number(packageQuantity);
									currentQuantity[1] = Number(currentQuantity[1]) + Number(retailQuantity);
									_inputQuantityMap.put(warehouseId, currentQuantity[0]+'/'+currentQuantity[1]);
								}
							}
							_inputQuantityMap.keyArray.forEach(function(warehouseId){
								// set input quantity
								var selector = '#productWarehouseDialog input[id*=warehouseRow][warehouseid="' + warehouseId + '"]';
								$(selector).val(_inputQuantityMap.get(warehouseId));
							});

							// set remain quantity
							var programAtSelectingRow = htData[SPCreateOrder._selectingIndex][COLSALE.PROGRAM];
							data.rows.forEach(function(item, index){
								var selector = '#productWarehouseDialog input[id*=warehouseRow][warehouseid="' + item.warehouseId + '"]';
								/*var warehousesOfProduct = SPCreateOrder._mapPRODUCTSbyWarehouseOnGrid.get(p.productId) || [];
								var processingWarehouse = warehousesOfProduct.filter(function(item2){
															return item2.warehouseId == item.warehouseId;
														});
								var remainQuantity = (_inputQuantityMap.get(item.warehouseId) || 0) + (processingWarehouse.length != 0 ? processingWarehouse[0].availableQuantity : 0);*/
//								var warehousesOfProduct = SPCreateOrder._mapPRODUCTSbyWarehouse.get(p.productId) || [];
								var warehousesOfProduct = GridSaleBusiness.getWarehouseByType(p.productId) || [];
								var processingWarehouse = warehousesOfProduct.filter(function(item2){
															return item2.warehouseId == item.warehouseId;
														});
								var qtt = 0;
								for (var ij = 0, szj = htData.length; ij < szj; ij++) {
									var r = htData[ij];
									var warehouseId = GridSaleBusiness._mapWarehouseIdSelect.get(ij);
									if (r[COLSALE.PRO_CODE] == SPCreateOrder._editingProductCode
										&& warehouseId == item.warehouseId
										&& programAtSelectingRow != r[COLSALE.PROGRAM]) {
										qtt += Number(StockValidateInput.getQuantity(r[COLSALE.QUANTITY], convfact));
									}
								}
								var remainQuantity = (processingWarehouse.length > 0 ? processingWarehouse[0].availableQuantity : 0) - qtt;
								$(selector).parent().append('<span>(' + GridSaleBusiness.formatStockQuantity(remainQuantity, convfact).toString() + ')</span>');
								// save temp data to each row in grid for validating
								$("#productWarehouseGrids").datagrid('getRows')[index].remainQuantity = remainQuantity;
								$(selector).data('remainQuantity', remainQuantity);
								// custom display style
								setTimeout(function(){
									$(selector).parent().css('line-height', $(selector).parent().height() + 'px');
								}, 200);
							});
						} else if (productWarehouse === SPCreateOrder.productWarehouseType.PROMOTION_PRODUCT ||
									productWarehouse === SPCreateOrder.productWarehouseType.SALE_ORDER_PROMOTION) {
							var rows = null;
							var promotionProgramGroup = null;
							if (productWarehouse === SPCreateOrder.productWarehouseType.PROMOTION_PRODUCT) {
								rows = $('#gridPromotionProduct').datagrid('getRows');
								promotionProgramGroup = SPCreateOrder._promotionProgramGroup;
							} else if (productWarehouse === SPCreateOrder.productWarehouseType.SALE_ORDER_PROMOTION) {
								rows = $('#gridSaleOrderPromotion').datagrid('getRows');
								promotionProgramGroup = SPAdjustmentOrder._promotionProgramGroup;
							}
							
							if (rows !== null && rows !== undefined && rows.length !== 0) {
								try {
									var selectingRow = rows[Number(index)];
									var key = selectingRow.programCode + '_' + selectingRow.productGroupId + '_' + selectingRow.levelPromo /*+ '_' + selectingRow.changeProduct*/;
									if (promotionProgramGroup !== null) {
										var rowsInGroup = promotionProgramGroup.get(key);
										rowsInGroup.forEach(function(item, idx) {
											if (item.productCode == SPCreateOrder._editingProductCode) {
												var quantity = item.quantity,
												warehouseId = item.warehouse.id;
												if (!warehouseId) { warehouseId = item.warehouse.warehouseId; }
												var selector = '#productWarehouseDialog input[id*=warehouseRow][warehouseid="' + warehouseId + '"]';
												$(selector).val(quantity);
											}
										});
									}									
								} catch (e){
									// pass through
								}
							}
						} else if (productWarehouse == SPCreateOrder.productWarehouseType.KEY_SHOP) {
							SPCreateOrder.initChooseWarehouseKeyShop(data, index);
						}
	        		}
	        	});
	        },
	        onClose : function(){
	        	$('#productWarehouseDialog').html(html);
	        	$('#productWarehouseDialogContainer').hide();
	        	$('#productWarehouseDialogContainer .GridSection').html('<table id="productWarehouseGrids"></table>');
	        }
		});
	},

	/**
	 * Ham nhap so luong ton kho dap ung cua sp theo Kho
	 */
	selectProductByWarehouse: function(product) {
		flag = true;
		var index = $('#indexTmpToAddRow').val();
		var mapWarehouseQuantity = new Map();
		var defaultWarehouseObj = null;
		var totalQuantity = 0;	// use to check whether total quantity exceed max promotion quota
		var showErrMsgFunc = function(msg) {
			$('.easyui-dialog #GeneralInforErrors').html(msg).show();
			setTimeout(function() {
				$('.easyui-dialog #GeneralInforErrors').hide();
			}, 3000);
		}
		$('.easyui-dialog .clsProductVOQuantity').each(function(){
			var obj = $(this);
			var data = {};			
			var rowId = obj.attr('id').split('_')[1];
			var warehouseId = obj.attr('warehouseId');
			var cRow = $("#productWarehouseGrids").datagrid('getRows')[rowId];
			var convfact = cRow['convfact'];
			var availableQuantity = cRow.availableQuantity;//cRow['remainQuantity'] || $(this).data('remainQuantity');
			var stock = cRow['stock'];
			var warehouseName = $("#productWarehouseGrids").datagrid('getRows')[rowId]['warehouseName'];
			if (obj.val().length>0) {
				var quantity = obj.val();
				data.warehouseId = warehouseId;
				data.warehouseName = warehouseName;
				data.convfact =convfact;
				data.convfact_value = quantity;
				data.quantity = StockValidateInput.getQuantity(quantity,data.convfact);
				data.availableQuantity = availableQuantity;
				data.stock = stock;
				data.warehouse = $("#productWarehouseGrids").datagrid('getRows')[rowId];
				totalQuantity += Number(data.quantity);
				if(isNaN(data.quantity) || Number(data.quantity)<0){
					flag = false;index = rowId;
					var errMsg = 'Giá trị số lượng nhập vào kho '+ Utils.XSSEncode(warehouseName) +' không hợp lệ';
					if (Number(data.quantity) < 0) {
						errMsg = 'Giá trị số lượng phải là số nguyên dương.';
					}
					$('.easyui-dialog #GeneralInforErrors').html(errMsg).show();

					return false;
				} else if (data.quantity > data.availableQuantity) {
					flag = false;index = rowId;
					$('.easyui-dialog #GeneralInforErrors').html('Giá trị số lượng nhập vào kho '+ Utils.XSSEncode(warehouseName) +' lớn hơn tồn kho đáp ứng.').show();
					return false;
				}
				if (data.quantity > 0) {
					mapWarehouseQuantity.put(data.warehouseId,data);
				}
				if (defaultWarehouseObj === null 
					&& !isNaN(data.availableQuantity) && Number(data.availableQuantity) > 0) {
					defaultWarehouseObj = data;
				}
			}
		});
		if (mapWarehouseQuantity.size() === 0 && defaultWarehouseObj !== null) {
			mapWarehouseQuantity.put(defaultWarehouseObj.warehouseId, defaultWarehouseObj);
		}
		if (flag) {
			var editingProductCode = SPCreateOrder._editingProductCode !== null ? SPCreateOrder._editingProductCode : '';
			var selectingIndex = SPCreateOrder._selectingIndex;
			if (SPCreateOrder._selectingProductWarehouseType === SPCreateOrder.productWarehouseType.SALE_PRODUCT 
				&& mapWarehouseQuantity.size() !== 0) {
				var newIndex = index;
				var htSale = $('#gridSaleData').handsontable('getInstance');
				/*
				 * xoa het cac san pham (cung ma voi san pham dang chon de thao tac tren dialog & cung ma CT HTTM) da chon truoc do trong danh sach san pham ban
				 */
				var saleProducts = htSale.getData();
				var numSaleProduct = saleProducts[saleProducts.length - 1][0] === null ? saleProducts.length - 1 : saleProducts.length;
				var programAtSelectingRow = saleProducts[SPCreateOrder._selectingIndex][COLSALE.PROGRAM];
				for (var k = numSaleProduct - 1; k >= 0; k--) {
					var productCodeAtRowK = htSale.getDataAtCell(k, COLSALE.PRO_CODE);
					var program = htSale.getDataAtCell(k, COLSALE.PROGRAM);
					if (productCodeAtRowK !== null && productCodeAtRowK !== undefined
						&& productCodeAtRowK.toString() === editingProductCode.toString()
						&& program == programAtSelectingRow) {
						GridSaleBusiness.restoreWarehouseAvailableQuantityWhenDeleteRow(k, editingProductCode);
						SPCreateOrder._isRemovingRow = true;
						htSale.alter('remove_row', k);
					}
				}
				/*
				 * thay the bang cac so luong nhap tren dialog
				 */
				saleProducts = htSale.getData();
				numSaleProduct = saleProducts[saleProducts.length - 1][0] === null ? saleProducts.length - 1 : saleProducts.length;
				var emptyRowIndex = saleProducts.length - 1;
				var quantityToUpdateMap = new Map();
				SPCreateOrder._selectedWarehouse = new Map();
				for (var i=0;i<mapWarehouseQuantity.size();i++) {
					var index = GridSaleBusiness._mapWarehouseIdSelect.keyArray[i];
					var warehouse = mapWarehouseQuantity.valArray[i];
					if (warehouse !== null && warehouse !== undefined && warehouse.availableQuantity > 0) {
						var productCode = htSale.getDataAtCell(index,COLSALE.PRO_CODE);
						if (numSaleProduct === saleProducts.length) {
							htSale.alter('insert_row');	// insert last empty row
							SPCreateOrder._isRemovingRow = false;
						} else {
							index = saleProducts.length - 1;
						}
						index = emptyRowIndex++;
						htSale.alter('insert_row', index);
						SPCreateOrder._isRemovingRow = false;
						$('#product_'+index).attr('warehouseId', warehouse.warehouseId);
						GridSaleBusiness._mapWarehouseIdSelect.put(index, warehouse.warehouseId);
						quantityToUpdateMap.put(index, warehouse.convfact_value);//nhập trên grid thế nào thì cho xuống thế đó
						var insertData = new Array();
						insertData.push(new Array(index, COLSALE.PRO_CODE, editingProductCode));
						insertData.push(new Array(index, COLSALE.WAREHOUSE, warehouse.warehouseName));
						//insertData.push(new Array(index, COLSALE.PROGRAM, programAtSelectingRow));
						SPCreateOrder._selectedWarehouse.put(index, {
							warehouseId: warehouse.warehouseId,
							quantity: quantityToUpdateMap.get(index),
							availableQuantity: warehouse.availableQuantity,
							insertData: insertData
						});
						
						saleProducts = htSale.getData();
						numSaleProduct = saleProducts[saleProducts.length - 1][0] === null ? saleProducts.length - 1 : saleProducts.length;
						index++;
						SPCreateOrder._isInsertProductFromDialogToGrid = true;
						SPCreateOrder._syncFlag.isSetDefaultProgram = false;
					}
				}

				if (SPCreateOrder._selectedWarehouse !== null && SPCreateOrder._selectedWarehouse !== undefined) {
					SPCreateOrder._selectedWarehouse.keyArray.forEach(function(item, index) {
						var data = SPCreateOrder._selectedWarehouse.get(item);
						htSale.setDataAtCell(data.insertData);
					});
				}

				// this is a trick haha, dont know why. f*ck ?!?
				// now i know why, but i dont want to fix this !!!
				var p = GridSaleBusiness.mapSaleProduct.get(editingProductCode);
				setTimeout(function() {
					SPCreateOrder._syncFlag.isCheckInputQuantityValid = false;
					quantityToUpdateMap.keyArray.forEach(function(item, index) {
						htSale.setDataAtCell(item, COLSALE.QUANTITY, quantityToUpdateMap.get(item));
						var qty = StockValidateInput.getQuantity(quantityToUpdateMap.get(item), p.convfact);
						GridSaleBusiness.changeWarehouseAvailableQuantity(p.productId, SPCreateOrder._selectedWarehouse.get(item).warehouseId, -1*qty);
						$('#product_' + item)
						.attr('warehouseId', SPCreateOrder._selectedWarehouse.get(item).warehouseId)
						.prop('warehouseId', SPCreateOrder._selectedWarehouse.get(item).warehouseId);
					});
					SPCreateOrder._isInsertProductFromDialogToGrid = false;
					SPCreateOrder._syncFlag.isCheckInputQuantityValid = true;
					SPCreateOrder._syncFlag.isSetDefaultProgram = true;
					SPCreateOrder._syncFlag.isPaymentButtonClickable = true;
					setTimeout(function(){
						quantityToUpdateMap.keyArray.forEach(function(item, index) {
							htSale.setDataAtCell(item, COLSALE.PROGRAM, programAtSelectingRow);
						});	
					}, 300);					
				}, 100);
				
//				setTimeout(function(){ htSale.selectCell(index,COLSALE.PRO_CODE);},300);
			} else if (SPCreateOrder._selectingProductWarehouseType === SPCreateOrder.productWarehouseType.PROMOTION_PRODUCT) {
				//console.log(mapWarehouseQuantity);
				var errMsg = null;
				if (mapWarehouseQuantity.keyArray.length === 0) {
					errMsg = "Vui lòng nhập số lượng.";
					showErrMsgFunc(errMsg);
					return false;
				}

				var $grid = $('#gridPromotionProduct');
				var rows = $grid.datagrid('getRows');
				var startIndexToInsertRow = null;
				var tempData = new Array();
				var patternRow = null;
				var selectingRow = rows[SPCreateOrder._selectingIndex];
				var isQttEditing = (selectingRow.isEdited == 0 && selectingRow.hasPortion != 1 && selectingRow.stock < selectingRow.maxQuantity);
				// check if total quantity exceed max promotion quota
				var maxPromotionProductQuotaQuantity = 0;
				if (SPCreateOrder._lstPromotionProductIsAuto !== null && SPCreateOrder._lstPromotionProductIsAuto !== undefined) {
					if (selectingRow.isEdited == 1) {
						for (var i = 0; i < SPCreateOrder._lstPromotionProductIsAuto.valArray.length; i++) {
							var item = SPCreateOrder._lstPromotionProductIsAuto.valArray[i];
							if (item.productCode.trim().indexOf(" ") < 0
								&& item.productCode != editingProductCode 
								&& item.programCode === selectingRow.programCode
								&& item.levelPromo == selectingRow.levelPromo
								&& item.productGroupId == selectingRow.productGroupId
								&& Number(item.changeProduct) === Number(selectingRow.changeProduct)
								&& (item.changeProduct == 1 || item.productCode == selectingRow.productCode)) {
								totalQuantity += item.quantity;
							}
						}
					}
					for (var i = 0; i < SPCreateOrder._lstPromotionProductIsAuto.valArray.length; i++) {
						var item = SPCreateOrder._lstPromotionProductIsAuto.valArray[i];
						if (item.productCode.trim().indexOf(" ") < 0
							&& item.productCode === editingProductCode 
							&& item.programCode === selectingRow.programCode
							&& item.levelPromo == selectingRow.levelPromo
							&& item.productGroupId == selectingRow.productGroupId
							&& Number(item.changeProduct) === Number(selectingRow.changeProduct)) {
							if (selectingRow.isEdited == 1 || isQttEditing) {
								maxPromotionProductQuotaQuantity = item.maxQuantity;
								break;
							}
							if (item.productCode.trim().indexOf(" ") < 0) {
								maxPromotionProductQuotaQuantity += (Number(item.quantity) < item.maxQuantity) ?
											item.quantity : item.maxQuantity;
								if (maxPromotionProductQuotaQuantity >= item.maxQuantity) {
									maxPromotionProductQuotaQuantity = item.maxQuantity;
									break;
								}
							}
						}
					}
				}
				
				if ((selectingRow.isEdited != 1 && !isQttEditing) && !isNaN(Number(totalQuantity)) && Number(totalQuantity) != maxPromotionProductQuotaQuantity) {
					errMsg = 'Tổng số lượng các kho phải bằng số lượng đang phân bổ cho sản phẩm (' + maxPromotionProductQuotaQuantity + ')';
					showErrMsgFunc(errMsg);
					return false;
				} else if (!isNaN(Number(totalQuantity)) && Number(totalQuantity) > maxPromotionProductQuotaQuantity) {
					errMsg = 'Tổng số lượng sản phẩm vượt quá số lượng sản phẩm tối đa được hưởng trong CTKM (' + maxPromotionProductQuotaQuantity + ')';
					showErrMsgFunc(errMsg);
					return false;
				}

				/*
				 * xoa cac dong san pham hien co, trong group dang chon
				 */
				var removedRow = SPCreateOrder._promotionProgramGroup.get(selectingRow.programCode + '_' + selectingRow.productGroupId + '_' + selectingRow.levelPromo /*+ '_' + selectingRow.changeProduct*/);
				if (removedRow !== null && removedRow !== undefined) {
					for (var k = removedRow.length-1; k >= 0; k--) {
						var item = removedRow[k];
						if (item.productCode === editingProductCode) {
							startIndexToInsertRow = k;
							if (patternRow === null) {
								patternRow = new Object(), copyObjectProperties(item, patternRow);
							}
							$grid.datagrid('deleteRow', Number(item.rownum) - 1);
						}
					}
				}

				/*
				 * prepare data
				 */
				var gridRows = $grid.datagrid('getRows');
				mapWarehouseQuantity.keyArray.forEach(function(item, index) {
					var row = new Object();
					copyObjectProperties(patternRow, row);					
					row.warehouse = mapWarehouseQuantity.get(item).warehouse;
					row.warehouse.id = row.warehouse.warehouseId;
					row.warehouse.warehouseId = row.warehouse.warehouseId;
					row.stock = row.warehouse.availableQuantity;
					row.quantity = mapWarehouseQuantity.get(item).quantity;
					if (selectingRow.isEdited != 1 && isQttEditing) {
						row.isEdited = 1;
					}
					tempData.push(row);
					gridRows.push(row);
				});

				/*
				 * sort rows by program_code, promo_level, can_change_product
				 */
				gridRows.sort(function(o1, o2){
					var id = '-1413001269113';
					if (o1.programCode != o2.programCode) {
						return o1.programCode > o2.programCode;
					}
					if (o1.productCode.trim().indexOf(" ") > 0) {
						return false;
					}
					if (o2.productCode.trim().indexOf(" ") > 0) {
						return true;
					}
					if (Number(o1.levelPromo) != Number(o2.levelPromo)) {
						return o1.levelPromo > o2.levelPromo;
					}
					if (!isNaN(o1.productGroupId) && !isNaN(o2.productGroupId)
						&& Number(o1.productGroupId) != Number(o2.productGroupId)) {
						return o1.productGroupId < o2.productGroupId;
					}
					if (Number(o1.changeProduct) != Number(o2.changeProduct)) {
						return Number(o1.changeProduct) > Number(o2.changeProduct);
					}
					if (o1.productCode != o2.productCode) {
						return o1.productCode > o2.productCode;
					}
					return o1.warehouse.seq > o2.warehouse.seq;
				});

				/*
				 * re-calculate row's sequence number
				 */
				SPCreateOrder._lstPromotionProductIsAuto = new Map();
				gridRows.forEach(function(item, index){
					gridRows[index].rownum = index + 1;
					SPCreateOrder._lstPromotionProductIsAuto.put(index, item);
				});

				$grid.datagrid('loadData', gridRows);
				SPCreateOrder.showTotalGrossWeightAndAmount();
			} else if (SPCreateOrder._selectingProductWarehouseType === SPCreateOrder.productWarehouseType.SALE_ORDER_PROMOTION) {
				var errMsg = null;
				if (mapWarehouseQuantity.keyArray.length === 0) {
					errMsg = "Vui lòng nhập số lượng.";
					showErrMsgFunc(errMsg);
					return false;
				}
				var $grid = $('#gridSaleOrderPromotion');
				var rows = $grid.datagrid('getRows');
				var startIndexToInsertRow = null;
				var tempData = new Array();
				var patternRow = null;
				var selectingRow = rows[SPCreateOrder._selectingIndex];
				var isQttEditing = (selectingRow.isEdited == 0 && selectingRow.hasPortion != 1 && selectingRow.stock < selectingRow.maxQuantity);
				var deletedIndexs = new Array();	// index cua cac dong can xoa
				// check if total quantity exceed max promotion quota
				var maxPromotionProductQuotaQuantity = 0;
				if (SPCreateOrder._lstPromotionProductForOrderIsAuto) {
					for (var i = 0; i < SPCreateOrder._lstPromotionProductForOrderIsAuto.valArray.length; i++) {
						var item = SPCreateOrder._lstPromotionProductForOrderIsAuto.valArray[i];
						if (item.productCode.trim().indexOf(" ") < 0
							&& item.productCode != editingProductCode 
							&& item.programCode === selectingRow.programCode
							&& item.levelPromo == selectingRow.levelPromo
							&& item.productGroupId == selectingRow.productGroupId
							&& Number(item.changeProduct) === Number(selectingRow.changeProduct)
							&& (item.changeProduct == 1 || item.productCode == selectingRow.productCode)) {
							totalQuantity += item.quantity;
						}
					}
					for (var i = 0; i < SPCreateOrder._lstPromotionProductForOrderIsAuto.valArray.length; i++) {
						var item = SPCreateOrder._lstPromotionProductForOrderIsAuto.valArray[i];
						if (item.promotionType != PromotionType.PROMOTION_FOR_ZV21) {
							if (item.productCode.trim().indexOf(" ") < 0
								&& item.productCode === editingProductCode 
								&& item.programCode === selectingRow.programCode 
								&& item.levelPromo === selectingRow.levelPromo
								&& Number(item.changeProduct) === Number(selectingRow.changeProduct)) {
								/*if (!maxPromotionProductQuotaQuantity) {
									maxPromotionProductQuotaQuantity = item.maxQuantity;
								}*/
								if (selectingRow.isEdited == 1 || isQttEditing) {
									maxPromotionProductQuotaQuantity = item.maxQuantity;
									//break;
								}
								if (item.productCode.trim().indexOf(" ") < 0) {
									maxPromotionProductQuotaQuantity += (Number(item.quantity) < item.maxQuantity) ?
												item.quantity : item.maxQuantity;
									if (maxPromotionProductQuotaQuantity >= item.maxQuantity) {
										maxPromotionProductQuotaQuantity = item.maxQuantity;
										//break;
									}
								}
								if (!patternRow) {
									patternRow = new Object(), copyObjectProperties(SPCreateOrder._lstPromotionProductForOrderIsAuto.get(i), patternRow);
								}
								deletedIndexs.push(i);
							}
						}
					}
				}
				
				if ((selectingRow.isEdited != 1 && !isQttEditing) && !isNaN(Number(totalQuantity)) && Number(totalQuantity) != maxPromotionProductQuotaQuantity) {
					errMsg = 'Tổng số lượng các kho phải bằng số lượng đang phân bổ cho sản phẩm (' + maxPromotionProductQuotaQuantity + ')';
					showErrMsgFunc(errMsg);
					return false;
				} else if (!isNaN(Number(totalQuantity)) && Number(totalQuantity) > maxPromotionProductQuotaQuantity) {
					errMsg = 'Tổng số lượng sản phẩm vượt quá số lượng sản phẩm tối đa được hưởng trong CTKM.';
					showErrMsgFunc(errMsg);
					return false;
				}

				/*
				 * prepare data
				 */
				var gridRows = $grid.datagrid('getRows');
				mapWarehouseQuantity.keyArray.forEach(function(item, index) {
					var row = new Object();
					copyObjectProperties(patternRow, row);					
					row.warehouse = mapWarehouseQuantity.get(item).warehouse;
					row.warehouse.id = row.warehouse.warehouseId;
					row.stock = row.warehouse.availableQuantity;
					row.quantity = mapWarehouseQuantity.get(item).quantity;
					if (selectingRow.isEdited != 1 && isQttEditing) {
						row.isEdited = 1;
					}
					tempData.push(row);
					gridRows.push(row);
				});

				/*
				 * xoa cac dong san pham hien co tuong ung voi san pham dang chon doi kho
				 */
				for (var k = deletedIndexs.length - 1; k >= 0; k--) {
					gridRows.splice(deletedIndexs[k], 1);
				}

				/*
				 * sort rows by program_code, promo_level, can_change_product
				 */
				gridRows.sort(function(o1, o2){
					var id = '-1413001269113';
					/*if (o1.groupId != o2.groupId) {
						return o1.groupId > o2.groupId;
					}*/
					if (o1.programCode != o2.programCode) {
						return o1.programCode > o2.programCode;
					}
					if (o1.productCode.trim().indexOf(" ") > 0) {
						return false;
					}
					if (o2.productCode.trim().indexOf(" ") > 0) {
						return true;
					}
					if (Number(o1.levelPromo) != Number(o2.levelPromo)) {
						return o1.levelPromo > o2.levelPromo;
					}
					if (!isNaN(o1.productGroupId) && !isNaN(o2.productGroupId)
						&& Number(o1.productGroupId) != Number(o2.productGroupId)) {
						return o1.productGroupId < o2.productGroupId;
					}
					if (Number(o1.changeProduct) != Number(o2.changeProduct)) {
						return Number(o1.changeProduct) > Number(o2.changeProduct);
					}
					if (o1.productCode != o2.productCode) {
						return o1.productCode > o2.productCode;
					}
					return o1.warehouse.seq > o2.warehouse.seq;
				});

				/*
				 * re-calculate row's sequence number
				 */
				SPCreateOrder._lstPromotionProductForOrderIsAuto = new Map();
				gridRows.forEach(function(item, index){
					gridRows[index].rownum = index + 1;
					SPCreateOrder._lstPromotionProductForOrderIsAuto.put(index, item);
				});

				SPCreateOrder.fillTablePromotionProductForOrder(SPCreateOrder._lstPromotionProductForOrderIsAuto);
				SPCreateOrder.showTotalGrossWeightAndAmount();
			} else if (SPCreateOrder._selectingProductWarehouseType === SPCreateOrder.productWarehouseType.KEY_SHOP) {
				SPCreateOrder.chooseWarehouseKeyShop(mapWarehouseQuantity);
			}
			SPCreateOrder._syncFlag.isPaymentButtonClickable = true;
			$('.easyui-dialog').dialog('close');
			SPCreateOrder.orderChanged = true;
			if (SPAdjustmentOrder._orderId > 0 && $("#payment").prop("disabled")) {
				enable("btnOrderSave");
			}
		}
	},
	
	/**
	 * Tinh tong tien va tong khoi luong cho don hang
	 */
	fillTotalAuto: function(){
		var totalWeight = parseFloat($('#totalWeightSale').html())+ parseFloat($('#totalWeightFree').html());
		var totalAmount = Utils.returnMoneyValue($('#totalAmountSale').html()) - Utils.returnMoneyValue($('#totalAmountFree').html());
		$('#totalWeight').html(formatFloatValue(totalWeight,2));
		$('#totalAmount').html(formatCurrency(totalAmount));
	},
	

	/**
	 * Ham tim kiem san pham
	 */
	searchProduct:function(){		
		$("#productGrids").datagrid('load',SPCreateOrder.getParamsSearchProduct());
		return false;
	},
	

	/**
	 * Ham xem tthong tin sp
	 */
	viewAddProduct:function(){
		var customerCode=$('#customerCode').val().trim();
		if(customerCode.length==0){
			$('#GeneralInforErrors').html('Vui lòng chọn mã khách hàng').show();
		}else{
			var data = new Object();
			Utils.getHtmlDataByAjax(data, "/sale-product/create-order/add-product",function(data) {
				$("#spSaleProductInfor").html(data).show();
				$("#spPromotionProductInfor").html('').hide();
			}, 'loading2', null);
		}		
	},
	
	/**
	 * Load thong tin cua khach hang khi chon khach hang
	 * @param customerCode
	 * @param staffCode
	 * @param isShowNVBHConfirm
	 */
	customerDetails:function(customerCode, staffCode, isShowNVBHConfirm){
		if(isNullOrEmpty(customerCode)){
			return;
		}
		var url = '/sale-product/create-order/customer-and-sale-staff-details'; //?customerCode='+customerCode;
		var params = {};
		params.customerCode = customerCode;
		params.shopCodeFilter = $('#shopCodeCB').combobox('getValue');
		params.orderDate = $('#orderDate').val().trim();
		if(!isNullOrEmpty(staffCode)) {
			params.salerCode = staffCode;
		}
		$('.ErrorMsgStyle').html('').hide();
		$('.loadingTitle').show();			
		Utils.getJSONDataByAjaxNotOverlay(params, url, function(data){
			$('.loadingTitle').hide();
			if(!data.error){
				$('#customerName').html(Utils.XSSEncode(data.customerName));
				$('#address').html(Utils.XSSEncode(data.address));
				var phone = "";
				if(data.phone != undefined && data.phone != null){
					phone += data.phone;
				}
				if(data.mobiphone != undefined && data.mobiphone != null){
					phone += (" / " + data.mobiphone);
				}
				$("#phone").html(Utils.XSSEncode(phone));
				$('#channelType').html(Utils.XSSEncode(data.channelTypeName));
				SPCreateOrder.enabledStaff();
				//Load lại combo NVBH
				Utils.bindStaffCbx('#staffSaleCode', data.lstStaffSale);
				//----------------------
				$('#cashierStaffCode').combobox('setValue', Utils.XSSEncode(data.cashierStaffCode));
				$('#transferStaffCode').combobox('setValue', Utils.XSSEncode(data.transferStaffCode));
				$('#staffSaleCode').combobox('setValue', Utils.XSSEncode(data.staffSaleCode));
				var isFocus = false;
				if (isNullOrEmpty(data.cashierStaffCode) || data.cashierStaffCode == undefined) {
					$('#cashierStaffCode').next().find('input').focus();
					isFocus = true;
				}
				if (isNullOrEmpty(data.transferStaffCode)|| data.transferStaffCode == undefined) {
					if(!isFocus){
						$('#transferStaffCode').next().find('input').focus();
						isFocus = true;
					}
				}
				if (isNullOrEmpty(data.staffSaleCode) || data.staffSaleCode == undefined) {
					if(!isFocus){
						$('#staffSaleCode').next().find('input').focus();
					}
					SPCreateOrder.isShowDialogNVBHConfirm = true;
				}else{
					$('#carId').next().find('input').focus();
				}
				
				/**Lay danh sach chuong trinh ho tro thuong mai**/
		        if($.isArray(data.commercials) && data.commercials.length > 0) {
		        	GridSaleBusiness.listCS = data.commercials;
		        	GridSaleBusiness.mapCS = new Map();
		        	GridSaleBusiness.listCSCode = new Array();
					for(var i = 0; i < data.commercials.length; i++) {
						GridSaleBusiness.mapCS.put(GridSaleBusiness.listCS[i].code, GridSaleBusiness.listCS[i]);
						GridSaleBusiness.listCSCode.push(GridSaleBusiness.listCS[i].code);
					}
		        }
		        
				/** Lay danh sach san pham ban*/
				GridSaleBusiness.listSaleProduct = data.listSaleProduct;
				GridSaleBusiness.mapSaleProduct = new Map();
				GridSaleBusiness.listSaleProductCode = new Array();
				if(GridSaleBusiness.listSaleProduct != null && GridSaleBusiness.listSaleProduct != undefined) {
					for(var i = 0; i < GridSaleBusiness.listSaleProduct.length; i++) {
						var saleProduct = GridSaleBusiness.mapSaleProduct.get(GridSaleBusiness.listSaleProduct[i].productCode); 
						if (saleProduct == null) {
							GridSaleBusiness.mapSaleProduct.put(GridSaleBusiness.listSaleProduct[i].productCode, GridSaleBusiness.listSaleProduct[i]);
							GridSaleBusiness.listSaleProductCode.push(Utils.XSSEncode(GridSaleBusiness.listSaleProduct[i].productCode + ' - ' + GridSaleBusiness.listSaleProduct[i].productName));
						} else if (saleProduct.promotionProgramCode != undefined && saleProduct.promotionProgramCode != null && saleProduct.promotionProgramCode != '') {//các dòng chỉ có 1 CTKM, nên khi for để lấy 1 dòng sẽ gộp các CTKM vào 1 
							saleProduct.promotionProgramCode += ', ' + Utils.XSSEncode(GridSaleBusiness.listSaleProduct[i].promotionProgramCode);
						} else if (GridSaleBusiness.listSaleProduct[i].promotionProgramCode != undefined && GridSaleBusiness.listSaleProduct[i].promotionProgramCode != null){
							saleProduct.promotionProgramCode = Utils.XSSEncode(GridSaleBusiness.listSaleProduct[i].promotionProgramCode);
						}
					}
//					$('#gridSaleContainer').css('height', 220);
					GridSaleBusiness.resetSaleGrid();
				}else{
					GridSaleBusiness.disableSaleGrid();
				}
				SPCreateOrder.loadGridFree();
				OpenProductPromotion.loadOpenProductGrid(null);
				SPCreateOrder.loadGridAccumulative({});
				SPCreateOrder._currentCustomerCode = data.shortCode;
				SPCreateOrder._currentSaleStaff = $('#staffSaleCode').combobox('getValue');
				//Lấy dữ liệu keyshop
				SPCreateOrder.getKeyShopData(data);
				isLoadKeyShopGrid = 1;//request lấy keyshop nếu không có thì đóng div keyshop lại
				SPCreateOrder.loadKeyShopGrid(SPCreateOrder._lstKeyShop);
				//----------------------------
			}else{				
				$('#GeneralInforErrors').html(data.errMsg).show();
				$('#customerName').html('');
				$('#address').html('');
				$('#phone').html('');
				SPCreateOrder.resetStaff();
				GridSaleBusiness.disableSaleGrid();
				return false;
			}
		});		
		enable('payment');	
		disabled('btnOrderSave');
		disabled('approveButton');
		enable('saleProductAddProduct');
	},	
	
	/**
	 * Load thong tin cua khach hang khi chon khach hang
	 * @param customerCode
	 * @param staffCode
	 * @param isShowNVBHConfirm
	 */
	staffDetails:function(customerCode, staffCode, isShowNVBHConfirm){
		var shopCodeFilter = $('#shopCodeCB').combobox('getValue');
		var orderDate = $('#orderDate').val();
		if(isNullOrEmpty(customerCode)){
			return;
		}
		var url = '/sale-product/create-order/customer-and-sale-staff-details?customerCode='+customerCode;
		if (!isNullOrEmpty(shopCodeFilter)) {
			url = url + '&shopCodeFilter=' + shopCodeFilter;
		};
		if(!isNullOrEmpty(staffCode)) {
			url = url + '&salerCode=' + staffCode;
		}
		if (!isNullOrEmpty(orderDate)) {
			url = url + '&orderDate=' + orderDate;
		}
		$('.ErrorMsgStyle').html('').hide();
		$('.loadingTitle').show();			
		$.getJSON(encodeURI(url), function(data) {		
			$('.loadingTitle').hide();
			if(!data.error){
				$('#customerName').html(Utils.XSSEncode(data.customerName));
				$('#address').html(Utils.XSSEncode(data.address));
				$('#phone').html(Utils.XSSEncode(data.phone));
				$('#channelType').html(Utils.XSSEncode(data.channelTypeName));
				SPCreateOrder.enabledStaff();
				$('#staffSaleCode').combobox('setValue', Utils.XSSEncode(data.staffSaleCode));
				var isFocus = false;
				if (isNullOrEmpty(data.cashierStaffCode) || data.cashierStaffCode == undefined) {
					$('#cashierStaffCode').next().find('input').focus();
					isFocus = true;
				}
				if (isNullOrEmpty(data.transferStaffCode)|| data.transferStaffCode == undefined) {
					if(!isFocus){
						$('#transferStaffCode').next().find('input').focus();
						isFocus = true;
					}
				}
				if (isNullOrEmpty(data.staffSaleCode) || data.staffSaleCode == undefined) {
					if(!isFocus){
						$('#staffSaleCode').next().find('input').focus();
					}
					SPCreateOrder.isShowDialogNVBHConfirm = true;
				}else{
					$('#carId').next().find('input').focus();
				}
				
				/**Lay danh sach chuong trinh ho tro thuong mai**/
		        if($.isArray(data.commercials) && data.commercials.length > 0) {
		        	GridSaleBusiness.listCS = data.commercials;
		        	GridSaleBusiness.mapCS = new Map();
		        	GridSaleBusiness.listCSCode = new Array();
					for(var i = 0; i < data.commercials.length; i++) {
						GridSaleBusiness.mapCS.put(GridSaleBusiness.listCS[i].code, GridSaleBusiness.listCS[i]);
						GridSaleBusiness.listCSCode.push(GridSaleBusiness.listCS[i].code);
					}
		        }
		        
				/** Lay danh sach san pham ban*/
				GridSaleBusiness.listSaleProduct = data.listSaleProduct;
				GridSaleBusiness.mapSaleProduct = new Map();
				GridSaleBusiness.listSaleProductCode = new Array();
				if(GridSaleBusiness.listSaleProduct != null && GridSaleBusiness.listSaleProduct != undefined) {
					for(var i = 0; i < GridSaleBusiness.listSaleProduct.length; i++) {
						var saleProduct = GridSaleBusiness.mapSaleProduct.get(GridSaleBusiness.listSaleProduct[i].productCode); 
						if (saleProduct == null) {
							GridSaleBusiness.mapSaleProduct.put(GridSaleBusiness.listSaleProduct[i].productCode, GridSaleBusiness.listSaleProduct[i]);
							GridSaleBusiness.listSaleProductCode.push(Utils.XSSEncode(GridSaleBusiness.listSaleProduct[i].productCode + ' - ' + GridSaleBusiness.listSaleProduct[i].productName));
						} else if (saleProduct.promotionProgramCode != undefined && saleProduct.promotionProgramCode != null && saleProduct.promotionProgramCode != '') {//các dòng chỉ có 1 CTKM, nên khi for để lấy 1 dòng sẽ gộp các CTKM vào 1 
							saleProduct.promotionProgramCode += ', ' + Utils.XSSEncode(GridSaleBusiness.listSaleProduct[i].promotionProgramCode);
						} else if (GridSaleBusiness.listSaleProduct[i].promotionProgramCode != undefined && GridSaleBusiness.listSaleProduct[i].promotionProgramCode != null){
							saleProduct.promotionProgramCode = Utils.XSSEncode(GridSaleBusiness.listSaleProduct[i].promotionProgramCode);
						}
					}
//					$('#gridSaleContainer').css('height', 220);
					GridSaleBusiness.resetSaleGrid();
				}else{
					GridSaleBusiness.disableSaleGrid();
				}
				SPCreateOrder.loadGridFree();
				SPCreateOrder._currentCustomerCode = Utils.XSSEncode(data.shortCode);
				SPCreateOrder._currentSaleStaff = $('#staffSaleCode').combobox('getValue');
			}else{				
				$('#GeneralInforErrors').html(Utils.XSSEncode(data.errMsg)).show();
				$('#customerName').html('');
				$('#address').html('');
				$('#phone').html('');
				SPCreateOrder.resetStaff();
				GridSaleBusiness.disableSaleGrid();
				return false;
			}
		});		
		enable('payment');	
		disabled('btnOrderSave');
		disabled('approveButton');
		enable('saleProductAddProduct');
	},
	
	
	/**
	 * Ham xem thong tin sp
	 */
	viewProductDetails:function(rowId){
		showLoadingIcon();
		var obj = GridSaleBusiness.mapSaleProduct.get(rowId);
		$('#GeneralInforErrors').html('').hide();
		var customerCode = null;
		var staffCode = null;
		var shopCodeFilter = null;
		if (window.location.href.indexOf("/commons/view-info") < 0) {
			customerCode = $('#customerCode').val().trim();
			staffCode = $('#staffSaleCode').combobox('getValue');
			shopCodeFilter = $('#shopCodeCB').combobox('getValue');
			if(staffCode.length==0){
				$('#GeneralInforErrors').html('Vui lòng chọn mã NVBH').show();
				return false;
			}
			if(customerCode.length==0){
				$('#GeneralInforErrors').html('Vui lòng chọn mã khách hàng').show();
				return false;
			}
		} else {
			customerCode = $("#customerCodeLink").html().trim();
			staffCode = $("#saleStaff").html().trim();
		}
		var productCode = rowId;
		if (obj) {
			productCode = obj.productCode;
		}
		if(productCode!=''){
			var url = '/commons/products-details?productCode='+productCode+'&customerCode='+customerCode+'&salerCode='+staffCode+'&shopCodeFilter='+shopCodeFilter;
			if (SPAdjustmentOrder._orderId) {
				url = url + "&orderId="+ SPAdjustmentOrder._orderId;
			}
			$.getJSON(url, function(data) {
				hideLoadingIcon();
				if(!data.error){
					var isSaleProduct = true;
					CommonSearch.showProductInfo(Utils.XSSEncode(data.product.productCode), Utils.XSSEncode(data.product.productName), 
							data.product.convfact,
							StockValidateInput.formatStockQuantity(data.avaiableQuantity, data.product.convfact),
							data.price, StockValidateInput.getQuantity(data.avaiableQuantity, data.product.convfact), data.promotion, isSaleProduct, data.priceThung);
				}else{
					$('#GeneralInforErrors').html(data.errMsg).show();
				}					
			});				
		}	
	},
	
	/**
	 * prepare for payment
	 */
	initSalesStaffInfor:function(selectorCode){
		if($('#customerCode').val().trim().length==0){
			$('#GeneralInforErrors').html('Vui lòng chọn khách hàng trước khi chọn NVBH,NVTT,NVGH').show();
		}else{
			$('#GeneralInforErrors').html('').hide();
			var arrParams = new Array();
			var param = new Object();
			param.name='shopCode';
			param.value=$('#shopCode').val();
			arrParams.push(param);
			CommonSearch.searchSaleStaffOnDialog(function(data){
				$('#' +selectorCode).val(Utils.XSSEncode(data.code));
			},arrParams);
		}
	},
	
	initCashierStaffInfor:function(selectorCode){
		if($('#customerCode').val().trim().length==0){
			$('#GeneralInforErrors').html('Vui lòng chọn khách hàng trước khi chọn NVBH,NVTT,NVGH').show();
		}else{
			$('#GeneralInforErrors').html('').hide();
			var arrParams = new Array();
			var param = new Object();
			param.name='shopCode';
			param.value=$('#shopCode').val();
			arrParams.push(param);
			CommonSearch.searchCashierStaffOnDialog(function(data){
				$('#' +selectorCode).val(Utils.XSSEncode(data.code));
			},arrParams);
		}
	},
	showPromotionProgramDetail:function(code){
		var html = $('#createOrderPromotionProgramDetailContainer').html();
		$('#createOrderPromotionProgramDetailContainer').show();
		$('#createOrderPromotionProgramDetail').dialog({
			closed: false,  
			cache: false,  
		    modal: true,
		    onOpen: function() {
		    	SPSearchSale.showDialogZIndex('createOrderPromotionProgramDetail');
		    	$.getJSON('/commons/promotion-detail?ppCode='+code, function(data) {
					if(!data.error){
						if (data.isPromotion) {
							$('.easyui-dialog #createOrderPromotionProgramDetail').html('');
							$('.easyui-dialog #coPPDetailCode').html(data.program.promotionProgramCode!=null? Utils.XSSEncode(data.program.promotionProgramCode):'');
							$('.easyui-dialog #coPPDetailName').html(data.program.promotionProgramName!=null? Utils.XSSEncode(data.program.promotionProgramName):'');
							$('.easyui-dialog #coPPDetailType').html(data.program.type!=null? Utils.XSSEncode(data.program.type):'');
							$('.easyui-dialog #coPPDetialFormat').html(data.program.proFormat!=null? Utils.XSSEncode(data.program.proFormat):'');
							$('.easyui-dialog #coPPDetailFromDate').html(Utils.XSSEncode(data.fromDate));
							$('.easyui-dialog #coPPDetailToDate').html(Utils.XSSEncode(data.toDate));
							$('.easyui-dialog #coPPDetailDescription').html(data.program.description!=null? Utils.XSSEncode(data.program.description):'');
						} else {
							$('.easyui-dialog #createOrderPromotionProgramDetail').html('');
							$('.easyui-dialog #coPPDetailCode').html(Utils.XSSEncode(data.program.displayProgramCode));
							$('.easyui-dialog #coPPDetailName').html(data.program.displayProgramName!=null? Utils.XSSEncode(data.program.displayProgramName):'');
							$('.easyui-dialog #coPPDetailType').html("");
							$('.easyui-dialog #coPPDetialFormat').html('');
							$('.easyui-dialog #coPPDetailFromDate').html(Utils.XSSEncode(data.fromDate));
							$('.easyui-dialog #coPPDetailToDate').html(Utils.XSSEncode(data.toDate));
							$('.easyui-dialog #coPPDetailDescription').html('');
						}
					} else {
						$('.easyui-dialog #createOrderPromotionProgramDetail').html('');
						$('.easyui-dialog #coPPDetailCode').html('');
						$('.easyui-dialog #coPPDetailName').html('');
						$('.easyui-dialog #coPPDetailType').html('');
						$('.easyui-dialog #coPPDetialFormat').html('');
						$('.easyui-dialog #coPPDetailFromDate').html("");
						$('.easyui-dialog #coPPDetailToDate').html("");
						$('.easyui-dialog #coPPDetailDescription').html('');
					}
				});
		    },
		    onClose : function(){
		    	$('#createOrderPromotionProgramDetailContainer').html(html);
	        	$('#createOrderPromotionProgramDetailContainer').hide();
		    }
		});
	},
	
	
	initTransferStaffInfor:function(selectorCode){
		if($('#customerCode').val().trim().length==0){
			$('#GeneralInforErrors').html('Vui lòng chọn khách hàng trước khi chọn NVBH,NVTT,NVGH').show();
		}else{
			$('#GeneralInforErrors').html('').hide();
			var arrParams = new Array();
			var param = new Object();
			param.name='shopCode';
			param.value=$('#shopCode').val();
			arrParams.push(param);
			CommonSearch.searchTransferStaffOnDialog(function(data){
				$('#' +selectorCode).val(Utils.XSSEncode(data.code));
			},arrParams);
		}	
	},
	
	checkConfact:function(obj){
		if (obj != undefined && obj != null) {
			var reg = /[^0-9//]/;
			if(reg.test(obj)) return false;			
			var test = obj.split('/');
			if(test.length > 2) return false;
			if(test.length == 2 && test[0]=='' && test[1]=='') return false;
			if(test.length == 1 && test[0]=='') return false;
			return true;
		} else {
			return false;
		}
	},
	
	convert:function(convfact_value,convfact){
		if(convfact_value.indexOf('/')==-1){
			return convfact_value;
		}else{
			var arrCount = convfact_value.split('/');
			var bigUnit = parseInt(arrCount[0].trim());	
			var smallUnit = 0;
			if(arrCount.length>1){
				smallUnit = parseInt(arrCount[1].trim());
			}
			return bigUnit*convfact + smallUnit;
		}
	},

	autoSplitWarehouse: function(productId, saleOrderLot, warehouseType) {
		var promotionProductQuantityInWarehouse = new Map();
		var cloneProductQuantityInWarehouse = function(productId) {
			var productsWarehouses = GridSaleBusiness.getWarehouseOnGridByType(productId, warehouseType) || [];
			if (productsWarehouses.length != 0 && !promotionProductQuantityInWarehouse.get(productId)) {
				var tmpProductsWarehouses = [];
				$.extend(true, tmpProductsWarehouses, productsWarehouses);
				promotionProductQuantityInWarehouse.put(productId, tmpProductsWarehouses);
			}
		};
		var autoSplitWarehouse = function(saleOrderLot) {
			var warehouseOfProducts = promotionProductQuantityInWarehouse.get(saleOrderLot.stockTotal.product.id);
			var quantity = saleOrderLot.quantity;
			var out = [];
			if (warehouseOfProducts) {
				for (var i = 0; i < warehouseOfProducts.length && quantity > 0; i++) {
					if (warehouseOfProducts[i].availableQuantity != 0) {
						var distributedQuantity = Math.min(quantity, warehouseOfProducts[i].availableQuantity);
						var newSaleOrderLot = {};
						$.extend(true, newSaleOrderLot, saleOrderLot);
						newSaleOrderLot.quantity = distributedQuantity;
						// set stock
						var j = -1;
						var warehouses = GridSaleBusiness.getWarehouseOnGridByType(saleOrderLot.stockTotal.product.id);
						do {
							j++;
							newSaleOrderLot.warehouse = warehouses[j];
							newSaleOrderLot.stock = warehouses[j].availableQuantity;
							newSaleOrderLot.stockTotal.availableQuantity = warehouses[j].availableQuantity;
							newSaleOrderLot.stockTotal.warehouse = warehouses[j];
							newSaleOrderLot.stockTotal.id = null;
							newSaleOrderLot.stockTotal.quantity = null;
						} while (j < warehouses.length && warehouses[j].warehouseId != warehouseOfProducts[i].warehouseId);
						out.push(newSaleOrderLot);
						quantity -= distributedQuantity;
						warehouseOfProducts[i].availableQuantity -= distributedQuantity;
					}
				}
			}
			if (out.length == 0) {
				out.push(saleOrderLot);
			} else {
				if (quantity > 0) {
					out[out.length - 1].quantity += quantity;
				}
			}
			return out;
		};

		cloneProductQuantityInWarehouse(productId);
		var autoSplitedSaleOrderLot = autoSplitWarehouse(saleOrderLot);
		return autoSplitedSaleOrderLot;

	},

	setSelectingRowIndex: function(index) {
		SPCreateOrder._selectingIndex = index;
	},

	paymentPrepare: function(){
		var interval = setInterval(function() {
			if (SPCreateOrder._syncFlag.isPaymentButtonClickable && GridSaleBusiness._isvalidQuantity) {
				clearInterval(interval);
				SPCreateOrder.payment();
			} else {//moi bo sung
				clearInterval(interval);
			}
		}, 100);
	},
	
	/**
	 * Ham tinh tien
	 * 
	 */
	payment: function(key,mapId,typeSortKey){
		DEBUG ? console.log("--- do payment ---") : null;		
		$('#serverErrors').html('').hide();
		$('.ErrorMsgStyle').html('').hide();		
		var msg = Utils.getMessageOfRequireCheck('shopCode', 'Mã đơn vị');
		if(msg.length==0){
			msg = Utils.getMessageOfRequireCheck('customerCode', 'Mã khách hàng');		
		}		
		if(msg.length==0){
			msg = Utils.getMessageOfRequireCheck('staffSaleCode', 'Nhân viên bán hàng', true);
		}
		if(msg.length==0){
			msg = SPCreateOrder.checkOrderDate();
		}
		// KIEM TRA THONG TIN SAN PHAM NHAP 
		var mapProductQuantity = new Map();
		var mapProductQuantityPackage = new Map();
		var mapProductQuantityRetail = new Map();
		var mapProductWarehouse = new Map();
		var mapProductWarehouseQuantity = new Map();
		var quantityTotal = null;
		var quantityPackageTotal = null;
		var quantityRetailTotal = null;
		var warehouseStr = null;
		var productWarehouseQuantStr = null;
		var tmp = $('#gridSaleData').handsontable('getInstance');
		var row = tmp.getData();
		//PRO_CODE: 0, PRO_NAME: 1, PRICE: 2, WAREHOUSE: 3, STOCK: 4, QUANTITY: 5, AMOUNT: 6, DISCOUNT: 7, PROGRAM: 8, DELETE: 9
		var totalQuantityOfProducts = new Map();	// dung de validate tong so luong da nhap cua san pham co vuot qua tong ton kho cua san pham hay khong
		for (var index=0;index<row.length;index++) {
			quantityTotal = 0;
			quantityPackageTotal = 0;
			quantityRetailTotal = 0;
			var obj = SPCreateOrder.getObjectSaleProductInGrid(row[index],index);
			if (!isNullOrEmpty(obj.productCode)) {
				var errMsg = '';
				if (GridSaleBusiness._mapWarehouseIdSelect.get(index) == null) {
					errMsg = 'Chưa chọn kho cho sản phẩm '+Utils.XSSEncode(obj.productCode)+'. Vui lòng kiểm tra lại.';
				}
				if (isNullOrEmpty(errMsg) && isNaN(obj.quantity)) {
					errMsg = 'Giá trị số lượng sản phẩm '+Utils.XSSEncode(obj.productCode)+' phải là số nguyên dương.';
				}
				if (isNullOrEmpty(errMsg)) {
					if (isNullOrEmpty(obj.quantity) || obj.quantity <= 0) {
						errMsg = 'Vui lòng nhập thực đặt cho sản phẩm ' + Utils.XSSEncode(obj.productCode);
					}
				}				
				if (!isNullOrEmpty(errMsg)) {
					$('#generalInfoDiv #GeneralInforErrors').html(errMsg).show();
					tmp.selectCell(index,COLSALE.QUANTITY);
					tmp.getActiveEditor().beginEditing();
					window.scrollTo(0,0);
					return false;
				}
				
				/*
				 * validate: quantity <= availableQuantity?
				 */
				try {
					var keyMap = obj.productCode;
					if (!totalQuantityOfProducts.get(keyMap)) {
						totalQuantityOfProducts.put(keyMap, {
							quantity: 0
						});
					}
					var mapVal = totalQuantityOfProducts.get(keyMap);
					if (!mapVal.availableQuantity) {
						var pro = GridSaleBusiness.mapSaleProduct.get(obj.productCode);
						if (pro != null && pro != undefined) {
							var productId = pro.productId;
							mapVal.availableQuantity = GridSaleBusiness.getAvailableQuantityWarehouseOnGridByType(productId);
						} else {
							$('#generalInfoDiv #GeneralInforErrors').html("Sản phẩm " + Utils.XSSEncode(obj.productCode) + " không có tồn kho. Vui lòng nhập kho cho sản phẩm.").show();
							return false;
						}
					}
					mapVal.quantity += Number(obj.quantity);
					if (mapVal.quantity > mapVal.availableQuantity) {
						var errMsg = 'Thực đặt sản phẩm ' +Utils.XSSEncode(obj.productCode)+ ' vượt quá tồn kho . Vui lòng nhập lại.';
						$('#generalInfoDiv #GeneralInforErrors').html(errMsg).show();
						tmp.selectCell(index,COLSALE.QUANTITY);
						window.scrollTo(0,0);
						tmp.getActiveEditor().beginEditing();
						return false;
					}
				} catch (e) {
					// ops! error
					var errMsg = 'Có lỗi xảy ra trong quá trình xử lý. Vui lòng tải lại trang (F5) để thực hiện lại thao tác.';
					$('#generalInfoDiv #GeneralInforErrors').html(errMsg).show();
					return false;
				}
				var tmpKey = Utils.XSSEncode(obj.productCode + '_' + obj.commercialSupport);
				if (mapProductQuantity.get(tmpKey)==null || mapProductQuantity.get(tmpKey) == undefined ) {
					warehouseStr = "";
					productWarehouseQuantStr = "";
					var product = SPCreateOrder.findProductCodeInMap(obj.productCode);
					if (product != null && product.promotionProgramCode != null) {
						if (isNullOrEmpty(obj.commercialSupport)) {
							obj.commercialSupport = Utils.XSSEncode(product.promotionProgramCode);
							obj.commercialSupportType = 'ZV';
						}
					}
					var whMap = new Map();
					for (var j=0;j<row.length;j++) {
						if (obj.productCode == row[j][COLSALE.PRO_CODE] && obj.commercialSupport == row[j][COLSALE.PROGRAM]) {
							if (!whMap.get(GridSaleBusiness._mapWarehouseIdSelect.get(j))){
								whMap.put(GridSaleBusiness._mapWarehouseIdSelect.get(j), 0);
							}
							var quantity = whMap.get(GridSaleBusiness._mapWarehouseIdSelect.get(j)) + getQuantity(row[j][COLSALE.QUANTITY],product.convfact);
							whMap.put(GridSaleBusiness._mapWarehouseIdSelect.get(j), quantity);
							quantityTotal += getQuantity(row[j][COLSALE.QUANTITY],product.convfact);
							if (row[j][COLSALE.QUANTITY]) {
								var quaArr = row[j][COLSALE.QUANTITY].split('/');
								if (quaArr.length > 1) {
									quantityPackageTotal += Number(quaArr[0]);
									quantityRetailTotal += Number(quaArr[1]);
								} else {
									quantityRetailTotal += Number(quaArr[0]);
								}
							}
						};
					}
					warehouseStr = whMap.keyArray.reduce(function(prevVal, currVal){
									return prevVal + ',' + currVal;
								});
					productWarehouseQuantStr = whMap.valArray.reduce(function(prevVal, currVal){
												return prevVal + ',' + currVal;
											});
					if (!isNullOrEmpty(warehouseStr)) {
						mapProductQuantity.put(tmpKey,quantityTotal);
						mapProductQuantityPackage.put(tmpKey,quantityPackageTotal);
						mapProductQuantityRetail.put(tmpKey,quantityRetailTotal);
						mapProductWarehouseQuantity.put(tmpKey,productWarehouseQuantStr);
						mapProductWarehouse.put(tmpKey,warehouseStr);
					}
				};
			}
		}
		if(msg.length>0){
			$('#generalInfoDiv #GeneralInforErrors').html(msg).show();
		}
		setTimeout(function() {
			$('#generalInfoDiv #GeneralInforErrors').hide();
		}, 3000);
		if (msg.length > 0) {
			return false;
		}
		var shopCode = $('#shopCode').val().trim();
		var customerCode=$('#customerCode').val().trim();
		if(customerCode.length==0){
			$('#generalInfoDiv #GeneralInforErrors').html('Vui lòng chọn mã khách hàng').show();
		}else{
			var amount_total = 0;
			var total = 0;
			var lstCommercialSupport = new Array(),
			    lstCommercialSupportType = new Array(),
			    lstProduct = new Array(),
			    lstQuantity = new Array(),
				lstQuantityPackage = new Array(),
				lstQuantityRetail = new Array(),
			    //Cac list luu danh sach sale_order_lot
				lstWarehouseSaleProduct = new Array(),//list SP an theo Kho
				lstWarehouseId = new Array(),//list Kho cua tung ma SP
				lstWarehouseQuantity = new Array();//list So luong thuc dat theo Kho;
			var processedRows = new Array();
			for(var i=0;i<row.length;++i){
				var obj = SPCreateOrder.getObjectSaleProductInGrid(row[i], i);
				var tmpKey = Utils.XSSEncode(obj.productCode + '_' + obj.commercialSupport);
				if(!isNullOrEmpty(obj.productCode)) {
					if ($.inArray(tmpKey, processedRows) < 0) {
						processedRows.push(tmpKey);
						lstProduct.push(obj.productCode);
						var quantity = mapProductQuantity.get(tmpKey);
						var quantityPackage = mapProductQuantityPackage.get(tmpKey);
						var quantityRetail = mapProductQuantityRetail.get(tmpKey);
						lstQuantity.push(quantity);
						lstQuantityPackage.push(quantityPackage);
						lstQuantityRetail.push(quantityRetail);
						//Luu cac thong tin tren don hang theo Kho de luu vao sale_order_lot
						//Khong can check trung ma SP
						var warehouse = mapProductWarehouse.get(tmpKey);
						var warehouseQuantity = mapProductWarehouseQuantity.get(tmpKey);
						if (!isNullOrEmpty(warehouse)) {
							lstWarehouseId.push(warehouse);
							lstWarehouseQuantity.push(warehouseQuantity);
						} else {
							lstWarehouseId.push('');
							lstWarehouseQuantity.push('');
						}
						if (obj.commercialSupportType == undefined || obj.commercialSupportType == null) {
							lstCommercialSupport.push('');
							lstCommercialSupportType.push('');
						} else {
							if(obj.commercialSupportType.indexOf('ZV') == -1 && obj.commercialSupportType != '') {
								lstCommercialSupport.push(obj.commercialSupport);
								lstCommercialSupportType.push(obj.commercialSupportType);
							} else if(obj.commercialSupport.length > 0 && obj.commercialSupportType.length == 0) {
								lstCommercialSupport.push(obj.commercialSupport);
								lstCommercialSupportType.push('');
							} else {
								lstCommercialSupport.push('');
								lstCommercialSupportType.push('');
							}
						}
					}
				} 
			}
			if(lstProduct.length==0){
				$('#generalInfoDiv #GeneralInforErrors').html('Không tồn tại sản phẩm nào trong đơn hàng').show();
				return false;
			}
			if(lstQuantity.length==0){
				$('#generalInfoDiv #GeneralInforErrors').html('Vui lòng nhập đúng số lượng sản phẩm').show();
				return false;
			}			
			var data = new Object();
			data.orderDate = $('#orderDate').val().trim();
			data.lstProduct = lstProduct;
			data.lstCommercialSupport = lstCommercialSupport;
			data.lstCommercialSupportType = lstCommercialSupportType;
			data.lstQuantity = lstQuantity;
			data.lstQuantityPackage = lstQuantityPackage;
			data.lstQuantityRetail = lstQuantityRetail;
			data.customerCode = customerCode;
			data.shopCode = shopCode;
			//trungtm6 lay shopCode tren giao dien
			if ($('#shopCodeCB').length > 0){
				data.shopCode = $('#shopCodeCB').combobox('getValue');
			} 

			data.salerCode = $('#staffSaleCode').combobox('getValue').trim();
			//Luu danh sach sale_order_lot
			data.lstWarehouseId = lstWarehouseId;
			data.lstWarehouseSaleProduct = lstWarehouseSaleProduct;
			data.lstWarehouseQuantity = lstWarehouseQuantity;     
			
			if(!isNullOrEmpty($('#orderId').val())){ 
				data.orderId=$('#orderId').val();
			}
			if(typeSortKey!=null && typeSortKey!=undefined){
				data.typeSortKey = typeSortKey;
			}
			if(key!=null & key!=undefined){
				if(Number(key)<0){	
					SPCreateOrder.showChangePromotionProgram();
					return false;
				}else{
					data.key = key;
				}				
			} else {
				// reset previous discount amount
				var resetValue = null;
				for (var i = 0, htSale = $('#gridSaleData').handsontable('getInstance')
					; i < row.length
					; htSale.setDataAtCell(i++, COLSALE.DISCOUNT, resetValue));

			}
			$('#divOverlay').show();			
			$('#generalInfoDiv #GeneralInforErrors').hide();
			if (SPAdjustmentOrder._orderId) {
				data.orderId = SPAdjustmentOrder._orderId;
			}
			$.ajax({
				type : "POST",
				url : '/sale-product/create-order/payment',
				data :($.param(data, true)),
				dataType : "json",
				success : function(result) {
					SPAdjustmentOrder._firstLoadOrderForEdit = false;
					$('#divOverlay').hide();				
					if(result.error && result.errMsg!= undefined){
						$('#generalInfoDiv #GeneralInforErrors').html('Lỗi: ' + result.errMsg).show();
					} else {
						var row = $('#gridSaleData').handsontable('getInstance').getData();
						for (var index=0;index<row.length;index++) {
							var product = SPCreateOrder.findProductCodeInMap(row[index][COLSALE.PRO_CODE]);
							if (product != null && product.promotionProgramCode != null) {
								if (isNullOrEmpty(row[index][COLSALE.PROGRAM])) {
									row[index][COLSALE.PROGRAM] = product.promotionProgramCode;
								}
							}
						}
						SPAdjustmentOrder._changedStructurePromotionProgramsInSaleOrder = null;
						SPCreateOrder._lstDiscount = new Map();
						SPCreateOrder._lstDisper = new Map();						
						if (key!=null & key!=undefined) {
							/** KM ZV01 - ZV18 */
							var changePPromotion = result.changePPromotion;//Luu tat ca CTKM theo muc de ap dung chinh sua so suat

							var isEdited = changePPromotion[0].isEdited;

							var gridName = "";
							var listPromoTmp = new Map();
							if (result.isZV24) {
								gridName = "promotionGrid2";
								listPromoTmp = SPCreateOrder._lstPromotionOpenProduct;
							} else if (result.isZV21) {
								gridName = "gridSaleOrderPromotion";
								listPromoTmp = SPCreateOrder._lstPromotionProductForOrderIsAuto;
							} else {
								gridName = "gridPromotionProduct";
								listPromoTmp = SPCreateOrder._lstPromotionProductIsAuto;
							}
							var selectingRow1 = $('#'+gridName).datagrid('getRows')[SPCreateOrder._selectingIndex];
							for (var i = 0; i < SPCreateOrder._saleOrderQuantityReceivedList.length; i++) {
								var tmp = SPCreateOrder._saleOrderQuantityReceivedList[i];
								if (tmp.quantityReceived == undefined || tmp.quantityReceived == null) {
									tmp.quantityReceived = '';
								}
								if (tmp.maxQuantityReceived == undefined || tmp.maxQuantityReceived == null) {
									tmp.quantityReceived = '';
								}
								var listProduct = new Array();
								changePPromotion.forEach(function(item, tempIndex) {
									if (item.saleOrderDetail.programCode == tmp.promotionCode
										&& item.productGroupId == tmp.productGroupId
										&& item.saleOrderDetail.levelPromo == tmp.promotionLevel) {
										item.quantityReceived = tmp.quantityReceived;
										item.maxQuantityReceived = tmp.maxQuantityReceived;
										item.maxQuantityReceivedTotal = tmp.maxQuantityReceivedTotal;
										if (item.promotionType == PromotionType.PROMOTION_FOR_ZV21) {
											item.listPromotionForPromo21.forEach(function(item21,index) {
												item21.quantityReceived = tmp.quantityReceived;
												item21.maxQuantityReceived = tmp.maxQuantityReceived;
												item21.maxQuantityReceivedTotal = tmp.maxQuantityReceivedTotal;
												listProduct.push(item21);
											});
										} else {
											listProduct.push(item);
										}
									}
								});
								if (listProduct.length > 0) {
									if (SPCreateOrder._promotionPortionReceivedList.get(tmp.promotionCode+"_"+tmp.productGroupId+"_"+tmp.promotionLevel)) {
										SPCreateOrder._promotionPortionReceivedList.remove(tmp.promotionCode+"_"+tmp.productGroupId+"_"+tmp.promotionLevel);
										SPCreateOrder._promotionPortionReceivedList.put(tmp.promotionCode+"_"+tmp.productGroupId+"_"+tmp.promotionLevel,listProduct);
									} else {
										SPCreateOrder._promotionPortionReceivedList.put(tmp.promotionCode+"_"+tmp.productGroupId+"_"+tmp.promotionLevel,listProduct);
									}
								}
							}
							/*
							 * re-create SPCreateOrder._lstPromotionProductIsAuto according to grid data
							 */
							SPCreateOrder._lstPromotionProductIsAuto.clear();
							var tempMap = new Map();
							var $gridPromotionProduct = $('#gridPromotionProduct');
							$gridPromotionProduct.datagrid('getRows').forEach(function(item, index){
						        SPCreateOrder._lstPromotionProductIsAuto.put(index, item);
						        tempMap.put(item.productCode, '');
							});
							tempMap = null;
							
							if(result.canChaneMultiProduct) {
								SPCreateOrder.promotionMultiProductDialog(function(lstObj) {
									if(mapId != null && mapId != undefined && lstObj != null && lstObj != undefined && lstObj.length > 0) {
										var lstRemoveKey = new Array();
										var rowIndex = null;
										if(isNaN(mapId)) {
											var str1 = mapId.split('+');
											var str2 = str1[str1.length - 1];//index row
											rowIndex = str2;
											var str2Row = listPromoTmp.valArray[str2];
											if(str2Row.myTime == 0 || str2Row.myTime == null || str2Row.myTime == undefined || str2Row.myTime == 'null') {
												var myTime = (new Date()).getTime();
												listPromoTmp.keyArray[str2] = listPromoTmp.keyArray[str2] + '_' + myTime;
												lstRemoveKey.push(listPromoTmp.keyArray[str2]);
											}
											var str3 = mapId.substring(0, mapId.length - str2.length - 1);//ma ctkm
											var mapLength = listPromoTmp.keyArray.length;
											var staticMyTime = listPromoTmp.valArray[Number(str2)].myTime;
											if(staticMyTime == 0 || staticMyTime == 'null') {
												staticMyTime == null;
											}
											for(var i = 0; i < mapLength; i++) {
												if(staticMyTime != null && staticMyTime != undefined 
														&& listPromoTmp.valArray[i].myTime != null && listPromoTmp.valArray[i].myTime != undefined
														&& listPromoTmp.valArray[i].myTime != '' && !isNaN(listPromoTmp.valArray[i].myTime)
														&& listPromoTmp.valArray[i].myTime == staticMyTime) {
													listPromoTmp.keyArray[i] = listPromoTmp.keyArray[i] + "_" + listPromoTmp.valArray[i].myTime;
													lstRemoveKey.push(listPromoTmp.keyArray[i]);
												}
											}
										} else {/*in this case, mapId is index row*/
											var obj = listPromoTmp.get(mapId);
											if (obj == null || obj == undefined) {
												var staticMyTime = $('#'+gridName).datagrid('getRows')[mapId].myTime;
												if(staticMyTime == 0 || staticMyTime == 'null') {
													staticMyTime = null;
												}
												for(var i = 0; i < listPromoTmp.valArray.length; i++) {
													if(staticMyTime != null && staticMyTime != undefined
															&& listPromoTmp.valArray[i].myTime != null && listPromoTmp.valArray[i].myTime != undefined
															&& listPromoTmp.valArray[i].myTime != '' && !isNaN(listPromoTmp.valArray[i].myTime)
															&& listPromoTmp.valArray[i].myTime == staticMyTime) {
														listPromoTmp.keyArray[i] = listPromoTmp.keyArray[i] + "_" + listPromoTmp.valArray[i].myTime;
														lstRemoveKey.push(listPromoTmp.keyArray[i]);
													}
												}
											} else {
												lstRemoveKey.push(mapId);
											}
										}
										var strMap = mapId;
										if (isNaN(mapId)) {
											mapId = (mapId.split('+')[1]);
											if (isNaN(mapId)) {
												mapId = strMap;
												mapId = mapId.split('+')[2];
											}
										}
										if (lstRemoveKey.length > 0) {
											/*
											 * remove promotion product on promotion-grid
											 */
											var tmpObj = listPromoTmp.get(lstRemoveKey[0]);
											var programCode = Utils.XSSEncode(tmpObj.programCode);
											var levelPromo = tmpObj.levelPromo;
											var changeProduct = tmpObj.changeProduct;
											var rows = $('#gridPromotionProduct').datagrid('getRows');
											for (var i = rows.length - 1; i >= 0; i--) {
												if (rows[i].programCode == programCode
													&& rows[i].levelPromo == levelPromo
													&& rows[i].productGroupId == tmpObj.productGroupId
													&& rows[i].changeProduct == changeProduct) {
													$('#gridPromotionProduct').datagrid('deleteRow', i);
												}
											}

											for(var i = 0; i < lstRemoveKey.length; i++) {
												var tmpObj = listPromoTmp.get(lstRemoveKey[i]);
												if (tmpObj != undefined && tmpObj != null) {
													var programCode = tmpObj.programCode;
													var levelPromo = tmpObj.levelPromo;
													var changeProduct = tmpObj.changeProduct;
													for (var j = listPromoTmp.keyArray.length - 1; j >= 0; j--) {
														if (listPromoTmp.valArray[j].productCode.trim().indexOf(" ") < 0
															&& listPromoTmp.valArray[j].programCode === programCode
															&& listPromoTmp.valArray[j].productGroupId === tmpObj.productGroupId
															&& listPromoTmp.valArray[j].levelPromo === levelPromo
															&& listPromoTmp.valArray[j].changeProduct === changeProduct) {
															listPromoTmp.remove(listPromoTmp.keyArray[j]);
														}
													}
												}
											}
										}
										for(var i = 0; i < lstObj.length; i++) {
											var chooseObj = lstObj[i];
											chooseObj.rownum = (Number(mapId)+1);
											if (lstObj.length > 1) {
												var n = 0;
												for (var j = 0;j<listPromoTmp.keyArray.length;j++) {
													var tmp = listPromoTmp.keyArray[i];
													if (tmp == chooseObj.programCode+"_"+chooseObj.productId) {
														n+1;
													}
												}
												if (n > 0) {
													listPromoTmp.keyArray.splice(Number(mapId)+1,0,chooseObj.programCode+"_"+chooseObj.productId+n);
													listPromoTmp.valArray.splice(Number(mapId)+1,0,chooseObj);
												} else {
													listPromoTmp.keyArray.splice(Number(mapId)+1,0,chooseObj.programCode+"_"+chooseObj.productId);
													listPromoTmp.valArray.splice(Number(mapId)+1,0,chooseObj);
												}
											} else {
												listPromoTmp.keyArray.splice(Number(mapId),0,chooseObj.programCode+"_"+chooseObj.productId);
												listPromoTmp.valArray.splice(Number(mapId),0,chooseObj);
											}
											++mapId;
										}
										
										// sort data
										listPromoTmp.valArray.sort(function(o1, o2){
											var id = '-1413001269113';
											if (o1.programCode != o2.programCode) {
												return o1.programCode > o2.programCode;
											}
											if (o1.productCode.trim().indexOf(" ") > 0) {
												return false;
											}
											if (o2.productCode.trim().indexOf(" ") > 0) {
												return true;
											}
											if (Number(o1.levelPromo) != Number(o2.levelPromo)) {
												return o1.levelPromo > o2.levelPromo;
											}
											if (!isNaN(o1.productGroupId) && !isNaN(o2.productGroupId)
												&& Number(o1.productGroupId) != Number(o2.productGroupId)) {
												return o1.productGroupId < o2.productGroupId;
											}
											if (Number(o1.changeProduct) != Number(o2.changeProduct)) {
												return Number(o1.changeProduct) > Number(o2.changeProduct);
											}
											if (o1.productCode != o2.productCode) {
												if (o1.productCode.trim().indexOf(" ") > 0) {
													return false;
												}
												if (o2.productCode.trim().indexOf(" ") > 0) {
													return true;
												}
												return o1.productCode > o2.productCode;
											}
											return o1.warehouse.seq > o2.warehouse.seq;
										});

										/*
										 * reset row num
										 */
										listPromoTmp.valArray.forEach(function(item, index) {
											listPromoTmp.keyArray[index] = index;
											item.rownum = index + 1;
										});

										if (result.isZV24) {
											SPCreateOrder._lstPromotionOpenProduct = listPromoTmp;
											OpenProductPromotion.loadOpenProductGrid(SPCreateOrder._lstPromotionOpenProduct);
										} else if (result.isZV21) {
											SPCreateOrder._lstPromotionProductForOrderIsAuto = listPromoTmp;
											SPCreateOrder.fillTablePromotionProductForOrder(SPCreateOrder._lstPromotionProductForOrderIsAuto);
										} else {
											SPCreateOrder._lstPromotionProductIsAuto = listPromoTmp;
											SPCreateOrder.fillTablePromotionProduct(SPCreateOrder._lstPromotionProductIsAuto);
											OpenProductPromotion.refreshOpenProductGrid(); // load lai so thu tu cua grid KM mo moi
										}
										$('#searchStyle5').dialog('close');
										enable('backBtn');
										SPCreateOrder.orderChanged = true;
										SPCreateOrder.updatePromotionGrids();
									}
								}, changePPromotion, mapId, result.isZV21,result.errCodeForProductNotStock, result.isZV24);
							} else {
								SPCreateOrder.promotionProductDialog(function(chooseObj){
									if(mapId!=null & mapId!=undefined){
										if (!isNaN(mapId)) {
											/*
											 * delete previous selected product
											 */
											var rootSelectRow = $('#'+gridName).datagrid('getRows')[SPCreateOrder._selectingIndex];
											if (rootSelectRow) {
												for (var k = listPromoTmp.keyArray.length - 1; k >=0; k--) {
													var row = listPromoTmp.valArray[k];
													if (row && row.productCode.trim().indexOf(" ") < 0
														&& row.programCode.toString() == rootSelectRow.programCode.toString()
														&& row.levelPromo == rootSelectRow.levelPromo && row.changeProduct == 1) {
														if ((result.isZV21 && row.groupId == rootSelectRow.groupId)
															|| (!result.isZV21 && row.productGroupId == rootSelectRow.productGroupId)) { // lacnv1 - cung nhom
															listPromoTmp.remove(listPromoTmp.keyArray[k]);
														}
													}
												}
											}
											listPromoTmp.keyArray.splice(Number(mapId),0,mapId);
											listPromoTmp.valArray.splice(Number(mapId),0,chooseObj);
											/*
											 * sort data
											 */
											listPromoTmp.valArray.sort(function(o1, o2){
												var id = '-1413001269113';
												/*if (result.isZV21) {
													if (o1.groupId != o2.groupId) {
														return o1.groupId > o2.groupId;
													}
												}*/
												if (o1.programCode != o2.programCode) {
													return o1.programCode > o2.programCode;
												}
												if (o1.productCode.trim().indexOf(" ") > 0) {
													return false;
												}
												if (o2.productCode.trim().indexOf(" ") > 0) {
													return true;
												}
												if (Number(o1.levelPromo) != Number(o2.levelPromo)) {
													return o1.levelPromo > o2.levelPromo;
												}
												if (!isNaN(o1.productGroupId) && !isNaN(o2.productGroupId)
													&& Number(o1.productGroupId) != Number(o2.productGroupId)) {
													return o1.productGroupId < o2.productGroupId;
												}
												if (Number(o1.changeProduct) != Number(o2.changeProduct)) {
													return Number(o1.changeProduct) > Number(o2.changeProduct);
												}
												if (o1.productCode != o2.productCode) {
													return o1.productCode > o2.productCode;
												}
												return o1.warehouse.seq > o2.warehouse.seq;
											});
											
											/*
											 * reset row num
											 */
											for (var k = 0; k < listPromoTmp.keyArray.length; k++) {
												listPromoTmp.keyArray[k] = k;
												if (!result.isZV21) {
													listPromoTmp.valArray[k].rownum = k + 1;
												}
											}
											if (result.isZV24) {
												SPCreateOrder._lstPromotionOpenProduct = listPromoTmp;
												OpenProductPromotion.loadOpenProductGrid(SPCreateOrder._lstPromotionOpenProduct);
											} else if (result.isZV21) {
												SPCreateOrder._lstPromotionProductForOrderIsAuto = listPromoTmp;
												SPCreateOrder.fillTablePromotionProductForOrder(SPCreateOrder._lstPromotionProductForOrderIsAuto);
											} else {
												SPCreateOrder._lstPromotionProductIsAuto = listPromoTmp;
												SPCreateOrder.fillTablePromotionProduct(SPCreateOrder._lstPromotionProductIsAuto);
												OpenProductPromotion.refreshOpenProductGrid(); // load lai so thu tu cua grid KM mo moi
											}
											SPCreateOrder.orderChanged = true;
											SPCreateOrder.updatePromotionGrids();
										}
									}
								},changePPromotion, result.isZV21,result.errCodeForProductNotStock, result.isZV24, isEdited);
							}
							disabled('payment');
							enable('btnOrderSave');
							enable('backBtn');
							if ($("#btnApproveOrderIN").length > 0 && $("#btnApproveOrderIN").attr("onclick")) {
								enable("btnApproveOrderIN");
							}
						} else {
							SPAdjustmentOrder._changedStructurePromotionProgramsInSaleOrder = null;
							SPCreateOrder._lstPromotionProductForOrder = new Map();
							SPCreateOrder._lstPromotionProductIsAuto = new Map();
							SPCreateOrder._lstPromotionOpenProduct = new Map();
							SPCreateOrder._lstPromotionProductForOrderIsAuto = new Map();
							
							var htSale = $('#gridSaleData').handsontable('getInstance');
							var row = htSale.getData();
							var listBuyProduct = result.listBuyProduct;
							var lstPromoDetailOrder = result.lstPromoDetailOrder;
							var listLot = new Array();
							//Tach kho cho list sp ban
							SPCreateOrder._mapPromoDetail = new Map();
							for (var k =0;k < listBuyProduct.length;++k) {
								var tmp = listBuyProduct[k];
								if (tmp.listPromoDetail != undefined && tmp.listPromoDetail.length > 0) {
									SPCreateOrder._mapPromoDetail.put(tmp.product.id, tmp.listPromoDetail);
								}
								for (var m = 0;m <tmp.saleOrderLot.length;++m) {
									tmp.saleOrderLot[m].type = tmp.type;
									tmp.saleOrderLot[m].promotionType = tmp.promotionType;
									listLot.push(tmp.saleOrderLot[m]);
								};
							}
							//Set lai gia cho grid sp ban
							for (var index = 0;index<listLot.length;++index) {
								var obj = listLot[index];
								for (var idx=0;idx<row.length;idx++) {
									var updateRowIndex = new Array();
									var updatedRow = row.filter(function(item, idx){
										if (listLot[index].saleOrderDetail.product.productCode == item[COLSALE.PRO_CODE]){
											updateRowIndex.push(idx);
											return true;
										}
										return false;
									});
									updatedRow.forEach(function(rowObj, index){
									});
									break;
								};
							}

							// set lai CTKM tu dong neu no vua duoc bat len hoac off di
							var r = null;
							var pCode = null;
							var b = false;
							if (result.lstWarningPromotion instanceof Array && result.lstWarningPromotion.length > 0) {
								var msg1 = "Đã loại bỏ (những) CTKM ";
								var pCode1 = null;
								for (var i = 0, len1 = result.lstWarningPromotion.length; i < len1; i++) {
									pCode1 = result.lstWarningPromotion[i].promotionProgramCode;
									for (var idx = 0, len2 = row.length; idx < len2; idx++) {
										r = row[idx];
										pCode = r[COLSALE.PROGRAM];
										if (pCode == pCode1) {
											var product = GridSaleBusiness.mapSaleProduct.get(r[0]);
											product.promotionProgramCode = "";
											SPCreateOrder._syncFlag._allowAppliedNewProgram = true;
											r[COLSALE.PROGRAM] = "";
										}
									}
									if (i == 0) {
										msg1 = msg1 + Utils.XSSEncode(pCode1);
									} else {
										msg1 = msg1 + ", " + Utils.XSSEncode(pCode1);
									}
								}
								msg1 = msg1 + " do hết hạn.";
								$('#generalInfoDiv #GeneralInforErrors').html(msg1).show();
							}
							for (var idx = 0, len1 = row.length; idx < len1; idx++) {
								r = row[idx];
								pCode = r[COLSALE.PROGRAM];
								b = false;
								if (pCode) {
									var b = GridSaleBusiness.listCS.some(function(cs) {
										return cs.code == pCode;
									});
								}
								for (var index = 0, len2 = listLot.length; index < len2; index++) {
									var obj = listLot[index];
									if (!b && obj.saleOrderDetail.product.productCode == r[COLSALE.PRO_CODE]
										&& obj.warehouse.warehouseId == GridSaleBusiness._mapWarehouseIdSelect.get(idx)
										&& obj.saleOrderDetail.programCode && obj.saleOrderDetail.programCode != pCode) {
										var product = GridSaleBusiness.mapSaleProduct.get(obj.saleOrderDetail.product.productCode.trim());
										product.promotionProgramCode = obj.saleOrderDetail.programCode;
										SPCreateOrder._syncFlag._allowAppliedNewProgram = true;
										r[COLSALE.PROGRAM] = obj.saleOrderDetail.programCode;
										break;
									}
								}
							}
							SPCreateOrder._mapDiscountAmountOrder = new Map();
							SPCreateOrder._mapDiscountAmount = new Map();
							SPCreateOrder._mapDiscountPercent = new Map();
							SPCreateOrder._mapHasPromotion = new Map();
							//Tính chiết khấu
							for (var index = 0;index<listLot.length;++index) {
								var obj = listLot[index];
								/*
								 * filter cac dong du lieu se cap nhat tien CKTM theo (san pham, kho, CT HTTM)
								 */
								var updatedRowsIndex = new Array();
								//Lấy ra các dòng cần update CKMH
								var updatedRow = row.filter(function(item, idx){
													if (listLot[index].saleOrderDetail.product.productCode == item[COLSALE.PRO_CODE]
														&& listLot[index].warehouse.warehouseName == item[COLSALE.WAREHOUSE]
														&& !SPCreateOrder.checkCTHTTMValue(item[COLSALE.PROGRAM])){
														updatedRowsIndex.push(idx);
														return true;
													}
													return false;
												});

								// updatedRow.sort(function(obj1, obj2) {
								// 	return obj1[COLSALE.PRO_CODE] > obj2[COLSALE.PRO_CODE];
								// });
								/*
								 * tong so luong cua (san pham, kho, CT HTTM)
								 */
								var totalQuantity = updatedRow.reduce(function(prevVal, item){
														return prevVal + StockValidateInput.getQuantity(item[COLSALE.QUANTITY], listLot[index].product.convfact);
													}, 0);
								var ratio = 0
								if (obj.type != null && obj.type != undefined 
									&& obj.type == PromotionType.FREE_PERCENT && isNullOrEmpty(obj.discountPercent)) {
									ratio = obj.discountPercent;
								} 
								var ONE_HUNDRED = 100;
								var accumulativeDiscountAmount = 0,
									accumulativeDiscountPercent = 0;
								for (var indexTmp = 0;indexTmp < updatedRow.length;++indexTmp) {
									if (isNullOrEmpty(listLot[index].discountAmount)) {
										listLot[index].discountAmount = '';
										listLot[index].discountPercent = '';
									}

									/*
									 * phan chia tien CKTM theo so luong cua san pham tren kho
									 */
									var rowDiscountAmount = 0;
									var rowDiscountPercent = 0;
									var rowQuantity = StockValidateInput.getQuantity(updatedRow[indexTmp][COLSALE.QUANTITY], listLot[index].product.convfact);
									if (updatedRow.length == 1) {//Nếu chỉ có 1 dòng cần update thì set luôn ko cần tính
										rowDiscountAmount = listLot[index].discountAmount;
										rowDiscountPercent = listLot[index].discountPercent;
									} else {
										if (indexTmp == updatedRow.length - 1) {
											rowDiscountAmount = listLot[index].discountAmount - accumulativeDiscountAmount;
											rowDiscountPercent = ONE_HUNDRED - accumulativeDiscountPercent;
										} else {
											//rowDiscountAmount = Math.round((rowQuantity/totalQuantity) * listLot[index].discountAmount);
											rowDiscountAmount = Math.floor((rowQuantity/totalQuantity) * listLot[index].discountAmount);
											accumulativeDiscountAmount += rowDiscountAmount;
											rowDiscountPercent = Math.round10(rowQuantity*100/totalQuantity, -2);
											accumulativeDiscountPercent += rowDiscountPercent;
										}
									}
									updatedRow[indexTmp][COLSALE.DISCOUNT] = formatCurrency(rowDiscountAmount);
									htSale.setDataAtCell(updatedRowsIndex[indexTmp],COLSALE.DISCOUNT,formatCurrency(rowDiscountAmount));
									SPCreateOrder._mapDiscountAmount.keyArray[updatedRowsIndex[indexTmp]] = updatedRowsIndex[indexTmp];
									SPCreateOrder._mapDiscountAmount.valArray[updatedRowsIndex[indexTmp]] = rowDiscountAmount;
									SPCreateOrder._mapDiscountPercent.keyArray[updatedRowsIndex[indexTmp]] = updatedRowsIndex[indexTmp];
									SPCreateOrder._mapDiscountPercent.valArray[updatedRowsIndex[indexTmp]] = rowDiscountPercent;
								}
								SPCreateOrder._mapHasPromotion.put(listLot[index].saleOrderDetail.product.productCode, listLot[index].hasPromotion || 0);
							}
							htSale.render();
							var warehouselessProductMap = new Map();	// Map<program_code, List<product_code>>
							var pp = result.pp;
							var promotionProductQuantityInWarehouse = new Map();
							var cloneProductQuantityInWarehouse = function(productId) {
								var productsWarehouses = GridSaleBusiness.getWarehouseOnGridByType(productId, WarehouseType.PROMO) || [];
								if (productsWarehouses.length != 0 && !promotionProductQuantityInWarehouse.get(productId)) {
									var tmpProductsWarehouses = [];
									$.extend(true, tmpProductsWarehouses, productsWarehouses);
									promotionProductQuantityInWarehouse.put(productId, tmpProductsWarehouses);
								}
							};
							var autoSplitWarehouse = function(saleOrderLot) {
								var warehouseOfProducts = promotionProductQuantityInWarehouse.get(saleOrderLot.stockTotal != null ? saleOrderLot.stockTotal.product.id : saleOrderLot.saleOrderDetail.product.id);
								var quantity = saleOrderLot.quantity;
								var out = [];
								if (warehouseOfProducts) {
									for (var i = 0; i < warehouseOfProducts.length && quantity > 0; i++) {
										if (warehouseOfProducts[i].availableQuantity > 0 || SPAdjustmentOrder._orderId) {
											var distributedQuantity = Math.min(quantity, warehouseOfProducts[i].availableQuantity);
											var newSaleOrderLot = {};
											$.extend(true, newSaleOrderLot, saleOrderLot);
											newSaleOrderLot.quantity = distributedQuantity;
											var j = -1;
											var warehouses = GridSaleBusiness.getWarehouseByType(saleOrderLot.stockTotal.product.id, WarehouseType.PROMO);
											do {
												j++;
												newSaleOrderLot.warehouse = warehouses[j];
												newSaleOrderLot.stock = warehouses[j].availableQuantity;
												newSaleOrderLot.stockTotal.availableQuantity = warehouses[j].availableQuantity;
												newSaleOrderLot.stockTotal.warehouse = warehouses[j];
												newSaleOrderLot.stockTotal.id = null;
												newSaleOrderLot.stockTotal.quantity = null;
											} while (j < warehouses.length && warehouses[j].warehouseId != warehouseOfProducts[i].warehouseId);
											if (!SPAdjustmentOrder._orderId || newSaleOrderLot.stockTotal.availableQuantity != 0) {
												if (SPAdjustmentOrder._orderId) {
													distributedQuantity = Math.min(quantity, newSaleOrderLot.stockTotal.availableQuantity);
													newSaleOrderLot.quantity = distributedQuantity;
												}
												out.push(newSaleOrderLot);
												quantity -= distributedQuantity;
												warehouseOfProducts[i].availableQuantity -= distributedQuantity;
											}
										}
									}
								}
								if (out.length == 0) {
									out.push(saleOrderLot);
								} else {
									if (quantity > 0) {
										out[out.length - 1].quantity += quantity;
									}
								}
								return out;
							};
							var rowNum = 1;
							var rowNum_o = 1;
							if (pp != undefined && pp != null && pp.length > 0){
								for(var i=0;i<pp.length;++i){
									var obj = pp[i].saleOrderLot;
									if (obj != null && obj != undefined && pp[i].type == SPCreateOrder.PROMOTION_GET_TYPE.PRODUCT) {
										// cong so luong da tach kho tren server lai, de thuc hien tach kho lai tren client
										var oldQuantity = obj[0].quantity;
										for (var j = 1; j < obj.length; obj[0].quantity += obj[j].quantity, j++);
											var j = 0;
											obj[j].saleOrderDetail = pp[i].saleOrderDetail;
											obj[j].promoProduct = pp[i].promoProduct;
											obj[j].type = pp[i].type;
											obj[j].keyList = pp[i].keyList;
											obj[j].changeProduct = pp[i].changeProduct;
											obj[j].typeName = pp[i].typeName;
											obj[j].isEdited = pp[i].isEdited;
											obj[j].promotionType = pp[i].promotionType;
											obj[j].levelPromo = pp[i].levelPromo;
											obj[j].openPromo = pp[i].openPromo;
											obj[j].productGroupId = pp[i].productGroupId;
											obj[j].hasPortion = pp[i].hasPortion;

											if (pp[i].saleOrderDetail) {
												obj[j].groupLevelId = pp[i].saleOrderDetail.groupLevel.id;
											}
											/*
											 * tach kho, set lai stockTotal & warehouse
											 */
											cloneProductQuantityInWarehouse(obj[j].stockTotal != null ? obj[j].stockTotal.product.id : obj[j].saleOrderDetail.product.id);
											var autoSplitedSaleOrderLot = autoSplitWarehouse(obj[j]);
											//obj[0].quantity = oldQuantity;
											autoSplitedSaleOrderLot.forEach(function(sol) {
												if (sol.stockTotal && sol.warehouse) {
													var quantity = sol.quantity;
													var pproduct = SPCreateOrder.getObjectPromotionProduct(sol);
													//pproduct.quantity = pproduct.maxQuantity;
													pproduct.quantity = quantity;
													if (pproduct.warehouse != null && pproduct.warehouse.warehouseId == null) {
														pproduct.warehouse.warehouseId = pproduct.warehouse.id; 
													}
													if (pproduct.openPromo == 1) {
														pproduct.rownum = rowNum_o;
														SPCreateOrder._lstPromotionOpenProduct.put(rowNum_o - 1,pproduct);
														rowNum_o++;
													} else {
														pproduct.rownum = rowNum;
														SPCreateOrder._lstPromotionProductIsAuto.put(rowNum - 1, pproduct);
														rowNum++;
													}
												} else {
													if (!warehouselessProductMap.get(sol.saleOrderDetail.programCode)) {
														warehouselessProductMap.put(sol.saleOrderDetail.programCode, []);
													}
													if ($.inArray(sol.saleOrderDetail.product.productCode, warehouselessProductMap.get(sol.saleOrderDetail.programCode)) < 0) {
														warehouselessProductMap.get(sol.saleOrderDetail.programCode).push(sol.saleOrderDetail.product.productCode);
													}												
												}
											});
									};
								}
							}
							
							DEBUG ? console.log(warehouselessProductMap) : null;

							OpenProductPromotion.sortProductList(SPCreateOrder._lstPromotionOpenProduct, false);
							OpenProductPromotion.fillOpenOrderPromotionToGrid(result.zV19_20MM, result.zV21MM, htSale, row, warehouselessProductMap, promotionProductQuantityInWarehouse);

							var lstZV21 = result.zV21;
							var zV19z20 = result.zV19zV20;
							SPCreateOrder.programes = result.programes;
							var zLenPPAuto = SPCreateOrder._lstPromotionProductForOrderIsAuto.size();
							var zzrowNumForOrder = zLenPPAuto + 1;
							if(lstZV21!=undefined && lstZV21!=null && lstZV21.length > 0){
								SPCreateOrder._isUsingZVOrder = 'ZV21';
								//SPCreateOrder._isUsingZVCode = lstZV21[0].programCode;
								var tmpGroupId = 0;
								var hasHeader = false;
								for (var index=0;index<lstZV21.length;index++) {
									var zV21 = lstZV21[index];
									var pproduct = new Object();
									pproduct = SPCreateOrder.getObjectPromotionProduct(zV21);
									if (index == 0) {
										SPCreateOrder._isUsingZVCode = pproduct.programCode;
									}
									pproduct.rownum = zzrowNumForOrder;
									pproduct.groupId = ++tmpGroupId;	// gom nhom cac muc KM
									if (!hasHeader) {
										hasHeader = true;
										SPCreateOrder._lstPromotionProductForOrderIsAuto.put(zLenPPAuto,pproduct);
									}
									// zLenPPAuto = SPCreateOrder._lstPromotionProductForOrderIsAuto.size();
									var len = SPCreateOrder._lstPromotionProductForOrderIsAuto.size();
									for(var i=0;i<zV21.listPromotionForPromo21.length;++i){
										var pproduct = new Object();
										var obj = zV21.listPromotionForPromo21[i].saleOrderLot;
										if (obj != null && obj != undefined) {
											// cong so luong da tach kho tren server lai, de thuc hien tach kho lai tren client
											var oldQuantity = obj[0].quantity;
											for (var j = 1; j < obj.length; obj[0].quantity += obj[j].quantity, j++);
											

											// tach kho lai tu sale_order_lot dau tien
											//for (var j=0;j<obj.length;++j) {
												var j = 0;
												obj[j].saleOrderDetail = zV21.listPromotionForPromo21[i].saleOrderDetail;
												obj[j].promoProduct = zV21.listPromotionForPromo21[i].promoProduct;
												obj[j].type = zV21.listPromotionForPromo21[i].type;
												obj[j].keyList = zV21.listPromotionForPromo21[i].keyList;
												obj[j].changeProduct = zV21.listPromotionForPromo21[i].changeProduct;						
												obj[j].typeName = zV21.listPromotionForPromo21[i].typeName;						
												obj[j].isEdited = zV21.listPromotionForPromo21[i].isEdited;
												obj[j].promotionType = zV21.listPromotionForPromo21[i].promotionType;
												obj[j].levelPromo = zV21.listPromotionForPromo21[i].levelPromo;
												obj[j].productGroupId = zV21.listPromotionForPromo21[i].productGroupId;
												/*
												 * tach kho, set lai stockTotal & warehouse
												 */
												cloneProductQuantityInWarehouse(obj[j].stockTotal != null ? obj[j].stockTotal.product.id : obj[j].saleOrderDetail.product.id);
												var autoSplitedSaleOrderLot = autoSplitWarehouse(obj[j]);
												//obj[0].quantity = oldQuantity;
												autoSplitedSaleOrderLot.forEach(function(sol) {
													if (sol.stockTotal && sol.warehouse) {
														var quantity = sol.quantity;
														var pproduct = SPCreateOrder.getObjectPromotionProduct(sol);
														pproduct.quantity = quantity;
														//pproduct.maxQuantity = pproduct.quantity;	
														pproduct.rownum = zzrowNumForOrder;
														pproduct.groupId = tmpGroupId;
														pproduct.hasPortion = zV21.hasPortion;
														SPCreateOrder._lstPromotionProductForOrderIsAuto.put((len),pproduct);
														++len;
													} else {
														if (!warehouselessProductMap.get(sol.saleOrderDetail.programCode)) {
															warehouselessProductMap.put(sol.saleOrderDetail.programCode, []);
														}
														if ($.inArray(sol.saleOrderDetail.product.productCode, warehouselessProductMap.get(sol.saleOrderDetail.programCode)) < 0) {
															warehouselessProductMap.get(sol.saleOrderDetail.programCode).push(sol.saleOrderDetail.product.productCode);
														}
													}
												});

												
											//}
										}
									};
									zLenPPAuto = SPCreateOrder._lstPromotionProductForOrderIsAuto.size();
								}
							}else if(zV19z20!=undefined && zV19z20!=null && zV19z20.length > 0){
								/*
								 * phan bo tien CKTM don hang
								 */
								// tinh tong tien chiet khau
								var saleOrderDiscountAmount = zV19z20.reduce(function(prevTotalDiscountAmount, promotionProgram) {
																var totalDiscountAmount = promotionProgram.listPromoDetail.reduce(function(prevDiscountAmount, promoDetail) {
																							return prevDiscountAmount + Number(promoDetail.discountAmount);
																						}, 0);
																return prevTotalDiscountAmount + totalDiscountAmount;
															}, 0);

								/*
								 * lay cac dong du lieu tren grid de update CKTM: cac dong san pham khong co CT HTTM hoac co CT HTTM dang KM tu dong
								 */
								var rowObject = row.map(function(r, index){
									return {
										row: r,
										rowIndex: index,
									}
								});

								var updatedRow = rowObject.filter(function(item, idx){
													if (item.row[COLSALE.PRO_CODE]) {
														var isManualPromotionProgram = GridSaleBusiness.listCS.some(function(cs) {
															return cs.code == item.row[COLSALE.PROGRAM];
														});
														if ((item.row[COLSALE.PROGRAM] && !isManualPromotionProgram) || !item.row[COLSALE.PROGRAM]) {
															return true;
														}
														return false;
													}
													return false;
												});

								// updatedRow.sort(function(obj1, obj2) {
								// 	return obj1.row[COLSALE.PRO_CODE] > obj2.row[COLSALE.PRO_CODE];
								// });

								var tmpProductWarehouseMap = new Map();
								updatedRow.forEach(function(rowObj, index){
									var keyMap = rowObj.row[COLSALE.PRO_CODE] + '_';
									if (!tmpProductWarehouseMap.get(keyMap)) {
										tmpProductWarehouseMap.put(keyMap, {
											rows: [],
											totalAmount: 0
										});
									}
									var mapValue = tmpProductWarehouseMap.get(keyMap);
									mapValue.totalAmount += Number(rowObj.row[COLSALE.AMOUNT].toString().replace(/,/g, ""));
									mapValue.rows.push(rowObj);
								});

								/*
								 * tinh tong tien mua hang
								 */
								var totalBuyAmount = updatedRow.reduce(function(prevAmount, rowObj) {
														return prevAmount + Number(rowObj.row[COLSALE.AMOUNT].toString().replace(/,/g, ""));
													}, 0);

								for (var indexZV20=0;indexZV20<zV19z20.length;indexZV20++) {
									var zv = zV19z20[indexZV20];
									var pproduct = new Object();
									pproduct = SPCreateOrder.getObjectPromotionProduct(zv);
									pproduct.rownum = zzrowNumForOrder;
									pproduct.levelPromo = zv.saleOrderDetail.levelPromo;
									SPCreateOrder._lstPromotionProductForOrderIsAuto.put(zLenPPAuto,pproduct);
									++zLenPPAuto;
								}
								var ratio = 0;
								var obj = SPCreateOrder._lstPromotionProductForOrderIsAuto.get(0);
								if (obj.promotionType == PromotionType.PROMOTION_FOR_ORDER && obj.type == PromotionType.FREE_PERCENT) {
									SPCreateOrder._isUsingZVOrder = 'ZV19';
									SPCreateOrder._isUsingZVCode = obj.programCode;
									ratio = obj.discountPercent;
								} else {
									SPCreateOrder._isUsingZVOrder = 'ZV20';
									SPCreateOrder._isUsingZVCode = obj.programCode;
									//ratio = Math.round10(saleOrderDiscountAmount*100/totalBuyAmount,-2);
								}

								var accumulativeDistributedDiscountAmount = 0;
								var isExceededSaleOrderAmount = false;
								tmpProductWarehouseMap.keyArray.forEach(function(keyMap, tempIndex) {
									var product = SPCreateOrder.findProductCodeInMap(keyMap.split('_')[0]);
									if (product != null) {
										var productWarehouse = tmpProductWarehouseMap.get(keyMap);
										var totalProductAmount = productWarehouse.totalAmount;
										var productDiscountPercent = 0;
										var productDiscountAmount = 0;
										if (SPCreateOrder._isUsingZVOrder == 'ZV19') {
											productDiscountPercent = ratio;
											productDiscountAmount = Math.round((productDiscountPercent * totalProductAmount)/100);
										} else if (SPCreateOrder._isUsingZVOrder == 'ZV20') {
											productDiscountPercent = Math.round(saleOrderDiscountAmount*10000/totalBuyAmount)/100;
											// productDiscountPercent = Math.round10(totalProductAmount / totalBuyAmount, -2);
											//productDiscountAmount = Math.round((productDiscountPercent * totalProductAmount)/100);
											productDiscountAmount = Math.floor((productDiscountPercent * totalProductAmount)/100);
											if (productDiscountAmount >= saleOrderDiscountAmount) {
												mapProductDiscountAmount = saleOrderDiscountAmount;
												isExceededSaleOrderAmount = true;
											}
										}

										if (tempIndex == tmpProductWarehouseMap.keyArray.length - 1
											|| productDiscountAmount > saleOrderDiscountAmount - accumulativeDistributedDiscountAmount) {
											productDiscountAmount = saleOrderDiscountAmount - accumulativeDistributedDiscountAmount;
										}
										var tmpAccumulativeDiscountAmount = 0;
										for (var k = 0; k < productWarehouse.rows.length; k++) {
											var rowObj = productWarehouse.rows[k];
											var rowDiscountAmount = 0;
											if (k == productWarehouse.rows.length - 1) {
												rowDiscountAmount = Math.round(productDiscountAmount - tmpAccumulativeDiscountAmount);
											} else {
												var rowDiscountPercent = Math.round10(Number(rowObj.row[COLSALE.AMOUNT].toString().replace(/,/g, ""))/totalProductAmount, -2);
												//rowDiscountAmount = Math.round(rowDiscountPercent * productDiscountAmount);
												rowDiscountAmount = Math.floor(rowDiscountPercent * productDiscountAmount);
											}
											// update to grid
											var value = rowObj.row[COLSALE.DISCOUNT]; 
											if (value == null) value = "";
											var oldDiscountAmount = Number(value.toString().replace(/,/g, ""));
											htSale.setDataAtCell(rowObj.rowIndex ,COLSALE.DISCOUNT,formatCurrency(rowDiscountAmount + oldDiscountAmount));
											tmpAccumulativeDiscountAmount += rowDiscountAmount;

											SPCreateOrder._mapDiscountAmountOrder.keyArray[rowObj.rowIndex] = rowObj.rowIndex;
											SPCreateOrder._mapDiscountAmountOrder.valArray[rowObj.rowIndex] = rowDiscountAmount;
										}
										accumulativeDistributedDiscountAmount += productDiscountAmount;
									}
								});
							}
							
							SPCreateOrder._lstPromotionProductForOrder.put('zV21',result.zV21C);
							SPCreateOrder._lstPromotionProductForOrder.put('zV19z20',result.zV19zV20C);	
							SPCreateOrder._saleOrderQuantityReceivedList = result.saleOrderQuantityReceivedList;
							//Luu so suat cua KM don hang vao map de luu du lieu
							SPCreateOrder._promotionOrderReceivedList = new Map();
							//Luu tat ca CTKM theo muc de ap dung chinh sua so suat
							SPCreateOrder._promotionPortionReceivedList = new Map();
							SPCreateOrder._mapErrProgram = new Map();
							for (var i=0;i<result.saleOrderQuantityReceivedList.length;i++) {
								var tmp = result.saleOrderQuantityReceivedList[i];
								var listProduct = new Array();
								result.pp.forEach(function(item, tempIndex) {
									if (item.saleOrderDetail.programCode == tmp.promotionCode
										&& item.productGroupId == tmp.productGroupId
										&& item.saleOrderDetail.levelPromo == tmp.promotionLevel) {
										item.quantityReceived = tmp.quantityReceived;
										item.maxQuantityReceived = tmp.maxQuantityReceived;
										item.maxQuantityReceivedTotal = tmp.maxQuantityReceivedTotal;
										/*if (item.promotionType == PromotionType.PROMOTION_FOR_ZV21) {
											item.listPromotionForPromo21.forEach(function(item21,index) {
												item21.quantityReceived = tmp.quantityReceived;
												item21.maxQuantityReceived = tmp.maxQuantityReceived;
												listProduct.push(item21);
											});
										} else {
											listProduct.push(item);
										}*/
										if (item.promotionType != PromotionType.PROMOTION_FOR_ZV21) {
											listProduct.push(item);
										}
									}
								});
								SPCreateOrder._promotionPortionReceivedList.put(tmp.promotionCode+"_"+tmp.productGroupId+"_"+tmp.promotionLevel, listProduct);
								if (result.zV21C) {
									result.zV21C.forEach(function(item, tempIndex) {
										if (item.saleOrderDetail.programCode == tmp.promotionCode
											&& item.productGroupId == tmp.productGroupId
											&& item.saleOrderDetail.levelPromo == tmp.promotionLevel) {
											item.listPromotionForPromo21.forEach(function(item21,index) {
												item21.quantityReceived = tmp.quantityReceived;
												item21.maxQuantityReceived = tmp.maxQuantityReceived;
												item21.maxQuantityReceivedTotal = tmp.maxQuantityReceivedTotal;
												listProduct.push(item21);
											});
										}
									});
								} else if (result.zV21) {
									result.zV21.forEach(function(item, tempIndex) {
										if (item.saleOrderDetail.programCode == tmp.promotionCode
											&& item.productGroupId == tmp.productGroupId
											&& item.saleOrderDetail.levelPromo == tmp.promotionLevel) {
											item.listPromotionForPromo21.forEach(function(item21,index) {
												item21.quantityReceived = tmp.quantityReceived;
												item21.maxQuantityReceived = tmp.maxQuantityReceived;
												item21.maxQuantityReceivedTotal = tmp.maxQuantityReceivedTotal;
												listProduct.push(item21);
											});
										}
									});
								}
								if (result.zV19zV20C) {
									result.zV19zV20C.forEach(function(item, tempIndex) {
										if (item.saleOrderDetail.programCode == tmp.promotionCode
											&& item.productGroupId == tmp.productGroupId
											&& item.saleOrderDetail.levelPromo == tmp.promotionLevel) {
											listProduct.push(item);
										}
									});
								} else if (result.zV19zV20) {
									result.zV19zV20.forEach(function(item, tempIndex) {
										if (item.saleOrderDetail.programCode == tmp.promotionCode
											&& item.productGroupId == tmp.productGroupId
											&& item.saleOrderDetail.levelPromo == tmp.promotionLevel) {
											listProduct.push(item);
										}
									});
								}
								if (result.zV21MM) {
									result.zV21MM.forEach(function(item, tempIndex) {
										if (item.saleOrderDetail.programCode == tmp.promotionCode
											&& item.productGroupId == tmp.productGroupId
											&& item.saleOrderDetail.levelPromo == tmp.promotionLevel) {
											item.listPromotionForPromo21.forEach(function(item21,index) {
												item21.quantityReceived = tmp.quantityReceived;
												item21.maxQuantityReceived = tmp.maxQuantityReceived;
												item21.maxQuantityReceivedTotal = tmp.maxQuantityReceivedTotal;
												listProduct.push(item21);
											});
										}
									});
								}
								if (result.zV19_20MM) {
									result.zV19_20MM.forEach(function(item, tempIndex) {
										if (item.saleOrderDetail.programCode == tmp.promotionCode
											&& item.productGroupId == tmp.productGroupId
											&& item.saleOrderDetail.levelPromo == tmp.promotionLevel) {
											listProduct.push(item);
										}
									});
								}
								/*if (listProduct.length == 0) {
									SPCreateOrder._promotionPortionReceivedList.put(tmp.promotionCode+"_"+tmp.productGroupId+"_"+tmp.promotionLevel, listProduct);
								} else {
									SPCreateOrder._promotionPortionReceivedList.put(tmp.promotionCode+"_"+tmp.productGroupId+"_"+tmp.promotionLevel, listProduct);
								}*/
								SPCreateOrder._promotionPortionReceivedList.put(tmp.promotionCode+"_"+tmp.productGroupId+"_"+tmp.promotionLevel, listProduct);
								if (tmp.promotionProgram.type == 'ZV19' || tmp.promotionProgram.type == 'ZV20' || tmp.promotionProgram.type == 'ZV21') {
									SPCreateOrder._promotionOrderReceivedList.put(tmp.promotionProgram.promotionProgramCode+"_"+tmp.productGroupId+"_"+tmp.promotionLevel, tmp);
								}
								if (tmp.promotionProgram.type == 'ZV24' && tmp.orderPromo == 1) {
									SPCreateOrder._promotionPortionReceivedList.put(tmp.promotionCode+"_"+tmp.productGroupId+"_"+tmp.promotionLevel, tmp);
								}
								// vuongmq; Begin 11/01/2015; gan thong bao loi nhieu dong
								var salePrTmp = SPCreateOrder._mapErrProgram.get(tmp.promotionCode);
								if (salePrTmp != null) {
									salePrTmp.quantityReceivedProgram += tmp.quantityReceived;
									salePrTmp.numReceivedProgram += tmp.numReceived;
									salePrTmp.amountReceivedProgram += tmp.amountReceived;
									salePrTmp.maxQuantityReceivedTotalProgram += tmp.maxQuantityReceivedTotal;									
									if (tmp != null && tmp.exceedObject != null && tmp.exceedUnit != null) {
										if (salePrTmp.flagPromoQuantity != null) {
											if (tmp.flagPromoQuantity > salePrTmp.flagPromoQuantity) {
												salePrTmp.flagPromoQuantity = tmp.flagPromoQuantity;
											}
										} else {
											salePrTmp.flagPromoQuantity = tmp.flagPromoQuantity;
										}
										if (salePrTmp.exceedUnit != null) {
											if (tmp.exceedUnit > salePrTmp.exceedUnit) {
												salePrTmp.exceedUnit = tmp.exceedUnit;
											}
										} else {
											salePrTmp.exceedUnit = tmp.exceedUnit;
										}
										if (salePrTmp.exceedObject != null) {
											if (tmp.exceedObject > salePrTmp.exceedObject) {
												salePrTmp.exceedObject = tmp.exceedObject;
											}
										} else {
											salePrTmp.exceedObject = tmp.exceedObject;
										}
									}
									SPCreateOrder._mapErrProgram.put(tmp.promotionCode, salePrTmp);
								} else {
									var salePromotionView = new Object();
									salePromotionView.promotionCode = tmp.promotionCode;
									salePromotionView.promotionName = tmp.promotionName;
									salePromotionView.quantityReceivedProgram = tmp.quantityReceived;
									salePromotionView.numReceivedProgram = tmp.numReceived;
									salePromotionView.amountReceivedProgram = tmp.amountReceived;
									salePromotionView.maxQuantityReceivedTotalProgram = tmp.maxQuantityReceivedTotal;	
									salePromotionView.exceedObject = tmp.exceedObject;	
									salePromotionView.exceedUnit = tmp.exceedUnit;
									salePromotionView.flagPromoQuantity = tmp.flagPromoQuantity;
									SPCreateOrder._mapErrProgram.put(tmp.promotionCode, salePromotionView);
								}
							}
							if (SPCreateOrder._mapErrProgram.keyArray != null && SPCreateOrder._mapErrProgram.keyArray.length > 0) {
								var errMsgProgram = '';
								var dem = 1;
								for (var i = 0, len = SPCreateOrder._mapErrProgram.keyArray.length; i < len; i++) {
									//var keyProgramErr = SPCreateOrder._mapErrProgram.keyArray[i];
									//errMsgProgram += (i + 1) + '. ' + SPCreateOrder._mapErrProgram.get(keyProgramErr) + '<br>';
									var keyProgramErr = SPCreateOrder._mapErrProgram.keyArray[i];
									var program = SPCreateOrder._mapErrProgram.get(keyProgramErr);
									if (program != null && program.exceedObject != null && program.exceedUnit != null) {
										var errKhongNhanKM = 'Đơn hàng không được nhận khuyến mãi {0} do {1} đã dùng hết {2} phân bổ';
										var errNhanKM = 'Đơn hàng chỉ nhận được {0} suất của khuyến mãi {1} do {2} chỉ còn nhận tối đa {3}';
										var errObject = '';
										var errValueUnit = '';
										var valueUnit;
										if (program.exceedObject == SPCreateOrder._SALE_ORDER_PROMOTION_OBJECT_SHOP) {
											errObject = 'nhà phân phối';
										} else if (program.exceedObject == SPCreateOrder._SALE_ORDER_PROMOTION_OBJECT_STAFF) {
											errObject = 'nhân viên bán hàng';
										} else if (program.exceedObject == SPCreateOrder._SALE_ORDER_PROMOTION_OBJECT_CUSTOMER) {
											errObject = 'khách hàng';
										}
										if (program.exceedUnit == SPCreateOrder._SALE_ORDER_PROMOTION_UNIT_QUANTITY) {
											errUnit = 'số suất';
											errValueUnit = program.quantityReceivedProgram + ' ' + errUnit;
										} else if (program.exceedUnit == SPCreateOrder._SALE_ORDER_PROMOTION_UNIT_NUM) {
											errUnit = 'số lượng';
											errValueUnit = program.numReceivedProgram + ' ' + errUnit;
										} else if (program.exceedUnit == SPCreateOrder._SALE_ORDER_PROMOTION_UNIT_AMOUNT) {
											errUnit = 'số tiền';
											errValueUnit = errUnit + ' ' + formatCurrency(program.amountReceivedProgram);
										}
										var promotionCodeName = Utils.XSSEncode(program.promotionCode + ' - ' + program.promotionName);
										if (program.flagPromoQuantity == SPCreateOrder._DAT_KHONG_NHAN_KM) {
											errKhongNhanKM = format(errKhongNhanKM, promotionCodeName, errObject, errUnit);
											errMsgProgram += dem + '. ' + errKhongNhanKM + '<br>';
										} else if (program.flagPromoQuantity == SPCreateOrder._DAT_NHAN_MOT_PHAN_KM) {
											var quantityReceivedView = program.quantityReceivedProgram + '/' + program.maxQuantityReceivedTotalProgram;
											errNhanKM = format(errNhanKM, quantityReceivedView, promotionCodeName, errObject, errValueUnit);
											errMsgProgram += dem + '. ' + errNhanKM + '<br>';
										}
										dem++;
									}
								}
								if (errMsgProgram.length > 0) {
									$.messager.alert('Các CTKM không đủ số suất', errMsgProgram, 'warning');
								}
								// vuongmq; End 11/01/2015; gan thong bao loi nhieu dong
							}
							//Luu thong tin muc cua tung CTKM 
							if (SPCreateOrder.programes != undefined && SPCreateOrder.programes != null) {
								for (var i = 0;i < SPCreateOrder.programes.length;i++) {
									var obj = SPCreateOrder.programes[i];
									var tmp = result.saleOrderQuantityReceivedList[i];
									if (tmp.promotionCode == obj.promotionProgramCode) {
										SPCreateOrder.programes[i].promotionLevel =  tmp.promotionLevel;
									};
								};
							}
							
							/*if(!isNullOrEmpty(SPAdjustmentOrder.orderId)){
								var promotionMapSize = SPCreateOrder._lstPromotionProductForOrderIsAuto.size();
								for(var i=0;i<promotionMapSize;++i){								
									var promotionProduct = SPCreateOrder._lstPromotionProductForOrderIsAuto.get(SPCreateOrder._lstPromotionProductForOrderIsAuto.keyArray[i]);
									var productCode = promotionProduct.productCode;									
									if(!isNullOrEmpty(productCode)){
										var stockTotal = SPCreateOrder._PRODUCTS.get(productCode);
										if(stockTotal!=null)
											promotionProduct.stock = stockTotal.quantity;
										SPCreateOrder._lstPromotionProductForOrderIsAuto.put(i,promotionProduct);
									}
								}
							}*/

							// sap xep lai danh sach san pham
							OpenProductPromotion.sortProductList(SPCreateOrder._lstPromotionProductIsAuto, false);
							OpenProductPromotion.sortProductList(SPCreateOrder._lstPromotionProductForOrderIsAuto, true);

							SPCreateOrder.fillTablePromotionProduct(SPCreateOrder._lstPromotionProductIsAuto);
							SPCreateOrder.fillTablePromotionProductForOrder(SPCreateOrder._lstPromotionProductForOrderIsAuto);
							OpenProductPromotion.loadOpenProductGrid(SPCreateOrder._lstPromotionOpenProduct);
							SPCreateOrder._lstPromotionProductSaleLotVO = new Map();
							if (SPAdjustmentOrder._orderId != null) {
								$('#alertSuccess_successMsg').html('Đơn hàng vừa thực hiện tính lại KM.').show();
							} else {
								$('#alertSuccess_successMsg').show();
							}
							if (!isNullOrEmpty(result.errMsgInvalidPrice)) {
								var msg1 = $('#generalInfoDiv #GeneralInforErrors').html();
								if (msg1.length > 0) {
									msg1 = msg1 + "<br/>"
								}
								$('#generalInfoDiv #GeneralInforErrors').html(msg1 + 'Lỗi: ' + result.errMsgInvalidPrice).show();
								disabled('btnOrderSave');
								disabled('btnOrderReset');
								enable('payment');
								if ($("#btnApproveOrderIN").length > 0) {
									disabled("btnApproveOrderIN");
								}
							} else {
								var tmBanChar = setTimeout(function(){$('#alertSuccess_successMsg').hide();	clearTimeout(tmBanChar);},5000);
								setTimeout(function() {SPCreateOrder._syncFlag._allowAppliedNewProgram = false;}, 2500);
								disabled('payment');
								enable('btnOrderSave');
								SPCreateOrder.orderChanged = true;
								if ($("#btnApproveOrderIN").length > 0 && $("#btnApproveOrderIN").attr("onclick")) {
									enable("btnApproveOrderIN");
								}
							}
							
							//TungMT - Lưu danh sách SP đạt các CTKM
							SPCreateOrder._lstProgramDatSaleProduct = new Array();
							//Danh sách CTKM tiền, %
							for (var i = 0 ; i < SPCreateOrder._mapPromoDetail.keyArray.length ; i++) {
								var val = SPCreateOrder._mapPromoDetail.valArray[i];
								for (var j = 0 ; j < val.length ; j++) {
									SPCreateOrder._lstProgramDatSaleProduct.push({'programCode': val[j].programCode, 'productId': SPCreateOrder._mapPromoDetail.keyArray[i]});
								}
							}
							//Danh sách CTKM SP
							for (var i = 0 ; i < result.pp.length ; i++) {
								var pTemp = result.pp[i];
								if (pTemp.listBuyProduct != null) {
									var programCode = pTemp.saleOrderDetail.programCode;
									for (var j = 0 ; j < pTemp.listBuyProduct.length ; j++) {
										SPCreateOrder._lstProgramDatSaleProduct.push({'programCode':programCode, 'productId': pTemp.listBuyProduct[j].product.id});
									}
								}
							}
							//----------------------------------------
							/*
							 * thong bao loi SPKM ko co ton kho
							 */
							if (warehouselessProductMap.keyArray.length > 0) {
								var errorMessage = warehouselessProductMap.keyArray.reduce(function(prevProgramStr, programCode) {
													var products = warehouselessProductMap.get(programCode);
													if (products && products.length > 0) {
														var productsStr = products.reduce(function(prevProductStr, productCode) {
																			return prevProductStr + ', ' + Utils.XSSEncode(productCode);
																		});
														return prevProgramStr ? prevProgramStr : '' + '<br>Sản phẩm ' + Utils.XSSEncode(productsStr) + ' của CTKM ' + Utils.XSSEncode(programCode);
													} else {
														return prevProgramStr;
													}													
												}, '');
								errorMessage = 'Các SPKM sau không có tồn kho. Vui lòng nhập kho cho sản phẩm để được hưởng KM.'
											+ '<br>'
											+ errorMessage;
								$.messager.alert('Các SPKM không có tồn kho', errorMessage, 'warning');
							}
							
							xhrSave = null;
							SPCreateOrder.showTotalGrossWeightAndAmount();
							SPCreateOrder.updatePromotionGrids();
							setTimeout(function(){
								if ($('#chkKeyshop').is(':checked')) {
									var lst = SPCreateOrder.calKeyShop();
									$('#gridKeyShop').datagrid('loadData',lst);
									//sau khi tính lại key shop thì tính lại chiết khấu luôn
									var htSale = $("#gridSaleData").handsontable("getInstance");
									var rows = htSale.getData();
									SPCreateOrder._lstDiscountKeyShop = new Map();
									for (var i = 0 ; i < lst.length ; i++) {
										if (lst[i].productId == null) {
											var totalAmount = 0;
											for (var k = 0 ; k < rows.length ; k++) {
												if (rows[k][COLSALE.PRO_CODE] && rows[k][COLSALE.AMOUNT]) {
													totalAmount += Number(rows[k][COLSALE.AMOUNT].replace(/,/g,''));
												}
											}
											if (totalAmount == 0) {break;}
											var discountTemp = 0;//Dùng để dòng cuối cùng ko tính mà lấy tổng trừ cho discountTemp
											for (var k = 0 ; k < rows.length -1 ; k++) {
												if (rows[k][COLSALE.PRO_CODE] && rows[k][COLSALE.AMOUNT]) {
													var amount = Number(rows[k][COLSALE.AMOUNT].replace(/,/g,''));
													var currentDiscount = Number(rows[k][COLSALE.DISCOUNT].replace(/,/g,''));
													var discount = Math.floor(lst[i].quantityAmount*amount/totalAmount);
													if (k == rows.length - 2) {//dòng cuối thì lấy tổng chiết khấu trừ các dòng còn lại
														discount = lst[i].quantityAmount - discountTemp;
													}
													discountTemp += discount;
													var obj = new Object();
													obj.discount = discount;
													obj.detail = lst[i].ksCode;
													obj.maxDiscount = Number(lst[i].total) - Number(lst[i].totalDone);
													var lstDiscountByKS = SPCreateOrder._lstDiscountKeyShop.get(k);
													if (lstDiscountByKS == null) {
														lstDiscountByKS = new Array();
													}
													lstDiscountByKS.push(obj);
													SPCreateOrder._lstDiscountKeyShop.put(k, lstDiscountByKS);//set key theo index dòng của grid mua
													htSale.setDataAtCell(k, COLSALE.DISCOUNT, formatCurrency(discount + currentDiscount));
												}
											}
										}
									} 
									SPCreateOrder.showTotalGrossWeightAndAmount();
									
									//thêm weight của sản phẩm trả thưởng keyshop
//									var weight = SPCreateOrder.getWeightOfKeyShop();
//									var totalWeight = Number($('#totalWeight').html().replace(/,/g,''));
//									totalWeight += weight;
//									$('#totalWeight').html(formatCurrency(Math.round10(totalWeight, -3)));
									//-------------------------------------------------------------
									
									$('#divOverlay').hide();
								}
							},1000);
						}
						if (!key) {
							/*
							 * hien thi ds san pham tich luy
							 */
							$('#divOverlay').show();
							setTimeout(function(){
								SPCreateOrder.loadGridAccumulative(result);
								$('#divOverlay').hide();
							},1000);
						}
					}
				},
				error:function(XMLHttpRequest, textStatus, errorThrown) {
					$('#divOverlay').hide();	
				}
			});
			
		}
	},
	
	/*
	 * tungmt - Kiểm tra programCode có phải đang trên grid KM đơn hàng hay ko 
	 */
	isShowDiscountPromotionOrder:function(programCode, programeTypeCode) {
		//Do KM đơn hàng chỉ cho chọn 1 trong các KM nên chỉ có 1 loại KM trên grid đơn hàng
		//Nên chỉ cần lấy programCode dòng đầu tiên là biết đơn hàng đang chọn KM đơn hàng nào 
		if (programeTypeCode == 'ZV19' || programeTypeCode == 'ZV20' || programeTypeCode == 'ZV21') {
			var rows = $('#gridSaleOrderPromotion').datagrid('getRows');
			if (rows != null && rows.length > 0 && programCode == rows[0].programCode) {
				return true;
			}
			return false;
		}
		return true;
	},
	
	/*
	 * tungmt: lay danh sách chiết khấu và tính theo từng CTKM
	 */
	getListDiscountDetailByProduct:function(proCode, r){
		var lst = new Array();
		var pro = GridSaleBusiness.mapSaleProduct.get(proCode);
		var rowQuantity = 0, totalQuantity = 0;
		var rows = $("#gridSaleData").handsontable("getInstance").getData();
		var row = rows[r];
		rowQuantity = row[COLSALE.QUANTITY];
		//tính tổng số lượng sp bán
		for (var i = 0 ; i < rows.length ; i++) {
			//kiểm tra nếu là dòng có cùng SP và ko phải KM tay thì + quantity
			if (proCode == rows[i][COLSALE.PRO_CODE] && !SPCreateOrder.checkCTHTTMValue(rows[i][COLSALE.PROGRAM])) {
				totalQuantity += Number(StockValidateInput.getQuantity(rows[i][COLSALE.QUANTITY], pro.convfact));
			}
		}
		//phân bổ theo số lượng
		var obj = SPCreateOrder._mapPromoDetail.get(pro.productId);
		if (obj != null && obj.length > 0) {
			for (var i = 0 ; i < obj.length ; i++) {
				//kiểm tra nếu là KM ZV 19->21 mà ko được chọn trên giao diện thì không show ra
				if (!SPCreateOrder.isShowDiscountPromotionOrder(obj[i].programCode, obj[i].programeTypeCode)) {
					continue;
				}
				var discount = Math.round10(obj[i].discountAmount * StockValidateInput.getQuantity(rowQuantity, pro.convfact) / totalQuantity , 0);
				var isKS = obj[i].programeTypeCode != null && obj[i].programeTypeCode == 'KS' ? true : false;
				lst.push({'programCode': obj[i].programCode,'discount': discount, 'isKS': isKS});
			}
		}
		
		//Sum cac discount theo CTKM - 
		//Do đơn hàng được KM nhiều mức nên sẽ có 2 dòng cho 1 CTKM -> khi show phải sum 2 dòng thành 1 
		//(Chỉ sum khi show, còn lưu thì vẫn tách)
		for (var i = 0 ; i < lst.length ; i++) {
			for (var j = i + 1 ; j < lst.length ; j++) {
				if (lst[i].programCode == lst[j].programCode) {
					lst[i].discount += lst[j].discount;
					lst.splice(j,1);
					j--;
				}
			}
		}
		//--------------------------------------------------------------------------
		
		//add thêm discount key shop
		if ($('#chkKeyshop').is(':checked')) {
			var lstDiscountByKS = SPCreateOrder._lstDiscountKeyShop.get(r);
			if (lstDiscountByKS != null) {
				for (var i = 0 ; i < lstDiscountByKS.length ; i++) {
					var r = lstDiscountByKS[i];
					lst.push({'programCode': r.detail,'discount': r.discount, 'isKS': true});
				}
			}
		}
		return lst;
	},
	
	getSaleOrderPromoLot:function() {
		var lst = new Array();
		var rows = $("#gridSaleData").handsontable("getInstance").getData();
		for (var k = 0 ; k < rows.length ; k++) {
			var rowQuantity = 0, totalQuantity = 0;
			if (!SPCreateOrder.checkCTHTTMValue(rows[k][COLSALE.PROGRAM])) {
				var row = rows[k];
				var warehouseId = GridSaleBusiness._mapWarehouseIdSelect.get(k);
				var proCode = row[COLSALE.PRO_CODE];
				var pro = GridSaleBusiness.mapSaleProduct.get(proCode);
				if (pro == null) {
					continue;
				}
				rowQuantity = row[COLSALE.QUANTITY];
				//tính tổng số lượng sp bán
				for (var i = 0 ; i < rows.length ; i++) {
					//kiểm tra nếu là dòng có cùng SP và ko phải KM tay thì + quantity
					if (proCode == rows[i][COLSALE.PRO_CODE] && !SPCreateOrder.checkCTHTTMValue(rows[i][COLSALE.PROGRAM])) {
						totalQuantity += Number(StockValidateInput.getQuantity(rows[i][COLSALE.QUANTITY], pro.convfact));
					}
				}
				var obj = SPCreateOrder._mapPromoDetail.get(pro.productId);
				if (obj != null && obj.length > 0) {
					for (var i = 0 ; i < obj.length ; i++) {
						//kiểm tra nếu là KM ZV 19->21 mà ko được chọn trên giao diện thì không show ra
						if (!SPCreateOrder.isShowDiscountPromotionOrder(obj[i].programCode, obj[i].programeTypeCode)) {
							continue;
						}
						var discount = Math.round10(obj[i].discountAmount * StockValidateInput.getQuantity(rowQuantity, pro.convfact) / totalQuantity , 0);
						var lot = new Object();
						lot.productId = pro.productId;
						lot.productCode = proCode;
						lot.warehouseId = warehouseId;
						lot.programCode = obj[i].programCode;
						lot.discount = discount;
						lst.push(lot);
					}
				}
			}
			var lstDiscountByKS = SPCreateOrder._lstDiscountKeyShop.get(k);
			if (lstDiscountByKS != null) {
				for (var i = 0 ; i < lstDiscountByKS.length ; i++) {
					var r = lstDiscountByKS[i];
					var lot = new Object();
					lot.productId = pro.productId;
					lot.productCode = proCode;
					lot.warehouseId = warehouseId;
					lot.programCode = r.detail;
					lot.discount = r.discount;
					lot.maxDiscount = r.maxDiscount;
					lot.isKS = true;
					lst.push(lot);
				}
			}
		}
		
		//merge các dòng trùng product, warehouse và programCode
		for (var i = 0 ; i < lst.length ; i++) {
			for (var j = i + 1 ; j < lst.length ; j++) {
				if (lst[i].productId == lst[j].productId && lst[i].warehouseId == lst[j].warehouseId && lst[i].programCode == lst[j].programCode) {
					lst[i].discount += lst[j].discount;
					lst.splice(j,1);
					j--;
				}
			}
		}
		
		return lst;
	},
	
	/*
	 * tungmt : Xử lý load lại chi tiết CKMH đối với từng CTKM theo sản phẩm 
	 */
	showDiscountPromoDetail:function(proCode, r) {
		var lst = SPCreateOrder.getListDiscountDetailByProduct(proCode, r);
		var htmlDiscount = $('#discountDetailDialogDiv').html();
		$('#discountDetailDialog').dialog({
			title : 'Danh sách chiết khấu theo chương trình',
			width:400,
			height:'auto',
			onOpen: function(){
				if(SPCreateOrder._isLoadGridDiscountDetail != 1){
					$('#gridDiscountDetail').datagrid('loadData',lst);
				}else{
					KeyShop._isLoadGridLevel = 0;
					$('#gridDiscountDetail').datagrid({
						data: lst,
						singleSelect: true,
						width:$('#discountDetailDiv').width() - 10,				
						fitColumns:true,		
						scrollbarSize:0,
						autoRowHeight : true,
						columns: [[
						    {field:'programCode',title:'Chiết khấu',width:70,resizable:false, sortable:false,align:'left', formatter: function(v,r,i){
						    	if (r.isKS == undefined || r.isKS == null || !r.isKS) {
						    		return '<a class="delIcons" onclick="return SPCreateOrder.showPromotionProgramDetail(\'' + Utils.XSSEncode(v) + '\');" href="javascript:void(0)">' + Utils.XSSEncode(v) + '</a>';
						    	} else {// ks thì không có chi tiết
						    		return '<a class="delIcons" onclick="return SPCreateOrder.showKeyShopDetail(\'' + Utils.XSSEncode(v) + '\');" href="javascript:void(0)">' + Utils.XSSEncode(v) + '</a>';
						    	}
						    }},
						    {field:'discount', title:'Số tiền', align:'right', resizable:false, sortable:false, width: 70, formatter:function(v,r,i){
						    	return Utils.XSSEncode(formatCurrency(v));
						    }},
						]],	
						onLoadSuccess: function(data){
						}
					});
				}
			},
			onClose: function(){
				$('#discountDetailDialogDiv').html(htmlDiscount);
				$('#discountDetailDiv').html('<table id="gridDiscountDetail"></table>');
			}
		});
		$('#discountDetailDialog').dialog('open');
	},

	/**
	 * Xu ly thieu kho hay khong de cho phep sua so luong khuyen mai
	 */
	updatePromotionGrids: function() {
		if (SPAdjustmentOrder._isVansale) {
			return;
		}
		var mapT1 = new Map();
		var rows = $("#gridSaleData").handsontable('getData');
		var r = null;
		var qtt = null;
		var kkk = null;
		var lstWh = null;
		var p = null;
		for (var i = 0, sz = rows.length; i < sz; i++) {
			r = SPCreateOrder.getObjectSaleProductInGrid(rows[i], i);
			if (!(r && r.productCode)) {
				continue;
			}
			kkk = r.productCode+"_"+r.warehouseId;
			qtt = mapT1.get(kkk);
			if (qtt == null) {
				p = GridSaleBusiness.mapSaleProduct.get(r.productCode.trim());
//				lstWh = SPCreateOrder._mapPRODUCTSbyWarehouse.get(p.productId);
				lstWh = GridSaleBusiness.getWarehouseByType(p.productId);
				if (lstWh instanceof Array && lstWh.length > 0) {
					for (var j = 0, szj = lstWh.length; j < szj; j++) {
						if (lstWh[j].warehouseId == r.warehouseId) {
							qtt = lstWh[j].availableQuantity - r.quantity;
							break;
						}
					}
				} else {
					qtt = 0;
				}
			} else {
				qtt = qtt - r.quantity;
			}
			mapT1.put(kkk, qtt);
		}

		if ($("#spPromotionProductInfor .datagrid-view").length > 0) {
			rows = $('#gridPromotionProduct').datagrid('getRows');
			for (var i = 0, sz = rows.length; i < sz; i++) {
				if (r.warehouse == null || r.warehouse == undefined) {
					continue;
				}
				r = rows[i];
				kkk = r.productCode+"_"+(r.warehouse.warehouseId||r.warehouse.id);
				qtt = mapT1.get(kkk);
				if (qtt == null) {
//					lstWh = SPCreateOrder._mapPRODUCTSbyWarehouse.get(r.productId);
					lstWh = GridSaleBusiness.getWarehouseByType(r.productId);
					if (lstWh instanceof Array && lstWh.length > 0) {
						for (var j = 0, szj = lstWh.length; j < szj; j++) {
							if (lstWh[j].warehouseId == (r.warehouse.warehouseId||r.warehouse.id)) {
								qtt = lstWh[j].availableQuantity - r.quantity;
								break;
							}
						}
					} else {
						qtt = 0;
					}
				} else {
					qtt = qtt - r.quantity;
				}
				mapT1.put(kkk, qtt);
				if (qtt < 0 && r.isEdited == 0) {
					if (r.hasPortion == 1) {
						for (var j = 0; j < sz; j++) {
							if (r.programCode == rows[j].programCode && r.productGroupId == rows[j].productGroupId
								&& r.levelPromo == rows[j].levelPromo) {
								$("#portion_promotion_value_"+i).removeAttr("disabled");
								break;
							}
						}
					} else {
						$("#convact_promotion_value_"+i).removeAttr("disabled");
					}
				}
			}
		}

		if ($("#promotionGridContainer2 .datagrid-view").length > 0) {
			rows = $('#promotionGrid2').datagrid('getRows');
			for (var i = 0, sz = rows.length; i < sz; i++) {
				r = rows[i];
				if (r.productCode.trim().indexOf(" ") > 0 || r.warehouse == null || r.warehouse == undefined) {
					continue; // khong xu ly doi voi dong header cua khuyen mai don hang
				}
				kkk = r.productCode+"_"+(r.warehouse.warehouseId||r.warehouse.id);
				qtt = mapT1.get(kkk);
				if (qtt == null) {
//					lstWh = SPCreateOrder._mapPRODUCTSbyWarehouse.get(r.productId);
					lstWh = GridSaleBusiness.getWarehouseByType(r.productId);
					if (lstWh instanceof Array && lstWh.length > 0) {
						for (var j = 0, szj = lstWh.length; j < szj; j++) {
							if (lstWh[j].warehouseId == (r.warehouse.warehouseId||r.warehouse.id)) {
								qtt = lstWh[j].availableQuantity - r.quantity;
								break;
							}
						}
					} else {
						qtt = 0;
					}
				} else {
					qtt = qtt - r.quantity;
				}
				mapT1.put(kkk, qtt);
				if (qtt < 0 && r.isEdited == 0) {
					if (r.hasPortion == 1) {
						for (var j = 0; j < sz; j++) {
							if (r.programCode == rows[j].programCode && r.productGroupId == rows[j].productGroupId
								&& r.levelPromo == rows[j].levelPromo) {
								$("#op_portion_promotion_value_"+i).removeAttr("disabled");
								break;
							}
						}
					} else {
						$("#op_convact_promotion_value_"+i).removeAttr("disabled");
					}
				}
			}
		}

		if ($("#saleOrderPromotion .datagrid-view").length > 0) {
			rows = $('#gridSaleOrderPromotion').datagrid('getRows');
			for (var i = 0, sz = rows.length; i < sz; i++) {
				r = rows[i];
				if (r.productCode.trim().indexOf(" ") > 0 || r.warehouse == null || r.warehouse == undefined) {
					continue; // khong xu ly doi voi dong header cua khuyen mai don hang
				}
				kkk = r.productCode+"_"+(r.warehouse.warehouseId||r.warehouse.id);
				qtt = mapT1.get(kkk);
				if (qtt == null) {
//					lstWh = SPCreateOrder._mapPRODUCTSbyWarehouse.get(r.productId);
					lstWh = GridSaleBusiness.getWarehouseByType(r.productId);
					if (lstWh instanceof Array && lstWh.length > 0) {
						for (var j = 0, szj = lstWh.length; j < szj; j++) {
							if (lstWh[j].warehouseId == (r.warehouse.warehouseId||r.warehouse.id)) {
								qtt = lstWh[j].availableQuantity - r.quantity;
								break;
							}
						}
					} else {
						qtt = 0;
					}
				} else {
					qtt = qtt - r.quantity;
				}
				mapT1.put(kkk, qtt);
				if (qtt < 0 && r.isEdited == 0) {
					if (r.hasPortion == 1) {
						for (var j = 0; j < sz; j++) {
							if (r.programCode == rows[j].programCode && r.productGroupId == rows[j].productGroupId
								&& r.levelPromo == rows[j].levelPromo) {
								$("#portion_promotion_value_order_"+i).removeAttr("disabled");
								break;
							}
						}
					} else {
						$("#convact_promotion_value_order_"+i).removeAttr("disabled");
					}
				}
			}
		}
	},
	
	increBuyProduct:function(buyProducts){
		if(buyProducts == undefined || buyProducts == null || buyProducts.length == 0) {
			return null;
		}
		var temp = jQuery.extend(true, [], buyProducts[0]);
		//cong don cac list buyproduct thanh 1 list
		for(var x = 1 ; x < buyProducts.length ; x++){
			for(var y = 0 ; y < buyProducts[x].length ; y++){
				var existsProduct = false;
				for(var z = 0 ; z < temp.length ; z++){
					if(buyProducts[x][y].productId == temp[z].productId){
						temp[z].value = Math.round10(temp[z].value + buyProducts[x][y].value,-2);
						existsProduct = true;
					}
				}
				if(!existsProduct){//chua co trong lst thi add them vao
					temp.push(jQuery.extend(true, {}, buyProducts[x][y]));
				}
			}
		}
		return temp;
	},
	
	/**
	 * Phân bổ lại số tiền KM nếu thay đổi ZV 19,20
	 * @author tungmt
	 * @date 15/12/2014
	 */
	changeGridAccumulativeWhenChangeProgram: function(){
		var rows = $('#accumulativePromotionGrid').datagrid('getRows');
		var lstResult = new Array();
		var totalAcc = 0;
		var totalAmount = Number($('#total').html().replace(/,/g,''));
		for(var i=0 ; i < rows.length ; i++){
			if(rows[i].isHeader || (rows[i].productId != undefined && rows[i].productId != null)){
				lstResult.push(rows[i]);
			}else if(rows[i].lstAmountPercent != undefined && rows[i].lstAmountPercent != null){//KM %
				var amountPercent = 0;
				var m=0;
				var lstRationActualy = new Map();
				for(m = 0 ; m < rows[i].lstAmountPercent.length ; m++){
					if(totalAcc + rows[i].lstAmountPercent[m] > totalAmount){
						continue;
					}else{
						totalAcc += rows[i].lstAmountPercent[m];
						amountPercent += rows[i].lstAmountPercent[m];
						lstRationActualy.put(m,m);
					}
				}
				var strConlai = '';
				rows[i].rationActualy = lstRationActualy.keyArray;
				if(lstRationActualy.size() > 0 && lstRationActualy.size() < rows[i].lstAmountPercent.length){
					var conlai = 0;
					for(k = 0 ; k < rows[i].lstAmountPercent.length ; k++){
						if(lstRationActualy.get(k) == null ){
							conlai += rows[i].lstAmountPercent[k];
						}
					}
					strConlai = ' (còn lại ' + formatCurrency(conlai) + ')';
				}
				rows[i].value = amountPercent;
				rows[i].shareDiscount = SPCreateOrder.shareDiscountSaleProduct(rows[i].value);
				SPCreateOrder._totalAcc.push(rows[i].shareDiscount);
				rows[i].valueText = formatCurrency(rows[i].value) + strConlai;
				if(rows[i].value > 0){
					lstResult.push(rows[i]);
				}else{
					SPCreateOrder._lstCTTLMoneyQueue.push(rows[i]);
				}
			}else{//KM tien
				var strConlai = '';
				if(rows[i].subGroupLevel != null){
					if(totalAcc < totalAmount){
						var k=0;
						var lstRationActualy = new Map();
						for(k = 0 ; k < rows[i].ratiton ; k++){
							if(totalAcc + rows[i].subGroupLevel.maxAmount > totalAmount){
								break;
							}else{
								totalAcc += rows[i].subGroupLevel.maxAmount;
								lstRationActualy.put(k,k);
							}
						}
						rows[i].rationActualy = lstRationActualy.keyArray;
						rows[i].value = k * rows[i].subGroupLevel.maxAmount;
						rows[i].shareDiscount = SPCreateOrder.shareDiscountSaleProduct(rows[i].value);
						SPCreateOrder._totalAcc.push(rows[i].shareDiscount);
						if(k > 0 && k < rows[i].ratiton){
							var conlai = (rows[i].ratiton - k ) * rows[i].subGroupLevel.maxAmount;
							strConlai = ' (còn lại ' + formatCurrency(conlai) + ')';
						}
					}else{
						rows[i].rationActualy = [];
						rows[i].value = 0;
					}
				}
				rows[i].valueText = formatCurrency(rows[i].value) + strConlai;
				if(rows[i].value > 0){
					lstResult.push(rows[i]);
				}else{
					SPCreateOrder._lstCTTLMoneyQueue.push(rows[i]);
				}
			}
		}
		//tru tiep
		if(totalAcc < totalAmount && SPCreateOrder._lstCTTLMoneyQueue.length > 0){
			for(var i = 0 ; i < SPCreateOrder._lstCTTLMoneyQueue.length ; i++){
				var row = SPCreateOrder._lstCTTLMoneyQueue[i];
				if(row.lstAmountPercent != undefined && row.lstAmountPercent != null){//KM %
					var amountPercent = 0;
					var m=0;
					var lstRationActualy = new Map();
					for(m = 0 ; m < row.lstAmountPercent.length ; m++){
						if(totalAcc + row.lstAmountPercent[m] > totalAmount){
							continue;
						}else{
							totalAcc += row.lstAmountPercent[m];
							amountPercent += row.lstAmountPercent[m];
							lstRationActualy.put(m,m);
						}
					}
					var strConlai = '';
					row.rationActualy = lstRationActualy.keyArray;
					if(lstRationActualy.size() > 0 && lstRationActualy.size() < row.lstAmountPercent.length){
						var conlai = 0;
						for(k = 0 ; k < row.lstAmountPercent.length ; k++){
							if(lstRationActualy.get(k) == null){//neu ko co trong list phan bo thi + vao
								conlai += row.lstAmountPercent[k];
							}
						}
						strConlai = ' (còn lại ' + formatCurrency(conlai) + ')';
					}
					row.value = amountPercent;
					row.shareDiscount = SPCreateOrder.shareDiscountSaleProduct(row.value);
					SPCreateOrder._totalAcc.push(row.shareDiscount);
					row.valueText = formatCurrency(row.value) + strConlai;
					if(row.value > 0){
						lstResult.push(row);
						SPCreateOrder._lstCTTLMoneyQueue.splice(i,1);
						i--;
					}
				}else{//KM tien
					var strConlai = '';
					if(row.subGroupLevel != null){
						if(totalAcc < totalAmount){
							var k=0;
							var lstRationActualy = new Map();
							for(k = 0 ; k < row.ratiton ; k++){
								if(totalAcc + row.subGroupLevel.maxAmount > totalAmount){
									break;
								}else{
									totalAcc += row.subGroupLevel.maxAmount;
									lstRationActualy.put(k,k);
								}
							}
							row.rationActualy = lstRationActualy.keyArray;
							row.value = k * row.subGroupLevel.maxAmount;
							row.shareDiscount = SPCreateOrder.shareDiscountSaleProduct(row.value);
							SPCreateOrder._totalAcc.push(row.shareDiscount);
							if(k > 0 && k < row.ratiton){
								var conlai = (row.ratiton - k ) * row.subGroupLevel.maxAmount;
								strConlai = ' (còn lại ' + formatCurrency(conlai) + ')';
							}
						}else{
							row.rationActualy = [];
							row.value = 0;
						}
					}
					row.valueText = formatCurrency(row.value) + strConlai;
					if(row.value > 0){
						lstResult.push(row);
						SPCreateOrder._lstCTTLMoneyQueue.splice(i,1);
						i--;
					}
				}
			}
		}
		$('#accumulativePromotionGrid').datagrid('loadData',lstResult);
		if(totalAcc != 0 && totalAcc <= totalAmount){
			totalAmount -= totalAcc;
			$('#total').html(formatCurrency(totalAmount));
//			SPCreateOrder._totalAcc = totalAcc;
//			SPCreateOrder.shareDiscountSaleProduct(totalAcc);
		}
	},
	
	unshareDiscountSaleProduct:function(totalAcc){
		var htSale = $("#gridSaleData").handsontable("getInstance");
		var rows1 = htSale.getData();
		if (rows1 && rows1.length > 0) {
			var r = null;
			var rows = [];
			var ids = [];
			for (var i = 0, sz = rows1.length; i < sz; i++) {
				r = rows1[i];
				if (r[COLSALE.PRO_CODE]) {
					rows.push(r);
					ids.push(i);
				}
			}
			var discount = 0;
			for (var i = 0, sz = rows.length; i < sz; i++) {
				r = rows[i];
				if (r[COLSALE.PRO_CODE]) {
					var d = 0;
					for(var j = 0 ; j < totalAcc.length ; j++){
						for(var k = 0 ; k < totalAcc[j].length ; k++){
							if(r[COLSALE.PRO_CODE] == totalAcc[j][k].productCode && totalAcc[j][k].value > 0){
								d += totalAcc[j][k].value;
								totalAcc[j][k].value=0;
								break;
							}
						}
					}
					if (i == sz - 1) {
						var _dis = r[COLSALE.DISCOUNT];
						var dis = 0;
						if (_dis) {
							dis = Number(_dis.replace(/,/g, ''));
						}
						dis -= d;
						if(dis < 0) dis = 0;
						htSale.setDataAtCell(ids[i], COLSALE.DISCOUNT, formatCurrency(dis));
					} else {
						var _dis = r[COLSALE.DISCOUNT];
						var dis = 0;
						if (_dis) {
							dis = Number(_dis.replace(/,/g, ''));
						}
						dis -= d;
						if(dis < 0) dis = 0;
						htSale.setDataAtCell(ids[i], COLSALE.DISCOUNT, formatCurrency(dis));
					}
				}
			}
		}
	},
	
	shareDiscountSaleProduct:function(totalAcc, loadForEdit){
		var lstResult = new Array();
		var htSale = $("#gridSaleData").handsontable("getInstance");
		var rows1 = htSale.getData();
		if (rows1 && rows1.length > 0) {
			var r = null;
			var amount = Number($("#totalAmountSale").html().replace(/,/g, ''));
			var rows = [];
			var ids = [];
			for (var i = 0, sz = rows1.length; i < sz; i++) {
				r = rows1[i];
				var _am = r[COLSALE.AMOUNT];
				var am = 0;
				if (_am) {
					am = Number(_am.replace(/,/g, ''));
				}
				if (r[COLSALE.PRO_CODE] && am > 0) {
					rows.push(r);
					ids.push(i);
				}
			}
			var discount = 0;
			var d = 0;
			for (var i = 0, sz = rows.length; i < sz; i++) {
				r = rows[i];
				if (r[COLSALE.PRO_CODE]) {
					if (i == sz - 1) {
						var _dis = r[COLSALE.DISCOUNT];
						var dis = 0;
						if (_dis) {
							dis = Number(_dis.replace(/,/g, ''));
						}
						d = totalAcc - discount;
						dis += d;
						lstResult.push({productCode:r[COLSALE.PRO_CODE],value:d});
						discount = totalAcc;
						if (!loadForEdit) { // luc moi load len de chinh sua khong can set lai gia tri nay
							htSale.setDataAtCell(ids[i], COLSALE.DISCOUNT, formatCurrency(dis));
						}
					} else {
						var _am = r[COLSALE.AMOUNT];
						var am = 0;
						if (_am) {
							am = Number(_am.replace(/,/g, ''));
						}
						var _dis = r[COLSALE.DISCOUNT];
						var dis = 0;
						if (_dis) {
							dis = Number(_dis.replace(/,/g, ''));
						}
//						d = Math.round(am / amount * totalAcc);
						d = Math.round(Math.round10(totalAcc/amount*100,-2)*am/100);
						discount += d;
						dis += d;
						lstResult.push({productCode:r[COLSALE.PRO_CODE],value:d});
						if (!loadForEdit) { // luc moi load len de chinh sua khong can set lai gia tri nay
							htSale.setDataAtCell(ids[i], COLSALE.DISCOUNT, formatCurrency(dis));
						}
					}
				}
			}
		}
		return lstResult;
	},
	
	setValueBuyProduct:function(buyProducts){
		if(buyProducts == null) return null;
		for(var i = 0 ; i < buyProducts.length ; i++){
			for(var j = 0 ; j < buyProducts[i].length ; j++){
				buyProducts[i][j].value = Number(buyProducts[i][j].valueStr);
			}
		}
	},
	
	/**
	 * convert accumulative promotion grid
	 * @author tungmt
	 * @date 15/12/2014
	 */
	convertAccumulative2Row: function(data,totalAmount) {
		var result = new Array();
		SPCreateOrder._lstCTTLMoneyQueue = new Array();
		SPCreateOrder._totalAcc = new Array();//reset lai tong tien KM tich luy
		var lst = data.accumulativePromotionItems;
		var totalAmount = Number($('#total').html().replace(/,/g,''));
		var totalAcc = 0;
		if(lst==null) return [];
		var rownum=0;
		SPCreateOrder._stockTmp = new Map();
		var lstDiscountAcc = new Array();
		for(var i=0 ; i < lst.length ; i++){
			var item = lst[i];
//			item.buyProducts = SPCreateOrder.setValueBuyProduct(item.buyProducts);
			if(item.promotionProducts == null && item.lstAmountPercent == null){//KM tien
				rownum++;
				var obj = jQuery.extend(true, {}, item);
//				obj.buyProducts = SPCreateOrder.increBuyProduct(obj.buyProducts);
				obj.rownum = rownum;
				obj.isMoney = true;
				obj.programCode = obj.promotionProgramCode;
				obj.productCode = 'Khuyến mãi tích lũy';
				obj.value = 0;
				obj.payingOrder = 1;
				var strConlai = '';
				if(obj.subGroupLevel != null){
					if(totalAcc < totalAmount){
						var k=0;
						var lstRationActualy = new Map();
						for(k = 0 ; k < obj.ratiton ; k++){
							if(totalAcc + obj.subGroupLevel.maxAmount > totalAmount){
								break;
							}else{
								totalAcc += obj.subGroupLevel.maxAmount;
								lstRationActualy.put(k,k);
							}
						}
						obj.rationActualy = lstRationActualy.keyArray;
						obj.value = k * obj.subGroupLevel.maxAmount;
						obj.shareDiscount = SPCreateOrder.shareDiscountSaleProduct(obj.value);
						SPCreateOrder._totalAcc.push(obj.shareDiscount);
						if(k > 0 && k < obj.ratiton){
							var conlai = (obj.ratiton - k ) * obj.subGroupLevel.maxAmount;
							strConlai = ' (còn lại ' + formatCurrency(conlai) + ')';
						}
					}else{
						obj.rationActualy = [];
						obj.value = 0;
					}
				}
				obj.valueText = formatCurrency(obj.value) + strConlai;
				if(obj.value > 0){
					result.push(obj);
				}else{
					rownum--;
					SPCreateOrder._lstCTTLMoneyQueue.push(obj);
				}
			}else if(item.lstAmountPercent != null){//KM %
				var amountPercent = 0;
				var m=0;
				var lstRationActualy = new Map();
				for(m = 0 ; m < item.lstAmountPercent.length ; m++){
					if(totalAcc + item.lstAmountPercent[m] > totalAmount){
						continue;
					}else{
						totalAcc += item.lstAmountPercent[m];
						amountPercent += item.lstAmountPercent[m];
						lstRationActualy.put(m,m);
					}
				}
				rownum++;
				var obj = jQuery.extend(true, {}, item);
//				obj.buyProducts = SPCreateOrder.increBuyProduct(obj.buyProducts);
				obj.rationActualy = lstRationActualy.keyArray;
				obj.isPercent = true;
				obj.rownum = rownum;
				obj.programCode = obj.promotionProgramCode;
				obj.productCode = 'Khuyến mãi tích lũy ';
				obj.value = amountPercent;
				obj.shareDiscount = SPCreateOrder.shareDiscountSaleProduct(obj.value);
				SPCreateOrder._totalAcc.push(obj.shareDiscount);
				obj.payingOrder = 1;
				var strConlai = '';
				if(lstRationActualy.size() > 0 && lstRationActualy.size() < item.lstAmountPercent.length){
					var conlai = 0;
					for(k = 0 ; k < item.lstAmountPercent.length ; k++){
						if(lstRationActualy.get(k) == null){
							conlai += item.lstAmountPercent[k];
						}
					}
					strConlai = ' (còn lại ' + formatCurrency(conlai) + ')';
				}
				obj.valueText = formatCurrency(obj.value) + strConlai;
				if(obj.value > 0){
					result.push(obj);
				}else{
					rownum--;
					SPCreateOrder._lstCTTLMoneyQueue.push(obj);
				}
			}else{//KM SP
				for(var m = 0 ; m < item.ratiton ; m++){
					rownum++;
					var titleRow = new Object();
					titleRow.rownum = rownum;
					titleRow.isHeader = true;
					titleRow.productCode = "Khuyến mãi tích lũy (lần " + (m + 1) + ")";
					result.push(titleRow);
					var indexDoiKM = null;
					for(var j = 0 ; j < item.promotionProducts.length ; j++){
						var obj = jQuery.extend(true, {}, item);
						obj.rownum = rownum;
						var product = item.promotionProducts[j];
						if(product.productId == null){
							obj.type = PromotionType.FREE_PRICE;
						}else{
							obj.type = PromotionType.FREE_PRODUCT;
						}
						obj.buyProducts = item.buyProducts[m];
						obj.productId = product.productId;
						obj.productCode = product.productCode;
						obj.productName = product.productName;
						obj.programCode = obj.promotionProgramCode;
						obj.valueType = product.valueType;
						obj.value = product.value;
						obj.convfact = product.convfact;
						obj.isRequired = product.isRequired;
						obj.orderNumber = product.orderNumber;
						obj.minQuantity = product.minQuantity;
						obj.maxQuantity = product.maxQuantity;
						obj.quantityReceived = obj.ratiton;
						obj.maxQuantityReceived = obj.value!=null?obj.value:obj.maxQuantity;
						obj.payingOrder = m+1;
						obj.lstSaleOrderLot = product.lstSaleOrderLot;
						if(obj.isRequired == 0) {
							if( indexDoiKM!=undefined && indexDoiKM!=null){
								if(result[indexDoiKM].lstDoiKM==undefined || result[indexDoiKM].lstDoiKM==null){
									result[indexDoiKM].lstDoiKM = new Array();
								}
								result[indexDoiKM].lstDoiKM.push(jQuery.extend(true, {}, obj));
								continue;
							}else{
								indexDoiKM = result.length;
								obj.lstDoiKM = new Array();
								obj.lstDoiKM.push(jQuery.extend(true, {}, obj));
							}
						}
						
						//tach kho
						var lstTemp = SPCreateOrder.splitWarehouseAccumulative(obj);
						result = result.concat(lstTemp);
					}
				}
			}
		}
		if(totalAcc != 0 && totalAcc <= totalAmount){
			totalAmount -= totalAcc;
			$('#total').html(formatCurrency(totalAmount));
//			SPCreateOrder._totalAcc = totalAcc;
//			SPCreateOrder.shareDiscountSaleProduct(totalAcc);
		}
		return result;
	},

	convertAccumulative2RowForEdit: function(data, totalAmount) {
		var result = new Array();
		var lst = data.accumulativePromotionItems;
		var totalAmount = Number($('#total').html().replace(/,/g,''));
		var totalAcc = 0;
		if (lst == null || lst.length == 0) {
			return [];
		}
		var rownum=0;
		SPCreateOrder._stockTmp = new Map();
		for (var i=0 ; i < lst.length ; i++) {
			var item = lst[i];
//			item.buyProducts = SPCreateOrder.setValueBuyProduct(item.buyProducts);
			if (item.promotionProducts == null && item.lstAmountPercent == null) {//KM tien
				rownum++;
				var obj = jQuery.extend(true, {}, item);
				obj.buyProducts = SPCreateOrder.increBuyProduct(obj.buyProducts);
				obj.rownum = rownum;
				obj.isMoney = true;
				obj.programCode = obj.promotionProgramCode;
				obj.productCode = 'Khuyến mãi tích lũy';
				obj.value = obj.amount;
				obj.shareDiscount = SPCreateOrder.shareDiscountSaleProduct(obj.value, true);
				SPCreateOrder._totalAcc.push(obj.shareDiscount);
				obj.payingOrder = 1;
				/*if(obj.subGroupLevel != null){
					obj.value = obj.ratiton * obj.subGroupLevel.maxAmount;
				}*/
				/*if (obj.debitAcc) {
					obj.value += obj.debitAcc;
				}*/
				/*if (totalAcc + obj.value > totalAmount) {
					obj.valueActually = totalAmount - totalAcc;
					$('#total').html('0');
				} else {*/
					obj.valueActually = obj.value;
				//}
				//totalAcc += obj.valueActually;
				/*if (obj.value != obj.valueActually) {
					obj.valueText = formatCurrency(obj.valueActually) + ' (còn lại '+ formatCurrency(obj.value - obj.valueActually) +')';
				} else {*/
					obj.valueText = formatCurrency(obj.value);
				//}
				result.push(obj);
			} else if (item.lstAmountPercent != null) {//KM %
				/*if (item.debitAcc != null) {
					item.lstAmountPercent[0] += item.debitAcc;
				}*/
				var amountPercent = 0;
				for(var m = 0 ; m < item.lstAmountPercent.length ; m++){
					amountPercent += item.lstAmountPercent[m];
				}
				rownum++;
				var obj = jQuery.extend(true, {}, item);
				obj.buyProducts = SPCreateOrder.increBuyProduct(obj.buyProducts);
				obj.isPercent = true;
				obj.rownum = rownum;
				obj.programCode = obj.promotionProgramCode;
				obj.productCode = 'Khuyến mãi tích lũy ';
				obj.value = amountPercent;
				obj.shareDiscount = SPCreateOrder.shareDiscountSaleProduct(obj.value, true);
				SPCreateOrder._totalAcc.push(obj.shareDiscount);
				obj.payingOrder = 1;
				/*if (totalAcc + obj.value > totalAmount) {
					obj.valueActually = totalAmount - totalAcc;
					$('#total').html('0');
				}else{*/
					obj.valueActually = obj.value;
				//}
				//totalAcc += obj.valueActually;
				/*if (obj.value != obj.valueActually) {
					obj.valueText = formatCurrency(obj.valueActually) + ' (còn lại '+ formatCurrency(obj.value - obj.valueActually) +')';
				}else{*/
					obj.valueText = formatCurrency(obj.value);
				//}
				result.push(obj);
				
			} else {//KM SP
				var lstDoiSPKM = [];
				if (item.lstPromoProducts instanceof Array && item.lstPromoProducts.length > 0) {
					for (var j = 0, szj = item.lstPromoProducts.length; j < szj; j++) {
						var obj = jQuery.extend(true, {}, item);
						var product = item.lstPromoProducts[j];
						if (product.productId == null) {
							obj.type = PromotionType.FREE_PRICE;
						} else {
							obj.type = PromotionType.FREE_PRODUCT;
						}
						//obj.buyProducts = item.buyProducts[m];
						obj.productId = product.productId;
						obj.productCode = product.productCode;
						obj.productName = product.productName;
						obj.programCode = obj.promotionProgramCode;
						obj.valueType = product.valueType;
						obj.value = product.value;
						obj.convfact = product.convfact;
						obj.isRequired = product.isRequired;
						obj.orderNumber = product.orderNumber;
						obj.minQuantity = product.minQuantity;
						obj.maxQuantity = product.maxQuantity;
						obj.quantityReceived = obj.ratiton;
						obj.maxQuantityReceived = product.value != null ? product.value : product.maxQuantity;
						if (obj.isRequired == 0) {
							lstDoiSPKM.push(obj);
						}
					}
				}
				for (var m = 0, szm = item.ratiton; m < szm; m++) {
					rownum++;
					var titleRow = new Object();
					titleRow.rownum = rownum;
					titleRow.isHeader = true;
					titleRow.productCode = "Khuyến mãi tích lũy (lần " + (m + 1) + ")";
					result.push(titleRow);
					var indexDoiKM = null;
					for (var j = 0, szj = item.promotionProducts.length; j < szj; j++) {
						var obj = jQuery.extend(true, {}, item);
						obj.rownum = rownum;
						var product = item.promotionProducts[j];
						if (product.payingOrder != m+1) {
							continue;
						}
						if (product.productId == null) {
							obj.type = PromotionType.FREE_PRICE;
						} else {
							obj.type = PromotionType.FREE_PRODUCT;
						}
						obj.buyProducts = item.buyProducts[m];
						obj.productId = product.productId;
						obj.productCode = product.productCode;
						obj.productName = product.productName;
						obj.programCode = obj.promotionProgramCode;
						obj.valueType = product.valueType;
						obj.value = product.valueActually;
						obj.convfact = product.convfact;
						obj.isRequired = product.isRequired;
						obj.orderNumber = product.orderNumber;
						obj.minQuantity = product.minQuantity;
						//obj.maxQuantity = product.maxQuantity;
						obj.maxQuantity = product.maxQuantityFree;
						obj.maxQuantityFreeTotal = product.maxQuantityFreeTotal; /** vuongmq; 08/01/2015; hien thi maxQuantityFreeTotal so luong tinh khuyen mai*/
						obj.quantityReceived = obj.ratiton;
						obj.maxQuantityReceived = obj.value != null ? obj.value : obj.maxQuantity;
						obj.payingOrder = m+1;
						obj.lstSaleOrderLot = product.lstSaleOrderLot;
						/*if (obj.isRequired == 0) {
							if (indexDoiKM != undefined && indexDoiKM != null) {
								if (result[indexDoiKM].lstDoiKM == undefined || result[indexDoiKM].lstDoiKM == null) {
									result[indexDoiKM].lstDoiKM = new Array();
								}
								var obj1 = jQuery.extend(true, {}, obj);
								obj1.value = product.value;
								result[indexDoiKM].lstDoiKM.push(obj1);
								//continue;
							} else {
								indexDoiKM = result.length;
								obj.lstDoiKM = new Array();
								var obj1 = jQuery.extend(true, {}, obj);
								obj1.value = product.value;
								obj.lstDoiKM.push(obj1);
							}
						}*/
						if (obj.isRequired == 0) {
							//obj.lstDoiKM = lstDoiSPKM;
							var lstDoiSPKM1 = [];
							for (var t = 0, szt = lstDoiSPKM.length; t < szt; t++) {
								var obj1 = $.extend(true, {}, lstDoiSPKM[t]);
								obj1.rownum = obj.rownum;
								obj1.buyProducts = obj.buyProducts;
								lstDoiSPKM1.push(obj1);
							}
							obj.lstDoiKM = lstDoiSPKM1;
						}
						
						// tach kho
						var lstTemp = null;
						if (obj.lstSaleOrderLot instanceof Array && obj.lstSaleOrderLot.length > 0) {
							lstTemp = SPCreateOrder.setWarehouseAccumulativeProduct(obj);
						} else {
							lstTemp = SPCreateOrder.splitWarehouseAccumulative(obj);
						}
						result = result.concat(lstTemp);
					}
				}
			}
		}
		/*if(totalAcc != 0 && totalAcc <= totalAmount){
			totalAmount -= totalAcc;
			$('#total').html(formatCurrency(totalAmount));
		}*/
		return result;
	},

	setWarehouseAccumulativeProduct: function(obj) {
		/*var url = window.location.href;
		if (url.indexOf("/commons/view-info") > -1) { // man hinh xem don hang
			var result = new Array();
			for (var i = 0, sz = obj.lstSaleOrderLot.length; i < sz; i++) {
				var wh = new Object();
				wh.warehouseName = obj.lstSaleOrderLot[i].warehouseName;
				wh.warehouseId = obj.lstSaleOrderLot[i].warehouseId;
				var obj1 = jQuery.extend(true, {}, obj);
				obj1.warehouse = wh;
				obj1.stock = obj.lstSaleOrderLot[i].availableQuantity;
				obj1.stockSource = obj1.stock;
				obj1.value = obj.lstSaleOrderLot[i].quantity;

				result.push(obj1);
				break;
			}
			return result;
		} else {*/
			var result = new Array();
			//var lstStock = SPCreateOrder._mapPRODUCTSbyWarehouseOnGrid.get(obj.productId);
			//if(lstStock==null){
//				lstStock =  SPCreateOrder._mapPRODUCTSbyWarehouse.get(obj.productId);
				lstStock =  GridSaleBusiness.getWarehouseByType.get(obj.productId);
			//}
			if (lstStock != null) {
				var stock = jQuery.extend(true, [], lstStock);
				//var quantity = obj.value;
				for (var i = 0, sz = obj.lstSaleOrderLot.length; i < sz; i++) {
					for (var k = 0 ; k < stock.length; k++) {
						if(stock[k].warehouseId == obj.lstSaleOrderLot[i].warehouseId) {
							var wh = new Object();
							wh.warehouseName = stock[k].warehouseName;
							wh.warehouseId = stock[k].warehouseId;
							var obj1 = jQuery.extend(true, {}, obj);
							obj1.warehouse = wh;
							obj1.stock = stock[k].availableQuantity;
							obj1.stockSource = obj1.stock;
							obj1.value = obj.lstSaleOrderLot[i].quantity;

							result.push(obj1);
							break;
						}
					}
				}
				//SPCreateOrder._mapPRODUCTSbyWarehouseOnGrid.put(obj.productId, stock);
			}
			return result;
		//}
	},
	
	/**
	 * tach kho KM tich luy
	 * @author tungmt
	 * @date 18/12/2014
	 */
	splitWarehouseAccumulative:function(obj){
		var result = new Array();
//		var lstStock = SPCreateOrder._mapPRODUCTSbyWarehouseOnGrid.get(obj.productId);
		var lstStock = GridSaleBusiness.getWarehouseOnGridByType(obj.productId);
//		if (lstStock == null) {
////			lstStock =  SPCreateOrder._mapPRODUCTSbyWarehouse.get(obj.productId);
//			lstStock =  GridSaleBusiness.getWarehouseByType(obj.productId);
//		}
		// --
//		var lstA = SPCreateOrder._mapPRODUCTSbyWarehouse.get(obj.productId);
		var lstA = GridSaleBusiness.getWarehouseByType(obj.productId);
		if(lstA != null){
			if (lstA instanceof Array) {
				for (var i = 0, sz = lstStock.length; i < sz; i++) {
					for (var j = 0, szj = lstA.length; j < szj; j++) {
						if (lstStock[i].warehouseId == lstA[j].warehouseId) {
							lstStock[i].stockSource = lstA[j].availableQuantity;
							break;
						}
					}
				}
			} else {
				for (var i = 0, sz = lstStock.length; i < sz; i++) {
					lstStock[i].stockSource = lstStock[i].availableQuantity;
				}
			}
		}
		// --
		if (lstStock != null) {
			var qtt = 0;
			var stock = jQuery.extend(true, [], lstStock);
			var quantity = obj.value;
			for (var k = 0; k < stock.length; k++) {
				if (stock[k].availableQuantity <= 0) {
					continue;
				}
				var wh = new Object();
				wh.warehouseName = stock[k].warehouseName;
				wh.warehouseId = stock[k].warehouseId;
				obj.warehouse = wh;
				qtt = SPCreateOrder._stockTmp.get(wh.warehouseId+"_"+obj.productId);
				if (qtt == null) {
					qtt = 0;
				}
				if (stock[k].availableQuantity >= quantity + qtt) {
					obj.stock = stock[k].availableQuantity;
					obj.stockSource = stock[k].stockSource;
					obj.value = quantity;
					//stock[k].availableQuantity = stock[k].availableQuantity - quantity;
					SPCreateOrder._stockTmp.put(wh.warehouseId+"_"+obj.productId, qtt+obj.value);
					result.push(obj);
					break;
				} else {
					var obj1 = jQuery.extend(true, {}, obj);
					obj1.stock = stock[k].availableQuantity;
					obj1.stockSource = stock[k].stockSource;
					obj1.value = stock[k].availableQuantity - qtt;
					if (obj1.value < 0) {
						obj1.value = 0;
					}
					quantity = quantity - stock[k].availableQuantity;
					SPCreateOrder._stockTmp.put(wh.warehouseId+"_"+obj.productId, qtt+obj1.value);
					//stock[k].availableQuantity = 0;												
					result.push(obj1);
				}
				if (quantity <= 0) {
					break;
				}
			}
			if (result.length == 0 && stock.length > 0) {
				var wh = new Object();
				wh.warehouseName = stock[0].warehouseName;
				wh.warehouseId = stock[0].warehouseId;
				obj.warehouse = wh;
				obj.stock = stock[0].availableQuantity;
				obj.stockSource = stock[0].stockSource;
				obj.value = quantity;
				result.push(obj);
			}
			//SPCreateOrder._mapPRODUCTSbyWarehouseOnGrid.put(obj.productId,stock);
			//kiem tra tong ton kho du? dap ung
			if(result.length > 0 && result[result.length-1].value < quantity){
				result[result.length-1].valueMax = quantity;
			}
		}else{
			var wh = new Object();
			wh.warehouseName = 'N/A';
			wh.warehouseId = -1;
			obj.warehouse = wh;
			obj.stock = 0;
			obj.stockSource = 0;
			obj.value = quantity;
			result.push(obj);
		}
		return result;
	},
	
	/**
	 * load accumulative promotion grid
	 * @author tungmt
	 * @date 15/12/2014
	 */
	loadGridAccumulative: function(data) {
		try{
			$("#h2KMTL").next().show();
			$("#h2KMTL").show();
			SPCreateOrder._lstCTTL = SPCreateOrder.convertAccumulative2Row(data);
			$('#accumulativePromotionGrid').datagrid('loadData',SPCreateOrder._lstCTTL);
			if (SPCreateOrder._lstCTTL == null || SPCreateOrder._lstCTTL.length == 0) {
				$("#h2KMTL").next().hide();
				$("#h2KMTL").hide();
			}
		} catch (e) {
			// pass through
			DEBUG ? console.log(e) : 1;
		}
	},
	loadGridAccumulativeForEdit: function(data) {
		try{
			$("#h2KMTL").next().show();
			$("#h2KMTL").show();
			SPCreateOrder._lstCTTL = SPCreateOrder.convertAccumulative2RowForEdit(data);
			$('#accumulativePromotionGrid').datagrid('loadData',SPCreateOrder._lstCTTL);
			if (SPCreateOrder._lstCTTL == null || SPCreateOrder._lstCTTL.length == 0) {
				$("#h2KMTL").next().hide();
				$("#h2KMTL").hide();
			}
		} catch (e) {
			// pass through
			DEBUG ? console.log(e) : 1;
		}
	},

	mergeGridAccumulative: function(){
		var lstRow = $('#accumulativePromotionGrid').datagrid('getRows');
		var index = 0;
		for(var i = 1 ; i < lstRow.length ; i++){
			if(lstRow[i].rownum != lstRow[i-1].rownum || i == lstRow.length - 1){
				if(i == lstRow.length - 1 && lstRow[i].rownum == lstRow[i-1].rownum){
					i = lstRow.length;
				}
				$('#accumulativePromotionGrid').datagrid('mergeCells',{  
		                index: index,  
		                field: 'rownum',  
		                rowspan: (i-index) 
		            });
				$('#accumulativePromotionGrid').datagrid('mergeCells',{  
		                index: index+1,
		                field: 'programCode',
		                rowspan: (i-index-1)
		            });
				$('#accumulativePromotionGrid').datagrid('mergeCells',{  
		                index: index,  
		                field: 'productCode',  
		                colspan: 4 
		            });
				index = i;
			}
		}
		if(i == lstRow.length && lstRow.length>1){// KM tien or % dong cuoi
			$('#accumulativePromotionGrid').datagrid('mergeCells',{  
	                index: index,  
	                field: 'productCode',  
	                colspan: 4 
	            });
		}
	},
	
	getAccumulative2Save:function(){
		try{
			if ($("#promotionGridContainer4 .datagrid-view").length == 0) {
				return new Map();
			}
			var rows = $('#accumulativePromotionGrid').datagrid('getRows');
			var lst = new Map();
			var mapProgramPayingOrder = new Map();
			for(var i = 0 ; i < rows.length ; i++){
				if(rows[i].isHeader == undefined || rows[i].isHeader == null){
					var obj = new Object();
					obj.value = rows[i].value;
					//kiem tra con ton kho o? san pham Doi KM khac
					if(rows[i].valueActualy <= 0 && rows[i].lstDoiKM != undefined && rows[i].lstDoiKM != null && rows[i].lstDoiKM.length > 0){
						for(var j = 0 ; j < rows[i].lstDoiKM.length ; j++){
//							var lstStock = SPCreateOrder._mapPRODUCTSbyWarehouseOnGrid.get(rows[i].lstDoiKM[j].productId);
							var lstStock = GridSaleBusiness.getWarehouseOnGridByType(rows[i].lstDoiKM[j].productId);
//							if (lstStock == null) {
//								lstStock = GridSaleBusiness.getWarehouseByType(rows[i].lstDoiKM[j].productId);
//							}
							if (lstStock != null) {
								for (var k = 0; k < lstStock.length; k++) {
									if (lstStock[k].availableQuantity > 0) {
										var code = rows[i].productCode;// rows[i].lstDoiKM[j].productCode;
										$('#serverErrors').html('Sản phẩm ' + Utils.XSSEncode(code) + ' có thể đổi SPKM khác còn tồn kho.').show();
										return -1;
									}
								}
							}
						}
					}
					//Kiem tra sản phẩm còn tồn kho ở kho khác
					if(rows[i].valueActualy < rows[i].value && rows[i].warehouse != null){
						var valueActualy = 0 ;//gia tri tong cua sp KM trong 1 lan dat
						for(var k = 0 ; k < rows.length ; k++){
							if(rows[k].productId == rows[i].productId && rows[k].rownum == rows[i].rownum){
								valueActualy += Number(rows[k].valueActualy) ;
							}
						}
						var lstStock = GridSaleBusiness.getWarehouseOnGridByType(rows[i].productId);
//						if (lstStock == null) {
//							lstStock = GridSaleBusiness.getWarehouseByType(rows[i].productId);
//						}
						var totalStock = 0;
						if (lstStock != null) {
							for (var k = 0; k < lstStock.length; k++) {
								totalStock += lstStock[k].availableQuantity;
							}
						}
						if(valueActualy < totalStock){
							var code = rows[i].productCode;// rows[i].lstDoiKM[j].productCode;
							$('#serverErrors').html('Sản phẩm ' + Utils.XSSEncode(code) + ' có thể chọn thêm số lượng ở kho khác.').show();
							return -1;
						}
					}
					
					if(rows[i].valueActualy != undefined && rows[i].valueActualy != null){
						obj.valueActualy = rows[i].valueActualy;
					}
					if(obj.value == 0) continue;
					if(rows[i].stock != undefined && rows[i].stock != null && rows[i].stock < rows[i].value){//KM SP
						if(rows[i].stock <= 0) obj.value = 0; 
						else obj.value = rows[i].stock;
					}
					obj.programCode = rows[i].programCode;
					obj.maxQuantity = rows[i].maxQuantity;
					obj.maxQuantityFree = rows[i].maxQuantity;
					if(rows[i].subGroupLevel!=null){
						obj.groupLevelId = rows[i].subGroupLevel.parentLevel.id;
						obj.productGroupId = rows[i].subGroupLevel.productGroup.id;
					}
					if(rows[i].maxQuantityReceived!= undefined && rows[i].maxQuantityReceived!=null){
						obj.maxQuantity = rows[i].maxQuantityReceived;
					}
					if(!rows[i].isMoney && !rows[i].isPercent){//KM SP
						obj.warehouseId = rows[i].warehouse.warehouseId;
						obj.valueType=1;
					}else if(rows[i].isMoney){// KM tien
						obj.valueType=2;
					}else{// KM %
						obj.valueType=3;
						obj.percent = rows[i].subGroupLevel.percent;
					}
					obj.productId = rows[i].productId;
					obj.payingOrder = 1;//rows[i].payingOrder;
					if(rows[i].shareDiscount != undefined && rows[i].shareDiscount != null){
						var lstShareDiscount = new Map();
						for(var k = 0 ; k < rows[i].shareDiscount.length ; k++){
							var dis = lstShareDiscount.get(rows[i].shareDiscount.productCode);
							if(dis == null){
								dis = new Object();
								dis.value = 0;
								dis.productCode = rows[i].shareDiscount.productCode;
							}
							dis.value += rows[i].shareDiscount.value;
							lstShareDiscount.put(rows[i].shareDiscount.productCode,dis);
						}
						obj.shareDiscount = rows[i].shareDiscount;
					}
					if(rows[i].rationActualy != undefined && rows[i].rationActualy !=null){// KM tien, %
						var lstActualy = new Array();
						for(var k = 0 ; k < rows[i].rationActualy.length ; k++){
							lstActualy.push(rows[i].buyProducts[rows[i].rationActualy[k]]);
						}
						obj.promotion = rows[i].rationActualy.length;
						obj.buyProducts = SPCreateOrder.increBuyProduct(lstActualy);
					}else{// KM SP
						obj.buyProducts = rows[i].buyProducts;
					}
					if (mapProgramPayingOrder.get(obj.programCode) != null) {
						var abc = mapProgramPayingOrder.get(obj.programCode);
						if (rows[i].rownum == abc.rownum) {
							obj.payingOrder = abc.payingOrder;
						} else {
							abc.rownum = rows[i].rownum;
							obj.payingOrder = (++abc.payingOrder);
						}
					} else {
						mapProgramPayingOrder.put(obj.programCode, {rownum: rows[i].rownum, payingOrder: obj.payingOrder});
					}
					if(lst.get(rows[i].rownum)!=null){
						var objTemp = lst.get(rows[i].rownum);
						objTemp.promoProducts.push(obj);
						lst.put(rows[i].rownum,objTemp);
					}else{
						obj.promoProducts = new Array();
						obj.promoProducts.push( jQuery.extend(true, {}, obj));
						lst.put(rows[i].rownum,obj);
					}
				}
			}
			return lst;
		} catch (e) {
			// pass through
		}
	},
	
	/**
	 * create accumulative promotion grid
	 * @author tuannd20
	 * @return {[type]} [description]
	 * @date 12/12/2014
	 */
	createAccumulativePromotionGrid: function() {
		$('#accumulativePromotionGrid').datagrid({
			columns: [[
			    {field: 'rownum', title: 'STT', width: 40, resizable: false, sortable: false, align: 'center'
				},
			    {field: 'productCode', title: 'Mã sản phẩm', width: 70, resizable:false, sortable:false, align:'left', 
			    		formatter: function(v,r,i){
			    			if(r.isHeader || r.isMoney || r.isPercent){
			    				return Utils.XSSEncode(r.productCode);
			    			}else{
			    				return PromotionProductFmt.productCodeFmt(v,r,i);
			    			}
			    		}
				},
				{field: 'productName', title: 'Tên sản phẩm', align: 'left', resizable:false, sortable:false, width: 150,
					formatter: PromotionProductFmt.productFmt
				},
				{field: 'warehouse', title: 'Kho', align: 'center', resizable: false, sortable: false, width:110, 
					formatter:function(v,r,i){
						if (r.warehouse != null) {
							return '<a class="delIcons" onclick="return SPCreateOrder.openSelectProductWarehouseDialogCTTL(\''+r.productCode+'\',\''+i+'\', SPCreateOrder.productWarehouseType.PROMOTION_PRODUCT,'+r.rownum+');" href="javascript:void(0)">'+Utils.XSSEncode(r.warehouse.warehouseName)+'</a>';
						}
					}
				},
				{field: 'stock', title:'Tồn kho đáp ứng', align: 'right', resizable: false, sortable: false, width: 70,
					//formatter: PromotionProductFmt.stockFmt
					formatter: function(v, r, i) {
						if (r.stockSource == undefined || r.stockSource == null) {
							r.stockSource = 0;
						}
						return '<span class="tlStyle'+i+'" id="tl_stock_'+r.productId+'_'+i+'">'+formatQuantityEx(r.stockSource,r.convfact)+'</span>';
					}
				},
				{field: 'quantity', title: 'Tổng', align: 'right', resizable: false, sortable: false, width: 80, 
					formatter:function(v,r,index){
						if(r.isMoney || r.isPercent){//KM Tien
							return '<span class="ppStyle'+index+'">'+Utils.XSSEncode(r.valueText)+'</span>';
						}else{
							if(r.isHeader==undefined || r.isHeader==null){
								var strInfo='';
								var value = r.value;
								if(r.stock < r.value){
									strInfo = ' <span style="color:red;">('+r.value+')</span>';
									if(r.stock < 0) value=0;
									else value=r.stock;
								}
								if(r.valueMax != undefined && r.valueMax != null){
									strInfo = ' <span style="color:red;">('+Utils.XSSEncode(r.valueMax)+')</span>';
								}
								r.valueActualy = value;
								return '<span class="ppStyle'+index+'">'+Utils.XSSEncode(formatQuantityEx(value,r.convfact))+ strInfo +'</span>';
							}
						}
					}
				},
				{field: 'programCode', title: 'Khuyến mãi', align: 'center', resizable: false, sortable: false, width:110, 
					formatter:PromotionProductFmt.programCodeFmt
				},
				{field: 'key', title: 'Đổi KM', align: 'center', resizable: false, sortable: false, width: 80, 
					formatter:function(v,r,i){
						if(r.isHeader){
							return '<a href="javascript:void(0)" onclick="return SPCreateOrder.deleteAccumulativePromotion('+i+');" id="btnRemoveProduct_0" class="delIcons"><img width="19" height="20" src="/resources/images/icon-delete.png"></a>';
						}else if(r.lstDoiKM!=undefined && r.lstDoiKM!=null && r.lstDoiKM.length>1){
							return '<a href="javascript:void(0)" onclick="return SPCreateOrder.promotionProductDialogCTTL('+r.rownum+','+r.productId+',\''+Utils.XSSEncode(r.programCode)+'\');" id="btnRemoveProduct_0" class="delIcons">Đổi SPKM</a>';
						}
					}
				}
			]],	
			showFooter: true,
			singleSelect: true,

			width: $(window).width() - 40,
			fitColumns: true,		
			scrollbarSize: 0,
			autoRowHeight: true,
			rowStyler: function(i,r){
				if(!r.isMoney && !r.isPercent && (r.isHeader==undefined || r.isHeader==null)){//KM Tien
					if(r.stock < r.value || (r.valueMax != undefined && r.valueMax != null)){
						return 'color:'+WEB_COLOR.ORANGE_CODE+';'; 
					}
				}
				return '';
			},
			onLoadSuccess: function(data){
				SPCreateOrder.mergeGridAccumulative();
			}
		});
		setTimeout(function() {
			$("#h2KMTL").next().hide();
			$("#h2KMTL").hide();
		},750);
	},
	
	/**
	 * Xoa 1 lan dat khuyen mai
	 */
	deleteAccumulativePromotion:function(index){
		var rows = $('#accumulativePromotionGrid').datagrid('getRows');
		var rownum = rows[index].rownum;
		//
		/*var programCode = null;
		var payingOrder = null;*/
		for(var i = 0 ; i < rows.length ; i++){
			if(rows[i].rownum == rownum){
				$('#accumulativePromotionGrid').datagrid('deleteRow', i);
				/*if (programCode == null && !isNullOrEmpty(rows[i].promotionProgramCode)) {
					programCode = rows[i].promotionProgramCode;
					payingOrder = rows[i].payingOrder;
				}*/
				i--;
			}
		}
		for(var i = index ; i < rows.length ; i++){
			rows[i].rownum--;
			/*if (rows[i].lstDoiKM instanceof Array) {
				for (var j = 0, szj = rows[i].lstDoiKM.length; j < szj; j++) {
					rows[i].lstDoiKM[j].rownum = rows[i].rownum;
				}
			}
			if (programCode && rows[i].promotionProgramCode == programCode
				&& rows[i].payingOrder > payingOrder) { // lacnv1 - cap nhat lai payingOrder de luu xuong csdl
				rows[i].payingOrder--;
			}*/
		}
		$('#accumulativePromotionGrid').datagrid('loadData',rows);
	},
	
	/**
	 * Ham hien thi dialog doi~SPKM ap dung cho chon mot SPKM 
	 * @params result 
	 * @callback
	 */
	promotionProductDialogCTTL:function(rownum , productId, programCode){
		var rows = $('#accumulativePromotionGrid').datagrid('getRows');
		var  lstDoiSPKM = new Array();
		for(var i = 0 ; i < rows.length ; i++){
			if(rows[i].rownum == rownum && rows[i].productId == productId && rows[i].programCode == programCode){
				lstDoiSPKM = jQuery.extend(true, [], rows[i].lstDoiKM);
				break;
			}
		}
		var isMultiChoose = true;
		if(lstDoiSPKM!=null && lstDoiSPKM.length>0){
			for(var i = 1 ; i < lstDoiSPKM.length ; i++){
				if(lstDoiSPKM[0].maxQuantityReceived != lstDoiSPKM[i].maxQuantityReceived){
					isMultiChoose = false;
					break;
				}
			}
		}
		var html = $('#searchStyle5').html();
		$('#searchStyle5').dialog({
			closed: false,  
	        cache: false,  
	        modal: true, 
	        onOpen: function() {
	        	if(!isMultiChoose){
		        	$('.easyui-dialog#searchStyle5 #listPromotionProduct').datagrid({
		        		data:lstDoiSPKM,
		        		rownumbers:true,
		        		singleSelect: true,	
		        		scrollbarSize:0,
		        		width:$('#searchStyle5').width() - 15,
		        		columns:[[  
		        		   {field:'productCode',title:'Mã sản phẩm', align:'left',width:120, sortable:false, resizable:false},  
		        		   {field:'productName',title:'Tên sản phẩm', align:'left',width:250, sortable:false, resizable:false},
		        		   {field:'quantity',title:'Số lượng',width:100,align:'right', sortable:false, resizable:false,
		        			   formatter:function(v,r,i){
		        				   return formatQuantityEx(r.value,r.convfact);
		        			   }},
		        		   {field:'ctkm',title:'Chọn',width:80,align:'center', sortable:false, resizable:false, 
		        			   formatter:function(v,r,i) {
		        				   return "<a href='javascript:void(0)' onclick=\"return SPCreateOrder.selectProductPromotionCTTL("+ rownum +","+productId+",'"+Utils.XSSEncode(programCode)+"',"+r.productId+");\">Chọn</a>";
		        		   }},
		        		]],
		        		onLoadSuccess:function(){
		        			$('.easyui-dialog .datagrid-header-rownumber').html('STT');
		        		}
		        	});
	        	}else{
	        		var maxQuantityMutilChoose = lstDoiSPKM[0].value;
	        		for(var i = 0 ; i < lstDoiSPKM.length ; i++){
        				var lstStock = GridSaleBusiness.getWarehouseOnGridByType(lstDoiSPKM[i].productId);
//						if (lstStock == null) {
//							lstStock = GridSaleBusiness.getWarehouseByType(lstDoiSPKM[i].productId);
//						}
        				// --
//	      				var lstA = SPCreateOrder._mapPRODUCTSbyWarehouse.get(lstDoiSPKM[i].productId);
						var lstA = GridSaleBusiness.getWarehouseByType(lstDoiSPKM[i].productId);
						if (lstA instanceof Array) {
							for (var ij = 0, szi = lstStock.length; ij < szi; ij++) {
								for (var j = 0, szj = lstA.length; j < szj; j++) {
									if (lstStock[ij].warehouseId == lstA[j].warehouseId) {
										lstStock[ij].stockSource = lstA[j].availableQuantity;
										break;
									}
								}
							}
						} else {
							for (var ij = 0, szi = lstStock.length; ij< szi; ij++) {
								lstStock[ij].stockSource = lstStock[ij].availableQuantity;
							}
						}
						// --
        				if(lstStock!=null && lstStock.length>0){
        					lstDoiSPKM[i].warehouse = lstStock[0];
        				}
        				lstDoiSPKM[i].maxQuantity = maxQuantityMutilChoose;
	        		}
	        		$('.easyui-dialog#searchStyle5 #listPromotionProduct').datagrid({
						data:lstDoiSPKM,
		        		rownumbers:true,
		        		scrollbarSize:0,
		        		fitColumns:true,
		        		singleSelect: true,
		        		width:$('#searchStyle5').width() - 15,
		        		columns:[[  
	   	        		   {field:'productCode',title:'Mã sản phẩm', align:'left',width:90, sortable:false, resizable:false},  
	   	        		   {field:'productName',title:'Tên sản phẩm', align:'left',width:210, sortable:false, resizable:false},
	   	        		   {field:'stock', title:'Kho', align:'left',width:100,sortable:false,resizable:false,
	   	        			   formatter:function(v,r,i){
	   	        					if(r.warehouse!=null){
		   	        					return Utils.XSSEncode(r.warehouse.warehouseName);
		   	        				}
	   	        			   }
	   	        		   },
	   	        		   {field:'availableQuantity', title:'Tồn kho', align:'right',width:100,sortable:false,resizable:false,
	   	        			   formatter:function(v,r,i){
		   	        				if(r.warehouse!=null){
		   	        					//return formatQuantityEx(r.warehouse.availableQuantity,r.warehouse.convfact);
		   	        					return formatQuantityEx(r.warehouse.stockSource,r.warehouse.convfact);
		   	        				}
	   	        			   }
	   	        		   },
	   	        		   {field:'quantity',title:'Số lượng',width:100,sortable:false, resizable:false,
	   	        			   formatter:function(v,r,i){
	   	        				   var id = 'multi_change_product_quantity_' + r.productId + '_' + i;
	   	        				   var valueActualy = SPCreateOrder.getValueActualyForGridMulti(r,rownum);
	   	        				   if(valueActualy == undefined || valueActualy == null) {
	   	        					   valueActualy=0;
	   	        				   }
   	        					   if(i == 0) {
   	        						   return '<input value="' + formatQuantityEx(valueActualy,r.convfact) + '" id="' + id + '" index="'+i+'" class="Multi_Change_Product_Quantity Multi_Change_Product_Quantity_Begin" onkeypress="" onblur="" style="text-align: right; width: 50px;"/><span class="ppStyle0" style="color: rgb(255, 0, 0);float:right;">('+formatQuantityEx(r.value,r.convfact)+')</span>';
   	   	        				   } else {
   	   	        					   return '<input value="' + formatQuantityEx(valueActualy,r.convfact) + '" id="' + id + '" index="'+i+'" class="Multi_Change_Product_Quantity" onkeypress="" onblur="" style="text-align: right; width: 50px;"/><span class="ppStyle0" style="color: rgb(255, 0, 0);float:right;">('+formatQuantityEx(r.value,r.convfact)+')</span>';
   	   	        				   }
	   	        			   }
	   	        		   }
	   	        		]],
	   	        		onLoadSuccess:function(){
	   	        			$('.easyui-dialog .datagrid-header-rownumber').html('STT');
	   	        			$('.Multi_Change_Product_Quantity_Begin').focus();
	   	        		}
					});
	        		$('.Multi_Change_Product_Quantity').live('blur',function() {
						var val = $(this).val();
						var index = $(this).attr('index');
						var convfact = $('.easyui-dialog#searchStyle5 #listPromotionProduct').datagrid('getRows')[index].convfact;
						$(this).val(formatQuantityEx(val,convfact));
					});
					$('#btnMultiChoose').show();
					$('#btnMultiChoose').bind('click', function() {
						SPCreateOrder.selectMultiProductPromotionCTTL(rownum,programCode,maxQuantityMutilChoose);
					});
	        	}
	        	setTimeout(function(){
       				CommonSearchEasyUI.fitEasyDialog("searchStyle5");
       			}, 100);
	        },
	        onClose : function(){
	        	$('#searchStyle5').html(html);
	        }
		});
	},
	
	getValueActualyForGridMulti:function(r,rownum){
		var rows = $('#accumulativePromotionGrid').datagrid('getRows');
		for(var i = 0 ; i < rows.length ; i++){
			if(rows[i].productId != undefined && rows[i].productId != null && rows[i].productId == r.productId
				&& rows[i].rownum == rownum && rows[i].warehouse != undefined && rows[i].warehouse != null 
				&& r.warehouse != null && rows[i].warehouse.warehouseId == r.warehouse.warehouseId){
				return rows[i].valueActualy;
			}
		}
		return 0;
	},
	
	selectMultiProductPromotionCTTL:function(rownum , programCode, totalMaxQuantity){
		$('#errMultiChooseProduct').hide();
		var rowSelect = $('.easyui-dialog#searchStyle5 #listPromotionProduct').datagrid('getRows');
		var listResult = new Array();
		var totalQuantity = 0;
		var __myTime = (new Date()).getTime();
		var totalStock = 0;
		for(var i = 0; i < rowSelect.length; i++) {
			var rSel = rowSelect[i];
			var quantity = StockValidateInput.getQuantity($('#multi_change_product_quantity_' + rSel.productId + '_' + i).val(), rSel.convfact);
			if(rSel.warehouse != null){
				totalStock += rSel.warehouse.availableQuantity;
			}
			if(isNullOrEmpty(quantity) || quantity == undefined || isNaN(quantity)) {
				$('#multi_change_product_quantity_'+rSel.productId+'_'+i).focus();
				$('#errMultiChooseProduct').html('Số lượng của sản phẩm ' + Utils.XSSEncode(rSel.productCode)+ 'không hợp lệ. Vui lòng nhập lại').show();
				return;
			}
			if(rSel.warehouse != undefined && Number(quantity) > Number(rSel.warehouse.availableQuantity)) {
				$('#multi_change_product_quantity_'+rSel.productId+'_'+i).focus();
				$('#errMultiChooseProduct').html('Số lượng tồn kho đáp ứng của sản phẩm '+ Utils.XSSEncode(rSel.productCode)+' không đủ. Vui lòng nhập lại').show();
				return;
			}
			totalQuantity += quantity;
			if (quantity > 0) {
				if(rSel.warehouse == undefined || rSel.warehouse == null){
					$('#multi_change_product_quantity_'+rSel.productId+'_'+i).val('').focus();
					$('#errMultiChooseProduct').html('Sản phẩm '+ Utils.XSSEncode(rSel.productCode)+' không có tồn kho. Vui lòng nhập lại').show();
					return;
				}
				rSel.value = quantity;
				rSel.stock = rSel.warehouse.availableQuantity;
				rSel.maxQuantityReceived = quantity;
				listResult.push(rSel);
			} 
		}
		if(listResult != null && listResult.length > 0){//tinh tong sl can KM cho SP chọn cuối cùng
			var totalActualyReceive = 0;
			for(var i = 0 ; i < listResult.length - 1 ; i++){
				totalActualyReceive += listResult[i].value;
			}
			if(totalActualyReceive < listResult[listResult.length-1].maxQuantity){
				listResult[listResult.length-1].value = listResult[listResult.length-1].maxQuantity - totalActualyReceive;
			}
		}
		if (listResult.length == 0) {
			$('#errMultiChooseProduct').html('Vui lòng chọn sản phẩm đổi khuyến mãi.').show();
			setTimeout(function(){
				$('#errMultiChooseProduct').hide();
			}, 3000);
			return;
		}
		if(totalStock < totalMaxQuantity){
			if(totalQuantity != totalStock){
				$('#errMultiChooseProduct').html('Tổng số lượng của các sản phẩm KM phải bằng tồng tồn kho còn lại (' + totalStock + ')').show();
				return;
			}
		}else{
			if ( totalQuantity != totalMaxQuantity) {
				if (totalQuantity > totalMaxQuantity) {
					$('#errMultiChooseProduct').html('Số lượng vượt quá số lượng khuyến mãi tối đa. Vui lòng nhập lại').show();
					return;
				}else{
					$('#errMultiChooseProduct').html('Tổng số lượng của các sản phẩm KM phải bằng (' + totalMaxQuantity + ')').show();
					return;
				}
			}
		}
		
		//reload grid
		var rows = $('#accumulativePromotionGrid').datagrid('getRows');
		var indexStart = null;
		for(var i = 0 ; i < rows.length ; i++){
			if(rows[i].rownum == rownum && rows[i].isRequired == 0 && rows[i].programCode == programCode){
				if(indexStart == null){
					indexStart = i;
					if(rows[i].lstDoiKM!=undefined && rows[i].lstDoiKM!=null){
						for(var k = 0 ; k < listResult.length ; k++){
							listResult[k].rownum = rownum;
							listResult[k].lstDoiKM = jQuery.extend(true, [], rows[i].lstDoiKM);
							if (listResult[k].lstDoiKM instanceof Array) {
								for (var j = 0, szj = listResult[k].lstDoiKM.length; j < szj; j++) {
									listResult[k].lstDoiKM[j].rownum = rownum;
								}
							}
							var lstStock = GridSaleBusiness.getWarehouseOnGridByType(listResult[k].productId);
//							if (lstStock == null) {
//								lstStock = GridSaleBusiness.getWarehouseByType(listResult[k].productId);
//							}
							// --
//		      				var lstA = SPCreateOrder._mapPRODUCTSbyWarehouse.get(listResult[k].productId);
		      				var lstA = GridSaleBusiness.getWarehouseByType(listResult[k].productId);
							if (lstA instanceof Array) {
								for (var j = 0, szj = lstA.length; j < szj; j++) {
									for (var ij = 0, szi = lstStock.length; ij < szi; ij++) {
										if (lstStock[ij].warehouseId == lstA[j].warehouseId) {
											lstStock[ij].stockSource = lstA[j].availableQuantity;
											break;
										}
									}
									if (listResult[k].warehouse.warehouseId == lstA[j].warehouseId) {
										listResult[k].stockSource = lstA[j].availableQuantity;
									}
								}
							} else {
								for (var ij = 0, szi = lstStock.length; ij< szi; ij++) {
									lstStock[ij].stockSource = lstStock[ij].availableQuantity;
									if (listResult[k].warehouse.warehouseId == lstStock[ij].warehouseId) {
										listResult[k].stockSource = lstStock[ij].availableQuantity;
									}
								}
							}
							// --
							SPCreateOrder._mapPRODUCTSbyWarehouseOnGrid.put(listResult[k].productId,lstStock);
						}
					}
				}
				rows.splice(i,1);
				i--;
			}
		}
		var flag = true;
		var newRows = new Array();
		for(var i = 0 ; i < rows.length ; i++){
			if(i >= indexStart && flag){
				newRows = newRows.concat(listResult);
				flag = false;
			}
			newRows.push(rows[i]);
		}
		if(flag){
			newRows = newRows.concat(listResult);
		}
		$('#accumulativePromotionGrid').datagrid('loadData',newRows);
		$('.easyui-dialog').dialog('close');
		SPCreateOrder.orderChanged = true;
	},
	
	selectProductPromotionCTTL: function(rownum , productId, programCode,productIdChoose){
		var rows = $('#accumulativePromotionGrid').datagrid('getRows');
		var lstTemp = [];
		var indexStart = null;
		for(var i = 0 ; i < rows.length ; i++){
			if(rows[i].rownum == rownum && rows[i].productId == productId && rows[i].programCode == programCode){
				if(lstTemp.length==0){
					indexStart = i;
					if(rows[i].lstDoiKM!=undefined && rows[i].lstDoiKM!=null){
						for(var k = 0 ; k < rows[i].lstDoiKM.length ; k++){
							if(rows[i].lstDoiKM[k].productId == productIdChoose){
								lstTemp = SPCreateOrder.splitWarehouseAccumulative(jQuery.extend(true, {}, rows[i].lstDoiKM[k]));
								if(lstTemp!=null && lstTemp.length>0){
									for(var m = 0 ; m < lstTemp.length ; m++){
										lstTemp[m].rownum = rownum;
										lstTemp[m].lstDoiKM = jQuery.extend(true, [], rows[i].lstDoiKM);
										if (lstTemp[m].lstDoiKM instanceof Array) {
											for (var j = 0, szj = lstTemp[m].lstDoiKM.length; j < szj; j++) {
												lstTemp[m].lstDoiKM[j].rownum = rownum;
											}
										}
									}
								}
							}
						}
					}
				}
				rows.splice(i,1);
				i--;
			}
		}
		var flag = true;
		var newRows = new Array();
		for(var i = 0 ; i < rows.length ; i++){
			if(i >= indexStart && flag){
				newRows = newRows.concat(lstTemp);
				flag = false;
			}
			newRows.push(rows[i]);
		}
		if(flag){
			newRows = newRows.concat(lstTemp);
		}
		$('#accumulativePromotionGrid').datagrid('loadData',newRows);
		$('.easyui-dialog').dialog('close');
		SPCreateOrder.orderChanged = true;
	},
	
	selectProductByWarehouseCTTL: function(productId,rownum,valueActualy,value) {
		flag = true;
		var index = $('#indexTmpToAddRow').val();
		var mapWarehouseQuantity = new Map();
		var defaultWarehouseObj = null;
		var totalQuantity = 0;	// use to check whether total quantity exceed max promotion quota
		var showErrMsgFunc = function(msg) {
			$('.easyui-dialog #GeneralInforErrors').html(msg).show();
			setTimeout(function() {
				$('.easyui-dialog #GeneralInforErrors').hide();
			}, 3000);
		}
		$('.easyui-dialog .clsProductVOQuantity').each(function(){
			var obj = $(this);
			var data = {};			
			var rowId = obj.attr('id').split('_')[1];
			var warehouseId = obj.attr('warehouseId');
			var convfact = $("#productWarehouseGrids").datagrid('getRows')[rowId]['convfact'];
			var availableQuantity = $("#productWarehouseGrids").datagrid('getRows')[rowId]['availableQuantity'];
			var stock = $("#productWarehouseGrids").datagrid('getRows')[rowId]['availableQuantity'];
			var warehouseName = $("#productWarehouseGrids").datagrid('getRows')[rowId]['warehouseName'];
			if(obj.val().length>0){				 
				var quantity = obj.val();
				data.warehouseId = warehouseId;
				data.warehouseName = warehouseName;
				data.convfact =convfact;
				data.convfact_value = quantity;
				data.quantity = StockValidateInput.getQuantity(quantity,data.convfact);
				data.availableQuantity = availableQuantity;
				data.stock = stock;
				data.warehouse = $("#productWarehouseGrids").datagrid('getRows')[rowId];
				totalQuantity += Number(data.quantity);
				if(isNaN(data.quantity) || Number(data.quantity)<0){
					flag = false;index = rowId;
					var errMsg = 'Giá trị số lượng nhập vào kho '+ Utils.XSSEncode(warehouseName) +' không hợp lệ';
					if (Number(data.quantity) < 0) {
						errMsg = 'Giá trị số lượng phải là số nguyên dương.';
					}
					$('.easyui-dialog #GeneralInforErrors').html(errMsg).show();

					return false;
				} else if (data.quantity > data.availableQuantity) {
					flag = false;index = rowId;
					$('.easyui-dialog #GeneralInforErrors').html('Giá trị số lượng nhập vào kho '+ Utils.XSSEncode(warehouseName) +' lớn hơn tồn kho đáp ứng.').show();
					return false;
				}else if (totalQuantity > valueActualy){
					flag = false;index = rowId;
					$('.easyui-dialog #GeneralInforErrors').html('Tổng giá trị nhập vào lớn hơn số lượng khuyến mãi '+valueActualy).show();
					return false;
				}
				if (data.quantity > 0) {
					mapWarehouseQuantity.put(data.warehouseId,data);
				}
			}
		});
		if(mapWarehouseQuantity != null && mapWarehouseQuantity.size() > 0){//tinh tong sl can KM cho SP chọn cuối cùng
			var totalActualyReceive = 0;
			for(var i = 0 ; i < mapWarehouseQuantity.valArray.length - 1 ; i++){
				totalActualyReceive += mapWarehouseQuantity.valArray[i].convfact_value;
			}
			if(totalActualyReceive < value){
				mapWarehouseQuantity.valArray[mapWarehouseQuantity.size()-1].convfact_value = value - totalActualyReceive;
			}
		}
		if (totalQuantity != valueActualy){
			flag = false;
			$('.easyui-dialog #GeneralInforErrors').html('Tổng giá trị nhập vào phải bằng số lượng khuyến mãi '+valueActualy).show();
		}
		if(!flag){
			return;
		}
		var rows = $('#accumulativePromotionGrid').datagrid('getRows');
		var newRows = new Array();
		var rowCurrent = null;
		var indexStart = 0;
		for(var i = 0 ; i < rows.length ; i++){
			if(rows[i].rownum == rownum && rows[i].productId == productId){
				if(rowCurrent==null){
					indexStart = i;
					rowCurrent = rows[i];
				}
				rows.splice(i,1);
				i--;
			}
		}
		var flag = true;
		for(var i = 0 ; i < rows.length ; i++){
			if(i >= indexStart && flag){
				for(var k = 0 ; k < mapWarehouseQuantity.size() ; k++){
					var obj = jQuery.extend(true, {}, rowCurrent);
					obj.warehouse.warehouseName = mapWarehouseQuantity.valArray[k].warehouseName;
					obj.warehouse.warehouseId = mapWarehouseQuantity.valArray[k].warehouseId;
					obj.value = mapWarehouseQuantity.valArray[k].convfact_value;
					obj.stock = mapWarehouseQuantity.valArray[k].availableQuantity;
					newRows.push(obj);
				}
				flag = false;
			}
			newRows.push(rows[i]);
		}
		if(flag){
			for(var k = 0 ; k < mapWarehouseQuantity.size() ; k++){
				var obj = jQuery.extend(true, {}, rowCurrent);
				obj.warehouse.warehouseName = mapWarehouseQuantity.valArray[k].warehouseName;
				obj.warehouse.warehouseId = mapWarehouseQuantity.valArray[k].warehouseId;
				obj.value = mapWarehouseQuantity.valArray[k].convfact_value;
				obj.stock = mapWarehouseQuantity.valArray[k].availableQuantity;
				newRows.push(obj);
			}
		}
		$('#accumulativePromotionGrid').datagrid('loadData',newRows);
		$('.easyui-dialog').dialog('close');
		SPCreateOrder.orderChanged = true;
	},
	
	/**
	 * Ham mo dialog goi y ton kho dap ung sp theo Kho
	 */
	openSelectProductWarehouseDialogCTTL: function(productCode,index, productWarehouse,rownum){
		var orderNumber = $('#orderNumber').val();
		$('#productWarehouseDialogContainer').show();
		$('.clsProductVOQuantity').bind('keydown',function(e) {
			var index = $(this).attr('index');
			var isPressed = false;
			if (e.keyCode == keyCodes.ARROW_DOWN) {
				++index;
				isPressed = true;
			} else if (e.keyCode == keyCodes.ARROW_UP) {
				--index;
				isPressed = true;
			} 
			if (index > -1 && isPressed == true) {
				$('#productRow_'+index).focus();
			}
		});
		$('#indexTmpToAddRow').val(index);
		var product = GridSaleBusiness.mapSaleProduct.get(productCode);
		if (productWarehouse === SPCreateOrder.productWarehouseType.PROMOTION_PRODUCT) {
			product = $('#accumulativePromotionGrid').datagrid('getRows')[index];
		}
//		var lstData = SPCreateOrder._mapPRODUCTSbyWarehouseOnGrid.get(product.productId);
		var lstData = GridSaleBusiness.getWarehouseOnGridByType(product.productId);
//		var lstA = SPCreateOrder._mapPRODUCTSbyWarehouse.get(product.productId);
		var lstA = GridSaleBusiness.getWarehouseByType(lstDoiSPKM[i].productId);
		if (lstA instanceof Array) {
			for (var i = 0, sz = lstData.length; i < sz; i++) {
				for (var j = 0, szj = lstA.length; j < szj; j++) {
					if (lstData[i].warehouseId == lstA[j].warehouseId) {
						lstData[i].stockSource = lstA[j].availableQuantity;
						break;
					}
				}
			}
		} else {
			for (var i = 0, sz = lstData.length; i < sz; i++) {
				lstData[i].stockSource = lstData[i].availableQuantity;
			}
		}
		var html = $('#productWarehouseDialog').html();
		$('#productWarehouseDialog').window('refresh');
		var title = "Tồn kho đáp ứng của sản phẩm " + Utils.XSSEncode(product.productCode + " - " + product.productName) + ' tương ứng các kho';
		$('#productWarehouseDialog').dialog({closed: false,title:title,modal: true,
			onOpen: function() {			
				$('#spProductWarehouseCode').html(Utils.XSSEncode(product.productCode));
				$('#spProductWarehouseName').html(Utils.XSSEncode(product.productName));
				$('.easyui-dialog #GeneralInforErrors').html('').hide();
				var value = 0 ;//gia tri tong cua sp KM trong 1 lan dat
				var rows = $('#accumulativePromotionGrid').datagrid('getRows');
				for(var i = 0 ; i < rows.length ; i++){
					if(productCode == rows[i].productCode && rows[i].rownum == rownum){
						value += Number(rows[i].value) ;
					}
				}
				var totalStock = 0 ;
				if(lstData != null && lstData.length > 0){
					for(var i = 0 ; i < lstData.length ; i++){
						totalStock += lstData[i].availableQuantity;
					}
				}
				if(totalStock > value){
					totalStock = value;
				}
				$('.easyui-dialog #btnSelectWarehouse').attr('onclick','return SPCreateOrder.selectProductByWarehouseCTTL('+product.productId+','+rownum+','+totalStock+','+value+');');
				SPCreateOrder._editingProductCode = product.productCode;
				try {
					SPCreateOrder._selectingIndex = Number(index);
				} catch (e) {
					// pass through
				}
				Utils.bindAutoSearch();
	        	$("#productWarehouseGrids").datagrid({
	        		data: lstData,
	        		fitColumns:true,
	        		pagination : true,
	        		width: $('#productWarehouseDialog').width()-25,
	        		scrollbarSize:0,
	        		singleSelect:true,
	        		rownumbers:true,cache: false,
	        		columns:[[
	        			{field:'warehouseCode',title:'Kho',width:$('#productWarehouseDialog').width() - 35 - 120 - 80 - 100 - 130, align:'left', formatter: function(value, row, index) {
	        				return '<span class="prStyle'+index+'">'+Utils.XSSEncode(row.warehouseName)+'</span>';
	        			}},
	        			{field:'availableQuantity',title: 'Tồn kho đáp ứng' /*isNullOrEmpty(SPAdjustmentOrder.orderId)? 'Tồn kho đáp ứng':'Tồn kho thực tế'*/,
	        				align:'right',width:130, formatter: function(value, row, index) {
	        					var value = Number(row.stockSource);//Number(row.availableQuantity);	  
	        					var displayStr = formatQuantityEx(value, row.convfact);	        					
	        					return '<span class="prStyle'+index+'">' + displayStr + '</span>';
	        				}
	        			},  
	        			{field:'quantity',title:'Số lượng',width:130, algin:'center', formatter: function(value, row, index) {
	        					var id = "warehouseRow_" + index + "_" + Utils.XSSEncode(productWarehouse);
	        					return '<input id=\"' + id + '\" index="'+index+'" warehouseId="'+row.warehouseId+'" maxlength="8" type="text" style="width: 65px; margin-bottom: 0px;" onkeypress="NextAndPrevTextField(event,this,\'clsProductVOQuantity\')" class="clsProductVOQuantity InputTextStyle InputText1Style prStyle'+index+'" size="10"/>&nbsp;';
	        				}
	        			}, 
	        		]],	        		
	        		onBeforeLoad: function() {
	        			
	        		},
	        		onLoadSuccess: function() {
	        			$('.clsProductVOQuantity').css('text-align','right').css('padding-right','5px');
						$('.ccSupportCode').css('text-align','left').css('padding-left','5px');
						Utils.bindFormatOnTextfieldInputCss('clsProductVOQuantity',Utils._TF_NUMBER_CONVFACT);
						$('.datagrid-header-rownumber').html('STT');
						$(window).resize();
						$('#warehouseRow_0' + productWarehouse).focus();
						setTimeout(function(){
							CommonSearchEasyUI.fitEasyDialog("productWarehouseDialog");
						}, 100);
	        		}
	        	});
	        },
	        onClose : function(){
	        	$('.easyui-dialog #btnSelectWarehouse').attr('onclick','return SPCreateOrder.selectProductByWarehouse();');
	        	$('#productWarehouseDialog').html(html);
	        	$('#productWarehouseDialogContainer').hide();
	        	$('#productWarehouseDialogContainer .GridSection').html('<table id="productWarehouseGrids"></table>');
	        }
		});
	},
	
	getObjectPromotionProduct:function(pp){
		var pproduct = {};
		if(pp.promoProduct==null || pp.promoProduct==undefined){
			pproduct.productCode = '';
			pproduct.productName = '';
		}else{
			pproduct.productCode = pp.promoProduct.productCode;
			pproduct.productName = pp.promoProduct.productName;
			pproduct.grossWeight= pp.promoProduct.grossWeight;
		}	
		if(pp.saleOrderDetail==null || pp.saleOrderDetail==undefined){
			pproduct.programCode = '';									
			pproduct.discountPercent = 0;
			pproduct.discountAmount=0;
		}else{
			pproduct.programCode = pp.saleOrderDetail.programCode;	
			var promoProduct = pp.promoProduct;
			if(promoProduct!=null && promoProduct!=undefined){
				pproduct.convfact = pp.promoProduct.convfact;
				pproduct.checkLot = pp.promoProduct.checkLot;
				pproduct.productId = pp.promoProduct.id;										
			}else{
				pproduct.convfact = '';										
			}							
			pproduct.discountPercent = pp.saleOrderDetail.discountPercent;
			pproduct.discountAmount = pp.saleOrderDetail.discountAmount;
			pproduct.maxDiscountAmount = pp.saleOrderDetail.discountAmount;
			pproduct.quantity = pp.saleOrderDetail.quantity;
			pproduct.maxQuantity = pp.saleOrderDetail.quantity;
			pproduct.maxQuantityFreeTotal = pp.saleOrderDetail.maxQuantityFreeTotal;
			pproduct.quantityReceived = pp.saleOrderDetail.promotionQuotation;
			pproduct.warehouse = pp.warehouse;
			pproduct.stockTotal = pp.stockTotal;
			pproduct.productGroup = pp.saleOrderDetail.productGroup;
			pproduct.groupLevel = pp.saleOrderDetail.groupLevel;
			var proQuatity = parseInt(pproduct.quantity);
			var stock = 0;
			if (pp.stockTotal != null && pp.stockTotal != undefined) {
				stock = parseInt(pp.stockTotal.availableQuantity);	
			}
			if(stock==0){
				pproduct.quantity=0;
			}
			pproduct.stock = stock;
		}	
		pproduct.type = pp.type;
		pproduct.key = pp.keyList;
		pproduct.changeProduct = pp.changeProduct;						
		pproduct.typeName = pp.typeName;						
		pproduct.isEdited = pp.isEdited;
		pproduct.promotionType = pp.promotionType;
		pproduct.levelPromo =  pp.saleOrderDetail.levelPromo;
		pproduct.openPromo = pp.openPromo;
		pproduct.productGroupId = pp.productGroupId;
		pproduct.groupLevelId = pp.groupLevelId;
		pproduct.hasPortion = pp.hasPortion;
		if(pp.promotionType==PromotionType.PROMOTION_FOR_ZV21 || pp.promotionType==PromotionType.PROMOTION_FOR_ORDER){
			pproduct.productCode = 'Khuyến mãi đơn hàng';			
		}
		return pproduct;
	},
	
	showChangePromotionProgram:function(){
		var html = $('#paymentChangePPContainer').html();
		$('#paymentChangePPContainer').show();
		$('#paymentChangePP').dialog({
			closed: false, 
			cache: false,
			modal: true,
			onOpen: function() {
				$('.easyui-dialog #paymentChangePPGrids').datagrid({
					data:SPCreateOrder.programes,
					autoWidth: true,
					width: $('#paymentChangePP').width()-50,
					columns:[[
					     {field:'promotionProgramCode', title: 'Mã CT', width: 100, sortable:false,resizable:true , align: 'left'},
						 {field:'promotionProgramName', title: 'Tên CT', width:$('#paymentChangePP').width()-235,sortable:false,resizable:true , align: 'left'},
						 // {field:'promotionLevel', title: 'Mức KM', width: 100, sortable:false,resizable:true , align: 'left'},
						 {field:'select', title: 'Chọn', width:80,sortable:false,resizable:true , align: 'left',formatter:function(value,row,index){
							 return '<a href="javascript:void(0)" onclick="SPCreateOrder.selectPromotionProgram('+index+')">Chọn </a>';
						 }},
					]],
					onLoadSuccess: function() {
						setTimeout(function() {
							CommonSearchEasyUI.fitEasyDialog("paymentChangePP");
						}, 200);
					}
				});
			},
			onClose : function(){
				$('#paymentChangePPContainer').html(html);
				$('#paymentChangePPContainer').hide();
			}
		});
	},
	
	selectPromotionProgram:function(index){
		var row = $('.easyui-dialog #paymentChangePPGrids').datagrid('getRows')[index];
		var ppCode = row.promotionProgramCode;
		var ppType = row.type;
		var htSaleData = $('#gridSaleData').handsontable('getInstance');
		var rowData = htSaleData.getData();
		/** reset map */
		var zPPAutoSize = SPCreateOrder._lstPromotionProductForOrderIsAuto.size();
		var PPMAPTEMP = new Map();
		for(var i = 0; i < SPCreateOrder._lstPromotionProductForOrderIsAuto.size(); i++) {
			PPMAPTEMP.put(SPCreateOrder._lstPromotionProductForOrderIsAuto.keyArray[i], SPCreateOrder._lstPromotionProductForOrderIsAuto.valArray[i]);
		}
		for(var i=0;i<zPPAutoSize;++i){
			var row = PPMAPTEMP.get(PPMAPTEMP.keyArray[i]);
			if(row!=null && row != undefined){
				if(row.promotionType==PromotionType.PROMOTION_FOR_ZV21 
						|| row.promotionType==PromotionType.PROMOTION_FOR_ORDER
						|| row.promotionType==PromotionType.PROMOTION_FOR_ORDER_TYPE_PRODUCT){
					SPCreateOrder._lstPromotionProductForOrderIsAuto.remove(PPMAPTEMP.keyArray[i]);
				}
			}			
		}	
		zPPAutoSize = SPCreateOrder._lstPromotionProductForOrderIsAuto.size();
		var zzrowNumForOrder = zPPAutoSize+1;
		
		if(ppType=='ZV21'){
			var hasHeader = false;
			var zV21 = SPCreateOrder._lstPromotionProductForOrder.get('zV21');
			if(zV21!=null && zV21.length > 0){
				for(var k=0;k<zV21.length;++k){
					var zv = zV21[k];
					var ZV21 = SPCreateOrder.getObjectPromotionProduct(zv);
					ZV21.changeProduct = 1;
					ZV21.key = -1;								
					ZV21.rownum = zzrowNumForOrder;
					if (ZV21.programCode ==  ppCode) {
						if (!hasHeader) {
							SPCreateOrder._lstPromotionProductForOrderIsAuto.put(zPPAutoSize,ZV21);
							hasHeader = true;
						}
						// reset lai so suat toi da
						if (SPCreateOrder._saleOrderQuantityReceivedList != null && SPCreateOrder._saleOrderQuantityReceivedList.length > 0) {
							for (var ind=0; ind<SPCreateOrder._saleOrderQuantityReceivedList.length; ind++) {
								var objTmp = SPCreateOrder._saleOrderQuantityReceivedList[ind];
								if (objTmp.promotionCode == ZV21.programCode
									&& objTmp.productGroupId == ZV21.productGroupId
									&& objTmp.promotionLevel == ZV21.levelPromo) {
									objTmp.quantityReceived = objTmp.maxQuantityReceived;
									break;
								}
							}
						}
						zPPAutoSize = SPCreateOrder._lstPromotionProductForOrderIsAuto.size();

						var warehouselessProductMap = new Map();	// Map<program_code, List<product_code>>
						for(var i=0;i<zv.listPromotionForPromo21.length;++i){
							var pproduct = new Object();
							var obj = zv.listPromotionForPromo21[i].saleOrderLot;
							if (obj != null && obj != undefined) {
								// cong so luong da tach kho tren server lai, de thuc hien tach kho lai tren client
								var oldQuantity = obj[0].quantity;
								for (var j = 1; j < obj.length; obj[0].quantity += obj[j].quantity, j++);
								
								// tach kho lai tu sale_order_lot dau tien
								var j = 0;
								obj[j].saleOrderDetail = zv.listPromotionForPromo21[i].saleOrderDetail;
								obj[j].promoProduct = zv.listPromotionForPromo21[i].promoProduct;
								obj[j].type = zv.listPromotionForPromo21[i].type;
								obj[j].keyList = zv.listPromotionForPromo21[i].keyList;
								obj[j].changeProduct = zv.listPromotionForPromo21[i].changeProduct;						
								obj[j].typeName = zv.listPromotionForPromo21[i].typeName;						
								obj[j].isEdited = zv.listPromotionForPromo21[i].isEdited;
								obj[j].promotionType = zv.listPromotionForPromo21[i].promotionType;
								obj[j].levelPromo = zv.listPromotionForPromo21[i].levelPromo;
								obj[j].productGroupId = zv.listPromotionForPromo21[i].productGroupId;
								/*
								 * tach kho, set lai stockTotal & warehouse
								 */
								var autoSplitedSaleOrderLot = SPCreateOrder.autoSplitWarehouse(obj[j].stockTotal.product.id,obj[j]);
								//obj[0].quantity = oldQuantity;
								autoSplitedSaleOrderLot.forEach(function(sol) {
									var quantity = sol.quantity;
									var pproduct = SPCreateOrder.getObjectPromotionProduct(sol);
									if (!pproduct.productGroupId && pproduct.productGroup) {
										pproduct.productGroupId = pproduct.productGroup.id;
									}
									pproduct.quantity = quantity;
									//pproduct.maxQuantity = pproduct.quantity;	
									pproduct.rownum = zzrowNumForOrder;
									pproduct.hasPortion = zv.hasPortion;
									SPCreateOrder._lstPromotionProductForOrderIsAuto.put((zPPAutoSize),pproduct);
									++zPPAutoSize;
								});
							} else {
								var promotionInfo = zv.listPromotionForPromo21[i];
								if (!warehouselessProductMap.get(promotionInfo.saleOrderDetail.programCode)) {
									warehouselessProductMap.put(promotionInfo.saleOrderDetail.programCode, []);
								}
								if ($.inArray(promotionInfo.promoProduct.productCode, warehouselessProductMap.get(promotionInfo.saleOrderDetail.programCode)) < 0) {
									warehouselessProductMap.get(promotionInfo.saleOrderDetail.programCode).push(promotionInfo.promoProduct.productCode);
								}
							}
						};
						SPCreateOrder._isUsingZVOrder = 'ZV21';		
						SPCreateOrder._isUsingZVCode = ZV21.programCode;
					}
				}			
				/*
				 * reset gia tri CKMH
				 */
				// tinh tong tien chiet khau
				
				var updatedRowsIndex = new Array();
				var updatedRow = rowData.filter(function(item, idx){
									if (item[COLSALE.PRO_CODE]) {
										var foundCS = GridSaleBusiness.listCS.some(function(cs) {
											return cs.code == item[COLSALE.PROGRAM];
										});
										if ((item[COLSALE.PROGRAM] && !foundCS) || !item[COLSALE.PROGRAM]) {
											updatedRowsIndex.push(idx);
											return true;
										}
										return false;
									}
									return false;													
								});

				updatedRow.forEach(function(row, index) {
					var discountAmount = SPCreateOrder._mapDiscountAmountOrder.get(updatedRowsIndex[index]);
					if (discountAmount == null || discountAmount == undefined) discountAmount = 0;
					var totalDiscountTmp = row[COLSALE.DISCOUNT];
					if (isNullOrEmpty(totalDiscountTmp)) totalDiscountTmp = 0;
					var totalDiscount = Number(totalDiscountTmp.toString().replace(/,/g, ""));
					var tmp = totalDiscount - discountAmount
					if (tmp == 0) tmp = "";
					htSaleData.setDataAtCell(updatedRowsIndex[index] ,COLSALE.DISCOUNT,formatCurrency(tmp));
					SPCreateOrder._mapDiscountAmountOrder.remove(updatedRowsIndex[index]);
				});

				/*
				 * thong bao loi SPKM ko co ton kho
				 */
				if (warehouselessProductMap.keyArray.length > 0) {
					var errorMessage = warehouselessProductMap.keyArray.reduce(function(prevProgramStr, programCode) {
										var products = warehouselessProductMap.get(programCode);
										if (products && products.length > 0) {
											var productsStr = products.reduce(function(prevProductStr, productCode) {
																return prevProductStr + ', ' + Utils.XSSEncode(productCode);
															});
											return prevProgramStr ? prevProgramStr : '' + '<br>Sản phẩm ' + Utils.XSSEncode(productsStr) + ' của CTKM ' + Utils.XSSEncode(programCode);
										} else {
											return prevProgramStr;
										}													
									}, '');
					errorMessage = 'Các SPKM sau không có tồn kho. Vui lòng nhập kho cho sản phẩm để được hưởng KM.'
								+ '<br>'
								+ errorMessage;
					$.messager.alert('Các SPKM không có tồn kho', errorMessage, 'warning');
				}
			}
		}else if(ppType=='ZV19' || ppType=='ZV20'){
			var ZV1920 = SPCreateOrder._lstPromotionProductForOrder.get('zV19z20');
			var isChecked = false;
			var dataIndex = new Array();
			var ratio = 0;
			for(var i=0;i<ZV1920.length;++i){
				var obj = new Object();
				obj = SPCreateOrder.getObjectPromotionProduct(ZV1920[i]);
				obj.changeProduct = 1;
				obj.key = -2;
				obj.listPromoDetail = ZV1920[i].listPromoDetail;
				obj.rownum = zzrowNumForOrder;
				if(obj.promotionType==PromotionType.PROMOTION_FOR_ORDER && obj.type==PromotionType.FREE_PRICE && ppType=='ZV20' &&  ppCode == obj.programCode){
					SPCreateOrder._isUsingZVOrder = 'ZV20';
					SPCreateOrder._isUsingZVCode = obj.programCode;
					SPCreateOrder._lstPromotionProductForOrderIsAuto.put(zPPAutoSize,obj);
					// ratio =Math.round(saleOrderDiscountAmount*10000/totalBuyAmount)/100;
					dataIndex.push(i);
				}else if(obj.promotionType==PromotionType.PROMOTION_FOR_ORDER && obj.type==PromotionType.FREE_PERCENT && ppType=='ZV19' && ppCode == obj.programCode){
					SPCreateOrder._isUsingZVOrder = 'ZV19';	
					SPCreateOrder._isUsingZVCode = obj.programCode;				
					SPCreateOrder._lstPromotionProductForOrderIsAuto.put(zPPAutoSize,obj);
					ratio = obj.discountPercent/100;
					dataIndex.push(i);
				}	
				zPPAutoSize++;
			}	
			
			var listProduct = new Array();
			dataIndex.forEach(function(item) {
				listProduct.push(ZV1920[item]);
			});

			/*
			 * phan bo tien CKTM don hang
			 */
			// tinh tong tien chiet khau
			var saleOrderDiscountAmount = listProduct.reduce(function(prevTotalDiscountAmount, promotionProgram) {
											var totalDiscountAmount = promotionProgram.listPromoDetail.reduce(function(prevDiscountAmount, promoDetail) {
																		return prevDiscountAmount + Number(promoDetail.discountAmount);
																	}, 0);
											return prevTotalDiscountAmount + totalDiscountAmount;
										}, 0);

			/*
			 * lay cac dong du lieu tren grid de update CKTM: cac dong san pham khong co CT HTTM hoac co CT HTTM dang KM tu dong
			 */
			var rowObject = rowData.map(function(r, index){
				return {
					row: r,
					rowIndex: index,
				}
			});

			var updatedRow = rowObject.filter(function(item, idx){
								if (item.row[COLSALE.PRO_CODE]) {
									var isManualPromotionProgram = GridSaleBusiness.listCS.some(function(cs) {
										return cs.code == item.row[COLSALE.PROGRAM];
									});
									if ((item.row[COLSALE.PROGRAM] && !isManualPromotionProgram) || !item.row[COLSALE.PROGRAM]) {
										return true;
									}
									return false;
								}
								return false;
							});

			// updatedRow.sort(function(obj1, obj2) {
			// 	return obj1.row[COLSALE.PRO_CODE] > obj2.row[COLSALE.PRO_CODE];
			// });

			var tmpProductWarehouseMap = new Map();
			updatedRow.forEach(function(rowObj, index){
				var keyMap = rowObj.row[COLSALE.PRO_CODE] + '_';
				if (!tmpProductWarehouseMap.get(keyMap)) {
					tmpProductWarehouseMap.put(keyMap, {
						rows: [],
						totalAmount: 0
					});
				}
				var mapValue = tmpProductWarehouseMap.get(keyMap);
				mapValue.totalAmount += Number(rowObj.row[COLSALE.AMOUNT].toString().replace(/,/g, ""));
				mapValue.rows.push(rowObj);
			});

			/*
			 * tinh tong tien mua hang
			 */
			var totalBuyAmount = updatedRow.reduce(function(prevAmount, rowObj) {
									return prevAmount + Number(rowObj.row[COLSALE.AMOUNT].toString().replace(/,/g, ""));
								}, 0);

			/*
			 * phan bo CKTM
			 */
			var accumulativeDistributedDiscountAmount = 0;
			tmpProductWarehouseMap.keyArray.forEach(function(keyMap, tempIndex) {
				var product = SPCreateOrder.findProductCodeInMap(keyMap.split('_')[0]);
				if (product != null) {
					var productWarehouse = tmpProductWarehouseMap.get(keyMap);
					var totalProductAmount = productWarehouse.totalAmount;
					var productDiscountPercent = 0;
					var productDiscountAmount = 0;
					if (SPCreateOrder._isUsingZVOrder == 'ZV19') {
						productDiscountPercent = ratio;
						productDiscountAmount = Math.round(productDiscountPercent * totalProductAmount);
					} else if (SPCreateOrder._isUsingZVOrder == 'ZV20') {
						productDiscountPercent =Math.round(saleOrderDiscountAmount*10000/totalBuyAmount)/100;
						//productDiscountAmount = Math.round((productDiscountPercent * totalProductAmount)/100);
						productDiscountAmount = Math.floor((productDiscountPercent * totalProductAmount)/100);
						if (productDiscountAmount >= saleOrderDiscountAmount) {
							productDiscountAmount = saleOrderDiscountAmount;
						}
					}					

					if (tempIndex == tmpProductWarehouseMap.keyArray.length - 1
						|| productDiscountAmount > saleOrderDiscountAmount - accumulativeDistributedDiscountAmount) {
						productDiscountAmount = saleOrderDiscountAmount - accumulativeDistributedDiscountAmount;
					}
					var tmpAccumulativeDiscountAmount = 0;
					for (var k = 0; k < productWarehouse.rows.length; k++) {
						var rowObj = productWarehouse.rows[k];
						var rowDiscountAmount = 0;
						if (k == productWarehouse.rows.length - 1) {
							rowDiscountAmount = Math.round(productDiscountAmount - tmpAccumulativeDiscountAmount);
						} else {
							var rowDiscountPercent = Math.round10(Number(rowObj.row[COLSALE.AMOUNT].toString().replace(/,/g, ""))/totalProductAmount, -2);
							//rowDiscountAmount = Math.round(rowDiscountPercent * productDiscountAmount);
							rowDiscountAmount = Math.floor(rowDiscountPercent * productDiscountAmount);
						}
						// update to grid
						var oldDiscountAmount = Number((htSaleData.getDataAtCell(rowObj.rowIndex ,COLSALE.DISCOUNT) || "").toString().replace(/,/g, "")) 
												 - SPCreateOrder._mapDiscountAmountOrder.get(rowObj.rowIndex);
						htSaleData.setDataAtCell(rowObj.rowIndex ,COLSALE.DISCOUNT,formatCurrency(rowDiscountAmount + oldDiscountAmount));
						tmpAccumulativeDiscountAmount += rowDiscountAmount;

						SPCreateOrder._mapDiscountAmountOrder.keyArray[rowObj.rowIndex] = rowObj.rowIndex;
						SPCreateOrder._mapDiscountAmountOrder.valArray[rowObj.rowIndex] = rowDiscountAmount;
					}
					accumulativeDistributedDiscountAmount += productDiscountAmount;
				}
			});
			//
		}
			
		SPCreateOrder.fillTablePromotionProductForOrder();
		if (SPAdjustmentOrder._orderId > 0 && $("#payment").prop("disabled")) {
			enable("btnOrderSave");
		}
		$('#divOverlay').show();
		setTimeout(function(){
			SPCreateOrder.unshareDiscountSaleProduct(SPCreateOrder._totalAcc);
			SPCreateOrder._totalAcc = new Array();
			SPCreateOrder.changeGridAccumulativeWhenChangeProgram();
			$('#divOverlay').hide();
		},1000);
		$('.easyui-dialog').dialog('close');
	},
	
	fillTablePromotionProduct:function(pp){
		SPAdjustmentOrder.initPromotionProductGrid();
		//$('#gridPromotionProduct').datagrid('loadData', SPCreateOrder._lstPromotionProductIsAuto.valArray);
		var rows = $('#gridPromotionProduct').datagrid('getRows');
		var mergesCommons = new Array();
		var mergesZV1921 = new Array();
		var mergesRowNum = new Array();
		var indexRowNum = -1;
		for(var i=0;i<rows.length;++i){
			var row = rows[i];
			var color = SPAdjustmentOrder.addStyleRowProductDialog(i,row);
			if(color==WEB_COLOR.ORANGE){
				$('span.prStyle'+i).each(function(){ $(this).css('color',WEB_COLOR.ORANGE_CODE);});
			}else if(color==WEB_COLOR.RED){
				$('span.prStyle'+i).each(function(){ $(this).css('color',WEB_COLOR.RED_CODE);});
			}else {
			  $('span.prStyle'+i).each(function(){ $(this).css('color',WEB_COLOR.BLACK_CODE);});
			}
			if(row.promotionType!=undefined && row.promotionType!=null){				
				if(row.promotionType==PromotionType.PROMOTION_FOR_ORDER){
					var cell = {index:i,colspan:2};
					mergesZV1921.push(cell);
				}else if(row.promotionType==PromotionType.PROMOTION_FOR_ZV21){
					var cell = {index:i,colspan:6};					
					mergesZV1921.push(cell);
				}	
				if(row.promotionType==PromotionType.PROMOTION_FOR_ZV21 
						|| row.promotionType==PromotionType.PROMOTION_FOR_ORDER
						|| row.promotionType==PromotionType.PROMOTION_FOR_ORDER_TYPE_PRODUCT){
					if(indexRowNum<0){
						indexRowNum = i;
					}
					mergesRowNum.push(row);
				}
			}	
			if(row.type==PromotionType.FREE_PRICE || row.type==PromotionType.FREE_PERCENT){
				var cell = {index:i,colspan:2};
				mergesCommons.push(cell);
			}
		}
		if(indexRowNum>=0){
			$('#gridPromotionProduct').datagrid('mergeCells',{  
                index: indexRowNum,  
                field: 'rownum',  
                rowspan: mergesRowNum.length  
            }); 
		}
		for(var i=0; i<mergesCommons.length; i++){  
			$('#gridPromotionProduct').datagrid('mergeCells',{  
                index: mergesCommons[i].index,  
                field: 'stock',  
                colspan: mergesCommons[i].colspan  
            });  
        }		
		for(var i=0; i<mergesZV1921.length; i++){  
			$('#gridPromotionProduct').datagrid('mergeCells',{  
                index: mergesZV1921[i].index,  
                field: 'productCode',  
                colspan: mergesZV1921[i].colspan  
            });  
        }		
        SPAdjustmentOrder.mergesColRowsPromotionGrid('gridPromotionProduct');
		SPCreateOrder.showTotalGrossWeightAndAmount();
	},
	
	fillTablePromotionProductForOrder:function(pp){
		SPAdjustmentOrder.initSalePromotionProductGrid();
		var rows = $('#gridSaleOrderPromotion').datagrid('getRows');
		var mergesCommons = new Array();
		var mergesZV1921 = new Array();
		var mergesRowNum = new Array();
		var indexRowNum = -1;
		for(var i=0;i<rows.length;++i){
			var row = rows[i];
			if(row.promotionType!=undefined && row.promotionType!=null){				
				if(row.promotionType==PromotionType.PROMOTION_FOR_ORDER){
					var cell = {index:i,colspan:2};
					mergesZV1921.push(cell);
				}else if(row.promotionType==PromotionType.PROMOTION_FOR_ZV21){
					var cell = {index:i,colspan:6};					
					mergesZV1921.push(cell);
				}	
				if(row.promotionType==PromotionType.PROMOTION_FOR_ZV21 
						|| row.promotionType==PromotionType.PROMOTION_FOR_ORDER
						|| row.promotionType==PromotionType.PROMOTION_FOR_ORDER_TYPE_PRODUCT){
					if(indexRowNum<0){
						indexRowNum = i;
					}
					mergesRowNum.push(row);
				}
			}	
			if(row.type==PromotionType.FREE_PRICE || row.type==PromotionType.FREE_PERCENT){
				var cell = {index:i,colspan:2};
				mergesCommons.push(cell);
			}
		}
		if(indexRowNum>=0){
			$('#gridSaleOrderPromotion').datagrid('mergeCells',{  
                index: indexRowNum,  
                field: 'rownum',  
                rowspan: mergesRowNum.length  
            }); 
		}
		for(var i=0; i<mergesCommons.length; i++){  
			$('#gridSaleOrderPromotion').datagrid('mergeCells',{  
                index: mergesCommons[i].index,  
                field: 'stock',  
                colspan: mergesCommons[i].colspan  
            });  
        }		
		for(var i=0; i<mergesZV1921.length; i++){  
			$('#gridSaleOrderPromotion').datagrid('mergeCells',{  
                index: mergesZV1921[i].index,  
                field: 'productCode',  
                colspan: mergesZV1921[i].colspan  
            });  
        }		
		SPCreateOrder.showTotalGrossWeightAndAmount();
	},
	
	
	changePromotionProduct:function(mapId){
		//Danh sach san pham khuyen mai thay the
		var obj = SPCreateOrder._lstPromotionProductForOrderIsAuto.get(mapId);
		if(obj==null || obj == undefined){
			return false;
		}
		var promotionCode = obj.programCode;
		var arrParams = new Array();
		var param = new Object();
		param.name = 'promotionCode';
		param.value = promotionCode;
		arrParams.push(param);
		CommonSearch.promotionProduct(function(data){
			obj.productCode = data.ppCode;
			obj.productName = data.ppName;
			obj.quantity = data.ppQuantity;
			SPCreateOrder.fillTablePromotionProduct(SPCreateOrder._lstPromotionProductForOrderIsAuto);
		}, arrParams);
	},

	/**
	 * change promotion product when adjust sale order
	 * @param rowIndex selecting row's index in grid
	 * @author tuannd20
	 * @date 10/10/2014
	 */
	changePromotionProductForAdjustSaleOrder: function(rowIndex, isZV21, isZV24) {
		var row = isZV24 ? $('#promotionGrid2').datagrid('getRows')[rowIndex] :
						isZV21 ? $('#gridSaleOrderPromotion').datagrid('getRows')[rowIndex] :
						$('#gridPromotionProduct').datagrid('getRows')[rowIndex];
		var data = {
			orderId: SPAdjustmentOrder._orderId,
			productCode: row.productCode,
			programCode: row.programCode,
			promoLevel: row.levelPromo,
			groupLevelId: row.groupLevel ? row.groupLevel.id : row.groupLevelId
		};
		$.get('/sale-product/create-order/edit/change-promotion-product', data, function(result){
			var changePPromotion = result.changePPromotion;
			if (changePPromotion == null || changePPromotion == undefined || changePPromotion.length == 0) {
			    	return;
			}
			var isEdited = changePPromotion[0].isEdited;

			var listPromoTmp = new Map();
			if (result.isZV24) {
				listPromoTmp = SPCreateOrder._lstPromotionOpenProduct;
			} else if (result.isZV21) {
				listPromoTmp = SPCreateOrder._lstPromotionProductForOrderIsAuto;
			} else {
				listPromoTmp = SPCreateOrder._lstPromotionProductIsAuto;
			}
		    // //Luu tat ca CTKM theo muc de ap dung chinh sua so suat
			// SPCreateOrder._promotionPortionReceivedList = new Map();
			for (var i=0;i<SPCreateOrder._saleOrderQuantityReceivedList.length;i++) {
				var tmp = SPCreateOrder._saleOrderQuantityReceivedList[i];
				var listProduct = new Array();
				changePPromotion.forEach(function(item, tempIndex) {
					if (item.saleOrderDetail.programCode == tmp.promotionCode
						&& item.productGroupId == tmp.productGroupId
						&& item.saleOrderDetail.levelPromo == tmp.promotionLevel) {
						tmp.quantityReceived = tmp.maxQuantityReceived;
						item.quantityReceived = tmp.quantityReceived;
						item.maxQuantityReceived = tmp.maxQuantityReceived;
						item.maxQuantityReceivedTotal = tmp.maxQuantityReceivedTotal;
						if (item.promotionType == PromotionType.PROMOTION_FOR_ZV21) {
							item.listPromotionForPromo21.forEach(function(item21,index) {
								item21.quantityReceived = tmp.quantityReceived;
								item21.maxQuantityReceived = tmp.maxQuantityReceived;
								item21.maxQuantityReceivedTotal = tmp.maxQuantityReceivedTotal;
								listProduct.push(item21);
							});
						} else {
							listProduct.push(item);
						}
					}
				});
				if (listProduct.length > 0) {
					SPCreateOrder._promotionPortionReceivedList.put(tmp.promotionCode+"_"+tmp.productGroupId+"_"+tmp.promotionLevel,listProduct);
				}
			}

		    if (result.canChaneMultiProduct) {
		    	SPCreateOrder.promotionMultiProductDialog(function(lstObj) {
		    		var promotionGroup = null;
		    		if (result.isZV24) {
						promotionGroup = OpenProductPromotion._promotionProgramGroup;
					} else if (result.isZV21) {
						promotionGroup = SPAdjustmentOrder._promotionProgramGroup;
					} else {
						promotionGroup = SPCreateOrder._promotionProgramGroup;
					}
	    			/*
	    			 * prepare data for grid
	    			 */
	    			var listPromoTmp = new Array();
	    			var key = row.programCode + '_' + row.productGroupId + '_' + row.levelPromo /*+ '_' + row.changeProduct*/;
	    			promotionGroup.remove(key);
	    			promotionGroup.valArray.forEach(function(itemsInGroup){
	    				if (itemsInGroup !== null && itemsInGroup !== undefined && itemsInGroup instanceof Array) {
	    					itemsInGroup.forEach(function(item) {
	    						listPromoTmp.push(item);
	    					});
	    				}
	    			});
	    			if (result.isZV21 && SPCreateOrder._lstPromotionProductForOrderIsAuto != null) {
		    			for (var ij = 0, szij = SPCreateOrder._lstPromotionProductForOrderIsAuto.size(); ij < szij; ij++) {
		    				if (SPCreateOrder._lstPromotionProductForOrderIsAuto.valArray[ij].productCode.trim().indexOf(" ") > -1) {
		    					listPromoTmp.push(SPCreateOrder._lstPromotionProductForOrderIsAuto.valArray[ij]);
		    				}
		    			}
		    		}
	    			lstObj.forEach(function(item) {
	    				listPromoTmp.push(item);
	    			});

	    			/*
	    			 * sort data
	    			 */
	    			listPromoTmp.sort(function(o1, o2){
	    				var id = '-1413001269113';
	    				if (o1.programCode != o2.programCode) {
	    					return o1.programCode > o2.programCode;
	    				}
					if (o1.productCode.trim().indexOf(" ") > 0) {
						return false;
					}
					if (o2.productCode.trim().indexOf(" ") > 0) {
						return true;
					}
	    				if (Number(o1.levelPromo) != Number(o2.levelPromo)) {
	    					return o1.levelPromo > o2.levelPromo;
	    				}
	    				if (!isNaN(o1.productGroupId) && !isNaN(o2.productGroupId)
	    					&& Number(o1.productGroupId) != Number(o2.productGroupId)) {
	    					return o1.productGroupId < o2.productGroupId;
	    				}
	    				if (Number(o1.changeProduct) != Number(o2.changeProduct)) {
	    					return Number(o1.changeProduct) > Number(o2.changeProduct);
	    				}
	    				if (o1.productCode != o2.productCode) {
	    					return o1.productCode > o2.productCode;
	    				}
	    				return o1.warehouse.seq > o2.warehouse.seq;
	    			});
	    			
	    			/*
	    			 * reset row num
	    			 */
	    			listPromoTmp.forEach(function(item, index) {
	    				item.rownum = index + 1;
	    			});
	    			if (result.isZV24) {
						SPCreateOrder._lstPromotionOpenProduct.keyArray = listPromoTmp.map(function(item, index){
	    					return index;
	    				});
	    				SPCreateOrder._lstPromotionOpenProduct.valArray = listPromoTmp;
						OpenProductPromotion.loadOpenProductGrid(SPCreateOrder._lstPromotionOpenProduct);
					} else if (result.isZV21) {
	    				//SPCreateOrder._lstPromotionProductForOrderIsAuto = listPromoTmp;
	    				SPCreateOrder._lstPromotionProductForOrderIsAuto.keyArray = listPromoTmp.map(function(item, index){
	    					return index;
	    				});
	    				SPCreateOrder._lstPromotionProductForOrderIsAuto.valArray = listPromoTmp;
	    				SPCreateOrder.fillTablePromotionProductForOrder(SPCreateOrder._lstPromotionProductForOrderIsAuto);
	    			} else {
	    				SPCreateOrder._lstPromotionProductIsAuto.keyArray = listPromoTmp.map(function(item, index){
	    					return index;
	    				});
	    				SPCreateOrder._lstPromotionProductIsAuto.valArray = listPromoTmp;
	    				SPCreateOrder.fillTablePromotionProduct(SPCreateOrder._lstPromotionProductIsAuto);
	    			}
	    			$('#searchStyle5').dialog('close');
	    			enable('backBtn');
	    			SPCreateOrder.orderChanged = true;
	    			SPCreateOrder.updatePromotionGrids();
		    	}, changePPromotion, 0, isZV21, null, isZV24);
		    	
		    } else {
		    	SPCreateOrder.promotionProductDialog(function(chooseObj) {
		    		if (rowIndex != null & rowIndex != undefined) {
		    			if (!isNaN(rowIndex)) {
		    				listPromoTmp.remove(rowIndex);
		    				listPromoTmp.keyArray.splice(Number(rowIndex), 0, rowIndex);
		    				listPromoTmp.valArray.splice(Number(rowIndex), 0, chooseObj);
		    				chooseObj.rownum = rowIndex + 1;
		    				if (result.isZV24) {
	    						SPCreateOrder._lstPromotionOpenProduct = listPromoTmp;
	    						OpenProductPromotion.loadOpenProductGrid(SPCreateOrder._lstPromotionOpenProduct);
	    					} else if (result.isZV21) {
		    					SPCreateOrder._lstPromotionProductForOrderIsAuto = listPromoTmp;
		    					SPCreateOrder.fillTablePromotionProductForOrder(SPCreateOrder._lstPromotionProductForOrderIsAuto);
		    				} else {
		    					SPCreateOrder._lstPromotionProductIsAuto = listPromoTmp;
		    					SPCreateOrder.fillTablePromotionProduct(SPCreateOrder._lstPromotionProductIsAuto);
		    				}
		    			}
		    			SPCreateOrder.orderChanged = true;
		    			SPCreateOrder.updatePromotionGrids();
		    		}
		    	}, changePPromotion, isZV21, null, isZV24, isEdited);
		    }
		});
	},
	
	resetPromotionGrid:function(){
		SPCreateOrder._mapDiscountAmount = new Map();
		SPCreateOrder._mapDiscountPercent = new Map();
		SPCreateOrder._lstPromotionProductIsAuto= new Map();
		SPCreateOrder._lstPromotionProductForOrderIsAuto = new Map();
		SPAdjustmentOrder.initPromotionProductGrid();
		SPAdjustmentOrder.initSalePromotionProductGrid();
	},
	
	/**
	 * Tim kiem sp trong map
	 * @returns {object}
	 */
	findProductCodeInMap:function(productCode){		
		if(SPCreateOrder._PRODUCTS!=null){
			for(var i=0;i<SPCreateOrder._PRODUCTS.size();i++){
				var obj = SPCreateOrder._PRODUCTS.get(SPCreateOrder._PRODUCTS.keyArray[i]);
				if(obj!=null && obj.productCode==productCode){
					return obj;
				}
			}
		}
		return null;		
	},
	
	/**
	 * Tim kiem sp trong list truyen vao
	 * @returns {object}
	 */
	findProductCodeInList:function(productCode,listProduct){		
		if(listProduct!=null){
			for(var i=0;i<listProduct.length;i++){
				var obj = listProduct[i];
				if(obj!=null && obj.productCode==productCode){
					return obj;
				}
			}
		}
		return null;		
	},
	
	/**
	 * Load thong tin Object SP mua tu grid
	 * @returns {object}
	 */
	getObjectSaleProductInGrid:function(rowData,index){
		//PRO_CODE: 0, PRO_NAME: 1, PACKING_PRICE: 2, PRICE: 3, WAREHOUSE: 4, STOCK: 5, QUANTITY: 6, AMOUNT: 7, DISCOUNT: 8, PROGRAM: 9, DELETE: 10
		var object = new Object();
		if (rowData != null) {
			product = SPCreateOrder.findProductCodeInMap( rowData[COLSALE.PRO_CODE]);
			if (product != null) {
				object.productCode = rowData[COLSALE.PRO_CODE];
				object.productName = rowData[COLSALE.PRO_NAME];
				object.pkPrice = rowData[COLSALE.PACKING_PRICE];//gia thung
				object.price = rowData[COLSALE.PRICE];//gia le
				if (!Utils.isEmpty(object.pkPrice)){
					object.pkPrice = object.pkPrice.replace(/,/g, "");
				}
				if (!Utils.isEmpty(object.price)){
					object.price = object.price.replace(/,/g, "");
				}
				object.warehouseId = GridSaleBusiness._mapWarehouseIdSelect.get(index);
				object.availableQuantity = product.availableQuantity;
				object.convfact = product.convfact;
				object.quantity = getQuantity(rowData[COLSALE.QUANTITY],product.convfact);
				object.discountAmount = rowData[COLSALE.DISCOUNT];
				/*if (SPAdjustmentOrder._orderId != null) {
					var commercial = GridSaleBusiness.mapCS.get(rowData[COLSALE.PROGRAM]);
					if (commercial != null) {
						object.commercialSupport = rowData[COLSALE.PROGRAM];
						object.commercialSupportType = commercial.type;
					} else if (rowData[COLSALE.PROGRAM] != null && commercial == null) {
						object.commercialSupport = rowData[COLSALE.PROGRAM];
						object.commercialSupportType = 'ZV';
					}
				} else {
					var commercial = GridSaleBusiness.mapCS.get(rowData[COLSALE.PROGRAM]);
					if (commercial != null) {
						object.commercialSupport = rowData[COLSALE.PROGRAM];
						object.commercialSupportType = commercial.type;
					} else {
						object.commercialSupport ='';
						object.commercialSupportType = '';
					}
				}*/

				var commercial = GridSaleBusiness.mapCS.get(rowData[COLSALE.PROGRAM]);
				if (commercial != null) {
					object.commercialSupport = rowData[COLSALE.PROGRAM];
					object.commercialSupportType = commercial.type;
				} else if (rowData[COLSALE.PROGRAM] != null && commercial == null) {
					object.commercialSupport = rowData[COLSALE.PROGRAM];
					object.commercialSupportType = 'ZV';
				}
				
			}
		};
		return object;
	} ,

	/**
	 * check if promotive quantity received on grid exceed quota (max quantity can received)
	 * @author tuannd20
	 * @param {Object} out output information about invalid data
	 * @return {Boolean} true if exceed, otherwise, false.
	 * @date 21/10/2014
	 */
	checkPromotiveQuantityReceivedExceedQuota: function(out) {
		/*
		 * validate in each group (promotion program, level) for ZV01-18
		 */
		for (var i = 0; i < SPCreateOrder._promotionProgramGroup.keyArray.length; i++) {
			var groupId = SPCreateOrder._promotionProgramGroup.keyArray[i];
			var rows = SPCreateOrder._promotionProgramGroup.get(groupId);
			/*
			 * prepare data for validate
			 */
			var productQuantityMap = new Map();
			rows.forEach(function(row) {
				var key = row.programCode + "_" + row.productGroupId + '_' + row.levelPromo + (row.canChaneMultiProduct ? "" : "_" + row.productCode);
				if (!productQuantityMap.get(key)) {
					productQuantityMap.put(key, {
						inputQuantity: 0,
						indexStr: "",
						isEdit: 1
					});
				}
				var info = productQuantityMap.get(key);
				info.maxReceivableQuantity = row.maxQuantity;
				info.inputQuantity += row.quantity;
				info.indexStr += row.rownum + ", ";
				info.isEdit = row.isEdited;
				productQuantityMap.put(key, info);
			});
			/*
			 * validate
			 */
			for (var j = 0; j < productQuantityMap.keyArray.length; j++) {
				var mapValue = productQuantityMap.valArray[j];
				var indexStr = mapValue.indexStr.trim().replace(/(?=,\s?$).*/g, "");
				if (mapValue.inputQuantity > mapValue.maxReceivableQuantity) {
					out.errMsg = "Tổng số lượng sản phẩm khuyến mãi ở dòng " + indexStr + " ở danh sách sản phẩm khuyến mãi vượt quá số lượng khuyến mãi tối đa được nhận.";
					return true;
				} else {
					/*
					 * isEdit = 0
					 * isEdit = 1
					 * isEdit = 2
					 */
					/*if (!mapValue.isEdit && mapValue.inputQuantity != mapValue.maxReceivableQuantity) {
						out.errMsg = "Tổng số lượng sản phẩm khuyến mãi ở dòng " + indexStr + " ở danh sách sản phẩm khuyến mãi phải bằng số lượng khuyến mãi tối đa được nhận.";
						return true;
					}*/
				}
			}
		}
		// khuyen mai mo moi
		for (var i = 0; i < OpenProductPromotion._promotionProgramGroup.keyArray.length; i++) {
			var groupId = OpenProductPromotion._promotionProgramGroup.keyArray[i];
			var rows = OpenProductPromotion._promotionProgramGroup.get(groupId);
			/*
			 * prepare data for validate
			 */
			var productQuantityMap = new Map();
			rows.forEach(function(row) {
				var key = row.programCode + "_" + row.productGroupId + '_' + row.levelPromo + (row.canChaneMultiProduct ? "" : "_" + row.productCode);
				if (!productQuantityMap.get(key)) {
					productQuantityMap.put(key, {
						inputQuantity: 0,
						indexStr: "",
						isEdit: 1
					});
				}
				var info = productQuantityMap.get(key);
				info.maxReceivableQuantity = row.maxQuantity;
				info.inputQuantity += row.quantity;
				info.indexStr += row.rownum + ", ";
				info.isEdit = row.isEdited;
				productQuantityMap.put(key, info);
			});
			/*
			 * validate
			 */
			for (var j = 0; j < productQuantityMap.keyArray.length; j++) {
				var mapValue = productQuantityMap.valArray[j];
				var indexStr = mapValue.indexStr.trim().replace(/(?=,\s?$).*/g, "");
				if (mapValue.inputQuantity > mapValue.maxReceivableQuantity) {
					out.errMsg = "Tổng số lượng sản phẩm khuyến mãi ở dòng " + indexStr + " ở danh sách sản phẩm khuyến mãi mở mới vượt quá số lượng khuyến mãi tối đa được nhận.";
					return true;
				} else {
					/*
					 * isEdit = 0
					 * isEdit = 1
					 * isEdit = 2
					 */
					/*if (!mapValue.isEdit && mapValue.inputQuantity != mapValue.maxReceivableQuantity) {
						out.errMsg = "Tổng số lượng sản phẩm khuyến mãi ở dòng " + indexStr + " ở danh sách sản phẩm khuyến mãi phải bằng số lượng khuyến mãi tối đa được nhận.";
						return true;
					}*/
				}
			}
		}
		//
		if (!out.errMsg) {
			/*
			 * validate for ZV21
			 */
			var rows = $('#gridSaleOrderPromotion').datagrid('getRows');
			// prepare data
			var promotionLevelGroup = new Map();
			rows.forEach(function(row, index) {
				if (row.promotionType != PromotionType.PROMOTION_FOR_ZV21) {
					var keyMap = Utils.XSSEncode(row.programCode + "_" + row.levelPromo + (row.canChaneMultiProduct ? "" : "_" + row.productCode));
					if (!promotionLevelGroup.get(keyMap)) {
						promotionLevelGroup.put(keyMap, {
							inputQuantity: 0,
							maxQuantity: 0,
							indexStr: "",
							//isEdit: 1
						});
					}
					var info = promotionLevelGroup.get(keyMap);
					info.inputQuantity += row.quantity;
					info.maxQuantity = row.maxQuantity;
					info.indexStr += (index + 1) + ", ";
					info.isEdit = row.isEdited;
					promotionLevelGroup.put(keyMap, info);				
				}
			});
			// validate
			for (var i = 0; i < promotionLevelGroup.valArray.length; i++) {
				var mapValue = promotionLevelGroup.valArray[i];
				if (mapValue.inputQuantity > mapValue.maxQuantity) {
					var indexStr = mapValue.indexStr.trim().replace(/(?=,\s?$).*/g, "");
					out.errMsg = "Tổng số lượng sản phẩm khuyến mãi ở dòng " + indexStr + " ở danh sách sản phẩm khuyến mãi đơn hàng vượt quá số lượng khuyến mãi tối đa được nhận.";
					return true;
				}
			}
		}
		return false;
	},
	
	/**
	 * Tao don hang
	 * @returns {Boolean}
	 */
	createOrder:function(message){
		$('.ErrorMsgStyle').html('').hide();
		var disabled = $('#payment').attr('disabled');
		if(disabled!='disabled' && SPCreateOrder._isChangeOnSaleGrid){
			return false;
		}
		$('#serverErrors').hide();
		var msg ='';
		msg = Utils.getMessageOfRequireCheck('customerCode', 'Mã khách hàng');
		if(msg.length==0){
			msg = Utils.getMessageOfRequireCheck('staffSaleCode', 'Nhân viên bán hàng', true);
		}
		if(msg.length==0) {
			msg = SPCreateOrder.checkOrderDate();
		}
		if(msg.length == 0 && $('#deliveryDate').val()!=null && $('#deliveryDate').val().length > 0){
			msg = Utils.getMessageOfInvalidFormatDate('deliveryDate', 'Ngày giao');
		}
		if(msg.length==0){
			msg = SPCreateOrder.checkOrderDate();
		}
		var deliveryDate = $('#deliveryDate').val().trim();
		var curDate = $('#currentLockDay').val().trim();
		if(msg.length == 0 && deliveryDate.length > 0){
			if (!Utils.compareDate(curDate,deliveryDate)) {
				$('#generalInfoDiv #GeneralInforErrors').html('Ngày giao không được nhỏ hơn ngày chốt').show();
				window.scrollTo(0,0);
				return false;
			}
		}
		//Kiểm tra KM ZV 21 đơn hàng có dòng SP nào không 
		//nếu chỉ có dòng header ZV21 mà ko có SP (do ko có trong kho) thì sẽ báo lỗi
		var rows = $('#gridSaleOrderPromotion').datagrid('getRows');
		if (msg.length == 0 && rows.length > 0) {
			if (rows[0].type == 1 && rows.length < 2) {// KM Hàng (ZV 21) && chỉ có 1 dòng header
				msg = 'Lỗi không lưu được đơn hàng khi KM đơn hàng không có sản phẩm nào.';
			}
		}
		//------------------------------------------------------------
		if(msg.length>0){
			$('#generalInfoDiv #GeneralInforErrors').html(msg).show();
			window.scrollTo(0,0);
			return false;
		}		
		var data = new Object();
		//trungtm6 comment
//		data.shopCode = $('#shopCode').val().trim();
		data.shopCodeFilter = $('#shopCode').val().trim();
		if ($('#shopCodeCB').length > 0){
			var shopCodeFilter = $('#shopCodeCB').combobox('getValue').trim();
			data.shopCodeFilter = shopCodeFilter;
		}
		
		data.orderDate = $('#orderDate').val().trim();
		data.customerCode = $('#customerCode').val().trim();
		data.carId = $('#carId').combobox('getValue').trim();
		data.deliveryCode = $('#transferStaffCode').combobox('getValue').trim();
		data.cashierCode = $('#cashierStaffCode').combobox('getValue').trim();
		data.salerCode = $('#staffSaleCode').combobox('getValue').trim();
		data.deliveryDate = $('#deliveryDate').val().trim();
		
		if($('#priorityId').val()!=null) {
			data.priorityId = $('#priorityId').val().trim();
		}
		if (SPAdjustmentOrder._orderId != null && SPAdjustmentOrder._orderId != undefined) {
			data.orderId = SPAdjustmentOrder._orderId;
		}
		var mapProductQuantity = new Map();
		var mapProductQuantityPackage = new Map();
		var mapProductQuantityRetail = new Map();
		var mapProductWarehouse = new Map();
		var mapProductWarehouseQuantity = new Map();
		var mapProductWarehouseQuantityPackage = new Map();
		var mapProductWarehouseQuantityRetail = new Map();
		var mapProductDiscountAmount = new Map();
		var quantityTotal = null;
		var quantityPackageTotal = null;
		var quantityRetailTotal = null;
		var quantityRow = null;
		var quantityPackageRow = null;
		var quantityRetailRow = null;
		var warehouseStr = null;
		var productWarehouseQuantStr = null;
		var lstPriceValue = [];//trungtm6 luu gia tren giao dien
		var lstpkPriceValue = [];//trungtm6 luu gia tren giao dien
		var tmp = $('#gridSaleData').handsontable('getInstance');
		var $gridPromotionProduct = $('#gridPromotionProduct');
		var row = tmp.getData();
		var hasManualPromotion = false;
		//PRO_CODE: 0, PRO_NAME: 1, PRICE: 2, WAREHOUSE: 3, STOCK: 4, QUANTITY: 5, AMOUNT: 6, DISCOUNT: 7, PROGRAM: 8, DELETE: 9
		for (var index=0;index<row.length;index++) {
			quantityTotal = 0;
			quantityPackageTotal = 0;
			quantityRetailTotal = 0;
			quantityRow = 0;
			quantityPackageRow = 0;
			quantityRetailRow = 0;
			var obj = SPCreateOrder.getObjectSaleProductInGrid(row[index],index);
			if (!isNullOrEmpty(obj.productCode)) {
				if (isNullOrEmpty(obj.quantity)) {
					$('#generalInfoDiv #GeneralInforErrors').html('Vui lòng nhập thực đặt cho sản phẩm ' + Utils.XSSEncode(obj.productCode + ' - ' + obj.productName)).show();
					tmp.selectCell(index,COLSALE.QUANTITY);
					window.scrollTo(0,0);
					return false;
				}
				var tmpKey = Utils.XSSEncode(obj.productCode + '_' + obj.commercialSupport);
				if (mapProductQuantity.get(tmpKey)==null || mapProductQuantity.get(tmpKey) == undefined ) {
					warehouseStr = "";
					productWarehouseQuantStr = "";
					var product = SPCreateOrder.findProductCodeInMap(obj.productCode);
					if (product != null && product.promotionProgramCode != null) {
						if (isNullOrEmpty(obj.commercialSupport)) {
							obj.commercialSupport = product.promotionProgramCode;
							obj.commercialSupportType = 'ZV';
						}
					}
//					//Luu tong so luong thuc dat cua tung san pham vao map trong truong hop tach dong SP theo Kho
					var whMap = new Map();
					for (var j=0;j<row.length;j++) {
						if (obj.productCode == row[j][COLSALE.PRO_CODE] && obj.commercialSupport == row[j][COLSALE.PROGRAM]) {
							if (!whMap.get(GridSaleBusiness._mapWarehouseIdSelect.get(j))){
								whMap.put(GridSaleBusiness._mapWarehouseIdSelect.get(j), {
									quantity: 0,
									quantityPackage: 0,
									quantityRetail: 0,
									discountAmount: 0
								});
							}
							quantityRow = getQuantity(row[j][COLSALE.QUANTITY], product.convfact);
							quantityTotal += quantityRow;
							var quaArr = row[j][COLSALE.QUANTITY].split('/');
							if (quaArr.length > 1) {
								quantityPackageRow = Number(quaArr[0]);
								quantityPackageTotal += quantityPackageRow;
								quantityRetailRow = Number(quaArr[1]);
								quantityRetailTotal += quantityRetailRow;
							} else {
								quantityRetailRow = Number(quaArr[0])
								quantityRetailTotal += quantityRetailRow;
							}
							var quantity = whMap.get(GridSaleBusiness._mapWarehouseIdSelect.get(j)).quantity + quantityRow;
							var quantityPackage = whMap.get(GridSaleBusiness._mapWarehouseIdSelect.get(j)).quantityPackage + quantityPackageRow;
							var quantityRetail = whMap.get(GridSaleBusiness._mapWarehouseIdSelect.get(j)).quantityRetail + quantityRetailRow;
							//whMap.put(GridSaleBusiness._mapWarehouseIdSelect.get(j), quantity);
							whMap.get(GridSaleBusiness._mapWarehouseIdSelect.get(j)).quantity = quantity;
							whMap.get(GridSaleBusiness._mapWarehouseIdSelect.get(j)).quantityPackage = quantityPackage;
							whMap.get(GridSaleBusiness._mapWarehouseIdSelect.get(j)).quantityRetail = quantityRetail;
							whMap.get(GridSaleBusiness._mapWarehouseIdSelect.get(j)).discountAmount += Number((row[j][COLSALE.DISCOUNT] || "").toString().replace(/,/g, ""));
						};
					}
					warehouseStr = whMap.keyArray.reduce(function(prevVal, currVal){
									return prevVal + ',' + currVal;
								});
					var productWarehouseQtyAmt = whMap.valArray.reduce(function(prevVal, currVal){
												var quantity = prevVal.quantity + ',' + currVal.quantity;
												var quantityPackage = prevVal.quantityPackage + ',' + currVal.quantityPackage;
												var quantityRetail = prevVal.quantityRetail + ',' + currVal.quantityRetail;
												var discountAmount = prevVal.discountAmount + ',' + currVal.discountAmount;
												return {
													quantity: quantity,
													quantityPackage: quantityPackage,
													quantityRetail: quantityRetail,
													discountAmount: discountAmount
												};
											});
					if (!isNullOrEmpty(warehouseStr)) {
						mapProductQuantity.put(tmpKey,quantityTotal);
						mapProductQuantityPackage.put(tmpKey,quantityPackageTotal);
						mapProductQuantityRetail.put(tmpKey,quantityRetailTotal);
						mapProductWarehouseQuantity.put(tmpKey, productWarehouseQtyAmt.quantity);
						mapProductWarehouseQuantityPackage.put(tmpKey, productWarehouseQtyAmt.quantityPackage);
						mapProductWarehouseQuantityRetail.put(tmpKey, productWarehouseQtyAmt.quantityRetail);
						mapProductWarehouse.put(tmpKey,warehouseStr);
						mapProductDiscountAmount.put(tmpKey, productWarehouseQtyAmt.discountAmount);
					}
				};
				var pCode1 = tmp.getDataAtCell(index, COLSALE.PROGRAM);
				if (pCode1 && GridSaleBusiness.mapCS.get(pCode1)) {
					hasManualPromotion = true;
				}
			}
		}
		var 
			//Cac list luu danh sach sale_order_detail
			lstCommercialSupport = new Array(),       //List ctrinh HTTM
			lstCommercialSupportType = new Array(),   //List loai ctring HTTM
			lstProduct = new Array(),                 //list SP ban
			lstQuantity = new Array(),                //list So luong thuc dat
			lstQuantityPackage = new Array(),		  //list So luong thung
			lstQuantityRetail = new Array(),		  //list So luong le
			lstSaleProductDiscountAmount = new Array(),
			lstSaleProductDiscountPercent = new Array(),
			lstBuyProductCommercialSupportProgram = new Array(), //list ma CTKM hoac CTHTTM luu vao dong sp ban
			lstBuyProductCommercialSupportProgramType = new Array(),//list loai CTKM hoac CTHTTM luu vao dong sp ban	
			lstProductHasPromotion = new Array(),
			//Cac list luu danh sach sale_order_lot
			lstWarehouseSaleProduct = new Array(),//list SP an theo Kho
			lstWarehouseId = new Array(),//list Kho cua tung ma SP
			lstWarehouseQuantity = new Array();//list So luong thuc dat theo Kho
			lstWarehouseQuantityPackage = new Array();
			lstWarehouseQuantityRetail = new Array();
			lstPromoLevelForOrder = new Array();
		var processedRows = new Array();
		/*
		 * Map<product_code, total_quantity>
		 * tong so luong (dat hang, khuyen mai) cua san pham.
		 * =>  Dung de validate xem tong so luong ban + so luong KM cua san pham co vuot qua ton kho hay ko
		 */
		var totalQuanityOfProductsInCart = new Map();
		for(var i=0;i<row.length;++i){
			var obj = SPCreateOrder.getObjectSaleProductInGrid(row[i],i);
			var product = SPCreateOrder.findProductCodeInMap(obj.productCode);
			var check_lot = obj.checkLot==1 || obj.checkLot=='1'? true:false;
			var tmpKey = obj.productCode + '_' + obj.commercialSupport;
			if(product != null && product.productId != null) {
				//Kiem tra check trung maSP thi chi luu mot dong trong truong hop tach dong SP theo Kho
				//Chu y ko lay so luong thuc dat tu trong grid ( do phai luu tong so thuc dat cua tat ca dong trung ma SP )
				//ma lay tu map luu o tren 
				//if ($.inArray(obj.productCode, lstProduct) < 0) {
				if ($.inArray(tmpKey, processedRows) < 0) {
					processedRows.push(tmpKey);
					if(obj.commercialSupport == undefined || obj.commercialSupport==null){
						lstBuyProductCommercialSupportProgram.push('');
					}else{
						lstBuyProductCommercialSupportProgram.push(obj.commercialSupport);
					}
					if (obj.commercialSupportType != undefined 
							&& (obj.commercialSupportType == 'ZV' || obj.commercialSupportType.indexOf('ZV') != -1)) {
						lstBuyProductCommercialSupportProgramType.push('ZV');
						if(obj.commercialSupport==undefined || obj.commercialSupport == null){
							lstBuyProductCommercialSupportProgram.push('');
						}
					} else {
						var tmp = GridSaleBusiness.mapCS.get(obj.commercialSupport);
						if (tmp != undefined && tmp != null) {
							lstBuyProductCommercialSupportProgramType.push(tmp.type);
						} else {
							lstBuyProductCommercialSupportProgramType.push('');
						}
					}
					lstProduct.push(obj.productCode);
					//thong tin gia thung gia le
					lstpkPriceValue.push(obj.pkPrice);
					lstPriceValue.push(obj.price);
					if(obj.commercialSupport == undefined || obj.commercialSupport == null){
						obj.commercialSupport = '';
					}
					if(obj.commercialSupportType==undefined || obj.commercialSupportType == null){
						obj.commercialSupportType = '';
					}
					if(obj.commercialSupportType != 'ZV' && obj.commercialSupportType != '') {
						lstCommercialSupport.push(obj.commercialSupport);
						lstCommercialSupportType.push(obj.commercialSupportType);
					} else if(obj.commercialSupport != '' && obj.commercialSupportType == '') {
						var program = GridSaleBusiness.mapCS.get(obj.commercialSupport);
						if (program == null || program == undefined) {
							$('#serverErrors').html('CTHTTM ' +Utils.XSSEncode(obj.commercialSupport)+' không tồn tại. Vui lòng chọn lại').show();
							var dataGrid = $('#gridSaleData').handsontable('getInstance');
							dataGrid.selectCell(i, COLSALE.PROGRAM);
							return false;
						}
						lstCommercialSupport.push(obj.commercialSupport);
						lstCommercialSupportType.push('');
					} else {
						lstCommercialSupport.push('');
						lstCommercialSupportType.push('');
					}
					
					var quantity = mapProductQuantity.get(tmpKey);
					var quantityPackage = mapProductQuantityPackage.get(tmpKey);
					var quantityRetail = mapProductQuantityRetail.get(tmpKey);
					lstQuantity.push(Number(quantity));
					lstQuantityPackage.push(Number(quantityPackage));
					lstQuantityRetail.push(Number(quantityRetail));

					//Luu cac thong tin tren don hang theo Kho de luu vao sale_order_lot
					//Khong can check trung ma SP
					var warehouse = mapProductWarehouse.get(tmpKey);
					var warehouseQuantity = mapProductWarehouseQuantity.get(tmpKey);
					var warehouseQuantityPackage = mapProductWarehouseQuantityPackage.get(tmpKey);
					var warehouseQuantityRetail = mapProductWarehouseQuantityRetail.get(tmpKey);
					if (!isNullOrEmpty(warehouse)) {
						lstWarehouseId.push(warehouse);
						lstWarehouseQuantity.push(warehouseQuantity);
						lstWarehouseQuantityPackage.push(warehouseQuantityPackage);
						lstWarehouseQuantityRetail.push(warehouseQuantityRetail);
						lstSaleProductDiscountAmount.push(mapProductDiscountAmount.get(tmpKey));
					} else {
						lstWarehouseId.push('');
						lstWarehouseQuantity.push('');
						lstWarehouseQuantityPackage.push('');
						lstWarehouseQuantityRetail.push('');
						lstSaleProductDiscountAmount.push('');
					}
					if (SPCreateOrder._mapHasPromotion.get(obj.productCode)) {
						lstProductHasPromotion.push(SPCreateOrder._mapHasPromotion.get(obj.productCode));
					} else {
						if (obj.commercialSupport) {
							var isManualPromotionProgram = GridSaleBusiness.listCS.some(function(cs) {
								return cs.code == obj.commercialSupport;
							});
							if (isManualPromotionProgram) {
								lstProductHasPromotion.push(1);
							} else {
								lstProductHasPromotion.push(0);
							}
						} else {
							lstProductHasPromotion.push(0);
						}						
					}
					//lstProductHasPromotion.push(1);
				} else {
					/*
					 * [Patch luong xu ly]
					 * Muc dich: lay tien chiet khau theo nhom (san pham, kho)
					 * Nguyen nhan: theo cach xu ly o tren, cac san pham da xu ly 1 lan se ko xu ly lai nua (ko quan tam nhieu kho),
					 * nen tien chiet khau lay theo nhom (san pham, kho)
					 */
				}
				/*
				 * put temp data for validate later
				 */
				if (!totalQuanityOfProductsInCart.get(obj.productCode)) {
					totalQuanityOfProductsInCart.put(obj.productCode, {
						quantity: 0,
						warehouses: new Map()	// quantity in each warehouse
					});
				}
				var productInfoInCart = totalQuanityOfProductsInCart.get(obj.productCode);
				productInfoInCart.quantity += Number(obj.quantity);
				// get warehouse_id of warehouse on row
				var tmpWhId = Number(GridSaleBusiness._mapWarehouseIdSelect.get(i));
				if (!productInfoInCart.warehouses.get(tmpWhId)) {
					productInfoInCart.warehouses.put(tmpWhId, 0);
				}
				productInfoInCart.warehouses.put(tmpWhId, productInfoInCart.warehouses.get(tmpWhId) + Number(obj.quantity));
				totalQuanityOfProductsInCart.put(obj.productCode, productInfoInCart);
				
				//Truong hop co ton tai sp huy
				//Chua xu ly
				if( check_lot && obj.commercialSupportType=='ZH'){
					
				}
				//Truong hop sdung lo
				//Chua xu ly
				else if(check_lot){
					
				}
				//put vao list truyen xuong Action ( hien tai chua khai bao list )
				
			} else {
				if (isNullOrEmpty(obj.productId) && !isNullOrEmpty(obj.commercialSupport)) {
					$('#serverErrors').html('Sản phẩm không tồn tại. Vui lòng chọn lại ').show();
					$('#saleProductGrid').datagrid('selectRow',i).datagrid('beginEdit',i);
					$('.datagrid-row-editing td[field=productId] input').focus();
					return false;
				}
			}
		}
		if(lstProduct.length==0){
			$('#serverErrors').html('Không tồn tại sản phẩm nào trong đơn hàng').show();
			return false;
		}
		if(lstQuantity.length==0){
			$('#serverErrors').html('Vui lòng nhập đúng thực đặt sản phẩm').show();
			return false;
		}

		var isError = false;
		$("[id^=portion_promotion_value_]").each(function(){
			var v = $(this).val();
			if (isNaN(v)) {
				isError = true;
				return;
			}
		});
		if (!isError) {
			$("[id^=op_portion_promotion_value_]").each(function(){
				var v = $(this).val();
				if (isNaN(v)) {
					isError = true;
					return;
				}
			});
		}
		if (isError == true) {
			$('#serverErrors').html('Số suất nhập vào không đúng định dạng số.').show();
			return false;
		}

		//trungtm6 comment
		if(lstCommercialSupport.length>0){
			data.lstCommercialSupport = lstCommercialSupport;
		}
		if(lstCommercialSupportType.length>0){
			data.lstCommercialSupportType = lstCommercialSupportType;
		}
		if(lstSaleProductDiscountAmount.length>0){
			data.lstSaleProductDiscountAmount = lstSaleProductDiscountAmount;
		}
		if(lstSaleProductDiscountPercent.length>0){
			data.lstSaleProductDiscountPercent = lstSaleProductDiscountPercent;
		}
		if (lstProductHasPromotion.length > 0) {
			data.lstProductHasPromotion = lstProductHasPromotion;
		}
		//Luu danh sach sale_order_detail
		data.lstProduct=lstProduct;
		data.lstQuantity=lstQuantity;
		data.lstQuantityPackage=lstQuantityPackage;
		data.lstQuantityRetail=lstQuantityRetail;
		data.lstBuyProductCommercialSupportProgram = lstBuyProductCommercialSupportProgram;
		data.lstBuyProductCommercialSupportProgramType = lstBuyProductCommercialSupportProgramType;
		//Luu danh sach sale_order_lot
		data.lstWarehouseId = lstWarehouseId;
		data.lstWarehouseSaleProduct = lstWarehouseSaleProduct;
		data.lstWarehouseQuantity = lstWarehouseQuantity;
		data.lstWarehouseQuantityPackage = lstWarehouseQuantityPackage;
		data.lstWarehouseQuantityRetail = lstWarehouseQuantityRetail;
		
		//Luu danh sach gia
		data.lstpkPriceValue = lstpkPriceValue;
		data.lstPriceValue = lstPriceValue;
		
		//Luu danh sach so suat cua CTKM cho toan bo don hang
		data.isUsingZVOrder = SPCreateOrder._isUsingZVOrder;
		data.isUsingZVCode = SPCreateOrder._isUsingZVCode;
		var saleOrderQuantityReceivedList = SPCreateOrder._saleOrderQuantityReceivedList;
		if(saleOrderQuantityReceivedList!=undefined && saleOrderQuantityReceivedList!=null){
			var quantityReceivedList = new Array();
			var isError = false;
			$.each(saleOrderQuantityReceivedList,function(i,e){
				var obj = new Object();
				obj.promotionId = e.promotionProgram.id;
				obj.groupId = e.productGroup ? e.productGroup.id : e.productGroupId;
				obj.levelId = e.groupLevel ? e.groupLevel.id : e.groupLevelId;
				obj.quantityReceived = e.quantityReceived;
				obj.maxQuantityReceived = e.maxQuantityReceived;
				obj.maxQuantityReceivedTotal = e.maxQuantityReceivedTotal;
				// begin vuongmq; 08/01/2015; them cac bien theo xu ly save: sale_order_promotion
				obj.maxAmountReceivedTotal = e.maxAmountReceivedTotal;
				obj.exceedObject = e.exceedObject;
				obj.exceedUnit = e.exceedUnit;
				// end vuongmq; 08/01/2015; them cac bien theo xu ly save: sale_order_promotion
				obj.promotionLevel = e.promotionLevel;
				obj.productGroupId = e.productGroupId;
				obj.promotionCode = e.promotionProgram.promotionProgramCode;
				if (e.promotionProgram.type == 'ZV19' || e.promotionProgram.type == 'ZV20' || e.promotionProgram.type == 'ZV21') {
					if (e.promotionProgram.promotionProgramCode == SPCreateOrder._isUsingZVCode) {
						quantityReceivedList.push(obj);
					}
				} else {
					quantityReceivedList.push(obj);
				}
				if (obj.quantityReceived > obj.maxQuantityReceived) {
					$('#serverErrors').html('Số suất của CTKM ' +Utils.XSSEncode(e.promotionProgram.promotionProgramCode)+' vượt quá số suất được hưởng. Vui lòng nhập số suất nhỏ hơn hoặc bằng '+ obj.maxQuantityReceived).show();
					isError = true;
					return;
				}
			});
			if (isError) {
				return false;
			}
			convertToSimpleObject(data,quantityReceivedList, 'quantityReceivedList');
		}
		var lstCTTL = SPCreateOrder.getAccumulative2Save();
		if(lstCTTL == -1){
			return;
		}
		//kiem tra so luong để loại bỏ các lần đạt = 0 hết
		if(lstCTTL!=null && lstCTTL.valArray.length>0){
			for(var i = 0 ; i < lstCTTL.valArray.length ; i++){
				if(lstCTTL.valArray[i].promoProducts != undefined && lstCTTL.valArray[i].promoProducts != null){
					var isExistsProduct = false; 
					for(var j = 0 ; j < lstCTTL.valArray[i].promoProducts.length ; j++){
						if(lstCTTL.valArray[i].promoProducts[j].value > 0){
							isExistsProduct = true;
						}
					}
					if(!isExistsProduct){
						lstCTTL.remove(lstCTTL.keyArray[i]);
						i--;
					}
				}
			}
		}
		
		//kiem tra canh bao so luong tung lan dat
		if(SPCreateOrder._confirmQuantityAcc  && lstCTTL!=null && lstCTTL.valArray.length>0){
			var lstAcc = lstCTTL.valArray;
			for(var i = 0 ; i < lstAcc.length ; i++){
				if(lstAcc[i].valueType == 1){// KM SP thi moi xet
					var maxQuantityAcc = 0;
					var valueAcc = 0;
					for(var j = 0 ; j < lstAcc[i].promoProducts.length ; j++){
						valueAcc += lstAcc[i].promoProducts[j].valueActualy;
						maxQuantityAcc += lstAcc[i].promoProducts[j].maxQuantity;
					}
					if(maxQuantityAcc != valueAcc){
						$.messager.confirm(jsp_common_xacnhan,'Khuyến mãi tích lũy sản phẩm không đủ tồn kho. Bạn có muốn tiếp tục tạo đơn hàng ?.',function(r){  
							if(r){
								SPCreateOrder._confirmQuantityAcc = false;
								setTimeout(function(){SPCreateOrder.createOrder();},200);
								return ;
							}
						});	
						return;
					}
				}
			}
		}else{
			SPCreateOrder._confirmQuantityAcc = true;
		}
		convertToSimpleObject(data,lstCTTL.valArray,'lstCTTL');
		//Luu danh sach sale order promo detail cho don hang
		var saleOrderPromoDetail = SPCreateOrder._mapPromoDetail;
		if(saleOrderPromoDetail!=undefined && saleOrderPromoDetail!=null){
			var lstSOPromoDetail = new Array();
			for (var i=0;i<saleOrderPromoDetail.size();i++) {
				var saleOrderPromoDetailList = SPCreateOrder._mapPromoDetail.valArray[i];
				if(saleOrderPromoDetailList!=undefined && saleOrderPromoDetailList!=null){
					$.each(saleOrderPromoDetailList,function(j,e){
						var obj = new Object();
						obj.discountAmount = e.discountAmount;
						obj.discountPercent = e.discountPercent;
						obj.isOwner = e.isOwner;
						obj.maxAmountFree = e.maxAmountFree;
						obj.programCode = e.programCode;
						obj.programType = e.programType;
						obj.programTypeCode = e.programeTypeCode;
						obj.productGroupId = e.productGroupId;
						obj.groupLevelId =  e.groupLevel ? e.groupLevel.id : e.groupLevelId;
						obj.productId = SPCreateOrder._mapPromoDetail.keyArray[i];
						if (e.isOwner == 2) {
							if (e.programCode == SPCreateOrder._isUsingZVCode || e.programeTypeCode == 'ZV24') {
								lstSOPromoDetail.push(obj);
							}
						} else {
							lstSOPromoDetail.push(obj);
						}
						
					});			
				};
			}
			convertToSimpleObject(data,lstSOPromoDetail,'lstSOPromoDetail');
		}
		
		var lstPromotionCode = new Array();		
		var lstPromotionProduct = new Array();
		var lstDiscount = new Array();
		var lstDisper = new Array();
		var lstPromotionType = new Array();
		var lstPromotionProductQuatity = new Array();
		var lstPromotionProductQuatityPackage = new Array();
		var lstPromotionProductQuatityRetail = new Array();
		var lstPromotionProductMaxQuatity = new Array(); 
		var lstPromotionProductMaxQuatityCk = new Array();
		//Cac list luu danh sach sale_order_lot
		var lstPromoWarehouseId = new Array(),//list Kho cua tung ma SP
			lstPromoWarehouseQuantity = new Array(),//list So luong thuc dat theo Kho
			lstPromoLevel = new Array();	// list promo level
		//var lstProductGroupAndLevelGroupId = new Array();//list product group va level group cua SPKM
		var mapProductGroupAndLevelGroupId = new Map();
		var lstMyTime = new Array();
		var mapPromoLevel = new Map();
		var mapProductPromoQuantity = new Map();
		var mapProductPromoQuantityPackage = new Map();
		var mapProductPromoQuantityRetail = new Map();
		var mapProductPromoWarehouse = new Map();
		var mapProductPromoWarehouseQuantity = new Map();
		var quantityTotal = null;
		var quantityPackageTotal = null;
		var quantityRetailTotal = null;
		var warehouseStr = null;
		var productWarehouseQuantStr = null;
		var promoLevelStr = null;

		// validate so luong KM toi da
		var outInfo = {};
		if (SPCreateOrder.checkPromotiveQuantityReceivedExceedQuota(outInfo)) {
			$('#serverErrors').html(outInfo.errMsg).show();
			setTimeout(function() {
				$('#serverErrors').html('').hide();
			}, 5000);			
			return false;
		}
		
		/*
		 * re-create temp promotion map
		 */
		SPCreateOrder._lstPromotionProductIsAuto.clear();
		var tempMap = new Map();
		$gridPromotionProduct.datagrid('getRows').forEach(function(item, index){
	         SPCreateOrder._lstPromotionProductIsAuto.put(index, item);
	        tempMap.put(item.productCode, '');
		});
		tempMap = null;

		var len = SPCreateOrder._lstPromotionProductIsAuto.size();
		var lenForOrder = SPCreateOrder._lstPromotionProductForOrderIsAuto.size();
	
		for (var index=0; index<len; index++) {
			quantityTotal = 0;
			quantityPackageTotal = 0;
			quantityRetailTotal = 0;
			var obj = SPCreateOrder._lstPromotionProductIsAuto.get(SPCreateOrder._lstPromotionProductIsAuto.keyArray[index]);
			if (!isNullOrEmpty(obj.productCode)) {
				//_sid = '-1413624577692';
				var mapKey = Utils.XSSEncode(obj.productCode + '_' + obj.programCode + '_' + obj.productGroupId + '_' + obj.levelPromo + '_' + obj.changeProduct);
				if (mapProductPromoQuantity.get(mapKey)==null || mapProductPromoQuantity.get(mapKey) == undefined ) {

					warehouseStr = "";
					productWarehouseQuantStr = "";
					promoLevelStr = "";
					
					//Tach dong sp KM theo kho
					for (var l = 0; l < len; l++) {
						var product = SPCreateOrder.findProductCodeInMap(SPCreateOrder._lstPromotionProductIsAuto.valArray[l].productCode);
						var lRowObj = SPCreateOrder._lstPromotionProductIsAuto.get(SPCreateOrder._lstPromotionProductIsAuto.keyArray[l]);
						//_sid = '-1413624577692';
						var indexKey = Utils.XSSEncode(obj.productCode + '_' + obj.programCode + '_' + obj.productGroupId + '_' + obj.levelPromo + '_'+ obj.changeProduct),
							lRowKey = Utils.XSSEncode(lRowObj.productCode + '_' + lRowObj.programCode + '_' + lRowObj.productGroupId + '_' + lRowObj.levelPromo + '_' + lRowObj.changeProduct);
						if (indexKey == lRowKey) {
							if ((lRowObj.warehouse == null || lRowObj.warehouse == undefined) && lRowObj.quantity > 0) {
								$('#serverErrors').html("Sản phẩm khuyến mãi " + Utils.XSSEncode(lRowObj.productCode) + " không có kho.").show();
								return false;
							}
							if (lRowObj.warehouse == null || lRowObj.warehouse == undefined) {
								warehouseStr += "0,";
								productWarehouseQuantStr += "0," ;
								promoLevelStr += lRowObj.levelPromo + ",";
							} else {
								warehouseStr += (lRowObj.warehouse.warehouseId || lRowObj.warehouse.id) + ",";
								productWarehouseQuantStr += getQuantity(lRowObj.quantity, product.convfact) + "," ;
								quantityTotal += getQuantity(lRowObj.quantity,product.convfact);
								var quaArr = ('' + lRowObj.quantity).split('/');
								if (quaArr.length > 1) {
									quantityPackageTotal += Number(quaArr[0]);
									quantityRetailTotal += Number(quaArr[1]);
								} else {
									quantityRetailTotal += Number(quaArr[0]);
								}
								promoLevelStr += lRowObj.levelPromo + ",";
							}
						}
					}
					if (!isNullOrEmpty(warehouseStr)) {
						mapProductPromoQuantity.put(mapKey,quantityTotal);
						mapProductPromoQuantityPackage.put(mapKey,quantityPackageTotal);
						mapProductPromoQuantityRetail.put(mapKey,quantityRetailTotal);
						mapProductPromoWarehouseQuantity.put(mapKey,productWarehouseQuantStr);
						mapProductPromoWarehouse.put(mapKey,warehouseStr);
						mapPromoLevel.put(mapKey, promoLevelStr);
					}
				}
			}
		}
		// khuyen mai mo moi
		var kq = OpenProductPromotion.processSaleOrderLot(mapProductPromoQuantity, mapProductPromoWarehouseQuantity,
															mapProductPromoWarehouse, mapPromoLevel);
		/*mapProductPromoQuantity = kq.mapProductPromoQuantity;
		mapProductPromoWarehouseQuantity = kq.mapProductPromoWarehouseQuantity;
		mapProductPromoWarehouse = kq.mapProductPromoWarehouse;
		mapPromoLevel = kq.mapPromoLevel;*/
		if (kq.msg != undefined && kq.msg != null && kq.msg.length > 0) {
			$('#serverErrors').html(kq.msg).show();
			return false;
		}
		// ---
		index = 0;
		for (index;index<lenForOrder;index++) {
			quantityTotal = 0;
			quantityPackageTotal = 0;
			quantityRetailTotal = 0;
			var obj = SPCreateOrder._lstPromotionProductForOrderIsAuto.get(SPCreateOrder._lstPromotionProductForOrderIsAuto.keyArray[index]);	
			if (!isNullOrEmpty(obj.productCode)) {
				//_sid = '-1413624577692';
				var mapKey = Utils.XSSEncode(obj.productCode + '_' + obj.programCode + '_' + obj.productGroupId + '_' + obj.levelPromo + '_' + obj.changeProduct);
				if (mapProductPromoQuantity.get(mapKey)==null || mapProductPromoQuantity.get(mapKey) == undefined ) {
					warehouseStr = "";
					productWarehouseQuantStr = "";
					promoLevelStr = "";

					//Tach dong sp KM theo kho
					for (var l=0;l<lenForOrder;l++) {

						var temp = SPCreateOrder._lstPromotionProductForOrderIsAuto.get(SPCreateOrder._lstPromotionProductForOrderIsAuto.keyArray[l]);	
						
						if(temp!=null && temp.promotionType==PromotionType.PROMOTION_FOR_ZV21){
							continue;
						} 
						if (temp.promotionType == PromotionType.PROMOTION_FOR_ORDER_TYPE_PRODUCT) {
							var product = SPCreateOrder.findProductCodeInMap(temp.productCode);
							var lRowObj = SPCreateOrder._lstPromotionProductForOrderIsAuto.get(SPCreateOrder._lstPromotionProductForOrderIsAuto.keyArray[l]);
							//_sid = '-1413624577692';
							var indexKey = Utils.XSSEncode(obj.productCode + '_' + obj.programCode + '_' + obj.productGroupId + '_' + obj.levelPromo + '_'+ obj.changeProduct),
								lRowKey = Utils.XSSEncode(lRowObj.productCode + '_' + lRowObj.programCode + '_' + lRowObj.productGroupId + '_' + lRowObj.levelPromo + '_' + lRowObj.changeProduct);

							if (indexKey == lRowKey) {
								if ((lRowObj.warehouse == null || lRowObj.warehouse == undefined) && lRowObj.quantity > 0) {
									$('#serverErrors').html("Sản phẩm khuyến mãi " + Utils.XSSEncode(lRowObj.productCode) + " không có kho.").show();
									return false;
								}
								if (lRowObj.warehouse == null || lRowObj.warehouse == undefined) {
									warehouseStr += "0,";
									productWarehouseQuantStr += "0," ;
									promoLevelStr += lRowObj.levelPromo + ",";
								} else {
									warehouseStr += (lRowObj.warehouse.warehouseId || lRowObj.warehouse.id) + ",";
									productWarehouseQuantStr += getQuantity(lRowObj.quantity, product.convfact) + "," ;
									quantityTotal += getQuantity(lRowObj.quantity, product.convfact);
									var quaArr = (''+lRowObj.quantity).split('/');
									if (quaArr.length > 1) {
										quantityPackageTotal += Number(quaArr[0]);
										quantityRetailTotal += Number(quaArr[1]);
									} else {
										quantityRetailTotal += Number(quaArr[0]);
									}
									promoLevelStr += lRowObj.levelPromo + ",";
								}
							}
						}
					}
					if (!isNullOrEmpty(warehouseStr)) {
						mapProductPromoQuantity.put(mapKey,quantityTotal);
						mapProductPromoQuantityPackage.put(mapKey,quantityPackageTotal);
						mapProductPromoQuantityRetail.put(mapKey,quantityRetailTotal);
						mapProductPromoWarehouseQuantity.put(mapKey,productWarehouseQuantStr);
						mapProductPromoWarehouse.put(mapKey,warehouseStr);
						mapPromoLevel.put(mapKey, promoLevelStr);
					}
				};
			}
		}
		tempMap = null; tempMap = new Map();
		for(var i=0; i < len; ++i){
			var temp = SPCreateOrder._lstPromotionProductIsAuto.get(SPCreateOrder._lstPromotionProductIsAuto.keyArray[i]);
			var isProductInMultiWarehouse = false;
			//_sid = '-1413624577692';
			var tempMapKey = Utils.XSSEncode(temp.productCode + '_' + temp.programCode + '_' + temp.productGroupId + '_' + temp.levelPromo + '_' + temp.changeProduct);
			if (tempMap.get(tempMapKey) !== null && tempMap.get(tempMapKey) !== undefined) {
				isProductInMultiWarehouse = true;
			} else {
				tempMap.put(tempMapKey, {
					totalQuantity: 0
				});
			}

			//Luu cac thong tin tren don hang theo Kho de luu vao sale_order_lot
			//Khong can check trung ma SP
			var warehouse = mapProductPromoWarehouse.get(tempMapKey);
			var warehouseQuantity = mapProductPromoWarehouseQuantity.get(tempMapKey);
			var promoLevel = mapPromoLevel.get(tempMapKey);
			if (!isNullOrEmpty(warehouse) && !isProductInMultiWarehouse) {
				lstPromoWarehouseId.push(warehouse);
				lstPromoWarehouseQuantity.push(warehouseQuantity);
				lstPromoLevel.push(promoLevel);
			}			
			/*
			 * promotion product's total quantity
			 */
			if (!isProductInMultiWarehouse) {
				var quantity = mapProductPromoQuantity.get(tempMapKey);
				var quantityPackage = mapProductPromoQuantityPackage.get(tempMapKey);
				var quantityRetail = mapProductPromoQuantityRetail.get(tempMapKey);
				lstPromotionProductQuatity.push(Number(quantity));
				lstPromotionProductQuatityPackage.push(Number(quantityPackage));
				lstPromotionProductQuatityRetail.push(Number(quantityRetail));
				lstPromotionCode.push(temp.programCode);			
				lstPromotionType.push(temp.type);
				lstDiscount.push(temp.discountAmount);
				lstDisper.push(temp.discountPercent);
				
				/*
				 * put temp data for validate later
				 */
				if (!totalQuanityOfProductsInCart.get(temp.productCode)) {
					totalQuanityOfProductsInCart.put(temp.productCode, {
						quantity: 0,
						warehouses: new Map()	// quantity in each warehouse
					});
				}
				var productInfoInCart = totalQuanityOfProductsInCart.get(temp.productCode);
				productInfoInCart.quantity += Number(temp.quantity);
				// get warehouse_id of warehouse on row
				var tmpWhId = 0;
				if (temp.warehouse != null && temp.warehouse != undefined) {
					tmpWhId = Number(temp.warehouse.warehouseId);
				}
				if (!productInfoInCart.warehouses.get(tmpWhId)) {
					productInfoInCart.warehouses.put(tmpWhId, 0);
				}
				productInfoInCart.warehouses.put(tmpWhId, productInfoInCart.warehouses.get(tmpWhId) + Number(temp.quantity));
				totalQuanityOfProductsInCart.put(temp.productCode, productInfoInCart);
			}
			
			if(temp.type == PromotionType.FREE_PRICE || temp.type == PromotionType.FREE_PERCENT) {
				var zvalue = $('#discountAmount_'+i).val();
				if(zvalue.length==0){
					msg = 'Bạn chưa nhập số tiền khuyến mãi. Vui lòng nhập giá trị.';
					$('#discountAmount_'+i).focus();
				}else{
					var maxDiscountAmount = Number(temp.maxDiscountAmount);
					var discountAmount = Number(temp.discountAmount);
					if(maxDiscountAmount<discountAmount){
						msg = 'Số tiền khuyến mãi vượt quá Số tiền khuyến mãi tối đa được hưởng. Vui lòng nhập lại.';
						$('#discountAmount_'+i).focus();
					}
				}	
				lstPromotionProduct.push('');
				lstPromotionProductMaxQuatity.push(temp.maxDiscountAmount);
				lstPromotionProductMaxQuatityCk.push(temp.maxDiscountAmount);
				lstMyTime.push((temp.myTime == null || temp.myTime == undefined) ? 0 : temp.myTime);
				//lstProductGroupAndLevelGroupId.push("");
				mapProductGroupAndLevelGroupId.put(tempMapKey, "");
			}else if(temp.type == PromotionType.FREE_PRODUCT) {				
				var zcvalue = $('#convact_promotion_value_'+i).val();
				var zvalue = getQuantity(zcvalue,temp.convfact);
				if(zcvalue.length==0){
					msg = 'Bạn chưa nhập số lượng sản phẩm khuyến mãi. Vui lòng nhập giá trị.';
					$('#convact_promotion_value_'+i).focus();
				}else if(isNaN(zvalue)){
					msg = 'Số lượng sản phẩm khuyến mãi không hợp lệ. Vui lòng nhập giá trị.';
					$('#convact_promotion_value_'+i).focus();
				}else{
					var maxQuantity = Number(temp.maxQuantity);				
					var qty = Number(temp.quantity);
					if(maxQuantity<qty){
						msg = 'Số lượng khuyến mãi vượt quá số lượng khuyến mãi tối đa được hưởng. Vui lòng nhập lại.';
						$('#convact_promotion_value_'+i).focus();
					}
				}
				if (!isProductInMultiWarehouse) {
					lstPromotionProduct.push(temp.productCode);
					//lstPromotionProductMaxQuatity.push(temp.maxQuantity);
					lstPromotionProductMaxQuatity.push(temp.maxQuantityFreeTotal); // vuongmq; 30/01/2016; truyen 1 gia tri tinh Km max
					lstPromotionProductMaxQuatityCk.push(temp.maxQuantityCk == undefined ? temp.maxQuantity : temp.maxQuantityCk);
					lstMyTime.push((temp.myTime == null || temp.myTime == undefined) ? 0 : temp.myTime);
				}
				/*lstProductGroupAndLevelGroupId.push((temp.productGroup ? temp.productGroup.id : temp.productGroupId)+"_"
					+(temp.groupLevel ? temp.groupLevel.id : temp.groupLevelId));*/
				mapProductGroupAndLevelGroupId.put(tempMapKey, (temp.productGroup ? temp.productGroup.id : temp.productGroupId)+"_"
					+(temp.groupLevel ? temp.groupLevel.id : temp.groupLevelId));
			}
			if(msg.length>0){				
				$('#serverErrors').html(msg).show();
				$('#gridPromotionProduct').datagrid('selectRow',i);
				return false;
			}
			//Truong hop co lo
			//Chua xu ly
		}
		// tempMap.valArray.forEach(function(item, index) {
		// 	lstPromotionProductQuatity.push(item.totalQuantity);
		// });

		// khuyen mai mo moi
		var kq1 = OpenProductPromotion.checkOpenProduct(tempMap, mapProductPromoQuantity, mapProductPromoWarehouse,
					mapProductPromoWarehouseQuantity, mapPromoLevel, totalQuanityOfProductsInCart);
		if (kq1.msg != undefined && kq1.msg != null && kq1.msg.length > 0) {
			$('#serverErrors').html(kq1.msg).show();
			$('#promotionGrid2').datagrid('selectRow', kq1.idx);
			return false;
		} else {
			lstPromoWarehouseId = lstPromoWarehouseId.concat(kq1.lstPromoWarehouseId);
			lstPromoWarehouseQuantity = lstPromoWarehouseQuantity.concat(kq1.lstPromoWarehouseQuantity);
			lstPromoLevel = lstPromoLevel.concat(kq1.lstPromoLevel);
			lstPromotionProductQuatity = lstPromotionProductQuatity.concat(kq1.lstPromotionProductQuatity);
			lstPromotionCode = lstPromotionCode.concat(kq1.lstPromotionCode);
			lstPromotionType = lstPromotionType.concat(kq1.lstPromotionType);
			lstDiscount = lstDiscount.concat(kq1.lstDiscount);
			lstDisper = lstDisper.concat(kq1.lstDisper);
			lstPromotionProduct = lstPromotionProduct.concat(kq1.lstPromotionProduct);
			lstPromotionProductMaxQuatity = lstPromotionProductMaxQuatity.concat(kq1.lstPromotionProductMaxQuatity);
			lstPromotionProductMaxQuatityCk = lstPromotionProductMaxQuatityCk.concat(kq1.lstPromotionProductMaxQuatityCk);
			lstMyTime = lstMyTime.concat(kq1.lstMyTime);
			//lstProductGroupAndLevelGroupId = lstProductGroupAndLevelGroupId.concat(kq1.lstProductGroupAndLevelGroupId);
			if (kq1.mapProductGroupAndLevelGroupId && kq1.mapProductGroupAndLevelGroupId.size() > 0) {
				for (var ij = 0, szj = kq1.mapProductGroupAndLevelGroupId.size(); ij < szj; ij++) {
					mapProductGroupAndLevelGroupId.put(kq1.mapProductGroupAndLevelGroupId.keyArray[ij], kq1.mapProductGroupAndLevelGroupId.valArray[ij])
				}
			}
			//totalQuanityOfProductsInCart = kq1.totalQuanityOfProductsInCart;
		}
		// --

		var processedRows = new Map();
		for(var i=0;i<lenForOrder;++i){			
			var temp = SPCreateOrder._lstPromotionProductForOrderIsAuto.get(SPCreateOrder._lstPromotionProductForOrderIsAuto.keyArray[i]);
				
			if(temp.promotionType==PromotionType.PROMOTION_FOR_ZV21){
				continue;
			}	

			var isProductInMultiWarehouse = false;
			var tempMapKey = Utils.XSSEncode(temp.productCode + '_' + temp.programCode + '_' + temp.productGroupId + '_' + temp.levelPromo + '_' + temp.changeProduct);
			if (tempMap.get(tempMapKey) !== null && tempMap.get(tempMapKey) !== undefined) {
				isProductInMultiWarehouse = true;
			} else {
				tempMap.put(tempMapKey, {
					totalQuantity: 0
				});
			}


			if (temp.promotionType == PromotionType.PROMOTION_FOR_ORDER_TYPE_PRODUCT) {
				mapProductGroupAndLevelGroupId.put(tempMapKey, (temp.productGroup ? temp.productGroup.id : temp.productGroupId)+"_"+
					(temp.groupLevel ? temp.groupLevel.id : temp.groupLevelId));
				//Luu cac thong tin tren don hang theo Kho de luu vao sale_order_lot
				//Khong can check trung ma SP
				//_sid = '-1413624577692';
				if (!isProductInMultiWarehouse) {
					var warehouse = mapProductPromoWarehouse.get(tempMapKey);
					var warehouseQuantity = mapProductPromoWarehouseQuantity.get(tempMapKey);
					var promoLevel = mapPromoLevel.get(tempMapKey);
					if (!isNullOrEmpty(warehouse)) {
						lstPromoWarehouseId.push(warehouse);
						lstPromoWarehouseQuantity.push(warehouseQuantity);
						lstPromoLevel.push(promoLevel);
					} else {
						lstPromoWarehouseId.push('');
						lstPromoWarehouseQuantity.push('');
						lstPromoLevel.push('');
					}
					var quantity = mapProductPromoQuantity.get(tempMapKey);
					var quantityPackage = mapProductPromoQuantityPackage.get(tempMapKey);
					var quantityRetail = mapProductPromoQuantityRetail.get(tempMapKey);
					lstPromotionProductQuatity.push(Number(quantity));
					lstPromotionProductQuatityPackage.push(Number(quantityPackage));
					lstPromotionProductQuatityRetail.push(Number(quantityRetail));
					lstPromotionCode.push(temp.programCode);			
					lstPromotionType.push(temp.type);
					lstDiscount.push(temp.discountAmount);
					lstDisper.push(temp.discountPercent);
				}
				
				/*
				 * put temp data for validate later
				 */
				if (!totalQuanityOfProductsInCart.get(temp.productCode)) {
					totalQuanityOfProductsInCart.put(temp.productCode, {
						quantity: 0,
						warehouses: new Map()	// quantity in each warehouse
					});
				}
				var productInfoInCart = totalQuanityOfProductsInCart.get(temp.productCode);
				productInfoInCart.quantity += Number(temp.quantity);
				// get warehouse_id of warehouse on row
				var tmpWhId = 0;
				if (temp.warehouse != null && temp.warehouse != undefined) {
					tmpWhId = Number(temp.warehouse.warehouseId);
				}
				if (!productInfoInCart.warehouses.get(tmpWhId)) {
					productInfoInCart.warehouses.put(tmpWhId, 0);
				}
				productInfoInCart.warehouses.put(tmpWhId, productInfoInCart.warehouses.get(tmpWhId) + Number(temp.quantity));
				totalQuanityOfProductsInCart.put(temp.productCode, productInfoInCart);
			} else {
				//lstProductGroupAndLevelGroupId.push("");
				mapProductGroupAndLevelGroupId.put(tempMapKey, "");
			}
			
			/*
			 * other
			 */
			if (!isProductInMultiWarehouse) {
				if(temp.type == PromotionType.FREE_PRICE || temp.type == PromotionType.FREE_PERCENT) {
					lstPromotionProduct.push('');
					lstPromotionProductMaxQuatity.push('');
					lstPromotionProductMaxQuatityCk.push('');
					lstPromoLevel.push('');
					lstMyTime.push((temp.myTime == null || temp.myTime == undefined) ? 0 : temp.myTime);
				}else if(temp.type == PromotionType.FREE_PRODUCT) {
					lstPromotionProduct.push(temp.productCode);
					//lstPromotionProductMaxQuatity.push(temp.maxQuantity);
					lstPromotionProductMaxQuatity.push(temp.maxQuantityFreeTotal); // vuongmq; 30/01/2016; truyen 1 gia tri tinh Km max
					lstPromotionProductMaxQuatityCk.push(temp.maxQuantityCk == undefined ? temp.maxQuantity : temp.maxQuantityCk);
					lstMyTime.push((temp.myTime == null || temp.myTime == undefined) ? 0 : temp.myTime);
				}
			}
			if(msg.length>0){	
				$('#serverErrors').html(msg).show();
				$('#gridSaleOrderPromotion').datagrid('selectRow', i);
				return false;
			}
			//Truong hop co lo
			//Chua xu ly
		}
		/*
		 * check if total product's quantity (buy-quantity and promotive quantity) exceed total product's quantity in warehouse
		 */
		//Kiểm tra trả thưởng keyShop có vượt quá mức trả hay không
		var msgKeyShop = SPCreateOrder.checkQuantityAmountKeyShop();
		if (msgKeyShop != '') {
			$('#serverErrors').html(msgKeyShop).show();
			setTimeout(function() {
				$('#serverErrors').html('').hide();
			}, 10000);
			return;
		}
		//Add thêm số lượng sản phẩm keyshop để check tồn kho tất cả
		var rows = $('#gridKeyShop').datagrid('getRows');
		for (var i = 0 ; i < rows.length ; i++) {
			if (rows[i].productId != null) {
				var quantityAmount = getQuantity($('#quantityAmount' + i).val(), rows[i].convfact);
				var obj = totalQuanityOfProductsInCart.get(rows[i].productCode);
				if (obj == null) {//nếu chưa có kho thì thêm vào
					totalQuanityOfProductsInCart.put(rows[i].productCode, {
						quantity: 0,
						warehouses: new Map()
					})
					obj = totalQuanityOfProductsInCart.get(rows[i].productCode);
				}
				obj.quantity += Number(quantityAmount);
				var wh = obj.warehouses.get(rows[i].warehouse.warehouseId);
				if (wh == null) {
					wh = 0;
				}
				wh += Number(quantityAmount);
				obj.warehouses.put(rows[i].warehouse.warehouseId, wh);
				totalQuanityOfProductsInCart.put(rows[i].productCode, obj);
			}
		}
		
		var errMsg = "";
		for (var idx = 0; idx < totalQuanityOfProductsInCart.keyArray.length; idx++) {
			var productCode = totalQuanityOfProductsInCart.keyArray[idx];
			var productInfoInCart = totalQuanityOfProductsInCart.get(productCode);
			var totalOrderedQuanity = productInfoInCart.quantity;
			var product = GridSaleBusiness.mapSaleProduct.get(productCode);
			if (product != null) {
				var warehousesOfProduct = GridSaleBusiness.getWarehouseByType(product.productId);
				
				//for  qua từng sp và kho để kiểm tra
				for (var i = 0 ; i < productInfoInCart.warehouses.keyArray.length ; i++) {
					var whId = productInfoInCart.warehouses.keyArray[i];
					for (var j = 0 ; j < warehousesOfProduct.length ; j++) {
						if (whId == warehousesOfProduct[j].warehouseId && warehousesOfProduct[j].availableQuantity < productInfoInCart.warehouses.valArray[i]) {
							errMsg += ", " + productCode;
						}
					}
				}
			}
//			var product = GridSaleBusiness.mapSaleProduct.get(productCode);
//			if (product != null) {
////				var warehousesOfProduct = SPCreateOrder._mapPRODUCTSbyWarehouse.get(product.productId);
//				var warehousesOfProduct = GridSaleBusiness.getWarehouseByType(product.productId);
//				var totalQuantityInWarehouse = 0;
//				var availableQuantityExceededWarehouse = null;
//				warehousesOfProduct.forEach(function(warehouse) {
//					totalQuantityInWarehouse += warehouse.availableQuantity;
//					if (!availableQuantityExceededWarehouse 
//						&& warehouse.availableQuantity < productInfoInCart.warehouses.get(Number(warehouse.warehouseId))) {
//						availableQuantityExceededWarehouse = warehouse;
//					}
//				});
//
//				var errMsg = null;
//				if (totalOrderedQuanity > totalQuantityInWarehouse) {
//					errMsg = 'Sản phẩm "' + Utils.XSSEncode(productCode) + '" có tổng thực đặt và/hoặc số lượng KM vượt quá tồn kho. Vui lòng điều chỉnh số lượng bán hoặc số lượng KM.';
//				} else {
//					if (availableQuantityExceededWarehouse) {
//						errMsg = 'Sản phẩm "' + Utils.XSSEncode(productCode) + '" ở danh sách sản phẩm KM có số lượng vượt quá tồn kho của kho "' + availableQuantityExceededWarehouse.warehouseName + '".'
//								+ ' Vui lòng điều chỉnh kho cho sản phẩm.';
//					}
//				}
//				if (errMsg) {
//					$('#serverErrors').html(errMsg).show();
//					setTimeout(function() {
//						$('#serverErrors').html('').hide();
//					}, 10000);
//					return;
//				}				
//			}
		}

		//check tổng tiền hàng bị âm thì thông báo lỗi
		var totalAmount = Number($('#total').html().replace(/,/g,''));
		if(totalAmount < 0){
			$('#serverErrors').html('Lỗi: Tổng tiền đơn hàng – Tổng tiền chiết khấu < 0.').show();
			setTimeout(function() {
				$('#serverErrors').html('').hide();
			}, 10000);
			return;
		}
		
		//trungtm6 comment
		data.lstPromotionCode = lstPromotionCode;		
		data.lstPromotionProduct = lstPromotionProduct;
		data.lstPromotionType = lstPromotionType;
		data.lstDiscount = lstDiscount;
		data.lstDisper = lstDisper;
		data.lstPromotionProductQuatity = lstPromotionProductQuatity;
		data.lstPromotionProductQuatityPackage = lstPromotionProductQuatityPackage;
		data.lstPromotionProductQuatityRetail = lstPromotionProductQuatityRetail;
		data.lstPromotionProductMaxQuatity = lstPromotionProductMaxQuatity;
		data.lstPromotionProductMaxQuatityCk = lstPromotionProductMaxQuatityCk;
		data.lstMyTime = lstMyTime;
		//Luu sale_order_lot
		data.lstProductGroupAndLevelGroupId = mapProductGroupAndLevelGroupId.valArray;//lstProductGroupAndLevelGroupId;
		data.lstPromoWarehouseId = lstPromoWarehouseId;
		data.lstPromoWarehouseQuantity = lstPromoWarehouseQuantity;  
		data.lstPromoLevel = lstPromoLevel;
		
		//lưu key shop
		if ($('#chkKeyshop').is(':checked')) {
			var lstKeyShop = jQuery.extend(true, [], $('#gridKeyShop').datagrid('getRows'));
			if (lstKeyShop != null && lstKeyShop.length > 0) {
				for (var i = 0, j = 0 ; i < lstKeyShop.length ; i++, j++) {
					var r = lstKeyShop[i];
					if (r.productId != null && $('#quantityAmount' + j).length > 0) {//nếu là sản phẩm thì lấy số trên giao diện nhập
						//do i có thể bị trừ nhưng trên giao diện chưa đổi id của dòng đó nên phải có j để chạy tiếp thay cho i
						r.quantityAmount = StockValidateInput.getQuantity($('#quantityAmount' + j).val(), r.convfact);
						r.warehouseId = r.warehouse.warehouseId;//tạo biến mới để action dễ lấy
					}
					//Nếu nhập 0 thì ko lưu
					if (r.quantityAmount <= 0) {
						lstKeyShop.splice(i,1);
						i--;
					}
				}
				convertToSimpleObject(data,lstKeyShop,'lstKeyShop');
			}
		}
		
		//promolot
		var lstPromoLot = SPCreateOrder.getSaleOrderPromoLot();
		if (lstPromoLot != null) {
			convertToSimpleObject(data,lstPromoLot,'lstPromoLot');
		}
		
		//Danh sách sản phẩm đạt CTKM
		if (SPCreateOrder._lstProgramDatSaleProduct.length > 0) {
			convertToSimpleObject(data,SPCreateOrder._lstProgramDatSaleProduct,'lstProgramDatSaleProduct');
		}

		var cfMsg = hasManualPromotion ? "<b style='color:#f00;'>Đơn hàng có khuyến mãi tay / hủy / đổi / trả / CTTB</b><br/>Bạn có muốn tiếp tục lưu đơn hàng?" : "Bạn có muốn lưu đơn hàng?";
		if (SPPrintOrder.saveOrderCallback != undefined && SPPrintOrder.saveOrderCallback != null) {
			cfMsg = "Đơn hàng có thay đổi, bạn có muốn lưu đơn hàng trước khi duyệt?";
		}
		if(message != undefined && message != null){
			data.isSaveNotCheck = 1;
			cfMsg = message;
		}
		if (errMsg.length > 0) {
			errMsg = 'Sản phẩm "' + Utils.XSSEncode(errMsg.replace(", ",'')) + '" có tổng thực đặt và/hoặc số lượng KM vượt quá tồn kho. Vui lòng điều chỉnh số lượng bán hoặc số lượng KM.';
			cfMsg = errMsg + '<br/>' + cfMsg;
		}
		DEBUG ? console.log(data) : null;
		if (data.orderId != null && data.orderId != undefined) {
			//Chinh sua don hang
			Utils.addOrSaveData(data, '/sale-product/create-order/adjust-edit-order', SPCreateOrder._xhrSave, "serverErrors", function(result){
				if(!result.error){
					if(!result.error){
						var curUrl = window.location.href; // lacnv1: trang tao don hang thi khong load lai trang
						if (curUrl.indexOf("/sale-product/create-order/info") > -1) {
							$("#btnOrderSave").attr("disabled", "disabled");
							$("#btnOrderSave").addClass("BtnGeneralDStyle");
							enable('btnOrderReset');
						} else {
							if (SPPrintOrder.saveOrderCallback == undefined || SPPrintOrder.saveOrderCallback == null) {
								$.cookie('saveOrder', 1);
								SPCreateOrder.orderChanged = false;
								//setTimeout(function(){window.location.reload();},0);
								setTimeout(function(){window.location.assign(window.location.href);},0);
							} else { // man hinh sua don (xac nhan IN), luu truoc khi duyet
								SPCreateOrder.orderChanged = false;
								SPPrintOrder.saveOrderCallback.call(this, result);
							}
						}
						//$('#gridSaleData tbody tr td:nth-child(' + (COLSALE.WAREHOUSE + 2) + ') a, #spPromotionProductInfor tbody tr td[field=warehouse] a, #saleOrderPromotion tbody tr td[field=warehouse] a').removeAttr('onclick').removeAttr('onclick');
						SPCreateOrder._isCreatingSaleOrder = false;
					}else{
						if(result.flag){
							$('#serverErrors').html(result.errMsg).show();
							disabled('btnOrderSave');
						}else{
							$('#serverErrors').html(result.errMsg).show();
						}				
					}
				}else{
					var msgWarning = '';
					if(result.errMsg == 'TOTAL_DEBIT_LARGER_THAN_MAX_DEBIT'){
						msgWarning += 'Số tiền nợ của khách hàng đã vượt quá hạn mức nợ cho phép. Bạn có muốn tiếp tục ?';
					}else if(result.errMsg == 'MAX_DAY_DEBIT'){
						msgWarning += 'Số ngày nợ của khách hàng đã vượt quá hạn mức nợ cho phép. Bạn có muốn tiếp tục ?';
					}
					if(msgWarning != ''){
						SPCreateOrder.createOrder(msgWarning);
						return;
					}else{
						$('#serverErrors').html(result.errMsg).show();
					}
				}
			}, 'loading2', '', false, cfMsg,  function(errResult) {
				var msgWarning = '';
				if(errResult.errMsg == 'TOTAL_DEBIT_LARGER_THAN_MAX_DEBIT'){
					msgWarning += 'Số tiền nợ của khách hàng đã vượt quá hạn mức nợ cho phép , bạn có muốn tiếp tục.';
				}else if(errResult.errMsg == 'MAX_DAY_DEBIT'){
					msgWarning += 'Số ngày nợ của khách hàng đã vượt quá hạn mức nợ cho phép , bạn có muốn tiếp tục.';
				}
				if(msgWarning != ''){
					SPCreateOrder.createOrder(msgWarning);
					return;
				}
			});
			if (hasManualPromotion) {
				$(".messager-icon").removeClass("messager-question");
				$(".messager-icon").addClass("messager-warning");
			}
		} else {
			//Tao moi don hang
			Utils.addOrSaveData(data, '/sale-product/create-order/order', SPCreateOrder._xhrSave, "serverErrors", function(result){
				if(!result.error){
					var tmz = setTimeout(function(){
						SPCreateOrder._isCreatingSaleOrder = false;
						$('#btnOrderSave').attr('disabled','disabled');	
						$('#btnOrderSave').addClass('BtnGeneralDStyle');
						$('#customerCode').attr('disabled','disabled');	$('#customerCode').addClass('BtnGeneralDStyle');
						
						$('#staffSaleCode').combobox('disable');		
						$('#staffSaleCode').parent().addClass('BoxDisSelect');
						
						$('#cashierStaffCode').combobox('disable');
						$('#cashierStaffCode').parent().addClass('BoxDisSelect');
						
						$('#transferStaffCode').combobox('disable');
						$('#transferStaffCode').parent().addClass('BoxDisSelect');
						
						$('#carId').combobox('disable');
						$('#carId').parent().addClass('BoxDisSelect');	
						
						$('#deliveryDate').attr('disabled','disabled');	
						$('#priorityId').attr('disabled','disabled');$('#priorityId').parent().addClass('BoxDisSelect');
						$('.cls_Promotion_Convfact_Value').each(function(){$(this).attr('disabled','disabled');});
						$('.cls_Promotion_Portion_Value').each(function(){$(this).attr('disabled','disabled');});
						$('td[field=key] a').html('');
						$('td[field=key] a').removeAttr('onclick');
						$('td[field=delete ] a').removeAttr('onclick');
						$('#gridSaleData tbody tr td:nth-child(' + (COLSALE.WAREHOUSE + 2) + ') a, #spPromotionProductInfor tbody tr td[field=warehouse] a, #saleOrderPromotion tbody tr td[field=warehouse] a').removeAttr('onclick').removeAttr('onclick');
						$('.cls_Convfact_Value').each(function(){$(this).attr('disabled','disabled');});
						$('.discountAmount').each(function(){$(this).attr('disabled','disabled');});
						$('.cls_CommercialSupporCode').each(function(){	$(this).attr('disabled','disabled');});	
						
						var htSale = $('#gridSaleData').handsontable('getInstance');
						for(var i=0;i<htSale.countRows();i++){
							for (var p in COLSALE) {
							    htSale.getCellMeta(i, COLSALE[p]).readOnly = true;
							}
						}
						htSale.render();
						SPAdjustmentOrder._orderId = result.saleOrder.id;
						isAuthorize = false;	
						enable('btnOrderCopy');
						enable('btnOrderReset');
						$('#backBtn').hide();
						$("#spSaleProductInfor a.delIcons:has(img)").remove();
					},200);
				}else{
					$('#divOverlay').hide();
					var msgWarning = '';
					if(result.errMsg == 'TOTAL_DEBIT_LARGER_THAN_MAX_DEBIT'){
						msgWarning += 'Số tiền nợ của khách hàng đã vượt quá hạn mức nợ cho phép , bạn có muốn tiếp tục.';
					}else if(result.errMsg == 'MAX_DAY_DEBIT'){
						msgWarning += 'Số ngày nợ của khách hàng đã vượt quá hạn mức nợ cho phép , bạn có muốn tiếp tục.';
					}
					if(msgWarning != ''){
						SPCreateOrder.createOrder(msgWarning);
						return;
					}else{
						$('#serverErrors').html(result.errMsg).show();
					}
				}
				if(result.saleOrder != null && result.saleOrder != undefined) {
					$('#orderNumber').val(result.saleOrder.orderNumber);
				}
			}, 'loading2', '', false, cfMsg, function(errResult) {
				if(errResult.isRePayment) {
					enable('payment');
					$('#btnOrderReset').attr('disabled','disabled');
					$('#btnOrderReset').addClass('BtnGeneralDStyle');
//					disabled('btnOrderReset');
					$('#payment').removeClass('BtnGeneralDStyle');	
					$('#btnOrderSave').attr('disabled','disabled');
					$('#btnOrderSave').addClass('BtnGeneralDStyle');
				}
				var msgWarning = '';
				if(errResult.errMsg == 'TOTAL_DEBIT_LARGER_THAN_MAX_DEBIT'){
					msgWarning += 'Số tiền nợ của khách hàng đã vượt quá hạn mức nợ cho phép , bạn có muốn tiếp tục.';
				}else if(errResult.errMsg == 'MAX_DAY_DEBIT'){
					msgWarning += 'Số ngày nợ của khách hàng đã vượt quá hạn mức nợ cho phép , bạn có muốn tiếp tục.';
				}
				if(msgWarning != ''){
					SPCreateOrder.createOrder(msgWarning);
					return;
				}
			});
			if (hasManualPromotion) {
				$(".messager-icon").removeClass("messager-question");
				$(".messager-icon").addClass("messager-warning");
			}
		}
	},
	
	// Kiem tra phan tu ele co ton tai trong lst hay khong.
	// author tulv2 
	// 24.09.2014
	checkElementExistsInList: function(lst,ele, existsInfo){
		var flag = false;
		for(var i = 0, sz = lst.length; i < sz; i++) {
			if(lst[i]==ele){
				flag = true;
				existsInfo.index = i;
				break;
			}
		}
		return flag;
	},
	/**
	 * Ham to mau grid chon dialog sp
	 * @params index
	 */
	amountBlured: function(i){
		var product = $("#productGrids").datagrid('getRows')[i];
		var color =  SPAdjustmentOrder.addStyleRowProductDialog(i,product);
		  if(color==WEB_COLOR.ORANGE){
				$('span.prStyle'+i).each(function(){ $(this).css('color',WEB_COLOR.ORANGE_CODE);});
			}else if(color==WEB_COLOR.RED){
				$('span.prStyle'+i).each(function(){ $(this).css('color',WEB_COLOR.RED_CODE);});
			}else {
				  $('span.prStyle'+i).each(function(){ $(this).css('color',WEB_COLOR.BLACK_CODE);});
			 }
	},	
	
	/**
	 * Ham xem chi tiet spKM
	 * @params productCode
	 */
	viewProductPromotionDetail:function(productCode){
		showLoadingIcon();
		$('#GeneralInforErrors').html('').hide();
		var staffCode= null;
		var customerCode = null;
		var shopCodeFilter = $('#shopCodeCB').combobox('getValue');
		if (SPReturnOrder._isReturn) {
			customerCode = $("#customerCodeHeader").html().trim();
			staffCode = $('#staffCodeHeader').html().trim();
		} else if (SPCreateOrder._isViewOrder) {
			customerCode = $("#customerCodeLink").html().trim();
			staffCode = $("#staffCode").val().trim();
		} else {
			staffCode = $('#staffSaleCode').combobox('getValue');
			if (staffCode.length == 0) {
				$('#GeneralInforErrors').html('Vui lòng chọn mã NVBH').show();
				return false;
			}
			customerCode = $('#customerCode').val().trim();
			if (customerCode.length == 0) {
				$('#GeneralInforErrors').html('Vui lòng chọn mã khách hàng')
						.show();
				return false;
			}
		}
		if (productCode != '') {
			var url='/commons/products-details?productCode='+productCode+'&customerCode='+customerCode+'&salerCode='+staffCode + '&shopCodeFilter=' + shopCodeFilter;
			if (SPAdjustmentOrder._orderId) {
				url = url + "&orderId="+ SPAdjustmentOrder._orderId;
			}
			$.getJSON(encodeURI(url), function(data) {
				hideLoadingIcon();
				if(!data.error){
					var isSaleProduct = false;
					CommonSearch.showProductInfo(data.product.productCode, data.product.productName, 
							data.product.convfact,
							formatQuantityEx(data.avaiableQuantity, data.product.convfact),
							data.price, data.avaiableQuantity, data.promotion, isSaleProduct);
				}else{
					$('#GeneralInforErrors').html(data.errMsg).show();
				}					
			});				
		}
	},	
	
	/**
	 * Ham reset thong tin don hang
	 */
	resetOrder:function() {
		$('.BreadcrumbSection li:last span').html('Tạo đơn bán hàng');
		$('#backBtn').show();
		$('#orderNumber').val('');
		enable('customerCode');
		enable('deliveryDate');
		$('#carId').combobox('setValue','');
		enableSelectbox('carId');
		$('#carId').combobox('enable');
		enableSelectbox('priorityId');
		$('#customerCode').val('');
		$('#customerName').html('');
		$('#address').html('');
		$('#phone').html('');
		$('#carId').val('');
		$('#carId').change();
		var lockDate = $('#currentLockDay').val();
		$('#deliveryDate').val(lockDate);
		var priority = $('#priorityId option').next().val();
		$('#priorityId').val(priority);
		$('#priorityId').change();
		SPAdjustmentOrder._orderId = null;

//		var shopCodeFilter = $('#shopCodeCB').combobox('getValue');
		//refresh keyshop
		var isLoadKeyShopGrid = 1;
		$('#gridKeyShop').datagrid('loadData',[]);
		//-----------------------------------
		SPCreateOrder.loadProductForSale(SPCreateOrder._shopCodeFilter);
		SPCreateOrder.resetStaff();
		GridSaleBusiness.resetSaleGrid(1);
		SPCreateOrder.resetPromotionGrid();
		SPCreateOrder._lstPromotionOpenProduct = null;
		OpenProductPromotion.loadOpenProductGrid(null);
		SPCreateOrder.loadGridAccumulative({});
		SPCreateOrder._lstDiscountKeyShop = new Map();
		SPCreateOrder._promotionPortionReceivedList = new Map();
		$('.cls_Promotion_Convfact_Value').each(function(){$(this).attr('disabled',false);});
		$('.cls_Convfact_Value').each(function(){$(this).attr('disabled',false);});
		$('.discountAmount').each(function(){$(this).attr('disabled',false);});
		$('.cls_CommercialSupporCode').each(function(){	$(this).attr('disabled',false);});		
		isAuthorize = true;
		enable('payment');			
		disabled('btnOrderSave');
		$('.ErrorMsgStyle').html('').hide();
		$('#customerCode').focus();
		disabled('btnOrderCopy');
		disabled('btnOrderReset');
		enable('btnOrderSave');
		SPCreateOrder._isCreatingSaleOrder = true;
	},
	
	/**
	 * Ham hien thi dialog doi~SPKM ap dung cho chon nhieu sp 
	 * @params result 
	 * @params mapId
	 * @callback
	 */
	promotionMultiProductDialog: function(callback, result, mapId, isZV21,errCodeForProductNotStock, isZV24) {
		CommonSearch._currentSearchCallback = callback;
		var isEdited = '';
		var html = $('#searchStyle5').html();
		$('#searchStyle5').dialog({
			closed: false,
			cache: false,
			modal: true,
			onOpen: function() {
				$('#errMultiChooseProduct').hide();
				if (!isNullOrEmpty(errCodeForProductNotStock)) {
		        		$('#errMultiChooseProduct').html("Các SPKM: "+ errCodeForProductNotStock + " không có tồn kho để tham gia đổi SPKM. Vui lòng kiểm tra lại dữ liệu.").show();
		        		setTimeout(function() { $('#errMultiChooseProduct').html('').hide(); },10000)
		        	}
				var lst = new Array();
				var max = 0;
				for (var i = 0; i < result.length; i++) {
					var obj = result[i].saleOrderLot;
					if (obj != null && obj != undefined) {
						for (var j = 0; j < obj.length; ++j) {
							obj[j].saleOrderDetail = result[i].saleOrderDetail;
							obj[j].promoProduct = result[i].promoProduct;
							obj[j].levelPromo = result[i].levelPromo;
							obj[j].type = result[i].type;
							obj[j].keyList = result[i].keyList;
							obj[j].changeProduct = result[i].changeProduct;						
							obj[j].typeName = result[i].typeName;
							obj[j].isEdited = result[i].isEdited;
							obj[j].productGroupId = result[i].productGroupId;
							isEdited = result[i].isEdited;
							obj[j].promotionType = result[i].promotionType;
							obj[j].openPromo = result[i].openPromo;
							obj[j].hasPortion = result[i].hasPortion;
							var tmpObj = SPCreateOrder.getObjectPromotionProduct(obj[j]);
							
							tmpObj.quantity = 0;
							lst.push(tmpObj);
						};
					};
				}
				var rootSelectRow = result.isZV24 ? $('#promotionGrid2').datagrid('getRows')[SPCreateOrder._selectingIndex] 
												: isZV21 ? $('#gridSaleOrderPromotion').datagrid('getRows')[SPCreateOrder._selectingIndex]
												: $('#gridPromotionProduct').datagrid('getRows')[SPCreateOrder._selectingIndex];
				var ration = null;
				var isPortionEditing = (isEdited == 2) || (isEdited == 0 && rootSelectRow.hasPortion == 1);
				if (isPortionEditing) {
					if (SPCreateOrder._saleOrderQuantityReceivedList != null && SPCreateOrder._saleOrderQuantityReceivedList.length > 0) {
						for (var ind=0; ind<SPCreateOrder._saleOrderQuantityReceivedList.length; ind++) {
							var objTmp = SPCreateOrder._saleOrderQuantityReceivedList[ind];
							if (objTmp.promotionCode == rootSelectRow.programCode
								&& objTmp.productGroupId == rootSelectRow.productGroupId
								&& objTmp.promotionLevel == rootSelectRow.levelPromo) {
								rootSelectRow.quantityReceived = objTmp.quantityReceived;
								break;
							}
						}
					}
					ration = rootSelectRow.quantityReceived / rootSelectRow.maxQuantityReceived;
				} else {
					ration = 1;
				}
				$('.easyui-dialog#searchStyle5 #listPromotionProduct').datagrid({
					data:lst,
	        		rownumbers:true,
	        		scrollbarSize:0,
	        		fitColumns:true,
	        		singleSelect: true,
	        		width:$('#searchStyle5').width() - 15,
	        		columns:[[  
   	        		   	{field:'productCode',title:'Mã sản phẩm', align:'left',width:90, sortable:false, resizable:false},  
   	        		   	{field:'productName',title:'Tên sản phẩm', align:'left',width:210, sortable:false, resizable:false},
   	        		   	{field:'stock', title:'Kho', align:'left',width:100,sortable:false,resizable:false,
   	        			   	formatter:function(value,row,index){
   	        				   	if (row.warehouse != null && row.warehouse != undefined) {
   	        					   	return Utils.XSSEncode(row.warehouse.warehouseName);
   	        				   	}
   	        			   	}
   	        		   	},
   	        		   	{field:'availableQuantity', title:'Tồn kho', align:'right',width:100,sortable:false,resizable:false,
   	        			   	formatter:function(value,row,index){
   	        				   	if (row.stockTotal != null && row.stockTotal != undefined) {
   	        				   		row.availableQuantity = row.stockTotal.availableQuantity;
   	        					   //if (!isNullOrEmpty(SPAdjustmentOrder._orderId)) {
   	        					//	   return formatQuantityEx(row.stockTotal.quantity,row.convfact);
   	        					   //} else {
   	        						return formatQuantityEx(row.stockTotal.availableQuantity,row.convfact);
   	        					   //};
   	        				   	};
   	        			   	}
   	        		   	},
   	        		   	{field:'quantity',title:'Số lượng',width:100,sortable:false, resizable:false,
   	        			   	formatter:function(value,row,index){
   	        				   	var quantity = row.quantity;
   	        				   	var id = 'multi_change_product_quantity_' + row.productId + '_' + index;
   	        				   	if(isNaN(mapId)) {
   	        					   	if(index == 0) {
   	   	        						return '<input id="' + id + '" value="'+formatQuantityEx(quantity,row.convfact)+'" index="'+index+'" class="Multi_Change_Product_Quantity Multi_Change_Product_Quantity_Begin" onkeypress="" onblur="" style="text-align: right; width: 50px;"/><span class="ppStyle0" style="color: rgb(255, 0, 0);float:right;">('+formatQuantityEx(row.maxQuantity*ration,row.convfact)+')</span>';
   	   	        				   	} else if(index == lst.length - 1) {
   	   	        						return '<input id="' + id + '" value="'+formatQuantityEx(quantity,row.convfact)+'" index="'+index+'" class="Multi_Change_Product_Quantity Multi_Change_Product_Quantity_End" onkeypress="" onblur="" style="text-align: right; width: 50px;"/><span class="ppStyle0" style="color: rgb(255, 0, 0);float:right;">('+formatQuantityEx(row.maxQuantity*ration,row.convfact)+')</span>';
   	   	        				   	} else {
   	   	        						return '<input id="' + id + '" value="'+formatQuantityEx(quantity,row.convfact)+'" index="'+index+'" class="Multi_Change_Product_Quantity" onkeypress="" onblur="" style="text-align: right; width: 50px;"/><span class="ppStyle0" style="color: rgb(255, 0, 0);float:right;">('+formatQuantityEx(row.maxQuantity*ration,row.convfact)+')</span>';
   	   	        				   	};
   	        				   	} else {
   	        					   	if(index == 0) {
   	   	        						return '<input id="' + id + '" value="'+formatQuantityEx(quantity,row.convfact)+'" index="'+index+'" class="Multi_Change_Product_Quantity Multi_Change_Product_Quantity_Begin" onkeypress="" onblur="" style="text-align: right; width: 50px;"/><span class="ppStyle0" style="color: rgb(255, 0, 0);float:right;">('+formatQuantityEx(row.maxQuantity*ration,row.convfact)+')</span>';
   	   	        				   	} else if(index == lst.length - 1) {
   	   	        						return '<input id="' + id + '" value="'+formatQuantityEx(quantity,row.convfact)+'" index="'+index+'" class="Multi_Change_Product_Quantity Multi_Change_Product_Quantity_End" onkeypress="" onblur="" style="text-align: right; width: 50px;"/><span class="ppStyle0" style="color: rgb(255, 0, 0);float:right;">('+formatQuantityEx(row.maxQuantity*ration,row.convfact)+')</span>';
   	   	        				   	} else {
   	   	        						return '<input id="' + id + '" value="'+formatQuantityEx(quantity,row.convfact)+'" index="'+index+'" class="Multi_Change_Product_Quantity" onkeypress="" onblur="" style="text-align: right; width: 50px;"/><span class="ppStyle0" style="color: rgb(255, 0, 0);float:right;">('+formatQuantityEx(row.maxQuantity*ration,row.convfact)+')</span>';
   	   	        				   	};
   	        				   	};
   	        			   	}
   	        		   	}
   	        		]],
   	        		onLoadSuccess:function(){
   	        			setTimeout(function(){
   	        				CommonSearchEasyUI.fitEasyDialog("searchStyle5");
   	        			}, 100);
   	        			$('.easyui-dialog .datagrid-header-rownumber').html('STT');
   	        			$('.Multi_Change_Product_Quantity_Begin').focus();
   	        			Utils.bindFormatOnTextfieldInputCss('Multi_Change_Product_Quantity', Utils._TF_NUMBER_CONVFACT);

   	        			/*
   	        			 * restore previous data on promotion grid to dialog
   	        			 */
   	        			var $grid = $('.easyui-dialog#searchStyle5 #listPromotionProduct');
   	        			if (!isZV21 && !isZV24) {	// ZV01-18
   	        				var rootSelectRow = $('#gridPromotionProduct').datagrid('getRows')[SPCreateOrder._selectingIndex];
   	        				var groupingId = Utils.XSSEncode(rootSelectRow.programCode + '_' + rootSelectRow.productGroupId + '_' + rootSelectRow.levelPromo) /*+ '_' + rootSelectRow.changeProduct*/;
   	        				var groupingRows = SPCreateOrder._promotionProgramGroup.get(groupingId);
   	        				$grid.datagrid('getRows').forEach(function(rowOnGrid, index) {
   	        					for (var k = 0; k < groupingRows.length; k++) {
   	        						if (groupingRows[k].productCode == rowOnGrid.productCode
   	        							&& groupingRows[k].warehouse && rowOnGrid.warehouse
   	        							&& (groupingRows[k].warehouse.warehouseId || groupingRows[k].warehouse.id) == (rowOnGrid.warehouse.warehouseId || rowOnGrid.warehouse.id)) {
   	        							//rowOnGrid.quantity = groupingRows[k].quantity;
   	        							$grid.datagrid('updateRow', {
   	        								index: index,
   	        								row: {
   	        									quantity: groupingRows[k].quantity
   	        								}
   	        							});
   	        							break;
   	        						}
   	        					}
   	        				});
   	        			} else if (isZV24) {
   	        				var rootSelectRow = $('#promotionGrid2').datagrid('getRows')[SPCreateOrder._selectingIndex];
   	        				var groupingId = rootSelectRow.programCode + '_' + rootSelectRow.productGroupId + '_' + rootSelectRow.levelPromo /*+ '_' + rootSelectRow.changeProduct*/;
   	        				var groupingRows = OpenProductPromotion._promotionProgramGroup.get(groupingId);
   	        				$grid.datagrid('getRows').forEach(function(rowOnGrid, index) {
   	        					for (var k = 0; k < groupingRows.length; k++) {
   	        						if (groupingRows[k].productCode == rowOnGrid.productCode
   	        							&& groupingRows[k].warehouse && rowOnGrid.warehouse
   	        							&& (groupingRows[k].warehouse.warehouseId || groupingRows[k].warehouse.id) == (rowOnGrid.warehouse.warehouseId || rowOnGrid.warehouse.id)) {
   	        							//rowOnGrid.quantity = groupingRows[k].quantity;
   	        							$grid.datagrid('updateRow', {
   	        								index: index,
   	        								row: {
   	        									quantity: groupingRows[k].quantity
   	        								}
   	        							});
   	        							break;
   	        						}
   	        					}
   	        				});
   	        			} else {	// ZV21
   	        				var rootSelectRow = $('#gridSaleOrderPromotion').datagrid('getRows')[SPCreateOrder._selectingIndex];
   	        				var orderPromotionGridRows = SPAdjustmentOrder._promotionProgramGroup.get(rootSelectRow.programCode + '_' + rootSelectRow.productGroupId + '_' + rootSelectRow.levelPromo);
   	        				$grid.datagrid('getRows').forEach(function(rowOnGrid, index) {
   	        					for (var k = 0/*, orderPromotionGridRows = $('#gridSaleOrderPromotion').datagrid('getRows')*/
   	        						; k < orderPromotionGridRows.length; k++) {
   	        						if (orderPromotionGridRows[k].groupId == rootSelectRow.groupId
   	        							&& orderPromotionGridRows[k].productCode == rowOnGrid.productCode
   	        							&& (orderPromotionGridRows[k].warehouse.warehouseId || orderPromotionGridRows[k].warehouse.id) == (rowOnGrid.warehouse.warehouseId || rowOnGrid.warehouse.id)) {
   	        							$grid.datagrid('updateRow', {
   	        								index: index,
   	        								row: {
   	        									quantity: orderPromotionGridRows[k].quantity
   	        								}
   	        							});
   	        							break;
   	        						}
   	        					}
   	        				});
   	        			}
   	        		}
				});
				$('.Multi_Change_Product_Quantity').live('blur',function() {
					var val = $(this).val();
					var index = $(this).attr('index');
					var convfact = $('.easyui-dialog#searchStyle5 #listPromotionProduct').datagrid('getRows')[index].convfact;
					$(this).val(formatQuantityEx(val,convfact));
				});
				$('#btnMultiChoose').show();
				$('#btnMultiChoose').bind('click', function() {
					var rootSelectRow = result.isZV24 ? $('#promotionGrid2').datagrid('getRows')[SPCreateOrder._selectingIndex] 
												: isZV21 ? $('#gridSaleOrderPromotion').datagrid('getRows')[SPCreateOrder._selectingIndex]
												: $('#gridPromotionProduct').datagrid('getRows')[SPCreateOrder._selectingIndex];
					$('#errMultiChooseProduct').hide();
					var rowSelect = $('.easyui-dialog#searchStyle5 #listPromotionProduct').datagrid('getRows');
					var listResult = new Array();
					var totalQuantity = 0;
					var totalMaxQuantity = 0;
					var __myTime = (new Date()).getTime();
					var stock = 0;
					for(var i = 0; i < rowSelect.length; i++) {
						var rSel = rowSelect[i];
						var quantity = StockValidateInput.getQuantity($('#multi_change_product_quantity_' + rSel.productId + '_' + i).val(), rSel.convfact);

						stock = stock + rSel.availableQuantity;

						if(isNullOrEmpty(quantity) || quantity == undefined || isNaN(quantity)) {
							$('#multi_change_product_quantity_'+rSel.productId).focus();
							$('#errMultiChooseProduct').html('Số lượng của sản phẩm ' + Utils.XSSEncode(rSel.productCode)+ 'không hợp lệ. Vui lòng nhập lại').show();
							return;
						}
						if(rSel.availableQuantity != undefined && Number(quantity) > Number(rSel.availableQuantity)) {
							$('#multi_change_product_quantity_'+rSel.productId).focus();
							$('#errMultiChooseProduct').html('Số lượng tồn kho đáp ứng của sản phẩm '+ Utils.XSSEncode(rSel.productCode)+' không đủ. Vui lòng nhập lại').show();
							return;
						}
						totalQuantity += quantity;
						totalMaxQuantity = rSel.maxQuantity;
						if (quantity > 0) {
							rSel.quantity = quantity;
							rSel.canChaneMultiProduct = true;
							rSel.myTime = __myTime;
							if (isZV21 && rootSelectRow) {
								rSel.groupId = rootSelectRow.groupId;
							}
							listResult.push(rSel);
						}
					}
					if (totalQuantity > totalMaxQuantity) {
						$('#errMultiChooseProduct').html('Số lượng vượt quá số lượng khuyến mãi tối đa. Vui lòng nhập lại').show();
						return;
					}
					totalMaxQuantity = totalMaxQuantity * ration;
					if (((Number(isEdited) == 0 && (isPortionEditing || stock >= totalMaxQuantity))
						|| Number(isEdited) == 2) && totalQuantity != totalMaxQuantity) {
						$('#errMultiChooseProduct').html('Tổng số lượng của các sản phẩm KM phải bằng (' + totalMaxQuantity + ')').show();
						return;
					}
					if(listResult.length == 1) {
						var rSel = listResult[0];
						rSel.canChaneMultiProduct = undefined;
						rSel.myTime = __myTime;
						listResult = new Array();
						if (isZV21 && rootSelectRow) {
							rSel.groupId = rootSelectRow.groupId;
						}
						if (isPortionEditing) {
							rSel.quantityReceived = rootSelectRow.quantityReceived;
						} else {
							rSel.quantityReceived = rootSelectRow.maxQuantityReceived;
						}
						rSel.isEdited = Number(isEdited);
						if (Number(isEdited) == 0 && !isPortionEditing && stock < totalMaxQuantity) {
							rSel.isEdited = 1;
						}
						listResult.push(rSel);
					} else {
						for(var j = 0; j < listResult.length; j++) {
							var obj = listResult[j];
							obj.maxQuantityCk = obj.maxQuantity;
							//obj.maxQuantity = obj.quantity;
							obj.myTime = __myTime;
							if (isPortionEditing) {
								obj.quantityReceived = rootSelectRow.quantityReceived;
							} else {
								obj.quantityReceived = rootSelectRow.maxQuantityReceived;
							}
							obj.isEdited = Number(isEdited);
							if (Number(isEdited) == 0 && !isPortionEditing && stock < totalMaxQuantity) {
								obj.isEdited = 1;
							}
						};
					}
					if (listResult.length == 0) {
						$('#errMultiChooseProduct').html('Vui lòng chọn sản phẩm đổi khuyến mãi.').show();
						setTimeout(function(){
							$('#errMultiChooseProduct').hide();
						}, 3000);
						return;
					}
					if (CommonSearch._currentSearchCallback != null) {
						CommonSearch._currentSearchCallback.call(this, listResult);
					};
				});
				$('.easyui-dialog td[field=quantity] input').bind('keyup', function(e) {
					$('#errMultiChooseProduct').hide();
					if(e.keyCode == keyCodes.ENTER) {
						var index = Number($(this).attr('index'));
						$('.easyui-dialog#searchStyle5 #listPromotionProduct').datagrid('selectRow', index);
						$('#btnMultiChoose').click();
					} else if(e.keyCode == keyCodes.ARROW_UP) {
						var index = Number($(this).attr('index'));
						var row = $('.easyui-dialog#searchStyle5 #listPromotionProduct').datagrid('getRows')[index];
						var quantity = StockValidateInput.getQuantity($(this).val(), row.convfact);
						if(quantity > row.maxQuantity * ration) {
							$(this).focus();
							$('#errMultiChooseProduct').html('Số lượt khuyến mãi vượt quá giá trị cho phép. Vui lòng nhập lại').show();
							return;
						}
						if(!$(this).hasClass('Multi_Change_Product_Quantity_Begin')) {
							$('.Multi_Change_Product_Quantity[index='+(index-1)+']').focus();
						};
					} else if(e.keyCode == keyCodes.ARROW_DOWN) {
						var index = Number($(this).attr('index'));
						var row = $('.easyui-dialog#searchStyle5 #listPromotionProduct').datagrid('getRows')[index];
						var quantity = StockValidateInput.getQuantity($(this).val(), row.convfact);
						if(quantity > row.maxQuantity * ration) {
							$(this).focus();
							$('#errMultiChooseProduct').html('Số lượt khuyến mãi vượt quá giá trị cho phép. Vui lòng nhập lại').show();
							return;
						}
						if(!$(this).hasClass('Multi_Change_Product_Quantity_End')) {
							$('.Multi_Change_Product_Quantity[index='+(index+1)+']').focus();
						};
					};
				});
			},
	        onClose : function(){
	        	$('#searchStyle5').html(html);
	        }
		});
	},
	
	/**
	 * Ham hien thi dialog doi~SPKM ap dung cho chon mot SPKM 
	 * @params result 
	 * @callback
	 */
	promotionProductDialog:function(callback,result, isZV21,errCodeForProductNotStock, isZV24, isEdited){
		CommonSearch._currentSearchCallback = callback;
		var html = $('#searchStyle5').html();
		$('#searchStyle5').dialog({
			closed: false,  
			cache: false,  
			modal: true, 
			onOpen: function() {
				if (!isNullOrEmpty(errCodeForProductNotStock)) {
					$('#errMultiChooseProduct').html("Các SPKM: " + Utils.XSSEncode(errCodeForProductNotStock) + " không có tồn kho để tham gia đổi SPKM. Vui lòng kiểm tra lại dữ liệu.").show();
					setTimeout(function() {
						$('#errMultiChooseProduct').html('').hide(); 
					},10000);
				}
			
				var lst = new Array();
				for(var i = 0; i < result.length; i++){
					var obj = result[i].saleOrderLot;
					if (obj != null && obj != undefined) {
						for (var j=0;j<obj.length;++j) {
							obj[j].saleOrderDetail = result[i].saleOrderDetail;
							obj[j].promoProduct = result[i].promoProduct;
							obj[j].type = result[i].type;
							obj[j].keyList = result[i].keyList;
							obj[j].changeProduct = result[i].changeProduct;						
							obj[j].typeName = result[i].typeName;
							obj[j].isEdited = result[i].isEdited;
							obj[j].promotionType = result[i].promotionType;
							obj[j].openPromo = result[i].openPromo;
							obj[j].productGroupId = result[i].productGroupId;
							obj[j].hasPortion = result[i].hasPortion;
							var pproduct = SPCreateOrder.getObjectPromotionProduct(obj[j]);	
							pproduct.quantity = pproduct.maxQuantity;
							pproduct.rownum = i+1;
							lst.push(pproduct);
						};
					};
				}
				var rootSelectRow = isZV24 ? $('#promotionGrid2').datagrid('getRows')[SPCreateOrder._selectingIndex]
							  : isZV21 ? $('#gridSaleOrderPromotion').datagrid('getRows')[SPCreateOrder._selectingIndex]
							  	: $('#gridPromotionProduct').datagrid('getRows')[SPCreateOrder._selectingIndex];
				var ration = null;
				var isPortionEditing = (isEdited == 2) || (isEdited == 0 && rootSelectRow.hasPortion == 1);
				if (isPortionEditing) {
					if (SPCreateOrder._saleOrderQuantityReceivedList != null && SPCreateOrder._saleOrderQuantityReceivedList.length > 0) {
						for (var ind=0; ind<SPCreateOrder._saleOrderQuantityReceivedList.length; ind++) {
							var objTmp = SPCreateOrder._saleOrderQuantityReceivedList[ind];
							if (objTmp.promotionCode == rootSelectRow.programCode
								&& objTmp.productGroupId == rootSelectRow.productGroupId
								&& objTmp.promotionLevel == rootSelectRow.levelPromo) {
								rootSelectRow.quantityReceived = objTmp.quantityReceived;
								break;
							}
						}
					}
					ration = rootSelectRow.quantityReceived / rootSelectRow.maxQuantityReceived;
				} else {
					ration = 1;
				}
				$('.easyui-dialog#searchStyle5 #listPromotionProduct').datagrid({
					data: lst,
					rownumbers: true,
					singleSelect: true,
					scrollbarSize: 0,
					width: $('#searchStyle5').width() - 15,
					columns: [[  
					   	{field:'productCode',title:'Mã sản phẩm', align:'left',width:120, sortable:false, resizable:false},  
					   	{field:'productName',title:'Tên sản phẩm', align:'left',width:250, sortable:false, resizable:false},
					   	{field:'quantity',title:'Số lượng',width:100,align:'right', sortable:false, resizable:false,
						   	formatter: function(value, row, index) {
						   		value = value * ration;
						   		row.quantity = value;
							   	return formatQuantityEx(value, row.convfact);
						}},
					   	{field:'ctkm',title:'Chọn',width:80,align:'center', sortable:false, resizable:false, 
						   	formatter: function(value, row, index) {
							   	return "<a href='javascript:void(0)' onclick=\"return SPCreateOrder.getResultPromotionProduct("+ index+ ", " + isZV21 + "," + isZV24 + ","+isEdited+");\">Chọn</a>";
					   	}},
					]],
					onLoadSuccess: function() {
						setTimeout(function(){
   	        				CommonSearchEasyUI.fitEasyDialog("searchStyle5");
   	        			}, 100);
						$('.easyui-dialog .datagrid-header-rownumber').html('STT');
					}
				});
			},
			onClose : function() {
				$('#searchStyle5').html(html);
			}
		});
	},
	
	/**
	 * Hamlay thong tin spkm
	 * @params rowId 
	 */
	getResultPromotionProduct:function(rowId, isZV21, isZV24, isEdited){
		var obj = $('.easyui-dialog#searchStyle5 #listPromotionProduct').datagrid('getRows')[rowId];
		var rootSelectRow = isZV24 ? $('#promotionGrid2').datagrid('getRows')[SPCreateOrder._selectingIndex]
							  : isZV21 ? $('#gridSaleOrderPromotion').datagrid('getRows')[SPCreateOrder._selectingIndex]
							  	: $('#gridPromotionProduct').datagrid('getRows')[SPCreateOrder._selectingIndex];
		if (isZV21) {
			obj.groupId = rootSelectRow.groupId;
		}
		obj.productGroupId = rootSelectRow.productGroupId;
		obj.levelPromo = rootSelectRow.levelPromo;

		var isPortionEditing = (isEdited == 2) || (isEdited == 0 && rootSelectRow.hasPortion == 1);
		if (isPortionEditing) {
			if (SPCreateOrder._saleOrderQuantityReceivedList != null && SPCreateOrder._saleOrderQuantityReceivedList.length > 0) {
				for (var ind=0; ind<SPCreateOrder._saleOrderQuantityReceivedList.length; ind++) {
					var objTmp = SPCreateOrder._saleOrderQuantityReceivedList[ind];
					if (objTmp.promotionCode == rootSelectRow.programCode
						&& objTmp.productGroupId == rootSelectRow.productGroupId
						&& objTmp.promotionLevel == rootSelectRow.levelPromo) {
						rootSelectRow.quantityReceived = objTmp.quantityReceived;
						break;
					}
				}
			}
			//obj.quantityReceived = rootSelectRow.quantityReceived;
		} else {
			//obj.quantityReceived = rootSelectRow.maxQuantityReceived;
		}

		if (CommonSearch._currentSearchCallback != null) {
			CommonSearch._currentSearchCallback.call(this, obj);
			$('#searchStyle5').dialog('close');
		}
	},
	
	/**
	 * Ham binding su kien sau khi thay doi so luong spKM
	 * @params event 
	 * @params index
	 */
	qtyPromotionChanged: function(e, i,gridName) {
		var list = new Map();
		var inputId = "";
		if (gridName == 0) {
			gridName = "gridPromotionProduct";
			list = SPCreateOrder._lstPromotionProductIsAuto;
			inputId = "convact_promotion_value_";
		} else if (gridName == 1) {
			gridName = "gridSaleOrderPromotion";
			list = SPCreateOrder._lstPromotionProductForOrderIsAuto;
			inputId = "convact_promotion_value_order_";
		}
		$('#serverErrors').html('').hide();
		var ppRow = null;
		if (isNaN(i)) {
			/*var arr = i.split('_');
			var productId = arr[1] || "";
			var programCode = arr[0] || "";
			var productGroupId = arr[2] || 0;
			var levelPromo = arr[3] || 0;
			var whID = arr[4] || 0;*/
			var rows = $('#'+gridName).datagrid('getRows');
			for (var k = 0; k < rows.length; k++) {
				var row = rows[k];
				/*if (row.productCode.trim().indexOf(" ") < 0
					&& row.productId == productId && row.commercialSupport == programCode
					&& row.productGroupId == productGroupId
					&& row.levelPromo == levelPromo
					&& (row.warehouse.id||row.warehouse.warehouseId) == whID) {*/
				if (row.productCode.trim().indexOf(" ") < 0
					&& row.commercialSupport + '_' + row.productId + '_' + row.productGroupId +'_' + row.levelPromo+'_' + (row.warehouse?(row.warehouse.id||row.warehouse.warehouseId):0) == i) {
					i = k;
					ppRow = row;
					break;
				}
			}
		} else {
			ppRow = $('#'+gridName).datagrid('getRows')[i];
		}
		if(ppRow==null){
			return false;
		}
		var convfact = ppRow.convfact;
		var value = $('#'+inputId+i).val().trim();
		var proQuatity = 0;
		var pProduct = null;
		if(isNaN(i)) {
			pProduct = list.get(i);
		} else {
			pProduct = list.valArray[i];
		}
		if(!isNullOrEmpty(value)){
			proQuatity = Number(getQuantity(value,convfact));			
			if(isNaN(proQuatity)) {							
				$('#'+inputId+i).val('');	
				proQuatity = 0;
			}else{
				var maxPromotionProductQuotaQuantity = 0;
				var tQtt = 0;
				if (list !== undefined && list !== null) {
					for (var k = 0; k < list.valArray.length; k++) {
						var item = list.valArray[k];
						if (item.productCode === ppRow.productCode 
							&& item.programCode === ppRow.programCode
							&& item.levelPromo == ppRow.levelPromo
							&& item.productGroupId == ppRow.productGroupId
							&& Number(item.changeProduct) === Number(ppRow.changeProduct)) {
							maxPromotionProductQuotaQuantity = item.maxQuantity;
							break;
						}
					}
					for (var k = 0; k < list.valArray.length; k++) {
						var item = list.valArray[k];
						if (item.productCode.trim().indexOf(" ") < 0
							&& item.programCode === ppRow.programCode
							&& item.levelPromo == ppRow.levelPromo
							&& item.productGroupId == ppRow.productGroupId
							&& Number(item.changeProduct) === Number(ppRow.changeProduct)
							&& (item.changeProduct == 1 || item.productCode == ppRow.productCode)) {
							if (i != k) {
								tQtt += item.quantity;
							} else {
								tQtt += proQuatity;
							}
						}
					}
				}
				if (tQtt > maxPromotionProductQuotaQuantity) {
					//proQuatity = ppRow.quantity;
					$('#serverErrors').html("Tổng số lượng sản phẩm khuyến mãi vượt quá số lượng tối đa được nhận.").show();
				}
				$('#'+inputId+i).val(formatQuantity(proQuatity, convfact));
				//$('#'+inputId+i).val(formatQuantity(value,convfact));
			}
		}
		if (pProduct != undefined && pProduct != null) {
			if(isNaN(i)) {
				pProduct.quantity = proQuatity;
				list.put(i, pProduct);
				SPCreateOrder.showTotalGrossWeightAndAmount();
			} else {
				pProduct.quantity = proQuatity;
				list.valArray[i] = pProduct;
				SPCreateOrder.showTotalGrossWeightAndAmount();
			}
		} else {
			pProduct = list.get(ppRow.programCode +"_"+ ppRow.productId);
			if (pProduct != undefined && pProduct != null) {
				pProduct.quantity = proQuatity;
				SPCreateOrder.showTotalGrossWeightAndAmount();	
			}
		}
		// Tang lai so suat neu nhap so luong > 0
		if (SPCreateOrder._saleOrderQuantityReceivedList != null && SPCreateOrder._saleOrderQuantityReceivedList.length > 0) {
			for (var ind=0; ind<SPCreateOrder._saleOrderQuantityReceivedList.length; ind++) {
				var objTmp = SPCreateOrder._saleOrderQuantityReceivedList[ind];
				if (objTmp.promotionCode == ppRow.programCode
					&& objTmp.productGroupId == ppRow.productGroupId
					&& objTmp.promotionLevel == ppRow.levelPromo) {
					if (ppRow.quantity > 0) {
						objTmp.quantityReceived = objTmp.maxQuantityReceived;
					}
					break;
				}
			}
		}
		SPCreateOrder.orderChanged = true;
		SPCreateOrder.updatePromotionGrids();
		if (SPAdjustmentOrder._orderId > 0 && $("#payment").prop("disabled")) {
			enable("btnOrderSave");
		}
		return true;		
	},
	
	/**
	 *  Ham binding su kien sau khi thay doi chiet khau KM
	 * @params event 
	 * @params index
	 */
	qtyPromotionChangedAmount:function(e,i){
		$('#serverErrors').html('').hide();
		var ppRow = $('#gridPromotionProduct').datagrid('getRows')[i];
		if(ppRow==null){
			return false;
		}
		var pProduct = SPCreateOrder._lstPromotionProductIsAuto.valArray[i];
		var discountAmount = $('#discountAmount_'+i).val().trim();
		discountAmount =Utils.returnMoneyValue(discountAmount);	
		if(!isNullOrEmpty(discountAmount)){
			if(isNaN(discountAmount)){
				$('#discountAmount_'+i).val('');
				discountAmount = 0;
			}else{
				$('#discountAmount_'+i).val(formatFloatValue(discountAmount));
			}
		}else{
			discountAmount = 0;
		}
		pProduct.discountAmount = discountAmount;
		SPCreateOrder._lstPromotionProductIsAuto.valArray[i] = pProduct;
		SPCreateOrder.showTotalGrossWeightAndAmount();
		SPCreateOrder.orderChanged = true;
		return true;		
	},
	
		/**
	 * Ham binding su kien sau khi thay doi so suat
	 * @params event 
	 * @params index
	 */
	qtyPromotionChangedPortion: function(e, i,gridName) {
		var list = new Map();
		var inputId = "";
		if (gridName == 0) {
			gridName = "gridPromotionProduct";
			list = SPCreateOrder._lstPromotionProductIsAuto;
			inputId = "portion_promotion_value_";
		} else if (gridName == 1) {
			gridName = "gridSaleOrderPromotion";
			list = SPCreateOrder._lstPromotionProductForOrderIsAuto;
			inputId = "portion_promotion_value_order_";
		}
		$('#serverErrors').html('').hide();
		var ppRow = null;
		var productId = "";
		var programCode = "";
		var levelPromo = 0;
		var productGroupId = 0;
		var ratio = 0;
		if(isNaN(i)) {
			/*var arr = i.split('_');
			productId = arr[1] || "";
			programCode = arr[0] || "";
			levelPromo = arr[3] || 0;
			productGroupId = arr[2] || 0;*/
			var rows = $('#'+gridName).datagrid('getRows');
			for(var k = 0; k < rows.length; k++) {
				var row = rows[k];
				/*if(row.productId == productId && row.programCode == programCode
					&& row.productGroupId == productGroupId
					&& row.levelPromo == levelPromo) {*/
				if (row.productCode.trim().indexOf(" ") < 0
					&& row.programCode+'_'+row.productId + '_' +row.productGroupId+'_'+ row.levelPromo == i) {
					i = k;
					ppRow = row;
					productId = ppRow.productId;
					programCode = ppRow.programCode;
					levelPromo = ppRow.levelPromo;
					productGroupId = ppRow.productGroupId;
					//ratio = Math.round(row.maxQuantity/Number(row.maxQuantityReceived));
					break;
				}
			}
		} else {
			ppRow = $('#'+gridName).datagrid('getRows')[i];
			productId = ppRow.productId;
			programCode = ppRow.programCode;
			levelPromo = ppRow.levelPromo;
			productGroupId = ppRow.productGroupId;
			//ratio = Math.round(ppRow.maxQuantity/Number(ppRow.maxQuantityReceived));
		}
		if(ppRow==null){
			return false;
		}
		var quantityReceived = ppRow.maxQuantityReceived;
		var value = $('#'+inputId+i).val().trim();
		var proQuatity = 0;
		var pProduct = null;
		if(isNaN(i)) {
			pProduct = list.get(i);
		} else {
			pProduct = list.valArray[i];
		}
		if (pProduct != undefined && pProduct != null) {
			if(!isNullOrEmpty(value)){
				if (isNaN(value)) {
					$('#serverErrors').html('Số suất của CTKM ' +Utils.XSSEncode(pProduct.programCode)+' không đúng định dạng số. Vui lòng nhập lại').show();
					$('#'+inputId+i).focus();
					return false;
				}
				if (Number(value) > quantityReceived) {
					$('#serverErrors').html('Số suất của CTKM ' +Utils.XSSEncode(pProduct.programCode)+' vượt quá số suất được hưởng. Vui lòng nhập số suất nhỏ hơn hoặc bằng '+ quantityReceived).show();
					$('#'+inputId+i).focus();
					//return false;
				}
				$('#'+inputId+i).val(value);
			}
			if(isNaN(i)) {
				list.put(i, pProduct);
			} else {
				list.valArray[i] = pProduct;
			}
			pProduct.quantityReceived = Number(value);
			SPCreateOrder.showTotalGrossWeightAndAmount();
		} else {
			pProduct = list.get(ppRow.programCode +"_"+ ppRow.productId);
			if (pProduct != undefined && pProduct != null) {
				pProduct.quantityReceived = Number(value);
				SPCreateOrder.showTotalGrossWeightAndAmount();	
			}
		}
		//Set lai gia tri cho so luong SPKM cua so suat moi chinh sua
		var rowsData = $('#'+gridName).datagrid('getRows');
		var qtt1 = null;
		var kkk = null;
		var mapT1 = new Map();
		var mapT2 = new Map();
		var mapT3 = new Map();
		var b = false;
		for (var l = 0, szl = rowsData.length; l < szl; l++) {
			var row = rowsData[l];
			if (row.productCode.trim().indexOf(" ") > 0) {
				continue;
			}
			if (row.programCode == programCode  && row.productGroupId == productGroupId && row.levelPromo == levelPromo) {
				//row.quantity = ratio*Number(value);
				if (row.changeProduct == 1) {
					kkk = programCode+"_"+productGroupId+"_"+levelPromo;
				} else {
					kkk = programCode+"_"+productGroupId+"_"+levelPromo+"_"+row.productCode;
				}
				if (mapT1.get(kkk) == null) {
					b = false;
					ratio = Math.round(row.maxQuantity/Number(row.maxQuantityReceived));
					qtt1 = ratio*Number(value);
				} else {
					b = true;
					qtt1 = mapT1.get(kkk);
				}

				if (row.stock >= qtt1) {
					row.quantity = qtt1;
					mapT1.put(kkk, 0);
				} else {
					row.quantity = row.stock;
					mapT1.put(kkk, qtt1 - row.stock);
				}
				mapT2.put(kkk, l);

				if (row.changeProduct == 1 && b && row.quantity == 0) {
					$("#"+gridName).datagrid("deleteRow", l);
					l--;
					szl--;
					rowsData =  $("#"+gridName).datagrid("getRows");
					for (var idx1 = 0, sz1 = list.size(); idx1 < sz1; idx1++) {
						if (list.valArray.length <= idx1 ||
							(list.valArray[idx1].productCode == row.productCode
							&& (list.valArray[idx1].warehouse.id || list.valArray[idx1].warehouse.warehouseId) == (row.warehouse.id||row.warehouse.warehouseId))) {
							list.remove(list.keyArray[idx1]);
							idx1--;
							sz1--;
						}
					}
				}
				
				/*if (row.quantity > row.maxQuantity) {
					row.quantity = row.maxQuantity;
				}*/
				/*if (row.promotionType != PromotionType.PROMOTION_FOR_ZV21) {
					if (row.promotionType != undefined && row.promotionType == PromotionType.PROMOTION_FOR_ORDER_TYPE_PRODUCT) {
						$('#convact_promotion_value_order_'+l).val(formatQuantityEx(row.quantity,row.convfact));
					} else {
						$('#convact_promotion_value_'+l).val(formatQuantityEx(row.quantity,row.convfact));
					}
					row.quantityReceived = Number(value);
				}*/
			}
		}
		for (var ij = 0, szj = mapT1.size(); ij < szj; ij++) {
			if (mapT1.valArray[ij] > 0) {
				var idx = mapT2.get(mapT1.keyArray[ij]);
				if (-1 < idx && idx < rowsData.length) {
					rowsData[idx].quantity = rowsData[idx].quantity + mapT1.valArray[ij];
					/*if (rowsData[idx].quantity > rowsData[idx].maxQuantity) {
						rowsData[idx].quantity = rowsData[idx].maxQuantity;
					}*/
					/*if (rowsData[idx].promotionType != PromotionType.PROMOTION_FOR_ZV21) {
						if (rowsData[idx].promotionType != undefined && rowsData[idx].promotionType == PromotionType.PROMOTION_FOR_ORDER_TYPE_PRODUCT) {
							$('#convact_promotion_value_order_'+idx).val(formatQuantityEx(rowsData[idx].quantity, rowsData[idx].convfact));
						} else {
							$('#convact_promotion_value_'+idx).val(formatQuantityEx(rowsData[idx].quantity, rowsData[idx].convfact));
						}
					}*/
				}
			}
		}
		//Luu gia tri so suat moi chinh sua
		if (SPCreateOrder._saleOrderQuantityReceivedList != null && SPCreateOrder._saleOrderQuantityReceivedList.length > 0) {
			for (var ind=0;ind<SPCreateOrder._saleOrderQuantityReceivedList.length;ind++) {
				var objTmp = SPCreateOrder._saleOrderQuantityReceivedList[ind];
				if(objTmp.promotionCode == programCode
					&& objTmp.productGroupId == productGroupId
					&& objTmp.promotionLevel == levelPromo) {
					objTmp.quantityReceived = Number(value);
					break;
				}
			}
		}
		if (gridName == "gridPromotionProduct") {
			OpenProductPromotion.sortProductList(SPCreateOrder._lstPromotionProductIsAuto, false);
			SPCreateOrder.fillTablePromotionProduct(SPCreateOrder._lstPromotionProductIsAuto);
			OpenProductPromotion.refreshOpenProductGrid(); // load lai so thu tu cua grid KM mo moi
		} else if (gridName == "gridSaleOrderPromotion") {
			OpenProductPromotion.sortProductList(SPCreateOrder._lstPromotionProductForOrderIsAuto, true);
			SPCreateOrder.fillTablePromotionProductForOrder(SPCreateOrder._lstPromotionProductForOrderIsAuto);
		}
		SPCreateOrder.showTotalGrossWeightAndAmount();
		SPCreateOrder.orderChanged = true;
		SPCreateOrder.updatePromotionGrids();
		if (SPAdjustmentOrder._orderId > 0 && $("#payment").prop("disabled")) {
			enable("btnOrderSave");
		}
		return true;
	},
	/**
	 *  Ham tinh toan tong tien , tong kluong va tong ckhau don hang
	 * @params type 
	 */
	showTotalGrossWeightAndAmount:function(type){
		if (!GridSaleBusiness._isvalidQuantity){
			$('#totalWeight').html('');
			$('#totalDiscount').html('');
			$('#totalVAT').html('');
			$('#total').html('');
		} else {
			var len = 0;
			var totalPromotionGrossW = 0;
			var totalPromotionOrderGrossW = 0;
			var totalPromotionAmount = 0;
			var temp = 0;	
			
			//Tong tien & trong luong cua grid sp ban
			var htSale = $('#gridSaleData').handsontable('getInstance');
			var quantityCol = htSale.getDataAtCol(COLSALE.QUANTITY);
			var discountCol = htSale.getDataAtCol(COLSALE.DISCOUNT);
			var totalWeight = 0;
			var totalAmount = 0;
			var totalDiscount = 0;
			var PRECISION = 3;
			if (!Utils.isEmpty(apConfig.sysDigitDecimal)){
				PRECISION = Number(apConfig.sysDigitDecimal);
			}
			
			for(var i = 0;i<htSale.getData().length;i++){
				if(quantityCol[i] != null){
					var product = GridSaleBusiness.mapSaleProduct.get(htSale.getDataAtCell(i,COLSALE.PRO_CODE));
					if(product != null){
						//trungtm6 sua thanh tien = slThung * giaThung + slLe * giaLe
						var quanStr = htSale.getData()[i][COLSALE.QUANTITY];
						var quanArr = quanStr.split('/');
						var quantity = StockValidateInput.getQuantity(quantityCol[i],product.convfact);
						var promotionCode = htSale.getDataAtCell(i,COLSALE.PROGRAM);
						if (isNullOrEmpty(promotionCode) || GridSaleBusiness.mapCS.get(promotionCode) == null) {
							var pkPrice = htSale.getData()[i][COLSALE.PACKING_PRICE];
							var price = htSale.getData()[i][COLSALE.PRICE];
							if (!isNullOrEmpty(pkPrice) && !isNullOrEmpty(price) && $.isArray(quanArr)) {
								pkPrice = Number(pkPrice.replace(/,/g, ""));
								price = Number(price.replace(/,/g, ""));
								if (quanArr.length > 1) {// nhập ko có '/' thì coi như số lẻ
									totalAmount = totalAmount + (Number(quanArr[0]) * pkPrice + Number(quanArr[1]) * price);
								} else {
									totalAmount = totalAmount + (Number(quanArr[0]) * price);
								}
							}
						}
						totalWeight = totalWeight + quantity*product.grossWeight;
					}
				} 
				if (!isNullOrEmpty(discountCol[i])) {
					var value = discountCol[i].toString().replace(/,/g, "");
					totalDiscount += Number(value);
				}
			}
			$('#totalWeightSale').html(formatFloatValue(totalWeight, 3));
			$('#totalAmountSale').html(formatCurrency(totalAmount));
			$('#totalDiscountSale').html(formatCurrency(totalDiscount));
			//Tinh tong tien khuyen mai CK SP
			var PROMOTION_PRODUCT = $('#gridPromotionProduct').datagrid('getRows');
			var hasPermissionToEdit = false;
			len = PROMOTION_PRODUCT.length;
			for(var i=0;i<len;++i){
				var row = PROMOTION_PRODUCT[i];
				if((row.type == 2 || row.type == 3) && !isNaN(row.discountAmount)) {
					totalPromotionAmount += Math.round(row.discountAmount);
				}else if(row.type == 1){
					var value = $('#convact_promotion_value_'+i).val();
					if(!isNullOrEmpty(value) && !isNullOrEmpty(row.convfact) && !isNaN(row.convfact)){
						temp = getQuantity(value.trim(),row.convfact);	
					}else{
						temp = 0;
					}		
					if (temp > row.stock) {
						hasPermissionToEdit = true;
					}
					if(!isNaN(temp) && !isNullOrEmpty(row.grossWeight)){					
						totalPromotionGrossW += Number(temp * row.grossWeight);					
					}
					var color =  SPAdjustmentOrder.addStyleRowSalePromotionProduct(i,row);
					if(color==WEB_COLOR.ORANGE){
						$('span.ppStyle'+i).each(function(){ $(this).css('color',WEB_COLOR.ORANGE_CODE);});
					}else if(color==WEB_COLOR.RED){
						$('span.ppStyle'+i).each(function(){ $(this).css('color',WEB_COLOR.RED_CODE);});
					}else {
						  $('span.ppStyle'+i).each(function(){ $(this).css('color',WEB_COLOR.BLACK_CODE);});
					}		
					/*if(row.isEdited == 0){
						disabled('portion_promotion_value_'+i);	
					} else*/ if (row.isEdited == 1) {
						disabled('portion_promotion_value_'+i);
					}
				}
			}
			// if (hasPermissionToEdit) {
			// 	for(var j=0;j<len;++j){
			// 		var row = PROMOTION_PRODUCT[j];
			// 		if(row.isEdited == 0){
			// 			enable('convact_promotion_value_'+j);	
			// 		}
			// 	}
			// }
			totalWeight += totalPromotionGrossW;
			$('#gridPromotionProduct').datagrid('reloadFooter',[{
				isFooter:true,
				quantity:formatFloatValue(totalPromotionGrossW, PRECISION)}]);
			
			// Tong trong luong va chiet khau khuyen mai don hang
			var PROMOTION_ORDER = $('#gridSaleOrderPromotion').datagrid('getRows');
			len = PROMOTION_ORDER.length;
			for(var i=0;i<len;++i){
				var row = PROMOTION_ORDER[i];
				if(row.productCode.indexOf(' ') == -1){
					if(row.type == 1){
						var value = $('#convact_promotion_value_order_'+i).val();
						if(!isNullOrEmpty(value) && !isNullOrEmpty(row.convfact) && !isNaN(row.convfact)){
							temp = getQuantity(value.trim(),row.convfact);	
						}else{
							temp = 0;
						}				
						if(!isNaN(temp) && !isNullOrEmpty(row.grossWeight)){	
							totalPromotionOrderGrossW += Number(temp * row.grossWeight);
							//totalWeight += Number(temp * row.grossWeight);
						}
						var color =  SPAdjustmentOrder.addStyleRowSalePromotion(i,row);
						if(color==WEB_COLOR.ORANGE){
							$('span.od_ppStyle'+i).each(function(){ $(this).css('color',WEB_COLOR.ORANGE_CODE);});
						}else if(color==WEB_COLOR.RED){
							$('span.od_ppStyle'+i).each(function(){ $(this).css('color',WEB_COLOR.RED_CODE);});
						}else {
							  $('span.od_ppStyle'+i).each(function(){ $(this).css('color',WEB_COLOR.BLACK_CODE);});
						}
						/*if(row.isEdited == 0){
							disabled('portion_promotion_value_order_'+i);
						} else*/ if (row.isEdited == 1) {
							disabled('portion_promotion_value_order_'+i);
						}
					}
				}
			}
			totalWeight += totalPromotionOrderGrossW;
			$('#gridSaleOrderPromotion').datagrid('reloadFooter',[{
				isFooter:true,
				quantity:formatFloatValue(totalPromotionOrderGrossW, PRECISION)}]);
			
			// Tong trong luong va tong tien thanh toan don hang
			//Fill gia tri tong tien don hang
			if ($("#promotionGridContainer2 .datagrid-view").length > 0) {
				var fts = $("#promotionGrid2").datagrid("getFooterRows");
				if (fts != null && fts.length > 0) {
					totalWeight = totalWeight + Number(fts[0].quantity);
				}
				var lstOP = $('#promotionGrid2').datagrid('getRows');
				var hasPermissionToEdit = false;
				len = lstOP.length;
				for (var i = 0; i < len; ++i) {
					var row = lstOP[i];
					if (row.type == PromotionType.FREE_PRODUCT && row.productCode.indexOf(' ') == -1) {
						var value = $('#op_convact_promotion_value_'+i).val();
						if (!isNullOrEmpty(value) && !isNullOrEmpty(row.convfact) && !isNaN(row.convfact)){
							temp = getQuantity(value.trim(),row.convfact);	
						} else {
							temp = 0;
						}
						if (temp > row.stock) {
							hasPermissionToEdit = true;
						}
						var color =  SPAdjustmentOrder.addStyleRowSalePromotionProduct(i, row);
						if (color == WEB_COLOR.ORANGE) {
							$('span.op_ppStyle'+i).each(function(){ $(this).css('color',WEB_COLOR.ORANGE_CODE);});
						} else if (color==WEB_COLOR.RED) {
							$('span.op_ppStyle'+i).each(function(){ $(this).css('color',WEB_COLOR.RED_CODE);});
						} else {
							  $('span.op_ppStyle'+i).each(function(){ $(this).css('color',WEB_COLOR.BLACK_CODE);});
						}		
						if (/*row.isEdited == 0 ||*/ row.isEdited == 1) {
							disabled('op_portion_promotion_value_'+i);	
						}
					} else if (row.type == PromotionType.FREE_PRICE || row.type == PromotionType.FREE_PERCENT) {
						totalPromotionAmount += Math.round(row.discountAmount);
					}
				}
			}
			var total = totalAmount - totalDiscount;
			$('#totalWeight').html(formatFloatValue(totalWeight, 3));
			$('#totalDiscount').html(formatCurrency(totalDiscount));
			if (SPCreateOrder._isAllowShowPrice){//cau hinh co show gia
				$('#totalVAT').html(formatCurrency(totalAmount));
				$('#total').html(formatCurrency(total));
			} else {
				$('#totalVAT').html('');
				$('#total').html('');
			}
			
			
			$('.cls_Promotion_Convfact_Value').each(function(){
				var id = $(this).attr('id');
				Utils.bindFormatOnTextfield(id,Utils._TF_NUMBER_CONVFACT,'',true);
				$(this).attr('maxlength',8);
			});	
			$('.discountAmount').each(function(){
				var id = $(this).attr('id');
				Utils.bindFormatOntextfieldCurrencyFor(id,Utils._TF_NUMBER_COMMA);
				$(this).attr('maxlength',17);
				if(!isAuthorize){
					disabled(id);					
				}
			});
			Utils.bindFormatOnTextfieldInputCss('cls_Promotion_Portion_Value', Utils._TF_NUMBER);
			if(!isAuthorize){
				$('.delIcons').removeAttr('onclick');
			}
			$('.clazzfooter').parent().css('text-align','center');
			$('.clazzfootervalue').parent().css('text-align','right');
			
			//thêm weight của sản phẩm trả thưởng keyshop
			if ($('#chkKeyshop').is(':checked')) {
				var weight = SPCreateOrder.getWeightOfKeyShop();
				var totalWeight = Number($('#totalWeight').html().replace(/,/g,''));
				totalWeight += weight;
				$('#totalWeight').html(formatCurrency(Math.round10(totalWeight, -3)));
			}
			//---------------------------------------------------------------
		}
				
	},
	
	/**
	 *  Ham kiem tra don hang la don tablet hay ko
	 */
	checkTablet:function(){
		if($('#tablet').length!=0 && $('#tablet').val().trim()!='' && $('#tablet').val().trim()=='2'){
			return true;
		}
		return false;
	},
	
	/**
	 *  Ham enable nut Tinh tien va disable nut Luu
	 */
	enablePaymentAndDisableSave:function(){
		enable('payment');
		disabled('btnOrderSave');
		disabled('btnOrderReset');
	},
	
	/**
	 * Ham nhap file import don hang
	 */
	importSaleOrder:function(){
		$('#importFrm').ajaxForm(options);
		$.messager.confirm(jsp_common_xacnhan,'Bạn có muốn nhập file excel này?',function(r){  
			if(r){
				$('#isView').val(0);
				$('#importFrm').submit();
				return false;
			}
		});	
	},
	
	/**
	 * Ham nhap file import don hang co CTTB
	 */
	importSaleOrderDisplay:function(){
		$('.ErrorMsgStyle').hide();
		$('.SuccessMsgStyle').hide();
		var excelFile = $('#excelFileDisplay').val();
		if (excelFile == null || excelFile == undefined || excelFile == '') {
			$('#errExcelMsg').html('Bạn chưa chọn file excel để import dữ liệu!').show();
			return false;
		}
		$.messager.confirm(jsp_common_xacnhan,'Bạn có muốn nhập file excel này?.',function(r){  
			if(r){
				$('#isView').val(0);
				$('#importDisplayFrm').submit();
				return false;
			}
		
		});	
	},
	beforeImportExcel: function(){
		if(!previewImportExcelFile(document.getElementById("excelFileDisplay"), 'importDisplayFrm', 'fakefilepcDisplay', 'errExcelMsg')){
			return false;
		}		
		$('#errExcelMsg').html('').hide();
		$('#successMsg').html('').hide();
		showLoadingIcon();
		return true;
	},
	
	/** CHON LO HUY CHO SAN PHAM */
	getProductLotList:function(index){
		
	},
	/** CHON LO CHO SAN PHAM BAN */
	getSaleProductLotList:function(rowIndex){
		
	},
	/** CHON LO CHO SAN PHAM KHUYEN MAI */
	getSalePromotionProductLotList:function(rowIndex){
		
	},
	/** CHON LO CHO HUY */
	fillProductLotTable:function(){
		
	},
	/** FILL TABLE : CHON LO CHO SAN PHAM KM */
	fillPromotionProductSaleLotTable:function(idex){
		
	},
	/** Xu ly thay doi so luong cua lo */
	quantityLotChanged:function(selector){
		
	},
	/** CHON LO CHO SAN PHAM HUY */
	onLotChange:function() {	
		
	},
	/** CHON LO CHO SAN PHAM BAN,DOI,TRA,KM TAY */
	onProductSaleLotChange:function() {
		
	},
	/** Chon lo cho san pham KM */
	onPromotionProductSaleLotChange:function(i, idex) {
		
	},
	/**
	 * clear temp data
	 * @author tuannd20
	 * @since 22/09/2014
	 */
	clearTempData: function() {
		GridSaleBusiness._mapWarehouseIdSelect = new Map();
	},
	/**
	 * effect change background color
	 * @author tuannd20
	 * @since 22/09/2014
	 */
	changeBackgroundColorEffect: function(selector) {
		$(selector).css('background-color', 'rgb(255, 255, 0)');
		var bStep = 10, blue = 0;
		var it = setInterval(function() {
		    $(selector).css('background-color', 'rgb(255, 255, ' + blue + ')');
		    blue += bStep;
		    if (blue > 255) {
		        clearInterval(it);
		        $(selector).css('background-color', '');
		    }
		}, 50);
	},

	/**
	 * apply row-strip to product-promotion grid
	 * @author tuannd20
	 * @since 09/10/2014
	 * @return {[type]} [description]
	 */
	applyRowStripToPromotionGrid: function() {
		try {
			if (SPCreateOrder._promotionProgramGroup != null) {
				SPCreateOrder._promotionProgramGroup.valArray.forEach(function(rowsInGroup, index){
					if (rowsInGroup != null && rowsInGroup instanceof Array) {
						rowsInGroup.forEach(function(item){
							if (index%2 == 1) {
								$('#spPromotionProductInfor table.datagrid-btable tr:eq(' + (Number(item.rownum) - 1) + ')').addClass('row-striped');
							} else {
								$('#spPromotionProductInfor table.datagrid-btable tr:eq(' + (Number(item.rownum) - 1) + ')').removeClass('row-striped');
							}
						});
					}

				});
			}
		} catch (e) {
			// pass through
		}
	},
	
	/**
	 * F9 tim kiem don hang de sua
	 * 
	 * @author lacnv1
	 * @since Sep 27, 2014
	 */
	openSearchWebOrderDlg: function() {
		if ($("#customerCode").val().trim().length > 0) {
			$.messager.confirm("Xác nhận", "Chọn đơn hàng mới sẽ hủy bỏ toàn bộ thông tin đơn hàng đã nhập, Bạn có muốn tiếp tục?", function(y) {
				if (y) {
					GridSaleBusiness.mapCS = new Map();
					GridSaleBusiness.listCSCode = new Array();
					GridSaleBusiness.mapSaleProduct = new Map();
					GridSaleBusiness.listSaleProductCode = new Array();
					SPCreateOrder.resetOrder();
					SPCreateOrder.showSearchWebOrderDlg();
				}
			});
		} else {
			GridSaleBusiness.mapCS = new Map();
			GridSaleBusiness.listCSCode = new Array();
			GridSaleBusiness.mapSaleProduct = new Map();
			GridSaleBusiness.listSaleProductCode = new Array();
			SPCreateOrder.resetOrder();
			SPCreateOrder.showSearchWebOrderDlg();
		}
	},
	
	/**
	 * Mo popup tim kiem don hang tren web de chinh sua
	 * 
	 * @author lacnv1
	 * @since Sep 26, 2014
	 */
	showSearchWebOrderDlg: function() {
		var html = $('#searchWebOrderDiv').html();
		$('#searchWebOrderPopup').dialog({
			closed: false,
	        cache: false,
	        modal: true,
	        width: $(window).width()- 50,
	        height: "auto",
	        title: create_order_tt_dh,
	        onOpen:function() {
	        	$("#searchWebOrderPopup").addClass("easyui-dialog");
	        	$('.InputTextStyle[id!=deliveryDate]').val('');
	    		$('.ErrorMsgStyle').hide();
	    		VTUtilJS.applyDateTimePicker("fromDateDlg");
	    		VTUtilJS.applyDateTimePicker("toDateDlg");
	    		var lockDate = $('#currentLockDay').val();
	    		$('#fromDateDlg').val(lockDate);
	    		$('#toDateDlg').val(lockDate);
	    		$('#orderNumberDlg').focus();
	    		
	    		$("#orderNumberDlg").parent().bind("keyup", function(event) {
	    			if (event.keyCode == keyCodes.ENTER) {
	    				$("#searchWebOrderPopup #btnSearchDlg").click();
	    			}
	    		});
	    		
	    		//load du lieu cho 2 combobox NVBH, NVGH
	    		/*var par = new Object();	
	    		if(SPCreateOrder._shopCodeFilter != null){
	    			par.shopCodeFilter = SPCreateOrder._shopCodeFilter;
	    		}					
	    		SPCreateOrder.loadStaffCbxs(par);*/
	    		SPCreateOrder.getStaffByShopCode();
	    		
	    		var params = {shopCodeFilter: $('#shopCodeCB').combobox('getValue'), fromDate:lockDate, toDate:lockDate};
	    		$('#searchWebOrderPopup #gridSearchBill').datagrid({
		  			  url: '/sale-product/create-order/search-web-order-not-approved',
		  			  rownumbers: true,
		  			  pagination: true,
		  			  fitColumns: true,
		  			  scrollbarSize: 0,
		  		      singleSelect: true,
		  			  pageSize: 20,
		  			  autoRowHeight: false,
		  			  height: "auto",
		  			  queryParams: params,
		  			  width: $(window).width() - 75,
		  			  frozenColumns: [[
						  {field:'select', title:create_order_chon, width:40, fixed:true, align:'center',sortable : false,resizable : false,formatter : function(value, row, index) {
			  					return "<a href='javascript:void(0)' class='cmsdefault' onclick='SPCreateOrder.loadSaleOrderToEdit("+index+");'><img width='16' height='16' src='/resources/images/icon_edit.png'/></a>";
			  				}},
						  {field: 'orderNumber', title: create_order_so_dh, width: 120, sortable:false,resizable:false, align: 'left'},
							{field: 'orderDate', title: create_order_ngay, width: 80, sortable:false,resizable:false, align: 'center',formatter:function(value,row,index){
								if (row.orderDate == null) {
									return '';
								}
								return $.datepicker.formatDate('dd/mm/yy', new Date(row.orderDate));
							}},
						  {field: 'customer.shortCode', title: create_order_khach_hang, width: 200, sortable:false,resizable:false, align: 'left',formatter: function(value,row, index){
						  	return (row.customer == null) ? "" : Utils.XSSEncode(row.customer.shortCode + " - " + row.customer.customerName);
						  } }
		  			  ]],
		  			  columns:[[
		  			    {field: 'customer.address', title: create_order_dia_chi, width: 200, sortable:false,resizable:false, align: 'left',formatter: function(value,row, index){
		  			    	return (row.customer == null) ? "" : Utils.XSSEncode(row.customer.address);
		  			    } },
		  			    {field: 'staff', title: create_order_nvbh, width: 80, sortable:false,resizable:false, align: 'left',formatter: function(value,row, index){
		  			    	return (row.staff == null) ? "" : Utils.XSSEncode(row.staff.staffCode);
		  			    } },
		  			    {field: 'delivery', title: create_order_nvgh, width: 80, sortable:false,resizable:false, align: 'left',formatter: function(value,row, index){
		  			    	return (row.delivery == null) ? "" : Utils.XSSEncode(row.delivery.staffCode);
		  			    } },
		  			    {field: 'quantity', title: create_order_total_sale_quan, width: 80, sortable:false,resizable:false, align: 'right',formatter: function(value,row, index){
		  			    	return (row.quantity == null) ? "" : Utils.XSSEncode(row.quantity);
		  			    } },
		  			    {field: 'amount', title: sale_product_customer_thanh_tien, width: 80, sortable:false,resizable:false, align: 'right',formatter: function(value,row, index){
		  			    	return (row.amount == null) ? "" : formatCurrencyInterger(row.amount);
		  			    } }
		  			  ]],
		  			  onLoadSuccess: function(data) {
		  				if (!SPCreateOrder._isAllowShowPrice){
		  					$('#searchWebOrderPopup #gridSearchBill').datagrid('hideColumn', 'amount');
		  				}
		  				setTimeout(function(){
			    			CommonSearchEasyUI.fitEasyDialog("searchWebOrderPopup");
			    		}, 1000);
		  			  }
		  		});
	        },
	        onClose: function() {
	        	$('#searchWebOrderPopup').dialog("destroy");
	        	$('#searchWebOrderDiv').html(html);
	        }
		});
	},
	
	/**
	 * Tim kiem don hang web de chinh sua
	 * 
	 * @author lacnv1
	 * @since Sep 26, 2014
	 */
	searchWebSaleOrder : function() {
		$('.ErrorMsgStyle').hide();
		var msg = '';	
		var fDate = $('#fromDateDlg').val().trim();
		var tDate = $('#toDateDlg').val().trim();
		if(fDate == '__/__/____'){
			$('#fromDateDlg').val('');
		}
		if(tDate == '__/__/____'){
			$('#toDateDlg').val('');
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfInvalidFormatDate('fromDateDlg', 'Từ ngày');
		}
		if(msg.length ==0){
			msg = Utils.getMessageOfInvalidFormatDate('toDateDlg', 'Đến ngày');
		}
		
		if(msg.length == 0 && !Utils.compareDate(fDate, tDate)){
			msg = msgErr_fromdate_greater_todate;
			$('#fromDateDlg').focus();
		}
		if(msg.length > 0){
			$('.easyui-dialog #errorSearchMsg').html(msg).show();
			return;
		}
		var params = {
				shopCodeFilter: $("#shopCodeCB").combobox('getValue'),
				code: $("#orderNumberDlg").val().trim(),
				fromDate: $("#fromDateDlg").val().trim(),
				toDate: $("#toDateDlg").val().trim(),
				customerCode: $("#shortCodeDlg").val().trim(),
				name: $("#customerNameDlg").val().trim(),
				salerCode: $("#staffCodeDlg").combobox('getValue'),
				deliveryCode: $("#deliveryCodeDlg").combobox('getValue')
		};
		$('#searchWebOrderPopup #gridSearchBill').datagrid('load', params);
	},
	
	loadSaleOrderToEdit: function(idx) {
		var rs = $("#searchWebOrderPopup #gridSearchBill").datagrid("getRows");
		var r = rs[idx];
		var url = "/sale-product/create-order/get-sale-order-for-adjustment";
		Utils.getJSONDataByAjax({orderId:r.id, shopCodeFilter: $('#shopCodeCB').combobox('getValue')}, url, function(data) {
			SPAdjustmentOrder._firstLoadOrderForEdit = true;
			var so = data.saleOrder;
			if (!so) {
				return;
			}
			
			$("#orderNumber").val(Utils.XSSEncode(so.orderNumber));
			$('#orderDate').val($.datepicker.formatDate('dd/mm/yy', new Date(so.orderDate)));
			$("#saleOrderId").val(so.id);
			$('.BreadcrumbSection li:last span').html('Sửa đơn bán hàng');
			
			//$("#customerCode").val(Utils.XSSEncode(so.customer.shortCode));
			//SPCreateOrder.customerDetails(so.customer.shortCode, so.staff.staffCode, false);
			
			if (so.customer) {
				$("#customerCode").val(Utils.XSSEncode(so.customer.shortCode));
				$("#customerName").html(Utils.XSSEncode(so.customer.customerName));
				$("#address").html(Utils.XSSEncode(so.customer.address));
				var phone = "";
				if(so.customer.phone != undefined && so.customer.phone != null){
					phone += so.customer.phone;
				}
				if(so.customer.mobiphone != undefined && so.customer.mobiphone != null){
					phone += (" / " + so.customer.mobiphone);
				}
				$("#phone").html(Utils.XSSEncode(phone));
			}
			$("#priorityId").val(so.priority).change();
			if (so.deliveryDate) {
				$("#deliveryDate").val($.datepicker.formatDate('dd/mm/yy', new Date(so.deliveryDate)));
			} else {
				$("#deliveryDate").val("");
			}
			if (so.car) {
				$("#carId").combobox("setValue", so.car.id);
			}
			if (so.staff) {
				$("#staffSaleCode").combobox("setValue", Utils.XSSEncode(so.staff.staffCode));
			}
			if (so.cashier) {
				$("#cashierStaffCode").combobox("setValue", Utils.XSSEncode(so.cashier.staffCode));
			}
			if (so.delivery) {
				$("#transferStaffCode").combobox("setValue", Utils.XSSEncode(so.delivery.staffCode));
			}
			$(".easyui-dialog").dialog("close");
			$("#staffSaleCode").combobox("disable");
			$("#customerCode").attr("disabled", "disabled");
			
			$('#cashierStaffCode').combobox('enable');
			$('#cashierStaffCode').parent().removeClass('BoxDisSelect');
			
			$('#transferStaffCode').combobox('enable');
			$('#transferStaffCode').parent().removeClass('BoxDisSelect');
			
			SPAdjustmentOrder._orderId = so.id;
			$("#loadingProduct").show();
			SPAdjustmentOrder.loadSaleOrderForAdjustment(so.id, {
				priceWarning: data.priceWarning,
				shopCode: $('#shopCodeCB').combobox('getValue')
			});
			
			//Có sản phẩm sai giá, bắt buộc phải tính tiền lại
			if(data.priceWarning!=undefined && data.priceWarning!=null && data.priceWarning==1){
				disabled('btnOrderSave');
				enable('payment');
				setTimeout(function(){
					$('#GeneralInforWarning').html('Đơn hàng có sản phẩm sai giá, yêu cầu tính tiền lại.').show();
					setTimeout(function(){$('#GeneralInforWarning').hide();},5000);
				},3000);
			}
		});
		//window.location.href = url;
	},
	
	/**
	 * Huy don hang tren web
	 * 
	 * @author lacnv1
	 */
	cancelWebOrder: function() {
		var orderId = SPAdjustmentOrder._orderId;
		if (orderId && Number(orderId) > 0) {
			var params = {
					orderId: orderId,
					approvedStep: 1
			};
			Utils.addOrSaveData(params, '/sale-product/create-order/cancel-web-order', null, 'errMsgOrder', function(data) {
				if(!data.error){
					$('#successMsg').html('Hủy đơn hàng thành công').show();
					setTimeout(function() {
						$('#successMsg').hide();
						SPCreateOrder.resetOrder();
					}, 3000);
				}
			}, null, null, false, "Bạn có muốn hủy đơn hàng?", null, 'hide');
			return;
		}
		SPCreateOrder.resetOrder();
	},
	initUnitCbx: function(){				
		var params = {};
		$.ajax({
			type : "POST",
			url:'/commons/list-child-shop',
			data :($.param(params, true)),
			dataType : "json",
			success : function(data) {
				listNVBH = data.lstNVBH;
				$('#shopCodeCB').combobox({
					valueField: 'shopCode',
					textField:  'shopName',
					data: data.rows,
					panelWidth: '206',
					formatter: function(row) {
						return '<span style="font-weight:bold">' + Utils.XSSEncode(row.shopCode) + '</span><br/>' + '<span style="color:#888">' + Utils.XSSEncode(row.shopName) + '</span>';
					},
					filter: function(q, row){
						q = new String(q).toUpperCase().trim();
						var opts = $(this).combobox('options');
						return ((unicodeToEnglish(row[opts.textField]).indexOf(unicodeToEnglish(q)) == 0) || unicodeToEnglish(row[opts.valueField]).indexOf(unicodeToEnglish(q)) == 0) ;
					},	
			        onSelect: function(rec){
			        	if (rec != null && rec.shopCode != SPCreateOrder._currentShopCode){
			        		var htSale = $('#gridSaleData').handsontable('getInstance');
							var data = htSale.getData();
							if (data.length > 0) {
								var row = data[0];
								if (!isNullOrEmpty(row[0])) {
									$.messager.confirm(jsp_common_xacnhan,'Danh sách sản phẩm sẽ bị xóa khi chọn lại Đơn vị. <br /> <b>Bạn có muốn chọn Đơn vị mới?</b>',function(r){  
									    if (r){ 					    	
									    	SPCreateOrder.resetChangeShop(rec);
									    } else {
									    	$('#shopCodeCB').combobox('setValue', Utils.XSSEncode(SPCreateOrder._currentShopCode));
									    }
									});
								} else {
									SPCreateOrder.resetChangeShop(rec);
								}
							} else {
								SPCreateOrder.resetChangeShop(rec);
							}
			        	}
			        },
			        onLoadSuccess: function(){
			        	var arr = $('#shopCodeCB').combobox('getData');
			        	if (arr != null && arr.length > 0){
			        		$('#shopCodeCB').combobox('select', arr[0].shopCode);
			        		SPCreateOrder._currentShopCode = Utils.XSSEncode(arr[0].shopCode);
			        	}
			        	
			        }
				});
			}
		});
	},
	resetChangeShop: function(rec){
		if (rec != undefined && rec != null){
			SPCreateOrder._currentShopCode = rec.shopCode;
	    	SPCreateOrder._shopCodeFilter = rec.shopCode;
	    	SPCreateOrder.resetOrder();
            SPCreateOrder.getStaffByShopCode();
            SPCreateOrder.getOrderDate();
		}
	},
	getOrderDate:function(){
		var params = new Object();
	   	params.shopCode = SPCreateOrder._currentShopCode;
	   	Utils.getJSONDataByAjax(params,'/catalog/utility/closing-day/check', function(data) {
	   		if (data.appDate != undefined && data.appDate != null) {
	   			SPCreateOrder.orderDate = data.appDate;//set biến orderDate truoc orderdate trên giao diện để khi so sánh coi như ko đổi => ko confirm thay đổi orderDate 
	   			$('#orderDate').val(data.appDate).change();
	   			$('#deliveryDate').val(data.appDate).change();
	   			$('#currentLockDay').val(data.appDate).change();
	   			SPCreateOrder._lockDate = data.appDate;
	   			SPCreateOrder.checkOrderDateOnLoadPage();
	   		}
	   	});
	},
	checkOrderDate:function(){
		var msg = '';
		var orderDate = $('#orderDate').val().trim();
		var lockDate = SPCreateOrder._lockDate != '' ? SPCreateOrder._lockDate : ReportUtils.getCurrentDateString();
		var currentDate = ReportUtils.getCurrentDateString();
		var deliveryDate = $('#deliveryDate').val();
		if (orderDate.length == 0) {
			msg = create_order_order_date_null;
		} else if(Utils.compareDateInt(lockDate, orderDate) > 0){
			msg = create_order_order_date_lock_date;
		} else if (Utils.compareDateInt(orderDate, currentDate) > 0) {
			msg = create_order_order_date_current_date;
		} else if (deliveryDate.length > 0 && Utils.compareDateInt(deliveryDate, orderDate) < 0) {
			msg = create_order_order_date_delivery_date;
		}
		if (msg.length > 0) {
			$('#orderDate').focus();
		}
		return msg;
	},
	changeOrderDate:function() {
		if ($('#orderDate').val() != SPCreateOrder.orderDate) {
			$.messager.confirm(jsp_common_xacnhan,'Thay đổi ngày đơn hàng sẽ load lại nhân viên và đơn hàng. <br /><b>Bạn có muốn tiếp tục không ?</b>',function(r){  
				if(r){
					SPCreateOrder.orderDate = $('#orderDate').val();
					SPCreateOrder.customerDetails($('#customerCode').val());
					SPCreateOrder.isShowDialogNVBHConfirm = false;//để không confirm nvbh
					var deliveryDate = $('#deliveryDate').val();
					var orderDate = $('#orderDate').val().trim();
					if (deliveryDate.length > 0 && Utils.compareDateInt(deliveryDate, orderDate) < 0) {
						$('#deliveryDate').val(orderDate);
					}
				} else {
					$('#orderDate').val(SPCreateOrder.orderDate).change();
				}
			});
		}
	},
	checkOrderDateOnLoadPage:function(){
		$('#GeneralInforErrors').hide();
		var msg = SPCreateOrder.checkOrderDate();
		if (msg.length > 0 && msg != create_order_order_date_delivery_date) {
			SPCreateOrder.resetStaff();
			disabled('customerCode');
			$('#GeneralInforErrors').html(msg).show();
		} else {
			enable('customerCode');
		}
	},
	changeColumnIndex: function(isShowPrice){
		/*var COLSALE = {
				PRO_CODE: 0, PRO_NAME: 1, PACKING_PRICE: 2, PRICE: 3, WAREHOUSE: 4, STOCK: 5, QUANTITY: 6, AMOUNT: 7, DISCOUNT: 8, PROGRAM: 9, DELETE: 10
			};*/
		if (isShowPrice != undefined && isShowPrice != null && !isShowPrice){
			COLSALE.PRO_CODE = 0;
			COLSALE.PRO_NAME = 1;
			COLSALE.WAREHOUSE = 2;
			COLSALE.STOCK = 3;
			COLSALE.QUANTITY = 4;
			COLSALE.DISCOUNT = 5;
			COLSALE.PROGRAM = 6;
			COLSALE.DELETE = 7;
			COLSALE.PACKING_PRICE = 8;
			COLSALE.PRICE = 9;
			COLSALE.AMOUNT = 10;
		} else {
			COLSALE.PRO_CODE = 0;
			COLSALE.PRO_NAME = 1;
			COLSALE.PACKING_PRICE = 2;
			COLSALE.PRICE = 3;
			COLSALE.WAREHOUSE = 4;
			COLSALE.STOCK = 5;
			COLSALE.QUANTITY = 6;
			COLSALE.AMOUNT = 7;
			COLSALE.DISCOUNT = 8;
			COLSALE.PROGRAM = 9;
			COLSALE.DELETE = 10;			
		}
	},
	showValidateError: function(errText){
		$('#generalInfoDiv #GeneralInforErrors').html(errText).show();											
		setTimeout(function() {
			$('#generalInfoDiv #GeneralInforErrors').hide();
		}, 3000);
	}
};

var GridSaleBusiness = {
	_mapWarehouse: new Map(),//danh sach Kho
	_mapWarehouseIdSelect: new Map(),//Danh sach ID Kho da chon theo index
	listCS : new Array(), //danh sach cthttm
	listCSCode : new Array(), //danh sach ma cthttm
	listSaleProduct : new Array(), //danh sach san pham
	listSaleProductCode : new Array(),//danh sach ma san pham
	listCellMerge: new Array(),//list cell can gom nhom
	mapCS: new Map(), //map danh sach cthttm
	mapSaleProduct : new Map(), //map danh sach san pham
	mapProductLot: null,//map lo cua san pham
	mapSaleProductGrid: new Map(),//map danh sach san pham ban
	isEditGridSale: false,//quyen edit grid sp ban hay ko
	_stt: 'STT',
	_productCode:'Mã sản phẩm',
	_productName:'Tên sản phẩm',
	_price:'Đơn giá',
	_priceOUM1: 'Giá thùng',
	_priceOUM2: 'Giá lẻ',
	_warehouse:'Kho',
	_availableQuantity:'Tồn kho đáp ứng',
    _quantity:'Thực đặt',
    _amount:'Thành tiền',
    _discount:'CKMH',
    _commercialSupport:'CT HTTM',
    _deleteSaleProduct:'',
    _isvalidQuantity:true,
	/**
	 * Ham formatter cho cac column
	 * @param instance: gia tri hien hanh khi thao tac grid
	 * @param td: table td cu ly cac thuoc tinh cua td 
	 * @param row: thu tu cua row
	 * @param col: thu tu cua column
	 * @param prop: cac thuoc tinh cua grid
	 * @param value: gia tri cua cell
	 * @param cellProperties: thuoc tinh cua cell khi format
	 * */
	styleTextStockNegative: function (instance, td, row, col, prop, value, cellProperties) {
		Handsontable.renderers.TextRenderer.apply(this, arguments);
		var proCode = $('#gridSaleData').handsontable('getInstance').getDataAtCell(row, COLSALE.PRO_CODE);
		var availableQuantity =  $('#gridSaleData').handsontable('getInstance').getDataAtCell(row, COLSALE.STOCK);
		var pro = GridSaleBusiness.mapSaleProduct.get(proCode);
		var warehouseId = GridSaleBusiness._mapWarehouseIdSelect.get(row);
		if(col == COLSALE.PRO_NAME || col == COLSALE.PACKING_PRICE || col == COLSALE.PRICE || col == COLSALE.STOCK || col == COLSALE.AMOUNT 
			 || col == COLSALE.QUANTITY){
			if(!isNullOrEmpty(proCode)){
				if (col == COLSALE.PACKING_PRICE || col == COLSALE.PRICE || col == COLSALE.STOCK || col == COLSALE.AMOUNT || col == COLSALE.DISCOUNT || col == COLSALE.QUANTITY) {
					td.style.textAlign = "right";
				} else if (col == COLSALE.PRO_NAME) {
					td.style.textAlign = "left";
				}
				if(pro != null){
					if (warehouseId != null && warehouseId != undefined) {
						var warehouse = GridSaleBusiness._mapWarehouse.get(warehouseId);
						if (warehouse != null && warehouse != undefined) {
							availableQuantity = StockValidateInput.getQuantity(availableQuantity, pro.convfact);
							if (availableQuantity < 0) {
								td.style.color = WEB_COLOR.RED_CODE;
							} else {
								var quanConv = $('#gridSaleData').handsontable('getInstance').getDataAtCell(row, COLSALE.QUANTITY);
								if(!isNullOrEmpty(quanConv)){
									var quan = StockValidateInput.getQuantity(quanConv, pro.convfact);
									if(quan > availableQuantity){
										td.style.color = WEB_COLOR.ORANGE_CODE;
									};
								};
							};
						}
					}
				};
			};
		} else if (col == COLSALE.WAREHOUSE) {
			if (pro != null) {
				if (warehouseId != null) {
					var warehouse = GridSaleBusiness._mapWarehouse.get(warehouseId);
					if (warehouse != null && warehouse != undefined) {
						var btnChangeWarehouseIdStr = "btnChangeWarehouse_" + row;
						html = '<a class="delIcons" id="' + btnChangeWarehouseIdStr + (SPCreateOrder._isCreatingSaleOrder ? '" onclick="return SPCreateOrder.openSelectProductWarehouseDialog(\''+Utils.XSSEncode(proCode)+'\',\''+row+'\', SPCreateOrder.productWarehouseType.SALE_PRODUCT);"' : '') + ' href="javascript:void(0)">'+Utils.XSSEncode(warehouse.warehouseName)+'</a>';
						td.innerHTML = html;
						td.align = 'center';
					};
				};
			};
		} else if (col == COLSALE.DISCOUNT) {
			if (pro != null && value != null) {
				html = '<a class="delIcons" onclick="SPCreateOrder.showDiscountPromoDetail(\'' + Utils.XSSEncode(proCode) + '\',\'' + row + '\')" href="javascript:void(0)">' + Utils.XSSEncode(value) + '</a>';
				td.innerHTML = html;
				td.align = 'right';
			};
		} else if (col == COLSALE.PROGRAM) {
			var programCode = $('#gridSaleData').handsontable('getInstance').getDataAtCell(row, COLSALE.PROGRAM);
			if (!isNullOrEmpty(programCode)) {
				var lst = programCode.split(',');
				html = '<a class="delIcons" onclick="return SPCreateOrder.showPromotionProgramDetail(\'' + Utils.XSSEncode(lst[0].trim()) + '\');" href="javascript:void(0)">' + Utils.XSSEncode(lst[0]) + '</a>';
				for (var i = 1 ; i < lst.length ; i++) {
					html += ', <a class="delIcons" onclick="return SPCreateOrder.showPromotionProgramDetail(\'' + Utils.XSSEncode(lst[i].trim()) + '\');" href="javascript:void(0)">' + Utils.XSSEncode(lst[i]) + '</a>';
				}
				td.innerHTML = html;
				td.align = 'center';
			};
		};
		//cuonglt3 neu cau hinh khong show gia thi an 3 cot
		if (col == COLSALE.PACKING_PRICE || col == COLSALE.PRICE || col == COLSALE.AMOUNT) {
			if (!SPCreateOrder._isAllowShowPrice) {
				td.hidden = true;
			};
		};
	},
	
	/**
	 * Ham format dong dau tien cua grid
	 */
	firstRowRenderer: function(instance, td, row, col, prop, value, cellProperties) {
		  Handsontable.renderers.TextRenderer.apply(this, arguments);
		  td.style.fontWeight = 'bold';
		  td.style.color = 'green';
		  td.style.background = '#CEC';
	},
	
	/**
	 * Ham lay thong tin row cua cell can merge
	 * @param row
	 */
	getRowCellMerge: function(row){
		var result = new Array();
		for(var i = 0; i< GridSaleBusiness.listCellMerge.length;i++){
			var startRow = GridSaleBusiness.listCellMerge[i].row;
			var endRow = GridSaleBusiness.listCellMerge[i].rowspan + startRow - 1;
			if(row >= startRow && row <= endRow){
				while(row<=endRow){
					result.push(row++);
				}
				break;
			}
		}
		return result;
	},
	
	/**
	 * Ham lay thu tu cell cua row can merge
	 * @param row
	 */
	getIndexCellMerge: function(row){
		var result = new Array();
		for(var i = 0; i< GridSaleBusiness.listCellMerge.length;i++){
			if(GridSaleBusiness.listCellMerge[i].row = row){
				result.push(i);
			}
		}
		return result;
	},
	
	/**
	 * Ham reset list merge cell
	 * @param row
	 * @param numberRowDelete
	 */
	resetListCellMerge: function(row, numRowDel){
		for(var i = 0; i< GridSaleBusiness.listCellMerge.length;i++){
			var r = GridSaleBusiness.listCellMerge[i].row;
			if(r > row + numRowDel - 1){
				GridSaleBusiness.listCellMerge[i].row = r - numRowDel;
			}
		}
	},
	
	/**
	 * Ham lay thong tin cac cot cua grid
	 */
	getColumnProperty: function(){
		var col = [
           //product code
			{
				type : 'autocomplete',
				source : GridSaleBusiness.listSaleProductCode,
				strict: false,
				allowInvalid: false,
				renderer: GridSaleBusiness.productCodeRenderer
			},
			
			//product name
			{
				readOnly: true,
				renderer: GridSaleBusiness.styleTextStockNegative
			},
			
			//price thung
			{   
				 readOnly: !SPCreateOrder._isAllowModifyPrice,				 
//				readOnly: false,
//				 type: 'numeric',
//				 format: '0,0.00',
				renderer: GridSaleBusiness.styleTextStockNegative
			},
			//price le
			{
				 readOnly: !SPCreateOrder._isAllowModifyPrice,
//				readOnly: false,
//				 type: 'numeric',
//				 format: '0,0.00',
				renderer: GridSaleBusiness.styleTextStockNegative
			},
			//warehouse
			{
				readOnly: true,
				renderer: GridSaleBusiness.styleTextStockNegative
			},
			//stock
			{
				readOnly: true,
				renderer: GridSaleBusiness.styleTextStockNegative
			},
			
			//quantity
			{
				renderer: GridSaleBusiness.styleTextStockNegative,
//				validator: SPCreateOrder.convfactValidation,
				allowInvalid: false
			},
			
			//amount
			{
				readOnly: true,
//				type: 'numeric',
//			    format: '0,0.00',
				renderer: GridSaleBusiness.styleTextStockNegative
			},
			//discount
			{
				readOnly: true,
				renderer: GridSaleBusiness.styleTextStockNegative
			},
			//cthttm
			{
				type : 'autocomplete',
				source : GridSaleBusiness.listCSCode,
				renderer: GridSaleBusiness.styleTextStockNegative
			},
			//delete
			{
				renderer: GridSaleBusiness.deleteItemRenderer
			},
			
		];
		var colNotShowPrice = [
		           //product code
					{
						type : 'autocomplete',
						source : GridSaleBusiness.listSaleProductCode,
						strict: false,
						allowInvalid: false,
						renderer: GridSaleBusiness.productCodeRenderer
					},
					
					//product name
					{
						readOnly: true,
						renderer: GridSaleBusiness.styleTextStockNegative
					},
					
					//warehouse
					{
						readOnly: true,
						renderer: GridSaleBusiness.styleTextStockNegative
					},
					//stock
					{
						readOnly: true,
						renderer: GridSaleBusiness.styleTextStockNegative
					},
					
					//quantity
					{
						renderer: GridSaleBusiness.styleTextStockNegative,
//						validator: SPCreateOrder.convfactValidation,
						allowInvalid: false
					},
					//discount
					{
						readOnly: true,
						renderer: GridSaleBusiness.styleTextStockNegative
					},
					//cthttm
					{
						type : 'autocomplete',
						source : GridSaleBusiness.listCSCode,
						renderer: GridSaleBusiness.styleTextStockNegative
					},
					//delete
					{
						renderer: GridSaleBusiness.deleteItemRenderer
					},
					
				];
		if (SPCreateOrder._isAllowShowPrice) {
			return col;
		}
		return colNotShowPrice;
	},
	
	/**
	 * Ham set property cell trong hansontable
	 */
	deleteItemRenderer: function(instance, td, row, col, prop, value, cellProperties) {
		if ($("#gridSaleData").handsontable("getCellMeta", 0, COLSALE.DELETE).readOnly) {
			Handsontable.renderers.TextRenderer.apply(this, arguments);
			td.innerHTML = '';
			td.align = 'center';
		} else {
			Handsontable.renderers.TextRenderer.apply(this, arguments);
			var btnRemoveProductIdStr = "btnRemoveProduct_" + row;
			html = '<a class="delIcons" id="' + btnRemoveProductIdStr + '"' + ' onclick="return GridSaleBusiness.deleteSaleProduct(\''+row+'\',\''+col+'\',\''+instance+'\');" href="javascript:void(0)"><img src="/resources/images/icon-delete.png" width="19" height="20" /></a>'
			td.innerHTML = html;
			td.align = 'center';
		}
	},
	
	/**
	 * Ham set product code 
	 */
	productCodeRenderer: function (instance, td, row, col, prop, value, cellProperties) {
		Handsontable.renderers.TextRenderer.apply(this, arguments);
		var html = '';
		if(value != null){
			if (SPCreateOrder._isViewOrder == true) {
				html = '<a id="product_'+ row + '" onclick="return SPSearchSale.viewProductDetails(\''+Utils.XSSEncode(value)+'\');" href="javascript:void(0)">'+Utils.XSSEncode(value)+'</a>'
			} else {
				html = '<a id="product_'+ row + '" onclick="return SPCreateOrder.viewProductDetails(\''+Utils.XSSEncode(value)+'\');" href="javascript:void(0)">'+Utils.XSSEncode(value)+'</a>'
			}
		}
		td.innerHTML = html;
	},

	/**
	 * restore warehouse's available quantity when delete a row on sale grid
	 * @author tuannd20
	 * @param  {var} r           deleting row
	 * @param  {var} productCode product's code at deleting row
	 * @return {var} 
	 * @date 14/10/2014
	 */
	restoreWarehouseAvailableQuantityWhenDeleteRow: function(r, productCode) {
		// restore quantity to de-select warehouse
		var htSale = $('#gridSaleData').handsontable('getInstance');
		var pro = GridSaleBusiness.mapSaleProduct.get(productCode);
		var warehouseId = Number(GridSaleBusiness._mapWarehouseIdSelect.get(r));
		var quantity = htSale.getDataAtCell(r, COLSALE.QUANTITY);
		if (pro) {
			quantity = StockValidateInput.getQuantity(quantity, pro.convfact);
			GridSaleBusiness.changeWarehouseAvailableQuantity(pro.productId, warehouseId, quantity);
		}		
	},
	
	/**
	 * Ham delete sp ban
	 */
	deleteSaleProduct: function(r,c,htSale) {
		r = Number(r);
		c = Number(c);
		htSale = $('#gridSaleData').handsontable('getInstance');
		var proCode = htSale.getDataAtCell(r,COLSALE.PRO_CODE);
		var pro = GridSaleBusiness.mapSaleProduct.get(proCode);
		htSale.deselectCell();
		$.messager.confirm(jsp_common_xacnhan,'Bạn có muốn xóa sản phẩm '+ Utils.XSSEncode(proCode) +'?',function(is){  
		    if (is){
			    	// restore quantity to de-select warehouse
			    	GridSaleBusiness.restoreWarehouseAvailableQuantityWhenDeleteRow(r, proCode);

			    	// if there isn't product with 'productCode' anymore, deleting product in 'SPCreateOrder._mapPRODUCTSbyWarehouseOnGrid' also.
			    	var count = 0;	// count product with 'productCode' (except row 'r') on sale grid
			    	for (var i = 0, row = htSale.getData(); i < row.length; row[i][COLSALE.PRO_CODE] == proCode && i != r ? count++ : null, i++);
			    	if (!count && pro) {
			    		SPCreateOrder._mapPRODUCTSbyWarehouseOnGrid.remove(pro.productId);
			    	}
			    	
	//		    	if(pro.checkLot == 1){
	//		    		//Xu ly khi co lo
	//		    		
	//		    	}else{
			    		SPCreateOrder._isRemovingRow = true;
			    		htSale.alter('remove_row',r);		    		
	//		    	}
			    	SPCreateOrder.showTotalGrossWeightAndAmount();
			    	htSale.selectCell(0,0);
			    	htSale.render();
			    	enable('payment');
				disabled('btnOrderSave');
				disabled('btnOrderReset');
		    }else{
		    		htSale.selectCell(r,c);
		    }
		});
	},
	
	/**
	 * Ham format Quantity cho SP ban
	 */
	formatStockQuantity:function(amount,convfact){
		if(amount==null || amount==undefined || amount.toString().length==0){
			return '';
		}
		if(convfact==null || convfact==undefined || convfact.toString().length==0){
			return '';
		}
		if (convfact == 0) convfact = 1;
		amount = amount.toString();
		if(amount.indexOf('/')>=0){
			var arrCount = amount.split('/');
			if(arrCount.length>=3){
				return '';
			}else{
				if(amount.split('/').length <= 1){
					return parseInt(amount,10) + '/0';						
				}
				if(amount.split('/')[0].length==0 || isNaN(amount.split('/')[0])){
					return '0' + '/' + parseInt(amount.split('/')[1],10);
				}
				if(amount.split('/')[1].length==0 || isNaN(amount.split('/')[1])){
					return parseInt(amount.split('/')[0],10) + '/0';
				}
				if(isNaN(amount.split('/')[0]) || isNaN(amount.split('/')[1])){
					return '0/0';
				}
				if(amount.split('/')[1]>convfact){
					var amount_temp = parseInt(amount.split('/')[0],10) + parseInt(amount.split('/')[1]/convfact);
					return amount_temp + '/' + parseInt(amount.split('/')[1])%convfact;
				}					
				return parseInt(amount.split('/')[0],10) + '/' + parseInt(amount.split('/')[1],10);	
			}
		}else{	
			if(isNaN(amount)){
				return '0/0';
			}
			if(parseInt(amount)>=convfact){
				var amount_temp = parseInt(amount/convfact,10);
				var amount_mod = amount%convfact;
				amount = parseInt(amount_temp,10) + '/' + parseInt(amount_mod,10);	
			}else{
				amount = '0/' + parseInt(amount,10);		
			}				
		}	
		return amount;
	},

	/**
	 * check if product's input quantity exceed total product's quantity in all warehouse
	 * @author tuannd20
	 * @param {var} productId product's id
	 * @param {var} inputQuantity product's input quantity
	 * @return {Boolean} true if exceed, otherwise, false.
	 * @date 14/10/2014
	 */
	isProductQuantityExceedTotalWarehouseQuantity: function(productId, inputQuantity, type) {
		var quantity = Number(inputQuantity);
		if (isNaN(quantity)) {
			return true;
		}
//		var productsWarehouses = SPCreateOrder._mapPRODUCTSbyWarehouseOnGrid.get(productId);
		var productsWarehouses = GridSaleBusiness.getWarehouseOnGridByType(productId, type);
		if ((productsWarehouses || []).length == 0) {
			return true;
		}
		/*
		 * calculate total available quantity of product in all warehouse
		 */
		var totalAvailableQuantity = productsWarehouses.reduce(function(prevVal, warehouse){
										return prevVal + (isNaN(Number(warehouse.availableQuantity)) ? 0 : Number(warehouse.availableQuantity));
									}, 0);
		return quantity > totalAvailableQuantity;
	},

	/**
	 * Clone product's warehouse data to a new map used to check if product's input quantity exceed total product's quantity.
	 * Won't clone data if it was cloned before.
	 * @author tuannd20
	 * @param  {var} productId id of processing product
	 * @return {void}
	 * @date 14/10/2014
	 */
	copyProductWarehouseData: function(productId) {
//		var productsWarehouses = SPCreateOrder._mapPRODUCTSbyWarehouse.get(productId) || [];
		var productsWarehouses = GridSaleBusiness.getWarehouseByType(productId) || [];
		if (productsWarehouses.length != 0 && GridSaleBusiness.getWarehouseOnGridByType(productId) == null) {
			var tmpProductsWarehouses = [];
			$.extend(true, tmpProductsWarehouses, productsWarehouses);
			SPCreateOrder._mapPRODUCTSbyWarehouseOnGrid.put(productId, tmpProductsWarehouses);
		}
	},

	/**
	 * get all selectable warehouse which having available_quanity > 0 of product
	 * @author tuannd20
	 * @param  {var} productId product id
	 * @return {Array} array of selectable warehouses
	 * @date 14/10/2014
	 */
	getAllSelectableWarehouseOfProduct: function(productId){
//		var warehousesOfProduct = SPCreateOrder._mapPRODUCTSbyWarehouseOnGrid.get(productId) || [];
		var warehousesOfProduct = GridSaleBusiness.getWarehouseOnGridByType(productId) || [];
		return warehousesOfProduct.filter(function(item){
				return item.availableQuantity > 0;
			});
	},

	/**
	 * find warehouse of product
	 * @author tuannd20
	 * @param  {var} productId product's id
	 * @param  {var} warehouseId warehouse's id
	 * @return {var} index of warehouse in SPCreateOrder._mapPRODUCTSbyWarehouseOnGrid.valArray
	 * @date 14/10/2014
	 */
	findWarehouseOfProduct: function(productId, warehouseId) {
//		var warehousesOfProduct = SPCreateOrder._mapPRODUCTSbyWarehouseOnGrid.get(productId) || [];
		var warehousesOfProduct = GridSaleBusiness.getWarehouseOnGridByType(productId) || [];
		for (var i = 0; i < warehousesOfProduct.length; i++) {
			if (Number(warehousesOfProduct[i].warehouseId) === Number(warehouseId)) {
				return i;
			}
		}
		return -1;
	},

	/**
	 * add or subtract warehouse's available quantity of product
	 * @author tuannd20
	 * @param  {var} productId product's id
	 * @param  {var} warehouseId warehouse's id
	 * @param  {var} changeQuantity change quantity
	 * @return {void}
	 * @date 14/10/2014
	 */
	changeWarehouseAvailableQuantity: function(productId, warehouseId, changeQuantity) {
//		var foundIndex = GridSaleBusiness.findWarehouseOfProduct(productId, warehouseId);
//		if (foundIndex != -1) {
//			GridSaleBusiness.getWarehouseOnGridByType(productId)[foundIndex].availableQuantity += Number(changeQuantity);
//		}
	},

	/**
	 * change temp data when insert a row on sale grid
	 * @author tuannd20
	 * @param  {Integer} row row's index
	 * @return {void}
	 * @date 16/10/2014
	 */
	changeTempDataWhenInsertARowOnSaleGrid: function(row) {
		/*for (var i = row; i < GridSaleBusiness._mapWarehouseIdSelect.keyArray.length; i++) {
			GridSaleBusiness._mapWarehouseIdSelect.keyArray[i]++;
		}*/
		for (var i = 0; i < GridSaleBusiness._mapWarehouseIdSelect.keyArray.length; i++) {
			if (GridSaleBusiness._mapWarehouseIdSelect.keyArray[i] >= row) {
				GridSaleBusiness._mapWarehouseIdSelect.keyArray[i]++;
			}			
		}
	},
	
	getWarehouseOnGridByType:function(productId, type) {
		var productsWarehouses = SPCreateOrder._mapPRODUCTSbyWarehouseOnGrid.get(productId);
		if (productsWarehouses == undefined || productsWarehouses == null) {
			productsWarehouses = SPCreateOrder._mapPRODUCTSbyWarehouse.get(productId);
		}
		if (type == undefined || type == null || productsWarehouses == null) {
			return productsWarehouses;
		}
		if (SPCreateOrder._isDividualWarehouse == true) {
			var lst = new Array();
			for (var i = 0 ; i < productsWarehouses.length ; i++) {
				if (productsWarehouses[i].warehouseType == type) {
					lst.push(productsWarehouses[i]);
				}
			}
			return jQuery.extend(true, [], lst);
		} else {
			return GridSaleBusiness.orderByProductsByWarehouse(jQuery.extend(true, [], productsWarehouses), type);
		}
	},
	
	getWarehouseByType:function(productId, type) {
		var productsWarehouses = SPCreateOrder._mapPRODUCTSbyWarehouse.get(productId);
		if (type == undefined || type == null || productsWarehouses == null) {
			return productsWarehouses;
		}
		if (SPCreateOrder._isDividualWarehouse == true) {
			var lst = new Array();
			for (var i = 0 ; i < productsWarehouses.length ; i++) {
				if (productsWarehouses[i].warehouseType == type) {
					lst.push(productsWarehouses[i]);
				}
			}
			return jQuery.extend(true, [], lst);
		} else {
			return GridSaleBusiness.orderByProductsByWarehouse(jQuery.extend(true, [], productsWarehouses), type);
		}
	},
	
	getAvailableQuantityWarehouseOnGridByType:function(productId, type) {
		var productsWarehouses = GridSaleBusiness.getWarehouseByType(productId, type);
		if (productsWarehouses == null) return 0;
		var lst = new Array();
		if (type == undefined || type == null) {
			lst = productsWarehouses;
		} else {
			for (var i = 0 ; i < productsWarehouses.length ; i++) {
				if (productsWarehouses[i].warehouseType == type) {
					lst.push(productsWarehouses[i]);
				}
			}
		}
		var quantity = lst.reduce(function(prevQuantity, warehouse) {
				return prevQuantity + warehouse.availableQuantity;
			}, 0);
		return quantity;
	},
	
	orderByProductsByWarehouse:function(lst, type) {
		if (WarehouseType.SALE == type) {
			lst.sort(function (a, b) {
				if (a.warehouseType > b.warehouseType) {
					return 1;
				}
				if (a.warehouseType < b.warehouseType) {
					return -1;
				}
				if (a.seq > b.seq) {
					return 1;
				}
				if (a.seq < b.seq) {
					return -1;
				}				
				return 0;
			});
		} else {
			lst.sort(function (a, b) {
				if (a.warehouseType < b.warehouseType) {
					return 1;
				}
				if (a.warehouseType > b.warehouseType) {
					return -1;
				}
				if (a.seq > b.seq) {
					return 1;
				}
				if (a.seq < b.seq) {
					return -1;
				}
				return 0;
			});
		}
		return lst;
	},
	
	/**
	 * Ham khoi tao grid san pham ban
	 */
	initSaleGrid: function() {
		var width = $(window).width() - 55;
		var tmpWidth = 100+100+120+120+120+120+100+120+50;
		var columnWidths = new Object();
		var columnsHeader = new Object();
		if (SPCreateOrder._isAllowShowPrice) {
			columnWidths = [200, 280, 100, 100, 120, 120, 120, 120, 100,120,50];
			columnsHeader = [GridSaleBusiness._productCode,GridSaleBusiness._productName,
						 GridSaleBusiness._priceOUM1, GridSaleBusiness._priceOUM2,
			             GridSaleBusiness._warehouse,GridSaleBusiness._availableQuantity,GridSaleBusiness._quantity,
			             GridSaleBusiness._amount,GridSaleBusiness._discount,GridSaleBusiness._commercialSupport,
			             GridSaleBusiness._deleteSaleProduct];
		} else{
//			columnWidths = [200, 280, 120, 120, 120, 100, 120, 50];
			columnWidths = [200, 280, 150, 150, 150, 150, 150, 50];
			columnsHeader = [GridSaleBusiness._productCode,GridSaleBusiness._productName,
			             GridSaleBusiness._warehouse,GridSaleBusiness._availableQuantity,GridSaleBusiness._quantity,
			             GridSaleBusiness._discount,GridSaleBusiness._commercialSupport,
			             GridSaleBusiness._deleteSaleProduct];
		};
		$('#gridSaleData').handsontable({
			data: [],
			colWidths: columnWidths,
			width:$(window).width() - 55,
			rowHeaders: true,
			outsideClickDeselects: true,
			multiSelect : false,
			fillHandle: true,
			comments: true,
//			contextMenu: {
////			    callback: function (key, options) {
////			      if (key === 'about') {
////			        setTimeout(function () {
////			          //timeout is used to make sure the menu collapsed before alert is shown
////			          alert("This is a context menu with default and custom options mixed");
////			        }, 100);
////			      }
////			    },
//			    items: {
//			      "remove_row": {
//			        name: 'Xóa sản phẩm',
//			        callback: function(key, opt) {
//			        	if (opt.end != null && opt.end != undefined) {
//			        		GridSaleBusiness.deleteSaleProduct(opt.end.row, opt.end.col, null);
//			        	}
//		            }
//			      },
//			      "hsep1": "---------",
//			    }
//			},
            cells: function (row, col, prop) {
			},
			// colHeaders: [GridSaleBusiness._productCode,GridSaleBusiness._productName,
			// 			 GridSaleBusiness._priceOUM1, GridSaleBusiness._priceOUM2,
			//              GridSaleBusiness._warehouse,GridSaleBusiness._availableQuantity,GridSaleBusiness._quantity,
			//              GridSaleBusiness._amount,GridSaleBusiness._discount,GridSaleBusiness._commercialSupport,
			//              GridSaleBusiness._deleteSaleProduct],
			colHeaders : columnsHeader,
			columns: GridSaleBusiness.getColumnProperty(),
			//Ham xu ly du lieu sau thay doi
			afterChange: function(changes, source) {//SPCreateOrder
				SPCreateOrder.changeColumnIndex(SPCreateOrder._isAllowShowPrice);
				if(changes != null && $.isArray(changes)) {
					DEBUG && !SPCreateOrder._syncFlag.isCheckInputQuantityValid ? console.log("afterChange") : null;
					DEBUG && !SPCreateOrder._syncFlag.isCheckInputQuantityValid ? console.log(changes) : null;
					var htSale = $('#gridSaleData').handsontable('getInstance');
					for(var i = 0; i < changes.length; i++) {
						var row = changes[i];
						var r = row[0];
						var c = row[1];
						var oldValue = row[2];
						var newValue = row[3];
						var isEditDiscount = 0;
						
						//START DEBUG TUNGMT
//						if (typeof tungmt == 'undefined') {
//							tungmt = 0;
//							dem = 0;
//						}
//						tungmt++;
//						console.log('Đếm: '+ tungmt);
//						if (tungmt == dem) {
//							console.log('zo');
//						}
//						console.log(oldValue + ' - ' +newValue);
						//END DEBUG TUNGMT----------------------------------------------
						
						//Tu dong fill ten sp, don gia, ton kho dap ung, ctkm khi chon sp
						if(newValue != oldValue && newValue != null){
							switch(c){
							//Khi nhap gia tri vao cot Ma SP
							case COLSALE.PRO_CODE: {
								var oldProduct = GridSaleBusiness.mapSaleProduct.get(oldValue);
								var newProduct = GridSaleBusiness.mapSaleProduct.get(newValue);
								var isSelect = true;
								if(newProduct != null & isSelect) {
									if (oldValue && oldProduct) {
										/*
										 * restore previous quantity (if any)
										 */
										var warehouseId = Number(GridSaleBusiness._mapWarehouseIdSelect.get(r));
										var currentProductQuantity = htSale.getDataAtCell(r, COLSALE.QUANTITY);
										if (oldValue != null) {
											var tmpQuantity = GridSaleBusiness.isValidQuantityInput(currentProductQuantity) ? currentProductQuantity : 0;
											tmpQuantity = StockValidateInput.getQuantity(tmpQuantity, oldProduct.convfact);
											GridSaleBusiness.changeWarehouseAvailableQuantity(oldProduct.productId, warehouseId, tmpQuantity);
										}
									}


									//truong hop co lo
									if(newProduct.checkLot == 1){ 
																				
									}else{// truong hop khong co lo
										htSale.setDataAtCell(r, COLSALE.PRO_NAME, newProduct.productName, source);
										// clone product's warehouse data
										GridSaleBusiness.copyProductWarehouseData(newProduct.productId);

										htSale.setDataAtCell(r, COLSALE.PRICE, formatCurrency(newProduct.price), source);
										htSale.setDataAtCell(r, COLSALE.PACKING_PRICE, formatCurrency(newProduct.packagePrice), source);
										if(htSale.getDataAtCell(r, COLSALE.QUANTITY) != null){
											htSale.setDataAtCell(r, COLSALE.QUANTITY, null, source);
										}
										if (SPCreateOrder._syncFlag.isCheckInputQuantityValid) {
											setTimeout(function() {
												if(htSale.getDataAtCell(r, COLSALE.PROGRAM) != null){
													htSale.setDataAtCell(r, COLSALE.PROGRAM, null, source);
												}
												htSale.setDataAtCell(r, COLSALE.PRICE, formatCurrency(newProduct.price), source);
												htSale.setDataAtCell(r, COLSALE.PACKING_PRICE, formatCurrency(newProduct.packagePrice), source);
												if (newProduct.promotionProgramCode != null && SPCreateOrder._syncFlag.isSetDefaultProgram) {
													DEBUG ? console.log('--- set default program ---') : null;
													htSale.setDataAtCell(r, COLSALE.PROGRAM, newProduct.promotionProgramCode, source);
												}
											}, 100);
										}										
										
										htSale.setDataAtCell(r, COLSALE.DELETE,0, source);
										if (!SPCreateOrder._isInsertProductFromDialogToGrid) {
											/*
											 * chon kho cho san pham:
											 * theo thu tu uu tien cua cac kho, kho nao co ton kho > 0 dau tien, thi chon kho do.
											 * neu nhu tat ca cac kho deu co ton kho = 0 thi chon kho co do uu tien cao nhat
											 */
//											var productsWarehouses = SPCreateOrder._mapPRODUCTSbyWarehouseOnGrid.get(newProduct.productId);
											var productsWarehouses = GridSaleBusiness.getWarehouseOnGridByType(newProduct.productId, WarehouseType.SALE);
											if (productsWarehouses && productsWarehouses.length > 0) {
												var chooseWarehouseIndex = 0;	// index cua kho se duoc chon
												// bo qua cac kho co ton kho = 0
												for (; chooseWarehouseIndex < productsWarehouses.length && productsWarehouses[chooseWarehouseIndex].availableQuantity <= 0 
														&& productsWarehouses[chooseWarehouseIndex].warehouseType != WarehouseType.SALE; chooseWarehouseIndex++);
												if (chooseWarehouseIndex === productsWarehouses.length) {
													if (!SPCreateOrder._syncFlag.isCheckInputQuantityValid) {
														chooseWarehouseIndex = productsWarehouses.length - 1;
													} else {
														chooseWarehouseIndex = 0;
													}
												}
												
												/*
												 * set du lieu vao grid
												 */
												var selectedWarehouse = productsWarehouses[chooseWarehouseIndex];
												if (selectedWarehouse != undefined && selectedWarehouse != null) {
													if (!SPCreateOrder._syncFlag.isCheckInputQuantityValid) {	// truong hop dang tach kho
														selectedWarehouse = productsWarehouses.filter(function(wh){
																				return wh.warehouseId == GridSaleBusiness._mapWarehouseIdSelect.get(r);
																			})[0];
													}
													// update stock
													var warehousesOfProduct = GridSaleBusiness.getWarehouseByType(newProduct.productId) || [];
													chooseWarehouse = warehousesOfProduct.filter(function(item){
																		return item.warehouseId == selectedWarehouse.warehouseId;
																	});
													htSale.setDataAtCell(r, COLSALE.STOCK, GridSaleBusiness.formatStockQuantity(chooseWarehouse[0].availableQuantity, newProduct.convfact), source);
													
													// update temp data
													$('#product_'+r).attr('warehouseId', selectedWarehouse.warehouseId);
													GridSaleBusiness._mapWarehouseIdSelect.put(r, selectedWarehouse.warehouseId);
													htSale.setDataAtCell(r, COLSALE.WAREHOUSE, selectedWarehouse.warehouseName, source);
												}
											} else {
												htSale.setDataAtCell(r, COLSALE.STOCK, '', source);
												htSale.setDataAtCell(r, COLSALE.WAREHOUSE, '', source);
												// update temp data
												$('#product_'+r).attr('warehouseId', '');
												GridSaleBusiness._mapWarehouseIdSelect.remove(r);
												$('#GeneralInforErrors2').html('Sản phẩm ' + Utils.XSSEncode(newProduct.productCode) + ' không có tồn kho. Vui lòng nhập kho cho sản phẩm').show();
												setTimeout(function() {
													$('#GeneralInforErrors2').hide();
												}, 3000);
												return;
											}
										} else {
											$('#product_'+r).attr('warehouseId', SPCreateOrder._selectedWarehouse.get(r).warehouseId);
											htSale.setDataAtCell(r, COLSALE.STOCK, GridSaleBusiness.formatStockQuantity(SPCreateOrder._selectedWarehouse.get(r).availableQuantity, newProduct.convfact), source);
										}
										htSale.selectCell(r, COLSALE.QUANTITY);
									}
								} else if (newProduct == null) {
									htSale.setDataAtCell(r, COLSALE.PRO_CODE,null, source);
//									htSale.selectCell(r,COLSALE.PRO_CODE);
								}
								
								//reset gia tri CK khi chon lai SP
								if (SPCreateOrder._syncFlag.isCheckInputQuantityValid) {
									htSale.setDataAtCell(r, COLSALE.PROGRAM,'', source);
									htSale.setDataAtCell(r, COLSALE.DISCOUNT,'', source);
									SPCreateOrder._mapDiscountAmount.remove(r);
								}
								
								SPCreateOrder.showTotalGrossWeightAndAmount();

								SPCreateOrder._syncFlag.isSettingProductCodeDone = true;
								SPCreateOrder._syncFlag.isSettingProgramDone = false;
								SPCreateOrder._syncFlag.processNextItem = false;
								DEBUG ? console.log('    setting product code done at ' + (new Date()).getTime()) : null;
							}
							break;
							case COLSALE.QUANTITY: {
								if(SPCreateOrder._syncFlag.processNextItem){
									break;
								}
								var newProductCode = htSale.getDataAtCell(r, COLSALE.PRO_CODE);
								var newProduct = GridSaleBusiness.mapSaleProduct.get(newProductCode);
								var program = htSale.getDataAtCell(r, COLSALE.PROGRAM);
								htSale.setDataAtCell(r, COLSALE.DISCOUNT, null);
								if (!GridSaleBusiness._isvalidQuantity && SPCreateOrder._syncFlag.isCheckInputQuantityValid) {
									/*
									 * restore previous quantity (if any)
									 */
									var warehouseId = Number(GridSaleBusiness._mapWarehouseIdSelect.get(r));
									if (oldValue != null) {
										var tmpQuantity = GridSaleBusiness.isValidQuantityInput(oldValue) ? oldValue : 0;
										tmpQuantity = StockValidateInput.getQuantity(tmpQuantity, newProduct.convfact);
										GridSaleBusiness.changeWarehouseAvailableQuantity(newProduct.productId, warehouseId, tmpQuantity);
									}
								}
								/*
								 * process split warehouse (if any)
								 */
								DEBUG ? console.log('- quantity: ' + newValue + ', ' + SPCreateOrder._syncFlag.isCheckInputQuantityValid) : null;
								if(newValue != null && GridSaleBusiness._isvalidQuantity && SPCreateOrder._syncFlag.isCheckInputQuantityValid){
									/*
									 * restore previous quantity (if any)
									 */
									var warehouseId = Number(GridSaleBusiness._mapWarehouseIdSelect.get(r));
									if (oldValue != null) {
										var tmpQuantity = GridSaleBusiness.isValidQuantityInput(oldValue) ? oldValue : 0;
										tmpQuantity = StockValidateInput.getQuantity(tmpQuantity, newProduct.convfact);
										GridSaleBusiness.changeWarehouseAvailableQuantity(newProduct.productId, warehouseId, tmpQuantity);
									}

									var isQuantityExceedStock = false;	// so luong nhap vao co vuot qua gia tri ton kho hay ko
									if (newProduct!=null) {
										var newQuantity = StockValidateInput.getQuantity(newValue,newProduct.convfact);
										var availableQuantity = htSale.getDataAtCell(r, COLSALE.STOCK);
										availableQuantity = StockValidateInput.getQuantity(availableQuantity,newProduct.convfact);
										if (isNaN(Number(newQuantity)) || Number(newQuantity) < 0) {
											htSale.selectCell(r, COLSALE.QUANTITY);
											htSale.getActiveEditor().beginEditing();
											return;
										}
										
										var isProductQuantityExceedTotalWarehouseQuantity = GridSaleBusiness.isProductQuantityExceedTotalWarehouseQuantity(newProduct.productId, Number(newQuantity), WarehouseType.SALE);
										/*
										 * Neu nhu so luong nhap vao cua san pham, vuot qua ton kho cua tat ca ca kho, thong bao loi ngay.
										 * Chi thuc hien tach kho tu dong khi so luong nhap vao dam bao ko vuot qua ton kho cua tat ca cac kho
										 */
										if (isProductQuantityExceedTotalWarehouseQuantity) {
											$('#generalInfoDiv #GeneralInforErrors').html('Thực đặt của sản phẩm ' + Utils.XSSEncode(newProduct.productCode) + ' vượt quá tồn kho . Vui lòng nhập lại').show();
											GridSaleBusiness.changeWarehouseAvailableQuantity(newProduct.productId, warehouseId, -1*Number(newQuantity));
											setTimeout(function() {
												htSale.selectCell(r, COLSALE.QUANTITY);
											}, 100);
											htSale.getActiveEditor().beginEditing();
											setTimeout(function() {
												$('#generalInfoDiv #GeneralInforErrors').hide();
											}, 3000);
											SPCreateOrder._syncFlag.isPaymentButtonClickable = true;
											return;
										}
									} else {
										SPCreateOrder._isInsertProductFromDialogToGrid = false;
									}
									if (!isQuantityExceedStock) {
//										htSale.selectCell(r+1,COLSALE.PRO_CODE);
									}
									SPCreateOrder.showTotalGrossWeightAndAmount();
								}
								SPCreateOrder._syncFlag.isSettingQuantityDone = true;
								SPCreateOrder._syncFlag.isSettingProductCodeDone = false;
								SPCreateOrder._syncFlag.processNextItem = true;
								DEBUG ? console.log('    setting quantity done at ' + (new Date()).getTime()) : null;
							}
							break;
							case COLSALE.DISCOUNT:
								isEditDiscount = 1;
								break;
							case COLSALE.PROGRAM:
								if(htSale.getDataAtCell(r, COLSALE.QUANTITY) != null){
//									htSale.selectCell(r+1, COLSALE.PRO_CODE);
								}
								SPCreateOrder.showTotalGrossWeightAndAmount();

								SPCreateOrder._syncFlag.isSettingProgramDone = true;
								SPCreateOrder._syncFlag.isSettingQuantityDone = false;
								//GridSaleBusiness.changeWarehouseSaleGrid(r,source, newValue, oldValue);//đổi lại kho khi chuyển qua lại giữa KM tay và KM tự động
								DEBUG ? console.log('    setting program done at ' + (new Date()).getTime()) : null;
							}
							if (isEditDiscount == 0) {
								/*if (c == COLSALE.PROGRAM && SPCreateOrder._syncFlag._allowAppliedNewProgram) {
									SPCreateOrder._syncFlag._allowAppliedNewProgram = false;
								} else {*/
								if (c != COLSALE.PROGRAM || !SPCreateOrder._syncFlag._allowAppliedNewProgram) {
									SPCreateOrder._isChangeOnSaleGrid = true;
									enable('payment');
									disabled('btnOrderSave');
									disabled('btnOrderReset');
									if ($("#btnApproveOrderIN").length > 0) {
										disabled("btnApproveOrderIN");
										SPCreateOrder.orderChanged = true;
									}
								}
							}
						} else {
							switch (c) {
								case COLSALE.QUANTITY:
									SPCreateOrder._syncFlag.isSettingQuantityDone = true;
									SPCreateOrder._syncFlag.isSettingProductCodeDone = false;
									SPCreateOrder._syncFlag.processNextItem = true;
									SPCreateOrder._syncFlag.isPaymentButtonClickable = true;
									enable('payment');
									DEBUG ? console.log('    setting quantity done' + (new Date()).getTime()) : null;
									break;
								case COLSALE.PROGRAM:
									//GridSaleBusiness.changeWarehouseSaleGrid(r,source, newValue, oldValue);//đổi lại kho khi chuyển qua lại giữa KM tay và KM tự động
									break;
							}
						}
					}
				}
			},
			//Xu ly du lieu truoc khi thay doi
			onBeforeChange: function(changes, source) {//SPCreateOrder
				SPCreateOrder.changeColumnIndex(SPCreateOrder._isAllowShowPrice);
				if(changes != null && $.isArray(changes)) {
					DEBUG && !SPCreateOrder._syncFlag.isCheckInputQuantityValid ? console.log("onBeforeChange") : null;
					DEBUG && !SPCreateOrder._syncFlag.isCheckInputQuantityValid ? console.log(changes) : null;
					for(var i = 0; i < changes.length; i++) {
						var htSale = $('#gridSaleData').handsontable('getInstance');
						var row = changes[i];
						var r = row[0];
						var c = row[1];
						var oldValue = row[2];
						var newValue = row[3];
						var productCode = htSale.getDataAtCell(r, COLSALE.PRO_CODE);
						switch(c){
						case COLSALE.PRO_CODE:
							var proCode = "";
							var products = GridSaleBusiness.mapSaleProduct.valArray
							for ( var i = 0, n = products.length; i < n; i++) {
								if (newValue == Utils.XSSEncode(products[i].productCode + " - " + products[i].productName).trim()) {
									proCode = products[i].productCode;
									break;
								}
							}
							if (proCode.length > 0) {
								row[3] = proCode;
							}			
							if (!SPCreateOrder._syncFlag.isCheckInputQuantityValid) {	// dang tach kho
								if (!oldValue && !newValue) {
									changes[i][2] = changes[i][3] = htSale.getDataAtCell(r, COLSALE.PRO_CODE);
								}
							}
							break;
						case COLSALE.QUANTITY: { // Chuyen doi quantity -> quantity convaft va fill amount
							if(productCode != null){
								/*
								 * disable payment-button
								 */
								disabled('payment');
//								SPCreateOrder._syncFlag.isPaymentButtonClickable = false;

								var product = GridSaleBusiness.mapSaleProduct.get(productCode.trim());
								if(product != null) {
									if(newValue != null){
										if (newValue == '') {
											newValue = '0';
										}
										// if((!/^[0-9\/]+$/.test(newValue)) ||
										// (String(newValue).indexOf('/')!=-1 &&
										// String(newValue).split('/').length >
										// 2)){
										if (!GridSaleBusiness.isValidQuantityInput(newValue)) {
											htSale.setDataAtCell(r, COLSALE.AMOUNT,0, source);
											htSale.selectCell(r,COLSALE.QUANTITY);
											setTimeout(function() {
												htSale.selectCell(r,COLSALE.QUANTITY);
											}, 100);
											GridSaleBusiness._isvalidQuantity = false;
											SPCreateOrder.showValidateError('Thực đặt không đúng định dạng. Vui lòng nhập lại')											
											/*$('#generalInfoDiv #GeneralInforErrors').html('Thực đặt không đúng định dạng. Vui lòng nhập lại').show();											
											setTimeout(function() {
												$('#generalInfoDiv #GeneralInforErrors').hide();
											}, 3000);*/
											break;
										} else {
											GridSaleBusiness._isvalidQuantity = true;
											// trungtm6 sua thanh tien = slThung
											// * giaThung + slLe * giaLe
											var pkPriceStr = htSale.getDataAtCell(r, COLSALE.PACKING_PRICE);
											var priceStr = htSale.getDataAtCell(r, COLSALE.PRICE);
											var pkPriceNum = product.packagePrice;
											var priceNum = product.price;
											
											if (newValue.indexOf('/') == -1) {
												if (SPCreateOrder._sysConvertQuantityConfig == SPCreateOrder.SYS_CONVERT_QUANTITY_CONFIG.ZEROQUANTITY) {
													newValue = '0/' + newValue;
												} else if (SPCreateOrder._sysConvertQuantityConfig == SPCreateOrder.SYS_CONVERT_QUANTITY_CONFIG.QUANTITYZERO) {
													newValue = newValue + '/0';
												}
											} else if (newValue.indexOf('/') == 0) {
												newValue = '0' + newValue;
											} else if (newValue.indexOf('/') == newValue.length - 1) {
												newValue = newValue + '0';
											}
											var quanArr = newValue.split('/');
											var amount = 0;
											if ($.isArray(quanArr) && quanArr.length == 2 && !Utils.isEmpty(pkPriceStr) && !Utils.isEmpty(priceStr)){
												pkPriceNum = Number(pkPriceStr.replace(/,/g, ""));
												priceNum = Number(priceStr.replace(/,/g, ""));
												amount = Number(quanArr[0]) * pkPriceNum + Number(quanArr[1]) * priceNum;
											}
											// var amount = newValue * product.price;
											var cs = GridSaleBusiness.mapCS.get(htSale.getDataAtCell(r,COLSALE.PROGRAM));
											// console.log('set amount: row[' +
											// r + '] = ' + amount + ', ' +
											// newValue + '(' +
											// newConvfactQuantity + ')x' +
											// product.price);
											if(cs == null){
												formatCurrency(amount)
												htSale.setDataAtCell(r, COLSALE.AMOUNT, formatFloatValue(amount, apConfig.sysDigitDecimal), source);
											}else{
												htSale.setDataAtCell(r, COLSALE.AMOUNT,0, source);
											}
											row[3] = newValue;
										}
									}else{
										htSale.setDataAtCell(r, COLSALE.AMOUNT, null, source);
										row[3] = null;
									}
								}else{
//									htSale.selectCell(r,COLSALE.PRO_CODE);
									return false;
								}
							}else{
//								htSale.selectCell(r,COLSALE.PRO_CODE);
								return false;
							}
						}
						break;
						case COLSALE.DISCOUNT:
							if(productCode != null){
								if (newValue != null) {
									row[COLSALE.DISCOUNT] = formatCurrency(newValue);
								} 
							} else{
//								htSale.selectCell(r,COLSALE.PRO_CODE);
								return false;
							}
							break;
						case COLSALE.PROGRAM:
							if (SPCreateOrder._syncFlag.isCheckInputQuantityValid) {
								if(productCode == null){
//									htSale.selectCell(r,COLSALE.PRO_CODE);
									return false;
								}else{
									var product = GridSaleBusiness.mapSaleProduct.get(productCode.trim());
									if(product == null ) {
//										htSale.selectCell(r,COLSALE.PRO_CODE);
										return false;
									}else{
										var cs = GridSaleBusiness.mapCS.get(newValue);
										if(cs == null){
											row[3] = product.promotionProgramCode;
											// tulv2 - 3105 22.09.2014
											if(newValue==""){
												var newConvfactQuantity = htSale.getDataAtCell(r, COLSALE.QUANTITY);
												newValue = StockValidateInput.getQuantity(newConvfactQuantity, product.convfact);
												var amount = newValue * product.price;
												htSale.setDataAtCell(r, COLSALE.AMOUNT, formatCurrency(amount), source);
											}
											// end 3105
										}else{
											htSale.setDataAtCell(r, COLSALE.AMOUNT,0, source);
											// Cap nhat lai CKMH khi chon CTHTTM
											htSale.setDataAtCell(r, COLSALE.DISCOUNT,'', source);
											SPCreateOrder._mapDiscountAmount.remove(r);
										}
									}
								}
							}	
							break;
						case COLSALE.PACKING_PRICE:
							//apConfig.sysDigitDecimal LAY TU serverInfo.js
							if (!Utils.isEmpty(newValue)) {
								if (GridSaleBusiness.isValidPriceInput(newValue.replace(/,/g, ""))) {
									var product = GridSaleBusiness.mapSaleProduct.get(productCode.trim());
									var pkPriceNum = Number(newValue.replace(/,/g, ""));
									var priceStr = htSale.getDataAtCell(r, COLSALE.PRICE);
										
									var quanStr = htSale.getDataAtCell(r, COLSALE.QUANTITY);
									if(product != null && newValue != null && !Utils.isEmpty(priceStr) && !Utils.isEmpty(quanStr)){
										var priceNum = Number(priceStr.replace(/,/g, ""));
										var quanArr = quanStr.split('/');
										var amount = 0;
										if ($.isArray(quanArr) && quanArr.length == 2){
											amount = Number(quanArr[0]) * pkPriceNum + Number(quanArr[1]) * priceNum;
											htSale.setDataAtCell(r, COLSALE.AMOUNT, formatFloatValue(amount, apConfig.sysDigitDecimal), source);
										}								
									}
									SPCreateOrder.showTotalGrossWeightAndAmount();
								} else {
//									htSale.setDataAtCell(r, COLSALE.PACKING_PRICE, oldValue, source);
									SPCreateOrder.showValidateError('Giá thùng không đúng định dạng. Vui lòng nhập lại')
									htSale.setDataAtCell(r, COLSALE.AMOUNT, 0, source);
								}
							}
							break;							
						case COLSALE.PRICE:
							if (!Utils.isEmpty(newValue)) {
								if (GridSaleBusiness.isValidPriceInput(newValue.replace(/,/g, ""))) {
									var product = GridSaleBusiness.mapSaleProduct.get(productCode.trim());
									var priceNum = Number(newValue.replace(/,/g, ""));
									var pkPriceStr = htSale.getDataAtCell(r, COLSALE.PACKING_PRICE);
									
									var quanStr = htSale.getDataAtCell(r, COLSALE.QUANTITY);
									if(product != null && newValue != null && !Utils.isEmpty(pkPriceStr) && !Utils.isEmpty(quanStr)){
										var pkPriceNum = Number(pkPriceStr.replace(/,/g, ""));	
										var quanArr = quanStr.split('/');
										var amount = 0;
										if ($.isArray(quanArr) && quanArr.length == 2){
											amount = Number(quanArr[0]) * pkPriceNum + Number(quanArr[1]) * priceNum;
											//formatCurrency(amount)
											htSale.setDataAtCell(r, COLSALE.AMOUNT, formatFloatValue(amount, apConfig.sysDigitDecimal), source);
										}								
									}
									SPCreateOrder.showTotalGrossWeightAndAmount();
								} else {
//									htSale.setDataAtCell(r, COLSALE.PRICE, oldValue, source);
									SPCreateOrder.showValidateError('Giá lẻ không đúng định dạng. Vui lòng nhập lại')
									htSale.setDataAtCell(r, COLSALE.AMOUNT, 0, source);
								}
							}
							break;
					}
					GridSaleBusiness.isEditGridSale = false;
				}
			}
			},
			//Xu ly sau khi validate
			afterValidate: function(isValid,value,row,prop,source) {
//				if (!isValid) {
//					$('#generalInfoDiv #GeneralInforErrors').html('Thực đặt không đúng định dạng. Vui lòng nhập lại').show();
//				} else if (value != null) {
//					$('#generalInfoDiv #GeneralInforErrors').html('').hide()
//				}
				if (!isValid) {
					SPCreateOrder.showValidateError('Thực đặt không đúng định dạng. Vui lòng nhập lại');
				}				
			},
			//Xu ly event khi thao tac ban phim vao grid sp ban
			beforeKeyDown: function(event){
				var htSale = $('#gridSaleData').handsontable('getInstance');
				var rowSelect = htSale.getSelected();
				if(rowSelect != undefined && rowSelect != null && !htSale.getCellMeta(0, 0).readOnly){
					var r = rowSelect[0];
					var c = rowSelect[1];
					
//					if(event.keyCode == keyCodes.DELETE){ //event delete row
//						event.stopImmediatePropagation();
//						var orderNumber = $('#orderNumber').val();
//						if (orderNumber === undefined || isNullOrEmpty(orderNumber)) {
//							if(rowSelect != null){
//								GridSaleBusiness.deleteSaleProduct(r,c,htSale);
//					    	}
//						}
//					}
					if(c == COLSALE.PRO_CODE){
						if(event.keyCode == keyCodes.TAB){
							event.stopImmediatePropagation();
							if(event.shiftKey){
								if(r == 0){
									$('#priorityId').focus();
									htSale.deselectCell();
								}else{
									htSale.selectCell(r-1,COLSALE.PROGRAM);
								}
							}else{
								if(r < htSale.countRows()){
									htSale.selectCell(r,COLSALE.QUANTITY);
								}
							}
							event.preventDefault();
						} else if(event.keyCode == keyCode_F9) {
//							SPCreateOrder.openSelectProductDialog();
							event.preventDefault();
						} else if ((event.keyCode == keyCodes.DELETE || event.keyCode == keyCodes.BACK_SPACE)
								&& $(".handsontableInputHolder:not(:hidden)").length == 0) {
							event.stopImmediatePropagation();
							var pcode1 = htSale.getDataAtCell(r, c);
							if (pcode1) {
								GridSaleBusiness.deleteSaleProduct(r, c, htSale);
							}
							event.preventDefault();
						}
					}
					
					if(c == COLSALE.QUANTITY){
						/*
						 * set max_length cho textarea
						 */
						var quantityTextMaxlength = 10;
						$(event.target).prop('maxlength', quantityTextMaxlength);
						
//						if (GridSaleBusiness._isvalidQuantity) {
							if(event.keyCode == keyCodes.TAB){
								event.stopImmediatePropagation();
								if(!event.shiftKey){
									if(r < htSale.countRows()){
										htSale.selectCell(r,COLSALE.PROGRAM);
									}
								}else{
//									htSale.selectCell(r,COLSALE.PRO_CODE);
								}
								event.preventDefault();
							} else if (event.keyCode == keyCodes.ENTER) {
								htSale.selectCell(r,COLSALE.PROGRAM);
								event.preventDefault();
							}
//						} else {
//							event.preventDefault();
//						}
					}
					
					if(c == COLSALE.PROGRAM){
						if(event.keyCode == keyCodes.TAB){
							event.stopImmediatePropagation();
							if(!event.shiftKey){
								if(r < htSale.countRows() - 1){
//									htSale.selectCell(r+1,COLSALE.PRO_CODE);
								}
							}else{
								htSale.selectCell(r,COLSALE.QUANTITY);
							}
							event.preventDefault();
						} 
//						else if (event.keyCode == keyCodes.ENTER) {
//							htSale.selectCell(r+1,COLSALE.PRO_CODE);
//							event.preventDefault();
//						} 
					}
//					if (c == COLSALE.DELETE) {
//						if (event.keyCode == keyCodes.TAB || event.keyCode == keyCodes.ENTER) {
//							GridSaleBusiness.deleteSaleProduct(r,c,htSale);
//							event.preventDefault();
//						}
//					}
				}
			},
			beforeRemoveRow: function(index, amount) {
				if (SPCreateOrder._isRemovingRow !== null && SPCreateOrder._isRemovingRow === true) {
					GridSaleBusiness._mapWarehouseIdSelect.remove(index);
					// decrease sequence in GridSaleBusiness._mapWarehouseIdSelect.keyArray
					GridSaleBusiness._mapWarehouseIdSelect.keyArray.forEach(function(item, k) {
						//if (k >= index) {
						if (item > index) {
							GridSaleBusiness._mapWarehouseIdSelect.keyArray[k]--;
						}
					});
					SPCreateOrder._isRemovingRow = false;
				}
			},
			afterRemoveRow: function(index, amount) {
				
			}
		});
		$('.handsontableInputHolder .handsontableInput').live('input',function(){
		    GridSaleBusiness.isEditGridSale = true;
		});
	},
	
	changeWarehouseSaleGrid:function(r, source, newValue, oldValue){
		if (SPCreateOrder.checkCTHTTMValue(newValue) == SPCreateOrder.checkCTHTTMValue(oldValue)) {//nếu cũ và mới đều cùng KM tự động hoặc KM tay thì ko cần đổi
			return;
		}
		var htSale = $('#gridSaleData').handsontable('getInstance');
		// Kiểm tra chuyển kho bán or KM
		var productCode = htSale.getDataAtCell(r, COLSALE.PRO_CODE);
		var product = GridSaleBusiness.mapSaleProduct.get(productCode);
		var whType = WarehouseType.SALE;
		if (SPCreateOrder.checkCTHTTMValue(newValue)) {
			whType = WarehouseType.PROMO;
		}
		var warehousesOfProduct = GridSaleBusiness.getWarehouseByType(product.productId, whType) || [];
		if (warehousesOfProduct != null && warehousesOfProduct.length > 0) {//có kho set lại tồn kho và cập nhật kho
			var chooseWarehouse = warehousesOfProduct[0];
			htSale.setDataAtCell(r, COLSALE.STOCK, GridSaleBusiness.formatStockQuantity(chooseWarehouse.availableQuantity, product.convfact), source);
			// update temp data
			$('#product_'+r).attr('warehouseId', chooseWarehouse.warehouseId);
			GridSaleBusiness._mapWarehouseIdSelect.put(r, chooseWarehouse.warehouseId);
			htSale.setDataAtCell(r, COLSALE.WAREHOUSE, chooseWarehouse.warehouseName, source);
		} else {
			//chuyển CT HTTM qua lại giữa KM tay và KM tự động mà đích đến không có kho
			if (whType == WarehouseType.PROMO) {//chuyển từ bán sang km
				$('#GeneralInforErrors').html('Không có tồn kho khuyến mãi để thực hiện.').show();
				htSale.setDataAtCell(r, COLSALE.PROGRAM, "", null);
			} else {//chuyển từ km sang bán
				$('#GeneralInforErrors').html('Không có tồn kho bán để thực hiện.').show();
				htSale.setDataAtCell(r, COLSALE.WAREHOUSE, "", source);
				GridSaleBusiness._mapWarehouseIdSelect.remove(r);
			}
			htSale.setDataAtCell(r, COLSALE.STOCK, "", source);
			setTimeout(function(){$('#GeneralInforErrors').hide();},2000);
		}
	},

	/**
	 * check if quantity-input is valid
	 * @author tuannd20
	 * @param  {String}  inputStr quantity input string
	 * @return {Boolean}          true: valid, false: invalid
	 * @date 17/10/2014
	 */
	isValidQuantityInput: function(inputStr) {
		inputStr = (inputStr || "").toString();
		return !((!/^[0-9\/]+$/.test(inputStr)) || (String(inputStr).indexOf('/')!=-1 && String(inputStr).split('/').length > 2));		
	},
	/**
	 * check if price-input is valid
	 * @author trungtm6
	 * @param  {String}  inputStr price input string
	 * @return {Boolean}          true: valid, false: invalid
	 * @date 22/04/2015
	 */
	isValidPriceInput: function(inputStr) {
		inputStr = (inputStr || "").toString();
		return (/^[0-9.,]+$/.test(inputStr));		
	},
	
	/**
	 * Ham reset grid them moi san pham ban
	 */
	resetSaleGrid: function(type) {
		GridSaleBusiness.listCellMerge = new Array();
		GridSaleBusiness.mapSaleProductGrid = new Map();
		GridSaleBusiness.isEditGridSale = false;
		SPCreateOrder._mapPRODUCTSbyWarehouseOnGrid = new Map();
		var cols = GridSaleBusiness.getColumnProperty();
		var data = new Array();
		var minSpareRows = 0;
		if (type != 1) {
			for(var i = 0; i < 1; i++) {
				var row = new Array();
				for(var j = 0, size = cols.length; j < size; j++) {
					row.push(null);
				}
				data.push(row);
			}
			minSpareRows = 1;
		}
		var hsSale = $('#gridSaleData').handsontable('getInstance');
//		hsSale.alter('insert_row');
//		hsSale.updateSettings({data: data, columns: GridSaleBusiness.getColumnProperty(), minSpareRows: minSpareRows});
		hsSale.updateSettings({data: data, columns: cols, minSpareRows: minSpareRows});
		$('#totalWeightSale').html(0);
		$('#totalAmountSale').html(0);
		$('#totalWeight').html(0);
		$('#totalAmount').html(0);
	},
	
	/**
	 * Ham disble grid sp ban
	 */
	disableSaleGrid: function(){
		var hsSale = $('#gridSaleData').handsontable('getInstance');
		hsSale.updateSettings({data: []});
		$('#totalWeightSale').html(0);
		$('#totalAmountSale').html(0);
		$('#totalWeight').html(0);
		$('#totalAmount').html(0);
//		$('#gridSaleContainer').css('height', 70);
	},
	
	/**
	 * Lay oldQuantity trong lstProduct.
	 * 
	 * @author tulv2
	 * @since 27.09.2014
	 * */
	getOldQuantityOfProductInWarehouse: function(productCode, warehouseId){
		try {
			if((productCode!=null || productCode!='' || productCode != undefined) && (warehouseId!=null  || warehouseId!='' || warehouseId != undefined) && (SPCreateOrder._lstProduct!=null || SPCreateOrder._lstProduct != undefined)){
				var arr = SPCreateOrder._lstProduct.valArray;
				if(arr!=null || arr !=undefined){
					for(var i = 0, sz = arr.length; i < sz; i++){
						if((arr[i].productCode!=null || arr[i].productCode!=undefined) && (arr[i].warehouseId !=null || arr[i].warehouseId != undefined))
						  if (arr[i].productCode == productCode && arr[i].warehouseId == warehouseId) {
						    return arr[i].oldQuantity;
						 }
					}
				}
			}	
			return 0;
		} catch (e) {
			return 0;
		}
	}
};
var COLSALE = {
	PRO_CODE: 0, PRO_NAME: 1, PACKING_PRICE: 2, PRICE: 3, WAREHOUSE: 4, STOCK: 5, QUANTITY: 6, AMOUNT: 7, DISCOUNT: 8, PROGRAM: 9, DELETE: 10
};
//var COLSALESHOWPRICE = {
//	PRO_CODE: 0, PRO_NAME: 1, PACKING_PRICE: 2, PRICE: 3, WAREHOUSE: 4, STOCK: 5, QUANTITY: 6, AMOUNT: 7, DISCOUNT: 8, PROGRAM: 9, DELETE: 10
//};
//var COLSALENOTSHOWPRICE = {
//	PRO_CODE: 0, PRO_NAME: 1,  WAREHOUSE: 2, STOCK: 3, QUANTITY: 4, DISCOUNT: 5, PROGRAM: 6, DELETE: 7
//};
var ERROR = {
	NOT_EXISTS : 1,
	NOT_EXISTS_SALE_CAT : 2,
	NOT_ERR : 3,
	IS_EQUIPMENT : 4
	};
var PromotionType = {
	FREE_PRODUCT :1,
	FREE_PRICE : 2,
	FREE_PERCENT:3,
	PROMOTION_FOR_ORDER: 4,
	PROMOTION_FOR_ORDER_TYPE_PRODUCT: 5,
	PROMOTION_FOR_ZV21: 6,
	PROMOTION_OPEN_PRODUCT: 8
};
var WarehouseType = {
	SALE : 0,
	PROMO : 1
};
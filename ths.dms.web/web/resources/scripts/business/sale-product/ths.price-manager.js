var PriceManager = {
	_mapPrice: null,
	_arrId: null,
	_priceId: null,
	_paramPriceSearch: null,
	_shopChoose: null,
	_valueTmp: 0,
	_paramSearch: null,
	_flagChangeWaitting : {
		isdelete: 0, //Xoa
		isrunning: 1 //Duyet
	},	
	
	///@author hunglm16; @since: September 03,2014; @description Tao tham so tim kiem Price
	processVATInPrice:function(vat) {
		if(vat==undefined || vat == null) {
			return 0;
		}
		var arrNum = vat.toString().trim().split(".");
		if(arrNum.length > 2) {
			vat = parseFloat(arrNum[0].trim() + "." + arrNum[1].trim());
			PriceManager.processVATInPrice(vat);
		}
		return parseFloat(vat).toFixed(2);
	},
	///@author hunglm16; @since: September 03,2014; @description Tao tham so tim kiem Price
	createPriceManagerParamaterSearch:function() {
		PriceManager._paramSearch = {};
		PriceManager._paramSearch.productCode = $('#txtProductCode').val().trim();
		PriceManager._paramSearch.productName = $('#txtProductName').val().trim();
		PriceManager._paramSearch.catId = $('#ddlProductInfo').val();
		PriceManager._paramSearch.shopTypeId = $('#ddlTypeShop').val();
		PriceManager._paramSearch.shopCode = $('#txtShopCode').val().trim();
		PriceManager._paramSearch.customerTypeId = $('#ddlTypeCustomer').val();
		PriceManager._paramSearch.customerCode = $('#txtCustomerCode').val().trim();
		PriceManager._paramSearch.fromDateStr = $('#txtFromDate').val().trim();
		PriceManager._paramSearch.toDateStr = $('#txtToDate').val().trim();	
		PriceManager._paramSearch.status = $('#ddlStatus').val();
	},
	///@author vuongbd; @since: 13/05/2015; @description Hien thi Dialog thay doi gia
	dialogChangePriceManager :function(id, status) {
		$('.ErrorMsgStyle').html('').hide();
		var price = PriceManager._mapPrice.get(id);
		if(price!=undefined && price!=null) {
			$('#txtProductCodeDl').val(price.productCode);
			$('#txtProductNameDl').val(price.productName);
			$('#txtShopTypeNameDl').val(price.shopTypeName);
			$('#txtShopCodeDl').val(price.shopName);
			$('#txtCustomerTypeNameDl').val(price.customerTypeName);
			$('#txtCustomerCodeDl').val(price.customerName);			
			$('#txtVATDl').val(PriceManager.processVATInPrice(price.vat));
			$('#txtPriceNotVATDl').val(formatCurrency(price.priceNotVAT));
			$('#txtPriceDl').val(formatCurrency(price.price));
			$('#txtPricePackageNotVATDl').val(formatCurrency(price.pricePackageNotVAT));
			$('#txtPricePackageDl').val(formatCurrency(price.pricePackage));
			PriceManager._priceId = id;
		}
		$('#changePriceDialog').dialog({
			title: price_change,
			width: 690, 
			height: 'auto',
			closed: false,
			cache: false,
			modal: true,
			onOpen: function() {
				
			},
			onClose: function() {
				$('#txtVATDl').val(0);
				$('#txtPriceDl').val(100);
				$('#txtPriceNotVATDl').val(100);
				$('#txtPricePackageDl').val(100);
				$('#txtPricePackageNotVATDl').val(100);
				PriceManager._priceId = null;
			}
	});
	},
	///@author hunglm16; @since: August 28,2014; @description Hien thi Dialog tim kiem danh sach don vi
	showSearchListShopByCMS:function(flag, url) {
		$('#common-dialog-search-2-textbox').dialog({
			title: 'Thông tin tìm kiếm',
			width: 600, 
			height: 'auto',
			closed: false,
			cache: false,
			modal: true,
			onOpen: function() {
				$('#txtCmnShopCode').val("").show();
				$('#txtCmnShopName').val("").show();
				var params = {};
				params.code = $('#txtCmnShopCode').val();
				params.name = $('#txtCmnShopName').val();
				$('#common-dialog-grid-search').datagrid({
					url : url,
					autoRowHeight : true,
					rownumbers : true,
					checkOnSelect :true,
					pagination:true,
					rowNum : 10,
					pageSize: 10,
					scrollbarSize : 0,
					singleSelect:true,
					pageNumber:1,
					queryParams:params,
					fitColumns:true,
					width : ($('#common-dialog-search-2-textbox').width() - 25),
					columns : [[
						{field: 'shopCode', title: price_unit_code, align: 'left', width: 110, sortable:false, resizable:false, formatter: function(value, row, index) {
							return Utils.XSSEncode(value);
						}},
						{field: 'shopName', title: price_unit_name, align: 'left', width: 150, sortable:false, resizable:false, formatter: function(value, row, index) {
							return Utils.XSSEncode(value);
						}},
						{field: 'choose', title: '', align: 'center', width: 40, sortable:false, resizable:false, formatter:function(value,row,index) {
							return '<a href="javascript:void(0)" onclick="return PriceManager.callSelectF9SialogShopSearchPrice(' +flag+', ' +row.shopId+',\'' +Utils.XSSEncode(row.shopCode)+'\',\'' +Utils.XSSEncode(row.shopName)+'\');">chọn</a>';         
						}}
					]],
					onLoadSuccess :function() {
						$('#common-dialog-search-2-textbox .datagrid-header-rownumber').html('STT');
						var dHeight = $('#common-dialog-search-2-textbox').height();
						var wHeight = $(window).height();
						var top = (wHeight - dHeight) / 2 > 180 ? (wHeight - dHeight) / 2 - 180 : (wHeight - dHeight) / 2;
					}
				});
				
			},
			onClose: function() {
				$('#txtCmnShopCode').val("").change();
				$('#txtCmnShopName').val("").change();
			}
		});
	},
	///@author hunglm16; @since: August 28,2014; @description Goi nhan tham so gui ve luc chon Don vi tu Dialog tim kiem
	callSelectF9SialogShopSearchPrice: function(flag, shopId, shopCode, shopName) {
		if(flag==0) {
			PriceManager._shopChoose = {};
			PriceManager._shopChoose.shopId = shopId;
			if(shopCode!=undefined && shopCode!=null) {
				PriceManager._shopChoose.shopCode = shopCode;
				$('#txtShopCode').val(shopCode.trim());
			}
			if(shopName!=undefined && shopName!=null) {
				PriceManager._shopChoose.shopName = shopName;
				$('#lblShopName').html(shopName.trim()).change();
			}
			$('#common-dialog-search-tree-in-grid-choose-single').dialog("close");
		}
	},
	///@author hunglm16; @since: August 26,2014; @description Tao tham so tim kiem Price
	createPriceParamaterSearch:function() {
		PriceManager._paramPriceSearch = {
			productCode 	: $('#txtProductCode').val.trim(),
			productName 	: $('#txtProductName').val.trim(),
			catId 			: $('#ddlProductInfo').val(),			
			shopTypeId  	: $('#ddlTypeShop').val(),
			shopCode 		: $('#txtShopCode').val().trim(),			
			customerTypeId 	: $('#ddlTypeCustomer').val(),			
			customerCode 	: $('#txtCustomerCode').val().trim(),
			fromDateStr 	: $('#txtFromDate').val().trim(),
			toDateStr 		: $('#txtToDate').val().trim(),
			status 			: $('#ddlStatus').val()
		};
	},
	///@author hunglm16; @since: August 26,2014; @description Tim kiem Price
	searchProductPrice:function() {
		$('.ErrorMsgStyle').html('').hide();
		var msg = '';
		var firstLoadPage = true;
		var fDate = $('#txtFromDate').val();
		var tDate = $('#txtToDate').val();
		if(fDate.length >0) {
			msg = Utils.getMessageOfInvalidFormatDate('txtFromDate', price_grid_column7);
		}
		if(msg.length == 0 && tDate.length >0) {
			msg = Utils.getMessageOfInvalidFormatDate('txtToDate', price_grid_column8);
		}
		if(fDate.length >0 && tDate.length >0 && !Utils.compareDate(fDate, tDate)) {
				msg = price_search_error_01;
				$('#fromDate').focus();
		}
		if(msg.length > 0) {
			$('#errMsgPriceManager').html(msg).show();
			return false;
		}
		PriceManager.createPriceManagerParamaterSearch();
		$('#productPriceDgDiv').html('<table id="dgProductPrice"></table>').change().show();	
		$('#dgProductPrice').datagrid({
			url: '/price-manage/search-PriceProduct',
			width : $('#productPriceDgDiv').width() - 15,
			queryParams: PriceManager._paramSearch,
			/*{
				productCode 	: $('#txtProductCode').val().trim(),
				productName 	: $('#txtProductName').val().trim(),
				catId 			: $('#ddlProductInfo').val(),			
				shopTypeId  	: $('#ddlTypeShop').val(),
				shopCode 		: $('#txtShopCode').val().trim(),			
				customerTypeId 	: $('#ddlTypeShop').val(),			
				customerCode 	: $('#txtShopCode').val().trim(),
				fromDateStr 	: $('#txtFromDate').val().trim(),
				toDateStr 		: $('#txtToDate').val().trim()		
			},*/
			pagination : true,
	        rownumbers : true,
	        pageNumber : 1,
	        checkOnSelect: false,
			selectOnCheck: false,
	        scrollbarSize: 0,
	        autoWidth: true,
	        pageList: [10, 20],
	        autoRowHeight : true,
	        fitColumns : true,
			columns:[[
				{field: 'cbxWarningPrdPr', title: '<input type = "checkbox" onchange="return PriceManager.changeCheckboxAllByGridPrdPr(this);" id = "ckallPrdPr">', width: 40, align: 'center', formatter: function(value, row, index) {
					var html = '';
					if (row.status != undefined && row.status != null && row.status == 2) {
						html += '<input type = "checkbox" class = "changegridcbxr" onchange="return PriceManager.changeCheckboxByGridPrdPr(this);" value="' +row.id+'" id="ckr' +row.id+'">';	
					}
					return html;
				}},
				{field: 'shopTypeName', title: price_grid_column1, width: 50, sortable: true, align: 'left', formatter: function(value, row, index) {
					return Utils.XSSEncode(value);
				}},  
			    {field: 'shopText', title: price_grid_column2, width: 110, sortable: true, align: 'left', formatter: function(value, row, index) {
			    	var valueS = '';
			    	/*if (row.shopCode != undefined && row.shopCode != null) {
			    		valueS = valueS + row.shopCode + " - ";
			    	}*/
			    	if (row.shopName != undefined && row.shopName != null) {
			    		valueS = valueS + row.shopName;
			    	}
			    	return Utils.XSSEncode(valueS);
			    }},
			    {field: 'typeCustomerStr', title: price_grid_column3, width: 60, sortable: true, align: 'left', formatter: function(value, row, index) {
			    	//return Utils.XSSEncode(value);
			    	return Utils.XSSEncode(row.customerTypeName);
			    }},
			      {field: 'customerName', title: price_grid_column4, width: 110, sortable: true, align: 'left', formatter: function(value, row, index) {
			    	return Utils.XSSEncode(value);
			    }},
			    {field: 'productCode', title: price_grid_column5, width: 60, sortable: true, align: 'left', formatter: function(value, row, index) {
			    	return Utils.XSSEncode(value);
			    }},
			    {field: 'productName', title: price_grid_column6, width: 110, sortable: true, align: 'left', formatter: function(value, row, index) {
			    	return Utils.XSSEncode(value);
			    }},
			    {field: 'fromDateStr', title: price_grid_column7, width: 60, sortable: true, align: 'center', formatter: function(value, row, index) {
			    	return Utils.XSSEncode(value);
			    }},
			    {field: 'toDateStr', title: price_grid_column8, width: 60, sortable: true, align: 'center', formatter: function(value, row, index) {
			    	return Utils.XSSEncode(value);
			    }},
			    {field: 'pricePackageNotVAT', title: price_grid_column13, width: 60, sortable: true, align: 'right', formatter: function(value, row, index) {
			    	return VTUtilJS.formatCurrency(Utils.formatDoubleValue(value));
			    }},
			    {field: 'pricePackage', title: price_grid_column14, width: 60, sortable: true, align: 'right', formatter: function(value, row, index) {
			    	return VTUtilJS.formatCurrency(Utils.formatDoubleValue(value));
			    }},
			    {field: 'priceNotVAT', title: price_grid_column9, width: 50, sortable: true, align: 'right', formatter: function(value, row, index) {
			    	return VTUtilJS.formatCurrency(Utils.formatDoubleValue(value));
			    }},
			    {field: 'price', title: price_grid_column10, width: 50, sortable: true, align: 'right', formatter: function(value, row, index) {
			    	return VTUtilJS.formatCurrency(Utils.formatDoubleValue(value));
			    }},
			    {field: 'vat', title: price_grid_column11, width: 30, sortable: true, align: 'right', formatter: function(value, row, index) {
			    	return PriceManager.processVATInPrice(value);
			    }},
			    {field: 'status', title: price_grid_column12, width: 60, sortable: true, align: 'left', formatter: function(value, row, index) {
			    	return statusTypeText.parseValue(value);
			    }},
			    {field: 'change', title: '', width: 20, align: 'center', formatter: function(value, row, index) {
			    	var html = '';
			    	if (row.status!=undefined && row.status!=null && row.status == 2) {
			    		html += '<a href="javascript:void(0)" id="dg_priceManage_update" onclick="return PriceManager.dialogChangePriceManager(' +row.id+',' +row.status+');"><img title="Chỉnh sửa" src="/resources/images/icon-edit.png" width="16" heigh="16"></a>';
			    	} else if (row.status!=undefined && row.status!=null && row.status == 1) {
			    		html += '<a href="javascript:void(0)" id="dg_priceManage_stop" onclick="return PriceManager.stopPriceManager(' +row.id+',' +row.status+',\'' +row.productCode+'\');"><img title="' +common_status_stopped+'" src="/resources/images/icon-stop.png" width="16" heigh="16"></a>';
			    	}
			    	return html;
			    }}
			]],
	        onLoadSuccess :function(data) {
				$('.datagrid-header-rownumber').html(price_stt);
				Utils.updateRownumWidthAndHeightForDataGrid('dgProductPrice');
				PriceManager._mapPrice = new Map();
			 	if(data!=undefined && data!=null && data.rows!=undefined && data.rows!=null) {
			 		for(var i=0; i< data.rows.length; i++) {
			 			PriceManager._mapPrice.put(data.rows[i].id, data.rows[i]);
			 		}
			 	}
			 	PriceManager.changeCheckboxAllByGridPrdPr($('#ckallPrdPr').prop("checked"));
			 	if($('td[field="change"] a').length == 0) {
			 		$('#dgProductPrice').datagrid("hideColumn", "change");
			 	}else{
			 		$('#dgProductPrice').datagrid("showColumn", "change");
			 	}
			 	$('#ckallPrdPr').prop("checked", false);
			 	if($('td[field="cbxWarningPrdPr"] input').length < 2) {
			 		$('#dgProductPrice').datagrid("hideColumn", "cbxWarningPrdPr");
			 		$('.changewarningPrdpr').hide();
			 	}else{
			 		$('#dgProductPrice').datagrid("showColumn", "cbxWarningPrdPr");
			 		$('.changewarningPrdpr').show();
			 	}
			 	//Phan quyen control
			 	var arrDelete =  $('#productPriceDgDiv td[field="change"]');
				if (arrDelete != undefined && arrDelete != null && arrDelete.length > 0) {
				  for (var i = 0, size = arrDelete.length; i < size; i++) {
				    $(arrDelete[i]).prop("id", "group_edit_pr_gr_row_change_" + i);
					$(arrDelete[i]).addClass("cmsiscontrol");
				  }
				}
				Utils.functionAccessFillControl('productPriceDgDiv', function(data) {
					//Xu ly cac su kien lien quan den cac control phan quyen
					var arrTmpLength =  $('#productPriceDgDiv td[id^="group_edit_pr_gr_row_change_"]').length;
					var invisibleLenght = $('.isCMSInvisible[id^="group_edit_pr_gr_row_change_"]').length;
					if (arrTmpLength > 0 && arrTmpLength != invisibleLenght) {
						$('#dgProductPrice').datagrid("showColumn", "change");
					} else {
						$('#dgProductPrice').datagrid("hideColumn", "change");
					}
				});
	        }
		});
	},
	///@author hunglm16; @since: August 15,2014; @description Xoa Kho
	stopPriceManager: function(id, status, productCode) {
		if(status != undefined && status != null && (status == 2 || status == 1)) {
			$(".ErrorMsgStyle").html('').hide();
			var msg = "";
			if(productCode==undefined || productCode ==null) {
				productCode = "";
			}
			productCode = productCode.toString().trim();
			var params = {
					id: id				
			};
			var msg = price_search_error_02 +productCode+"?";
			Utils.addOrSaveData(params, "/price-manage/stopPrice", null, 'errMsgPriceManager', function(data) {
				PriceManager.searchProductPrice();
				$("#successMsgPriceManager").html(price_search_error_03).show();
				setTimeout(function() {
					$('.SuccessMsgStyle').html("").hide();
				 }, 1500);
			}, null, null, null, msg);
		}
	},
	///@author hunglm16; @since: August 15,2014; @description them moi hoac cap nhat thong tin gia
	changePriceByDialog: function() {
		$(".ErrorMsgStyle").html('').hide();
		var msg = "";
		var price = null;
		var flagPr = false;
		if(PriceManager._priceId!=undefined && PriceManager._priceId!=null) {
			price = PriceManager._mapPrice.get(PriceManager._priceId);
			if(price!=undefined && price!=null) {
				flagPr = true;
			}
		}
		if(!flagPr) {
			msg = price_search_error_04;
			$("#errMsgDialogChangePrice").html(msg).show();
			return false;
		}
		if (msg.length == 0) {
			msg = Utils.getMessageOfRequireCheck("txtVATDl", "VAT");
		}
		if (msg.length == 0) {
			var vat = parseFloat($('#txtVATDl').val().trim());
			if (vat > 100 || vat <0) {
				msg = price_search_error_05;
			}
		}
		
		if (msg.length == 0) {
			msg = Utils.getMessageOfRequireCheck("txtPricePackageNotVATDl", price_grid_column13);
		}
		if (msg.length == 0) {
			msg = Utils.getMessageOfInvaildInteger("txtPricePackageNotVATDl",price_grid_column13);
			if (msg.length > 0) {
				msg = price_search_error_06;
			}
		}

		if (msg.length == 0) {
			msg = Utils.getMessageOfRequireCheck("txtPriceNotVATDl", price_grid_column15);
		}
		if (msg.length == 0) {
			msg = Utils.getMessageOfInvaildInteger("txtPriceNotVATDl", price_grid_column15);
			if (msg.length > 0) {
				msg = price_search_error_06;
			}
		}

		if (msg.length == 0) {
			msg = Utils.getMessageOfRequireCheck("txtPricePackageDl", price_grid_column16);
		}
		if (msg.length == 0) {
			msg = Utils.getMessageOfInvaildInteger("txtPricePackageDl", price_grid_column16);
			if (msg.length > 0) {
				msg = price_search_error_07;
			}
		}
		
		if (msg.length == 0) {
			msg = Utils.getMessageOfRequireCheck("txtPriceDl", price_grid_column17);
		}
		if (msg.length == 0) {
			msg = Utils.getMessageOfInvaildInteger("txtPriceDl", price_grid_column17);
			if (msg.length > 0) {
				msg = price_search_error_07;
			}
		}
		if (msg.length > 0) {
			$("#errMsgDialogChangePrice").html(msg).show();
			return false;
		}
		var isFlag = false;
		var msg = price_search_error_08;
		var params = {
				id: PriceManager._priceId,				
				vat: $("#txtVATDl").val().replace(/,/g, '').trim(),
				price: $("#txtPriceDl").val().replace(/,/g, '').trim(),
				priceNotVat: $("#txtPriceNotVATDl").val().replace(/,/g, '').trim(),
				pricePackage: $("#txtPricePackageDl").val().replace(/,/g, '').trim(),
				pricePackageNotVat: $("#txtPricePackageNotVATDl").val().replace(/,/g, '').trim()
		};
		Utils.addOrSaveData(params, "/price-manage/addorUpdatePrice", null, 'errMsgDialogChangePrice', function(data) {
			PriceManager.searchProductPrice();
			$("#successMsgDialogChangePrice").html(price_search_error_03).show();
			setTimeout(function() {
				$('#changePriceDialog').dialog("close");
				$('.SuccessMsgStyle').html("").hide();
			 }, 1500);
		}, null, null, null, msg);
		return false;
	},
	///@author hunglm16; @since: August 15,2014; @description Xoa Kho
	updatePriceNotVAT: function(flag) {
		if(flag!=undefined && flag!=null) {
			if(flag == 1) {
				//TxtVAT onChange Event
				var value = $('#txtVATDl').val();
				var vat = 0;
				if(value!=undefined && value!=null && value.trim().length>0) {
					try{
						vat = parseFloat(value.replace(/,/g,"").trim());
						if(vat.toString()==="NaN") {
							vat = 0;
							$('#txtVATDl').val(PriceManager.processVATInPrice(vat));
							$('#txtPriceNotVATDl').val($('#txtPriceDl').val().trim());
							return;
						}
					}catch(e) {
						vat = 0;
					}
					if(vat<=0) {
						vat = 0;
						$('#txtVATDl').val(PriceManager.processVATInPrice(vat));
						$('#txtPriceNotVATDl').val($('#txtPriceDl').val().trim());
						return;
					}
					if(vat>100) {
						vat = 100;
						$('#txtVATDl').val(PriceManager.processVATInPrice(vat));
					}
					var vatNew = vat.toFixed(2);
					if(vatNew != vat) {
						$('#txtVATDl').val(vatNew).change();
					}
					var priceDl = $('#txtPriceDl').val().replace(/,/g,"").trim();
					if(priceDl.length > 0) {
						var priceNotVatN = parseFloat(100) + parseFloat(vatNew);
						priceNotVatN = parseFloat(100) / parseFloat(priceNotVatN);
						var priceC = parseFloat(priceDl.replace(/,/g,"").trim());
						priceNotVatN = parseFloat(priceC) * parseFloat(priceNotVatN);
						$('#txtPriceNotVATDl').val(VTUtilJS.formatCurrency(Math.round(priceNotVatN)));
					}else{
						$('#txtPriceNotVATDl').val(0).change();
					}
					$('#txtVATDl').val(PriceManager.processVATInPrice(vatNew));
				}else{
					$('#txtVATDl').val(0).change();
				}
			}else if(flag == 2) {
				//TxtPrice onChange Event
				var value = $('#txtPriceDl').val();
				var price = 0;
				if(value!=undefined && value!=null && value.trim().length>0) {
					try{
						price = parseFloat(value.replace(/,/g,"").trim());
						if(price.toString()==="NaN") {
							price = 0;
							$('#txtPriceDl').val(price);
						}
					}catch(e) {
						price = 0;
					}
					if(price<0) {
						price = 0;
						$('#txtPriceDl').val(price);
					}
					var priceNew = price.toFixed(0);
					if(priceNew != price) {
						$('#txtPriceDl').val(VTUtilJS.formatCurrency(priceNew)).change();
					}
					var vatDl = $('#txtVATDl').val().replace(/,/g,"").trim();
					if(vatDl.length>0) {
						var vat = parseFloat(vatDl.replace(/,/g,"").trim());
						if(parseFloat(vat)<=0) {
							$('#txtPriceNotVATDl').val(VTUtilJS.formatCurrency(Math.round(price)));
							return;
						}
						var priceNotVatN = parseFloat(100) + parseFloat(vat);
						priceNotVatN = parseFloat(100) / parseFloat(priceNotVatN);
						priceNotVatN = parseFloat(priceNew) * parseFloat(priceNotVatN);
						$('#txtPriceNotVATDl').val(VTUtilJS.formatCurrency(Math.round(priceNotVatN)));
					}else{
						$('#txtPriceNotVATDl').val(0);
						$('#txtPriceDl').val(0);
					}
				}else{
					$('#txtPriceDl').val(0);
					$('#txtPriceNotVATDl').val(0);
				}
			}else{
				return;
			}
		}else{
			return;
		}
	},


	updatePricePackageNotVAT: function(flag) {
		if(flag!=undefined && flag!=null) {
			if(flag == 1) {
				//TxtVAT onChange Event
				var value = $('#txtVATDl').val();
				var vat = 0;
				if(value!=undefined && value!=null && value.trim().length>0) {
					try{
						vat = parseFloat(value.replace(/,/g,"").trim());
						if(vat.toString()==="NaN") {
							vat = 0;
							$('#txtVATDl').val(PriceManager.processVATInPrice(vat));
							$('#txtPricePackageNotVATDl').val($('#txtPricePackageDl').val().trim());
							return;
						}
					}catch(e) {
						vat = 0;
					}
					if(vat<=0) {
						vat = 0;
						$('#txtVATDl').val(PriceManager.processVATInPrice(vat));
						$('#txtPricePackageNotVATDl').val($('#txtPricePackageDl').val().trim());
						return;
					}
					if(vat>100) {
						vat = 100;
						$('#txtVATDl').val(PriceManager.processVATInPrice(vat));
					}
					var vatNew = vat.toFixed(2);
					if(vatNew != vat) {
						$('#txtVATDl').val(vatNew).change();
					}
					var priceDl = $('#txtPricePackageDl').val().replace(/,/g,"").trim();
					if(priceDl.length > 0) {
						var priceNotVatN = parseFloat(100) + parseFloat(vatNew);
						priceNotVatN = parseFloat(100) / parseFloat(priceNotVatN);
						var priceC = parseFloat(priceDl.replace(/,/g,"").trim());
						priceNotVatN = parseFloat(priceC) * parseFloat(priceNotVatN);
						$('#txtPricePackageNotVATDl').val(VTUtilJS.formatCurrency(Math.round(priceNotVatN)));
					}else{
						$('#txtPricePackageNotVATDl').val(0).change();
					}
					$('#txtVATDl').val(PriceManager.processVATInPrice(vatNew));
				}else{
					$('#txtVATDl').val(0).change();
				}
			}else if(flag == 2) {
				//TxtPrice onChange Event
				var value = $('#txtPricePackageDl').val();
				var price = 0;
				if(value!=undefined && value!=null && value.trim().length>0) {
					try{
						price = parseFloat(value.replace(/,/g,"").trim());
						if(price.toString()==="NaN") {
							price = 0;
							$('#txtPricePackageDl').val(price);
						}
					}catch(e) {
						price = 0;
					}
					if(price<0) {
						price = 0;
						$('#txtPricePackageDl').val(price);
					}
					var priceNew = price.toFixed(0);
					if(priceNew != price) {
						$('#txtPricePackageDl').val(VTUtilJS.formatCurrency(priceNew)).change();
					}
					var vatDl = $('#txtVATDl').val().replace(/,/g,"").trim();
					if(vatDl.length>0) {
						var vat = parseFloat(vatDl.replace(/,/g,"").trim());
						if(parseFloat(vat)<=0) {
							$('#txtPricePackageNotVATDl').val(VTUtilJS.formatCurrency(Math.round(price)));
							return;
						}
						var priceNotVatN = parseFloat(100) + parseFloat(vat);
						priceNotVatN = parseFloat(100) / parseFloat(priceNotVatN);
						priceNotVatN = parseFloat(priceNew) * parseFloat(priceNotVatN);
						$('#txtPricePackageNotVATDl').val(VTUtilJS.formatCurrency(Math.round(priceNotVatN)));
					}else{
						$('#txtPricePackageNotVATDl').val(0);
						$('#txtPricePackageDl').val(0);
					}
				}else{
					$('#txtPricePackageDl').val(0);
					$('#txtPricePackageNotVATDl').val(0);
				}
			}else{
				return;
			}
		}else{
			return;
		}
	},
	
	
	///@author HungLM16; @since JULY 03,2014; @description Import Gia
	importPriceProduct : function() {
		$('.SuccessMsgStyle').html('').hide();
		$('.ErrorMsgStyle').html('').hide();
		
		Utils.importExcelUtils(function(data) {
			PriceManager.searchProductPrice();
			$('#excelFileFormPrice').val("").change();
			$('#fakefilepc').val("").change();
			if (data.message!=undefined && data.message!=null && data.message.trim().length >0) {
				$('#errMsgImportPrice').html(data.message.trim()).change().show();
			} else {
				$('#successMsgImportPrice').html(price_search_error_03).show();
				var tm = setTimeout(function() {
					//Load lai danh sach quyen
					$('#successMsgImportPrice').html("").hide();
					clearTimeout(tm);
				 }, 1500);
			}
		}, 'importFrmPrice', 'excelFileFormPrice', 'errMsgImportPrice');
		
	},
	
	///@author HungLM16; @since JULY 17,2014; @description Bat cac su kien sau khi chon tren Grid
	exportBySearchPrice:function() {
		$('.ErrorMsgStyle').html('').hide();
		var msg = '';
		if(PriceManager._paramSearch!=null) {
			var rows = $('#dgProductPrice').datagrid("getData");
			if(rows == null || rows.length==0) {
				msg = price_search_error_09;
			}
		}else{
			msg = price_search_error_09;
		}
		if(msg.length > 0) {
			$('#errMsgImportPrice').html(msg).show();
			return false;
		}
		var msg = price_search_error_10;
		$.messager.confirm(price_grid_column18, msg, function(r) {
			if (r) {
				var params = PriceManager._paramSearch;
				ReportUtils.exportReport('/price-manage/exportPriceProduct', params, 'errMsgPermission');
			}
		});
		
	},
	
	fillTxtProductCodeByF9 : function(code,name) {
		$('#txtProductCode').val(code).show();
		$('#txtProductName').val(name).show();
		$('#common-dialog-search-2-textbox').dialog('close');
		$('#txtProductCode').focus();
	},
	
	fillTxtShopCodeByF9 : function(code) {
		$('#txtShopCode').val(code).show();
		$('#common-dialog-search-2-textbox').dialog('close');
		$('#txtShopCode').focus();
	},
	
	/**
	 * @author hunglm16
	 * @since September 22, 2014
	 **/
	changeCheckboxAllByGridPrdPr : function (cbx) {
		var check = $(cbx).prop("checked");
		if (check) {
			$('.changegridcbxr').prop("checked", true);
			var rows = $('.changegridcbxr');
			PriceManager._arrId = new Array();
			for (var i=0; i<rows.length; i++) {
				PriceManager._arrId.push(Number($($('.changegridcbxr')[i]).prop("value")));
			}
		} else {
			$('.changegridcbxr').prop("checked", false);
			PriceManager._arrId = new Array();
		}
	},
	/**
	 * @author hunglm16
	 * @since September 22, 2014
	 **/
	changeCheckboxByGridPrdPr : function (cbx) {
		var check = $(cbx).prop("checked");
		if (check) {
			if ($('.changegridcbxr:checked').length == $('.changegridcbxr').length) {
				$('#ckallPrdPr').click();
			} else {
				PriceManager._arrId.push(Number($(cbx).prop("value")));
			}
		} else {
			$('#ckallPrdPr').prop("checked", false);
			if(PriceManager._arrId != null && PriceManager._arrId.length > 0) {
				var i = PriceManager._arrId.indexOf(Number($(cbx).prop("value")));
				if(i != -1) {
					PriceManager._arrId.splice(i, 1);
				}
			}
		}
	},
	
	updateChangeWaittingPrice : function (flag) {
		if(PriceManager._arrId != null && PriceManager._arrId.length > 0) {
			$('.ErrorMsgStyle').html('').hide();
			var textG = "-1";
			for (var i=0; i<PriceManager._arrId.length; i++) {
				textG = textG + "," + PriceManager._arrId;
			}
			textG = textG.replace(/-1,/g, "");
			textG = textG.replace(/-1/g, "");
			var msg = '';
			if(flag == PriceManager._flagChangeWaitting.isrunning) {
				msg = price_search_error_13;				
			} else if(flag == PriceManager._flagChangeWaitting.isdelete) {
				msg = price_search_error_14;
			}
			if (msg.length == 0) {
				$('#errMsgImportPrice').html(price_search_error_11).show();
				return false;
			}
			var params = {
					textG: textG,
					isEvent: flag
			};
			Utils.addOrSaveData(params, "/price-manage/changeWaitingPrice", null, 'errMsgImportPrice', function(data) {
				PriceManager.searchProductPrice();
				$("#successMsgImportPrice").html(price_search_error_03).show();
				setTimeout(function() {
					$('.SuccessMsgStyle').html("").hide();
				 }, 1500);
			}, null, null, null, msg);
			return false;
		} else {
			$('#errMsgImportPrice').html(price_search_error_12).show();
			return false;
		}
	},

	/**
	 * download file template import gia san pham
	 * @author vuongbd
	 * @since 06/05/2015
	 */
	downloadImportPriceTemplateFile: function() {
		var url = "/price-manage/download-import-price-template-file";
		ReportUtils.exportReport(url, {}, 'errMsg');
		return false;
	},
};
/**
 * Cập nhật thông tin thuế GTGT
 */
var SPAdjustmentTax = {
		chooseStatus : 0,		
		listPayment : new Array(),
		oldInvoice : new Array(),
		listInvoiceId : new Array(),
		mapOrderNumber: new Array(),
		mapInvoiceNumber: new Array(),
		cellChanges: new Array(),
		tmpInvoice: '',
		_textChangeBySeach: {
			all: '',
			max: '',
			min: '',
			avg: ''
		},
		_mapGridDonTong: null,
		_donChinh:null,
		hideAllTab : function() {
			$('#infoTab').removeClass('Active');
			$('#propertyTab').removeClass('Active');
			$('#infoTabContainer').hide();
			$('#propertyTabContainer').hide();
		},
		initInfo: function() {
			Utils.initUnitCbx('cbxShop', {}, null, function(){
				SPAdjustmentTax.handleSelectShop();
			});//230		
			Utils.initUnitCbx('cbxShop1', {}, null, function(){
				SPAdjustmentTax.handleSelectShop1();
			});//230		
		},
		handleSelectShop1: function() {
			var params= {};
			params.shopCode = $('#cbxShop1').combobox('getValue');
			Utils.getJSONDataByAjax(params, '/sale-product/assign-transfer-staff/lst-staff-cbx' , function(data) {
				if (data != null) {
					$('#strLockDate').html(data.lockDate);
					$('#createToDate').val(data.lockDate);
					$('#createFromDate').val(data.lockDate);
					$('#createFromDate1').val(data.lockDate);
					$('#createToDate1').val(data.lockDate);
					Utils.bindStaffCbx('#saleStaffCode1', data.lstSale, null, 206);
					Utils.bindStaffCbx('#deliveryStaffCode1', data.lstDeliver, null, 206);
				}
			});
			
		},
		handleSelectShop: function() {
			var params= {};
			params.shopCode = $('#cbxShop').combobox('getValue');
			Utils.getJSONDataByAjax(params, '/sale-product/assign-transfer-staff/lst-staff-cbx' , function(data) {
				if (data != null) {
					$('#strLockDate').html(data.lockDate);
					$('#date').val(data.lockDate);
					$('#createToDate').val(data.lockDate);
					$('#createFromDate').val(data.lockDate);
					$('#createFromDate1').val(data.lockDate);
					$('#createToDate1').val(data.lockDate);
					Utils.bindStaffCbx('#saleStaffCode', data.lstSale, null, 206);
					Utils.bindStaffCbx('#deliveryStaffCode', data.lstDeliver, null, 206);
				}
			});
			
		},
		showTab1 : function() {
			SPAdjustmentTax.hideAllTab();
			$('#numberValueOrder').val(formatCurrency(SPAdjustmentTax._textChangeBySeach.avg));
			$('#infoTab').addClass('Active');
			$('#infoTabContainer').show();
		},
		showTab2 : function() {
			SPAdjustmentTax.hideAllTab();
			$('#propertyTab').addClass('Active');
			$('#propertyTabContainer').show();
			$('#chooseStatus1').val('0');//chua co hoa don
			$('#chooseStatus1').change();
			$('#choosePaymentMethod1').val('-1');
			$('#numberValueOrder1').val(formatCurrency(SPAdjustmentTax._textChangeBySeach.avg));
			$('#choosePaymentMethod1').change();
			Utils.bindAutoSearchEx('');
			SPAdjustmentTax.oldInvoice = new Array();
			var $container = $("#grid3");
			$container.handsontable({
			  rowHeaders: true,
			  colHeaders: true,
			  manualColumnResize: true,
			  colWidths: [130, 150, 120, 90, 90, 180, 180, 250, 100, 100, 80, 40],
			  colHeaders: ['Hóa đơn nội bộ', 'Hóa đơn đỏ', 'Thanh toán', 'Tổng tiền', 'Tiền thuế', 'Khách hàng', 'Tên đơn vị', 'Địa chỉ', 'NVBH', 'NVGH', 'Ngày', 'Thuế suất'],
			  minSpareRows: 1,
			  width: $(window).width() - 45,
			  columns: [
			    {data: "orderNumber", readOnly: true},
			    {data: "invoiceNumber", renderer:SPAdjustmentTax.boldRedRenderer, validator: SPAdjustmentTax.maxLengthValidate, allowInvalid: false},
			    {data: "payment", type: 'dropdown', source: ["Tiền mặt", "Chuyển khoản"]},
			    {data: "total", readOnly: true, renderer:SPAdjustmentTax.boldRenderer},
			    {data: "taxTotal", readOnly: true, renderer:SPAdjustmentTax.boldRenderer},
			    {data: "customer", readOnly: true},
			    {data: "cpnyName", readOnly: true},
			    {data: "customerAddr", readOnly: true},
			    {data: "staffInfo", readOnly: true},
			    {data: "deliveryStaffInfo", readOnly: true},
			    {data: "date", readOnly: true},
			    {data: "stringVat", readOnly: true}
			  ],
			  afterChange: function(changes, source) {
				  if(changes != null){
					  if(changes[0][1] == 'invoiceNumber'){
						  $.each(changes, function (index, element) {
					            var change = element;
					            var rowIndex = change[0];

					            var oldValue = change[2];
					            var newValue = change[3];

					            var cellChange = {
					                'rowIndex': rowIndex,
					                'columnIndex': 1
					            };

					            if(oldValue != newValue){
					            	SPAdjustmentTax.cellChanges.push(cellChange);
					            	var td = $("#grid3").handsontable('getCell', rowIndex, 1 );
					            	$(td).css('color','Maroon');
					            }
					       });
					  }
				  }
			  },
			  afterRender: function () {
				  $.each(SPAdjustmentTax.cellChanges, function (index, element) {
		              var cellChange = element;
		              var rowIndex = cellChange['rowIndex'];
		              var columnIndex = cellChange['columnIndex'];
		              var td = $("#grid3").handsontable('getCell', rowIndex, columnIndex );
		              $(td).css('color','Maroon');
		          });
			  }
			});
			SPAdjustmentTax.khoiTaoTabDonGop();
			var handsontable1 = $container.data('handsontable');
			handsontable1.loadData(null);
		},
		khoiTaoTabDonGop:function(){
			SPAdjustmentTax._mapGridDonTong = new Map();
			$('#grid1').datagrid({
				rownumbers : true,
				columns:[[	
							{field: 'soId', title: '', width: 30, align: 'center', sortable:false,resizable:false,formatter:function(value,row,index){
								return '<a id="grid1Add_' +index+'"  href="javascript:void(0)" onclick="return SPAdjustmentTax.addDonGop(' +index+');"><img title="Thêm vào đơn tổng" src="/resources/images/icon-add.png" style="padding-right: 10px;"></a>';
					    	}},
							{field: 'orderNumber', title: 'Hóa đơn nội bộ', width: 150, align: 'left', sortable:false,resizable:false},
							{field: 'customer', title: 'Khách hàng', width: 150, align: 'left', sortable:false,resizable:false,formatter:function(value,row,index){
					    		if(row.customer!=null){
					    			return Utils.XSSEncode(row.customer.shortCode+"-"+row.customer.customerName);
					    		}
								return "";
					    	}},
						    {field: 'address', title: 'Địa chỉ', width: 370, align: 'left', sortable:false,resizable:false,formatter:function(value,row,index){
					    		if(row.customer!=null){
					    			return Utils.XSSEncode(row.customer.address);
					    		}
								return "";
					    	}},
						    {field: 'staff', title: 'NVBH', width: 150, align: 'left', sortable:false,resizable:false,formatter:function(value,row,index){
					    		if(row.staff!=null){
					    			return Utils.XSSEncode(row.staff.staffCode);
					    		}
								return "";
					    	}},
						    {field: 'delivery', title: 'NVGH', width: 150, align: 'left', sortable:false,resizable:false,formatter:function(value,row,index){
					    		if(row.delivery!=null){
					    			return Utils.XSSEncode(row.delivery.staffCode);
					    		}
								return "";return formatCurrency(row.amount);
					    	}},
						    {field: 'orderDate', title: 'Ngày', width: 100, align: 'center', sortable:false,resizable:false,formatter:function(value,row,index){
						    	return(row.orderDate == null)? '':$.datepicker.formatDate('dd/mm/yy', new Date(row.orderDate));
					    	}},		    
						    {field: 'total', title: 'Thành tiền', width: 100, align: 'right', sortable:false,resizable:false,formatter:function(value,row,index){
					    		return formatCurrency(row.total);
					    	}}
						  ]],	  
						  onLoadSuccess:function(){
								$('.datagrid-header-rownumber').html('STT');  
						      	$('#grid1').datagrid('resize');					
						  }
			});
			$('#grid2').datagrid({
				rownumbers : true,
				columns:[[	
							{field: 'soId', title: '', width: 30, align: 'center', sortable:false,resizable:false,formatter:function(value,row,index){
								return '<a id="grid2Delete_' +index+'" href="javascript:void(0)" onclick="return SPAdjustmentTax.deleteDonGop(' +index+');"><img title="Xóa khỏi đơn tổng" src="/resources/images/icon-delete.png" style="padding-right: 10px;"></a>';
					    	}},
					    	{field: 'donChinh', title: 'Đơn chính', width: 50, align: 'center', sortable:false,resizable:false,formatter:function(value,row,index){
					    		if(row.isDonChinh!=undefined && row.isDonChinh!=null && row.isDonChinh == 1){
					    			return '<input type="radio" name="donChinh" checked id=' +row.id+' value=' +row.id+' onclick="SPAdjustmentTax.checkRadiobt(this);" >';
					    		}
					    		return '<input type="radio" name="donChinh" id=' +row.id+' value=' +row.id+' onclick="SPAdjustmentTax.checkRadiobt(this);">';
					    	}},
							{field: 'orderNumber', title: 'Hóa đơn nội bộ', width: 150, align: 'left', sortable:false,resizable:false},
							{field: 'customer', title: 'Khách hàng', width: 150, align: 'left', sortable:false,resizable:false,formatter:function(value,row,index){
					    		if(row.customer!=null){
					    			return Utils.XSSEncode(row.customer.shortCode+"-"+row.customer.customerName);
					    		}
								return "";
					    	}},
						    {field: 'address', title: 'Địa chỉ', width: 370, align: 'left', sortable:false,resizable:false,formatter:function(value,row,index){
					    		if(row.customer!=null){
					    			return Utils.XSSEncode(row.customer.address);
					    		}
								return "";
					    	}},
						    {field: 'staff', title: 'NVBH', width: 150, align: 'left', sortable:false,resizable:false,formatter:function(value,row,index){
					    		if(row.staff!=null){
					    			return Utils.XSSEncode(row.staff.staffCode);
					    		}
								return "";
					    	}},
						    {field: 'delivery', title: 'NVGH', width: 150, align: 'left', sortable:false,resizable:false,formatter:function(value,row,index){
					    		if(row.delivery!=null){
					    			return Utils.XSSEncode(row.delivery.staffCode);
					    		}
								return "";return formatCurrency(row.amount);
					    	}},
						    {field: 'orderDate', title: 'Ngày', width: 100, align: 'center', sortable:false,resizable:false,formatter:function(value,row,index){
						    	return(row.orderDate == null)? '':$.datepicker.formatDate('dd/mm/yy', new Date(row.orderDate));
					    	}},		    
						    {field: 'total', title: 'Thành tiền', width: 100, align: 'right', sortable:false,resizable:false,formatter:function(value,row,index){
					    		return formatCurrency(row.total);
					    	}}
//					    	,		    
//						    {field: 'vat', title: 'Thuế suất', width: 70, align: 'right',sortable:false,resizable:false,formatter:function(value,row,index){
//						    	return(row.vat == null)?'': row.vat +'%';
//					    	}}       
						  ]],	  
						  onLoadSuccess:function(){
								$('.datagrid-header-rownumber').html('STT');  
						      	$('#grid2').datagrid('resize');							
						  }
			});
			$('#grid1').datagrid('loadData', []);
			$('#grid2').datagrid('loadData', []);
			var $container1 = $("#grid3");
			var handsontable1 = $container1.data('handsontable');
			if(handsontable1!=null){
				handsontable1.loadData(null);
			}
			
		},
		checkRadiobt:function(myRadio){
			SPAdjustmentTax._donChinh = myRadio.value;
			return true;
		},
		addDonGop:function(index){
			var row = $('#grid1').datagrid('getRows')[index];
			if(row !=null){
				if (SPAdjustmentTax._mapGridDonTong.get(row.id) == null) {
					if(SPAdjustmentTax._mapGridDonTong.valArray.length == 0){
						row.isDonChinh = 1;
						SPAdjustmentTax._donChinh = row.id;
					}
					SPAdjustmentTax._mapGridDonTong.put(row.id, row);
					$('#grid2').datagrid('appendRow',row);
					row.isDonChinh = null;
				}
			}
		},
		deleteDonGop:function(index){
			var row = $('#grid2').datagrid('getRows')[index];
			if(row !=null){
					SPAdjustmentTax._donChinh = null;
//				}
				if (SPAdjustmentTax._mapGridDonTong.get(row.id) != null) {
					SPAdjustmentTax._mapGridDonTong.remove(row.id);
					$('#grid2').datagrid('deleteRow',index);
					var rows = $('#grid2').datagrid('getRows');
					$('#grid2').datagrid('loadData',rows);
					var $container1 = $("#grid3");
					var handsontable1 = $container1.data('handsontable');
					if(handsontable1!=null){
						handsontable1.loadData(null);
					}
				}
			}
		},
		onInvoiceNumberInput: function(id) {
			var classIdName = id.split('-')[0];
			var classQueryId = $('.' +classIdName);
			for(var i = 0; i < classQueryId.length; i++) {
				$(classQueryId).attr('checked', 'checked');
			}
		},
		checkAll: function(checkAll) {
			Utils.onCheckAllChange(checkAll, 'ApproveStatus');
		},
		checkThisBox: function(checkThis, id) {
			Utils.onCheckboxChangeForCheckAll(checkThis, 'ApproveStatus', 'allApproveStatus');
			if(id != undefined) {
				if(checkThis) {
					var classIdName = id.split('-')[0];
					var classQueryId = $('.' +classIdName);
					for(var i = 0; i < classQueryId.length; i++) {
						$(classQueryId).attr('checked', 'checked');
					}
				} else {
					var classIdName = id.split('-')[0];
					var classQueryId = $('.' +classIdName);
					for(var i = 0; i < classQueryId.length; i++) {
						$(classQueryId).removeAttr('checked');
					}
				}
			}
		},
		getParams:function(){
			var params = new Object();		
			/*if ($('#cbxShop').combobox('getValue')!= undefined && $('#cbxShop').combobox('getValue') != null) {
				params.shopCode = $('#cbxShop').combobox('getValue').trim();
			}*/		
			params.chooseStatus = $('#chooseStatus').val().trim();
			params.createFromDate = $('#createFromDate').val().trim();
			params.createToDate = $('#createToDate').val().trim();
			params.choosePaymentMethod = $('#choosePaymentMethod').val().trim();
			params.taxValue = $('#taxValue').val().trim();
			params.deliveryStaffCode = encodeChar($('#deliveryStaffCode').combobox('getValue').trim()); 
			params.invoiceNumber = $('#invoiceNumber').val().trim();
			params.redInvoiceNumber = $('#redInvoiceNumber').val().trim();
			params.saleStaffCode = encodeChar($('#saleStaffCode').combobox('getValue').trim());
			params.customerCode = encodeChar($('#customerCode').val().trim());
//			params.isValueOrder = $('#isValueOrder').combobox('getValue').trim();
			params.isValueOrder = $('#isValueOrder').val().trim();
			var numberOrder = $('#numberValueOrder').val().trim();
			var numberValueOrder = "";
			if ( $('#numberValueOrder').val() != undefined &&  $('#numberValueOrder').val() != null &&  $('#numberValueOrder').val() !='') {
				numberValueOrder = numberOrder.replace(/,/g, '');
			}
			params.numberValueOrder = numberValueOrder;
			return params;
		},	
		getParams1:function(){
			var params = new Object();		
		/*	if ($('#cbxShop1').combobox('getValue')!= undefined && $('#cbxShop1').combobox('getValue') != null) {
				params.shopCode = $('#cbxShop1').combobox('getValue').trim();
			}*/
			params.chooseStatus = $('#chooseStatus1').val().trim();
			params.createFromDate = $('#createFromDate1').val().trim();
			params.createToDate = $('#createToDate1').val().trim();
			params.choosePaymentMethod = $('#choosePaymentMethod1').val().trim();
			params.taxValue = $('#taxValue1').val().trim();
			params.deliveryStaffCode = encodeChar($('#deliveryStaffCode1').combobox('getValue').trim()); 
			params.invoiceNumber = $('#invoiceNumber1').val().trim();
			params.redInvoiceNumber = $('#redInvoiceNumber1').val().trim();
			params.saleStaffCode = encodeChar($('#saleStaffCode1').combobox('getValue').trim());
			params.customerCode = encodeChar($('#customerCode1').val().trim());
			params.isValueOrder = $('#isValueOrder1').val().trim();
			var numberOrder = $('#numberValueOrder1').val().trim();
			var numberValueOrder1 = "";
			if ( $('#numberValueOrder1').val() != undefined &&  $('#numberValueOrder1').val() != null &&  $('#numberValueOrder1').val() !='') {
				numberValueOrder1 = numberOrder.replace(/,/g, '');
			}
			params.numberValueOrder1 = numberValueOrder1;
			return params;
		},	
		//tim kiem hoa don vat le
		searchVATle:	function() {
			var msg = '';
			var fDate = $('#createFromDate').val();
			if(isNullOrEmpty(fDate)) {
				msg = 'Từ ngày không được để trống. Vui lòng nhập từ ngày';
				$('#createFromDate').focus();
			} else if(isNullOrEmpty(msg)) {
				msg = Utils.getMessageOfInvalidFormatDate('createFromDate', 'từ ngày');
			}
			var tDate = $('#createToDate').val();
			if(isNullOrEmpty(tDate)) {
				msg = 'Đến ngày không được để trống. Vui lòng nhập đến ngày';
				$('#createToDate').focus();
			} else if(isNullOrEmpty(msg)) {
				msg = Utils.getMessageOfInvalidFormatDate('createToDate', 'đến ngày');
			}
			if (!Utils.compareDate(fDate, tDate)) {
				msg = msgErr_fromdate_greater_todate;
				$('#fDate').focus();
			}
			if (msg.length == 0) {
			msg = Utils.getMessageOfSpecialCharactersValidateEx('numberValueOrder','Giá trị ĐH', Utils._TF_NUMBER_COMMA);
			$('#numberValueOrder').focus();
			}
			if (msg.length > 0) {
				$('#errMsg').html(msg).show();
				return false;
			}
			
			$('#divOverlay').show();
			var $container = $("#grid");
			var handsontable = $container.data('handsontable');
			var params = SPAdjustmentTax.getParams();
			params.shopCodeSearch = $('#cbxShop').combobox('getValue').trim();
			//params.page = 1;
			//params.rows = 50;
			var kData = $.param(params, true);
			$.ajax({
			    url: "/sale-product/adjust-tax/search",
			    dataType: 'json',
			    data : (kData), 
			    type: 'GET',
			    success: function (res) {
			    	$('#divOverlay').hide();
			    	SPAdjustmentTax.cellChanges = new Array();
			    	if(res.rows.length > 0){
			    		handsontable.loadData(res.rows);
			    		SPAdjustmentTax.oldInvoice = $('#grid').handsontable('getDataAtCol', 1);
			    		var chooseStatus = $('#chooseStatus').val();
			    		if(chooseStatus != 0){
							$("#chkTypePay").css("display", "none");
							$("#chkTypePay1").css("display", "none");
							$("#chkTypePay2").css("display", "none");
							
			    			SPAdjustmentTax.listInvoiceId = new Array();
			    			for(var i = 0; i < res.rows.length; i++){
				    			SPAdjustmentTax.listInvoiceId.push(res.rows[i].id);
				    		}
			    			if(SPAdjustmentTax.listInvoiceId[SPAdjustmentTax.listInvoiceId.length - 1] == null){
			    				SPAdjustmentTax.listInvoiceId.splice(SPAdjustmentTax.listInvoiceId.length - 1, 1);
			    			}
			    		}else{
							$("#chkTypePay").css("display", "block");
							$("#chkTypePay1").css("display", "block");
							$("#chkTypePay2").css("display", "block");
			    		}
			    	}else{
			    		handsontable.loadData(null);
			    		SPAdjustmentTax.oldInvoice = new Array();
			    	}
			    }
			 });
			var value = $('#isValueOrder').val();
			if (Number(value) ==  -1) {
				SPAdjustmentTax._textChangeBySeach.all = $('#numberValueOrder').val().replace(/,/g, '');
			} else {
				SPAdjustmentTax._textChangeBySeach.avg = $('#numberValueOrder').val().replace(/,/g, '');
			}			
			return false;
		},
		//tim kiem hoa don vat gop
		searchVATgop:	function() {
			$('.ErrorMsgStyle').html('').hide();
			$('#grid2').datagrid('loadData', []);
			var $container1 = $("#grid3");
			var handsontable1 = $container1.data('handsontable');
			if(handsontable1!=null){
				handsontable1.loadData(null);
			}
			var msg = '';
			var fDate = $('#createFromDate1').val();
			if(isNullOrEmpty(fDate)) {
				msg = 'Từ ngày không được để trống. Vui lòng nhập từ ngày';
				$('#createFromDate1').focus();
			} else if(isNullOrEmpty(msg)) {
				msg = Utils.getMessageOfInvalidFormatDate('createFromDate1', 'từ ngày');
			}
			var tDate = $('#createToDate1').val();
			if(isNullOrEmpty(tDate)) {
				msg = 'Đến ngày không được để trống. Vui lòng nhập đến ngày';
				$('#createToDate1').focus();
			} else if(isNullOrEmpty(msg)) {
				msg = Utils.getMessageOfInvalidFormatDate('createToDate1', 'đến ngày');
			}
			if (!Utils.compareDate(fDate, tDate)) {
				msg = msgErr_fromdate_greater_todate;
				$('#fDate1').focus();
			}
			
			if (msg.length > 0) {
				$('#errMsg1').html(msg).show();
				return false;
			}
			$("#grid1").datagrid({
				url:'/sale-product/adjust-tax/searchVATgop',
				width: $('#areaGrid1').width(),	
				queryParams: {
					chooseStatus : $('#chooseStatus1').val().trim(),
					createFromDate : $('#createFromDate1').val().trim(),
					createToDate : $('#createToDate1').val().trim(),
					choosePaymentMethod : $('#choosePaymentMethod1').val().trim(),
					taxValue : $('#taxValue1').val().trim(),
					deliveryStaffCode : encodeChar($('#deliveryStaffCode1').combobox('getValue').trim()), 
					invoiceNumber : $('#invoiceNumber1').val().trim(),
					redInvoiceNumber : $('#redInvoiceNumber1').val().trim(),
					saleStaffCode : encodeChar($('#saleStaffCode1').combobox('getValue').trim()),
					customerCode : encodeChar($('#customerCode1').val().trim()),
					isValueOrder : $('#isValueOrder1').val().trim(),
				//	numberOrder1 : $('#numberValueOrder1').val().trim(),
					numberValueOrder1 : $('#numberValueOrder1').val().trim().replace(/,/g, ''),			
					shopCodeSearch : $('#cbxShop1').combobox('getValue').trim(),
				},
				onLoadSuccess:function(rows){
					$('.datagrid-header-rownumber').html('STT');  
			      	$('#grid1').datagrid('resize');		
			      	SPAdjustmentTax._mapGridDonTong = new Map();
			      	if(rows.length>0){
			      		$("#chkTypePayEX").css("display", "none");
						$("#chkTypePayEX1").css("display", "none");
						$("#chkTypePayEX2").css("display", "none");
			      	}else{
			      		$("#chkTypePayEX").css("display", "block");
						$("#chkTypePayEX1").css("display", "block");
						$("#chkTypePayEX2").css("display", "block");
			      	}
			  }
			});
			var value = $('#isValueOrder1').val();
			if (Number(value) ==  -1) {
				SPAdjustmentTax._textChangeBySeach.all = $('#numberValueOrder1').val().replace(/,/g, '');
			} else {
				SPAdjustmentTax._textChangeBySeach.avg = $('#numberValueOrder1').val().replace(/,/g, '');
			}
			return false;
		},
		searchVATGopDaCoHoaDon:	function() {
			$('.ErrorMsgStyle').html('').hide();
			var msg = '';
			var fDate = $('#createFromDate1').val();
			if(isNullOrEmpty(fDate)) {
				msg = 'Từ ngày không được để trống. Vui lòng nhập từ ngày';
				$('#createFromDate1').focus();
			} else if(isNullOrEmpty(msg)) {
				msg = Utils.getMessageOfInvalidFormatDate('createFromDate1', 'từ ngày');
			}
			var tDate = $('#createToDate1').val();
			if(isNullOrEmpty(tDate)) {
				msg = 'Đến ngày không được để trống. Vui lòng nhập đến ngày';
				$('#createToDate1').focus();
			} else if(isNullOrEmpty(msg)) {
				msg = Utils.getMessageOfInvalidFormatDate('createToDate1', 'đến ngày');
			}
			if (!Utils.compareDate(fDate, tDate)) {
				msg = msgErr_fromdate_greater_todate;
				$('#fDate1').focus();
			}
			
			if (msg.length > 0) {
				$('#errMsg1').html(msg).show();
				return false;
			}
			
			$('#divOverlay').show();
			var $container = $("#grid3");
			var handsontable = $container.data('handsontable');
			var params = SPAdjustmentTax.getParams1();
			params.shopCodeSearch = $('#cbxShop1').combobox('getValue').trim();
			params.isVATGop = true;
			var kData = $.param(params, true);
			$.ajax({
			    url: "/sale-product/adjust-tax/search",
			    dataType: 'json',
			    data : (kData),
			    type: 'GET',
			    success: function (res) {
			    	$('#divOverlay').hide();
			    	SPAdjustmentTax.cellChanges = new Array();
			    	if(res.rows.length > 0){
			    		handsontable.loadData(res.rows);
			    		SPAdjustmentTax.oldInvoice = $('#grid3').handsontable('getDataAtCol', 1);
			    		var chooseStatus = $('#chooseStatus1').val();
			    		if(chooseStatus != 0){
							$("#chkTypePayEX").css("display", "none");
							$("#chkTypePayEX1").css("display", "none");
							$("#chkTypePayEX2").css("display", "none");
							
			    			SPAdjustmentTax.listInvoiceId = new Array();
			    			for(var i = 0; i < res.rows.length; i++){
				    			SPAdjustmentTax.listInvoiceId.push(res.rows[i].id);
				    		}
			    			if(SPAdjustmentTax.listInvoiceId[SPAdjustmentTax.listInvoiceId.length - 1] == null){
			    				SPAdjustmentTax.listInvoiceId.splice(SPAdjustmentTax.listInvoiceId.length - 1, 1);
			    			}
			    		}else{
							$("#chkTypePayEX").css("display", "block");
							$("#chkTypePayEX1").css("display", "block");
							$("#chkTypePayEX2").css("display", "block");
			    		}
			    	}else{
			    		handsontable.loadData(null);
			    		SPAdjustmentTax.oldInvoice = new Array();
			    	}
			    }
			 });
			var value = $('#isValueOrder1').val();
			if (Number(value) ==  -1) {
				SPAdjustmentTax._textChangeBySeach.all = $('#numberValueOrder1').val().replace(/,/g, '');
			} else {
				SPAdjustmentTax._textChangeBySeach.avg = $('#numberValueOrder1').val().replace(/,/g, '');
			}
			return false;
		},
		onSelectStatusChange1: function() {
			var chooseStatus = $('#chooseStatus1').val();
			
			if(chooseStatus == 0) {	
				$('#divDonTong').show();
				$('#save1').unbind('click');
				$('#save1').bind('click', function(e) {
					SPAdjustmentTax.saveDonGop();
				});
				SPAdjustmentTax.khoiTaoTabDonGop();
				$('#save1').show();
				$('#choosePaymentMethod1').parent().addClass('BoxDisSelect');
				$('#choosePaymentMethod1').attr('disabled', 'disabled');
				$('#taxValue1').attr('disabled', 'disabled');
				$('#redInvoiceNumber1').attr('disabled', 'disabled');
				$('#redInvoiceNumber1').val('');
			
				$('#btnSearch1').unbind('click');
				$('#btnSearch1').bind('click', function(e) {
					SPAdjustmentTax.searchVATgop();
				});
				
			} else {
				$('#divDonTong').hide();
				$('#save1').show();
				SPAdjustmentTax.khoiTaoTabDonGop();
				$('#choosePaymentMethod1').parent().removeClass('BoxDisSelect');
				$('#choosePaymentMethod1').removeAttr('disabled');
				$('#taxValue1').removeAttr('disabled');
				$('#redInvoiceNumber1').removeAttr('disabled', 'disabled');
				
				$('#save1').unbind('click');
				$('#save1').bind('click', function(e) {
					SPAdjustmentTax.saveWithInvoice1();
				});
				$('#btnSearch1').unbind('click');
				$('#btnSearch1').bind('click', function(e) {
					SPAdjustmentTax.searchVATGopDaCoHoaDon();
				});
			}	
			SPAdjustmentTax.chooseStatus = chooseStatus;
			return false;
		},
		onSelectStatusChange: function() {
			var chooseStatus = $('#chooseStatus').val();
			if(chooseStatus == 0) {				
				$('#cancel').hide();
				$('#save').show();
				$('#choosePaymentMethod').parent().addClass('BoxDisSelect');
				$('#choosePaymentMethod').attr('disabled', 'disabled');
				$('#taxValue').attr('disabled', 'disabled');
				$('#redInvoiceNumber').attr('disabled', 'disabled');
				$('#redInvoiceNumber').val('');
			
				$('#save').unbind('click');
				$('#save').bind('click', function(e) {
					SPAdjustmentTax.save();
				});
			} else {
				$('#cancel').show();
				$('#save').show();
				$('#choosePaymentMethod').parent().removeClass('BoxDisSelect');
				$('#choosePaymentMethod').removeAttr('disabled');
				$('#taxValue').removeAttr('disabled');
				$('#redInvoiceNumber').removeAttr('disabled', 'disabled');
				
				$('#save').unbind('click');
				$('#save').bind('click', function(e) {
					SPAdjustmentTax.saveWithInvoice();
				});
			}	
			SPAdjustmentTax.chooseStatus = chooseStatus;
			return false;
		},
		checkInputInvoiceNumber: function(className) {			
			var msg = '';
			var classQuery = $('.' +className);			
			for(var i = 0; i < classQuery.length; i++) {
				var idSelector = $(classQuery[i]).attr('id');
				msg = Utils.getMessageOfRequireCheck(idSelector, 'Hóa đơn đỏ');
				if(msg.length = 0) {
					msg = Utils.getMessageOfSpecialCharactersValidate(idSelector, 'Hóa đơn đỏ', Utils._CODE);
				}
				var newInvoiceNumber = $('#' +idSelector).val().trim();
				var oldInvoiceNumber = $('#' +idSelector).attr('vatOldOrder');
				oldInvoiceNumber = oldInvoiceNumber==undefined? '':oldInvoiceNumber.trim();
				if(oldInvoiceNumber.length>0 && oldInvoiceNumber.toUpperCase() == newInvoiceNumber.toUpperCase()){
					msg = 'Số hóa đơn mới không được trùng số hóa đơn cũ. Vui lòng nhập số hóa đơn mới';
				}
				if(msg.length > 0) {
					$('#errMsg').html(msg).show();					
					$('#' +idSelector).focus();
					return false;
				}			
			}		
			return true;
		},
		saveDonGop: function(){
			$('.ErrorMsgStyle').html('').hide();
			var msg = '';
			if(!/^[^<|>|?|\\|\'|\"|&|~#|$|%|@|*|(|)|^|\[|\]|\{|\}|ư|ơ|Ơ|Ư|ô|Ô|ă|Ă|â|Â|`]+$/.test(SPAdjustmentTax.tmpInvoice) && SPAdjustmentTax.tmpInvoice != ''){
				msg = 'Số hóa đơn không được có ký tự đặc biệt. Vui lòng kiểm tra lại.';
			}
			if(msg.length > 0) {
				$('#errMsg2').html(msg).show();
				return false;
			}
			
			checkInput = true;
			var listPaymentMethod = Array();
			
			msg = Utils.getMessageOfRequireCheck('createFromDate1', 'Từ ngày');
			if(msg.length > 0) {
				$('#errMsg2').html(msg).show();
				return false;
			}			

			var listOrderNumber = $('#grid3').handsontable('getDataAtCol', 0);
			var listInvoiceNumber = $('#grid3').handsontable('getDataAtCol', 1);
			var listPayMethod = $('#grid3').handsontable('getDataAtCol', 2);
			SPAdjustmentTax.mapInvoiceNumber = new Array();
			SPAdjustmentTax.mapOrderNumber = new Array();
			
			var tmpOrderNumber = listOrderNumber[0];
			var tmpOrderNumber1 = '';
			var count = 0;
			var count1 = 0;
			
			////////////////////////////
			for(var i = listOrderNumber.length - 1; i >= 0; i--){
				if(listOrderNumber[i] == null){
					listOrderNumber.splice(i, 1);
				}else{
					break;
				}
			}
			listOrderNumber.push(null);
			
			for(var i = 0, size = listOrderNumber.length; i < size; i++){
				//tao ra mapOrder
				if(listOrderNumber[i] === tmpOrderNumber){
					count++;
				}else{
					var data = new Array();
					data.push(tmpOrderNumber);
					data.push(count);
					SPAdjustmentTax.mapOrderNumber.push(data);
					
					tmpOrderNumber = listOrderNumber[i];
					count = 1;
				}
			
				//tao ra mapInvoice
				if(tmpOrderNumber1 == '' && listInvoiceNumber[i] != ''){
					tmpOrderNumber1 = listOrderNumber[i];
				}
				if(listOrderNumber[i] === tmpOrderNumber1 && listInvoiceNumber[i] != ''){
					count1++;
				}else{
					if(count1 > 0){
						var data = new Array();
						data.push(tmpOrderNumber1);
						data.push(count1);
						SPAdjustmentTax.mapInvoiceNumber.push(data);
					}
					tmpOrderNumber1 = listOrderNumber[i];
					if(listInvoiceNumber[i] != ''){
						count1 = 1;
					}else{
						count1 = 0;
					}
				}
			}

			msg = SPAdjustmentTax.compareMap(SPAdjustmentTax.mapInvoiceNumber, SPAdjustmentTax.mapOrderNumber);
			
			if(msg.length == 0){
				for(var i = listInvoiceNumber.length - 1; i >= 0; i--){
					if(listInvoiceNumber[i] == null || listInvoiceNumber[i] == '' || listInvoiceNumber[i].length == 0){
						listInvoiceNumber.splice(i, 1);
						listOrderNumber.splice(i, 1);
					}else{
						if(listPayMethod[i] === 'Tiền mặt'){
							listPaymentMethod.push(1);
						}else{
							listPaymentMethod.push(2);
						}
					}
				}			
				msg = SPAdjustmentTax.checkInvoiceNumber(listInvoiceNumber);
			}
			
			if(msg.length == 0){
				if(listOrderNumber[listOrderNumber.length - 1] == null){
					msg = 'Có số hóa đơn thừa, không dành cho đơn hàng nào. Vui lòng kiểm tra lại.';
				}
			}
			
			if(msg.length>0){				
				$('#errMsg2').html(msg).show();
				return false;
			}
			
			var params = SPAdjustmentTax.getParams();				
			params.listOrderNumber = listOrderNumber;
			params.listInvoiceNumber = listInvoiceNumber;
			params.listPaymentMethod = listPaymentMethod;
			params.listSaleOrderIdx = SPAdjustmentTax._mapGridDonTong.keyArray;
			params.saleOrderIdPri = SPAdjustmentTax._donChinh;
			params.shopCode = $('#cbxShop1').combobox('getValue').trim();
			if($('#chkTypePayEX').is(':checked')){
				params.typePaymentAll = $('#sigleSelectTypePay1').val().trim();
			}			
			
			Utils.addOrSaveData(params, '/sale-product/adjust-tax/saveVATGop', null, 'errMsg2', function(data) {
				SPAdjustmentTax.showTab2();
			}, 'saveOrCancelLoading');
		},
		save: function() {
			$('.ErrorMsgStyle').html('').hide();
			var msg = '';
			if(!/^[^<|>|?|\\|\'|\"|&|~#|$|%|@|*|(|)|^|\[|\]|\{|\}|ư|ơ|Ơ|Ư|ô|Ô|ă|Ă|â|Â|`]+$/.test(SPAdjustmentTax.tmpInvoice) && SPAdjustmentTax.tmpInvoice != ''){
				msg = 'Số hóa đơn không được có ký tự đặc biệt. Vui lòng kiểm tra lại.';
			}
			if(msg.length > 0) {
				$('#errMsg').html(msg).show();
				return false;
			}
			
			checkInput = true;
			var listPaymentMethod = Array();
			
			msg = Utils.getMessageOfRequireCheck('createFromDate', 'Từ ngày');
			if(msg.length > 0) {
				$('#errorSearchMsg').html(msg).show();
				return false;
			}			

			var listOrderNumber = $('#grid').handsontable('getDataAtCol', 0);
			var listInvoiceNumber = $('#grid').handsontable('getDataAtCol', 1);
			var listPayMethod = $('#grid').handsontable('getDataAtCol', 2);
			SPAdjustmentTax.mapInvoiceNumber = new Array();
			SPAdjustmentTax.mapOrderNumber = new Array();
			
			var tmpOrderNumber = listOrderNumber[0];
			var tmpOrderNumber1 = '';
			var count = 0;
			var count1 = 0;
			
			////////////////////////////
			for(var i = listOrderNumber.length - 1; i >= 0; i--){
				if(listOrderNumber[i] == null){
					listOrderNumber.splice(i, 1);
				}else{
					break;
				}
			}
			listOrderNumber.push(null);
			
			for(var i = 0, size = listOrderNumber.length; i < size; i++){
				//tao ra mapOrder
				if(listOrderNumber[i] === tmpOrderNumber){
					count++;
				}else{
					var data = new Array();
					data.push(tmpOrderNumber);
					data.push(count);
					SPAdjustmentTax.mapOrderNumber.push(data);
					
					tmpOrderNumber = listOrderNumber[i];
					count = 1;
				}
			
				//tao ra mapInvoice
				if(tmpOrderNumber1 == '' && listInvoiceNumber[i] != ''){
					tmpOrderNumber1 = listOrderNumber[i];
				}
				if(listOrderNumber[i] === tmpOrderNumber1 && listInvoiceNumber[i] != ''){
					count1++;
				}else{
					if(count1 > 0){
						var data = new Array();
						data.push(tmpOrderNumber1);
						data.push(count1);
						SPAdjustmentTax.mapInvoiceNumber.push(data);
					}
					tmpOrderNumber1 = listOrderNumber[i];
					if(listInvoiceNumber[i] != ''){
						count1 = 1;
					}else{
						count1 = 0;
					}
				}
			}

			msg = SPAdjustmentTax.compareMap(SPAdjustmentTax.mapInvoiceNumber, SPAdjustmentTax.mapOrderNumber);
			
			if(msg.length == 0){
				for(var i = listInvoiceNumber.length - 1; i >= 0; i--){
					if(listInvoiceNumber[i] == null || listInvoiceNumber[i] == '' || listInvoiceNumber[i].length == 0){
						listInvoiceNumber.splice(i, 1);
						listOrderNumber.splice(i, 1);
					}else{
						if(listPayMethod[i] === 'Tiền mặt'){
							listPaymentMethod.push(1);
						}else{
							listPaymentMethod.push(2);
						}
					}
				}			
				msg = SPAdjustmentTax.checkInvoiceNumber(listInvoiceNumber);
			}
			
			if(msg.length == 0){
				if(listOrderNumber[listOrderNumber.length - 1] == null){
					msg = 'Có số hóa đơn thừa, không dành cho đơn hàng nào. Vui lòng kiểm tra lại.';
				}
			}
			
			if(msg.length>0){				
				$('#errMsg').html(msg).show();
				return false;
			}
			
			var params = SPAdjustmentTax.getParams();				
			params.listOrderNumber = listOrderNumber;
			params.listInvoiceNumber = listInvoiceNumber;
			params.listPaymentMethod = listPaymentMethod;
			params.shopCode = $('#cbxShop').combobox('getValue').trim();
			if($('#chkTypePay').is(':checked')){
				params.typePaymentAll = $('#sigleSelectTypePay').val().trim();
			}			
			
			Utils.addOrSaveData(params, '/sale-product/adjust-tax/save', null, 'errMsg', function(data) {
				SPAdjustmentTax.searchVATle();
			}, 'saveOrCancelLoading');
		},
		checkInvoiceNumber: function(array){
			if(array.length == 0 || array == null){
				return 'Chưa nhập số hóa đơn cho bất kì đơn nào. Vui lòng cập nhập số hóa đơn.';
			}
			if(array[0].length > 50){
				return 'Số hóa đơn nhập vào không vượt quá 50 ký tự. Vui lòng kiểm tra lại.';
			}
			
			for(var i = 0; i < array.length - 1; i++){
				if(array[i].length > 50){
					return 'Số hóa đơn nhập vào không vượt quá 50 ký tự. Vui lòng kiểm tra lại.';
				}
				if(array[i] != ''){
					//kiem tra co bi trung so hoa don nhap vao hay khong
					for(var j = i + 1; j < array.length; j++){
						if(array[j] === array[i]){
							return 'Số hóa đơn nhập vào không được trùng nhau. Vui lòng nhập số hóa đơn khác';
						}
					}
				}
			}
			return '';
		},
		//false: giong cu, true: khac
		compareMap: function(array, array1){
			for (var i = 0; i < array.length; i++) {
				for (var j = 0; j < array1.length; j++) {
					if (array[i][0] === array1[j][0]) {
						if (array[i][1] != array1[j][1]) {
							return 'Chưa cập nhật đầy đủ số hóa đơn cho đơn hàng ' + Utils.XSSEncode(array[i][0]) + '. Vui lòng kiểm tra lại';
						} else {
							break;
						}
					}
				}
			}
			return '';
		},
		saveWithInvoice: function() {
			$('#errMsg').html('').hide();
			var msg = '';
			
			var listOrderNumber = $('#grid').handsontable('getDataAtCol', 0);
			var listInvoiceNumber = $('#grid').handsontable('getDataAtCol', 1);
			
			SPAdjustmentTax.mapOrderNumber = new Array();
			var mapDeleteInvoice = new Array();
			
			var tmpListInvoiceNumber = new Array();
			var tmpListInvoiceId = new Array();
			var tmpDeleteOrderNumber = new Array();
			var listDeleteInvoiceId = new Array();
			
			for(var i = listOrderNumber.length - 1; i >= 0; i--){
				if(listOrderNumber[i] == null){
					listOrderNumber.splice(i, 1);
				}else{
					break;
				}
			}
			
			for(var i = listInvoiceNumber.length - 1; i > listOrderNumber.length - 1; i--){
				listInvoiceNumber.splice(i, 1);
			}
			//kiem tra trung so hoa don
			msg = SPAdjustmentTax.checkInvoiceNumber(listInvoiceNumber);
			
			//tao list hoa don cap nhat
			if(msg.length ==0){
				var count = 0;
				for(var i = 0, size = listInvoiceNumber.length; i < size; i++){
					if(listInvoiceNumber[i] != SPAdjustmentTax.oldInvoice[i]){
						if(listInvoiceNumber[i] != ''){
							tmpListInvoiceNumber.push(listInvoiceNumber[i]);
							tmpListInvoiceId.push(SPAdjustmentTax.listInvoiceId[i]);
						}else{
							tmpDeleteOrderNumber.push(listOrderNumber[i]);
							listDeleteInvoiceId.push(SPAdjustmentTax.listInvoiceId[i]);
						}
						
					}else{
						count++;
					}
				}
				if(count == listInvoiceNumber.length){
					msg = 'Không có sự thay đổi nào. Vui lòng thay đổi số hóa đơn và nhấn Lưu';
				}
			}
			
			if(msg.length == 0){
				if(listDeleteInvoiceId.length > 0){
					msg = 'Số hóa đơn không được để trống. Vui kiểm tra lại.';
				}
			}
			
			//kiem tra hoa don xoa
			if(msg.length == 0){
				var tmpOrderNumber = listOrderNumber[0];
				var count = 0;
				
				////////////////
				listOrderNumber.push(null);
				for(var i = 0, size = listOrderNumber.length; i < size; i++){
					//tao ra mapOrder
					if(listOrderNumber[i] === tmpOrderNumber){
						count++;
					}else{
						var data = new Array();
						data.push(tmpOrderNumber);
						data.push(count);
						SPAdjustmentTax.mapOrderNumber.push(data);
						
						tmpOrderNumber = listOrderNumber[i];
						count = 1;
						
						if(i == size){
							var data = new Array();
							data.push(tmpOrderNumber);
							data.push(count);
							SPAdjustmentTax.mapOrderNumber.push(data);
						}
					}
				}
				
				
				var tmpOrderNumber1 = tmpDeleteOrderNumber[0];
				var count1 = 0;
				////////////////////////
				tmpDeleteOrderNumber.push(null);
				for(var i = 0, size = tmpDeleteOrderNumber.length; i < size; i++){
					//tao ra mapOrder
					if(tmpDeleteOrderNumber[i] === tmpOrderNumber1){
						count1++;
					}else{
						var data = new Array();
						data.push(tmpOrderNumber1);
						data.push(count1);
						mapDeleteInvoice.push(data);
						
						tmpOrderNumber1 = tmpDeleteOrderNumber[i];
						count1 = 1;
					}
				}
				
				msg = SPAdjustmentTax.compareMap(mapDeleteInvoice, SPAdjustmentTax.mapOrderNumber);
			}
			
			if(msg.length>0){
				$('#errMsg').html(msg).show();
				return false;
			}
			
			var params = new Object();
			params.listInvoiceNumber = tmpListInvoiceNumber;
			params.listInvoiceId = tmpListInvoiceId;
			
			url = '/sale-product/adjust-tax/saveWithInvoice';
			Utils.addOrSaveData(params, url, null, 'errMsg', function(data) {
				SPAdjustmentTax.searchVATle();
			});
		},
		saveWithInvoice1: function() {
			$('#errMsg2').html('').hide();
			var msg = '';
			
			var listOrderNumber = $('#grid3').handsontable('getDataAtCol', 0);
			var listInvoiceNumber = $('#grid3').handsontable('getDataAtCol', 1);
			
			SPAdjustmentTax.mapOrderNumber = new Array();
			var mapDeleteInvoice = new Array();
			
			var tmpListInvoiceNumber = new Array();
			var tmpListInvoiceId = new Array();
			var tmpDeleteOrderNumber = new Array();
			var listDeleteInvoiceId = new Array();
			
			for(var i = listOrderNumber.length - 1; i >= 0; i--){
				if(listOrderNumber[i] == null){
					listOrderNumber.splice(i, 1);
				}else{
					break;
				}
			}
			
			for(var i = listInvoiceNumber.length - 1; i > listOrderNumber.length - 1; i--){
				listInvoiceNumber.splice(i, 1);
			}
			//kiem tra trung so hoa don
			msg = SPAdjustmentTax.checkInvoiceNumber(listInvoiceNumber);
			
			//tao list hoa don cap nhat
			if(msg.length ==0){
				var count = 0;
				for(var i = 0, size = listInvoiceNumber.length; i < size; i++){
					if(listInvoiceNumber[i] != SPAdjustmentTax.oldInvoice[i]){
						if(listInvoiceNumber[i] != ''){
							tmpListInvoiceNumber.push(listInvoiceNumber[i]);
							tmpListInvoiceId.push(SPAdjustmentTax.listInvoiceId[i]);
						}else{
							tmpDeleteOrderNumber.push(listOrderNumber[i]);
							listDeleteInvoiceId.push(SPAdjustmentTax.listInvoiceId[i]);
						}
						
					}else{
						count++;
					}
				}
				if(count == listInvoiceNumber.length){
					msg = 'Không có sự thay đổi nào. Vui lòng thay đổi số hóa đơn và nhấn Lưu';
				}
			}
			
			if(msg.length == 0){
				if(listDeleteInvoiceId.length > 0){
					msg = 'Số hóa đơn không được để trống. Vui kiểm tra lại.';
				}
			}
			
			//kiem tra hoa don xoa
			if(msg.length == 0){
				var tmpOrderNumber = listOrderNumber[0];
				var count = 0;
				
				////////////////
				listOrderNumber.push(null);
				for(var i = 0, size = listOrderNumber.length; i < size; i++){
					//tao ra mapOrder
					if(listOrderNumber[i] === tmpOrderNumber){
						count++;
					}else{
						var data = new Array();
						data.push(tmpOrderNumber);
						data.push(count);
						SPAdjustmentTax.mapOrderNumber.push(data);
						
						tmpOrderNumber = listOrderNumber[i];
						count = 1;
						
						if(i == size){
							var data = new Array();
							data.push(tmpOrderNumber);
							data.push(count);
							SPAdjustmentTax.mapOrderNumber.push(data);
						}
					}
				}
				
				
				var tmpOrderNumber1 = tmpDeleteOrderNumber[0];
				var count1 = 0;
				////////////////////////
				tmpDeleteOrderNumber.push(null);
				for(var i = 0, size = tmpDeleteOrderNumber.length; i < size; i++){
					//tao ra mapOrder
					if(tmpDeleteOrderNumber[i] === tmpOrderNumber1){
						count1++;
					}else{
						var data = new Array();
						data.push(tmpOrderNumber1);
						data.push(count1);
						mapDeleteInvoice.push(data);
						
						tmpOrderNumber1 = tmpDeleteOrderNumber[i];
						count1 = 1;
					}
				}
				
				msg = SPAdjustmentTax.compareMap(mapDeleteInvoice, SPAdjustmentTax.mapOrderNumber);
			}
			
			if(msg.length>0){
				$('#errMsg2').html(msg).show();
				return false;
			}
			
			var params = new Object();
			params.listInvoiceNumber = tmpListInvoiceNumber;
			params.listInvoiceId = tmpListInvoiceId;
			
			url = '/sale-product/adjust-tax/saveWithInvoice';
			Utils.addOrSaveData(params, url, null, 'errMsg2', function(data) {
				SPAdjustmentTax.searchVATGopDaCoHoaDon();
			},null,'#divGop ',null,null, function(data) {
				if(data.errMsg != null && data.errMsg != ''){
					$('#errMsg2').html(data.errMsg).show();
					return;
				}
			});
		},
		cancel: function() {
			var params = new Object();
			params.listInvoiceId = SPAdjustmentTax.listInvoiceId;		
			Utils.addOrSaveData(params, '/sale-product/adjust-tax/cancel', null, 'errMsg', function() {
				SPAdjustmentTax.searchVATle();
			}, 'saveOrCancelLoading');
		},
		gopHoaDon: function(){
			$('.ErrorMsgStyle').html('').hide();
			if(SPAdjustmentTax._mapGridDonTong.keyArray.length == 0){
				$('#errMsg3').html('Chưa có đơn gộp. Vui lòng chọn đơn gộp').show();
				return;
			}
			if(SPAdjustmentTax._donChinh == null){
				$('#errMsg3').html('Vui lòng chọn đơn chính').show();
				return;
			}
			var params = new Object();
			params.listSaleOrderIdx = SPAdjustmentTax._mapGridDonTong.keyArray;
			params.saleOrderIdPri = SPAdjustmentTax._donChinh;
			var $container = $("#grid3");
			var handsontable = $container.data('handsontable');
			var kData = $.param(params, true);
			$.ajax({
			    url: "/sale-product/adjust-tax/gopHoaDon",
			    dataType: 'json',
			    data : (kData),
			    type: 'GET',
			    success: function (res) {
			    	$('#divOverlay').hide();
			    	SPAdjustmentTax.cellChanges = new Array();
			    	if(res.rows.length > 0){
			    		handsontable.loadData(res.rows);
			    		SPAdjustmentTax.oldInvoice = $('#grid3').handsontable('getDataAtCol', 1);
			    		var chooseStatus = $('#chooseStatus').val();
			    		if(chooseStatus != 0){
							$("#chkTypePay").css("display", "none");
							$("#chkTypePay1").css("display", "none");
							$("#chkTypePay2").css("display", "none");
							
			    			SPAdjustmentTax.listInvoiceId = new Array();
			    			for(var i = 0; i < res.rows.length; i++){
				    			SPAdjustmentTax.listInvoiceId.push(res.rows[i].id);
				    		}
			    			if(SPAdjustmentTax.listInvoiceId[SPAdjustmentTax.listInvoiceId.length - 1] == null){
			    				SPAdjustmentTax.listInvoiceId.splice(SPAdjustmentTax.listInvoiceId.length - 1, 1);
			    			}
			    		}else{
							$("#chkTypePay").css("display", "block");
							$("#chkTypePay1").css("display", "block");
							$("#chkTypePay2").css("display", "block");
			    		}
			    	}else{
			    		handsontable.loadData(null);
			    		SPAdjustmentTax.oldInvoice = new Array();
			    	}
			    }
			 });
		},
		maxLengthValidate:function (value, callback) {
			if(value.length == 0 || value == null || value == ''){
				callback(true);
			}
		    if (value.length > 50) {
		      callback(false);
		    }else if(!isNullOrEmpty(value) && !/^[0-9a-zA-Z-_.]+$/.test(value)){//(!/^[^<|>|?|\\|\'|\"|&|~#|$|%|@|*|(|)|^|\[|\]|\{|\}|ư|ơ|Ơ|Ư|ô|Ô|ă|Ă|â|Â|`]+$/.test(value)){
		    	$('#errMsg2').html('Số hóa đơn không được có ký tự đặc biệt. Vui lòng kiểm tra lại.').show();
		    	SPAdjustmentTax.tmpInvoice = value;
		    	callback(false);
			} else {
				SPAdjustmentTax.tmpInvoice = value;
				$('.ErrorMsgStyle').html('').hide();
		      	callback(true);
		    }
		},
		boldRenderer:function (instance, td, row, col, prop, value, cellProperties) {
			  Handsontable.renderers.TextRenderer.apply(this, arguments);
			  td.style.fontWeight = 'bold';
			  //sstd.style.color = 'brown';
		},
		boldRedRenderer:function (instance, td, row, col, prop, value, cellProperties) {
			 Handsontable.renderers.TextRenderer.apply(this, arguments);
			  //td.style.fontWeight = 'bold';
			  td.style.color = 'OrangeRed';
		},
		brownRenderer:function (instance, td, row, col, prop, value, cellProperties) {
			Handsontable.renderers.TextRenderer.apply(this, arguments);
			  //td.style.fontWeight = 'bold';
			  td.style.color = 'black';
		}
};
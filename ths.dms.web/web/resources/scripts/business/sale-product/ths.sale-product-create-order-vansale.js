/**
 * Create sale order vansale
 * @author vuonghn
 * @since 03-06-2014
 */
function SaleProductObject() {
	productCode = null;
	productName = null;
	stockQuantity = null;
	lot = null;
	quantity = null;
	price = null;
	amount = null;
};
var CreateSaleOrderVansaleDB = {
	_listSaleProduct: null,
	_mapSaleProduct: null,
	_listSaleProductCode: null,
	_currentStaffSale: null,
	
	getColumnProperty: function(){
		var col = 
		[
         {type: 'autocomplete', allowInvalid: false, strict: true, source: CreateSaleOrderVansaleDB._listSaleProductCode, data: 'productCode'},
         {readOnly: true, data: 'productName'},
         {readOnly: true, data: 'stockQuantity'},
         {readOnly: true, data: 'lot'},
         {data: 'quantity'},
         {readOnly: true, type: 'numeric', data: 'price'},
         {readOnly: true, type: 'numeric', data: 'amount'},
         {readOnly: true}
         ];
		return col;
	},
	initSaleProductGrid: function(){
		$('#saleProductVanGrid').handsontable({
			data: [],
			colWidths: [150, 250, 130, 130, 130, 130, 150, 50],
			outsideClickDeselects: true,
			multiSelect : false,
			rowHeaders: true,
			manualColumnResize: true,
			width: $(window).width() - 70,
			colHeaders: ['Mã sản phẩm', 'Tên sản phẩm', 'Tồn kho', 'Chọn Lô', 'Số lượng', 'Giá bán', 'Thành tiền',' '],
			columns: CreateSaleOrderVansaleDB.getColumnProperty(),
			afterGetColHeader: function (col, TH) {
				  TH.style.fontWeight = 'bold';
				  TH.style.color = 'white';
				  TH.style.background = 'DarkCyan'; 
		    	  TH.style.verticalAlign = 'middle';
		    	  TH.style.textAlign = 'center';
			 },
		});
	},
	resetSaleProductGrid: function(){
		$('#gridSaleContainer').css('height', 300);
		var data = new Array();
		data.push(new SaleProductObject());
		$('#saleProductVanGrid').handsontable('updateSettings', {data: data ,minSpareRows: 1, columns: CreateSaleOrderVansaleDB.getColumnProperty()});
		enable('save');
	},
	disableSaleProductGrid: function(){
		$('#gridSaleContainer').css('height', 70);
		$('#saleProductVanGrid').handsontable('updateSettings', {data: [], minSpareRows: 0});
		disabled('print');
		disabled('save');
	},
	getProductStaffSale: function(params){
		var kData = $.param(params, true);
		$.ajax({
			type : "post",
			url : "/sale-product/create-vansale/get-sale-product-staff",
			data : kData,
			dataType : "json",
			success : function(data) {
				if(data.error == undefined || data.error == null || data.error == true){
					$('#GeneralInforErrors').html(data.errMsg).show();
					CreateSaleOrderVansaleDB.disableSaleProductGrid();
				}else{
					CreateSaleOrderVansaleDB._listSaleProduct = data.listSaleProduct;
					CreateSaleOrderVansaleDB._mapSaleProduct = new Map();
					CreateSaleOrderVansaleDB._listSaleProductCode = new Array();
					if(CreateSaleOrderVansaleDB._listSaleProduct != null && CreateSaleOrderVansaleDB._listSaleProduct != undefined) {
						for(var i = 0; i < CreateSaleOrderVansaleDB._listSaleProduct.length; i++) {
							CreateSaleOrderVansaleDB._mapSaleProduct.put(CreateSaleOrderVansaleDB._listSaleProduct[i].productCode, CreateSaleOrderVansaleDB._listSaleProduct[i]);
							CreateSaleOrderVansaleDB._listSaleProductCode.push(CreateSaleOrderVansaleDB._listSaleProduct[i].productCode);
						}
						CreateSaleOrderVansaleDB.resetSaleProductGrid();
					}else{
						CreateSaleOrderVansaleDB.disableSaleProductGrid();
					}
					
				}
			},
			error:function(XMLHttpRequest, textStatus, errorThrown) {
			}
		});
	}
		
};
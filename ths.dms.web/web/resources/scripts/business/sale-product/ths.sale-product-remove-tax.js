/**
 * Cập nhật thông tin thuế GTGT
 */
var SPRemoveTax = {
		listInvoiceId : new Array(),
		listPayment: new Array(),
		_firstLoad: true,
		getParams:function(){
			var params = new Object();			
			params.chooseStatus = 1;
			params.createFromDate = $('#createFromDate').val().trim();
			params.createToDate = $('#createToDate').val().trim();
			params.choosePaymentMethod = $('#choosePaymentMethod').val().trim();
			params.taxValue = $('#taxValue').val().trim();
			params.deliveryStaffCode = encodeChar($('#deliveryStaffCode').combobox('getValue').trim()); 
			params.invoiceNumber = $('#invoiceNumber').val().trim();
			params.redInvoiceNumber = $('#redInvoiceNumber').val().trim();
			params.saleStaffCode = encodeChar($('#saleStaffCode').combobox('getValue').trim());
			params.customerCode = encodeChar($('#customerCode').val().trim());
			params.isValueOrder = $('#isValueOrder').val().trim();
			params.currentShopCode = $('#cbxShop').combobox('getValue');
			return params;
		},
		getGridUrl: function(data){		
			return "/sale-product/remove-tax/search?chooseStatus=" + data.chooseStatus 
			+ "&createFromDate=" + data.createFromDate 
			+ "&createToDate=" + data.createToDate 
			+ '&choosePaymentMethod='+ data.choosePaymentMethod 
			+ '&taxValue=' + data.taxValue
			+ '&deliveryStaffCode=' + data.deliveryStaffCode
			+ '&invoiceNumber=' + data.invoiceNumber
			+ '&redInvoiceNumber=' + data.redInvoiceNumber
			+ '&saleStaffCode=' + data.saleStaffCode
			+ '&customerCode=' + data.customerCode
			+ '&isValueOrder=' + data.isValueOrder
			+ '&currentShopCode=' + data.currentShopCode
			;
		},
		search:	function() {
			var msg = '';
			if (isNullOrEmpty($('#cbxShop').combobox('getValue'))) {
				msg = 'Đơn vị không được để trống. Vui lòng nhập đơn vị';
			}			
			var fDate = $('#createFromDate').val();
			if (isNullOrEmpty(fDate)) {
				msg = 'Từ ngày không được để trống. Vui lòng nhập từ ngày';
				$('#createFromDate').focus();
			} else if (isNullOrEmpty(msg)) {
				msg = Utils.getMessageOfInvalidFormatDate('createFromDate', 'từ ngày');
			}
			var tDate = $('#createToDate').val();
			if (isNullOrEmpty(tDate)) {
				msg = 'Đến ngày không được để trống. Vui lòng nhập đến ngày';
				$('#createToDate').focus();
			} else if (isNullOrEmpty(msg)) {
				msg = Utils.getMessageOfInvalidFormatDate('createToDate', 'đến ngày');
			}
			if (!Utils.compareDate(fDate, tDate)) {
				msg = msgErr_fromdate_greater_todate;
				$('#fDate').focus();
			}
			
			if (msg.length > 0) {
				$('#errMsg').html(msg).show();
				return false;
			}
			
			var params = SPRemoveTax.getParams();
			SPRemoveTax.listInvoiceId = new Array();
			SPRemoveTax.initGrid(params);
			return false;
		},
		save: function() {
			$('#errMsg').html('').hide();
			var currentShopCode = $('#cbxShop').combobox('getValue');
			if (isNullOrEmpty(currentShopCode)) {
				$('#errMsg').html('Đơn vị không được để trống. Vui lòng nhập đơn vị.').show();
			}
			if (SPRemoveTax.listInvoiceId.length == 0) {
				$('#errMsg').html('Chưa có hóa đơn nào được chọn. Vui lòng kiểm tra lại.').show();
			} else {
				var params = new Object();
				params.listDeleteInvoiceId = SPRemoveTax.listInvoiceId;		
				params.currentShopCode = currentShopCode;
				Utils.addOrSaveData(params, '/sale-product/remove-tax/cancel', null, 'errMsg', function() {
					SPRemoveTax.search();
				}, 'saveOrCancelLoading');
			}
		},
		/**
		 * khoi tao combobox don vi
		 */
		initUnitCbx: function() {
			Utils.initUnitCbx("cbxShop", null, 206, function(data){
				SPRemoveTax.changeShop(data);
			});
		},
		/**
		 * xu ly thay doi don vi
		 */
		changeShop: function(data) {
			var shopCode = data.shopCode;
			$('#shopCode').val(shopCode);
			$('#shopId').val(data.id);
			var params = new Object();
			if (shopCode != null) {
				params.currentShopCode = shopCode;
			}
			var kData = $.param(params, true);
			$.ajax({
				type : "POST",
				url : '/sale-product/remove-tax/change-shop',
				data : (kData),
				dataType : "json",
				success : function(data) {
					if (data.createFromDate != null && data.createFromDate != undefined) {
						$('#createFromDate').val(data.createFromDate);
						$('#createToDate').val(data.createFromDate);
						$('#lockDate').text(data.createFromDate);
					}
					
					if (data.lstNVBHVo != null && data.lstNVBHVo != undefined) {
						Utils.bindStaffCbx('#saleStaffCode', data.lstNVBHVo);
					}
					if (data.lstNVGHVo != null && data.lstNVGHVo != undefined) {
						Utils.bindStaffCbx('#deliveryStaffCode', data.lstNVGHVo);
					}
					if (SPRemoveTax._firstLoad) {
						SPRemoveTax.search();
						SPRemoveTax._firstLoad = false;
					}
				}
			});

		},
		/**
		 * khoi tao gird 
		 */
		initGrid: function(params) {
			var url = SPRemoveTax.getGridUrl(params);
			$("#grid").datagrid({
				url:url,
		        rownumbers : true,
		        pageNumber : 1,
		        scrollbarSize: 0,		      
		        autoRowHeight : true,		
		        pageList  : [10,20,30,50,100,200,300,500,1000],
		        pagination:true,
		        checkOnSelect :true,
		        width: $(window).width() - 50,
				height:'auto',
				columns:[[
					{field: 'id',checkbox:true, width: 20, align: 'center',sortable:false,resizable:false},    
					{field: 'orderNumber', title: 'Hóa đơn nội bộ', width: 80, sortable:false,resizable:false, align: 'left'},
					{field: 'invoiceNumber', title: 'Hóa đơn đỏ', width: 80, sortable:false,resizable:false, align: 'left'},
					{field: 'payment', title: 'Thanh toán', width: 80, sortable:false,resizable:false, align: 'left'},
					{field: 'total', title: 'Tổng tiền', width: 80, sortable:false,resizable:false, align: 'right'},
					{field: 'taxTotal', title: 'Tiền thuế', width: 80, sortable:false,resizable:false, align: 'right'},
				    {field: 'customer', title: 'Khách hàng', width: 160, sortable:false,resizable:false, align: 'left'},
				    {field: 'custDelAddr', title: 'Địa chỉ giao hàng', width: 130, sortable:false,resizable:false, align: 'left'},
				    {field: 'customerAddr', title: 'Địa chỉ khách hàng', width: 200, sortable:false,resizable:false, align: 'left'},
				    {field: 'staffInfo', title: 'NVBH', width: 100, sortable:false,resizable:false, align: 'left'},
				    {field: 'deliveryStaffInfo', title: 'NVGH', width: 100, sortable:false,resizable:false, align: 'left'},
				    {field: 'date', title: 'Ngày', width: 90, sortable:false,resizable:false, align: 'center'},				    
				    {field: 'stringVat', title: 'Thuế suất', width: 50, sortable:false,resizable:false, align: 'right'}
				]],
				onCheck : function(rowIndex, rowData) {
					var selectedId = rowData['id'];
					SPRemoveTax.listInvoiceId.push(selectedId);
				},
				onUncheck : function(rowIndex, rowData) {
					var selectedId = rowData['id'];
					var i = SPRemoveTax.listInvoiceId.indexOf(selectedId);
					if(i != -1) {
						SPRemoveTax.listInvoiceId.splice(i, 1);
					}
					
				},
				onCheckAll : function(rows) {
					if($.isArray(rows)) {
						for(var i = 0; i < rows.length; i++) {
							var row = rows[i];
							var selectedId = row['id'];
							SPRemoveTax.listInvoiceId.push(selectedId);
				    	}
				    }
				},
				onUncheckAll : function(rows) {
					SPRemoveTax.listInvoiceId = new Array();
				},
				onLoadSuccess:function(){
					$('#gridContainer .datagrid-header-rownumber').html('STT');
					$('#grid').datagrid('resize');
				}
			});
		}
};
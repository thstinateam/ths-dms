/**
 * Xác nhận đơn hàng
 */
var SPConfirmOrder = {
	_lstOrder:new Map(),//lst don hang xac nhan
	_lstPay:new Map(),//lst don hang thanh toan
	_lstOrderChangePrice:new Map(),
	_lstOrderSkipDebit:new Map(),
	_idApply:null,
	_idDebit:null,
	_mapOrderErr: null,
	_isLoadGridDetail:1,
	_currentShopCode : '',
	_ShowPrice:{
		isShowPrice: 1, // cau hinh hien gia
		notShowPrice:2 // cau hinh khong hien gia
	},
	_isAllowShowPrice : true,
	_isAllowModifyPrice : false,
	_isFirstTimeLoading : true,
	ORDER_CONFIRM_URL: '/sale-product/confirm-order/info',
	/**
	 * Ẩn tất cả các tab
	 * @author tungmt
	 * @since 29/08/2014
	 */
	hideAllTab:function(){
		$('#orderTab').removeClass('Active');
		$('#payTab').removeClass('Active');
		$('#orderTabContainer').hide();
		$('#payTabContainer').hide();
	},
	/**
	 * Ẩn tất cả các message
	 * @author tungmt
	 * @since 29/08/2014
	 */
	hideAllMsg:function(){
		$(".ErrorMsgStyle").hide();
		$(".SuccessMsgStyle").hide();
		$('#errMsgOrder').html('').hide();
		$('#errMsgPay').html('').hide();
		$('#successMsgOrder').html('').hide();
		$('#successMsgPay').html('').hide();
	},
	/**
	 * Hiện tất cả các tab
	 * @author tungmt
	 * @since 29/08/2014
	 */
	showTab:function(type){
		SPConfirmOrder.hideAllTab();
		if(type!=undefined && type!=null && type=='DONHANG'){
			$('#orderTab').addClass('Active');
			$('#orderTabContainer').show();
			SPConfirmOrder.searchOrder();
		}else {
			SPConfirmOrder.showPayConfirm();
		}
	},	
	showPayConfirm: function(){
		$('#payTab').addClass('Active');
		$('#payTabContainer').show();
		var params = {};
		$.ajax({
			type : "POST",
			url:'/commons/list-child-shop',
			data :($.param(params, true)),
			dataType : "json",
			success : function(data) {				
				$('#shopCode').combobox({
					valueField: 'shopCode',
					textField:  'shopName',
					data: data.rows,
					width: 200,
//					panelWidth: '200',
					formatter: function(row) {
						return '<span style="font-weight:bold">' + Utils.XSSEncode(row.shopCode) + '</span><br/>' + '<span style="color:#888">' + Utils.XSSEncode(row.shopName) + '</span>';
					},
					filter: function(q, row){
						q = new String(q).toUpperCase().trim();
						var opts = $(this).combobox('options');
						return ((unicodeToEnglish(row[opts.textField]).indexOf(unicodeToEnglish(q)) == 0) || unicodeToEnglish(row[opts.valueField]).indexOf(unicodeToEnglish(q)) == 0) ;
					},	
			        onSelect: function(rec){
			        	SPConfirmOrder.loadSaleStaffCbx();
			        },
			        onLoadSuccess: function(){
			        	var arr = $('#shopCode').combobox('getData');
			        	if (arr != null && arr.length > 0){
			        		$('#shopCode').combobox('select', arr[0].shopCode);
//			        		$('#shopCode').combobox('setValue', arr[0].shopCode);
			        	}
			        	var time = setTimeout(function(){
			        		SPConfirmOrder.searchPay();
			        		}, 1000);
			        	
			        }
				});
			}
		});
		
	},
	
	/**
	 * load combobox don vi (thanh toan)
	 * @author trietptm
	 * @since Feb 15, 2016
	 */
	initUnitCbxPay: function() {
		var params = {};
		$.ajax({
			type : "POST",
			url:'/commons/list-child-shop',
			data :($.param(params, true)),
			dataType : "json",
			success : function(data) {				
				$('#shopCode').combobox({
					valueField: 'shopCode',
					textField: 'shopName',
					data: data.rows,
					width: 200,
//					panelWidth: '200',
					formatter: function(row) {
						return '<span style="font-weight:bold">' + Utils.XSSEncode(row.shopCode) + '</span><br/>' + '<span style="color:#888">' + Utils.XSSEncode(row.shopName) + '</span>';
					},
					filter: function(q, row) {
						q = new String(q).toUpperCase().trim();
						var opts = $(this).combobox('options');
						return ((unicodeToEnglish(row[opts.textField]).indexOf(unicodeToEnglish(q)) == 0) || unicodeToEnglish(row[opts.valueField]).indexOf(unicodeToEnglish(q)) == 0) ;
					},	
			        onSelect: function(rec) {
			        	SPConfirmOrder.loadSaleStaffCbx();
			        },
			        onLoadSuccess: function() {
			        	var arr = $('#shopCode').combobox('getData');
			        	if (arr != null && arr.length > 0){
			        		$('#shopCode').combobox('select', arr[0].shopCode);
//			        		$('#shopCode').combobox('setValue', arr[0].shopCode);
			        	}
		        		SPConfirmOrder.searchPay();
			        }
				});
			}
		});
	},
	
	/**
	 * Hiện cảnh báo cho đơn hàng
	 * @author tungmt
	 * @since 29/08/2014
	 */
	showWarning: function(id) {
		var order = SPConfirmOrder._lstOrder.get(id);
		if (order != null) {
			$.messager.alert('Thông báo', Utils.XSSEncode(order.message).replace(/\n/g,"</br>"), 'warning').window({  
			    width:600
			}).window('hcenter');
			$('.messager-button .l-btn-text').html('Đóng');
		}
	},
	/**
	 * Sự kiện checkbox tab đơn hàng
	 * @author tungmt
	 * @since 29/08/2014
	 */
	cbOrderClick:function(t,id){
		if (id != undefined && id != null) { // lacnv1 -- khong cho check don vansale cua nhan vien chua chot ngay
			var order=SPConfirmOrder._lstOrder.get(id);
			if($(t).is(':checked')){
				order.check=1;
				if ($(".cbOrder:checked").length == $(".cbOrder").length) {
					$('.cbOrderAll').attr('checked', 'checked');
				}
			}else{
				order.check=0;
				$('.cbOrderAll').removeAttr('checked');
			}
		} else {
			$(t).removeAttr("checked");
		}
	},
	/**
	 * Sự kiện checkall tab đơn hàng
	 * @author tungmt
	 * @since 29/08/2014
	 */
	cbOrderClickAll:function(t){
		var check=0;
		if($(t).is(':checked')){
			check = 1;
		}
		var order = null;
		var lst = SPConfirmOrder._lstOrder.valArray;
		for(var i=0,n=lst.length ; i<n ; i++){
			order = lst[i];
//			if (order.lockedStock == 1) { // lacnv1 -- khong cho check don vansale cua nhan vien chua chot ngay
			order.check = check;
//			}
		}
		if (check == 1) {
			$('.cbOrder[value!=-1]').attr('checked', true);
		}else{
			$('.cbOrder').attr('checked', false);
		}
	},
	/**
	 * Sự kiện checkbox tab thanh toán
	 * @author tungmt
	 * @since 29/08/2014
	 */
	cbPayClick: function(t, id) {
		var order = SPConfirmOrder._lstPay.get(id);
		if (order != null) {
			if ($(t).is(':checked')) {
				order.check=1;
				//$('.cbPay'+id).attr('checked',true);
				if ($(".cbPay:checked").length == $(".cbPay").length) {
					$('.cbPayAll').attr('checked', 'checked');
				}
			} else {
				order.check=0;
				//$('.cbPayAll').attr('checked',false);
				$('.cbPayAll').removeAttr('checked');
				//$('.cbPay'+id).attr('checked',false);
			}
		}
	},
	/**
	 * Sự kiện checkall tab thanh toán
	 * @author tungmt
	 * @since 29/08/2014
	 */
	cbPayClickAll: function(t) {
		var check = 0;
		if ($(t).is(':checked')) {
			check = 1;
		}
		for (var i = 0, n = SPConfirmOrder._lstPay.valArray.length ; i < n ; i++) {
			SPConfirmOrder._lstPay.valArray[i].check = check;
		}
		if (check == 1) {
			$('.cbPay').attr('checked', true);
		} else {
			$('.cbPay').attr('checked', false);
		}
	},
	bindComboboxStaffEasyUI:function(objectId,prefix){
		var parent = '';
		if(prefix!=undefined && prefix!=null){
			parent = prefix;
		}			
		$(parent + '#'+objectId).combobox({			
			valueField : 'staffCode',
			textField : 'staffCode',
			data : Utils.getDataComboboxStaff(objectId,prefix),
			//width:250,
			panelWidth:206,
			formatter: function(row) {
				return '<span style="font-weight:bold">' + Utils.XSSEncode(row.staffCode) + '</span><br/>' + '<span style="color:#888">' + Utils.XSSEncode(row.staffName) + '</span>';
			},
			filter: function(q, row){
				q = new String(q).toUpperCase().trim();
				var opts = $(this).combobox('options');
				return row['searchText'].indexOf(unicodeToEnglish(q))>=0;
			},			
			onChange:function(newvalue,oldvalue){		    	
				//$('.combo-panel').each(function(){if(!$(this).is(':hidden')){$(this).parent().css('width','250px');$(this).css('width','248px');}});
			}
		});
		$('.combo-arrow').unbind('click');
		//$('.combo-arrow').live('click',function(){$('.combo-panel').each(function(){if(!$(this).is(':hidden')){$(this).parent().css('width','250px');$(this).css('width','248px');}});});
	},
	/**
	 * Hiện thông tin khách hàng
	 * @author tungmt
	 * @since 29/08/2014
	 */
	showCustomerInfor:function(customerId,shopCode){
		$('#salesCustomerDialog').css("visibility", "visible");
		var html = $('#salesCustomerDialog').html();
		var data = new Object();
		data.customerId = customerId;
		data.typeReturn = 'success';
		data.shopCode = SPConfirmOrder._currentShopCode;
		Utils.getHtmlDataByAjax(data, "/commons/customer/get-info", function(data){
			$("#salesCustomerDialogUIDialog").html(data);
			$('#salesCustomerDialogUIDialog .ValueStyle').css('font-weight','bold');
			var showGia = ($("#showGia").val() == "1");
			$('#salesCustomerDialogUIDialog').dialog({  	       
		        closed: false,  
		        cache: false,  
		        modal: true,
		        width : 650,
//		        height :700,
		        onOpen: function(){
		        	$('#salesCustomerDialog').html('');
		        	SPSearchSale.showDialogZIndex('salesCustomerDialogUIDialog');
		        	$('.easyui-dialog #gridCustomerInfor').datagrid({
		    			url : "/commons/customer/get-sale-order",
		    			view: bufferview,	
		    		    singleSelect:true,		   
		    		    width: 600,
		    		    height: 200,
		    		    rownumbers : true,
		    		    autoRowHeight:false,
		    		    pageSize:50,  
		    		    fitColumns:true,
		    		    autoWidth: true,		    		      			
		    			queryParams:{
		    				customerId:customerId,
		    				shopCode:SPConfirmOrder._currentShopCode
		    			},		    			                  
		    		    columns:[[	        
		    			    {field:'orderDate', title: sale_product_customer_ngay, width: 100, sortable:false,resizable:true , align: 'left',
		    			    	formatter:function(value,row,index){
		    			    		if(row.orderDate==null) return '';
		    			    		var d =  new Date(row.orderDate);
		    						var hh = d.getHours()<10? '0'+d.getHours():d.getHours();
		    						var mm = d.getMinutes()<10? '0'+d.getMinutes():d.getMinutes();
		    						var ss = d.getSeconds()<10? '0'+d.getSeconds():d.getSeconds();
		    						return $.datepicker.formatDate('dd/mm/yy', d) + ' ' +hh + ':' + mm + ':'+ ss;		    						
		    			    	}},
		    			    {field:'orderNumber', title: sale_product_customer_so_don_hang, width:120,sortable:false,resizable:true , align: 'left'},
		    			    {field:'amount', title: sale_product_customer_thanh_tien, width: 100, align: 'right', sortable:false,resizable:true,formatter:CommonFormatter.numberFormatter},
		    			    {field:'quantity', title: sale_product_customer_sl, width: 50, align: 'right', sortable:false,resizable:true,formatter:CommonFormatter.numberFormatter}
		    		    ]],	    
		    		    onLoadSuccess :function(data){
		    		    	var isShowPrice = null;
		    		    	if (data.rows.length > 0) {
		    		    		isShowPrice = data.rows[0].isShowPrice;
		    		    	}
		    		    	if (isShowPrice != undefined && isShowPrice != null && isShowPrice == SPConfirmOrder._ShowPrice.isShowPrice) {
		    		    		$('#gridCustomerInfor').datagrid('showColumn', 'amount');
//		    		    		 $('#gridCustomerInfor').datagrid('showColumn', 'quantity');
		    		    	} else {
		    		    		$('#gridCustomerInfor').datagrid('hideColumn', 'amount');
//		    		    		 $('#gridCustomerInfor').datagrid('hideColumn', 'quantity');
		    		    	}
		    		    	$('.datagrid-header-rownumber').html(sale_product_customer_stt);	  
		    		    	$('.datagrid-header-row td div').css('text-align','center');
		    		    	updateRownumWidthForJqGrid('.easyui-dialog');
		    		    	setTimeout(function(){
								CommonSearchEasyUI.fitEasyDialog("salesCustomerDialogUIDialog");
							}, 100);
//		    		    	$(window).resize();
		    		    }
		    		});
		        	$("#btnCloseDlg").removeAttr("onclick");
		        	$("#btnCloseDlg").attr("onclick", "$('#salesCustomerDialogUIDialog').dialog('close');");
		        },
		        onClose : function(){
		        	$('#salesCustomerDialog').html(html);
		        	$('#salesCustomerDialog').css("visibility", "hidden");	        	
		        }
		     });
		});
	},
	/**
	 * Tìm kiếm đơn hàng
	 * @author tungmt
	 * @since 29/08/2014
	 */
	searchOrder:function(showErrorMsg){
		// SPConfirmOrder.loadGridOrder();
		$('#panelDetail').hide();
		if (showErrorMsg != 1) {
			SPConfirmOrder.hideAllMsg();
		}
		var params=new Object();
		var nvbhId = $('#nvbhId').combobox('getValue');
		if(nvbhId != -1){
			params.searchStaffCode= nvbhId;
		}
		params.orderType=$('#orderType').val();
		try {
			params.deliveryStaff = $('#deliveryStaff').combobox('getValue');
		} catch(e) {
			// pass through
		}

		$("#description").val("");
		
		params.priorityCode = $('#priorityId').val();
		params.customerCode = $('#customerCode').val().trim();
		params.customerName = $('#customerName').val().trim();
		
		//Thêm dk gia trị
		params.isValueOrder = $('#isValueOrder').val().trim();
		var numberOrder = $('#numberValueOrder').val().trim();
		var numberValueOrder = "";
		if ( $('#numberValueOrder').val() != undefined &&  $('#numberValueOrder').val() != null &&  $('#numberValueOrder').val() !='') {
			numberValueOrder = numberOrder.replace(/,/g, '');
		}
		params.numberValueOrder = numberValueOrder;
		params.currentShopCode = $("#shopCodeCB").combobox('getValue');
		SPConfirmOrder._lstOrder=new Map();
		$(".cbOrderAll").removeAttr("checked");
		$("#gridOrder").datagrid('load', params);
	},
	/**
	 * Tìm kiếm chi tiết đơn hàng
	 * @author tungmt
	 * @since 29/08/2014
	 */
	searchOrderDetail:function(id){
		$('#panelDetail').show();
		SPConfirmOrder.hideAllMsg();
		var params=new Object();
		params.saleOrderId=id;
		params.currentShopCode = $("#shopCodeCB").combobox('getValue');
		var order=SPConfirmOrder._lstOrder.get(id);
		$('#orderNumberDetail').html(Utils.XSSEncode(order.orderNumber));
		if(SPConfirmOrder._isLoadGridDetail != 1){
			$("#gridDetail").datagrid('load', params);
		}else{
			SPConfirmOrder._isLoadGridDetail = 0;
			SPConfirmOrder.loadGridDetail(params);
		}
	},
	/**
	 * Tìm kiếm thanh toán
	 * @author tungmt
	 * @since 29/08/2014
	 */
	searchPay:function(){
		SPConfirmOrder.hideAllMsg();
		var par = {}
    	var shopCode  = $('#shopCode').combobox('getValue');
    	var staffCode = $('#nvbhIdPay').combobox('getValue');
    	if(!Utils.isEmpty(staffCode)){
			par.staffCode = staffCode;
		}
		par.currentShopCode = shopCode;
		$("#description").val("");
		SPConfirmOrder._lstPay=new Map();
		par.page=1;
		$(".cbPayAll").removeAttr("checked");
		SPConfirmOrder.loadGridPay(par,shopCode);
		
	},
	loadSaleStaffCbx: function(){
		var par = new Object() ;
		par.currentShopCode = $('#shopCode').combobox('getValue');
		$.ajax({
			type : "POST",
			url: '/sale-product/confirm-payment/load-sell-staff-by-shop-code',
			data :($.param(par, true)),
			dataType : "json",
			success : function(data) {
				if (data != undefined && data != null && data.lstNVBHVo != null){
					Utils.bindStaffCbx('#nvbhIdPay', data.lstNVBHVo, null, 200);
				}
			}
		});
	},
	
	/**
	 * Hiển thị thêm thông tin các cột được rút gọn
	 * @author tungmt
	 * @since 29/08/2014
	 */
	showInfoPay:function(t,html,type){
		if(type!=undefined && type!=null && type=='CUSTOMER'){
			var str=html.split(':xuongdong');
			html='';
			for(var i=0;i<str.length;i++){
				var s=str[i].split('.-');//lay Id customer
	    		html+='<a class="cmsdefault" href="javascript:void(0);" onclick="SPConfirmOrder.showCustomerInfor('+s[0]+')">'+s[1]+'</a></br>';
			}
		}
		$(t).parent().html(html.replace(/:xuongdong/g,"</br>"));
	},
	initUnitCbx: function(){				
		var params = {};
		$.ajax({
			type : "POST",
			url:'/commons/list-child-shop',
			data :($.param(params, true)),
			dataType : "json",
			success : function(data) {
				listNVBH = data.lstNVBH;
				$('#shopCodeCB').combobox({
					valueField: 'shopCode',
					textField:  'shopName',
					data: data.rows,
					panelWidth: '206',
					formatter: function(row) {
						return '<span style="font-weight:bold">' + Utils.XSSEncode(row.shopCode) + '</span><br/>' + '<span style="color:#888">' + Utils.XSSEncode(row.shopName) + '</span>';
					},
					filter: function(q, row){
						q = new String(q).toUpperCase().trim();
						var opts = $(this).combobox('options');
						return ((unicodeToEnglish(row[opts.textField]).indexOf(unicodeToEnglish(q)) == 0) || unicodeToEnglish(row[opts.valueField]).indexOf(unicodeToEnglish(q)) == 0) ;
					},	
			        onSelect: function(param){
			            // SPCreateOrder.resetOrder();
			            SPConfirmOrder.getStaffByShopCode();
			            SPConfirmOrder.getLockDate();
			        },
			        onLoadSuccess: function(){
			        	var arr = $('#shopCodeCB').combobox('getData');
			        	if (arr != null && arr.length > 0){
			        		$('#shopCodeCB').combobox('select', arr[0].shopCode);
			        		SPConfirmOrder.getLockDate();
			        	}
			        	
			        }
				});
			}
		});
	},
	/*
	 * tungmt
	 * 12/08/2015
	 * Lấy ngày làm việc của NPP
	 */
	getLockDate:function() {
		var params = new Object();
	   	params.shopCode = $('#shopCodeCB').combobox('getValue');
	   	Utils.getJSONDataByAjax(params, '/catalog/utility/closing-day/check', function(data) {
	   		if (data.appDate != undefined && data.appDate != null) {
	   			$('#lockDay').html(data.appDate);
	   		}
	   	});
	},
	resetSearchDetail: function() {
		$("#customerCode").val("");
		$("#customerName").val("");
		$('#gridOrder').datagrid('loadData', {"total":0,"rows":[]});
		try {
			$('#gridDetail').datagrid('loadData', {"total":0,"rows":[]});
		} catch (e) {
			// TODO: handle exception
		}
		$('#panelDetail').hide();
		if (SPConfirmOrder._isAllowShowPrice) {
			$("#gtDH").show();
			$("#devValueOrder").show();
			$("#numberValueOrder").show();
		} else {
			$("#gtDH").hide();
			$("#devValueOrder").hide();
			$("#numberValueOrder").hide();
		}
	},

	/**
	*lay danh sach nhan vien theo shop don vi
	*/
	getStaffByShopCode: function(shopCodeFilter) {
		 var shopCodeFilter = $('#shopCodeCB').combobox('getValue');
		 SPConfirmOrder._currentShopCode = shopCodeFilter;
		 var params = new Object();
		if(shopCodeFilter != null){
			params.shopCodeFilter = shopCodeFilter;
		}
		// SPCreateOrder.loadProductForSale(shopCodeFilter);
		var kData = $.param(params, true);
		 $.ajax({
			type : "POST",
			url : '/sale-product/create-order/load-staff-by-shop-code',
			data : (kData),
			dataType : "json",
			success : function(data) {
				SPConfirmOrder._isAllowShowPrice = data.isAllowShowPrice;
				SPConfirmOrder._isAllowModifyPrice = data.isAllowModifyPrice;
				SPConfirmOrder.loadGridOrder();
				SPConfirmOrder.resetSearchDetail();
				if (SPConfirmOrder._isFirstTimeLoading) {
					SPConfirmOrder.searchOrder();
				}
				SPConfirmOrder._isFirstTimeLoading = false;			
				var lstNVBHVo = data.lstNVBHVo;
				var lstNVTTVo = data.lstNVTTVo;
				var lstNVGHVo = data.lstNVGHVo;
				if(lstNVBHVo != undefined && lstNVBHVo != null) {
					Utils.bindStaffCbx('#nvbhId', lstNVBHVo);
				 }
				if(lstNVGHVo != undefined && lstNVGHVo != null) {
					Utils.bindStaffCbx('#deliveryStaff', lstNVGHVo);
				 }
			},
			error:function(XMLHttpRequest, textStatus, errorThrown) {
				console.log("Error");
			}
		});

	},
	/**
	 * Load dataGrid đơn hàng
	 * @author tungmt
	 * @since 29/08/2014
	 */
	loadGridOrder:function(params){
		var pr = {};
		if (params != undefined && params != null){
			pr = params;
		}
		var firstLoad = true;
		$('#gridOrder').datagrid({  
	 	    url:'/sale-product/confirm-order/searchOrder',
		    checkOnSelect : false,
			pageSize: 10,
	 		autoWidth: true,
			pageNumber:1,
		    selectOnCheck:false,
		    singleSelect:true,
		    scrollbarSize:0,
		    autoRowHeight : true,
		    rownumbers:true,
		    height: "auto",
		    width: $('#dgGridOrderContainer').width()-25,
		    pagination: true,
		    pageList: [5, 10, 20, 30, 50, 100, 200, 500],
		    queryParams: pr, 
		    frozenColumns: [[
				{field:'checkbox', title:'<input class="cbOrderAll" type="checkbox" onclick="SPConfirmOrder.cbOrderClickAll(this)" />', width:35, align: 'center', formatter : function(v,r,i) {
					var order = r;
					if (order.check != undefined && order.check != null && order.check == 1) {
						return '<input value="' + Utils.XSSEncode(r.id) + '" checked="checked" class="cbOrder" type="checkbox" onclick="SPConfirmOrder.cbOrderClick(this,' + Utils.XSSEncode(r.id) + ')" />';
					}
					return '<input value="' + Utils.XSSEncode(r.id) + '" class="cbOrder" type="checkbox" onclick="SPConfirmOrder.cbOrderClick(this,' + Utils.XSSEncode(r.id) + ')" />';
				}},
				{field:'detail', title:'Chi tiết', width:80, align: 'left', formatter : function(v,r,i) {
					var str='<a id="aDetail" title="Xem chi tiết" href="javascript:void(0)" onclick= "SPConfirmOrder.searchOrderDetail('+r.id+');"><img src="/resources/images/icon-view.png" height="17"/></a>';
					if(r.message!=null && r.message.trim()!=''){
						str+='<a id="aWarning" title="Cảnh báo" href="javascript:void(0)" onclick="SPConfirmOrder.showWarning(\''+Utils.XSSEncode(r.id)+'\');" style="margin-left: 6px;"><img src="/resources/images/icon-error.png" height="17"/></a>';
					}
					return str;
				}},
				{field:'refOrderNumber', title:confirm_order_so_ref, width:120, align: 'left', formatter : function(v,r,i) {
		        	return Utils.XSSEncode(v);
		        }},
		        {field:'staff', title:confirm_order_nvbh, width:200, align: 'left', formatter : function(v,r,i) {
		        	if(r.staffCode!=null){
		        		return Utils.XSSEncode(r.staffCode+' - '+r.staffName);
		        	}
		        	return '';
		        }},
		        {field:'customer', title:confirm_order_khach_hang, width:140, align: 'left', formatter : function(v,r,i) {
		        	if(r.customerId!=null){
		        		return '<a class="cmsdefault" href="javascript:void(0);" onclick="return SPConfirmOrder.showCustomerInfor('+Utils.XSSEncode(r.customerId)+')">'+Utils.XSSEncode(r.customerCode+' - '+r.customerName)+'</a>';
		        	}
		        	return '';
		        }},
		        {field:'totalValue', title:confirm_order_tong_tien, width:105, hidden:!SPConfirmOrder._isAllowShowPrice, align: 'right', formatter : function(v,r,i) {
		        	if(v!=undefined && v!=null && v!=''){
		        		return formatCurrency(Utils.XSSEncode(v));
		        	}else{
		        		return '0';
		        	}
		        }},
		    ]],
			columns:[[
		        {field:'saleOrderDiscount', title:'CK đơn hàng', width:105, hidden:!SPConfirmOrder._isAllowShowPrice, align: 'right', formatter : function(v,r,i) {
		        	if(v!=undefined && v!=null && v!=''){
		        		return formatCurrency(Utils.XSSEncode(v));
		        	}else{
		        		return '0';
		        	}
		        }},
		        {field:'totalDiscount', title:'Tổng tiền KM', width:105, hidden:!SPConfirmOrder._isAllowShowPrice, align: 'right', formatter : function(v,r,i) {
		        	if(v!=undefined && v!=null && v!=''){
		        		return formatCurrency(Utils.XSSEncode(v));
		        	}else{
		        		return '0';
		        	}
		        }},
				{field:'orderDate', title:'Ngày tạo', width:130, align: 'center', formatter :function(v,r,i) {
					if(v!=undefined && v!=null){
						var dateTimeStr = Utils.XSSEncode(v);
						var timePortionHhMiStr = '';
						try {
							var timePortionStr = dateTimeStr.split('T')[1];
							timePortionHhMiStr = timePortionStr;
						} catch (e) {
						}
						return $.datepicker.formatDate('dd/mm/yy', new Date(dateTimeStr)) + (timePortionHhMiStr ? ' ' + timePortionHhMiStr : '');
					}
					return '';
				}},
				{field:'deliveryDate', title:'Ngày giao', width:80, align: 'center', formatter : function(v,r,i) {
					if(v!=undefined && v!=null){
						return $.datepicker.formatDate('dd/mm/yy', new Date(Utils.XSSEncode(v)));
					}
					return '';
				}},
				{field:'priorityStr', title:'Độ ưu tiên', width:100, align: 'left', formatter : function(v,r,i) {
					return Utils.XSSEncode(v);
				}},
		        {field:'address', title:'Địa chỉ', width:250, align: 'left', formatter : function(v,r,i) {
		        	if(r.address!=null){
		        		return Utils.XSSEncode(r.address);
		        	}
		        	return '';
		        }},
		        {field:'deliveryCode', title:'NVGH', width:200, align: 'left', formatter : function(v,r,i) {
		        	if(v){
		        		return Utils.XSSEncode(r.deliveryCode+' - '+r.deliveryName);
		        	}
		        	return '';
		        }}
		    ]],
		    onBeforeLoad: function(param) {
		    	if (firstLoad) {
		    		firstLoad = false;
		    		return false;
		    	}
		    },
		    onLoadSuccess :function(data){
		    	$('.datagrid-header-rownumber').html('STT');
			   	$('.datagrid-header-row td div').css('text-align','center');
			   	updateRownumWidthForJqGrid('.easyui-dialog');
		   	 	$(window).resize();
		   	 	SPConfirmOrder._lstOrder = new Map();
		   	 	for(var i=0;i<data.rows.length;i++){
					data.rows[i].stt=i+1;
					SPConfirmOrder._lstOrder.put(data.rows[i].id, data.rows[i]);
				}
				$('.cbOrderAll').removeAttr('checked');
		    },
		    rowStyler: function(index,r){
				if (r.message!=null && r.message.trim()!=''){
					return 'color:red;'; // return inline style
				}
			}
		});
	},
	initPageControl: function() {
		var shopId = $('#shopId').val();
		$('#customerCode').bind('keyup', function(event) {					
			if (event.keyCode == keyCode_F9) {
				VCommonJS.showDialogSearch2({
				    inputs : [
				        {id:'code', maxlength:50, label:'Mã KH'},
				        {id:'name', maxlength:250, label:'Tên KH'},
				        {id:'address', maxlength:250, label:'Địa chỉ', width:410}
				    ],
				    params:{shopCode:$('#shopCodeCB').combobox('getValue')},
				    url : '/commons/customer-in-shop/search',
				    onShow : function() {
				    	var tabindex = -1;
			        	$('.InputTextStyle, select, input, button , li , tr, td, th, label, ul, a, img').each(function () {
							if (this.type != 'hidden') {
								$(this).attr("tabindex", tabindex);
								tabindex -=1;
							}
						});	        		        	
			        	$('.easyui-dialog #seachStyle1Code').focus();
						$('.easyui-dialog #seachStyle1AddressLabel').show();
						$('.easyui-dialog #seachStyle1Address').show();
						$('.easyui-dialog #searchStyle1Grid').show();
						$('.easyui-dialog #btnSearchStyle1').css('margin-left', 270);
						$('.easyui-dialog #btnSearchStyle1').css('margin-top', 5);
						$('.easyui-dialog #seachStyle1CodeLabel').html('Mã KH');
						$('.easyui-dialog #seachStyle1NameLabel').html('Tên KH');
						
						Utils.bindAutoSearch();
						$('.easyui-dialog #btnSearchStyle1').bind('click',function(event) {
							if(!$(this).is(':hidden')){
								var code = $('.easyui-dialog #seachStyle1Code').val().trim();
								var name = $('.easyui-dialog #seachStyle1Name').val().trim();
								var address = $('.easyui-dialog #seachStyle1Address').val().trim();
								$('.easyui-dialog #searchStyle1Grid').datagrid('load',{code :code, name: name,address:address});
								var records = $('.easyui-dialog #searchStyle1Grid').datagrid('getRows').length;
								$('.easyui-dialog #seachStyle1Code').focus();
								if (records != null && records == 1 && !isNullOrEmpty(code)) {
									$('#custID').val($('.easyui-dialog #searchStyle1Grid').datagrid('getRows')[0].shortCode);
									$('#custName').val($('.easyui-dialog #searchStyle1Grid').datagrid('getRows')[0].customerName);
									$('#searchStyle1EasyUIDialogDiv').css("visibility", "hidden");					
									$('.easyui-dialog').dialog('close');
									$('#custID').change();
								}
							}					
						});
				    },
				    columns : [[
				        {field:'shortCode', title:'Mã KH', align:'left', width: 110, sortable:false, resizable:false, formatter : function(v,r,i) {
				        	return Utils.XSSEncode(v);
				        }},
				        {field:'customerName', title:'Tên KH', align:'left', width: 150, sortable:false, resizable:false, formatter : function(v,r,i) {
				        	return Utils.XSSEncode(v);
				        }},
				        {field:'address', title:'Địa chỉ', align:'left', width: 170, sortable:false, resizable:false, formatter : function(v,r,i) {
				        	return Utils.XSSEncode(v);
				        }},
				        {field:'choose', title:'', align:'center', width:40, sortable:false, resizable:false, formatter:function(value,row,index) {
				        	return "<a href='javascript:void(0)' onclick=\"return SPCreateOrder.getResultSeachStyle1EasyUIDialog("+ index + ");\">Chọn</a>";        
				        }}
				    ]]
				});
			} 
		});
		Utils.bindAutoSearch();
		Utils.bindComboboxStaffEasyUI('deliveryStaff');
	},
	/**
	 * Load dataGrid chi tiết đơn hàng
	 * @author tungmt
	 * @since 29/08/2014
	 */
	loadGridDetail:function(params) {
		$('#gridDetail').datagrid({  
		    url:'/sale-product/confirm-order/searchDetail',
		    selectOnCheck: false,
		    rownumbers: true,
		    scrollbarSize: 0,
		    autoRowHeight : true,
		    fitColumns : true,
			checkOnSelect : true,
			singleSelect: true,
			showFooter: true,
		    width: $('#dgGridDetailContainer').width(),
		    queryParams: params, 
		    columns:[[
		        {field:'productCode', title:'Mã hàng', width:60, align: 'left', formatter : function(v,r,i) {
		        	v=Utils.XSSEncode(v);
		        	if (r.staffCode == undefined || r.staffCode == null) {
		        		r.staffCode='';
		        	}
		        	if (r.productId != null) {
		        		return '<a class="cmsdefault" href="javascript:void(0);" onclick="return SPConfirmOrder.viewProductDetails(\'' + v + '\',\'' + Utils.XSSEncode(r.customerCode) + '\',' + Utils.XSSEncode(r.convfact) + ',\'' + Utils.XSSEncode(r.staffCode) + '\')">' + v + '</a>';
		        	}
		        	return v;
		        }},
		        {field:'productName', title:'Tên hàng', width:170, align: 'left', formatter:function(v,r,i) {
		        	return Utils.XSSEncode(v);
		        }},
		        {field:'quantity', title:'Thùng/lẻ', width:50, align: 'right', formatter :function(v,r,i) {
		        	if (v != 0 && r.convfact != 0) {
		        		return formatQuantity(Utils.XSSEncode(v), Utils.XSSEncode(r.convfact));
		        	}
		        }},
		        {field:'availableQuantity', title:'Tồn kho đáp ứng', width:50, align: 'right', formatter : function(v,r,i) {
		        	return formatQuantity(Utils.XSSEncode(v), Utils.XSSEncode(r.convfact));
		        }},
		        {field:'promotion', title:'Hàng KM', width:50, align: 'center', formatter : function(v,r,i) {
		        	if (r.isFreeItem != undefined && r.isFreeItem != null && r.isFreeItem == 1) {
		        		return '<img src="/resources/images/icon-tick.png" width="16" height="16"/>';
		        	}
		        	return '';
		        }},
		        {field:'programCode', title:'CTKM', width:70, align: 'left', formatter : function(v,r,i) {
		        	v=Utils.XSSEncode(v);
		        	if (r.programType == 'KEY_SHOP') {
		        		return '<a class="delIcons" onclick="return SPCreateOrder.showKeyShopDetail(\'' + Utils.XSSEncode(v) + '\');" href="javascript:void(0)">' + Utils.XSSEncode(v) + '</a>';
		        	}
		        	return '<a class="cmsdefault" href="javascript:void(0);" onclick="return SPCreateOrder.showPromotionProgramDetail(\'' + v + '\')">' + v + '</a>';
		        }},
		        {field:'promotionProgramCode', title:'KM hết hạn', width:50, align: 'center', formatter : function(v,r,i) {
		        	if (r.isFreeItem != undefined && r.isFreeItem != null && r.programCode != null && r.promotionProgramCode == null && r.isVansale != 1 && r.programType != 'KEY_SHOP') {
		        		return '<img src="/resources/images/icon-tick.png" width="16" height="16"/>';
		        	}
		        	return '';
		        }},
		        {field:'price', title:'Sai giá', hidden:!SPConfirmOrder._isAllowShowPrice, width:50, align: 'center', formatter : function(v,r,i) {
		        	if (r.isInvalidPrice == 1 && r.isVansale != 1
		        			&& r.isFreeItem != null && r.isFreeItem != undefined && r.isFreeItem == 0) {	// chi check sai gia doi voi san pham ban (ko check doi voi SPKM)
		        		return '<img src="/resources/images/icon-tick.png" width="16" height="16"/>';
		        	}
		        	return '';
		        }},
		        {field:'outOfStock', title:'Thiếu hàng', width:50, align: 'center', formatter : function(v,r,i) {
		        	if (v!=null && v==1 && r.isVansale != 1) {
		        		return '<img src="/resources/images/icon-tick.png" width="16" height="16"/>';
		        	}
		        	return '';
		        }},
		        {field:'isChangeQuantity', title:'Đổi số lượng', width:50, align: 'center', formatter : function(v,r,i) {
		        	if (v != null && v == 1) {
		        		return '<img src="/resources/images/icon-tick.png" width="16" height="16"/>';
		        	}
		        	return '';
		        }},
		        {field:'isChangeRation', title:'Đổi số suất', width:50, align: 'center', formatter : function(v,r,i) {
		        	if (v != null && v == 1) {
		        		return '<img src="/resources/images/icon-tick.png" width="16" height="16"/>';
		        	}
		        	return '';
		        }},
		        {field:'isExceedKeyShop', title:'Lỗi trả CTHTTM', width:50, align: 'center', formatter : function(v,r,i) {
		        	if (v != null && v == 1) {
		        		return '<img src="/resources/images/icon-tick.png" width="16" height="16"/>';
		        	}
		        	return '';
		        }},
		    ]],
		    onLoadSuccess:function(data) {
		    	//Utils.functionAccessFillControl("dgGridDetailContainer");
		    	$('#dgGridDetailContainer .datagrid-ftable').css('color','red');//footer
		    	$('.datagrid-header-rownumber').html('STT');
			   	$('.datagrid-header-row td div').css('text-align','center');
			   	updateRownumWidthForJqGrid('.easyui-dialog');
			   	if (!SPConfirmOrder._isAllowShowPrice) {
			   		$('#gridDetail').datagrid('hideColumn','price');
			   	} 
		   	 	$(window).resize();
		    }
		});
		$('html, body').animate({ scrollTop: $(document).height() }, 1000);
	},
	/**
	 * Load grid thanh toan
	 * @author tungmt
	 * @since 29/08/2014
	 */
	loadGridPay:function(params,shopCode){
		$('#dgGridPayContainer').html('<table id="gridPay"></table><div id="pager"></div>');
		$('#gridPay').datagrid({  
	 	    url:'/sale-product/confirm-payment/searchPay',
		    checkOnSelect : false,
			pageSize: 10,
			pageList: [10, 20, 30, 50, 100, 200, 500],
			//autoWidth: true,
			fitColumns : ($('#dgGridPayContainer').width()-20 > 1325),
			pageNumber:1,
		    selectOnCheck:false,
		    singleSelect:true,
		    scrollbarSize:0,
		    autoRowHeight : true,
			pagination:true,
			singleSelect:true,
			queryParams:params,
		    width: $('#dgGridPayContainer').width()-20,
		    frozenColumns: [[
				GridUtil.getRowNumField('gridPay'),
				{field:'checkbox', title:'<input class="cbPayAll" type="checkbox" onclick="SPConfirmOrder.cbPayClickAll(this)" />', width:35,sortable:true,resize:true, align: 'center', formatter : function(v,r,i) {
					var order=SPConfirmOrder._lstPay.get(r.payReceivedId);
					if(order==null){
						r.id=r.payReceivedId;
						SPConfirmOrder._lstPay.put(r.id,r);
						order=r;
					}
					if(order.check!=undefined && order.check!=null && order.check==1){
						return '<input checked="checked" class="cbPay cbPay'+order.id+'" type="checkbox" onclick="SPConfirmOrder.cbPayClick(this,'+Utils.XSSEncode(order.id)+')" />';
					}
					return '<input class="cbPay cbPay'+order.id+'" type="checkbox" onclick="SPConfirmOrder.cbPayClick(this,'+Utils.XSSEncode(order.id)+')" />';
				}},
				{field:'refOrderNumber', title:confirm_order_so_ref , width:120,sortable:true, resize:true, align: 'left', formatter : function(v,r,i) {
		        	v=Utils.XSSEncode(v);
		        	if(v!=null && v!=''){
			        	var str=v.split('\n');
			        	var html='';
			        	if(str.length>1){
			        		html=str[0]+'<a class="cmsdefault" href="javascript:void(0);" onclick="SPConfirmOrder.showInfoPay(this,\''+v.replace(/\n/g,':xuongdong')+'\')">...</a>';
			        	}else{
			        		html=v;
			        	}
			        	return html;
		        	}
		        	return '';
		        }},
		        {field:'orderNumber', title:confirm_order_so_don_hang, width:100,sortable:true, resize:true, align: 'center', formatter : function(v,r,i) {
		        	v=Utils.XSSEncode(v);
		        	if(v!=null && v!=''){
			        	var str=v.split('\n');
			        	var html='';
			        	if(str.length>1){
			        		html=str[0]+'<a class="cmsdefault" href="javascript:void(0);" onclick="SPConfirmOrder.showInfoPay(this,\''+v.replace(/\n/g,':xuongdong')+'\')">...</a>';
			        	}else{
			        		html=v;
			        	}
			        	return html;
		        	}
		        	return '';
		        }},
		        {field:'payReceivedNumber', title:confirm_order_so_chung_tu, width:120,sortable:true, resize:true, align: 'left', formatter : function(v,r,i) {
		        	if (Math.abs(r.remain) < Math.abs(r.amount+r.discount)) {
		        		return '<a href="javascript:void(0);" style="color:#800;font-weight:bold;text-decoration:underline;" \
		        						onclick="$.messager.alert(\'Cảnh báo\',\'Đơn thanh toán vượt quá số tiền còn nợ của đơn hàng\', \'warning\');">'
		        				+ Utils.XSSEncode(v) + '</a>';
		        	}
		        	return Utils.XSSEncode(v);
		        }},
		        {field:'staffCode', title:confirm_order_nvbh, width:180,sortable:true,resize:true, align: 'left', formatter : function(v,r,i) {
		        	v=Utils.XSSEncode(v);
		        	if(v!=null && v!=''){
			        	var str=v.split('\n');
			        	var html='';
			        	if(str.length>1){
			        		html=str[0]+'<a class="cmsdefault" href="javascript:void(0);" onclick="SPConfirmOrder.showInfoPay(this,\''+v.replace(/\n/g,':xuongdong')+'\')">...</a>';
			        	}else{
			        		html=v;
			        	}
			        	return html;
		        	}
		        	return '';
		        }},
		        {field:'customerCode', title:confirm_order_khach_hang, width:120,sortable:true,resize:true, align: 'left', formatter : function(v,r,i) {
		        	v=Utils.XSSEncode(v);
		        	if(v!=null && v!=''){
			        	var str=v.split('\n');
			        	var html='';
			        	if(str.length>1){
			        		var s=str[0].split('.-');//lay Id customer
			        		html+='<a class="cmsdefault" href="javascript:void(0);" onclick="SPConfirmOrder.showCustomerInfor('+s[0]+',\''+ shopCode +'\');">'+s[1]+'</a>';
			        		html+='<a class="cmsdefault" href="javascript:void(0);" onclick="SPConfirmOrder.showInfoPay(this,\''+v.replace(/\n/g,':xuongdong')+'\',\'CUSTOMER\')">...</a>';
			        	}else{
			        		var s=v.split('.-');//lay Id customer
			        		html+='<a class="cmsdefault" href="javascript:void(0);" onclick="SPConfirmOrder.showCustomerInfor('+s[0]+',\''+ shopCode +'\');">'+s[1]+'</a>';
			        	}
			        	return html;
		        	}
		        	return '';
		        }}
				]],
			    columns:[[
			        {field:'total', title:confirm_order_tong_tien , width:80,sortable:true,resize:true, align: 'right', formatter : function(v,r,i) {
			        	return formatCurrency(Utils.XSSEncode(v));
			        }},
			        {field:'discount', title:confirm_order_chiet_khau, width:80,sortable:true,resize:true, align: 'right', formatter : function(v,r,i) {
			        	return formatCurrency(Utils.XSSEncode(v));
			        }},
			        {field:'amount', title:confirm_order_thanh_toan, width:80,sortable:true, resize:true, align: 'right', formatter : function(v,r,i) {
			        	return formatCurrency(Utils.XSSEncode(v));
			        }},
			        {field:'payDate', title:confirm_order_ngay, width:70,sortable:true, resize:true, align: 'center', formatter :function(v,r,i) {
			        	return $.datepicker.formatDate('dd/mm/yy', new Date(Utils.XSSEncode(v)));
			        }},
			        {field:'address', title:confirm_order_dia_chi, width:230,sortable:true, align: 'left', formatter : function(v,r,i) {
			        	v=Utils.XSSEncode(v);
			        	if(v!=null && v!=''){
				        	var str=v.split('\n');
				        	var html='';
				        	if(str.length>1){
				        		html=str[0]+'<a class="cmsdefault" href="javascript:void(0);" onclick="SPConfirmOrder.showInfoPay(this,\''+v.replace(/\n/g,':xuongdong')+'\')">...</a>';
				        	}else{
				        		html=v;
				        	}
				        	return html;
			        	}
			        	return '';
			        }},
			        {field:'deliveryCode', title:confirm_order_nvgh, width:180,sortable:true,resize:true, align: 'left', formatter : function(v,r,i) {
			        	if(v){
			        		return Utils.XSSEncode(v);
			        	}
			        	return '';
			        }}
		    ]],
		    onLoadSuccess :function(data){
		    	if (data.isShowPrice != undefined && data.isShowPrice != null && data.isShowPrice) {
		    		$('#gridPay').datagrid('showColumn', 'total');
		    		$('#gridPay').datagrid('showColumn', 'discount');
		    	} else {
		    		$('#gridPay').datagrid('hideColumn', 'total');
		    		$('#gridPay').datagrid('hideColumn', 'discount');
		    	}
		    	Utils.functionAccessFillControl();
		    	GridUtil.updateRowNumWidth("#dgGridPayContainer", "gridPay");
			   	$('.datagrid-header-row td div').css('text-align','center');
		   	 	$(window).resize();
		   	    
		    },
		    rowStyler: function(index,row){
				if (Math.abs(row.remain) < Math.abs(row.amount+row.discount)) {
					return "color:#800;";
				}
            }
		});
	},
	/**
	 * Hủy thanh toán
	 * @author tungmt
	 * @since 29/08/2014
	 */
	cancelPay: function() {
		SPConfirmOrder.hideAllMsg();
		var lstOrderId = new Array();
		for (var i = 0, n = SPConfirmOrder._lstPay.valArray.length; i < n ; i++) {
			var order = SPConfirmOrder._lstPay.valArray[i];
			if (order.check != undefined && order.check != null && order.check == 1) {
				lstOrderId.push(order.id);
			}
		}
		if (lstOrderId.length == 0) {
			$('#errMsgPay').html('Bạn chưa chọn chứng từ để hủy.').show();
			return ;
		}
		var params = new Object();
		params.currentShopCode = $('#shopCode').combobox('getValue');
		params.lstPayReceivedId = lstOrderId;
		params.description = $('#description').val().trim();
		Utils.addOrSaveData(params, '/sale-product/confirm-payment/cancel-pay', null, 'errMsgPay', function(data) {
			if(!data.error){
				SPConfirmOrder.searchPay();
				$('#successMsgPay').html('Lưu dữ liệu thành công').show();
				var tm = setTimeout(function(){
					$('#successMsgPay').html('').hide();
					clearTimeout(tm);
				}, 2000);
			}
		});
	},
	/**
	 * Xác nhận thanh toán
	 * @author tungmt
	 * @since 29/08/2014
	 */
	confirmPay: function() {
		SPConfirmOrder.hideAllMsg();
		var lstOrderId = new Array();
		for (var i = 0, n = SPConfirmOrder._lstPay.valArray.length; i < n ; i++) {
			var order = SPConfirmOrder._lstPay.valArray[i];
			if (order.check != undefined && order.check != null && order.check == 1) {
				lstOrderId.push(order.id);
			}
		}
		if (lstOrderId.length == 0) {
			$('#errMsgPay').html('Bạn chưa chọn chứng từ để thanh toán.').show();
			return ;
		}
		var params = new Object();
		params.currentShopCode = $('#shopCode').combobox('getValue');
		params.lstPayReceivedId = lstOrderId;
		params.description = $('#description').val().trim();
		Utils.addOrSaveData(params, '/sale-product/confirm-payment/confirm-pay', null, 'errMsgPay', function(data) {
			if(!data.error){
				SPConfirmOrder.searchPay();
				$('#successMsgPay').html('Lưu dữ liệu thành công').show();
				var tm = setTimeout(function(){
					$('#successMsgPay').html('').hide();
					clearTimeout(tm);
				}, 2000);
			}
		});
	},
	/**
	 * Từ chối đơn hàng
	 * @author tungmt
	 * @param {var} saleOrderId sale order to cancel
	 * @since 29/08/2014
	 */
	denyOrder:function(saleOrderId, confirmMsg){
		/*
		 * add param 'saleOrderId' to deny sale order when view sale order's detail
		 * @modified by tuannd20
		 * @date 03/10/2014
		 */
		SPConfirmOrder.hideAllMsg();
		var lstOrderId=new Array();
		var params=new Object();
		if (saleOrderId !== null && saleOrderId !== undefined) {
			lstOrderId.push(saleOrderId);
		} else {
			for(var i=0,n=SPConfirmOrder._lstOrder.valArray.length; i<n ; i++){
				var order=SPConfirmOrder._lstOrder.valArray[i];
				if(order.check!=undefined && order.check!=null && order.check==1){
					lstOrderId.push(order.id);
				}
			}
			if(lstOrderId.length==0){
				$('#errMsgOrder').html('Bạn chưa chọn đơn hàng để từ chối.').show();
				return ;
			}
			params.description=$('#description').val().trim();
		}
		params.lstOrderId=lstOrderId;
		params.currentShopCode = $('#shopCodeCB').combobox('getValue');
		if (confirmMsg != undefined && confirmMsg != null) {
			params.isCheckOrderDeny = true;
		}
		Utils.addOrSaveData(params, '/sale-product/confirm-order/deny-order', null, 'errMsgOrder', function(data) {
			if(!data.error){
				if (saleOrderId === null || saleOrderId === undefined) {
					SPConfirmOrder.searchOrder(1);
				}else{
					SPConfirmOrder.disabledEditOrderTablet();
				}
				$('#successMsgOrder').html('Lưu dữ liệu thành công').show();
				var tm = setTimeout(function(){
					$('#successMsgOrder').html('').hide();
					clearTimeout(tm);
					if (saleOrderId) {
						window.location.href = SPConfirmOrder.ORDER_CONFIRM_URL;
					}
				}, 2000);
			} else {
				var str = data.errDenySO != undefined ? data.errDenySO + '<br/>' : '';
				str += data.errDenyCO != undefined ? data.errDenyCO : '';
				if (str != '') {
					$('#errMsgOrder').html(str).show();
				}
				if (data.errConfirm != undefined && data.errConfirm != null && data.errConfirm) {//confirm
					SPConfirmOrder.denyOrder(saleOrderId, data.errConfirm);
				}
			}
		}, null, null, null, confirmMsg);
	},
	
	/**
	 * Hủy đơn hàng
	 * @author tungmt
	 * @param {var} saleOrderId sale order to cancel
	 * @since 29/08/2014
	 */
	cancelOrder: function(saleOrderId) {
		/*
		 * add param 'saleOrderId' to cancel sale order when view sale order's detail
		 * @modified by tuannd20
		 * @date 03/10/2014
		 */
		SPConfirmOrder.hideAllMsg();
		var lstOrderId=new Array();
		var params=new Object();
		if (saleOrderId !== null && saleOrderId !== undefined) {
			lstOrderId.push(saleOrderId);
		} else {
			for(var i=0,n=SPConfirmOrder._lstOrder.valArray.length; i<n ; i++){
				var order=SPConfirmOrder._lstOrder.valArray[i];
				if(order.check!=undefined && order.check!=null && order.check==1){
					lstOrderId.push(order.id);
				}
			}
			if(lstOrderId.length==0){
				$('#errMsgOrder').html('Bạn chưa chọn đơn hàng để hủy.').show();
				return ;
			}
		}
		params.lstOrderId=lstOrderId;
		params.description=$('#description').val().trim();
		params.approvedStep = 0;
		params.currentShopCode = $('#shopCodeCB').combobox('getValue');
		Utils.addOrSaveData(params, '/sale-product/confirm-order/cancel-order', null, 'errMsgOrder', function(data) {
			if(!data.error){
				if (saleOrderId === null || saleOrderId === undefined) {
					SPConfirmOrder.searchOrder(1);
				}else{//màn hình sửa đơn tablet
					SPConfirmOrder.disabledEditOrderTablet();
				}
				$('#successMsgOrder').html('Lưu dữ liệu thành công').show();
				var tm = setTimeout(function(){
					$('#successMsgOrder').html('').hide();
					clearTimeout(tm);
					if (saleOrderId) {
						window.location.href = SPConfirmOrder.ORDER_CONFIRM_URL;
					}
				}, 2000);
			}
		});
	},
	disabledEditOrderTablet:function(){
		disabled('btnOrderApproved');
		disabled('btnOrderCancel');
		disabled('btnOrderDeny');
		disabled('btnOrderSave');
		disabled('payment');
		$('#backBtn').hide();
		SPConfirmOrder.disabledGridEditOrderTablet();
	},
	disabledGridEditOrderTablet:function(){
		try{
			var htSale = $('#gridSaleData').handsontable('getInstance');
			for(var i=0;i<htSale.countRows();i++){
				for (var p in COLSALE) {
				    htSale.getCellMeta(i, COLSALE[p]).readOnly = true;
				}
			}
			htSale.render();
		}catch(e){
			
		}
	},
	/**
	 * Xác nhận đơn hàng
	 * @author tungmt
	 * @param {var} saleOrderId sale order to cancel
	 * @since 29/08/2014
	 */
	confirmOrder:function(saleOrderId,idErrMsg, isCheckPrice){//skipDebit : không cần kiểm tra hạn mức nợ
		/*
		 * add param 'saleOrderId' to cancel sale order when view sale order's detail
		 * @modified by tuannd20
		 * @date 03/10/2014
		 */
		SPConfirmOrder.hideAllMsg();
		var lstOrderId=new Array();
		if (saleOrderId !== null && saleOrderId !== undefined) {
			lstOrderId.push(saleOrderId);
		} else {
			for(var i=0,n=SPConfirmOrder._lstOrder.valArray.length; i<n ; i++){
				var order=SPConfirmOrder._lstOrder.valArray[i];
				if(order.check!=undefined && order.check!=null && order.check==1){
					lstOrderId.push(order.id);
				}
			}
			if(lstOrderId.length==0){
				$('#errMsgOrder').html('Bạn chưa chọn đơn hàng để xác nhận.').show();
				return ;
			}
		}
		var params = new Object();
		params.lstOrderId = lstOrderId;
		if (saleOrderId !== null && saleOrderId !== undefined) {
			params.saleOrderId = saleOrderId;
		}
		if (isCheckPrice !== null && isCheckPrice !== undefined) {
			params.isCheckPrice = isCheckPrice
		}
		params.currentShopCode = $('#shopCodeCB').combobox('getValue');
		SPConfirmOrder._lstOrderChangePrice=new Map();
		var idErrOrder='errMsgOrder';
		if(idErrMsg!=undefined && idErrMsg!=null && idErrMsg!=''){
			idErrOrder=idErrMsg;
		}
		var mes='Bạn có muốn xác nhận đơn hàng này?';
		if (SPAdjustmentOrder._editingSaleOrder && SPAdjustmentOrder._editingSaleOrder.orderType
			&& SPAdjustmentOrder._editingSaleOrder.orderType == orderType.IN) {
			if(saleOrderId!=undefined && saleOrderId!=null && $('#btnOrderSave').attr('disabled')!='disabled'){
				mes='Xác nhận đơn hàng sẽ hủy bỏ toàn bộ thông tin đơn hàng đã nhập. Bạn có muốn tiếp tục?';
			}
		}		
		Utils.addOrSaveData(params, '/sale-product/confirm-order/confirm-order', null, idErrOrder, SPConfirmOrder.confirmCallback,null,null,null,mes);
	},
	confirmCallback: function(data) {
		//trường hợp xác nhận đơn đã qua bước xác nhận thì throw exception và thông báo
		if(data.error && data.errMsg != undefined && data.errMsg == 'DON_HANG_DA_XU_LY'){
			$('#serverErrors').html('Đơn hàng đã xử lý, yêu cầu kiểm tra lại').show();
			$('#errMsgOrder').html('Đã có đơn hàng đã xử lý, yêu cầu kiểm tra lại').show();
			return;
		}
		var curUrl = window.location.href;
		if (curUrl.indexOf("/sale-product/create-order/adjust-info") > -1 && data.lstErrors && data.lstErrors.length > 0) {
			data.error = true;

			var msg = '';
			if (data.lstErrors[0].systemError == 'X') {
				data.errMsg = 'Lỗi hệ thống.';
			} else if (data.lstErrors[0].confirmedErr == 'X') {
				data.errMsg = 'Đơn hàng đã xác nhận.';
			} else if (data.lstErrors[0].orderNotIntegrityErr == 'X') {
				data.errMsg = 'Lỗi toàn vẹn đơn hàng.';
			} else if (data.lstErrors[0].quantityErr != null && data.lstErrors[0].quantityErr.length > 0) {
				data.errMsg = 'Đơn hàng hết tồn kho sản phẩm ' + data.lstErrors[0].quantityErr + '.';
			} else if (data.lstErrors[0].promotionErr == 'X') {
				data.errMsg = 'Đơn hàng có CTKM hết hạn.';
			} else if (data.lstErrors[0].amountErr == 'X') {
				data.errMsg = 'Đơn hàng rớt chi tiết.';
			} else if (data.lstErrors[0].SOCOErr == 'X') {
				data.errMsg = 'Đơn bán SO chưa qua bước in hóa đơn.';
			} else if (data.lstErrors[0].openPromo == 'X') {
				data.errMsg = 'Không xác nhận đơn hàng do có khuyến mãi mở mới đã được trả.';
			} else if (data.lstErrors[0].accumulation == 'X') {
				data.errMsg = 'Yêu cầu tính tiền lại cho đơn hàng do trả thưởng khuyến mãi tích lũy không còn đúng đắn.';
			} else if (data.lstErrors[0].notEnoughPortion == 'X') {
				data.errMsg = 'Đơn hàng có CTKM không đủ số suất.';
			} else if (data.lstErrors[0].keyShopErr == 'X') {
				data.errMsg = 'Đơn hàng có lỗi trả CTHTTM.';
			} else if (data.lstErrors[0].customerNotActive == 'X') {
				data.errMsg = 'Khách hàng này đã ngưng hoạt động, bạn không thể tiến hành xác nhận đơn hàng được!';
			}
			
			if (data.lstErrors[0].debitMax == 'X') {
				msg += 'Số tiền của khách hàng vượt quá định mức. Bạn có muốn xác nhận tiếp ?<br>';
			}
			if (data.lstErrors[0].debitMaxDate == 'X') {
				msg += 'Số ngày nợ của khách hàng vượt quá định mức. Bạn có muốn xác nhận tiếp ?<br>';
			}
			if (data.lstErrors[0].priceErr == 'X') {
				msg += 'Đơn hàng có sản phẩm sai giá. Bạn có muốn xác nhận tiếp ?<br>';
			}
			if (data.lstErrors[0].changeQuantity == 'X') {
				msg += 'Đơn hàng có SPKM bị đổi số lượng. Bạn có muốn xác nhận tiếp ?<br>';
			}
			if (data.lstErrors[0].changeRation == 'X') {
				msg += 'Đơn hàng có CTKM bị đổi số suất. Bạn có muốn xác nhận tiếp ?<br>';
			}
	        
			if(msg != '' && (data.errMsg == undefined || data.errMsg == null || data.errMsg == '')){
				$.messager.confirm("Xác nhận", msg, function(y) {
					if (y) {
						showLoadingIcon();
						var params=new Object();
						params.lstOrderId=[SPAdjustmentOrder._orderId];
						params.description=$('#description').val().trim();
						params.shopCode = $('#shopCodeCB').combobox('getValue');
						Utils.saveData(params, '/sale-product/confirm-order/accept-order-err', null, 'errMsg', function(data) {
							if(!data.error){
								if (curUrl.indexOf("/sale-product/create-order/adjust-info") > -1){
									SPConfirmOrder.disabledButtonAdjust();
								}
								$('#successMsg').html('Xác nhận đơn hàng thành công').show();
								setTimeout(function(){$('#successMsgOrder').hide();},4000);
							}
						});
					}
				});
				return ;
			}
		}
		if (data.confirmAccumulation == 1) {
			data.error = true;
		}
		if (!data.error) {
			if(data.lstErrors != undefined && data.lstErrors != null && data.lstErrors.length > 0){
				SPConfirmOrder.openDialogErr(data);
			}else{
				$('#successMsgOrder').html('Xác nhận đơn hàng thành công').show();
				if (curUrl.indexOf("/sale-product/create-order/adjust-info") > -1){
					SPConfirmOrder.disabledButtonAdjust();
				}
				setTimeout(function(){
					$('#successMsgOrder').hide();
				},4000);
			}
			SPConfirmOrder.searchOrder(1);
		} 
		else {
			setTimeout(function() {$(".SuccessMsgStyle").hide();}, 20);
			if (data.confirmAccumulation == 1) {
				$.messager.confirm("Xác nhận", data.errMsg, function(y) {
					if (y) {
						showLoadingIcon();
						var params = new Object();
						params.lstOrderId = [SPAdjustmentOrder._orderId];
						params.saleOrderId = SPAdjustmentOrder._orderId;
						SPConfirmOrder._lstOrderChangePrice=new Map();
						params.confirmAccumulation = 1;
						Utils.saveData(params, '/sale-product/confirm-order/confirm-order', null, "serverErrors", SPConfirmOrder.confirmCallback);
					}
				});
			} else {
				$("#serverErrors").html(data.errMsg).show();
			}
		}
	},
	disabledButtonAdjust:function(){
		disabled('btnOrderSave');
		disabled('btnOrderApproved');
		disabled('btnOrderDeny');
		disabled('btnOrderCancel');
		disabled('payment');
	},
	confirmApplyPrice:function(){
		for(var i=0,n=SPConfirmOrder._lstOrderChangePrice.valArray.length; i<n ; i++){
			var order=SPConfirmOrder._lstOrderChangePrice.valArray[i];
			if(order!=null){
				if(order.isChange==undefined || order.isChange==null){
					SPConfirmOrder._idApply=order.id;
					SPConfirmOrder.showMessagePrice(order.orderNumber);
					return;
				}
			}else{//sửa đơn hàng tablet
				SPConfirmOrder._lstOrderChangePrice=new Map();
				SPConfirmOrder.showMessagePrice('');
				return;
			}
		}
		SPConfirmOrder.searchOrder();
	},
	confirmSkipDebit:function(){
		var lst=SPConfirmOrder._lstOrderSkipDebit.valArray;
		for(var i=0;i<lst.length;i++){
			if(lst[i].isChange==undefined || lst[i].isChange==null){
				SPConfirmOrder._idDebit=lst[i].id;
				SPConfirmOrder.showMessageDebit(lst[i]);
				return;
			}
		}
		SPConfirmOrder.searchOrder();
	},
	/**
	 * Hiện thông báo sai giá để xác nhận đổi giá
	 * @author tungmt
	 * @since 29/08/2014
	 */
	showMessagePrice:function(orderNumber){
		$('#txtConfirmPrice').html('Đơn hàng '+Utils.XSSEncode(orderNumber)+' có sản phẩm sai giá. Bạn có muốn thay đổi giá.');
		var html=$('#divConfirmPrice').html();
		$('#divOverlay').show();
		$.messager.show({
			title:'<div class="messageTitle">Xác nhận</div>',
			msg:html,
			timeout:null,
			showType:'fade',
			width:'auto',
			height:'auto',
			style:{
				right:'',
				bottom:''
			}
		});
		$('.panel-tool-close').bind('click',function(){
			$('#divOverlay').hide();
		});
	},
	/**
	 * Thông báo hạn mức nợ (Nếu xác nhận thì cho xác nhận đơn hàng luôn không cần check mức nợ)
	 * @author tungmt
	 * @since 29/08/2014
	 */
	showMessageDebit:function(obj){
		var str='';
		if(obj.debitMax!=null && obj.debitMax=='X' && obj.debitMaxDate!=null && obj.debitMaxDate=='X'){
			str='Số tiền và số ngày nợ';
		}else if(obj.debitMax!=null && obj.debitMax=='X'){
			str='Số tiền nợ';
		}else{
			str='Số ngày nợ';
		}
		str+=' của khách hàng '+Utils.XSSEncode(obj.customerCode+'-'+obj.customerName)+' vượt quá định mức nợ cho phép, bạn có muốn tiếp tục không?';
		$('#txtConfirmDebit').html(str);
		var html=$('#divConfirmDebit').html();
		$('#divOverlay').show();
		$.messager.show({
			title:'<div class="messageTitle">Xác nhận</div>',
			msg:html,
			timeout:null,
			showType:'fade',
			width:'auto',
			height:'auto',
			style:{
				right:'',
				bottom:''
			}
		});
		$('.panel-tool-close').bind('click',function(){
			$('#divOverlay').hide();
		});
	},
	/**
	 * Đóng thông báo
	 * @author tungmt
	 * @since 29/08/2014
	 */
	closeMessage:function(){
		$('.messageTitle').parent().parent().parent().remove();
		$('#divOverlay').hide();
	},
	/**
	 * Không đổi giá tất cả đơn hàng sai giá
	 * @author tungmt
	 * @since 29/08/2014
	 */
	dismissPriceAll:function(){
		SPConfirmOrder._idApply=null;
		SPConfirmOrder._lstOrderChangePrice=new Map();
		SPConfirmOrder.closeMessage();
		SPConfirmOrder.searchOrder();
	},
	/**
	 * Không đổi giá đơn hàng sai giá
	 * @author tungmt
	 * @since 29/08/2014
	 */
	dismissPrice:function(){
		if(SPConfirmOrder._idApply!=null){
			var orderChange=SPConfirmOrder._lstOrderChangePrice.get(SPConfirmOrder._idApply);
			orderChange.isChange=0;
			SPConfirmOrder._idApply=null;
			SPConfirmOrder.closeMessage();
			SPConfirmOrder.confirmApplyPrice();
		}else{
			SPConfirmOrder._idApply=null;
			SPConfirmOrder.closeMessage();
		}
	},
	/**
	 * Đổi giá đơn hàng sai giá
	 * @author tungmt
	 * @since 29/08/2014
	 */
	applyPrice:function(saleOrderId){
		if(saleOrderId!=undefined && saleOrderId!=null && saleOrderId!=0){//man hinh sửa đơn hàng tablet
			SPConfirmOrder.applyNewPrice([saleOrderId]);
		}else{
			if(SPConfirmOrder._idApply!=null){
				var orderChange=SPConfirmOrder._lstOrderChangePrice.get(SPConfirmOrder._idApply);
				if(orderChange!=null){
					orderChange.isChange=1;
					SPConfirmOrder._idApply=null;
					SPConfirmOrder.applyNewPrice([orderChange.id]);
				}
			}
		}
	},
	/**
	 * Đổi giá tất cả đơn hàng sai giá
	 * @author tungmt
	 * @since 29/08/2014
	 */
	applyPriceAll:function(){
		var lstOrderId=new Array();
		for(var i=0,n=SPConfirmOrder._lstOrderChangePrice.valArray.length; i<n ; i++){
			var order=SPConfirmOrder._lstOrderChangePrice.valArray[i];
			if(order.isChange==undefined || order.isChange==null){
				lstOrderId.push(order.id);
			}
		}
		SPConfirmOrder._lstOrderChangePrice=new Map();
		SPConfirmOrder.applyNewPrice(lstOrderId);
	},
	/**
	 * Thực hiện đổi giá mới
	 * @author tungmt
	 * @since 29/08/2014
	 */
	applyNewPrice:function(lstOrderId){
		SPConfirmOrder.closeMessage();
		var params=new Object();
		params.lstOrderId=lstOrderId;
		params.description=$('#description').val().trim();
		Utils.addOrSaveData(params, '/sale-product/confirm-order/apply-new-price', null, 'errMsgOrder', function(data) {
			if(!data.error){
				if(SPConfirmOrder._lstOrderChangePrice.valArray.length>0){
					SPConfirmOrder.confirmApplyPrice();
				}else{
					SPConfirmOrder.searchOrder();
				}
				$('#successMsgOrder').html('Lưu dữ liệu thành công').show();
				var tm = setTimeout(function(){
					$('#successMsgOrder').html('').hide();
					clearTimeout(tm);
				}, 2000);
			}
		});
	},
	/**
	 * Không chấp nhận tất cả đơn hàng vượt quá hạn mức nợ
	 * @author tungmt
	 * @since 29/08/2014
	 */
	dismissDebitAll:function(){
		SPConfirmOrder._idDebit=null;
		SPConfirmOrder._lstOrderSkipDebit=new Map();
		SPConfirmOrder.closeMessage();
		SPConfirmOrder.searchOrder();
	},
	/**
	 * Không chấp nhận đơn hàng vượt quá hạn mức nợ
	 * @author tungmt
	 * @since 29/08/2014
	 */
	dismissDebit:function(){
		if(SPConfirmOrder._idDebit!=null){
			var orderChange=SPConfirmOrder._lstOrderSkipDebit.get(SPConfirmOrder._idDebit);
			orderChange.isChange=0;
			SPConfirmOrder._idDebit=null;
			SPConfirmOrder.closeMessage();
			SPConfirmOrder.confirmSkipDebit();
		}
	},
	/**
	 * Chấp nhận đơn hàng vượt quá hạn mức nợ
	 * @author tungmt
	 * @since 29/08/2014
	 */
	skipDebit:function(saleOrderId){
		if(saleOrderId!=undefined && saleOrderId!=null && saleOrderId!=0){//man hinh sửa đơn hàng tablet
			SPConfirmOrder.skipDebitCustomer([saleOrderId]);
		}else{
			if(SPConfirmOrder._idDebit!=null){
				var orderChange=SPConfirmOrder._lstOrderSkipDebit.get(SPConfirmOrder._idDebit);
				orderChange.isChange=1;
				SPConfirmOrder._idDebit=null;
				SPConfirmOrder.skipDebitCustomer([orderChange.id]);
			}
		}
	},
	/**
	 * Chấp nhận tất cả đơn hàng vượt quá hạn mức nợ
	 * @author tungmt
	 * @since 29/08/2014
	 */
	skipDebitAll:function(){
		var lstOrderId=new Array();
		for(var i=0,n=SPConfirmOrder._lstOrderSkipDebit.valArray.length; i<n ; i++){
			var order=SPConfirmOrder._lstOrderSkipDebit.valArray[i];
			if(order.isChange==undefined || order.isChange==null){
				lstOrderId.push(order.id);
			}
		}
		SPConfirmOrder._lstOrderSkipDebit=new Map();
		SPConfirmOrder.skipDebitCustomer(lstOrderId);
	},
	/**
	 * Thực hiện xác nhận đơn hàng vượt quá hạn mức
	 * @author tungmt
	 * @since 29/08/2014
	 */
	skipDebitCustomer:function(lstOrderId){
		SPConfirmOrder.closeMessage();
		var params=new Object();
		params.lstOrderId=lstOrderId;
		params.description=$('#description').val().trim();
		Utils.addOrSaveData(params, '/sale-product/confirm-order/skip-debit-customer', null, 'errMsgOrder', function(data) {
			if(!data.error){
				if(SPConfirmOrder._lstOrderSkipDebit.valArray.length>0){
					SPConfirmOrder.confirmSkipDebit();
				}else{
					SPConfirmOrder.searchOrder();
				}
				$('#successMsgOrder').html('Lưu dữ liệu thành công').show();
				var tm = setTimeout(function(){
					$('#successMsgOrder').html('').hide();
					clearTimeout(tm);
				}, 2000);
			}
		});
	},
	/**
	 * Xem thông tin sản phẩm
	 * @author tungmt
	 * @since 29/08/2014
	 */
	viewProductDetails:function(productCode,customerCode,convfact,salerCode){
		if(productCode!=''){
			$.getJSON('/commons/products-details?productCode='+productCode+'&customerCode='+customerCode+'&salerCode='+salerCode+'&shopCodeFilter='+SPConfirmOrder._currentShopCode, function(data) {
				if(!data.error){						
					CommonSearch.showProductInfo(data.product.productCode, data.product.productName, 
							data.product.convfact,
							StockValidateInput.formatStockQuantity(data.avaiableQuantity, convfact),
							data.price, StockValidateInput.getQuantity(data.avaiableQuantity,convfact), data.promotion, null, data.priceThung, SPConfirmOrder._isAllowShowPrice);						
				}					
			});				
		}
	},
	
	openDialogErr:function(data){
		$('.easyui-dialog #btnAccept').unbind('click');
		$('.easyui-dialog #successMsg').hide();
		CommonSearch._arrParams = null;
		CommonSearch._currentSearchCallback = null;
		$('#errEasyUIDialogDiv').show();
		var html = $('#errEasyUIDialogDiv').html();
		SPConfirmOrder._mapOrderErr = new Map();
		$('#errEasyUIDialog').dialog({  
	        title: 'Lỗi đơn hàng',  
	        closed: false,  
	        cache: false,  
	        modal: true,
	        onOpen: function(){
	        	$('.easyui-dialog #errGrid').show();
	        	$('.easyui-dialog #errGrid').datagrid('loadData',[]);
	        	if(data.lstSuccess != undefined && data.lstSuccess != null && data.lstSuccess != ''){
	        		var str = Utils.XSSEncode(data.lstSuccess);
	        		if(str.length > 300){
	        			str = str.substring(0,300) + '......';
	        		}
	        		$('.easyui-dialog #successMsgErr').html('Xác nhận thành công các đơn hàng '+str).show();
	        	}
				for(var i = 0 ,n = data.lstErrors.length; i < n ;){
					data.lstErrors[i].stt = i+1;
					i++;
				}
				$('.easyui-dialog #errGrid').datagrid('loadData',data.lstErrors);
	        },
	        onClose:function(){
	        	$('#btnAccept').show();
		    	$('.easyui-dialog #errGrid').datagrid('load',null);
		    }
		});
	},
	onClickCBErr:function(t, id){
		if($(t).is(':checked')){
			SPConfirmOrder._mapOrderErr.put(id,id);
		}else{
			SPConfirmOrder._mapOrderErr.remove(id);
		}
	},
	acceptErr:function(){
		SPConfirmOrder.closeMessage();
		if(SPConfirmOrder._mapOrderErr.valArray.length == 0){
			$('#successMsgErr').html('Chưa chọn đơn hàng nào.').show();
			return;
		}
		$(".ErrorMsgStyle").hide();
		$(".SuccessMsgStyle").hide();
		var params=new Object();
		params.lstOrderId=SPConfirmOrder._mapOrderErr.valArray;
		params.description=$('#description').val().trim();
		params.shopCode = $('#shopCodeCB').combobox('getValue');
		Utils.addOrSaveData(params, '/sale-product/confirm-order/accept-order-err', null, 'errMsgOrder', function(data) {
			if(!data.error){
				SPConfirmOrder.searchOrder();
				$('.easyui-dialog #successMsgErr').html('Lưu dữ liệu thành công').show();
				var tm = setTimeout(function(){
					$('.easyui-dialog #successMsgErr').html('').hide();
					$('.easyui-dialog').dialog('close');
					clearTimeout(tm);
				}, 2000);
			}
		});
	}
};
/**
 * trả hàng
 */
var SPReturnOrder = {
	_isAllowShowPrice: true,
	_isReturn: false,
	_isReturnKeyShop: true,
	searchSaleOrder: function() {
		$('.ErrorMsgStyle').hide();
		var msg = '';	
		var fDate = $('.easyui-dialog #fromDate').val().trim();
		var tDate = $('.easyui-dialog #toDate').val().trim();
		if(fDate == '__/__/____'){
			$('.easyui-dialog #fromDate').val('');
		}
		if(tDate == '__/__/____'){
			$('.easyui-dialog #toDate').val('');
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfInvalidFormatDate('fromDate', 'Từ ngày');
		}
		if(msg.length ==0){
			msg = Utils.getMessageOfInvalidFormatDate('toDate', 'Đến ngày');
		}
		
		if(msg.length == 0 && !Utils.compareDate(fDate, tDate)){
			msg = msgErr_fromdate_greater_todate;
			$('#fromDate').focus();
		}
		if(msg.length > 0){
			$('.easyui-dialog #errorSearchMsg').html(msg).show();
			return false;
		}
		var params = new Object();
		params = SPReturnOrder.getParam();
		params.flag = true;
		$('#gridSearchBill').datagrid('load',params);
		return false;
	},

	getParam : function() { // lấy tham số từ các field trên facybox tìm kiếm
		// đưa vào action
		var orderNumber = $(' #orderNumber').val().trim();
		var shortCode = $(' #shortCode').val().trim();
		var customerName = $(' #customerName').val().trim();
		var fDate = $(' #fromDate').val().trim();
		var tDate = $(' #toDate').val().trim();
		var staffCode = $(' #staffCode').val().trim();
		var deliveryCode = $(' #deliveryCode').val().trim();
		var data = new Object;
		if (orderNumber.length > 0 && orderNumber.length <= 20) {
			data.orderNumber = orderNumber;
		}
		if (shortCode.length > 0 && shortCode.length <= 10) {
			data.shortCode = shortCode;
		}
		if (customerName.length > 0 && customerName.length <= 250) {
			data.customerName = customerName;
		}
		if (staffCode.length > 0 && staffCode.length <= 50) {
			data.staffCode = staffCode;
		}
		if (deliveryCode.length > 0 && deliveryCode.length <= 50) {
			data.deliveryCode = deliveryCode;
		}
		if (fDate.length > 0 && fDate.length <= 50) {
			data.fromDate = fDate;
		}
		if (tDate.length > 0 && tDate.length <= 50) {
			data.toDate = tDate;
		}
		data.orderTypeString = $('#orderType').val().trim();
		data.saleOrderStatusString = 1; // APPROVED
		data.saleOrderTypeString = 1; // TYPE
		data.shopCode = $('#shopCodeCB').combobox('getValue');
		return data;
	},
	choosePOCode : function(index) {		//saleOrderId, customerId, staffCode, deliveryCode, orderNumber
		var row = $('#gridSearchBill').datagrid('getRows')[index];
		var paramsSaleOrderDetail = new Object();
		$('.ErrorMsgStyle').hide();
		paramsSaleOrderDetail.saleOrderId = row.id;                    //saleOrderId;
		paramsSaleOrderDetail.customerId = row.customer.id;          //customerId
		paramsSaleOrderDetail.staffCode = row.staff.staffCode;      //staffCode;
		if (row.delivery != null) {
			paramsSaleOrderDetail.deliveryCode = row.delivery.staffCode;//deliveryCode;
		}
		paramsSaleOrderDetail.orderNumber = row.orderNumber;        //orderNumber;
		$('#popup1').dialog('close');
		Utils.getHtmlDataByAjax(paramsSaleOrderDetail,'/sale-product/return-product/customerdetail', function(data) {
			try {
				var _data = JSON.parse(data);
				if (_data.error == true && _data.errMsg != undefined) {
						SPReturnOrder.dialogBoxError(_data.errMsg);
				}
			} catch (e) {
				$('#customerDetail').html(data);
				
				paramsSaleOrderDetail.saleOrderId = $("#returnOrderId").val().trim();
				$('#saleOrderReturnId').val(paramsSaleOrderDetail.saleOrderId);
				
				Utils.getJSONDataByAjax(paramsSaleOrderDetail, '/sale-product/return-product/saleorderdetail', function(data) {
					SPReturnOrder.loadProductGrid(data.lstDetail);
					SPReturnOrder.loadPromotionGrid(data.lstPromotion, "promotionGrid", "promotionH2", "promotionContainer");
					SPReturnOrder.loadPromotionGrid(data.lstOpenPromotion, "openPromoGrid", "openPromoH2", "openPromoContainer");
					SPReturnOrder.loadPromotionGrid(data.lstOrderPromotion, "orderPromoGrid", "orderPromoH2", "orderPromoContainer");
					SPReturnOrder.loadAccumlationGrid(data.lstAccumulation);
					if (SPReturnOrder._isReturnKeyShop) {
						SPReturnOrder._isReturn = true;
						SPSearchSale.loadKeyShop(paramsSaleOrderDetail.saleOrderId, row.shopCode, 40);
					}
				}, 'loadingSearch', 'GET');

				var returnOrderNumber = $("#returnOrderNumber").val().trim();
				if (returnOrderNumber.length > 0) {
					$("#btnReturn").attr("disabled", "disabled");
					$("#btnReturn").addClass("BtnGeneralDStyle");
					$("#btnReturn").removeAttr("onclick");
					$("#btnCancel").removeAttr("disabled");
					$("#btnCancel").removeClass("BtnGeneralDStyle");
					$("#btnCancel").attr("onclick", "SPReturnOrder.cancelReturnOrder("+Number(paramsSaleOrderDetail.saleOrderId)+");")
				} else {
					$("#btnCancel").attr("disabled", "disabled");
					$("#btnCancel").addClass("BtnGeneralDStyle");
					$("#btnCancel").removeAttr("onclick");
					$("#btnReturn").removeAttr("disabled");
					$("#btnReturn").removeClass("BtnGeneralDStyle");
					$("#btnReturn").attr("onclick", "SPReturnOrder.returnProductOrder("+Number(paramsSaleOrderDetail.saleOrderId)+");")
				}
			}
		}, 'loadingSearch', 'GET'); // ajax show lên trang tra hang thong tin khach hang.
	},
	showProductInfo: function(productCode, productName, convfactNumber, stockNumber, priceValue, total, promotionProgram) {
		var html = $('#showProductInfoContainerRT').html();
		$('#showProductInfoRT').dialog({
			title: "Thông tin mặt hàng",
	        	closed: false,  
	        	cache: false,  
	        	modal: true,
	        	onOpen: function() {
	        		$('#showProductInfoRT').addClass("easyui-dialog");
				$('#showProductInfoRT #productCode').text(Utils.XSSEncode(productCode));
				$('#showProductInfoRT #productName').text(Utils.XSSEncode(productName));
				$('#showProductInfoRT #convfactNumber').text(Utils.XSSEncode(convfactNumber));
				$('#showProductInfoRT #stockNumber').text(Utils.XSSEncode(stockNumber));
				$('#showProductInfoRT #priceValue').text(Utils.XSSEncode(priceValue));
				$('#showProductInfoRT #totalQuantity').text(Utils.XSSEncode(total));
				if (promotionProgram == null || promotionProgram == undefined || promotionProgram.length == 0) {
					$('#showProductInfoRT #promotionProgram').text('');
				} else {
					$('#showProductInfoRT #promotionProgram').text(Utils.XSSEncode(promotionProgram));
				}				
	        	},
	        	onClose:function() {
	        		$('#showProductInfoRT').dialog("destroy");
	        		$('#showProductInfoContainerRT').html(html);
	        	}
		});
	},
	showPromotionProgramInfo : function(ppCode, ppName, ppType, ppFormat, fromDate, toDate, ppDescription) {
		var html = $('#PopupShowPromotionProgramInfoDiv').html();
		$('#PopupShowPromotionProgramInfo').dialog({
			title: "Thông tin CTHTTM",
			closed: false,  
	        	cache: false,   
	        	modal: true,
	        	width: 600,
	        	height: "auto",
	        	onOpen: function() {
		 		$('#PopupShowPromotionProgramInfo').addClass("easyui-dialog");
				$('#PopupShowPromotionProgramInfo #programCode').text(Utils.XSSEncode(ppCode));
				$('#PopupShowPromotionProgramInfo #programName').text(Utils.XSSEncode(ppName));
				$('#PopupShowPromotionProgramInfo #programType').text(Utils.XSSEncode(ppType));
				$('#PopupShowPromotionProgramInfo #programFormat').text(Utils.XSSEncode(ppFormat));
				$('#PopupShowPromotionProgramInfo #fromDatePP').text(Utils.XSSEncode(fromDate));
				$('#PopupShowPromotionProgramInfo #toDatePP').text(Utils.XSSEncode(toDate));
				$('#PopupShowPromotionProgramInfo #programDescription').text(Utils.XSSEncode(ppDescription));
	        	},
	        	onClose: function() {
	        		$('#PopupShowPromotionProgramInfo').dialog("destroy");
	        		$('#PopupShowPromotionProgramInfoDiv').html(html);
	        	}
		});
	},
	choosePromotionProgram: function(ppCode, pType) {
		var params = new Object();
		params.ppCode = ppCode;
		params.typeReturn = pType;
		Utils.getHtmlDataByAjax(params, '/sale-product/return-product/promotionprogram',
				function(data) {
				try {
					var _data = JSON.parse(data);
					if (_data != null && _data.errMsg == undefined) {
						SPReturnOrder.showPromotionProgramInfo(_data.ppCode, _data.ppName, _data.ppType, _data.ppFormat, _data.fromDate, _data.toDate, _data.ppDescription);
						if (_data.toDate) {
							$("#PopupShowPromotionProgramInfo #programDescription").prev().css("margin-top", "");
							$("#PopupShowPromotionProgramInfo #programDescription").css("margin-top", "");
						} else {
							$("#PopupShowPromotionProgramInfo #programDescription").prev().css("margin-top", "3px");
							$("#PopupShowPromotionProgramInfo #programDescription").css("margin-top", "3px");
						}
					}
				} catch (e) {
						SPReturnOrder.dialogBoxError(_data.errMsg);
					}
				}, 'loadingSearch', 'GET');
	},
	
	returnProductOrder: function (saleOrderId) {
		var paramsSaleOrderDetail = new Object();
		if (saleOrderId != null) {
			paramsSaleOrderDetail.saleOrderId = saleOrderId;
			paramsSaleOrderDetail.shopCode = $('#shopCodeCB').combobox('getValue');
			paramsSaleOrderDetail.flag = false;
			/*$.messager.confirm("Xác nhận", "Bạn có thật sự muốn trả đơn hàng này?", function(r) {
				if (r) {
					SPReturnOrder._returnOrder(paramsSaleOrderDetail);
				}
			});*/
		
			Utils.addOrSaveData(paramsSaleOrderDetail, '/sale-product/return-product/returnproductorder', null, 'errMsg', function (data){
				if(data.saleOrderNull == null && data.error == null){
					if (data.saleOrderId != null && data.saleOrderId != undefined) {
						$('#returnOrderNumber').val(data.saleOrderNumber);
						$('#saleOrderReturnId').val(data.saleOrderId);
						
						$("#btnReturn").attr("disabled", "disabled");
						$("#btnReturn").addClass("BtnGeneralDStyle");
						$("#btnReturn").removeAttr("onclick");
						
						$("#btnCancel").removeAttr("disabled");
						$("#btnCancel").removeClass("BtnGeneralDStyle");
						$("#btnCancel").attr("onclick", "SPReturnOrder.cancelReturnOrder("+Number(data.saleOrderId)+");");
					}
				} else {
					$('#errMsg').html('Đơn hàng không hợp lệ.').show();
					return;
				}
				
			}, 'loadingSearch', null, null, 'Bạn có muốn trả đơn hàng này?');
		}
	},
	_returnOrder: function(params) {
		Utils.saveData(params, "/sale-product/return-product/returnproductorder", null, "errMsg", function (data) {
			if (data.saleOrderNull == null && (data.error == null || !data.error)) {
				if (data.saleOrderId != null && data.saleOrderId != undefined) {
					$('#returnOrderNumber').val(data.saleOrderNumber);
					$('#saleOrderReturnId').val(data.saleOrderId);
					
					$("#btnReturn").attr("disabled", "disabled");
					$("#btnReturn").addClass("BtnGeneralDStyle");
					$("#btnReturn").removeAttr("onclick");
					
					$("#btnCancel").removeAttr("disabled");
					$("#btnCancel").removeClass("BtnGeneralDStyle");
					$("#btnCancel").attr("onclick", "SPReturnOrder.cancelReturnOrder("+Number(data.saleOrderId)+");");
				}
			} else {
				$('#errMsg').html('Đơn hàng không hợp lệ.').show();
				return;
			}
		});
	},
	getGridUrl: function(orderNumber,customerId,shortCode,startDate,endDate,staffCode,deliveryCode,orderTypeString,saleOrderStatusString,saleOrderTypeString){
		return "/sale-product/return-product/searchReturnCat?orderNumber=" + encodeChar(orderNumber) + "&customerId=" + customerId +  "&shortCode=" + encodeChar(shortCode)+
		"&startDate=" + encodeChar(startDate)+"&endDate=" + encodeChar(endDate)+"&staffCode=" + encodeChar(staffCode)+"&deliveryCode=" + encodeChar(deliveryCode)+
		"&orderTypeString=" + encodeChar(orderTypeString)+"&saleOrderStatusString=" + saleOrderStatusString +"&saleOrderTypeString="+ saleOrderTypeString;
	},
	openSearchBillDialog: function() {
		var html = $('#searchBillDialog').html();
		$('#popup1').dialog({
			title: "Tìm kiếm đơn hàng",
			closed: false,  
			cache: false,   
			modal: true,
			width: $(window).width()- 150,
			onOpen:function() {
				$('#popup1').addClass("easyui-dialog");
	        	$("#popup1 #orderNumber").parent().unbind("keyup");
	        	$("#popup1 #orderNumber").parent().bind("keyup", function(event) {
	        		if (event.keyCode == keyCodes.ENTER) {
	        			$("#popup1 #btnSearch").click();
	        		}
	        	});
	        	$('#popup1 .InputTextStyle').val('');
	    		$('#popup1 #errorSearchMsg').hide();
	    		var sysDate = $('#date').val();
	    		setDateTimePicker('popup1 #fromDate');
	    		$('#popup1 #fromDate').val(sysDate);
	    		setDateTimePicker('popup1 #toDate');
	    		$('#popup1 #toDate').val(sysDate);
	    		$('#popup1 #orderNumber').focus();

	    		var params = new Object();
	    		params = SPReturnOrder.getParam();
	    		params.flag = false;
	    		var url = '/sale-product/return-product/search';
	    		SPReturnOrder.initGridSearchBill(url, params);
	    		setTimeout(function(){
    				CommonSearchEasyUI.fitEasyDialog(); 
	    		},1000);
	    		$('.easyui-dialog .datagrid-pager').html('').hide();
        	},
        	onClose:function() {
        		$('#popup1').dialog("destroy");
        		$('#searchBillDialog').html(html);
        		$("#orderTypeDlgDiv").html('<select class="MySelectBoxClass" id="orderType">\
								<option value="IN" selected="selected">Presale</option>\
								<option value="SO">Vansale</option></select>');
        		$("#orderType").customStyle();
        	}
		});
		
		return false;
	},
	showCustomerInfoEasyUI: function(customerId){
		var html = $('#PopupShowCustomerInfoDiv').html();
		$('#PopupShowCustomerInfo').dialog({
			title: "Thông tin khách hàng",
			closed: false,  
	        	cache: false,   
	        	modal: true,
	        	width: 657,
	        	onOpen:function() {
	        		$('#PopupShowCustomerInfo').addClass("easyui-dialog");
	        		var getCustomerInfoUrl = '/sale-product/return-product/customer/get-info?customerId='+ customerId;
				$.getJSON(getCustomerInfoUrl, function(data) {
					if (data != null || data != undefined) {
						$('#PopupShowCustomerInfo #shortCodep').text(Utils.XSSEncode(data.customerShortCode));
						$('#PopupShowCustomerInfo #customerNamep').text(Utils.XSSEncode(data.customerName));
						$('#PopupShowCustomerInfo #addressp').text(Utils.XSSEncode(data.address));
						$('#PopupShowCustomerInfo #phonep').text(Utils.XSSEncode(data.phone));
						$('#PopupShowCustomerInfo #mobiphonep').text(Utils.XSSEncode(data.mobiphone));
						$('#PopupShowCustomerInfo #shopTypeNamep').text(Utils.XSSEncode(data.shopTypeName));
						$('#PopupShowCustomerInfo #loyaltyp').text(Utils.XSSEncode(data.loyalty));
						$('#PopupShowCustomerInfo #contactNamep').text(Utils.XSSEncode(data.contactName));
						$('#PopupShowCustomerInfo #totalInDatep').text(formatCurrency(Utils.XSSEncode(data.totalInDate)));
//						$('#numSkuInDate').text(data.numSkuInDate);
						$('#PopupShowCustomerInfo #numOrderInMonthp').text(Utils.XSSEncode(data.numOrderInMonth));
						$('#PopupShowCustomerInfo #avgTotalInLastTwoMonthp').text(formatCurrency(Utils.XSSEncode(data.avgTotalInLastTwoMonth)));
						$('#PopupShowCustomerInfo #totalInMonthp').text(formatCurrency(Utils.XSSEncode(data.totalInMonth)));
					}
				});
				var url = '/sale-product/return-product/customer/get-sale-order';
		    		$('#gridCustomerInfo').datagrid({
		  			url:url,
		  		  	pageList  : [10,20,30],
		  			width: $('#PopupShowCustomerInfo').width() - 25,
		  			pageSize : 10,
		  			scrollbarSize : 0,
		  			//pagination:true,			  
		  			fitColumns:true,		     
		  		      singleSelect:true,
		  			method : 'GET',
		  			rownumbers: true,
		  			autoRowHeight : true,
		  			queryParams:{customerId : customerId},
		  			columns:[[	
						{field:'orderDate',title:'Ngày',width:50, align:'center', sortable:false, resizable:false, formatter:function(value,row,index) {
							var date = new Date(row.orderDate);
							return $.datepicker.formatDate('dd/mm/yy',date);
						}},  
						{field:'orderNumber',title:'Số hóa đơn', align:'left',width:100, sortable:false,resizable:false},  
						{field:'amount',title:'Thành tiền',width:100,align:'right', formatter:function(value,row,index) {
							return formatCurrency(Utils.XSSEncode(row.amount));
						}} 
		  			]],
		  			onLoadSuccess:function(){
		  				$('.datagrid-header-rownumber').html('STT'); 
	//	  				$('#gridSearchBill').datagrid('resize');
		  				setTimeout(function(){
				    			CommonSearchEasyUI.fitEasyDialog();
				    		},1000);
		  			}
		  		});
	        	},
	        	onClose:function() {
	        		$('#PopupShowCustomerInfo').dialog("destroy");
	        		$('#PopupShowCustomerInfoDiv').html(html);
	        	}
		});
		
		return false;
	},
	
	searchCustomerEasyUIOnDialog : function(callback, arrParam) {
		SPReturnOrder.openSearchCustomerEasyUIExDialog("Mã KH", "Tên KH",
				"Tìm kiếm khách hàng", "/commons/customer-in-shop/search", callback,
				"shortCode", "customerName", arrParam);
		return false;
	},
	getResultSeachStyle1EasyUIDialog : function(rowIndex) {
		var grid = $('.easyui-dialog #searchCustomerGrid');
		grid.datagrid('unselectAll');
		grid.datagrid('selectRow',rowIndex);
		var row = grid.datagrid('getSelected');		
		var obj = {};
		obj.code = row[$('.easyui-dialog #searchCustomerCodeText').val()];
		obj.name = row[$('.easyui-dialog #searchCustomerNameText').val()];
		obj.id = row[$('.easyui-dialog #searchCustomerIdText').val()];
		if (CommonSearch._currentSearchCallback != null) {
			CommonSearch._currentSearchCallback.call(this, obj);
			$('#searchCustomerEasyUIDialog').dialog('close');
		}
		return false;
	},
	openSearchCustomerEasyUIExDialog : function(codeText, nameText, title, url, callback, codeFieldText, nameFieldText, arrParam,idFieldText,addressText, addressFieldText) {
		$('#searchCustomerEasyUIDialog #btnsearchCustomer').unbind('click');		
		CommonSearch._arrParams = null;
		CommonSearch._currentSearchCallback = null;
		var html = $('#searchCustomerEasyUIDialogDiv').html();
		$('#searchCustomerEasyUIDialog').dialog({  
	        	title: title,  
	        	closed: false,  
	        	cache: false,  
	        	modal: true,
	        	onOpen: function() {
	        		$('#searchCustomerEasyUIDialog').addClass("easyui-dialog");
					var tabindex = -1;
					$('.InputTextStyle, select, input, button , li , tr, td, th, label, ul, a, img').each(function () {
						if (this.type != 'hidden') {
							$(this).attr("tabindex", tabindex);
							tabindex -=1;
						}
					});								
					$('#searchCustomerEasyUIDialog #seachStyle1Code').focus();
					$('#searchCustomerEasyUIDialog #searchCustomerUrl').val(url);
					
					$('#searchCustomerEasyUIDialog #searchCustomerCodeText').val(codeFieldText);
					$('#searchCustomerEasyUIDialog #searchCustomerNameText').val(nameFieldText);
					$('#searchCustomerEasyUIDialog #searchCustomerIdText').val(idFieldText);
					if(addressText != null && addressText != undefined && addressFieldText != null && addressFieldText != undefined) {
						$('#searchCustomerEasyUIDialog #searchCustomerAddressText').val(addressFieldText);
						$('#searchCustomerEasyUIDialog #seachStyle1AddressLabel').show();
						$('#searchCustomerEasyUIDialog #seachStyle1Address').show();					
					}
					$('#searchCustomerEasyUIDialog #btnsearchCustomer').css('margin-left', 270);
					$('#searchCustomerEasyUIDialog #btnsearchCustomer').css('margin-top', 5);				
					CommonSearch._currentSearchCallback = callback;
					CommonSearch._arrParams = arrParam;
					var codeField = 'code';
					var nameField = 'name';
					var idField = 'id';
					if (codeFieldText != null && codeFieldText != undefined && codeFieldText.length > 0) {
						codeField = codeFieldText;
					}
					if (nameFieldText != null&& nameFieldText != undefined && nameFieldText.length > 0) {
						nameField = nameFieldText;
					}
					if (idFieldText != null&& idFieldText != undefined && idFieldText.length > 0) {
						idField = idFieldText;
					}
					Utils.bindAutoSearch();
					$('#searchCustomerEasyUIDialog #seachStyle1CodeLabel').html(codeText);
					$('#searchCustomerEasyUIDialog #seachStyle1NameLabel').html(nameText);
	//				Utils.bindAutoSearch();
					$('#searchCustomerEasyUIDialog #searchCustomerGrid').show();
					var params = {};
					for (var i in arrParam) {
						params[arrParam[i].name] = arrParam[i].value;
					}
					params.page=1;///
					$('#searchCustomerEasyUIDialog #searchCustomerGrid').datagrid({
						url: url,
						queryParams: params,
						autoRowHeight : true,
						rownumbers : false, 
						checkOnSelect :true,
						pagination:true,
						rowNum : 10,
						pageSize:10,
						scrollbarSize : 0,
						singleSelect:true,
						pageNumber:1,
						fitColumns:true,
						width : ($('#searchCustomerEasyUIDialog').width() - 40),
					    columns:[[
							{field:"no", title:"STT", sortable: false, resizable: false, width:45, fixed:true, align:"center", formatter:function(v, r, i) {
								var p = $('#searchCustomerEasyUIDialog #searchCustomerGrid').datagrid("options").pageNumber;
								var n = $('#searchCustomerEasyUIDialog #searchCustomerGrid').datagrid("options").pageSize;
								return (Number(p) - 1) * Number(n) + i + 1;
							}},
				        	{field:codeField,title:codeText,align:'left', width:100, sortable : false,resizable : false, formatter: function(v, r, i){
					        	if (v) {
					        		return Utils.XSSEncode(v);
					        	}
					        	return "";
				        	}},  
				        	{field:nameField,title:nameText,align:'left', width:200, sortable : false,resizable : false, formatter: function(v, r, i) {
					        	if (v) {
					        		return Utils.XSSEncode(v);
					        	}
					        	return "";
				        	}},				        
				        	{field:'select', title:'Chọn', width:55, fixed:true, align:'center',sortable : false,resizable : false,formatter : CustomerSearchEasyUIFormatter.selectCellIconFormatterEasyUIDialog},
				        	{field :idField,hidden : true},
					    ]],
					    onLoadSuccess :function(){
					    	 tabindex = 1;
				    		 $('.easyui-dialog input,.easyui-dialog select,.easyui-dialog button').each(function () {
				    		 	if (this.type != 'hidden') {
					    	     		$(this).attr("tabindex", '');
								tabindex++;
				    		 	}
							});
			    		 	setTimeout(function(){
				    			CommonSearchEasyUI.fitEasyDialog();
				    		},1000);
					    }
					});
					$('#searchCustomerEasyUIDialog #btnsearchCustomer').unbind('click');
					$('#searchCustomerEasyUIDialog #btnsearchCustomer').bind('click', function(event) {
						if(!$(this).is(':hidden')){
							var code = $('#searchCustomerEasyUIDialog #seachStyle1Code').val().trim();
							var name = $('#searchCustomerEasyUIDialog #seachStyle1Name').val().trim();
							var address = $('#searchCustomerEasyUIDialog #seachStyle1Address').val().trim();
							var params = new Object();
							params.code=code;
							params.name= name;
							params.address = address;
							$('#searchCustomerEasyUIDialog #searchCustomerGrid').datagrid('load',params);						
							$('#searchCustomerEasyUIDialog #seachStyle1Code').focus();
						}					
					});
				
					$('#searchCustomerEasyUIDialog #seachStyle1Code').unbind('keyup');
					$('#searchCustomerEasyUIDialog #seachStyle1Code').bind('keyup', function(e) {	
						if(e.keyCode == keyCodes.ENTER) {
							$('#searchCustomerEasyUIDialog #btnsearchCustomer').click();
						}
					});
				 
				 	$('#searchCustomerEasyUIDialog #seachStyle1Name').unbind('keyup');
				 	$('#searchCustomerEasyUIDialog #seachStyle1Name').bind('keyup', function(e) {	
					 	if(e.keyCode == keyCodes.ENTER) {
						 	$('#searchCustomerEasyUIDialog #btnsearchCustomer').click();
					 	}
				 	});
	        	},
	        	onBeforeClose: function() {
					var tabindex = 1;
					$(' .InputTextStyle, select, input, button , li , tr, td, th, label, ul, a, img').each(function () {
						if (this.type != 'hidden') {
							$(this).attr("tabindex", '');
						}
						tabindex ++;
					});
	        	},
	        	onClose : function(){
	        		$('#searchCustomerEasyUIDialog').dialog("destroy");
		        	$('#searchCustomerEasyUIDialogDiv').html(html);
		        	//$('#searchCustomerEasyUIDialogDiv').hide();
		        	//$('#searchCustomerContainerGrid').html('<table id="searchCustomerGrid" class="easyui-datagrid" style="width: 520px;"></table><div id="searchCustomerPager"></div>');
		        	//$('#searchCustomerEasyUIDialog #seachStyle1Code').val('');
		        	//$('#searchCustomerEasyUIDialog #seachStyle1Name').val('');
		        	//$('#searchCustomerEasyUIDialog #seachStyle1Address').val('');	        	
	        	}
	    });
		return false;
	},
	
	cancelReturnOrder: function(soId) {
		if (soId != null) {
			var params = {saleOrderId: soId, shopCode: $('#shopCodeCB').combobox('getValue')};
			Utils.addOrSaveData(params, '/sale-product/return-product/cancel-returnOrder', null, 'errMsg', function (data){
				if (!data.error) {
					setTimeout(function() {
						window.location.href = "/sale-product/return-product/info";
					}, 3500);
				} else {
					if (data.errMsg) {
						$('#errMsg').html(errMsg).show();
					} else {
						$('#errMsg').html('Đơn hàng không hợp lệ.').show();
					}
					return;
				}
				
			}, 'loadingSearch',null,null,'Bạn có muốn hủy đơn trả hàng không?');
		}
	},

	/**
	 * Khoi tao grid, do du lieu cho grid san pham ban
	 *
	 * @author lacnv1
	 * @since Mar 26, 2015
	 */
	loadProductGrid: function(lstData) {
		$("#productH2").next().show();
		$("#productH2").show();
		if (lstData == undefined || lstData == null) {
			lstData = [];
		}
		var gridData = {
			total: lstData.length,
			rows: lstData//,
			//footer: [{isFooter: true, productName:"Tổng", quantity:formatFloatValue(w, 2)}]
		};
		var wd = screen.width || $(window).width();
		if ($("#gridContainer .datagrid-view").length == 0) { // chua co grid, khoi tao moi
    		SPReturnOrder.initProductGrid(gridData, wd)
		} else {
			$("#productGrid").datagrid("loadData", gridData);
		}
		if (lstData.length == 0) {
			$("#productH2").next().hide();
			$("#productH2").hide();
		}
	},

	/**
	 * Khoi tao grid, do du lieu cho grid san pham khuyen mai
	 *
	 * @author lacnv1
	 * @since Mar 26, 2015
	 */
	loadPromotionGrid: function(lstData, gridId, h2Id, containerId) {
		if (gridId == null || gridId == undefined) {
			gridId = "promotionGrid";
		}
		if (h2Id == null || h2Id == undefined) {
			h2Id = "#promotionH2";
		} else {
			h2Id = "#"+h2Id;
		}
		if (containerId == null || containerId == undefined) {
			containerId = "#promotionContainer";
		} else {
			containerId = "#"+containerId;
		}
		$(h2Id).next().show();
		$(h2Id).show();
		if (lstData == undefined || lstData == null) {
			lstData = [];
		}
		var gridData = {
			total: lstData.length,
			rows: lstData//,
			//footer: [{isFooter: true, productName:"Tổng", quantity:formatFloatValue(w, 2)}]
		};
		var wd = screen.width || $(window).width();
		if ($(containerId+" .datagrid-view").length == 0) { // chua co grid, khoi tao moi
			$("#"+gridId).datagrid({
				//showFooter: true,
				singleSelect: true,
				width: wd - 40,
				fitColumns: true,
				scrollbarSize: 0,
				autoRowHeight: true,
				rownumbers: false,
				data: gridData,
				columns: [[
					//GridUtil.getRowNumField2(gridId),
					{field:"rowno", title:"STT", sortable: false, resizable: false, fixed:true, align:"center", formatter:function(v, r, i) {
						if (r.programTypeCode == "ZV23") {
							return r.rowNo;
						}
						var p = $('#'+gridId).datagrid("options").pageNumber;
						var n = $('#'+gridId).datagrid("options").pageSize;
						if (p == undefined || p == null || n == undefined || n == null) {
							return (i + 1);
						}
						return (Number(p) - 1) * Number(n) + i + 1;
					}},
					GridUtil.getField("productCode", "Mã sản phẩm", 100, "left", function(v, r, i) {
						if (v == undefined || v == null) {
							if (r.programTypeCode == "ZV19" || r.programTypeCode == "ZV20") {
								return "Khuyến mãi đơn hàng";
							}
							if (r.programTypeCode == "ZV23") {
								return "Khuyến mãi tích lũy";
							}
							return "";
						}
						if (r.convfact) {
							return "<a href='javascript:void(0);' onclick='SPReturnOrder.showProductInfoDlg(\""+Utils.XSSEncode(v)+"\", 0);'>"+Utils.XSSEncode(v)+"</a>";
						}
						return v;
//					}},
					}),
					GridUtil.getNormalField("productName", "Tên sản phẩm", 200, "left"),
					//GridUtil.getNormalField("price", "Đơn giá", 100, "right"),
					GridUtil.getField("warehouseName", "Kho", 150, "left", function(v, r, i) {
						if (r.convfact) {
							return Utils.XSSEncode(v);
						}
						if (r.discountAmount != undefined && r.discountAmount != null) {
							return "<span><span style='float:left;margin-left:5px;'>"
								+ formatCurrency(r.maxAmountFree) + "</span><span style='float:right;'>("
								+ formatCurrency(r.discountAmount) + ")</span></span>";
						}
						return "";
					}),
					//GridUtil.getNormalField("availableQuantity", "Tồn kho đáp ứng", 120, "right"),
					GridUtil.getField("quantity", "Tổng", 100, "right", function(v, r, i) {
						if (v != undefined && v != null) {
							return formatQuantity(v, r.convfact) + "&nbsp;&nbsp;(" + formatQuantity(r.maxQuantityFree, r.convfact) + ")";
						}
						return "";
					}),
					GridUtil.getField("quantityReceived", "Số suất", 55, "right", function(v, r, i) {
						if (v != undefined && v != null) {
							return v + "&nbsp;&nbsp;(" + Utils.XSSEncode(r.maxQuantityReceived) + ")";
						}
						return "";
					}),
					//GridUtil.getNormalField("discountAmount", "CKMH", 100, "right"),
					GridUtil.getField("programCode", "Khuyến mãi", 100, "left", function(v, r, i) {
						if (v == undefined || v == null) {
							return "";
						}
						var t = r.progamType;
						if (t == undefined || t == null) {
							t = "";
						}
						return "<a href='javascript:void(0);' onclick='SPReturnOrder.choosePromotionProgram(\""+Utils.XSSEncode(v)+"\", \""+Utils.XSSEncode(t)+"\");'>"
								+Utils.XSSEncode(v)+"</a>";
//					}}
					})
				]],
				onLoadSuccess: function(data) {
					GridUtil.updateRowNumWidth(containerId, gridId);
					SPReturnOrder.mergePromotionProductGrid(gridId);
				}
			});
		} else {
			$("#"+gridId).datagrid("loadData", gridData);
		}
		if (lstData.length == 0) {
			$(h2Id).next().hide();
			$(h2Id).hide();
		}
	},

	/**
	 * Gom nhom san pham khuyen mai
	 *
	 * @author lacnv1
	 * @since Mar 28, 2015
	 */
	mergePromotionProductGrid: function(gridName) {
		if (gridName == undefined || gridName == null) {
			gridName = "#promotionGrid";
		} else {
			gridName = "#"+gridName;
		}
		var rows = $(gridName).datagrid('getRows');
		var r = null;
		var mapMerge = new Map();
		var lstMergeCode = [];
		var lstMergeAmount = [];
		var mapRNMerge = new Map();
		for (var i = 0, sz = rows.length; i < sz; i++) {
			r = rows[i];
			if (r.programTypeCode == "ZV23") { // khuyen mai tich luy
				var cell = mapMerge.get(r.programCode+"_"+r.productGroupId+"_"+r.levelPromo);
				if (cell != undefined && cell != null) {
					cell.rowspan++;
				} else {
					cell = {index:i, rowspan:1};
				}
				mapMerge.put(r.programCode+"_"+r.productGroupId+"_"+r.levelPromo, cell);
				if (r.convfact == null || r.convfact == undefined || !Number(r.convfact)) {
					lstMergeCode.push(i);
					lstMergeAmount.push(i);
				}
				cell = mapRNMerge.get(r.rowNo);
				if (cell != undefined && cell != null) {
					cell.rowspan++;
				} else {
					cell = {index:i, rowspan:1};
				}
				mapRNMerge.put(r.rowNo, cell);
			} else if (r.convfact != null && r.convfact != undefined && Number(r.convfact)) {
				var cell = mapMerge.get(r.programCode+"_"+r.productGroupId+"_"+r.levelPromo);
				if (cell != undefined && cell != null) {
					cell.rowspan++;
				} else {
					cell = {index:i, rowspan:1};
				}
				mapMerge.put(r.programCode+"_"+r.productGroupId+"_"+r.levelPromo, cell);
			} else {
				lstMergeCode.push(i);
				lstMergeAmount.push(i);
			}
		}
		var lstMerge = mapMerge.valArray;
		for (var i = 0, sz = lstMerge.length; i < sz; i++) {
			$(gridName).datagrid('mergeCells', {
	                	index: lstMerge[i].index,
	                	field: 'quantityReceived',
	                	rowspan: lstMerge[i].rowspan
	            });
			$(gridName).datagrid('mergeCells', {
	                	index: lstMerge[i].index,
	                	field: 'programCode',
	               	rowspan: lstMerge[i].rowspan
	            });
	      }
	      for (var i = 0, sz = lstMergeCode.length; i < sz; i++) {
			$(gridName).datagrid('mergeCells', {
	                	index: lstMergeCode[i],
	                	field: 'productCode',
	                	colspan: 2
	            });
	      }
	      for (var i = 0, sz = lstMergeAmount.length; i < sz; i++) {
			$(gridName).datagrid('mergeCells', {
	                	index: lstMergeAmount[i],
	                	field: 'warehouseName',
	                	colspan: 2
	            });
	      }
	      lstMerge = mapRNMerge.valArray;
		for (var i = 0, sz = lstMerge.length; i < sz; i++) {
			$(gridName).datagrid('mergeCells', {
	                	index: lstMerge[i].index,
	                	field: 'rowno',
	                	rowspan: lstMerge[i].rowspan
	            });
	      }
	},

	/**
	 * Xem thong tin chi tiet san pham
	 *
	 * @author lacnv1
	 * @since Mar 28, 2015
	 */
	showProductInfoDlg: function(productCode, isSaleProduct) {
		showLoadingIcon();
		$(".ErrorMsgStyle").hide();
		if (productCode && productCode.trim().length > 0) {
			var url='/commons/products-details?productCode='+productCode+'&customerCode='+$("#customerCodeHeader").text().trim()
					+'&salerCode='+$("#staffCodeHeader").text().trim() + "&orderId="+ $("#returnOrderId").val().trim() + "&shopCodeFilter="+ $("#shopCodeCB").combobox('getValue');
			$.getJSON(encodeURI(url), function(data) {
				hideLoadingIcon();
				if (!data.error) {
					/*var isSaleProduct1 = (isSaleProduct == 1);
					CommonSearch.showProductInfo(data.product.productCode, data.product.productName, 
							data.product.convfact,
							formatQuantityEx(data.avaiableQuantity, data.product.convfact),
							data.price, data.avaiableQuantity, data.promotion, isSaleProduct1);*/
					SPReturnOrder.showProductInfo(data.product.productCode, data.product.productName,
						data.product.convfact, formatQuantityEx(data.avaiableQuantity, data.product.convfact),
						data.price, data.avaiableQuantity, data.promotion);
				}
			});
		}
	},

	/**
	 * Khoi tao grid, load san pham khuyen mai tich luy
	 *
	 * @author lacnv1
	 * @since Mar 30, 2015
	 */
	loadAccumlationGrid: function(lstData) {
		if (lstData == undefined || lstData == null) {
			lstData = [];
		}
		var r = null;
		var p = null;
		var pn = -1;
		var pCode = null;
		var stt = 1;
		for (var i = 0, sz = lstData.length; i < sz; i++) {
			r = lstData[i];
			if (pCode != r.programCode) {
				pn = -1;
				pCode = r.programCode;
			}
			if (pn != r.payingOrder) {
				r.rowNo = stt;
			} else {
				r.rowNo = lstData[i-1].rowNo;
			}
			if (r.convfact && pn != r.payingOrder) {
				pn = r.payingOrder;
				p = {
					productCode: "Khuyến mãi tích lũy (lần " + pn + ")",
					programCode: r.programCode,
					productGroupId: r.productGroupId,
					levelPromo: r.levelPromo,
					programTypeCode: "ZV23",
					payingOrder: pn,
					rowNo: stt
				};
				lstData.splice(i, 0, p);
				sz = lstData.length;
				i++;
				stt++;
			} else if (!r.convfact) {
				stt++;
			}
		}
		SPReturnOrder.loadPromotionGrid(lstData, "accPromoGrid", "accPromoH2", "accPromoContainer");
	},
	//load danh sach don vi
	initUnitCbx: function(){	
		Utils.initUnitCbx('shopCodeCB', null, 206, function() {
			SPReturnOrder.onSelectShopCodeCombobox();
		});
	},
	
	onSelectShopCodeCombobox: function() {
		$('#returnOrderNumber').val("");
		$('#poCode').val("");
		$('#customerCodeHeader').text("");
		$('#customerNameHeader').text("");
		$('#customerAddressHeader').text("");
		$('#staffCodeHeader').text("");
		$('#deliveryCodeHeader').text("");
		$('#phoneHeader').text("");
		$('#deliveryDateHeader').text("");
		$('#carNumberHeader').text("");
		
		$('#gridContainer').html('<div id="productGrid"></div>');
		$('#promotionH2').hide();
		$('#promotionContainer').html('<div id="promotionGrid"></div>');
		$('#openPromoH2').hide();
		$('#openPromoContainer').html('<div id="openPromoGrid"></div>');
		$('#orderPromoH2').hide();
		$('#orderPromoContainer').html('<div id="orderPromoGrid"></div>');
		$('#accPromoH2').hide();
		$('#accPromoContainer').html('<div id="accPromoGrid"></div>');
		
		$('#pTotalWeight').text("0.0 kg");
		$('#pAmount').text("0 VNĐ");
		$('#pDiscount').text("0 VNĐ");
		$('#pTotal').text("0 VNĐ");
		
		$("#btnCancel").attr("disabled", "disabled");
		$("#btnCancel").addClass("BtnGeneralDStyle");
		$("#btnReturn").attr("disabled", "disabled");
		$("#btnReturn").addClass("BtnGeneralDStyle");
		
		var shopCode = $('#shopCodeCB').combobox('getValue');
		var params = new Object();
		if(shopCode != null){
			params.shopCode = shopCode;
		}
		var kData = $.param(params, true);
		 $.ajax({
			type : "POST",
			url : '/sale-product/return-product/select-shop-code',
			data : (kData),
			dataType : "json",
			success : function(data) {
				SPReturnOrder._isAllowShowPrice = data.isAllowShowPrice;
				if(SPReturnOrder._isAllowShowPrice){
					$('#sumPrice').show();
				} else {
					$('#sumPrice').hide();
				}
				if (data.fromDate != null && data.fromDate != undefined) {
					$('#fromDate').val(data.fromDate);
					$('#toDate').val(data.fromDate);
					$('#date').val(data.fromDate);
					$('#strLockDate').text(data.fromDate);
				}
			},
			error:function(XMLHttpRequest, textStatus, errorThrown) {
//				console.log("Error");
			}
		});

	},
	
	initGridSearchBill: function(url, params) {
		$('#gridSearchBill').datagrid({
  			url: url,
  			view: bufferview,	  
  			fitColumns: true,		     
  			singleSelect: true,
  			method : 'GET',
  			autoRowHeight : false,
  			height: 200,
  			queryParams: params,
  			width: $(window).width()- 180,
  			frozenColumns:[[
				{field: 'rownum', title: 'STT', width: 35, sortable:false, resizable:false, align: 'center', formatter:function(value,row,index) {
				  	return (index+1);
				}},
				{field:'select', title:'Chọn', width:40, align:'center', sortable : false, resizable : false, formatter : function(value, row, index) {
	  				return "<a href='javascript:void(0)' class='cmsdefault' onclick=\"return SPReturnOrder.choosePOCode(" + index + ");\"><img width='16' height='16' src='/resources/images/icon_edit.png'/></a>";
	  			}},
				{field: 'orderNumber', title: 'Số ĐH', width: 100, sortable:false, resizable:false, align: 'left'},
				{field: 'orderDate', title: 'Ngày', width: 80, sortable:false, resizable:false, align: 'center', formatter:function(value,row,index) {
					if(row.orderDate==null) return '';
						return $.datepicker.formatDate('dd/mm/yy', new Date(row.orderDate));
				}},
				{field: 'customer.shortCode', title: 'Khách hàng', width: 200, sortable:false, resizable:false, align: 'left', formatter: function(value,row, index) {
				  	return (row.customer == null) ? "" : Utils.XSSEncode(row.customer.shortCode + " - " + row.customer.customerName);
				}}
  			]],             
  			columns:[[
  			    {field: 'customer.address', title: 'Địa chỉ', width: 200, sortable:false, resizable:false, align: 'left', formatter: function(value,row, index) {
  			    	return (row.customer == null) ? "" : Utils.XSSEncode(row.customer.address);
  			    } },	  			    
  			    {field: 'staff', title: 'NVBH', width: 80, sortable:false, resizable:false, align: 'left', formatter: function(value,row, index) {
  			    	return (row.staff == null) ? "" : Utils.XSSEncode(row.staff.staffCode);
  			    } },
  			    {field: 'delivery', title: 'NVGH', width: 80, sortable:false, resizable:false, align: 'left',formatter: function(value,row, index) {
  			    	return (row.delivery == null) ? "" : Utils.XSSEncode(row.delivery.staffCode);
  			    } },
  			    {field: 'quantity', title: 'Sản lượng', width: 50, sortable:false, resizable:false, align: 'right', formatter: function(value,row, index) {
			    	return (row.quantity == null) ? "" : formatCurrencyInterger(Utils.XSSEncode(row.quantity));
			    } },
  			    {field: 'total', title: 'Thành tiền', width: 80, sortable:false, resizable:false, align: 'right', formatter: function(value,row, index) {
  			    	return (row.total == null) ? "" : formatCurrencyInterger(Utils.XSSEncode(row.total));
  			    } }
  			]],
  			onLoadSuccess: function(data) {
				if (SPReturnOrder._isAllowShowPrice) {
		    		$('#gridSearchBill').datagrid('showColumn', 'total');
		    	} else {
		    		$('#gridSearchBill').datagrid('hideColumn', 'total');
		    	}
			}
  		});
	},
	
	initProductGrid: function(gridData, wd) {
		$("#productGrid").datagrid({
			//showFooter: true,
			singleSelect: true,
			width: wd - 40,
			fitColumns: true,
			scrollbarSize: 0,
			autoRowHeight: true,
			rownumbers: true,
			data: gridData,
			columns: [[
				GridUtil.getField("productCode", "Mã sản phẩm", 100, "left", function(v, r, i) {
					return "<a href='javascript:void(0);' onclick='SPReturnOrder.showProductInfoDlg(\""+Utils.XSSEncode(v)+"\", 1);'>"+Utils.XSSEncode(v)+"</a>";
				}),
				GridUtil.getNormalField("productName", "Tên sản phẩm", 200, "left"),
				GridUtil.getField("price", "Đơn giá", 100, "right", function(v, r, i) {
					return r.packagePrice + "/" + r.price;
				}),
				GridUtil.getNormalField("warehouseName", "Kho", 150, "left"),
				//GridUtil.getNormalField("availableQuantity", "Tồn kho đáp ứng", 120, "right"),
				GridUtil.getField("quantityPackage", "Thực đặt", 100, "right", function(v, r, i) {
//					return formatQuantity(r.quantity, r.convfact);
					return r.quantityPackage + "/" + r.quantityRetail;
				}),
				GridUtil.getField("amount", "Thành tiền", 120, "right", function(v, r, i) {
					return formatCurrency(r.quantity * r.price);
				}),
				GridUtil.getNormalField("discountAmount", "CKMH", 100, "right"),
				GridUtil.getField("programCode", "CTHTTM", 100, "left", function(v, r, i) {
					if (v == undefined || v == null) {
						return "";
					}
//					var t = r.progamType;
//					if (t == undefined || t == null) {
//						t = "";
//					}
//					return "<a href='javascript:void(0);' onclick='SPReturnOrder.choosePromotionProgram(\""+Utils.XSSEncode(v)+"\", \""+Utils.XSSEncode(t)+"\");'>"
//						+Utils.XSSEncode(v)+"</a>";

					var lst = v.split(',');
					html = '<a class="delIcons" onclick="return SPCreateOrder.showPromotionProgramDetail(\'' + lst[0].trim() + '\');" href="javascript:void(0)">' + lst[0] + '</a>';
					for (var i = 1 ; i < lst.length ; i++) {
						html += ', <a class="delIcons" onclick="return SPCreateOrder.showPromotionProgramDetail(\'' + lst[i].trim() + '\');" href="javascript:void(0)">' + lst[i] + '</a>';
					}
					return html;
				})
			]],
			onLoadSuccess: function(data) {
				GridUtil.updateDefaultRowNumWidth("#gridContainer", "productGrid");
				if (SPReturnOrder._isAllowShowPrice) {
		    		$('#productGrid').datagrid('showColumn', 'price');
		    		$('#productGrid').datagrid('showColumn', 'amount');
		    		$('#productGrid').datagrid('showColumn', 'discountAmount');
		    	} else {
		    		$('#productGrid').datagrid('hideColumn', 'price');
		    		$('#productGrid').datagrid('hideColumn', 'amount');
		    		$('#productGrid').datagrid('hideColumn', 'discountAmount');
		    	}
			}
		});
	}	
};
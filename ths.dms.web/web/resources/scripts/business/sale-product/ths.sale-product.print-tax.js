/**
 * in hóa đơn thuế GTGT
 */
var SPPrintTax = {
		pdfBase64:null,
		scale:null,
		_map:null,
		buyer:null,
		stockStaff:null,
		selStaff:null,
		_textChangeBySeach: {
			all: '',
			max: '',
			min: '',
			avg: ''
		},
		initInfo: function() {
			Utils.initUnitCbx('cbxShop', {}, null, function(){
				SPPrintTax.handleSelectShop();
			});//230
		},
		handleSelectShop: function() {
			var params= {};
			params.shopCode = $('#cbxShop').combobox('getValue');
			Utils.getJSONDataByAjax(params, '/sale-product/assign-transfer-staff/lst-staff-cbx' , function(data) {
				if (data != null) {
					$('#strLockDate').html(data.lockDate);
					$('#datepickerFromDate').val(data.lockDate);
					$('#datepickerToDate').val(data.lockDate);
					Utils.bindStaffCbx('#saleStaff', data.lstSale, null, 206);			
				}
			});
			
		},
		getGridUrl: function(data){	
			var url = "/sale-product/print-tax/search?taxValueString=" + encodeChar(data.taxValueString);
			url += "&fromDate=" + encodeChar(data.fromDate);
			url += "&toDate=" + encodeChar(data.toDate);
			url += '&orderNumber='+ data.orderNumber;
			url += '&saleStaff=' + encodeChar(data.saleStaff);
			url += '&shortCode=' + data.shortCode;
			url += '&addressCus=' + data.addressCus;
			url += '&status=' + data.status;
			url += '&custPayment=' + data.custPayment;
			url += '&isValueOrder=' + data.isValueOrder;
			url += '&shopCodeSearch=' + data.shopCodeSearch;
			return url;
		},	
		getParams:function(){			
			var data = new Object();
			data.taxValueString = $('#taxValue').val().trim();
			data.fromDate = $('#datepickerFromDate').val().trim();
			data.toDate = $('#datepickerToDate').val().trim();
			data.orderNumber = encodeChar($('#orderNumber').val().trim());
			var saleStaff = '';
			if($('#saleStaff').combobox('getValue') != undefined && $('#saleStaff').combobox('getValue') != null ){
				saleStaff = $('#saleStaff').combobox('getValue').trim();
			}
			data.saleStaff = saleStaff;
			data.shortCode = encodeChar($('#shortCode').val());
			data.custPayment = encodeChar($('#custPayment').val().trim());	
			data.addressCus = encodeChar($('#addressCus').val().trim());
			data.status = encodeChar($('#status').val().trim());
			data.isValueOrder = $('#isValueOrder').val().trim();
			if ($('#numberValueOrder').val()!= null && $('#numberValueOrder').val() !='' && $('#numberValueOrder').val() != undefined){
				data.numberValueOrder = $('#numberValueOrder').val().trim();
			}	
			return data;
		},
		getTaxSearch:function(){
			$('#errMsg').html('').hide();
			var msg='';
			msg = Utils.getMessageOfRequireCheck('datepickerFromDate','từ ngày');
			if(msg.length == 0){
				msg = Utils.getMessageOfRequireCheck('datepickerToDate','đến ngày');
			}
			if(msg.length == 0 && $('#datepickerFromDate').val().trim() == '__/__/____' ){
				msg='Bạn chưa nhập giá trị cho trường từ ngày';
			}
			if(msg.length == 0 && $('#datepickerToDate').val().trim() == '__/__/____'){
				msg='Bạn chưa nhập giá trị cho trường đến ngày';
			}
			var fDate = $('#datepickerFromDate').val();
			var tDate = $('#datepickerToDate').val();
			if(msg.length == 0){
				if (fDate != '' && !Utils.isDate(fDate)) {
					msg = 'Từ ngày không hợp lệ, vui lòng nhập theo định dạng dd/MM/yyyy';
					$('#datepickerFromDate').focus();
				}
			}
			if(msg.length == 0){
				if(tDate != '' && !Utils.isDate(tDate)){
					msg = 'Đến ngày không hợp lệ, vui lòng nhập theo định dạng dd/MM/yyyy';
					$('#datepickerToDate').focus();
				}
			}
			if(msg.length == 0){
				if(fDate != '' && tDate != ''){
					if(!Utils.compareDate(fDate, tDate)){
						msg = 'Từ ngày phải là ngày trước hoặc cùng ngày với đến ngày';	
						$('#datepickerFromDate').focus();
					}
				}
			}
			if($('#numberValueOrder').val() < 0){
				msg = 'Giá trị ĐH không được âm!';	
				$('#numberValueOrder').focus();
			}
			if(msg.length > 0){
				$('#errMsg').html(msg).show();
				return false;
			} 		
			var params= new Object();
			params = SPPrintTax.getParams();
			if ($('#cbxShop').combobox('getValue') != undefined && $('#cbxShop').combobox('getValue') != null) {
				params.shopCodeSearch = $('#cbxShop').combobox('getValue');
			}
			var url = SPPrintTax.getGridUrl(params);
			SPPrintTax._map = new Map();
			if($('#status').val().trim() == 1 || $('#status').val().trim() == 0){
				$("#grid").datagrid({
					url:url,
			        rownumbers : true,
			        pageNumber : 1,
			        scrollbarSize: 0,		      
			        autoRowHeight : true,		
			        pageList  : [10,20,30,50,100,200,300,500,1000],
			        pagination:true,
			        checkOnSelect :true,
			        width: $(window).width() - 40,
					height:'auto',
					frozenColumns:[[
						{field: 'id',checkbox:true, width: 20, align: 'center',sortable:false,resizable:false},
						{field: 'orderNumber', title: 'Hóa đơn nội bộ', width: 90, sortable:false,resizable:false, align: 'left', formatter:function(v, r, i) {
							return Utils.XSSEncode(v);
						}},
						{field: 'invoiceNumber', title: 'Hóa đơn đỏ', width: 80, sortable:false,resizable:false, align: 'left', formatter:function(v, r, i) {
							return Utils.XSSEncode(v);
						}},
						{field: 'customerCode', title: 'Khách hàng', width: 150, sortable:false,resizable:false, align: 'left', formatter:function(value,rowData,rowIndex){
							return Utils.XSSEncode(rowData.customerCode + ' - ' + rowData.custName);
						}},
						{field: 'invoiceDate', title: 'Trạng thái', width: 50, sortable:false,resizable:false, align: 'left',formatter:function(value,rowData,rowIndex){
							if(rowData.invoiceDate != null && rowData.invoiceDate != undefined) {
								return 'Đã in';
							} else {
								return 'Chưa in';
							}
						}}
					]],
					columns:[[
					    {field: 'customerAddr', title: 'Địa chỉ', width: 250, sortable:false,resizable:false, align: 'left', formatter:function(v, r, i) {
							return Utils.XSSEncode(v);
						}},
					    {field: 'staff', title: 'NVBH', width: 100, sortable:false,resizable:false, align: 'left',formatter:function(value,rowData,rowIndex){
					    	if(rowData.staff != null){
					    		return Utils.XSSEncode(rowData.staff.staffCode);
					    	}
					    }},
					    {field: 'deliveryStaff', title: 'NVGH', width: 100, sortable:false,resizable:false, align: 'left',formatter:function(value,rowData,rowIndex){
					    	if(rowData.deliveryStaff != null){
					    		return Utils.XSSEncode(rowData.deliveryStaff.staffCode);
					    	}
					    }},
					    {field: 'amount', title: 'Thành tiền', width: 100, sortable:false,resizable:false, align: 'right',formatter:function(value,rowData,rowIndex){
					    	return formatCurrency(rowData.amount);
					    }},
					    {field: 'custPayment', title: 'Thanh toán', width: 100, sortable:false,resizable:false, align: 'left',formatter:function(value,rowData,rowIndex){
							if(rowData.custPayment == 'MONEY'){ 
								return 'Tiền mặt';
							}else if(rowData.custPayment == 'BANK_TRANSFER'){
								return 'Chuyển khoản';
							}
						}},
					    {field: 'orderDate', title: 'Ngày', width: 90, sortable:false,resizable:false, align: 'center',formatter:function(value,rowData,rowIndex){
					    	return $.datepicker.formatDate('dd/mm/yy', new Date(rowData.orderDate));
					    }},				    
					    {field: 'vat', title: 'Thuế suất', width: 60, sortable:false,resizable:false, align: 'right',formatter:function(value,rowData,rowIndex){
					    	return rowData.vat + '%';
					    }}
					]],
					onCheck : function(rowIndex, rowData) {
						var selectedId = rowData['id'];
						SPPrintTax._map.put(selectedId,rowData);
					},
					onUncheck : function(rowIndex, rowData) {
						var selectedId = rowData['id'];
						SPPrintTax._map.remove(selectedId);
					},
					onCheckAll : function(rows) {
						if($.isArray(rows)) {
							for(var i = 0; i < rows.length; i++) {
								var row = rows[i];
								var selectedId = row['id'];
								SPPrintTax._map.put(selectedId, row);
					    	}
					    }
					},
					onUncheckAll : function(rows) {
						if($.isArray(rows)) {
							for(var i = 0; i < rows.length; i++) {
								var row = rows[i];
								var selectedId = row['id'];
								SPPrintTax._map.remove(selectedId);
					    	}
					    }
					},
					onLoadSuccess:function(){
						$('#gridContainer .datagrid-header-rownumber').html('STT');
						$('#grid').datagrid('resize');
					}
				});
			}else{
				$("#grid").datagrid({
					url:url,
			        rownumbers : true,
			        pageNumber : 1,
			        scrollbarSize: 0,		      
			        autoRowHeight : true,		
			        pageList  : [10,20,30,100,200,300,500,1000],
			        pagination:true,
			        checkOnSelect :true,
			        width: $(window).width() - 40,
					height:'auto',
					frozenColumns:[[
						{field: 'id',checkbox:true, width: 20, align: 'center',sortable:false,resizable:false, formatter:function(v, r, i) {
							return Utils.XSSEncode(v);
						}},
						{field: 'orderNumber', title: 'Hóa đơn nội bộ', width: 90, sortable:false,resizable:false, align: 'left', formatter:function(v, r, i) {
							return Utils.XSSEncode(v);
						}},
						{field: 'invoiceNumber', title: 'Hóa đơn đỏ', width: 80, sortable:false,resizable:false, align: 'left', formatter:function(v, r, i) {
							return Utils.XSSEncode(v);
						}},
						{field: 'customerCode', title: 'Khách hàng', width: 150, sortable:false,resizable:false, align: 'left',formatter:function(value,rowData,rowIndex){
							return Utils.XSSEncode(rowData.customerCode+' - '+rowData.custName);
						}},
						{field: 'invoiceDate', title: 'Trạng thái', width: 50, sortable:false,resizable:false, align: 'left',formatter:function(value,rowData,rowIndex){
							if(rowData.invoiceDate != null && rowData.invoiceDate != undefined) {
								return 'Đã in';
							} else {
								return 'Chưa in';
							}
						}}
					]],
					columns:[[
					    {field: 'customerAddr', title: 'Địa chỉ', width: 250, sortable:false,resizable:false, align: 'left'},
					    {field: 'staff', title: 'NVBH', width: 100, sortable:false,resizable:false, align: 'left',formatter:function(value,rowData,rowIndex){
					    	if(rowData.staff != null){
					    		return Utils.XSSEncode(rowData.staff.staffCode);
					    	}
					    }},
					    {field: 'deliveryStaff', title: 'NVGH', width: 100, sortable:false,resizable:false, align: 'left',formatter:function(value,rowData,rowIndex){
					    	if(rowData.deliveryStaff != null){
					    		return Utils.XSSEncode(rowData.deliveryStaff.staffCode);
					    	}
					    }},
					    {field: 'amount', title: 'Thành tiền', width: 100, sortable:false,resizable:false, align: 'right',formatter:function(value,rowData,rowIndex){
					    	return formatCurrency(rowData.amount);
					    }},
					    {field: 'custPayment', title: 'Thanh toán', width: 100, sortable:false,resizable:false, align: 'left',formatter:function(value,rowData,rowIndex){
							if(rowData.custPayment == 'MONEY'){ 
								return 'Tiền mặt';
							}else if(rowData.custPayment == 'BANK_TRANSFER'){
								return 'Chuyển khoản';
							}
						}},
					    {field: 'orderDate', title: 'Ngày', width: 90, sortable:false,resizable:false, align: 'center',formatter:function(value,rowData,rowIndex){
					    	return $.datepicker.formatDate('dd/mm/yy', new Date(rowData.orderDate));
					    }},				    
					    {field: 'vat', title: 'Thuế suất', width: 60, sortable:false,resizable:false, align: 'right',formatter:function(value,rowData,rowIndex){
					    	return rowData.vat + '%';
					    }}
					]],
					onCheck : function(rowIndex, rowData) {
						var selectedId = rowData['id'];
						SPPrintTax._map.put(selectedId,rowData);
					},
					onUncheck : function(rowIndex, rowData) {
						var selectedId = rowData['id'];
						SPPrintTax._map.remove(selectedId);
					},
					onCheckAll : function(rows) {
						if($.isArray(rows)) {
							for(var i = 0; i < rows.length; i++) {
								var row = rows[i];
								var selectedId = row['id'];
								SPPrintTax._map.put(selectedId, row);
					    	}
					    }
					},
					onUncheckAll : function(rows) {
						if($.isArray(rows)) {
							for(var i = 0; i < rows.length; i++) {
								var row = rows[i];
								var selectedId = row['id'];
								SPPrintTax._map.remove(selectedId);
					    	}
					    }
					},
					onLoadSuccess:function(){
						$('#gridContainer .datagrid-header-rownumber').html('STT');
						$('#grid').datagrid('resize');
					}
				});
			}
			$('#orderNumber').focus();
			return false;
			
		},
		getListCheckbox:function(){
			var selected = SPPrintTax._map.keyArray;
			if(selected.length>0){
				var data = new Object();		
				data.checkbox=selected;
				data.stockStaff=$('#stockStaff').val();
				return data;
			}
			else return null;
		},
		updatePrintDate: function() {
			$('#errMsg').html(''); 
			$('#errMsg').hide();
			var params = SPPrintTax.getListCheckbox();
			if(params != null) {
				$.ajax({
					type : "POST",
					url : '/sale-product/print-tax/update-print-date',
					data :$.param(params, true),
					dataType : "json",
					success : function(data) {
						window.frames[0].print();
					},
					error:function(XMLHttpRequest, textStatus, errorThrown) {
						$('#errMsg').html('Lỗi hệ thống. Vui lòng thử lại sau').show(); 
					}
				});
				
			} else {
				$('#errMsg').html('Bạn chưa chọn hóa đơn nào để in.').show();
			}
		},
		updatePrintDateNew: function() {
			$('#errMsg').html(''); 
			$('#errMsg').hide();
			var params = SPPrintTax.getListCheckbox();
			if(params != null) {
				$.ajax({
					type : "POST",
					url : '/sale-product/print-tax/update-print-date-new',
					data :$.param(params, true),
					dataType : "json",
					success : function(data) {
						$('#exportPdfFile').hide();
						window.frames[0].print();
					},
					error:function(XMLHttpRequest, textStatus, errorThrown) {
						$('#errMsg').html('Lỗi hệ thống. Vui lòng thử lại sau').show(); 
					}
				});
				
			} else {
				$('#errMsg').html('Bạn chưa chọn hóa đơn nào để in.').show();
			}
		},
		printTax: function() {
			$('#errMsg').html('').hide();
			var params = SPPrintTax.getListCheckbox();
			if(params != null) {
				Utils.addOrSaveData(params, '/sale-product/print-tax/print', null, null, function(data) {
					if(data.error == false) {
						$('#grid').datagrid('reload');
						SPPrintTax._map = new Map();
					}
					window.location.href = data.view;
				}, 'printTaxLoading', null, null, 'Bạn có muốn in hóa đơn VAT?', null, null);
			} else {
				$('#errMsg').html('Bạn chưa chọn số hóa đơn. Vui lòng chọn số hóa đơn muốn in').show();
			}
		},
		viewTax:function() {
			$('#errMsg').html(''); 
			$('#errMsg').hide();
			var params = SPPrintTax.getListCheckbox();
			var date = new Date();
			var day = date.getDay() + 1;
			if(day < 10) {
				day = '0' + day;
			} else {
				day = day + '';
			}
			var month = date.getMonth();
			if(month < 10) {
				month = '0' + month;
			} else {
				month = month + '';
			}
			var year = date.getFullYear();
			year = year + '';
			if(params != null) {
				params.day = day;
				params.month = month;
				params.year = year;
				Utils.getHtmlDataByAjax(params, '/sale-product/print-tax/view-tax', function(data) {
					try {
						var _data = JSON.parse(data);
						if(_data.error && _data.errMsg != undefined) {
							$('#errMsg').html(escapex(_data.errMsg)).show();
						}
						return false;
					} catch (e) {
						$('#viewTaxDialogContainer').show();
						var html = $('#viewTaxDialogContainer').html();
						$('#viewTaxContainer').dialog({
							title:"In phiếu hóa đơn thuế giá trị gia tăng",
							closed: false,  
					        cache: false,  
					        modal: true,
					        width : 838,
					        height: 540,
					        onOpen: function(){
					        	$('#frameContainer').html('<iframe id="viewTaxFrame" style="width: 210mm; height: 120mm;"></iframe>');
					        	setTimeout(function() {
					        		$('#viewTaxFrame').contents().find('html').html(data);
					        	}, 1000);
					        },
					        onClose: function() {
					        	$('#viewTaxDialogContainer').hide();
					        }
						});
					}
					
				});
			} else if (params == null) {
				$('#errMsg').html('Bạn chưa chọn hóa đơn nào để in.').show();
			}
		},
		viewTaxNew:function() {
			SPPrintTax._pdf = null;
			SPPrintTax._numPages = null;
			SPPrintTax.iPage = null;
			SPPrintTax.scale = null;
			$('#errMsg').html(''); 
			$('#errMsg').hide();
			var params = SPPrintTax.getListCheckbox();
			var date = new Date();
			var day = date.getDate();
			if(day < 10) {
				day = '0' + day;
			} else {
				day = day + '';
			}
			var month = date.getMonth() + 1;
			if(month < 10) {
				month = '0' + month;
			} else {
				month = month + '';
			}
			var year = date.getFullYear();
			year = year + '';
			if(params != null) {
				params.day = day;
				params.month = month;
				params.year = year;
				var buyer = $('#buyer').val();
				if(buyer=='' || buyer == "" || buyer == null || buyer == undefined) {
					$('#errMsg').html('Bạn chưa nhập Người mua hàng. Vui lòng nhập Người mua hàng').show();
					$('#buyer').focus();
					return;
				}
				if(buyer != SPPrintTax.buyer) {
					params.buyer = buyer;
					SPPrintTax.buyer = buyer;
				}
				var stockStaff = $('#stockStaff').val();
				if(stockStaff=='' || stockStaff == "" || stockStaff == null || stockStaff == undefined) {
					$('#errMsg').html('Bạn chưa nhập Thủ kho. Vui lòng nhập Thủ kho').show();
					$('#stockStaff').focus();
					return;
				}
				if(stockStaff != SPPrintTax.stockStaff) {
					params.stockStaff = stockStaff;
					SPPrintTax.stockStaff = stockStaff;
				}
				var selStaff = $('#selStaff').val();
				if(selStaff=='' || selStaff == "" || selStaff == null || selStaff == undefined) {
					$('#errMsg').html('Bạn chưa nhập Người bán hàn. Vui lòng nhập Người bán hàng').show();
					$('#selStaff').focus();
					return;
				}
				if(selStaff != SPPrintTax.selStaff) {
					params.selStaff = selStaff;
					SPPrintTax.selStaff = selStaff;
				}
				Utils.getHtmlDataByAjax(params, '/sale-product/print-tax/view-tax-new', function(data) {
					console.log(data);
					try {
						var _data = JSON.parse(data);
						if(_data.error && _data.errMsg != undefined) {
							$('#errMsg').html(_data.errMsg).show();
						} else {
							window.open(_data.downloadPath);
						}
					} catch (e) {
					}
				});
			} else {
				$('#errMsg').html('Bạn chưa chọn hóa đơn nào để in.').show();
			}
		},
		appletViewer: function() {
			$('#errMsg').html(''); 
			$('#errMsg').hide();
			var params = SPPrintTax.getListCheckbox();
			var date = new Date();
			var day = date.getDate();
			if(day < 10) {
				day = '0' + day;
			} else {
				day = day + '';
			}
			var month = date.getMonth() + 1;
			if(month < 10) {
				month = '0' + month;
			} else {
				month = month + '';
			}
			var year = date.getFullYear();
			year = year + '';
			params.day = day;
			params.month = month;
			params.year = year;
			if ($('#cbxShop').combobox('getValue') != null) {
				params.shopCodeSearch = $('#cbxShop').combobox('getValue').trim();
			}
			if(params != null) {
				var buyer = $('#buyer').val();
				if(buyer=='' || buyer == "" || buyer == null || buyer == undefined) {
					$('#errMsg').html('Bạn chưa nhập Người mua hàng. Vui lòng nhập Người mua hàng').show();
					$('#buyer').focus();
					return;
				}
				if(buyer != SPPrintTax.buyer) {
					params.buyer = buyer;
					SPPrintTax.buyer = buyer;
				}
				var stockStaff = $('#stockStaff').val();
				if(stockStaff=='' || stockStaff == "" || stockStaff == null || stockStaff == undefined) {
					$('#errMsg').html('Bạn chưa nhập Thủ kho. Vui lòng nhập Thủ kho').show();
					$('#stockStaff').focus();
					return;
				}
				if(stockStaff != SPPrintTax.stockStaff) {
					params.stockStaff = stockStaff;
					SPPrintTax.stockStaff = stockStaff;
				}
				var selStaff = $('#selStaff').val();
				if(selStaff=='' || selStaff == "" || selStaff == null || selStaff == undefined) {
					$('#errMsg').html('Bạn chưa nhập Người bán hàn. Vui lòng nhập Người bán hàng').show();
					$('#selStaff').focus();
					return;
				}
				if(selStaff != SPPrintTax.selStaff) {
					params.selStaff = selStaff;
					SPPrintTax.selStaff = selStaff;
				}
				try {
					params.reportCode = $('#function_code').val();
				} catch(e) {
					// Oop! Error
				}
				Utils.getHtmlDataByAjax(params, '/sale-product/print-tax/view-tax-new', function(data) {
					console.log(data);
					try {
						var _data = JSON.parse(data);
						if(_data.error && _data.errMsg != undefined) {
							$('#errMsg').html(_data.errMsg).show();
						} else {
							var width = $(window).width();
							var height = $(window).height();
							window.open('/sale-product/print-tax/open-tax-view?windowWidth='+width+'&windowHeight='+height);
						}
					} catch (e) {
					}
				});
			} else {
				$('#errMsg').html('Bạn chưa chọn hóa đơn nào để in.').show();
			}
		},
		downloadFile:function(){
			$('#errMsg').html('').hide();
			$('#divOverlay').show();
			$.ajax({
				type : "POST",
				url : '/sale-product/print-tax/download-file',
				dataType : "json",
				success : function(data) {
					$('#divOverlay').hide();
					if(!data.error){
						window.location.href = data.path;
					}else{
						$('#errMsg').html('Hiện tại cửa hàng chưa có file mẫu.').show();
					}
					
				},
				error:function(XMLHttpRequest, textStatus, errorThrown) {
					$('#divOverlay').hide();
					$('#errMsg').html('Lỗi hệ thống. Vui lòng thử lại sau').show(); 
				}
			});
		},
		uploadFile: function(){
			if(!uploadZIPFile(document.getElementById("zipFile"))){
				return false;
			}
			
			var options = {
					beforeSubmit : SPPrintTax.beforeUploadFile,
					success : SPPrintTax.afterUploadFile,
					type : "POST",
					dataType : 'json'
				};
			$('#importFrm').ajaxForm(options);
			$.messager.confirm('Xác nhận', 'Bạn có muốn lưu thông tin này ?',	function(r) {
				if (r) {
					try{
						$('#importFrm').submit();}
					catch(e) {
					}
				}
			});
		},
		beforeUploadFile: function(){
			$('#divOverlay').show();
			$('#errMsg').hide();
		},
		afterUploadFile: function(data, statusText, xhr, $form){
			$('#divOverlay').hide();
			if (statusText == 'success') {
				if(data.error != undefined && data.error == false){
					$('#zipFile').replaceWith($('#zipFile').val('').clone(true));
					$('#fakefilepc').val('');
					$('#errMsg').html('Upload file thành công!').show();
					setTimeout(function(){$('#errMsg').hide();},4000);
				}else{
					$('#errMsg').html(data.errMsg).show();
				}
			}
		},
		printTemplate:function(){
			$('#errMsg').html('').hide();
			$('#divOverlap').show();
			$.ajax({
				type:"POST",
				url:'/sale-product/print-tax/print-template',
				dataType:"json",
				success:function(data){
					$('#divOverlay').hide();
					if(data.error != undefined && !data.error){
						if(data.hasData != undefined && !data.hasData){
							$('#errMsg').html('Hiện tại cửa hàng chưa có file mẫu.').show();
						}else{
							window.location.href = data.path;
						}
												
					}else{
						$('#errMsg').html('Hiện tại cửa hàng chưa có file mẫu.').show();
					}
				},
				error:function(XMLHttpRequest, textStatus, errorThrown){
					$('#divOverlay').hide();
					$('#errMsg').html("Lỗi hệ thống. Vui lòng thử lại sau.").show();
				}
			});
		}
};
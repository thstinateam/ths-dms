/*
 * Copyright YYYY Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/**
 * Ho tro tinh toan khuyen mai mo moi
 * 
 * @author lacnv1
 * @since Dec 04, 2014
 */
 var OpenProductPromotion = {

		_promotionProgramGroup: null,
		_startIndex: 0,

		/**
		 * Khoi tao thong so cho grid san pham khuyen mai mo moi
		 */
		initOpenProductGrid: function() {
			$('#promotionGrid2').datagrid({
				//showHeader: false,
				showFooter: true,
				singleSelect: true,
				width: $(window).width() - 40,
				fitColumns: true,		
				scrollbarSize: 0,
				autoRowHeight: true,
				data: { total: 0, rows: [], footer: [{isFooter: true, productName:"Tổng", quantity:0}] },
				columns: [[
					{field:'rownum',title:'STT',width:40,resizable:false, sortable:false,align:'center', formatter: function(v, r, i) {
						if (!r.isFooter) {
							return Number(v + OpenProductPromotion._startIndex);
						}
						return "";
					}},
					{field:'productCode',title:'Mã sản phẩm',width:100,resizable:false, sortable:false,align:'left', formatter: PromotionProductFmt.productCodeFmt},
					{field:'productName', title:'Tên sản phẩm', align:'left', resizable:false, sortable:false,
						width: SPAdjustmentOrder.promotionProductWith> 200 ? SPAdjustmentOrder.promotionProductWith : 200,
								formatter:PromotionProductFmt.productFmt},
					{field:'warehouse', title:'Kho', align:'center', resizable:false, sortable:false, width:110, formatter:function (v, r, i) {
						if (r.warehouse != null) {
							return '<a class="delIcons" onclick="return OpenProductPromotion.openSelectWarehouseDlg(\'' +r.productCode+'\',\'' +i+'\', SPCreateOrder.productWarehouseType.PROMOTION_OPEN_PRODUCT);" href="javascript:void(0)">' +Utils.XSSEncode(r.warehouse.warehouseName)+'</a>';
						}
					}},
					{field:'stock',align:'right', resizable:false, sortable:false, width:120, title:'Tồn kho đáp ứng', formatter: PromotionProductFmt.openProductStockFmt},
					{field:'quantity', title:'Tổng', align:'center', resizable:false, sortable:false, width:120, formatter:PromotionProductFmt.openProductQuantityFmt},
					{field:'programPortion', title:'Số suất', align:'center', resizable:false, sortable:false, width:50, formatter:PromotionProductFmt.openProductProgramPortionFmt},
					{field:'programCode', title:'Khuyến mãi', align:'center', resizable:false, sortable:false, width:110, formatter:PromotionProductFmt.programCodeFmt},
					{field:'key', title:'Đổi KM', align:'center', resizable:false, sortable:false, width:80, formatter:PromotionProductFmt.keyFmt}
				]],
				onLoadSuccess: function(data) {
					/*
					 * chia nhom (san pham, CTKM, duoc doi san pham khuyen mai hay ko)
					 */
					if (data !== null && data !== undefined && data.rows !== null && data.rows !== undefined &&  data.rows instanceof Array) {
						OpenProductPromotion._promotionProgramGroup = new Map();
						data.rows.forEach(function(item, index){
							var groupingId = item.programCode + '_' + item.productGroupId + '_' + item.levelPromo;
							var itemsInGroup = OpenProductPromotion._promotionProgramGroup.get(groupingId);
							if (itemsInGroup === null || itemsInGroup === undefined) {
								OpenProductPromotion._promotionProgramGroup.put(groupingId, new Array());
								itemsInGroup = new Array();
							}
							itemsInGroup.push(item);
							OpenProductPromotion._promotionProgramGroup.put(groupingId, itemsInGroup);
						});
					}
					$('#promotionGridContainer2 .opDiscountAmount').attr('disabled','disabled');
					OpenProductPromotion.applyRowStripToPromotionGrid();
					SPAdjustmentOrder.mergesColRowsPromotionGrid("promotionGrid2");
					SPCreateOrder.showTotalGrossWeightAndAmount();
				}
			});
			setTimeout(function() {
				$("#h2MM").next().hide();
				$("#h2MM").hide();
			},750);
		},

		/**
		 * Load du lieu cho grid san pham khuyen mai mo moi
		 */
		loadOpenProductGrid: function(pMap) {
			$("#h2MM").next().show();
			$("#h2MM").show();
			if (pMap == null) {
				pMap = new Map();
			}
			var lst = pMap.valArray;
			var total = lst.length;
			var w = 0;
			for (var i = 0; i < total; i++) {
				if (!isNaN(lst[i].quantity) && !isNaN(lst[i].grossWeight)) {
					w += lst[i].quantity * lst[i].grossWeight;
				}
			}
			var gridData = {
				total: total,
				rows: lst,
				footer: [{isFooter: true, productName:"Tổng", quantity:formatFloatValue(w, 2)}]
			};
			if (SPCreateOrder._lstPromotionProductIsAuto != null) {
				OpenProductPromotion._startIndex = SPCreateOrder._lstPromotionProductIsAuto.size();
			} else {
				OpenProductPromotion._startIndex = 0;
			}
			$('#promotionGrid2').datagrid("loadData", gridData);
			$("#promotionGridContainer2 .datagrid-ftable td[field=productName] div").css("text-align", "center");
			$("#promotionGridContainer2 .datagrid-ftable td[field=quantity] div").css("text-align", "right");

			if (pMap.size() == 0) {
				$("#h2MM").next().hide();
				$("#h2MM").hide();
			}
		},

		/**
		 * load lai footer
		 */
		reloadOpenProductGridFooter: function(pMap) {
			if (pMap == null) {
				pMap = new Map();
			}
			var lst = pMap.valArray;
			var total = lst.length;
			var w = 0;
			for (var i = 0; i < total; i++) {
				if (!isNaN(lst[i].quantity) && !isNaN(lst[i].grossWeight)) {
					w += lst[i].quantity * lst[i].grossWeight;
				}
			}
			$('#promotionGrid2').datagrid("reloadFooter", [{isFooter: true, productName:"Tổng", quantity:formatFloatValue(w, 2)}]);
			$("#promotionGridContainer2 .datagrid-ftable td[field=productName] div").css("text-align", "center");
			$("#promotionGridContainer2 .datagrid-ftable td[field=quantity] div").css("text-align", "right");
			SPCreateOrder.showTotalGrossWeightAndAmount();
		},

		/**
		 * Refresh lai grid (danh lai so thu tu)
		 */
		refreshOpenProductGrid: function() {
			if (SPCreateOrder._lstPromotionProductIsAuto != null) {
				OpenProductPromotion._startIndex = SPCreateOrder._lstPromotionProductIsAuto.size();
			} else {
				OpenProductPromotion._startIndex = 0;
			}
			var gridData = $('#promotionGrid2').datagrid("getData");
			$('#promotionGrid2').datagrid("loadData", gridData);
		},

		/**
		 * Mo popup chon kho cho san pham khuyen mai mo moi
		 */
		openSelectWarehouseDlg: function(productCode, index, productWarehouse){
			var orderNumber = $('#orderNumber').val();
			// if (orderNumber !== undefined && !isNullOrEmpty(orderNumber)) {
			// 	return;
			// }
			$('#productWarehouseDialogContainer').show();
			$('.clsProductVOQuantity').bind('keydown',function(e) {
				var idx = $(this).attr('index');
				var isPressed = false;
				if (e.keyCode == keyCodes.ARROW_DOWN) {
					++idx;
					isPressed = true;
				} else if (e.keyCode == keyCodes.ARROW_UP) {
					--idx;
					isPressed = true;
				} 
				if (idx > -1 && isPressed == true) {
					$('#productRow_' +idx).focus();
				}
			});
			$('#indexTmpToAddRow').val(index);
			var rows = $('#promotionGrid2').datagrid('getRows');
			var product = rows[index];
			var lstData = SPCreateOrder._mapPRODUCTSbyWarehouse.get(product.productId);
			var html = $('#openProductWarehouseDlgContainer').html();
			var title = "Tồn kho đáp ứng của sản phẩm " + Utils.XSSEncode(product.productCode + " - " + product.productName) + ' tương ứng các kho';
			$('#openProductWarehouseDlg').dialog({
				width:750, height: 'auto',
				closed: false,title:title,modal: true,
				onOpen: function() {
					$('#openProductWarehouseDlg').addClass("easyui-dialog");
					$('#spProductWarehouseCode').html(Utils.XSSEncode(product.productCode));
					$('#spProductWarehouseName').html(Utils.XSSEncode(product.productName));
					$('.easyui-dialog #GeneralInforErrors').html('').hide();
					SPCreateOrder._editingProductCode = product.productCode;
					try {
						SPCreateOrder._selectingIndex = Number(index);
					} catch (e) {
						// pass through
					}
					Utils.bindAutoSearch();
					$("#openProductWarehouseGrid").datagrid({
						data: lstData,
						fitColumns: true,
						pagination: true,
						width: $('#openProductWarehouseDlg').width()-25,
						scrollbarSize:0,
						singleSelect:true,
						rownumbers:true,
						cache: false,
						columns:[[
							{field:'warehouseCode',title:'Kho',width:$('#openProductWarehouseDlg').width() - 35 - 120 - 80 - 100 - 130, align:'left', formatter: function(value, row, index) {
								return '<span class="prStyle' +index+'">' +Utils.XSSEncode(row.warehouseName)+'</span>';
							}},
							{field:'availableQuantity',title: 'Tồn kho đáp ứng',
								align:'right',width:130, formatter: function(value, row, index) {
									var value = Number(row.availableQuantity);
									var displayStr = formatQuantityEx(value, row.convfact);	        					
									return '<span class="prStyle' +index+'">' + displayStr + '</span>';
								}
							},  
							{field:'quantity',title:'Số lượng',width:130, algin:'center', formatter: function(value, row, index) {
									var id = "warehouseRow_" + index + "_" + productWarehouse;
									return '<input id=\"' + id + '\" index="' +index+'" warehouseId="' +row.warehouseId+'" maxlength="8" type="text" style="width: 65px; margin-bottom: 0px;" onkeypress="NextAndPrevTextField(event,this,\'clsProductVOQuantity\')" class="clsProductVOQuantity InputTextStyle InputText1Style prStyle' +index+'" size="10"/>&nbsp;';
								}
							}, 
						]],	        		
						onBeforeLoad: function() {
							
						},
						onLoadSuccess: function() {
							$('.clsProductVOQuantity').css('text-align','right').css('padding-right','5px');
							$('.ccSupportCode').css('text-align','left').css('padding-left','5px');
							Utils.bindFormatOnTextfieldInputCss('clsProductVOQuantity', Utils._TF_NUMBER_CONVFACT);
							$('.datagrid-header-rownumber').html('STT');
							$(window).resize();
							$('#warehouseRow_0' + productWarehouse).focus();
							setTimeout(function(){
								CommonSearchEasyUI.fitEasyDialog("openProductWarehouseDlg");
							}, 200);
							/*
							 * restore data from main grid to dialog's grid
							 */
							SPCreateOrder._selectingProductWarehouseType = productWarehouse;
							
							if (rows !== null && rows !== undefined && rows.length > 0) {
								try {
									var selectingRow = rows[Number(index)];
									var key = selectingRow.programCode + '_' + selectingRow.productGroupId + '_' + selectingRow.levelPromo /*+ '_' + selectingRow.changeProduct*/;
									if (OpenProductPromotion._promotionProgramGroup !== null) {
										var rowsInGroup = OpenProductPromotion._promotionProgramGroup.get(key);
										rowsInGroup.forEach(function(item, idx) {
											if (item.productCode == SPCreateOrder._editingProductCode) {
												var quantity = item.quantity,
													warehouseId = item.warehouse.id;
												if (!warehouseId) { warehouseId = item.warehouse.warehouseId; }
												var selector = '#openProductWarehouseDlg input[id*=warehouseRow][warehouseid="' + warehouseId + '"]';
												$(selector).val(quantity);										
											}
										});
									}									
								} catch (e){
									// pass through
								}
							}
						}
					});
				},
				onClose : function(){
					$('#openProductWarehouseDlg').dialog("destroy");
					$('#openProductWarehouseDlgContainer').html(html);
				}
			});
		},

		/**
		 * Chon san pham theo kho tren popup
		 */
		selectProductByWarehouse: function(product) {
			flag = true;
			var index = $('#indexTmpToAddRow').val();
			var mapWarehouseQuantity = new Map();
			var defaultWarehouseObj = null;
			var totalQuantity = 0;	// use to check whether total quantity exceed max promotion quota
			var showErrMsgFunc = function(msg) {
				$('#openProductWarehouseDlg #dlgGeneralInforErrors2').html(msg).show();
				setTimeout(function() {
					$('.#openProductWarehouseDlg #dlgGeneralInforErrors2').hide();
				}, 3000);
			}
			$('#openProductWarehouseDlg .clsProductVOQuantity').each(function(){
				var obj = $(this);
				var data = {};			
				var rowId = obj.attr('id').split('_')[1];
				var warehouseId = obj.attr('warehouseId');
				var wrows = $("#openProductWarehouseGrid").datagrid('getRows');
				var r = wrows[rowId];
				var convfact = r.convfact;
				var availableQuantity = r.remainQuantity || $(this).data('remainQuantity');
				var stock = r.stock;
				var warehouseName = r.warehouseName;
				if (obj.val().length > 0) {				 
					var quantity = obj.val();
					data.warehouseId = warehouseId;
					data.warehouseName = warehouseName;
					data.convfact =convfact;
					data.convfact_value = quantity;
					data.quantity = StockValidateInput.getQuantity(quantity,data.convfact);
					data.availableQuantity = availableQuantity;
					data.stock = stock;
					data.warehouse = r;
					totalQuantity += Number(data.quantity);
					if (isNaN(data.quantity) || Number(data.quantity) < 0) {
						flag = false;
						index = rowId;
						var errMsg = 'Giá trị số lượng nhập vào kho ' + Utils.XSSEncode(warehouseName) +' không hợp lệ';
						if (Number(data.quantity) < 0) {
							errMsg = 'Giá trị số lượng phải là số nguyên dương.';
						}
						$('#openProductWarehouseDlg #dlgGeneralInforErrors2').html(errMsg).show();

						return false;
					} else if (data.quantity > data.availableQuantity) {
						flag = false;
						index = rowId;
						$('#openProductWarehouseDlg #dlgGeneralInforErrors2').html('Giá trị số lượng nhập vào kho ' + Utils.XSSEncode(warehouseName) +' lớn hơn tồn kho đáp ứng.').show();
						return false;
					}
					if (data.quantity > 0) {
						mapWarehouseQuantity.put(data.warehouseId,data);
					}
					if (defaultWarehouseObj === null 
						&& !isNaN(data.availableQuantity) && Number(data.availableQuantity) > 0) {
						defaultWarehouseObj = data;
					}
				}
			});
			if (mapWarehouseQuantity.size() === 0 && defaultWarehouseObj !== null) {
				mapWarehouseQuantity.put(defaultWarehouseObj.warehouseId, defaultWarehouseObj);
			}
			if (flag) {
				var editingProductCode = SPCreateOrder._editingProductCode !== null ? SPCreateOrder._editingProductCode : '';
				var selectingIndex = SPCreateOrder._selectingIndex;
				//if (SPCreateOrder._selectingProductWarehouseType === SPCreateOrder.productWarehouseType.PROMOTION_OPEN_PRODUCT) {
					var errMsg = null;
					if (mapWarehouseQuantity.keyArray.length === 0) {
						errMsg = "Vui lòng nhập số lượng.";
						$('#openProductWarehouseDlg #dlgGeneralInforErrors2').html(errMsg).show();
						return false;
					}

					var rows = $('#promotionGrid2').datagrid('getRows');
					var startIndexToInsertRow = null;
					var tempData = new Array();
					var patternRow = null;
					var selectingRow = rows[SPCreateOrder._selectingIndex];
					var isQttEditing = (selectingRow.isEdited == 0 && selectingRow.hasPortion != 1 && selectingRow.stock < selectingRow.maxQuantity);
					// check if total quantity exceed max promotion quota
					var maxPromotionProductQuotaQuantity = 0;
					if (SPCreateOrder._lstPromotionOpenProduct !== undefined && SPCreateOrder._lstPromotionOpenProduct !== null) {
						if (selectingRow.isEdited == 1) {
							for (var i = 0; i < SPCreateOrder._lstPromotionOpenProduct.valArray.length; i++) {
								var item = SPCreateOrder._lstPromotionOpenProduct.valArray[i];
								if (item.productCode.trim().indexOf(" ") < 0
									&& item.productCode != editingProductCode 
									&& item.programCode === selectingRow.programCode
									&& item.levelPromo == selectingRow.levelPromo
									&& item.productGroupId == selectingRow.productGroupId
									&& Number(item.changeProduct) === Number(selectingRow.changeProduct)
									&& (item.changeProduct == 1 || item.productCode == selectingRow.productCode)) {
									totalQuantity += item.quantity;
								}
							}
						}
						for (var i = 0; i < SPCreateOrder._lstPromotionOpenProduct.valArray.length; i++) {
							var item = SPCreateOrder._lstPromotionOpenProduct.valArray[i];
							if (item.productCode.trim().indexOf(" ") < 0
								&& item.productCode === editingProductCode 
								&& item.programCode === selectingRow.programCode
								&& item.levelPromo == selectingRow.levelPromo
								&& item.productGroupId == selectingRow.productGroupId
								&& Number(item.changeProduct) === Number(selectingRow.changeProduct)) {
								if (selectingRow.isEdited == 1 || isQttEditing) {
									maxPromotionProductQuotaQuantity = item.maxQuantity;
									break;
								}
								maxPromotionProductQuotaQuantity += (Number(item.quantity) < item.maxQuantity) ?
										item.quantity : item.maxQuantity;
								if (maxPromotionProductQuotaQuantity >= item.maxQuantity) {
									maxPromotionProductQuotaQuantity = item.maxQuantity;
									break;
								}
							}
						}
					}
					
					if ((selectingRow.isEdited != 1 && !isQttEditing) && !isNaN(Number(totalQuantity)) && Number(totalQuantity) != maxPromotionProductQuotaQuantity) {
						errMsg = 'Tổng số lượng các kho phải bằng số lượng đang phân bổ cho sản phẩm (' + maxPromotionProductQuotaQuantity + ')';
						$('#openProductWarehouseDlg #dlgGeneralInforErrors2').html(errMsg).show();
						return false;
					} else if (!isNaN(Number(totalQuantity)) && Number(totalQuantity) > maxPromotionProductQuotaQuantity) {
						errMsg = 'Tổng số lượng sản phẩm vượt quá số lượng sản phẩm tối đa được hưởng trong CTKM (' + maxPromotionProductQuotaQuantity + ')';
						$('#openProductWarehouseDlg #dlgGeneralInforErrors2').html(errMsg).show();
						return false;
					}

					/*
					 * xoa cac dong san pham hien co, trong group dang chon
					 */
					var removedRow = OpenProductPromotion._promotionProgramGroup.get(selectingRow.programCode + '_' + selectingRow.productGroupId + '_' + selectingRow.levelPromo /*+ '_' + selectingRow.changeProduct*/);
					if (removedRow !== undefined && removedRow !== null) {
						for (var k = removedRow.length-1; k >= 0; k--) {
							var item = removedRow[k];
							if (item.productCode === editingProductCode) {
								startIndexToInsertRow = k;
								if (patternRow === null) {
									patternRow = new Object(), copyObjectProperties(item, patternRow);
								}
								$('#promotionGrid2').datagrid('deleteRow', Number(item.rownum) - 1);
							}
						}
					}

					/*
					 * prepare data
					 */
					var gridRows = $('#promotionGrid2').datagrid('getRows');
					mapWarehouseQuantity.keyArray.forEach(function(item, index) {
						var row = new Object();
						copyObjectProperties(patternRow, row);					
						row.warehouse = mapWarehouseQuantity.get(item).warehouse;
						row.warehouse.id = row.warehouse.warehouseId;
						row.warehouse.warehouseId = row.warehouse.warehouseId;
						row.stock = row.warehouse.availableQuantity;
						row.quantity = mapWarehouseQuantity.get(item).quantity;
						if (selectingRow.isEdited != 1 && isQttEditing) {
							row.isEdited = 1;
						}
						tempData.push(row);
						gridRows.push(row);
					});

					/*
					 * sort rows by program_code, promo_level, can_change_product
					 */
					gridRows.sort(function(o1, o2){
						var id = '-1413001269113';
						if (o1.programCode != o2.programCode) {
							return o1.programCode > o2.programCode;
						}
						if (o1.productCode.trim().indexOf(" ") > 0) {
							return false;
						}
						if (o2.productCode.trim().indexOf(" ") > 0) {
							return true;
						}
						if (Number(o1.levelPromo) != Number(o2.levelPromo)) {
							return o1.levelPromo > o2.levelPromo;
						}
						if (!isNaN(o1.productGroupId) && !isNaN(o2.productGroupId)
							&& Number(o1.productGroupId) != Number(o2.productGroupId)) {
							return o1.productGroupId < o2.productGroupId;
						}
						if (Number(o1.changeProduct) != Number(o2.changeProduct)) {
							return Number(o1.changeProduct) > Number(o2.changeProduct);
						}
						if (o1.productCode != o2.productCode) {
							return o1.productCode > o2.productCode;
						}
						return o1.warehouse.seq > o2.warehouse.seq;
					});

					/*
					 * re-calculate row's sequence number
					 */
					SPCreateOrder._lstPromotionOpenProduct = new Map();
					gridRows.forEach(function(item, index){
						gridRows[index].rownum = index + 1;
						SPCreateOrder._lstPromotionOpenProduct.put(index, item);
					});

					OpenProductPromotion.loadOpenProductGrid(SPCreateOrder._lstPromotionOpenProduct);
					SPCreateOrder.showTotalGrossWeightAndAmount();
				//}
				SPCreateOrder._syncFlag.isPaymentButtonClickable = true;
				$('.easyui-dialog').dialog('close');
				SPCreateOrder.orderChanged = true;
				if (SPAdjustmentOrder._orderId > 0 && $("#payment").prop("disabled")) {
					enable("btnOrderSave");
				}
			}
		},

		/**
		 * Xu ly tach kho cho san pham mo moi truoc khi luu
		 */
		processSaleOrderLot: function(mapProductPromoQuantity, mapProductPromoWarehouseQuantity, mapProductPromoWarehouse, mapPromoLevel) {
			if ($("#promotionGridContainer2 .datagrid-view").length == 0) { // neu tren man hinh chua co grid khuyen mai mo moi
				var kq = {
					mapProductPromoQuantity: mapProductPromoQuantity,
					mapProductPromoWarehouseQuantity: mapProductPromoWarehouseQuantity,
					mapProductPromoWarehouse: mapProductPromoWarehouse,
					mapPromoLevel: mapPromoLevel
				};

				return kq;
			}
			if (SPCreateOrder._lstPromotionOpenProduct != null) {
				SPCreateOrder._lstPromotionOpenProduct.clear();
			} else {
				SPCreateOrder._lstPromotionOpenProduct = new Map();
			}
			var opArr = $("#promotionGrid2").datagrid("getRows");
			opArr.forEach(function(item, idx) {
				SPCreateOrder._lstPromotionOpenProduct.put(idx, item);
			});

			var quantityTotal = null;
			var warehouseStr = null;
			var productWarehouseQuantStr = null;
			var promoLevelStr = null;

			for (var index=0, sz = SPCreateOrder._lstPromotionOpenProduct.size(); index < sz; index++) {
				quantityTotal = 0;
				var obj = SPCreateOrder._lstPromotionOpenProduct.valArray[index];
				if (!isNullOrEmpty(obj.productCode) && obj.productCode.trim().indexOf(" ") < 0) {
					//_sid = '-1413624577692';
					var mapKey = obj.productCode + '_' + obj.programCode + '_' + obj.productGroupId + '_' + obj.levelPromo;
					if (mapProductPromoQuantity.get(mapKey)==null || mapProductPromoQuantity.get(mapKey) == undefined ) {

						warehouseStr = "";
						productWarehouseQuantStr = "";
						promoLevelStr = "";
						
						//Tach dong sp KM theo kho
						for (var l = 0; l < sz; l++) {
							var lRowObj = SPCreateOrder._lstPromotionOpenProduct.valArray[l];
							var product = SPCreateOrder.findProductCodeInMap(lRowObj.productCode);
							//_sid = '-1413624577692';
							var indexKey = obj.productCode + '_' + obj.programCode + '_' + obj.productGroupId + '_' + obj.levelPromo;
							var lRowKey = lRowObj.productCode + '_' + lRowObj.programCode + '_' + lRowObj.productGroupId + '_' + lRowObj.levelPromo;
							if (indexKey == lRowKey) {
								if ((lRowObj.warehouse == null || lRowObj.warehouse == undefined) && lRowObj.quantity > 0) {
									return { msg: "Sản phẩm khuyến mãi " + Utils.XSSEncode(lRowObj.productCode) + " không có kho." };
								}
								if (lRowObj.warehouse == null || lRowObj.warehouse == undefined) {
									warehouseStr += "0,";
									productWarehouseQuantStr += "0," ;
									promoLevelStr += lRowObj.levelPromo + ",";
								} else {
									warehouseStr += (lRowObj.warehouse.warehouseId || lRowObj.warehouse.id) + ",";
									productWarehouseQuantStr += getQuantity(lRowObj.quantity, product.convfact) + "," ;
									quantityTotal += getQuantity(lRowObj.quantity, product.convfact);
									promoLevelStr += lRowObj.levelPromo + ",";
								}
							}
						}
						if (!isNullOrEmpty(warehouseStr)) {
							mapProductPromoQuantity.put(mapKey, quantityTotal);
							mapProductPromoWarehouseQuantity.put(mapKey, productWarehouseQuantStr);
							mapProductPromoWarehouse.put(mapKey, warehouseStr);
							mapPromoLevel.put(mapKey, promoLevelStr);
						}
					}
				}
			}

			var kq = {
				mapProductPromoQuantity: mapProductPromoQuantity,
				mapProductPromoWarehouseQuantity: mapProductPromoWarehouseQuantity,
				mapProductPromoWarehouse: mapProductPromoWarehouse,
				mapPromoLevel: mapPromoLevel
			};

			return kq;
		},

		/**
		 * Kiem tra danh sach san pham khuyen mai mo moi truoc khi luu
		 */
		checkOpenProduct: function(tempMap, mapProductPromoQuantity, mapProductPromoWarehouse,
					mapProductPromoWarehouseQuantity, mapPromoLevel, totalQuanityOfProductsInCart) {
			var lstPromoWarehouseId = [];
			var lstPromoWarehouseQuantity = [];
			var lstPromoLevel = [];
			var lstPromotionProductQuatity = [];
			var lstPromotionCode = [];
			var lstPromotionType = [];
			var lstDiscount = [];
			var lstDisper = [];
			var lstPromotionProduct = [];
			var lstPromotionProductMaxQuatity = [];
			var lstPromotionProductMaxQuatityCk = [];
			var lstMyTime = [];
			var mapProductGroupAndLevelGroupId = new Map();
			var msg = null;

			if ($("#promotionGridContainer2 .datagrid-view").length == 0) { // neu tren man hinh chua co grid khuyen mai mo moi
				var kq = {
					lstPromoWarehouseId: lstPromoWarehouseId,
					lstPromoWarehouseQuantity: lstPromoWarehouseQuantity,
					lstPromoLevel: lstPromoLevel,
					lstPromotionProductQuatity: lstPromotionProductQuatity,
					lstPromotionCode: lstPromotionCode,
					lstPromotionType: lstPromotionType,
					lstDiscount: lstDiscount,
					lstDisper: lstDisper,
					lstPromotionProduct: lstPromotionProduct,
					lstPromotionProductMaxQuatity: lstPromotionProductMaxQuatity,
					lstPromotionProductMaxQuatityCk: lstPromotionProductMaxQuatityCk,
					lstMyTime: lstMyTime,
					mapProductGroupAndLevelGroupId: mapProductGroupAndLevelGroupId,
					totalQuanityOfProductsInCart: totalQuanityOfProductsInCart
				};
				return kq;
			}

			for (var i=0, sz = SPCreateOrder._lstPromotionOpenProduct.size(); i < sz; ++i) {
				var temp = SPCreateOrder._lstPromotionOpenProduct.valArray[i];
				var isProductInMultiWarehouse = false;
				//_sid = '-1413624577692';
				var tempMapKey = temp.productCode + '_' + temp.programCode + '_' + temp.productGroupId + '_' + temp.levelPromo /*+ '_' + temp.changeProduct*/;
				if (tempMap.get(tempMapKey) !== null && tempMap.get(tempMapKey) !== undefined) {
					isProductInMultiWarehouse = true;
				} else {
					tempMap.put(tempMapKey, {
						totalQuantity: 0
					});
				}

				//Luu cac thong tin tren don hang theo Kho de luu vao sale_order_lot
				//Khong can check trung ma SP
				var warehouse = mapProductPromoWarehouse.get(tempMapKey);
				var warehouseQuantity = mapProductPromoWarehouseQuantity.get(tempMapKey);
				var promoLevel = mapPromoLevel.get(tempMapKey);
				if (!isNullOrEmpty(warehouse) && !isProductInMultiWarehouse) {
					lstPromoWarehouseId.push(warehouse);
					lstPromoWarehouseQuantity.push(warehouseQuantity);
					lstPromoLevel.push(promoLevel);
				/*}
				/*
				 * promotion product's total quantity
				 *
				if (!isNullOrEmpty(warehouse) && !isProductInMultiWarehouse) {*/
					var quantity = mapProductPromoQuantity.get(tempMapKey);
					lstPromotionProductQuatity.push(Number(quantity));
					lstPromotionCode.push(temp.programCode);			
					lstPromotionType.push(temp.type);
					lstDiscount.push(temp.discountAmount);
					lstDisper.push(temp.discountPercent);
					
					/*
					 * put temp data for validate later
					 */
					if (!totalQuanityOfProductsInCart.get(temp.productCode)) {
						totalQuanityOfProductsInCart.put(temp.productCode, {
							quantity: 0,
							warehouses: new Map()	// quantity in each warehouse
						});
					}
					var productInfoInCart = totalQuanityOfProductsInCart.get(temp.productCode);
					productInfoInCart.quantity += Number(temp.quantity);
					// get warehouse_id of warehouse on row
					var tmpWhId = 0;
					if (temp.warehouse != null && temp.warehouse != undefined) {
						tmpWhId = Number(temp.warehouse.warehouseId);
					}
					if (!productInfoInCart.warehouses.get(tmpWhId)) {
						productInfoInCart.warehouses.put(tmpWhId, 0);
					}
					productInfoInCart.warehouses.put(tmpWhId, productInfoInCart.warehouses.get(tmpWhId) + Number(temp.quantity));
					totalQuanityOfProductsInCart.put(temp.productCode, productInfoInCart);
				}
				
				if (temp.type == PromotionType.FREE_PRICE || temp.type == PromotionType.FREE_PERCENT) {
					var zvalue = $('#opDiscountAmount_' +i).val();
					if (zvalue.length == 0) {
						msg = 'Bạn chưa nhập số tiền khuyến mãi. Vui lòng nhập giá trị.';
						$('#opDiscountAmount_' +i).focus();
					} else {
						var maxDiscountAmount = Number(temp.maxDiscountAmount);
						var discountAmount = Number(temp.discountAmount);
						if (maxDiscountAmount<discountAmount) {
							msg = 'Số tiền khuyến mãi vượt quá Số tiền khuyến mãi tối đa được hưởng. Vui lòng nhập lại.';
							$('#opDiscountAmount_' +i).focus();
						}
					}
					/*lstPromotionProduct.push(""); // 19.01
					lstPromotionProductMaxQuatity.push(temp.maxDiscountAmount);
					lstPromotionProductMaxQuatityCk.push(temp.maxDiscountAmount);
					lstMyTime.push((temp.myTime == null || temp.myTime == undefined) ? 0 : temp.myTime);
					mapProductGroupAndLevelGroupId.put(tempMapKey, "");*/
				} else if (temp.type == PromotionType.FREE_PRODUCT && temp.productCode.trim().indexOf(" ") < 0) {
					var zcvalue = $('#op_convact_promotion_value_' +i).val();
					var zvalue = getQuantity(zcvalue, temp.convfact);
					if (zcvalue.length == 0) {
						msg = 'Bạn chưa nhập số lượng sản phẩm khuyến mãi. Vui lòng nhập giá trị.';
						$('#op_convact_promotion_value_' +i).focus();
					} else if (isNaN(zvalue)) {
						msg = 'Số lượng sản phẩm khuyến mãi không hợp lệ. Vui lòng nhập giá trị.';
						$('#op_convact_promotion_value_' +i).focus();
					} else {
						var maxQuantity = Number(temp.maxQuantity);
						var qty = Number(temp.quantity);
						if (maxQuantity < qty) {
							msg = 'Số lượng khuyến mãi vượt quá số lượng khuyến mãi tối đa được hưởng. Vui lòng nhập lại.';
							$('#op_convact_promotion_value_' +i).focus();
						}
					}
					if (!isProductInMultiWarehouse) {
						lstPromotionProduct.push(temp.productCode);
						lstPromotionProductMaxQuatity.push(temp.maxQuantity);
						lstPromotionProductMaxQuatityCk.push(temp.maxQuantityCk == undefined ? temp.maxQuantity : temp.maxQuantityCk);
						lstMyTime.push((temp.myTime == null || temp.myTime == undefined) ? 0 : temp.myTime);
					}
					mapProductGroupAndLevelGroupId.put(tempMapKey, (temp.productGroup ? temp.productGroup.id : temp.productGroupId)+"_"
						+(temp.groupLevel ? temp.groupLevel.id : temp.groupLevelId));
				}
				if (msg != null && msg.length > 0) {
					var kq = { msg: msg, idx: i };
					return kq;
				}
				//Truong hop co lo
				//Chua xu ly
			}

			var kq = {
				lstPromoWarehouseId: lstPromoWarehouseId,
				lstPromoWarehouseQuantity: lstPromoWarehouseQuantity,
				lstPromoLevel: lstPromoLevel,
				lstPromotionProductQuatity: lstPromotionProductQuatity,
				lstPromotionCode: lstPromotionCode,
				lstPromotionType: lstPromotionType,
				lstDiscount: lstDiscount,
				lstDisper: lstDisper,
				lstPromotionProduct: lstPromotionProduct,
				lstPromotionProductMaxQuatity: lstPromotionProductMaxQuatity,
				lstPromotionProductMaxQuatityCk: lstPromotionProductMaxQuatityCk,
				lstMyTime: lstMyTime,
				mapProductGroupAndLevelGroupId: mapProductGroupAndLevelGroupId,
				totalQuanityOfProductsInCart: totalQuanityOfProductsInCart
			};
			return kq;
		},

		/**
		 * apply row-strip to product-promotion grid
		 */
		applyRowStripToPromotionGrid: function() {
			try {
				if (OpenProductPromotion._promotionProgramGroup != null) {
					OpenProductPromotion._promotionProgramGroup.valArray.forEach(function(rowsInGroup, index){
						if (rowsInGroup != null && rowsInGroup instanceof Array) {
							rowsInGroup.forEach(function(item){
								if (index%2 == 1) {
									$("#promotionGridContainer2 table.datagrid-btable tr:has(td[field=rownum] div:contains(" + item.rownum+ "))").addClass("row-striped");
								} else {
									$("#promotionGridContainer2 table.datagrid-btable tr:has(td[field=rownum] div:contains(" + item.rownum+ "))").removeClass("row-striped");
								}
							});
						}
					});
				}
			} catch (e) {
				// pass through
			}
		},

		sortProductList: function(mapTmp, isZV21) {
			mapTmp.valArray.sort(function(o1, o2){
				var id = '-1413001269113';
				/*if (isZV21) {
					if (o1.groupId != o2.groupId) {
						return o1.groupId > o2.groupId;
					}
				}*/
				if (o1.programCode != o2.programCode) {
					return o1.programCode > o2.programCode;
				}
				if (o1.productCode.trim().indexOf(" ") > 0) {
					return false;
				}
				if (o2.productCode.trim().indexOf(" ") > 0) {
					return true;
				}
				if (Number(o1.levelPromo) != Number(o2.levelPromo)) {
					return o1.levelPromo > o2.levelPromo;
				}
				if (!isNaN(o1.productGroupId) && !isNaN(o2.productGroupId)
					&& Number(o1.productGroupId) != Number(o2.productGroupId)) {
					return o1.productGroupId < o2.productGroupId;
				}
				if (Number(o1.changeProduct) != Number(o2.changeProduct)) {
					return Number(o1.changeProduct) > Number(o2.changeProduct);
				}
				if (o1.productCode != o2.productCode) {
					return o1.productCode > o2.productCode;
				}
				return o1.warehouse.seq > o2.warehouse.seq;
			});
			mapTmp.valArray.forEach(function(item, index) {
    				item.rownum = index + 1;
    			});
		},

		qtyPromotionChanged: function(e, i) {
			var gridName = "promotionGrid2";
			var list = SPCreateOrder._lstPromotionOpenProduct;
			var inputId = "op_convact_promotion_value_";
			$('#serverErrors').html('').hide();
			var ppRow = null;
			if(isNaN(i)) {
				var rows = $('#' +gridName).datagrid('getRows');
				for(var k = 0; k < rows.length; k++) {
					/*var arr = i.split('_');
					var productId = arr[1] || "";
					var programCode = arr[0] || "";
					var productGroupId = arr[2] || 0;
					var levelPromo = arr[3] || 0;
					var whID = arr[4] || 0;*/
					var row = rows[k];
					/*if(row.productCode.trim().indexOf(" ") < 0
						&& row.productId == productId && row.commercialSupport == programCode
						&& row.productGroupId == productGroupId
						&& row.levelPromo == levelPromo
						&& (row.warehouse.id||row.warehouse.warehouseId) == whID) {*/
					if (row.productCode.trim().indexOf(" ") < 0
						&& row.commercialSupport + '_' + row.productId + '_' + row.productGroupId +'_' + row.levelPromo+'_' + (row.warehouse?(row.warehouse.id||row.warehouse.warehouseId):0) == i) {
						i = k;
						ppRow = row;
						break;
					}
				}
			} else {
				ppRow = $('#' +gridName).datagrid('getRows')[i];
			}
			if(ppRow==null){
				return false;
			}
			var convfact = ppRow.convfact;
			var value = $('#' +inputId+i).val().trim();
			var proQuatity = 0;
			var pProduct = null;
			if(isNaN(i)) {
				pProduct = list.get(i);
			} else {
				pProduct = list.valArray[i];
			}
			if(!isNullOrEmpty(value)){
				proQuatity = Number(getQuantity(value,convfact));
				if(isNaN(proQuatity)) {							
					$('#' +inputId+i).val('');	
					proQuatity = 0;
				}else{
					var maxPromotionProductQuotaQuantity = 0;
					var tQtt = 0;
					if (list !== undefined && list !== null) {
						for (var k = 0; k < list.valArray.length; k++) {
							var item = list.valArray[k];
							if (item.productCode === ppRow.productCode 
								&& item.programCode === ppRow.programCode
								&& item.levelPromo == ppRow.levelPromo
								&& item.productGroupId == ppRow.productGroupId
								&& Number(item.changeProduct) === Number(ppRow.changeProduct)) {
								maxPromotionProductQuotaQuantity = item.maxQuantity;
								break;
							}
						}
						for (var k = 0; k < list.valArray.length; k++) {
							var item = list.valArray[k];
							if (item.programCode === ppRow.programCode
								&& item.levelPromo == ppRow.levelPromo
								&& item.productGroupId == ppRow.productGroupId
								&& Number(item.changeProduct) === Number(ppRow.changeProduct)
								&& (item.changeProduct == 1 || item.productCode == ppRow.productCode)) {
								if (i != k) {
									tQtt += item.quantity;
								} else {
									tQtt += proQuatity;
								}
							}
						}
					}
					if (tQtt > maxPromotionProductQuotaQuantity) {
						//proQuatity = ppRow.quantity;
						$('#serverErrors').html("Tổng số lượng sản phẩm khuyến mãi vượt quá số lượng tối đa được nhận.").show();
					}
					$('#' +inputId+i).val(formatQuantity(proQuatity,convfact));
				}
			}
			if (pProduct != undefined && pProduct != null) {
				if(isNaN(i)) {
					pProduct.quantity = proQuatity;
					list.put(i, pProduct);
					OpenProductPromotion.reloadOpenProductGridFooter(SPCreateOrder._lstPromotionOpenProduct);
				} else {
					pProduct.quantity = proQuatity;
					list.valArray[i] = pProduct;
					OpenProductPromotion.reloadOpenProductGridFooter(SPCreateOrder._lstPromotionOpenProduct);
				}
			} else {
				pProduct = list.get(ppRow.programCode +"_" + ppRow.productId);
				if (pProduct != undefined && pProduct != null) {
					pProduct.quantity = proQuatity;
					OpenProductPromotion.reloadOpenProductGridFooter(SPCreateOrder._lstPromotionOpenProduct);
				}
			}
			// Tang lai so suat neu nhap so luong > 0
			if (SPCreateOrder._saleOrderQuantityReceivedList != null && SPCreateOrder._saleOrderQuantityReceivedList.length > 0) {
				for (var ind=0; ind<SPCreateOrder._saleOrderQuantityReceivedList.length; ind++) {
					var objTmp = SPCreateOrder._saleOrderQuantityReceivedList[ind];
					if (objTmp.promotionCode == ppRow.programCode
						&& objTmp.productGroupId == ppRow.productGroupId
						&& objTmp.promotionLevel == ppRow.levelPromo) {
						if (ppRow.quantity > 0) {
							objTmp.quantityReceived = objTmp.maxQuantityReceived;
						}
						break;
					}
				}
			}
			SPCreateOrder.orderChanged = true;
			SPCreateOrder.updatePromotionGrids();
			if (SPAdjustmentOrder._orderId > 0 && $("#payment").prop("disabled")) {
				enable("btnOrderSave");
			}
			return true;		
		},

		qtyPromotionChangedAmount:function(e,i){
			$('#serverErrors').html('').hide();
			var ppRow = $('#promotionGrid2').datagrid('getRows')[i];
			if(ppRow==null){
				return false;
			}
			var pProduct = SPCreateOrder._lstPromotionOpenProduct.valArray[i];
			var discountAmount = $('#opDiscountAmount_' +i).val().trim();
			discountAmount =Utils.returnMoneyValue(discountAmount);	
			if(!isNullOrEmpty(discountAmount)){
				if(isNaN(discountAmount)){
					$('#opDiscountAmount_' +i).val('');
					discountAmount = 0;
				}else{
					$('#opDiscountAmount_' +i).val(formatFloatValue(discountAmount));
				}
			}else{
				discountAmount = 0;
			}
			pProduct.discountAmount = discountAmount;
			SPCreateOrder._lstPromotionOpenProduct.valArray[i] = pProduct;
			OpenProductPromotion.reloadOpenProductGridFooter(SPCreateOrder._lstPromotionOpenProduct);
			SPCreateOrder.orderChanged = true;
			return true;
		},

		qtyPromotionChangedPortion: function(e, i) {
			var gridName = "promotionGrid2";
			var list = SPCreateOrder._lstPromotionOpenProduct;
			var inputId = "op_portion_promotion_value_";
			$('#serverErrors').html('').hide();
			var ppRow = null;
			var productId = "";
			var programCode = "";
			var levelPromo = 0;
			var productGroupId = 0;
			var ratio = 0;
			if (isNaN(i)) {
				/*var arr = i.split('_');
				productId = arr[1] || "";
				programCode = arr[0] || "";
				levelPromo = arr[3] || 0;
				productGroupId = arr[2] || 0;*/
				var rows = $('#' +gridName).datagrid('getRows');
				for (var k = 0; k < rows.length; k++) {
					var row = rows[k];
					/*if (row.productId == productId && row.programCode == programCode
						&& row.productGroupId == productGroupId
						&& row.levelPromo == levelPromo) {*/
					if (row.productCode.trim().indexOf(" ") < 0
						&& row.programCode+'_' +row.productId + '_' +row.productGroupId+'_' + row.levelPromo == i) {
						i = k;
						ppRow = row;
						productId = ppRow.productId;
						programCode = ppRow.programCode;
						levelPromo = ppRow.levelPromo;
						productGroupId = ppRow.productGroupId;
						//ratio = Math.round(row.maxQuantity/Number(row.maxQuantityReceived));
						break;
					}
				}
			} else {
				ppRow = $('#' +gridName).datagrid('getRows')[i];
				productId = ppRow.productId;
				programCode = ppRow.programCode;
				levelPromo = ppRow.levelPromo;
				productGroupId = ppRow.productGroupId;
				//ratio = Math.round(ppRow.maxQuantity/Number(ppRow.maxQuantityReceived));
			}
			if(ppRow==null){
				return false;
			}
			var quantityReceived = ppRow.maxQuantityReceived;
			var value = $('#' +inputId+i).val().trim();
			var proQuatity = 0;
			var pProduct = null;
			if(isNaN(i)) {
				pProduct = list.get(i);
			} else {
				pProduct = list.valArray[i];
			}
			if (pProduct != undefined && pProduct != null) {
				if (!isNullOrEmpty(value)) {
					if (isNaN(value)) {
						$('#serverErrors').html('Số suất của CTKM ' +Utils.XSSEncode(pProduct.programCode)+' không đúng định dạng số. Vui lòng nhập lại').show();
						$('#' +inputId+i).focus();
						return false;
					}
					if (Number(value) > quantityReceived) {
						$('#serverErrors').html('Số suất của CTKM ' +Utils.XSSEncode(pProduct.programCode)+' vượt quá số suất được hưởng. Vui lòng nhập số suất nhỏ hơn hoặc bằng ' + quantityReceived).show();
						$('#' +inputId+i).focus();
						//return false;
					}
					$('#' +inputId+i).val(value);
				}
				if (isNaN(i)) {
					list.put(i, pProduct);
				} else {
					list.valArray[i] = pProduct;
				}
				pProduct.quantityReceived = Number(value);
				OpenProductPromotion.reloadOpenProductGridFooter(SPCreateOrder._lstPromotionOpenProduct);
			} else {
				pProduct = list.get(ppRow.programCode +"_" + ppRow.productId);
				if (pProduct != undefined && pProduct != null) {
					pProduct.quantityReceived = Number(value);
					OpenProductPromotion.reloadOpenProductGridFooter(SPCreateOrder._lstPromotionOpenProduct);
				}
			}
			//Set lai gia tri cho so luong SPKM cua so suat moi chinh sua
			var rowsData = $('#' +gridName).datagrid('getRows');
			var qtt1 = null;
			var kkk = null;
			var mapT1 = new Map();
			var mapT2 = new Map();
			var b = false;
			for (var l = 0; l < rowsData.length; l++) {
				var row = rowsData[l];
				if (row.programCode == programCode  && row.productGroupId == productGroupId && row.levelPromo == levelPromo) {
					//row.quantity = ratio*Number(value);
					if (row.changeProduct == 1) {
						kkk = Utils.XSSEncode(programCode+"_" +productGroupId+"_" +levelPromo);
					} else {
						kkk = Utils.XSSEncode(programCode+"_" +productGroupId+"_" +levelPromo+"_" +row.productCode);
					}
					if (mapT1.get(kkk) == null) {
						b = false;
						ratio = Math.round(row.maxQuantity/Number(row.maxQuantityReceived));
						qtt1 = ratio*Number(value);
					} else {
						b = true;
						qtt1 = mapT1.get(kkk);
					}

					if (row.stock >= qtt1) {
						row.quantity = qtt1;
						mapT1.put(kkk, 0);
					} else {
						row.quantity = row.stock;
						mapT1.put(kkk, qtt1 - row.stock);
					}
					mapT2.put(kkk, l);

					if (row.changeProduct == 1 && b && row.quantity == 0) {
						$("#" +gridName).datagrid("deleteRow", l);
						l--;
						szl--;
						rowsData =  $("#" +gridName).datagrid("getRows");
						for (var idx1 = 0, sz1 = list.size(); idx1 < sz1; idx1++) {
							if (list.valArray.length <= idx1 ||
								(list.valArray[idx1].productCode == row.productCode
								&& (list.valArray[idx1].warehouse.id || list.valArray[idx1].warehouse.warehouseId) == (row.warehouse.id||row.warehouse.warehouseId))) {
								list.remove(list.keyArray[idx1]);
								idx1--;
								sz1--;
							}
						}
					}
				}
			}
			for (var ij = 0, szj = mapT1.size(); ij < szj; ij++) {
				if (mapT1.valArray[ij] > 0) {
					var idx = mapT2.get(mapT1.keyArray[ij]);
					if (-1 < idx && idx < rowsData.length) {
						rowsData[idx].quantity = rowsData[idx].quantity + mapT1.valArray[ij];
					}
				}
			}
			//Luu gia tri so suat moi chinh sua
			if (SPCreateOrder._saleOrderQuantityReceivedList != null && SPCreateOrder._saleOrderQuantityReceivedList.length > 0) {
				for (var ind=0; ind<SPCreateOrder._saleOrderQuantityReceivedList.length; ind++) {
					var objTmp = SPCreateOrder._saleOrderQuantityReceivedList[ind];
					if (objTmp.promotionCode == programCode
						&& objTmp.productGroupId == productGroupId
						&& objTmp.promotionLevel == levelPromo) {
						objTmp.quantityReceived = Number(value);
						break;
					}
				}
			}
			OpenProductPromotion.loadOpenProductGrid(SPCreateOrder._lstPromotionOpenProduct);
			SPCreateOrder.orderChanged = true;
			SPCreateOrder.updatePromotionGrids();
			if (SPAdjustmentOrder._orderId > 0 && $("#payment").prop("disabled")) {
				enable("btnOrderSave");
			}
			return true;
		},

		/**
		 * Hien thi thong tin khuyen mai don hang mo moi len grid
		 */
		fillOpenOrderPromotionToGrid: function(zv19zv20MM, zv21MM, htSale, saleRows, warehouselessProductMap, promotionProductQuantityInWarehouse) {
			if (SPCreateOrder._lstPromotionOpenProduct == null) {
				SPCreateOrder._lstPromotionOpenProduct = new Map();
			}
			var mapTmp = SPCreateOrder._lstPromotionOpenProduct;
			var isZV19 = false;
			var zzrowNumForOrder = mapTmp.size();
			if (zv19zv20MM instanceof Array && zv19zv20MM.length > 0) {
				/*
				 * phan bo tien CKTM don hang
				 */
				// tinh tong tien chiet khau
				var saleOrderDiscountAmount = zv19zv20MM.reduce(function(prevTotalDiscountAmount, promotionProgram) {
												var totalDiscountAmount = promotionProgram.listPromoDetail.reduce(function(prevDiscountAmount, promoDetail) {
																			return prevDiscountAmount + Number(promoDetail.discountAmount);
																		}, 0);
												return prevTotalDiscountAmount + totalDiscountAmount;
											}, 0);

				/*
				 * lay cac dong du lieu tren grid de update CKTM: cac dong san pham khong co CT HTTM hoac co CT HTTM dang KM tu dong
				 */
				var rowObject = saleRows.map(function(r, index){
					return {
						row: r,
						rowIndex: index,
					}
				});

				var updatedRow = rowObject.filter(function(item, idx){
									if (item.row[COLSALE.PRO_CODE]) {
										var isManualPromotionProgram = GridSaleBusiness.listCS.some(function(cs) {
											return cs.code == item.row[COLSALE.PROGRAM];
										});
										if ((item.row[COLSALE.PROGRAM] && !isManualPromotionProgram) || !item.row[COLSALE.PROGRAM]) {
											return true;
										}
										return false;
									}
									return false;
								});

				var tmpProductWarehouseMap = new Map();
				updatedRow.forEach(function(rowObj, index){
					var keyMap = rowObj.row[COLSALE.PRO_CODE] + '_';
					if (!tmpProductWarehouseMap.get(keyMap)) {
						tmpProductWarehouseMap.put(keyMap, {
							rows: [],
							totalAmount: 0
						});
					}
					var mapValue = tmpProductWarehouseMap.get(keyMap);
					mapValue.totalAmount += Number(rowObj.row[COLSALE.AMOUNT].toString().replace(/,/g, ""));
					mapValue.rows.push(rowObj);
				});

				/*
				 * tinh tong tien mua hang
				 */
				var totalBuyAmount = updatedRow.reduce(function(prevAmount, rowObj) {
										return prevAmount + Number(rowObj.row[COLSALE.AMOUNT].toString().replace(/,/g, ""));
									}, 0);

				var pCode1 = null;
				for (var ij = 0, szj = zv19zv20MM.length; ij < szj; ij++) {
					var zv = zv19zv20MM[ij];
					var pproduct = SPCreateOrder.getObjectPromotionProduct(zv);
					zzrowNumForOrder++;
					pproduct.productCode = "Khuyến mãi mở mới";
					pproduct.rownum = zzrowNumForOrder;
					pproduct.levelPromo = zv.saleOrderDetail.levelPromo;
					mapTmp.put(zzrowNumForOrder-1, pproduct);
				}

				for (var ij = 0, szj = mapTmp.size(); ij < szj; ij++) {
					var ratio = 0;
					var obj = mapTmp.get(ij);
					if (obj.promotionType == PromotionType.PROMOTION_FOR_ORDER && obj.type == PromotionType.FREE_PERCENT) {
						isZV19 = true;
						ratio = obj.discountPercent;
					} else {
						isZV19 = false;
					}

					var accumulativeDistributedDiscountAmount = 0;
					var isExceededSaleOrderAmount = false;
					tmpProductWarehouseMap.keyArray.forEach(function(keyMap, tempIndex) {
						var product = SPCreateOrder.findProductCodeInMap(keyMap.split('_')[0]);
						if (product != null) {
							var productWarehouse = tmpProductWarehouseMap.get(keyMap);
							var totalProductAmount = productWarehouse.totalAmount;
							var productDiscountPercent = 0;
							var productDiscountAmount = 0;
							if (isZV19) {
								productDiscountPercent = ratio;
								productDiscountAmount = Math.round((productDiscountPercent * totalProductAmount)/100);
							} else {
								productDiscountPercent = Math.round(obj.discountAmount*10000/totalBuyAmount)/100;
								// productDiscountPercent = Math.round10(totalProductAmount / totalBuyAmount, -2);
								//productDiscountAmount = Math.round((productDiscountPercent * totalProductAmount)/100);
								productDiscountAmount = Math.floor((productDiscountPercent * totalProductAmount)/100);
								if (productDiscountAmount >= obj.discountAmount) {
									mapProductDiscountAmount = obj.discountAmount;
									isExceededSaleOrderAmount = true;
								}
							}

							if (tempIndex == tmpProductWarehouseMap.keyArray.length - 1
								|| productDiscountAmount > obj.discountAmount - accumulativeDistributedDiscountAmount) {
								productDiscountAmount = obj.discountAmount - accumulativeDistributedDiscountAmount;
							}
							var tmpAccumulativeDiscountAmount = 0;
							for (var k = 0; k < productWarehouse.rows.length; k++) {
								var rowObj = productWarehouse.rows[k];
								var rowDiscountAmount = 0;
								if (k == productWarehouse.rows.length - 1) {
									rowDiscountAmount = Math.round(productDiscountAmount - tmpAccumulativeDiscountAmount);
								} else {
									var rowDiscountPercent = Math.round10(Number(rowObj.row[COLSALE.AMOUNT].toString().replace(/,/g, ""))/totalProductAmount, -2);
									//rowDiscountAmount = Math.round(rowDiscountPercent * productDiscountAmount);
									rowDiscountAmount = Math.floor(rowDiscountPercent * productDiscountAmount);
								}
								// update to grid
								var value = rowObj.row[COLSALE.DISCOUNT]; 
								if (value == null) value = "";
								var oldDiscountAmount = Number(value.toString().replace(/,/g, ""));
								htSale.setDataAtCell(rowObj.rowIndex,COLSALE.DISCOUNT,formatCurrency(rowDiscountAmount + oldDiscountAmount));
								tmpAccumulativeDiscountAmount += rowDiscountAmount;

								SPCreateOrder._mapDiscountAmountOrder.keyArray[rowObj.rowIndex] = rowObj.rowIndex;
								SPCreateOrder._mapDiscountAmountOrder.valArray[rowObj.rowIndex] = rowDiscountAmount+oldDiscountAmount;
							}
							accumulativeDistributedDiscountAmount += productDiscountAmount;
						}
					});
				}
			}

			if (zv21MM instanceof Array && zv21MM.length > 0) {

				var cloneProductQuantityInWarehouse = function(productId) {
					// clone from 'SPCreateOrder._mapPRODUCTSbyWarehouseOnGrid', then 'SPCreateOrder._mapPRODUCTSbyWarehouse'
					var productsWarehouses = SPCreateOrder._mapPRODUCTSbyWarehouseOnGrid.get(productId) || [];
					if (productsWarehouses.length === 0) {
						productsWarehouses = SPCreateOrder._mapPRODUCTSbyWarehouse.get(productId) || [];
					}
					if (productsWarehouses.length != 0 && !promotionProductQuantityInWarehouse.get(productId)) {
						var tmpProductsWarehouses = [];
						$.extend(true, tmpProductsWarehouses, productsWarehouses);
						promotionProductQuantityInWarehouse.put(productId, tmpProductsWarehouses);
					}
				};
				var autoSplitWarehouse = function(saleOrderLot) {
					var warehouseOfProducts = promotionProductQuantityInWarehouse.get(saleOrderLot.stockTotal != null ? saleOrderLot.stockTotal.product.id : saleOrderLot.saleOrderDetail.product.id);
					var quantity = saleOrderLot.quantity;
					var out = [];
					if (warehouseOfProducts) {
						for (var i = 0; i < warehouseOfProducts.length && quantity > 0; i++) {
							if (warehouseOfProducts[i].availableQuantity != 0 || SPAdjustmentOrder._orderId) {
								var distributedQuantity = Math.min(quantity, warehouseOfProducts[i].availableQuantity);
								var newSaleOrderLot = {};
								$.extend(true, newSaleOrderLot, saleOrderLot);
								newSaleOrderLot.quantity = distributedQuantity;
								// set stock
								var j = -1,
									warehouses = SPCreateOrder._mapPRODUCTSbyWarehouse.get(saleOrderLot.stockTotal.product.id);
								do {
									j++;
									newSaleOrderLot.warehouse = warehouses[j];
									newSaleOrderLot.stock = warehouses[j].availableQuantity;
									
									newSaleOrderLot.stockTotal.availableQuantity = warehouses[j].availableQuantity;
									newSaleOrderLot.stockTotal.warehouse = warehouses[j];
									newSaleOrderLot.stockTotal.id = null;
									newSaleOrderLot.stockTotal.quantity = null;
								} while (j < warehouses.length && warehouses[j].warehouseId != warehouseOfProducts[i].warehouseId);
								if (!SPAdjustmentOrder._orderId || newSaleOrderLot.stockTotal.availableQuantity != 0) {
									if (SPAdjustmentOrder._orderId) {
										distributedQuantity = Math.min(quantity, newSaleOrderLot.stockTotal.availableQuantity);
										newSaleOrderLot.quantity = distributedQuantity;
									}
									out.push(newSaleOrderLot);
									quantity -= distributedQuantity;
									warehouseOfProducts[i].availableQuantity -= distributedQuantity;
								}
							}
						}
					}
					if (out.length == 0) {
						out.push(saleOrderLot);
					} else {
						if (quantity > 0) {
							out[out.length - 1].quantity += quantity;
						}
					}
					return out;
				};

				zzrowNumForOrder++;		
				var zLenPPAuto = mapTmp.size();
				//var mapHeader = new Map();
				var tmpGroupId = 1000;
				for (var ij = 0, szj = zv21MM.length; ij < szj; ij++) {
					var zV21 = zv21MM[ij];
					var pproduct = new Object();
					pproduct = SPCreateOrder.getObjectPromotionProduct(zV21);
					pproduct.rownum = zzrowNumForOrder;
					pproduct.groupId = ++tmpGroupId;	// gom nhom cac muc KM
					/*if (mapHeader.get(pproduct.programCode) == null) {
						mapHeader.put(pproduct.programCode, 1);
						mapTmp.put(zLenPPAuto, pproduct);
					}*/

					var len = mapTmp.size();
					for (var i = 0, sz = zV21.listPromotionForPromo21.length; i < sz; i++) {
						var pproduct = new Object();
						var obj = zV21.listPromotionForPromo21[i].saleOrderLot;
						if (obj != null && obj != undefined) {
							// cong so luong da tach kho tren server lai, de thuc hien tach kho lai tren client
							var oldQuantity = obj[0].quantity;
							for (var j = 1; j < obj.length; obj[0].quantity += obj[j].quantity, j++);

							// tach kho lai tu sale_order_lot dau tien
							var j = 0;
							obj[j].saleOrderDetail = zV21.listPromotionForPromo21[i].saleOrderDetail;
							obj[j].promoProduct = zV21.listPromotionForPromo21[i].promoProduct;
							obj[j].type = zV21.listPromotionForPromo21[i].type;
							obj[j].keyList = zV21.listPromotionForPromo21[i].keyList;
							obj[j].changeProduct = zV21.listPromotionForPromo21[i].changeProduct;						
							obj[j].typeName = zV21.listPromotionForPromo21[i].typeName;						
							obj[j].isEdited = zV21.listPromotionForPromo21[i].isEdited;
							obj[j].promotionType = zV21.listPromotionForPromo21[i].promotionType;
							obj[j].levelPromo = zV21.listPromotionForPromo21[i].levelPromo;
							obj[j].productGroupId = zV21.listPromotionForPromo21[i].productGroupId;
							/*
							 * tach kho, set lai stockTotal & warehouse
							 */
							cloneProductQuantityInWarehouse(obj[j].stockTotal != null ? obj[j].stockTotal.product.id : obj[j].saleOrderDetail.product.id);
							var autoSplitedSaleOrderLot = autoSplitWarehouse(obj[j]);
							//obj[0].quantity = oldQuantity;
							autoSplitedSaleOrderLot.forEach(function(sol) {
								if (sol.stockTotal && sol.warehouse) {
									var quantity = sol.quantity;
									var pproduct = SPCreateOrder.getObjectPromotionProduct(sol);
									pproduct.quantity = quantity;
									//pproduct.maxQuantity = pproduct.quantity;
									pproduct.rownum = zzrowNumForOrder;
									pproduct.groupId = tmpGroupId;
									pproduct.openPromo = 1;
									pproduct.hasPortion = zV21.hasPortion;
									mapTmp.put(len, pproduct);
									++len;
									zzrowNumForOrder++;
								} else {
									if (!warehouselessProductMap.get(sol.saleOrderDetail.programCode)) {
										warehouselessProductMap.put(sol.saleOrderDetail.programCode, []);
									}
									if ($.inArray(sol.saleOrderDetail.product.productCode, warehouselessProductMap.get(sol.saleOrderDetail.programCode)) < 0) {
										warehouselessProductMap.get(sol.saleOrderDetail.programCode).push(sol.saleOrderDetail.product.productCode);
									}
								}
							});
						}
					};
					zLenPPAuto = mapTmp.size();
				}
			}

			/*mapTmp.valArray.forEach(function(item, index) {
    				item.rownum = index + 1;
    			});*/
		}
 };
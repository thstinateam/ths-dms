/**
 * in phiếu giao hàng
 */
var SPPrintReceiveOrder = {
	PRINT_CUSTOMER:0,
	PRINT_DELIVERY:1,
	PRINT_ORDER:2,
	_lstSaleOrderSelect:null,	
	_ORDER_TYPE: {NVGH: 1, NVBH: 2},
	checkAll: function(checkAll) {
		Utils.onCheckAllChange(checkAll, 'ApproveStatus');
	},
	checkThis: function(checkThis) {
		Utils.onCheckboxChangeForCheckAll(checkThis, 'ApproveStatus', 'allApproveStatus');
	},
	search: function() {
		$('.ErrorMsgStyle').html('').hide();
		var params = new Object();
		var	msg = Utils.getMessageOfInvalidFormatDate('createDateFrom', 'Từ ngày');
		if(msg.length > 0) {
			$('#errorSearchMsg').html(msg).show();
			return;
		}
		msg = Utils.getMessageOfRequireCheck('createDateFrom', 'Từ ngày');
		if(msg.length > 0) {
			$('#errorSearchMsg').html(msg).show();
			return;
		}
		var	msg = Utils.getMessageOfInvalidFormatDate('createDateTo', 'Đến ngày');
		if(msg.length > 0) {
			$('#errorSearchMsg').html(msg).show();
			return;
		}
		msg = Utils.getMessageOfRequireCheck('createDateTo', 'Đến ngày');
		if(msg.length > 0) {
			$('#errorSearchMsg').html(msg).show();
			return;
		}
		if(msg.length==0 && !Utils.compareDate($('#createDateFrom').val().trim(), $('#createDateTo').val().trim())){
			msg = 'Từ ngày phải là ngày trước hoặc cùng ngày với đến ngày';
		}
		
		if(msg.length > 0) {
			$('#errorSearchMsg').html(msg).show();
			return;
		}else{
			$('#errorSearchMsg').hide();
		}
		var object = new Object();
		object.shopCode = $('#cbxShop').combobox('getValue');
		
		object.saleStaffCode = $('#saleStaffCode').combobox('getValue').trim();
		object.deliveryStaffCode = $('#deliveryStaffCode').combobox('getValue').trim();
		object.createDateFrom = $('#createDateFrom').val().trim();
		object.createDateTo = $('#createDateTo').val().trim();
		//object.placeCreaterOn = $('#placeCreaterOn').val().trim();
		object.valueOrder = $('#isValueOrder').val().trim();
		var valPr = $('#isNotPrint').val();
		if(valPr>=0){
			object.isPrint = valPr==0;
		}	
		object.shortCode = $('#customerCode').val().trim();
		object.printTime = $('#printDate').val().trim();
		object.printBatch = $('#printBatch').val().trim();
//		object.printVATStatus = $('#printVATStatus').val().trim();
		var orderTypes = $('#orderType').val();
		if (orderTypes != null) {
			if (orderTypes instanceof Array) {
				while (orderTypes.length > 0 && isNullOrEmpty(orderTypes[0])) {
					orderTypes.splice(0, 1);
				}
				var out = {};
				convertToSimpleObject(out, orderTypes, 'orderTypes');
				$.extend(object, out);
			} else {
				object.orderTypes = orderTypes.trim();
			}
		}

		//object.priorityCode = $('#priority').val().trim();//SangTN: Loc theo do uu tien don hang
		//object.pageNumber=1;
		
		object.orderValueCompareType = $('#isValueOrder').val().trim();
		var numberOrder = $('#numberValueOrder').val().trim();
		var orderValue = "";
		if ( $('#numberValueOrder').val() != undefined &&  $('#numberValueOrder').val() != null &&  $('#numberValueOrder').val() !='') {
			orderValue = numberOrder.replace(/,/g, '');
		}
		object.orderValue = orderValue;
		$('#gridPrintReceiveOrder').datagrid('load', object);
		
	},
	printOddOrder: function(orderType) {
		$('#viewOddContainer').dialog('close');
		
		$('.ErrorMsgStyle').html('').hide();
		var listSaleOrderId = new Array();
		var rows = $('#gridPrintReceiveOrder').datagrid('getSelections');
		var r = null;
		for(var i=0, sz=rows.length;i<sz;++i){
			r = rows[i];
			//trungtm6 comment
			/*if (r.delivery == null) {
				$("#errMsg").html("Đơn hàng " + Utils.XSSEncode(r.orderNumber) + " chưa chỉ định NVGH, đề nghị qua thực hiện chỉ định NVGH trước khi in").show();
				return;
			}*/
			//end trungtm6 comment
			/*if (r.car == null) {
				$("#errMsg").html("Đơn hàng " + Utils.XSSEncode(r.orderNumber) + " chưa chỉ định xe giao hàng, đề nghị qua thực hiện chỉ định xe trước khi in").show();
				return;
			}*/
			listSaleOrderId.push(r.id);
		}
		if(listSaleOrderId.length == 0) {
			$('#errMsg').html('Bạn chưa chọn hóa đơn nào. vui lòng chọn hóa đơn').show();			
			return;
		}
		
		var params = new Object();
		params.shopCode = $('#cbxShop').combobox('getValue');
		params.listSaleOrderId = listSaleOrderId;
		params.orderType = orderType;
		try {
			params.reportCode = $('#function_code').val();
		} catch(e) {
			// Oop! Error
		}
		
		var url = '/sale-product/print-receive-order/print-odd-order';
		$('#divOverlay').show();
		$.ajax({
			type : "POST",
			url : url,
			data :($.param(params, true)),
			dataType : "json",
			success : function(data) {
				$('#divOverlay').hide();
				$("#gridPrintReceiveOrder").datagrid("reload");
				if(data.error && data.errMsg != undefined) {
					$('#errMsg').html(data.errMsg).show();
					var tm = setTimeout(function(){
						$('#errMsg').html('').hide();
						clearTimeout(tm);
					}, 3000);
				} else {
					var filePath = ReportUtils.buildReportFilePath(data.view);
					window.location.href = filePath;
				}
			},
			error:function(XMLHttpRequest, textStatus, errorThrown) {
				$.ajax({
					type : "POST",
					url : '/check-session',
					dataType : "json",
					success : function(data) {
						
					},
					error: function(XMLHttpRequest, textStatus, errorThrown) {
						window.location.href = '/home';
					}
				});
			}
		});
	},
	printOddOrderPdf: function(orderType, pageSize) {
		$('#viewOddContainer').dialog('close');
		
		$('.ErrorMsgStyle').html('').hide();
		var listSaleOrderId = new Array();
		var rows = $('#gridPrintReceiveOrder').datagrid('getSelections');
		var r = null;
		for(var i=0, sz=rows.length;i<sz;++i){
			r = rows[i];
			//trungtm6
			/*if (r.delivery == null) {
				$("#errMsg").html("Đơn hàng " + Utils.XSSEncode(r.orderNumber) + " chưa chỉ định NVGH, đề nghị qua thực hiện chỉ định NVGH trước khi in").show();
				return;
			}*/
			//end trungtm6
			/*if (r.car == null) {
				$("#errMsg").html("Đơn hàng " + Utils.XSSEncode(r.orderNumber) + " chưa chỉ định xe giao hàng, đề nghị qua thực hiện chỉ định xe trước khi in").show();
				return;
			}*/
			listSaleOrderId.push(r.id);
		}
		if(listSaleOrderId.length == 0) {
			$('#errMsg').html('Bạn chưa chọn hóa đơn nào. vui lòng chọn hóa đơn').show();			
			return;
		}
		
		var params = new Object();
		params.shopCode = $('#cbxShop').combobox('getValue');
		params.listSaleOrderId = listSaleOrderId;
		params.orderType = orderType;
		params.printPageSize = pageSize;
		
		try {
			params.reportCode = $('#function_code').val();
		} catch(e) {
			// Oop! Error
		}

		var url = '/sale-product/print-receive-order/print-odd-order-pdf';
		$('#divOverlay').show();
		$.ajax({
			type : "POST",
			url : url,
			data :($.param(params, true)),
			dataType : "json",
			success : function(data) {
				$('#divOverlay').hide();
				$("#gridPrintReceiveOrder").datagrid("reload");
				if(data.error && data.errMsg != undefined) {
					$('#errMsg').html(data.errMsg).show();
					var tm = setTimeout(function(){
						$('#errMsg').html('').hide();
						clearTimeout(tm);
					}, 3000);
				} else {
					var filePath = ReportUtils.buildReportFilePath(data.view);
					ReportUtils.openInNewTab(filePath);
				}
			},
			error:function(XMLHttpRequest, textStatus, errorThrown) {
				$.ajax({
					type : "POST",
					url : '/check-session',
					dataType : "json",
					success : function(data) {
						
					},
					error: function(XMLHttpRequest, textStatus, errorThrown) {
						window.location.href = '/home';
					}
				});
			}
		});
	},
	print: function(){
		var listSaleOrderId = new Array();
		var rows = $('#gridPrintReceiveOrder').datagrid('getSelections');
		for(var i=0;i<rows.length;++i){
			listSaleOrderId.push(rows[i].id);
		}
		$('#divOverlay').show();
		var params = new Object();
		params.shopCode = $('#cbxShop').combobox('getValue');
		params.listSaleOrderId = listSaleOrderId;
		var url = '/sale-product/print-receive-order/update-print';
		Utils.saveData(params, url, null, 'errMsg', function(data) {
			$('#divOverlay').hide();
			if(data.error && data.errMsg != undefined) {
				$('#errMsg').html(data.errMsg).show();
				var tm = setTimeout(function() {
					$('#errMsg').html('').hide();
					clearTimeout(tm);
				}, 3000);
			} else {
				window.frames['viewOddFrame'].focus();
				try {
					window.frames['viewOddFrame'].contentWindow.print();
				} catch (ex) {
					window.frames['viewOddFrame'].print();
				}
			}
		}, null, null, null, function(XMLHttpRequest, textStatus, errorThrown) {
			$.ajax({
				type : "POST",
				url : '/check-session',
				dataType : "json",
				success : function(data) {
					
				},
				error: function(XMLHttpRequest, textStatus, errorThrown) {
					window.location.href = '/home';
				}
			});
		}, 'true');
	},
	updatePrintWithType: function(){
		var listSaleOrderId = new Array();
		var rows = $('#gridPrintReceiveOrder').datagrid('getSelections');
		for(var i=0;i<rows.length;++i){
			listSaleOrderId.push(rows[i].id);
		}
		
		var params = new Object();
		params.listSaleOrderId = listSaleOrderId;
		
		var url = '/sale-product/print-receive-order/update-print-with-type';
		$('#divOverlay').show();
		Utils.saveData(params, url, null, 'errMsg', function(data) {
			$('#divOverlay').hide();
			if(data.error && data.errMsg != undefined) {
				$('#errMsg').html(data.errMsg).show();
				var tm = setTimeout(function(){
					$('#errMsg').html('').hide();
					clearTimeout(tm);
				}, 3000);
			} else {
				window.frames["viewGopFrame"].focus();
				try {
					window.frames['viewGopFrame'].contentWindow.print();
				} catch (e) {
					window.frames['viewGopFrame'].print();
				}
			}
		}, null, null, null, function(XMLHttpRequest, textStatus, errorThrown) {
			$.ajax({
				type : "POST",
				url : '/check-session',
				dataType : "json",
				success : function(data) {
					
				},
				error: function(XMLHttpRequest, textStatus, errorThrown) {
					window.location.href = '/home';
				}
			});
		}, 'true');
	},
	viewOddOrder: function(orderType) {
		$('#frameContainer').empty();
		$('.ErrorMsgStyle').html('').hide();
		var listSaleOrderId = new Array();
		var rows = $('#gridPrintReceiveOrder').datagrid('getSelections');
		var r = null;
		for (var i = 0, sz = rows.length; i < sz; ++i) {
			r = rows[i];
			if (orderType == SPPrintReceiveOrder._ORDER_TYPE.NVGH) {// nvgh
				if (r.delivery == null) {
					$("#errMsg").html("Đơn hàng " + Utils.XSSEncode(r.orderNumber) + " chưa chỉ định NVGH, đề nghị qua thực hiện chỉ định NVGH trước khi in").show();
					return;
				}
			}
			
			/*if (r.car == null) {
				$("#errMsg").html("Đơn hàng " + Utils.XSSEncode(r.orderNumber) + " chưa chỉ định xe giao hàng, đề nghị qua thực hiện chỉ định xe trước khi in").show();
				return;
			}*/
			listSaleOrderId.push(r.id);
		}
		if (listSaleOrderId.length == 0) {
			$('#errMsg').html('Bạn chưa chọn hóa đơn nào. vui lòng chọn hóa đơn').show();
			return;
		}
		if (orderType == SPPrintReceiveOrder._ORDER_TYPE.NVGH) {// NVGH
			$("#btnOddExcel").unbind('click');
			$("#btnOddExcel").bind('click', function(event) {
				SPPrintReceiveOrder.printOddOrder(1);
			});

			$("#btnOddPdf, #btnOddPdfA4").unbind('click');
			$("#btnOddPdf, #btnOddPdfA4").bind('click', function(event) {
				var pageSize = $(event.target).attr('page-size');
				SPPrintReceiveOrder.printOddOrderPdf(1, pageSize);
			});
		} else {
			$("#btnOddExcel").unbind('click');
			$("#btnOddExcel").bind('click', function(event) {
				SPPrintReceiveOrder.printOddOrder(2);
			});

			$("#btnOddPdf, #btnOddPdfA4").unbind('click');
			$("#btnOddPdf, #btnOddPdfA4").bind('click', function(event) {
				var pageSize = $(event.target).attr('page-size');
				SPPrintReceiveOrder.printOddOrderPdf(2, pageSize);
			});
		}
		var params = new Object();
		params.shopCode = $('#cbxShop').combobox('getValue');
		params.listSaleOrderId = listSaleOrderId;
		params.orderType = orderType;
		if (params != null) {
			Utils.getHtmlDataByAjax(params, '/sale-product/print-receive-order/view-odd', function(data) {
				try {
					var _data = JSON.parse(data);
					if (_data.error && _data.errMsg != undefined) {
						$('#errMsg').html(escapex(_data.errMsg)).show();
					}
					return false;
				} catch (e) {
					$('#viewOddDialogContainer').show();
					var html = $('#viewGopDialogContainer').html();
					$('#viewOddContainer').dialog({
						title:"In phiếu giao nhận và thanh toán",
						closed: false,  
				        cache: false,  
				        modal: true,
				        width : 750,
				        height: 540,
				        onOpen: function(){
				        	$('#frameContainer').html('<iframe id="viewOddFrame" style="width: 705px; height: 470px;"><html><body></body></html></iframe>');
				        	setTimeout(function() {
				        		$('#viewOddFrame').contents().find('body').html(data);
				        	}, 1000);
				        },
				        onClose: function() {
				        	//$('#viewOddDialogContainer').html(html);
				        	$('#viewOddDialogContainer').hide();
				        }
					});
				}
//				var display_setting="location=no,menubar=no,width=700,height=480,top=50,left=200,scrollbars=yes";
//				var print_window= window.open("", "", display_setting);
//				var print_document = print_window.document;
//				print_document.open();
//				print_document.write("<html><head></head>");
//				print_document.write("<body>");
//				print_document.write(data);
//				print_document.write("</body></html>");
//				print_window.print();
			});
		} else {
			$('#errMsg').html('Bạn chưa chọn hóa đơn nào để in.').show();
		}
	},
	printTotalOrder: function() {
		var listSaleOrderId = new Array();
		var rows = $('#gridPrintReceiveOrder').datagrid('getSelections');
		var r = null;
		for(var i=0, sz=rows.length;i<sz;++i){
			r = rows[i];
			if (r.delivery == null) {
				$("#errMsg").html("Đơn hàng " + Utils.XSSEncode(r.orderNumber) + " chưa chỉ định NVGH, đề nghị qua thực hiện chỉ định NVGH trước khi in").show();
				return;
			}
			/*if (r.car == null) {
				$("#errMsg").html("Đơn hàng " + Utils.XSSEncode(r.orderNumber) + " chưa chỉ định xe giao hàng, đề nghị qua thực hiện chỉ định xe trước khi in").show();
				return;
			}*/
			listSaleOrderId.push(r.id);
		}
		if(listSaleOrderId.length == 0) {
			$('#errMsg').html('Bạn chưa chọn hóa đơn nào. vui lòng chọn hóa đơn').show();
			return;
		}
		var params = new Object();
		params.listSaleOrderId = listSaleOrderId;
		try {
			params.reportCode = $('#function_code').val();
		} catch(e) {
			// Oop! Error
		}
		var url = '/sale-product/print-receive-order/print-total-order';
		$('#divOverlay').show();
		$.ajax({
			type : "POST",
			url : url,
			data :($.param(params, true)),
			dataType : "json",
			success : function(data) {
				$('#divOverlay').hide();
				$("#gridPrintReceiveOrder").datagrid("reload");
				if(data.error && data.errMsg != undefined) {
					$('#errMsg').html(data.errMsg).show();
					var tm = setTimeout(function(){
						$('#errMsg').html('').hide();
						clearTimeout(tm);
					}, 3000);
				} else {
					var filePath = ReportUtils.buildReportFilePath(data.view);
					ReportUtils.openInNewTab(filePath);
				}
			},
			error:function(XMLHttpRequest, textStatus, errorThrown) {
				$.ajax({
					type : "POST",
					url : '/check-session',
					dataType : "json",
					success : function(data) {
					},
					error: function(XMLHttpRequest, textStatus, errorThrown) {
						window.location.href = '/home';
					}
				});
			}
		});
	},
	showDialogPrint:function(){	
		$('.ErrorMsgStyle').html('').hide();
		var listSaleOrderId = new Array();
		var rows = $('#gridPrintReceiveOrder').datagrid('getSelections');
		var r = null;
		for(var i=0, sz=rows.length;i<sz;++i){
			r = rows[i];
			/*if (r.delivery == null) {
				$("#errMsg").html("Đơn hàng " + Utils.XSSEncode(r.orderNumber) + " chưa chỉ định NVGH, đề nghị qua thực hiện chỉ định NVGH trước khi in").show();
				return;
			}*/
			/*if (r.car == null) {
				$("#errMsg").html("Đơn hàng " + Utils.XSSEncode(r.orderNumber) + " chưa chỉ định xe giao hàng, đề nghị qua thực hiện chỉ định xe trước khi in").show();
				return;
			}*/
			listSaleOrderId.push(r.id);
		}
		if(listSaleOrderId.length == 0) {
			$('#errMsg').html('Bạn chưa chọn hóa đơn nào. vui lòng chọn hóa đơn').show();
			return;
		}
		$('#showDialogPrintContainer').css("visibility", "visible");
		var html = $('#showDialogPrintContainer').html();
		$('#showDialogPrintDialog').dialog({  	       
	        closed: false,  
	        cache: false,  
	        modal: true,
	        width : 450,
	        height :135,
	        onOpen: function(){	   
	        	var selector = $('.easyui-dialog #typePrint');
	        	if(!selector.next().hasClass('CustomStyleSelectBox')){
	        		$('.easyui-dialog #typePrint').customStyle();
	        	}	      
	        	$('.easyui-dialog #typePrint').val(0).change();
	        },
	        onClose : function(){
	        	$('#showDialogPrintContainer').html(html);
	        	$('#showDialogPrintContainer').css("visibility", "hidden");  	        	
	        }
		});	
	},
	printWithType:function(){
		$('#viewGopContainer').dialog('close');
		
		var listSaleOrderId = new Array();
		var rows = $('#gridPrintReceiveOrder').datagrid('getSelections');
		var r = null;
		for(var i=0, sz=rows.length;i<sz;++i){
			r = rows[i];
//			if (r.delivery == null) {
//				$("#errMsg").html("Đơn hàng " + Utils.XSSEncode(r.orderNumber) + " chưa chỉ định NVGH, đề nghị qua thực hiện chỉ định NVGH trước khi in").show();
//				return;
//			}
			/*if (r.car == null) {
				$("#errMsg").html("Đơn hàng " + Utils.XSSEncode(r.orderNumber) + " chưa chỉ định xe giao hàng, đề nghị qua thực hiện chỉ định xe trước khi in").show();
				return;
			}*/
			listSaleOrderId.push(r.id);
		}
		if(listSaleOrderId.length==0){
			$('#errMsgDlgPrint').html('Bạn chưa chọn hóa đơn nào. vui lòng chọn hóa đơn').show();
			return false;
		}
		var params = new Object();
		var createDateFrom = $('#createDateFrom').val();
		var createDateTo = $('#createDateTo').val();
		if(createDateFrom != null || createDateFrom != '' || createDateFrom != undefined) {
			params.createDateFrom = createDateFrom;
		}
		if(createDateTo != null || createDateTo != '' || createDateTo != undefined) {
			params.createDateTo = createDateTo;
		}
		params.listSaleOrderId = listSaleOrderId;
		params.typePrint = $('.easyui-dialog #typePrint').val().trim();

		try {
			params.reportCode = $('#function_code').val();
		} catch(e) {
			// Oop! Error
		}
		
		$('#divOverlay').show();
		$.ajax({
			type : "POST",
			url : '/sale-product/print-receive-order/print/withtype',
			data :($.param(params, true)),
			dataType : "json",
			success : function(data) {
				//SPPrintReceiveOrder.search();
				$("#gridPrintReceiveOrder").datagrid("reload");
				$('#divOverlay').hide();
				if(data.error && data.errMsg != undefined) {
					$('#errMsg').html(data.errMsg).show();					
				} else {
					var filePath = ReportUtils.buildReportFilePath(data.path);
					//window.location.href = filePath;
					ReportUtils.openInNewTab(filePath);
				}
			}
		});
	},
	viewWithType: function () {
		$('#frameContainer1').empty();
		var typePrint = $('.easyui-dialog #typePrint').val().trim()
		var listSaleOrderId = new Array();
		var rows = $('#gridPrintReceiveOrder').datagrid('getSelections');
		var r = null;
		for(var i=0, sz=rows.length;i<sz;++i){
			r = rows[i];
			if (typePrint == SPPrintReceiveOrder.PRINT_DELIVERY && r.delivery == null) {
				$("#errMsgDlgPrint").html("Đơn hàng " + Utils.XSSEncode(r.orderNumber) + " chưa chỉ định NVGH, đề nghị qua thực hiện chỉ định NVGH trước khi in").show();
				return;
			}
			/*if (r.car == null) {
				$("#errMsg").html("Đơn hàng " + Utils.XSSEncode(r.orderNumber) + " chưa chỉ định xe giao hàng, đề nghị qua thực hiện chỉ định xe trước khi in").show();
				return;
			}*/
			listSaleOrderId.push(r.id);
		}
		if(listSaleOrderId.length==0){
			$('#errMsgDlgPrint').html('Bạn chưa chọn hóa đơn nào. vui lòng chọn hóa đơn').show();
			return false;
		}
		var params = new Object();
		var createDateFrom = $('#createDateFrom').val();
		var createDateTo = $('#createDateTo').val();
		if(createDateFrom != null || createDateFrom != '' || createDateFrom != undefined) {
			params.createDateFrom = createDateFrom;
		}
		if(createDateTo != null || createDateTo != '' || createDateTo != undefined) {
			params.createDateTo = createDateTo;
		}
		params.listSaleOrderId = listSaleOrderId;
		params.typePrint = typePrint;
		$('#showDialogPrintDialog').dialog('close');
		if(params != null) {
			Utils.getHtmlDataByAjax(params, '/sale-product/print-receive-order/view-gop', function(data) {
				try {
					var _data = JSON.parse(data);
					if(_data.error && _data.errMsg != undefined) {
						$('#errMsg').html(escapex(_data.errMsg)).show();
					}
					return false;
				} catch (e) {
					$('#viewGopDialogContainer').show();
					var html = $('#viewGopDialogContainer').html();
					$('#viewGopContainer').dialog({
						title:"In phiếu giao nhận và thanh toán",
						closed: false,  
				        cache: false,  
				        modal: true,
				        width : 750,
				        height: 540,
				        onOpen: function(){
				        	$('#frameContainer1').html('<iframe id="viewGopFrame" style="width: 705px; height: 470px;"><html><body></body></html></iframe>');
				        	setTimeout(function() {
				        		$('#viewGopFrame').contents().find('body').html(data);
				        	}, 1000);
				        },
				        onClose: function() {
				        	//$('#viewOddDialogContainer').html(html);
				        	$('#viewGopDialogContainer').hide();
				        }
					});
				}
//				var display_setting="location=no,menubar=no,width=700,height=480,top=50,left=200,scrollbars=yes";
//				var print_window= window.open("", "", display_setting);
//				var print_document = print_window.document;
//				print_document.open();
//				print_document.write("<html><head></head>");
//				print_document.write("<body>");
//				print_document.write(data);
//				print_document.write("</body></html>");
//				print_window.print();
			});
		} else {
			$('#errMsg').html('Bạn chưa chọn hóa đơn nào để in.').show();
		}
	},
	printExportOrder: function() {
		var listSaleOrderId = new Array();
		var rows = $('#gridPrintReceiveOrder').datagrid('getSelections');
		var r = null;
		for(var i=0, sz=rows.length;i<sz;++i){
			r = rows[i];
			if (r.delivery == null) {
				$("#errMsg").html("Đơn hàng " + Utils.XSSEncode(r.orderNumber) + " chưa chỉ định NVGH, đề nghị qua thực hiện chỉ định NVGH trước khi in").show();
				return;
			}
			/*if (r.car == null) {
				$("#errMsg").html("Đơn hàng " + Utils.XSSEncode(r.orderNumber) + " chưa chỉ định xe giao hàng, đề nghị qua thực hiện chỉ định xe trước khi in").show();
				return;
			}*/
			listSaleOrderId.push(r.id);
		}
		if(listSaleOrderId.length == 0) {
			$('#errMsg').html('Bạn chưa chọn hóa đơn nào. vui lòng chọn hóa đơn').show();
			return;
		}
		var params = new Object();
		var createDateFrom = $('#createDateFrom').val();
		var createDateTo = $('#createDateTo').val();
		if(createDateFrom != null || createDateFrom != '' || createDateFrom != undefined) {
			params.createDateFrom = createDateFrom;
		}
		if(createDateTo != null || createDateTo != '' || createDateTo != undefined) {
			params.createDateTo = createDateTo;
		}
		params.listSaleOrderId = listSaleOrderId;
		try {
			params.reportCode = $('#function_code').val();
		} catch(e) {
			// Oop! Error
		}
		var url = '/sale-product/print-receive-order/print-export-order';
		$('#divOverlay').show();
		$.ajax({
			type : "POST",
			url : url,
			data :($.param(params, true)),
			dataType : "json",
			success : function(data) {
				$('#divOverlay').hide();
				$("#gridPrintReceiveOrder").datagrid("reload");
				if(data.error && data.errMsg != undefined) {
					$('#errMsg').html(data.errMsg).show();					
				} else {
					var filePath = ReportUtils.buildReportFilePath(data.view);
					ReportUtils.openInNewTab(filePath);
					setTimeout(function(){ //Set timeout Ä‘á»ƒ Ä‘áº£m báº£o file load lÃªn hoÃ n táº¥t
	                    CommonSearch.deleteFileExcelExport(data.view);
					},2000);
				}
			}
		});
	},
	showPhieuVCNoiBoPopup: function() {
		$(".ErrorMsgStyle").hide();
		var listSaleOrderId = [];
		var rows = $('#gridPrintReceiveOrder').datagrid('getSelections');
		var r = null;
		for(var i=0, sz=rows.length;i<sz;++i){
			r = rows[i];
			if (r.delivery == null) {
				$("#errMsg").html("Đơn hàng " + Utils.XSSEncode(r.orderNumber) + " chưa chỉ định NVGH, đề nghị qua thực hiện chỉ định NVGH trước khi in").show();
				return;
			}
			/*if (r.car == null) {
				$("#errMsg").html("Đơn hàng " + Utils.XSSEncode(r.orderNumber) + " chưa chỉ định xe giao hàng, đề nghị qua thực hiện chỉ định xe trước khi in").show();
				return;
			}*/
			listSaleOrderId.push(r.id);
		}
		if(listSaleOrderId.length == 0) {
			$('#errMsg').html('Bạn chưa chọn hóa đơn nào. vui lòng chọn hóa đơn').show();
			return;
		}
		
		var html = $("#phieuVCNoiBoContainer").html();
		$("#phieuVCNoiBoPopup").dialog({
			title: "Phiếu xuất kho kiêm vận chuyển nội bộ",
			closed: false,
	        cache: false, 
	        modal: true,
	        width : 670,
	        height :220,
	        onOpen: function(){
	        	$("#phieuVCNoiBoPopup").addClass("easyui-dialog");
	        	setDateTimePicker("dateTrans");
	        	var v = localStorage.getItem("phieuVCNoiBoPopup_nameTrans");
	        	if (v != undefined && v != null && String(v)) {
	        		$("#phieuVCNoiBoPopup #nameTrans").val(v.trim());
	        	}
	        	v = localStorage.getItem("phieuVCNoiBoPopup_dateTrans");
	        	if (v != undefined && v != null && String(v)) {
	        		$("#phieuVCNoiBoPopup #dateTrans").val(v.trim());
	        	}
	        	$("#phieuVCNoiBoPopup #dateTrans").val(getCurrentDate());
	        	$("#phieuVCNoiBoPopup #nameTrans").focus();
	        },
			onClose: function() {
				var v = $("#phieuVCNoiBoPopup #nameTrans").val().trim();
				localStorage.setItem("phieuVCNoiBoPopup_nameTrans", v);
				v = $("#phieuVCNoiBoPopup #dateTrans").val().trim();
				localStorage.setItem("phieuVCNoiBoPopup_dateTrans", v);
				$("#phieuVCNoiBoPopup").dialog("destroy");
				$("#phieuVCNoiBoContainer").html(html);
			}
		});
	},
	printTransport: function() {
		$(".ErrorMsgStyle").hide();
		var msg = Utils.getMessageOfRequireCheck("phieuVCNoiBoPopup #nameTrans", "Tên lệnh điều động");
		if (msg.length == 0) {
			msg = Utils.getMessageOfRequireCheck("phieuVCNoiBoPopup #dateTrans", "Ngày lệnh điều động");
		}
		if (msg.length == 0) {
			msg = Utils.getMessageOfInvalidFormatDate("phieuVCNoiBoPopup #dateTrans", "Ngày lệnh điều động");
		}
		if (msg.length == 0) {
			msg = Utils.getMessageOfRequireCheck("phieuVCNoiBoPopup #contentTrans", "Nội dung");
		}
		if (msg.length > 0) {
			$("#errMsgVCNB").html(msg).show();
			return;
		}
		
		var listSaleOrderId = [];
		var rows = $('#gridPrintReceiveOrder').datagrid('getSelections');
		var r = null;
		for(var i=0, sz=rows.length;i<sz;++i){
			r = rows[i];
			if (r.delivery == null) {
				$("#errMsg").html("Đơn hàng " + Utils.XSSEncode(r.orderNumber) + " chưa chỉ định NVGH, đề nghị qua thực hiện chỉ định NVGH trước khi in").show();
				return;
			}
			/*if (r.car == null) {
				$("#errMsg").html("Đơn hàng " + Utils.XSSEncode(r.orderNumber) + " chưa chỉ định xe giao hàng, đề nghị qua thực hiện chỉ định xe trước khi in").show();
				return;
			}*/
			listSaleOrderId.push(r.id);
		}
		if(listSaleOrderId.length == 0) {
			$('#errMsg').html('Bạn chưa chọn hóa đơn nào. vui lòng chọn hóa đơn').show();
			return;
		}
		
		var params = {listSaleOrderId:listSaleOrderId,
							name:$("#phieuVCNoiBoPopup #nameTrans").val().trim(),
							printTime:$("#phieuVCNoiBoPopup #dateTrans").val().trim(),
							content:$("#phieuVCNoiBoPopup #contentTrans").val().trim()};
		try {
			params.reportCode = $('#function_code').val();
		} catch(e) {
			// Oop! Error
		}
		$("#phieuVCNoiBoPopup").dialog("close");
		Utils.getJSONDataByAjax(params, "/sale-product/print-receive-order/print-transport", function(data) {
			hideLoadingIcon();
			$("#gridPrintReceiveOrder").datagrid("reload");
			if (data.error) {
				if (data.errMsg != undefined && data.errMsg != null) {
					$("#errMsg").html(data.errMsg).show();
				} else {
					$("#errMsg").html("Lỗi").show();
				}
			} else if (!data.hasData) {
				$("#errMsg").html("Không có dữ liệu").show();
			} else {
				var filePath = ReportUtils.buildReportFilePath(data.path);
				window.location.href = filePath;
			}
		});
	},
	/**
	 * Xem phieu giao hang bang applet phuc vu demo
	 *
	 * @author lacnv1
	 * @since Apr 22, 2015
	 */
	viewApplet: function() {
		var orderType = 1;
		
		$('.ErrorMsgStyle').html('').hide();
		var listSaleOrderId = new Array();
		var rows = $('#gridPrintReceiveOrder').datagrid('getSelections');
		var r = null;
		for(var i=0, sz=rows.length;i<sz;++i){
			r = rows[i];
			if (r.delivery == null) {
				$("#errMsg").html("Ä�Æ¡n hÃ ng " + Utils.XSSEncode(r.orderNumber) + " chÆ°a chá»‰ Ä‘á»‹nh NVGH, Ä‘á»� nghá»‹ qua thá»±c hiá»‡n chá»‰ Ä‘á»‹nh NVGH trÆ°á»›c khi in").show();
				return;
			}
			listSaleOrderId.push(r.id);
		}
		if(listSaleOrderId.length == 0) {
			$('#errMsg').html('Báº¡n chÆ°a chá»�n hÃ³a Ä‘Æ¡n nÃ o. vui lÃ²ng chá»�n hÃ³a Ä‘Æ¡n').show();			
			return;
		}
		
		var params = new Object();
		params.listSaleOrderId = listSaleOrderId;
		params.orderType = orderType;
		try {
			params.reportCode = $('#function_code').val();
		} catch(e) {
			// Oop! Error
		}
		
		var url = '/sale-product/print-receive-order/print-odd-order-pdf';
		$('#divOverlay').show();
		$.ajax({
			type : "POST",
			url : url,
			data :($.param(params, true)),
			dataType : "json",
			success : function(data) {
				$('#divOverlay').hide();
				$("#gridPrintReceiveOrder").datagrid("reload");
				if(data.error && data.errMsg != undefined) {
					$('#errMsg').html(data.errMsg).show();
					var tm = setTimeout(function(){
						$('#errMsg').html('').hide();
						clearTimeout(tm);
					}, 3000);
				} else {
					var width = $(window).width();
					var height = $(window).height();
					window.open('/commons/open-tax-view?windowWidth='+width+'&windowHeight='+height);
				}
			},
			error:function(XMLHttpRequest, textStatus, errorThrown) {
				$.ajax({
					type : "POST",
					url : '/check-session',
					dataType : "json",
					success : function(data) {
						
					},
					error: function(XMLHttpRequest, textStatus, errorThrown) {
						window.location.href = '/home';
					}
				});
			}
		});
	},
	initInfo: function() {
		Utils.initUnitCbx('cbxShop', {}, 206, function(){
			SPPrintReceiveOrder.handleSelectShop();
			if ($('#gridContainer .datagrid').length == 0) {
				SPPrintReceiveOrder.bindDataGrid();
			}
		});//230
//		var ti = setTimeout(function() {
//			SPPrintReceiveOrder.bindDataGrid();
//		}, 3000);
	},
	handleSelectShop: function() {
		var params= {};
		params.shopCode = $('#cbxShop').combobox('getValue');
		Utils.getJSONDataByAjax(params, '/sale-product/assign-transfer-staff/lst-staff-cbx' , function(data) {
			if (data != null) {
				$('#strLockDate').html(data.lockDate);
				$('#createDateFrom').val(data.lockDate);
				$('#createDateTo').val(data.lockDate);
				Utils.bindStaffCbx('#saleStaffCode', data.lstSale, null, 206);
				Utils.bindStaffCbx('#deliveryStaffCode', data.lstDeliver, null, 206);
			}
		});
		
	},
	bindDataGrid: function() {
		var valPr = $('#isNotPrint').val();
		var isPrint = null;
		if(valPr>=0){
			isPrint = valPr==0;
		}
		
		$('#gridPrintReceiveOrder').datagrid({  
		    url:'/sale-product/print-receive-order/search',
		    rownumbers:true,
		    selectOnCheck:true,
		    singleSelect:false,
		    rownumbers:true,
		    width: $(window).width() - 45,
			autoRowHeight:true,		    
		    scrollbarSize:0,
		    pagination : true,
		    pageNumber : 1,
		    pageList  : [10,20,30,50,100,200,300,500,1000],
	        rowNum : 10,
		    queryParams: {
		    	shopCode: $('#cbxShop').combobox('getValue'),
		    	createDateFrom: $('#createDateFrom').val(),
		    	createDateTo: $('#createDateTo').val(),
		    	//placeCreaterOn: $('#placeCreaterOn').val(),
		    	isPrint: isPrint
		    	//priorityCode:$('#priority').val().trim()
		    }, 
		    frozenColumns:[[
				{field:'id', checkbox:true, width:50, sortable:false,resizable:false, align:'center'},      
				{field:'orderNumber', title:'Số đơn hàng', width:150, sortable:false,resizable:false, align:'left'},  
				{field:'orderDate', title:'Ngày', width:100, sortable:false,resizable:false, align:'center', formatter : function(value,row,index) {
		        	if(row.orderDate != null) {
		        		//return $.datepicker.formatDate('dd/mm/yy', new Date(row.orderDate));
		        		return $.datepicker.formatDate('dd/mm/yy', new Date(row.orderDate.substr(0,10)));
		        	} 
		        }},
				{field:'customer', title:'Khách hàng', width:200, sortable:false,resizable:false, align: 'left', formatter : function(value,row,index) {
					if(row.customer != null) {
						return Utils.XSSEncode(row.customer.shortCode + ' - ' + row.customer.customerName);
					} else {
						return '';
					}
				}},
				{field:'quantity', title:'Số lượng', width:100, sortable:false,resizable:false, align:'right', formatter : function(value,row,index) {
		        	if(row.quantity != null) {
		        		return formatCurrencyInterger(row.quantity);
		        	} else {
		        		return 0;
		        	}
		        }},
				{field:'total', title:'Tổng tiền', width:100, sortable:false,resizable:false, align:'right', formatter : function(value,row,index) {
		        	if(row.total != null) {
		        		return formatCurrencyInterger(row.total);
		        	} else {
		        		return 0;
		        	}
		        }}
		    ]],
		    columns:[[ 				
		        {field:'customer.address', title:'Địa chỉ', width:350, sortable:false,resizable:false, align:'left', formatter : function(value,row,index) {
		        	if(row.customer != null && row.customer.address != null) {
		        		return Utils.XSSEncode(row.customer.address);
		        	} else {
		        		return '';
		        	}
		        }},
		        {field:'staff.staffCode', title:'NVBH', width:150, sortable:false,resizable:false, align:'left', formatter : function(value,row,index) {
		        	if(row.staff!=null && row.staff.staffCode != null) {
		        		return Utils.XSSEncode(row.staff.staffCode);
		        	} else {
		        		return '';
		        	}
		        }},
		        {field:'timePrint', title:'Ngày In', width:100, sortable:false,resizable:false, align:'center', formatter : function(value,row,index) {
		        	if(row.timePrint != null) {
		        		return $.datepicker.formatDate('dd/mm/yy', new Date(row.timePrint));
		        	} 
		        }},
		        {field:'printBatch', title:'Lượt In', width:100, sortable:false,resizable:false, align:'center', formatter : function(value,row,index) {
		        	if(row.printBatch != null) {
		        		return Utils.XSSEncode(row.printBatch);
		        	} 
		        	return 0;
		        }},
		        {field:'delivery.staffCode', title:'NVGH', width:150, sortable:false,resizable:false, align:'left', formatter : function(value,row,index) {
		        	if(row.delivery!=null && row.delivery.staffCode != null) {
		        		return Utils.XSSEncode(row.delivery.staffCode);
		        	} else {
		        		return '';
		        	}
		        }},
		        {field:'car.carNumber', title:'Xe GH', width:150, sortable:false,resizable:false, align:'left', formatter : function(value,row,index) {
		        	if(row.car != null && row.car.carNumber != null) {
		        		return Utils.XSSEncode(row.car.carNumber);
		        	} else {
		        		return '';
		        	}
		        }},
		        {field:'approved.value', title:'Trạng thái', width:150, sortable:false,resizable:false, align:'left', formatter : function(value,row,index) {
		        	if(row.approved!=null && row.approved=='APPROVED'){
		        		return 'Đã duyệt';
		        	}
		        }},
		        {field:'TTIn', title:'Trạng thái in', width:150, sortable:false,resizable:false, align:'left', formatter : function(value,row,index) {
		        	if(row.timePrint != null) {
		        		return 'Đã in';
		        	} else {
		        		return 'Chưa in';
		        	}
		        }}
		    ]],    
		    onLoadSuccess :function(data){	    	
		    	$('.datagrid-header-rownumber').html('STT');	    	
		    	 updateRownumWidthForJqGrid('.easyui-dialog');
		    	 $('#gridPrintReceiveOrder').datagrid('unselectAll');
	   		 $(window).resize();    		 
		    }
		});
	}
	
};
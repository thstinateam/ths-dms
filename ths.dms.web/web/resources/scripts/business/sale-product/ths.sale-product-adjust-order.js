var SPAdjustmentOrder = {
	_xhrSave : null,
	_xhrDel : null,
	_typeSave:null,
	_callBack:null,
	_loadDetail:null,
	_choseProductId : null,
	_mapCusutomerForCopyOrder:null,
	_orderId: null,
	_editingSaleOrder: null,
	_editGrid:true,
	_keyMapPromotionForOrder:null,
	_zv21ListPromotion : new Array(),
	_changedStructurePromotionProgramsInSaleOrder: null,	// danh sach cac CTKM cua DH da bi thay doi co cau
	/*
	 * when sale order load first time, don't show option 'Doi CTKM'
	 */
	_firstLoadOrderForEdit: false,
	_promotionProductWith:$(window).width() - 25 - 110 - 120 - 100 - 70 - 100 - 100 - 60 - 110-40,
	_saleProductWith:$(window).width() - 110 - 60 - 110 - 80 - 120 - 100 - 70 - 50 - 106-40-60,

	/*
	 * thong tin loai CT HTTM: tay, huy, doi, tra
	 */
	PROGRAM_TYPE_MAP: new Map([
		{key: "ZM", value: "1"},
		{key: "DISPLAY_SCORE", value: "2"},
		{key: "ZD", value: "3"},
		{key: "ZH", value: "4"},
		{key: "ZT", value: "5"},
		{key: "MANUAL_PROM", value: "dummy"}
	]),
	initPromotionProductGrid:function() {
		$('#gridPromotionProduct').datagrid({
			data : SPCreateOrder._lstPromotionProductIsAuto.valArray,
			columns: [[
			    {field:'rownum',title:'STT',width:40,resizable:false, sortable:false,align:'center'},
			    {field:'productCode',title:'Mã sản phẩm',width:100,resizable:false, sortable:false,align:'left', 
			    	formatter: PromotionProductFmt.productCodeFmt},
				{field:'productName', title:'Tên sản phẩm', align:'left', resizable:false, sortable:false,
					width: SPAdjustmentOrder.promotionProductWith> 200 ? SPAdjustmentOrder.promotionProductWith : 200,
							formatter:PromotionProductFmt.productFmt},
				{field:'warehouse', title:'Kho', align:'center', resizable:false, sortable:false, width:110, formatter:PromotionProductFmt.stockTotalFmt},
				{field:'stock',align:'right', resizable:false, sortable:false, width:120,
					title:'Tồn kho đáp ứng',
					formatter: PromotionProductFmt.stockFmt},
				{field:'quantity', title:'Tổng', align:'center', resizable:false, sortable:false, width:120, formatter:PromotionProductFmt.quantityFmt},
				{field:'programPortion', title:'Số suất', align:'center', resizable:false, sortable:false, width:50, formatter:PromotionProductFmt.programPortionFmt},
				{field:'programCode', title:'Khuyến mãi', align:'center', resizable:false, sortable:false, width:110, formatter:PromotionProductFmt.programCodeFmt},
				{field:'key', title:'Đổi KM', align:'center', resizable:false, sortable:false, width:80, formatter:PromotionProductFmt.keyFmt}
			]],	
			showFooter: true,
			singleSelect: true,

			width:$(window).width() - 40,
			fitColumns:true,		
			scrollbarSize:0,
			autoRowHeight : true,
			onLoadSuccess: function(data){
				/*
				 * chia nhom (san pham, CTKM, duoc doi san pham khuyen mai hay ko)
				 */
				if (data !== null && data !== undefined && data.rows !== null && data.rows !== undefined &&  data.rows instanceof Array) {
					SPCreateOrder._promotionProgramGroup = new Map();
					data.rows.forEach(function(item, index){
						var groupingId = Utils.XSSEncode(item.programCode + '_' + item.productGroupId + '_' + item.levelPromo) /*+ '_' + item.changeProduct*/;
						var itemsInGroup = SPCreateOrder._promotionProgramGroup.get(groupingId);
						if (itemsInGroup === null || itemsInGroup === undefined) {
							SPCreateOrder._promotionProgramGroup.put(groupingId, new Array());
							itemsInGroup = new Array();
						}
						itemsInGroup.push(item);
						SPCreateOrder._promotionProgramGroup.put(groupingId, itemsInGroup);
					});
					
				}
				SPCreateOrder.applyRowStripToPromotionGrid();
				SPAdjustmentOrder.mergesColRowsPromotionGrid('gridPromotionProduct');

				if (data == null || !data.rows || data.rows.length == 0) {
					setTimeout(function() {
						$("#h2SPKM").next().hide();
						$("#h2SPKM").hide();
					}, 100);
				} else {
					$("#h2SPKM").next().show();
					$("#h2SPKM").show();
				}
			}
		});
	},
	initSalePromotionProductGrid:function() {
//		$("#h2KMDH").next().show();
//		$("#h2KMDH").show();
		$('#gridSaleOrderPromotion').datagrid({
			data: SPCreateOrder._lstPromotionProductForOrderIsAuto.valArray,
			columns: [[
			    {field:'rownum',title:'STT',width:40,resizable:false, sortable:false,align:'center'},
			    {field:'productCode',title:'Mã sản phẩm',width:100,resizable:false, sortable:false,align:'left', 
			    	formatter: PromotionProductFmt.productCodeFmt},
				{field:'productName', title:'Tên sản phẩm', align:'left', resizable:false, sortable:false,
					width: SPAdjustmentOrder.promotionProductWith> 200 ? SPAdjustmentOrder.promotionProductWith : 200,
							formatter:PromotionProductFmt.productFmt},
				{field:'warehouse', title:'Kho', align:'center', resizable:false, sortable:false, width:110, formatter:PromotionProductFmt.warehouseFmt},
				{field:'stock',align:'right', resizable:false, sortable:false, width:120,
					title:'Tồn kho đáp ứng',
					formatter: PromotionProductFmt.stockFmt},
				{field:'quantity', title:'Tổng', align:'center', resizable:false, sortable:false, width:120, formatter:PromotionProductFmt.orderQuantityFmt},
				{field:'programPortion', title:'Số suất', align:'center', resizable:false, sortable:false, width:50, formatter:PromotionProductFmt.programPortionOrderFmt},
				{field:'programCode', title:'Khuyến mãi', align:'center', resizable:false, sortable:false, width:110, formatter:PromotionProductFmt.programCodeFmt},
				{field:'key', title:'Đổi KM', align:'center', resizable:false, sortable:false, width:80, formatter:PromotionProductFmt.keyFmt}
			]],	
			showFooter: true,
			singleSelect: true,
			width:$(window).width() - 40,				
			fitColumns:true,		
			scrollbarSize:0,
			autoRowHeight : true,
			onLoadSuccess: function(data){
				/*
				 * chia nhom (san pham, CTKM, duoc doi san pham khuyen mai hay ko)
				 */
				if (data !== null && data !== undefined && data.rows !== null && data.rows !== undefined &&  data.rows instanceof Array) {
					SPAdjustmentOrder._promotionProgramGroup = new Map();
					data.rows.forEach(function(item, index){
						item.r_num = index+1;
						var groupingId = Utils.XSSEncode(item.programCode + '_' + item.productGroupId + '_' + item.levelPromo) /*+ '_' + item.changeProduct*/;
						var itemsInGroup = SPAdjustmentOrder._promotionProgramGroup.get(groupingId);
						if (itemsInGroup === null || itemsInGroup === undefined) {
							SPAdjustmentOrder._promotionProgramGroup.put(groupingId, new Array());
							itemsInGroup = new Array();
						}
						itemsInGroup.push(item);
						SPAdjustmentOrder._promotionProgramGroup.put(groupingId, itemsInGroup);
					});
				}
				$('#saleOrderPromotion .discountAmount').attr('disabled','disabled');
				SPAdjustmentOrder.applyRowStripToPromotionGrid();
				SPAdjustmentOrder.mergesColRowsPromotionGrid('gridSaleOrderPromotion');
				SPCreateOrder.showTotalGrossWeightAndAmount();

				if (data == null || !data.rows || data.rows.length == 0) {
					setTimeout(function() {
						$("#h2KMDH").next().hide();
						$("#h2KMDH").hide();
					}, 100);
				} else {
					$("#h2KMDH").next().show();
					$("#h2KMDH").show();
				}
			}
		});
	},
	/**
	 * apply row-strip to product-promotion grid
	 * @author tuannd20
	 * @since 09/10/2014
	 * @return {[type]} [description]
	 */
	applyRowStripToPromotionGrid: function() {
		try {
			if (SPAdjustmentOrder._promotionProgramGroup != null) {
				SPAdjustmentOrder._promotionProgramGroup.valArray.forEach(function(rowsInGroup, index){
					if (rowsInGroup != null && rowsInGroup instanceof Array) {
						rowsInGroup.forEach(function(item){
							if (index%2 == 1) {
								$('#saleOrderPromotion table.datagrid-btable tr:eq(' + (Number(item.r_num) - 1) + ')').addClass('row-striped');
							} else {
								$('#saleOrderPromotion table.datagrid-btable tr:eq(' + (Number(item.r_num) - 1) + ')').removeClass('row-striped');
							}
						});
					}

				});
			}
		} catch (e) {
			// pass through
		}
	},
	mergesColRowsPromotionGrid:function(gridName){
		var rows = $('#' +gridName).datagrid('getRows');
		var mergesCommons = new Array();
		var mergesZV1921 = new Array();
		var mergesRowNum = new Map();
		var mergePortion = new Map();
		var listExistProgram = new Array();
		var indexRowNum = -1;
		for(var i=0;i<rows.length;++i){
			var row = rows[i];
			if(row.promotionType!=undefined && row.promotionType!=null){				
				if(row.promotionType==PromotionType.PROMOTION_FOR_ORDER
					|| (row.promotionType == PromotionType.FREE_PRODUCT && row.type == PromotionType.FREE_PRICE)){
					var cell = {index:i,colspan:2};
					mergesZV1921.push(cell);
				} else if (row.promotionType==PromotionType.PROMOTION_FOR_ZV21){
					var cell = {index:i,colspan:5};					
					mergesZV1921.push(cell);
				}	
			}	
			if(row.type==PromotionType.FREE_PRICE || row.type==PromotionType.FREE_PERCENT){
				var cell = {index:i,colspan:2};
				mergesCommons.push(cell);
			}
			if(row.promotionType != PromotionType.PROMOTION_FOR_ORDER && row.promotionType != PromotionType.PROMOTION_FOR_ZV21){
				var tmp = SPCreateOrder._promotionPortionReceivedList.get(row.programCode+"_"+row.productGroupId+"_"+row.levelPromo);
				if (tmp != undefined && tmp != null) {
					var en = false;
					if (tmp.length > 0 && tmp[0].isEdited == 0 && row.stock < row.quantity && row.hasPortion == 1) {
						en = true;
					}
					var cell = mergePortion.get(row.programCode+"_"+row.productGroupId+"_"+row.levelPromo);
					if (cell != undefined && cell != null) {
						cell.rowspan++;
						if (!cell.en) {
							cell.en = en;
						}
					} else {
						cell = {index:i, rowspan:1, en:en};
					}
					mergePortion.put(Utils.XSSEncode(row.programCode+"_"+row.productGroupId+"_"+row.levelPromo),cell);
				}
			}
		}
		var listMergePortion = mergePortion.valArray;
		//Merge rownum cua CTKM
		/*if(indexRowNum>=0){
			$('#' +gridName).datagrid('mergeCells',{   
	                	index: indexRowNum,  
	                	field: 'rownum',  
	                	rowspan: mergesRowNum.length  
	            }); 
		}*/
		/*for (var i = 0, sz = mergesRowNum.size(); i < sz; i++) {  
			$('#' +gridName).datagrid('mergeCells',{  
	                	index: mergesRowNum.valArray[i].index,
	                	field: 'rownum',
	                	rowspan: mergesRowNum.valArray[i].rowspan
	            });
	      }*/
		//Merge cac CTKM co cung muc 
		for (var i = 0, sz = listMergePortion.length; i < sz; i++){
			if (listMergePortion[i].en) {
				if (gridName == "gridPromotionProduct") {
					$("#portion_promotion_value_"+listMergePortion[i].index).removeAttr("disabled");
				} else if (gridName == "gridSaleOrderPromotion") {
					$("#portion_promotion_value_order_"+listMergePortion[i].index).removeAttr("disabled");
				} else if (gridName == "promotionGrid2") {
					$("#op_portion_promotion_value_"+listMergePortion[i].index).removeAttr("disabled");
				}
			}
			$('#' +gridName).datagrid('mergeCells',{  
	                	index: listMergePortion[i].index,  
	                	field: 'programPortion',
	                	rowspan: listMergePortion[i].rowspan  
	            });
			$('#' +gridName).datagrid('mergeCells',{  
	                	index: listMergePortion[i].index,  
	                	field: 'programCode',
	               	rowspan: listMergePortion[i].rowspan  
	            });
	      }
		for(var i = 0, sz = mergesCommons.length; i < sz; i++){  
			$('#' +gridName).datagrid('mergeCells',{  
	                	index: mergesCommons[i].index,  
	                	field: 'stock',  
	                	colspan: mergesCommons[i].colspan  
	            });
	      }		
		for(var i = 0, sz = mergesZV1921.length; i < sz; i++){  
			$('#' +gridName).datagrid('mergeCells',{   
	                	index: mergesZV1921[i].index,  
	                	field: 'productCode',  
	                	colspan: mergesZV1921[i].colspan  
	            });
      	}
	},
	pageLoad:function(options){
		SPAdjustmentOrder._orderId = $('#orderId').val();
		SPCreateOrder.bindAutoComplete();
		GridSaleBusiness.initSaleGrid();
		SPCreateOrder.loadGridFree();
		OpenProductPromotion.initOpenProductGrid();
		SPCreateOrder.createAccumulativePromotionGrid();
		if(!isAuthorize){
			$('#carId').combobox('disable');
			disableSelectbox('carId');
			disabled('deliveryDate');
			disableSelectbox('priorityId');
			$('.CalendarLink').unbind('click');		
			$('#cashierStaffCode').combobox('disable');
			disableSelectbox('cashierStaffCode');
			$('#transferStaffCode').combobox('disable');
			disableSelectbox('transferStaffCode');
			disabled('payment');
			$('#payment').hide();
			$('#backBtn').hide();
		}
		disableSelectbox('shopCodeCB');	
		$('#staffSaleCode').combobox('disable');
		disableSelectbox('staffSaleCode');	
		disabled('payment');
		enable('btnOrderSave');
		enable('approveButton');
		SPAdjustmentOrder.disableAllTextField();
//		SPAdjustmentOrder.loadPaymentHide();
		SPCreateOrder.setEventInput();	
		$('#deliveryDate, #priorityId').change(function(event) {
			if ($("#payment").prop("disabled")) {
				enable('btnOrderSave');
			}
		});
		$(' .cls_Promotion_Convfact_Value, .discountAmount').live('keypress',function(e) {
			if ($("#payment").prop("disabled")) {
				enable('btnOrderSave');	
			}
		});
		$('#cashierStaffCode, #transferStaffCode, #carId').combobox({
			onChange:function(oldVal,newVal) {
				if ($("#payment").prop("disabled")) {
					enable('btnOrderSave');
				}
			}
		});
		if($('#cashierStaffCode').combobox('getValue') == '') {
			setTimeout(function() {
				$($('#cashierStaffCode').next().find('input')[0]).select();			
			}, 2000);
		} else if($('#transferStaffCode').combobox('getValue') == '') {
			setTimeout(function() {
				$($('#transferStaffCode').next().find('input')[0]).select();			
			}, 2000);
		} else {
			setTimeout(function() {
				$('#deliveryDate').select();			
			}, 2000);
		}
		
		setTimeout(function() {
			SPAdjustmentOrder.loadSaleOrderForAdjustment(SPAdjustmentOrder.orderId, options);
		}, 200);

		/*
		 * init some other data
		 */
		SPAdjustmentOrder.PROGRAM_TYPE_MAP = new Map([
													{key: "ZM", value: "1"},
													{key: "DISPLAY_SCORE", value: "2"},
													{key: "ZD", value: "3"},
													{key: "ZH", value: "4"},
													{key: "ZT", value: "5"},
													{key: "MANUAL_PROM", value: "dummy"}
												]);
	},
	loadSaleOrderForAdjustment: function(soId, options) {
		SPCreateOrder._lstProduct = new Map();
		SPCreateOrder._lstPromotionProductIsAuto = new Map();	
		//List tong hop: sale_order_detail & order_detail_promotion
		var sale_order_detail_map = new Map();
		var list = new Array();
		var url = '/commons/adjust-load-product-for-sale?orderId=' + soId;
		if(options != undefined && options != null && options.shopCode != undefined && options.shopCode != null){
			url += '&shopCodeFilter=' + options.shopCode;
		}		
		$.getJSON(url, function(value) {
			if (value.saleOrder != null && (value.saleOrder.orderType == "SO" || value.saleOrder.orderType == "CO")) {
				SPAdjustmentOrder._isVansale = true;
			} else {
				SPAdjustmentOrder._isVansale = false;
			}
			SPAdjustmentOrder._firstLoadOrderForEdit = true;
			var errMsg = '';
			if($.isArray(value.productStocks) && value.productStocks.length > 0) {
				for (var i=0;i<value.productStocks.length;i++) {
					var tmp = value.productStocks[i];
					SPCreateOrder._PRODUCTS.put(tmp.productCode,tmp);
				}
			}
			if($.isArray(value.products) && value.products.length > 0) {
				for (var j=0;j<value.products.length;j++) {
					var tmp2 = value.products[j];
					SPCreateOrder._EQUIPMENT.put(tmp2.productCode,tmp2.productCode);
				}
			}
			if($.isArray(value.lstWarehouse) && value.lstWarehouse.length > 0) {
				for (var k=0;k<value.lstWarehouse.length;k++) {
					var tmp3 = value.lstWarehouse[k];
					var obj =  new Object();
					obj.warehouseId = tmp3.warehouseId;
					obj.warehouseName = tmp3.warehouseName;
					obj.warehouseCode = tmp3.warehouseCode;
					obj.seq = tmp3.sep;
					GridSaleBusiness._mapWarehouse.put(tmp3.warehouseId,obj);
				}
			}
			//Lay danh sach chuong trinh ho tro thuong mai
			if($.isArray(value.listCommercialSupport) && value.listCommercialSupport.length > 0) {
	        	GridSaleBusiness.listCS = value.listCommercialSupport;
	        	GridSaleBusiness.mapCS = new Map();
	        	GridSaleBusiness.listCSCode = new Array();
				for(var i = 0; i < value.listCommercialSupport.length; i++) {
					GridSaleBusiness.mapCS.put(GridSaleBusiness.listCS[i].code, GridSaleBusiness.listCS[i]);
					GridSaleBusiness.listCSCode.push(GridSaleBusiness.listCS[i].code);
				}
	        }
			//bo set timeout để khi load keyshop sẽ lấy đc tồn kho thực + số lượng đã đặt 
//			setTimeout(function() {
				if(SPCreateOrder._PRODUCTS.size() > 0) {
					for (var l=0;l<SPCreateOrder._PRODUCTS.keyArray.length;l++) {
						var tmp = SPCreateOrder._PRODUCTS.valArray[l];
						var productId = tmp.productId;
						SPCreateOrder._mapPRODUCTSbyWarehouse.put(productId, value.mapProductByWarehouse[productId]);
					}
				}
			//Lay danh sach chuong trinh ho tro thuong mai
	        if($.isArray(value.commercials) && value.commercials.length > 0) {
	        	GridSaleBusiness.listCS = value.commercials;
	        	GridSaleBusiness.mapCS = new Map();
	        	GridSaleBusiness.listCSCode = new Array();
				for(var i = 0; i < value.commercials.length; i++) {
					GridSaleBusiness.mapCS.put(GridSaleBusiness.listCS[i].code, GridSaleBusiness.listCS[i]);
					GridSaleBusiness.listCSCode.push(GridSaleBusiness.listCS[i].code);
				}
	        }
	        //Load danh sach sp ban theo Kho
	        SPCreateOrder._mapHasPromotion = new Map();
	        var lstWarehouselessProduct = new Array();	// danh sach cac san pham ko co ton kho
	        var lstProductParticipatingPromotionProgram = new Array();	// danh sach cac san pham co tham gia CTKM tu dong (khi tao don tren TABLET thi chua co tham gia)
	        if($.isArray(value.sale_order_lot) && value.sale_order_lot.length > 0) {
				for(var i = 0, rowIndex = 0; i < value.sale_order_lot.length; i++) {
					var obj = value.sale_order_lot[i];
					var soDetailId = obj.id;
					var lotId = obj.saleOrderLotId;
					var productId = obj.productId;
					var productCode = obj.productCode;
					var productName = obj.productName;
					var availableQuantity = formatQuantity(obj.availableQuantity,obj.convfact);
					/*if (Number(obj.availableQuantity) === 0) {
						availableQuantity = formatQuantity(obj.quantity, obj.convfact);
					}*/
					var price = obj.price;
					var pkPrice = obj.pkPrice;
					var pkPriceNotVat = obj.pkPriceNotVat;
					var amount = obj.amount;
					var warehouseId = obj.warehouseId;
					var warehouseName = obj.warehouseName;
					var commercialSupport = obj.programCode;
					var convfact = obj.convfact;					
					var quantity = Number(obj.quantity);
					var quantityPackage = Number(obj.quantityPackage);
					var quantityRetail = Number(obj.quantityRetail);
					var convfact_value = quantityPackage + '/' + quantityRetail;
					var oldQuantity = Number(obj.oldQuantity);
					var grossWeight = Number(obj.grossWeight);
					var commercialSupportType = obj.promotionType;
					var joinProgramCode = obj.joinProgramCode;
					var stock = Number(obj.stockQuantity);
					var discountAmount = obj.discountAmount;
					var discountPercent = obj.discountPercent;
					var isFree = obj.isFreeItem;
					var product = new Object();
					var row = new Object();
					product.productCode = productCode;
					product.productName = productName;
					product.price = price;
					product.pkPrice = pkPrice;
					product.pkPriceNotVat = pkPriceNotVat;	
					product.warehouseName = warehouseName;
					product.availableQuantity = availableQuantity;
					product.quantity= quantity;
					product.quantityPackage= quantityPackage;
					product.quantityRetail= quantityRetail;
					product.oldQuantity = oldQuantity;
					product.discountAmount = discountAmount;
					product.discountPercent = discountPercent;
					product.amount = (price*quantity);
					product.commercialSupport = commercialSupport;
					product.soDetailId = soDetailId;
					product.lotId = lotId;
					product.warehouseId = warehouseId;
					product.convfact = convfact;
					product.productId = productId;
					product.convfact_value = quantity;
					product.grossWeight = grossWeight;
					product.stock = stock;
					product.commercialSupportType = commercialSupportType;
					product.joinProgramCode = joinProgramCode;
					product.isFree = isFree;
					if(isNullOrEmpty(commercialSupport)) {
						SPCreateOrder._lstProduct.put(lotId, product);
						SPCreateOrder._mapHasPromotion.put(productCode, 0);
					} else {
						SPCreateOrder._lstProduct.put(lotId+'-' +commercialSupport, product);
						SPCreateOrder._mapHasPromotion.put(productCode, 1);
					}
					
					row.a = productCode;
					row.b = productName;
					row.c = formatCurrency(price);
					row.j = formatCurrency(pkPrice);	
					row.d = warehouseName;
					row.e = availableQuantity;
					row.f= convfact_value;
//					row.g = formatCurrency(price*quantity);
					row.g = formatCurrency(amount);
					row.h = formatCurrency(discountAmount);
					//row.i = commercialSupport;
					if (isNullOrEmpty(joinProgramCode) || product.isFree == 0) {
						//nếu program ko có hoặc là SP bán thì lấy program theo map đã có sẵn lấy theo hàm SPCreateOrder.customerDetails
						var currentProduct = GridSaleBusiness.mapSaleProduct.get(productCode);
						if (currentProduct && currentProduct.promotionProgramCode) {
							joinProgramCode = currentProduct.promotionProgramCode;
							//fix bug nên tạm thời không check đơn hàng có KM mới và cần tính tiền lại => đóng dòng dưới
//							lstProductParticipatingPromotionProgram.push(productCode);
						}
					}
					row.i = joinProgramCode;
					sale_order_detail_map.put(lotId,row);
					if (!warehouseId && obj.accumulation != 1) {
						lstWarehouselessProduct.push(productCode);
					}
					
					if (product.isFree == 0) {
						if (!isNullOrEmpty(discountAmount)) {
							SPCreateOrder._mapDiscountAmount.put(i,discountAmount);
							SPCreateOrder._mapDiscountPercent.put(i,discountPercent);
						}
						list.push(row);
						GridSaleBusiness._mapWarehouseIdSelect.put(rowIndex++, warehouseId);
					} else {
						// xu ly load san pham km huy, doi, tra, tay vao ds san pham ban & tinh lai amount
						var cs = GridSaleBusiness.mapCS.get(product.commercialSupport);
						if (product.commercialSupport && !isNullOrEmpty(product.commercialSupport)
							&& cs != null && SPAdjustmentOrder.PROGRAM_TYPE_MAP.get(cs.type)) {
							row.g = 0;
							row.i = product.commercialSupport;
							product.amount = 0;
							list.push(row);
							GridSaleBusiness._mapWarehouseIdSelect.put(rowIndex++, warehouseId);
						}
					}
				}
				SPAdjustmentOrder._loadDetail=1;
	        }
	        var data = new Array();
	        
	        for(var i = 0; i < list.length; i++) {
	        	if (SPCreateOrder._isAllowShowPrice){// cau hinh co show gia
					var row = new Array();
					row.push(list[i].a);
					row.push(list[i].b);					
					row.push(list[i].j);//gia thung
					row.push(list[i].c);// gia le
					row.push(list[i].d);
					row.push(list[i].e);
					row.push(list[i].f);
					row.push(list[i].g);
					row.push(list[i].h);
					row.push(list[i].i);
					row.push(null);
					data.push(row);
				} else {
		        	var row = new Array();
					row.push(list[i].a);
					row.push(list[i].b);					
					/*row.push(list[i].j);//gia thung
					row.push(list[i].c);// gia le
	*/				row.push(list[i].d);
					row.push(list[i].e);
					row.push(list[i].f);
	//				row.push(list[i].g);//thanh tien
					row.push(list[i].h);
					row.push(list[i].i);
					row.push(null);
					data.push(row);
				}
	        }
			
			SPCreateOrder._isCreatingSaleOrder = true;
	        var hsSale = $('#gridSaleData').handsontable('getInstance');
			hsSale.updateSettings({data: data , columns: GridSaleBusiness.getColumnProperty(), minSpareRows: 1}); // 27.09.2014
//			hsSale.render();
			//hsSale.alter('insert_row',data.length);
			SPCreateOrder.showTotalGrossWeightAndAmount();
			if ($("#loadingProduct:not(:hidden)").length > 0) {
				$("#loadingProduct").hide();
			}

			SPCreateOrder._mapPromoDetail = new Map();
			if (value.lstPromoDetails instanceof Array && value.lstPromoDetails.length > 0) {
				var promos = null;
				var pdId = null;
				for (var ij = 0, szj = value.lstPromoDetails.length; ij < szj; ij++) {
					if (value.lstPromoDetails[ij].saleOrderDetail) {
						pdId = value.lstPromoDetails[ij].saleOrderDetail.product.id;
						promos = SPCreateOrder._mapPromoDetail.get(pdId);
						if (promos == null) {
							promos = [];
						}
						value.lstPromoDetails[ij].saleOrderDetail = null;
						promos.push(value.lstPromoDetails[ij]);
						SPCreateOrder._mapPromoDetail.put(pdId, promos);
					}
				}
			}

			SPAdjustmentOrder._changedStructurePromotionProgramsInSaleOrder = value.changedStructurePromotionProgramsInSaleOrder;
			SPCreateOrder._saleOrderQuantityReceivedList = value.saleOrderQuantityReceivedList;
			//Luu so suat cua KM don hang vao map de luu du lieu
			SPCreateOrder._promotionOrderReceivedList = new Map();
			//Luu tat ca CTKM theo muc de ap dung chinh sua so suat
			SPCreateOrder._promotionPortionReceivedList = new Map();
			if (value.saleOrderQuantityReceivedList) {
				for (var i = 0, sz = value.saleOrderQuantityReceivedList.length; i < sz; i++) {
					var tmp = value.saleOrderQuantityReceivedList[i];
					var listProduct = new Array();
					value.order_detail_promotion.forEach(function(item, tempIndex) {
						if (item.programCode == tmp.promotionCode && item.productGroupId == tmp.productGroupId
							&& item.levelPromo == tmp.promotionLevel) {
							item.quantityReceived = tmp.quantityReceived;
							item.maxQuantityReceived = tmp.maxQuantityReceived;
							item.maxQuantityReceivedTotal = tmp.maxQuantityReceivedTotal;
							item.saleOrderDetail = item;
							item.saleOrderDetail.product = item;
							listProduct.push(item);
						}
					});
					SPCreateOrder._promotionPortionReceivedList.put(Utils.XSSEncode(tmp.promotionCode+"_"+tmp.productGroupId+"_"+tmp.promotionLevel), listProduct);
					if (tmp.promotionProgram.type == 'ZV19' || tmp.promotionProgram.type == 'ZV20' || tmp.promotionProgram.type == 'ZV21') {
						SPCreateOrder._promotionOrderReceivedList.put(Utils.XSSEncode(tmp.promotionProgram.promotionProgramCode+"_"+tmp.productGroupId+"_"+tmp.promotionLevel), tmp);
					}
				}
			}
			//Luu thong tin muc cua tung CTKM 
			if (SPCreateOrder.programes != undefined && SPCreateOrder.programes != null && value.saleOrderQuantityReceivedList) {
				for (var i = 0, sz = SPCreateOrder.programes.length; i < sz; i++) {
					var obj = SPCreateOrder.programes[i];
					var tmp = value.saleOrderQuantityReceivedList[i];
					if (tmp != undefined && tmp != null && tmp.promotionCode == obj.promotionProgramCode) {
						SPCreateOrder.programes[i].promotionLevel =  tmp.promotionLevel;
					};
				};
			}
			
			SPCreateOrder._lstPromotionProductIsAuto = new Map();
			SPCreateOrder._lstPromotionOpenProduct = new Map();
			SPCreateOrder._lstPromotionProductForOrderIsAuto = new Map();
			var mapProgramLevelPromoGroup = new Map();
			var groupId = 1;
			//Danh sach san pham KM
			if($.isArray(value.order_detail_promotion) && value.order_detail_promotion.length > 0) {
				var hasHeader = false;
				for(var i = 0; i < value.order_detail_promotion.length; i++) {
					var obj = value.order_detail_promotion[i];
					var pproduct = {};	
					pproduct.isEdited = Number(obj.isEdited);
					if (!isNullOrEmpty(obj.productCode)) {
						pproduct.productCode = obj.productCode;
						pproduct.productName = obj.productName;
						pproduct.ppIndex = i;
					} else {
						pproduct.productCode = '';
						pproduct.productName = '';
					}
					if (!isNullOrEmpty(obj.programCode)) {
						pproduct.programCode = obj.programCode;
						if (!isNullOrEmpty(obj.productCode)) {
							pproduct.convfact = obj.convfact;
							pproduct.productId = obj.productId;
						} else {
							pproduct.convfact = '';
						}
						pproduct.quantity=obj.quantity;
						if (isNullOrEmpty(pproduct.quantity)) {
							pproduct.quantiy = 0;
						}
						pproduct.maxQuantity = obj.maxQuantityFree;
						pproduct.maxQuantityCk = obj.maxQuantityFreeCk;
						pproduct.maxQuantityFreeTotal = obj.maxQuantityFreeTotal; /** vuongmq; 08/01/2015; hien thi maxQuantityFreeTotal so luong tinh khuyen mai*/
						var proQuatity = parseInt(pproduct.quantity);
						var stock = parseInt(obj.availableQuantity);
						if(proQuatity > stock && obj.accumulation != 1) {
							if($('#disabledButton').length!=0 && $('#disabledButton').val()!='1') {//nếu là đơn hàng chưa duyệt thì mới thông báo
								errMsg = 'Mặt hàng ' +Utils.XSSEncode(pproduct.productCode+' - ' +pproduct.productName)+' hiện không đủ trong kho. Vui lòng chọn mặt hàng khác hoặc điều chỉnh số lượng khuyến mãi.';
							}
						}
					} else {
						pproduct.programCode = '';
					}
					pproduct.type = Number(obj.type);
					pproduct.key = obj.keyList;
					pproduct.changeProduct = obj.changeProduct;
					pproduct.stock = isNullOrEmpty(obj.availableQuantity)?0:obj.availableQuantity;
					pproduct.grossWeight = isNullOrEmpty(obj.grossWeight)?0:obj.grossWeight;		
					pproduct.promotionType = obj.ppType;
					pproduct.commercialSupport = obj.programCode;
					pproduct.commercialSupportType = obj.promotionType;
					pproduct.isFree = 1;
					pproduct.rownum = i +1;
					pproduct.warehouse = obj.warehouse;
					pproduct.levelPromo = obj.levelPromo;
					pproduct.productGroup = obj.productGroup;
					pproduct.productGroupId = obj.productGroupId;
					pproduct.groupLevel = obj.groupLevel;
					pproduct.openPromo = obj.openPromo;
					pproduct.accumulation = obj.accumulation;
					pproduct.hasPortion = obj.hasPortion;
					var PROMOTION_TYPE_ZV21 = "ZV21";
					if (pproduct.promotionType==PromotionType.PROMOTION_FOR_ORDER
						|| pproduct.promotionType==PromotionType.PROMOTION_FOR_ZV21
						|| pproduct.promotionType == PROMOTION_TYPE_ZV21){
						if (pproduct.promotionType==PromotionType.PROMOTION_FOR_ORDER) {
							pproduct.productCode = 'Khuyến mãi đơn hàng';
							pproduct.discountAmount = obj.discountAmount;
							pproduct.maxDiscountAmount = obj.discountAmount;
							
						} else if (pproduct.promotionType==PromotionType.PROMOTION_FOR_ZV21
								|| pproduct.promotionType == PROMOTION_TYPE_ZV21) {
							pproduct.promotionType = PromotionType.PROMOTION_FOR_ORDER_TYPE_PRODUCT;
							if (!obj.warehouse && obj.accumulation != 1) {
								lstWarehouselessProduct.push(obj.productCode);
							}
						}

						var key = Utils.XSSEncode(pproduct.programCode + '_' + pproduct.levelPromo);
						if (!mapProgramLevelPromoGroup.get(key)) {
							if (!hasHeader && pproduct.promotionType==PromotionType.PROMOTION_FOR_ORDER_TYPE_PRODUCT) {
								var groupHeaderRow = {};
								$.extend(true, groupHeaderRow, pproduct);
								groupHeaderRow.productCode = 'Khuyến mãi đơn hàng';
								groupHeaderRow.promotionType = PromotionType.PROMOTION_FOR_ZV21;
								groupHeaderRow.groupId = groupId;
								SPCreateOrder._lstPromotionProductForOrderIsAuto.keyArray.splice(SPCreateOrder._lstPromotionProductIsAuto.size(), 0, -1);
								SPCreateOrder._lstPromotionProductForOrderIsAuto.valArray.splice(SPCreateOrder._lstPromotionProductIsAuto.size(), 0, groupHeaderRow);
								hasHeader = true;
							}
							mapProgramLevelPromoGroup.put(key, groupId++);
						}

						pproduct.groupId = mapProgramLevelPromoGroup.get(key);
						if(pproduct.canChaneMultiProduct) {
							SPCreateOrder._lstPromotionProductForOrderIsAuto.keyArray.splice(SPCreateOrder._lstPromotionProductIsAuto.size(), 0, pproduct.programCode+'_' +pproduct.productId);
							SPCreateOrder._lstPromotionProductForOrderIsAuto.valArray.splice(SPCreateOrder._lstPromotionProductIsAuto.size(), 0, pproduct);
						} else {
							SPCreateOrder._lstPromotionProductForOrderIsAuto.put(i, pproduct);				
						}
						
					} else if (pproduct.type == PromotionType.FREE_PRODUCT) {
						pproduct.myTime = obj.myTime;
						if(pproduct.myTime == 0 || pproduct.myTime == 'null') {
							pproduct.myTime = null;
						}
						if (pproduct.openPromo == 1) {
							if(pproduct.canChaneMultiProduct) {
								SPCreateOrder._lstPromotionOpenProduct.keyArray.splice(SPCreateOrder._lstPromotionOpenProduct.size(), 0, pproduct.programCode+'_' +pproduct.productId);
								SPCreateOrder._lstPromotionOpenProduct.valArray.splice(SPCreateOrder._lstPromotionOpenProduct.size(), 0, pproduct);
							} else {
								SPCreateOrder._lstPromotionOpenProduct.put(i, pproduct);
							}
						} else if (pproduct.accumulation != 1) {
							if(pproduct.canChaneMultiProduct) {
								SPCreateOrder._lstPromotionProductIsAuto.keyArray.splice(SPCreateOrder._lstPromotionProductIsAuto.size(), 0, pproduct.programCode+'_' +pproduct.productId);
								SPCreateOrder._lstPromotionProductIsAuto.valArray.splice(SPCreateOrder._lstPromotionProductIsAuto.size(), 0, pproduct);
							} else {
								SPCreateOrder._lstPromotionProductIsAuto.put(i, pproduct);
							}
						}
						if (!obj.warehouse && obj.accumulation != 1) {
							lstWarehouselessProduct.push(obj.productCode);
						}
					} else if (pproduct.type == PromotionType.FREE_PRICE && pproduct.promotionType == PromotionType.FREE_PRODUCT) {
						pproduct.productCode = 'Khuyến mãi mở mới';
						pproduct.discountAmount = obj.discountAmount;
						pproduct.maxDiscountAmount = obj.discountAmount;
						if (pproduct.canChaneMultiProduct) {
							SPCreateOrder._lstPromotionOpenProduct.keyArray.splice(SPCreateOrder._lstPromotionOpenProduct.size(), 0, pproduct.programCode+'_' +pproduct.productId);
							SPCreateOrder._lstPromotionOpenProduct.valArray.splice(SPCreateOrder._lstPromotionOpenProduct.size(), 0, pproduct);
						} else {
							SPCreateOrder._lstPromotionOpenProduct.put(i, pproduct);
						}
					}
				}

				/*
				 * sort data ZV01-18
				 */
				SPCreateOrder._lstPromotionProductIsAuto.valArray.sort(function(o1, o2){
					var id = '-1413001269113';
					/*if (isZV21) {
						if (o1.groupId != o2.groupId) {
							return o1.groupId > o2.groupId;
						}
					}*/
					if (o1.programCode != o2.programCode) {
						return o1.programCode > o2.programCode;
					}
					if (o1.productCode.trim().indexOf(" ") > 0) {
						return false;
					}
					if (o2.productCode.trim().indexOf(" ") > 0) {
						return true;
					}
					if (Number(o1.levelPromo) != Number(o2.levelPromo)) {
						return o1.levelPromo > o2.levelPromo;
					}
					if (!isNaN(o1.productGroupId) && !isNaN(o2.productGroupId)
						&& Number(o1.productGroupId) != Number(o2.productGroupId)) {
						return o1.productGroupId < o2.productGroupId;
					}
					if (Number(o1.changeProduct) != Number(o2.changeProduct)) {
						return Number(o1.changeProduct) > Number(o2.changeProduct);
					}
					if (o1.productCode != o2.productCode) {
						return o1.productCode > o2.productCode;
					}
					return o1.warehouse.seq > o2.warehouse.seq;
				});
				/*
				 * re-create order number
				 */
				SPCreateOrder._lstPromotionProductIsAuto.valArray.forEach(function(item, index){
					SPCreateOrder._lstPromotionProductIsAuto.keyArray[index] = index;
					item.rownum = index + 1;
				});
				OpenProductPromotion.sortProductList(SPCreateOrder._lstPromotionOpenProduct, false);
				SPCreateOrder._lstPromotionOpenProduct.valArray.forEach(function(item, index){
					SPCreateOrder._lstPromotionOpenProduct.keyArray[index] = index;
				});

				/*
				 * ZV21: sort rows by program_code, promo_level, can_change_product
				 */
				SPCreateOrder._lstPromotionProductForOrderIsAuto.valArray.sort(function(o1, o2){
					var id = '-1413001269113';
					/*if (isZV21) {
						if (o1.groupId != o2.groupId) {
							return o1.groupId > o2.groupId;
						}
					}*/
					if (o1.programCode != o2.programCode) {
						return o1.programCode > o2.programCode;
					}
					if (o1.productCode.trim().indexOf(" ") > 0) {
						return false;
					}
					if (o2.productCode.trim().indexOf(" ") > 0) {
						return true;
					}
					if (Number(o1.levelPromo) != Number(o2.levelPromo)) {
						return o1.levelPromo > o2.levelPromo;
					}
					if (!isNaN(o1.productGroupId) && !isNaN(o2.productGroupId)
						&& Number(o1.productGroupId) != Number(o2.productGroupId)) {
						return o1.productGroupId < o2.productGroupId;
					}
					if (Number(o1.changeProduct) != Number(o2.changeProduct)) {
						return Number(o1.changeProduct) > Number(o2.changeProduct);
					}
					if (o1.productCode != o2.productCode) {
						return o1.productCode > o2.productCode;
					}
					return o1.warehouse.seq > o2.warehouse.seq;
				});

				/*
				 * re-calculate row's sequence number
				 */
				SPCreateOrder._lstPromotionProductForOrderIsAuto.keyArray.forEach(function(item, index){
					SPCreateOrder._lstPromotionProductForOrderIsAuto.keyArray[index] = index;
					SPCreateOrder._lstPromotionProductForOrderIsAuto.valArray[index].rownum = index + 1;
				});
				for (var ij = 0, szij = SPCreateOrder._lstPromotionProductForOrderIsAuto.size(); ij < szij; ij++) {
					if (SPCreateOrder._lstPromotionProductForOrderIsAuto.valArray[ij].programCode
						&& SPCreateOrder._lstPromotionProductForOrderIsAuto.valArray[ij].programCode.indexOf(" ") < 0) {
						SPCreateOrder._isUsingZVCode = SPCreateOrder._lstPromotionProductForOrderIsAuto.valArray[ij].programCode;
					}
				}

				setTimeout(function () {
					SPCreateOrder.fillTablePromotionProductForOrder(SPCreateOrder._lstPromotionProductForOrderIsAuto);
					SPCreateOrder.fillTablePromotionProduct(SPCreateOrder._lstPromotionProductIsAuto);
					OpenProductPromotion.loadOpenProductGrid(SPCreateOrder._lstPromotionOpenProduct);
				}, 300);
			}
			setTimeout(function() {SPCreateOrder.updatePromotionGrids();}, 1700);
//			setTimeout(function() { SPCreateOrder.loadGridAccumulativeForEdit(value); }, 1300); // load grid khuyen mai tich luy

//			if (options && options.priceWarning && options.priceWarning == 1) {
//			} else {
//				disabled('payment');
//			}
			//Khi load thì luôn hiện nút tính tiền và ẩn nút lưu
			enable('payment');
			disabled('btnOrderSave');
			
			SPAdjustmentOrder._editingSaleOrder = null;
			var errorHappended = false;
			if (value.saleOrder && value.saleOrder.orderType && value.saleOrder.orderType == orderType.IN) {
				SPAdjustmentOrder._editingSaleOrder = value.saleOrder;
				setTimeout(function() {
					/*
					 * show errors on sale order
					 */
					if (lstWarehouselessProduct.length != 0) {
						var productStr = lstWarehouselessProduct.reduce(function(prev, curr){
											return prev.indexOf(curr) != -1 ? prev : prev + ', ' + Utils.XSSEncode(curr);
										});
						$('#generalInfoDiv #GeneralInforErrors').html('Các sản phẩm: ' + Utils.XSSEncode(productStr) + ' không có tồn kho. Vui lòng nhập kho cho sản phẩm.' + (errMsg.trim() ? '<br>' + errMsg.trim() : '')).show();
						/*setTimeout(function() {
							$('#GeneralInforErrors').html('').hide();
						}, 3000);*/
						enable('btnOrderDeny');
						enable('btnOrderCancel');
						disabled('btnOrderApproved');
						errorHappended = true;
					}
				}, 1000);
			}

			if (lstProductParticipatingPromotionProgram.length > 0) {
				setTimeout(function() {
					var message = $('#generalInfoDiv #GeneralInforErrors').html() + '<br>';
					if (!errorHappended) {
						message = '';
					}
					message += 'Đơn hàng có sản phẩm tham gia CTKM. Vui lòng tính tiền lại để tính lại khuyến mãi.';
					$('#generalInfoDiv #GeneralInforErrors').html(message).show();
					enable('payment');
					disabled('btnOrderSave');
					disabled('btnOrderApproved');
					errorHappended = true;
				}, 1000);
			}

			if (value.lstPromotion) {
				setTimeout(function() {
					var message = $('#generalInfoDiv #GeneralInforErrors').html() + '<br>';
					if (!errorHappended) {
						message = '';
					}
					message += '(Các) chương trình khuyến mãi ' + Utils.XSSEncode(value.lstPromotion) + ' đã hết hạn. Vui lòng tính tiền lại.';
					$('#generalInfoDiv #GeneralInforErrors').html(message).show();
					enable('payment');
					disabled('btnOrderSave');
					disabled('btnOrderApproved');
				}, 1000);
			}

			if (SPAdjustmentOrder._changedStructurePromotionProgramsInSaleOrder && SPAdjustmentOrder._changedStructurePromotionProgramsInSaleOrder.length > 0) {
				var errMsg = "(Các) Chương trình khuyến mãi: " + SPAdjustmentOrder._changedStructurePromotionProgramsInSaleOrder.join(', ') + " đã thay đổi cơ cấu. Vui lòng tính tiền lại để hưởng KM theo cơ cấu mới.";
				$.messager.alert('Cảnh báo', errMsg, 'warning');
				enable('payment');
				disabled('btnOrderSave');
			}
			
			//Lấy dữ liệu keyshop mà khách hàng có thể nhận vào listKeyShop
			SPCreateOrder.getKeyShopData(value);
			//load dữ liêu keyshop trong saleOrderDetail lên màn hình
			SPCreateOrder.loadKeyShopEdit(value);
			//----------------------------
		});
	},
	disabledProductCols:function(rowIndex){
		if(!isAuthorize){
			return false;
		}		
		var productCode = $('.datagrid-row-editing td[field=productId] input.validatebox-text').val();
		var product = $('#saleProductGrid').datagrid('getRows')[rowIndex];
		if(product!=null && !isNullOrEmpty(product.productId) && SPCreateOrder._lstSaleProductMap.get(product.productId)!=null){		
			$('td[field=productId] div table tbody tr td input').css('display','none');
			$('td[field=productId] div table tbody tr td span').css('display','none');
			$('td[field=productId] div table tbody tr td').append('<a class="product-detail" index=' +rowIndex+' val="' +Utils.XSSEncode(productCode)+'" href="javascript:void(0)">' +Utils.XSSEncode(productCode)+'</a>');
			$('.datagrid-row-editing td[field=convfact_value] input').focus();
		}else{	
			$('td[field=productId] input.validatebox-text').attr('disabled',false);
			$('td[field=productId] input.validatebox-text').focus();
		}
	},
	addStyleRowSaleProduct:function(index,product){
		var color = WEB_COLOR.BLACK;
		if(product==null){
			return color;
		}else{
			var availableQuantity = product.availableQuantity;			
			var convfact = product.convfact;			
			if(availableQuantity==null || convfact==null){
				availableQuantity = 0;
			}
			availableQuantity = getQuantity(availableQuantity,convfact);
			if(Number(availableQuantity)<0){
				return WEB_COLOR.RED;
			}
			if(isNullOrEmpty(product.convfact_value)){
				return color;
			}
			var quantity = Number(getQuantity(product.convfact_value,convfact));
			if(Number(availableQuantity)<quantity){
				return WEB_COLOR.ORANGE;
			}
		}		
		return color;
	},	
	addStyleRowSalePromotion:function(index,product){
		var color = WEB_COLOR.BLACK;
		if(product==null){
			return color;
		}else{
			var stock = product.stock;
			var convfact = product.convfact;			
			if(stock==null || convfact==null){
				return color;
			}

			if(Number(stock)<0){
				return WEB_COLOR.RED;
			}
			if(isNullOrEmpty(product.quantity)){
				return color;
			}
			var quantity = Number(getQuantity(product.quantity,convfact));
			if(Number(stock)<quantity){
				return WEB_COLOR.ORANGE;
			}
		}		
		return color;
	},
	addStyleRowSalePromotionProduct:function(index,product){
		var color = WEB_COLOR.BLACK;
		if(product==null){
			return color;
		}else{
			var stock = 0;
			if (SPAdjustmentOrder._orderId != null && SPAdjustmentOrder._orderId != undefined) {
				stock = product.stock;
			} else {
				stock = product.stockTotal ? product.stockTotal.availableQuantity : 0;
			}
			
			var convfact = product.convfact;	
			var quantity = Number(getQuantity(product.quantity,convfact));	
			if(isNullOrEmpty(product.quantity)){
				return color;
			}
			if(stock==null || convfact==null){
				return color;
			}
			if(Number(stock)<0){
				return WEB_COLOR.RED;
			}
			if(Number(stock)<quantity){
				return WEB_COLOR.ORANGE;
			}	
		}		
		return color;
	},
	addStyleRowProductDialog:function(index,product){
		var color = WEB_COLOR.BLACK;
		if(product==null){
			return color;
		}else{
			var availableQuantity = product.availableQuantity;
			if(!isNullOrEmpty(SPAdjustmentOrder.orderId)){
				availableQuantity = product.stock;
			}
			var convfact = product.convfact;			
			if(availableQuantity==null || convfact==null){
				availableQuantity = 0;
			}
			if(Number(availableQuantity)<0){
				return WEB_COLOR.RED;
			}			
			var convfact_value = $('#productRow_' +index).val();
			if(isNullOrEmpty(convfact_value)){
				return color;
			}
			var quantity = Number(getQuantity(convfact_value.trim(),convfact));
			if(Number(availableQuantity)<quantity){
				return WEB_COLOR.ORANGE;
			}			
		}		
		return color;
	},
	getParams:function(){
		var shopCode=$('#shopCode').val().trim();
		var customerCode=$('#customerCode').val().trim();
		var data = new Object();
		if( shopCode != null && shopCode != '' && shopCode != undefined){
			data.shopCode = shopCode;
		}
		if( customerCode != null && customerCode != '' && customerCode != undefined){
			data.customerCode = customerCode;
		}
		return data;
	},
	viewDetailProduct:function(productCode){
		var params=SPAdjustmentOrder.getParams();
		params.productCode=productCode;
		Utils.getHtmlDataByAjax(params, '/commons/products-details',
				function(data) {
					try {
						var myData = JSON.parse(data);
						alert(myData);
						CommonSearch.showProductInfo(myData.product.productCode,myData.product.productName
								,myData.product.convfact,myData.product.safetyStock,myData.price
								, ''  ,myData.promotion);
					} catch (err) {
						return;
					}
				}, 'loadingSearch', 'POST');
	},
	showCommercialSupportCode:function(t){
		var arrParam = new Array();
		var param = {};
		param.name = "shopCode";
		param.value = $('#shopCode').val();
		arrParam.push(param);
		CommonSearch.searchPromotionOnDialog(function(data){
			$(t).val(data.code);
		},arrParam);
	},
	removeOrder:function(){
		$('#serverErrors').hide();
		$('#GeneralInforErrors').hide();
		var status=$('#saleOrderStatus').val().trim();
		var orderId=$('#orderId').val().trim();
		if( status == null || status == undefined || status == '' || status =='1'){
			$('#GeneralInforErrors').html('Đơn hàng không hợp lệ hoặc đã được duyệt').show();
			var tm = setTimeout(function(){
				$('#GeneralInforErrors').html('').hide();
				clearTimeout(tm);					
			}, 5000);
			return false;
		}
		if(orderId == null || orderId == undefined || orderId == ''){
			$('#GeneralInforErrors').html('Bạn chưa chọn đơn hàng').show();
			var tm = setTimeout(function(){
				$('#GeneralInforErrors').html('').hide();
				clearTimeout(tm);					
			}, 5000);
			return false;
		}
		var params=new Object();				
		params.orderId=orderId;
		$.messager.confirm('Xác nhận',  'Bạn có muốn xóa đơn hàng này không?', function(r){
			if (r){		
				
				Utils.saveData(params, '/sale-product/adjust-order/remove-order', null, 'serverErrors', function(data) {
					$('#Save_successMsg').html('Xóa đơn hàng thành công').show();
					setTimeout(function(){window.location.href = '/sale-product/search-sale/info';},2000);
				}, null, null, null, null, null);
			}
		});	
	},
	approvedOrder:function(){
		//Duyệt đơn hàng
		var status=$('#saleOrderStatus').val().trim();
		var orderId=$('#orderId').val().trim();
		if( status == null || status == undefined || status == '' || status =='1'){
			$('#GeneralInforErrors').html('Đơn hàng không hợp lệ hoặc đã được duyệt').show();
			var tm = setTimeout(function(){
				$('#GeneralInforErrors').html('').hide();
				clearTimeout(tm);					
			}, 5000);
			return false;
		}
		if(orderId == null || orderId == undefined || orderId == ''){
			$('#GeneralInforErrors').html('Bạn chưa chọn đơn hàng').show();
			var tm = setTimeout(function(){
				$('#GeneralInforErrors').html('').hide();
				clearTimeout(tm);					
			}, 5000);
			return false;
		}
		var params=new Object();
		params.orderId=orderId;
		Utils.saveData(params, '/sale-product/adjust-order/approved-order', null, 'errMsg', function(data) {				
				if(!data.error){
					$.cookie('approvedOrder', 1);
					isAuthorize = false;
					//TUNGTT
					if(!isAuthorize){
						//disableSelectbox('carId');
						$('#carId').combobox('disable');
						disableSelectbox('carId');
						
						disabled('deliveryDate');
						disableSelectbox('priorityId');
						$('.CalendarLink').unbind('click');		
						$('#cashierStaffCode').combobox('disable');
						disableSelectbox('cashierStaffCode');
						$('#transferStaffCode').combobox('disable');
						disableSelectbox('transferStaffCode');
						$('#payment').hide();
						//tungtt
						$('#backBtn').hide();
						$('#approveButton').hide();
						$('#approveButton').next().hide();
						$('#approveButton').next().next().hide();
					}
					$('#staffSaleCode').combobox('disable');
					disableSelectbox('staffSaleCode');	
					disabled('payment');
					enable('btnOrderSave');
					enable('approveButton');
					$('#orderNumber').val(data.newOrderNumber);
					SPAdjustmentOrder.disableAllTextField();
					//setTimeout(function(){window.location.reload(); }, 0);
				}else{
					$('#serverErrors').html(result.errMsg).show();
				}
			}, 'loading2', null, null,function(data){
				$('#serverErrors').html(data.errMsg).show();
		});	
	},
	checkApprovedStatus:function(){
		var approvestatus= $('#saleOrderStatus').val().trim();
		if( approvestatus != null && approvestatus != '' && approvestatus != undefined && approvestatus!=1){
			return true;
		} else{
			$('#GeneralInforErrors').html('Đơn hàng không hợp lệ hoặc đã được duyệt. Không thể thay đổi').show();
			var tm = setTimeout(function(){
				$('#GeneralInforErrors').html('').hide();
				clearTimeout(tm);					
			}, 5000);
			return false;
		}
	},
	saveOrder:function(type){
		$('#GeneralInforErrors').html('').hide();
		$('#serverErrors').html('').hide();
		if(type==3){ // chi dung cho duyet don vansale
			//Duyệt đơn hàng
			var status=$('#saleOrderApprovedVan').val().trim();
			var orderId=$('#orderId').val().trim();
			if( status =='1'){
				$('#GeneralInforErrors').html('Đơn hàng không hợp lệ hoặc đã được duyệt').show();
				var tm = setTimeout(function(){
					$('#GeneralInforErrors').html('').hide();
					clearTimeout(tm);					
				}, 5000);
				return false;
			}
			if(orderId == null || orderId == undefined || orderId == ''){
				$('#GeneralInforErrors').html('Bạn chưa chọn đơn hàng').show();
				var tm = setTimeout(function(){
					$('#GeneralInforErrors').html('').hide();
					clearTimeout(tm);					
				}, 5000);
				return false;
			}
			var params=new Object();
			params.orderId=orderId;
			Utils.saveData(params, '/sale-product/adjust-order/approved-order-van', null, 'errMsg', function(data) {				
					if(!data.error){
						$.cookie('approvedOrder', 1);
						isAuthorize = false;
						if(!isAuthorize){
							$('#approveButton').hide();
						}
					}else{
						$('#serverErrors').html(result.errMsg).show();
					}
					setTimeout(function(){window.location.assign(window.location.href);},0);
					
				}, 'loading2', null, null,function(data){
					$('#serverErrors').html(data.errMsg).show();
			});	
		}else{
			if (SPAdjustmentOrder.checkApprovedStatus()){
				var disabled = $('#payment').attr('disabled');
				if(disabled!='disabled'){
					$('#serverErrors').html('Bạn chưa tính tiền đơn hàng. Vui lòng tính tiền').show();
					return false;
				}
				return SPAdjustmentOrder.editOrder();
			}
		}
	},
	addProduct:function(){
		if(SPAdjustmentOrder.checkApprovedStatus()){			
			return SPCreateOrder.openSelectProductDialog();
		}
	},
	loadPaymentHide:function(){
		var len = SPAdjustmentOrder.zv21ListPromotion.length;
		var zLenPPAuto = SPCreateOrder._lstPromotionProductIsAuto.size();
		var zzrowNumForOrder = zLenPPAuto + 1;
		if(len>0){
			/** title ZV21 */			
			var pproduct = new Object();
			pproduct.changeProduct  = 1;
			pproduct.checkLot = 'Khuyến mãi đơn hàng';
			pproduct.convfact = '';
			pproduct.discountAmount = pproduct.checkLot;
			pproduct.discountPercent = 0;
			pproduct.key = -1;
			pproduct.maxDiscountAmount = 0;
			pproduct.maxQuantity = 0;
			pproduct.maxQuantityFreeTotal = 0;
			pproduct.productCode = pproduct.checkLot;
			pproduct.productName = pproduct.checkLot;
			pproduct.programCode = SPAdjustmentOrder.zv21ListPromotion[0].programCode;
			pproduct.promotionType = PromotionType.PROMOTION_FOR_ZV21;
			pproduct.quantity = pproduct.checkLot;
			pproduct.stock = pproduct.checkLot;
			pproduct.type = 1;
			pproduct.typeName = 'KM hàng';
			pproduct.rownum = zzrowNumForOrder;
			SPCreateOrder._lstPromotionProductIsAuto.put(zLenPPAuto,pproduct);
			SPAdjustmentOrder.keyMapPromotionForOrder = zLenPPAuto;
			zLenPPAuto = SPCreateOrder._lstPromotionProductIsAuto.size();
			pproduct.rownum = zzrowNumForOrder;
			for(var i=0;i<len;++i){		
				pproduct = new Object();
				pproduct = SPAdjustmentOrder.zv21ListPromotion[i];
				pproduct.rownum = zzrowNumForOrder;
				SPCreateOrder._lstPromotionProductIsAuto.put((zLenPPAuto+i),pproduct);				
			}			
		}	
		SPAdjustmentOrder.initSaleProductGrid();
		SPAdjustmentOrder.initPromotionProductGrid();
		//bo di de don hang tu tablet co the doi duoc ctkm
		/*if(SPCreateOrder.checkTablet())
			return true;*/
		if(isAuthorize){
			SPAdjustmentOrder.paymentHide();
		}
		
	},
	paymentHide:function(){
		$('#loadingPPromotion').show();
		var shopCode = $('#shopCode').val().trim();
		var customerCode=$('#customerCode').val().trim();
		if(customerCode.length==0){
		}else{
			if(SPCreateOrder._lstProduct.size()==0){
				return false;
			}
			var amount_total = 0;
			var total = 0;
			var lstCommercialSupport = new Array(),lstCommercialSupportType = new Array(),lstProduct = new Array(),lstQuantity = new Array();			
			for(var i=0;i<SPCreateOrder._lstProduct.size();++i){
				if(SPCreateOrder._lstProduct.keyArray[i] != '-') { 
					var obj = SPCreateOrder._lstProduct.get(SPCreateOrder._lstProduct.keyArray[i]);
					lstProduct.push(obj.productCode);	
					if(obj.commercialSupportType.indexOf('ZV') == -1 && obj.commercialSupportType != '') {
						lstCommercialSupport.push(obj.commercialSupport);
						lstCommercialSupportType.push(obj.commercialSupportType);
					} else if(obj.commercialSupport.length > 0 && obj.commercialSupportType.length == 0) {
						lstCommercialSupport.push(obj.commercialSupport);
						lstCommercialSupportType.push('');
					} else {
						lstCommercialSupport.push('');
						lstCommercialSupportType.push('');
					}
					lstQuantity.push(getQuantity(obj.convfact_value,obj.convfact));
				}
			}		
			var data = new Object();			
			data.lstProduct = lstProduct;
			data.lstCommercialSupport = lstCommercialSupport;
			data.lstCommercialSupportType = lstCommercialSupportType;
			data.lstQuantity = lstQuantity;
			data.customerCode = customerCode;
			data.shopCode = shopCode;
			if($('#orderId').val()!=''){
				data.orderId=$('#orderId').val();
			}			
			$.ajax({
				type : "POST",
				url : '/sale-product/create-order/payment',
				data :($.param(data, true)),
				dataType : "json",
				success : function(result) {					
					if(result.error && result.errMsg!= undefined){
						$('#loadingPPromotion').hide();
					} else {					
							SPCreateOrder._lstDiscount = new Map();
							SPCreateOrder._lstDisper = new Map();
							$('#footer_saleProductDetails #total').html(formatCurrency(result._total));
							var pp = result.pp;	
							var lstPromotionProductIsAutoTemp = new Map();
							SPCreateOrder._lstPromotionProductForOrder = new Map();
							SPCreateOrder.programes = result.programes;											
							for(var i=0;i<pp.length;++i){						
								var pproduct = new Object();
								pproduct = SPCreateOrder.getObjectPromotionProduct(pp[i]);								
								lstPromotionProductIsAutoTemp.put(i,pproduct);
							}
							var zV21 = result.zV21;							
							var zLenPPAuto = lstPromotionProductIsAutoTemp.size();						
							if(zV21!=undefined && zV21!=null){							
								for(var i=0;i<zV21.listPromotionForPromo21.length;++i){
									var pproduct = new Object();
									pproduct = SPCreateOrder.getObjectPromotionProduct(zV21.listPromotionForPromo21[i]);									
									lstPromotionProductIsAutoTemp.put((zLenPPAuto+i),pproduct);
								}	
							}						
							if(SPAdjustmentOrder.keyMapPromotionForOrder!=null){
								var indexZVForOrder = SPAdjustmentOrder.keyMapPromotionForOrder;
								var pproduct = SPCreateOrder._lstPromotionProductIsAuto.get(indexZVForOrder);
								if(indexZVForOrder!=null ){
									if(result.programes != null && result.programes != undefined && result.programes.length>0)
										pproduct.changeProduct = 1;									
									else
										pproduct.changeProduct = 0;
									if(pproduct.promotionType==PromotionType.PROMOTION_FOR_ORDER){
										pproduct.key = -2;
									}
									SPCreateOrder._lstPromotionProductIsAuto.put(indexZVForOrder,pproduct);
								}
							}
							SPCreateOrder._lstPromotionProductForOrder.put('zV21',result.zV21C);
							SPCreateOrder._lstPromotionProductForOrder.put('zV19z20',result.zV19zV20C);
							
							for(i=0;i<SPCreateOrder._lstPromotionProductIsAuto.keyArray.length;i++){
								if(SPCreateOrder._lstPromotionProductIsAuto.keyArray[i]!=undefined){
									var keyPromotion=SPCreateOrder._lstPromotionProductIsAuto.keyArray[i];
									var promotion=SPCreateOrder._lstPromotionProductIsAuto.valArray[i];									
									if(!isNullOrEmpty(promotion.productCode)){
										var keyTemp=null;
										for(j=0;j<lstPromotionProductIsAutoTemp.keyArray.length;j++){
											if(lstPromotionProductIsAutoTemp.keyArray[j]!=undefined){												
												var promotionTemp = lstPromotionProductIsAutoTemp.get(lstPromotionProductIsAutoTemp.keyArray[j]);
												if(promotion.programCode==promotionTemp.programCode){
													if(promotion.key == '' || promotion.key == "") {
														promotion.key = promotionTemp.key;
														promotion.changeProduct = promotionTemp.changeProduct;
													} else if(!isNaN(promotion.key) || Number(promotion.key) < 0) {
													} else {
														promotion.key = promotionTemp.key;
														promotion.changeProduct = promotionTemp.changeProduct;
													}
													SPCreateOrder._lstPromotionProductIsAuto.valArray[i] = promotion;
													keyTemp=lstPromotionProductIsAutoTemp.keyArray[j];
												}
											}
										}
									}									
								}
							}
							SPAdjustmentOrder.initPromotionProductGrid();							
							$('#loadingPPromotion').hide();
					}
					xhrSave = null;				
				},
				error:function(XMLHttpRequest, textStatus, errorThrown) {
				}
			});
			
		}
		
	},
	
	/** 
	 * Luu don hang
	 *  */ 
	
	editOrder:function(){
		$('#serverErrors').html('').hide();
		$('#GeneralInforErrors').hide();
		var msg ='';
		msg = Utils.getMessageOfRequireCheck('shopCode', 'Mã đơn vị');
		if(msg.length==0){
			msg = Utils.getMessageOfRequireCheck('customerCode', 'Mã khách hàng');
		}
		
		if(msg.length==0){
			msg = Utils.getMessageOfRequireCheck('staffSaleCode', 'Nhân viên bán hàng', true);					
		}
		if(msg.length==0 && $('#priorityId').val()!=null){
			msg = Utils.getMessageOfRequireCheck('priorityId', 'Độ ưu tiên');
		}
		if(msg.length == 0 && $('#deliveryDate').val()!=null && $('#deliveryDate').val().length > 0){
			msg = Utils.getMessageOfInvalidFormatDate('deliveryDate', 'Ngày giao');
		}
		var deliveryDate = $('#deliveryDate').val().trim();
		var curDate = $('#currentLockDay').val().trim();
		if(msg.length ==0 && deliveryDate.length >0){
			if (!Utils.compareDate(curDate,deliveryDate)) {
				$('#GeneralInforErrors').html('Ngày giao không được nhỏ hơn ngày chốt').show();
				window.scrollTo(0,0);
				return false;
			}
		}
		if(msg.length>0){
			$('#GeneralInforErrors').html(msg).show();
			window.scrollTo(0,0);
			return false;
		}	
		if(msg.length>0){
			$('#GeneralInforErrors').html(msg).show();
			window.scrollTo(0,0);
			return false;
		}		
		var data = new Object();
		data.shopCode = $('#shopCode').val().trim();
		data.customerCode = $('#customerCode').val().trim();
		data.carId = $('#carId').combobox('getValue').trim();
		data.deliveryCode = $('#transferStaffCode').combobox('getValue').trim();
		data.cashierCode = $('#cashierStaffCode').combobox('getValue').trim();
		data.salerCode = $('#staffSaleCode').combobox('getValue').trim();
		data.orderId = $('#orderId').val().trim();
		if($('#priorityId').val()!=null)
			data.priorityId = $('#priorityId').val().trim();
		data.deliveryDate = $('#deliveryDate').val().trim();
		var lstCommercialSupport = new Array(),lstCommercialSupportType = new Array(),lstProduct = new Array(),lstQuantity = new Array(),lstProductDestroy = new Array(), lstProductSaleLot = new Array(), lstPromotionProductSaleLot = new Array(),			
		lstBuyProductCommercialSupportProgram = new Array(), lstBuyProductCommercialSupportProgramType = new Array();
		for(var i=0;i<SPCreateOrder._lstProduct.size();++i){
			var obj = SPCreateOrder._lstProduct.get(SPCreateOrder._lstProduct.keyArray[i]);
			if(!isNullOrEmpty(obj.productId)) {
				lstProduct.push(obj.productCode);
				lstBuyProductCommercialSupportProgram.push(obj.commercialSupport);
				if (obj.commercialSupportType != undefined 
						&& (obj.commercialSupportType == 'ZV' || obj.commercialSupportType.indexOf('ZV') != -1)) {
					lstBuyProductCommercialSupportProgramType.push('ZV');
				} else {
					var tmp = SPCreateOrder._lstCommercialSupportMap.get(obj.commercialSupport);
					if (tmp != undefined && tmp != null) {
						lstBuyProductCommercialSupportProgramType.push(tmp.type);
					} else {
						lstBuyProductCommercialSupportProgramType.push('');
					}
				}
				if(obj.commercialSupportType.indexOf('ZV') == -1 && obj.commercialSupportType != '') {
					lstCommercialSupport.push(obj.commercialSupport);
					lstCommercialSupportType.push(obj.commercialSupportType);
				} else if(obj.commercialSupport != '' && obj.commercialSupportType == '') {
					lstCommercialSupport.push(obj.commercialSupport);
					lstCommercialSupportType.push('');
				} else {
					lstCommercialSupport.push('');
					lstCommercialSupportType.push('');
				}
				lstQuantity.push(getQuantity(obj.convfact_value,obj.convfact));
				if((obj.checkLot==1 || obj.checkLot=='1') &&(obj.commercialSupportType=='ZH')){
					var lotDetail = SPCreateOrder._lstProductLotVO.get(obj.productCode);
					if(lotDetail==null || lotDetail == undefined){
						$('#serverErrors').html('Tổng số lượng các lô hủy không bằng số lượng hủy đã nhập. Vui lòng nhập lại').show();
						return false;
					}
					var sumQuantityLot = 0;
					for(var j=0;j<lotDetail.productLots.size();++j){
						var detail = lotDetail.productLots.get(lotDetail.productLots.keyArray[j]);
						if(parseInt(detail.quatity, 10)>0){	
							sumQuantityLot = sumQuantityLot + parseInt(detail.quatity, 10);
						}
					}
					if(parseInt(sumQuantityLot, 10)!=parseInt(lstQuantity[i], 10)){
						$('#serverErrors').html('Tổng số lượng các lô hủy không bằng số lượng hủy đã nhập. Vui lòng nhập lại').show();
						return false;
					}
					lstProductDestroy.push(lotDetail.strMap);
					lstProductSaleLot.push(obj.productCode + ';');
				} else if((obj.checkLot==1 || obj.checkLot=='1')) {
					var lotDetail = SPCreateOrder._lstProductSaleLotVO.get(obj.productCode);
					
					if(lotDetail == null || lotDetail == undefined) {
						/*if(valueTemp!=undefined && valueTemp!=0){
							$('#serverErrors').html('Bạn chưa nhập lô cho sản phẩm bán. Vui lòng nhập lô').show();
							return false;
						}*/
						$('#serverErrors').html('Bạn chưa nhập lô cho sản phẩm bán. Vui lòng nhập lô').show();
						return false;
					}
					var sumQuantityLot = 0;
					for(var j = 0; j < lotDetail.productLots.size();j++) {
						var detail = lotDetail.productLots.get(lotDetail.productLots.keyArray[j]);
						if(parseInt(detail.quatity, 10) > 0) {
							sumQuantityLot = sumQuantityLot + parseInt(detail.quatity, 10);
						}
					}
					if(parseInt(sumQuantityLot, 10) != parseInt(lstQuantity[i], 10)) {
						$('#serverErrors').html('Tổng số lượng các lô bán không bằng số lượng bán đã nhập. Vui lòng nhập lại').show();
						return false;
					}
					lstProductSaleLot.push(lotDetail.strMap);
					lstProductDestroy.push(obj.productCode + ';');
				}else{
					lstProductDestroy.push(obj.productCode + ';');
					lstProductSaleLot.push(obj.productCode + ';');
				}
			}
		}
		if(lstCommercialSupport.length>0){
			data.lstCommercialSupport = lstCommercialSupport;
		}
		if(lstCommercialSupportType.length>0){
			data.lstCommercialSupportType = lstCommercialSupportType;
		}
		if(lstProduct.length==0){
			$('#serverErrors').html('Không tồn tại sản phẩm nào trong đơn hàng').show();
			return false;
		}
		if(lstQuantity.length==0){
			$('#serverErrors').html('Vui lòng nhập đúng số lượng sản phẩm').show();
			return false;
		}
		data.lstProduct=lstProduct;
		data.lstQuantity=lstQuantity;
		data.lstProductDestroy = lstProductDestroy;
		data.lstProductSaleLot = lstProductSaleLot;
		// 10-03-2014 - nhanlt6: fix bug 0000155 luu cac CTHTTM cua san pham mua
		data.lstBuyProductCommercialSupportProgram = lstBuyProductCommercialSupportProgram;
		data.lstBuyProductCommercialSupportProgramType = lstBuyProductCommercialSupportProgramType;
		var lstPromotionCode = new Array();
		//var lstPromotionProductSale = new Array();
		var lstPromotionProduct = new Array();
		var lstDiscount = new Array();
		var lstDisper = new Array();
		var lstPromotionType = new Array();
		var lstPromotionProductQuatity = new Array();
		var lstPromotionProductMaxQuatity = new Array();
		var lstPromotionProductMaxQuatityCk = new Array();
		var lstMyTime = new Array();
		for(var i=0;i<SPCreateOrder._lstPromotionProductIsAuto.size();++i){
			var temp = SPCreateOrder._lstPromotionProductIsAuto.valArray[i];
			if(temp.promotionType==PromotionType.PROMOTION_FOR_ZV21){
				continue;
			}							
			lstPromotionCode.push(temp.programCode);					
			lstPromotionType.push(temp.type);
			
			/**Lay so luong, %, amout chinh sua*/
			//var lstEditData = $('#gridPromotionProduct').datagrid('getData');
			
			lstDiscount.push(temp.discountAmount);
			lstDisper.push(temp.discountPercent);
			lstPromotionProductQuatity.push(temp.quantity);			
			if(temp.type == PromotionType.FREE_PRICE || temp.type == PromotionType.FREE_PERCENT){
				var zvalue = $('#discountAmount_' +i).val();
				if(zvalue.length==0){
					msg = 'Bạn chưa nhập số tiền khuyến mãi. Vui lòng nhập giá trị.';
					$('#discountAmount_' +i).focus();
				}else{
					var maxDiscountAmount = Number(temp.maxDiscountAmount);
					var discountAmount = Number(temp.discountAmount);
					if(maxDiscountAmount<discountAmount){
						msg = 'Số tiền khuyến mãi vượt quá Số tiền khuyến mãi tối đa được hưởng. Vui lòng nhập lại.';
						$('#discountAmount_' +i).focus();
					}
				}	
				lstPromotionProduct.push('');
				lstPromotionProductMaxQuatity.push(temp.maxDiscountAmount);
				lstPromotionProductMaxQuatityCk.push(temp.maxDiscountAmount);
				lstMyTime.push((temp.myTime == null || temp.myTime == undefined) ? 0 : temp.myTime);
			}else if(temp.type == PromotionType.FREE_PRODUCT) {
				var zcvalue = $('#convact_promotion_value_' +i).val();
				var zvalue = getQuantity(zcvalue,temp.convfact);
				if(zcvalue.length==0){
					msg = 'Bạn chưa nhập số lượng sản phẩm khuyến mãi. Vui lòng nhập giá trị.';
					$('#convact_promotion_value_' +i).focus();
				}else if(isNaN(zvalue)){
					msg = 'Số lượng sản phẩm khuyến mãi không hợp lệ. Vui lòng nhập giá trị.';
					$('#convact_promotion_value_' +i).focus();
				}else{
					var maxQuantity = Number(temp.maxQuantity);				
					var qty = Number(temp.quantity);
					if(temp.isEdited == 0 && $('#tablet').val() != 2) {
						if(qty != maxQuantity && qty != 0) {
							msg = 'Số lượng khuyến sản phẩm khuyến mãi ' + Utils.XSSEncode(temp.productCode) + ' chỉ được phép bằng ' + maxQuantity + ' hoặc bằng 0. Vui lòng kiểm tra lại.';
							$('#convact_promotion_value_' +i).focus();
						}
					} else if(maxQuantity<qty){
						msg = 'Số lượng khuyến mãi vượt quá số lượng khuyến mãi tối đa được hưởng. Vui lòng nhập lại.';
						$('#convact_promotion_value_' +i).focus();
					}
				}	
				lstPromotionProduct.push(temp.productCode);
				lstPromotionProductMaxQuatity.push(temp.maxQuantity);
				lstPromotionProductMaxQuatityCk.push(temp.maxQuantityCk == undefined ? temp.maxQuantity : temp.maxQuantityCk);
				lstMyTime.push((temp.myTime == null || temp.myTime == undefined) ? 0 : temp.myTime);
			}
			if(msg.length>0){				
				$('#serverErrors').html(msg).show();
				$('#gridPromotionProduct').datagrid('selectRow',i);
				return false;
			}
			//Truong hop co lo
		}		
		data.lstPromotionCode = lstPromotionCode;		
		data.lstPromotionProduct = lstPromotionProduct;
		data.lstPromotionType = lstPromotionType;
		data.lstDiscount = lstDiscount;
		data.lstDisper = lstDisper;
		data.lstPromotionProductQuatity = lstPromotionProductQuatity;
		data.lstPromotionProductSaleLot = lstPromotionProductSaleLot;
		data.lstPromotionProductMaxQuatity = lstPromotionProductMaxQuatity;
		data.lstPromotionProductMaxQuatityCk = lstPromotionProductMaxQuatityCk;
		data.lstMyTime = lstMyTime;
		if(SPAdjustmentOrder._typeSave==1){//duyet
			Utils.addOrSaveData(data, '/sale-product/adjust-order/edit-order-approved', SPCreateOrder._xhrSave, 'serverErrors', function(result){
				if(!result.error){
					if(!result.error){
						if(SPAdjustmentOrder._typeSave==1){//duyet
							//Kiem tra gia truoc khi duyet
							$.ajax({
								type : "POST",
								url : "/sale-product/search-sale/check-price-one-saleorder",
								data :(($.param(data, true))),
								dataType: "json",
								success : function(data) {
									$('#divOverlay').hide();
									if(!data.error) {
										//Tien hanh duyet tiep don hang
										SPAdjustmentOrder.approvedOrder();
									}else{
										//Hien thi thong bao don hang sai gia
										$('#productError').html(Utils.XSSEncode(data.rows.productCodeString));
										
										$('#warningPopup').dialog({
											title : 'Xác nhận',
											width:400,
											height:'auto',
											onOpen: function(){
												$("#errMsgSelectShop").hide();
												$("#btnCancel").bind('click',function(event) {
													$('#warningPopup').dialog('close');
												});
												
												$("#btnOk").unbind('click');
												$("#btnOk").bind('click',function(event) {
													$('#warningPopup').dialog('close');
													SPAdjustmentOrder.approvedOrder();
												});
											}
										});
										
										$('#warningPopup').dialog('open');
									}
								},
								error:function(){
									$('#divOverlay').hide();
								}
							});
							
						}else{
							$.cookie('saveOrder', 1);
							//setTimeout(function(){window.location.reload();},0);
							setTimeout(function(){window.location.assign(window.location.href);},0);
						}
					}else{
						if(result.flag){
							$('#serverErrors').html(result.errMsg).show();
							disabled('btnOrderSave');
							disabled('approveButton');
						}else{
							$('#serverErrors').html(result.errMsg).show();
						}				
					}
				}else{
					$('#serverErrors').html(result.errMsg).show();
				}
			}, 'loading2', '', false, '',null,true);
		}else{
			Utils.addOrSaveData(data, '/sale-product/adjust-order/edit-order', SPCreateOrder._xhrSave, 'serverErrors', function(result){
				if(!result.error){
					if(!result.error){
						$.cookie('saveOrder', 1);
						//setTimeout(function(){window.location.reload();},0);
						setTimeout(function(){window.location.assign(window.location.href);},0);
					}else{
						if(result.flag){
							$('#serverErrors').html(result.errMsg).show();
							disabled('btnOrderSave');
							disabled('approveButton');
						}else{
							$('#serverErrors').html(result.errMsg).show();
						}				
					}
				}else{
					$('#serverErrors').html(result.errMsg).show();
				}
			}, 'loading2', '', false, '','');
		}
	},
	deleteProductLot:function(id){
		$('#rowLot_' +id).remove();
	},
	editProductLot:function(id){
		var lot=$('#lot_' +id).html();
		var qty=$('#qty_' +id).html();
		$('#d_lot').val(lot);
		$('#d_qtyCount').val(qty);
		$('#idLotEdit').val(id);
	},
	saveProductLot:function(){
		var idLot=$('#idLotEdit').val();
		$('#lot_' +id).html($('#d_lot').val());
		$('#qty_' +id).html($('#d_qtyCount').val());
		$('#d_lot').val('');
		$('#d_qtyCount').val('');
	},
	productLot:function(code){
		var arrParam = new Array();
		var param = {};
		param.value = code;
		CommonSearch.viewDetailLot(function(data){
			if (data != null && data.name != null && data.name != undefined && !data.error) {
				var stt=0;
				var html='';
				for( i=0;i<data.list.length;i++){
					stt=stt+1;
					html+="<tr id='rowLot_"+data.list[i].id+"' > ";
					html+="<td class='ColsTd1' style='width: 50px;'>"+stt+"</td>";
					html+="<td class='ColsTd2' style='width: 150px;' id='lot_" + data.list[i].id + "'>" + data.list[i].lot + "</td>";
					html+="<td class='ColsTd3' style='width: 100px;'><span class='qtyBeforeCount' id='qty_" + data.list[i].id + "'>" + data.list[i].availableQuantity + "</span></td>";
					html+="<td class='ColsTd4' style='width: 50px;' ><a onclick='return SPAdjustmentOrder.editProductLot(" + data.list[i].id + ");'>";
					html+="		<img src='../../../resources/images/icon-edit.png' width='15' height='16' /></a></td>";
					html+="<td class='ColsThEnd' style='width: 50px;'><a onclick='return SPAdjustmentOrder.deleteProductLot(" + data.list[i].id + ");'>";
					html+="		<img src='../../../resources/images/icon-delete.png' width='15' height='16' /></a></td>";
					html+="</tr>";
				}	
				$('#stockCClist').html(html);
				$('#totalBeforeCount').html(data.count);
			}
		},param);
	},
	viewDetailSaleLot : function(callback, productCode) {
		CommonSearch._currentSearchCallback = callback;
		$.getJSON('/sale-product/create-order/list-product-lot?productCode=' + productCode, function(data) {
			if (callback != null && callback != undefined) {
				callback.call(this, data);
			}
		});
	},
	deactiveAllMainTab: function(){
		$('#tab1 a').removeClass('Active');
		$('#tab2 a').removeClass('Active');
		$('#tabContent1').hide();
		$('#tabContent2').hide();
	},
	gotoTab: function(tabIndex){
		SPAdjustmentOrder.deactiveAllMainTab();
		switch (tabIndex) {
		case 0:
			$('#tabContent1').show();
			$('#tab1 a').addClass('Active');
			break;
		case 1:
			$('#tabContent2').show();
			$('#tab2 a').addClass('Active');
			AttributesManager.getAttributesForm('tabContent2',AttributesManager.DP_PAY_PERIOD_RESULT);
			$('#typeAttribute').val(AttributesManager.DP_PAY_PERIOD_RESULT);
			break;
		default:
			$('#tabContent1').show();
			break;
		}
	},
	disableAllTextField:function(){
		if($('#disabledButton').val()=='1'){
			$('.InputTextStyle').each(function(){
				$(this).attr('disabled','disabled');
			});
			$('.lotApproved').each(function(){
				$(this).removeAttr('onclick');
				$(this).css('color','#989898');
			});
			$('.btnDelete').each(function(){
				$(this).hide();
			});
		}
	},
	initCopyOrder:function() {
		if (SPAdjustmentOrder._mapCusutomerForCopyOrder != undefined
				&& SPAdjustmentOrder._mapCusutomerForCopyOrder.size() > 0) {
			var data = new Object();
			data.listCustomerForOrderCopy = SPAdjustmentOrder._mapCusutomerForCopyOrder.keyArray;
			data.saleOrderId = Number(SPAdjustmentOrder.orderId);
			Utils.addOrSaveData(data, '/sale-product/adjust-order/copy', SPAdjustmentOrder._xhrSave, 'searchCustomerForCopyDialog #errMsgSearch', function(result){
				if(!result.error){
					SPAdjustmentOrder._mapCusutomerForCopyOrder = new Map();
					var orderNumberStr = "";
					if (result.lstSOCopied != undefined && result.lstSOCopied !=null) {
						for (var i=0;i<result.lstSOCopied.length;i++) {
							if (i==0) {
								orderNumberStr = Utils.XSSEncode(result.lstSOCopied[i].orderNumber);
							} else {
								orderNumberStr += ","+Utils.XSSEncode(result.lstSOCopied[i].orderNumber);
							}
						}
						$('#successMsg').html('Sao chép thành công với các mã đơn hàng mới: ' + Utils.XSSEncode(orderNumberStr)).show();
						$('.easyui-dialog').dialog('close');
						setTimeout(function() {$('#successMsg').html('').hide();},5000);
					}
				}else{
					$('.easyui-dialog#searchCustomerForCopyDialog #errMsgSearch').html(result.errMsg).show();
				}
			}, 'loading2', null, false, null,null,true);
		} else {
			$('.easyui-dialog #errMsgSearch').html('Vui lòng chọn ít nhất một khách hàng để sao chép đon hàng').show();
			return false;
		}
	},
	/** Tinh nang sao chep don hang */
	copyOrder:function() {
		$('#serverErrors').html('').show;
		SPAdjustmentOrder._mapCusutomerForCopyOrder = new Map();
		$('.easyui-dialog #btnSearchStyle1').unbind('click');
		$('#searchCustomerForCopyDiv').show();
		var html = $('#searchCustomerForCopyDiv').html();
		$('#searchCustomerForCopyDialog').dialog({  
	        title: 'Thông tin tìm kiếm',  
	        closed: false,  
	        cache: false,  
	        modal: true,
	        width:650,
	        onOpen: function(){		        	
	        	var tabindex = -1;
	        	$('.InputTextStyle, select, input, button , li , tr, td, th, label, ul, a, img').each(function () {
					if (this.type != 'hidden') {
						$(this).attr("tabindex", tabindex);
						tabindex -=1;
					}
				});	
	        	$('.easyui-dialog#searchCustomerForCopyDialog #seachStyle1Code').attr('disabled',false);
				$('.easyui-dialog#searchCustomerForCopyDialog #seachStyle1Name').attr('disabled',false);
				$('.easyui-dialog#searchCustomerForCopyDialog #seachStyle1Address').attr('disabled',false);
	        	$('.easyui-dialog#searchCustomerForCopyDialog #seachStyle1Code').focus();
				$('.easyui-dialog#searchCustomerForCopyDialog #seachStyle1AddressLabel').show();
				$('.easyui-dialog#searchCustomerForCopyDialog #seachStyle1Address').show();
				$('.easyui-dialog#searchCustomerForCopyDialog #searchStyle1Grid').show();
				$('.easyui-dialog#searchCustomerForCopyDialog #btnSearchStyle1').css('margin-left', 270);
				$('.easyui-dialog#searchCustomerForCopyDialog #btnSearchStyle1').css('margin-top', 5);
				$('.easyui-dialog#searchCustomerForCopyDialog #seachStyle1CodeLabel').html('Mã KH');
				$('.easyui-dialog#searchCustomerForCopyDialog #seachStyle1NameLabel').html('Tên KH');
				Utils.bindAutoSearch();
//				CommonSearch._currentSearchCallback = callback;
				$('.easyui-dialog #searchStyle1Grid').datagrid({
					url : '/commons/customer-in-shop/search',
					autoRowHeight : true,
					rownumbers : true, 
					checkOnSelect :true,
					pagination:true,
					rowNum : 10,
					pageSize:10,
					scrollbarSize : 0,
					fitColumns:true,
					queryParams:{
						page:1
					},
					width : 600,
				    columns:[[  
				        {field:'shortCode',title:'Mã KH',align:'left', width:100, sortable : false,resizable : false},  
				        {field:'customerName',title:'Tên KH',align:'left', width:200, sortable : false,resizable : false},				        
				        {field:'address',title:'Địa chỉ',align:'left', width:300, sortable : false,resizable : false},
				        {field :'id',checkbox:true,title:'Chọn'},
				    ]],
				    onCheck :function(rowIndex,rowData) {
				    	if (SPAdjustmentOrder._mapCusutomerForCopyOrder != null 
				    			&& SPAdjustmentOrder._mapCusutomerForCopyOrder.get(rowData.id) == null) {
				    		SPAdjustmentOrder._mapCusutomerForCopyOrder.put(rowData.id,rowData);
				    	}
				    },
				    onUncheck :function(rowIndex,rowData) {
				    	if (SPAdjustmentOrder._mapCusutomerForCopyOrder.size() > 0 
				    		&& SPAdjustmentOrder._mapCusutomerForCopyOrder.get(rowData.id) != null) {
				    		SPAdjustmentOrder._mapCusutomerForCopyOrder.remove(rowData.id);
				    	}
				    },
				    onCheckAll :function(rows) {
				    	for (var i=0;i<rows.length;i++) {
				    		if (SPAdjustmentOrder._mapCusutomerForCopyOrder != null 
						    		&& SPAdjustmentOrder._mapCusutomerForCopyOrder.get(rows[i].id) == null) {
						    		SPAdjustmentOrder._mapCusutomerForCopyOrder.put(rows[i].id,rows[i]);
						    	}
				    	}
				    },
				    onUncheckAll :function(rows) {
						for (var i=0;i<rows.length;i++) {
							if (SPAdjustmentOrder._mapCusutomerForCopyOrder.size() > 0 
	   					    		&& SPAdjustmentOrder._mapCusutomerForCopyOrder.get(rows[i].id) != null) {
						    		SPAdjustmentOrder._mapCusutomerForCopyOrder.remove(rows[i].id);
						    	}			    		
						}
				    },
				    onLoadSuccess :function(data){
				    	 tabindex = 1;
			    		 $('.easyui-dialog input,.easyui-dialog select,.easyui-dialog button').each(function () {
				    		 if (this.type != 'hidden') {
					    	     $(this).attr("tabindex", '');
								 tabindex++;
				    		 }
						 });
			    		 $('.datagrid-header-rownumber').html('STT');	
			    		 updateRownumWidthForJqGrid('.easyui-dialog');
			    		 $(window).resize();
			    		 var length = data.rows.length;
			    		 for (var i=0;i<length;i++) {
			    			 var object = SPAdjustmentOrder._mapCusutomerForCopyOrder.get(data.rows[i].id);
			    			 if (object != null) {
			    				 $('.easyui-dialog #searchStyle1Grid').datagrid('checkRow',i);
			    			 }
			    		 }
				    }
				});
				$('.easyui-dialog#searchCustomerForCopyDialog #btnSearchStyle1').bind('click',function(event) {
					if(!$(this).is(':hidden')){
						var code = $('.easyui-dialog#searchCustomerForCopyDialog #seachStyle1Code').val().trim();
						var name = $('.easyui-dialog#searchCustomerForCopyDialog #seachStyle1Name').val().trim();
						var address = $('.easyui-dialog#searchCustomerForCopyDialog #seachStyle1Address').val().trim();
						$('.easyui-dialog#searchCustomerForCopyDialog #searchStyle1Grid').datagrid('load',{code :code, name: name,address:address});
						var records = $('.easyui-dialog#searchCustomerForCopyDialog #searchStyle1Grid').datagrid('getRows').length;
						$('.easyui-dialog#searchCustomerForCopyDialog #seachStyle1Code').focus();
						if (records != null && records == 1 && !isNullOrEmpty(code)) {
							$('#customerCode').val($('.easyui-dialog#searchCustomerForCopyDialog #searchStyle1Grid').datagrid('getRows')[0].shortCode);
							$('#customerName').val($('.easyui-dialog#searchCustomerForCopyDialog #searchStyle1Grid').datagrid('getRows')[0].customerName);
							$('#searchCustomerForCopyDiv').css("visibility", "hidden");					
							$('.easyui-dialog').dialog('close');
							$('#customerCode').change();
						}
					}					
				});
				$('.easyui-dialog #btnClose').bind('click',function(event) {
					$('#searchCustomerForCopyDiv').css("visibility", "hidden");					
					$('.easyui-dialog').dialog('close');
				});
	        },
	        onBeforeClose: function() {
	        	var curIdFocus = $('#cur_focus').val();
				$('#' + curIdFocus).focus();				
				$('.easyui-dialog #searchStyle1Grid').datagrid('reload',{	
					pageSize: 10
				});
	        },
	        onClose : function(){
	        	$('#searchCustomerForCopyDiv').html(html);
	        	$('#searchCustomerForCopyDiv').hide();
	        	$('#searchStyle1ContainerGrid').html('<table id="searchStyle1Grid" class="easyui-datagrid" style="width: 520px;"></table><div id="searchStyle1Pager"></div>');
	        	$('.easyui-dialog #seachStyle1Code').val('');
	        	$('.easyui-dialog #seachStyle1Name').val('');
	        	$('.easyui-dialog #seachStyle1Address').val('');	 
	        	$('.easyui-dialog #errMsgSearch').html('').hide();
	        	SPAdjustmentOrder._mapCusutomerForCopyOrder = new Map();
	        }
		});
	}
};

var PromotionProductFmt = {
		productFmt:function(value,row,index) {		
			if (row.isFooter) {
				return '<b class="clazzfooter" style="text-align:center">Tổng</b>';
			}
			var st = 'ppStyle' + index;
			if (row.openPromo == 1) {
				st = 'op_ppStyle' + index;
			} else if (row.promotionType == 5 || row.promotionType == 4) {
				st = 'od_ppStyle' + index;
			}
			return '<span class="' +st+'" id="productName_' +row.productId+'_' +index+'">' +Utils.XSSEncode(row.productName)+'</span>';
		},
		stockTotalFmt:function(value,row,index) {
			if (row.warehouse != null) {
				if (SPCreateOrder._isViewOrder) {
					return Utils.XSSEncode(row.warehouse.warehouseName);
				} else {
					return '<a class="delIcons" onclick="return SPCreateOrder.openSelectProductWarehouseDialog(\'' + Utils.XSSEncode(row.productCode) + '\',\'' + index + '\', SPCreateOrder.productWarehouseType.PROMOTION_PRODUCT);" href="javascript:void(0)">' + Utils.XSSEncode(row.warehouse.warehouseName) + '</a>';
				}
			}
		},
		warehouseFmt:function(value,row,index) {
			if (row.warehouse != null) {
				if (SPCreateOrder._isViewOrder) {
					return Utils.XSSEncode(row.warehouse.warehouseName);
				} else {
					return '<a class="delIcons" onclick="return SPCreateOrder.openSelectProductWarehouseDialog(\'' + Utils.XSSEncode(row.productCode) + '\',\'' + index + '\', SPCreateOrder.productWarehouseType.SALE_ORDER_PROMOTION);" href="javascript:void(0)">' + Utils.XSSEncode(row.warehouse.warehouseName) + '</a>';
				}
				
			}
		},
		stockFmt:function(value,row,index) {
			if (row.isFooter) {
				return '';
			}
			var st = 'ppStyle' + index;
			if (row.openPromo == 1) {
				st = 'op_ppStyle' + index;
			} else if (row.promotionType == 5 || row.promotionType == 4) {
				st = 'od_ppStyle' + index;
			}
			if (row.type == PromotionType.FREE_PRICE || row.type == PromotionType.FREE_PERCENT) {
				var maxDiscountAmount = formatFloatValue(row.maxDiscountAmount);
				var discountAmount = formatFloatValue(row.discountAmount);
				var tmp = discountAmount;
				if (isAllowEditPromotion) {
					return '<input  value="' +tmp+'" placeholder="VNĐ" onchange="return SPCreateOrder.qtyPromotionChangedAmount(event,' +index+')" type="text"  style="text-align: right; width: 119px;" id="discountAmount_' +index+'" class="discountAmount InputTextStyle InputText1Style " onkeypress="NextAndPrevTextField(event,this,\'discountAmount\')"  /><span>(' +Utils.XSSEncode(maxDiscountAmount)+') VNĐ</span>';
				} else {
					return '<input disabled="disabled" placeholder="VNĐ" value="' +Utils.XSSEncode(discountAmount)+'" onchange="return SPCreateOrder.qtyPromotionChangedAmount(event,' +index+')" type="text"  style="text-align: right; width: 119px;" id="discountAmount_' +index+'" class="discountAmount InputTextStyle InputText1Style " /><span>(' +Utils.XSSEncode(maxDiscountAmount)+') VNĐ</span>';
				}						
			} else if (row.type == PromotionType.FREE_PRODUCT) {	
				if (row.stock==null) {
					row.stock=0;
				}
				return '<span class="' +st+'" id="stock_' +row.productId+'_' +index+'">' +formatQuantityEx(row.stock,row.convfact)+'</span>';
			}
		},
		openProductStockFmt:function(value,row,index) {
			if (row.isFooter) {
				return '';
			}
			if (row.type == PromotionType.FREE_PRICE || row.type == PromotionType.FREE_PERCENT) {
				var maxDiscountAmount = formatFloatValue(row.maxDiscountAmount);
				var discountAmount = formatFloatValue(row.discountAmount);
				var tmp = discountAmount;
				if (isAllowEditPromotion) {
					return '<input  value="' +tmp+'" placeholder="VNĐ" onchange="OpenProductPromotion.qtyPromotionChangedAmount(event,' +index+')" type="text"  style="text-align: right; width: 119px;" id="opDiscountAmount_' +index+'" class="opDiscountAmount InputTextStyle InputText1Style " onkeypress="NextAndPrevTextField(event,this,\'opDiscountAmount\')"  /><span>(' +Utils.XSSEncode(maxDiscountAmount)+') VNĐ</span>';
				} else {
					return '<input disabled="disabled" placeholder="VNĐ" value="' +discountAmount+'" onchange="OpenProductPromotion.qtyPromotionChangedAmount(event,' +index+')" type="text"  style="text-align: right; width: 119px;" id="opDiscountAmount_' +index+'" class="opDiscountAmount InputTextStyle InputText1Style " /><span>(' +Utils.XSSEncode(maxDiscountAmount)+') VNĐ</span>';
				}						
			} else if (row.type == PromotionType.FREE_PRODUCT) {	
				if (row.stock==null) {
					row.stock=0;
				}
				return '<span class="op_ppStyle' +index+'" id="stock_' +row.productId+'_' +index+'">' +formatQuantityEx(row.stock,row.convfact)+'</span>';
			}
		},
		
		quantityFmt:function(value,row,index) {
			if (row.isFooter) {
				return '<b class="clazzfootervalue" >' +Utils.XSSEncode(value)+' Kg</b>';
			}
			var st = 'ppStyle' + index;
			if (row.openPromo == 1) {
				st = 'op_ppStyle' + index;
			} else if (row.promotionType == 5 || row.promotionType == 4) {
				st = 'od_ppStyle' + index;
			}
			if (row.type == PromotionType.FREE_PRICE || row.type == PromotionType.FREE_PERCENT) {
				return '';
			}else if(row.type == PromotionType.FREE_PRODUCT) {	
				//var maxQuantity = !isNullOrEmpty(row.maxQuantity) && row.maxQuantity != 0? formatQuantity(row.maxQuantity,row.convfact):'0';
				/** vuongmq; 08/01/2015; hien thi maxQuantityFreeTotal so luong tinh khuyen mai*/
				//var maxQuantity = !isNullOrEmpty(row.maxQuantityFreeTotal) && row.maxQuantityFreeTotal != 0? formatQuantity(row.maxQuantityFreeTotal,row.convfact):'0'; 
				var maxQuantity = !isNullOrEmpty(row.quantity) && row.quantity != 0 ? formatQuantity(row.quantity, row.convfact) : '0'; 
				var value = !isNullOrEmpty(row.quantity) && row.quantity != 0? formatQuantityEx(row.quantity,row.convfact):'0';
				if(isAllowEditPromotion) {
					if((row.isEdited == 0 && (row.quantity <= row.stock || row.hasPortion == 1)) || row.isEdited == 2){
						if(!isNullOrEmpty(row.commercialSupport)){
							return '<input type="text" disabled="disabled" index=' + index +' onchange="return SPCreateOrder.qtyPromotionChanged(event, \'' + Utils.XSSEncode(row.programCode + '_' + row.productId + '_' + row.productGroupId +'_' + row.levelPromo+'_' + (row.warehouse?(row.warehouse.id||row.warehouse.warehouseId):0))+ '\', 0);" '
									+ 'style="text-align: right; width: 50px;" id="convact_promotion_value_' +index+'" ' 
									+ 'class="cls_Promotion_Convfact_Value InputTextStyle InputText1Style ' +st+'" onkeypress="NextAndPrevTextField(event,this,\'cls_Promotion_Convfact_Value\')" ' 
									+ 'value="' +Utils.XSSEncode(value)+'"/><span class="' +st+'">(' +Utils.XSSEncode(maxQuantity)+')</span>';
						} else {
							return '<input type="text" disabled="disabled" index=' + index +' onchange="return SPCreateOrder.qtyPromotionChanged(event, ' +index+', 0);" style="text-align: right; width: 50px;" id="convact_promotion_value_' +index+'" class="cls_Promotion_Convfact_Value InputTextStyle InputText1Style ' +st+'" onkeypress="NextAndPrevTextField(event,this,\'cls_Promotion_Convfact_Value\')" value="' +Utils.XSSEncode(value)+'"/><span class="' +st+'">(' +Utils.XSSEncode(maxQuantity)+')</span>';
						}
					} else {
						if(!isNullOrEmpty(row.commercialSupport)){
							return '<input type="text" index=' + index +' onchange="return SPCreateOrder.qtyPromotionChanged(event, \'' + Utils.XSSEncode(row.programCode + '_' + row.productId + '_' + row.productGroupId +'_' + row.levelPromo+'_' + (row.warehouse?(row.warehouse.id||row.warehouse.warehouseId):0))+ '\', 0);" '
									+ 'style="text-align: right; width: 50px;" id="convact_promotion_value_' +index+'" ' 
									+ 'class="cls_Promotion_Convfact_Value InputTextStyle InputText1Style ' +st+'" onkeypress="NextAndPrevTextField(event,this,\'cls_Promotion_Convfact_Value\')" ' 
									+ 'value="' +Utils.XSSEncode(value)+'"/><span class="' +st+'">(' +Utils.XSSEncode(maxQuantity)+')</span>';
						} else {
							return '<input type="text" index=' + index +' onchange="return SPCreateOrder.qtyPromotionChanged(event, ' +index+', 0);" style="text-align: right; width: 50px;" id="convact_promotion_value_' +index+'" class="cls_Promotion_Convfact_Value InputTextStyle InputText1Style ' +st+'" onkeypress="NextAndPrevTextField(event,this,\'cls_Promotion_Convfact_Value\')" value="' +Utils.XSSEncode(value)+'"/><span class="' +st+'">(' +Utils.XSSEncode(maxQuantity)+')</span>';
						}
					}
				} else {
					return '<input disabled="disabled" index=' + index +' type="text" onchange="return SPCreateOrder.qtyPromotionChanged(event, ' +index+', 0);" style="text-align: right; width: 50px;" id="convact_promotion_value_' +index+'" class="cls_Promotion_Convfact_Value InputTextStyle InputText1Style ' +st+'" value="' +Utils.XSSEncode(value)+'"/><span class="' +st+'">(' +Utils.XSSEncode(maxQuantity)+')</span>';
				}
			}
		},

		openProductQuantityFmt:function(value,row,index) {
			if (row.isFooter) {
				return '<b class="clazzfootervalue" >' +Utils.XSSEncode(value)+' Kg</b>';
			}
			if(row.type == PromotionType.FREE_PRICE || row.type == PromotionType.FREE_PERCENT) {
				return '';
			}
			var maxQuantity = !isNullOrEmpty(row.maxQuantity) && row.maxQuantity != 0? formatQuantity(row.maxQuantity,row.convfact):'0';
			var value = !isNullOrEmpty(row.quantity) && row.quantity != 0? formatQuantityEx(row.quantity,row.convfact):'0';
			if (isAllowEditPromotion) {
				if((row.isEdited == 0 && (row.quantity <= row.stock || row.hasPortion == 1)) || row.isEdited == 2){
					if(!isNullOrEmpty(row.commercialSupport)){
						return '<input type="text" disabled="disabled" index=' + index +' onchange="OpenProductPromotion.qtyPromotionChanged(event, \'' + Utils.XSSEncode(row.programCode + '_' + row.productId + '_' + row.productGroupId +'_' + row.levelPromo+'_' + (row.warehouse?(row.warehouse.id||row.warehouse.warehouseId):0))+ '\');" '
								+ 'style="text-align: right; width: 50px;" id="op_convact_promotion_value_' +index+'" ' 
								+ 'class="cls_Promotion_Convfact_Value InputTextStyle InputText1Style op_ppStyle' +index+'" onkeypress="NextAndPrevTextField(event,this,\'cls_Promotion_Convfact_Value\')" ' 
								+ 'value="' +Utils.XSSEncode(value)+'"/><span class="op_ppStyle' +index+'">(' +Utils.XSSEncode(maxQuantity)+')</span>';
					} else {
						return '<input type="text" disabled="disabled" index=' + index +' onchange="OpenProductPromotion.qtyPromotionChanged(event, ' +index+');" style="text-align: right; width: 50px;" id="op_convact_promotion_value_' +index+'" class="cls_Promotion_Convfact_Value InputTextStyle InputText1Style op_ppStyle' +index+'" onkeypress="NextAndPrevTextField(event,this,\'cls_Promotion_Convfact_Value\')" value="' +Utils.XSSEncode(value)+'"/><span class="op_ppStyle' +index+'">(' +Utils.XSSEncode(maxQuantity)+')</span>';
					}
				} else {
					if(!isNullOrEmpty(row.commercialSupport)){
						return '<input type="text" index=' + index +' onchange="OpenProductPromotion.qtyPromotionChanged(event, \'' + Utils.XSSEncode(row.programCode + '_' + row.productId + '_' + row.productGroupId +'_' + row.levelPromo+'_' + (row.warehouse?(row.warehouse.id||row.warehouse.warehouseId):0))+ '\');" '
								+ 'style="text-align: right; width: 50px;" id="op_convact_promotion_value_' +index+'" ' 
								+ 'class="cls_Promotion_Convfact_Value InputTextStyle InputText1Style op_ppStyle' +index+'" onkeypress="NextAndPrevTextField(event,this,\'cls_Promotion_Convfact_Value\')" ' 
								+ 'value="' +Utils.XSSEncode(value)+'"/><span class="op_ppStyle' +index+'">(' +Utils.XSSEncode(maxQuantity)+')</span>';
					} else {
						return '<input type="text" index=' + index +' onchange="OpenProductPromotion.qtyPromotionChanged(event, ' +index+');" style="text-align: right; width: 50px;" id="op_convact_promotion_value_' +index+'" class="cls_Promotion_Convfact_Value InputTextStyle InputText1Style op_ppStyle' +index+'" onkeypress="NextAndPrevTextField(event,this,\'cls_Promotion_Convfact_Value\')" value="' +Utils.XSSEncode(value)+'"/><span class="op_ppStyle' +index+'">(' +Utils.XSSEncode(maxQuantity)+')</span>';
					}
				}
			} else {
				return '<input disabled="disabled" index=' + index +' type="text" onchange="OpenProductPromotion.qtyPromotionChanged(event, ' +index+');" style="text-align: right; width: 50px;" id="op_convact_promotion_value_' +index+'" class="cls_Promotion_Convfact_Value InputTextStyle InputText1Style op_ppStyle' +index+'" value="' +Utils.XSSEncode(value)+'"/><span class="op_ppStyle' +index+'">(' +Utils.XSSEncode(maxQuantity)+')</span>';
			}
		},
		
		orderQuantityFmt:function(value,row,index) {
			if (row.isFooter) {
				return '<b class="clazzfootervalue" >' +Utils.XSSEncode(value)+' Kg</b>';
			}
			var st = 'ppStyle' + index;
			if (row.openPromo == 1) {
				st = 'op_ppStyle' + index;
			} else if (row.promotionType == 5 || row.promotionType == 4) {
				st = 'od_ppStyle' + index;
			}
			if(row.type == PromotionType.FREE_PRICE || row.type == PromotionType.FREE_PERCENT) {
				return '';
			}else if(row.type == PromotionType.FREE_PRODUCT) {
				//var maxQuantity = !isNullOrEmpty(row.maxQuantity) && row.maxQuantity != 0? formatQuantity(row.maxQuantity,row.convfact):'0';
				/** vuongmq; 08/01/2015; hien thi maxQuantityFreeTotal so luong tinh khuyen mai*/
				var maxQuantity = !isNullOrEmpty(row.maxQuantityFreeTotal) && row.maxQuantityFreeTotal != 0? formatQuantity(row.maxQuantityFreeTotal,row.convfact):'0';
				var value = !isNullOrEmpty(row.quantity) && row.quantity != 0? formatQuantityEx(row.quantity,row.convfact):'0';
				if(isAllowEditPromotion) {
					if((row.isEdited == 0 && (row.quantity <= row.stock || row.hasPortion == 1)) || row.isEdited == 2){
						if(!isNullOrEmpty(row.commercialSupport)){
							return '<input type="text" disabled="disabled" index=' + index +' onchange="return SPCreateOrder.qtyPromotionChanged(event, \'' +Utils.XSSEncode(row.programCode+'_' +row.productId + '_' + row.productGroupId +'_' + row.levelPromo+'_' + (row.warehouse?(row.warehouse.id||row.warehouse.warehouseId):0)) + '\', 1);" style="text-align: right; width: 50px;" id="convact_promotion_value_order_' +index
								+'" class="cls_Promotion_Convfact_Value InputTextStyle InputText1Style ' +st+'" onkeypress="NextAndPrevTextField(event,this,\'cls_Promotion_Convfact_Value\')" value="' +Utils.XSSEncode(value)+'"/><span class="' +st+'">(' +Utils.XSSEncode(maxQuantity)+')</span>';
						} else {
							return '<input type="text" disabled="disabled" index=' + index +' onchange="return SPCreateOrder.qtyPromotionChanged(event, ' +index+', 1);" style="text-align: right; width: 50px;" id="convact_promotion_value_order_' +index+'" class="cls_Promotion_Convfact_Value InputTextStyle InputText1Style ' +st+'" onkeypress="NextAndPrevTextField(event,this,\'cls_Promotion_Convfact_Value\')" value="' +value+'"/><span class="' +st+'">(' +Utils.XSSEncode(maxQuantity)+')</span>';
						}
					} else {
						if(!isNullOrEmpty(row.commercialSupport)){
							return '<input type="text" index=' + index +' onchange="return SPCreateOrder.qtyPromotionChanged(event, \'' +row.programCode+'_' +row.productId + '_' + row.productGroupId +'_' + row.levelPromo+'_' + (row.warehouse?(row.warehouse.id||row.warehouse.warehouseId):0) + '\', 1);" style="text-align: right; width: 50px;" id="convact_promotion_value_order_' +index
							+'" class="cls_Promotion_Convfact_Value InputTextStyle InputText1Style ' +st+'" onkeypress="NextAndPrevTextField(event,this,\'cls_Promotion_Convfact_Value\')" value="' +Utils.XSSEncode(value)+'"/><span class="' +st+'">(' +Utils.XSSEncode(maxQuantity)+')</span>';
						} else {
							return '<input type="text" index=' + index +' onchange="return SPCreateOrder.qtyPromotionChanged(event, ' +index+', 1);" style="text-align: right; width: 50px;" id="convact_promotion_value_order_' +index+'" class="cls_Promotion_Convfact_Value InputTextStyle InputText1Style ' +st+'" onkeypress="NextAndPrevTextField(event,this,\'cls_Promotion_Convfact_Value\')" value="' +Utils.XSSEncode(value)+'"/><span class="' +st+'">(' +Utils.XSSEncode(maxQuantity)+')</span>';
						}
					}
				} else {
					return '<input disabled="disabled" index=' + index +' type="text" onchange="return SPCreateOrder.qtyPromotionChanged(event, ' +index+', 1);" style="text-align: right; width: 50px;" id="convact_promotion_value_order_' +index+'" class="cls_Promotion_Convfact_Value InputTextStyle InputText1Style ' +st+'" value="' +Utils.XSSEncode(value)+'"/><span class="' +st+'">(' +Utils.XSSEncode(maxQuantity)+')</span>';
				}
			}
		},
		discountAmountFmt:function(value,row,index) {
			if (row.isFooter) {
				return '';
			}
			var st = 'ppStyle' + index;
			if (row.openPromo == 1) {
				st = 'op_ppStyle' + index;
			} else if (row.promotionType == 5 || row.promotionType == 4) {
				st = 'od_ppStyle' + index;
			}
			if (row.type == PromotionType.FREE_PERCENT) {
				return '<span class="' +st+'">' + Utils.XSSEncode(formatFloatValue(row.discountPercent,2)) + '%</span>';
			}
			return '';
		},
		checkLotFmt:function(value,row,index) {
			if (row.isFooter) {
				return '';
			}
			return '';
		},
		typeFmt:function(value,row,index) {
			if(row.isFooter){
				return '<b class="clazzfootervalue" >' +Utils.XSSEncode(value)+' VNĐ</b>';
			}
			var st = 'ppStyle' + index;
			if (row.openPromo == 1) {
				st = 'op_ppStyle' + index;
			} else if (row.promotionType == 5 || row.promotionType == 4) {
				st = 'od_ppStyle' + index;
			}
			if(row.type == 2 || row.type == 3) {
				return '<span class="' +st+'">KM Tiền</span>';
			} else if(row.type == 1) {
				return '<span class="' +st+'">KM sản phẩm</span>';
			}
		},
		programCodeFmt: function(value,row,index) {
			if (row.isFooter) {
				return '';
			}
			return '<a class="ppPrCodeStyle" id="promotionProgramCode' + index+'" index=' + index +' href="javascript:void(0)" class="ppStyle' +index+'" onclick="return SPCreateOrder.showPromotionProgramDetail(\'' +Utils.XSSEncode(row.programCode)+'\')">' +Utils.XSSEncode(row.programCode)+'</a>';
		},
		programPortionFmt: function(value,row,index) {
			if (row.isFooter) {
				return '';
			}
			var st = 'ppStyle' + index;
			if (row.openPromo == 1) {
				st = 'op_ppStyle' + index;
			} else if (row.promotionType == 5 || row.promotionType == 4) {
				st = 'od_ppStyle' + index;
			}
			//0: không cho phép sửa số lượng, số suất
			//1: cho phép sửa số lượng, không cho sửa số suất
			//2: không cho phép sửa số lượng, cho phép sửa số suất”
			//var curUrl = window.location.href;
			var showPortion = SPCreateOrder._isViewOrder;//(curUrl.indexOf("/commons/view-info") > -1);
			if (!isNullOrEmpty(row.programCode) && SPCreateOrder._promotionPortionReceivedList != null) {
				var programPortion = SPCreateOrder._promotionPortionReceivedList.get(row.programCode+"_"+row.productGroupId+"_"+row.levelPromo);
				if (programPortion != null && programPortion[0] != null && programPortion[0].isEdited != 1) {
					for (var i = 0;i<programPortion.length;i++) {
						var tmp = programPortion[i];
						var productId = 0;
						//var curUrl = window.location.href;
						if (SPAdjustmentOrder._orderId) {
						 	productId = tmp.productId;
						 	if (!productId) {
						 		productId = tmp.saleOrderDetail.product.id;
						 	}
						} else {
							productId = tmp.saleOrderDetail.product.id;
						}
						if (productId == row.productId) {
							if (SPCreateOrder._saleOrderQuantityReceivedList != null && SPCreateOrder._saleOrderQuantityReceivedList.length > 0) {
								for (var ind=0; ind<SPCreateOrder._saleOrderQuantityReceivedList.length; ind++) {
									var objTmp = SPCreateOrder._saleOrderQuantityReceivedList[ind];
									if (objTmp.promotionCode == row.programCode
										&& objTmp.productGroupId == row.productGroupId
										&& objTmp.promotionLevel == row.levelPromo) {
										if (row.quantityReceived == undefined && row.quantityReceived == null) {
											row.quantityReceived = objTmp.quantityReceived;
										}
										break;
									}
								}
							}
							if (row.quantityReceived == undefined || row.quantityReceived == null || row.quantityReceived < 0) {
								row.quantityReceived = tmp.quantityReceived;
							}
							row.maxQuantityReceived = tmp.quantityReceived;
							row.maxQuantityReceivedTotal = tmp.maxQuantityReceivedTotal;
							if (programPortion[0].isEdited == 0
								&& (row.quantity <= row.stock || row.hasPortion != 1)
								&& row.quantityReceived == row.maxQuantityReceived) {
								row.maxQuantityReceived = tmp.maxQuantityReceived;
								row.maxQuantityReceivedTotal = tmp.maxQuantityReceivedTotal;
								//return '<input disabled="disabled" type="text" index=' + index +'  onchange="return SPCreateOrder.qtyPromotionChangedPortion(event, \'' +Utils.XSSEncode(row.programCode+'_' +row.productId + '_' +row.productGroupId+'_' + row.levelPromo) + '\', 0);" style="text-align: right; width: 20px;" id="portion_promotion_value_' +index+'" class="cls_Promotion_Portion_Value InputTextStyle InputText1Style ' +st+'" onkeypress="NextAndPrevTextField(event,this,\'cls_Promotion_Convfact_Value\')" value="' +Utils.XSSEncode(row.quantityReceived)+'"/><span class="' +st+'">(' +Utils.XSSEncode(tmp.maxQuantityReceived)+')</span>';
								/** vuongmq; 08/01/2015; hien thi maxQuantityReceivedTotal so suat tinh khuyen mai*/
								return '<input disabled="disabled" type="text" index=' + index +'  onchange="return SPCreateOrder.qtyPromotionChangedPortion(event, \'' +Utils.XSSEncode(row.programCode+'_' +row.productId + '_' +row.productGroupId+'_' + row.levelPromo) + '\', 0);" style="text-align: right; width: 20px;" id="portion_promotion_value_' +index+'" class="cls_Promotion_Portion_Value InputTextStyle InputText1Style ' +st+'" onkeypress="NextAndPrevTextField(event,this,\'cls_Promotion_Convfact_Value\')" value="' +Utils.XSSEncode(row.quantityReceived)+'"/><span class="' +st+'">(' +Utils.XSSEncode(tmp.maxQuantityReceivedTotal)+')</span>';
							} else {
								row.maxQuantityReceived = tmp.maxQuantityReceived;
								row.maxQuantityReceivedTotal = tmp.maxQuantityReceivedTotal;
								//return '<input type="text" index=' + index +'  onchange="return SPCreateOrder.qtyPromotionChangedPortion(event, \'' +Utils.XSSEncode(row.programCode+'_' +row.productId + '_' +row.productGroupId+'_' + row.levelPromo) + '\', 0);" style="text-align: right; width: 20px;" id="portion_promotion_value_' +index+'" class="cls_Promotion_Portion_Value InputTextStyle InputText1Style ' +st+'" onkeypress="NextAndPrevTextField(event,this,\'cls_Promotion_Convfact_Value\')" value="' +Utils.XSSEncode(row.quantityReceived)+'"/><span class="' +st+'">(' +Utils.XSSEncode(tmp.maxQuantityReceived)+')</span>';
								/** vuongmq; 08/01/2015; hien thi maxQuantityReceivedTotal so suat tinh khuyen mai*/
								return '<input type="text" index=' + index +'  onchange="return SPCreateOrder.qtyPromotionChangedPortion(event, \'' +Utils.XSSEncode(row.programCode+'_' +row.productId + '_' +row.productGroupId+'_' + row.levelPromo) + '\', 0);" style="text-align: right; width: 20px;" id="portion_promotion_value_' +index+'" class="cls_Promotion_Portion_Value InputTextStyle InputText1Style ' +st+'" onkeypress="NextAndPrevTextField(event,this,\'cls_Promotion_Convfact_Value\')" value="' +Utils.XSSEncode(row.quantityReceived)+'"/><span class="' +st+'">(' +Utils.XSSEncode(tmp.maxQuantityReceivedTotal)+')</span>';
							}
						}
					}
				} else if (programPortion != null && programPortion[0] != null && showPortion) {
					return programPortion[0].quantityReceived;
				}
			}
		},
		openProductProgramPortionFmt: function(value,row,index) {
			if (row.isFooter) {
				return '';
			}
			//0: không cho phép sửa số lượng, số suất
			//1: cho phép sửa số lượng, không cho sửa số suất
			//2: không cho phép sửa số lượng, cho phép sửa số suất
			//var curUrl = window.location.href;
			var showPortion = SPCreateOrder._isViewOrder;//(curUrl.indexOf("/commons/view-info") > -1);
			if (row.type == PromotionType.FREE_PRICE || row.type == PromotionType.FREE_PERCENT) {
				var programPortion = SPCreateOrder._promotionPortionReceivedList.get(row.programCode+"_"+row.productGroupId+"_"+row.levelPromo);
				if (programPortion != null) {
					if (programPortion instanceof Array && programPortion.length > 0
						&& (showPortion || programPortion[0].isEdited != 1)) {
						return programPortion[0].quantityReceived;
					} else if (showPortion || programPortion.isEdited != 1) {
						return programPortion.quantityReceived;
					}
				}
			} else if (!isNullOrEmpty(row.programCode) && SPCreateOrder._promotionPortionReceivedList != null) {
				var programPortion = SPCreateOrder._promotionPortionReceivedList.get(row.programCode+"_"+row.productGroupId+"_"+row.levelPromo);
				if (programPortion != null && programPortion[0] != null && programPortion[0].isEdited != 1) {
					for (var i = 0;i<programPortion.length;i++) {
						var tmp = programPortion[i];
						var productId = 0;
						if (SPAdjustmentOrder._orderId) {
						 	productId = tmp.productId;
						 	if (!productId) {
						 		productId = tmp.saleOrderDetail.product.id;
						 	}
						} else {
							productId = tmp.saleOrderDetail.product.id;
						}
						if (productId == row.productId) {
							if (SPCreateOrder._saleOrderQuantityReceivedList != null && SPCreateOrder._saleOrderQuantityReceivedList.length > 0) {
								for (var ind=0; ind<SPCreateOrder._saleOrderQuantityReceivedList.length; ind++) {
									var objTmp = SPCreateOrder._saleOrderQuantityReceivedList[ind];
									if (objTmp.promotionCode == row.programCode
										&& objTmp.productGroupId == row.productGroupId
										&& objTmp.promotionLevel == row.levelPromo) {
										if (row.quantityReceived == undefined && row.quantityReceived == null) {
											row.quantityReceived = objTmp.quantityReceived;
										}
										break;
									}
								}
							}
							if (row.quantityReceived == undefined || row.quantityReceived == null || row.quantityReceived < 0) {
								row.quantityReceived = tmp.quantityReceived;
							}
							row.maxQuantityReceived = tmp.quantityReceived;
							if (programPortion[0].isEdited == 0
								&& (row.quantity <= row.stock || row.hasPortion != 1)
								&& row.quantityReceived == row.maxQuantityReceived) {
								row.maxQuantityReceived = tmp.maxQuantityReceived;
								return '<input disabled="disabled" type="text" index=' + index +'  onchange="OpenProductPromotion.qtyPromotionChangedPortion(event, \'' +Utils.XSSEncode(row.programCode+'_' +row.productId + '_' +row.productGroupId+'_' + row.levelPromo) + '\');" style="text-align: right; width: 20px;" id="op_portion_promotion_value_' +index+'" class="cls_Promotion_Portion_Value InputTextStyle InputText1Style op_ppStyle' +index+'" onkeypress="NextAndPrevTextField(event,this,\'cls_Promotion_Convfact_Value\')" value="' +Utils.XSSEncode(row.quantityReceived)+'"/><span class="op_ppStyle' +index+'">(' +Utils.XSSEncode(tmp.maxQuantityReceived)+')</span>';
							} else {
								row.maxQuantityReceived = tmp.maxQuantityReceived;
								if (isAllowEditPromotion) {
									return '<input type="text" index=' + index +'  onchange="OpenProductPromotion.qtyPromotionChangedPortion(event, \'' +Utils.XSSEncode(row.programCode+'_' +row.productId + '_' +row.productGroupId+'_' + row.levelPromo) + '\');" style="text-align: right; width: 20px;" id="op_portion_promotion_value_' +index+'" class="cls_Promotion_Portion_Value InputTextStyle InputText1Style op_ppStyle' +index+'" onkeypress="NextAndPrevTextField(event,this,\'cls_Promotion_Convfact_Value\')" value="' +Utils.XSSEncode(row.quantityReceived)+'"/><span class="op_ppStyle' +index+'">(' +Utils.XSSEncode(tmp.maxQuantityReceived)+')</span>';
								} else {
									return '<input disabled="disabled" type="text" index=' + index +'  onchange="OpenProductPromotion.qtyPromotionChangedPortion(event, \'' +Utils.XSSEncode(row.programCode+'_' +row.productId + '_' +row.productGroupId+'_' + row.levelPromo) + '\');" style="text-align: right; width: 20px;" id="op_portion_promotion_value_' +index+'" class="cls_Promotion_Portion_Value InputTextStyle InputText1Style op_ppStyle' +index+'" onkeypress="NextAndPrevTextField(event,this,\'cls_Promotion_Convfact_Value\')" value="' +Utils.XSSEncode(row.quantityReceived)+'"/><span class="op_ppStyle' +index+'">(' +Utils.XSSEncode(tmp.maxQuantityReceived)+')</span>';
								}
							}
						}
					}
				} else if (programPortion != null && programPortion[0] != null && showPortion) {
					return programPortion[0].quantityReceived;
				}
			}
		},
		
		programPortionOrderFmt: function(value,row,index) {
			if(row.isFooter) {
				return '';
			}
			//var curUrl = window.location.href;
			var showPortion = SPCreateOrder._isViewOrder;//(curUrl.indexOf("/commons/view-info") > -1);
			if (row.promotionType == PromotionType.PROMOTION_FOR_ORDER) {
				var programPortion = SPCreateOrder._promotionOrderReceivedList.get(row.programCode+"_"+row.productGroupId+"_"+row.levelPromo);
				if (programPortion != null && (showPortion || programPortion.isEdited != 1)) {
					return programPortion.quantityReceived;
				}
			} else {
				if (!isNullOrEmpty(row.programCode) && SPCreateOrder._promotionPortionReceivedList != null) {
					var programPortion = SPCreateOrder._promotionPortionReceivedList.get(row.programCode+"_"+row.productGroupId+"_"+row.levelPromo);
					if (programPortion != null && programPortion[0] != null && programPortion[0].isEdited != 1) {
						for (var i = 0;i<programPortion.length;i++) {
							var tmp = programPortion[i];
							var productId = 0;
							//var curUrl = window.location.href;
							if (SPAdjustmentOrder._orderId) {
							 	productId = tmp.productId;
							 	if (!productId) {
							 		productId = tmp.saleOrderDetail.product.id;
							 	}
							} else {
								productId = tmp.saleOrderDetail.product.id;
							}
							if (productId == row.productId) {
								if (SPCreateOrder._saleOrderQuantityReceivedList != null && SPCreateOrder._saleOrderQuantityReceivedList.length > 0) {
									for (var ind=0; ind<SPCreateOrder._saleOrderQuantityReceivedList.length; ind++) {
										var objTmp = SPCreateOrder._saleOrderQuantityReceivedList[ind];
										if (objTmp.promotionCode == row.programCode
											&& objTmp.productGroupId == row.productGroupId
											&& objTmp.promotionLevel == row.levelPromo) {
											if (row.quantityReceived == undefined && row.quantityReceived == null) {
												row.quantityReceived = objTmp.quantityReceived;
											}
											break;
										}
									}
								}
								if (row.quantityReceived == undefined || row.quantityReceived == null || row.quantityReceived < 0) {
									row.quantityReceived = tmp.quantityReceived;
								}
								row.maxQuantityReceived = tmp.quantityReceived;
								row.maxQuantityReceivedTotal = tmp.maxQuantityReceivedTotal;
								if (programPortion[0].isEdited == 0
									&& (row.quantity <= row.stock || row.hasPortion != 1)
									&& row.quantityReceived == row.maxQuantityReceived) {
									row.maxQuantityReceived = tmp.maxQuantityReceived;
									row.maxQuantityReceivedTotal = tmp.maxQuantityReceivedTotal;
									//return '<input disabled="disabled" type="text" index=' + index +' onchange="return SPCreateOrder.qtyPromotionChangedPortion(event, \'' +Utils.XSSEncode(row.programCode+'_' +row.productId + '_' +row.productGroupId+'_' + row.levelPromo) + '\', 1);" style="text-align: right; width: 20px;" id="portion_promotion_value_order_' +index+'" class="cls_Promotion_Portion_Value InputTextStyle InputText1Style od_ppStyle' +index+'" onkeypress="NextAndPrevTextField(event,this,\'cls_Promotion_Convfact_Value\')" value="' +row.quantityReceived+'"/><span class="od_ppStyle' +index+'">(' +Utils.XSSEncode(tmp.maxQuantityReceived)+')</span>';
									/** vuongmq; 08/01/2015; hien thi maxQuantityReceivedTotal so suat tinh khuyen mai*/
									return '<input disabled="disabled" type="text" index=' + index +' onchange="return SPCreateOrder.qtyPromotionChangedPortion(event, \'' +Utils.XSSEncode(row.programCode+'_' +row.productId + '_' +row.productGroupId+'_' + row.levelPromo) + '\', 1);" style="text-align: right; width: 20px;" id="portion_promotion_value_order_' +index+'" class="cls_Promotion_Portion_Value InputTextStyle InputText1Style od_ppStyle' +index+'" onkeypress="NextAndPrevTextField(event,this,\'cls_Promotion_Convfact_Value\')" value="' +row.quantityReceived+'"/><span class="od_ppStyle' +index+'">(' +Utils.XSSEncode(tmp.maxQuantityReceivedTotal)+')</span>';
								} else {
									row.maxQuantityReceived = tmp.maxQuantityReceived;
									row.maxQuantityReceivedTotal = tmp.maxQuantityReceivedTotal;
									if (isAllowEditPromotion) {
										//return '<input type="text" index=' + index +' onchange="return SPCreateOrder.qtyPromotionChangedPortion(event, \'' +Utils.XSSEncode(row.programCode+'_' +row.productId + '_' +row.productGroupId+'_' + row.levelPromo) + '\', 1);" style="text-align: right; width: 20px;" id="portion_promotion_value_order_' +index+'" class="cls_Promotion_Portion_Value InputTextStyle InputText1Style od_ppStyle' +index+'" onkeypress="NextAndPrevTextField(event,this,\'cls_Promotion_Convfact_Value\')" value="' +row.quantityReceived+'"/><span class="od_ppStyle' +index+'">(' +Utils.XSSEncode(tmp.maxQuantityReceived)+')</span>';
										/** vuongmq; 08/01/2015; hien thi maxQuantityReceivedTotal so suat tinh khuyen mai*/
										return '<input type="text" index=' + index +' onchange="return SPCreateOrder.qtyPromotionChangedPortion(event, \'' +Utils.XSSEncode(row.programCode+'_' +row.productId + '_' +row.productGroupId+'_' + row.levelPromo) + '\', 1);" style="text-align: right; width: 20px;" id="portion_promotion_value_order_' +index+'" class="cls_Promotion_Portion_Value InputTextStyle InputText1Style od_ppStyle' +index+'" onkeypress="NextAndPrevTextField(event,this,\'cls_Promotion_Convfact_Value\')" value="' +row.quantityReceived+'"/><span class="od_ppStyle' +index+'">(' +Utils.XSSEncode(tmp.maxQuantityReceivedTotal)+')</span>';
									} else {
										//return '<input disabled="disabled" type="text" index=' + index +' onchange="return SPCreateOrder.qtyPromotionChangedPortion(event, \'' +Utils.XSSEncode(row.programCode+'_' +row.productId + '_' +row.productGroupId+'_' + row.levelPromo) + '\', 1);" style="text-align: right; width: 20px;" id="portion_promotion_value_order_' +index+'" class="cls_Promotion_Portion_Value InputTextStyle InputText1Style od_ppStyle' +index+'" onkeypress="NextAndPrevTextField(event,this,\'cls_Promotion_Convfact_Value\')" value="' +row.quantityReceived+'"/><span class="od_ppStyle' +index+'">(' +Utils.XSSEncode(tmp.maxQuantityReceived)+')</span>';
										/** vuongmq; 08/01/2015; hien thi maxQuantityReceivedTotal so suat tinh khuyen mai*/
										return '<input disabled="disabled" type="text" index=' + index +' onchange="return SPCreateOrder.qtyPromotionChangedPortion(event, \'' +Utils.XSSEncode(row.programCode+'_' +row.productId + '_' +row.productGroupId+'_' + row.levelPromo) + '\', 1);" style="text-align: right; width: 20px;" id="portion_promotion_value_order_' +index+'" class="cls_Promotion_Portion_Value InputTextStyle InputText1Style od_ppStyle' +index+'" onkeypress="NextAndPrevTextField(event,this,\'cls_Promotion_Convfact_Value\')" value="' +row.quantityReceived+'"/><span class="od_ppStyle' +index+'">(' +Utils.XSSEncode(tmp.maxQuantityReceivedTotal)+')</span>';
									}
								}
							}
						}
					} else if (programPortion != null && programPortion[0] != null && showPortion) {
						return programPortion[0].quantityReceived;
					}
				} 
			}
		},
		keyFmt: function(value,row,index) {
			if(row.isFooter || SPAdjustmentOrder._isVansale) {
				return '';
			}
			if(!isAuthorize){
				return '';
			}
			if (row.changeProduct != undefined && row.changeProduct!=null && row.changeProduct!='0' && row.changeProduct!=0 
				){
				var title = '';
				if (row.promotionType==PromotionType.PROMOTION_FOR_ZV21 || row.promotionType==PromotionType.PROMOTION_FOR_ORDER){
					title = 'Đổi CTKM';
				}else{
					title = 'Đổi SPKM';
				}
				/*
				 * neu nhu dang tao don hang thi khi doi SPKM, chay lai ham tinh tien (payment)
				 * nguoc lai, truy van danh sach cac san pham co the doi tu thong tin CTKM, muc KM da luu cua don hang
				 */
				var prepareStatement = 'SPCreateOrder.setSelectingRowIndex(' + (index) + ')';
				if (SPAdjustmentOrder._orderId === null) {
					if (row.promotionType==PromotionType.PROMOTION_FOR_ORDER_TYPE_PRODUCT && Number(row.key)>0){
						return '<a class="ppKeyStyle" id="promotionKey' + index+'" index=' + index +' href="javascript:void(0)" onclick="return ' + prepareStatement + ', ' + 'SPCreateOrder.payment(' +Utils.XSSEncode(row.key)+',' +(index)+',1)">' +Utils.XSSEncode(title)+'</a>';
					}
					
					if (row.canChaneMultiProduct) {
						return '<a class="ppKeyStyle" id="promotionKey' + index+'" index=' + index +' href="javascript:void(0)" onclick="return ' + prepareStatement + ', ' + 'SPCreateOrder.payment(' +row.key+',\'' +row.programCode+' +' +index+'\')">' +Utils.XSSEncode(title)+'</a>';
					} else {
						return '<a class="ppKeyStyle" id="promotionKey' + index+'" index=' + index 
														+ ' href="javascript:void(0)" onclick="return ' + prepareStatement + ', ' + 'SPCreateOrder.payment(' +row.key+',' +index+')">'
														+ Utils.XSSEncode(title) + '</a>';
					}
				} else {
					/*
					 * neu nhu CTKM da bi thay doi co cau thi ko cho doi SPKM
					 */
					if (SPAdjustmentOrder._changedStructurePromotionProgramsInSaleOrder
						&& SPAdjustmentOrder._changedStructurePromotionProgramsInSaleOrder.length > 0
						&& $.inArray(row.programCode, SPAdjustmentOrder._changedStructurePromotionProgramsInSaleOrder) >= 0) {
						return '';
					}

					if(row.promotionType==PromotionType.PROMOTION_FOR_ZV21 || row.promotionType==PromotionType.PROMOTION_FOR_ORDER){
						if (!SPAdjustmentOrder._firstLoadOrderForEdit) {
							return '<a class="ppKeyStyle" id="promotionKey' + index+'" index=' + index 
															+ ' href="javascript:void(0)" onclick="return ' + prepareStatement + ', ' + 'SPCreateOrder.payment(' +Utils.XSSEncode(row.key)+',' +index+')">'
															+ Utils.XSSEncode(title) + '</a>';
						} else {
							//SPAdjustmentOrder._firstLoadOrderForEdit = false;
							return '';
						}						
					} else {
						if(row.promotionType==PromotionType.PROMOTION_FOR_ORDER_TYPE_PRODUCT && Number(row.key)>0){
							if (SPAdjustmentOrder._firstLoadOrderForEdit) {
								//
							} else {
								return '<a class="ppKeyStyle" id="promotionKey' + index+'" index=' + index +' href="javascript:void(0)" onclick="return ' + prepareStatement + ', ' + 'SPCreateOrder.payment(' +Utils.XSSEncode(row.key)+',' +index+',1)">' +Utils.XSSEncode(title)+'</a>';	
							}
							
						} else{
							if (SPAdjustmentOrder._firstLoadOrderForEdit) {
								return '<a class="ppKeyStyle" id="promotionKey' + index + '" index=' + index 
										+ ' href="javascript:void(0)" onclick="return ' + prepareStatement + ', ' + 'SPCreateOrder.changePromotionProductForAdjustSaleOrder(' + index + ',' + (row.promotionType==PromotionType.PROMOTION_FOR_ORDER_TYPE_PRODUCT) + ',' + (row.openPromo == 1) + ')">' +Utils.XSSEncode(title)+'</a>';
							} else {
								if(row.canChaneMultiProduct) {
									return '<a class="ppKeyStyle" id="promotionKey' + index+'" index=' + index +' href="javascript:void(0)" onclick="return ' + prepareStatement + ', ' + 'SPCreateOrder.payment(' +row.key+',\'' +row.programCode+' +' +index+'\')">' +Utils.XSSEncode(title)+'</a>';
								} else {
									return '<a class="ppKeyStyle" id="promotionKey' + index+'" index=' + index 
																	+ ' href="javascript:void(0)" onclick="return ' + prepareStatement + ', ' + 'SPCreateOrder.payment(' +Utils.XSSEncode(row.key)+',' +index+')">'
																	+ Utils.XSSEncode(title) + '</a>';
								}
							}
						}
					}					
				}
			} else {
				return '';
			}
		},
		productCodeFmt:function(value,row,index) {
			if(row.isFooter) {
				return '';
			}
			var st = 'ppStyle' + index;
			if (row.openPromo == 1) {
				st = 'op_ppStyle' + index;
			} else if (row.promotionType == 5 || row.promotionType == 4) {
				st = 'od_ppStyle' + index;
			}
			if(row.promotionType!=undefined && row.promotionType!=null && (row.promotionType==4 || row.promotionType==6)){
				return '<span class="' +st+'">' +Utils.XSSEncode(value)+'</span>';
			}
			if (value.trim().indexOf(" ") > 0) {
				return '<span class="' +st+'">' +Utils.XSSEncode(value)+'</span>';
			}
			return '<a href="javascript:void(0)" class="ppCodeStyle" id="promotionProduct' + index+'" index=' + index +' onclick="return SPCreateOrder.viewProductPromotionDetail(\'' +Utils.XSSEncode(row.productCode)+'\')">' +Utils.XSSEncode(row.productCode)+'</a>';
		}
};
var WEB_COLOR ={
	RED_CODE:'#FF0000',
	GREEN_CODE:'#66FF66',
	ORANGE_CODE:'#FF6600',
	BLACK_CODE:'#000000',
	RED:1,
	GREEN:2,
	ORANGE:3,
	BLACK:4	
};

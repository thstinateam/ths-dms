/**
 * In hóa đơn
 */
var SPPrintOrder = {
	_lstOrder: new Map(),//lst don hang
	_params: null,
	saveOrderCallback: null,
	_isAllowShowPrice : true,
	_isLoad : true,
	_xhSave: null,
	_url: null,
	search:function(){
		var params = VTUtilJS.getFormData('search-form');		
		if(params.shopCodeCB == undefined || params.shopCodeCB == null ){
			params.shopCodeCB = "";
		}
		if(params != null) {
			params.currentShopCode = $('#shopCodeCB').combobox('getValue');
			params.nvbhCode = $('#nvbhCode').combobox('getValue');
			params.nvghCode = $('#nvghCode').combobox('getValue');
			SPPrintOrder._params = params;			
			SPPrintOrder._lstOrder = new Map();
			if(SPPrintOrder._isLoad) {
				SPPrintOrder.initGrid(params);
			} else {
				$('#grid').datagrid('uncheckAll');	
				$('#grid').datagrid('load', params);				
			}
		}
	},
	confirm: function() {
		$('#errorMsg').hide();
		if (SPPrintOrder._lstOrder.size() == 0) {
			$('#errorMsg').html('Bạn chưa chọn đơn hàng để xác nhận').show();
			return;
		}
		$.messager.confirm('Xác nhận','Bạn có muốn xác nhận đơn hàng đã chọn ?', function(r) {
			if (r) {
				SPPrintOrder._url = '/sale-product/print-order/confirm';
				SPPrintOrder._confirm();
			}
		});
	},
	/**
	 * duyet va cap nhat kho
	 * @author trietptm
	 * @since oct 22, 2015
	 */
	confirmAndUpdateStock: function() {
		$('#errorMsg').hide();
		if (SPPrintOrder._lstOrder.size() == 0) {
			$('#errorMsg').html('Bạn chưa chọn đơn hàng để xác nhận').show();
			return;
		}
		$.messager.confirm('Xác nhận và cập nhật kho','Bạn có muốn xác nhận và cập nhật kho đơn hàng đã chọn ?', function(r) {
			if (r) {
				SPPrintOrder._url = '/sale-product/print-order/confirm-update-stock';
				SPPrintOrder._confirm();
			}
		});
	},
	_confirmCancelInvoice: function(data) {
		SPPrintOrder._params.lstRemovedInvoice = [];
		/*for (var i = 0, sz = data.lstOrderHasInvoice.length; i < sz; i++) {
			SPPrintOrder._params.lstRemovedInvoice.push(data.lstOrderHasInvoice[i].returnOrderId)
		}
		$.messager.confirm('Xác nhận', 'Huy hoa don', function(r) {
			if (r) {
				SPPrintOrder._confirm();
			}
		});*/
		var lstTmp = SPPrintOrder._params.lstRemovedInvoice;
		var lstData = data.lstOrderHasInvoice;
		var html = $("#printOrderWarningPopupDiv").html();
		$("#printOrderWarningPopup").dialog({
			title: "Xác nhận",
			close: false,
			modal: true,
			cache: false,
			width: 550,
			height: "auto",
			onOpen: function() {
				$("#printOrderWarningPopup").addClass("easyui-dialog");

				$("#printOrderWarningPopup #invoiceGrid").datagrid({
					data: { total: lstData.length, rows: lstData },
					width: $("#ivGridCtn").width(),
	    				height: 'auto',
	    				singleSelect: true,
	    				checkOnSelect: false,
	    				selectOnCheck: false,
	    				fitColumns: true,
	    				scrollbarSize:0,
	    				rownumbers : true,
	    				columns: [[
	    					{field:'returnOrderNumber', title:"Số đơn hàng", width:200,align:'left',sortable:false,resizable:false, formatter:function(value,row,index) {
    	    						return VTUtilJS.XSSEncode(value);
    	    					}},
    	    					{field:'orderNumber', title:"Đơn hàng gốc", width:150,align:'left',sortable:false,resizable:false, formatter:function(value,row,index) {
    	    						return VTUtilJS.XSSEncode(value);
    	    					}},
    	    					{field:'invoiceNumber', title:"Hóa đơn", width:200,align:'left',sortable:false,resizable:false, formatter:function(value,row,index) {
    	    						return VTUtilJS.XSSEncode(value);
    	    					}},
    	    					{field:'check', checkbox: true}
	    				]],
	    				onCheck: function(idx, r) {
	    					if (lstTmp.indexOf(r.returnOrderId) < 0) {
	    						lstTmp.push(r.returnOrderId);
	    					}
	    				},
	    				onUncheck: function(idx, r) {
	    					var j = lstTmp.indexOf(r.returnOrderId);
	    					if (j > -1) {
	    						lstTmp.splice(j, 1);
	    					}
	    				},
	    				onCheckAll: function(rows) {
	    					for (var i = 0, sz = rows.length; i < sz; i++) {
	    						if (lstTmp.indexOf(rows[i].returnOrderId) < 0) {
		    						lstTmp.push(rows[i].returnOrderId);
		    					}
	    					}
	    				},
	    				onUncheckAll: function(rows) {
	    					for (var i = 0, sz = rows.length; i < sz; i++) {
	    						var j = lstTmp.indexOf(rows[i].returnOrderId);
		    					if (j > -1) {
		    						lstTmp.splice(j, 1);
		    					}
	    					}
	    				},
	    				onLoadSuccess: function(data) {
	    					setTimeout(function() {
	    						CommonSearchEasyUI.fitEasyDialog("printOrderWarningPopup");
	    					}, 500);
	    				}
				});

				$("#btnOK").unbind("click");
				$("#btnOK").bind("click", function(ev) {
					if (lstTmp.length == 0) {
						lstTmp.push(-1);
					}
					$.messager.confirm('Xác nhận', 'Bạn muốn tiếp tục xác nhận đơn hàng?', function(r) {
						if (r) {
							SPPrintOrder._confirm();
							setTimeout(function () {
								$("#printOrderWarningPopup").dialog("close");
							}, 200);
						}
					});
				});
			},
			onClose: function() {
				$("#printOrderWarningPopup").dialog("destroy");
				$("#printOrderWarningPopupDiv").html(html);
				SPPrintOrder._params.lstRemovedInvoice = [];
			}
		});
	},
	_confirm: function() {
		$('.ErrorMsgStyle').hide();
		var params = SPPrintOrder._params;
		params.listId = SPPrintOrder._lstOrder.keyArray;
//		VTUtilJS.postFormJson(params, '/sale-product/print-order/confirm', function(data) {
		Utils.saveData(params, SPPrintOrder._url, SPPrintOrder._xhSave, "errorMsg", function(data) {
			$('.ErrorMsgStyle').hide();
			$('.SuccessMsgStyle').html('').hide();
			if (data.error) {
				if(data.error && data.errMsg != undefined && data.errMsg == 'DON_HANG_DA_XU_LY'){
					data.errMsg = 'Đã có đơn hàng đã xử lý, yêu cầu kiểm tra lại';
					return;
				}
				VTUtilJS.showMessageBanner(true, data.errMsg);
			} else {
				if (data.lstOrderHasInvoice != undefined && data.lstOrderHasInvoice != null && data.lstOrderHasInvoice.length > 0) {
					SPPrintOrder._confirmCancelInvoice(data);
					return;
				}
				SPPrintOrder._params.lstRemovedInvoice = null;
				var listError = data.listError;
				if(!VTUtilJS.isNullOrEmpty(listError) && $.isArray(listError) && listError.length == SPPrintOrder._lstOrder.size()) {
					var html = '<div id="popup-error-container">\
						<div id="popup-error">\
							<div class="PopupContentMid">\
								<div class="GeneralForm Search1Form">\
									<div id="gridErrorContainer">\
									<div id="gridError"></div>\
									</div>\
									<div class="Clear"></div>\
								</div>\
							<div class="BtnCenterSection">\
						</div>\
						</div>';
					$("body").append(html);
					$('#popup-error').dialog({
						title: 'Cảnh báo',
						width: 1200,
						heigth: "auto",
						modal: true, close: false, cache: false,
						onOpen: function() {
							$('#gridError').datagrid({
								data : listError,
								width: $("#gridProductContainer").width(),
			    				height: 'auto',
			    				singleSelect: true,
			    				fitColumns: true,
			    				scrollbarSize:0,
			    				rownumbers : true,
			    				columns: [[
	    	    					{field: 'orderNumber', title: "Số đơn hàng", width: 180, align: 'left', sortable: false, resizable: false, formatter: function(value,row,index) {
	    	    						return VTUtilJS.XSSEncode(value);
	    	    					}},
//	    	    					{field: 'lockDate', title: "Ngày chốt", width: 100, align: 'center', sortable: false, resizable: false, formatter: function(value, row, index) {
//	    	    						return VTUtilJS.XSSEncode(value);
//	    	    					}},
	    	    					{field: 'promotion', title: "Hết số suất CTKM", width: 100, align: 'center', sortable: false, resizable: false, formatter: function(value, row, index) {
	    	    						return VTUtilJS.XSSEncode(value);
	    	    					}},
	    	    					{field:  'discountAmountPromotion', title:  "Hết số tiền trả CTKM", width: 100, align: 'center', sortable: false, resizable: false, formatter: function(value, row, index) {
	    	    						return VTUtilJS.XSSEncode(value);
	    	    					}},
	    	    					{field: 'numPromotion', title: "Hết số lượng trả CTKM", width: 100, align: 'center', sortable: false, resizable: false, formatter: function(value, row, index) {
	    	    						return VTUtilJS.XSSEncode(value);
	    	    					}},
	    	    					{field: 'stockCounting', title: "Đang kiểm kê kho", width: 100, align: 'center', sortable: false, resizable: false, formatter: function(value, row, index) {
	    	    						return VTUtilJS.XSSEncode(value);
	    	    					}},
//	    	    					{field: 'NVGH', title: "Chưa có NVGH", width: 100,align: 'center',sortable: false,resizable: false,formatter: function(value, row, index) {
//	    	    						return VTUtilJS.XSSEncode(value);
//	    	    					}},
//	    	    					{field: 'openPromotion', title: "Chưa trả KM mở mới", width: 100,align: 'center',sortable: false,resizable: false,formatter: function(value, row, index) {
//	    	    						return VTUtilJS.XSSEncode(value);
//	    	    					}},
//	    	    					{field: 'lostOpenPromotion', title: "Không thỏa điều kiện mở mới", width: 100,align: 'center',sortable: false,resizable: false,formatter: function(value, row, index) {
//	    	    						return VTUtilJS.XSSEncode(value);
//	    	    					}},
//	    	    					{field: 'accumulation', title: "Đã trả KM tích lũy", width: 100,align: 'center',sortable: false,resizable: false,formatter: function(value, row, index) {
//	    	    						return VTUtilJS.XSSEncode(value);
//	    	    					}},
	    	    					{field: 'orderConfirmedAlready', title: "Đơn hàng đã xác nhận", width: 100, align: 'center', sortable: false, resizable: false, formatter: function(value, row, index) {
	    	    						return VTUtilJS.XSSEncode(value);
	    	    					}},
	    	    					//KeyShop
	    	    					{field: 'overRewardMoney', title: "Có CTHTTM trả thưởng tiền vượt mức", width: 100, align: 'center', sortable: false, resizable: false, formatter: function(value, row, index) {
	    	    						return VTUtilJS.XSSEncode(value);
	    	    					}},
	    	    					{field: 'overRewardProduct', title: "Có CTHTTM trả thưởng sản phẩm vượt mức ", width: 100, align: 'center', sortable: false, resizable: false, formatter: function(value, row, index) {
	    	    						return VTUtilJS.XSSEncode(value);
	    	    					}},
	    	    					{field: 'failQuantityStockTotal', title: "Tồn kho không đáp ứng", width: 100, align: 'center', sortable: false, resizable: false, formatter: function(value, row, index) {
	    	    						return VTUtilJS.XSSEncode(value);
	    	    					}},
	    	    					{field: 'customerNotActive', title: "Khách hàng ngưng hoạt động", width: 100, align: 'center', sortable: false, resizable: false, formatter: function(value, row, index) {
	    	    						return VTUtilJS.XSSEncode(value);
	    	    					}},
	    	    					{field: 'systemError', title: "Lỗi hệ thống", width: 100, align: 'center', sortable: false, resizable: false, formatter: function(value, row, index) {
	    	    						return VTUtilJS.XSSEncode(value);
	    	    					}}
	    	    					
	    	    				]],
	    	    				onLoadSuccess: function(data) {
	    	    					$('.datagrid-header-rownumber').html('STT');
	    	    					setTimeout(function() {CommonSearchEasyUI.fitEasyDialog("popup-error");}, 500);
	    	    				}
							});
						},
						onClose: function() {
							$("#popup-error").dialog("destroy");
							$("#popup-error-container").remove();
							if (!isNullOrEmpty(data.errCode)) {
				    			StockUpdateOrder.showPopupUpdateStockTotal(data.errCode);
				    		}
						}
					});
				} else if(!VTUtilJS.isNullOrEmpty(listError) && $.isArray(listError) && listError.length > 0) {
					var html = '<div id="popup-error-container">\
						<div id="popup-error">\
							<div class="PopupContentMid">\
								<div class="GeneralForm Search1Form">\
									<div id="gridErrorContainer">\
									<div id="gridError"></div>\
									</div>\
									<div class="Clear"></div>\
								</div>\
							<div class="BtnCenterSection">\
						</div>\
						</div>';
					$("body").append(html);
					$('#popup-error').dialog({
						title: 'Cảnh báo',
						width: 1200,
						heigth: "auto",
						modal: true, close: false, cache: false,
						onOpen: function() {
							$('#popup-error').addClass("easyui-dialog");
							$('#gridError').datagrid({
								data : listError,
								width: $("#gridProductContainer").width(),
			    				height: 'auto',
			    				singleSelect: true,
			    				fitColumns: true,
			    				scrollbarSize:0,
			    				rownumbers : true,
			    				columns: [[
		    	    					{field: 'orderNumber', title: "Số đơn hàng", width: 180, align: 'left', sortable: false, resizable: false, formatter: function(value,row,index) {
		    	    						return VTUtilJS.XSSEncode(value);
		    	    					}},
//			    	    				{field: 'lockDate', title: "Ngày chốt", width: 100, align: 'center', sortable: false, resizable: false, formatter: function(value, row, index) {
//			    	    					return VTUtilJS.XSSEncode(value);
//			    	    				}},
		    	    					{field: 'promotion', title: "Hết số suất CTKM", width: 100, align: 'center', sortable: false, resizable: false, formatter: function(value, row, index) {
		    	    						return VTUtilJS.XSSEncode(value);
		    	    					}},
		    	    					{field: 'discountAmountPromotion', title: "Hết số tiền trả CTKM", width: 100, align: 'center', sortable: false, resizable: false, formatter: function(value, row, index) {
		    	    						return VTUtilJS.XSSEncode(value);
		    	    					}},
		    	    					{field: 'numPromotion', title: "Hết số lượng trả CTKM", width: 100, align: 'center', sortable: false, resizable: false, formatter: function(value, row, index) {
		    	    						return VTUtilJS.XSSEncode(value);
		    	    					}},
		    	    					{field: 'stockCounting', title: "Đang kiểm kê kho", width: 100, align: 'center', sortable: false, resizable: false, formatter: function(value, row, index) {
		    	    						return VTUtilJS.XSSEncode(value);
		    	    					}},
//			    	    				{field: 'NVGH', title: "Chưa có NVGH", width: 100,align: 'center',sortable: false,resizable: false,formatter: function(value, row, index) {
//			    	    					return VTUtilJS.XSSEncode(value);
//			    	    				}},
//			    	    				{field: 'openPromotion', title: "Chưa trả KM mở mới", width: 100,align: 'center',sortable: false,resizable: false,formatter: function(value, row, index) {
//			    	    					return VTUtilJS.XSSEncode(value);
//			    	    				}},
//			    	    				{field: 'lostOpenPromotion', title: "Không thỏa điều kiện mở mới", width: 100,align: 'center',sortable: false,resizable: false,formatter: function(value, row, index) {
//			    	    					return VTUtilJS.XSSEncode(value);
//			    	    				}},
//			    	    				{field: 'accumulation', title: "Đã trả KM tích lũy", width: 100,align: 'center',sortable: false,resizable: false,formatter: function(value, row, index) {
//			    	    					return VTUtilJS.XSSEncode(value);
//			    	    				}},
		    	    					{field: 'orderConfirmedAlready', title: "Đơn hàng đã xác nhận", width: 100, align: 'center', sortable: false, resizable: false, formatter: function(value, row, index) {
		    	    						return VTUtilJS.XSSEncode(value);
		    	    					}},
		    	    					//KeyShop
		    	    					{field: 'overRewardMoney', title: "Có CTHTTM trả thưởng tiền vượt mức", width: 100, align: 'center', sortable: false, resizable: false, formatter: function(value, row, index) {
		    	    						return VTUtilJS.XSSEncode(value);
		    	    					}},
		    	    					{field: 'overRewardProduct', title: "Có CTHTTM trả thưởng sản phẩm vượt mức ", width: 100, align: 'center', sortable: false, resizable: false, formatter: function(value, row, index) {
		    	    						return VTUtilJS.XSSEncode(value);
		    	    					}},
		    	    					{field: 'failQuantityStockTotal', title: "Tồn kho không đáp ứng", width: 100, align: 'center', sortable: false, resizable: false, formatter: function(value, row, index) {
		    	    						return VTUtilJS.XSSEncode(value);
		    	    					}},
		    	    					{field: 'customerNotActive', title: "Khách hàng ngưng hoạt động", width: 100, align: 'center', sortable: false, resizable: false, formatter: function(value, row, index) {
		    	    						return VTUtilJS.XSSEncode(value);
		    	    					}},
		    	    					{field: 'systemError', title: "Lỗi hệ thống", width: 100, align: 'center', sortable: false, resizable: false, formatter: function(value, row, index) {
		    	    						return VTUtilJS.XSSEncode(value);
		    	    					}}
		    	    				]],
	    	    				onLoadSuccess: function(data) {
	    	    					$('.datagrid-header-rownumber').html('STT');
	    	    					setTimeout(function() {CommonSearchEasyUI.fitEasyDialog("popup-error");}, 500);
	    	    				}
							});
						},
						onClose: function() {
							$("#popup-error").dialog("destroy");
							$("#popup-error-container").remove();
							if (!isNullOrEmpty(data.errCode)) {
				    			StockUpdateOrder.showPopupUpdateStockTotal(data.errCode);
				    		}
						}
					});
					/*SPPrintOrder._lstOrder = new Map();
					$('#grid').datagrid('uncheckAll');
					$('#grid').datagrid('reload', params);*/
				} else if (!isNullOrEmpty(data.errCode)) {
	    			StockUpdateOrder.showPopupUpdateStockTotal(data.errCode);
	    		} else {
					$('#successMsg').html('Xác nhận đơn hàng thành công').show();
					setTimeout(function() {
						$('#successMsg').html('').hide();
					}, 3000);
					/*SPPrintOrder._lstOrder = new Map();
					$('#grid').datagrid('uncheckAll');
					$('#grid').datagrid('reload', params);*/
				}			
				
				SPPrintOrder.search();
			}
		}, null, null, false, null, true);
	},
	/**
	 * cancel orders
	 * @author tuannd20
	 * @return {void}
	 * @date 03/11/2014
	 */
	cancelOrder: function() {
		$('#errorMsg').html('').hide();
		var params = SPPrintOrder._params;
		if(SPPrintOrder._lstOrder.size() == 0) {
			$('#errorMsg').html('Bạn chưa chọn đơn hàng để hủy.').show();
			return;
		}
		params.lstOrderNumber = SPPrintOrder._lstOrder.valArray.map(function(row){
																    return row.refOrder;
																});
		Utils.addOrSaveData(params, '/sale-product/print-order/cancel', null, null, function(data) {
			$('#errorMsg').html('').hide();
			$('#successMsg').html('').hide();
			if(data.error) {
				VTUtilJS.showMessageBanner(true, data.errMsg);
			} else {
				var successOrders = data.successOrders && data.successOrders.length > 0 
										? data.successOrders.reduce(function(prevStr, orderNumber){
												return Utils.XSSEncode(prevStr + ', ' + orderNumber);
											}) 
										: "";
				if (!isNullOrEmpty(successOrders)) {
					$('#successMsg').html('Hủy thành công các đơn hàng: ' + successOrders).show();
				}

				var errorOrdersMsg = data.errorOrdersMsg && data.errorOrdersMsg.length > 0 
										? data.errorOrdersMsg.reduce(function(prevStr, msg){
												return prevStr + (prevStr == "" ? "" : '<br>') + '- ' + msg;
											}, "") 
										: "";
				if (!isNullOrEmpty(errorOrdersMsg)) {
					$('#errorMsg').html('Các đơn hàng lỗi khi hủy:<br>' + Utils.XSSEncode(errorOrdersMsg)).show();
				}

				setTimeout(function() {
					$('#successMsg').html('').hide();
				}, 5000);
				
				SPPrintOrder.search();
			}
		}, null, null, null, 'Bạn có muốn hủy đơn hàng đã chọn?', null, '1');		
		/*$.messager.confirm('Xác nhận','Bạn có muốn hủy đơn hàng đã chọn?',function(r){
			if (r){
//				VTUtilJS.postFormJson(params, '/sale-product/print-order/cancel', function(data) {
							
			}
		});*/
	},
	cancelOneOrder: function(orderNumber) {
		$('.ErrorMsgStyle').hide();
		var params = {};
		if(!orderNumber || orderNumber.trim().length == 0) {
			return;
		}
		params.lstOrderNumber = [orderNumber];
		$.messager.confirm('Xác nhận','Bạn có muốn hủy đơn hàng?',function(r){
			if (r){
				Utils.saveData(params, '/sale-product/print-order/cancel', null, 'errMsgOrder', function(data) {
					$('#successMsg').hide();
					if(data.error) {
						VTUtilJS.showMessageBanner(true, data.errMsg);
					} else {
						var successOrders = data.successOrders && data.successOrders.length > 0 
												? data.successOrders.reduce(function(prevStr, orderNo){
														return prevStr + ', ' + orderNo;
													}) 
												: "";
						if (!isNullOrEmpty(successOrders)) {
							$('#successMsg').html('Hủy đơn hàng thành công.').show();
						} else {
							var errorOrdersMsg = data.errorOrdersMsg && data.errorOrdersMsg.length > 0 
												? data.errorOrdersMsg.reduce(function(prevStr, msg){
														return prevStr + (prevStr == "" ? "" : '<br>') + '- ' + msg;
													}, "") 
												: "";
							if (!isNullOrEmpty(errorOrdersMsg)) {
								$('#serverErrors').html('Có lỗi khi hủy đơn hàng').show();
							}
						}

						setTimeout(function() {
							$('#successMsg').hide();
							if (!isNullOrEmpty(successOrders)) {
								window.location.href = '/sale-product/print-order/info';
							}
						}, 2000);
					}
				},null,null,null,null,'true');					
			}
		});
	},
	getResultF9Customer:function(index) {
		var row = $('#common-dialog-grid-search').datagrid('getRows')[index];
		$('#customerCode').val(row.shortCode);
		$('#common-dialog-search-2-textbox').dialog('close');
	},
	/**
	 * Duyet don hang man hinh chinh sua
	 */
	approveOrder: function() {
		//SPCreateOrder.orderChanged = true;
		if (SPCreateOrder.orderChanged) {
			SPPrintOrder.saveOrderCallback = function() {
				setTimeout(function () {
					$('#successMsg').hide();
				}, 150);
				SPPrintOrder._approveOrder();
				SPPrintOrder.saveOrderCallback = null;
			};
			SPCreateOrder.createOrder();
		} else {
			SPPrintOrder.saveOrderCallback = null;
			$.messager.confirm('Xác nhận','Bạn có muốn xác nhận đơn hàng?',function(r){
				if (r) {
					SPPrintOrder._approveOrder();
				}
			});
		}
	},
	_approveOrder: function() {
		$('.ErrorMsgStyle').hide();
		var params = {};

		params.listId = [SPAdjustmentOrder.orderId];
		Utils.saveData(params, '/sale-product/print-order/confirm', null, "serverErrors", function(data) {
//		VTUtilJS.postFormJson(params, '/sale-product/print-order/confirm', function(data) {
			$('.ErrorMsgStyle').hide();
			$('#successMsg').hide();
			if (data.error) {
				$('#serverErrors').html(data.errMsg).show();
			} else {
				var listError = data.listError;
				if(!VTUtilJS.isNullOrEmpty(listError) && $.isArray(listError)) {
					var html = '<div id="popup-error-container">\
						<div id="popup-error">\
							<div class="PopupContentMid">\
								<div class="GeneralForm Search1Form">\
									<div id="gridErrorContainer">\
									<div id="gridError"></div>\
									</div>\
									<div class="Clear"></div>\
								</div>\
							<div class="BtnCenterSection">\
						</div>\
						</div>';
					$("body").append(html);
					$('#popup-error').dialog({
						title: 'Cảnh báo',
						width: 600,
						heigth: "auto",
						modal: true, close: false, cache: false,
						onOpen: function() {
							$('#gridError').datagrid({
								data : listError,
								width: $("#gridProductContainer").width(),
			    				height: 'auto',
			    				singleSelect: true,
			    				fitColumns: true,
			    				scrollbarSize:0,
			    				rownumbers : true,
			    				columns: [[
	    	    					{field: 'orderNumber', title: "Số đơn hàng", width: 180, align: 'left', sortable: false, resizable: false, formatter: function(value,row,index) {
	    	    						return VTUtilJS.XSSEncode(value);
	    	    					}},
//				    	    		{field: 'lockDate', title: "Ngày chốt", width: 100, align: 'center', sortable: false, resizable: false, formatter: function(value, row, index) {
//				    	    			return VTUtilJS.XSSEncode(value);
//				    	    		}},
	    	    					{field: 'promotion', title: "Hết số suất CTKM", width: 100, align: 'center', sortable: false, resizable: false, formatter: function(value, row, index) {
	    	    						return VTUtilJS.XSSEncode(value);
	    	    					}},
	    	    					{field: 'discountAmountPromotion', title: "Hết số tiền trả CTKM", width: 100, align: 'center', sortable: false, resizable: false, formatter: function(value, row, index) {
	    	    						return VTUtilJS.XSSEncode(value);
	    	    					}},
	    	    					{field: 'numPromotion', title: "Hết số lượng trả CTKM", width: 100, align: 'center', sortable: false, resizable: false, formatter: function(value, row, index) {
	    	    						return VTUtilJS.XSSEncode(value);
	    	    					}},
	    	    					{field: 'stockCounting', title: "Đang kiểm kê kho", width: 100, align: 'center', sortable: false, resizable: false, formatter: function(value, row, index) {
	    	    						return VTUtilJS.XSSEncode(value);
	    	    					}},
//				    	    		{field: 'NVGH', title: "Chưa có NVGH", width: 100,align: 'center',sortable: false,resizable: false,formatter: function(value, row, index) {
//				    	    			return VTUtilJS.XSSEncode(value);
//				    	    		}},
//				    	    		{field: 'openPromotion', title: "Chưa trả KM mở mới", width: 100,align: 'center',sortable: false,resizable: false,formatter: function(value, row, index) {
//				    	    			return VTUtilJS.XSSEncode(value);
//				    	    		}},
//				    	    		{field: 'lostOpenPromotion', title: "Không thỏa điều kiện mở mới", width: 100,align: 'center',sortable: false,resizable: false,formatter: function(value, row, index) {
//				    	    			return VTUtilJS.XSSEncode(value);
//				    	    		}},
//				    	    		{field: 'accumulation', title: "Đã trả KM tích lũy", width: 100,align: 'center',sortable: false,resizable: false,formatter: function(value, row, index) {
//				    	    			return VTUtilJS.XSSEncode(value);
//				    	    		}},
	    	    					{field: 'orderConfirmedAlready', title: "Đơn hàng đã xác nhận", width: 100, align: 'center', sortable: false, resizable: false, formatter: function(value, row, index) {
	    	    						return VTUtilJS.XSSEncode(value);
	    	    					}},
	    	    					//KeyShop
	    	    					{field: 'overRewardMoney', title: "Có CTHTTM trả thưởng tiền vượt mức", width: 100, align: 'center', sortable: false, resizable: false, formatter: function(value, row, index) {
	    	    						return VTUtilJS.XSSEncode(value);
	    	    					}},
	    	    					{field: 'overRewardProduct', title: "Có CTHTTM trả thưởng sản phẩm vượt mức ", width: 100, align: 'center', sortable: false, resizable: false, formatter: function(value, row, index) {
	    	    						return VTUtilJS.XSSEncode(value);
	    	    					}},
	    	    					{field: 'failQuantityStockTotal', title: "Tồn kho không đáp ứng", width: 100, align: 'center', sortable: false, resizable: false, formatter: function(value, row, index) {
	    	    						return VTUtilJS.XSSEncode(value);
	    	    					}},
	    	    					
	    	    					{field: 'systemError', title: "Lỗi hệ thống", width: 100, align: 'center', sortable: false, resizable: false, formatter: function(value, row, index) {
	    	    						return VTUtilJS.XSSEncode(value);
	    	    					}}
	    	    				]],
	    	    				onLoadSuccess: function(data) {
	    	    					$('.datagrid-header-rownumber').html('STT');
	    	    					setTimeout(function() {CommonSearchEasyUI.fitEasyDialog("popup-error");}, 500);
	    	    				}
							});
						},
						onClose: function() {
							$("#popup-error").dialog("destroy");
							$("#popup-error-container").remove();
						}
					});
				} else {
					$('#successMsg').html('Xác nhận đơn hàng thành công').show();
					setTimeout(function() {
						window.location.href = '/sale-product/print-order/info';
					}, 2000);
				}
			}
		}, null, null, false, null, true);
	},
	/**
	 * show chi tiet don hang
	 * @author nhutnn	
	 * @since 30/03/2015
	 */
	showDetailInPopup: function(orderId, orderType, shopCode) {
		// popup xem chi tiet		
		var params= new Object();
		params.orderId = orderId;
		if(orderType != null && orderType != ""){
			params.orderType = orderType;
		}
		if(shopCode != undefined && shopCode != null){
			params.currentShopCode = shopCode;
		}
		
		Utils.getHtmlDataByAjax(params,"/commons/view-info",function(data){
			if(data != null){
				var html = $("#billDetailPopupDiv").html();
				$("#billDetailPopup").html("<div class='GeneralForm' style='padding:0;'>" + data + "</div>");
				$("#billDetailPopup").dialog({
					title: "Xem chi tiết đơn hàng",
					cache: false,
					modal: true,
					close: false,
					width: window.innerWidth - 30,
					height: "auto",
					onOpen: function() {
						$("#billDetailPopup").addClass("easyui-dialog");
						$("#billDetailPopup").find($("div[class='SearchInSection SProduct1Form']")).css( "border", 0);
						$("#billDetailPopup").find($("p[class='ValueStyle Value1Style']")).css( "padding-top", 3);			
						$('#billDetailPopup').parent().attr('style', function(i,s) { return s + 'z-index: 90010 !important;' });
					},
					onClose: function() {
						$("#billDetailPopup").dialog("destroy");
						$("#billDetailPopupDiv").html(html);
					}
				});	
			}						
		});	
	},
	
	initUnitCbx: function(){				
		var params = {};
		$.ajax({
			type : "POST",
			url:'/commons/list-child-shop',
			data :($.param(params, true)),
			dataType : "json",
			success : function(data) {
				listNVBH = data.lstNVBH;
				$('#shopCodeCB').combobox({
					valueField: 'shopCode',
					textField:  'shopName',
					data: data.rows,
					panelWidth: '206',
					formatter: function(row) {
						return '<span style="font-weight:bold">' + Utils.XSSEncode(row.shopCode) + '</span><br/>' + '<span style="color:#888">' + Utils.XSSEncode(row.shopName) + '</span>';
					},
					filter: function(q, row){
						q = new String(q).toUpperCase().trim();
						var opts = $(this).combobox('options');
						return ((unicodeToEnglish(row[opts.textField]).indexOf(unicodeToEnglish(q)) == 0) || unicodeToEnglish(row[opts.valueField]).indexOf(unicodeToEnglish(q)) == 0) ;
					},	
			        onSelect: function(param){
			            SPPrintOrder.getStaffByShopCode();
			            
			        },
			        onLoadSuccess: function(){
			        	var arr = $('#shopCodeCB').combobox('getData');
			        	if (arr != null && arr.length > 0){
			        		$('#shopCodeCB').combobox('select', arr[0].shopCode);
			        	}
			        	
			        }
				});
			}
		});
	},
	/**
	*lay danh sach nhan vien theo shop don vi
	*/
	getStaffByShopCode: function() {
		 var currentShopCode = $('#shopCodeCB').combobox('getValue');
		 var params = new Object();
		if(currentShopCode != null){
			params.currentShopCode = currentShopCode;
		}
		var kData = $.param(params, true);
		 $.ajax({
			type : "POST",
			url : '/sale-product/print-order/load-staff-by-shop-code',
			data : (kData),
			dataType : "json",
			success : function(data) {
				SPPrintOrder._isAllowShowPrice = data.isAllowShowPrice;
				SPCreateOrder._isAllowShowPrice = data.isAllowShowPrice;
				if(SPPrintOrder._isAllowShowPrice){
					$('#valueOrderDiv').show();
					$('#isValueOrder').val(-1);
					$('#isValueOrder').change();
				} else {
					$('#valueOrderDiv').hide();
				}
				
				$('#shopId').val(data.shopId);

				var lstNVBHVo = data.lstNVBHVo;
				var lstNVGHVo = data.lstNVGHVo;

				if(lstNVBHVo != undefined && lstNVBHVo != null) {
					Utils.bindStaffCbx('#nvbhCode', lstNVBHVo);
				}
				
				if(lstNVGHVo != undefined && lstNVGHVo != null) {
					Utils.bindStaffCbx('#nvghCode', lstNVGHVo);
				}

				var numberValueOrder = data.numberValue;
				if(numberValueOrder != '' && numberValueOrder != "") {
					SPAdjustmentTax.numberValueOrder = numberValueOrder;
					SPAdjustmentTax._textChangeBySeach.avg = numberValueOrder.replace(/,/g, '');
				}
				
				$('#strLockDate').text(data.lockDate);
				
				if(SPPrintOrder._isLoad){
	        		SPPrintOrder.search();
	        		SPPrintOrder._isLoad = false;
	        	}
				 
			},
			error:function(XMLHttpRequest, textStatus, errorThrown) {
				console.log("Error");
			}
		});

	},
	
	initGrid: function(params) {
		$('#grid').datagrid({  
		    url:'/sale-product/print-order/searchOrder',
		    selectOnCheck:false,
		    rownumbers:true,
		    scrollbarSize:0,
		    fitColumns : ($('#dgGridContainer').width() - 20 > 1700),
			checkOnSelect :false,
			singleSelect: true,
			pageSize : 10,
			pageList : [10,20,50, 100, 200],
			pagination:true,
		    width: $('#dgGridContainer').width() - 20,	    		    
//		    queryParams: SPPrintOrder._params,
		    queryParams: params,
		    frozenColumns: [[
				{field:'cb', checkbox:true, align:'center', width:80,sortable : false,resizable : false},
				{field:'link', title:'', width:50, fixed:true, align:'center', sortable:false, resizable:false, formatter : function(v,r,i) {
					var orderT="";
					if(r.orderType != undefined && r.orderType != null){
						orderT = Utils.XSSEncode(r.orderType);
					}
					var str = '<a href="javascript:void(0);" onclick="SPPrintOrder.showDetailInPopup('+r.id +',\'' + orderT + '\',\'' + r.shopCode + '\');" title="Xem chi tiết" id="aEdit"><img height="17" src="/resources/images/icon-view.png"></a>';
					return str;
				}},
				{field:'refOrder', title:'Số Ref', width:130, align: 'left', formatter : function(v,r,i) {
					return VTUtilJS.XSSEncode(v);
				}},
				{field:'staffCode', title:'NVBH', width:200, align: 'left', formatter : function(v,r,i) {
					if(VTUtilJS.isNullOrEmpty(r.staffCode) && VTUtilJS.isNullOrEmpty(r.staffName)) {
						return '';
					} else if(VTUtilJS.isNullOrEmpty(r.staffCode)) {
						return VTUtilJS.XSSEncode(r.staffName);
					} else if(VTUtilJS.isNullOrEmpty(r.staffName)) {
						return VTUtilJS.XSSEncode(r.staffCode);
					}
					return VTUtilJS.XSSEncode(r.staffCode+'-'+r.staffName);
				}},
				{field:'shortCode', title:'Khách hàng', width:140, align: 'left', formatter : function(v,r,i) {
					if(VTUtilJS.isNullOrEmpty(r.shortCode) && VTUtilJS.isNullOrEmpty(r.customerName)) {
						return '';
					} else if(VTUtilJS.isNullOrEmpty(r.shortCode)) {
						return VTUtilJS.XSSEncode(r.customerName);
					} else if(VTUtilJS.isNullOrEmpty(r.customerName)) {
						return VTUtilJS.XSSEncode(r.shortCode);
					}
					return VTUtilJS.XSSEncode(r.shortCode + '-' + r.customerName);
				}}
		    ]],
		    columns:[[
				{field:'amount', title:'Tổng tiền', width:100, align: 'right', formatter : function(v,r,i) {
					return formatCurrency(VTUtilJS.XSSEncode(v));
				}},
				{field:'quantity', title:'Tổng sản lượng', width:100, align: 'right', formatter : function(v,r,i) {
					return formatCurrency(VTUtilJS.XSSEncode(v));
				}},
				{field:'discount', title:'CK đơn hàng', width:100, align: 'right', formatter : function(v,r,i) {
					return formatCurrency(VTUtilJS.XSSEncode(v));
				}},
				{field:'discountAmount', title:'Tổng tiền KM', width:100, align: 'right', formatter : function(v,r,i) {
					return formatCurrency(VTUtilJS.XSSEncode(v));
				}},
				{field:'createDate', title:'Ngày tạo', width:130, align: 'center', formatter :function(v,r,i) {
					return VTUtilJS.XSSEncode(v);
				}},
				{field:'deliveryDate', title:'Ngày giao', width:80, align: 'center', formatter : function(v,r,i) {
					return VTUtilJS.XSSEncode(v);
				}},
				{field:'priority', title:'Độ ưu tiên', width:100, align: 'left', formatter : function(v,r,i) {
					return VTUtilJS.XSSEncode(v);
				}},
				{field:'address', title:'Địa chỉ', width:250, align: 'left', formatter : function(v,r,i) {
					return VTUtilJS.XSSEncode(r.address);
				}},
				{field:'deliveryCode', title:'NVGH', width:150, align: 'left', formatter : function(v,r,i) {
					if(VTUtilJS.isNullOrEmpty(r.deliveryCode) && VTUtilJS.isNullOrEmpty(r.deliveryName)) {
						return '';
					} else if(VTUtilJS.isNullOrEmpty(r.deliveryCode)) {
						return VTUtilJS.XSSEncode(r.deliveryName);
					} else if(VTUtilJS.isNullOrEmpty(r.deliveryName)) {
						return VTUtilJS.XSSEncode(r.deliveryCode);
					}
					return VTUtilJS.XSSEncode(r.deliveryCode+'-'+r.deliveryName);
				}},
		    ]],
		    onCheck:function(idx,row){
		    	SPPrintOrder._lstOrder.put(row.id, row);
			},
			onUncheck:function(idx,row){
				SPPrintOrder._lstOrder.remove(row.id, row);
			},
			onCheckAll:function(rows){
				for(var i = 0; i < rows.length; i++) {
					SPPrintOrder._lstOrder.put(rows[i].id, rows[i]);
				}
			},
			onUncheckAll:function(rows){
				for(var i = 0; i < rows.length; i++) {
					SPPrintOrder._lstOrder.remove(rows[i].id, rows[i]);
				}
			},
		    onLoadSuccess :function(data){
		    	$('.datagrid-header-rownumber').html('STT');
		    	SPPrintOrder._lstOrder = new Map();
		    	if (SPPrintOrder._isAllowShowPrice) {
		    		$('#grid').datagrid('showColumn', 'amount');
		    		$('#grid').datagrid('showColumn', 'discount');
		    		$('#grid').datagrid('showColumn', 'discountAmount');
		    	} else {
		    		$('#grid').datagrid('hideColumn', 'amount');
		    		$('#grid').datagrid('hideColumn', 'discount');
		    		$('#grid').datagrid('hideColumn', 'discountAmount');
		    	}
		    }
		});
	}
};

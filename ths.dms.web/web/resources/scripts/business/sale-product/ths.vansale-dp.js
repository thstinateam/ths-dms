/*
 * Copyright YYYY Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/**
 * Lap don ban hang vansale
 * 
 * @author lacnv1
 */
var VansaleDP = {
		_curSaler: "",
		_pageLocked: false,
		_mapProducts: new Map(),
		_lstProducts: [],
		_lstSelected: null,
		_currentShopCode : '',
		_shopCodeFilter: "",
		/**
		 * load trang
		 * 
		 * @author lacnv1
		 */
		loadPage: function(lockPage, errMsg) {	
				VansaleDP.initUnitCbx();
				$('td[field=productCode] .combo-text').live('keyup', function(event) {
					if (event.keyCode == keyCode_F9) {
						VansaleDP.showProductDlg();
					}
				});
				$('td[field=quantity] .qttTextBox').live('keypress', function(event) {
					if (event.keyCode == keyCodes.ENTER) {
						var rs = $("#dgrid").datagrid("getRows");
						var r = rs[VansaleDP._editIdx];
						if (formatQuantity(r.quantity, r.convfact) == formatQuantity($(this).val().trim(), r.convfact)) {
							if (VansaleDP._editIdx < rs.length-1) {
								$("#dgrid").datagrid("selectRow", VansaleDP._editIdx+1);
								var cbs = $("#dgrid").datagrid("getEditors", VansaleDP._editIdx);
								$(cbs[0].target).next().find(".combo-text").focus();
							}
						} else {
							VansaleDP._itv = setInterval(function() {
								if (VansaleDP._idxTmp != null && VansaleDP._idxTmp != undefined) {
									if (VansaleDP._idxTmp < rs.length-1) {
										$("#dgrid").datagrid("selectRow", VansaleDP._idxTmp+1);
										var cbs = $("#dgrid").datagrid("getEditors", VansaleDP._idxTmp+1);
										$(cbs[0].target).next().find(".combo-text").focus();
									}
									VansaleDP._idxTmp = null;
									clearInterval(VansaleDP._itv);
								}
							}, 100);
						}
					}
			});
			var gridData = {
					total: 1,
					rows: [{}],
					footer: [{isFooter: true, weight:0, amount:0}]
				};
			VansaleDP.initGrid(gridData);
		},
		
		initUnitCbx: function(){				
			var params = {};
			$.ajax({
				type : "POST",
				url:'/commons/list-child-shop',
				data :($.param(params, true)),
				dataType : "json",
				success : function(data) {
					listNVBH = data.lstNVBH;
					$('#shopCodeCB').combobox({
						valueField: 'shopCode',
						textField:  'shopName',
						data: data.rows,
						panelWidth: '206',
						formatter: function(row) {
							return '<span style="font-weight:bold">' + VTUtilJS.XSSEncode(row.shopCode) + '</span><br/>' + '<span style="color:#888">' + VTUtilJS.XSSEncode(row.shopName) + '</span>';
						},
						filter: function(q, row){
							q = new String(q).toUpperCase().trim();
							var opts = $(this).combobox('options');
							return ((unicodeToEnglish(row[opts.textField]).indexOf(unicodeToEnglish(q)) == 0) || unicodeToEnglish(row[opts.valueField]).indexOf(unicodeToEnglish(q)) == 0) ;
						},	
				        onSelect: function(rec){
				        	if (rec != null && rec.shopCode != VansaleDP._currentShopCode){
				        		VansaleDP.handleSelectShopVansaleDP(rec.shopCode);
				        		VansaleDP.resetChangeShop(rec);
				        	}
				        },
				        onLoadSuccess: function(){
				        	var arr = $('#shopCodeCB').combobox('getData');
				        	if (arr != null && arr.length > 0){
				        		$('#shopCodeCB').combobox('select', arr[0].shopCode);
				        		VansaleDP._currentShopCode = arr[0].shopCode;
				        	}
				        	
				        }
					});
				}
			});
		},
		
		/**
		 * change shop lay ngay lam viec
		 * @author vuongmq
		 * @since 11/09/2015 
		 */
		handleSelectShopVansaleDP: function(shopCode) {
			var params= {};
			params.shopCode = shopCode;
			Utils.getJSONDataByAjaxNotOverlay(params, '/commons/view-day-lock-select-shop' , function(data) {
				if (data != null) {
					$('#fDate').val(data.lockDate);
				}
			});			
		},

		resetChangeShop: function(rec){
			if (rec != undefined && rec != null){
				VansaleDP._currentShopCode = rec.shopCode;
				VansaleDP._shopCodeFilter = rec.shopCode;
				$("#car").val("").change();
				VansaleDP.getStaffByShopCode();
			}
		},
		getStaffByShopCode: function(shopCodeFilter) {		
			var params = new Object();	
			if(VansaleDP._shopCodeFilter != null){
					params.shopCodeFilter = VansaleDP._shopCodeFilter;
			}					
			VansaleDP.loadStaffCbxs(params);
		},
		loadStaffCbxs: function(params){
			var kData = $.param(params, true);
			 $.ajax({
				type : "POST",
				url : '/sale-product/vansale/search-staff',
				data : (kData),
				dataType : "json",
				success : function(data) {
					$("#car").combobox({
						valueField: 'id',
						textField: 'carNumber',
						data: data.lstCar,
						panelWidth: 206,
						formatter: function(row) {
							return '<span style="font-weight:bold">' + VTUtilJS.XSSEncode(row.carNumber) + '</span>';
						},
						filter: function(q, row){
							q = new String(q).toUpperCase().trim();
							var opts = $(this).combobox('options');
							return ((unicodeToEnglish(row[opts.textField]).indexOf(unicodeToEnglish(q)) == 0) || unicodeToEnglish(row[opts.valueField]).indexOf(unicodeToEnglish(q)) == 0) ;
						},
						onSelect: function(rec){
							$("#car").val(rec.id);
				        }
					});
					$("#saler").combobox({
						valueField: 'staffCode',
						textField: 'staffName',
						data: data.lstStaff,
						panelWidth: 206,
						formatter: function(row) {
							return '<span style="font-weight:bold">' + VTUtilJS.XSSEncode(row.staffCode) + '</span><br/>' + '<span style="color:#888">' + VTUtilJS.XSSEncode(row.staffName) + '</span>';
						},
						filter: function(q, row){
							q = new String(q).toUpperCase().trim();
							var opts = $(this).combobox('options');
							//return row['searchText'].indexOf(unicodeToEnglish(q))>=0;
							return ((unicodeToEnglish(row[opts.textField]).indexOf(unicodeToEnglish(q)) == 0) || unicodeToEnglish(row[opts.valueField]).indexOf(unicodeToEnglish(q)) == 0) ;
						},
						onHidePanel: function() {
							$(".ErrorMsgStyle").hide();
							var v = $("#saler").combobox("getValue").trim();
							if (v !== VansaleDP._curSaler) {
								if (VansaleDP._curSaler) {
									$.messager.confirm("Xác nhận", "Chọn lại NVBH sẽ loại bỏ một số mặt hàng trong danh sách NVBH không được bán, bạn có muốn tiếp tục?", function(y) {
										if (y) {
											$("#btnSave").removeAttr("disabled");
											$("#btnSave").attr("onclick", "VansaleDP.saveOrder();");
											$("#btnSave").removeClass("BtnGeneralDStyle");
											$("#DPNumber").val("");
											VansaleDP._curSaler = v;
											VansaleDP.getProductBySaler(v);
										} else {
											$("#saler").combobox("setValue", VansaleDP._curSaler);
										}
									});
								} else {
									VansaleDP._curSaler = v;
									VansaleDP.getProductBySaler(v);
								}
							}
						}
					});
					$("#saler + .combo .combo-text").focus();
				},
				error:function(XMLHttpRequest, textStatus, errorThrown) {
					console.log("Error");
				}
			});
		},
		/**
		 * Khoi tao datagrid
		 * 
		 * @author lacnv1
		 */
		initGrid: function(gridData) {
			$("#dgrid").datagrid({
				url: "",
				data: gridData,
				width: $("#gridContainer").width() - 10,
				height: "auto",
				scrollbarSize: 0,
				singleSelect: true,
				checkOnSelect: false,
				selectOnCheck: false,
				rownumbers: true,
				fitColumns: true,
				showFooter: true,
				columns: [[
				    {field: "productCode", title:"Mã sản phẩm<br/>(F9)", width: 100, sortable:false, resizable:false, align: "left", formatter: function(v, r, i) {
				    	if (r.isFooter) {
				    		return "";
				    	}
				    	return VTUtilJS.XSSEncode(v);
				    }, editor: {
				    	type: "combobox",
				    	options: {
				    		valueField: 'productCode',
							textField: 'productCode',
							formatter: function(r) {
								var productName = r['productName'];
								/*if(productName.length > 25) {
									productName = productName.substring(0, 25) + "...";
								}*/
								return "<div style='white-space: nowrap;overflow:hidden;text-overflow: ellipsis;'>" + Utils.XSSEncode(productName) + "</div>";
							},
							onHidePanel: function() {
								$(".ErrorMsgStyle").hide();
								VansaleDP.chooseProduct();
							},
							filter: function(q, row){
								q = new String(q).toUpperCase().trim();
								return row['productCode'].indexOf(q)>=0;
							}
				    	}
				    }},
					{field: "productName", title:"Tên sản phẩm", width: 200, sortable:false, resizable:false, align: "left", formatter: function(v, r, i) {
				    	if (r.isFooter) {
				    		return "";
				    	}
				    	return VTUtilJS.XSSEncode(v);
				    }},
					{field: "warehouseId", title:"Kho", width: 150, sortable:false, resizable:false, align: "left", formatter: function(v, r, i) {
				    	if (r.isFooter) {
				    		return "";
				    	}
				    	return VTUtilJS.XSSEncode(r.warehouseName);
				    }, editor: {
				    	type: "combobox",
				    	options: {
				    		valueField: 'warehouseId',
							textField: 'warehouseName',
							editable: false,
							formatter: function(r) {
								return Utils.XSSEncode(r['warehouseName']);
							},
							onHidePanel: function() {
								$(".ErrorMsgStyle").hide();
								VansaleDP.chooseWarehouse();
							}
				    	}
				    }},
					{field: "availableQuantity", title:"Tồn kho<br/>đáp ứng", width: 90, fixed:true, sortable:false, resizable:false, align: "right", formatter: function(v, r, i) {
				    	if (r.isFooter) {
				    		return "<b>Tổng</b>";
				    	}
				    	return formatQuantity(v, r.convfact);
				    }},
					{field: "quantity", title:"Số lượng", width: 90, fixed:true, sortable:false, resizable:false, align: "right", formatter: function(v, r, i) {
						if (r.isFooter) {
				    		return "<b>" + formatFloatValue(r.weight, 2) + " kg</b>";
				    	}
						if (VansaleDP._pageLocked) {
							return "";
						}
						var inp = '<input type="text" id="qtt'+i+'" class="InputTextStyle qttTextBox" style="margin:0;width:75px;text-align:right;" \
								maxlength="10" value="{0}" onchange="VansaleDP.changeQuantity('+i+');" onkeypress="return VansaleDP.quantityTextBoxKeypress(event);" />';
						if (v != undefined && v != null) {
							inp = inp.replace("{0}", formatQuantity(v, r.convfact));
						} else {
							inp = inp.replace("{0}", "");
						}
						return inp;
				    }},
					{field: "price", title:"Giá bán lẻ", width: 100, sortable:false, resizable:false, align: "right", formatter: function(v, r, i) {
						if (r.isFooter) {
				    		return "";
				    	}
						return formatCurrency(v);
				    }},
					{field: "amount", title:"Thành tiền", width: 120, sortable:false, resizable:false, align: "right", formatter: function(v, r, i) {
						if (r.isFooter) {
				    		return "<b>" + formatCurrency(v) + " VNĐ</b>";
				    	}
						return formatCurrency(v);
				    }},
					{field: "del", title:"", width: 35, fixed:true, sortable:false, resizable:false, align: "center", formatter: function(v, r, i) {
						if (r.isFooter) {
				    		return "";
				    	}
						return '<a id="del'+i+'" href="javascript:void(0);" class="delA" onclick="VansaleDP.removeProduct('+i+');"><img width="15" height="15" alt="del" src="/resources/images/icon_delete.png" title="Xóa" /></a>';
				    }}
				]],
				onLoadSuccess: function(data) {
					$('.datagrid-header-rownumber').html('STT');
					GridUtil.updateDefaultRowNumWidth("#gridContainer", "dgrid");
					if (VansaleDP._pageLocked) {
						$(".delA").removeAttr("onclick");
					}
				},
				onSelect: function(i) {
					if (VansaleDP._pageLocked) {
						return;
					}
					if (VansaleDP._editIdx != i) {
						if (VansaleDP.endEditing()) {
							$('#dgrid').datagrid('beginEdit', i);
							VansaleDP._editIdx = i;
						}
						VansaleDP.loadOptionsForRow();
					}
				}
			});
		},
		
		/**
		 * Lay ds sp theo NVBH
		 * 
		 * @author lacnv1
		 */
		getProductBySaler: function(staffCode) {
			$("#loadingProduct").show();
			VansaleDP._editIdx = undefined;
			var gridData = {
					total: 1,
					rows: [{}],
					footer: [{isFooter: true, weight:0, amount:0}]
				};
			$("#dgrid").datagrid("loadData", gridData);
			var params = {staffCode:staffCode, checkStock: 1};
			Utils.getJSONDataByAjaxNotOverlay(params, "/sale-product/vansale/search-product", function(data) {
				if (!data.error) {
					var r = null;
					var i, sz;
					VansaleDP._lstProducts = [];
					VansaleDP._mapProducts = new Map();
					for (i = 0, sz = data.rows.length; i < sz; i++) {
						r = data.rows[i];
						if (VansaleDP._lstProducts.indexOf(r.productCode) < 0) {
							VansaleDP._lstProducts.push(r.productCode);
							VansaleDP._mapProducts.put(r.productCode, [r]);
						} else {
							VansaleDP._mapProducts.get(r.productCode).push(r);
						}
					}
					var rs = $("#dgrid").datagrid("getRows");
					for (var j = 0, szj = rs.length; j < szj; j++) {
						if (rs[j].productCode && VansaleDP._lstProducts.indexOf(rs[j].productCode) < 0) {
							$("#dgrid").datagrid("deleteRow", j);
							j--;
							szj--;
							rs = $("#dgrid").datagrid("getRows");
						}
					}
					VansaleDP.endEditing();
					$("#dgrid").datagrid("acceptChanges");
					VansaleDP.updateIndex();
					VansaleDP.refreshFooter();
					$("#loadingProduct").hide();
				}
			});
		},
		
		/**
		 * Lay ds sp ban cho combobox
		 * 
		 * @author lacnv1
		 */
		getProductData: function() {
			var rows = VansaleDP._lstProducts;
			lst = [];
			for (i = 0, sz = rows.length; i < sz; i++) {
				var p = VansaleDP._mapProducts.get(rows[i]);
				lst.push({productCode: rows[i], productName: rows[i] + " - " + (p[0].productName)});
			}
			return lst;
		},
		
		/**
		 * Ket thuc chinh sua grid
		 * 
		 * @author lacnv1
		 */
		endEditing: function() {
			if (VansaleDP._editIdx == undefined) {
				return true;
			}
			if ($('#dgrid').datagrid('validateRow', VansaleDP._editIdx)) {
				$('#dgrid').datagrid('endEdit', VansaleDP._editIdx);
				VansaleDP._editIdx = undefined;
				return true;
			} else {
				return false;
			}
		},
		
		/**
		 * Chon sp trong combobox
		 * 
		 * @author lacnv1
		 */
		chooseProduct: function() {
			var rs = $("#dgrid").datagrid("getRows");
			if (rs.length > VansaleDP._editIdx) {
				var r = rs[VansaleDP._editIdx];
				var cbs = $("#dgrid").datagrid("getEditors", VansaleDP._editIdx);
				var v = $(cbs[0].target).combobox("getValue").split(" - ")[0];
				if (v !== r.productCode) {
					var arrp = VansaleDP._mapProducts.get(v);
					if (arrp && arrp.length > 0) {
						var idx = 0;
						var b = true;
						for (var sz = arrp.length; idx < sz;) {
							b = true;
							for (var i = 0, szi = rs.length; i < szi; i++) {
								if (i !== VansaleDP._editIdx && rs[i].productCode === v
										&& rs[i].warehouseId === arrp[idx].warehouseId) {
									idx++;
									b = false;
									break;
								}
							}
							if (b) {
								break;
							}
						}
						if (idx >= arrp.length) {
							$("#errMsg").html("Bạn đã chọn tất cả các kho của sản phẩm <b><i>"+v+"</i></b>").show();
							$(cbs[0].target).combobox("setValue", "");
							return;
						}
						r.productId = arrp[idx].productId;
						r.productCode = v;
						r.productName = arrp[idx].productName;
						r.warehouseName = arrp[idx].warehouseName;
						r.warehouseId = arrp[idx].warehouseId;
						$(cbs[1].target).combobox("loadData", [].concat(arrp));
						$(cbs[1].target).combobox("setValue", arrp[idx].warehouseId);
						r.availableQuantity = arrp[idx].availableQuantity;
						r.price = arrp[idx].price;
						r.convfact = arrp[idx].convfact;
						r.grossWeight = arrp[idx].grossWeight;
						r.quantity = null;
						r.amount = 0;
						
						$('#dgrid').datagrid('endEdit', VansaleDP._editIdx);
						$('#dgrid').datagrid('beginEdit', VansaleDP._editIdx);
						VansaleDP.loadOptionsForRow();
						$("#qtt"+VansaleDP._editIdx).focus();
						if (rs.length > 0 && (rs.length - 1) == VansaleDP._editIdx && rs[rs.length - 1].productCode) {
							$('#dgrid').datagrid("appendRow", {});
						}
					} else {
						$(cbs[0].target).combobox("setValue", r.productCode);
					}
				}
			}
		},
		
		/**
		 * Chon sp tiep theo khi nhap so luong vuot qua ton kho dap ung
		 * 
		 * @author lacnv1
		 */
		chooseProductEx: function(qtt) {
			var rs = $("#dgrid").datagrid("getRows");
			if (rs.length > VansaleDP._editIdx) {
				var r = rs[VansaleDP._editIdx];
				var cbs = $("#dgrid").datagrid("getEditors", VansaleDP._editIdx);
				var v = $(cbs[0].target).combobox("getValue");
				if (v !== r.productCode) {
					var arrp = VansaleDP._mapProducts.get(v);
					if (arrp && arrp.length > 0) {
						var idx = 0;
						var b = true;
						for (var sz = arrp.length; idx < sz;) {
							b = true;
							for (var i = 0, szi = rs.length; i < szi; i++) {
								if (i !== VansaleDP._editIdx && rs[i].productCode === v
										&& rs[i].warehouseId === arrp[idx].warehouseId) {
									idx++;
									b = false;
									break;
								}
							}
							if (b) {
								break;
							}
						}
						if (idx >= arrp.length) {
							$("#errMsg").html("Sản phẩm <b><i>"+v+"</i></b> không đủ tồn kho đáp ứng").show();
							$("#dgrid").datagrid("deleteRow", VansaleDP._editIdx);
							VansaleDP.endEditing();
							VansaleDP.updateIndex();
							VansaleDP.refreshFooter();
							return;
						}
						r.productId = arrp[idx].productId;
						r.productCode = v;
						r.productName = arrp[idx].productName;
						r.warehouseName = arrp[idx].warehouseName;
						r.warehouseId = arrp[idx].warehouseId;
						$(cbs[1].target).combobox("loadData", [].concat(arrp));
						$(cbs[1].target).combobox("setValue", arrp[idx].warehouseId);
						r.availableQuantity = arrp[idx].availableQuantity;
						r.price = arrp[idx].price;
						r.convfact = arrp[idx].convfact;
						r.grossWeight = arrp[idx].grossWeight;
						if (qtt > r.availableQuantity) { // so luong nhap vao van con lon hon ton kho dap ung
							r.quantity = r.availableQuantity;
							r.amount = r.quantity * r.price;
							qtt = qtt - r.availableQuantity;
							
							var k = VansaleDP._editIdx;
							VansaleDP.endEditing();
							$("#dgrid").datagrid("insertRow", {index:k + 1, row:{}});
							if (VansaleDP.endEditing()) {
								$('#dgrid').datagrid('beginEdit', k+1);
								VansaleDP._editIdx = k+1;
							}
							VansaleDP.loadOptionsForRow();
							var cbs = $("#dgrid").datagrid("getEditors", VansaleDP._editIdx);
							$(cbs[0].target).combobox("setValue", r.productCode);
							VansaleDP.chooseProductEx(qtt); // chon kho tiep theo
						} else if (qtt > 0) {
							r.quantity = qtt;
							r.amount = r.quantity * r.price;
						} else {
							r.quantity = null;
							r.amount = 0;
						}
						
						$('#dgrid').datagrid('endEdit', VansaleDP._editIdx);
						$('#dgrid').datagrid('beginEdit', VansaleDP._editIdx);
						VansaleDP.loadOptionsForRow();
						$("#qtt"+VansaleDP._editIdx).focus();
						if (rs.length > 0 && (rs.length - 1) == VansaleDP._editIdx && rs[rs.length - 1].productCode) {
							$('#dgrid').datagrid("appendRow", {});
						}
					} else {
						$(cbs[0].target).combobox("setValue", r.productCode);
					}
				}
			}
		},
		
		/**
		 * Chon sp trong combobox
		 * 
		 * @author lacnv1
		 */
		chooseWarehouse: function() {
			var rs = $("#dgrid").datagrid("getRows");
			if (rs.length > VansaleDP._editIdx) {
				var r = rs[VansaleDP._editIdx];
				var cbs = $("#dgrid").datagrid("getEditors", VansaleDP._editIdx);
				var pCode = $(cbs[0].target).combobox("getValue");
				var v = Number($(cbs[1].target).combobox("getValue"));
				if (v !== r.warehouseId) {
					var rs = $("#dgrid").datagrid("getRows");
					for (var i = 0, sz = rs.length - 1; i < sz; i++) {
						if (i == VansaleDP._editIdx) {
							continue;
						}
						if (pCode == rs[i].productCode && v == rs[i].warehouseId) {
							$(cbs[1].target).combobox("setValue", r.warehouseId);
							$("#errMsg").html("Sản phẩm <b><i>" + VTUtilJS.XSSEncode(pCode) + "</i></b> đã chọn kho <b><i>" + VTUtilJS.XSSEncode(rs[i].warehouseName) + "</i></b>").show();
							return;
						}
					}
					var arrp = VansaleDP._mapProducts.get(pCode);
					if (arrp && arrp.length > 0) {
						var i = 0;
						for (var sz = arrp.length; i < sz; i++) {
							if (arrp[i].warehouseId === v) {
								break;
							}
						}
						r.warehouseName = arrp[i].warehouseName;
						r.warehouseId = arrp[i].warehouseId;
						r.availableQuantity = arrp[i].availableQuantity;
						$('#dgrid').datagrid('endEdit', VansaleDP._editIdx);
						$('#dgrid').datagrid('beginEdit', VansaleDP._editIdx);
						VansaleDP.loadOptionsForRow();
						$("#qtt"+VansaleDP._editIdx).focus();
					}
				}
			}
		},
		
		/**
		 * Lay ds sp cho combobox cua dong dang sua
		 * 
		 * @author lacnv1
		 */
		loadOptionsForRow: function() {
			var cbs = $("#dgrid").datagrid("getEditors", VansaleDP._editIdx);
			$(cbs[0].target).combobox("loadData", VansaleDP.getProductData());
			var r = $("#dgrid").datagrid("getRows")[VansaleDP._editIdx];
			if (r && r.productCode) {
				var arrp = VansaleDP._mapProducts.get(r.productCode);
				$(cbs[1].target).combobox("loadData", [].concat(arrp));
			}
		},
		
		/**
		 * Thay doi so luong ban sp
		 * 
		 * @author lacnv1
		 */
		changeQuantity: function(i) {
			VansaleDP._idxTmp = null; // dung de bat enter
			$(".ErrorMsgStyle").hide();
			var rs = $("#dgrid").datagrid("getRows");
			if (rs && rs.length > i) {
				var v1 = getQuantity($("#qtt"+i).val().trim(), rs[i].convfact);
				if (isNaN(v1)) {
					rs[i].quantity = null;
				} else {
					rs[i].quantity = v1;
				}
				var qtt = 0;
				if (rs[i].quantity > rs[i].availableQuantity) {
					qtt = rs[i].quantity - rs[i].availableQuantity;
					rs[i].quantity = rs[i].availableQuantity;
				}
				if (rs[i].productCode && rs[i].price) {
					rs[i].amount = Number(rs[i].quantity) * rs[i].price;
				}
				if (qtt > 0) {
					VansaleDP.partWarehouse(i, qtt);
				} else {
					VansaleDP.endEditing();
				}
			}
			VansaleDP.refreshFooter();
			VansaleDP._idxTmp = i; // dung de bat enter
		},
		
		/**
		 * tach kho theo so luong nhap
		 * 
		 * @author lacnv1
		 */
		partWarehouse: function(idx, quantity) {
			var rs = $("#dgrid").datagrid("getRows");
			if (idx >= rs.length) {
				return;
			}
			var rc = rs[idx];
			var arrp = VansaleDP._mapProducts.get(rc.productCode);
			var r = null;
			var qtt = -1;
			var b;
			var ic = -1;
			var i = 0;
			for (var sz = arrp.length; i < sz; i++) { // tim kho con co the them so luong
				b = -1;
				qtt = -1;
				for (var j = 0, szj = rs.length; j < szj; j++) {
					r = rs[j];
					if (j == idx && r.warehouseId === arrp[i].warehouseId) {
						b = 0;
						break;
					}
					if (r.productCode === rc.productCode && r.warehouseId === arrp[i].warehouseId) {
						if (r.quantity == null || r.quantity < r.availableQuantity) {
							b = 1;
							qtt = r.availableQuantity - (r.quantity == null ? 0 : r.quantity);
							ic = j;
							break;
						} else {
							b = 0;
						}
					}
				}
				if (b) {
					if (b === -1 && i < arrp.length) {
						qtt = arrp[i].availableQuantity;
					}
					break;
				}
			}
			if (qtt > 0) { // cac kho sp con co the them so luong
				var t = quantity;
				if (qtt < quantity) {
					quantity = qtt;
				}
				qtt = t - quantity;
				if (ic > -1) { // kho duoc chon da co dong tren ds
					if (VansaleDP.endEditing()) {
						$('#dgrid').datagrid('beginEdit', ic);
						VansaleDP._editIdx = ic;
					}
					rs[ic].quantity = Number(rs[ic].quantity) + quantity;
					rs[ic].amount = rs[ic].quantity * rs[ic].price;
					VansaleDP.endEditing();
					VansaleDP.updateIndex();
					VansaleDP.refreshFooter();
				} else {
					idx = idx + 1;
					$("#dgrid").datagrid("insertRow", {index:idx, row:{}});
					if (VansaleDP.endEditing()) {
						$('#dgrid').datagrid('beginEdit', idx);
						VansaleDP._editIdx = idx;
					}
					r = $("#dgrid").datagrid("getRows")[VansaleDP._editIdx];
					r.productId = arrp[i].productId;
					r.productCode = arrp[i].productCode;
					r.productName = arrp[i].productName;
					r.warehouseName = arrp[i].warehouseName;
					r.warehouseId = arrp[i].warehouseId;
					var cbs = $("#dgrid").datagrid("getEditors", VansaleDP._editIdx);
					$(cbs[1].target).combobox("loadData", [].concat(arrp));
					$(cbs[1].target).combobox("setValue", r.warehouseId);
					r.availableQuantity = arrp[i].availableQuantity;
					r.price = arrp[i].price;
					r.convfact = arrp[i].convfact;
					r.grossWeight = arrp[i].grossWeight;
					r.quantity = quantity;
					r.amount = r.quantity * r.price;
					VansaleDP.loadOptionsForRow();
					$(cbs[0].target).combobox("setValue", r.productCode);
					
					VansaleDP.endEditing();
					VansaleDP.updateIndex();
					VansaleDP.refreshFooter();
				}
				if (qtt > 0) {
					VansaleDP.partWarehouse(idx, qtt);
				}
			} else {
				$("#errMsg").html("Sản phẩm <b><i>" + VTUtilJS.XSSEncode(rc.productCode) + "</i></b> không đủ tồn kho đáp ứng").show();
				VansaleDP.endEditing();
				VansaleDP.updateIndex();
				VansaleDP.refreshFooter();
			}
		},
		
		/**
		 * Load lai footer cua grid
		 * 
		 * @author lacnv1
		 */
		refreshFooter: function() {
			var rs = $("#dgrid").datagrid("getRows");
			var r = null;
			var weight = 0;
			var amount = 0;
			for (var i = 0, sz = rs.length; i < sz; i++) {
				r = rs[i];
				if (r.productCode && r.quantity) {
					weight = weight + Number(r.quantity) * r.grossWeight;
					amount = amount + r.amount;
				}
			}
			var ft = $("#dgrid").datagrid("getData").footer[0];
			ft.weight = weight;
			ft.amount = amount;
			$("#dgrid").datagrid("reloadFooter");
		},
		
		/**
		 * Cap nhat lai id control theo index cua cac dong
		 * 
		 * @author lacnv1
		 */
		updateIndex: function() {
			var i = 0;
			$(".qttTextBox").each(function() {
				$(this).attr("id", "qtt"+i);
				$(this).attr("onchange", "VansaleDP.changeQuantity("+i+");");
				i++;
			});
			i = 0;
			$(".delA").each(function() {
				$(this).attr("id", "del"+i);
				$(this).attr("onclick", "VansaleDP.removeProduct("+i+");");
				i++;
			});
		},
		
		/**
		 * Xoa sp
		 * 
		 * @author lacnv1
		 */
		removeProduct: function(i) {
			$(".ErrorMsgStyle").hide();
			$.messager.confirm("Xác nhận", "Bạn có muốn xóa sản phẩm khỏi danh sách bán?", function(y) {
				if (y) {
					var rs = $("#dgrid").datagrid("getRows");
					if (i < rs.length - 1) {
						$("#dgrid").datagrid("deleteRow", i);
						VansaleDP.endEditing();
						VansaleDP.updateIndex();
						VansaleDP.refreshFooter();
					}
				}
			});
		},
		
		/**
		 * Luu don hang
		 * 
		 * @author lacnv1
		 */
		saveOrder: function() {
			$(".ErrorMsgStyle").hide();
			
			var staffCode = $("#saler").combobox("getValue").trim();
			if (staffCode.length == 0) {
				$("#errMsg").html("Bạn chưa chọn giá trị cho trường Mã NV").show();
				$("#saler + .combo .combo-text").focus();
				return;
			}
			
			var carId = $("#car").val().trim();
			if (carId.length == 0) {
				$("#errMsg").html("Bạn chưa chọn giá trị cho trường Xe").show();
				$("#car").focus();
				return;
			}
			
			var rs = $("#dgrid").datagrid("getRows");
			if (rs.length == 1 && !rs[0].productCode) {
				$("#errMsg").html("Bạn chưa chọn mặt hàng nào").show();
				return;
			}
			var lst = [];
			var j;
			for (var i = 0, sz = rs.length - 1; i < sz; i++) {
				if (!rs[i].productCode) {
					continue;
				}
				if (!rs[i].warehouseId) {
					$("#errMsg").html("Bạn chưa chọn kho cho sản phẩm").show();
					$("#dgrid").datagrid("selectRow", i);
					return;
				}
				for (j = 0; j < i; j++) {
					if (!rs[j].productCode) {
						continue;
					}
					if (rs[i].productCode == rs[j].productCode && rs[i].warehouseId == rs[j].warehouseId) {
						$("#errMsg").html("Sản phẩm <b><i>" + VTUtilJS.XSSEncode(rs[i].productCode) + "</i></b> với kho <b><i>" + VTUtilJS.XSSEncode(rs[i].warehouseName) + "</i></b> có nhiều hơn 1 dòng").show();
						$("#dgrid").datagrid("selectRow", i);
						return;
					}
				}
				if (!rs[i].quantity) {
					$("#errMsg").html("Bạn chưa nhập số lượng cho sản phẩm").show();
					$("#dgrid").datagrid("selectRow", i);
					$("#qtt"+i).focus();
					return;
				}
				if (rs[i].quantity <= 0) {
					$("#errMsg").html("Số lượng phải lớn hơn 0").show();
					$("#dgrid").datagrid("selectRow", i);
					$("#qtt"+i).focus();
					return;
				}
				if (rs[i].quantity > rs[i].availableQuantity) {
					$("#errMsg").html("Số lượng sản phẩm không được vượt quá tồn kho đáp ứng").show();
					$("#dgrid").datagrid("selectRow", i);
					$("#qtt"+i).focus();
					return;
				}
				lst.push({productId: rs[i].productId, warehouseId: rs[i].warehouseId, quantity: rs[i].quantity, price: rs[i].price});
			}
			
			var dataModel = {staffCode:staffCode, carId: Number(carId), lstProducts:lst};
			JSONUtil.saveData2(dataModel, "/sale-product/vansale/save-order", "Bạn có muốn lưu thông tin này?",
					"errMsg", function(data) {
				$("#btnSave").attr("disabled", "disabled");
				$("#btnSave").removeAttr("onclick");
				$("#btnSave").addClass("BtnGeneralDStyle");
				$("#DPNumber").val(data.stockTransCode);
			});
		},
		
		/**
		 * thay doi so luong (dialog)
		 * 
		 * @author lacnv1
		 */
		changeQuantityDlg: function(idx) {
			var rs = $("#productPopup #gridDlg").datagrid("getRows");
			var v = $("#qttDlg"+idx).val().trim();
			if (v.length == 0) {
				rs[idx].quantity = null;
				VansaleDP._lstSelected.remove(rs[idx].productId + "_" + rs[idx].warehouseId);
			} else {
				var v1 = getQuantity(v, rs[idx].convfact);
				if (isNaN(v1)) {
					rs[idx].quantity = null;
				} else {
					rs[idx].quantity = v1;
				}
				$("#qttDlg"+idx).val(formatQuantity(rs[idx].quantity, rs[idx].convfact));
				VansaleDP._lstSelected.put(rs[idx].productId + "_" + rs[idx].warehouseId, rs[idx]);
			}
		},
		
		/**
		 * chon sp (tren dialog)
		 * 
		 * @author lacnv1
		 */
		chooseProductOnDlg: function() {
			$(".ErrorMsgStyle").hide();
			var rs = VansaleDP._lstSelected.valArray;
			var r = null;
			var lstTmp = [];
			for (var i = 0, sz = rs.length; i < sz; i++) {
				r = rs[i];
				if (r.quantity) {
					if (r.quantity <= 0) {
						$("#errMsgDlg").html("Số lượng sản phẩm <b><i>" + VTUtilJS.XSSEncode(r.productCode) + "</i></b> phải lớn hơn 0").show();
						$("#productPopup #gridDlg").datagrid("unselectAll");
						//$("#productPopup #qttDlg"+i).focus();
						return;
					}
					if (r.quantity > r.availableQuantity) {
						$("#errMsgDlg").html("Số lượng sản phẩm <b><i>" + VTUtilJS.XSSEncode(r.productCode) + "</i></b> không được vượt quá tồn kho đáp ứng").show();
						$("#productPopup #gridDlg").datagrid("unselectAll");
						return;
					}
					lstTmp.push(r);
				}
			}
			if (lstTmp.length == 0) {
				$("#errMsgDlg").html("Không có sản phẩm nào được chọn").show();
				return;
			}
			
			var rows = $("#dgrid").datagrid("getRows");
			var j, szj;
			for (var i = 0, sz = rows.length; i < sz; i++) {
				for (j = 0, szj = lstTmp.length; j < szj; j++) {
					if (rows[i].productCode === lstTmp[j].productCode && rows[i].warehouseId === lstTmp[j].warehouseId) {
						$("#dgrid").datagrid("selectRow", i);
						rows[i].quantity = lstTmp[j].quantity;
						rows[i].amount = rows[i].quantity * rows[i].price;
						lstTmp.splice(j, 1);
						szj = lstTmp.length;
						break;
					}
				}
			}
			var idx = rows.length;
			if (!rows[rows.length - 1].productCode) {
				idx = rows.length - 1;
			}
			for (j = 0, szj = lstTmp.length; j < szj; j++) {
				lstTmp[j].amount = lstTmp[j].quantity * lstTmp[j].price;
				$("#dgrid").datagrid("insertRow", {index: idx, row: lstTmp[j]});
				idx++;
			}
			
			$("#productPopup").dialog("close");
			$("#dgrid").datagrid("acceptChanges");
			VansaleDP._editIdx = undefined;
			VansaleDP.refreshFooter();
			VansaleDP.updateIndex();
		},
		
		/**
		 * show dialog chon sp
		 * 
		 * @author lacnv1
		 */
		showProductDlg: function() {
			$(".ErrorMsgStyle").hide();
			if ($("#saler").combobox("getValue").trim().length == 0) {
				return;
			}
			var html = '<div id="productPopupDiv" style="display:none;">\
				<div id="productPopup">\
				<div class="PopupContentMid">\
					<div class="GeneralForm Search1Form">\
						<label class="LabelStyle" style="width:100px;">Mã sản phẩm</label>\
						<input id="codeDlg" class="InputTextStyle" style="width:170px;" maxlength="40" />\
						<label class="LabelStyle" style="width:100px;">Tên sản phẩm</label>\
						<input id="nameDlg" class="InputTextStyle" style="width:300px;" maxlength="100" />\
						<div class="Clear"></div>\
						<div class="BtnCenterSection">\
						    <button class="BtnGeneralStyle" id="btnSearchDlg">Tìm kiếm</button>\
						</div>\
						\
						<div class="GridSection" id="gridDlgDiv">\
							<div id="gridDlg"></div>\
						</div>\
						\
						<p id="errMsgDlg" class="ErrorMsgStyle" style="padding-left:5px;display:none;"></p>\
						<div class="Clear"></div>\
						<div class="BtnCenterSection">\
						    <button class="BtnGeneralStyle" id="btnChooseDlg">Chọn</button>\
							<button class="BtnGeneralStyle" id="btnCloseDlg" onclick="javascript:$(\'#productPopup\').dialog(\'close\');">Đóng</button>\
						</div>\
					</div>\
				</div>\
				</div>\
			</div>';
			$("body").append(html);
			
			var lstTmp = null;
			$("#productPopup").dialog({
				title: "Chọn Sản phẩm",
				width: 750,
				heigth: "auto",
				modal: true, close: false, cache: false,
				onOpen: function() {
					$("#productPopup").addClass("easyui-dialog");
					$("#productPopup #codeDlg").focus();
					setTimeout(function() {
		        		var cbs = $("#dgrid").datagrid("getEditors", VansaleDP._editIdx);
			        	$(cbs[0].target).combobox("hidePanel");
		        	}, 500);
					
					VansaleDP._lstSelected = new Map();
					var rs = $("#dgrid").datagrid("getRows");
					for (var i = 0, sz = rs.length; i < sz; i++) {
						if (rs[i].productCode && rs[i].warehouseId && rs[i].quantity) {
							VansaleDP._lstSelected.put(rs[i].productId + "_" + rs[i].warehouseId, rs[i]);
						}
					}
					
					$("#productPopup #gridDlg").datagrid({
					    url : '/sale-product/vansale/search-product?isPaging=true&staffCode='+$("#saler").combobox("getValue").trim(),
						//rownumbers: true,
						width: $("#productPopup #gridDlgDiv").width(),
						height: "auto",
						pagination: true,
						fitColumns: true,
						scrollbarSize: 0,
						singleSelect: true,
						columns: [[
						    GridUtil.getRowNumField("productPopup #gridDlg"),
							GridUtil.getNormalField("productCode", "Mã sản phẩm", 100),
					        GridUtil.getNormalField("productName", "Tên sản phẩm", 150),
					        GridUtil.getNormalField("warehouseName", "Kho", 120),
					        {field:'availableQuantity', title:'Tồn kho đáp ứng', align:'right', width: 60, sortable:false, resizable:false, formatter: function(v, r, i) {
					        	if (v == undefined || v == null) {
					        		return "";
					        	}
					        	return formatQuantity(v, r.convfact);
					        }},
					        GridUtil.getNormalField("price", "Giá", 80, "right"),
					        {field:'quantity', title:'Số lượng', align:'center', width:90, fixed:true, sortable:false, resizable:false, formatter:function(v, r, i) {
					        	if (r.availableQuantity && r.availableQuantity > 0) {
					        		return '<input type="text" id="qttDlg'+i+'" class="InputTextStyle qttDlgText" style="margin:0;width:75px;text-align:right;" \
											maxlength="10" onchange="VansaleDP.changeQuantityDlg('+i+')" onkeyup="VansaleDP.nextAndPrevTextField(event, this, \'qttDlgText\');" onkeypress="return VansaleDP.quantityTextBoxKeypress(event);" />';
					        	} else {
					        		return '<input type="text" id="qttDlg'+i+'" class="InputTextStyle" style="margin:0;width:75px;text-align:right;" disabled="disabled" maxlength="10" />';
					        	}
					        }}
						]],
						onLoadSuccess: function(data) {
							$(".ErrorMsgStyle").hide();
							GridUtil.updateRowNumWidth("#productPopup", "gridDlg");
							var rows = data.rows;
							var r = null;
							var v = null;
							for (var i = 0, sz = rows.length; i < sz; i++) {
								r = rows[i];
								v = VansaleDP._lstSelected.get(r.productId + "_" + r.warehouseId);
								if (v) {
									r.quantity = v.quantity;
									$("#qttDlg"+i).val(formatQuantity(r.quantity, r.convfact));
								}
							}
							
							setTimeout(function(){
								var hDlg=parseInt($("#productPopup").parent().height());
								var hW=parseInt($(window).height());
								var d=hW-hDlg;
								d=d/2+document.documentElement.scrollTop;
								if (d < 0) { d = 0; }
								$("#productPopup").parent().css('top',d);
				    		},1000);
						},
						rowStyler: function(i, r) {
							if (!r.availableQuantity || r.availableQuantity <= 0) {
								return "color:#f00;";
							}
							return "";
						}
					});
					
					$("#productPopup #btnSearchDlg").click(function() {
						var p = {
								code: $("#productPopup #codeDlg").val().trim(),
								name: $("#productPopup #nameDlg").val().trim()
						};
						$(".ErrorMsgStyle").hide();
						$("#productPopup #gridDlg").datagrid("load", p);
					});
					$("#productPopup #codeDlg, #productPopup #nameDlg").keyup(function(event) {
						if (event.keyCode == keyCodes.ENTER) {
							$("#productPopup #btnSearchDlg").click();
						}
					});
					$("#productPopup #btnChooseDlg").click(function() {
						VansaleDP.chooseProductOnDlg();
					});
				},
				onClose: function() {
					$("#productPopup").dialog("destroy");
					$("#productPopupDiv").remove();
					VansaleDP._lstSelected = null;
				}
			});
		},
		
		/**
		 * su kien nhan phim len xuong tren textbox so luong (dlg)
		 * 
		 * @author lacnv1
		 */
		nextAndPrevTextField: function(e,selector,clazz){	
			var index = $('input.'+clazz).index(selector);	
			var shiftKey = e.shiftKey && e.keyCode == keyCodes.TAB;	
			if((e.keyCode == keyCodes.ARROW_DOWN)){	
				++index;
				var nextSelector = $('input.'+clazz).eq(index);
				if($(nextSelector).length > 0){
					setTimeout(function(){
						$(nextSelector).focus();
						var i = $(nextSelector).attr("id").replace("qttDlg", "");
						$("#productPopup #gridDlg").datagrid("selectRow", i);
					}, 20);
				}
			}else if(e.keyCode == keyCodes.ARROW_UP || shiftKey){
				--index;
				var nextSelector = $('input.'+clazz).eq(index);
				if($(nextSelector).length > 0){
					setTimeout(function(){
						$(nextSelector).focus();
						var i = $(nextSelector).attr("id").replace("qttDlg", "");
						$("#productPopup #gridDlg").datagrid("selectRow", i);
					}, 20);
				}			
			}
		},
		
		/**
		 * su kien go phim tren textbox soluong
		 * 
		 * @author lacnv1
		 */
		quantityTextBoxKeypress: function(e) {
			var key;
			var keychar;
			if (window.event) {
			   key = window.event.keyCode;
			} else if (e) {
			   key = e.which;
			} else {
			   return true;
			}
			keychar = String.fromCharCode(key);

			if (e.ctrlKey && ("aAcCvV").indexOf(keychar) > -1) {
				return true;
			} else if ((key == null) || (key == 0) || (key == 8) ||  (key == 9) || (key == 13) || (key == 27)) {
			   return true;
			} else if (("0123456789/").indexOf(keychar) > -1) {
				return true;
			} else {
			   return false;
			}
		},
		initGridView: function(orderId, shopCode) {
			$("#dgrid").datagrid({
				url: '/commons/get-order-detail',
				width: $("#gridContainer").width() - 10,
				height: "auto",
				scrollbarSize: 0,
				singleSelect: true,
				checkOnSelect: false,
				selectOnCheck: false,
				rownumbers: true,
				fitColumns: true,
				showFooter: true,
				queryParams:{orderId:orderId,shopCodeFilter:shopCode},
				columns: [[
				    {field: "productCode", title:"Mã sản phẩm", width: 100, sortable:false, resizable:false, align: "left", formatter: function(v, r, i) {
				    	if (r.isFooter) {
				    		return "";
				    	}
				    	return VTUtilJS.XSSEncode(v);
				    }},
					{field: "productName", title:"Tên sản phẩm", width: 200, sortable:false, resizable:false, align: "left", formatter: function(v, r, i) {
				    	if (r.isFooter) {
				    		return "";
				    	}
				    	return VTUtilJS.XSSEncode(v);
				    }},
					{field: "warehouseId", title:"Kho", width: 150, sortable:false, resizable:false, align: "left", formatter: function(v, r, i) {
				    	if (r.isFooter) {
				    		return "<b>Trọng lượng</b>";
				    	}
				    	return VTUtilJS.XSSEncode(r.warehouseName);
				    }},
					{field: "quantity", title:"Số lượng", width: 90, fixed:true, sortable:false, resizable:false, align: "right", formatter: function(v, r, i) {
						if (r.isFooter) {
				    		return "<b>" + formatFloatValue(r.weight, 2) + " kg</b>";
				    	}
						var inp = '';
						if (v != undefined && v != null) {
							inp = formatQuantity(v, r.convfact);
						}
						return inp;
				    }},
					{field: "price", title:"Giá bán", width: 100, sortable:false, resizable:false, align: "right", formatter: function(v, r, i) {
						if (r.isFooter) {
				    		return "<b>Tổng tiền</b>";
				    	}
						return formatCurrency(v);
				    }},
					{field: "amount", title:"Thành tiền", width: 120, sortable:false, resizable:false, align: "right", formatter: function(v, r, i) {
						if (r.isFooter) {
				    		return "<b>" + formatCurrency(v) + " VNĐ</b>";
				    	}
						return formatCurrency(v);
				    }}
				]],
				onBeforeLoad: function(param) {
			    	if (!SPCreateOrder._isAllowShowPrice){
			    		$('#dgrid').datagrid('hideColumn','price');
			    		$('#dgrid').datagrid('hideColumn','amount');
			    	}
			    },
				onLoadSuccess: function(data) {
					$('.datagrid-header-rownumber').html('STT');
					GridUtil.updateDefaultRowNumWidth("#gridContainer", "dgrid");
				}
			});
		},
		loadPageView:function(id,type,shopCode){
			if(type == 'DCTG'){
				VansaleDP.initGridViewDCTG(id, shopCode);
			}else if(type == 'DC'){
				VansaleDP.initGridViewDC(id, shopCode);
			}else{
				VansaleDP.initGridView(id, shopCode);
			}
		},
		initGridViewDCTG: function(orderId, shopCode) {
			$("#dgrid").datagrid({
				url: '/commons/get-order-detail',
				width: $("#gridContainer").width() - 10,
				height: "auto",
				scrollbarSize: 0,
				singleSelect: true,
				checkOnSelect: false,
				selectOnCheck: false,
				rownumbers: true,
				fitColumns: true,
				queryParams:{orderId:orderId, shopCodeFilter:shopCode},
				columns: [[
				    {field: "productCode", title:"Mã sản phẩm", width: 100, sortable:false, resizable:false, align: "left", formatter: function(v, r, i) {
				    	return VTUtilJS.XSSEncode(v);
				    }},
					{field: "productName", title:"Tên sản phẩm", width: 200, sortable:false, resizable:false, align: "left", formatter: function(v, r, i) {
				    	return VTUtilJS.XSSEncode(v);
				    }},
					{field: "warehouseId", title:"Kho", width: 150, sortable:false, resizable:false, align: "left", formatter: function(v, r, i) {
				    	return VTUtilJS.XSSEncode(r.warehouseName);
				    }},
					{field: "quantity", title:"Số lượng", width: 90, fixed:true, sortable:false, resizable:false, align: "right", formatter: function(v, r, i) {
						var inp = '';
						if (v != undefined && v != null) {
							inp = formatQuantity(v, r.convfact);
						}
						return inp;
				    }}
				]],
				onLoadSuccess: function(data) {
					$('.datagrid-header-rownumber').html('STT');
					GridUtil.updateDefaultRowNumWidth("#gridContainer", "dgrid");
				}
			});
		},
		initGridViewDC: function(orderId, shopCode) {
			$("#dgrid").datagrid({
				url: '/commons/get-order-detail',
				width: $("#gridContainer").width() - 10,
				height: "auto",
				scrollbarSize: 0,
				singleSelect: true,
				checkOnSelect: false,
				selectOnCheck: false,
				rownumbers: true,
				fitColumns: true,
				showFooter: true,
				queryParams:{orderId:orderId, shopCodeFilter:shopCode},
				columns: [[
				    {field: "productCode", title:"Mã sản phẩm", width: 100, sortable:false, resizable:false, align: "left", formatter: function(v, r, i) {
				    	if (r.isFooter) {
				    		return "";
				    	}
				    	return VTUtilJS.XSSEncode(v);
				    }},
					{field: "productName", title:"Tên sản phẩm", width: 200, sortable:false, resizable:false, align: "left", formatter: function(v, r, i) {
				    	if (r.isFooter) {
				    		return "<b style='float:right'>Trọng lượng</b>";
				    	}
				    	return VTUtilJS.XSSEncode(v);
				    }},
					{field: "quantity", title:"Số lượng", width: 90, fixed:true, sortable:false, resizable:false, align: "right", formatter: function(v, r, i) {
						if (r.isFooter) {
				    		return "<b>" + formatFloatValue(r.weight, 2) + " kg</b>";
				    	}
						var inp = '';
						if (v != undefined && v != null) {
							inp = formatQuantity(v, r.convfact);
						}
						return inp;
				    }},
					{field: "price", title:"Giá bán", width: 100, sortable:false, resizable:false, align: "right", formatter: function(v, r, i) {
						if (r.isFooter) {
				    		return "<b>Tổng tiền</b>";
				    	}
						return formatCurrency(v);
				    }},
					{field: "amount", title:"Thành tiền", width: 120, sortable:false, resizable:false, align: "right", formatter: function(v, r, i) {
						if (r.isFooter) {
				    		return "<b>" + formatCurrency(v) + " VNĐ</b>";
				    	}
						return formatCurrency(v);
				    }}
				]],
				onBeforeLoad: function(param) {
			    	if (!SPCreateOrder._isAllowShowPrice){
			    		$('#dgrid').datagrid('hideColumn','price');
			    		$('#dgrid').datagrid('hideColumn','amount');
			    	}
			    },
				onLoadSuccess: function(data) {
					$('.datagrid-header-rownumber').html('STT');
					GridUtil.updateDefaultRowNumWidth("#gridContainer", "dgrid");
				}
			});
		}
};
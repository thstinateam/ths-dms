var CreateSaleOrder = {
	mapCustomer : new Map(),
	mapNVBH : new Map(),
	mapNVGH : new Map(),
	mapNVTT : new Map(),
	mapCar : new Map(),
	currentCustomerCode : '',
	currentSaleStaff : '',
	isShowDialogNVBHConfirm: true,
	
	/**
	 * Ham validation khi nhap so luong san pham
	 * @param value
	 * @param callback
	 */
	convfactValidation: function(value, callback) {
		if(!/^[0-9\/]+$/.test(value)){
			callback(false);
		} else if(value.indexOf('/')!=-1 && value.split('/').length > 2){
			callback(false);
		} else {
			value = 0;
			callback(true);
		}
	},
	
	/**
	 * Ham set property cell trong hansontable
	 */
	firstColStyle: function(__instance, __td, __row, __col, __prop, __value, __cellProperties) {
		Handsontable.renderers.TextRenderer.apply(this, arguments);
		__td.style.verticalAlign = 'middle';
	},
	
	/**
	 * Ham set product code 
	 */
	productCodeRenderer: function (instance, td, row, col, prop, value, cellProperties) {
		Handsontable.renderers.TextRenderer.apply(this, arguments);
		var html = '';
		if(value != null){
			html = '<a onclick="return CreateSaleOrder.viewProductPromotionDetail(\'' +value+'\');" href="javascript:void(0)">' +Utils.XSSEncode(value)+'</a>'
		}
		td.innerHTML = html;
		  //return td;
	},
	
	/**
	 * Ham bind autoComplete cho cac loai staff
	 */
	bindAutoComplete:function(){		
		Utils.bindComboboxStaffEasyUI('staffSaleCode');
		Utils.bindComboboxStaffEasyUI('cashierStaffCode');		
		Utils.bindComboboxStaffEasyUI('transferStaffCode');
		Utils.bindComboboxCarEasyUI('carId');
		$('#cashierStaffCode, #transferStaffCode').combobox({			
			onChange:function(newvalue,oldvalue){
				$('.combo-panel').each(function(){if(!$(this).is(':hidden')){$(this).parent().css('width','250px');$(this).css('width','248px');}});				
			}
		});
		$('#staffSaleCode').combobox({
			onChange: function(newvalue, oldvalue){
				if(newvalue.length > 0 && CreateSaleOrder.isShowDialogNVBHConfirm){
					var htSale = $('#gridSaleData').handsontable('getInstance');
					var data = htSale.getData();
					if(data.length > 0){
						var row = data[0];
						if(!isNullOrEmpty(row[0])){
							$.messager.confirm('Xác nhận','Danh sách sản phẩm sẽ bị xóa khi chọn lại NVBH. <br /> <b>Bạn có muốn chọn nhân viên mới?</b>',function(r){  
							    if (r){ 					    	
							    	CreateSaleOrder.customerDetails(CreateSaleOrder.currentCustomerCode, newvalue);
							    	CreateSaleOrder.currentSaleStaff = newvalue;
							    }else{
							    	$('#staffSaleCode').combobox('setValue',CreateSaleOrder.currentSaleStaff);
							    }
							});
						}else{
							CreateSaleOrder.customerDetails(CreateSaleOrder.currentCustomerCode, newvalue);
							CreateSaleOrder.currentSaleStaff = newvalue;
						}
					}else{
						CreateSaleOrder.customerDetails(CreateSaleOrder.currentCustomerCode, newvalue);
						CreateSaleOrder.currentSaleStaff = newvalue;}
				}
				CreateSaleOrder.isShowDialogNVBHConfirm = true;
				$('.combo-panel').each(function(){if(!$(this).is(':hidden')){$(this).parent().css('width','250px');$(this).css('width','248px');}});
			}
		});
	},
	
	/**
	 * Ham tra ve ket qua khi chon khach hang tren datagrid
	 * @param rowIndex
	 * @returns {Boolean}
	 */
	getResultSeachStyle1EasyUIDialog : function(rowIndex) {
		var row = $('.easyui-dialog #searchStyle1Grid').datagrid('getRows')[rowIndex];			
		var obj = {};
		obj.code = row.shortCode;
		obj.name = row.customerName;
		obj.address = row.address;
		obj.id = row.id;
		
		if (CommonSearch._currentSearchCallback != null) {
			$('.easyui-dialog').dialog('close');
			CommonSearch._currentSearchCallback.call(this, obj);
		}	
		return false;
	},
	
	/**
	 * Ham open dialog search khach hang
	 * @param url
	 * @param callback
	 */
	openCustomerOnDialog:function(url,callback){		
		$('.easyui-dialog #btnSearchStyle1').unbind('click');
		$('#searchStyle1EasyUIDialogDiv').show();
		var html = $('#searchStyle1EasyUIDialogDiv').html();
		$('#searchStyle1EasyUIDialog').dialog({  
	        title: 'Thông tin tìm kiếm',  
	        closed: false,  
	        cache: false,  
	        modal: true,
	        width:650,
	        onOpen: function(){		        	
	        	var tabindex = -1;
	        	$('.InputTextStyle, select, input, button , li , tr, td, th, label, ul, a, img').each(function () {
					if (this.type != 'hidden') {
						$(this).attr("tabindex", tabindex);
						tabindex -=1;
					}
				});	        		        	
	        	$('.easyui-dialog #seachStyle1Code').focus();
				$('.easyui-dialog #seachStyle1AddressLabel').show();
				$('.easyui-dialog #seachStyle1Address').show();
				$('.easyui-dialog #searchStyle1Grid').show();
				$('.easyui-dialog #btnSearchStyle1').css('margin-left', 270);
				$('.easyui-dialog #btnSearchStyle1').css('margin-top', 5);
				$('.easyui-dialog #seachStyle1CodeLabel').html('Mã KH');
				$('.easyui-dialog #seachStyle1NameLabel').html('Tên KH');
				
				Utils.bindAutoSearch();
				CommonSearch._currentSearchCallback = callback;
				$('.easyui-dialog #searchStyle1Grid').datagrid({
					url : url,
					autoRowHeight : true,
					rownumbers : true, 
					checkOnSelect :true,
					pagination:true,
					rowNum : 10,
					pageSize:10,
					scrollbarSize : 0,
//					fitColumns:true,
					queryParams:{
						page:1
					},
					width : 600,
				    columns:[[  
				        {field:'shortCode',title:'Mã KH',align:'left', width:100, sortable : false,resizable : false},  
				        {field:'customerName',title:'Tên KH',align:'left', width:150, sortable : false,resizable : false},				        
				        {field:'address',title:'Địa chỉ',align:'left', width:250, sortable : false,resizable : false},
				        {field:'select', title:'Chọn', width:68, align:'center',sortable : false,resizable : false,formatter : 
				        	function(value, rowData, rowIndex) {
							return "<a href='javascript:void(0)' onclick=\"return CreateSaleOrder.getResultSeachStyle1EasyUIDialog(" + rowIndex + ");\">Chọn</a>";
				         }},
				        {field :'id',hidden : true},
				    ]],
				    onLoadSuccess :function(data){
				    	 tabindex = 1;
			    		 $('.easyui-dialog input,.easyui-dialog select,.easyui-dialog button').each(function () {
				    		 if (this.type != 'hidden') {
					    	     $(this).attr("tabindex", '');
								 tabindex++;
				    		 }
						 });
			    		 $('.datagrid-header-rownumber').html('STT');	
			    		 updateRownumWidthForJqGrid('.easyui-dialog');
			    		 $(window).resize();
			    		 var code = $('.easyui-dialog #seachStyle1Code').val().trim();
						 if (data != null && data.rows.length == 1 && !isNullOrEmpty(code)) {
							 $('#customerCode').val(data.rows[0].shortCode);
							 $('#customerName').val(data.rows[0].customerName);
							 $('#searchStyle1EasyUIDialogDiv').css("visibility", "hidden");					
							 $('.easyui-dialog').dialog('close');
							 $('#customerCode').change();
						 }
				    }
				});
				$('.easyui-dialog #btnSearchStyle1').bind('click',function(event) {
					if(!$(this).is(':hidden')){
						var code = $('.easyui-dialog #seachStyle1Code').val().trim();
						var name = $('.easyui-dialog #seachStyle1Name').val().trim();
						var address = $('.easyui-dialog #seachStyle1Address').val().trim();
						$('.easyui-dialog #searchStyle1Grid').datagrid('load',{code :code, name: name,address:address});
						var records = $('.easyui-dialog #searchStyle1Grid').datagrid('getRows').length;
						$('.easyui-dialog #seachStyle1Code').focus();
						if (records != null && records == 1 && !isNullOrEmpty(code)) {
							$('#customerCode').val($('.easyui-dialog #searchStyle1Grid').datagrid('getRows')[0].shortCode);
							$('#customerName').val($('.easyui-dialog #searchStyle1Grid').datagrid('getRows')[0].customerName);
							$('#searchStyle1EasyUIDialogDiv').css("visibility", "hidden");					
							$('.easyui-dialog').dialog('close');
							$('#customerCode').change();
						}
					}					
				});
				$('.easyui-dialog #btnClose').bind('click',function(event) {
					$('#searchStyle1EasyUIDialogDiv').css("visibility", "hidden");					
					$('.easyui-dialog').dialog('close');
				});
	        },
	        onBeforeClose: function() {
	        	var curIdFocus = $('#cur_focus').val();
				$('#' + curIdFocus).focus();				
				$('.easyui-dialog #searchStyle1Grid').datagrid('reload',{	
					pageSize: 10
				});
	        },
	        onClose : function(){
	        	$('#searchStyle1EasyUIDialogDiv').html(html);
	        	$('#searchStyle1EasyUIDialogDiv').hide();
	        	$('#searchStyle1ContainerGrid').html('<table id="searchStyle1Grid" class="easyui-datagrid" style="width: 520px;"></table><div id="searchStyle1Pager"></div>');
	        	$('.easyui-dialog #seachStyle1Code').val('');
	        	$('.easyui-dialog #seachStyle1Name').val('');
	        	$('.easyui-dialog #seachStyle1Address').val('');	        	
	        }
		});
	},
	
	/**
	 * disable selectbox all staff
	 */
	disabledStaff:function(){
		$('#staffSaleCode').combobox('disable');		
		$('#staffSaleCode').parent().addClass('BoxDisSelect');		
		$('#cashierStaffCode').combobox('disable');
		$('#cashierStaffCode').parent().addClass('BoxDisSelect');		
		$('#transferStaffCode').combobox('disable');
		$('#transferStaffCode').parent().addClass('BoxDisSelect');
	},
	
	/**
	 * enable selectbox all staff
	 */
	enabledStaff:function(){
		$('#staffSaleCode').combobox('enable');		
		$('#staffSaleCode').parent().removeClass('BoxDisSelect');			
		$('#cashierStaffCode').combobox('enable');
		$('#cashierStaffCode').parent().removeClass('BoxDisSelect');			
		$('#transferStaffCode').combobox('enable');
		$('#transferStaffCode').parent().removeClass('BoxDisSelect');
	},
	
	/**
	 * reset selectbox all staff
	 */
	resetStaff:function(){
		$('#staffSaleCode').combobox('setValue','');
		$('#staffSaleCode').combobox('disable');		
		$('#staffSaleCode').parent().addClass('BoxDisSelect');				
		$('#cashierStaffCode').combobox('setValue','');
		$('#cashierStaffCode').combobox('disable');
		$('#cashierStaffCode').parent().addClass('BoxDisSelect');
		$('#transferStaffCode').combobox('setValue','');
		$('#transferStaffCode').combobox('disable');
		$('#transferStaffCode').parent().addClass('BoxDisSelect');
	},
	
	/**
	 * goi khi load trang
	 */
	loadPage: function(){
		disabled('btnOrderSave');
		disabled('payment');
		applyDateTimePicker('#deliveryDate');
		$('#deliveryDate').datepicker('setDate', new Date());
		$('.MySelectBoxClass').customStyle();
		CreateSaleOrder.bindAutoComplete();
		GridSaleBusiness.initSaleGrid();
		GridFreeBusiness.initFreeGrid();
		$(window).bind('keydown', function(e) {
			// To hop : Ctrl + Alt: Tinh tien va Tao don hang 
		   if (e.ctrlKey && e.altKey) {
			   if (!$("#payment").is(':disabled')) {
				   if(!GridSaleBusiness.isEditGridSale){
					   CreateSaleOrder.pay();
				   }
			   } else if (!$("#btnOrderSave").is(':disabled') && $("#payment").is(':disabled')) {
				   if(!GridSaleBusiness.isEditGridSale){
					   CreateSaleOrder.createOrder();
				   }
			   }
		   }
		});
		$('#priorityId').bind('keypress', function(event){
			if(event.keyCode == keyCodes.TAB && !event.shiftKey){
				if($('#gridSaleData').handsontable('getInstance').countRows() >0){
					$('#gridSaleData').handsontable('getInstance').selectCell(0,COLSALE.PRO_CODE);
				}
			}
		});
		$('#customerCode').bind('keyup', function(event) {					
			if (event.keyCode == keyCode_F9) {
				CreateSaleOrder.openCustomerOnDialog(
						'/commons/customer-in-shop/search',function(data){					
					$('#customerCode').val(data.code);
					$('#customerCode').change();
				});						
			} 
		});
		$('#customerCode').live('change',function(){
			CreateSaleOrder.isShowDialogNVBHConfirm = false;
			var shortCode = $(this).val().trim().toUpperCase();	
			if(isNullOrEmpty(shortCode)){
				$('#GeneralInforErrors').html('Chọn khách hàng trước khi chọn NVBH,NVGH,NVTT.').show();
				CreateSaleOrder.resetStaff();
				return false;
			}
			var htSale = $('#gridSaleData').handsontable('getInstance');
			var data = htSale.getData();
			if(CreateSaleOrder.currentCustomerCode.length > 0 && CreateSaleOrder.currentCustomerCode != shortCode){
				var len = data.length;
				if(len>=1){
					var row = data[0];
					if(!isNullOrEmpty(row[0])){
						$.messager.confirm('Xác nhận','Danh sách sản phẩm đã chọn sẽ xóa nếu chọn lại KH. <br /> <b>Bạn có muốn chọn KH khác không ?</b>.',function(r){  
							if(r){
								CreateSaleOrder.customerDetails(shortCode, null);
								CreateSaleOrder.currentCustomerCode = shortCode;
							}else{
								$('#customerCode').val(CreateSaleOrder.currentCustomerCode);								
							}
						});	
					}else{
						CreateSaleOrder.customerDetails(shortCode,null);
						CreateSaleOrder.currentCustomerCode = shortCode;
					}
				}else{
					CreateSaleOrder.customerDetails(shortCode,null);
					CreateSaleOrder.currentCustomerCode = shortCode;
				}							
			}else{
				CreateSaleOrder.customerDetails(shortCode,null);
				CreateSaleOrder.currentCustomerCode = shortCode;
			}			
		});
		var tm = setTimeout(function(){
			CreateSaleOrder.disabledStaff();
			$('#customerCode').focus();	
			clearTimeout(tm);
			},1000);
	},
	
	/**
	 * Load thong tin cua khach hang khi chon khach hang
	 * @param customerCode
	 * @param staffCode
	 * @param isShowNVBHConfirm
	 */
	customerDetails:function(customerCode, staffCode, isShowNVBHConfirm){
		if(isNullOrEmpty(customerCode)){
			return;
		}
		var url = '/sale-product/create-sale-order/customer-details?customerCode=' +customerCode;
		if(!isNullOrEmpty(staffCode)) {
			url = url + '&saleStaffCode=' + staffCode;
		}
		$('.ErrorMsgStyle').html('').hide();
		$('.loadingTitle').show();			
		$.getJSON(encodeURI(url), function(data) {		
			$('.loadingTitle').hide();
			if(!data.error){
				$('#customerName').html(data.customerName);
				$('#address').html(data.address);	
				$('#phone').html(data.phone);
				$('#channelType').html(data.channelTypeName);
				CreateSaleOrder.enabledStaff();
				$('#cashierStaffCode').combobox('setValue', data.cashierStaffCode);
				$('#transferStaffCode').combobox('setValue', data.transferStaffCode);				
				$('#staffSaleCode').combobox('setValue', data.staffSaleCode);
				var isFocus = false;
				if (isNullOrEmpty(data.cashierStaffCode) || data.cashierStaffCode == undefined) {
					/*var val = $('#cashierStaffCode').combobox('getData')[1];
					$('#cashierStaffCode').combobox('setValue', val.staffCode);	*/
					$('#cashierStaffCode').next().find('input').focus();
					isFocus = true;
				}
				if (isNullOrEmpty(data.transferStaffCode)|| data.transferStaffCode == undefined) {
					/*var val = $('#transferStaffCode').combobox('getData')[1];
					$('#transferStaffCode').combobox('setValue', val.staffCode);*/
					if(!isFocus){
						$('#transferStaffCode').next().find('input').focus();
						isFocus = true;
					}
				}
				if (isNullOrEmpty(data.staffSaleCode) || data.staffSaleCode == undefined) {
					/*var val = $('#staffSaleCode').combobox('getData')[1];
					$('#staffSaleCode').combobox('setValue', val.staffCode);*/
					if(!isFocus){
						$('#staffSaleCode').next().find('input').focus();
					}
					CreateSaleOrder.isShowDialogNVBHConfirm = true;
				}else{
					$('#carId').next().find('input').focus();
				}
				
				/**Lay danh sach chuong trinh ho tro thuong mai**/
		        if($.isArray(data.commercials) && data.commercials.length > 0) {
		        	GridSaleBusiness.listCS = data.commercials;
		        	GridSaleBusiness.mapCS = new Map();
		        	GridSaleBusiness.listCSCode = new Array();
					for(var i = 0; i < data.commercials.length; i++) {
						GridSaleBusiness.mapCS.put(GridSaleBusiness.listCS[i].code, GridSaleBusiness.listCS[i]);
						GridSaleBusiness.listCSCode.push(GridSaleBusiness.listCS[i].code);
					}
		        }
		        
				/** Lay danh sach san pham(*/
				GridSaleBusiness.listSaleProduct = data.listSaleProduct;
				GridSaleBusiness.mapSaleProduct = new Map();
				GridSaleBusiness.listSaleProductCode = new Array();
				if(GridSaleBusiness.listSaleProduct != null && GridSaleBusiness.listSaleProduct != undefined) {
					for(var i = 0; i < GridSaleBusiness.listSaleProduct.length; i++) {
						GridSaleBusiness.mapSaleProduct.put(GridSaleBusiness.listSaleProduct[i].productCode, GridSaleBusiness.listSaleProduct[i]);
						GridSaleBusiness.listSaleProductCode.push(GridSaleBusiness.listSaleProduct[i].productCode);
					}
					$('#gridSaleContainer').css('height', 300);
					GridSaleBusiness.resetSaleGrid();
				}else{
					GridSaleBusiness.disableSaleGrid();
				}
				GridFreeBusiness.resetFreeGrid();
				CreateSaleOrder.currentCustomerCode = data.shortCode;
				CreateSaleOrder.currentSaleStaff = $('#staffSaleCode').combobox('getValue');
				
			}else{				
				$('#GeneralInforErrors').html(data.errMsg).show();
				$('#customerName').html('');
				$('#address').html('');
				$('#phone').html('');
				CreateSaleOrder.resetStaff();
				GridSaleBusiness.disableSaleGrid();
				disabled('btnOrderSave');
				disabled('payment');
				return false;
			}
		});		
		enable('payment');	
		disabled('btnOrderSave');
		disabled('approveButton');
		enable('saleProductAddProduct');
	},	
	
	/**
	 * Ham xu ly tinh tien
	 * @returns {Boolean}
	 */
	pay: function() {
		/**Kiem tra dieu kien*/
		$('#serverErrors').html('').hide();
		$('.ErrorMsgStyle').html('').hide();		
		var listProductCode = new Array();
		var listQuantityStr = new Array();
		var listQuantityInt = new Array();
		var listCS = new Array();
		var listCSType = new Array();
		var data = $('#gridSaleData').handsontable('getInstance').getData();
		var isFail = false;
		var errMsg = '';
		var errRow = -1;
		var errCol = -1;
		errMsg = Utils.getMessageOfRequireCheck('shopCode', 'Mã đơn vị');
		if(errMsg.length==0){
			errMsg = Utils.getMessageOfRequireCheck('customerCode', 'Mã khách hàng');		
		}
		if(errMsg.length > 0){
			$('#GeneralInforErrors').html(errMsg).show();
			return false;
		}
		for(var i = 0; i < data.length; i++) {
			var rowData = data[i];
			var productCode = rowData[COLSALE.PRO_CODE];
			var quantity = rowData[COLSALE.QUANTITY];
			var cs = data[i][COLSALE.PROGRAM];
			if(!isNullOrEmpty(productCode) && GridSaleBusiness.mapSaleProduct.get(productCode) != null) {
				var product = GridSaleBusiness.mapSaleProduct.get(productCode);
				if(!/^[0-9\/]+$/.test(quantity)) {
					isFail = true;
					errMsg = 'Bạn chưa nhập số lượng cho sản phẩm ' + Utils.XSSEncode(productCode);
					errRow = i;
					errCol = COLSALE.QUANTITY;
					break;
				} else if(quantity.indexOf('/')!=-1 && quantity.split('/').length > 2) {
					isFail = true;
					errMsg = 'Bạn chưa nhập số lượng cho sản phẩm ' + Utils.XSSEncode(productCode);
					errRow = i;
					errCol = COLSALE.QUANTITY;
					break;
				} else if(isNullOrEmpty(quantity)) {
					isFail = true;
					errMsg = 'Bạn chưa nhập số lượng cho sản phẩm ' + Utils.XSSEncode(productCode);
					errRow = i;
					errCol = COLSALE.QUANTITY;
					break;
				}else if(StockValidateInput.getQuantity(quantity, product.convfact) == 0){ 
					isFail = true;
					errMsg = 'Bạn chưa nhập số lượng cho sản phẩm ' + Utils.XSSEncode(productCode);
					errRow = i;
					errCol = COLSALE.QUANTITY;
				}else {
					var csObj = null;
					if(!isNullOrEmpty(cs) && cs != product.promotionProgramCode) {
						csObj = GridSaleBusiness.mapCS.get(cs);
						if(csObj == null) {
							/*bao loi*/
							isFail = true;
							errMsg = 'Mã chương trình HTTM nhập vào không tồn tại. Vui lòng nhập mã khác';
							errRow = i;
							errCol = COLSALE.PROGRAM;
							break;
						}
					}
					for(var j = i+1; j < data.length; j++){
						var pro_temp = data[j][COLSALE.PRO_CODE];
						var prog_temp = data[j][COLSALE.PROGRAM];
						if(!isNullOrEmpty(pro_temp) && pro_temp == productCode){
							if(isNullOrEmpty(cs) && isNullOrEmpty(prog_temp)){
								isFail = true;
								errMsg = 'Sản phẩm ' + Utils.XSSEncode(productCode) +' chỉ cho phép đặt 1 lần trong đơn hàng';
								errRow = j;
								errCol = COLSALE.PRO_CODE;
								break;
							}else{
								if(!isNullOrEmpty(prog_temp) && !isNullOrEmpty(cs) && cs == prog_temp){
									isFail = true;
									errMsg = 'Sản phẩm ' + Utils.XSSEncode(productCode) +' không được trùng CT HTTM trong đơn hàng';
									errRow = j;
									errCol = COLSALE.PROGRAM;
									break;
								}
							}
						}
					}
					if(isFail){break;}
					
					/** set du lieu vao parram va set danh sach san pham cua don hang ban(GridSaleBusiness.mapSaleProductGrid) phuc vu luu don hang*/
					listProductCode.push(productCode);
					listQuantityStr.push(quantity);
					product.quantity = StockValidateInput.getQuantity(quantity, product.convfact);
					listQuantityInt.push(StockValidateInput.getQuantity(quantity, product.convfact));
					if(!isNullOrEmpty(cs) && cs != product.promotionProgramCode) {
							listCS.push(cs);
							listCSType.push(csObj.promotionType);
							product.commercialSupport = cs;
							product.commercialSupportType = csObj.promotionType;
					}else{
						listCS.push('');
						listCSType.push('');
						product.commercialSupport = product.promotionProgramCode != null?product.promotionProgramCode:'';
						product.commercialSupportType = 'ZV';
					}
					GridSaleBusiness.mapSaleProductGrid.put(product.productId+'-' +product.commercialSupport, product);
				}
			}
		}
		if(listProductCode.length == 0 && !isFail){
			isFail = true;
			errMsg = "Không tồn tại sản phẩm nào trong đơn hàng!";
			errRow = 0;
			errCol = 0;
		}
		
		if(!isFail) {
			var params = new Object();
			params.listCS = listCS;
			params.listCSType = listCSType;
			params.listProductCode = listProductCode;
			params.listQuantityStr = listQuantityStr;
			params.listQuantityInt = listQuantityInt;
			params.customerCode = CreateSaleOrder.currentCustomerCode;
			data.shopCode = $('#shopCode').val().trim();
			$('#divOverlay').show();			
			$('#GeneralInforErrors').hide();
			$.ajax({
				type : "POST",
				url : '/sale-product/create-sale-order/pay',
				data :($.param(params, true)),
				dataType : "json",
				success : function(result) {
					$('#divOverlay').hide();
					if(!result.error){
						$('#gridFreeContainer').css('height', 200);
						GridFreeBusiness.listFreeProductAuto = new Map();
						GridFreeBusiness.listPromotionProductForOrder = new Map();
						GridFreeBusiness.mapChangePPromotionZV01_18 = result.changePPromotionZV01_18;
						GridFreeBusiness.mapChangePPromotionZV21 = result.changePPromotionZV21;
						GridFreeBusiness.programes = result.programes;
						//zv01_18
						var pp = result.pp;
						for(var i = 0; i < pp.length; i++) {
							var pproduct = GridFreeBusiness.getObjectPromotionProduct(pp[i]);	
							pproduct.quantity = pproduct.maxQuantity;
							pproduct.rownum = i+1;
							GridFreeBusiness.listFreeProductAuto.put(Utils.XSSEncode(pproduct.programCode+'-' +pproduct.productCode),pproduct);
						}
						//zv19_21
						var zV21 = result.zV21;
						var zV19z20 = result.zV19zV20;
						var zLenPPAuto = GridFreeBusiness.listFreeProductAuto.size();
						var zzrowNumForOrder = zLenPPAuto + 1;
						if(zV21!=undefined && zV21!=null){								
							var pproduct = new Object();
							pproduct = GridFreeBusiness.getObjectPromotionProduct(zV21);
							pproduct.rownum = zzrowNumForOrder;
							GridFreeBusiness.listFreeProductAuto.put(Utils.XSSEncode(pproduct.programCode+'-' +pproduct.productCode),pproduct);
							zLenPPAuto = GridFreeBusiness.listFreeProductAuto.size();
							for(var i=0;i<zV21.listPromotionForPromo21.length;++i){
								var pproduct = new Object();
								pproduct = GridFreeBusiness.getObjectPromotionProduct(zV21.listPromotionForPromo21[i]);
								pproduct.quantity = pproduct.maxQuantity;	
								pproduct.rownum = zzrowNumForOrder;
								GridFreeBusiness.listFreeProductAuto.put(Utils.XSSEncode(pproduct.programCode+'-' +pproduct.productCode),pproduct);
							}	
						}else if(zV19z20!=undefined && zV19z20!=null){								
							var pproduct = new Object();
							pproduct = GridFreeBusiness.getObjectPromotionProduct(zV19z20);
							pproduct.rownum = zzrowNumForOrder;
							GridFreeBusiness.listFreeProductAuto.put(Utils.XSSEncode(pproduct.programCode+'-' +pproduct.productCode),pproduct);																
						}
						GridFreeBusiness.listPromotionProductForOrder.put('zV21',result.zV21C);
						GridFreeBusiness.listPromotionProductForOrder.put('zV19zV20',result.zV19zV20C);	
						GridFreeBusiness.fillDataTableFreeProduct(GridFreeBusiness.listFreeProductAuto.valArray);
						$('html, body').animate({ scrollTop: $(document).height() }, 0);
						enable('btnOrderSave');
						disabled('payment');
					}else{
						$('#GeneralInforErrors').html(result.errMsg).show();
					}
				},
				error:function(XMLHttpRequest, textStatus, errorThrown) {
					$('#divOverlay').hide();	
				}
			});
		}else{
			$('#GeneralInforErrors').html(errMsg).show();
			$('#gridSaleData').handsontable('getInstance').selectCell(errRow, errCol);
		}
	},
	
	/**
	 * Tinh tong tien va tong khoi luong cho don hang
	 */
	fillTotalAuto: function(){
		var totalWeight = parseFloat($('#totalWeightSale').html())+ parseFloat($('#totalWeightFree').html());
		var totalAmount = Utils.returnMoneyValue($('#totalAmountSale').html()) - Utils.returnMoneyValue($('#totalAmountFree').html());
		$('#totalWeight').html(formatFloatValue(totalWeight,2));
		$('#totalAmount').html(formatCurrency(totalAmount));
	},
	
	/**
	 * Tao don hang
	 * @returns {Boolean}
	 */
	createOrder:function(){
		$('.ErrorMsgStyle').html('').hide();
		var disabled = $('#payment').attr('disabled');
		if(disabled!='disabled'){
			return false;
		}
		$('#serverErrors').html('').hide();
		var msg ='';
		msg = Utils.getMessageOfRequireCheck('shopCode', 'Mã đơn vị');
		if(msg.length==0){
			msg = Utils.getMessageOfRequireCheck('customerCode', 'Mã khách hàng');
		}
		if(msg.length==0){
			msg = Utils.getMessageOfRequireCheck('staffSaleCode', 'Nhân viên bán hàng', true);
		}
		if(msg.length == 0 && $('#deliveryDate').val()!=null && $('#deliveryDate').val().length > 0){
			msg = Utils.getMessageOfInvalidFormatDate('deliveryDate', 'Ngày giao');
		}
		var deliveryDate = $('#deliveryDate').val().trim();
		var curDate = $('#currentLockDay').val().trim();
		if(msg.length ==0 && deliveryDate.length >0){
			if (!Utils.compareDate(curDate,deliveryDate)) {
				$('#GeneralInforErrors').html('Ngày giao không được nhỏ hơn ngày chốt').show();
				window.scrollTo(0,0);
				return false;
			}
		}
		if(msg.length>0){
			$('#GeneralInforErrors').html(msg).show();
			window.scrollTo(0,0);
			return false;
		}		
		var data = new Object();
		data.shopCode = $('#shopCode').val().trim();
		data.customerCode = $('#customerCode').val().trim();
		data.carId = $('#carId').val().trim();
		data.deliveryCode = $('#transferStaffCode').combobox('getValue').trim();
		data.cashierCode = $('#cashierStaffCode').combobox('getValue').trim();
		data.salerCode = $('#staffSaleCode').combobox('getValue').trim();
		data.deliveryDate = $('#deliveryDate').val().trim();
		if($('#priorityId').val()!=null) {
			data.priorityId = $('#priorityId').val().trim();
		}
		var htSale = $('#gridSaleData').handsontable('getInstance');
		var saleData = htSale.getData();
		/*for (var i=0;i<saleData.length;i++) {
			var productCode = saleData[i][COLSALE.PRO_CODE];
			if(productCode != null){
				var product = GridSaleBusiness.mapSaleProduct.get(productCode);
				if (product != null && product.promotionProgramCode != null) {
					if (isNullOrEmpty(saleData[i][COLSALE.PROGRAM])) {
						saleData[i][COLSALE.PROGRAM] = product.promotionProgramCode;
						row[index].commercialSupportType = 'ZV';
					}
				}
			}
		}*/
		var lstCommercialSupport = new Array()
			, lstCommercialSupportType = new Array()
			, lstProduct = new Array()
			, lstQuantity = new Array()
			, lstProductDestroy = new Array()
			, lstProductSaleLot = new Array()
			, lstPromotionProductSaleLot = new Array()
			, lstBuyProductCommercialSupportProgram = new Array()
			, lstBuyProductCommercialSupportProgramType = new Array();	
		for(var i=0;i<GridSaleBusiness.mapSaleProductGrid.size();++i){
			var obj = GridSaleBusiness.mapSaleProductGrid.get(GridSaleBusiness.mapSaleProductGrid.keyArray[i]);
			var check_lot = obj.checkLot==1 || obj.checkLot=='1'? true:false;
			if(!isNullOrEmpty(obj.productId)) {
				lstBuyProductCommercialSupportProgram.push(obj.commercialSupport);
				if (obj.commercialSupportType != undefined 
						&& (obj.commercialSupportType == 'ZV' || obj.commercialSupportType.indexOf('ZV') != -1)) {
					lstBuyProductCommercialSupportProgramType.push('ZV');
				} else {
					var tmp = GridSaleBusiness.mapCS.get(obj.commercialSupport);
					if (tmp != undefined && tmp != null) {
						lstBuyProductCommercialSupportProgramType.push(tmp.type);
					} else {
						lstBuyProductCommercialSupportProgramType.push('');
					}
				}
				lstProduct.push(obj.productCode);	
				if(obj.commercialSupportType != 'ZV' && obj.commercialSupportType != '') {
					lstCommercialSupport.push(obj.commercialSupport);
					lstCommercialSupportType.push(obj.commercialSupportType);
				} else if(obj.commercialSupport != '' && obj.commercialSupportType == '') {
					var program = GridSaleBusiness.mapCS.get(obj.commercialSupport);
					if (program == null || program == undefined) {
						$('#serverErrors').html('CTHTTM ' + Utils.XSSEncode(obj.commercialSupport) +' không tổn tại. Vui lòng chọn lại').show();
						$('#saleProductGrid').datagrid('selectRow', i);
						return false;
					}
					lstCommercialSupport.push(obj.commercialSupport);
					lstCommercialSupportType.push('');
				} else {
					lstCommercialSupport.push('');
					lstCommercialSupportType.push('');
				}
				lstQuantity.push(obj.quantity);
				if( check_lot && obj.commercialSupportType=='ZH' && false){ //Lo lam sau
					var lotDetail = SPCreateOrder._lstProductLotVO.get(obj.productCode);
					if(lotDetail==null || lotDetail == undefined){
						$('#serverErrors').html('Tổng số lượng các lô hủy của sản phẩm ' + Utils.XSSEncode(obj.productCode) +' không bằng số lượng hủy đã nhập.').show();
						$('#saleProductGrid').datagrid('selectRow', i);
						return false;
					}
					var sumQuantityLot = 0;
					for(var j=0;j<lotDetail.productLots.size();++j){
						var detail = lotDetail.productLots.get(lotDetail.productLots.keyArray[j]);
						var quantityLot = Number(detail.quatity); 
						if(quantityLot>0){	
							sumQuantityLot = sumQuantityLot + quantityLot;
						}
					}
					if(Number(sumQuantityLot)!=Number(lstQuantity[i])){
						$('#serverErrors').html('Tổng số lượng các lô hủy của sản phẩm ' + Utils.XSSEncode(obj.productCode) +' không bằng số lượng hủy đã nhập.').show();
						$('#saleProductGrid').datagrid('selectRow', i);
						return false;
					}
					lstProductDestroy.push(lotDetail.strMap);
					lstProductSaleLot.push(obj.productCode + ';');
				}else if(check_lot && isFSXN && false){ //Lo lam sau
					var lotDetail = SPCreateOrder._lstProductSaleLotVO.get(obj.productCode);
					if(lotDetail == null || lotDetail == undefined) {
						lstProductDestroy.push(obj.productCode + ';');
						lstProductSaleLot.push(obj.productCode + ';');
					} else {
						var sumQuantityLot = 0;
						var strLot = new Array();;
						for(var j = 0; j < lotDetail.productLots.size(); j++) {
							var detail = lotDetail.productLots.get(lotDetail.productLots.keyArray[j]);
							if(Number(detail.quatity) > 0) {
								sumQuantityLot = sumQuantityLot + Number(detail.quatity);
								strLot.push(detail.lot);
							}
						}
						if(Number(sumQuantityLot) != Number(lstQuantity[i])) {							
							$('#serverErrors').html('Tổng số lượng các lô bán của sản phẩm ' + Utils.XSSEncode(obj.productCode) +' không bằng số lượng bán đã nhập.').show();
							$('#saleProductGrid').datagrid('selectRow', i);
							return false;
						}
						lstProductSaleLot.push(lotDetail.strMap);
						lstProductDestroy.push(obj.productCode + ';');
					}
					
				} else {
					lstProductDestroy.push(obj.productCode + ';');
					lstProductSaleLot.push(obj.productCode + ';');
				}
			} else {
				if (isNullOrEmpty(obj.productId) && !isNullOrEmpty(obj.commercialSupport)) {
					$('#serverErrors').html('Sản phẩm không tồn tại. Vui lòng chọn lại ').show();
					htSale.selectCell(i,COLSALE.PRO_CODE);
					return false;
				}
			}
		}
		
		if(lstCommercialSupport.length>0){
			data.lstCommercialSupport = lstCommercialSupport;
		}
		if(lstCommercialSupportType.length>0){
			data.lstCommercialSupportType = lstCommercialSupportType;
		}
		if(lstProduct.length==0){
			$('#serverErrors').html('Không tồn tại sản phẩm nào trong đơn hàng').show();
			return false;
		}
		if(lstQuantity.length==0){
			$('#serverErrors').html('Vui lòng nhập đúng số lượng sản phẩm').show();
			return false;
		}
		data.lstProduct=lstProduct;
		data.lstQuantity=lstQuantity;
		data.lstProductDestroy = lstProductDestroy;
		data.lstProductSaleLot = lstProductSaleLot;
		data.lstBuyProductCommercialSupportProgram = lstBuyProductCommercialSupportProgram;
		data.lstBuyProductCommercialSupportProgramType = lstBuyProductCommercialSupportProgramType;
		
		var lstPromotionCode = new Array();		
		var lstPromotionProduct = new Array();
		var lstDiscount = new Array();
		var lstDisper = new Array();
		var lstPromotionType = new Array();
		var lstPromotionProductQuatity = new Array();
		var lstPromotionProductMaxQuatity = new Array();
		var lstPromotionProductMaxQuatityCk = new Array();
		var htFree = $('#gridFreeData').handsontable('getInstance');
		for(var i=0;i<GridFreeBusiness.listFreeProductAuto.size();++i){			
			var temp = GridFreeBusiness.listFreeProductAuto.get(GridFreeBusiness.listFreeProductAuto.keyArray[i]);		
			if(temp.promotionType==PromotionType.PROMOTION_FOR_ZV21){
				continue;
			}				
			lstPromotionCode.push(temp.programCode);			
			lstPromotionType.push(temp.type);
			lstDiscount.push(temp.discountAmount);
			lstDisper.push(temp.discountPercent);
			lstPromotionProductQuatity.push(temp.quantity);					
			if(temp.type == PromotionType.FREE_PRICE || temp.type == PromotionType.FREE_PERCENT) {
				var zvalue = temp.discountAmount;
				if(zvalue.length==0){
					msg = 'Bạn chưa nhập số tiền khuyến mãi. Vui lòng nhập giá trị.';
					htFree.selectCell(i,COLFREE.QUANTITY);
				}else{
					var maxDiscountAmount = Number(temp.maxDiscountAmount);
					var discountAmount = Number(temp.discountAmount);
					if(maxDiscountAmount<discountAmount){
						msg = 'Số tiền khuyến mãi vượt quá Số tiền khuyến mãi tối đa được hưởng. Vui lòng nhập lại.';
						htFree.selectCell(i,COLFREE.QUANTITY);
					}
				}	
				lstPromotionProduct.push('');
				lstPromotionProductMaxQuatity.push(temp.maxDiscountAmount);
				lstPromotionProductMaxQuatityCk.push(temp.maxDiscountAmount);
			}else if(temp.type == PromotionType.FREE_PRODUCT) {				
				var zcvalue = temp.quantity;
				var zvalue = getQuantity(zcvalue,temp.convfact);
				if(zcvalue.length==0){
					msg = 'Bạn chưa nhập số lượng sản phẩm khuyến mãi. Vui lòng nhập giá trị.';
					htFree.selectCell(i,COLFREE.QUANTITY);
				}else if(isNaN(zvalue)){
					msg = 'Số lượng sản phẩm khuyến mãi không hợp lệ. Vui lòng nhập giá trị.';
					htFree.selectCell(i,COLFREE.QUANTITY);
				}else{
					var maxQuantity = Number(temp.maxQuantity);				
					var qty = Number(temp.quantity);
					if(maxQuantity<qty){
						msg = 'Số lượng khuyến mãi vượt quá số lượng khuyến mãi tối đa được hưởng. Vui lòng nhập lại.';
						htFree.selectCell(i,COLFREE.QUANTITY);
					}
				}	
				lstPromotionProduct.push(temp.productCode);
				lstPromotionProductMaxQuatity.push(temp.maxQuantity);
				lstPromotionProductMaxQuatityCk.push(temp.maxQuantityCk == undefined ? temp.maxQuantity : temp.maxQuantityCk);
			}
			if(msg.length>0){				
				$('#serverErrors').html(msg).show();
				$('#gridPromotionProduct').datagrid('selectRow',i);
				return false;
			}
			if(isFSXN && false){//Lo de sau
				if(temp.type == '1' && temp.checkLot!=undefined && temp.checkLot==1) {
					var lotDetail = SPCreateOrder._lstPromotionProductSaleLotVO.get(temp.productCode+'-' +i);
					if(lotDetail == null || lotDetail == undefined) {
						if($('#convact_promotion_value_' +i).length!=0 && $('#convact_promotion_value_' +i).val()!='0'){
							lstPromotionProductSaleLot.push(temp.productCode + ';');
						}
					} else {
						var sumQuantityLot = 0;						
						if(lotDetail!=null){
							for(var j = 0; j < lotDetail.productLots.size();j++) {
								var detail = lotDetail.productLots.get(lotDetail.productLots.keyArray[j]);
								if(Number(detail.quatity) > 0) {
									sumQuantityLot += Number(detail.quatity);									
								}
							}
						}
						if(Number(sumQuantityLot) != Number(temp.quantity)) {
							$('#serverErrors').html('Tổng số lượng các lô của sản phẩm ' + Utils.XSSEncode(temp.productCode) +' không bằng số lượng khuyến mãi.').show();
							$('#gridPromotionProduct').datagrid('selectRow',i);
							return false;
						}
						if(lotDetail!=null) {
							lstPromotionProductSaleLot.push(lotDetail.strMap);
						} else {
							lstPromotionProductSaleLot.push(temp.productCode + ';');
						}
					}					
				} else {
					lstPromotionProductSaleLot.push(temp.productCode + ';');
				}
			}
		}		
		data.lstPromotionCode = lstPromotionCode;		
		data.lstPromotionProduct = lstPromotionProduct;
		data.lstPromotionType = lstPromotionType;
		data.lstDiscount = lstDiscount;
		data.lstDisper = lstDisper;
		data.lstPromotionProductQuatity = lstPromotionProductQuatity;
		data.lstPromotionProductSaleLot = lstPromotionProductSaleLot;
		data.lstPromotionProductMaxQuatity = lstPromotionProductMaxQuatity;
		data.lstPromotionProductMaxQuatityCk = lstPromotionProductMaxQuatityCk;
		Utils.addOrSaveData(data, '/sale-product/create-sale-order/create-order', null, 'serverErrors', function(result){
			if(!result.error){
				var tmz = setTimeout(function(){
					$('#btnOrderSave').attr('disabled','disabled');	$('#btnOrderSave').addClass('BtnGeneralDStyle');
					$('#customerCode').attr('disabled','disabled');	$('#customerCode').addClass('BtnGeneralDStyle');
					
//					SPCreateOrder._createSuccess = true;
//					SPCreateOrder._isSave=true;	
					
					
					$('#staffSaleCode').combobox('disable');		
					$('#staffSaleCode').parent().addClass('BoxDisSelect');
					
					$('#cashierStaffCode').combobox('disable');
					$('#cashierStaffCode').parent().addClass('BoxDisSelect');
					
					$('#transferStaffCode').combobox('disable');
					$('#transferStaffCode').parent().addClass('BoxDisSelect');
					
					//$('#carId').attr('disabled','disabled');
					$('#carId').combobox('disable');
					$('#carId').parent().addClass('BoxDisSelect');							
					$('#deliveryDate').attr('disabled','disabled');	
					$('#priorityId').attr('disabled','disabled');$('#priorityId').parent().addClass('BoxDisSelect');
					isAuthorize = false;
					var htSale = $('#gridSaleData').handsontable('getInstance');
					var htFree = $('#gridFreeData').handsontable('getInstance');
					for(var i=0;i<htSale.countRows();i++){
						htSale.getCellMeta(i,COLSALE.PRO_CODE).readOnly = true;
						htSale.getCellMeta(i,COLSALE.QUANTITY).readOnly = true;
						htSale.getCellMeta(i,COLSALE.PROGRAM).readOnly =true;
					}
					for(var i=0;i<htFree.countRows();i++){
						htFree.getCellMeta(i,COLFREE.QUANTITY).readOnly = true;
						htFree.setDataAtCell(i,COLFREE.PROGRAM_CHANGE,'');
					}
					htSale.render();
					htFree.render();
					//SPAdjustmentOrder.orderId = result.saleOrder.id;
					//enable('btnOrderCopy');
					//enable('btnOrderReset');
				},200);
			}else{
				$('#serverErrors').html(result.errMsg).show();		
			}
			if(result.saleOrder != null && result.saleOrder != undefined) {
				$('#orderNumber').val(result.saleOrder.orderNumber);
			}
		}, 'loading2', '', false, '', function(errResult) {
			if(errResult.isRePayment) {
				enable('payment');
				$('#payment').removeClass('BtnGeneralDStyle');		
				$('#btnOrderSave').attr('disabled','disabled');
				$('#btnOrderSave').addClass('BtnGeneralDStyle');
			}
		});
	},
	viewProductPromotionDetail:function(productCode){		
		$('#GeneralInforErrors').html('').hide();
		var customerCode=$('#customerCode').val().trim();
		if(customerCode.length==0){
			$('#GeneralInforErrors').html('Vui lòng chọn mã khách hàng').show();
		}else{			
			if(productCode!=''){
				var url='/sale-product/create-sale-order/product-detail?productCode=' +productCode+'&customerCode=' +customerCode;
				$.getJSON(encodeURI(url), function(data) {
					if(!data.error){	
						CommonSearch.showProductInfo(data.product.productCode, data.product.productName, 
								data.product.convfact,
								formatQuantityEx(data.avaiableQuantity, data.product.convfact),
								data.price, data.avaiableQuantity, data.promotion);
					}else{
						$('#GeneralInforErrors').html(data.errMsg).show();
					}					
				});				
			}
		}
	},
	
	showPromotionProgramDetail:function(code){
		var html = $('#createOrderPromotionProgramDetailContainer').html();
		$('#createOrderPromotionProgramDetailContainer').show();
		$('#createOrderPromotionProgramDetail').dialog({
			closed: false,  
			cache: false,  
		    modal: true,
		    onOpen: function() {
		    	$.getJSON('/sale-product/create-sale-order/promotion-detail?ppCode=' +code, function(data) {
					if(!data.error){
						$('.easyui-dialog #createOrderPromotionProgramDetail').html('');
						$('.easyui-dialog #coPPDetailCode').html(data.program.promotionProgramCode!=null? data.program.promotionProgramCode:'');
						$('.easyui-dialog #coPPDetailName').html(data.program.promotionProgramName!=null? data.program.promotionProgramName:'');
						$('.easyui-dialog #coPPDetailType').html(data.program.type!=null? data.program.type:'');
						$('.easyui-dialog #coPPDetialFormat').html(data.program.proFormat!=null? data.program.proFormat:'');
						$('.easyui-dialog #coPPDetailFromDate').html(data.fromDate);
						$('.easyui-dialog #coPPDetailToDate').html(data.toDate);
						$('.easyui-dialog #coPPDetailDescription').html(data.program.description!=null? data.program.description:'');
					}
				});
		    },
		    onClose : function(){
		    	$('#createOrderPromotionProgramDetailContainer').html(html);
	        	$('#createOrderPromotionProgramDetailContainer').hide();
		    }
		});
	},
	
};

var GridSaleBusiness = {
	listCS : new Array(), //danh sach cthttm
	listCSCode : new Array(), //danh sach ma cthttm
	mapCS: new Map(), //map danh sach cthttm
	listSaleProduct : new Array(), //danh sach san pham
	listSaleProductCode : new Array(),//danh sach ma san pham
	mapSaleProduct : new Map(), //map danh sach san pham
	mapProductLot: null,//map lo cua san pham
	listCellMerge: new Array(),
	mapSaleProductGrid: new Map(),//map danh sach san pham ban
	isEditGridSale: false,
	styleTextStockNegative: function (instance, td, row, col, prop, value, cellProperties) {
		Handsontable.renderers.TextRenderer.apply(this, arguments);
		if(col == COLSALE.PRO_NAME || col == COLSALE.PRICE || col == COLSALE.STOCK || col == COLSALE.AMOUNT){
			var proCode = $('#gridSaleData').handsontable('getInstance').getDataAtCell(row, COLSALE.PRO_CODE);
			if(!isNullOrEmpty(proCode)){
				var pro = GridSaleBusiness.mapSaleProduct.get(proCode);
				if(pro != null){
					if(pro.availableQuantity < 0){
						td.style.color = '#FF0000';
					}else{
						var quanConv = $('#gridSaleData').handsontable('getInstance').getDataAtCell(row, COLSALE.QUANTITY);
						if(!isNullOrEmpty(quanConv)){
							var quan = StockValidateInput.getQuantity(quanConv, pro.convfact);
							if(quan > pro.availableQuantity){
								td.style.color = '#FF0000';
							}
						}
					}
				}
			}
		}
	},
	getColumnProperty: function(){
		var col = [
		           //product code
					{
						type : 'autocomplete',
						source : GridSaleBusiness.listSaleProductCode,
						strict: true,
						allowInvalid: false,
						renderer: CreateSaleOrder.productCodeRenderer
					},
					
					//product name
					{
						readOnly: true,
						renderer: GridSaleBusiness.styleTextStockNegative
					},
					
					//price
					{
						readOnly: true,
						renderer: GridSaleBusiness.styleTextStockNegative
					},
					
					//stock
					{
						readOnly: true,
						renderer: GridSaleBusiness.styleTextStockNegative
					},
					
					//quantity
					{
						//validator : CreateSaleOrder.convfactValidation,
						//allowInvalid: false
					},
					
					//amount
					{
						readOnly: true,
						renderer: GridSaleBusiness.styleTextStockNegative
					},
					
					//cthttm
					{
						type : 'autocomplete',
						source : GridSaleBusiness.listCSCode,
						//strict: true,
						//allowInvalid: false
					},
					
					//lot
					{readOnly: true}
				];
		return col;
	},
	getRowCellMerge: function(row){
		var result = new Array();
		for(var i = 0; i< GridSaleBusiness.listCellMerge.length;i++){
			var startRow = GridSaleBusiness.listCellMerge[i].row;
			var endRow = GridSaleBusiness.listCellMerge[i].rowspan + startRow - 1;
			if(row >= startRow && row <= endRow){
				while(row<=endRow){
					result.push(row++);
				}
				break;
			}
		}
		return result;
	},
	getIndexCellMerge: function(row){
		var result = new Array();
		for(var i = 0; i< GridSaleBusiness.listCellMerge.length;i++){
			if(GridSaleBusiness.listCellMerge[i].row = row){
				result.push(i);
			}
		}
		return result;
	},
	resetListCellMerge: function(row, numRowDel){
		for(var i = 0; i< GridSaleBusiness.listCellMerge.length;i++){
			var r = GridSaleBusiness.listCellMerge[i].row;
			if(r > row + numRowDel - 1){
				GridSaleBusiness.listCellMerge[i].row = r - numRowDel;
			}
		}
	},
	
	/**
	 * Khoi tao grid san pham ban
	 */
	initSaleGrid: function() {
		//create  handsontable
		$('#gridSaleData').handsontable({
			data: [],
			colWidths: [40, 75, 35, 40, 40, 50, 55],
			outsideClickDeselects: true,
			multiSelect : false,
			fillHandle: true,
            cells: function (row, col, prop) {
				/*var cellProperties = {};
				cellProperties.renderer = CreateSaleOrder.firstColStyle;
				return cellProperties;*/
			},
			colHeaders: ['Mã sản phẩm', 'Tên sản phẩm', 'Đơn giá', 'Tồn kho đáp ứng', 'Thực đặt', 'Thành tiền', 'CT HTTM', 'Số lô(Tồn kho đáp ứng)'],
			columns: GridSaleBusiness.getColumnProperty(),
			afterChange: function(changes, source) {
				if(changes != null && $.isArray(changes)) {
					var htSale = $('#gridSaleData').handsontable('getInstance');
					for(var i = 0; i < changes.length; i++) {
						var row = changes[i];
						var r = row[0];
						var c = row[1];
						var oldValue = row[2];
						var newValue = row[3];
						//Tu dong fill ten sp, don gia, ton kho dap ung, ctkm khi chon sp
						if(newValue != oldValue){
							switch(c){
							case COLSALE.PRO_CODE:
								var oldProduct = GridSaleBusiness.mapSaleProduct.get(oldValue);
								var newProduct = GridSaleBusiness.mapSaleProduct.get(newValue);
								var isSelect = true;//htSale.getSelected()[0] == r;
								if(newProduct != null & isSelect) {
									//check lot
									var isLot = $('#fsxn').val();
									if(newProduct.checkLot == 1 && isLot == 0 && false){ //truong hop co lo
										/**
										 * doi voi truong hop product chon la co lo thi phai set cac truong hop sau day:
										 * -product moi co lo va product cu ko co lo
										 * 	+ de tranh truong hop chen du lieu, phai remove di may cai lot row cua product cu -> them row moi dang sau product row moi
										 * -product moi co lo va product cu co lo
										 */
										var lstLot = GridSaleBusiness.mapProductLot[newProduct.productCode];
										if(lstLot != undefined && lstLot != null && lstLot.length > 0){
											if(oldProduct != null){
											}
											//fill data lot
											var sizeLot = lstLot.length;
											var sizeRemain = htSale.countRows() - r;
											if(sizeLot > sizeRemain){
												htSale.alter('insert_row',null,sizeLot - sizeRemain);
											}
											for(var i = 0; i<sizeLot; i++){
												if(i<sizeLot-1){
													htSale.setDataAtCell(r+i+1 , 0, newProduct.productCode);
												}
												htSale.setDataAtCell(r+i, 1, newProduct.productName);
												htSale.setDataAtCell(r+i, 2, formatCurrency(newProduct.price));
												htSale.setDataAtCell(r+i, 3, StockValidateInput.formatStockQuantity(newProduct.availableQuantity, newProduct.convfact));
												if(newProduct.promotionProgramCode != null){
													htSale.setDataAtCell(r+i, 6, newProduct.promotionProgramCode);
												}
												var strLot = lstLot[i].lot + "(" + StockValidateInput.formatStockQuantity(lstLot[i].availableQuantity, newProduct.convfact) + ")";
												htSale.setDataAtCell(r+i, 7, strLot);
											}
											//merge cell
											var mergeCellOb = [{row: r, col: 0, rowspan: sizeLot, colspan: 1},
											                   {row: r, col: 1, rowspan: sizeLot, colspan: 1},
											                   {row: r, col: 2, rowspan: sizeLot, colspan: 1},
											                   {row: r, col: 3, rowspan: sizeLot, colspan: 1}];
											var lstTemp  = GridSaleBusiness.listCellMerge;
											GridSaleBusiness.listCellMerge = lstTemp.concat(mergeCellOb);
											htSale.updateSettings({mergeCells: GridSaleBusiness.listCellMerge});
											//disable cell product code
											htSale.getCellMeta(r,c).readOnly = true;
											var index = GridSaleBusiness.listSaleProductCode.indexOf(newProduct.productCode); 
											GridSaleBusiness.listSaleProductCode.splice(index, 1);
											htSale.render();
										}else{
											htSale.alter('remove_row',r);
											$('#saleInputErr').html("Tất cả các lô sản phẩm đã hết hạn!").show();
											setTimeout(function(){
												$('#saleInputErr').hide();
											}, 5000);
										}
									}else{// truong hop khong co lo
										//fill data
										htSale.setDataAtCell(r, COLSALE.PRO_NAME, newProduct.productName);
										htSale.setDataAtCell(r, COLSALE.PRICE, formatCurrency(newProduct.price));
										htSale.setDataAtCell(r, COLSALE.STOCK, StockValidateInput.formatStockQuantity(newProduct.availableQuantity, newProduct.convfact));
										if(htSale.getDataAtCell(r, COLSALE.QUANTITY) != null){
											htSale.setDataAtCell(r, COLSALE.QUANTITY, null);
										}
										if(htSale.getDataAtCell(r, COLSALE.PROGRAM) != null){
											htSale.setDataAtCell(r, COLSALE.PROGRAM, null);
										}
										if(newProduct.promotionProgramCode != null){
											htSale.setDataAtCell(r, COLSALE.PROGRAM, newProduct.promotionProgramCode);
										}
										htSale.selectCell(r, COLSALE.QUANTITY);
										//htSale.render();
									}
									
								}
								break;
							case COLSALE.QUANTITY:
								if(newValue != null){
									//htSale.selectCell(r, COLSALE.PROGRAM);
									GridSaleBusiness.fillSaleTotalAuto();
								}
								break;
							case COLSALE.PROGRAM:
								if(htSale.getDataAtCell(r, COLSALE.QUANTITY) != null){
									htSale.selectCell(r+1, COLSALE.PRO_CODE);
								}
								GridSaleBusiness.fillSaleTotalAuto();
							}
							enable('payment');
							disabled('btnOrderSave');
						}
					}
				}
			},
			onBeforeChange: function(changes, source) {
				if(changes != null && $.isArray(changes)) {
					for(var i = 0; i < changes.length; i++) {
						var htSale = $('#gridSaleData').handsontable('getInstance');
						var row = changes[i];
						var r = row[0];
						var c = row[1];
						var oldValue = row[2];
						var newValue = row[3];
						var productCode = htSale.getDataAtCell(r, COLSALE.PRO_CODE);
						switch(c){
						case COLSALE.PRO_CODE:
							break;
						case COLSALE.QUANTITY: //Chuyen doi quantity -> quantity convaft va fill amount
							if(productCode != null){
								var product = GridSaleBusiness.mapSaleProduct.get(productCode.trim());
								if(product != null) {
									if(newValue != null){
										newConvfactQuantity = StockValidateInput.formatStockQuantity(newValue, product.convfact);
										newValue = StockValidateInput.getQuantity(newConvfactQuantity, product.convfact);
										var amount = newValue * product.price;
										var cs = GridSaleBusiness.mapCS.get(htSale.getDataAtCell(r,COLSALE.PROGRAM));
										if(cs == null){
											htSale.setDataAtCell(r, COLSALE.AMOUNT, formatCurrency(amount));
										}else{
											htSale.setDataAtCell(r, COLSALE.AMOUNT,0);
										}
										row[3] = newConvfactQuantity;
									}else{
										htSale.setDataAtCell(r, COLSALE.AMOUNT, null);
										row[3] = null;
									}
								}else{
									htSale.selectCell(r,COLSALE.PRO_CODE);
									return false;
								}
							}else{
								htSale.selectCell(r,COLSALE.PRO_CODE);
								return false;
							}
							break;
						case COLSALE.PROGRAM:
							if(productCode == null){
								htSale.selectCell(r,COLSALE.PRO_CODE);
								return false;
							}else{
								var product = GridSaleBusiness.mapSaleProduct.get(productCode.trim());
								if(product == null ) {
									htSale.selectCell(r,COLSALE.PRO_CODE);
									return false;
								}else{
									var cs = GridSaleBusiness.mapCS.get(newValue);
									if(cs == null){
										row[3] = product.promotionProgramCode;
									}else{
										htSale.setDataAtCell(r, COLSALE.AMOUNT,0);
									}
								}
							}
						}
						GridSaleBusiness.isEditGridSale = false;
					}
				}
			},
			beforeKeyDown: function(event){
				var htSale = $('#gridSaleData').handsontable('getInstance');
				var rowSelect = htSale.getSelected();
				if(rowSelect != undefined && rowSelect != null){
					var r = rowSelect[0];
					var c = rowSelect[1];
					
					if(event.keyCode == keyCodes.DELETE){ //event delete row
						event.stopImmediatePropagation();
						if(rowSelect != null){
				    		var proCode = htSale.getDataAtCell(r,0);
				    		var pro = GridSaleBusiness.mapSaleProduct.get(proCode);
				    		if(pro!=null){
				    			htSale.deselectCell();
				    			$.messager.confirm('Xác nhận','Bạn có muốn xóa sản phẩm ' + Utils.XSSEncode(pro.productCode) +' này?',function(is){  
								    if (is){
								    	var isLot = $('#fsxn').val();
								    	if(pro.checkLot == 1 && isLot == 0 && false){
								    		var lstIndexMerge = GridSaleBusiness.getIndexCellMerge(r);
								    		var lstRow = GridSaleBusiness.getRowCellMerge(r);
								    		if(lstIndexMerge.length > 0){
								    			GridSaleBusiness.listCellMerge.splice(lstIndexMerge[0],lstIndexMerge.length);
								    		}
								    		GridSaleBusiness.resetListCellMerge(r,lstRow.length);
								    		for(var i = 0; i< lstRow.length; i++){
								    			htSale.alter('remove_row',lstRow[0]);
								    		}
								    	}else{
								    		htSale.alter('remove_row',r);
								    		//GridSaleBusiness.resetListCellMerge(r,1);
								    	}
								    	/*GridSaleBusiness.listSaleProductCode.push(pro.productCode);
								    	GridSaleBusiness.listSaleProductCode.sort();*/
								    	GridSaleBusiness.fillSaleTotalAuto();
								    	htSale.selectCell(0,0);
								    	htSale.render();
								    	enable('payment');
										disabled('btnOrderSave');
								    }else{
								    	htSale.selectCell(r,c);
								    }
								});
				    		}
				    	}
					}
					
					if(c == COLSALE.PRO_CODE){
						if(event.keyCode == keyCodes.TAB){
							event.stopImmediatePropagation();
							if(event.shiftKey){
								if(r == 0){
									$('#priorityId').focus();
									htSale.deselectCell();
								}else{
									htSale.selectCell(r-1,COLSALE.PROGRAM);
								}
							}else{
								if(r < htSale.countRows()){
									htSale.selectCell(r,COLSALE.QUANTITY);
								}
							}
							event.preventDefault();
						}
					}
					
					if(c == COLSALE.QUANTITY){
						if(event.keyCode == keyCodes.TAB){
							event.stopImmediatePropagation();
							if(!event.shiftKey){
								if(r < htSale.countRows()){
									htSale.selectCell(r,COLSALE.PROGRAM);
								}
							}else{
								htSale.selectCell(r,COLSALE.PRO_CODE);
							}
							event.preventDefault();
						}
					}
					
					if(c == COLSALE.PROGRAM){
						if(event.keyCode == keyCodes.TAB){
							event.stopImmediatePropagation();
							if(!event.shiftKey){
								if(r < htSale.countRows() - 1){
									htSale.selectCell(r+1,COLSALE.PRO_CODE);
								}
							}else{
								htSale.selectCell(r,COLSALE.QUANTITY);
							}
							event.preventDefault();
						}
					}
				}
			}
		});
		$('.handsontableInputHolder .handsontableInput').live('input',function(){
		    GridSaleBusiness.isEditGridSale = true;
		});
	},
	
	/**
	 * reset grid them moi san pham ban
	 */
	resetSaleGrid: function() {
		GridSaleBusiness.listCellMerge = new Array();
		GridSaleBusiness.mapSaleProductGrid = new Map();
		GridSaleBusiness.isEditGridSale = false;
		var data = new Array();
		for(var i = 0; i < 1; i++) {
			var row = new Array();
			for(var j = 0; j < 7; j++) {
				row.push(null);
			}
			data.push(row);
		}
		var hsSale = $('#gridSaleData').handsontable('getInstance');
		hsSale.updateSettings({data: data, columns: GridSaleBusiness.getColumnProperty(), minSpareRows: 1});
		//hsSale.selectCell(0,0);
		$('#totalWeightSale').html(0);
		$('#totalAmountSale').html(0);
		$('#totalWeight').html(0);
		$('#totalAmount').html(0);
	},
	
	disableSaleGrid: function(){
		/*GridSaleBusiness.listCellMerge = new Array();
		GridSaleBusiness.mapSaleProductGrid = new Map();
		GridSaleBusiness.isEditGridSale = false;*/
		var hsSale = $('#gridSaleData').handsontable('getInstance');
		hsSale.updateSettings({data: []});
		$('#totalWeightSale').html(0);
		$('#totalAmountSale').html(0);
		$('#totalWeight').html(0);
		$('#totalAmount').html(0);
		$('#gridSaleContainer').css('height', 70);
	},
	
	/**
	 * tinh tong trong luong va tong tinh cua danh sach san pham ban
	 */
	fillSaleTotalAuto: function(){
		var htSale = $('#gridSaleData').handsontable('getInstance');
		var quantityCol = htSale.getDataAtCol(COLSALE.QUANTITY);
		var totalWeight = 0;
		var totalAmount = 0;
		for(var i = 0;i<quantityCol.length;i++){
			if(quantityCol[i] != null){
				var product = GridSaleBusiness.mapSaleProduct.get(htSale.getDataAtCell(i,COLSALE.PRO_CODE));
				if(product != null){
					var quantity = StockValidateInput.getQuantity(quantityCol[i],product.convfact);
					var promotionCode = htSale.getDataAtCell(i,COLSALE.PROGRAM);
					if(isNullOrEmpty(promotionCode) || GridSaleBusiness.mapCS.get(promotionCode) == null){
						totalAmount = totalAmount + quantity*product.price;
					}
					totalWeight = totalWeight + quantity*product.grossWeight;
				}
			}
		}
		$('#totalWeightSale').html(formatFloatValue(totalWeight,2));
		$('#totalAmountSale').html(formatCurrency(totalAmount));
	},
};

var GridFreeBusiness = {
		listFreeProductAuto : new Map(),//danh sach record khuyen mai
		mapChangePPromotionZV01_18 : new Map(),//map danh sach san pham doi cua khuyen mai sp zv01-18
		mapChangePPromotionZV21 : new Map(),//map danh sach san pham doi cua khuyen mai zv21
		listPromotionProductForOrder : new Map(),
		programes: new Array(),//danh sach chuong trinh khuyen mai don hang co the thay doi
		isEnterChangePromotion: true,
		promotionType : {
				FREE_PRODUCT :1,
				FREE_PRICE : 2,
				FREE_PERCENT:3,
				PROMOTION_FOR_ORDER: 4,
				PROMOTION_FOR_ORDER_TYPE_PRODUCT: 5,
				PROMOTION_FOR_ZV21: 6
		},
		
		promotionCodeRenderer: function (instance, td, row, col, prop, value, cellProperties) {
			var html = '';
			if(value != null){
				html = '<a onclick="return CreateSaleOrder.showPromotionProgramDetail(\'' +value+'\');" href="javascript:void(0)">' +value+'</a>';
			}
			td.innerHTML = html;
		},
		
		styleTextStockNegative: function (instance, td, row, col, prop, value, cellProperties) {
			Handsontable.renderers.TextRenderer.apply(this, arguments);
			if(col == COLFREE.PRO_NAME || col == COLFREE.STOCK || col == COLFREE.MAX || col == COLFREE.PROGRAM_TYPE){
				var htFree = $('#gridFreeData').handsontable('getInstance');
				var pp = GridFreeBusiness.listFreeProductAuto.get(htFree.getDataAtCell(row,COLFREE.PROGRAM)+'-' +htFree.getDataAtCell(row,COLFREE.PRO_CODE));
				if(pp != null && pp.type == GridFreeBusiness.promotionType.FREE_PRODUCT){
					if(pp.stock < 0){
						td.style.color = '#FF0000';
					}else{
						if(pp.quantity > pp.stock){
							td.style.color = '#FF0000';
						}
					}
				}
			}
		},
		
		/**
		 * khoi tao grid khuyen mai
		 */
		initFreeGrid: function() {
			$('#gridFreeData').handsontable({
				data: [],
				colWidths: [40, 75, 40, 40, 40, 30, 30, 40, 40, 40],
				outsideClickDeselects: true,
				multiSelect : false,
				columns: [
				          	//product code
							{
								readOnly: true,
								renderer: CreateSaleOrder.productCodeRenderer
							},
							
							//product name
							{
								readOnly: true,
								renderer: GridFreeBusiness.styleTextStockNegative
							},
							
							//stock avaliable
							{
								readOnly: true,
								renderer: GridFreeBusiness.styleTextStockNegative
							},
							
							//quantity
							{allowInvalid: false},
							
							//max
							{
								readOnly: true,
								renderer: GridFreeBusiness.styleTextStockNegative
							},
							
							//discount percent
							{readOnly: true},
							
							//lot
							{readOnly: true},
							
							//program type
							{
								readOnly: true,
								renderer: GridFreeBusiness.styleTextStockNegative
							},
							
							//program code
							{
								readOnly: true,
								renderer: GridFreeBusiness.promotionCodeRenderer
							},
							
							//change program
							{readOnly: true, renderer: 'html'}
						],
				colHeaders: ['Mã sản phẩm', 'Tên sản phẩm', 'Tồn kho đáp ứng', 'Số lượng', 'Max', 'Chiết khấu', 'Chọn lô', 'Loại khuyến mại', 'Khuyến mại','Đổi KM'],
				afterChange: function(changes, source) {
					htFree = $('#gridFreeData').handsontable('getInstance');
					if(changes != null && $.isArray(changes)) {
						for(var i = 0; i < changes.length; i++) {
							var row = changes[i];
							var r = row[0];
							var c = row[1];
							var oldValue = row[2];
							var newValue = row[3];
							switch(c){
							 case COLFREE.QUANTITY:
								 if(oldValue != undefined && oldValue != null){//kiem tra khong phai lan set value dau tien
									 var pp = GridFreeBusiness.listFreeProductAuto.get(htFree.getDataAtCell(r,COLFREE.PROGRAM)+'-' +htFree.getDataAtCell(r,COLFREE.PRO_CODE));
									 if(pp != null){
										 if(pp.promotionType == GridFreeBusiness.promotionType.PROMOTION_FOR_ORDER
											|| pp.type == GridFreeBusiness.promotionType.FREE_PRICE 
											|| pp.type == GridFreeBusiness.promotionType.FREE_PERCENT){
											pp.discountAmount = newValue;
										 }else{
											if(pp.promotionType != GridFreeBusiness.promotionType.PROMOTION_FOR_ZV21 
													&& (pp.promotionType == GridFreeBusiness.promotionType.PROMOTION_FOR_ORDER_TYPE_PRODUCT 
															|| pp.type == GridFreeBusiness.promotionType.FREE_PRODUCT)){
												pp.quantity = StockValidateInput.getQuantity(newValue, pp.convfact);
											}
										}
									 }
									 GridFreeBusiness.fillFreeTotalAuto();
								 }
							}
						}
					}
				},
				onBeforeChange: function(changes, source) {
					if(changes != null && $.isArray(changes)) {
						for(var i = 0; i < changes.length; i++) {
							var htFree = $('#gridFreeData').handsontable('getInstance');
							var row = changes[i];
							var r = row[0];
							var c = row[1];
							var oldValue = row[2];
							var newValue = row[3];
							switch(c){
							case COLFREE.QUANTITY://chuyen doi quantity -> quantity convfact khi nhap so luong san pham
								var pro_code = htFree.getDataAtCell(r,COLFREE.PRO_CODE);
								var promo_code = htFree.getDataAtCell(r,COLFREE.PROGRAM);
								if(pro_code != null && promo_code != null){
									var pp = GridFreeBusiness.listFreeProductAuto.get(promo_code+'-' +pro_code);
									if(pp != null && pp.type == GridFreeBusiness.promotionType.FREE_PRODUCT){
										newConvfactQuantity = StockValidateInput.formatStockQuantity(newValue, pp.convfact);
										row[3] = newConvfactQuantity;
									}else{
										if(pp.type == GridFreeBusiness.promotionType.FREE_PERCENT || pp.type == GridFreeBusiness.promotionType.FREE_PRICE){
											if(newValue < 0){
												row[3] = 0;
											}
										}
									}
								}
							}
						}
					}
				},
				beforeKeyDown: function(event){
					var htFree = $('#gridFreeData').handsontable('getInstance');
					var selected = htFree.getSelected();
					if(selected != undefined && selected != null){
					var r = selected[0];
					var c = selected[1];
						if(event.keyCode == keyCodes.ENTER){
							// doi khuyen mai
							if(c == COLFREE.PROGRAM_CHANGE && htFree.getDataAtCell(r,c) != null){
								event.stopImmediatePropagation();
								GridFreeBusiness.changePromotion();
								//event.preventDefault();
							}
						}
					}
				}
				
			});
		},
		
		/**
		 * reset grid khuyen mai
		 */
		resetFreeGrid: function() {
			GridFreeBusiness.listFreeProductAuto = new Map();
			GridFreeBusiness.mapChangePPromotionZV01_18 = new Map();
			GridFreeBusiness.mapChangePPromotionZV21 = new Map();
			GridFreeBusiness.listPromotionProductForOrder = new Map();
			GridFreeBusiness.programes = new Array();
			var htFree = $('#gridFreeData').handsontable('getInstance');
			htFree.updateSettings({data: []});
			$('#totalWeightFree').html(0);
			$('#totalAmountFree').html(0);
			$('#gridFreeContainer').css('height', 70);
		},
		
		/**
		 * Covert doi tuong khuyen mai tu action thanh doi tuong khuyen mai tren grid khuyen mai
		 */
		getObjectPromotionProduct:function(pp){
			var pproduct = {};
			if(pp.promoProduct==null || pp.promoProduct==undefined){
				pproduct.productCode = '';
				pproduct.productName = '';
			}else{
				pproduct.productCode = pp.promoProduct.productCode;
				pproduct.productName = pp.promoProduct.productName;
			}	
			if(pp.saleOrderDetail==null || pp.saleOrderDetail==undefined){
				pproduct.programCode = '';									
				pproduct.discountPercent = 0;
				pproduct.discountAmount=0;
			}else{
				pproduct.programCode = pp.saleOrderDetail.programCode;	
				var promoProduct = pp.promoProduct;
				if(promoProduct!=null && promoProduct!=undefined){
					pproduct.convfact = pp.promoProduct.convfact;
					pproduct.checkLot = pp.promoProduct.checkLot;
					pproduct.productId = pp.promoProduct.id;										
				}else{
					pproduct.convfact = '';										
				}							
				pproduct.discountPercent = pp.saleOrderDetail.discountPercent;
				pproduct.discountAmount = pp.saleOrderDetail.discountAmount;
				pproduct.maxDiscountAmount = pp.saleOrderDetail.discountAmount;
				pproduct.quantity = pp.saleOrderDetail.quantity;
				pproduct.maxQuantity = pp.saleOrderDetail.quantity;
				var stock = parseInt(pp.stock);									
				if(stock==0){
					pproduct.quantity=0;
				}
			}
			pproduct.type = pp.type;
			pproduct.key = pp.keyList;
			pproduct.changeProduct = pp.changeProduct;						
			pproduct.typeName = pp.typeName;						
			pproduct.stock = pp.stock;
			if(pp.promoProduct != null){
				pproduct.grossWeight= pp.promoProduct.grossWeight;
			}
			pproduct.promotionType = pp.promotionType;
			if(pp.promotionType==GridFreeBusiness.promotionType.PROMOTION_FOR_ZV21 || pp.promotionType==GridFreeBusiness.promotionType.PROMOTION_FOR_ORDER){
				pproduct.productName = 'Khuyến mãi đơn hàng';			
			}
			return pproduct;
		},
		
		/**
		 * Fill lai grid san pham khuyen mai voi danh sach truyen vao
		 * @param listFreeProduct
		 */
		fillDataTableFreeProduct: function(listFreeProduct){
			var htFree = $('#gridFreeData').handsontable('getInstance');
			htFree.updateSettings({data: []});
			var htFree = $('#gridFreeData').handsontable('getInstance');
			for(var i = 0; i<listFreeProduct.length; i++){
				var freePro = listFreeProduct[i];
				htFree.alter('insert_row');
				GridFreeBusiness.fillRowPPromotion(htFree,freePro,i);
			}
			htFree.render();
			htFree.selectCell(0,0);
			GridFreeBusiness.fillFreeTotalAuto();
		},
		
		/**
		 * Fill tung row trong grid
		 * @param htFree
		 * @param ppromotion
		 * @param row
		 */
		fillRowPPromotion: function(htFree, ppromotion, row){
			htFree.setDataAtCell(row,COLFREE.PRO_CODE, ppromotion.productCode);
			htFree.setDataAtCell(row,COLFREE.PRO_NAME, ppromotion.productName);
			
			//fill stock
			if(ppromotion.type == GridFreeBusiness.promotionType.FREE_PRODUCT) {	
				if(ppromotion.stock==null){
					ppromotion.stock=0;
				}
				htFree.setDataAtCell(row,COLFREE.STOCK, StockValidateInput.formatStockQuantity(ppromotion.stock, ppromotion.convfact));
			}
			
			//fill quantity
			if(ppromotion.type == GridFreeBusiness.promotionType.FREE_PRICE || ppromotion.type == GridFreeBusiness.promotionType.FREE_PERCENT) {
				var maxDiscountAmount = formatFloatValue(ppromotion.maxDiscountAmount);
				var discountAmount = Math.round(ppromotion.discountAmount);//formatFloatValue(ppromotion.discountAmount);
				var cellMeta = htFree.getCellMeta(row,COLFREE.QUANTITY);
				cellMeta.type = 'numeric';
				cellMeta.format = '0,0';
				htFree.setDataAtCell(row, COLFREE.QUANTITY, discountAmount);
				htFree.setDataAtCell(row, COLFREE.MAX, '(' +maxDiscountAmount+')' +'VNĐ');
			}else if(ppromotion.type == GridFreeBusiness.promotionType.FREE_PRODUCT && ppromotion.promotionType != 6) {	
				var maxQuantity = !isNullOrEmpty(ppromotion.maxQuantity) && ppromotion.maxQuantity != 0? StockValidateInput.formatStockQuantity(ppromotion.maxQuantity, ppromotion.convfact):'0';
				var value = !isNullOrEmpty(ppromotion.quantity) && ppromotion.quantity != 0? StockValidateInput.formatStockQuantity(ppromotion.quantity, ppromotion.convfact):'0';
				htFree.setDataAtCell(row, COLFREE.QUANTITY, value);
				htFree.setDataAtCell(row, COLFREE.MAX, maxQuantity);
			}else{
				htFree.getCellMeta(row, COLFREE.QUANTITY).readOnly = true;
			}
			if(!isAllowEditPromotion){
				htFree.getCellMeta(row,COLFREE.QUANTITY).readOnly = true;
			}
			
			//fill %
			if(ppromotion.type == GridFreeBusiness.promotionType.FREE_PERCENT) {
				var discount = formatFloatValue(ppromotion.discountPercent, 2) + '%';
				htFree.setDataAtCell(row, COLFREE.DISCOUNT, discount);
			}
			
			//fill lot
			
			//fill promotion type
			if(ppromotion.type == 2 || ppromotion.type == 3) {
				htFree.setDataAtCell(row,COLFREE.PROGRAM_TYPE,'KM Tiền');
			} else if(ppromotion.type == 1) {
				htFree.setDataAtCell(row,COLFREE.PROGRAM_TYPE,'KM sản phẩm');
			}
			
			htFree.setDataAtCell(row,COLFREE.PROGRAM,ppromotion.programCode);
			
			//fill change promotion
			if( ppromotion.changeProduct != undefined && ppromotion.changeProduct!=null 
					&& ppromotion.changeProduct!='0' && ppromotion.changeProduct!=0){
				var title = '';
				if(ppromotion.promotionType==GridFreeBusiness.promotionType.PROMOTION_FOR_ZV21 
						|| ppromotion.promotionType==GridFreeBusiness.promotionType.PROMOTION_FOR_ORDER){
					title = 'Đổi CTKM';
				}else{
					title = 'Đổi SPKM';
				}
				var value = '<a class="ppKeyStyle" href="javascript:void(0)" onclick="GridFreeBusiness.changePromotion()">' +title+'</a>';
				htFree.setDataAtCell(row,COLFREE.PROGRAM_CHANGE,value);
			}
		},
		
		/**
		 * tinh tong trong luong va tong tien cho cac don hang khuyen mai
		 */
		fillFreeTotalAuto: function(){
			var totalWeight = 0;
			var totalAmount = 0;
			var lst = GridFreeBusiness.listFreeProductAuto.valArray;
			for(var i=0;i<lst.length;i++){
				var pp = lst[i];
				if(pp != null){
					 if(pp.promotionType == GridFreeBusiness.promotionType.PROMOTION_FOR_ORDER
						|| pp.type == GridFreeBusiness.promotionType.FREE_PRICE 
						|| pp.type == GridFreeBusiness.promotionType.FREE_PERCENT){
						totalAmount += Math.round(pp.discountAmount);
					 }else{
						if(pp.promotionType != GridFreeBusiness.promotionType.PROMOTION_FOR_ZV21 
								&& (pp.promotionType == GridFreeBusiness.promotionType.PROMOTION_FOR_ORDER_TYPE_PRODUCT 
										|| pp.type == GridFreeBusiness.promotionType.FREE_PRODUCT)){
							totalWeight += pp.quantity*pp.grossWeight;
						}
					}
				 }
			}
			$('#totalWeightFree').html(formatFloatValue(totalWeight,2));
			$('#totalAmountFree').html(formatCurrency(totalAmount));
			CreateSaleOrder.fillTotalAuto();
		},
		
		/**
		 * Kiem tra khuyen mai san pham co thay doi san pham nhieu hay mot
		 * @param listChange
		 * @returns {Boolean}
		 */
		getChangeMultiProduct: function(listChange){
			var canChaneMultiProduct = true;
			var promotionProgramTypeCode = "";
			var quantityChange = 0;
			if (listChange != null && listChange.length > 0) {
				for(var i = 0; i< listChange.length; i++) {
					var sodVO = listChange[i];
					if(promotionProgramTypeCode.length == 0 && quantityChange == 0) {
						promotionProgramTypeCode = sodVO.saleOrderDetail.programeTypeCode;
						quantityChange = sodVO.saleOrderDetail.quantity;
					} else if(promotionProgramTypeCode != sodVO.saleOrderDetail.programeTypeCode
							||quantityChange != sodVO.saleOrderDetail.quantity) {
						canChaneMultiProduct = false;
					}
				}
			}
			return canChaneMultiProduct;
		},
		
		/**
		 * Ham xu ly doi khuyen mai
		 */
		changePromotion: function(){
			var htFree = $('#gridFreeData').handsontable('getInstance');
			var selected = htFree.getSelected();
			if(selected != undefined && selected != null){
				var r = selected[0];
				var c = selected[1];
				var pro_code = htFree.getDataAtCell(r,COLFREE.PRO_CODE);
				var promo_code = htFree.getDataAtCell(r, COLFREE.PROGRAM);
				htFree.deselectCell();
				if (pro_code != null && promo_code != null && GridFreeBusiness.listFreeProductAuto.get(promo_code+'-' +pro_code) != null) {
					var freePro = GridFreeBusiness.listFreeProductAuto.get(promo_code+'-' +pro_code);
					if(freePro.promotionType == GridFreeBusiness.promotionType.PROMOTION_FOR_ZV21 
							|| freePro.promotionType == GridFreeBusiness.promotionType.PROMOTION_FOR_ORDER){
						//doi ctkm
						GridFreeBusiness.showChangePromotionProgram();
					}else{
						//doi spkm
						var freeProChange = null;
						if(freePro.promotionType == GridFreeBusiness.promotionType.PROMOTION_FOR_ORDER_TYPE_PRODUCT && Number(freePro.key)>0){
							//doi spkm cua zv21
							freeProChange = GridFreeBusiness.mapChangePPromotionZV21[freePro.key];
						}else{
							//doi spkm cua zv01-18
							freeProChange = GridFreeBusiness.mapChangePPromotionZV01_18[freePro.key];
						}
						//change
						if(GridFreeBusiness.getChangeMultiProduct(freeProChange)){
							//doi nhieu sp
							GridFreeBusiness.promotionMultiProductDialog(function(lstObj){
								var mapTemp = new Map();
								var lstKey = GridFreeBusiness.listFreeProductAuto.keyArray;
								for(var i = 0; i < lstKey.length; i++){
									var proTemp = GridFreeBusiness.listFreeProductAuto.get(lstKey[i]);
									if(proTemp.programCode == freePro.programCode && proTemp.productCode == freePro.productCode){
										for(var j = 0; j < lstObj.length; j++){
											var freeProNew = lstObj[j];
											mapTemp.put(freeProNew.programCode+'-' +freeProNew.productCode,freeProNew);
										}
									}else{
										if(proTemp.programCode != freePro.programCode || proTemp.promotionType == GridFreeBusiness.promotionType.PROMOTION_FOR_ZV21){
											mapTemp.put(lstKey[i],GridFreeBusiness.listFreeProductAuto.get(lstKey[i]));
										}
									}
								}
								GridFreeBusiness.listFreeProductAuto = mapTemp;
								GridFreeBusiness.fillDataTableFreeProduct(GridFreeBusiness.listFreeProductAuto.valArray);
								$('#searchStyle5').dialog('close');
							},freeProChange);
						}else{
							//doi 1 sp
							GridFreeBusiness.promotionProductDialog(function(chooseObj){
								var mapTemp = new Map();
								var lstKey = GridFreeBusiness.listFreeProductAuto.keyArray;
								for(var i = 0; i < lstKey.length; i++){
									var proTemp = GridFreeBusiness.listFreeProductAuto.get(lstKey[i]);
									if(proTemp.programCode == freePro.programCode && proTemp.productCode == freePro.productCode){
											mapTemp.put(chooseObj.programCode+'-' +chooseObj.productCode,chooseObj);
									}else{
										if(proTemp.programCode != freePro.programCode || proTemp.promotionType == GridFreeBusiness.promotionType.PROMOTION_FOR_ZV21){
											mapTemp.put(lstKey[i],GridFreeBusiness.listFreeProductAuto.get(lstKey[i]));
										}
									}
								}
								GridFreeBusiness.listFreeProductAuto = mapTemp;
								GridFreeBusiness.fillDataTableFreeProduct(GridFreeBusiness.listFreeProductAuto.valArray);
							},freeProChange);
						}
					}
				}
			}
		},
		
		/**
		 * ham doi nhieu san pham trong khuyen mai san pham
		 * @param callback
		 * @param result
		 */
		promotionMultiProductDialog: function(callback, result) {
			CommonSearch._currentSearchCallback = callback;
			var t;
			var html = $('#searchStyle5').html();
			$('#searchStyle5').dialog({
				closed: false,
				cache: false,
				modal: true,
				onOpen: function() {
					$('#errMultiChooseProduct').hide();
					var lst = new Array();
					for(var i = 0; i < result.length; i++) {
						var obj = GridFreeBusiness.getObjectPromotionProduct(result[i]);
						if(i == 0) {
							obj.quantity = obj.maxQuantity;
						} else {
							obj.quantity = 0;
						}
						lst.push(obj);
					}
					$('.easyui-dialog#searchStyle5 #listPromotionProduct').datagrid({
						data:lst,
		        		rownumbers:true,
		        		scrollbarSize:0,
		        		fitColumns:true,
		        		width:$('#searchStyle5').width() - 15,
		        		columns:[[  
	   	        		   {field:'productCode',title:'Mã sản phẩm', align:'left',width:90, sortable:false, resizable:false},  
	   	        		   {field:'productName',title:'Tên sản phẩm', align:'left',width:210, sortable:false, resizable:false},
	   	        		   {field:'stock', title:'Kho', align:'right',width:100,sortable:false,resizable:false,
	   	        			   formatter:function(value,row,index){
	   	        				   var stock = row.stock;
	   	        				   if(stock != null && stock != undefined) {
									   return formatQuantityEx(stock,row.convfact);
	   	        				   } else {
	   	        					   return 0;
	   	        				   }
	   	        			   }
	   	        		   },
	   	        		   {field:'quantity',title:'Số lượng',width:100,sortable:false, resizable:false,
	   	        			   formatter:function(value,row,index){
	   	        				   var quantity = row.quantity;
   	        					   if(index == 0) {
   	   	        					return '<input id="multi_change_product_quantity_' +row.productId+'" value="' +formatQuantityEx(quantity,row.convfact)+'" index="' +index+'" class="Multi_Change_Product_Quantity Multi_Change_Product_Quantity_Begin" onkeypress="" onblur="" style="text-align: right; width: 50px;"/><span class="ppStyle0" style="color: rgb(255, 0, 0);float:right;">(' +formatQuantityEx(row.maxQuantity,row.convfact)+')</span>';
   	   	        				   } else if(index == lst.length - 1) {
   	   	        					return '<input id="multi_change_product_quantity_' +row.productId+'" value="' +formatQuantityEx(quantity,row.convfact)+'" index="' +index+'" class="Multi_Change_Product_Quantity Multi_Change_Product_Quantity_End" onkeypress="" onblur="" style="text-align: right; width: 50px;"/><span class="ppStyle0" style="color: rgb(255, 0, 0);float:right;">(' +formatQuantityEx(row.maxQuantity,row.convfact)+')</span>';
   	   	        				   } else {
   	   	        					return '<input id="multi_change_product_quantity_' +row.productId+'" value="' +formatQuantityEx(quantity,row.convfact)+'" index="' +index+'" class="Multi_Change_Product_Quantity" onkeypress="" onblur="" style="text-align: right; width: 50px;"/><span class="ppStyle0" style="color: rgb(255, 0, 0);float:right;">(' +formatQuantityEx(row.maxQuantity,row.convfact)+')</span>';
   	   	        				   }
	   	        			   }
	   	        		   },
	   	        		]],
	   	        		onLoadSuccess:function(){
	   	        			$('.easyui-dialog .datagrid-header-rownumber').html('STT');
	   	        			$('.Multi_Change_Product_Quantity_Begin').focus();
	   	        		}
					});
					$('.Multi_Change_Product_Quantity').live('blur',function() {
						var val = $(this).val();
						var index = $(this).attr('index');
						var convfact = $('.easyui-dialog#searchStyle5 #listPromotionProduct').datagrid('getRows')[index].convfact;
						$(this).val(formatQuantityEx(val,convfact));
					});
					$('#btnMultiChoose').show();
					$('#btnMultiChoose').bind('click', function() {
						$('#errMultiChooseProduct').hide();
						var rowSelect = $('.easyui-dialog#searchStyle5 #listPromotionProduct').datagrid('getRows');
						var listResult = new Array();
						var totalQuantity = 0;
						var totalMaxQuantity = 0;
						for(var i = 0; i < rowSelect.length; i++) {
							var rSel = rowSelect[i];
							var quantity = StockValidateInput.getQuantity($('#multi_change_product_quantity_' +rSel.productId).val(), rSel.convfact);
							if(isNullOrEmpty(quantity) || quantity == undefined || isNaN(quantity)) {
								$('#multi_change_product_quantity_' +rSel.productId).focus();
								$('#errMultiChooseProduct').html('Số lượng không hợp lệ. Vui lòng nhập lại').show();
								return;
							}
							totalQuantity += quantity;
							totalMaxQuantity = rSel.maxQuantity;
							if (quantity > 0) {
								rSel.quantity = quantity;
								rSel.canChaneMultiProduct = true;
								listResult.push(rSel);
							} 
						}
						if (totalQuantity > totalMaxQuantity) {
							$('#errMultiChooseProduct').html('Số lượng vượt quá số lượng khuyến mãi tối đa. Vui lòng nhập lại').show();
							return;
						}
						if(listResult.length == 1) {
							var rSel = listResult[0];
							rSel.canChaneMultiProduct = undefined;
							listResult = new Array();
							listResult.push(rSel);
						} else {
							for(var j = 0; j < listResult.length; j++) {
								var obj = listResult[j];
								obj.maxQuantityCk = obj.maxQuantity;
								obj.maxQuantity = obj.quantity;
							}
						}
						if(CommonSearch._currentSearchCallback != null) {
							CommonSearch._currentSearchCallback.call(this, listResult);
						}
					});
					$('.easyui-dialog td[field=quantity] input').bind('keypress', function(e) {
						$('#errMultiChooseProduct').hide();
						if(e.keyCode == keyCodes.ENTER && !GridFreeBusiness.isEnterChangePromotion) {
							var index = Number($(this).attr('index'));
							$('.easyui-dialog#searchStyle5 #listPromotionProduct').datagrid('selectRow', index);
							$('#btnMultiChoose').click();
						} else if(e.keyCode == keyCodes.ARROW_UP) {
							var index = Number($(this).attr('index'));
							var row = $('.easyui-dialog#searchStyle5 #listPromotionProduct').datagrid('getRows')[index];
							var quantity = StockValidateInput.getQuantity($(this).val(), row.convfact);
							if(quantity > row.maxQuantity) {
								$(this).focus();
								$('#errMultiChooseProduct').html('Số lượt khuyến mãi vượt quá giá trị cho phép. Vui lòng nhập lại').show();
								return;
							}
							if(!$(this).hasClass('Multi_Change_Product_Quantity_Begin')) {
								$('.Multi_Change_Product_Quantity[index=' +(index-1)+']').focus();
							}
						} else if(e.keyCode == keyCodes.ARROW_DOWN) {
							var index = Number($(this).attr('index'));
							var row = $('.easyui-dialog#searchStyle5 #listPromotionProduct').datagrid('getRows')[index];
							var quantity = StockValidateInput.getQuantity($(this).val(), row.convfact);
							if(quantity > row.maxQuantity) {
								$(this).focus();
								$('#errMultiChooseProduct').html('Số lượt khuyến mãi vượt quá giá trị cho phép. Vui lòng nhập lại').show();
								return;
							}
							if(!$(this).hasClass('Multi_Change_Product_Quantity_End')) {
								$('.Multi_Change_Product_Quantity[index=' +(index+1)+']').focus();
							}
						}
					});
					t = setTimeout(function(){
						GridFreeBusiness.isEnterChangePromotion = false;
					},100);
				},
		        onClose : function(){
		        	clearTimeout(t);
		        	GridFreeBusiness.isEnterChangePromotion = true;
		        	$('#gridFreeData').handsontable('getInstance').selectCell(0,0);
		        	$('#searchStyle5').html(html);
		        }
			});
		},
		
		/**
		 * Ham doi chon san pham cho khuyen mai san pham
		 * @param callback
		 * @param result
		 */
		promotionProductDialog:function(callback,result){
			CommonSearch._currentSearchCallback = callback;
			var html = $('#searchStyle5').html();
			$('#searchStyle5').dialog({
				closed: false,  
		        cache: false,  
		        modal: true, 
		        onOpen: function() {
		        	var lst = new Array();
		        	for(var i=0;i<result.length;i++){
		        		var obj = new Object();
		        		obj = GridFreeBusiness.getObjectPromotionProduct(result[i]);
		        		obj.quantity = obj.maxQuantity;
		        		lst.push(obj);
					}
		        	$('.easyui-dialog#searchStyle5 #listPromotionProduct').datagrid({
		        		data:lst,
		        		rownumbers:true,
		        		singleSelect: true,	
		        		scrollbarSize:0,
		        		width:$('#searchStyle5').width() - 15,
		        		columns:[[  
		        		   {field:'productCode',title:'Mã sản phẩm', align:'left',width:120, sortable:false, resizable:false},  
		        		   {field:'productName',title:'Tên sản phẩm', align:'left',width:250, sortable:false, resizable:false},
		        		   {field:'quantity',title:'Số lượng',width:100,align:'right', sortable:false, resizable:false,
		        			   formatter:function(value,row,index){
		        				   return formatQuantityEx(value,row.convfact);
		        			   }},
		        		   {field:'ctkm',title:'Chọn',width:80,align:'center', sortable:false, resizable:false, 
		        			   formatter:function(value, row, index) {
		        				   return "<a href='javascript:void(0)' onclick=\"return GridFreeBusiness.getResultPromotionProduct(" + index+ ");\">Chọn</a>";
		        		   }},
		        		]],
		        		onLoadSuccess:function(){
		        			$('.easyui-dialog .datagrid-header-rownumber').html('STT');
		        		}
		        	});
		        },
		        onClose : function(){
		        	$('#gridFreeData').handsontable('getInstance').selectCell(0,0);
		        	$('#searchStyle5').html(html);
		        }
			});
		},
		
		/**
		 * Action chon san pham trong popup doi chon san pham km
		 * @param rowId
		 */
		getResultPromotionProduct:function(rowId){
			var obj = $('.easyui-dialog#searchStyle5 #listPromotionProduct').datagrid('getRows')[rowId];
			if (CommonSearch._currentSearchCallback != null) {
				CommonSearch._currentSearchCallback.call(this, obj);
				$('#searchStyle5').dialog('close');
			}		
		},
		
		/**
		 * Doi ctkm don hang
		 */
		showChangePromotionProgram:function(){
			var html = $('#paymentChangePPContainer').html();
			$('#paymentChangePPContainer').show();
			$('#paymentChangePP').dialog({
				closed: false, 
				cache: false,
				modal: true,
				onOpen: function() {
					$('.easyui-dialog #paymentChangePPGrids').datagrid({
						data:GridFreeBusiness.programes,
						autoWidth: true,
						width: $('#paymentChangePP').width()-50,
						columns:[[
						     {field:'promotionProgramCode', title: 'Mã CT', width: 100, sortable:false,resizable:true , align: 'left'},
							 {field:'promotionProgramName', title: 'Tên CT', width:$('#paymentChangePP').width()-235,sortable:false,resizable:true , align: 'left'},
							 {field:'select', title: 'Chọn', width:80,sortable:false,resizable:true , align: 'left',formatter:function(value,row,index){
								 return '<a href="javascript:void(0)" onclick="GridFreeBusiness.selectPromotionProgram(' +index+')">Chọn </a>';
							 }},
						]]
					});
				},
				onClose : function(){
					$('#gridFreeData').handsontable('getInstance').selectCell(0,0);
					$('#paymentChangePPContainer').html(html);
					$('#paymentChangePPContainer').hide();
				}
			});
		},
		
		/**
		 * Ham chon ctkm doi
		 * @param index
		 */
		selectPromotionProgram:function(index){
			var row = $('.easyui-dialog #paymentChangePPGrids').datagrid('getRows')[index];
			var ppType = row.type;
			
			/** reset map */
			var zPPAutoSize = GridFreeBusiness.listFreeProductAuto.size();
			var keyArray = GridFreeBusiness.listFreeProductAuto.keyArray;
			var keyRemove = new Array();
			var PPMAPTEMP = new Map();
			PPMAPTEMP = GridFreeBusiness.listFreeProductAuto;
			for(var i=0;i<zPPAutoSize;++i){
				var key = keyArray[i];
				var record = PPMAPTEMP.get(key);
				if(record!=null && record != undefined){
					if(record.promotionType==GridFreeBusiness.promotionType.PROMOTION_FOR_ZV21 
							|| record.promotionType==GridFreeBusiness.promotionType.PROMOTION_FOR_ORDER
							|| record.promotionType==GridFreeBusiness.promotionType.PROMOTION_FOR_ORDER_TYPE_PRODUCT){
						keyRemove.push(key);
					}
				}			
			}
			for(var i = 0;i<keyRemove.length;i++){
				GridFreeBusiness.listFreeProductAuto.remove(keyRemove[i]);
			}
			
			if(ppType=='ZV21'){			
				var ZV21 = GridFreeBusiness.listPromotionProductForOrder.get('zV21');
				if(ZV21!=null){			
					var ppZV21 = GridFreeBusiness.getObjectPromotionProduct(ZV21);
					ppZV21.changeProduct = 1;
					ppZV21.key = -1;								
					GridFreeBusiness.listFreeProductAuto.put(ZV21.programCode+'-' +ZV21.productCode,ppZV21);
					for(var i=0;i<ZV21.listPromotionForPromo21.length;++i){
						var pproduct = new Object();
						pproduct = GridFreeBusiness.getObjectPromotionProduct(ZV21.listPromotionForPromo21[i]);
						pproduct.quantity = pproduct.maxQuantity;
						GridFreeBusiness.listFreeProductAuto.put(Utils.XSSEncode(pproduct.programCode+'-' +pproduct.productCode),pproduct);
					}
				}
			}else if(ppType=='ZV19' || ppType=='ZV20'){
				var ZV1920 = GridFreeBusiness.listPromotionProductForOrder.get('zV19zV20');
				for(var i=0;i<ZV1920.length;++i){
					var obj = GridFreeBusiness.getObjectPromotionProduct(ZV1920[i]);
					obj.changeProduct = 1;
					obj.key = -2;
					if(obj.promotionType==GridFreeBusiness.promotionType.PROMOTION_FOR_ORDER && obj.type==PromotionType.FREE_PRICE && ppType=='ZV20'){
						GridFreeBusiness.listFreeProductAuto.put(Utils.XSSEncode(obj.programCode+'-' +obj.productCode),obj);
					}else if(obj.promotionType==PromotionType.PROMOTION_FOR_ORDER && obj.type==PromotionType.FREE_PERCENT && ppType=='ZV19'){					
						GridFreeBusiness.listFreeProductAuto.put(Utils.XSSEncode(obj.programCode+'-' +obj.productCode),obj);
					}				
				}			
			}
			GridFreeBusiness.fillDataTableFreeProduct(GridFreeBusiness.listFreeProductAuto.valArray);
			$('.easyui-dialog').dialog('close');
		},
};
var COLSALE = {
	PRO_CODE: 0, PRO_NAME: 1, PRICE: 2, STOCK: 3, QUANTITY: 4, AMOUNT: 5, PROGRAM: 6, LOT: 7	
};
var COLFREE = {
	PRO_CODE: 0, PRO_NAME: 1, STOCK: 2, QUANTITY: 3, MAX: 4, DISCOUNT: 5, LOT: 6, PROGRAM_TYPE: 7, PROGRAM: 8, PROGRAM_CHANGE: 9	
};
/**
 * tìm kiếm bán hàng
 */
var SPSearchSale = {
		_lstOrder : null,
		_isLoad : true,
		pageLoad:function() {			
			$('#orderNumber').focus();
			applyDateTimePicker("#fromDate");
			applyDateTimePicker("#toDate");
			applyDateTimePicker("#deliveryDate");
			// $('.MySelectBoxClass').customStyle();	
			$('#customerCode').bind('keyup', function(event) {					
				if (event.keyCode == keyCode_F9) {
					VCommonJS.showDialogSearch2({
					    inputs : [
					        {id:'code', maxlength:50, label:'Mã KH'},
					        {id:'name', maxlength:250, label:'Tên KH'},
					        {id:'address', maxlength:250, label:'Địa chỉ', width:410}
					    ],
					    params:{shopCode: $('#shopCodeCB').combobox('getValue')},
					    url : '/commons/customer-in-shop/search',
					    onShow : function() {
					    	var tabindex = -1;
				        	$('.InputTextStyle, select, input, button , li , tr, td, th, label, ul, a, img').each(function () {
								if (this.type != 'hidden') {
									$(this).attr("tabindex", tabindex);
									tabindex -=1;
								}
							});	        		        	
				        	$('.easyui-dialog #seachStyle1Code').focus();
							$('.easyui-dialog #seachStyle1AddressLabel').show();
							$('.easyui-dialog #seachStyle1Address').show();
							$('.easyui-dialog #searchStyle1Grid').show();
							$('.easyui-dialog #btnSearchStyle1').css('margin-left', 270);
							$('.easyui-dialog #btnSearchStyle1').css('margin-top', 5);
							$('.easyui-dialog #seachStyle1CodeLabel').html('Mã KH');
							$('.easyui-dialog #seachStyle1NameLabel').html('Tên KH');
							
							Utils.bindAutoSearch();
							$('.easyui-dialog #btnSearchStyle1').bind('click',function(event) {
								if(!$(this).is(':hidden')){
									var code = $('.easyui-dialog #seachStyle1Code').val().trim();
									var name = $('.easyui-dialog #seachStyle1Name').val().trim();
									var address = $('.easyui-dialog #seachStyle1Address').val().trim();
									$('.easyui-dialog #searchStyle1Grid').datagrid('load',{code :code, name: name,address:address});
									var records = $('.easyui-dialog #searchStyle1Grid').datagrid('getRows').length;
									$('.easyui-dialog #seachStyle1Code').focus();
									if (records != null && records == 1 && !isNullOrEmpty(code)) {
										$('#custID').val($('.easyui-dialog #searchStyle1Grid').datagrid('getRows')[0].shortCode);
										$('#custName').val($('.easyui-dialog #searchStyle1Grid').datagrid('getRows')[0].customerName);
										$('#searchStyle1EasyUIDialogDiv').css("visibility", "hidden");					
										$('.easyui-dialog').dialog('close');
										$('#custID').change();
									}
								}					
							});
					    },
					    columns : [[
					        {field:'shortCode', title:'Mã KH', align:'left', width: 110, sortable:false, resizable:false, formatter : function(v,r,i) {
					        	return Utils.XSSEncode(v);
					        }},
					        {field:'customerName', title:'Tên KH', align:'left', width: 150, sortable:false, resizable:false, formatter : function(v,r,i) {
					        	return Utils.XSSEncode(v);
					        }},
					        {field:'address', title:'Địa chỉ', align:'left', width: 170, sortable:false, resizable:false, formatter : function(v,r,i) {
					        	return Utils.XSSEncode(v);
					        }},
					        {field:'choose', title:'', align:'center', width:40, sortable:false, resizable:false, formatter:function(value,row,index) {
					        	return "<a href='javascript:void(0)' onclick=\"return SPCreateOrder.getResultSeachStyle1EasyUIDialog("+ index + ");\">Chọn</a>";        
					        }}
					    ]]
					});
				} 
			});	
			Utils.bindAutoSearch();
			Utils.bindComboboxStaffEasyUI('saleStaff');
			Utils.bindComboboxStaffEasyUI('deliveryStaff');
		},
		closeOrderView:function(url){
			if(url != undefined && url != null && url != ''){
				window.location.href = url;
			}else{
				window.location.href='/sale-product/search-sale/info';
			}
		},


		/**
		 * Ham xem thong tin sp
		 * @author: nhanlt6
		 */
		viewProductDetails:function(rowId){
			var obj = GridSaleBusiness.mapSaleProduct.get(rowId);
			$('#GeneralInforErrors').html('').hide();
			var shopCodeFilter = "";
			if($('#shopCodeCB').length == 1) {
				shopCodeFilter = $('#shopCodeCB').combobox('getValue');
			}
			var customerCode=$('#custCode').val().trim();
			var staffCode = $('#staffCode').val().trim();
			if(staffCode.length==0){
				$('#GeneralInforErrors').html('Vui lòng chọn mã NVBH').show();
				return false;
			}
			if(customerCode.length==0){
				$('#GeneralInforErrors').html('Vui lòng chọn mã khách hàng').show();
				return false;
			}else{
				var productCode = obj.productCode; 
				if(productCode!=''){
					var url = '/commons/products-details?productCode='+productCode+'&customerCode='+customerCode+'&salerCode='+staffCode;
					if (SPAdjustmentOrder._orderId) {
						url = url + "&orderId="+ SPAdjustmentOrder._orderId;
					}
					if(shopCodeFilter.length > 0){
						url = url + "&shopCodeFilter="+ shopCodeFilter;
					}
					$.getJSON(url, function(data) {
						if(!data.error){
							var isSaleProduct = true;
							CommonSearch.showProductInfo(data.product.productCode, data.product.productName, 
									data.product.convfact,
									StockValidateInput.formatStockQuantity(data.avaiableQuantity, obj.convfact),
									data.price, StockValidateInput.getQuantity(data.avaiableQuantity,obj.convfact), data.promotion, isSaleProduct);
						}else{
							$('#GeneralInforErrors').html(data.errMsg).show();
						}					
					});				
				}
			}		
		},

		loadPageDetail:function(shopCode){
			SPCreateOrder._isViewOrder = true;
			SPAdjustmentOrder._orderId = $('#orderId').val();
			GridSaleBusiness.initSaleGrid();
			SPCreateOrder.loadGridFree();
			OpenProductPromotion.initOpenProductGrid();
			SPCreateOrder.createAccumulativePromotionGrid();
			
			setTimeout(function() {
				SPSearchSale.loadSaleOrderForAdjustment(SPAdjustmentOrder._orderId, shopCode);
			}, 200);

			setTimeout(function() {
				var htSale = $('#gridSaleData').handsontable('getInstance');
				for(var i=0;i<htSale.countRows();i++){
					for (var p in COLSALE) {
					    htSale.getCellMeta(i, COLSALE[p]).readOnly = true;
					}
				}
				htSale.render();
				$("#spSaleProductInfor a.delIcons:has(img)").remove();
				$('.cls_Promotion_Convfact_Value').each(function(){$(this).attr('disabled','disabled');});
				$('.cls_Promotion_Portion_Value').each(function(){$(this).attr('disabled','disabled');});
				$('td[field=key] a').html('');
				$('td[field=key] a').removeAttr('onclick');
				$('td[field=delete ] a').removeAttr('onclick');
				$('#gridSaleData tbody tr td:nth-child(' + (COLSALE.WAREHOUSE + 2) + ') a, #spPromotionProductInfor tbody tr td[field=warehouse] a, #saleOrderPromotion tbody tr td[field=warehouse] a').removeAttr('onclick').removeAttr('onclick');
				$('.cls_Convfact_Value').each(function(){$(this).attr('disabled','disabled');});
				$('.discountAmount').each(function(){$(this).attr('disabled','disabled');});
				$('.cls_CommercialSupporCode').each(function(){	$(this).attr('disabled','disabled');});	
						
			},2000);
			/*
			 * init some other data
			 */
			SPAdjustmentOrder.PROGRAM_TYPE_MAP = new Map([
														{key: "ZM", value: "1"},
														{key: "DISPLAY_SCORE", value: "2"},
														{key: "ZD", value: "3"},
														{key: "ZH", value: "4"},
														{key: "ZT", value: "5"},
														{key: "MANUAL_PROM", value: "dummy"}
													]);
	},

		/**
		 * Load param de tim kiem don hang
		 * @author nhanlt6
		 * @since 17/11/2014
		 */
		getParamsForSearchOrder:function() {
			$('.ErrorMsgStyle').hide();
			var msg = '';	
			var fDate = $('#fromDate').val().trim();
			var tDate = $('#toDate').val().trim();
			var deliveryDate = $('#deliveryDate').val().trim();
			if(fDate == '__/__/____'){
				$('#fromDate').val('');
			}
			if(tDate == '__/__/____'){
				$('#toDate').val('');
			}
			if(deliveryDate == '__/__/____'){
				$('#deliveryDate').val('');
			}
			if(msg.length == 0){
				msg = Utils.getMessageOfInvalidFormatDate('fromDate', 'Từ ngày');
			}
			if(msg.length ==0){
				msg = Utils.getMessageOfInvalidFormatDate('toDate', 'Đến ngày');
			}
			if(msg.length ==0){
				msg = Utils.getMessageOfInvalidFormatDate('deliveryDate', 'Ngày giao');
			}
			
			if(msg.length == 0 && !Utils.compareDate(fDate, tDate)){
				msg = msgErr_fromdate_greater_todate;
				$('#fromDate').focus();
			}
			if(msg.length > 0){
				$('#errorMsg').html(msg).show();
				return;
			}
			var params = {
				orderNumber: $('#orderNumber').val().trim(),
				refOrderNumber : $('#refOrderNumber').val().trim(),
				fromDate: $("#fromDate").val().trim(),
				toDate: $("#toDate").val().trim(),
				deliveryDate: $("#deliveryDate").val().trim(),
//				orderStatus : $('#orderStatus').val().trim(),
				customerCode : $('#customerCode').val().trim(),
				customerName : $('#customerName').val().trim(),
				orderSource : $('#orderSource').val().trim(),
				saleStaff : $('#saleStaff').combobox('getValue'),
				deliveryStaff : $('#deliveryStaff').combobox('getValue'),
				priorityCode: $('#priorityId').val(),
				currentShopCode: $('#shopCodeCB').combobox('getValue')
			};
			var orderType = $('#orderType').val();
			if (orderType != null) {
				if (orderType instanceof Array) {
					params.orderType = orderType.join(",");
					for (var i = 0, sz = orderType.length; i < sz; i++) {
						if (orderType[i].trim().length == 0) {
							params.orderType = "";
							break;
						}
					}
				} else {
					params.orderType = orderType.trim();
				}
			}
			var orderStatus = $('#orderStatus').val().trim();
			if(orderStatus != '-1' && orderStatus != ''){
				if(orderStatus == '0'){//Đã trả
					params.saleOrderType = 0;
				}else{
					var str=orderStatus.split('-');
					if(str.length > 1){
						params.approved = str[0];
						if(str[1] != ''){
							params.approvedStep = str[1];
						}
					}
				}
			}else{
				params.saleOrderType = -1;
			}
			return params;
		},

		/**
		 * Tìm kiếm đơn hàng
		 * @author nhanlt6
		 * @since 17/11/2014
		 */
		searchOrder:function(){
			$('#panelDetail').hide();
			$('#errMsg').hide();
			var params = SPSearchSale.getParamsForSearchOrder();
			SPSearchSale._lstOrder = new Map();
			if(SPSearchSale._isLoad){
				SPSearchSale.loadGridOrder(params);
			} else {
				$("#grid").datagrid('load', params);
			}
		},

		/**
		 * Tìm kiếm chi tiết đơn hàng
		 * @author tungmt
		 * @since 29/08/2014
		 */
		searchOrderDetail:function(id){
			$('#panelDetail').show();
			SPConfirmOrder.hideAllMsg();
			var params=new Object();
			params.saleOrderId=id;
			var order=SPConfirmOrder._lstOrder.get(id);
			$('#orderNumberDetail').html(order.orderNumber);
			SPConfirmOrder.loadGridDetail(params);
		},

		/**
		 * Load danh sách đơn hàng
		 * @author nhanlt
		 * @since 17/11/2014
		 */
		loadGridOrder:function(params){
//			var firstLoad = true;
			$('#grid').datagrid({
				url: '/sale-product/search-sale/search',
			    checkOnSelect: false,
				pageSize:20,
				pageNumber:1,
			    singleSelect:true,
			    scrollbarSize:0,
			    autoRowHeight : true,
			    height: "auto",
			    width: $('#dgGridContainer').width()-25,
			    pagination: true,
			    rownumbers: true,
		    	pageList: [10, 20, 30, 50, 100, 200, 500],
		    	queryParams: params,
			    frozenColumns: [[
					{field:'detail', title:'Chi tiết', width:30, align: 'left', formatter : function(v,r,i) {
						var orderT = Utils.XSSEncode(r.orderType);
						var currentShopCode = $('#shopCodeCB').combobox('getValue');
						var str = '<a href="javascript:void(0);" onclick="SPPrintOrder.showDetailInPopup('+r.id +',\''+orderT + '\',\'' + currentShopCode +'\');" title="Xem chi tiết" id="aEdit"><img height="17" src="/resources/images/icon-view.png"></a>';
						return str;
					}},
					{field:'refOrderNumber', title:'Số ĐH tham chiếu', width:120, align: 'left', formatter : function(v,r,i) {
						return Utils.XSSEncode(v);
					}},
					{field:'orderNumber', title:'Số ĐH', width:120, align: 'left', formatter : function(v,r,i) {
						return Utils.XSSEncode(v);
					}},
					{field:'staff', title:'NVBH', width:120, align: 'left', formatter : function(v,r,i) {
						if(r.staffCode!=null){
							return Utils.XSSEncode(r.staffCode);
						}
						return '';
					}},
					{field:'customer', title:'Khách hàng', width:140, align: 'left', formatter : function(v,r,i) {
						if(r.customerId!=null){
							return '<a class="cmsdefault" href="javascript:void(0);" onclick="return SPConfirmOrder.showCustomerInfor('+Utils.XSSEncode(r.customerId)+')">'+Utils.XSSEncode(r.customerCode+' - '+r.customerName)+'</a>';
						}
						return '';
					}},
					{field:'totalValue', title:'Tổng tiền', width:120, align: 'right', formatter : function(v,r,i) {
						if(v!=undefined && v!=null && v!=''){
							return formatCurrency(Utils.XSSEncode(v));
						}else{
							return '0';
						}
					}},
			    ]],
			    columns:[[
			        {field:'saleOrderDiscount', title:'CK đơn hàng', width:120, align: 'right', formatter : function(v,r,i) {
			        	if(v!=undefined && v!=null && v!=''){
			        		return formatCurrency(Utils.XSSEncode(v));
			        	}else{
			        		return '0';
			        	}
			        }},
			        {field:'totalDiscount', title:'Tổng tiền KM', width:120, align: 'right', formatter : function(v,r,i) {
			        	if(v!=undefined && v!=null && v!=''){
			        		return formatCurrency(Utils.XSSEncode(v));
			        	}else{
			        		return '0';
			        	}
			        }},
					{field:'orderDate', title:'Ngày tạo', width:160, align: 'center', formatter :function(v,r,i) {
						if(v!=undefined && v!=null){
							var dateTimeStr = Utils.XSSEncode(v);
							var timePortionHhMiStr = '';
							try {
								/*
								 * get time portion
								 */
								var timePortionStr = dateTimeStr.split('T')[1];
								//timePortionHhMiStr = timePortionStr.substring(0, timePortionStr.lastIndexOf(':'));
								timePortionHhMiStr = timePortionStr;
							} catch (e) {
								// pass through
							}
							return $.datepicker.formatDate('dd/mm/yy', new Date(dateTimeStr)) + (timePortionHhMiStr ? ' ' + timePortionHhMiStr : '');
						}
						return '';
					}},
					{field:'deliveryDate', title:'Ngày giao', width:80, align: 'center', formatter : function(v,r,i) {
						if(v!=undefined && v!=null){
							return $.datepicker.formatDate('dd/mm/yy', new Date(Utils.XSSEncode(v)));
						}
						return '';
					}},
					{field:'priorityStr', title:'Độ ưu tiên', width:100, align: 'left', formatter : function(v,r,i) {
			        	if(!isNullOrEmpty(v)){
			        		return Utils.XSSEncode(v);
			        	}
			        	return '';
			        }},
			        {field:'address', title:'Địa chỉ', width:250, align: 'left', formatter : function(v,r,i) {
			        	if(r.address!=null){
			        		return Utils.XSSEncode(r.address);
			        	}
			        	return '';
			        }},
			        {field:'orderStatus', title:'Trạng thái', width:80, align: 'left', formatter : function(v,r,i) {
			        	if(!isNullOrEmpty(v)){// SaleOrder
			        		if (r.saleOrderType == 1 || r.saleOrderType == 2) {
			        			if (v == orderStatus.NOT_APPROVED && r.approvedStep == 0) {
			        				return "Chưa xác nhận đơn hàng";
			        			} else if (v == orderStatus.NOT_APPROVED && r.approvedStep == 1) {
			        				return "Đã xác nhận đơn hàng";
			        			} else if (v == orderStatus.APPROVED && r.approvedStep == 2) {
			        				return "Đã xác nhận IN";
			        			} else if(v == orderStatus.APPROVED && r.approvedStep == 3){
			        				return "Đã cập nhật kho";
			        			} else if (v == 2){
			        				return "Từ chối";
			        			} else if (v == 3){
			        				return "Hủy";
			        			}
			        		} else if (r.saleOrderType == 0) {
			        			return "Đã trả";
			        		}else{//DCT,DCG, GO,DP
			        			if(r.fromOrderId != null){
			        				return "Đã trả";
			        			}else{
				        			if (v == orderStatus.NOT_APPROVED && r.approvedStep == 1) {
				        				return "Đã xác nhận đơn hàng";
				        			} else if (v == orderStatus.APPROVED && r.approvedStep == 2) {
				        				return "Đã xác nhận IN";
				        			} else if(v == orderStatus.APPROVED && r.approvedStep == 3){
				        				return "Đã cập nhật kho";
				        			} else if (v == 2){
				        				return "Từ chối";
				        			} else if (v == 3){
				        				return "Hủy";
				        			}
			        			}
			        		}
			        	}
			        	return '';
			        }},
			        {field:'orderType', title:'Loại ĐH', width:50, align: 'left', formatter : function(v,r,i) {
			        	if(!isNullOrEmpty(v)){
			        		return Utils.XSSEncode(v);
			        	}
			        	return '';
			        }},
			        {field:'fromSaleOrderNumber', title:'Đơn gốc', width:120, align: 'left', formatter : function(v,r,i) {
			        	if(!isNullOrEmpty(v)){
			        		return Utils.XSSEncode(v);
			        	}
			        	return '';
			        }},
			        {field:'deliveryCode', title:'NVGH', width:120, align: 'left', formatter : function(v,r,i) {
			        	if(r.deliveryCode!=null){
			        		return Utils.XSSEncode(r.deliveryCode);
			        	}
			        	return '';
			        }}
			    ]],
			    onBeforeLoad: function(param) {
			    	if (!SPCreateOrder._isAllowShowPrice){
			    		$('#grid').datagrid('hideColumn','totalDiscount');
			    		$('#grid').datagrid('hideColumn','totalValue');
			    		$('#grid').datagrid('hideColumn','saleOrderDiscount');
			    	}
//			    	if (firstLoad) {
//			    		firstLoad = false;
//			    		return false;
//			    	}
			    	$.extend(true, param, SPSearchSale.getParamsForSearchOrder());
			    },
			    onLoadSuccess :function(data){
			    	$('.datagrid-header-rownumber').html('STT');
				   	$('.datagrid-header-row td div').css('text-align','center');
				   	//updateRownumWidthForJqGrid('.easyui-dialog');
				   	//Utils.functionAccessFillControl();

				   	var orderType = $("#orderType").val();
				   	if (orderType == null) {
				   		$("#btnXml:hidden").show();
				   	} else  if (orderType instanceof Array) {
				   		if (orderType.length == 0) {
				   			$("#btnXml:hidden").show();
				   		} else if (orderType.indexOf("IN") > -1 || orderType.indexOf("SO") > -1
				   			|| orderType.indexOf("CM") > -1 || orderType.indexOf("CO") > -1) {
				   			$("#btnXml:hidden").show();
				   		} else {
				   			$("#btnXml:not(:hidden)").hide();
				   		}
				   	} else {
				   		orderType = orderType.trim();
					   	if (orderType.length == 0 || orderType == "IN" || orderType == "SO"
					   		|| orderType == "CM" || orderType == "CO") {
					   		$("#btnXml:hidden").show();
					   	} else {
					   		$("#btnXml:not(:hidden)").hide();
					   	}
					}

			   	 	$(window).resize();
			    },
			    rowStyler: function(index,r){
					if (r.message!=null && r.message.trim()!=''){
						return 'color:red;'; // return inline style
					}
				}
			});
		},
		
		/**
		 * clone tu SPAdjustmentOrder.loadSaleOrderForAdjustment va bo mot so thu khong can thiet
		 */
		loadSaleOrderForAdjustment: function(soId, shopCode) {
			SPCreateOrder._lstProduct = new Map();
			SPCreateOrder._lstPromotionProductIsAuto = new Map();	
			//List tong hop: sale_order_detail & order_detail_promotion
			var sale_order_detail_map = new Map();
			var data = new Array();
			var row = null;
			$.getJSON('/commons/adjust-load-product-for-sale?viewOnly=1&orderId=' + soId + '&shopCodeFilter=' + shopCode, function(value) {
				if (value.saleOrder != null && (value.saleOrder.orderType == "SO" || value.saleOrder.orderType == "CO")) {
					SPAdjustmentOrder._isVansale = true;
				} else {
					SPAdjustmentOrder._isVansale = false;
				}
				SPAdjustmentOrder._firstLoadOrderForEdit = true;
				var errMsg = '';
				
				if($.isArray(value.productStocks) && value.productStocks.length > 0) {
					for (var i=0;i<value.productStocks.length;i++) {
						var tmp = value.productStocks[i];
						SPCreateOrder._PRODUCTS.put(tmp.productCode,tmp);
					}
				}

				//Lay danh sach chuong trinh ho tro thuong mai
		       if($.isArray(value.listCommercialSupport) && value.listCommercialSupport.length > 0) {
		        	GridSaleBusiness.listCS = value.listCommercialSupport;
		        	GridSaleBusiness.mapCS = new Map();
		        	GridSaleBusiness.listCSCode = new Array();
					for(var i = 0; i < value.listCommercialSupport.length; i++) {
						GridSaleBusiness.mapCS.put(GridSaleBusiness.listCS[i].code, GridSaleBusiness.listCS[i]);
						GridSaleBusiness.listCSCode.push(GridSaleBusiness.listCS[i].code);
					}
		       }
		        
		        setTimeout(function() {
					if(SPCreateOrder._PRODUCTS.size() > 0) {
						for (var l=0;l<SPCreateOrder._PRODUCTS.keyArray.length;l++) {
							var tmp = SPCreateOrder._PRODUCTS.valArray[l];
							var productId = tmp.productId;
							SPCreateOrder._mapPRODUCTSbyWarehouse.put(productId, value.mapProductByWarehouse[productId]);
						}
					}
				},300);

				//Lay danh sach sp co the ban
				if($.isArray(value.listSaleProduct) && value.listSaleProduct.length > 0) {
					for(var i = 0; i < value.listSaleProduct.length; i++) {
						GridSaleBusiness.mapSaleProduct.put(value.listSaleProduct[i].productCode, value.listSaleProduct[i]);
					}
				}
				
		        //Load danh sach sp ban theo Kho
		        if($.isArray(value.sale_order_lot) && value.sale_order_lot.length > 0) {
					for(var i = 0, rowIndex = 0; i < value.sale_order_lot.length; i++) {
						var obj = value.sale_order_lot[i];
						//console.log(obj.promotionType);
						/*if (obj.isFreeItem && obj.isFreeItem == 1
								&& (obj.promotionType && obj.promotionType.indexOf("ZV") == 0)) {
							continue;
						}*/
						var soDetailId = obj.id;
						var lotId = obj.saleOrderLotId;
						var productId = obj.productId;
						var productCode = obj.productCode;
						var productName = obj.productName;
						var availableQuantity = formatQuantity(obj.availableQuantity, obj.convfact);
						var price = obj.price;
						var pkPrice = obj.pkPrice;
						var warehouseId = obj.warehouseId;
						var warehouseName = obj.warehouseName;
						var commercialSupport = obj.programCode;
						var convfact = obj.convfact;
						var convfact_value = "";
						if (obj.quantityPackage != null && obj.quantityPackage != undefined && obj.quantityRetail != null && obj.quantityRetail != undefined) {
							convfact_value = obj.quantityPackage + '/' + obj.quantityRetail;
						} else {
							convfact_value = formatQuantity(obj.quantity, obj.convfact);
						}
						var quantity = Number(obj.quantity);
						var oldQuantity = Number(obj.oldQuantity);
						var grossWeight = Number(obj.grossWeight);
						var commercialSupportType = obj.promotionType;
						var joinProgramCode = obj.joinProgramCode;
						var stock = Number(obj.stockQuantity);
						var discountAmount = obj.discountAmount;
						var discountPercent = obj.discountPercent;
						var isFree = obj.isFreeItem;
						
						row = [];
						row.push(productCode);
						row.push(productName);
						if (SPCreateOrder._isAllowShowPrice){
							row.push(formatCurrency(pkPrice));
							row.push(formatCurrency(price));
						}
						row.push(warehouseName);
						row.push(availableQuantity);
						row.push(convfact_value);
						if (SPCreateOrder._isAllowShowPrice){
							if (isFree && isFree == 1) {
								row.push(0);
							} else {
								row.push(formatCurrency(price*quantity));
							}
							row.push(formatCurrency(discountAmount));
						}
						row.push(commercialSupport);
						
						if (isFree == 0) {
							data.push(row);
						} else {
							// xu ly load san pham km huy, doi, tra, tay vao ds san pham ban & tinh lai amount
							var cs = GridSaleBusiness.mapCS.get(commercialSupport);
							if (commercialSupport && !isNullOrEmpty(commercialSupport)
								&& cs != null && SPAdjustmentOrder.PROGRAM_TYPE_MAP.get(cs.type)) {
								data.push(row);
							}
						}
					}
					SPAdjustmentOrder._loadDetail=1;
		        }
		        
				SPCreateOrder._isCreatingSaleOrder = true;
		        var hsSale = $('#gridSaleData').handsontable('getInstance');
				var colProps = GridSaleBusiness.getColumnProperty();
				colProps.splice(colProps.length - 1, 1);
				hsSale.updateSettings({data: data , columns: colProps, minSpareRows: 1});
				hsSale.render();
				
//				SPCreateOrder.showTotalGrossWeightAndAmount();
				
				if ($("#loadingProduct:not(:hidden)").length > 0) {
					$("#loadingProduct").hide();
				}
				
				SPCreateOrder._mapPromoDetail = new Map();
				if (value.lstPromoDetails instanceof Array && value.lstPromoDetails.length > 0) {
					var promos = null;
					var pdId = null;
					for (var ij = 0, szj = value.lstPromoDetails.length; ij < szj; ij++) {
						if (value.lstPromoDetails[ij].saleOrderDetail) {
							pdId = value.lstPromoDetails[ij].saleOrderDetail.product.id;
							promos = SPCreateOrder._mapPromoDetail.get(pdId);
							if (promos == null) {
								promos = [];
							}
							value.lstPromoDetails[ij].saleOrderDetail = null;
							promos.push(value.lstPromoDetails[ij]);
							SPCreateOrder._mapPromoDetail.put(pdId, promos);
						}
					}
				}

				SPCreateOrder._saleOrderQuantityReceivedList = value.saleOrderQuantityReceivedList;
				//Luu so suat cua KM don hang vao map de luu du lieu
				SPCreateOrder._promotionOrderReceivedList = new Map();
				//Luu tat ca CTKM theo muc de ap dung chinh sua so suat
				SPCreateOrder._promotionPortionReceivedList = new Map();
				if (value.saleOrderQuantityReceivedList) {
					for (var i=0;i<value.saleOrderQuantityReceivedList.length;i++) {
						var tmp = value.saleOrderQuantityReceivedList[i];
						var listProduct = new Array();
						value.order_detail_promotion.forEach(function(item, tempIndex) {
							if (item.programCode == tmp.promotionCode && item.levelPromo == tmp.promotionLevel) {
								item.quantityReceived = tmp.quantityReceived;
								item.maxQuantityReceived = tmp.maxQuantityReceived;
								item.maxQuantityReceivedTotal = tmp.maxQuantityReceivedTotal;
								item.saleOrderDetail = item;
								item.saleOrderDetail.product = item;
								listProduct.push(item);
							}
						});
						SPCreateOrder._promotionPortionReceivedList.put(tmp.promotionCode+"_"+tmp.productGroupId+"_"+tmp.promotionLevel, listProduct);
						if (tmp.promotionProgram.type == 'ZV19' || tmp.promotionProgram.type == 'ZV20' || tmp.promotionProgram.type == 'ZV21') {
							SPCreateOrder._promotionOrderReceivedList.put(tmp.promotionProgram.promotionProgramCode+"_"+tmp.productGroupId+"_"+tmp.promotionLevel, tmp);
						};
					}
				}
				
				SPCreateOrder._lstPromotionProductIsAuto = new Map();
				SPCreateOrder._lstPromotionOpenProduct = new Map();
				SPCreateOrder._lstPromotionProductForOrderIsAuto = new Map();
				var mapProgramLevelPromoGroup = new Map();
				var groupId = 1;
				//Danh sach san pham KM
				if($.isArray(value.order_detail_promotion) && value.order_detail_promotion.length > 0) {
					var hasHeader = false;
					for(var i = 0; i < value.order_detail_promotion.length; i++) {
						var obj = value.order_detail_promotion[i];
						var pproduct = {};	
						pproduct.isEdited = Number(obj.isEdited);
						if (!isNullOrEmpty(obj.productCode)) {
							pproduct.productCode = obj.productCode;
							pproduct.productName = obj.productName;
							pproduct.ppIndex = i;
						} else {
							pproduct.productCode = '';
							pproduct.productName = '';
						}
						if (!isNullOrEmpty(obj.programCode)) {
							pproduct.programCode = obj.programCode;
							if (!isNullOrEmpty(obj.productCode)) {
								pproduct.convfact = obj.convfact;
								pproduct.productId = obj.productId;
							} else {
								pproduct.convfact = '';
							}
							pproduct.quantity = obj.quantity;
							if (isNullOrEmpty(pproduct.quantity)) {
								pproduct.quantiy = 0;
							}
							pproduct.quantityPackage = obj.quantityPackage;
							if (isNullOrEmpty(pproduct.quantityPackage)) {
								pproduct.quantityPackage = 0;
							}
							pproduct.quantityRetail = obj.quantityRetail;
							if (isNullOrEmpty(pproduct.quantityRetail)) {
								pproduct.quantityRetail = 0;
							}
							pproduct.maxQuantity = obj.maxQuantityFree;
							pproduct.maxQuantityCk = obj.maxQuantityFreeCk;
							pproduct.maxQuantityFreeTotal = obj.maxQuantityFreeTotal; /** vuongmq; 08/01/2015; hien thi maxQuantityFreeTotal so luong tinh khuyen mai*/
							var proQuatity = parseInt(pproduct.quantity);
							var stock = parseInt(obj.availableQuantity);
						} else {
							pproduct.programCode = '';		
						}
						pproduct.type = Number(obj.type);
						pproduct.key = obj.keyList;
						pproduct.changeProduct = obj.changeProduct;
						pproduct.stock = isNullOrEmpty(obj.availableQuantity)?0:obj.availableQuantity;
						pproduct.grossWeight = isNullOrEmpty(obj.grossWeight)?0:obj.grossWeight;		
						pproduct.promotionType = obj.ppType;
						pproduct.commercialSupport = obj.programCode;
						pproduct.commercialSupportType = obj.promotionType;
						pproduct.isFree = 1;
						pproduct.rownum = i +1;
						pproduct.warehouse = obj.warehouse;
						pproduct.levelPromo = obj.levelPromo;
						pproduct.productGroup = obj.productGroup;
						pproduct.productGroupId = obj.productGroupId;
						pproduct.groupLevel = obj.groupLevel;
						pproduct.openPromo = obj.openPromo;
						pproduct.accumulation = obj.accumulation;
						var PROMOTION_TYPE_ZV21 = "ZV21";
						if(pproduct.promotionType==PromotionType.PROMOTION_FOR_ORDER 
							|| pproduct.promotionType==PromotionType.PROMOTION_FOR_ZV21
							|| pproduct.promotionType == PROMOTION_TYPE_ZV21){
							if(pproduct.promotionType==PromotionType.PROMOTION_FOR_ORDER) {
								pproduct.productCode = 'Khuyến mãi đơn hàng';
								pproduct.discountAmount = obj.discountAmount;
								pproduct.maxDiscountAmount = obj.discountAmount;
								
							} else if(pproduct.promotionType==PromotionType.PROMOTION_FOR_ZV21
									|| pproduct.promotionType == PROMOTION_TYPE_ZV21) {
								pproduct.promotionType = PromotionType.PROMOTION_FOR_ORDER_TYPE_PRODUCT;
								/*if (!obj.warehouse) {
									lstWarehouselessProduct.push(obj.productCode);
								}*/
							}

							var key = pproduct.programCode + '_' + pproduct.levelPromo;
							if (!mapProgramLevelPromoGroup.get(key)) {
								if (!hasHeader && pproduct.promotionType==PromotionType.PROMOTION_FOR_ORDER_TYPE_PRODUCT) {
									var groupHeaderRow = {};
									$.extend(true, groupHeaderRow, pproduct);
									groupHeaderRow.productCode = 'Khuyến mãi đơn hàng';
									groupHeaderRow.promotionType = PromotionType.PROMOTION_FOR_ZV21;
									groupHeaderRow.groupId = groupId;
									SPCreateOrder._lstPromotionProductForOrderIsAuto.keyArray.splice(SPCreateOrder._lstPromotionProductIsAuto.size(), 0, -1);
									SPCreateOrder._lstPromotionProductForOrderIsAuto.valArray.splice(SPCreateOrder._lstPromotionProductIsAuto.size(), 0, groupHeaderRow);
									hasHeader = true;
								}							
								mapProgramLevelPromoGroup.put(key, groupId++);
							}

							pproduct.groupId = mapProgramLevelPromoGroup.get(key);
							if(pproduct.canChaneMultiProduct) {
								SPCreateOrder._lstPromotionProductForOrderIsAuto.keyArray.splice(SPCreateOrder._lstPromotionProductIsAuto.size(), 0, pproduct.programCode+'_'+pproduct.productId);
								SPCreateOrder._lstPromotionProductForOrderIsAuto.valArray.splice(SPCreateOrder._lstPromotionProductIsAuto.size(), 0, pproduct);
							} else {
								SPCreateOrder._lstPromotionProductForOrderIsAuto.put(i,pproduct);				
							}
							
						}else if(pproduct.type == PromotionType.FREE_PRODUCT) {
							pproduct.myTime = obj.myTime;
							if(pproduct.myTime == 0 || pproduct.myTime == 'null') {
								pproduct.myTime = null;
							}
							if (pproduct.openPromo == 1) {
								if(pproduct.canChaneMultiProduct) {
									SPCreateOrder._lstPromotionOpenProduct.keyArray.splice(SPCreateOrder._lstPromotionOpenProduct.size(), 0, pproduct.programCode+'_'+pproduct.productId);
									SPCreateOrder._lstPromotionOpenProduct.valArray.splice(SPCreateOrder._lstPromotionOpenProduct.size(), 0, pproduct);
								} else {
									SPCreateOrder._lstPromotionOpenProduct.put(i,pproduct);
								}
							} else if (pproduct.accumulation != 1) {
								if(pproduct.canChaneMultiProduct) {
									SPCreateOrder._lstPromotionProductIsAuto.keyArray.splice(SPCreateOrder._lstPromotionProductIsAuto.size(), 0, pproduct.programCode+'_'+pproduct.productId);
									SPCreateOrder._lstPromotionProductIsAuto.valArray.splice(SPCreateOrder._lstPromotionProductIsAuto.size(), 0, pproduct);
								} else {
									SPCreateOrder._lstPromotionProductIsAuto.put(i,pproduct);				
								}
							}
							/*if (!obj.warehouse) {
								lstWarehouselessProduct.push(obj.productCode);
							}*/
						} else if (pproduct.type == PromotionType.FREE_PRICE && pproduct.promotionType == PromotionType.FREE_PRODUCT) {
							pproduct.productCode = 'Khuyến mãi mở mới';
							pproduct.discountAmount = obj.discountAmount;
							pproduct.maxDiscountAmount = obj.discountAmount;
							if(pproduct.canChaneMultiProduct) {
								SPCreateOrder._lstPromotionOpenProduct.keyArray.splice(SPCreateOrder._lstPromotionOpenProduct.size(), 0, pproduct.programCode+'_'+pproduct.productId);
								SPCreateOrder._lstPromotionOpenProduct.valArray.splice(SPCreateOrder._lstPromotionOpenProduct.size(), 0, pproduct);
							} else {
								SPCreateOrder._lstPromotionOpenProduct.put(i, pproduct);
							}
						}
						
					}

					/*
					 * sort data ZV01-18
					 */
					SPCreateOrder._lstPromotionProductIsAuto.valArray.sort(function(o1, o2){
						var id = '-1413001269113';
						/*if (isZV21) {
							if (o1.groupId != o2.groupId) {
								return o1.groupId > o2.groupId;
							}
						}*/
						if (o1.programCode != o2.programCode) {
							return o1.programCode > o2.programCode;
						}
						if (o1.productCode.trim().indexOf(" ") > 0) {
							return false;
						}
						if (o2.productCode.trim().indexOf(" ") > 0) {
							return true;
						}
						if (Number(o1.levelPromo) != Number(o2.levelPromo)) {
							return o1.levelPromo < o2.levelPromo;
						}
						if (!isNaN(o1.productGroupId) && !isNaN(o2.productGroupId)
							&& Number(o1.productGroupId) != Number(o2.productGroupId)) {
							return o1.productGroupId < o2.productGroupId;
						}
						if (Number(o1.changeProduct) != Number(o2.changeProduct)) {
							return Number(o1.changeProduct) > Number(o2.changeProduct);
						}
						if (o1.productCode != o2.productCode) {
							return o1.productCode > o2.productCode;
						}
						return o1.warehouse.seq > o2.warehouse.seq;
					});
					/*
					 * re-create order number
					 */
					SPCreateOrder._lstPromotionProductIsAuto.valArray.forEach(function(item, index){
						SPCreateOrder._lstPromotionProductIsAuto.keyArray[index] = index;
						item.rownum = index + 1;
					});
					OpenProductPromotion.sortProductList(SPCreateOrder._lstPromotionOpenProduct, false);
					SPCreateOrder._lstPromotionOpenProduct.valArray.forEach(function(item, index){
						SPCreateOrder._lstPromotionOpenProduct.keyArray[index] = index;
					});

					/*
					 * ZV21: sort rows by program_code, promo_level, can_change_product
					 */
					SPCreateOrder._lstPromotionProductForOrderIsAuto.valArray.sort(function(o1, o2){
						var id = '-1413001269113';
						/*if (isZV21) {
							if (o1.groupId != o2.groupId) {
								return o1.groupId > o2.groupId;
							}
						}*/
						if (o1.programCode != o2.programCode) {
							return o1.programCode > o2.programCode;
						}
						if (o1.productCode.trim().indexOf(" ") > 0) {
							return false;
						}
						if (o2.productCode.trim().indexOf(" ") > 0) {
							return true;
						}
						if (Number(o1.levelPromo) != Number(o2.levelPromo)) {
							return o1.levelPromo < o2.levelPromo;
						}
						if (!isNaN(o1.productGroupId) && !isNaN(o2.productGroupId)
							&& Number(o1.productGroupId) != Number(o2.productGroupId)) {
							return o1.productGroupId < o2.productGroupId;
						}
						if (Number(o1.changeProduct) != Number(o2.changeProduct)) {
							return Number(o1.changeProduct) > Number(o2.changeProduct);
						}
						if (o1.productCode != o2.productCode) {
							return o1.productCode > o2.productCode;
						}
						return o1.warehouse.seq > o2.warehouse.seq;
					});

					/*
					 * re-calculate row's sequence number
					 */
					SPCreateOrder._lstPromotionProductForOrderIsAuto.keyArray.forEach(function(item, index){
						SPCreateOrder._lstPromotionProductForOrderIsAuto.keyArray[index] = index;
						SPCreateOrder._lstPromotionProductForOrderIsAuto.valArray[index].rownum = index + 1;
					});

					setTimeout(function () {
						isAllowEditPromotion = false;
						SPCreateOrder.fillTablePromotionProductForOrder(SPCreateOrder._lstPromotionProductForOrderIsAuto);
						SPCreateOrder.fillTablePromotionProduct(SPCreateOrder._lstPromotionProductIsAuto);
						OpenProductPromotion.loadOpenProductGrid(SPCreateOrder._lstPromotionOpenProduct);
					}, 300);
				}

				setTimeout(function() {
					SPCreateOrder.loadGridAccumulativeForEdit(value);
					$("td[field=key] [id^=btnRemoveProduct_]").remove();
//					$("td[field=warehouse] a").removeAttr("onclick");
					$('#totalWeight').html(formatCurrency(value.saleOrder.totalWeight));
					$('#totalVAT').html(formatCurrency(value.saleOrder.amount));
					$('#total').html(formatCurrency(value.saleOrder.total));
					$('#totalDiscount').html(formatCurrency(value.saleOrder.discount));
				}, 1300); // load grid khuyen mai tich luy
				
				SPSearchSale.loadKeyShop(SPAdjustmentOrder._orderId, shopCode);
				
			});
		},

		/**
		 * Xuat don hang ra file xml
		 *
		 * @author lacnv1
		 * @since Mar 13, 2014
		 */
		exportXml: function() {
			$(".ErrorMsgStyle").hide();
			var params = SPSearchSale.getParamsForSearchOrder();
			ReportUtils.exportReport("/sale-product/search-sale/export-xml", params);
		},
		showDialogZIndex: function(id) {
			$('#' + id).parent().attr('style',$('#' + id).parent().attr('style') + ';z-index:10000000 !important');
		},
		initUnitCbx: function(){				
			var params = {};
			$.ajax({
				type : "POST",
				url:'/commons/list-child-shop',
				data :($.param(params, true)),
				dataType : "json",
				success : function(data) {
					listNVBH = data.lstNVBH;
					$('#shopCodeCB').combobox({
						valueField: 'shopCode',
						textField:  'shopName',
						data: data.rows,
						panelWidth: '206',
						formatter: function(row) {
							return '<span style="font-weight:bold">' + Utils.XSSEncode(row.shopCode) + '</span><br/>' + '<span style="color:#888">' + Utils.XSSEncode(row.shopName) + '</span>';
						},
						filter: function(q, row){
							q = new String(q).toUpperCase().trim();
							var opts = $(this).combobox('options');
							return ((unicodeToEnglish(row[opts.textField]).indexOf(unicodeToEnglish(q)) == 0) || unicodeToEnglish(row[opts.valueField]).indexOf(unicodeToEnglish(q)) == 0) ;
						},	
				        onSelect: function(param){
				            SPSearchSale.getStaffByShopCode();
				        },
				        onLoadSuccess: function(){
				        	var arr = $('#shopCodeCB').combobox('getData');
				        	if (arr != null && arr.length > 0){
				        		$('#shopCodeCB').combobox('select', arr[0].shopCode);
				        	}
				        	
				        }
					});
				}
			});
		},
		getStaffByShopCode: function() {
			 var shopCode = $('#shopCodeCB').combobox('getValue');
			 var params = new Object();
			if(shopCode != null){
				params.currentShopCode = shopCode;
			}
			var kData = $.param(params, true);
			 $.ajax({
				type : "POST",
				url : '/sale-product/search-sale/load-staff-by-shop-code',
				data : (kData),
				dataType : "json",
				success : function(data) {	
					if (data.isShowPrice != null && data.isShowPrice != undefined) {
						SPCreateOrder._isAllowShowPrice = data.isShowPrice ;
					} else {
						SPCreateOrder._isAllowShowPrice = false;
					}
					$('#shopId').val(data.shopId);

					var lstNVBH = data.lstNVBH;
					var lstNVGH = data.lstNVGH;

					if(lstNVBH != undefined && lstNVBH != null) {
						Utils.bindStaffCbx('#saleStaff', lstNVBH);
					}
					
					if(lstNVGH != undefined && lstNVGH != null) {
						Utils.bindStaffCbx('#deliveryStaff', lstNVGH);
					}
					
					$('#fromDate').val(data.lockDate);
					$('#toDate').val(data.lockDate);
					$('#strLockDate').text(data.lockDate);
					
					if(SPSearchSale._isLoad){
		        		SPSearchSale.searchOrder();
		        		SPSearchSale._isLoad = false;
		        	}					 
				},
				error:function(XMLHttpRequest, textStatus, errorThrown) {
					console.log("Error");
				}
			});

		},
		/*
		 * trietptm
		 * Khởi tạo grid keyshop
		 */
		initKeyShopGrid:function(data, width) {
			var wd = screen.width || $(window).width();
			$('#gridKeyShop').datagrid({
				data: data,
				singleSelect: true,
				width: wd - width,
				fitColumns: true,		
				scrollbarSize: 0,
				autoRowHeight : true,
				columns: [[
				    {field:'ksCode', title:'CT HTTM', width:100, resizable:false, sortable:false, align:'left', formatter: function(v, r, i) {
				    	return '<a class="delIcons" onclick = "return SPCreateOrder.showKeyShopDetail(\'' + Utils.XSSEncode(v) + '\');" href="javascript:void(0)">' + Utils.XSSEncode(v) + '</a>';
				    }},
				    {field:'productCode', title:'Mã sản phẩm',width:100, resizable:false, sortable:false, align:'left', formatter: function(v, r, i) {
				    	v = Utils.XSSEncode(v);
				    	return '<a href="javascript:void(0)" class="ppCodeStyle" id="promotionProduct7" index="7" onclick="return SPCreateOrder.viewProductPromotionDetail(\'' + v + '\')">' + v + '</a>';
				    }},
				    {field:'productName', title:'Tên sản phẩm', align:'left', resizable:false, sortable:false, width: 150, formatter:function(v,r,i) {
				    	return Utils.XSSEncode(v);
				    }},
				    {field:'quantityAmount', title:'Số lượng/Tiền', align:'right', resizable:false, sortable:false, width: 100, formatter:function(v,r,i) {
				    	v = Utils.XSSEncode(v);
				    	if (r.productCode != null && r.productCode != '') {//sản phẩm
				    		return r.quantityPackage + '/' + r.quantityRetail;
				    	} else {
				    		return formatCurrency(v);
				    	}
				    }},
				    {field:'warehouseName', title:'Kho', align:'left', resizable:false, sortable:false, width: 100, formatter:function(v,r,i) {
				    	if (r.productCode != null && r.productCode != '') {//sản phẩm
				    		return Utils.XSSEncode(r.warehouseName)
				    	}
				    	return '';
				    }},
				    {field:'availableQuantity', title:'Tồn kho', align:'right', resizable:false, sortable:false, width: 100, formatter:function(v, r, i) {
				    	if (r.productCode != null && r.productCode != '') {//sản phẩm
					    	return GridSaleBusiness.formatStockQuantity(Utils.XSSEncode(r.availableQuantity), r.convfact);
				    	}
				    	return '';
				    }},
				    {field:'totalDone', title:'Tổng trả', align:'right', resizable:false, sortable:false, width: 100, formatter:function(v,r,i) {
				    	if (r.productCode != null && r.productCode != '') {//sản phẩm
				    		return GridSaleBusiness.formatStockQuantity(Utils.XSSEncode(v), r.convfact);
				    	} else {
				    		return formatCurrency(Utils.XSSEncode(v));
				    	}
				    }},
				    {field:'total', title:'Tổng thưởng', align:'right', resizable:false, sortable:false, width: 100, formatter:function(v,r,i) {
				    	if (r.productCode != null && r.productCode != '') {//sản phẩm
				    		return GridSaleBusiness.formatStockQuantity(Utils.XSSEncode(v), r.convfact);
				    	} else {
				    		return formatCurrency(Utils.XSSEncode(v));
				    	}
				    }},
				]],	
				onLoadSuccess: function(data) {
					setTimeout(function(){
	       				CommonSearchEasyUI.fitEasyDialog("billDetailPopup");
	       			}, 100);
				}
			});
		},
		loadKeyShop: function(soId, shopCode, widthSize) {
			var width = 55;
			if (widthSize != null && widthSize != undefined) {
				width = widthSize;
			}
			$.getJSON('/commons/load-key-shop?orderId=' + soId + '&currentShopCode=' + shopCode, function(value) {
				if($.isArray(value.lstKeyShop) && value.lstKeyShop.length > 0) {
					$('#h2KS').show();
					$('#keyShop').show();
					SPSearchSale.initKeyShopGrid(value.lstKeyShop, width);
				} else {
					$('#h2KS').hide();
					$('#keyShop').hide();
				}
			});
		},
};
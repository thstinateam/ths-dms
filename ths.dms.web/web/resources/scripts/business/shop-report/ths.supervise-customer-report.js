var SuperviseCustomer = {	
		/* BAO CAO NHAN VIEN KHONG BAT MAY */
	mapNVGS : null,
	mapNVBH : null,
	mapTBHV : null,
	mapProduct : null,
	mapCustomer : null,
	mapCycle: new Map(),
	_lstProduct : null,
	_lstCat : null,
	_lstSubCat : null,
	exportBCNVKBM: function(){
		var msg = '';
		$('#errMsg').html('').hide();
		var multiShop = $("#shop").data("kendoMultiSelect");
		var dataShop = multiShop.dataItems();
		if(dataShop==null||dataShop.length==0){
			msg = format(msgErr_required_field,'đơn vị');
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfRequireCheck('fDate','Từ ngày');
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfInvalidFormatDate('fDate','Từ ngày');
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfRequireCheck('tDate','Từ ngày');
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfInvalidFormatDate('tDate','�?ến ngày');
		}
		var fDate = $('#fDate').val();
		var tDate = $('#tDate').val();
		var curDate = ReportUtils.getCurrentDateString();
		if(msg.length == 0 && !Utils.compareDate(fDate, curDate)){
			msg = 'Từ ngày phải nh�? hơn hoặc bằng ngày hiện tại';
			$('#fDate').focus();
		}	
		if(msg.length == 0 && !Utils.compareDate(tDate, curDate)){
			msg = '�?ến ngày phải nh�? hơn hoặc bằng ngày hiện tại';
			$('#tDate').focus();
		}
		if(!Utils.compareDate(fDate, tDate)){
			msg = msgErr_fromdate_greater_todate;		
			$('#fDate').focus();
		}
		if(msg.length > 0){
			$('#errMsg').html(msg).show();
			return false;
		}
		var staffCode = $('#staffCode').val().trim();
		var dataModel = new Object();
		if(dataShop!=null&&dataShop.length>0){
			var lstShopId = dataShop[0].id;
			for(var i=1;i<dataShop.length;i++){
				lstShopId += ","+dataShop[i].id;
			}
			dataModel.strListShopId = lstShopId;
		}
		dataModel.fromDate = $('#fDate').val().trim();
		dataModel.toDate = $('#tDate').val().trim();
		dataModel.staffCode = staffCode;
		ReportUtils.exportReport('/report/superivse/bc-nvkbm/export',dataModel);
	},
	/**
	 * Load ds san pham
	 * 
	 * @author hoanv25
	 * @since July 10, 2015
	 * */
	loadComboProduct:function(url){
		$("#lstProduct").kendoMultiSelect({
			dataSource: {
				transport: {
	                read: {
	                    dataType: "json",
	                    url: url
	                }
	            }
			},
			filter: "contains",
	        dataTextField: "nvbhName",
	        dataValueField: "nvbhId",
			itemTemplate: function(data, e, s, h, q) {
				return '<div class="tree-vnm" node-id="'+Utils.XSSEncode(data.nvbhId)+'" style="width:180px"><span class="tree-vnm-text">'+Utils.XSSEncode(data.nvbhCode)+' - '+Utils.XSSEncode(data.nvbhName) +'</span></div>';	
			},
	        tagTemplate:  '#: data.nvbhName #',
	        value: [$('#nvbhId').val()]		       
	    });	 
	},
	/**
	 * Bc Ke hoach tieu thu
	 * 
	 * @author longnh15
	 * @since July 10, 2015
	 * */
	exportBCKHTT: function(){
		var msg = '';
		$('#errMsg').html('').hide();
		var multiProduct = $("#lstProduct").data("kendoMultiSelect");
		var dataProduct = null;
		if (multiProduct != null) {
			dataProduct = multiProduct.dataItems();
		}
		
		if ($('#shopId').val().trim().length == 0 || $('#shopId').val().trim() == activeType.ALL) {
			msg = format(msgErr_required_field,'đơn vị');
		}
		if (msg.length == 0) {
			msg = Utils.getMessageOfRequireCheck('fDate','Từ ngày');
		}
		if (msg.length == 0) {
			msg = Utils.getMessageOfRequireCheck('tDate','Đến ngày');
		}
		if (msg.length > 0) {
			$('#errMsg').html(msg).show();
			return false;
		}
		if (!checkDateForCycle()) {
			return;
		}
		var lstProductId = [];
		var lstProductName = [];
		for(var i=0;i<dataProduct.length;i++){
			lstProductId.push(dataProduct[i].nvbhId);
			lstProductName.push(dataProduct[i].nvbhName)
		}
		var dataModel = new Object();
		dataModel.strListShopId=$('#shopId').val();
		dataModel.fromDate = $('#fDate').val().trim();
		dataModel.toDate = $('#tDate').val().trim();
		dataModel.cycleId = $('#cycle').val();
		dataModel.lstProduct = lstProductId.join(',');
		dataModel.lstCat = $('#category').val();
		dataModel.lstSubCat = $('#sub_category').val();
		ReportUtils.exportReport('/report/superivse/bckhtt-export',dataModel,'errMsg');

	},
	//vuongmq - Bao cao hinh anh
	exportBCVT9: function(){
		var msg = '';
		/*var multiShop = $("#shop").data("kendoMultiSelect");
		var dataShop = multiShop.dataItems();*/
		$('#errMsg').html('').hide(); 
		
		var shopKendo = $("#shop").data("kendoMultiSelect");
		var lstShopId = shopKendo.value();
		if(lstShopId.length <= 0){
			msg = format(msgErr_required_field,'�?ơn vị');
		}
		
		if(msg.length == 0){
			msg = Utils.getMessageOfRequireCheck('fromDate','Từ ngày');
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfInvalidFormatDate('fromDate','Từ ngày');
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfRequireCheck('toDate','�?ến ngày');
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfInvalidFormatDate('toDate','�?ến ngày');
		}
		var fDate = $('#fromDate').val();
		var tDate = $('#toDate').val();
		var curDate = ReportUtils.getCurrentDateString();
		if(msg.length == 0 && !Utils.compareDate(fDate, curDate)){
			msg = 'Từ ngày phải nh�? hơn hoặc bằng ngày hiện tại';
			$('#fromDate').focus();
		}	
		if(msg.length == 0 && !Utils.compareDate(tDate, curDate)){
			msg = '�?ến ngày phải nh�? hơn hoặc bằng ngày hiện tại';
			$('#toDate').focus();
		}
		if(!Utils.compareDate(fDate, tDate)) {
			msg ='Từ ngày phải nh�? hơn đến ngày.';
			$('#fromDate').focus();
		}

		var imageType = $('#imageType').val();
		if(imageType == null && imageType.length <= 0) {
			msg = format(msgErr_required_field,'Loại Hình ảnh');
		}
		var displayProgramId = $('#displayProgramId').val();
		if(imageType != null && imageType.length > 0 && (imageType.indexOf(5) != -1 || imageType.indexOf("5") != -1)) {
			if(displayProgramId == null && displayProgramId.length <= 0) {
				msg = format(msgErr_required_field,'Chương trình');
			}
		}
		
		if(msg.length > 0){
			$('#errMsg').html(msg).show();
			return false;
		}
		
		var dataModel = new Object();

		dataModel.strListShopId = lstShopId.toString().trim();
		dataModel.staffCode = $('#staffCode').val().trim(); 
		dataModel.fromDate = $('#fromDate').val();
		dataModel.toDate = $('#toDate').val();
		if(imageType != null && imageType.length > 0) {
			dataModel.imageType = imageType;
		}
		if(displayProgramId != null && displayProgramId.length > 0) {
			dataModel.displayProgramId = displayProgramId;
		}
		ReportUtils.exportReport('/report/superivse/bcha-vt9/export',dataModel);
	},
	
	exportKD101:function(){
		var msg = '';
		$('#errMsg').html('').hide();		
		if(msg.length == 0){
			msg = Utils.getMessageOfRequireCheck('fDate','Từ ngày');
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfInvalidFormatDate('fDate','Từ ngày');
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfRequireCheck('tDate','Từ ngày');
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfInvalidFormatDate('tDate','�?ến ngày');
		}
		var fDate = $('#fDate').val();
		var tDate = $('#tDate').val();
		if(!Utils.compareDate(fDate, tDate)){
			msg = msgErr_fromdate_greater_todate;		
			$('#fDate').focus();
		}
		if(msg.length > 0){
			$('#errMsg').html(msg).show();
			return false;
		}
		var dataModel = new Object();
		dataModel.fromDate = $('#fDate').val().trim();
		dataModel.toDate = $('#tDate').val().trim();
		ReportUtils.exportReport('/report/superivse/bccrmkd101/export',dataModel);
	},		
		/* BAO CAO NHAN VIEN KHONG GHE THAM KHACH HANG TRONG TUYEN */
	exportBCNVKGTKHTT: function() {
		var msg = '';
		var multiShop = $("#shop").data("kendoMultiSelect");
		var dataShop = multiShop.dataItems();
		$('#errMsg').html('').hide();
		
		/*if($('#shop').combotree('getValue').trim().length == 0 || $('#shop').combotree('getValue').trim() == activeType.ALL){
			msg = format(msgErr_required_field,'đơn vị');
		}
		
		if(msg.length == 0){
			msg = Utils.getMessageOfRequireCheck('shopCode','�?ơn vị');
		}*/
		
		if(dataShop.length <= 0){
			msg = format(msgErr_required_field,'�?ơn vị');
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfRequireCheck('fDate','Từ ngày');
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfRequireCheck('tDate','Từ ngày');
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfInvalidFormatDate('fDate','Từ ngày');
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfInvalidFormatDate('tDate','�?ến ngày');
		}
		/*if(msg.length == 0){
			msg = Utils.getMessageOfSpecialCharactersValidate('staffCode','NPP',Utils._CODE);
		}	
		if(msg.length == 0){
			msg = Utils.getMessageOfSpecialCharactersValidate('staffOwnerCode','NPP',Utils._CODE);
		}*/
		var fDate = $('#fDate').val();
		var tDate = $('#tDate').val();
		if(!Utils.compareDate(fDate, tDate)){
			msg = msgErr_fromdate_greater_todate;		
			$('#fDate').focus();
		}
		if(msg.length > 0){
			$('#errMsg').html(msg).show();
			return false;
		}
		var dataModel = new Object();
		if(dataShop!=null && dataShop.length>0){
			var lstShop = dataShop[0].id;
			for(var i=1;i<dataShop.length;i++){
				lstShop += "," + dataShop[i].id;
			}
			dataModel.strListShopId = lstShop;
		}
		//dataModel.shopId = $('#shopCode').val().trim();
		dataModel.fromDate = $('#fDate').val().trim();
		dataModel.toDate = $('#tDate').val().trim();
		dataModel.staffCode = $('#staffCode').val().trim();
		dataModel.staffOwnerCode = $('#staffOwnerCode').val().trim();
		//ReportUtils.exportReport('/report/superivse/export-bcnvkgtkhtt',dataModel);
		var kData = $.param(dataModel,true);
		$('#divOverlay').show();		
		$.ajax({
			type : "POST",
			url : "/report/superivse/export-bcnvkgtkhtt",
			data :(kData),
			dataType: "json",
			success : function(data) {
				$('#divOverlay').hide();				
				if(!data.error) {
					if(data.hasData != null && data.hasData != undefined && data.hasData == true) {
						window.location.href = data.path;
						setTimeout(function(){ //Set timeout để đảm bảo file load lên hoàn tất
		                    CommonSearch.deleteFileExcelExport(data.path);
						},2000);
					} else {
						$('#errMsg').html('Không có dữ liệu để xuất').show();
					}
				} else {
					$('#errMsg').html(data.errMsg).show();					
				}
			},
			error:function(XMLHttpRequest, textStatus, errorDivThrown) {
				$('#divOverlay').hide();
				StockIssued.xhrExport = null;
			}
		});
	},
	
	reportNVBHDSVM: function(){//BAO CAO DI MUON VE SOM CUA NVBH
		var msg = '';
		$('#errMsg').html('').hide();
		var multiShop = $('#shop').data("kendoMultiSelect");
		var dataShop = multiShop.dataItems();
		if(dataShop==null||dataShop.length==0){
			msg = format(msgErr_required_field,'đơn vị');
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfRequireCheck('fDate','Từ ngày');
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfInvalidFormatDate('fDate','Từ ngày');
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfRequireCheck('tDate','Từ ngày');
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfInvalidFormatDate('tDate','�?ến ngày');
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfRequireCheck('fromHour','Từ gi�?');
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfSpecialCharactersValidateEx('fromHour','Từ gi�?',Utils._TF_NUMBER);
		}
		if(msg.length==0){
			var value = $('#fromHour').val();
			if(parseInt(value,10) <0 || parseInt(value,10) >23){
				msg = "Từ gi�? phải từ 0 đến 23.";
				$('#fromHour').focus();
			}
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfRequireCheck('fromMinute','Từ phút');
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfSpecialCharactersValidateEx('fromMinute','Từ phút',Utils._TF_NUMBER);
		}
		if(msg.length==0){
			var value = $('#fromMinute').val();
			if(parseInt(value,10) <0 || parseInt(value,10) >59){
				msg = "Từ phút phải từ 0 đến 59.";
				$('#fromMinute').focus();
			}
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfRequireCheck('toHour','�?ến gi�?');
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfSpecialCharactersValidateEx('toHour','�?ến gi�?',Utils._TF_NUMBER);
		}
		if(msg.length==0){
			var value = $('#toHour').val();
			if(parseInt(value,10) <0 || parseInt(value,10) >23){
				msg = "�?ến gi�? phải từ 0 đến 23.";
				$('#toHour').focus();
			}
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfRequireCheck('toMinute','�?ến phút');
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfSpecialCharactersValidateEx('toMinute','�?ến phút',Utils._TF_NUMBER);
		}
		if(msg.length==0){
			var value = $('#toMinute').val();
			if(parseInt(value,10) <0 || parseInt(value,10) >59){
				msg = "�?ến phút phải từ 0 đến 59.";
				$('#toMinute').focus();
			}
		}
		var fDate = $('#fDate').val();
		var tDate = $('#tDate').val();
		var fromHour = $('#fromHour').val();
		var toHour = $('#toHour').val();
		if(!Utils.compareDate(fDate, tDate)){
			msg = msgErr_fromdate_greater_todate;		
			$('#fDate').focus();
		}
		if(msg.length == 0){
			if(parseInt(fromHour)>parseInt(toHour)){
				msg = "Từ gi�? không được lớn hơn đến gi�?.";
				$('#fromHour').focus();
			}
		}
		if(msg.length == 0 && parseInt(fromHour)==parseInt(toHour)){
			var fromMinute = $('#fromMinute').val();
			var toMinute = $('#toMinute').val();
			if(parseInt(fromMinute)>parseInt(toMinute)){
				msg = "Từ phút không được lớn hơn đến phút.";
				$('#fromMinute').focus();
			}
		}
		if(msg.length > 0){
			$('#errMsg').html(msg).show();
			return false;
		}
		var dataModel = new Object();
		if(dataShop != null && dataShop.length>0){
			var lstShop = dataShop[0].id;
			for(var i=1;i<dataShop.length;i++){
				lstShop += ","+dataShop[i].id;
			}
			dataModel.strListShopId = lstShop;
		}
		dataModel.fromDate = $('#fDate').val().trim();
		dataModel.toDate = $('#tDate').val().trim();
		dataModel.staffCode = $('#staffCode').val().trim();
		dataModel.staffOwnerCode = $('#staffOwnerCode').val().trim();
		dataModel.fromHour = $('#fromHour').val().trim();
		dataModel.fromMinute = $('#fromMinute').val().trim();
		dataModel.toHour = $('#toHour').val().trim();
		dataModel.toMinute = $('#toMinute').val().trim();
		ReportUtils.exportReport('/report/superivse/dmvs-nvbh/export',dataModel);

	},
	/* BAO CAO THOI GIAN GHE THAM CUA NVBH  GS1.2*/
	reportBCTGGTNVBH : function(){
		var msg = '';
		$('#errMsg').html('').hide();
		if ($('#shopId').val().trim().length == 0 || $('#shopId').val().trim() == activeType.ALL) {
			msg = format(msgErr_required_field,'đơn vị');
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfRequireCheck('fDate','Từ ngày');
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfRequireCheck('tDate','�?ến ngày');
		}
		if(msg.length > 0){
			$('#errMsg').html(msg).show();
			return false;
		}
		if (!checkDateForCycle()) {
			return;
		}
		var dataModel = new Object();
		dataModel.strListShopId=$('#shopId').val();
		dataModel.fromDate = $('#fDate').val().trim();
		dataModel.toDate = $('#tDate').val().trim();
		dataModel.cycleId = $('#cycle').val();
		dataModel.staffType = $('#staffType').val();
		ReportUtils.exportReport('/report/superivse/tggt-nvbh/export',dataModel);
	},
	
	/* BAO CAO THOI GIAN GHE THAM CUA NVBH_1 */
	/* CuongND */
	/* @vuongmq update: 31/03/2014
	 * xuat excel 2007
	 */
	reportBCTGGTNVBH_1 : function(){
		var msg = '';
		$('#errMsg').html('').hide();
		
		var multiShop = $("#shop").data("kendoMultiSelect");
		var dataShop = multiShop.dataItems();
		
		if(dataShop.length <= 0){
			msg = format(msgErr_required_field,'�?ơn vị');
		}
//		if(msg.length == 0){
//			msg = Utils.getMessageOfRequireCheck('shop','�?ơn vị');
//		}
		if(msg.length == 0){
			msg = Utils.getMessageOfRequireCheck('fromHour','Từ gi�?');
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfRequireCheck('fromMinute','Từ phút');
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfRequireCheck('toHour','�?ến gi�?');
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfRequireCheck('toMinute','�?ến phút');
		}
		if ($('#fromDate').val()!= "" && $('#toDate').val()!= ""){
			var fDate = $('#fromDate').val();
			var tDate = $('#toDate').val();
			if(!Utils.compareDate(fDate, tDate)){
				msg = msgErr_fromdate_greater_todate;		
				$('#errMsg').html(msg).show();
				$('#fromDate').focus();
				return false;
			}
		}
		if ($('#fromMinute').val()!= ""){
			if (parseInt($('#fromMinute').val())< 0 || parseInt($('#fromMinute').val()) > 59)
			{
				$('#errMsg').html('Giá trị phút nằm trong khoảng [0-59]. M�?i bạn nhập lại.').show();
				$('#fromMinute').focus();
				return false;
			}
		}
		if ($('#toMinute').val()!= ""){
			if (parseInt($('#toMinute').val())< 0 || parseInt($('#toMinute').val()) > 59)
			{
				$('#errMsg').html('Giá trị phút nằm trong khoảng [0-59]. M�?i bạn nhập lại.').show();
				$('#toMinute').focus();
				return false;
			}
		}
		if ($('#fromHour').val()!=null){
			if (parseInt($('#fromHour').val())< 0 || parseInt($('#fromHour').val()) > 23)
			{
				$('#errMsg').html('Giá trị gi�? nằm trong khoảng [0-23]. M�?i bạn nhập lại.').show();
				$('#fromHour').focus();
				return false;
			}
		}
		if ($('#toHour').val()!=null){
			if (parseInt($('#toHour').val())< 0 || parseInt($('#toHour').val()) > 23)
			{
				$('#errMsg').html('Giá trị gi�? nằm trong khoảng [0-23]. M�?i bạn nhập lại.').show();
				$('#toHour').focus();
				return false;
			}
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfSpecialCharactersValidateEx('fromHour','Từ gi�?',Utils._TF_NUMBER);
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfSpecialCharactersValidateEx('fromMinute','Từ phút',Utils._TF_NUMBER);
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfSpecialCharactersValidateEx('toHour','�?ến gi�?',Utils._TF_NUMBER);
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfSpecialCharactersValidateEx('toMinute','�?ến phút',Utils._TF_NUMBER);
		}
		if(msg.length > 0){
			$('#errMsg').html(msg).show();
			return false;
		}

		var dataModel = new Object();
		if(dataShop!=null && dataShop.length>0){
			var lstShop = dataShop[0].id;
			for(var i=1;i<dataShop.length;i++){
				lstShop += "," + dataShop[i].id;
			}
			dataModel.strListShopId = lstShop;
		}
		//dataModel.shopId = $('#shop').combotree('getValue');
		dataModel.fromDate = $('#fromDate').val();
		dataModel.toDate = $('#toDate').val();
		dataModel.fromHour = $('#fromHour').val();
		dataModel.toHour = $('#toHour').val();
		dataModel.fromMinute = $('#fromMinute').val();
		dataModel.toMinute = $('#toMinute').val();
		dataModel.lstNVGSCode = $('#staffOwnerCode').val().trim();
		dataModel.lstNVBHCode = $('#staffSaleCode').val().trim();
		ReportUtils.exportReport('/report/superivse/tggt-nvbh-1/export',dataModel);
	},
	viewTimeVisitCustomer: function(){
		var msg = '';
		$('#errMsg').html('').hide();
		if ($('#shopId').val().trim().length == 0 || $('#shopId').val().trim() == activeType.ALL) {
			msg = format(msgErr_required_field,'đơn vị');
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfRequireCheck('fDate','Từ ngày');
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfRequireCheck('tDate','�?ến ngày');
		}
		if(msg.length > 0){
			$('#errMsg').html(msg).show();
			return false;
		}
		if (!checkDateForCycle()) {
			return;
		}
		var dataModel = new Object();
		dataModel.strListShopId=$('#shopId').val();
		dataModel.fromDate = $('#fDate').val().trim();
		dataModel.toDate = $('#tDate').val().trim();
		dataModel.cycleId = $('#cycle').val();
		ReportUtils.exportReport('/report/superivse/view',dataModel);
	},
	viewBCKHTT: function(){
		var msg = '';
		$('#errMsg').html('').hide();
		if ($('#shopId').val().trim().length == 0 || $('#shopId').val().trim() == activeType.ALL) {
			msg = format(msgErr_required_field,'đơn vị');
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfRequireCheck('fDate','Từ ngày');
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfRequireCheck('tDate','�?ến ngày');
		}
		if(msg.length > 0){
			$('#errMsg').html(msg).show();
			return false;
		}
		if (!checkDateForCycle()) {
			return;
		}
		var dataModel = new Object();
		dataModel.strListShopId=$('#shopId').val();
		dataModel.fromDate = $('#fDate').val().trim();
		dataModel.toDate = $('#tDate').val().trim();
		dataModel.cycleId = $('#cycle').val();
		ReportUtils.exportReport('/report/superivse/export-bcnvkgtkhtt',dataModel);
	},
	selectCheckbox : function (id,code,type){
		if (type==1){ //nvGS
			if ($('.fancybox-inner #fgid_'+id).is(':checked')){
				SuperviseCustomer.mapNVGS.put(id,code);
			}else{
				SuperviseCustomer.mapNVGS.remove(id);
			}
		}else if (type==2 || type==0){ //NVBH
			if ($('.fancybox-inner #fgid_'+id).is(':checked')){
				SuperviseCustomer.mapNVBH.put(id,code);
			}else{
				SuperviseCustomer.mapNVBH.remove(id);
			}
		}else if (type==3) //TBHV
		{
			if ($('.fancybox-inner #fgid_'+id).is(':checked')){
				SuperviseCustomer.mapTBHV.put(id,code);
			}else{
				SuperviseCustomer.mapTBHV.remove(id);
			}
		}else if (type==4) //Customer CRM KD12
		{
			if ($('.fancybox-inner #fgid_'+id).is(':checked')){
				SuperviseCustomer.mapCustomer.put(id,code);
			}else{
				SuperviseCustomer.mapCustomer.remove(id);
			}
		}
		
	},
	mySelectfancybox : function(type,codeText, nameText, title, url, callback,	codeFieldText, nameFieldText, arrParam, addressText, addressFieldText) {
		var html = $('#fancybox-nvbh-1').html();
		CommonSearch._arrParams = null;
		$(window).unbind('keyup');
		$.fancybox(html,{
			modal : true,
			title : title,						
			autoResize: false,
			afterShow : function() {
				isFromBKPTHNVGH = false;
				//@CuongND : my try Code <reset tabindex>
				var tabindex = -1;
				$('.InputTextStyle, select, input, button , li , tr, td, th, label, ul, a, img').each(function () {
					if (this.type != 'hidden') {
						$(this).attr("tabindex", tabindex);
						tabindex -=1;
					}
				});
				//end <reset tabindex>
				$('#fancybox-nvbh-1').html('');
				if (type==1) $('.fancybox-inner #loai').val(1);      //NVBH_1
				else if (type==2) $('.fancybox-inner #loai').val(2); //NVBH_1
				else if (type==3) $('.fancybox-inner #loai').val(3); //TBHV
				else if (type==4) $('.fancybox-inner #loai').val(4); //CRM KD10 product
				$('.fancybox-inner #seachStyle1Code').focus();
				$('.fancybox-inner #my-searchStyle1Url').val(url);
				
				$('.fancybox-inner #my-searchStyle1CodeText').val(codeFieldText);
				$('.fancybox-inner #my-searchStyle1NameText').val(nameFieldText);
				if (type==2){
					$('.fancybox-inner #myFancyboxSelect').bind('click',function(){
						var stR = "";
						for (var i=0; i< SuperviseCustomer.mapNVBH.size(); i ++){
							stR += SuperviseCustomer.mapNVBH.valArray[i];
							if (i != SuperviseCustomer.mapNVBH.size-1) stR +=";";
						}
						$('#staffSaleCode').val(stR);
						$.fancybox.close();
					});
				}else if (type == 1){
					$('.fancybox-inner #myFancyboxSelect').bind('click',function(){
						$.fancybox.close();
						var stR = "";
						for (var i=0; i< SuperviseCustomer.mapNVGS.size(); i ++){
							stR += SuperviseCustomer.mapNVGS.valArray[i];
							if (i != SuperviseCustomer.mapNVGS.size-1) stR +=";";
						}
						$('#staffOwnerCode').val(stR);
						$.fancybox.close();
					});
				}else if(type == 3) //TBHV
				{
					$('.fancybox-inner #myFancyboxSelect').bind('click',function(){
						var stR = "";
						for (var i=0; i< SuperviseCustomer.mapTBHV.size(); i ++){
							stR += SuperviseCustomer.mapTBHV.valArray[i];
							if (i != SuperviseCustomer.mapTBHV.size -1) stR +=";";
						}
						$('#staffTBHV').val(stR);
						$.fancybox.close();
					});
				}else if(type == 4) //Product CRM KD10
				{
					$('.fancybox-inner #myFancyboxSelect').bind('click',function(){
						var stR = "";
						for (var i=0; i< SuperviseCustomer.mapCustomer.size(); i ++){
							stR += SuperviseCustomer.mapCustomer.valArray[i];
							if (i != SuperviseCustomer.mapCustomer.size-1) stR +=";";
						}
						$('#customerCode').val(stR);
						$.fancybox.close();
					});
				} 
				if(addressText != null && addressText != undefined && addressFieldText != null && addressFieldText != undefined) {
					$('.fancybox-inner #my-searchStyle1AddressText').val(addressFieldText);
					$('.fancybox-inner #seachStyle1AddressLabel').show();
					$('.fancybox-inner #seachStyle1Address').show();
					$('.fancybox-inner #btnSearchStyle1').css('margin-left', 45);
				}
				CommonSearch._currentSearchCallback = callback;
				CommonSearch._arrParams = arrParam;
				var codeField = 'code';
				var nameField = 'name';
				var addressField = 'address';
				if (codeFieldText != null && codeFieldText != undefined && codeFieldText.length > 0) {
					codeField = codeFieldText;
				}
				if (nameFieldText != null&& nameFieldText != undefined && nameFieldText.length > 0) {
					nameField = nameFieldText;
				}
				if(addressText != null && addressText != undefined && addressFieldText != null && addressFieldText != undefined) {
					addressField = addressFieldText;
				}
				$('.fancybox-inner #seachStyle1CodeLabel').html(codeText);
				$('.fancybox-inner #seachStyle1NameLabel').html(nameText);
				$('.fancybox-inner #seachStyle1AddressLabel').html(addressText);
				
				Utils.bindAutoSearch();
				if(addressText != null && addressText != undefined && addressFieldText != null && addressFieldText != undefined) {
					$(".fancybox-inner #my-searchStyle1Grid").jqGrid({
						url : CommonSearch.getSearchStyle1GridUrl(url, '', '', arrParam, ''),
						colModel : [{name : codeField,index : codeField,align : 'left',label : codeText,width : 100,sortable : false,resizable : false, formatter : CommonSearchFormatter.myCode},
						            {name : nameField,index : nameField,label : nameText,sortable : false,resizable : false,align : 'left' ,formatter : CommonSearchFormatter.myName},
						            {name : addressField,index : addressField,label : addressText,sortable : false,resizable : false,align : 'left'},
						            {name : 'select',label : 'Ch�?n',width : 50,align : 'center',sortable : false,resizable : false,formatter : CommonSearchFormatter.selectCellIconFormatter_nvbh_1},
						            {name : 'id',index : 'id',hidden : true}
						            ],
						            pager : $('.fancybox-inner #my-searchStyle1Pager'),
									height : 'auto',rowNum : 10,width : ($('.fancybox-inner #my-searchStyle1ContainerGrid').width()),
									gridComplete : function() {
										$('#jqgh_searchStyle1Grid_rn').html('STT');
										$('#pg_my-searchStyle1Pager').css('padding-right','20px');																	
										tabindex = 1;
										$('.fancybox-inner input,.fancybox-inner select,.fancybox-inner button').each(function () {
											if (this.type != 'hidden') {
											$(this).attr("tabindex", '');
											tabindex++;
											}
										});
										updateRownumWidthForJqGrid('.fancybox-inner');
										$('#my-searchStyle1Pager_right').css('width','180px');//@LamNH:fix bug 0003172
										
										//$.fancybox.update();
										$(window).resize();
										
										var fancyHeight = $('.fancybox-wrap.fancybox-desktop.fancybox-type-html.fancybox-opened').css('height');
										fancyHeight = parseInt(fancyHeight);
										var wWidth = window.innerWidth;
										wWidth = parseInt(wWidth);
										var wHeight = window.innerHeight;
										wHeight = parseInt(wHeight);
										var distant = wHeight - fancyHeight;
										distant = distant / 2;
										if(wHeight > fancyHeight) {
											var tm = setTimeout(function() {
												//$('.fancybox-wrap.fancybox-desktop.fancybox-type-html.fancybox-opened').css('top', document.documentElement.scrollTop +  distant);
												$(window).resize();
												$('.fancybox-wrap.fancybox-desktop.fancybox-type-html.fancybox-opened').animate({'top': document.documentElement.scrollTop +  distant}, 50);
											}, 10);				
										} else {
											var tm = setTimeout(function() {
												//$('.fancybox-wrap.fancybox-desktop.fancybox-type-html.fancybox-opened').css('top', document.documentElement.scrollTop +  distant);
												$(window).resize();
												$('.fancybox-wrap.fancybox-desktop.fancybox-type-html.fancybox-opened').animate({'top': document.documentElement.scrollTop}, 50);
											}, 10);	
										}
										//TODO : the hien checkbox cua fancybox
										if (type==2 || type == 0){
											for (var i=0; i< SuperviseCustomer.mapNVBH.keyArray.length; i++){
												$('#fgid_'+SuperviseCustomer.mapNVBH.keyArray[i]).attr('checked','checked');
											}
										}else if (type==1){
											for (var i=0; i< SuperviseCustomer.mapNVGS.keyArray.length; i++){
												$('#fgid_'+SuperviseCustomer.mapNVGS.keyArray[i]).attr('checked','checked');
											}
										}else if (type==3){
											for (var i=0; i< SuperviseCustomer.mapTBHV.keyArray.length; i++){
												$('#fgid_'+SuperviseCustomer.mapTBHV.keyArray[i]).attr('checked','checked');
											}
										}else if (type==4){
											for (var i=0; i< SuperviseCustomer.mapCustomer.keyArray.length; i++){
												$('#fgid_'+SuperviseCustomer.mapCustomer.keyArray[i]).attr('checked','checked');
											}
										}
									}												
								}).navGrid($('.fancybox-inner #my-searchStyle1Pager'),{edit : false,add : false,del : false,search : false});
				} else {
					$(".fancybox-inner #my-searchStyle1Grid").jqGrid({
						url : CommonSearch.getSearchStyle1GridUrl(url, '', '', arrParam),
						colModel : [{name : codeField,index : codeField,align : 'left',label : codeText,width : 100,sortable : false,resizable : false,formatter : CommonSearchFormatter.myCode},
						            {name : nameField,index : nameField,label : nameText,sortable : false,resizable : false,align : 'left',formatter : CommonSearchFormatter.myName},
						            {name : 'select',label : 'Ch�?n',width : 50,align : 'center',sortable : false,resizable : false,formatter : CommonSearchFormatter.selectCellIconFormatter_nvbh_1},
						            {name : 'id',index : 'id',hidden : true}
						            ],
						            pager : $('.fancybox-inner #my-searchStyle1Pager'),
									height : 'auto',rowNum : 10,width : ($('.fancybox-inner #my-searchStyle1ContainerGrid').width()),
									gridComplete : function() {
										$('#jqgh_searchStyle1Grid_rn').html('STT');
										$('#pg_my-searchStyle1Pager').css('padding-right','20px');																	
										tabindex = 1;
										$('.fancybox-inner input,.fancybox-inner select,.fancybox-inner button').each(function () {
											if (this.type != 'hidden') {
											$(this).attr("tabindex", '');
											tabindex++;
											}
										});
										updateRownumWidthForJqGrid('.fancybox-inner');
										$('#my-searchStyle1Pager_right').css('width','180px');
										
										//$.fancybox.update();
										$(window).resize();
										if (type==2 || type == 0){
											for (var i=0; i< SuperviseCustomer.mapNVBH.keyArray.length; i++){
												$('#fgid_'+SuperviseCustomer.mapNVBH.keyArray[i]).attr('checked','checked');
											}
										}else if (type==1){
											for (var i=0; i< SuperviseCustomer.mapNVGS.keyArray.length; i++){
												$('#fgid_'+SuperviseCustomer.mapNVGS.keyArray[i]).attr('checked','checked');
											}
										}else if (type==3){
											for (var i=0; i< SuperviseCustomer.mapTBHV.keyArray.length; i++){
												$('#fgid_'+SuperviseCustomer.mapTBHV.keyArray[i]).attr('checked','checked');
											}
										}else if (type==4){  //CRM KD12
											for (var i=0; i< SuperviseCustomer.mapCustomer.keyArray.length; i++){
												$('#fgid_'+SuperviseCustomer.mapCustomer.keyArray[i]).attr('checked','checked');
											}
										}
									}												
								}).navGrid($('.fancybox-inner #my-searchStyle1Pager'),{edit : false,add : false,del : false,search : false});
				}
				$('.fancybox-inner #btnSearchStyle1').bind('click',function(event) {
					var code = $('.fancybox-inner #seachStyle1Code').val().trim();
					var name = $('.fancybox-inner #seachStyle1Name').val().trim();
					//var address = $('.fancybox-inner #seachStyle1Address').val().trim();
					var url = '';
					if(addressText != null && addressText != undefined && addressFieldText != null && addressFieldText != undefined) {
						url = CommonSearch.getSearchStyle1GridUrl($('.fancybox-inner #my-searchStyle1Url').val().trim(),code,name,CommonSearch._arrParams,address);
					} else {
						url = CommonSearch.getSearchStyle1GridUrl($('.fancybox-inner #my-searchStyle1Url').val().trim(),code,name,CommonSearch._arrParams);
					}
					$(".fancybox-inner #my-searchStyle1Grid").setGridParam({url : url,page : 1}).trigger("reloadGrid");
					$('.fancybox-inner #seachStyle1Code').focus();
				});
				//$.fancybox.update();
			},
			afterClose : function() {
				isFromBKPTHNVGH = true;//lampv
				var tabindex = 1;
				$(' .InputTextStyle, select, input, button , li , tr, td, th, label, ul, a, img').each(function () {
					if (this.type != 'hidden') {
						$(this).attr("tabindex", '');
					}
					tabindex ++;
				});	
				var curIdFocus = $('#cur_focus').val();
				$('#'+ curIdFocus).focus();
				CommonSearch._currentSearchCallback = null;
				$('#fancybox-nvbh-1').html(html);
				$(window).bind('keyup',function(event){
					if(event.keyCode == keyCodes.ENTER){		   				
						$('#reportContainer .BtnGeneralStyle').click();
					}
				});
			}
		});
		return false;
	},
	
	/* BAO CAO THOI GIAN TAT MAY */	
	//HUYNP4
	reportBCTGTM : function(){
		var msg = '';
		var dataShop = $("#shopId").val();
		$('#errMsg').html('').hide();
		
		if(!dataShop || dataShop <= 0){
			msg = format(msgErr_required_field,'đơn vị');
		}
		
		if(msg.length == 0){
			msg = Utils.getMessageOfRequireCheck('minutes','số phút vượt quá');
		}
		if(msg.length == 0 && $('#minutes').val().trim().length >0){
			msg = Utils.getMessageOfSpecialCharactersValidateEx('minutes','Số phút vượt quá',Utils._TF_NUMBER,null);
		}
		var fDate = $('#fDate').val();
		var tDate = $('#tDate').val();
		if(msg.length == 0){
			msg = Utils.getMessageOfRequireCheck('fDate','từ ngày');
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfInvalidFormatDate('fDate','từ ngày');
		}
		if(msg.length == 0){
			if(!Utils.compareCurrentDate(fDate)){
				msg = "Từ ngày phải bé hơn hoặc bằng ngày hiện tại.";
			}
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfRequireCheck('tDate','đến ngày');
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfInvalidFormatDate('tDate','đến ngày');
		}
		if(msg.length == 0){
			if(!Utils.compareCurrentDate(tDate)){
				msg = "Đến ngày phải bé hơn hoặc bằng ngày hiện tại.";
			}
		}
		if(msg.length == 0){			
			if(!Utils.compareDate(fDate, tDate)){
				msg = msgErr_fromdate_greater_todate;
			}
		}
		var apminutes = '0';
		var minutes=$('#minutes').val().trim();
		if(msg.length == 0){
			if(apminutes!='' && minutes!=''){
				if(parseInt(apminutes) >= parseInt(minutes)){
				msg = "Số phút tắt vượt quá phải lớn hơn " + apminutes + ".";
				}
			}
		}
		if(msg.length > 0){
			$('#errMsg').html(msg).show();
			return false;
		}
		var dataModel = new Object();
		dataModel.shopId = dataShop;
		dataModel.minutes = $('#minutes').val().trim();
		dataModel.fromDate = $('#fDate').val().trim();
		dataModel.toDate = $('#tDate').val().trim();
		ReportUtils.exportReport('/report/superivse/bctgtm/export', dataModel);
	},
	exportBCDSBHSKUCTTKH: function(){
		
		var msg = '';
		var multiShop = $("#shop").data("kendoMultiSelect");
		var dataShop = multiShop.dataItems();
		$('#errMsg').html('').hide();
		/*if(msg.length == 0){
			msg = Utils.getMessageOfRequireCheck('shopId','�?ơn vị');
		}*/ 
		
		if(dataShop.length <= 0){
			msg = format(msgErr_required_field,'�?ơn vị');
		}
		
		var fDate = $('#fDate').val();
		var tDate = $('#tDate').val();
		if(msg.length == 0){
			msg = Utils.getMessageOfRequireCheck('fDate','Từ ngày');
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfInvalidFormatDate('fDate','Từ ngày');
		}
		if(msg.length == 0){
			if(!Utils.compareCurrentDate(fDate)){
				msg = "Từ ngày phải bé hơn hoặc bằng ngày hiện tại.";
			}
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfRequireCheck('tDate','�?ến ngày');
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfInvalidFormatDate('tDate','�?ến ngày');
		}
		if(msg.length == 0){
			if(!Utils.compareCurrentDate(tDate)){
				msg = "�?ến ngày phải bé hơn hoặc bằng ngày hiện tại.";
			}
		}
		if(msg.length == 0){			
			if(!Utils.compareDate(fDate, tDate)){
				msg = msgErr_fromdate_greater_todate;
			}
		}
		
		if(msg.length > 0){
			$('#errMsg').html(msg).show();
			return false;
		}
		
		var dataModel = new Object();
		dataModel.shopId = $('#curShopId').val().trim();
		if(dataShop!=null && dataShop.length>0){
			var lstShop = dataShop[0].id;
			for(var i=1;i<dataShop.length;i++){
				lstShop += "," + dataShop[i].id;
			}
			dataModel.lstShopId = lstShop;
		}
		dataModel.fromDate = $('#fDate').val().trim();
		dataModel.toDate = $('#tDate').val().trim();
		dataModel.shopCode = $('#shopCode').val().trim();		
		ReportUtils.exportReport('/report/superivse/bcdsbhskucttkh/export',dataModel);
	},
	//Bao cao DS1
	exportBCDSBHSKUCTTNVBH: function(){
		
		var msg = '';
		var multiShop = $("#shop").data("kendoMultiSelect");
		var dataShop = multiShop.dataItems();
		$('#errMsg').html('').hide();
		if(dataShop==null||dataShop.length==0){
			msg = format(msgErr_required_field,'�?ơn vị');
		}
		var fDate = $('#fDate').val();
		var tDate = $('#tDate').val();
		if(msg.length == 0){
			msg = Utils.getMessageOfRequireCheck('fDate','Từ ngày');
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfInvalidFormatDate('fDate','Từ ngày');
		}
		if(msg.length == 0){
			if(!Utils.compareCurrentDate(fDate)){
				msg = "Từ ngày phải bé hơn hoặc bằng ngày hiện tại.";
			}
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfRequireCheck('tDate','�?ến ngày');
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfInvalidFormatDate('tDate','�?ến ngày');
		}
		if(msg.length == 0){
			if(!Utils.compareCurrentDate(tDate)){
				msg = "�?ến ngày phải bé hơn hoặc bằng ngày hiện tại.";
			}
		}
		if(msg.length == 0){			
			if(!Utils.compareDate(fDate, tDate)){
				msg = msgErr_fromdate_greater_todate;
			}
		}
		
		if(msg.length > 0){
			$('#errMsg').html(msg).show();
			return false;
		}
		var dataModel = new Object();
		if(dataShop!=null && dataShop.length>0){
			var lstShop = dataShop[0].id;
			for(var i=1;i<dataShop.length;i++){
				lstShop += "," + dataShop[i].id;
			}
			dataModel.strListShopId = lstShop;
		}
		//dataModel.shopId = $('#shopId').val().trim();				
		if(dataShop!=null && dataShop.length>0){
			var lstShop = dataShop[0].id;
			for(var i=1;i<dataShop.length;i++){
				lstShop += "," + dataShop[i].id;
			}
			dataModel.listShopId = lstShop;
		}
		dataModel.fromDate = $('#fDate').val().trim();
		dataModel.toDate = $('#tDate').val().trim();
		dataModel.shopCode = $('#shopCode').val().trim();		
		ReportUtils.exportReport('/report/superivse/bcdsbhskucttnvbh/export',dataModel);
	},

	treeProduct : function(){
		var html = $('#productTreeDialog').html();
		$.fancybox(html,
				{
					modal: true,
					title: 'Thêm thông tin sản phẩm',
					afterShow: function(){
						Utils.resetCheckboxStateOfTree();
						Utils.bindAutoSearch();	
						$('.fancybox-inner #treeGroup').hide();
						$('.fancybox-inner #treeQuotaGroup').hide();
						$('#productTreeDialog').html('');
						
						$('.fancybox-inner #btnSearchProductTree').bind('click',function(){
							//ProgrammeDisplayCatalog.searchProductOnTree();
							$('#productTree li').each(function(){
								var _id = $(this).attr('id');
								var type = $(this).attr('contentitemid');
								if($(this).hasClass('jstree-checked') && type=='product'){
									ProgrammeDisplayCatalog._mapProduct.put(_id,_id);		
								}else {
									ProgrammeDisplayCatalog._mapProduct.remove(_id);
								}
							});
							var productCode = encodeChar($('#productCodeDlg').val().trim());
							var productName = encodeChar($('#productNameDlg').val().trim());
							$('#productTreeContent').data('jsp').destroy();
							ProgrammeDisplayCatalog.loadDataForTreeWithCheckboxExCRM_SKU('/rest/catalog/display-program/product/list.json?displayProgramId='+ 0 +'&productCode='+ productCode +'&productName='+ productName + '&nodeType=cat&catId=0&subCatId=0','productTree');
							setTimeout(function() {
								$('#productTreeContent').jScrollPane();
								$('#productTree').bind("loaded.jstree open_node.jstree close_node.jstree", function (event, data) {
									//Utils.applyCheckboxStateOfTree();
									if($('#productTreeContent').data('jsp') != undefined || $('#productTreeContent').data('jsp') != null) {
										$('#productTreeContent').data('jsp').destroy();
										setTimeout(function(){
											$('#productTreeContent').jScrollPane();
										},500);	
									} else {
										setTimeout(function(){
											$('#productTreeContent').jScrollPane();
										},500);	
									}
									SuperviseCustomer.loadCheckStateCRM_KD1(SuperviseCustomer.mapProduct,"product",'productTree li');
								});
								SuperviseCustomer.loadCheckStateCRM_KD1(SuperviseCustomer.mapProduct,"product",'productTree li');
							}, 500);
						});
						// Cuong ND edit code -----------------------------------
						SuperviseCustomer.loadCheckStateCRM_KD1(SuperviseCustomer.mapProduct,"product",'productTree li');
						$('.fancybox-inner #btnQ').bind('click',function(){
							$('li li li').each(function(){
							    if($(this).hasClass('jstree-checked')){
							    	var id = $(this).attr('id');
							    	var str = $('#'+id +' a').text().trim();
							    	str = str.substring(0,str.indexOf('-'));
							    	SuperviseCustomer.mapProduct.put(id,str.trim());
							    }
							});
							var string = "";
							for (var i=0; i< SuperviseCustomer.mapProduct.valArray.length; i++){
								if (i == 0) {
									string += SuperviseCustomer.mapProduct.valArray[i];
								}else{
									string += ";"+SuperviseCustomer.mapProduct.valArray[i];
								}
							}
							$('#product').val(string);
							$.fancybox.close();
						});
						//--------------------------------------------------------------
						var displayId = $('#selId').val();
						var productCode = encodeURI('');
						var productName = encodeURI('');
						ProgrammeDisplayCatalog.loadDataForTreeWithCheckboxExCRM_SKU('/rest/catalog/display-program/product/list.json?displayProgramId=0&productCode='+ productCode +'&productName='+ productName + '&nodeType=cat&catId=0&subCatId=0','productTree');
						ProgrammeDisplayCatalog._mapProduct = new Map();
						$('#productTree').bind("loaded.jstree open_node.jstree close_node.jstree", function (event, data) {
							if($('#productTreeContent').data('jsp') != undefined || $('#productTreeContent').data('jsp') != null) {
								$('#productTreeContent').data('jsp').destroy();
								setTimeout(function(){
									$('#productTreeContent').jScrollPane();
								},500);	
							} else {
								setTimeout(function(){
									$('#productTreeContent').jScrollPane();
								},500);	
							}
							//$('#productTreeContent .jspContainer').css('width','500px');							
						});
						ProgrammeDisplayCatalog.openNodeOnTree('productTree');
					},
					afterClose: function(){
						$('#productTreeDialog').html(html);				
					}
				}
			);
		return false;
	},
	selectListProduct : function(){
		
	},
	changeMyMap : function(type){
		switch(type){
		case 1: //NVGS
			var str = $('#staffOwnerCode').val();
			tMap = new Map();
			if(str == ""){
				SuperviseCustomer.mapNVGS.clear();
			}else{
				var code; var index;
				while(str.length > 0){
					if (str.indexOf(';')== -1){
						code = str;
					}
					else code = str.substring(0,str.indexOf(';'));
					index = SuperviseCustomer.mapNVGS.findIt4Val(code);
					if (index != -1){
						tMap.put(SuperviseCustomer.mapNVGS.keyArray[index],code);
					}
					if (str.indexOf(';') != -1) {
						str = str.substring(str.indexOf(';')+1,str.length);
					}else{
						str = "";
					}
				}
				SuperviseCustomer.mapNVGS = tMap;
			}
			break;
		case 2: //NVBH
			var str = $('#staffSaleCode').val();
			tMap = new Map();
			if(str == ""){
				SuperviseCustomer.mapNVBH.clear();
			}else{
				var code; var index;
				while(str.length > 0){
					if (str.indexOf(';')== -1){
						code = str;
					}
					else code = str.substring(0,str.indexOf(';'));
					index = SuperviseCustomer.mapNVBH.findIt4Val(code);
					if (index != -1){
						tMap.put(SuperviseCustomer.mapNVBH.keyArray[index],code);
					}
					if (str.indexOf(';')!= -1) {
						str = str.substring(str.indexOf(';')+1,str.length);
					}else{
						str = "";
					}
				}
				SuperviseCustomer.mapNVBH = tMap;
			}
			break;
		case 3: //TBHV
			var str = $('#staffTBHV').val();
			tMap = new Map();
			if(str == ""){
				SuperviseCustomer.mapTBHV.clear();
			}else{
				var code; var index;
				while(str.length > 0){
					if (str.indexOf(';')== -1){
						code = str;
					}
					else code = str.substring(0,str.indexOf(';'));
					index = SuperviseCustomer.mapTBHV.findIt4Val(code);
					if (index != -1){
						tMap.put(SuperviseCustomer.mapTBHV.keyArray[index],code);
					}
					if (str.indexOf(';')!= -1) {
						str = str.substring(str.indexOf(';')+1,str.length);
					}else{
						str = "";
					}
				}
				SuperviseCustomer.mapTBHV = tMap;
			}
			break;
		case 4: //Customer
			var str = $('#customerCode').val();
			tMap = new Map();
			if(str == ""){
				SuperviseCustomer.mapCustomer.clear();
			}else{
				var code; var index;
				while(str.length > 0){
					if (str.indexOf(';')== -1){
						code = str;
					}
					else code = str.substring(0,str.indexOf(';'));
					index = SuperviseCustomer.mapCustomer.findIt4Val(code);
					if (index != -1){
						tMap.put(SuperviseCustomer.mapCustomer.keyArray[index],code);
					}
					if (str.indexOf(';')!= -1) {
						str = str.substring(str.indexOf(';')+1,str.length);
					}else{
						str = "";
					}
				}
				SuperviseCustomer.mapCustomer = tMap;
			}
			break;
		}
	},
	loadCheckStateCRM_KD1:function(MAP,nodeType,selector){
		for(var i=0;i<MAP.keyArray.length;++i){
			var _obj = MAP.keyArray[i];					
			$('#' + selector).each(function(){
				var _id = $(this).attr('id');
				var type = $(this).attr('contentitemid');
				if(_id==_obj && nodeType==type ){
					$(this).removeClass('jstree-unchecked');
					$(this).addClass('jstree-checked');
				}
			});
		}
	},
	//Hunglm16 January 15,2014
	reportBCTGBHHQCNVBH_DS3 : function(){
		//DS3 - Bao cao thoi gian ban hang hieu qua cua nhan vien ban hang
		var msg = '';
		$('#errMsg').html('').hide();
		var multiShop = $("#shop").data("kendoMultiSelect");
		var dataShop = multiShop.dataItems();
		var fromDate = $('#fromDate').val();
		if(dataShop == null || dataShop.length ==0){
			msg = "Bạn chưa ch�?n đơn vị";
		}
		/*if(msg.length == 0 && $('#staffOwnerCode').val().trim().length >0) {
			msg = Utils.getMessageOfSpecialCharactersValidate('staffOwnerCode','Mã nhân viên giám sát',Utils._CODE);
		}	
		if(msg.length == 0 && $('#staffCode').val().trim().length >0){ 
			msg = Utils.getMessageOfSpecialCharactersValidate('staffCode','Mã nhân viên bán hàng',Utils._CODE);
		}*/
		if(msg.length == 0){
			msg = Utils.getMessageOfRequireCheck('fromDate','Ngày');
		}
		if(msg.length == 0){
			if (fromDate != '' && !Utils.isDate(fromDate)) {
				msg = 'Ngày không hợp lệ, vui lòng nhập theo định dạng dd/MM/yyyy';
			}
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfRequireCheck('fromHour','Từ gi�?');
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfInvaildInteger('fromHour','Từ gi�?');
		}
		if(msg.length ==0){
			var value = $('#fromHour').val();
			if(parseInt(value,10) <0 || parseInt(value,10) >23){
				msg = "Từ gi�? phải từ 0 đến 23.";
			}
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfRequireCheck('toHour','�?ến gi�?');
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfInvaildInteger('toHour','�?ến gi�?');
		}
		if(msg.length ==0){
			var value = $('#toHour').val();
			if(parseInt(value,10) <0 || parseInt(value,10) >23){
				msg = "�?ến gi�? phải từ 0 đến 23.";
			}
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfRequireCheck('fromMinute','Từ phút');
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfInvaildInteger('fromMinute','Từ phút');
		}
		if(msg.length ==0){
			var value = $('#fromMinute').val();
			if(parseInt(value,10) <0 || parseInt(value,10) >59){
				msg = "Từ phút phải từ 0 đến 59.";
			}
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfRequireCheck('toMinute','�?ến phút');
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfInvaildInteger('toMinute','�?ến phút');
		}
		if(msg.length ==0){
			var value = $('#toMinute').val();
			if(parseInt(value,10) <0 || parseInt(value,10) >59){
				msg = "�?ến phút phải từ 0 đến 59.";
			}
		}
		if(msg.length == 0){
			
		}
		if(msg.length==0){
			var toHour=$('#toHour').val();
			var fromHour=$('#fromHour').val();
			if(parseInt(fromHour)==parseInt(toHour)){
				var fMin = $('#fromMinute').val();
				var tMin = $('#toMinute').val();
				if(parseInt(fMin)>parseInt(tMin)){
					msg = "Từ phút không được lớn hơn đến phút.";
				}
			}else if(parseInt(fromHour)>parseInt(toHour)){
				msg = "Từ gi�? không được lớn hơn đến gi�?.";
			}
		}
		if(msg.length > 0){
			$('#errMsg').html(msg).show();
			return false;
		}
		var dataModel = new Object();		
		//dataModel.shopId = $('.combo-value').val();
		if(dataShop!=null && dataShop.length>0){
			var lstShop = dataShop[0].id;
			for(var i=1;i<dataShop.length;i++){
				lstShop += "," + dataShop[i].id;
			}
			dataModel.strListShopId = lstShop;
		}
		dataModel.staffOwnerCode = $('#staffOwnerCode').val().trim();
		dataModel.staffSaleCode = $('#staffCode').val().trim();
		dataModel.fromDate = $('#fromDate').val();
		dataModel.fromHour = $('#fromHour').val();
		dataModel.toHour = $('#toHour').val();
		dataModel.fromMinute = $('#fromMinute').val();
		dataModel.toMinute = $('#toMinute').val();
		dataModel.sprTime = $('#sprTime').val();
		
		ReportUtils.exportReport('/report/superivse/bctgbhhqnvbh/export', dataModel);
	},
	
	//vuonghn VT7-Bao cao ghe tham khach hang
	exportBCVT7: function(){
		var msg = '';
		$('#errMsg').html('').hide();
		var shopKendo = $("#shop").data("kendoMultiSelect");
		var dataShop = shopKendo.dataItems();
		if(dataShop==null||dataShop.length==0){
			msg = format(msgErr_required_field,'�?ơn vị');
		}
//		if(msg.length == 0 && $('#staffOwnerCode').val().trim().length >0){ 
//			msg = Utils.getMessageOfSpecialCharactersValidate('staffOwnerCode','GSNPP',Utils._CODE);
//		}
		
		if(msg.length == 0){
			msg = Utils.getMessageOfRequireCheck('fromDate','Từ ngày');
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfInvalidFormatDate('fromDate','Từ ngày');
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfRequireCheck('toDate','�?ến ngày');
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfInvalidFormatDate('toDate','�?ến ngày');
		}
		var fDate = $('#fromDate').val();
		var tDate = $('#toDate').val();
		var curDate = ReportUtils.getCurrentDateString();
		if(msg.length == 0 && !Utils.compareDate(fDate, curDate)){
			msg = 'Từ ngày không được lớn hơn ngày hiện tại. Vui lòng nhập lại';
			$('#fromDate').focus();
		}	
		if(msg.length == 0 && !Utils.compareDate(tDate, curDate)){
			msg = '�?ến ngày không được lớn hơn ngày hiện tại. Vui lòng nhập lại';
			$('#toDate').focus();
		}
		if(msg.length == 0 && !Utils.compareDate(fDate, tDate)) {
			msg ='Từ ngày không được lớn hơn đến ngày. Vui lòng nhập lại';
			$('#fromDate').focus();
		}
		if(msg.length > 0){
			$('#errMsg').html(msg).show();
			return false;
		}
		var lstShopId = dataShop[0].id;
		for(var i=1;i<dataShop.length;i++){
			lstShopId += "," + dataShop[i].id;
		}
		var dataModel = new Object();		
		//dataModel.shopCode = $('#shopCode').val().trim();
		dataModel.strListShopId = lstShopId;
		dataModel.staffOwnerCode = $('#staffOwnerCode').val().trim();
		dataModel.fromDate = $('#fromDate').val();
		dataModel.toDate = $('#toDate').val();
		ReportUtils.exportReport('/report/superivse/vt7-bcgtkh/export',dataModel);
	},
	exportVT7_1: function(){
		var msg = '';
		$('#errMsg').html('').hide();
		var shopKendo = $("#shop").data("kendoMultiSelect");
		var dataShop = shopKendo.dataItems();
		if(dataShop==null||dataShop.length==0){
			msg = format(msgErr_required_field,'�?ơn vị');
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfRequireCheck('fromDate','Từ ngày');
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfInvalidFormatDate('fromDate','Từ ngày');
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfRequireCheck('toDate','�?ến ngày');
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfInvalidFormatDate('toDate','�?ến ngày');
		}
		var fDate = $('#fromDate').val();
		var tDate = $('#toDate').val();
		var curDate = ReportUtils.getCurrentDateString();
		if(msg.length == 0 && !Utils.compareDate(fDate, curDate)){
			msg = 'Từ ngày không được lớn hơn ngày hiện tại. Vui lòng nhập lại';
			$('#fromDate').focus();
		}	
		if(msg.length == 0 && !Utils.compareDate(tDate, curDate)){
			msg = '�?ến ngày không được lớn hơn ngày hiện tại. Vui lòng nhập lại';
			$('#toDate').focus();
		}
		if(msg.length == 0 && !Utils.compareDate(fDate, tDate)) {
			msg ='Từ ngày không được lớn hơn đến ngày. Vui lòng nhập lại';
			$('#fromDate').focus();
		}
		if(msg.length > 0){
			$('#errMsg').html(msg).show();
			return false;
		}
		var lstShopId = dataShop[0].id;
		for(var i=1;i<dataShop.length;i++){
			lstShopId += "," + dataShop[i].id;
		}
		var dataModel = new Object();		
		dataModel.strListShopId = lstShopId;
		dataModel.staffOwnerCode = $('#staffOwnerCode').val().trim();
		dataModel.fromDate = $('#fromDate').val();
		dataModel.toDate = $('#toDate').val();
		ReportUtils.exportReport('/report/superivse/vt7_1/export',dataModel);
	},
	exportBCVT6: function(){
		var msg = '';
		var multiShop = $("#shop").data("kendoMultiSelect");
		var dataShop = multiShop.dataItems();
		$('#errMsg').html('').hide();
		if(dataShop.length <= 0){
			msg = format(msgErr_required_field,'�?ơn vị');
		}
			
		if(msg.length == 0){
			msg = Utils.getMessageOfRequireCheck('fromDate','Ngày');
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfInvalidFormatDate('fromDate','Ngày');
		}
		if(msg.length > 0){
			$('#errMsg').html(msg).show();
			return false;
		}
		var dataModel = new Object();
		if(dataShop!=null && dataShop.length>0){
			var lstShop = dataShop[0].id;
			for(var i=1;i<dataShop.length;i++){
				lstShop += "," + dataShop[i].id;
			}
			dataModel.strListShopId = lstShop;
		}
//		dataModel.shopId = $('#shopCode').val().trim();
		dataModel.staffOwnerCode = $('#staffOwnerCode').val().trim();
		dataModel.staffSaleCode = $('#staffSaleCode').val().trim();
		dataModel.fromDate = $('#fromDate').val().trim();
		ReportUtils.exportReport('/report/superivse/bcvt6/export',dataModel);
	},
	//vuongh: VT10 - Bao cao ket qua cham trung bay
	exportBCVT10: function(){
		var msg = '';
		$('#errMsg').html('').hide();
		var shopKendo = $("#shop").data("kendoMultiSelect");
		var lstShopId = shopKendo.value();
		if(lstShopId.length == 0) {
			msg = format(msgErr_required_field,'�?ơn vị');
		}
		/*if(msg.length == 0 && $('#superStaffCode').val().trim().length >0){ 
			msg = Utils.getMessageOfSpecialCharactersValidate('superStaffCode','Mã nhân viên giám sát',Utils._CODE);
		}*/
		if(msg.length == 0){
			msg = Utils.getMessageOfRequireCheck('fromDate','Từ ngày');
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfInvalidFormatDate('fromDate','Từ ngày');
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfRequireCheck('toDate','�?ến ngày');
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfInvalidFormatDate('toDate','�?ến ngày');
		}
		var fDate = $('#fromDate').val();
		var tDate = $('#toDate').val();
		var curDate = ReportUtils.getCurrentDateString();
		if(msg.length == 0 && !Utils.compareDate(fDate, curDate)){
			msg = 'Từ ngày phải nh�? hơn hoặc bằng ngày hiện tại';
			$('#fromDate').focus();
		}	
		if(msg.length == 0 && !Utils.compareDate(tDate, curDate)){
			msg = '�?ến ngày phải nh�? hơn hoặc bằng ngày hiện tại';
			$('#toDate').focus();
		}
		if(!Utils.compareDate(fDate, tDate)) {
			msg ='Từ ngày phải nh�? hơn đến ngày.';
			$('#fromDate').focus();
		}
		if(msg.length > 0){
			$('#errMsg').html(msg).show();
			return false;
		}
		
		var dataModel = new Object();
		
		dataModel.strListShopId = lstShopId.toString();
		dataModel.superStaffCode = $('#superStaffCode').val().trim(); 
		dataModel.fromDate = $('#fromDate').val();
		dataModel.toDate = $('#toDate').val();
		ReportUtils.exportReport('/report/superivse/vt10-bckqctb/export',dataModel);
	},
	
	// vuongmq: vt11 - bao cao nhan vien mo clip
	exportBCVT11: function(){
		var msg = '';
		$('#errMsg').html('').hide(); 
		if(msg.length == 0){
			msg = Utils.getMessageOfRequireCheck('fromDate','Từ ngày');
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfInvalidFormatDate('fromDate','Từ ngày');
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfRequireCheck('toDate','�?ến ngày');
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfInvalidFormatDate('toDate','�?ến ngày');
		}
		var fDate = $('#fromDate').val();
		var tDate = $('#toDate').val();
		var curDate = ReportUtils.getCurrentDateString();
		if(msg.length == 0 && !Utils.compareDate(fDate, curDate)){
			msg = 'Từ ngày phải nh�? hơn hoặc bằng ngày hiện tại';
			$('#fromDate').focus();
		}	
		if(msg.length == 0 && !Utils.compareDate(tDate, curDate)){
			msg = '�?ến ngày phải nh�? hơn hoặc bằng ngày hiện tại';
			$('#toDate').focus();
		}
		if(!Utils.compareDate(fDate, tDate)) {
			msg ='Từ ngày phải nh�? hơn đến ngày.';
			$('#fromDate').focus();
		}
		if(msg.length > 0){
			$('#errMsg').html(msg).show();
			return false;
		}
		
		var shopKendo = $("#shop").data("kendoMultiSelect");
		var lstShopId = shopKendo.value();
		if(lstShopId.length == 0) {
			$('#errMsg').html('Bạn chưa ch�?n NPP. Vui lòng ch�?n NPP').show();
			return false;
		}
		
		var dataModel = new Object();
		//dataModel.lstShopId = lstShopId;
		dataModel.strListShopId = lstShopId.toString();
		dataModel.fromDate = $('#fromDate').val();
		dataModel.toDate = $('#toDate').val();
		//ReportUtils.exportReport('/report/superivse/vt11-bcnvmclip/export',dataModel,'errMsg');
		ReportUtils.exportReport('/report/superivse/vt11-bcnvmclip/export',dataModel);
	},
	
	/**
	 * Bc 1.3 ket qua di tuyen
	 * 
	 * @author hoanv25
	 * @since July 07, 2015
	 * */
	resultTimeVisitCustomer: function(){
		var msg = '';
		$('#errMsg').html('').hide();
		if ($('#shopId').val().trim().length == 0 || $('#shopId').val().trim() == activeType.ALL) {
			msg = format(msgErr_required_field,'đơn vị');
		}
		if (msg.length == 0) {
			msg = Utils.getMessageOfRequireCheck('fDate','Từ ngày');
		}
		if (msg.length == 0) {
			msg = Utils.getMessageOfRequireCheck('tDate','�?ến ngày');
		}
		if (msg.length > 0) {
			$('#errMsg').html(msg).show();
			return false;
		}
		if (!checkDateForCycle()) {
			return;
		}
		var dataModel = new Object();
		dataModel.strListShopId=$('#shopId').val();
		dataModel.fromDate = $('#fDate').val().trim();
		dataModel.toDate = $('#tDate').val().trim();
		dataModel.cycleId = $('#cycle').val();
		ReportUtils.exportReport('/report/superivse/vt6/export',dataModel);
	},
	
	/**
	 * Load ds khach hang 
	 * 
	 * @author hoanv25
	 * @since July 08, 2015
	 * */
	loadComboNVBH:function(url){
		$("#nvbh").kendoMultiSelect({
			dataSource: {
				transport: {
	                read: {
	                    dataType: "json",
	                    url: url
	                }
	            }
			},
			filter: "contains",
	        dataTextField: "nvbhName",
	        dataValueField: "nvbhId",
			itemTemplate: function(data, e, s, h, q) {
				return '<div class="tree-vnm" node-id="'+Utils.XSSEncode(data.nvbhId)+'" style="width:180px"><span class="tree-vnm-text">'+Utils.XSSEncode(data.nvbhCode)+' - '+Utils.XSSEncode(data.nvbhName) +'</span></div>';	
			},
	        tagTemplate:  '#: data.nvbhName #',
	        value: [$('#nvbhId').val()]
	    });	 
	},
	
	/**
	 * Bc GS 1.4 mo moi khach hang 
	 * 
	 * @author hoanv25
	 * @since July 08, 2015
	 * */
	exportBCMMKH: function(){
		var msg = '';		
	/*	var gsKendo = $("#gs").data("kendoMultiSelect");
		var lstGsId = gsKendo.value();*/
		var nvbhKendo = $("#nvbh").data("kendoMultiSelect");
		var lstNvbhId = nvbhKendo.value();		
		var multiShop = $("#shop").data("kendoMultiSelect");
		var dataShop = multiShop.dataItems();
		var dataNVBH = null;
		if (nvbhKendo != null) {
			dataNVBH = nvbhKendo.dataItems();
		}
		if(dataShop!=null && dataShop.length>0){
			var lstShop = new Array();
			for(var i=0;i<dataShop.length;i++){
				lstShop.push(dataShop[i].id);
			}
		}
		var lstNVBH = [];
		for(var i=0;i<dataNVBH.length;i++){
			lstNVBH.push(dataNVBH[i].nvbhName);
		}
		$('#errMsg').html('').hide();
		/*if ($('#shopId').val().trim().length == 0 || $('#shopId').val().trim() == activeType.ALL) {
			msg = format(msgErr_required_field,'đơn vị');
		}*/
		if (msg.length == 0) {
			msg = Utils.getMessageOfRequireCheck('fDate','Từ ngày');
		}
		if (msg.length == 0) {
			msg = Utils.getMessageOfRequireCheck('tDate','Đến ngày');
		}
		if (msg.length > 0) {
			$('#errMsg').html(msg).show();
			return false;
		}
		if (!checkDateForCycle()) {
			return;
		}
		var dataModel = new Object();
		dataModel.strListShopId=lstShop.toString().trim();
		dataModel.fromDate = $('#fDate').val().trim();
		dataModel.toDate = $('#tDate').val().trim();
		dataModel.cycleId = $('#cycle').val();
		//dataModel.staffType = $('#staffType').val();
		/*if (lstGsId != undefined && lstGsId != null && lstGsId != "") {
			dataModel.strListGsId = lstGsId.toString().trim();
		}*/
		if (lstNvbhId != undefined && lstNvbhId != null && lstNvbhId != "") {
			dataModel.strListNvbhId = lstNvbhId.toString().trim();
		}
		if (lstNVBH != undefined && lstNVBH != null && lstNVBH != "") {
			dataModel.strListNVBH = lstNVBH.toString().trim();
		}
		ReportUtils.exportReport('/report/superivse/vt11-bcnvmclip/export',dataModel);
	},
	
	//VT12 Bao cao xoa khach hang tham gia cttb
	exportBCVT12: function(){
		var msg = '';
		$('#errMsg').html('').hide(); 
		var shopKendo = $("#shop").data("kendoMultiSelect");
		var lstShopId = shopKendo.value();
		if(lstShopId.length == 0) {
			$('#errMsg').html(msg = format(msgErr_required_field,'�?ơn vị')).show();
			return false;
		}
		
		var dataModel = new Object();		
		var displayProgramId = $('#displayProgram').val();		
		if(displayProgramId != null && displayProgramId.length > 0) {			
			//dataModel.idCTTB = displayProgramId[0];
			var lstDisplayProgram = new Array();
			for(var i = 0; i<displayProgramId.length; i++){
				if(displayProgramId[i] != null && displayProgramId[i] != ""){
					lstDisplayProgram.push(displayProgramId[i]);
				}
			}
			dataModel.displayProgramId = lstDisplayProgram.toString();
		} else {
			$('#errMsg').html('Bạn chưa ch�?n chương trình. Vui lòng ch�?n chương trình').show();
			return false;
		}
		
		var monthYear = $('#month').val().trim();
		if(monthYear != null && monthYear.length > 0) {		
			dataModel.monthYear = monthYear;
		}else {
			$('#errMsg').html('Bạn chưa ch�?n tháng. Vui lòng ch�?n tháng').show();
			return false;
		}
		
		dataModel.strListShopId = lstShopId.toString();
		ReportUtils.exportReport('/report/superivse/vt12-bcxkhtgcttb/export',dataModel);
	},
	

	/**
	 * 6.1.2 Thời gian làm việc của NVBH
	 * @author hunglm16
	 * @since 19/10/2015
	 */
	report6D1D2TGLVCNVBH : function() {
		var msg = '';
		$('#errMsg').html('').hide();
		if ($('#shopId').val().trim().length == 0 || $('#shopId').val().trim() == activeType.ALL) {
			msg = format(msgErr_required_field, 'đơn vị');
		}
		var msg = '';
		if (msg.length == 0) {
			msg = Utils.getMessageOfRequireCheck('fDate', 'Từ ngày');
		}
		if (msg.length == 0) {
			msg = Utils.getMessageOfRequireCheck('tDate', 'Đến ngày');
		}
		if (msg.length == 0) {
			msg = Utils.getMessageOfInvalidFormatDate('fDate','Từ ngày');
		}
		if (msg.length == 0) {
			msg = Utils.getMessageOfInvalidFormatDate('tDate','Đến ngày');
		}
		var fDate = $('#fDate').val();
		var tDate = $('#tDate').val();
		if (!Utils.compareDate(fDate, tDate)) {
			msg = js_fromdate_todate_validate;		
			$('#fDate').focus();
		}
		if (msg.length == 0) {
			msg = Utils.compareCurrentDateEx('tDate','Đến ngày');
		}
		if (msg.length > 0) {
			$('#errMsg').html(msg).show();
			return false;
		}
		var dataModel = new Object();
		dataModel.strListShopId = $('#shopId').val();
		dataModel.fromDate = $('#fDate').val().trim();
		dataModel.toDate = $('#tDate').val().trim();
		//dataModel.cycleId = $('#cycle').val();
//		dataModel.staffType = $('#staffType').val();
		ReportUtils.exportReport('/report/superivse/tggt-nvbh/export', dataModel);
	}
};
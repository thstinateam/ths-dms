var CRMReportDSPPTNH = {
		_isCheckAll: false,
		_lstChecker: null,
		_lstUnchecker: null,
		_arrMultiChoiceDisplay: null,
		tDateChangeByfDateIntoFirtMonth : function (){
			
		},
		/* BAO CAO DOANH SO PHAN PHOI THEO NHOM HANG */
		selectProduct:function(obj,code){
			if(code=='null') return;
			if(obj.checked){
				CRMReportDSPPTNH._listProductCode.push(code);
			}else{
				removeE(CRMReportDSPPTNH._listProductCode,code);
			}
		},
		
		exportDSPPTNH: function(){
			var msg = '';
			$('#errMsg').html('').hide();
			if(msg.length == 0){
				msg = Utils.getMessageOfRequireCheck('tDate','Đến ngày');
			}
			if(msg.length == 0){
				msg = Utils.getMessageOfInvalidFormatDate('tDate','Đến ngày');
			}
			var currentTime = new Date();
			var month = currentTime.getMonth() + 1;
			var day = currentTime.getDate();
			var year = currentTime.getFullYear();
			if(!Utils.compareDate($('#tDate').val().trim(),day + '/' + month + '/' + year)){
				msg = 'Đến ngày không được lớn hơn ngày hiện tại';
				$('#tDate').focus();
			}
			if(msg.length > 0){
				$('#errMsg').html(msg).show();
				return false;
			}
			var dataModel = new Object();		
			dataModel.toDate = $('#tDate').val().trim();
			dataModel.shopId = $('#shopId').val().trim();
			dataModel.productCode = $('#productCode').val().trim();
			ReportUtils.exportReport('/report/crm/bcds-pptnh/export',dataModel);
		},
		/* BAO CAO KD 19.1s*/
		exportKD19_1 : function(){
			var msg = '';
			$('#errMsg').html('').hide();
			if(msg.length == 0){
				msg = Utils.getMessageOfRequireCheck('fDate','Từ ngày');
			}
			if(msg.length == 0){
				msg = Utils.getMessageOfInvalidFormatDate('fDate','Từ ngày');
			}
			if(msg.length == 0){
				msg = Utils.getMessageOfRequireCheck('tDate','Đến ngày');
			}
			if(msg.length == 0){
				msg = Utils.getMessageOfInvalidFormatDate('tDate','Đến ngày');
			}
			if(msg.length == 0){
				msg = Utils.getMessageOfRequireCheck('number','Số lần');
			}
			var fDate = $('#fDate').val();
			var tDate = $('#tDate').val();
			var curDate = ReportUtils.getCurrentDateString();
			if(msg.length == 0 && !Utils.compareDate(fDate, tDate)){
				msg = msgErr_fromdate_greater_todate;
				$('#fDate').focus();
			}
			if(msg.length == 0 && !Utils.compareDate(tDate, curDate)){
				msg = "Đến ngày không được lớn hơn ngày hiện tại";
				$('#tDate').focus();
			}
			if(msg.length == 0 && fDate.split('/')[1] != tDate.split('/')[1]){
				msg = "Đến ngày phải cùng tháng với Từ ngày.";
				$('#tDate').focus();
			}
			if(msg.length == 0){
				msg = Utils.getMessageOfSpecialCharactersValidateEx('number','Số lần',Utils._TF_NUMBER);
			}
			if(msg.length > 0){
				$('#errMsg').html(msg).show();
				return false;
			}
			var dataModel = new Object();
			dataModel.fromDate = $('#fDate').val().trim();
			dataModel.toDate = $('#tDate').val().trim();
			dataModel.shopId = $('#shopId').val().trim();
			dataModel.solan = $('#number').val().trim();
			ReportUtils.exportReport('/report/crm/kd19-1/export',dataModel);
		},
		/**
		 * Xuất báo cáo KD1.2 Doanh số phân phối theo nhãn hàng
		 * @author tungtt21
		 * @returns {Boolean}
		 */
		exportKD1_2: function(){
			var msg = '';
			var multiShop = $("#shop").data("kendoMultiSelect");
			var dataShop = multiShop.dataItems();
			$('#errMsg').html('').hide();
			
			if(msg.length == 0){
				if(dataShop == null || dataShop.length == 0){
					msg = "Bạn chưa chọn đơn vị. Vui lòng chọn đơn vị.";
				}
			}
			if(msg.length == 0){
				msg = Utils.getMessageOfRequireCheck('tDate','Đến ngày');
			}
			if(msg.length == 0){
				msg = Utils.getMessageOfInvalidFormatDate('tDate','Đến ngày');
			}		
			if(msg.length > 0){
				$('#errMsg').html(msg).show();
				return false;
			}
			
			var dataModel = new Object();			
			dataModel.toDate = $('#tDate').val().trim();
			dataModel.productCode = $('#product').val().trim();
			if(dataShop!=null && dataShop.length>0){
				var lstShop = dataShop[0].id;
				for(var i=1;i<dataShop.length;i++){
					lstShop += "," + dataShop[i].id;
				}
				dataModel.listShopId = lstShop;
			}
			
			var kData = $.param(dataModel,true);
			$('#divOverlay').show();		
			$.ajax({
				type : "POST",
				url : "/report/crm/kd1_2/export",
				data :(kData),
				dataType: "json",
				success : function(data) {
					$('#divOverlay').hide();				
					if(!data.error) {
						if(data.hasData != null && data.hasData != undefined && data.hasData == true) {
							window.location.href = data.path;
							setTimeout(function(){ //Set timeout để đảm bảo file load lên hoàn tất
			                    CommonSearch.deleteFileExcelExport(data.path);
							},2000);
						} else {
							$('#errMsg').html('Không có dữ liệu để xuất').show();
						}
					} else {
						$('#errMsg').html(data.errMsg).show();					
					}
				},
				error:function(XMLHttpRequest, textStatus, errorDivThrown) {
					$('#divOverlay').hide();
					StockIssued.xhrExport = null;
				}
			});
			return false;
		},
		/**
		 * Xuất báo cáo KD1.3 Doanh số phân phối theo nhóm hàng
		 * @author tungtt21
		 * @returns {Boolean}
		 */
		exportKD1_3: function(){
			var msg = '';
			var multiShop = $("#shop").data("kendoMultiSelect");
			var dataShop = multiShop.dataItems();
			$('#errMsg').html('').hide();
			
			if(msg.length == 0){
				if(dataShop == null || dataShop.length == 0){
					msg = "Bạn chưa chọn đơn vị. Vui lòng chọn đơn vị.";
				}
			}
			if(msg.length == 0){
				msg = Utils.getMessageOfRequireCheck('tDate','Đến ngày');
			}
			if(msg.length == 0){
				msg = Utils.getMessageOfInvalidFormatDate('tDate','Đến ngày');
			}		
			if(msg.length > 0){
				$('#errMsg').html(msg).show();
				return false;
			}
			
			var dataModel = new Object();			
			dataModel.toDate = $('#tDate').val().trim();
			dataModel.productCode = $('#product').val().trim();
			if(dataShop!=null && dataShop.length>0){
				var lstShop = dataShop[0].id;
				for(var i=1;i<dataShop.length;i++){
					lstShop += "," + dataShop[i].id;
				}
				dataModel.listShopId = lstShop;
			}
			
			var kData = $.param(dataModel,true);
			$('#divOverlay').show();		
			$.ajax({
				type : "POST",
				url : "/report/crm/kd1_3/export",
				data :(kData),
				dataType: "json",
				success : function(data) {
					$('#divOverlay').hide();				
					if(!data.error) {
						if(data.hasData != null && data.hasData != undefined && data.hasData == true) {
							window.location.href = data.path;
							setTimeout(function(){ //Set timeout để đảm bảo file load lên hoàn tất
			                    CommonSearch.deleteFileExcelExport(data.path);
							},2000);
						} else {
							$('#errMsg').html('Không có dữ liệu để xuất').show();
						}
					} else {
						$('#errMsg').html(data.errMsg).show();					
					}
				},
				error:function(XMLHttpRequest, textStatus, errorDivThrown) {
					$('#divOverlay').hide();
					StockIssued.xhrExport = null;
				}
			});
			return false;
		},
		exportKD13: function(){
			var fTmp = '';
			var tTmp = '';
			var msg = '';
			$('#errMsg').html('').hide();
			if($('#shopId').val().trim().length == 0 || $('#shopId').val().trim() == activeType.ALL){
				msg = format(msgErr_required_field,'Đơn vị');
			}
			fTitle = 'Từ tháng';
			tTitle = 'Đến tháng';
			if(msg.length == 0){
				msg = Utils.getMessageOfRequireCheck('fDate',fTitle);
			}
			if(msg.length == 0){
				msg = Utils.getMessageOfRequireCheck('tDate',tTitle);
			}
			fTmp = $('#fDate').val().trim();
			tTmp = $('#tDate').val().trim();
			$('#fDate').val("01/"+fTmp);
			$('#tDate').val("01/"+tTmp);
			var fDate = $('#fDate').val();
			var tDate = $('#tDate').val();
			if(!Utils.compareDate(fDate, tDate)){
				msg = fTitle +" không được lớn hơn "+ tTitle;		
				$('#fDate').focus();
			}
			$('#fDate').val(fTmp);
			$('#tDate').val(tTmp);
			
			var currentTime = new Date();
			var month = currentTime.getMonth() + 1;
			var day = currentTime.getDate();
			var year = currentTime.getFullYear();
			if(!Utils.compareDate(tDate,day + '/' + month + '/' + year)){
				msg = 'Đến tháng không được lớn hơn tháng/năm hiện tại';
				$('#tDate').focus();
			}
			
			if(msg.length > 0){
				$('#errMsg').html(msg).show();
				return false;
			}
			var dataModel = new Object();		
			dataModel.shopId = $('#shopId').val().trim();
			dataModel.fromDate =  "01/"+$('#fDate').val().trim();
			dataModel.toDate = "01/"+$('#tDate').val().trim();
			ReportUtils.exportReport('/report/crm/kd13/export',dataModel);
		},
		/* KD10.3 Tổng kết thực hiện DS,PP theo mức tham gia */
		exportTKTHDSPPTMTG: function(){
			var msg = '';
			$('#errMsg').html('').hide();
			if(msg.length == 0){
				msg = Utils.getMessageOfRequireCheck('displayProgramCode','Mã CTTB');
			}
			if(msg.length == 0){
				msg = Utils.getMessageOfRequireCheck('productCode','Nhãn hàng');
			}
			if(msg.length == 0){
				msg = Utils.getMessageOfRequireCheck('tDate','Đến ngày');
			}
			if(msg.length == 0){
				msg = Utils.getMessageOfInvalidFormatDate('tDate','Đến ngày');
			}
			if(msg.length > 0){
				$('#errMsg').html(msg).show();
				return false;
			}
			var dataModel = new Object();		
			dataModel.toDate = $('#tDate').val().trim();
			dataModel.shopId = $('#shopId').val().trim();
			dataModel.displayProgramCode = $('#displayProgramCode').val().trim();
			dataModel.productCode = $('#productCode').val().trim();
			//ReportUtils.exportReport('/report/crm/kd10-3/export',dataModel);
			var kData = $.param(dataModel,true);
			$('#divOverlay').show();		
			$.ajax({
				type : "POST",
				url : "/report/crm/kd10-3/export",
				data :(kData),
				dataType: "json",
				success : function(data) {
					$('#divOverlay').hide();				
					if(!data.error) {
						if(data.hasData != null && data.hasData != undefined && data.hasData == true) {
							window.location.href = data.path;
							setTimeout(function(){ //Set timeout để đảm bảo file load lên hoàn tất
			                    CommonSearch.deleteFileExcelExport(data.path);
							},2000);
						} else {
							$('#errMsg').html('Không có dữ liệu để xuất').show();
						}
					} else {
						$('#errMsg').html(data.errMsg).show();					
					}
				},
				error:function(XMLHttpRequest, textStatus, errorDivThrown) {
					$('#divOverlay').hide();
					StockIssued.xhrExport = null;
				}
			});
			return false;
		},
	exportDSBHNVBHSKU: function(){
		var msg = '';
		$('#errMsg').html('').hide();
		if(msg.length == 0){
			msg = Utils.getMessageOfRequireCheck('fDate','Từ ngày');
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfInvalidFormatDate('fDate','Từ ngày');
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfRequireCheck('tDate','Đến ngày');
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfInvalidFormatDate('tDate','Đến ngày');
		}
		var fDate = $('#fDate').val();
		var tDate = $('#tDate').val();
		if(!Utils.compareDate(fDate, tDate)){
			msg = msgErr_fromdate_greater_todate;		
			$('#fDate').focus();
		}
		if(msg.length > 0){
			$('#errMsg').html(msg).show();
			return false;
		}
		var dataModel = new Object();		
		dataModel.fromDate = $('#fDate').val().trim();
		dataModel.toDate = $('#tDate').val().trim();
		ReportUtils.exportReport('/report/crm/kd1/export',dataModel);
	},
	exportDSBHNVBH: function(){
		var msg = '';
		$('#errMsg').html('').hide();
		if(msg.length == 0){
			msg = Utils.getMessageOfInvalidFormatDate('tDate','Đến ngày');
		}
		if(msg.length > 0){
			$('#errMsg').html(msg).show();
			return false;
		}
		var shopId = $('#shopId').val();
		var parentSuperStaffCode = $('#parentSuperStaffCode').val().replace(/;/g, ',');
		var staffSaleCode = $('#staffSaleCode').val().replace(/;/g, ',');
		var superStaffCode = $('#superStaffCode').val().replace(/;/g, ',');
		var toDate = $('#tDate').val();
		var now = new Date();
		var cMonth = now.getMonth() + 1;
		var tMonth = toDate.split('/')[1];
		tMonth = Number(tMonth);
		if(tMonth != cMonth) {
			$('#errMsg').html('Đến ngày phải cùng tháng với tháng hiện tại. Vui lòng nhập lại').show();
			return false;
		}
		if(!Utils.compareCurrentDate(toDate)) {
			$('#errMsg').html('Đến ngày phải nhỏ hơn ngày hiện tại. Vui lòng nhập lại').show();
			return false;
		}
		var cat = $('#cat').val();
		var dataModel = new Object();
		dataModel.shopId = shopId;
		dataModel.parentSuperStaffCode = parentSuperStaffCode;
		dataModel.staffSaleCode = staffSaleCode;
		dataModel.superStaffCode = superStaffCode;
		dataModel.toDate = toDate;
		dataModel.cat = cat;
		ReportUtils.exportReport('/report/crm/kd3/export',dataModel);
	},

	/* BAO CAO BAO PHỦ PHƯỜNG XÃ THEO CẤP ĐỘ DOANH SỐ */
	 exportBPPXTCDDS: function(){
		var msg = '';
		$('#errMsg').html('').hide();
		if(msg.length == 0){
			msg = Utils.getMessageOfRequireCheck('fDate','Từ tháng');
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfInvalidFormatDate('fDate','Từ tháng');
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfRequireCheck('tDate','Từ tháng');
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfInvalidFormatDate('tDate','Đến tháng');
		}
		var fDate = $('#fDate').val();
		var tDate = $('#tDate').val();
		if(!Utils.compareDate(fDate, tDate)){
			msg = msgErr_fromdate_greater_todate;		
			$('#fDate').focus();
		}
		if(msg.length > 0){
			$('#errMsg').html(msg).show();
			return false;
		}
		var dataModel = new Object();		
		dataModel.fromDate = $('#fDate').val().trim();
		dataModel.toDate = $('#tDate').val().trim();
		ReportUtils.exportReport('/report/crm/bcbppx-tcdds/export',dataModel);
	},
	
	exportKD10: function(){		
		var msg = "";
		$('#errMsg').html(msg).hide();
		if(msg.length == 0){
			msg = Utils.getMessageOfRequireCheck('fDate','Từ ngày');
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfInvalidFormatDate('fDate','Từ ngày');
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfRequireCheck('tDate','Từ ngày');
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfRequireCheck('subCat','Ngành hàng',true);
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfInvalidFormatDate('tDate','Đến ngày');
		}
		var fDate = $('#fDate').val();
		var tDate = $('#tDate').val();
		var curDate = ReportUtils.getCurrentDateString();
		if(msg.length == 0 && !Utils.compareDate(fDate, curDate)){
			msg = 'Từ ngày phải nhỏ hơn hoặc bằng ngày hiện tại';
			$('#fDate').focus();
		}	
		if(msg.length == 0 && !Utils.compareDate(tDate, curDate)){
			msg = 'Đến ngày phải nhỏ hơn hoặc bằng ngày hiện tại';
			$('#fDate').focus();
		}
		if(msg.length > 0){
			$('#errMsg').html(msg).show();
			return false;
		}
		var productInfoCode = $('#subCat').val().trim();
		var dataModel = new Object();
		dataModel.fromDate = $('#fDate').val();
		dataModel.toDate = $('#tDate').val();
		dataModel.braneId = $('#subCat').val();
		ReportUtils.exportReport('/report/crm/kd10/export',dataModel);
	},
	/**
	 * Bao cao KD9
	 * @author tungtt21
	 * @returns {Boolean}
	 */
	exportKD9: function(){
		var msg = '';
		$('#errMsg').html('').hide();
		if(msg.length == 0){
			msg = Utils.getMessageOfRequireCheck('shopCode','Đơn vị');
		}
		if(msg.length == 0) {
			msg = Utils.getMessageOfRequireCheck('fDate', 'Từ ngày');
		}
		if(msg.length == 0) {
			msg = Utils.getMessageOfRequireCheck('tDate', 'Đến ngày');
		}
		if(msg.length == 0) {
			msg = Utils.getMessageOfInvalidFormatDate('fDate', 'Từ ngày');
		}
		if(msg.length == 0) {
			msg = Utils.getMessageOfInvalidFormatDate('tDate', 'Đến ngày');
		}
		var fDate = $('#fDate').val();
		var tDate = $('#tDate').val();
		var curDate = ReportUtils.getCurrentDateString();
		if(msg.length == 0 && !Utils.compareDate(fDate, curDate)){
			msg = 'Từ ngày phải nhỏ hơn hoặc bằng ngày hiện tại';
			$('#fDate').focus();
		}	
		if(msg.length == 0 && !Utils.compareDate(tDate, curDate)){
			msg = 'Đến ngày phải nhỏ hơn hoặc bằng ngày hiện tại';
			$('#tDate').focus();
		}
		
		if(msg.length == 0 && !Utils.compareDate1(fDate, tDate)){
			msg = 'Từ ngày phải nhỏ hơn đến ngày';
			$('#fDate').focus();
		}
		
		var month1 = fDate.split('/')[1];
		month1 = Number(month1);
		var month2 = tDate.split('/')[1];
		month2 = Number(month2);
		var year1 = fDate.split('/')[2];
		var year2 = tDate.split('/')[2];
		year1 = parseInt(year1);
		year2 = parseInt(year2);
		if(month1 != month2 || year1 != year2) {
			msg = 'Từ ngày phải cùng tháng cùng năm với đến ngày.';
			$('#tDate').focus();
		}
		
		if(msg.length > 0){
			$('#errMsg').html(msg).show();
			return false;
		}
		
		var dataModel = new Object();
		dataModel.fromDate = $('#fDate').val().trim();
		dataModel.toDate = $('#tDate').val().trim();
		dataModel.shopId = $('#shop').combotree('getValue').trim();
		dataModel.productCode = $('#product').val().trim();
		
		var kData = $.param(dataModel,true);
		$('#divOverlay').show();		
		$.ajax({
			type : "POST",
			url : "/report/crm/sku30/export",
			data :(kData),
			dataType: "json",
			success : function(data) {
				$('#divOverlay').hide();				
				if(!data.error) {
					if(data.hasData != null && data.hasData != undefined && data.hasData == true) {
						window.location.href = data.path;
						setTimeout(function(){ //Set timeout để đảm bảo file load lên hoàn tất
		                    CommonSearch.deleteFileExcelExport(data.path);
						},2000);
					} else {
						$('#errMsg').html('Không có dữ liệu để xuất').show();
					}
				} else {
					$('#errMsg').html(data.errMsg).show();					
				}
			},
			error:function(XMLHttpRequest, textStatus, errorDivThrown) {
				$('#divOverlay').hide();
				StockIssued.xhrExport = null;
			}
		});
		return false;
	},
	exportKD21 : function(){
		var msg = '';
		$('#errMsg').html('').hide();
		if($('#shopCode').val().trim().length == 0 || $('#shopCode').val().trim() == activeType.ALL){
			msg = format(msgErr_required_field,'đơn vị');
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfRequireCheck('fDate', 'Từ ngày');
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfInvalidFormatDate('fDate','Từ ngày');
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfRequireCheck('tDate','Đến ngày');
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfInvalidFormatDate('tDate','Đến ngày');
		}
		var fDate = $('#fDate').val();
		var tDate = $('#tDate').val();
		if(!Utils.compareDate(fDate, tDate)){
			msg = msgErr_fromdate_greater_todate;		
			$('#fDate').focus();
		}
		if(msg.length > 0){
			$('#errMsg').html(msg).show();
			return false;
		}
		var dataModel = new Object();
		dataModel.shopId = $('#shopCode').val().trim();
		dataModel.fromDate = $('#fDate').val().trim();
		dataModel.toDate = $('#tDate').val().trim();
		ReportUtils.exportReport('/report/crm/kd21/export',dataModel);
	},
	validateKD2: function(){
		var msg = '';
		$('#errMsg').html('').hide();
		if(msg.length == 0){
			msg = Utils.getMessageOfRequireCheck('tDate','Đến ngày');
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfInvalidFormatDate('tDate','Đến ngày');
		}
		if(msg.length > 0){
			$('#errMsg').html(msg).show();
			return false;
		}
		return true;
	},
	exportKD2 : function(){
		if(!CRMReportDSPPTNH.validateKD2()) return ;
		var dataModel = new Object();
		dataModel.toDate = $('#tDate').val().trim();
		if($('#tbhv').val().trim()!=''){
			dataModel.lstTbhv = $('#tbhv').val().trim().split(";");
		}
		if($('#gsnpp').val().trim()!=''){
			dataModel.lstGsnpp = $('#gsnpp').val().trim().split(";");
		}
		if($('#nvbh').val().trim()!=''){
			dataModel.lstNvbh = $('#nvbh').val().trim().split(";");
		}
		if($('#subcat').val().trim()!=''){
			dataModel.lstProduct = $('#subcat').val().trim().split(";");
		}
		dataModel.shopS = $('#shopId').val().trim();
		ReportUtils.exportReport('/report/crm/kd2/export',dataModel);
	},
	validateKD81: function(){
		var msg = '';
		$('#errMsg').html('').hide();
		if(msg.length == 0){
			msg = Utils.getMessageOfRequireCheck('fDate','Từ ngày');
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfInvalidFormatDate('fDate','Từ ngày');
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfRequireCheck('tDate','Đến ngày');
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfInvalidFormatDate('tDate','Đến ngày');
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfRequireCheck('solan','Số lần');
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfRequireCheck('shopId','Đơn vị');
		}
		var fDate = $('#fDate').val();
		var tDate = $('#tDate').val();
		var curDate = ReportUtils.getCurrentDateString();
		if(msg.length == 0 && !Utils.compareDate(fDate, tDate)){
			msg = msgErr_fromdate_greater_todate;
			$('#fDate').focus();
		}
		if(msg.length == 0 && !Utils.compareDate(tDate, curDate)){
			msg = "Đến ngày không được lớn hơn ngày hiện tại";
			$('#tDate').focus();
		}
		if(msg.length == 0 && fDate.split('/')[1] != tDate.split('/')[1]){
			msg = "Đến ngày phải cùng tháng với Từ ngày.";
			$('#tDate').focus();
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfSpecialCharactersValidateEx('solan','Số lần',Utils._TF_NUMBER);
		}
		if(msg.length > 0){
			$('#errMsg').html(msg).show();
			return false;
		}
		return true;
	},
	exportKD81 : function(){
		if(!CRMReportDSPPTNH.validateKD81()) return;
		var dataModel = new Object();
		dataModel.fromDate = $('#fDate').val().trim();
		dataModel.toDate = $('#tDate').val().trim();
		dataModel.shopS = $('#shopId').val().trim();
		dataModel.solan = $('#solan').val().trim();
		if($('#nvbh').val().trim()!=''){
			dataModel.lstNvbh = $('#nvbh').val().trim().split(";");
		}
		ReportUtils.exportReport('/report/crm/kd81/export',dataModel);
	},
	exportKD_8_1 : function(){
		if(!CRMReportDSPPTNH.validateKD81()) return;
		var dataModel = new Object();
		dataModel.fromDate = $('#fDate').val().trim();
		dataModel.toDate = $('#tDate').val().trim();
		dataModel.shopS = $('#shopId').val().trim();
		dataModel.solan = $('#solan').val().trim();
		var lstNvbh = new Array();
		if($('#nvbh').val().trim()!=''){
			lstNvbh = $('#nvbh').val().trim().split(",");
			dataModel.lstNvbh = lstNvbh;
		}
		ReportUtils.exportReport('/report/crm/kd81/export',dataModel);
	},
	validateKD16: function(){
		var msg = '';
		$('#errMsg').html('').hide();
		if(msg.length == 0){
			msg = Utils.getMessageOfRequireCheck('tDate','Đến ngày');
		}
		if(msg.length == 0){
			if($('#tDate').val().trim().length > 0 && !Utils.isDate('1/'+$('#tDate').val().trim(), '/')){
				$('#tDate').focus();			
				msg = 'Đến ngày không đúng định dạng mm/dd';
			}
		}
		var tDate = $('#tDate').val();
		var curDate = ReportUtils.getCurrentDateString();
		if(msg.length == 0 && !Utils.compareDate(tDate, curDate)){
			msg = "Đến ngày không được lớn hơn ngày hiện tại";
			$('#tDate').focus();
		}
		if(msg.length > 0){
			$('#errMsg').html(msg).show();
			return false;
		}
		return true;
	},
	exportKD16 : function(){
		if(!CRMReportDSPPTNH.validateKD16()) return;
		var dataModel = new Object();
		dataModel.toDate = $('#tDate').val().trim();
		ReportUtils.exportReport('/report/crm/kd16/export',dataModel);
	},
	exportKD8 : function(){
		var msg = '';
		$('#errMsg').html('').hide();
		if($('#shopCode').val().trim().length == 0 || $('#shopCode').val().trim() == activeType.ALL){
			msg = format(msgErr_required_field,'đơn vị');
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfRequireCheck('fDate', 'Từ ngày');
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfInvalidFormatDate('fDate','Từ ngày');
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfRequireCheck('tDate','Đến ngày');
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfInvalidFormatDate('tDate','Đến ngày');
		}
		var fDate = $('#fDate').val();
		var tDate = $('#tDate').val();
		if(!Utils.compareDate(fDate, tDate)){
			msg = msgErr_fromdate_greater_todate;		
			$('#fDate').focus();
		}
		if(msg.length > 0){
			$('#errMsg').html(msg).show();
			return false;
		}
		var dataModel = new Object();		
		dataModel.shopId = $('#shopCode').val().trim();
		dataModel.fromDate = $('#fDate').val().trim();
		dataModel.toDate = $('#tDate').val().trim();
		//dataModel.staffCode = $('#staffCode').val().trim();
		ReportUtils.exportReport('/report/crm/kd8/export',dataModel);
	},
	exportKD1_1: function(){
		var msg = '';
		var multiShop = $("#shop").data("kendoMultiSelect");
		var dataShop = multiShop.dataItems();
		$('#errMsg').html('').hide();
		
		if(msg.length == 0){
			if(dataShop == null || dataShop.length == 0){
				msg = "Bạn chưa chọn đơn vị. Vui lòng chọn đơn vị.";
			}
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfRequireCheck('tDate','Đến ngày');
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfInvalidFormatDate('tDate','Đến ngày');
		}		
		if(msg.length > 0){
			$('#errMsg').html(msg).show();
			return false;
		}
		
		var dataModel = new Object();			
		dataModel.toDate = $('#tDate').val().trim();
		dataModel.productCode = $('#product').val().trim();
		
		if(dataShop!=null && dataShop.length>0){
			var lstShop = dataShop[0].id;
			for(var i=1;i<dataShop.length;i++){
				lstShop += "," + dataShop[i].id;
			}
			dataModel.listShopId = lstShop;
		}
		
		var kData = $.param(dataModel,true);
		$('#divOverlay').show();		
		$.ajax({
			type : "POST",
			url : "/report/crm/kd1_1/export",
			data :(kData),
			dataType: "json",
			success : function(data) {
				$('#divOverlay').hide();				
				if(!data.error) {
					if(data.hasData != null && data.hasData != undefined && data.hasData == true) {
						window.location.href = data.path;
						setTimeout(function(){ //Set timeout để đảm bảo file load lên hoàn tất
		                    CommonSearch.deleteFileExcelExport(data.path);
						},2000);
					} else {
						$('#errMsg').html('Không có dữ liệu để xuất').show();
					}
				} else {
					$('#errMsg').html(data.errMsg).show();					
				}
			},
			error:function(XMLHttpRequest, textStatus, errorDivThrown) {
				$('#divOverlay').hide();
				StockIssued.xhrExport = null;
			}
		});
		return false;
	},
	reportSDLCSPDS : function(){
		var msg = '';
		$('#errMsg').html('').hide();
		var dataModel = new Object();
		dataModel.fromMonth = $('#fromMonth').val().trim();
		dataModel.toMonth = $('#toMonth').val().trim();
		ReportUtils.previewReportProcessNoData('/report/crm/sdlcpsds/export',dataModel);
	},
	exportKD4 : function(){
		$('#errMsg').html('').hide();
		var msg = "";
		if(msg.length == 0){
			msg = Utils.getMessageOfRequireCheck('toDate','Đến ngày');
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfInvalidFormatDate('toDate','Đến ngày');
		}
		var toDate =  $('#toDate').val().trim();
		if(msg.length == 0 && !Utils.compareCurrentDate(toDate)) {
			msg = 'Đến ngày phải nhỏ hơn ngày hiện tại. Vui lòng nhập lại';
			$('#toDate').focus();
		}
//		var now = new Date();
//		var cMonth = now.getMonth() + 1;
//		var cYear = now.getFullYear();
//		var aoDate = $('#toDate').val().split('/');
//		var month = aoDate[1];
//		var year = aoDate[2];
//		if(month != cMonth || year != cYear ){
//			msg = 'Đến ngày không thuộc tháng hiện tại';
//			$('#toDate').focus();
//		}
		if(msg.length > 0){
			$('#errMsg').html(msg).show();
			return false;
		}
		var dataModel = new Object();
		dataModel.toDate = toDate;
		ReportUtils.exportReport('/report/crm/kd4/export',dataModel);
	},
	exportKD14 : function(){
		$('#errMsg').html('').hide();
		var msg = '';
		if(msg.length == 0){
			msg = Utils.getMessageOfRequireCheck('fromDate','Từ tháng');
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfRequireCheck('toDate','Đến tháng');
		}
		if(msg.length > 0){
			$('#errMsg').html(msg).show();
			return false;
		}
		var shopCode = $('#shopCode').val();
		var fromMonth = $('#fromDate').val();
		var toMonth = $('#toDate').val();
		var year1 = '';
		year1 = fromMonth.split('/')[1];
		year1 = parseInt(year1);
		var year2 = '';
		year2 = toMonth.split('/')[1];
		year2 = parseInt(year2);
		if(year1 != '' && year2 != '' && year1 != year2) {
			$('#errMsg').html('Từ tháng và đến tháng phải cùng một năm').show();
			return;
		}
		var fromDate = '01/' + fromMonth;
		var toDate = '01/' + toMonth;
		
		var now = new Date();
		var nowMonth = now.getMonth() + 1;
		var nowYear = now.getFullYear();
		
		if(year2 >= nowYear && Number(toMonth.split('/')[0]) > nowMonth) {
			$('#errMsg').html('Đến tháng phải nhỏ hơn tháng hiện tại. Vui lòng nhập lại').show();
			$('#toDate').focus();
			return;
		}
		
		if(!Utils.isDate(fromDate, '/')){
			$('#errMsg').html('Từ tháng không hợp lệ. Vui lòng nhập lại').show();
			$('#fromDate').focus();
			return;
		}
		
		if(!Utils.isDate(toDate, '/')) {
			$('#errMsg').html('Đến tháng không hợp lệ. Vui lòng nhập lại').show();
			$('#toDate').focus();
			return;
		}
		
		if(!Utils.compareDate(fromDate, toDate)) {
			$('#errMsg').html('Từ tháng phải nhỏ hơn đến tháng').show();
			return;
		}
		var dataModel = new Object();
		dataModel.shopCode = shopCode;
		dataModel.fromDate = fromDate;
		dataModel.toDate = toDate;
		ReportUtils.exportReport('/report/crm/kd14/export',dataModel, null, function(){
			$('#.fancybox-inner #formatType option[value=PDF]').remove();
		});
	},
	exportKD007 : function(){
		var msg = "";
		if(msg.length == 0){
			msg = Utils.getMessageOfRequireCheck('fDate','Từ ngày');
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfRequireCheck('tDate','Đến ngày');
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfInvalidFormatDate('fDate','Từ ngày');
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfInvalidFormatDate('tDate','Đến ngày');
		}
		var fDate = $('#fDate').val();
		var tDate = $('#tDate').val();
		if(!Utils.compareDate(fDate, tDate)){
			msg = msgErr_fromdate_greater_todate;		
			$('#tDate').focus();
		}
		if(msg.length > 0){
			$('#errMsg').html(msg).show();
			return false;
		}
		var dataModel = new Object();
		dataModel.shopId = $('#shopId').val().trim();
		dataModel.fromDate = $('#fDate').val().trim();
		dataModel.toDate = $('#tDate').val().trim();
		ReportUtils.exportReport('/report/crm/kd7/export',dataModel);
	},
	exportKD10_1 : function(){
		$('#errMsg').html('').hide();
		var msg = "";
		msg = Utils.getMessageOfRequireCheck('subCat','Nhãn hàng',true);
		if(msg.length == 0){
			msg = Utils.getMessageOfRequireCheck('toDate','Đến ngày');
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfInvalidFormatDate('toDate','Đến ngày');
		}
		var now = new Date();
		var cMonth = now.getMonth() + 1;
		var cYear = now.getFullYear();
		var aoDate = $('#toDate').val().split('/');
		var month = aoDate[1];
		var year = aoDate[2];
		if(month != cMonth || year != cYear ){
			msg = 'Đến ngày không thuộc tháng hiện tại';
			$('#toDate').focus();
		}
		if(msg.length > 0){
			$('#errMsg').html(msg).show();
			return false;
		}
		var dataModel = new Object();
		dataModel.toDate = $('#toDate').val();
		dataModel.subCatId = $('#subCat').val();
		ReportUtils.exportReport('/report/crm/kd10_1/export',dataModel, true);
	},
	exportKD12 : function(){
		var msg = "";
		$('#errMsg').html(msg).hide();
		if($('#tDate').val()==""){
			msg = "Bạn vui lòng chọn đến ngày.";
			$('#tDate').focus();
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfInvalidFormatDate('tDate','Đến ngày');
		}
		if(!Utils.compareCurrentDate($('#tDate').val())){
			msg="Đến ngày lớn hơn ngày hiện tại, mời bạn chọn lại.";
			$('#tDate').focus();
		}
		if(msg.length > 0){
			$('#errMsg').html(msg).show();
			return false;
		}
		var dataModel = new Object();
		dataModel.toDate = $('#tDate').val();
		dataModel.shopId = $('#shop').combotree('getValue');
		dataModel.listNVBHCode = $('#staffSaleCode').val().trim();
		dataModel.listNVBHId = SuperviseCustomer.mapNVBH.keyArray;
		dataModel.checkKD12 = 0;
		var kData = $.param(dataModel, true);
		$.ajax({
			type : "POST",
			url : '/report/crm/kd12/export',
			dataType : "json",
			data : (kData),
			success : function(data) {
				if(data.error == false) {
					dataModel.checkKD12 = 1;
					dataModel.listNVBHId = data.listnvbhid;
					ReportUtils.exportReport('/report/crm/kd12/export',dataModel);
				}else{
					$('#errMsg').html(data.errorMsg).show();
				}
			},
			error: function(XMLHttpRequest, textStatus, errorThrown) {
			}
		});	
	},
	exportKD15 : function(){
		var msg = "";
		$('#errMsg').html(msg).hide();
		msg = Utils.getMessageOfRequireCheck('tDate','Tháng');		
		if(msg.length == 0){
			$('#tmptDate').val('01/' + $('#tDate').val().trim());
			msg = Utils.getMessageOfInvalidFormatDate('tmptDate','Tháng');
			$('#tDate').focus();
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfRequireCheck('inputValue','Số tháng');
		}
		if(msg.length == 0){
			var inputValue = $('#inputValue').val().trim();
			if(isNaN(inputValue) || inputValue<1 || inputValue>12){
				$('#inputValue').focus();
				msg = 'Số tháng là kiểu số nguyên và chỉ nằm trong đoạn từ 1 - 12 (tháng).';
			}		
		}
		if(msg.length > 0){
			$('#errMsg').html(msg).show();
			return false;
		}
		var dataModel = new Object();
		dataModel.toDate = '01/' + $('#tDate').val();
		dataModel.inputValue = $('#inputValue').val();			
		ReportUtils.exportReport('/report/crm/kd15/export',dataModel,true);
	},
	exportKD19 : function(){
		var msg = "";
		$('#errMsg').html(msg).hide();
		if($('#badscore').val()==""){
			msg="Số lần là trường bắt buộc.Yêu cầu nhập.";
			$('#badscore').focus();
		}
		if (msg.length==0){
			msg = Utils.getMessageOfSpecialCharactersValidateEx('badscore', 'Số lần ', Utils._TF_NUMBER,'');
			$('#badscore').focus();
		}
		if (parseInt($('#badscore').val(),10)==0 && msg.length == 0){
			msg="Số lần là một số nguyên dương.";
			$('#badscore').focus();
		}
		if($('#tDate').val()=="" && msg.length == 0){
			msg="Đến ngày là trường bắt buộc.Yêu cầu nhập.";
			$('#tDate').focus();
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfInvalidFormatDate('tDate','Đến ngày');
		}
		if(!Utils.compareCurrentDate($('#tDate').val()) && msg.length == 0){
			msg="Đến ngày lớn hơn ngày hiện tại, mời bạn chọn lại.";
			$('#tDate').focus();
		}
		if(msg.length > 0){
			$('#errMsg').html(msg).show();
			return false;
		}
		var dataModel = new Object();
		dataModel.toDate = $('#tDate').val();
		dataModel.shopId = $('#shop').combotree('getValue');
		dataModel.numTimeBadScore = $('#badscore').val();
		ReportUtils.exportReport('/report/crm/kd19/export',dataModel);
	},
	/*@author: hunglm16; @since: February 10, 2014; @description: KD10.2-Điểm lẻ cần phân phối theo SKUs đề nghị*/
	exportKD10_2 : function(){
		$('.ErrorMsgStyle').html('').show();
		var msg = "";
		msg = Utils.getMessageOfRequireCheck('subCat','Nhãn hàng',true);
		if(msg.length == 0){
			msg = Utils.getMessageOfRequireCheck('tDate','Đến ngày');
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfInvalidFormatDate('tDate','Đến ngày');
		}
		if(msg.length > 0){
			$('#errMsg').html(msg).show();
			return false;
		}
		var dataModel = new Object();
		dataModel.toDate = $('#tDate').val();
		dataModel.subCatId = $('#subCat').val();
		ReportUtils.exportExcel('/report/crm/kd10_2/export',dataModel, 'errMsg');
	},
	exportBCKD18: function(){
			var msg = '';
			$('#errMsg').html('').hide();
			if(msg.length ==0){
				msg = Utils.getMessageOfRequireCheck('fromMonth','Từ tháng');
			}
			if(msg.length ==0){
				msg = Utils.getMessageOfInvalidFormatDateEx('fromMonth','Từ tháng', Utils._DATE_MM_YYYY);
			}
			if(msg.length ==0){
				msg = Utils.getMessageOfRequireCheck('toMonth','Đến tháng');
			}
			if(msg.length ==0){
				msg = Utils.getMessageOfInvalidFormatDateEx('toMonth','Đến tháng', Utils._DATE_MM_YYYY);
			}
			var fMonth = $('#fromMonth').val();
			var tMonth = $('#toMonth').val();
			if(!Utils.compareMonth(fMonth, tMonth)){
				msg = msgErr_frommonth_greater_tomonth;		
				$('#fromMonth').focus();
			}
			if(msg.length > 0){
				$('#errMsg').html(msg).show();
				return false;
			}
			var dataModel = new Object();		
			dataModel.fromDate = $('#fromMonth').val().trim();
			dataModel.toDate = $('#toMonth').val().trim();
			ReportUtils.exportReport('/report/crm/kd18/export',dataModel);
		},
	 /*@author: phuongvm; @description: KD16.1-Thống kê số xã chưa PSDS */
	exportKD_16_1 : function(){
		var msg = '';
		$('#errMsg').html('').hide();
		if(msg.length ==0){
			msg = Utils.getMessageOfRequireCheck('fromMonth','Từ tháng');
		}
		if(msg.length ==0){
			msg = Utils.getMessageOfInvalidFormatDateEx('fromMonth','Từ tháng', Utils._DATE_MM_YYYY);
		}
		if(msg.length ==0){
			msg = Utils.getMessageOfRequireCheck('toMonth','Đến tháng');
		}
		if(msg.length ==0){
			msg = Utils.getMessageOfInvalidFormatDateEx('toMonth','Đến tháng', Utils._DATE_MM_YYYY);
		}
		var fMonth = $('#fromMonth').val();
		var tMonth = $('#toMonth').val();
		if(!Utils.compareMonth(fMonth, tMonth)){
			msg = msgErr_frommonth_greater_tomonth;		
			$('#fromMonth').focus();
		}
		if(msg.length > 0){
			$('#errMsg').html(msg).show();
			return false;
		}
		var dataModel = new Object();		
		dataModel.fromMonth = $('#fromMonth').val().trim();
		dataModel.toMonth = $('#toMonth').val().trim();
		ReportUtils.exportReport('/report/crm/kd16_1/export',dataModel);
	},
	exportKD11_1 : function(){
		var msg = '';
		$('#errMsg').html('').hide();
		if(msg.length == 0){
			msg = Utils.getMessageOfRequireCheck('programCode', 'CTTB');
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfRequireCheck('fDate', 'Từ ngày');
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfInvalidFormatDate('fDate','Từ ngày');
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfRequireCheck('tDate','Đến ngày');
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfInvalidFormatDate('tDate','Đến ngày');
		}
		var fDate = $('#fDate').val();
		var tDate = $('#tDate').val();
		if(!Utils.compareDate(fDate, tDate)){
			msg = msgErr_fromdate_greater_todate;		
			$('#fDate').focus();
		}
		if(msg.length > 0){
			$('#errMsg').html(msg).show();
			return false;
		}
		var dataModel = new Object();		
		dataModel.programCode = $('#programCode').val().trim();
		dataModel.fromDate = $('#fDate').val().trim();
		dataModel.toDate = $('#tDate').val().trim();
		dataModel.type = $('#type').val().trim();
		$('#typeReport').val($('#type').val().trim());
		ReportUtils.exportReport('/report/crm/kd11_1/export',dataModel);
	},
	exportBCVT4 : function(){
		var msg = '';
		$('#errMsg').html('').hide();
		if(msg.length == 0){
			msg = Utils.getMessageOfRequireCheck('fromDate', 'Từ ngày');
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfInvalidFormatDate('fromDate','Từ ngày');
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfRequireCheck('toDate','Đến ngày');
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfInvalidFormatDate('toDate','Đến ngày');
		}
		var fDate = $('#fromDate').val();
		var tDate = $('#toDate').val();
		if(!Utils.compareDate(fDate, tDate)){
			msg = msgErr_fromdate_greater_todate;		
			$('#fromDate').focus();
		}
		if(msg.length > 0){
			$('#errMsg').html(msg).show();
			return false;
		}
		var dataModel = new Object();
		dataModel.fromDate = $('#fromDate').val().trim();
		dataModel.toDate = $('#toDate').val().trim();
		ReportUtils.exportReport('/report/crm/vt4/export',dataModel);
	},
	exportBCDS1 : function(){
		/*var msg = '';
		$('#errMsg').html('').hide();
		if(msg.length == 0){
			msg = Utils.getMessageOfRequireCheck('fromDate', 'Từ ngày');
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfInvalidFormatDate('fromDate','Từ ngày');
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfRequireCheck('toDate','Đến ngày');
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfInvalidFormatDate('toDate','Đến ngày');
		}
		var fDate = $('#fromDate').val();
		var tDate = $('#toDate').val();
		if(!Utils.compareDate(fDate, tDate)){
			msg = msgErr_fromdate_greater_todate;		
			$('#fromDate').focus();
		}
		if(msg.length > 0){
			$('#errMsg').html(msg).show();
			return false;
		}
		var dataModel = new Object();
		dataModel.fromDate = $('#fromDate').val().trim();
		dataModel.toDate = $('#toDate').val().trim();
		ReportUtils.exportReport('/report/crm/ds1/export',dataModel);*/
		
		var dataModel = new Object();
		dataModel.typeCurrentShop = 10000;
		dataModel.formatType = 'XLS';
		var kData = $.param(dataModel, true);
		$('#divOverlay').show();
		$.ajax({
			type : 'GET',
			url : '/report/crm/ds1/export',
			data :(kData),
			dataType : "json",
			success : function(data) {
				$('#divOverlay').hide();
				if(!data.error && data.path!= null && data.path.length > 0){
					window.location.href = data.path;
					setTimeout(function(){ //Set timeout để đảm bảo file load lên hoàn tất
	                    CommonSearch.deleteFileExcelExport(data.path);
					},2000);
				}
			},
		error:function(XMLHttpRequest, textStatus, errorThrown) {}});
	},
	exportBCDS2 : function(){
		var msg = '';
		$('#errMsg').html('').hide();
		if(msg.length == 0){
			msg = Utils.getMessageOfRequireCheck('fromDate', 'Từ ngày');
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfInvalidFormatDate('fromDate','Từ ngày');
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfRequireCheck('toDate','Đến ngày');
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfInvalidFormatDate('toDate','Đến ngày');
		}
		var fDate = $('#fromDate').val();
		var tDate = $('#toDate').val();
		if(!Utils.compareDate(fDate, tDate)){
			msg = msgErr_fromdate_greater_todate;		
			$('#fDate').focus();
		}
		if(msg.length > 0){
			$('#errMsg').html(msg).show();
			return false;
		}
		var dataModel = new Object();
		dataModel.fromDate = $('#fromDate').val().trim();
		dataModel.toDate = $('#toDate').val().trim();
		ReportUtils.exportReport('/report/crm/ds2/export',dataModel);
	},
	
	exportBCVT2 : function(){
		var msg = '';
		$('#errMsg').html('').hide();
		if(msg.length == 0){
			msg = Utils.getMessageOfRequireCheck('fromDate', 'Từ ngày');
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfInvalidFormatDate('fromDate','Từ ngày');
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfRequireCheck('toDate','Đến ngày');
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfInvalidFormatDate('toDate','Đến ngày');
		}
		var fDate = $('#fromDate').val();
		var tDate = $('#toDate').val();
		if(!Utils.compareDate(fDate, tDate)){
			msg = msgErr_fromdate_greater_todate;		
			$('#fromDate').focus();
		}
		if(msg.length > 0){
			$('#errMsg').html(msg).show();
			return false;
		}
		var dataModel = new Object();
		dataModel.fromDate = $('#fromDate').val().trim();
		dataModel.toDate = $('#toDate').val().trim();
		ReportUtils.exportReport('/report/crm/vt2/export',dataModel);
	},
	
	/**
	 * Bao cao KD1- Doanh so ban hang cua nhan vien ban hang theo SKUs
	 * @author hunglm16
	 * @since January 23,2014
	 * */
	exportBCTHCTHTB_KD1 : function(){
		var msg = '';
		$('#errMsg').html('').hide();
		
		if(msg.length == 0 && $('.combo-value').val()==undefined && $('.combo-value').val().length ==0){
			msg = "Chưa chọn Đơn Vị";
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfRequireCheck('tDate','Đến ngày');
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfInvalidFormatDate('tDate','Đến ngày');
		}
		if(msg.length > 0){
			$('#errMsg').html(msg).show();
			return false;
		}
		var dataModel = new Object();
		dataModel.shopId = $('.combo-value').val();
		dataModel.toDate = $('#tDate').val();
		dataModel.lstProductCode = $('#product').val();
		ReportUtils.exportExcel('/report/crm/kd1/export', dataModel, 'errMsg');
	},
	
	// vuonghn : KD5
	exportKD5: function(){
		var msg = '';
		$('#errMsg').html('').hide();
		if(msg.length == 0){
			msg = Utils.getMessageOfRequireCheck('fDate','Từ ngày');
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfInvalidFormatDate('fDate','Từ ngày');
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfRequireCheck('tDate','Đến ngày');
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfInvalidFormatDate('tDate','Đến ngày');
		}
		var fDate = $('#fDate').val();
		var tDate = $('#tDate').val();
		if(!Utils.compareDate(fDate, tDate)){
			msg = msgErr_fromdate_greater_todate;		
			$('#fDate').focus();
		}
		var currentTime = new Date();
		var month = currentTime.getMonth() + 1;
		var day = currentTime.getDate();
		var year = currentTime.getFullYear();
		if(!Utils.compareDate(fDate,day + '/' + month + '/' + year)){
			msg = 'Từ ngày không được lớn hơn ngày hiện tại';
			$('#fDate').focus();
		}
		if(msg.length == 0 && !Utils.compareCurrentDate(tDate)) {
			msg = 'Đến ngày phải nhỏ hơn ngày hiện tại. Vui lòng nhập lại';
			$('#tDate').focus();
		}
		var lstCategoryId = '';
		var lstCategory = $('#category').val();
		if(lstCategory != null && lstCategory.length>0){
			for(var i=0;i<lstCategory.length;i++){
				if(lstCategory[i]!=null&&lstCategory[i]!=''){
					if(lstCategoryId.length>0){
						lstCategoryId+=',';
					}
					lstCategoryId+=lstCategory[i];
				}
			}
		}else{
			msg = "Vui lòng chọn ngành hàng.";
		}
		if(msg.length > 0){
			$('#errMsg').html(msg).show();
			return false;
		}
		var dataModel = new Object();
		dataModel.shopId = $('#shopId').val();
		dataModel.lstCategoryId = lstCategoryId;
		dataModel.fromDate = $('#fDate').val().trim();
		dataModel.toDate = $('#tDate').val().trim();
		ReportUtils.exportReport('/report/crm/kd5/export',dataModel);
	},	
	
	/**
	 * Bao cao KD3- Doanh so ban hang cua nhan vien ban hang theo SKUs
	 * @author hunglm16
	 * @since January 23,2014
	 * */
	exportBCTHCTHTB_KD3 : function(){
		var msg = '';
		$('#errMsg').html('').hide();
		
		if(msg.length == 0 && $('.combo-value').val()==undefined && $('.combo-value').val().length ==0){
			msg = "Chưa chọn Đơn Vị";
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfRequireCheck('tDate','Đến ngày');
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfInvalidFormatDate('tDate','Đến ngày');
		}
		if(msg.length > 0){
			$('#errMsg').html(msg).show();
			return false;
		}
		var dataModel = new Object();
		dataModel.shopId = $('.combo-value').val();
		dataModel.toDate = $('#tDate').val();
		dataModel.lstProductCode = $('#product').val();
		ReportUtils.exportExcel('/report/crm/kd3/export', dataModel, 'errMsg');
	},
	exportKD10: function(){
		var msg = "";
		if(msg.length == 0){
			msg = Utils.getMessageOfRequireCheck('fDate','Từ ngày');
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfInvalidFormatDate('fDate','Từ ngày');
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfRequireCheck('tDate','Từ ngày');
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfInvalidFormatDate('tDate','Đến ngày');
		}
		var fDate = $('#fDate').val();
		var tDate = $('#tDate').val();
		var dataModel = new Object();
		dataModel.fromDate = $('#fDate').val();
		dataModel.toDate = $('#tDate').val();
		ReportUtils.exportReport('/report/crm/kd10/export',dataModel);
	}
};
function removeE(list,key){
	if (list!=null && list.length > 0){
		for (var i=0; i< list.length;i++){
			if (list[i]== key){
				list.splice(i,1);
			}
		}
	}
}
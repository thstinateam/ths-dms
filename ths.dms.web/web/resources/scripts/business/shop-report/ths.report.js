/*
 * JavaScript file created by Rockstarapps Concatenation
*/

/*
 * START OF FILE - /web/web/resources/scripts/business/shop-report/vnm.stock-report.js
 */
var StockReport = {	
		
   /**
	* Bc 3.1 xuat nhap ton
	* 
	* @author hoanv25
	* @since July 31, 2015
	* */	
	report7_4_2: function(){
		var msg = '';
		$('#errMsg').html('').hide();
		if($('#shopId').val().trim().length == 0 || $('#shopId').val().trim() == activeType.ALL){
			msg = format(msgErr_required_field,'đơn vị');
		}
		if(msg.length==0){
			if($('#isNPP').val() == "false"){
				msg="Đơn vị bạn vừa chọn không phải là nhà phân phối cụ thể!";
			}
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfRequireCheck('fDate','Từ ngày');
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfInvalidFormatDate('fDate','Từ ngày');
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfRequireCheck('tDate','Từ ngày');
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfInvalidFormatDate('tDate','Đến ngày');
		}
		var fDate = $('#fDate').val();
		var tDate = $('#tDate').val();
		if(!Utils.compareDate(fDate, tDate)){
			msg = msgErr_fromdate_greater_todate;		
			$('#fDate').focus();
		}		
		if(!Utils.compareDate(fDate, tDate)){
			msg = msgErr_fromdate_greater_todate;		
			$('#fDate').focus();
		}
		var currentTime = new Date();
		var month = currentTime.getMonth() + 1;
		var day = currentTime.getDate();
		var year = currentTime.getFullYear();
		if(!Utils.compareDate(fDate,day + '/' + month + '/' + year)){
			msg = 'Từ ngày không được lớn hơn ngày hiện tại';
			$('#fDate').focus();
		}
		if(!Utils.compareDate(tDate,day + '/' + month + '/' + year)){
			msg = 'Đến ngày không được lớn hơn ngày hiện tại';
			$('#tDate').focus();
		}
		if(msg.length > 0){
			$('#errMsg').html(msg).show();
			return false;
		}
		var dataModel = new Object();		
		dataModel.strListShopId=$('#shopId').val();
		dataModel.fromDate = $('#fDate').val().trim();
		dataModel.toDate = $('#tDate').val().trim();
		dataModel.orderStock = $('#orderStock').val();
		if ($('#orderProduct').val() != null && $('#orderProduct').val() > 0) {
			dataModel.orderProduct = $('#orderProduct').val();
		}	
		dataModel.cycleId = $('#cycle').val();			
		if($('input[name=formatType]:radio:checked').length > 0 && $('input[name=formatType]:radio:checked').val() != undefined) {
            dataModel.formatType = $('input[name=formatType]:radio:checked').val().toUpperCase();
		}
		ReportUtils.exportReport('/report/stock/f1/export',dataModel);
	},
	/**
	* Bc 3.1 Thay doi gia tri loai sp theo dieu kien kho chon
	* 
	* @author hoanv25
	* @since August 3, 2015
	* */	
	change: function() {
		if ($('#orderStock').val() == 1) {
			$('#sub .CustomStyleSelectBoxInner').html("Tất cả");
			html = '<option value="0" selected="selected">' + 'Tất cả' + '</option>';
			html += '<option value="1">' + 'Thành phẩm' + '</option>';
			html += '<option value="2">' + 'POSM' + '</option>';
			$('#orderProduct').html(html);
		} else {
			$('#sub .CustomStyleSelectBoxInner').html("Thành phẩm");
			html = '<option value="1" selected="selected">' + 'Thành phẩm' + '</option>';
			$('#orderProduct').html(html);
		}
	},
	/**
	* Bc 3.2 xuat nhap ton chi tiet
	* 
	* @author hoanv25
	* @since August 3, 2015
	* */	
	report7_4_3: function(){
		var msg = '';
		$('#errMsg').html('').hide();
		if($('#shopId').val().trim().length == 0 || $('#shopId').val().trim() == activeType.ALL){
			msg = format(msgErr_required_field,'đơn vị');
		}
		if(msg.length==0){
			if($('#isNPP').val() == "false"){
				msg="Đơn vị bạn vừa chọn không phải là nhà phân phối cụ thể!";
			}
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfRequireCheck('fDate','Từ ngày');
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfInvalidFormatDate('fDate','Từ ngày');
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfRequireCheck('tDate','Từ ngày');
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfInvalidFormatDate('tDate','Đến ngày');
		}
		var fDate = $('#fDate').val();
		var tDate = $('#tDate').val();
		if(!Utils.compareDate(fDate, tDate)){
			msg = msgErr_fromdate_greater_todate;		
			$('#fDate').focus();
		}		
		if(!Utils.compareDate(fDate, tDate)){
			msg = msgErr_fromdate_greater_todate;		
			$('#fDate').focus();
		}
		var currentTime = new Date();
		var month = currentTime.getMonth() + 1;
		var day = currentTime.getDate();
		var year = currentTime.getFullYear();
		if(!Utils.compareDate(fDate,day + '/' + month + '/' + year)){
			msg = 'Từ ngày không được lớn hơn ngày hiện tại';
			$('#fDate').focus();
		}
		if(!Utils.compareDate(tDate,day + '/' + month + '/' + year)){
			msg = 'Đến ngày không được lớn hơn ngày hiện tại';
			$('#tDate').focus();
		}
		if(msg.length > 0){
			$('#errMsg').html(msg).show();
			return false;
		}
		var dataModel = new Object();		
		dataModel.strListShopId=$('#shopId').val();
		dataModel.fromDate = $('#fDate').val().trim();
		dataModel.toDate = $('#tDate').val().trim();
		/*dataModel.orderStock = $('#orderStock').val();
		if ($('#orderProduct').val() != null && $('#orderProduct').val() > 0) {
			dataModel.orderProduct = $('#orderProduct').val();
		}*/
		dataModel.cycleId = $('#cycle').val();
		ReportUtils.exportReport('/report/stock/xntct/export',dataModel);
	},
	
	/**
	* Bc 4.2 ton kho theo nhan vien vansale
	* 
	* @author hoanv25
	* @since August 3, 2015
	* */	
	reportTKTNVVS4_2: function(){
		var msg = '';
		$('#errMsg').html('').hide();
		if($('#shopId').val().trim().length == 0 || $('#shopId').val().trim() == activeType.ALL){
			msg = format(msgErr_required_field,'đơn vị');
		}
		if(msg.length==0){
			if($('#isNPP').val() == "false"){
				msg="Đơn vị bạn vừa chọn không phải là nhà phân phối cụ thể!";
			}
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfRequireCheck('fDate','Từ ngày');
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfInvalidFormatDate('fDate','Từ ngày');
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfRequireCheck('tDate','Đến ngày');
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfInvalidFormatDate('tDate','Đến ngày');
		}
		var fDate = $('#fDate').val();
		var tDate = $('#tDate').val();
		if(!Utils.compareDate(fDate, tDate)){
			msg = msgErr_fromdate_greater_todate;		
			$('#fDate').focus();
		}		

		var currentTime = new Date();
		var month = currentTime.getMonth() + 1;
		var day = currentTime.getDate();
		var year = currentTime.getFullYear();
		if(!Utils.compareDate(fDate,day + '/' + month + '/' + year)){
			msg = 'Từ ngày không được lớn hơn ngày hiện tại';
			$('#fDate').focus();
		}
		if(!Utils.compareDate(tDate,day + '/' + month + '/' + year)){
			msg = 'Đến ngày không được lớn hơn ngày hiện tại';
			$('#tDate').focus();
		}
		if(msg.length > 0){
			$('#errMsg').html(msg).show();
			return false;
		}
		var dataModel = new Object();		
		dataModel.strListShopId=$('#shopId').val();
		dataModel.fromDate = $('#fDate').val().trim();
		dataModel.toDate = $('#tDate').val().trim();
		
		ReportUtils.exportReport('/report/stock/tktnvvs/export',dataModel);
	},
	
	/**
	* Bc 3.3 tinh trang don hang
	* 
	* @author hoanv25
	* @version
	* @since August 5, 2015
	* */	
	report7_4_5: function(){
		var msg = '';
		$('#errMsg').html('').hide();
		if($('#shopId').val().trim().length == 0 || $('#shopId').val().trim() == activeType.ALL){
			msg = format(msgErr_required_field,'đơn vị');
		}
		if(msg.length==0){
			if($('#isNPP').val() == "false"){
				msg="Đơn vị bạn vừa chọn không phải là nhà phân phối cụ thể!";
			}
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfRequireCheck('fDate','Từ ngày');
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfInvalidFormatDate('fDate','Từ ngày');
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfRequireCheck('tDate','Từ ngày');
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfInvalidFormatDate('tDate','Đến ngày');
		}
		var fDate = $('#fDate').val();
		var tDate = $('#tDate').val();
		if(!Utils.compareDate(fDate, tDate)){
			msg = msgErr_fromdate_greater_todate;		
			$('#fDate').focus();
		}		
		if(!Utils.compareDate(fDate, tDate)){
			msg = msgErr_fromdate_greater_todate;		
			$('#fDate').focus();
		}
		var currentTime = new Date();
		var month = currentTime.getMonth() + 1;
		var day = currentTime.getDate();
		var year = currentTime.getFullYear();
		if(!Utils.compareDate(fDate,day + '/' + month + '/' + year)){
			msg = 'Từ ngày không được lớn hơn ngày hiện tại';
			$('#fDate').focus();
		}
		if(msg.length > 0){
			$('#errMsg').html(msg).show();
			return false;
		}
		var dataModel = new Object();		
		dataModel.shopId = $('#shopId').val();
		dataModel.fromDate = $('#fDate').val().trim();
		dataModel.toDate = $('#tDate').val().trim();
		dataModel.orderNumber = $('#orderNumber').val().trim();
		if ($('#orderStatus').val() != null && $('#orderStatus').val() >= 0) {
			dataModel.orderStatus = $('#orderStatus').val();
		}
		dataModel.cycleId = $('#cycle').val();
		ReportUtils.exportReport('/report/stock/bcttnh/export',dataModel);
	},
	
	/**
	* Bc 3.4 bao cao kiem ke
	* 
	* @author hoanv25
	* @since August 3, 2015
	* */	
	report7_4_4: function(){
		var msg = '';
		$('#errMsg').html('').hide();
		if($('#shopId').val().trim().length == 0 || $('#shopId').val().trim() == activeType.ALL){
			msg = format(msgErr_required_field,'đơn vị');
		}
		if(msg.length==0){
			if($('#isNPP').val() == "false"){
				msg="Đơn vị bạn vừa chọn không phải là nhà phân phối cụ thể!";
			}
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfRequireCheck('fDate','Từ ngày');
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfInvalidFormatDate('fDate','Từ ngày');
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfRequireCheck('tDate','Từ ngày');
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfInvalidFormatDate('tDate','Đến ngày');
		}
		var fDate = $('#fDate').val();
		var tDate = $('#tDate').val();
		if(!Utils.compareDate(fDate, tDate)){
			msg = msgErr_fromdate_greater_todate;		
			$('#fDate').focus();
		}		
		if(!Utils.compareDate(fDate, tDate)){
			msg = msgErr_fromdate_greater_todate;		
			$('#fDate').focus();
		}
		var currentTime = new Date();
		var month = currentTime.getMonth() + 1;
		var day = currentTime.getDate();
		var year = currentTime.getFullYear();
		if(!Utils.compareDate(fDate,day + '/' + month + '/' + year)){
			msg = 'Từ ngày không được lớn hơn ngày hiện tại';
			$('#fDate').focus();
		}
		if(msg.length > 0){
			$('#errMsg').html(msg).show();
			return false;
		}
		var dataModel = new Object();		
		dataModel.strListShopId=$('#shopId').val();
		dataModel.fromDate = $('#fDate').val().trim();
		dataModel.toDate = $('#tDate').val().trim();
		dataModel.orderStock = $('#orderStock').val();
		if ($('#orderProduct').val() != null && $('#orderProduct').val() > 0) {
			dataModel.orderProduct = $('#orderProduct').val();
		}	
		dataModel.orderStatus = $('#orderStatus').val();
		dataModel.cycleId = $('#cycle').val();
		ReportUtils.exportReport('/report/stock/bckk/export',dataModel);
	},
	
	reportXNTCT: function(){
		var msg = '';
		$('#errMsg').html('').hide();
		if($('#shopId').val().trim().length == 0 || $('#shopId').val().trim() == activeType.ALL){
			msg = format(msgErr_required_field,'đơn vị');
		}
		if(msg.length == 0) {
			if($('#isNPP').val() == "false") {
				msg = "Đơn vị bạn vừa chọn không phải là 1 nhà phân phối cụ thể!";
			}
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfInvalidFormatDate('fDate','Từ ngày');
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfInvalidFormatDate('tDate','Đến ngày');
		}
		var fDate = $('#fDate').val();
		var tDate = $('#tDate').val();
		if(!Utils.compareDate(fDate, tDate)){
			msg = msgErr_fromdate_greater_todate;		
			$('#fDate').focus();
		}
		if(msg.length > 0){
			$('#errMsg').html(msg).show();
			return false;
		}
		var dataModel = new Object();		
		dataModel.shopId = $('#shopId').val().trim();
		dataModel.fromDate = $('#fDate').val().trim();
		dataModel.toDate = $('#tDate').val().trim();
		dataModel.formatType = $('input[name="formatType"]:checked').val();
		ReportUtils.exportReport('/report/stock/xntct/export',dataModel);
	}
};
/*
 * END OF FILE - /web/web/resources/scripts/business/shop-report/vnm.stock-report.js
 */

/*
 * START OF FILE - /web/web/resources/scripts/business/shop-report/vnm.report-utils.js
 */
var ReportUtils = {
	_isCheckAll: false,
	_objectSearchCTTB: null,
	_lstChecker: null,
	_lstUnchecker: null,
	_arrMultiChoiceDisplay: null,
	lstStaffId:null,
	_callbackBeforeImport:null,
	_callbackAfterImport:null,
	_idChoose: '',
	_currentShopCode:'',
	
	loadCombo: function(callback){		 
		var params = {};
		$.ajax({
			type : "POST",
			url:'/commons/list-shop-filter',
			data :($.param(params, true)),
			dataType : "json",
			success : function(data) {
				$('#shop').combobox({
					valueField: 'shopCode',
					textField:  'shopName',
					data: data.rows,
					panelWidth: '200',
					formatter: function(row) {
						return '<span style="font-weight:bold">' + Utils.XSSEncode(row.shopCode) + '</span><br/>' + '<span style="color:#888">' + Utils.XSSEncode(row.shopName) + '</span>';
					},
					filter: function(q, row){
						q = new String(q).toUpperCase().trim();
						var opts = $(this).combobox('options');
						return ((unicodeToEnglish(row[opts.textField]).indexOf(unicodeToEnglish(q)) == 0) || unicodeToEnglish(row[opts.valueField]).indexOf(unicodeToEnglish(q)) == 0) ;
					},	
			        onSelect: function(rec){
			        	if (rec != null && rec.shopCode != ReportUtils._currentShopCode){
			        		if(callback!=undefined && callback!=null){
								callback.call(this,rec.id);
							}
							$('#shopid').val(rec.id);
			        	}
			        },
			        onLoadSuccess: function(){
			        	var arr = $('#shop').combobox('getData');
			        	if (arr != null && arr.length > 0){
			        		$('#shop').combobox('select', arr[0].shopCode);
			        		ReportUtils._currentShopCode = arr[0].shopCode;
			        	}
			        	
			        }
				});
			}
		});
	},
	loadComboTree: function(controlId, storeCode, defaultValue, callback, params, vungType){		 
		var _url= '/rest/report/shop/combotree/1/0.json';
		if (vungType != undefined && vungType != null) {
			_url = '/rest/report/vung/combotree/1/0.json';	
		}
		$('#' + controlId).combotree({url: _url, 
			formatter: function(node) {
				return Utils.XSSEncode(node.text);
			},
			onChange: function(newValue, oldValue) {				
				if (params != null && params.length >0) {
					for (var i = 0; i < params.length; i++) {
						$('#'+params[i]).val('');
					}
				}
				if (DebitPayReport._lstStaff != null && DebitPayReport._lstStaff.size() >0) {
					DebitPayReport._lstStaff = new Map();
				}
			}
		});
		var t = $('#'+ controlId).combotree('tree');	
		t.tree('options').url = '';
		t.tree({			
			formatter: function(node) {
				return Utils.XSSEncode(node.text);
			},
			onSelect:function(node) {	
				$('#'+storeCode).val(node.id);
				if(callback!=undefined && callback!=null){
					callback.call(this, node.id, node);
				}
			},
		});
		if(defaultValue == undefined || defaultValue == null){
			$('#' + controlId).combotree('setValue',activeType.ALL);
		} else {
			$('#' + controlId).combotree('setValue',defaultValue);
		}
	},
	loadComboTreeAuthorize: function(controlId, storeCode,defaultValue,callback,params,vungType){		 
		var _url= '/rest/report/shop/combotree/authorize/1/0.json';
		$('#' + controlId).combotree({url: _url,
			formatter: function(node) {
				return Utils.XSSEncode(node.text);
			},
			onChange: function(newValue,oldValue){				
				if(params != null && params.length >0){
					for(var i=0;i<params.length;i++){
						$('#'+params[i]).val('');
					}
				}
				if(DebitPayReport._lstStaff != null && DebitPayReport._lstStaff.size() >0){
					DebitPayReport._lstStaff = new Map();
				}
			}
		});
		var t = $('#'+ controlId).combotree('tree');	
		t.tree({			
			formatter: function(node) {
				return Utils.XSSEncode(node.text);
			},
			onSelect:function(node){	
				if(callback!=undefined && callback!=null){//tungmt
					callback.call(this, node.id);
				}
				$('#'+storeCode).val(node.id);
				$.getJSON("/report/are-you-shop?shopId="+node.id,function(result){
					$('#isNPP').val(result.isNPP);
				});
			}
		});
		if(defaultValue == undefined || defaultValue == null){
			$('#' + controlId).combotree('setValue',activeType.ALL);
		} else {
			$('#' + controlId).combotree('setValue',defaultValue);
		}
	},
	loadComboTreeAuthorizeEx: function(controlId, storeCode,defaultValue,callback,params,vungType){		 
		var _url= '/catalog/report-manager/combobox-tree-shop';
		$('#' + controlId).combotree({url: _url,
			formatter: function(node) {
				return Utils.XSSEncode(node.text);
			},
			onChange: function(newValue,oldValue){				
				if(params != null && params.length >0){
					for(var i=0;i<params.length;i++){
						$('#'+params[i]).val('');
					}
				}
				if(DebitPayReport._lstStaff != null && DebitPayReport._lstStaff.size() >0){
					DebitPayReport._lstStaff = new Map();
				}
			}
		});
		var t = $('#'+ controlId).combotree('tree');	
		t.tree({			
			formatter: function(node) {
				return Utils.XSSEncode(node.text);
			},
			onSelect:function(node){	
				if(callback!=undefined && callback!=null){//tungmt
					callback.call(this, node.id);
				}
				$('#'+storeCode).val(node.id);
				$.getJSON("/report/are-you-shop?shopId="+node.id,function(result){
					$('#isNPP').val(result.isNPP);
				});
			}
		});
		if(defaultValue == undefined || defaultValue == null){
			$('#' + controlId).combotree('setValue',activeType.ALL);
		} else {
			$('#' + controlId).combotree('setValue',defaultValue);
		}
	},
	loadComboTree2: function(controlId, storeCode, defaultValue, callback, params, vungType){		 
		var _url= '/rest/report/shop/combotree/1/0.json';
		if (vungType != undefined && vungType != null) {
			_url = '/rest/report/vung/combotree/1/0.json';	
		}
		$('#' + controlId).combotree({
			url: _url,
			formatter: function(node) {
				return Utils.XSSEncode(node.text);
			},
			onChange: function(newValue, oldValue) {
				if (params != null && params.length > 0) {
					for (var i = 0; i < params.length; i++) {
						$('#'+params[i]).val('');
					}
				}
			}
		});
		
		var t = $('#'+ controlId).combotree('tree');	
		t.tree('options').url = '';
		t.tree({
			formatter: function(node) {
				return Utils.XSSEncode(node.text);
			},
			onSelect:function(node){	
				if (callback != undefined && callback != null) {
					callback.call(this, node);
				}
				$('#'+storeCode).val(node.id);
			}
		});
		if (defaultValue == undefined || defaultValue == null) {
			$('#' + controlId).combotree('setValue', activeType.ALL);
		} else {
			$('#' + controlId).combotree('setValue', defaultValue);
		}
	},
	/**
	 * @author hunglm16
	 * @description update lai ham chung
	 * */
	exportReport: function(url, dataModel, errMsg){
		try {
			dataModel.reportCode = $('#function_code').val();
		} catch(e) {
			// Oop! Error
		}
		var kData = $.param(dataModel,true);
		if (VTUtilJS.isNullOrEmpty(errMsg)) {
			errMsg = 'errMsg';
		}
		$('#'+errMsg).html('').hide();
		$('#divOverlay').show();		
		$.ajax({
			type : "POST",
			url : url,
			data : (kData),
			dataType: "json",
			success : function(data) {
				$('#divOverlay').hide();				
				if (!data.error) {
					if(data.hasData != undefined && data.hasData != null && data.hasData == false) {
						$('#'+errMsg).html('Không có dữ liệu để xuất!').show();
					} else {
						var formatType=$('input[name="formatType"]:checked').val();
						var filePath = data.path || data.view;
						if (filePath) {
							filePath = ReportUtils.buildReportFilePath(filePath);
							if(formatType && formatType == "PDF"){
								ReportUtils.openInNewTab(filePath);
							} else {
								var idx = filePath.lastIndexOf(".");
								if (idx > 0 && idx < filePath.length) {
									var ext = filePath.substring(idx, idx + 4);
									if (ext != null && ReportUtils.PDF_FORMAT === ext.toLowerCase()) {
										ReportUtils.openInNewTab(filePath);
									} else {
										window.location.href = filePath;
									}
								} else {
									window.location.href = filePath;
								}
							}
							setTimeout(function(){
								//Set timeout de dam bao file load len hoan tat
								CommonSearch.deleteFileExcelExport(filePath);
							}, 15000);
						} else {
							$('#'+errMsg).html('Không xác định được đường dẫn đến tập tin dữ liệu').show();
						}
					}
				} else {
					$('#'+errMsg).html(data.errMsg).show();					
				}
			},
			error: function (XMLHttpRequest, textStatus, errorDivThrown) {
				$('#divOverlay').hide();
				StockIssued.xhrExport = null;
			}
		});
	},
	/**
	 * @author tamvnm
	 * @sine 22/07/2015
	 * @description export co call back
	 * */
	exportReportWithCallBack: function(dataModel, url, errMsg, callback){
		try {
			dataModel.reportCode = $('#function_code').val();
		} catch(e) {
			// Oop! Error
		}
		var kData = $.param(dataModel,true);
		if (VTUtilJS.isNullOrEmpty(errMsg)) {
			errMsg = 'errMsg';
		}
		$('#'+errMsg).html('').hide();
		$('#divOverlay').show();
		$("#imgOverlay").show();
		$.ajax({
			type : "POST",
			url : url,
			data : (kData),
			dataType: "json",
			success : function(data) {
				$('#divOverlay').hide();				
				if (!data.error) {
					if(data.hasData != undefined && data.hasData != null && data.hasData == false) {
						$('#'+errMsg).html('Không có dữ liệu để xuất!').show();
					} else {
						var formatType=$('input[name="formatType"]:checked').val();
						var filePath = data.path || data.view;
						if (filePath) {
							filePath = ReportUtils.buildReportFilePath(filePath);
							if(formatType && formatType == "PDF"){
								ReportUtils.openInNewTab(filePath);
							} else {
								var idx = filePath.lastIndexOf(".");
								if (idx > 0 && idx < filePath.length) {
									var ext = filePath.substring(idx, idx + 4);
									if (ext != null && ReportUtils.PDF_FORMAT === ext.toLowerCase()) {
										ReportUtils.openInNewTab(filePath);
									} else {
										ReportUtils.openInNewTab(filePath);
									}
								} else {
									ReportUtils.openInNewTab(filePath);
								}
							}
							setTimeout(function(){
								//Set timeout de dam bao file load len hoan tat
								CommonSearch.deleteFileExcelExport(filePath);
							}, 15000);
							if(callback!= null && callback!= undefined){
								callback.call(this,data);
							}
						} else {
							$('#'+errMsg).html('Không xác định được đường dẫn đến tập tin dữ liệu').show();
						}
					}
				} else {
					$('#'+errMsg).html(data.errMsg).show();					
				}
			},
			error: function (XMLHttpRequest, textStatus, errorDivThrown) {
				$('#divOverlay').hide();
				StockIssued.xhrExport = null;
			}
		});
	},
	openInNewTab:function(url) {
		var win = window.open(url, '_blank');
		win.focus();
	},
	/**
	 * Tao cay bao cao
	 * 
	 * @author hunglm16
	 * @since October 1,2014
	 * */
	loadTree: function(){
		loadDataForTree('/rest/report/tree.json');
		$('#tree').bind("loaded.jstree", function(event, data){
			//ReportUtils.showReportForm('XNTF1');
		});
		$('#tree').bind("select_node.jstree", function (event, data) {			
			var id = data.rslt.obj.attr("id");
			var url = data.rslt.obj.attr("url");
			var text = data.rslt.obj.attr("name");
			if (ReportUtils._idChoose != undefined && ReportUtils._idChoose!=null && ReportUtils._idChoose != id) {
				if (url == undefined || url == null || url.trim().length == 0) {
					return;
				}
				ReportUtils._idChoose = id;
				if(id!= null && id!= undefined && id.length > 0){
					$.ajax({
						type : 'GET',
						url : url,			
						dataType : "html",
						success : function(data) {
							$('#reportType').val(id);
							$('#title').html(text);
							$('#reportContainer').html(data);				
						},
						error:function(XMLHttpRequest, textStatus, errorThrown) {}}
					);
				}
			}
	    });
		$('#tree').bind("open_node.jstree", function (event, data) {
	    });
	},
	setCurrentDateForCalendar: function(objId,firstDay){
		var currentTime = new Date();
		var month = currentTime.getMonth() + 1;
		var day = currentTime.getDate();
		var year = currentTime.getFullYear();
		if(month < 10){
			month = '0' + month;
		}
		if(day < 10){
			day = '0' + day;
		}
		if(firstDay != undefined && firstDay != null ){
			day = firstDay;
		}
		$('#' + objId).val(day + '/' + month + '/' + year);
	},
	setCurrentMonthForCalendar: function(objId){
		var currentTime = new Date();
		var month = currentTime.getMonth() + 1;
		var year = currentTime.getFullYear();
		if(month < 10){
			month = '0' + month;
		}

		$('#' + objId).val(month + '/' + year);
	},
	getCurrentDateString: function(){
		var currentTime = new Date();
		var month = currentTime.getMonth() + 1;
		var day = currentTime.getDate();
		var year = currentTime.getFullYear();
		if(month < 10){
			month = '0' + month;
		}
		if(day < 10){
			day = '0' + day;
		}
		return day + '/' + month + '/' + year;
	},
	bindEnterOnWindow:function(){
		$(window).bind('keypress',function(event){
			if(event.keyCode == keyCodes.ENTER){
				/*var len = 0;//Đếm những dialog không ẩn
				$('.easyui-dialog').each(function(){
					if(!$(this).is(':hidden')){
						++len;
					}
				});
				if(!$('#searchStyle2EasyUIDialog').is(':hidden')){
					++len;
				}
				//Nếu có 1 cái hiện, tức có 1 cái không ẩn, tức có 1 popup search nhân viên thì không được click xuất báo cáo khi enter,
				//Vì enter đó là để search nhân viên, không phải để xuất báo cáo.
				//Khi nào tất cả popup đều ẩn, không có popup nào hiện cả, lúc đó mà bấm enter thì rõ ràng là enter đó để gọi lệnh click xuất báo cáo nè:
				if(len==0)
					$('#reportContainer .BtnGeneralStyle').click();*/
				if ($(".easyui-dialog:not(:hidden)").length == 0 && $('#searchStyle2EasyUIDialog:not(:hidden)').length == 0) {
					$('#reportContainer .BtnGeneralStyle').click();
				}
			}
			if(event.keyCode == keyCodes.ESCAPE){
				$.fancybox.close();
			}
		});
	},
	loadTreeStaff: function(){
		loadDataForStaffTree();
		$('#tree').bind("loaded.jstree", function(event, data){
			if($('#treeContainer').data("jsp")!= undefined){
				$('#treeContainer').data("jsp").destroy();
			}
			$('#treeContainer').jScrollPane();
			$('#tree li a').each(function(){
				if($(this).html().indexOf("(N/A)")==-1) 
					$(this).css('color','blue');
				});
		});
		$('#tree').bind("select_node.jstree", function (event, data) {			
			var id = data.rslt.obj.attr("id");
			SuperviseSales.moveToStaff(id);
			if($('#treeContainer').data("jsp")!= undefined){
				$('#treeContainer').data("jsp").destroy();
			}
			$('#treeContainer').jScrollPane();
	    });
		$('#tree').bind("open_node.jstree", function (event, data) {
				if($('#treeContainer').data("jsp")!= undefined){
					$('#treeContainer').data("jsp").destroy();
				}
				$('.loadingOverLayStaff').remove();
				setTimeout(function(){
					$('#treeContainer').jScrollPane();
					$('#tree li a').each(function(){
						if($(this).html().indexOf("(N/A)")==-1) 
							$(this).css('color','blue');
					});
				},500);
	    });
		$("#tree").bind("before.jstree", function (e, data) {
			if(data.func === "open_node") {
				$('.StaffSelectBtmSection').append("<p class='LoadingStyle loadingOverLayStaff'><img width='16' height='16' src='/resources/images/loading.gif'/>");
			}
			if(data.func === "correct_state"){
				$('.loadingOverLayStaff').remove();
			}
		});
	},
	loadComboTreeStockTrans: function(controlId, storeCode, staffCode, shopId, defaultValue)
	{		 
		var _url= '/rest/report/stocktrans/combotree/' + shopId + "/" + staffCode + '.json';
		
		$('#' + controlId).combotree({url: _url});
		var t = $('#'+ controlId).combotree('tree');	
		t.tree('options').url = '';
		t.tree({			
			onSelect:function(node){	
				$('#'+storeCode).val(node.id);
			},
			onBeforeExpand:function(result)
			{	            					
				var nodes = t.tree('getChildren',result.target);
				for(var i=0;i<nodes.length;++i){
					t.tree('remove',nodes[i].target);
				}
				
			},	 
			onExpand: function(result){	
				$('.panel-body-noheader ul').css('display','block');			
		}});
		if(defaultValue == undefined || defaultValue == null){
			$('#' + controlId).combotree('setValue',activeType.ALL);
		} else {
			$('#' + controlId).combotree('setValue',defaultValue);
		}
	},
	//CTTB 2013
	loadComboReportTreeEx: function(controlId, storeCode,defaultValue,callback){
		var _url='/rest/report/shop/combotree/1/0.json';	
		$('#' + controlId).combotree({url: _url,
			onChange: function(){
			}
			});
		var t = $('#'+ controlId).combotree('tree');	
		t.tree('options').url = '';
		t.tree({			
			onSelect:function(node){
				$.getJSON('/rest/catalog/shop/'+node.id+'/code.json', function(data){
					data.name = Utils.XSSEncode(data.name);
					if(callback!=undefined && callback!=null){
						callback.call(this, data.name);
					}
					$('#'+storeCode).val(Utils.XSSEncode(data.name));
				});
			},	  
			formatter:function(node){
				return Utils.XSSEncode(node.text);
			},
			onBeforeExpand:function(result){	            					
				var nodes = t.tree('getChildren',result.target);
				for(var i=0;i<nodes.length;++i){
					t.tree('remove',nodes[i].target);
				}
				$.ajax({
					type : "POST",
					url : '/rest/report/shop/combotree/2/'+ result.id +'.json',					
					dataType : "json",
					success : function(r) {
						$(r).each(function(i,e){
							e.text = Utils.XSSEncode(e.text);
						});
						t.tree('append',{
							parent:result.target,
							data:r
						});
						$('.panel-body-noheader ul').css('display','block');
					}
				});
			},
			onExpand: function(result){	
				$('.panel-body-noheader ul').css('display','block');			
		}});
		if(defaultValue == undefined || defaultValue == null){
			$('#' + controlId).combotree('setValue',activeType.ALL);
		} else {
			$('#' + controlId).combotree('setValue',defaultValue);
		}
	},
	
	
	/**
	 * Dialog tim kiem chon lua CTTB
	 * @author 
	 * @since January 17,2014
	 * */
	openDialogSearchCTTB : function(urlParameter) {
		var html = '<div id="common-dialog-search-2-textbox" class="easyui-dialog">';
		html += '<div class="PopupContentMid">';
		html += '<div class="GeneralForm Search1Form" >';
		html += '<label class="LabelStyle Label1Style" style=" width: 100px;">Mã CTTB</label>';
		html += '<input id="programCode" maxlength="50" type="text" style="width: 145px;" class="InputTextStyle InputText1Style"/>';
		html += '<label class="LabelStyle Label1Style" style=" width: 100px;">Tên CTTB</label>';
		html += '<input id="programName" maxlength="250" type="text" style="width: 145px;" class="InputTextStyle InputText1Style"/>';
		html += '<div class="Clear"></div>'; 
		html += '<div class="BtnCenterSection">';
		html += '<button class="BtnGeneralStyle BtnGeneralMStyle" tabindex="3" id="common-dialog-button-search">Tìm kiếm</button>';
		html += '</div>';
		html += '<div class="Clear"></div>';
		html += '<div class="GridSection" id="common-dialog-grid-container">';
		html += '<table id="common-dialog-grid-search"></table>';
		html += '</div>';
		html += '<div class="BtnCenterSection">';
		html += '<button id ="common-dialog-button-choose" class="BtnGeneralStyle BtnGeneralMStyle">Chọn</button>';
		html += '<button class="BtnGeneralStyle BtnGeneralMStyle" onclick="$(\'#common-dialog-search-2-textbox\').dialog(\'close\');">Đóng</button>';
		html += '</div>';
		html += '<p id="dialog-search-error" style="display: none;" class="ErrorMsgStyle"></p>';
		html += '</div>';
		html += '</div>';
		html += '</div>';
		//style="display: none;"
		$('body').append(html);
		
		$('#common-dialog-search-2-textbox').dialog({
			 title: 'CHỌN CHƯƠNG TRÌNH TRƯNG BÀY',
			 width: 580, 
			 height: 'auto',
			 closed: false,
			 cache: false,
			 modal: true,
			 onOpen: function() {
				 $('#common-dialog-search-2-textbox #common-dialog-button-search').unbind('click');
				 $('#common-dialog-search-2-textbox #common-dialog-button-search').bind('click', function() {
					 $('#common-dialog-search-2-textbox #dialog-search-error').html('').hide();
					 $('#common-dialog-grid-search').datagrid('reload');
				 });
				 $('#common-dialog-search-2-textbox input').unbind('keyup');
				 $('#common-dialog-search-2-textbox input').bind('keyup', function(e) {	
					 if(e.keyCode == keyCodes.ENTER) {
						 $('#common-dialog-search-2-textbox #common-dialog-button-search').click();
					 }
				 });
				 $('#common-dialog-search-2-textbox #common-dialog-button-choose').bind('click', function() {
					 if(ReportUtils.lstStaffId.size() == 0) {
						 $('#common-dialog-search-2-textbox #dialog-search-error').html('Bạn chưa chọn CTTB. Vui lòng chọn CTTB').show();
					 }else{
						 $('#common-dialog-search-2-textbox #dialog-search-error').html('').hide();
						 var listStaffStr = ReportUtils.lstStaffId.valArray[0].displayProgramCode;
						 for(var i = 1; i < ReportUtils.lstStaffId.valArray.length; i++) {
							 listStaffStr += ',' + ReportUtils.lstStaffId.valArray[i].displayProgramCode;
						 }
						 $('#multiChoiceCTTB').val(listStaffStr);
						 $('#common-dialog-search-2-textbox').dialog('close');
					 }
				 });
				 ReportUtils.lstStaffId = new Map();
				 $('#common-dialog-grid-search').datagrid({
					 url : urlParameter,
					 autoRowHeight : true,
					 rownumbers : true, 
					 pagination:true,
					 rowNum : 10,
					 pageSize:10,
					 scrollbarSize : 0,
					 pageNumber:1,
					 queryParams:{
						 page:1
					 },
					 onBeforeLoad: function(param) {
						 var fromDate = $('#fDate').val().trim();
						 var toDate = $('#tDate').val().trim();
						 var shopId = $('#shop').combotree('getValue');
						 var code = $('#common-dialog-search-2-textbox #programCode').val().trim();
						 var name = $('#common-dialog-search-2-textbox #programName').val().trim();
						 
						 param.code = code;
						 param.name = name;
						 param.fromDate = fromDate;
						 param.toDate = toDate;
						 param.shopId = shopId;
						 param = $.param(param, true);
					 },
					 onCheck : function (index,row) {
						 ReportUtils.lstStaffId.put(row.id, row);
					 },
					 onUncheck : function (index,row) { 
						 ReportUtils.lstStaffId.remove(row.id);
					 },
					 onCheckAll : function(rows) {
						 for(var i = 0; i < rows.length; i++) {
							 var row = rows[i];
							 ReportUtils.lstStaffId.put(row.id, row);
						 }
					 },
					 onUncheckAll : function(rows) {
						 for(var i = 0; i < rows.length; i++) {
							 var row = rows[i];
							 ReportUtils.lstStaffId.remove(row.id);
						 }
					 },
					 fitColumns:true,
					 width : ($('#common-dialog-search-2-textbox').width() - 40),
					 columns:[[
					 	{field: 'displayProgramCode', title: 'Mã CTTB', sortable: false, resizable: false, width: 60, align: 'left',
							formatter: function(val, row, idx) {
								if (val != undefined && val != null) {
									return Utils.XSSEncode(val);
								}
								return '';
							}
						},
						{field: 'displayProgramName', title: 'Tên CTTB', sortable: false, resizable: false, width: 130, align: 'left',
							formatter: function(val, row, idx) {
								if (val != undefined && val != null) {
									return Utils.XSSEncode(val);
								}
								return '';
							}
						},
						{field: 'times', title: 'Thời gian', sortable: false, resizable: false, width: 100, align: 'left',
							formatter: function(val, row, idx) {
								return GridFormatterUtils.dateFromdateTodate(row.fromDate, row.toDate);
							}
						},
						{field: 'checkerSelect', title: '<input id="checkSelectAll" type="checkbox" title="Chọn tất cả" onchange="ReportUtils.onchangeCheckerAll()" />',
							sortable: false, resizable: false, width: 20, align: 'center',
							formatter: function(val, row, idx) {
								var str='';
								if(ReportUtils.lstStaffId.get(row.id)!=null){
									str=' checked="checked" ';
								}
								return '<input type ="checkbox" '+str+' class="displayProgramCheckbox" value="'+row.id+'" id="check_'+row.id+'" onclick="ReportUtils.onCheckListCus('+row.id+');">';
							}
						}
					 ]],
					 onLoadSuccess :function(data){							 
						 if(($.isArray(data) && data.length != 0) || (data.rows != null && data.rows != undefined && $.isArray(data.rows) && data.rows.length != 0)) {
							 var __keys = (data.rows == null || data.rows == undefined) ? Object.keys(data[0]) : Object.keys(data.rows[0]);
							 var rows = (data.rows == null || data.rows == undefined) ? data : data.rows;
							 var isCheckAll=true;
							 for(var i = 0; i < rows.length; i++) {
								 if(ReportUtils.lstStaffId.get(rows[i].id)) {
									 $('#common-dialog-grid-search').datagrid('checkRow', i);
								 } else {
									 isCheckAll = false;
								 }
							 }
							 if(isCheckAll) {
								 $('#common-dialog-search-2-textbox .datagrid-header-row input[type=checkbox]').attr('checked', 'checked');
							 }
						 }
						 $('#common-dialog-search-2-textbox .datagrid-header-rownumber').html('STT');
						 var dHeight = $('#common-dialog-search-2-textbox').height();
						 var wHeight = $(window).height();
						 var top = (wHeight - dHeight) / 2 > 180 ? (wHeight - dHeight) / 2 - 180 : (wHeight - dHeight) / 2;
						 $('#common-dialog-search-2-textbox').dialog('move', {top : top});
					 }
				 });
			 },
			 onClose: function() {
				 ReportUtils.lstStaffId = null;
				 $('#common-dialog-search-2-textbox').remove(); 
			 }
		});
		
//		$('.ErrorMsgStyle').html("").show();
//		$('.SuccessMsgStyle').html("").show();
// 		$('#btnSearchStyleDisPlayPro').bind('keyup',function(event){
//			if(event.keyCode == 13){
//				//Goi ham reload grid
//			}
//		});
//		
//		$('#seachStyleDisPlayProCode').bind('keyup',function(event){
//			if(event.keyCode == 13){
//				//Goi ham reload grid
//			}
//		}).focus();
//		
//		$('#seachStyleDisPlayProName').bind('keyup',function(event){
//			if(event.keyCode == 13){
//				//Goi ham reload grid
//			}
//		});
//		if(ReportUtils._objectSearchCTTB!=null){
//			ReportUtils._objectSearchCTTB.name=encodeChar($('#seachStyleDisPlayProCode').val());
//			ReportUtils._objectSearchCTTB.code=encodeChar($('#seachStyleDisPlayProName').val());
//		}else{
//			ReportUtils._objectSearchCTTB = new Object();
//			ReportUtils._objectSearchCTTB.name=encodeChar($('#seachStyleDisPlayProCode').val());
//			ReportUtils._objectSearchCTTB.code=encodeChar($('#seachStyleDisPlayProName').val());
//		}
//		
// 		$('#searchStyleDisPlayProForTreeP').dialog({  
//	        title: "CHỌN CHƯƠNG TRÌNH TRƯNG BÀY",  
//	        closed: false,  
//	        cache: false,  
//	        modal: true,
//	        width : 616,
//	        height :'auto',
//	        onOpen: function(){
//				$('#seachStyleDisPlayProCode').focus();
//				$('#searchStyleDisPlayProForTreeP #common-dialog-button-search').unbind('click');
//				 $('#searchStyleDisPlayProForTreeP #common-dialog-button-search').bind('click', function() {
//					 $('#searchStyleDisPlayProForTreeP #dialog-search-error').html('').hide();
//					 $('#searchCTTBGrid').datagrid('reload');
//				 });
//				 $('#searchStyleDisPlayProForTreeP input').unbind('keyup');
//				 $('#searchStyleDisPlayProForTreeP input').bind('keyup', function(e) {	
//					 if(e.keyCode == keyCodes.ENTER) {
//						 $('#searchStyleDisPlayProForTreeP #btnSearchCTTB').click();
//					 }
//				 });
//				 $('#searchStyleDisPlayProForTreeP #btnSelectProgramDisplay').bind('click', function() {
//					 if(ReportUtils.lstStaffId.size() == 0) {
//						 $('#searchStyleDisPlayProForTreeP #errMsgDialogDisplayPro').html('Bạn chưa chọn CTTB. Vui lòng chọn CTTB').show();
//					 }else{
//						 $('#searchStyleDisPlayProForTreeP #errMsgDialogDisplayPro').html('').hide();
//						 var listStaffStr = ReportUtils.lstStaffId.valArray[0].displayProgramCode;
//						 for(var i = 1; i < ReportUtils.lstStaffId.valArray.length; i++) {
//							 listStaffStr += ',' + ReportUtils.lstStaffId.valArray[i].displayProgramCode;
//						 }
//						 $('#multiChoiceCTTB').val(listStaffStr);
//						 $('#searchStyleDisPlayProForTreeP').dialog('close');
//					 }
//				 });
//				 ReportUtils.lstStaffId = new Map();
//				$('#searchCTTBGrid').datagrid({
//					url: urlParameter,
//					queryParams:ReportUtils._objectSearchCTTB,
//					pageList: [10],
//					pageSize: 10,
//					height: 'auto',
//					scrollbarSize: 0,
//					pagination: true,
//					fitColumns: true,
//					singleSelect: true,
//					selectOnCheck: false,
//					checkOnSelect: false,
//					method: 'GET',
//					rownumbers: true,
//					width: $('#custGridCtn').width(),
//					columns: [[
//						{field: 'displayProgramCode', title: 'Mã CTTB', sortable: false, resizable: false, width: 60, align: 'left',
//							formatter: function(val, row, idx) {
//								if (val != undefined && val != null) {
//									return Utils.XSSEncode(val);
//								}
//								return '';
//							}
//						},
//						{field: 'displayProgramName', title: 'Tên CTTB', sortable: false, resizable: false, width: 150, align: 'left',
//							formatter: function(val, row, idx) {
//								if (val != undefined && val != null) {
//									return Utils.XSSEncode(val);
//								}
//								return '';
//							}
//						},
//						{field: 'times', title: 'Thời gian', sortable: false, resizable: false, width: 80, align: 'left',
//							formatter: function(val, row, idx) {
//								return GridFormatterUtils.dateFromdateTodate(row.fromDate, row.toDate);
//							}
//						},
//						{field: 'checkerSelect', title: '<input id="checkSelectAll" type="checkbox" title="Chọn tất cả" onchange="ReportUtils.onchangeCheckerAll()" />',
//							sortable: false, resizable: false, width: 20, align: 'center',
//							formatter: function(val, row, idx) {
//								return '<input type ="checkbox" class="displayProgramCheckbox" value="'+row.id+'" id="check_'+row.id+'" onclick="ReportUtils.onCheckListCus('+row.id+');">';
//							}
//						}
//					]],
//					onBeforeLoad: function(params) {
//						
//					},
//					onCheck : function (index,row) {
//						 ReportUtils.lstStaffId.put(row.id, row);
//					 },
//					 onUncheck : function (index,row) { 
//						 ReportUtils.lstStaffId.remove(row.id);
//					 },
//					 onCheckAll : function(rows) {
//						 for(var i = 0; i < rows.length; i++) {
//							 var row = rows[i];
//							 ReportUtils.lstStaffId.put(row.id, row);
//						 }
//					 },
//					 onUncheckAll : function(rows) {
//						 for(var i = 0; i < rows.length; i++) {
//							 var row = rows[i];
//							 ReportUtils.lstStaffId.remove(row.id);
//						 }
//					 },
//					onLoadSuccess : function(data) {
//						$('.datagrid-header-rownumber').html('STT');
//						updateRownumWidthForJqGrid('#orderGrid1');
//						$('.datagrid-body-inner td').css('vertical-align', 'middle');
//						if($.isArray(data.rows)) {
//							var i =0, j = 0;
//							if(ReportUtils._isCheckAll==true){
//								$('input.displayProgramCheckbox').attr('checked', 'checked');
//								if(ReportUtils._lstUnchecker!=undefined && ReportUtils._lstUnchecker!=null && ReportUtils._lstUnchecker.length>0){
//									flagFor = false;
//									for(i=0; i< ReportUtils._lstUnchecker.length; ++i){
//										for(j = 0; j < data.rows.length; j++) {
//											if(ReportUtils._lstUnchecker[i]==data.rows[j].id){
//						    					$('input#check_'+data.rows[j].id.toString().trim()).removeAttr('checked');
//						    				}
//										}
//									}
//								}
//							}else{
//								$('input.displayProgramCheckbox').removeAttr('checked');
//								if(ReportUtils._lstChecker!=undefined && ReportUtils._lstChecker!=null && ReportUtils._lstChecker.length>0){
//									for(i=0; i< ReportUtils._lstChecker.length; ++i){
//										for(j = 0; j < data.rows.length; j++) {
//											if(ReportUtils._lstChecker[i]==data.rows[j].id){
//						    					$('input#check_'+data.rows[j].id.toString().trim()).attr('checked', 'checked');
//						    				}
//										}
//									}
//								}
//							}
//						
//						}
//					}
//				});
//	        },
//	        buttons:'#bottomDisplayProDialog',
//	        onBeforeClose: function() {
//	        },
//	        onClose : function(){
//	        	$('#searchStyleDisPlayProContainerGrid').html('<table id="searchStyleDisPlayProGrid" class="easyui-treegrid"></table>').show();
//	        	$('#seachStyleDisPlayProCode').val("").show();
//	        	$('#seachStyleDisPlayProName').val("").show();
//	        }
//	    });
//		return false;
	},
	/**
	 * search Display Program
	 * @author hunglm16
	 * @since January 17,2014
	 * */
	searchMultiDisplayPro: function(){
		$('.ErrorMsgStyle').html("").show();
		$('.SuccessMsgStyle').html("").show();
		var params = {};
		params.shopId = $('#shop').combotree('getValue');
		params.year = encodeChar($('#fullYearIn10').val());
		params.code=encodeChar($('#seachStyleDisPlayProCode').val());
		params.name=encodeChar($('#seachStyleDisPlayProName').val());
		$('#searchCTTBGrid').datagrid('reload', params);
	},
	/**
	 * Su kien dong Dialog Tim kiem va chon lua CTTB
	 * @author hunglm16
	 * @since January 17,2014
	 * */
	onCloseDialogSearchMultiDisplayPro: function(){
		$('.ErrorMsgStyle').html("").show();
		$('.SuccessMsgStyle').html("").show();
		ReportUtils._lstChecker = new Array();
		ReportUtils._lstUnchecker = new Array();
		ReportUtils._isCheckAll = false;
		$('#seachStyleDisPlayProCode').val("").show();
    	$('#seachStyleDisPlayProName').val("").show();
    	$('#searchStyleDisPlayProDL').dialog('close');
	},
	/**
	 * Su kien chon cac CTTB
	 * @author hunglm16
	 * @since January 17,2014
	 * */
	multiChoiceDisplayPro: function(){
		var msg = "";
		if(!ReportUtils._isCheckAll && ReportUtils._lstChecker.length==0 && ReportUtils._lstUnchecker==0){
			msg = "Chưa chọn CTTB";
			$('#errMsgDialogDisplayPro').html(msg).show();
			return false;
		}
		if(msg.length==0){
			var arrIdStr = "-1";
			var i = 0;
			var params = {};
			if(ReportUtils._isCheckAll){
				params.flagCheckAll = true;
				for(i =0; i<ReportUtils._lstUnchecker.length; ++i){
					arrIdStr = arrIdStr.trim()+"," + ReportUtils._lstUnchecker[i].toString();
				}
				arrIdStr = arrIdStr.trim().replace(/-1,/g, "");
				arrIdStr = arrIdStr.trim().replace(/-1/g, "");
				params.lstIdStr = arrIdStr.trim();
			}else{
				params.flagCheckAll = false;
				for(i =0; i<ReportUtils._lstChecker.length; ++i){
					arrIdStr = arrIdStr.trim()+"," + ReportUtils._lstChecker[i].toString();
				}
				arrIdStr = arrIdStr.trim().replace(/-1,/g, "");
				arrIdStr = arrIdStr.trim().replace(/-1/g, "");
				params.lstIdStr = arrIdStr.trim();
			}
			params.year = $('#fullYearIn10').val();
			params.code = $('#seachStyleDisPlayProCode').val();
			params.name = $('#seachStyleDisPlayProName').val();
			params.shopId = $('#shop').combotree('getValue');
			var url = '/programme-display/lst-Display-Program-By-ListId';
			Utils.getJSONDataByAjaxNotOverlay(params,url,function(data){
				if(data.lstData!=undefined && data.lstData!=null && data.lstData.length>0){
					ReportUtils._arrMultiChoiceDisplay = new Array();
					var strTemp = "-1";
					for(var j=0; j<data.lstData.length; ++j){
						//Danh sach nhung CTTB duoc chon
						ReportUtils._arrMultiChoiceDisplay.push(data.lstData[j].id);
						strTemp = strTemp.trim() + "," + data.lstData[j].displayProgramCode;
					}
					strTemp = strTemp.trim().replace(/-1,/g, "");
					strTemp = strTemp.trim().replace(/-1/g, "");
					$('#multiChoiceCTTB').val(strTemp.trim());
				}
			});
		}
		ReportUtils.onCloseDialogSearchMultiDisplayPro();
	},
	
	onchangeCheckerAll: function() {
		$('.ErrorMsgStyle').html("").show();
		$('.SuccessMsgStyle').html("").show();
		//Bat su kien check, uncheck tat ca
		var flag = $('#checkSelectAll').is(':checked');
		if(flag!=false){
			$('input.displayProgramCheckbox').attr('checked', 'checked');
			ReportUtils._lstChecker = new Array();
			ReportUtils._lstUnchecker = new Array();
			ReportUtils._isCheckAll = true;
		}else{
			$('input.displayProgramCheckbox').removeAttr('checked');
			ReportUtils._lstChecker = new Array();
			ReportUtils._lstUnchecker = new Array();
			ReportUtils._isCheckAll = false;
		}
	},
	onCheckListCus:function(id){
		$('.ErrorMsgStyle').html("").show();
		$('.SuccessMsgStyle').html("").show();
		var flag = $('#check_'+id.toString()).is(':checked');
		var flagCheckAll = $('#checkSelectAll').is(':checked');
		var flagFor = false;
		//Bat su kien check, uncheck trong danh sach khach hang
		if(flag ==false && flagCheckAll==true){
			$('#checkSelectAll').removeAttr('checked');
			ReportUtils._lstUnchecker.push(id);
		}
		if(flagCheckAll!=true && ReportUtils._isCheckAll == true){
			if(ReportUtils._lstUnchecker!=undefined && ReportUtils._lstUnchecker!=null && ReportUtils._lstUnchecker.length>0){
				flagFor = false;
				for(var i=0; i< ReportUtils._lstUnchecker.length; ++i){
					if(ReportUtils._lstUnchecker[i]==id){
						flagFor = true;
						break;
					}
				}
				if(!flagFor){
					ReportUtils._lstUnchecker.push(id);
				}else{
					ReportUtils.removeArrayByValue(ReportUtils._lstUnchecker, id);
				}
			}else{
				ReportUtils._lstUnchecker = new Array();
				ReportUtils._lstUnchecker.push(id);
			}
		}
		if(flagCheckAll!=true && ReportUtils._isCheckAll != true){
			if(ReportUtils._lstChecker!=undefined && ReportUtils._lstChecker!=null && ReportUtils._lstChecker.length>0){
				flagFor = false;
				for(var i=0; i< ReportUtils._lstChecker.length; ++i){
					if(ReportUtils._lstChecker[i]==id){
						flagFor = true;
						break;
					}
				}
				if(!flagFor){
					ReportUtils._lstChecker.push(id);
				}else{
					ReportUtils.removeArrayByValue(ReportUtils._lstChecker, id);
				}
			}else{
				ReportUtils._lstChecker = new Array();
				ReportUtils._lstChecker.push(id);
			}
		}
	},
	removeArrayByValue:function(arr,val){
		//HungLm16 - Delete Item in Array
		for(var i=0; i<arr.length; i++) {
	        if(arr[i] == val){
	        	arr.splice(i, 1);
		        break;
	        }
	    }
	},
	loadKendoTreeMultipleChoice: function(controlId, defaultShopCodeValue,defaultIdValue){
		var flagFirstLoad = true;
		var valueInTree = null;
		var lstDataMultiselect =null;
		$('#' + controlId).kendoMultiSelect({
	        dataTextField: "shopCode",
	        dataValueField: "id",
			itemTemplate: function(data, e, s, h, q) {
				if(data.type.channelTypeCode == 'VNM') {//VNM
					return '<div class="tree-vnm" node-id="'+Utils.XSSEncode(data.id)+'" style="width:230px"><span class="tree-vnm-node"><div style="width: 15px; display: inline-block;"></div></span><span class="tree-vnm-text">'+Utils.XSSEncode(data.shopCode) + ' - ' + Utils.XSSEncode(data.shopName)+'</span></div>';
					}else if(data.type.channelTypeCode == 'MIEN'){
					return '<div class="tree-mien" node-id="'+Utils.XSSEncode(data.id)+'" style="width:220px;"><span class="tree-mien-node"><div style="width: 15px; display: inline-block;"></div></span><span class="tree-mien-text">'+Utils.XSSEncode(data.shopCode) + ' - ' + Utils.XSSEncode(data.shopName)+'</span></div>';
				}else if(data.type.channelTypeCode == 'VUNG'){
					return '<div class="tree-vung" node-id="'+Utils.XSSEncode(data.id)+'" style="width:200px"><span class="tree-vung-node"><div style="width: 15px; display: inline-block;"></div></span><span class="tree-vung-text">'+Utils.XSSEncode(data.shopCode) + ' - ' + Utils.XSSEncode(data.shopName)+'</span></div>';
				}else if(data.type.channelTypeCode == 'NPP'){
					return '<div class="tree-npp" node-id="'+Utils.XSSEncode(data.id)+'" style="display:-moz-grid-group"><span class="tree-npp-node"><div style="width: 15px; display: inline-block;"></div></span><span class="tree-npp-text">'+Utils.XSSEncode(data.shopCode) + '-' + Utils.XSSEncode(data.shopName)+'</span></div>';
				}
			},
	        tagTemplate:  '#: data.shopCode #',			  
	        dataSource: {
	            transport: {
	                read: {
	                    dataType: "json",
	                    url: "/rest/report/shop/kendoui-combobox-ho.json"
	                }
	            }
	        },
	        change: function(e) {
	        	var lstValueTmp = this.value();
	        	var lstValue = new Array();
	        	var lstDeleted = new Array();
	        	//xoa trong datasource cua cay
	        	var multiselect = $('#' + controlId).data("kendoMultiSelect");
	        	var lstDataMultiselectTmp = new Array();
	        	var lstNodeToBeDeleted = new Array();
	        	$(valueInTree._data).each(function(i,e){
	        		lstDataMultiselectTmp.push(e);
	        	});
	        	
	        	lstDataMultiselect = new kendo.data.DataSource({
					  data: lstDataMultiselectTmp,
					  schema: {
					    model: { 
					    	id: "id"
					    		,
					    	children: 'items'
					   	}
					  }
					});
	        	lstDataMultiselect.read();
	        	$(lstValue).each(function(i4,e4){
	        		var obje4 = valueInTree.get(e4);
	        		$(obje4.items).each(function(i3,e3){
	        			$(e3.items).each(function(i2,e2){
	        				$(e2.items).each(function(i1,e1){
	        					lstNodeToBeDeleted.push(e1.id);
	    	        		});
	        				lstNodeToBeDeleted.push(e2.id);
		        		});
	        			lstNodeToBeDeleted.push(e3.id);
	        		});
	        	});
	        	
	        	$(lstNodeToBeDeleted).each(function(i,e){
	        		if(lstDataMultiselect.get(e) != null){
	        			lstDataMultiselect.remove(lstDataMultiselect.get(e));
	        		}
	        	});
	        	
	        	multiselect.setDataSource(lstDataMultiselect);
	        	multiselect.value(lstValue);
	        },
		    value: [{ shopCode: $('#'+defaultShopCodeValue).val(), id: $('#'+defaultIdValue).val()}],
		    dataBound: function(e){
		    	if(!flagFirstLoad)
		    		return;
		    	var dataMultiselect = $('#' + controlId).data("kendoMultiSelect");
		    	$(dataMultiselect.dataSource._data).each(function(i,e){
		    		if(e.parentShop != null){
		    			var obj = dataMultiselect.dataSource.get(e.parentShop.id);
		    			if(obj != null){
		    				if(obj.items == null|| obj.items == undefined){
		    					obj.items = new Array();
		    					obj.items.push(e);
		    				}else{
		    					obj.items.push(e);
		    				}
		    				
		    			}
		    		}
		    	});
		    	
				valueInTree = new kendo.data.DataSource({
					  data: dataMultiselect.dataSource._data,
					  schema: {
					    model: { 
					    	id: "id"
					    		,
					    	children: 'items'
					   	}
					  }
					});
				valueInTree.read();
		    }
		});
		if(defaultIdValue != null && defaultIdValue != undefined){
			var multiselect = $('#'+controlId).data("kendoMultiSelect");
			multiselect.value(defaultIdValue);
		}
		flagFirstLoad = false;
	},
	/**
	 * tao duong dan den file bao cao
	 * @author tuannd20
	 * @return String Duong dan toi file bao cao
	 * @since  13/04/2015
	 */
	buildReportFilePath: function(reportFilePath) {
		var filePath = '';
		try {
			var reportToken = $('#report_token').val();
			filePath = reportFilePath + '?v=' + reportToken;
		} catch (e) {
			// Oop! Error
		}
		return filePath;
	},
};
/*
 * END OF FILE - /web/web/resources/scripts/business/shop-report/vnm.report-utils.js
 */

/*
 * START OF FILE - /web/web/resources/scripts/business/shop-report/vnm.daily-follow-report.js
 */
var DailyFollowReport = {
	
	reportDeliveryTicketAdd: function(){
		var msg = '';
		$('#errMsg').html('').hide();
//		if($('#shopCode').val().trim().length == 0 || $('#shopCode').val().trim() == activeType.ALL){
//			msg = format(msgErr_required_field,'đơn vị');
//		}
		if(msg.length == 0){
			msg = Utils.getMessageOfInvalidFormatDate('fDate','Từ ngày');
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfInvalidFormatDate('tDate','Đến ngày');
		}
		var fDate = $('#fDate').val();
		var tDate = $('#tDate').val();
		if(!Utils.compareDate(fDate, tDate)){
			msg = msgErr_fromdate_greater_todate;		
			$('#fDate').focus();
		}
		if(msg.length > 0){
			$('#errMsg').html(msg).show();
			return false;
		}
		var dataModel = new Object();		
		dataModel.shopCode = $('#shopCode').val().trim();
		dataModel.fromDate = $('#fDate').val().trim();
		dataModel.toDate = $('#tDate').val().trim();
		dataModel.staffCode = $('#staffCode').val().trim();
		ReportUtils.exportReport('/report/daily-follow/delivey-ticket-pay-sum/export', dataModel);
	},
	//7.1.2
	exportBKDHNVGH: function(){
		var msg = '';
		$('#errMsg').html('').hide();
		if($('#shopId').val().trim().length == 0 || $('#shopId').val().trim() == activeType.ALL){
			msg = format(msgErr_required_field,'Đơn vị');
		}
		if(msg.length == 0){
			if($('#isNPP').val() == "false"){
				msg = "Đơn vị bạn vừa chọn không phải là 1 nhà phân phối cụ thể!";
			}
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfRequireCheck('fDate','Từ ngày');
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfInvalidFormatDateNew('fDate','Từ ngày');
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfRequireCheck('tDate','Đến ngày');
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfInvalidFormatDateNew('tDate','Đến ngày');
		}
		var fDate = $('#fDate').val();
		var tDate = $('#tDate').val();
		if(!Utils.compareDateNew(fDate, tDate)){
			msg = msgErr_fromdate_greater_todate;		
			$('#fDate').focus();
		}
		var currentTime = new Date();
		var month = currentTime.getMonth() + 1;
		var day = currentTime.getDate();
		var year = currentTime.getFullYear();
		var hour = currentTime.getHours();
		var minute = currentTime.getMinutes();
		if(month < 10){
			month = '0' + month;
		}
		if(day < 10){
			day = '0' + day;
		}
		if(hour < 10){
			hour = '0' + hour;
		}
		if(minute < 10){
			minute = '0' + minute;
		}
		if(!Utils.compareDateNew(fDate,day + '/' + month + '/' + year + ' ' + hour + ':' + minute)){
			msg = 'Từ ngày không được lớn hơn ngày hiện tại';
			$('#fDate').focus();
		}
		if($('#isPrint').val().trim() == 1){
			/*if(msg.length == 0){
				msg = Utils.getMessageOfRequireCheck('fromHour','Từ giờ');
			}
			
			if(msg.length == 0){
				msg = Utils.getMessageOfRequireCheck('toHour','Đến giờ');
			}
			
			if(msg.length == 0){
				msg = Utils.getMessageOfSpecialCharactersValidateEx('fromHour','Từ giờ',Utils._TF_NUMBER);
			}

			if(msg.length == 0){
				msg = Utils.getMessageOfSpecialCharactersValidateEx('toHour','Đến giờ',Utils._TF_NUMBER);
			}
			
			if ($('#fromHour').val()!=null){
				if (parseInt($('#fromHour').val())< 0 || parseInt($('#fromHour').val()) > 23)
				{
					$('#errMsg').html('Giá trị giờ nằm trong khoảng [0-23]. Mời bạn nhập lại.').show();
					$('#fromHour').focus();
					return false;
				}
			}
			
			if ($('#toHour').val()!=null){
				if (parseInt($('#toHour').val())< 0 || parseInt($('#toHour').val()) > 23)
				{
					$('#errMsg').html('Giá trị giờ nằm trong khoảng [0-23]. Mời bạn nhập lại.').show();
					$('#toHour').focus();
					return false;
				}
			}*/
		}
		if(msg.length > 0){
			$('#errMsg').html(msg).show();
			return false;
		}
		var dataModel = new Object();		
		if($('input[name=formatType]:radio:checked').length > 0 && $('input[name=formatType]:radio:checked').val() != undefined) {
            dataModel.formatType = $('input[name=formatType]:radio:checked').val().toUpperCase();
		}
		dataModel.staffCode = $('#staffCode').val().trim();
		dataModel.staffSaleCode = $('#staffSaleCode').val().trim();
		dataModel.isPrint = $('#isPrint').val().trim();
		dataModel.shopId = $('#shopId').val().trim();
		dataModel.fromDate = $('#fDate').val().trim();
		dataModel.toDate = $('#tDate').val().trim();
		//dataModel.fromHour = $('#fromHour').val().trim();
		//dataModel.toHour = $('#toHour').val().trim();
		dataModel.saleOrderNumber = $('#saleOrderNumber').val().trim();
		ReportUtils.exportReport('/report/daily-follow/bkdhnvgh/export',dataModel);
	},
	//7.1.2a
	exportBKDHNVGH_A: function(){
		var msg = '';
		$('#errMsg').html('').hide();
		if($('#shopId').val().trim().length == 0 || $('#shopId').val().trim() == activeType.ALL){
			msg = format(msgErr_required_field,'Đơn vị');
		}
		if(msg.length == 0){
			if($('#isNPP').val() == "false"){
				msg = "Đơn vị bạn vừa chọn không phải là 1 nhà phân phối cụ thể!";
			}
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfRequireCheck('fDate','Từ ngày');
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfInvalidFormatDate('fDate','Từ ngày');
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfRequireCheck('tDate','Đến ngày');
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfInvalidFormatDate('tDate','Đến ngày');
		}
		var fDate = $('#fDate').val();
		var tDate = $('#tDate').val();
		if(!Utils.compareDate(fDate, tDate)){
			msg = msgErr_fromdate_greater_todate;		
			$('#fDate').focus();
		}
		var currentTime = new Date();
		var month = currentTime.getMonth() + 1;
		var day = currentTime.getDate();
		var year = currentTime.getFullYear();
		if(!Utils.compareDate(fDate,day + '/' + month + '/' + year)){
			msg = 'Từ ngày không được lớn hơn ngày hiện tại';
			$('#fDate').focus();
		}
		if(msg.length > 0){
			$('#errMsg').html(msg).show();
			return false;
		}
		var dataModel = new Object();		
		if($('input[name=formatType]:radio:checked').length > 0 && $('input[name=formatType]:radio:checked').val() != undefined) {
            dataModel.formatType = $('input[name=formatType]:radio:checked').val().toUpperCase();
		}
		dataModel.staffCode = $('#staffCode').val().trim();
		dataModel.staffSaleCode = $('#staffSaleCode').val().trim();
		dataModel.shopId = $('#shopId').val().trim();
		dataModel.fromDate = $('#fDate').val().trim();
		dataModel.toDate = $('#tDate').val().trim();
		dataModel.saleOrderNumber = $('#saleOrderNumber').val().trim();
		dataModel.notApproved = '1';
		ReportUtils.exportReport('/report/daily-follow/bkdhnvgh/export', dataModel);
	},
	exportPGNTTG: function(){
		var msg = '';
		$('#errMsg').html('').hide();
		if($('#shopId').val().trim().length == 0 || $('#shopId').val().trim() == activeType.ALL){
			msg = format(msgErr_required_field,'đơn vị');
		}
		if(msg.length == 0){
			if($('#isNPP').val() == "false"){
				msg = "Đơn vị bạn vừa chọn không phải là 1 nhà phân phối cụ thể!";
			}
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfRequireCheck('fDate','Từ ngày');
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfInvalidFormatDate('fDate','Từ ngày');
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfRequireCheck('tDate','Đến ngày');
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfInvalidFormatDate('tDate','Đến ngày');
		}
		var fDate = $('#fDate').val();
		var tDate = $('#tDate').val();
		if(!Utils.compareDate(fDate, tDate)){
			msg = msgErr_fromdate_greater_todate;		
			$('#fDate').focus();
		}
		var currentTime = new Date();
		var month = currentTime.getMonth() + 1;
		var day = currentTime.getDate();
		var year = currentTime.getFullYear();
		if(!Utils.compareDate(fDate,day + '/' + month + '/' + year)){
			msg = 'Từ ngày không được lớn hơn ngày hiện tại';
			$('#fDate').focus();
		}
		
		if(msg.length > 0){
			$('#errMsg').html(msg).show();
			return false;
		}
		var dataModel = new Object();		
		dataModel.staffCode = $('#staffCode').val().trim();
		dataModel.shopId = $('#shopId').val().trim();
		dataModel.fromDate = $('#fDate').val().trim();
		dataModel.toDate = $('#tDate').val().trim();
		if($('input[name=formatType]:radio:checked').length > 0 && $('input[name=formatType]:radio:checked').val() != undefined) {
            dataModel.formatType = $('input[name=formatType]:radio:checked').val().toUpperCase();
		}
		ReportUtils.exportReport('/report/daily-follow/pgnttg/export',dataModel);
	},
	//7.1.1
	exportPXHTNVGH : function(){
		var msg = '';
		$('#errMsg').html('').hide();
		if ($('#shopId').val().trim().length == 0 || $('#shopId').val().trim() == activeType.ALL) {
			msg = format(msgErr_required_field,'đơn vị');
		}
		if (msg.length == 0) {
			if ($('#isNPP').val() == "false") {
				msg = "Đơn vị bạn vừa chọn không phải là 1 nhà phân phối cụ thể!";
			}
		}
		if (msg.length == 0) {
			msg = Utils.getMessageOfRequireCheck('fDate','Từ ngày');
		}
		if (msg.length == 0) {
			msg = Utils.getMessageOfInvalidFormatDateNew('fDate','Từ ngày');
		}
		if (msg.length == 0) {
			msg = Utils.getMessageOfRequireCheck('tDate','Đến ngày');
		}
		if (msg.length == 0) {
			msg = Utils.getMessageOfInvalidFormatDateNew('tDate','Đến ngày');
		}
		var fDate = $('#fDate').val();
		var tDate = $('#tDate').val();
		if (!Utils.compareDateNew(fDate, tDate)) {
			msg = msgErr_fromdate_greater_todate;		
			$('#fDate').focus();
		}
		
		var currentTime = new Date();
		var month = currentTime.getMonth() + 1;
		var day = currentTime.getDate();
		var year = currentTime.getFullYear();
		var hour = currentTime.getHours();
		var minute = currentTime.getMinutes();
		if(month < 10){
			month = '0' + month;
		}
		if(day < 10){
			day = '0' + day;
		}
		if(hour < 10){
			hour = '0' + hour;
		}
		if(minute < 10){
			minute = '0' + minute;
		}
		if(!Utils.compareDateNew(fDate,day + '/' + month + '/' + year + ' ' + hour + ':' + minute)){
			msg = 'Từ ngày không được lớn hơn ngày hiện tại';
			$('#fDate').focus();
		}
		
		if ($('#isPrint').val().trim() == 1) {
			/*if (msg.length == 0) {
				msg = Utils.getMessageOfRequireCheck('fromHour','Từ giờ');
			}
			
			if (msg.length == 0) {
				msg = Utils.getMessageOfRequireCheck('toHour','Đến giờ');
			}
			
			if(msg.length == 0) {
				msg = Utils.getMessageOfSpecialCharactersValidateEx('fromHour','Từ giờ',Utils._TF_NUMBER);
			}

			if(msg.length == 0) {
				msg = Utils.getMessageOfSpecialCharactersValidateEx('toHour','Đến giờ',Utils._TF_NUMBER);
			}
			
			if ($('#fromHour').val()!=null) {
				if (parseInt($('#fromHour').val())< 0 || parseInt($('#fromHour').val()) > 23) {
					$('#errMsg').html('Giá trị giờ nằm trong khoảng [0-23]. Mời bạn nhập lại.').show();
					$('#fromHour').focus();
					return false;
				}
			}
			
			if ($('#toHour').val()!=null) {
				if (parseInt($('#toHour').val())< 0 || parseInt($('#toHour').val()) > 23) {
					$('#errMsg').html('Giá trị giờ nằm trong khoảng [0-23]. Mời bạn nhập lại.').show();
					$('#toHour').focus();
					return false;
				}
			}*/
		}
		if (msg.length > 0) {
			$('#errMsg').html(msg).show();
			return false;
		}
		var saleOrderNumber = $('#saleOrderNumber').val().trim();
		if (saleOrderNumber.length>0) {
			saleOrderNumber = saleOrderNumber.replace(/;/g, ",");
		}
		
		var dataModel = new Object();		
		dataModel.shopId = $('#shopId').val().trim();
		dataModel.staffSaleCode = $('#staffSaleCode').val().trim();
		dataModel.isPrint = $('#isPrint').val().trim();
		dataModel.fromDate = $('#fDate').val().trim();
		dataModel.deliveryCode = $('#staffCode').val().trim();
		dataModel.toDate = $('#tDate').val().trim();
		//dataModel.fromHour = $('#fromHour').val().trim();
		//dataModel.toHour = $('#toHour').val().trim();
		dataModel.lstSaleOderNumberStr = saleOrderNumber;
		if ($('input[name=formatType]:radio:checked').length > 0 && $('input[name=formatType]:radio:checked').val() != undefined) {
            dataModel.formatType = $('input[name=formatType]:radio:checked').val().toUpperCase();
		}
		ReportUtils.exportReport('/report/daily-follow/pxhtnvgh/export', dataModel, 'errMsg');
	},
	//7.1.1a
	exportPXHTNVGH_A: function(){		
		var msg = '';
		$('#errMsg').html('').hide();
		if($('#shopId').val().trim().length == 0 || $('#shopId').val().trim() == activeType.ALL){
			msg = format(msgErr_required_field,'đơn vị');
		}
		if(msg.length == 0){
			if($('#isNPP').val() == "false"){
				msg = "Đơn vị bạn vừa chọn không phải là 1 nhà phân phối cụ thể!";
			}
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfRequireCheck('fDate','Từ ngày');
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfInvalidFormatDate('fDate','Từ ngày');
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfRequireCheck('tDate','Đến ngày');
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfInvalidFormatDate('tDate','Đến ngày');
		}
		var fDate = $('#fDate').val();
		var tDate = $('#tDate').val();
		if(!Utils.compareDate(fDate, tDate)){
			msg = msgErr_fromdate_greater_todate;		
			$('#fDate').focus();
		}
		
		var currentTime = new Date();
		var month = currentTime.getMonth() + 1;
		var day = currentTime.getDate();
		var year = currentTime.getFullYear();
		if(!Utils.compareDate(fDate,day + '/' + month + '/' + year)){
			msg = 'Từ ngày không được lớn hơn ngày hiện tại';
			$('#fDate').focus();
		}
		
		if(msg.length > 0){
			$('#errMsg').html(msg).show();
			return false;
		}
		var saleOrderNumber = $('#saleOrderNumber').val().trim();
		if(saleOrderNumber.length>0){
			saleOrderNumber = saleOrderNumber.replace(/;/g, ",");
		}
		
		var dataModel = new Object();		
		dataModel.shopId = $('#shopId').val().trim();
		dataModel.staffSaleCode = $('#staffSaleCode').val().trim();
		dataModel.fromDate = $('#fDate').val().trim();
		dataModel.deliveryCode = $('#staffCode').val().trim();
		dataModel.toDate = $('#tDate').val().trim();
		dataModel.lstSaleOderNumberStr = saleOrderNumber;
		if($('input[name=formatType]:radio:checked').length > 0 && $('input[name=formatType]:radio:checked').val() != undefined) {
            dataModel.formatType = $('input[name=formatType]:radio:checked').val().toUpperCase();
		}
		dataModel.notApproved = '1';
		ReportUtils.exportReport('/report/daily-follow/pxhtnvgh/export',dataModel);
	},
	/**
	 * @author sangtn
	 * @since 23/05/2014
	 * @description Phiếu xuất hàng theo nhân viên bán hàng 7.1.7 
	 * */
	exportPXHTNVBH : function(){
		var msg = '';
		$('#errMsg').html('').hide();
		if($('#shopId').val().trim().length == 0 || $('#shopId').val().trim() == activeType.ALL){
			msg = format(msgErr_required_field,'đơn vị');
		}
		if(msg.length == 0){
			if($('#isNPP').val() == "false"){
				msg = "Đơn vị bạn vừa chọn không phải là 1 nhà phân phối cụ thể!";
			}
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfRequireCheck('fDate','Từ ngày');
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfInvalidFormatDate('fDate','Từ ngày');
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfRequireCheck('tDate','Đến ngày');
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfInvalidFormatDate('tDate','Đến ngày');
		}
		var fDate = $('#fDate').val();
		var tDate = $('#tDate').val();
		if(!Utils.compareDate(fDate, tDate)){
			msg = msgErr_fromdate_greater_todate;		
			$('#fDate').focus();
		}
		
		var currentTime = new Date();
		var month = currentTime.getMonth() + 1;
		var day = currentTime.getDate();
		var year = currentTime.getFullYear();
		if(!Utils.compareDate(fDate,day + '/' + month + '/' + year)){
			msg = 'Từ ngày không được lớn hơn ngày hiện tại';
			$('#fDate').focus();
		}
		
		if(msg.length > 0){
			$('#errMsg').html(msg).show();
			return false;
		}
		var saleOrderNumber = $('#saleOrderNumber').val().trim();
		if(saleOrderNumber.length>0){
			saleOrderNumber = saleOrderNumber.replace(/;/g, ",");
		}
		var dataModel = new Object();		
		dataModel.shopId = $('#shopId').val().trim();
		dataModel.staffSaleCode = $('#staffSaleCode').val().trim();
		dataModel.fromDate = $('#fDate').val().trim();
		dataModel.deliveryCode = $('#staffCode').val().trim();
		dataModel.toDate = $('#tDate').val().trim();
		dataModel.lstSaleOderNumberStr = saleOrderNumber;
		if($('input[name=formatType]:radio:checked').length > 0 && $('input[name=formatType]:radio:checked').val() != undefined) {
            dataModel.formatType = $('input[name=formatType]:radio:checked').val().toUpperCase();
		}

		ReportUtils.exportReport('/report/daily-follow/pxhtnvbh/export',dataModel);
	},
	/* PHIEU TRA HANG THEO NVGH */
	reportPTHNVGH : function(){
		var msg = '';
		$('#errMsg').html('').hide();
		if($('#shopId').val().trim().length == 0 || $('#shopId').val().trim() == activeType.ALL){
			msg = format(msgErr_required_field,'đơn vị');
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfSpecialCharactersValidate('deliveryCode','Mã nhân viên giao hàng',Utils._CODE);
		}

		if(msg.length == 0){
			if($('#isNPP').val() == "false"){
				msg = "Đơn vị bạn vừa chọn không phải là 1 nhà phân phối cụ thể!";
			}
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfRequireCheck('fDate','Từ ngày');
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfInvalidFormatDate('fDate','Từ ngày');
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfRequireCheck('tDate','Đến ngày');
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfInvalidFormatDate('tDate','Đến ngày');
		}
		var fDate = $('#fDate').val();
		var tDate = $('#tDate').val();
		if(!Utils.compareDate(fDate, tDate)){
			msg = msgErr_fromdate_greater_todate;		
			$('#fDate').focus();
		}
		
		var currentTime = new Date();
		var month = currentTime.getMonth() + 1;
		var day = currentTime.getDate();
		var year = currentTime.getFullYear();
		if(!Utils.compareDate(fDate,day + '/' + month + '/' + year)){
			msg = 'Từ ngày không được lớn hơn ngày hiện tại';
			$('#fDate').focus();
		}
		
		if(msg.length > 0){
			$('#errMsg').html(msg).show();
			return false;
		}
		var dataModel = new Object();		
		dataModel.shopId = $('#shopId').val().trim();
		dataModel.deliveryCode = $('#deliveryCode').val().trim();
		dataModel.fromDate = $('#fDate').val().trim();
		dataModel.toDate = $('#tDate').val().trim();
		if($('input[name=formatType]:radio:checked').length > 0 && $('input[name=formatType]:radio:checked').val() != undefined) {
            dataModel.formatType = $('input[name=formatType]:radio:checked').val().toUpperCase();
		}

		ReportUtils.exportReport('/report/daily-follow/pthnvgh/export',dataModel);
	},
	reportBKPTHNVGH : function(){
		var msg = '';
		$('#errMsg').html('').hide();
		if($('#shopId').val().trim().length == 0 || $('#shopId').val().trim() == activeType.ALL){
			msg = format(msgErr_required_field,'đơn vị');
		}
		if(msg.length == 0) {
			if($('#isNPP').val() == "false") {
				msg = "Đơn vị bạn vừa chọn không phải là 1 nhà phân phối cụ thể!";
			}
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfInvalidFormatDate('fDate','Từ ngày');
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfInvalidFormatDate('tDate','Đến ngày');
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfEmptyDate('fDate','Từ ngày');
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfEmptyDate('tDate','Đến ngày');
		}
		if(msg.length > 0){
			$('#errMsg').html(msg).show();
			return false;
		}
		var fDate = $('#fDate').val();
		var tDate = $('#tDate').val();
		var cDate = ReportUtils.getCurrentDateString();
		if(!Utils.compareDate(fDate, cDate)){
			msg = msgErr_fromdate_greater_currentdate;		
			$('#fDate').focus();
		}
		if(!Utils.compareDate(fDate, tDate)){
			msg = msgErr_fromdate_greater_todate;		
			$('#fDate').focus();
		}
		if(msg.length > 0){
			$('#errMsg').html(msg).show();
			return false;
		}
		var dataModel = new Object();		
		if($('input[name=formatType]:radio:checked').length > 0 && $('input[name=formatType]:radio:checked').val() != undefined) {
            dataModel.formatType = $('input[name=formatType]:radio:checked').val().toUpperCase();
		}
		dataModel.shopId = $('#shopId').val().trim();
		dataModel.fromDate = $('#fDate').val().trim();
		dataModel.toDate = $('#tDate').val().trim();
		dataModel.staffCode = $('#staffCode').val().trim();
		//ReportUtils.exportReportProcessNoData('/report/daily-follow/bkpthnvgh/export',dataModel);
		ReportUtils.exportReport('/report/daily-follow/bkpthnvgh/export',dataModel);
	},
	reportPDTH : function(){
		var msg = '';
		$('#errMsg').html('').hide();
		var shopId = $("#shop").combotree("getValue");
		//if($('#shopCode').val().trim().length == 0 || $('#shopCode').val().trim() == activeType.ALL){
		if (!shopId && shopId.length == 0) {
			msg = format(msgErr_required_field,'đơn vị');
		}
		if(msg.length == 0){
			if($('#isNPP').val() == "false"){
				msg = "Đơn vị bạn vừa chọn không phải là 1 nhà phân phối cụ thể!";
			}
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfRequireCheck('lstSaleOderNumber','Đơn hàng');
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfRequireCheck('fDate','Từ ngày');
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfInvalidFormatDate('fDate','Từ ngày');
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfRequireCheck('tDate','Đến ngày');
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfInvalidFormatDate('tDate','Đến ngày');
		}
		var fDate = $('#fDate').val();
		var tDate = $('#tDate').val();
		var curDate = $('#curDate').val();
		if(!Utils.compareDate(fDate, tDate)){
			msg = msgErr_fromdate_greater_todate;		
			$('#fDate').focus();
		}
		if(!Utils.compareDate(fDate, curDate)){
			msg = "Từ ngày không được lớn hơn ngày hiện tại";		
			$('#fDate').focus();
		}
		if(msg.length > 0){
			$('#errMsg').html(msg).show();
			return false;
		}
		var lstSaleOderNumber = $('#lstSaleOderNumber').val().trim();
		if(lstSaleOderNumber.length >0){
			lstSaleOderNumber = lstSaleOderNumber.replace(/;/g, ',');
		}
		var dataModel = new Object();		
		dataModel.shopId = $('#shop').combotree("getValue");
		dataModel.fromDate = $('#fDate').val().trim();
		dataModel.toDate = $('#tDate').val().trim();
		dataModel.lstSaleOderNumberStr = lstSaleOderNumber;
		if($('input[name=formatType]:radio:checked').length > 0 && $('input[name=formatType]:radio:checked').val() != undefined) {
            dataModel.formatType = $('input[name=formatType]:radio:checked').val().toUpperCase();
		}

		ReportUtils.exportReport('/report/daily-follow/pdth/export',dataModel);
	},
	reportPDHCG:function(){ //vuonghn
		var msg = '';
		$('#errMsg').html('').hide();
		if($('#shopCode').val().trim().length == 0 || $('#shopCode').val().trim() == activeType.ALL){
			msg = format(msgErr_required_field,'đơn vị');
		}
		if(msg.length == 0){
			if($('#isNPP').val() == "false"){
				msg = "Đơn vị bạn vừa chọn không phải là 1 nhà phân phối cụ thể!";
			}
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfRequireCheck('fDate','Từ ngày');
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfInvalidFormatDate('fDate','Từ ngày');
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfRequireCheck('tDate','Đến ngày');
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfInvalidFormatDate('tDate','Đến ngày');
		}
		if(msg.length == 0 && $('#staffCode').val().trim()!='' && $('#staffCode').val().trim().length>50){
			msg = "Mã NVGH không được vượt quá 50 ký tự.";
		}
		var fDate = $('#fDate').val();
		var tDate = $('#tDate').val();
		var curDate = $('#curDate').val();
		if(!Utils.compareDate(fDate, tDate)){
			msg = msgErr_fromdate_greater_todate;		
			$('#fDate').focus();
		}
		if(!Utils.compareDate(fDate, curDate)){
			msg = "Từ ngày không được lớn hơn ngày hiện tại";		
			$('#fDate').focus();
		}
		if(msg.length > 0){
			$('#errMsg').html(msg).show();
			return false;
		}
		var dataModel = new Object();		
		dataModel.shopId = $('#shopCode').val().trim();
		dataModel.fromDate = $('#fDate').val().trim();
		dataModel.toDate = $('#tDate').val().trim();
		dataModel.deliveryCode = $('#staffCode').val().trim();
		if($('input[name=formatType]:radio:checked').length > 0 && $('input[name=formatType]:radio:checked').val() != undefined) {
            dataModel.formatType = $('input[name=formatType]:radio:checked').val().toUpperCase();
		}
		var kData = $.param(dataModel,true);
		$('#divOverlay').show();		
		$.ajax({
			type : "POST",
			url : "/report/daily-follow/pdhcg/export",
			data :(kData),
			dataType: "json",
			success : function(data) {
				$('#divOverlay').hide();				
				if(!data.error) {
					if(data.hasData != undefined && data.hasData != null && data.hasData == false) {
						$('#errMsg').html('Không có dữ liệu để xuất báo cáo').show();
					} else {
						window.location.href = data.path;
						setTimeout(function(){ //Set timeout để đảm bảo file load lên hoàn tất
		                    CommonSearch.deleteFileExcelExport(data.path);
						},2000);
					}
				} else {
					$('#errMsg').html(data.errMsg).show();					
				}
			},
			error:function(XMLHttpRequest, textStatus, errorDivThrown) {
				$('#divOverlay').hide();
				StockIssued.xhrExport = null;
			}
		});
		return false;
	},
	/**
	 * 7.1.8 Bao cao Phieu Thu Tra Cua Khach Hang
	 * 
	 * @author hunglm16
	 * @author October 08,2014
	 * @description Update Phan Quyen CMS
	 * */
	exportPTHCKH: function(){
		var msg = '';
		$('#errMsg').html('').hide();
//		if($('#shopId').val().trim().length == 0 || $('#shopId').val().trim() == activeType.ALL){
//			msg = format(msgErr_required_field,'đơn vị');
//		}
		if(msg.length == 0){
			msg = Utils.getMessageOfRequireCheck('fDate','Từ ngày');
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfInvalidFormatDate('fDate','Từ ngày');
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfRequireCheck('tDate','Từ ngày');
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfInvalidFormatDate('tDate','Đến ngày');
		}
		var fDate = $('#fDate').val();
		var tDate = $('#tDate').val();
		if(!Utils.compareDate(fDate, tDate)){
			msg = msgErr_fromdate_greater_todate;		
			$('#fDate').focus();
		}
		var currentTime = new Date();
		var month = currentTime.getMonth() + 1;
		var day = currentTime.getDate();
		var year = currentTime.getFullYear();
		if(!Utils.compareDate(fDate,day + '/' + month + '/' + year)){
			msg = 'Từ ngày không được lớn hơn ngày hiện tại';
			$('#fDate').focus();
		}
		
		if(msg.length > 0){
			$('#errMsg').html(msg).show();
			return false;
		}
		var lstStaffCode = $('#staffCode').val().trim();
		if(lstStaffCode.length >0){
			lstStaffCode = lstStaffCode.replace(/;/g, ',');
		}
		
		var dataModel = new Object();		
		dataModel.customerCode = $('#customerCode').val().trim();
		dataModel.shopId = $('#shopId').val().trim();
		dataModel.fromDate = $('#fDate').val().trim();
		dataModel.toDate = $('#tDate').val().trim();
		dataModel.lstStaffCodeStr = lstStaffCode;
		if($('input[name=formatType]:radio:checked').length > 0 && $('input[name=formatType]:radio:checked').val() != undefined) {
            dataModel.formatType = $('input[name=formatType]:radio:checked').val().toUpperCase();
       }
		ReportUtils.exportReport('/report/daily-follow/pthckh/export',dataModel);
	},
	
	exportVNM10_1_17: function() {
		var msg = '';
		$('#errMsg').hide();
		msg = Utils.getMessageOfRequireCheck('shopId', 'Đơn vị');
		$('#shop').focus();
		if (msg.length == 0) {
			msg = Utils.getMessageOfRequireCheck('promotionCode', 'Mã CT');
		}
		if (msg.length == 0) {
			msg = Utils.getMessageOfRequireCheck('categoryCode', 'Ngành hàng');
		}
		
		if (msg.length == 0) {
			msg = Utils.getMessageOfRequireCheck('fDate', 'Từ ngày');
		}
		if (msg.length == 0) {
			msg = Utils.getMessageOfRequireCheck('tDate', 'Đến ngày');
		}
		if (msg.length == 0) {
			msg = Utils.getMessageOfInvalidFormatDate('fDate', 'Từ ngày');
		}
		if (msg.length == 0) {
			msg = Utils.getMessageOfInvalidFormatDate('tDate', 'Từ ngày');
		}
		var fDate = $('#fDate').val().trim();
		var tDate = $('#tDate').val().trim();
		if (msg.length == 0 && !Utils.compareCurrentDate(fDate)) {
			msg = 'Từ ngày phải nhỏ hơn hoặc bằng ngày hiện tại';
			$('#fDate').focus();
		}

		if (msg.length == 0 && !Utils.compareDate(fDate, tDate)) {
			msg = 'Từ ngày phải nhỏ hơn hoặc bằng đến ngày';
			$('#fDate').focus();
		}

		if (msg.length > 0) {
			$('#errMsg').html(msg).show();
			return false;
		}

		var dataModel = new Object();
		dataModel.shopId = $('#shopId').val().trim();
		dataModel.fromDate = fDate;
		dataModel.toDate = tDate;
		dataModel.programCode = $('#promotionCode').val().replace(/ /g, '');
		dataModel.categoryCode = $('#categoryCode').val().replace(/ /g, '');
//		dataModel.formatType = 'XLS';//$('input[type=radio]:checked').val();
		if($('input[name=formatType]:radio:checked').length > 0 && $('input[name=formatType]:radio:checked').val() != undefined) {
            dataModel.formatType = $('input[name=formatType]:radio:checked').val().toUpperCase();
		}
		ReportUtils.exportReport('/report/daily-follow/bcVnm-thdhhh/export', dataModel);
		return false;
	},
	onSelectIsPrintChange:function(){
		var isPrint = $('#isPrint').val();
		/*if(isPrint != 1){
			$('#fromHour').attr('disabled', 'disabled');
			$('#toHour').attr('disabled', 'disabled');
		}else{
			$('#fromHour').removeAttr('disabled');
			$('#toHour').removeAttr('disabled');
		}*/
	}
	
};
/*
 * END OF FILE - /web/web/resources/scripts/business/shop-report/vnm.daily-follow-report.js
 */

/*
 * START OF FILE - /web/web/resources/scripts/business/shop-report/vnm.po-report.js
 */
var PoReport = {	
	reportCustomerService: function(){
		var msg = '';
		$('#errMsg').html('').hide();
		if($('#shopCode').val().trim().length == 0 || $('#shopCode').val().trim() == activeType.ALL){
			msg = format(msgErr_required_field,'đơn vị');
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfInvalidFormatDate('fDate','Từ ngày');
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfInvalidFormatDate('tDate','Đến ngày');
		}
		var fDate = $('#fDate').val();
		var tDate = $('#tDate').val();
		if(!Utils.compareDate(fDate, tDate)){
			msg = msgErr_fromdate_greater_todate;		
			$('#fDate').focus();
		}
		if(msg.length > 0){
			$('#errMsg').html(msg).show();
			return false;
		}
		var dataModel = new Object();		
		dataModel.shopCode = $('#shopCode').val().trim();
		dataModel.fromDate = $('#fDate').val().trim();
		dataModel.toDate = $('#tDate').val().trim();
		ReportUtils.exportReport('/report/po/manage-customer-service/export',dataModel);
	},
	exportDCPOCTDVKH_5_5 : function(){
		var msg = '';
		$('#errMsg').html('').hide();
		if($('#shopId').val().trim().length == 0 || $('#shopId').val().trim() == activeType.ALL){
			msg = format(msgErr_required_field,'đơn vị');
		}
		if(msg.length == 0){
			if($('#isNPP').val() == "false"){
				msg = "Đơn vị bạn vừa chọn không phải là 1 nhà phân phối cụ thể!";
			}
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfInvalidFormatDate('fDate','Từ ngày');
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfInvalidFormatDate('tDate','Đến ngày');
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfEmptyDate('fDate','Từ ngày');
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfEmptyDate('tDate','Đến ngày');
		}
		if(msg.length > 0){
			$('#errMsg').html(msg).show();
			return false;
		}
		var fDate = $('#fDate').val();
		var tDate = $('#tDate').val();
		var cDate = ReportUtils.getCurrentDateString();
		if(!Utils.compareDate(fDate, cDate)){
			msg = msgErr_fromdate_greater_currentdate;		
			$('#fDate').focus();
		}
		if(!Utils.compareDate(fDate, tDate)){
			msg = msgErr_fromdate_greater_todate;		
			$('#fDate').focus();
		}
		if(msg.length > 0){
			$('#errMsg').html(msg).show();
			return false;
		}
		var dataModel = new Object();		
		dataModel.shopId = $('#shopId').val().trim();
		dataModel.fromDate = $('#fDate').val().trim();
		dataModel.toDate = $('#tDate').val().trim();
		ReportUtils.exportReport('/report/po/dcpoctdvkh/export',dataModel);
	},
	reportBTDTTDH: function() {
		var msg = '';
		$('#errMsg').html('').hide();
		if($('#shopId').val().trim().length == 0 || $('#shopId').val().trim() == activeType.ALL){
			msg = format(msgErr_required_field,'đơn vị');
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfInvalidFormatDate('fDate','Từ ngày');
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfInvalidFormatDate('tDate','Đến ngày');
		}
		var fDate = $('#fDate').val();
		var tDate = $('#tDate').val();
		if(msg.length == 0 && !Utils.compareCurrentDate(fDate)) {
			msg = 'Từ ngày phải nhỏ hơn hoặc bằng ngày hiện tại';
		}
		
		if(msg.length == 0 && !Utils.compareDate(fDate, tDate)) {
			msg = 'Từ ngày phải nhỏ hơn hoặc bằng đến ngày';
		}
		
		if(msg.length == 0 && !Utils.compareDate(fDate, tDate)){
			msg = msgErr_fromdate_greater_todate;		
			$('#fDate').focus();
		}
		if(msg.length > 0){
			$('#errMsg').html(msg).show();
			return false;
		}
		var dataModel = new Object();		
		dataModel.shopId = $('#shopId').val().trim();
		dataModel.fromDate = $('#fDate').val().trim();
		dataModel.toDate = $('#tDate').val().trim();
		ReportUtils.exportReport('/report/po/btdttdh/export',dataModel);
	},
	exportBKCTCTMH: function() {
		var msg = '';
		$('#errMsg').html('').hide();
		if($('#shopId').val().trim().length == 0 || $('#shopId').val().trim() == activeType.ALL){
			msg = format(msgErr_required_field,'đơn vị');
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfInvalidFormatDate('fDate','Từ ngày');
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfInvalidFormatDate('tDate','Đến ngày');
		}
		var fDate = $('#fDate').val();
		var tDate = $('#tDate').val();
		if(msg.length == 0 && !Utils.compareCurrentDate(fDate)) {
			msg = 'Từ ngày phải nhỏ hơn hoặc bằng ngày hiện tại';
		}
		
		if(msg.length == 0 && !Utils.compareDate(fDate, tDate)) {
			msg = 'Từ ngày phải nhỏ hơn hoặc bằng đến ngày';
		}
		
		if(msg.length == 0 && !Utils.compareDate(fDate, tDate)){
			msg = msgErr_fromdate_greater_todate;		
			$('#fDate').focus();
		}
		if(msg.length > 0){
			$('#errMsg').html(msg).show();
			return false;
		}
		var dataModel = new Object();		
		if($('input[name=formatType]:radio:checked').length > 0 && $('input[name=formatType]:radio:checked').val() != undefined) {
            dataModel.formatType = $('input[name=formatType]:radio:checked').val().toUpperCase();
		}
		if ($('input[name=pageFormat]:radio:checked').length > 0 && $('input[name=pageFormat]:radio:checked').val() != undefined) {
			dataModel.pageFormat = $('input[name=pageFormat]:radio:checked').val().toUpperCase();
		}
		dataModel.shopId = $('#shopId').val().trim();
		dataModel.fromDate = $('#fDate').val().trim();
		dataModel.toDate = $('#tDate').val().trim();
		dataModel.type = $('#type').val().trim();
		
		ReportUtils.exportReport('/report/po/bkctctmh/export',dataModel);
		/*var kData = $.param(dataModel,true);
		$('#divOverlay').show();		
		$.ajax({
			type : "POST",
			url : "/report/po/bkctctmh/export",
			data :(kData),
			dataType: "json",
			success : function(data) {
				$('#divOverlay').hide();				
				if(!data.error) {
					if(data.hasData != null && data.hasData != undefined && data.hasData == true) {
						window.location.href = data.path;
						setTimeout(function(){ //Set timeout để đảm bảo file load lên hoàn tất
		                    CommonSearch.deleteFileExcelExport(data.path);
						},2000);
					} else {
						$('#errMsg').html('Không có dữ liệu để xuất').show();
					}
				} else {
					$('#errMsg').html(data.errMsg).show();					
				}
			},
			error:function(XMLHttpRequest, textStatus, errorDivThrown) {
				$('#divOverlay').hide();
				StockIssued.xhrExport = null;
			}
		});
		return false;*/
	},
	//namlb bao cao PO Auto NPP
	exportPOAutoNPP: function() {
		var msg = '';
		$('#errMsg').html('').hide();
		if($('#shopId').val().trim().length == 0 || $('#shopId').val().trim() == activeType.ALL){
			msg = format(msgErr_required_field,'đơn vị');
		}
		if(msg.length == 0){
			if($('#isNPP').val() == "false"){
				msg = "Đơn vị bạn vừa chọn không phải là 1 nhà phân phối cụ thể!";
			}
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfInvalidFormatDate('fDate','Từ ngày');
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfInvalidFormatDate('tDate','Đến ngày');
		}
		var fDate = $('#fDate').val();
		var tDate = $('#tDate').val();
		if(msg.length == 0 && !Utils.compareCurrentDate(fDate)) {
			msg = 'Từ ngày phải nhỏ hơn hoặc bằng ngày hiện tại';
		}
		
		if(msg.length == 0 && !Utils.compareDate(fDate, tDate)) {
			msg = 'Từ ngày phải nhỏ hơn hoặc bằng đến ngày';
		}
		
		if(msg.length == 0 && !Utils.compareDate(fDate, tDate)){
			msg = msgErr_fromdate_greater_todate;		
			$('#fDate').focus();
		}
		if(msg.length > 0){
			$('#errMsg').html(msg).show();
			return false;
		}
		var dataModel = new Object();		
		dataModel.shopId = $('#shopId').val().trim();
		dataModel.fromDate = $('#fDate').val().trim();
		dataModel.toDate = $('#tDate').val().trim();
		//ReportUtils.exportReport('/report/po/bkctctmh/export',dataModel);
		var kData = $.param(dataModel,true);
		$('#divOverlay').show();		
		$.ajax({
			type : "POST",
			url : "/report/po/bcpoautoNPP/export",
			data :(kData),
			dataType: "json",
			success : function(data) {
				$('#divOverlay').hide();				
				if(!data.error) {
					if(data.hasData != null && data.hasData != undefined && data.hasData == true) {
						window.location.href = data.path;
						setTimeout(function(){ //Set timeout để đảm bảo file load lên hoàn tất
		                    CommonSearch.deleteFileExcelExport(data.path);
						},2000);
					} else {
						$('#errMsg').html('Không có dữ liệu để xuất').show();
					}
				} else {
					$('#errMsg').html(data.errMsg).show();					
				}
			},
			error:function(XMLHttpRequest, textStatus, errorDivThrown) {
				$('#divOverlay').hide();
				StockIssued.xhrExport = null;
			}
		});
		return false;
	},
	//phuongvm bao cao phieu dat hang vinamilk 5.1
	exportPDHVNM_5_1: function() {
		var msg = '';
		$('#errMsg').html('').hide();
		if($('#shopId').val().trim().length == 0 || $('#shopId').val().trim() == activeType.ALL){
			msg = format(msgErr_required_field,'đơn vị');
		}
		if(msg.length == 0){
			if($('#isNPP').val() == "false"){
				msg = "Đơn vị bạn vừa chọn không phải là 1 nhà phân phối cụ thể!";
			}
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfInvalidFormatDate('fDate','Từ ngày');
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfInvalidFormatDate('tDate','Đến ngày');
		}
		var fDate = $('#fDate').val();
		var tDate = $('#tDate').val();
		if(msg.length == 0 && !Utils.compareCurrentDate(fDate)) {
			msg = 'Từ ngày phải nhỏ hơn hoặc bằng ngày hiện tại';
		}
		
		if(msg.length == 0 && !Utils.compareDate(fDate, tDate)) {
			msg = 'Từ ngày phải nhỏ hơn hoặc bằng đến ngày';
		}
		
		if(msg.length == 0 && !Utils.compareDate(fDate, tDate)){
			msg = msgErr_fromdate_greater_todate;		
			$('#fDate').focus();
		}
		if(msg.length > 0){
			$('#errMsg').html(msg).show();
			return false;
		}
		var dataModel = new Object();		
		dataModel.shopId = $('#shopId').val().trim();
		dataModel.fromDate = $('#fDate').val().trim();
		dataModel.toDate = $('#tDate').val().trim();
		if($('input[name=formatType]:radio:checked').length > 0 && $('input[name=formatType]:radio:checked').val() != undefined) {
            dataModel.formatType = $('input[name=formatType]:radio:checked').val().toUpperCase();
		}

		ReportUtils.exportReport('/report/po/pdhvnm/export',dataModel);
	},
	//vuonghn xuat ban ke chung tu hang hoa mua vao
	exportBKCTHHMV: function() {
		var msg = '';
		$('#errMsg').html('').hide();
		if($('#shopId').val().trim().length == 0 || $('#shopId').val().trim() == activeType.ALL){
			msg = format(msgErr_required_field,'đơn vị');
		}
		if(msg.length == 0){
			if($('#isNPP').val() == "false"){
				msg = "Đơn vị bạn vừa chọn không phải là 1 nhà phân phối cụ thể!";
			}
		}
		if (msg.length == 0) {
			msg = Utils.getMessageOfRequireCheck('fDate', 'Từ ngày');
		}
		if (msg.length == 0) {
			msg = Utils.getMessageOfRequireCheck('tDate', 'Đến ngày');
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfInvalidFormatDate('fDate','Từ ngày');
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfInvalidFormatDate('tDate','Đến ngày');
		}
		var fDate = $('#fDate').val();
		var tDate = $('#tDate').val();
		if(msg.length == 0 && !Utils.compareCurrentDate(fDate)) {
			msg = 'Từ ngày phải nhỏ hơn hoặc bằng ngày hiện tại';
		}
		
		if(msg.length == 0 && !Utils.compareDate(fDate, tDate)) {
			msg = 'Từ ngày phải nhỏ hơn hoặc bằng đến ngày';
		}
		
		if(msg.length == 0 && !Utils.compareDate(fDate, tDate)){
			msg = msgErr_fromdate_greater_todate;		
			$('#fDate').focus();
		}
		if(msg.length > 0){
			$('#errMsg').html(msg).show();
			return false;
		}
		var dataModel = new Object();		
		if($('input[name=formatType]:radio:checked').length > 0 && $('input[name=formatType]:radio:checked').val() != undefined) {
            dataModel.formatType = $('input[name=formatType]:radio:checked').val().toUpperCase();
		}
		dataModel.shopId = $('#shopId').val().trim();
		dataModel.fromDate = $('#fDate').val().trim();
		dataModel.toDate = $('#tDate').val().trim();
		ReportUtils.exportReport('/report/po/bkcthhmv/export',dataModel);
		/*var kData = $.param(dataModel,true);
		$('#divOverlay').show();		
		$.ajax({
			type : "POST",
			url : "/report/po/bkcthhmv/export",
			data :(kData),
			dataType: "json",
			success : function(data) {
				$('#divOverlay').hide();				
				if(!data.error) {
					if(data.hasData != null && data.hasData != undefined && data.hasData == true) {
						window.location.href = data.path;
						setTimeout(function(){ //Set timeout để đảm bảo file load lên hoàn tất
		                    CommonSearch.deleteFileExcelExport(data.path);
						},2000);
					} else {
						$('#errMsg').html('Không có dữ liệu để xuất').show();
					}
				} else {
					$('#errMsg').html(data.errMsg).show();					
				}
			},
			error:function(XMLHttpRequest, textStatus, errorDivThrown) {
				$('#divOverlay').hide();
				StockIssued.xhrExport = null;
			}
		});*/
		return false;
	},
	
	exportTDMH9: function() {
		var msg = '';
		$('#errMsg').hide();
		msg = Utils.getMessageOfRequireCheck('shopId', 'Đơn vị');
		$('#shop').focus();
		
		if (msg.length == 0) {
			msg = Utils.getMessageOfRequireCheck('fDate', 'Từ ngày');
		}
		if (msg.length == 0) {
			msg = Utils.getMessageOfRequireCheck('tDate', 'Đến ngày');
		}
		if (msg.length == 0) {
			msg = Utils.getMessageOfInvalidFormatDate('fDate', 'Từ ngày');
		}
		if (msg.length == 0) {
			msg = Utils.getMessageOfInvalidFormatDate('tDate', 'Từ ngày');
		}
		var fDate = $('#fDate').val().trim();
		var tDate = $('#tDate').val().trim();
		if(msg.length == 0 && !Utils.compareCurrentDate(fDate)) {
			msg = 'Từ ngày phải nhỏ hơn hoặc bằng ngày hiện tại';
			$('#fDate').focus();
		}
		
		if(msg.length == 0 && !Utils.compareDate(fDate, tDate)) {
			msg = 'Từ ngày phải nhỏ hơn hoặc bằng đến ngày';
			$('#fDate').focus();
		}
		
		if (msg.length > 0) {
			$('#errMsg').html(msg).show();
			return false;
		}
		
		var dataModel = new Object();
		dataModel.shopId = $('#shopId').val().trim();
		dataModel.fromDate = fDate;
		dataModel.toDate = tDate;
		if($('input[name=formatType]:radio:checked').length > 0 && $('input[name=formatType]:radio:checked').val() != undefined) {
			dataModel.formatType = $('input[name=formatType]:radio:checked').val().toUpperCase();
		}
		ReportUtils.exportReport('/report/po/tdmh_7_3_9/export',dataModel);
		return false;
	}
};
//bang ke mua hang theo mat hang
var productReport = {	
		productBuyReport: function(){
		var msg = '';
		$('#errMsg').html('').hide();
		if($('#shopCode').val().trim().length == 0 || $('#shopCode').val().trim() == activeType.ALL){
			msg = format(msgErr_required_field,'đơn vị');
		}
		if(msg.length == 0){
			if($('#isNPP').val() == "false"){
				msg = "Đơn vị bạn vừa chọn không phải là 1 nhà phân phối cụ thể!";
			}
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfInvalidFormatDate('fDate','Từ ngày');
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfInvalidFormatDate('tDate','Đến ngày');
		}
		var fDate = $('#fDate').val();
		var tDate = $('#tDate').val();
		if(!Utils.compareDate(fDate, tDate)){
			msg = msgErr_fromdate_greater_todate;		
			$('#fDate').focus();
		}
		if(msg.length > 0){
			$('#errMsg').html(msg).show();
			return false;
		}
		var dataModel = new Object();		
		dataModel.shopCode = $('#shopCode').val().trim();
		dataModel.fromDate = $('#fDate').val().trim();
		dataModel.toDate = $('#tDate').val().trim();
		ReportUtils.exportReport('/report/productbuy/product-buy/export',dataModel);
	}
};

/*
 * END OF FILE - /web/web/resources/scripts/business/shop-report/vnm.po-report.js
 */

/*
 * START OF FILE - /web/web/resources/scripts/business/shop-report/vnm.deliveryTicketdaily.js
 */
var dailyTicket = {
		/**
		 * 7.1.3 Bao cao Hoa don giao hang va thanh toan
		 * 
		 *  @author hunglm16
		 *  @since October 08,2014
		 *  @description Phan quyen CMS
		 * */
		reportDeliveryTicketAdd: function(){
			var msg = '';
			$('#errMsg').html('').hide();
			if($('#shopId').val().trim().length == 0 || $('#shopId').val().trim() == activeType.ALL){
				msg = format(msgErr_required_field,'đơn vị');
			}
			if(msg.length == 0){
				if($('#isNPP').val() == "false"){
					msg = "Đơn vị bạn vừa chọn không phải là 1 nhà phân phối cụ thể!";
					
				}
			}
			var chot = '';
			if (msg == ""){
				msg = Utils.getMessageOfRequireCheck('fDate', 'Từ ngày', false);
				chot = 'fDate';
			}
			if (msg == ""){
				msg = Utils.getMessageOfRequireCheck('tDate', 'Đến ngày', false);
				chot = 'tDate';
			}
			if($('#isPrint').val().trim() == 1){
				/*if(msg.length == 0){
					msg = Utils.getMessageOfRequireCheck('fromHour','Từ giờ');
				}
				
				if(msg.length == 0){
					msg = Utils.getMessageOfRequireCheck('toHour','Đến giờ');
				}
				
				if(msg.length == 0){
					msg = Utils.getMessageOfSpecialCharactersValidateEx('fromHour','Từ giờ',Utils._TF_NUMBER);
				}

				if(msg.length == 0){
					msg = Utils.getMessageOfSpecialCharactersValidateEx('toHour','Đến giờ',Utils._TF_NUMBER);
				}
				
				if ($('#fromHour').val()!=null){
					if (parseInt($('#fromHour').val())< 0 || parseInt($('#fromHour').val()) > 23)
					{
						$('#errMsg').html('Giá trị giờ nằm trong khoảng [0-23]. Mời bạn nhập lại.').show();
						$('#fromHour').focus();
						return false;
					}
				}
				
				if ($('#toHour').val()!=null){
					if (parseInt($('#toHour').val())< 0 || parseInt($('#toHour').val()) > 23)
					{
						$('#errMsg').html('Giá trị giờ nằm trong khoảng [0-23]. Mời bạn nhập lại.').show();
						$('#toHour').focus();
						return false;
					}
				}*/
			}
			
			if(msg.length > 0){
				$('#errMsg').html(msg).show();
				$('#'+chot).focus();
				return false;
			}
			if(msg.length == 0){
				msg = Utils.getMessageOfInvalidFormatDateNew('fDate','Từ ngày');
			}
			if(msg.length == 0){
				msg = Utils.getMessageOfInvalidFormatDateNew('tDate','Đến ngày');
			}
			var fDate = $('#fDate').val();
			var tDate = $('#tDate').val();
			if(!Utils.compareDate(fDate, tDate)){
				msg = msgErr_fromdate_greater_todate;		
				$('#fDate').focus();
			}
			if(msg.length > 0){
				$('#errMsg').html(msg).show();
				return false;
			}
			var dataModel = new Object();		
			dataModel.shopId = $('#shop').combotree('getValue');
			dataModel.fromDate = $('#fDate').val().trim();
			dataModel.toDate = $('#tDate').val().trim();
			//dataModel.fromHour = $('#fromHour').val().trim();
			//dataModel.toHour = $('#toHour').val().trim();
			dataModel.staffCode = $('#staffCode').val().trim();
			dataModel.isPrint = $('#isPrint').val();
			if($('input[name=formatType]:radio:checked').length > 0 && $('input[name=formatType]:radio:checked').val() != undefined) {
				dataModel.formatType = $('input[name=formatType]:radio:checked').val().toUpperCase();
			}
			ReportUtils.exportReport('/report/daily-follow/pgntn_view',dataModel);
		},
		//7.1.3a
		reportDeliveryTicketAdd_A: function(){
			var msg = '';
			$('#errMsg').html('').hide();
//			if($('#shopId').val().trim().length == 0 || $('#shopId').val().trim() == activeType.ALL){
//				msg = format(msgErr_required_field,'đơn vị');
//			}
//			if(msg.length == 0){
//				if($('#isNPP').val() == "false"){
//					msg = "Đơn vị bạn vừa chọn không phải là 1 nhà phân phối cụ thể!";
//					
//				}
//			}
			var chot = '';
			if (msg == ""){
				msg = Utils.getMessageOfRequireCheck('fDate', 'Từ ngày', false);
				chot = 'fDate';
			}
			if (msg == ""){
				msg = Utils.getMessageOfRequireCheck('tDate', 'Đến ngày', false);
				chot = 'tDate';
			}
			if(msg.length > 0){
				$('#errMsg').html(msg).show();
				$('#'+chot).focus();
				return false;
			}
			if(msg.length == 0){
				msg = Utils.getMessageOfInvalidFormatDate('fDate','Từ ngày');
			}
			if(msg.length == 0){
				msg = Utils.getMessageOfInvalidFormatDate('tDate','Đến ngày');
			}
			var fDate = $('#fDate').val();
			var tDate = $('#tDate').val();
			if(!Utils.compareDate(fDate, tDate)){
				msg = msgErr_fromdate_greater_todate;		
				$('#fDate').focus();
			}
			if(msg.length > 0){
				$('#errMsg').html(msg).show();
				return false;
			}
			var dataModel = new Object();		
			dataModel.shopId = $('#shop').combotree('getValue');
			dataModel.fromDate = $('#fDate').val().trim();
			dataModel.toDate = $('#tDate').val().trim();
			dataModel.staffCode = $('#staffCode').val().trim();
			dataModel.notApproved = '1';
			if($('input[name=formatType]:radio:checked').length > 0 && $('input[name=formatType]:radio:checked').val() != undefined) {
				dataModel.formatType = $('input[name=formatType]:radio:checked').val().toUpperCase();
			}
			ReportUtils.exportReport('/report/daily-follow/pgntn_view', dataModel);
		}
};
/*
 * END OF FILE - /web/web/resources/scripts/business/shop-report/vnm.deliveryTicketdaily.js
 */

/*
 * START OF FILE - /web/web/resources/scripts/business/shop-report/vnm.tax-report.js
 */
var TaxReport = {	
	exportCTHDGTGT: function(){
		$('#errMsg').html('').hide();
		var msg = '';
		if($('#shop').combotree('getValue').trim().length == 0 || $('#shop').combotree('getValue').trim() == activeType.ALL){
			msg = format(msgErr_required_field,'đơn vị');
		}
		
		if(msg.length == 0){
			if($('#isNPP').val() == "false"){
				msg = "Đơn vị bạn vừa chọn không phải là 1 nhà phân phối cụ thể!";
			}
		}
		
		if(msg.length == 0){
			msg = Utils.getMessageOfRequireCheck('fDate','Từ ngày');
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfInvalidFormatDate('fDate','Từ ngày');
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfRequireCheck('tDate','Đến ngày');
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfInvalidFormatDate('tDate','Đến ngày');
		}

		var fDate = $('#fDate').val();
		var tDate = $('#tDate').val();
		if(!Utils.compareDate(fDate, tDate)){
			msg = msgErr_fromdate_greater_todate;		
			$('#fDate').focus();
		}
		var currentTime = new Date();
		var month = currentTime.getMonth() + 1;
		var day = currentTime.getDate();
		var year = currentTime.getFullYear();
		if(!Utils.compareDate(fDate,day + '/' + month + '/' + year)){
			msg = 'Từ ngày không được lớn hơn ngày hiện tại';
			$('#fDate').focus();
		}
		if(msg.length > 0){
			$('#errMsg').html(msg).show();
			return false;
		}
		var data = new Object();		
		data.shopId = $('#shop').combotree('getValue').trim();
		data.staffCode = $('#staffCode').val().trim();
		data.fromDate = $('#fDate').val().trim();
		data.toDate = $('#tDate').val().trim();
		data.tax = $('#tax').val().trim();
		data.status = $('#status').combobox('getValue').trim();
		data.type = $('#type').val().trim();
		var kData = $.param(data,true);
		$('#divOverlay').show();		
		$.ajax({
			type : "POST",
			url : "/report/tax/cthd-gtgt/export",
			data :(kData),
			dataType: "json",
			success : function(data) {
				$('#divOverlay').hide();				
				if(!data.error) {
					if(data.hasData != null && data.hasData != undefined && data.hasData == true) {
						window.location.href = data.path;
						setTimeout(function(){ //Set timeout để đảm bảo file load lên hoàn tất
		                    CommonSearch.deleteFileExcelExport(data.path);
						},2000);
					} else {
						$('#errMsg').html('Không có dữ liệu để xuất').show();
					}
				} else {
					$('#errMsg').html(data.errMsg).show();					
				}
			},
			error:function(XMLHttpRequest, textStatus, errorDivThrown) {
				$('#divOverlay').hide();
				StockIssued.xhrExport = null;
			}
		});
		return false;
	},
	
	export7_7_8: function(){
		$('#errMsg').html('').hide();
		var msg = '';
		if($('#shop').combotree('getValue').trim().length == 0 || $('#shop').combotree('getValue').trim() == activeType.ALL){
			msg = format(msgErr_required_field,'đơn vị');
		}
		
		if(msg.length == 0){
			if($('#isNPP').val() == "false"){
				msg = "Đơn vị bạn vừa chọn không phải là 1 nhà phân phối cụ thể!";
			}
		}
		
		if(msg.length == 0){
			msg = Utils.getMessageOfRequireCheck('fDate','Từ ngày');
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfInvalidFormatDate('fDate','Từ ngày');
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfRequireCheck('tDate','Đến ngày');
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfInvalidFormatDate('tDate','Đến ngày');
		}

		var fDate = $('#fDate').val();
		var tDate = $('#tDate').val();
		if(!Utils.compareDate(fDate, tDate)){
			msg = msgErr_fromdate_greater_todate;		
			$('#fDate').focus();
		}
		var currentTime = new Date();
		var month = currentTime.getMonth() + 1;
		var day = currentTime.getDate();
		var year = currentTime.getFullYear();
		if(!Utils.compareDate(fDate,day + '/' + month + '/' + year)){
			msg = 'Từ ngày không được lớn hơn ngày hiện tại';
			$('#fDate').focus();
		}
		if(msg.length > 0){
			$('#errMsg').html(msg).show();
			return false;
		}
		var data = new Object();		
		data.shopId = $('#shop').combotree('getValue').trim();
		data.fromDate = $('#fDate').val().trim();
		data.toDate = $('#tDate').val().trim();
		
		data.formatType = $('input[name="formatType"]:checked').val();

		var kData = $.param(data,true);
		$('#divOverlay').show();		
		$.ajax({
			type : "POST",
			url : "/report/tax/thue-rpt7-7-8/export",
			data :(kData),
			dataType: "json",
			success : function(data) {
				$('#divOverlay').hide();				
				if(!data.error) {
					if(data.hasData != null && data.hasData != undefined && data.hasData == true) {
						window.location.href = data.path;
						setTimeout(function(){ //Set timeout để đảm bảo file load lên hoàn tất
		                    CommonSearch.deleteFileExcelExport(data.path);
						},2000);
					} else {
						$('#errMsg').html('Không có dữ liệu để xuất').show();
					}
				} else {
					$('#errMsg').html(data.errMsg).show();					
				}
			},
			error:function(XMLHttpRequest, textStatus, errorDivThrown) {
				$('#divOverlay').hide();
				StockIssued.xhrExport = null;
			}
		});
		return false;
	},
	
	reportHDGTGT: function(){
		var msg = '';
		$('#errMsg').html('').hide();
		msg = Utils.getMessageOfRequireCheck('tax','Thuế suất');
		if(msg.length == 0){
			msg = Utils.getMessageOfSpecialCharactersValidateEx('tax','Thuế suất',Utils._TF_NUMBER);
			$('#tax').focus();			
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfRequireCheck('fDate','Từ ngày');
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfInvalidFormatDate('fDate','Từ ngày');
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfRequireCheck('tDate','Từ ngày');
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfInvalidFormatDate('tDate','Đến ngày');
		}
		var fDate = $('#fDate').val();
		var tDate = $('#tDate').val();
		if(!Utils.compareDate(fDate, tDate)){
			msg = msgErr_fromdate_greater_todate;		
			$('#fDate').focus();
		}
		var currentTime = new Date();
		var month = currentTime.getMonth() + 1;
		var day = currentTime.getDate();
		var year = currentTime.getFullYear();
		if(!Utils.compareDate(fDate,day + '/' + month + '/' + year)){
			msg = 'Từ ngày không được lớn hơn ngày hiện tại';
			$('#fDate').focus();
		}
		
		if(msg.length > 0){
			$('#errMsg').html(msg).show();
			return false;
		}
		var dataModel = new Object();		
		dataModel.taxRate = $('#tax').val().trim();
		dataModel.staffCode = $('#staffCode').val().trim();
		dataModel.fromDate = $('#fDate').val().trim();
		dataModel.toDate = $('#tDate').val().trim();
		dataModel.custPayment = $('#custPayment').val().trim();
		dataModel.orderNumber = $('#orderNumber').val().trim();
		dataModel.customerCode = $('#customerCode').val().trim();		
		dataModel.invoiceNumber = $('#invoiceNumber').val().trim();
		//ReportUtils.exportReport('/report/tax/hdgtgt/export',dataModel);
		ReportUtils.exportReport('/report/tax/hdgtgt/export',dataModel);
	},
	/**
	 * @author vuonghn
	 * @since 13/05/2014
	 * @description Xuat bao cao 7.7.6. Bang ke hoa don gia tri gia tang
	 * */
	export7_7_6: function(){
		
		var msg = '';
		var multiShop = $("#shop").data("kendoMultiSelect");
		var dataShop = multiShop.dataItems();
		$('#errMsg').html('').hide();
		if(dataShop==null||dataShop.length==0){
			msg = format(msgErr_required_field,'Đơn vị');
		}
		var fDate = $('#fDate').val();
		var tDate = $('#tDate').val();
		if(msg.length == 0){
			msg = Utils.getMessageOfRequireCheck('fDate','Từ ngày');
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfInvalidFormatDate('fDate','Từ ngày');
		}
		if(msg.length == 0){
			if(!Utils.compareCurrentDate(fDate)){
				msg = "Từ ngày phải bé hơn hoặc bằng ngày hiện tại.";
			}
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfRequireCheck('tDate','Đến ngày');
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfInvalidFormatDate('tDate','Đến ngày');
		}
		if(msg.length == 0){
			if(!Utils.compareCurrentDate(tDate)){
				msg = "Đến ngày phải bé hơn hoặc bằng ngày hiện tại.";
			}
		}
		if(msg.length == 0){			
			if(!Utils.compareDate(fDate, tDate)){
				msg = msgErr_fromdate_greater_todate;
			}
		}
		
		if(msg.length > 0){
			$('#errMsg').html(msg).show();
			return false;
		}
		var dataModel = new Object();
		if(dataShop!=null && dataShop.length>0){
			var lstShop = dataShop[0].id;
			for(var i=1;i<dataShop.length;i++){
				lstShop += "," + dataShop[i].id;
			}
			dataModel.strListShopId = lstShop;
		}
		//dataModel.shopId = $('#shopId').val().trim();				
		if(dataShop!=null && dataShop.length>0){
			var lstShop = dataShop[0].id;
			for(var i=1;i<dataShop.length;i++){
				lstShop += "," + dataShop[i].id;
			}
			dataModel.listShopId = lstShop;
		}
		dataModel.fromDate = $('#fDate').val().trim();
		dataModel.toDate = $('#tDate').val().trim();
		dataModel.shopCode = $('#shopCode').val().trim();
		dataModel.formatType = $('input[name="formatType"]:checked').val();
		//ReportUtils.exportReport('/report/tax/bkhd-gtgt/export',dataModel);
		ReportUtils.exportReport('/report/tax/bkhd-gtgt/export',dataModel);
	}
};
/*
 * END OF FILE - /web/web/resources/scripts/business/shop-report/vnm.tax-report.js
 */

/*
 * START OF FILE - /web/web/resources/scripts/business/shop-report/vnm.sales-revenue-report.js
 */
/*
 *  BAO CAO DOANH THU BAN HANG
 */
var SalesRevenueReport = {
	/**
	 * Rpt 7.2.1: Bao cao Theo doi Ban hang theo mat hang
	 * */
	exportTDBHTMH : function() {
		$('#errMsg').html('').hide();
		var msg = '';
		var t = $('#shop').combotree('tree');
		var node = t.tree('find', $('#shop').combotree('getValue'));
		if (node == undefined || node == null || node.isLevel == undefined || node.isLevel == null || node.isLevel != 5) {
			msg = "Đơn vị bạn vừa chọn không phải là 1 nhà phân phối cụ thể!";
		}
		
		if (msg.length == 0) {
			msg = Utils.getMessageOfRequireCheck('fDate', 'Từ ngày');
		}
		if (msg.length == 0) {
			msg = Utils.getMessageOfInvalidFormatDate('fDate', 'Từ ngày');
		}
		if (msg.length == 0) {
			msg = Utils.getMessageOfRequireCheck('tDate', 'Đến ngày');
		}
		if (msg.length == 0) {
			msg = Utils.getMessageOfInvalidFormatDate('tDate', 'Đến ngày');
		}
		var fDate = $('#fDate').val();
		var tDate = $('#tDate').val();
		if (!Utils.compareDate(fDate, tDate)) {
			msg = msgErr_fromdate_greater_todate;
			$('#fDate').focus();
		}
		var currentTime = new Date();
		var month = currentTime.getMonth() + 1;
		var day = currentTime.getDate();
		var year = currentTime.getFullYear();
		if (!Utils.compareDate(fDate, day + '/' + month + '/' + year)) {
			msg = 'Từ ngày không được lớn hơn ngày hiện tại';
			$('#fDate').focus();
		}
		if (msg.length > 0) {
			$('#errMsg').html(msg).show();
			return false;
		}
		var data = new Object();
		data.shopId = $('#shopId').val().trim();
		data.staffCode = $('#staffCode').val().trim();
		data.fromDate = $('#fDate').val().trim();
		data.toDate = $('#tDate').val().trim();
		if ($('input[name=formatType]:radio:checked').length > 0 && $('input[name=formatType]:radio:checked').val() != undefined) {
			data.formatType = $('input[name=formatType]:radio:checked').val().toUpperCase();
		}
		// ReportUtils.exportReport('/report/sales-revenue/view-dayreport',dataModel);
		var kData = $.param(data, true);
		$('#divOverlay').show();
		ReportUtils.exportReport('/report/sales-revenue/tdbhtmh/export', data);
		
//		$.ajax({
//			type : "POST",
//			url : "/report/sales-revenue/tdbhtmh/export",
//			data : (kData),
//			dataType : "json",
//			success : function(data) {
//				$('#divOverlay').hide();
//				if (!data.error) {
//					if (data.hasData != null && data.hasData != undefined && data.hasData == true) {
//						window.location.href = data.path;
//						setTimeout(function() { // Set timeout để đảm bảo file
//							// load lên hoàn tất
//							CommonSearch.deleteFileExcelExport(data.path);
//						}, 2000);
//					} else {
//						$('#errMsg').html('Không có dữ liệu để xuất!').show();
//					}
//				} else {
//					$('#errMsg').html(data.errMsg).show();
//				}
//			},
//			error : function(XMLHttpRequest, textStatus, errorDivThrown) {
//				$('#divOverlay').hide();
//				StockIssued.xhrExport = null;
//			}
//		});
		
		return false;

	},
	exportRpt10_1_2 : function() {
		$('#errMsg').html('').hide();
		var msg = '';
		if ($('#shopId').val().trim().length == 0
				|| $('#shopId').val().trim() == activeType.ALL) {
			msg = format(msgErr_required_field, 'đơn vị');
		}
		if (msg.length == 0) {
			if ($('#isNPP').val() == "false") {
				msg = "Đơn vị bạn vừa chọn không phải là 1 nhà phân phối cụ thể!";
			}
		}
		if (msg.length == 0) {
			msg = Utils.getMessageOfRequireCheck('fDate', 'Từ ngày');
		}
		if (msg.length == 0) {
			msg = Utils.getMessageOfInvalidFormatDate('fDate', 'Từ ngày');
		}
		if (msg.length == 0) {
			msg = Utils.getMessageOfRequireCheck('tDate', 'Đến ngày');
		}
		if (msg.length == 0) {
			msg = Utils.getMessageOfInvalidFormatDate('tDate', 'Đến ngày');
		}
		var fDate = $('#fDate').val();
		var tDate = $('#tDate').val();
		if (!Utils.compareDate(fDate, tDate)) {
			msg = msgErr_fromdate_greater_todate;
			$('#fDate').focus();
		}
		if (msg.length == 0) {
			var fdate = $('#fDate').val().trim().split('/');
			var tdate = $('#tDate').val().trim().split('/');
			if(fdate[1]!=tdate[1]){
				msg = 'Từ ngày không cùng tháng với đến ngày';
			}
		}
		var currentTime = new Date();
		var month = currentTime.getMonth() + 1;
		var day = currentTime.getDate();
		var year = currentTime.getFullYear();
		if (!Utils.compareDate(fDate, day + '/' + month + '/' + year)) {
			msg = 'Từ ngày không được lớn hơn ngày hiện tại';
			$('#fDate').focus();
		}
		if (msg.length > 0) {
			$('#errMsg').html(msg).show();
			return false;
		}
		var data = new Object();
		data.shopId = $('#shopId').val().trim();
		data.staffCode = $('#staffCode').val().trim();
		data.fromDate = $('#fDate').val().trim();
		data.toDate = $('#tDate').val().trim();
		if ($('input[name=formatType]:radio:checked').length > 0
				&& $('input[name=formatType]:radio:checked').val() != undefined) {
			data.formatType = $('input[name=formatType]:radio:checked').val()
					.toUpperCase();
		}
		ReportUtils.exportReport('/report/sales-revenue/rpt10_1_2/export', data);
		/*var kData = $.param(data, true);
		$('#divOverlay').show();
		$.ajax({
			type : "POST",
			url : "/report/sales-revenue/rpt10_1_2/export",
			data : (kData),
			dataType : "json",
			success : function(data) {
				$('#divOverlay').hide();
				if (!data.error) {
					if (data.hasData != null && data.hasData != undefined
							&& data.hasData == true) {
						window.location.href = data.path;
						setTimeout(function() { // Set timeout để đảm bảo file
							// load lên hoàn tất
							CommonSearch.deleteFileExcelExport(data.path);
						}, 2000);
					} else {
						$('#errMsg').html('Không có dữ liệu để xuất!').show();
					}
				} else {
					$('#errMsg').html(data.errMsg).show();
				}
			},
			error : function(XMLHttpRequest, textStatus, errorDivThrown) {
				$('#divOverlay').hide();
				StockIssued.xhrExport = null;
			}
		});*/
		return false;


	},
	exportTDDSTKHNHMH : function() {
		$('#errMsg').html('').hide();
		var msg = '';
		if ($('#shopId').val().trim().length == 0
				|| $('#shopId').val().trim() == activeType.ALL) {
			msg = format(msgErr_required_field, 'đơn vị');
		}
		if (msg.length == 0) {
			if ($('#isNPP').val() == "false") {
				msg = "Đơn vị bạn vừa chọn không phải là 1 nhà phân phối cụ thể!";
			}
		}
		if (msg.length == 0) {
			msg = Utils.getMessageOfRequireCheck('fDate', 'Từ ngày');
		}
		if (msg.length == 0) {
			msg = Utils.getMessageOfInvalidFormatDate('fDate', 'Từ ngày');
		}
		if (msg.length == 0) {
			msg = Utils.getMessageOfRequireCheck('tDate', 'Đến ngày');
		}
		if (msg.length == 0) {
			msg = Utils.getMessageOfInvalidFormatDate('tDate', 'Đến ngày');
		}
		var fDate = $('#fDate').val();
		var tDate = $('#tDate').val();
		if (!Utils.compareDate(fDate, tDate)) {
			msg = msgErr_fromdate_greater_todate;
			$('#fDate').focus();
		}
		var currentTime = new Date();
		var month = currentTime.getMonth() + 1;
		var day = currentTime.getDate();
		var year = currentTime.getFullYear();
		if (!Utils.compareDate(fDate, day + '/' + month + '/' + year)) {
			msg = 'Từ ngày không được lớn hơn ngày hiện tại';
			$('#fDate').focus();
		}
		if (msg.length > 0) {
			$('#errMsg').html(msg).show();
			return false;
		}
		var data = new Object();
		data.shopId = $('#shopId').val().trim();
		data.customerCode = $('#customerCode').val().trim();
		data.fromDate = $('#fDate').val().trim();
		data.toDate = $('#tDate').val().trim();
		if ($('input[name=formatType]:radio:checked').length > 0
				&& $('input[name=formatType]:radio:checked').val() != undefined) {
			data.formatType = $('input[name=formatType]:radio:checked').val()
					.toUpperCase();
		}
		ReportUtils.exportReport('/report/sales-revenue/tddstkhnhmh/export', data);
	},
	exportDSKHTMH : function() {
		$('#errMsg').html('').hide();
		var msg = '';
		if ($('#shopId').val().trim().length == 0
				|| $('#shopId').val().trim() == activeType.ALL) {
			msg = format(msgErr_required_field, 'đơn vị');
		}
		if (msg.length == 0) {
			if ($('#isNPP').val() == "false") {
				msg = "Đơn vị bạn vừa chọn không phải là 1 nhà phân phối cụ thể!";
			}
		}
		if (msg.length == 0) {
			msg = Utils.getMessageOfRequireCheck('fDate', 'Từ ngày');
		}
		if (msg.length == 0) {
			msg = Utils.getMessageOfInvalidFormatDate('fDate', 'Từ ngày');
		}
		if (msg.length == 0) {
			msg = Utils.getMessageOfRequireCheck('tDate', 'Đến ngày');
		}
		if (msg.length == 0) {
			msg = Utils.getMessageOfInvalidFormatDate('tDate', 'Đến ngày');
		}
		var fDate = $('#fDate').val();
		var tDate = $('#tDate').val();
		if (!Utils.compareDate(fDate, tDate)) {
			msg = msgErr_fromdate_greater_todate;
			$('#fDate').focus();
		}
		var currentTime = new Date();
		var month = currentTime.getMonth() + 1;
		var day = currentTime.getDate();
		var year = currentTime.getFullYear();
		if (!Utils.compareDate(fDate, day + '/' + month + '/' + year)) {
			msg = 'Từ ngày không được lớn hơn ngày hiện tại';
			$('#fDate').focus();
		}
		if (msg.length > 0) {
			$('#errMsg').html(msg).show();
			return false;
		}
		var data = new Object();
		if ($('input[name=formatType]:radio:checked').length > 0
				&& $('input[name=formatType]:radio:checked').val() != undefined) {
			data.formatType = $('input[name=formatType]:radio:checked').val()
					.toUpperCase();
		}
		data.shopId = $('#shopId').val().trim();
		data.customerCode = $('#customerCode').val().trim().toUpperCase();
		data.fromDate = $('#fDate').val().trim();
		data.toDate = $('#tDate').val().trim();
		data.strListCustomer = $('#customerCode').val().trim().toUpperCase();
		var strListCategory = ""; 
		/*if(ProductCatalog._listCategory != undefined && ProductCatalog._listCategory != null 
				&& ProductCatalog._listCategory.length > 0 && ProductCatalog._listCategory[0] != '0' ){
			var sizeListCategory = ProductCatalog._listCategory.length; 
			for(var i = 0; i< sizeListCategory-1; i++){
				strListCategory += ProductCatalog._listCategory[i].trim().toUpperCase() + ",";
			}
			strListCategory += ProductCatalog._listCategory[sizeListCategory-1].trim().toUpperCase();
		}*/
		var arr = $('#category').val();
		if (arr != null && arr.length > 0) {
			strListCategory = arr.join(",");
		}
		data.strListCategory = strListCategory.trim().toUpperCase();		
		data.strListProduct = $('#productCode').val().trim().toUpperCase();
		ReportUtils.exportReport('/report/sales-revenue/dskhtmh/export', data);
		return false;
	},
	exportTTCTTB : function() {
		$('#errMsg').html('').hide();
		var msg = '';
		if ($('#shop').combotree('getValue').trim().length == 0
				|| $('#shop').combotree('getValue').trim() == activeType.ALL) {
			msg = format(msgErr_required_field, 'đơn vị');
		}

		if (msg.length == 0) {
			if ($('#isNPP').val() == "0") {
				msg = "Đơn vị bạn vừa chọn không phải là 1 nhà phân phối cụ thể!";
			}
		}

		if (msg.length == 0) {
			msg = Utils.getMessageOfRequireCheck('fDate', 'Từ ngày');
		}
		if (msg.length == 0) {
			msg = Utils.getMessageOfInvalidFormatDate('fDate', 'Từ ngày');
		}
		if (msg.length == 0) {
			msg = Utils.getMessageOfRequireCheck('tDate', 'Đến ngày');
		}
		if (msg.length == 0) {
			msg = Utils.getMessageOfInvalidFormatDate('tDate', 'Đến ngày');
		}
		var fDate = $('#fDate').val();
		var tDate = $('#tDate').val();
		if (!Utils.compareDate(fDate, tDate)) {
			msg = msgErr_fromdate_greater_todate;
			$('#fDate').focus();
		}
		var currentTime = new Date();
		var month = currentTime.getMonth() + 1;
		var day = currentTime.getDate();
		var year = currentTime.getFullYear();
		if (!Utils.compareDate(fDate, day + '/' + month + '/' + year)) {
			msg = 'Từ ngày không được lớn hơn ngày hiện tại';
			$('#fDate').focus();
		}
		if (msg.length > 0) {
			$('#errMsg').html(msg).show();
			return false;
		}
		var data = new Object();
		data.shopId = $('#shop').combotree('getValue').trim();
		data.lstDisProCode = $('#multiChoiceCTTB').val();
		data.fromDate = $('#fDate').val().trim();
		data.toDate = $('#tDate').val().trim();
		data.type = $('#type').val().trim();

		if ($('input[name=formatType]:radio:checked').length > 0
				&& $('input[name=formatType]:radio:checked').val() != undefined) {
			data.formatType = $('input[name=formatType]:radio:checked').val()
					.toUpperCase();
		}
		ReportUtils.exportReport('/report/sales-revenue/ttcttb/export', data);
//		var kData = $.param(data, true);
//		$('#divOverlay').show();
//		$.ajax({
//			type : "POST",
//			url : "/report/sales-revenue/ttcttb/export",
//			data : (kData),
//			dataType : "json",
//			success : function(data) {
//				$('#divOverlay').hide();
//				if (!data.error) {
//					if (data.hasData != null && data.hasData != undefined
//							&& data.hasData == true) {
//						window.location.href = data.path;
//						setTimeout(function() { // Set timeout để đảm bảo file
//							// load lên hoàn tất
//							CommonSearch.deleteFileExcelExport(data.path);
//						}, 2000);
//					} else {
//						$('#errMsg').html('Không có dữ liệu để xuất').show();
//					}
//				} else {
//					$('#errMsg').html(data.errMsg).show();
//				}
//			},
//			error : function(XMLHttpRequest, textStatus, errorDivThrown) {
//				$('#divOverlay').hide();
//				StockIssued.xhrExport = null;
//			}
//		});
		return false;

	},
	exportDTTHNV : function() {
		$('#errMsg').html('').hide();
		var msg = '';
		if ($('#shop').combotree('getValue').trim().length == 0
				|| $('#shop').combotree('getValue').trim() == activeType.ALL) {
			msg = format(msgErr_required_field, 'đơn vị');
		}

		if (msg.length == 0) {
			if ($('#isNPP').val() == "false") {
				msg = "Đơn vị bạn vừa chọn không phải là 1 nhà phân phối cụ thể!";
			}
		}

		if (msg.length == 0) {
			msg = Utils.getMessageOfRequireCheck('fDate', 'Từ ngày');
		}
		if (msg.length == 0) {
			msg = Utils.getMessageOfInvalidFormatDate('fDate', 'Từ ngày');
		}
		if (msg.length == 0) {
			msg = Utils.getMessageOfRequireCheck('tDate', 'Đến ngày');
		}
		if (msg.length == 0) {
			msg = Utils.getMessageOfInvalidFormatDate('tDate', 'Đến ngày');
		}
		var fDate = $('#fDate').val();
		var tDate = $('#tDate').val();
		if (!Utils.compareDate(fDate, tDate)) {
			msg = msgErr_fromdate_greater_todate;
			$('#fDate').focus();
		}
		var currentTime = new Date();
		var month = currentTime.getMonth() + 1;
		var day = currentTime.getDate();
		var year = currentTime.getFullYear();
		if (!Utils.compareDate(fDate, day + '/' + month + '/' + year)) {
			msg = 'Từ ngày không được lớn hơn ngày hiện tại';
			$('#fDate').focus();
		}
		if (msg.length > 0) {
			$('#errMsg').html(msg).show();
			return false;
		}
		var data = new Object();
		data.shopId = $('#shop').combotree('getValue').trim();
		data.staffCode = $('#staffCode').val().trim();
		data.fromDate = $('#fDate').val().trim();
		data.toDate = $('#tDate').val().trim();

		if ($('input[name=formatType]:radio:checked').length > 0
				&& $('input[name=formatType]:radio:checked').val() != undefined) {
			data.formatType = $('input[name=formatType]:radio:checked').val()
					.toUpperCase();
		}
		var kData = $.param(data, true);
		$('#divOverlay').show();
		$.ajax({
			type : "POST",
			url : "/report/sales-revenue/dtthnv/export",
			data : (kData),
			dataType : "json",
			success : function(data) {
				$('#divOverlay').hide();
				if (!data.error) {
					if (data.hasData != null && data.hasData != undefined
							&& data.hasData == true) {
						window.location.href = data.path;
						setTimeout(function() { // Set timeout để đảm bảo file
							// load lên hoàn tất
							CommonSearch.deleteFileExcelExport(data.path);
						}, 2000);
					} else {
						$('#errMsg').html('Không có dữ liệu để xuất').show();
					}
				} else {
					$('#errMsg').html(data.errMsg).show();
				}
			},
			error : function(XMLHttpRequest, textStatus, errorDivThrown) {
				$('#divOverlay').hide();
				StockIssued.xhrExport = null;
			}
		});
		return false;
	},
	exportBTHDN : function() {
		$('#errMsg').html('').hide();
		var msg = '';
		if ($('#shop').combotree('getValue').trim().length == 0
				|| $('#shop').combotree('getValue').trim() == activeType.ALL) {
			msg = format(msgErr_required_field, 'đơn vị');
		}

		if (msg.length == 0) {
			if ($('#isNPP').val() == "false") {
				msg = "Đơn vị bạn vừa chọn không phải là 1 nhà phân phối cụ thể!";
			}
		}

		if (msg.length == 0) {
			msg = Utils.getMessageOfRequireCheck('fDate', 'Từ ngày');
		}
		if (msg.length == 0) {
			msg = Utils.getMessageOfInvalidFormatDate('fDate', 'Từ ngày');
		}
		if (msg.length == 0) {
			msg = Utils.getMessageOfRequireCheck('tDate', 'Đến ngày');
		}
		if (msg.length == 0) {
			msg = Utils.getMessageOfInvalidFormatDate('tDate', 'Đến ngày');
		}
		var fDate = $('#fDate').val();
		var tDate = $('#tDate').val();
		if (!Utils.compareDate(fDate, tDate)) {
			msg = msgErr_fromdate_greater_todate;
			$('#fDate').focus();
		}
		var currentTime = new Date();
		var month = currentTime.getMonth() + 1;
		var day = currentTime.getDate();
		var year = currentTime.getFullYear();
		if (!Utils.compareDate(fDate, day + '/' + month + '/' + year)) {
			msg = 'Từ ngày không được lớn hơn ngày hiện tại';
			$('#fDate').focus();
		}
		if (msg.length > 0) {
			$('#errMsg').html(msg).show();
			return false;
		}
		var data = new Object();
		data.shopId = $('#shop').combotree('getValue').trim();
		data.catCode = $('#catCode').val().trim();
		data.fromDate = $('#fDate').val().trim();
		data.toDate = $('#tDate').val().trim();

		var kData = $.param(data, true);
		$('#divOverlay').show();
		$.ajax({
			type : "POST",
			url : "/report/sales-revenue/bthdh/export",
			data : (kData),
			dataType : "json",
			success : function(data) {
				$('#divOverlay').hide();
				if (!data.error) {
					if (data.hasData != null && data.hasData != undefined
							&& data.hasData == true) {
						window.location.href = data.path;
						setTimeout(function() { // Set timeout để đảm bảo file
							// load lên hoàn tất
							CommonSearch.deleteFileExcelExport(data.path);
						}, 2000);
					} else {
						$('#errMsg').html('Không có dữ liệu để xuất').show();
					}
				} else {
					$('#errMsg').html(data.errMsg).show();
				}
			},
			error : function(XMLHttpRequest, textStatus, errorDivThrown) {
				$('#divOverlay').hide();
				StockIssued.xhrExport = null;
			}
		});
		return false;
	},
	exportBCN : function() {
		$('#errMsg').html('').hide();
		var msg = '';
		if ($('#shop').combotree('getValue').trim().length == 0
				|| $('#shop').combotree('getValue').trim() == activeType.ALL) {
			msg = format(msgErr_required_field, 'đơn vị');
		}

		if (msg.length == 0) {
			if ($('#isNPP').val() == "false") {
				msg = "Đơn vị bạn vừa chọn không phải là 1 nhà phân phối cụ thể!";
			}
		}

		if (msg.length == 0) {
			msg = Utils.getMessageOfRequireCheck('fDate', 'Từ ngày');
		}
		if (msg.length == 0) {
			msg = Utils.getMessageOfInvalidFormatDate('fDate', 'Từ ngày');
		}

		var fDate = $('#fDate').val();
		var currentTime = new Date();
		var month = currentTime.getMonth() + 1;
		var day = currentTime.getDate();
		var year = currentTime.getFullYear();
		if (!Utils.compareDate(fDate, day + '/' + month + '/' + year)) {
			msg = 'Ngày báo cáo không được lớn hơn ngày hiện tại';
			$('#fDate').focus();
		}
		if (msg.length > 0) {
			$('#errMsg').html(msg).show();
			return false;
		}
		var data = new Object();
		data.shopId = $('#shop').combotree('getValue').trim();
		data.toDate = $('#fDate').val().trim();
		
		if ($('input[name=formatType]:radio:checked').length > 0
				&& $('input[name=formatType]:radio:checked').val() != undefined) {
			data.formatType = $('input[name=formatType]:radio:checked').val()
					.toUpperCase();
		}

		ReportUtils.exportReport('/report/sales-revenue/bcn/export', data);
	},
	exportCTKMTCT : function() {
		$('#errMsg').html('').hide();
		var msg = '';
		if ($('#shop').combotree('getValue').trim().length == 0
				|| $('#shop').combotree('getValue').trim() == activeType.ALL) {
			msg = format(msgErr_required_field, 'đơn vị');
		}

		if (msg.length == 0) {
			if ($('#isNPP').val() == "false") {
				msg = "Đơn vị bạn vừa chọn không phải là 1 nhà phân phối cụ thể!";
			}
		}
		if (msg.length == 0) {
			msg = Utils.getMessageOfSpecialCharactersValidate('staffCode',
					'NVBH', Utils._CODE);
		}
		if (msg.length == 0) {
			msg = Utils.getMessageOfRequireCheck('fDate', 'Từ ngày');
		}
		if (msg.length == 0) {
			msg = Utils.getMessageOfInvalidFormatDate('fDate', 'Từ ngày');
		}
		if (msg.length == 0) {
			msg = Utils.getMessageOfRequireCheck('tDate', 'Đến ngày');
		}
		if (msg.length == 0) {
			msg = Utils.getMessageOfInvalidFormatDate('tDate', 'Đến ngày');
		}
		var fDate = $('#fDate').val();
		var tDate = $('#tDate').val();
		if (msg.length == 0) {
			if (!Utils.compareDate(fDate, tDate)) {
				msg = msgErr_fromdate_greater_todate;
				$('#fDate').focus();
			}
		}
		var currentTime = new Date();
		var month = currentTime.getMonth() + 1;
		var day = currentTime.getDate();
		var year = currentTime.getFullYear();
		if (msg.length == 0) {
			if (!Utils.compareDate(fDate, day + '/' + month + '/' + year)) {
				msg = 'Từ ngày không được lớn hơn ngày hiện tại';
				$('#fDate').focus();
			}
		}
		if (msg.length > 0) {
			$('#errMsg').html(msg).show();
			return false;
		}
		var data = new Object();
		data.shopId = $('#shop').combotree('getValue').trim();
		data.staffCode = $('#staffCode').val().trim();
		data.fromDate = $('#fDate').val().trim();
		data.toDate = $('#tDate').val().trim();

		if ($('input[name=formatType]:radio:checked').length > 0 && $('input[name=formatType]:radio:checked').val() != undefined) {
			data.formatType = $('input[name=formatType]:radio:checked').val().toUpperCase();
		}

		if (data.formatType === "XLS") {
			ReportUtils.exportReport('/report/sales-revenue/ctkmtct/export', data);
		} else {
			ReportUtils.exportReport('/report/sales-revenue/ctkmtct/exportPDF', data);
		}
		
	},
	exportCTKMTCTNV : function() {
		$('#errMsg').html('').hide();
		var msg = '';
		if ($('#shop').combotree('getValue').trim().length == 0
				|| $('#shop').combotree('getValue').trim() == activeType.ALL) {
			msg = format(msgErr_required_field, 'đơn vị');
		}

		if (msg.length == 0) {
			if ($('#isNPP').val() == "false") {
				msg = "Đơn vị bạn vừa chọn không phải là 1 nhà phân phối cụ thể!";
			}
		}
		if (msg.length == 0) {
			msg = Utils.getMessageOfSpecialCharactersValidate('staffCode',
					'NVBH', Utils._CODE);
		}
		if (msg.length == 0) {
			msg = Utils.getMessageOfRequireCheck('fDate', 'Từ ngày');
		}
		if (msg.length == 0) {
			msg = Utils.getMessageOfInvalidFormatDate('fDate', 'Từ ngày');
		}
		if (msg.length == 0) {
			msg = Utils.getMessageOfRequireCheck('tDate', 'Đến ngày');
		}
		if (msg.length == 0) {
			msg = Utils.getMessageOfInvalidFormatDate('tDate', 'Đến ngày');
		}
		var fDate = $('#fDate').val();
		var tDate = $('#tDate').val();
		if (msg.length == 0) {
			if (!Utils.compareDate(fDate, tDate)) {
				msg = msgErr_fromdate_greater_todate;
				$('#fDate').focus();
			}
		}
		var currentTime = new Date();
		var month = currentTime.getMonth() + 1;
		var day = currentTime.getDate();
		var year = currentTime.getFullYear();
		if (msg.length == 0) {
			if (!Utils.compareDate(fDate, day + '/' + month + '/' + year)) {
				msg = 'Từ ngày không được lớn hơn ngày hiện tại';
				$('#fDate').focus();
			}
		}
		if (msg.length > 0) {
			$('#errMsg').html(msg).show();
			return false;
		}
		var data = new Object();
		data.shopId = $('#shop').combotree('getValue').trim();
		data.staffCode = $('#staffCode').val().trim();
		data.fromDate = $('#fDate').val().trim();
		data.toDate = $('#tDate').val().trim();

		var kData = $.param(data, true);
		$('#divOverlay').show();
		$.ajax({
			type : "POST",
			url : "/report/sales-revenue/ctkmtctnv/export",
			data : (kData),
			dataType : "json",
			success : function(data) {
				$('#divOverlay').hide();
				if (!data.error) {
					if (data.hasData != null && data.hasData != undefined
							&& data.hasData == true) {
						window.location.href = data.path;
						setTimeout(function() { // Set timeout để đảm bảo file
							// load lên hoàn tất
							CommonSearch.deleteFileExcelExport(data.path);
						}, 2000);
					} else {
						$('#errMsg').html('Không có dữ liệu để xuất').show();
					}
				} else {
					$('#errMsg').html(data.errMsg).show();
				}
			},
			error : function(XMLHttpRequest, textStatus, errorDivThrown) {
				$('#divOverlay').hide();
				StockIssued.xhrExport = null;
			}
		});
		return false;
	},
	/* Begin Doanh so theo muc chuong trinh TB */
	exportDSTMCTTB : function() {
		$('#errMsg').html('').hide();
		var msg = '';
		if ($('#shop').combotree('getValue').trim().length == 0
				|| $('#shop').combotree('getValue').trim() == activeType.ALL) {
			msg = format(msgErr_required_field, 'đơn vị');
		}
		if (msg.length == 0) {
			if ($('#isNPP').val() == "false") {
				msg = "Đơn vị bạn vừa chọn không phải là 1 nhà phân phối cụ thể!";
			}
		}
		if (msg.length == 0) {
			msg = Utils.getMessageOfRequireCheck('multiChoiceCTTB',
					'Chương trình trưng bày');
		}
		if (msg.length > 0) {
			$('#errMsg').html(msg).show();
			return false;
		}
		var data = new Object();
		data.shopId = $('#shop').combotree('getValue').trim();
		data.year = $('#fullYearIn10').val();
		data.lstProgramDisplayCode = $('#multiChoiceCTTB').val();
		data.customer = $('#customerCode').val();
		ReportUtils.exportReport('/report/sales-revenue/dstmcttb/export', data);
		
	},

	// vuongmq
	/* End Doanh so theo muc chuong trinh TB */
	exportTDCTDS : function() {
		$('#errMsg').html('').hide();
		var msg = '';
		if ($('#shop').combotree('getValue').trim().length == 0
				|| $('#shop').combotree('getValue').trim() == activeType.ALL) {
			msg = format(msgErr_required_field, 'đơn vị');
		}

		if (msg.length == 0) {
			if ($('#isNPP').val() == "false") {
				msg = "Đơn vị bạn vừa chọn không phải là 1 nhà phân phối cụ thể!";
			}
		}

		if (msg.length == 0) {
			msg = Utils.getMessageOfRequireCheck('month', 'Tháng');
		}

		if (msg.length == 0) {
			var currentTime = new Date();
			var month = currentTime.getMonth() + 1;
			var day = currentTime.getDate();
			var year = currentTime.getFullYear();

			var monthString = "01/" + $('#month').val().trim();
			var tmp = Utils.compareMonthString(monthString, day + '/' + month
					+ '/' + year);
			if (tmp == 1) {
				msg = 'Tháng không được lớn hơn tháng hiện tại';
				$('#month').focus();
			}
		}

		if (msg.length > 0) {
			$('#errMsg').html(msg).show();
			return false;
		}

		var data = new Object();
		data.shopId = $('#shop').combotree('getValue').trim();
		data.staffCode = $('#staffCode').val().trim();
		var date = $('#month').val().trim().split('/');
		data.month = date[0];
		data.year = date[1];

		var kData = $.param(data, true);
		$('#divOverlay').show();
		$.ajax({
			type : "POST",
			url : "/report/sales-revenue/tdctds/export",
			data : (kData),
			dataType : "json",
			success : function(data) {
				$('#divOverlay').hide();
				if (!data.error) {
					if (data.hasData != null && data.hasData != undefined
							&& data.hasData == true) {
						window.location.href = data.path;
						setTimeout(function() { // Set timeout để đảm bảo file
							// load lên hoàn tất
							CommonSearch.deleteFileExcelExport(data.path);
						}, 2000);
					} else {
						$('#errMsg').html('Không có dữ liệu để xuất').show();
					}
				} else {
					$('#errMsg').html(data.errMsg).show();
				}
			},
			error : function(XMLHttpRequest, textStatus, errorDivThrown) {
				$('#divOverlay').hide();
				StockIssued.xhrExport = null;
			}
		});
		return false;

	},
	exportDTBHTNTNVBH : function() {
		$('#errMsg').html('').hide();
		var msg = '';
		if ($('#shop').combotree('getValue').trim().length == 0
				|| $('#shop').combotree('getValue').trim() == activeType.ALL) {
			msg = format(msgErr_required_field, 'đơn vị');
		}

		if (msg.length == 0) {
			msg = Utils.getMessageOfRequireCheck('fDate', 'Từ ngày');
		}
		if (msg.length == 0) {
			msg = Utils.getMessageOfInvalidFormatDate('fDate', 'Từ ngày');
		}
		if (msg.length == 0) {
			msg = Utils.getMessageOfRequireCheck('tDate', 'Đến ngày');
		}
		if (msg.length == 0) {
			msg = Utils.getMessageOfInvalidFormatDate('tDate', 'Đến ngày');
		}
		var fDate = $('#fDate').val();
		var tDate = $('#tDate').val();
		if (!Utils.compareDate(fDate, tDate)) {
			msg = msgErr_fromdate_greater_todate;
			$('#fDate').focus();
		}
		var currentTime = new Date();
		var month = currentTime.getMonth() + 1;
		var day = currentTime.getDate();
		var year = currentTime.getFullYear();
		if (!Utils.compareDate(fDate, day + '/' + month + '/' + year)) {
			msg = 'Từ ngày không được lớn hơn ngày hiện tại';
			$('#fDate').focus();
		}
		if (msg.length > 0) {
			$('#errMsg').html(msg).show();
			return false;
		}
		var data = new Object();
		data.shopId = $('#shopId').val().trim();
		data.staffCode = $('#staffCode').val().trim();
		data.fromDate = $('#fDate').val().trim();
		data.toDate = $('#tDate').val().trim();

		if ($('input[name=formatType]:radio:checked').length > 0
				&& $('input[name=formatType]:radio:checked').val() != undefined) {
			data.formatType = $('input[name=formatType]:radio:checked').val()
					.toUpperCase();
		}

		if (data.formatType === "PDF") {
			ReportUtils.exportReport('/report/sales-revenue/dtbhtntnvbh/export', data);
		} else {
			ReportUtils.exportReport('/report/sales-revenue/dtbhtntnvbh/export', data);
		}

	},
	exportDSTHNVBHTN : function() {
		$('#errMsg').html('').hide();
		var msg = '';
		if ($('#shop').combotree('getValue').trim().length == 0
				|| $('#shop').combotree('getValue').trim() == activeType.ALL) {
			msg = format(msgErr_required_field, 'đơn vị');
		}

		if (msg.length == 0) {
			if ($('#isNPP').val() == "false") {
				msg = "Đơn vị bạn vừa chọn không phải là 1 nhà phân phối cụ thể!";
			}
		}
		if (msg.length == 0) {
			msg = Utils.getMessageOfRequireCheck('fDate', 'Từ ngày');
		}
		if (msg.length == 0) {
			msg = Utils.getMessageOfInvalidFormatDate('fDate', 'Từ ngày');
		}
		if (msg.length == 0) {
			msg = Utils.getMessageOfRequireCheck('tDate', 'Đến ngày');
		}
		if (msg.length == 0) {
			msg = Utils.getMessageOfInvalidFormatDate('tDate', 'Đến ngày');
		}
		var fDate = $('#fDate').val();
		var tDate = $('#tDate').val();
		if (!Utils.compareDate(fDate, tDate)) {
			msg = msgErr_fromdate_greater_todate;
			$('#fDate').focus();
		}
		var currentTime = new Date();
		var month = currentTime.getMonth() + 1;
		var day = currentTime.getDate();
		var year = currentTime.getFullYear();
		if (!Utils.compareDate(fDate, day + '/' + month + '/' + year)) {
			msg = 'Từ ngày không được lớn hơn ngày hiện tại';
			$('#fDate').focus();
		}
		if (msg.length > 0) {
			$('#errMsg').html(msg).show();
			return false;
		}
		var data = new Object();
		data.type = $('#type').val().trim();
		data.shopId = $('#shopId').val().trim();
		data.staffCode = $('#staffCode').val().trim();
		data.fromDate = $('#fDate').val().trim();
		data.toDate = $('#tDate').val().trim();

		if ($('input[name=formatType]:radio:checked').length > 0
				&& $('input[name=formatType]:radio:checked').val() != undefined) {
			data.formatType = $('input[name=formatType]:radio:checked').val()
					.toUpperCase();
		}

		ReportUtils.exportReport('/report/sales-revenue/dsthnvbhtn/export', data, 'errMsg');

	},
	exportTHCTHTB : function() {
		// vuonghn update
		$('#errMsg').html('').hide();
		var msg = '';
		if ($('#shopId').val().trim().length == 0
				|| $('#shopId').val().trim() == activeType.ALL) {
			msg = format(msgErr_required_field, 'đơn vị');
		}
		if (msg.length == 0) {
			if ($('#isNPP').val() == "false") {
				msg = "Đơn vị bạn vừa chọn không phải là 1 nhà phân phối cụ thể!";
			}
		}
		if (msg.length == 0) {
			if ($('#fullYearIn10').val().trim() == '')
				msg = "Bạn chưa chọn năm";
		}
		if (msg.length > 0) {
			$('#errMsg').html(msg).show();
			return false;
		}
		var data = new Object();
		data.shopId = $('#shopId').val().trim();
		data.year = $('#fullYearIn10').val();
		data.lstProgramDisplayCode = $('#multiChoiceCTTB').val().trim();
		// ReportUtils.exportReport('/report/sales-revenue/view-dayreport',dataModel);
		if ($('input[name=formatType]:radio:checked').length > 0
				&& $('input[name=formatType]:radio:checked').val() != undefined) {
			data.formatType = $('input[name=formatType]:radio:checked').val()
					.toUpperCase();
		}
		var kData = $.param(data, true);
		$('#divOverlay').show();
		$.ajax({
			type : "POST",
			url : "/report/sales-revenue/thcthtb/export",
			data : (kData),
			dataType : "json",
			success : function(data) {
				$('#divOverlay').hide();
				if (!data.error) {
					if (data.hasData != undefined && data.hasData != null
							&& data.hasData == false) {
						$('#errMsg').html('Không có dữ liệu để xuất báo cáo')
								.show();
					} else {
						window.location.href = data.path;
						setTimeout(function() { // Set timeout để đảm bảo file
							// load lên hoàn tất
							CommonSearch.deleteFileExcelExport(data.path);
						}, 2000);
					}
				} else {
					$('#errMsg').html(data.errMsg).show();
				}
			},
			error : function(XMLHttpRequest, textStatus, errorDivThrown) {
				$('#divOverlay').hide();
				StockIssued.xhrExport = null;
			}
		});
		return false;

	},
	exportBCCTKM : function() { // vuonghn Bao cao chi tiet khuyen mai
		$('#errMsg').html('').hide();
		var msg = '';
		if ($('#shopId').val().trim().length == 0
				|| $('#shopId').val().trim() == activeType.ALL) {
			msg = format(msgErr_required_field, 'đơn vị');
		}
		/*
		 * if(msg.length == 0){ if($('#isNPP').val() == "false"){ msg = "Đơn vị
		 * bạn vừa chọn không phải là 1 nhà phân phối cụ thể!"; } }
		 */
		if (msg.length == 0) {
			msg = Utils.getMessageOfRequireCheck('fDate', 'Từ ngày');
		}
		if (msg.length == 0) {
			msg = Utils.getMessageOfInvalidFormatDate('fDate', 'Từ ngày');
		}
		if (msg.length == 0) {
			msg = Utils.getMessageOfRequireCheck('tDate', 'Đến ngày');
		}
		if (msg.length == 0) {
			msg = Utils.getMessageOfInvalidFormatDate('tDate', 'Đến ngày');
		}
		var fDate = $('#fDate').val();
		var tDate = $('#tDate').val();
		if (!Utils.compareDate(fDate, tDate)) {
			msg = msgErr_fromdate_greater_todate;
			$('#fDate').focus();
		}
		var currentTime = new Date();
		var month = currentTime.getMonth() + 1;
		var day = currentTime.getDate();
		var year = currentTime.getFullYear();
		if (!Utils.compareDate(fDate, day + '/' + month + '/' + year)) {
			msg = 'Từ ngày không được lớn hơn ngày hiện tại';
			$('#fDate').focus();
		}
		if (msg.length > 0) {
			$('#errMsg').html(msg).show();
			return false;
		}
		var data = new Object();
		data.shopId = $('#shopId').val().trim();
		data.staffCode = $('#staffCode').val().trim();
		data.fromDate = $('#fDate').val().trim();
		data.toDate = $('#tDate').val().trim();
		// ReportUtils.exportReport('/report/sales-revenue/view-dayreport',dataModel);
		var kData = $.param(data, true);
		$('#divOverlay').show();
		$.ajax({
			type : "POST",
			url : "/report/sales-revenue/bcctkm/export",
			data : (kData),
			dataType : "json",
			success : function(data) {
				$('#divOverlay').hide();
				if (!data.error) {
					if (data.hasData != null && data.hasData != undefined
							&& data.hasData == true) {
						window.location.href = data.path;
						setTimeout(function() { // Set timeout để đảm bảo file
							// load lên hoàn tất
							CommonSearch.deleteFileExcelExport(data.path);
						}, 2000);
					} else {
						$('#errMsg').html('Không có dữ liệu để xuất').show();
					}
				} else {
					$('#errMsg').html(data.errMsg).show();
				}
			},
			error : function(XMLHttpRequest, textStatus, errorDivThrown) {
				$('#divOverlay').hide();
				StockIssued.xhrExport = null;
			}
		});
		return false;

	},
	/* BAO CAO NGAY */
	exportDayReport : function() {
		$('#errMsg').html('').hide();
		var msg = '';
		if ($('#shopId').val().trim().length == 0
				|| $('#shopId').val().trim() == activeType.ALL) {
			msg = format(msgErr_required_field, 'đơn vị');
		}
		if (msg.length > 0) {
			$('#errMsg').html(msg).show();
			return false;
		}
		var dataModel = new Object();
		dataModel.shopId = $('#shopId').val().trim();
		ReportUtils.exportReport('/report/sales-revenue/view-dayreport',
				dataModel);
	},
	exportPoInDate : function() {
		var msg = '';
		$('#errMsg').html('').hide();
		if ($('#shopId').val().trim().length == 0
				|| $('#shopId').val().trim() == activeType.ALL) {
			msg = format(msgErr_required_field, 'đơn vị');
		}
		var chot = '';
		if (msg == "") {
			msg = Utils.getMessageOfRequireCheck('fDate', 'Từ ngày', false);
			chot = 'fDate';
		}
		if (msg == "") {
			ms = Utils.getMessageOfRequireCheck('tDate', 'Đến ngày', false);
			chot = 'tDate';
		}
		if (msg.length > 0) {
			$('#errMsg').html(msg).show();
			$('#' + chot).focus();
			return false;
		}
		if (msg.length == 0) {
			msg = Utils.getMessageOfInvalidFormatDate('fDate', 'Từ ngày');
		}
		if (msg.length == 0) {
			msg = Utils.getMessageOfInvalidFormatDate('tDate', 'Đến ngày');
		}
		var fDate = $('#fDate').val();
		var tDate = $('#tDate').val();
		if (!Utils.compareDate(fDate, tDate)) {
			msg = msgErr_fromdate_greater_todate;
			$('#fDate').focus();
		}
		if (msg.length > 0) {
			$('#errMsg').html(msg).show();
			return false;
		}
		var dataModel = new Object();
		dataModel.shopId = $('#shopId').val().trim();
		dataModel.fromDate = $('#fDate').val().trim();
		dataModel.toDate = $('#tDate').val().trim();
		dataModel.staffCode = $('#staffCode').val().trim();
		ReportUtils.exportReport('/report/sales-revenue/poPage', dataModel);
	},

	exportBHTSP : function() {
		$('#errMsg').html('').hide();
		var msg = '';
		if ($('#shopCode').val().trim().length == 0
				|| $('#shopCode').val().trim() == activeType.ALL) {
			msg = format(msgErr_required_field, 'đơn vị');
		}

		if (msg.length == 0) {
			msg = Utils.getMessageOfRequireCheck('fDate', 'Từ ngày');
		}
		if (msg.length == 0) {
			msg = Utils.getMessageOfInvalidFormatDate('fDate', 'Từ ngày');
		}
		if (msg.length == 0) {
			msg = Utils.getMessageOfRequireCheck('tDate', 'Đến ngày');
		}
		if (msg.length == 0) {
			msg = Utils.getMessageOfInvalidFormatDate('tDate', 'Đến ngày');
		}
		var fDate = $('#fDate').val();
		var tDate = $('#tDate').val();
		if (!Utils.compareDate(fDate, tDate)) {
			msg = msgErr_fromdate_greater_todate;
			$('#fDate').focus();
		}
		var currentTime = new Date();
		var month = currentTime.getMonth() + 1;
		var day = currentTime.getDate();
		var year = currentTime.getFullYear();
		if (!Utils.compareDate(fDate, day + '/' + month + '/' + year)) {
			msg = 'Từ ngày không được lớn hơn ngày hiện tại';
			$('#fDate').focus();
		}
		if (msg.length > 0) {
			$('#errMsg').html(msg).show();
			return false;
		}
		var data = new Object();
		if ($('input[name=formatType]:radio:checked').length > 0
				&& $('input[name=formatType]:radio:checked').val() != undefined) {
			data.formatType = $('input[name=formatType]:radio:checked').val()
					.toUpperCase();
		}

		data.customerCode = $('#customerCode').val().trim();
		data.fromDate = $('#fDate').val().trim();
		data.toDate = $('#tDate').val().trim();
		data.staffCode = $('#staffCode').val().trim();
		data.shopId = $('#reportContainer input.combo-value').val().trim();

		$
				.getJSON(
						"/report/are-you-shop?shopId=" + data.shopId,
						function(result) {

							if (!result.isNPP) {
								msg = 'Đơn vị chọn phải là nhà phân phối';
								$('#errMsg').html(msg).show();

							} else {
								var kData = $.param(data, true);
								$('#divOverlay').show();
								$
										.ajax({
											type : "POST",
											url : "/report/sales-revenue/bhtsp/export",
											data : (kData),
											dataType : "json",
											success : function(data) {
												$('#divOverlay').hide();
												if (!data.error) {
													if (data.haveData) {
														window.location.href = data.path;
														setTimeout(
																function() { // Set
																	// timeout
																	// để
																	// đảm
																	// bảo
																	// file
																	// load
																	// lên
																	// hoàn
																	// tất
																	CommonSearch
																			.deleteFileExcelExport(data.path);
																}, 2000);
													} else {
														msg = 'Không có dữ liệu để xuất';

														if (data.validate) {
															msg = data.errMsg;
														}

														$('#errMsg').html(msg)
																.show();
													}
												} else {
													$('#errMsg').html(
															data.errMsg).show();
												}
											},
											error : function(XMLHttpRequest,
													textStatus, errorDivThrown) {
												$('#divOverlay').hide();
												StockIssued.xhrExport = null;
											}
										});
							}

						});

		// ReportUtils.exportReport('/report/sales-revenue/view-dayreport',dataModel);

		return false;
	},

	/**
	 * DS3.11-Bao cao tong hop chi tra hang trung bay
	 * 
	 * @author hunglm16
	 * @since January 17,2014
	 */
	exportBCTHCTHTB_DS311 : function() {
		var msg = '';
		$('#errMsg').html('').hide();

		if (msg.length == 0 && $('.combo-value').val() == undefined
				&& $('.combo-value').val().length == 0) {
			msg = "Chưa chọn Đơn Vị";
		}
		if (msg.length == 0 && $('#isNPP').val().trim() === "false") {
			msg = 'Đơn vị chọn phải là nhà phân phối';
		}
		if (msg.length == 0) {
			msg = Utils.getMessageOfRequireCheck('fullYearIn10', 'Năm');
		}
		if (msg.length > 0) {
			$('#errMsg').html(msg).show();
			return false;
		}
		var dataModel = new Object();
		dataModel.shopId = $('.combo-value').val();
		dataModel.year = encodeChar($('#fullYearIn10').val());
		dataModel.lstDisProCode = $('#multiChoiceCTTB').val();
		dataModel.customerCode = $('#customerCode').val();
		if ($('input[name=formatType]:radio:checked').length > 0
				&& $('input[name=formatType]:radio:checked').val() != undefined) {
			dataModel.formatType = $('input[name=formatType]:radio:checked')
					.val().toUpperCase();
		}
		ReportUtils.exportReport('/report/sales-revenue/DS3-11/export',
				dataModel);
	},

	/**
	 * DS3-Báo cáo danh sách đơn hàng trong ngày theo NVBH
	 * 
	 * @author hunglm16
	 * @since January 25,2014
	 */
	exportBCDSDHTNTNVBH_DS31 : function() {
		var msg = '';
		$('#errMsg').html('').hide();

		if (msg.length == 0 && $('.combo-value').val() == undefined
				&& $('.combo-value').val().length == 0) {
			msg = "Chưa chọn Đơn Vị";
		}
		if (msg.length == 0 && $('#isNPP').val().trim() === "false") {
			msg = 'Đơn vị chọn phải là nhà phân phối';
		}
		if (msg.length == 0) {
			msg = Utils.getMessageOfRequireCheck('fDate', 'Từ ngày');
		}
		if (msg.length == 0) {
			msg = Utils.getMessageOfInvalidFormatDate('fDate', 'Từ ngày');
		}
		if (msg.length == 0) {
			msg = Utils.getMessageOfRequireCheck('tDate', 'Đến ngày');
		}
		if (msg.length == 0) {
			msg = Utils.getMessageOfInvalidFormatDate('tDate', 'Đến ngày');
		}
		var fDate = $('#fDate').val();
		var tDate = $('#tDate').val();
		if (msg.length == 0 && !Utils.compareDate(fDate, tDate)) {
			msg = msgErr_fromdate_greater_todate;
			$('#fDate').focus();
		}
		var currentTime = new Date();
		var month = currentTime.getMonth() + 1;
		var day = currentTime.getDate();
		var year = currentTime.getFullYear();
		if (msg.length == 0
				&& !Utils.compareDate(fDate, day + '/' + month + '/' + year)) {
			msg = 'Từ ngày không được lớn hơn ngày hiện tại';
			$('#fDate').focus();
		}

		if (msg.length > 0) {
			$('#errMsg').html(msg).show();
			return false;
		}

		var dataModel = new Object();
		dataModel.shopId = $('.combo-value').val();
		dataModel.lstStaffCode = $('#staffCode').val().trim();
		dataModel.fromDate = $('#fDate').val().trim();
		dataModel.toDate = $('#tDate').val().trim();
		ReportUtils.exportExcel('/report/sales-revenue/DS3-dsdhtntnvbh/export',
				dataModel, 'errMsg');
	},

	exportTDBH7 : function() {
		var msg = '';
		$('#errMsg').hide();
		msg = Utils.getMessageOfRequireCheck('shopId', 'Đơn vị');
		$('#shop').focus();

		if (msg.length == 0) {
			msg = Utils.getMessageOfRequireCheck('fDate', 'Từ ngày');
		}
		if (msg.length == 0) {
			msg = Utils.getMessageOfRequireCheck('tDate', 'Đến ngày');
		}
		var fDate = $('#fDate').val().trim();
		var tDate = $('#tDate').val().trim();
		if (msg.length == 0 && !Utils.compareCurrentDate(fDate)) {
			msg = 'Từ ngày phải nhỏ hơn hoặc bằng ngày hiện tại';
			$('#fDate').focus();
		}

		if (msg.length == 0 && !Utils.compareDate(fDate, tDate)) {
			msg = 'Từ ngày phải nhỏ hơn hoặc bằng đến ngày';
			$('#fDate').focus();
		}

		if (msg.length > 0) {
			$('#errMsg').html(msg).show();
			return false;
		}

		var dataModel = new Object();
		dataModel.shopId = $('#shopId').val().trim();
		dataModel.fromDate = fDate;
		dataModel.toDate = tDate;
		if ($('input[name=formatType]:radio:checked').length > 0
				&& $('input[name=formatType]:radio:checked').val() != undefined) {
			dataModel.formatType = $('input[name=formatType]:radio:checked').val().toUpperCase();
		}
		ReportUtils.exportReport('/report/sales-revenue/tdbh_7_2_7/export', dataModel);
		return false;
	},

	exportTDBH8 : function() {
		var msg = '';
		$('#errMsg').hide();
		msg = Utils.getMessageOfRequireCheck('shopId', 'Đơn vị');
		$('#shop').focus();

		if (msg.length == 0) {
			msg = Utils.getMessageOfRequireCheck('fDate', 'Từ ngày');
		}
		var fDate = $('#fDate').val().trim();
		if (msg.length == 0 && !Utils.compareCurrentDate(fDate)) {
			msg = 'Từ ngày phải nhỏ hơn hoặc bằng ngày hiện tại';
			$('#fDate').focus();
		}

		if (msg.length > 0) {
			$('#errMsg').html(msg).show();
			return false;
		}

		var dataModel = new Object();
		dataModel.shopId = $('#shopId').val().trim();
		dataModel.staffCode = $('#staffCode').val().trim();
		dataModel.fromDate = fDate;

		if ($('input[name=formatType]:radio:checked').length > 0
				&& $('input[name=formatType]:radio:checked').val() != undefined) {
			dataModel.formatType = $('input[name=formatType]:radio:checked')
					.val().toUpperCase();
		}
		ReportUtils.exportReport('/report/sales-revenue/tdbh_7_2_8/export', dataModel);
		return false;
	},
	
	exportTDTTCN: function() {
		var msg = '';
		$('#errMsg').hide();

		if (msg.length == 0) {
			msg = Utils.getMessageOfRequireCheck('fDate', 'Từ ngày');
		}
		if (msg.length == 0) {
			msg = Utils.getMessageOfInvalidFormatDate('fDate', 'Từ ngày');
		}
		if (msg.length == 0) {
			msg = Utils.getMessageOfRequireCheck('tDate', 'Đến ngày');
		}
		if (msg.length == 0) {
			msg = Utils.getMessageOfInvalidFormatDate('tDate', 'Đến ngày');
		}
		var fDate = $('#fDate').val().trim();
		var tDate = $('#tDate').val().trim();
		if (msg.length == 0 && !Utils.compareCurrentDate(fDate)) {
			msg = 'Từ ngày phải nhỏ hơn hoặc bằng ngày hiện tại';
			$('#fDate').focus();
		}

		if (msg.length == 0 && !Utils.compareDate(fDate, tDate)) {
			msg = 'Từ ngày phải nhỏ hơn hoặc bằng đến ngày';
			$('#fDate').focus();
		}

		if (msg.length > 0) {
			$('#errMsg').html(msg).show();
			return false;
		}

		var dataModel = new Object();
		dataModel.type = $('#type').val().trim();
		dataModel.fromDate = fDate;
		dataModel.toDate = tDate;
		dataModel.lstNVBHCode = $('#staffCode').val().trim();
		dataModel.lstNVGHCode = $('#delivery').val().trim();
		dataModel.lstCustomerCode = $('#customerCode').val().trim();
		dataModel.checkExport = $('#checkExport').val().trim();
		
		ReportUtils.exportReport('/report/sales-revenue/tdbh-7_2_5/export', dataModel);
	},
	exportPTDTBHNPP: function(){
		
		var msg = '';
		var multiShop = $("#shop").data("kendoMultiSelect");
		var dataShop = multiShop.dataItems();
		$('#errMsg').html('').hide();
		if(dataShop==null||dataShop.length==0){
			msg = format(msgErr_required_field,'Đơn vị');
		}
		var fDate = $('#fDate').val();
		var tDate = $('#tDate').val();
		if(msg.length == 0){
			msg = Utils.getMessageOfRequireCheck('fDate','Từ ngày');
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfInvalidFormatDate('fDate','Từ ngày');
		}
		if(msg.length == 0){
			if(!Utils.compareCurrentDate(fDate)){
				msg = "Từ ngày phải bé hơn hoặc bằng ngày hiện tại.";
			}
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfRequireCheck('tDate','Đến ngày');
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfInvalidFormatDate('tDate','Đến ngày');
		}
		if(msg.length == 0){
			if(!Utils.compareCurrentDate(tDate)){
				msg = "Đến ngày phải bé hơn hoặc bằng ngày hiện tại.";
			}
		}
		if(msg.length == 0){			
			if(!Utils.compareDate(fDate, tDate)){
				msg = msgErr_fromdate_greater_todate;
			}
		}
		
		if(msg.length > 0){
			$('#errMsg').html(msg).show();
			return false;
		}
		var dataModel = new Object();
		if(dataShop!=null && dataShop.length>0){
			var lstShop = dataShop[0].id;
			for(var i=1;i<dataShop.length;i++){
				lstShop += "," + dataShop[i].id;
			}
			dataModel.strListShopId = lstShop;
		}
		//dataModel.shopId = $('#shopId').val().trim();				
		if(dataShop!=null && dataShop.length>0){
			var lstShop = dataShop[0].id;
			for(var i=1;i<dataShop.length;i++){
				lstShop += "," + dataShop[i].id;
			}
			dataModel.listShopId = lstShop;
		}
		dataModel.fromDate = $('#fDate').val().trim();
		dataModel.toDate = $('#tDate').val().trim();
		dataModel.shopCode = $('#shopCode').val().trim();		
		ReportUtils.exportReport('/report/sales-revenue/tdbh-7_2_12/export',dataModel);
	},
	/**
	 * @author sangtn
	 * @since 13/05/2014
	 * @description Xuat bao cao 7.2.13. Phan tich doanh thu ban hang theo NVBH 
	 * */
	export7_2_13: function() {
		var msg = '';
		var multiShop = $("#shop").data("kendoMultiSelect");
		var dataShop = multiShop.dataItems();
		$('#errMsg').html('').hide();
		if(dataShop==null||dataShop.length==0){
			msg = format(msgErr_required_field,'Đơn vị');
		}
		var fDate = $('#fDate').val();
		var tDate = $('#tDate').val();
		if(msg.length == 0){
			msg = Utils.getMessageOfRequireCheck('fDate','Từ ngày');
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfInvalidFormatDate('fDate','Từ ngày');
		}
		if(msg.length == 0){
			if(!Utils.compareCurrentDate(fDate)){
				msg = "Từ ngày phải bé hơn hoặc bằng ngày hiện tại.";
			}
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfRequireCheck('tDate','Đến ngày');
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfInvalidFormatDate('tDate','Đến ngày');
		}
		if(msg.length == 0){
			if(!Utils.compareCurrentDate(tDate)){
				msg = "Đến ngày phải bé hơn hoặc bằng ngày hiện tại.";
			}
		}
		if(msg.length == 0){			
			if(!Utils.compareDate(fDate, tDate)){
				msg = msgErr_fromdate_greater_todate;
			}
		}
		
		if(msg.length > 0){
			$('#errMsg').html(msg).show();
			return false;
		}
		var dataModel = new Object();
		if (dataShop != null && dataShop.length > 0) {
			var lstShop = dataShop[0].shopId;
			for (var i = 1; i < dataShop.length; i++) {
				lstShop += "," + dataShop[i].shopId;
			}
			dataModel.strListShopId = lstShop;
		}
		dataModel.fromDate = $('#fDate').val().trim();
		dataModel.toDate = $('#tDate').val().trim();
		//dataModel.shopCode = $('#shopCode').val().trim();
		ReportUtils.exportReport('/report/sales-revenue/tdbh-7_2_13/export', dataModel);
	},
	/**
	 * @author vuonghn
	 * @since 13/05/2014
	 * @description Xuat bao cao 7.2.14. Phan tich doanh thu ban hang theo NVBH 
	 * */
	export7_2_14: function() {
		var msg = '';
		var multiShop = $("#shop").data("kendoMultiSelect");
		var dataShop = multiShop.dataItems();
		$('#errMsg').html('').hide();
		if(dataShop==null||dataShop.length==0){
			msg = format(msgErr_required_field,'Đơn vị');
		}
		var fDate = $('#fDate').val();
		var tDate = $('#tDate').val();
		if(msg.length == 0){
			msg = Utils.getMessageOfRequireCheck('fDate','Từ ngày');
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfInvalidFormatDate('fDate','Từ ngày');
		}
		if(msg.length == 0){
			if(!Utils.compareCurrentDate(fDate)){
				msg = "Từ ngày phải bé hơn hoặc bằng ngày hiện tại.";
			}
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfRequireCheck('tDate','Đến ngày');
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfInvalidFormatDate('tDate','Đến ngày');
		}
		if(msg.length == 0){
			if(!Utils.compareCurrentDate(tDate)){
				msg = "Đến ngày phải bé hơn hoặc bằng ngày hiện tại.";
			}
		}
		if(msg.length == 0){			
			if(!Utils.compareDate(fDate, tDate)){
				msg = msgErr_fromdate_greater_todate;
			}
		}
		
		if(msg.length > 0){
			$('#errMsg').html(msg).show();
			return false;
		}
		var dataModel = new Object();
		if(dataShop!=null && dataShop.length>0){
			var lstShop = dataShop[0].shopId;
			for(var i=1;i<dataShop.length;i++){
				lstShop += "," + dataShop[i].shopId;
			}
			dataModel.strListShopId = lstShop;
		}
		dataModel.fromDate = $('#fDate').val().trim();
		dataModel.toDate = $('#tDate').val().trim();
		ReportUtils.exportReport('/report/sales-revenue/tdbh-7_2_14/export', dataModel);
	},
	// vuongmq: PTDTBHSKU 7_2_15
	exportPTDTBHSKU7_1_15: function() {
		var msg = '';
		var multiShop = $("#shop").data("kendoMultiSelect");
		var dataShop = multiShop.dataItems();
		$('#errMsg').html('').hide();
		if(dataShop==null||dataShop.length==0){
			msg = format(msgErr_required_field,'Đơn vị');
		}
		$('#shop').focus();
		if (msg.length == 0) {
			msg = Utils.getMessageOfRequireCheck('fromDate', 'Từ ngày');
		}
		if (msg.length == 0) {
			msg = Utils.getMessageOfRequireCheck('toDate', 'Đến ngày');
		}
		if (msg.length == 0) {
			msg = Utils.getMessageOfInvalidFormatDate('fromDate', 'Từ ngày');
		}
		if (msg.length == 0) {
			msg = Utils.getMessageOfInvalidFormatDate('toDate', 'Từ ngày');
		}
		var fDate = $('#fromDate').val().trim();
		var tDate = $('#toDate').val().trim();
		if (msg.length == 0 && !Utils.compareCurrentDate(fDate)) {
			msg = 'Từ ngày phải nhỏ hơn hoặc bằng ngày hiện tại';
			$('#fDate').focus();
		}

		if (msg.length == 0 && !Utils.compareDate(fDate, tDate)) {
			msg = 'Từ ngày phải nhỏ hơn hoặc bằng đến ngày';
			$('#fDate').focus();
		}

		if (msg.length > 0) {
			$('#errMsg').html(msg).show();
			return false;
		}

		var dataModel = new Object();
		if(dataShop!=null && dataShop.length>0){
			var lstShop = dataShop[0].shopId;
			for(var i=1;i<dataShop.length;i++){
				lstShop += "," + dataShop[i].shopId;
			}
			dataModel.strListShopId = lstShop;
		}
		dataModel.fromDate = fDate;
		dataModel.toDate = tDate;
		ReportUtils.exportReport('/report/sales-revenue/ptdtbh-sku_7_2_15/export', dataModel);
	},
	// vuongmq:  vuongmq: 13/08/2013, bao cao don hang tra theo gia tri
	exportBCDHT_THEOGIATRI: function() {
		var msg = '';
		var multiShop = $("#shop").data("kendoMultiSelect");
		var dataShop = multiShop.dataItems();
		$('#errMsg').html('').hide();
		if(dataShop==null||dataShop.length==0){
			msg = format(msgErr_required_field,'Đơn vị');
		}
		$('#shop').focus();
		if (msg.length == 0) {
			msg = Utils.getMessageOfRequireCheck('fromDate', 'Từ ngày');
		}
		if (msg.length == 0) {
			msg = Utils.getMessageOfRequireCheck('toDate', 'Đến ngày');
		}
		if (msg.length == 0) {
			msg = Utils.getMessageOfInvalidFormatDate('fromDate', 'Từ ngày');
		}
		if (msg.length == 0) {
			msg = Utils.getMessageOfInvalidFormatDate('toDate', 'Từ ngày');
		}
		var fDate = $('#fromDate').val().trim();
		var tDate = $('#toDate').val().trim();
		if (msg.length == 0 && !Utils.compareCurrentDate(fDate)) {
			msg = 'Từ ngày phải nhỏ hơn hoặc bằng ngày hiện tại';
			$('#fDate').focus();
		}

		if (msg.length == 0 && !Utils.compareDate(fDate, tDate)) {
			msg = 'Từ ngày phải nhỏ hơn hoặc bằng đến ngày';
			$('#fDate').focus();
		}

		if (msg.length > 0) {
			$('#errMsg').html(msg).show();
			return false;
		}

		var dataModel = new Object();
		if(dataShop!=null && dataShop.length>0){
			var lstShop = dataShop[0].id;
			for(var i=1;i<dataShop.length;i++){
				lstShop += "," + dataShop[i].id;
			}
			dataModel.strListShopId = lstShop;
		}
		dataModel.fromDate = fDate;
		dataModel.toDate = tDate;
		//ReportUtils.exportReport('/report/sales-revenue/bcdht_theogiatri/export', dataModel);
		ReportUtils.exportReport('/report/sales-revenue/bcdht_theogiatri/export', dataModel);
	},
	// vuongmq: vuongmq: 13/08/2013, bao cao don hang tra theo tong dong hang
	exportBCDHT_THEOTONGDONHANG: function() {
		var msg = '';
		var multiShop = $("#shop").data("kendoMultiSelect");
		var dataShop = multiShop.dataItems();
		$('#errMsg').html('').hide();
		if(dataShop==null||dataShop.length==0){
			msg = format(msgErr_required_field,'Đơn vị');
		}
		$('#shop').focus();
		if (msg.length == 0) {
			msg = Utils.getMessageOfRequireCheck('fromDate', 'Từ ngày');
		}
		if (msg.length == 0) {
			msg = Utils.getMessageOfRequireCheck('toDate', 'Đến ngày');
		}
		if (msg.length == 0) {
			msg = Utils.getMessageOfInvalidFormatDate('fromDate', 'Từ ngày');
		}
		if (msg.length == 0) {
			msg = Utils.getMessageOfInvalidFormatDate('toDate', 'Từ ngày');
		}
		var fDate = $('#fromDate').val().trim();
		var tDate = $('#toDate').val().trim();
		if (msg.length == 0 && !Utils.compareCurrentDate(fDate)) {
			msg = 'Từ ngày phải nhỏ hơn hoặc bằng ngày hiện tại';
			$('#fDate').focus();
		}

		if (msg.length == 0 && !Utils.compareDate(fDate, tDate)) {
			msg = 'Từ ngày phải nhỏ hơn hoặc bằng đến ngày';
			$('#fDate').focus();
		}

		if (msg.length > 0) {
			$('#errMsg').html(msg).show();
			return false;
		}

		var dataModel = new Object();
		if(dataShop!=null && dataShop.length>0){
			var lstShop = dataShop[0].id;
			for(var i=1;i<dataShop.length;i++){
				lstShop += "," + dataShop[i].id;
			}
			dataModel.strListShopId = lstShop;
		}
		dataModel.fromDate = fDate;
		dataModel.toDate = tDate;
		//ReportUtils.exportReport('/report/sales-revenue/bcdht_theotongdonhang/export', dataModel);
		ReportUtils.exportReport('/report/sales-revenue/bcdht_theotongdonhang/export', dataModel);
	}
};
/*
 * END OF FILE - /web/web/resources/scripts/business/shop-report/vnm.sales-revenue-report.js
 */

/*
 * START OF FILE - /web/web/resources/scripts/business/shop-report/vnm.supervise-customer-report.js
 */
var SuperviseCustomer = {	
		/* BAO CAO NHAN VIEN KHONG BAT MAY */
	mapNVGS : null,
	mapNVBH : null,
	mapTBHV : null,
	mapProduct : null,
	mapCustomer : null,
	mapCycle: new Map(),
	_lstProduct : null,
	_lstCat : null,
	_lstSubCat : null,
	exportBCNVKBM: function(){
		var msg = '';
		$('#errMsg').html('').hide();
		var multiShop = $("#shop").data("kendoMultiSelect");
		var dataShop = multiShop.dataItems();
		if(dataShop==null||dataShop.length==0){
			msg = format(msgErr_required_field,'đơn vị');
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfRequireCheck('fDate','Từ ngày');
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfInvalidFormatDate('fDate','Từ ngày');
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfRequireCheck('tDate','Từ ngày');
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfInvalidFormatDate('tDate','�?ến ngày');
		}
		var fDate = $('#fDate').val();
		var tDate = $('#tDate').val();
		var curDate = ReportUtils.getCurrentDateString();
		if(msg.length == 0 && !Utils.compareDate(fDate, curDate)){
			msg = 'Từ ngày phải nh�? hơn hoặc bằng ngày hiện tại';
			$('#fDate').focus();
		}	
		if(msg.length == 0 && !Utils.compareDate(tDate, curDate)){
			msg = '�?ến ngày phải nh�? hơn hoặc bằng ngày hiện tại';
			$('#tDate').focus();
		}
		if(!Utils.compareDate(fDate, tDate)){
			msg = msgErr_fromdate_greater_todate;		
			$('#fDate').focus();
		}
		if(msg.length > 0){
			$('#errMsg').html(msg).show();
			return false;
		}
		var staffCode = $('#staffCode').val().trim();
		var dataModel = new Object();
		if(dataShop!=null&&dataShop.length>0){
			var lstShopId = dataShop[0].id;
			for(var i=1;i<dataShop.length;i++){
				lstShopId += ","+dataShop[i].id;
			}
			dataModel.strListShopId = lstShopId;
		}
		dataModel.fromDate = $('#fDate').val().trim();
		dataModel.toDate = $('#tDate').val().trim();
		dataModel.staffCode = staffCode;
		ReportUtils.exportReport('/report/superivse/bc-nvkbm/export',dataModel);
	},
	/**
	 * Load ds san pham
	 * 
	 * @author hoanv25
	 * @since July 10, 2015
	 * */
	loadComboProduct:function(url){
		$("#lstProduct").kendoMultiSelect({
			dataSource: {
				transport: {
	                read: {
	                    dataType: "json",
	                    url: url
	                }
	            }
			},
			filter: "contains",
	        dataTextField: "nvbhName",
	        dataValueField: "nvbhId",
			itemTemplate: function(data, e, s, h, q) {
				return '<div class="tree-vnm" node-id="'+Utils.XSSEncode(data.nvbhId)+'" style="width:180px"><span class="tree-vnm-text">'+Utils.XSSEncode(data.nvbhCode)+' - '+Utils.XSSEncode(data.nvbhName) +'</span></div>';	
			},
	        tagTemplate:  '#: data.nvbhName #',
	        value: [$('#nvbhId').val()]		       
	    });	 
	},
	/**
	 * Bc Ke hoach tieu thu
	 * 
	 * @author longnh15
	 * @since July 10, 2015
	 * */
	exportBCKHTT: function(){
		var msg = '';
		$('#errMsg').html('').hide();
		var multiProduct = $("#lstProduct").data("kendoMultiSelect");
		var dataProduct = null;
		if (multiProduct != null) {
			dataProduct = multiProduct.dataItems();
		}
		
		if ($('#shopId').val().trim().length == 0 || $('#shopId').val().trim() == activeType.ALL) {
			msg = format(msgErr_required_field,'đơn vị');
		}
		if (msg.length == 0) {
			msg = Utils.getMessageOfRequireCheck('fDate','Từ ngày');
		}
		if (msg.length == 0) {
			msg = Utils.getMessageOfRequireCheck('tDate','Đến ngày');
		}
		if (msg.length > 0) {
			$('#errMsg').html(msg).show();
			return false;
		}
		if (!checkDateForCycle()) {
			return;
		}
		var lstProductId = [];
		var lstProductName = [];
		for(var i=0;i<dataProduct.length;i++){
			lstProductId.push(dataProduct[i].nvbhId);
			lstProductName.push(dataProduct[i].nvbhName)
		}
		var dataModel = new Object();
		dataModel.strListShopId=$('#shopId').val();
		dataModel.fromDate = $('#fDate').val().trim();
		dataModel.toDate = $('#tDate').val().trim();
		dataModel.cycleId = $('#cycle').val();
		dataModel.lstProduct = lstProductId.join(',');
		dataModel.lstCat = $('#category').val();
		dataModel.lstSubCat = $('#sub_category').val();
		ReportUtils.exportReport('/report/superivse/bckhtt-export',dataModel,'errMsg');

	},
	//vuongmq - Bao cao hinh anh
	exportBCVT9: function(){
		var msg = '';
		/*var multiShop = $("#shop").data("kendoMultiSelect");
		var dataShop = multiShop.dataItems();*/
		$('#errMsg').html('').hide(); 
		
		var shopKendo = $("#shop").data("kendoMultiSelect");
		var lstShopId = shopKendo.value();
		if(lstShopId.length <= 0){
			msg = format(msgErr_required_field,'�?ơn vị');
		}
		
		if(msg.length == 0){
			msg = Utils.getMessageOfRequireCheck('fromDate','Từ ngày');
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfInvalidFormatDate('fromDate','Từ ngày');
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfRequireCheck('toDate','�?ến ngày');
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfInvalidFormatDate('toDate','�?ến ngày');
		}
		var fDate = $('#fromDate').val();
		var tDate = $('#toDate').val();
		var curDate = ReportUtils.getCurrentDateString();
		if(msg.length == 0 && !Utils.compareDate(fDate, curDate)){
			msg = 'Từ ngày phải nh�? hơn hoặc bằng ngày hiện tại';
			$('#fromDate').focus();
		}	
		if(msg.length == 0 && !Utils.compareDate(tDate, curDate)){
			msg = '�?ến ngày phải nh�? hơn hoặc bằng ngày hiện tại';
			$('#toDate').focus();
		}
		if(!Utils.compareDate(fDate, tDate)) {
			msg ='Từ ngày phải nh�? hơn đến ngày.';
			$('#fromDate').focus();
		}

		var imageType = $('#imageType').val();
		if(imageType == null && imageType.length <= 0) {
			msg = format(msgErr_required_field,'Loại Hình ảnh');
		}
		var displayProgramId = $('#displayProgramId').val();
		if(imageType != null && imageType.length > 0 && (imageType.indexOf(5) != -1 || imageType.indexOf("5") != -1)) {
			if(displayProgramId == null && displayProgramId.length <= 0) {
				msg = format(msgErr_required_field,'Chương trình');
			}
		}
		
		if(msg.length > 0){
			$('#errMsg').html(msg).show();
			return false;
		}
		
		var dataModel = new Object();

		dataModel.strListShopId = lstShopId.toString().trim();
		dataModel.staffCode = $('#staffCode').val().trim(); 
		dataModel.fromDate = $('#fromDate').val();
		dataModel.toDate = $('#toDate').val();
		if(imageType != null && imageType.length > 0) {
			dataModel.imageType = imageType;
		}
		if(displayProgramId != null && displayProgramId.length > 0) {
			dataModel.displayProgramId = displayProgramId;
		}
		ReportUtils.exportReport('/report/superivse/bcha-vt9/export',dataModel);
	},
	
	exportKD101:function(){
		var msg = '';
		$('#errMsg').html('').hide();		
		if(msg.length == 0){
			msg = Utils.getMessageOfRequireCheck('fDate','Từ ngày');
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfInvalidFormatDate('fDate','Từ ngày');
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfRequireCheck('tDate','Từ ngày');
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfInvalidFormatDate('tDate','�?ến ngày');
		}
		var fDate = $('#fDate').val();
		var tDate = $('#tDate').val();
		if(!Utils.compareDate(fDate, tDate)){
			msg = msgErr_fromdate_greater_todate;		
			$('#fDate').focus();
		}
		if(msg.length > 0){
			$('#errMsg').html(msg).show();
			return false;
		}
		var dataModel = new Object();
		dataModel.fromDate = $('#fDate').val().trim();
		dataModel.toDate = $('#tDate').val().trim();
		ReportUtils.exportReport('/report/superivse/bccrmkd101/export',dataModel);
	},		
		/* BAO CAO NHAN VIEN KHONG GHE THAM KHACH HANG TRONG TUYEN */
	exportBCNVKGTKHTT: function() {
		var msg = '';
		var multiShop = $("#shop").data("kendoMultiSelect");
		var dataShop = multiShop.dataItems();
		$('#errMsg').html('').hide();
		
		/*if($('#shop').combotree('getValue').trim().length == 0 || $('#shop').combotree('getValue').trim() == activeType.ALL){
			msg = format(msgErr_required_field,'đơn vị');
		}
		
		if(msg.length == 0){
			msg = Utils.getMessageOfRequireCheck('shopCode','�?ơn vị');
		}*/
		
		if(dataShop.length <= 0){
			msg = format(msgErr_required_field,'�?ơn vị');
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfRequireCheck('fDate','Từ ngày');
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfRequireCheck('tDate','Từ ngày');
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfInvalidFormatDate('fDate','Từ ngày');
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfInvalidFormatDate('tDate','�?ến ngày');
		}
		/*if(msg.length == 0){
			msg = Utils.getMessageOfSpecialCharactersValidate('staffCode','NPP',Utils._CODE);
		}	
		if(msg.length == 0){
			msg = Utils.getMessageOfSpecialCharactersValidate('staffOwnerCode','NPP',Utils._CODE);
		}*/
		var fDate = $('#fDate').val();
		var tDate = $('#tDate').val();
		if(!Utils.compareDate(fDate, tDate)){
			msg = msgErr_fromdate_greater_todate;		
			$('#fDate').focus();
		}
		if(msg.length > 0){
			$('#errMsg').html(msg).show();
			return false;
		}
		var dataModel = new Object();
		if(dataShop!=null && dataShop.length>0){
			var lstShop = dataShop[0].id;
			for(var i=1;i<dataShop.length;i++){
				lstShop += "," + dataShop[i].id;
			}
			dataModel.strListShopId = lstShop;
		}
		//dataModel.shopId = $('#shopCode').val().trim();
		dataModel.fromDate = $('#fDate').val().trim();
		dataModel.toDate = $('#tDate').val().trim();
		dataModel.staffCode = $('#staffCode').val().trim();
		dataModel.staffOwnerCode = $('#staffOwnerCode').val().trim();
		//ReportUtils.exportReport('/report/superivse/export-bcnvkgtkhtt',dataModel);
		var kData = $.param(dataModel,true);
		$('#divOverlay').show();		
		$.ajax({
			type : "POST",
			url : "/report/superivse/export-bcnvkgtkhtt",
			data :(kData),
			dataType: "json",
			success : function(data) {
				$('#divOverlay').hide();				
				if(!data.error) {
					if(data.hasData != null && data.hasData != undefined && data.hasData == true) {
						window.location.href = data.path;
						setTimeout(function(){ //Set timeout để đảm bảo file load lên hoàn tất
		                    CommonSearch.deleteFileExcelExport(data.path);
						},2000);
					} else {
						$('#errMsg').html('Không có dữ liệu để xuất').show();
					}
				} else {
					$('#errMsg').html(data.errMsg).show();					
				}
			},
			error:function(XMLHttpRequest, textStatus, errorDivThrown) {
				$('#divOverlay').hide();
				StockIssued.xhrExport = null;
			}
		});
	},
	
	reportNVBHDSVM: function(){//BAO CAO DI MUON VE SOM CUA NVBH
		var msg = '';
		$('#errMsg').html('').hide();
		var multiShop = $('#shop').data("kendoMultiSelect");
		var dataShop = multiShop.dataItems();
		if(dataShop==null||dataShop.length==0){
			msg = format(msgErr_required_field,'đơn vị');
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfRequireCheck('fDate','Từ ngày');
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfInvalidFormatDate('fDate','Từ ngày');
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfRequireCheck('tDate','Từ ngày');
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfInvalidFormatDate('tDate','�?ến ngày');
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfRequireCheck('fromHour','Từ gi�?');
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfSpecialCharactersValidateEx('fromHour','Từ gi�?',Utils._TF_NUMBER);
		}
		if(msg.length==0){
			var value = $('#fromHour').val();
			if(parseInt(value,10) <0 || parseInt(value,10) >23){
				msg = "Từ gi�? phải từ 0 đến 23.";
				$('#fromHour').focus();
			}
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfRequireCheck('fromMinute','Từ phút');
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfSpecialCharactersValidateEx('fromMinute','Từ phút',Utils._TF_NUMBER);
		}
		if(msg.length==0){
			var value = $('#fromMinute').val();
			if(parseInt(value,10) <0 || parseInt(value,10) >59){
				msg = "Từ phút phải từ 0 đến 59.";
				$('#fromMinute').focus();
			}
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfRequireCheck('toHour','�?ến gi�?');
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfSpecialCharactersValidateEx('toHour','�?ến gi�?',Utils._TF_NUMBER);
		}
		if(msg.length==0){
			var value = $('#toHour').val();
			if(parseInt(value,10) <0 || parseInt(value,10) >23){
				msg = "�?ến gi�? phải từ 0 đến 23.";
				$('#toHour').focus();
			}
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfRequireCheck('toMinute','�?ến phút');
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfSpecialCharactersValidateEx('toMinute','�?ến phút',Utils._TF_NUMBER);
		}
		if(msg.length==0){
			var value = $('#toMinute').val();
			if(parseInt(value,10) <0 || parseInt(value,10) >59){
				msg = "�?ến phút phải từ 0 đến 59.";
				$('#toMinute').focus();
			}
		}
		var fDate = $('#fDate').val();
		var tDate = $('#tDate').val();
		var fromHour = $('#fromHour').val();
		var toHour = $('#toHour').val();
		if(!Utils.compareDate(fDate, tDate)){
			msg = msgErr_fromdate_greater_todate;		
			$('#fDate').focus();
		}
		if(msg.length == 0){
			if(parseInt(fromHour)>parseInt(toHour)){
				msg = "Từ gi�? không được lớn hơn đến gi�?.";
				$('#fromHour').focus();
			}
		}
		if(msg.length == 0 && parseInt(fromHour)==parseInt(toHour)){
			var fromMinute = $('#fromMinute').val();
			var toMinute = $('#toMinute').val();
			if(parseInt(fromMinute)>parseInt(toMinute)){
				msg = "Từ phút không được lớn hơn đến phút.";
				$('#fromMinute').focus();
			}
		}
		if(msg.length > 0){
			$('#errMsg').html(msg).show();
			return false;
		}
		var dataModel = new Object();
		if(dataShop != null && dataShop.length>0){
			var lstShop = dataShop[0].id;
			for(var i=1;i<dataShop.length;i++){
				lstShop += ","+dataShop[i].id;
			}
			dataModel.strListShopId = lstShop;
		}
		dataModel.fromDate = $('#fDate').val().trim();
		dataModel.toDate = $('#tDate').val().trim();
		dataModel.staffCode = $('#staffCode').val().trim();
		dataModel.staffOwnerCode = $('#staffOwnerCode').val().trim();
		dataModel.fromHour = $('#fromHour').val().trim();
		dataModel.fromMinute = $('#fromMinute').val().trim();
		dataModel.toHour = $('#toHour').val().trim();
		dataModel.toMinute = $('#toMinute').val().trim();
		ReportUtils.exportReport('/report/superivse/dmvs-nvbh/export',dataModel);

	},
	/* BAO CAO THOI GIAN GHE THAM CUA NVBH  GS1.2*/
	reportBCTGGTNVBH : function(){
		var msg = '';
		$('#errMsg').html('').hide();
		if ($('#shopId').val().trim().length == 0 || $('#shopId').val().trim() == activeType.ALL) {
			msg = format(msgErr_required_field,'đơn vị');
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfRequireCheck('fDate','Từ ngày');
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfRequireCheck('tDate','�?ến ngày');
		}
		if(msg.length > 0){
			$('#errMsg').html(msg).show();
			return false;
		}
		if (!checkDateForCycle()) {
			return;
		}
		var dataModel = new Object();
		dataModel.strListShopId=$('#shopId').val();
		dataModel.fromDate = $('#fDate').val().trim();
		dataModel.toDate = $('#tDate').val().trim();
		dataModel.cycleId = $('#cycle').val();
		dataModel.staffType = $('#staffType').val();
		ReportUtils.exportReport('/report/superivse/tggt-nvbh/export',dataModel);
	},
	
	/* BAO CAO THOI GIAN GHE THAM CUA NVBH_1 */
	/* CuongND */
	/* @vuongmq update: 31/03/2014
	 * xuat excel 2007
	 */
	reportBCTGGTNVBH_1 : function(){
		var msg = '';
		$('#errMsg').html('').hide();
		
		var multiShop = $("#shop").data("kendoMultiSelect");
		var dataShop = multiShop.dataItems();
		
		if(dataShop.length <= 0){
			msg = format(msgErr_required_field,'�?ơn vị');
		}
//		if(msg.length == 0){
//			msg = Utils.getMessageOfRequireCheck('shop','�?ơn vị');
//		}
		if(msg.length == 0){
			msg = Utils.getMessageOfRequireCheck('fromHour','Từ gi�?');
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfRequireCheck('fromMinute','Từ phút');
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfRequireCheck('toHour','�?ến gi�?');
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfRequireCheck('toMinute','�?ến phút');
		}
		if ($('#fromDate').val()!= "" && $('#toDate').val()!= ""){
			var fDate = $('#fromDate').val();
			var tDate = $('#toDate').val();
			if(!Utils.compareDate(fDate, tDate)){
				msg = msgErr_fromdate_greater_todate;		
				$('#errMsg').html(msg).show();
				$('#fromDate').focus();
				return false;
			}
		}
		if ($('#fromMinute').val()!= ""){
			if (parseInt($('#fromMinute').val())< 0 || parseInt($('#fromMinute').val()) > 59)
			{
				$('#errMsg').html('Giá trị phút nằm trong khoảng [0-59]. M�?i bạn nhập lại.').show();
				$('#fromMinute').focus();
				return false;
			}
		}
		if ($('#toMinute').val()!= ""){
			if (parseInt($('#toMinute').val())< 0 || parseInt($('#toMinute').val()) > 59)
			{
				$('#errMsg').html('Giá trị phút nằm trong khoảng [0-59]. M�?i bạn nhập lại.').show();
				$('#toMinute').focus();
				return false;
			}
		}
		if ($('#fromHour').val()!=null){
			if (parseInt($('#fromHour').val())< 0 || parseInt($('#fromHour').val()) > 23)
			{
				$('#errMsg').html('Giá trị gi�? nằm trong khoảng [0-23]. M�?i bạn nhập lại.').show();
				$('#fromHour').focus();
				return false;
			}
		}
		if ($('#toHour').val()!=null){
			if (parseInt($('#toHour').val())< 0 || parseInt($('#toHour').val()) > 23)
			{
				$('#errMsg').html('Giá trị gi�? nằm trong khoảng [0-23]. M�?i bạn nhập lại.').show();
				$('#toHour').focus();
				return false;
			}
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfSpecialCharactersValidateEx('fromHour','Từ gi�?',Utils._TF_NUMBER);
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfSpecialCharactersValidateEx('fromMinute','Từ phút',Utils._TF_NUMBER);
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfSpecialCharactersValidateEx('toHour','�?ến gi�?',Utils._TF_NUMBER);
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfSpecialCharactersValidateEx('toMinute','�?ến phút',Utils._TF_NUMBER);
		}
		if(msg.length > 0){
			$('#errMsg').html(msg).show();
			return false;
		}

		var dataModel = new Object();
		if(dataShop!=null && dataShop.length>0){
			var lstShop = dataShop[0].id;
			for(var i=1;i<dataShop.length;i++){
				lstShop += "," + dataShop[i].id;
			}
			dataModel.strListShopId = lstShop;
		}
		//dataModel.shopId = $('#shop').combotree('getValue');
		dataModel.fromDate = $('#fromDate').val();
		dataModel.toDate = $('#toDate').val();
		dataModel.fromHour = $('#fromHour').val();
		dataModel.toHour = $('#toHour').val();
		dataModel.fromMinute = $('#fromMinute').val();
		dataModel.toMinute = $('#toMinute').val();
		dataModel.lstNVGSCode = $('#staffOwnerCode').val().trim();
		dataModel.lstNVBHCode = $('#staffSaleCode').val().trim();
		ReportUtils.exportReport('/report/superivse/tggt-nvbh-1/export',dataModel);
	},
	viewTimeVisitCustomer: function(){
		var msg = '';
		$('#errMsg').html('').hide();
		if ($('#shopId').val().trim().length == 0 || $('#shopId').val().trim() == activeType.ALL) {
			msg = format(msgErr_required_field,'đơn vị');
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfRequireCheck('fDate','Từ ngày');
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfRequireCheck('tDate','�?ến ngày');
		}
		if(msg.length > 0){
			$('#errMsg').html(msg).show();
			return false;
		}
		if (!checkDateForCycle()) {
			return;
		}
		var dataModel = new Object();
		dataModel.strListShopId=$('#shopId').val();
		dataModel.fromDate = $('#fDate').val().trim();
		dataModel.toDate = $('#tDate').val().trim();
		dataModel.cycleId = $('#cycle').val();
		ReportUtils.exportReport('/report/superivse/view',dataModel);
	},
	viewBCKHTT: function(){
		var msg = '';
		$('#errMsg').html('').hide();
		if ($('#shopId').val().trim().length == 0 || $('#shopId').val().trim() == activeType.ALL) {
			msg = format(msgErr_required_field,'đơn vị');
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfRequireCheck('fDate','Từ ngày');
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfRequireCheck('tDate','�?ến ngày');
		}
		if(msg.length > 0){
			$('#errMsg').html(msg).show();
			return false;
		}
		if (!checkDateForCycle()) {
			return;
		}
		var dataModel = new Object();
		dataModel.strListShopId=$('#shopId').val();
		dataModel.fromDate = $('#fDate').val().trim();
		dataModel.toDate = $('#tDate').val().trim();
		dataModel.cycleId = $('#cycle').val();
		ReportUtils.exportReport('/report/superivse/export-bcnvkgtkhtt',dataModel);
	},
	selectCheckbox : function (id,code,type){
		if (type==1){ //nvGS
			if ($('.fancybox-inner #fgid_'+id).is(':checked')){
				SuperviseCustomer.mapNVGS.put(id,code);
			}else{
				SuperviseCustomer.mapNVGS.remove(id);
			}
		}else if (type==2 || type==0){ //NVBH
			if ($('.fancybox-inner #fgid_'+id).is(':checked')){
				SuperviseCustomer.mapNVBH.put(id,code);
			}else{
				SuperviseCustomer.mapNVBH.remove(id);
			}
		}else if (type==3) //TBHV
		{
			if ($('.fancybox-inner #fgid_'+id).is(':checked')){
				SuperviseCustomer.mapTBHV.put(id,code);
			}else{
				SuperviseCustomer.mapTBHV.remove(id);
			}
		}else if (type==4) //Customer CRM KD12
		{
			if ($('.fancybox-inner #fgid_'+id).is(':checked')){
				SuperviseCustomer.mapCustomer.put(id,code);
			}else{
				SuperviseCustomer.mapCustomer.remove(id);
			}
		}
		
	},
	mySelectfancybox : function(type,codeText, nameText, title, url, callback,	codeFieldText, nameFieldText, arrParam, addressText, addressFieldText) {
		var html = $('#fancybox-nvbh-1').html();
		CommonSearch._arrParams = null;
		$(window).unbind('keyup');
		$.fancybox(html,{
			modal : true,
			title : title,						
			autoResize: false,
			afterShow : function() {
				isFromBKPTHNVGH = false;
				//@CuongND : my try Code <reset tabindex>
				var tabindex = -1;
				$('.InputTextStyle, select, input, button , li , tr, td, th, label, ul, a, img').each(function () {
					if (this.type != 'hidden') {
						$(this).attr("tabindex", tabindex);
						tabindex -=1;
					}
				});
				//end <reset tabindex>
				$('#fancybox-nvbh-1').html('');
				if (type==1) $('.fancybox-inner #loai').val(1);      //NVBH_1
				else if (type==2) $('.fancybox-inner #loai').val(2); //NVBH_1
				else if (type==3) $('.fancybox-inner #loai').val(3); //TBHV
				else if (type==4) $('.fancybox-inner #loai').val(4); //CRM KD10 product
				$('.fancybox-inner #seachStyle1Code').focus();
				$('.fancybox-inner #my-searchStyle1Url').val(url);
				
				$('.fancybox-inner #my-searchStyle1CodeText').val(codeFieldText);
				$('.fancybox-inner #my-searchStyle1NameText').val(nameFieldText);
				if (type==2){
					$('.fancybox-inner #myFancyboxSelect').bind('click',function(){
						var stR = "";
						for (var i=0; i< SuperviseCustomer.mapNVBH.size(); i ++){
							stR += SuperviseCustomer.mapNVBH.valArray[i];
							if (i != SuperviseCustomer.mapNVBH.size-1) stR +=";";
						}
						$('#staffSaleCode').val(stR);
						$.fancybox.close();
					});
				}else if (type == 1){
					$('.fancybox-inner #myFancyboxSelect').bind('click',function(){
						$.fancybox.close();
						var stR = "";
						for (var i=0; i< SuperviseCustomer.mapNVGS.size(); i ++){
							stR += SuperviseCustomer.mapNVGS.valArray[i];
							if (i != SuperviseCustomer.mapNVGS.size-1) stR +=";";
						}
						$('#staffOwnerCode').val(stR);
						$.fancybox.close();
					});
				}else if(type == 3) //TBHV
				{
					$('.fancybox-inner #myFancyboxSelect').bind('click',function(){
						var stR = "";
						for (var i=0; i< SuperviseCustomer.mapTBHV.size(); i ++){
							stR += SuperviseCustomer.mapTBHV.valArray[i];
							if (i != SuperviseCustomer.mapTBHV.size -1) stR +=";";
						}
						$('#staffTBHV').val(stR);
						$.fancybox.close();
					});
				}else if(type == 4) //Product CRM KD10
				{
					$('.fancybox-inner #myFancyboxSelect').bind('click',function(){
						var stR = "";
						for (var i=0; i< SuperviseCustomer.mapCustomer.size(); i ++){
							stR += SuperviseCustomer.mapCustomer.valArray[i];
							if (i != SuperviseCustomer.mapCustomer.size-1) stR +=";";
						}
						$('#customerCode').val(stR);
						$.fancybox.close();
					});
				} 
				if(addressText != null && addressText != undefined && addressFieldText != null && addressFieldText != undefined) {
					$('.fancybox-inner #my-searchStyle1AddressText').val(addressFieldText);
					$('.fancybox-inner #seachStyle1AddressLabel').show();
					$('.fancybox-inner #seachStyle1Address').show();
					$('.fancybox-inner #btnSearchStyle1').css('margin-left', 45);
				}
				CommonSearch._currentSearchCallback = callback;
				CommonSearch._arrParams = arrParam;
				var codeField = 'code';
				var nameField = 'name';
				var addressField = 'address';
				if (codeFieldText != null && codeFieldText != undefined && codeFieldText.length > 0) {
					codeField = codeFieldText;
				}
				if (nameFieldText != null&& nameFieldText != undefined && nameFieldText.length > 0) {
					nameField = nameFieldText;
				}
				if(addressText != null && addressText != undefined && addressFieldText != null && addressFieldText != undefined) {
					addressField = addressFieldText;
				}
				$('.fancybox-inner #seachStyle1CodeLabel').html(codeText);
				$('.fancybox-inner #seachStyle1NameLabel').html(nameText);
				$('.fancybox-inner #seachStyle1AddressLabel').html(addressText);
				
				Utils.bindAutoSearch();
				if(addressText != null && addressText != undefined && addressFieldText != null && addressFieldText != undefined) {
					$(".fancybox-inner #my-searchStyle1Grid").jqGrid({
						url : CommonSearch.getSearchStyle1GridUrl(url, '', '', arrParam, ''),
						colModel : [{name : codeField,index : codeField,align : 'left',label : codeText,width : 100,sortable : false,resizable : false, formatter : CommonSearchFormatter.myCode},
						            {name : nameField,index : nameField,label : nameText,sortable : false,resizable : false,align : 'left' ,formatter : CommonSearchFormatter.myName},
						            {name : addressField,index : addressField,label : addressText,sortable : false,resizable : false,align : 'left'},
						            {name : 'select',label : 'Ch�?n',width : 50,align : 'center',sortable : false,resizable : false,formatter : CommonSearchFormatter.selectCellIconFormatter_nvbh_1},
						            {name : 'id',index : 'id',hidden : true}
						            ],
						            pager : $('.fancybox-inner #my-searchStyle1Pager'),
									height : 'auto',rowNum : 10,width : ($('.fancybox-inner #my-searchStyle1ContainerGrid').width()),
									gridComplete : function() {
										$('#jqgh_searchStyle1Grid_rn').html('STT');
										$('#pg_my-searchStyle1Pager').css('padding-right','20px');																	
										tabindex = 1;
										$('.fancybox-inner input,.fancybox-inner select,.fancybox-inner button').each(function () {
											if (this.type != 'hidden') {
											$(this).attr("tabindex", '');
											tabindex++;
											}
										});
										updateRownumWidthForJqGrid('.fancybox-inner');
										$('#my-searchStyle1Pager_right').css('width','180px');//@LamNH:fix bug 0003172
										
										//$.fancybox.update();
										$(window).resize();
										
										var fancyHeight = $('.fancybox-wrap.fancybox-desktop.fancybox-type-html.fancybox-opened').css('height');
										fancyHeight = parseInt(fancyHeight);
										var wWidth = window.innerWidth;
										wWidth = parseInt(wWidth);
										var wHeight = window.innerHeight;
										wHeight = parseInt(wHeight);
										var distant = wHeight - fancyHeight;
										distant = distant / 2;
										if(wHeight > fancyHeight) {
											var tm = setTimeout(function() {
												//$('.fancybox-wrap.fancybox-desktop.fancybox-type-html.fancybox-opened').css('top', document.documentElement.scrollTop +  distant);
												$(window).resize();
												$('.fancybox-wrap.fancybox-desktop.fancybox-type-html.fancybox-opened').animate({'top': document.documentElement.scrollTop +  distant}, 50);
											}, 10);				
										} else {
											var tm = setTimeout(function() {
												//$('.fancybox-wrap.fancybox-desktop.fancybox-type-html.fancybox-opened').css('top', document.documentElement.scrollTop +  distant);
												$(window).resize();
												$('.fancybox-wrap.fancybox-desktop.fancybox-type-html.fancybox-opened').animate({'top': document.documentElement.scrollTop}, 50);
											}, 10);	
										}
										//TODO : the hien checkbox cua fancybox
										if (type==2 || type == 0){
											for (var i=0; i< SuperviseCustomer.mapNVBH.keyArray.length; i++){
												$('#fgid_'+SuperviseCustomer.mapNVBH.keyArray[i]).attr('checked','checked');
											}
										}else if (type==1){
											for (var i=0; i< SuperviseCustomer.mapNVGS.keyArray.length; i++){
												$('#fgid_'+SuperviseCustomer.mapNVGS.keyArray[i]).attr('checked','checked');
											}
										}else if (type==3){
											for (var i=0; i< SuperviseCustomer.mapTBHV.keyArray.length; i++){
												$('#fgid_'+SuperviseCustomer.mapTBHV.keyArray[i]).attr('checked','checked');
											}
										}else if (type==4){
											for (var i=0; i< SuperviseCustomer.mapCustomer.keyArray.length; i++){
												$('#fgid_'+SuperviseCustomer.mapCustomer.keyArray[i]).attr('checked','checked');
											}
										}
									}												
								}).navGrid($('.fancybox-inner #my-searchStyle1Pager'),{edit : false,add : false,del : false,search : false});
				} else {
					$(".fancybox-inner #my-searchStyle1Grid").jqGrid({
						url : CommonSearch.getSearchStyle1GridUrl(url, '', '', arrParam),
						colModel : [{name : codeField,index : codeField,align : 'left',label : codeText,width : 100,sortable : false,resizable : false,formatter : CommonSearchFormatter.myCode},
						            {name : nameField,index : nameField,label : nameText,sortable : false,resizable : false,align : 'left',formatter : CommonSearchFormatter.myName},
						            {name : 'select',label : 'Ch�?n',width : 50,align : 'center',sortable : false,resizable : false,formatter : CommonSearchFormatter.selectCellIconFormatter_nvbh_1},
						            {name : 'id',index : 'id',hidden : true}
						            ],
						            pager : $('.fancybox-inner #my-searchStyle1Pager'),
									height : 'auto',rowNum : 10,width : ($('.fancybox-inner #my-searchStyle1ContainerGrid').width()),
									gridComplete : function() {
										$('#jqgh_searchStyle1Grid_rn').html('STT');
										$('#pg_my-searchStyle1Pager').css('padding-right','20px');																	
										tabindex = 1;
										$('.fancybox-inner input,.fancybox-inner select,.fancybox-inner button').each(function () {
											if (this.type != 'hidden') {
											$(this).attr("tabindex", '');
											tabindex++;
											}
										});
										updateRownumWidthForJqGrid('.fancybox-inner');
										$('#my-searchStyle1Pager_right').css('width','180px');
										
										//$.fancybox.update();
										$(window).resize();
										if (type==2 || type == 0){
											for (var i=0; i< SuperviseCustomer.mapNVBH.keyArray.length; i++){
												$('#fgid_'+SuperviseCustomer.mapNVBH.keyArray[i]).attr('checked','checked');
											}
										}else if (type==1){
											for (var i=0; i< SuperviseCustomer.mapNVGS.keyArray.length; i++){
												$('#fgid_'+SuperviseCustomer.mapNVGS.keyArray[i]).attr('checked','checked');
											}
										}else if (type==3){
											for (var i=0; i< SuperviseCustomer.mapTBHV.keyArray.length; i++){
												$('#fgid_'+SuperviseCustomer.mapTBHV.keyArray[i]).attr('checked','checked');
											}
										}else if (type==4){  //CRM KD12
											for (var i=0; i< SuperviseCustomer.mapCustomer.keyArray.length; i++){
												$('#fgid_'+SuperviseCustomer.mapCustomer.keyArray[i]).attr('checked','checked');
											}
										}
									}												
								}).navGrid($('.fancybox-inner #my-searchStyle1Pager'),{edit : false,add : false,del : false,search : false});
				}
				$('.fancybox-inner #btnSearchStyle1').bind('click',function(event) {
					var code = $('.fancybox-inner #seachStyle1Code').val().trim();
					var name = $('.fancybox-inner #seachStyle1Name').val().trim();
					//var address = $('.fancybox-inner #seachStyle1Address').val().trim();
					var url = '';
					if(addressText != null && addressText != undefined && addressFieldText != null && addressFieldText != undefined) {
						url = CommonSearch.getSearchStyle1GridUrl($('.fancybox-inner #my-searchStyle1Url').val().trim(),code,name,CommonSearch._arrParams,address);
					} else {
						url = CommonSearch.getSearchStyle1GridUrl($('.fancybox-inner #my-searchStyle1Url').val().trim(),code,name,CommonSearch._arrParams);
					}
					$(".fancybox-inner #my-searchStyle1Grid").setGridParam({url : url,page : 1}).trigger("reloadGrid");
					$('.fancybox-inner #seachStyle1Code').focus();
				});
				//$.fancybox.update();
			},
			afterClose : function() {
				isFromBKPTHNVGH = true;//lampv
				var tabindex = 1;
				$(' .InputTextStyle, select, input, button , li , tr, td, th, label, ul, a, img').each(function () {
					if (this.type != 'hidden') {
						$(this).attr("tabindex", '');
					}
					tabindex ++;
				});	
				var curIdFocus = $('#cur_focus').val();
				$('#'+ curIdFocus).focus();
				CommonSearch._currentSearchCallback = null;
				$('#fancybox-nvbh-1').html(html);
				$(window).bind('keyup',function(event){
					if(event.keyCode == keyCodes.ENTER){		   				
						$('#reportContainer .BtnGeneralStyle').click();
					}
				});
			}
		});
		return false;
	},
	
	/* BAO CAO THOI GIAN TAT MAY */	
	//HUYNP4
	reportBCTGTM : function(){
		var msg = '';
		var dataShop = $("#shopId").val();
		$('#errMsg').html('').hide();
		
		if(!dataShop || dataShop <= 0){
			msg = format(msgErr_required_field,'đơn vị');
		}
		
		if(msg.length == 0){
			msg = Utils.getMessageOfRequireCheck('minutes','số phút vượt quá');
		}
		if(msg.length == 0 && $('#minutes').val().trim().length >0){
			msg = Utils.getMessageOfSpecialCharactersValidateEx('minutes','Số phút vượt quá',Utils._TF_NUMBER,null);
		}
		var fDate = $('#fDate').val();
		var tDate = $('#tDate').val();
		if(msg.length == 0){
			msg = Utils.getMessageOfRequireCheck('fDate','từ ngày');
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfInvalidFormatDate('fDate','từ ngày');
		}
		if(msg.length == 0){
			if(!Utils.compareCurrentDate(fDate)){
				msg = "Từ ngày phải bé hơn hoặc bằng ngày hiện tại.";
			}
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfRequireCheck('tDate','đến ngày');
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfInvalidFormatDate('tDate','đến ngày');
		}
		if(msg.length == 0){
			if(!Utils.compareCurrentDate(tDate)){
				msg = "Đến ngày phải bé hơn hoặc bằng ngày hiện tại.";
			}
		}
		if(msg.length == 0){			
			if(!Utils.compareDate(fDate, tDate)){
				msg = msgErr_fromdate_greater_todate;
			}
		}
		var apminutes = '0';
		var minutes=$('#minutes').val().trim();
		if(msg.length == 0){
			if(apminutes!='' && minutes!=''){
				if(parseInt(apminutes) >= parseInt(minutes)){
				msg = "Số phút tắt vượt quá phải lớn hơn " + apminutes + ".";
				}
			}
		}
		if(msg.length > 0){
			$('#errMsg').html(msg).show();
			return false;
		}
		var dataModel = new Object();
		dataModel.shopId = dataShop;
		dataModel.minutes = $('#minutes').val().trim();
		dataModel.fromDate = $('#fDate').val().trim();
		dataModel.toDate = $('#tDate').val().trim();
		ReportUtils.exportReport('/report/superivse/bctgtm/export', dataModel);
	},
	exportBCDSBHSKUCTTKH: function(){
		
		var msg = '';
		var multiShop = $("#shop").data("kendoMultiSelect");
		var dataShop = multiShop.dataItems();
		$('#errMsg').html('').hide();
		/*if(msg.length == 0){
			msg = Utils.getMessageOfRequireCheck('shopId','�?ơn vị');
		}*/ 
		
		if(dataShop.length <= 0){
			msg = format(msgErr_required_field,'�?ơn vị');
		}
		
		var fDate = $('#fDate').val();
		var tDate = $('#tDate').val();
		if(msg.length == 0){
			msg = Utils.getMessageOfRequireCheck('fDate','Từ ngày');
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfInvalidFormatDate('fDate','Từ ngày');
		}
		if(msg.length == 0){
			if(!Utils.compareCurrentDate(fDate)){
				msg = "Từ ngày phải bé hơn hoặc bằng ngày hiện tại.";
			}
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfRequireCheck('tDate','�?ến ngày');
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfInvalidFormatDate('tDate','�?ến ngày');
		}
		if(msg.length == 0){
			if(!Utils.compareCurrentDate(tDate)){
				msg = "�?ến ngày phải bé hơn hoặc bằng ngày hiện tại.";
			}
		}
		if(msg.length == 0){			
			if(!Utils.compareDate(fDate, tDate)){
				msg = msgErr_fromdate_greater_todate;
			}
		}
		
		if(msg.length > 0){
			$('#errMsg').html(msg).show();
			return false;
		}
		
		var dataModel = new Object();
		dataModel.shopId = $('#curShopId').val().trim();
		if(dataShop!=null && dataShop.length>0){
			var lstShop = dataShop[0].id;
			for(var i=1;i<dataShop.length;i++){
				lstShop += "," + dataShop[i].id;
			}
			dataModel.lstShopId = lstShop;
		}
		dataModel.fromDate = $('#fDate').val().trim();
		dataModel.toDate = $('#tDate').val().trim();
		dataModel.shopCode = $('#shopCode').val().trim();		
		ReportUtils.exportReport('/report/superivse/bcdsbhskucttkh/export',dataModel);
	},
	//Bao cao DS1
	exportBCDSBHSKUCTTNVBH: function(){
		
		var msg = '';
		var multiShop = $("#shop").data("kendoMultiSelect");
		var dataShop = multiShop.dataItems();
		$('#errMsg').html('').hide();
		if(dataShop==null||dataShop.length==0){
			msg = format(msgErr_required_field,'�?ơn vị');
		}
		var fDate = $('#fDate').val();
		var tDate = $('#tDate').val();
		if(msg.length == 0){
			msg = Utils.getMessageOfRequireCheck('fDate','Từ ngày');
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfInvalidFormatDate('fDate','Từ ngày');
		}
		if(msg.length == 0){
			if(!Utils.compareCurrentDate(fDate)){
				msg = "Từ ngày phải bé hơn hoặc bằng ngày hiện tại.";
			}
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfRequireCheck('tDate','�?ến ngày');
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfInvalidFormatDate('tDate','�?ến ngày');
		}
		if(msg.length == 0){
			if(!Utils.compareCurrentDate(tDate)){
				msg = "�?ến ngày phải bé hơn hoặc bằng ngày hiện tại.";
			}
		}
		if(msg.length == 0){			
			if(!Utils.compareDate(fDate, tDate)){
				msg = msgErr_fromdate_greater_todate;
			}
		}
		
		if(msg.length > 0){
			$('#errMsg').html(msg).show();
			return false;
		}
		var dataModel = new Object();
		if(dataShop!=null && dataShop.length>0){
			var lstShop = dataShop[0].id;
			for(var i=1;i<dataShop.length;i++){
				lstShop += "," + dataShop[i].id;
			}
			dataModel.strListShopId = lstShop;
		}
		//dataModel.shopId = $('#shopId').val().trim();				
		if(dataShop!=null && dataShop.length>0){
			var lstShop = dataShop[0].id;
			for(var i=1;i<dataShop.length;i++){
				lstShop += "," + dataShop[i].id;
			}
			dataModel.listShopId = lstShop;
		}
		dataModel.fromDate = $('#fDate').val().trim();
		dataModel.toDate = $('#tDate').val().trim();
		dataModel.shopCode = $('#shopCode').val().trim();		
		ReportUtils.exportReport('/report/superivse/bcdsbhskucttnvbh/export',dataModel);
	},

	treeProduct : function(){
		var html = $('#productTreeDialog').html();
		$.fancybox(html,
				{
					modal: true,
					title: 'Thêm thông tin sản phẩm',
					afterShow: function(){
						Utils.resetCheckboxStateOfTree();
						Utils.bindAutoSearch();	
						$('.fancybox-inner #treeGroup').hide();
						$('.fancybox-inner #treeQuotaGroup').hide();
						$('#productTreeDialog').html('');
						
						$('.fancybox-inner #btnSearchProductTree').bind('click',function(){
							//ProgrammeDisplayCatalog.searchProductOnTree();
							$('#productTree li').each(function(){
								var _id = $(this).attr('id');
								var type = $(this).attr('contentitemid');
								if($(this).hasClass('jstree-checked') && type=='product'){
									ProgrammeDisplayCatalog._mapProduct.put(_id,_id);		
								}else {
									ProgrammeDisplayCatalog._mapProduct.remove(_id);
								}
							});
							var productCode = encodeChar($('#productCodeDlg').val().trim());
							var productName = encodeChar($('#productNameDlg').val().trim());
							$('#productTreeContent').data('jsp').destroy();
							ProgrammeDisplayCatalog.loadDataForTreeWithCheckboxExCRM_SKU('/rest/catalog/display-program/product/list.json?displayProgramId='+ 0 +'&productCode='+ productCode +'&productName='+ productName + '&nodeType=cat&catId=0&subCatId=0','productTree');
							setTimeout(function() {
								$('#productTreeContent').jScrollPane();
								$('#productTree').bind("loaded.jstree open_node.jstree close_node.jstree", function (event, data) {
									//Utils.applyCheckboxStateOfTree();
									if($('#productTreeContent').data('jsp') != undefined || $('#productTreeContent').data('jsp') != null) {
										$('#productTreeContent').data('jsp').destroy();
										setTimeout(function(){
											$('#productTreeContent').jScrollPane();
										},500);	
									} else {
										setTimeout(function(){
											$('#productTreeContent').jScrollPane();
										},500);	
									}
									SuperviseCustomer.loadCheckStateCRM_KD1(SuperviseCustomer.mapProduct,"product",'productTree li');
								});
								SuperviseCustomer.loadCheckStateCRM_KD1(SuperviseCustomer.mapProduct,"product",'productTree li');
							}, 500);
						});
						// Cuong ND edit code -----------------------------------
						SuperviseCustomer.loadCheckStateCRM_KD1(SuperviseCustomer.mapProduct,"product",'productTree li');
						$('.fancybox-inner #btnQ').bind('click',function(){
							$('li li li').each(function(){
							    if($(this).hasClass('jstree-checked')){
							    	var id = $(this).attr('id');
							    	var str = $('#'+id +' a').text().trim();
							    	str = str.substring(0,str.indexOf('-'));
							    	SuperviseCustomer.mapProduct.put(id,str.trim());
							    }
							});
							var string = "";
							for (var i=0; i< SuperviseCustomer.mapProduct.valArray.length; i++){
								if (i == 0) {
									string += SuperviseCustomer.mapProduct.valArray[i];
								}else{
									string += ";"+SuperviseCustomer.mapProduct.valArray[i];
								}
							}
							$('#product').val(string);
							$.fancybox.close();
						});
						//--------------------------------------------------------------
						var displayId = $('#selId').val();
						var productCode = encodeURI('');
						var productName = encodeURI('');
						ProgrammeDisplayCatalog.loadDataForTreeWithCheckboxExCRM_SKU('/rest/catalog/display-program/product/list.json?displayProgramId=0&productCode='+ productCode +'&productName='+ productName + '&nodeType=cat&catId=0&subCatId=0','productTree');
						ProgrammeDisplayCatalog._mapProduct = new Map();
						$('#productTree').bind("loaded.jstree open_node.jstree close_node.jstree", function (event, data) {
							if($('#productTreeContent').data('jsp') != undefined || $('#productTreeContent').data('jsp') != null) {
								$('#productTreeContent').data('jsp').destroy();
								setTimeout(function(){
									$('#productTreeContent').jScrollPane();
								},500);	
							} else {
								setTimeout(function(){
									$('#productTreeContent').jScrollPane();
								},500);	
							}
							//$('#productTreeContent .jspContainer').css('width','500px');							
						});
						ProgrammeDisplayCatalog.openNodeOnTree('productTree');
					},
					afterClose: function(){
						$('#productTreeDialog').html(html);				
					}
				}
			);
		return false;
	},
	selectListProduct : function(){
		
	},
	changeMyMap : function(type){
		switch(type){
		case 1: //NVGS
			var str = $('#staffOwnerCode').val();
			tMap = new Map();
			if(str == ""){
				SuperviseCustomer.mapNVGS.clear();
			}else{
				var code; var index;
				while(str.length > 0){
					if (str.indexOf(';')== -1){
						code = str;
					}
					else code = str.substring(0,str.indexOf(';'));
					index = SuperviseCustomer.mapNVGS.findIt4Val(code);
					if (index != -1){
						tMap.put(SuperviseCustomer.mapNVGS.keyArray[index],code);
					}
					if (str.indexOf(';') != -1) {
						str = str.substring(str.indexOf(';')+1,str.length);
					}else{
						str = "";
					}
				}
				SuperviseCustomer.mapNVGS = tMap;
			}
			break;
		case 2: //NVBH
			var str = $('#staffSaleCode').val();
			tMap = new Map();
			if(str == ""){
				SuperviseCustomer.mapNVBH.clear();
			}else{
				var code; var index;
				while(str.length > 0){
					if (str.indexOf(';')== -1){
						code = str;
					}
					else code = str.substring(0,str.indexOf(';'));
					index = SuperviseCustomer.mapNVBH.findIt4Val(code);
					if (index != -1){
						tMap.put(SuperviseCustomer.mapNVBH.keyArray[index],code);
					}
					if (str.indexOf(';')!= -1) {
						str = str.substring(str.indexOf(';')+1,str.length);
					}else{
						str = "";
					}
				}
				SuperviseCustomer.mapNVBH = tMap;
			}
			break;
		case 3: //TBHV
			var str = $('#staffTBHV').val();
			tMap = new Map();
			if(str == ""){
				SuperviseCustomer.mapTBHV.clear();
			}else{
				var code; var index;
				while(str.length > 0){
					if (str.indexOf(';')== -1){
						code = str;
					}
					else code = str.substring(0,str.indexOf(';'));
					index = SuperviseCustomer.mapTBHV.findIt4Val(code);
					if (index != -1){
						tMap.put(SuperviseCustomer.mapTBHV.keyArray[index],code);
					}
					if (str.indexOf(';')!= -1) {
						str = str.substring(str.indexOf(';')+1,str.length);
					}else{
						str = "";
					}
				}
				SuperviseCustomer.mapTBHV = tMap;
			}
			break;
		case 4: //Customer
			var str = $('#customerCode').val();
			tMap = new Map();
			if(str == ""){
				SuperviseCustomer.mapCustomer.clear();
			}else{
				var code; var index;
				while(str.length > 0){
					if (str.indexOf(';')== -1){
						code = str;
					}
					else code = str.substring(0,str.indexOf(';'));
					index = SuperviseCustomer.mapCustomer.findIt4Val(code);
					if (index != -1){
						tMap.put(SuperviseCustomer.mapCustomer.keyArray[index],code);
					}
					if (str.indexOf(';')!= -1) {
						str = str.substring(str.indexOf(';')+1,str.length);
					}else{
						str = "";
					}
				}
				SuperviseCustomer.mapCustomer = tMap;
			}
			break;
		}
	},
	loadCheckStateCRM_KD1:function(MAP,nodeType,selector){
		for(var i=0;i<MAP.keyArray.length;++i){
			var _obj = MAP.keyArray[i];					
			$('#' + selector).each(function(){
				var _id = $(this).attr('id');
				var type = $(this).attr('contentitemid');
				if(_id==_obj && nodeType==type ){
					$(this).removeClass('jstree-unchecked');
					$(this).addClass('jstree-checked');
				}
			});
		}
	},
	//Hunglm16 January 15,2014
	reportBCTGBHHQCNVBH_DS3 : function(){
		//DS3 - Bao cao thoi gian ban hang hieu qua cua nhan vien ban hang
		var msg = '';
		$('#errMsg').html('').hide();
		var multiShop = $("#shop").data("kendoMultiSelect");
		var dataShop = multiShop.dataItems();
		var fromDate = $('#fromDate').val();
		if(dataShop == null || dataShop.length ==0){
			msg = "Bạn chưa ch�?n đơn vị";
		}
		/*if(msg.length == 0 && $('#staffOwnerCode').val().trim().length >0) {
			msg = Utils.getMessageOfSpecialCharactersValidate('staffOwnerCode','Mã nhân viên giám sát',Utils._CODE);
		}	
		if(msg.length == 0 && $('#staffCode').val().trim().length >0){ 
			msg = Utils.getMessageOfSpecialCharactersValidate('staffCode','Mã nhân viên bán hàng',Utils._CODE);
		}*/
		if(msg.length == 0){
			msg = Utils.getMessageOfRequireCheck('fromDate','Ngày');
		}
		if(msg.length == 0){
			if (fromDate != '' && !Utils.isDate(fromDate)) {
				msg = 'Ngày không hợp lệ, vui lòng nhập theo định dạng dd/MM/yyyy';
			}
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfRequireCheck('fromHour','Từ gi�?');
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfInvaildInteger('fromHour','Từ gi�?');
		}
		if(msg.length ==0){
			var value = $('#fromHour').val();
			if(parseInt(value,10) <0 || parseInt(value,10) >23){
				msg = "Từ gi�? phải từ 0 đến 23.";
			}
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfRequireCheck('toHour','�?ến gi�?');
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfInvaildInteger('toHour','�?ến gi�?');
		}
		if(msg.length ==0){
			var value = $('#toHour').val();
			if(parseInt(value,10) <0 || parseInt(value,10) >23){
				msg = "�?ến gi�? phải từ 0 đến 23.";
			}
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfRequireCheck('fromMinute','Từ phút');
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfInvaildInteger('fromMinute','Từ phút');
		}
		if(msg.length ==0){
			var value = $('#fromMinute').val();
			if(parseInt(value,10) <0 || parseInt(value,10) >59){
				msg = "Từ phút phải từ 0 đến 59.";
			}
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfRequireCheck('toMinute','�?ến phút');
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfInvaildInteger('toMinute','�?ến phút');
		}
		if(msg.length ==0){
			var value = $('#toMinute').val();
			if(parseInt(value,10) <0 || parseInt(value,10) >59){
				msg = "�?ến phút phải từ 0 đến 59.";
			}
		}
		if(msg.length == 0){
			
		}
		if(msg.length==0){
			var toHour=$('#toHour').val();
			var fromHour=$('#fromHour').val();
			if(parseInt(fromHour)==parseInt(toHour)){
				var fMin = $('#fromMinute').val();
				var tMin = $('#toMinute').val();
				if(parseInt(fMin)>parseInt(tMin)){
					msg = "Từ phút không được lớn hơn đến phút.";
				}
			}else if(parseInt(fromHour)>parseInt(toHour)){
				msg = "Từ gi�? không được lớn hơn đến gi�?.";
			}
		}
		if(msg.length > 0){
			$('#errMsg').html(msg).show();
			return false;
		}
		var dataModel = new Object();		
		//dataModel.shopId = $('.combo-value').val();
		if(dataShop!=null && dataShop.length>0){
			var lstShop = dataShop[0].id;
			for(var i=1;i<dataShop.length;i++){
				lstShop += "," + dataShop[i].id;
			}
			dataModel.strListShopId = lstShop;
		}
		dataModel.staffOwnerCode = $('#staffOwnerCode').val().trim();
		dataModel.staffSaleCode = $('#staffCode').val().trim();
		dataModel.fromDate = $('#fromDate').val();
		dataModel.fromHour = $('#fromHour').val();
		dataModel.toHour = $('#toHour').val();
		dataModel.fromMinute = $('#fromMinute').val();
		dataModel.toMinute = $('#toMinute').val();
		dataModel.sprTime = $('#sprTime').val();
		
		ReportUtils.exportReport('/report/superivse/bctgbhhqnvbh/export', dataModel);
	},
	
	//vuonghn VT7-Bao cao ghe tham khach hang
	exportBCVT7: function(){
		var msg = '';
		$('#errMsg').html('').hide();
		var shopKendo = $("#shop").data("kendoMultiSelect");
		var dataShop = shopKendo.dataItems();
		if(dataShop==null||dataShop.length==0){
			msg = format(msgErr_required_field,'�?ơn vị');
		}
//		if(msg.length == 0 && $('#staffOwnerCode').val().trim().length >0){ 
//			msg = Utils.getMessageOfSpecialCharactersValidate('staffOwnerCode','GSNPP',Utils._CODE);
//		}
		
		if(msg.length == 0){
			msg = Utils.getMessageOfRequireCheck('fromDate','Từ ngày');
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfInvalidFormatDate('fromDate','Từ ngày');
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfRequireCheck('toDate','�?ến ngày');
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfInvalidFormatDate('toDate','�?ến ngày');
		}
		var fDate = $('#fromDate').val();
		var tDate = $('#toDate').val();
		var curDate = ReportUtils.getCurrentDateString();
		if(msg.length == 0 && !Utils.compareDate(fDate, curDate)){
			msg = 'Từ ngày không được lớn hơn ngày hiện tại. Vui lòng nhập lại';
			$('#fromDate').focus();
		}	
		if(msg.length == 0 && !Utils.compareDate(tDate, curDate)){
			msg = '�?ến ngày không được lớn hơn ngày hiện tại. Vui lòng nhập lại';
			$('#toDate').focus();
		}
		if(msg.length == 0 && !Utils.compareDate(fDate, tDate)) {
			msg ='Từ ngày không được lớn hơn đến ngày. Vui lòng nhập lại';
			$('#fromDate').focus();
		}
		if(msg.length > 0){
			$('#errMsg').html(msg).show();
			return false;
		}
		var lstShopId = dataShop[0].id;
		for(var i=1;i<dataShop.length;i++){
			lstShopId += "," + dataShop[i].id;
		}
		var dataModel = new Object();		
		//dataModel.shopCode = $('#shopCode').val().trim();
		dataModel.strListShopId = lstShopId;
		dataModel.staffOwnerCode = $('#staffOwnerCode').val().trim();
		dataModel.fromDate = $('#fromDate').val();
		dataModel.toDate = $('#toDate').val();
		ReportUtils.exportReport('/report/superivse/vt7-bcgtkh/export',dataModel);
	},
	exportVT7_1: function(){
		var msg = '';
		$('#errMsg').html('').hide();
		var shopKendo = $("#shop").data("kendoMultiSelect");
		var dataShop = shopKendo.dataItems();
		if(dataShop==null||dataShop.length==0){
			msg = format(msgErr_required_field,'�?ơn vị');
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfRequireCheck('fromDate','Từ ngày');
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfInvalidFormatDate('fromDate','Từ ngày');
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfRequireCheck('toDate','�?ến ngày');
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfInvalidFormatDate('toDate','�?ến ngày');
		}
		var fDate = $('#fromDate').val();
		var tDate = $('#toDate').val();
		var curDate = ReportUtils.getCurrentDateString();
		if(msg.length == 0 && !Utils.compareDate(fDate, curDate)){
			msg = 'Từ ngày không được lớn hơn ngày hiện tại. Vui lòng nhập lại';
			$('#fromDate').focus();
		}	
		if(msg.length == 0 && !Utils.compareDate(tDate, curDate)){
			msg = '�?ến ngày không được lớn hơn ngày hiện tại. Vui lòng nhập lại';
			$('#toDate').focus();
		}
		if(msg.length == 0 && !Utils.compareDate(fDate, tDate)) {
			msg ='Từ ngày không được lớn hơn đến ngày. Vui lòng nhập lại';
			$('#fromDate').focus();
		}
		if(msg.length > 0){
			$('#errMsg').html(msg).show();
			return false;
		}
		var lstShopId = dataShop[0].id;
		for(var i=1;i<dataShop.length;i++){
			lstShopId += "," + dataShop[i].id;
		}
		var dataModel = new Object();		
		dataModel.strListShopId = lstShopId;
		dataModel.staffOwnerCode = $('#staffOwnerCode').val().trim();
		dataModel.fromDate = $('#fromDate').val();
		dataModel.toDate = $('#toDate').val();
		ReportUtils.exportReport('/report/superivse/vt7_1/export',dataModel);
	},
	exportBCVT6: function(){
		var msg = '';
		var multiShop = $("#shop").data("kendoMultiSelect");
		var dataShop = multiShop.dataItems();
		$('#errMsg').html('').hide();
		if(dataShop.length <= 0){
			msg = format(msgErr_required_field,'�?ơn vị');
		}
			
		if(msg.length == 0){
			msg = Utils.getMessageOfRequireCheck('fromDate','Ngày');
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfInvalidFormatDate('fromDate','Ngày');
		}
		if(msg.length > 0){
			$('#errMsg').html(msg).show();
			return false;
		}
		var dataModel = new Object();
		if(dataShop!=null && dataShop.length>0){
			var lstShop = dataShop[0].id;
			for(var i=1;i<dataShop.length;i++){
				lstShop += "," + dataShop[i].id;
			}
			dataModel.strListShopId = lstShop;
		}
//		dataModel.shopId = $('#shopCode').val().trim();
		dataModel.staffOwnerCode = $('#staffOwnerCode').val().trim();
		dataModel.staffSaleCode = $('#staffSaleCode').val().trim();
		dataModel.fromDate = $('#fromDate').val().trim();
		ReportUtils.exportReport('/report/superivse/bcvt6/export',dataModel);
	},
	//vuongh: VT10 - Bao cao ket qua cham trung bay
	exportBCVT10: function(){
		var msg = '';
		$('#errMsg').html('').hide();
		var shopKendo = $("#shop").data("kendoMultiSelect");
		var lstShopId = shopKendo.value();
		if(lstShopId.length == 0) {
			msg = format(msgErr_required_field,'�?ơn vị');
		}
		/*if(msg.length == 0 && $('#superStaffCode').val().trim().length >0){ 
			msg = Utils.getMessageOfSpecialCharactersValidate('superStaffCode','Mã nhân viên giám sát',Utils._CODE);
		}*/
		if(msg.length == 0){
			msg = Utils.getMessageOfRequireCheck('fromDate','Từ ngày');
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfInvalidFormatDate('fromDate','Từ ngày');
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfRequireCheck('toDate','�?ến ngày');
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfInvalidFormatDate('toDate','�?ến ngày');
		}
		var fDate = $('#fromDate').val();
		var tDate = $('#toDate').val();
		var curDate = ReportUtils.getCurrentDateString();
		if(msg.length == 0 && !Utils.compareDate(fDate, curDate)){
			msg = 'Từ ngày phải nh�? hơn hoặc bằng ngày hiện tại';
			$('#fromDate').focus();
		}	
		if(msg.length == 0 && !Utils.compareDate(tDate, curDate)){
			msg = '�?ến ngày phải nh�? hơn hoặc bằng ngày hiện tại';
			$('#toDate').focus();
		}
		if(!Utils.compareDate(fDate, tDate)) {
			msg ='Từ ngày phải nh�? hơn đến ngày.';
			$('#fromDate').focus();
		}
		if(msg.length > 0){
			$('#errMsg').html(msg).show();
			return false;
		}
		
		var dataModel = new Object();
		
		dataModel.strListShopId = lstShopId.toString();
		dataModel.superStaffCode = $('#superStaffCode').val().trim(); 
		dataModel.fromDate = $('#fromDate').val();
		dataModel.toDate = $('#toDate').val();
		ReportUtils.exportReport('/report/superivse/vt10-bckqctb/export',dataModel);
	},
	
	// vuongmq: vt11 - bao cao nhan vien mo clip
	exportBCVT11: function(){
		var msg = '';
		$('#errMsg').html('').hide(); 
		if(msg.length == 0){
			msg = Utils.getMessageOfRequireCheck('fromDate','Từ ngày');
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfInvalidFormatDate('fromDate','Từ ngày');
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfRequireCheck('toDate','�?ến ngày');
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfInvalidFormatDate('toDate','�?ến ngày');
		}
		var fDate = $('#fromDate').val();
		var tDate = $('#toDate').val();
		var curDate = ReportUtils.getCurrentDateString();
		if(msg.length == 0 && !Utils.compareDate(fDate, curDate)){
			msg = 'Từ ngày phải nh�? hơn hoặc bằng ngày hiện tại';
			$('#fromDate').focus();
		}	
		if(msg.length == 0 && !Utils.compareDate(tDate, curDate)){
			msg = '�?ến ngày phải nh�? hơn hoặc bằng ngày hiện tại';
			$('#toDate').focus();
		}
		if(!Utils.compareDate(fDate, tDate)) {
			msg ='Từ ngày phải nh�? hơn đến ngày.';
			$('#fromDate').focus();
		}
		if(msg.length > 0){
			$('#errMsg').html(msg).show();
			return false;
		}
		
		var shopKendo = $("#shop").data("kendoMultiSelect");
		var lstShopId = shopKendo.value();
		if(lstShopId.length == 0) {
			$('#errMsg').html('Bạn chưa ch�?n NPP. Vui lòng ch�?n NPP').show();
			return false;
		}
		
		var dataModel = new Object();
		//dataModel.lstShopId = lstShopId;
		dataModel.strListShopId = lstShopId.toString();
		dataModel.fromDate = $('#fromDate').val();
		dataModel.toDate = $('#toDate').val();
		//ReportUtils.exportReport('/report/superivse/vt11-bcnvmclip/export',dataModel,'errMsg');
		ReportUtils.exportReport('/report/superivse/vt11-bcnvmclip/export',dataModel);
	},
	
	/**
	 * Bc 1.3 ket qua di tuyen
	 * 
	 * @author hoanv25
	 * @since July 07, 2015
	 * */
	resultTimeVisitCustomer: function(){
		var msg = '';
		$('#errMsg').html('').hide();
		if ($('#shopId').val().trim().length == 0 || $('#shopId').val().trim() == activeType.ALL) {
			msg = format(msgErr_required_field,'đơn vị');
		}
		if (msg.length == 0) {
			msg = Utils.getMessageOfRequireCheck('fDate','Từ ngày');
		}
		if (msg.length == 0) {
			msg = Utils.getMessageOfRequireCheck('tDate','�?ến ngày');
		}
		if (msg.length > 0) {
			$('#errMsg').html(msg).show();
			return false;
		}
		if (!checkDateForCycle()) {
			return;
		}
		var dataModel = new Object();
		dataModel.strListShopId=$('#shopId').val();
		dataModel.fromDate = $('#fDate').val().trim();
		dataModel.toDate = $('#tDate').val().trim();
		dataModel.cycleId = $('#cycle').val();
		ReportUtils.exportReport('/report/superivse/vt6/export',dataModel);
	},
	
	/**
	 * Load ds khach hang 
	 * 
	 * @author hoanv25
	 * @since July 08, 2015
	 * */
	loadComboNVBH:function(url){
		$("#nvbh").kendoMultiSelect({
			dataSource: {
				transport: {
	                read: {
	                    dataType: "json",
	                    url: url
	                }
	            }
			},
			filter: "contains",
	        dataTextField: "nvbhName",
	        dataValueField: "nvbhId",
			itemTemplate: function(data, e, s, h, q) {
				return '<div class="tree-vnm" node-id="'+Utils.XSSEncode(data.nvbhId)+'" style="width:180px"><span class="tree-vnm-text">'+Utils.XSSEncode(data.nvbhCode)+' - '+Utils.XSSEncode(data.nvbhName) +'</span></div>';	
			},
	        tagTemplate:  '#: data.nvbhName #',
	        value: [$('#nvbhId').val()]
	    });	 
	},
	
	/**
	 * Bc GS 1.4 mo moi khach hang 
	 * 
	 * @author hoanv25
	 * @since July 08, 2015
	 * */
	exportBCMMKH: function(){
		var msg = '';		
	/*	var gsKendo = $("#gs").data("kendoMultiSelect");
		var lstGsId = gsKendo.value();*/
		var nvbhKendo = $("#nvbh").data("kendoMultiSelect");
		var lstNvbhId = nvbhKendo.value();		
		var multiShop = $("#shop").data("kendoMultiSelect");
		var dataShop = multiShop.dataItems();
		var dataNVBH = null;
		if (nvbhKendo != null) {
			dataNVBH = nvbhKendo.dataItems();
		}
		if(dataShop!=null && dataShop.length>0){
			var lstShop = new Array();
			for(var i=0;i<dataShop.length;i++){
				lstShop.push(dataShop[i].id);
			}
		}
		var lstNVBH = [];
		for(var i=0;i<dataNVBH.length;i++){
			lstNVBH.push(dataNVBH[i].nvbhName);
		}
		$('#errMsg').html('').hide();
		/*if ($('#shopId').val().trim().length == 0 || $('#shopId').val().trim() == activeType.ALL) {
			msg = format(msgErr_required_field,'đơn vị');
		}*/
		if (msg.length == 0) {
			msg = Utils.getMessageOfRequireCheck('fDate','Từ ngày');
		}
		if (msg.length == 0) {
			msg = Utils.getMessageOfRequireCheck('tDate','Đến ngày');
		}
		if (msg.length > 0) {
			$('#errMsg').html(msg).show();
			return false;
		}
		if (!checkDateForCycle()) {
			return;
		}
		var dataModel = new Object();
		dataModel.strListShopId=lstShop.toString().trim();
		dataModel.fromDate = $('#fDate').val().trim();
		dataModel.toDate = $('#tDate').val().trim();
		dataModel.cycleId = $('#cycle').val();
		//dataModel.staffType = $('#staffType').val();
		/*if (lstGsId != undefined && lstGsId != null && lstGsId != "") {
			dataModel.strListGsId = lstGsId.toString().trim();
		}*/
		if (lstNvbhId != undefined && lstNvbhId != null && lstNvbhId != "") {
			dataModel.strListNvbhId = lstNvbhId.toString().trim();
		}
		if (lstNVBH != undefined && lstNVBH != null && lstNVBH != "") {
			dataModel.strListNVBH = lstNVBH.toString().trim();
		}
		ReportUtils.exportReport('/report/superivse/vt11-bcnvmclip/export',dataModel);
	},
	
	//VT12 Bao cao xoa khach hang tham gia cttb
	exportBCVT12: function(){
		var msg = '';
		$('#errMsg').html('').hide(); 
		var shopKendo = $("#shop").data("kendoMultiSelect");
		var lstShopId = shopKendo.value();
		if(lstShopId.length == 0) {
			$('#errMsg').html(msg = format(msgErr_required_field,'�?ơn vị')).show();
			return false;
		}
		
		var dataModel = new Object();		
		var displayProgramId = $('#displayProgram').val();		
		if(displayProgramId != null && displayProgramId.length > 0) {			
			//dataModel.idCTTB = displayProgramId[0];
			var lstDisplayProgram = new Array();
			for(var i = 0; i<displayProgramId.length; i++){
				if(displayProgramId[i] != null && displayProgramId[i] != ""){
					lstDisplayProgram.push(displayProgramId[i]);
				}
			}
			dataModel.displayProgramId = lstDisplayProgram.toString();
		} else {
			$('#errMsg').html('Bạn chưa ch�?n chương trình. Vui lòng ch�?n chương trình').show();
			return false;
		}
		
		var monthYear = $('#month').val().trim();
		if(monthYear != null && monthYear.length > 0) {		
			dataModel.monthYear = monthYear;
		}else {
			$('#errMsg').html('Bạn chưa ch�?n tháng. Vui lòng ch�?n tháng').show();
			return false;
		}
		
		dataModel.strListShopId = lstShopId.toString();
		ReportUtils.exportReport('/report/superivse/vt12-bcxkhtgcttb/export',dataModel);
	},
	

	/**
	 * 6.1.2 Thời gian làm việc của NVBH
	 * @author hunglm16
	 * @since 19/10/2015
	 */
	report6D1D2TGLVCNVBH : function() {
		var msg = '';
		$('#errMsg').html('').hide();
		if ($('#shopId').val().trim().length == 0 || $('#shopId').val().trim() == activeType.ALL) {
			msg = format(msgErr_required_field, 'đơn vị');
		}
		var msg = '';
		if (msg.length == 0) {
			msg = Utils.getMessageOfRequireCheck('fDate', 'Từ ngày');
		}
		if (msg.length == 0) {
			msg = Utils.getMessageOfRequireCheck('tDate', 'Đến ngày');
		}
		if (msg.length == 0) {
			msg = Utils.getMessageOfInvalidFormatDate('fDate','Từ ngày');
		}
		if (msg.length == 0) {
			msg = Utils.getMessageOfInvalidFormatDate('tDate','Đến ngày');
		}
		var fDate = $('#fDate').val();
		var tDate = $('#tDate').val();
		if (!Utils.compareDate(fDate, tDate)) {
			msg = js_fromdate_todate_validate;		
			$('#fDate').focus();
		}
		if (msg.length == 0) {
			msg = Utils.compareCurrentDateEx('tDate','Đến ngày');
		}
		if (msg.length > 0) {
			$('#errMsg').html(msg).show();
			return false;
		}
		var dataModel = new Object();
		dataModel.strListShopId = $('#shopId').val();
		dataModel.fromDate = $('#fDate').val().trim();
		dataModel.toDate = $('#tDate').val().trim();
		//dataModel.cycleId = $('#cycle').val();
//		dataModel.staffType = $('#staffType').val();
		ReportUtils.exportReport('/report/superivse/tggt-nvbh/export', dataModel);
	}
};
/*
 * END OF FILE - /web/web/resources/scripts/business/shop-report/vnm.supervise-customer-report.js
 */

/*
 * START OF FILE - /web/web/resources/scripts/business/shop-report/vnm.debit-pay-report.js
 */
var DebitPayReport = {
	_lstCustomer:null,
	_lstStaff:null,
	_lstStaffOwner:null,
	_lstProduct:null,
	_lstSubCat:null,
	_objectStaff:null,
	_objectProduct:null,
	_objectSubCat:null,
	/* BAO CAO TONG CONG NO */
	reportTCN : function(){
		var msg = '';
		var objectName = '';
		$('#errMsg').html('').hide();
//		if($('#shopId').val().trim().length == 0 || $('#shopId').val().trim() == activeType.ALL){
//			msg = format(msgErr_required_field,'đơn vị');
//		}
//		if(msg.length > 0){
//			$('#errMsg').html(msg).show();
//			return false;
//		}
		var dataModel = new Object();		
		dataModel.shopId = $('#shopId').val().trim();
		dataModel.objectType = $('#objectType').val();
		//dataModel.lstCustomer = $('#customerCode').val().trim().split(";");
		//dataModel.lstStaff = $('#staffCode').val().trim().split(";");
		dataModel.lstCustomer = $('#customerCode').val().trim().split(",");
		dataModel.lstStaff = $('#staffCode').val().trim().split(",");
		if($('input[name=formatType]:radio:checked').length > 0 && $('input[name=formatType]:radio:checked').val() != undefined) {
            dataModel.formatType = $('input[name=formatType]:radio:checked').val().toUpperCase();
       }

		ReportUtils.exportReport('/report/debit-pay/tcn/export',dataModel);
	},
	validateTTKH:function(){
		var msg = '';
		$('#errMsg').html('').hide();
		if($('#shopId').val().trim().length == 0 || $('#shopId').val().trim() == activeType.ALL){
			msg = format(msgErr_required_field,'đơn vị');
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfRequireCheck('fDate','Từ ngày');
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfInvalidFormatDate('fDate','Từ ngày');
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfRequireCheck('tDate','Đến ngày');
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfInvalidFormatDate('tDate','Đến ngày');
		}
		var fDate = $('#fDate').val();
		var tDate = $('#tDate').val();
		var curDate = $('#curDate').val();
		if(!Utils.compareDate(fDate, tDate)){
			msg = msgErr_fromdate_greater_todate;		
			$('#fDate').focus();
		}
		if(!Utils.compareDate(fDate, curDate)){
			msg = "Từ ngày không được lớn hơn ngày hiện tại";		
			$('#fDate').focus();
		}
		if(msg.length > 0){
			$('#errMsg').html(msg).show();
			return false;
		}
		return true;
	},
	selectCustomer:function(obj,code){
		if(code=='null') return;
		if(obj.checked){
			DebitPayReport._lstCustomer.put(code, code);
		}else{
			DebitPayReport._lstCustomer.remove(code);
		}
		var lstCutomer='';
		for(var i=0;i<DebitPayReport._lstCustomer.keyArray.length;i++){
			if(DebitPayReport._lstCustomer.keyArray[i]!=undefined && DebitPayReport._lstCustomer.keyArray[i]!=null)
				lstCutomer+= DebitPayReport._lstCustomer.keyArray[i]+";";
		}
		lstCutomer=lstCutomer.substring(0,lstCutomer.length-1);
		$('#customerCode').val(lstCutomer);
	},
	selectStaff:function(obj,code){
		if(code=='null') return;
		if(obj.checked){
			DebitPayReport._lstStaff.put(code, code);
		}else{
			DebitPayReport._lstStaff.remove(code);
		}
		var lstStaff='';
		for(var i=0;i<DebitPayReport._lstStaff.keyArray.length;i++){
			if(DebitPayReport._lstStaff.keyArray[i]!=undefined && DebitPayReport._lstStaff.keyArray[i]!=null)
				lstStaff+= DebitPayReport._lstStaff.keyArray[i]+";";
		}
		lstStaff=lstStaff.substring(0,lstStaff.length-1);
		var os = 'staffCode';
		if(DebitPayReport._objectStaff!=undefined && DebitPayReport._objectStaff!=null){
			os = DebitPayReport._objectStaff;
		}
		$('#'+os).val(lstStaff);
	},
	selectSupervisorStaff:function(obj,code){
		if(code=='null') return;
		if(obj.checked){
			DebitPayReport._lstStaffOwner.put(code, code);
		}else{
			DebitPayReport._lstStaffOwner.remove(code);
		}
		var lstStaff='';
		for(var i=0;i<DebitPayReport._lstStaffOwner.keyArray.length;i++){
			if(DebitPayReport._lstStaffOwner.keyArray[i]!=undefined && DebitPayReport._lstStaffOwner.keyArray[i]!=null)
				lstStaff+= DebitPayReport._lstStaffOwner.keyArray[i]+";";
		}
		lstStaff=lstStaff.substring(0,lstStaff.length-1);
		$('#staffOwnerCode').val(lstStaff);
	},
	selectProduct:function(obj,code){
		if(code=='null') return;
		if(obj.checked){
			DebitPayReport._lstProduct.put(code, code);
		}else{
			DebitPayReport._lstProduct.remove(code);
		}
		var lstProduct='';
		for(var i=0;i<DebitPayReport._lstProduct.keyArray.length;i++){
			if(DebitPayReport._lstProduct.keyArray[i]!=undefined && DebitPayReport._lstProduct.keyArray[i]!=null)
				lstProduct+= DebitPayReport._lstProduct.keyArray[i]+";";
		}
		lstProduct=lstProduct.substring(0,lstProduct.length-1);
		var os = 'productCode';
		if(DebitPayReport._objectProduct!=undefined && DebitPayReport._objectProduct!=null){
			os = DebitPayReport._objectProduct;
		}
		$('#'+os).val(lstProduct);
	},
	selectSubCat:function(obj,code){
		if(code=='null') return;
		if(obj.checked){
			DebitPayReport._lstSubCat.put(code, code);
		}else{
			DebitPayReport._lstSubCat.remove(code);
		}
		var lstSubCat='';
		for(var i=0;i<DebitPayReport._lstSubCat.keyArray.length;i++){
			if(DebitPayReport._lstSubCat.keyArray[i]!=undefined && DebitPayReport._lstSubCat.keyArray[i]!=null)
				lstSubCat+= DebitPayReport._lstSubCat.keyArray[i]+";";
		}
		lstSubCat=lstSubCat.substring(0,lstSubCat.length-1);
		var os = 'productCode';
		if(DebitPayReport._objectSubCat!=undefined && DebitPayReport._objectSubCat!=null){
			os = DebitPayReport._objectSubCat;
		}
		$('#'+os).val(lstSubCat);
	},
	reportTTKH : function(){
		if(!DebitPayReport.validateTTKH()) return false;
		var dataModel = new Object();		
		dataModel.shopId = $('#shopId').val().trim();
		dataModel.fromDate = $('#fDate').val().trim();
		dataModel.toDate = $('#tDate').val().trim();
		dataModel.typePerson = $('#objectType').val();
		//dataModel.lstCustomer = $('#customerCode').val().trim().split(";");
		//dataModel.lstStaff = $('#staffCode').val().trim().split(";");
		if($('input[name=formatType]:radio:checked').length > 0 && $('input[name=formatType]:radio:checked').val() != undefined) {
            dataModel.formatType = $('input[name=formatType]:radio:checked').val().toUpperCase();
		}
		if(dataModel.typePerson == 1)
		{
			dataModel.codePerson = $('#staffCode').val().trim();
		}
		else
		{
			dataModel.codePerson = $('#customerCode').val().trim();
		}
		
		$.getJSON("/report/are-you-shop?shopId="+dataModel.shopId,function(result)
				{
					
					if(!result.isNPP)
					{
						msg = 'Đơn vị chọn phải là nhà phân phối';
						$('#errMsg').html(msg).show();
						
					}
					else
					{
						ReportUtils.exportReport('/report/debit-pay/ttkh/export',dataModel);
					}
				});
		
		
	},
	exportPT : function () {
		$('#errMsg').html('').hide();
		var msg = '';
		if($('#shopId').val().trim().length == 0 || $('#shopId').val().trim() == activeType.ALL){
			msg = format(msgErr_required_field,'đơn vị');
		}
		
		if(msg.length == 0){
			if($('#isNPP').val() == "false"){
				msg = "Đơn vị bạn vừa chọn không phải là 1 nhà phân phối cụ thể!";
			}
		}
		
		if(msg.length == 0){
			msg = Utils.getMessageOfRequireCheck('fDate','Từ ngày');
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfInvalidFormatDate('fDate','Từ ngày');
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfRequireCheck('tDate','Đến ngày');
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfInvalidFormatDate('tDate','Đến ngày');
		}
		var fDate = $('#fDate').val();
		var tDate = $('#tDate').val();
		if(!Utils.compareDate(fDate, tDate)){
			msg = msgErr_fromdate_greater_todate;		
			$('#fDate').focus();
		}
		var currentTime = new Date();
		var month = currentTime.getMonth() + 1;
		var day = currentTime.getDate();
		var year = currentTime.getFullYear();
		if(!Utils.compareDate(fDate,day + '/' + month + '/' + year)){
			msg = 'Từ ngày không được lớn hơn ngày hiện tại';
			$('#fDate').focus();
		}
		if(msg.length > 0){
			$('#errMsg').html(msg).show();
			return false;
		}
		var data = new Object();		
		data.shopId = $('#shopId').val().trim();
		data.customerCode = $('#customerCode').val().trim();
		data.fromDate = $('#fDate').val().trim();
		data.toDate = $('#tDate').val().trim();
		if($('input[name=formatType]:radio:checked').length > 0 && $('input[name=formatType]:radio:checked').val() != undefined) {
			data.formatType = $('input[name=formatType]:radio:checked').val().toUpperCase();
      }

		var kData = $.param(data,true);
		$('#divOverlay').show();		
		$.ajax({
			type : "POST",
			url : "/report/debit-pay/pt/export",
			data :(kData),
			dataType: "json",
			success : function(data) {
				$('#divOverlay').hide();				
				if(!data.error) {
					if(data.hasData != null && data.hasData != undefined && data.hasData == true) {
						window.location.href = data.path;
						setTimeout(function(){ //Set timeout để đảm bảo file load lên hoàn tất
		                    CommonSearch.deleteFileExcelExport(data.path);
						},2000);
					} else {
						$('#errMsg').html('Không có dữ liệu để xuất').show();
					}
				} else {
					$('#errMsg').html(data.errMsg).show();					
				}
			},
			error:function(XMLHttpRequest, textStatus, errorDivThrown) {
				$('#divOverlay').hide();
				StockIssued.xhrExport = null;
			}
		});
		return false;
	}
};
/*
 * END OF FILE - /web/web/resources/scripts/business/shop-report/vnm.debit-pay-report.js
 */

/*
 * START OF FILE - /web/web/resources/scripts/business/shop-report/vnm.vansales-report.js
 */
var ExportTrans = {
		ExportTransAdd: function(){
			$('#errMsg').html('').hide();
			var msg = '';
			var chot = '';
			if($('#shopCode').val().trim().length == 0 || $('#shopCode').val().trim() == activeType.ALL){
				msg = format(msgErr_required_field,'đơn vị');
			}
			if(msg.length == 0) {
				if($('#isNPP').val() == "false") {
					msg = "Đơn vị bạn vừa chọn không phải là 1 nhà phân phối cụ thể!";
				}
			}
			if(msg.length == 0){
				msg = Utils.getMessageOfRequireCheck('staffCode','NVBH');
			}
			if(msg.length == 0){
				msg = Utils.getMessageOfRequireCheck('transacName','Tên lệnh điều động');
			}
			if(msg.length == 0){
				msg = Utils.getMessageOfRequireCheck('transacDate','Ngày');
			}
			if(msg.length == 0){
				msg = Utils.getMessageOfRequireCheck('transacContent','Nội dung');
			}
			if(msg.length == 0){
				msg = Utils.getMessageOfSpecialCharactersValidate('staffCode','NPP',Utils._CODE);
			}	
			if(msg.length == 0){
				msg = Utils.getMessageOfInvalidFormatDate('transacDate','Ngày');
			}
			if(msg.length > 0){
				$('#errMsg').html(msg).show();
				$('#'+chot).focus();
				return false;
			}
			var exportTransaction = $("#exportTransaction").val();
			if(exportTransaction == null || exportTransaction == -1 || exportTransaction == "" || exportTransaction == undefined){
				$('#errMsg').html("Vui lòng chọn giao dịch xuất trong ngày").show();
				$('#exportTransaction').focus();
				return false;
			}
			if(msg.length > 0){
				$('#errMsg').html(msg).show();
				$('#'+chot).focus();
				return false;
			}
			var dataModel = new Object();		
			dataModel.shopCode = $('#shopCode').val().trim();
			dataModel.staffCode = $('#staffCode').val().trim();
			dataModel.stockTransId = $('#exportTransaction').val().trim();
			dataModel.transacDate = $('#transacDate').val().trim();
			dataModel.transacName = $('#transacName').val().trim();
			dataModel.transacContent = $('#transacContent').val().trim();
			dataModel.exportTransaction = $('#exportTransaction').val().trim();
			//ReportUtils.exportReport('/report/exportandtrans/viewexport',dataModel);
			var kData = $.param(dataModel,true);
			$('#divOverlay').show();		
			$.ajax({
				type : "POST",
				url : "/report/exportandtrans/viewexport",
				data :(kData),
				dataType: "json",
				success : function(data) {
					$('#divOverlay').hide();				
					if(!data.error) {
						if(data.hasData != undefined && data.hasData != null && data.hasData == false) {
							$('#errMsg').html('Không có dữ liệu để xuất báo cáo!').show();
						} else {
							window.location.href = data.path;
							setTimeout(function(){ //Set timeout để đảm bảo file load lên hoàn tất
	                            CommonSearch.deleteFileExcelExport(data.path);
							},2000);
						}
					} else {
						$('#errMsg').html(data.errMsg).show();					
					}
				},
				error:function(XMLHttpRequest, textStatus, errorDivThrown) {
					$('#divOverlay').hide();
					StockIssued.xhrExport = null;
				}
			});
		},
		getListStockTrans: function(staffCode, objStockTrans, stockTransId,isAll){
			if(staffCode != null && staffCode != undefined){
				$.ajax({
					type: "POST",
					url: "/report/exportandtrans/getliststocktrans",
					dataType: "json",
					data: ({staffCode:staffCode}),		        
					success: function(data) {
						//if(data!= null && data!= undefined && data.lstStockTrans!= null && data.lstStockTrans.length > 0){
						//data.hasData != null && data.hasData != undefined &&
						if( data.hasData == true ) {
							var html = new Array();
							if(isAll == undefined || isAll == null){
								html.push("<option value='-1'>[Chọn giao dịch xuất]</option>");
							}else{
								html.push("<option value='-1'>Tất cả</option>");
							}
							for(var i=0;i<data.lstStockTrans.length;i++){
								html.push("<option value='"+ data.lstStockTrans[i].id +"'>"+ Utils.XSSEncode(data.lstStockTrans[i].stockTransCode) +"</option>");
							}
							$(objStockTrans).html(html.join(""));
							if(stockTransId!= undefined && stockTransId > 0){
								$(objStockTrans).val(stockTransId);
								$(objStockTrans).change();
							}else{
								$(objStockTrans).change();
							}
						}else {
							$(objStockTrans).html("<option value='-1'>[Không có giao dịch xuất]</option>").change();
						}			        			        	
					},
					error: function (XMLHttpRequest, textStatus, errorThrown) {}
				});
			}else{
				if(isAll == undefined || isAll == null){
					$(objStockTrans).html("<option value='-1'>[Không có giao dịch xuất]</option>").change();
				}else{
					$(objStockTrans).html("<option value='-1'>Tất cả</option>").change();
				}
			}	
		},
		validateStaff: function(staffCode,shopId){
			if(staffCode != null && staffCode != undefined && shopId != null && shopId > 0){
				$.ajax({
					type: "POST",
					url: "/report/exportandtrans/validatestaff",
					dataType: "json",
					data: ({staffCode:staffCode,shopId:shopId}),		        
					success: function(data) {
						if (data.error) {
							var errMsg = 'NVBH khộng thuộc về đơn vị';
							$("#errMsg").html(errMsg).show();
							$("#staffCode").val('');
						}
					},
					error: function (XMLHttpRequest, textStatus, errorThrown) {
					}
				});
			}	
		},
		reportBCXNKNVBH : function(){//SangTN: Bao cao xuat nhap khau nhan vien ban hang
			$('#errMsg').html('').hide();
			var msg = '';
			if($('#shopId').val().trim().length == 0 || $('#shopId').val().trim() == activeType.ALL){
				msg = format(msgErr_required_field,'đơn vị');
			}
			if(msg.length == 0){
				if($('#isNPP').val() == "false"){
					msg = "Đơn vị bạn vừa chọn không phải là 1 nhà phân phối cụ thể!";
				}
			}
			if(msg.length == 0){
				msg = Utils.getMessageOfRequireCheck('fDate','Từ ngày');
			}
			if(msg.length == 0){
				msg = Utils.getMessageOfInvalidFormatDate('fDate','Từ ngày');
			}
			if(msg.length == 0){
				msg = Utils.getMessageOfRequireCheck('tDate','Đến ngày');
			}
			if(msg.length == 0){
				msg = Utils.getMessageOfInvalidFormatDate('tDate','Đến ngày');
			}
			var fDate = $('#fDate').val();
			var tDate = $('#tDate').val();
			if(!Utils.compareDate(fDate, tDate)){
				msg = msgErr_fromdate_greater_todate;		
				$('#fDate').focus();
			}
			var currentTime = new Date();
			var month = currentTime.getMonth() + 1;
			var day = currentTime.getDate();
			var year = currentTime.getFullYear();
			if(!Utils.compareDate(fDate,day + '/' + month + '/' + year)){
				msg = 'Từ ngày không được lớn hơn ngày hiện tại';
				$('#fDate').focus();
			}
			if(msg.length > 0){
				$('#errMsg').html(msg).show();
				return false;
			}
			var data = new Object();	
			if($('input[name=formatType]:radio:checked').length > 0 && $('input[name=formatType]:radio:checked').val() != undefined) {
				data.formatType = $('input[name=formatType]:radio:checked').val().toUpperCase();
			}
			data.shopId = $('#shopId').val().trim();
			data.staffCode = $('#staffCode').val().trim();
			data.fromDate = $('#fDate').val().trim();
			data.toDate = $('#tDate').val().trim();
			data.toDate = $('#tDate').val().trim();
			data.typeBill = $('#typeBill').val().trim();
			if($('input[name=formatType]:radio:checked').length > 0 && $('input[name=formatType]:radio:checked').val() != undefined) {
				data.formatType = $('input[name=formatType]:radio:checked').val().toUpperCase();
          }

			var kData = $.param(data,true);
			$('#divOverlay').show();		
			$.ajax({
				type : "POST",
				url : "/report/vansales/xnknvbh/export",
				data :(kData),
				dataType: "json",
				success : function(data) {
					$('#divOverlay').hide();				
					if(!data.error) {
						if(data.hasData != null && data.hasData != undefined && data.hasData == true) {
							window.location.href = data.path;
							setTimeout(function(){ //Set timeout để đảm bảo file load lên hoàn tất
			                    CommonSearch.deleteFileExcelExport(data.path);
							},2000);
						} else {
							$('#errMsg').html('Không có dữ liệu để xuất!').show();
						}
					} else {
						$('#errMsg').html(data.errMsg).show();					
					}
				},
				error:function(XMLHttpRequest, textStatus, errorDivThrown) {
					$('#divOverlay').hide();
					StockIssued.xhrExport = null;
				}
			});
			return false;
		},
		exportPXKKVCNB: function(){
			$('#errMsg').html('').hide();
			var msg = "";
			if($('#shopId').val().trim().length == 0 || $('#shopId').val().trim() == activeType.ALL){
				msg = format(msgErr_required_field,' Đơn vị');
			}
			if(msg.length == 0){
				if($('#isNPP').val() == "false"){
					msg = "Đơn vị bạn vừa chọn không phải là 1 nhà phân phối cụ thể!";
				}
			}
			if(msg.length == 0){
				msg = Utils.getMessageOfRequireCheck('staffCode','NVBH');
			}
			if(msg.length > 0) {
				$('#errMsg').html(msg).show();
				return false;
			}
			var exportTransaction = $("#idTransCode").val();
			if(exportTransaction == null || exportTransaction == -1 || exportTransaction == "" || exportTransaction == undefined){
				$('#errMsg').html("Vui lòng chọn giao dịch xuất trong ngày").show();
				$('#idTransCode').focus();
				return false;
			}
			if(msg.length == 0){
				msg = Utils.getMessageOfRequireCheck('ipNameTrans','Tên lệnh điều động');
			}
			if(msg.length == 0) {
				msg = Utils.getMessageOfRequireCheck('fDate','Từ ngày');
			}
			if(msg.length == 0)	{
				msg = Utils.getMessageOfInvalidFormatDate('fDate','Từ ngày');
			}
			if(msg.length == 0){
				msg = Utils.getMessageOfRequireCheck('ipContentTrans','Nội dung');
			}			
			if(msg.length > 0) {
				$('#errMsg').html(msg).show();
				return false;
			}
			var data = new Object();		
			data.shopId = $('#shopId').val().trim();
			data.staffCode = $('#staffCode').val().trim();
			data.fromDate = $('#fDate').val().trim();
			var tt = document.getElementById('idTransCode');
		    var selIndex = tt.selectedIndex;
		    var selValue = tt.options[selIndex].value;
			data.transacCode = selValue;
						
			data.transacContent = $('#ipContentTrans').val().trim();
			data.transacName = $('#ipNameTrans').val().trim();
			if($('input[name=formatType]:radio:checked').length > 0 && $('input[name=formatType]:radio:checked').val() != undefined) {
                data.formatType = $('input[name=formatType]:radio:checked').val().toUpperCase();
			}
			var kData = $.param(data,true);
			$('#divOverlay').show();		
			$.ajax({
				type : "POST",
				url : "/report/vansales/pxkkvcnb/export",
				data :(kData),
				dataType: "json",
				success : function(data) {
					$('#divOverlay').hide();				
					if(!data.error) {
						if(data.hasData != null && data.hasData != undefined && data.hasData == true) {
							window.location.href = data.path;
							setTimeout(function(){ //Set timeout để đảm bảo file load lên hoàn tất
			                    CommonSearch.deleteFileExcelExport(data.path);
							},2000);
						}else {
							$('#errMsg').html('Không có dữ liệu để xuất').show();
						}					
					} else {
						$('#errMsg').html(data.errMsg).show();
					}
				},
				error:function(XMLHttpRequest, textStatus, errorDivThrown) {
					$('#divOverlay').hide();
					StockIssued.xhrExport = null;
				}
			});
		}
};
//HungTT27
var SaleStaff = {
		reportSaleStaff: function(){
			$('#errMsg').html('').hide();
			var msg = '';
			if($('#shopId').val().trim().length == 0 || $('#shopId').val().trim() == activeType.ALL){
				msg = format(msgErr_required_field,'đơn vị');
			}			
			if(msg.length == 0){
				if($('#isNPP').val() == "false"){
					msg = "Đơn vị bạn vừa chọn không phải là 1 nhà phân phối cụ thể!";
				}
			}
			if(msg.length == 0){
				msg = Utils.getMessageOfRequireCheck('fDate','Từ ngày');
			}
			if(msg.length == 0){
				msg = Utils.getMessageOfInvalidFormatDate('fDate','Từ ngày');
			}
			if(msg.length == 0){
				msg = Utils.getMessageOfRequireCheck('tDate','Đến ngày');
			}
			if(msg.length == 0){
				msg = Utils.getMessageOfInvalidFormatDate('tDate','Đến ngày');
			}
			var fDate = $('#fDate').val();
			var tDate = $('#tDate').val();
			if(!Utils.compareDate(fDate, tDate)){
				msg = msgErr_fromdate_greater_todate;		
				$('#fDate').focus();
			}
			var currentTime = new Date();
			var month = currentTime.getMonth() + 1;
			var day = currentTime.getDate();
			var year = currentTime.getFullYear();
			if(!Utils.compareDate(fDate,day + '/' + month + '/' + year)){
				msg = 'Từ ngày không được lớn hơn ngày hiện tại';
				$('#fDate').focus();
			}
			if(msg.length > 0){
				$('#errMsg').html(msg).show();
				return false;
			}
			var data = new Object();		
			data.shopId = $('#shopId').val().trim();
			data.staffCode = $('#staffCode').val().trim();
			data.fromDate = $('#fDate').val().trim();
			data.toDate = $('#tDate').val().trim();
			if($('input[name=formatType]:radio:checked').length > 0 && $('input[name=formatType]:radio:checked').val() != undefined) {
                data.formatType = $('input[name=formatType]:radio:checked').val().toUpperCase();
			}

			var kData = $.param(data,true);
			$('#divOverlay').show();		
			$.ajax({
				type : "POST",
				url : "/report/vansales/xbtnv/export",
				data :(kData),
				dataType: "json",
				success : function(data) {
					$('#divOverlay').hide();				
					if(!data.error) {
						if(data.hasData != null && data.hasData != undefined && data.hasData == true) {
							window.location.href = data.path;
							setTimeout(function(){ //Set timeout để đảm bảo file load lên hoàn tất
			                    CommonSearch.deleteFileExcelExport(data.path);
							},2000);
						} else {
							$('#errMsg').html('Không có dữ liệu để xuất').show();
						}
					} else {
						$('#errMsg').html(data.errMsg).show();					
					}
				},
				error:function(XMLHttpRequest, textStatus, errorDivThrown) {
					$('#divOverlay').hide();
					StockIssued.xhrExport = null;
				}
			});
			return false;
			
//			var dataModel = new Object();		
//			dataModel.shopId = $('#shopCode').val().trim();
//			dataModel.fromDate = $('#fDate').val().trim();
//			dataModel.toDate = $('#tDate').val().trim();
//			dataModel.staffCode = $('#staffCode').val().trim();
//			ReportUtils.exportReport('/report/vansales/view',dataModel);
		}
};
var salePo = {
		PoAdd: function(){
			var msg = '';
			$('#errMsg').html('').hide();
			if($('#shopId').val().trim().length == 0 || $('#shopId').val().trim() == activeType.ALL){
				msg = format(msgErr_required_field,'đơn vị');
			}
			
			var chot = '';
			if (msg == ""){
				msg = Utils.getMessageOfRequireCheck('fDate', 'Từ ngày', false);
				chot = 'fDate';
			}
			if (msg == ""){
				ms = Utils.getMessageOfRequireCheck('tDate', 'Đến ngày', false);
				chot = 'tDate';
			}
			if(msg.length > 0){
				$('#errMsg').html(msg).show();
				$('#'+chot).focus();
				return false;
			}
			if(msg.length == 0){
				msg = Utils.getMessageOfInvalidFormatDate('fDate','Từ ngày');
			}
			if(msg.length == 0){
				msg = Utils.getMessageOfInvalidFormatDate('tDate','Đến ngày');
			}
			var fDate = $('#fDate').val();
			var tDate = $('#tDate').val();
			if(!Utils.compareDate(fDate, tDate)){
				msg = msgErr_fromdate_greater_todate;		
				$('#fDate').focus();
			}
			if(msg.length > 0){
				$('#errMsg').html(msg).show();
				return false;
			}
			var dataModel = new Object();		
			dataModel.shopId = $('#shopId').val().trim();
			dataModel.fromDate = $('#fDate').val().trim();
			dataModel.toDate = $('#tDate').val().trim();
			dataModel.lstStaffId = SuperviseCustomer.mapNVBH.keyArray;
			ReportUtils.exportReport('/report/sales-revenue/poView',dataModel);
		}
};
/*
 * END OF FILE - /web/web/resources/scripts/business/shop-report/vnm.vansales-report.js
 */

/*
 * START OF FILE - /web/web/resources/scripts/business/shop-report/vnm.customer-report.js
 */
var CustomerReport = {	
	exportDSKHTTBH : function(){
		var msg = '';
		$('#errMsg').html('').hide();
		if($('#shopId').val().trim().length == 0 || $('#shopId').val().trim() == activeType.ALL){
			msg = format(msgErr_required_field,'Đơn vị');
		}
		
		if(msg.length == 0){
			if($('#isNPP').val() == "false" || $('#isNPP').val()=='0'){
				msg = "Đơn vị bạn vừa chọn không phải là 1 nhà phân phối cụ thể!";
			}
		}
		
		if(msg.length > 0){
			$('#errMsg').html(msg).show();
			return false;
		}
		var dataModel = new Object();
		if($('input[name=formatType]:radio:checked').length > 0 && $('input[name=formatType]:radio:checked').val() != undefined) {
            dataModel.formatType = $('input[name=formatType]:radio:checked').val().toUpperCase();
		}
		dataModel.shopId = $('#shopId').val().trim();
		dataModel.saleStaffCode = $('#saleStaffCode').val().trim();
		dataModel.customerCode = $('#customerCode').val().trim();
		dataModel.customerName = $('#customerName').val().trim();
		ReportUtils.exportReport('/report/customer/dskhttbh/export',dataModel,'errMsg');
	},
	exportDSKH: function(){
		var msg = '';
		$('#errMsg').html('').hide();
		if($('#shopId').val().trim().length == 0 || $('#shopId').val().trim() == activeType.ALL){
			msg = format(msgErr_required_field,'Đơn vị');
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfRequireCheck('kindReport','Loại báo cáo',true);
			//Truyen chu "true" vao vi minh doc code thay: getMessageOfRequireCheck: function(objectId, objectName,isSelectBox)
			//isSelectBox la bien boolean phan biet selecBox & textField.
		}
//		if(msg.length == 0){
//			msg = Utils.getMessageOfRequireCheck('shopCode','Mã đơn vị');
//		}
//		if(msg.length == 0){
//			msg = Utils.getMessageOfInvalidFormatDate('saleStaffCode','Mã NVBH');
//		}
//		if(msg.length == 0){
//			msg = Utils.getMessageOfRequireCheck('customerCode','Mã KH');
//		}
//		if(msg.length == 0){
//			msg = Utils.getMessageOfInvalidFormatDate('customerName','Tên KH');
//		}
		if(msg.length > 0){
			$('#errMsg').html(msg).show();
			return false;
		}
		var dataModel = new Object();
		if($('input[name=formatType]:radio:checked').length > 0 && $('input[name=formatType]:radio:checked').val() != undefined) {
            dataModel.formatType = $('input[name=formatType]:radio:checked').val().toUpperCase();
		}
		dataModel.typeReport = $('#kindReport').val();
		dataModel.status = $('#status').val();
//		dataModel.shopCode = $('#shopCode').val().trim();
		dataModel.shopId = $('#shopId').val().trim();
		dataModel.saleStaffCode = $('#saleStaffCode').val().trim();
		dataModel.customerCode = $('#customerCode').val().trim();
		dataModel.customerName = $('#customerName').val().trim();
		ReportUtils.exportReport('/report/customer/dskh/export', dataModel);
		/*var kData = $.param(dataModel,true);
		$('#divOverlay').show();		
		$.ajax({
			type : "POST",
			url : "/report/customer/dskh/export",
			data :(kData),
			dataType: "json",
			success : function(data) {
				$('#divOverlay').hide();				
				if(!data.error) {
					window.location.href = data.path;
					setTimeout(function(){ //Set timeout để đảm bảo file load lên hoàn tất
	                    CommonSearch.deleteFileExcelExport(data.path);
					},2000);
				} else {
					$('#errMsg').html(data.errMsg).show();					
				}
			},
			error:function(XMLHttpRequest, textStatus, errorDivThrown) {
				$('#divOverlay').hide();
				StockIssued.xhrExport = null;
			}
		});*/
		return false;
	},
	exportKD5: function(){
		var fTmp = '';
		var tTmp = '';
		var msg = '';
		$('#errMsg').html('').hide();
		var fTitle = 'Từ ngày';
		var tTitle = 'Đến ngày';
		if($('#type').val().trim() == 17){
			fTitle = 'Từ tháng';
			tTitle = 'Đến tháng';
			if(msg.length == 0){
				msg = Utils.getMessageOfRequireCheck('fDate',fTitle);
			}
			if(msg.length == 0){
				msg = Utils.getMessageOfRequireCheck('tDate',tTitle);
			}
			fTmp = $('#fDate').val().trim();
			tTmp = $('#tDate').val().trim();
			$('#fDate').val("01/"+fTmp);
			$('#tDate').val("01/"+tTmp);
			var fDate = $('#fDate').val();
			var tDate = $('#tDate').val();
			if(!Utils.compareDate(fDate, tDate)){
				msg = fTitle +" không được lớn hơn "+ tTitle;		
				$('#fDate').focus();
			}
			$('#fDate').val(fTmp);
			$('#tDate').val(tTmp);
		}else{
			if(msg.length == 0){
				msg = Utils.getMessageOfRequireCheck('fDate',fTitle);
			}
			if(msg.length == 0){
				msg = Utils.getMessageOfInvalidFormatDate('fDate',fTitle);
			}
			var fDate = $('#fDate').val();
			var tDate = $('#tDate').val();
			if(!Utils.compareDate(fDate, tDate)){
				msg = msgErr_fromdate_greater_todate;		
				$('#fDate').focus();
			}
			var currentTime = new Date();
			var month = currentTime.getMonth() + 1;
			var day = currentTime.getDate();
			var year = currentTime.getFullYear();
			if(!Utils.compareDate(fDate,day + '/' + month + '/' + year)){
				msg = 'Từ ngày không được lớn hơn ngày hiện tại';
				$('#fDate').focus();
			}
			if(msg.length == 0){
				msg = Utils.getMessageOfRequireCheck('tDate',tTitle);
			}
			if(msg.length == 0){
				msg = Utils.getMessageOfInvalidFormatDate('tDate',tTitle);
			}
			if(msg.length == 0 && !Utils.compareCurrentDate(tDate)) {
				msg = 'Đến ngày phải nhỏ hơn ngày hiện tại. Vui lòng nhập lại';
				$('#tDate').focus();
			}
		}
		if(msg.length > 0){
			$('#errMsg').html(msg).show();
			return false;
		}
		var dataModel = new Object();	
		dataModel.fromDate = $('#fDate').val().trim();
		dataModel.toDate = $('#tDate').val().trim();
		if($('#type').val().trim() == 17){
			dataModel.fromDate = "01/"+$('#fDate').val().trim();
			dataModel.toDate = "01/"+$('#tDate').val().trim();
		}
		dataModel.type = $('#type').val().trim();
		$('#typeReport').val($('#type').val().trim());
		ReportUtils.previewReport('/report/crm/kd5/export',dataModel);

	},
//end-me	
	
	export7_5_1TH: function() {
		$('#errMsg').hide();
		var msg = '';
		msg = Utils.getMessageOfRequireCheck('shopId', 'Đơn vị');
		$('#shop').focus();
		
		if (msg.length == 0) {
			msg = Utils.getMessageOfRequireCheck('rptType', 'Loại báo cáo', true);
		}
		
		if (msg.length > 0) {
			$('#errMsg').html(msg).show();
			return;
		}
		var dataModel = new Object();		
		dataModel.shopId = $('#shopId').val().trim();
		if ($('#rptType').val().trim() == 'TH') {
			ReportUtils.exportReport('/report/customer/tnpt-th/export', dataModel);
		} else {
			ReportUtils.exportReport('/report/customer/tnpt-ct/export', dataModel);
		}
	},
	
	export7_5_1CT: function() {
		$('#errMsg').hide();
		var dataModel = new Object();
		ReportUtils.exportReport('/report/customer/tnpt-ct/export', dataModel);
	}
};
/*
 * END OF FILE - /web/web/resources/scripts/business/shop-report/vnm.customer-report.js
 */

/*
 * START OF FILE - /web/web/resources/scripts/business/shop-report/vnm.importBonus-report.js
 */
//HungTT27
var ImportBonus = {
		reportImportBonus: function(){
			var msg = '';
			$('#errMsg').html('').hide();
			if(msg.length == 0){
				msg = Utils.getMessageOfRequireCheck('fDate', 'Từ ngày');
			}
			if(msg.length == 0){
				msg = Utils.getMessageOfInvalidFormatDate('fDate','Từ ngày');
			}
			if(msg.length == 0){
				msg = Utils.getMessageOfRequireCheck('tDate','Đến ngày');
			}
			if(msg.length == 0){
				msg = Utils.getMessageOfInvalidFormatDate('tDate','Đến ngày');
			}
			var fDate = $('#fDate').val();
			var tDate = $('#tDate').val();
			if(!Utils.compareDate(fDate, tDate)){
				msg = msgErr_fromdate_greater_todate;		
				$('#fDate').focus();
			}
			var ccDate = getCurrentDate();		
			if(!Utils.compareDate(tDate, ccDate)){
				msg = 'Đến ngày không được lớn hơn ngày hiện tại';		
				$('#tDate').focus();
			}
			if(msg.length > 0){
				$('#errMsg').html(msg).show();
				return false;
			}
			var dataModel = new Object();		
			dataModel.fromDate = $('#fDate').val().trim();
			dataModel.toDate = $('#tDate').val().trim();
			ReportUtils.exportReport('/report/importBonusAndSaleGroup/view',dataModel);
		}
};
/*
 * END OF FILE - /web/web/resources/scripts/business/shop-report/vnm.importBonus-report.js
 */

/*
 * START OF FILE - /web/web/resources/scripts/business/shop-report/vnm.crm-report.js
 */
var CRMReportDSPPTNH = {
		_isCheckAll: false,
		_lstChecker: null,
		_lstUnchecker: null,
		_arrMultiChoiceDisplay: null,
		tDateChangeByfDateIntoFirtMonth : function (){
			
		},
		/* BAO CAO DOANH SO PHAN PHOI THEO NHOM HANG */
		selectProduct:function(obj,code){
			if(code=='null') return;
			if(obj.checked){
				CRMReportDSPPTNH._listProductCode.push(code);
			}else{
				removeE(CRMReportDSPPTNH._listProductCode,code);
			}
		},
		
		exportDSPPTNH: function(){
			var msg = '';
			$('#errMsg').html('').hide();
			if(msg.length == 0){
				msg = Utils.getMessageOfRequireCheck('tDate','Đến ngày');
			}
			if(msg.length == 0){
				msg = Utils.getMessageOfInvalidFormatDate('tDate','Đến ngày');
			}
			var currentTime = new Date();
			var month = currentTime.getMonth() + 1;
			var day = currentTime.getDate();
			var year = currentTime.getFullYear();
			if(!Utils.compareDate($('#tDate').val().trim(),day + '/' + month + '/' + year)){
				msg = 'Đến ngày không được lớn hơn ngày hiện tại';
				$('#tDate').focus();
			}
			if(msg.length > 0){
				$('#errMsg').html(msg).show();
				return false;
			}
			var dataModel = new Object();		
			dataModel.toDate = $('#tDate').val().trim();
			dataModel.shopId = $('#shopId').val().trim();
			dataModel.productCode = $('#productCode').val().trim();
			ReportUtils.exportReport('/report/crm/bcds-pptnh/export',dataModel);
		},
		/* BAO CAO KD 19.1s*/
		exportKD19_1 : function(){
			var msg = '';
			$('#errMsg').html('').hide();
			if(msg.length == 0){
				msg = Utils.getMessageOfRequireCheck('fDate','Từ ngày');
			}
			if(msg.length == 0){
				msg = Utils.getMessageOfInvalidFormatDate('fDate','Từ ngày');
			}
			if(msg.length == 0){
				msg = Utils.getMessageOfRequireCheck('tDate','Đến ngày');
			}
			if(msg.length == 0){
				msg = Utils.getMessageOfInvalidFormatDate('tDate','Đến ngày');
			}
			if(msg.length == 0){
				msg = Utils.getMessageOfRequireCheck('number','Số lần');
			}
			var fDate = $('#fDate').val();
			var tDate = $('#tDate').val();
			var curDate = ReportUtils.getCurrentDateString();
			if(msg.length == 0 && !Utils.compareDate(fDate, tDate)){
				msg = msgErr_fromdate_greater_todate;
				$('#fDate').focus();
			}
			if(msg.length == 0 && !Utils.compareDate(tDate, curDate)){
				msg = "Đến ngày không được lớn hơn ngày hiện tại";
				$('#tDate').focus();
			}
			if(msg.length == 0 && fDate.split('/')[1] != tDate.split('/')[1]){
				msg = "Đến ngày phải cùng tháng với Từ ngày.";
				$('#tDate').focus();
			}
			if(msg.length == 0){
				msg = Utils.getMessageOfSpecialCharactersValidateEx('number','Số lần',Utils._TF_NUMBER);
			}
			if(msg.length > 0){
				$('#errMsg').html(msg).show();
				return false;
			}
			var dataModel = new Object();
			dataModel.fromDate = $('#fDate').val().trim();
			dataModel.toDate = $('#tDate').val().trim();
			dataModel.shopId = $('#shopId').val().trim();
			dataModel.solan = $('#number').val().trim();
			ReportUtils.exportReport('/report/crm/kd19-1/export',dataModel);
		},
		/**
		 * Xuất báo cáo KD1.2 Doanh số phân phối theo nhãn hàng
		 * @author tungtt21
		 * @returns {Boolean}
		 */
		exportKD1_2: function(){
			var msg = '';
			var multiShop = $("#shop").data("kendoMultiSelect");
			var dataShop = multiShop.dataItems();
			$('#errMsg').html('').hide();
			
			if(msg.length == 0){
				if(dataShop == null || dataShop.length == 0){
					msg = "Bạn chưa chọn đơn vị. Vui lòng chọn đơn vị.";
				}
			}
			if(msg.length == 0){
				msg = Utils.getMessageOfRequireCheck('tDate','Đến ngày');
			}
			if(msg.length == 0){
				msg = Utils.getMessageOfInvalidFormatDate('tDate','Đến ngày');
			}		
			if(msg.length > 0){
				$('#errMsg').html(msg).show();
				return false;
			}
			
			var dataModel = new Object();			
			dataModel.toDate = $('#tDate').val().trim();
			dataModel.productCode = $('#product').val().trim();
			if(dataShop!=null && dataShop.length>0){
				var lstShop = dataShop[0].id;
				for(var i=1;i<dataShop.length;i++){
					lstShop += "," + dataShop[i].id;
				}
				dataModel.listShopId = lstShop;
			}
			
			var kData = $.param(dataModel,true);
			$('#divOverlay').show();		
			$.ajax({
				type : "POST",
				url : "/report/crm/kd1_2/export",
				data :(kData),
				dataType: "json",
				success : function(data) {
					$('#divOverlay').hide();				
					if(!data.error) {
						if(data.hasData != null && data.hasData != undefined && data.hasData == true) {
							window.location.href = data.path;
							setTimeout(function(){ //Set timeout để đảm bảo file load lên hoàn tất
			                    CommonSearch.deleteFileExcelExport(data.path);
							},2000);
						} else {
							$('#errMsg').html('Không có dữ liệu để xuất').show();
						}
					} else {
						$('#errMsg').html(data.errMsg).show();					
					}
				},
				error:function(XMLHttpRequest, textStatus, errorDivThrown) {
					$('#divOverlay').hide();
					StockIssued.xhrExport = null;
				}
			});
			return false;
		},
		/**
		 * Xuất báo cáo KD1.3 Doanh số phân phối theo nhóm hàng
		 * @author tungtt21
		 * @returns {Boolean}
		 */
		exportKD1_3: function(){
			var msg = '';
			var multiShop = $("#shop").data("kendoMultiSelect");
			var dataShop = multiShop.dataItems();
			$('#errMsg').html('').hide();
			
			if(msg.length == 0){
				if(dataShop == null || dataShop.length == 0){
					msg = "Bạn chưa chọn đơn vị. Vui lòng chọn đơn vị.";
				}
			}
			if(msg.length == 0){
				msg = Utils.getMessageOfRequireCheck('tDate','Đến ngày');
			}
			if(msg.length == 0){
				msg = Utils.getMessageOfInvalidFormatDate('tDate','Đến ngày');
			}		
			if(msg.length > 0){
				$('#errMsg').html(msg).show();
				return false;
			}
			
			var dataModel = new Object();			
			dataModel.toDate = $('#tDate').val().trim();
			dataModel.productCode = $('#product').val().trim();
			if(dataShop!=null && dataShop.length>0){
				var lstShop = dataShop[0].id;
				for(var i=1;i<dataShop.length;i++){
					lstShop += "," + dataShop[i].id;
				}
				dataModel.listShopId = lstShop;
			}
			
			var kData = $.param(dataModel,true);
			$('#divOverlay').show();		
			$.ajax({
				type : "POST",
				url : "/report/crm/kd1_3/export",
				data :(kData),
				dataType: "json",
				success : function(data) {
					$('#divOverlay').hide();				
					if(!data.error) {
						if(data.hasData != null && data.hasData != undefined && data.hasData == true) {
							window.location.href = data.path;
							setTimeout(function(){ //Set timeout để đảm bảo file load lên hoàn tất
			                    CommonSearch.deleteFileExcelExport(data.path);
							},2000);
						} else {
							$('#errMsg').html('Không có dữ liệu để xuất').show();
						}
					} else {
						$('#errMsg').html(data.errMsg).show();					
					}
				},
				error:function(XMLHttpRequest, textStatus, errorDivThrown) {
					$('#divOverlay').hide();
					StockIssued.xhrExport = null;
				}
			});
			return false;
		},
		exportKD13: function(){
			var fTmp = '';
			var tTmp = '';
			var msg = '';
			$('#errMsg').html('').hide();
			if($('#shopId').val().trim().length == 0 || $('#shopId').val().trim() == activeType.ALL){
				msg = format(msgErr_required_field,'Đơn vị');
			}
			fTitle = 'Từ tháng';
			tTitle = 'Đến tháng';
			if(msg.length == 0){
				msg = Utils.getMessageOfRequireCheck('fDate',fTitle);
			}
			if(msg.length == 0){
				msg = Utils.getMessageOfRequireCheck('tDate',tTitle);
			}
			fTmp = $('#fDate').val().trim();
			tTmp = $('#tDate').val().trim();
			$('#fDate').val("01/"+fTmp);
			$('#tDate').val("01/"+tTmp);
			var fDate = $('#fDate').val();
			var tDate = $('#tDate').val();
			if(!Utils.compareDate(fDate, tDate)){
				msg = fTitle +" không được lớn hơn "+ tTitle;		
				$('#fDate').focus();
			}
			$('#fDate').val(fTmp);
			$('#tDate').val(tTmp);
			
			var currentTime = new Date();
			var month = currentTime.getMonth() + 1;
			var day = currentTime.getDate();
			var year = currentTime.getFullYear();
			if(!Utils.compareDate(tDate,day + '/' + month + '/' + year)){
				msg = 'Đến tháng không được lớn hơn tháng/năm hiện tại';
				$('#tDate').focus();
			}
			
			if(msg.length > 0){
				$('#errMsg').html(msg).show();
				return false;
			}
			var dataModel = new Object();		
			dataModel.shopId = $('#shopId').val().trim();
			dataModel.fromDate =  "01/"+$('#fDate').val().trim();
			dataModel.toDate = "01/"+$('#tDate').val().trim();
			ReportUtils.exportReport('/report/crm/kd13/export',dataModel);
		},
		/* KD10.3 Tổng kết thực hiện DS,PP theo mức tham gia */
		exportTKTHDSPPTMTG: function(){
			var msg = '';
			$('#errMsg').html('').hide();
			if(msg.length == 0){
				msg = Utils.getMessageOfRequireCheck('displayProgramCode','Mã CTTB');
			}
			if(msg.length == 0){
				msg = Utils.getMessageOfRequireCheck('productCode','Nhãn hàng');
			}
			if(msg.length == 0){
				msg = Utils.getMessageOfRequireCheck('tDate','Đến ngày');
			}
			if(msg.length == 0){
				msg = Utils.getMessageOfInvalidFormatDate('tDate','Đến ngày');
			}
			if(msg.length > 0){
				$('#errMsg').html(msg).show();
				return false;
			}
			var dataModel = new Object();		
			dataModel.toDate = $('#tDate').val().trim();
			dataModel.shopId = $('#shopId').val().trim();
			dataModel.displayProgramCode = $('#displayProgramCode').val().trim();
			dataModel.productCode = $('#productCode').val().trim();
			//ReportUtils.exportReport('/report/crm/kd10-3/export',dataModel);
			var kData = $.param(dataModel,true);
			$('#divOverlay').show();		
			$.ajax({
				type : "POST",
				url : "/report/crm/kd10-3/export",
				data :(kData),
				dataType: "json",
				success : function(data) {
					$('#divOverlay').hide();				
					if(!data.error) {
						if(data.hasData != null && data.hasData != undefined && data.hasData == true) {
							window.location.href = data.path;
							setTimeout(function(){ //Set timeout để đảm bảo file load lên hoàn tất
			                    CommonSearch.deleteFileExcelExport(data.path);
							},2000);
						} else {
							$('#errMsg').html('Không có dữ liệu để xuất').show();
						}
					} else {
						$('#errMsg').html(data.errMsg).show();					
					}
				},
				error:function(XMLHttpRequest, textStatus, errorDivThrown) {
					$('#divOverlay').hide();
					StockIssued.xhrExport = null;
				}
			});
			return false;
		},
	exportDSBHNVBHSKU: function(){
		var msg = '';
		$('#errMsg').html('').hide();
		if(msg.length == 0){
			msg = Utils.getMessageOfRequireCheck('fDate','Từ ngày');
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfInvalidFormatDate('fDate','Từ ngày');
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfRequireCheck('tDate','Đến ngày');
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfInvalidFormatDate('tDate','Đến ngày');
		}
		var fDate = $('#fDate').val();
		var tDate = $('#tDate').val();
		if(!Utils.compareDate(fDate, tDate)){
			msg = msgErr_fromdate_greater_todate;		
			$('#fDate').focus();
		}
		if(msg.length > 0){
			$('#errMsg').html(msg).show();
			return false;
		}
		var dataModel = new Object();		
		dataModel.fromDate = $('#fDate').val().trim();
		dataModel.toDate = $('#tDate').val().trim();
		ReportUtils.exportReport('/report/crm/kd1/export',dataModel);
	},
	exportDSBHNVBH: function(){
		var msg = '';
		$('#errMsg').html('').hide();
		if(msg.length == 0){
			msg = Utils.getMessageOfInvalidFormatDate('tDate','Đến ngày');
		}
		if(msg.length > 0){
			$('#errMsg').html(msg).show();
			return false;
		}
		var shopId = $('#shopId').val();
		var parentSuperStaffCode = $('#parentSuperStaffCode').val().replace(/;/g, ',');
		var staffSaleCode = $('#staffSaleCode').val().replace(/;/g, ',');
		var superStaffCode = $('#superStaffCode').val().replace(/;/g, ',');
		var toDate = $('#tDate').val();
		var now = new Date();
		var cMonth = now.getMonth() + 1;
		var tMonth = toDate.split('/')[1];
		tMonth = Number(tMonth);
		if(tMonth != cMonth) {
			$('#errMsg').html('Đến ngày phải cùng tháng với tháng hiện tại. Vui lòng nhập lại').show();
			return false;
		}
		if(!Utils.compareCurrentDate(toDate)) {
			$('#errMsg').html('Đến ngày phải nhỏ hơn ngày hiện tại. Vui lòng nhập lại').show();
			return false;
		}
		var cat = $('#cat').val();
		var dataModel = new Object();
		dataModel.shopId = shopId;
		dataModel.parentSuperStaffCode = parentSuperStaffCode;
		dataModel.staffSaleCode = staffSaleCode;
		dataModel.superStaffCode = superStaffCode;
		dataModel.toDate = toDate;
		dataModel.cat = cat;
		ReportUtils.exportReport('/report/crm/kd3/export',dataModel);
	},

	/* BAO CAO BAO PHỦ PHƯỜNG XÃ THEO CẤP ĐỘ DOANH SỐ */
	 exportBPPXTCDDS: function(){
		var msg = '';
		$('#errMsg').html('').hide();
		if(msg.length == 0){
			msg = Utils.getMessageOfRequireCheck('fDate','Từ tháng');
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfInvalidFormatDate('fDate','Từ tháng');
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfRequireCheck('tDate','Từ tháng');
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfInvalidFormatDate('tDate','Đến tháng');
		}
		var fDate = $('#fDate').val();
		var tDate = $('#tDate').val();
		if(!Utils.compareDate(fDate, tDate)){
			msg = msgErr_fromdate_greater_todate;		
			$('#fDate').focus();
		}
		if(msg.length > 0){
			$('#errMsg').html(msg).show();
			return false;
		}
		var dataModel = new Object();		
		dataModel.fromDate = $('#fDate').val().trim();
		dataModel.toDate = $('#tDate').val().trim();
		ReportUtils.exportReport('/report/crm/bcbppx-tcdds/export',dataModel);
	},
	
	exportKD10: function(){		
		var msg = "";
		$('#errMsg').html(msg).hide();
		if(msg.length == 0){
			msg = Utils.getMessageOfRequireCheck('fDate','Từ ngày');
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfInvalidFormatDate('fDate','Từ ngày');
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfRequireCheck('tDate','Từ ngày');
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfRequireCheck('subCat','Ngành hàng',true);
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfInvalidFormatDate('tDate','Đến ngày');
		}
		var fDate = $('#fDate').val();
		var tDate = $('#tDate').val();
		var curDate = ReportUtils.getCurrentDateString();
		if(msg.length == 0 && !Utils.compareDate(fDate, curDate)){
			msg = 'Từ ngày phải nhỏ hơn hoặc bằng ngày hiện tại';
			$('#fDate').focus();
		}	
		if(msg.length == 0 && !Utils.compareDate(tDate, curDate)){
			msg = 'Đến ngày phải nhỏ hơn hoặc bằng ngày hiện tại';
			$('#fDate').focus();
		}
		if(msg.length > 0){
			$('#errMsg').html(msg).show();
			return false;
		}
		var productInfoCode = $('#subCat').val().trim();
		var dataModel = new Object();
		dataModel.fromDate = $('#fDate').val();
		dataModel.toDate = $('#tDate').val();
		dataModel.braneId = $('#subCat').val();
		ReportUtils.exportReport('/report/crm/kd10/export',dataModel);
	},
	/**
	 * Bao cao KD9
	 * @author tungtt21
	 * @returns {Boolean}
	 */
	exportKD9: function(){
		var msg = '';
		$('#errMsg').html('').hide();
		if(msg.length == 0){
			msg = Utils.getMessageOfRequireCheck('shopCode','Đơn vị');
		}
		if(msg.length == 0) {
			msg = Utils.getMessageOfRequireCheck('fDate', 'Từ ngày');
		}
		if(msg.length == 0) {
			msg = Utils.getMessageOfRequireCheck('tDate', 'Đến ngày');
		}
		if(msg.length == 0) {
			msg = Utils.getMessageOfInvalidFormatDate('fDate', 'Từ ngày');
		}
		if(msg.length == 0) {
			msg = Utils.getMessageOfInvalidFormatDate('tDate', 'Đến ngày');
		}
		var fDate = $('#fDate').val();
		var tDate = $('#tDate').val();
		var curDate = ReportUtils.getCurrentDateString();
		if(msg.length == 0 && !Utils.compareDate(fDate, curDate)){
			msg = 'Từ ngày phải nhỏ hơn hoặc bằng ngày hiện tại';
			$('#fDate').focus();
		}	
		if(msg.length == 0 && !Utils.compareDate(tDate, curDate)){
			msg = 'Đến ngày phải nhỏ hơn hoặc bằng ngày hiện tại';
			$('#tDate').focus();
		}
		
		if(msg.length == 0 && !Utils.compareDate1(fDate, tDate)){
			msg = 'Từ ngày phải nhỏ hơn đến ngày';
			$('#fDate').focus();
		}
		
		var month1 = fDate.split('/')[1];
		month1 = Number(month1);
		var month2 = tDate.split('/')[1];
		month2 = Number(month2);
		var year1 = fDate.split('/')[2];
		var year2 = tDate.split('/')[2];
		year1 = parseInt(year1);
		year2 = parseInt(year2);
		if(month1 != month2 || year1 != year2) {
			msg = 'Từ ngày phải cùng tháng cùng năm với đến ngày.';
			$('#tDate').focus();
		}
		
		if(msg.length > 0){
			$('#errMsg').html(msg).show();
			return false;
		}
		
		var dataModel = new Object();
		dataModel.fromDate = $('#fDate').val().trim();
		dataModel.toDate = $('#tDate').val().trim();
		dataModel.shopId = $('#shop').combotree('getValue').trim();
		dataModel.productCode = $('#product').val().trim();
		
		var kData = $.param(dataModel,true);
		$('#divOverlay').show();		
		$.ajax({
			type : "POST",
			url : "/report/crm/sku30/export",
			data :(kData),
			dataType: "json",
			success : function(data) {
				$('#divOverlay').hide();				
				if(!data.error) {
					if(data.hasData != null && data.hasData != undefined && data.hasData == true) {
						window.location.href = data.path;
						setTimeout(function(){ //Set timeout để đảm bảo file load lên hoàn tất
		                    CommonSearch.deleteFileExcelExport(data.path);
						},2000);
					} else {
						$('#errMsg').html('Không có dữ liệu để xuất').show();
					}
				} else {
					$('#errMsg').html(data.errMsg).show();					
				}
			},
			error:function(XMLHttpRequest, textStatus, errorDivThrown) {
				$('#divOverlay').hide();
				StockIssued.xhrExport = null;
			}
		});
		return false;
	},
	exportKD21 : function(){
		var msg = '';
		$('#errMsg').html('').hide();
		if($('#shopCode').val().trim().length == 0 || $('#shopCode').val().trim() == activeType.ALL){
			msg = format(msgErr_required_field,'đơn vị');
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfRequireCheck('fDate', 'Từ ngày');
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfInvalidFormatDate('fDate','Từ ngày');
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfRequireCheck('tDate','Đến ngày');
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfInvalidFormatDate('tDate','Đến ngày');
		}
		var fDate = $('#fDate').val();
		var tDate = $('#tDate').val();
		if(!Utils.compareDate(fDate, tDate)){
			msg = msgErr_fromdate_greater_todate;		
			$('#fDate').focus();
		}
		if(msg.length > 0){
			$('#errMsg').html(msg).show();
			return false;
		}
		var dataModel = new Object();
		dataModel.shopId = $('#shopCode').val().trim();
		dataModel.fromDate = $('#fDate').val().trim();
		dataModel.toDate = $('#tDate').val().trim();
		ReportUtils.exportReport('/report/crm/kd21/export',dataModel);
	},
	validateKD2: function(){
		var msg = '';
		$('#errMsg').html('').hide();
		if(msg.length == 0){
			msg = Utils.getMessageOfRequireCheck('tDate','Đến ngày');
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfInvalidFormatDate('tDate','Đến ngày');
		}
		if(msg.length > 0){
			$('#errMsg').html(msg).show();
			return false;
		}
		return true;
	},
	exportKD2 : function(){
		if(!CRMReportDSPPTNH.validateKD2()) return ;
		var dataModel = new Object();
		dataModel.toDate = $('#tDate').val().trim();
		if($('#tbhv').val().trim()!=''){
			dataModel.lstTbhv = $('#tbhv').val().trim().split(";");
		}
		if($('#gsnpp').val().trim()!=''){
			dataModel.lstGsnpp = $('#gsnpp').val().trim().split(";");
		}
		if($('#nvbh').val().trim()!=''){
			dataModel.lstNvbh = $('#nvbh').val().trim().split(";");
		}
		if($('#subcat').val().trim()!=''){
			dataModel.lstProduct = $('#subcat').val().trim().split(";");
		}
		dataModel.shopS = $('#shopId').val().trim();
		ReportUtils.exportReport('/report/crm/kd2/export',dataModel);
	},
	validateKD81: function(){
		var msg = '';
		$('#errMsg').html('').hide();
		if(msg.length == 0){
			msg = Utils.getMessageOfRequireCheck('fDate','Từ ngày');
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfInvalidFormatDate('fDate','Từ ngày');
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfRequireCheck('tDate','Đến ngày');
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfInvalidFormatDate('tDate','Đến ngày');
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfRequireCheck('solan','Số lần');
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfRequireCheck('shopId','Đơn vị');
		}
		var fDate = $('#fDate').val();
		var tDate = $('#tDate').val();
		var curDate = ReportUtils.getCurrentDateString();
		if(msg.length == 0 && !Utils.compareDate(fDate, tDate)){
			msg = msgErr_fromdate_greater_todate;
			$('#fDate').focus();
		}
		if(msg.length == 0 && !Utils.compareDate(tDate, curDate)){
			msg = "Đến ngày không được lớn hơn ngày hiện tại";
			$('#tDate').focus();
		}
		if(msg.length == 0 && fDate.split('/')[1] != tDate.split('/')[1]){
			msg = "Đến ngày phải cùng tháng với Từ ngày.";
			$('#tDate').focus();
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfSpecialCharactersValidateEx('solan','Số lần',Utils._TF_NUMBER);
		}
		if(msg.length > 0){
			$('#errMsg').html(msg).show();
			return false;
		}
		return true;
	},
	exportKD81 : function(){
		if(!CRMReportDSPPTNH.validateKD81()) return;
		var dataModel = new Object();
		dataModel.fromDate = $('#fDate').val().trim();
		dataModel.toDate = $('#tDate').val().trim();
		dataModel.shopS = $('#shopId').val().trim();
		dataModel.solan = $('#solan').val().trim();
		if($('#nvbh').val().trim()!=''){
			dataModel.lstNvbh = $('#nvbh').val().trim().split(";");
		}
		ReportUtils.exportReport('/report/crm/kd81/export',dataModel);
	},
	exportKD_8_1 : function(){
		if(!CRMReportDSPPTNH.validateKD81()) return;
		var dataModel = new Object();
		dataModel.fromDate = $('#fDate').val().trim();
		dataModel.toDate = $('#tDate').val().trim();
		dataModel.shopS = $('#shopId').val().trim();
		dataModel.solan = $('#solan').val().trim();
		var lstNvbh = new Array();
		if($('#nvbh').val().trim()!=''){
			lstNvbh = $('#nvbh').val().trim().split(",");
			dataModel.lstNvbh = lstNvbh;
		}
		ReportUtils.exportReport('/report/crm/kd81/export',dataModel);
	},
	validateKD16: function(){
		var msg = '';
		$('#errMsg').html('').hide();
		if(msg.length == 0){
			msg = Utils.getMessageOfRequireCheck('tDate','Đến ngày');
		}
		if(msg.length == 0){
			if($('#tDate').val().trim().length > 0 && !Utils.isDate('1/'+$('#tDate').val().trim(), '/')){
				$('#tDate').focus();			
				msg = 'Đến ngày không đúng định dạng mm/dd';
			}
		}
		var tDate = $('#tDate').val();
		var curDate = ReportUtils.getCurrentDateString();
		if(msg.length == 0 && !Utils.compareDate(tDate, curDate)){
			msg = "Đến ngày không được lớn hơn ngày hiện tại";
			$('#tDate').focus();
		}
		if(msg.length > 0){
			$('#errMsg').html(msg).show();
			return false;
		}
		return true;
	},
	exportKD16 : function(){
		if(!CRMReportDSPPTNH.validateKD16()) return;
		var dataModel = new Object();
		dataModel.toDate = $('#tDate').val().trim();
		ReportUtils.exportReport('/report/crm/kd16/export',dataModel);
	},
	exportKD8 : function(){
		var msg = '';
		$('#errMsg').html('').hide();
		if($('#shopCode').val().trim().length == 0 || $('#shopCode').val().trim() == activeType.ALL){
			msg = format(msgErr_required_field,'đơn vị');
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfRequireCheck('fDate', 'Từ ngày');
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfInvalidFormatDate('fDate','Từ ngày');
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfRequireCheck('tDate','Đến ngày');
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfInvalidFormatDate('tDate','Đến ngày');
		}
		var fDate = $('#fDate').val();
		var tDate = $('#tDate').val();
		if(!Utils.compareDate(fDate, tDate)){
			msg = msgErr_fromdate_greater_todate;		
			$('#fDate').focus();
		}
		if(msg.length > 0){
			$('#errMsg').html(msg).show();
			return false;
		}
		var dataModel = new Object();		
		dataModel.shopId = $('#shopCode').val().trim();
		dataModel.fromDate = $('#fDate').val().trim();
		dataModel.toDate = $('#tDate').val().trim();
		//dataModel.staffCode = $('#staffCode').val().trim();
		ReportUtils.exportReport('/report/crm/kd8/export',dataModel);
	},
	exportKD1_1: function(){
		var msg = '';
		var multiShop = $("#shop").data("kendoMultiSelect");
		var dataShop = multiShop.dataItems();
		$('#errMsg').html('').hide();
		
		if(msg.length == 0){
			if(dataShop == null || dataShop.length == 0){
				msg = "Bạn chưa chọn đơn vị. Vui lòng chọn đơn vị.";
			}
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfRequireCheck('tDate','Đến ngày');
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfInvalidFormatDate('tDate','Đến ngày');
		}		
		if(msg.length > 0){
			$('#errMsg').html(msg).show();
			return false;
		}
		
		var dataModel = new Object();			
		dataModel.toDate = $('#tDate').val().trim();
		dataModel.productCode = $('#product').val().trim();
		
		if(dataShop!=null && dataShop.length>0){
			var lstShop = dataShop[0].id;
			for(var i=1;i<dataShop.length;i++){
				lstShop += "," + dataShop[i].id;
			}
			dataModel.listShopId = lstShop;
		}
		
		var kData = $.param(dataModel,true);
		$('#divOverlay').show();		
		$.ajax({
			type : "POST",
			url : "/report/crm/kd1_1/export",
			data :(kData),
			dataType: "json",
			success : function(data) {
				$('#divOverlay').hide();				
				if(!data.error) {
					if(data.hasData != null && data.hasData != undefined && data.hasData == true) {
						window.location.href = data.path;
						setTimeout(function(){ //Set timeout để đảm bảo file load lên hoàn tất
		                    CommonSearch.deleteFileExcelExport(data.path);
						},2000);
					} else {
						$('#errMsg').html('Không có dữ liệu để xuất').show();
					}
				} else {
					$('#errMsg').html(data.errMsg).show();					
				}
			},
			error:function(XMLHttpRequest, textStatus, errorDivThrown) {
				$('#divOverlay').hide();
				StockIssued.xhrExport = null;
			}
		});
		return false;
	},
	reportSDLCSPDS : function(){
		var msg = '';
		$('#errMsg').html('').hide();
		var dataModel = new Object();
		dataModel.fromMonth = $('#fromMonth').val().trim();
		dataModel.toMonth = $('#toMonth').val().trim();
		ReportUtils.previewReportProcessNoData('/report/crm/sdlcpsds/export',dataModel);
	},
	exportKD4 : function(){
		$('#errMsg').html('').hide();
		var msg = "";
		if(msg.length == 0){
			msg = Utils.getMessageOfRequireCheck('toDate','Đến ngày');
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfInvalidFormatDate('toDate','Đến ngày');
		}
		var toDate =  $('#toDate').val().trim();
		if(msg.length == 0 && !Utils.compareCurrentDate(toDate)) {
			msg = 'Đến ngày phải nhỏ hơn ngày hiện tại. Vui lòng nhập lại';
			$('#toDate').focus();
		}
//		var now = new Date();
//		var cMonth = now.getMonth() + 1;
//		var cYear = now.getFullYear();
//		var aoDate = $('#toDate').val().split('/');
//		var month = aoDate[1];
//		var year = aoDate[2];
//		if(month != cMonth || year != cYear ){
//			msg = 'Đến ngày không thuộc tháng hiện tại';
//			$('#toDate').focus();
//		}
		if(msg.length > 0){
			$('#errMsg').html(msg).show();
			return false;
		}
		var dataModel = new Object();
		dataModel.toDate = toDate;
		ReportUtils.exportReport('/report/crm/kd4/export',dataModel);
	},
	exportKD14 : function(){
		$('#errMsg').html('').hide();
		var msg = '';
		if(msg.length == 0){
			msg = Utils.getMessageOfRequireCheck('fromDate','Từ tháng');
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfRequireCheck('toDate','Đến tháng');
		}
		if(msg.length > 0){
			$('#errMsg').html(msg).show();
			return false;
		}
		var shopCode = $('#shopCode').val();
		var fromMonth = $('#fromDate').val();
		var toMonth = $('#toDate').val();
		var year1 = '';
		year1 = fromMonth.split('/')[1];
		year1 = parseInt(year1);
		var year2 = '';
		year2 = toMonth.split('/')[1];
		year2 = parseInt(year2);
		if(year1 != '' && year2 != '' && year1 != year2) {
			$('#errMsg').html('Từ tháng và đến tháng phải cùng một năm').show();
			return;
		}
		var fromDate = '01/' + fromMonth;
		var toDate = '01/' + toMonth;
		
		var now = new Date();
		var nowMonth = now.getMonth() + 1;
		var nowYear = now.getFullYear();
		
		if(year2 >= nowYear && Number(toMonth.split('/')[0]) > nowMonth) {
			$('#errMsg').html('Đến tháng phải nhỏ hơn tháng hiện tại. Vui lòng nhập lại').show();
			$('#toDate').focus();
			return;
		}
		
		if(!Utils.isDate(fromDate, '/')){
			$('#errMsg').html('Từ tháng không hợp lệ. Vui lòng nhập lại').show();
			$('#fromDate').focus();
			return;
		}
		
		if(!Utils.isDate(toDate, '/')) {
			$('#errMsg').html('Đến tháng không hợp lệ. Vui lòng nhập lại').show();
			$('#toDate').focus();
			return;
		}
		
		if(!Utils.compareDate(fromDate, toDate)) {
			$('#errMsg').html('Từ tháng phải nhỏ hơn đến tháng').show();
			return;
		}
		var dataModel = new Object();
		dataModel.shopCode = shopCode;
		dataModel.fromDate = fromDate;
		dataModel.toDate = toDate;
		ReportUtils.exportReport('/report/crm/kd14/export',dataModel, null, function(){
			$('#.fancybox-inner #formatType option[value=PDF]').remove();
		});
	},
	exportKD007 : function(){
		var msg = "";
		if(msg.length == 0){
			msg = Utils.getMessageOfRequireCheck('fDate','Từ ngày');
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfRequireCheck('tDate','Đến ngày');
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfInvalidFormatDate('fDate','Từ ngày');
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfInvalidFormatDate('tDate','Đến ngày');
		}
		var fDate = $('#fDate').val();
		var tDate = $('#tDate').val();
		if(!Utils.compareDate(fDate, tDate)){
			msg = msgErr_fromdate_greater_todate;		
			$('#tDate').focus();
		}
		if(msg.length > 0){
			$('#errMsg').html(msg).show();
			return false;
		}
		var dataModel = new Object();
		dataModel.shopId = $('#shopId').val().trim();
		dataModel.fromDate = $('#fDate').val().trim();
		dataModel.toDate = $('#tDate').val().trim();
		ReportUtils.exportReport('/report/crm/kd7/export',dataModel);
	},
	exportKD10_1 : function(){
		$('#errMsg').html('').hide();
		var msg = "";
		msg = Utils.getMessageOfRequireCheck('subCat','Nhãn hàng',true);
		if(msg.length == 0){
			msg = Utils.getMessageOfRequireCheck('toDate','Đến ngày');
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfInvalidFormatDate('toDate','Đến ngày');
		}
		var now = new Date();
		var cMonth = now.getMonth() + 1;
		var cYear = now.getFullYear();
		var aoDate = $('#toDate').val().split('/');
		var month = aoDate[1];
		var year = aoDate[2];
		if(month != cMonth || year != cYear ){
			msg = 'Đến ngày không thuộc tháng hiện tại';
			$('#toDate').focus();
		}
		if(msg.length > 0){
			$('#errMsg').html(msg).show();
			return false;
		}
		var dataModel = new Object();
		dataModel.toDate = $('#toDate').val();
		dataModel.subCatId = $('#subCat').val();
		ReportUtils.exportReport('/report/crm/kd10_1/export',dataModel, true);
	},
	exportKD12 : function(){
		var msg = "";
		$('#errMsg').html(msg).hide();
		if($('#tDate').val()==""){
			msg = "Bạn vui lòng chọn đến ngày.";
			$('#tDate').focus();
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfInvalidFormatDate('tDate','Đến ngày');
		}
		if(!Utils.compareCurrentDate($('#tDate').val())){
			msg="Đến ngày lớn hơn ngày hiện tại, mời bạn chọn lại.";
			$('#tDate').focus();
		}
		if(msg.length > 0){
			$('#errMsg').html(msg).show();
			return false;
		}
		var dataModel = new Object();
		dataModel.toDate = $('#tDate').val();
		dataModel.shopId = $('#shop').combotree('getValue');
		dataModel.listNVBHCode = $('#staffSaleCode').val().trim();
		dataModel.listNVBHId = SuperviseCustomer.mapNVBH.keyArray;
		dataModel.checkKD12 = 0;
		var kData = $.param(dataModel, true);
		$.ajax({
			type : "POST",
			url : '/report/crm/kd12/export',
			dataType : "json",
			data : (kData),
			success : function(data) {
				if(data.error == false) {
					dataModel.checkKD12 = 1;
					dataModel.listNVBHId = data.listnvbhid;
					ReportUtils.exportReport('/report/crm/kd12/export',dataModel);
				}else{
					$('#errMsg').html(data.errorMsg).show();
				}
			},
			error: function(XMLHttpRequest, textStatus, errorThrown) {
			}
		});	
	},
	exportKD15 : function(){
		var msg = "";
		$('#errMsg').html(msg).hide();
		msg = Utils.getMessageOfRequireCheck('tDate','Tháng');		
		if(msg.length == 0){
			$('#tmptDate').val('01/' + $('#tDate').val().trim());
			msg = Utils.getMessageOfInvalidFormatDate('tmptDate','Tháng');
			$('#tDate').focus();
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfRequireCheck('inputValue','Số tháng');
		}
		if(msg.length == 0){
			var inputValue = $('#inputValue').val().trim();
			if(isNaN(inputValue) || inputValue<1 || inputValue>12){
				$('#inputValue').focus();
				msg = 'Số tháng là kiểu số nguyên và chỉ nằm trong đoạn từ 1 - 12 (tháng).';
			}		
		}
		if(msg.length > 0){
			$('#errMsg').html(msg).show();
			return false;
		}
		var dataModel = new Object();
		dataModel.toDate = '01/' + $('#tDate').val();
		dataModel.inputValue = $('#inputValue').val();			
		ReportUtils.exportReport('/report/crm/kd15/export',dataModel,true);
	},
	exportKD19 : function(){
		var msg = "";
		$('#errMsg').html(msg).hide();
		if($('#badscore').val()==""){
			msg="Số lần là trường bắt buộc.Yêu cầu nhập.";
			$('#badscore').focus();
		}
		if (msg.length==0){
			msg = Utils.getMessageOfSpecialCharactersValidateEx('badscore', 'Số lần ', Utils._TF_NUMBER,'');
			$('#badscore').focus();
		}
		if (parseInt($('#badscore').val(),10)==0 && msg.length == 0){
			msg="Số lần là một số nguyên dương.";
			$('#badscore').focus();
		}
		if($('#tDate').val()=="" && msg.length == 0){
			msg="Đến ngày là trường bắt buộc.Yêu cầu nhập.";
			$('#tDate').focus();
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfInvalidFormatDate('tDate','Đến ngày');
		}
		if(!Utils.compareCurrentDate($('#tDate').val()) && msg.length == 0){
			msg="Đến ngày lớn hơn ngày hiện tại, mời bạn chọn lại.";
			$('#tDate').focus();
		}
		if(msg.length > 0){
			$('#errMsg').html(msg).show();
			return false;
		}
		var dataModel = new Object();
		dataModel.toDate = $('#tDate').val();
		dataModel.shopId = $('#shop').combotree('getValue');
		dataModel.numTimeBadScore = $('#badscore').val();
		ReportUtils.exportReport('/report/crm/kd19/export',dataModel);
	},
	/*@author: hunglm16; @since: February 10, 2014; @description: KD10.2-Điểm lẻ cần phân phối theo SKUs đề nghị*/
	exportKD10_2 : function(){
		$('.ErrorMsgStyle').html('').show();
		var msg = "";
		msg = Utils.getMessageOfRequireCheck('subCat','Nhãn hàng',true);
		if(msg.length == 0){
			msg = Utils.getMessageOfRequireCheck('tDate','Đến ngày');
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfInvalidFormatDate('tDate','Đến ngày');
		}
		if(msg.length > 0){
			$('#errMsg').html(msg).show();
			return false;
		}
		var dataModel = new Object();
		dataModel.toDate = $('#tDate').val();
		dataModel.subCatId = $('#subCat').val();
		ReportUtils.exportExcel('/report/crm/kd10_2/export',dataModel, 'errMsg');
	},
	exportBCKD18: function(){
			var msg = '';
			$('#errMsg').html('').hide();
			if(msg.length ==0){
				msg = Utils.getMessageOfRequireCheck('fromMonth','Từ tháng');
			}
			if(msg.length ==0){
				msg = Utils.getMessageOfInvalidFormatDateEx('fromMonth','Từ tháng', Utils._DATE_MM_YYYY);
			}
			if(msg.length ==0){
				msg = Utils.getMessageOfRequireCheck('toMonth','Đến tháng');
			}
			if(msg.length ==0){
				msg = Utils.getMessageOfInvalidFormatDateEx('toMonth','Đến tháng', Utils._DATE_MM_YYYY);
			}
			var fMonth = $('#fromMonth').val();
			var tMonth = $('#toMonth').val();
			if(!Utils.compareMonth(fMonth, tMonth)){
				msg = msgErr_frommonth_greater_tomonth;		
				$('#fromMonth').focus();
			}
			if(msg.length > 0){
				$('#errMsg').html(msg).show();
				return false;
			}
			var dataModel = new Object();		
			dataModel.fromDate = $('#fromMonth').val().trim();
			dataModel.toDate = $('#toMonth').val().trim();
			ReportUtils.exportReport('/report/crm/kd18/export',dataModel);
		},
	 /*@author: phuongvm; @description: KD16.1-Thống kê số xã chưa PSDS */
	exportKD_16_1 : function(){
		var msg = '';
		$('#errMsg').html('').hide();
		if(msg.length ==0){
			msg = Utils.getMessageOfRequireCheck('fromMonth','Từ tháng');
		}
		if(msg.length ==0){
			msg = Utils.getMessageOfInvalidFormatDateEx('fromMonth','Từ tháng', Utils._DATE_MM_YYYY);
		}
		if(msg.length ==0){
			msg = Utils.getMessageOfRequireCheck('toMonth','Đến tháng');
		}
		if(msg.length ==0){
			msg = Utils.getMessageOfInvalidFormatDateEx('toMonth','Đến tháng', Utils._DATE_MM_YYYY);
		}
		var fMonth = $('#fromMonth').val();
		var tMonth = $('#toMonth').val();
		if(!Utils.compareMonth(fMonth, tMonth)){
			msg = msgErr_frommonth_greater_tomonth;		
			$('#fromMonth').focus();
		}
		if(msg.length > 0){
			$('#errMsg').html(msg).show();
			return false;
		}
		var dataModel = new Object();		
		dataModel.fromMonth = $('#fromMonth').val().trim();
		dataModel.toMonth = $('#toMonth').val().trim();
		ReportUtils.exportReport('/report/crm/kd16_1/export',dataModel);
	},
	exportKD11_1 : function(){
		var msg = '';
		$('#errMsg').html('').hide();
		if(msg.length == 0){
			msg = Utils.getMessageOfRequireCheck('programCode', 'CTTB');
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfRequireCheck('fDate', 'Từ ngày');
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfInvalidFormatDate('fDate','Từ ngày');
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfRequireCheck('tDate','Đến ngày');
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfInvalidFormatDate('tDate','Đến ngày');
		}
		var fDate = $('#fDate').val();
		var tDate = $('#tDate').val();
		if(!Utils.compareDate(fDate, tDate)){
			msg = msgErr_fromdate_greater_todate;		
			$('#fDate').focus();
		}
		if(msg.length > 0){
			$('#errMsg').html(msg).show();
			return false;
		}
		var dataModel = new Object();		
		dataModel.programCode = $('#programCode').val().trim();
		dataModel.fromDate = $('#fDate').val().trim();
		dataModel.toDate = $('#tDate').val().trim();
		dataModel.type = $('#type').val().trim();
		$('#typeReport').val($('#type').val().trim());
		ReportUtils.exportReport('/report/crm/kd11_1/export',dataModel);
	},
	exportBCVT4 : function(){
		var msg = '';
		$('#errMsg').html('').hide();
		if(msg.length == 0){
			msg = Utils.getMessageOfRequireCheck('fromDate', 'Từ ngày');
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfInvalidFormatDate('fromDate','Từ ngày');
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfRequireCheck('toDate','Đến ngày');
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfInvalidFormatDate('toDate','Đến ngày');
		}
		var fDate = $('#fromDate').val();
		var tDate = $('#toDate').val();
		if(!Utils.compareDate(fDate, tDate)){
			msg = msgErr_fromdate_greater_todate;		
			$('#fromDate').focus();
		}
		if(msg.length > 0){
			$('#errMsg').html(msg).show();
			return false;
		}
		var dataModel = new Object();
		dataModel.fromDate = $('#fromDate').val().trim();
		dataModel.toDate = $('#toDate').val().trim();
		ReportUtils.exportReport('/report/crm/vt4/export',dataModel);
	},
	exportBCDS1 : function(){
		/*var msg = '';
		$('#errMsg').html('').hide();
		if(msg.length == 0){
			msg = Utils.getMessageOfRequireCheck('fromDate', 'Từ ngày');
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfInvalidFormatDate('fromDate','Từ ngày');
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfRequireCheck('toDate','Đến ngày');
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfInvalidFormatDate('toDate','Đến ngày');
		}
		var fDate = $('#fromDate').val();
		var tDate = $('#toDate').val();
		if(!Utils.compareDate(fDate, tDate)){
			msg = msgErr_fromdate_greater_todate;		
			$('#fromDate').focus();
		}
		if(msg.length > 0){
			$('#errMsg').html(msg).show();
			return false;
		}
		var dataModel = new Object();
		dataModel.fromDate = $('#fromDate').val().trim();
		dataModel.toDate = $('#toDate').val().trim();
		ReportUtils.exportReport('/report/crm/ds1/export',dataModel);*/
		
		var dataModel = new Object();
		dataModel.typeCurrentShop = 10000;
		dataModel.formatType = 'XLS';
		var kData = $.param(dataModel, true);
		$('#divOverlay').show();
		$.ajax({
			type : 'GET',
			url : '/report/crm/ds1/export',
			data :(kData),
			dataType : "json",
			success : function(data) {
				$('#divOverlay').hide();
				if(!data.error && data.path!= null && data.path.length > 0){
					window.location.href = data.path;
					setTimeout(function(){ //Set timeout để đảm bảo file load lên hoàn tất
	                    CommonSearch.deleteFileExcelExport(data.path);
					},2000);
				}
			},
		error:function(XMLHttpRequest, textStatus, errorThrown) {}});
	},
	exportBCDS2 : function(){
		var msg = '';
		$('#errMsg').html('').hide();
		if(msg.length == 0){
			msg = Utils.getMessageOfRequireCheck('fromDate', 'Từ ngày');
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfInvalidFormatDate('fromDate','Từ ngày');
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfRequireCheck('toDate','Đến ngày');
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfInvalidFormatDate('toDate','Đến ngày');
		}
		var fDate = $('#fromDate').val();
		var tDate = $('#toDate').val();
		if(!Utils.compareDate(fDate, tDate)){
			msg = msgErr_fromdate_greater_todate;		
			$('#fDate').focus();
		}
		if(msg.length > 0){
			$('#errMsg').html(msg).show();
			return false;
		}
		var dataModel = new Object();
		dataModel.fromDate = $('#fromDate').val().trim();
		dataModel.toDate = $('#toDate').val().trim();
		ReportUtils.exportReport('/report/crm/ds2/export',dataModel);
	},
	
	exportBCVT2 : function(){
		var msg = '';
		$('#errMsg').html('').hide();
		if(msg.length == 0){
			msg = Utils.getMessageOfRequireCheck('fromDate', 'Từ ngày');
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfInvalidFormatDate('fromDate','Từ ngày');
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfRequireCheck('toDate','Đến ngày');
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfInvalidFormatDate('toDate','Đến ngày');
		}
		var fDate = $('#fromDate').val();
		var tDate = $('#toDate').val();
		if(!Utils.compareDate(fDate, tDate)){
			msg = msgErr_fromdate_greater_todate;		
			$('#fromDate').focus();
		}
		if(msg.length > 0){
			$('#errMsg').html(msg).show();
			return false;
		}
		var dataModel = new Object();
		dataModel.fromDate = $('#fromDate').val().trim();
		dataModel.toDate = $('#toDate').val().trim();
		ReportUtils.exportReport('/report/crm/vt2/export',dataModel);
	},
	
	/**
	 * Bao cao KD1- Doanh so ban hang cua nhan vien ban hang theo SKUs
	 * @author hunglm16
	 * @since January 23,2014
	 * */
	exportBCTHCTHTB_KD1 : function(){
		var msg = '';
		$('#errMsg').html('').hide();
		
		if(msg.length == 0 && $('.combo-value').val()==undefined && $('.combo-value').val().length ==0){
			msg = "Chưa chọn Đơn Vị";
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfRequireCheck('tDate','Đến ngày');
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfInvalidFormatDate('tDate','Đến ngày');
		}
		if(msg.length > 0){
			$('#errMsg').html(msg).show();
			return false;
		}
		var dataModel = new Object();
		dataModel.shopId = $('.combo-value').val();
		dataModel.toDate = $('#tDate').val();
		dataModel.lstProductCode = $('#product').val();
		ReportUtils.exportExcel('/report/crm/kd1/export', dataModel, 'errMsg');
	},
	
	// vuonghn : KD5
	exportKD5: function(){
		var msg = '';
		$('#errMsg').html('').hide();
		if(msg.length == 0){
			msg = Utils.getMessageOfRequireCheck('fDate','Từ ngày');
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfInvalidFormatDate('fDate','Từ ngày');
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfRequireCheck('tDate','Đến ngày');
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfInvalidFormatDate('tDate','Đến ngày');
		}
		var fDate = $('#fDate').val();
		var tDate = $('#tDate').val();
		if(!Utils.compareDate(fDate, tDate)){
			msg = msgErr_fromdate_greater_todate;		
			$('#fDate').focus();
		}
		var currentTime = new Date();
		var month = currentTime.getMonth() + 1;
		var day = currentTime.getDate();
		var year = currentTime.getFullYear();
		if(!Utils.compareDate(fDate,day + '/' + month + '/' + year)){
			msg = 'Từ ngày không được lớn hơn ngày hiện tại';
			$('#fDate').focus();
		}
		if(msg.length == 0 && !Utils.compareCurrentDate(tDate)) {
			msg = 'Đến ngày phải nhỏ hơn ngày hiện tại. Vui lòng nhập lại';
			$('#tDate').focus();
		}
		var lstCategoryId = '';
		var lstCategory = $('#category').val();
		if(lstCategory != null && lstCategory.length>0){
			for(var i=0;i<lstCategory.length;i++){
				if(lstCategory[i]!=null&&lstCategory[i]!=''){
					if(lstCategoryId.length>0){
						lstCategoryId+=',';
					}
					lstCategoryId+=lstCategory[i];
				}
			}
		}else{
			msg = "Vui lòng chọn ngành hàng.";
		}
		if(msg.length > 0){
			$('#errMsg').html(msg).show();
			return false;
		}
		var dataModel = new Object();
		dataModel.shopId = $('#shopId').val();
		dataModel.lstCategoryId = lstCategoryId;
		dataModel.fromDate = $('#fDate').val().trim();
		dataModel.toDate = $('#tDate').val().trim();
		ReportUtils.exportReport('/report/crm/kd5/export',dataModel);
	},	
	
	/**
	 * Bao cao KD3- Doanh so ban hang cua nhan vien ban hang theo SKUs
	 * @author hunglm16
	 * @since January 23,2014
	 * */
	exportBCTHCTHTB_KD3 : function(){
		var msg = '';
		$('#errMsg').html('').hide();
		
		if(msg.length == 0 && $('.combo-value').val()==undefined && $('.combo-value').val().length ==0){
			msg = "Chưa chọn Đơn Vị";
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfRequireCheck('tDate','Đến ngày');
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfInvalidFormatDate('tDate','Đến ngày');
		}
		if(msg.length > 0){
			$('#errMsg').html(msg).show();
			return false;
		}
		var dataModel = new Object();
		dataModel.shopId = $('.combo-value').val();
		dataModel.toDate = $('#tDate').val();
		dataModel.lstProductCode = $('#product').val();
		ReportUtils.exportExcel('/report/crm/kd3/export', dataModel, 'errMsg');
	},
	exportKD10: function(){
		var msg = "";
		if(msg.length == 0){
			msg = Utils.getMessageOfRequireCheck('fDate','Từ ngày');
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfInvalidFormatDate('fDate','Từ ngày');
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfRequireCheck('tDate','Từ ngày');
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfInvalidFormatDate('tDate','Đến ngày');
		}
		var fDate = $('#fDate').val();
		var tDate = $('#tDate').val();
		var dataModel = new Object();
		dataModel.fromDate = $('#fDate').val();
		dataModel.toDate = $('#tDate').val();
		ReportUtils.exportReport('/report/crm/kd10/export',dataModel);
	}
};
function removeE(list,key){
	if (list!=null && list.length > 0){
		for (var i=0; i< list.length;i++){
			if (list[i]== key){
				list.splice(i,1);
			}
		}
	}
}
/*
 * END OF FILE - /web/web/resources/scripts/business/shop-report/vnm.crm-report.js
 */

/*
 * START OF FILE - /web/web/resources/scripts/business/shop-report/vnm.crm-report.utils.js
 */
//TUNGTT
var CRMReportUtils = {
		_listStaffCode:null,
		_listStaffOwnerCode:null,
		_listStaffTBHV:null,
		_listProduct:null,

		showDialogSearchProduct : function(){
			var html = '<div id="common-dialog-search-2-textbox" class="easyui-dialog">';
			html += '<div class="PopupContentMid">';
			html += '<div class="GeneralForm Search1Form" >';
			html += '<label class="LabelStyle Label1Style" style=" width: 100px;">Mã SP</label>';
			html += '<input id="productCode" maxlength="50" type="text" style="width: 145px;" class="InputTextStyle InputText1Style"/>';
			html += '<label class="LabelStyle Label1Style" style=" width: 100px;">Tên SP</label>';
			html += '<input id="productName" maxlength="250" type="text" style="width: 145px;" class="InputTextStyle InputText1Style"/>';
			html += '<div class="Clear"></div>'; 
			html += '<div class="BtnCenterSection">';
			html += '<button class="BtnGeneralStyle BtnGeneralMStyle" tabindex="3" id="common-dialog-button-search">Tìm kiếm</button>';
			html += '</div>';
			html += '<div class="Clear"></div>';
			html += '<div class="GridSection" id="common-dialog-grid-container">';
			html += '<table id="common-dialog-grid-search"></table>';
			html += '</div>';
			html += '<div class="BtnCenterSection">';
			html += '<button id ="common-dialog-button-choose" class="BtnGeneralStyle BtnGeneralMStyle">Chọn</button>';
			html += '<button class="BtnGeneralStyle BtnGeneralMStyle" onclick="$(\'#common-dialog-search-2-textbox\').dialog(\'close\');">Đóng</button>';
			html += '</div>';
			html += '<p id="dialog-search-error" style="display: none;" class="ErrorMsgStyle"></p>';
			html += '</div>';
			html += '</div>';
			html += '</div>';
			//style="display: none;"
			$('body').append(html);
			
			$('#common-dialog-search-2-textbox').dialog({
				 title: 'CHỌN SẢN PHẨM',
				 width: 580, 
				 height: 'auto',
				 closed: false,
				 cache: false,
				 modal: true,
				 onOpen: function() {
					 $('#common-dialog-search-2-textbox #common-dialog-button-search').unbind('click');
					 $('#common-dialog-search-2-textbox #common-dialog-button-search').bind('click', function() {
						 $('#common-dialog-search-2-textbox #dialog-search-error').html('').hide();
						 $('#common-dialog-grid-search').datagrid('reload');
					 });
					 $('#common-dialog-search-2-textbox input').unbind('keyup');
					 $('#common-dialog-search-2-textbox input').bind('keyup', function(e) {	
						 if(e.keyCode == keyCodes.ENTER) {
							 $('#common-dialog-search-2-textbox #common-dialog-button-search').click();
						 }
					 });
					 $('#common-dialog-search-2-textbox #common-dialog-button-choose').bind('click', function() {
						 if(CRMReportUtils._listProduct.size() == 0) {
							 $('#common-dialog-search-2-textbox #dialog-search-error').html('Bạn chưa chọn sản phẩm. Vui lòng chọn sản phẩm').show();
						 }else{
							 $('#common-dialog-search-2-textbox #dialog-search-error').html('').hide();
							 var listStaffStr = CRMReportUtils._listProduct.valArray[0].productCode;
							 for(var i = 1; i < CRMReportUtils._listProduct.valArray.length; i++) {
								 listStaffStr += ',' + Utils.XSSEncode(CRMReportUtils._listProduct.valArray[i].productCode);
							 }
							 $('#product').val(Utils.XSSEncode(listStaffStr));
							 $('#common-dialog-search-2-textbox').dialog('close');
						 }
					 });
					 CRMReportUtils._listProduct = new Map();
					 $('#common-dialog-grid-search').datagrid({
						 url : '/commons/product/search',
						 autoRowHeight : true,
						 rownumbers : true, 
						 pagination:true,
						 rowNum : 10,
						 pageSize:10,
						 scrollbarSize : 0,
						 pageNumber:1,
						 queryParams:{
							 page:1
						 },
						 onBeforeLoad: function(param) {
							 var code = $('#common-dialog-search-2-textbox #productCode').val().trim();
							 var name = $('#common-dialog-search-2-textbox #productName').val().trim();

							 param.code = code;
							 param.name = name;
							 
							 param = $.param(param, true);
						 },
						 onCheck : function (index,row) {
							 CRMReportUtils._listProduct.put(row.id, row);
						 },
						 onUncheck : function (index,row) { 
							 CRMReportUtils._listProduct.remove(row.id);
						 },
						 onCheckAll : function(rows) {
							 for(var i = 0; i < rows.length; i++) {
								 var row = rows[i];
								 CRMReportUtils._listProduct.put(row.id, row);
							 }
						 },
						 onUncheckAll : function(rows) {
							 for(var i = 0; i < rows.length; i++) {
								 var row = rows[i];
								 CRMReportUtils._listProduct.remove(row.id);
							 }
						 },
						 fitColumns:true,
						 width : ($('#common-dialog-search-2-textbox').width() - 40),
						 columns:[[
						 	{field: 'productCode', title: 'Mã SP', align:'left', width: 40, sortable : false, resizable : false},
						 	{field: 'productName', title: 'Tên SP', align:'left', width: 40, sortable : false, resizable : false},
						 	{field : 'choose', checkbox : true, align: 'center', width: 20, sortable : false, resizable : false}
						 ]],
						 onLoadSuccess :function(data){							 
							 if(($.isArray(data) && data.length != 0) || (data.rows != null && data.rows != undefined && $.isArray(data.rows) && data.rows.length != 0)) {
								 var __keys = (data.rows == null || data.rows == undefined) ? Object.keys(data[0]) : Object.keys(data.rows[0]);
								 var rows = (data.rows == null || data.rows == undefined) ? data : data.rows;
								 var isCheckAll=true;
								 for(var i = 0; i < rows.length; i++) {
									 if(CRMReportUtils._listProduct.get(rows[i].id)) {
										 $('#common-dialog-grid-search').datagrid('checkRow', i);
									 } else {
										 isCheckAll = false;
									 }
								 }
								 if(isCheckAll) {
									 $('#common-dialog-search-2-textbox .datagrid-header-row input[type=checkbox]').attr('checked', 'checked');
								 }else{
									 $('#common-dialog-search-2-textbox .datagrid-header-row input[type=checkbox]').attr('checked', false);
								 }
							 }
							 $('#common-dialog-search-2-textbox .datagrid-header-rownumber').html('STT');
							 var dHeight = $('#common-dialog-search-2-textbox').height();
							 var wHeight = $(window).height();
							 var top = (wHeight - dHeight) / 2 > 180 ? (wHeight - dHeight) / 2 - 180 : (wHeight - dHeight) / 2;
							 $('#common-dialog-search-2-textbox').dialog('move', {top : top});
						 }
					 });
				 },
				 onClose: function() {
					 CRMReportUtils._listProduct = null;
					 $('#common-dialog-search-2-textbox').remove(); 
				 }
			});
		},
		showDialogSearchNVBH : function(shopId,isGetShopOnly){
			var html = '<div id="common-dialog-search-2-textbox" class="easyui-dialog">';
			html += '<div class="PopupContentMid">';
			html += '<div class="GeneralForm Search1Form" >';
			html += '<label class="LabelStyle Label1Style" style=" width: 100px;">Mã NVBH</label>';
			html += '<input id="staffCode" maxlength="50" type="text" style="width: 145px;" class="InputTextStyle InputText1Style"/>';
			html += '<label class="LabelStyle Label1Style" style=" width: 100px;">Tên NVBH</label>';
			html += '<input id="staffName" maxlength="250" type="text" style="width: 145px;" class="InputTextStyle InputText1Style"/>';
			html += '<div class="Clear"></div>'; 
			html += '<div class="BtnCenterSection">';
			html += '<button class="BtnGeneralStyle BtnGeneralMStyle" tabindex="3" id="common-dialog-button-search">Tìm kiếm</button>';
			html += '</div>';
			html += '<div class="Clear"></div>';
			html += '<div class="GridSection" id="common-dialog-grid-container">';
			html += '<table id="common-dialog-grid-search"></table>';
			html += '</div>';
			html += '<div class="BtnCenterSection">';
			html += '<button id ="common-dialog-button-choose" class="BtnGeneralStyle BtnGeneralMStyle">Chọn</button>';
			html += '<button class="BtnGeneralStyle BtnGeneralMStyle" onclick="$(\'#common-dialog-search-2-textbox\').dialog(\'close\');">Đóng</button>';
			html += '</div>';
			html += '<p id="dialog-search-error" style="display: none;" class="ErrorMsgStyle"></p>';
			html += '</div>';
			html += '</div>';
			html += '</div>';
			//style="display: none;"
			$('body').append(html);
			
			$('#common-dialog-search-2-textbox').dialog({
				 title: 'CHỌN NHÂN VIÊN BÁN HÀNG',
				 width: 580, 
				 height: 'auto',
				 closed: false,
				 cache: false,
				 modal: true,
				 onOpen: function() {
					 $('#common-dialog-search-2-textbox #common-dialog-button-search').unbind('click');
					 $('#common-dialog-search-2-textbox #common-dialog-button-search').bind('click', function() {
						 $('#common-dialog-search-2-textbox #dialog-search-error').html('').hide();
						 $('#common-dialog-grid-search').datagrid('reload');
					 });
					 $('#common-dialog-search-2-textbox input').unbind('keyup');
					 $('#common-dialog-search-2-textbox input').bind('keyup', function(e) {	
						 if(e.keyCode == keyCodes.ENTER) {
							 $('#common-dialog-search-2-textbox #common-dialog-button-search').click();
						 }
					 });
					 $('#common-dialog-search-2-textbox #common-dialog-button-choose').bind('click', function() {
						 if(CRMReportUtils._listStaffCode.size() == 0) {
							 $('#common-dialog-search-2-textbox #dialog-search-error').html('Bạn chưa chọn NVBH. Vui lòng chọn NVBH').show();
						 }else{
							 $('#common-dialog-search-2-textbox #dialog-search-error').html('').hide();
							 var listStaffStr = CRMReportUtils._listStaffCode.valArray[0].staffCode;
							 for(var i = 1; i < CRMReportUtils._listStaffCode.valArray.length; i++) {
								 listStaffStr += ',' + CRMReportUtils._listStaffCode.valArray[i].staffCode;
							 }
							 $('.ModuleList3Form #staffCode').val(listStaffStr);
							 $('#common-dialog-search-2-textbox').dialog('close');
						 }
					 });
					 CRMReportUtils._listStaffCode = new Map();
					 $('#common-dialog-grid-search').datagrid({
						 url : '/commons/nvbh-2/search',
						 autoRowHeight : true,
						 rownumbers : true, 
						 pagination:true,
						 rowNum : 10,
						 pageSize:10,
						 scrollbarSize : 0,
						 pageNumber:1,
						 queryParams:{
							 page:1
						 },
						 onBeforeLoad: function(param) {
							 var code = $('#common-dialog-search-2-textbox #staffCode').val().trim();
							 var name = $('#common-dialog-search-2-textbox #staffName').val().trim();
							 if(isGetShopOnly == undefined || isGetShopOnly == null){
								 param.isGetShopOnly = true;//SangTN: Khong lay shop con
							 }else{
								 param.isGetShopOnly = isGetShopOnly;
							 }
							 param.code = code;
							 param.name = name;
							 param.shopId = shopId;
							 
							 param.lstGSNPPCode = $('#staffOwnerCode').val();
							 param.lstTBHVCode = $('#staffTBHV').val();
							 
							 param = $.param(param, true);
						 },
						 onCheck : function (index,row) {
							 CRMReportUtils._listStaffCode.put(row.id, row);
						 },
						 onUncheck : function (index,row) { 
							 CRMReportUtils._listStaffCode.remove(row.id);
						 },
						 onCheckAll : function(rows) {
							 for(var i = 0; i < rows.length; i++) {
								 var row = rows[i];
								 CRMReportUtils._listStaffCode.put(row.id, row);
							 }
						 },
						 onUncheckAll : function(rows) {
							 for(var i = 0; i < rows.length; i++) {
								 var row = rows[i];
								 CRMReportUtils._listStaffCode.remove(row.id);
							 }
						 },
						 fitColumns:true,
						 width : ($('#common-dialog-search-2-textbox').width() - 40),
						 columns:[[
						 	{field: 'staffCode', title: 'Mã NVBH', align:'left', width: 40, sortable : false, resizable : false},
						 	{field: 'staffName', title: 'Tên NVBH', align:'left', width: 40, sortable : false, resizable : false},
						 	{field : 'choose', checkbox : true, align: 'center', width: 20, sortable : false, resizable : false}
						 ]],
						 onLoadSuccess :function(data){							 
							 if(($.isArray(data) && data.length != 0) || (data.rows != null && data.rows != undefined && $.isArray(data.rows) && data.rows.length != 0)) {
								 var __keys = (data.rows == null || data.rows == undefined) ? Object.keys(data[0]) : Object.keys(data.rows[0]);
								 var rows = (data.rows == null || data.rows == undefined) ? data : data.rows;
								 var isCheckAll=true;
								 for(var i = 0; i < rows.length; i++) {
									 if(CRMReportUtils._listStaffCode.get(rows[i].id)) {
										 $('#common-dialog-grid-search').datagrid('checkRow', i);
									 } else {
										 isCheckAll = false;
									 }
								 }
								 if(isCheckAll) {
									 $('#common-dialog-search-2-textbox .datagrid-header-row input[type=checkbox]').attr('checked', 'checked');
								 }
							 }
							 $('#common-dialog-search-2-textbox .datagrid-header-rownumber').html('STT');
							 var dHeight = $('#common-dialog-search-2-textbox').height();
							 var wHeight = $(window).height();
							 var top = (wHeight - dHeight) / 2 > 180 ? (wHeight - dHeight) / 2 - 180 : (wHeight - dHeight) / 2;
							 $('#common-dialog-search-2-textbox').dialog('move', {top : top});
						 }
					 });
				 },
				 onClose: function() {
					 CRMReportUtils._listStaffCode = null;
					 $('#common-dialog-search-2-textbox').remove(); 
				 }
			});
		},
		showDialogSearchGSNPP : function(shopId){
			var html = '<div id="common-dialog-search-2-textbox" class="easyui-dialog">';
			html += '<div class="PopupContentMid">';
			html += '<div class="GeneralForm Search1Form" >';
			html += '<label class="LabelStyle Label1Style" style=" width: 100px;">Mã NVBH</label>';
			html += '<input id="staffCode" maxlength="50" type="text" style="width: 145px;" class="InputTextStyle InputText1Style"/>';
			html += '<label class="LabelStyle Label1Style" style=" width: 100px;">Tên NVBH</label>';
			html += '<input id="staffName" maxlength="250" type="text" style="width: 145px;" class="InputTextStyle InputText1Style"/>';
			html += '<div class="Clear"></div>'; 
			html += '<div class="BtnCenterSection">';
			html += '<button class="BtnGeneralStyle BtnGeneralMStyle" tabindex="3" id="common-dialog-button-search">Tìm kiếm</button>';
			html += '</div>';
			html += '<div class="Clear"></div>';
			html += '<div class="GridSection" id="common-dialog-grid-container">';
			html += '<table id="common-dialog-grid-search"></table>';
			html += '</div>';
			html += '<div class="BtnCenterSection">';
			html += '<button id ="common-dialog-button-choose" class="BtnGeneralStyle BtnGeneralMStyle">Chọn</button>';
			html += '<button class="BtnGeneralStyle BtnGeneralMStyle" onclick="$(\'#common-dialog-search-2-textbox\').dialog(\'close\');">Đóng</button>';
			html += '</div>';
			html += '<p id="dialog-search-error" style="display: none;" class="ErrorMsgStyle"></p>';
			html += '</div>';
			html += '</div>';
			html += '</div>';
			//style="display: none;"
			$('body').append(html);
			
			$('#common-dialog-search-2-textbox').dialog({
				 title: 'CHỌN GIÁM SÁT NHÀ PHÂN PHỐI',
				 width: 580, 
				 height: 'auto',
				 closed: false,
				 cache: false,
				 modal: true,
				 onOpen: function() {
					 $('#common-dialog-search-2-textbox #common-dialog-button-search').unbind('click');
					 $('#common-dialog-search-2-textbox #common-dialog-button-search').bind('click', function() {
						 $('#common-dialog-search-2-textbox #dialog-search-error').html('').hide();
						 $('#common-dialog-grid-search').datagrid('reload');
					 });
					 $('#common-dialog-search-2-textbox input').unbind('keyup');
					 $('#common-dialog-search-2-textbox input').bind('keyup', function(e) {	
						 if(e.keyCode == keyCodes.ENTER) {
							 $('#common-dialog-search-2-textbox #common-dialog-button-search').click();
						 }
					 });
					 $('#common-dialog-search-2-textbox #common-dialog-button-choose').bind('click', function() {
						 if(CRMReportUtils._listStaffOwnerCode.size() == 0) {
							 $('#common-dialog-search-2-textbox #dialog-search-error').html('Bạn chưa chọn GSNPP. Vui lòng chọn GSNPP').show();
						 }else{
							 $('#common-dialog-search-2-textbox #dialog-search-error').html('').hide();
							 var listStaffStr = CRMReportUtils._listStaffOwnerCode.valArray[0].staffCode;
							 for(var i = 1; i < CRMReportUtils._listStaffOwnerCode.valArray.length; i++) {
								 listStaffStr += ';' + CRMReportUtils._listStaffOwnerCode.valArray[i].staffCode;
							 }
							 $('.ModuleList3Form #staffOwnerCode').val(listStaffStr);
							 $('.ModuleList3Form #staffCode').val("");
							 $('#common-dialog-search-2-textbox').dialog('close');
						 }
					 });
					 CRMReportUtils._listStaffOwnerCode = new Map();
					 $('#common-dialog-grid-search').datagrid({
						 url : '/commons/nvgs/search',
						 autoRowHeight : true,
						 rownumbers : true, 
						 pagination:true,
						 rowNum : 10,
						 pageSize:10,
						 scrollbarSize : 0,
						 pageNumber:1,
						 queryParams:{
							 page:1
						 },
						 onBeforeLoad: function(param) {
							 var code = $('#common-dialog-search-2-textbox #staffCode').val().trim();
							 var name = $('#common-dialog-search-2-textbox #staffName').val().trim();

							 param.code = code;
							 param.name = name;
							 param.shopId = shopId;
							 param.lstTBHVCode = $('#staffTBHV').val();
							 param = $.param(param, true);
						 },
						 onCheck : function (index,row) {
							 CRMReportUtils._listStaffOwnerCode.put(row.id, row);
						 },
						 onUncheck : function (index,row) { 
							 CRMReportUtils._listStaffOwnerCode.remove(row.id);
						 },
						 onCheckAll : function(rows) {
							 for(var i = 0; i < rows.length; i++) {
								 var row = rows[i];
								 CRMReportUtils._listStaffOwnerCode.put(row.id, row);
							 }
						 },
						 onUncheckAll : function(rows) {
							 for(var i = 0; i < rows.length; i++) {
								 var row = rows[i];
								 CRMReportUtils._listStaffOwnerCode.remove(row.id);
							 }
						 },
						 fitColumns:true,
						 width : ($('#common-dialog-search-2-textbox').width() - 40),
						 columns:[[
						 	{field: 'staffCode', title: 'Mã NVBH', align:'left', width: 40, sortable : false, resizable : false},
						 	{field: 'staffName', title: 'Tên NVBH', align:'left', width: 40, sortable : false, resizable : false},
						 	{field : 'choose', checkbox : true, align: 'center', width: 20, sortable : false, resizable : false}
						 ]],
						 onLoadSuccess :function(data){							 
							 if(($.isArray(data) && data.length != 0) || (data.rows != null && data.rows != undefined && $.isArray(data.rows) && data.rows.length != 0)) {
								 var __keys = (data.rows == null || data.rows == undefined) ? Object.keys(data[0]) : Object.keys(data.rows[0]);
								 var rows = (data.rows == null || data.rows == undefined) ? data : data.rows;
								 var isCheckAll=true;
								 for(var i = 0; i < rows.length; i++) {
									 if(CRMReportUtils._listStaffOwnerCode.get(rows[i].id)) {
										 $('#common-dialog-grid-search').datagrid('checkRow', i);
									 } else {
										 isCheckAll = false;
									 }
								 }
								 if(isCheckAll) {
									 $('#common-dialog-search-2-textbox .datagrid-header-row input[type=checkbox]').attr('checked', 'checked');
								 }
							 }
							 $('#common-dialog-search-2-textbox .datagrid-header-rownumber').html('STT');
							 var dHeight = $('#common-dialog-search-2-textbox').height();
							 var wHeight = $(window).height();
							 var top = (wHeight - dHeight) / 2 > 180 ? (wHeight - dHeight) / 2 - 180 : (wHeight - dHeight) / 2;
							 $('#common-dialog-search-2-textbox').dialog('move', {top : top});
						 }
					 });
				 },
				 onClose: function() {
					 CRMReportUtils._listStaffOwnerCode = null;
					 $('#common-dialog-search-2-textbox').remove(); 
				 }
			});
		},
		showDialogSearchTBHV : function(shopId){
			var html = '<div id="common-dialog-search-2-textbox" class="easyui-dialog">';
			html += '<div class="PopupContentMid">';
			html += '<div class="GeneralForm Search1Form" >';
			html += '<label class="LabelStyle Label1Style" style=" width: 100px;">Mã NVBH</label>';
			html += '<input id="staffCode" maxlength="50" type="text" style="width: 145px;" class="InputTextStyle InputText1Style"/>';
			html += '<label class="LabelStyle Label1Style" style=" width: 100px;">Tên NVBH</label>';
			html += '<input id="staffName" maxlength="250" type="text" style="width: 145px;" class="InputTextStyle InputText1Style"/>';
			html += '<div class="Clear"></div>'; 
			html += '<div class="BtnCenterSection">';
			html += '<button class="BtnGeneralStyle BtnGeneralMStyle" tabindex="3" id="common-dialog-button-search">Tìm kiếm</button>';
			html += '</div>';
			html += '<div class="Clear"></div>';
			html += '<div class="GridSection" id="common-dialog-grid-container">';
			html += '<table id="common-dialog-grid-search"></table>';
			html += '</div>';
			html += '<div class="BtnCenterSection">';
			html += '<button id ="common-dialog-button-choose" class="BtnGeneralStyle BtnGeneralMStyle">Chọn</button>';
			html += '<button class="BtnGeneralStyle BtnGeneralMStyle" onclick="$(\'#common-dialog-search-2-textbox\').dialog(\'close\');">Đóng</button>';
			html += '</div>';
			html += '<p id="dialog-search-error" style="display: none;" class="ErrorMsgStyle"></p>';
			html += '</div>';
			html += '</div>';
			html += '</div>';
			//style="display: none;"
			$('body').append(html);
			
			$('#common-dialog-search-2-textbox').dialog({
				 title: 'CHỌN TRƯỞNG BÁN HÀNG VÙNG',
				 width: 580, 
				 height: 'auto',
				 closed: false,
				 cache: false,
				 modal: true,
				 onOpen: function() {
					 $('#common-dialog-search-2-textbox #common-dialog-button-search').unbind('click');
					 $('#common-dialog-search-2-textbox #common-dialog-button-search').bind('click', function() {
						 $('#common-dialog-search-2-textbox #dialog-search-error').html('').hide();
						 $('#common-dialog-grid-search').datagrid('reload');
					 });
					 $('#common-dialog-search-2-textbox input').unbind('keyup');
					 $('#common-dialog-search-2-textbox input').bind('keyup', function(e) {	
						 if(e.keyCode == keyCodes.ENTER) {
							 $('#common-dialog-search-2-textbox #common-dialog-button-search').click();
						 }
					 });
					 $('#common-dialog-search-2-textbox #common-dialog-button-choose').bind('click', function() {
						 if(CRMReportUtils._listStaffTBHV.size() == 0) {
							 $('#common-dialog-search-2-textbox #dialog-search-error').html('Bạn chưa chọn TBHV. Vui lòng chọn TBHV').show();
						 }else{
							 $('#common-dialog-search-2-textbox #dialog-search-error').html('').hide();
							 var listStaffStr = CRMReportUtils._listStaffTBHV.valArray[0].staffCode;
							 for(var i = 1; i < CRMReportUtils._listStaffTBHV.valArray.length; i++) {
								 listStaffStr += ';' + CRMReportUtils._listStaffTBHV.valArray[i].staffCode;
							 }
							 $('.ModuleList3Form #staffTBHV').val(listStaffStr);
							 $('.ModuleList3Form #staffOwnerCode').val("");
							 $('.ModuleList3Form #staffCode').val("");
							 $('#common-dialog-search-2-textbox').dialog('close');
						 }
					 });
					 CRMReportUtils._listStaffTBHV = new Map();
					 $('#common-dialog-grid-search').datagrid({
						 url : '/commons/tbhv/search',
						 autoRowHeight : true,
						 rownumbers : true, 
						 pagination:true,
						 rowNum : 10,
						 pageSize:10,
						 scrollbarSize : 0,
						 pageNumber:1,
						 queryParams:{
							 page:1
						 },
						 onBeforeLoad: function(param) {
							 var code = $('#common-dialog-search-2-textbox #staffCode').val().trim();
							 var name = $('#common-dialog-search-2-textbox #staffName').val().trim();

							 param.code = code;
							 param.name = name;
							 param.shopId = shopId;
							 param = $.param(param, true);
						 },
						 onCheck : function (index,row) {
							 CRMReportUtils._listStaffTBHV.put(row.id, row);
						 },
						 onUncheck : function (index,row) { 
							 CRMReportUtils._listStaffTBHV.remove(row.id);
						 },
						 onCheckAll : function(rows) {
							 for(var i = 0; i < rows.length; i++) {
								 var row = rows[i];
								 CRMReportUtils._listStaffTBHV.put(row.id, row);
							 }
						 },
						 onUncheckAll : function(rows) {
							 for(var i = 0; i < rows.length; i++) {
								 var row = rows[i];
								 CRMReportUtils._listStaffTBHV.remove(row.id);
							 }
						 },
						 fitColumns:true,
						 width : ($('#common-dialog-search-2-textbox').width() - 40),
						 columns:[[
						 	{field: 'staffCode', title: 'Mã NVBH', align:'left', width: 40, sortable : false, resizable : false},
						 	{field: 'staffName', title: 'Tên NVBH', align:'left', width: 40, sortable : false, resizable : false},
						 	{field : 'choose', checkbox : true, align: 'center', width: 20, sortable : false, resizable : false}
						 ]],
						 onLoadSuccess :function(data){							 
							 if(($.isArray(data) && data.length != 0) || (data.rows != null && data.rows != undefined && $.isArray(data.rows) && data.rows.length != 0)) {
								 var __keys = (data.rows == null || data.rows == undefined) ? Object.keys(data[0]) : Object.keys(data.rows[0]);
								 var rows = (data.rows == null || data.rows == undefined) ? data : data.rows;
								 var isCheckAll=true;
								 for(var i = 0; i < rows.length; i++) {
									 if(CRMReportUtils._listStaffTBHV.get(rows[i].id)) {
										 $('#common-dialog-grid-search').datagrid('checkRow', i);
									 } else {
										 isCheckAll = false;
									 }
								 }
								 if(isCheckAll) {
									 $('#common-dialog-search-2-textbox .datagrid-header-row input[type=checkbox]').attr('checked', 'checked');
								 }
							 }
							 $('#common-dialog-search-2-textbox .datagrid-header-rownumber').html('STT');
							 var dHeight = $('#common-dialog-search-2-textbox').height();
							 var wHeight = $(window).height();
							 var top = (wHeight - dHeight) / 2 > 180 ? (wHeight - dHeight) / 2 - 180 : (wHeight - dHeight) / 2;
							 $('#common-dialog-search-2-textbox').dialog('move', {top : top});
						 }
					 });
				 },
				 onClose: function() {
					 CRMReportUtils._listStaffTBHV = null;
					 $('#common-dialog-search-2-textbox').remove(); 
				 }
			});
		},
		loadComboTree: function(controlId, storeCode,defaultValue,callback,params,vungType){		 
			var _url= '/rest/report/shop/combotree/1/0.json';
			if(vungType!=undefined && vungType!=null){
				_url = '/rest/report/vung/combotree/1/0.json';	
			}
			$('#' + controlId).combotree({url: _url,
				formatter: function(node) {
					return Utils.XSSEncode(node.text);
				},
				onChange: function(newValue,oldValue){				
					if(params != null && params.length >0){
						for(var i=0;i<params.length;i++){
							$('#'+params[i]).val('');
						}
					}
					if(DebitPayReport._lstStaff != null && DebitPayReport._lstStaff.size() >0){
						DebitPayReport._lstStaff = new Map();
					}
				}
			});
			var t = $('#'+ controlId).combotree('tree');	
			t.tree('options').url = '';
			t.tree({			
				formatter: function(node) {
					return Utils.XSSEncode(node.text);
				},
				onSelect:function(node){	
					if(callback!=undefined && callback!=null){//tungmt
						callback.call(this, node.id);
					}
					$('#'+storeCode).val(node.id);
					$.getJSON("/report/are-you-shop?shopId="+node.id,function(result){
						$('#isNPP').val(result.isNPP);
					});
					$('.ModuleList3Form #staffTBHV').val("");
					 $('.ModuleList3Form #staffOwnerCode').val("");
					 $('.ModuleList3Form #staffCode').val("");
				},	            				
				onBeforeExpand:function(result){	            					
					var nodes = t.tree('getChildren',result.target);
					for(var i=0;i<nodes.length;++i){
						t.tree('remove',nodes[i].target);
					}
					var ___jaxurl = '/rest/report/shop/combotree/2/';
					if(vungType!=undefined && vungType!=null){
						___jaxurl = '/rest/report/vung/combotree/2/';	
					}
					$.ajax({
						type : "POST",
						url : ___jaxurl + result.id +'.json',					
						dataType : "json",
						success : function(r) {						
							$(r).each(function(i,e){
								e.text = Utils.XSSEncode(e.text);
							});
							t.tree('append',{
								parent:result.target,
								data:r
							});
							$('.panel-body-noheader ul').css('display','block');
						}
					});
				},
				onExpand: function(result){	
					$('.panel-body-noheader ul').css('display','block');
				}
				});
			if(defaultValue == undefined || defaultValue == null){
				$('#' + controlId).combotree('setValue',activeType.ALL);
			} else {
				$('#' + controlId).combotree('setValue',defaultValue);
			}
		}
};
/*
 * END OF FILE - /web/web/resources/scripts/business/shop-report/vnm.crm-report.utils.js
 */

/*
 * START OF FILE - /web/web/resources/scripts/business/shop-report/vnm.ho-report.js
 */
var HoReport = {
		exportSLDS1_1: function() {
			$('#errMsg').hide();
			var msg = '';
			var multiShop = $("#shop").data("kendoMultiSelect");
			var dataShop = null;
			if (multiShop != null) {
				dataShop = multiShop.dataItems();
			}
			
			if(dataShop == null || dataShop.length <= 0){
				msg = format(msgErr_required_field,'Đơn vị');
			}
			
			if(msg.length == 0){
				msg = Utils.getMessageOfRequireCheck('fromDate','Từ ngày');
			}
			if(msg.length == 0){
				msg = Utils.getMessageOfInvalidFormatDate('fromDate','Từ ngày');
			}
			if(msg.length == 0){
				msg = Utils.getMessageOfRequireCheck('toDate','Đến ngày');
			}
			if(msg.length == 0){
				msg = Utils.getMessageOfInvalidFormatDate('toDate','Đến ngày');
			}
			var fDate = $('#fromDate').val();
			var tDate = $('#toDate').val();
			var curDate = ReportUtils.getCurrentDateString();
			if(msg.length == 0 && !Utils.compareDate(fDate, curDate)){
				msg = 'Từ ngày không được lớn hơn ngày hiện tại. Vui lòng nhập lại';
				$('#fromDate').focus();
			}	
			if(msg.length == 0 && !Utils.compareDate(tDate, curDate)){
				msg = 'Đến ngày không được lớn hơn ngày hiện tại. Vui lòng nhập lại';
				$('#toDate').focus();
			}
			if(msg.length == 0 && !Utils.compareDate(fDate, tDate)) {
				msg ='Từ ngày không được lớn hơn đến ngày. Vui lòng nhập lại';
				$('#fromDate').focus();
			}
			if(msg.length > 0){
				$('#errMsg').html(msg).show();
				return false;
			}
			
			var dataModel = new Object();
			var lstShopId = [];
			for(var i=0;i<dataShop.length;i++){
				lstShopId.push(dataShop[i].shopId);
			}
			dataModel.lstShopId = lstShopId.join(',');
			//dataModel.lstStaffSaleCode = $('#staffSaleCode').val().trim().toString();
			dataModel.fromDate = $('#fromDate').val();
			dataModel.toDate = $('#toDate').val();
			dataModel.formatType = $('input[type=radio]:checked').val();
			dataModel.typeSales = $('#typeSales').val();
			if($('#checkExport').is(':checked')) {
				dataModel.checkExport = true;
			} else {
				dataModel.checkExport = false;
			}
			ReportUtils.exportReport('/report/ho/slds1_1/export', dataModel);
		},
		/**
		 * Bc SLDS theo NVBH, SP, NH
		 * 
		 * @author hoanv25
		 * @since July 10, 2015
		 * */
		exportSLDS1_1_NUTI: function() {
			var msg = '';
			$('#errMsg').html('').hide();
			var nvbhKendo = $("#lstProduct").data("kendoMultiSelect");
			var lstNvbhId = nvbhKendo.value();
		/*	var multiShop = $("#shop").data("kendoMultiSelect");*/
			var dataShop = null;
			if (nvbhKendo != null) {
				dataShop = nvbhKendo.dataItems();
			}
			if ($('#shopId').val().trim().length == 0 || $('#shopId').val().trim() == activeType.ALL) {
				msg = format(msgErr_required_field,'đơn vị');
			}
			if (msg.length == 0) {
				msg = Utils.getMessageOfRequireCheck('fDate','Từ ngày');
			}
			if (msg.length == 0) {
				msg = Utils.getMessageOfRequireCheck('tDate','Đến ngày');
			}
			if (msg.length > 0) {
				$('#errMsg').html(msg).show();
				return false;
			}
			if (!checkDateForCycle()) {
				return;
			}
			var dataModel = new Object();		
			var lstShopId = [];
			for(var i=0;i<dataShop.length;i++){
				lstShopId.push(dataShop[i].nvbhName);
			}
			dataModel.strListShopId=$('#shopId').val();
			dataModel.fromDate = $('#fDate').val().trim();
			dataModel.toDate = $('#tDate').val().trim();
			dataModel.cycleId = $('#cycle').val();
			if ($('#category').val() > 0) {
				dataModel.categoryId = $('#category').val();
			}
			if ($('#sub_category').val() > 0) {
				dataModel.subCategoryId = $('#sub_category').val();		
			}
			if (lstNvbhId != undefined && lstNvbhId != null && lstNvbhId != "") {
				dataModel.strListProductId = lstNvbhId.toString().trim();
			}
			if (lstShopId != undefined && lstShopId != null && lstShopId != "") {
				dataModel.strListProductName = lstShopId.toString().trim();
			}
			ReportUtils.exportReport('/report/ho/slds1_1_nuti/export', dataModel);
		},
		
		/**
		 * Bc SLDS theo NVBH
		 * 
		 * @author hoanv25
		 * @since July 15, 2015
		 * */
		exportSLDS2_2_NUTI: function() {
			var msg = '';
			$('#errMsg').html('').hide();
			var nvbhKendo = $("#lstProduct").data("kendoMultiSelect");
			var lstNvbhId = nvbhKendo.value();
			var dataShop = null;
			if (nvbhKendo != null) {
				dataShop = nvbhKendo.dataItems();
			}
			if ($('#shopId').val().trim().length == 0 || $('#shopId').val().trim() == activeType.ALL) {
				msg = format(msgErr_required_field,'đơn vị');
			}
			if (msg.length == 0) {
				msg = Utils.getMessageOfRequireCheck('fDate','Từ ngày');
			}
			if (msg.length == 0) {
				msg = Utils.getMessageOfRequireCheck('tDate','Đến ngày');
			}
			if (msg.length > 0) {
				$('#errMsg').html(msg).show();
				return false;
			}
			if (!checkDateForCycle()) {
				return;
			}
			var dataModel = new Object();		
			var lstShopId = [];
			for(var i=0;i<dataShop.length;i++){
				lstShopId.push(dataShop[i].nvbhName);
			}
			dataModel.chooseShopId = $('#shopId').val();
			dataModel.fromDate = $('#fDate').val().trim();
			dataModel.toDate = $('#tDate').val().trim();
			dataModel.cycleId = $('#cycle').val();
			if ($('#category').val() > 0) {
				dataModel.categoryId = $('#category').val();
			}
			if ($('#sub_category').val() > 0) {
				dataModel.subCategoryId = $('#sub_category').val();		
			}
			if (lstNvbhId != undefined && lstNvbhId != null && lstNvbhId != "") {
				dataModel.strListProductId = lstNvbhId.toString().trim();
			}
			if (lstShopId != undefined && lstShopId != null && lstShopId != "") {
				dataModel.strListProductName = lstShopId.toString().trim();
			}
			ReportUtils.exportReport('/report/ho/slds1_2/export', dataModel);
		},
		
		/**
		 * Bc SLDS theo Khach Hang
		 * 
		 * @author hoanv25
		 * @since July 20, 2015
		 * */
		exportSLDS2_3_NUTI: function() {
			var msg = '';
			$('#errMsg').html('').hide();
			var nvbhKendo = $("#lstProduct").data("kendoMultiSelect");
			var lstNvbhId = nvbhKendo.value();
			var dataShop = null;
			if (nvbhKendo != null) {
				dataShop = nvbhKendo.dataItems();
			}
			if ($('#shopId').val().trim().length == 0 || $('#shopId').val().trim() == activeType.ALL) {
				msg = format(msgErr_required_field,'đơn vị');
			}
			if (msg.length == 0) {
				msg = Utils.getMessageOfRequireCheck('fDate','Từ ngày');
			}
			if (msg.length == 0) {
				msg = Utils.getMessageOfRequireCheck('tDate','Đến ngày');
			}
			if (msg.length > 0) {
				$('#errMsg').html(msg).show();
				return false;
			}
			if (!checkDateForCycle()) {
				return;
			}
			var dataModel = new Object();		
			var lstShopId = [];
			for(var i=0;i<dataShop.length;i++){
				lstShopId.push(dataShop[i].nvbhName);
			}
			dataModel.strListShopId=$('#shopId').val();
			dataModel.fromDate = $('#fDate').val().trim();
			dataModel.toDate = $('#tDate').val().trim();
			dataModel.cycleId = $('#cycle').val();
			if ($('#category').val() > 0) {
				dataModel.categoryId = $('#category').val();
			}
			if ($('#sub_category').val() > 0) {
				dataModel.subCategoryId = $('#sub_category').val();		
			}
			if (lstNvbhId != undefined && lstNvbhId != null && lstNvbhId != "") {
				dataModel.strListProductId = lstNvbhId.toString().trim();
			}
			if (lstShopId != undefined && lstShopId != null && lstShopId != "") {
				dataModel.strListProductName = lstShopId.toString().trim();
			}
			ReportUtils.exportReport('/report/ho/slds1_3/export', dataModel);
		},
		
		/**
		 * Bc SLDS 2.4 Theo doi KPI
		 * 
		 * @author hoanv25
		 * @since July 24, 2015
		 * */
		exportSLDS2_4_NUTI: function() {
			var msg = '';
			$('#errMsg').html('').hide();		
			if ($('#shopId').val().trim().length == 0 || $('#shopId').val().trim() == activeType.ALL) {
				msg = format(msgErr_required_field,'đơn vị');
			}		
			if (msg.length > 0) {
				$('#errMsg').html(msg).show();
				return false;
			}
			if (!checkDateForCycle()) {
				return;
			}
			var dataModel = new Object();		
			dataModel.strListShopId=$('#shopId').val();
			dataModel.cycleId = $('#cycle').val();			
			ReportUtils.exportReport('/report/ho/slds2_4/export', dataModel);
		},
		
		/**
		 * Bc 2.6 Danh sach don hang
		 * 
		 * @author hoanv25
		 * @since July 21, 2015
		 * */
		exportSLDS2_6_NUTI: function(){		
			var msg = '';	
			var nvbhKendo = $("#nvbh").data("kendoMultiSelect");
			var lstNvbhId = nvbhKendo.value();		
			var multiShop = $("#shop").data("kendoMultiSelect");
			var dataShop = multiShop.dataItems();
			var dataNVBH = null;
			if (nvbhKendo != null) {
				dataNVBH = nvbhKendo.dataItems();
			}
			if(dataShop!=null && dataShop.length>0){
				var lstShop = new Array();
				for(var i=0;i<dataShop.length;i++){
					lstShop.push(dataShop[i].id);
				}
			}
			var lstNVBH = [];
			for(var i=0;i<dataNVBH.length;i++){
				lstNVBH.push(dataNVBH[i].nvbhName);
			}
			$('#errMsg').html('').hide();
			if (msg.length == 0) {
				msg = Utils.getMessageOfRequireCheck('fDate','Từ ngày');
			}
			if (msg.length == 0) {
				msg = Utils.getMessageOfRequireCheck('tDate','Đến ngày');
			}
			if (msg.length > 0) {
				$('#errMsg').html(msg).show();
				return false;
			}
			if (!checkDateForCycle()) {
				return;
			}
			var orderType = $('#orderType').val();		
			var orderStatus = $('#orderStatus').val().trim();
			var dataModel = new Object();
			dataModel.strListShopId=lstShop.toString().trim();
			dataModel.fromDate = $('#fDate').val().trim();
			dataModel.toDate = $('#tDate').val().trim();
			dataModel.cycleId = $('#cycle').val();
			if (orderType != null) {
				dataModel.orderType = $('#orderType').val();
			}
			dataModel.orderNumber = $('#orderNumber').val().trim();	
			if ($('#orderSource').val().trim() != 0) {
				dataModel.orderSource = $('#orderSource').val().trim();
			}			
			if(orderStatus != '-1' && orderStatus != ''){
				if(orderStatus == '0'){//Đã trả
					dataModel.saleOrderType = 0;
				}else{
					var str=orderStatus.split('-');
					if(str.length > 1){
						dataModel.approved = str[0];
						if(str[1] != ''){
							dataModel.approvedStep = str[1];
						}
					}
				}
			}			
			if (lstNvbhId != undefined && lstNvbhId != null && lstNvbhId != "") {
				dataModel.strListNvbhId = lstNvbhId.toString().trim();
			}
			if (lstNVBH != undefined && lstNVBH != null && lstNVBH != "") {
				dataModel.strListNVBH = lstNVBH.toString().trim();
			}
			ReportUtils.exportReport('/report/ho/Rpt_HO_SLDS2_6/exportXLSX',dataModel);
		},
		
		exportSLDS1_2: function() {
			$('#errMsg').hide();
			var msg = '';
			var multiShop = $("#shop").data("kendoMultiSelect");
			var dataShop = null;
			if (multiShop != null) {
				dataShop = multiShop.dataItems();
			}
			
			if(dataShop == null || dataShop.length <= 0){
				msg = format(msgErr_required_field,'Đơn vị');
			}
			
			if(msg.length == 0){
				msg = Utils.getMessageOfRequireCheck('fromDate','Từ ngày');
			}
			if(msg.length == 0){
				msg = Utils.getMessageOfInvalidFormatDate('fromDate','Từ ngày');
			}
			if(msg.length == 0){
				msg = Utils.getMessageOfRequireCheck('toDate','Đến ngày');
			}
			if(msg.length == 0){
				msg = Utils.getMessageOfInvalidFormatDate('toDate','Đến ngày');
			}
			var fDate = $('#fromDate').val();
			var tDate = $('#toDate').val();
			var curDate = ReportUtils.getCurrentDateString();
			if(msg.length == 0 && !Utils.compareDate(fDate, curDate)){
				msg = 'Từ ngày không được lớn hơn ngày hiện tại. Vui lòng nhập lại';
				$('#fromDate').focus();
			}	
			if(msg.length == 0 && !Utils.compareDate(tDate, curDate)){
				msg = 'Đến ngày không được lớn hơn ngày hiện tại. Vui lòng nhập lại';
				$('#toDate').focus();
			}
			if(msg.length == 0 && !Utils.compareDate(fDate, tDate)) {
				msg ='Từ ngày không được lớn hơn đến ngày. Vui lòng nhập lại';
				$('#fromDate').focus();
			}
			
			if(msg.length > 0){
				$('#errMsg').html(msg).show();
				return false;
			}
			
			var dataModel = new Object();
			var lstShopId = [];
			for(var i=0;i<dataShop.length;i++){
				lstShopId.push(dataShop[i].shopId);
			}
			dataModel.lstShopId = lstShopId.join(',');
			//dataModel.lstStaffSaleCode = $('#staffSaleCode').val().trim().toString();
			dataModel.fromDate = $('#fromDate').val();
			dataModel.toDate = $('#toDate').val();
			dataModel.formatType = $('input[type=radio]:checked').val();
			if($('#checkExport').is(':checked')) {
				dataModel.checkExport = true;
			} else {
				dataModel.checkExport = false;
			}
			ReportUtils.exportReport('/report/ho/slds1_2/export', dataModel);
		},
		
		exportSLDS1_3:function() {
			var msg = '';
			$('#errMsg').html('').hide();
			var shopKendo = $("#shop").data("kendoMultiSelect");
			var dataShop = shopKendo.dataItems();
			if(dataShop==null||dataShop.length==0){
				msg = format(msgErr_required_field,'Đơn vị');
			}
			if(msg.length == 0){
				msg = Utils.getMessageOfRequireCheck('fromDate','Từ ngày');
			}
			if(msg.length == 0){
				msg = Utils.getMessageOfInvalidFormatDate('fromDate','Từ ngày');
			}
			if(msg.length == 0){
				msg = Utils.getMessageOfRequireCheck('toDate','Đến ngày');
			}
			if(msg.length == 0){
				msg = Utils.getMessageOfInvalidFormatDate('toDate','Đến ngày');
			}
			var fDate = $('#fromDate').val();
			var tDate = $('#toDate').val();
			var curDate = ReportUtils.getCurrentDateString();
			if(msg.length == 0 && !Utils.compareDate(fDate, curDate)){
				msg = 'Từ ngày không được lớn hơn ngày hiện tại. Vui lòng nhập lại';
				$('#fromDate').focus();
			}	
			if(msg.length == 0 && !Utils.compareDate(tDate, curDate)){
				msg = 'Đến ngày không được lớn hơn ngày hiện tại. Vui lòng nhập lại';
				$('#toDate').focus();
			}
			if(msg.length == 0 && !Utils.compareDate(fDate, tDate)) {
				msg ='Từ ngày không được lớn hơn đến ngày. Vui lòng nhập lại';
				$('#fromDate').focus();
			}
			if(msg.length > 0){
				$('#errMsg').html(msg).show();
				return false;
			}
			var lstShopId = dataShop[0].id;
			for(var i=1;i<dataShop.length;i++){
				lstShopId += "," + dataShop[i].shopId;
			}
			var dataModel = new Object();		
			dataModel.strListShopId = lstShopId;
			dataModel.fromDate = $('#fromDate').val();
			dataModel.toDate = $('#toDate').val();
			dataModel.formatType = $('input[type=radio]:checked').val();
			if($('#checkExport').is(':checked')) {
				dataModel.checkExport = true;
			} else {
				dataModel.checkExport = false;
			}
			ReportUtils.exportReport('/report/ho/slds1_3/export', dataModel);
		},
		
		exportXNTCT: function() {
			$('#errMsg').hide();
			var msg = '';
			var multiShop = $("#shop").data("kendoMultiSelect");
			var dataShop = null;
			if (multiShop != null) {
				dataShop = multiShop.dataItems();
			}
			
			if(dataShop == null || dataShop.length <= 0){
				msg = format(msgErr_required_field,'Đơn vị');
			}
			
			if (msg.length == 0) {
				msg = Utils.getMessageOfRequireCheck('fromDate', 'Từ ngày');
			}
			if (msg.length == 0) {
				msg = Utils.getMessageOfRequireCheck('toDate', 'Đến ngày');
			}
			if (msg.length == 0) {
				msg = Utils.getMessageOfInvalidFormatDate('fromDate', 'Từ ngày');
			}
			if (msg.length == 0) {
				msg = Utils.getMessageOfInvalidFormatDate('toDate', 'Từ ngày');
			}
			var fDate = $('#fromDate').val().trim();
			var tDate = $('#toDate').val().trim();
			if (msg.length == 0 && !Utils.compareCurrentDate(fDate)) {
				msg = 'Từ ngày phải nhỏ hơn hoặc bằng ngày hiện tại';
				$('#fromDate').focus();
			}

			if (msg.length == 0 && !Utils.compareDate(fDate, tDate)) {
				msg = 'Từ ngày phải nhỏ hơn hoặc bằng đến ngày';
				$('#fromDate').focus();
			}
			
			if(msg.length > 0){
				$('#errMsg').html(msg).show();
				return false;
			}
			
			var dataModel = new Object();
			var lstShopId = [];
			for(var i=0;i<dataShop.length;i++){
				lstShopId.push(dataShop[i].id);
			}
			
			dataModel.lstShopId = lstShopId.join(',');
			dataModel.catCode = $('#catCode').val().trim();
			dataModel.catCode = dataModel.catCode.replace(/ /g, '');
			dataModel.catCode = dataModel.catCode.replace(/;/g, ',');
			dataModel.productCode = $('#product').val().trim();
			dataModel.productCode = dataModel.productCode.replace(/ /g, '');
			dataModel.productCode = dataModel.productCode.replace(/;/g, ',');
			dataModel.fromDate = fDate;
			dataModel.toDate = tDate;
			dataModel.formatType = $('input[type=radio]:checked').val();
			if($('#checkExport').is(':checked')){
				dataModel.checkExport = true;
			}else{
				dataModel.checkExport = false;
			}
			if($('input[name=formatType]:radio:checked').length > 0 && $('input[name=formatType]:radio:checked').val() != undefined) {
	            dataModel.formatType = $('input[name=formatType]:radio:checked').val().toUpperCase();
			}
			ReportUtils.exportReport('/report/ho/xntct/export', dataModel);
		},
		exportF1: function() {
			$('#errMsg').hide();
			var msg = '';
			var multiShop = $("#shop").data("kendoMultiSelect");
			var dataShop = null;
			if (multiShop != null) {
				dataShop = multiShop.dataItems();
			}
			if(dataShop == null || dataShop.length <= 0){
				msg = format(msgErr_required_field,'Đơn vị');
			}
			if(msg.length == 0){
				msg = Utils.getMessageOfRequireCheck('fromDate','Từ ngày');
			}
			if(msg.length == 0){
				msg = Utils.getMessageOfInvalidFormatDate('fromDate','Từ ngày');
			}
			if(msg.length == 0){
				msg = Utils.getMessageOfRequireCheck('toDate','Đến ngày');
			}
			if(msg.length == 0){
				msg = Utils.getMessageOfInvalidFormatDate('toDate','Đến ngày');
			}
			var fDate = $('#fromDate').val();
			var tDate = $('#toDate').val();
			var curDate = ReportUtils.getCurrentDateString();
			if(msg.length == 0 && !Utils.compareDate(fDate, curDate)){
				msg = 'Từ ngày không được lớn hơn ngày hiện tại. Vui lòng nhập lại';
				$('#fromDate').focus();
			}	
			if(msg.length == 0 && !Utils.compareDate(tDate, curDate)){
				msg = 'Đến ngày không được lớn hơn ngày hiện tại. Vui lòng nhập lại';
				$('#toDate').focus();
			}
			if(msg.length == 0 && !Utils.compareDate(fDate, tDate)) {
				msg ='Từ ngày không được lớn hơn đến ngày. Vui lòng nhập lại';
				$('#fromDate').focus();
			}
			if(msg.length > 0){
				$('#errMsg').html(msg).show();
				return false;
			}
			
			var dataModel = new Object();
			var lstShopId = [];
			for(var i=0;i<dataShop.length;i++){
				lstShopId.push(dataShop[i].id);
			}
			dataModel.lstShopId = lstShopId.join(',');
			dataModel.catCode = $('#catCode').val().trim().toString();
			dataModel.productCode = $('#product').val().trim().toString();
			dataModel.fromDate = $('#fromDate').val();
			dataModel.toDate = $('#toDate').val();
			dataModel.formatType = $('input[type=radio]:checked').val();
			if($('#checkExport').is(':checked')){
				dataModel.checkExport = true;
			}else{
				dataModel.checkExport = false;
			}
			ReportUtils.exportReport('/report/ho/f1/export', dataModel);
		},
		
		exportSLDS1_4: function() {
			$('#errMsg').hide();
			var msg = '';
			var multiShop = $("#shop").data("kendoMultiSelect");
			var dataShop = null;
			if (multiShop != null) {
				dataShop = multiShop.dataItems();
			}
			
			if(dataShop == null || dataShop.length <= 0){
				msg = format(msgErr_required_field,'Đơn vị');
			}
			
			if (msg.length == 0) {
				msg = Utils.getMessageOfRequireCheck('fDate', 'Từ ngày');
			}
			if (msg.length == 0) {
				msg = Utils.getMessageOfRequireCheck('tDate', 'Đến ngày');
			}
			if (msg.length == 0) {
				msg = Utils.getMessageOfInvalidFormatDate('fDate', 'Từ ngày');
			}
			if (msg.length == 0) {
				msg = Utils.getMessageOfInvalidFormatDate('tDate', 'Từ ngày');
			}
			var fDate = $('#fDate').val().trim();
			var tDate = $('#tDate').val().trim();
			if (msg.length == 0 && !Utils.compareCurrentDate(fDate)) {
				msg = 'Từ ngày phải nhỏ hơn hoặc bằng ngày hiện tại';
				$('#fDate').focus();
			}

			if (msg.length == 0 && !Utils.compareDate(fDate, tDate)) {
				msg = 'Từ ngày phải nhỏ hơn hoặc bằng đến ngày';
				$('#fDate').focus();
			}
			
			if(msg.length > 0){
				$('#errMsg').html(msg).show();
				return false;
			}
			
			var dataModel = new Object();
			var lstShopId = [];
			for (var i=0; i < dataShop.length; i++) {
				lstShopId.push(dataShop[i].shopId);
			}
			dataModel.lstShopId = lstShopId.join(',');
			var s = $('#categoryCode').val().trim();
			s = s.replace(/ /g, '');
			dataModel.catCode = s.replace(/;/g, ',');
			dataModel.fromDate = fDate;
			dataModel.toDate = tDate;
			if($('#checkExport').is(':checked')) {
				dataModel.checkExport = true;
			} else {
				dataModel.checkExport = false;
			}
			dataModel.formatType = $('input[type=radio]:checked').val();
			var ck = $('input[type=radio]:checked').val();
			if (ck!= undefined && ck!=null && 'XLS'== ck) {
				ReportUtils.exportReport('/report/ho/slds1_4/export-xlsx', dataModel);
			} else {
				ReportUtils.exportReport('/report/ho/slds1_4/export', dataModel);
			}
		},
		/**
		 * Load ds san pham
		 * 
		 * @author hoanv25
		 * @since July 10, 2015
		 * */
		loadComboProduct:function(url){
			$("#lstProduct").kendoMultiSelect({
				dataSource: {
					transport: {
		                read: {
		                    dataType: "json",
		                    url: url
		                }
		            }
				},
				filter: "contains",
		        dataTextField: "nvbhName",
		        dataValueField: "nvbhId",
				itemTemplate: function(data, e, s, h, q) {
					return '<div class="tree-vnm" node-id="'+Utils.XSSEncode(data.nvbhId)+'" style="width:180px"><span class="tree-vnm-text">'+Utils.XSSEncode(data.nvbhCode)+' - '+Utils.XSSEncode(data.nvbhName) +'</span></div>';	
				},
		        tagTemplate:  '#: data.nvbhName #',
		        value: [$('#nvbhId').val()]		       
		    });	 
		},
		exportSLDS1_5: function() {
			$('#errMsg').hide();
			var msg = '';
			var multiShop = $("#shop").data("kendoMultiSelect");
			var dataShop = null;
			if (multiShop != null) {
				dataShop = multiShop.dataItems();
			}
			
			if(dataShop == null || dataShop.length <= 0){
				msg = format(msgErr_required_field,'Đơn vị');
			}
			
			if(msg.length == 0){
				msg = Utils.getMessageOfRequireCheck('fromDate','Từ ngày');
			}
			if(msg.length == 0){
				msg = Utils.getMessageOfInvalidFormatDate('fromDate','Từ ngày');
			}
			if(msg.length == 0){
				msg = Utils.getMessageOfRequireCheck('toDate','Đến ngày');
			}
			if(msg.length == 0){
				msg = Utils.getMessageOfInvalidFormatDate('toDate','Đến ngày');
			}
			var fDate = $('#fromDate').val();
			var tDate = $('#toDate').val();
			var curDate = ReportUtils.getCurrentDateString();
			if(msg.length == 0 && !Utils.compareDate(fDate, curDate)){
				msg = 'Từ ngày không được lớn hơn ngày hiện tại. Vui lòng nhập lại';
				$('#fromDate').focus();
			}	
			if(msg.length == 0 && !Utils.compareDate(tDate, curDate)){
				msg = 'Đến ngày không được lớn hơn ngày hiện tại. Vui lòng nhập lại';
				$('#toDate').focus();
			}
			if(msg.length == 0 && !Utils.compareDate(fDate, tDate)) {
				msg ='Từ ngày không được lớn hơn đến ngày. Vui lòng nhập lại';
				$('#fromDate').focus();
			}
			if(msg.length > 0){
				$('#errMsg').html(msg).show();
				return false;
			}
			
			var dataModel = new Object();
			var lstShopId = [];
			for(var i=0;i<dataShop.length;i++){
				lstShopId.push(dataShop[i].id);
			}
			dataModel.lstShopId = lstShopId.join(',');
			dataModel.fromDate = $('#fromDate').val();
			dataModel.toDate = $('#toDate').val();
			dataModel.formatType = $('input[type=radio]:checked').val();
			if($('#checkExport').is(':checked')) {
				dataModel.checkExport = true;
			} else {
				dataModel.checkExport = false;
			}
			ReportUtils.exportReport('/report/ho/slds1_5/export', dataModel);
		},
		
		finalFixed: {
			ALL: -2
		},
		/**
		 * Bao cao 6.2.2 ASO
		 * 
		 * @author hunglm16
		 * @return Excel File
		 * @since October 09,2015
		 * @description API - POI
		 */
		reportASOForExcel: function() {
			$('.ErrorMsgStyle').html('').change();
			var dataModel = new Object();
			var msg = '';
			//Lay tham so nganh hang
			var catKendo = $("#category").data("kendoMultiSelect");
			var lstCatKendo = catKendo.value();
			if (lstCatKendo != null && lstCatKendo != undefined && lstCatKendo.length > 0) {
				var catIdStr = lstCatKendo[0];
				for (var i = 1, size = lstCatKendo.length; i < size; i++) {
					catIdStr = catIdStr + "," + lstCatKendo[i];
				}
				dataModel.catIdStr = catIdStr;
			} else {
				dataModel.catIdStr = '';
			}
			//Lay tham so nganh hang con
			var catChildKendo = $("#categoryChild").data("kendoMultiSelect");
			var lstCatChildKendo = catChildKendo.value();
			if (lstCatChildKendo != null && lstCatChildKendo != undefined && lstCatChildKendo.length > 0) {
				var sbCatIdStr = lstCatChildKendo[0];
				for (var i = 1, size = lstCatChildKendo.length; i < size; i++) {
					sbCatIdStr = sbCatIdStr + "," + lstCatChildKendo[i];
				}
				dataModel.subCatIdStr = sbCatIdStr;
			} else {
				dataModel.subCatIdStr = '';
			}
			dataModel.shopId = $('#shop').combotree('getValue');
			dataModel.cycleId = $('#cycle').val();
			ReportUtils.exportReport('/report/ho/rpt_aso/export', dataModel);
		},
		/**
		 * Xu ly su load combo tree shop
		 * @author hunglm16
		 * @param controlId
		 * @param storeCode
		 * @param defaultValue
		 * @param callback
		 * @param params
		 * @returns
		 * @since 19/05/2015
		 * @description danh cho bao cao khuyen mai
		 */
		loadComboTree: function(controlId, storeCode, defaultValue, callback, params) {		 
			var _url= '/rest/report/shop/combotree/1/0.json';
			$('#' + controlId).combotree({url: _url, 
				formatter: function(node) {
					return Utils.XSSEncode(node.text);
				},
				onChange: function (newValue, oldValue) {
					var html = '<input id="promotionProgram" type="text" data-placeholder="Tất cả" style="width:215px; height: auto;" class="InputTextStyle InputText1Style" />';
					$('#ddHTMLPromotionProgram').html(html).change();
					//Xu ly load lai CTKM
					HoReport.changePromotionProgramByShop(StatusType.ACTIVE, $('#' + controlId).combotree('getValue'));
					var tmAZ = setTimeout(function() {
						$('#promotionProgram_listbox div').width(450);
						clearTimeout(tmAZ);
					}, 700);
				}
			});
			var t = $('#'+ controlId).combotree('tree');	
			t.tree('options').url = '';
			t.tree({			
				formatter: function(node) {
					return Utils.XSSEncode(node.text);
				},
				onSelect:function(node) {	
					$('#'+storeCode).val(node.id);
					if(callback!=undefined && callback!=null){
						callback.call(this, node.id);
					}
				},
			});
			if (defaultValue == undefined || defaultValue == null) {
				$('#' + controlId).combotree('setValue', activeType.ALL);
			} else {
				$('#' + controlId).combotree('setValue', defaultValue);
			}
		},
		/**
		 * Xu ly su kien thay doi cua combo tree shop
		 * @author hunglm16
		 * @param status
		 * @param shopId
		 * @returns
		 * @since 19/05/2015
		 * @description danh cho bao cao khuyen mai
		 */
		changePromotionProgramByShop: function(status, shopId) {
			if (status == undefined || status == null) {
				status = StatusType.ALL;
			}
			if (shopId == undefined || shopId == null) {
				shopId = StatusType.ALL;
			}
			var urlJS = "/rest/report/progamram_promotion/kendoui-combobox";
			urlJS = urlJS + "/" + status;
			urlJS = urlJS + "/" + shopId;
			urlJS = urlJS + ".json";
			$("#promotionProgram").kendoMultiSelect({
		        dataTextField: "promotionProgramCode",
		        dataValueField: "id",
		        filter: "contains",
				itemTemplate: function (data, e, s, h, q) {
					return '<div node-id="'+Utils.XSSEncode(data.equipStockId)+'" style="width:230px"><span><div style="width: 15px; display: inline-block;"></div></span><span>'+Utils.XSSEncode(data.promotionProgramCode) + ' - ' + Utils.XSSEncode(data.promotionProgramName)+'</span></div>';
				},
				tagTemplate: '#: data.promotionProgramCode #',
		        change: function(e) {
		        },
		        dataSource: {
		            transport: {
		                read: {
		                    dataType: "json",
		                    url: urlJS.trim()
		                }
		            }
		        }
		        ,value: [shopId]
		    });
		},
		
		/**
		 * Xuat bao cao 5.1 Tong ket khuyen mai
		 * 
		 * @author hunglm16
		 * @since 19/09/2015
		 */
		reportKM5D1TKKM: function() {
			$('.ErrorMsgStyle').html('').change();
			var msg = '';
			if (msg.length == 0) {
				msg = Utils.getMessageOfRequireCheck('fromDate', 'Từ ngày');
			}
			if (msg.length == 0) {
				msg = Utils.getMessageOfRequireCheck('toDate', 'Đến ngày');
			}
			if (msg.length == 0) {
				msg = Utils.getMessageOfInvalidFormatDate('fromDate','Từ ngày');
			}
			if (msg.length == 0) {
				msg = Utils.getMessageOfInvalidFormatDate('toDate','Đến ngày');
			}
			var fDate = $('#fromDate').val();
			var tDate = $('#toDate').val();
			if (!Utils.compareDate(fDate, tDate)) {
				msg = js_fromdate_todate_validate;		
				$('#fDate').focus();
			}
			if (msg.length == 0) {
				msg = Utils.compareCurrentDateEx('toDate','Đến ngày');
			}
			if (msg.length > 0) {
				$('#errMsg').html(msg).show();
				return false;
			}
			
			var proKendo = $("#promotionProgram").data("kendoMultiSelect");
			var lstProId = proKendo.value();
			
			var dataModel = new Object();
			dataModel.promationProgramStr = lstProId.toString().trim();
			dataModel.fromDate = $('#fromDate').val();
			dataModel.toDate = $('#toDate').val();
			dataModel.shopStr = $('#shop').combotree('getValue');
			ReportUtils.exportReport('/report/ho/km_5_1_tkkm/export', dataModel);
		},

		/**
		 * Bc theo yeu cau KH
		 * @author vuongmq
		 * @since 06/10/2015
		 * */
		exportGeneral: function() {
			var msg = '';
			$('.ErrorMsgStyle').html('').hide();
			if (msg.length == 0) {
				msg = Utils.getMessageOfRequireCheck('fDate', 'Ngày');
			}
			if (msg.length > 0) {
				$('#errMsg').html(msg).show();
				return false;
			}
			var dataModel = new Object();
			dataModel.shopStr = $('#curShopId').val();
			dataModel.typeReport = $('#type').val();
			dataModel.fromDate = $('#fDate').val().trim();
			ReportUtils.exportReport('/report/ho/general/export', dataModel);
		},
		/**
		 * Bc Don hang phat sinh trong ngay
		 * 
		 * @author hunglm16
		 * @since 19/09/2015
		 */
		report1D1DSPSTN: function() {
			$('.ErrorMsgStyle').html('').change();
			var msg = '';
			var msg = '';
			if (msg.length == 0) {
				msg = Utils.getMessageOfRequireCheck('fromDate', reportMultiLanguage.fromDate);
			}
			if (msg.length == 0) {
				msg = Utils.getMessageOfRequireCheck('toDate', reportMultiLanguage.toDate);
			}
			if (msg.length == 0) {
				msg = Utils.getMessageOfInvalidFormatDate('fromDate', reportMultiLanguage.fromDate);
			}
			if (msg.length == 0) {
				msg = Utils.getMessageOfInvalidFormatDate('toDate', reportMultiLanguage.toDate);
			}
			var fDate = $('#fromDate').val();
			var tDate = $('#toDate').val();
			if (!Utils.compareDate(fDate, tDate)) {
				msg = js_fromdate_todate_validate;		
				$('#fDate').focus();
			}
			if (msg.length == 0) {
				msg = Utils.compareCurrentDateEx('toDate', reportMultiLanguage.toDate);
			}
			if (msg.length > 0) {
				$('#errMsg').html(msg).show();
				return false;
			}
			
			var dataModel = new Object();
			dataModel.fromDate = $('#fromDate').val();
			dataModel.toDate = $('#toDate').val();
//			dataModel.cycleId = $('#cycle').val();
			dataModel.shopStr = $('#shop').combotree('getValue');
			ReportUtils.exportReport('/report/ho/rpt_1_1_dhpstn/export', dataModel);
		},
		
		/**
		 * Bc Thong ke don hang giao trong ngay
		 * 
		 * @author hunglm16
		 * @since 22/10/2015
		 */
		report1D2DSPSTN: function() {
			$('.ErrorMsgStyle').html('').change();
			var msg = '';
			if (msg.length == 0) {
				msg = Utils.getMessageOfRequireCheck('fromDate', reportMultiLanguage.fromDate);
			}
			if (msg.length == 0) {
				msg = Utils.getMessageOfRequireCheck('toDate', reportMultiLanguage.toDate);
			}
			if (msg.length == 0) {
				msg = Utils.getMessageOfInvalidFormatDate('fromDate', reportMultiLanguage.fromDate);
			}
			if (msg.length == 0) {
				msg = Utils.getMessageOfInvalidFormatDate('toDate', reportMultiLanguage.toDate);
			}
			var fDate = $('#fromDate').val();
			var tDate = $('#toDate').val();
			if (!Utils.compareDate(fDate, tDate)) {
				msg = js_fromdate_todate_validate;		
				$('#fDate').focus();
			}
			if (msg.length == 0) {
				msg = Utils.compareCurrentDateEx('toDate', reportMultiLanguage.toDate);
			}
			if (msg.length > 0) {
				$('#errMsg').html(msg).show();
				return false;
			}
			
			var dataModel = new Object();
			dataModel.fromDate = $('#fromDate').val();
			dataModel.toDate = $('#toDate').val();
//			dataModel.cycleId = $('#cycle').val();
			dataModel.shopStr = $('#shop').combotree('getValue');
			ReportUtils.exportReport('/report/ho/rpt_1_2_dhpstn/export', dataModel);
		},
		
		/**
		 * [1.3] Bao cao Giao hang (don hang) theo NVGH
		 * 
		 * @author hunglm16
		 * @since 22/10/2015
		 */
		report1D3DHTNVGH: function() {
			$('.ErrorMsgStyle').html('').change();
			var msg = '';
			if (msg.length == 0) {
				msg = Utils.getMessageOfRequireCheck('fromDate', reportMultiLanguage.fromDate);
			}
			if (msg.length == 0) {
				msg = Utils.getMessageOfRequireCheck('toDate', reportMultiLanguage.toDate);
			}
			if (msg.length == 0) {
				msg = Utils.getMessageOfInvalidFormatDate('fromDate', reportMultiLanguage.fromDate);
			}
			if (msg.length == 0) {
				msg = Utils.getMessageOfInvalidFormatDate('toDate', reportMultiLanguage.toDate);
			}
			var fDate = $('#fromDate').val();
			var tDate = $('#toDate').val();
			if (!Utils.compareDate(fDate, tDate)) {
				msg = js_fromdate_todate_validate;		
				$('#fDate').focus();
			}
			if (msg.length == 0) {
				msg = Utils.compareCurrentDateEx('toDate', reportMultiLanguage.toDate);
			}
			if (msg.length > 0) {
				$('#errMsg').html(msg).show();
				return false;
			}
			var dataModel = new Object();
			dataModel.fromDate = $('#fromDate').val();
			dataModel.toDate = $('#toDate').val();
//			dataModel.cycleId = $('#cycle').val();
			dataModel.shopStr = $('#shop').combotree('getValue');
			ReportUtils.exportReport('/report/ho/rpt_1_3_dhtnvgh/export', dataModel);
		},
		
		/**
		 * [1.4] Bao cao don hang rot
		 * 
		 * @author hunglm16
		 * @since 30/10/2015
		 */
		report1D4BCDHR: function() {
			$('.ErrorMsgStyle').html('').change();
			var msg = '';
			if (msg.length == 0) {
				msg = Utils.getMessageOfRequireCheck('fromDate', 'Từ ngày');
			}
			if (msg.length == 0) {
				msg = Utils.getMessageOfRequireCheck('toDate', 'Đến ngày');
			}
			if (msg.length == 0) {
				msg = Utils.getMessageOfInvalidFormatDate('fromDate','Từ ngày');
			}
			if (msg.length == 0) {
				msg = Utils.getMessageOfInvalidFormatDate('toDate','Đến ngày');
			}
			var fDate = $('#fromDate').val();
			var tDate = $('#toDate').val();
			if (!Utils.compareDate(fDate, tDate)) {
				msg = js_fromdate_todate_validate;		
				$('#fDate').focus();
			}
			if (msg.length == 0) {
				msg = Utils.compareCurrentDateEx('toDate','Đến ngày');
			}
			if (msg.length > 0) {
				$('#errMsg').html(msg).show();
				return false;
			}
			var dataModel = new Object();
			dataModel.fromDate = $('#fromDate').val();
			dataModel.toDate = $('#toDate').val();
//			dataModel.cycleId = $('#cycle').val();
			dataModel.shopStr = $('#shop').combotree('getValue');
			ReportUtils.exportReport('/report/ho/rpt_1_4_bcdhr/export', dataModel);
		},
		
		/**
		 * Bc SLDS theo Khach Hang
		 * @author vuongmq
		 * @since 26/10/2015
		 * */
		exportSLDSKH: function() {
			var msg = '';
			$('#errMsg').html('').hide();
			var productKendo = $("#lstProduct").data("kendoMultiSelect");
			var lstProduct = productKendo.value();
			var dataProduct = null;
			if (productKendo != null) {
				dataProduct = productKendo.dataItems();
			}
			if ($('#shopId').val().trim().length == 0 || $('#shopId').val().trim() == activeType.ALL) {
				msg = format(msgErr_required_field,'đơn vị');
			}
			if (msg.length == 0) {
				msg = Utils.getMessageOfRequireCheck('fDate','Từ ngày');
			}
			if (msg.length == 0) {
				msg = Utils.getMessageOfRequireCheck('tDate','Đến ngày');
			}
			if (msg.length > 0) {
				$('#errMsg').html(msg).show();
				return false;
			}
			if (!checkDateForCycle()) {
				return false;
			}
			var dataModel = new Object();
			var lstProductName = [];
			for (var i = 0; i < dataProduct.length; i++) {
				lstProductName.push(dataProduct[i].nvbhName);
			}
			dataModel.strListShopId = $('#shopId').val();
			dataModel.fromDate = $('#fDate').val().trim();
			dataModel.toDate = $('#tDate').val().trim();
			dataModel.cycleId = $('#cycle').val();
			if ($('#category').val() > 0) {
				dataModel.categoryId = $('#category').val();
			}
			if ($('#sub_category').val() > 0) {
				dataModel.subCategoryId = $('#sub_category').val();
			}
			if (lstProduct != undefined && lstProduct != null && lstProduct != "") {
				dataModel.strListProductId = lstProduct.toString().trim();
			}
			if (lstProductName != undefined && lstProductName != null && lstProductName != "") {
				dataModel.strListProductName = lstProductName.join(', ');
			}
			ReportUtils.exportReport('/report/ho/sldskh/export', dataModel);
		},

		/**
		 * Bc DS chuong trinh theo Khach Hang
		 * @author vuongmq
		 * @since 27/10/2015
		 * */
		exportDSCTKH: function() {
			var msg = '';
			$('#errMsg').html('').hide();
			if ($('#shopId').val().trim().length == 0 || $('#shopId').val().trim() == activeType.ALL) {
				msg = format(msgErr_required_field, 'đơn vị');
			}
			var ksId = $('#curKsId').val();
			var cycleIdFrom = $('#cycle').val();
			var cycleIdTo = $('#cycleTo').val();
			if (msg.length == 0) {
				if (isNullOrEmpty(ksId) || ksId == -1) {
					msg = 'Vui lòng chọn chương trình';
				}
			}
			if (msg.length == 0) {
				if (isNullOrEmpty(cycleIdFrom)) {
					msg = 'Từ chu kỳ không được để trống';
				}
			}
			if (msg.length == 0) {
				if (isNullOrEmpty(cycleIdTo)) {
					msg = 'Đến chu kỳ không được để trống';
				}
			}
			if (msg.length == 0) {
				if (!isNaN(cycleIdFrom) && !isNaN(cycleIdTo) && Number(cycleIdFrom) > Number(cycleIdTo)) {
					msg = 'Đến chu kỳ phải cùng hoặc sau từ chu kỳ';
				}
			}
			if (msg.length > 0) {
				$('#errMsg').html(msg).show();
				return false;
			}
			var dataModel = new Object();
			dataModel.strListShopId = $('#shopId').val();
			dataModel.ksId = ksId;
			dataModel.cycleId = cycleIdFrom;
			dataModel.cycleIdTo = cycleIdTo;
			ReportUtils.exportReport('/report/ho/dsctkh/export', dataModel);
		},
		
		/**
		 * Bao cao tien do thuc hien chuong trinh 5.5
		 * @author trietptm
		 * @since Nov 28, 2015
		 * */
		exportProgressProgram: function() {
			var msg = '';
			$('#errMsg').html('').hide();
			if ($('#shopId').val().trim().length == 0 || $('#shopId').val().trim() == activeType.ALL) {
				msg = format(msgErr_required_field, 'đơn vị');
			}
			var ksId = $('#curKsId').val();
			var cycleIdFrom = $('#cycle').val();
			var cycleIdTo = $('#cycleTo').val();
			if (msg.length == 0) {
				if (isNullOrEmpty(ksId) || ksId == -1) {
					msg = 'Vui lòng chọn chương trình';
				}
			}
			if (msg.length == 0) {
				if (isNullOrEmpty(cycleIdFrom)) {
					msg = 'Từ chu kỳ không được để trống';
				}
			}
			if (msg.length == 0) {
				if (isNullOrEmpty(cycleIdTo)) {
					msg = 'Đến chu kỳ không được để trống';
				}
			}
			if (msg.length == 0) {
				if (!isNaN(cycleIdFrom) && !isNaN(cycleIdTo) && Number(cycleIdFrom) > Number(cycleIdTo)) {
					msg = 'Đến chu kỳ phải cùng hoặc sau từ chu kỳ';
				}
			}
			if (msg.length > 0) {
				$('#errMsg').html(msg).show();
				return false;
			}
			var dataModel = new Object();
			dataModel.strListShopId = $('#shopId').val();
			dataModel.ksId = ksId;
			dataModel.cycleId = cycleIdFrom;
			dataModel.cycleIdTo = cycleIdTo;
			ReportUtils.exportReport('/report/ho/progress_program/export', dataModel);
		},

		/**
		 * Xu ly khi thay doi don vi; lay danh sach 
		 * @author vuongmq
		 * @since 19/10/2015
		 */
		viewCycleKeyShop: function(ksId) {
			if (ksId != undefined && ksId != null) {
				$.getJSON('/commons/view-cycle-key-shop?ksId=' + ksId, function(data) {
					$('#cycle').html('');
					$('#cycleTo').html('');
					if (data != undefined && data != null) {
						var rows = data.lstCycle;
						if (rows != undefined && rows != null && rows.length > 0) { 
							for (var i = 0; i < rows.length; i++) {
								$('#cycle').append('<option value = "' + Utils.XSSEncode(rows[i].cycleId) + '">' + Utils.XSSEncode(rows[i].cycleName) + '</option>'); 
								$('#cycleTo').append('<option value = "' + Utils.XSSEncode(rows[i].cycleId) + '">' + Utils.XSSEncode(rows[i].cycleName) + '</option>'); 
							}
						}
						$('#cycle').val(data.fromCycleId).change();
						$('#cycleTo').val(data.toCycleId).change();
					}
				});
			}
		},
		
		/**
		 * 6.4.4 bao cao Raw Data
		 * @author trietptm
		 * @since Nov 05, 2015
		 */
		exportRawData: function() {
			var msg = '';
			$('#errMsg').html('').hide();
			if ($('#shopId').val().trim().length == 0 || $('#shopId').val().trim() == activeType.ALL) {
				msg = format(msgErr_required_field, 'đơn vị');
			}
			if (msg.length == 0) {
				msg = Utils.getMessageOfRequireCheck('fDate', 'Từ ngày');
			}
			if (msg.length == 0) {
				msg = Utils.getMessageOfRequireCheck('tDate', 'Đến ngày');
			}
			if (msg.length == 0) {
				msg = Utils.getMessageOfInvalidFormatDate('fDate','Từ ngày');
			}
			if (msg.length == 0) {
				msg = Utils.getMessageOfInvalidFormatDate('tDate','Đến ngày');
			}
			var fDate = $('#fDate').val();
			var tDate = $('#tDate').val();
			if (!Utils.compareDate(fDate, tDate)) {
				msg = js_fromdate_todate_validate;		
				$('#fDate').focus();
			}
			if (msg.length == 0) {
				msg = Utils.compareCurrentDateEx('tDate','Đến ngày');
			}
			if (msg.length > 0) {
				$('#errMsg').html(msg).show();
				return false;
			}
			var dataModel = new Object();
			dataModel.strListShopId = $('#shopId').val();
			dataModel.fromDate = $('#fDate').val().trim();
			dataModel.toDate = $('#tDate').val().trim();
			ReportUtils.exportReport('/report/ho/raw-data/export', dataModel);
		},
		
		/**
		 * 6.4.5 bao cao Raw Data cho don dieu chinh
		 * @author trietptm
		 * @since Nov 16, 2015
		 */
		exportRawDataStockTrans: function() {
			var msg = '';
			$('#errMsg').html('').hide();
			if ($('#shopId').val().trim().length == 0 || $('#shopId').val().trim() == activeType.ALL) {
				msg = format(msgErr_required_field, 'đơn vị');
			}
			if (msg.length == 0) {
				msg = Utils.getMessageOfRequireCheck('fDate', 'Từ ngày');
			}
			if (msg.length == 0) {
				msg = Utils.getMessageOfRequireCheck('tDate', 'Đến ngày');
			}
			if (msg.length == 0) {
				msg = Utils.getMessageOfInvalidFormatDate('fDate','Từ ngày');
			}
			if (msg.length == 0) {
				msg = Utils.getMessageOfInvalidFormatDate('tDate','Đến ngày');
			}
			var fDate = $('#fDate').val();
			var tDate = $('#tDate').val();
			if (!Utils.compareDate(fDate, tDate)) {
				msg = js_fromdate_todate_validate;		
				$('#fDate').focus();
			}
			if (msg.length == 0) {
				msg = Utils.compareCurrentDateEx('tDate','Đến ngày');
			}
			if (msg.length > 0) {
				$('#errMsg').html(msg).show();
				return false;
			}
			var dataModel = new Object();
			dataModel.strListShopId = $('#shopId').val();
			dataModel.fromDate = $('#fDate').val().trim();
			dataModel.toDate = $('#tDate').val().trim();
			ReportUtils.exportReport('/report/ho/raw-data-stock-trans/export', dataModel);
		},
		
		/**
		* Bc cham cong
		* @author trietptm
		* @since Oct 27, 2015
		*/
		reportTimeKeeping: function() {
			var msg = '';
			$('#errMsg').html('').hide();		
			if ($('#shopId').val().trim().length == 0 || $('#shopId').val().trim() == activeType.ALL) {
				msg = format(msgErr_required_field,'đơn vị');
			}		
			if (msg.length > 0) {
				$('#errMsg').html(msg).show();
				return false;
			}
			if (!checkDateForCycle()) {
				return;
			}
			var dataModel = new Object();		
			dataModel.strListShopId=$('#shopId').val();
			dataModel.cycleId = $('#cycle').val();			
			ReportUtils.exportReport('/report/ho/time_keeping/export', dataModel);
		},

		/**
		 * Bc Aso khong hoat dong
		 * @author vuongmq
		 * @since 03/11/2015
		 * */
		exportAsoInactive: function() {
			var msg = '';
			$('#errMsg').html('').hide();
			msg = Utils.getMessageOfRequireCheck('cycle', 'Chu kỳ');
			if (msg.length > 0) {
				$('#errMsg').html(msg).show();
				return false;
			}
			var dataModel = new Object();
			dataModel.strListShopId = $('#shopId').val();
			dataModel.cycleId = $('#cycle').val();
			ReportUtils.exportReport('/report/ho/aso-inactive/export', dataModel);
		},
		
		/**
		 * Bc thoi gian ghe tham cua KH
		 * @author vuongmq
		 * @since 05/11/2015
		 * */
		exportTGGTKH: function() {
			var msg = '';
			$('#errMsg').html('').hide();
			if ($('#shopId').val().trim().length == 0 || $('#shopId').val().trim() == activeType.ALL) {
				msg = format(msgErr_required_field, 'đơn vị');
			}
			var msg = '';
			if (msg.length == 0) {
				msg = Utils.getMessageOfRequireCheck('fDate', 'Từ ngày');
			}
			if (msg.length == 0) {
				msg = Utils.getMessageOfRequireCheck('tDate', 'Đến ngày');
			}
			if (msg.length == 0) {
				msg = Utils.getMessageOfInvalidFormatDate('fDate','Từ ngày');
			}
			if (msg.length == 0) {
				msg = Utils.getMessageOfInvalidFormatDate('tDate','Đến ngày');
			}
			var fDate = $('#fDate').val();
			var tDate = $('#tDate').val();
			if (!Utils.compareDate(fDate, tDate)) {
				msg = js_fromdate_todate_validate;		
				$('#fDate').focus();
			}
			if (msg.length == 0) {
				msg = Utils.compareCurrentDateEx('tDate','Đến ngày');
			}
			if (msg.length > 0) {
				$('#errMsg').html(msg).show();
				return false;
			}
			var dataModel = new Object();
			dataModel.strListShopId = $('#shopId').val();
			dataModel.fromDate = $('#fDate').val().trim();
			dataModel.toDate = $('#tDate').val().trim();
			ReportUtils.exportReport('/report/ho/tggtkh/export', dataModel);
		},
		
		/**
		 * Bc khong ghe tham KH trong tuyen
		 * @author vuongmq
		 * @since 06/11/2015
		 * */
		exportKGTKHTT: function() {
			$('.ErrorMsgStyle').html('').change();
			var msg = '';
			$('#errMsg').html('').hide();
			var shopId = $('#shopId').val();
			if (shopId.trim().length == 0 || shopId.trim() == activeType.ALL) {
				msg = format(msgErr_required_field, 'Đơn vị');
			}
			var msg = '';
			if (msg.length == 0) {
				msg = Utils.getMessageOfRequireCheck('fDate', 'Từ ngày');
			}
			if (msg.length == 0) {
				msg = Utils.getMessageOfRequireCheck('tDate', 'Đến ngày');
			}
			if (msg.length == 0) {
				msg = Utils.getMessageOfInvalidFormatDate('fDate','Từ ngày');
			}
			if (msg.length == 0) {
				msg = Utils.getMessageOfInvalidFormatDate('tDate','Đến ngày');
			}
			var fDate = $('#fDate').val();
			var tDate = $('#tDate').val();
			if (!Utils.compareDate(fDate, tDate)) {
				msg = js_fromdate_todate_validate;		
				$('#fDate').focus();
			}
			if (msg.length == 0) {
				msg = Utils.compareCurrentDateEx('tDate', 'Đến ngày');
			}
			if (msg.length > 0) {
				$('#errMsg').html(msg).show();
				return false;
			}
			var dataModel = new Object();
			dataModel.strListShopId = shopId;
			dataModel.fromDate = fDate;
			dataModel.toDate = tDate;
			ReportUtils.exportReport('/report/ho/kgtkhtt/export', dataModel);
		},
		
		/**
		 * Bc chi tiet khuyen mai
		 * @author duongdt3
		 * @since 06/11/2015
		 * */
		exportBCCTKM: function() {
			$('.ErrorMsgStyle').html('').change();
			var msg = '';
			if (msg.length == 0) {
				msg = Utils.getMessageOfRequireCheck('fDate', reportMultiLanguage.fromDate);
			}
			if (msg.length == 0) {
				msg = Utils.getMessageOfRequireCheck('tDate', reportMultiLanguage.toDate);
			}
			if (msg.length == 0) {
				msg = Utils.getMessageOfInvalidFormatDate('fDate', reportMultiLanguage.fromDate);
			}
			if (msg.length == 0) {
				msg = Utils.getMessageOfInvalidFormatDate('tDate', reportMultiLanguage.toDate);
			}
			var fDate = $('#fDate').val();
			var tDate = $('#tDate').val();
			if (!Utils.compareDate(fDate, tDate)) {
				msg = js_fromdate_todate_validate;		
				$('#fDate').focus();
			}
			if (msg.length == 0) {
				msg = Utils.compareCurrentDateEx('tDate', reportMultiLanguage.toDate);
			}
			if (msg.length > 0) {
				$('#errMsg').html(msg).show();
				return false;
			}
			var dataModel = new Object();
			dataModel.strListShopId = $('#shopId').val();
			dataModel.fromDate = $('#fDate').val();
			dataModel.toDate = $('#tDate').val();
			ReportUtils.exportReport('/report/ho/bcctkm/export', dataModel);
		},
		
		/**
		 * [6.1.3] Bao cao don hang rot
		 * 
		 * @author hunglm16
		 * @since 09/11/2015
		 */
		reportGrowthSKUs: function() {
			$('.ErrorMsgStyle').html('').change();
			var dataModel = new Object();
			dataModel.year = $('#year').val();
			dataModel.shopId = $('#shop').combotree('getValue');
			ReportUtils.exportReport('/report/ho/rpt_growth_skus/export-excel', dataModel);
		},

		/**
		 * Bc Chi tiet mua hang
		 * @author vuongmq
		 * @since 29/12/2015
		 */
		report2D3CTMH: function() {
			$('.ErrorMsgStyle').html('').change();
			var msg = '';
			if (msg.length == 0) {
				msg = Utils.getMessageOfRequireCheck('fromDate', reportMultiLanguage.fromDate);
			}
			if (msg.length == 0) {
				msg = Utils.getMessageOfRequireCheck('toDate', reportMultiLanguage.toDate);
			}
			if (msg.length == 0) {
				msg = Utils.getMessageOfInvalidFormatDate('fromDate', reportMultiLanguage.fromDate);
			}
			if (msg.length == 0) {
				msg = Utils.getMessageOfInvalidFormatDate('toDate', reportMultiLanguage.toDate);
			}
			var fDate = $('#fromDate').val();
			var tDate = $('#toDate').val();
			if (!Utils.compareDate(fDate, tDate)) {
				msg = js_fromdate_todate_validate;		
				$('#fDate').focus();
			}
			if (msg.length == 0) {
				msg = Utils.compareCurrentDateEx('toDate', reportMultiLanguage.toDate);
			}
			if (msg.length > 0) {
				$('#errMsg').html(msg).show();
				return false;
			}
			var dataModel = new Object();
			dataModel.fromDate = $('#fromDate').val();
			dataModel.toDate = $('#toDate').val();
			dataModel.strListShopId = $('#shop').combotree('getValue'); // or $('#shop').val()
			ReportUtils.exportReport('/report/ho/rpt_2d3_ctmh/export', dataModel);
		},
		
		/**
		 * Bang ke chi tiet hang hoa mua vao
		 * @author dungnt19
		 * @since 19/01/2016
		 */
		report2D1CTHHMV: function() {
			$('.ErrorMsgStyle').html('').change();
			var msg = '';
			if (msg.length == 0) {
				msg = Utils.getMessageOfRequireCheck('fromDate', reportMultiLanguage.fromDate);
			}
			if (msg.length == 0) {
				msg = Utils.getMessageOfRequireCheck('toDate', reportMultiLanguage.toDate);
			}
			if (msg.length == 0) {
				msg = Utils.getMessageOfInvalidFormatDate('fromDate', reportMultiLanguage.fromDate);
			}
			if (msg.length == 0) {
				msg = Utils.getMessageOfInvalidFormatDate('toDate', reportMultiLanguage.toDate);
			}
			var fDate = $('#fromDate').val();
			var tDate = $('#toDate').val();
			if (!Utils.compareDate(fDate, tDate)) {
				msg = js_fromdate_todate_validate;		
				$('#fDate').focus();
			}
			if (msg.length == 0) {
				msg = Utils.compareCurrentDateEx('toDate', reportMultiLanguage.toDate);
			}
			if (msg.length > 0) {
				$('#errMsg').html(msg).show();
				return false;
			}
			var dataModel = new Object();
			dataModel.fromDate = $('#fromDate').val();
			dataModel.toDate = $('#toDate').val();
			dataModel.strListShopId = $('#shop').combotree('getValue'); // or $('#shop').val()
			ReportUtils.exportReport('/report/ho/rpt_2d1_cthhmv/export', dataModel);
		},
		
		/**
		 * So sanh so luong dat hang va giao hang theo don hang mua
		 * @author dungnt19
		 * @since 19/01/2016
		 */
		report2D2SSSLDHMH: function() {
			$('.ErrorMsgStyle').html('').change();
			var msg = '';
			if (msg.length == 0) {
				msg = Utils.getMessageOfRequireCheck('fromDate', reportMultiLanguage.fromDate);
			}
			if (msg.length == 0) {
				msg = Utils.getMessageOfRequireCheck('toDate', reportMultiLanguage.toDate);
			}
			if (msg.length == 0) {
				msg = Utils.getMessageOfInvalidFormatDate('fromDate', reportMultiLanguage.fromDate);
			}
			if (msg.length == 0) {
				msg = Utils.getMessageOfInvalidFormatDate('toDate', reportMultiLanguage.toDate);
			}
			var fDate = $('#fromDate').val();
			var tDate = $('#toDate').val();
			if (!Utils.compareDate(fDate, tDate)) {
				msg = js_fromdate_todate_validate;		
				$('#fDate').focus();
			}
			if (msg.length == 0) {
				msg = Utils.compareCurrentDateEx('toDate', reportMultiLanguage.toDate);
			}
			if (msg.length > 0) {
				$('#errMsg').html(msg).show();
				return false;
			}
			var dataModel = new Object();
			dataModel.fromDate = $('#fromDate').val();
			dataModel.toDate = $('#toDate').val();
			dataModel.strListShopId = $('#shop').combotree('getValue'); // or $('#shop').val()
			ReportUtils.exportReport('/report/ho/rpt_2d2_sssldhmh/export', dataModel);
		},
};
/*
 * END OF FILE - /web/web/resources/scripts/business/shop-report/vnm.ho-report.js
 */

/*
 * JavaScript file created by Rockstarapps Concatenation
*/

var ExportTrans = {
		ExportTransAdd: function(){
			$('#errMsg').html('').hide();
			var msg = '';
			var chot = '';
			if($('#shopCode').val().trim().length == 0 || $('#shopCode').val().trim() == activeType.ALL){
				msg = format(msgErr_required_field,'đơn vị');
			}
			if(msg.length == 0) {
				if($('#isNPP').val() == "false") {
					msg = "Đơn vị bạn vừa chọn không phải là 1 nhà phân phối cụ thể!";
				}
			}
			if(msg.length == 0){
				msg = Utils.getMessageOfRequireCheck('staffCode','NVBH');
			}
			if(msg.length == 0){
				msg = Utils.getMessageOfRequireCheck('transacName','Tên lệnh điều động');
			}
			if(msg.length == 0){
				msg = Utils.getMessageOfRequireCheck('transacDate','Ngày');
			}
			if(msg.length == 0){
				msg = Utils.getMessageOfRequireCheck('transacContent','Nội dung');
			}
			if(msg.length == 0){
				msg = Utils.getMessageOfSpecialCharactersValidate('staffCode','NPP',Utils._CODE);
			}	
			if(msg.length == 0){
				msg = Utils.getMessageOfInvalidFormatDate('transacDate','Ngày');
			}
			if(msg.length > 0){
				$('#errMsg').html(msg).show();
				$('#'+chot).focus();
				return false;
			}
			var exportTransaction = $("#exportTransaction").val();
			if(exportTransaction == null || exportTransaction == -1 || exportTransaction == "" || exportTransaction == undefined){
				$('#errMsg').html("Vui lòng chọn giao dịch xuất trong ngày").show();
				$('#exportTransaction').focus();
				return false;
			}
			if(msg.length > 0){
				$('#errMsg').html(msg).show();
				$('#'+chot).focus();
				return false;
			}
			var dataModel = new Object();		
			dataModel.shopCode = $('#shopCode').val().trim();
			dataModel.staffCode = $('#staffCode').val().trim();
			dataModel.stockTransId = $('#exportTransaction').val().trim();
			dataModel.transacDate = $('#transacDate').val().trim();
			dataModel.transacName = $('#transacName').val().trim();
			dataModel.transacContent = $('#transacContent').val().trim();
			dataModel.exportTransaction = $('#exportTransaction').val().trim();
			//ReportUtils.exportReport('/report/exportandtrans/viewexport',dataModel);
			var kData = $.param(dataModel,true);
			$('#divOverlay').show();		
			$.ajax({
				type : "POST",
				url : "/report/exportandtrans/viewexport",
				data :(kData),
				dataType: "json",
				success : function(data) {
					$('#divOverlay').hide();				
					if(!data.error) {
						if(data.hasData != undefined && data.hasData != null && data.hasData == false) {
							$('#errMsg').html('Không có dữ liệu để xuất báo cáo!').show();
						} else {
							window.location.href = data.path;
							setTimeout(function(){ //Set timeout để đảm bảo file load lên hoàn tất
	                            CommonSearch.deleteFileExcelExport(data.path);
							},2000);
						}
					} else {
						$('#errMsg').html(data.errMsg).show();					
					}
				},
				error:function(XMLHttpRequest, textStatus, errorDivThrown) {
					$('#divOverlay').hide();
					StockIssued.xhrExport = null;
				}
			});
		},
		getListStockTrans: function(staffCode, objStockTrans, stockTransId,isAll){
			if(staffCode != null && staffCode != undefined){
				$.ajax({
					type: "POST",
					url: "/report/exportandtrans/getliststocktrans",
					dataType: "json",
					data: ({staffCode:staffCode}),		        
					success: function(data) {
						//if(data!= null && data!= undefined && data.lstStockTrans!= null && data.lstStockTrans.length > 0){
						//data.hasData != null && data.hasData != undefined &&
						if( data.hasData == true ) {
							var html = new Array();
							if(isAll == undefined || isAll == null){
								html.push("<option value='-1'>[Chọn giao dịch xuất]</option>");
							}else{
								html.push("<option value='-1'>Tất cả</option>");
							}
							for(var i=0;i<data.lstStockTrans.length;i++){
								html.push("<option value='"+ data.lstStockTrans[i].id +"'>"+ Utils.XSSEncode(data.lstStockTrans[i].stockTransCode) +"</option>");
							}
							$(objStockTrans).html(html.join(""));
							if(stockTransId!= undefined && stockTransId > 0){
								$(objStockTrans).val(stockTransId);
								$(objStockTrans).change();
							}else{
								$(objStockTrans).change();
							}
						}else {
							$(objStockTrans).html("<option value='-1'>[Không có giao dịch xuất]</option>").change();
						}			        			        	
					},
					error: function (XMLHttpRequest, textStatus, errorThrown) {}
				});
			}else{
				if(isAll == undefined || isAll == null){
					$(objStockTrans).html("<option value='-1'>[Không có giao dịch xuất]</option>").change();
				}else{
					$(objStockTrans).html("<option value='-1'>Tất cả</option>").change();
				}
			}	
		},
		validateStaff: function(staffCode,shopId){
			if(staffCode != null && staffCode != undefined && shopId != null && shopId > 0){
				$.ajax({
					type: "POST",
					url: "/report/exportandtrans/validatestaff",
					dataType: "json",
					data: ({staffCode:staffCode,shopId:shopId}),		        
					success: function(data) {
						if (data.error) {
							var errMsg = 'NVBH khộng thuộc về đơn vị';
							$("#errMsg").html(errMsg).show();
							$("#staffCode").val('');
						}
					},
					error: function (XMLHttpRequest, textStatus, errorThrown) {
					}
				});
			}	
		},
		reportBCXNKNVBH : function(){//SangTN: Bao cao xuat nhap khau nhan vien ban hang
			$('#errMsg').html('').hide();
			var msg = '';
			if($('#shopId').val().trim().length == 0 || $('#shopId').val().trim() == activeType.ALL){
				msg = format(msgErr_required_field,'đơn vị');
			}
			if(msg.length == 0){
				if($('#isNPP').val() == "false"){
					msg = "Đơn vị bạn vừa chọn không phải là 1 nhà phân phối cụ thể!";
				}
			}
			if(msg.length == 0){
				msg = Utils.getMessageOfRequireCheck('fDate','Từ ngày');
			}
			if(msg.length == 0){
				msg = Utils.getMessageOfInvalidFormatDate('fDate','Từ ngày');
			}
			if(msg.length == 0){
				msg = Utils.getMessageOfRequireCheck('tDate','Đến ngày');
			}
			if(msg.length == 0){
				msg = Utils.getMessageOfInvalidFormatDate('tDate','Đến ngày');
			}
			var fDate = $('#fDate').val();
			var tDate = $('#tDate').val();
			if(!Utils.compareDate(fDate, tDate)){
				msg = msgErr_fromdate_greater_todate;		
				$('#fDate').focus();
			}
			var currentTime = new Date();
			var month = currentTime.getMonth() + 1;
			var day = currentTime.getDate();
			var year = currentTime.getFullYear();
			if(!Utils.compareDate(fDate,day + '/' + month + '/' + year)){
				msg = 'Từ ngày không được lớn hơn ngày hiện tại';
				$('#fDate').focus();
			}
			if(msg.length > 0){
				$('#errMsg').html(msg).show();
				return false;
			}
			var data = new Object();	
			if($('input[name=formatType]:radio:checked').length > 0 && $('input[name=formatType]:radio:checked').val() != undefined) {
				data.formatType = $('input[name=formatType]:radio:checked').val().toUpperCase();
			}
			data.shopId = $('#shopId').val().trim();
			data.staffCode = $('#staffCode').val().trim();
			data.fromDate = $('#fDate').val().trim();
			data.toDate = $('#tDate').val().trim();
			data.toDate = $('#tDate').val().trim();
			data.typeBill = $('#typeBill').val().trim();
			if($('input[name=formatType]:radio:checked').length > 0 && $('input[name=formatType]:radio:checked').val() != undefined) {
				data.formatType = $('input[name=formatType]:radio:checked').val().toUpperCase();
          }

			var kData = $.param(data,true);
			$('#divOverlay').show();		
			$.ajax({
				type : "POST",
				url : "/report/vansales/xnknvbh/export",
				data :(kData),
				dataType: "json",
				success : function(data) {
					$('#divOverlay').hide();				
					if(!data.error) {
						if(data.hasData != null && data.hasData != undefined && data.hasData == true) {
							window.location.href = data.path;
							setTimeout(function(){ //Set timeout để đảm bảo file load lên hoàn tất
			                    CommonSearch.deleteFileExcelExport(data.path);
							},2000);
						} else {
							$('#errMsg').html('Không có dữ liệu để xuất!').show();
						}
					} else {
						$('#errMsg').html(data.errMsg).show();					
					}
				},
				error:function(XMLHttpRequest, textStatus, errorDivThrown) {
					$('#divOverlay').hide();
					StockIssued.xhrExport = null;
				}
			});
			return false;
		},
		exportPXKKVCNB: function(){
			$('#errMsg').html('').hide();
			var msg = "";
			if($('#shopId').val().trim().length == 0 || $('#shopId').val().trim() == activeType.ALL){
				msg = format(msgErr_required_field,' Đơn vị');
			}
			if(msg.length == 0){
				if($('#isNPP').val() == "false"){
					msg = "Đơn vị bạn vừa chọn không phải là 1 nhà phân phối cụ thể!";
				}
			}
			if(msg.length == 0){
				msg = Utils.getMessageOfRequireCheck('staffCode','NVBH');
			}
			if(msg.length > 0) {
				$('#errMsg').html(msg).show();
				return false;
			}
			var exportTransaction = $("#idTransCode").val();
			if(exportTransaction == null || exportTransaction == -1 || exportTransaction == "" || exportTransaction == undefined){
				$('#errMsg').html("Vui lòng chọn giao dịch xuất trong ngày").show();
				$('#idTransCode').focus();
				return false;
			}
			if(msg.length == 0){
				msg = Utils.getMessageOfRequireCheck('ipNameTrans','Tên lệnh điều động');
			}
			if(msg.length == 0) {
				msg = Utils.getMessageOfRequireCheck('fDate','Từ ngày');
			}
			if(msg.length == 0)	{
				msg = Utils.getMessageOfInvalidFormatDate('fDate','Từ ngày');
			}
			if(msg.length == 0){
				msg = Utils.getMessageOfRequireCheck('ipContentTrans','Nội dung');
			}			
			if(msg.length > 0) {
				$('#errMsg').html(msg).show();
				return false;
			}
			var data = new Object();		
			data.shopId = $('#shopId').val().trim();
			data.staffCode = $('#staffCode').val().trim();
			data.fromDate = $('#fDate').val().trim();
			var tt = document.getElementById('idTransCode');
		    var selIndex = tt.selectedIndex;
		    var selValue = tt.options[selIndex].value;
			data.transacCode = selValue;
						
			data.transacContent = $('#ipContentTrans').val().trim();
			data.transacName = $('#ipNameTrans').val().trim();
			if($('input[name=formatType]:radio:checked').length > 0 && $('input[name=formatType]:radio:checked').val() != undefined) {
                data.formatType = $('input[name=formatType]:radio:checked').val().toUpperCase();
			}
			var kData = $.param(data,true);
			$('#divOverlay').show();		
			$.ajax({
				type : "POST",
				url : "/report/vansales/pxkkvcnb/export",
				data :(kData),
				dataType: "json",
				success : function(data) {
					$('#divOverlay').hide();				
					if(!data.error) {
						if(data.hasData != null && data.hasData != undefined && data.hasData == true) {
							window.location.href = data.path;
							setTimeout(function(){ //Set timeout để đảm bảo file load lên hoàn tất
			                    CommonSearch.deleteFileExcelExport(data.path);
							},2000);
						}else {
							$('#errMsg').html('Không có dữ liệu để xuất').show();
						}					
					} else {
						$('#errMsg').html(data.errMsg).show();
					}
				},
				error:function(XMLHttpRequest, textStatus, errorDivThrown) {
					$('#divOverlay').hide();
					StockIssued.xhrExport = null;
				}
			});
		}
};
//HungTT27
var SaleStaff = {
		reportSaleStaff: function(){
			$('#errMsg').html('').hide();
			var msg = '';
			if($('#shopId').val().trim().length == 0 || $('#shopId').val().trim() == activeType.ALL){
				msg = format(msgErr_required_field,'đơn vị');
			}			
			if(msg.length == 0){
				if($('#isNPP').val() == "false"){
					msg = "Đơn vị bạn vừa chọn không phải là 1 nhà phân phối cụ thể!";
				}
			}
			if(msg.length == 0){
				msg = Utils.getMessageOfRequireCheck('fDate','Từ ngày');
			}
			if(msg.length == 0){
				msg = Utils.getMessageOfInvalidFormatDate('fDate','Từ ngày');
			}
			if(msg.length == 0){
				msg = Utils.getMessageOfRequireCheck('tDate','Đến ngày');
			}
			if(msg.length == 0){
				msg = Utils.getMessageOfInvalidFormatDate('tDate','Đến ngày');
			}
			var fDate = $('#fDate').val();
			var tDate = $('#tDate').val();
			if(!Utils.compareDate(fDate, tDate)){
				msg = msgErr_fromdate_greater_todate;		
				$('#fDate').focus();
			}
			var currentTime = new Date();
			var month = currentTime.getMonth() + 1;
			var day = currentTime.getDate();
			var year = currentTime.getFullYear();
			if(!Utils.compareDate(fDate,day + '/' + month + '/' + year)){
				msg = 'Từ ngày không được lớn hơn ngày hiện tại';
				$('#fDate').focus();
			}
			if(msg.length > 0){
				$('#errMsg').html(msg).show();
				return false;
			}
			var data = new Object();		
			data.shopId = $('#shopId').val().trim();
			data.staffCode = $('#staffCode').val().trim();
			data.fromDate = $('#fDate').val().trim();
			data.toDate = $('#tDate').val().trim();
			if($('input[name=formatType]:radio:checked').length > 0 && $('input[name=formatType]:radio:checked').val() != undefined) {
                data.formatType = $('input[name=formatType]:radio:checked').val().toUpperCase();
			}

			var kData = $.param(data,true);
			$('#divOverlay').show();		
			$.ajax({
				type : "POST",
				url : "/report/vansales/xbtnv/export",
				data :(kData),
				dataType: "json",
				success : function(data) {
					$('#divOverlay').hide();				
					if(!data.error) {
						if(data.hasData != null && data.hasData != undefined && data.hasData == true) {
							window.location.href = data.path;
							setTimeout(function(){ //Set timeout để đảm bảo file load lên hoàn tất
			                    CommonSearch.deleteFileExcelExport(data.path);
							},2000);
						} else {
							$('#errMsg').html('Không có dữ liệu để xuất').show();
						}
					} else {
						$('#errMsg').html(data.errMsg).show();					
					}
				},
				error:function(XMLHttpRequest, textStatus, errorDivThrown) {
					$('#divOverlay').hide();
					StockIssued.xhrExport = null;
				}
			});
			return false;
			
//			var dataModel = new Object();		
//			dataModel.shopId = $('#shopCode').val().trim();
//			dataModel.fromDate = $('#fDate').val().trim();
//			dataModel.toDate = $('#tDate').val().trim();
//			dataModel.staffCode = $('#staffCode').val().trim();
//			ReportUtils.exportReport('/report/vansales/view',dataModel);
		}
};
var salePo = {
		PoAdd: function(){
			var msg = '';
			$('#errMsg').html('').hide();
			if($('#shopId').val().trim().length == 0 || $('#shopId').val().trim() == activeType.ALL){
				msg = format(msgErr_required_field,'đơn vị');
			}
			
			var chot = '';
			if (msg == ""){
				msg = Utils.getMessageOfRequireCheck('fDate', 'Từ ngày', false);
				chot = 'fDate';
			}
			if (msg == ""){
				ms = Utils.getMessageOfRequireCheck('tDate', 'Đến ngày', false);
				chot = 'tDate';
			}
			if(msg.length > 0){
				$('#errMsg').html(msg).show();
				$('#'+chot).focus();
				return false;
			}
			if(msg.length == 0){
				msg = Utils.getMessageOfInvalidFormatDate('fDate','Từ ngày');
			}
			if(msg.length == 0){
				msg = Utils.getMessageOfInvalidFormatDate('tDate','Đến ngày');
			}
			var fDate = $('#fDate').val();
			var tDate = $('#tDate').val();
			if(!Utils.compareDate(fDate, tDate)){
				msg = msgErr_fromdate_greater_todate;		
				$('#fDate').focus();
			}
			if(msg.length > 0){
				$('#errMsg').html(msg).show();
				return false;
			}
			var dataModel = new Object();		
			dataModel.shopId = $('#shopId').val().trim();
			dataModel.fromDate = $('#fDate').val().trim();
			dataModel.toDate = $('#tDate').val().trim();
			dataModel.lstStaffId = SuperviseCustomer.mapNVBH.keyArray;
			ReportUtils.exportReport('/report/sales-revenue/poView',dataModel);
		}
};
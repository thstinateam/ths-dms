var DebitPayReport = {
	_lstCustomer:null,
	_lstStaff:null,
	_lstStaffOwner:null,
	_lstProduct:null,
	_lstSubCat:null,
	_objectStaff:null,
	_objectProduct:null,
	_objectSubCat:null,
	/* BAO CAO TONG CONG NO */
	reportTCN : function(){
		var msg = '';
		var objectName = '';
		$('#errMsg').html('').hide();
//		if($('#shopId').val().trim().length == 0 || $('#shopId').val().trim() == activeType.ALL){
//			msg = format(msgErr_required_field,'đơn vị');
//		}
//		if(msg.length > 0){
//			$('#errMsg').html(msg).show();
//			return false;
//		}
		var dataModel = new Object();		
		dataModel.shopId = $('#shopId').val().trim();
		dataModel.objectType = $('#objectType').val();
		//dataModel.lstCustomer = $('#customerCode').val().trim().split(";");
		//dataModel.lstStaff = $('#staffCode').val().trim().split(";");
		dataModel.lstCustomer = $('#customerCode').val().trim().split(",");
		dataModel.lstStaff = $('#staffCode').val().trim().split(",");
		if($('input[name=formatType]:radio:checked').length > 0 && $('input[name=formatType]:radio:checked').val() != undefined) {
            dataModel.formatType = $('input[name=formatType]:radio:checked').val().toUpperCase();
       }

		ReportUtils.exportReport('/report/debit-pay/tcn/export',dataModel);
	},
	validateTTKH:function(){
		var msg = '';
		$('#errMsg').html('').hide();
		if($('#shopId').val().trim().length == 0 || $('#shopId').val().trim() == activeType.ALL){
			msg = format(msgErr_required_field,'đơn vị');
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfRequireCheck('fDate','Từ ngày');
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfInvalidFormatDate('fDate','Từ ngày');
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfRequireCheck('tDate','Đến ngày');
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfInvalidFormatDate('tDate','Đến ngày');
		}
		var fDate = $('#fDate').val();
		var tDate = $('#tDate').val();
		var curDate = $('#curDate').val();
		if(!Utils.compareDate(fDate, tDate)){
			msg = msgErr_fromdate_greater_todate;		
			$('#fDate').focus();
		}
		if(!Utils.compareDate(fDate, curDate)){
			msg = "Từ ngày không được lớn hơn ngày hiện tại";		
			$('#fDate').focus();
		}
		if(msg.length > 0){
			$('#errMsg').html(msg).show();
			return false;
		}
		return true;
	},
	selectCustomer:function(obj,code){
		if(code=='null') return;
		if(obj.checked){
			DebitPayReport._lstCustomer.put(code, code);
		}else{
			DebitPayReport._lstCustomer.remove(code);
		}
		var lstCutomer='';
		for(var i=0;i<DebitPayReport._lstCustomer.keyArray.length;i++){
			if(DebitPayReport._lstCustomer.keyArray[i]!=undefined && DebitPayReport._lstCustomer.keyArray[i]!=null)
				lstCutomer+= DebitPayReport._lstCustomer.keyArray[i]+";";
		}
		lstCutomer=lstCutomer.substring(0,lstCutomer.length-1);
		$('#customerCode').val(lstCutomer);
	},
	selectStaff:function(obj,code){
		if(code=='null') return;
		if(obj.checked){
			DebitPayReport._lstStaff.put(code, code);
		}else{
			DebitPayReport._lstStaff.remove(code);
		}
		var lstStaff='';
		for(var i=0;i<DebitPayReport._lstStaff.keyArray.length;i++){
			if(DebitPayReport._lstStaff.keyArray[i]!=undefined && DebitPayReport._lstStaff.keyArray[i]!=null)
				lstStaff+= DebitPayReport._lstStaff.keyArray[i]+";";
		}
		lstStaff=lstStaff.substring(0,lstStaff.length-1);
		var os = 'staffCode';
		if(DebitPayReport._objectStaff!=undefined && DebitPayReport._objectStaff!=null){
			os = DebitPayReport._objectStaff;
		}
		$('#'+os).val(lstStaff);
	},
	selectSupervisorStaff:function(obj,code){
		if(code=='null') return;
		if(obj.checked){
			DebitPayReport._lstStaffOwner.put(code, code);
		}else{
			DebitPayReport._lstStaffOwner.remove(code);
		}
		var lstStaff='';
		for(var i=0;i<DebitPayReport._lstStaffOwner.keyArray.length;i++){
			if(DebitPayReport._lstStaffOwner.keyArray[i]!=undefined && DebitPayReport._lstStaffOwner.keyArray[i]!=null)
				lstStaff+= DebitPayReport._lstStaffOwner.keyArray[i]+";";
		}
		lstStaff=lstStaff.substring(0,lstStaff.length-1);
		$('#staffOwnerCode').val(lstStaff);
	},
	selectProduct:function(obj,code){
		if(code=='null') return;
		if(obj.checked){
			DebitPayReport._lstProduct.put(code, code);
		}else{
			DebitPayReport._lstProduct.remove(code);
		}
		var lstProduct='';
		for(var i=0;i<DebitPayReport._lstProduct.keyArray.length;i++){
			if(DebitPayReport._lstProduct.keyArray[i]!=undefined && DebitPayReport._lstProduct.keyArray[i]!=null)
				lstProduct+= DebitPayReport._lstProduct.keyArray[i]+";";
		}
		lstProduct=lstProduct.substring(0,lstProduct.length-1);
		var os = 'productCode';
		if(DebitPayReport._objectProduct!=undefined && DebitPayReport._objectProduct!=null){
			os = DebitPayReport._objectProduct;
		}
		$('#'+os).val(lstProduct);
	},
	selectSubCat:function(obj,code){
		if(code=='null') return;
		if(obj.checked){
			DebitPayReport._lstSubCat.put(code, code);
		}else{
			DebitPayReport._lstSubCat.remove(code);
		}
		var lstSubCat='';
		for(var i=0;i<DebitPayReport._lstSubCat.keyArray.length;i++){
			if(DebitPayReport._lstSubCat.keyArray[i]!=undefined && DebitPayReport._lstSubCat.keyArray[i]!=null)
				lstSubCat+= DebitPayReport._lstSubCat.keyArray[i]+";";
		}
		lstSubCat=lstSubCat.substring(0,lstSubCat.length-1);
		var os = 'productCode';
		if(DebitPayReport._objectSubCat!=undefined && DebitPayReport._objectSubCat!=null){
			os = DebitPayReport._objectSubCat;
		}
		$('#'+os).val(lstSubCat);
	},
	reportTTKH : function(){
		if(!DebitPayReport.validateTTKH()) return false;
		var dataModel = new Object();		
		dataModel.shopId = $('#shopId').val().trim();
		dataModel.fromDate = $('#fDate').val().trim();
		dataModel.toDate = $('#tDate').val().trim();
		dataModel.typePerson = $('#objectType').val();
		//dataModel.lstCustomer = $('#customerCode').val().trim().split(";");
		//dataModel.lstStaff = $('#staffCode').val().trim().split(";");
		if($('input[name=formatType]:radio:checked').length > 0 && $('input[name=formatType]:radio:checked').val() != undefined) {
            dataModel.formatType = $('input[name=formatType]:radio:checked').val().toUpperCase();
		}
		if(dataModel.typePerson == 1)
		{
			dataModel.codePerson = $('#staffCode').val().trim();
		}
		else
		{
			dataModel.codePerson = $('#customerCode').val().trim();
		}
		
		$.getJSON("/report/are-you-shop?shopId="+dataModel.shopId,function(result)
				{
					
					if(!result.isNPP)
					{
						msg = 'Đơn vị chọn phải là nhà phân phối';
						$('#errMsg').html(msg).show();
						
					}
					else
					{
						ReportUtils.exportReport('/report/debit-pay/ttkh/export',dataModel);
					}
				});
		
		
	},
	exportPT : function () {
		$('#errMsg').html('').hide();
		var msg = '';
		if($('#shopId').val().trim().length == 0 || $('#shopId').val().trim() == activeType.ALL){
			msg = format(msgErr_required_field,'đơn vị');
		}
		
		if(msg.length == 0){
			if($('#isNPP').val() == "false"){
				msg = "Đơn vị bạn vừa chọn không phải là 1 nhà phân phối cụ thể!";
			}
		}
		
		if(msg.length == 0){
			msg = Utils.getMessageOfRequireCheck('fDate','Từ ngày');
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfInvalidFormatDate('fDate','Từ ngày');
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfRequireCheck('tDate','Đến ngày');
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfInvalidFormatDate('tDate','Đến ngày');
		}
		var fDate = $('#fDate').val();
		var tDate = $('#tDate').val();
		if(!Utils.compareDate(fDate, tDate)){
			msg = msgErr_fromdate_greater_todate;		
			$('#fDate').focus();
		}
		var currentTime = new Date();
		var month = currentTime.getMonth() + 1;
		var day = currentTime.getDate();
		var year = currentTime.getFullYear();
		if(!Utils.compareDate(fDate,day + '/' + month + '/' + year)){
			msg = 'Từ ngày không được lớn hơn ngày hiện tại';
			$('#fDate').focus();
		}
		if(msg.length > 0){
			$('#errMsg').html(msg).show();
			return false;
		}
		var data = new Object();		
		data.shopId = $('#shopId').val().trim();
		data.customerCode = $('#customerCode').val().trim();
		data.fromDate = $('#fDate').val().trim();
		data.toDate = $('#tDate').val().trim();
		if($('input[name=formatType]:radio:checked').length > 0 && $('input[name=formatType]:radio:checked').val() != undefined) {
			data.formatType = $('input[name=formatType]:radio:checked').val().toUpperCase();
      }

		var kData = $.param(data,true);
		$('#divOverlay').show();		
		$.ajax({
			type : "POST",
			url : "/report/debit-pay/pt/export",
			data :(kData),
			dataType: "json",
			success : function(data) {
				$('#divOverlay').hide();				
				if(!data.error) {
					if(data.hasData != null && data.hasData != undefined && data.hasData == true) {
						window.location.href = data.path;
						setTimeout(function(){ //Set timeout để đảm bảo file load lên hoàn tất
		                    CommonSearch.deleteFileExcelExport(data.path);
						},2000);
					} else {
						$('#errMsg').html('Không có dữ liệu để xuất').show();
					}
				} else {
					$('#errMsg').html(data.errMsg).show();					
				}
			},
			error:function(XMLHttpRequest, textStatus, errorDivThrown) {
				$('#divOverlay').hide();
				StockIssued.xhrExport = null;
			}
		});
		return false;
	}
};
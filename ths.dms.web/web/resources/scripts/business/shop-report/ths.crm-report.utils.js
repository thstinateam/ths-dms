//TUNGTT
var CRMReportUtils = {
		_listStaffCode:null,
		_listStaffOwnerCode:null,
		_listStaffTBHV:null,
		_listProduct:null,

		showDialogSearchProduct : function(){
			var html = '<div id="common-dialog-search-2-textbox" class="easyui-dialog">';
			html += '<div class="PopupContentMid">';
			html += '<div class="GeneralForm Search1Form" >';
			html += '<label class="LabelStyle Label1Style" style=" width: 100px;">Mã SP</label>';
			html += '<input id="productCode" maxlength="50" type="text" style="width: 145px;" class="InputTextStyle InputText1Style"/>';
			html += '<label class="LabelStyle Label1Style" style=" width: 100px;">Tên SP</label>';
			html += '<input id="productName" maxlength="250" type="text" style="width: 145px;" class="InputTextStyle InputText1Style"/>';
			html += '<div class="Clear"></div>'; 
			html += '<div class="BtnCenterSection">';
			html += '<button class="BtnGeneralStyle BtnGeneralMStyle" tabindex="3" id="common-dialog-button-search">Tìm kiếm</button>';
			html += '</div>';
			html += '<div class="Clear"></div>';
			html += '<div class="GridSection" id="common-dialog-grid-container">';
			html += '<table id="common-dialog-grid-search"></table>';
			html += '</div>';
			html += '<div class="BtnCenterSection">';
			html += '<button id ="common-dialog-button-choose" class="BtnGeneralStyle BtnGeneralMStyle">Chọn</button>';
			html += '<button class="BtnGeneralStyle BtnGeneralMStyle" onclick="$(\'#common-dialog-search-2-textbox\').dialog(\'close\');">Đóng</button>';
			html += '</div>';
			html += '<p id="dialog-search-error" style="display: none;" class="ErrorMsgStyle"></p>';
			html += '</div>';
			html += '</div>';
			html += '</div>';
			//style="display: none;"
			$('body').append(html);
			
			$('#common-dialog-search-2-textbox').dialog({
				 title: 'CHỌN SẢN PHẨM',
				 width: 580, 
				 height: 'auto',
				 closed: false,
				 cache: false,
				 modal: true,
				 onOpen: function() {
					 $('#common-dialog-search-2-textbox #common-dialog-button-search').unbind('click');
					 $('#common-dialog-search-2-textbox #common-dialog-button-search').bind('click', function() {
						 $('#common-dialog-search-2-textbox #dialog-search-error').html('').hide();
						 $('#common-dialog-grid-search').datagrid('reload');
					 });
					 $('#common-dialog-search-2-textbox input').unbind('keyup');
					 $('#common-dialog-search-2-textbox input').bind('keyup', function(e) {	
						 if(e.keyCode == keyCodes.ENTER) {
							 $('#common-dialog-search-2-textbox #common-dialog-button-search').click();
						 }
					 });
					 $('#common-dialog-search-2-textbox #common-dialog-button-choose').bind('click', function() {
						 if(CRMReportUtils._listProduct.size() == 0) {
							 $('#common-dialog-search-2-textbox #dialog-search-error').html('Bạn chưa chọn sản phẩm. Vui lòng chọn sản phẩm').show();
						 }else{
							 $('#common-dialog-search-2-textbox #dialog-search-error').html('').hide();
							 var listStaffStr = CRMReportUtils._listProduct.valArray[0].productCode;
							 for(var i = 1; i < CRMReportUtils._listProduct.valArray.length; i++) {
								 listStaffStr += ',' + Utils.XSSEncode(CRMReportUtils._listProduct.valArray[i].productCode);
							 }
							 $('#product').val(Utils.XSSEncode(listStaffStr));
							 $('#common-dialog-search-2-textbox').dialog('close');
						 }
					 });
					 CRMReportUtils._listProduct = new Map();
					 $('#common-dialog-grid-search').datagrid({
						 url : '/commons/product/search',
						 autoRowHeight : true,
						 rownumbers : true, 
						 pagination:true,
						 rowNum : 10,
						 pageSize:10,
						 scrollbarSize : 0,
						 pageNumber:1,
						 queryParams:{
							 page:1
						 },
						 onBeforeLoad: function(param) {
							 var code = $('#common-dialog-search-2-textbox #productCode').val().trim();
							 var name = $('#common-dialog-search-2-textbox #productName').val().trim();

							 param.code = code;
							 param.name = name;
							 
							 param = $.param(param, true);
						 },
						 onCheck : function (index,row) {
							 CRMReportUtils._listProduct.put(row.id, row);
						 },
						 onUncheck : function (index,row) { 
							 CRMReportUtils._listProduct.remove(row.id);
						 },
						 onCheckAll : function(rows) {
							 for(var i = 0; i < rows.length; i++) {
								 var row = rows[i];
								 CRMReportUtils._listProduct.put(row.id, row);
							 }
						 },
						 onUncheckAll : function(rows) {
							 for(var i = 0; i < rows.length; i++) {
								 var row = rows[i];
								 CRMReportUtils._listProduct.remove(row.id);
							 }
						 },
						 fitColumns:true,
						 width : ($('#common-dialog-search-2-textbox').width() - 40),
						 columns:[[
						 	{field: 'productCode', title: 'Mã SP', align:'left', width: 40, sortable : false, resizable : false},
						 	{field: 'productName', title: 'Tên SP', align:'left', width: 40, sortable : false, resizable : false},
						 	{field : 'choose', checkbox : true, align: 'center', width: 20, sortable : false, resizable : false}
						 ]],
						 onLoadSuccess :function(data){							 
							 if(($.isArray(data) && data.length != 0) || (data.rows != null && data.rows != undefined && $.isArray(data.rows) && data.rows.length != 0)) {
								 var __keys = (data.rows == null || data.rows == undefined) ? Object.keys(data[0]) : Object.keys(data.rows[0]);
								 var rows = (data.rows == null || data.rows == undefined) ? data : data.rows;
								 var isCheckAll=true;
								 for(var i = 0; i < rows.length; i++) {
									 if(CRMReportUtils._listProduct.get(rows[i].id)) {
										 $('#common-dialog-grid-search').datagrid('checkRow', i);
									 } else {
										 isCheckAll = false;
									 }
								 }
								 if(isCheckAll) {
									 $('#common-dialog-search-2-textbox .datagrid-header-row input[type=checkbox]').attr('checked', 'checked');
								 }else{
									 $('#common-dialog-search-2-textbox .datagrid-header-row input[type=checkbox]').attr('checked', false);
								 }
							 }
							 $('#common-dialog-search-2-textbox .datagrid-header-rownumber').html('STT');
							 var dHeight = $('#common-dialog-search-2-textbox').height();
							 var wHeight = $(window).height();
							 var top = (wHeight - dHeight) / 2 > 180 ? (wHeight - dHeight) / 2 - 180 : (wHeight - dHeight) / 2;
							 $('#common-dialog-search-2-textbox').dialog('move', {top : top});
						 }
					 });
				 },
				 onClose: function() {
					 CRMReportUtils._listProduct = null;
					 $('#common-dialog-search-2-textbox').remove(); 
				 }
			});
		},
		showDialogSearchNVBH : function(shopId,isGetShopOnly){
			var html = '<div id="common-dialog-search-2-textbox" class="easyui-dialog">';
			html += '<div class="PopupContentMid">';
			html += '<div class="GeneralForm Search1Form" >';
			html += '<label class="LabelStyle Label1Style" style=" width: 100px;">Mã NVBH</label>';
			html += '<input id="staffCode" maxlength="50" type="text" style="width: 145px;" class="InputTextStyle InputText1Style"/>';
			html += '<label class="LabelStyle Label1Style" style=" width: 100px;">Tên NVBH</label>';
			html += '<input id="staffName" maxlength="250" type="text" style="width: 145px;" class="InputTextStyle InputText1Style"/>';
			html += '<div class="Clear"></div>'; 
			html += '<div class="BtnCenterSection">';
			html += '<button class="BtnGeneralStyle BtnGeneralMStyle" tabindex="3" id="common-dialog-button-search">Tìm kiếm</button>';
			html += '</div>';
			html += '<div class="Clear"></div>';
			html += '<div class="GridSection" id="common-dialog-grid-container">';
			html += '<table id="common-dialog-grid-search"></table>';
			html += '</div>';
			html += '<div class="BtnCenterSection">';
			html += '<button id ="common-dialog-button-choose" class="BtnGeneralStyle BtnGeneralMStyle">Chọn</button>';
			html += '<button class="BtnGeneralStyle BtnGeneralMStyle" onclick="$(\'#common-dialog-search-2-textbox\').dialog(\'close\');">Đóng</button>';
			html += '</div>';
			html += '<p id="dialog-search-error" style="display: none;" class="ErrorMsgStyle"></p>';
			html += '</div>';
			html += '</div>';
			html += '</div>';
			//style="display: none;"
			$('body').append(html);
			
			$('#common-dialog-search-2-textbox').dialog({
				 title: 'CHỌN NHÂN VIÊN BÁN HÀNG',
				 width: 580, 
				 height: 'auto',
				 closed: false,
				 cache: false,
				 modal: true,
				 onOpen: function() {
					 $('#common-dialog-search-2-textbox #common-dialog-button-search').unbind('click');
					 $('#common-dialog-search-2-textbox #common-dialog-button-search').bind('click', function() {
						 $('#common-dialog-search-2-textbox #dialog-search-error').html('').hide();
						 $('#common-dialog-grid-search').datagrid('reload');
					 });
					 $('#common-dialog-search-2-textbox input').unbind('keyup');
					 $('#common-dialog-search-2-textbox input').bind('keyup', function(e) {	
						 if(e.keyCode == keyCodes.ENTER) {
							 $('#common-dialog-search-2-textbox #common-dialog-button-search').click();
						 }
					 });
					 $('#common-dialog-search-2-textbox #common-dialog-button-choose').bind('click', function() {
						 if(CRMReportUtils._listStaffCode.size() == 0) {
							 $('#common-dialog-search-2-textbox #dialog-search-error').html('Bạn chưa chọn NVBH. Vui lòng chọn NVBH').show();
						 }else{
							 $('#common-dialog-search-2-textbox #dialog-search-error').html('').hide();
							 var listStaffStr = CRMReportUtils._listStaffCode.valArray[0].staffCode;
							 for(var i = 1; i < CRMReportUtils._listStaffCode.valArray.length; i++) {
								 listStaffStr += ',' + CRMReportUtils._listStaffCode.valArray[i].staffCode;
							 }
							 $('.ModuleList3Form #staffCode').val(listStaffStr);
							 $('#common-dialog-search-2-textbox').dialog('close');
						 }
					 });
					 CRMReportUtils._listStaffCode = new Map();
					 $('#common-dialog-grid-search').datagrid({
						 url : '/commons/nvbh-2/search',
						 autoRowHeight : true,
						 rownumbers : true, 
						 pagination:true,
						 rowNum : 10,
						 pageSize:10,
						 scrollbarSize : 0,
						 pageNumber:1,
						 queryParams:{
							 page:1
						 },
						 onBeforeLoad: function(param) {
							 var code = $('#common-dialog-search-2-textbox #staffCode').val().trim();
							 var name = $('#common-dialog-search-2-textbox #staffName').val().trim();
							 if(isGetShopOnly == undefined || isGetShopOnly == null){
								 param.isGetShopOnly = true;//SangTN: Khong lay shop con
							 }else{
								 param.isGetShopOnly = isGetShopOnly;
							 }
							 param.code = code;
							 param.name = name;
							 param.shopId = shopId;
							 
							 param.lstGSNPPCode = $('#staffOwnerCode').val();
							 param.lstTBHVCode = $('#staffTBHV').val();
							 
							 param = $.param(param, true);
						 },
						 onCheck : function (index,row) {
							 CRMReportUtils._listStaffCode.put(row.id, row);
						 },
						 onUncheck : function (index,row) { 
							 CRMReportUtils._listStaffCode.remove(row.id);
						 },
						 onCheckAll : function(rows) {
							 for(var i = 0; i < rows.length; i++) {
								 var row = rows[i];
								 CRMReportUtils._listStaffCode.put(row.id, row);
							 }
						 },
						 onUncheckAll : function(rows) {
							 for(var i = 0; i < rows.length; i++) {
								 var row = rows[i];
								 CRMReportUtils._listStaffCode.remove(row.id);
							 }
						 },
						 fitColumns:true,
						 width : ($('#common-dialog-search-2-textbox').width() - 40),
						 columns:[[
						 	{field: 'staffCode', title: 'Mã NVBH', align:'left', width: 40, sortable : false, resizable : false},
						 	{field: 'staffName', title: 'Tên NVBH', align:'left', width: 40, sortable : false, resizable : false},
						 	{field : 'choose', checkbox : true, align: 'center', width: 20, sortable : false, resizable : false}
						 ]],
						 onLoadSuccess :function(data){							 
							 if(($.isArray(data) && data.length != 0) || (data.rows != null && data.rows != undefined && $.isArray(data.rows) && data.rows.length != 0)) {
								 var __keys = (data.rows == null || data.rows == undefined) ? Object.keys(data[0]) : Object.keys(data.rows[0]);
								 var rows = (data.rows == null || data.rows == undefined) ? data : data.rows;
								 var isCheckAll=true;
								 for(var i = 0; i < rows.length; i++) {
									 if(CRMReportUtils._listStaffCode.get(rows[i].id)) {
										 $('#common-dialog-grid-search').datagrid('checkRow', i);
									 } else {
										 isCheckAll = false;
									 }
								 }
								 if(isCheckAll) {
									 $('#common-dialog-search-2-textbox .datagrid-header-row input[type=checkbox]').attr('checked', 'checked');
								 }
							 }
							 $('#common-dialog-search-2-textbox .datagrid-header-rownumber').html('STT');
							 var dHeight = $('#common-dialog-search-2-textbox').height();
							 var wHeight = $(window).height();
							 var top = (wHeight - dHeight) / 2 > 180 ? (wHeight - dHeight) / 2 - 180 : (wHeight - dHeight) / 2;
							 $('#common-dialog-search-2-textbox').dialog('move', {top : top});
						 }
					 });
				 },
				 onClose: function() {
					 CRMReportUtils._listStaffCode = null;
					 $('#common-dialog-search-2-textbox').remove(); 
				 }
			});
		},
		showDialogSearchGSNPP : function(shopId){
			var html = '<div id="common-dialog-search-2-textbox" class="easyui-dialog">';
			html += '<div class="PopupContentMid">';
			html += '<div class="GeneralForm Search1Form" >';
			html += '<label class="LabelStyle Label1Style" style=" width: 100px;">Mã NVBH</label>';
			html += '<input id="staffCode" maxlength="50" type="text" style="width: 145px;" class="InputTextStyle InputText1Style"/>';
			html += '<label class="LabelStyle Label1Style" style=" width: 100px;">Tên NVBH</label>';
			html += '<input id="staffName" maxlength="250" type="text" style="width: 145px;" class="InputTextStyle InputText1Style"/>';
			html += '<div class="Clear"></div>'; 
			html += '<div class="BtnCenterSection">';
			html += '<button class="BtnGeneralStyle BtnGeneralMStyle" tabindex="3" id="common-dialog-button-search">Tìm kiếm</button>';
			html += '</div>';
			html += '<div class="Clear"></div>';
			html += '<div class="GridSection" id="common-dialog-grid-container">';
			html += '<table id="common-dialog-grid-search"></table>';
			html += '</div>';
			html += '<div class="BtnCenterSection">';
			html += '<button id ="common-dialog-button-choose" class="BtnGeneralStyle BtnGeneralMStyle">Chọn</button>';
			html += '<button class="BtnGeneralStyle BtnGeneralMStyle" onclick="$(\'#common-dialog-search-2-textbox\').dialog(\'close\');">Đóng</button>';
			html += '</div>';
			html += '<p id="dialog-search-error" style="display: none;" class="ErrorMsgStyle"></p>';
			html += '</div>';
			html += '</div>';
			html += '</div>';
			//style="display: none;"
			$('body').append(html);
			
			$('#common-dialog-search-2-textbox').dialog({
				 title: 'CHỌN GIÁM SÁT NHÀ PHÂN PHỐI',
				 width: 580, 
				 height: 'auto',
				 closed: false,
				 cache: false,
				 modal: true,
				 onOpen: function() {
					 $('#common-dialog-search-2-textbox #common-dialog-button-search').unbind('click');
					 $('#common-dialog-search-2-textbox #common-dialog-button-search').bind('click', function() {
						 $('#common-dialog-search-2-textbox #dialog-search-error').html('').hide();
						 $('#common-dialog-grid-search').datagrid('reload');
					 });
					 $('#common-dialog-search-2-textbox input').unbind('keyup');
					 $('#common-dialog-search-2-textbox input').bind('keyup', function(e) {	
						 if(e.keyCode == keyCodes.ENTER) {
							 $('#common-dialog-search-2-textbox #common-dialog-button-search').click();
						 }
					 });
					 $('#common-dialog-search-2-textbox #common-dialog-button-choose').bind('click', function() {
						 if(CRMReportUtils._listStaffOwnerCode.size() == 0) {
							 $('#common-dialog-search-2-textbox #dialog-search-error').html('Bạn chưa chọn GSNPP. Vui lòng chọn GSNPP').show();
						 }else{
							 $('#common-dialog-search-2-textbox #dialog-search-error').html('').hide();
							 var listStaffStr = CRMReportUtils._listStaffOwnerCode.valArray[0].staffCode;
							 for(var i = 1; i < CRMReportUtils._listStaffOwnerCode.valArray.length; i++) {
								 listStaffStr += ';' + CRMReportUtils._listStaffOwnerCode.valArray[i].staffCode;
							 }
							 $('.ModuleList3Form #staffOwnerCode').val(listStaffStr);
							 $('.ModuleList3Form #staffCode').val("");
							 $('#common-dialog-search-2-textbox').dialog('close');
						 }
					 });
					 CRMReportUtils._listStaffOwnerCode = new Map();
					 $('#common-dialog-grid-search').datagrid({
						 url : '/commons/nvgs/search',
						 autoRowHeight : true,
						 rownumbers : true, 
						 pagination:true,
						 rowNum : 10,
						 pageSize:10,
						 scrollbarSize : 0,
						 pageNumber:1,
						 queryParams:{
							 page:1
						 },
						 onBeforeLoad: function(param) {
							 var code = $('#common-dialog-search-2-textbox #staffCode').val().trim();
							 var name = $('#common-dialog-search-2-textbox #staffName').val().trim();

							 param.code = code;
							 param.name = name;
							 param.shopId = shopId;
							 param.lstTBHVCode = $('#staffTBHV').val();
							 param = $.param(param, true);
						 },
						 onCheck : function (index,row) {
							 CRMReportUtils._listStaffOwnerCode.put(row.id, row);
						 },
						 onUncheck : function (index,row) { 
							 CRMReportUtils._listStaffOwnerCode.remove(row.id);
						 },
						 onCheckAll : function(rows) {
							 for(var i = 0; i < rows.length; i++) {
								 var row = rows[i];
								 CRMReportUtils._listStaffOwnerCode.put(row.id, row);
							 }
						 },
						 onUncheckAll : function(rows) {
							 for(var i = 0; i < rows.length; i++) {
								 var row = rows[i];
								 CRMReportUtils._listStaffOwnerCode.remove(row.id);
							 }
						 },
						 fitColumns:true,
						 width : ($('#common-dialog-search-2-textbox').width() - 40),
						 columns:[[
						 	{field: 'staffCode', title: 'Mã NVBH', align:'left', width: 40, sortable : false, resizable : false},
						 	{field: 'staffName', title: 'Tên NVBH', align:'left', width: 40, sortable : false, resizable : false},
						 	{field : 'choose', checkbox : true, align: 'center', width: 20, sortable : false, resizable : false}
						 ]],
						 onLoadSuccess :function(data){							 
							 if(($.isArray(data) && data.length != 0) || (data.rows != null && data.rows != undefined && $.isArray(data.rows) && data.rows.length != 0)) {
								 var __keys = (data.rows == null || data.rows == undefined) ? Object.keys(data[0]) : Object.keys(data.rows[0]);
								 var rows = (data.rows == null || data.rows == undefined) ? data : data.rows;
								 var isCheckAll=true;
								 for(var i = 0; i < rows.length; i++) {
									 if(CRMReportUtils._listStaffOwnerCode.get(rows[i].id)) {
										 $('#common-dialog-grid-search').datagrid('checkRow', i);
									 } else {
										 isCheckAll = false;
									 }
								 }
								 if(isCheckAll) {
									 $('#common-dialog-search-2-textbox .datagrid-header-row input[type=checkbox]').attr('checked', 'checked');
								 }
							 }
							 $('#common-dialog-search-2-textbox .datagrid-header-rownumber').html('STT');
							 var dHeight = $('#common-dialog-search-2-textbox').height();
							 var wHeight = $(window).height();
							 var top = (wHeight - dHeight) / 2 > 180 ? (wHeight - dHeight) / 2 - 180 : (wHeight - dHeight) / 2;
							 $('#common-dialog-search-2-textbox').dialog('move', {top : top});
						 }
					 });
				 },
				 onClose: function() {
					 CRMReportUtils._listStaffOwnerCode = null;
					 $('#common-dialog-search-2-textbox').remove(); 
				 }
			});
		},
		showDialogSearchTBHV : function(shopId){
			var html = '<div id="common-dialog-search-2-textbox" class="easyui-dialog">';
			html += '<div class="PopupContentMid">';
			html += '<div class="GeneralForm Search1Form" >';
			html += '<label class="LabelStyle Label1Style" style=" width: 100px;">Mã NVBH</label>';
			html += '<input id="staffCode" maxlength="50" type="text" style="width: 145px;" class="InputTextStyle InputText1Style"/>';
			html += '<label class="LabelStyle Label1Style" style=" width: 100px;">Tên NVBH</label>';
			html += '<input id="staffName" maxlength="250" type="text" style="width: 145px;" class="InputTextStyle InputText1Style"/>';
			html += '<div class="Clear"></div>'; 
			html += '<div class="BtnCenterSection">';
			html += '<button class="BtnGeneralStyle BtnGeneralMStyle" tabindex="3" id="common-dialog-button-search">Tìm kiếm</button>';
			html += '</div>';
			html += '<div class="Clear"></div>';
			html += '<div class="GridSection" id="common-dialog-grid-container">';
			html += '<table id="common-dialog-grid-search"></table>';
			html += '</div>';
			html += '<div class="BtnCenterSection">';
			html += '<button id ="common-dialog-button-choose" class="BtnGeneralStyle BtnGeneralMStyle">Chọn</button>';
			html += '<button class="BtnGeneralStyle BtnGeneralMStyle" onclick="$(\'#common-dialog-search-2-textbox\').dialog(\'close\');">Đóng</button>';
			html += '</div>';
			html += '<p id="dialog-search-error" style="display: none;" class="ErrorMsgStyle"></p>';
			html += '</div>';
			html += '</div>';
			html += '</div>';
			//style="display: none;"
			$('body').append(html);
			
			$('#common-dialog-search-2-textbox').dialog({
				 title: 'CHỌN TRƯỞNG BÁN HÀNG VÙNG',
				 width: 580, 
				 height: 'auto',
				 closed: false,
				 cache: false,
				 modal: true,
				 onOpen: function() {
					 $('#common-dialog-search-2-textbox #common-dialog-button-search').unbind('click');
					 $('#common-dialog-search-2-textbox #common-dialog-button-search').bind('click', function() {
						 $('#common-dialog-search-2-textbox #dialog-search-error').html('').hide();
						 $('#common-dialog-grid-search').datagrid('reload');
					 });
					 $('#common-dialog-search-2-textbox input').unbind('keyup');
					 $('#common-dialog-search-2-textbox input').bind('keyup', function(e) {	
						 if(e.keyCode == keyCodes.ENTER) {
							 $('#common-dialog-search-2-textbox #common-dialog-button-search').click();
						 }
					 });
					 $('#common-dialog-search-2-textbox #common-dialog-button-choose').bind('click', function() {
						 if(CRMReportUtils._listStaffTBHV.size() == 0) {
							 $('#common-dialog-search-2-textbox #dialog-search-error').html('Bạn chưa chọn TBHV. Vui lòng chọn TBHV').show();
						 }else{
							 $('#common-dialog-search-2-textbox #dialog-search-error').html('').hide();
							 var listStaffStr = CRMReportUtils._listStaffTBHV.valArray[0].staffCode;
							 for(var i = 1; i < CRMReportUtils._listStaffTBHV.valArray.length; i++) {
								 listStaffStr += ';' + CRMReportUtils._listStaffTBHV.valArray[i].staffCode;
							 }
							 $('.ModuleList3Form #staffTBHV').val(listStaffStr);
							 $('.ModuleList3Form #staffOwnerCode').val("");
							 $('.ModuleList3Form #staffCode').val("");
							 $('#common-dialog-search-2-textbox').dialog('close');
						 }
					 });
					 CRMReportUtils._listStaffTBHV = new Map();
					 $('#common-dialog-grid-search').datagrid({
						 url : '/commons/tbhv/search',
						 autoRowHeight : true,
						 rownumbers : true, 
						 pagination:true,
						 rowNum : 10,
						 pageSize:10,
						 scrollbarSize : 0,
						 pageNumber:1,
						 queryParams:{
							 page:1
						 },
						 onBeforeLoad: function(param) {
							 var code = $('#common-dialog-search-2-textbox #staffCode').val().trim();
							 var name = $('#common-dialog-search-2-textbox #staffName').val().trim();

							 param.code = code;
							 param.name = name;
							 param.shopId = shopId;
							 param = $.param(param, true);
						 },
						 onCheck : function (index,row) {
							 CRMReportUtils._listStaffTBHV.put(row.id, row);
						 },
						 onUncheck : function (index,row) { 
							 CRMReportUtils._listStaffTBHV.remove(row.id);
						 },
						 onCheckAll : function(rows) {
							 for(var i = 0; i < rows.length; i++) {
								 var row = rows[i];
								 CRMReportUtils._listStaffTBHV.put(row.id, row);
							 }
						 },
						 onUncheckAll : function(rows) {
							 for(var i = 0; i < rows.length; i++) {
								 var row = rows[i];
								 CRMReportUtils._listStaffTBHV.remove(row.id);
							 }
						 },
						 fitColumns:true,
						 width : ($('#common-dialog-search-2-textbox').width() - 40),
						 columns:[[
						 	{field: 'staffCode', title: 'Mã NVBH', align:'left', width: 40, sortable : false, resizable : false},
						 	{field: 'staffName', title: 'Tên NVBH', align:'left', width: 40, sortable : false, resizable : false},
						 	{field : 'choose', checkbox : true, align: 'center', width: 20, sortable : false, resizable : false}
						 ]],
						 onLoadSuccess :function(data){							 
							 if(($.isArray(data) && data.length != 0) || (data.rows != null && data.rows != undefined && $.isArray(data.rows) && data.rows.length != 0)) {
								 var __keys = (data.rows == null || data.rows == undefined) ? Object.keys(data[0]) : Object.keys(data.rows[0]);
								 var rows = (data.rows == null || data.rows == undefined) ? data : data.rows;
								 var isCheckAll=true;
								 for(var i = 0; i < rows.length; i++) {
									 if(CRMReportUtils._listStaffTBHV.get(rows[i].id)) {
										 $('#common-dialog-grid-search').datagrid('checkRow', i);
									 } else {
										 isCheckAll = false;
									 }
								 }
								 if(isCheckAll) {
									 $('#common-dialog-search-2-textbox .datagrid-header-row input[type=checkbox]').attr('checked', 'checked');
								 }
							 }
							 $('#common-dialog-search-2-textbox .datagrid-header-rownumber').html('STT');
							 var dHeight = $('#common-dialog-search-2-textbox').height();
							 var wHeight = $(window).height();
							 var top = (wHeight - dHeight) / 2 > 180 ? (wHeight - dHeight) / 2 - 180 : (wHeight - dHeight) / 2;
							 $('#common-dialog-search-2-textbox').dialog('move', {top : top});
						 }
					 });
				 },
				 onClose: function() {
					 CRMReportUtils._listStaffTBHV = null;
					 $('#common-dialog-search-2-textbox').remove(); 
				 }
			});
		},
		loadComboTree: function(controlId, storeCode,defaultValue,callback,params,vungType){		 
			var _url= '/rest/report/shop/combotree/1/0.json';
			if(vungType!=undefined && vungType!=null){
				_url = '/rest/report/vung/combotree/1/0.json';	
			}
			$('#' + controlId).combotree({url: _url,
				formatter: function(node) {
					return Utils.XSSEncode(node.text);
				},
				onChange: function(newValue,oldValue){				
					if(params != null && params.length >0){
						for(var i=0;i<params.length;i++){
							$('#'+params[i]).val('');
						}
					}
					if(DebitPayReport._lstStaff != null && DebitPayReport._lstStaff.size() >0){
						DebitPayReport._lstStaff = new Map();
					}
				}
			});
			var t = $('#'+ controlId).combotree('tree');	
			t.tree('options').url = '';
			t.tree({			
				formatter: function(node) {
					return Utils.XSSEncode(node.text);
				},
				onSelect:function(node){	
					if(callback!=undefined && callback!=null){//tungmt
						callback.call(this, node.id);
					}
					$('#'+storeCode).val(node.id);
					$.getJSON("/report/are-you-shop?shopId="+node.id,function(result){
						$('#isNPP').val(result.isNPP);
					});
					$('.ModuleList3Form #staffTBHV').val("");
					 $('.ModuleList3Form #staffOwnerCode').val("");
					 $('.ModuleList3Form #staffCode').val("");
				},	            				
				onBeforeExpand:function(result){	            					
					var nodes = t.tree('getChildren',result.target);
					for(var i=0;i<nodes.length;++i){
						t.tree('remove',nodes[i].target);
					}
					var ___jaxurl = '/rest/report/shop/combotree/2/';
					if(vungType!=undefined && vungType!=null){
						___jaxurl = '/rest/report/vung/combotree/2/';	
					}
					$.ajax({
						type : "POST",
						url : ___jaxurl + result.id +'.json',					
						dataType : "json",
						success : function(r) {						
							$(r).each(function(i,e){
								e.text = Utils.XSSEncode(e.text);
							});
							t.tree('append',{
								parent:result.target,
								data:r
							});
							$('.panel-body-noheader ul').css('display','block');
						}
					});
				},
				onExpand: function(result){	
					$('.panel-body-noheader ul').css('display','block');
				}
				});
			if(defaultValue == undefined || defaultValue == null){
				$('#' + controlId).combotree('setValue',activeType.ALL);
			} else {
				$('#' + controlId).combotree('setValue',defaultValue);
			}
		}
};
var PoReport = {	
	reportCustomerService: function(){
		var msg = '';
		$('#errMsg').html('').hide();
		if($('#shopCode').val().trim().length == 0 || $('#shopCode').val().trim() == activeType.ALL){
			msg = format(msgErr_required_field,'đơn vị');
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfInvalidFormatDate('fDate','Từ ngày');
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfInvalidFormatDate('tDate','Đến ngày');
		}
		var fDate = $('#fDate').val();
		var tDate = $('#tDate').val();
		if(!Utils.compareDate(fDate, tDate)){
			msg = msgErr_fromdate_greater_todate;		
			$('#fDate').focus();
		}
		if(msg.length > 0){
			$('#errMsg').html(msg).show();
			return false;
		}
		var dataModel = new Object();		
		dataModel.shopCode = $('#shopCode').val().trim();
		dataModel.fromDate = $('#fDate').val().trim();
		dataModel.toDate = $('#tDate').val().trim();
		ReportUtils.exportReport('/report/po/manage-customer-service/export',dataModel);
	},
	exportDCPOCTDVKH_5_5 : function(){
		var msg = '';
		$('#errMsg').html('').hide();
		if($('#shopId').val().trim().length == 0 || $('#shopId').val().trim() == activeType.ALL){
			msg = format(msgErr_required_field,'đơn vị');
		}
		if(msg.length == 0){
			if($('#isNPP').val() == "false"){
				msg = "Đơn vị bạn vừa chọn không phải là 1 nhà phân phối cụ thể!";
			}
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfInvalidFormatDate('fDate','Từ ngày');
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfInvalidFormatDate('tDate','Đến ngày');
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfEmptyDate('fDate','Từ ngày');
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfEmptyDate('tDate','Đến ngày');
		}
		if(msg.length > 0){
			$('#errMsg').html(msg).show();
			return false;
		}
		var fDate = $('#fDate').val();
		var tDate = $('#tDate').val();
		var cDate = ReportUtils.getCurrentDateString();
		if(!Utils.compareDate(fDate, cDate)){
			msg = msgErr_fromdate_greater_currentdate;		
			$('#fDate').focus();
		}
		if(!Utils.compareDate(fDate, tDate)){
			msg = msgErr_fromdate_greater_todate;		
			$('#fDate').focus();
		}
		if(msg.length > 0){
			$('#errMsg').html(msg).show();
			return false;
		}
		var dataModel = new Object();		
		dataModel.shopId = $('#shopId').val().trim();
		dataModel.fromDate = $('#fDate').val().trim();
		dataModel.toDate = $('#tDate').val().trim();
		ReportUtils.exportReport('/report/po/dcpoctdvkh/export',dataModel);
	},
	reportBTDTTDH: function() {
		var msg = '';
		$('#errMsg').html('').hide();
		if($('#shopId').val().trim().length == 0 || $('#shopId').val().trim() == activeType.ALL){
			msg = format(msgErr_required_field,'đơn vị');
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfInvalidFormatDate('fDate','Từ ngày');
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfInvalidFormatDate('tDate','Đến ngày');
		}
		var fDate = $('#fDate').val();
		var tDate = $('#tDate').val();
		if(msg.length == 0 && !Utils.compareCurrentDate(fDate)) {
			msg = 'Từ ngày phải nhỏ hơn hoặc bằng ngày hiện tại';
		}
		
		if(msg.length == 0 && !Utils.compareDate(fDate, tDate)) {
			msg = 'Từ ngày phải nhỏ hơn hoặc bằng đến ngày';
		}
		
		if(msg.length == 0 && !Utils.compareDate(fDate, tDate)){
			msg = msgErr_fromdate_greater_todate;		
			$('#fDate').focus();
		}
		if(msg.length > 0){
			$('#errMsg').html(msg).show();
			return false;
		}
		var dataModel = new Object();		
		dataModel.shopId = $('#shopId').val().trim();
		dataModel.fromDate = $('#fDate').val().trim();
		dataModel.toDate = $('#tDate').val().trim();
		ReportUtils.exportReport('/report/po/btdttdh/export',dataModel);
	},
	exportBKCTCTMH: function() {
		var msg = '';
		$('#errMsg').html('').hide();
		if($('#shopId').val().trim().length == 0 || $('#shopId').val().trim() == activeType.ALL){
			msg = format(msgErr_required_field,'đơn vị');
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfInvalidFormatDate('fDate','Từ ngày');
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfInvalidFormatDate('tDate','Đến ngày');
		}
		var fDate = $('#fDate').val();
		var tDate = $('#tDate').val();
		if(msg.length == 0 && !Utils.compareCurrentDate(fDate)) {
			msg = 'Từ ngày phải nhỏ hơn hoặc bằng ngày hiện tại';
		}
		
		if(msg.length == 0 && !Utils.compareDate(fDate, tDate)) {
			msg = 'Từ ngày phải nhỏ hơn hoặc bằng đến ngày';
		}
		
		if(msg.length == 0 && !Utils.compareDate(fDate, tDate)){
			msg = msgErr_fromdate_greater_todate;		
			$('#fDate').focus();
		}
		if(msg.length > 0){
			$('#errMsg').html(msg).show();
			return false;
		}
		var dataModel = new Object();		
		if($('input[name=formatType]:radio:checked').length > 0 && $('input[name=formatType]:radio:checked').val() != undefined) {
            dataModel.formatType = $('input[name=formatType]:radio:checked').val().toUpperCase();
		}
		if ($('input[name=pageFormat]:radio:checked').length > 0 && $('input[name=pageFormat]:radio:checked').val() != undefined) {
			dataModel.pageFormat = $('input[name=pageFormat]:radio:checked').val().toUpperCase();
		}
		dataModel.shopId = $('#shopId').val().trim();
		dataModel.fromDate = $('#fDate').val().trim();
		dataModel.toDate = $('#tDate').val().trim();
		dataModel.type = $('#type').val().trim();
		
		ReportUtils.exportReport('/report/po/bkctctmh/export',dataModel);
		/*var kData = $.param(dataModel,true);
		$('#divOverlay').show();		
		$.ajax({
			type : "POST",
			url : "/report/po/bkctctmh/export",
			data :(kData),
			dataType: "json",
			success : function(data) {
				$('#divOverlay').hide();				
				if(!data.error) {
					if(data.hasData != null && data.hasData != undefined && data.hasData == true) {
						window.location.href = data.path;
						setTimeout(function(){ //Set timeout để đảm bảo file load lên hoàn tất
		                    CommonSearch.deleteFileExcelExport(data.path);
						},2000);
					} else {
						$('#errMsg').html('Không có dữ liệu để xuất').show();
					}
				} else {
					$('#errMsg').html(data.errMsg).show();					
				}
			},
			error:function(XMLHttpRequest, textStatus, errorDivThrown) {
				$('#divOverlay').hide();
				StockIssued.xhrExport = null;
			}
		});
		return false;*/
	},
	//namlb bao cao PO Auto NPP
	exportPOAutoNPP: function() {
		var msg = '';
		$('#errMsg').html('').hide();
		if($('#shopId').val().trim().length == 0 || $('#shopId').val().trim() == activeType.ALL){
			msg = format(msgErr_required_field,'đơn vị');
		}
		if(msg.length == 0){
			if($('#isNPP').val() == "false"){
				msg = "Đơn vị bạn vừa chọn không phải là 1 nhà phân phối cụ thể!";
			}
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfInvalidFormatDate('fDate','Từ ngày');
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfInvalidFormatDate('tDate','Đến ngày');
		}
		var fDate = $('#fDate').val();
		var tDate = $('#tDate').val();
		if(msg.length == 0 && !Utils.compareCurrentDate(fDate)) {
			msg = 'Từ ngày phải nhỏ hơn hoặc bằng ngày hiện tại';
		}
		
		if(msg.length == 0 && !Utils.compareDate(fDate, tDate)) {
			msg = 'Từ ngày phải nhỏ hơn hoặc bằng đến ngày';
		}
		
		if(msg.length == 0 && !Utils.compareDate(fDate, tDate)){
			msg = msgErr_fromdate_greater_todate;		
			$('#fDate').focus();
		}
		if(msg.length > 0){
			$('#errMsg').html(msg).show();
			return false;
		}
		var dataModel = new Object();		
		dataModel.shopId = $('#shopId').val().trim();
		dataModel.fromDate = $('#fDate').val().trim();
		dataModel.toDate = $('#tDate').val().trim();
		//ReportUtils.exportReport('/report/po/bkctctmh/export',dataModel);
		var kData = $.param(dataModel,true);
		$('#divOverlay').show();		
		$.ajax({
			type : "POST",
			url : "/report/po/bcpoautoNPP/export",
			data :(kData),
			dataType: "json",
			success : function(data) {
				$('#divOverlay').hide();				
				if(!data.error) {
					if(data.hasData != null && data.hasData != undefined && data.hasData == true) {
						window.location.href = data.path;
						setTimeout(function(){ //Set timeout để đảm bảo file load lên hoàn tất
		                    CommonSearch.deleteFileExcelExport(data.path);
						},2000);
					} else {
						$('#errMsg').html('Không có dữ liệu để xuất').show();
					}
				} else {
					$('#errMsg').html(data.errMsg).show();					
				}
			},
			error:function(XMLHttpRequest, textStatus, errorDivThrown) {
				$('#divOverlay').hide();
				StockIssued.xhrExport = null;
			}
		});
		return false;
	},
	//phuongvm bao cao phieu dat hang vinamilk 5.1
	exportPDHVNM_5_1: function() {
		var msg = '';
		$('#errMsg').html('').hide();
		if($('#shopId').val().trim().length == 0 || $('#shopId').val().trim() == activeType.ALL){
			msg = format(msgErr_required_field,'đơn vị');
		}
		if(msg.length == 0){
			if($('#isNPP').val() == "false"){
				msg = "Đơn vị bạn vừa chọn không phải là 1 nhà phân phối cụ thể!";
			}
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfInvalidFormatDate('fDate','Từ ngày');
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfInvalidFormatDate('tDate','Đến ngày');
		}
		var fDate = $('#fDate').val();
		var tDate = $('#tDate').val();
		if(msg.length == 0 && !Utils.compareCurrentDate(fDate)) {
			msg = 'Từ ngày phải nhỏ hơn hoặc bằng ngày hiện tại';
		}
		
		if(msg.length == 0 && !Utils.compareDate(fDate, tDate)) {
			msg = 'Từ ngày phải nhỏ hơn hoặc bằng đến ngày';
		}
		
		if(msg.length == 0 && !Utils.compareDate(fDate, tDate)){
			msg = msgErr_fromdate_greater_todate;		
			$('#fDate').focus();
		}
		if(msg.length > 0){
			$('#errMsg').html(msg).show();
			return false;
		}
		var dataModel = new Object();		
		dataModel.shopId = $('#shopId').val().trim();
		dataModel.fromDate = $('#fDate').val().trim();
		dataModel.toDate = $('#tDate').val().trim();
		if($('input[name=formatType]:radio:checked').length > 0 && $('input[name=formatType]:radio:checked').val() != undefined) {
            dataModel.formatType = $('input[name=formatType]:radio:checked').val().toUpperCase();
		}

		ReportUtils.exportReport('/report/po/pdhvnm/export',dataModel);
	},
	//vuonghn xuat ban ke chung tu hang hoa mua vao
	exportBKCTHHMV: function() {
		var msg = '';
		$('#errMsg').html('').hide();
		if($('#shopId').val().trim().length == 0 || $('#shopId').val().trim() == activeType.ALL){
			msg = format(msgErr_required_field,'đơn vị');
		}
		if(msg.length == 0){
			if($('#isNPP').val() == "false"){
				msg = "Đơn vị bạn vừa chọn không phải là 1 nhà phân phối cụ thể!";
			}
		}
		if (msg.length == 0) {
			msg = Utils.getMessageOfRequireCheck('fDate', 'Từ ngày');
		}
		if (msg.length == 0) {
			msg = Utils.getMessageOfRequireCheck('tDate', 'Đến ngày');
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfInvalidFormatDate('fDate','Từ ngày');
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfInvalidFormatDate('tDate','Đến ngày');
		}
		var fDate = $('#fDate').val();
		var tDate = $('#tDate').val();
		if(msg.length == 0 && !Utils.compareCurrentDate(fDate)) {
			msg = 'Từ ngày phải nhỏ hơn hoặc bằng ngày hiện tại';
		}
		
		if(msg.length == 0 && !Utils.compareDate(fDate, tDate)) {
			msg = 'Từ ngày phải nhỏ hơn hoặc bằng đến ngày';
		}
		
		if(msg.length == 0 && !Utils.compareDate(fDate, tDate)){
			msg = msgErr_fromdate_greater_todate;		
			$('#fDate').focus();
		}
		if(msg.length > 0){
			$('#errMsg').html(msg).show();
			return false;
		}
		var dataModel = new Object();		
		if($('input[name=formatType]:radio:checked').length > 0 && $('input[name=formatType]:radio:checked').val() != undefined) {
            dataModel.formatType = $('input[name=formatType]:radio:checked').val().toUpperCase();
		}
		dataModel.shopId = $('#shopId').val().trim();
		dataModel.fromDate = $('#fDate').val().trim();
		dataModel.toDate = $('#tDate').val().trim();
		ReportUtils.exportReport('/report/po/bkcthhmv/export',dataModel);
		/*var kData = $.param(dataModel,true);
		$('#divOverlay').show();		
		$.ajax({
			type : "POST",
			url : "/report/po/bkcthhmv/export",
			data :(kData),
			dataType: "json",
			success : function(data) {
				$('#divOverlay').hide();				
				if(!data.error) {
					if(data.hasData != null && data.hasData != undefined && data.hasData == true) {
						window.location.href = data.path;
						setTimeout(function(){ //Set timeout để đảm bảo file load lên hoàn tất
		                    CommonSearch.deleteFileExcelExport(data.path);
						},2000);
					} else {
						$('#errMsg').html('Không có dữ liệu để xuất').show();
					}
				} else {
					$('#errMsg').html(data.errMsg).show();					
				}
			},
			error:function(XMLHttpRequest, textStatus, errorDivThrown) {
				$('#divOverlay').hide();
				StockIssued.xhrExport = null;
			}
		});*/
		return false;
	},
	
	exportTDMH9: function() {
		var msg = '';
		$('#errMsg').hide();
		msg = Utils.getMessageOfRequireCheck('shopId', 'Đơn vị');
		$('#shop').focus();
		
		if (msg.length == 0) {
			msg = Utils.getMessageOfRequireCheck('fDate', 'Từ ngày');
		}
		if (msg.length == 0) {
			msg = Utils.getMessageOfRequireCheck('tDate', 'Đến ngày');
		}
		if (msg.length == 0) {
			msg = Utils.getMessageOfInvalidFormatDate('fDate', 'Từ ngày');
		}
		if (msg.length == 0) {
			msg = Utils.getMessageOfInvalidFormatDate('tDate', 'Từ ngày');
		}
		var fDate = $('#fDate').val().trim();
		var tDate = $('#tDate').val().trim();
		if(msg.length == 0 && !Utils.compareCurrentDate(fDate)) {
			msg = 'Từ ngày phải nhỏ hơn hoặc bằng ngày hiện tại';
			$('#fDate').focus();
		}
		
		if(msg.length == 0 && !Utils.compareDate(fDate, tDate)) {
			msg = 'Từ ngày phải nhỏ hơn hoặc bằng đến ngày';
			$('#fDate').focus();
		}
		
		if (msg.length > 0) {
			$('#errMsg').html(msg).show();
			return false;
		}
		
		var dataModel = new Object();
		dataModel.shopId = $('#shopId').val().trim();
		dataModel.fromDate = fDate;
		dataModel.toDate = tDate;
		if($('input[name=formatType]:radio:checked').length > 0 && $('input[name=formatType]:radio:checked').val() != undefined) {
			dataModel.formatType = $('input[name=formatType]:radio:checked').val().toUpperCase();
		}
		ReportUtils.exportReport('/report/po/tdmh_7_3_9/export',dataModel);
		return false;
	}
};
//bang ke mua hang theo mat hang
var productReport = {	
		productBuyReport: function(){
		var msg = '';
		$('#errMsg').html('').hide();
		if($('#shopCode').val().trim().length == 0 || $('#shopCode').val().trim() == activeType.ALL){
			msg = format(msgErr_required_field,'đơn vị');
		}
		if(msg.length == 0){
			if($('#isNPP').val() == "false"){
				msg = "Đơn vị bạn vừa chọn không phải là 1 nhà phân phối cụ thể!";
			}
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfInvalidFormatDate('fDate','Từ ngày');
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfInvalidFormatDate('tDate','Đến ngày');
		}
		var fDate = $('#fDate').val();
		var tDate = $('#tDate').val();
		if(!Utils.compareDate(fDate, tDate)){
			msg = msgErr_fromdate_greater_todate;		
			$('#fDate').focus();
		}
		if(msg.length > 0){
			$('#errMsg').html(msg).show();
			return false;
		}
		var dataModel = new Object();		
		dataModel.shopCode = $('#shopCode').val().trim();
		dataModel.fromDate = $('#fDate').val().trim();
		dataModel.toDate = $('#tDate').val().trim();
		ReportUtils.exportReport('/report/productbuy/product-buy/export',dataModel);
	}
};

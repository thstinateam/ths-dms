var CustomerReport = {	
	exportDSKHTTBH : function(){
		var msg = '';
		$('#errMsg').html('').hide();
		if($('#shopId').val().trim().length == 0 || $('#shopId').val().trim() == activeType.ALL){
			msg = format(msgErr_required_field,'Đơn vị');
		}
		
		if(msg.length == 0){
			if($('#isNPP').val() == "false" || $('#isNPP').val()=='0'){
				msg = "Đơn vị bạn vừa chọn không phải là 1 nhà phân phối cụ thể!";
			}
		}
		
		if(msg.length > 0){
			$('#errMsg').html(msg).show();
			return false;
		}
		var dataModel = new Object();
		if($('input[name=formatType]:radio:checked').length > 0 && $('input[name=formatType]:radio:checked').val() != undefined) {
            dataModel.formatType = $('input[name=formatType]:radio:checked').val().toUpperCase();
		}
		dataModel.shopId = $('#shopId').val().trim();
		dataModel.saleStaffCode = $('#saleStaffCode').val().trim();
		dataModel.customerCode = $('#customerCode').val().trim();
		dataModel.customerName = $('#customerName').val().trim();
		ReportUtils.exportReport('/report/customer/dskhttbh/export',dataModel,'errMsg');
	},
	exportDSKH: function(){
		var msg = '';
		$('#errMsg').html('').hide();
		if($('#shopId').val().trim().length == 0 || $('#shopId').val().trim() == activeType.ALL){
			msg = format(msgErr_required_field,'Đơn vị');
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfRequireCheck('kindReport','Loại báo cáo',true);
			//Truyen chu "true" vao vi minh doc code thay: getMessageOfRequireCheck: function(objectId, objectName,isSelectBox)
			//isSelectBox la bien boolean phan biet selecBox & textField.
		}
//		if(msg.length == 0){
//			msg = Utils.getMessageOfRequireCheck('shopCode','Mã đơn vị');
//		}
//		if(msg.length == 0){
//			msg = Utils.getMessageOfInvalidFormatDate('saleStaffCode','Mã NVBH');
//		}
//		if(msg.length == 0){
//			msg = Utils.getMessageOfRequireCheck('customerCode','Mã KH');
//		}
//		if(msg.length == 0){
//			msg = Utils.getMessageOfInvalidFormatDate('customerName','Tên KH');
//		}
		if(msg.length > 0){
			$('#errMsg').html(msg).show();
			return false;
		}
		var dataModel = new Object();
		if($('input[name=formatType]:radio:checked').length > 0 && $('input[name=formatType]:radio:checked').val() != undefined) {
            dataModel.formatType = $('input[name=formatType]:radio:checked').val().toUpperCase();
		}
		dataModel.typeReport = $('#kindReport').val();
		dataModel.status = $('#status').val();
//		dataModel.shopCode = $('#shopCode').val().trim();
		dataModel.shopId = $('#shopId').val().trim();
		dataModel.saleStaffCode = $('#saleStaffCode').val().trim();
		dataModel.customerCode = $('#customerCode').val().trim();
		dataModel.customerName = $('#customerName').val().trim();
		ReportUtils.exportReport('/report/customer/dskh/export', dataModel);
		/*var kData = $.param(dataModel,true);
		$('#divOverlay').show();		
		$.ajax({
			type : "POST",
			url : "/report/customer/dskh/export",
			data :(kData),
			dataType: "json",
			success : function(data) {
				$('#divOverlay').hide();				
				if(!data.error) {
					window.location.href = data.path;
					setTimeout(function(){ //Set timeout để đảm bảo file load lên hoàn tất
	                    CommonSearch.deleteFileExcelExport(data.path);
					},2000);
				} else {
					$('#errMsg').html(data.errMsg).show();					
				}
			},
			error:function(XMLHttpRequest, textStatus, errorDivThrown) {
				$('#divOverlay').hide();
				StockIssued.xhrExport = null;
			}
		});*/
		return false;
	},
	exportKD5: function(){
		var fTmp = '';
		var tTmp = '';
		var msg = '';
		$('#errMsg').html('').hide();
		var fTitle = 'Từ ngày';
		var tTitle = 'Đến ngày';
		if($('#type').val().trim() == 17){
			fTitle = 'Từ tháng';
			tTitle = 'Đến tháng';
			if(msg.length == 0){
				msg = Utils.getMessageOfRequireCheck('fDate',fTitle);
			}
			if(msg.length == 0){
				msg = Utils.getMessageOfRequireCheck('tDate',tTitle);
			}
			fTmp = $('#fDate').val().trim();
			tTmp = $('#tDate').val().trim();
			$('#fDate').val("01/"+fTmp);
			$('#tDate').val("01/"+tTmp);
			var fDate = $('#fDate').val();
			var tDate = $('#tDate').val();
			if(!Utils.compareDate(fDate, tDate)){
				msg = fTitle +" không được lớn hơn "+ tTitle;		
				$('#fDate').focus();
			}
			$('#fDate').val(fTmp);
			$('#tDate').val(tTmp);
		}else{
			if(msg.length == 0){
				msg = Utils.getMessageOfRequireCheck('fDate',fTitle);
			}
			if(msg.length == 0){
				msg = Utils.getMessageOfInvalidFormatDate('fDate',fTitle);
			}
			var fDate = $('#fDate').val();
			var tDate = $('#tDate').val();
			if(!Utils.compareDate(fDate, tDate)){
				msg = msgErr_fromdate_greater_todate;		
				$('#fDate').focus();
			}
			var currentTime = new Date();
			var month = currentTime.getMonth() + 1;
			var day = currentTime.getDate();
			var year = currentTime.getFullYear();
			if(!Utils.compareDate(fDate,day + '/' + month + '/' + year)){
				msg = 'Từ ngày không được lớn hơn ngày hiện tại';
				$('#fDate').focus();
			}
			if(msg.length == 0){
				msg = Utils.getMessageOfRequireCheck('tDate',tTitle);
			}
			if(msg.length == 0){
				msg = Utils.getMessageOfInvalidFormatDate('tDate',tTitle);
			}
			if(msg.length == 0 && !Utils.compareCurrentDate(tDate)) {
				msg = 'Đến ngày phải nhỏ hơn ngày hiện tại. Vui lòng nhập lại';
				$('#tDate').focus();
			}
		}
		if(msg.length > 0){
			$('#errMsg').html(msg).show();
			return false;
		}
		var dataModel = new Object();	
		dataModel.fromDate = $('#fDate').val().trim();
		dataModel.toDate = $('#tDate').val().trim();
		if($('#type').val().trim() == 17){
			dataModel.fromDate = "01/"+$('#fDate').val().trim();
			dataModel.toDate = "01/"+$('#tDate').val().trim();
		}
		dataModel.type = $('#type').val().trim();
		$('#typeReport').val($('#type').val().trim());
		ReportUtils.previewReport('/report/crm/kd5/export',dataModel);

	},
//end-me	
	
	export7_5_1TH: function() {
		$('#errMsg').hide();
		var msg = '';
		msg = Utils.getMessageOfRequireCheck('shopId', 'Đơn vị');
		$('#shop').focus();
		
		if (msg.length == 0) {
			msg = Utils.getMessageOfRequireCheck('rptType', 'Loại báo cáo', true);
		}
		
		if (msg.length > 0) {
			$('#errMsg').html(msg).show();
			return;
		}
		var dataModel = new Object();		
		dataModel.shopId = $('#shopId').val().trim();
		if ($('#rptType').val().trim() == 'TH') {
			ReportUtils.exportReport('/report/customer/tnpt-th/export', dataModel);
		} else {
			ReportUtils.exportReport('/report/customer/tnpt-ct/export', dataModel);
		}
	},
	
	export7_5_1CT: function() {
		$('#errMsg').hide();
		var dataModel = new Object();
		ReportUtils.exportReport('/report/customer/tnpt-ct/export', dataModel);
	}
};
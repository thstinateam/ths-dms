var DebitRetrieveReport = {	
		
   /**
	* Bc exportCNPTCT3_2
	* 
	* @author thangnv31
	* @since Dec 22, 2015
	* */	
	exportCNPTCT3_2: function() {
		var msg = '';
		$('#errMsg').html('').hide();
		if ($('#shopId').val().trim().length == 0 || $('#shopId').val().trim() == activeType.ALL) {
			msg = format(msgErr_required_field,'đơn vị');
		}
		if (msg.length == 0) {
			if ($('#isNPP').val() == "false") {
				msg="Đơn vị bạn vừa chọn không phải là nhà phân phối cụ thể!";
			}
		}
		if (msg.length == 0) {
			msg = Utils.getMessageOfRequireCheck('fDate','Từ ngày');
		}
		if (msg.length == 0) {
			msg = Utils.getMessageOfInvalidFormatDate('fDate','Từ ngày');
		}
		if (msg.length == 0) {
			msg = Utils.getMessageOfRequireCheck('tDate','Từ ngày');
		}
		if (msg.length == 0) {
			msg = Utils.getMessageOfInvalidFormatDate('tDate','Đến ngày');
		}
		var fDate = $('#fDate').val();
		var tDate = $('#tDate').val();
		if (!Utils.compareDate(fDate, tDate)) {
			msg = msgErr_fromdate_greater_todate;		
			$('#fDate').focus();
		}
		var currentTime = new Date();
		var month = currentTime.getMonth() + 1;
		var day = currentTime.getDate();
		var year = currentTime.getFullYear();
		if (!Utils.compareDate(fDate, day + '/' + month + '/' + year)) {
			msg = 'Từ ngày không được lớn hơn ngày hiện tại';
			$('#fDate').focus();
		}
		if (!Utils.compareDate(tDate, day + '/' + month + '/' + year)) {
			msg = 'Đến ngày không được lớn hơn ngày hiện tại';
			$('#tDate').focus();
		}
		if (msg.length > 0) {
			$('#errMsg').html(msg).show();
			return false;
		}
		var dataModel = new Object();		
		dataModel.shopId = $('#shopId').val();
		dataModel.fromDate = $('#fDate').val().trim();
		dataModel.toDate = $('#tDate').val().trim();
		dataModel.cycleId = $('#cycle').val();
		ReportUtils.exportReport('/report/debit/cnptct/export', dataModel);
	},
	
	/**
	* BC 3.1 cong no phai thu tong hop
	* @author trietptm
	* @since Dec 29, 2015
	* */	
	exportReceivableSummary: function() {
		var msg = '';
		$('#errMsg').html('').hide();
		if ($('#shopId').val().trim().length == 0 || $('#shopId').val().trim() == activeType.ALL) {
			msg = format(msgErr_required_field,'đơn vị');
		}
		if (msg.length == 0) {
			if ($('#isNPP').val() == "false") {
				msg="Đơn vị bạn vừa chọn không phải là nhà phân phối cụ thể!";
			}
		}
		if (msg.length == 0) {
			msg = Utils.getMessageOfRequireCheck('fDate','Từ ngày');
		}
		if (msg.length == 0) {
			msg = Utils.getMessageOfInvalidFormatDate('fDate','Từ ngày');
		}
		if (msg.length == 0) {
			msg = Utils.getMessageOfRequireCheck('tDate','Từ ngày');
		}
		if (msg.length == 0) {
			msg = Utils.getMessageOfInvalidFormatDate('tDate','Đến ngày');
		}
		var fDate = $('#fDate').val();
		var tDate = $('#tDate').val();
		if (!Utils.compareDate(fDate, tDate)) {
			msg = msgErr_fromdate_greater_todate;		
			$('#fDate').focus();
		}		
		if (!Utils.compareDate(fDate, tDate)) {
			msg = msgErr_fromdate_greater_todate;		
			$('#fDate').focus();
		}
		var currentTime = new Date();
		var month = currentTime.getMonth() + 1;
		var day = currentTime.getDate();
		var year = currentTime.getFullYear();
		if (!Utils.compareDate(fDate,day + '/' + month + '/' + year)) {
			msg = 'Từ ngày không được lớn hơn ngày hiện tại';
			$('#fDate').focus();
		}
		if (!Utils.compareDate(tDate,day + '/' + month + '/' + year)) {
			msg = 'Đến ngày không được lớn hơn ngày hiện tại';
			$('#tDate').focus();
		}
		if (msg.length > 0) {
			$('#errMsg').html(msg).show();
			return false;
		}
		var dataModel = new Object();		
		dataModel.shopId = $('#shopId').val();
		dataModel.fromDate = $('#fDate').val().trim();
		dataModel.toDate = $('#tDate').val().trim();
		dataModel.cycleId = $('#cycle').val();
		ReportUtils.exportReport('/report/debit/receivable_summary/export', dataModel);
	},
	
	/**
	* BC 3.4 cong no phai tra chi tiet
	* @author trietptm
	* @since Dec 31, 2015
	* */	
	exportPayableDetail: function() {
		var msg = '';
		$('#errMsg').html('').hide();
		if ($('#shopId').val().trim().length == 0 || $('#shopId').val().trim() == activeType.ALL) {
			msg = format(msgErr_required_field, 'đơn vị');
		}
		if (msg.length == 0) {
			msg = Utils.getMessageOfRequireCheck('fDate', 'Từ ngày');
		}
		if (msg.length == 0) {
			msg = Utils.getMessageOfRequireCheck('tDate', 'Đến ngày');
		}
		if (msg.length == 0) {
			msg = Utils.getMessageOfInvalidFormatDate('fDate','Từ ngày');
		}
		if (msg.length == 0) {
			msg = Utils.getMessageOfInvalidFormatDate('tDate','Đến ngày');
		}
		var fDate = $('#fDate').val();
		var tDate = $('#tDate').val();
		if (!Utils.compareDate(fDate, tDate)) {
			msg = js_fromdate_todate_validate;		
			$('#fDate').focus();
		}
		if (msg.length == 0) {
			msg = Utils.compareCurrentDateEx('tDate','Đến ngày');
		}
		if (msg.length > 0) {
			$('#errMsg').html(msg).show();
			return false;
		}
		var dataModel = new Object();
		dataModel.strListShopId = $('#shopId').val();
		dataModel.fromDate = $('#fDate').val().trim();
		dataModel.toDate = $('#tDate').val().trim();
		ReportUtils.exportReport('/report/debit/payable_detail/export', dataModel);
	},
};
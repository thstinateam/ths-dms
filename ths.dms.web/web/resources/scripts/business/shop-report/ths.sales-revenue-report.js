/*
 *  BAO CAO DOANH THU BAN HANG
 */
var SalesRevenueReport = {
	/**
	 * Rpt 7.2.1: Bao cao Theo doi Ban hang theo mat hang
	 * */
	exportTDBHTMH : function() {
		$('#errMsg').html('').hide();
		var msg = '';
		var t = $('#shop').combotree('tree');
		var node = t.tree('find', $('#shop').combotree('getValue'));
		if (node == undefined || node == null || node.isLevel == undefined || node.isLevel == null || node.isLevel != 5) {
			msg = "Đơn vị bạn vừa chọn không phải là 1 nhà phân phối cụ thể!";
		}
		
		if (msg.length == 0) {
			msg = Utils.getMessageOfRequireCheck('fDate', 'Từ ngày');
		}
		if (msg.length == 0) {
			msg = Utils.getMessageOfInvalidFormatDate('fDate', 'Từ ngày');
		}
		if (msg.length == 0) {
			msg = Utils.getMessageOfRequireCheck('tDate', 'Đến ngày');
		}
		if (msg.length == 0) {
			msg = Utils.getMessageOfInvalidFormatDate('tDate', 'Đến ngày');
		}
		var fDate = $('#fDate').val();
		var tDate = $('#tDate').val();
		if (!Utils.compareDate(fDate, tDate)) {
			msg = msgErr_fromdate_greater_todate;
			$('#fDate').focus();
		}
		var currentTime = new Date();
		var month = currentTime.getMonth() + 1;
		var day = currentTime.getDate();
		var year = currentTime.getFullYear();
		if (!Utils.compareDate(fDate, day + '/' + month + '/' + year)) {
			msg = 'Từ ngày không được lớn hơn ngày hiện tại';
			$('#fDate').focus();
		}
		if (msg.length > 0) {
			$('#errMsg').html(msg).show();
			return false;
		}
		var data = new Object();
		data.shopId = $('#shopId').val().trim();
		data.staffCode = $('#staffCode').val().trim();
		data.fromDate = $('#fDate').val().trim();
		data.toDate = $('#tDate').val().trim();
		if ($('input[name=formatType]:radio:checked').length > 0 && $('input[name=formatType]:radio:checked').val() != undefined) {
			data.formatType = $('input[name=formatType]:radio:checked').val().toUpperCase();
		}
		// ReportUtils.exportReport('/report/sales-revenue/view-dayreport',dataModel);
		var kData = $.param(data, true);
		$('#divOverlay').show();
		ReportUtils.exportReport('/report/sales-revenue/tdbhtmh/export', data);
		
//		$.ajax({
//			type : "POST",
//			url : "/report/sales-revenue/tdbhtmh/export",
//			data : (kData),
//			dataType : "json",
//			success : function(data) {
//				$('#divOverlay').hide();
//				if (!data.error) {
//					if (data.hasData != null && data.hasData != undefined && data.hasData == true) {
//						window.location.href = data.path;
//						setTimeout(function() { // Set timeout để đảm bảo file
//							// load lên hoàn tất
//							CommonSearch.deleteFileExcelExport(data.path);
//						}, 2000);
//					} else {
//						$('#errMsg').html('Không có dữ liệu để xuất!').show();
//					}
//				} else {
//					$('#errMsg').html(data.errMsg).show();
//				}
//			},
//			error : function(XMLHttpRequest, textStatus, errorDivThrown) {
//				$('#divOverlay').hide();
//				StockIssued.xhrExport = null;
//			}
//		});
		
		return false;

	},
	exportRpt10_1_2 : function() {
		$('#errMsg').html('').hide();
		var msg = '';
		if ($('#shopId').val().trim().length == 0
				|| $('#shopId').val().trim() == activeType.ALL) {
			msg = format(msgErr_required_field, 'đơn vị');
		}
		if (msg.length == 0) {
			if ($('#isNPP').val() == "false") {
				msg = "Đơn vị bạn vừa chọn không phải là 1 nhà phân phối cụ thể!";
			}
		}
		if (msg.length == 0) {
			msg = Utils.getMessageOfRequireCheck('fDate', 'Từ ngày');
		}
		if (msg.length == 0) {
			msg = Utils.getMessageOfInvalidFormatDate('fDate', 'Từ ngày');
		}
		if (msg.length == 0) {
			msg = Utils.getMessageOfRequireCheck('tDate', 'Đến ngày');
		}
		if (msg.length == 0) {
			msg = Utils.getMessageOfInvalidFormatDate('tDate', 'Đến ngày');
		}
		var fDate = $('#fDate').val();
		var tDate = $('#tDate').val();
		if (!Utils.compareDate(fDate, tDate)) {
			msg = msgErr_fromdate_greater_todate;
			$('#fDate').focus();
		}
		if (msg.length == 0) {
			var fdate = $('#fDate').val().trim().split('/');
			var tdate = $('#tDate').val().trim().split('/');
			if(fdate[1]!=tdate[1]){
				msg = 'Từ ngày không cùng tháng với đến ngày';
			}
		}
		var currentTime = new Date();
		var month = currentTime.getMonth() + 1;
		var day = currentTime.getDate();
		var year = currentTime.getFullYear();
		if (!Utils.compareDate(fDate, day + '/' + month + '/' + year)) {
			msg = 'Từ ngày không được lớn hơn ngày hiện tại';
			$('#fDate').focus();
		}
		if (msg.length > 0) {
			$('#errMsg').html(msg).show();
			return false;
		}
		var data = new Object();
		data.shopId = $('#shopId').val().trim();
		data.staffCode = $('#staffCode').val().trim();
		data.fromDate = $('#fDate').val().trim();
		data.toDate = $('#tDate').val().trim();
		if ($('input[name=formatType]:radio:checked').length > 0
				&& $('input[name=formatType]:radio:checked').val() != undefined) {
			data.formatType = $('input[name=formatType]:radio:checked').val()
					.toUpperCase();
		}
		ReportUtils.exportReport('/report/sales-revenue/rpt10_1_2/export', data);
		/*var kData = $.param(data, true);
		$('#divOverlay').show();
		$.ajax({
			type : "POST",
			url : "/report/sales-revenue/rpt10_1_2/export",
			data : (kData),
			dataType : "json",
			success : function(data) {
				$('#divOverlay').hide();
				if (!data.error) {
					if (data.hasData != null && data.hasData != undefined
							&& data.hasData == true) {
						window.location.href = data.path;
						setTimeout(function() { // Set timeout để đảm bảo file
							// load lên hoàn tất
							CommonSearch.deleteFileExcelExport(data.path);
						}, 2000);
					} else {
						$('#errMsg').html('Không có dữ liệu để xuất!').show();
					}
				} else {
					$('#errMsg').html(data.errMsg).show();
				}
			},
			error : function(XMLHttpRequest, textStatus, errorDivThrown) {
				$('#divOverlay').hide();
				StockIssued.xhrExport = null;
			}
		});*/
		return false;


	},
	exportTDDSTKHNHMH : function() {
		$('#errMsg').html('').hide();
		var msg = '';
		if ($('#shopId').val().trim().length == 0
				|| $('#shopId').val().trim() == activeType.ALL) {
			msg = format(msgErr_required_field, 'đơn vị');
		}
		if (msg.length == 0) {
			if ($('#isNPP').val() == "false") {
				msg = "Đơn vị bạn vừa chọn không phải là 1 nhà phân phối cụ thể!";
			}
		}
		if (msg.length == 0) {
			msg = Utils.getMessageOfRequireCheck('fDate', 'Từ ngày');
		}
		if (msg.length == 0) {
			msg = Utils.getMessageOfInvalidFormatDate('fDate', 'Từ ngày');
		}
		if (msg.length == 0) {
			msg = Utils.getMessageOfRequireCheck('tDate', 'Đến ngày');
		}
		if (msg.length == 0) {
			msg = Utils.getMessageOfInvalidFormatDate('tDate', 'Đến ngày');
		}
		var fDate = $('#fDate').val();
		var tDate = $('#tDate').val();
		if (!Utils.compareDate(fDate, tDate)) {
			msg = msgErr_fromdate_greater_todate;
			$('#fDate').focus();
		}
		var currentTime = new Date();
		var month = currentTime.getMonth() + 1;
		var day = currentTime.getDate();
		var year = currentTime.getFullYear();
		if (!Utils.compareDate(fDate, day + '/' + month + '/' + year)) {
			msg = 'Từ ngày không được lớn hơn ngày hiện tại';
			$('#fDate').focus();
		}
		if (msg.length > 0) {
			$('#errMsg').html(msg).show();
			return false;
		}
		var data = new Object();
		data.shopId = $('#shopId').val().trim();
		data.customerCode = $('#customerCode').val().trim();
		data.fromDate = $('#fDate').val().trim();
		data.toDate = $('#tDate').val().trim();
		if ($('input[name=formatType]:radio:checked').length > 0
				&& $('input[name=formatType]:radio:checked').val() != undefined) {
			data.formatType = $('input[name=formatType]:radio:checked').val()
					.toUpperCase();
		}
		ReportUtils.exportReport('/report/sales-revenue/tddstkhnhmh/export', data);
	},
	exportDSKHTMH : function() {
		$('#errMsg').html('').hide();
		var msg = '';
		if ($('#shopId').val().trim().length == 0
				|| $('#shopId').val().trim() == activeType.ALL) {
			msg = format(msgErr_required_field, 'đơn vị');
		}
		if (msg.length == 0) {
			if ($('#isNPP').val() == "false") {
				msg = "Đơn vị bạn vừa chọn không phải là 1 nhà phân phối cụ thể!";
			}
		}
		if (msg.length == 0) {
			msg = Utils.getMessageOfRequireCheck('fDate', 'Từ ngày');
		}
		if (msg.length == 0) {
			msg = Utils.getMessageOfInvalidFormatDate('fDate', 'Từ ngày');
		}
		if (msg.length == 0) {
			msg = Utils.getMessageOfRequireCheck('tDate', 'Đến ngày');
		}
		if (msg.length == 0) {
			msg = Utils.getMessageOfInvalidFormatDate('tDate', 'Đến ngày');
		}
		var fDate = $('#fDate').val();
		var tDate = $('#tDate').val();
		if (!Utils.compareDate(fDate, tDate)) {
			msg = msgErr_fromdate_greater_todate;
			$('#fDate').focus();
		}
		var currentTime = new Date();
		var month = currentTime.getMonth() + 1;
		var day = currentTime.getDate();
		var year = currentTime.getFullYear();
		if (!Utils.compareDate(fDate, day + '/' + month + '/' + year)) {
			msg = 'Từ ngày không được lớn hơn ngày hiện tại';
			$('#fDate').focus();
		}
		if (msg.length > 0) {
			$('#errMsg').html(msg).show();
			return false;
		}
		var data = new Object();
		if ($('input[name=formatType]:radio:checked').length > 0
				&& $('input[name=formatType]:radio:checked').val() != undefined) {
			data.formatType = $('input[name=formatType]:radio:checked').val()
					.toUpperCase();
		}
		data.shopId = $('#shopId').val().trim();
		data.customerCode = $('#customerCode').val().trim().toUpperCase();
		data.fromDate = $('#fDate').val().trim();
		data.toDate = $('#tDate').val().trim();
		data.strListCustomer = $('#customerCode').val().trim().toUpperCase();
		var strListCategory = ""; 
		/*if(ProductCatalog._listCategory != undefined && ProductCatalog._listCategory != null 
				&& ProductCatalog._listCategory.length > 0 && ProductCatalog._listCategory[0] != '0' ){
			var sizeListCategory = ProductCatalog._listCategory.length; 
			for(var i = 0; i< sizeListCategory-1; i++){
				strListCategory += ProductCatalog._listCategory[i].trim().toUpperCase() + ",";
			}
			strListCategory += ProductCatalog._listCategory[sizeListCategory-1].trim().toUpperCase();
		}*/
		var arr = $('#category').val();
		if (arr != null && arr.length > 0) {
			strListCategory = arr.join(",");
		}
		data.strListCategory = strListCategory.trim().toUpperCase();		
		data.strListProduct = $('#productCode').val().trim().toUpperCase();
		ReportUtils.exportReport('/report/sales-revenue/dskhtmh/export', data);
		return false;
	},
	exportTTCTTB : function() {
		$('#errMsg').html('').hide();
		var msg = '';
		if ($('#shop').combotree('getValue').trim().length == 0
				|| $('#shop').combotree('getValue').trim() == activeType.ALL) {
			msg = format(msgErr_required_field, 'đơn vị');
		}

		if (msg.length == 0) {
			if ($('#isNPP').val() == "0") {
				msg = "Đơn vị bạn vừa chọn không phải là 1 nhà phân phối cụ thể!";
			}
		}

		if (msg.length == 0) {
			msg = Utils.getMessageOfRequireCheck('fDate', 'Từ ngày');
		}
		if (msg.length == 0) {
			msg = Utils.getMessageOfInvalidFormatDate('fDate', 'Từ ngày');
		}
		if (msg.length == 0) {
			msg = Utils.getMessageOfRequireCheck('tDate', 'Đến ngày');
		}
		if (msg.length == 0) {
			msg = Utils.getMessageOfInvalidFormatDate('tDate', 'Đến ngày');
		}
		var fDate = $('#fDate').val();
		var tDate = $('#tDate').val();
		if (!Utils.compareDate(fDate, tDate)) {
			msg = msgErr_fromdate_greater_todate;
			$('#fDate').focus();
		}
		var currentTime = new Date();
		var month = currentTime.getMonth() + 1;
		var day = currentTime.getDate();
		var year = currentTime.getFullYear();
		if (!Utils.compareDate(fDate, day + '/' + month + '/' + year)) {
			msg = 'Từ ngày không được lớn hơn ngày hiện tại';
			$('#fDate').focus();
		}
		if (msg.length > 0) {
			$('#errMsg').html(msg).show();
			return false;
		}
		var data = new Object();
		data.shopId = $('#shop').combotree('getValue').trim();
		data.lstDisProCode = $('#multiChoiceCTTB').val();
		data.fromDate = $('#fDate').val().trim();
		data.toDate = $('#tDate').val().trim();
		data.type = $('#type').val().trim();

		if ($('input[name=formatType]:radio:checked').length > 0
				&& $('input[name=formatType]:radio:checked').val() != undefined) {
			data.formatType = $('input[name=formatType]:radio:checked').val()
					.toUpperCase();
		}
		ReportUtils.exportReport('/report/sales-revenue/ttcttb/export', data);
//		var kData = $.param(data, true);
//		$('#divOverlay').show();
//		$.ajax({
//			type : "POST",
//			url : "/report/sales-revenue/ttcttb/export",
//			data : (kData),
//			dataType : "json",
//			success : function(data) {
//				$('#divOverlay').hide();
//				if (!data.error) {
//					if (data.hasData != null && data.hasData != undefined
//							&& data.hasData == true) {
//						window.location.href = data.path;
//						setTimeout(function() { // Set timeout để đảm bảo file
//							// load lên hoàn tất
//							CommonSearch.deleteFileExcelExport(data.path);
//						}, 2000);
//					} else {
//						$('#errMsg').html('Không có dữ liệu để xuất').show();
//					}
//				} else {
//					$('#errMsg').html(data.errMsg).show();
//				}
//			},
//			error : function(XMLHttpRequest, textStatus, errorDivThrown) {
//				$('#divOverlay').hide();
//				StockIssued.xhrExport = null;
//			}
//		});
		return false;

	},
	exportDTTHNV : function() {
		$('#errMsg').html('').hide();
		var msg = '';
		if ($('#shop').combotree('getValue').trim().length == 0
				|| $('#shop').combotree('getValue').trim() == activeType.ALL) {
			msg = format(msgErr_required_field, 'đơn vị');
		}

		if (msg.length == 0) {
			if ($('#isNPP').val() == "false") {
				msg = "Đơn vị bạn vừa chọn không phải là 1 nhà phân phối cụ thể!";
			}
		}

		if (msg.length == 0) {
			msg = Utils.getMessageOfRequireCheck('fDate', 'Từ ngày');
		}
		if (msg.length == 0) {
			msg = Utils.getMessageOfInvalidFormatDate('fDate', 'Từ ngày');
		}
		if (msg.length == 0) {
			msg = Utils.getMessageOfRequireCheck('tDate', 'Đến ngày');
		}
		if (msg.length == 0) {
			msg = Utils.getMessageOfInvalidFormatDate('tDate', 'Đến ngày');
		}
		var fDate = $('#fDate').val();
		var tDate = $('#tDate').val();
		if (!Utils.compareDate(fDate, tDate)) {
			msg = msgErr_fromdate_greater_todate;
			$('#fDate').focus();
		}
		var currentTime = new Date();
		var month = currentTime.getMonth() + 1;
		var day = currentTime.getDate();
		var year = currentTime.getFullYear();
		if (!Utils.compareDate(fDate, day + '/' + month + '/' + year)) {
			msg = 'Từ ngày không được lớn hơn ngày hiện tại';
			$('#fDate').focus();
		}
		if (msg.length > 0) {
			$('#errMsg').html(msg).show();
			return false;
		}
		var data = new Object();
		data.shopId = $('#shop').combotree('getValue').trim();
		data.staffCode = $('#staffCode').val().trim();
		data.fromDate = $('#fDate').val().trim();
		data.toDate = $('#tDate').val().trim();

		if ($('input[name=formatType]:radio:checked').length > 0
				&& $('input[name=formatType]:radio:checked').val() != undefined) {
			data.formatType = $('input[name=formatType]:radio:checked').val()
					.toUpperCase();
		}
		var kData = $.param(data, true);
		$('#divOverlay').show();
		$.ajax({
			type : "POST",
			url : "/report/sales-revenue/dtthnv/export",
			data : (kData),
			dataType : "json",
			success : function(data) {
				$('#divOverlay').hide();
				if (!data.error) {
					if (data.hasData != null && data.hasData != undefined
							&& data.hasData == true) {
						window.location.href = data.path;
						setTimeout(function() { // Set timeout để đảm bảo file
							// load lên hoàn tất
							CommonSearch.deleteFileExcelExport(data.path);
						}, 2000);
					} else {
						$('#errMsg').html('Không có dữ liệu để xuất').show();
					}
				} else {
					$('#errMsg').html(data.errMsg).show();
				}
			},
			error : function(XMLHttpRequest, textStatus, errorDivThrown) {
				$('#divOverlay').hide();
				StockIssued.xhrExport = null;
			}
		});
		return false;
	},
	exportBTHDN : function() {
		$('#errMsg').html('').hide();
		var msg = '';
		if ($('#shop').combotree('getValue').trim().length == 0
				|| $('#shop').combotree('getValue').trim() == activeType.ALL) {
			msg = format(msgErr_required_field, 'đơn vị');
		}

		if (msg.length == 0) {
			if ($('#isNPP').val() == "false") {
				msg = "Đơn vị bạn vừa chọn không phải là 1 nhà phân phối cụ thể!";
			}
		}

		if (msg.length == 0) {
			msg = Utils.getMessageOfRequireCheck('fDate', 'Từ ngày');
		}
		if (msg.length == 0) {
			msg = Utils.getMessageOfInvalidFormatDate('fDate', 'Từ ngày');
		}
		if (msg.length == 0) {
			msg = Utils.getMessageOfRequireCheck('tDate', 'Đến ngày');
		}
		if (msg.length == 0) {
			msg = Utils.getMessageOfInvalidFormatDate('tDate', 'Đến ngày');
		}
		var fDate = $('#fDate').val();
		var tDate = $('#tDate').val();
		if (!Utils.compareDate(fDate, tDate)) {
			msg = msgErr_fromdate_greater_todate;
			$('#fDate').focus();
		}
		var currentTime = new Date();
		var month = currentTime.getMonth() + 1;
		var day = currentTime.getDate();
		var year = currentTime.getFullYear();
		if (!Utils.compareDate(fDate, day + '/' + month + '/' + year)) {
			msg = 'Từ ngày không được lớn hơn ngày hiện tại';
			$('#fDate').focus();
		}
		if (msg.length > 0) {
			$('#errMsg').html(msg).show();
			return false;
		}
		var data = new Object();
		data.shopId = $('#shop').combotree('getValue').trim();
		data.catCode = $('#catCode').val().trim();
		data.fromDate = $('#fDate').val().trim();
		data.toDate = $('#tDate').val().trim();

		var kData = $.param(data, true);
		$('#divOverlay').show();
		$.ajax({
			type : "POST",
			url : "/report/sales-revenue/bthdh/export",
			data : (kData),
			dataType : "json",
			success : function(data) {
				$('#divOverlay').hide();
				if (!data.error) {
					if (data.hasData != null && data.hasData != undefined
							&& data.hasData == true) {
						window.location.href = data.path;
						setTimeout(function() { // Set timeout để đảm bảo file
							// load lên hoàn tất
							CommonSearch.deleteFileExcelExport(data.path);
						}, 2000);
					} else {
						$('#errMsg').html('Không có dữ liệu để xuất').show();
					}
				} else {
					$('#errMsg').html(data.errMsg).show();
				}
			},
			error : function(XMLHttpRequest, textStatus, errorDivThrown) {
				$('#divOverlay').hide();
				StockIssued.xhrExport = null;
			}
		});
		return false;
	},
	exportBCN : function() {
		$('#errMsg').html('').hide();
		var msg = '';
		if ($('#shop').combotree('getValue').trim().length == 0
				|| $('#shop').combotree('getValue').trim() == activeType.ALL) {
			msg = format(msgErr_required_field, 'đơn vị');
		}

		if (msg.length == 0) {
			if ($('#isNPP').val() == "false") {
				msg = "Đơn vị bạn vừa chọn không phải là 1 nhà phân phối cụ thể!";
			}
		}

		if (msg.length == 0) {
			msg = Utils.getMessageOfRequireCheck('fDate', 'Từ ngày');
		}
		if (msg.length == 0) {
			msg = Utils.getMessageOfInvalidFormatDate('fDate', 'Từ ngày');
		}

		var fDate = $('#fDate').val();
		var currentTime = new Date();
		var month = currentTime.getMonth() + 1;
		var day = currentTime.getDate();
		var year = currentTime.getFullYear();
		if (!Utils.compareDate(fDate, day + '/' + month + '/' + year)) {
			msg = 'Ngày báo cáo không được lớn hơn ngày hiện tại';
			$('#fDate').focus();
		}
		if (msg.length > 0) {
			$('#errMsg').html(msg).show();
			return false;
		}
		var data = new Object();
		data.shopId = $('#shop').combotree('getValue').trim();
		data.toDate = $('#fDate').val().trim();
		
		if ($('input[name=formatType]:radio:checked').length > 0
				&& $('input[name=formatType]:radio:checked').val() != undefined) {
			data.formatType = $('input[name=formatType]:radio:checked').val()
					.toUpperCase();
		}

		ReportUtils.exportReport('/report/sales-revenue/bcn/export', data);
	},
	exportCTKMTCT : function() {
		$('#errMsg').html('').hide();
		var msg = '';
		if ($('#shop').combotree('getValue').trim().length == 0
				|| $('#shop').combotree('getValue').trim() == activeType.ALL) {
			msg = format(msgErr_required_field, 'đơn vị');
		}

		if (msg.length == 0) {
			if ($('#isNPP').val() == "false") {
				msg = "Đơn vị bạn vừa chọn không phải là 1 nhà phân phối cụ thể!";
			}
		}
		if (msg.length == 0) {
			msg = Utils.getMessageOfSpecialCharactersValidate('staffCode',
					'NVBH', Utils._CODE);
		}
		if (msg.length == 0) {
			msg = Utils.getMessageOfRequireCheck('fDate', 'Từ ngày');
		}
		if (msg.length == 0) {
			msg = Utils.getMessageOfInvalidFormatDate('fDate', 'Từ ngày');
		}
		if (msg.length == 0) {
			msg = Utils.getMessageOfRequireCheck('tDate', 'Đến ngày');
		}
		if (msg.length == 0) {
			msg = Utils.getMessageOfInvalidFormatDate('tDate', 'Đến ngày');
		}
		var fDate = $('#fDate').val();
		var tDate = $('#tDate').val();
		if (msg.length == 0) {
			if (!Utils.compareDate(fDate, tDate)) {
				msg = msgErr_fromdate_greater_todate;
				$('#fDate').focus();
			}
		}
		var currentTime = new Date();
		var month = currentTime.getMonth() + 1;
		var day = currentTime.getDate();
		var year = currentTime.getFullYear();
		if (msg.length == 0) {
			if (!Utils.compareDate(fDate, day + '/' + month + '/' + year)) {
				msg = 'Từ ngày không được lớn hơn ngày hiện tại';
				$('#fDate').focus();
			}
		}
		if (msg.length > 0) {
			$('#errMsg').html(msg).show();
			return false;
		}
		var data = new Object();
		data.shopId = $('#shop').combotree('getValue').trim();
		data.staffCode = $('#staffCode').val().trim();
		data.fromDate = $('#fDate').val().trim();
		data.toDate = $('#tDate').val().trim();

		if ($('input[name=formatType]:radio:checked').length > 0 && $('input[name=formatType]:radio:checked').val() != undefined) {
			data.formatType = $('input[name=formatType]:radio:checked').val().toUpperCase();
		}

		if (data.formatType === "XLS") {
			ReportUtils.exportReport('/report/sales-revenue/ctkmtct/export', data);
		} else {
			ReportUtils.exportReport('/report/sales-revenue/ctkmtct/exportPDF', data);
		}
		
	},
	exportCTKMTCTNV : function() {
		$('#errMsg').html('').hide();
		var msg = '';
		if ($('#shop').combotree('getValue').trim().length == 0
				|| $('#shop').combotree('getValue').trim() == activeType.ALL) {
			msg = format(msgErr_required_field, 'đơn vị');
		}

		if (msg.length == 0) {
			if ($('#isNPP').val() == "false") {
				msg = "Đơn vị bạn vừa chọn không phải là 1 nhà phân phối cụ thể!";
			}
		}
		if (msg.length == 0) {
			msg = Utils.getMessageOfSpecialCharactersValidate('staffCode',
					'NVBH', Utils._CODE);
		}
		if (msg.length == 0) {
			msg = Utils.getMessageOfRequireCheck('fDate', 'Từ ngày');
		}
		if (msg.length == 0) {
			msg = Utils.getMessageOfInvalidFormatDate('fDate', 'Từ ngày');
		}
		if (msg.length == 0) {
			msg = Utils.getMessageOfRequireCheck('tDate', 'Đến ngày');
		}
		if (msg.length == 0) {
			msg = Utils.getMessageOfInvalidFormatDate('tDate', 'Đến ngày');
		}
		var fDate = $('#fDate').val();
		var tDate = $('#tDate').val();
		if (msg.length == 0) {
			if (!Utils.compareDate(fDate, tDate)) {
				msg = msgErr_fromdate_greater_todate;
				$('#fDate').focus();
			}
		}
		var currentTime = new Date();
		var month = currentTime.getMonth() + 1;
		var day = currentTime.getDate();
		var year = currentTime.getFullYear();
		if (msg.length == 0) {
			if (!Utils.compareDate(fDate, day + '/' + month + '/' + year)) {
				msg = 'Từ ngày không được lớn hơn ngày hiện tại';
				$('#fDate').focus();
			}
		}
		if (msg.length > 0) {
			$('#errMsg').html(msg).show();
			return false;
		}
		var data = new Object();
		data.shopId = $('#shop').combotree('getValue').trim();
		data.staffCode = $('#staffCode').val().trim();
		data.fromDate = $('#fDate').val().trim();
		data.toDate = $('#tDate').val().trim();

		var kData = $.param(data, true);
		$('#divOverlay').show();
		$.ajax({
			type : "POST",
			url : "/report/sales-revenue/ctkmtctnv/export",
			data : (kData),
			dataType : "json",
			success : function(data) {
				$('#divOverlay').hide();
				if (!data.error) {
					if (data.hasData != null && data.hasData != undefined
							&& data.hasData == true) {
						window.location.href = data.path;
						setTimeout(function() { // Set timeout để đảm bảo file
							// load lên hoàn tất
							CommonSearch.deleteFileExcelExport(data.path);
						}, 2000);
					} else {
						$('#errMsg').html('Không có dữ liệu để xuất').show();
					}
				} else {
					$('#errMsg').html(data.errMsg).show();
				}
			},
			error : function(XMLHttpRequest, textStatus, errorDivThrown) {
				$('#divOverlay').hide();
				StockIssued.xhrExport = null;
			}
		});
		return false;
	},
	/* Begin Doanh so theo muc chuong trinh TB */
	exportDSTMCTTB : function() {
		$('#errMsg').html('').hide();
		var msg = '';
		if ($('#shop').combotree('getValue').trim().length == 0
				|| $('#shop').combotree('getValue').trim() == activeType.ALL) {
			msg = format(msgErr_required_field, 'đơn vị');
		}
		if (msg.length == 0) {
			if ($('#isNPP').val() == "false") {
				msg = "Đơn vị bạn vừa chọn không phải là 1 nhà phân phối cụ thể!";
			}
		}
		if (msg.length == 0) {
			msg = Utils.getMessageOfRequireCheck('multiChoiceCTTB',
					'Chương trình trưng bày');
		}
		if (msg.length > 0) {
			$('#errMsg').html(msg).show();
			return false;
		}
		var data = new Object();
		data.shopId = $('#shop').combotree('getValue').trim();
		data.year = $('#fullYearIn10').val();
		data.lstProgramDisplayCode = $('#multiChoiceCTTB').val();
		data.customer = $('#customerCode').val();
		ReportUtils.exportReport('/report/sales-revenue/dstmcttb/export', data);
		
	},

	// vuongmq
	/* End Doanh so theo muc chuong trinh TB */
	exportTDCTDS : function() {
		$('#errMsg').html('').hide();
		var msg = '';
		if ($('#shop').combotree('getValue').trim().length == 0
				|| $('#shop').combotree('getValue').trim() == activeType.ALL) {
			msg = format(msgErr_required_field, 'đơn vị');
		}

		if (msg.length == 0) {
			if ($('#isNPP').val() == "false") {
				msg = "Đơn vị bạn vừa chọn không phải là 1 nhà phân phối cụ thể!";
			}
		}

		if (msg.length == 0) {
			msg = Utils.getMessageOfRequireCheck('month', 'Tháng');
		}

		if (msg.length == 0) {
			var currentTime = new Date();
			var month = currentTime.getMonth() + 1;
			var day = currentTime.getDate();
			var year = currentTime.getFullYear();

			var monthString = "01/" + $('#month').val().trim();
			var tmp = Utils.compareMonthString(monthString, day + '/' + month
					+ '/' + year);
			if (tmp == 1) {
				msg = 'Tháng không được lớn hơn tháng hiện tại';
				$('#month').focus();
			}
		}

		if (msg.length > 0) {
			$('#errMsg').html(msg).show();
			return false;
		}

		var data = new Object();
		data.shopId = $('#shop').combotree('getValue').trim();
		data.staffCode = $('#staffCode').val().trim();
		var date = $('#month').val().trim().split('/');
		data.month = date[0];
		data.year = date[1];

		var kData = $.param(data, true);
		$('#divOverlay').show();
		$.ajax({
			type : "POST",
			url : "/report/sales-revenue/tdctds/export",
			data : (kData),
			dataType : "json",
			success : function(data) {
				$('#divOverlay').hide();
				if (!data.error) {
					if (data.hasData != null && data.hasData != undefined
							&& data.hasData == true) {
						window.location.href = data.path;
						setTimeout(function() { // Set timeout để đảm bảo file
							// load lên hoàn tất
							CommonSearch.deleteFileExcelExport(data.path);
						}, 2000);
					} else {
						$('#errMsg').html('Không có dữ liệu để xuất').show();
					}
				} else {
					$('#errMsg').html(data.errMsg).show();
				}
			},
			error : function(XMLHttpRequest, textStatus, errorDivThrown) {
				$('#divOverlay').hide();
				StockIssued.xhrExport = null;
			}
		});
		return false;

	},
	exportDTBHTNTNVBH : function() {
		$('#errMsg').html('').hide();
		var msg = '';
		if ($('#shop').combotree('getValue').trim().length == 0
				|| $('#shop').combotree('getValue').trim() == activeType.ALL) {
			msg = format(msgErr_required_field, 'đơn vị');
		}

		if (msg.length == 0) {
			msg = Utils.getMessageOfRequireCheck('fDate', 'Từ ngày');
		}
		if (msg.length == 0) {
			msg = Utils.getMessageOfInvalidFormatDate('fDate', 'Từ ngày');
		}
		if (msg.length == 0) {
			msg = Utils.getMessageOfRequireCheck('tDate', 'Đến ngày');
		}
		if (msg.length == 0) {
			msg = Utils.getMessageOfInvalidFormatDate('tDate', 'Đến ngày');
		}
		var fDate = $('#fDate').val();
		var tDate = $('#tDate').val();
		if (!Utils.compareDate(fDate, tDate)) {
			msg = msgErr_fromdate_greater_todate;
			$('#fDate').focus();
		}
		var currentTime = new Date();
		var month = currentTime.getMonth() + 1;
		var day = currentTime.getDate();
		var year = currentTime.getFullYear();
		if (!Utils.compareDate(fDate, day + '/' + month + '/' + year)) {
			msg = 'Từ ngày không được lớn hơn ngày hiện tại';
			$('#fDate').focus();
		}
		if (msg.length > 0) {
			$('#errMsg').html(msg).show();
			return false;
		}
		var data = new Object();
		data.shopId = $('#shopId').val().trim();
		data.staffCode = $('#staffCode').val().trim();
		data.fromDate = $('#fDate').val().trim();
		data.toDate = $('#tDate').val().trim();

		if ($('input[name=formatType]:radio:checked').length > 0
				&& $('input[name=formatType]:radio:checked').val() != undefined) {
			data.formatType = $('input[name=formatType]:radio:checked').val()
					.toUpperCase();
		}

		if (data.formatType === "PDF") {
			ReportUtils.exportReport('/report/sales-revenue/dtbhtntnvbh/export', data);
		} else {
			ReportUtils.exportReport('/report/sales-revenue/dtbhtntnvbh/export', data);
		}

	},
	exportDSTHNVBHTN : function() {
		$('#errMsg').html('').hide();
		var msg = '';
		if ($('#shop').combotree('getValue').trim().length == 0
				|| $('#shop').combotree('getValue').trim() == activeType.ALL) {
			msg = format(msgErr_required_field, 'đơn vị');
		}

		if (msg.length == 0) {
			if ($('#isNPP').val() == "false") {
				msg = "Đơn vị bạn vừa chọn không phải là 1 nhà phân phối cụ thể!";
			}
		}
		if (msg.length == 0) {
			msg = Utils.getMessageOfRequireCheck('fDate', 'Từ ngày');
		}
		if (msg.length == 0) {
			msg = Utils.getMessageOfInvalidFormatDate('fDate', 'Từ ngày');
		}
		if (msg.length == 0) {
			msg = Utils.getMessageOfRequireCheck('tDate', 'Đến ngày');
		}
		if (msg.length == 0) {
			msg = Utils.getMessageOfInvalidFormatDate('tDate', 'Đến ngày');
		}
		var fDate = $('#fDate').val();
		var tDate = $('#tDate').val();
		if (!Utils.compareDate(fDate, tDate)) {
			msg = msgErr_fromdate_greater_todate;
			$('#fDate').focus();
		}
		var currentTime = new Date();
		var month = currentTime.getMonth() + 1;
		var day = currentTime.getDate();
		var year = currentTime.getFullYear();
		if (!Utils.compareDate(fDate, day + '/' + month + '/' + year)) {
			msg = 'Từ ngày không được lớn hơn ngày hiện tại';
			$('#fDate').focus();
		}
		if (msg.length > 0) {
			$('#errMsg').html(msg).show();
			return false;
		}
		var data = new Object();
		data.type = $('#type').val().trim();
		data.shopId = $('#shopId').val().trim();
		data.staffCode = $('#staffCode').val().trim();
		data.fromDate = $('#fDate').val().trim();
		data.toDate = $('#tDate').val().trim();

		if ($('input[name=formatType]:radio:checked').length > 0
				&& $('input[name=formatType]:radio:checked').val() != undefined) {
			data.formatType = $('input[name=formatType]:radio:checked').val()
					.toUpperCase();
		}

		ReportUtils.exportReport('/report/sales-revenue/dsthnvbhtn/export', data, 'errMsg');

	},
	exportTHCTHTB : function() {
		// vuonghn update
		$('#errMsg').html('').hide();
		var msg = '';
		if ($('#shopId').val().trim().length == 0
				|| $('#shopId').val().trim() == activeType.ALL) {
			msg = format(msgErr_required_field, 'đơn vị');
		}
		if (msg.length == 0) {
			if ($('#isNPP').val() == "false") {
				msg = "Đơn vị bạn vừa chọn không phải là 1 nhà phân phối cụ thể!";
			}
		}
		if (msg.length == 0) {
			if ($('#fullYearIn10').val().trim() == '')
				msg = "Bạn chưa chọn năm";
		}
		if (msg.length > 0) {
			$('#errMsg').html(msg).show();
			return false;
		}
		var data = new Object();
		data.shopId = $('#shopId').val().trim();
		data.year = $('#fullYearIn10').val();
		data.lstProgramDisplayCode = $('#multiChoiceCTTB').val().trim();
		// ReportUtils.exportReport('/report/sales-revenue/view-dayreport',dataModel);
		if ($('input[name=formatType]:radio:checked').length > 0
				&& $('input[name=formatType]:radio:checked').val() != undefined) {
			data.formatType = $('input[name=formatType]:radio:checked').val()
					.toUpperCase();
		}
		var kData = $.param(data, true);
		$('#divOverlay').show();
		$.ajax({
			type : "POST",
			url : "/report/sales-revenue/thcthtb/export",
			data : (kData),
			dataType : "json",
			success : function(data) {
				$('#divOverlay').hide();
				if (!data.error) {
					if (data.hasData != undefined && data.hasData != null
							&& data.hasData == false) {
						$('#errMsg').html('Không có dữ liệu để xuất báo cáo')
								.show();
					} else {
						window.location.href = data.path;
						setTimeout(function() { // Set timeout để đảm bảo file
							// load lên hoàn tất
							CommonSearch.deleteFileExcelExport(data.path);
						}, 2000);
					}
				} else {
					$('#errMsg').html(data.errMsg).show();
				}
			},
			error : function(XMLHttpRequest, textStatus, errorDivThrown) {
				$('#divOverlay').hide();
				StockIssued.xhrExport = null;
			}
		});
		return false;

	},
	exportBCCTKM : function() { // vuonghn Bao cao chi tiet khuyen mai
		$('#errMsg').html('').hide();
		var msg = '';
		if ($('#shopId').val().trim().length == 0
				|| $('#shopId').val().trim() == activeType.ALL) {
			msg = format(msgErr_required_field, 'đơn vị');
		}
		/*
		 * if(msg.length == 0){ if($('#isNPP').val() == "false"){ msg = "Đơn vị
		 * bạn vừa chọn không phải là 1 nhà phân phối cụ thể!"; } }
		 */
		if (msg.length == 0) {
			msg = Utils.getMessageOfRequireCheck('fDate', 'Từ ngày');
		}
		if (msg.length == 0) {
			msg = Utils.getMessageOfInvalidFormatDate('fDate', 'Từ ngày');
		}
		if (msg.length == 0) {
			msg = Utils.getMessageOfRequireCheck('tDate', 'Đến ngày');
		}
		if (msg.length == 0) {
			msg = Utils.getMessageOfInvalidFormatDate('tDate', 'Đến ngày');
		}
		var fDate = $('#fDate').val();
		var tDate = $('#tDate').val();
		if (!Utils.compareDate(fDate, tDate)) {
			msg = msgErr_fromdate_greater_todate;
			$('#fDate').focus();
		}
		var currentTime = new Date();
		var month = currentTime.getMonth() + 1;
		var day = currentTime.getDate();
		var year = currentTime.getFullYear();
		if (!Utils.compareDate(fDate, day + '/' + month + '/' + year)) {
			msg = 'Từ ngày không được lớn hơn ngày hiện tại';
			$('#fDate').focus();
		}
		if (msg.length > 0) {
			$('#errMsg').html(msg).show();
			return false;
		}
		var data = new Object();
		data.shopId = $('#shopId').val().trim();
		data.staffCode = $('#staffCode').val().trim();
		data.fromDate = $('#fDate').val().trim();
		data.toDate = $('#tDate').val().trim();
		// ReportUtils.exportReport('/report/sales-revenue/view-dayreport',dataModel);
		var kData = $.param(data, true);
		$('#divOverlay').show();
		$.ajax({
			type : "POST",
			url : "/report/sales-revenue/bcctkm/export",
			data : (kData),
			dataType : "json",
			success : function(data) {
				$('#divOverlay').hide();
				if (!data.error) {
					if (data.hasData != null && data.hasData != undefined
							&& data.hasData == true) {
						window.location.href = data.path;
						setTimeout(function() { // Set timeout để đảm bảo file
							// load lên hoàn tất
							CommonSearch.deleteFileExcelExport(data.path);
						}, 2000);
					} else {
						$('#errMsg').html('Không có dữ liệu để xuất').show();
					}
				} else {
					$('#errMsg').html(data.errMsg).show();
				}
			},
			error : function(XMLHttpRequest, textStatus, errorDivThrown) {
				$('#divOverlay').hide();
				StockIssued.xhrExport = null;
			}
		});
		return false;

	},
	/* BAO CAO NGAY */
	exportDayReport : function() {
		$('#errMsg').html('').hide();
		var msg = '';
		if ($('#shopId').val().trim().length == 0
				|| $('#shopId').val().trim() == activeType.ALL) {
			msg = format(msgErr_required_field, 'đơn vị');
		}
		if (msg.length > 0) {
			$('#errMsg').html(msg).show();
			return false;
		}
		var dataModel = new Object();
		dataModel.shopId = $('#shopId').val().trim();
		ReportUtils.exportReport('/report/sales-revenue/view-dayreport',
				dataModel);
	},
	exportPoInDate : function() {
		var msg = '';
		$('#errMsg').html('').hide();
		if ($('#shopId').val().trim().length == 0
				|| $('#shopId').val().trim() == activeType.ALL) {
			msg = format(msgErr_required_field, 'đơn vị');
		}
		var chot = '';
		if (msg == "") {
			msg = Utils.getMessageOfRequireCheck('fDate', 'Từ ngày', false);
			chot = 'fDate';
		}
		if (msg == "") {
			ms = Utils.getMessageOfRequireCheck('tDate', 'Đến ngày', false);
			chot = 'tDate';
		}
		if (msg.length > 0) {
			$('#errMsg').html(msg).show();
			$('#' + chot).focus();
			return false;
		}
		if (msg.length == 0) {
			msg = Utils.getMessageOfInvalidFormatDate('fDate', 'Từ ngày');
		}
		if (msg.length == 0) {
			msg = Utils.getMessageOfInvalidFormatDate('tDate', 'Đến ngày');
		}
		var fDate = $('#fDate').val();
		var tDate = $('#tDate').val();
		if (!Utils.compareDate(fDate, tDate)) {
			msg = msgErr_fromdate_greater_todate;
			$('#fDate').focus();
		}
		if (msg.length > 0) {
			$('#errMsg').html(msg).show();
			return false;
		}
		var dataModel = new Object();
		dataModel.shopId = $('#shopId').val().trim();
		dataModel.fromDate = $('#fDate').val().trim();
		dataModel.toDate = $('#tDate').val().trim();
		dataModel.staffCode = $('#staffCode').val().trim();
		ReportUtils.exportReport('/report/sales-revenue/poPage', dataModel);
	},

	exportBHTSP : function() {
		$('#errMsg').html('').hide();
		var msg = '';
		if ($('#shopCode').val().trim().length == 0
				|| $('#shopCode').val().trim() == activeType.ALL) {
			msg = format(msgErr_required_field, 'đơn vị');
		}

		if (msg.length == 0) {
			msg = Utils.getMessageOfRequireCheck('fDate', 'Từ ngày');
		}
		if (msg.length == 0) {
			msg = Utils.getMessageOfInvalidFormatDate('fDate', 'Từ ngày');
		}
		if (msg.length == 0) {
			msg = Utils.getMessageOfRequireCheck('tDate', 'Đến ngày');
		}
		if (msg.length == 0) {
			msg = Utils.getMessageOfInvalidFormatDate('tDate', 'Đến ngày');
		}
		var fDate = $('#fDate').val();
		var tDate = $('#tDate').val();
		if (!Utils.compareDate(fDate, tDate)) {
			msg = msgErr_fromdate_greater_todate;
			$('#fDate').focus();
		}
		var currentTime = new Date();
		var month = currentTime.getMonth() + 1;
		var day = currentTime.getDate();
		var year = currentTime.getFullYear();
		if (!Utils.compareDate(fDate, day + '/' + month + '/' + year)) {
			msg = 'Từ ngày không được lớn hơn ngày hiện tại';
			$('#fDate').focus();
		}
		if (msg.length > 0) {
			$('#errMsg').html(msg).show();
			return false;
		}
		var data = new Object();
		if ($('input[name=formatType]:radio:checked').length > 0
				&& $('input[name=formatType]:radio:checked').val() != undefined) {
			data.formatType = $('input[name=formatType]:radio:checked').val()
					.toUpperCase();
		}

		data.customerCode = $('#customerCode').val().trim();
		data.fromDate = $('#fDate').val().trim();
		data.toDate = $('#tDate').val().trim();
		data.staffCode = $('#staffCode').val().trim();
		data.shopId = $('#reportContainer input.combo-value').val().trim();

		$
				.getJSON(
						"/report/are-you-shop?shopId=" + data.shopId,
						function(result) {

							if (!result.isNPP) {
								msg = 'Đơn vị chọn phải là nhà phân phối';
								$('#errMsg').html(msg).show();

							} else {
								var kData = $.param(data, true);
								$('#divOverlay').show();
								$
										.ajax({
											type : "POST",
											url : "/report/sales-revenue/bhtsp/export",
											data : (kData),
											dataType : "json",
											success : function(data) {
												$('#divOverlay').hide();
												if (!data.error) {
													if (data.haveData) {
														window.location.href = data.path;
														setTimeout(
																function() { // Set
																	// timeout
																	// để
																	// đảm
																	// bảo
																	// file
																	// load
																	// lên
																	// hoàn
																	// tất
																	CommonSearch
																			.deleteFileExcelExport(data.path);
																}, 2000);
													} else {
														msg = 'Không có dữ liệu để xuất';

														if (data.validate) {
															msg = data.errMsg;
														}

														$('#errMsg').html(msg)
																.show();
													}
												} else {
													$('#errMsg').html(
															data.errMsg).show();
												}
											},
											error : function(XMLHttpRequest,
													textStatus, errorDivThrown) {
												$('#divOverlay').hide();
												StockIssued.xhrExport = null;
											}
										});
							}

						});

		// ReportUtils.exportReport('/report/sales-revenue/view-dayreport',dataModel);

		return false;
	},

	/**
	 * DS3.11-Bao cao tong hop chi tra hang trung bay
	 * 
	 * @author hunglm16
	 * @since January 17,2014
	 */
	exportBCTHCTHTB_DS311 : function() {
		var msg = '';
		$('#errMsg').html('').hide();

		if (msg.length == 0 && $('.combo-value').val() == undefined
				&& $('.combo-value').val().length == 0) {
			msg = "Chưa chọn Đơn Vị";
		}
		if (msg.length == 0 && $('#isNPP').val().trim() === "false") {
			msg = 'Đơn vị chọn phải là nhà phân phối';
		}
		if (msg.length == 0) {
			msg = Utils.getMessageOfRequireCheck('fullYearIn10', 'Năm');
		}
		if (msg.length > 0) {
			$('#errMsg').html(msg).show();
			return false;
		}
		var dataModel = new Object();
		dataModel.shopId = $('.combo-value').val();
		dataModel.year = encodeChar($('#fullYearIn10').val());
		dataModel.lstDisProCode = $('#multiChoiceCTTB').val();
		dataModel.customerCode = $('#customerCode').val();
		if ($('input[name=formatType]:radio:checked').length > 0
				&& $('input[name=formatType]:radio:checked').val() != undefined) {
			dataModel.formatType = $('input[name=formatType]:radio:checked')
					.val().toUpperCase();
		}
		ReportUtils.exportReport('/report/sales-revenue/DS3-11/export',
				dataModel);
	},

	/**
	 * DS3-Báo cáo danh sách đơn hàng trong ngày theo NVBH
	 * 
	 * @author hunglm16
	 * @since January 25,2014
	 */
	exportBCDSDHTNTNVBH_DS31 : function() {
		var msg = '';
		$('#errMsg').html('').hide();

		if (msg.length == 0 && $('.combo-value').val() == undefined
				&& $('.combo-value').val().length == 0) {
			msg = "Chưa chọn Đơn Vị";
		}
		if (msg.length == 0 && $('#isNPP').val().trim() === "false") {
			msg = 'Đơn vị chọn phải là nhà phân phối';
		}
		if (msg.length == 0) {
			msg = Utils.getMessageOfRequireCheck('fDate', 'Từ ngày');
		}
		if (msg.length == 0) {
			msg = Utils.getMessageOfInvalidFormatDate('fDate', 'Từ ngày');
		}
		if (msg.length == 0) {
			msg = Utils.getMessageOfRequireCheck('tDate', 'Đến ngày');
		}
		if (msg.length == 0) {
			msg = Utils.getMessageOfInvalidFormatDate('tDate', 'Đến ngày');
		}
		var fDate = $('#fDate').val();
		var tDate = $('#tDate').val();
		if (msg.length == 0 && !Utils.compareDate(fDate, tDate)) {
			msg = msgErr_fromdate_greater_todate;
			$('#fDate').focus();
		}
		var currentTime = new Date();
		var month = currentTime.getMonth() + 1;
		var day = currentTime.getDate();
		var year = currentTime.getFullYear();
		if (msg.length == 0
				&& !Utils.compareDate(fDate, day + '/' + month + '/' + year)) {
			msg = 'Từ ngày không được lớn hơn ngày hiện tại';
			$('#fDate').focus();
		}

		if (msg.length > 0) {
			$('#errMsg').html(msg).show();
			return false;
		}

		var dataModel = new Object();
		dataModel.shopId = $('.combo-value').val();
		dataModel.lstStaffCode = $('#staffCode').val().trim();
		dataModel.fromDate = $('#fDate').val().trim();
		dataModel.toDate = $('#tDate').val().trim();
		ReportUtils.exportExcel('/report/sales-revenue/DS3-dsdhtntnvbh/export',
				dataModel, 'errMsg');
	},

	exportTDBH7 : function() {
		var msg = '';
		$('#errMsg').hide();
		msg = Utils.getMessageOfRequireCheck('shopId', 'Đơn vị');
		$('#shop').focus();

		if (msg.length == 0) {
			msg = Utils.getMessageOfRequireCheck('fDate', 'Từ ngày');
		}
		if (msg.length == 0) {
			msg = Utils.getMessageOfRequireCheck('tDate', 'Đến ngày');
		}
		var fDate = $('#fDate').val().trim();
		var tDate = $('#tDate').val().trim();
		if (msg.length == 0 && !Utils.compareCurrentDate(fDate)) {
			msg = 'Từ ngày phải nhỏ hơn hoặc bằng ngày hiện tại';
			$('#fDate').focus();
		}

		if (msg.length == 0 && !Utils.compareDate(fDate, tDate)) {
			msg = 'Từ ngày phải nhỏ hơn hoặc bằng đến ngày';
			$('#fDate').focus();
		}

		if (msg.length > 0) {
			$('#errMsg').html(msg).show();
			return false;
		}

		var dataModel = new Object();
		dataModel.shopId = $('#shopId').val().trim();
		dataModel.fromDate = fDate;
		dataModel.toDate = tDate;
		if ($('input[name=formatType]:radio:checked').length > 0
				&& $('input[name=formatType]:radio:checked').val() != undefined) {
			dataModel.formatType = $('input[name=formatType]:radio:checked').val().toUpperCase();
		}
		ReportUtils.exportReport('/report/sales-revenue/tdbh_7_2_7/export', dataModel);
		return false;
	},

	exportTDBH8 : function() {
		var msg = '';
		$('#errMsg').hide();
		msg = Utils.getMessageOfRequireCheck('shopId', 'Đơn vị');
		$('#shop').focus();

		if (msg.length == 0) {
			msg = Utils.getMessageOfRequireCheck('fDate', 'Từ ngày');
		}
		var fDate = $('#fDate').val().trim();
		if (msg.length == 0 && !Utils.compareCurrentDate(fDate)) {
			msg = 'Từ ngày phải nhỏ hơn hoặc bằng ngày hiện tại';
			$('#fDate').focus();
		}

		if (msg.length > 0) {
			$('#errMsg').html(msg).show();
			return false;
		}

		var dataModel = new Object();
		dataModel.shopId = $('#shopId').val().trim();
		dataModel.staffCode = $('#staffCode').val().trim();
		dataModel.fromDate = fDate;

		if ($('input[name=formatType]:radio:checked').length > 0
				&& $('input[name=formatType]:radio:checked').val() != undefined) {
			dataModel.formatType = $('input[name=formatType]:radio:checked')
					.val().toUpperCase();
		}
		ReportUtils.exportReport('/report/sales-revenue/tdbh_7_2_8/export', dataModel);
		return false;
	},
	
	exportTDTTCN: function() {
		var msg = '';
		$('#errMsg').hide();

		if (msg.length == 0) {
			msg = Utils.getMessageOfRequireCheck('fDate', 'Từ ngày');
		}
		if (msg.length == 0) {
			msg = Utils.getMessageOfInvalidFormatDate('fDate', 'Từ ngày');
		}
		if (msg.length == 0) {
			msg = Utils.getMessageOfRequireCheck('tDate', 'Đến ngày');
		}
		if (msg.length == 0) {
			msg = Utils.getMessageOfInvalidFormatDate('tDate', 'Đến ngày');
		}
		var fDate = $('#fDate').val().trim();
		var tDate = $('#tDate').val().trim();
		if (msg.length == 0 && !Utils.compareCurrentDate(fDate)) {
			msg = 'Từ ngày phải nhỏ hơn hoặc bằng ngày hiện tại';
			$('#fDate').focus();
		}

		if (msg.length == 0 && !Utils.compareDate(fDate, tDate)) {
			msg = 'Từ ngày phải nhỏ hơn hoặc bằng đến ngày';
			$('#fDate').focus();
		}

		if (msg.length > 0) {
			$('#errMsg').html(msg).show();
			return false;
		}

		var dataModel = new Object();
		dataModel.type = $('#type').val().trim();
		dataModel.fromDate = fDate;
		dataModel.toDate = tDate;
		dataModel.lstNVBHCode = $('#staffCode').val().trim();
		dataModel.lstNVGHCode = $('#delivery').val().trim();
		dataModel.lstCustomerCode = $('#customerCode').val().trim();
		dataModel.checkExport = $('#checkExport').val().trim();
		
		ReportUtils.exportReport('/report/sales-revenue/tdbh-7_2_5/export', dataModel);
	},
	exportPTDTBHNPP: function(){
		
		var msg = '';
		var multiShop = $("#shop").data("kendoMultiSelect");
		var dataShop = multiShop.dataItems();
		$('#errMsg').html('').hide();
		if(dataShop==null||dataShop.length==0){
			msg = format(msgErr_required_field,'Đơn vị');
		}
		var fDate = $('#fDate').val();
		var tDate = $('#tDate').val();
		if(msg.length == 0){
			msg = Utils.getMessageOfRequireCheck('fDate','Từ ngày');
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfInvalidFormatDate('fDate','Từ ngày');
		}
		if(msg.length == 0){
			if(!Utils.compareCurrentDate(fDate)){
				msg = "Từ ngày phải bé hơn hoặc bằng ngày hiện tại.";
			}
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfRequireCheck('tDate','Đến ngày');
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfInvalidFormatDate('tDate','Đến ngày');
		}
		if(msg.length == 0){
			if(!Utils.compareCurrentDate(tDate)){
				msg = "Đến ngày phải bé hơn hoặc bằng ngày hiện tại.";
			}
		}
		if(msg.length == 0){			
			if(!Utils.compareDate(fDate, tDate)){
				msg = msgErr_fromdate_greater_todate;
			}
		}
		
		if(msg.length > 0){
			$('#errMsg').html(msg).show();
			return false;
		}
		var dataModel = new Object();
		if(dataShop!=null && dataShop.length>0){
			var lstShop = dataShop[0].id;
			for(var i=1;i<dataShop.length;i++){
				lstShop += "," + dataShop[i].id;
			}
			dataModel.strListShopId = lstShop;
		}
		//dataModel.shopId = $('#shopId').val().trim();				
		if(dataShop!=null && dataShop.length>0){
			var lstShop = dataShop[0].id;
			for(var i=1;i<dataShop.length;i++){
				lstShop += "," + dataShop[i].id;
			}
			dataModel.listShopId = lstShop;
		}
		dataModel.fromDate = $('#fDate').val().trim();
		dataModel.toDate = $('#tDate').val().trim();
		dataModel.shopCode = $('#shopCode').val().trim();		
		ReportUtils.exportReport('/report/sales-revenue/tdbh-7_2_12/export',dataModel);
	},
	/**
	 * @author sangtn
	 * @since 13/05/2014
	 * @description Xuat bao cao 7.2.13. Phan tich doanh thu ban hang theo NVBH 
	 * */
	export7_2_13: function() {
		var msg = '';
		var multiShop = $("#shop").data("kendoMultiSelect");
		var dataShop = multiShop.dataItems();
		$('#errMsg').html('').hide();
		if(dataShop==null||dataShop.length==0){
			msg = format(msgErr_required_field,'Đơn vị');
		}
		var fDate = $('#fDate').val();
		var tDate = $('#tDate').val();
		if(msg.length == 0){
			msg = Utils.getMessageOfRequireCheck('fDate','Từ ngày');
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfInvalidFormatDate('fDate','Từ ngày');
		}
		if(msg.length == 0){
			if(!Utils.compareCurrentDate(fDate)){
				msg = "Từ ngày phải bé hơn hoặc bằng ngày hiện tại.";
			}
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfRequireCheck('tDate','Đến ngày');
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfInvalidFormatDate('tDate','Đến ngày');
		}
		if(msg.length == 0){
			if(!Utils.compareCurrentDate(tDate)){
				msg = "Đến ngày phải bé hơn hoặc bằng ngày hiện tại.";
			}
		}
		if(msg.length == 0){			
			if(!Utils.compareDate(fDate, tDate)){
				msg = msgErr_fromdate_greater_todate;
			}
		}
		
		if(msg.length > 0){
			$('#errMsg').html(msg).show();
			return false;
		}
		var dataModel = new Object();
		if (dataShop != null && dataShop.length > 0) {
			var lstShop = dataShop[0].shopId;
			for (var i = 1; i < dataShop.length; i++) {
				lstShop += "," + dataShop[i].shopId;
			}
			dataModel.strListShopId = lstShop;
		}
		dataModel.fromDate = $('#fDate').val().trim();
		dataModel.toDate = $('#tDate').val().trim();
		//dataModel.shopCode = $('#shopCode').val().trim();
		ReportUtils.exportReport('/report/sales-revenue/tdbh-7_2_13/export', dataModel);
	},
	/**
	 * @author vuonghn
	 * @since 13/05/2014
	 * @description Xuat bao cao 7.2.14. Phan tich doanh thu ban hang theo NVBH 
	 * */
	export7_2_14: function() {
		var msg = '';
		var multiShop = $("#shop").data("kendoMultiSelect");
		var dataShop = multiShop.dataItems();
		$('#errMsg').html('').hide();
		if(dataShop==null||dataShop.length==0){
			msg = format(msgErr_required_field,'Đơn vị');
		}
		var fDate = $('#fDate').val();
		var tDate = $('#tDate').val();
		if(msg.length == 0){
			msg = Utils.getMessageOfRequireCheck('fDate','Từ ngày');
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfInvalidFormatDate('fDate','Từ ngày');
		}
		if(msg.length == 0){
			if(!Utils.compareCurrentDate(fDate)){
				msg = "Từ ngày phải bé hơn hoặc bằng ngày hiện tại.";
			}
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfRequireCheck('tDate','Đến ngày');
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfInvalidFormatDate('tDate','Đến ngày');
		}
		if(msg.length == 0){
			if(!Utils.compareCurrentDate(tDate)){
				msg = "Đến ngày phải bé hơn hoặc bằng ngày hiện tại.";
			}
		}
		if(msg.length == 0){			
			if(!Utils.compareDate(fDate, tDate)){
				msg = msgErr_fromdate_greater_todate;
			}
		}
		
		if(msg.length > 0){
			$('#errMsg').html(msg).show();
			return false;
		}
		var dataModel = new Object();
		if(dataShop!=null && dataShop.length>0){
			var lstShop = dataShop[0].shopId;
			for(var i=1;i<dataShop.length;i++){
				lstShop += "," + dataShop[i].shopId;
			}
			dataModel.strListShopId = lstShop;
		}
		dataModel.fromDate = $('#fDate').val().trim();
		dataModel.toDate = $('#tDate').val().trim();
		ReportUtils.exportReport('/report/sales-revenue/tdbh-7_2_14/export', dataModel);
	},
	// vuongmq: PTDTBHSKU 7_2_15
	exportPTDTBHSKU7_1_15: function() {
		var msg = '';
		var multiShop = $("#shop").data("kendoMultiSelect");
		var dataShop = multiShop.dataItems();
		$('#errMsg').html('').hide();
		if(dataShop==null||dataShop.length==0){
			msg = format(msgErr_required_field,'Đơn vị');
		}
		$('#shop').focus();
		if (msg.length == 0) {
			msg = Utils.getMessageOfRequireCheck('fromDate', 'Từ ngày');
		}
		if (msg.length == 0) {
			msg = Utils.getMessageOfRequireCheck('toDate', 'Đến ngày');
		}
		if (msg.length == 0) {
			msg = Utils.getMessageOfInvalidFormatDate('fromDate', 'Từ ngày');
		}
		if (msg.length == 0) {
			msg = Utils.getMessageOfInvalidFormatDate('toDate', 'Từ ngày');
		}
		var fDate = $('#fromDate').val().trim();
		var tDate = $('#toDate').val().trim();
		if (msg.length == 0 && !Utils.compareCurrentDate(fDate)) {
			msg = 'Từ ngày phải nhỏ hơn hoặc bằng ngày hiện tại';
			$('#fDate').focus();
		}

		if (msg.length == 0 && !Utils.compareDate(fDate, tDate)) {
			msg = 'Từ ngày phải nhỏ hơn hoặc bằng đến ngày';
			$('#fDate').focus();
		}

		if (msg.length > 0) {
			$('#errMsg').html(msg).show();
			return false;
		}

		var dataModel = new Object();
		if(dataShop!=null && dataShop.length>0){
			var lstShop = dataShop[0].shopId;
			for(var i=1;i<dataShop.length;i++){
				lstShop += "," + dataShop[i].shopId;
			}
			dataModel.strListShopId = lstShop;
		}
		dataModel.fromDate = fDate;
		dataModel.toDate = tDate;
		ReportUtils.exportReport('/report/sales-revenue/ptdtbh-sku_7_2_15/export', dataModel);
	},
	// vuongmq:  vuongmq: 13/08/2013, bao cao don hang tra theo gia tri
	exportBCDHT_THEOGIATRI: function() {
		var msg = '';
		var multiShop = $("#shop").data("kendoMultiSelect");
		var dataShop = multiShop.dataItems();
		$('#errMsg').html('').hide();
		if(dataShop==null||dataShop.length==0){
			msg = format(msgErr_required_field,'Đơn vị');
		}
		$('#shop').focus();
		if (msg.length == 0) {
			msg = Utils.getMessageOfRequireCheck('fromDate', 'Từ ngày');
		}
		if (msg.length == 0) {
			msg = Utils.getMessageOfRequireCheck('toDate', 'Đến ngày');
		}
		if (msg.length == 0) {
			msg = Utils.getMessageOfInvalidFormatDate('fromDate', 'Từ ngày');
		}
		if (msg.length == 0) {
			msg = Utils.getMessageOfInvalidFormatDate('toDate', 'Từ ngày');
		}
		var fDate = $('#fromDate').val().trim();
		var tDate = $('#toDate').val().trim();
		if (msg.length == 0 && !Utils.compareCurrentDate(fDate)) {
			msg = 'Từ ngày phải nhỏ hơn hoặc bằng ngày hiện tại';
			$('#fDate').focus();
		}

		if (msg.length == 0 && !Utils.compareDate(fDate, tDate)) {
			msg = 'Từ ngày phải nhỏ hơn hoặc bằng đến ngày';
			$('#fDate').focus();
		}

		if (msg.length > 0) {
			$('#errMsg').html(msg).show();
			return false;
		}

		var dataModel = new Object();
		if(dataShop!=null && dataShop.length>0){
			var lstShop = dataShop[0].id;
			for(var i=1;i<dataShop.length;i++){
				lstShop += "," + dataShop[i].id;
			}
			dataModel.strListShopId = lstShop;
		}
		dataModel.fromDate = fDate;
		dataModel.toDate = tDate;
		//ReportUtils.exportReport('/report/sales-revenue/bcdht_theogiatri/export', dataModel);
		ReportUtils.exportReport('/report/sales-revenue/bcdht_theogiatri/export', dataModel);
	},
	// vuongmq: vuongmq: 13/08/2013, bao cao don hang tra theo tong dong hang
	exportBCDHT_THEOTONGDONHANG: function() {
		var msg = '';
		var multiShop = $("#shop").data("kendoMultiSelect");
		var dataShop = multiShop.dataItems();
		$('#errMsg').html('').hide();
		if(dataShop==null||dataShop.length==0){
			msg = format(msgErr_required_field,'Đơn vị');
		}
		$('#shop').focus();
		if (msg.length == 0) {
			msg = Utils.getMessageOfRequireCheck('fromDate', 'Từ ngày');
		}
		if (msg.length == 0) {
			msg = Utils.getMessageOfRequireCheck('toDate', 'Đến ngày');
		}
		if (msg.length == 0) {
			msg = Utils.getMessageOfInvalidFormatDate('fromDate', 'Từ ngày');
		}
		if (msg.length == 0) {
			msg = Utils.getMessageOfInvalidFormatDate('toDate', 'Từ ngày');
		}
		var fDate = $('#fromDate').val().trim();
		var tDate = $('#toDate').val().trim();
		if (msg.length == 0 && !Utils.compareCurrentDate(fDate)) {
			msg = 'Từ ngày phải nhỏ hơn hoặc bằng ngày hiện tại';
			$('#fDate').focus();
		}

		if (msg.length == 0 && !Utils.compareDate(fDate, tDate)) {
			msg = 'Từ ngày phải nhỏ hơn hoặc bằng đến ngày';
			$('#fDate').focus();
		}

		if (msg.length > 0) {
			$('#errMsg').html(msg).show();
			return false;
		}

		var dataModel = new Object();
		if(dataShop!=null && dataShop.length>0){
			var lstShop = dataShop[0].id;
			for(var i=1;i<dataShop.length;i++){
				lstShop += "," + dataShop[i].id;
			}
			dataModel.strListShopId = lstShop;
		}
		dataModel.fromDate = fDate;
		dataModel.toDate = tDate;
		//ReportUtils.exportReport('/report/sales-revenue/bcdht_theotongdonhang/export', dataModel);
		ReportUtils.exportReport('/report/sales-revenue/bcdht_theotongdonhang/export', dataModel);
	}
};
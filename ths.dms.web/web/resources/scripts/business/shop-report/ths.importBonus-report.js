//HungTT27
var ImportBonus = {
		reportImportBonus: function(){
			var msg = '';
			$('#errMsg').html('').hide();
			if(msg.length == 0){
				msg = Utils.getMessageOfRequireCheck('fDate', 'Từ ngày');
			}
			if(msg.length == 0){
				msg = Utils.getMessageOfInvalidFormatDate('fDate','Từ ngày');
			}
			if(msg.length == 0){
				msg = Utils.getMessageOfRequireCheck('tDate','Đến ngày');
			}
			if(msg.length == 0){
				msg = Utils.getMessageOfInvalidFormatDate('tDate','Đến ngày');
			}
			var fDate = $('#fDate').val();
			var tDate = $('#tDate').val();
			if(!Utils.compareDate(fDate, tDate)){
				msg = msgErr_fromdate_greater_todate;		
				$('#fDate').focus();
			}
			var ccDate = getCurrentDate();		
			if(!Utils.compareDate(tDate, ccDate)){
				msg = 'Đến ngày không được lớn hơn ngày hiện tại';		
				$('#tDate').focus();
			}
			if(msg.length > 0){
				$('#errMsg').html(msg).show();
				return false;
			}
			var dataModel = new Object();		
			dataModel.fromDate = $('#fDate').val().trim();
			dataModel.toDate = $('#tDate').val().trim();
			ReportUtils.exportReport('/report/importBonusAndSaleGroup/view',dataModel);
		}
};
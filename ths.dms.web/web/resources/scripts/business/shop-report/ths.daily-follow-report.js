var DailyFollowReport = {
	
	reportDeliveryTicketAdd: function(){
		var msg = '';
		$('#errMsg').html('').hide();
//		if($('#shopCode').val().trim().length == 0 || $('#shopCode').val().trim() == activeType.ALL){
//			msg = format(msgErr_required_field,'đơn vị');
//		}
		if(msg.length == 0){
			msg = Utils.getMessageOfInvalidFormatDate('fDate','Từ ngày');
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfInvalidFormatDate('tDate','Đến ngày');
		}
		var fDate = $('#fDate').val();
		var tDate = $('#tDate').val();
		if(!Utils.compareDate(fDate, tDate)){
			msg = msgErr_fromdate_greater_todate;		
			$('#fDate').focus();
		}
		if(msg.length > 0){
			$('#errMsg').html(msg).show();
			return false;
		}
		var dataModel = new Object();		
		dataModel.shopCode = $('#shopCode').val().trim();
		dataModel.fromDate = $('#fDate').val().trim();
		dataModel.toDate = $('#tDate').val().trim();
		dataModel.staffCode = $('#staffCode').val().trim();
		ReportUtils.exportReport('/report/daily-follow/delivey-ticket-pay-sum/export', dataModel);
	},
	//7.1.2
	exportBKDHNVGH: function(){
		var msg = '';
		$('#errMsg').html('').hide();
		if($('#shopId').val().trim().length == 0 || $('#shopId').val().trim() == activeType.ALL){
			msg = format(msgErr_required_field,'Đơn vị');
		}
		if(msg.length == 0){
			if($('#isNPP').val() == "false"){
				msg = "Đơn vị bạn vừa chọn không phải là 1 nhà phân phối cụ thể!";
			}
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfRequireCheck('fDate','Từ ngày');
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfInvalidFormatDateNew('fDate','Từ ngày');
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfRequireCheck('tDate','Đến ngày');
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfInvalidFormatDateNew('tDate','Đến ngày');
		}
		var fDate = $('#fDate').val();
		var tDate = $('#tDate').val();
		if(!Utils.compareDateNew(fDate, tDate)){
			msg = msgErr_fromdate_greater_todate;		
			$('#fDate').focus();
		}
		var currentTime = new Date();
		var month = currentTime.getMonth() + 1;
		var day = currentTime.getDate();
		var year = currentTime.getFullYear();
		var hour = currentTime.getHours();
		var minute = currentTime.getMinutes();
		if(month < 10){
			month = '0' + month;
		}
		if(day < 10){
			day = '0' + day;
		}
		if(hour < 10){
			hour = '0' + hour;
		}
		if(minute < 10){
			minute = '0' + minute;
		}
		if(!Utils.compareDateNew(fDate,day + '/' + month + '/' + year + ' ' + hour + ':' + minute)){
			msg = 'Từ ngày không được lớn hơn ngày hiện tại';
			$('#fDate').focus();
		}
		if($('#isPrint').val().trim() == 1){
			/*if(msg.length == 0){
				msg = Utils.getMessageOfRequireCheck('fromHour','Từ giờ');
			}
			
			if(msg.length == 0){
				msg = Utils.getMessageOfRequireCheck('toHour','Đến giờ');
			}
			
			if(msg.length == 0){
				msg = Utils.getMessageOfSpecialCharactersValidateEx('fromHour','Từ giờ',Utils._TF_NUMBER);
			}

			if(msg.length == 0){
				msg = Utils.getMessageOfSpecialCharactersValidateEx('toHour','Đến giờ',Utils._TF_NUMBER);
			}
			
			if ($('#fromHour').val()!=null){
				if (parseInt($('#fromHour').val())< 0 || parseInt($('#fromHour').val()) > 23)
				{
					$('#errMsg').html('Giá trị giờ nằm trong khoảng [0-23]. Mời bạn nhập lại.').show();
					$('#fromHour').focus();
					return false;
				}
			}
			
			if ($('#toHour').val()!=null){
				if (parseInt($('#toHour').val())< 0 || parseInt($('#toHour').val()) > 23)
				{
					$('#errMsg').html('Giá trị giờ nằm trong khoảng [0-23]. Mời bạn nhập lại.').show();
					$('#toHour').focus();
					return false;
				}
			}*/
		}
		if(msg.length > 0){
			$('#errMsg').html(msg).show();
			return false;
		}
		var dataModel = new Object();		
		if($('input[name=formatType]:radio:checked').length > 0 && $('input[name=formatType]:radio:checked').val() != undefined) {
            dataModel.formatType = $('input[name=formatType]:radio:checked').val().toUpperCase();
		}
		dataModel.staffCode = $('#staffCode').val().trim();
		dataModel.staffSaleCode = $('#staffSaleCode').val().trim();
		dataModel.isPrint = $('#isPrint').val().trim();
		dataModel.shopId = $('#shopId').val().trim();
		dataModel.fromDate = $('#fDate').val().trim();
		dataModel.toDate = $('#tDate').val().trim();
		//dataModel.fromHour = $('#fromHour').val().trim();
		//dataModel.toHour = $('#toHour').val().trim();
		dataModel.saleOrderNumber = $('#saleOrderNumber').val().trim();
		ReportUtils.exportReport('/report/daily-follow/bkdhnvgh/export',dataModel);
	},
	//7.1.2a
	exportBKDHNVGH_A: function(){
		var msg = '';
		$('#errMsg').html('').hide();
		if($('#shopId').val().trim().length == 0 || $('#shopId').val().trim() == activeType.ALL){
			msg = format(msgErr_required_field,'Đơn vị');
		}
		if(msg.length == 0){
			if($('#isNPP').val() == "false"){
				msg = "Đơn vị bạn vừa chọn không phải là 1 nhà phân phối cụ thể!";
			}
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfRequireCheck('fDate','Từ ngày');
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfInvalidFormatDate('fDate','Từ ngày');
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfRequireCheck('tDate','Đến ngày');
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfInvalidFormatDate('tDate','Đến ngày');
		}
		var fDate = $('#fDate').val();
		var tDate = $('#tDate').val();
		if(!Utils.compareDate(fDate, tDate)){
			msg = msgErr_fromdate_greater_todate;		
			$('#fDate').focus();
		}
		var currentTime = new Date();
		var month = currentTime.getMonth() + 1;
		var day = currentTime.getDate();
		var year = currentTime.getFullYear();
		if(!Utils.compareDate(fDate,day + '/' + month + '/' + year)){
			msg = 'Từ ngày không được lớn hơn ngày hiện tại';
			$('#fDate').focus();
		}
		if(msg.length > 0){
			$('#errMsg').html(msg).show();
			return false;
		}
		var dataModel = new Object();		
		if($('input[name=formatType]:radio:checked').length > 0 && $('input[name=formatType]:radio:checked').val() != undefined) {
            dataModel.formatType = $('input[name=formatType]:radio:checked').val().toUpperCase();
		}
		dataModel.staffCode = $('#staffCode').val().trim();
		dataModel.staffSaleCode = $('#staffSaleCode').val().trim();
		dataModel.shopId = $('#shopId').val().trim();
		dataModel.fromDate = $('#fDate').val().trim();
		dataModel.toDate = $('#tDate').val().trim();
		dataModel.saleOrderNumber = $('#saleOrderNumber').val().trim();
		dataModel.notApproved = '1';
		ReportUtils.exportReport('/report/daily-follow/bkdhnvgh/export', dataModel);
	},
	exportPGNTTG: function(){
		var msg = '';
		$('#errMsg').html('').hide();
		if($('#shopId').val().trim().length == 0 || $('#shopId').val().trim() == activeType.ALL){
			msg = format(msgErr_required_field,'đơn vị');
		}
		if(msg.length == 0){
			if($('#isNPP').val() == "false"){
				msg = "Đơn vị bạn vừa chọn không phải là 1 nhà phân phối cụ thể!";
			}
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfRequireCheck('fDate','Từ ngày');
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfInvalidFormatDate('fDate','Từ ngày');
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfRequireCheck('tDate','Đến ngày');
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfInvalidFormatDate('tDate','Đến ngày');
		}
		var fDate = $('#fDate').val();
		var tDate = $('#tDate').val();
		if(!Utils.compareDate(fDate, tDate)){
			msg = msgErr_fromdate_greater_todate;		
			$('#fDate').focus();
		}
		var currentTime = new Date();
		var month = currentTime.getMonth() + 1;
		var day = currentTime.getDate();
		var year = currentTime.getFullYear();
		if(!Utils.compareDate(fDate,day + '/' + month + '/' + year)){
			msg = 'Từ ngày không được lớn hơn ngày hiện tại';
			$('#fDate').focus();
		}
		
		if(msg.length > 0){
			$('#errMsg').html(msg).show();
			return false;
		}
		var dataModel = new Object();		
		dataModel.staffCode = $('#staffCode').val().trim();
		dataModel.shopId = $('#shopId').val().trim();
		dataModel.fromDate = $('#fDate').val().trim();
		dataModel.toDate = $('#tDate').val().trim();
		if($('input[name=formatType]:radio:checked').length > 0 && $('input[name=formatType]:radio:checked').val() != undefined) {
            dataModel.formatType = $('input[name=formatType]:radio:checked').val().toUpperCase();
		}
		ReportUtils.exportReport('/report/daily-follow/pgnttg/export',dataModel);
	},
	//7.1.1
	exportPXHTNVGH : function(){
		var msg = '';
		$('#errMsg').html('').hide();
		if ($('#shopId').val().trim().length == 0 || $('#shopId').val().trim() == activeType.ALL) {
			msg = format(msgErr_required_field,'đơn vị');
		}
		if (msg.length == 0) {
			if ($('#isNPP').val() == "false") {
				msg = "Đơn vị bạn vừa chọn không phải là 1 nhà phân phối cụ thể!";
			}
		}
		if (msg.length == 0) {
			msg = Utils.getMessageOfRequireCheck('fDate','Từ ngày');
		}
		if (msg.length == 0) {
			msg = Utils.getMessageOfInvalidFormatDateNew('fDate','Từ ngày');
		}
		if (msg.length == 0) {
			msg = Utils.getMessageOfRequireCheck('tDate','Đến ngày');
		}
		if (msg.length == 0) {
			msg = Utils.getMessageOfInvalidFormatDateNew('tDate','Đến ngày');
		}
		var fDate = $('#fDate').val();
		var tDate = $('#tDate').val();
		if (!Utils.compareDateNew(fDate, tDate)) {
			msg = msgErr_fromdate_greater_todate;		
			$('#fDate').focus();
		}
		
		var currentTime = new Date();
		var month = currentTime.getMonth() + 1;
		var day = currentTime.getDate();
		var year = currentTime.getFullYear();
		var hour = currentTime.getHours();
		var minute = currentTime.getMinutes();
		if(month < 10){
			month = '0' + month;
		}
		if(day < 10){
			day = '0' + day;
		}
		if(hour < 10){
			hour = '0' + hour;
		}
		if(minute < 10){
			minute = '0' + minute;
		}
		if(!Utils.compareDateNew(fDate,day + '/' + month + '/' + year + ' ' + hour + ':' + minute)){
			msg = 'Từ ngày không được lớn hơn ngày hiện tại';
			$('#fDate').focus();
		}
		
		if ($('#isPrint').val().trim() == 1) {
			/*if (msg.length == 0) {
				msg = Utils.getMessageOfRequireCheck('fromHour','Từ giờ');
			}
			
			if (msg.length == 0) {
				msg = Utils.getMessageOfRequireCheck('toHour','Đến giờ');
			}
			
			if(msg.length == 0) {
				msg = Utils.getMessageOfSpecialCharactersValidateEx('fromHour','Từ giờ',Utils._TF_NUMBER);
			}

			if(msg.length == 0) {
				msg = Utils.getMessageOfSpecialCharactersValidateEx('toHour','Đến giờ',Utils._TF_NUMBER);
			}
			
			if ($('#fromHour').val()!=null) {
				if (parseInt($('#fromHour').val())< 0 || parseInt($('#fromHour').val()) > 23) {
					$('#errMsg').html('Giá trị giờ nằm trong khoảng [0-23]. Mời bạn nhập lại.').show();
					$('#fromHour').focus();
					return false;
				}
			}
			
			if ($('#toHour').val()!=null) {
				if (parseInt($('#toHour').val())< 0 || parseInt($('#toHour').val()) > 23) {
					$('#errMsg').html('Giá trị giờ nằm trong khoảng [0-23]. Mời bạn nhập lại.').show();
					$('#toHour').focus();
					return false;
				}
			}*/
		}
		if (msg.length > 0) {
			$('#errMsg').html(msg).show();
			return false;
		}
		var saleOrderNumber = $('#saleOrderNumber').val().trim();
		if (saleOrderNumber.length>0) {
			saleOrderNumber = saleOrderNumber.replace(/;/g, ",");
		}
		
		var dataModel = new Object();		
		dataModel.shopId = $('#shopId').val().trim();
		dataModel.staffSaleCode = $('#staffSaleCode').val().trim();
		dataModel.isPrint = $('#isPrint').val().trim();
		dataModel.fromDate = $('#fDate').val().trim();
		dataModel.deliveryCode = $('#staffCode').val().trim();
		dataModel.toDate = $('#tDate').val().trim();
		//dataModel.fromHour = $('#fromHour').val().trim();
		//dataModel.toHour = $('#toHour').val().trim();
		dataModel.lstSaleOderNumberStr = saleOrderNumber;
		if ($('input[name=formatType]:radio:checked').length > 0 && $('input[name=formatType]:radio:checked').val() != undefined) {
            dataModel.formatType = $('input[name=formatType]:radio:checked').val().toUpperCase();
		}
		ReportUtils.exportReport('/report/daily-follow/pxhtnvgh/export', dataModel, 'errMsg');
	},
	//7.1.1a
	exportPXHTNVGH_A: function(){		
		var msg = '';
		$('#errMsg').html('').hide();
		if($('#shopId').val().trim().length == 0 || $('#shopId').val().trim() == activeType.ALL){
			msg = format(msgErr_required_field,'đơn vị');
		}
		if(msg.length == 0){
			if($('#isNPP').val() == "false"){
				msg = "Đơn vị bạn vừa chọn không phải là 1 nhà phân phối cụ thể!";
			}
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfRequireCheck('fDate','Từ ngày');
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfInvalidFormatDate('fDate','Từ ngày');
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfRequireCheck('tDate','Đến ngày');
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfInvalidFormatDate('tDate','Đến ngày');
		}
		var fDate = $('#fDate').val();
		var tDate = $('#tDate').val();
		if(!Utils.compareDate(fDate, tDate)){
			msg = msgErr_fromdate_greater_todate;		
			$('#fDate').focus();
		}
		
		var currentTime = new Date();
		var month = currentTime.getMonth() + 1;
		var day = currentTime.getDate();
		var year = currentTime.getFullYear();
		if(!Utils.compareDate(fDate,day + '/' + month + '/' + year)){
			msg = 'Từ ngày không được lớn hơn ngày hiện tại';
			$('#fDate').focus();
		}
		
		if(msg.length > 0){
			$('#errMsg').html(msg).show();
			return false;
		}
		var saleOrderNumber = $('#saleOrderNumber').val().trim();
		if(saleOrderNumber.length>0){
			saleOrderNumber = saleOrderNumber.replace(/;/g, ",");
		}
		
		var dataModel = new Object();		
		dataModel.shopId = $('#shopId').val().trim();
		dataModel.staffSaleCode = $('#staffSaleCode').val().trim();
		dataModel.fromDate = $('#fDate').val().trim();
		dataModel.deliveryCode = $('#staffCode').val().trim();
		dataModel.toDate = $('#tDate').val().trim();
		dataModel.lstSaleOderNumberStr = saleOrderNumber;
		if($('input[name=formatType]:radio:checked').length > 0 && $('input[name=formatType]:radio:checked').val() != undefined) {
            dataModel.formatType = $('input[name=formatType]:radio:checked').val().toUpperCase();
		}
		dataModel.notApproved = '1';
		ReportUtils.exportReport('/report/daily-follow/pxhtnvgh/export',dataModel);
	},
	/**
	 * @author sangtn
	 * @since 23/05/2014
	 * @description Phiếu xuất hàng theo nhân viên bán hàng 7.1.7 
	 * */
	exportPXHTNVBH : function(){
		var msg = '';
		$('#errMsg').html('').hide();
		if($('#shopId').val().trim().length == 0 || $('#shopId').val().trim() == activeType.ALL){
			msg = format(msgErr_required_field,'đơn vị');
		}
		if(msg.length == 0){
			if($('#isNPP').val() == "false"){
				msg = "Đơn vị bạn vừa chọn không phải là 1 nhà phân phối cụ thể!";
			}
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfRequireCheck('fDate','Từ ngày');
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfInvalidFormatDate('fDate','Từ ngày');
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfRequireCheck('tDate','Đến ngày');
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfInvalidFormatDate('tDate','Đến ngày');
		}
		var fDate = $('#fDate').val();
		var tDate = $('#tDate').val();
		if(!Utils.compareDate(fDate, tDate)){
			msg = msgErr_fromdate_greater_todate;		
			$('#fDate').focus();
		}
		
		var currentTime = new Date();
		var month = currentTime.getMonth() + 1;
		var day = currentTime.getDate();
		var year = currentTime.getFullYear();
		if(!Utils.compareDate(fDate,day + '/' + month + '/' + year)){
			msg = 'Từ ngày không được lớn hơn ngày hiện tại';
			$('#fDate').focus();
		}
		
		if(msg.length > 0){
			$('#errMsg').html(msg).show();
			return false;
		}
		var saleOrderNumber = $('#saleOrderNumber').val().trim();
		if(saleOrderNumber.length>0){
			saleOrderNumber = saleOrderNumber.replace(/;/g, ",");
		}
		var dataModel = new Object();		
		dataModel.shopId = $('#shopId').val().trim();
		dataModel.staffSaleCode = $('#staffSaleCode').val().trim();
		dataModel.fromDate = $('#fDate').val().trim();
		dataModel.deliveryCode = $('#staffCode').val().trim();
		dataModel.toDate = $('#tDate').val().trim();
		dataModel.lstSaleOderNumberStr = saleOrderNumber;
		if($('input[name=formatType]:radio:checked').length > 0 && $('input[name=formatType]:radio:checked').val() != undefined) {
            dataModel.formatType = $('input[name=formatType]:radio:checked').val().toUpperCase();
		}

		ReportUtils.exportReport('/report/daily-follow/pxhtnvbh/export',dataModel);
	},
	/* PHIEU TRA HANG THEO NVGH */
	reportPTHNVGH : function(){
		var msg = '';
		$('#errMsg').html('').hide();
		if($('#shopId').val().trim().length == 0 || $('#shopId').val().trim() == activeType.ALL){
			msg = format(msgErr_required_field,'đơn vị');
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfSpecialCharactersValidate('deliveryCode','Mã nhân viên giao hàng',Utils._CODE);
		}

		if(msg.length == 0){
			if($('#isNPP').val() == "false"){
				msg = "Đơn vị bạn vừa chọn không phải là 1 nhà phân phối cụ thể!";
			}
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfRequireCheck('fDate','Từ ngày');
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfInvalidFormatDate('fDate','Từ ngày');
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfRequireCheck('tDate','Đến ngày');
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfInvalidFormatDate('tDate','Đến ngày');
		}
		var fDate = $('#fDate').val();
		var tDate = $('#tDate').val();
		if(!Utils.compareDate(fDate, tDate)){
			msg = msgErr_fromdate_greater_todate;		
			$('#fDate').focus();
		}
		
		var currentTime = new Date();
		var month = currentTime.getMonth() + 1;
		var day = currentTime.getDate();
		var year = currentTime.getFullYear();
		if(!Utils.compareDate(fDate,day + '/' + month + '/' + year)){
			msg = 'Từ ngày không được lớn hơn ngày hiện tại';
			$('#fDate').focus();
		}
		
		if(msg.length > 0){
			$('#errMsg').html(msg).show();
			return false;
		}
		var dataModel = new Object();		
		dataModel.shopId = $('#shopId').val().trim();
		dataModel.deliveryCode = $('#deliveryCode').val().trim();
		dataModel.fromDate = $('#fDate').val().trim();
		dataModel.toDate = $('#tDate').val().trim();
		if($('input[name=formatType]:radio:checked').length > 0 && $('input[name=formatType]:radio:checked').val() != undefined) {
            dataModel.formatType = $('input[name=formatType]:radio:checked').val().toUpperCase();
		}

		ReportUtils.exportReport('/report/daily-follow/pthnvgh/export',dataModel);
	},
	reportBKPTHNVGH : function(){
		var msg = '';
		$('#errMsg').html('').hide();
		if($('#shopId').val().trim().length == 0 || $('#shopId').val().trim() == activeType.ALL){
			msg = format(msgErr_required_field,'đơn vị');
		}
		if(msg.length == 0) {
			if($('#isNPP').val() == "false") {
				msg = "Đơn vị bạn vừa chọn không phải là 1 nhà phân phối cụ thể!";
			}
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfInvalidFormatDate('fDate','Từ ngày');
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfInvalidFormatDate('tDate','Đến ngày');
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfEmptyDate('fDate','Từ ngày');
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfEmptyDate('tDate','Đến ngày');
		}
		if(msg.length > 0){
			$('#errMsg').html(msg).show();
			return false;
		}
		var fDate = $('#fDate').val();
		var tDate = $('#tDate').val();
		var cDate = ReportUtils.getCurrentDateString();
		if(!Utils.compareDate(fDate, cDate)){
			msg = msgErr_fromdate_greater_currentdate;		
			$('#fDate').focus();
		}
		if(!Utils.compareDate(fDate, tDate)){
			msg = msgErr_fromdate_greater_todate;		
			$('#fDate').focus();
		}
		if(msg.length > 0){
			$('#errMsg').html(msg).show();
			return false;
		}
		var dataModel = new Object();		
		if($('input[name=formatType]:radio:checked').length > 0 && $('input[name=formatType]:radio:checked').val() != undefined) {
            dataModel.formatType = $('input[name=formatType]:radio:checked').val().toUpperCase();
		}
		dataModel.shopId = $('#shopId').val().trim();
		dataModel.fromDate = $('#fDate').val().trim();
		dataModel.toDate = $('#tDate').val().trim();
		dataModel.staffCode = $('#staffCode').val().trim();
		//ReportUtils.exportReportProcessNoData('/report/daily-follow/bkpthnvgh/export',dataModel);
		ReportUtils.exportReport('/report/daily-follow/bkpthnvgh/export',dataModel);
	},
	reportPDTH : function(){
		var msg = '';
		$('#errMsg').html('').hide();
		var shopId = $("#shop").combotree("getValue");
		//if($('#shopCode').val().trim().length == 0 || $('#shopCode').val().trim() == activeType.ALL){
		if (!shopId && shopId.length == 0) {
			msg = format(msgErr_required_field,'đơn vị');
		}
		if(msg.length == 0){
			if($('#isNPP').val() == "false"){
				msg = "Đơn vị bạn vừa chọn không phải là 1 nhà phân phối cụ thể!";
			}
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfRequireCheck('lstSaleOderNumber','Đơn hàng');
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfRequireCheck('fDate','Từ ngày');
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfInvalidFormatDate('fDate','Từ ngày');
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfRequireCheck('tDate','Đến ngày');
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfInvalidFormatDate('tDate','Đến ngày');
		}
		var fDate = $('#fDate').val();
		var tDate = $('#tDate').val();
		var curDate = $('#curDate').val();
		if(!Utils.compareDate(fDate, tDate)){
			msg = msgErr_fromdate_greater_todate;		
			$('#fDate').focus();
		}
		if(!Utils.compareDate(fDate, curDate)){
			msg = "Từ ngày không được lớn hơn ngày hiện tại";		
			$('#fDate').focus();
		}
		if(msg.length > 0){
			$('#errMsg').html(msg).show();
			return false;
		}
		var lstSaleOderNumber = $('#lstSaleOderNumber').val().trim();
		if(lstSaleOderNumber.length >0){
			lstSaleOderNumber = lstSaleOderNumber.replace(/;/g, ',');
		}
		var dataModel = new Object();		
		dataModel.shopId = $('#shop').combotree("getValue");
		dataModel.fromDate = $('#fDate').val().trim();
		dataModel.toDate = $('#tDate').val().trim();
		dataModel.lstSaleOderNumberStr = lstSaleOderNumber;
		if($('input[name=formatType]:radio:checked').length > 0 && $('input[name=formatType]:radio:checked').val() != undefined) {
            dataModel.formatType = $('input[name=formatType]:radio:checked').val().toUpperCase();
		}

		ReportUtils.exportReport('/report/daily-follow/pdth/export',dataModel);
	},
	reportPDHCG:function(){ //vuonghn
		var msg = '';
		$('#errMsg').html('').hide();
		if($('#shopCode').val().trim().length == 0 || $('#shopCode').val().trim() == activeType.ALL){
			msg = format(msgErr_required_field,'đơn vị');
		}
		if(msg.length == 0){
			if($('#isNPP').val() == "false"){
				msg = "Đơn vị bạn vừa chọn không phải là 1 nhà phân phối cụ thể!";
			}
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfRequireCheck('fDate','Từ ngày');
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfInvalidFormatDate('fDate','Từ ngày');
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfRequireCheck('tDate','Đến ngày');
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfInvalidFormatDate('tDate','Đến ngày');
		}
		if(msg.length == 0 && $('#staffCode').val().trim()!='' && $('#staffCode').val().trim().length>50){
			msg = "Mã NVGH không được vượt quá 50 ký tự.";
		}
		var fDate = $('#fDate').val();
		var tDate = $('#tDate').val();
		var curDate = $('#curDate').val();
		if(!Utils.compareDate(fDate, tDate)){
			msg = msgErr_fromdate_greater_todate;		
			$('#fDate').focus();
		}
		if(!Utils.compareDate(fDate, curDate)){
			msg = "Từ ngày không được lớn hơn ngày hiện tại";		
			$('#fDate').focus();
		}
		if(msg.length > 0){
			$('#errMsg').html(msg).show();
			return false;
		}
		var dataModel = new Object();		
		dataModel.shopId = $('#shopCode').val().trim();
		dataModel.fromDate = $('#fDate').val().trim();
		dataModel.toDate = $('#tDate').val().trim();
		dataModel.deliveryCode = $('#staffCode').val().trim();
		if($('input[name=formatType]:radio:checked').length > 0 && $('input[name=formatType]:radio:checked').val() != undefined) {
            dataModel.formatType = $('input[name=formatType]:radio:checked').val().toUpperCase();
		}
		var kData = $.param(dataModel,true);
		$('#divOverlay').show();		
		$.ajax({
			type : "POST",
			url : "/report/daily-follow/pdhcg/export",
			data :(kData),
			dataType: "json",
			success : function(data) {
				$('#divOverlay').hide();				
				if(!data.error) {
					if(data.hasData != undefined && data.hasData != null && data.hasData == false) {
						$('#errMsg').html('Không có dữ liệu để xuất báo cáo').show();
					} else {
						window.location.href = data.path;
						setTimeout(function(){ //Set timeout để đảm bảo file load lên hoàn tất
		                    CommonSearch.deleteFileExcelExport(data.path);
						},2000);
					}
				} else {
					$('#errMsg').html(data.errMsg).show();					
				}
			},
			error:function(XMLHttpRequest, textStatus, errorDivThrown) {
				$('#divOverlay').hide();
				StockIssued.xhrExport = null;
			}
		});
		return false;
	},
	/**
	 * 7.1.8 Bao cao Phieu Thu Tra Cua Khach Hang
	 * 
	 * @author hunglm16
	 * @author October 08,2014
	 * @description Update Phan Quyen CMS
	 * */
	exportPTHCKH: function(){
		var msg = '';
		$('#errMsg').html('').hide();
//		if($('#shopId').val().trim().length == 0 || $('#shopId').val().trim() == activeType.ALL){
//			msg = format(msgErr_required_field,'đơn vị');
//		}
		if(msg.length == 0){
			msg = Utils.getMessageOfRequireCheck('fDate','Từ ngày');
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfInvalidFormatDate('fDate','Từ ngày');
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfRequireCheck('tDate','Từ ngày');
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfInvalidFormatDate('tDate','Đến ngày');
		}
		var fDate = $('#fDate').val();
		var tDate = $('#tDate').val();
		if(!Utils.compareDate(fDate, tDate)){
			msg = msgErr_fromdate_greater_todate;		
			$('#fDate').focus();
		}
		var currentTime = new Date();
		var month = currentTime.getMonth() + 1;
		var day = currentTime.getDate();
		var year = currentTime.getFullYear();
		if(!Utils.compareDate(fDate,day + '/' + month + '/' + year)){
			msg = 'Từ ngày không được lớn hơn ngày hiện tại';
			$('#fDate').focus();
		}
		
		if(msg.length > 0){
			$('#errMsg').html(msg).show();
			return false;
		}
		var lstStaffCode = $('#staffCode').val().trim();
		if(lstStaffCode.length >0){
			lstStaffCode = lstStaffCode.replace(/;/g, ',');
		}
		
		var dataModel = new Object();		
		dataModel.customerCode = $('#customerCode').val().trim();
		dataModel.shopId = $('#shopId').val().trim();
		dataModel.fromDate = $('#fDate').val().trim();
		dataModel.toDate = $('#tDate').val().trim();
		dataModel.lstStaffCodeStr = lstStaffCode;
		if($('input[name=formatType]:radio:checked').length > 0 && $('input[name=formatType]:radio:checked').val() != undefined) {
            dataModel.formatType = $('input[name=formatType]:radio:checked').val().toUpperCase();
       }
		ReportUtils.exportReport('/report/daily-follow/pthckh/export',dataModel);
	},
	
	exportVNM10_1_17: function() {
		var msg = '';
		$('#errMsg').hide();
		msg = Utils.getMessageOfRequireCheck('shopId', 'Đơn vị');
		$('#shop').focus();
		if (msg.length == 0) {
			msg = Utils.getMessageOfRequireCheck('promotionCode', 'Mã CT');
		}
		if (msg.length == 0) {
			msg = Utils.getMessageOfRequireCheck('categoryCode', 'Ngành hàng');
		}
		
		if (msg.length == 0) {
			msg = Utils.getMessageOfRequireCheck('fDate', 'Từ ngày');
		}
		if (msg.length == 0) {
			msg = Utils.getMessageOfRequireCheck('tDate', 'Đến ngày');
		}
		if (msg.length == 0) {
			msg = Utils.getMessageOfInvalidFormatDate('fDate', 'Từ ngày');
		}
		if (msg.length == 0) {
			msg = Utils.getMessageOfInvalidFormatDate('tDate', 'Từ ngày');
		}
		var fDate = $('#fDate').val().trim();
		var tDate = $('#tDate').val().trim();
		if (msg.length == 0 && !Utils.compareCurrentDate(fDate)) {
			msg = 'Từ ngày phải nhỏ hơn hoặc bằng ngày hiện tại';
			$('#fDate').focus();
		}

		if (msg.length == 0 && !Utils.compareDate(fDate, tDate)) {
			msg = 'Từ ngày phải nhỏ hơn hoặc bằng đến ngày';
			$('#fDate').focus();
		}

		if (msg.length > 0) {
			$('#errMsg').html(msg).show();
			return false;
		}

		var dataModel = new Object();
		dataModel.shopId = $('#shopId').val().trim();
		dataModel.fromDate = fDate;
		dataModel.toDate = tDate;
		dataModel.programCode = $('#promotionCode').val().replace(/ /g, '');
		dataModel.categoryCode = $('#categoryCode').val().replace(/ /g, '');
//		dataModel.formatType = 'XLS';//$('input[type=radio]:checked').val();
		if($('input[name=formatType]:radio:checked').length > 0 && $('input[name=formatType]:radio:checked').val() != undefined) {
            dataModel.formatType = $('input[name=formatType]:radio:checked').val().toUpperCase();
		}
		ReportUtils.exportReport('/report/daily-follow/bcVnm-thdhhh/export', dataModel);
		return false;
	},
	onSelectIsPrintChange:function(){
		var isPrint = $('#isPrint').val();
		/*if(isPrint != 1){
			$('#fromHour').attr('disabled', 'disabled');
			$('#toHour').attr('disabled', 'disabled');
		}else{
			$('#fromHour').removeAttr('disabled');
			$('#toHour').removeAttr('disabled');
		}*/
	}
	
};
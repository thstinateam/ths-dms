var dailyTicket = {
		/**
		 * 7.1.3 Bao cao Hoa don giao hang va thanh toan
		 * 
		 *  @author hunglm16
		 *  @since October 08,2014
		 *  @description Phan quyen CMS
		 * */
		reportDeliveryTicketAdd: function(){
			var msg = '';
			$('#errMsg').html('').hide();
			if($('#shopId').val().trim().length == 0 || $('#shopId').val().trim() == activeType.ALL){
				msg = format(msgErr_required_field,'đơn vị');
			}
			if(msg.length == 0){
				if($('#isNPP').val() == "false"){
					msg = "Đơn vị bạn vừa chọn không phải là 1 nhà phân phối cụ thể!";
					
				}
			}
			var chot = '';
			if (msg == ""){
				msg = Utils.getMessageOfRequireCheck('fDate', 'Từ ngày', false);
				chot = 'fDate';
			}
			if (msg == ""){
				msg = Utils.getMessageOfRequireCheck('tDate', 'Đến ngày', false);
				chot = 'tDate';
			}
			if($('#isPrint').val().trim() == 1){
				/*if(msg.length == 0){
					msg = Utils.getMessageOfRequireCheck('fromHour','Từ giờ');
				}
				
				if(msg.length == 0){
					msg = Utils.getMessageOfRequireCheck('toHour','Đến giờ');
				}
				
				if(msg.length == 0){
					msg = Utils.getMessageOfSpecialCharactersValidateEx('fromHour','Từ giờ',Utils._TF_NUMBER);
				}

				if(msg.length == 0){
					msg = Utils.getMessageOfSpecialCharactersValidateEx('toHour','Đến giờ',Utils._TF_NUMBER);
				}
				
				if ($('#fromHour').val()!=null){
					if (parseInt($('#fromHour').val())< 0 || parseInt($('#fromHour').val()) > 23)
					{
						$('#errMsg').html('Giá trị giờ nằm trong khoảng [0-23]. Mời bạn nhập lại.').show();
						$('#fromHour').focus();
						return false;
					}
				}
				
				if ($('#toHour').val()!=null){
					if (parseInt($('#toHour').val())< 0 || parseInt($('#toHour').val()) > 23)
					{
						$('#errMsg').html('Giá trị giờ nằm trong khoảng [0-23]. Mời bạn nhập lại.').show();
						$('#toHour').focus();
						return false;
					}
				}*/
			}
			
			if(msg.length > 0){
				$('#errMsg').html(msg).show();
				$('#'+chot).focus();
				return false;
			}
			if(msg.length == 0){
				msg = Utils.getMessageOfInvalidFormatDateNew('fDate','Từ ngày');
			}
			if(msg.length == 0){
				msg = Utils.getMessageOfInvalidFormatDateNew('tDate','Đến ngày');
			}
			var fDate = $('#fDate').val();
			var tDate = $('#tDate').val();
			if(!Utils.compareDate(fDate, tDate)){
				msg = msgErr_fromdate_greater_todate;		
				$('#fDate').focus();
			}
			if(msg.length > 0){
				$('#errMsg').html(msg).show();
				return false;
			}
			var dataModel = new Object();		
			dataModel.shopId = $('#shop').combotree('getValue');
			dataModel.fromDate = $('#fDate').val().trim();
			dataModel.toDate = $('#tDate').val().trim();
			//dataModel.fromHour = $('#fromHour').val().trim();
			//dataModel.toHour = $('#toHour').val().trim();
			dataModel.staffCode = $('#staffCode').val().trim();
			dataModel.isPrint = $('#isPrint').val();
			if($('input[name=formatType]:radio:checked').length > 0 && $('input[name=formatType]:radio:checked').val() != undefined) {
				dataModel.formatType = $('input[name=formatType]:radio:checked').val().toUpperCase();
			}
			ReportUtils.exportReport('/report/daily-follow/pgntn_view',dataModel);
		},
		//7.1.3a
		reportDeliveryTicketAdd_A: function(){
			var msg = '';
			$('#errMsg').html('').hide();
//			if($('#shopId').val().trim().length == 0 || $('#shopId').val().trim() == activeType.ALL){
//				msg = format(msgErr_required_field,'đơn vị');
//			}
//			if(msg.length == 0){
//				if($('#isNPP').val() == "false"){
//					msg = "Đơn vị bạn vừa chọn không phải là 1 nhà phân phối cụ thể!";
//					
//				}
//			}
			var chot = '';
			if (msg == ""){
				msg = Utils.getMessageOfRequireCheck('fDate', 'Từ ngày', false);
				chot = 'fDate';
			}
			if (msg == ""){
				msg = Utils.getMessageOfRequireCheck('tDate', 'Đến ngày', false);
				chot = 'tDate';
			}
			if(msg.length > 0){
				$('#errMsg').html(msg).show();
				$('#'+chot).focus();
				return false;
			}
			if(msg.length == 0){
				msg = Utils.getMessageOfInvalidFormatDate('fDate','Từ ngày');
			}
			if(msg.length == 0){
				msg = Utils.getMessageOfInvalidFormatDate('tDate','Đến ngày');
			}
			var fDate = $('#fDate').val();
			var tDate = $('#tDate').val();
			if(!Utils.compareDate(fDate, tDate)){
				msg = msgErr_fromdate_greater_todate;		
				$('#fDate').focus();
			}
			if(msg.length > 0){
				$('#errMsg').html(msg).show();
				return false;
			}
			var dataModel = new Object();		
			dataModel.shopId = $('#shop').combotree('getValue');
			dataModel.fromDate = $('#fDate').val().trim();
			dataModel.toDate = $('#tDate').val().trim();
			dataModel.staffCode = $('#staffCode').val().trim();
			dataModel.notApproved = '1';
			if($('input[name=formatType]:radio:checked').length > 0 && $('input[name=formatType]:radio:checked').val() != undefined) {
				dataModel.formatType = $('input[name=formatType]:radio:checked').val().toUpperCase();
			}
			ReportUtils.exportReport('/report/daily-follow/pgntn_view', dataModel);
		}
};
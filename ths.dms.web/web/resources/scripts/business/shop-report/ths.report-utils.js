var ReportUtils = {
	_isCheckAll: false,
	_objectSearchCTTB: null,
	_lstChecker: null,
	_lstUnchecker: null,
	_arrMultiChoiceDisplay: null,
	lstStaffId:null,
	_callbackBeforeImport:null,
	_callbackAfterImport:null,
	_idChoose: '',
	_currentShopCode:'',
	
	loadCombo: function(callback){		 
		var params = {};
		$.ajax({
			type : "POST",
			url:'/commons/list-shop-filter',
			data :($.param(params, true)),
			dataType : "json",
			success : function(data) {
				$('#shop').combobox({
					valueField: 'shopCode',
					textField:  'shopName',
					data: data.rows,
					panelWidth: '200',
					formatter: function(row) {
						return '<span style="font-weight:bold">' + Utils.XSSEncode(row.shopCode) + '</span><br/>' + '<span style="color:#888">' + Utils.XSSEncode(row.shopName) + '</span>';
					},
					filter: function(q, row){
						q = new String(q).toUpperCase().trim();
						var opts = $(this).combobox('options');
						return ((unicodeToEnglish(row[opts.textField]).indexOf(unicodeToEnglish(q)) == 0) || unicodeToEnglish(row[opts.valueField]).indexOf(unicodeToEnglish(q)) == 0) ;
					},	
			        onSelect: function(rec){
			        	if (rec != null && rec.shopCode != ReportUtils._currentShopCode){
			        		if(callback!=undefined && callback!=null){
								callback.call(this,rec.id);
							}
							$('#shopid').val(rec.id);
			        	}
			        },
			        onLoadSuccess: function(){
			        	var arr = $('#shop').combobox('getData');
			        	if (arr != null && arr.length > 0){
			        		$('#shop').combobox('select', arr[0].shopCode);
			        		ReportUtils._currentShopCode = arr[0].shopCode;
			        	}
			        	
			        }
				});
			}
		});
	},
	loadComboTree: function(controlId, storeCode, defaultValue, callback, params, vungType){		 
		var _url= '/rest/report/shop/combotree/1/0.json';
		if (vungType != undefined && vungType != null) {
			_url = '/rest/report/vung/combotree/1/0.json';	
		}
		$('#' + controlId).combotree({url: _url, 
			formatter: function(node) {
				return Utils.XSSEncode(node.text);
			},
			onChange: function(newValue, oldValue) {				
				if (params != null && params.length >0) {
					for (var i = 0; i < params.length; i++) {
						$('#'+params[i]).val('');
					}
				}
				if (DebitPayReport._lstStaff != null && DebitPayReport._lstStaff.size() >0) {
					DebitPayReport._lstStaff = new Map();
				}
			}
		});
		var t = $('#'+ controlId).combotree('tree');	
		t.tree('options').url = '';
		t.tree({			
			formatter: function(node) {
				return Utils.XSSEncode(node.text);
			},
			onSelect:function(node) {	
				$('#'+storeCode).val(node.id);
				if(callback!=undefined && callback!=null){
					callback.call(this, node.id, node);
				}
			},
		});
		if(defaultValue == undefined || defaultValue == null){
			$('#' + controlId).combotree('setValue',activeType.ALL);
		} else {
			$('#' + controlId).combotree('setValue',defaultValue);
		}
	},
	loadComboTreeAuthorize: function(controlId, storeCode,defaultValue,callback,params,vungType){		 
		var _url= '/rest/report/shop/combotree/authorize/1/0.json';
		$('#' + controlId).combotree({url: _url,
			formatter: function(node) {
				return Utils.XSSEncode(node.text);
			},
			onChange: function(newValue,oldValue){				
				if(params != null && params.length >0){
					for(var i=0;i<params.length;i++){
						$('#'+params[i]).val('');
					}
				}
				if(DebitPayReport._lstStaff != null && DebitPayReport._lstStaff.size() >0){
					DebitPayReport._lstStaff = new Map();
				}
			}
		});
		var t = $('#'+ controlId).combotree('tree');	
		t.tree({			
			formatter: function(node) {
				return Utils.XSSEncode(node.text);
			},
			onSelect:function(node){	
				if(callback!=undefined && callback!=null){//tungmt
					callback.call(this, node.id);
				}
				$('#'+storeCode).val(node.id);
				$.getJSON("/report/are-you-shop?shopId="+node.id,function(result){
					$('#isNPP').val(result.isNPP);
				});
			}
		});
		if(defaultValue == undefined || defaultValue == null){
			$('#' + controlId).combotree('setValue',activeType.ALL);
		} else {
			$('#' + controlId).combotree('setValue',defaultValue);
		}
	},
	loadComboTreeAuthorizeEx: function(controlId, storeCode,defaultValue,callback,params,vungType){		 
		var _url= '/catalog/report-manager/combobox-tree-shop';
		$('#' + controlId).combotree({url: _url,
			formatter: function(node) {
				return Utils.XSSEncode(node.text);
			},
			onChange: function(newValue,oldValue){				
				if(params != null && params.length >0){
					for(var i=0;i<params.length;i++){
						$('#'+params[i]).val('');
					}
				}
				if(DebitPayReport._lstStaff != null && DebitPayReport._lstStaff.size() >0){
					DebitPayReport._lstStaff = new Map();
				}
			}
		});
		var t = $('#'+ controlId).combotree('tree');	
		t.tree({			
			formatter: function(node) {
				return Utils.XSSEncode(node.text);
			},
			onSelect:function(node){	
				if(callback!=undefined && callback!=null){//tungmt
					callback.call(this, node.id);
				}
				$('#'+storeCode).val(node.id);
				$.getJSON("/report/are-you-shop?shopId="+node.id,function(result){
					$('#isNPP').val(result.isNPP);
				});
			}
		});
		if(defaultValue == undefined || defaultValue == null){
			$('#' + controlId).combotree('setValue',activeType.ALL);
		} else {
			$('#' + controlId).combotree('setValue',defaultValue);
		}
	},
	loadComboTree2: function(controlId, storeCode, defaultValue, callback, params, vungType){		 
		var _url= '/rest/report/shop/combotree/1/0.json';
		if (vungType != undefined && vungType != null) {
			_url = '/rest/report/vung/combotree/1/0.json';	
		}
		$('#' + controlId).combotree({
			url: _url,
			formatter: function(node) {
				return Utils.XSSEncode(node.text);
			},
			onChange: function(newValue, oldValue) {
				if (params != null && params.length > 0) {
					for (var i = 0; i < params.length; i++) {
						$('#'+params[i]).val('');
					}
				}
			}
		});
		
		var t = $('#'+ controlId).combotree('tree');	
		t.tree('options').url = '';
		t.tree({
			formatter: function(node) {
				return Utils.XSSEncode(node.text);
			},
			onSelect:function(node){	
				if (callback != undefined && callback != null) {
					callback.call(this, node);
				}
				$('#'+storeCode).val(node.id);
			}
		});
		if (defaultValue == undefined || defaultValue == null) {
			$('#' + controlId).combotree('setValue', activeType.ALL);
		} else {
			$('#' + controlId).combotree('setValue', defaultValue);
		}
	},
	/**
	 * @author hunglm16
	 * @description update lai ham chung
	 * */
	exportReport: function(url, dataModel, errMsg){
		try {
			dataModel.reportCode = $('#function_code').val();
		} catch(e) {
			// Oop! Error
		}
		var kData = $.param(dataModel,true);
		if (VTUtilJS.isNullOrEmpty(errMsg)) {
			errMsg = 'errMsg';
		}
		$('#'+errMsg).html('').hide();
		$('#divOverlay').show();		
		$.ajax({
			type : "POST",
			url : url,
			data : (kData),
			dataType: "json",
			success : function(data) {
				$('#divOverlay').hide();				
				if (!data.error) {
					if(data.hasData != undefined && data.hasData != null && data.hasData == false) {
						$('#'+errMsg).html('Không có dữ liệu để xuất!').show();
					} else {
						var formatType=$('input[name="formatType"]:checked').val();
						var filePath = data.path || data.view;
						if (filePath) {
							filePath = ReportUtils.buildReportFilePath(filePath);
							if(formatType && formatType == "PDF"){
								ReportUtils.openInNewTab(filePath);
							} else {
								var idx = filePath.lastIndexOf(".");
								if (idx > 0 && idx < filePath.length) {
									var ext = filePath.substring(idx, idx + 4);
									if (ext != null && ReportUtils.PDF_FORMAT === ext.toLowerCase()) {
										ReportUtils.openInNewTab(filePath);
									} else {
										window.location.href = filePath;
									}
								} else {
									window.location.href = filePath;
								}
							}
							setTimeout(function(){
								//Set timeout de dam bao file load len hoan tat
								CommonSearch.deleteFileExcelExport(filePath);
							}, 15000);
						} else {
							$('#'+errMsg).html('Không xác định được đường dẫn đến tập tin dữ liệu').show();
						}
					}
				} else {
					$('#'+errMsg).html(data.errMsg).show();					
				}
			},
			error: function (XMLHttpRequest, textStatus, errorDivThrown) {
				$('#divOverlay').hide();
				StockIssued.xhrExport = null;
			}
		});
	},
	/**
	 * @author tamvnm
	 * @sine 22/07/2015
	 * @description export co call back
	 * */
	exportReportWithCallBack: function(dataModel, url, errMsg, callback){
		try {
			dataModel.reportCode = $('#function_code').val();
		} catch(e) {
			// Oop! Error
		}
		var kData = $.param(dataModel,true);
		if (VTUtilJS.isNullOrEmpty(errMsg)) {
			errMsg = 'errMsg';
		}
		$('#'+errMsg).html('').hide();
		$('#divOverlay').show();
		$("#imgOverlay").show();
		$.ajax({
			type : "POST",
			url : url,
			data : (kData),
			dataType: "json",
			success : function(data) {
				$('#divOverlay').hide();				
				if (!data.error) {
					if(data.hasData != undefined && data.hasData != null && data.hasData == false) {
						$('#'+errMsg).html('Không có dữ liệu để xuất!').show();
					} else {
						var formatType=$('input[name="formatType"]:checked').val();
						var filePath = data.path || data.view;
						if (filePath) {
							filePath = ReportUtils.buildReportFilePath(filePath);
							if(formatType && formatType == "PDF"){
								ReportUtils.openInNewTab(filePath);
							} else {
								var idx = filePath.lastIndexOf(".");
								if (idx > 0 && idx < filePath.length) {
									var ext = filePath.substring(idx, idx + 4);
									if (ext != null && ReportUtils.PDF_FORMAT === ext.toLowerCase()) {
										ReportUtils.openInNewTab(filePath);
									} else {
										ReportUtils.openInNewTab(filePath);
									}
								} else {
									ReportUtils.openInNewTab(filePath);
								}
							}
							setTimeout(function(){
								//Set timeout de dam bao file load len hoan tat
								CommonSearch.deleteFileExcelExport(filePath);
							}, 15000);
							if(callback!= null && callback!= undefined){
								callback.call(this,data);
							}
						} else {
							$('#'+errMsg).html('Không xác định được đường dẫn đến tập tin dữ liệu').show();
						}
					}
				} else {
					$('#'+errMsg).html(data.errMsg).show();					
				}
			},
			error: function (XMLHttpRequest, textStatus, errorDivThrown) {
				$('#divOverlay').hide();
				StockIssued.xhrExport = null;
			}
		});
	},
	openInNewTab:function(url) {
		var win = window.open(url, '_blank');
		win.focus();
	},
	/**
	 * Tao cay bao cao
	 * 
	 * @author hunglm16
	 * @since October 1,2014
	 * */
	loadTree: function(){
		loadDataForTree('/rest/report/tree.json');
		$('#tree').bind("loaded.jstree", function(event, data){
			//ReportUtils.showReportForm('XNTF1');
		});
		$('#tree').bind("select_node.jstree", function (event, data) {			
			var id = data.rslt.obj.attr("id");
			var url = data.rslt.obj.attr("url");
			var text = data.rslt.obj.attr("name");
			if (ReportUtils._idChoose != undefined && ReportUtils._idChoose!=null && ReportUtils._idChoose != id) {
				if (url == undefined || url == null || url.trim().length == 0) {
					return;
				}
				ReportUtils._idChoose = id;
				if(id!= null && id!= undefined && id.length > 0){
					$.ajax({
						type : 'GET',
						url : url,			
						dataType : "html",
						success : function(data) {
							$('#reportType').val(id);
							$('#title').html(text);
							$('#reportContainer').html(data);				
						},
						error:function(XMLHttpRequest, textStatus, errorThrown) {}}
					);
				}
			}
	    });
		$('#tree').bind("open_node.jstree", function (event, data) {
	    });
	},
	setCurrentDateForCalendar: function(objId,firstDay){
		var currentTime = new Date();
		var month = currentTime.getMonth() + 1;
		var day = currentTime.getDate();
		var year = currentTime.getFullYear();
		if(month < 10){
			month = '0' + month;
		}
		if(day < 10){
			day = '0' + day;
		}
		if(firstDay != undefined && firstDay != null ){
			day = firstDay;
		}
		$('#' + objId).val(day + '/' + month + '/' + year);
	},
	setCurrentMonthForCalendar: function(objId){
		var currentTime = new Date();
		var month = currentTime.getMonth() + 1;
		var year = currentTime.getFullYear();
		if(month < 10){
			month = '0' + month;
		}

		$('#' + objId).val(month + '/' + year);
	},
	getCurrentDateString: function(){
		var currentTime = new Date();
		var month = currentTime.getMonth() + 1;
		var day = currentTime.getDate();
		var year = currentTime.getFullYear();
		if(month < 10){
			month = '0' + month;
		}
		if(day < 10){
			day = '0' + day;
		}
		return day + '/' + month + '/' + year;
	},
	bindEnterOnWindow:function(){
		$(window).bind('keypress',function(event){
			if(event.keyCode == keyCodes.ENTER){
				/*var len = 0;//Đếm những dialog không ẩn
				$('.easyui-dialog').each(function(){
					if(!$(this).is(':hidden')){
						++len;
					}
				});
				if(!$('#searchStyle2EasyUIDialog').is(':hidden')){
					++len;
				}
				//Nếu có 1 cái hiện, tức có 1 cái không ẩn, tức có 1 popup search nhân viên thì không được click xuất báo cáo khi enter,
				//Vì enter đó là để search nhân viên, không phải để xuất báo cáo.
				//Khi nào tất cả popup đều ẩn, không có popup nào hiện cả, lúc đó mà bấm enter thì rõ ràng là enter đó để gọi lệnh click xuất báo cáo nè:
				if(len==0)
					$('#reportContainer .BtnGeneralStyle').click();*/
				if ($(".easyui-dialog:not(:hidden)").length == 0 && $('#searchStyle2EasyUIDialog:not(:hidden)').length == 0) {
					$('#reportContainer .BtnGeneralStyle').click();
				}
			}
			if(event.keyCode == keyCodes.ESCAPE){
				$.fancybox.close();
			}
		});
	},
	loadTreeStaff: function(){
		loadDataForStaffTree();
		$('#tree').bind("loaded.jstree", function(event, data){
			if($('#treeContainer').data("jsp")!= undefined){
				$('#treeContainer').data("jsp").destroy();
			}
			$('#treeContainer').jScrollPane();
			$('#tree li a').each(function(){
				if($(this).html().indexOf("(N/A)")==-1) 
					$(this).css('color','blue');
				});
		});
		$('#tree').bind("select_node.jstree", function (event, data) {			
			var id = data.rslt.obj.attr("id");
			SuperviseSales.moveToStaff(id);
			if($('#treeContainer').data("jsp")!= undefined){
				$('#treeContainer').data("jsp").destroy();
			}
			$('#treeContainer').jScrollPane();
	    });
		$('#tree').bind("open_node.jstree", function (event, data) {
				if($('#treeContainer').data("jsp")!= undefined){
					$('#treeContainer').data("jsp").destroy();
				}
				$('.loadingOverLayStaff').remove();
				setTimeout(function(){
					$('#treeContainer').jScrollPane();
					$('#tree li a').each(function(){
						if($(this).html().indexOf("(N/A)")==-1) 
							$(this).css('color','blue');
					});
				},500);
	    });
		$("#tree").bind("before.jstree", function (e, data) {
			if(data.func === "open_node") {
				$('.StaffSelectBtmSection').append("<p class='LoadingStyle loadingOverLayStaff'><img width='16' height='16' src='/resources/images/loading.gif'/>");
			}
			if(data.func === "correct_state"){
				$('.loadingOverLayStaff').remove();
			}
		});
	},
	loadComboTreeStockTrans: function(controlId, storeCode, staffCode, shopId, defaultValue)
	{		 
		var _url= '/rest/report/stocktrans/combotree/' + shopId + "/" + staffCode + '.json';
		
		$('#' + controlId).combotree({url: _url});
		var t = $('#'+ controlId).combotree('tree');	
		t.tree('options').url = '';
		t.tree({			
			onSelect:function(node){	
				$('#'+storeCode).val(node.id);
			},
			onBeforeExpand:function(result)
			{	            					
				var nodes = t.tree('getChildren',result.target);
				for(var i=0;i<nodes.length;++i){
					t.tree('remove',nodes[i].target);
				}
				
			},	 
			onExpand: function(result){	
				$('.panel-body-noheader ul').css('display','block');			
		}});
		if(defaultValue == undefined || defaultValue == null){
			$('#' + controlId).combotree('setValue',activeType.ALL);
		} else {
			$('#' + controlId).combotree('setValue',defaultValue);
		}
	},
	//CTTB 2013
	loadComboReportTreeEx: function(controlId, storeCode,defaultValue,callback){
		var _url='/rest/report/shop/combotree/1/0.json';	
		$('#' + controlId).combotree({url: _url,
			onChange: function(){
			}
			});
		var t = $('#'+ controlId).combotree('tree');	
		t.tree('options').url = '';
		t.tree({			
			onSelect:function(node){
				$.getJSON('/rest/catalog/shop/'+node.id+'/code.json', function(data){
					data.name = Utils.XSSEncode(data.name);
					if(callback!=undefined && callback!=null){
						callback.call(this, data.name);
					}
					$('#'+storeCode).val(Utils.XSSEncode(data.name));
				});
			},	  
			formatter:function(node){
				return Utils.XSSEncode(node.text);
			},
			onBeforeExpand:function(result){	            					
				var nodes = t.tree('getChildren',result.target);
				for(var i=0;i<nodes.length;++i){
					t.tree('remove',nodes[i].target);
				}
				$.ajax({
					type : "POST",
					url : '/rest/report/shop/combotree/2/'+ result.id +'.json',					
					dataType : "json",
					success : function(r) {
						$(r).each(function(i,e){
							e.text = Utils.XSSEncode(e.text);
						});
						t.tree('append',{
							parent:result.target,
							data:r
						});
						$('.panel-body-noheader ul').css('display','block');
					}
				});
			},
			onExpand: function(result){	
				$('.panel-body-noheader ul').css('display','block');			
		}});
		if(defaultValue == undefined || defaultValue == null){
			$('#' + controlId).combotree('setValue',activeType.ALL);
		} else {
			$('#' + controlId).combotree('setValue',defaultValue);
		}
	},
	
	
	/**
	 * Dialog tim kiem chon lua CTTB
	 * @author 
	 * @since January 17,2014
	 * */
	openDialogSearchCTTB : function(urlParameter) {
		var html = '<div id="common-dialog-search-2-textbox" class="easyui-dialog">';
		html += '<div class="PopupContentMid">';
		html += '<div class="GeneralForm Search1Form" >';
		html += '<label class="LabelStyle Label1Style" style=" width: 100px;">Mã CTTB</label>';
		html += '<input id="programCode" maxlength="50" type="text" style="width: 145px;" class="InputTextStyle InputText1Style"/>';
		html += '<label class="LabelStyle Label1Style" style=" width: 100px;">Tên CTTB</label>';
		html += '<input id="programName" maxlength="250" type="text" style="width: 145px;" class="InputTextStyle InputText1Style"/>';
		html += '<div class="Clear"></div>'; 
		html += '<div class="BtnCenterSection">';
		html += '<button class="BtnGeneralStyle BtnGeneralMStyle" tabindex="3" id="common-dialog-button-search">Tìm kiếm</button>';
		html += '</div>';
		html += '<div class="Clear"></div>';
		html += '<div class="GridSection" id="common-dialog-grid-container">';
		html += '<table id="common-dialog-grid-search"></table>';
		html += '</div>';
		html += '<div class="BtnCenterSection">';
		html += '<button id ="common-dialog-button-choose" class="BtnGeneralStyle BtnGeneralMStyle">Chọn</button>';
		html += '<button class="BtnGeneralStyle BtnGeneralMStyle" onclick="$(\'#common-dialog-search-2-textbox\').dialog(\'close\');">Đóng</button>';
		html += '</div>';
		html += '<p id="dialog-search-error" style="display: none;" class="ErrorMsgStyle"></p>';
		html += '</div>';
		html += '</div>';
		html += '</div>';
		//style="display: none;"
		$('body').append(html);
		
		$('#common-dialog-search-2-textbox').dialog({
			 title: 'CHỌN CHƯƠNG TRÌNH TRƯNG BÀY',
			 width: 580, 
			 height: 'auto',
			 closed: false,
			 cache: false,
			 modal: true,
			 onOpen: function() {
				 $('#common-dialog-search-2-textbox #common-dialog-button-search').unbind('click');
				 $('#common-dialog-search-2-textbox #common-dialog-button-search').bind('click', function() {
					 $('#common-dialog-search-2-textbox #dialog-search-error').html('').hide();
					 $('#common-dialog-grid-search').datagrid('reload');
				 });
				 $('#common-dialog-search-2-textbox input').unbind('keyup');
				 $('#common-dialog-search-2-textbox input').bind('keyup', function(e) {	
					 if(e.keyCode == keyCodes.ENTER) {
						 $('#common-dialog-search-2-textbox #common-dialog-button-search').click();
					 }
				 });
				 $('#common-dialog-search-2-textbox #common-dialog-button-choose').bind('click', function() {
					 if(ReportUtils.lstStaffId.size() == 0) {
						 $('#common-dialog-search-2-textbox #dialog-search-error').html('Bạn chưa chọn CTTB. Vui lòng chọn CTTB').show();
					 }else{
						 $('#common-dialog-search-2-textbox #dialog-search-error').html('').hide();
						 var listStaffStr = ReportUtils.lstStaffId.valArray[0].displayProgramCode;
						 for(var i = 1; i < ReportUtils.lstStaffId.valArray.length; i++) {
							 listStaffStr += ',' + ReportUtils.lstStaffId.valArray[i].displayProgramCode;
						 }
						 $('#multiChoiceCTTB').val(listStaffStr);
						 $('#common-dialog-search-2-textbox').dialog('close');
					 }
				 });
				 ReportUtils.lstStaffId = new Map();
				 $('#common-dialog-grid-search').datagrid({
					 url : urlParameter,
					 autoRowHeight : true,
					 rownumbers : true, 
					 pagination:true,
					 rowNum : 10,
					 pageSize:10,
					 scrollbarSize : 0,
					 pageNumber:1,
					 queryParams:{
						 page:1
					 },
					 onBeforeLoad: function(param) {
						 var fromDate = $('#fDate').val().trim();
						 var toDate = $('#tDate').val().trim();
						 var shopId = $('#shop').combotree('getValue');
						 var code = $('#common-dialog-search-2-textbox #programCode').val().trim();
						 var name = $('#common-dialog-search-2-textbox #programName').val().trim();
						 
						 param.code = code;
						 param.name = name;
						 param.fromDate = fromDate;
						 param.toDate = toDate;
						 param.shopId = shopId;
						 param = $.param(param, true);
					 },
					 onCheck : function (index,row) {
						 ReportUtils.lstStaffId.put(row.id, row);
					 },
					 onUncheck : function (index,row) { 
						 ReportUtils.lstStaffId.remove(row.id);
					 },
					 onCheckAll : function(rows) {
						 for(var i = 0; i < rows.length; i++) {
							 var row = rows[i];
							 ReportUtils.lstStaffId.put(row.id, row);
						 }
					 },
					 onUncheckAll : function(rows) {
						 for(var i = 0; i < rows.length; i++) {
							 var row = rows[i];
							 ReportUtils.lstStaffId.remove(row.id);
						 }
					 },
					 fitColumns:true,
					 width : ($('#common-dialog-search-2-textbox').width() - 40),
					 columns:[[
					 	{field: 'displayProgramCode', title: 'Mã CTTB', sortable: false, resizable: false, width: 60, align: 'left',
							formatter: function(val, row, idx) {
								if (val != undefined && val != null) {
									return Utils.XSSEncode(val);
								}
								return '';
							}
						},
						{field: 'displayProgramName', title: 'Tên CTTB', sortable: false, resizable: false, width: 130, align: 'left',
							formatter: function(val, row, idx) {
								if (val != undefined && val != null) {
									return Utils.XSSEncode(val);
								}
								return '';
							}
						},
						{field: 'times', title: 'Thời gian', sortable: false, resizable: false, width: 100, align: 'left',
							formatter: function(val, row, idx) {
								return GridFormatterUtils.dateFromdateTodate(row.fromDate, row.toDate);
							}
						},
						{field: 'checkerSelect', title: '<input id="checkSelectAll" type="checkbox" title="Chọn tất cả" onchange="ReportUtils.onchangeCheckerAll()" />',
							sortable: false, resizable: false, width: 20, align: 'center',
							formatter: function(val, row, idx) {
								var str='';
								if(ReportUtils.lstStaffId.get(row.id)!=null){
									str=' checked="checked" ';
								}
								return '<input type ="checkbox" '+str+' class="displayProgramCheckbox" value="'+row.id+'" id="check_'+row.id+'" onclick="ReportUtils.onCheckListCus('+row.id+');">';
							}
						}
					 ]],
					 onLoadSuccess :function(data){							 
						 if(($.isArray(data) && data.length != 0) || (data.rows != null && data.rows != undefined && $.isArray(data.rows) && data.rows.length != 0)) {
							 var __keys = (data.rows == null || data.rows == undefined) ? Object.keys(data[0]) : Object.keys(data.rows[0]);
							 var rows = (data.rows == null || data.rows == undefined) ? data : data.rows;
							 var isCheckAll=true;
							 for(var i = 0; i < rows.length; i++) {
								 if(ReportUtils.lstStaffId.get(rows[i].id)) {
									 $('#common-dialog-grid-search').datagrid('checkRow', i);
								 } else {
									 isCheckAll = false;
								 }
							 }
							 if(isCheckAll) {
								 $('#common-dialog-search-2-textbox .datagrid-header-row input[type=checkbox]').attr('checked', 'checked');
							 }
						 }
						 $('#common-dialog-search-2-textbox .datagrid-header-rownumber').html('STT');
						 var dHeight = $('#common-dialog-search-2-textbox').height();
						 var wHeight = $(window).height();
						 var top = (wHeight - dHeight) / 2 > 180 ? (wHeight - dHeight) / 2 - 180 : (wHeight - dHeight) / 2;
						 $('#common-dialog-search-2-textbox').dialog('move', {top : top});
					 }
				 });
			 },
			 onClose: function() {
				 ReportUtils.lstStaffId = null;
				 $('#common-dialog-search-2-textbox').remove(); 
			 }
		});
		
//		$('.ErrorMsgStyle').html("").show();
//		$('.SuccessMsgStyle').html("").show();
// 		$('#btnSearchStyleDisPlayPro').bind('keyup',function(event){
//			if(event.keyCode == 13){
//				//Goi ham reload grid
//			}
//		});
//		
//		$('#seachStyleDisPlayProCode').bind('keyup',function(event){
//			if(event.keyCode == 13){
//				//Goi ham reload grid
//			}
//		}).focus();
//		
//		$('#seachStyleDisPlayProName').bind('keyup',function(event){
//			if(event.keyCode == 13){
//				//Goi ham reload grid
//			}
//		});
//		if(ReportUtils._objectSearchCTTB!=null){
//			ReportUtils._objectSearchCTTB.name=encodeChar($('#seachStyleDisPlayProCode').val());
//			ReportUtils._objectSearchCTTB.code=encodeChar($('#seachStyleDisPlayProName').val());
//		}else{
//			ReportUtils._objectSearchCTTB = new Object();
//			ReportUtils._objectSearchCTTB.name=encodeChar($('#seachStyleDisPlayProCode').val());
//			ReportUtils._objectSearchCTTB.code=encodeChar($('#seachStyleDisPlayProName').val());
//		}
//		
// 		$('#searchStyleDisPlayProForTreeP').dialog({  
//	        title: "CHỌN CHƯƠNG TRÌNH TRƯNG BÀY",  
//	        closed: false,  
//	        cache: false,  
//	        modal: true,
//	        width : 616,
//	        height :'auto',
//	        onOpen: function(){
//				$('#seachStyleDisPlayProCode').focus();
//				$('#searchStyleDisPlayProForTreeP #common-dialog-button-search').unbind('click');
//				 $('#searchStyleDisPlayProForTreeP #common-dialog-button-search').bind('click', function() {
//					 $('#searchStyleDisPlayProForTreeP #dialog-search-error').html('').hide();
//					 $('#searchCTTBGrid').datagrid('reload');
//				 });
//				 $('#searchStyleDisPlayProForTreeP input').unbind('keyup');
//				 $('#searchStyleDisPlayProForTreeP input').bind('keyup', function(e) {	
//					 if(e.keyCode == keyCodes.ENTER) {
//						 $('#searchStyleDisPlayProForTreeP #btnSearchCTTB').click();
//					 }
//				 });
//				 $('#searchStyleDisPlayProForTreeP #btnSelectProgramDisplay').bind('click', function() {
//					 if(ReportUtils.lstStaffId.size() == 0) {
//						 $('#searchStyleDisPlayProForTreeP #errMsgDialogDisplayPro').html('Bạn chưa chọn CTTB. Vui lòng chọn CTTB').show();
//					 }else{
//						 $('#searchStyleDisPlayProForTreeP #errMsgDialogDisplayPro').html('').hide();
//						 var listStaffStr = ReportUtils.lstStaffId.valArray[0].displayProgramCode;
//						 for(var i = 1; i < ReportUtils.lstStaffId.valArray.length; i++) {
//							 listStaffStr += ',' + ReportUtils.lstStaffId.valArray[i].displayProgramCode;
//						 }
//						 $('#multiChoiceCTTB').val(listStaffStr);
//						 $('#searchStyleDisPlayProForTreeP').dialog('close');
//					 }
//				 });
//				 ReportUtils.lstStaffId = new Map();
//				$('#searchCTTBGrid').datagrid({
//					url: urlParameter,
//					queryParams:ReportUtils._objectSearchCTTB,
//					pageList: [10],
//					pageSize: 10,
//					height: 'auto',
//					scrollbarSize: 0,
//					pagination: true,
//					fitColumns: true,
//					singleSelect: true,
//					selectOnCheck: false,
//					checkOnSelect: false,
//					method: 'GET',
//					rownumbers: true,
//					width: $('#custGridCtn').width(),
//					columns: [[
//						{field: 'displayProgramCode', title: 'Mã CTTB', sortable: false, resizable: false, width: 60, align: 'left',
//							formatter: function(val, row, idx) {
//								if (val != undefined && val != null) {
//									return Utils.XSSEncode(val);
//								}
//								return '';
//							}
//						},
//						{field: 'displayProgramName', title: 'Tên CTTB', sortable: false, resizable: false, width: 150, align: 'left',
//							formatter: function(val, row, idx) {
//								if (val != undefined && val != null) {
//									return Utils.XSSEncode(val);
//								}
//								return '';
//							}
//						},
//						{field: 'times', title: 'Thời gian', sortable: false, resizable: false, width: 80, align: 'left',
//							formatter: function(val, row, idx) {
//								return GridFormatterUtils.dateFromdateTodate(row.fromDate, row.toDate);
//							}
//						},
//						{field: 'checkerSelect', title: '<input id="checkSelectAll" type="checkbox" title="Chọn tất cả" onchange="ReportUtils.onchangeCheckerAll()" />',
//							sortable: false, resizable: false, width: 20, align: 'center',
//							formatter: function(val, row, idx) {
//								return '<input type ="checkbox" class="displayProgramCheckbox" value="'+row.id+'" id="check_'+row.id+'" onclick="ReportUtils.onCheckListCus('+row.id+');">';
//							}
//						}
//					]],
//					onBeforeLoad: function(params) {
//						
//					},
//					onCheck : function (index,row) {
//						 ReportUtils.lstStaffId.put(row.id, row);
//					 },
//					 onUncheck : function (index,row) { 
//						 ReportUtils.lstStaffId.remove(row.id);
//					 },
//					 onCheckAll : function(rows) {
//						 for(var i = 0; i < rows.length; i++) {
//							 var row = rows[i];
//							 ReportUtils.lstStaffId.put(row.id, row);
//						 }
//					 },
//					 onUncheckAll : function(rows) {
//						 for(var i = 0; i < rows.length; i++) {
//							 var row = rows[i];
//							 ReportUtils.lstStaffId.remove(row.id);
//						 }
//					 },
//					onLoadSuccess : function(data) {
//						$('.datagrid-header-rownumber').html('STT');
//						updateRownumWidthForJqGrid('#orderGrid1');
//						$('.datagrid-body-inner td').css('vertical-align', 'middle');
//						if($.isArray(data.rows)) {
//							var i =0, j = 0;
//							if(ReportUtils._isCheckAll==true){
//								$('input.displayProgramCheckbox').attr('checked', 'checked');
//								if(ReportUtils._lstUnchecker!=undefined && ReportUtils._lstUnchecker!=null && ReportUtils._lstUnchecker.length>0){
//									flagFor = false;
//									for(i=0; i< ReportUtils._lstUnchecker.length; ++i){
//										for(j = 0; j < data.rows.length; j++) {
//											if(ReportUtils._lstUnchecker[i]==data.rows[j].id){
//						    					$('input#check_'+data.rows[j].id.toString().trim()).removeAttr('checked');
//						    				}
//										}
//									}
//								}
//							}else{
//								$('input.displayProgramCheckbox').removeAttr('checked');
//								if(ReportUtils._lstChecker!=undefined && ReportUtils._lstChecker!=null && ReportUtils._lstChecker.length>0){
//									for(i=0; i< ReportUtils._lstChecker.length; ++i){
//										for(j = 0; j < data.rows.length; j++) {
//											if(ReportUtils._lstChecker[i]==data.rows[j].id){
//						    					$('input#check_'+data.rows[j].id.toString().trim()).attr('checked', 'checked');
//						    				}
//										}
//									}
//								}
//							}
//						
//						}
//					}
//				});
//	        },
//	        buttons:'#bottomDisplayProDialog',
//	        onBeforeClose: function() {
//	        },
//	        onClose : function(){
//	        	$('#searchStyleDisPlayProContainerGrid').html('<table id="searchStyleDisPlayProGrid" class="easyui-treegrid"></table>').show();
//	        	$('#seachStyleDisPlayProCode').val("").show();
//	        	$('#seachStyleDisPlayProName').val("").show();
//	        }
//	    });
//		return false;
	},
	/**
	 * search Display Program
	 * @author hunglm16
	 * @since January 17,2014
	 * */
	searchMultiDisplayPro: function(){
		$('.ErrorMsgStyle').html("").show();
		$('.SuccessMsgStyle').html("").show();
		var params = {};
		params.shopId = $('#shop').combotree('getValue');
		params.year = encodeChar($('#fullYearIn10').val());
		params.code=encodeChar($('#seachStyleDisPlayProCode').val());
		params.name=encodeChar($('#seachStyleDisPlayProName').val());
		$('#searchCTTBGrid').datagrid('reload', params);
	},
	/**
	 * Su kien dong Dialog Tim kiem va chon lua CTTB
	 * @author hunglm16
	 * @since January 17,2014
	 * */
	onCloseDialogSearchMultiDisplayPro: function(){
		$('.ErrorMsgStyle').html("").show();
		$('.SuccessMsgStyle').html("").show();
		ReportUtils._lstChecker = new Array();
		ReportUtils._lstUnchecker = new Array();
		ReportUtils._isCheckAll = false;
		$('#seachStyleDisPlayProCode').val("").show();
    	$('#seachStyleDisPlayProName').val("").show();
    	$('#searchStyleDisPlayProDL').dialog('close');
	},
	/**
	 * Su kien chon cac CTTB
	 * @author hunglm16
	 * @since January 17,2014
	 * */
	multiChoiceDisplayPro: function(){
		var msg = "";
		if(!ReportUtils._isCheckAll && ReportUtils._lstChecker.length==0 && ReportUtils._lstUnchecker==0){
			msg = "Chưa chọn CTTB";
			$('#errMsgDialogDisplayPro').html(msg).show();
			return false;
		}
		if(msg.length==0){
			var arrIdStr = "-1";
			var i = 0;
			var params = {};
			if(ReportUtils._isCheckAll){
				params.flagCheckAll = true;
				for(i =0; i<ReportUtils._lstUnchecker.length; ++i){
					arrIdStr = arrIdStr.trim()+"," + ReportUtils._lstUnchecker[i].toString();
				}
				arrIdStr = arrIdStr.trim().replace(/-1,/g, "");
				arrIdStr = arrIdStr.trim().replace(/-1/g, "");
				params.lstIdStr = arrIdStr.trim();
			}else{
				params.flagCheckAll = false;
				for(i =0; i<ReportUtils._lstChecker.length; ++i){
					arrIdStr = arrIdStr.trim()+"," + ReportUtils._lstChecker[i].toString();
				}
				arrIdStr = arrIdStr.trim().replace(/-1,/g, "");
				arrIdStr = arrIdStr.trim().replace(/-1/g, "");
				params.lstIdStr = arrIdStr.trim();
			}
			params.year = $('#fullYearIn10').val();
			params.code = $('#seachStyleDisPlayProCode').val();
			params.name = $('#seachStyleDisPlayProName').val();
			params.shopId = $('#shop').combotree('getValue');
			var url = '/programme-display/lst-Display-Program-By-ListId';
			Utils.getJSONDataByAjaxNotOverlay(params,url,function(data){
				if(data.lstData!=undefined && data.lstData!=null && data.lstData.length>0){
					ReportUtils._arrMultiChoiceDisplay = new Array();
					var strTemp = "-1";
					for(var j=0; j<data.lstData.length; ++j){
						//Danh sach nhung CTTB duoc chon
						ReportUtils._arrMultiChoiceDisplay.push(data.lstData[j].id);
						strTemp = strTemp.trim() + "," + data.lstData[j].displayProgramCode;
					}
					strTemp = strTemp.trim().replace(/-1,/g, "");
					strTemp = strTemp.trim().replace(/-1/g, "");
					$('#multiChoiceCTTB').val(strTemp.trim());
				}
			});
		}
		ReportUtils.onCloseDialogSearchMultiDisplayPro();
	},
	
	onchangeCheckerAll: function() {
		$('.ErrorMsgStyle').html("").show();
		$('.SuccessMsgStyle').html("").show();
		//Bat su kien check, uncheck tat ca
		var flag = $('#checkSelectAll').is(':checked');
		if(flag!=false){
			$('input.displayProgramCheckbox').attr('checked', 'checked');
			ReportUtils._lstChecker = new Array();
			ReportUtils._lstUnchecker = new Array();
			ReportUtils._isCheckAll = true;
		}else{
			$('input.displayProgramCheckbox').removeAttr('checked');
			ReportUtils._lstChecker = new Array();
			ReportUtils._lstUnchecker = new Array();
			ReportUtils._isCheckAll = false;
		}
	},
	onCheckListCus:function(id){
		$('.ErrorMsgStyle').html("").show();
		$('.SuccessMsgStyle').html("").show();
		var flag = $('#check_'+id.toString()).is(':checked');
		var flagCheckAll = $('#checkSelectAll').is(':checked');
		var flagFor = false;
		//Bat su kien check, uncheck trong danh sach khach hang
		if(flag ==false && flagCheckAll==true){
			$('#checkSelectAll').removeAttr('checked');
			ReportUtils._lstUnchecker.push(id);
		}
		if(flagCheckAll!=true && ReportUtils._isCheckAll == true){
			if(ReportUtils._lstUnchecker!=undefined && ReportUtils._lstUnchecker!=null && ReportUtils._lstUnchecker.length>0){
				flagFor = false;
				for(var i=0; i< ReportUtils._lstUnchecker.length; ++i){
					if(ReportUtils._lstUnchecker[i]==id){
						flagFor = true;
						break;
					}
				}
				if(!flagFor){
					ReportUtils._lstUnchecker.push(id);
				}else{
					ReportUtils.removeArrayByValue(ReportUtils._lstUnchecker, id);
				}
			}else{
				ReportUtils._lstUnchecker = new Array();
				ReportUtils._lstUnchecker.push(id);
			}
		}
		if(flagCheckAll!=true && ReportUtils._isCheckAll != true){
			if(ReportUtils._lstChecker!=undefined && ReportUtils._lstChecker!=null && ReportUtils._lstChecker.length>0){
				flagFor = false;
				for(var i=0; i< ReportUtils._lstChecker.length; ++i){
					if(ReportUtils._lstChecker[i]==id){
						flagFor = true;
						break;
					}
				}
				if(!flagFor){
					ReportUtils._lstChecker.push(id);
				}else{
					ReportUtils.removeArrayByValue(ReportUtils._lstChecker, id);
				}
			}else{
				ReportUtils._lstChecker = new Array();
				ReportUtils._lstChecker.push(id);
			}
		}
	},
	removeArrayByValue:function(arr,val){
		//HungLm16 - Delete Item in Array
		for(var i=0; i<arr.length; i++) {
	        if(arr[i] == val){
	        	arr.splice(i, 1);
		        break;
	        }
	    }
	},
	loadKendoTreeMultipleChoice: function(controlId, defaultShopCodeValue,defaultIdValue){
		var flagFirstLoad = true;
		var valueInTree = null;
		var lstDataMultiselect =null;
		$('#' + controlId).kendoMultiSelect({
	        dataTextField: "shopCode",
	        dataValueField: "id",
			itemTemplate: function(data, e, s, h, q) {
				if(data.type.channelTypeCode == 'VNM') {//VNM
					return '<div class="tree-vnm" node-id="'+Utils.XSSEncode(data.id)+'" style="width:230px"><span class="tree-vnm-node"><div style="width: 15px; display: inline-block;"></div></span><span class="tree-vnm-text">'+Utils.XSSEncode(data.shopCode) + ' - ' + Utils.XSSEncode(data.shopName)+'</span></div>';
					}else if(data.type.channelTypeCode == 'MIEN'){
					return '<div class="tree-mien" node-id="'+Utils.XSSEncode(data.id)+'" style="width:220px;"><span class="tree-mien-node"><div style="width: 15px; display: inline-block;"></div></span><span class="tree-mien-text">'+Utils.XSSEncode(data.shopCode) + ' - ' + Utils.XSSEncode(data.shopName)+'</span></div>';
				}else if(data.type.channelTypeCode == 'VUNG'){
					return '<div class="tree-vung" node-id="'+Utils.XSSEncode(data.id)+'" style="width:200px"><span class="tree-vung-node"><div style="width: 15px; display: inline-block;"></div></span><span class="tree-vung-text">'+Utils.XSSEncode(data.shopCode) + ' - ' + Utils.XSSEncode(data.shopName)+'</span></div>';
				}else if(data.type.channelTypeCode == 'NPP'){
					return '<div class="tree-npp" node-id="'+Utils.XSSEncode(data.id)+'" style="display:-moz-grid-group"><span class="tree-npp-node"><div style="width: 15px; display: inline-block;"></div></span><span class="tree-npp-text">'+Utils.XSSEncode(data.shopCode) + '-' + Utils.XSSEncode(data.shopName)+'</span></div>';
				}
			},
	        tagTemplate:  '#: data.shopCode #',			  
	        dataSource: {
	            transport: {
	                read: {
	                    dataType: "json",
	                    url: "/rest/report/shop/kendoui-combobox-ho.json"
	                }
	            }
	        },
	        change: function(e) {
	        	var lstValueTmp = this.value();
	        	var lstValue = new Array();
	        	var lstDeleted = new Array();
	        	//xoa trong datasource cua cay
	        	var multiselect = $('#' + controlId).data("kendoMultiSelect");
	        	var lstDataMultiselectTmp = new Array();
	        	var lstNodeToBeDeleted = new Array();
	        	$(valueInTree._data).each(function(i,e){
	        		lstDataMultiselectTmp.push(e);
	        	});
	        	
	        	lstDataMultiselect = new kendo.data.DataSource({
					  data: lstDataMultiselectTmp,
					  schema: {
					    model: { 
					    	id: "id"
					    		,
					    	children: 'items'
					   	}
					  }
					});
	        	lstDataMultiselect.read();
	        	$(lstValue).each(function(i4,e4){
	        		var obje4 = valueInTree.get(e4);
	        		$(obje4.items).each(function(i3,e3){
	        			$(e3.items).each(function(i2,e2){
	        				$(e2.items).each(function(i1,e1){
	        					lstNodeToBeDeleted.push(e1.id);
	    	        		});
	        				lstNodeToBeDeleted.push(e2.id);
		        		});
	        			lstNodeToBeDeleted.push(e3.id);
	        		});
	        	});
	        	
	        	$(lstNodeToBeDeleted).each(function(i,e){
	        		if(lstDataMultiselect.get(e) != null){
	        			lstDataMultiselect.remove(lstDataMultiselect.get(e));
	        		}
	        	});
	        	
	        	multiselect.setDataSource(lstDataMultiselect);
	        	multiselect.value(lstValue);
	        },
		    value: [{ shopCode: $('#'+defaultShopCodeValue).val(), id: $('#'+defaultIdValue).val()}],
		    dataBound: function(e){
		    	if(!flagFirstLoad)
		    		return;
		    	var dataMultiselect = $('#' + controlId).data("kendoMultiSelect");
		    	$(dataMultiselect.dataSource._data).each(function(i,e){
		    		if(e.parentShop != null){
		    			var obj = dataMultiselect.dataSource.get(e.parentShop.id);
		    			if(obj != null){
		    				if(obj.items == null|| obj.items == undefined){
		    					obj.items = new Array();
		    					obj.items.push(e);
		    				}else{
		    					obj.items.push(e);
		    				}
		    				
		    			}
		    		}
		    	});
		    	
				valueInTree = new kendo.data.DataSource({
					  data: dataMultiselect.dataSource._data,
					  schema: {
					    model: { 
					    	id: "id"
					    		,
					    	children: 'items'
					   	}
					  }
					});
				valueInTree.read();
		    }
		});
		if(defaultIdValue != null && defaultIdValue != undefined){
			var multiselect = $('#'+controlId).data("kendoMultiSelect");
			multiselect.value(defaultIdValue);
		}
		flagFirstLoad = false;
	},
	/**
	 * tao duong dan den file bao cao
	 * @author tuannd20
	 * @return String Duong dan toi file bao cao
	 * @since  13/04/2015
	 */
	buildReportFilePath: function(reportFilePath) {
		var filePath = '';
		try {
			var reportToken = $('#report_token').val();
			filePath = reportFilePath + '?v=' + reportToken;
		} catch (e) {
			// Oop! Error
		}
		return filePath;
	},
};
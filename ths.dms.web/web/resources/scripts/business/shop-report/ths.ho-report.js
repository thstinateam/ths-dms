var HoReport = {
		exportSLDS1_1: function() {
			$('#errMsg').hide();
			var msg = '';
			var multiShop = $("#shop").data("kendoMultiSelect");
			var dataShop = null;
			if (multiShop != null) {
				dataShop = multiShop.dataItems();
			}
			
			if(dataShop == null || dataShop.length <= 0){
				msg = format(msgErr_required_field,'Đơn vị');
			}
			
			if(msg.length == 0){
				msg = Utils.getMessageOfRequireCheck('fromDate','Từ ngày');
			}
			if(msg.length == 0){
				msg = Utils.getMessageOfInvalidFormatDate('fromDate','Từ ngày');
			}
			if(msg.length == 0){
				msg = Utils.getMessageOfRequireCheck('toDate','Đến ngày');
			}
			if(msg.length == 0){
				msg = Utils.getMessageOfInvalidFormatDate('toDate','Đến ngày');
			}
			var fDate = $('#fromDate').val();
			var tDate = $('#toDate').val();
			var curDate = ReportUtils.getCurrentDateString();
			if(msg.length == 0 && !Utils.compareDate(fDate, curDate)){
				msg = 'Từ ngày không được lớn hơn ngày hiện tại. Vui lòng nhập lại';
				$('#fromDate').focus();
			}	
			if(msg.length == 0 && !Utils.compareDate(tDate, curDate)){
				msg = 'Đến ngày không được lớn hơn ngày hiện tại. Vui lòng nhập lại';
				$('#toDate').focus();
			}
			if(msg.length == 0 && !Utils.compareDate(fDate, tDate)) {
				msg ='Từ ngày không được lớn hơn đến ngày. Vui lòng nhập lại';
				$('#fromDate').focus();
			}
			if(msg.length > 0){
				$('#errMsg').html(msg).show();
				return false;
			}
			
			var dataModel = new Object();
			var lstShopId = [];
			for(var i=0;i<dataShop.length;i++){
				lstShopId.push(dataShop[i].shopId);
			}
			dataModel.lstShopId = lstShopId.join(',');
			//dataModel.lstStaffSaleCode = $('#staffSaleCode').val().trim().toString();
			dataModel.fromDate = $('#fromDate').val();
			dataModel.toDate = $('#toDate').val();
			dataModel.formatType = $('input[type=radio]:checked').val();
			dataModel.typeSales = $('#typeSales').val();
			if($('#checkExport').is(':checked')) {
				dataModel.checkExport = true;
			} else {
				dataModel.checkExport = false;
			}
			ReportUtils.exportReport('/report/ho/slds1_1/export', dataModel);
		},
		/**
		 * Bc SLDS theo NVBH, SP, NH
		 * 
		 * @author hoanv25
		 * @since July 10, 2015
		 * */
		exportSLDS1_1_NUTI: function() {
			var msg = '';
			$('#errMsg').html('').hide();
			var nvbhKendo = $("#lstProduct").data("kendoMultiSelect");
			var lstNvbhId = nvbhKendo.value();
		/*	var multiShop = $("#shop").data("kendoMultiSelect");*/
			var dataShop = null;
			if (nvbhKendo != null) {
				dataShop = nvbhKendo.dataItems();
			}
			if ($('#shopId').val().trim().length == 0 || $('#shopId').val().trim() == activeType.ALL) {
				msg = format(msgErr_required_field,'đơn vị');
			}
			if (msg.length == 0) {
				msg = Utils.getMessageOfRequireCheck('fDate','Từ ngày');
			}
			if (msg.length == 0) {
				msg = Utils.getMessageOfRequireCheck('tDate','Đến ngày');
			}
			if (msg.length > 0) {
				$('#errMsg').html(msg).show();
				return false;
			}
			if (!checkDateForCycle()) {
				return;
			}
			var dataModel = new Object();		
			var lstShopId = [];
			for(var i=0;i<dataShop.length;i++){
				lstShopId.push(dataShop[i].nvbhName);
			}
			dataModel.strListShopId=$('#shopId').val();
			dataModel.fromDate = $('#fDate').val().trim();
			dataModel.toDate = $('#tDate').val().trim();
			dataModel.cycleId = $('#cycle').val();
			if ($('#category').val() > 0) {
				dataModel.categoryId = $('#category').val();
			}
			if ($('#sub_category').val() > 0) {
				dataModel.subCategoryId = $('#sub_category').val();		
			}
			if (lstNvbhId != undefined && lstNvbhId != null && lstNvbhId != "") {
				dataModel.strListProductId = lstNvbhId.toString().trim();
			}
			if (lstShopId != undefined && lstShopId != null && lstShopId != "") {
				dataModel.strListProductName = lstShopId.toString().trim();
			}
			ReportUtils.exportReport('/report/ho/slds1_1_nuti/export', dataModel);
		},
		
		/**
		 * Bc SLDS theo NVBH
		 * 
		 * @author hoanv25
		 * @since July 15, 2015
		 * */
		exportSLDS2_2_NUTI: function() {
			var msg = '';
			$('#errMsg').html('').hide();
			var nvbhKendo = $("#lstProduct").data("kendoMultiSelect");
			var lstNvbhId = nvbhKendo.value();
			var dataShop = null;
			if (nvbhKendo != null) {
				dataShop = nvbhKendo.dataItems();
			}
			if ($('#shopId').val().trim().length == 0 || $('#shopId').val().trim() == activeType.ALL) {
				msg = format(msgErr_required_field,'đơn vị');
			}
			if (msg.length == 0) {
				msg = Utils.getMessageOfRequireCheck('fDate','Từ ngày');
			}
			if (msg.length == 0) {
				msg = Utils.getMessageOfRequireCheck('tDate','Đến ngày');
			}
			if (msg.length > 0) {
				$('#errMsg').html(msg).show();
				return false;
			}
			if (!checkDateForCycle()) {
				return;
			}
			var dataModel = new Object();		
			var lstShopId = [];
			for(var i=0;i<dataShop.length;i++){
				lstShopId.push(dataShop[i].nvbhName);
			}
			dataModel.chooseShopId = $('#shopId').val();
			dataModel.fromDate = $('#fDate').val().trim();
			dataModel.toDate = $('#tDate').val().trim();
			dataModel.cycleId = $('#cycle').val();
			if ($('#category').val() > 0) {
				dataModel.categoryId = $('#category').val();
			}
			if ($('#sub_category').val() > 0) {
				dataModel.subCategoryId = $('#sub_category').val();		
			}
			if (lstNvbhId != undefined && lstNvbhId != null && lstNvbhId != "") {
				dataModel.strListProductId = lstNvbhId.toString().trim();
			}
			if (lstShopId != undefined && lstShopId != null && lstShopId != "") {
				dataModel.strListProductName = lstShopId.toString().trim();
			}
			ReportUtils.exportReport('/report/ho/slds1_2/export', dataModel);
		},
		
		/**
		 * Bc SLDS theo Khach Hang
		 * 
		 * @author hoanv25
		 * @since July 20, 2015
		 * */
		exportSLDS2_3_NUTI: function() {
			var msg = '';
			$('#errMsg').html('').hide();
			var nvbhKendo = $("#lstProduct").data("kendoMultiSelect");
			var lstNvbhId = nvbhKendo.value();
			var dataShop = null;
			if (nvbhKendo != null) {
				dataShop = nvbhKendo.dataItems();
			}
			if ($('#shopId').val().trim().length == 0 || $('#shopId').val().trim() == activeType.ALL) {
				msg = format(msgErr_required_field,'đơn vị');
			}
			if (msg.length == 0) {
				msg = Utils.getMessageOfRequireCheck('fDate','Từ ngày');
			}
			if (msg.length == 0) {
				msg = Utils.getMessageOfRequireCheck('tDate','Đến ngày');
			}
			if (msg.length > 0) {
				$('#errMsg').html(msg).show();
				return false;
			}
			if (!checkDateForCycle()) {
				return;
			}
			var dataModel = new Object();		
			var lstShopId = [];
			for(var i=0;i<dataShop.length;i++){
				lstShopId.push(dataShop[i].nvbhName);
			}
			dataModel.strListShopId=$('#shopId').val();
			dataModel.fromDate = $('#fDate').val().trim();
			dataModel.toDate = $('#tDate').val().trim();
			dataModel.cycleId = $('#cycle').val();
			if ($('#category').val() > 0) {
				dataModel.categoryId = $('#category').val();
			}
			if ($('#sub_category').val() > 0) {
				dataModel.subCategoryId = $('#sub_category').val();		
			}
			if (lstNvbhId != undefined && lstNvbhId != null && lstNvbhId != "") {
				dataModel.strListProductId = lstNvbhId.toString().trim();
			}
			if (lstShopId != undefined && lstShopId != null && lstShopId != "") {
				dataModel.strListProductName = lstShopId.toString().trim();
			}
			ReportUtils.exportReport('/report/ho/slds1_3/export', dataModel);
		},
		
		/**
		 * Bc SLDS 2.4 Theo doi KPI
		 * 
		 * @author hoanv25
		 * @since July 24, 2015
		 * */
		exportSLDS2_4_NUTI: function() {
			var msg = '';
			$('#errMsg').html('').hide();		
			if ($('#shopId').val().trim().length == 0 || $('#shopId').val().trim() == activeType.ALL) {
				msg = format(msgErr_required_field,'đơn vị');
			}		
			if (msg.length > 0) {
				$('#errMsg').html(msg).show();
				return false;
			}
			if (!checkDateForCycle()) {
				return;
			}
			var dataModel = new Object();		
			dataModel.strListShopId=$('#shopId').val();
			dataModel.cycleId = $('#cycle').val();			
			ReportUtils.exportReport('/report/ho/slds2_4/export', dataModel);
		},
		
		/**
		 * Bc 2.6 Danh sach don hang
		 * 
		 * @author hoanv25
		 * @since July 21, 2015
		 * */
		exportSLDS2_6_NUTI: function(){		
			var msg = '';	
			var nvbhKendo = $("#nvbh").data("kendoMultiSelect");
			var lstNvbhId = nvbhKendo.value();		
			var multiShop = $("#shop").data("kendoMultiSelect");
			var dataShop = multiShop.dataItems();
			var dataNVBH = null;
			if (nvbhKendo != null) {
				dataNVBH = nvbhKendo.dataItems();
			}
			if(dataShop!=null && dataShop.length>0){
				var lstShop = new Array();
				for(var i=0;i<dataShop.length;i++){
					lstShop.push(dataShop[i].id);
				}
			}
			var lstNVBH = [];
			for(var i=0;i<dataNVBH.length;i++){
				lstNVBH.push(dataNVBH[i].nvbhName);
			}
			$('#errMsg').html('').hide();
			if (msg.length == 0) {
				msg = Utils.getMessageOfRequireCheck('fDate','Từ ngày');
			}
			if (msg.length == 0) {
				msg = Utils.getMessageOfRequireCheck('tDate','Đến ngày');
			}
			if (msg.length > 0) {
				$('#errMsg').html(msg).show();
				return false;
			}
			if (!checkDateForCycle()) {
				return;
			}
			var orderType = $('#orderType').val();		
			var orderStatus = $('#orderStatus').val().trim();
			var dataModel = new Object();
			dataModel.strListShopId=lstShop.toString().trim();
			dataModel.fromDate = $('#fDate').val().trim();
			dataModel.toDate = $('#tDate').val().trim();
			dataModel.cycleId = $('#cycle').val();
			if (orderType != null) {
				dataModel.orderType = $('#orderType').val();
			}
			dataModel.orderNumber = $('#orderNumber').val().trim();	
			if ($('#orderSource').val().trim() != 0) {
				dataModel.orderSource = $('#orderSource').val().trim();
			}			
			if(orderStatus != '-1' && orderStatus != ''){
				if(orderStatus == '0'){//Đã trả
					dataModel.saleOrderType = 0;
				}else{
					var str=orderStatus.split('-');
					if(str.length > 1){
						dataModel.approved = str[0];
						if(str[1] != ''){
							dataModel.approvedStep = str[1];
						}
					}
				}
			}			
			if (lstNvbhId != undefined && lstNvbhId != null && lstNvbhId != "") {
				dataModel.strListNvbhId = lstNvbhId.toString().trim();
			}
			if (lstNVBH != undefined && lstNVBH != null && lstNVBH != "") {
				dataModel.strListNVBH = lstNVBH.toString().trim();
			}
			ReportUtils.exportReport('/report/ho/Rpt_HO_SLDS2_6/exportXLSX',dataModel);
		},
		
		exportSLDS1_2: function() {
			$('#errMsg').hide();
			var msg = '';
			var multiShop = $("#shop").data("kendoMultiSelect");
			var dataShop = null;
			if (multiShop != null) {
				dataShop = multiShop.dataItems();
			}
			
			if(dataShop == null || dataShop.length <= 0){
				msg = format(msgErr_required_field,'Đơn vị');
			}
			
			if(msg.length == 0){
				msg = Utils.getMessageOfRequireCheck('fromDate','Từ ngày');
			}
			if(msg.length == 0){
				msg = Utils.getMessageOfInvalidFormatDate('fromDate','Từ ngày');
			}
			if(msg.length == 0){
				msg = Utils.getMessageOfRequireCheck('toDate','Đến ngày');
			}
			if(msg.length == 0){
				msg = Utils.getMessageOfInvalidFormatDate('toDate','Đến ngày');
			}
			var fDate = $('#fromDate').val();
			var tDate = $('#toDate').val();
			var curDate = ReportUtils.getCurrentDateString();
			if(msg.length == 0 && !Utils.compareDate(fDate, curDate)){
				msg = 'Từ ngày không được lớn hơn ngày hiện tại. Vui lòng nhập lại';
				$('#fromDate').focus();
			}	
			if(msg.length == 0 && !Utils.compareDate(tDate, curDate)){
				msg = 'Đến ngày không được lớn hơn ngày hiện tại. Vui lòng nhập lại';
				$('#toDate').focus();
			}
			if(msg.length == 0 && !Utils.compareDate(fDate, tDate)) {
				msg ='Từ ngày không được lớn hơn đến ngày. Vui lòng nhập lại';
				$('#fromDate').focus();
			}
			
			if(msg.length > 0){
				$('#errMsg').html(msg).show();
				return false;
			}
			
			var dataModel = new Object();
			var lstShopId = [];
			for(var i=0;i<dataShop.length;i++){
				lstShopId.push(dataShop[i].shopId);
			}
			dataModel.lstShopId = lstShopId.join(',');
			//dataModel.lstStaffSaleCode = $('#staffSaleCode').val().trim().toString();
			dataModel.fromDate = $('#fromDate').val();
			dataModel.toDate = $('#toDate').val();
			dataModel.formatType = $('input[type=radio]:checked').val();
			if($('#checkExport').is(':checked')) {
				dataModel.checkExport = true;
			} else {
				dataModel.checkExport = false;
			}
			ReportUtils.exportReport('/report/ho/slds1_2/export', dataModel);
		},
		
		exportSLDS1_3:function() {
			var msg = '';
			$('#errMsg').html('').hide();
			var shopKendo = $("#shop").data("kendoMultiSelect");
			var dataShop = shopKendo.dataItems();
			if(dataShop==null||dataShop.length==0){
				msg = format(msgErr_required_field,'Đơn vị');
			}
			if(msg.length == 0){
				msg = Utils.getMessageOfRequireCheck('fromDate','Từ ngày');
			}
			if(msg.length == 0){
				msg = Utils.getMessageOfInvalidFormatDate('fromDate','Từ ngày');
			}
			if(msg.length == 0){
				msg = Utils.getMessageOfRequireCheck('toDate','Đến ngày');
			}
			if(msg.length == 0){
				msg = Utils.getMessageOfInvalidFormatDate('toDate','Đến ngày');
			}
			var fDate = $('#fromDate').val();
			var tDate = $('#toDate').val();
			var curDate = ReportUtils.getCurrentDateString();
			if(msg.length == 0 && !Utils.compareDate(fDate, curDate)){
				msg = 'Từ ngày không được lớn hơn ngày hiện tại. Vui lòng nhập lại';
				$('#fromDate').focus();
			}	
			if(msg.length == 0 && !Utils.compareDate(tDate, curDate)){
				msg = 'Đến ngày không được lớn hơn ngày hiện tại. Vui lòng nhập lại';
				$('#toDate').focus();
			}
			if(msg.length == 0 && !Utils.compareDate(fDate, tDate)) {
				msg ='Từ ngày không được lớn hơn đến ngày. Vui lòng nhập lại';
				$('#fromDate').focus();
			}
			if(msg.length > 0){
				$('#errMsg').html(msg).show();
				return false;
			}
			var lstShopId = dataShop[0].id;
			for(var i=1;i<dataShop.length;i++){
				lstShopId += "," + dataShop[i].shopId;
			}
			var dataModel = new Object();		
			dataModel.strListShopId = lstShopId;
			dataModel.fromDate = $('#fromDate').val();
			dataModel.toDate = $('#toDate').val();
			dataModel.formatType = $('input[type=radio]:checked').val();
			if($('#checkExport').is(':checked')) {
				dataModel.checkExport = true;
			} else {
				dataModel.checkExport = false;
			}
			ReportUtils.exportReport('/report/ho/slds1_3/export', dataModel);
		},
		
		exportXNTCT: function() {
			$('#errMsg').hide();
			var msg = '';
			var multiShop = $("#shop").data("kendoMultiSelect");
			var dataShop = null;
			if (multiShop != null) {
				dataShop = multiShop.dataItems();
			}
			
			if(dataShop == null || dataShop.length <= 0){
				msg = format(msgErr_required_field,'Đơn vị');
			}
			
			if (msg.length == 0) {
				msg = Utils.getMessageOfRequireCheck('fromDate', 'Từ ngày');
			}
			if (msg.length == 0) {
				msg = Utils.getMessageOfRequireCheck('toDate', 'Đến ngày');
			}
			if (msg.length == 0) {
				msg = Utils.getMessageOfInvalidFormatDate('fromDate', 'Từ ngày');
			}
			if (msg.length == 0) {
				msg = Utils.getMessageOfInvalidFormatDate('toDate', 'Từ ngày');
			}
			var fDate = $('#fromDate').val().trim();
			var tDate = $('#toDate').val().trim();
			if (msg.length == 0 && !Utils.compareCurrentDate(fDate)) {
				msg = 'Từ ngày phải nhỏ hơn hoặc bằng ngày hiện tại';
				$('#fromDate').focus();
			}

			if (msg.length == 0 && !Utils.compareDate(fDate, tDate)) {
				msg = 'Từ ngày phải nhỏ hơn hoặc bằng đến ngày';
				$('#fromDate').focus();
			}
			
			if(msg.length > 0){
				$('#errMsg').html(msg).show();
				return false;
			}
			
			var dataModel = new Object();
			var lstShopId = [];
			for(var i=0;i<dataShop.length;i++){
				lstShopId.push(dataShop[i].id);
			}
			
			dataModel.lstShopId = lstShopId.join(',');
			dataModel.catCode = $('#catCode').val().trim();
			dataModel.catCode = dataModel.catCode.replace(/ /g, '');
			dataModel.catCode = dataModel.catCode.replace(/;/g, ',');
			dataModel.productCode = $('#product').val().trim();
			dataModel.productCode = dataModel.productCode.replace(/ /g, '');
			dataModel.productCode = dataModel.productCode.replace(/;/g, ',');
			dataModel.fromDate = fDate;
			dataModel.toDate = tDate;
			dataModel.formatType = $('input[type=radio]:checked').val();
			if($('#checkExport').is(':checked')){
				dataModel.checkExport = true;
			}else{
				dataModel.checkExport = false;
			}
			if($('input[name=formatType]:radio:checked').length > 0 && $('input[name=formatType]:radio:checked').val() != undefined) {
	            dataModel.formatType = $('input[name=formatType]:radio:checked').val().toUpperCase();
			}
			ReportUtils.exportReport('/report/ho/xntct/export', dataModel);
		},
		exportF1: function() {
			$('#errMsg').hide();
			var msg = '';
			var multiShop = $("#shop").data("kendoMultiSelect");
			var dataShop = null;
			if (multiShop != null) {
				dataShop = multiShop.dataItems();
			}
			if(dataShop == null || dataShop.length <= 0){
				msg = format(msgErr_required_field,'Đơn vị');
			}
			if(msg.length == 0){
				msg = Utils.getMessageOfRequireCheck('fromDate','Từ ngày');
			}
			if(msg.length == 0){
				msg = Utils.getMessageOfInvalidFormatDate('fromDate','Từ ngày');
			}
			if(msg.length == 0){
				msg = Utils.getMessageOfRequireCheck('toDate','Đến ngày');
			}
			if(msg.length == 0){
				msg = Utils.getMessageOfInvalidFormatDate('toDate','Đến ngày');
			}
			var fDate = $('#fromDate').val();
			var tDate = $('#toDate').val();
			var curDate = ReportUtils.getCurrentDateString();
			if(msg.length == 0 && !Utils.compareDate(fDate, curDate)){
				msg = 'Từ ngày không được lớn hơn ngày hiện tại. Vui lòng nhập lại';
				$('#fromDate').focus();
			}	
			if(msg.length == 0 && !Utils.compareDate(tDate, curDate)){
				msg = 'Đến ngày không được lớn hơn ngày hiện tại. Vui lòng nhập lại';
				$('#toDate').focus();
			}
			if(msg.length == 0 && !Utils.compareDate(fDate, tDate)) {
				msg ='Từ ngày không được lớn hơn đến ngày. Vui lòng nhập lại';
				$('#fromDate').focus();
			}
			if(msg.length > 0){
				$('#errMsg').html(msg).show();
				return false;
			}
			
			var dataModel = new Object();
			var lstShopId = [];
			for(var i=0;i<dataShop.length;i++){
				lstShopId.push(dataShop[i].id);
			}
			dataModel.lstShopId = lstShopId.join(',');
			dataModel.catCode = $('#catCode').val().trim().toString();
			dataModel.productCode = $('#product').val().trim().toString();
			dataModel.fromDate = $('#fromDate').val();
			dataModel.toDate = $('#toDate').val();
			dataModel.formatType = $('input[type=radio]:checked').val();
			if($('#checkExport').is(':checked')){
				dataModel.checkExport = true;
			}else{
				dataModel.checkExport = false;
			}
			ReportUtils.exportReport('/report/ho/f1/export', dataModel);
		},
		
		exportSLDS1_4: function() {
			$('#errMsg').hide();
			var msg = '';
			var multiShop = $("#shop").data("kendoMultiSelect");
			var dataShop = null;
			if (multiShop != null) {
				dataShop = multiShop.dataItems();
			}
			
			if(dataShop == null || dataShop.length <= 0){
				msg = format(msgErr_required_field,'Đơn vị');
			}
			
			if (msg.length == 0) {
				msg = Utils.getMessageOfRequireCheck('fDate', 'Từ ngày');
			}
			if (msg.length == 0) {
				msg = Utils.getMessageOfRequireCheck('tDate', 'Đến ngày');
			}
			if (msg.length == 0) {
				msg = Utils.getMessageOfInvalidFormatDate('fDate', 'Từ ngày');
			}
			if (msg.length == 0) {
				msg = Utils.getMessageOfInvalidFormatDate('tDate', 'Từ ngày');
			}
			var fDate = $('#fDate').val().trim();
			var tDate = $('#tDate').val().trim();
			if (msg.length == 0 && !Utils.compareCurrentDate(fDate)) {
				msg = 'Từ ngày phải nhỏ hơn hoặc bằng ngày hiện tại';
				$('#fDate').focus();
			}

			if (msg.length == 0 && !Utils.compareDate(fDate, tDate)) {
				msg = 'Từ ngày phải nhỏ hơn hoặc bằng đến ngày';
				$('#fDate').focus();
			}
			
			if(msg.length > 0){
				$('#errMsg').html(msg).show();
				return false;
			}
			
			var dataModel = new Object();
			var lstShopId = [];
			for (var i=0; i < dataShop.length; i++) {
				lstShopId.push(dataShop[i].shopId);
			}
			dataModel.lstShopId = lstShopId.join(',');
			var s = $('#categoryCode').val().trim();
			s = s.replace(/ /g, '');
			dataModel.catCode = s.replace(/;/g, ',');
			dataModel.fromDate = fDate;
			dataModel.toDate = tDate;
			if($('#checkExport').is(':checked')) {
				dataModel.checkExport = true;
			} else {
				dataModel.checkExport = false;
			}
			dataModel.formatType = $('input[type=radio]:checked').val();
			var ck = $('input[type=radio]:checked').val();
			if (ck!= undefined && ck!=null && 'XLS'== ck) {
				ReportUtils.exportReport('/report/ho/slds1_4/export-xlsx', dataModel);
			} else {
				ReportUtils.exportReport('/report/ho/slds1_4/export', dataModel);
			}
		},
		/**
		 * Load ds san pham
		 * 
		 * @author hoanv25
		 * @since July 10, 2015
		 * */
		loadComboProduct:function(url){
			$("#lstProduct").kendoMultiSelect({
				dataSource: {
					transport: {
		                read: {
		                    dataType: "json",
		                    url: url
		                }
		            }
				},
				filter: "contains",
		        dataTextField: "nvbhName",
		        dataValueField: "nvbhId",
				itemTemplate: function(data, e, s, h, q) {
					return '<div class="tree-vnm" node-id="'+Utils.XSSEncode(data.nvbhId)+'" style="width:180px"><span class="tree-vnm-text">'+Utils.XSSEncode(data.nvbhCode)+' - '+Utils.XSSEncode(data.nvbhName) +'</span></div>';	
				},
		        tagTemplate:  '#: data.nvbhName #',
		        value: [$('#nvbhId').val()]		       
		    });	 
		},
		exportSLDS1_5: function() {
			$('#errMsg').hide();
			var msg = '';
			var multiShop = $("#shop").data("kendoMultiSelect");
			var dataShop = null;
			if (multiShop != null) {
				dataShop = multiShop.dataItems();
			}
			
			if(dataShop == null || dataShop.length <= 0){
				msg = format(msgErr_required_field,'Đơn vị');
			}
			
			if(msg.length == 0){
				msg = Utils.getMessageOfRequireCheck('fromDate','Từ ngày');
			}
			if(msg.length == 0){
				msg = Utils.getMessageOfInvalidFormatDate('fromDate','Từ ngày');
			}
			if(msg.length == 0){
				msg = Utils.getMessageOfRequireCheck('toDate','Đến ngày');
			}
			if(msg.length == 0){
				msg = Utils.getMessageOfInvalidFormatDate('toDate','Đến ngày');
			}
			var fDate = $('#fromDate').val();
			var tDate = $('#toDate').val();
			var curDate = ReportUtils.getCurrentDateString();
			if(msg.length == 0 && !Utils.compareDate(fDate, curDate)){
				msg = 'Từ ngày không được lớn hơn ngày hiện tại. Vui lòng nhập lại';
				$('#fromDate').focus();
			}	
			if(msg.length == 0 && !Utils.compareDate(tDate, curDate)){
				msg = 'Đến ngày không được lớn hơn ngày hiện tại. Vui lòng nhập lại';
				$('#toDate').focus();
			}
			if(msg.length == 0 && !Utils.compareDate(fDate, tDate)) {
				msg ='Từ ngày không được lớn hơn đến ngày. Vui lòng nhập lại';
				$('#fromDate').focus();
			}
			if(msg.length > 0){
				$('#errMsg').html(msg).show();
				return false;
			}
			
			var dataModel = new Object();
			var lstShopId = [];
			for(var i=0;i<dataShop.length;i++){
				lstShopId.push(dataShop[i].id);
			}
			dataModel.lstShopId = lstShopId.join(',');
			dataModel.fromDate = $('#fromDate').val();
			dataModel.toDate = $('#toDate').val();
			dataModel.formatType = $('input[type=radio]:checked').val();
			if($('#checkExport').is(':checked')) {
				dataModel.checkExport = true;
			} else {
				dataModel.checkExport = false;
			}
			ReportUtils.exportReport('/report/ho/slds1_5/export', dataModel);
		},
		
		finalFixed: {
			ALL: -2
		},
		/**
		 * Bao cao 6.2.2 ASO
		 * 
		 * @author hunglm16
		 * @return Excel File
		 * @since October 09,2015
		 * @description API - POI
		 */
		reportASOForExcel: function() {
			$('.ErrorMsgStyle').html('').change();
			var dataModel = new Object();
			var msg = '';
			//Lay tham so nganh hang
			var catKendo = $("#category").data("kendoMultiSelect");
			var lstCatKendo = catKendo.value();
			if (lstCatKendo != null && lstCatKendo != undefined && lstCatKendo.length > 0) {
				var catIdStr = lstCatKendo[0];
				for (var i = 1, size = lstCatKendo.length; i < size; i++) {
					catIdStr = catIdStr + "," + lstCatKendo[i];
				}
				dataModel.catIdStr = catIdStr;
			} else {
				dataModel.catIdStr = '';
			}
			//Lay tham so nganh hang con
			var catChildKendo = $("#categoryChild").data("kendoMultiSelect");
			var lstCatChildKendo = catChildKendo.value();
			if (lstCatChildKendo != null && lstCatChildKendo != undefined && lstCatChildKendo.length > 0) {
				var sbCatIdStr = lstCatChildKendo[0];
				for (var i = 1, size = lstCatChildKendo.length; i < size; i++) {
					sbCatIdStr = sbCatIdStr + "," + lstCatChildKendo[i];
				}
				dataModel.subCatIdStr = sbCatIdStr;
			} else {
				dataModel.subCatIdStr = '';
			}
			dataModel.shopId = $('#shop').combotree('getValue');
			dataModel.cycleId = $('#cycle').val();
			ReportUtils.exportReport('/report/ho/rpt_aso/export', dataModel);
		},
		/**
		 * Xu ly su load combo tree shop
		 * @author hunglm16
		 * @param controlId
		 * @param storeCode
		 * @param defaultValue
		 * @param callback
		 * @param params
		 * @returns
		 * @since 19/05/2015
		 * @description danh cho bao cao khuyen mai
		 */
		loadComboTree: function(controlId, storeCode, defaultValue, callback, params) {		 
			var _url= '/rest/report/shop/combotree/1/0.json';
			$('#' + controlId).combotree({url: _url, 
				formatter: function(node) {
					return Utils.XSSEncode(node.text);
				},
				onChange: function (newValue, oldValue) {
					var html = '<input id="promotionProgram" type="text" data-placeholder="Tất cả" style="width:215px; height: auto;" class="InputTextStyle InputText1Style" />';
					$('#ddHTMLPromotionProgram').html(html).change();
					//Xu ly load lai CTKM
					HoReport.changePromotionProgramByShop(StatusType.ACTIVE, $('#' + controlId).combotree('getValue'));
					var tmAZ = setTimeout(function() {
						$('#promotionProgram_listbox div').width(450);
						clearTimeout(tmAZ);
					}, 700);
				}
			});
			var t = $('#'+ controlId).combotree('tree');	
			t.tree('options').url = '';
			t.tree({			
				formatter: function(node) {
					return Utils.XSSEncode(node.text);
				},
				onSelect:function(node) {	
					$('#'+storeCode).val(node.id);
					if(callback!=undefined && callback!=null){
						callback.call(this, node.id);
					}
				},
			});
			if (defaultValue == undefined || defaultValue == null) {
				$('#' + controlId).combotree('setValue', activeType.ALL);
			} else {
				$('#' + controlId).combotree('setValue', defaultValue);
			}
		},
		/**
		 * Xu ly su kien thay doi cua combo tree shop
		 * @author hunglm16
		 * @param status
		 * @param shopId
		 * @returns
		 * @since 19/05/2015
		 * @description danh cho bao cao khuyen mai
		 */
		changePromotionProgramByShop: function(status, shopId) {
			if (status == undefined || status == null) {
				status = StatusType.ALL;
			}
			if (shopId == undefined || shopId == null) {
				shopId = StatusType.ALL;
			}
			var urlJS = "/rest/report/progamram_promotion/kendoui-combobox";
			urlJS = urlJS + "/" + status;
			urlJS = urlJS + "/" + shopId;
			urlJS = urlJS + ".json";
			$("#promotionProgram").kendoMultiSelect({
		        dataTextField: "promotionProgramCode",
		        dataValueField: "id",
		        filter: "contains",
				itemTemplate: function (data, e, s, h, q) {
					return '<div node-id="'+Utils.XSSEncode(data.equipStockId)+'" style="width:230px"><span><div style="width: 15px; display: inline-block;"></div></span><span>'+Utils.XSSEncode(data.promotionProgramCode) + ' - ' + Utils.XSSEncode(data.promotionProgramName)+'</span></div>';
				},
				tagTemplate: '#: data.promotionProgramCode #',
		        change: function(e) {
		        },
		        dataSource: {
		            transport: {
		                read: {
		                    dataType: "json",
		                    url: urlJS.trim()
		                }
		            }
		        }
		        ,value: [shopId]
		    });
		},
		
		/**
		 * Xuat bao cao 5.1 Tong ket khuyen mai
		 * 
		 * @author hunglm16
		 * @since 19/09/2015
		 */
		reportKM5D1TKKM: function() {
			$('.ErrorMsgStyle').html('').change();
			var msg = '';
			if (msg.length == 0) {
				msg = Utils.getMessageOfRequireCheck('fromDate', 'Từ ngày');
			}
			if (msg.length == 0) {
				msg = Utils.getMessageOfRequireCheck('toDate', 'Đến ngày');
			}
			if (msg.length == 0) {
				msg = Utils.getMessageOfInvalidFormatDate('fromDate','Từ ngày');
			}
			if (msg.length == 0) {
				msg = Utils.getMessageOfInvalidFormatDate('toDate','Đến ngày');
			}
			var fDate = $('#fromDate').val();
			var tDate = $('#toDate').val();
			if (!Utils.compareDate(fDate, tDate)) {
				msg = js_fromdate_todate_validate;		
				$('#fDate').focus();
			}
			if (msg.length == 0) {
				msg = Utils.compareCurrentDateEx('toDate','Đến ngày');
			}
			if (msg.length > 0) {
				$('#errMsg').html(msg).show();
				return false;
			}
			
			var proKendo = $("#promotionProgram").data("kendoMultiSelect");
			var lstProId = proKendo.value();
			
			var dataModel = new Object();
			dataModel.promationProgramStr = lstProId.toString().trim();
			dataModel.fromDate = $('#fromDate').val();
			dataModel.toDate = $('#toDate').val();
			dataModel.shopStr = $('#shop').combotree('getValue');
			ReportUtils.exportReport('/report/ho/km_5_1_tkkm/export', dataModel);
		},

		/**
		 * Bc theo yeu cau KH
		 * @author vuongmq
		 * @since 06/10/2015
		 * */
		exportGeneral: function() {
			var msg = '';
			$('.ErrorMsgStyle').html('').hide();
			if (msg.length == 0) {
				msg = Utils.getMessageOfRequireCheck('fDate', 'Ngày');
			}
			if (msg.length > 0) {
				$('#errMsg').html(msg).show();
				return false;
			}
			var dataModel = new Object();
			dataModel.shopStr = $('#curShopId').val();
			dataModel.typeReport = $('#type').val();
			dataModel.fromDate = $('#fDate').val().trim();
			ReportUtils.exportReport('/report/ho/general/export', dataModel);
		},
		/**
		 * Bc Don hang phat sinh trong ngay
		 * 
		 * @author hunglm16
		 * @since 19/09/2015
		 */
		report1D1DSPSTN: function() {
			$('.ErrorMsgStyle').html('').change();
			var msg = '';
			var msg = '';
			if (msg.length == 0) {
				msg = Utils.getMessageOfRequireCheck('fromDate', reportMultiLanguage.fromDate);
			}
			if (msg.length == 0) {
				msg = Utils.getMessageOfRequireCheck('toDate', reportMultiLanguage.toDate);
			}
			if (msg.length == 0) {
				msg = Utils.getMessageOfInvalidFormatDate('fromDate', reportMultiLanguage.fromDate);
			}
			if (msg.length == 0) {
				msg = Utils.getMessageOfInvalidFormatDate('toDate', reportMultiLanguage.toDate);
			}
			var fDate = $('#fromDate').val();
			var tDate = $('#toDate').val();
			if (!Utils.compareDate(fDate, tDate)) {
				msg = js_fromdate_todate_validate;		
				$('#fDate').focus();
			}
			if (msg.length == 0) {
				msg = Utils.compareCurrentDateEx('toDate', reportMultiLanguage.toDate);
			}
			if (msg.length > 0) {
				$('#errMsg').html(msg).show();
				return false;
			}
			
			var dataModel = new Object();
			dataModel.fromDate = $('#fromDate').val();
			dataModel.toDate = $('#toDate').val();
//			dataModel.cycleId = $('#cycle').val();
			dataModel.shopStr = $('#shop').combotree('getValue');
			ReportUtils.exportReport('/report/ho/rpt_1_1_dhpstn/export', dataModel);
		},
		
		/**
		 * Bc Thong ke don hang giao trong ngay
		 * 
		 * @author hunglm16
		 * @since 22/10/2015
		 */
		report1D2DSPSTN: function() {
			$('.ErrorMsgStyle').html('').change();
			var msg = '';
			if (msg.length == 0) {
				msg = Utils.getMessageOfRequireCheck('fromDate', reportMultiLanguage.fromDate);
			}
			if (msg.length == 0) {
				msg = Utils.getMessageOfRequireCheck('toDate', reportMultiLanguage.toDate);
			}
			if (msg.length == 0) {
				msg = Utils.getMessageOfInvalidFormatDate('fromDate', reportMultiLanguage.fromDate);
			}
			if (msg.length == 0) {
				msg = Utils.getMessageOfInvalidFormatDate('toDate', reportMultiLanguage.toDate);
			}
			var fDate = $('#fromDate').val();
			var tDate = $('#toDate').val();
			if (!Utils.compareDate(fDate, tDate)) {
				msg = js_fromdate_todate_validate;		
				$('#fDate').focus();
			}
			if (msg.length == 0) {
				msg = Utils.compareCurrentDateEx('toDate', reportMultiLanguage.toDate);
			}
			if (msg.length > 0) {
				$('#errMsg').html(msg).show();
				return false;
			}
			
			var dataModel = new Object();
			dataModel.fromDate = $('#fromDate').val();
			dataModel.toDate = $('#toDate').val();
//			dataModel.cycleId = $('#cycle').val();
			dataModel.shopStr = $('#shop').combotree('getValue');
			ReportUtils.exportReport('/report/ho/rpt_1_2_dhpstn/export', dataModel);
		},
		
		/**
		 * [1.3] Bao cao Giao hang (don hang) theo NVGH
		 * 
		 * @author hunglm16
		 * @since 22/10/2015
		 */
		report1D3DHTNVGH: function() {
			$('.ErrorMsgStyle').html('').change();
			var msg = '';
			if (msg.length == 0) {
				msg = Utils.getMessageOfRequireCheck('fromDate', reportMultiLanguage.fromDate);
			}
			if (msg.length == 0) {
				msg = Utils.getMessageOfRequireCheck('toDate', reportMultiLanguage.toDate);
			}
			if (msg.length == 0) {
				msg = Utils.getMessageOfInvalidFormatDate('fromDate', reportMultiLanguage.fromDate);
			}
			if (msg.length == 0) {
				msg = Utils.getMessageOfInvalidFormatDate('toDate', reportMultiLanguage.toDate);
			}
			var fDate = $('#fromDate').val();
			var tDate = $('#toDate').val();
			if (!Utils.compareDate(fDate, tDate)) {
				msg = js_fromdate_todate_validate;		
				$('#fDate').focus();
			}
			if (msg.length == 0) {
				msg = Utils.compareCurrentDateEx('toDate', reportMultiLanguage.toDate);
			}
			if (msg.length > 0) {
				$('#errMsg').html(msg).show();
				return false;
			}
			var dataModel = new Object();
			dataModel.fromDate = $('#fromDate').val();
			dataModel.toDate = $('#toDate').val();
//			dataModel.cycleId = $('#cycle').val();
			dataModel.shopStr = $('#shop').combotree('getValue');
			ReportUtils.exportReport('/report/ho/rpt_1_3_dhtnvgh/export', dataModel);
		},
		
		/**
		 * [1.4] Bao cao don hang rot
		 * 
		 * @author hunglm16
		 * @since 30/10/2015
		 */
		report1D4BCDHR: function() {
			$('.ErrorMsgStyle').html('').change();
			var msg = '';
			if (msg.length == 0) {
				msg = Utils.getMessageOfRequireCheck('fromDate', 'Từ ngày');
			}
			if (msg.length == 0) {
				msg = Utils.getMessageOfRequireCheck('toDate', 'Đến ngày');
			}
			if (msg.length == 0) {
				msg = Utils.getMessageOfInvalidFormatDate('fromDate','Từ ngày');
			}
			if (msg.length == 0) {
				msg = Utils.getMessageOfInvalidFormatDate('toDate','Đến ngày');
			}
			var fDate = $('#fromDate').val();
			var tDate = $('#toDate').val();
			if (!Utils.compareDate(fDate, tDate)) {
				msg = js_fromdate_todate_validate;		
				$('#fDate').focus();
			}
			if (msg.length == 0) {
				msg = Utils.compareCurrentDateEx('toDate','Đến ngày');
			}
			if (msg.length > 0) {
				$('#errMsg').html(msg).show();
				return false;
			}
			var dataModel = new Object();
			dataModel.fromDate = $('#fromDate').val();
			dataModel.toDate = $('#toDate').val();
//			dataModel.cycleId = $('#cycle').val();
			dataModel.shopStr = $('#shop').combotree('getValue');
			ReportUtils.exportReport('/report/ho/rpt_1_4_bcdhr/export', dataModel);
		},
		
		/**
		 * Bc SLDS theo Khach Hang
		 * @author vuongmq
		 * @since 26/10/2015
		 * */
		exportSLDSKH: function() {
			var msg = '';
			$('#errMsg').html('').hide();
			var productKendo = $("#lstProduct").data("kendoMultiSelect");
			var lstProduct = productKendo.value();
			var dataProduct = null;
			if (productKendo != null) {
				dataProduct = productKendo.dataItems();
			}
			if ($('#shopId').val().trim().length == 0 || $('#shopId').val().trim() == activeType.ALL) {
				msg = format(msgErr_required_field,'đơn vị');
			}
			if (msg.length == 0) {
				msg = Utils.getMessageOfRequireCheck('fDate','Từ ngày');
			}
			if (msg.length == 0) {
				msg = Utils.getMessageOfRequireCheck('tDate','Đến ngày');
			}
			if (msg.length > 0) {
				$('#errMsg').html(msg).show();
				return false;
			}
			if (!checkDateForCycle()) {
				return false;
			}
			var dataModel = new Object();
			var lstProductName = [];
			for (var i = 0; i < dataProduct.length; i++) {
				lstProductName.push(dataProduct[i].nvbhName);
			}
			dataModel.strListShopId = $('#shopId').val();
			dataModel.fromDate = $('#fDate').val().trim();
			dataModel.toDate = $('#tDate').val().trim();
			dataModel.cycleId = $('#cycle').val();
			if ($('#category').val() > 0) {
				dataModel.categoryId = $('#category').val();
			}
			if ($('#sub_category').val() > 0) {
				dataModel.subCategoryId = $('#sub_category').val();
			}
			if (lstProduct != undefined && lstProduct != null && lstProduct != "") {
				dataModel.strListProductId = lstProduct.toString().trim();
			}
			if (lstProductName != undefined && lstProductName != null && lstProductName != "") {
				dataModel.strListProductName = lstProductName.join(', ');
			}
			ReportUtils.exportReport('/report/ho/sldskh/export', dataModel);
		},

		/**
		 * Bc DS chuong trinh theo Khach Hang
		 * @author vuongmq
		 * @since 27/10/2015
		 * */
		exportDSCTKH: function() {
			var msg = '';
			$('#errMsg').html('').hide();
			if ($('#shopId').val().trim().length == 0 || $('#shopId').val().trim() == activeType.ALL) {
				msg = format(msgErr_required_field, 'đơn vị');
			}
			var ksId = $('#curKsId').val();
			var cycleIdFrom = $('#cycle').val();
			var cycleIdTo = $('#cycleTo').val();
			if (msg.length == 0) {
				if (isNullOrEmpty(ksId) || ksId == -1) {
					msg = 'Vui lòng chọn chương trình';
				}
			}
			if (msg.length == 0) {
				if (isNullOrEmpty(cycleIdFrom)) {
					msg = 'Từ chu kỳ không được để trống';
				}
			}
			if (msg.length == 0) {
				if (isNullOrEmpty(cycleIdTo)) {
					msg = 'Đến chu kỳ không được để trống';
				}
			}
			if (msg.length == 0) {
				if (!isNaN(cycleIdFrom) && !isNaN(cycleIdTo) && Number(cycleIdFrom) > Number(cycleIdTo)) {
					msg = 'Đến chu kỳ phải cùng hoặc sau từ chu kỳ';
				}
			}
			if (msg.length > 0) {
				$('#errMsg').html(msg).show();
				return false;
			}
			var dataModel = new Object();
			dataModel.strListShopId = $('#shopId').val();
			dataModel.ksId = ksId;
			dataModel.cycleId = cycleIdFrom;
			dataModel.cycleIdTo = cycleIdTo;
			ReportUtils.exportReport('/report/ho/dsctkh/export', dataModel);
		},
		
		/**
		 * Bao cao tien do thuc hien chuong trinh 5.5
		 * @author trietptm
		 * @since Nov 28, 2015
		 * */
		exportProgressProgram: function() {
			var msg = '';
			$('#errMsg').html('').hide();
			if ($('#shopId').val().trim().length == 0 || $('#shopId').val().trim() == activeType.ALL) {
				msg = format(msgErr_required_field, 'đơn vị');
			}
			var ksId = $('#curKsId').val();
			var cycleIdFrom = $('#cycle').val();
			var cycleIdTo = $('#cycleTo').val();
			if (msg.length == 0) {
				if (isNullOrEmpty(ksId) || ksId == -1) {
					msg = 'Vui lòng chọn chương trình';
				}
			}
			if (msg.length == 0) {
				if (isNullOrEmpty(cycleIdFrom)) {
					msg = 'Từ chu kỳ không được để trống';
				}
			}
			if (msg.length == 0) {
				if (isNullOrEmpty(cycleIdTo)) {
					msg = 'Đến chu kỳ không được để trống';
				}
			}
			if (msg.length == 0) {
				if (!isNaN(cycleIdFrom) && !isNaN(cycleIdTo) && Number(cycleIdFrom) > Number(cycleIdTo)) {
					msg = 'Đến chu kỳ phải cùng hoặc sau từ chu kỳ';
				}
			}
			if (msg.length > 0) {
				$('#errMsg').html(msg).show();
				return false;
			}
			var dataModel = new Object();
			dataModel.strListShopId = $('#shopId').val();
			dataModel.ksId = ksId;
			dataModel.cycleId = cycleIdFrom;
			dataModel.cycleIdTo = cycleIdTo;
			ReportUtils.exportReport('/report/ho/progress_program/export', dataModel);
		},

		/**
		 * Xu ly khi thay doi don vi; lay danh sach 
		 * @author vuongmq
		 * @since 19/10/2015
		 */
		viewCycleKeyShop: function(ksId) {
			if (ksId != undefined && ksId != null) {
				$.getJSON('/commons/view-cycle-key-shop?ksId=' + ksId, function(data) {
					$('#cycle').html('');
					$('#cycleTo').html('');
					if (data != undefined && data != null) {
						var rows = data.lstCycle;
						if (rows != undefined && rows != null && rows.length > 0) { 
							for (var i = 0; i < rows.length; i++) {
								$('#cycle').append('<option value = "' + Utils.XSSEncode(rows[i].cycleId) + '">' + Utils.XSSEncode(rows[i].cycleName) + '</option>'); 
								$('#cycleTo').append('<option value = "' + Utils.XSSEncode(rows[i].cycleId) + '">' + Utils.XSSEncode(rows[i].cycleName) + '</option>'); 
							}
						}
						$('#cycle').val(data.fromCycleId).change();
						$('#cycleTo').val(data.toCycleId).change();
					}
				});
			}
		},
		
		/**
		 * 6.4.4 bao cao Raw Data
		 * @author trietptm
		 * @since Nov 05, 2015
		 */
		exportRawData: function() {
			var msg = '';
			$('#errMsg').html('').hide();
			if ($('#shopId').val().trim().length == 0 || $('#shopId').val().trim() == activeType.ALL) {
				msg = format(msgErr_required_field, 'đơn vị');
			}
			if (msg.length == 0) {
				msg = Utils.getMessageOfRequireCheck('fDate', 'Từ ngày');
			}
			if (msg.length == 0) {
				msg = Utils.getMessageOfRequireCheck('tDate', 'Đến ngày');
			}
			if (msg.length == 0) {
				msg = Utils.getMessageOfInvalidFormatDate('fDate','Từ ngày');
			}
			if (msg.length == 0) {
				msg = Utils.getMessageOfInvalidFormatDate('tDate','Đến ngày');
			}
			var fDate = $('#fDate').val();
			var tDate = $('#tDate').val();
			if (!Utils.compareDate(fDate, tDate)) {
				msg = js_fromdate_todate_validate;		
				$('#fDate').focus();
			}
			if (msg.length == 0) {
				msg = Utils.compareCurrentDateEx('tDate','Đến ngày');
			}
			if (msg.length > 0) {
				$('#errMsg').html(msg).show();
				return false;
			}
			var dataModel = new Object();
			dataModel.strListShopId = $('#shopId').val();
			dataModel.fromDate = $('#fDate').val().trim();
			dataModel.toDate = $('#tDate').val().trim();
			ReportUtils.exportReport('/report/ho/raw-data/export', dataModel);
		},
		
		/**
		 * 6.4.5 bao cao Raw Data cho don dieu chinh
		 * @author trietptm
		 * @since Nov 16, 2015
		 */
		exportRawDataStockTrans: function() {
			var msg = '';
			$('#errMsg').html('').hide();
			if ($('#shopId').val().trim().length == 0 || $('#shopId').val().trim() == activeType.ALL) {
				msg = format(msgErr_required_field, 'đơn vị');
			}
			if (msg.length == 0) {
				msg = Utils.getMessageOfRequireCheck('fDate', 'Từ ngày');
			}
			if (msg.length == 0) {
				msg = Utils.getMessageOfRequireCheck('tDate', 'Đến ngày');
			}
			if (msg.length == 0) {
				msg = Utils.getMessageOfInvalidFormatDate('fDate','Từ ngày');
			}
			if (msg.length == 0) {
				msg = Utils.getMessageOfInvalidFormatDate('tDate','Đến ngày');
			}
			var fDate = $('#fDate').val();
			var tDate = $('#tDate').val();
			if (!Utils.compareDate(fDate, tDate)) {
				msg = js_fromdate_todate_validate;		
				$('#fDate').focus();
			}
			if (msg.length == 0) {
				msg = Utils.compareCurrentDateEx('tDate','Đến ngày');
			}
			if (msg.length > 0) {
				$('#errMsg').html(msg).show();
				return false;
			}
			var dataModel = new Object();
			dataModel.strListShopId = $('#shopId').val();
			dataModel.fromDate = $('#fDate').val().trim();
			dataModel.toDate = $('#tDate').val().trim();
			ReportUtils.exportReport('/report/ho/raw-data-stock-trans/export', dataModel);
		},
		
		/**
		* Bc cham cong
		* @author trietptm
		* @since Oct 27, 2015
		*/
		reportTimeKeeping: function() {
			var msg = '';
			$('#errMsg').html('').hide();		
			if ($('#shopId').val().trim().length == 0 || $('#shopId').val().trim() == activeType.ALL) {
				msg = format(msgErr_required_field,'đơn vị');
			}		
			if (msg.length > 0) {
				$('#errMsg').html(msg).show();
				return false;
			}
			if (!checkDateForCycle()) {
				return;
			}
			var dataModel = new Object();		
			dataModel.strListShopId=$('#shopId').val();
			dataModel.cycleId = $('#cycle').val();			
			ReportUtils.exportReport('/report/ho/time_keeping/export', dataModel);
		},

		/**
		 * Bc Aso khong hoat dong
		 * @author vuongmq
		 * @since 03/11/2015
		 * */
		exportAsoInactive: function() {
			var msg = '';
			$('#errMsg').html('').hide();
			msg = Utils.getMessageOfRequireCheck('cycle', 'Chu kỳ');
			if (msg.length > 0) {
				$('#errMsg').html(msg).show();
				return false;
			}
			var dataModel = new Object();
			dataModel.strListShopId = $('#shopId').val();
			dataModel.cycleId = $('#cycle').val();
			ReportUtils.exportReport('/report/ho/aso-inactive/export', dataModel);
		},
		
		/**
		 * Bc thoi gian ghe tham cua KH
		 * @author vuongmq
		 * @since 05/11/2015
		 * */
		exportTGGTKH: function() {
			var msg = '';
			$('#errMsg').html('').hide();
			if ($('#shopId').val().trim().length == 0 || $('#shopId').val().trim() == activeType.ALL) {
				msg = format(msgErr_required_field, 'đơn vị');
			}
			var msg = '';
			if (msg.length == 0) {
				msg = Utils.getMessageOfRequireCheck('fDate', 'Từ ngày');
			}
			if (msg.length == 0) {
				msg = Utils.getMessageOfRequireCheck('tDate', 'Đến ngày');
			}
			if (msg.length == 0) {
				msg = Utils.getMessageOfInvalidFormatDate('fDate','Từ ngày');
			}
			if (msg.length == 0) {
				msg = Utils.getMessageOfInvalidFormatDate('tDate','Đến ngày');
			}
			var fDate = $('#fDate').val();
			var tDate = $('#tDate').val();
			if (!Utils.compareDate(fDate, tDate)) {
				msg = js_fromdate_todate_validate;		
				$('#fDate').focus();
			}
			if (msg.length == 0) {
				msg = Utils.compareCurrentDateEx('tDate','Đến ngày');
			}
			if (msg.length > 0) {
				$('#errMsg').html(msg).show();
				return false;
			}
			var dataModel = new Object();
			dataModel.strListShopId = $('#shopId').val();
			dataModel.fromDate = $('#fDate').val().trim();
			dataModel.toDate = $('#tDate').val().trim();
			ReportUtils.exportReport('/report/ho/tggtkh/export', dataModel);
		},
		
		/**
		 * Bc khong ghe tham KH trong tuyen
		 * @author vuongmq
		 * @since 06/11/2015
		 * */
		exportKGTKHTT: function() {
			$('.ErrorMsgStyle').html('').change();
			var msg = '';
			$('#errMsg').html('').hide();
			var shopId = $('#shopId').val();
			if (shopId.trim().length == 0 || shopId.trim() == activeType.ALL) {
				msg = format(msgErr_required_field, 'Đơn vị');
			}
			var msg = '';
			if (msg.length == 0) {
				msg = Utils.getMessageOfRequireCheck('fDate', 'Từ ngày');
			}
			if (msg.length == 0) {
				msg = Utils.getMessageOfRequireCheck('tDate', 'Đến ngày');
			}
			if (msg.length == 0) {
				msg = Utils.getMessageOfInvalidFormatDate('fDate','Từ ngày');
			}
			if (msg.length == 0) {
				msg = Utils.getMessageOfInvalidFormatDate('tDate','Đến ngày');
			}
			var fDate = $('#fDate').val();
			var tDate = $('#tDate').val();
			if (!Utils.compareDate(fDate, tDate)) {
				msg = js_fromdate_todate_validate;		
				$('#fDate').focus();
			}
			if (msg.length == 0) {
				msg = Utils.compareCurrentDateEx('tDate', 'Đến ngày');
			}
			if (msg.length > 0) {
				$('#errMsg').html(msg).show();
				return false;
			}
			var dataModel = new Object();
			dataModel.strListShopId = shopId;
			dataModel.fromDate = fDate;
			dataModel.toDate = tDate;
			ReportUtils.exportReport('/report/ho/kgtkhtt/export', dataModel);
		},
		
		/**
		 * Bc chi tiet khuyen mai
		 * @author duongdt3
		 * @since 06/11/2015
		 * */
		exportBCCTKM: function() {
			$('.ErrorMsgStyle').html('').change();
			var msg = '';
			if (msg.length == 0) {
				msg = Utils.getMessageOfRequireCheck('fDate', reportMultiLanguage.fromDate);
			}
			if (msg.length == 0) {
				msg = Utils.getMessageOfRequireCheck('tDate', reportMultiLanguage.toDate);
			}
			if (msg.length == 0) {
				msg = Utils.getMessageOfInvalidFormatDate('fDate', reportMultiLanguage.fromDate);
			}
			if (msg.length == 0) {
				msg = Utils.getMessageOfInvalidFormatDate('tDate', reportMultiLanguage.toDate);
			}
			var fDate = $('#fDate').val();
			var tDate = $('#tDate').val();
			if (!Utils.compareDate(fDate, tDate)) {
				msg = js_fromdate_todate_validate;		
				$('#fDate').focus();
			}
			if (msg.length == 0) {
				msg = Utils.compareCurrentDateEx('tDate', reportMultiLanguage.toDate);
			}
			if (msg.length > 0) {
				$('#errMsg').html(msg).show();
				return false;
			}
			var dataModel = new Object();
			dataModel.strListShopId = $('#shopId').val();
			dataModel.fromDate = $('#fDate').val();
			dataModel.toDate = $('#tDate').val();
			ReportUtils.exportReport('/report/ho/bcctkm/export', dataModel);
		},
		
		/**
		 * [6.1.3] Bao cao don hang rot
		 * 
		 * @author hunglm16
		 * @since 09/11/2015
		 */
		reportGrowthSKUs: function() {
			$('.ErrorMsgStyle').html('').change();
			var dataModel = new Object();
			dataModel.year = $('#year').val();
			dataModel.shopId = $('#shop').combotree('getValue');
			ReportUtils.exportReport('/report/ho/rpt_growth_skus/export-excel', dataModel);
		},

		/**
		 * Bc Chi tiet mua hang
		 * @author vuongmq
		 * @since 29/12/2015
		 */
		report2D3CTMH: function() {
			$('.ErrorMsgStyle').html('').change();
			var msg = '';
			if (msg.length == 0) {
				msg = Utils.getMessageOfRequireCheck('fromDate', reportMultiLanguage.fromDate);
			}
			if (msg.length == 0) {
				msg = Utils.getMessageOfRequireCheck('toDate', reportMultiLanguage.toDate);
			}
			if (msg.length == 0) {
				msg = Utils.getMessageOfInvalidFormatDate('fromDate', reportMultiLanguage.fromDate);
			}
			if (msg.length == 0) {
				msg = Utils.getMessageOfInvalidFormatDate('toDate', reportMultiLanguage.toDate);
			}
			var fDate = $('#fromDate').val();
			var tDate = $('#toDate').val();
			if (!Utils.compareDate(fDate, tDate)) {
				msg = js_fromdate_todate_validate;		
				$('#fDate').focus();
			}
			if (msg.length == 0) {
				msg = Utils.compareCurrentDateEx('toDate', reportMultiLanguage.toDate);
			}
			if (msg.length > 0) {
				$('#errMsg').html(msg).show();
				return false;
			}
			var dataModel = new Object();
			dataModel.fromDate = $('#fromDate').val();
			dataModel.toDate = $('#toDate').val();
			dataModel.strListShopId = $('#shop').combotree('getValue'); // or $('#shop').val()
			ReportUtils.exportReport('/report/ho/rpt_2d3_ctmh/export', dataModel);
		},
		
		/**
		 * Bang ke chi tiet hang hoa mua vao
		 * @author dungnt19
		 * @since 19/01/2016
		 */
		report2D1CTHHMV: function() {
			$('.ErrorMsgStyle').html('').change();
			var msg = '';
			if (msg.length == 0) {
				msg = Utils.getMessageOfRequireCheck('fromDate', reportMultiLanguage.fromDate);
			}
			if (msg.length == 0) {
				msg = Utils.getMessageOfRequireCheck('toDate', reportMultiLanguage.toDate);
			}
			if (msg.length == 0) {
				msg = Utils.getMessageOfInvalidFormatDate('fromDate', reportMultiLanguage.fromDate);
			}
			if (msg.length == 0) {
				msg = Utils.getMessageOfInvalidFormatDate('toDate', reportMultiLanguage.toDate);
			}
			var fDate = $('#fromDate').val();
			var tDate = $('#toDate').val();
			if (!Utils.compareDate(fDate, tDate)) {
				msg = js_fromdate_todate_validate;		
				$('#fDate').focus();
			}
			if (msg.length == 0) {
				msg = Utils.compareCurrentDateEx('toDate', reportMultiLanguage.toDate);
			}
			if (msg.length > 0) {
				$('#errMsg').html(msg).show();
				return false;
			}
			var dataModel = new Object();
			dataModel.fromDate = $('#fromDate').val();
			dataModel.toDate = $('#toDate').val();
			dataModel.strListShopId = $('#shop').combotree('getValue'); // or $('#shop').val()
			ReportUtils.exportReport('/report/ho/rpt_2d1_cthhmv/export', dataModel);
		},
		
		/**
		 * So sanh so luong dat hang va giao hang theo don hang mua
		 * @author dungnt19
		 * @since 19/01/2016
		 */
		report2D2SSSLDHMH: function() {
			$('.ErrorMsgStyle').html('').change();
			var msg = '';
			if (msg.length == 0) {
				msg = Utils.getMessageOfRequireCheck('fromDate', reportMultiLanguage.fromDate);
			}
			if (msg.length == 0) {
				msg = Utils.getMessageOfRequireCheck('toDate', reportMultiLanguage.toDate);
			}
			if (msg.length == 0) {
				msg = Utils.getMessageOfInvalidFormatDate('fromDate', reportMultiLanguage.fromDate);
			}
			if (msg.length == 0) {
				msg = Utils.getMessageOfInvalidFormatDate('toDate', reportMultiLanguage.toDate);
			}
			var fDate = $('#fromDate').val();
			var tDate = $('#toDate').val();
			if (!Utils.compareDate(fDate, tDate)) {
				msg = js_fromdate_todate_validate;		
				$('#fDate').focus();
			}
			if (msg.length == 0) {
				msg = Utils.compareCurrentDateEx('toDate', reportMultiLanguage.toDate);
			}
			if (msg.length > 0) {
				$('#errMsg').html(msg).show();
				return false;
			}
			var dataModel = new Object();
			dataModel.fromDate = $('#fromDate').val();
			dataModel.toDate = $('#toDate').val();
			dataModel.strListShopId = $('#shop').combotree('getValue'); // or $('#shop').val()
			ReportUtils.exportReport('/report/ho/rpt_2d2_sssldhmh/export', dataModel);
		},
};
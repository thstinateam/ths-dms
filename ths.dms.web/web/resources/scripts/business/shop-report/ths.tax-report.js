var TaxReport = {	
	exportCTHDGTGT: function(){
		$('#errMsg').html('').hide();
		var msg = '';
		if($('#shop').combotree('getValue').trim().length == 0 || $('#shop').combotree('getValue').trim() == activeType.ALL){
			msg = format(msgErr_required_field,'đơn vị');
		}
		
		if(msg.length == 0){
			if($('#isNPP').val() == "false"){
				msg = "Đơn vị bạn vừa chọn không phải là 1 nhà phân phối cụ thể!";
			}
		}
		
		if(msg.length == 0){
			msg = Utils.getMessageOfRequireCheck('fDate','Từ ngày');
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfInvalidFormatDate('fDate','Từ ngày');
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfRequireCheck('tDate','Đến ngày');
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfInvalidFormatDate('tDate','Đến ngày');
		}

		var fDate = $('#fDate').val();
		var tDate = $('#tDate').val();
		if(!Utils.compareDate(fDate, tDate)){
			msg = msgErr_fromdate_greater_todate;		
			$('#fDate').focus();
		}
		var currentTime = new Date();
		var month = currentTime.getMonth() + 1;
		var day = currentTime.getDate();
		var year = currentTime.getFullYear();
		if(!Utils.compareDate(fDate,day + '/' + month + '/' + year)){
			msg = 'Từ ngày không được lớn hơn ngày hiện tại';
			$('#fDate').focus();
		}
		if(msg.length > 0){
			$('#errMsg').html(msg).show();
			return false;
		}
		var data = new Object();		
		data.shopId = $('#shop').combotree('getValue').trim();
		data.staffCode = $('#staffCode').val().trim();
		data.fromDate = $('#fDate').val().trim();
		data.toDate = $('#tDate').val().trim();
		data.tax = $('#tax').val().trim();
		data.status = $('#status').combobox('getValue').trim();
		data.type = $('#type').val().trim();
		var kData = $.param(data,true);
		$('#divOverlay').show();		
		$.ajax({
			type : "POST",
			url : "/report/tax/cthd-gtgt/export",
			data :(kData),
			dataType: "json",
			success : function(data) {
				$('#divOverlay').hide();				
				if(!data.error) {
					if(data.hasData != null && data.hasData != undefined && data.hasData == true) {
						window.location.href = data.path;
						setTimeout(function(){ //Set timeout để đảm bảo file load lên hoàn tất
		                    CommonSearch.deleteFileExcelExport(data.path);
						},2000);
					} else {
						$('#errMsg').html('Không có dữ liệu để xuất').show();
					}
				} else {
					$('#errMsg').html(data.errMsg).show();					
				}
			},
			error:function(XMLHttpRequest, textStatus, errorDivThrown) {
				$('#divOverlay').hide();
				StockIssued.xhrExport = null;
			}
		});
		return false;
	},
	
	export7_7_8: function(){
		$('#errMsg').html('').hide();
		var msg = '';
		if($('#shop').combotree('getValue').trim().length == 0 || $('#shop').combotree('getValue').trim() == activeType.ALL){
			msg = format(msgErr_required_field,'đơn vị');
		}
		
		if(msg.length == 0){
			if($('#isNPP').val() == "false"){
				msg = "Đơn vị bạn vừa chọn không phải là 1 nhà phân phối cụ thể!";
			}
		}
		
		if(msg.length == 0){
			msg = Utils.getMessageOfRequireCheck('fDate','Từ ngày');
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfInvalidFormatDate('fDate','Từ ngày');
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfRequireCheck('tDate','Đến ngày');
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfInvalidFormatDate('tDate','Đến ngày');
		}

		var fDate = $('#fDate').val();
		var tDate = $('#tDate').val();
		if(!Utils.compareDate(fDate, tDate)){
			msg = msgErr_fromdate_greater_todate;		
			$('#fDate').focus();
		}
		var currentTime = new Date();
		var month = currentTime.getMonth() + 1;
		var day = currentTime.getDate();
		var year = currentTime.getFullYear();
		if(!Utils.compareDate(fDate,day + '/' + month + '/' + year)){
			msg = 'Từ ngày không được lớn hơn ngày hiện tại';
			$('#fDate').focus();
		}
		if(msg.length > 0){
			$('#errMsg').html(msg).show();
			return false;
		}
		var data = new Object();		
		data.shopId = $('#shop').combotree('getValue').trim();
		data.fromDate = $('#fDate').val().trim();
		data.toDate = $('#tDate').val().trim();
		
		data.formatType = $('input[name="formatType"]:checked').val();

		var kData = $.param(data,true);
		$('#divOverlay').show();		
		$.ajax({
			type : "POST",
			url : "/report/tax/thue-rpt7-7-8/export",
			data :(kData),
			dataType: "json",
			success : function(data) {
				$('#divOverlay').hide();				
				if(!data.error) {
					if(data.hasData != null && data.hasData != undefined && data.hasData == true) {
						window.location.href = data.path;
						setTimeout(function(){ //Set timeout để đảm bảo file load lên hoàn tất
		                    CommonSearch.deleteFileExcelExport(data.path);
						},2000);
					} else {
						$('#errMsg').html('Không có dữ liệu để xuất').show();
					}
				} else {
					$('#errMsg').html(data.errMsg).show();					
				}
			},
			error:function(XMLHttpRequest, textStatus, errorDivThrown) {
				$('#divOverlay').hide();
				StockIssued.xhrExport = null;
			}
		});
		return false;
	},
	
	reportHDGTGT: function(){
		var msg = '';
		$('#errMsg').html('').hide();
		msg = Utils.getMessageOfRequireCheck('tax','Thuế suất');
		if(msg.length == 0){
			msg = Utils.getMessageOfSpecialCharactersValidateEx('tax','Thuế suất',Utils._TF_NUMBER);
			$('#tax').focus();			
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfRequireCheck('fDate','Từ ngày');
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfInvalidFormatDate('fDate','Từ ngày');
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfRequireCheck('tDate','Từ ngày');
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfInvalidFormatDate('tDate','Đến ngày');
		}
		var fDate = $('#fDate').val();
		var tDate = $('#tDate').val();
		if(!Utils.compareDate(fDate, tDate)){
			msg = msgErr_fromdate_greater_todate;		
			$('#fDate').focus();
		}
		var currentTime = new Date();
		var month = currentTime.getMonth() + 1;
		var day = currentTime.getDate();
		var year = currentTime.getFullYear();
		if(!Utils.compareDate(fDate,day + '/' + month + '/' + year)){
			msg = 'Từ ngày không được lớn hơn ngày hiện tại';
			$('#fDate').focus();
		}
		
		if(msg.length > 0){
			$('#errMsg').html(msg).show();
			return false;
		}
		var dataModel = new Object();		
		dataModel.taxRate = $('#tax').val().trim();
		dataModel.staffCode = $('#staffCode').val().trim();
		dataModel.fromDate = $('#fDate').val().trim();
		dataModel.toDate = $('#tDate').val().trim();
		dataModel.custPayment = $('#custPayment').val().trim();
		dataModel.orderNumber = $('#orderNumber').val().trim();
		dataModel.customerCode = $('#customerCode').val().trim();		
		dataModel.invoiceNumber = $('#invoiceNumber').val().trim();
		//ReportUtils.exportReport('/report/tax/hdgtgt/export',dataModel);
		ReportUtils.exportReport('/report/tax/hdgtgt/export',dataModel);
	},
	/**
	 * @author vuonghn
	 * @since 13/05/2014
	 * @description Xuat bao cao 7.7.6. Bang ke hoa don gia tri gia tang
	 * */
	export7_7_6: function(){
		
		var msg = '';
		var multiShop = $("#shop").data("kendoMultiSelect");
		var dataShop = multiShop.dataItems();
		$('#errMsg').html('').hide();
		if(dataShop==null||dataShop.length==0){
			msg = format(msgErr_required_field,'Đơn vị');
		}
		var fDate = $('#fDate').val();
		var tDate = $('#tDate').val();
		if(msg.length == 0){
			msg = Utils.getMessageOfRequireCheck('fDate','Từ ngày');
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfInvalidFormatDate('fDate','Từ ngày');
		}
		if(msg.length == 0){
			if(!Utils.compareCurrentDate(fDate)){
				msg = "Từ ngày phải bé hơn hoặc bằng ngày hiện tại.";
			}
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfRequireCheck('tDate','Đến ngày');
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfInvalidFormatDate('tDate','Đến ngày');
		}
		if(msg.length == 0){
			if(!Utils.compareCurrentDate(tDate)){
				msg = "Đến ngày phải bé hơn hoặc bằng ngày hiện tại.";
			}
		}
		if(msg.length == 0){			
			if(!Utils.compareDate(fDate, tDate)){
				msg = msgErr_fromdate_greater_todate;
			}
		}
		
		if(msg.length > 0){
			$('#errMsg').html(msg).show();
			return false;
		}
		var dataModel = new Object();
		if(dataShop!=null && dataShop.length>0){
			var lstShop = dataShop[0].id;
			for(var i=1;i<dataShop.length;i++){
				lstShop += "," + dataShop[i].id;
			}
			dataModel.strListShopId = lstShop;
		}
		//dataModel.shopId = $('#shopId').val().trim();				
		if(dataShop!=null && dataShop.length>0){
			var lstShop = dataShop[0].id;
			for(var i=1;i<dataShop.length;i++){
				lstShop += "," + dataShop[i].id;
			}
			dataModel.listShopId = lstShop;
		}
		dataModel.fromDate = $('#fDate').val().trim();
		dataModel.toDate = $('#tDate').val().trim();
		dataModel.shopCode = $('#shopCode').val().trim();
		dataModel.formatType = $('input[name="formatType"]:checked').val();
		//ReportUtils.exportReport('/report/tax/bkhd-gtgt/export',dataModel);
		ReportUtils.exportReport('/report/tax/bkhd-gtgt/export',dataModel);
	}
};
var StockReport = {	
		
   /**
	* Bc 3.1 xuat nhap ton
	* 
	* @author hoanv25
	* @since July 31, 2015
	* */	
	report7_4_2: function(){
		var msg = '';
		$('#errMsg').html('').hide();
		if($('#shopId').val().trim().length == 0 || $('#shopId').val().trim() == activeType.ALL){
			msg = format(msgErr_required_field,'đơn vị');
		}
		if(msg.length==0){
			if($('#isNPP').val() == "false"){
				msg="Đơn vị bạn vừa chọn không phải là nhà phân phối cụ thể!";
			}
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfRequireCheck('fDate','Từ ngày');
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfInvalidFormatDate('fDate','Từ ngày');
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfRequireCheck('tDate','Từ ngày');
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfInvalidFormatDate('tDate','Đến ngày');
		}
		var fDate = $('#fDate').val();
		var tDate = $('#tDate').val();
		if(!Utils.compareDate(fDate, tDate)){
			msg = msgErr_fromdate_greater_todate;		
			$('#fDate').focus();
		}		
		if(!Utils.compareDate(fDate, tDate)){
			msg = msgErr_fromdate_greater_todate;		
			$('#fDate').focus();
		}
		var currentTime = new Date();
		var month = currentTime.getMonth() + 1;
		var day = currentTime.getDate();
		var year = currentTime.getFullYear();
		if(!Utils.compareDate(fDate,day + '/' + month + '/' + year)){
			msg = 'Từ ngày không được lớn hơn ngày hiện tại';
			$('#fDate').focus();
		}
		if(!Utils.compareDate(tDate,day + '/' + month + '/' + year)){
			msg = 'Đến ngày không được lớn hơn ngày hiện tại';
			$('#tDate').focus();
		}
		if(msg.length > 0){
			$('#errMsg').html(msg).show();
			return false;
		}
		var dataModel = new Object();		
		dataModel.strListShopId=$('#shopId').val();
		dataModel.fromDate = $('#fDate').val().trim();
		dataModel.toDate = $('#tDate').val().trim();
		dataModel.orderStock = $('#orderStock').val();
		if ($('#orderProduct').val() != null && $('#orderProduct').val() > 0) {
			dataModel.orderProduct = $('#orderProduct').val();
		}	
		dataModel.cycleId = $('#cycle').val();			
		if($('input[name=formatType]:radio:checked').length > 0 && $('input[name=formatType]:radio:checked').val() != undefined) {
            dataModel.formatType = $('input[name=formatType]:radio:checked').val().toUpperCase();
		}
		ReportUtils.exportReport('/report/stock/f1/export',dataModel);
	},
	/**
	* Bc 3.1 Thay doi gia tri loai sp theo dieu kien kho chon
	* 
	* @author hoanv25
	* @since August 3, 2015
	* */	
	change: function() {
		if ($('#orderStock').val() == 1) {
			$('#sub .CustomStyleSelectBoxInner').html("Tất cả");
			html = '<option value="0" selected="selected">' + 'Tất cả' + '</option>';
			html += '<option value="1">' + 'Thành phẩm' + '</option>';
			html += '<option value="2">' + 'POSM' + '</option>';
			$('#orderProduct').html(html);
		} else {
			$('#sub .CustomStyleSelectBoxInner').html("Thành phẩm");
			html = '<option value="1" selected="selected">' + 'Thành phẩm' + '</option>';
			$('#orderProduct').html(html);
		}
	},
	/**
	* Bc 3.2 xuat nhap ton chi tiet
	* 
	* @author hoanv25
	* @since August 3, 2015
	* */	
	report7_4_3: function(){
		var msg = '';
		$('#errMsg').html('').hide();
		if($('#shopId').val().trim().length == 0 || $('#shopId').val().trim() == activeType.ALL){
			msg = format(msgErr_required_field,'đơn vị');
		}
		if(msg.length==0){
			if($('#isNPP').val() == "false"){
				msg="Đơn vị bạn vừa chọn không phải là nhà phân phối cụ thể!";
			}
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfRequireCheck('fDate','Từ ngày');
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfInvalidFormatDate('fDate','Từ ngày');
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfRequireCheck('tDate','Từ ngày');
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfInvalidFormatDate('tDate','Đến ngày');
		}
		var fDate = $('#fDate').val();
		var tDate = $('#tDate').val();
		if(!Utils.compareDate(fDate, tDate)){
			msg = msgErr_fromdate_greater_todate;		
			$('#fDate').focus();
		}		
		if(!Utils.compareDate(fDate, tDate)){
			msg = msgErr_fromdate_greater_todate;		
			$('#fDate').focus();
		}
		var currentTime = new Date();
		var month = currentTime.getMonth() + 1;
		var day = currentTime.getDate();
		var year = currentTime.getFullYear();
		if(!Utils.compareDate(fDate,day + '/' + month + '/' + year)){
			msg = 'Từ ngày không được lớn hơn ngày hiện tại';
			$('#fDate').focus();
		}
		if(!Utils.compareDate(tDate,day + '/' + month + '/' + year)){
			msg = 'Đến ngày không được lớn hơn ngày hiện tại';
			$('#tDate').focus();
		}
		if(msg.length > 0){
			$('#errMsg').html(msg).show();
			return false;
		}
		var dataModel = new Object();		
		dataModel.strListShopId=$('#shopId').val();
		dataModel.fromDate = $('#fDate').val().trim();
		dataModel.toDate = $('#tDate').val().trim();
		/*dataModel.orderStock = $('#orderStock').val();
		if ($('#orderProduct').val() != null && $('#orderProduct').val() > 0) {
			dataModel.orderProduct = $('#orderProduct').val();
		}*/
		dataModel.cycleId = $('#cycle').val();
		ReportUtils.exportReport('/report/stock/xntct/export',dataModel);
	},
	
	/**
	* Bc 4.2 ton kho theo nhan vien vansale
	* 
	* @author hoanv25
	* @since August 3, 2015
	* */	
	reportTKTNVVS4_2: function(){
		var msg = '';
		$('#errMsg').html('').hide();
		if($('#shopId').val().trim().length == 0 || $('#shopId').val().trim() == activeType.ALL){
			msg = format(msgErr_required_field,'đơn vị');
		}
		if(msg.length==0){
			if($('#isNPP').val() == "false"){
				msg="Đơn vị bạn vừa chọn không phải là nhà phân phối cụ thể!";
			}
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfRequireCheck('fDate','Từ ngày');
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfInvalidFormatDate('fDate','Từ ngày');
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfRequireCheck('tDate','Đến ngày');
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfInvalidFormatDate('tDate','Đến ngày');
		}
		var fDate = $('#fDate').val();
		var tDate = $('#tDate').val();
		if(!Utils.compareDate(fDate, tDate)){
			msg = msgErr_fromdate_greater_todate;		
			$('#fDate').focus();
		}		

		var currentTime = new Date();
		var month = currentTime.getMonth() + 1;
		var day = currentTime.getDate();
		var year = currentTime.getFullYear();
		if(!Utils.compareDate(fDate,day + '/' + month + '/' + year)){
			msg = 'Từ ngày không được lớn hơn ngày hiện tại';
			$('#fDate').focus();
		}
		if(!Utils.compareDate(tDate,day + '/' + month + '/' + year)){
			msg = 'Đến ngày không được lớn hơn ngày hiện tại';
			$('#tDate').focus();
		}
		if(msg.length > 0){
			$('#errMsg').html(msg).show();
			return false;
		}
		var dataModel = new Object();		
		dataModel.strListShopId=$('#shopId').val();
		dataModel.fromDate = $('#fDate').val().trim();
		dataModel.toDate = $('#tDate').val().trim();
		
		ReportUtils.exportReport('/report/stock/tktnvvs/export',dataModel);
	},
	
	/**
	* Bc 3.3 tinh trang don hang
	* 
	* @author hoanv25
	* @version
	* @since August 5, 2015
	* */	
	report7_4_5: function(){
		var msg = '';
		$('#errMsg').html('').hide();
		if($('#shopId').val().trim().length == 0 || $('#shopId').val().trim() == activeType.ALL){
			msg = format(msgErr_required_field,'đơn vị');
		}
		if(msg.length==0){
			if($('#isNPP').val() == "false"){
				msg="Đơn vị bạn vừa chọn không phải là nhà phân phối cụ thể!";
			}
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfRequireCheck('fDate','Từ ngày');
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfInvalidFormatDate('fDate','Từ ngày');
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfRequireCheck('tDate','Từ ngày');
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfInvalidFormatDate('tDate','Đến ngày');
		}
		var fDate = $('#fDate').val();
		var tDate = $('#tDate').val();
		if(!Utils.compareDate(fDate, tDate)){
			msg = msgErr_fromdate_greater_todate;		
			$('#fDate').focus();
		}		
		if(!Utils.compareDate(fDate, tDate)){
			msg = msgErr_fromdate_greater_todate;		
			$('#fDate').focus();
		}
		var currentTime = new Date();
		var month = currentTime.getMonth() + 1;
		var day = currentTime.getDate();
		var year = currentTime.getFullYear();
		if(!Utils.compareDate(fDate,day + '/' + month + '/' + year)){
			msg = 'Từ ngày không được lớn hơn ngày hiện tại';
			$('#fDate').focus();
		}
		if(msg.length > 0){
			$('#errMsg').html(msg).show();
			return false;
		}
		var dataModel = new Object();		
		dataModel.shopId = $('#shopId').val();
		dataModel.fromDate = $('#fDate').val().trim();
		dataModel.toDate = $('#tDate').val().trim();
		dataModel.orderNumber = $('#orderNumber').val().trim();
		if ($('#orderStatus').val() != null && $('#orderStatus').val() >= 0) {
			dataModel.orderStatus = $('#orderStatus').val();
		}
		dataModel.cycleId = $('#cycle').val();
		ReportUtils.exportReport('/report/stock/bcttnh/export',dataModel);
	},
	
	/**
	* Bc 3.4 bao cao kiem ke
	* 
	* @author hoanv25
	* @since August 3, 2015
	* */	
	report7_4_4: function(){
		var msg = '';
		$('#errMsg').html('').hide();
		if($('#shopId').val().trim().length == 0 || $('#shopId').val().trim() == activeType.ALL){
			msg = format(msgErr_required_field,'đơn vị');
		}
		if(msg.length==0){
			if($('#isNPP').val() == "false"){
				msg="Đơn vị bạn vừa chọn không phải là nhà phân phối cụ thể!";
			}
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfRequireCheck('fDate','Từ ngày');
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfInvalidFormatDate('fDate','Từ ngày');
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfRequireCheck('tDate','Từ ngày');
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfInvalidFormatDate('tDate','Đến ngày');
		}
		var fDate = $('#fDate').val();
		var tDate = $('#tDate').val();
		if(!Utils.compareDate(fDate, tDate)){
			msg = msgErr_fromdate_greater_todate;		
			$('#fDate').focus();
		}		
		if(!Utils.compareDate(fDate, tDate)){
			msg = msgErr_fromdate_greater_todate;		
			$('#fDate').focus();
		}
		var currentTime = new Date();
		var month = currentTime.getMonth() + 1;
		var day = currentTime.getDate();
		var year = currentTime.getFullYear();
		if(!Utils.compareDate(fDate,day + '/' + month + '/' + year)){
			msg = 'Từ ngày không được lớn hơn ngày hiện tại';
			$('#fDate').focus();
		}
		if(msg.length > 0){
			$('#errMsg').html(msg).show();
			return false;
		}
		var dataModel = new Object();		
		dataModel.strListShopId=$('#shopId').val();
		dataModel.fromDate = $('#fDate').val().trim();
		dataModel.toDate = $('#tDate').val().trim();
		dataModel.orderStock = $('#orderStock').val();
		if ($('#orderProduct').val() != null && $('#orderProduct').val() > 0) {
			dataModel.orderProduct = $('#orderProduct').val();
		}	
		dataModel.orderStatus = $('#orderStatus').val();
		dataModel.cycleId = $('#cycle').val();
		ReportUtils.exportReport('/report/stock/bckk/export',dataModel);
	},
	
	reportXNTCT: function(){
		var msg = '';
		$('#errMsg').html('').hide();
		if($('#shopId').val().trim().length == 0 || $('#shopId').val().trim() == activeType.ALL){
			msg = format(msgErr_required_field,'đơn vị');
		}
		if(msg.length == 0) {
			if($('#isNPP').val() == "false") {
				msg = "Đơn vị bạn vừa chọn không phải là 1 nhà phân phối cụ thể!";
			}
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfInvalidFormatDate('fDate','Từ ngày');
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfInvalidFormatDate('tDate','Đến ngày');
		}
		var fDate = $('#fDate').val();
		var tDate = $('#tDate').val();
		if(!Utils.compareDate(fDate, tDate)){
			msg = msgErr_fromdate_greater_todate;		
			$('#fDate').focus();
		}
		if(msg.length > 0){
			$('#errMsg').html(msg).show();
			return false;
		}
		var dataModel = new Object();		
		dataModel.shopId = $('#shopId').val().trim();
		dataModel.fromDate = $('#fDate').val().trim();
		dataModel.toDate = $('#tDate').val().trim();
		dataModel.formatType = $('input[name="formatType"]:checked').val();
		ReportUtils.exportReport('/report/stock/xntct/export',dataModel);
	}
};
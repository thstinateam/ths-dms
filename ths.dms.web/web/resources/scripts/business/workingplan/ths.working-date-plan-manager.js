var WorkingDatePlan = {
	_daysInMonth: [31,28,31,30,31,30,31,31,30,31,30,31],
	_dayInWeek: [work_date_CN, work_date_T2, work_date_T3, work_date_T4, work_date_T5, work_date_T6, work_date_T7],
	_VIEW_BY_YEAR: 0,
	_VIEW_BY_MONTH: 1,
	_xhrSave : null,
	_hideErrExcelMsg: 0,
	_currentShopId: 0,
	viewTabMonth:0,
	_checkType:null,
	_flagCheckExitsParent:false,
	getDaysInMonth: function(month,year){
	    if ((month==1)&&(year%4==0)&&((year%100!=0)||(year%400==0))){
	      return 29;
	    }else{
	      return WorkingDatePlan._daysInMonth[month];
	    }
	},
	getCalendarByMonth: function(month,year,viewType){
		$('#dataBox').hide();
		var vt = WorkingDatePlan._VIEW_BY_YEAR;
		if(viewType!= null && viewType!= undefined){
			vt = viewType;
		}
		var html = new Array();
		if (vt == WorkingDatePlan._VIEW_BY_YEAR) {
			html.push('<h5>' + Utils.XSSEncode(work_date_month_title + ' ' + month) + '</h5>');
			html.push('<p id="numExDay_' + month +'" class="Warning3Style"></p>');
			html.push('<div class="GeneralTable Table21Section">');
		} else {
			html.push('<div class="GeneralTable Table22Section">');
		}
		html.push('<table width="100%" border="0" cellspacing="0" cellpadding="0">');
		html.push('<thead>');
		html.push('<tr>');
		html.push('<th>' + Utils.XSSEncode(work_date_CN) + '</th>');
		html.push('<th>' + Utils.XSSEncode(work_date_th2) + '</th>');
		html.push('<th>' + Utils.XSSEncode(work_date_th3) + '</th>');
		html.push('<th>' + Utils.XSSEncode(work_date_th4) + '</th>');
		html.push('<th>' + Utils.XSSEncode(work_date_th5) + '</th>');
		html.push('<th>' + Utils.XSSEncode(work_date_th6) + '</th>');
		html.push('<th class="ColsTh7 ColsThEnd">' + Utils.XSSEncode(work_date_th7) + '</th>');
		html.push('</tr>');
		html.push('</thead>');
		html.push('<tbody>');		
		// insert day
		var firstDayDate = new Date(year, month - 1, 1);
        var firstDay=firstDayDate.getDay();
        var maxCell = 42;
        // get days of previous month
        var preYear = year;
        if (firstDay > 0) {
        	html.push('<tr>');
        	var preMonth = month - 1;        	
        	if(preMonth < 1){
        		preMonth = 12;
        		preYear = year - 1;
        	}
        	for (var i = 0; i < firstDay; i++) {
        		var pd = WorkingDatePlan.getDaysInMonth(preMonth-1, preYear) - firstDay + i + 1;
				html.push('<td class="ExtCols">' + Utils.XSSEncode(pd.toString()) + '</td>');
        	}
        }
        
        //get days in month
        var dOM = WorkingDatePlan.getDaysInMonth(month - 1, preYear) + firstDay;
        for (var j = firstDay; j < dOM; j++) {
        	var holClass = '';
        	if (j % 7 == 0) {
        		html.push('<tr>');
        		holClass = ' SunCols';
        	}
        	html.push('<td class="DayInfo' + Utils.XSSEncode(holClass) +'" id="tm_' +(j-firstDay+1)+'_' + month +'" onclick="WorkingDatePlan.showDate(event,' +(j-firstDay+1)+',' + month +',' + year +',this,' + j%7 +')">' + (j-firstDay+1) +'</td>');        	        	
        	if (j % 7 == 6) {
        		html.push('</tr>');
        	}
        }
        
        // get days of next month   
        if(dOM < (7*5)){
        	maxCell = 7*5;
        }
        if(dOM != (7*5)){
        	var nxtMonth = month + 1;
        	var nxtYear = year;
        	if(nxtMonth > 12){
        		nxtMonth = 1;
        		nxtYear = year + 1;
        	}
        	var k = 0;
        	for(var i=dOM;i<maxCell;i++){
        		k++;
        		html.push('<td class="ExtCols">' + k +'</td>');
        	}
        	html.push('</tr>');
        } 
		//////////////		
		html.push('</tbody>');    
		html.push('</table">');
		html.push('</div">');
		return html.join("");
	},
	loadCalendar: function(){
		var year = $('#currentYear').val().trim();
		var month = $('#currentMonth').val().trim();
		for(var i=1;i<=12;i++){
			$('#m' + i).html(WorkingDatePlan.getCalendarByMonth(i, year));
		}
		$('#monthContent').html(WorkingDatePlan.getCalendarByMonth(i, year,WorkingDatePlan._VIEW_BY_MONTH));
		WorkingDatePlan.getSaleDayInfo(0, year);	
		WorkingDatePlan.getSaleDayInfo(month, year);
		WorkingDatePlan.setCurrentDate();
	},
	currentYearChanged: function(){
		var year = $('#currentYear').val().trim();
		$('#yearText').text(year);
		WorkingDatePlan.loadCalendar();
//		WorkingDatePlan.currentMonthChanged();
	},
	showYearTab: function(){
		WorkingDatePlan.viewTabMonth = 0;
		$('#currentYear').val($('#currentYearMonthView').val().trim());
		$('#currentYear').change();
		$('#viewByMonth').removeClass('Active');
		$('#viewByYear').addClass('Active');
		$('#viewMonth').hide();
		$('#viewYear').show();
		if(WorkingDatePlan._checkType!=null && WorkingDatePlan._checkType && WorkingDatePlan._flagCheckExitsParent){
			$('#viewYear #btnThuaKe').show();
		}else{
			$('#viewYear #btnThuaKe').hide();
		}
	},
	showMonthTab: function(){
		WorkingDatePlan.viewTabMonth = 1;
		$('#currentYearMonthView').val($('#currentYear').val().trim());
		$('#currentYearMonthView').change();
		$('#viewByYear').removeClass('Active');
		$('#viewByMonth').addClass('Active');
		$('#viewYear').hide();
		$('#viewMonth').show();		
		var year = $('#currentYear').val().trim();
		var month = $('#currentMonth').val().trim();
		$('#monthContent').html(WorkingDatePlan.getCalendarByMonth(month, year,WorkingDatePlan._VIEW_BY_MONTH));
		//TODO
		WorkingDatePlan.getSaleDayInfo(month, year);
		WorkingDatePlan.setCurrentDate();
		
		if(WorkingDatePlan._checkType!=null && WorkingDatePlan._checkType && WorkingDatePlan._flagCheckExitsParent){
			$('#viewMonth #btnThuaKe').show();
		}else{
			$('#viewMonth #btnThuaKe').hide();
		}
	},
	currentMonthChanged: function(){
		var year = $('#currentYearMonthView').val().trim();
		var month = $('#currentMonth').val().trim();
		$('#monthContent').html(WorkingDatePlan.getCalendarByMonth(month, year,WorkingDatePlan._VIEW_BY_MONTH));		
		WorkingDatePlan.getSaleDayInfo(month, year);
		WorkingDatePlan.setCurrentDate();
	},
	showDate: function(event,day,month,year,obj,dow){
		/**Cho chon ngay chu nhat lun**/
//		if(dow!=0){
			$('#errMsg').html('').hide();
			$('#errExcelMsg').html('').hide();
			$('#dataBox').show();
			var left = event.pageX;
			var top = event.pageY;
			var w_box = 260;
			var h_box = 140;	
			if($('#btnUpdateSaleDay').is(':hidden')){
				h_box = 108;
			}
			top = top - h_box;
			left = left - (w_box/2);
			$('#dataBox .BoxEditState').css({'left':left,'top':top});
			var id = "tm_" + day + '_' + month;
			if (day < 10) {
				day = '0' + day;
			}
			if (month < 10) {
				month = '0' + month;
			}
			var currentTime = new Date();
			var curMonth = currentTime.getMonth() + 1;
			var curYear = currentTime.getFullYear();
			var dateString = WorkingDatePlan._dayInWeek[dow] + ' ' +  day + '/' + month + '/' + year;
			$.getJSON('/rest/catalog/check-system-time.json?time=' + day + '/' + month + '/' + year, function(data){
				if(data!= undefined && data.value == 1 && WorkingDatePlan._checkType!=null && WorkingDatePlan._checkType){
					if (year > curYear) {
						$('#btnUpdateSaleDay').show();
					} else if (year = curYear && month > curMonth) {
						$('#btnUpdateSaleDay').show();
					} else {
						$('#btnUpdateSaleDay').hide();
					}
				} else {
					$('#btnUpdateSaleDay').hide();
				}
			});
			$('#dateValue').val(dateString);
			if($('#' + id).hasClass('OffCols')){
				setSelectBoxValue('dateType',1);
				setTextboxValue('reasonGroup',$('#' + id).attr('data'));
			} else {
				setSelectBoxValue('dateType',0);
				setTextboxValue('reasonGroup','');
			}
//		}
	},
	saveDateInfo: function(){
		var msg = '';
		var dateValue = '';
		$('#errMsg').html('').hide();
		msg = Utils.getMessageOfRequireCheck('txtShopCode',common_unit_name_err);
		if(msg.length  > 0){	
			$('#txtShopCode').focus();
			$('#errMsg').html(msg).show();
			return false;
		}
		msg = Utils.getMessageOfRequireCheck('dateValue',work_date_day);
		if(msg.length  == 0){			
			var arrItems = $('#dateValue').val().trim().split(' ');
			if(arrItems.length == 1){
				dateValue = arrItems[0].trim();
			} else {
				dateValue = arrItems[1].trim();
			}
			if(!Utils.isDate(dateValue, '/')){
				msg = format(msgErr_invalid_format_date,work_date_day);
				$('#dateValue').focus();
			}
			if(msg.length == 0){
				msg = Utils.getMessageOfRequireCheck('dateType',jsp_common_status,true);
			}
			if(msg.length == 0 && $('#dateType').val().trim()== '1'){
				msg = Utils.getMessageOfRequireCheck('reasonGroup',work_date_reason,true);
			}
			if(msg.length > 0){
				$('#errMsg').html(msg).show();
				return false;
			}
		}
		if(msg.length > 0){
			$('#errMsg').html(msg).show();
			return false;
		}
		var dataModel = new Object();		
		dataModel.dateValue = dateValue;
		dataModel.dateType = $('#dateType').val().trim();
		dataModel.reasonCode = $('#reasonGroup').val().trim();		
		dataModel.shopCodeStr = $('#txtShopCode').val().trim();
		Utils.addOrSaveRowOnGrid(dataModel, "/working-date-plan/save", WorkingDatePlan._xhrSave, null, null, function(data){
			if(!data.error){
				var arrDate = $('#dateValue').val().trim().split('/');
				WorkingDatePlan.getSaleDayInfo(arrDate[1], arrDate[2]);
				$('#dataBox').hide();
			}
		});		
		return false;
	},
	getSaleDayInfo: function(month,year){
		if (WorkingDatePlan._hideErrExcelMsg <= 0) {
			$('#errExcelMsg').hide();
		} else {
			WorkingDatePlan._hideErrExcelMsg--;
		}
		if(/^0\d/.test(month)){
			month = month.replace('0','');
		}
		if(parseInt(month) == 0){
			$('.DayInfo').each(function(){
				$(this).removeClass('OffCols');
				$(this).removeAttr('data');
			});
		} else {
			$('#m' +parseInt(month)+' .DayInfo,#monthContent .DayInfo').each(function(){
				$(this).removeClass('OffCols');
				$(this).removeAttr('data');
			});
		}
		$('#numWorkDayByMonth').text('');
		var shopCode = $('#txtShopCode').val().trim();
		$.getJSON('/rest/catalog/working-date/except-day/' +month+ '/' + year +'/list.json?shopCode=' +shopCode, function(data){						
			for(var i=0;i<data.length-1;i++){	
				var id = data[i].content1;
				var value = data[i].content2;
				if($('#viewYear #' + id)!= null){
					$('#viewYear #' + id).addClass('OffCols');
					$('#viewYear #' + id).attr('data',value);
				}
				if($('#viewMonth #' + id)!= null){
					$('#viewMonth #' + id).addClass('OffCols');
					$('#viewMonth #' + id).attr('data',value);
				}
			}
			if(data[data.length-1] != null){
				var objExDay = data[data.length-1];
				var arrDay = [objExDay.content1,objExDay.content2,objExDay.content3,objExDay.content4,objExDay.content5,objExDay.content6,objExDay.content7,objExDay.content8,objExDay.content9,objExDay.content10,objExDay.content11,objExDay.content12];
				if(parseInt(month) == 0){
					for(var i=0;i<arrDay.length;i++){
						var numWorkDay = WorkingDatePlan.getDaysInMonth(i,year) - arrDay[i];
						if ($('#viewYear #numExDay_' + (i + 1)).html() != null && numWorkDay > 0) {
							$('#viewYear #numExDay_' + (i + 1)).html(format(work_date_co_so_ngay_lam_viec,numWorkDay));
						} else if ($('#viewYear #numExDay_' + (i + 1)).html() != null) { 
							$('#viewYear #numExDay_' + (i + 1)).html('');
						}					
					}
				}else{
					var numWorkDay = WorkingDatePlan.getDaysInMonth(month-1,year) - arrDay[month-1];
					if (numWorkDay > 0) {
						$('#viewYear #numExDay_' + (month)).html(format(work_date_co_so_ngay_lam_viec,numWorkDay));
						$('#numWorkDayByMonth').text(format(work_date_co_so_ngay_lam_viec,numWorkDay));
					} else {
						$('#viewYear #numExDay_' + (month)).html('');
					}
				}
				
			}
		});
	},
	setCurrentDate: function(){
		var currentTime = new Date();
		var month = currentTime.getMonth() + 1;
		var day = currentTime.getDate();
		var year = currentTime.getFullYear();
		if(parseInt($('#currentYear').val().trim()) == year){
			var id="tm_" + day + '' + month;
			$('#viewYear #' + id).addClass('NowCols');
			$('#viewMonth #' + id).addClass('NowCols');
		}
	},
	getSundaysInMonth: function(month,year){		
		var days = new Date(year,month,0 ).getDate();
		var fDate = new Date( month +'/01/' + year );
		var sundays = [ 8 - fDate.getDay() ];
		for ( var i = sundays[0] + 7; i <= days; i += 7 ) {
			sundays.push( i );
		}
		if(fDate.getDay() == 0){
			sundays.push(1);
		}
		return sundays;
	},

 	fillCodeForInputTextByDialogSearch2 : function (txt, id, code, name, isShow) {
 		$('.ErrorMsgStyle').html('').hide();
 		WorkingDatePlan._currentShopId = id;
		if (isShow == undefined || isShow == null) {
			$('#' + txt.trim()).val(Utils.XSSEncode(code + ' - ' + name));
			$('#' + txt.trim()).attr(txt + '_' + id, id);
		} else if (Utils._isShowInputTextByDlSearch2.code == isShow) {
			$('#' + txt.trim()).val(Utils.XSSEncode(code));
			$('#' + txt.trim()).attr(txt + '_' + id, id);
		} else if (Utils._isShowInputTextByDlSearch2.name == isShow) {
			$('#' + txt.trim()).val(Utils.XSSEncode(name));
			$('#' + txt.trim()).attr(txt + '_' + id, id);
		} else if (Utils._isShowInputTextByDlSearch2.id == isShow) {
			$('#' + txt.trim()).val(id);
			$('#' + txt.trim()).attr(txt + '_' + id, id);
		} else {
			$('#' + txt.trim()).val("");
			$('#' + txt.trim()).attr(txt + '_0', 0);
		}
		$('#common-dialog-search-tree-in-grid-choose-single').dialog("close");
		shopChanged();
		if (WorkingDatePlan.viewTabMonth == 1) {
			WorkingDatePlan.currentMonthChanged();
		} else {
			WorkingDatePlan.loadCalendar();
		}
		WorkingDatePlan.getShopParentWorkingDateConfigCheckParent(WorkingDatePlan._currentShopId);
	},
	checkTypeInWorkingDateConfig: function(shopId){
		var params = {shopId:shopId};
		Utils.getJSONDataByAjaxNotOverlay(params, "/working-date-plan/check-type-working-date-config", function(data) {
			if(data.checkType!=null && data.checkType && WorkingDatePlan._flagCheckExitsParent){
				WorkingDatePlan._checkType = data.checkType;
				$('#viewYear #btnThuaKe').show();
				$('#viewMonth #btnThuaKe').show();
				$('#msgNote').hide();
				$('#viewYear #btnThietLap').css('margin-left','1%');
			}else{
				WorkingDatePlan._checkType = data.checkType;
				$('#msgNote').hide();
				var nameCode = $('#parentCodeNameHid').val().trim();
				if(!Utils.isEmpty(nameCode)){
					$('#msgNote').html(format(work_date_mess_note_not_exist_parent,nameCode));
					$('#msgNote').show();
				}
				$('#viewYear #btnThuaKe').hide();
				$('#viewMonth #btnThuaKe').hide();
				$('#viewYear #btnThietLap').css('margin-left','13%');
			}
		});
	},
	getShopParentInWorkingDateConfig: function(shopId){
		var params = {shopId:shopId};
		Utils.getJSONDataByAjaxNotOverlay(params, "/working-date-plan/get-shop-parent-working-date-config", function(data) {
			if(data.parentCodeName!=null){
				$('#parentCodeNameHid').val(data.parentCodeName);
			}
		});
	},
	inheritWorkingDate : function(){
		var today = new Date();
		var month = today.getMonth()+1; // thang hien tai
		var nameCode = $('#parentCodeNameHid').val().trim();
		var title = Utils.XSSEncode(format(work_date_mess_confirm_inharit, nameCode, Number(month)+1));
		var params = new Object();
		params.shopId = WorkingDatePlan._currentShopId ;
		url = '/working-date-plan/inherit-working-date';
		$.messager.confirm(jsp_common_xacnhan, title, function(r){
			if(r){
				Utils.saveData(params, url, WorkingDatePlan._xhrSave, 'errMsg', function(data){
					if(WorkingDatePlan.viewTabMonth == 1){
						WorkingDatePlan.currentMonthChanged();
					}else{
						WorkingDatePlan.loadCalendar();
					}
					$('#successMsg').html(work_date_mess_success).show();
					WorkingDatePlan.checkTypeInWorkingDateConfig(WorkingDatePlan._currentShopId);
				});		
			}
		});
		return false;
	},
	/***BEGIN VUONGMQ; 29/01/2015 DIALOG QL Ngay lam viec**/
	_valueThang:[
	               {value:'-1',name: work_date_chon_thang},
	               {value:'1',name: msgThang1},
	               {value:'2',name: msgThang2},
	               {value:'3',name: msgThang3},
	               {value:'4',name: msgThang4},
	               {value:'5',name: msgThang5},
	               {value:'6',name: msgThang6},
	               {value:'7',name: msgThang7},
	               {value:'8',name: msgThang8},
	               {value:'9',name: msgThang9},
	               {value:'10',name: msgThang10},
	               {value:'11',name: msgThang11},
	               {value:'12',name: msgThang12},
	           ]
	,
	_valueNgay: [
	               {value:'-1',name: work_date_chon_ngay},
	               {value:'1',name: jsp_common_ngay_1},
	               {value:'2',name: jsp_common_ngay_2},
	               {value:'3',name: jsp_common_ngay_3},
	               {value:'4',name: jsp_common_ngay_4},
	               {value:'5',name: jsp_common_ngay_5},
	               {value:'6',name: jsp_common_ngay_6},
	               {value:'7',name: jsp_common_ngay_7},
	               {value:'8',name: jsp_common_ngay_8},
	               {value:'9',name: jsp_common_ngay_9},
	               {value:'10',name: jsp_common_ngay_10},
	               {value:'11',name: jsp_common_ngay_11},
	               {value:'12',name: jsp_common_ngay_12},
	               {value:'13',name: jsp_common_ngay_13},
	               {value:'14',name: jsp_common_ngay_14},
	               {value:'15',name: jsp_common_ngay_15},
	               {value:'16',name: jsp_common_ngay_16},
	               {value:'17',name: jsp_common_ngay_17},
	               {value:'18',name: jsp_common_ngay_18},
	               {value:'19',name: jsp_common_ngay_19},
	               {value:'20',name: jsp_common_ngay_20},
	               {value:'21',name: jsp_common_ngay_21},
	               {value:'22',name: jsp_common_ngay_22},
	               {value:'23',name: jsp_common_ngay_23},
	               {value:'24',name: jsp_common_ngay_24},
	               {value:'25',name: jsp_common_ngay_25},
	               {value:'26',name: jsp_common_ngay_26},
	               {value:'27',name: jsp_common_ngay_27},
	               {value:'28',name: jsp_common_ngay_28},
	               {value:'29',name: jsp_common_ngay_29},
	               {value:'30',name: jsp_common_ngay_30},
	               {value:'31',name: jsp_common_ngay_31},
	           ]
	 ,
	 _valueThu: [
	               {value:'2',name: jsp_common_thu_hai},
	               {value:'3',name: jsp_common_thu_ba},
	               {value:'4',name: jsp_common_thu_bon},
	               {value:'5',name: jsp_common_thu_nam},
	               {value:'6',name: jsp_common_thu_sau},
	               {value:'7',name: jsp_common_thu_bay},
	               {value:'1',name: jsp_common_thu_chunhat}
	              ]
	 ,
	 fillWeekdaysToMultiSelect: function() {
		var html = '<option value="0" >' + Utils.XSSEncode(jsp_common_status_all) +'</option>';
		if (WorkingDatePlan._valueThu.length > 0) {
			var lstThu = WorkingDatePlan._valueThu;
			for (var i = 0; i < lstThu.length; i++) {
				html += '<option  value="' + lstThu[i].value +'">' +
					Utils.XSSEncode(lstThu[i].name) + '</option>';									
			}
			$('#valueDefaultWorkDate').html(html);
			$('#valueDefaultWorkDate').change();
			$('#ddcl-valueDefaultWorkDate').remove();
			$('#ddcl-valueDefaultWorkDate-ddw').remove();
			$('#valueDefaultWorkDate').dropdownchecklist({
				emptyText: work_date_chon_thu,
				firstItemChecksAll: true,
				maxDropHeight: 350
			});
		} else {
			$('#valueDefaultWorkDate').html('');
			$('#valueDefaultWorkDate').dropdownchecklist({
				emptyText:'-------------------',
				firstItemChecksAll: true,
				maxDropHeight: 350
			});
		}
		$('.ui-dropdownchecklist-text').css({"padding-left":"5px"});
	},
	loadGridChooseWorking: function(){
		$('#gridChooseWorking').datagrid({
	           // url: '/attributes/dynamic/load-attribute-detail',
	            autoRowHeight : true,
	            rownumbers : true, 
	            checkOnSelect :true,
	           // pagination:true,
	            //rowNum : 10,
	            fitColumns:true,
	            //pageList  : [10,20,30],
	            scrollbarSize:0,
	            width: $('#gridContainer').width(),
	           // queryParams:params,
	            columns:[[          
	                {field:'month', title: work_date_day, width: 220, sortable:true,resizable:false , align: 'left', 
						 formatter:function(value, row, index){
						 	var html = new Array();
						 	html.push('<select style="width:50%; float:left" id="comboMonth_' +index+'" class="comboMonth" onclick="WorkingDatePlan.loadComboboxNgay(this, ' +index+');" >');
						 	var selectors = WorkingDatePlan._valueThang;
						 	for(var i = 0, size = selectors.length; i < size; ++ i){
						 	   var q = selectors[i];
						 	   if(q && q.value == -1){
							 		html.push(format('<option value="{0}" selected="selected">{1}</option>',q.value,q.name));
						 	   }else{
						 		   html.push(format('<option value="{0}">{1}</option>',q.value,q.name));
						 	   }
						 	}						 				 	
						 	html.push('</select>');
						 	html.push('  ');
						 	html.push('<select style="width:50%" id="comboDate_' +index+'" class="comboDate">');
						 	var selectors = WorkingDatePlan._valueNgay;
						 	var lengthSelect = 1; // se view chon ngay
						 	for(var i = 0, size = lengthSelect; i < size; ++ i){
						 	   var q = selectors[i];
						 	   if(q && q.value == -1){
							 		html.push(format('<option value="{0}" selected="selected">{1}</option>',q.value,q.name));
						 	   }else{
						 		   html.push(format('<option value="{0}">{1}</option>',q.value,q.name));
						 	   }
						 	}						 				 	
						 	html.push('</select>');
						 	return html.join("");
						 }
	              	},
	                {field:'reson', title: catalog_reason, width:300,sortable:true,resizable:false , align: 'left',// editor:'numberbox' , 
	                	formatter: function(value, row, index){
	              			return '<input id="txtReason_' +index+'" class="txtReason" name="nameFile" type="text" class="" placeholder="' +work_date_ly_do_nhap+'" style="width: 260px;" maxlength="100" />';
	              		}
	              	},
	                {field:'edit', title:'<a href= "javascript:void(0);" title="' +jsp_common_taomoi+'" id="btnAddGrid" onclick= "WorkingDatePlan.addWorkingDataGrid();"><img src="/resources/images/icon_add.png" /></a>',
	                    width: 34, align: 'center',sortable:false,resizable:false,
	                    formatter: function(val, row, index) {
	                        var value= '';
	                        value += '   <a href="javascript:void(0)" title="' +jsp_common_xoa+'" class="btnDeleteGrid" onclick="WorkingDatePlan.deleteWorkingDataGrid(' +index+');"><img src="/resources/images/icon_delete.png"/></a>';
	                        return value;
	                    }
	                },
	            ]],
	            onLoadSuccess :function(data){          
	            	WorkingDatePlan.addWorkingDataGrid();
	                $('.datagrid-header-rownumber').html(Utils.XSSEncode(jsp_common_numerical_order));
	                $(window).resize();             
	            }
	        });
	},

	loadComboboxNgay: function(id, index){
		var htmlValue = new Array();
	 	var selectors = WorkingDatePlan._valueNgay;
	 	var valueMonth = $(id).val();
	 	var lengthSelect = 1; // se view chon ngay
	 	if(valueMonth != null && valueMonth != -1){
	 		if(valueMonth == 2){
	 			// 29 ngay
	 			lengthSelect = selectors.length - 2;
	 		} else if (valueMonth == 4 || valueMonth == 6 || valueMonth == 9 || valueMonth == 11) {
	 			// 30 ngay
	 			lengthSelect = selectors.length - 1;
	 		} else {
	 			// 31 ngay; valueMonth == 1 || valueMonth == 3 || valueMonth == 5 || valueMonth == 7 || valueMonth == 8 || valueMonth == 10 ||valueMonth == 12
	 			lengthSelect = selectors.length;
	 		}
	 	}
	 	for(var i = 0, size = lengthSelect; i < size; ++ i){
	 	   var q = selectors[i];
	 	   if(q && q.value == -1){
		 		htmlValue.push(format('<option value="{0}" selected="selected">{1}</option>',q.value,q.name));
	 	   }else{
	 		   htmlValue.push(format('<option value="{0}">{1}</option>',q.value,q.name));
	 	   }
	 	}						 				 	
	 	$('#comboDate_' +index).html(htmlValue.join(""));
	},
	getShopParentWorkingDateConfigCheckParent: function(shopId){
		var htmlValue = '';
		var params = {shopId:shopId};
		Utils.getJSONDataByAjaxNotOverlay(params, "/working-date-plan/getShopParent-WorkingDateConfig-CheckParent", function(data) {
			if(data.parentCodeName!=null && data.parentCodeName != "" ){
				htmlValue = data.parentCodeName;
				$('#shopParent').val(data.parentCodeName);
				$('#parentCodeNameHid').val(data.parentCodeName);
			}else{
				$('#shopParent').val('');
				$('#parentCodeNameHid').val('');
			}
			if(data.flagCheckExitsParent!=null){
				WorkingDatePlan._flagCheckExitsParent = data.flagCheckExitsParent;
			}
			WorkingDatePlan.checkTypeInWorkingDateConfig(WorkingDatePlan._currentShopId);
		});
	},
	viewDialogWorking: function(){
		$('.SuccessMsgStyle').html('').hide();
		$('.ErrorMsgStyle').html('').hide();
		$('#popupWorkingDate').dialog({
			title: work_date_thiet_lap_ngay_lam_viec,
			closed : false,
			cache : false,
			modal : true,
			width : 650,
			height : 400,
			onOpen: function(){	
				$('input[type=radio]').removeAttr('checked'); //default no check radio
				$('#fromDate').val(getCurrentMonthAddOne());
				$('#toDate').val(getCurrentMonthAddOneNextYear());
				/** apply lai picker cho dia log*/
				$('#fromDate').removeData('monthpicker');
				applyMonthPickerDialog('fromDate', null, false);
				$('#toDate').removeData('monthpicker');
				applyMonthPickerDialog('toDate', 1, false);
				var shopParent = '';
				if ($('#shopParent').val() != null && $('#shopParent').val().trim().length > 0) {
					shopParent = '(<span style="color: #f00; font-weight:bold">' +Utils.XSSEncode($('#shopParent').val())+'</span>)';
				}
				$('label[for=copyParentDl]').html(work_date_copy_lich_tu_cha +' ' + shopParent);
				WorkingDatePlan.fillWeekdaysToMultiSelect(); // load combo thu
				WorkingDatePlan.loadGridChooseWorking(); // load grid
				$('#gridChooseWorking').datagrid('loadData', []);
			},
		 	onBeforeClose: function() {
	        },
	        onClose : function(){
	        }
		});
	},
	addWorkingDataGrid: function(){
		var len = $('#gridChooseWorking').datagrid('getRows').length;
		//var obj = new Object();		
		//obj.code = len + 1;		
		$('#gridChooseWorking').datagrid('appendRow',{});
	},
	deleteWorkingDataGrid: function(index){
		//var grid ='#gridChooseWorking';
		//var row = $(grid).datagrid ('getRows')[index];					
		$('#gridChooseWorking').datagrid('deleteRow',index);	
		//var rows = $(grid).datagrid ('getRows');					
		//$(grid).datagrid('loadData',rows);
		WorkingDatePlan.loadOnclickDeleteGrid();
	},
	loadOnclickDeleteGrid: function(){
		var i = 0;
		$('.btnDeleteGrid').each(function(){
		  $(this).attr('onclick', 'WorkingDatePlan.deleteWorkingDataGrid(' +i+');');
		  i++;
		});
	},
	saveWorkingDate: function(){
		$('.ErrorMsgStyle').html('').hide();
		$('.successMsg').html('').hide();
		var msg = Utils.getMessageOfRequireCheck('fromDate', work_date_chon_tu_thang_thiet_lap_view);
		if(msg.length > 0){
			$('#popupWorkingDate #errMsg').html(msg).show();
			return false;
		}
		msg = Utils.getMessageOfRequireCheck('toDate', work_date_chon_den_thang_thiet_lap_view);
		if(msg.length > 0){
			$('#popupWorkingDate #errMsg').html(msg).show();
			return false;
		}
		var fromDate =  $('#fromDate').val();
		var toDate =  $('#toDate').val();
		if(!Utils.compareMonth(getCurrentMonthAddOne(), fromDate)){ // b >= a: true 
			$('#popupWorkingDate #errMsg').html(work_date_thang_thiet_lap_sau_thang_hien_tai).show();
			return false;
		}
		if(!Utils.compareMonth(fromDate, toDate)){
			$('#popupWorkingDate #errMsg').html(work_date_thang_thiet_lap_nho_bang_den_thang).show();
			return false;
		}
		var currentYearView = getCurrentYear() + 10;
		var toDateView = toDate.split('/');
		if(parseInt(toDateView[1]) > currentYearView){
			$('#popupWorkingDate #errMsg').html(work_date_chon_tu_den_thang_thiet_lap_khong_qua_10_nam).show();
			return false;
		}
		var dataModel = new Object();
		dataModel.shopId = WorkingDatePlan._currentShopId;
		dataModel.shopCodeStr = $('#txtShopCode').val().trim();
		dataModel.fromMonth = fromDate;
		dataModel.toMonth = toDate;
		// flag check grid
		var flagGrid = false;
		if($('#copyParentDl').is(':checked')){
			//copy lich cha
			dataModel.type = 0;
			if($('#shopParent').val() == null || $('#shopParent').val() == ""){
				$('#popupWorkingDate #errMsg').html(work_date_khong_thay_don_vi_cha_lap_lich).show();
				return false;
			}
		} else if ($('#createDl').is(':checked')) {
			// thiet lap rieng
			dataModel.type = 1;			
			//var defaultWorkDate =  $('#valueDefaultWorkDate').val();
			var arrDefaultWork = $('#valueDefaultWorkDate').val();
			if (arrDefaultWork == null ) {
				arrDefaultWork = [];
			} else if (arrDefaultWork.length > 0 && arrDefaultWork[0] == 0) {
				arrDefaultWork.splice(0, 1);
			}
			dataModel.lstDefaultWork = arrDefaultWork;
			var lstWorkDate = new Array();
			var i = 0;
			$('#gridContainer .datagrid-view2 .datagrid-body tr').each(function(){
				var obj = new Object();
				obj.date =  $(this).find('.comboDate').val(); 
				obj.month = $(this).find('.comboMonth').val();
				obj.reason = $(this).find('.txtReason').val();
				if((obj.date != null && obj.date == -1) && (obj.month != null && obj.month != -1) ){
					$('#popupWorkingDate #errMsg').html(work_date_vui_long_chon_ngay+' ' +(i+1)+'!').show();
					flagGrid = true;
					return false;
				}
				if((obj.month != null && obj.month == -1) && (obj.date != null && obj.date != -1)){
					$('#popupWorkingDate #errMsg').html(Utils.XSSEncode(work_date_vui_long_chon_thang+' ' +(i+1)+'!')).show();
					flagGrid = true;
					return false;
				}
				if(obj.reason != null && obj.reason.length > 100) {
					var errReason = format(work_date_ly_do_maxlength,100);
					$('#popupWorkingDate #errMsg').html(Utils.XSSEncode(errReason+' ' +common_trang_dong+' ' +(i+1)+'!')).show();
					flagGrid = true;
					return false;
				}
				if((obj.month != null && obj.month != -1) && (obj.date != null && obj.date != -1)){
					lstWorkDate.push(obj);
				}
				i++;
				
			});
			dataModel.lstWorkDate = lstWorkDate;
		} else {
			$('#popupWorkingDate #errMsg').html(work_date_chon_thiet_lap).show();
			return false;
		}
		if(flagGrid){
			return false;
		}
		//day len Action
		var msgDialog = work_date_ban_co_muon_thiet_lap_ngay_lam_viec;
		JSONUtil.saveData2(dataModel, "/working-date-plan/create-work-date", msgDialog, "errMsg", function(data) {
			if(!data.error) {
				$('#popupWorkingDate #successMsg').html(msgCommon1).show();
				setTimeout(function(){
					$('.easyui-dialog').dialog('close');
					$('.SuccessMsgStyle').html("").hide();
				}, 1500);
				if(WorkingDatePlan.viewTabMonth == 1){
					WorkingDatePlan.currentMonthChanged();
				}else{
					WorkingDatePlan.loadCalendar();
				}
				WorkingDatePlan.getShopParentWorkingDateConfigCheckParent(WorkingDatePlan._currentShopId);
			} else{
				$('#popupWorkingDate #errMsg').html(data.errMsg).show();
			}
		}, "", function (data) { // callBackFail
			/*StockUpdateOrder.showDialogStockOrderError(data);
			$('#btnSearch').click();*/
			console.log("dsad VUONGMQ");
			if(data.errMsg != undefined) {
				$('#popupWorkingDate #errMsg').html(data.errMsg).show();
				//$('#errMsg').html(data.errMsg).show();
			}
		});
		return false;
	
	}
	/***END VUONGMQ; 29/01/2015 DIALOG QL Ngay lam viec**/
};
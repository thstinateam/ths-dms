var Videos={
	page:null,
	max:null,
	filterMedia:null,
	listProductCodeCurrentForVideo:null,
	listProductCodeAddForVideo:null,
	listChooseProduct:null,	
	setFilterMedia:function(){
		filterMedia = new Object();
		var mediaCode = $('#mediaCode').val().trim();
		var mediaName = $('#mediaName').val().trim();
		var status = $('#status').val();
		
		filterMedia.mediaCode = mediaCode;
		filterMedia.mediaName = mediaName;
		filterMedia.status = status;
		return filterMedia;
	},
	
	getFileName:function(){
		$('#errMsgUpload').html('').hide();
		$('#videoFileName').html(document.getElementById("videoFile").value);
	},
	
	openPopupUpload:function(){
		$('#videoUploadPopup').show();		
		$('.easyui-dialog #errMsgDlg').html('').hide();
		$('.easyui-dialog #errMsgVideo').html('').hide();
		$('.easyui-dialog #resultMsgVideo').html('').hide();
		$('#videoUploadPopup #mediaCode').prop('disabled', false);
		$('#videoUploadPopup #videoFile').prop('disabled', false);
		$('#videoUploadPopup #fakefilepc').prop('disabled', false);
				
		$('#videoUploadPopup #mediaCode').val('');
		$('#videoUploadPopup #mediaName').val('');
		$('#videoUploadPopup #videoFile').val('');
		$('#videoUploadPopup #fakefilepc').val('');
		$('#videoUploadPopup').dialog({
			title: 'UPLOAD VIDEO',		    
		    closed: false,		    
		    modal: true,
		    width: 450,
		    height: 270,
		    onOpen:function(){
		    	$('.ImportVideo1Form #mediaCode').focus();
		    }
		});		
		$('#videoUploadPopup #btnUploadVideo').removeAttr('onclick');
		$('#videoUploadPopup #btnUploadVideo').attr('onclick', 'return Videos.uploadVideoFile();');
		$('#videoUploadPopup #btnUploadVideo').html('Tải lên');
	},
	searchVideos:function(){
		var params = new Object();
		params = Videos.setFilterMedia();
		
		$('.ErrorMsgStyle').html('').hide();
		$('#grid').datagrid('reload',params);	
	},
	uploadVideoFile:function(){		
		$('.easyui-dialog #errMsgDlg').hide();
		if($('.easyui-dialog #videoFile').val() == '') {
			$('.easyui-dialog #errMsgDlg').html('Vui lòng chọn file Video.').show();
			return;
		}		
		
		var error = $('.easyui-dialog #errMsgVideo');
		
		if(error != null && error.html() != "") {			
			return;
		}	
		
		var mediaCode = $('#videoUploadPopup #mediaCode').val().trim();
		var mediaName = $('#videoUploadPopup #mediaName').val().trim();			
		if(mediaCode == '' || mediaCode == '-1'){
			$('.easyui-dialog #errMsgDlg').html('Bạn chưa nhập mã video.').show();
			$('#videoUploadPopup #mediaCode').focus();
			return false;
		}
		if(mediaName == '' || mediaName == '-1'){
			$('.easyui-dialog #errMsgDlg').html('Bạn chưa nhập tên video.').show();
			$('#videoUploadPopup #mediaName').focus();
			return false;
		}
		
		var msg = Utils.getMessageOfSpecialCharactersValidate('videoUploadPopup #mediaCode', 'Mã video', Utils._CODE);
		if(msg.length==0){
			msg = Utils.getMessageOfSpecialCharactersValidate('videoUploadPopup #mediaName', 'Tên video', Utils._NAME);
		}			
		if(msg.length==0){
			msg = Utils.getMessageOfMaxlengthValidate('videoUploadPopup #mediaCode', 'Mã video', 50);
		}
		
		if(msg.length==0){
			msg = Utils.getMessageOfMaxlengthValidate('videoUploadPopup #mediaName', 'Tên video', 100);
		}
		
		if(msg.length>0){
			$('.easyui-dialog #errMsgDlg').html(msg).show();
			return false;
		}
		
		//code chuan cua anh Hung
		var options = { 
				beforeSubmit: Videos.beforeUploadVideo,   
		 		success:      Videos.afterUploadVideo, 
		 		type: "POST",
		 		dataType: 'html',
		 		data:({})
		 }; 
			
		$.messager.confirm('Xác nhận','Bạn có muốn tải file lên?',function(r){
			if (r){
				$('.easyui-dialog #errMsgDlg').hide();	
				$('#videoUploadPopup #importFrm').ajaxForm(options);
		 		$('#videoUploadPopup #importFrm').submit();
		 		return false;
			}
		});
	},	
	showUpdateVideo:function(mediaCode, mediaName, status){
		$('#videoUploadPopup').show();		
		$('.easyui-dialog #errMsgDlg').html('').hide();
		$('.easyui-dialog #errMsgVideo').html('').hide();
		$('.easyui-dialog #resultMsgVideo').html('').hide();
		
		
		$('#videoUploadPopup #mediaName').val(Utils.XSSEncode(mediaName));
		setSelectBoxValue('status',status,'#videoUploadPopup ');		
		$('#videoUploadPopup #mediaCode').val(mediaCode);
		$('#videoUploadPopup #mediaCode').prop('disabled', true);
		$('#videoUploadPopup #videoFile').prop('disabled', true);
		$('#videoUploadPopup #fakefilepc').prop('disabled', true);
		
		
		$('#videoUploadPopup').dialog({
			title: 'CẬP NHẬT VIDEO',		    
		    closed: false,	    
		    modal: true,
		    width: 450,
		    height: 270,
		    onOpen:function(){
		    	$('.ImportVideo1Form #mediaName').focus();
		    }
		});
		$('#videoUploadPopup #btnUploadVideo').removeAttr('onclick');		
		$('#videoUploadPopup #btnUploadVideo').attr('onclick', 'Videos.updateVideo()');
		$('#videoUploadPopup #btnUploadVideo').html('Cập nhật');
		
	},	

	updateVideo:function(){
		$('.easyui-dialog #errMsgDlg').html('').hide();
		$('.easyui-dialog #errMsgVideo').html('').hide();
		$('.easyui-dialog #resultMsgVideo').html('').hide();

		var mediaCode = $('#videoUploadPopup #mediaCode').val().trim();
		var mediaName = $('#videoUploadPopup #mediaName').val().trim();
		var status = $('#videoUploadPopup #status').val();
		
		if(mediaName == '' || mediaName == '-1'){
			$('.easyui-dialog #errMsgDlg').html('Bạn chưa nhập tên video.').show();
			return false;
		}
		
		msg = Utils.getMessageOfSpecialCharactersValidate('videoUploadPopup #mediaName', 'Tên video', Utils._NAME);				
		if(msg.length>0){
			$('.easyui-dialog #errMsgDlg').html(msg).show();
			return false;
		}
		var obj = new Object();
		obj.mediaCode = mediaCode;
		obj.mediaName = mediaName;
		obj.status = status;
		
		Utils.addOrSaveData(obj, '/videos/update-video', null, 'errMsgVideo', function(){
			setTimeout(function(){
				$('#grid').datagrid('reload');
				$('.easyui-dialog  #resultMsgVideo').removeClass('ErrorMsgStyle').addClass('SuccessMsgStyle');   			
    			$('.easyui-dialog  #resultMsgVideo').html('Bạn đã cập nhật thành công!').show();
    			setTimeout(function(){
    				$('#videoUploadPopup').dialog('close');
    			}, 1500);
			}, 1000);			

		});
	},
	
	previewUploadVideoFile:function(fileObj,formId,inputText){				
		$('.easyui-dialog #errMsgDlg').html('').hide();
		$('.easyui-dialog #errMsgVideo').html('').hide();
		$('.easyui-dialog #resultMsgVideo').html('').hide();
		
		var importTokenVal = $('#importTokenVal').val();
		if(undefined != importTokenVal && null != importTokenVal){
			$('#importTokenVal').val($('#token').val());
		}		
		
		var inputId = 'fakefilepc';
		if(inputText != null || inputText != undefined){
			inputId = inputText;
		}		
		if (fileObj.value != '') {
			if (!/(\.mp4|\.avi)$/i.test(fileObj.value)) {
				$('.easyui-dialog #errMsgVideo').html('Tập tin chọn phải có đuôi mở rộng là .avi, .mp4.').show();
				fileObj.value = '';
				fileObj.focus();			
				document.getElementById(inputId).value ='';			
				return false;		
			} else {				
				if(fileObj.files[0].size > 10485760){//>10M=10485760
					$('.easyui-dialog #errMsgVideo').html('Kích thước file video vượt quá 10MB. Không thể tải lên.').show();
				}else{
					$('.easyui-dialog #errMsgVideo').html('').hide();			
					if ($.browser.msie){
					    var path = encodeURI(what.value);
					    path = path.replace("C:%5Cfakepath%5C","");
					    path = decodeURI(path);
					    document.getElementById(inputId).value = path;
					}else{
						document.getElementById(inputId).value = fileObj.value;
					}
				}		
			}
		}else{
			$('#.easyui-dialog #errMsgVideo').html('Vui lòng chọn file video.').show();		
			return false;
		}
		return true;
	},
	beforeUploadVideo: function(){
		if(!Videos.previewUploadVideoFile(document.getElementById("videoFile"))){
			return false;
		}			
		
		$('.easyui-dialog #errMsgDlg').html('').hide();
		$('.easyui-dialog #errMsgVideo').html('').hide();
		$('#resultMsgVideo').html('').hide();
		$('#successUpload').html('').hide();
		$('#divOverlay').show();
		return true;
	},
	afterUploadVideo: function(responseText, statusText, xhr, $form){		
		$('#divOverlay').hide();
		if(('#grid').length>0){
			$('#grid').datagrid('reload');
		}
		if (statusText == 'success') {			
	    	$("#responseDiv").html(responseText);
	    	updateTokenImport();
	    	if((null != $('#errorVideo') && undefined != $('#errorVideo') && $('#errorVideo').html().trim() == 'true') 
	    			|| $('#errorVideoMsg').html().trim().length > 0){
	    		$('.panel-body #errMsgVideo').html($('#errorVideoMsg').html()).show();
	    	} else {
	    		$('.easyui-dialog  #resultMsgVideo').removeClass('ErrorMsgStyle').addClass('SuccessMsgStyle');   			
    			$('.easyui-dialog  #resultMsgVideo').html('Bạn đã upload thành công!').show();  	
    			setTimeout(function(){
    				$('#videoUploadPopup').window('close');
    			}, 1500);
	    	}
	    }	
	},

	chbProductAllClick:function(){	
		Videos.listChooseProduct = new Map();
		$('.easyui-dialog #searchGrid').datagrid('reload');		
	},
	addProductForVideo:function(){
		//listProductCodeAddForVideo = new Array();
		$('#errMsgDialog').html('').hide();
		var mediaCode = $('#mediaCodeTemp').val().trim();
		var index = $('#indexMediaTemp').val();	
		var obj = new Object();
		//obj.listProductCodeAddForVideo = listProductCodeAddForVideo;
		obj.mediaCode = mediaCode;
		
		var isCheckAll = $('#chbProductAll').is(':checked');
		if(!isCheckAll){
			if(null != Videos.listChooseProduct && Videos.listChooseProduct.size() > 0){
				obj.listChooseProductId = Videos.listChooseProduct.keyArray;
			}else{
				$('#errMsgDialog').html('Bạn chưa chọn sản phẩm nào.').show();			
				return false;
			}
			obj.checkAll = 0;
		}else{
			obj.checkAll = 1;
			var code = $('.easyui-dialog #productCodeUIDialog').val().trim();
			var name = $('.easyui-dialog #productNameUIDialog').val().trim();
			var categoryName = $('.easyui-dialog #categoryCodeUIDialog').val().trim();
			obj.productCode = code;
			obj.productName = name;
			obj.productCategory = categoryName;
		}	
		Utils.addOrSaveData(obj, '/videos/add-product-for-video', null, 'errMsgDialog', function(){
			setTimeout(function(){
				$('#searchProductForVideoStyle').dialog('close');				
				if(undefined != index && null != index && index != '' && index != '-1'){
					var ddv = $('#grid').datagrid('getRowDetail', index).find('table.ddv');
					ddv.datagrid('reload');
					$('.easyui-dialog').dialog('close');
				}else{
					$('#grid').datagrid('reload');
				}				
			}, 1500);			

		});	
	},
	deleteProductForVideo:function(productCode, mediaCode, index){
		var obj = new Object();
		obj.productCode = productCode;
		obj.mediaCode = mediaCode;
		
		Utils.addOrSaveData(obj, '/videos/delete-product-for-video', null, null, function(){
			setTimeout(function(){
				var ddv = $('#grid').datagrid('getRowDetail', index).find('table.ddv');
				ddv.datagrid('reload');
			}, 500);			

		});
	},
	getUrlVideo:function(mediaCode, mediaName){
		var formatUrl = '';
		$.ajax({
			type : "POST",
			url : '/videos/get-media-item-for-video',
			dataType: "json",
			data: {
				'mediaCode':mediaCode
			},
			success:function(data, textStatus, jqXHR){
				Videos.showVideo(mediaCode, mediaName, data.url);
			}		
		});
		return formatUrl;
	},
	showVideo:function(mediaCode, mediaName, formatUrl){
		$('#showVideoDlg').show();		
		var title = Utils.XSSEncode(mediaCode + ' - ' + mediaName);
		$('#showVideoDlg').dialog({
			title: title,		    
		    closed: false,		    
		    modal: true,
		    width: 720,
		    top:100,
		    onOpen:function(){
		    	$('#player_api').css('width','100% !important');
		    	$('#boxPlayer').css('display', 'block');
				$('#player').show();	
				if(formatUrl.substring(formatUrl.length-3,formatUrl.length) == 'avi'){
					var url = imgServerPath + formatUrl;
					$('#player').html('<a class="media {width:780, height:551}" href="' +url+'"></a>');
					$('a.media').media();
				}else {
					flowplayer("player", "/resources/scripts/plugins/flowplayer-3.2.16/flowplayer/flowplayer-3.2.16.swf",{
						plugins: {
					        audio: {
					            url: '/resources/scripts/plugins/flowplayer-3.2.16/flowplayer/audio/flowplayer.audio-3.2.10.swf'
					        }
					    },
					    clip: {
					        url: videoServerPath + formatUrl,
					        autoPlay: false,
					        autoBuffering: true	        
					    },
					    play: {
				            replayLabel: 'Xem lại'
				        }
					});
				}
		    }		    
		});		
		
	},
	closeVideo:function(){
		$('#showVideoDlg').dialog('close');
		$('#showVideoDlg').hide();
		$('#player').hide();
		$('#btnCloseVideo').hide();
		$('#boxPlayer').css('display', 'none');
	},
	
	openProductUIDialog : function(mediaCode, index){
		$('#productDialogContainer').css("visibility", "visible");
		var html = $('#productDialogContainer').html();
		if(undefined != mediaCode && null != mediaCode){
			$('#mediaCodeTemp').val(mediaCode);
		}else{
			mediaCode = $('#mediaCodeTemp').val().trim();
		}
		
		if(undefined != index && null != index){
			$('#indexMediaTemp').val(index);
		}else{
			index = $('#indexMediaTemp');
		}
		$('#productEasyUIDialog #errMsgDialog').html('').hide();
		$('#productEasyUIDialog #successMsg').html('').hide();
		Videos.listChooseProduct = new Map();
		
		$('#productEasyUIDialog').dialog({  
	        title: 'Tìm kiếm sản phẩm',  
	        closed: false,  
	        cache: false,  
	        modal: true,
	        height:600,
	        onOpen: function(){
	        	var isCheckAll = $('#chbProductAll').is(':checked');
				if(isCheckAll){
					$('#chbProductAll').removeAttr('checked');
				}
				$('.easyui-dialog #productCodeUIDialog').focus();
				$('.easyui-dialog #productCodeUIDialog').val('');
				$('.easyui-dialog #productNameUIDialog').val('');
				$('.easyui-dialog #categoryCodeUIDialog').val('');
				Utils.bindAutoSearch();
				$('.easyui-dialog #searchGrid').show();
				$('.easyui-dialog #searchGrid').datagrid({
					url:'/videos/search-product-for-video',
					autoRowHeight : true,
					rownumbers : true, 
					checkOnSelect :true,
					pagination:true,				
				 	queryParams: {
				 		productCode: '',
						productName: '',
						productCategory: '',
						mediaCode:mediaCode						
					},
					rowNum : 10,
					pageList  : [10,20,30],
					width : ($('.easyui-dialog #grid').width()),
				    columns:[[  
				        {field:'productCode',title:'Mã SP',align:'left', width:110, sortable : false,resizable : false, formatter:function(value,rowData,rowIndex) {
				        	return Utils.XSSEncode(value);
				        }},  
				        {field:'productName',title:'Tên SP',align:'left', width:250, sortable : false,resizable : false, formatter:function(value,rowData,rowIndex) {
				        	return Utils.XSSEncode(value);
				        }},
				        {field:'productCat',title:'Ngành hàng',align:'left', width:100, sortable : false,resizable : false, formatter:function(value,rowData,rowIndex) {
				        	return Utils.XSSEncode(value);
				        }},				        
				        {field:'id', checkbox:true}
				    ]],
				    onLoadSuccess :function(data){
				    	$('.datagrid-header-rownumber').html('STT');
			    		updateRownumWidthForJqGrid('.easyui-dialog');
			    		$(window).resize();
			    		 
			 			$('td[field=id] .datagrid-header-check input').removeAttr('checked');
			 			
			 			var isCheckAll = true;
			 			for(var i = 0; i < data.rows.length; i++) {
			 				var row = data.rows[i];
			 				var id = row.id;
			 				if(Videos.listChooseProduct.get(id) != null && Videos.listChooseProduct.get(id) != undefined) {
			 					$('.easyui-dialog #searchGrid').datagrid('checkRow', i);
			 				} else {
			 					isCheckAll = false;
			 				}
			 			}
			 			if(isCheckAll) {
			 				$('.easyui-dialog #searchGrid').datagrid('checkAll');
			 			}
				    },
				    
					
					 onCheck:function(rowIndex, rowData){
					    	var id = rowData['id'];
					    	Videos.listChooseProduct.put(id, rowData);				    	
							var isCheckAll = $('#chbProductAll').is(':checked');
							if(isCheckAll){
								$('#chbProductAll').removeAttr('checked');
							}							
					    },					    
					    onUncheck:function(rowIndex, rowData){
					    	var id = rowData['id'];
					    	Videos.listChooseProduct.remove(id);				    	
					    },
					    onCheckAll:function(rows){
					    	for(var i = 0; i<rows.length; i++){
					    		var id= rows[i]['id'];
					    		Videos.listChooseProduct.put(id, rows[i]);					    	
					    	}
					    	var isCheckAll = $('#chbProductAll').is(':checked');
							if(isCheckAll){
								$('#chbProductAll').removeAttr('checked');
							}
					    },
					    onUncheckAll:function(rows){
					    	for(var i = 0; i<rows.length; i++){
					    		var id= rows[i]['id'];
					    		Videos.listChooseProduct.remove(id);				    	
					    	}
					    },
				});  
				$('.easyui-dialog #btnSearchUIDialog').bind('click',function(event) {
					var code = $('.easyui-dialog #productCodeUIDialog').val().trim();
					var name = $('.easyui-dialog #productNameUIDialog').val().trim();
					var categoryName = $('.easyui-dialog #categoryCodeUIDialog').val().trim();
					var mediaCodeTemp = $('#mediaCodeTemp').val().trim();
					if(undefined == mediaCodeTemp || null== mediaCodeTemp || mediaCodeTemp == '' || mediaCodeTemp == '-1'){
						mediaCodeTemp = mediaCode;
					}
					$('.easyui-dialog #searchGrid').datagrid('load',{productCode : code, productName: name, productCategory: categoryName, mediaCode:mediaCodeTemp});
					$('.easyui-dialog #productCodeUIDialog').focus();
				});
				$('.easyui-dialog #btnClose').bind('click',function(event) {
					$('.easyui-dialog').dialog('close');
					$('#productDialogContainer').css("visibility", "hidden");
				});
	        },
	        onBeforeClose: function() {
	        	
	        },
	        onClose : function(){	        	
	        	 $('#productDialogContainer').html(html);
				 $('#productDialogContainer').css('visibility', 'hidden');
				 $('#grid').html('<table id="searchGrid" class="easyui-datagrid"></table><div id="gridPager"></div>');
	        },
	    });
		return false;
	}
};
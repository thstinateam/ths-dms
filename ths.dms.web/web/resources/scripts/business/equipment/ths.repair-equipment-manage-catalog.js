var RepairEquipmentManageCatalog = {
	_VATTU: 'VATTU',  /** kiểm tra đơn giá vật tư; Chuc nang: QL sua chua*/
	_NHANCONG: 'NHANCONG', /** kiểm tra đơn giá nhân công; Chuc nang: QL sua chua*/
	_flagPagePheDuyet: false, /** co nay de biet trang phe duyet*/
	_paramsSearch: null, /** param nay cho search va export excel theo grid*/
	_valueTruMot: -1,
	_rolePermission: null,
	_roleKTNPP: 'KTNPP',
	_roleTNKD: 'TNKD',
	_maxWorkerPriceDetail: 0,
	_msgGiaNhanCongMax: '',
	_msgGiaNhanCongMaxFocus: '',
	_PHEDUYET: 1, /** Phe duyet; khi cap nhat phieu sua chua */
	lstId : null,
	_xhrDel :null,
	_pMap:null,
	isImportSuccess : false,
	_mapRepair:new Map(),
	paramForDelete:new Object(),
	_xhrSave : null,
	_editIndex: null,
	_mapEquip: null,
	_lstEquipAvailable:[],
	_mapDetail: null,
	_rowDgTmp: null,
	_mapEquipSaleplanCurrent: null,
	_mapEquipSaleplanNew: null,
	_params:null,
	_editIndex:null,
	_lstEquipInRecord:null,
	_numberEquipInRecord:null,
	_lstEquipInsert:null,
	_editIndex:null,
	_colum1Width:null,
	_colum1_1Width: null, //
	_colum2Width:null,
	_colum2_1Width: null, //
	_colum2_2Width: null, //
	_colum3Width:null,
	_colum3_1Width: null, //
	_colum4Width:null,
	_colum5Width:null,
	_countFile: 0,
	_arrFileDelete: null,
	_lstEquipGroup: null,
	_lstEquipItemId:new Array(),
	//tamvnm
	/*** vuongmq; 29/05/2015; lay danh sach nhom thiet bi khi chon loai thiet bi; ham getListEquipGroupVOByCategry tao trong common*/
	onchangeDdlEquipCategory : function(cbx) {
		var id = Number($(cbx).val());
		var html = '';
		if (id == undefined || id == null){
			equipCategoryId = 0;
		}
		Utils.getJSONDataByAjax({
			equipCategoryId: id
		//}, '/equipment-list-manage/getListEquipGroupVOByCategry', function(data) {
		}, '/commons/getListEquipGroupVOByCategry', function(data) {
			if (data.rows != undefined && data.rows != null && data.rows.length > 0){
				$('#ddlEquipGroupId').html('<option value="-1" selected="selected">Tất cả</option>');
				var rows = data.rows;
				for (var i = 0, size = rows.length; i < size; i++) {
					$('#ddlEquipGroupId').append('<option value="' + rows[i].id + '">' + Utils.XSSEncode(rows[i].code) + ' - ' + Utils.XSSEncode(rows[i].name) + '</option>');
				}
			} else {
				$('#ddlEquipGroupId').html('<option value="-2" selected="selected">Không có dữ liệu</option>').change();
			}
			$('#ddlEquipGroupId').change();
		}, null, null);
	},

	/*** tamvnm; 22/07/2015; lay danh sach nhom thiet bi khi chon loai thiet bi; ham getListEquipGroupVOByCategry tao trong common*/
	onchangeDdlEquipCategoryAuto : function(cbx) {
		var id = Number($(cbx).val());
		var html = '';
		if (id == undefined || id == null){
			equipCategoryId = 0;
		}
		Utils.getJSONDataByAjax({
			equipCategoryId: id
		}, '/commons/getListEquipGroupVOByCategry', function(data) {
			if (data.rows != undefined && data.rows != null && data.rows.length > 0){
				var equipGroup = data.rows;
				var obj = {
					id: -1,
					code: "Tất cả",
					name: "Tất cả",
					codeName: "Tất cả",
					searchText: unicodeToEnglish("Tất cả").toUpperCase(),
				};
				equipGroup.splice(0, 0, obj);
				for(var i = 0, sz = equipGroup.length; i < sz; i++) {
			 		equipGroup[i].searchText = unicodeToEnglish(equipGroup[i].code + " " + equipGroup[i].name);
			 		equipGroup[i].searchText = equipGroup[i].searchText.toUpperCase();
			 		equipGroup[i].code = Utils.XSSEncode(equipGroup[i].code);
			 		equipGroup[i].name = Utils.XSSEncode(equipGroup[i].name);
				}
			 	$('#ddlEquipGroupId').combobox("loadData", equipGroup);
			 	$('#ddlEquipGroupId').combobox("setValue", -1);
			} else {
				
			}

		}, null, null);
	},
	/** phuongvm, cai nay search trong MH: Ql sua chua  */
	searchRepair: function() {
		RepairEquipmentManageCatalog._mapRepair = new Map();
		$('#equipHistoryTemplateDiv').css("visibility","hidden").hide();
		var err = "";
		$('#errMsg').html('').hide();
		var msg="";
		var fDate = $('#fDate').val();
		var tDate =$('#tDate').val();
		if (msg.length == 0 && $('#fDate').val().trim().length > 0 && !Utils.isDate($('#fDate').val(),'/') && $('#fDate').val() != '__/__/____') {
			msg = 'Ngày tạo từ không tồn tại hoặc không đúng định dạng dd/mm/yyyy.';
			err = '#fDate';
		}
		if (msg.length == 0 && $('#tDate').val().trim().length > 0 && !Utils.isDate($('#tDate').val(),'/') && $('#tDate').val() != '__/__/____') {
			msg = 'Đến ngày không tồn tại hoặc không đúng định dạng dd/mm/yyyy.';
			err = '#tDate';
		}
		if (msg.length==0 && fDate.length > 0 && tDate.length>0 && !Utils.compareDate(fDate, tDate)) {
			msg = 'Ngày tạo từ phải là ngày trước hoặc cùng ngày với đến ngày';
		}
		if(msg.length>0){
			$('#errMsg').html(msg).show();
			$(err).focus();
			setTimeout(function(){$('#errMsg').hide();},3000);
			return false;
		}
		var equipCategoryId = $('#ddlEquipCategory').val();
		//tamvnm: thay doi thanh autoCompleteCombobox: -2 la truong hop tim group ko co trong BD
		// var equipGroupId = $('#ddlEquipGroupId').combobox("getValue");
		var equipGroupId = -1;
		if ($('#ddlEquipGroupId').combobox("getValue") != "") {
			if ($('#ddlEquipGroupId').combobox("getValue") != -1) {
				if (Utils.getMessageOfInvaildNumberNew($('#ddlEquipGroupId').combobox("getValue")).length == 0) {
					equipGroupId = $('#ddlEquipGroupId').combobox("getValue");
				} else {
					equipGroupId = -2;
				}
			}
		} else {
			$('#ddlEquipGroupId').combobox("setValue", -1);
		}
		var statusPayment = $('#cbxStatusPayment').val();
		var status = $('#cbxStatus').val();
		var equipCode = $('#txtCode').val();
		var formCode = $('#txtRepairCode').val();
		var fromDate = $('#fDate').val();
		var toDate = $('#tDate').val();
		var seri = $('#txtSerial').val();
		/** put params vao; xu dung cho export */
		RepairEquipmentManageCatalog._paramsSearch = new Object();
		RepairEquipmentManageCatalog._paramsSearch.equipCategoryId = equipCategoryId;
		RepairEquipmentManageCatalog._paramsSearch.equipGroupId = equipGroupId;
		RepairEquipmentManageCatalog._paramsSearch.statusPayment = statusPayment;
		RepairEquipmentManageCatalog._paramsSearch.status = status;
		RepairEquipmentManageCatalog._paramsSearch.equipCode = equipCode;
		RepairEquipmentManageCatalog._paramsSearch.formCode = formCode;
		RepairEquipmentManageCatalog._paramsSearch.fromDate = fromDate;
		RepairEquipmentManageCatalog._paramsSearch.toDate = toDate;
		RepairEquipmentManageCatalog._paramsSearch.seri = seri;
		$('#grid').datagrid('load',{
			page:1,
			equipCategoryId:equipCategoryId,
			equipGroupId:equipGroupId,
			statusPayment:statusPayment,
			status:status,
			equipCode:equipCode,
			formCode:formCode,
			fromDate:fromDate,
			toDate:toDate,
			seri:seri
			});
		
	},
	viewDetail:function(id, code){
		$('.SuccessMsgStyle').html('').hide();
		$('.ErrorMsgStyle').html('').hide();
		if (code != undefined && code != null && code.trim().length > 0) {
			$('#equipmentCodeFromSpan').html(code);
		} else {
			$('#equipmentCodeFromSpan').html("");
		}
		$('#equipHistoryTemplateDiv').css("visibility","visible").show();
		$('#equipHistoryDg').datagrid({
			url : "/equipment-repair-manage/searchRepairDetail",
			pagination : true,
	        rownumbers : true,
	        pageNumber : 1,
	        scrollbarSize: 0,
	        autoWidth: true,
	        pageList: [10],
	        autoRowHeight : true,
	        fitColumns : true,
			queryParams: {equipRepairId: id},
			width: $('#equipHistoryDgContainerGrid').width() - 15,
		    columns:[[	        
			    {field:'equipItemName', title: 'Hạng mục', width: 260, sortable: false, resizable: false , align: 'left', formatter: function(value,row,index){
			    	var hangmuc = "";
			    	if(row.equipItem!=null){
			    		hangmuc = row.equipItem.name;
			    	}
			    	return Utils.XSSEncode(hangmuc);
			    }},
			    {field:'repairCount', title: 'Lần sửa', width: 60, sortable: false, resizable: false , align: 'right', formatter: function(value,row,index){
			    	return value;
			    }},
			    {field:'createDateString', title: 'Ngày sửa', width: 80, sortable: false, resizable: false , align: 'center', formatter: function(value,row,index){
			    	return Utils.XSSEncode(value);
			    }},
			    {field:'warranty', title: 'Bảo hành (tháng)', width: 80, align: 'right', sortable: false, resizable: false, formatter: function(value,row,index){
			    	if (value != undefined && value != null) {
			    		return formatCurrency(value);
			    	}
			    	return '';
			    }},
			    {field:'materialPrice', title: 'Đơn giá vật tư', width: 90, align: 'right', sortable: false, resizable: false, formatter: function(value,row,index){
			    	if (value != undefined && value != null) {
			    		return formatCurrency(value);
			    	}
			    	return '';
			    }},
			    {field:'workerPrice', title: 'Đơn giá nhân công', width: 90, align: 'right',sortable: false, resizable: false, formatter: function(value,row,index){
			    	if (value != undefined && value != null) {
			    		return formatCurrency(value);
			    	}
			    	return '';
			    }},
			    {field:'quantity', title: 'Số lượng', width:50, sortable: false, resizable: false , align: 'right', formatter: function(value,row,index){
			    	if (value != undefined && value != null) {
			    		return formatCurrency(value);
			    	}
			    	return '';
			    }},	
			    {field:'totalAmount',title: 'Tổng tiền', width: 100, sortable: false, resizable: false, align: 'right', formatter:function(value,row,index){
			    	if (value != undefined && value != null) {
			    		return formatCurrency(value);
			    	}
			    	return '';
			    }}
		    ]],
		    onLoadSuccess :function(data){	    	
		    	$('.datagrid-header-rownumber').html('STT');
		    	//Utils.functionAccessFillControl('equipHistoryDg');
	   		 	$(window).resize();    		 
		    }
		});
	},
	/** phuongvm, QLsua chua-> cap nhat trang thai cua phieu sua chua;*/
	updateStatus: function() {
		$('#errMsg').html('').hide();	
		var statusForm = $('#cbxStatusUpdate').val().trim();
		if (statusForm < 0) {
			$('#errMsg').html('Vui lòng chọn trạng thái').show();
			return;
		}
		var arra = RepairEquipmentManageCatalog._mapRepair.valArray;		
		var lstId = new Array();
		if (arra == null || arra.length == 0) {
			$('#errMsg').html('Bạn chưa chọn phiếu sửa chữa để cập nhật. Vui lòng chọn phiếu').show();
			return;
		}		
		for (var i=0, size=arra.length; i<size; ++i) {
			lstId.push(arra[i].id);			
		}
		var params = new Object();
		params.lstId = lstId;
		params.statusForm = statusForm;
		if ($("#rejectReason").val()!=null && $("#rejectReason").val()!='') {
			params.rejectReason = $("#rejectReason").val();
		}
		var title = {title:'Thông tin lỗi ',pageList:10};
		Utils.addOrSaveData(params, '/equipment-repair-manage/update', null, 'errMsg', function(data) {
			if ( data.lstErr != undefined && data.lstErr != null && data.lstErr.length > 0 ) {
				VCommonJS.showDialogList({
				    data : data.lstErr,
				    dialogInfo: title,
				    columns : [[
				        {field:'formCode', title:'Mã biên bản', align:'left', width: 110, sortable:false, resizable:false, formatter: function(value, row, index) {
					    	return Utils.XSSEncode(value);
					    }},
				        {field:'formStatusErr', title:'Trạng thái biên bản', align:'center', width: 110, sortable:false, resizable:false, formatter: function(value,row,index){
				        	var html="";
							if(value == 1){
								html = '<img src="/resources/images/icon_delete.png" height="20" width="20" style="margin-right:5px;" title="Error">';
							}
							return html;
						} }
					]]
				});	
			} else {
				$('#successMsg').html('Lưu dữ liệu thành công').show();
				setTimeout(function(){$('#successMsg').html('').hide();},1500);
			}
			RepairEquipmentManageCatalog._mapRepair = new Map();
			$('#grid').datagrid('reload');
		}, null, true,null, 'Bạn có muốn cập nhật phiếu sửa chữa?', null, true);
		
	},
	/** phuongvm, cai nay search trong MH: phe duyet sua chua*/
	searchRepairEx:function(){
		RepairEquipmentManageCatalog._mapRepair = new Map();
		$('#equipHistoryTemplateDiv').css("visibility","hidden").hide();
		var err = "";
		$('#errMsg').html('').hide();
		var msg="";
		var fDate = $('#fDate').val();
		var tDate =$('#tDate').val();
		if(msg.length == 0 && $('#fDate').val().trim().length>0 && !Utils.isDate($('#fDate').val(),'/') && $('#fDate').val() != '__/__/____') {
			msg = 'Ngày tạo từ không tồn tại hoặc không đúng định dạng dd/mm/yyyy.';
			err = '#fDate';
		}
		if(msg.length == 0 && $('#tDate').val().trim().length>0 && !Utils.isDate($('#tDate').val(),'/') && $('#tDate').val() != '__/__/____') {
			msg = 'Đến ngày không tồn tại hoặc không đúng định dạng dd/mm/yyyy.';
			err = '#tDate';
		}
		if(msg.length==0 && fDate.length>0 && tDate.length>0 && !Utils.compareDate(fDate, tDate)){
			msg = 'Ngày tạo từ phải là ngày trước hoặc cùng ngày với đến ngày';
		}
		if(msg.length>0){
			$('#errMsg').html(msg).show();
			$(err).focus();
			setTimeout(function(){$('#errMsg').hide();},3000);
			return false;
		}
		var equipCode = $('#txtCode').val();
		var formCode = $('#txtRepairCode').val();
		var fromDate = $('#fDate').val();
		var toDate = $('#tDate').val();
		var seri = $('#txtSerial').val();
		var customerCode = $('#customerCode').val();
		var customerName = $('#customerName').val();
		var shopKendo = $("#shop").data("kendoMultiSelect");
		var dataShop = shopKendo.dataItems();
		var lstShopId = '';
		if (dataShop != null && dataShop.length > 0) {
			lstShopId = dataShop[0].shopId;
			for (var i = 1, sz = dataShop.length; i < sz; i++) {
				lstShopId += "," + dataShop[i].shopId;
			}
		}
		$('#grid').datagrid('load',{
			page:1,
			customerCode:customerCode,
			customerName:customerName,
			equipCode:equipCode,
			formCode:formCode,
			fromDate:fromDate,
			toDate:toDate,
			strListShopId:lstShopId,
			seri:seri
			});
		
	},
	/**
	 * Them mot dong hang muc
	 * @author phuongvm
	 * @since 05/01/2015
	 */
	insertEquipmentItemInGrid: function(){	
		$('#errMsg').html('').hide();
		var sz = $("#gridDetail").datagrid("getRows").length;
//		if(RepairEquipmentManageCatalog._editIndex != null && RepairEquipmentManageCatalog._editIndex == sz-1){
//			var row = $('#gridDetail').datagrid('getRows')[RepairEquipmentManageCatalog._editIndex];
//			
//			if($('#equipmentCodeInGrid').val() == ''){
//				$('#errMsgInfo').html('Bạn chưa xử lý xong dòng thiết bị thêm mới!').show();
//				return false;
//			}else if($('#equipmentCodeInGrid').val() != undefined){
//				row.equipmentCode = $('#equipmentCodeInGrid').val();
//			}
//			if($('#seriNumberInGrid').val() == ''){
//				$('#errMsgInfo').html('Bạn chưa xử lý xong dòng thiết bị thêm mới!').show();
//				return false;
//			}else if($('#seriNumberInGrid').val() != undefined){
//				row.seriNumber = $('#seriNumberInGrid').val();
//			}
//			if($('#contentDelivery').val() != undefined){
//				row.contentDelivery = $('#contentDelivery').val();
//			}
//			
//			$('#equipmentCodeInGrid').remove();
//			$('#seriNumberInGrid').remove();
//			$('#contentDelivery').remove();
//			$('#gridDetail').datagrid('updateRow',{
//				index: RepairEquipmentManageCatalog._editIndex,	
//				row: row
//			});
//			if(RepairEquipmentManageCatalog._lstEquipInsert == null){
//				RepairEquipmentManageCatalog._lstEquipInsert = new Array();
//			}
//			RepairEquipmentManageCatalog._lstEquipInsert.push(row.equipmentCode);
//		}		
		RepairEquipmentManageCatalog._editIndex = sz;	
		$('#gridDetail').datagrid("appendRow", {});	
		// F9 tren grid
		RepairEquipmentManageCatalog.editInputInGrid();
		$("#gridDetail").datagrid("selectRow", RepairEquipmentManageCatalog._editIndex);	
		$('#equipItemCodeInGrid_').focus();
		
		/*RepairEquipmentManageCatalog._colum1Width =  $('#equipItemCodeInGrid_').parent().width();
		RepairEquipmentManageCatalog._colum1_1Width = $('#warantyDateInGrid_').parent().width(); //
		RepairEquipmentManageCatalog._colum2Width =  $('#warantyInGrid_').parent().width();
		RepairEquipmentManageCatalog._colum2_1Width =  $('#warantyExpiredDateInGrid_').parent().width(); //
		RepairEquipmentManageCatalog._colum2_2Width =  $('#materialPriceDefaultInGrid_').parent().width(); //
		RepairEquipmentManageCatalog._colum3Width =  $('#materialPriceInGrid_').parent().width();
		RepairEquipmentManageCatalog._colum3_1Width = $('#wokerPriceDefaultInGrid_').parent().width(); //
		RepairEquipmentManageCatalog._colum4Width =  $('#wokerPriceInGrid_').parent().width();
		RepairEquipmentManageCatalog._colum5Width =  $('#quantityInGrid_').parent().width();
		$('#equipItemCodeInGrid_').width($('#equipItemCodeInGrid_').parent().width());
		$('#warantyDateInGrid_').width($('#warantyDateInGrid_').parent().width());	//
		$('#warantyInGrid_').width($('#warantyInGrid_').parent().width());	
		$('#warantyExpiredDateInGrid_').width($('#warantyExpiredDateInGrid_').parent().width());// 
		$('#materialPriceDefaultInGrid_').width($('#materialPriceDefaultInGrid_').parent().width());//
		$('#materialPriceInGrid_').width($('#materialPriceInGrid_').parent().width());	
		$('#wokerPriceDefaultInGrid_').width($('#wokerPriceDefaultInGrid_').parent().width()); //
		$('#wokerPriceInGrid_').width($('#wokerPriceInGrid_').parent().width());
		$('#quantityInGrid_').width($('#quantityInGrid_').parent().width());*/
		RepairEquipmentManageCatalog.loadDateTimerPickerNgayBaoHanh();
	},
	/**
	 * su kien tren grid
	 * @author phuongvm
	 * @since 05/01/2015
	 */
	
	editInputInGrid: function(){
		$('#equipItemCodeInGrid_').bind('keyup', function(e){
			if(e.keyCode==keyCode_F9){
				RepairEquipmentManageCatalog._lstEquipItemId = new Array();
				var rows = $('#gridDetail').datagrid('getRows');
				if(rows!=null && rows.length > 0){
					for(var i =0;i<rows.length;i++){
						RepairEquipmentManageCatalog._lstEquipItemId.push(rows[i].id);
					}
				}
				RepairEquipmentManageCatalog.showPopupSearchEquipItem();
			}
		});
	},
	/**
	 * Show popup tim kiem hang muc
	 * @author phuongvm
	 * @since 05/01/2015
	 */
	showPopupSearchEquipItem: function() {
		$('.SuccessMsgStyle').html('');
		$('.ErrorMsgStyle').html('');
		var msg = '';
		if (msg.length == 0) {
			msg = Utils.getMessageOfRequireCheck('txtEquipCode', 'Mã thiết bị');
			err = '#txtEquipCode';
			if (msg.trim().length > 0) {
				$('#errMsg').html('Vui lòng nhập Mã thiết bị hợp lệ (nhấn F9)').show();
				$(err).focus();
				return;
			}
		}
		var equipCode = $('#txtEquipCode').val().trim();
		VCommonJS.showDialogSearch2WithCheckbox({
			params : {
				equipCode: equipCode,
				strEquip: RepairEquipmentManageCatalog._lstEquipItemId.join(','),
				status: 1
			},
			inputs : [
		        {id:'name', maxlength:250, label:'Tên hạng mục',width:450},
		    ],
		    chooseCallback : function(listObj) {
		        if (listObj != undefined && listObj != null && listObj.length > 0) {
		        	//var rows = new Array();
		        	for (var i=0;i<listObj.length ; i++) {
		        		var donGiaVTDM = RepairEquipmentManageCatalog.getVatTuNhanCongHangMuc(listObj[i], RepairEquipmentManageCatalog._VATTU);
		        		var donGiaNCDM = RepairEquipmentManageCatalog.getVatTuNhanCongHangMuc(listObj[i], RepairEquipmentManageCatalog._NHANCONG);
		        		var row = {
		        			id:	listObj[i].id,
		        			hangMuc:listObj[i].name,	
		        			ngayBatDauBaoHanh:'',//
		        			baoHanh:'',	
		        			ngayHetHanBaoHanh:'',//
		        			donGiaVatTuDinhMuc: donGiaVTDM , //
		        			donGiaVatTu:'',	
		        			donGiaNhanCongDinhMuc: donGiaNCDM, //
		        			donGiaNhanCong:'',	
		        			soLuong:'',
		        			description: '', //01/07/2015; them ghi chu vuot dinh muc	
		        			tongTien:'',	
		        			lanSua:listObj[i].repairCount,
		        			dataView: listObj[i]
		        		};
		        		var idx1 = 0;
		        		var rowsss = $('#gridDetail').datagrid("getRows");
		        		if (rowsss.length > 0) {
			        		idx1 = rowsss.length - 1;
			        	}
		        		$('#gridDetail').datagrid('insertRow',{row:row,index:idx1});
		        		RepairEquipmentManageCatalog._lstEquipItemId.push(row.id);
		        	}
		        	RepairEquipmentManageCatalog.loadDateTimerPickerNgayBaoHanh();
		        	RepairEquipmentManageCatalog.bindEventChangeText();
		        } 
		        $('#common-dialog-search-2-textbox').dialog("close");
		    },
		    //url : '/commons/search-equipItem',
		    url: '/commons/search-equipItem-popup-repair',
		    title: 'Chọn hạng mục',
		    columns : [[
		        {field:'code', title:'Mã hạng mục', align:'left', width: 100, sortable:false, resizable:false, formatter: function(value, row, index) {
			    	return Utils.XSSEncode(value);
			    }},
		        {field:'name', title:'Tên hạng mục', align:'left', width: 200, sortable:false, resizable:false, formatter: function(value, row, index) {
			    	return Utils.XSSEncode(value);
			    }},
		        {field:'cb', checkbox:true, align:'center', width:80,sortable : false,resizable : false},
		    ]]
		});
	},
	/**
	 * loadDateTimerPickerNgayBaoHanh va ngay het han bao hanh luong
	 * @author vuongmq
	 * @since 07/04/2015
	 */
	loadDateTimerPickerNgayBaoHanh: function() {
		$(".warantyDateInGridClzz:not(.hasDatepicker)").each(function() {
			setDateTimePicker($(this).attr("id"));
		});
		/** dateTimePicker cho ngay het han bao hanh luong*/
		$(".warantyExpiredDateInGridClzz:not(.hasDatepicker)").each(function() {
			setDateTimePicker($(this).attr("id"));
			var valueCheckNgayHetHanBaoHanh = $('#checkExpiredDate').val();
			if (valueCheckNgayHetHanBaoHanh == 0 || valueCheckNgayHetHanBaoHanh == '0') {
				disableDateTimePicker($(this).attr("id"));
			}
			//enableDateTimePicker($(this).attr('id'));
		});
	},
	/**
	 * tra ve don gia vat tu, don gia nhan cong, de view len
	 * @author vuongmq
	 * @since 07/04/2015
	 */
	getVatTuNhanCongHangMuc: function(row, type){
		var value ='';
		if (row != undefined && row != null) {
			if (type == RepairEquipmentManageCatalog._VATTU) {
				if (row.fromMaterialPrice != null && row.toWorkerPrice != null) {
					//value = formatCurrency(row.fromMaterialPrice) +' - '+ formatCurrency(row.toMaterialPrice);
					if (!isNaN(row.fromMaterialPrice) && !isNaN(row.toMaterialPrice)) {
						if (Number(row.fromMaterialPrice) != Number(row.toMaterialPrice)) {
							value = formatCurrency(row.fromMaterialPrice) +' - '+ formatCurrency(row.toMaterialPrice);
						} else {
							// truong hop bang nhau lay 1 gia tri
							value = formatCurrency(row.fromMaterialPrice);
						}
					}					
				} else if (row.fromMaterialPrice != null) {
					value = '>= '+formatCurrency(row.fromMaterialPrice);
				} else if (row.toMaterialPrice != null) {
					value = '<= '+formatCurrency(row.toMaterialPrice);
				}
			} else if (type == RepairEquipmentManageCatalog._NHANCONG) {
				if (row.fromWorkerPrice != null && row.toWorkerPrice != null) {
					//value = formatCurrency(row.fromWorkerPrice) +' - '+ formatCurrency(row.toWorkerPrice);
					if (!isNaN(row.fromWorkerPrice) && !isNaN(row.toWorkerPrice)) {
						if (Number(row.fromWorkerPrice) != Number(row.toWorkerPrice)) {
							value = formatCurrency(row.fromWorkerPrice) +' - '+ formatCurrency(row.toWorkerPrice);
						} else {
							// truong hop bang nhau lay 1 gia tri
							value = formatCurrency(row.fromWorkerPrice);
						}
					}
				} else if (row.fromWorkerPrice != null) {
					value = '>= '+formatCurrency(row.fromWorkerPrice);
				} else if (row.toWorkerPrice != null) {
					value = '<= '+formatCurrency(row.toWorkerPrice);
				}
			}
		}
		return value;
	},
	
	/**
	 * Show popup tim kiem thiet bi
	 * @author phuongvm
	 * @since 06/01/2015
	 */
	showPopupSearchEquipment: function() {
		$('.ErrorMsgStyle').html('').hide();
		$('#easyuiPopupSearchEquipment').show();
		$('#easyuiPopupSearchEquipment').dialog({  
	        closed: false,  
	        cache: false,  
	        modal: true,
	        width : 1000,
//	        height : 'auto',
	        onOpen: function(){	    
	        	//setDateTimePicker('yearManufacturing');
	        	Utils.bindComboboxEquipGroupEasyUICodeName('groupEquipment', null, 193);
	        	Utils.bindComboboxEquipGroupEasyUICodeName('providerId', null, 193);
	        	var year = new Date();
	        	year = year.getFullYear(); 
	        	var op = '<option value="" selected="selected"></option><option value="'+year+'">'+year+'</option>';
	        	for(var i=1; i<100; i++){
	        		op+='<option value="'+(year-i)+'">'+(year-i)+'</option>';
	        	}
	        	$('#yearManufacturing').html(op);
	        	$('#equipmentCode, #seriNumber, #typeEquipment').bind('keyup',function(event){
	        		if(event.keyCode == keyCodes.ENTER){
	        			$('#btnSeachEquipmentDlg').click(); 
	        		}
	        	});
	        	$('#yearManufacturing').bind('keyup',function(event){
	        		if(event.keyCode == keyCodes.ENTER){
	        			$('#btnSeachEquipmentDlg').click(); 
	        		}
	        	});
	        	$('#groupEquipment, #providerId').next().find(".combo-text.validatebox-text").bind('keyup',function(event){
	        		if(event.keyCode == keyCodes.ENTER){
	        			$('#btnSeachEquipmentDlg').click(); 
	        		}
	        	});
	        	$('#stockCode').bind('keyup',function(event){
	        		if(event.keyCode == keyCodes.ENTER){
	        			$('#btnSeachEquipmentDlg').click(); 
	        		}
	        	});
//	        	$('#stockCode').bind('keyup',function(event){
//	        		if(event.keyCode == keyCodes.F9){
//	        			RepairEquipmentManageCatalog.showPopupStock();
//	        		}
//	        	});
	        	RepairEquipmentManageCatalog.getEquipCatalog();
	        	var params = new Object();
	        	var groupId = $('#groupEquipment').combobox('getValue');
	        	if (groupId == -1 || groupId == "") {
	        		groupId = "";
	        	}
	        	var providerId = $('#providerId').combobox('getValue');
	        	if (providerId == -1 || providerId == "") {
	        		providerId = "";
	        	}
				params = {
						equipCode: $('#equipmentCode').val().trim(),
						seriNumber: $('#seriNumber').val().trim(),
						categoryId: $('#typeEquipment').val().trim(),
						// groupId: $('#groupEquipment').val().trim(),
						groupId: groupId,
						// providerId: $('#providerId').val().trim(),
						providerId: providerId,
						yearManufacture: $('#yearManufacturing').val().trim(),
						stockCode: $('#stockCode').val().trim()
					};
				$('#equipmentGridDialog').datagrid({
					url : '/equipment-repair-manage/search-equipment-delivery',
					autoRowHeight : true,
					rownumbers : true, 
					//fitColumns:true,
					pagination:true,
					scrollbarSize:0,
					pageSize:10,
					pageList: [10],
					width: $('#equipmentGridDialogContainer').width(),
					height: 'auto',
					autoWidth: true,	
					queryParams: params,
					columns:[[
						{field: 'select',title:'',width: 50,sortable:false,resizable:false, align: 'center', formatter: function(value, row, index) {
							return '<a href="javascript:void(0);" onclick="RepairEquipmentManageCatalog.selectEquipment('+index+');">Chọn</a>';
						}},	      
					    {field: 'equipmentCode',title:'Mã thiết bị',width: 200,sortable:false,resizable:false, align: 'left', formatter: function(value, row, index) {
							return Utils.XSSEncode(value);
						}},	
					    {field: 'seriNumber',title:'Số serial',width: 100,sortable:false,resizable:false, align: 'left', formatter: function(value, row, index) {
							return Utils.XSSEncode(value);
						}},	
					    {field: 'healthStatus',title:'Tình trạng thiết bị',width: 100,sortable:false,resizable:false, align: 'left', formatter: function(value, row, index) {
							return Utils.XSSEncode(value);
						}},	
					    {field: 'stock',title:'Kho',width: 120,sortable:false,resizable:false, align: 'left', formatter: function(value, row, index) {
					    	var html="";
							if(row != null && row.stockCode != null && row.stockName!= null){
								html = row.stockCode+' - '+row.stockName; 
							}
							return Utils.XSSEncode(html);
						}},	
					    {field: 'typeEquipment',title:'Loại thiết bị',width: 80,sortable:false,resizable:false, align: 'left', formatter: function(value, row, index) {
							return Utils.XSSEncode(value);
						}},	
					    {field: 'groupEquipment',title:'Nhóm thiết bị',width: 200,sortable:false,resizable:false, align: 'left', formatter: function(value, row, index) {
					    	var html="";
							if(row != null && row.groupEquipmentCode != null && row.groupEquipmentName!= null){
								html = row.groupEquipmentCode+' - '+row.groupEquipmentName; 
							}
							return Utils.XSSEncode(html);
						}},	
					    {field: 'capacity',title:'Dung tích (lít)',width: 80,sortable:false,resizable:false, align: 'right', formatter: function(value, row, index) {
							return Utils.XSSEncode(value);
						}},	
					    {field: 'equipmentBrand',title:'Hiệu',width: 80,sortable:false,resizable:false, align: 'left', formatter: function(value, row, index) {
							return Utils.XSSEncode(value);
						}},	
					    {field: 'equipmentProvider',title:'NCC',width: 80,sortable:false,resizable:false, align: 'left', formatter: function(value, row, index) {
							return Utils.XSSEncode(value);
						}},	
					    {field: 'yearManufacture',title:'Năm sản xuất',width: 80,sortable:false,resizable:false, align: 'left', formatter: function(value, row, index) {
							return Utils.XSSEncode(value);
						}}
					]],
					onBeforeLoad:function(param){											 
					},
					onLoadSuccess :function(data){   			 
						if (data == null || data.total == 0) {
							//$("#errMsgInfo").html("Không tìm thấy danh sách biên bản giao nhận thiết bị!").show();
						}else{
							$('.datagrid-header-rownumber').html('STT');		    	
						}	    		
					}
				});
	        },
			onClose: function(){
				$('#easyuiPopupSearchEquipment').hide();
				$('#equipmentCode').val('').change();
				$('#seriNumber').val('').change();
				$('#typeEquipment').html('<option value="" selected="selected">Tất cả</option>');
				// $('#groupEquipment').html('<option value="" selected="selected">Tất cả</option>').change();
				// $('#providerId').html('<option value="" selected="selected">Tất cả</option>');
				$('#yearManufacturing').val('').change();
				$('#stockCode').val('').change();
				$('#equipmentGridDialog').datagrid("loadData",[]);
			}
		});
	},
	/**
	 * Tim kiem thiet bi
	 * @author nhutnn
	 * @since 12/12/2014
	 */
	searchEquipment: function(){
		var params=new Object(); 
		params.equipCode = $('#equipmentCode').val().trim();
		params.seriNumber = $('#seriNumber').val().trim();
		params.categoryId = $('#typeEquipment').val().trim();
		//tamvnm: thay doi thanh autoCompleteCombobox: -1 la truong hop tim group ko co trong BD
		// params.groupId = $('#groupEquipment').val().trim();
		if ($('#groupEquipment').combobox('getValue') == "") {
			$('#groupEquipment').combobox('setValue', -1);
			params.groupId = "";
		} else if ($('#groupEquipment').combobox('getValue') == -1) {
			params.groupId = "";
		} else {
			if (Utils.getMessageOfInvaildNumberNew($('#groupEquipment').combobox("getValue")).length == 0) {
				params.groupId = $('#groupEquipment').combobox("getValue");
			} else {
				params.groupId = -1;
			}
		}
		// params.providerId = $('#providerId').val().trim();
		if ($('#providerId').combobox('getValue') == "") {
			$('#providerId').combobox('setValue', -1);
			params.providerId = "";
		} else if ($('#providerId').combobox('getValue') == -1) {
			params.providerId = "";
		} else {
			if (Utils.getMessageOfInvaildNumberNew($('#providerId').combobox("getValue")).length == 0) {
				params.providerId = $('#providerId').combobox("getValue");
			} else {
				params.providerId = -1;
			}
		}
		params.yearManufacture = $('#yearManufacturing').val().trim();
		params.stockCode = $('#stockCode').val().trim();
		$('#equipmentGridDialog').datagrid('load',params);
	},
	/**
	 * Chon thiet bi fill vao cac textbox
	 * @author phuongvm
	 * @since 06/01/2015
	 */
	selectEquipment: function(index){
		var rowPopup = $('#equipmentGridDialog').datagrid('getRows')[index];
		var kho="";
		if(rowPopup.stockCode != undefined && rowPopup.stockCode != null && rowPopup.stockName != undefined && rowPopup.stockName!= null){
			kho = rowPopup.stockCode+' - '+rowPopup.stockName; 
		}
		$('#txtEquipCode').val(rowPopup.equipmentCode);
		$('#lblEquipName').html(Utils.XSSEncode(rowPopup.groupEquipmentName));
		$('#txtStock').val(kho);
		$('#txtExpiredDate').val(rowPopup.warrantyExpiredDate); /** 06/04/2015; vuongmq; ngay het han bh*/
		$('#txtRepairCount').val(rowPopup.repairCount);
		var rows = new Array();
    	RepairEquipmentManageCatalog._lstEquipItemId = new Array();
    	$('#gridDetail').datagrid('loadData',rows);
		
		$('#easyuiPopupSearchEquipment').dialog('close');
	},
	/**
	 * Lay danh muc loai, ncc thiet bi
	 * @author nhutnn
	 * @since 20/12/2014
	 */
	getEquipCatalog: function(){
		$('#typeEquipment').html('<option value="" selected="selected">Tất cả</option>');
    	$('#providerId').html('<option value=""  selected="selected">Tất cả</option>');
    	$('#groupEquipment').html('<option value="" selected="selected">Tất cả</option>'); // vuongmq; 16/06/2015; reset lai
		$.getJSON('/equipment-repair-manage/get-equipment-catalog', function(result) {				
			if(result.equipsCategory != undefined && result.equipsCategory != null && result.equipsCategory.length > 0) {					
				for(var i=0; i < result.equipsCategory.length; i++){
					$('#typeEquipment').append('<option value="'+ result.equipsCategory[i].id +'">'+ Utils.XSSEncode(result.equipsCategory[i].code) +' - ' +Utils.XSSEncode(result.equipsCategory[i].name) +'</option>');  
				}
			} 
			$('#typeEquipment').change();
			// if(result.equipProviders != undefined && result.equipProviders != null && result.equipProviders.length > 0) {					
			// 	for(var i=0; i < result.equipProviders.length; i++){
			// 		$('#providerId').append('<option value="'+ result.equipProviders[i].id +'">'+ Utils.XSSEncode(result.equipProviders[i].code) +' - '+Utils.XSSEncode(result.equipProviders[i].name)+'</option>');  
			// 	}
			// } 
			// $('#providerId').change();	

			if(result.equipProviders != undefined && result.equipProviders != null && result.equipProviders.length > 0) {					
				for (var i = 0, isize = result.equipProviders.length; i < isize; i++) {
					result.equipProviders[i].searchText = unicodeToEnglish(result.equipProviders[i].code + " " + result.equipProviders[i].name);
 					result.equipProviders[i].searchText = result.equipProviders[i].searchText.toUpperCase();
 					result.equipProviders[i].code = Utils.XSSEncode(result.equipProviders[i].code);
 					result.equipProviders[i].name = Utils.XSSEncode(result.equipProviders[i].name);
				}
				var obj = {
					id: -1,
					code: "Tất cả",
					name: "Tất cả",
					codeName: "Tất cả",
					searchText: unicodeToEnglish("Tất cả").toUpperCase()
				};
				result.equipProviders.splice(0, 0, obj);
				$('#providerId').combobox("loadData", result.equipProviders);
		 		$('#providerId').combobox("setValue", -1);
			}

			// if(result.lstEquipGroup != undefined && result.lstEquipGroup != null && result.lstEquipGroup.length > 0) {					
			// 	for(var i=0; i < result.lstEquipGroup.length; i++){
			// 		$('#groupEquipment').append('<option value="'+ result.lstEquipGroup[i].id +'">'+ Utils.XSSEncode(result.lstEquipGroup[i].code) +' - '+Utils.XSSEncode(result.lstEquipGroup[i].name)+'</option>');  
			// 	}
			// } 
			// $('#groupEquipment').change();
			if(result.lstEquipGroup != undefined && result.lstEquipGroup != null && result.lstEquipGroup.length > 0) {	
				for (var i = 0, isize = result.lstEquipGroup.length; i < isize; i++) {
					result.lstEquipGroup[i].searchText = unicodeToEnglish(result.lstEquipGroup[i].code + " " + result.lstEquipGroup[i].name);
	 				result.lstEquipGroup[i].searchText = result.lstEquipGroup[i].searchText.toUpperCase();
	 				// result.lstEquipGroup[i].code = Utils.XSSEncode(result.lstEquipGroup[i].code);
	 				// result.lstEquipGroup[i].name = Utils.XSSEncode(result.lstEquipGroup[i].name);
				}				
				var obj = {
				id: -1,
				code: "Tất cả",
				name: "Tất cả",
				codeName: "Tất cả",
				searchText: unicodeToEnglish("Tất cả").toUpperCase()
				};
				result.lstEquipGroup.splice(0, 0, obj);
				$('#groupEquipment').combobox("loadData", result.lstEquipGroup);
		 		$('#groupEquipment').combobox("setValue", -1);
			} 				
		});
	},
	/**
	 * Xu ly trên input loai thiet bi
	 * @author nhutnn
	 * @since 19/12/2014
	 */
	onChangeTypeEquip: function(cb){
		var typeEquip = $(cb).val().trim();
		$('#groupEquipment').html('<option value="" selected="selected">Tất cả</option>');
		if(typeEquip != undefined && typeEquip != null && typeEquip != ''){
			$.getJSON('/equipment-manage-delivery/get-equipment-group?id='+typeEquip, function(result) {				
				if(result.equipGroups != undefined && result.equipGroups != null && result.equipGroups.length > 0) {					
					for(var i=0; i < result.equipGroups.length; i++){
						$('#groupEquipment').append('<option value="'+ result.equipGroups[i].id +'">'+ Utils.XSSEncode(result.equipGroups[i].code) + ' - '+Utils.XSSEncode(result.equipGroups[i].name)+'</option>');  
					}
				} 
				$('#groupEquipment').change();				
			});
		}
	},

	/**
	 * bind su kien khi thay doi text box ngay bat dau bao hanh, hien thi so
	 * @author phuongvm
	 * @since 06/01/2015
	 */
	changeNgayBatDauBaoHanh: function(index, id){
		if (id != null && id != RepairEquipmentManageCatalog._valueTruMot) {
			var ngayBatDauBaoHanh = $('#warantyDateInGrid_'+id).val();
			if (ngayBatDauBaoHanh != null && ngayBatDauBaoHanh != '') {
				var data = $('#gridDetail').datagrid('getRows')[index];
				if (data != null) {
					var soThangBaoHanhMacDinh = 0;
					if (data.dataView != undefined && data.dataView != null) {
						soThangBaoHanhMacDinh = data.dataView.warranty;
						$('#warantyInGrid_'+id).val(soThangBaoHanhMacDinh);
					}
					var ngayGiaHan = soThangBaoHanhMacDinh*value30NgayInfoJS;
					var ngayHetHanBaoHanh = getNextDateForNumber(ngayBatDauBaoHanh, ngayGiaHan);
					if(ngayHetHanBaoHanh != undefined && ngayHetHanBaoHanh != null){
						$('#warantyExpiredDateInGrid_'+id).val(ngayHetHanBaoHanh);	
					}
				}
			}
		}
	},
	/**
	 * bind su kien khi thay doi text box gridDetail chi tiet han muc
	 * @author phuongvm
	 * @since 06/01/2015
	 */

	 /**
	 * cap nhat; bind su kien khi thay doi text box gridDetail chi tiet han muc
	 * @author vuongmq
	 * @since 14/04/2015
	 */
	bindEventChangeText: function(){
		/** chon so thang bao hanh */
		$('.warantyInGridClzz').blur(function(){
			var idTmp = $(this).attr('id');
			var id = idTmp.split('_')[1];
			if (id != undefined && id != null && id != '') {
				var ngayBatDauBaoHanh = $('#warantyDateInGrid_'+id).val();
				if (ngayBatDauBaoHanh != undefined && ngayBatDauBaoHanh != null && Utils.isDate(ngayBatDauBaoHanh)) {
					var ngay = 0;
					if ($(this).val() != undefined && $(this).val() != null) {
						ngay = $(this).val();
					}
					var ngayGiaHan = ngay*value30NgayInfoJS;
					var ngayHetHanBaoHanh = getNextDateForNumber(ngayBatDauBaoHanh, ngayGiaHan);
					if(ngayHetHanBaoHanh != undefined && ngayHetHanBaoHanh != null){
						$('#warantyExpiredDateInGrid_'+id).val(ngayHetHanBaoHanh);	
					}
				}
			}
		});
		/** chon don gia vat tu; lay tong vat tu detail */
		$('.materialPriceInGridClzz').blur(function(){
			$('.ErrorMsgStyle').html('').hide();
			var idTmp = $(this).attr('id');
			var id = idTmp.split('_')[1];
			if (id != undefined && id != null && id != '') {
				RepairEquipmentManageCatalog.tongVatTuDetail(id);
			}
		});
		/** chon don gia nhan cong; lay max gia nhan cong */
		$('.wokerPriceInGridClzz').blur(function(){
			$('.ErrorMsgStyle').html('').hide();
			RepairEquipmentManageCatalog.maxWorkerPriceChangeTextDetail();
		});
		/** chon so luong*/
		$('.quantityInGridClzz').blur(function () {
		    var idQuan = $(this).attr('id');
		    var soLuong = $(this).val();
		    var id = idQuan.split('_')[1];
		    if (id != undefined && id != null && id != '') {
			    var donGiaVT = $('#materialPriceInGrid_'+id).val();
			    var donGiaNC = $('#wokerPriceInGrid_'+id).val();
			    if (donGiaVT==null) {
			      donGiaVT = 0;
			    }
			    if (donGiaNC==null) {
			      donGiaNC = 0;
			    }		
			    //var tong = soLuong*donGiaVT+Number(donGiaNC);
			    var tong = soLuong*donGiaVT;
			    tong = formatCurrency(tong);
			    $('#tongTien_'+id).val(tong);
			    RepairEquipmentManageCatalog.totalAmountChangeText();
			}
		});
		$('.vinput-money').each(function(){
			var objectId = $(this).attr('id');
			if (objectId!=null && objectId!=undefined && objectId.length>0) {
				Utils.formatCurrencyFor(objectId);
			}			
		});
		Utils.bindFormatOnTextfieldInputCss('vinput-money',Utils._TF_NUMBER);
//		Utils.bindFormatOnTextfieldInputCss('warantyInGridClzz',Utils._TF_NUMBER);
//		Utils.bindFormatOnTextfieldInputCss('materialPriceInGridClzz',Utils._TF_NUMBER);
//		Utils.bindFormatOnTextfieldInputCss('wokerPriceInGridClzz',Utils._TF_NUMBER);
//		Utils.bindFormatOnTextfieldInputCss('quantityInGridClzz',Utils._TF_NUMBER);
	},
	totalAmountChangeText: function(){
		var tongCong = 0; 
		var row = $('#gridDetail').datagrid('getRows');
		var i = 1;
	    $('.tongTienClzz').each( function() {
	    	if (!RepairEquipmentManageCatalog._flagPagePheDuyet) {
	    		// khong phai trang phe duyet; 
				if (i != row.length) {
					var value = $(this).attr('value');
					value = Utils.returnMoneyValue(value);
					if(!isNaN(value)){
						tongCong +=Number(value);
					}
				}
			} else {
				var value = $(this).attr('value');
				value = Utils.returnMoneyValue(value);
				if(!isNaN(value)){
					tongCong +=Number(value);
				}
			}
			i++;
		});
		var nhanCong = $('#txtWorkerPrice').val();
		var value = Utils.returnMoneyValue(nhanCong);
		if (!isNaN(value)) {
			tongCong += Number(value);
		}
		/** tongCong += gia nhan cong*/
	    $('#txtTotalAmount').val(formatCurrency(tongCong));
	},
	tongVatTuDetail: function(id) {
	    var soLuong = $('#quantityInGrid_'+id).val();
	    if (soLuong==null) {
	    	soLuong = 0;
	    }
	    var donGiaVT = $('#materialPriceInGrid_'+id).val();
	    if (donGiaVT==null) {
	      donGiaVT = 0;
	    }
	    var tong = soLuong*donGiaVT;
	    tong = formatCurrency(tong);
	    $('#tongTien_'+id).val(tong);
	    RepairEquipmentManageCatalog.totalAmountChangeText();
	},
	/**
	 * bind su kien khi thay doi text box don gia nhan cong
	 * lay don gia nhan cong max -> Gia nhan cong (txtWorkerPrice)
	 * @author vuongmq
	 * @since 14/04/2015
	 */
	 maxWorkerPriceChangeTextDetail: function(isCheck) {
		var row = $('#gridDetail').datagrid('getRows');
		var i = 1;
		var maxNhanCong = 0;
	    $('.wokerPriceInGridClzz').each( function() {
	    	if (!RepairEquipmentManageCatalog._flagPagePheDuyet) {
		    	// khong phai trang phe duyet; 
				if (i != row.length) {
					var value = $(this).val();
					value = Utils.returnMoneyValue(value);
					if (!isNaN(value)) {
						if (parseInt(value) > parseInt(maxNhanCong)) {
							maxNhanCong = value;
						}
					}
				}
			} else {
				var value = $(this).val();
				value = Utils.returnMoneyValue(value);
				if (!isNaN(value)) {
					if (parseInt(value) > parseInt(maxNhanCong)) {
						maxNhanCong = value;
					}
				}
			}
			i++;
		});
		RepairEquipmentManageCatalog._maxWorkerPriceDetail = maxNhanCong;
		if (isCheck == undefined || isCheck == null) {
			// truong hop nhap gia tri duoi gridDetail don gia nhan cong thi gan gia tri
	    	$('#txtWorkerPrice').val(formatCurrency(maxNhanCong));
	    }
	    RepairEquipmentManageCatalog.totalAmountChangeText();
	},

	/**
	 * Xoa mot dong hang muc
	 * 
	 * @author phuongvm
	 * @since 06/01/2015
	 * */
	deleteRow: function (index) {
		var row = $('#gridDetail').datagrid('getRows')[index];
		if(row.id!=undefined){
			$('#gridDetail').datagrid("deleteRow", index);
			RepairEquipmentManageCatalog.totalAmountChangeText();
			for(var i = 0;i<RepairEquipmentManageCatalog._lstEquipItemId.length;i++){
				if(RepairEquipmentManageCatalog._lstEquipItemId[i] == row.id){
					RepairEquipmentManageCatalog._lstEquipItemId.splice(i,1);
					break;
				}
			}
			RepairEquipmentManageCatalog.reloadGridDetail();
		}
	},

	/**
	 * thuc hien load lai danh sach khi add hoac delete row
	 * 
	 * @author phuongvm
	 * @since 09/01/2015
	 * */
	reloadGridDetail:function(){
		var rows = new Array();
		$('.quantityInGridClzz').each(function () {
			var row = new Object();
			var idQuan = $(this).attr('id');
		    var id = idQuan.split('_')[1];
		    if(id!=undefined && id != null && id!= ''){
		    	row.id = id;
		    	row.hangMuc = $('#hangMuc_'+id).html();
		    	row.ngayBatDauBaoHanh = $('#warantyDateInGrid_'+id).val(); //
		    	setDateTimePicker('warantyDateInGrid_'+id);
		    	row.baoHanh = $('#warantyInGrid_'+id).val();
		    	row.ngayHetHanBaoHanh = $('#warantyExpiredDateInGrid_'+id).val();//
		    	setDateTimePicker('warantyExpiredDateInGrid_'+id);
		    	row.donGiaVatTuDinhMuc = $('#materialPriceDefaultInGrid_'+id).html(); //
		    	row.donGiaVatTu = $('#materialPriceInGrid_'+id).val();
		    	row.donGiaNhanCongDinhMuc = $('#wokerPriceDefaultInGrid_'+id).html(); //
			    row.donGiaNhanCong = $('#wokerPriceInGrid_'+id).val();
			    row.soLuong = $('#quantityInGrid_'+id).val();
			    row.description = $('#descriptionInGrid_'+id).val(); // 01/07/2015; them ghi chu vuot dinh muc
			    row.lanSua = $('#lanSua_'+id).val();
			    row.tongTien = $('#tongTien_'+id).val();
			    /** lay dataview gan lai*/
			    var index = $('#materialPriceInGrid_'+id).attr('gridIndex');
			    if (index != undefined && index != null) {
			    	var data = $('#gridDetail').datagrid ('getRows')[index];
			    	if (data != undefined && data != null) {
			    		row.dataView = data.dataView; /** lay record cua dataView gan lai*/
			    	}
			    }
			    rows.push(row);
		    }
		});
		$('#gridDetail').datagrid("loadData", rows);
	},
	showDlgImportExcel: function(){
		$('#easyuiPopupImportExcel').show();
		$('.ErrorMsgStyle').hide();
		$('#easyuiPopupImportExcel').dialog({  
	        closed: false,  
	        cache: false,  
	        modal: true,
	        onOpen: function(){	 
	        	$('.easyui-dialog .BtnGeneralStyle').css('color','');
	        },
	        onClose: function(){
	        	EquipmentListEquipment.searchListEquipment();
				$("#grid").datagrid('uncheckAll');
				$("#fakefilepcBBGN").val("");
				$("#excelFileBBGN").val("");				
	        }
		});
	},
	/**
	 * Xoa tap tin dinh kem
	 * 
	 * @author hunglm16
	 * @since Feb 10,214
	 * */
	removeEquipAttachFile: function(id){
		if (id == undefined || id == null || isNaN(id)) {
			return;
		}
		if (RepairEquipmentManageCatalog._arrFileDelete == null || RepairEquipmentManageCatalog._arrFileDelete == undefined) {
			RepairEquipmentManageCatalog._arrFileDelete = [];
		}
		RepairEquipmentManageCatalog._arrFileDelete.push(id);
		$('#divEquipAttachFile'+id).remove();
		if (RepairEquipmentManageCatalog._countFile != undefined && RepairEquipmentManageCatalog._countFile != null && RepairEquipmentManageCatalog._countFile > 0) {
			RepairEquipmentManageCatalog._countFile = RepairEquipmentManageCatalog._countFile - 1;
		} else {
			RepairEquipmentManageCatalog._countFile = 0;
		}
	},
	/**
	 * Lap bien ban sua chua
	 * 
	 * @author phuongvm
	 * @Since 07/01/2015
	 * */
	saveRepair: function(pheDuyet) {
		$(".ErrorMsgStyle").html('').hide();
		var msg = "";
		var err = ""; 
		var lstEquipItemId = new Array();
		var lstNgayBatDauBaoHanh = new Array();  // vuongmq 14/04/2015
		var lstNgayHetHanBaoHanh = new Array(); // vuongmq 14/04/2015
		var lstBaoHanh = new Array();
		var lstDonGiaVT = new Array();
		var lstDonGiaNC = new Array();
		var lstSoLuong = new Array();
		var lstDescription = new Array(); // vuongmq; 01/07/2015; them ghi chu vuot dinh muc
		var lstSoLanSuaChua = new Array();
		var lstTongTienDetail = new Array(); // vuongmq 14/04/2015

		var lstDonGiaVTDinhMucTu = new Array(); // vuongmq 24/04/2015
		var lstDonGiaVTDinhMucDen = new Array(); // vuongmq 24/04/2015
		var lstDonGiaNCDinhMucTu = new Array(); // vuongmq 24/04/2015
		var lstDonGiaNCDinhMucDen = new Array(); // vuongmq 24/04/2015
		
		
		msg = Utils.getMessageOfRequireCheck('txtEquipCode','Mã thiết bị');
		err = '#txtEquipCode';
		if (msg.length == 0) {
			msg = Utils.getMessageOfSpecialCharactersValidate('txtEquipCode', 'Mã thiết bị', Utils._CODE);
			err = '#txtEquipCode';
		}
		if (msg.length == 0) {
			msg = Utils.getMessageOfRequireCheck('txtRepairCount', 'Sửa lần thứ');
			err = '#txtEquipCode';
			if (msg.trim().length > 0) {
				$('#errMsgEquipDetail').html('Vui lòng nhập Mã thiết bị hợp lệ (nhấn F9)').show();
				$(err).focus();
				return;
			}
		}

		if(msg.length == 0 && $('#createDate').val().trim().length > 0 && !Utils.isDate($('#createDate').val()) && $('#createDate').val() != '__/__/____' ){
			msg = 'Ngày biên bản không tồn tại hoặc không đúng định dạng dd/mm/yyyy.';
			err = '#createDate';
		}

		var curDate = ReportUtils.getCurrentDateString(); //Lay ngay hien tai sysDate
		if (msg.length == 0 && !Utils.compareDate($('#createDate').val().trim(), curDate)) {
			msg = 'Ngày biên bản không được sau ngày hiện tại. Vui lòng nhập lại';
			$('#createDate').focus();
		}
		if (msg.length == 0 && $('#note').attr('maxlength') > 500) {
			msg = 'Bạn nhập quá giới hạn của trường note';
		}
		
		if (msg.length == 0) {
			if (RepairEquipmentManageCatalog._msgGiaNhanCongMax != null && RepairEquipmentManageCatalog._msgGiaNhanCongMax.length > 0) {
				msg = RepairEquipmentManageCatalog._msgGiaNhanCongMax;
				err = RepairEquipmentManageCatalog._msgGiaNhanCongMaxFocus;
			}
		}
		var rows = $('#gridDetail').datagrid('getRows');
		var length = rows.length;
		if(pheDuyet !=undefined && pheDuyet != null && pheDuyet == RepairEquipmentManageCatalog._PHEDUYET){ // neu man hinh phe duyet link qua thi phai + them 1 dong
			length+=1;
		}
		if (msg.length == 0) {
			if (length == 1) {
				//msg = "Chưa có hạng mục nào được chọn";
				msg = "Vui lòng Chọn hạng mục bằng F9";
			} else {
				$('.quantityInGridClzz').each(function () {
					var idQuan = $(this).attr('id');
				    var id = idQuan.split('_')[1];
				    if (id != undefined && id != null && id != '') {
				    	var ngayBatDauBaoHanh = $('#warantyDateInGrid_'+id).val().trim();
				    	var ngayHetHanBaoHanh = $('#warantyExpiredDateInGrid_'+id).val().trim();
				    	var soLuong = Utils.returnMoneyValue($('#quantityInGrid_'+id).val());
					    var donGiaVT = Utils.returnMoneyValue($('#materialPriceInGrid_'+id).val());
					    var donGiaNC = Utils.returnMoneyValue($('#wokerPriceInGrid_'+id).val());
					    var baoHanh = Utils.returnMoneyValue($('#warantyInGrid_'+id).val());
					    var lanSua = Utils.returnMoneyValue($('#lanSua_'+id).val());
					    var tongtienct = Utils.returnMoneyValue($('#tongTien_'+id).val());
					    var description = $('#descriptionInGrid_'+id).val(); // vuongmq; 01/07/2015; them ghi chu vuot dinh muc; no trim(0)
					    /** vuongmq; 24/04/2015; lay them don gia vat tu, nhan cong, tu va den theo dinh muc*/	
					    var donGiaVTDMTu = '';				    
					    var donGiaVTDMDen = '';
					    var donGiaNCDMTu = '';
					    var donGiaNCDMDen = '';
					    var index = $(this).attr('gridIndex');
						if (index != undefined && index > RepairEquipmentManageCatalog._valueTruMot) {
							var row = $('#gridDetail').datagrid('getRows')[index];
							if (row != undefined && row != null) {
								var data = row.dataView;
								if (data != undefined && data != null) {
									if (data.fromMaterialPrice != null) {
										donGiaVTDMTu = data.fromMaterialPrice;
									}
									if (data.toMaterialPrice != null) {
										donGiaVTDMDen = data.toMaterialPrice;
									}
									if (data.fromWorkerPrice != null) {
										donGiaNCDMTu = data.fromWorkerPrice;
									}
									if (data.toWorkerPrice != null) {
										donGiaNCDMDen = data.toWorkerPrice;
									}
								}
							}
						}
						if (ngayBatDauBaoHanh != null && ngayBatDauBaoHanh != '') {
							/** fix: 220117; vuongmq; 19/05/2015 kiem tra so thang bao hanh khi nhap ngay bat dau bao hanh*/
							if (baoHanh =='' || baoHanh =='0') {
								msg  = 'Vui lòng nhập Số tháng bảo hành lớn hơn 0';
						    	err = '#warantyInGrid_'+id;
						    	$('#errMsgEquipDetail').html(msg).show();
								$(err).focus();
								return false;
							}
						}
//					    if ((donGiaNC== ''|| donGiaNC== '0' ) && (donGiaVT== ''|| donGiaVT== '0' )) {
//					    	msg  = 'Vui lòng nhập đơn giá vật tư hoặc đơn giá nhân công lớn hơn 0';
//					    	err = '#materialPriceInGrid_'+id;
//					    	$('#errMsgEquipDetail').html(msg).show();
//							$(err).focus();
//							return false;
//					    }
					    //if ((donGiaVT== ''|| donGiaVT== '0' )&& soLuong!='') {
						if (donGiaVT == '0' && donGiaNC!= '' && donGiaNC!= '0') {
						    	msg  = 'Vui lòng nhập đơn giá vật tư lớn hơn 0';
						    	err = '#materialPriceInGrid_'+id;
						    	$('#errMsgEquipDetail').html(msg).show();
								$(err).focus();
								return false;
						}	
						if (donGiaVT != '' && donGiaVT != '0' && donGiaNC== '0') {
					    	msg  = 'Vui lòng nhập đơn giá nhân công lớn hơn 0';
					    	err = '#wokerPriceInGrid_'+id;
					    	$('#errMsgEquipDetail').html(msg).show();
							$(err).focus();
							return false;
						}						
					    if ((donGiaVT== ''|| donGiaVT== '0') && (donGiaNC== ''|| donGiaNC== '0')) {
					    	msg  = 'Vui lòng nhập đơn giá vật tư hoặc đơn giá nhân công lớn hơn 0';
					    	if(donGiaVT== ''|| donGiaVT== '0'){
					    		err = '#materialPriceInGrid_'+id;
					    	} else {
					    		err = '#wokerPriceInGrid_'+id;
					    	}					    	
					    	$('#errMsgEquipDetail').html(msg).show();
							$(err).focus();
							return false;
					    }
					    
						if ((donGiaVT != '' && donGiaVT != '0') && (soLuong == '' || soLuong == '0')) {
							msg = 'Vui lòng nhập số lượng lớn hơn 0';
							err = '#quantityInGrid_' + id;
							$('#errMsgEquipDetail').html(msg).show();
							$(err).focus();
							return false;
						}
					    // vuongmq; 01/07/2015; bat buoc phai nhap so luong
					  //   if(soLuong== ''|| soLuong== '0') {
					  //   	msg  = 'Vui lòng nhập số lượng lớn hơn 0';
					  //   	err = '#quantityInGrid_'+id;
					  //   	$('#errMsgEquipDetail').html(msg).show();
							// $(err).focus();
							// return false;
					  //   }
						if (donGiaVT == null) {
							donGiaVT = "";
						}
						if (donGiaNC == null) {
							donGiaNC = "";
						}
					    if (baoHanh==null) {
					    	baoHanh = "";
					    }
					    if (lanSua==null) {
					    	lanSua = "";
					    }
					    if (tongtienct == null) {
					    	tongtienct = "";
					    }
					    lstEquipItemId.push(id);
					    lstNgayBatDauBaoHanh.push(ngayBatDauBaoHanh); //
					    lstNgayHetHanBaoHanh.push(ngayHetHanBaoHanh); //
					    lstBaoHanh.push(baoHanh);
					    lstDonGiaVT.push(donGiaVT);
					    lstDonGiaNC.push(donGiaNC);
					    lstSoLuong.push(soLuong);
					    lstSoLanSuaChua.push(lanSua);
					    lstTongTienDetail.push(tongtienct); // tong tien dong chi tiet
					    lstDonGiaVTDinhMucTu.push(donGiaVTDMTu); //
					    lstDonGiaVTDinhMucDen.push(donGiaVTDMDen); //
					    lstDonGiaNCDinhMucTu.push(donGiaNCDMTu); //
					    lstDonGiaNCDinhMucDen.push(donGiaNCDMDen); //
					    lstDescription.push(description); // vuongmq; 01/07/2015; them ghi chu vuot dinh muc
				    }
				});
			}
		}
		if (msg.trim().length > 0) {
			$('#errMsgEquipDetail').html(msg).show();
			$(err).focus();
			return false;
		}
		var params = new Object();
		params.equipCode = $('#txtEquipCode').val().trim(); // ma thiet bi
		params.status = $('#cbxStatus').val().trim(); // trang thai
		params.expiredDate = $('#txtExpiredDate').val().trim() // ngay het han bao hanh
		params.workerPrice = $('#txtWorkerPrice').val().trim(); // giá nhan cong
		params.totalAmount = Utils.returnMoneyValue($('#txtTotalAmount').val().trim()); // tong tien
		params.repairCountForm = $('#txtRepairCount').val().trim(); // sua lan thu
		params.condition = $('#txtCondition').val().trim(); // tinh trang hu hong
		params.reason = $('#txtReason').val().trim(); // ly do de nghi
		params.createDate = $('#createDate').val();
		params.note = $('#note').val();
		if($('#txtRejectReason').val()!=undefined && $('#txtRejectReason').val()!= null){
			params.rejectReason = $("#txtRejectReason").val();
		}
		
		params.lstEquipItemId = lstEquipItemId;
		params.lstNgayBatDauBaoHanh = lstNgayBatDauBaoHanh; //
		params.lstNgayHetHanBaoHanh = lstNgayHetHanBaoHanh; //
		params.lstBaoHanh = lstBaoHanh;
		params.lstDonGiaVT = lstDonGiaVT;
		params.lstDonGiaNC = lstDonGiaNC;
		params.lstSoLuong = lstSoLuong;
		params.lstSoLanSuaChua = lstSoLanSuaChua;
		params.lstTongTienDetail = lstTongTienDetail; //
		params.lstDonGiaVTDinhMucTu = lstDonGiaVTDinhMucTu; // vuongmq; 24/04/2015
		params.lstDonGiaVTDinhMucDen = lstDonGiaVTDinhMucDen; // vuongmq; 24/04/2015
		params.lstDonGiaNCDinhMucTu = lstDonGiaNCDinhMucTu; // vuongmq; 24/04/2015
		params.lstDonGiaNCDinhMucDen = lstDonGiaNCDinhMucDen; // vuongmq; 24/04/2015
		params.lstDescription = lstDescription; // vuongmq; 01/07/2015; them ghi chu vuot dinh muc
		var idForm = $("#idRecordHidden").val();
		if(idForm!=null && idForm != ''){
			params.idForm = idForm;
		}
		if (RepairEquipmentManageCatalog._arrFileDelete == undefined || RepairEquipmentManageCatalog._arrFileDelete == null) {
			RepairEquipmentManageCatalog._arrFileDelete = [];
		}
		params.equipAttachFileStr = RepairEquipmentManageCatalog._arrFileDelete.join(',');
		var msgDialog = "Bạn có muốn lưu phiếu sửa chữa?";
		var href = '/equipment-repair-manage/info';
		if(pheDuyet !=undefined && pheDuyet != null && pheDuyet == RepairEquipmentManageCatalog._PHEDUYET){
			href = '/equipment-repair-manage/approved/info';
		}
		if (DropzonePlugin._dropzoneObject != null) {
			var arrFile = DropzonePlugin._dropzoneObject.getFilesWithStatus(Dropzone.ADDED);
			if (arrFile != null && arrFile != undefined && arrFile.length > 0) {
				if (RepairEquipmentManageCatalog._countFile == 5) {
					$('#errMsgEquipDetail').html("Tổng số tập tin đính kèm không được vượt quá 5").show();
					return false;
				}
				var maxLength = 5 - RepairEquipmentManageCatalog._countFile;
				if (arrFile.length > maxLength) {
					$('#errMsgEquipDetail').html("Số tập tin thêm mới không được vượt quá " + maxLength).show();
					return false;
				}
				msg = UploadUtil.getMsgByFileRequireCheck(UploadUtil._typeFileRequireCheck.equipRecord);
				if (msg.trim().length > 0) {
					$('#errMsgEquipDetail').html(msg).show();
					return false;
				}
				$.messager.confirm('Xác nhận', msgDialog, function(r){
					if (r){
						var data = JSONUtil.getSimpleObject2(params);
						UploadUtil.updateAdditionalDataForUpload(data);
						UploadUtil.startUpload('errMsgEquipDetail');
						return false;
					}
				});
				return false;
			}
		}
		Utils.addOrSaveData(params, "/equipment-repair-manage/saveRepair", null, 'errMsgEquipDetail', function(data) {
			if (data.error != undefined && !data.error) {
				//$("#successMsg").html("Lưu dữ liệu thành công").show();
				$("#successMsgEquipDetail").html("Lưu dữ liệu thành công").show();
				setTimeout(function(){
					window.location.href = href;
				}, 1500);				
			}else{
				if(data.errMsg!=undefined && data.errMsg!= null && data.errMsg!=''){
					$('#errMsgEquipDetail').html(data.errMsg).show();
				}
			}			
		}, null, null, null, msgDialog, function(data) {
			if (data.error != undefined && data.error) {
				$('#errMsgEquipDetail').html(data.errMsg).show();
			}		
		});
		return false;
	},
	/**
	 * Chon kho
	 * @author phuongvm
	 * @since 22/12/2014
	 */
	selectStock: function(shortCode){
		$('#warehouse').val(shortCode);		
		$('#easyuiPopupSearchStock').dialog('close');
	},
	exportExcel : function(notificationId){
		$('#errMsg').html('').hide();
		$.messager.confirm('Xác nhận','Bạn có muốn xuất file?',function(r){
			if(r){	
				var dataModel = new Object();
				dataModel.notificationId = notificationId;
				ReportUtils.exportReport('/adsprogram/notification/exportExcel',dataModel,'errMsg');									
				}			
		});	
 		return false;

	},

	/**
	 * In phieu Quan ly sua chua
	 * @author vuongmq
	 * @since 22/04/2015
	 * */
	printRepair: function() {
		$('.SuccessMsgStyle').html('').hide();
		$('.ErrorMsgStyle').html('').hide();
		var dataModel = new Object();
		var arra = RepairEquipmentManageCatalog._mapRepair.valArray;		
		var lstId = new Array();
		if (arra == null || arra.length == 0) {
			$('#errMsg').html('Bạn chưa chọn phiếu sửa chữa để in. Vui lòng chọn phiếu').show();
			return;
		}		
		for (var i=0, size = arra.length; i<size; ++i) {
			lstId.push(arra[i].id);			
		}
		dataModel.lstId = lstId; /** danh sach phieu sua chua de lay danh sach phieu sua chua*/
		$.messager.confirm('Xác nhận','Bạn có muốn in phiếu sửa chữa?',function(r){
			if(r){				
				ReportUtils.exportReport('/equipment-repair-manage/print-Repair', dataModel);
			}
		});	
		
	},
	/**
	 * Mo Dialog Nhap Excel
	 * 
	 * @author phuongvm
	 * @since 14/01/2015
	 * */

	 /**
	 * Mo Dialog Nhap Excel Quan ly sua chua
	 * @author vuongmq
	 * @since 20/04/2015
	 * */
	openDialogImportRepair: function (){
		$('.SuccessMsgStyle').html('').hide();
		$('.ErrorMsgStyle').html('').hide();
		$('#equipHistoryTemplateDiv').css("visibility","hidden").hide();
		$('#easyuiPopupImportExcel').dialog({
			title: 'Nhập Excel Quản lý sửa chữa',
			width: 465, 
			height: 'auto',
			closed: false,
			cache: false,
			modal: true,
			onOpen: function() {
				
			},
			onClose: function() {
				$('.ErrorMsgStyle').html('').hide();
				$("#grid").datagrid('uncheckAll');
				$("#fakefilepcListEquip").val("");
				$("#excelFileListEquip").val("");
			}
		});
	},
	/**
	 * Import Excel phieu sua chua
	 * 
	 * @author phuongvm
	 * @since 14/01/2015
	 * */

	 /**
	 * Import Excel phieu sua chua;
	 * cap nhat khong dong popup; khong thong bao luu du lieu thanh cong
	 * @author vuongmq
	 * @since 21/04/2015
	 * */
	importExcelRepair: function(){
		$('.SuccessMsgStyle').html('').hide();
		$('.ErrorMsgStyle').html('').hide();
		if($('#fakefilepcListEquip').val().length <= 0 ){
			$('#errExcelMsgListEquip').html('Vui lòng chọn tập tin Excel').show();
			return false;
		}
		Utils.importExcelUtils(function(data){
			RepairEquipmentManageCatalog.searchRepair();
			$('#fakefilepcListEquip').val("").change();
			if (data.message != undefined && data.message != null && data.message.trim().length > 0) {
				$('#errExcelMsgListEquip').html(data.message.trim()).change().show();
			}else{
				/*$('#successExcelMsgListEquip').html("Lưu dữ liệu thành công").show();
				var tm = setTimeout(function(){
					$('.SuccessMsgStyle').html('').hide();
					clearTimeout(tm);
					$('#easyuiPopupImportExcel').dialog('close');
				 }, 1500);*/
			}
		}, 'importFrmListEquip', 'excelFileListEquip', 'errExcelMsgListEquip');
		
	},
	upload : function(){ //upload trong fancybox
		$('#resultExcelMsg').html('').hide();
		$('#errExcelMsg').html('').hide();
		var msg="";
		var err="";
		var fDate = $('#fromDateDl').val().trim();	
		var content = $('#contentTxtDl').val().trim();
		var description = $('#descriptionDl').val().trim();
		if(msg.length == 0){
			msg = Utils.getMessageOfRequireCheck('contentTxtDl','Nội dung');
			err = '#contentTxtDl';
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfRequireCheck('descriptionDl','Mô tả');
			err = '#descriptionDl';
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfRequireCheck('fromDateDl','Ngày');
			err = '#fromDateDl';
		}
		if(msg.length == 0 && $('#fromDateDl').val().trim().length>0 && !Utils.isDate($('#fromDateDl').val(),'/')){
			msg = 'Ngày không tồn tại hoặc không đúng định dạng dd/mm/yyyy.';
			err = '#fromDateDl';
		}
		if(msg.length == 0) {
			msg = Utils.compareCurrentDateEx2('fromDateDl','Ngày');
			if(msg.length !=0){
				err = '#fromDateDl';
			}
		}
		if(msg.length>0){
			$('#errExcelMsg').html(msg).show();
			$(err).focus();
			setTimeout(function(){$('#errMsg').hide();},3000);
			return false;
		}
		if($('#excelFile').val() ==  ""){
			$('#errExcelMsg').html("Vui lòng chọn file Excel").show();
			return false;
		}
		var isLogin= 0;
		var isUploadDB= 0;
		if($('#checkLogin').is(':checked')){
			isLogin =1;
		}
		if($('#checkUploadDB').is(':checked')){
			isUploadDB =1;
		}
		var fH= Number($('#fromHour').val().trim()) *60 + Number($('#fromMinute').val().trim());
		var tH= Number($('#toHour').val().trim()) *60 + Number($('#toMinute').val().trim());
		if(fH>tH){
			$('#errExcelMsg').html("Từ giờ phải nhỏ hơn hoặc bằng đến giờ.").show();
			return false;
		}
		var fromH = $('#fromHour').val().trim()+':'+$('#fromMinute').val().trim();
		var toH = $('#toHour').val().trim()+':'+$('#toMinute').val().trim();
		fDate = $('#fromDateDl').val().trim();	
		var options = { 
				beforeSubmit: ReportUtils.beforeImportExcel,   
		 		success:      RepairEquipmentManageCatalog.afterImportExcel, 
		 		type: "POST",
		 		dataType: 'html',
		 		data:({isUploadDB:isUploadDB,isLogin:isLogin,fromDate:fDate,content:content,description:description,fromH:fromH,toH:toH})
		 	}; 
		$('#easyuiPopup #importFrm').ajaxForm(options);
		$.messager.confirm('Xác nhận','Bạn có muốn nhập từ file?',function(r){
			if(r){				
				$('#easyuiPopup #importFrm').submit();
			}
		});		
 		return false;
	},
	
	/**
	 * khoi tao grid danh sach cac hang muc sua chua
	 * @author tuannd20
	 * @param long equipmentRepairFormId id cua phieu sua chua
	 * @param viewActionType thao tac tren trang: CREATE, EDIT, VIEW
	 * @return void
	 */
	initializeRepairItemsGrid: function(equipmentRepairFormId, viewActionType) {
		$('#gridRepairItem').datagrid({
			url : "/equipment-repair-manage/searchRepairDetail",
			//pagination : true,
			rownumbers : true,
			pageNumber : 1,
			scrollbarSize: 0,
			autoWidth: true,
			//pageList: [10],
			autoRowHeight : true,
			fitColumns : true,
			singleSelect: true,
			queryParams: {equipRepairId: equipmentRepairFormId},
			width: $('#gridRepairItemContainer').width() - 15,
			columns: [[
				{field: 'equipItemName', title: 'Hạng mục', width: 200, sortable: false, resizable: false, align: 'left', formatter: function(value, row, index){
					var hangmuc = "";
					if(row.equipItem != null) {
						hangmuc = row.equipItem.name;
					}
					return Utils.XSSEncode(hangmuc);
				}},
				/*{field: 'warranty', title: 'Bảo hành (tháng)', width: 80, align: 'right', sortable: false, resizable: false, formatter: function(value, row, index){
					if (value != undefined && value != null) {
						return formatCurrency(value);
					}
					return '';
				}},*/
				{field: 'warrantyExpireDate', title: 'Ngày hết hạn bảo hành', width: 80, align: 'right', sortable: false, resizable: false, formatter: function(value, row, index){
					if (value != undefined && value != null) {
						return $.datepicker.formatDate('dd/mm/yy', new Date(value.substr(0,10)));
					}
					return '';
				}},
				{field: 'materialPrice', title: 'Đơn giá vật tư', width: 90, align: 'right', sortable: false, resizable: false, formatter: function(value, row, index){
					if (value != undefined && value != null) {
						return formatCurrency(value);
					}
					return '';
				}},
				/*{field: 'workerPrice', title: 'Đơn giá nhân công', width: 90, align: 'right',sortable: false, resizable: false, formatter: function(value, row, index){
					if (value != undefined && value != null) {
						return formatCurrency(value);
					}
					return '';
				}},*/
				{field: 'quantity', title: 'Số lượng', width:60, sortable: false, resizable: false , align: 'right', formatter: function(value, row, index){
					if (value != undefined && value != null) {
						return formatCurrency(value);
					}
					return '';
				}},
				{field: 'repairCount', title: 'Lần sửa', width: 50, sortable: false, resizable: false , align: 'right', formatter: function(value, row, index){
					return isNaN(value) ? Utils.XSSEncode(value) : value;
				}},
				{field: 'totalAmount', title: 'Tổng tiền (VNĐ)', width: 100, sortable: false, resizable: false, align: 'right', formatter: function(value, row, index) {
					if (value != undefined && value != null) {
						return formatCurrency(value);
					}
					return '';
				}},
				/*{field: 'totalActualAmount', title: 'Tổng tiền thực tế (VNĐ)', width: 120, sortable: false, resizable: false , align: 'right',
					formatter: function(value, row, index) {
						if (viewActionType === ViewActionType.CREATE || viewActionType === ViewActionType.EDIT) {
							return '<input id="totalActualAmount_' + row.id + '" class="InputTextStyle" type="text" style="margin-bottom: 2px; width: 96%; text-align: right;" value="' + (value ? formatCurrency(Utils.XSSEncode(value)) : 0) + '"/>';
						}
						return value ? formatCurrency(Utils.XSSEncode(value)) : 0;
					}
				},*/
			]],
			onLoadSuccess:function(data){
				$('.datagrid-header-rownumber').html('STT');
				//Utils.functionAccessFillControl('equipHistoryDg');
				if (viewActionType === ViewActionType.CREATE || viewActionType === ViewActionType.EDIT) {
					if (data && data.rows && data.rows.length) {
						/*var $grid = $('#gridRepairItem');
						for (var i = 0; i < data.rows.length; i++) {
							$grid.datagrid('beginEdit', i);
							var totalActualAmountEditor = $grid.datagrid('getEditor', {index: i, field: 'totalActualAmount'});
							$(totalActualAmountEditor.target).attr('maxlength', 22);
						}*/
						/** vuongmq: 20/04/2015; comment lai; khong cho nhap gia tri tien nua*/
						/*$('#gridRepairItemContainer td[field=totalActualAmount] input').each(function() {
						    var totalActualAmountInputId = $(this).attr('id');
						    Utils.bindFormatOnTextfield(totalActualAmountInputId, Utils._TF_NUMBER);
						    Utils.formatCurrencyFor(totalActualAmountInputId);
						    var maxlength = 17 + Math.floor(17/3);
						    $(this).attr('maxlength', maxlength);
						    $(this)
						    .on('paste blur', function () {
								var element = this;
								setTimeout(function () {
									var text = $(element).val();
									var rawText = text.replace(/,/g, '');
									if (rawText.length > 17) {
										rawText = rawText.substr(0, 17);
										console.log(rawText);
										$(element).val(formatCurrency(rawText));
									}
								}, 50);
							});
						});*/
						RepairEquipmentManageCatalog.loadInfoPayment(data.rows);
					}
				}
	   		 	$(window).resize();			 
			}
		});
	},

	/**
	 * lay cac gia tri tien len thong tin chung
	 * @param: rows: nhieu dong EquipRepairFormDtl
	 * @return void
	 */
	loadInfoPayment: function(rows) {
		if (rows != null) {
			var giaNhanCong = 0;
			var giaVatTu = 0;
			var tongTien = 0;
			//lay o equipRepairForm
			if (rows[0] != null && rows[0].equipRepairForm != null) {
				// gia nhan cong
				if (rows[0].equipRepairForm.workerPrice != null) {
					giaNhanCong = rows[0].equipRepairForm.workerPrice;
				}
				$('#txtWorkerPrice').val(formatCurrency(giaNhanCong));
				/** gia vat tu $('#txtMaterialPrice').val();  cai nay duyet danh sach detail cong tong tien lai*/
				/** tong tien */
				/*tongTien = rows[0].equipRepairForm.totalAmount;
				$('#txtTotalAmount').val(formatCurrency(tongTien));*/
			}
			var len = rows.length;
			for (var i = 0; i < len; i++) {
				if (rows[i].totalAmount != null && !isNaN(rows[i].totalAmount)) {
					giaVatTu += Number(rows[i].totalAmount);
				}
			}
			//  gia vat tu
			$('#txtMaterialPrice').val(formatCurrency(giaVatTu));
			if (!isNaN(giaNhanCong) && !isNaN(giaVatTu)) {
				tongTien += Number(giaNhanCong) + Number(giaVatTu);
			}
			// tong tien
			$('#txtTotalAmount').val(formatCurrency(tongTien));
		}
	},

	/**
	 * khoi tao control tren trang
	 * @param  viewActionType loai hanh dong tren trang: create, edit, read-only
	 * @return void
	 */
	initializePageControl: function(viewActionType) {
		if (viewActionType === ViewActionType.EDIT) {
			disabled('txtPaymentRecordCode');
			disableDateTimePicker('txtPaymentDate');
		} else if (viewActionType === ViewActionType.READONLY) {
			disabled('txtPaymentRecordCode');
			disableDateTimePicker('txtPaymentDate');
			$('#btnUpdate').remove();
		}
	},
	/**
	 * cap nhat phieu thanh toan
	 * @author tuannd20
	 * @param  long equipmentRepairRecordId id cua phieu sua chua
	 * @return void
	 */

	 /**
	 * cap nhat phieu thanh toan; co them cac loai tien
	 * @author vuongmq
	 * @param  long equipmentRepairRecordId id cua phieu sua chua
	 * @return void
	 */
	updateEquipmentRepairPaymentRecord: function(equipmentRepairRecordId) {
		var showErrorMsg = function(msg, errorFieldId) {
			$('#errMsg').html(msg).show();
			$(errorFieldId).focus();
			setTimeout(function(){
				$('#errMsg').hide();
			} ,3000);
			return;
		};
		var paymentRecordCode = $('#txtPaymentRecordCode').val();
		var paymentDate = $('#txtPaymentDate').val();
		var workerPrice = $('#txtWorkerPrice').val();
		var materialPrice = $('#txtMaterialPrice').val();
		var totalAmount = $('#txtTotalAmount').val();
		var msg = '';
		msg = Utils.getMessageOfRequireCheck('txtPaymentRecordCode','Mã phiếu');
		var errorFieldId = '#txtEquipCode';
		if (msg.length == 0){
			msg = Utils.getMessageOfSpecialCharactersValidate('txtPaymentRecordCode', 'Mã phiếu', Utils._CODE);
			errorFieldId = '#txtEquipCode';
		}

		if (isNullOrEmpty(msg) && 
			(isNullOrEmpty(paymentDate) 
			|| (paymentDate.trim().length>0 && !Utils.isDate(paymentDate,'/')))){
			msg = 'Ngày thanh toán không tồn tại hoặc không đúng định dạng dd/mm/yyyy.';
			errorFieldId = '#txtPaymentDate';
		}

		if (!isNullOrEmpty(msg)) {
			showErrorMsg(msg, errorFieldId);
			return;
		}

		var $grid = $('#gridRepairItem');
		var paymentRecordDetailsParam = [];
		var paymentRecordDetails = $grid.datagrid('getRows');
		for (var i = 0; i < paymentRecordDetails.length; i++) {
			var paymentRecordDetail = paymentRecordDetails[i];
			/*var totalActualAmountEditor = $grid.datagrid('getEditor', {index: i, field: 'totalActualAmount'});
			var totalRepairAmountCurrency = $(totalActualAmountEditor.target).val();*/
			/*var totalActualAmountInput = $('#gridRepairItemContainer td[field=totalActualAmount] input[id*=' + paymentRecordDetail.id + ']');
			var totalRepairAmountCurrency = '';
			try {
				//totalRepairAmountCurrency = $(totalActualAmountInput).val();
			} catch(e) {
				// pass through
			}
			
			if (isNullOrEmpty(totalRepairAmountCurrency)) {
				msg = 'Vui lòng nhập thanh toán cho hạng mục: ' + paymentRecordDetail.equipItem.name;
				errorFieldId = $(totalActualAmountInput).attr('id');
				showErrorMsg(msg, errorFieldId);
				return;
			}*/
			/** vuongmq; 20/04/2015; Gia trị totalAmount khong duoc nhap nua*/
			var totalRepairAmountCurrency = 0;
			if (paymentRecordDetails[i].totalAmount != null) {
				totalRepairAmountCurrency = paymentRecordDetails[i].totalAmount;
			}
			//var totalRepairAmount = totalRepairAmountCurrency.replace(/,/g, '');
			var totalRepairAmount = Utils.returnMoneyValue(totalRepairAmountCurrency);
			paymentRecordDetailsParam.push({
				itemId: paymentRecordDetail.id,
				totalRepairAmount: totalRepairAmount
			});
		}

		var dataModel = {
			paymentRecordCode: paymentRecordCode,
			paymentDateInDDMMYYYFormat: paymentDate,
			workerPrice: workerPrice ? Utils.returnMoneyValue(workerPrice): 0, //
			materialPrice: materialPrice ? Utils.returnMoneyValue(materialPrice): 0, //
			totalAmount: totalAmount ? Utils.returnMoneyValue(totalAmount): 0, //
			paymentRecordDetails: paymentRecordDetailsParam
		};

		var flatDataModel = {};
		convertToSimpleObject(flatDataModel, dataModel, 'equipmentRepairPaymentRecord');
		flatDataModel.equipRepairId = equipmentRepairRecordId;
		Utils.addOrSaveRowOnGrid(flatDataModel, '/equipment-repair-manage/update-payment-record', null, null, 'errMsg', function(data){
			if (data.error == false) {
				$("#successMsg").html("Lưu dữ liệu thành công").show();
				try {
					$('#btnUpdate').remove();
					try {
						$('#gridRepairItemContainer td[field=totalActualAmount] input').each(function() {
						    $(this).attr('disabled', 'disabled');
						});
					} catch(e) {
						// pass through
					}
					disabled('txtPaymentRecordCode');
					disableDateTimePicker('txtPaymentDate');
				} catch (e) {
					// pass through
				}
				setTimeout(function() {
					window.location.href = '/equipment-repair-manage/info';
				}, 3000);
			}
		});
	},
	/**
	 * chon kho
	 * 
	 * @return the string
	 * @author phuongvm
	 * @since 19/01/2015
	 */
	selectStock: function(shopCode){
		$('#stockCode').val(shopCode);		
		$('#easyuiPopupSearchStock').dialog('close');
	},
	/**
	 * Show popup chon kho
	 * @author phuongvm
	 * @since 19/01/2015
	 */
	showPopupStock:function(){
	$('#easyuiPopupSearchStock').show();
		$('#easyuiPopupSearchStock').dialog({  
	        closed: false,  
	        cache: false,  
	        modal: true,
	        width : 800,
	        height : 'auto',
	        onOpen: function(){	
				$('#shopCode').focus();
				$('#shopCode, #shopName,#customerCode, #customerName').bind('keyup',function(event){
					if(event.keyCode == keyCodes.ENTER){
						$('#btnSearchStockDlg').click(); 
					}
				});	
				$('#stockGridDialog').datagrid({
					url : '/equipment-repair-manage/searchStock',
					autoRowHeight : true,
					rownumbers : true, 
					fitColumns:true,
					scrollbarSize:0,
					pagination:true,
					pageSize: 10,
					pageList: [10],
					width: $('#stockGridDialogContainer').width(),
					height: 'auto',
					autoWidth: true,	
					queryParams:{
						shopCode: $('#shopCode').val().trim(),
						shopName: $('#shopName').val().trim()
					},
					columns:[[
						{field:'shopCode', title:'Đơn vị', align:'left', width: 140, sortable:false, resizable:false, formatter:function(value,row,index) {
			        	return Utils.XSSEncode(row.shopCode + " - " + row.shopName) ;        
			        }},
			        {field:'customerCode', title:'Khách hàng', align:'left', width: 140, sortable:false, resizable:false, formatter:function(value,row,index) {
			        	var html = '';
			        	if(row.customerCode != null){
			        		html = row.customerCode + ' - ' + row.customerName ;
			        	}   
			        	return Utils.XSSEncode(html);
			        }},
			        {field:'address', title:'Địa chỉ', align:'left', width: 150, sortable:false, resizable:false, formatter: function(value, row, index) {
				    	return Utils.XSSEncode(value);
				    }},
			        {field:'stockCode', title:'Mã kho', align:'left', width: 80, sortable:false, resizable:false, formatter: function(value, row, index) {
				    	return Utils.XSSEncode(value);
				    }},
			        {field:'choose', title:'', align:'center', width:40, sortable:false, resizable:false, formatter:function(value,row,index) {
			        	return "<a href='javascript:void(0)' onclick=\"return RepairEquipmentManageCatalog.selectStock('"+ Utils.XSSEncode(row.stockCode) +"');\">Chọn</a>";        
			        }}			
					]],
					onBeforeLoad:function(param){											 
					},
					onLoadSuccess :function(data){   			 
						if (data == null || data.total == 0) {
							//$("#errMsgInfo").html("Không tìm thấy danh sách biên bản giao nhận thiết bị!").show();
						}else{
							$('.datagrid-header-rownumber').html('STT');		    	
						}	    		
					}
				});				
	        },
			onClose: function(){
				$('#easyuiPopupSearchStock').hide();
				$('#shopCode').val('').change();
				$('#shopName').val('').change();
				$('#customerCode').val('').change();
				$('#customerName').val('').change();
				$('#stockGridDialog').datagrid('loadData',[]);
				$('#stockCode').focus();
			}
		});		
		return false;
	},
	/**
	 * Tim kiem kho
	 * @author phuongvm
	 * @since 19/01/2015
	 */
	searchStock: function(){
		var params=new Object(); 
		params.shopCode = $('#shopCode').val().trim();
		params.shopName = $('#shopName').val().trim();
		params.customerCode = $('#customerCode').val().trim();
		params.customerName = $('#customerName').val().trim();
		$('#stockGridDialog').datagrid('load',params);
	},
	/**
	 * xuat file excel BBSC
	 * @author nhutnn
	 * @since 16/01/2015
	 */
	exportBySearchListRepair: function(){
		$('.ErrorMsgStyle').html("").hide();
		var err = "";
		$('#errMsg').html('').hide();
		var msg="";
		var fDate = $('#fDate').val();
		var tDate =$('#tDate').val();
		if (msg.length == 0 && $('#fDate').val().trim().length > 0 && !Utils.isDate($('#fDate').val(),'/')) {
			msg = 'Ngày tạo từ không tồn tại hoặc không đúng định dạng dd/mm/yyyy.';
			err = '#fDate';
		}
		if (msg.length == 0 && $('#tDate').val().trim().length > 0 && !Utils.isDate($('#tDate').val(),'/')) {
			msg = 'Đến ngày không tồn tại hoặc không đúng định dạng dd/mm/yyyy.';
			err = '#tDate';
		}
		if (msg.length==0 && fDate.length > 0 && tDate.length > 0 && !Utils.compareDate(fDate, tDate)) {
			msg = 'Ngày tạo từ phải là ngày trước hoặc cùng ngày với đến ngày';
		}
		var data = $('#grid').datagrid('getRows');
		if (data != null && data.length <= 0 ) {
			msg = 'Không có dữ liệu xuất báo cáo';
		}
		if (msg.length > 0) {
			$('#errMsg').html(msg).show();
			$(err).focus();
			setTimeout(function(){$('#errMsg').hide();},3000);
			return false;
		}
		
		/** vuongmq; 20/04/2015; luc nay chi can lay paramsSearch la xong: RepairEquipmentManageCatalog._paramsSearch*/
		$.messager.confirm('Xác nhận','Bạn có muốn xuất file excel?',function(r) {
			if (r) {
				ReportUtils.exportReport('/equipment-repair-manage/export-repair-record', RepairEquipmentManageCatalog._paramsSearch, 'errMsg');					
			}
		});		
		return false;
	},
	/**
	 * tao gia tri cho autoComplete Combobox Nhom thiet bi
	 * @author tamnm
	 * @since 17/06/2015
	 */
	setEquipGroup: function(index) {
		var equipGroup = RepairEquipmentManageCatalog._lstEquipGroup;
		var obj = {
			id: -1,
			code: "Tất cả",
			name: "Tất cả",
			codeName: "Tất cả",
			searchText: unicodeToEnglish("Tất cả").toUpperCase(),
		};
		equipGroup.splice(0, 0, obj);
		for(var i = 0, sz = equipGroup.length; i < sz; i++) {
	 		equipGroup[i].searchText = unicodeToEnglish(equipGroup[i].code + " " + equipGroup[i].name);
	 		equipGroup[i].searchText = equipGroup[i].searchText.toUpperCase();
		}
	 	// equipGroup.splice(0, 0, { code: "", name: "", searchText:"" });
	 	$('#ddlEquipGroupId').combobox("loadData", equipGroup);
	 	$('#ddlEquipGroupId').combobox("setValue", -1);
	},
};
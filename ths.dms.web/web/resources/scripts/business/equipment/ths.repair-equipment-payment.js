var RepairEquipmentPayment = {
	_paramsSearch: null, /** param nay cho search va export excel theo grid*/
	_paramsSearchPopup: null, /** param nay cho search va export excel theo gridRepairPopup*/
	_mapRepair: null,
	_mapRepairPopup: null,

	// vuongmq; 22/06/2015; set param search quan ly thanh toan phieu sua chua
	setParamSearchPayForm: function() {
		var formCode = $('#txtRepairCode').val();
		var fromDate = $('#fDate').val();
		var status = $('#cbxStatus').val();
		/** put params vao; xu dung cho export */
		RepairEquipmentPayment._paramsSearch = new Object();
		RepairEquipmentPayment._paramsSearch.formCode = formCode;
		RepairEquipmentPayment._paramsSearch.fromDate = fromDate;
		RepairEquipmentPayment._paramsSearch.status = status;
	},

	/** vuongmq, 22/06/2015; cai nay search trong MH: Ql thanh toan sua chua  */
	searchRepairPayForm: function() {
		$('.SuccessMsgStyle').html('').hide();
		$('.ErrorMsgStyle').html('').hide();
		RepairEquipmentPayment._mapRepair = new Map();
		$('#equipHistoryTemplateDiv').css("visibility","hidden").hide();
		var err = "";
		$('#errMsg').html('').hide();
		var msg="";
		var fDate = $('#fDate').val();
		if (msg.length == 0 && fDate.trim().length > 0 && !Utils.isDate(fDate,'/') && fDate != '__/__/____') {
			msg = 'Ngày biên bản từ không tồn tại hoặc không đúng định dạng dd/mm/yyyy.';
			err = '#fDate';
		}
		if (msg.length > 0) {
			$('#errMsg').html(msg).show();
			$(err).focus();
			setTimeout(function(){$('#errMsg').hide();},3000);
			return false;
		}
		RepairEquipmentPayment.setParamSearchPayForm();
		$('#grid').datagrid('load',{
			page:1,
			//RepairEquipmentPayment._paramsSearch
			formCode: RepairEquipmentPayment._paramsSearch.formCode,
			fromDate: RepairEquipmentPayment._paramsSearch.fromDate,
			status: RepairEquipmentPayment._paramsSearch.status,
			});
		
	},
	/** vuongmq, 22/06/2015; MH chi thiet thanh toan sua chua  */
	viewDetail:function(id, code){
		$('.SuccessMsgStyle').html('').hide();
		$('.ErrorMsgStyle').html('').hide();
		if (code != undefined && code != null && code.trim().length > 0) {
			$('#equipmentCodeFromSpan').html(code);
		} else {
			$('#equipmentCodeFromSpan').html("");
		}
		$('#equipHistoryTemplateDiv').css("visibility","visible").show();
		$('#equipHistoryDg').datagrid({
			url : "/equipment-repair-payment/searchRepairPayDetail",
			pagination : true,
	        rownumbers : true,
	        pageNumber : 1,
	        scrollbarSize: 0,
	        autoWidth: true,
	        pageList: [10],
	        autoRowHeight : true,
	        fitColumns : true,
			queryParams: {id: id},
			width: $('#equipHistoryDgContainerGrid').width() - 15,
		    columns:[[
				/*{field: 'check',checkbox:true, width: 40, sortable:false,resizable:false, align: 'left'},*/
				{field:'maPhieu', title: 'Mã phiếu', width: 120, sortable: false, resizable: false , align: 'left', formatter:function(value,row,index){
			    	return Utils.XSSEncode(value);
			    }},
			    {field:'ngayLap', title: 'Ngày lập', width: 80, sortable: false, resizable: false , align: 'center', formatter:function(value,row,index){
			    	return Utils.XSSEncode(value);
			    }},	
				{field:'tongTien', title: 'Tổng tiền', width: 120, sortable:false, resizable: false, align: 'right', formatter:function(value,row,index){
					if (value != undefined && value != null) {
			    		return formatCurrency(value);
			    	}	
			    	return '';	
			    }},
		    ]],
		    onLoadSuccess :function(data){	    	
		    	$('.datagrid-header-rownumber').html('STT');
		    	//Utils.functionAccessFillControl('equipHistoryDg');
	   		 	$(window).resize();    		 
		    }
		});
	},

	/**
	 * Them danh sach các phieu sua chua
	 * @author vuongmq
	 * @since 22/06/2015
	 * */
	viewPopupRepair: function() {
	 	$('#easyuiPopupSearchRepair').show();
		$('#easyuiPopupSearchRepair').dialog({  
	        closed: false,  
	        cache: false,  
	        modal: true,
	        width : 1000,
//	        height : 'auto',
	        onOpen: function(){
	        	$('#shop, #txtRepairCodePopup, #txtCode, #fromDate, #toDate, #customerCode, #customerName, #txtSerial,').bind('keyup',function(event){
	        		if(event.keyCode == keyCodes.ENTER){
	        			$('#btnSearchRepairPopup').click(); 
	        		}
	        	});
	        	$('#shop').focus();
	        	$("#fromDate").val(getLastWeek());//Lay truoc 7 ngay
				ReportUtils.setCurrentDateForCalendar("toDate");
	        	RepairEquipmentPayment.viewPopupShop();
	        	RepairEquipmentPayment.viewGridPopupRepair();
	        },
			onClose: function(){
				$('#easyuiPopupSearchRepair').hide();
				$('#shop').val('').change();
				$('#txtCode').val('').change();
				$('#txtRepairCodePopup').val('').change();
				$('#fromDate').val('').change();
				$('#toDate').val('').change();
				$('#customerCode').val('').change();
				$('#customerName').val('').change();
				$('#txtSerial').val('').change();
				$('#gridRepairPopup').datagrid("loadData",[]);

				$('.SuccessMsgStyle').html('').hide();
				$('.ErrorMsgStyle').html('').hide();
			}
		});
	},

	/* vuongmq; 23/06/2015; show popup NPP phan quyen CMS*/
	viewPopupShop: function() {
		$('#shop').bind('keyup', function(event) {
			$('.SuccessMsgStyle').html('').hide();
			$('.ErrorMsgStyle').html('').hide();
			if (event.keyCode == keyCode_F9) {
				VCommonJS.showDialogSearch2({
					dialogInfo: {
						title: 'Tìm kiếm đơn vị'
					},
					/*params : {
						shopCode : $('#txtShopCode').val().trim(),
						status : activeType.RUNNING
					},*/
					inputs : [
					{id:'code', maxlength:50, label:'Mã đơn vị'},
					{id:'name', maxlength:250, label:'Tên đơn vị'},
					      ],
					url : '/commons/search-shop-show-list-NPP',
					columns : [[
						{field:'shopCode', title:'Mã đơn vị', align:'left', width: 120, sortable:false, resizable:false, formatter: function(value, row, index) {
					    	return Utils.XSSEncode(value);
					    }},
						{field:'shopName', title:'Tên đơn vị', align:'left', width: 180, sortable:false, resizable:false, formatter: function(value, row, index) {
					    	return Utils.XSSEncode(value);
					    }},
						{field:'choose', title:'', align:'center', width:40, sortable:false, resizable:false, formatter:function(value,row,index) {
							return '<a href="javascript:void(0)" onclick="RepairEquipmentPayment.fillTextBoxByShopCode(\''+ Utils.XSSEncode(row.shopCode) +'\',\''+row.shopId+'\')">chọn</a>';         
						}}
					]]
				});
			}
		});
	},

	 /**
	 * Dien gia tri Ma Don vi vao shop
	 * @author vuongmq
	 * @since 23/06/2015
	 * */
	 fillTextBoxByShopCode: function (shopCode, shopId) {
		$('#shop').val(shopCode);
		$('#shop').change();
		$('#shop').focus();
		$('#common-dialog-search-2-textbox').dialog("close");
		$('#common-dialog-search-2-textbox').remove();
	 },

	 /** vuongmq; 23/06/2015; set Param search Grid popup danh sach phieu sua chua*/
	 setParamSearchPopupRepair: function() {
	 	/* add them dieu kien tru cac phieu sua chua duoi danh sach gridRepairItem */
		var data = $('#gridRepairItem').datagrid('getRows');
		var lstRepairId = [];
		if (data != undefined && data != null && data.length > 0) {
			for (var i = 0; i < data.length; i++) {
				if (data[i].id != null && data[i].id != '') {
					lstRepairId.push(data[i].id);
				}
			}
		}
		// lay them dieu kien la danh sach phieu sua chua not in lstId; datagrid khong truyen len duoc mang[]
	 	RepairEquipmentPayment._paramsSearchPopup = {
			shopCode: $('#shop').val(), // ma NPP
			equipCode: $('#txtCode').val(),
			formCode: $('#txtRepairCodePopup').val(),
			fromDate: $('#fromDate').val(),
			toDate: $('#toDate').val(),
			customerCode: $('#customerCode').val(),
			customerName: $('#customerName').val(),
			seri: $('#txtSerial').val(),
			status: $('#statusPopup').val(),
			statusPayment: $('#statusPayment').val(),
			lstIdStr: lstRepairId.join(','),
		};
	 },

	/** vuongmq; 23/06/2015; search Grid popup danh sach phieu sua chua*/
	viewGridPopupRepair: function() {
		RepairEquipmentPayment.setParamSearchPopupRepair();
		RepairEquipmentPayment._mapRepairPopup = new Map();
		$('#gridRepairPopup').datagrid({
			url : "/equipment-repair-payment/searchRepairPopup",
			pagination : true,
	        rownumbers : true,
	        pageNumber : 1,
	        scrollbarSize: 0,
	        autoWidth: true,
	        pageList: [10, 20, 50],
	        autoRowHeight : true,
	        //fitColumns : true,
			queryParams: RepairEquipmentPayment._paramsSearchPopup,
			width: $('#gridContainer').width() - 15,
			checkOnSelect: false, // vuongmq; 19/05/2015; chi cho check checkox

			frozenColumns:[[        
				{field: 'check',checkbox:true, width: 50, sortable:false,resizable:false, align: 'left'},
				{field:'donVi', title: 'Đơn vị', width: 180, align: 'left',sortable: false, resizable: false, formatter:function(value,row,index){
			    	return Utils.XSSEncode(value);
			    }},
			    {field:'maPhieu', title: 'Mã phiếu', width: 120, sortable: false, resizable: false , align: 'left', formatter:function(value,row,index){
			    	return Utils.XSSEncode(value);
			    }},
			    {field:'ngayTao', title: 'Ngày tạo', width: 80, sortable: false, resizable: false , align: 'centre', formatter:function(value,row,index){
			    	return Utils.XSSEncode(value);
			    }},
			]],
			columns:[[
			    {field:'loaiThietBi', title: 'Loại thiết bị', width: 160, sortable: false, resizable: false , align: 'left', formatter:function(value,row,index){
			    	return Utils.XSSEncode(value);
			    }},
			    {field:'nhomThietBi', title: 'Nhóm thiết bị', width: 180, sortable: false, resizable: false , align: 'left', formatter:function(value,row,index){
			    	return Utils.XSSEncode(value);
			    }},
			    {field:'maThietBi', title: 'Mã thiết bị', width: 180, align: 'left', sortable: false, resizable: false, formatter:function(value,row,index){
			    	return Utils.XSSEncode(value);
			    }},
			    {field:'soSeri', title: 'Số serial', width: 120, align: 'left', sortable: false, resizable: false, formatter:function(value,row,index){
			    	return Utils.XSSEncode(value);
			    }},
			    {field:'namSanXuat', title: 'Năm sản xuất', width: 80, sortable:false, resizable: false, align: 'right', formatter:function(value,row,index){
					if (value != undefined && value != null) {
			    		return value;
			    	}	
			    	return '';	
			    }},
			    {field:'khachHang', title: 'Khách hàng', width: 160, sortable:false, resizable: false, align: 'left', formatter:function(value,row,index){
			    	return Utils.XSSEncode(value);	
			    }},
			    {field:'diaChi', title: 'Địa chỉ', width: 200, sortable:false, resizable: false, align: 'left', formatter:function(value,row,index){
			    	return Utils.XSSEncode(value);	
			    }},
			    /*{field:'trangThai',title: 'Trạng thái', width: 50, sortable: false, resizable: false, align: 'left', formatter:function(value,row,index){
			    	var status = "";
			    	if(value == 0){
			    		status = "Dự thảo";
			    	}else if(value == 1){
			    		status = "Chờ duyệt";
			    	}else if(value == 2){
			    		status = "Đã Duyệt";
			    	}else if(value == 3){
			    		status = "Không duyệt";
			    	}else if(value == 4){
			    		status = "Hủy";
			    	}else if(value == 5){
			    		status = "Sửa chữa";
			    	}else if(value == 6){
			    		status = "Hoàn tất";
			    	}
			    	return Utils.XSSEncode(status);
			    }},*/
				/*{field:'tinhTrangHuHong', title: 'Tình trạng hư hỏng', width: 150, sortable: false, resizable: false, align: 'left', formatter:function(value,row,index){
					return Utils.XSSEncode(value);
			    }},	
				{field:'lyDoDeNghi', title: 'Lý do/Đề nghị', width: 150, sortable: false, resizable: false, align: 'left', formatter:function(value,row,index){
			    	return Utils.XSSEncode(value);
			    }},*/	
				{field:'tongTien', title: 'Tổng tiền', width: 120, sortable:false, resizable: false, align: 'right', formatter:function(value,row,index){
					if (value != undefined && value != null) {
			    		return formatCurrency(value);
			    	}	
			    	return '';	
			    }},
		    ]],
		    onLoadSuccess :function(data){	    	
		    	$('.datagrid-header-rownumber').html('STT');
		    	$('.datagrid-header-check input').removeAttr('checked');
	        	var rows = $('#gridRepairPopup').datagrid('getRows');
		    	for(var i = 0; i < rows.length; i++) {
		    		if(RepairEquipmentPayment._mapRepairPopup.get(rows[i].id)!=null) {
		    			$('#gridRepairPopup').datagrid('checkRow', i);
		    		}
		    	}
	   		 	$(window).resize();    		 
		    },
		    onCheck:function(index,row){
				RepairEquipmentPayment._mapRepairPopup.put(row.id,row);   
		    },
		    onUncheck:function(index,row){
		    	RepairEquipmentPayment._mapRepairPopup.remove(row.id);
		    },
		    onCheckAll:function(rows){
		    	for(var i = 0;i<rows.length;i++){
		    		var row = rows[i];
		    		RepairEquipmentPayment._mapRepairPopup.put(row.id,row);
		    	}
		    },
		    onUncheckAll:function(rows){
		    	for(var i = 0;i<rows.length;i++){
		    		var row = rows[i];
		    		RepairEquipmentPayment._mapRepairPopup.remove(row.id);		    		
		    	}
		    }
		});
	},

	/**
	 * Tim kiem danh sach phieu sua chua tren popup
	 * @author vuongmq
	 * @since 23/06/2014
	 */
	searchPopupRepair: function() {
		$('.SuccessMsgStyle').html('').hide();
		$('.ErrorMsgStyle').html('').hide();
		var msg = '';
		if (msg.length == 0) {
			msg = Utils.getMessageOfInvalidFormatDate('fromDate','Từ ngày');
		}
		if (msg.length == 0) {
			msg = Utils.getMessageOfInvalidFormatDate('toDate','Đến ngày');
		}
		var fDate = $('#fromDate').val();
		var tDate = $('#toDate').val();
		var curDate = ReportUtils.getCurrentDateString();
		if(msg.length == 0 && !Utils.compareDate(tDate, curDate)){
			msg = "Đến ngày không được lớn hơn ngày hiện tại";
			$('#toDate').focus();
		}
		if (msg.length == 0) {
			if (!Utils.compareDate(fDate, tDate)) {
				msg = msgErr_fromdate_greater_todate;		
				$('#fromDate').focus();
			}
		}
		if (msg.length > 0) {
			$('#errMsgPopup').html(msg).show();
			return false;
		}
		RepairEquipmentPayment.setParamSearchPopupRepair();
		$('#gridRepairPopup').datagrid('load',RepairEquipmentPayment._paramsSearchPopup);
	},

	/**
	 * Chon danh sach phieu sua chua tren popup; insert vao grid gridRepairItem cua phieu thanh toan
	 * @author vuongmq
	 * @since 23/06/2014
	 */
	chooseRepairPopup: function() {
		if (RepairEquipmentPayment._mapRepairPopup != undefined && RepairEquipmentPayment._mapRepairPopup != null && RepairEquipmentPayment._mapRepairPopup.keyArray != null && RepairEquipmentPayment._mapRepairPopup.keyArray.length > 0) {
        	//var rows = new Array();
        	var lengthMap = RepairEquipmentPayment._mapRepairPopup.keyArray.length;
        	for (var i = 0; i < lengthMap ; i++) {
        		var key = RepairEquipmentPayment._mapRepairPopup.keyArray[i];
        		var dataRow =  RepairEquipmentPayment._mapRepairPopup.get(key);
        		var row = {
        			id:	dataRow.id,
        			maPhieu: dataRow.maPhieu,
        			ngayLap: dataRow.ngayTao, // ngay tao tren popup phieu sua chua, dua xuong la ngay lap cua phieu sua chua
        			tongTien: dataRow.tongTien,
        		};
        		var idx1 = 0;
        		var rowsss = $('#gridRepairItem').datagrid("getRows");
        		if (rowsss.length > 0) {
	        		idx1 = rowsss.length - 1;
	        	}
        		$('#gridRepairItem').datagrid('insertRow',{row:row,index:idx1});
        		//RepairEquipmentPayment._lstEquipItemId.push(row.id);
        	}
        	$('#easyuiPopupSearchRepair').dialog('close');
        } else {
        	$('#errMsgPopup').html("Bạn chưa chọn thiết bị.").show();
        }
	},

	/**
	 * Xoa mot dong cua phieu sua chua trong phieu thanh toan
	 * @author vuongmq
	 * @since 22/06/2015
	 * */
	deleteRow: function (index) {
		var row = $('#gridRepairItem').datagrid('getRows')[index];
		if (row != null && row.id != undefined && row.id != null) {
			$('#gridRepairItem').datagrid("deleteRow", index);
			/*for (var i = 0; i < RepairEquipmentPayment._lstEquipItemId.length; i++) {
				if (RepairEquipmentPayment._lstEquipItemId[i] == row.id) {
					RepairEquipmentPayment._lstEquipItemId.splice(i,1);
					break;
				}
			}
			RepairEquipmentPayment.reloadGridDetail();*/
			/** vuongmq; 30/06/2015 ;cap nhat lai danh dach grid theo index moi */
			var data = $('#gridRepairItem').datagrid('getRows');
			$('#gridRepairItem').datagrid('loadData', data);
		}
	},

	/**
	 * khoi tao grid danh sach cac hang muc sua chua; cua phieu thanh toan
	 * @author vuongmq
	 * @date 23/06/2015
	 */
	initializeRepairItemsGrid: function(idPayment, status, statusPayment) {
		$('#gridRepairItem').datagrid({
			url : "/equipment-repair-payment/searchRepairPayDetail",
			//pagination : true,
			rownumbers : true,
			pageNumber : 1,
			scrollbarSize: 0,
			autoWidth: true,
			//pageList: [10],
			autoRowHeight : true,
			fitColumns : true,
			singleSelect: true,
			queryParams: {id: idPayment},
			width: $('#gridRepairItemContainer').width() - 15,
			columns:[[
				/*{field: 'check',checkbox:true, width: 40, sortable:false,resizable:false, align: 'left'},*/
				{field:'maPhieu', title: 'Mã phiếu', width: 120, sortable: false, resizable: false , align: 'left', formatter:function(value,row,index){
			    	return Utils.XSSEncode(value);
			    }},
			    {field:'ngayLap', title: 'Ngày lập', width: 80, sortable: false, resizable: false , align: 'center', formatter:function(value,row,index){
			    	return Utils.XSSEncode(value);
			    }},	
				{field:'tongTien', title: 'Tổng tiền', width: 120, sortable:false, resizable: false, align: 'right', formatter:function(value,row,index){
					if (value != undefined && value != null) {
			    		return formatCurrency(value);
			    	}	
			    	return '';	
			    }},
			    {field:'edit', title:'<a onclick="RepairEquipmentPayment.viewPopupRepair()" title="Thêm phiếu sửa chữa"><img src="/resources/images/icon_add.png"/></a>', width: 70, align: 'center', sortable: false, resizable: true, formatter: function(value,row,index){
			    	return '<a onclick="RepairEquipmentPayment.deleteRow('+index+',\''+row.id+'\',\''+ Utils.XSSEncode(row.maPhieu) +'\')" href="javascript:void(0)"><img width="15" height="15" src="/resources/images/icon_delete.png" title="Xóa phiếu sửa chữa '+row.maPhieu+'"></a>';
				}},
				{field: 'id', sortable:false, hidden: true},
		    ]],
			onLoadSuccess:function(data){
				$('.datagrid-header-rownumber').html('STT');
				//Utils.functionAccessFillControl('equipHistoryDg');
				/*if (viewActionType === ViewActionType.CREATE || viewActionType === ViewActionType.EDIT) {
					if (data && data.rows && data.rows.length) {
					
						RepairEquipmentPayment.loadInfoPayment(data.rows);
					}
				}*/
				if (statusPayment == '' || statusPayment != StatusRecordsEquip.APPROVED) {
					// quyen Ql tao moi, chinh sua
					if (status == StatusRecordsEquip.WAITING_APPROVAL || status == StatusRecordsEquip.APPROVED || status == StatusRecordsEquip.CANCELLATION) {
						$("#gridRepairItem").datagrid('hideColumn', 'edit');
					}
				} else {
					// quyen duyet
					$("#gridRepairItem").datagrid('hideColumn', 'edit');
				}
	   		 	$(window).resize();			 
			}
		});
	},

	 /**
	 * cap nhat phieu thanh toan; them tao moi phieu thanh toan
	 * @author vuongmq
	 * @param  long idPayment id cua phieu sua chua
	 * @return void
	 */
	updateEquipmentRepairPaymentRecord: function(idPayment) {
		$('.SuccessMsgStyle').html('').hide();
		$('.ErrorMsgStyle').html('').hide();
		var msg = '';
		if (msg.length == 0) {
			msg = Utils.getMessageOfRequireCheck('fDate','Ngày thanh toán');
		}
		if (msg.length == 0) {
			msg = Utils.getMessageOfInvalidFormatDate('fDate','Ngày thanh toán');
		}
		var fDate = $('#fDate').val();
		var curDate = ReportUtils.getCurrentDateString();
		if(msg.length == 0 && !Utils.compareDate(fDate, curDate)){
			msg = "Ngày thanh toán không được lớn hơn ngày hiện tại";
			$('#fDate').focus();
		}
		var row = $('#gridRepairItem').datagrid('getRows');
		if (msg.length == 0) {
			if (row == null || row.length == 0) {
				msg = 'Vui lòng chọn phiếu sửa chữa để thanh toán';
			}
		}
		if (msg.length > 0) {
			$('#errMsgDetail').html(msg).show();
			return false;
		}
		var row = $('#gridRepairItem').datagrid('getRows');
		var lstRepairId = [];
		for (var i = 0; i < row.length; i++) {
			if (row[i] != null && row[i].id != null) {
				lstRepairId.push(row[i].id);
			}
		}
		var id = '';
		if (idPayment != undefined && idPayment != null) {
			id = idPayment;
		}
		var status = $('#cbxStatus').val();
		var dataModel = {};
		dataModel.id = id; // truong hop cap nhat phiwu thanh toan thi co Id
		dataModel.paymentDate = fDate;
		dataModel.status = status;
		dataModel.lstId = lstRepairId;
		var titleErr = {title:'Lỗi các phiếu sửa chữa ',pageList:10};
		Utils.addOrSaveData(dataModel, '/equipment-repair-payment/update-payment-record', null, 'errMsgDetail', function(data) {
			if (data.error == false) {
				if (data.maPhieu != undefined && data.maPhieu != '') {
					$('#txtRepairCode').val(data.maPhieu);
					// cap nhat lai Id cho btnUpdate neu muon chuyen, cap nhat lai phieu
					$('#btnUpdate').attr('onclick','RepairEquipmentPayment.updateEquipmentRepairPaymentRecord('+data.idPhieu+');')
				}
				if (data.status != undefined && data.status != '' && data.status == StatusRecordsEquip.WAITING_APPROVAL) {
					// quyen Ql tao moi, chinh sua
					var htmlCombo = '<option value="1">Chờ duyệt</option> <option value="4">Hủy</option>';
					$('#cbxStatus').html(htmlCombo).change();
					$('#cbxStatus').val(status).change();
					
					RepairEquipmentPayment.initializeRepairItemsGrid(idPayment, data.status);
					disableDateTimePicker('fDate');
				}
				if (data.lstErr != undefined && data.lstErr != null && data.lstErr.length > 0 ) {
					VCommonJS.showDialogList({
						data : data.lstErr,
						dialogInfo: titleErr,
						columns : [[
							{field:'formCode', title:'Mã phiếu sửa chữa', align:'left', width: 60, sortable:false, resizable:false, formatter: function(value, row, index) {
						    	return Utils.XSSEncode(value);
						    }},
							{field:'formStatusErr', title:'Trạng thái', align:'left', width: 180, sortable:false, resizable:false, formatter: function(value,row,index){
								var html="";
								if(value == 1){
									html = '<img src="/resources/images/icon_delete.png" height="20" width="20" style="margin-right:5px;" title="Error"> ' + row.recordNotComplete;
								}
								return html;
							} }
						]]
					});	
				} else {
					var status = $('#cbxStatus').val();
					if (status != undefined && status != '' && (status == StatusRecordsEquip.APPROVAL || status == StatusRecordsEquip.NO_APPROVAL)) {
						disableSelectbox('cbxStatus');
						$('#btnUpdate').remove();
					}
					$('#successMsgDetail').html('Lưu dữ liệu thành công').show();
					setTimeout(function(){$('#successMsgDetail').html('').hide();}, 1500);
				}
			}
		}, null, null, null, null, null, true);
	},
	
	/** vuongmq, QLthanh toan sua chua-> cap nhat trang thai cua phieu thanh toan sua chua;*/
	updateStatus: function() {
		$('.SuccessMsgStyle').html('').hide();
		$('.ErrorMsgStyle').html('').hide();	
		var statusForm = $('#cbxStatusUpdate').val().trim();
		if (statusForm < 0) {
			$('#errMsg').html('Vui lòng chọn trạng thái').show();
			return;
		}
		var arra = RepairEquipmentPayment._mapRepair.valArray;		
		var lstId = new Array();
		if (arra == null || arra.length == 0) {
			$('#errMsg').html('Bạn chưa chọn phiếu thanh toán sửa chữa để cập nhật. Vui lòng chọn phiếu.').show();
			return;
		}		
		for (var i=0, size = arra.length; i < size; ++i) {
			lstId.push(arra[i].id);			
		}
		var params = new Object();
		params.lstId = lstId;
		params.statusForm = statusForm;
		/*if ($("#rejectReason").val()!=null && $("#rejectReason").val()!='') {
			params.rejectReason = $("#rejectReason").val();
		}*/
		var title = {title:'Lỗi lưu trạng thái phiếu thanh toán',pageList:10};
		Utils.addOrSaveData(params, '/equipment-repair-payment/updateStatus', null, 'errMsg', function(data) {
			if ( data.lstErr != undefined && data.lstErr != null && data.lstErr.length > 0 ) {
				VCommonJS.showDialogList({
				    data : data.lstErr,
				    dialogInfo: title,
				    columns : [[
				        {field:'formCode', title:'Mã biên bản', align:'left', width: 120, sortable:false, resizable:false, formatter: function(value, row, index) {
					    	return Utils.XSSEncode(value);
					    }},
				        {field:'formStatusErr', title:'Trạng thái biên bản', align:'left', width: 180, sortable:false, resizable:false, formatter: function(value,row,index){
				        	var html="";
							if(value == 1){
								html = '<img src="/resources/images/icon_delete.png" height="20" width="20" style="margin-right:5px;" title="Error"> ' + row.recordNotComplete;
							}
							return html;
						} }
					]]
				});	
			} else {
				$('#successMsg').html('Lưu dữ liệu thành công').show();
				setTimeout(function(){$('#successMsg').html('').hide();},1500);
			}
			RepairEquipmentPayment._mapRepair = new Map();
			$('#grid').datagrid('reload');
			$('#equipHistoryTemplateDiv').css("visibility", "hidden").hide();
		}, null, true,null, 'Bạn có muốn cập nhật phiếu thanh toán?', null, true);
		
	},

	/**
	 * xuat file excel Phieu thanh toan sua chua
	 * @author nhutnn
	 * @since 16/01/2015
	 */
	exportListRepairPayForm: function() {
		$('.ErrorMsgStyle').html("").hide();
		var msg = '';
		/*var data = $('#grid').datagrid('getRows');
		if (data != null && data.length <= 0 ) {
			msg = 'Không có dữ liệu xuất báo cáo';
		}*/
		var arra = RepairEquipmentPayment._mapRepair.valArray;		
		var lstId = new Array();
		if (msg.length == 0) {
			if (arra == null || arra.length == 0) {
				msg = 'Bạn chưa chọn phiếu thanh toán sửa chữa để xuất file excel. Vui lòng chọn phiếu.';
			}
		}
		if (msg.length > 0) {
			$('#errMsg').html(msg).show();
			setTimeout(function(){$('#errMsg').hide();},3000);
			return false;
		}
		for (var i=0, size = arra.length; i < size; ++i) {
			lstId.push(arra[i].id);			
		}
		var params = new Object();
		params.lstId = lstId;
		$.messager.confirm('Xác nhận','Bạn có muốn xuất file excel?',function(r) {
			if (r) {
				ReportUtils.exportReport('/equipment-repair-payment/export-repair-payform', params, 'errMsg');					
			}
		});		
		return false;
	},
	///////////////////////////////////////////// END VUONGMQ ///////////////////////////////////////
};
/**
 * JS thuc hien nghiep vu Danh Sach Thiet Bi
 * 
 * @author hunglm16
 * @since December 17,2014
 * */
var EquipmentListEquipment = {
		/**
		 * Khai bao thuoc tinh Bien Toan Cuc
		 * 
		 * @author hunglm16
		 * @since December 17,214
		 * */
		_listEquipmentDg: null,
		_mapData: null,
		_params: null,
		_equipChange: null,
		_countFile:0,
		_arrFileDelete:null,
		_lstEquipGroup: null,
		_lstEquipProvider: null,
		_curShopId: null,
		/**
		 * Tim kiem danh sach thiet bi
		 * 
		 * @author hunglm16
		 * @since December 17,214
		 * */
		searchListEquipment: function () {
			$('.SuccessMsgStyle').html('').hide();
			$('.ErrorMsgStyle').html('').hide();
			$('#equipHistoryTemplateDiv').css("visibility","hidden").hide();
			var msg = '';
			var fromDate = $('#fDate').val().trim();
			var toDate = $('#tDate').val().trim();
			if(msg.length == 0){
				msg = Utils.getMessageOfInvalidFormatDate('fDate', 'Ngày cấp từ');
			}
			if(msg.length ==0){
				msg = Utils.getMessageOfInvalidFormatDate('tDate', 'Ngày cấp đến');
			}
			if(msg.length == 0 && !Utils.compareDate(fromDate, toDate)){
				msg = "Ngày cấp từ phải là ngày trước hoặc cùng ngày với ngày cấp đến.";
				$('#fromDate').focus();
			}
			if(msg.length > 0){
				$('#errMsg').html(msg).show();
				return false;
			}
			//tamvnm: thay doi thanh autoCompleteCombobox: -2 la truong hop tim group ko co trong BD
			var providerId = -1;
			var groupId = -1;
			if ($('#ddlEquipProviderId').combobox("getValue") != "") {
				if ($('#ddlEquipProviderId').combobox("getValue") != -1) {
					if (Utils.getMessageOfInvaildNumberNew($('#ddlEquipProviderId').combobox("getValue")).length == 0) {
						providerId = $('#ddlEquipProviderId').combobox("getValue");
					} else {
						providerId = -2;
					}
				}
			} else {
				$('#ddlEquipProviderId').combobox("setValue", -1);
			}

			if ($('#ddlEquipGroupId').combobox("getValue") != "") {
				if ($('#ddlEquipGroupId').combobox("getValue") != -1) {
					if (Utils.getMessageOfInvaildNumberNew($('#ddlEquipGroupId').combobox("getValue")).length == 0) {
						groupId = $('#ddlEquipGroupId').combobox("getValue");
					} else {
						groupId = -2;
					}
				}
			} else {
				$('#ddlEquipGroupId').combobox("setValue", -1);
			}

			var multiShop = $("#shop").data("kendoMultiSelect");
			var dataShop = multiShop.dataItems();
			var arrShopId = [];
			if (dataShop != undefined && dataShop != null) {
				for (var i = 0, size = dataShop.length; i < size; i++) {
					arrShopId.push(dataShop[i].shopId);
				}
			}
			EquipmentListEquipment._params = {
					equipCategoryId: $('#ddlEquipCategory').val(),
					// equipGroupId: $('#ddlEquipGroupId').val(),
					equipGroupId: groupId,
					tradeType: $('#ddlTradeType').val(),
					tradeStatus: $('#ddlTradeStatus').val(),
					usageStatus: $('#ddlUsageStatus').val(), 
					// equipProviderId: $('#ddlEquipProviderId').val(), 
					equipProviderId: providerId, 
					code: $('#txtCode').val(),
					seriNumber: $('#txtSerial').val(),
//					equipImportRecordCode: $('#txtEquipImportRecordCode').val(),
					stockCode: $('#txtStockCode').val().trim(),
					stockName: $('#txtStockName').val().trim(),
					customerCode: $('#txtCustomerCode').val().trim(),
					customerName: $('#txtCustomerName').val().trim(),
					customerAddress: $('#txtCustomerAddress').val().trim(),
					fromDateStr: fromDate, 
					toDateStr: toDate,
					arrShop: arrShopId.toString()
					// arrShop: $('#shop').combobox('getValue')
			};
			$('#listEquipmentDg').datagrid("load", EquipmentListEquipment._params);
		},
		/**
		 * Xuat Excel Theo Tim kiem danh sach thiet bi
		 * 
		 * @author hunglm16
		 * @since December 18,214
		 * */
		exportBySearchListEquipment: function() {
			$('.SuccessMsgStyle').html('').hide();
			$('.ErrorMsgStyle').html('').hide();
			$('#equipHistoryTemplateDiv').css("visibility","hidden").hide();
			var msg = '';
			if(EquipmentListEquipment._params != null){
				var rows = $('#listEquipmentDg').datagrid("getRows");
				if(rows == null || rows.length==0){
					msg = "Không có dữ liệu xuất Excel";
				}
			}else{
				msg = "Không có dữ liệu xuất Excel";
			}
			if(msg.length > 0){
				$('#errMsg').html(msg).show();
				return false;
			}
			var msg = 'Bạn có muốn xuất Excel?';
			$.messager.confirm('Xác nhận', msg, function(r){
				if (r){
					ReportUtils.exportReport('/equipment-list-manage/exportBySearchEquipmentVO', EquipmentListEquipment._params, 'errMsg');
				}
			});
			
		},
		
		/**
		 * Tai tap tin Excel Import Danh sach thiet bi
		 * 
		 * @author hunglm16
		 * @since December 22,2014
		 * @return Excel
		 * */
		downloadTemplateImportListEquip: function() {
			$('.SuccessMsgStyle').html('').hide();
			$('.ErrorMsgStyle').html('').hide();
			var msg = 'Bạn có muốn tải tập tin Import?';
			ReportUtils.exportReport('/equipment-list-manage/downloadTemplateImportListEquip', {}, 'errMsg');
		},
		
		/**
		 * Mo Dialog Nhap Excel danh sach thiet bi
		 * 
		 * @author hunglm16
		 * @since December 18,214
		 * */
		// openDialogImportListEquipment: function (){
		// 	$('.SuccessMsgStyle').html('').hide();
		// 	$('.ErrorMsgStyle').html('').hide();
		// 	$('#equipHistoryTemplateDiv').css("visibility","hidden").hide();
		// 	$('#easyuiPopupImportExcel').dialog({
		// 		title: 'Nhập Excel Danh sách thiết bị',
		// 		width: 465, 
		// 		height: 'auto',
		// 		closed: false,
		// 		cache: false,
		// 		modal: true,
		// 		onOpen: function() {
					
		// 		},
		// 		onClose: function() {
		// 			$('.ErrorMsgStyle').html('').hide();
		// 		}
		// 	});
		// },
		/**
		 * Import Excel danh sach thiet bi
		 * 
		 * @author hunglm16
		 * @since December 18,214
		 * */
		importListEquipment : function(){
			$('.SuccessMsgStyle').html('').hide();
			$('.ErrorMsgStyle').html('').hide();
			
			Utils.importExcelUtils(function(data){
				EquipmentListEquipment.searchListEquipment();
				$('#fakefilepcListEquip').val("").change();
				if (data.message!=undefined && data.message!=null && data.message.trim().length >0){
					$('#errExcelMsgListEquip').html(data.message.trim()).change().show();
				}else{
					$('#successExcelMsgListEquip').html("Lưu dữ liệu thành công").show();
					var tm = setTimeout(function(){
						$('.SuccessMsgStyle').html('').hide();
						clearTimeout(tm);
						$('#easyuiPopupImportExcel').dialog('close');
					 }, 1500);
				}
			}, 'importFrmListEquip', 'excelFileListEquip', 'errExcelMsgListEquip');
			
		},
		
		/**
		 * Import Excel danh sach thiet bi
		 * 
		 * @author hunglm16
		 * @since December 18,214
		 * */
		viewEquipmentHistory : function(id, code){
			$('.SuccessMsgStyle').html('').hide();
			$('.ErrorMsgStyle').html('').hide();
			if (code != undefined && code != null && code.trim().length > 0) {
				$('#equipmentCodeFromSpan').html(code);
			} else {
				$('#equipmentCodeFromSpan').html("");
			}
			$('#equipHistoryTemplateDiv').css("visibility","visible").show();
			$('#equipHistoryDg').datagrid({
				url : "/equipment-list-manage/getEquipRFVOByListEquipment",
				pagination : true,
		        rownumbers : true,
		        pageNumber : 1,
		        scrollbarSize: 0,
		        autoWidth: true,
		        pageList: [20],
		        autoRowHeight : true,
		        fitColumns : true,
				queryParams: {id: id},
				width: $('#equipHistoryDgContainerGrid').width() - 15,
			    columns:[[	        
				    {field:'equipItemName', title: 'Hạng mục', width: 150, sortable: false, resizable: false , align: 'left', formatter: function(value,row,index){
				    	return Utils.XSSEncode(value);
				    }},
				    {field:'repairCount', title: 'Lần sửa', width: 90, sortable: false, resizable: false , align: 'rigth', formatter: function(value,row,index){
				    	return value;
				    }},
				    {field:'completeDateStr', title: 'Ngày sửa', width: 110, sortable: false, resizable: false , align: 'center', formatter: function(value,row,index){
				    	return Utils.XSSEncode(value);
				    }},
				    {field:'warranty', title: 'Bảo hành - tháng', width: 60, align: 'rigth', sortable: false, resizable: false, formatter: function(value,row,index){
				    	if (value != undefined && value != null) {
				    		return formatCurrency(value);
				    	}
				    	return '';
				    }},
				    {field:'materialPrice', title: 'Đơn giá vật tư', width: 120, align: 'right', sortable: false, resizable: false, formatter: function(value,row,index){
				    	if (value != undefined && value != null) {
				    		return formatCurrency(value);
				    	}
				    	return '';
				    }},
				    {field:'workerPrice', title: 'Đơn giá nhân công', width: 90, align: 'right',sortable: false, resizable: false, formatter: function(value,row,index){
				    	if (value != undefined && value != null) {
				    		return formatCurrency(value);
				    	}
				    	return '';
				    }},
				    {field:'quantity', title: 'Số lượng', width:100, sortable: false, resizable: false , align: 'right', formatter: function(value,row,index){
				    	if (value != undefined && value != null) {
				    		return formatCurrency(value);
				    	}
				    	return '';
				    }},	
				    {field:'totalAmount',title: 'Tổng tiền', width: 100, sortable: false, resizable: false, align: 'right', formatter:function(value,row,index){
				    	if (value != undefined && value != null) {
				    		return formatCurrency(value);
				    	}
				    	return '';
				    }}
			    ]],
			    onLoadSuccess :function(data){	    	
			    	$('.datagrid-header-rownumber').html('STT');
			    	//Utils.functionAccessFillControl('equipHistoryDg');
		   		 	$(window).resize();    		 
			    }
			});
		},
		/**
		 * Xoa tap tin dinh kem
		 * 
		 * @author liemtpt
		 * @since 25/03/2015
		 * */
		removeEquipAttachFile: function(id){
			if (id == undefined || id == null || isNaN(id)) {
				return;
			}
			if (EquipmentListEquipment._arrFileDelete == null || EquipmentListEquipment._arrFileDelete == undefined) {
				EquipmentListEquipment._arrFileDelete = [];
			}
			EquipmentListEquipment._arrFileDelete.push(id);
			$('#divEquipAttachFile'+id).remove();
			if (EquipmentListEquipment._countFile > 0) {
				EquipmentListEquipment._countFile = EquipmentListEquipment._countFile - 1;
			}
		},
		/**
		 * Cap moi Thiet bi
		 * 
		 * @author hunglm16
		 * @since December 20,2014
		 * */
		addOrUpdateEquipment : function (id) {
			$('.SuccessMsgStyle').html('').hide();
			$('.ErrorMsgStyle').html('').hide();
			var msg = '';
			var txtQuantity = '';
			var seriNumber = '';
			if (!isNaN(id) && id > 0) {
				seriNumber = $('#txtSerial').val().trim();
				var arrSeriNumber = seriNumber.split(" ");
				if (arrSeriNumber.length > 1) {
					msg = "Số serial không được phép tồn tại khoảng trắng và chữ có dấu";
				} else {
					msg = Utils.getMessageOfSpecialCharactersValidate('txtSerial', 'Số lượng', Utils._SERIAL);
					if (msg.length > 0) {
						msg = "Số serial không được phép tồn tại khoảng trắng và chữ có dấu";
					}
				}
			} else {
				//tamvnm: bo cap moi
				//So luong
				// txtQuantity = $('#txtQuantity').val().trim();
				// if (msg.length == 0) {
				// 	msg = Utils.getMessageOfRequireCheck('txtQuantity', 'Số lượng');
				// }
				// if (msg.length == 0) {
				// 	msg = Utils.getMessageOfSpecialCharactersValidate('txtQuantity', 'Số lượng', Utils._TF_NUMBER_COMMA);
				// }
				// if (msg.length == 0) {
				// 	if (Number(txtQuantity.replace(/,/g,'')) < 1 || Number(txtQuantity) > 9999) {
				// 		msg = "Số lượng phải là số nguyên lớn hơn 0 và nhỏ hơn 10,000";
				// 	}
				// }
					msg = "Bạn không được phép tạo mới thiết bị!";
					$('#errMsg').html(msg).show();
					return false;

			}
			//Nhom thiet bi
			var ddlEquipGroupId = $('#ddlEquipGroupId').val();
			if (msg.length == 0) {
				msg = Utils.getMessageOfRequireCheck('ddlEquipGroupId', 'Nhóm thiết bị');
			}
			//Nha cung cap
			//tamvnm: thay doi thanh autoCompleteCombobox
			// var ddlEquipProviderId = $('#ddlEquipProviderId').val();
			var ddlEquipProviderId = $('#ddlEquipProviderId').combobox('getValue');
			if (msg.length == 0) {
				// msg = Utils.getMessageOfRequireCheck('ddlEquipProviderId', 'Nhà cung cấp');
				if (ddlEquipProviderId == undefined || ddlEquipProviderId == "") {
					msg = 'Nhà cung cấp không được để trống';
				}
			}
			//Nguyen gia
			var txtPrice = $('#txtPrice').val().trim();
			if (msg.length == 0) {
				msg = Utils.getMessageOfRequireCheck('txtPrice', 'Nguyên giá');
			}
			if (msg.length == 0) {
				msg = Utils.getMessageOfSpecialCharactersValidate('txtPrice', 'Nguyên giá', Utils._TF_NUMBER_COMMA);
			}
			if (msg.length == 0) {
				if (Number(txtPrice.replace(/,/g,'')) < 100 || txtPrice.trim().length > 22) {
					msg = "Nguyên giá phải là kiểu số nguyên dương lớn hơn hoặc bằng 100 và có tối đa 17 ký tự số";
					$('#txtPrice').focus();
				}
			}
			//Ngay het bao hanh
			var txtWarrantyExpiredDate = $('#txtWarrantyExpiredDate').val().trim();
			// if (msg.length == 0) {
			// 	msg = Utils.getMessageOfRequireCheck('txtWarrantyExpiredDate', 'Ngày hết hạn bảo hành');
			// }
			// if (msg.length == 0 && txtWarrantyExpiredDate.length > 0) {
			// 	msg = Utils.getMessageOfInvalidFormatDate('txtWarrantyExpiredDate', 'Ngày hết hạn bảo hành');
			// 	$('#txtWarrantyExpiredDate').focus();
			// }
			//Nam san xuat
			var txtManufacturingYear = $('#txtManufacturingYear').val().trim();
			if (msg.length == 0) {
				msg = Utils.getMessageOfRequireCheck('txtManufacturingYear', 'Năm sản xuất');
			}
			if (msg.length == 0) {
				if (txtManufacturingYear.length != 4 || Number(txtManufacturingYear) < 1000 || Number(txtManufacturingYear) > 9999) {
					msg = 'Năm sản xuất phải có 4 ký tự số';
					$('#txtManufacturingYear').focus();
				}
			}
			
			if(msg.length == 0){
				var yearWarrent = Number(txtWarrantyExpiredDate.split("/")[2]);
				if (yearWarrent < Number(txtManufacturingYear)) {
					msg = 'Năm sản xuất không được lớn hơn năm của Ngày hết hạn bảo hành';
				}
			}
			
			//Trang thai su dung
			var ddlUsageStatus = $('#ddlUsageStatus').val().trim();
			if (msg.length == 0) {
				msg = Utils.getMessageOfRequireCheck('ddlUsageStatus', 'Số lượng');
			}
			//Tinh trang thiet bi
			var ddlHealthStatus = $('#ddlHealthStatus').val().trim();
			if (msg.length == 0) {
				msg = Utils.getMessageOfRequireCheck('ddlHealthStatus', 'Tình trạng thiết bị');
			}
			if (msg.length == 0) {
				msg = Utils.getMessageOfSpecialCharactersValidate('ddlHealthStatus', 'Tình trạng thiết bị', Utils._NAME);
			}
			//Kho(F9)
			var stockCode = $('#stockCode').val().trim();
			if (msg.length == 0) {
				msg = Utils.getMessageOfRequireCheck('stockCode', 'Kho');
			}
			if (msg.length == 0) {
				msg = Utils.getMessageOfSpecialCharactersValidate('stockCode', 'Kho', Utils._CODE);
			}
			if (msg.length > 0){
				$('#errMsg').html(msg).show();
				return false;
			}
			
			//Xu ly nghiep vu
			var params = {
					equipGroupId: ddlEquipGroupId,
					equipProviderId: ddlEquipProviderId,
					price: Number(txtPrice.replace(/,/g,'').trim()),
					warrantyExpiredDateStr: txtWarrantyExpiredDate,
					yearManufacture: Number(txtManufacturingYear),
					usageStatus: Number(ddlUsageStatus),
					healthStatus: ddlHealthStatus,
					stockCode:stockCode
			};
			if (id != undefined && id != null && !isNaN(id) && id > 0) {
				params.id = id;
				params.seriNumber = seriNumber;
			} else {
				params.quantity = Number(txtQuantity.replace(/,/g,'').trim());
				var arrFile = DropzonePlugin._dropzoneObject.getFilesWithStatus(Dropzone.ADDED);
				if (arrFile != null && arrFile != undefined && arrFile.length > 0) {
					if (EquipmentListEquipment._countFile == 5) {
						$('#errMsg').html("Tổng số tập tin đính kèm không được vượt quá 5").show();
						return false;
					}
					var maxLength = 5 - EquipmentListEquipment._countFile;
					if (arrFile.length > maxLength) {
						$('#errMsg').html("Số tập tin thêm mới không được vượt quá " + maxLength).show();
						return false;
					}
					msg = UploadUtil.getMsgByFileRequireCheck(UploadUtil._typeFileRequireCheck.equipRecord);
					if (msg.trim().length > 0) {
						$('#errMsg').html(msg).show();
						return false;
					}
					$.messager.confirm('Xác nhận', 'Bạn có muốn lưu dữ liệu?', function(r){
						if (r){
							var data = JSONUtil.getSimpleObject2(params);
							UploadUtil.updateAdditionalDataForUpload(data);
							UploadUtil.startUpload('errMsg');
							return false;
						}
					});
					return false;
				}
			}
			Utils.addOrSaveData(params, "/equipment-list-manage/addOrUpdateEquipment", null, 'errMsg', function(data) {
				$("#successMsg").html("Lưu dữ liệu thành công").show();
				if (id != undefined && id != null && !isNaN(id) && id > 0) {
					setTimeout(function(){
						$('.SuccessMsgStyle').html("").hide();
					 }, 1500);
				} else {
					setTimeout(function(){
						$('.SuccessMsgStyle').html("").hide();
						window.location.href='/equipment-list-manage/info';
					 }, 700);
				}
			}, null, null, null, msg);
			return false;
		},
		
		/**
		 * Thay doi du lieu nhom thiet bi khi thay doi loai thiet bi
		 * 
		 * @author hunglm16
		 * @since December 21,2014
		 * */
		onchangeDdlEquipCategory : function(cbx) {
			var id = Number($(cbx).val());
			var html = '';
			if (id == undefined || id == null){
				equipCategoryId = 0;
			}
			Utils.getJSONDataByAjax({
				equipCategoryId: id
			}, '/equipment-list-manage/getListEquipGroupVOByCategry', function(data) {
				if (data.rows != undefined && data.rows != null && data.rows.length > 0){
					$('#ddlEquipGroupId').html('<option value="-1" selected="selected">Tất cả</option>');
					var rows = data.rows;
					for (var i = 0; i < rows.length; i++) {
						$('#ddlEquipGroupId').append('<option value="'+rows[i].id+'">'+rows[i].code+' - '+rows[i].name+'</option>');
					}
				} else {
					$('#ddlEquipGroupId').html('<option value="-2" selected="selected">Không có dữ liệu</option>').change();
				}
				$('#ddlEquipGroupId').change();
			}, null, null);
		},
		/**
		 * Thay doi du lieu nhom thiet bi khi thay doi loai thiet bi
		 * 
		 * @author tamvnm
		 * @since 23/07/2015
		 * */
		onchangeDdlEquipCategoryAuto : function(cbx) {
			var id = Number($(cbx).val());
			var html = '';
			if (id == undefined || id == null){
				equipCategoryId = 0;
			}
			Utils.getJSONDataByAjax({
				equipCategoryId: id
			}, '/equipment-list-manage/getListEquipGroupVOByCategry', function(data) {
				if (data.rows != undefined && data.rows != null && data.rows.length > 0){
					var equipGroup = data.rows;
					var obj = {
						id: -1,
						code: "Tất cả",
						name: "Tất cả",
						codeName: "Tất cả",
						searchText: "Tất cả"
					};
					equipGroup.splice(0, 0, obj);
					for(var i = 0, sz = equipGroup.length; i < sz; i++) {
				 		equipGroup[i].searchText = unicodeToEnglish(equipGroup[i].code + " " + equipGroup[i].name);
				 		equipGroup[i].searchText = equipGroup[i].searchText.toUpperCase();
					}
				 	$('#ddlEquipGroupId').combobox("loadData", equipGroup);
				 	$('#ddlEquipGroupId').combobox("setValue", -1);
				} else {
					
				}
			}, null, null);
		},
		
		/**
		 * Thay doi du lieu nhom thiet bi khi thay doi loai thiet bi
		 * 
		 * @author hunglm16
		 * @since December 21,2014
		 * */
		onchangeDdlTradeStatusDiv : function(cbx) {
			var id = Number($(cbx).val());
			var html = '';
			if (id == undefined || id == null){
				$('#ddlTradeType').html('<option value="" selected="selected">Không xác định</option>');
			} else if (id == 0) {
				selectedDropdowlist('ddlTradeType', -1);
				disableSelectbox('ddlTradeType');
			} else {
				enableSelectbox('ddlTradeType');
			}
		},
	 /**
	 * tao gia tri cho autoComplete Combobox Nha cung cap
	 * @author tamnm
	 * @since 17/06/2015
	 */
	setEquipProvider: function(index) {
		var equipProvider = EquipmentListEquipment._lstEquipProvider;
		var obj = {
			id: -1,
			code: "Tất cả",
			name: "Tất cả",
			codeName: "Tất cả",
			searchText: "Tất cả"
		};
		equipProvider.splice(0, 0, obj);
		for(var i = 0, sz = equipProvider.length; i < sz; i++) {
	 		equipProvider[i].searchText = unicodeToEnglish(equipProvider[i].code + " " + equipProvider[i].name);
	 		equipProvider[i].searchText = equipProvider[i].searchText.toUpperCase();
		}
	 	$('#ddlEquipProviderId').combobox("loadData", equipProvider);
	 	$('#ddlEquipProviderId').combobox("setValue", -1);
	},
	/**
	 * tao gia tri cho autoComplete Combobox Nhom thiet bi
	 * @author tamnm
	 * @since 17/06/2015
	 */
	setEquipGroup: function(index) {
		var equipGroup = EquipmentListEquipment._lstEquipGroup;
		var obj = {
			id: -1,
			code: "Tất cả",
			name: "Tất cả",
			codeName: "Tất cả",
			searchText: "Tất cả"
		};
		equipGroup.splice(0, 0, obj);
		for(var i = 0, sz = equipGroup.length; i < sz; i++) {
	 		equipGroup[i].searchText = unicodeToEnglish(equipGroup[i].code + " " + equipGroup[i].name);
	 		equipGroup[i].searchText = equipGroup[i].searchText.toUpperCase();
		}
	 	$('#ddlEquipGroupId').combobox("loadData", equipGroup);
	 	$('#ddlEquipGroupId').combobox("setValue", -1);
	}
};
var EquipStockAdjustForm = {
	lstId : null ,
	currentGridIndexStockAdjustDtl: null,
	_isCreater:0,
	_status:null,
	_mapEquipStockAdjust:new Map(),
	

	/**
	 * @author Datpv4
	 * @since July 02,2015
	 * @description tim kiem phieu sua chua
	 */
	searchEquipStockAdjustForm: function() {		
		$('#equipStockAdjustDtlTemplateDiv').hide();
		var err = "";
		$('#errMsg').html('').hide();
		var msg="";
		
		var fDate = $('#fDate').val();
		var tDate =$('#tDate').val();
		if (msg.length == 0 && $('#fDate').val().trim().length > 0 && !Utils.isDate($('#fDate').val(),'/') && $('#fDate').val() != '__/__/____') {
			msg = 'Ngày tạo từ không tồn tại hoặc không đúng định dạng dd/mm/yyyy.';
			err = '#fDate';
		}
		if (msg.length == 0 && $('#tDate').val().trim().length > 0 && !Utils.isDate($('#tDate').val(),'/') && $('#tDate').val() != '__/__/____') {
			msg = 'Đến ngày không tồn tại hoặc không đúng định dạng dd/mm/yyyy.';
			err = '#tDate';
		}
		if (msg.length==0 && fDate.length > 0 && tDate.length>0 && !Utils.compareDate(fDate, tDate)) {
			msg = 'Ngày tạo từ phải là ngày trước hoặc cùng ngày với đến ngày';
		}
		if(msg.length>0){
			$('#errMsg').html(msg).show();
			$(err).focus();
			setTimeout(function(){$('#errMsg').hide();},3000);
			return false;
		}
		var status = $('#cbxStatus').val();
		var code = $('#txtCode').val();
		var fromDate = $('#fDate').val();
		var toDate = $('#tDate').val();
		
		$('#grid').datagrid('load',{
			page:1,
			code: code,			
			status:status , 			
			fromDate:fromDate,
			toDate:toDate,
			isCreate:EquipStockAdjustForm._isCreater
			});
	},
	
	/**
	 * @author Datpv4
	 * @since July 02,2015
	 * @description chi tiet phieu sua chua
	 */
	initializeEquipStockAdjustDtlGrid: function(id,code) {
		 if (code!=undefined) {
			 	$('#equipStockAdjustCode').text(code);
				$('#equipStockAdjustDtlTemplateDiv').css("visibility","visible").show();
			}
		$('#gridEquipStockAdjustDtlItem').datagrid({
			url : "/equip-stock-adjust/searchEquipStockAdjustDtl",
			pagination : false,
	        rownumbers : true,
	        /*pageNumber : 1,*/
	        scrollbarSize: true,
	        autoWidth: true,
	       /* pageSize: 20,*/
	        /*pageList: [20, 30, 50],*/
	       /* autoRowHeight : true,*/
	        height:400,
			queryParams: {id: id},
			width: $('#gridEquipStockAdjustDtlItemContainer').width(),
			frozenColumns:[[
	            {field:'edit', title:'<a id= "dg_add" class="cmsiscontrol" onclick="EquipStockAdjustForm.viewPopupStockAdjust()"  title="Nhập thông tin thiết bị"><img src="/resources/images/icon_add.png"/></a>', width: 80, align: 'center', sortable: false, resizable: true, formatter: function(value,row,index){
				    	return '<a style = "margin-right: 5px;" onclick="EquipStockAdjustForm.viewPopupStockAdjust('+index+')" href="javascript:void(0)"><img width="18" height="18" src="/resources/images/icon-edit.png" title="Xem phiếu cập nhật kho điều chỉnh "></a><a onclick="EquipStockAdjustForm.deleteRow('+index+')" href="javascript:void(0)"><img width="15" height="15" src="/resources/images/icon_delete.png" title="Xóa phiếu cập nhật kho điều chỉnh"></a>';
				 }},
				{field:'equipId', title: 'ID thiết bị', width: 80,hidden:true, sortable: false, resizable: false , align: 'left', formatter:function(value,row,index){
			    	return Utils.XSSEncode(value);
			    }},
			    {field:'equipCode', title: 'Mã thiết bị', width: 120, sortable: false, resizable: false , align: 'left', formatter:function(value,row,index){
			    	return Utils.XSSEncode(value);
			    }},
			    {field:'serial', title: 'Số serial', width: 180, sortable: false, resizable: false , align: 'left', formatter:function(value,row,index){
			    	return Utils.XSSEncode(value);
			    }},	
			    
			    {field:'healthStatus', title: 'ID Tình trạng thiết bị', hidden:true, width: 120, sortable:false, resizable: false, align: 'left', formatter:function(value,row,index){
			    	return Utils.XSSEncode(value);
			    	
			    }},
				{field:'healthStatusStr', title: 'Tình trạng thiết bị', width: 120, sortable:false, resizable: false, align: 'left', formatter:function(value,row,index){
			    	return Utils.XSSEncode(value);
			    	
			    }},
			    
			    {field:'equipGroupId', title: 'Id Nhóm thiết bị', hidden:true, width: 120, sortable:false, resizable: false, align: 'left', formatter:function(value,row,index){
			    	return Utils.XSSEncode(value);
			    }},
			    
			    {field:'equipGroupCode', title: 'Mã nhóm thiết bị', hidden:true, width: 120, sortable:false, resizable: false, align: 'left', formatter:function(value,row,index){
			    	return Utils.XSSEncode(value);
			    }},
			    
			    {field:'equipGroupName', title: 'Nhóm thiết bị', width: 180, sortable:false, resizable: false, align: 'left', formatter:function(value,row,index){
			    	if(row.equipGroupCode==undefined){
			    		return Utils.XSSEncode(row.equipGroupName);
			    	}else {
			    		return Utils.XSSEncode(row.equipGroupCode +"-"+row.equipGroupName);
			    	}
			    }}
	    		]],	
			columns:[[
		        
			    {field:'equipCateGogyName', title: 'Loại', width: 150, sortable:false, resizable: false, align: 'left', formatter:function(value,row,index){
					return Utils.XSSEncode(value);
			    }},
			    {field:'capacity', title: 'Dung tích (lít)', width: 120, sortable:false, resizable: false, align: 'left', formatter:function(value,row,index){
			    	return Utils.XSSEncode(value);
			    }},
			    {field:'equipBrandName', title: 'Hiệu', width: 180, sortable:false, resizable: false, align: 'left', formatter:function(value,row,index){
			    	return Utils.XSSEncode(value);
			    	
			    }},
			    {field:'equipProviderId', title: 'Id NCC',hidden:true, width: 120, sortable:false, resizable: false, align: 'left', formatter:function(value,row,index){
			    	return Utils.XSSEncode(value);
			    
			    }},
			    {field:'equipProviderCode', title: 'Code NCC', hidden:true , width: 120, sortable:false, resizable: false, align: 'left', formatter:function(value,row,index){
			    	return Utils.XSSEncode(value);
			    
			    }},
			    {field:'equipProviderName', title: 'NCC', width: 120, sortable:false, resizable: false, align: 'left', formatter:function(value,row,index){
			    	if(row.equipProviderCode==undefined){
			    		return Utils.XSSEncode(row.equipProviderName);
			    	} else {
			    		return Utils.XSSEncode(row.equipProviderCode+"-"+row.equipProviderName);
			    	}
			    }},
			    {field:'manufacturingYear', title: 'Năm sản xuất', width: 80, sortable:false, resizable: false, align: 'center', formatter:function(value,row,index){
			    	return Utils.XSSEncode(value);
			    	
			    }},
			    {field:'price', title: 'Nguyên giá', width: 120, sortable:false, resizable: false, align: 'right', formatter:function(value,row,index){
			    	return formatCurrency(value);
			    	
			    }},
			    {field:'warrantyExpiredDate', title: 'Ngày hết hạn bảo hành', width: 100, sortable:false, resizable: false, align: 'center', formatter:function(value,row,index){
			    	return Utils.XSSEncode(value);
			    	
			    }},
			    {field:'stockId', title: 'Id Kho',hidden:true, width: 100, sortable:false, resizable: false, align: 'left', formatter:function(value,row,index){
			    	return Utils.XSSEncode(value);
			    }},
			   /* {field:'stockCode', title: 'Type Kho',hidden:true, width: 100, sortable:false, resizable: false, align: 'left', formatter:function(value,row,index){
			    	return Utils.XSSEncode(value);
			    }},*/
			    
			    {field:'stockCode', title: 'Ma Kho', width: 180, hidden:true, sortable:false, resizable: false, align: 'left', formatter:function(value,row,index){
			    	return Utils.XSSEncode(value);
			    }},
			    
			    {field:'stockName', title: 'Kho', width: 180, sortable:false, resizable: false, align: 'left', formatter:function(value,row,index){
			    	return Utils.XSSEncode(row.stockCode +"-"+row.stockName);
			    }},
			    
				{field: 'id', sortable:false, hidden: true},
		    ]],
			onLoadSuccess:function(data){
				$(window).resize();
		   		 if (code!=undefined) {//Neu view chi tiet trong man hinh Quan ly cap nhat kho dieu chinh
		   			$("#gridEquipStockAdjustDtlItem").datagrid('hideColumn', 'edit');
				} else if($('#gridEquipStockAdjustDtlItemContainer .cmsiscontrol[id^="dg_"]').length == 0){//Phan quyen chuc nang
					disabled('txtDescription');
					//trang thai la cho duyet Neu va la quyen duyet, khi xem chi tiet dc chuyen sang Duyet va khong duyet
					if(EquipStockAdjustForm._status==1){
						html = '<option value="1" >Chờ duyệt</option> <option value="2">Duyệt</option><option value="3">Không duyệt</option>';
						$('#cbxStatus').html(html).change();
						$("#gridEquipStockAdjustDtlItem").datagrid('hideColumn', 'edit');
					} else {
						disabled('btnUpdate');
					}
				}
			}
		});
	},
	
	/**
	 * @author Datpv4
	 * @since July 03,2015
	 * @description Delete data grid row by Index
	 */
	deleteRow: function (index) {
		var row = $('#gridEquipStockAdjustDtlItem').datagrid('getRows')[index];
		$('#gridEquipStockAdjustDtlItem').datagrid("deleteRow", index);
		var data = $('#gridEquipStockAdjustDtlItem').datagrid('getRows');
		$('#gridEquipStockAdjustDtlItem').datagrid('loadData', data);
		
	},
	/**
	 * @author Datpv4
	 * @since July 03,2015
	 * @description man hinh them moi, chinh sua thiet bi
	 */
	viewPopupStockAdjust: function(gridIndex) {
	 	$('#easyuiPopupSearchStockAdjust').show();
		$('#easyuiPopupSearchStockAdjust').dialog({  
	        closed: false,  
	        cache: false,  
	        modal: true,
	        width :960,
	        onOpen: function(){
	        	if(gridIndex!=undefined){
	        		EquipStockAdjustForm.currentGridIndexStockAdjustDtl = gridIndex;
		        	var rows = $('#gridEquipStockAdjustDtlItem').datagrid('getRows');    // get current page rows
		        	var row = rows[gridIndex]; 
		        	
		        	$("#ddlEquipGroupId").data("kendoComboBox").value(row.equipGroupId);
		        	$("#ddlEquipProviderId").data("kendoComboBox").value(row.equipProviderId);
		        	
		        	$('#ddlPrice').val(row.price);
		        	$('#ddlManufacturing').val(row.manufacturingYear);
		        	$('#ddlHealthStatus').val(row.healthStatus).change();
		        	$('#stockCode').val(row.stockCode);
		        	$('#ddlStockId').val(row.stockId);
					$('#ddlStockName').val(row.stockName);
		        	$('#ddlSeri').val(row.serial);
		        	$('#ddlExpireDate').val(row.warrantyExpiredDate);
		        	$('#ddlNo').val(EquipStockAdjustForm.currentGridIndexStockAdjustDtl+1);
		        	$('#ddlEquipId').val(row.equipId);
	        	} 
	        	$('#ddlPrice').focus();
	        	$('#errMsgEquipmentDlg').html("").hide();
	        },
			onClose: function(){
				EquipStockAdjustForm.currentGridIndexStockAdjustDtl = null;
				$('#ddlSeri').val("");
				$("#ddlHealthStatus").val($("#ddlHealthStatus option:first").val()).change();
				$("#ddlEquipCategoryName").val($("#ddlEquipCategoryName option:first").val()).change();
				$('#ddlManufacturing').val("");
				$('#ddlStockId').val("");
				$('#ddlStockName').val("");
				$('#ddlPrice').val("");
				$('#stockCode').val("");
				$('#ddlNo').val("");
	        	$('#ddlEquipId').val("");
				$('#errMsgEquipmentDlg').html("").hide();
				ReportUtils.setCurrentDateForCalendar('ddlExpireDate');
				
			  var widgetEquipGroupId = $("#ddlEquipGroupId").data("kendoComboBox");
	          widgetEquipGroupId.select(0);
	          var widgetEquipProviderId = $("#ddlEquipProviderId").data("kendoComboBox");
	          widgetEquipProviderId.select(0);   
		              
		          
			}
		});
	},
	/**
	 * @author TamVNM
	 * @Updated Datpv4
	 * @since July 06,2015
	 * @description Tim kiem kho phan quyen
	 */
	showPopupSearchStock: function(txtInput){
		var txt = "stockCode";
		VCommonJS.showDialogSearch2({
			inputs : [
		        {id:'code', maxlength:50, label:'Mã đơn vị'},
		        {id:'name', maxlength:250, label:'Tên đơn vị'},
		    ],
		    url : '/commons/get-equipment-stock',
		    columns : [[
		        {field:'codeName', title:'Đơn vị', align:'left', width: 110, sortable:false, resizable:false, formatter:function(value,row,index) {
		        	var nameCode = row.shopCode + '-' + row.shopName;
		        	return Utils.XSSEncode(nameCode);         
		        }},
		        {field:'equipStockCode', title:'Mã kho', align:'left', width: 200, sortable:false, resizable:false},
		        {field:'choose', title:'', align:'center', width:40, sortable:false, resizable:false, formatter:function(value,row,index) {
		            return '<a href="javascript:void(0)" onclick="EquipStockAdjustForm.chooseStock(\''+txt+'\', ' + row.equipStockId +', \''+Utils.XSSEncode(row.equipStockCode)+'\', \''+Utils.XSSEncode(row.equipStockName)+'\','+Utils.XSSEncode(Utils._isShowInputTextByDlSearch2.code)+');">chọn</a>';         
		        }}
		    ]]
		});
	},
	
	/**
	 * @author Datpv4
	 * @since July 06,2015
	 * @description chon kho
	 */
	chooseStock: function (txt,stockId,stockCode,stockName){
		$('#'+txt).val(stockCode);
		$('#ddlStockName').val(stockName);
		$('#ddlStockId').val(stockId);
		$('#common-dialog-search-2-textbox').dialog("close");
		$('#common-dialog-search-2-textbox').remove();
	},
	
	/**
	 * @author Datpv4
	 * @since July 08,2015
	 * @description chon thiet bi
	 */
	chooseStockAdjustPopup: function(txtInput){		
		var msg = Utils.getMessageOfInvalidFormatDate('ddlExpireDate','Ngày hết hạn bảo hành');
		var iDFocus ;
		if(msg.length==0){
			msg = Utils.getMessageOfRequireCheck('ddlEquipGroupIdDiv .k-input', 'Nhóm thiết bị');
			iDFocus = "#ddlEquipGroupIdDiv .k-input";
		}
		if(msg.length==0){
			msg = Utils.getMessageOfRequireCheck('ddlEquipProviderIdDiv .k-input', 'NCC');
			iDFocus = "#ddlEquipProviderIdDiv .k-input";
		}
		if(msg.length==0){
			msg = Utils.getMessageOfRequireCheck('ddlPrice', 'Nguyên giá');
			iDFocus ="#ddlPrice";
		}
		if(msg.length==0){
			msg = Utils.getMessageOfRequireCheck('ddlManufacturing', 'Năm sản xuất');
			iDFocus = "#ddlManufacturing";
		}
		if(msg.length==0){
			msg = Utils.getMessageOfRequireCheck('ddlStockId', 'Kho');
			var stockCode = $("#stockCode").val().trim().toUpperCase();
			var stockName ="" ;
			try {
				stockName = $('#ddlStock option[value='+stockCode+']').text();
			} catch(e){
				msg = "Kho không tồn tại";
				stockName ="";
			}
			if(stockName.trim()!=""){
				$('#ddlStockName').val(stockName.trim());
				msg ="";
			} else {
				msg = "Kho không tồn tại";
				stockName ="";
			}
		}
		iDFocus = "#stockCode";
		if(msg.length==0){
			var curDate = ReportUtils.getCurrentDateString();
			var curDateStr = curDate.split("/");
			if($("#ddlManufacturing").val()>curDateStr[2]){
				msg = "Năm sản xuất phải cùng hoặc trước năm hiện tại";
				iDFocus = "#ddlManufacturing";
			}
			
		}
		if(msg.length==0){
			var ddlExpireDateStr = $('#ddlExpireDate').val().split("/");
			if(ddlExpireDateStr[2]<$("#ddlManufacturing").val()){
				msg = "Năm hết hạn bảo hành phải cùng hoặc sau năm sản xuất";
				iDFocus = "#ddlExpireDate";
			}
		}
		if(msg.length > 0){
			$('#errMsgEquipmentDlg').html(msg).show();
			setTimeout(function(){$('#errMsgEquipmentDlg').html('').hide();},1500);
			$(iDFocus).focus();
			
			return false;
		}
		var equipGroupID = $("#ddlEquipGroupId").data("kendoComboBox").value();
		var equipProviderId = $("#ddlEquipProviderId").data("kendoComboBox").value();
		
		var row = {
				serial:$('#ddlSeri').val(),
				healthStatus:$('#ddlHealthStatus').val(),
				healthStatusStr:$('#ddlHealthStatus option:selected').text(),
				equipGroupId:equipGroupID,
				equipGroupName:$('#ddlEquipGroupId option:selected').text(),
				equipCateGogyName:$('#ddlEquipCategoryName option[value='+equipGroupID+']').text(),
				capacity:$('#ddlCapacity option[value='+equipGroupID+']').text(),
				equipBrandName:$('#ddlBrandName option[value='+equipGroupID+']').text(),
				equipProviderId:equipProviderId,
				equipProviderName:$('#ddlEquipProviderId option[value='+equipProviderId+']').text(),
				manufacturingYear:$('#ddlManufacturing').val(),
				stockCode:$('#stockCode').val(),
				stockName:$('#ddlStockName').val(),
				stockId:$('#ddlStockId').val(),
				price:$('#ddlPrice').val(),
				warrantyExpiredDate:$('#ddlExpireDate').val()
		};
		
		var action = 'updateRow';
		var idx = EquipStockAdjustForm.currentGridIndexStockAdjustDtl;
		if(idx==null ){
			action ='insertRow';
			var rows = $('#gridEquipStockAdjustDtlItem').datagrid("getRows");
			if (rows.length > 0) {
	    		idx = rows.length - 1;
	    	}
		}
		
		$('#gridEquipStockAdjustDtlItem').datagrid(action,{row:row,index:idx});
		var data = $('#gridEquipStockAdjustDtlItem').datagrid('getRows');
		$('#gridEquipStockAdjustDtlItem').datagrid('loadData', data);
		$('#easyuiPopupSearchStockAdjust').dialog('close');
	},
	
	/**
	 * @author Datpv4
	 * @since July 09,2015
	 * @description them moi va cap nhat phieu 
	 */
	updateEquipStockAdjustRecord: function(id) {
		$('.SuccessMsgStyle').html('').hide();
		$('.ErrorMsgStyle').html('').hide();
		var row = $('#gridEquipStockAdjustDtlItem').datagrid('getRows');
		var rowsLength = row.length; 
		if(rowsLength==0){
			var msg="Vui lòng nhập thiết bị";
			if(msg.length > 0){
				$('.ErrorMsgStyle').html(msg).show();
				$('#errMsgEquipmentDlg').html(msg).show();
				setTimeout(function(){$('.ErrorMsgStyle').html('').hide();},1500);
				return false;
			}
		}
		
		var lstEquipStockAdjustDtlId = new Array();
		var lstEquipId = new Array();
		var lstSerial = new Array();
		var lstHealthStatus = new Array();
		var lstEquipGroupId = new Array();
		var lstEquipGroupName = new Array();
		var lstEquipCateGoryName = new Array();
		var lstCapacity = new Array();
		var lstEquipProviderId = new Array();
		var lstEquipProviderName = new Array();
		var lstManufacturingYear = new Array();
		var lstWarrantyExpiredDate = new Array();
		var lstStockId = new Array();
		var lstStockType = new Array();
		var lstStockName = new Array();
		var lstStockCode = new Array();
		var lstPrice = new Array();
		
		for (var i = 0; i < rowsLength; i++) {
			var msg ="";
			if (row[i] != null) {
				if(row[i].id!=undefined){
					lstEquipStockAdjustDtlId.push(row[i].id);
				}
				lstSerial.push(row[i].serial==undefined?" ":row[i].serial);
				
				lstHealthStatus.push(row[i].healthStatus==undefined?" ":row[i].healthStatus);
				if(row[i].equipGroupId!=undefined){
					lstEquipGroupId.push(row[i].equipGroupId);
				} 
				lstEquipGroupName.push(row[i].equipGroupName==undefined?" ":row[i].equipGroupName);
				lstEquipCateGoryName.push(row[i].equipCateGoryName==undefined?" ":row[i].equipCateGoryName);
				if(row[i].equipProviderId!=undefined){
					lstEquipProviderId.push(row[i].equipProviderId);
				} 
					
				lstEquipProviderName.push(row[i].equipProviderName==undefined?" ":row[i].equipProviderName);
				if(row[i].manufacturingYear!=undefined){
					lstManufacturingYear.push(row[i].manufacturingYear);
				} 
				if(row[i].warrantyExpiredDate!=undefined){
					lstWarrantyExpiredDate.push(row[i].warrantyExpiredDate);
				}
				if(row[i].stockId!=undefined){
					lstStockId.push(row[i].stockId==undefined?null:row[i].stockId);
				}
				lstStockName.push(row[i].stockName=undefined?" ":row[i].stockName);
				if(row[i].price!=undefined){
					lstPrice.push(row[i].price==undefined?null:row[i].price);
				}
				if(row[i].stockCode!=undefined){
					lstStockCode.push(row[i].stockCode==undefined?null:row[i].stockCode);
				}
			}
		}		
		var status = $('#cbxStatus').val();
		
		var dataModel = {};
		if(id!=undefined){
			dataModel.id = id;
		}
		
		if ($('#note').attr('maxlength') > 500) {
			$('#errMsgEquipmentDlg').html('Bạn nhập quá giới hạn của trường note').show();
			return false;
		}

		dataModel.status = status;
		dataModel.description = $('#txtDescription').val();
		dataModel.rowsLength = rowsLength;
		dataModel.lstEquipStockAdjustDtlId = lstEquipStockAdjustDtlId;
		dataModel.lstEquipId =lstEquipId;
		dataModel.lstSerial = lstSerial;
		dataModel.lstHealthStatus = lstHealthStatus;
		dataModel.lstEquipGroupId = lstEquipGroupId;
		dataModel.lstEquipGroupName = lstEquipGroupName;
		dataModel.lstEquipCateGoryName = lstEquipCateGoryName;
		dataModel.lstEquipProviderId = lstEquipProviderId;
		dataModel.lstEquipProviderName = lstEquipProviderName;
		dataModel.lstManufacturingYear = lstManufacturingYear;
		dataModel.lstWarrantyExpiredDate = lstWarrantyExpiredDate;
		dataModel.lstStockId = lstStockId;
		dataModel.lstStockType = lstStockType;
		dataModel.lstStockName = lstStockName;
		dataModel.lstStockCode = lstStockCode;
		dataModel.lstPrice = lstPrice;
		dataModel.note = $('#note').val();
		var titleErr = {title:'Lỗi các phiếu ',pageList:10};
		Utils.addOrSaveData(dataModel, '/equip-stock-adjust/update-stock-adjust-record', null, 'errMsgDetail', function(data) {
			if (data.error == false) {
				if (data.equipStockAdjustCode != undefined && data.equipStockAdjustCode != '') {
					$('#txtEquipStockAdjustCode').val(data.equipStockAdjustCode);
					// cap nhat lai Id cho btnUpdate neu muon chuyen, cap nhat lai phieu
					$('#btnUpdate').attr('onclick','EquipStockAdjustForm.updateEquipStockAdjustRecord('+data.equipStockAdjustId+');');
				}														
				if ( data.lstErr != undefined && data.lstErr != null && data.lstErr.length > 0 ) {
					VCommonJS.showDialogList({
						data : data.lstErr,
						dialogInfo: titleErr,
						columns : [[
							{field:'Index', title:'Dòng', align:'left', width: 60, sortable:false, resizable:false},
							{field:'formStatusErr', title:'Trạng thái', align:'left', width: 180, sortable:false, resizable:false, formatter: function(value,row,index){
								var html="";
								if(value == 1){
									html = '<img src="/resources/images/icon_delete.png" height="20" width="20" style="margin-right:5px;" title="Error"> ' + row.recordNotComplete;
								}
								return html;
							} }
						]]
					});	
				} else {
					$('#successMsgDetail').html('Lưu dữ liệu thành công').show();
					setTimeout(function(){$('#successMsgDetail').html('').hide();},1500);
					window.location.href="/equip-stock-adjust/info";
				}
			}
		}, null, null, null, null, null, true);
		
	},
	/**
	 * @author Datpv4
	 * @since July 09,2015
	 * @description cap nhat trang thai phieu
	 */
	updateStatus: function(id) {
		$('.SuccessMsgStyle').html('').hide();
		$('.ErrorMsgStyle').html('').hide();	
		$('#equipStockAdjustDtlTemplateDiv').hide();
		var statusForm = $('#cbxStatusUpdate').val().trim();
		if (statusForm < 0) {
			$('#errMsg').html('Vui lòng chọn trạng thái').show();
			return;
		}
		var arra = EquipStockAdjustForm._mapEquipStockAdjust.valArray;		
		var lstId = new Array();
		if (arra == null || arra.length == 0) {
			$('#errMsg').html('Bạn chưa chọn phiếu cập nhật kho điều chỉnh để cập nhật. Vui lòng chọn phiếu.').show();
			return;
		}		
		for (var i=0, size = arra.length; i < size; ++i) {
			lstId.push(arra[i].id);			
		}
		var params = new Object();
		params.lstId = lstId;
		params.statusForm = statusForm;
		var title = {title:'Lỗi lưu trạng thái cập nhật kho điều chỉnh',pageList:10};
		Utils.addOrSaveData(params, '/equip-stock-adjust/updateStatus', null, 'errMsg', function(data) {
			if ( data.lstErr != undefined && data.lstErr != null && data.lstErr.length > 0 ) {
				VCommonJS.showDialogList({
				    data : data.lstErr,
				    dialogInfo: title,
				    columns : [[
				        {field:'formCode', title:'Mã biên bản', align:'left', width: 120, sortable:false, resizable:false},
				        {field:'formStatusErr', title:'Trạng thái biên bản', align:'left', width: 180, sortable:false, resizable:false, formatter: function(value,row,index){
				        	var html="";
							if(value == 1){
								html = '<img src="/resources/images/icon_delete.png" height="20" width="20" style="margin-right:5px;" title="Error"> ' + row.recordNotComplete;
							}
							return html;
						} }
					]]
				});	
			} else {
				$('#successMsg').html('Lưu dữ liệu thành công').show();
				setTimeout(function(){$('#successMsg').html('').hide();},1500);
			}
			EquipStockAdjustForm._mapEquipStockAdjust = new Map();
			$('#grid').datagrid('reload');
		}, null, true,null, 'Bạn có muốn cập nhật phiếu cập nhật kho điều chỉnh?', null, true);
	},
	
	/**
	 * @author Datpv4
	 * @since July 10,2015
	 * @description Export phieu 
	 */
	exportEquipStockAdjust : function(){
		$('#equipStockAdjustDtlTemplateDiv').hide();
		var status = $('#cbxStatus').val();
		var code = $('#txtCode').val();
		var fDate = $('#fDate').val();
		var tDate =$('#tDate').val();
		var msg ='';
		if (msg.length == 0 && $('#fDate').val().trim().length > 0 && !Utils.isDate($('#fDate').val(),'/')) {
			msg = 'Ngày tạo từ không tồn tại hoặc không đúng định dạng dd/mm/yyyy.';
			err = '#fDate';
		}
		if (msg.length == 0 && $('#tDate').val().trim().length > 0 && !Utils.isDate($('#tDate').val(),'/')) {
			msg = 'Đến ngày không tồn tại hoặc không đúng định dạng dd/mm/yyyy.';
			err = '#tDate';
		}
		if (msg.length==0 && fDate.length > 0 && tDate.length>0 && !Utils.compareDate(fDate, tDate)) {
			msg = 'Ngày tạo từ phải là ngày trước hoặc cùng ngày với đến ngày';
		}
		if(msg.length>0){
			$('#errMsg').html(msg).show();
			$(err).focus();
			setTimeout(function(){$('#errMsg').hide();},3000);
			return false;
		}
		var arra = EquipStockAdjustForm._mapEquipStockAdjust.valArray;		
		var lstId = new Array();
		
		for (var i=0, size = arra.length; i < size; ++i) {
			lstId.push(arra[i].id);			
		}
		var dataModel = new Object();		
		dataModel.code=code;		
		dataModel.status=status; 			
		dataModel.fromDate=fDate;
		dataModel.toDate=tDate;
		dataModel.isCreate=EquipStockAdjustForm._isCreater;
		dataModel.lstId = lstId; 
		ReportUtils.exportReport('/equip-stock-adjust/exportEquipStockAdjust',dataModel);
	},
	
	/**
	 * @author Datpv4
	 * @since July 10,2015
	 * @description Download file template
	 */
	downloadTemplateExcel : function(){
		$('#equipStockAdjustDtlTemplateDiv').hide();
		var dataModel = new Object();	
		ReportUtils.exportReport('/equip-stock-adjust/downloadTemplateExcel',dataModel);
	},
	
	openDialogImportStockAdjust: function (){
		$('#equipStockAdjustDtlTemplateDiv').hide();
		$('.SuccessMsgStyle').html('').hide();
		$('.ErrorMsgStyle').html('').hide();
		$('#easyuiPopupImportExcel').dialog({
			title: 'Nhập excel Quản lý cập nhật kho điều chỉnh',
			width: 465, 
			height: 'auto',
			closed: false,
			cache: false,
			modal: true,
			onOpen: function() {
				
			},
			onClose: function() {
				$('.ErrorMsgStyle').html('').hide();
				$("#grid").datagrid('uncheckAll');
				$("#fakefilepcListEquip").val("");
				$("#excelFileListEquip").val("");
			}
		});
	},
	
	/**
	 * @author Datpv4
	 * @since July 10,2015
	 * @description import moi mot phieu cap nhat kho dieu chinh
	 */
	importExcelEquipStockAdjust: function(){
		$('.SuccessMsgStyle').html('').hide();
		$('.ErrorMsgStyle').html('').hide();
		if($('#fakefilepcListEquip').val().length <= 0 ){
			$('#errExcelMsgListEquip').html('Vui lòng chọn tập tin Excel').show();
			return false;
		}
		Utils.importExcelUtils(function(data){
			$('#btnSearch').click();
			$('#fakefilepcListEquip').val("").change();
			if (data.message != undefined && data.message != null && data.message.trim().length > 0) {
				$('#errExcelMsgListEquip').html(data.message.trim()).change().show();
			}
		}, 'importFrmListEquip', 'excelFileListEquip', 'errExcelMsgListEquip');
		
		
	},
};
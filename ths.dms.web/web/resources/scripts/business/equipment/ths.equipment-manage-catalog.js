var EquipmentManageCatalog = {
	lstId : null,
	_xhrDel :null,
	_pMap:null,
	isImportSuccess : false,
	_mapEviction:new Map(),
	paramForDelete:new Object(),
	_xhrSave : null,
	_editIndex: null,
	_mapEquip: null,
	_lstEquipAvailable:[],
	_mapDetail: null,
	_rowDgTmp: null,
	_mapEquipSaleplanCurrent: null,
	_mapEquipSaleplanNew: null,
	_mapEquipSaleplanDelete: null,
	_mapEquipSaleplanAdd: null,
	_lstEquipSaleplan:null,
	_lstEquipSaleplanDel:null,
	_customerCode: null,
	_editIndex:null,
	_lstEquipInRecord:null,
	_numberEquipInRecord:null,
	_lstEquipInsert:null,
	_idNewRow: -1,
	_lstRowID: null,
	_arrFileDelete: null,
	_countFile: 0,
	_isPressF9Customer: null,
	_curShopCode: null,
	_categoryType: 1,
	_lstCustomerType: null,
	/**
	 * Search danh muc thiet bi
	 * 
	 * @return the string
	 * @author hoanv25
	 * @since 14/12/2014
	 */
	searchinfo : function() {
		var msg = '';
		$('#errMsg').html('').hide();	
		var code =$('#code').val().trim();
		var name =$('#name').val().trim();
		var type = $('#type').val().trim();		
		var status = $('#status').val().trim();			
		if (msg.length > 0) {
			$('#errMsg').html(msg).show();
			return false;
		}
		var params =  new Array();
		params.code = code;
		params.name = name;
		params.type = type;		
		params.status = status;
		$('#grid').datagrid('load',params);
		return false;
	},	
	/**
	 * Luu quan ly danh muc thiet bi
	 * 
	 * @return the string
	 * @author hoanv25
	 * @since 14/12/2014
	 */
	save: function(){
		var msg = '';
		$('#errMsg').html('').hide();
		msg = Utils.getMessageOfRequireCheck('code', 'Mã');
		if (msg.length == 0) {
			msg = Utils.getMessageOfSpecialCharactersValidate('code', 'Mã', Utils._CODE);
		}		
		if (msg.length == 0) {
			msg = Utils.getMessageOfRequireCheck('name', 'Tên');
		}
		if (msg.length > 0) {
			$('#errMsg').html(msg).show();
			return false;
		}
		var id = $('#idChange').val().trim();
		var code = $('#code').val().trim();
		var name = $('#name').val().trim();
		var status = $('#status').val().trim();
		var type = $('#type').val().trim();
		var typeItem = $('#typeItem').val().trim();
		var warranty = Utils.returnMoneyValue($('#warranty').val().trim());
		var dataModel = new Object();	
		dataModel.id = id;
		dataModel.code = code;
		dataModel.name = name;
		dataModel.status = status;	
		dataModel.type = type;		
		dataModel.typeItem = typeItem;		
		dataModel.warranty = warranty;		
	//	var err="";
		Utils.addOrSaveData(dataModel, "/equipment-manage-catalog/save", null, 'errMsg', function(data) {
			if (data.error) {
				$('#errMsg').html(data.errMsg).show();
			}else {
				$('#successMsg').html('Cập nhật dữ liệu thành công').change().show();
				setTimeout(function(){
					$('.SuccessMsgStyle').html('').change().show();
					window.location.href='/equipment-manage-catalog/info';
				}, 700);
			  }
			}, null, null);		
		return false;
	},
	
	/**
	 * Them mot dong nhom thiet bi
	 * @author hoanv25
	 * @since 25/12/2014
	 */
	insertEquipmentGroupInGrid: function(){	
		$('#errMsg').html('').hide();
		if(EquipmentStockChange._lstEquipInsert == null){
			EquipmentStockChange._lstEquipInsert = new Array();
		}
		EquipmentStockChange._idNewRow = EquipmentStockChange._idNewRow - 1;
		$('#grid').datagrid("appendRow", {id: EquipmentStockChange._idNewRow});
		$("#grid").datagrid("selectRow", $("#grid").datagrid("getRows").length - 1);		
	},
	/**
	 * Xoa mot dong thiet bi trong luoi
	 * @author hoanv25
	 * @since 12/12/2014
	 */
	deleteEquipment: function(index, id){
		if (id == undefined || id == null) {
			return;
		}
		if (id > 0) {
			EquipmentStockChange._lstEquipSaleplanDel.push(id);
		}
		$('#grid').datagrid("deleteRow", index);
		var rowEquip = $('#grid').datagrid('getRows');
		var nSize = rowEquip.length;
		for(var i=index; i<nSize; i++){
			var row = rowEquip[i];
			$('#grid').datagrid('updateRow',{
				index: i,	
				row: row
			});	
		}
	},
	/**
	 * Luu quan ly danh muc nhom thiet bi
	 * 
	 * @return the string
	 * @author hoanv25
	 * @since 14/12/2014
	 * 
	 * @author hunglm16
	 * @since May 2,2015
	 * @descrption validate dang truc so cho chuc nang
	 */
	saveEquipGroup: function(){
		var msg = '';
		$('#errMsg').html('').hide();
		$('#btnCapNhat').focus();
		msg = Utils.getMessageOfRequireCheck('code', 'Mã nhóm');
		if (msg.length == 0) {
			msg = Utils.getMessageOfSpecialCharactersValidate('code', 'Mã nhóm', Utils._CODE);
		}		
		if (msg.length == 0) {
			msg = Utils.getMessageOfRequireCheck('name', 'Tên nhóm');
		}
		if (msg.length == 0) {
			msg = Utils.getMessageOfRequireCheck('brand', 'Hiệu');
		}
		if (msg.length == 0) {
			msg = Utils.getMessageOfSpecialCharactersValidateEx('toCapacity','Dung tích', Utils._TF_NUMBER_DOT);
		}		
		if (msg.length == 0) {
			msg = Utils.getMessageOfSpecialCharactersValidateEx('fromCapacity','Dung tích', Utils._TF_NUMBER_DOT);
		}
		if (msg.length > 0) {
			$('#errMsg').html(msg).show();
			return false;
		}		
		var code = $('#code').val().trim();
		var name = $('#name').val().trim();		
		var status = $('#status').val().trim();
		var type = $('#type').val().trim();
		var id = $('#idChange').val().trim();
		var equipBrandName = $('#brand').val().trim();
		var toCapacity = $('#toCapacity').val().trim();	
		var fromCapacity = $('#fromCapacity').val().trim();	
		if(toCapacity =='' && fromCapacity == ''){
			  $('#errMsg').html('Bạn chưa nhập giá trị Dung tích!').show();
			  $('#fromCapacity').focus();
			  return false;
		}
		if(fromCapacity != '' && fromCapacity != undefined && Number(fromCapacity) <=0) {
			  $('#errMsg').html('Dung tích từ không hợp lệ!').show();
			  $('#fromCapacity').focus();
			  return false;
		}
		if(toCapacity != '' && toCapacity != undefined && Number(toCapacity) <=0) {
			  $('#errMsg').html('Dung tích đến không hợp lệ!').show();
			  $('#toCapacity').focus();
			  return false;
		}
		if(Number(toCapacity) != '' && Number(toCapacity) > 0 && Number(fromCapacity) >0 && (Number(toCapacity) < Number(fromCapacity))) {
			  $('#errMsg').html('Dung tích từ không được lớn hơn dung tích đến!').show();
			  $('#toCapacity').focus();
			  return false;
		}
		//Dong cac row con dang mo
		EquipmentManageCatalog.endEditing();
		//Khai bao cac tham so mac dinh
		var arrTT = [];
		var arrDT = [];
		var arrKH = [];
		var arrDS = [];
		
		var rows = $('#grid').datagrid('getRows');
		
		if (rows != undefined && rows != null && rows.length > 0) {
			// check them cung loai KH
			var mapCheckType = new Map();
			for (var i = 0, size = rows.length; i < size; i++) {
				var row = rows[i];
				for (var j = i - 1; j >= 0; j --) {
					var rowp = rows[j];
					//Kiem tra tinh chung chom cua Loai Khach Hang
					if (row.customerTypeId != undefined && Number(row.customerTypeId) == 0 && Number(rowp.customerTypeId) != 0) {
						$('#errMsg').html('Loại khách hàng để trống hết hoặc có giá trị hết các tất cả các dòng').show();
						return false;
					} else if (rowp.customerTypeId != undefined && Number(row.customerTypeId) != 0 && Number(rowp.customerTypeId) == 0) {
						$('#errMsg').html('Loại khách hàng để trống hết hoặc có giá trị hết các tất cả các dòng').show();
						return false;
					}
				}
				// add vao mapCheckType cac loai KH co tren grid; ko co trong mapCheckType thi put vao
				// if (Number(row.customerTypeId) == 0) {
				// 	if (mapCheckType.findIt(Number(row.customerTypeId)) == -1) {
				// 		mapCheckType.put(Number(row.customerTypeId), 'Với loại KH Chưa xác định');
				// 	}
				// } else if (Number(row.customerTypeId) == 1) {
				// 	if (mapCheckType.findIt(Number(row.customerTypeId)) == -1) {
				// 		mapCheckType.put(Number(row.customerTypeId), 'Với loại KH Thành thị');
				// 	}
				// } else if (Number(row.customerTypeId) == 2) {
				// 	if (mapCheckType.findIt(Number(row.customerTypeId)) == -1) {
				// 		mapCheckType.put(Number(row.customerTypeId), 'Với loại KH Nông thôn');
				// 	}
				// }
				if (row.customerTypeId == 0) {
					if (mapCheckType.findIt(Number(row.customerTypeId)) == -1) {
						mapCheckType.put(Number(row.customerTypeId), 'Với loại KH Chưa xác định');
					}
				} else {
					if (mapCheckType.findIt(Number(row.customerTypeId)) == -1) {
						mapCheckType.put(Number(row.customerTypeId), 'Với loại KH');
					}
				}
			}
			// kiem tra tung loai KH:
			var szMapCheckType = mapCheckType.keyArray.length;
			for (var n = 0; n < szMapCheckType; n++) {				
				//Xay dung truc so du tren truc so
				var mapMonthFromTo = new Map();
				var flagTT1 = false; // {flagTT1 = true: thoa man 1 dong co thang bat dau tu 1}
				//Tim Tu Thang Lon Nhat va Den Thang lon nhat khac rong
				var countDtRong = 0;
				var fMonthMax = 1;
				var tMonthMax = 1;
				var indexRowTMonthNull = 0;
				for (var i = 0, size = rows.length; i < size; i++) {
					//Lay doi tuong de xu ly
					var row = rows[i];
					if (Number(row.customerTypeId) == mapCheckType.keyArray[n]) {
						//Xu ly cac doi tuong ban dau
						var tMontI = Number(row.toMonth.replace(/,/g, '').trim());
						var fMontI = Number(row.fromMonth.replace(/,/g, '').trim());
						//Kiem tra tinh hop le cua dong
						var dong = i + 1;
						if (row.fromMonth == undefined || row.fromMonth == null || isNaN(row.fromMonth.replace(/,/g, '').trim()) || fMontI < 1) {
							$('#errMsg').html('Từ tháng dòng thứ ' + dong + ' không hợp lệ').show();
							return false;
						} else if (isNaN(row.toMonth) || (row.toMonth.toString().trim().length > 0 && tMontI < 1)) {
							$('#errMsg').html('Đến tháng dòng thứ ' + dong + ' không hợp lệ').show();
							return false;
						} if (isNaN(row.amount) || Number(row.amount.replace(/,/g, '').trim()) <= 0) {
							$('#errMsg').html('Dòng thứ ' + dong + ' doanh số không hợp lệ').show();
							return false;
						} else if (fMontI > 0 && tMontI > 0 && tMontI < fMontI) {
							$('#errMsg').html('Dòng thứ ' + dong + ' Đến tháng nhỏ hơn Từ tháng').show();
							return false;
						} else if (fMontI == 1){
							flagTT1 = true;
						}
						if (row.toMonth == undefined || row.toMonth == null) {
							indexRowTMonthNull = i;
							countDtRong ++;
							if (countDtRong > 1) {
								$('#errMsg').html('Không được tồn tại 2 dòng có Đến tháng để trống').show();
								return false;
							}
						}
						for (var j = i - 1; j >= 0; j --) {
							var rowp = rows[j];
							if (Number(rowp.customerTypeId) == mapCheckType.keyArray[n]) {
								var dongp = j + 1;
								var tMontJ = Number(rowp.toMonth.toString().replace(/,/g, '').trim());
								var fMontJ = Number(rowp.fromMonth.toString().replace(/,/g, '').trim());
								
								if (fMontI == 1 && fMontJ == 1) {
									if (tMontI != tMontJ && tMontI != 1 && tMontJ != 1) {
										$('#errMsg').html('Dòng thứ ' + dong + ' và dòng thứ '+ dongp + ' giao nhau và Từ tháng, Đến tháng').show();
										//$('#grid').datagrid('beginEdit', i);
										return false;
									} else if (tMontI == tMontJ ) {
										$('#errMsg').html('Dòng thứ ' + dong + ' và dòng thứ '+ dongp + ' giao nhau và Từ tháng, Đến tháng').show();
										//$('#grid').datagrid('beginEdit', i);
										return false;
									}
								} else {
									//Kiem tra Chung chom khoang Tu thang, Den thang
									if (rowp.toMonth.trim().length == 0 && row.toMonth.trim().length == 0) {
										if (fMontI <= tMontJ) {
											$('#errMsg').html('Dòng thứ ' + dong + ' và dòng thứ '+ dongp + ' giao nhau Từ tháng, Đến tháng').show();
											return false;
										}
									} else if (rowp.toMonth.trim().length == 0 || row.toMonth.trim().length == 0) {
										if (rowp.toMonth.trim().length == 0) {
											if (fMontJ <= tMontI) {
												$('#errMsg').html('Dòng thứ ' + dong + ' và dòng thứ '+ dongp + ' giao nhau Từ tháng, Đến tháng').show();
												return false;
											}
										} else {
											if (fMontI <= tMontJ) {
												$('#errMsg').html('Dòng thứ ' + dong + ' và dòng thứ '+ dongp + ' giao nhau Từ tháng, Đến tháng').show();
												return false;
											}
										}
									} else if (tMontJ <= tMontI && tMontJ >= fMontI) {
										/**
										 * den thang J <= den thang I
										 * den thang J >= tu thang I
										 * */
										$('#errMsg').html('Dòng thứ ' + dong + ' và dòng thứ '+ dongp + ' giao nhau Từ tháng, Đến tháng').show();
										return false;
									} else if (fMontJ <= tMontI && fMontJ >= fMontI) {
										/**
										 * tu thang J <= den thang I
										 * tu thang J >= tu thang I
										 * */
										$('#errMsg').html('Dòng thứ ' + dong + ' và dòng thứ '+ dongp + ' giao nhau Từ tháng, Đến tháng').show();
										return false;
									}
								} // end if (Number(rowp.customerType) == mapCheckType.keyArray[n])
							}
						}
						
						/**
						 * Xu dung Map de luu vet theo fromMoth
						 * Mac dinh cac gia tri tren da dung sau khi validate
						 * **/
						var rowValue = mapMonthFromTo.get(fMontI);
						if (rowValue != null && Number(row.fromMonth.replace(/,/g, '').trim()) == 1 && fMontI == 1) {
							if (tMontI > Number(row.toMonth.replace(/,/g, '').trim())) {
								mapMonthFromTo.put(fMontI, tMontI);
							}
						} else {
							mapMonthFromTo.put(fMontI, tMontI);
						}
					} // end if (Number(row.customerType) == mapCheckType.keyArray[n])
				} // end for (var i = 0, size = rows.length; i < size; i++) 
				if (!flagTT1) {
					$('#errMsg').html('Phải tồn tại ít nhất 1 dòng có Từ tháng bằng 1, ' + mapCheckType.get(mapCheckType.keyArray[n])).show();
					return false;
				}
				//Sap xep Tu thang theo thu tu tang dan
				var keyValArr = mapMonthFromTo.keyArray;
				if (keyValArr != null && keyValArr.length > 1) {
					var keyValArrTmp = [];
					for (var j = 0, size = keyValArr.length; j < size; j++) {
						keyValArrTmp.push(keyValArr[j]);
					}
					var arrTTTmp = sortArray.quickSort(keyValArrTmp);
					var lenthT = arrTTTmp.length - 1;
					for (var i = 0; i < lenthT; i++) {
						var tMonth = mapMonthFromTo.get(arrTTTmp[i]);
						var tMonthN = mapMonthFromTo.get(arrTTTmp[i + 1]);
						if (tMonthN == 0 && i < lenthT - 1) {
							$('#errMsg').html('Chỉ được để trống Từ tháng ở dòng đang có Từ tháng lớn nhất ' + mapCheckType.get(mapCheckType.keyArray[n])).show();
							return false;
						}
						if (arrTTTmp[i + 1] - tMonth > 1) {
							$('#errMsg').html('Từ tháng ' + arrTTTmp[i] + ' phải có Đến tháng liền kề với Từ tháng ' + arrTTTmp[i + 1] + ', ' + mapCheckType.get(mapCheckType.keyArray[n])).show();
							return false;
						}
					}
				}
			} // end for (var n = 0; n < szMapCheckType; n++)
			/**
			 * Xu ly gan cac gi tri vao bien tam
			 * */
			for (var i = 0, size = rows.length; i < size; i++) {
				var row = rows[i];
				arrTT.push(i + '_' + row.fromMonth.trim());
				if (row.toMonth.trim().length > 0) {
					arrDT.push(i + '_' + row.toMonth.trim());
				} else {
					arrDT.push(i + '_0');
				}
				if (row.customerTypeId != undefined && String(row.customerTypeId).trim().length > 0) {
					arrKH.push(i + '_' + String(row.customerTypeId).trim());
				} else {
					arrKH.push(i + '_0');
				}
				arrDS.push(i + '_' + row.amount.trim());
			}
		} // end if (rows != undefined && rows != null && rows.length > 0) 
		
		
		/**
		 * Khoi tao cac tham so
		 * */
		var dataModel = new Object();	
		dataModel.id = id;
		dataModel.code = code;
		dataModel.name = name;
		dataModel.status = status;	
		dataModel.type = type;
		dataModel.equipBrandName = equipBrandName;
		dataModel.toCapacity = toCapacity;
		dataModel.fromCapacity = fromCapacity;		
		dataModel.arrDelStr = EquipmentStockChange._lstEquipSaleplanDel.join(',');
		dataModel.arrFromMonthStr = arrTT.join(',');
		dataModel.arrToMonthStr = arrDT.join(',');
		dataModel.arrCustomerTypeStr = arrKH.join(',');
		dataModel.arrAmountStr = arrDS.join(',');
		/**
		 * Xu ly goi ham AJAX
		 * */
		var msgDialog = "Bạn có muốn Cập nhật hay không?";
		JSONUtil.saveData2(dataModel, "/equipment-group-manage/saveEquipGroup", msgDialog, "errMsg", function(data) {
			if (data.error) {
				$('#errMsg').html(data.errMsg).show();
			}else {
				$('#successMsg').html('Cập nhật dữ liệu thành công').change().show();				
				setTimeout(function(){
					$('.SuccessMsgStyle').html('').change().show();				
				}, 1000);
				window.location.href='/equipment-group-manage/info';
			  }
		},null,null);		
		return false;
	},
	
	checkValidateColumnGrid: function (dem) {
		if (dem == undefined || dem == null) {
			return '';
		} else if (dem == 1) {
			return 'Từ tháng không được trống';
		}
		return '';
	},
	
	/**
	* Kiem tra du lieu khong thay doi
	* 
	* @author hoanv25
	*/
	checkChangeDataRow: function (row) {
		var rowOld = EquipmentManageCatalog._mapEquipSaleplanNew.get(row.id);
		if (rowOld != undefined && rowOld != null && rowOld.length > 0) {
			if (rowOld.fromMonth != row.fromMonth) {
				return true;
			} else if (rowOld.toMonth != row.toMonth){
				return true;
			}else if (rowOld.customerType != row.customerType){
				return true;
			}else if (rowOld.amount != row.amount){
				return true;
			}
			//Tiep tuc cac truong co the thay doi
		}
		return false;
	},
	/**
	 * Search danh muc nhom thiet bi
	 * 
	 * @return the string
	 * @author hoanv25
	 * @since 17/12/2014
	 */
	searchEquipGroup : function() {
		var msg = '';
		if (msg.length == 0) {
			msg = Utils.getMessageOfSpecialCharactersValidate('code', 'Mã',Utils._CODE);
		}	
		if (msg.length > 0) {
			$('#errMsg').html(msg).show();
			return false;
		}
		$('#errMsg').html('').hide();	
		var code =$('#code').val().trim();
		var name =$('#name').val().trim();
		var type = $('#type').val().trim();		
		var status = $('#status').val().trim();	
		var toCapacity = $('#toCapacity').val().trim();		
		var fromCapacity = $('#fromCapacity').val().trim();	
		var equipBrandName = $('#brand').val().trim();		
		var params =  new Array();
		params.code = code;
		params.name = name;
		params.type = type;		
		params.status = status;
		params.toCapacity = toCapacity;
		params.fromCapacity = fromCapacity;
		params.equipBrandName = equipBrandName;
		if (params.toCapacity.length > 0) {
			if (!/\d$/.test(params.toCapacity)) {
				$('#errMsg').html("Dung tích đến không hợp lệ!").show();
				$('#toCapacity').focus();
				return false;
			}
		}
		if (params.fromCapacity.length > 0) {
			if (!/\d$/.test(params.fromCapacity)) {
				$('#errMsg').html("Dung tích từ không hợp lệ!").show();
				$('#fromCapacity').focus();
				return false;
			}
		}
		if (params.fromCapacity.length > 0 && params.toCapacity.length > 0) {
			var fromCapacity = parseInt(params.fromCapacity);
			var toCapacity = parseInt(params.toCapacity);
			if (fromCapacity > toCapacity) {
				$('#errMsg').html("Dung tích từ không được lớn hơn dung tích đến!").show();
				$('#toCapacity').focus();
				return false;
			}
		}
		$('#grid').datagrid('load',params);
		return false;
	},
	
	endEditing: function() {
		if (EquipmentManageCatalog._editIndex == undefined) {
			return true;
		}
		if ($('#grid').datagrid('validateRow', EquipmentManageCatalog._editIndex)){
			$('#grid').datagrid('endEdit', EquipmentManageCatalog._editIndex);
			EquipmentManageCatalog._editIndex = undefined;
			return true;
		} else {
			return false;
		}
	},
	
	removeRowOnGrid:function(index){		
    	$('#errMsgProduct').html('').hide();
    	if(index != undefined && index != null ){
	    	$.messager.confirm('Xác nhận', 'Bạn có muốn xóa dòng dữ liệu này?', function(r){
				if (r){
					$('#grid').datagrid('deleteRow', index);			
				}
	    	});
    	}
    },
    
    /**
	 * Tai tap tin excel import danh muc nhom thiet bi
	 * 
	 * @author liemtpt
	 * @since 16/03/2015
	 * @return 
	 * */
	downloadTemplateImportEquipGroup: function() {
		$('.SuccessMsgStyle').html('').hide();
		$('.ErrorMsgStyle').html('').hide();
		var msg = 'Bạn có muốn tải tập tin Import?';
		ReportUtils.exportReport('/equipment-group-manage/downloadTemplateImportEquipGroup', {}, 'errMsg');
	},
	
	/**
	 * Mo dialog nhap excel danh muc nhom thiet bi
	 * 
	 * @author liemtpt
	 * @since 16/03/2015
	 * */
	openDialogImportEquipGroup: function (){
		$('.SuccessMsgStyle').html('').hide();
		$('.ErrorMsgStyle').html('').hide();
		$('#equipHistoryTemplateDiv').css("visibility","hidden").hide();
		$('#easyuiPopupImportExcel').dialog({
			title: 'Nhập excel danh mục nhóm thiết bị',
			width: 465, 
			height: 'auto',
			closed: false,
			cache: false,
			modal: true,
			onOpen: function() {
				
			},
			onClose: function() {
				$('.ErrorMsgStyle').html('').hide();
			}
		});
	},
	/**
	 * Import excel danh muc nhom thiet bi thiet bi
	 * 
	 * @author liemtpt
	 * @since 16/03/2015
	 * */
	importEquipGroup : function(){
		$('.SuccessMsgStyle').html('').hide();
		$('.ErrorMsgStyle').html('').hide();
		
		Utils.importExcelUtils(function(data){
			EquipmentManageCatalog.searchEquipGroup();
			$('#fakefilepc').val("").change();
			if (data.message!=undefined && data.message!=null && data.message.trim().length >0){
				$('#errExcelMsg').html(data.message.trim()).change().show();
			}else{
				$('#successExcelMsg').html("Lưu dữ liệu thành công").show();
				var tm = setTimeout(function(){
					$('.SuccessMsgStyle').html('').hide();
					clearTimeout(tm);
					$('#easyuiPopupImportExcel').dialog('close');
				 }, 1500);
			}
		}, 'importFrm', 'excelFile', 'errExcelMsg');
		
	},
	
	/**
	 * Xuat excel Theo Tim kiem danh muc nhom thiet bi
	 * 
	 * @author liemtpt
	 * @since 16/03/2015 
	 * 
	 **/
	exportBySearchEquipGroup: function() {
		$('.SuccessMsgStyle').html('').hide();
		$('.ErrorMsgStyle').html('').hide();
		$('#errMsg').html('').hide();	
		$('#equipHistoryTemplateDiv').css("visibility","hidden").hide();
		var msg = '';
//		if(EquipmentListEquipment._params != null){
			var rows = $('#grid').datagrid("getRows");
			if(rows.length==0) {
				$('#errMsg').html('Không có dữ liệu xuất Excel').show();
				return false;
			}
			
			/*if(rows == null || rows.length==0){
				msg = "Không có dữ liệu xuất Excel";
			}*/
//		}else{
//			msg = "Không có dữ liệu xuất Excel";
//		}
		/*if(msg.length > 0){
			$('#errMsg').html(msg).show();
			return false;
		}*/
		var code =$('#code').val().trim();
		var name =$('#name').val().trim();
		var type = $('#type').val().trim();		
		var status = $('#status').val().trim();	
		var toCapacity = $('#toCapacity').val().trim();		
		var fromCapacity = $('#fromCapacity').val().trim();	
		var equipBrandName = $('#brand').val().trim();		
		var params =  new Object();
		params.code = code;
		params.name = name;
		params.type = type;		
		params.status = status;
		params.toCapacity = toCapacity;
		params.fromCapacity = fromCapacity;
		params.equipBrandName = equipBrandName;
		
		var msg = 'Bạn có muốn xuất Excel?';
		$.messager.confirm('Xác nhận', msg, function(r){
			if (r){
				ReportUtils.exportReport('/equipment-group-manage/exportBySearchEquipmentVO',params, 'errMsg');
			}
		});
		
	},
	/**
	 * tai file excel template
	 * @author tamvnm
	 * @since 31/05/2015
	 */
	downloadTemplate:function () {
	 	var params = new Object();
	 	ReportUtils.exportReport('/equipment-eviction-manage/download-template', params, 'errMsg');
	 },
	//phuongvm thu hoi va thanh ly
    /**
	 * in BBTHTL
	 * @author phuongvm
	 * @since 16/01/2015
	 */
	printBBTHTL: function(id){
		$('.ErrorMsgStyle').html('').hide();
		var arra = EquipmentManageCatalog._mapEviction.valArray;		
		//lay danh sach bien ban
		var lstIdRecord = new Array();
		if(id != null){			
			lstIdRecord.push(id);
		}else{
			for(var i=0,size=arra.length;i<size;++i){
				if(arra[i].trangThaiBienBan == "Đã duyệt"){
					lstIdRecord.push(arra[i].id);	
				}
			}
			if(lstIdRecord.length == 0) {
				$('#errMsg').html('Vui lòng chọn biên bản đã duyệt để xem bản in!').show();
				return;
			}	
		}		
		
		var params = new Object();
		params.lstIdRecord = lstIdRecord;
		try {
			params.reportCode = $('#function_code').val();
		} catch(e) {
			// Oop! Error
		}
		$.messager.confirm('Xác nhận','Bạn có muốn xem bản in biên bản?',function(r){
			if(r){
				ReportUtils.exportReport('/equipment-eviction-manage/print', params, 'errMsg');
			}
		});		
		return false;
	},

	searchEviction:function(){
		EquipmentManageCatalog._mapEviction = new Map();
		var err = "";
		$('#errMsg').html('').hide();
		var msg="";
		var fDate = $('#fDate').val();
		var tDate =$('#tDate').val();
		var shopKendo = $("#shop").data("kendoMultiSelect");
		var dataShop = shopKendo.dataItems();

		if(msg.length == 0){
			msg = Utils.getMessageOfSpecialCharactersValidate('equipSugEvictionCode','Mã biên bản đề nghị thu hồi');
		}

		if(msg.length == 0 && $('#fDate').val().trim().length > 0 && !Utils.isDate($('#fDate').val()) && $('#fDate').val() != '__/__/____' ){
			msg = 'Từ ngày không tồn tại hoặc không đúng định dạng dd/mm/yyyy.';
			err = '#fDate';
		}
		if(msg.length == 0 && $('#tDate').val().trim().length>0 && !Utils.isDate($('#tDate').val()) && $('#tDate').val() != '__/__/____' ){
			msg = 'Đến ngày không tồn tại hoặc không đúng định dạng dd/mm/yyyy.';
			err = '#tDate';
		}
		if(msg.length==0 && fDate.length>0 && tDate.length>0 && !Utils.compareDate(fDate, tDate)){
			msg = 'Từ ngày phải là ngày trước hoặc cùng ngày với đến ngày';
		}
		if(msg.length>0){
			$('#errMsg').html(msg).show();
			$(err).focus();
			setTimeout(function(){$('#errMsg').hide();},3000);
			return false;
		}
		var params = new Object(); 
		params.equipCode = $('#equipCode').val().trim();
		params.seri = $('#seri').val().trim();
		params.customerCode = $('#customerCode').val().trim();
		params.statusForm = $('#status').val().trim();
		params.statusAction = $('#statusDelivery').val().trim();
		params.equipSugEvictionCode = $('#equipSugEvictionCode').val().trim();
		params.toDate = tDate;
		params.fromDate = fDate;
		if (dataShop == null || dataShop.length == 0) {
			strListShopId = '';
		} else {
			var lstShopId = dataShop[0].shopId;
			for(var i = 1; i < dataShop.length; i++){
				lstShopId += "," + dataShop[i].shopId;
			}
			params.strListShopId = lstShopId;
		}		
		$('#grid').datagrid('reload',params);
		// $('#grid').datagrid('load',params);
	},
	
	/**
	 * xuat file excel thu hoi va thanh ly
	 * @author tamvnm
	 * @since 14/01/2015
	 */
	exportExcelTHTL: function(){
		$('.errMsg').html("").hide();
		var lstChecked = EquipmentManageCatalog._lstRowID;
		var rows = $('#grid').datagrid('getRows');	
		if(rows.length==0) {
			$('#errMsg').html('Không có biên bản để xuất excel').show();
			return;
		}
		if(lstChecked.length == 0) {
			$('#errMsg').html('Vui lòng chọn biên bản đã duyệt để xuất excel!').show();
			return;
		}	

		var params = new Object();
		params.lstIdRecord = lstChecked;
		$.messager.confirm('Xác nhận','Bạn có muốn xuất file excel?',function(r){
			if(r){
				ReportUtils.exportReport('/equipment-eviction-manage/export-record-eviction', params, 'errMsg');					
			}
		});		
		return false;
	},
	
	getHistory: function(idRecord){
		$('#easyuiPopupHistory').dialog({  
	        closed: false,  
	        cache: false,  
	        modal: true,
	        width : 'auto',
	        height : 'auto',
	        onOpen: function(){	       
				$('#gridHistory').datagrid({
					url : '/equipment-eviction-manage/searchHistoryEviction',
					autoRowHeight : true,
					rownumbers:true,
					fitColumns:true,
					scrollbarSize:0,
					width: $('#gridHitoryDiv').width(),
					autoWidth: true,
					queryParams:{
						id : idRecord
					},		
					columns:[[
					    {field: 'ngay',title:'Ngày',width: 100,sortable:false,resizable:false, align: 'center', formatter: function(value,row,index){
							return Utils.XSSEncode(value);
						} },					
						{field: 'trangThaiGiaoNhan', title: 'Trạng thái biên bản', width: 100, sortable:false,resizable:false,align: 'left', formatter: function(value,row,index){
							return Utils.XSSEncode(value);
						}}			    
					]],
					onBeforeLoad:function(param){											 
					},
					onLoadSuccess :function(data){   			 
				    	$('.easyui-dialog .datagrid-header-rownumber').html('STT');	    		
					}
				});
	        }
		});
	},
	updateStatus: function() {
		$('#errMsg').html('').hide();		
		var arra = EquipmentManageCatalog._mapEviction.valArray;		
		var lstId = new Array();
		if(arra == null || arra.length == 0) {
			$('#errMsg').html('Bạn chưa chọn biên bản để cập nhật. Vui lòng chọn biên bản').show();
			return;
		}		
		for(var i=0,size=arra.length;i<size;++i){
			lstId.push(arra[i].id);			
		}
		var params = new Object();
		var statusForm = $('#statusRecordLabel').val().trim();
		var statusAction = $('#statusDeliveryLabel').val().trim();
		if(statusForm<0 && statusAction<1){
			$('#errMsg').html('Vui lòng chọn trạng thái').show();
			return;
		}
		params.lstId = lstId;
		params.statusAction = statusAction;
		params.statusForm = statusForm;
		Utils.addOrSaveData(params, '/equipment-eviction-manage/update', null, 'errMsg', function(data) {
			if(data.lstErr!=undefined && data.lstErr!=null && data.lstErr.length>0){
				VCommonJS.showDialogList({
				    data : data.lstErr,
				    columns : [[
				        {field:'formCode', title:'Mã biên bản', align:'left', width: 110, sortable:false, resizable:false},
				        {field:'formStatusErr', title:'Trạng thái biên bản', align:'center', width: 110, sortable:false, resizable:false, formatter: function(value,row,index){
				        	var html="";
							if(value == 1){
								html = '<img src="/resources/images/icon_delete.png" height="20" width="20" style="margin-right:5px;" title="Error">';
							}
							return html;
						}},
						 {field:'deliveryStatusErr', title:'Trạng thái giao nhận', align:'center', width: 200, sortable:false, resizable:false, formatter: function(value,row,index) {
				        	var html="";
							if(value == 1){
								html = '<img src="/resources/images/icon_delete.png" height="20" width="20" style="margin-right:5px;" title="Error">';
							}
							return html;
						} },
						{field:'recordNotComplete', title:'Chi tiết biên bản', align:'center', width:80, sortable:false, resizable:false, formatter: function(value, row, index) {
								return VTUtilJS.XSSEncode(value);
						}}			    
					]]
				});	
			}
			EquipmentManageCatalog._mapEviction = new Map();
			$('#grid').datagrid('reload');
			setSelectBoxValue('statusRecordLabel', -1);
			setSelectBoxValue('statusDeliveryLabel', 0);
		}, null, true,null, 'Bạn có muốn cập nhật biên bản?');
		
	},
	showDlgImportExcel: function(){
		$('#easyuiPopupImportExcel').show();
		$('.ErrorMsgStyle').hide();
		$('#easyuiPopupImportExcel').dialog({  
	        closed: false,  
	        cache: false,  
	        modal: true,
	        onOpen: function(){	 
	        	$('.easyui-dialog .BtnGeneralStyle').css('color','');
	        },
	        onClose: function(){
	        	EquipmentManageCatalog.searchEviction();
				$("#grid").datagrid('uncheckAll');
				$("#fakefilepcBBGN").val("");
				$("#excelFileBBGN").val("");				
	        }
		});
	},
	importExcelBBTHTL: function(){
		$('.ErrorMsgStyle').hide();
		VTUtilJS.importExcelUtils(function(data) {
			$("#errExcelMsgBBGN").html(data.message).show();		
			if(data.fileNameFail == null || data.fileNameFail == "" ){					
				setTimeout(function(){
					$('#errExcelMsgBBGN').html("").hide();
					$('#easyuiPopupImportExcel').dialog('close');
				}, 3000);				
			}else{
				$("#fakefilepcBBGN").val("");
				$("#excelFileBBGN").val("");			
				$('#grid').datagrid('reload');	
			}						
		}, "importFrmBBGN", "excelFileBBGN", null, "errExcelMsgBBGN");
		return false;
	},
	/**
	 * Chon don vi
	 * @author phuongvm
	 * @since 21/12/2014
	 */
	selectShop: function(id,code,name){
		$('#shopCode').val(code).change();	
		$('#customerName').val('');	
		$('#customerCode').val('');
		$('#address').val('');		
		$('#phone').val('');	
		$('.easyui-dialog').dialog('close');
		$('#shopCode').focus();	
		EquipmentManageCatalog._curShopCode = code;
	},
	/**
	 * Chon don vi
	 * @author phuongvm
	 * @since 21/12/2014
	 */
	selectNewShop: function(id,code,name){
		$('#newShopCode').val(code).change();
		$('#newCustomerCode').val('');
		$('#newAddress').val('');	
		$('.easyui-dialog').dialog('close');
		$('#newShopCode').focus();	
	},
	/**
	 * chon khach hang moi tu popup khach hang
	 * 
	 * @return the string
	 * @author phuongvm
	 * @since 21/12/2014
	 */
	selectNewCustomer: function(shortCode,customerName,address,phone){		
		$('#newCustomerCode').val(shortCode);
		$('#newAddress').val(address);		
		$('.easyui-dialog').dialog('close');
		$('#newCustomerCode').focus();	
	},
	/**
	 * chon khach hang tu popup khach hang dong thoi lay danh sach thiet bi trong bien ban giao nhan
	 * 
	 * @return the string
	 * @author phuongvm
	 * @since 20/12/2014
	 */
	selectCustomer: function(shortCode,customerName,address,phone){		
		$('#customerCode').val(shortCode);		
		$('#customerName').val(customerName);		
		$('#address').val(address);		
		$('#phone').val(phone);		
		EquipmentManageCatalog.eventselectCustomer(shortCode);
		$('.easyui-dialog').dialog('close');
		EquipmentManageCatalog._isPressF9Customer = false;
		EquipmentManageCatalog._customerCode = shortCode;
	},
	/**
	 * Xu ly su kien khi chon khach hang tu popup khach hang
	 * 
	 * @author phuongvm
	 * @since 20/12/2014
	 * */
	eventselectCustomer: function (shortCode,idEviction) {
		$('.ErrorMsgStyle').html('').hide();
		$('.SuccessMsgStyle').html('').hide();
		EquipmentManageCatalog._rowDgTmp = {};
		var id = -1;
		if(idEviction!=undefined && idEviction!=null){
			id = idEviction;
		}
		Utils.getJSONDataByAjaxNotOverlay ({customerCode: shortCode,shopCode:$('#shopCode').val().trim(),id:id}, '/equipment-eviction-manage/getListEquipInCustomer', function (data){
			EquipmentManageCatalog._mapEquip = new Map();
			EquipmentManageCatalog._mapDetail = new Map();
			EquipmentManageCatalog._lstEquipAvailable = new Array();
			if (data != undefined && data != null && data.rows != undefined && data.rows != null && data.rows.length >0) {
				if(idEviction!=undefined && idEviction!=null){
					if(data.lstEquipId != undefined && data.lstEquipId != null && data.lstEquipId.length >0){
						for (var i = 0; i < data.rows.length; i++) {
							EquipmentManageCatalog._mapEquip.put(data.rows[i].id, data.rows[i]);
//							EquipmentManageCatalog._lstEquipAvailable.push(data.rows[i]);
						}
//			   		 	for (var j = data.lstEquipId.length - 1; j >= 0; j--) {
			   		 	for (var j = 0; j < data.lstEquipId.length; j++) {	
			   		 		var prdId = data.lstEquipId[j];
			   				EquipmentManageCatalog._mapDetail.put(prdId,EquipmentManageCatalog._mapEquip.get(prdId));
//			   				EquipmentManageCatalog._mapEquip.remove(prdId);
			   			}
			   		 	for (var k = 0; k< EquipmentManageCatalog._mapEquip.keyArray.length; k++) {
			   		 		var idE = EquipmentManageCatalog._mapEquip.keyArray[k];
			   		 		var row = EquipmentManageCatalog._mapEquip.get(idE);
			   		 		EquipmentManageCatalog._lstEquipAvailable.push(row);
			   			}
					}
				}else{
					for (var i = 0; i < data.rows.length; i++) {
						EquipmentManageCatalog._mapEquip.put(data.rows[i].id, data.rows[i]);
						EquipmentManageCatalog._lstEquipAvailable.push(data.rows[i]);
					}
				}
			}
			$('#grid').datagrid({
				data: EquipmentManageCatalog.getDataInPrdStockOutputDg(),
				width :  $('#gridContainer').width() - 15,
				fitColumns : true,
			    rownumbers : true,
			    singleSelect:true,
			    height: 275,
			    columns:[[
				    {field:'soHopDong', title:'Số hợp đồng', width:120, align:'left', formatter: function(value, row, index){
				    	var prdName = 'dgSoHopDong'+ row.id;
				    	return '<label id="'+prdName+'" style="width: 100%; text-align: left;">'+Utils.XSSEncode(value)+'</label>';
				    }},
				    {field:'loaiThietBi',title:'Loại thiết bị', width:80,align:'left', formatter: function(value, row, index){
				    	var idLot = "dgLoaiThietBi"+ row.id;
				    	return '<label id="'+idLot+'" style="width: 100%; text-align: left;">'+Utils.XSSEncode(value)+'</label>';
				    }},
				    {field:'nhomThietBi', title:'Nhóm thiết bị', width:80, align:'left', formatter: function(value, row, index){
				    	var idAvlQ = "dgNhomThietBi"+ row.id;
				    	return '<label id="'+idAvlQ+'" style="width: 100%; text-align: left;">'+Utils.XSSEncode(value)+'</label>';
				    }},
				    {field:'maThietBi',title:'Mã thiết bị', width:120,align:'left',formatter: function(value, row, index){
						return Utils.XSSEncode(value);
					}
					, editor: {
						type:'combobox',
							options:{ 
							valueField:'id',
						    textField:'maThietBi',
						    data: EquipmentManageCatalog._lstEquipAvailable,
						    width: 200,
						    formatter: function(row) {
						    	return '<span style="font-weight:bold">' + row.maThietBi  + '</span>';
						    },
						    onSelect:function(rec){
						    	EquipmentManageCatalog._rowDgTmp = {
					    			id: rec.id,
									maThietBi: rec.maThietBi,
									soHopDong: rec.soHopDong,
									loaiThietBi: rec.loaiThietBi,
									nhomThietBi: rec.nhomThietBi,
									soSeri: rec.soSeri,
									soLuong: rec.soLuong,
									tinhTrangThietBi: rec.tinhTrangThietBi
						    	};
						    	var soHopDong = '';
						    	if(rec.soHopDong!=null){
						    		soHopDong = rec.soHopDong.trim();
						    	}
						    	var soSeri = '';
						    	if(rec.soSeri!=null){
						    		soSeri = rec.soSeri.trim();
						    	}
						    	$('#dgSoHopDong0').html(Utils.XSSEncode(soHopDong)).change();
						    	$('#dgLoaiThietBi0').html(Utils.XSSEncode(rec.loaiThietBi)).change();
						    	$('#dgNhomThietBi0').html(Utils.XSSEncode(rec.nhomThietBi.trim())).change();
						    	$('#dgSoSeri0').html(Utils.XSSEncode(soSeri)).change();
						    	$('#dgSoLuong0').html(Utils.XSSEncode(rec.soLuong)).change();
						    	$('#dgTinhTrangThietBi0').html(Utils.XSSEncode(rec.tinhTrangThietBi.trim())).change();
						    	
						    	EquipmentManageCatalog._mapDetail.put(EquipmentManageCatalog._rowDgTmp.id, EquipmentManageCatalog._rowDgTmp);
						    	$('#grid').datagrid("loadData", EquipmentManageCatalog.getDataInPrdStockOutputDg());
						    	try {
						    		$('.combo-panel').each(function(){
										if(!$(this).is(':hidden')){
											$(this).css('display','none');
										}			
									});
						    	} catch(err)  {
						    		//nothing.
						    	}
						    	
						    },
						    onChange:function(pCode){
//						    	$('.combo-panel').each(function(){
//									if(!$(this).is(':hidden')){
//										$(this).css('display','none');
//									}			
//								});
						    },
						    filter: function(q, row){
								q = new String(q).toUpperCase();
								var opts = $(this).combobox('options');
								return row[opts.textField].indexOf(q)>=0;
							}
						}
					}},
				    {field:'soSeri', title:'Số serial', width:100, align:'left', formatter: function(value, row, index){
				    	var idQuan = "dgSoSeri"+ row.id;
				    	return '<label id="'+idQuan+'" style="width: 100%; text-align: left;">'+Utils.XSSEncode(value)+'</label>';
				    }},
				    {field:'soLuong', title:'Số lượng', width:80, align:'right', formatter: function(value, row, index){
				    	var idPrice= "dgSoLuong"+ row.id;
				    	return '<label id="'+idPrice+'" style="width: 100%; text-align: right;">'+Utils.XSSEncode(value)+'</label>';
				    }},
				    {field:'tinhTrangThietBi', title:'Tình trạng thiết bị', width:80, align:'left', formatter: function(value, row, index){
				    	var idAmount = "dgTinhTrangThietBi"+ row.id;
				    	return '<label id="'+idAmount+'" style="width: 100%; text-align: left;">'+Utils.XSSEncode(value)+'</label>';
				    }},
				    {field:'detelte',title:'',width:50, align:'center', formatter: function(value, row, index){
				    	if (row.id != undefined && row.id != null && row.id > 0 && ($('#equipSugEvictionCode').val() == undefined ||  $('#equipSugEvictionCode').val() == '')) { 
				    		return '<a href="javascript:void(0)" id="dg_prdStockOutputDg_delete" onclick="return EquipmentManageCatalog.deleteRowInPrdStockOutputDg('+row.id+');"><img title="Xóa" src="/resources/images/icon_delete.png" width="15" heigh="15"></a>';
				    	}
				    }}
				]],
				onClickRow: function(rowIndex, rowData) {
					
				},
				onAfterEdit: function(rowIndex, rowData, changes) {
				},
				onBeforeEdit : function(rowIndex, rowData, changes){
				},
		        onLoadSuccess :function(data){
			    	 $('.datagrid-header-rownumber').html('STT');
			    	 Utils.updateRownumWidthAndHeightForDataGrid('grid');
			    	 //Xu ly cho hien thi
			    	 var arrInput = '';
			    	 EquipmentManageCatalog.getDataComboboxInPrdStockOutputDg();
			    	 var rows = $('#grid').datagrid('getRows');
			    	 if ($('#equipSugEvictionCode').val() != "") {
			    		$('#grid').datagrid("hideColumn", "detelte");
			    	 } else if(rows == null || rows.length == 0 || rows[rows.length-1].id != null){
			    		 EquipmentManageCatalog.insertRowStockTransDetailDg();
			    	 }
			    	
			    	 
			    	 
			    	 if (EquipmentManageCatalog._mapDetail != null && EquipmentManageCatalog._mapDetail.size() > 0) {
//			    		 for(var i=0; i < EquipmentManageCatalog._mapDetail.keyArray.length; i++){
//			    			 var productTmp = EquipmentManageCatalog._mapDetail.get(EquipmentManageCatalog._mapDetail.keyArray[i]);
//				    		 sumAmountRow += productTmp.amount;
//				    		 weight += productTmp.grossWeight;
//				    	 }
			    	 } else {
			    		 EquipmentManageCatalog._mapDetail = new Map();
			    	 }
			    	$('.combobox-f.combo-f').combobox("loadData", EquipmentManageCatalog._lstEquipAvailable);
			    	if ($('#equipSugEvictionCode').val() != "") {
			    		$('#grid').datagrid("hideColumn", "detelte");
			    	}
		    	}
			});
		}, null, null);
	},
	/**
	 * Lay du lieu cho PrdStockOutputDg
	 * 
	 * @author phuongvm
	 * @since 20/12/2014
	 * */
	getDataInPrdStockOutputDg : function() {
		var rows = [];
		if ($('#equipSugEvictionCode').val() != "") {
			if (EquipmentManageCatalog._mapDetail != null || EquipmentManageCatalog._mapDetail.size() > 0){
				for (var i=0; i< EquipmentManageCatalog._mapDetail.keyArray.length; i++) {
					rows.push(EquipmentManageCatalog._mapDetail.get(EquipmentManageCatalog._mapDetail.keyArray[i]));
					rows[i].equipSugEvictionCode = $('#equipSugEvictionCode').val();
				}
			}
		} else {
			if (EquipmentManageCatalog._mapDetail != null || EquipmentManageCatalog._mapDetail.size() > 0){
				for (var i=0; i< EquipmentManageCatalog._mapDetail.keyArray.length; i++) {
					rows.push(EquipmentManageCatalog._mapDetail.get(EquipmentManageCatalog._mapDetail.keyArray[i]));
				}
			}
		}

		
		return rows;
	},
	/**
	 * Lay danh sach thiet bi sau loai tru trong Grid
	 * 
	 * @author phuongvm
	 * @since 21/12/2014
	 * */
	getDataComboboxInPrdStockOutputDg: function () {
		if (EquipmentManageCatalog._mapDetail != null && EquipmentManageCatalog._mapDetail.size() > 0) {
   		 	for (var i = 0; i < EquipmentManageCatalog._mapDetail.keyArray.length; i++) {
	   		 	for (var j = EquipmentManageCatalog._lstEquipAvailable.length - 1; j >= 0; j--) {
	   		 		var prdId = EquipmentManageCatalog._mapDetail.keyArray[i];
	   				if (EquipmentManageCatalog._lstEquipAvailable[j].id == prdId) { 
	   					EquipmentManageCatalog._lstEquipAvailable.splice(j, true);
	   				}
	   			}
	    	}
   	 	}
	},
	/**
	 * Xoa mot dong thiet bi
	 * 
	 * @author phuongvm
	 * @since 20/12/2014
	 * */
	deleteRowInPrdStockOutputDg: function (id) {
		if (id == null || id == null || id == 0) {
			return;
		}
		if (EquipmentManageCatalog._mapDetail.keyArray.indexOf(id) > -1) {
			for (var i = 0; i< EquipmentManageCatalog._mapEquip.keyArray.length; i++) {
				if (EquipmentManageCatalog._mapEquip.keyArray[i] == id) {
					EquipmentManageCatalog._lstEquipAvailable.push(EquipmentManageCatalog._mapEquip.get(id));
					//Sap xep lai
		   		 	var arrSort = [];
		   		 	for (var i = 0; i < EquipmentManageCatalog._lstEquipAvailable.length; i++) {
		   		 		var value = EquipmentManageCatalog._lstEquipAvailable[i].maThietBi + '_-_-_' + EquipmentManageCatalog._lstEquipAvailable[i].id;
		   		 		arrSort.push(value.trim());
					}
		   		 	//lenh sap xep Order by ASC
		   		 	arrSort = arrSort.sort();
		   		 	//sap xep lai mang
		   		 	var data = [];
		   		 	for (var t = 0; t < arrSort.length; t++) {
		   		 		for (var i = 0; i < EquipmentManageCatalog._lstEquipAvailable.length; i++) {
		   		 			if(Number(arrSort[t].split('_-_-_')[1]) == EquipmentManageCatalog._lstEquipAvailable[i].id) {
		   		 				data.push(EquipmentManageCatalog._lstEquipAvailable[i]);
		   		 			}
		   		 		}
		   		 	}
		   		 	//gan lai mang vua sap xep 
		   		 	EquipmentManageCatalog._lstEquipAvailable = new Array();
		   		 	for (var t = 0; t < data.length; t++) {
		   		 		EquipmentManageCatalog._lstEquipAvailable.push(data[t]);		   		 		
		   		 	}
					break;
				}
			}
			EquipmentManageCatalog._mapDetail.remove(id);
			
			$('#grid').datagrid("loadData", EquipmentManageCatalog.getDataInPrdStockOutputDg());
		} else {
			$.messager.alert('Lỗi dữ liệu','Không tìm thấy id thiết bị muốn xóa.','error');
		}
	},
	/**
	 * Them moi 2 dong trong vao Datagrid 
	 * 
	 * @author phuongvm
	 * @Since 20/12/2014
	 * */
	insertRowStockTransDetailDg: function () {
		var indexMax = $('#grid').datagrid('getRows').length;
		$('#grid').datagrid('insertRow',{
			index: indexMax,
			row: {
				id: 0,
				maThietBi: '',
				soHopDong: '',
				loaiThietBi: '',
				nhomThietBi: '',
				soSeri: '',
				soLuong: '',
				tinhTrangThietBi: ''
			}
		});
		EquipmentManageCatalog._rowDgTmp = {
    			id: 0,
				maThietBi: '',
				soHopDong: '',
				loaiThietBi: '',
				nhomThietBi: '',
				soSeri: '',
				soLuong: '',
				tinhTrangThietBi: ''
	    	};
		$('#grid').datagrid('selectRow', indexMax).datagrid('beginEdit', indexMax);
	},
	/**
	 * Xoa tap tin dinh kem
	 * 
	 * @author hunglm16
	 * @since Feb 10,214
	 * */
	removeEquipAttachFile: function(id){
		if (id == undefined || id == null || isNaN(id)) {
			return;
		}
		if (EquipmentManageCatalog._arrFileDelete == null || EquipmentManageCatalog._arrFileDelete == undefined) {
			EquipmentManageCatalog._arrFileDelete = [];
		}
		EquipmentManageCatalog._arrFileDelete.push(id);
		$('#divEquipAttachFile'+id).remove();
		if (EquipmentManageCatalog._countFile != undefined && EquipmentManageCatalog._countFile != null && EquipmentManageCatalog._countFile > 0) {
			EquipmentManageCatalog._countFile = EquipmentManageCatalog._countFile - 1;
		} else {
			EquipmentManageCatalog._countFile = 0;
		}
	},
	/**
	 * Lap bien ban thu hoi thanh ly
	 * 
	 * @author phuongvm
	 * @Since 22/12/2014
	 * */
	saveEviction : function(){
		$(".ErrorMsgStyle").html('').hide();
		var msg = "";
		var err = ""; 
		var arrDataInsert = [];
		var createDate = $('#createDate').val();
		msg = Utils.getMessageOfRequireCheck('shopCode','Mã NPP');
		err = '#shopCode';
		if(msg.length == 0){
			msg = Utils.getMessageOfSpecialCharactersValidate('shopCode','Mã NPP',Utils._CODE);
			err = '#shopCode';
		}
		if(msg.length == 0 && $('#createDate').val().trim().length > 0 && !Utils.isDate($('#createDate').val()) && $('#createDate').val() != '__/__/____' ){
			msg = 'Ngày biên bản không tồn tại hoặc không đúng định dạng dd/mm/yyyy.';
			err = '#createDate';
		}

		var curDate = ReportUtils.getCurrentDateString(); //Lay ngay hien tai sysDate
		if (msg.length == 0 && !Utils.compareDate($('#createDate').val().trim(), curDate)) {
			msg = 'Ngày biên bản không được sau ngày hiện tại. Vui lòng nhập lại';
			$('#createDate').focus();
		}

		if(msg.length == 0){
			msg = Utils.getMessageOfRequireCheck('customerCode','Mã KH');
			err = '#customerCode';
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfSpecialCharactersValidate('customerCode','Mã KH',Utils._CODE);
			err = '#customerCode';
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfRequireCheck('fromRePresentor','Người đại diện');
			err = '#fromRePresentor';
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfSpecialCharactersValidate('fromRePresentor','Người đại diện', Utils._NAME); 
			err = '#fromRePresentor';
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfRequireCheck('toRePresentor','Bên cho mượn');
			err = '#toRePresentor';
		}
		if (msg.length == 0) {
		 	msg = Utils.getMessageOfSpecialCharactersValidate('toRePresentor', 'Bên cho mượn', Utils._NAME); 
		 	err = '#toRePresentor';
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfRequireCheck('warehouse','Kho nhập thu hồi');
			err = '#warehouse';
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfSpecialCharactersValidate('warehouse','Kho nhập thu hồi',Utils._CODE);
			err = '#warehouse';
		}
		if (msg.length == 0) {
		 	msg = Utils.getMessageOfSpecialCharactersValidate('note', 'Ghi chú', Utils._NAME); 
		 	err = '#note';
		}
		// if(msg.length == 0){
		// 	if($("input[name='currentPosition']:checked").val() == '3'){
		// 		if(msg.length == 0){
		// 			msg = Utils.getMessageOfRequireCheck('newShopCode','Mã NPP mới');
		// 			err = '#newShopCode';
		// 		}
		// 		if(msg.length == 0){
		// 			msg = Utils.getMessageOfSpecialCharactersValidate('newShopCode','Mã NPP mới',Utils._CODE);
		// 			err = '#newShopCode';
		// 		}
		// 		if(msg.length == 0){
		// 			msg = Utils.getMessageOfRequireCheck('newCustomerCode','Mã KH mới');
		// 			err = '#newCustomerCode';
		// 		}
		// 		if(msg.length == 0){
		// 			msg = Utils.getMessageOfSpecialCharactersValidate('newCustomerCode','Mã KH mới',Utils._CODE);
		// 			err = '#newCustomerCode';
		// 		}
		// 	}
		// }
		// var newCus = $('#newCustomerCode').val().trim();
		var cus = $('#customerCode').val().trim();
		var shop = $('#shopCode').val().trim();
		// var newShop = $('#newShopCode').val().trim();
		// if(msg.length == 0){
		// 	if(cus.toLowerCase() == newCus.toLowerCase() && shop.toLowerCase() == newShop.toLowerCase()){
		// 		msg = "Mã KH mới không được trùng với Mã KH";
		// 		err = '#newCustomerCode';
		// 	}
		// }
		if (msg.length == 0) {
			if (EquipmentManageCatalog._mapDetail.size() == 0) {
				msg = "Chưa có thiết bị nào được chọn";
			} else {
				for (var i = 0; i< EquipmentManageCatalog._mapDetail.keyArray.length; i++) {
					var equip = EquipmentManageCatalog._mapDetail.get(EquipmentManageCatalog._mapDetail.keyArray[i]);
					if (equip.id > 0) {
						var paramsPrd = equip.id;
						arrDataInsert.push(paramsPrd);
					}
				}
			}
		}
		if (msg.trim().length == 0 && arrDataInsert.length == 0) {
			msg = "Chưa có thiết bị nào được chọn";
		}
		if (msg.trim().length > 0) {
			$('#errMsg').html(msg).show();
			return false;
		}
		var params = new Object();
		params.shopCode = $('#shopCode').val().trim();
		params.customerCode = $('#customerCode').val().trim();
		params.fromRePresentor = $('#fromRePresentor').val().trim();
		params.toRePresentor = $('#toRePresentor').val().trim();
		params.stockCode = $('#warehouse').val().trim(); // ma kho
		params.resultBB = $("#reason").val();//ly do thu hoi
		// params.viTri = $("input[name='currentPosition']:checked").val();
		// if(params.viTri == '3'){
		// 	params.newShopCode = $('#newShopCode').val().trim();
		// 	params.newCustomerCode = $('#newCustomerCode').val().trim();
		// }
		params.lstId = arrDataInsert;
		params.status = $('#status').val().trim();
		params.statusDelivery = $('#statusDelivery').val().trim();
		params.createDate = createDate;
		params.note = $('#note').val();
		if($('#idForm').val() !='' && $('#idForm').val() != '0'){
			if(idForm!=null){
				params.idForm = $('#idForm').val(); 
			}
		}
		if (EquipmentManageCatalog._arrFileDelete == undefined || EquipmentManageCatalog._arrFileDelete == null) {
			EquipmentManageCatalog._arrFileDelete = [];
		}
		params.equipAttachFileStr = EquipmentManageCatalog._arrFileDelete.join(',');
		
		var msg = "Bạn có muốn lưu biên bản thu hồi thanh lý?";
		var arrFile = DropzonePlugin._dropzoneObject.getFilesWithStatus(Dropzone.ADDED);
		if (arrFile != null && arrFile != undefined && arrFile.length > 0) {
			if (EquipmentManageCatalog._countFile == 5) {
				$('#errMsg').html("Tổng số tập tin đính kèm không được vượt quá 5").show();
				return false;
			}
			var maxLength = 5 - EquipmentManageCatalog._countFile;
			if (arrFile.length > maxLength) {
				$('#errMsg').html("Số tập tin thêm mới không được vượt quá " + maxLength).show();
				return false;
			}
			msg = UploadUtil.getMsgByFileRequireCheck(UploadUtil._typeFileRequireCheck.equipRecord);
			if (msg.trim().length > 0) {
				$('#errMsg').html(msg).show();
				return false;
			}
			$.messager.confirm('Xác nhận', 'Bạn có muốn lưu dữ liệu?', function(r){
				if (r){
					var data = JSONUtil.getSimpleObject2(params);
					UploadUtil.updateAdditionalDataForUpload(data);
					UploadUtil.startUpload('errMsg');
					return false;
				}
			});
			return false;
		}
		
		Utils.addOrSaveData(params, "/equipment-eviction-manage/saveEviction", null, 'errMsg', function(data) {
			if (data.error != undefined && !data.error) {
				$("#successMsg").html("Lưu dữ liệu thành công").show();
				setTimeout(function(){
					window.location.href = '/equipment-eviction-manage/info';
				}, 1500);				
			}else{
				if(data.errMsg!=undefined && data.errMsg!= null && data.errMsg!=''){
					$('#errMsg').html(data.errMsg).show();
				}
			}			
		}, null, null, null, msg);
	},
	/**
	 * Chon kho
	 * @author phuongvm
	 * @since 22/12/2014
	 */
	selectStock: function(shortCode){
		$('#warehouse').val(shortCode);		
		$('#easyuiPopupSearchStock').dialog('close');
	},
	exportExcel : function(notificationId){
		$('#errMsg').html('').hide();
		$.messager.confirm('Xác nhận','Bạn có muốn xuất file?',function(r){
			if(r){	
				var dataModel = new Object();
				dataModel.notificationId = notificationId;
				ReportUtils.exportReport('/adsprogram/notification/exportExcel',dataModel,'errMsg');									
				}			
		});	
 		return false;

	},
	importExcel : function(){
		//mo fancybox
		$('#errMsg').html('').hide();
		$('#easyuiPopup').show();
		$('#errExcelMsg').html('').hide();
		$('#resultExcelMsg').html('').hide();
		ReportUtils._callBackAfterImport = function(){
			if (EquipmentManageCatalog.isImportSuccess){
				 $('.easyui-dialog #btnClose').unbind('click');
	   			 $('.easyui-dialog #btnClose').bind('click',function(){
	   				 $('#grid').datagrid('reload');
	   				 $('#easyuiPopup').dialog('close');
	   			 });
	   		 }else{
	   			$('.easyui-dialog #btnClose').unbind('click');
	   			 $('.easyui-dialog #btnClose').bind('click',function(){
	   				$('#grid').datagrid('reload');
	   				 $('#easyuiPopup').dialog('close');
	   			 });
	   		 }
			$('#grid').datagrid('reload');
			ReportUtils._callBackAfterImport=null;
		};
		$('#easyuiPopup').dialog({  
	        closed: false,  
	        cache: false,  
	        modal: true,
	        onOpen: function(){
	        	$('#contentTxtDl').val('');
	        	$('#contentTxtDl').focus();
	        	$('#descriptionDl').val('');
	        	$('#fromHour').val('09').change();
	        	$('#toHour').val('09').change();
	        	$('#fromMinute').val('00').change();
	        	$('#toMinute').val('00').change();
	        	$('#excelFile').val('');
				$('#fakefilepc').val('');
	        	$('#checkLogin').attr('checked',false);
	        	$('#checkUploadDB').attr('checked',false);
	        	$('.easyui-dialog #downloadTemplate').attr('href',excel_template_path + 'promotion/Bieu_mau_thong_bao_MTB.xls');
	        	var tabindex = -1;
				$('.InputTextStyle, select, input, button , li , tr, td, th, label, ul, a, img').each(function () {
					if (this.type != 'hidden') {
						$(this).attr("tabindex", tabindex);
						tabindex -=1;
					}
				});
				tabindex = 1;
	    		 $('.easyui-dialog input,.easyui-dialog select,.easyui-dialog button').each(function () {
		    		 if (this.type != 'hidden') {
			    	     $(this).attr("tabindex", '');
						 tabindex++;
		    		 }
				 });
	        },
	        onClose:function(){
	        	$('#contentTxt').focus();
	        	var tabindex = 1;
				$('.InputTextStyle, select, input, button , li , tr, td, th, label, ul, a, img').each(function () {
					if (this.type != 'hidden') {
						$(this).attr("tabindex", tabindex);
						tabindex ++;
					}
				});
	        }
		});
		
	},
	upload : function(){ //upload trong fancybox
		$('#resultExcelMsg').html('').hide();
		$('#errExcelMsg').html('').hide();
		var msg="";
		var err="";
		var fDate = $('#fromDateDl').val().trim();	
		var content = $('#contentTxtDl').val().trim();
		var description = $('#descriptionDl').val().trim();
		if(msg.length == 0){
			msg = Utils.getMessageOfRequireCheck('contentTxtDl','Nội dung');
			err = '#contentTxtDl';
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfRequireCheck('descriptionDl','Mô tả');
			err = '#descriptionDl';
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfRequireCheck('fromDateDl','Ngày');
			err = '#fromDateDl';
		}
		if(msg.length == 0 && $('#fromDateDl').val().trim().length>0 && !Utils.isDate($('#fromDateDl').val(),'/')){
			msg = 'Ngày không tồn tại hoặc không đúng định dạng dd/mm/yyyy.';
			err = '#fromDateDl';
		}
		if(msg.length == 0) {
			msg = Utils.compareCurrentDateEx2('fromDateDl','Ngày');
			if(msg.length !=0){
				err = '#fromDateDl';
			}
		}
		if(msg.length>0){
			$('#errExcelMsg').html(msg).show();
			$(err).focus();
			setTimeout(function(){$('#errMsg').hide();},3000);
			return false;
		}
		if($('#excelFile').val() ==  ""){
			$('#errExcelMsg').html("Vui lòng chọn file Excel").show();
			return false;
		}
		var isLogin= 0;
		var isUploadDB= 0;
		if($('#checkLogin').is(':checked')){
			isLogin =1;
		}
		if($('#checkUploadDB').is(':checked')){
			isUploadDB =1;
		}
		var fH= Number($('#fromHour').val().trim()) *60 + Number($('#fromMinute').val().trim());
		var tH= Number($('#toHour').val().trim()) *60 + Number($('#toMinute').val().trim());
		if(fH>tH){
			$('#errExcelMsg').html("Từ giờ phải nhỏ hơn hoặc bằng đến giờ.").show();
			return false;
		}
		var fromH = $('#fromHour').val().trim()+':'+$('#fromMinute').val().trim();
		var toH = $('#toHour').val().trim()+':'+$('#toMinute').val().trim();
		fDate = $('#fromDateDl').val().trim();	
		var options = { 
				beforeSubmit: ReportUtils.beforeImportExcel,   
		 		success:      EquipmentManageCatalog.afterImportExcel, 
		 		type: "POST",
		 		dataType: 'html',
		 		data:({isUploadDB:isUploadDB,isLogin:isLogin,fromDate:fDate,content:content,description:description,fromH:fromH,toH:toH})
		 	}; 
		$('#easyuiPopup #importFrm').ajaxForm(options);
		$.messager.confirm('Xác nhận','Bạn có muốn nhập từ file?',function(r){
			if(r){				
				$('#easyuiPopup #importFrm').submit();
			}
		});		
 		return false;
	},
	afterImportExcel: function(responseText, statusText, xhr, $form){		
		$('#divOverlay').hide();
		if(('#staffGrid').length>0){
			$('#staffGrid').datagrid('reload');
		}
		if (statusText == 'success') {				
	    	$("#responseDiv").html(responseText);
	    	updateTokenImport();
	    	if($('#errorExcel').html().trim() == 'true' || $('#errorExcelMsg').html().trim().length > 0){
	    		$('.panel-body #errExcelMsg').html($('#errorExcelMsg').html()).show();
	    	} else {
    			var totalRow = parseInt($('#totalRow').html().trim());
    			var numFail = parseInt($('#numFail').html().trim());
    			var fileNameFail = $('#fileNameFail').html();
    			var mes = 'Import không thành công do có '+numFail+' dòng lỗi';	    		
    			if(numFail > 0){
    				mes+= ' <a href="'+ fileNameFail +'">Xem chi tiết lỗi</a>';
    			}else{
    				mes = 'Lưu dữ liệu thành công';
    			}
    			if(numFail==0){
    				$('#resultExcelMsg').removeClass('ErrorMsgStyle SpriteErr').addClass('SuccessMsgStyle');
    				EquipmentManageCatalog.isImportSuccess = true;
    				$('#easyuiPopup').window('close');
    				$('#successMsg').html(mes).show();
    				EquipmentManageCatalog.search();
    				setTimeout(function(){
    					$('#successMsg').html('').hide();
					}, 3000);
    			}else{
    				$('#resultExcelMsg').removeClass('SuccessMsgStyle').addClass('ErrorMsgStyle SpriteErr');
    				if(numFail != totalRow) EquipmentManageCatalog.isImportSuccess = true;
    			}
    			if($('#excelFile').length!=0 && $('#fakefilepc').length!=0){//xóa link khi import
    				try{
    					$('#excelFile').val('');
    					$('#fakefilepc').val('');
    				}catch(err){}
    			}
    			
    			$('#resultExcelMsg').html(mes).show();
    			if(ReportUtils._callBackAfterImport != null){
    				ReportUtils._callBackAfterImport.call(this);
	    		}
	    	}
	    }	
	},
	//phuongvm thu hoi va thanh ly
	//phuongvm quan ly danh muc
	showDlgCatalogImportExcel: function(){
		$('#easyuiPopupImportExcel').show();
		$('.ErrorMsgStyle').hide();
		$('#easyuiPopupImportExcel').dialog({  
	        closed: false,  
	        cache: false,  
	        modal: true,
	        onOpen: function(){	 
	        	$('.easyui-dialog .BtnGeneralStyle').css('color','');
	        },
	        onClose: function(){
	        	EquipmentManageCatalog.searchinfo();
				$("#grid").datagrid('uncheckAll');
				$("#fakefilepcCatalog").val("");
				$("#excelFileCatalog").val("");				
	        }
		});
	},
	importExcelCatalog: function(){
		$('.ErrorMsgStyle').hide();
		VTUtilJS.importExcelUtils(function(data) {
			$("#errExcelMsgCatalog").html(data.message).show();		
			if(data.fileNameFail == null || data.fileNameFail == "" ){					
				setTimeout(function(){
					$('#errExcelMsgCatalog').html("").hide();
					$('#easyuiPopupImportExcel').dialog('close');
				}, 3000);				
			}else{
				$("#fakefilepcCatalog").val("");
				$("#excelFileCatalog").val("");			
				$('#grid').datagrid('reload');	
			}						
		}, "importFrmCatalog", "excelFileCatalog", null, "errExcelMsgCatalog");
		return false;
	},
	/**
	 * xuat file excel 
	 * @author phuongvm
	 * @since 16/03/2015
	 */
	exportExcel: function(){
		$('#errMsg').html('').hide();
		var msg ='';
		var err = '';
		if(msg.length == 0){
			msg = Utils.getMessageOfSpecialCharactersValidate('code','Mã');
			err = 'code';
		}
		if(msg.length > 0){
			$('#errMsg').html(msg).show();
			$('#'+err).focus();
			return false;
		}
		
		var params=new Object(); 
		params.code = $('#code').val().trim();
		params.name = $('#name').val().trim();
		params.type = $('#type').val().trim();
		params.status = $('#status').val().trim();
		
		$.messager.confirm('Xác nhận','Bạn có muốn xuất file excel?',function(r){
			if(r){
				ReportUtils.exportReport('/equipment-manage-catalog/export-excel', params, 'errMsg');					
			}
		});		
		return false;
	}
};
/**
 * Thiet lap dinh muc hang muc
 * 
 * @author liemtpt
 * @since 30/3/2015
 * 
 * @author hunglm16
 * @since May 07,2015
 * @description Ra soat va hotfix
 * */
var EquipItemConfig = {
		/**
		 * Khai bao thuoc tinh Bien Toan Cuc
		 * 
		 * */
		_listEquipmentDg: null,
		_mapData: null,
		_params: new Object(),
		_equipChange: null,
		_countFile: 0,
		_arrFileDelete: null,
		_equipItemConfig: null,
		_mapRowDetailChange: null,
		
		/**
		 * Tim kiem danh sach dinh muc hang muc
		 * @author liemtpt
		 * @since 07/04/2015
		 * 
		 * @author hunglm16
		 * @since 07/05/2015
		 * @description bo sung validate tim kiem
		 * */
		search: function () {
			$('.SuccessMsgStyle').html('').hide();
			$('.ErrorMsgStyle').html('').hide();
			var fromCapacity= $('#txtFromCapacity').val().trim().replace(/,/g, '');
			var toCapacity = $('#txtToCapacity').val().trim().replace(/,/g, '');
			var status = $('#cbxStatus').val().trim();
			var msg = '';
			if (isNaN(fromCapacity)) {
				$('#txtFromCapacity').focus();
				msg = 'Dung tích từ phải là kiểu số nguyên';
			}
			if (msg.length == 0 && isNaN(toCapacity)) {
				$('#txtToCapacity').focus();
				msg = 'Đến phải là kiểu số nguyên';
			}
			if (msg.trim().length > 0) {
				$('#errMsg').html(msg.trim()).show();
				$('#grid').datagrid("loadData", []);
				return;
			}
			EquipItemConfig._params = {
				fromCapacity: fromCapacity
				,toCapacity: toCapacity
				,status: status
			};
			$('#grid').datagrid("load", EquipItemConfig._params);
		},
		
		/**
		 * dialog add or update equip item
		 * @author liemtpt
		 * @since 10/04/2015
		 * 
		 * @author hunglm16
		 * @since 10/05/2015
		 * @description Ra soat va update
		 */
		dialogAddOrUpdateEquipmentItem: function(id, index){
			$(".ErrorMsgStyle").html('').hide();
			$(".SuccessMsgStyle").html('').hide();
			$('#fromCapacity').val("");				    			
			$('#toCapacity').val("");
			var titleView = 'Thêm mới định mức';
			var row = $('#grid').datagrid('getRows')[index];
	    	if (row != undefined && row != null && id == row.id) {
	    		EquipItemConfig._equipItemConfig = {
	    				id: row.id,
	    				status: row.status,
	    				equipItemConfigCode: row.equipItemConfigCode,
	    				toCapacity: row.toCapacity,
	    				fromCapacity: row.fromCapacity
	    		};
	    		titleView = 'Chỉnh sửa định mức ' + row.equipItemConfigCode;
	    		
	    	} else {
	    		EquipItemConfig._equipItemConfig = null;
	    	}
			$('#dialogEquipItemConfig').dialog({
				title: titleView,
				closed: false,
				cache: false,
				modal: true,
		        width: 460,
		        height: 'auto',
		        onOpen: function(){
		        	$('.easyui-dialog .InputTextStyle').val('');
		    		$('#dialogEquipItemConfig .MySelectBoxClass').css('width', '173');
		    		$('#dialogEquipItemConfig .CustomStyleSelectBox').css('width', '140');
		    		/** truong hop chinh sua*/
		    		if(!isNaN(id) && !isNaN(index) && Number(id) > 0 && Number(index) >= 0){
				    	$('#fromCapacity').focus();
				    	if (row.fromCapacity != undefined && row.fromCapacity != null) {
			    			$('#fromCapacity').val(row.fromCapacity);				    			
			    		}
			    		if (row.toCapacity != undefined && row.toCapacity != null) {
			    			$('#toCapacity').val(row.toCapacity);				    			
			    		}
		    		} 
		    		$('#fromCapacity').focus();
		    		$('#dialogEquipItemConfig #btnSaveEquipItemConfig').unbind('click');
		    		$('#dialogEquipItemConfig #btnSaveEquipItemConfig').bind('click', function(event) {
		    			EquipItemConfig.saveOrUpdateEquipItemConfig(id);
					});
		        },
		        onClose:function() {
		        	$(".ErrorMsgStyle").html('').hide();
					$(".SuccessMsgStyle").html('').hide();
					$('#fromCapacity').val("");				    			
					$('#toCapacity').val("");			    			
					EquipItemConfig._equipItemConfig = null;
		        }
			});
		},
		/**
		 * Them moi hay cap nhat dinh muc
		 * 
		 * @author liemtpt
		 * @since 11/04/2015
		 * 
		 * @author hunglm16
		 * @since May 07, 2015
		 * @description Ra soat va hotfix
		 * */
		saveOrUpdateEquipItemConfig : function (id) {
			$(".ErrorMsgStyle").html('').hide();
			$(".SuccessMsgStyle").html('').hide();
			var msg ="";
			var fromCapacity = $('#fromCapacity').val().trim().replace(/,/g,'');
			var toCapacity = $('#toCapacity').val().trim().replace(/,/g,'');
			if (isNaN(fromCapacity)) {
				$('#fromCapacity').focus();
				msg = 'Dung tích từ phải là kiểu số nguyên';
			}
			if (msg.length == 0 && isNaN(toCapacity)) {
				$('#toCapacity').focus();
				msg = 'Đến phải là kiểu số nguyên';
			}
			if (msg.length == 0 && fromCapacity.length == 0 && toCapacity.length == 0) {
				$('#fromCapacity').focus();
				msg = 'Dung tích từ và Đến phải ít nhất có một giá trị được nhập';
			} 
			if (msg.length == 0 && fromCapacity.length > 0 && toCapacity.length > 0 && Number(toCapacity) < Number(fromCapacity)) {
				$('#fromCapacity').focus();
				msg = 'Dung tích từ phải nhỏ hơn hoặc bằng Đến';
			}
			if (msg.length == 0 && !isNaN(id) && Number(id) > 0 && EquipItemConfig._equipItemConfig != null && EquipItemConfig._equipItemConfig.id == id) {
				if (EquipItemConfig._equipItemConfig.fromCapacity == Number(fromCapacity) && EquipItemConfig._equipItemConfig.toCapacity == Number(toCapacity)) {
					msg = 'Không thấy có giá trị nào được thay đổi';
				}
			}
			if (msg.trim().length > 0) {
				$('#errorMsgDl').html(msg).show();
				return false;
			}
			var msgConfirm = 'Bạn có muốn thêm mới định mức?';
			var params = new Object();	
			if (!isNaN(id) && Number(id) > 0) {
				params.id = id;
				msgConfirm = 'Bạn có muốn Cập nhật định mức?';
			}
			params.fromCapacity = $('#fromCapacity').val().trim();
			params.toCapacity = $('#toCapacity').val().trim();
//			params.status = $('#cbxStatusDl').val().trim();
			
			Utils.addOrSaveData(params, '/equip-item-config/save-equip-item-config', null,  'errorMsgDl', function(data) {
				$('#successMsgDl').html("Lưu dữ liệu thành công").show();
				$('#grid').datagrid('reload');
				var tm = setTimeout(function(){
					$('#dialogEquipItemConfig').dialog('close');
					$('.SuccessMsgStyle').html("").hide();
					clearTimeout(tm);
				 }, 1500);
			}, null, null, null, msgConfirm);
			return false;
		},
		
		/**
		 * Xoa dinh muc
		 * 
		 * @author liemtpt
		 * @since 13/04/2015
		 * */
		removedEquipmentItemConfig: function(id, code) {
			var msg = 'Bạn có muốn Xóa Định mức ' + code;
			$.messager.confirm('Xác nhận', msg, function(r){
	  			if (r){
	  				var params = new Object();
	  				params.id = id;
	  				Utils.saveData(params,'/equip-item-config/removed-equip-item-config', null, null, function(){
	  					$('#grid').datagrid('reload');			
	  				});
	  			}
	  		});
			return false;
		},
		/**
		 * Xoa dinh muc
		 * 
		 * @author hunglm16
		 * @since May 07,2015
		 * */
		stopEquipmentItemConfig: function(id, code) {
			var msg = 'Bạn có muốn Tạm ngưng Định mức ' + code;
			$.messager.confirm('Xác nhận', msg, function(r){
	  			if (r){
	  				var params = new Object();
	  				params.id = id;
	  				Utils.saveData(params,'/equip-item-config/stop-equip-item-config', null, null, function(){
	  					$('#grid').datagrid('reload');			
	  				});
	  			}
	  		});
			return false;
		},
		/**
		 * edit equip item config detail 
		 * 
		 * @author liemtpt
		 * @since 13/04/2015
		 * @return
		 * @description chinh sua du lieu row
		 * */
		editEquipItemConfigDtl: function(key, idx, indexRows){
			var idDiv = idx + '_' + indexRows;
			$('#dg_chidrent_edit_'+idDiv).show();
			$('#dg_chidrent_save_'+idDiv).show();
			$('#dg_chidrent_back_'+idDiv).show();
			var ddv = $('#grid').datagrid('getRowDetail', indexRows).find('table.ddv');
			var mapKeyId = indexRows.toString() +'_'+ idx.toString();
			var rowTmp = EquipItemConfig._mapRowDetailChange.get(mapKeyId);
			if (rowTmp == null || rowTmp == undefined) {
				EquipItemConfig._mapRowDetailChange.put(mapKeyId, jQuery.extend(true, {}, mapKeyId, ddv.datagrid('getRows')[idx]));			
			}
			ddv.datagrid('beginEdit', idx);
			EquipItemConfig.bindEventInput();
		},
		/**
		 * edit equip item config detail 
		 * 
		 * @author liemtpt
		 * @since 13/04/2015
		 * @return
		 * @description chinh sua du lieu row
		 * */
		editAllEquipItemConfigDtl: function(indexRows){
			var ddv = $('#grid').datagrid('getRowDetail', indexRows).find('table.ddv');
			var rows = ddv.datagrid('getRows');
			for(var i = 0; i < rows.length; i++){
				ddv.datagrid('beginEdit',i);
			}
			 EquipItemConfig.bindEventInput();
			$('.child_edit_all').show();
			$('.child_save_all').show();
			$('.child_back_all').show();
			$('.child_edit').hide();
			$('.child_save').hide();
			$('.child_back').hide();
		},
		/**
		 * back equip item config detail 
		 * 
		 * @author liemtpt
		 * @since 14/04/2015
		 * @return
		 * @description tro lai
		 * */
		backEquipItemConfigDtl: function(key, idx, indexRows){
			var ddv = $('#grid').datagrid('getRowDetail', indexRows).find('table.ddv');
			var mapKeyId = indexRows.toString() +'_'+ idx.toString();
			var rowTmp = EquipItemConfig._mapRowDetailChange.get(mapKeyId);
			$(ddv).datagrid('updateRow',{
				index: idx,
				row: rowTmp
			});
			ddv.datagrid('endEdit', idx);
			var divKey = idx + '_' + indexRows;
			$('#dg_chidrent_edit_'+divKey).show();
			$('#dg_chidrent_save_'+divKey).hide();
			$('#dg_chidrent_back_'+divKey).hide();
		},
		/**
		 * back all equip item config detail 
		 * 
		 * @author liemtpt
		 * @since 13/04/2015
		 * @return
		 * @description chinh sua tat ca du lieu sub grid
		 * */
		backAllEquipItemConfigDtl: function(indexRows){
			var ddv = $('#grid').datagrid('getRowDetail', indexRows).find('table.ddv');
			$(ddv).datagrid('reload');
			/*var rows = ddv.datagrid('getRows');
			for(var i = 0; i < rows.length; i++){
				ddv.datagrid('endEdit',i);
			}
			$('.child_edit_all').show();
			$('.child_save_all').hide();
			$('.child_back_all').hide();
			$('.child_edit').show();
			$('.child_save').hide();
			$('.child_back').hide();
			*/
		},
		
		/**
		 * cap nhat gia tri cho tat ca cac hang muc trong dinh muc
		 * 
		 * @author liemtpt
		 * @since 11/04/2015
		 * 
		 * @author hunglm16
		 * @since 08/05/2015
		 * @description ra soat va update
		 * */
		saveAllEquipItemConfigDtl: function (groupId, indexRows) {
			$(".ErrorMsgStyle").html('').hide();
			$(".SuccessMsgStyle").html('').hide();
			var obj = new Object();
			var lstObj = new Array();
			var ddv = $('#grid').datagrid('getRowDetail', indexRows).find('table.ddv');
			$(ddv).datagrid('acceptChanges');
			var rows = $(ddv).datagrid('getRows');
			$('.child_edit').hide();
			$('.child_save').hide();
			$('.child_back').hide();
			var flagException = false;
			for (var i = 0, size = rows.length; i < size; i++) {
				flagException = false;
				var row = rows[i];
				var msg = '';
				if (row.fromMaterialPrice != null && isNaN(row.fromMaterialPrice.toString().trim().replace(/,/g, ''))) {
					msg = row.equipItemCode.trim() + ' phải có Đơn giá vật tư từ là kiểu số nguyên';
				}
				if (row.fromMaterialPrice != null && row.fromMaterialPrice.toString().trim().length > 0 && Number(row.fromMaterialPrice.toString().trim().replace(/,/g, '')) < 1) {
					msg = row.equipItemCode.trim() + ' phải có Đơn giá vật tư từ là kiểu số nguyên';
				}
				if (msg.length == 0 && row.toMaterialPrice != null && isNaN(row.toMaterialPrice.toString().trim().replace(/,/g, ''))) {
					msg = row.equipItemCode.trim() + ' phải có Đơn giá vật tư đến là kiểu số nguyên';
				}
				if (msg.length == 0 && row.toMaterialPrice != null && row.toMaterialPrice.toString().trim().length > 0 && Number(row.toMaterialPrice.toString().trim().replace(/,/g, '')) < 1) {
					msg = row.equipItemCode.trim() + ' phải có Đơn giá vật tư đến là kiểu số nguyên';
				}
				if (row.fromMaterialPrice != null && !isNaN(row.fromMaterialPrice.toString().trim().replace(/,/g, ''))) {
					if (msg.length == 0 && row.toMaterialPrice != null && !isNaN(row.toMaterialPrice.toString().trim().replace(/,/g, ''))) {
						if (Number(row.fromMaterialPrice.toString().trim().replace(/,/g, '')) > 0 && Number(row.toMaterialPrice.toString().trim().replace(/,/g, '')) && Number(row.fromMaterialPrice.toString().trim().replace(/,/g, '')) > Number(row.toMaterialPrice.toString().trim().replace(/,/g, ''))) {
							msg = row.equipItemCode.trim() + ' phải có Đơn giá vật tư từ nhỏ hơn hoặc bằng Đến';
						}
					}
				}
				if ((row.fromMaterialPrice == null || row.fromMaterialPrice.toString().trim().length == 0) && (row.toMaterialPrice == null || row.toMaterialPrice.toString().trim().length == 0)) {
					msg = row.equipItemCode.trim() + ' phải có Đơn giá vật tư từ hoặc bằng Đến có giá trị';
				}
				if (msg.length == 0 && row.fromWorkerPrice != null && isNaN(row.fromWorkerPrice.toString().trim().replace(/,/g, ''))) {
					msg = row.equipItemCode.trim() + ' phải có Đơn giá nhân công từ là kiểu số nguyên';
				}
				if (msg.length == 0 && row.fromWorkerPrice != null && row.fromWorkerPrice.toString().trim().length > 0 && Number(row.fromWorkerPrice.toString().trim().replace(/,/g, '')) < 1) {
					msg = row.equipItemCode.trim() + ' phải có Đơn giá nhân công từ là kiểu số nguyên';
				}
				if (msg.length == 0 && row.toWorkerPrice != null && isNaN(row.toWorkerPrice.toString().trim().replace(/,/g, ''))) {
					msg = row.equipItemCode.trim() + ' phải có Đơn giá nhân công đến là kiểu số nguyên';
				}
				if (msg.length == 0 && row.toWorkerPrice != null && row.toWorkerPrice.toString().trim().length > 0 && Number(row.toWorkerPrice.toString().trim().replace(/,/g, '')) < 1) {
					msg = row.equipItemCode.trim() + ' phải có Đơn giá nhân công đến là kiểu số nguyên';
				}
				if (row.fromWorkerPrice != null && !isNaN(row.toWorkerPrice.toString().trim().replace(/,/g, ''))) {
					if (msg.length == 0 && row.toMaterialPrice != null && !isNaN(row.toWorkerPrice.toString().trim().replace(/,/g, ''))) {
						if (Number(row.fromWorkerPrice.toString().trim().replace(/,/g, '')) > 0 && Number(row.toWorkerPrice.toString().trim().replace(/,/g, '')) && Number(row.fromWorkerPrice.toString().trim().replace(/,/g, '')) > Number(row.toWorkerPrice.toString().trim().replace(/,/g, ''))) {
							msg = row.equipItemCode.trim() + ' phải có Đơn giá nhân công từ nhỏ hơn hoặc bằng Đến';
						}
					}
				}
				if ((row.fromWorkerPrice == null || row.fromWorkerPrice.toString().trim().length == 0) && (row.toWorkerPrice == null || row.toWorkerPrice.toString().trim().length == 0)) {
					msg = row.equipItemCode.trim() + ' phải có Đơn giá nhân công từ hoặc bằng Đến có giá trị';
				}
				
				if (row.id == undefined || row.id == null || row.id < 0) {
					if (row.fromWorkerPrice == null || row.fromWorkerPrice == undefined || row.fromWorkerPrice.toString().trim().length == 0) {
						if (row.fromMaterialPrice == null || row.fromMaterialPrice == undefined || row.fromMaterialPrice.toString().trim().length == 0) {
							if (row.toWorkerPrice == null || row.toWorkerPrice == undefined || row.toWorkerPrice.toString().trim().length == 0) {
								if (row.toMaterialPrice == null || row.toMaterialPrice == undefined || row.toMaterialPrice.toString().trim().length == 0) {
									msg = '';
									flagException = true;
								}
							}
						}
					}
				}
				
				if (msg.trim().length > 0) {
					$('#errMsg').html(msg.trim()).show();
					$(ddv).datagrid('beginEdit', i);
					return;
				}
				if (!flagException) {
					obj = new Object();
					obj.equipItemId = row.equipItemId;
					obj.fromMaterialPrice = Utils.returnMoneyValue(row.fromMaterialPrice);
					obj.toMaterialPrice = Utils.returnMoneyValue(row.toMaterialPrice);
					obj.fromWorkerPrice = Utils.returnMoneyValue(row.fromWorkerPrice);
					obj.toWorkerPrice = Utils.returnMoneyValue(row.toWorkerPrice);
					lstObj.push(obj);					
				}
			}
			if (lstObj.length == 0) {
				$('#errMsg').html('Không tìm thấy Hạng mục muốn cập nhật cho định mức' + $('#grid').datagrid('getRows')[indexRows].equipItemConfigCode).show();
				if (rows.length > 0) {
					$(ddv).datagrid('beginEdit', 0);				
				}
				return;
			}
			var params = new Object();	
			if(groupId != undefined && groupId != null){
				params.id = groupId;
			}
			params.lstEquipItemConfigDtlVO = lstObj;
			params.flagCheckAll = 1;
			var dataModel = {};
			convertToSimpleObject(dataModel, params, 'equipItemConfigVO');
			Utils.addOrSaveData(dataModel, '/equip-item-config/save-equip-item-config-dtl', null,  null, function(data) {
					ddv.datagrid('reload');
					$('.SuccessMsgStyle').html("").hide();
			}, null, null, null,null);
			return false;
		},
		/**
		 * Them moi hang muc trong dinh muc
		 * 
		 * @author liemtpt
		 * @since 11/04/2015
		 * 
		 * @author hunglm16
		 * @since 07/05/2015
		 * @description Ra soat va hotfix
		 * */
		saveEquipItemConfigDtl : function (groupId, idx, indexRows) {
			$(".ErrorMsgStyle").html('').hide();
			$(".SuccessMsgStyle").html('').hide();
			var obj = new Object();
			var lstObj = new Array();
			var ddv = $('#grid').datagrid('getRowDetail', indexRows).find('table.ddv');
			$(ddv).datagrid('acceptChanges');
			var row = ddv.datagrid('getRows')[idx];
			var msg = '';
			if (row.fromMaterialPrice != null && isNaN(row.fromMaterialPrice.toString().trim().replace(/,/g, ''))) {
				msg = row.equipItemCode.trim() + ' phải có Đơn giá vật tư từ là kiểu số nguyên';
			}
			if (row.fromMaterialPrice != null && row.fromMaterialPrice.toString().trim().length > 0 && Number(row.fromMaterialPrice.toString().trim().replace(/,/g, '')) < 1) {
				msg = row.equipItemCode.trim() + ' phải có Đơn giá vật tư từ là kiểu số nguyên';
			}
			if (msg.length == 0 && row.toMaterialPrice != null && isNaN(row.toMaterialPrice.toString().trim().replace(/,/g, ''))) {
				msg = row.equipItemCode.trim() + ' phải có Đơn giá vật tư đến là kiểu số nguyên';
			}
			if (msg.length == 0 && row.toMaterialPrice != null && row.toMaterialPrice.toString().trim().length > 0 && Number(row.toMaterialPrice.toString().trim().replace(/,/g, '')) < 1) {
				msg = row.equipItemCode.trim() + ' phải có Đơn giá vật tư đến là kiểu số nguyên';
			}
			if (row.fromMaterialPrice != null && !isNaN(row.fromMaterialPrice.toString().trim().replace(/,/g, ''))) {
				if (msg.length == 0 && row.toMaterialPrice != null && !isNaN(row.toMaterialPrice.toString().trim().replace(/,/g, ''))) {
					if (Number(row.fromMaterialPrice.toString().trim().replace(/,/g, '')) > 0 && Number(row.toMaterialPrice.toString().trim().replace(/,/g, '')) && Number(row.fromMaterialPrice.toString().trim().replace(/,/g, '')) > Number(row.toMaterialPrice.toString().trim().replace(/,/g, ''))) {
						msg = row.equipItemCode.trim() + ' phải có Đơn giá vật tư từ nhỏ hơn hoặc bằng Đến';
					}
				}
			}
			if ((row.fromMaterialPrice == null || row.fromMaterialPrice.toString().trim().length == 0) && (row.toMaterialPrice == null || row.toMaterialPrice.toString().trim().length == 0)) {
				msg = row.equipItemCode.trim() + ' phải có Đơn giá vật tư từ hoặc bằng Đến có giá trị';
			}
			if (msg.length == 0 && row.fromWorkerPrice != null && isNaN(row.fromWorkerPrice.toString().trim().replace(/,/g, ''))) {
				msg = row.equipItemCode.trim() + ' phải có Đơn giá nhân công từ là kiểu số nguyên';
			}
			if (msg.length == 0 && row.fromWorkerPrice != null && row.fromWorkerPrice.toString().trim().length > 0 && Number(row.fromWorkerPrice.toString().trim().replace(/,/g, '')) < 1) {
				msg = row.equipItemCode.trim() + ' phải có Đơn giá nhân công từ là kiểu số nguyên';
			}
			if (msg.length == 0 && row.toWorkerPrice != null && isNaN(row.toWorkerPrice.toString().trim().replace(/,/g, ''))) {
				msg = row.equipItemCode.trim() + ' phải có Đơn giá nhân công đến là kiểu số nguyên';
			}
			if (msg.length == 0 && row.toWorkerPrice != null && row.toWorkerPrice.toString().trim().length > 0 && Number(row.toWorkerPrice.toString().trim().replace(/,/g, '')) < 1) {
				msg = row.equipItemCode.trim() + ' phải có Đơn giá nhân công đến là kiểu số nguyên';
			}
			if (row.fromWorkerPrice != null && !isNaN(row.toWorkerPrice.toString().trim().replace(/,/g, ''))) {
				if (msg.length == 0 && row.toMaterialPrice != null && !isNaN(row.toWorkerPrice.toString().trim().replace(/,/g, ''))) {
					if (Number(row.fromWorkerPrice.toString().trim().replace(/,/g, '')) > 0 && Number(row.toWorkerPrice.toString().trim().replace(/,/g, '')) && Number(row.fromWorkerPrice.toString().trim().replace(/,/g, '')) > Number(row.toWorkerPrice.toString().trim().replace(/,/g, ''))) {
						msg = row.equipItemCode.trim() + ' phải có Đơn giá nhân công từ nhỏ hơn hoặc bằng Đến';
					}
				}
			}
			if ((row.fromWorkerPrice == null || row.fromWorkerPrice.toString().trim().length == 0) && (row.toWorkerPrice == null || row.toWorkerPrice.toString().trim().length == 0)) {
				msg = row.equipItemCode.trim() + ' phải có Đơn giá nhân công từ hoặc bằng Đến có giá trị';
			}
			if (msg.trim().length > 0) {
				$('#errMsg').html(msg.trim()).show();
				ddv.datagrid('beginEdit', idx);
				return;
			}
			var key = idx + 1;
//			$('#dg_chidrent_edit_'+key).hide();
			obj = new Object();
			obj.equipItemId = row.equipItemId;
			obj.fromMaterialPrice = Utils.returnMoneyValue(row.fromMaterialPrice);
			obj.toMaterialPrice = Utils.returnMoneyValue(row.toMaterialPrice);
			obj.fromWorkerPrice = Utils.returnMoneyValue(row.fromWorkerPrice);
			obj.toWorkerPrice = Utils.returnMoneyValue(row.toWorkerPrice);
			lstObj.push(obj);
			var params = new Object();	
			if(groupId != undefined && groupId != null){
				params.id = groupId;
			}
			params.lstEquipItemConfigDtlVO = lstObj;
			params.flagCheckAll = 0;
			var dataModel = {};
			convertToSimpleObject(dataModel, params, 'equipItemConfigVO');
			Utils.addOrSaveData(dataModel, '/equip-item-config/save-equip-item-config-dtl', null,  null, function(data) {
					ddv.datagrid('reload');
					$('.SuccessMsgStyle').html("").hide();
			}, null, null, null, null);
			
			return false;
		},
		bindEventInput:function(){
            $('td input').each(function(i,e){
                  $(this).attr('id','inputText'+i).attr('maxlength','22').css('text-align','right');
                  Utils.bindFormatOnTextfield('inputText'+i, Utils._TF_NUMBER_DOT);
                  Utils.formatCurrencyFor('inputText'+i);
            });
            $('td input').keypress(function() {
            	Utils.onchangeFormatCurrencyKeyPress(this, 17);
            });
      },
     
     /**
  	 * Xuat excel Thiet lap dinh muc hang muc
  	 * 
  	 * @author liemtpt
  	 * @since 16/03/2015 
  	 * 
  	 * @author hunglm16
  	 * @since May 05,2015
  	 */
      exportBySearchEquipItemConfig : function() {
  		$('.SuccessMsgStyle').html('').hide();
		$('.ErrorMsgStyle').html('').hide();
		var rows = $('#grid').datagrid('getRows');
		if (rows.length == 0) {
			$('#errMsg').html('Không có dữ liệu').show();
			return;
		}
		if (EquipItemConfig._params == undefined || EquipItemConfig._params == null) {
			var fromCapacity= $('#txtFromCapacity').val().trim();
			var toCapacity = $('#txtToCapacity').val().trim();
			var status = $('#cbxStatus').val().trim();
			
			EquipItemConfig._params = {
				fromCapacity: fromCapacity,
				toCapacity: toCapacity,
				status: status,
				reportCode: $('#function_code').val()
			};
		} else {
			EquipItemConfig._params.reportCode = $('#function_code').val();
		}
  		var msg = 'Bạn có muốn xuất Excel?';
  		$.messager.confirm('Xác nhận', msg, function(r){
  			if (r){
  				ReportUtils.exportReport('/equip-item-config/exportBySearchEquipItemConfig',EquipItemConfig._params, 'errMsg');
  			}
  		});
  		
  	},
  	/**
	 * Mo dialog nhap excel thiet lap dinh muc
	 * 
	 * @author liemtpt
	 * @since 16/04/2015
	 * */
  	openDialogImportEquipItemConfig : function (){
		$('.SuccessMsgStyle').html('').hide();
		$('.ErrorMsgStyle').html('').hide();
		$('#eqItem_import_easyuiPopupImportExcel').dialog({
			title: 'Nhập excel thiết lập định mức',
			width: 465, 
			height: 'auto',
			closed: false,
			cache: false,
			modal: true,
			onOpen: function() {
				$('#fakefileExcelpc').val('');
				$('#excelFile').val('');
			},
			onClose: function() {
				$('.ErrorMsgStyle').html('').hide();
				$('.ErrorMsgStyle').html('').hide();
			}
		});
	},
	/**
	 * Import excel thiet lap dinh muc hang muc
	 * 
	 * @author liemtpt
	 * @since 16/04/2015
	 * 
	 * @author hunglm16
	 * @since May 05,2015
	 * @description Ra soat va update
	 * */
	importEquipItemConfig : function(){
		$('.ErrorMsgStyle').html('').hide();
		$('.ErrorMsgStyle').html('').hide();
		Utils.importExcelUtils(function(data) {
			EquipItemConfig.search();//Refesh lai luong tim kiem
			$('#fakefileExcelpc').val("").change();
			if (data.message != undefined && data.message != null && data.message.trim().length >0) {
				$('#errImpExcelMsg').html(data.message.trim()).change().show();
			} else {
				$('#successImpExcelMsg').html("Lưu dữ liệu thành công").show();
				var tm = setTimeout(function() {
					$('.SuccessMsgStyle').html('').hide();
					clearTimeout(tm);
					$('#eqItem_import_easyuiPopupImportExcel').dialog('close');
				 }, 1500);
			}
		}, 'importExcelFrm', 'excelFile', 'errImpExcelMsg');
	},
	
     /**
  	 * Tai tap tin excel import thiet lap dinh muc hang muc
  	 * 
  	 * @author liemtpt
  	 * @since 16/04/2015
  	 * 
  	 * @author hunglm16
  	 * @since May 04,2015
  	 * @description thay doi phuong thuc xuat tap tin excel
  	 * */
  	downloadTemplateImportEquipItemConfig: function() {
  		$('.SuccessMsgStyle').html('').hide();
  		$('.ErrorMsgStyle').html('').hide();
  		var msg = 'Bạn có muốn tải tập tin Import?';
  		$.messager.confirm('Xác nhận', msg, function(r) {
			if (r){
		  		var dataModel = {};
		  		dataModel.reportCode = $('#function_code').val();
		  		ReportUtils.exportReport('/equip-item-config/downloadTemplateImportEquipItemConfig', dataModel);
			}
		});
  	},
	/**
	 * Xu ly su kien Change Input Dung tich
	 * 
	 * @author hunglm16
  	 * @since May 09,2015
	 * */
	onchangeCapacity: function(txt) {
		var kq = $(txt).val();
		kq = kq.replace(/,/g, '').trim();
		var len = kq.length;
		if (len > 10) {
			kq = kq.substring(len - 10);
			$(txt).val(VTUtilJS.formatCurrency(kq)).change();
		}
		return;
	}
};
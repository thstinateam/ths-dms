/**
 * JS thuc hien nghiep vu Quan Ly De Nghi Thu Hoi Thiet Bi
 *
 * @author nhutnn
 * @since 13/07/2015
 * */
var EquipmentSuggestEviction = {
	_mapRecored: null,
	_editIndex: null,
	_lstEquip: null,
	_lstStaff: null,
	_mapEviction: null,

	/**
	 * Tim kiem danh sach de nghi thu hoi
	 *
	 * @author nhutnn
	 * @since 13/07/2015
	 * */
	searchSuggestEviction: function() {
		$('.ErrorMsgStyle').html("").hide();
		var msg = '';
		var params = new Object();
		var multiShop = $("#shop").data("kendoMultiSelect");
		var dataShop = multiShop.dataItems();
		if (dataShop != null && dataShop.length > 0) {
			var lstShop = new Array();
			//var curShopCode = $('#curShopCode').val();
			for (var i = 0; i < dataShop.length; i++) {
				//if (dataShop[i].shopCode != curShopCode) {
					lstShop.push(dataShop[i].shopCode);
				//}
			}
			params.lstShop = lstShop.join(",");
		} else {
			msg = "Bạn chưa chọn Đơn vị!";
			err = '#shop';
		}
		if (msg.length == 0) {
			msg = Utils.getMessageOfSpecialCharactersValidate('code', 'Mã biên bản', Utils._CODE);
			err = "#code";
		}
		if (msg.length == 0) {
			msg = Utils.getMessageOfInvalidFormatDate("fDate","Từ ngày");
			err = "#fDate";
		}
		if (msg.length == 0) {
			msg = Utils.getMessageOfInvalidFormatDate("tDate","Đến ngày");
			err = "#tDate";
		}		
		
		var fDate = $('#fDate').val().trim();
		var tDate = $('#tDate').val().trim();		
		if (msg.length == 0 && fDate.length > 0 && tDate.length > 0 && !Utils.compareDate(fDate, tDate)) {
			msg = 'Từ ngày phải là ngày trước hoặc cùng ngày với đến ngày';
			err = '#fDate';
		}
		if (msg.length > 0) {
			$('#errMsgSuggestEviction').html(msg).show();
			$(err).focus();
			return false;
		}
		params.code = $('#code').val().trim();
		params.statusRecord = $('#status').val().trim();
		params.flagRecordStatus = $('#statusApproved_flag').val();
		params.fromDate = $('#fDate').val().trim();
		params.toDate = $('#tDate').val().trim();
		params.statusDelivery = $('#statusDelivery').val().trim();
		EquipmentSuggestEviction._mapRecored = new Map();
		$('#gridSuggestEviction').datagrid("uncheckAll");
		$('#gridSuggestEviction').datagrid("load", params);
		return false;
	},
	/**
	 * Luu thay doi trang thai bien ban
	 * 
	 * @author nhutnn
	 * @since 15/07/2015
	 */
	saveRecordChange: function() {
		$('.ErrorMsgStyle').html("").hide();
		var rows = $('#gridSuggestEviction').datagrid('getRows');
		if (rows.length == 0) {
			$('#errMsgSuggestEviction').html('Không có dữ liệu để lưu').show();
			return false;
		}
		if (EquipmentSuggestEviction._mapRecored.keyArray.length == 0) {
			$('#errMsgSuggestEviction').html('Bạn chưa chọn biên bản. Vui lòng chọn biên bản!').show();
			return false;
		}
		var statusRecordChange = $('#statusRecordLabel').val();
		if (statusRecordChange == undefined || statusRecordChange == null || statusRecordChange == "") {
			$('#errMsgSuggestEviction').html('Bạn chưa chọn trạng thái để lưu biên bản!').show();
			return false;
		}

		var lstIdRecord = new Array();
		for (var i = 0; i < EquipmentSuggestEviction._mapRecored.keyArray.length; i++) {
			lstIdRecord.push(EquipmentSuggestEviction._mapRecored.keyArray[i]);
		}
		var params = new Object();
		params.lstIdRecord = lstIdRecord;
		params.statusRecord = statusRecordChange;
		Utils.addOrSaveData(params, '/equip-suggest-eviction/save-record-change', null, 'errMsgSuggestEviction', function(data) {
			if (data.error) {
				$('#errMsgSuggestEviction').html(data.errMsg).show();
				return;
			}
			if (data.lstRecordError != null && data.lstRecordError.length > 0) {
				VCommonJS.showDialogList({
					dialogInfo:  { title: 'Thông tin lỗi'},
					data: data.lstRecordError,
					columns: [[
						{field: 'formCode', title: 'Mã biên bản', align: 'left', width: 120, sortable: false, resizable: false}, 
						{field: 'formStatusErr', title: 'Trạng thái biên bản', align: 'center', width: 60, sortable: false, resizable: false, formatter: function(value, row, index) {
							var html = "";
							if (row.formStatusErr == 1) {
								html = '<img src="/resources/images/icon_delete.png" height="20" width="20" style="margin-right:5px;" title="Error">';
							}
							return html;
						}},
						{field:'equipTrading', title:'Danh sách thiết bị đang tham gia giao dịch', align:'left', width:120, sortable:false, resizable:false,formatter: function(value, row, index){
							var html = "";
							if (row.equipTrading != undefined && row.equipTrading != null) {
								html = Utils.XSSEncode(row.equipTrading);
							}
							return html;
						}}
					]]
				});
			} else {
				$('#successMsgSuggestEviction').html('Lưu dữ liệu thành công').show();
				setTimeout(function() {
					$('#successMsgSuggestEviction').html('').hide();
				}, 3000);
			}
			EquipmentSuggestEviction._mapRecored = new Map();
			$('#gridSuggestEviction').datagrid('reload');
			$('#gridSuggestEviction').datagrid('uncheckAll');
		}, null, null, null, "Bạn có muốn cập nhật dữ liệu?", null);
	},
	/**
	 * Xuat file excel
	 * @author nhutnn
	 * @since 20/07/2015
	 */
	exportEquipSuggestEviction: function() {
		$('.ErrorMsgStyle').html("").hide();
		if (EquipmentSuggestEviction._mapRecored == null || EquipmentSuggestEviction._mapRecored.keyArray.length == 0) {
			$('#errMsgSuggestEviction').html('Bạn chưa chọn biên bản. Vui lòng chọn biên bản!').show();
			return false;
		}

		var lstIdRecord = new Array();
		for (var i = 0; i < EquipmentSuggestEviction._mapRecored.keyArray.length; i++) {
			lstIdRecord.push(EquipmentSuggestEviction._mapRecored.keyArray[i]);
		}
		var params = new Object();
		params.lstIdRecord = lstIdRecord;		
		ReportUtils.exportReport('/equip-suggest-eviction/export-excel', params, "errMsgSuggestEviction");
		$('#gridSuggestEviction').datagrid('uncheckAll');
		EquipmentSuggestEviction._mapRecored = new Map();
	},
	/**
	 * Show popup import
	 * @author nhutnn
	 * @since 22/07/2015
	 */
	showDlgImportExcel: function(){
		$('#easyuiPopupImportExcel').show();
		$('.ErrorMsgStyle').hide();
		$('#easyuiPopupImportExcel').dialog({  
	        closed: false,  
	        cache: false,  
	        modal: true,
	        height:'auto',
	        onOpen: function(){	 
	        	$('.easyui-dialog .BtnGeneralStyle').css('color','');
	        },
	        onClose: function(){
	        	EquipmentSuggestEviction.searchSuggestEviction();
				$("#gridSuggestEviction").datagrid('uncheckAll');
				$("#fakefilepcBBDNTH").val("");
				$("#excelFileBBDNTH").val("");				
	        }
		});
	},
	/**
	 * import excel
	 * @author nhutnn
	 * @since 22/07/2015
	 */
	importExcelBBDNTH: function(){
		$('.ErrorMsgStyle').hide();
		VTUtilJS.importExcelUtils(function(data) {
			$("#errExcelMsgBBDNTH").html(data.message).show();		
			if ($("#errExcelMsgBBDNTH span").length > 0 && (data.fileNameFail == null || data.fileNameFail == "")) {
				setTimeout(function(){
					$('#errExcelMsgBBDNTH').html("").hide();
					$('#easyuiPopupImportExcel').dialog('close');
				}, 3000);				
			}else{
				$("#fakefilepcBBDNTH").val("");
				$("#excelFileBBDNTH").val("");							
			}						
		}, "importFrmBBDNTH", "excelFileBBDNTH", null, "errExcelMsgBBDNTH");
		return false;
	},	
	/**
	 * Xoa dong chi tiet
	 * @author nhutnn
	 * @since 23/07/2015
	 */
	deleteRowEquipLendDetail: function(index){	
		$('.ErrorMsgStyle').html('').hide();	
		$.messager.confirm('Xác nhận', "Bạn có muốn xóa dữ liệu?", function(r){
			if(r){
				if(EquipmentSuggestEviction._editIndex != null){
					if(EquipmentSuggestEviction._editIndex == index){		
						EquipmentSuggestEviction._editIndex = null;
					} else if (EquipmentSuggestEviction._editIndex > index){
						// them dong moi
						var row = EquipmentSuggestEviction.getDetailEdit();
						$('#gridEquipSuggestEvictionChange').datagrid('updateRow', {
							index: EquipmentSuggestEviction._editIndex,
							row: row
						});
						
						EquipmentSuggestEviction._editIndex--;
					}
				}
				// xoa
				$('#gridEquipSuggestEvictionChange').datagrid('deleteRow', index);
				var rows = $("#gridEquipSuggestEvictionChange").datagrid("getRows");	
				$("#gridEquipSuggestEvictionChange").datagrid("loadData",rows);

				if(EquipmentSuggestEviction._editIndex != null){
					$('#gridEquipSuggestEvictionChange').datagrid('beginEdit', EquipmentSuggestEviction._editIndex);
					EquipmentSuggestEviction.addEditorInGridChangeLend();
				}
			}
		});
	},
	/**
	 * Them khach hang vao dong chi tiet
	 * @author nhutnn
	 * @since 23/07/2015
	 */
	addCustomerInfo: function(id, customerCode, customerCodeLong, customerName, phone, mobilephone, shopMienName, shopKenhName, shopCode, shopName){
		$('#divOverlay').show();		
		if (EquipmentSuggestEviction._editIndex != null) {
			var row = EquipmentSuggestEviction.getDetailEdit();
			row.customerId = id;	
			row.customerCode = customerCode;		
			row.customerCodeLong = customerCodeLong;
			row.customerName = customerName;
			if (phone != null && phone != "" && mobilephone != null && mobilephone != "") {
				row.mobile = phone + " / " + mobilephone;
			} else if (phone != null && phone != "") {
				row.mobile = phone;
			} else if (mobilephone != null && mobilephone != "") {
				row.mobile = mobilephone;
			} else {
				row.mobile = "";
			}
			row.shopCode = shopCode;
			row.shopName = shopName;
			row.shopCodeMien = shopMienName;
			//row.shopCodeKenh = shopKenhName;

			row.equipCode = "";
			row.equipSeri = "";
			row.equipGroup = "";
			row.healthStatus = "";

			row.relation = "";
			row.representative = "";
			row.ward = "";
			row.district = "";
			row.province = "";
			row.address = "";
			row.street = "";

			row.staffCode = "";
			row.staffName = "";
			row.staffPhone = "";
			row.staffMobile = "";
			row.phone = "";

			// them dong moi			
			$('#gridEquipSuggestEvictionChange').datagrid('updateRow', {
				index: EquipmentSuggestEviction._editIndex,
				row: row
			});

			$('#gridEquipSuggestEvictionChange').datagrid('beginEdit', EquipmentSuggestEviction._editIndex);
			EquipmentSuggestEviction.addEditorInGridChangeLend();
			var ed = $('#gridEquipSuggestEvictionChange').datagrid('getEditors', EquipmentSuggestEviction._editIndex);
			$(ed[2].target).combobox('clear');
			$(ed[6].target).combobox('clear');
			if(customerCode.length > 0 && shopCode.length > 0 && id != null) {
				// EquipmentSuggestEviction.getLstEquip(customerCode, shopCode, id);
				EquipmentSuggestEviction.getLstGSNPP(customerCode, shopCode, id);
			}		
		}
		$('#divOverlay').hide();
		$('.easyui-dialog').dialog('close');
	},
	
	/**
	 * Lay thong tin thiet bi
	 * @author nhutnn
	 * @since 23/07/2015
	 */	
	getLstEquip: function(shortCode, shopCode, id) {
		var params = new Object();
		params.shopCode = shopCode;
		params.shortCode = shortCode;
		params.stockId = id;
		var ed = $('#gridEquipSuggestEvictionChange').datagrid('getEditors', EquipmentSuggestEviction._editIndex);
		Utils.getJSONDataByAjax(params, '/equip-suggest-eviction/get-list-equip', function(result) {
			var row = $('#gridEquipSuggestEvictionChange').datagrid('getRows')[EquipmentSuggestEviction._editIndex];
			if (result.lstEquip != undefined && result.lstEquip != null && result.lstEquip.length > 0) {
				// co thiet bi trong kho khach hang						
				EquipmentSuggestEviction._lstEquip = result.lstEquip;
				if (row.equipCode != undefined && row.equipCode != null && row.equipCode != "") {
					var isExist = false;
					for (var i = 0; i < result.lstEquip.length; i++) {
						if (result.lstEquip[i].code == row.equipCode) {
							isExist = true;
							break;
						}
					}
					if (!isExist) {
						var t = {
							code: row.equipCode,
							serial: row.equipSeri,
							equipGroupName: row.equipGroup,
							healthStatus: row.healthStatus
						};
						result.lstEquip.push(t);
					}
				}
				$(ed[2].target).combobox('loadData', result.lstEquip);
				if (row.equipCode != undefined && row.equipCode != null && row.equipCode != "") {
					$(ed[2].target).combobox("select", row.equipCode);
				} else {
					$(ed[2].target).combobox("select", result.lstEquip[0].code);
				}
			} else {
				// khong thay thiet bi trong kho khach hang
				if (row.equipCode != undefined && row.equipCode != null && row.equipCode != "") {
					result.lstEquip = new Array();
					EquipmentSuggestEviction._lstEquip = result.lstEquip;
					var t = {
						code: row.equipCode,
						serial: row.equipSeri,
						equipGroupName: row.equipGroup,
						healthStatus: row.healthStatus
					};
					result.lstEquip.push(t);
					$(ed[2].target).combobox('loadData', result.lstEquip);
					$(ed[2].target).combobox("select", row.equipCode);
				} else {
					$(ed[2].target).combobox('loadData', []);
					EquipmentSuggestEviction._lstEquip = [];
				}
			}
		}, null, null);
	},
	/**
	 * Lay thong tin giam sat
	 * @author nhutnn
	 * @since 23/07/2015
	 */	
	getLstGSNPP: function(shortCode, shopCode, id) {
		var params = new Object();
		params.shopCode = shopCode;
		var ed = $('#gridEquipSuggestEvictionChange').datagrid('getEditors', EquipmentSuggestEviction._editIndex);
		Utils.getJSONDataByAjax(params,'/equip-suggest-eviction/get-list-gsnpp',function(result) {
			if (result.lstStaff != undefined && result.lstStaff != null && result.lstStaff.length > 0) {
				for (var i = 0, sz = result.lstStaff.length; i < sz; i++) {
					var obj = result.lstStaff[i];
					obj.nameText = obj.staffCode +" - "+ obj.staffName;					
				}							
				EquipmentSuggestEviction._lstStaff = result.lstStaff;
				var row = $('#gridEquipSuggestEvictionChange').datagrid('getRows')[EquipmentSuggestEviction._editIndex];
				$(ed[6].target).combobox('loadData', result.lstStaff);
				if(row.staffCode != undefined && row.staffCode != null && row.staffCode != "" ){
					$(ed[6].target).combobox("select", row.staffCode);
				}
			} else {
				EquipmentSuggestEviction._lstStaff = [];
				$(ed[6].target).combobox('loadData', []);
			}	
			
			EquipmentSuggestEviction.getLstEquip(shortCode, shopCode, id);	
		}, null, null);
	},
	/**
	 * Them editor tren dong them moi
	 * @author nhutnn
	 * @since 08/07/2015
	 */
	addEditorInGridChangeLend: function() {
		var ed = $('#gridEquipSuggestEvictionChange').datagrid('getEditors', EquipmentSuggestEviction._editIndex);
		var row = $('#gridEquipSuggestEvictionChange').datagrid('getRows')[EquipmentSuggestEviction._editIndex];
    	
		var getCustomer = function() {
			$(".ErrorMsgStyle").html('').hide();
			if ($(ed[0].target).text('getValue').val() != null && $(ed[0].target).text('getValue').val() != "") {
				if ($(ed[1].target).text('getValue').val() != null && $(ed[1].target).text('getValue').val() != "") {
					var params = new Object();
					params.isNPP = true;
					params.shopCode = $(ed[0].target).text('getValue').val();
					params.page = 1;
					params.max = 10;
					params.shortCode = $(ed[1].target).text('getValue').val();

					Utils.getJSONDataByAjax(params, '/equip-suggest-eviction/search-customer', function(result) {
						if (result.rows != null && result.rows.length > 0) {
							var row = result.rows[0];
							var phone = row.phone == null ? "" : row.phone;
							var mobilephone = row.mobiphone == null ? "" : row.mobiphone;

							EquipmentSuggestEviction.addCustomerInfo(row.id, row.shortCode, row.customerCode, row.customerName, phone, mobilephone, row.shopMienCode, row.shopKenhCode, row.shopCode, row.shopName);
						} else {
							EquipmentSuggestEviction.addCustomerInfo("", "", "", "", "", "", "", "", $(ed[0].target).text('getValue').val(), "");
							$('#errMsgChangeSuggestEviction').html("Không tìm thấy mã khách hàng trong hệ thống!").show();
							return false;
						}
						$(ed[1].target).focus();
					});
				}
			} else {
				$('#errMsgChangeSuggestEviction').html("Bạn chưa nhập mã NPP!").show();
				$(ed[0].target).focus();
				return false;
			}
		};
    	// Ma khach hang
		$(ed[1].target).bind('keyup', function(event) {
			if (event.keyCode == keyCode_F9) {
				$(".ErrorMsgStyle").html('').hide();
				// if($(ed[0].target).text('getValue').val() != null && $(ed[0].target).text('getValue').val() != ""){
					VCommonJS.showDialogSearch2({
						 params : {
						 	shopCodeEditor: $(ed[0].target).text('getValue').val() == null? "" : $(ed[0].target).text('getValue').val().trim(),
						 },
					    inputs : [
					    	{id:'shopCode', maxlength:50, label:'Mã đơn vị'},
					        {id:'shopName', maxlength:250, label:'Tên đơn vị'},
					        {id:'shortCode', maxlength:50, label:'Mã KH'},
					        {id:'customerName', maxlength:250, label:'Tên KH'},				       
					    ],			   
					    url : '/equip-suggest-eviction/search-customer',			   
					    columns : [[
					    	{field:'shopCode', title:'Đơn vị', align:'left', width: 150, sortable:false, resizable:false, formatter:function(value,row,index) {
					        	return Utils.XSSEncode(row.shopCode + " - " + row.shopName);
					        }},
					        {field:'shortCode', title:'Khách hàng', align:'left', width: 150, sortable:false, resizable:false, formatter:function(value,row,index) {
					        	return Utils.XSSEncode(row.shortCode + " - " + row.customerName);
					        }},
					        {field:'address', title:'Địa chỉ', align:'left', width: 150, sortable:false, resizable:false},				        
					        {field:'choose', title:'', align:'center', width:40, sortable:false, resizable:false, formatter:function(value,row,index) {
					        	var phone = row.phone == null ? "" : row.phone;
					        	var mobilephone = row.mobiphone == null ? "" : row.mobiphone;
					        	return "<a href='javascript:void(0)' onclick=\"return EquipmentSuggestEviction.addCustomerInfo('"+row.id+"','"+row.shortCode+"','"+row.customerCode+"','"+row.customerName+"','"+phone+"','"+mobilephone+"','"+row.shopMienCode+"','"+row.shopKenhCode+"','"+row.shopCode+"','"+row.shopName+"');\">Chọn</a>";        
					        }}
					    ]]
					});
					$(".easyui-dialog #code").focus();
				// } else {
				// 	$('#errMsgChangeSuggestEviction').html("Bạn chưa nhập mã đơn vị!").show();
				// 	$(ed[0].target).focus();
				// 	return false;
				// }
			} else if (event.keyCode == keyCodes.ENTER) {
				getCustomer();
			}
		});		

		$(ed[1].target).keydown(function(e) {
			var keyCode = e.which || e.keyCode; // for cross-browser compatibility
			if (keyCode == keyCodes.TAB) {
				getCustomer();
			}
		});

		if ($(ed[2].target).combobox('getData').length == 0 && EquipmentSuggestEviction._lstEquip != null) {
			$(ed[2].target).combobox('loadData', EquipmentSuggestEviction._lstEquip);
			if (row.equipCode != undefined && row.equipCode != null && row.equipCode != "") {
				$(ed[2].target).combobox("select", row.equipCode);
			}
		}
		if (row.stockTypeValue != undefined && row.stockTypeValue != null && row.stockTypeValue != "") {
			$(ed[4].target).combobox("select", row.stockTypeValue);
		}
		if ($(ed[6].target).combobox('getData').length == 0 && EquipmentSuggestEviction._lstStaff != null) {
			$(ed[6].target).combobox('loadData', EquipmentSuggestEviction._lstStaff);
			if (row.staffCode != undefined && row.staffCode != null && row.staffCode != "") {
				$(ed[6].target).combobox("select", row.staffCode);
			}
		}

		// thoi gian thu hoi tu
		$($(ed[3].target).datebox('textbox')).attr('id', 'evictionDate' + EquipmentSuggestEviction._editIndex).attr("maxlength", "10");
		Utils.bindFormatOnTextfield('evictionDate' + EquipmentSuggestEviction._editIndex, Utils._TF_NUMBER_CONVFACT);
	},
	/**
	 * Validate dong them moi
	 * @author nhutnn
	 * @since 08/07/2015
	 */
	validateEquipmentInGrid: function() {
		var currentDate = Utils.currentDate();
		var ed = $('#gridEquipSuggestEvictionChange').datagrid('getEditors', EquipmentSuggestEviction._editIndex);
		var msg = "";
		// ma NPP
		if (msg.length == 0 && ($(ed[0].target).text('getValue').val() == null || $(ed[0].target).text('getValue').val() == "")) {
			msg = "Bạn chưa nhập mã NPP!";
			$(ed[0].target).focus();
		}
		// ma kh
		if (msg.length == 0 && ($(ed[1].target).text('getValue').val() == null || $(ed[1].target).text('getValue').val() == "")) {
			msg = "Bạn chưa nhập mã khách hàng!";
			$(ed[1].target).focus();
		}
		// ma thiet bi
		if (msg.length == 0 && ($(ed[2].target).combobox('getValue') == null || $(ed[2].target).combobox('getValue') == "")) {
			msg = "Bạn chưa chọn thiết bị!";
			$(ed[2].target).focus();
		}
		// thoi gian thu hoi
		if (msg.length == 0 && ($(ed[3].target).datebox('getValue') == null || $(ed[3].target).datebox('getValue') == "")) {
			msg = "Bạn chưa nhập thời gian muốn thu hồi thiết bị!";
			$(ed[3].target).focus();
		}		
		if (msg.length == 0 && $(ed[3].target).datebox('getValue').length > 0 && !Utils.isDate($(ed[3].target).datebox('getValue'), '/')) {
			msg = format(msgErr_invalid_format_date,"Thời gian muốn thu hồi thiết bị");
			$(ed[3].target).focus();
		}		
		if (msg.length == 0 && $(ed[3].target).datebox('getValue').length > 0 && Utils.compareDateForTowDate($(ed[3].target).datebox('getValue'), currentDate) < 0) {
			msg = 'Thời gian muốn thu hồi thiết bị phải là ngày sau hoặc bằng ngày hiện tại';
			$(ed[3].target).focus();
		}
		// kho nhan
		if (msg.length == 0 && ($(ed[4].target).combobox('getValue') == null || $(ed[4].target).combobox('getValue') == "")) {
			msg = "Bạn chưa nhập kho nhận!";
			$(ed[4].target).focus();
		}
		// ly do
		if (msg.length == 0 && ($(ed[5].target).text('getValue').val() == null || $(ed[5].target).text('getValue').val() == "")) {
			msg = "Bạn chưa nhập lý do!";
			$(ed[5].target).focus();
		}
		// ma giam sat
		if (msg.length == 0 && ($(ed[6].target).combobox('getValue') == null || $(ed[6].target).combobox('getValue') == "")) {
			msg = "Bạn chưa chọn mã giám sát!";
			$(ed[6].target).focus();
		}

		return msg;
	},
	/**
	 * Them dong chi tiet
	 * @author nhutnn
	 * @since 07/07/2015
	 */
	insertEquipmentInGrid: function() {
		$('.ErrorMsgStyle').html('').hide();		
		if (EquipmentSuggestEviction._editIndex != null ) {
			var msg = EquipmentSuggestEviction.validateEquipmentInGrid();
			if (msg.length > 0) {
				$('#errMsgChangeSuggestEviction').html(msg).show();
				return false;
			}
			// them dong moi
			var row = EquipmentSuggestEviction.getDetailEdit();
			$('#gridEquipSuggestEvictionChange').datagrid('updateRow', {
				index: EquipmentSuggestEviction._editIndex,
				row: row
			});
		}
		var newRow = {timeEviction: getCurrentDate()};
		$('#gridEquipSuggestEvictionChange').datagrid("appendRow", newRow);
		EquipmentSuggestEviction._lstStaff = null;
		EquipmentSuggestEviction._lstEquip = null;
		var sz = $("#gridEquipSuggestEvictionChange").datagrid("getRows").length;
		EquipmentSuggestEviction._editIndex = sz-1;
		$('#gridEquipSuggestEvictionChange').datagrid('beginEdit', EquipmentSuggestEviction._editIndex);
		EquipmentSuggestEviction.addEditorInGridChangeLend();

	},
	/**
	 * Lay thong dong chi tiet them
	 * @author nhutnn
	 * @since 08/07/2015
	 */
	getDetailEdit: function(){
		var ed = $('#gridEquipSuggestEvictionChange').datagrid('getEditors', EquipmentSuggestEviction._editIndex);
		var row = $('#gridEquipSuggestEvictionChange').datagrid("getRows")[EquipmentSuggestEviction._editIndex];

		row.shopCode = $(ed[0].target).text("getValue").val().trim();
		row.customerCode = $(ed[1].target).text("getValue").val().trim();
		if($(ed[2].target).combobox("getValue") != null && $(ed[2].target).combobox("getValue") != ""){
			row.equipCode = $(ed[2].target).combobox("getValue");
		}
		
		row.timeEviction = $(ed[3].target).datebox("getValue").trim();

		row.stockType = $(ed[4].target).combobox("getText").trim();
		if($(ed[4].target).combobox("getValue") != null && $(ed[4].target).combobox("getValue") != ""){
			row.stockTypeValue = $(ed[4].target).combobox("getValue");
		}		

		row.reason = $(ed[5].target).text("getValue").val().trim();
		if($(ed[6].target).combobox("getValue") != null && $(ed[6].target).combobox("getValue") != ""){
			row.staffCode = $(ed[6].target).combobox("getValue");
		}		

		return row;
	},
	/**
	 * Chinh sua bien ban
	 * @author nhutnn
	 * @since 08/07/2015
	 */
	updateRecord: function(){
		$('.ErrorMsgStyle').html('').hide();
		var msg = "";
		var err = "";
		if (msg.length == 0 && ($("#status").val() == undefined || $("#status").val() == null || $("#status").val() == "")) {
			msg = 'Bạn chưa chọn trạng thái!';
			err = '#status';
		}
		if (msg.length == 0 && $('#createFormDate').val().trim().length > 0 && !Utils.isDate($('#createFormDate').val(), '/')) {
			msg = 'Ngày biên bản không tồn tại hoặc không đúng định dạng dd/mm/yyyy.';
			err = '#createFormDate';
		}
		var currentDate = Utils.currentDate();
		if (msg.length == 0 && $('#createFormDate').val().length > 0 && Utils.compareDateForTowDate($('#createFormDate').val(), currentDate) > 0) {
			msg = 'Ngày biên bản phải là ngày trước hoặc cùng ngày với ngày hiện tại';
			err = '#createFormDate';
		}
		var rows = $('#gridEquipSuggestEvictionChange').datagrid("getRows");
		if(msg.length == 0 && rows.length == 0){
			msg = 'Bạn chưa thêm chi tiết biên bản';
		}
		if (msg.length == 0){
			if(EquipmentSuggestEviction._editIndex != null){
				msg = EquipmentSuggestEviction.validateEquipmentInGrid();
				if(msg.length > 0){
					$('#errMsgChangeSuggestEviction').html(msg).show();
					return false;
				}				
				// them dong moi
				var row = EquipmentSuggestEviction.getDetailEdit();
				rows[EquipmentSuggestEviction._editIndex] = row;
				//$('#gridEquipSuggestEvictionChange').datagrid("updateRow", {index: EquipmentSuggestEviction._editIndex, row: row});
			}			
		}	

		if (msg.length == 0 && $('#note').attr('maxlength') > 500) {
			msg = 'Bạn nhập quá giới hạn của trường note';
		}

		if(msg.length > 0){
			$('#errMsgChangeSuggestEviction').html(msg).show();
			$(err).focus();
			return false;
		}
		
		var params = new Object();
		params.id = $("#idEquipSuggestEviction").val();
		params.lstDetails = rows;
		params.statusRecord = $("#status").val();
		params.createFormDate = $("#createFormDate").val();
		params.note = $('#note').val();
		//params = JSONUtil.getSimpleObject(params);
		JSONUtil.saveData2(params, "/equip-suggest-eviction/update", "Bạn có muốn cập nhật?", "errMsgChangeSuggestEviction", function(data){
			if(data.error){
				$("errMsgChangeSuggestEviction").html(data.errMsg).show();
			} else {
				$("#successMsgChangeSuggestEviction").html("Lưu dữ liệu thành công").show();
				setTimeout(function(){					
					if(window.location.pathname == "/equip-suggest-eviction/edit"){
						window.location.href = '/equip-suggest-eviction/info';	
					} else if(window.location.pathname == "/equip-suggest-eviction/change"){
						window.location.href = '/equip-suggest-eviction/approve';	
					}					
				}, 3500);
			}
		},null,null);
	},
	/**
	 * Xuat lai bien ban hop dong giao nhan lan nua
	 *
	 * @author nhutnn
	 * @since 10/07/2015
	 */
	viewPrintEquipSuggestEviction: function(id){
		$('.ErrorMsgStyle').html('').hide();
		
		var lstIdRecord = null;
		if(id != null){			
			lstIdRecord = new Array();
			lstIdRecord.push(id);
		}else{
			$('#errMsgSuggestEviction').html('Vui lòng chọn biên bản đã duyệt để xem bản in!').show();
		}		
		
		var params = new Object();
		params.lstIdRecord = lstIdRecord;	
		ReportUtils.exportReport('/equip-suggest-eviction/view-print', params);
		return false;
	},
	/**
	 * Xuat bien ban thu hoi
	 * 
	 * @author update nhutnn
	 * @since 10/07/2015
	 */
	exportEvictionForm: function(id) {
		$('.ErrorMsgStyle').html("").hide();
		var rows = $('#gridSuggestEviction').datagrid('getRows');
		if (rows.length == 0) {
			$('#errMsgSuggestEviction').html('Không có dữ liệu để xuất').show();
			return false;
		}

		if(id == null) {
			if (EquipmentSuggestEviction._mapRecored.keyArray.length == 0) {
				$('#errMsgSuggestEviction').html('Bạn chưa chọn biên bản. Vui lòng chọn biên bản!').show();
				return false;
			}
		}	

		var lstIdRecord = new Array();
		if(id == null) {
			for (var i = 0; i < EquipmentSuggestEviction._mapRecored.keyArray.length; i++) {
				lstIdRecord.push(EquipmentSuggestEviction._mapRecored.keyArray[i]);
			}
		}else{
			lstIdRecord.push(id);
		}

		var params = new Object();
		params.lstIdRecord = lstIdRecord;
		Utils.addOrSaveData(params, '/equip-suggest-eviction/export-eviction-record', null, 'errMsgSuggestEviction', function(data) {
			if (data.error) {
				$('#errMsgSuggestEviction').html(data.errMsg).show();
				return;
			}
			if (data.lstRecordError != null && data.lstRecordError.length > 0) {
				VCommonJS.showDialogList({
					dialogInfo: {title: 'Thông tin lỗi'},
					data: data.lstRecordError,
					columns: [[
						{field: 'formCode', title: 'Mã biên bản', align: 'left', width: 120, sortable: false, resizable: false}, 
						{field: 'formStatusErr', title: 'Biên bản không ở trạng thái duyệt', align: 'center', width: 60, sortable: false, resizable: false, formatter: function(value, row, index) {
							var html = "";
							if (row.formStatusErr == 1) {
								html = '<img src="/resources/images/icon_delete.png" height="20" width="20" style="margin-right:5px;" title="Error">';
							}
							return html;
						}},
						{field: 'deliveryStatusErr', title: 'Biên bản đã xuất biên bản thu hồi', align: 'center', width: 60, sortable: false, resizable: false, formatter: function(value, row, index) {
							var html = "";
							if (row.deliveryStatusErr == 1) {
								html = '<img src="/resources/images/icon_delete.png" height="20" width="20" style="margin-right:5px;" title="Error">';
							}
							return html;
						}}
					]]
				});
			} else {
				$('#successMsgSuggestEviction').html('Xuất biên bản thu hồi thành công!').show();
				setTimeout(function() {
					$('#successMsgSuggestEviction').html('').hide();
				}, 3000);
			}
			EquipmentSuggestEviction.searchSuggestEviction();
		}, null, null, null, "Bạn có muốn xuất biên bản thu hồi ?", null);
	},	
	/**
	 * Lay danh sach bien ban hop dong giao bi huy
	 *
	 * @author nhutnn
	 * @since 10/07/2015
	 */
	getEvictionFormAgain: function(id) {
		$('.ErrorMsgStyle').html("").hide();
		if(id == null) {
			$('#errMsgSuggestEviction').html('Bạn chưa chọn biên bản. Vui lòng chọn biên bản!').show();
		}	

		var params = new Object();
		params.id = id;
		Utils.getJSONDataByAjax(params, '/equip-suggest-eviction/get-list-eviction-export-again', function(data) {
			if (data.error) {
				$('#errMsgSuggestEviction').html(data.errMsg).show();
				return;
			}else {
				if (data.total > 1) {
					EquipmentSuggestEviction._mapEviction = new Map();  
					$("#idRecordPopup").val(id); 
					$('#easyuiPopupLstEviction').show();
					$('#easyuiPopupLstEviction').dialog({  
				        closed: false,  
				        cache: false,  
				        modal: true,
				        width : 430,	        
				        height : 'auto',
				        onOpen: function(){	      
							$('#gridDelivery').datagrid({
								data: data.rows,
								autoRowHeight : true,
								fitColumns:true,
								width: 390,
								height: "auto",
								scrollbarSize: 0,
								rownumbers:true,
								columns:[[
								 	{field: 'maNPP', title: 'Mã NPP', width: 80, sortable:false, resizable: false, align: 'left'},					
								    {field: 'maKH', title: 'Mã khách hàng', width: 80, sortable:false, resizable: false, align: 'left'},					
									{field: 'maPhieu', title: 'Mã biên bản thu hồi', width: 120, sortable: false, resizable: false,align: 'left'},		    
									{field: 'check',checkbox:true, width: 50, sortable:false,resizable:false, align: 'center'}
								]],
								onBeforeLoad: function(param){											 
								},
								onLoadSuccess: function(data){   			 
							   		$('.easyui-dialog .datagrid-header-rownumber').html('STT');						   		
								},onCheck:function(index,row){
									EquipmentSuggestEviction._mapEviction.put(row.id,row);   
							    },
							    onUncheck:function(index,row){
							    	EquipmentSuggestEviction._mapEviction.remove(row.id);
							    },
							    onCheckAll:function(rows){
							    	for(var i = 0;i<rows.length;i++){
							    		var row = rows[i];
							    		EquipmentSuggestEviction._mapEviction.put(row.id,row);
							    	}
							    },
							    onUncheckAll:function(rows){
							    	for(var i = 0;i<rows.length;i++){
							    		var row = rows[i];
							    		EquipmentSuggestEviction._mapEviction.remove(row.id);	
							    	}
							    }
							});
				        }, 
				        onClose: function(){
				        	EquipmentSuggestEviction._mapEviction = null;
				        }
					});
				} else {
					EquipmentSuggestEviction.exportEvictionFormAgain(id, data.rows[0].customerId);
				}
			}
		}, null, null);
	},
	/**
	 * Xuat lai bien ban hop dong giao nhan lan nua
	 *
	 * @author nhutnn
	 * @since 10/07/2015
	 */
	exportEvictionFormAgain: function(id, customerId){
		$('.ErrorMsgStyle').html("").hide();
		if(customerId == null && id == null) {
			if (EquipmentSuggestEviction._mapEviction.keyArray.length == 0) {
				$('#errMsgPopupLstEviction').html('Bạn chưa chọn biên bản. Vui lòng chọn biên bản!').show();
				return false;
			}
		}	

		var lstCustomerId = new Array();
		if(customerId == null && id == null) {
			for (var i = 0; i < EquipmentSuggestEviction._mapEviction.keyArray.length; i++) {
				var idEviction = EquipmentSuggestEviction._mapEviction.keyArray[i];
				lstCustomerId.push(EquipmentSuggestEviction._mapEviction.get(idEviction).customerId);
			}
		}else{
			lstCustomerId.push(customerId);
		}
		var errId = customerId == null?"errMsgPopupLstEviction":"errMsgSuggestEviction";
		var successId = customerId == null?"successMsgPopupLstEviction":"successMsgSuggestEviction";
		var params = new Object();
		if(id != null){ 
			params.id = id;
		} else {
			params.id = $("#idRecordPopup").val();
		}	
		params.lstCustomerId = lstCustomerId;
		Utils.addOrSaveData(params, '/equip-suggest-eviction/export-eviction-record-again', null, errId, function(data) {
			if (data.error) {
				$('#'+errId).html(data.errMsg).show();
				return;
			}else {
				$('#' + successId).html('Xuất lại biên bản thu hồi thành công!').show();
				$("#idRecordPopup").val("");
				setTimeout(function() {
					$('#' + successId).html('').hide();					
				}, 3000);
				$('#easyuiPopupLstEviction').window('close');
			}
			EquipmentSuggestEviction.searchSuggestEviction();
		}, null, null, null, "Bạn có muốn xuất lại biên bản thu hồi ?", null);
	},
	/**
	 * Quay về trang tìm kiếm
	 *
	 * @author nhutnn
	 * @since 27/07/2015
	 */
	returnSearchPage: function(){
		$.messager.confirm('Xác nhận', "Bạn có muốn quay lại trang tìm kiếm và không lưu dữ liệu thay đổi?", function(r){
			if(r){
				if(window.location.pathname == "/equip-suggest-eviction/edit"){
					window.location.href = '/equip-suggest-eviction/info';	
				} else if(window.location.pathname == "/equip-suggest-eviction/change"){
					window.location.href = '/equip-suggest-eviction/approve';	
				}					
			}
		});
	},
};
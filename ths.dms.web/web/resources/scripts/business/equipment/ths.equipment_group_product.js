/**
 * Copyright 2015 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *
 * Doi tuong thao tac voi giao dien cua chuc nang: Danh sach san pham cho nhom thiet bi
 * @author tuannd20
 * @version 1.0
 * @date 05/01/2015 
 */
var EquipmentGroupProduct = {
	_mapNode: new Map(),
	_mapCheck: new Map(),
	_mainGridOption: null,
	_mapDataDdv: new Map(),
	_selectingEquipmentGroupCode: null,
	_selectingEquipmentGroupIndex: null,
	_idExpendRow: 0,
	_rowGroupEquipIndex: 0,
	TREE_TYPE_EQUIPMENT_GROUP_PRODUCT: 5,
	/**
	 * khoi tao control tren trang
	 * @return {void}
	 */
	initializePageControl: function(option) {
		if (option) {
			EquipmentGroupProduct._mainGridOption = option;
			if (option.gridId) {
				$('#' + option.gridId).datagrid({
					view: detailview,
					url: option.url,
					pagination: true,
					pageList: [50],
					fitColumns: true,
					width: $(window).width() - 55,
					scrollbarSize: 0,
					rownumbers: true,			
					autoRowHeight: true,
					nowrap: false,
					singleSelect: true,
					columns: [[  
						{field: 'code', title: 'Mã nhóm thiết bị', width: 200, align:'left', sortable : false, resizable : false, 
							formatter: function(value, row, index) {
								return Utils.XSSEncode(value);
							}
						},
						{field: 'name', title: 'Tên nhóm thiết bị', width: 500, align: 'left', sortable : false, resizable : false,
							formatter: function(value, row, index){
								return '<div style="word-wrap: break-word">' + Utils.XSSEncode(value) +'</div>';
							}
						},
						{field: 'status', title: 'Trạng thái', width: 100, align: 'center', sortable : false, resizable : false,
							formatter: function(value, row, index){
								return '<div style="word-wrap: break-word">' + Utils.XSSEncode(statusTypeText.parseValue(value)) +'</div>';
							}
						},
					]],
					detailFormatter:function(index, row){  
						return '<div style="padding:2px; width:850"><table id="ddv-' + index + '" class="ddv"></table></div>';
					},
					onBeforeLoad: function(){
					},
					onLoadSuccess: function(){
						$('#' + option.gridId).datagrid('resize');
						$('.datagrid-header-rownumber').html('STT');
					},
					onExpandRow: function(index, row){
						var gridDetailId = '';
						EquipmentGroupProduct.initializeEquipmentGroupSubGrid(option.gridId, index, row);
						$('#' + option.gridId).datagrid('fixDetailRowHeight',index);
						$('#' + option.gridId).datagrid('resize');
					}
				});
			}
		}
	},

	/**
	 * tai file excel template
	 * @author tamvnm
	 * @since Sep 15 2015
	 */
	 downloadTemplate:function () {
		$('.downloadTemplateReport').attr('href', excel_template_path + '/equipment/Bieu_mau_import_danh_sach_san_pham_thiet_bi.xlsx');
	 },

	/**
	 * show popup import excel
	 * @author tamvnm
	 * @since Sep 15 2015
	 */
	showDlgImportExcel: function() {
		$('#easyuiPopupImportExcel').show();
		$('.ErrorMsgStyle').hide();
		$('#easyuiPopupImportExcel').dialog({  
	        closed: false,  
	        cache: false,  
	        modal: true,
			height: 'auto',
	        onOpen: function() {	 
	        	$('.easyui-dialog .BtnGeneralStyle').css('color','');
	        },
	        onClose: function() {
				$("#fakefilepcBBGN").val("");
				$("#excelFileBBGN").val("");				
	        }
		});
	},
	/**
	 * import excel san pham thiet bi
	 * @author tamvnm
	 * @since sep 15 2015
	 */
	importExcelGroupProduct: function() {
		$('.ErrorMsgStyle').hide();
		VTUtilJS.importExcelUtils(function(data) {
			$("#errExcelMsgBBGN").html(data.message).show();
			if (data.fileNameFail == null || data.fileNameFail == "" ) {					
				setTimeout(function() {
					$('#errExcelMsgBBGN').html("").hide();
					$('#easyuiPopupImportExcel').dialog('close');
				}, 3000);				
			} else {
				$("#fakefilepcBBGN").val("");
				$("#excelFileBBGN").val("");			
			}
		}, "importFrmBBGN", "excelFileBBGN", null, "errExcelMsgBBGN");
		return false;
	},

	exportExcelGroupProduct: function() {

		$('#errMsg').html('').hide();
		var msg ='';
		var EquipGroupCode = $('#equipmentGroupCode').val();
		var EquipGroupName = $('#equipmentGroupName').val();
		var status = $('#equipmentGroupStatus').val();

		if (EquipGroupCode != undefined && EquipGroupCode.length > 0) {
			EquipGroupCode = EquipGroupCode.trim();
		}
		if (EquipGroupName != undefined && EquipGroupName.length > 0) {
			EquipGroupName = EquipGroupName.trim();
		}

		
		var params=new Object(); 
		params.equipmentGroupCode = EquipGroupCode;
		params.equipmentGroupName = EquipGroupName;
		params.equipmentGroupStatus = status;
		$.messager.confirm('Xác nhận','Bạn có muốn xuất file excel?',function(r) {
			if (r) {
				ReportUtils.exportReport('/equipment-group-product/export-excel', params, 'errMsg');					
			}
		});		
		return false;
	},

	initializeEquipmentGroupSubGrid: function(mainGridId, expandingIndex, expandingRow) {
		EquipmentGroupProduct._selectingEquipmentGroupCode = expandingRow && expandingRow.code ? expandingRow.code : '';
		var editColumnTitle = '<a href="javascript:void(0);" \
							onclick=\"return EquipmentGroupProduct.openSearchProductDialog(' + (expandingRow && expandingRow.id ? expandingRow.id : 0) + ', ' + expandingIndex + ');\"> \
							<img width="15" height="16" src="/resources/images/icon_add.png"></a>';

		var ddv = $('#' + mainGridId).datagrid('getRowDetail', expandingIndex).find('table.ddv');
		
		var url = '/equipment-group-product/retrieve-products-in-equipment-group?equipmentGroupCode=' + expandingRow.code;
		var paramDetail = {
			equipmentGroupCode: expandingRow.code
		};
		ddv.datagrid({
			url: url,
			fitColumns: true,
			singleSelect: true,
			pagination: true,
			pageList: [10, 20, 30, 50],
			rownumbers: true,
			scrollbarSize: 0,
			loadMsg: '',
			height: 'auto',
			width: $(window).width() - 200,
			columns:[[
				{field: 'productCode', title: 'Mã sản phẩm', width: 110, sortable : false, resizable : false, 
					formatter: function(value, row, index) {
						return Utils.XSSEncode(value);
					}
				},
				{field: 'productName', title: 'Tên sản phẩm', width: 180, sortable : false, resizable : false, 
					formatter: function(value, row, index) {
						return Utils.XSSEncode(value);
					}
				},
				{field: 'fromDate', title: 'Ngày bắt đầu', width: 110, sortable : false, resizable : false, 
					formatter: function(value, row, index) {
						return Utils.XSSEncode(row.fromDateStr);						
					}
				},
				{field: 'toDate', title: 'Ngày kết thúc', width: 110, sortable : false, resizable : false, 
					formatter: function(value, row, index) {
						return Utils.XSSEncode(row.toDateStr);
					}
				},
				{field: 'edit', title: editColumnTitle, width: 30, align: 'center', sortable : false, resizable : false,
					formatter: function(value, row, index) {
						if (row.flagSys == 1) {
							return "<a href='javascript:void(0)' onclick=\"return EquipmentGroupProduct.deleteProductInEquipmentGroup('" + ddv.attr('id') + "', '" + row.productCode + "', '" + expandingRow.code + "', "+row.equipGroupProductId+");\"><img width='15' height='16' src='/resources/images/icon_delete.png'/></a>";							
						}
						return '';
					}
				}
			]],
			onResize:function(){
				$('#' + mainGridId).datagrid('fixDetailRowHeight',expandingIndex);
			},
			onLoadSuccess:function(){
				setTimeout(function(){
					$('#' + mainGridId).datagrid('fixDetailRowHeight', expandingIndex);
					$('#' + mainGridId).datagrid('resize');
				},0);
			}
		});
		$('#' + mainGridId).datagrid('fixDetailRowHeight', expandingIndex);
	},
	/**
	 * mo dialog tim kiem san pham
	 * @return {void}
	 * 
	 * @author hunglm16
	 * @description ra soat va update
	 */
	openSearchProductDialog: function(rowDataId, rowIndex) {
		$('.ErrorMsgStyle').html('').hide();
		$('.SuccessMsgStyle').html('').hide();
		EquipmentGroupProduct._idExpendRow = 0;
		EquipmentGroupProduct._rowGroupEquipIndex = 0;
		if (rowDataId) {
			EquipmentGroupProduct._idExpendRow = rowDataId;
		}
		if (rowIndex) {
			EquipmentGroupProduct._rowGroupEquipIndex = rowIndex;
		}
		EquipmentGroupProduct._selectingEquipmentGroupIndex = rowIndex;
		EquipmentGroupProduct._selectingEquipmentGroupCode = $('#' + EquipmentGroupProduct._mainGridOption.gridId).datagrid('getRows')[rowIndex].code;
		$("#productCodeDl").val('');
		$("#productNameDl").val('');
		$(".easyui-dialog #errMsgProductDlg").html('').hide();
		//EquipmentGroupProduct._mapNode = new Map();
		EquipmentGroupProduct._mapCheck = new Map();
		$("#popupProductSearch").dialog({
			onOpen:function() {
				selectedDropdowlist('productInforDl', activeType.ALL);
				$('#productStatusDl option[value="'+activeType.RUNNING+'"]').attr('selected','selected').change();
				$('#productCodeDl, #productNameDl').width($('#productInforDl').width() - 5);
				$('#productCodeDl').focus();
				$('#gridProductInsert').datagrid({
					url: '/equipment-group-product/search-product-by-insert',
					width : 860,
					queryParams: {
						productCode: $('#productCodeDl').val().trim(),
						productName: $('#productNameDl').val().trim(),
						productInforId: $('#productInforDl').val(),
						status: $('#productStatusDl').val()
					},
					pagination: true,
					scrollbarSize: 0,
					fitColumns : true,
					checkOnSelect: false,
					selectOnCheck: false,
				    rownumbers : true,
				    height: 275,
				    pageSize:10,
				    pageList: [10],
					columns:[[
						{field: 'productId', checkbox:true},
						{field: 'productInfoCode', title: 'Ngành hàng', width: 80, align: 'left', formatter: function(value, row, index){
							return Utils.XSSEncode(value);
						}}, 
						{field: 'productCode', title: 'Mã sản phẩm', width:90, align: 'left', formatter: function(value, row, index){
							return Utils.XSSEncode(value);
						}},  
					    {field: 'productName', title: 'Tên sản phẩm', width:180, align:'left', formatter: function(value, row, index){
					    	return Utils.XSSEncode(value);
					    }},
					    {field: 'fromDate', title:'Ngày bắt đầu', width:110,align:'center', formatter: function(value, row, index){
					    	return '<input type="text" class="InputTextStyle InputText1Style vinput-date" value="'+row.dateSysStr.trim()+'" onchange="EquipmentGroupProduct.onchangeDateInputDl(this);" name ="fDateGridDl" id="fDateGridDl_'+row.productCode+'" style="width: 90px; margin-top: 5px;">';
					    }},
					    {field: 'toDate', title:'Ngày kết thúc', width:110,align:'center', formatter: function(value, row, index){
					    	return '<input type="text" class="InputTextStyle InputText1Style vinput-date" onchange="EquipmentGroupProduct.onchangeDateInputDl(this);" name ="fDateGridDl" id="tDateGridDl_'+row.productCode+'" style="width: 110px; margin-top: 5px;">';
					    }},
					    {field: 'productStatus', title:'Trạng thái', width:80, align:'left', formatter: function(value, row, index){
					    	return statusTypeText.parseValue(value);
					    }},
					    {field: 'P', hidden: true}
					]],
					onCheck: function(rowIndex, rowData) {
						var productCode = rowData.productCode;
						EquipmentGroupProduct._mapCheck.put(rowData.productCode, {
							productCode: rowData.productCode,
							fDate: $('#fDateGridDl_' + productCode).val().trim(),
							tDate: $('#tDateGridDl_' + productCode).val().trim()
						});
						$('#gridProductInsert').datagrid('selectRow', rowIndex);
					},
					onUncheck: function(rowIndex, rowData) {
						var productCode = rowData.productCode;
						if (EquipmentGroupProduct._mapCheck.keyArray.indexOf(productCode) > -1) {
							EquipmentGroupProduct._mapCheck.remove(productCode);
						}
						$('#gridProductInsert').datagrid('unselectRow', rowIndex);
					},
					onCheckAll: function(rows) {
						$('#gridProductInsert').datagrid('selectAll');
						var dataGrid = $('#gridProductInsert').datagrid('getData').rows;
						for(var i=0, size = dataGrid.length; i < size; i++){
							var productCode = dataGrid[i].productCode;
							EquipmentGroupProduct._mapCheck.put(productCode, {
								productCode: dataGrid[i].productCode,
								fDate: $('#fDateGridDl_' + productCode).val().trim(),
								tDate: $('#tDateGridDl_' + productCode).val().trim()
							});						
						}
					},
					onUncheckAll: function(rows) {
						EquipmentGroupProduct._mapCheck = new Map();
						$('#gridProductInsert').datagrid('unselectAll');
					},
					onClickRow: function(rowIndex, rowData) {
						var rowCbx = $('[name=productId]')[rowIndex];
						if (rowCbx != undefined && rowCbx != null && !rowCbx.checked) {
							$('#gridProductInsert').datagrid('unselectRow', rowIndex);
						}else{
							$('#gridProductInsert').datagrid('selectRow', rowIndex);
						}
						return true;
					},
			        onLoadSuccess: function(data){
						$('#permissionContainerGrid .datagrid-header-rownumber').html('STT');
						updateRownumWidthForDataGrid('gridProductInsert');
						$('.vinput-date').each(function() {
							$(this).width(90);
							VTUtilJS.applyDateTimePicker($(this));
						});
					 	$('#gridProductInsert').datagrid('uncheckAll').datagrid('unselectAll');
					 	$(window).resize();
			        }
				});
			}
		});
		$("#popupProductSearch").dialog('open');
	},
	
	/**
	 * Xu ly su kien on change dialog insert product
	 * 
	 * @author hunglm16
	 * @since May 22, 2015
	 * 
	 * @param {input} -> txt
	 * */
	onchangeDateInputDl: function (txt) {
		var productCode = $(txt).prop('id').replace(/fDateGridDl_/g,'').replace(/tDateGridDl_/g,'');
		if (EquipmentGroupProduct._mapCheck.keyArray.indexOf(productCode) > -1) {
			var rowItem = EquipmentGroupProduct._mapCheck.get(productCode);
			EquipmentGroupProduct._mapCheck.put(productCode, {
				productCode: rowItem.productCode,
				fDate: $('#fDateGridDl_' + productCode).val().trim(),
				tDate: $('#tDateGridDl_' + productCode).val().trim()
			});
		}
	},
	
	/**
	 * tim kiem san pham tren cay
	 * @author  tuannd20
	 * @return cay san pham theo tieu chi tim kiem
	 * 
	 * @author hunglm16
	 * @since May 21,2015
	 * @description Ra soat va update
	 * @return data Grid
	 */
	searchProductOnTree: function() {
		$('.ErrorMsgStyle').html('').hide();
		$('.SuccessMsgStyle').html('').hide();
		$(".easyui-dialog #errMsgProductDlg").html('').hide();
		var paramsSearch = {
				productCode: $('#productCodeDl').val().trim(),
				productName: $('#productNameDl').val().trim(),
				productInforId: $('#productInforDl').val(),
				status: $('#productStatusDl').val()
		};
		$('#gridProductInsert').datagrid('load', paramsSearch);
	},
	/**
	 * ham xu ly su kien bam button tim kiem nhom thiet bi
	 * @author tuannd20
	 * @return {void} 
	 * @date 05/01/2015
	 */
	searchEquipmentGroupHandler: function() {
		var equipmentGroupFilter = {
			equipmentGroupCode: $('#equipmentGroupCode').val().trim(),
			equipmentGroupName: $('#equipmentGroupName').val().trim(),
			status: $('#equipmentGroupStatus').val()
		};
		if (equipmentGroupFilter.equipmentGroupStatus == 'ALL') {
			delete equipmentGroupFilter.equipmentGroupStatus;
		}
		var params = {};
		convertToSimpleObject(params, equipmentGroupFilter, "equipmentGroupFilter");
		$('#' + EquipmentGroupProduct._mainGridOption.gridId).datagrid('load', params);
	},
	/**
	 * them san pham vao equipment_group
	 * @author tuannd20
	 * @return {void}
	 */
	addProductsToEquipmentGroup: function() {
		$('.ErrorMsgStyle').html('').hide();
		$('.SuccessMsgStyle').html('').hide();
		if (EquipmentGroupProduct._mapCheck == null || EquipmentGroupProduct._mapCheck.size() == 0) {
			$('#errMsgPopup3').html('Bạn chưa chọn sản phẩm nào.').show();
			return false;
		}
		var msg = '';
		var mapProductFDateStr = 'hunglm@16';
		var mapProductToDateStr = 'hunglm@16';
		var arr = EquipmentGroupProduct._mapCheck.keyArray;
		//Sap xep lai mang tang dang theo Ma San Pham
		var rowKeys = arr.sort();
		//Xu ly cho rows map dau tien
		var productItem = null;
		for (var i = 0, sizei = rowKeys.length; i < sizei; i++) {
			productItem = EquipmentGroupProduct._mapCheck.get(rowKeys[i]);
			msg = EquipmentGroupProduct.validateProductDateSingleRowDl(productItem.productCode, productItem.fDate, productItem.tDate);
			if (msg.length > 0) {
				$('#errMsgPopup3').html(msg).show();
				return false;
			}
			mapProductFDateStr = mapProductFDateStr + ',' + productItem.productCode.trim() + '_' + productItem.fDate.trim();
			if (productItem.tDate.trim().length > 0) {
				mapProductToDateStr = mapProductToDateStr + ',' + productItem.productCode.trim() + '_' + productItem.tDate.trim();
			} else {
				mapProductToDateStr = mapProductToDateStr + ',' + productItem.productCode.trim() + '_ddMMyyyy';
			}
		}
		mapProductFDateStr = mapProductFDateStr.replace(/hunglm@16,/g, '').trim();
		mapProductToDateStr = mapProductToDateStr.replace(/hunglm@16,/g, '').trim();
		if (mapProductFDateStr.length == 0) {
			$('#errMsgPopup3').html('Không xác định được Sản phẩm với Ngày bắt đầu').show();
			return false;
		}
		if (mapProductToDateStr.length == 0) {
			$('#errMsgPopup3').html('Không xác định được Sản phẩm với Ngày kết thúc').show();
			return false;
		}
		var dataModel = {};
		dataModel.mapProductFDate = mapProductFDateStr;
		dataModel.mapProductTDate = mapProductToDateStr; 
		dataModel.equipGroupId = EquipmentGroupProduct._idExpendRow;
		EquipmentGroupProduct.addProductsToEquipmentGroupEvent(dataModel, '/equipment-group-product/add-products');
		
		return false;
	},
	
	/**
	 * Cap nhat du lieu mang tinh chat lap de qui
	 * 
	 * @author hunglm16
	 * @sing May 22,2015
	 * 
	 * @description Danh cho su kien them san pham theo ham addProductsToEquipmentGroup
	 * */
	addProductsToEquipmentGroupEvent: function(dataModel, url, message) {
		$('.ErrorMsgStyle').html('').hide();
		$('.SuccessMsgStyle').html('').hide();
		var msg = 'Bạn có muốn cập nhật?';
		if (message != undefined && message != null && message.trim().length > 0) {
			msg = message.trim();
		}
		$.messager.confirm('Xác nhận', msg, function(r) {
			if (r) {
				Utils.saveData(dataModel, url, null, 'errMsgPopup3', function (data) {
					var ddv = $('#' + EquipmentGroupProduct._mainGridOption.gridId).datagrid('getRowDetail', EquipmentGroupProduct._rowGroupEquipIndex).find('table.ddv');
					$(ddv).datagrid("reload");
					var countMsg = '';
					if (data.countSuccess) {
						countMsg = data.countSuccess.toString() + ' dòng';
					}
					//Xu ly cho thanh cong
					var tm = setTimeout(function() {
						$('#successMsg').html('Lưu dữ liệu thành công ' + countMsg).show();
						$("#popupProductSearch").dialog("close");
					}, 2000);
					return false;
				}, null, null, null, function (data) {
					var ddv = $('#' + EquipmentGroupProduct._mainGridOption.gridId).datagrid('getRowDetail', EquipmentGroupProduct._rowGroupEquipIndex).find('table.ddv');
					$(ddv).datagrid("reload");
					//Xu ly ho that bai
					var countMsg = '';
					if (data.countSuccess) {
						countMsg = data.countSuccess.toString() + ' dòng';
					}
					if (data.flag == 1) {
						if (data.excProductCodes) {
							dataModel.excProductCodes = data.excProductCodes;							
						}
						//Xu ly cho thanh cong
						var msgDltmp = data.errMsg;
						if (countMsg.length > 0) {
							msgDltmp = 'Lưu dữ liệu thành công' + countMsg + '. ' + msgDltmp;
						}
						EquipmentGroupProduct.addProductsToEquipmentGroupEvent(dataModel, url, msgDltmp);
					} else {
						if (countMsg.length > 0) {
							$('#successMsgPopup3').html('Lưu dữ liệu thành công' + countMsg).show();
							var tm = setTimeout(function(){
								$('.SuccessMsgStyle').html('').hide();
								clearTimeout(tm);
							}, 2000);						
						}
						$('#errMsgPopup3').html(data.errMsg).show();
						return false;
					}
				});
			} else {
				$("#popupProductSearch").dialog("close");
			}
		});
	},
	
	/**
	 * Validate tinh hop le cua fDate, tDate cua san pham
	 * 
	 * @author hunglm16
	 * @since May 23,2015
	 * 
	 * @param {string} productCode
	 * @param {string} fDate
	 * @param {string} tDate
	 * */
	validateProductDateSingleRowDl: function (productCode, fDate, tDate) {
		if (productCode == undefined || productCode == null) {
			return 'Không xác định được Sản phẩm';
		}
		if (fDate == undefined || fDate == null ) {
			return 'Ngày bắt đầu của Sản phẩm ' + productCode + ' không hợp lệ';
		}
		if (fDate.trim().length == 0) {
			return 'Ngày bắt đầu của Sản phẩm ' + productCode + ' không được để trống';
		}
		if (fDate.trim().length >0 && !Utils.isDate(fDate.trim(), '/')) {
			return 'Ngày bắt đầu của Sản phẩm ' + productCode + ' không đúng định dạng dd/MM/yyyy';
		}
		if (tDate == undefined || tDate == null ) {
			return 'Ngày kết thúc của Sản phẩm ' + productCode + ' không hợp lệ';
		}
		if (tDate.trim().length >0 && !Utils.isDate(tDate.trim(), '/')) {
			return 'Ngày kết thúc của Sản phẩm ' + productCode + ' không đúng định dạng dd/MM/yyyy';
		}
		if (fDate.trim().length > 0 && tDate.trim().length > 0 && !Utils.compareDate(fDate, tDate)) {
			return 'Ngày bắt đầu của Sản phẩm ' + productCode + ' phải nhỏ hơn Ngày kết thúc ';
		}
		return '';
	},
	
	/**
	 * Validate tinh chung chom ngay cua cac san pham
	 * 
	 * @author hunglm16
	 * @since May 23,2015
	 * 
	 * @param {string} productCodei
	 * @param {string} fDatei
	 * @param {string} tDatei
	 * 
	 * @param {string} productCodej
	 * @param {string} fDatej
	 * @param {string} tDatej
	 * 
	 * @description Chi su dung ham nay khi da validate qua ham EquipmentGroupProduct.validateProductDateSingleRowDl(...)
	 * */
	validateProductDateDoublrRowDl: function (productCodei, productCodej, fDatei, tDatei, fDatej, tDatej) {
		if (productCodei == undefined || productCodei == null || productCodej == undefined || productCodej == null) {
			return 'Không xác định được Sản phẩm so sánh';
		}
		if (fDatei == undefined || fDatei == null) {
			return 'Không xác định được Ngày bắt đầu của Sản phẩm ' + productCodei;
		}
		if (tDatei == undefined || tDatei == null) {
			return 'Không xác định được Ngày kêt thúc của Sản phẩm ' + productCodei;
		}
		if (fDatej == undefined || fDatej == null) {
			return 'Không xác định được Ngày bắt đầu của Sản phẩm ' + productCodej;
		}
		if (tDatej == undefined || tDatej == null) {
			return 'Không xác định được Ngày kêt thúc của Sản phẩm ' + productCodej;
		}
		/**
		 * Xet truong hop productCodei
		 * @param fDatei is null
		 * @param tDatei is null
		 * */
		if (fDatei.trim().length == 0 && tDatei.trim().length == 0) {
			return 'Sản phẩm ' + productCodei + ' và ' + productCodej + ' có khoảng đoạn Ngày giao nhau';
		}
		if (fDatej.trim().length == 0 && tDatej.trim().length == 0) {
			return 'Sản phẩm ' + productCodei + ' và ' + productCodej + ' có khoảng đoạn Ngày giao nhau';
		}
		
		if ((Utils.compareDateForTowDate(tDatei, fDatej) < 0) || (Utils.compareDateForTowDate(tDatej, fDatei) < 0)) {
			return '';
		}
		return 'Sản phẩm ' + productCodei + ' và ' + productCodej + ' có khoảng đoạn Ngày giao nhau';
	},
	
	/**
	 * xoa san pham trong nhom thiet bi
	 * @author tuannd20
	 * @param  {string} productCode		ma san pham can xoa
	 * @param  {string} equipmentGroupCode ma nhom thiet bi
	 * @return {void}
	 * @date 05/01/2015
	 * 
	 * @author hunglm16
	 * @description ra soat va update
	 */
	deleteProductInEquipmentGroup: function(gridId, productCode, equipmentGroupCode, equipGroupProductId) {
		var dataModel = {
			productCodes: [productCode],
			equipGroupProductIds: [equipGroupProductId],
			equipmentGroupCode: equipmentGroupCode
		};

		Utils.addOrSaveRowOnGrid(dataModel, "/equipment-group-product/remove-product", null, gridId, 'errMsg', function(data) {
			$("#successMsg").html("Lưu dữ liệu thành công").show();
		});
	},
	/**
	 * an cac thong bao thanh cong/that bai
	 * @author tuannd20
	 * @return void
	 */
	hideMessages: function() {
		setTimeout(function() {
			$("#successMsg, #errMsg").html('').hide();
		}, 3000);
	}
};
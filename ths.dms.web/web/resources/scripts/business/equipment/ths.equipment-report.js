/**
 * JS thuc hien nghiep vu Bao cao thiet bi
 * 
 * @author hoanv25
 * @since  May 05, 2015
 * */
var EquipmentReport = {		
		
	/**
	* Bc 20.2 Xuat nhap ton thiet bi
	* 
	* @author thangnv31
	* @since 2016-03-16
	* */	
		exportXnttb: function() {
		var msg = '';
		$('#errMsg').html('').hide();
		if (msg.length == 0) {
			msg = Utils.getMessageOfRequireCheck('fDate','Từ ngày');
		}
		if (msg.length == 0) {
			msg = Utils.getMessageOfInvalidFormatDate('fDate','Từ ngày');
		}
		if (msg.length == 0) {
			msg = Utils.getMessageOfRequireCheck('tDate','Từ ngày');
		}
		if (msg.length == 0) {
			msg = Utils.getMessageOfInvalidFormatDate('tDate','Đến ngày');
		}
		var fDate = $('#fDate').val();
		var tDate = $('#tDate').val();
		if (!Utils.compareDate(fDate, tDate)) {
			msg = msgErr_fromdate_greater_todate;		
			$('#fDate').focus();
		}
		var currentTime = new Date();
		var month = currentTime.getMonth() + 1;
		var day = currentTime.getDate();
		var year = currentTime.getFullYear();
		if (!Utils.compareDate(fDate, day + '/' + month + '/' + year)) {
			msg = 'Từ ngày không được lớn hơn ngày hiện tại';
			$('#fDate').focus();
		}
		if (!Utils.compareDate(tDate, day + '/' + month + '/' + year)) {
			msg = 'Đến ngày không được lớn hơn ngày hiện tại';
			$('#tDate').focus();
		}
		if (msg.length > 0) {
			$('#errMsg').html(msg).show();
			return false;
		}
		var dataModel = new Object();		
		dataModel.fromDate = $('#fDate').val().trim();
		dataModel.toDate = $('#tDate').val().trim();
		ReportUtils.exportReport('/report/equipment/xnttb/export', dataModel);
	},
		
	/**
	 * Load danh sach nhom thiet bi theo don vi chon
	 * @author hoanv25
	 * @since May 05,2015
	 **/
	loadComboEquipGroup:function(url){
		$("#equipGroup").kendoMultiSelect({
			dataSource: {
				transport: {
	                read: {
	                    dataType: "json",
	                    url: url
	                }
	            }
			},
			filter: "contains",
	        dataTextField: "equipGroupCode",
	        dataValueField: "equipGroupId",
			itemTemplate: function(data, e, s, h, q) {
				return '<div class="tree-vnm" node-id="'+Utils.XSSEncode(data.equipGroupId)+'" style="width:230px"><span class="tree-vnm-text">'+Utils.XSSEncode(data.equipGroupCode) +'-' + Utils.XSSEncode(data.equipGroupName)+'</span></div>';	
			},
	        tagTemplate:  '#: data.equipGroupCode #',
	        value: [$('#equipGroupId').val()]
	    });	 
	},
	
	/**
	 * Load danh sach nhom thiet bi theo nam thay doi
	 * @author hoanv25
	 * @since May 05,2015
	 * 
	 * @author hunglm16
	 * @since May 15,2015
	 * @description ra soat va bo sung
	 **/
	changeYearByOnload: function() {
		$('#errMsg').hide();
		var msg = '';
		var year = $('#year').val().trim();
		if(msg.length == 0){
			msg = Utils.getMessageOfRequireCheck('year', 'Năm');
		}
		if (msg.length == 0) {
			msg = Utils.getMessageOfSpecialCharactersValidateEx('year', 'Năm', Utils._TF_NUMBER);
		}
		if(msg.length >0){
			$('#errMsg').html(msg).show();
			return false;
		}
		var params = new Object();		
		params.year = $('#year').val().trim();
		Utils.getJSONDataByAjaxNotOverlay(params, '/report/equipment/getPeriod', function(data){						
			$('#period').html("").change();
			if (data != undefined && data != null && data.rows != undefined && data.rows != null) {
				for (var i = 0, size = data.rows.length; i < size; i++) {
					var row = '';
					if (data.rows[i].status && data.rows[i].status == 2) {
						row = '<option value="'+data.rows[i].id+'" selected="selected">' + data.rows[i].name + ' 222222xxx</option>';
					} else {
						row = '<option value="'+data.rows[i].id+'">' + data.rows[i].name + '</option>';
					}
					$('#period').append(row);
				}
			}
			$('#period').change().show();
		}, null, null);	
	},
	
	/**
	 * Bao cao thu hoi thiet bi
	 * @author hoanv25
	 * @since May 05,2015
	 **/
	exportEQ_3_1_7_1: function() {
		$('#errMsg').hide();
		var msg = '';
		var shopId = $('#shopId').val(); // shop select combobox
		if (shopId == null || shopId.length <= 0) {
			msg = format(msgErr_required_field, 'Đơn vị');
		}
		var fDate = $('#fDate').val();
		var tDate =$('#tDate').val();
		
		if(msg.length == 0 && $('#fDate').val().trim().length == 0){
			msg = 'Giá trị "từ ngày" chưa được nhập';
			err = '#fDate';
		}
		
		if(msg.length == 0 && $('#fDate').val().trim().length>0 && !Utils.isDate($('#fDate').val(),'/') && $('#fDate').val() != '__/__/____' ){
			msg = 'Từ ngày không tồn tại hoặc không đúng định dạng dd/mm/yyyy.';
			err = '#fDate';
		}
		
		if(msg.length == 0 && $('#tDate').val().trim().length == 0){
			msg = 'Giá trị "đến ngày" chưa được nhập';
			err = '#tDate';
		}
		
		if(msg.length == 0 && $('#tDate').val().trim().length>0 && !Utils.isDate($('#tDate').val(),'/') && $('#tDate').val() != '__/__/____'){
			msg = 'Đến ngày không tồn tại hoặc không đúng định dạng dd/mm/yyyy.';
			err = '#tDate';
		}
		if(msg.length == 0 && fDate.length > 0 && tDate.length > 0 && !Utils.compareDate(fDate, tDate)) {
			msg = 'Khoảng thời gian từ ngày - đến ngày không hợp lệ. Giá trị đến ngày phải cùng hoặc sau giá trị từ ngày';
		}
		
		if (msg.length > 0) {
			$('#errMsg').html(msg).show();
			return false;
		}
		var dataModel = new Object();
		dataModel.lstShopId = shopId; // combobox chon 1
		// dataModel.lstEquipId = lstEquipId.join(',');
		var multidataGroupEquip = $("#equipGroup").data("kendoMultiSelect");
		var dataGroup = multidataGroupEquip.dataItems();
		var lstEquipId = null;
		if (dataGroup != null && dataGroup.length > 0) {
			lstEquipId = dataGroup[0].id;
			for (var i = 1, size = dataGroup.length; i < size; i++) {
				lstEquipId += "," + dataGroup[i].id;
			}
			dataModel.lstEquipId = lstEquipId;
		}
		var multidataCategoryEquip = $("#categoryEquip").data("kendoMultiSelect");
		var dataCategory = multidataCategoryEquip.dataItems();
		if (dataCategory != null && dataCategory.length > 0) {
			var categoryIdMultil = dataCategory[0].id;
			for (var i = 1, size = dataCategory.length; i < size; i++) {
				categoryIdMultil += "," + dataCategory[i].id;
			}
			dataModel.categoryIdMultil = categoryIdMultil;
		}
		dataModel.shopId = $('#curShopId').val();
		dataModel.fromDate = fDate;
		dataModel.toDate = tDate;
		ReportUtils.exportReport('/report/equipment/wvkd03f9/export', dataModel);
	},

	/**
	 * Xuat bao bao thiet bi 3.1.1.8 danh sach tu dong tu mat sua chua theo ky
	 * @author hoanv25
	 * @since May 06,2015
	 **/
	/**
	 * modified
	 * @author vuongmq
	 * @since 07/04/2016
	 **/
	exportEQ_3_1_1_8: function() {
		$('.ErrorMsgStyle').html('').hide();
		var msg = '';
		var shopId = $('#shopId').val(); // shop select combobox
		if (shopId == null || shopId.length <= 0) {
			msg = format(msgErr_required_field, 'Đơn vị');
		}
		if (msg.length == 0) {
			msg = Utils.getMessageOfRequireCheck('fDate', 'Từ ngày');
		}
		if (msg.length == 0) {
			msg = Utils.getMessageOfInvalidFormatDate('fDate', 'Từ ngày');
		}
		if (msg.length == 0) {
			msg = Utils.getMessageOfRequireCheck('tDate', 'Đến ngày');
		}
		if (msg.length == 0) {
			msg = Utils.getMessageOfInvalidFormatDate('tDate', 'Đến ngày');
		}
		var fromDate = $('#fDate').val();
		var toDate = $('#tDate').val();
		if (msg.length == 0 && !Utils.compareDate(fromDate, toDate)) {
			msg = 'Từ ngày không được lớn hơn đến ngày. Vui lòng nhập lại';
			$('#fDate').focus();
		}
		var curDate = ReportUtils.getCurrentDateString();
		if (msg.length == 0 && !Utils.compareDate(toDate, curDate)) {
			msg = 'Đến ngày không được lớn hơn ngày hiện tại. Vui lòng nhập lại';
			$('#tDate').focus();
		}
		if (msg.length > 0) {
			$('#errMsg').html(msg).show();
			return false;
		}
		var dataModel = new Object();
		dataModel.lstShopId = shopId; // combobox chon 1
		var multidataGroupEquip = $("#equipGroup").data("kendoMultiSelect");
		var dataGroup = multidataGroupEquip.dataItems();
		var groupMultil = null;
		if (dataGroup != null && dataGroup.length > 0) {
			groupMultil = dataGroup[0].id;
			for (var i = 1, size = dataGroup.length; i < size; i++) {
				groupMultil += "," + dataGroup[i].id;
			}
			dataModel.groupMultil = groupMultil;
		}
		var multidataCategoryEquip = $("#categoryEquip").data("kendoMultiSelect");
		var dataCategory = multidataCategoryEquip.dataItems();
		if (dataCategory != null && dataCategory.length > 0) {
			var categoryIdMultil = dataCategory[0].id;
			for (var i = 1, size = dataCategory.length; i < size; i++) {
				categoryIdMultil += "," + dataCategory[i].id;
			}
			dataModel.categoryIdMultil = categoryIdMultil;
		}
		dataModel.shopId = $('#curShopId').val();
		dataModel.fromDate = fromDate;
		dataModel.toDate = toDate;
		ReportUtils.exportReport('/report/equipment/wvkd03f10/export', dataModel);
	},
	
	/**
	 * Xuat bao cao thiet bi 3.1.1.10 danh sach tong hop tu dong tu mat theo ky
	 * @author hoanv25
	 * @since May 13,2015
	 **/
	exportEQ_3_1_1_10: function() {
		$('#errMsg').hide();
		var msg = '';
		var shopId = $('#shopId').val();
		if (shopId.trim().length == 0 || shopId.trim() == activeType.ALL) {
			msg = format(msgErr_required_field, 'Đơn vị');
		}
		var fDate = $('#fDate').val();
		var tDate =$('#tDate').val();
		if(msg.length == 0 && $('#fDate').val().trim().length>0 && !Utils.isDate($('#fDate').val(),'/') && $('#fDate').val() != '__/__/____' ){
			msg = 'Từ ngày không tồn tại hoặc không đúng định dạng dd/mm/yyyy.';
			err = '#fDate';
		}
		if(msg.length == 0 && $('#tDate').val().trim().length>0 && !Utils.isDate($('#tDate').val(),'/') && $('#tDate').val() != '__/__/____'){
			msg = 'Đến ngày không tồn tại hoặc không đúng định dạng dd/mm/yyyy.';
			err = '#tDate';
		}
		if(msg.length == 0 && fDate.length > 0 && tDate.length > 0 && !Utils.compareDate(fDate, tDate)) {
			msg = 'Từ ngày tạo phải là ngày trước hoặc cùng ngày với đến ngày tạo';
		}
		if (msg.length > 0) {
			$('#errMsg').html(msg).show();
			return false;
		}
		var dataModel = new Object();
		dataModel.shopId = shopId;
		dataModel.fromDate = fDate;
		dataModel.toDate = tDate;
		var multidataGroupEquip = $("#equipGroup").data("kendoMultiSelect");
		var dataGroup = multidataGroupEquip.dataItems();
		var lstEquipId = null;
		if(dataGroup != null && dataGroup.length >0 ){
			lstEquipId = dataGroup[0].id;
			for(var i = 1, size = dataGroup.length; i < size; i++){
				lstEquipId += "," + dataGroup[i].id;
			}
			dataModel.lstEquipId = lstEquipId;
		}
		var multidataCategoryEquip = $("#categoryEquip").data("kendoMultiSelect");
		var dataCategory = multidataCategoryEquip.dataItems();
		if(dataCategory != null && dataCategory.length > 0 ) {
			var categoryIdMultil = dataCategory[0].id;
			for (var i = 1, size = dataCategory.length; i < size; i++) {
				categoryIdMultil += "," + dataCategory[i].id;
			}
			dataModel.categoryIdMultil = categoryIdMultil;
		}
		dataModel.reportCode= $('#function_code').val(), 
		ReportUtils.exportReport('/report/equipment/wvkd03f12/export', dataModel);
	},
	
	/**
	 * Xuat bao cao thiet bi 3.1.1.1 danh sach tong hop kiem ke tu dong tu mat theo ky
	 * @author hoanv25
	 * @since May 20,2015
	 **/
	exportEQ_3_1_1_1: function() {
		$('#errMsg').hide();
		var msg = '';
		var multiShop = $("#shop").data("kendoMultiSelect");
		var dataShop = null;
		if (multiShop != null) {
			dataShop = multiShop.dataItems();
		}
		if (dataShop == null || dataShop.length <= 0) {
			msg = format(msgErr_required_field, 'Đơn vị');
		}	
		if(msg.length == 0){
			msg = Utils.getMessageOfRequireCheck('year', 'Năm');
		}
		if (msg.length == 0) {
			msg = Utils.getMessageOfSpecialCharactersValidateEx('year', 'Năm',Utils._TF_NUMBER);
		}
		if (msg.length == 0) {
			if ($('#period').val() == undefined || $('#period').val() == -1) {
				msg = "Không có kỳ xuất báo cáo";
			}
		}
		if (msg.length == 0 && $('#statistic').val().trim() == -1) {
			msg = "Vui lòng chọn Kiểm kê";
		}

		if (msg.length > 0) {
			$('#errMsg').html(msg).show();
			return false;
		}
		var dataModel = new Object();
		var lstShopId = [];
		var lstEquipId = [];
		for (var i = 0; i < dataShop.length; i++) {
			lstShopId.push(dataShop[i].id);
		}
		dataModel.lstShopId = lstShopId.join(',');
		dataModel.shopId = $('#curShopId').val();
		dataModel.period = $('#period').val().trim();
		if ($('#statistic').val().trim() != -1) {
			dataModel.statisticRecord = $('#statistic').val().trim();
		}
		dataModel.formatType = $('input[type=radio]:checked').val();
		var ck = $('input[type=radio]:checked').val();
		if (ck!= undefined && ck!=null && 'XLS'== ck) {
			ReportUtils.exportReport('/report/equipment/kk11/export', dataModel);
		} else {
			ReportUtils.exportReport('/report/equipment/kk11/pdf', dataModel);
		}
	},
	/**
	 * Xuat bao cao hinh anh kiem ke kk1.2
	 * @author phuongvm
	 * @since 18/09/2015
	 **/
	exportKK1_2: function() {
		var msg = '';
		$('#errMsg').html('').hide(); 
		
		var shopKendo = $("#shop").data("kendoMultiSelect");
		var lstShopId = shopKendo.value();
		var shopCttKendo = $("#cttb").data("kendoMultiSelect");
		var lstCTTId = shopCttKendo.value();
		if (lstShopId.length <= 0) {
			msg = format(msgErr_required_field, 'Đơn vị');
		}
		
		if (msg.length == 0) {
			msg = Utils.getMessageOfRequireCheck('fDate', 'Ngày chụp từ');
		}
		if (msg.length == 0) {
			msg = Utils.getMessageOfRequireCheck('tDate', 'Ngày chụp đến');
		}
		if (msg.length == 0) {
			msg = Utils.getMessageOfInvalidFormatDateNew('fDate', 'Ngày chụp từ');
		}
		if (msg.length == 0) {
			msg = Utils.getMessageOfInvalidFormatDateNew('tDate', 'Ngày chụp đến');
		}
		var fDate = $('#fDate').val().trim();
		var tDate = $('#tDate').val().trim();
		var tmpFDate = '';
		with (arr = fDate.split('/')) {
			tmpFDate = arr[0] + '/' + (Number(arr[1]) - 1) + '/' + arr[2];
		}
		var tmpTDate = '';
		with (arr = tDate.split('/')) {
			tmpTDate = arr[0] + '/' + (Number(arr[1]) - 1) + '/' + arr[2];
		}

		if (!Utils.compareDateNew(tmpFDate, tmpTDate)) {
			msg = msgErr_fromdate_greater_todate;
			$('#fDate').focus();
		}
		var currentTime = new Date();
		var month = currentTime.getMonth() + 1;
		var day = currentTime.getDate();
		var year = currentTime.getFullYear();
		var hour = currentTime.getHours();
		var minute = currentTime.getMinutes();
		if (month < 10) {
			month = '0' + month;
		}
		if (day < 10) {
			day = '0' + day;
		}
		if (hour < 10) {
			hour = '0' + hour;
		}
		if (minute < 10) {
			minute = '0' + minute;
		}
		if (!Utils.compareDateNew(tmpTDate, day + '/' + month + '/' + year + ' 23:59')) {
			msg = 'Ngày chụp đến phải là ngày trước hoặc cùng với ngày hiện tại';
			$('#fDate').focus();
		}
		
		if (msg.length > 0) {
			$('#errMsg').html(msg).show();
			return false;
		}
		
		var dataModel = new Object();

		dataModel.strListShopId = lstShopId.toString().trim();
		dataModel.staffCode = $('#staffCode').val().trim(); 
		dataModel.fromDate = fDate;
		dataModel.toDate = tDate;
		if (lstCTTId != undefined && lstCTTId != null && lstCTTId != "") {
			dataModel.lstCheck = lstCTTId.toString().trim();
		}
		ReportUtils.exportReport('/report/equipment/kk12/export', dataModel);
	},
	/**
	 * Xuat bao cao kiem ke kk1.3
	 * @author tamvnm
	 * @since Sep 29 2015
	 **/
	exportKK1_3: function() {
		var msg = '';
		$('#errMsg').html('').hide(); 
		var shopKendo = $("#shop").data("kendoMultiSelect");
		var lstShopId = shopKendo.value();
		if (lstShopId.length <= 0) {
			msg = format(msgErr_required_field, 'Đơn vị');
		}
		if (msg.length == 0) {
			msg = Utils.getMessageOfRequireCheck('fDate', 'Từ ngày');
		}
		if (msg.length == 0) {
			msg = Utils.getMessageOfInvalidFormatDate('fDate', 'Từ ngày');
		}
		if (msg.length == 0) {
			msg = Utils.getMessageOfRequireCheck('tDate', 'Đến ngày');
		}
		if (msg.length == 0) {
			msg = Utils.getMessageOfInvalidFormatDate('tDate', 'Đến ngày');
		}
		var fDate = $('#fDate').val();
		var tDate = $('#tDate').val();
		var curDate = ReportUtils.getCurrentDateString();
		if (msg.length == 0 && !Utils.compareDate(fDate, curDate)) {
			msg = 'Từ ngày phải nhỏ hơn hoặc bằng ngày hiện tại';
			$('#fDate').focus();
		}
		// if(msg.length == 0 && !Utils.compareDate(tDate, curDate)){
		// 	msg = 'Đến ngày phải nhỏ hơn hoặc bằng ngày hiện tại';
		// 	$('#tDate').focus();
		// }
		if (!Utils.compareDate(fDate, tDate)) {
			msg = 'Từ ngày phải nhỏ hơn đến ngày.';
			$('#fDate').focus();
		}
		if (msg.length > 0) {
			$('#errMsg').html(msg).show();
			return false;
		}
		
		var dataModel = new Object();
		dataModel.strListShopId = lstShopId.toString().trim();
		dataModel.staffCode = $('#staffCode').val().trim();
		dataModel.statisticCode = $('#statisticCode').val().trim();
		dataModel.fromDate = $('#fDate').val();
		dataModel.toDate = $('#tDate').val();
		if ($('#chkIsStop').length > 0) {
			dataModel.isStop = $('#chkIsStop').is(':checked');
		}
		ReportUtils.exportReport('/report/equipment/kk13/export', dataModel);
	},

	/**
	 * Xuat bao bao thiet bi 3.1.1.5; bao cao mat tu theo ky
	 * @author vuongmq
	 * @since 18/05/2015
	 **/
	exportBCMATTU1_5: function() {
		$('#errMsg').hide();
		var msg = '';
		var shopId = $('#shopId').val(); // shop select combobox
		if (shopId == null || shopId.length <= 0) {
			msg = format(msgErr_required_field, 'Đơn vị');
		}
		var fDate = $('#fDate').val();
		var tDate =$('#tDate').val();
		
		if(msg.length == 0 && $('#fDate').val().trim().length == 0){
			msg = 'Giá trị "từ ngày" chưa được nhập';
			err = '#fDate';
		}
		
		if(msg.length == 0 && $('#fDate').val().trim().length>0 && !Utils.isDate($('#fDate').val(),'/') && $('#fDate').val() != '__/__/____' ){
			msg = 'Từ ngày không tồn tại hoặc không đúng định dạng dd/mm/yyyy.';
			err = '#fDate';
		}
		
		if(msg.length == 0 && $('#tDate').val().trim().length == 0){
			msg = 'Giá trị "đến ngày" chưa được nhập';
			err = '#tDate';
		}
		
		if(msg.length == 0 && $('#tDate').val().trim().length>0 && !Utils.isDate($('#tDate').val(),'/') && $('#tDate').val() != '__/__/____'){
			msg = 'Đến ngày không tồn tại hoặc không đúng định dạng dd/mm/yyyy.';
			err = '#tDate';
		}
		if(msg.length == 0 && fDate.length > 0 && tDate.length > 0 && !Utils.compareDate(fDate, tDate)) {
			msg = 'Khoảng thời gian từ ngày - đến ngày không hợp lệ. Giá trị đến ngày phải cùng hoặc sau giá trị từ ngày';
		}
		// khong can kiem tra vi lay tat cac cac nhom thiet bi
		/*if (dataCustomer == null || (dataCustomer != null && dataCustomer.length <= 0)) {
			msg = format(msgErr_required_field, 'Nhóm thiết bị');
		}*/
		if (msg.length > 0) {
			$('#errMsg').html(msg).show();
			return false;
		}
		var dataModel = new Object();
		// var lenCus = dataCustomer.length;
		var multidataGroupEquip = $("#equipGroup").data("kendoMultiSelect");
		var dataGroup = multidataGroupEquip.dataItems();
		var lstEquipId = null;
		if (dataGroup != null && dataGroup.length > 0) {
			lstEquipId = dataGroup[0].id;
			for (var i = 1, size = dataGroup.length; i < size; i++) {
				lstEquipId += "," + dataGroup[i].id;
			}
			dataModel.lstEquipId = lstEquipId;
		}

		var multidataCategoryEquip = $("#categoryEquip").data("kendoMultiSelect");
		var dataCategory = multidataCategoryEquip.dataItems();
		if (dataCategory != null && dataCategory.length > 0) {
			var categoryIdMultil = dataCategory[0].id;
			for (var i = 1, size = dataCategory.length; i < size; i++) {
				categoryIdMultil += "," + dataCategory[i].id;
			}
			dataModel.categoryIdMultil = categoryIdMultil;
		}
		// for (var i = 0; i < lenCus; i++) {
		// 	lstEquipId.push(dataCustomer[i].equipGroupId);
		// }
		dataModel.lstShopId = shopId; // combobox chon 1
		// dataModel.lstEquipId = lstEquipId.join(',');
		dataModel.shopId = $('#curShopId').val();
		dataModel.fromDate = fDate;
		dataModel.toDate = tDate;
		ReportUtils.exportReport('/report/equipment/bcmattu1_5/export', dataModel);
	},

	/**
	 * 3.1.1.3	[TL1.1] Danh sách tủ đông, tủ mát thanh lý theo kỳ 
	 * @author tamvnm
	 * @since 15/05/2015
	 **/
	exportTL1_1: function() {
		$('#errMsg').hide();
		var msg = '';
		var shopId = $('#shopId').val();
		if (shopId.trim().length == 0 || shopId.trim() == activeType.ALL) {
			msg = format(msgErr_required_field, 'Đơn vị');
		}
		var fDate = $('#fDate').val();
		var tDate =$('#tDate').val();
		if(msg.length == 0 && $('#fDate').val().trim().length>0 && !Utils.isDate($('#fDate').val(),'/') && $('#fDate').val() != '__/__/____' ){
			msg = 'Từ ngày không tồn tại hoặc không đúng định dạng dd/mm/yyyy.';
			err = '#fDate';
		}
		if(msg.length == 0 && $('#tDate').val().trim().length>0 && !Utils.isDate($('#tDate').val(),'/') && $('#tDate').val() != '__/__/____'){
			msg = 'Đến ngày không tồn tại hoặc không đúng định dạng dd/mm/yyyy.';
			err = '#tDate';
		}
		if(msg.length == 0 && fDate.length > 0 && tDate.length > 0 && !Utils.compareDate(fDate, tDate)) {
			msg = 'Từ ngày tạo phải là ngày trước hoặc cùng ngày với đến ngày tạo';
		}
		if (msg.length > 0) {
			$('#errMsg').html(msg).show();
			return false;
		}
		var dataModel = new Object();
		dataModel.shopId = shopId;
		dataModel.fromDate = fDate;
		dataModel.toDate = tDate;
		var multidataGroupEquip = $("#equipGroup").data("kendoMultiSelect");
		var dataGroup = multidataGroupEquip.dataItems();
		var lstEquipId = null;
		if(dataGroup != null && dataGroup.length >0 ){
			lstEquipId = dataGroup[0].id;
			for(var i = 1, size = dataGroup.length; i < size; i++){
				lstEquipId += "," + dataGroup[i].id;
			}
			dataModel.lstEquipId = lstEquipId;
		}
		var multidataCategoryEquip = $("#categoryEquip").data("kendoMultiSelect");
		var dataCategory = multidataCategoryEquip.dataItems();
		if(dataCategory != null && dataCategory.length > 0 ) {
			var categoryIdMultil = dataCategory[0].id;
			for (var i = 1, size = dataCategory.length; i < size; i++) {
				categoryIdMultil += "," + dataCategory[i].id;
			}
			dataModel.categoryIdMultil = categoryIdMultil;
		}
		dataModel.reportCode= $('#function_code').val(), 
		ReportUtils.exportReport('/report/equipment/tl1_1/export', dataModel);
	},

	/**
	 * 3.1.1.9	[WV-KD-03-F11] Danh sách khách hàng mượn tủ 
	 * @author tamvnm
	 * @since 20/05/2015
	 **/
	exportWVKD03F11: function() {
		$('#errMsg').hide();
		var msg = '';
		var multiShop = $("#shop").data("kendoMultiSelect");
		var dataShop = null;
		if (multiShop != null) {
			dataShop = multiShop.dataItems();
		}
		if (dataShop == null || dataShop.length <= 0) {
			msg = format(msgErr_required_field, 'Đơn vị');
		}
		var typeGG = $('#infoExport').val();
		var fromDate = '';
		var toDate = '';
		if (activeType.RUNNING == typeGG) {
			fromDate = $('#fromDate').val();
			if (msg.length == 0 && fromDate != '' && !Utils.isDate(fromDate)) {
				msg = 'Từ ngày không tồn tại hoặc không đúng định dạng dd/mm/yyyy.';
				$('#fromDate').focus();
			}
			toDate = $('#toDate').val();
			if (msg.length == 0 && toDate != '' && !Utils.isDate(toDate)) {
				msg = 'Đến ngày không tồn tại hoặc không đúng định dạng dd/mm/yyyy.';
				$('#toDate').focus();
			}
			if (msg.length == 0 && !Utils.compareDate(fromDate, toDate)) {
				msg = 'Từ ngày không được lớn hơn đến ngày. Vui lòng nhập lại';
				$('#fromDate').focus();
			}
			var curDate = ReportUtils.getCurrentDateString();
			if (msg.length == 0 && !Utils.compareDate(toDate, curDate)) {
				msg = 'Đến ngày không được lớn hơn ngày hiện tại. Vui lòng nhập lại';
				$('#toDate').focus();
			}
		}
		if (msg.length > 0) {
			$('#errMsg').html(msg).show();
			return false;
		}
		var dataModel = new Object();
		var lstShopId = [];
		for (var i = 0; i < dataShop.length; i++) {
			lstShopId.push(dataShop[i].shopId);
		}
		dataModel.lstShopId = lstShopId.join(',');
		dataModel.typeGG = typeGG;
		dataModel.fromDate = fromDate;
		dataModel.toDate = toDate;
		dataModel.shopId = $('#curShopId').val();
		ReportUtils.exportReport('/report/equipment/wvkd03f11/export', dataModel);
	},
	
	/**
	 * Xuat baocao 3.1.1.4 [SC1.1] Báo cáo phiếu sửa chữa chưa thực hiện
	 * @author Datpv4
	 * @since May 05,2015
	 **/
	/**
	 * modified
	 * @author vuongmq
	 * @since 04/04/2016
	 **/
	exportBCPSCCHT: function() {
		$('.ErrorMsgStyle').html('').hide();
		var msg = '';
		var shopId = $('#shopId').val(); // shop select combobox
		if (shopId == null || shopId.length <= 0) {
			msg = format(msgErr_required_field, 'Đơn vị');
		}
		if (msg.length == 0) {
			msg = Utils.getMessageOfRequireCheck('fDate', 'Từ ngày');
		}
		if (msg.length == 0) {
			msg = Utils.getMessageOfInvalidFormatDate('fDate', 'Từ ngày');
		}
		if (msg.length == 0) {
			msg = Utils.getMessageOfRequireCheck('tDate', 'Đến ngày');
		}
		if (msg.length == 0) {
			msg = Utils.getMessageOfInvalidFormatDate('tDate', 'Đến ngày');
		}
		var fromDate = $('#fDate').val();
		var toDate = $('#tDate').val();
		if (msg.length == 0 && !Utils.compareDate(fromDate, toDate)) {
			msg = 'Từ ngày không được lớn hơn đến ngày. Vui lòng nhập lại';
			$('#fDate').focus();
		}
		var curDate = ReportUtils.getCurrentDateString();
		if (msg.length == 0 && !Utils.compareDate(toDate, curDate)) {
			msg = 'Đến ngày không được lớn hơn ngày hiện tại. Vui lòng nhập lại';
			$('#tDate').focus();
		}
		if (msg.length > 0) {
			$('#errMsg').html(msg).show();
			return false;
		}
		var dataModel = new Object();
		var multidataGroupEquip = $("#equipGroup").data("kendoMultiSelect");
		var dataGroup = multidataGroupEquip.dataItems();
		var groupMultil = null;
		if (dataGroup != null && dataGroup.length > 0) {
			groupMultil = dataGroup[0].id;
			for (var i = 1, size = dataGroup.length; i < size; i++) {
				groupMultil += "," + dataGroup[i].id;
			}
			dataModel.groupMultil = groupMultil;
		}

		var multidataCategoryEquip = $("#categoryEquip").data("kendoMultiSelect");
		var dataCategory = multidataCategoryEquip.dataItems();
		if (dataCategory != null && dataCategory.length > 0) {
			var categoryIdMultil = dataCategory[0].id;
			for (var i = 1, size = dataCategory.length; i < size; i++) {
				categoryIdMultil += "," + dataCategory[i].id;
			}
			dataModel.categoryIdMultil = categoryIdMultil;
		}
		dataModel.lstShopId = shopId; // combobox chon 1
		dataModel.shopId = $('#curShopId').val();
		dataModel.fromDate = fromDate;
		dataModel.toDate = toDate;
		ReportUtils.exportReport('/report/equipment/cth14/export', dataModel);
	},
	
	/**
	 * Xuat baocao 3.1.1.6 [WV-KD-03-F5] Bao cao muon thiet bi
	 * @author Datpv4
	 * @since May 13,2015
	 **/
	exportEQ_3_1_1_6: function() {
		$('#errMsg').hide();
		var msg = '';
		var shopId = $('#shopId').val(); // shop select combobox
		if (shopId == null || shopId.length <= 0) {
			msg = format(msgErr_required_field, 'Đơn vị');
		}
		
		var fDate = $('#fDate').val();
		var tDate =$('#tDate').val();
		
		if(msg.length == 0 && $('#fDate').val().trim().length == 0){
			msg = 'Giá trị "từ ngày" chưa được nhập';
			err = '#fDate';
		}
		
		if(msg.length == 0 && $('#fDate').val().trim().length>0 && !Utils.isDate($('#fDate').val(),'/') && $('#fDate').val() != '__/__/____' ){
			msg = 'Từ ngày không tồn tại hoặc không đúng định dạng dd/mm/yyyy.';
			err = '#fDate';
		}
		
		if(msg.length == 0 && $('#tDate').val().trim().length == 0){
			msg = 'Giá trị "đến ngày" chưa được nhập';
			err = '#tDate';
		}
		
		if(msg.length == 0 && $('#tDate').val().trim().length>0 && !Utils.isDate($('#tDate').val(),'/') && $('#tDate').val() != '__/__/____'){
			msg = 'Đến ngày không tồn tại hoặc không đúng định dạng dd/mm/yyyy.';
			err = '#tDate';
		}
		if(msg.length == 0 && fDate.length > 0 && tDate.length > 0 && !Utils.compareDate(fDate, tDate)) {
			msg = 'Khoảng thời gian từ ngày - đến ngày không hợp lệ. Giá trị đến ngày phải cùng hoặc sau giá trị từ ngày';
		}
		
		if (msg.length > 0) {
			$('#errMsg').html(msg).show();
			return false;
		}
		
		var dataModel = new Object();
		dataModel.lstShopId = shopId;
		dataModel.shopId = $('#curShopId').val();
		dataModel.fromDate = fDate;
		dataModel.toDate = tDate;
		// dataModel.lstEquipId = lstEquipId.join(',');
		var multidataGroupEquip = $("#equipGroup").data("kendoMultiSelect");
		var dataGroup = multidataGroupEquip.dataItems();
		var lstEquipId = null;
		if(dataGroup != null && dataGroup.length >0 ){
			lstEquipId = dataGroup[0].id;
			for(var i = 1, size = dataGroup.length; i < size; i++){
				lstEquipId += "," + dataGroup[i].id;
			}
			dataModel.lstEquipId = lstEquipId;
		}
		var multidataCategoryEquip = $("#categoryEquip").data("kendoMultiSelect");
		var dataCategory = multidataCategoryEquip.dataItems();
		if(dataCategory != null && dataCategory.length > 0 ) {
			var categoryIdMultil = dataCategory[0].id;
			for (var i = 1, size = dataCategory.length; i < size; i++) {
				categoryIdMultil += "," + dataCategory[i].id;
			}
			dataModel.categoryIdMultil = categoryIdMultil;
		}
		dataModel.reportCode= $('#function_code').val(), 
		
		ReportUtils.exportReport('/report/equipment/wvkd03f5/export', dataModel);
	},
	
	/**
	 * Bao cao nhap xuat ton tu mat, tu dong ky [ten ky]
	 * 
	 * @author hunglm16
	 * @since May 18, 2015
	 * */
	exportXNT1D1: function(){
		var msg = '';
		$('#errMsg').html('').hide();
		var periodId = $('#period').val();
		if (isNaN(periodId) || Number(periodId) < 0) {
			msg = 'Kỳ không hợp lệ';
		}
		if (msg.length > 0) {
			$('#errMsg').html(msg).show();
			return false;
		}

		var multiStockEquip = $("#stockEquip").data("kendoMultiSelect");
		var dataStock = multiStockEquip.dataItems();

		var multidataGroupEquip = $("#groupEquip").data("kendoMultiSelect");
		var dataGroup = multidataGroupEquip.dataItems();

		var multidataCategoryEquip = $("#categoryEquip").data("kendoMultiSelect");
		var dataCategory = multidataCategoryEquip.dataItems();

		var dataModel = new Object();

		if (dataStock != null && dataStock.length > 0) {
			var stockIdMultil = dataStock[0].equipStockId;
			for (var i = 1, size = dataStock.length; i < size; i++) {
				stockIdMultil += "," + dataStock[i].equipStockId;
			}
			dataModel.stockMultil = stockIdMultil;
		}

		if (dataGroup != null && dataGroup.length > 0) {
			var groupIdMultil = dataGroup[0].id;
			for (var i = 1, size = dataGroup.length; i < size; i++) {
				groupIdMultil += "," + dataGroup[i].id;
			}
			dataModel.groupMultil = groupIdMultil;
		}
		if (dataCategory != null && dataCategory.length > 0) {
			var categoryIdMultil = dataCategory[0].id;
			for (var i = 1, size = dataCategory.length; i < size; i++) {
				categoryIdMultil += "," + dataCategory[i].id;
			}
			dataModel.categoryIdMultil = categoryIdMultil;
		}
		dataModel.equipPeriodId = periodId;
		dataModel.status = $('#period').val();
		//		dataModel.formatType = $('input[type=radio]:checked').val();
		ReportUtils.exportReport('/report/equipment/equipReportXnt1d1/export', dataModel);
	},
	
	/**
	 * Bao cao nhap xuat ton tu mat, tu dong theo trang thai ky [ten ky]
	 * 
	 * @author hunglm16
	 * @since July 03, 2015
	 * */
	exportXNT1D2: function(){
		var msg = '';
		$('#errMsg').html('').hide();
		var periodId = $('#period').val();
		if (isNaN(periodId) || Number(periodId) < 0) {
			msg = 'Kỳ không hợp lệ';
		}
		if (msg.length > 0) {
			$('#errMsg').html(msg).show();
			return false;
		}

		var multiStockEquip = $("#stockEquip").data("kendoMultiSelect");
		var dataStock = multiStockEquip.dataItems();

		var multidataGroupEquip = $("#groupEquip").data("kendoMultiSelect");
		var dataGroup = multidataGroupEquip.dataItems();

		var multidataCategoryEquip = $("#categoryEquip").data("kendoMultiSelect");
		var dataCategory = multidataCategoryEquip.dataItems();

		var dataModel = new Object();

		if (dataStock != null && dataStock.length > 0) {
			var stockIdMultil = dataStock[0].equipStockId;
			for (var i = 1, size = dataStock.length; i < size; i++) {
				stockIdMultil += "," + dataStock[i].equipStockId;
			}
			dataModel.stockMultil = stockIdMultil;
		}

		if (dataGroup != null && dataGroup.length > 0) {
			var groupIdMultil = dataGroup[0].id;
			for (var i = 1, size = dataGroup.length; i < size; i++) {
				groupIdMultil += "," + dataGroup[i].id;
			}
			dataModel.groupMultil = groupIdMultil;
		}
		if (dataCategory != null && dataCategory.length > 0) {
			var categoryIdMultil = dataCategory[0].id;
			for (var i = 1, size = dataCategory.length; i < size; i++) {
				categoryIdMultil += "," + dataCategory[i].id;
			}
			dataModel.categoryIdMultil = categoryIdMultil;
		}
		dataModel.equipPeriodId = periodId;
		dataModel.status = $('#status').val();
		//		dataModel.formatType = $('input[type=radio]:checked').val();
		ReportUtils.exportReport('/report/equipment/equipReportXnt1d2/export', dataModel);
	},
	
	/**
	 * Bao cao phat sinh doanh so tu mat tu dong ds1.1
	 * 
	 * @author phuongvm
	 * @since 16/12/2015
	 * */
	exportDS1_1: function() {
		var msg = '';
		$('#errMsg').hide();
		var multiShop = $("#shop").data("kendoMultiSelect");
		var dataShop = null;
		if (multiShop != null) {
			dataShop = multiShop.dataItems();
		}
		if (dataShop == null || dataShop.length <= 0) {
			msg = format(msgErr_required_field, 'Đơn vị');
		}
		if (msg.length == 0) {
			msg = Utils.getMessageOfRequireCheck('tDate', 'Đến ngày');
		}
		if (msg.length == 0) {
			msg = Utils.getMessageOfInvalidFormatDate('tDate', 'Đến ngày');
		}
		var tDate = $('#tDate').val().trim();
		// if (msg.length == 0 && !Utils.compareCurrentDate(tDate)) {
		// 	msg = 'Đến ngày phải nhỏ hơn hoặc bằng ngày hiện tại';
		// 	$('#tDate').focus();
		// }

		if (msg.length > 0) {
			$('#errMsg').html(msg).show();
			return false;
		}

		var dataModel = new Object();

		var lstShopId = [];
		for (var i = 0, sz = dataShop.length; i < sz; i++) {
			lstShopId.push(dataShop[i].id);
		}
		dataModel.lstShopId = lstShopId.join(',');
		dataModel.toDate = tDate;
		ReportUtils.exportReport('/report/equipment/ds1_1/export', dataModel);
	},
	
	/**
	 * GD 1.1 Bao cao cac giao dich phat sinh
	 * 
	 * @author hunglm16
	 * @since August 24, 2015
	 * */
	reportGD1D1BCCGDPS: function(){
		var msg = '';
		$('#errMsg').html('').hide();
		var shopKendo = $("#shop").data("kendoMultiSelect");
		var lstShopId = shopKendo.value();
		if (msg.length == 0 && lstShopId.length == 0) {
			msg = 'Vui lòng chọn đơn vị';
		}
		if (msg.length == 0) {
			msg = Utils.getMessageOfRequireCheck('fromDate','Từ ngày');
		}
		if (msg.length == 0) {
			msg = Utils.getMessageOfInvalidFormatDate('fromDate','Từ ngày');
		}
		if (msg.length == 0) {
			msg = Utils.getMessageOfRequireCheck('toDate','Đến ngày');
		}
		if (msg.length == 0) {
			msg = Utils.getMessageOfInvalidFormatDate('toDate','Đến ngày');
		}
		var fDate = $('#fromDate').val();
		var tDate = $('#toDate').val();
		if (!Utils.compareDate(fDate, tDate)) {
			msg = msgErr_fromdate_greater_todate;		
			$('#fDate').focus();
		}
		if (msg.length > 0) {
			$('#errMsg').html(msg).show();
			return false;
		}
		

		
		var dataModel = new Object();
		dataModel.lstShopId = lstShopId.toString().trim();
		dataModel.fromDate = $('#fromDate').val();
		dataModel.toDate = $('#toDate').val();
		dataModel.typeGG = $('#typeGG').val();
//		dataModel.formatType = $('input[type=radio]:checked').val();
		ReportUtils.exportReport('/report/equipment/equipReportCgdps/export', dataModel);
	},
	
	/**
	 * Kiem ke thiet bi 1.1
	 * @author vuongmq
	 * @since 28/03/2016 
	 */
	exportKKTB11: function() {
		$('.ErrorMsgStyle').html('').hide();
		var msg = '';
		var shopId = $('#shopId').val();
		if (shopId.trim().length == 0 || shopId.trim() == activeType.ALL) {
			msg = format(msgErr_required_field, 'Đơn vị');
		}
		var statisticCode = $('#cbxProgramKK').combobox('getValue');
		if (msg.length == 0) {
			if (isNullOrEmpty(statisticCode)) {
				msg = 'Vui lòng chọn chương trình';
			}
		}
		/*if (msg.length == 0) {
			msg = Utils.getMessageOfRequireCheck('fDate', 'Từ ngày');
		}
		if (msg.length == 0) {
			msg = Utils.getMessageOfRequireCheck('tDate', 'Đến ngày');
		}
		if (msg.length == 0) {
			msg = Utils.getMessageOfInvalidFormatDate('fDate', 'Từ ngày');
		}
		if (msg.length == 0) {
			msg = Utils.getMessageOfInvalidFormatDate('tDate', 'Đến ngày');
		}
		var fDate = $('#fDate').val();
		var tDate = $('#tDate').val();
		if (!Utils.compareDate(fDate, tDate)) {
			msg = js_fromdate_todate_validate;		
			$('#fDate').focus();
		}
		if (msg.length == 0) {
			msg = Utils.compareCurrentDateEx('tDate', 'Đến ngày');
		}*/
		if (msg.length > 0) {
			$('#errMsg').html(msg).show();
			return false;
		}
		var dataModel = new Object();
		dataModel.lstShopId = shopId;
		dataModel.statisticCode = statisticCode;
		ReportUtils.exportReport('/report/equipment/kktb11/export', dataModel);
	},
};
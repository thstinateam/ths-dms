/**
 * JS thuc hien nghiep vu Quan Ly De Nghi Muon Thiet Bi
 *
 * @author hoanv25
 * @since  March 09,2015
 * */
var EquipmentProposalBorrow = {
	_mapRecored: null,
	_editIndex: null,
	_lstEquipCategory: null,
	_lstHealthStatus: null,
	_lstProvince: null,
	_mapDelivery: null,
	/**
	 * Tim kiem danh sach de nghi muon tu
	 *
	 * @author hunglm16
	 * @since December 06, 2015
	 *
	 * @author update nhutnn
	 * @since update 02/07/2015
	 * */
	searchProposalBorrow: function() {
		$('.ErrorMsgStyle').html("").hide();
		var msg = '';
		var params = new Object();
		var multiShop = $("#shop").data("kendoMultiSelect");
		var dataShop = multiShop.dataItems();
		if (dataShop != null && dataShop.length > 0) {
			var lstShop = new Array();
			//var curShopCode = $('#curShopCode').val();
			for (var i = 0; i < dataShop.length; i++) {
				//if (dataShop[i].shopCode != curShopCode) {
					lstShop.push(dataShop[i].shopCode);
				//}
			}
			params.lstShop = lstShop.join(",");
		} else {
			msg = "Bạn chưa chọn Đơn vị!";
			err = '#shop';
		}
		if (msg.length == 0) {
			msg = Utils.getMessageOfSpecialCharactersValidate('code', 'Mã biên bản', Utils._CODE);
			err = "#code";
		}
		if (msg.length == 0) {
			msg = Utils.getMessageOfInvalidFormatDate("fDate","Từ ngày");
			err = "#fDate";
		}
		if (msg.length == 0) {
			msg = Utils.getMessageOfInvalidFormatDate("tDate","Đến ngày");
			err = "#tDate";
		}
		var fDate = $('#fDate').val().trim();
		var tDate = $('#tDate').val().trim();
		if (msg.length == 0 && fDate.length > 0 && tDate.length > 0 && !Utils.compareDate(fDate, tDate)) {
			msg = 'Từ ngày phải là ngày trước hoặc cùng ngày với đến ngày';
			err = '#fDate';
		}
		if (msg.length > 0) {
			$('#errLendMsg').html(msg).show();
			$(err).focus();
			return false;
		}
		params.code = $('#code').val().trim();
		params.superviseCode = $('#superviseCode').val().trim();
		params.statusRecord = $('#status').val().trim();
		params.flagRecordStatus = $('#statusApproved_flag').val();
		params.fromDate = $('#fDate').val().trim();
		params.toDate = $('#tDate').val().trim();
		params.statusDelivery = $('#statusDelivery').val().trim();
		EquipmentProposalBorrow._mapRecored = new Map();
		$('#gridLendEquip').datagrid("uncheckAll");
		$('#gridLendEquip').datagrid("load", params);
		return false;
	},
	/**
	 * Luu thay doi trang thai bien ban
	 * @author hoanv25
	 * @since March 10,2015
	 * 
	 * @author update nhutnn
	 * @since 03/07/2015
	 */
	saveRecordChange: function() {
		$('.ErrorMsgStyle').html("").hide();
		var rows = $('#gridLendEquip').datagrid('getRows');
		if (rows.length == 0) {
			$('#errLendMsg').html('Không có dữ liệu để lưu').show();
			return false;
		}
		if (EquipmentProposalBorrow._mapRecored.keyArray.length == 0) {
			$('#errLendMsg').html('Bạn chưa chọn biên bản. Vui lòng chọn biên bản!').show();
			return false;
		}
		var statusRecordChange = $('#statusRecordLabel').val();
		if (statusRecordChange == undefined || statusRecordChange == null || statusRecordChange == "") {
			$('#errLendMsg').html('Bạn chưa chọn trạng thái để lưu biên bản!').show();
			return false;
		}

		var lstIdRecord = new Array();
		for (var i = 0; i < EquipmentProposalBorrow._mapRecored.keyArray.length; i++) {
			lstIdRecord.push(EquipmentProposalBorrow._mapRecored.keyArray[i]);
		}
		var params = new Object();
		params.lstIdRecord = lstIdRecord;
		params.statusRecord = statusRecordChange;
		Utils.addOrSaveData(params, '/equipment-management-proposal-borrow/save-record-change', null, 'errLendMsg', function(data) {
			if (data.error) {
				$('#errLendMsg').html(data.errMsg).show();
				return;
			}
			if (data.lstRecordError != null && data.lstRecordError.length > 0) {
				VCommonJS.showDialogList({
					dialogInfo: {
						title: 'Lỗi'
					},
					data: data.lstRecordError,
					columns: [
						[{field: 'formCode', title: 'Mã biên bản', align: 'left', width: 150, sortable: false, resizable: false}, 
						{field: 'formStatusErr', title: 'Trạng thái biên bản', align: 'center', width: 80, sortable: false, resizable: false, formatter: function(value, row, index) {
							var html = "";
							if (row.formStatusErr == 1) {
								html = '<img src="/resources/images/icon_delete.png" height="20" width="20" style="margin-right:5px;" title="Error">';
							}
							return html;
						}},
						/*{field:'periodError', title:'Kỳ của biên bản', align:'center', width:80, sortable:false, resizable:false,formatter: function(value, row, index){
							var html="";
							if(row.periodError == 1){
								html = '<img src="/resources/images/icon_delete.png" height="20" width="20" style="margin-right:5px;" title="Error">';
							}
							return html;
							
						}}*/
						]
					]
				});
			} else {
				$('#successMsgLend').html('Lưu dữ liệu thành công').show();
				setTimeout(function() {
					$('#successMsgLend').html('').hide();
				}, 3000);
			}
			EquipmentProposalBorrow._mapRecored = new Map();
			$('#gridLendEquip').datagrid('reload');
			$('#gridLendEquip').datagrid('uncheckAll');
		}, null, null, null, "Bạn có muốn cập nhật ?", null);
	},
	/**
	 * Xuat file excel
	 * @author nhutnn
	 * @since 03/07/2015
	 */
	exportEquipLend: function() {
		$('.ErrorMsgStyle').html("").hide();
		if (EquipmentProposalBorrow._mapRecored == null || EquipmentProposalBorrow._mapRecored.keyArray.length == 0) {
			$('#errLendMsg').html('Bạn chưa chọn biên bản. Vui lòng chọn biên bản!').show();
			return false;
		}

		var lstIdRecord = new Array();
		for (var i = 0; i < EquipmentProposalBorrow._mapRecored.keyArray.length; i++) {
			lstIdRecord.push(EquipmentProposalBorrow._mapRecored.keyArray[i]);
		}
		var params = new Object();
		params.lstIdRecord = lstIdRecord;		
		ReportUtils.exportReport('/equipment-management-proposal-borrow/export-excel', params, "errLendMsg");
		$('#gridLendEquip').datagrid('uncheckAll');
		EquipmentProposalBorrow._mapRecored = new Map();
	},
	/**
	 * Xuat template import excel
	 * @author nhutnn
	 * @since 03/07/2015
	 */
	downloadTemplate: function() {
		var params = new Object();
		ReportUtils.exportReport('/equipment-management-proposal-borrow/download-template', params, 'errLendMsg');
	},
	/**
	 * Show popup import
	 * @author nhutnn
	 * @since 03/07/2015
	 */
	showDlgImportExcel: function(){
		$('#easyuiPopupImportExcel').show();
		$('.ErrorMsgStyle').hide();
		$('#easyuiPopupImportExcel').dialog({  
	        closed: false,  
	        cache: false,  
	        modal: true,
	        height:'auto',
	        onOpen: function(){	 
	        	$('.easyui-dialog .BtnGeneralStyle').css('color','');
	        },
	        onClose: function(){
	        	EquipmentProposalBorrow.searchProposalBorrow();
				$("#gridLendEquip").datagrid('uncheckAll');
				$("#fakefilepcBBDNM").val("");
				$("#excelFileBBDNM").val("");				
	        }
		});
	},
	/**
	 * import excel BBGN
	 * @author nhutnn
	 * @since 06/07/2015
	 */
	importExcelBBDNM: function() {
		$('.ErrorMsgStyle').hide();
		VTUtilJS.importExcelUtils(function(data) {
			$("#errExcelMsgBBDNM").html(data.message).show();
			if ($("#errExcelMsgBBDNM span").length > 0 && (data.fileNameFail == null || data.fileNameFail == "")) {
				setTimeout(function() {
					$('#errExcelMsgBBDNM').html("").hide();
					$('#easyuiPopupImportExcel').dialog('close');
				}, 3000);
			} else {
				$("#fakefilepcBBDNM").val("");
				$("#excelFileBBDNM").val("");
				//$('#gridRecordDelivery').datagrid('reload');	
			}
		}, "importFrmBBDNM", "excelFileBBDNM", null, "errExcelMsgBBDNM");
		return false;
	},
	/**
	 * Lay thong tin thiet bi
	 * @author nhutnn
	 * @since 07/07/2015
	 */	
	getLstInfo: function() {
		Utils.getJSONDataByAjax({},'/equipment-management-proposal-borrow/get-list-info',function(result) {
			if (result.lstEquipCategory != undefined && result.lstEquipCategory != null && result.lstEquipCategory.length > 0) {				
				for (var i = 0, sz = result.lstEquipCategory.length; i < sz; i++) {
					var obj = result.lstEquipCategory[i];
					obj.searchText = unicodeToEnglish(obj.code + " " + obj.name);
					obj.searchText = obj.searchText.toUpperCase();
				}	
				EquipmentProposalBorrow._lstEquipCategory = result.lstEquipCategory;
			}
			if (result.lstHealthy != undefined && result.lstHealthy != null && result.lstHealthy.length > 0) {
				for (var i = 0, sz = result.lstHealthy.length; i < sz; i++) {
					var obj = result.lstHealthy[i];
					obj.searchText = unicodeToEnglish(obj.apParamCode + " " + obj.apParamName);
					obj.searchText = obj.searchText.toUpperCase();
				}	
				EquipmentProposalBorrow._lstHealthStatus = result.lstHealthy;
			}
			if (result.lstProvince != undefined && result.lstProvince != null && result.lstProvince.length > 0) {
				for (var i = 0, sz = result.lstProvince.length; i < sz; i++) {
					var obj = result.lstProvince[i];
					obj.searchText = unicodeToEnglish(obj.areaCode + obj.areaName);
					obj.searchText = obj.searchText.toUpperCase();
				}
				EquipmentProposalBorrow._lstProvince = result.lstProvince;
			}
		}, null, null);
	},
	/**
	 * Them khach hang vao dong chi tiet
	 * @author nhutnn
	 * @since 07/07/2015
	 */
	//addCustomerInfo: function(customerCode, customerName, phone, mobiphone, street, housenumber, wardCode, districtCode, provinceCode, amount){
	addCustomerInfo: function(customerCode, customerName, phone, mobiphone, street, housenumber, wardCode, districtCode, provinceCode) {
		$('#divOverlay').show();		
		// var rows = $("#gridEquipLendChange").datagrid("getRows");	
		if(EquipmentProposalBorrow._editIndex != null ){
			var row = EquipmentProposalBorrow.getDetailEdit();
			row.customerCode = customerCode;
			row.customerName = customerName;
			if(phone != "" && mobiphone != "") {
				row.phone = phone +" / "+ mobiphone;	
			} else if(phone != "") {
				row.phone = phone;
			} else if(mobiphone != ""){
				row.phone = mobiphone;
			} else {
				row.phone = "";
			}
			row.street = street;
			row.address = housenumber;
			row.wardCode = wardCode;
			row.districtCode = districtCode;
			row.provinceCode = provinceCode;

			//row.amount = Math.floor(amount);
			// them dong moi			
			$('#gridEquipLendChange').datagrid("updateRow", {
				index: EquipmentProposalBorrow._editIndex,
				row: row
			});
			$('#gridEquipLendChange').datagrid('beginEdit', EquipmentProposalBorrow._editIndex);			
			EquipmentProposalBorrow.addEditorInGridChangeLend();
		}
		$('#divOverlay').hide();
		$('.easyui-dialog').dialog('close');
	},
	/**
	 * Xoa dong chi tiet
	 * @author nhutnn
	 * @since 07/07/2015
	 */
	deleteRowEquipLendDetail: function(index){		
		$.messager.confirm('Xác nhận', "Bạn có muốn xóa?", function(r){
			if(r){
				if(EquipmentProposalBorrow._editIndex != null){
					if(EquipmentProposalBorrow._editIndex == index){		
						EquipmentProposalBorrow._editIndex = null;
					} else if (EquipmentProposalBorrow._editIndex > index){
						// them dong moi
						var row = EquipmentProposalBorrow.getDetailEdit();
						$('#gridEquipLendChange').datagrid("updateRow", {
							index: EquipmentProposalBorrow._editIndex,
							row: row
						});
						
						EquipmentProposalBorrow._editIndex--;
					}
				}
				// xoa
				$('#gridEquipLendChange').datagrid('deleteRow', index);
				var rows = $("#gridEquipLendChange").datagrid("getRows");	
				$("#gridEquipLendChange").datagrid("loadData",rows);

				if(EquipmentProposalBorrow._editIndex != null){
					$('#gridEquipLendChange').datagrid('beginEdit', EquipmentProposalBorrow._editIndex);
					EquipmentProposalBorrow.addEditorInGridChangeLend();
				}
			}
		});
	},
	/**
	 * Them editor tren dong them moi
	 * @author nhutnn
	 * @since 08/07/2015
	 */
	addEditorInGridChangeLend: function() {
		var ed = $('#gridEquipLendChange').datagrid('getEditors', EquipmentProposalBorrow._editIndex);
		var row = $('#gridEquipLendChange').datagrid('getRows')[EquipmentProposalBorrow._editIndex];
    	
    	// Loai thiet bi
    	$(ed[0].target).combobox("loadData",EquipmentProposalBorrow._lstEquipCategory);			
		if(row.equipCategoryCode != undefined && row.equipCategoryCode != null && row.equipCategoryCode != "" ){
			$(ed[0].target).combobox("select", row.equipCategoryCode);
		}

		var getCustomer = function(){
			$(".ErrorMsgStyle").html('').hide();
			if($(ed[3].target).text('getValue').val() != null && $(ed[3].target).text('getValue').val() != ""){
				if($(ed[4].target).text('getValue').val() != null && $(ed[4].target).text('getValue').val() != ""){
					var params = new Object();
					params.shopCode = $(ed[3].target).text('getValue').val().trim();
					params.isAmountEquip = true;
					params.page = 1;
					params.max = 10;
					params.code = $(ed[4].target).text('getValue').val().trim();

					Utils.getJSONDataByAjax(params, '/commons/search-Customer-Show-List',function(result) {
						if(result.rows != null && result.rows.length > 0){
							var row = result.rows[0];
							var phone = row.phone == null ? "" : row.phone;
				        	var mobiphone = row.mobiphone == null ? "" : row.mobiphone;
				        	var street = row.street == null ? "" : row.street;
				        	var housenumber = row.housenumber == null ? "" : row.housenumber;
				        	var wardCode = row.wardCode == null ? "" : row.wardCode;
							var districtCode = row.districtCode == null ? "" : row.districtCode;
							var provinceCode = row.provinceCode == null ? "" : row.provinceCode;
							
				        	//EquipmentProposalBorrow.addCustomerInfo(row.shortCode, row.customerName, phone, mobiphone, street, housenumber, wardCode, districtCode, provinceCode, row.amountEquip);
				        	EquipmentProposalBorrow.addCustomerInfo(row.shortCode, row.customerName, phone, mobiphone, street, housenumber, wardCode, districtCode, provinceCode);
						}else {							
				        	//EquipmentProposalBorrow.addCustomerInfo("", "", "", "", "", "", "", "", "", "");
				        	EquipmentProposalBorrow.addCustomerInfo("", "", "", "", "", "", "", "", "");
				        	$('#errMsgChangeLend').html("Không tìm thấy mã khách hàng trong hệ thống!").show();								
							return false;
						}
						$(ed[4].target).focus();
					});
				}
			} else {
				$('#errMsgChangeLend').html("Bạn chưa nhập mã đơn vị!").show();
				$(ed[3].target).focus();
				return false;
			}
		};
    	// Ma khach hang
		$(ed[4].target).bind('keyup', function(event) {					
			if (event.keyCode == keyCodes.F9) {
				$(".ErrorMsgStyle").html('').hide();
				if($(ed[3].target).text('getValue').val() != null && $(ed[3].target).text('getValue').val() != ""){
					VCommonJS.showDialogSearch2({
						params : {
							shopCode: $(ed[3].target).text('getValue').val().trim(),
							isAmountEquip: true
						},
					    inputs : [
					        {id:'code', maxlength:50, label:'Mã KH'},
					        {id:'name', maxlength:250, label:'Tên KH'},
					        {id:'address', maxlength:250, label:'Địa chỉ', width:410}
					    ],			   
					    url : '/commons/search-Customer-Show-List',			   
					    columns : [[
					        {field:'shortCode', title:'Mã KH', align:'left', width: 110, sortable:false, resizable:false},
					        {field:'customerName', title:'Tên KH', align:'left', width: 150, sortable:false, resizable:false},
					        {field:'address', title:'Địa chỉ', align:'left', width: 170, sortable:false, resizable:false},
					        {field:'choose', title:'', align:'center', width:40, sortable:false, resizable:false, formatter:function(value,row,index) {
					        	var phone = row.phone == null ? "" : row.phone;
					        	var mobiphone = row.mobiphone == null ? "" : row.mobiphone;
					        	var street = row.street == null ? "" : row.street;
					        	var housenumber = row.housenumber == null ? "" : row.housenumber;
					        	var wardCode = row.wardCode == null ? "" : row.wardCode;
								var districtCode = row.districtCode == null ? "" : row.districtCode;
								var provinceCode = row.provinceCode == null ? "" : row.provinceCode;
								
					        	//return "<a href='javascript:void(0)' onclick=\"return EquipmentProposalBorrow.addCustomerInfo('"+row.shortCode+"','"+row.customerName+"','"+phone+"','"+mobiphone+"','"+street+"','"+housenumber+"','"+wardCode+"','"+districtCode+"','"+provinceCode+"','"+row.amountEquip+"');\">Chọn</a>";        
					        	return "<a href='javascript:void(0)' onclick=\"return EquipmentProposalBorrow.addCustomerInfo('"+row.shortCode+"','"+row.customerName+"','"+phone+"','"+mobiphone+"','"+street+"','"+housenumber+"','"+wardCode+"','"+districtCode+"','"+provinceCode+"');\">Chọn</a>";        
					        }}
					    ]]
					});
					$(".easyui-dialog #code").focus();
				} else {
					$('#errMsgChangeLend').html("Bạn chưa nhập mã đơn vị!").show();
					$(ed[3].target).focus();
					return false;
				}
			} else if (event.keyCode == keyCodes.ENTER) {
				getCustomer();
			}
		});				

		$(ed[4].target).keydown(function (e) {
	        var keyCode = e.which || e.keyCode; // for cross-browser compatibility
	        if(keyCode == keyCodes.TAB){
				getCustomer();
			}
	    });

		// tinh, thanh pho
    	$(ed[8].target).combobox("loadData", EquipmentProposalBorrow._lstProvince);
		if(row.provinceCode != undefined && row.provinceCode != null && row.provinceCode != "" ){
			$(ed[8].target).combobox("select", row.provinceCode);
		}
		
		// tinh trang tu
    	$(ed[19].target).combobox("loadData",EquipmentProposalBorrow._lstHealthStatus);
    	if(row.healthStatusCode != undefined && row.healthStatusCode != null && row.healthStatusCode != "" ){
			$(ed[19].target).combobox("select", row.healthStatusCode);
		}
		if(row.stockTypeValue != undefined && row.stockTypeValue != null && row.stockTypeValue != "" ){
			$(ed[20].target).combobox("select", row.stockTypeValue);
		}

		// ngay lap CMND
		$($(ed[14].target).datebox('textbox')).attr('id', 'cmndDate' + EquipmentProposalBorrow._editIndex).attr("maxlength","10");
		Utils.bindFormatOnTextfield('cmndDate' + EquipmentProposalBorrow._editIndex, Utils._TF_NUMBER_CONVFACT);

		// ngay DKKD
		$($(ed[17].target).datebox('textbox')).attr('id', 'dkkdDate' + EquipmentProposalBorrow._editIndex).attr("maxlength","10");
		Utils.bindFormatOnTextfield('dkkdDate' + EquipmentProposalBorrow._editIndex, Utils._TF_NUMBER_CONVFACT);

		// thoi gian muon tu
		$($(ed[18].target).datebox('textbox')).attr('id', 'lendDate' + EquipmentProposalBorrow._editIndex).attr("maxlength","10");
		Utils.bindFormatOnTextfield('lendDate' + EquipmentProposalBorrow._editIndex, Utils._TF_NUMBER_CONVFACT);
	},
	/**
	 * Validate dong them moi
	 * @author nhutnn
	 * @since 08/07/2015
	 */
	validateEquipmentInGrid: function() {
		var currentDate = Utils.currentDate();
		var ed = $('#gridEquipLendChange').datagrid('getEditors', EquipmentProposalBorrow._editIndex);
		var msg = "";
		if (msg.length == 0 && ($(ed[0].target).combobox('getValue') == null || $(ed[0].target).combobox('getValue') == "")) {
			msg = "Bạn chưa chọn loại thiết bị!";
			$(ed[0].target).focus();
		}
		if (msg.length == 0 && ($(ed[1].target).text('getValue').val() == null || $(ed[1].target).text('getValue').val() == "")) {
			msg = "Bạn chưa nhập dung tích!";
			$(ed[1].target).focus();
		}
		if (msg.length == 0 && ($(ed[2].target).numberbox('getValue') == null || $(ed[2].target).numberbox('getValue') == "")) {
			msg ="Bạn chưa nhập số lượng!";
			$(ed[2].target).focus();
		}
		if (msg.length == 0 && ($(ed[2].target).numberbox('getValue') <= 0)) {
			msg = "Bạn phải nhập số lượng là số nguyên dương!";
			$(ed[2].target).focus();
		}
		if (msg.length == 0 && ($(ed[3].target).text('getValue').val() == null || $(ed[3].target).text('getValue').val() == "")) {
			msg = "Bạn chưa nhập mã đơn vị!";
			$(ed[3].target).focus();
		}
		if (msg.length == 0 && ($(ed[4].target).text('getValue').val() == null || $(ed[4].target).text('getValue').val() == "")) {
			msg = "Bạn chưa nhập mã khách hàng!";
			$(ed[4].target).focus();
		}
		if (msg.length == 0 && ($(ed[5].target).text('getValue').val() == null || $(ed[5].target).text('getValue').val() == "")) {
			msg = "Bạn chưa nhập người đứng tên mượn thiết bị!";
			$(ed[5].target).focus();
		}
		if (msg.length == 0 && ($(ed[6].target).text('getValue').val() == null || $(ed[6].target).text('getValue').val() == "")) {
			msg = "Bạn chưa nhập quan hệ!";
			$(ed[6].target).focus();
		}
		if (msg.length == 0 && ($(ed[7].target).text('getValue').val() == null || $(ed[7].target).text('getValue').val() == "")) {
			msg = "Bạn chưa nhập số nhà!";
			$(ed[7].target).focus();
		}
		if (msg.length == 0 && ($(ed[8].target).combobox('getValue') == null || $(ed[8].target).combobox('getValue') == "")) {
			msg = "Bạn chưa nhập Tỉnh/Thành phố!";
			$(ed[8].target).focus();
		}
		if (msg.length == 0 && ($(ed[9].target).combobox('getValue') == null || $(ed[9].target).combobox('getValue') == "")) {
			msg = "Bạn chưa nhập quận/huyện!";
			$(ed[9].target).focus();
		}
		if (msg.length == 0 && ($(ed[10].target).combobox('getValue') == null || $(ed[10].target).combobox('getValue') == "")) {
			msg = "Bạn chưa nhập phường/xã!";
			$(ed[10].target).focus();
		}
		if (msg.length == 0 && ($(ed[11].target).text('getValue').val() == null || $(ed[11].target).text('getValue').val() == "")) {
			msg = "Bạn chưa nhập tên đường/phố/thôn!";
			$(ed[11].target).focus();
		}
		if (msg.length == 0 && ($(ed[12].target).text('getValue').val() == null || $(ed[12].target).text('getValue').val() == "")) {
			msg = "Bạn chưa nhập số CMND!";
			$(ed[12].target).focus();
		}
		if (msg.length == 0 && !/^[0-9]+$/.test($(ed[12].target).text('getValue').val())){
			msg = format(msgErr_invalid_format_num,"Số CMND");
			$(ed[12].target).focus();
		}
		if (msg.length == 0 && ($(ed[13].target).text('getValue').val() == null || $(ed[13].target).text('getValue').val() == "")) {
			msg = "Bạn chưa nhập nơi cấp CMND!";
			$(ed[13].target).focus();
		}
		if (msg.length == 0 && ($(ed[14].target).datebox('getValue') == null || $(ed[14].target).datebox('getValue') == "")) {
			msg = "Bạn chưa nhập ngày cấp CMND!";
			$(ed[14].target).focus();
		}		
		if (msg.length == 0 && $(ed[14].target).datebox('getValue').length > 0 && !Utils.isDate($(ed[14].target).datebox('getValue'), '/')) {
			msg = format(msgErr_invalid_format_date,"Ngày cấp CMND");
			$(ed[14].target).focus();
		}
		if (msg.length == 0 && $(ed[14].target).datebox('getValue').length > 0 && Utils.compareDateForTowDate($(ed[14].target).datebox('getValue'), currentDate) >= 0) {
			msg = 'Ngày cấp CMND phải là ngày trước ngày hiện tại';
			$(ed[14].target).focus();
		}
		// if (msg.length == 0 && $(ed[14].target).text('getValue').val() != "" && !/^[0-9]+$/.test($(ed[14].target).text('getValue').val())){
		// 	msg = format(msgErr_invalid_format_num,"Số ĐKKD");
		// 	$(ed[14].target).focus();
		// }
		if (msg.length == 0 && $(ed[17].target).datebox('getValue').length > 0 && !Utils.isDate($(ed[17].target).datebox('getValue'), '/')) {
			msg = format(msgErr_invalid_format_date,"Ngày cấp số ĐKKD");
			$(ed[17].target).focus();
		}
		if (msg.length == 0 && $(ed[17].target).datebox('getValue').length > 0 && Utils.compareDateForTowDate($(ed[17].target).datebox('getValue'), currentDate) >= 0) {
			msg = 'Ngày cấp số ĐKKD phải là ngày trước ngày hiện tại';
			$(ed[17].target).focus();
		}
		if (msg.length == 0 && ($(ed[18].target).datebox('getValue') == null || $(ed[18].target).datebox('getValue') == "")) {
			msg = "Bạn chưa nhập thời gian muốn nhận thiết bị!";
			$(ed[18].target).focus();
		}		
		if (msg.length == 0 && $(ed[18].target).datebox('getValue').length > 0 && !Utils.isDate($(ed[18].target).datebox('getValue'), '/')) {
			msg = format(msgErr_invalid_format_date,"Thời gian muốn nhận thiết bị");
			$(ed[18].target).focus();
		}		
		if (msg.length == 0 && $(ed[18].target).datebox('getValue').length > 0 && Utils.compareDateForTowDate($(ed[18].target).datebox('getValue'), currentDate) < 0) {
			msg = 'Thời gian muốn nhận thiết bị phải là ngày sau hoặc bằng ngày hiện tại';
			$(ed[18].target).focus();
		}
		if (msg.length == 0 && ($(ed[19].target).combobox('getValue') == null || $(ed[19].target).combobox('getValue') == "")) {
			msg = "Bạn chưa nhập tình trạng thiết bị!";
			$(ed[19].target).focus();
		}
		if (msg.length == 0 && ($(ed[20].target).combobox('getValue') == null || $(ed[20].target).combobox('getValue') == "")) {
			msg = "Bạn chưa nhập kho xuất!";
			$(ed[20].target).focus();
		}
		return msg;
	},
	/**
	 * Them dong chi tiet
	 * @author nhutnn
	 * @since 07/07/2015
	 */
	insertEquipmentInGrid: function() {
		$('.ErrorMsgStyle').html('').hide();
		if (EquipmentProposalBorrow._editIndex != null) {
			var msg = EquipmentProposalBorrow.validateEquipmentInGrid();
			if (msg.length > 0) {
				$('#errMsgChangeLend').html(msg).show();
				return false;
			}
			// them dong moi
			var row = EquipmentProposalBorrow.getDetailEdit();
			$('#gridEquipLendChange').datagrid("updateRow", {
				index: EquipmentProposalBorrow._editIndex,
				row: row
			});
		}

		var rowNew = {};
		if ($("#isLevelShop").val() == ShopDecentralizationSTT.NPP && $("#curShopCode").val() != null && $("#curShopCode").val() != "") {
			rowNew.shopCode = $("#curShopCode").val().trim();
		}
		$('#gridEquipLendChange').datagrid("appendRow", rowNew);
		var sz = $("#gridEquipLendChange").datagrid("getRows").length;
		EquipmentProposalBorrow._editIndex = sz - 1;
		$('#gridEquipLendChange').datagrid('beginEdit', EquipmentProposalBorrow._editIndex);
		EquipmentProposalBorrow.addEditorInGridChangeLend();
	},
	/**
	 * Lay thong dong chi tiet them
	 * @author nhutnn
	 * @since 08/07/2015
	 */
	getDetailEdit: function(){
		var ed = $('#gridEquipLendChange').datagrid('getEditors', EquipmentProposalBorrow._editIndex);
		var row = $('#gridEquipLendChange').datagrid("getRows")[EquipmentProposalBorrow._editIndex];
		row.equipCategory = $(ed[0].target).combobox("getText").trim();
		row.equipCategoryCode = $(ed[0].target).combobox("getValue");
		row.capacity = $(ed[1].target).text("getValue").val().trim();
		row.quantity = $(ed[2].target).numberbox("getValue").trim();

		row.shopCode = $(ed[3].target).text("getValue").val().trim();
		row.customerCode = $(ed[4].target).text("getValue").val().trim();
		row.representative = $(ed[5].target).text("getValue").val().trim();
		row.relation = $(ed[6].target).text("getValue").val().trim();

		row.address = $(ed[7].target).text("getValue").val().trim();
		row.province = $(ed[8].target).combobox("getText").trim();
		row.provinceCode = $(ed[8].target).combobox("getValue");

		row.district = $(ed[9].target).combobox("getText").trim();
		row.districtCode = $(ed[9].target).combobox("getValue");

		row.ward = $(ed[10].target).combobox("getText").trim();
		row.wardCode = $(ed[10].target).combobox("getValue");
		row.street = $(ed[11].target).text("getValue").val().trim();

		row.idNo = $(ed[12].target).text("getValue").val().trim();
		row.idNoPlace = $(ed[13].target).text("getValue").val().trim();
		row.idNoDate = $(ed[14].target).datebox("getValue").trim();

		row.businessNo = $(ed[15].target).text("getValue").val().trim();
		row.businessNoPlace = $(ed[16].target).text("getValue").val().trim();
		row.businessNoDate = $(ed[17].target).datebox("getValue").trim();

		row.timeLend = $(ed[18].target).datebox("getValue").trim();
		row.healthStatus = $(ed[19].target).combobox("getText").trim();
		row.healthStatusCode = $(ed[19].target).combobox("getValue");
		row.stockType = $(ed[20].target).combobox("getText").trim();
		row.stockTypeValue = $(ed[20].target).combobox("getValue");

		return row;
	},
	/**
	 * Chinh sua bien ban
	 * @author nhutnn
	 * @since 08/07/2015
	 */
	updateRecord: function(){
		$('.ErrorMsgStyle').html('').hide();
		var msg = "";
		var err = "";
		if (msg.length == 0 && ($("#status").val() == undefined || $("#status").val() == null || $("#status").val() == "")) {
			msg = 'Bạn chưa chọn trạng thái!';
			err = '#status';
		}
		if (msg.length == 0 && $('#staffCode').val() == 0) {
			msg = 'Bạn chưa chọn giám sát';
		}
		if (msg.length == 0 && $('#createFormDate').val().trim().length == 0) {
			msg = 'Bạn chưa nhập giá trị cho trường Ngày biên bản';
			err = '#createFormDate';
		}
		if (msg.length == 0 && $('#createFormDate').val().trim().length > 0 && !Utils.isDate($('#createFormDate').val(), '/')) {
			msg = 'Ngày biên bản không tồn tại hoặc không đúng định dạng dd/mm/yyyy.';
			err = '#createFormDate';
		}
		var currentDate = Utils.currentDate();
		if (msg.length == 0 && $('#createFormDate').val().length > 0 && Utils.compareDateForTowDate($('#createFormDate').val(), currentDate) > 0) {
			msg = 'Ngày biên bản phải là ngày trước hoặc cùng ngày với ngày hiện tại';
			err = '#createFormDate';
		}
		var rows = $('#gridEquipLendChange').datagrid("getRows");
		if(msg.length == 0 && rows.length == 0){
			msg = 'Bạn chưa thêm chi tiết biên bản';
		}
		if (msg.length == 0 && $('#note').attr('maxlength') > 500) {
			msg = 'Bạn nhập quá giới hạn của trường note';
		}
		if (msg.length == 0){
			if(EquipmentProposalBorrow._editIndex != null){
				msg = EquipmentProposalBorrow.validateEquipmentInGrid();
				if(msg.length > 0){
					$('#errMsgChangeLend').html(msg).show();
					return false;
				}				
				// them dong moi
				var row = EquipmentProposalBorrow.getDetailEdit();
				rows[EquipmentProposalBorrow._editIndex] = row;
				//$('#gridEquipLendChange').datagrid("updateRow", {index: EquipmentProposalBorrow._editIndex, row: row});
			}			
		}	

		if(msg.length > 0){
			$('#errMsgChangeLend').html(msg).show();
			$(err).focus();
			return false;
		}
		
		var params = new Object();
		params.id = $("#idEquipLend").val();
		params.lstDetails = rows;
		params.staffCode = $("#staffCode").val();
		params.statusRecord = $("#status").val();
		params.createFormDate = $("#createFormDate").val();
		params.note = $('#note').val();
		//params = JSONUtil.getSimpleObject(params);
		JSONUtil.saveData2(params, "/equipment-management-proposal-borrow/update", "Bạn có muốn cập nhật?", "errMsgChangeLend", function(data){
			if(data.error){
				$("errMsgChangeLend").html(data.errMsg).show();
			} else {
				$("#successMsgChangeLend").html("Lưu dữ liệu thành công").show();
				setTimeout(function(){						
					if(window.location.pathname == "/equipment-management-proposal-borrow/edit"){
						window.location.href = '/equipment-management-proposal-borrow/info';	
					} else if(window.location.pathname == "/equipment-management-proposal-borrow/change"){
						window.location.href = '/equipment-management-proposal-borrow/approve';	
					}					
				}, 3500);
			}
		},null,null);
	},
	/**
	 * Xuat bien ban hop dong giao nhan
	 * 
	 * @author update nhutnn
	 * @since 10/07/2015
	 */
	exportDeliveryRecord: function(id) {
		$('.ErrorMsgStyle').html("").hide();
		var rows = $('#gridLendEquip').datagrid('getRows');
		if (rows.length == 0) {
			$('#errLendMsg').html('Không có dữ liệu để xuất').show();
			return false;
		}

		if(id == null) {
			if (EquipmentProposalBorrow._mapRecored.keyArray.length == 0) {
				$('#errLendMsg').html('Bạn chưa chọn biên bản. Vui lòng chọn biên bản!').show();
				return false;
			}
		}	

		var lstIdRecord = new Array();
		if(id == null) {
			for (var i = 0; i < EquipmentProposalBorrow._mapRecored.keyArray.length; i++) {
				lstIdRecord.push(EquipmentProposalBorrow._mapRecored.keyArray[i]);
			}
		}else{
			lstIdRecord.push(id);
		}

		var params = new Object();
		params.lstIdRecord = lstIdRecord;
		Utils.addOrSaveData(params, '/equipment-management-proposal-borrow/export-delivery-record', null, 'errLendMsg', function(data) {
			if (data.error) {
				$('#errLendMsg').html(data.errMsg).show();
				return;
			}
			if (data.lstRecordError != null && data.lstRecordError.length > 0) {
				VCommonJS.showDialogList({
					title: 'LỖI',
					data: data.lstRecordError,
					columns: [[
						{field: 'formCode', title: 'Mã biên bản', align: 'left', width: 120, sortable: false, resizable: false}, 
						{field: 'formStatusErr', title: 'Biên bản không ở trạng thái duyệt', align: 'center', width: 60, sortable: false, resizable: false, formatter: function(value, row, index) {
							var html = "";
							if (row.formStatusErr == 1) {
								html = '<img src="/resources/images/icon_delete.png" height="20" width="20" style="margin-right:5px;" title="Error">';
							}
							return html;
						}},
						{field: 'deliveryStatusErr', title: 'Biên bản đã xuất hợp đồng giao nhận', align: 'center', width: 60, sortable: false, resizable: false, formatter: function(value, row, index) {
							var html = "";
							if (row.deliveryStatusErr == 1) {
								html = '<img src="/resources/images/icon_delete.png" height="20" width="20" style="margin-right:5px;" title="Error">';
							}
							return html;
						}}
					]]
				});
			} else {
				$('#successMsgLend').html('Xuất hợp đồng giao nhận thành công!').show();
				setTimeout(function() {
					$('#successMsgLend').html('').hide();
				}, 3000);
			}
			EquipmentProposalBorrow.searchProposalBorrow();
		}, null, null, null, "Bạn có muốn xuất biên bản hợp đồng giao nhận?", null);
	},
	/**
	 * Lay danh sach bien ban hop dong giao bi huy
	 *
	 * @author nhutnn
	 * @since 10/07/2015
	 */
	getDeliveryRecordAgain: function(id) {
		$('.ErrorMsgStyle').html("").hide();
		if(id == null) {
			$('#errLendMsg').html('Bạn chưa chọn biên bản. Vui lòng chọn biên bản!').show();
		}	

		var params = new Object();
		params.id = id;
		Utils.getJSONDataByAjax(params, '/equipment-management-proposal-borrow/get-list-delivery-export-again', function(data) {
			if (data.error) {
				$('#errLendMsg').html(data.errMsg).show();
				return;
			}else {
				if (data.total > 1) {
					EquipmentProposalBorrow._mapDelivery = new Map();  
					$("#idRecordPopup").val(id); 
					$('#easyuiPopupLstDelivery').show();
					$('#easyuiPopupLstDelivery').dialog({  
				        closed: false,  
				        cache: false,  
				        modal: true,
				        width : 350,	        
				        height : 'auto',
				        onOpen: function(){	      
							$('#gridDelivery').datagrid({
								data: data.rows,
								autoRowHeight : true,
								fitColumns:true,
								width: 310,
								height: "auto",
								scrollbarSize: 0,
								rownumbers:true,
								columns:[[
									{field: 'shopCode', title: 'Mã đơn vị', width: 80, sortable:false, resizable: false, align: 'left'},					
								    {field: 'customerCode', title: 'Mã khách hàng', width: 80, sortable:false, resizable: false, align: 'left'},					
									{field: 'recordCode', title: 'Mã biên bản giao nhận', width: 120, sortable: false, resizable: false,align: 'left'},		    
									{field: 'check',checkbox:true, width: 50, sortable:false,resizable:false, align: 'center'}
								]],
								onBeforeLoad: function(param){											 
								},
								onLoadSuccess: function(data){   			 
							   		$('.easyui-dialog .datagrid-header-rownumber').html('STT');	   	

								},onCheck:function(index,row){
									EquipmentProposalBorrow._mapDelivery.put(row.idRecord,row);   
							    },
							    onUncheck:function(index,row){
							    	EquipmentProposalBorrow._mapDelivery.remove(row.idRecord);
							    },
							    onCheckAll:function(rows){
							    	for(var i = 0;i<rows.length;i++){
							    		var row = rows[i];
							    		EquipmentProposalBorrow._mapDelivery.put(row.idRecord,row);
							    	}
							    },
							    onUncheckAll:function(rows){
							    	for(var i = 0;i<rows.length;i++){
							    		var row = rows[i];
							    		EquipmentProposalBorrow._mapDelivery.remove(row.idRecord);	
							    	}
							    }
							});
				        }, 
				        onClose: function(){
				        	EquipmentProposalBorrow._mapDelivery = null;
				        }
					});
				} else {
					EquipmentProposalBorrow.exportDeliveryRecordAgain(id, data.rows[0].customerId);
				}
			}
		}, null, null);
	},
	/**
	 * Xuat lai bien ban hop dong giao nhan lan nua
	 *
	 * @author nhutnn
	 * @since 10/07/2015
	 */
	exportDeliveryRecordAgain: function(id, customerId){
		$('.ErrorMsgStyle').html("").hide();
		if(customerId == null && id == null) {
			if (EquipmentProposalBorrow._mapDelivery.keyArray.length == 0) {
				$('#errMsgPopupLstDelivery').html('Bạn chưa chọn biên bản. Vui lòng chọn biên bản!').show();
				return false;
			}
		}	

		var lstCustomerId = new Array();
		if(customerId == null && id == null) {
			for (var i = 0; i < EquipmentProposalBorrow._mapDelivery.keyArray.length; i++) {
				var idDelivery = EquipmentProposalBorrow._mapDelivery.keyArray[i];
				lstCustomerId.push(EquipmentProposalBorrow._mapDelivery.get(idDelivery).customerId);
			}
		}else{
			lstCustomerId.push(customerId);
		}
		var errId = customerId == null?"errMsgPopupLstDelivery":"errLendMsg";		
		var params = new Object();
		if(id != null){ 
			params.id = id;
		} else {
			params.id = $("#idRecordPopup").val();
		}	
		params.lstCustomerId = lstCustomerId;
		Utils.addOrSaveData(params, '/equipment-management-proposal-borrow/export-delivery-export-again', null, errId, function(data) {
			if (data.error) {
				$('#'+errId).html(data.errMsg).show();
				return;
			}else {
				$('#successMsgLend').html('Xuất lại hợp đồng giao nhận thành công!').show();
				$("#idRecordPopup").val("");
				setTimeout(function() {
					$('#successMsgLend').html('').hide();					
				}, 3000);
				$('#easyuiPopupLstDelivery').window('close');
			}
			EquipmentProposalBorrow.searchProposalBorrow();
		}, null, null, null, "Bạn có muốn xuất lại biên bản hợp đồng giao nhận?", null);
	},
	/**
	 * Xuat lai bien ban hop dong giao nhan lan nua
	 *
	 * @author nhutnn
	 * @since 10/07/2015
	 */
	viewPrintEquipLend: function(id){
		$('.ErrorMsgStyle').html('').hide();
		
		var lstIdRecord = new Array();
		if(id != null){			
			lstIdRecord.push(id);
		}else{
			$('#errLendMsg').html('Vui lòng chọn biên bản đã duyệt để xem bản in!').show();
		}		
		
		var params = new Object();
		params.lstIdRecord = lstIdRecord;	
		ReportUtils.exportReport('/equipment-management-proposal-borrow/view-print', params);
		return false;
	},
	/**
	 * Quay về trang tìm kiếm
	 *
	 * @author nhutnn
	 * @since 27/07/2015
	 */
	returnSearchPage: function(){
		$.messager.confirm('Xác nhận', "Bạn có muốn quay lại trang tìm kiếm và không lưu dữ liệu thay đổi?", function(r){
			if (r) {
				if(window.location.pathname == "/equipment-management-proposal-borrow/edit"){
					window.location.href = '/equipment-management-proposal-borrow/info';	
				} else if(window.location.pathname == "/equipment-management-proposal-borrow/change"){
					window.location.href = '/equipment-management-proposal-borrow/approve';	
				}				
			}
		});
	},
};
var EquipManagePermission = {
	_params: null, /**dung cho dieu kien xuat excel theo dk search*/
	_staffId: null,
	_staffCode: null,
	_lstRoleId: null,
	_mapRole: null,
	_lstRoleUserId: null,
	searchStaff : function() {
		$('#errMsg').html('').hide();

		var params = new Object();
		params.personalCode = $('#personalCode').val().trim();
		params.personalName = $('#personalName').val().trim();
		params.personalStatus = $('#personalStatus').val();
		
		/**dung cho dieu kien xuat excel theo dk search*/		
		EquipManagePermission._params = new Object();
		EquipManagePermission._params.personalCode = $('#personalCode').val().trim();
		EquipManagePermission._params.personalName = $('#personalName').val().trim();
		EquipManagePermission._params.personalStatus = $('#personalStatus').val();

		var multiShop = $("#shop").data("kendoMultiSelect");
		var dataShop = multiShop.dataItems();
		var arrShopId = [];
		if (dataShop != undefined && dataShop != null) {
			for (var i = 0, size = dataShop.length; i < size; i++) {
				arrShopId.push(dataShop[i].shopId);
			}
		}
		params.listShopId = arrShopId.toString();
		EquipManagePermission._params.listShopId = arrShopId.toString();
		$('#gridPersonal').datagrid('reload',params);
		$('#gridPermission').datagrid('reload',new Object());
		$('#titleStaff').html('');
	},
	/**
	 * Show dialog import gan quyen cho nguoi dung
	 * @author vuongmq
	 * @date Mar 26, 2015
	 * */
	showDlgImportExcel: function(){
		$('#easyuiPopupImportExcel').show();
		$('.SuccessMsgStyle').html('').hide();
		$('.ErrorMsgStyle').hide();
		$('#easyuiPopupImportExcel').dialog({  
			width: 500,
			height: 'auto',
	        closed: false,  
	        cache: false,  
	        modal: true,
	        onOpen: function(){	 
	        	$('.easyui-dialog .BtnGeneralStyle').css('color','');
	        },
	        onClose: function(){
	        	/*EquipmentManageCatalog.searchEviction();
				$("#grid").datagrid('uncheckAll');*/
				$("#fakefilepcStaffPermission").val("");
				$("#excelFileStaffPermission").val("");				
	        }
		});
	},
	/**
	 * Import Excel gan quyen cho nguoi dung
	 * @author vuongmq
	 * @date Mar 26, 2015
	 * */
	importExcelStaffPermission: function(){
		$('.SuccessMsgStyle').html('').hide();
		$('.ErrorMsgStyle').html('').hide();
		if($('#fakefilepcStaffPermission').val().length <= 0 ){
			$('#errExcelMsg').html('Vui lòng chọn tập tin Excel').show();
			return false;
		}
		Utils.importExcelUtils(function(data){
			//EquipmentListEquipment.searchListEquipment();
			$('#fakefilepcStaffPermission').val("").change();
			if (data.message!=undefined && data.message!=null && data.message.trim().length >0){
				$('#errExcelMsg').html(data.message.trim()).change().show();
			}else{
				$('#successExcelMsg').html("Lưu dữ liệu thành công").show();
				var tm = setTimeout(function(){
					$('.SuccessMsgStyle').html('').hide();
					clearTimeout(tm);
					$('#easyuiPopupImportExcel').dialog('close');
					//$('#listEquipmentDg').datagrid("load", EquipmentStockChange._params);
				 }, 1500);
			}
		}, 'importFrmStaffPermission', 'excelFileStaffPermission', 'errExcelMsg');
		
	},
	/**
	 * xuat file excel gan quyen cho nguoi dung
	 * @author vuongmq
	 * @date March 26, 2015
	 */
	exportExcel: function(){		
		$('.SuccessMsgStyle').html('').hide();
		$('.ErrorMsgStyle').html('').hide();
		var msg ='';
		var data = $('#gridPersonal').datagrid('getRows');	
		if(data == null || data.length <=0){
			msg = "Không có dữ liệu xuất Excel";
		}
		if(msg.length > 0){
			$('#errMsg').html(msg).show();
			return false;
		}
		$.messager.confirm('Xác nhận','Bạn có muốn xuất file excel?',function(r){
			if(r){
				ReportUtils.exportReport('/equipManagePermission/exportExcelBySearchEquipStaffPermission', EquipManagePermission._params, 'errMsg');					
			}
		});		
		return false;
	},
	viewStaffPermission: function(staffId, staffCode){
		var params = new Object();
		if(staffId != undefined && staffId != null ) {
			EquipManagePermission._staffId = staffId;
			EquipManagePermission._staffCode = staffCode
		} else {
			staffId = EquipManagePermission._staffId;
			staffCode = EquipManagePermission._staffCode;
		}
		$('#panelPermission').css('display','block');
		$('#titleStaff').html(staffCode);
		params.staffId = staffId;
		params.permissionCode = $('#permissionCode').val().trim();
		params.permissionName = $('#permissionName').val().trim();
		params.permissionStatus = $('#permissionStatus').val();
		
		$('#gridPermission').datagrid('reload',params);
		$('html,body').animate({scrollTop: $('#gridPermissionContainer').offset().top}, 1500);
		$('#gridPermission').datagrid("resize");
	},
	showPopupRole: function() {
		$('.ErrorMsgStyle').html('').hide();
		$('#imgOverlay').css('position','fixed');
		var msg ='';
		if(EquipManagePermission._staffId == null || EquipManagePermission._staffId == undefined) {
			msg = "Bạn chưa chọn nhân viên!";
			$('#errMsgPermission').html(msg).show();
			return false;
		}
		VCommonJS.showDialogSearch2WithCheckbox({
	    inputs : [
	        {id:'roleCode', maxlength:50, label:'Mã quyền'},
	        {id:'roleName', maxlength:250, label:'Tên quyền'}
	    ],
	    chooseCallback : function(listObj) {
	    	console.log(listObj);
	    	if(listObj.length == 0) {
	    		msg = 'Vui lòng chọn quyền cần thêm';
	    		$('#errMsgInfo').html(msg).show();
	    	}
	    	EquipManagePermission._lstRoleId = new Array();
	        if($.isArray(listObj) && listObj.length > 0) {
	        	for(var i = 0; i < listObj.length; i++) {
	        		EquipManagePermission._lstRoleId.push(listObj[i].id);
	        	}
	        	EquipManagePermission.addRoleToUser(EquipManagePermission._lstRoleId);
	        }
	    },
	    url : '/commons/search-equipRole-show-list',
	    params : {
			staffRoleId : EquipManagePermission._staffId,
			statusRole : 1
		},
		// isCenter : true,
	    columns : [[
	        {field:'code', title:'Mã quyền', align:'left', width: 110, sortable:false, resizable:false, formatter: function(value, row, index){
		    	return Utils.XSSEncode(value);
		    }},
	        {field:'name', title:'Tên quyền', align:'left', width: 150, sortable:false, resizable:false, formatter: function(value, row, index){
		    	return Utils.XSSEncode(value);
		    }},
	        {field:'cb', checkbox:true, align:'center', width:80,sortable : false,resizable : false, formatter: function(value, row, index){
		    	return Utils.XSSEncode(value);
		    }},
	    ]]
	});
		var wHeight = $(window).height();
		var ptop = wHeight - 150;
		$('#common-dialog-search-2-textbox').dialog('move', {top :ptop});
		var html = '<p id="errMsgInfo" class="ErrorMsgStyle" style="display: none;"></p>';
		$('#common-dialog-search-2-textbox').append(html);
	},
	addRoleToUser: function(lstRoleId){
		$('#imgOverlay').css('position','fixed');
		var params = new Object();
		params.lstRoleId = lstRoleId;
		params.staffId = EquipManagePermission._staffId;

		Utils.addOrSaveData(params, '/equipManagePermission/add-permission', null, 'errMsgDialog', function(){
			setTimeout(function(){		
				$('#common-dialog-search-2-textbox').dialog('close');
				$('#gridPermission').datagrid('reload');				
			}, 1500);			

		});	

	},
	updateRoleUser: function (status) {
		$('.SuccessMsgStyle').html('').hide();
		$('.ErrorMsgStyle').html('').hide();
		$('#imgOverlay').css('position','fixed');
		var msg ='';
		var rows = $('#gridPermission').datagrid('getRows');
		if(rows == null || rows.length <=0){
			msg = "Vui lòng chọn quyền cần cập nhật!";
		}
		
		var isDupStatus = true;
		EquipManagePermission._lstRoleUserId = new Array();
		for(var i = 0; i < rows.length; i++) {
    		if(EquipManagePermission._mapRole.get(rows[i].id) != null) {
    			EquipManagePermission._lstRoleUserId.push(rows[i].equipRoleUserId);
    			if(status != rows[i].status) {
    				isDupStatus = false;
    			}
    		}
		}
		if(isDupStatus && status == 0) {
			msg = "Trường này đang bị khóa";
		}

		if(isDupStatus && status == 1) {
			msg = "Trường này đã được mở khóa";
		}

		if(EquipManagePermission._lstRoleUserId.length == 0) {
			msg = "Vui lòng chọn quyền cần cập nhật!";
		}


		if(msg.length > 0 || isDupStatus){
			$('#errMsgPermission').html(msg).show();
			return false;
		}
		//status: -1:xoa, 0:tam ngung, 1:hoat dong
		var params = new Object();
		params.lstRoleUserId = EquipManagePermission._lstRoleUserId;
		params.status = status;
		if(status == -1) {
			if(params.lstRoleUserId.length > 0) {
				$.messager.confirm('Xác nhận','Bạn có muốn xuất file excel?',function(r){
					if(r){
						Utils.addOrSaveData(params, '/equipManagePermission/update-permission', null, 'errMsgDialog', function(){
							setTimeout(function(){		
								$('#gridPermission').datagrid('reload');			
							}, 1500);			
						}, null, null, true);
					}
				});	
			} else {
				msg = "Vui lòng chọn quyền cần cập nhật!";
				$('#errMsgPermission').html(msg).show();
				return false;
			}
		} else {
			Utils.addOrSaveData(params, '/equipManagePermission/update-permission', null, 'errMsgDialog', function(rs){
					$('#gridPermission').datagrid('reload');			
							
			}, null, null, null, null, function(rf){
				if (rf != undefined && rf != null && rf.errMsg != undefined && rf.errMsg != null && rf.errMsg.trim().length > 0) {
					$('#errMsgPermission').html(rf.errMsg).show();
					$('#gridPermission').datagrid('reload');
					return false;
				}

			});	
		}


	} 
};
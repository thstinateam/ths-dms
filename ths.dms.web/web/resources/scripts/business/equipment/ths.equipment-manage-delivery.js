var EquipmentManagerDelivery = {
	_editIndex:null,
	_lstEquipInRecord:null,
	_numberEquipInRecord:null,
	_lstEquipInsert:null,
	_mapDelivery:null,
	_countFile:0,
	_arrFileDelete:null,
	_lstShop: null,
	_customerId: null,
	_lstStockInsert: null,
	_approved: 2,
	_paramsSearch: null,
	_shopToCode: null,
	_lstEquipGroup: null,
	_lstEquipProvider: null,
	_lstEquipStock: null,
	_DELIVERY_CONTENT_NEW: 'DELIVERY_CONTENT_NEW',
	_equipNumberDeprec: 0,
	_categoryName: null,
	_capacity: null,
	_isSetStockCombo: 1,
	_curShopId: null,
	/**
	 * Lay lich sua giao nhan
	 * @author nhutnn
	 * @since 12/12/2014
	 */
	getHistoryDelivery: function(idRecord){
		$('#easyuiPopupHistory').show();
		$('#easyuiPopupHistory').dialog({  
	        closed: false,  
	        cache: false,  
	        modal: true,
	        width : 240,	        
	        height : 'auto',
	        onOpen: function(){	      
				$('#gridHistoryDelivery').datagrid({
					url : '/equipment-manage-delivery/search-history-delivery',
					autoRowHeight : true,
					fitColumns:true,
					width: $('#gridHistory').width(),
					height: "auto",
					scrollbarSize: 0,
					rownumbers:true,
					queryParams:{
						idRecordDelivery : idRecord
					},		
					columns:[[
					    {field: 'ngay', title: 'Ngày', width: 75, sortable:false, resizable: false, align: 'left'},					
						{field: 'trangThaiGiaoNhan', title: 'Trạng thái', width: 100, sortable: false, resizable: false,align: 'left'}			    
					]],
					onBeforeLoad: function(param){											 
					},
					onLoadSuccess: function(data){   			 
				   		$('.easyui-dialog .datagrid-header-rownumber').html('STT');	   		
					}
				});
	        }
		});
	},
	createParamsSearch: function () {
		EquipmentManagerDelivery._paramsSearch = null;
	},
	/**
	 * Tim kiem danh sach bien ban giao nhan
	 * @author nhutnn
	 * @since 12/12/2014
	 */
	searchRecordDelivery: function(){
		
		$('#errMsgInfo').html('').hide();
		var msg ='';
		if(msg.length == 0){
			msg = Utils.getMessageOfSpecialCharactersValidate('staffCode','Mã nhân viên');
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfSpecialCharactersValidate('equipCode','Mã thiết bị');
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfSpecialCharactersValidate('equipLendCode','Mã biên bản đề nghị cho mượn');
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfSpecialCharactersValidate('customerCode','Mã khách hàng');
		}
//		if(msg.length == 0){
//			msg = Utils.getMessageOfSpecialCharactersValidate('customerName','Tên khách hàng');
//		}		
		var fdate = $('#fDate').val().trim();
		if(msg.length == 0 && fdate != '' && !Utils.isDate(fdate) && fdate != '__/__/____'){
			msg = 'Ngày biên bản từ không tồn tại hoặc không đúng định dạng dd/mm/yyyy.';
		}
		var tdate = $('#tDate').val().trim();
		if(msg.length == 0 && fdate != '' && tdate != '' && !Utils.isDate(tdate) && tdate != '__/__/____'){
			msg = 'Đến ngày không tồn tại hoặc không đúng định dạng dd/mm/yyyy.';
		}
		var fdateContract = $('#fDateContract').val().trim();
		if(msg.length == 0 && fdateContract != '' && !Utils.isDate(fdateContract) && fdateContract != '__/__/____'){
			msg = 'Ngày hợp đồng từ không tồn tại hoặc không đúng định dạng dd/mm/yyyy.';
		}
		var tdateContract = $('#tDateContract').val().trim();
		if(msg.length == 0 && fdateContract != '' && tdateContract != '' && !Utils.isDate(tdateContract) && tdateContract != '__/__/____'){
			msg = 'Đến ngày không tồn tại hoặc không đúng định dạng dd/mm/yyyy.';
		}
		if(msg.length == 0 && fdate != '' && tdate != '' && !Utils.compareDate(fdate, tdate)){
			msg = 'Đến ngày biên bản không được nhỏ hơn Từ ngày biên bản';	
		}
		if(msg.length == 0 && fdateContract != '' && tdateContract != '' && !Utils.compareDate(fdateContract, tdateContract)){
			msg = 'Đến ngày hợp đồng không được nhỏ hơn Từ ngày hợp đồng';	
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfSpecialCharactersValidate('numberContract','Số hợp đồng');
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfSpecialCharactersValidate('recordCode','Mã biên bản');
		}
		if(msg.length > 0){
			$('#errMsgInfo').html(msg).show();
			return false;
		}
		
		var params=new Object(); 
		params.staffCode = $('#staffCode').val().trim();
		params.customerCode = $('#customerCode').val().trim();
//		params.customerName = $('#customerName').val().trim();
		params.fromDate = $('#fDate').val().trim();
		params.toDate = $('#tDate').val().trim();
		params.fromContractDate = $('#fDateContract').val().trim();
		params.toContractDate = $('#tDateContract').val().trim();
		params.numberContract = $('#numberContract').val().trim();
		params.recordCode = $('#recordCode').val().trim();
		params.statusRecord = $('#statusRecord').val().trim();
		params.statusDelivery = $('#statusDelivery').val().trim();
		params.statusPrint = $('#statusPrint').val().trim();
		params.equipLendCode = $('#equipLendCode').val().trim();
		params.equipCode = $('#equipCode').val().trim();

		var multiShop = $("#shop").data("kendoMultiSelect");
		var dataShop = multiShop.dataItems();
		if(dataShop!=null && dataShop.length>0) {
			var lstShop = new Array();
			var curShopCode = $('#curShopCode').val();
			for(var i=0;i<dataShop.length;i++){
				if(dataShop[i].shopCode != curShopCode) {
					lstShop.push(dataShop[i].shopCode);
				}
			}
			params.lstShop = lstShop.join(",");
		}
		
		// if (!AppData.hasOwnProperty('equipment')) {
		// 	AppData.equipment = {};
		// }
		// if (!AppData.equipment.hasOwnProperty('delivery')) {
		// 	AppData.equipment.delivery = {};
		// }

		// $.extend(AppData.equipment.delivery.search_param, params);
		// AppData.equipment.delivery.search_param.mutilShop = new Object();
		// AppData.equipment.delivery.search_param.mutilShop = $("#shop").data("kendoMultiSelect").value();


		// if (AppData.hasOwnProperty('equipment') 
		// 	&& AppData.equipment.hasOwnProperty('delivery')
		// 	&& AppData.equipment.delivery.hasOwnProperty('search_param')) {
			
		// }

		
		EquipmentManagerDelivery._mapDelivery = new Map();
		$('#gridRecordDelivery').datagrid('load',params);
		$('#gridRecordDelivery').datagrid('uncheckAll');
	},
	/**
	 * show popup import excel
	 * @author nhutnn
	 * @since 12/12/2014
	 */
	showDlgImportExcel: function(){
		$('#easyuiPopupImportExcel').show();
		$('.ErrorMsgStyle').hide();
		$('#easyuiPopupImportExcel').dialog({  
	        closed: false,  
	        cache: false,  
	        modal: true,
			height: 'auto',
	        onOpen: function(){	 
	        	$('.easyui-dialog .BtnGeneralStyle').css('color','');
	        },
	        onClose: function(){
	        	EquipmentManagerDelivery.searchRecordDelivery();
				$("#gridRecordDelivery").datagrid('uncheckAll');
				$("#fakefilepcBBGN").val("");
				$("#excelFileBBGN").val("");				
	        }
		});
	},
	/**
	 * import excel BBGN
	 * @author nhutnn
	 * @since 12/12/2014
	 */
	importExcelBBGN: function(){
		$('.ErrorMsgStyle').hide();
		VTUtilJS.importExcelUtils(function(data) {
			$("#errExcelMsgBBGN").html(data.message).show();		
			if(data.fileNameFail == null || data.fileNameFail == "" ){					
				setTimeout(function(){
					$('#errExcelMsgBBGN').html("").hide();
					$('#easyuiPopupImportExcel').dialog('close');
				}, 3000);				
			}else{
				$("#fakefilepcBBGN").val("");
				$("#excelFileBBGN").val("");			
				//$('#gridRecordDelivery').datagrid('reload');	
			}						
		}, "importFrmBBGN", "excelFileBBGN", null, "errExcelMsgBBGN");
		return false;
	},
	/**
	 * xuat file excel BBGN
	 * @author tamvnm
	 * @since 14/04/2015
	 */
	exportExcelBBGN: function(){
		
		$('#errMsgInfo').html('').hide();
		var msg ='';
		var lstRowsChecked = $('#gridRecordDelivery').datagrid('getChecked');
		var rows =$('#gridRecordDelivery').datagrid('getRows');	
		var lstIdChecked = new Array();
		if (rows.length == 0) {
			msg = 'Không có biên bản để xuất excel';
		}
		if (lstRowsChecked.length == 0 && msg.length == 0) {
			//msg = 'Bạn chưa chọn biên bản để xuất excel. Vui lòng chọn biên bản';
			// for (var i = 0, isize = rows.length; i < isize; i++) {
			// 	lstIdChecked.push(rows[i].idRecord);
			// }
		} else {
			for (var i = 0; i < lstRowsChecked.length; i++) {
				// if (i < lstRowsChecked.length - 1) {
				// 	lstIdChecked += lstRowsChecked[i].idRecord + ',';
				// } else {
				// 	lstIdChecked += lstRowsChecked[i].idRecord;
				// }
				
				lstIdChecked.push(lstRowsChecked[i].idRecord);

			}
		}
		if (msg.length > 0) {
			$('#errMsgInfo').html(msg).show();
			return false;
		}
		
		var params = new Object();
		if (lstRowsChecked.length > 0) {
			params.lstIdChecked = lstIdChecked;
		} else {
			params.staffCode = $('#staffCode').val().trim();
			params.customerCode = $('#customerCode').val().trim();
//			params.customerName = $('#customerName').val().trim();
			params.fromDate = $('#fDate').val().trim();
			params.toDate = $('#tDate').val().trim();
			params.fromContractDate = $('#fDateContract').val().trim();
			params.toContractDate = $('#tDateContract').val().trim();
			params.numberContract = $('#numberContract').val().trim();
			params.recordCode = $('#recordCode').val().trim();
			params.statusRecord = $('#statusRecord').val().trim();
			params.statusDelivery = $('#statusDelivery').val().trim();
			params.statusPrint = $('#statusPrint').val().trim();
			params.equipLendCode = $('#equipLendCode').val().trim();
			params.equipCode = $('#equipCode').val().trim();

			var multiShop = $("#shop").data("kendoMultiSelect");
			var dataShop = multiShop.dataItems();
			if (dataShop != null && dataShop.length > 0) {
				var lstShop = new Array();
				var curShopCode = $('#curShopCode').val();
				for (var i = 0, isize = dataShop.length; i < isize; i++) {
					if (dataShop[i].shopCode != curShopCode) {
						lstShop.push(dataShop[i].shopCode);
					}
				}
				params.lstShop = lstShop.join(",");
			}
		}
		$.messager.confirm('Xác nhận', 'Bạn có muốn xuất file excel?', function(r) {
			if (r) {
				ReportUtils.exportReport('/equipment-manage-delivery/export-record-delivery', params, 'errMsgInfo');
			}
		});
		return false;
	},

	// /**
	//  * xuat file excel BBGN
	//  * @author nhutnn
	//  * @since 12/12/2014
	//  */
	// exportExcelBBGN: function(){
	// 	// $('.ErrorMsgStyle').html("").hide();
	// 	// //var lstChecked = $('#gridRecordDelivery').datagrid('getChecked');
	// 	// var rows = $('#gridRecordDelivery').datagrid('getRows');	
	// 	// if(rows.length==0) {
	// 	// 	$('#errMsgInfo').html('Không có biên bản để xuất excel').show();
	// 	// 	return;
	// 	// }
	// 	// // if(lstChecked.length == 0) {
	// 	// // 	$('#errMsgInfo').html('Bạn chưa chọn biên bản để xuất excel. Vui lòng chọn biên bản').show();
	// 	// // 	return;
	// 	// // }	
	// 	// // lay danh sach bien ban
	// 	// var lstIdRecord = new Array();
	// 	// for(var i=0; i<rows.length; i++){
	// 	// 	lstIdRecord.push(rows[i].idRecord);
	// 	// }
	// 	$('#errMsgInfo').html('').hide();
	// 	var msg ='';
	// 	if(msg.length == 0){
	// 		msg = Utils.getMessageOfSpecialCharactersValidate('staffCode','Mã nhân viên');
	// 	}
	// 	if(msg.length == 0){
	// 		msg = Utils.getMessageOfSpecialCharactersValidate('customerCode','Mã khách hàng');
	// 	}
	// 	if(msg.length == 0){
	// 		msg = Utils.getMessageOfSpecialCharactersValidate('customerName','Tên khách hàng');
	// 	}		
	// 	var fdate = $('#fDate').val().trim();
	// 	if(msg.length == 0 && fdate != '' && !Utils.isDate(fdate)){
	// 		msg = 'Từ ngày không tồn tại hoặc không đúng định dạng dd/mm/yyyy.';
	// 	}
	// 	var tdate = $('#tDate').val().trim();
	// 	if(msg.length == 0 && fdate != '' && !Utils.isDate(tdate)){
	// 		msg = 'Đến ngày không tồn tại hoặc không đúng định dạng dd/mm/yyyy.';
	// 	}
	// 	if(msg.length == 0 && fdate != '' && tdate != '' && !Utils.compareDate(fdate, tdate)){
	// 		msg = 'Đến ngày không được nhỏ hơn Từ ngày';	
	// 	}
	// 	if(msg.length == 0){
	// 		msg = Utils.getMessageOfSpecialCharactersValidate('numberContract','Số hợp đồng');
	// 	}
	// 	if(msg.length == 0){
	// 		msg = Utils.getMessageOfSpecialCharactersValidate('recordCode','Mã biên bản');
	// 	}
	// 	if(msg.length > 0){
	// 		$('#errMsgInfo').html(msg).show();
	// 		return false;
	// 	}
		
	// 	var params=new Object(); 
	// 	params.staffCode = $('#staffCode').val().trim();
	// 	params.customerCode = $('#customerCode').val().trim();
	// 	params.customerName = $('#customerName').val().trim();
	// 	params.fromDate = $('#fDate').val().trim();
	// 	params.toDate = $('#tDate').val().trim();
	// 	params.numberContract = $('#numberContract').val().trim();
	// 	params.recordCode = $('#recordCode').val().trim();
	// 	params.statusRecord = $('#statusRecord').val().trim();
	// 	params.statusDelivery = $('#statusDelivery').val().trim();
		
	// 	$.messager.confirm('Xác nhận','Bạn có muốn xuất file excel?',function(r){
	// 		if(r){
	// 			ReportUtils.exportReport('/equipment-manage-delivery/export-record-delivery', params, 'errMsgInfo');					
	// 		}
	// 	});		
	// 	return false;
	// },

	 /**
	 * tai file excel template
	 * @author tamvnm
	 * @since 31/05/2015
	 */
	 downloadTemplate:function () {
	 	var params = new Object();
	 	ReportUtils.exportReport('/equipment-manage-delivery/download-template', params, 'errMsg');
	 },
	/**
	 * in BBGN
	 * @author nhutnn
	 * @since 14/01/2015
	 */
	 ShowPopUpPrint: function(id, recordStatus){
	 	var lstIdRecord = new Array();
		if(id != null){		
			// if (recordStatus != 4) {
			// 	lstIdRecord.push(id);
			// } else {
			// 	$('#errMsgInfo').html('Không thể in biên bản đã Hủy. Vui lòng chọn biên bản khác!').show();
			// 	return;
			// }
			lstIdRecord.push(id);
		}else{
			if(EquipmentManagerDelivery._mapDelivery.keyArray.length == 0) {
				$('#errMsgInfo').html('Bạn chưa chọn biên bản để xem bản in. Vui lòng chọn biên bản!').show();
				return;
			}	
			for(var i=0; i<EquipmentManagerDelivery._mapDelivery.keyArray.length; i++){
				lstIdRecord.push(EquipmentManagerDelivery._mapDelivery.keyArray[i]);
			}
		}		
	 	$('#easyuiPopupPrint').show();
		$('.ErrorMsgStyle').hide();
		$('#easyuiPopupPrint').dialog({  
	        closed: false,  
	        cache: false,  
	        modal: true,
			height: 'auto',
	        onOpen: function(){	 
	        	$('.easyui-dialog .BtnGeneralStyle').css('color','');
	        	$('#printDelivery').attr('checked', true);
	        	$('#printRecord').attr('checked', true);
	        	$('#printLend').attr('checked', true);
	        	$("#statusIn").prop("disabled", false);
				$("#comboboxPrint").prop("disabled", false);
	        	$('#printLend').on( "click", function(){
				    if ($(this).is(":checked")) {
				      $("#statusIn").prop("disabled", false);
				      $("#comboboxPrint").prop("disabled", false);
				   	} else {
				      $("#statusIn").prop("disabled", true); 
				      $("#comboboxPrint").prop("disabled", true);
				   	}
				});
	        },
	        onClose: function(){
	        	EquipmentManagerDelivery.searchRecordDelivery();
				$("#gridRecordDelivery").datagrid('uncheckAll');				
	        }
		});
		
	 },

	printBBGN: function(id){
		$('.ErrorMsgStyle').html("").hide();	
		//lay danh sach bien ban
		var lstIdRecord = new Array();
		if(id != null){			
			lstIdRecord.push(id);
		}else{
			if(EquipmentManagerDelivery._mapDelivery.keyArray.length == 0) {
				$('#errMsgInfo').html('Bạn chưa chọn biên bản để xem bản in. Vui lòng chọn biên bản!').show();
				return;
				// var rows = $('#gridRecordDelivery').datagrid('getRows');
				
				// for (var i = 0, isize = rows.length; i < isize; i++) {
				// 	lstIdRecord.push(rows[i].idRecord);
				// }
			} else {
				for(var i=0; i<EquipmentManagerDelivery._mapDelivery.keyArray.length; i++){
					lstIdRecord.push(EquipmentManagerDelivery._mapDelivery.keyArray[i]);
				}
			}
			
		}

		var params = new Object();
		params.lstIdRecord = lstIdRecord;
		try {
			params.reportCode = $('#function_code').val();
		} catch(e) {
			// Oop! Error
		}
		var isPrintDelivery = false;
		var isPrintRecord = false;
		var isPrintLend = false;

		if ($('#printDelivery').is(":checked")) {
			// params.lstPrint.push(4);
			isPrintDelivery = true;
		}
		if ($('#printRecord').is(":checked")) {
			// params.lstPrint.push(5);
			isPrintRecord = true;
		}
		if ($('#printLend').is(":checked")) {
			// params.lstPrint.push($('#statusIn').val());
			params.printType = $('#statusIn').val();
			isPrintLend = true;
		}
		
		$.messager.confirm('Xác nhận','Bạn có muốn xem bản in biên bản?',function(r){
			if(r){
				if (isPrintDelivery) {
					ReportUtils.exportReportWithCallBack(params, '/equipment-manage-delivery/print-delivery', 'errMsg', function(data){					
						if (isPrintRecord) {
							ReportUtils.exportReportWithCallBack(params, '/equipment-manage-delivery/print-record', 'errMsg', function(data) {					
								if (isPrintLend) {
									ReportUtils.exportReport('/equipment-manage-delivery/print-lend', params, 'errMsg');	
								}
							}, null, null);
						} else if (isPrintLend) {
								ReportUtils.exportReport('/equipment-manage-delivery/print-lend', params, 'errMsg');	
						}
					}, null, null);
					// ReportUtils.exportReport('/equipment-manage-delivery/print-delivery', params, 'errMsgInfo');
				} else if (isPrintRecord) {
					ReportUtils.exportReportWithCallBack(params, '/equipment-manage-delivery/print-record', 'errMsg', function(data){					
						if (isPrintLend) {
							ReportUtils.exportReport('/equipment-manage-delivery/print-lend', params, 'errMsg');	
						}
					}, null, null);
				} else if (isPrintLend) {
					ReportUtils.exportReport('/equipment-manage-delivery/print-lend', params, 'errMsg');
				}
				
			}
		});		
		
		return false;
	},
	/**
	 * Luu bien ban ban giao
	 * @author nhutnn
	 * @since 12/12/2014
	 */
	saveRecordDelivery: function(){
		$('#errMsgInfo').html('').hide();
		// var rows = $('#gridRecordDelivery').datagrid('getRows');
		// if(rows.length == 0){
		// 	$('#errMsgInfo').html('Không có dữ liệu để lưu').show();
		// 	return false;
		// }
		var lstRecordSuccess = new Array();
		var lstCheck = new Array();
		if(EquipmentManagerDelivery._mapDelivery.keyArray.length == 0) {
			$('#errMsgInfo').html('Bạn chưa chọn biên bản. Vui lòng chọn biên bản!').show();
			return;
		}	
		for(var i=0; i<EquipmentManagerDelivery._mapDelivery.keyArray.length; i++){
			lstCheck.push(EquipmentManagerDelivery._mapDelivery.keyArray[i]);
		}
		//var lstCheck = $('#gridRecordDelivery').datagrid('getChecked');
		if(lstCheck.length > 0){
			var statusRecordChange = $('#statusRecordLabel').val().trim();
			var statusDeliveryChange = $('#statusDeliveryLabel').val().trim();
			for(var i=0; i < lstCheck.length; i++){				
				if(statusRecordChange != "" || statusDeliveryChange != ""){
					//lstRecordSuccess.push(lstCheck[i].idRecord);
					lstRecordSuccess.push(lstCheck[i]);
				}
			}	
			if(lstRecordSuccess.length <= 0){
				$('#errMsgInfo').html('Bạn chưa chọn trạng thái để lưu!').show();
				return;
			}
			var params = new Object();
			params.lstIdRecord = lstRecordSuccess;
			params.statusDelivery = statusDeliveryChange;
			params.statusRecord = statusRecordChange;
			
			Utils.addOrSaveData(params, '/equipment-manage-delivery/save-record-delivery', null, 'errMsgInfo', function(data) {
				if(data.error){
					$('#errMsgInfo').html(data.errMsgInfo).show();
					return;
				}
				if(data.lstRecordError != null && data.lstRecordError.length>0){					
					VCommonJS.showDialogList({		  
						dialogInfo: {
							title: 'Lỗi'
						},
						data : data.lstRecordError,			   
						columns : [[
							{field:'formCode', title:'Mã biên bản', align:'left', width: 150, sortable:false, resizable:false},
							{field:'formStatusErr', title:'Trạng thái biên bản', align:'center', width: 80, sortable:false, resizable:false,formatter: function(value, row, index){
								var html="";
								if(row.formStatusErr == 1){
									html = '<img src="/resources/images/icon_delete.png" height="20" width="20" style="margin-right:5px;" title="Error">';
								}
								return html;
								
							}},
							{field:'deliveryStatusErr', title:'Trạng thái giao nhận', align:'center', width: 80, sortable:false, resizable:false,formatter: function(value, row, index){
								var html="";
								if(row.deliveryStatusErr == 1){
									html = '<img src="/resources/images/icon_delete.png" height="20" width="20" style="margin-right:5px;" title="Error">';
								}
								return html;
								
							}},
							// {field:'periodError', title:'Kỳ của biên bản', align:'center', width:80, sortable:false, resizable:false,formatter: function(value, row, index){
							// 	var html="";
							// 	if(row.periodError == 1){
							// 		html = '<img src="/resources/images/icon_delete.png" height="20" width="20" style="margin-right:5px;" title="Error">';
							// 	}
							// 	return html;
							// }},
							{field:'recordNotComplete', title:'Chi tiết biên bản', align:'left', width:80, sortable:false, resizable:false,formatter: function(value, row, index){
								return VTUtilJS.XSSEncode(value);
							}}
						]]
					});
				}else{
					$('#successMsgInfo').html('Lưu dữ liệu thành công').show();
					EquipmentManagerDelivery.searchRecordDelivery();
				}
				//if(lstRecordSuccess.length > 0){
				//	var msg = 'Lưu dữ liệu thành công các biên bản ';
				//	for(var i=0; i<lstRecordSuccess.length; i++){
				//		msg += lstRecordSuccess[i]+', ';
				//	}
				//	msg = msg.substring(0,msg.length - 2);
				//	msg += '!';
				//	$('#errMsgInfo').html(msg).show();
				//}
				setTimeout(function(){ 
					$('#errMsgInfo').html('').hide();
				}, 5000);
				
				$('#gridRecordDelivery').datagrid('reload');
				$('#gridRecordDelivery').datagrid('uncheckAll');
			}, null, null, null, "Bạn có muốn cập nhật ?", null);	
		}else{
			$('#errMsgInfo').html('Bạn chưa chọn biên bản để lưu').show();
			return false;
		}		
	},
	/**
	 * kiem tra ki
	 * @author nhutnn
	 * @since 19/12/2014
	 */
	checkPeriod: function(id){
		// Utils.getJSONDataByAjaxNotOverlay({id : id}, '/equipment-manage-delivery/check-equip-period', function(data){						
		// 	if(data.error){
		// 		$('#errMsgInfo').html(data.errMsg).show();
		// 		return false;
		// 	}else{
		// 		if(id > 0){
		// 			window.location.href="/equipment-manage-delivery/change-record?id="+id;
		// 		}else{
		// 			window.location.href="/equipment-manage-delivery/change-record";
		// 		}				
		// 	}
		// }, null, null);		
		if (id > 0) {
			window.location.href = "/equipment-manage-delivery/change-record?id=" + id;
		} else {
			window.location.href = "/equipment-manage-delivery/change-record";
		}
	},

	getLstEquipInfo: function () {
		var params = new Object();
		var equipLendCode = $("#lendCode").val().trim();
		var customerId = $('#customerIdHidden').val();
		if (equipLendCode != undefined && equipLendCode != null && equipLendCode != '') {
			params.equipLendCode = equipLendCode;
		}
		if (customerId != undefined && customerId != null) {
			params.customerId = customerId;
		} 

		Utils.getJSONDataByAjaxNotOverlay(params,'/equipment-manage-delivery/get-equipment-info', function(result) {
			 if (result.lstEquipGroup != undefined && result.lstEquipGroup != null && result.lstEquipGroup.length > 0) {
			 	EquipmentManagerDelivery._lstEquipGroup = result.lstEquipGroup;
			 }
			  if (result.lstEquipProvider != undefined && result.lstEquipProvider != null && result.lstEquipProvider.length > 0) {
			 	EquipmentManagerDelivery._lstEquipProvider = result.lstEquipProvider;
			 }
		}, null, null);
	},


	/**
	 * tao gia tri cho autoComplete Combobox Nhom thiet bi
	 * @author tamnm
	 * @since 17/06/2015
	 */
	setEquipGroup: function(index) {
		var curIndex = null;
		if (index != undefined || index == null) {
			curIndex = EquipmentManagerDelivery._editIndex;
		} else {
			curIndex = index;
		}
		var equipGroup = EquipmentManagerDelivery._lstEquipGroup;
		for(var i = 0, sz = equipGroup.length; i < sz; i++) {
	 		equipGroup[i].searchText = unicodeToEnglish(equipGroup[i].code + " " + equipGroup[i].name);
	 		equipGroup[i].searchText = equipGroup[i].searchText.toUpperCase();
		}
	 	// equipGroup.splice(0, 0, { code: "", name: "", searchText:"" });
	 	$('#groupEquipCBX' + curIndex).combobox("loadData", equipGroup);
	},
	/**
	 * tao gia tri cho autoComplete Combobox Nha cung cap
	 * @author tamvnm
	 * @since 17/06/2015
	 */
	setEquipProvider: function(index) {
		var curIndex = null;
		if (index != undefined || index == null) {
			curIndex = EquipmentManagerDelivery._editIndex;
		} else {
			curIndex = index;
		}
		var equipProvider = EquipmentManagerDelivery._lstEquipProvider;
		for(var i = 0, sz = equipProvider.length; i < sz; i++) {
	 		equipProvider[i].searchText = unicodeToEnglish(equipProvider[i].code + " " + equipProvider[i].name);
	 		equipProvider[i].searchText = equipProvider[i].searchText.toUpperCase();
		}
	 	// equipGroup.splice(0, 0, { code: "", name: "", searchText:"" });
	 	$('#providerEquipCBX' + curIndex).combobox("loadData", equipProvider);
	},
	/**
	 * tao gia tri cho autoComplete Combobox kho NPP
	 * @author tamvnm
	 * @since Oct 21 2015
	 */
	setEquipStock: function(index) {
		var curIndex = index;
		if (index != undefined || index == null) {
			curIndex = EquipmentManagerDelivery._editIndex;
		}
		var equipStocks = EquipmentManagerDelivery._lstEquipStock;
		for (var i = 0, sz = equipStocks.length; i < sz; i++) {
			var searchText = unicodeToEnglish(equipStocks[i].stockCode + " " + equipStocks[i].stockName);
			equipStocks[i].searchText = searchText.toUpperCase();
		}
		$('#stockEquipCBX' + curIndex).combobox("loadData", equipStocks);
	},
	/**
	 * Xu ly trên input Don vi
	 * @author nhutnn
	 * @since 19/12/2014
	 */
	onChangeShopCode: function(code) {
		var shopToCode = $('#shopToCode').val().trim();
		EquipmentManagerDelivery._shopToCode = shopToCode;
		if (shopToCode != undefined && shopToCode != null && shopToCode != '') {
			$.getJSON('/equipment-manage-delivery/getListGSNPP?toShopCode=' + shopToCode, function(result) {
				if (result.rows != undefined && result.rows != null && result.rows.length > 0) {
					for (var i = 0, sz = result.rows.length; i < sz; i++) {
						result.rows[i].searchText = unicodeToEnglish(result.rows[i].staffCode + " " + result.rows[i].staffName);
						result.rows[i].searchText = result.rows[i].searchText.toUpperCase();
					}
					result.rows.splice(0, 0, {
						staffCode: "",
						staffName: "",
						searchText: ""
					});
					$('#staffMonitorCode').combobox("loadData", result.rows);
					if (result.rows.length > 1) {
						if (code != undefined && code != '' && code != null) {
							$('#staffMonitorCode').combobox("setValue", code);

						} else {
							$('#staffMonitorCode').combobox("setValue", result.rows[1].staffCode);
						}

					} else {
						$('#staffMonitorCode').combobox("setValue", "");
					}

				} else {
					var rows = [{
						staffCode: "",
						staffName: "",
						searchText: ""
					}];
					$('#staffMonitorCode').combobox("loadData", rows);
					$('#staffMonitorCode').combobox("setValue", "");
				}
				EquipmentManagerDelivery.getDataEquipStock(EquipmentManagerDelivery._isSetStockCombo);
			});
		}
	},
	/**
	 * Chon don vi
	 * @author nhutnn
	 * @since 12/12/2014
	 */
	selectFromShop: function(id,code,name){
		$('#shopFromCode').val(code).change();
		 // $('#customerCode').val("").change();	
		//$('#stockCode').val(code).change();		
		$('.easyui-dialog').dialog('close');
	},
	/**
	 * Chon don vi
	 * @author nhutnn
	 * @since 12/12/2014
	 * 
	 * @author hunglm16
	 * @since August 18, 2015
	 * @description Bo sung luong them moi
	 */
	selectToShop: function(id, code, name) {
		if (code != undefined) {
			$('#shopToCode').val(code).change();
			$('#shopToId').val(id);
			$('#customerCode').val("").change();
			$('#staffMonitorCode').combobox("setValue", "");
			//Xu lu luong bo sung
			$('#addressName').val("");
			$('#street').val("");
			$('#wardName').val("");
			$('#districtName').val("");
			$('#provinceName').val("");
			EquipmentManagerDelivery._customerId = null;
		}

		//$('#stockCode').val(code).change();
		try {
			$('.easyui-dialog').dialog('close');
			EquipmentManagerDelivery._shopToCode = $('#shopToCode').val().trim();
		} catch (e) {
			console.log('Dialog chưa mở đã gọi đóng.');
		}
		var shopToCode = $('#shopToCode').val();
		if (shopToCode != undefined && shopToCode != null && shopToCode != '') {
			$.getJSON('/equipment-manage-delivery/getListGSNPP?toShopCode=' + shopToCode, function(result) {
				if (result.rows != undefined && result.rows != null && result.rows.length > 0) {
					for (var i = 0, sz = result.rows.length; i < sz; i++) {
						result.rows[i].searchText = unicodeToEnglish(result.rows[i].staffCode + " " + result.rows[i].staffName);
						result.rows[i].searchText = result.rows[i].searchText.toUpperCase();
					}
					result.rows.splice(0, 0, {
						staffCode: "",
						staffName: "",
						searchText: ""
					});
					$('#staffMonitorCode').combobox("loadData", result.rows);
					// $('#staffMonitorCode').combobox("setValue", "");
					if (result.rows.length > 1) {
						$('#staffMonitorCode').combobox("setValue", result.rows[1].staffCode);
					} else {
						$('#staffMonitorCode').combobox("setValue", "");
					}
				} else {
					var rows = [{
						staffCode: "",
						staffName: "",
						searchText: ""
					}];
					$('#staffMonitorCode').combobox("loadData", rows);
				}
				EquipmentManagerDelivery.getDataEquipStock(EquipmentManagerDelivery._isSetStockCombo);
			});
		} else {
			var rows = [{
				staffCode: "",
				staffName: "",
				searchText: ""
			}];
			$('#staffMonitorCode').combobox("loadData", rows);
			EquipmentManagerDelivery.getDataEquipStock(EquipmentManagerDelivery._isSetStockCombo);
		}
	},
	/**
	 * Chon khach hang
	 * @author nhutnn
	 * @since 12/12/2014
	 * 
	 * @author hunglm16
	 * @since August 18, 2015
	 * @description ra soat va hotfix
	 */
	selectCustomer: function(shortCode, id, street, houseNumber, areaId){		
		$('#customerCode').val(shortCode);		
		if(id != undefined && id != null) {	
			EquipmentManagerDelivery._customerId = id;
		}
		$('#addressName').val('');
        $('#street').val('');
        if (!isNullOrEmpty(houseNumber)) {
        	$('#addressName').val(houseNumber);	
        }
        if (!isNullOrEmpty(street)) {
        	$('#street').val(street);	
        }
		var flag = false;
		var params = {};
		if (areaId != undefined && areaId != null) {
			params.id = areaId;
			flag = true;
		} else {
			params.customerId = id;
			flag = true;
		}
		$('#wardName').val("");
		$('#districtName').val("");
		$('#provinceName').val("");
		if (flag) {
			Utils.getJSONDataByAjax(params, '/commons/getAreaEntity', function (res){
				if (res != undefined && res != null) {
					if (res.area != undefined && res.area != null) {
						$('#wardName').val(res.area.areaName);
						if (res.area.parentArea != undefined && res.area.parentArea != null) {
							$('#districtName').val(res.area.parentArea.areaName);
							if (res.area.parentArea.parentArea != undefined && res.area.parentArea.parentArea != null) {
							  $('#provinceName').val(res.area.parentArea.parentArea.areaName);
							}
						}
					}
				}
				$('.easyui-dialog').dialog('close');
			});			
		} else {
			$('.easyui-dialog').dialog('close');
		}
	},
	/**
	 * Su kiem thay doi Ma khach hang
	 * 
	 * @author hunglm16
	 * @since August 17,2015
	 * */
	changeEventCustomerCode: function (txt) {
		$('#wardName').val("");
		$('#districtName').val("");
		$('#provinceName').val("");
		var shopToCode = $('#shopToCode').val().trim();
		var shortCode = $('#customerCode').val().trim();
		if (shopToCode.length > 0 && shortCode.length > 0) {
			var params = {};
			params.shortCode = shortCode;
			params.shopCode = shopToCode;
			Utils.getJSONDataByAjax(params, '/commons/getCustomerEntity', function (res){
				if (res != undefined && res != null && res.customer != null && res.customer != undefined) {
					$('#addressName').val(res.customer.housenumber);
					$('#street').val(res.customer.street);
					if (res.customer.area != undefined && res.customer.area != null) {
						$('#wardName').val(res.customer.area.areaName);
						if (res.customer.area.parentArea != undefined && res.customer.area.parentArea != null) {
							$('#districtName').val(res.customer.area.parentArea.areaName);
							if (res.customer.area.parentArea.parentArea != undefined && res.customer.area.parentArea.parentArea != null) {
							  $('#provinceName').val(res.customer.area.parentArea.parentArea.areaName);
							}
						}
					}
				}
			});
		}
	},
	/**
	 * Xoa thiet bi trong bien ban ban giao
	 * @author nhutnn
	 * @since 12/12/2014
	 */
	deleteEquipmentForDelivery: function(index){		
		var rowDelete = $('#gridEquipmentDelivery').datagrid('getRows')[index];
		if(EquipmentManagerDelivery._numberEquipInRecord != null && index < EquipmentManagerDelivery._numberEquipInRecord){
			if(EquipmentManagerDelivery._lstEquipInRecord == null){
				EquipmentManagerDelivery._lstEquipInRecord = new Array();
			}
			// var row = $('#gridEquipmentDelivery').datagrid('getRows')[index];
			EquipmentManagerDelivery._lstEquipInRecord.push(rowDelete.equipmentCode);
			EquipmentManagerDelivery._numberEquipInRecord--;
		}else if(EquipmentManagerDelivery._lstEquipInsert != null && index >= EquipmentManagerDelivery._numberEquipInRecord){
			// var row = $('#gridEquipmentDelivery').datagrid('getRows')[index];
			var ix = EquipmentManagerDelivery._lstEquipInsert.indexOf(rowDelete.equipmentCode);
			if (ix > -1) {
				EquipmentManagerDelivery._lstEquipInsert.splice(ix, 1);
			}
		}
		var rows = $('#gridEquipmentDelivery').datagrid('getRows');
		$('#gridEquipmentDelivery').datagrid("deleteRow", index);

		if (EquipmentManagerDelivery._editIndex == index) {
			EquipmentManagerDelivery._editIndex = null;
			return;
		}
		if (EquipmentManagerDelivery._editIndex == null) {
			var nSize = rows.length;
			for(var i = 0; i < nSize; i++) {
				var row = rows[i];
				var seriNumber = null;
				if (index == 0 || i >= index) {
					var curIndex = i + 1;
					seriNumber = $('#seriNumberInGrid' + curIndex).val();
				} else {
					seriNumber = $('#seriNumberInGrid' + i).val();
				}
				
				$('#gridEquipmentDelivery').datagrid('updateRow',{
					index: i,	
					row: row
				});	
				if ((row.seriNumber == null || row.seriNumber == '') && seriNumber != null && seriNumber != '') {
					$('#seriNumberInGrid' + i).val(seriNumber)
				}
			}
		} else {
			var curRow = $('#gridEquipmentDelivery').datagrid('getRows')[EquipmentManagerDelivery._editIndex - 1];
			var content = $('#contentDelivery').val();
			
			var equipCode = $('#equipmentCodeInGrid' + EquipmentManagerDelivery._editIndex).val();
			var seri = $('#seriNumberInGrid' + EquipmentManagerDelivery._editIndex).val();
			var dep = $('#depreciationInGrid' + EquipmentManagerDelivery._editIndex).val();
			var group = null;
			var provider = null;
			var stock = null;
			var price = null;
			var health = null;
			var manufacturingYear = null;

			if (curRow.groupEquipmentCode == undefined ) {
				group = $('#groupEquipCBX' + EquipmentManagerDelivery._editIndex).combobox('getValue');
				provider = $('#providerEquipCBX' + EquipmentManagerDelivery._editIndex).combobox('getValue');
				stock = $('#stockEquipCBX' + EquipmentManagerDelivery._editIndex).combobox('getValue');
				price = $('#price' + EquipmentManagerDelivery._editIndex).val();
				health = $('#healthStatus' + EquipmentManagerDelivery._editIndex).val();
				manufacturingYear = $('#yearManufacture' + EquipmentManagerDelivery._editIndex).val();
			}

			var nSize = rows.length - 1;
			for(var i = 0; i < nSize; i++) {
				var row = rows[i];
				var seriNumber = null;
				if (index == 0 || i >= index) {
					var curIndex = i + 1;
					seriNumber = $('#seriNumberInGrid' + curIndex).val();
				} else {
					seriNumber = $('#seriNumberInGrid' + i).val();
				}
				
				$('#gridEquipmentDelivery').datagrid('updateRow',{
					index: i,	
					row: row
				});	
				if ((row.seriNumber == null || row.seriNumber == '') && seriNumber != null && seriNumber != '') {
					$('#seriNumberInGrid' + i).val(seriNumber)
				}
			}

			EquipmentManagerDelivery._editIndex--;
			if (curRow.groupEquipmentCode != undefined ) {
				$('#gridEquipmentDelivery').datagrid("deleteRow", EquipmentManagerDelivery._editIndex);
				$('#gridEquipmentDelivery').datagrid("appendRow", {});
				
				$('#gridEquipmentDelivery').datagrid('updateRow',{
					index: EquipmentManagerDelivery._editIndex,	
					row: curRow
				});
				$('#contentDelivery').val(content);
				if (seri != undefined) {
					$('#seriNumberInGrid' + EquipmentManagerDelivery._editIndex).val(seri);
				}
				if (dep != undefined) {
					$('#depreciationInGrid' + EquipmentManagerDelivery._editIndex).val(dep);
				}

				EquipmentManagerDelivery.getEventContent();
				
			} else {
				$('#gridEquipmentDelivery').datagrid("deleteRow", EquipmentManagerDelivery._editIndex);
				$('#gridEquipmentDelivery').datagrid("appendRow", {});
				
				
				$('#contentDelivery').val(content);
				EquipmentManagerDelivery.setUIEquipInRecord();
				EquipmentManagerDelivery.getEventContent();
				if (content == EquipmentManagerDelivery._DELIVERY_CONTENT_NEW) {
					enableCombo('groupEquipCBX' + EquipmentManagerDelivery._editIndex);
				    enableCombo('providerEquipCBX' + EquipmentManagerDelivery._editIndex);
				    enableCombo('stockEquipCBX' + EquipmentManagerDelivery._editIndex);
				    enable('price' + EquipmentManagerDelivery._editIndex);
		            enable('healthStatus' + EquipmentManagerDelivery._editIndex);
		            enable('yearManufacture' + EquipmentManagerDelivery._editIndex);
		            disabled('equipmentCodeInGrid' + EquipmentManagerDelivery._editIndex);
				} else {
					disableCombo('groupEquipCBX' + EquipmentManagerDelivery._editIndex);
				    disableCombo('providerEquipCBX' + EquipmentManagerDelivery._editIndex);
				    disableCombo('stockEquipCBX' + EquipmentManagerDelivery._editIndex);
				    disabled('price' + EquipmentManagerDelivery._editIndex);
		            disabled('healthStatus' + EquipmentManagerDelivery._editIndex);
		            disabled('yearManufacture' + EquipmentManagerDelivery._editIndex);
		            enable('equipmentCodeInGrid' + EquipmentManagerDelivery._editIndex);
				}

	            EquipmentManagerDelivery.editInputInGrid();
				$('#groupEquipCBX' + EquipmentManagerDelivery._editIndex).combobox('setValue', group);
				$('#providerEquipCBX' + EquipmentManagerDelivery._editIndex).combobox('setValue', provider);
				$('#stockEquipCBX' + EquipmentManagerDelivery._editIndex).combobox('setValue', stock);
				$('#equipmentCodeInGrid' + EquipmentManagerDelivery._editIndex).val(equipCode);
				if (seri != undefined) {
					$('#seriNumberInGrid' + EquipmentManagerDelivery._editIndex).val(seri);
				}
				if (dep != undefined) {
					$('#depreciationInGrid' + EquipmentManagerDelivery._editIndex).val(dep);
				}
				if (price != undefined) {
					$('#price' + EquipmentManagerDelivery._editIndex).val(Utils.returnMoneyValue(price));
				}
				if (health != undefined) {
					$('#healthStatus' + EquipmentManagerDelivery._editIndex).val(health);
				}
				if (manufacturingYear != undefined) {
					$('#yearManufacture' + EquipmentManagerDelivery._editIndex).val(manufacturingYear);
				}

				
			}

		}
		
	},
	/**
	 * Them mot dong thiet bi
	 * @author nhutnn
	 * @since 19/12/2014
	 */
	insertEquipmentInGrid: function(){	
		$('#errMsgInfo').html('').hide();
		var sz = $("#gridEquipmentDelivery").datagrid("getRows").length;
		if(EquipmentManagerDelivery._editIndex != null && EquipmentManagerDelivery._editIndex == sz-1){
			var row = $('#gridEquipmentDelivery').datagrid('getRows')[EquipmentManagerDelivery._editIndex];
			if ($('#contentDelivery').val() != 'DELIVERY_CONTENT_NEW') {
				if($('#equipmentCodeInGrid' + EquipmentManagerDelivery._editIndex).val() == ''){
					$('#errMsgInfo').html('Bạn chưa nhập mã thiết bị').show();
					$('#equipmentCodeInGrid' + EquipmentManagerDelivery._editIndex).focus();
					return false;
				}else if($('#equipmentCodeInGrid' + EquipmentManagerDelivery._editIndex).val() != undefined){
					var rowEq = $('#gridEquipmentDelivery').datagrid('getRows');
					for(var i=0; i < rowEq.length-1; i++){
						if(rowEq[i].equipmentCode == $('#equipmentCodeInGrid' + EquipmentManagerDelivery._editIndex).val().trim()){
							$('#errMsgInfo').html("Mã thiết bị "+rowEq[i].equipmentCode+" đã tồn tại!").show();
							return false;
						}
					}
					row.equipmentCode = $('#equipmentCodeInGrid' + EquipmentManagerDelivery._editIndex).val();
				}
				if($('#seriNumberInGrid').val() == ''){
					// $('#errMsgInfo').html('Bạn chưa xử lý xong dòng thiết bị thêm mới!').show();
					// return false;
				}else if($('#seriNumberInGrid' + EquipmentManagerDelivery._editIndex).val() != undefined){
					row.seriNumber = $('#seriNumberInGrid' + EquipmentManagerDelivery._editIndex).val();
				}
				if($('#depreciationInGrid' + EquipmentManagerDelivery._editIndex).val() == ''){
					$('#errMsgInfo').html('Bạn chưa nhập số tháng khấu hao').show();
					$('#depreciationInGrid' + EquipmentManagerDelivery._editIndex).focus();
					return false;
				}else if($('#depreciationInGrid' + EquipmentManagerDelivery._editIndex).val() != undefined){
					row.depreciation = $('#depreciationInGrid' + EquipmentManagerDelivery._editIndex).val();
				}
				// if($('#stockInGrid').val() == ''){
				// 	$('#errMsgInfo').html('Bạn chưa xử lý xong dòng thiết bị thêm mới!').show();
				// 	return false;
				// }else if($('#stockInGrid').val() != undefined){
				// 	var toStockCode = $('#stockInGrid').val().trim();
				// 	row.toStock = $('#stockInGrid').val();
				// }
				if($('#contentDelivery').val() != undefined){
					row.contentDelivery = $('#contentDelivery').val();
				}
				
				$('#equipmentCodeInGrid' + EquipmentManagerDelivery._editIndex).remove();

				
				$('#seriNumberInGrid' + EquipmentManagerDelivery._editIndex).remove();

				
				// $('#stockInGrid').remove();
				$('#contentDelivery').remove();
				$('#gridEquipmentDelivery').datagrid('updateRow',{
					index: EquipmentManagerDelivery._editIndex,	
					row: row
				});
				if(EquipmentManagerDelivery._lstEquipInsert == null){
					EquipmentManagerDelivery._lstEquipInsert = new Array();
				}
				
				EquipmentManagerDelivery._lstEquipInsert.push(row.equipmentCode);
			} else {
				if($('#equipmentCodeInGrid' + EquipmentManagerDelivery._editIndex).val() != '') {
					$('#errMsgInfo').html('Bạn không được nhập mã thiết bị').show();
					$('#equipmentCodeInGrid' + EquipmentManagerDelivery._editIndex).focus();
					return false;
				} else if ($('#equipmentCodeInGrid' + EquipmentManagerDelivery._editIndex).val() != undefined) {
					// var rowEq = $('#gridEquipmentDelivery').datagrid('getRows');
					// for(var i=0; i < rowEq.length-1; i++){
					// 	if(rowEq[i].equipmentCode == $('#equipmentCodeInGrid' + EquipmentManagerDelivery._editIndex).val().trim()){
					// 		$('#errMsgInfo').html("Mã thiết bị "+rowEq[i].equipmentCode+" đã tồn tại!").show();
					// 		return false;
					// 	}
					// }
					// row.equipmentCode = $('#equipmentCodeInGrid' + EquipmentManagerDelivery._editIndex).val();
				}

				// nhom thiet bi
				if ($('#groupEquipCBX' + EquipmentManagerDelivery._editIndex).combobox('getValue') == '') {
					$('#errMsgInfo').html('Bạn chưa chọn nhóm thiết bị').show();
					$('#groupEquipCBX' + EquipmentManagerDelivery._editIndex).focus();
					return false;
				} else {
					row.groupEquipmentCode = $('#groupEquipCBX' + EquipmentManagerDelivery._editIndex).combobox('getValue');
					row.groupEquipmentName = $('#groupEquipCBX' + EquipmentManagerDelivery._editIndex).combobox('getText');
					if (EquipmentManagerDelivery._categoryName != null) {
						row.typeEquipment = EquipmentManagerDelivery._categoryName;
						EquipmentManagerDelivery._categoryName = null;
					}
					if (EquipmentManagerDelivery._capacity != null) {
						row.capacity = EquipmentManagerDelivery._capacity;
						EquipmentManagerDelivery._capacity = null;
					}
				}
				// Nha Cung cap
				if ($('#providerEquipCBX' + EquipmentManagerDelivery._editIndex).combobox('getValue') == '') {
					$('#errMsgInfo').html('Bạn chưa chọn nhà cung cấp').show();
					$('#providerEquipCBX' + EquipmentManagerDelivery._editIndex).focus();
					return false;
				} else {
					row.equipmentProviderCode = $('#providerEquipCBX' + EquipmentManagerDelivery._editIndex).combobox('getValue');
					row.equipmentProvider = $('#providerEquipCBX' + EquipmentManagerDelivery._editIndex).combobox('getText');
					
				}

				// kho NPP
				if ($('#stockEquipCBX' + EquipmentManagerDelivery._editIndex).combobox('getValue') == '') {
					$('#errMsgInfo').html('Bạn chưa chọn Kho').show();
					$('#stockEquipCBX' + EquipmentManagerDelivery._editIndex).focus();
					return false;
				} else {
					row.stockCode = $('#stockEquipCBX' + EquipmentManagerDelivery._editIndex).combobox('getValue');
					row.stockName = $('#stockEquipCBX' + EquipmentManagerDelivery._editIndex).combobox('getText');
					
				}

				if($('#seriNumberInGrid').val() == ''){
					// $('#errMsgInfo').html('Bạn chưa xử lý xong dòng thiết bị thêm mới!').show();
					// return false;
				}else if($('#seriNumberInGrid' + EquipmentManagerDelivery._editIndex).val() != undefined){
					row.seriNumber = $('#seriNumberInGrid' + EquipmentManagerDelivery._editIndex).val();
				}

				if($('#depreciationInGrid' + EquipmentManagerDelivery._editIndex).val() == ''){
					$('#errMsgInfo').html('Bạn chưa nhập số tháng khấu hao').show();
					$('#depreciationInGrid' + EquipmentManagerDelivery._editIndex).focus();
					return false;
				}else if($('#depreciationInGrid' + EquipmentManagerDelivery._editIndex).val() != undefined){
					row.depreciation = $('#depreciationInGrid' + EquipmentManagerDelivery._editIndex).val();
				}

				if($('#price' + EquipmentManagerDelivery._editIndex).val() == ''){
					$('#errMsgInfo').html('Bạn chưa nhập giá trị TBBH').show();
					$('#price' + EquipmentManagerDelivery._editIndex).focus();
					return false;
				} else if ($('#price' + EquipmentManagerDelivery._editIndex).val() < 100) {
					$('#errMsgInfo').html('Giá trị TBBH phải lớn hơn 100').show();
					$('#price' + EquipmentManagerDelivery._editIndex).focus();
					return false;
				} else if($('#price' + EquipmentManagerDelivery._editIndex).val() != undefined){
					var price = $('#price' + EquipmentManagerDelivery._editIndex).val();
					if (price != undefined) {
						row.price = Utils.returnMoneyValue(price);
					}
					
				}
				// if($('#stockInGrid').val() == ''){
				// 	$('#errMsgInfo').html('Bạn chưa xử lý xong dòng thiết bị thêm mới!').show();
				// 	return false;
				// }else if($('#stockInGrid').val() != undefined){
				// 	var toStockCode = $('#stockInGrid').val().trim();
				// 	row.toStock = $('#stockInGrid').val();
				// }
				if($('#contentDelivery').val() != undefined){
					row.contentDelivery = $('#contentDelivery').val();
				}

				if ($('#healthStatus' + EquipmentManagerDelivery._editIndex).val() == '') {
					$('#errMsgInfo').html('Bạn chưa chọn trình trạng thiết bị').show();
					$('#healthStatus' + EquipmentManagerDelivery._editIndex).focus();
					return false;
				} else {
					row.healthStatus = $('#healthStatus' + EquipmentManagerDelivery._editIndex).val();
					
				}

				if ($('#yearManufacture' + EquipmentManagerDelivery._editIndex).val() == '') {
					$('#errMsgInfo').html('Bạn chưa nhập năm sản xuất').show();
					$('#yearManufacture' + EquipmentManagerDelivery._editIndex).focus();
					return false;
				} else if ($('#yearManufacture' + EquipmentManagerDelivery._editIndex).val() > Utils.getCurrentYear) {
					$('#errMsgInfo').html('Năm sản xuất phải nhỏ hơn hoặc bằng năm hiện tại').show();
					$('#yearManufacture' + EquipmentManagerDelivery._editIndex).focus();
				} else {
					row.yearManufacture = $('#yearManufacture' + EquipmentManagerDelivery._editIndex).val();
				}

				$('#equipmentCodeInGrid' + EquipmentManagerDelivery._editIndex).remove();

				$('#seriNumberInGrid' + EquipmentManagerDelivery._editIndex).remove();

				// $('#stockInGrid').remove();
				$('#contentDelivery').remove();
				$('#gridEquipmentDelivery').datagrid('updateRow',{
					index: EquipmentManagerDelivery._editIndex,	
					row: row
				});
				if(EquipmentManagerDelivery._lstEquipInsert == null){
					EquipmentManagerDelivery._lstEquipInsert = new Array();
				}
				disabled('equipmentCodeInGrid' + EquipmentManagerDelivery._editIndex);

				EquipmentManagerDelivery._lstEquipInsert.push(row.equipmentCode);
			}
			
		}		
		EquipmentManagerDelivery._editIndex = sz;	
		$('#gridEquipmentDelivery').datagrid("appendRow", {});	
		// F9 tren grid
		EquipmentManagerDelivery.editInputInGrid();
		$("#gridEquipmentDelivery").datagrid("selectRow", EquipmentManagerDelivery._editIndex);	
		$('#equipmentCodeInGrid' + EquipmentManagerDelivery._editIndex).focus();
		// $('#contentDelivery').width($('#contentDelivery').parent().width());	
		// $('#equipmentCodeInGrid' + EquipmentManagerDelivery._editIndex).width($('#equipmentCodeInGrid' + EquipmentManagerDelivery._editIndex).parent().width());
		// $('#seriNumberInGrid').width($('#seriNumberInGrid').parent().width());
		// $('#stockInGrid').width($('#stockInGrid').parent().width());

		Utils.bindComboboxEquipGroupEasyUI('groupEquipCBX' + EquipmentManagerDelivery._editIndex);
		EquipmentManagerDelivery.setEquipGroup();
		Utils.bindComboboxEquipGroupEasyUI('providerEquipCBX' + EquipmentManagerDelivery._editIndex);
		EquipmentManagerDelivery.setEquipProvider();
		Utils.bindComboboxEquipGroupEasyUI('stockEquipCBX' + EquipmentManagerDelivery._editIndex);
		EquipmentManagerDelivery.setEquipStock();
		disableCombo('groupEquipCBX' + EquipmentManagerDelivery._editIndex);
		disableCombo('providerEquipCBX' + EquipmentManagerDelivery._editIndex);
		disableCombo('stockEquipCBX' + EquipmentManagerDelivery._editIndex);


		if ($('#price'+ EquipmentManagerDelivery._editIndex).val().indexOf(',')==-1) {
			$('#price'+ EquipmentManagerDelivery._editIndex).val(formatCurrency($('#price'+ EquipmentManagerDelivery._editIndex).val()));
			Utils.formatCurrencyFor('price'+ EquipmentManagerDelivery._editIndex);
			Utils.bindFormatOnTextfield('price'+ EquipmentManagerDelivery._editIndex,Utils._TF_NUMBER_COMMA);
		}
		disabled('price' + EquipmentManagerDelivery._editIndex);
		disabled('healthStatus' + EquipmentManagerDelivery._editIndex);
		disabled('yearManufacture' + EquipmentManagerDelivery._editIndex);
		// $('#contentDelivery').change(function() {
		//   if (this.value != 'DELIVERY_CONTENT_NEW') {
		//       $('#gridEquipmentDelivery').datagrid('deleteRow',EquipmentManagerDelivery._editIndex);
		//   	  EquipmentManagerDelivery.addEquipmentInRecord();
		//       $('#contentDelivery').val(this.value);
		//       disableCombo('groupEquipCBX' + EquipmentManagerDelivery._editIndex);
		//       disableCombo('providerEquipCBX' + EquipmentManagerDelivery._editIndex);
		//       console.log('khong phai cap moi');
		//       disabled('price' + EquipmentManagerDelivery._editIndex);
		//       disabled('healthStatus' + EquipmentManagerDelivery._editIndex);
		//       enable('equipmentCodeInGrid' + EquipmentManagerDelivery._editIndex);
		//   } else {
		//   	$('#gridEquipmentDelivery').datagrid('deleteRow',EquipmentManagerDelivery._editIndex);
		//   	EquipmentManagerDelivery.addEquipmentInRecord();
		//     $('#contentDelivery').val(this.value);
		//     enableCombo('groupEquipCBX' + EquipmentManagerDelivery._editIndex);
		//     enableCombo('providerEquipCBX' + EquipmentManagerDelivery._editIndex);
		//     console.log('cap moi');
		//    	enable('price' + EquipmentManagerDelivery._editIndex);
		//     enable('healthStatus' + EquipmentManagerDelivery._editIndex);
		//     disabled('equipmentCodeInGrid' + EquipmentManagerDelivery._editIndex);
		//   }
		EquipmentManagerDelivery.getEventContent();

	},
	/**
	 * Show popup tim kiem thiet bi
	 * @author nhutnn
	 * @since 12/12/2014
	 */
	showPopupSearchEquipment: function(){
		$('#easyuiPopupSearchEquipment').show();
		$('#easyuiPopupSearchEquipment').dialog({  
	        closed: false,  
	        cache: false, 
	        modal: true,
	        width : 1000,
	        height: 'auto',
	        onOpen: function(){	    
	        	//setDateTimePicker('yearManufacturing');
	        	Utils.bindComboboxEquipGroupEasyUICodeName('groupEquipment', null, 193);
	        	Utils.bindComboboxEquipGroupEasyUICodeName('providerId', null, 193);


	        	var year = new Date();
	        	year = year.getFullYear(); 
	        	var op = '<option value="" selected="selected">Chọn</option><option value="'+year+'">'+year+'</option>';
	        	for(var i=1; i<100; i++){
	        		op+='<option value="'+(year-i)+'">'+(year-i)+'</option>';
	        	}
	        	$('#yearManufacturing').html(op).change();
	        	$('#equipmentCode, #seriNumber, #typeEquipment').bind('keyup',function(event){
	        		if(event.keyCode == keyCodes.ENTER){
	        			$('#btnSeachEquipmentDlg').click(); 
	        		}
	        	});
	        	$('#yearManufacturing').bind('keyup',function(event){
	        		if(event.keyCode == keyCodes.ENTER){
	        			$('#btnSeachEquipmentDlg').click(); 
	        		}
	        	});
	        	$('#groupEquipment, #providerId').next().find(".combo-text.validatebox-text").bind('keyup',function(event){
	        		if(event.keyCode == keyCodes.ENTER){
	        			$('#btnSeachEquipmentDlg').click(); 
	        		}
	        	});
	        	$('#stockCode').bind('keyup',function(event){
	        		if(event.keyCode == keyCodes.ENTER){
	        			$('#btnSeachEquipmentDlg').click(); 
	        		}
	        	});
	        	$('#stockCode').bind('keyup',function(event){
	        		if(event.keyCode == keyCodes.F9){
	        			EquipmentManagerDelivery.showPopupStock('stockCode');
	        		}
	        	});

	        	if ($('#stockInGrid').val() != undefined && $('#stockInGrid').val() != null && $('#stockInGrid').val() != '') {
	        		$('#stockCode').val($('#stockInGrid').val());
	        	}

	        	EquipmentManagerDelivery.getEquipCatalog();
	        	//EquipmentManagerDelivery.getEquipGroup(null);
	        	var rowEquip = $('#gridEquipmentDelivery').datagrid('getRows');
	        	var lstEquipmentCode = '';
	        	for (var i = 0; i < rowEquip.length; i++) {
	        		// lstEquipmentCode.push(rowEquip[i].equipmentCode);
	        		if (rowEquip[i].equipmentCode != null) {
		        		if (i < rowEquip.length - 1) {
		        			lstEquipmentCode += rowEquip[i].equipmentCode + ', ';
		        		} else {
		        			lstEquipmentCode += rowEquip[i].equipmentCode;
		        		}
	        		}
	        	}
	        	var params = new Object();
	        	var groupId = $('#groupEquipment').combobox('getValue');
	        	if (groupId == -1 || groupId == "") {
	        		groupId = "";
	        	}
	        	var providerId = $('#providerId').combobox('getValue');
	        	if (providerId == -1 || providerId == "") {
	        		providerId = "";
	        	}
				params = {
						customerId: $('#customerIdHidden').val(),
						equipCode: $('#equipmentCode').val().trim(),
						seriNumber: $('#seriNumber').val().trim(),
						categoryId: $('#typeEquipment').val(),
						// groupId: $('#groupEquipment').val(),
						groupId: groupId,
						// providerId: $('#providerId').val(),
						providerId: providerId,
						yearManufacture: $('#yearManufacturing').val(),
						stockCode: $('#stockCode').val().trim(),
						equipLendCode: $("#equipLendCode").val().trim(),
						lstEquipmentCode: lstEquipmentCode
					}
				if($("#idRecordHidden").val() != "" && EquipmentManagerDelivery._lstEquipInRecord != null){
					params.lstEquipDelete = "";
					for(var i=0; i<EquipmentManagerDelivery._lstEquipInRecord.length; i++){
						params.lstEquipDelete += EquipmentManagerDelivery._lstEquipInRecord[i]+',';
					}					
				}
				if(EquipmentManagerDelivery._lstEquipInsert != null){
					params.lstEquipAdd = "";
					for(var i=0; i<EquipmentManagerDelivery._lstEquipInsert.length; i++){
						params.lstEquipAdd += EquipmentManagerDelivery._lstEquipInsert[i]+',';
					}					
				}
				$('#equipmentGridDialog').datagrid({
					url : '/equipment-manage-delivery/search-equipment-delivery',
					autoRowHeight : true,
					rownumbers : true, 
					//fitColumns:true,
					pagination:true,
					scrollbarSize:0,
					pageSize:10,
					width: $('#equipmentGridDialogContainer').width(),
					height: 'auto',
					autoWidth: true,	
					queryParams: params,
					columns:[[
						{field: 'select',title:'',width: 50,sortable:false,resizable:false, align: 'left', formatter: function(value, row, index) {
					    	return '<a href="javascript:void(0);" onclick="EquipmentManagerDelivery.selectEquipment('+index+');">Chọn</a>';
						}},
					    {field: 'equipmentCode',title:'Mã thiết bị',width: 200,sortable:false,resizable:false, align: 'left', formatter: function(value, row, index) {
							return Utils.XSSEncode(row.equipmentCode);
						}},	
					    {field: 'seriNumber',title:'Số serial',width: 100,sortable:false,resizable:false, align: 'left', formatter: function(value, row, index) {
							return Utils.XSSEncode(row.seriNumber);
						}},	
					    {field: 'healthStatus',title:'Tình trạng thiết bị',width: 100,sortable:false,resizable:false, align: 'left', formatter: function(value, row, index) {
							return Utils.XSSEncode(row.healthStatus);
						}},	
					    {field: 'stock',title:'Kho',width: 120,sortable:false,resizable:false, align: 'left', formatter: function(value, row, index) {
					    	var html="";
							if(row.stockCode != null && row.stockName!= null){
								html = row.stockCode+' - '+row.stockName; 
							}
							return Utils.XSSEncode(html);
						}},	
					    {field: 'typeEquipment',title:'Loại thiết bị',width: 80,sortable:false,resizable:false, align: 'left', formatter: function(value, row, index) {
							return Utils.XSSEncode(row.typeEquipment);
						}},	
					    {field: 'groupEquipment',title:'Nhóm thiết bị',width: 200,sortable:false,resizable:false, align: 'left', formatter: function(value, row, index) {
					    	var html="";
							if(row.groupEquipmentCode != null && row.groupEquipmentName!= null){
								html = row.groupEquipmentCode+' - '+row.groupEquipmentName; 
							}
							return Utils.XSSEncode(html);
						}},	
					    {field: 'capacity',title:'Dung tích (lít)',width: 80,sortable:false,resizable:false, align: 'center', formatter: function(value, row, index) {
							return Utils.XSSEncode(row.capacity);
						}},	
					    {field: 'equipmentBrand',title:'Hiệu',width: 80,sortable:false,resizable:false, align: 'left', formatter: function(value, row, index) {
							return Utils.XSSEncode(row.equipmentBrand);
						}},	
					    {field: 'equipmentProvider',title:'NCC',width: 80,sortable:false,resizable:false, align: 'left', formatter: function(value, row, index) {
							return Utils.XSSEncode(row.equipmentProvider);
						}},	
					    {field: 'yearManufacture',title:'Năm sản xuất',width: 80,sortable:false,resizable:false, align: 'left', formatter: function(value, row, index) {
							return Utils.XSSEncode(row.yearManufacture);
						}},											
					]],
					onBeforeLoad:function(param){											 
					},
					onLoadSuccess :function(data){   			 
						if (data == null || data.total == 0) {
							//$("#errMsgInfo").html("Không tìm thấy danh sách biên bản giao nhận thiết bị!").show();
						}else{
							$('.datagrid-header-rownumber').html('STT');		    	
						}	    	
						$('#equipmentCode').focus();	
						$('#easyuiPopupSearchEquipment').dialog('move', {top : 50});
					}
				});
	        },
			onClose: function(){
				$('#easyuiPopupSearchEquipment').hide();
				$('#equipmentCode').val('').change();
				$('#seriNumber').val('').change();
				$('#typeEquipment').html('<option value="" selected="selected">Tất cả</option>');
				// $('#groupEquipment').html('<option value="" selected="selected">Tất cả</option>').change();
				// $('#providerId').html('<option value="" selected="selected">Tất cả</option>');
				$('#yearManufacturing').val('').change();
				$('#stockCode').val('').change();
				$('#equipmentGridDialog').datagrid('loadData',[]);
			}
		});
	},
	/**
	 * Lay danh muc loai, ncc thiet bi
	 * @author nhutnn
	 * @since 20/12/2014
	 */
	getEquipCatalog: function(){
		$('#typeEquipment').html('<option value="" selected="selected">Tất cả</option>');
    	$('#provider').html('<option value=""  selected="selected">Tất cả</option>');
    	var params = new Object();
    	var equipLendCode = $("#equipLendCode").val().trim();
		if(equipLendCode != undefined && equipLendCode != null && equipLendCode != '') {
			params.equipLendCode = equipLendCode;
		}
		params.customerId = $("#customerIdHidden").val();
    	Utils.getJSONDataByAjaxNotOverlay(params, '/equipment-manage-delivery/get-equipment-catalog', function(result){	
		//$.getJSON('/equipment-manage-delivery/get-equipment-catalog', function(result) {				
			if(result.equipsCategory != undefined && result.equipsCategory != null && result.equipsCategory.length > 0) {					
				for(var i=0; i < result.equipsCategory.length; i++){
					$('#typeEquipment').append('<option value="'+ result.equipsCategory[i].id +'">'+ Utils.XSSEncode(result.equipsCategory[i].code) +' - '+ Utils.XSSEncode(result.equipsCategory[i].name) +'</option>');  
				}
			} 
			$('#typeEquipment').change();

			// if(result.equipProviders != undefined && result.equipProviders != null && result.equipProviders.length > 0) {					
			// 	for(var i=0; i < result.equipProviders.length; i++){
			// 		$('#providerId').append('<option value="'+ result.equipProviders[i].id +'">'+ Utils.XSSEncode(result.equipProviders[i].code)+' - '+ Utils.XSSEncode(result.equipProviders[i].name) +'</option>');  
			// 	}
			// } 
			// $('#providerId').change();	
					
			if(result.equipProviders != undefined && result.equipProviders != null && result.equipProviders.length > 0) {					
				for (var i = 0, isize = result.equipProviders.length; i < isize; i++) {
					result.equipProviders[i].searchText = unicodeToEnglish(result.equipProviders[i].code + " " + result.equipProviders[i].name);
 					result.equipProviders[i].searchText = result.equipProviders[i].searchText.toUpperCase();
				}
				var obj = {
					id: -1,
					code: "Tất cả",
					name: "Tất cả",
					codeName: "Tất cả",
					searchText: unicodeToEnglish("Tất cả").toUpperCase()
				};
				result.equipProviders.splice(0, 0, obj);
				$('#providerId').combobox("loadData", result.equipProviders);
		 		$('#providerId').combobox("setValue", -1);
			}
		},null,null);
	},
	// /**
	//  * Lay nhom thiet bi
	//  * @author nhutnn
	//  * @since 12/01/2015
	//  */
	// getEquipGroup: function(typeEquip){
	// 	var params = new Object();
	// 	if(typeEquip != undefined && typeEquip != null && typeEquip != ''){
	// 		params.id = typeEquip;
	// 	}
	// 	var equipLendCode = $("#equipLendCode").val().trim();
	// 	if(equipLendCode != undefined && equipLendCode != null && equipLendCode != '') {
	// 		params.equipLendCode = equipLendCode;
	// 	}
	// 	$('#groupEquipment').html('<option value="" selected="selected">Tất cả</option>');
 //    	Utils.getJSONDataByAjaxNotOverlay(params, '/equipment-manage-delivery/get-equipment-group', function(result){	
	// 	// $.getJSON('/equipment-manage-liquidation/get-equipment-catalog', function(result) {				
	// 		if(result.equipGroups != undefined && result.equipGroups != null && result.equipGroups.length > 0) {					
	// 			for(var i=0; i < result.equipGroups.length; i++){
	// 				$('#groupEquipment').append('<option value="'+ result.equipGroups[i].id +'">'+ Utils.XSSEncode(result.equipGroups[i].code+' - '+result.equipGroups[i].name) +'</option>');  
	// 			}
	// 		} 
	// 		$('#groupEquipment').change();				
	// 	},null,null);
	// },
		/**
	 * Lay nhom thiet bi autocompleteCombobox
	 * @author tamvnm
	 * @since 23/07/2015
	 */
	getEquipGroup: function(typeEquip){
		
		var params = new Object();
		if(typeEquip != undefined && typeEquip != null && typeEquip != ''){
			params.id = typeEquip;
		}
		var equipLendCode = $("#equipLendCode").val().trim();
		var customerId =  $("#customerIdHidden").val();
		if (equipLendCode != undefined && equipLendCode != null && equipLendCode != '') {
			params.equipLendCode = equipLendCode;
		}
		if (customerId != undefined && customerId != null) {
			params.customerId = customerId;
		}

    	Utils.getJSONDataByAjaxNotOverlay(params, '/equipment-manage-delivery/get-equipment-group', function(result){				
			if(result.equipGroups != undefined && result.equipGroups != null && result.equipGroups.length > 0) {	
				for (var i = 0, isize = result.equipGroups.length; i < isize; i++) {
					result.equipGroups[i].searchText = unicodeToEnglish(result.equipGroups[i].code + " " + result.equipGroups[i].name);
	 				result.equipGroups[i].searchText = result.equipGroups[i].searchText.toUpperCase();
				}				
				var obj = {
				id: -1,
				code: "Tất cả",
				name: "Tất cả",
				codeName: "Tất cả",
				searchText: unicodeToEnglish("Tất cả").toUpperCase()
				};
				result.equipGroups.splice(0, 0, obj);
				$('#groupEquipment').combobox("loadData", result.equipGroups);
		 		$('#groupEquipment').combobox("setValue", -1);
			} 
			
		},null,null);
	},
	/**
	 * Xu ly trên input loai thiet bi
	 * @author nhutnn
	 * @since 19/12/2014
	 */
	onChangeTypeEquip: function(cb){
		var typeEquip = $(cb).val().trim();
		EquipmentManagerDelivery.getEquipGroup(typeEquip);
		// $('#groupEquipment').html('<option value="" selected="selected">Tất cả</option>');
		// if(typeEquip != undefined && typeEquip != null && typeEquip != ''){
			// $.getJSON('/equipment-manage-delivery/get-equipment-group?id='+typeEquip, function(result) {				
			// 	if(result.equipGroups != undefined && result.equipGroups != null && result.equipGroups.length > 0) {					
			// 		for(var i=0; i < result.equipGroups.length; i++){
			// 			$('#groupEquipment').append('<option value="'+ result.equipGroups[i].id +'">'+ Utils.XSSEncode(result.equipGroups[i].name) +'</option>');  
			// 		}
			// 	} 
			// 	$('#groupEquipment').change();				
			// });			
		// }
	},

	/**
	 * Tao su kien nut Noi Dung
	 * @author tamvnm
	 * @since 25/06/2015
	 */
	getEventContent: function() {
		$('#contentDelivery').change(function() {
			var seri = $('#seriNumberInGrid' + EquipmentManagerDelivery._editIndex).val();
			var depreciation = $('#depreciationInGrid' + EquipmentManagerDelivery._editIndex).val();
		  	if (this.value != 'DELIVERY_CONTENT_NEW') {
		  		if (EquipmentManagerDelivery._shopToCode == "") {
		  			$('#errMsg').html('Bạn chưa chọn Mã NPP bên mượn').show();
		  			$('#shopToCode').focus();
		  		}
			    disableCombo('groupEquipCBX' + EquipmentManagerDelivery._editIndex);
			    disableCombo('providerEquipCBX' + EquipmentManagerDelivery._editIndex);
			    disableCombo('stockEquipCBX' + EquipmentManagerDelivery._editIndex);
			    disabled('price' + EquipmentManagerDelivery._editIndex);
	      		disabled('healthStatus' + EquipmentManagerDelivery._editIndex);
	      		disabled('yearManufacture' + EquipmentManagerDelivery._editIndex);
	      		enable('equipmentCodeInGrid' + EquipmentManagerDelivery._editIndex);
			} else {
			  	$('#gridEquipmentDelivery').datagrid('deleteRow',EquipmentManagerDelivery._editIndex);
			  	EquipmentManagerDelivery.addEquipmentInRecord();
			    $('#contentDelivery').val(this.value);
			    enableCombo('groupEquipCBX' + EquipmentManagerDelivery._editIndex);
			    enableCombo('providerEquipCBX' + EquipmentManagerDelivery._editIndex);
			    enableCombo('stockEquipCBX' + EquipmentManagerDelivery._editIndex);
			    enable('price' + EquipmentManagerDelivery._editIndex);
	            enable('healthStatus' + EquipmentManagerDelivery._editIndex);
	            enable('yearManufacture' + EquipmentManagerDelivery._editIndex);
	            disabled('equipmentCodeInGrid' + EquipmentManagerDelivery._editIndex);
			}  
			if (seri != undefined) {
				$('#seriNumberInGrid' + EquipmentManagerDelivery._editIndex).val(seri);
			}
			if (depreciation != undefined) {
				$('#depreciationInGrid' + EquipmentManagerDelivery._editIndex).val(depreciation);
			}
		});
		
		if ($('#price'+ EquipmentManagerDelivery._editIndex).val() != undefined && $('#price'+ EquipmentManagerDelivery._editIndex).val().indexOf(',')==-1) {
			$('#price'+ EquipmentManagerDelivery._editIndex).val(formatCurrency($('#price'+ EquipmentManagerDelivery._editIndex).val()));
			Utils.formatCurrencyFor('price'+ EquipmentManagerDelivery._editIndex);
			Utils.bindFormatOnTextfield('price'+ EquipmentManagerDelivery._editIndex,Utils._TF_NUMBER_COMMA);
		}
	},
	/**
	 * Khoi tao lai giao dien
	 * @author tamvnm
	 * @since 25/06/2015
	 */
	setUIEquipInRecord: function () {
		Utils.bindComboboxEquipGroupEasyUI('groupEquipCBX' + EquipmentManagerDelivery._editIndex);
		EquipmentManagerDelivery.setEquipGroup();
		Utils.bindComboboxEquipGroupEasyUI('providerEquipCBX' + EquipmentManagerDelivery._editIndex);
		EquipmentManagerDelivery.setEquipProvider();
		Utils.bindComboboxEquipGroupEasyUI('stockEquipCBX' + EquipmentManagerDelivery._editIndex);
		EquipmentManagerDelivery.setEquipStock();
		disableCombo('groupEquipCBX' + EquipmentManagerDelivery._editIndex);
		disableCombo('providerEquipCBX' + EquipmentManagerDelivery._editIndex);
		disableCombo('stockEquipCBX' + EquipmentManagerDelivery._editIndex);

		if ($('#price'+ EquipmentManagerDelivery._editIndex).val().indexOf(',') == -1) {
			$('#price'+ EquipmentManagerDelivery._editIndex).val(formatCurrency($('#price'+ EquipmentManagerDelivery._editIndex).val()));
			Utils.formatCurrencyFor('price'+ EquipmentManagerDelivery._editIndex);
			Utils.bindFormatOnTextfield('price'+ EquipmentManagerDelivery._editIndex,Utils._TF_NUMBER_COMMA);
		}
		disabled('price' + EquipmentManagerDelivery._editIndex);
		disabled('healthStatus' + EquipmentManagerDelivery._editIndex);
	},
	/**
	 * Chon thiet bi
	 * @author nhutnn
	 * @since 12/12/2014
	 */
	selectEquipment: function(index){
		var rowPopup = $('#equipmentGridDialog').datagrid('getRows')[index];
		var tempDep = null;
		var temContent = $('#contentDelivery').val();
		rowPopup.contentDelivery = null;

		if ($('#depreciationInGrid' + EquipmentManagerDelivery._editIndex) != undefined && $('#depreciationInGrid' + EquipmentManagerDelivery._editIndex).val() != '') {
			tempDep = $('#depreciationInGrid' + EquipmentManagerDelivery._editIndex).val();
		}
		
		if(EquipmentManagerDelivery._editIndex!=null){
			$('#gridEquipmentDelivery').datagrid('updateRow',{
				index: EquipmentManagerDelivery._editIndex,	// index start with 0
				row: rowPopup
			});
			$('#contentDelivery').width($('#contentDelivery').parent().width());
			$('#equipmentCodeInGrid' + EquipmentManagerDelivery._editIndex).width($('#equipmentCodeInGrid' + EquipmentManagerDelivery._editIndex).parent().width());
			$('#seriNumberInGrid').width($('#seriNumberInGrid').parent().width());
			$('#stock').width($('#stock').parent().width());
			if ($('#depreciationInGrid' + EquipmentManagerDelivery._editIndex) != undefined && tempDep != null) {
				$('#depreciationInGrid' + EquipmentManagerDelivery._editIndex).val(tempDep);
			}
			EquipmentManagerDelivery.editInputInGrid();		
			EquipmentManagerDelivery.getEventContent();
			$('#contentDelivery').val(temContent);
			// $('#contentDelivery').change(function() {
			// 	  if (this.value != 'DELIVERY_CONTENT_NEW') {
			// 	      $('#gridEquipmentDelivery').datagrid('deleteRow',EquipmentManagerDelivery._editIndex);
			// 	  	  EquipmentManagerDelivery.addEquipmentInRecord();
			// 	      $('#contentDelivery').val(this.value);
			// 	      disableCombo('groupEquipCBX' + EquipmentManagerDelivery._editIndex);
			// 	      disableCombo('providerEquipCBX' + EquipmentManagerDelivery._editIndex);
			// 	      console.log('khong phai cap moi');
			// 	      disabled('price' + EquipmentManagerDelivery._editIndex);
		 //      		  disabled('healthStatus' + EquipmentManagerDelivery._editIndex);
		 //      		  enable('equipmentCodeInGrid' + EquipmentManagerDelivery._editIndex);
			// 	  } else {
			// 	  	$('#gridEquipmentDelivery').datagrid('deleteRow',EquipmentManagerDelivery._editIndex);
			// 	  	EquipmentManagerDelivery.addEquipmentInRecord();
			// 	    $('#contentDelivery').val(this.value);
			// 	    enableCombo('groupEquipCBX' + EquipmentManagerDelivery._editIndex);
			// 	    enableCombo('providerEquipCBX' + EquipmentManagerDelivery._editIndex);
			// 	    console.log('cap moi');
			// 	    enable('price' + EquipmentManagerDelivery._editIndex);
		 //            enable('healthStatus' + EquipmentManagerDelivery._editIndex);
		 //            disabled('equipmentCodeInGrid' + EquipmentManagerDelivery._editIndex);
			// 	  }  
			// });
		}
		
		$('#easyuiPopupSearchEquipment').dialog('close');
	},
	/**
	 * Tim kiem thiet bi
	 * @author nhutnn
	 * @since 12/12/2014
	 */
	searchEquipment: function(){
		var params=new Object(); 
		params.equipCode = $('#equipmentCode').val().trim();
		params.seriNumber = $('#seriNumber').val().trim();
		params.categoryId = $('#typeEquipment').val().trim();
		params.customerId = $('#customerIdHidden').val();

		//tamvnm: thay doi thanh autoCompleteCombobox: -1 la truong hop tim group ko co trong BD
		// params.groupId = $('#groupEquipment').val().trim();
		if ($('#groupEquipment').combobox('getValue') == "") {
			$('#groupEquipment').combobox('setValue', -1);
			params.groupId = "";
		} else if ($('#groupEquipment').combobox('getValue') == -1) {
			params.groupId = "";
		} else {
			if (Utils.getMessageOfInvaildNumberNew($('#groupEquipment').combobox("getValue")).length == 0) {
				params.groupId = $('#groupEquipment').combobox("getValue");
			} else {
				params.groupId = -1;
			}
		}
		// params.providerId = $('#providerId').val().trim();
		if ($('#providerId').combobox('getValue') == "") {
			$('#providerId').combobox('setValue', -1);
			params.providerId = "";
		} else if ($('#providerId').combobox('getValue') == -1) {
			params.providerId = "";
		} else {
			if (Utils.getMessageOfInvaildNumberNew($('#providerId').combobox("getValue")).length == 0) {
				params.providerId = $('#providerId').combobox("getValue");
			} else {
				params.providerId = -1;
			}
		}
		
		params.yearManufacture = $('#yearManufacturing').val().trim();
		params.stockCode = $('#stockCode').val().trim();
		params.equipLendCode = $("#equipLendCode").val().trim();
		var rowEquip = $('#gridEquipmentDelivery').datagrid('getRows');
    	var lstEquipmentCode = '';
    	for (var i = 0; i < rowEquip.length; i++) {
    		// lstEquipmentCode.push(rowEquip[i].equipmentCode);
    		if (rowEquip[i].equipmentCode != null) {
        		if (i < rowEquip.length - 1) {
        			lstEquipmentCode += rowEquip[i].equipmentCode + ', ';
        		} else {
        			lstEquipmentCode += rowEquip[i].equipmentCode;
        		}
    		}
    	}
    	params.lstEquipmentCode = lstEquipmentCode;
		if($("#idRecordHidden").val() != "" && EquipmentManagerDelivery._lstEquipInRecord != null){
			params.lstEquipDelete = "";
			for(var i=0; i<EquipmentManagerDelivery._lstEquipInRecord.length; i++){
				params.lstEquipDelete += EquipmentManagerDelivery._lstEquipInRecord[i]+',';
			}					
		}
		if(EquipmentManagerDelivery._lstEquipInsert != null){
			params.lstEquipAdd = "";
			for(var i=0; i<EquipmentManagerDelivery._lstEquipInsert.length; i++){
				params.lstEquipAdd += EquipmentManagerDelivery._lstEquipInsert[i]+',';
			}					
		}
		$('#equipmentGridDialog').datagrid('load',params);
	},

	/**
	 * Show popup chon kho
	 * @author nhutnn
	 * @since 12/12/2014
	 */
	showPopupStock:function(txtInput){
	// $('#easyuiPopupSearchStock').show();
	// 	$('#easyuiPopupSearchStock').dialog({  
	//         closed: false,  
	//         cache: false,  
	//         modal: true,
	//         width : 600,
	//         height : 'auto',
	//         onOpen: function(){	
	// 			$('#shopCodePopupStock').focus();
	// 			$('#shopCodePopupStock, #shopNamePopupStock').bind('keyup',function(event){
	// 				if(event.keyCode == keyCodes.ENTER){
	// 					$('#btnSearchStockDlg').click(); 
	// 				}
	// 			});	
	// 			$('#stockGridDialog').datagrid({
	// 				url : '/commons/search-shop-show-list-NPP',
	// 				autoRowHeight : true,
	// 				rownumbers : true, 
	// 				fitColumns:true,
	// 				scrollbarSize:0,
	// 				pagination:true,
	// 				pageSize: 10,
	// 				pageList: [10],
	// 				width: $('#stockGridDialogContainer').width(),
	// 				height: 'auto',
	// 				autoWidth: true,	
	// 				queryParams:{
	// 					status : 1,
	// 					isUnionShopRoot: true,
	// 					code: $('#shopCodePopupStock').val().trim(),
	// 					name: $('#shopNamePopupStock').val().trim()
	// 				},
	// 				columns:[[
	// 					{field:'shopCode', title:'Mã Đơn vị', align:'left', width: 120, sortable:false, resizable:false},
	// 					{field:'shopName', title:'Tên Đơn vị', align:'left', width: 200, sortable:false, resizable:false},
	// 					{field:'choose', title:'', align:'center', width:40, sortable:false, resizable:false, formatter:function(value,row,index) {
	// 						var html =  "<a href='javascript:void(0)' onclick=\"return EquipmentManagerDelivery.selectStock('"+ row.shopCode + "');\">Chọn</a>";
	// 						return html;
	// 					}}				
	// 				]],
	// 				onBeforeLoad:function(param){											 
	// 				},
	// 				onLoadSuccess :function(data){   			 
	// 					if (data == null || data.total == 0) {
	// 						//$("#errMsgInfo").html("Không tìm thấy danh sách biên bản giao nhận thiết bị!").show();
	// 					}else{
	// 						$('.datagrid-header-rownumber').html('STT');		    	
	// 					}	    		
	// 				}
	// 			});				
	//         },
	// 		onClose: function(){
	// 			$('#easyuiPopupSearchStock').hide();
	// 			$('#shopCodePopupStock').val('').change();
	// 			$('#shopNamePopupStock').val('').change();
	// 		}
	// 	});		
	// 	return false;
		var txt = "stockInGrid";
		if(txtInput!=undefined){
			txt = txtInput;
		}
		$('#common-dialog-search-2-textbox').dialog("close");
		$('#common-dialog-search-2-textbox').remove();
		VCommonJS.showDialogSearch2({
			inputs : [
		        {id:'code', maxlength:50, label:'Mã đơn vị'},
		        {id:'name', maxlength:250, label:'Tên đơn vị'},
		    ],
		    url : '/commons/get-equipment-stock',
		    //isCenter : true,
		    columns : [[
		        {field:'codeName', title:'Đơn vị', align:'left', width: 110, sortable:false, resizable:false, formatter:function(value,row,index) {
		        	var nameCode = row.shopCode + '-' + row.shopName;
		        	return Utils.XSSEncode(nameCode);         
		        }},
		        {field:'equipStockCode', title:'Mã kho', align:'left', width: 200, sortable:false, resizable:false},
		        {field:'choose', title:'', align:'center', width:40, sortable:false, resizable:false, formatter:function(value,row,index) {
//			        	var nameCode = row.shopCode + '-' + row.name;
		            return '<a href="javascript:void(0)" onclick="Utils.fillCodeForInputTextByDialogSearch2(\''+txt+'\', ' + row.equipStockId +', \''+row.equipStockCode+'\', \''+row.name+'\','+Utils._isShowInputTextByDlSearch2.code+');">chọn</a>';         
		        }}
		    ]]
		});
	},
	/**
	 * Chon kho
	 * @author nhutnn
	 * @since 12/12/2014
	 */
	selectStock: function(shopCode){
		$('#stockCode').val(shopCode);		
		$('#easyuiPopupSearchStock').dialog('close');
	},
	/**
	 * Tim kiem kho
	 * @author nhutnn
	 * @since 12/12/2014
	 */
	searchStock: function(){
		var params=new Object(); 
		params.status=1;
		params.isUnionShopRoot = true;
		params.code = $('#shopCodePopupStock').val().trim();
		params.name = $('#shopNamePopupStock').val().trim();
		
		$('#stockGridDialog').datagrid('load',params);
	},
	/**
	 * Show popup ma va so seri thiet bi
	 * @author nhutnn
	 * @since 12/12/2014
	 */
	showPopupSelectEquipment:function(dataGrid){
		$('#easyuiPopupSelectEquipment').show();
		$('#easyuiPopupSelectEquipment').dialog({  
	        closed: false,  
	        cache: false,  
	        modal: true,
	        width : 450,
	        height : 'auto',
	        onOpen: function(){	        	
				$('#gridSelectEquipment').datagrid({
					//url : '/equipment-manage-delivery/search-seri-equipment',
					data: dataGrid,
					autoRowHeight : true,
					rownumbers:true,
					fitColumns:true,
					scrollbarSize:0,					
					width: $('#gridSelectEquipmentContainer').width(),
					autoWidth: true,
					columns:[[
					    {field: 'equipmentCode',title:'Mã thiết bị',width: 250,sortable:false,resizable:false, align: 'left' },	
					    {field: 'seriNumber',title:'Số serial',width: 100,sortable:false,resizable:false, align: 'left' },	
					    {field: 'select',title:'',width: 50,sortable:false,resizable:false, align: 'center', formatter: function(value, row, index) {
					    	return '<a href="javascript:void(0);" onclick="EquipmentManagerDelivery.selectEquipmentCode('+index+');">Chọn</a>';
						}}			    
					]],
					onBeforeLoad:function(param){											 
					},
					onLoadSuccess :function(data){   			 
						$('.easyui-dialog .datagrid-header-rownumber').html('STT');				
					}
				});
	        },
	        onClose: function(){
	        	$('#easyuiPopupSelectEquipment').hide();
	        }
		});
	},
	/**
	 * Chon ma, so seri thiet bi
	 * @author nhutnn
	 * @since 12/12/2014
	 */
	selectEquipmentCode: function(index){
		var rowPopup = $('#gridSelectEquipment').datagrid('getRows')[index];
		rowPopup.contentDelivery = null;
		var tempDep = null;
		if ($('#depreciationInGrid' + EquipmentManagerDelivery._editIndex) != undefined && $('#depreciationInGrid' + EquipmentManagerDelivery._editIndex).val() != '') {
			tempDep = $('#depreciationInGrid' + EquipmentManagerDelivery._editIndex).val();
		}
		if(EquipmentManagerDelivery._editIndex!=null){
			$('#gridEquipmentDelivery').datagrid('updateRow',{
				index: EquipmentManagerDelivery._editIndex,	// index start with 0
				row: rowPopup
			});
			$('#contentDelivery').width($('#contentDelivery').parent().width());
			$('#equipmentCodeInGrid' + EquipmentManagerDelivery._editIndex).width($('#equipmentCodeInGrid' + EquipmentManagerDelivery._editIndex).parent().width());
			$('#seriNumberInGrid').width($('#seriNumberInGrid').parent().width());
			$('#stock').width($('#stock').parent().width());
			if ($('#depreciationInGrid' + EquipmentManagerDelivery._editIndex) != undefined && tempDep != null) {
				$('#depreciationInGrid' + EquipmentManagerDelivery._editIndex).val(tempDep);
			}
			EquipmentManagerDelivery.editInputInGrid();
		}
		$('#easyuiPopupSelectEquipment').dialog('close');
	},
	/**
	 * Tao moi bien ban
	 * @author nhutnn
	 * @since 18/12/2014
	 */
	// createRecordDelivery: function(){
	// 	$('#errMsgInfo').html('').hide();
	// 	var msg ='';
	// 	var err = '';
	// 	var nContractTrim = $('#numberContract').val().trim();
	// 	$('#numberContract').val(nContractTrim);
	// 	if(msg.length == 0 && $('#numberContract').val().indexOf(' ')!=-1){
	// 		msg = 'Số hợp đồng không được có khoảng trắng';
	// 		err = '#numberContract';
	// 	}
	// 	if(msg.length == 0){
	// 		msg = Utils.getMessageOfSpecialCharactersValidate('numberContract','Số hợp đồng',Utils._NAME_CUSTYPE);
	// 		err = '#numberContract';
	// 	}
	// 	if(msg.length == 0){
	// 		msg = Utils.getMessageOfRequireCheck('numberContract','Số hợp đồng');
	// 		err = '#numberContract';
	// 	}	
	// 	if(msg.length == 0){
	// 		msg = Utils.getMessageOfRequireCheck('contractDate','Ngày hợp đồng');
	// 		err = '#contractDate';
	// 	}				
	// 	var contractDate = $('#contractDate').val().trim();
	// 	if(msg.length == 0 && contractDate != '' && !Utils.isDate(contractDate)){
	// 		msg = 'Ngày hợp đồng không tồn tại hoặc không đúng định dạng dd/mm/yyyy.';
	// 		err = '#contractDate';
	// 	}	

	// 	if ($('#shopFromCode').val() == '' && msg.length == 0) {
	// 		msg = 'Bạn chưa nhập giá trị cho trường "Đơn vị" BÊN CHO MƯỢN';
	// 		err = '#shopFromCode';
	// 	}
	// 	if ($('#address').val() == '' && msg.length == 0) {
	// 		msg = 'Bạn chưa nhập giá trị cho trường "Địa chỉ" BÊN CHO MƯỢN';
	// 		err = '#address';
	// 	}
	// 	if ($('#taxCode').val() == '' && msg.length == 0) {
	// 		msg = 'Bạn chưa nhập giá trị cho trường "Mã số thuế" BÊN CHO MƯỢN';
	// 		err = '#taxCode';
	// 	}
	// 	if ($('#numberPhone').val() == '' && msg.length == 0) {
	// 		msg = 'Bạn chưa nhập giá trị cho trường "Điện thoại" BÊN CHO MƯỢN';
	// 		err = '#numberPhone';
	// 	}
	// 	if ($('#fromRepresentative').val() == '' && msg.length == 0) {
	// 		msg = 'Bạn chưa nhập giá trị cho trường "Người đại diện" BÊN CHO MƯỢN';
	// 		err = '#fromRepresentative';
	// 	}
	// 	if ($('#fromPosition').val() == '' && msg.length == 0) {
	// 		msg = 'Bạn chưa nhập giá trị cho trường "Chức vụ" BÊN CHO MƯỢN';
	// 		err = '#fromPosition';
	// 	}
	// 	if ($('#fromPage').val() == '' && msg.length == 0) {
	// 		msg = 'Bạn chưa nhập giá trị cho trường "Giấy ủy quyền" BÊN CHO MƯỢN';
	// 		err = '#fromPage';
	// 	}
	// 	if ($('#fromPagePlace').val() == '' && msg.length == 0) {
	// 		msg = 'Bạn chưa nhập giá trị cho trường "Nơi cấp giấy quỷ quyền" BÊN CHO MƯỢN';
	// 		err = '#fromPagePlace';
	// 	}
	// 	var fromPDate = $('#pageDate').val().trim();
	// 	if(msg.length == 0 && fromPDate != '' && !Utils.isDate(fromPDate)){
	// 		msg = 'Ngày cấp GPKD không tồn tại hoặc không đúng định dạng dd/mm/yyyy.';
	// 		err = '#pageDate';
	// 	}	
	// 	if(msg.length == 0){
	// 		msg = Utils.getMessageOfRequireCheck('pageDate','Ngày cấp giấy ủy quyền" BÊN CHO MƯỢN');
	// 		err = '#pageDate';
	// 	}

	// 	if(msg.length == 0){
	// 		msg = Utils.getMessageOfSpecialCharactersValidate('shopToCode','Mã đơn vị BÊN MƯỢN',Utils._CODE);
	// 		err = '#shopToCode';
	// 	}
	// 	if(msg.length == 0){
	// 		msg = Utils.getMessageOfRequireCheck('shopToCode','Mã đơn vị BÊN MƯỢN');
	// 		err = '#shopToCode';
	// 	}	
	// 	if(msg.length == 0){
	// 		msg = Utils.getMessageOfRequireCheck('customerCode','Mã khách hàng');
	// 		err = '#customerCode';
	// 	}	
	// 	if(msg.length == 0){
	// 		msg = Utils.getMessageOfSpecialCharactersValidate('customerCode','Mã khách hàng',Utils._CODE);
	// 		err = '#customerCode';
	// 	}
	// 	if ($('#toObjectAddress').val() == '' && msg.length == 0) {
	// 		msg = 'Bạn chưa nhập giá trị cho trường "Địa chỉ đặt tủ" BÊN MƯỢN';
	// 		err = '#toObjectAddress';
	// 	}
	// 	// if ($('#toBusinessLicense').val() == '' && msg.length == 0) {
	// 	// 	msg = 'Bạn chưa nhập giá trị cho trường "Số GP ĐKKD" BÊN MƯỢN';
	// 	// 	err = '#toBusinessLicense';
	// 	// }
	// 	// if ($('#toBusinessPlace').val() == '' && msg.length == 0) {
	// 	// 	msg = 'Bạn chưa nhập giá trị cho trường "Nơi cấp GP ĐKKD BÊN MƯỢN';
	// 	// 	err = '#toBusinessPlace';
	// 	// }
	// 	var toBusinessDate = $('#toBusinessDate').val().trim();
	// 	if(msg.length == 0 && toBusinessDate != '' && !Utils.isDate(toBusinessDate)){
	// 		msg = 'Ngày cấp GPKD không tồn tại hoặc không đúng định dạng dd/mm/yyyy.';
	// 		err = '#toBusinessDate';
	// 	}	
	// 	// if(msg.length == 0){
	// 	// 	msg = Utils.getMessageOfRequireCheck('toBusinessDate','Ngày cấp GPKD');
	// 	// 	err = '#toBusinessDate';
	// 	// }
	// 	if ($('#toRepresentative').val() == '' && msg.length == 0) {
	// 		msg = 'Bạn chưa nhập giá trị cho trường "Người đại diện" BÊN MƯỢN';
	// 		err = '#toRepresentative';
	// 	}
	// 	if ($('#toPosition').val() == '' && msg.length == 0) {
	// 		msg = 'Bạn chưa nhập giá trị cho trường "Chức vụ người đại diện" BÊN MƯỢN';
	// 		err = '#toPosition';
	// 	}
	// 	if ($('#staffMonitorCode').combobox('getValue') == '' && msg.length == 0) {
	// 		msg = 'Bạn chưa nhập giá trị cho trường "Mã giám sát" BÊN MƯỢN';
	// 		err = '#staffMonitorCode';
	// 	}

	// 	// var isStaff = false;
	// 	// var shopToCode = $('#shopToCode').val().trim();
	// 	// if(shopToCode != undefined && shopToCode != null && shopToCode != ''){
	// 	// 	 $.getJSON('/equipment-manage-delivery/getListGSNPP?toShopCode='+shopToCode, function(result) {
	// 	// 		 if(result.rows != undefined && result.rows != null && result.rows.length > 0) {
	// 	// 		 	for(var i = 0, sz = result.rows.length; i < sz; i++) {
	// 	// 		 		if ($('#staffMonitorCode').combobox('getValue').trim() == result.rows[i].staffCode) {
	// 	// 					isStaff = true;
	// 	// 				}
	// 	// 		 	}
	// 	// 		 }

	// 	// 	});
	// 	// }
	// 	// if (!isStaff) {
	// 	// 	msg = 'Mã giám sát không quản lý "Đơn vị" BÊN MƯỢN';
	// 	// 	err = '#staffMonitorCode';
	// 	// }

	// 	var freezer = Utils.returnMoneyValue($('#freezer').val().trim());
	// 	var refrigerator = Utils.returnMoneyValue($("#refrigerator").val().trim());
	// 	var intFreezer = parseInt(freezer);
	// 	var intRefrigerator = parseInt(refrigerator);
	// 	if (msg.length == 0) {
	// 		if(freezer != '') {
	// 			if (intFreezer != freezer) {
	// 				msg = 'Tủ mát phải là số nguyên dương có tối đa 17 chữ số';
	// 				err = '#freezer';
	// 			} else if (intFreezer < 0  || intFreezer > 99999999999999999) {
	// 				msg = 'Tủ mát phải là số nguyên dương có tối đa 17 chữ số';
	// 				err = '#freezer';
	// 			}
	// 		} else {
	// 			msg = 'Tủ mát phải là số nguyên dương có tối đa 17 chữ số';
	// 			err = '#freezer';
	// 		}
	// 	}
	// 	if (msg.length == 0) {
	// 		if (refrigerator != '') {
	// 			if (intRefrigerator != refrigerator) {
	// 				msg = 'Tủ đông phải là số nguyên dương có tối đa 17 chữ số';
	// 				err = '#refrigerator';
	// 			} else if (intRefrigerator < 0  || intRefrigerator > 99999999999999999) {
	// 				msg = 'Tủ đông phải là số nguyên dương có tối đa 17 chữ số';
	// 				err = '#refrigerator';
	// 			}
	// 		} else {
	// 			msg = 'Tủ đông phải là số nguyên dương có tối đa 17 chữ số';
	// 				err = '#refrigerator';
	// 		}
	// 	}


	// 	if(msg.length > 0){
	// 		$('#errMsgInfo').html(msg).show();
	// 		$(err).focus();
	// 		return false;
	// 	}
		
	// 	var rowEquip = $('#gridEquipmentDelivery').datagrid('getRows');
	// 	if(rowEquip.length == 0){
	// 		$('#errMsgInfo').html('Không có thiết bị để cập nhật').show();
	// 		return false;
	// 	}
	// 	//var lstIdEquip = new Array();
	// 	var lstEquipCode = new Array();
	// 	var lstSeriNumber = new Array();
	// 	var lstContentDelivery = new Array();
	// 	var lstStockCode = new Array();
	// 	var lstDepreciation = new Array();
	// 	for (var i = 0; i < rowEquip.length; i++) {
	// 		if (rowEquip[i].seriNumber == null || rowEquip[i].depreciation == null) {
	// 			if ((rowEquip[i].equipmentId != null && rowEquip[i].equipmentCode == null) || rowEquip[i].equipmentId == undefined) {
	// 				lstEquipCode.push($('#equipmentCodeInGrid' + EquipmentManagerDelivery._editIndex).val());
	// 			} else {
	// 				lstEquipCode.push(rowEquip[i].equipmentCode);
	// 			}
	// 			if (rowEquip[i].contentDelivery != null) {
	// 				lstContentDelivery.push(rowEquip[i].contentDelivery);
	// 			} else if ($('#contentDelivery').val() != undefined) {
	// 				lstContentDelivery.push($('#contentDelivery').val());
	// 			}
	// 			if (rowEquip[i].seriNumber != null) {
	// 				lstSeriNumber.push(rowEquip[i].seriNumber);
	// 			} else {
	// 				if ($('#seriNumberInGrid' + i).val() != undefined && $('#seriNumberInGrid' + i).val() != null) {
	// 					lstSeriNumber.push($('#seriNumberInGrid' + i).val().trim());	
	// 				} else {
	// 					lstSeriNumber.push($('#seriNumberInGrid' + i).val());
	// 				}
	// 			}
	// 			if (rowEquip[i].depreciation != null) {
	// 				lstDepreciation.push(rowEquip[i].depreciation);
	// 			} else {
	// 				var depreciation = null;
	// 				if ($('#depreciationInGrid' + i).val() != undefined && $('#depreciationInGrid' + i).val() != null) {
	// 					depreciation = $('#depreciationInGrid' + i).val().trim();
	// 				} else {
	// 					depreciation = $('#depreciationInGrid' + i).val();
	// 				}
					 
	// 				var intDepreciation = parseInt(depreciation);
	// 				if ((depreciation != null && depreciation != intDepreciation) || intDepreciation < 0) {
	// 					msg = 'Vui lòng nhập Số tháng khấu hao là số nguyên dương';
	// 					err = '#depreciationInGrid' + i;
	// 					$('#errMsgInfo').html(msg).show();
	// 					$(err).focus();
	// 					return false;
	// 				} else {
	// 					if ($('#depreciationInGrid' + i).val() != undefined && $('#depreciationInGrid' + i).val() != null) {
	// 						lstDepreciation.push($('#depreciationInGrid' + i).val().trim());
	// 					} else {
	// 						lstDepreciation.push($('#depreciationInGrid' + i).val());
	// 					}
						
	// 				}
	// 			}				
	// 		} else if (i >= EquipmentManagerDelivery._numberEquipInRecord) {
	// 			if ((rowEquip[i].equipmentId != null && rowEquip[i].equipmentCode == null) || rowEquip[i].equipmentId == undefined) {
	// 				lstEquipCode.push($('#equipmentCodeInGrid' + EquipmentManagerDelivery._editIndex).val());
	// 			} else {
	// 				lstEquipCode.push(rowEquip[i].equipmentCode);
	// 			}
	// 			if (rowEquip[i].contentDelivery != null) {
	// 				lstContentDelivery.push(rowEquip[i].contentDelivery);
	// 			} else if ($('#contentDelivery').val() != undefined) {
	// 				lstContentDelivery.push($('#contentDelivery').val());
	// 			}
	// 			if (rowEquip[i].seriNumber == null) {
	// 				lstSeriNumber.push('');
	// 			} else {
	// 				lstSeriNumber.push(rowEquip[i].seriNumber);
	// 			}
				
	// 			lstDepreciation.push(rowEquip[i].depreciation);			
	// 		}	
	// 	}
		
	// 	// if ($('#equipmentCodeInGrid' + EquipmentManagerDelivery._editIndex).val() != undefined && $('#equipmentCodeInGrid' + EquipmentManagerDelivery._editIndex).val() != '') {
	// 	// 	var rowEq = $('#gridEquipmentDelivery').datagrid('getRows');
	// 	// 	for (var i=0; i < rowEq.length-1; i++){
	// 	// 		if (rowEq[i].equipmentCode == $('#equipmentCodeInGrid' + EquipmentManagerDelivery._editIndex).val().trim()) {
	// 	// 			$('#errMsgInfo').html("Mã thiết bị "+rowEq[i].equipmentCode+" đã tồn tại!").show();
	// 	// 			return false;
	// 	// 		}
	// 	// 	}
	// 	// 	lstEquipCode.push($('#equipmentCodeInGrid' + EquipmentManagerDelivery._editIndex).val());
	// 	// } else if ($('#equipmentCodeInGrid' + EquipmentManagerDelivery._editIndex).val() == '') {
	// 	// 	$('#errMsgInfo').html('Bạn chưa xử lý xong dòng thiết bị hiện tại!').show();
	// 	// 	return false;
	// 	// } else if (EquipmentManagerDelivery._numberEquipInRecord == null || 
	// 	// 	rowEquip.length-1 >= EquipmentManagerDelivery._numberEquipInRecord) {
	// 	// 		lstEquipCode.push(rowEquip[rowEquip.length-1].equipmentCode);
	// 	// }

	// 	// if ($('#contentDelivery').val() != undefined){
	// 	// 	lstContentDelivery.push($('#contentDelivery').val());
	// 	// } else {
	// 	// 	lstContentDelivery.push(rowEquip[rowEquip.length-1].contentDelivery);
	// 	// }
	// 	if ($('#stockInGrid').val() != undefined && $('#stock').val() != '') {
	// 		lstStockCode.push($('#stockInGrid').val());
	// 	} else {
	// 		lstStockCode.push(rowEquip[rowEquip.length-1].stockCode);
	// 	}
	// 	// if(rowEquip[rowEquip.length-1].equipmentId != undefined){
	// 	// 	lstIdEquip.push(rowEquip[rowEquip.length-1].equipmentId);
	// 	// }else{
	// 	// 	lstIdEquip.push(-1);
	// 	// }
		
	// 	var params=new Object(); 
	// 	params.shopFromCode = $('#shopFromCode').val().trim();
	// 	params.shopToCode = $('#shopToCode').val().trim();
	// 	params.customerCode = $('#customerCode').val().trim();
	// 	params.numberContract = $('#numberContract').val().trim();
	// 	params.contractDate = $('#contractDate').val().trim();
	// 	params.statusRecord = $('#status').val().trim();
	// 	params.statusDelivery = $('#statusDelivery').val().trim();
		
	// 	params.fromObjectAddress = $("#address").val().trim();
	// 	params.fromObjectTax  = $("#taxCode").val().trim();
	// 	params.fromObjectPhone  = $("#numberPhone").val().trim();
	// 	params.fromRepresentative = $("#fromRepresentative").val().trim();
	// 	params.fromObjectPosition = $("#fromPosition").val().trim();
	// 	params.fromPage = $("#fromPage").val().trim();
	// 	params.fromPagePlace = $("#fromPagePlace").val().trim();
	// 	params.fromPageDate = $("#pageDate").val().trim();
	// 	params.fromFax = $('#fromFax').val();
	// 	params.toObjectAddress = $("#toObjectAddress").val().trim();
	// 	params.toRepresentative = $("#toRepresentative").val().trim();
	// 	params.toObjectPosition = $("#toPosition").val().trim();
	// 	params.toIdNO = $("#toIdNO").val().trim();
	// 	params.toIdNODate = $("#idNODate").val().trim();
	// 	params.toIdNOPlace = $("#toIdNOPlace").val().trim();
	// 	params.freezer = Utils.returnMoneyValue($('#freezer').val().trim());
	// 	params.refrigerator = Utils.returnMoneyValue($("#refrigerator").val().trim());
	// 	params.fromShopCode = $("#shopFromCode").val().trim();
	// 	params.toShopCode = $("#shopToCode").val().trim();
	// 	params.staffCode = $('#staffMonitorCode').combobox('getValue').trim();
	// 	params.toBusinessLicense = $('#toBusinessLicense').val().trim();
	// 	params.toBusinessDate = $('#toBusinessDate').val().trim();
	// 	params.toBusinessPlace = $('#toBusinessPlace').val().trim();
		
	// 	// params.lstIdEquip = lstIdEquip;
	// 	if(lstEquipCode.length > 0){
	// 		params.lstEquipCode = lstEquipCode;
	// 		params.lstSeriNumber = lstSeriNumber;
	// 		params.lstContentDelivery = lstContentDelivery;	
	// 		params.lstDepreciation = lstDepreciation;
	// 	}	

	// 	if (params.statusRecord == 2 && lstSeriNumber.length > 0) {
	// 		for (var i = 0; i < lstSeriNumber.length; i++) {
	// 			if (lstSeriNumber[i] == null || lstSeriNumber[i] == '') {
	// 				$('#errMsgInfo').html("Vui lòng nhập đầy đủ thông tin seri khi chọn trạng thái Duyệt").show();
	// 				return false;
	// 			}
	// 		}
	// 	}
		
	// 	if ($('#equipmentCodeInGrid' + EquipmentManagerDelivery._editIndex).val() == '') {
	// 			$('#errMsgInfo').html('Bạn chưa nhập mã thiết bị').show();
	// 			$('#equipmentCodeInGrid' + EquipmentManagerDelivery._editIndex).focus();
	// 			return false;
	// 	}
	// 	if ($('#depreciationInGrid' + EquipmentManagerDelivery._editIndex).val() == '') {
	// 		$('#errMsgInfo').html('Bạn chưa nhập số tháng khấu hao').show();
	// 		$('#depreciationInGrid' + EquipmentManagerDelivery._editIndex).focus();
	// 		return false;
	// 	}
	// 	if ($('#stockInGrid').val() == '') {
	// 		$('#errMsgInfo').html('Bạn chưa xử lý xong dòng thiết bị thêm mới!').show();
	// 		return false;
	// 	}		
	// 	//upload file
	// 	var arrFile = DropzonePlugin._dropzoneObject.getFilesWithStatus(Dropzone.ADDED);
	// 	if (EquipmentManagerDelivery._arrFileDelete == undefined || EquipmentManagerDelivery._arrFileDelete == null) {
	// 		EquipmentManagerDelivery._arrFileDelete = [];
	// 	}
	// 	if (arrFile != null && arrFile != undefined && arrFile.length > 0) {
	// 		if (EquipmentManagerDelivery._countFile == 5) {
	// 			$('#errMsgInfo').html("Tổng số tập tin đính kèm không được vượt quá 5").show();
	// 			return false;
	// 		}
	// 		var maxLength = 5 - EquipmentManagerDelivery._countFile;
	// 		if(maxLength<0){
	// 			maxLength = 0;
	// 		}
	// 		if (arrFile.length > maxLength) {
	// 			$('#errMsgInfo').html("Số tập tin thêm mới không được vượt quá " + maxLength).show();
	// 			return false;
	// 		}
	// 		msg = UploadUtil.getMsgByFileRequireCheck(UploadUtil._typeFileRequireCheck.equipRecord);
	// 		if (msg.trim().length > 0) {
	// 			$('#errMsgInfo').html(msg).show();
	// 			return false;
	// 		}			
	// 	}
	// 	params.equipAttachFileStr= EquipmentManagerDelivery._arrFileDelete.join(',');
	// 	Utils.getJSONDataByAjaxNotOverlay({id : -1}, '/equipment-manage-delivery/check-equip-period', function(dt){						
	// 		if(dt.error){
	// 			$('#errMsgInfo').html(dt.errMsgInfo).show();
	// 			return false;
	// 		}else{
	// 			var isStaff = false;
	// 			var shopToCode = $('#shopToCode').val().trim();
	// 			if(shopToCode != undefined && shopToCode != null && shopToCode != ''){
	// 				 $.getJSON('/equipment-manage-delivery/getListGSNPP?toShopCode='+shopToCode, function(result) {
	// 					 if(result.rows != undefined && result.rows != null && result.rows.length > 0) {
	// 					 	for(var i = 0, sz = result.rows.length; i < sz; i++) {
	// 					 		if ($('#staffMonitorCode').combobox('getValue').trim() == result.rows[i].staffCode) {
	// 								isStaff = true;
	// 							}
	// 					 	}
	// 					 }
	// 					 if (result.rows.length == 0) {
	// 					 	msg = 'Không tìm thấy "Đơn vị" BÊN MƯỢN hoặc Mã giám sát không quản lý "Đơn vị" BÊN MƯỢN';
	// 						err = '#shopToCode';
	// 						$('#errMsgInfo').html(msg).show();
	// 						$(err).focus();
	// 						return false;
	// 					 } else if (!isStaff) {
	// 						msg = 'Mã giám sát không quản lý "Đơn vị" BÊN MƯỢN';
	// 						err = '#staffMonitorCode';
	// 						$('#errMsgInfo').html(msg).show();
	// 						$(err).focus();
	// 						return false;
	// 					} else {
	// 						$.messager.confirm('Xác nhận', 'Bạn có muốn lưu dữ liệu?', function(r){
	// 							if (r){
	// 								if (arrFile != null && arrFile != undefined && arrFile.length > 0) {
	// 									params = JSONUtil.getSimpleObject2(params);
	// 									UploadUtil.updateAdditionalDataForUpload(params);
	// 									UploadUtil.startUpload('errMsgInfo');
	// 									return false;
	// 								}					
	// 								Utils.saveData(params, '/equipment-manage-delivery/create-record-delivery', null, 'errMsgInfo', function(data){	
	// 									if(data.error){
	// 										$('#errMsgInfo').html(data.errMsgInfo).show();
	// 										return false;
	// 									}else{
	// 										$('#successMsgInfo').html("Cập nhật dữ liệu thành công.").show();	
	// 										EquipmentManagerDelivery._lstEquipInsert = null;
	// 										EquipmentManagerDelivery._lstStockInsert = null;
	// 										EquipmentManagerDelivery._editIndex = null;				
	// 										setTimeout(function(){
	// 											$('#successMsgInfo').html("").hide(); 
	// 											window.location.href = '/equipment-manage-delivery/info';						              		
	// 										}, 3000);
	// 									}
	// 								}, null, null, null, null);		
	// 							}
	// 						});
	// 					}
	// 				});
	// 			}
	// 		}
	// 	}, null, null);			
	// },
	/**
	 * Tao moi bien ban
	 * @author tamvnm
	 * @since 25/06/2015
	 */
	createRecordDelivery: function(){
		$('#errMsgInfo').html('').hide();
		var msg ='';
		var err = '';
		var nContractTrim = $('#numberContract').val().trim();
		$('#numberContract').val(nContractTrim);
		//if(msg.length == 0 && $('#numberContract').val().indexOf(' ')!=-1){
		//	msg = 'Số hợp đồng không được có khoảng trắng';
		//	err = '#numberContract';
		//}
		// if(msg.length == 0){
		// 	msg = Utils.getMessageOfSpecialCharactersValidate('numberContract','Số hợp đồng',Utils._NAME_CUSTYPE);
		// 	err = '#numberContract';
		// }
		var createDate = $('#createDate').val();
		if(msg.length == 0 && $('#createDate').val().trim().length > 0 && !Utils.isDate($('#createDate').val()) && $('#createDate').val() != '__/__/____' ){
			msg = 'Ngày biên bản không tồn tại hoặc không đúng định dạng dd/mm/yyyy.';
			err = '#createDate';
		}
		var curDate = ReportUtils.getCurrentDateString(); //Lay ngay hien tai sysDate
		if (msg.length == 0 && !Utils.compareDate($('#createDate').val().trim(), curDate)) {
			msg = 'Ngày biên bản không được sau ngày hiện tại. Vui lòng nhập lại';
			$('#createDate').focus();
		}

		if(msg.length == 0){
			msg = Utils.getMessageOfRequireCheck('numberContract','Số hợp đồng');
			err = '#numberContract';
		}	
		if(msg.length == 0){
			msg = Utils.getMessageOfRequireCheck('contractDate','Ngày hợp đồng');
			err = '#contractDate';
		}				
		var contractDate = $('#contractDate').val().trim();
		if(msg.length == 0 && contractDate != '' && !Utils.isDate(contractDate)){
			msg = 'Ngày hợp đồng không tồn tại hoặc không đúng định dạng dd/mm/yyyy.';
			err = '#contractDate';
		}	

		if ($('#shopFromCode').val() == '' && msg.length == 0) {
			msg = 'Bạn chưa nhập giá trị cho trường "Đơn vị" BÊN CHO MƯỢN';
			err = '#shopFromCode';
		}
		if ($('#address').val() == '' && msg.length == 0) {
			msg = 'Bạn chưa nhập giá trị cho trường "Địa chỉ" BÊN CHO MƯỢN';
			err = '#address';
		}
		if ($('#taxCode').val() == '' && msg.length == 0) {
			msg = 'Bạn chưa nhập giá trị cho trường "Mã số thuế" BÊN CHO MƯỢN';
			err = '#taxCode';
		}
		if ($('#numberPhone').val() == '' && msg.length == 0) {
			msg = 'Bạn chưa nhập giá trị cho trường "Điện thoại" BÊN CHO MƯỢN';
			err = '#numberPhone';
		}
		if ($('#fromRepresentative').val() == '' && msg.length == 0) {
			msg = 'Bạn chưa nhập giá trị cho trường "Người đại diện" BÊN CHO MƯỢN';
			err = '#fromRepresentative';
		}
		if ($('#fromPosition').val() == '' && msg.length == 0) {
			msg = 'Bạn chưa nhập giá trị cho trường "Chức vụ" BÊN CHO MƯỢN';
			err = '#fromPosition';
		}
		if ($('#fromPage').val() == '' && msg.length == 0) {
			msg = 'Bạn chưa nhập giá trị cho trường "Giấy ủy quyền" BÊN CHO MƯỢN';
			err = '#fromPage';
		}
		if ($('#fromPagePlace').val() == '' && msg.length == 0) {
			msg = 'Bạn chưa nhập giá trị cho trường "Nơi cấp giấy quỷ quyền" BÊN CHO MƯỢN';
			err = '#fromPagePlace';
		}
		var fromPDate = $('#pageDate').val().trim();
		if(msg.length == 0 && fromPDate != '' && !Utils.isDate(fromPDate)){
			msg = 'Ngày cấp GPKD không tồn tại hoặc không đúng định dạng dd/mm/yyyy.';
			err = '#pageDate';
		}	
		if(msg.length == 0){
			msg = Utils.getMessageOfRequireCheck('pageDate','Ngày cấp giấy ủy quyền" BÊN CHO MƯỢN');
			err = '#pageDate';
		}

		if(msg.length == 0){
			msg = Utils.getMessageOfSpecialCharactersValidate('shopToCode','Mã đơn vị BÊN MƯỢN',Utils._CODE);
			err = '#shopToCode';
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfRequireCheck('shopToCode','Mã đơn vị BÊN MƯỢN');
			err = '#shopToCode';
		}	
		if(msg.length == 0){
			msg = Utils.getMessageOfRequireCheck('customerCode','Mã khách hàng');
			err = '#customerCode';
		}	
		if(msg.length == 0){
			msg = Utils.getMessageOfSpecialCharactersValidate('customerCode','Mã khách hàng',Utils._CODE);
			err = '#customerCode';
		}
		if ($('#toObjectAddress').val() == '' && msg.length == 0) {
			msg = 'Bạn chưa nhập giá trị cho trường "Địa chỉ đặt tủ" BÊN MƯỢN';
			err = '#toObjectAddress';
		}
		// if ($('#toBusinessLicense').val() == '' && msg.length == 0) {
		// 	msg = 'Bạn chưa nhập giá trị cho trường "Số GP ĐKKD" BÊN MƯỢN';
		// 	err = '#toBusinessLicense';
		// }
		// if ($('#toBusinessPlace').val() == '' && msg.length == 0) {
		// 	msg = 'Bạn chưa nhập giá trị cho trường "Nơi cấp GP ĐKKD BÊN MƯỢN';
		// 	err = '#toBusinessPlace';
		// }
		var toBusinessDate = $('#toBusinessDate').val().trim();
		if(msg.length == 0 && toBusinessDate != '' && !Utils.isDate(toBusinessDate)){
			msg = 'Ngày cấp GPKD không tồn tại hoặc không đúng định dạng dd/mm/yyyy.';
			err = '#toBusinessDate';
		}	
		// if(msg.length == 0){
		// 	msg = Utils.getMessageOfRequireCheck('toBusinessDate','Ngày cấp GPKD');
		// 	err = '#toBusinessDate';
		// }
		if ($('#toRepresentative').val() == '' && msg.length == 0) {
			msg = 'Bạn chưa nhập giá trị cho trường "Người đại diện" BÊN MƯỢN';
			err = '#toRepresentative';
		}
		if ($('#toPosition').val() == '' && msg.length == 0) {
			msg = 'Bạn chưa nhập giá trị cho trường "Chức vụ người đại diện" BÊN MƯỢN';
			err = '#toPosition';
		}
		if ($('#staffMonitorCode').combobox('getValue') == '' && msg.length == 0) {
			msg = 'Bạn chưa nhập giá trị cho trường "Mã giám sát" BÊN MƯỢN';
			err = '#staffMonitorCode';
		}
		//tamvnm:09/09/2015 update yeu cau ko check so nha, phuong/xa
		// if ($('#addressName').val().trim() == '' && msg.length == 0) {
		// 	msg = 'Bạn chưa nhập giá trị cho trường Số nhà';
		// 	err = '#addressName';
		// }
		if ($('#street').val().trim() == '' && msg.length == 0) {
			msg = 'Bạn chưa nhập giá trị cho trường Tên đường phố/Thôn ấp';
			err = '#street';
		}
		if ($('#provinceName').val().trim() == '' && msg.length == 0) {
			msg = 'Bạn chưa nhập giá trị cho trường Tỉnh/TP';
			err = '#provinceName';
		}
		if ($('#districtName').val().trim() == '' && msg.length == 0) {
			msg = 'Bạn chưa nhập giá trị cho trường Quận/Huyện';
			err = '#districtName';
		}
		// if ($('#wardName').val().trim() == '' && msg.length == 0) {
		// 	msg = 'Bạn chưa nhập giá trị cho trường Phường/Xã';
		// 	err = '#wardName';
		// }

		var freezer = Utils.returnMoneyValue($('#freezer').val().trim());
		var refrigerator = Utils.returnMoneyValue($("#refrigerator").val().trim());
		var intFreezer = parseInt(freezer);
		var intRefrigerator = parseInt(refrigerator);
		if (msg.length == 0) {
			if(freezer != '') {
				if (intFreezer != freezer) {
					msg = 'Tủ mát phải là số nguyên dương có tối đa 17 chữ số';
					err = '#freezer';
				} else if (intFreezer < 0  || intFreezer > 99999999999999999) {
					msg = 'Tủ mát phải là số nguyên dương có tối đa 17 chữ số';
					err = '#freezer';
				}
			} else {
				//tamvnm: 07/07/2015 bo rang buoc ds tu mat
				//msg = 'Tủ mát phải là số nguyên dương có tối đa 17 chữ số';
				//err = '#freezer';
			}
		}
		if (msg.length == 0) {
			if (refrigerator != '') {
				if (intRefrigerator != refrigerator) {
					msg = 'Tủ đông phải là số nguyên dương có tối đa 17 chữ số';
					err = '#refrigerator';
				} else if (intRefrigerator < 0  || intRefrigerator > 99999999999999999) {
					msg = 'Tủ đông phải là số nguyên dương có tối đa 17 chữ số';
					err = '#refrigerator';
				}
			} else {
				//tamvnm: 07/07/2015 bo rang buoc ds tu dong
				//msg = 'Tủ đông phải là số nguyên dương có tối đa 17 chữ số';
				//err = '#refrigerator';
			}
		}


		if(msg.length > 0){
			$('#errMsgInfo').html(msg).show();
			$(err).focus();
			return false;
		}
		
		var rowEquip = $('#gridEquipmentDelivery').datagrid('getRows');
		if(rowEquip.length == 0){
			$('#errMsgInfo').html('Không có thiết bị để cập nhật').show();
			return false;
		}

		var lstContentDelivery = new Array();
		var lstGroup = new Array();
		var lstProvider = new Array();
		var lstStock = new Array();
		var lstEquipCode = new Array();
		var lstSeriNumber = new Array();
		var lstDepreciation = new Array();
		var lstEquipPrice = new Array();
		var lstHealth = new Array();
		var lstManufacturingYear = new Array();
		
		if (EquipmentManagerDelivery._editIndex == undefined) {
			for (var i = 0, isize = rowEquip.length; i < isize; i++) {
				var row = rowEquip[i];
				if (row.contentDelivery == EquipmentManagerDelivery._DELIVERY_CONTENT_NEW) {
					lstGroup.push(row.groupEquipmentCode);
					lstProvider.push(row.equipmentProviderCode);
					lstStock.push(row.stockCode);
					lstEquipPrice.push(Utils.returnMoneyValue(row.price));
					lstHealth.push(row.healthStatus);
					lstManufacturingYear.push(row.yearManufacture);
					if (row.equipmentCode != '' && row.equipmentCode != null) {
						lstEquipCode.push(row.equipmentCode);
					} else {
						lstEquipCode.push('');
					}
				} else {
					lstGroup.push('');
					lstProvider.push('');
					lstStock.push('');
					lstEquipPrice.push(0);
					lstHealth.push('');
					lstManufacturingYear.push(0);
					lstEquipCode.push(row.equipmentCode);
				}
				lstContentDelivery.push(row.contentDelivery);
				
				var seri = null;
				if (row.seriNumber != null && row.seriNumber != '') {
					seri = row.seriNumber;
				} else {
					if ($('#seriNumberInGrid' + i).val() != undefined) {
						seri = $('#seriNumberInGrid' + i).val();
					} 
				}
				lstSeriNumber.push(seri);
				lstDepreciation.push(row.depreciation); 
			}
		} else {
			for (var i = 0, isize = rowEquip.length-1; i < isize; i++) {
				var row = rowEquip[i];
				if (row.contentDelivery == EquipmentManagerDelivery._DELIVERY_CONTENT_NEW) {
					lstGroup.push(row.groupEquipmentCode);
					lstProvider.push(row.equipmentProviderCode);
					lstStock.push(row.stockCode);
					lstEquipPrice.push(Utils.returnMoneyValue(row.price));
					lstHealth.push(row.healthStatus);
					lstManufacturingYear.push(row.yearManufacture);
					if (row.equipmentCode != '' && row.equipmentCode != null) {
						lstEquipCode.push(row.equipmentCode);
					} else {
						lstEquipCode.push('');
					}
				} else {
					lstGroup.push('');
					lstProvider.push('');
					lstStock.push('');
					lstEquipPrice.push(0);
					lstHealth.push('');
					lstManufacturingYear.push(0);
					lstEquipCode.push(row.equipmentCode);
				}
				lstContentDelivery.push(row.contentDelivery);
				
				var seri = null;
				if (row.seriNumber != null && row.seriNumber != '') {
					seri = row.seriNumber;
				} else {
					if ($('#seriNumberInGrid' + i).val() != undefined) {
						seri = $('#seriNumberInGrid' + i).val();
					} 
				}
				lstSeriNumber.push(seri);
				lstDepreciation.push(row.depreciation);
			}
		}

		//xu ly dong cuoi
		var index = EquipmentManagerDelivery._editIndex;
		if (index == rowEquip.length-1) {
			if ($('#contentDelivery').val() != undefined && $('#contentDelivery').val() != null) {
				var curContent = $('#contentDelivery').val();
				var curGroup = null;
				var curProvider = null;
				var curStock = null;
				var curEquipCode = $('#equipmentCodeInGrid' + index).val().trim();
				
				var curSeri = null;
				if (rowEquip[index].seriNumber != null && rowEquip[index].seriNumber != '') {
					curSeri = rowEquip[index].seriNumber;
				} else if ($('#seriNumberInGrid' + index).val() != undefined) {
					curSeri = $('#seriNumberInGrid' + index).val();
				}

				// if (curContent == EquipmentManagerDelivery._DELIVERY_CONTENT_NEW) {
				// 	curSeri = $('#seriNumberInGrid' + index).val().trim();
				// } else {
				// 	if ($('#seriNumberInGrid' + index).val() == undefined) {
				// 		curSeri = rowEquip[index].seriNumber;
				// 	} else {
				// 		curSeri = $('#seriNumberInGrid' + index).val();
				// 	}
					
				// }
				
				var curDeprec = $('#depreciationInGrid' + index).val().trim();
				var curPrice = null;
				var curHealth = null;
				var curManufacturingYear = null;
				if (curContent != undefined && curContent == EquipmentManagerDelivery._DELIVERY_CONTENT_NEW) {
					curGroup = $('#groupEquipCBX' + index).combobox('getValue').trim();
					curProvider = $('#providerEquipCBX' + index).combobox('getValue').trim();
					curStock = $('#stockEquipCBX' + index).combobox('getValue').trim();
					curPrice = $('#price' + index).val().trim();
					curHealth = $('#healthStatus' + index).val().trim();
					curManufacturingYear = $('#yearManufacture' + index).val().trim();

					if (curGroup == '') {
						$('#errMsgInfo').html('Bạn chưa chọn nhóm thiết bị').show();
						$('#groupEquipCBX' + index).focus();
						return false;
					} else {

					}

					if (curProvider == '') {
						$('#errMsgInfo').html('Bạn chưa chọn nhà cung cấp').show();
						$('#providerEquipCBX' + index).focus();
						return false;
					} else {

					}

					if (curStock == '') {
						$('#errMsgInfo').html('Bạn chưa chọn kho').show();
						$('#stockEquipCBX' + index).focus();
						return false;
					}

					if (curEquipCode != '') {
						$('#errMsgInfo').html('Bạn không được nhập mã thiết bị').show();
						return false;
					} else {

					}

					if (curDeprec == '') {
						$('#errMsgInfo').html('Bạn chưa nhập số tháng khấu hao').show();
						return false;
					} else {
						var mes = Utils.getMessageOfInvaildNumberNew(curDeprec);
						if (mes.length > 0) {
							$('#errMsgInfo').html('Số tháng khấu hao phải là số nguyên dương').show();
							return false;
						}
					}

					if (curPrice == '') {
						$('#errMsgInfo').html('Bạn chưa nhập giá trị TBBH').show();
						return false;
					} else {
						var mes = Utils.getMessageOfInvaildNumber('price' + index, 'Giát trị TBBH');
						if (mes.length > 0) {
							$('#errMsgInfo').html('Giá trị TBBH phải là số nguyên dương').show();
							return false;
						}
					}

					if (curManufacturingYear == '') {
						$('#errMsgInfo').html('Bạn chưa nhập giá trị năm sản xuất').show();
						return false;
					} else {
						var mes = Utils.getMessageOfInvaildNumber('yearManufacture' + index, 'Năm sản xuất');
						if (mes.length > 0) {
							$('#errMsgInfo').html('Năm sản xuất phải là số nguyên dương').show();
							return false;
						}
					}

					if (curHealth == '') {
						$('#errMsgInfo').html('Bạn chưa chọn trình trạng thiết bị').show();
						return false;
					} else {

					}
					//khong co loi nao:
					lstGroup.push(curGroup);
					lstProvider.push(curProvider);
					lstStock.push(curStock);
					lstEquipPrice.push(Utils.returnMoneyValue(curPrice));
					lstHealth.push(curHealth);
					lstManufacturingYear.push(curManufacturingYear);
					lstEquipCode.push('');
					lstContentDelivery.push(curContent);
					lstSeriNumber.push(curSeri);
					lstDepreciation.push(curDeprec);
				} else {
					if (curEquipCode == '') {
						$('#errMsgInfo').html('Bạn chưa nhập mã thiết bị').show();
						return false;
					} else {
						curEquipCode = curEquipCode.trim();
						for (var i = 0, isize = lstEquipCode.length; i< isize; i++) {
							if (curEquipCode == lstEquipCode[i]) {		
								$('#errMsgInfo').html('Mã thiết bị ' + curEquipCode + 'bị trùng với dòng ' + i + 1).show();
								return false;
							}
						}
					}
					if (curDeprec == '') {
						$('#errMsgInfo').html('Bạn chưa nhập số tháng khấu hao').show();
						return false;
					} else {
						var mes = Utils.getMessageOfInvaildNumberNew(curDeprec);
						if (mes.length > 0) {
							$('#errMsgInfo').html('Số tháng khấu hao phải là số nguyên dương').show();
							return false;
						}
					}
					//khong co loi nao:
					lstGroup.push('');
					lstProvider.push('');
					lstStock.push('');
					lstEquipPrice.push(0);
					lstHealth.push('');
					lstManufacturingYear.push(0);
					lstEquipCode.push(curEquipCode);
					lstContentDelivery.push(curContent);
					// if (curSeri == null) {
					// 	lstSeriNumber.push('');	
					// } else {
					// 	lstSeriNumber.push(curSeri);
					// }
					lstSeriNumber.push(curSeri);
					
					lstDepreciation.push(curDeprec);
				}
			}
		}

		var params=new Object(); 
		params.shopFromCode = $('#shopFromCode').val().trim();
		params.shopToCode = $('#shopToCode').val().trim();
		params.customerCode = $('#customerCode').val().trim();
		params.numberContract = $('#numberContract').val().trim();
		params.contractDate = $('#contractDate').val().trim();
		params.statusRecord = $('#status').val().trim();
		params.statusDelivery = $('#statusDelivery').val().trim();
		
		params.fromObjectAddress = $("#address").val().trim();
		params.fromObjectTax  = $("#taxCode").val().trim();
		params.fromObjectPhone  = $("#numberPhone").val().trim();
		params.fromRepresentative = $("#fromRepresentative").val().trim();
		params.fromObjectPosition = $("#fromPosition").val().trim();
		params.fromPage = $("#fromPage").val().trim();
		params.fromPagePlace = $("#fromPagePlace").val().trim();
		params.fromPageDate = $("#pageDate").val().trim();
		params.fromFax = $('#fromFax').val();
		params.toPermanentAddress = $("#toPermanentAddress").val().trim();
		params.toRepresentative = $("#toRepresentative").val().trim();
		params.toObjectPosition = $("#toPosition").val().trim();
		params.toIdNO = $("#toIdNO").val().trim();
		params.toIdNODate = $("#idNODate").val().trim();
		params.toIdNOPlace = $("#toIdNOPlace").val().trim();
		params.freezer = Utils.returnMoneyValue($('#freezer').val().trim());
		params.refrigerator = Utils.returnMoneyValue($("#refrigerator").val().trim());
		params.fromShopCode = $("#shopFromCode").val().trim();
		params.toShopCode = $("#shopToCode").val().trim();
		params.staffCode = $('#staffMonitorCode').combobox('getValue').trim();
		params.toBusinessLicense = $('#toBusinessLicense').val().trim();
		params.toBusinessDate = $('#toBusinessDate').val().trim();
		params.toBusinessPlace = $('#toBusinessPlace').val().trim();
		params.createDate = createDate;
		
		params.addressName = $('#addressName').val().trim();
		params.street = $('#street').val().trim();
		params.provinceName = $('#provinceName').val().trim();
		params.districtName = $('#districtName').val().trim();
		params.wardName = $('#wardName').val().trim();
		params.note = $('#note').val();


		var curLength = lstContentDelivery.length;
		if (lstGroup.length != curLength || lstProvider.length != curLength || lstEquipCode.length != curLength
			|| lstSeriNumber.length != curLength || lstDepreciation.length != curLength || lstEquipPrice.length != curLength
			|| lstHealth.length != curLength || lstStock.length != curLength) {
			$('#errMsgInfo').html("Thiết thông tin thiết bi").show();
			return false;
		}

		if(lstContentDelivery.length > 0){
			params.lstContentDelivery = lstContentDelivery;
			params.lstGroup = lstGroup;
			params.lstProvider = lstProvider;
			params.lstStock = lstStock;
			params.lstEquipCode = lstEquipCode;
			params.lstSeriNumber = lstSeriNumber;
			params.lstDepreciation = lstDepreciation;
			params.lstEquipPrice = lstEquipPrice;
			params.lstHealth = lstHealth;
			params.lstManufacturingYear = lstManufacturingYear;
		}

		if (params.statusRecord == 2 && lstSeriNumber.length > 0) {
			for (var i = 0; i < lstSeriNumber.length; i++) {
				if (lstSeriNumber[i] == null || lstSeriNumber[i] == '') {
					$('#errMsgInfo').html("Vui lòng nhập đầy đủ thông tin seri khi chọn trạng thái Duyệt").show();
					return false;
				}
			}
		}
		
		//upload file
		var arrFile = DropzonePlugin._dropzoneObject.getFilesWithStatus(Dropzone.ADDED);
		if (EquipmentManagerDelivery._arrFileDelete == undefined || EquipmentManagerDelivery._arrFileDelete == null) {
			EquipmentManagerDelivery._arrFileDelete = [];
		}
		if (arrFile != null && arrFile != undefined && arrFile.length > 0) {
			if (EquipmentManagerDelivery._countFile == 5) {
				$('#errMsgInfo').html("Tổng số tập tin đính kèm không được vượt quá 5").show();
				return false;
			}
			var maxLength = 5 - EquipmentManagerDelivery._countFile;
			if(maxLength<0){
				maxLength = 0;
			}
			if (arrFile.length > maxLength) {
				$('#errMsgInfo').html("Số tập tin thêm mới không được vượt quá " + maxLength).show();
				return false;
			}
			msg = UploadUtil.getMsgByFileRequireCheck(UploadUtil._typeFileRequireCheck.equipRecord);
			if (msg.trim().length > 0) {
				$('#errMsgInfo').html(msg).show();
				return false;
			}			
		}
		params.equipAttachFileStr = EquipmentManagerDelivery._arrFileDelete.join(',');

		var isStaff = false;
		var shopToCode = $('#shopToCode').val().trim();
		if (shopToCode != undefined && shopToCode != null && shopToCode != '') {
			$.getJSON('/equipment-manage-delivery/getListGSNPP?toShopCode=' + shopToCode, function(result) {
				if (result.rows != undefined && result.rows != null && result.rows.length > 0) {
					for (var i = 0, sz = result.rows.length; i < sz; i++) {
						if ($('#staffMonitorCode').combobox('getValue').trim() == result.rows[i].staffCode) {
							isStaff = true;
						}
					}
				}
				if (result.rows.length == 0) {
					msg = 'Không tìm thấy "Đơn vị" BÊN MƯỢN hoặc Mã giám sát không quản lý "Đơn vị" BÊN MƯỢN';
					err = '#shopToCode';
					$('#errMsgInfo').html(msg).show();
					$(err).focus();
					return false;
				} else if (!isStaff) {
					msg = 'Mã giám sát không quản lý "Đơn vị" BÊN MƯỢN';
					err = '#staffMonitorCode';
					$('#errMsgInfo').html(msg).show();
					$(err).focus();
					return false;
				} else {
					$.messager.confirm('Xác nhận', 'Bạn có muốn lưu dữ liệu?', function(r) {
						if (r) {
							if (arrFile != null && arrFile != undefined && arrFile.length > 0) {
								params = JSONUtil.getSimpleObject2(params);
								UploadUtil.updateAdditionalDataForUpload(params);
								UploadUtil.startUpload('errMsgInfo');
								return false;
							}
							Utils.saveData(params, '/equipment-manage-delivery/create-record-delivery', null, 'errMsgInfo', function(data) {
								if (data.error) {
									$('#errMsgInfo').html(data.errMsgInfo).show();
									return false;
								} else {
									$('#successMsgInfo').html("Cập nhật dữ liệu thành công.").show();
									EquipmentManagerDelivery._lstEquipInsert = null;
									EquipmentManagerDelivery._lstStockInsert = null;
									EquipmentManagerDelivery._editIndex = null;
									setTimeout(function() {
										$('#successMsgInfo').html("").hide();
										window.location.href = '/equipment-manage-delivery/info';
									}, 3000);
								}
							}, null, null, null, null);
						}
					});
				}
			});
		}
	},
	/**
	 * Cap nhat bien ban voi tinh trang da duyet
	 * 
	 * @author hunglm16
	 * @since August 18,2015
	 * */
	updateRecordDeliveryByApproved: function() {
		var params = new Object();
		var staffMonitorCode = $('#staffMonitorCode').combobox('getValue').trim();
		if (staffMonitorCode != undefined && staffMonitorCode != null && staffMonitorCode.length > 0) {
			params.staffCode = staffMonitorCode;
			params.idRecordDelivery = $("#idRecordHidden").val().trim();
			$.messager.confirm('Xác nhận', "Bạn có muốn cập nhật biên bản này ?", function(r) {
				 if (r) {
					 Utils.saveData(params, '/equipment-manage-delivery/update-record-delivery', null, 'errMsgInfo', function(data) {	
						 if (data.error) {
							 $('#errMsgInfo').html(data.errMsgInfo).show();
							 return false;
						 } else {
							 $('#successMsgInfo').html("Cập nhật dữ liệu thành công.").show();
							 setTimeout(function() {
								 $('#successMsgInfo').html("").hide(); 
								 //window.location.href = '/equipment-manage-delivery/change-record?id='+ $("#idRecordHidden").val().trim();						
							 }, 1500);
						 }
					 }, null, null, null, null);
				 }
			 });		
		} else {
			$('#errMsgInfo').html('Không xác định được giám sát').show();
		}
		return false;
	},
	
	/**
	 * Chinh sua bien ban
	 * @author tamvnm
	 * @since 26/06/2015
	 */
	updateRecordDelivery: function(){
		$('#errMsgInfo').html('').hide();
		var msg ='';
		var err = '';
		//Xu ly cho truong hop da duyet
		if ($("#statusRecordHidden").val() == EquipmentManagerDelivery._approved) {
			return EquipmentManagerDelivery.updateRecordDeliveryByApproved();
		}
		
		var nContractTrim = $('#numberContract').val().trim();
		$('#numberContract').val(nContractTrim);
		//if(msg.length == 0 && $('#numberContract').val().indexOf(' ')!=-1){
		//	msg = 'Số hợp đồng không được có khoảng trắng';
		//	err = '#numberContract';
		//}
		// if(msg.length == 0){
		// 	msg = Utils.getMessageOfSpecialCharactersValidate('numberContract','Số hợp đồng',Utils._NAME_CUSTYPE);
		// 	err = '#numberContract';
		// }
		var createDate = $('#createDate').val();
		if(msg.length == 0 && $('#createDate').val().trim().length > 0 && !Utils.isDate($('#createDate').val()) && $('#createDate').val() != '__/__/____' ){
			msg = 'Ngày biên bản không tồn tại hoặc không đúng định dạng dd/mm/yyyy.';
			err = '#createDate';
		}
		var curDate = ReportUtils.getCurrentDateString(); //Lay ngay hien tai sysDate
		if (msg.length == 0 && !Utils.compareDate($('#createDate').val().trim(), curDate)) {
			msg = 'Ngày biên bản không được sau ngày hiện tại. Vui lòng nhập lại';
			$('#createDate').focus();
		}

		if(msg.length == 0){
			msg = Utils.getMessageOfRequireCheck('numberContract','Số hợp đồng');
			err = '#numberContract';
		}	
		if(msg.length == 0){
			msg = Utils.getMessageOfRequireCheck('contractDate','Ngày hợp đồng');
			err = '#contractDate';
		}				
		var contractDate = $('#contractDate').val().trim();
		if(msg.length == 0 && contractDate != '' && !Utils.isDate(contractDate)){
			msg = 'Ngày hợp đồng không tồn tại hoặc không đúng định dạng dd/mm/yyyy.';
			err = '#contractDate';
		}	

		if ($('#shopFromCode').val() == '' && msg.length == 0) {
			msg = 'Bạn chưa nhập giá trị cho trường "Đơn vị" BÊN CHO MƯỢN';
			err = '#shopFromCode';
		}
		if ($('#address').val() == '' && msg.length == 0) {
			msg = 'Bạn chưa nhập giá trị cho trường "Địa chỉ" BÊN CHO MƯỢN';
			err = '#address';
		}
		if ($('#taxCode').val() == '' && msg.length == 0) {
			msg = 'Bạn chưa nhập giá trị cho trường "Mã số thuế" BÊN CHO MƯỢN';
			err = '#taxCode';
		}
		if ($('#numberPhone').val() == '' && msg.length == 0) {
			msg = 'Bạn chưa nhập giá trị cho trường "Điện thoại" BÊN CHO MƯỢN';
			err = '#numberPhone';
		}
		if ($('#fromRepresentative').val() == '' && msg.length == 0) {
			msg = 'Bạn chưa nhập giá trị cho trường "Người đại diện" BÊN CHO MƯỢN';
			err = '#fromRepresentative';
		}
		if ($('#fromPosition').val() == '' && msg.length == 0) {
			msg = 'Bạn chưa nhập giá trị cho trường "Chức vụ" BÊN CHO MƯỢN';
			err = '#fromPosition';
		}
		if ($('#fromPage').val() == '' && msg.length == 0) {
			msg = 'Bạn chưa nhập giá trị cho trường "Giấy ủy quyền" BÊN CHO MƯỢN';
			err = '#fromPage';
		}
		if ($('#fromPagePlace').val() == '' && msg.length == 0) {
			msg = 'Bạn chưa nhập giá trị cho trường "Nơi cấp giấy quỷ quyền" BÊN CHO MƯỢN';
			err = '#fromPagePlace';
		}
		var fromPDate = $('#pageDate').val().trim();
		if(msg.length == 0 && fromPDate != '' && !Utils.isDate(fromPDate)){
			msg = 'Ngày cấp GPKD không tồn tại hoặc không đúng định dạng dd/mm/yyyy.';
			err = '#pageDate';
		}	
		if(msg.length == 0){
			msg = Utils.getMessageOfRequireCheck('pageDate','Ngày cấp giấy ủy quyền" BÊN CHO MƯỢN');
			err = '#pageDate';
		}

		if(msg.length == 0){
			msg = Utils.getMessageOfSpecialCharactersValidate('shopToCode','Mã đơn vị BÊN MƯỢN',Utils._CODE);
			err = '#shopToCode';
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfRequireCheck('shopToCode','Mã đơn vị BÊN MƯỢN');
			err = '#shopToCode';
		}	
		if(msg.length == 0){
			msg = Utils.getMessageOfRequireCheck('customerCode','Mã khách hàng');
			err = '#customerCode';
		}	
		if(msg.length == 0){
			msg = Utils.getMessageOfSpecialCharactersValidate('customerCode','Mã khách hàng',Utils._CODE);
			err = '#customerCode';
		}
		if ($('#toObjectAddress').val() == '' && msg.length == 0) {
			msg = 'Bạn chưa nhập giá trị cho trường "Địa chỉ đặt tủ" BÊN MƯỢN';
			err = '#toObjectAddress';
		}
		// if ($('#toBusinessLicense').val() == '' && msg.length == 0) {
		// 	msg = 'Bạn chưa nhập giá trị cho trường "Số GP ĐKKD" BÊN MƯỢN';
		// 	err = '#toBusinessLicense';
		// }
		// if ($('#toBusinessPlace').val() == '' && msg.length == 0) {
		// 	msg = 'Bạn chưa nhập giá trị cho trường "Nơi cấp GP ĐKKD BÊN MƯỢN';
		// 	err = '#toBusinessPlace';
		// }
		var toBusinessDate = $('#toBusinessDate').val().trim();
		if(msg.length == 0 && toBusinessDate != '' && !Utils.isDate(toBusinessDate)){
			msg = 'Ngày cấp GPKD không tồn tại hoặc không đúng định dạng dd/mm/yyyy.';
			err = '#toBusinessDate';
		}	
		// if(msg.length == 0){
		// 	msg = Utils.getMessageOfRequireCheck('toBusinessDate','Ngày cấp GPKD');
		// 	err = '#toBusinessDate';
		// }
		if ($('#toRepresentative').val() == '' && msg.length == 0) {
			msg = 'Bạn chưa nhập giá trị cho trường "Người đại diện" BÊN MƯỢN';
			err = '#toRepresentative';
		}
		if ($('#toPosition').val() == '' && msg.length == 0) {
			msg = 'Bạn chưa nhập giá trị cho trường "Chức vụ người đại diện" BÊN MƯỢN';
			err = '#toPosition';
		}
		if ($('#staffMonitorCode').combobox('getValue') == '' && msg.length == 0) {
			msg = 'Bạn chưa nhập giá trị cho trường "Mã giám sát" BÊN MƯỢN';
			err = '#staffMonitorCode';
		}
		//tamvnm:09/09/2015 update yeu cau ko check so nha, phuong/xa
		// if ($('#addressName').val().trim() == '' && msg.length == 0) {
		// 	msg = 'Bạn chưa nhập giá trị cho trường Số nhà';
		// 	err = '#addressName';
		// }
		if ($('#street').val().trim() == '' && msg.length == 0) {
			msg = 'Bạn chưa nhập giá trị cho trường Tên đường phố/Thôn ấp';
			err = '#street';
		}
		if ($('#provinceName').val().trim() == '' && msg.length == 0) {
			msg = 'Bạn chưa nhập giá trị cho trường Tỉnh/TP';
			err = '#provinceName';
		}
		if ($('#districtName').val().trim() == '' && msg.length == 0) {
			msg = 'Bạn chưa nhập giá trị cho trường Quận/Huyện';
			err = '#districtName';
		}
		// if ($('#wardName').val().trim() == '' && msg.length == 0) {
		// 	msg = 'Bạn chưa nhập giá trị cho trường Phường/Xã';
		// 	err = '#wardName';
		// }

		var freezer = Utils.returnMoneyValue($('#freezer').val().trim());
		var refrigerator = Utils.returnMoneyValue($("#refrigerator").val().trim());
		var intFreezer = parseInt(freezer);
		var intRefrigerator = parseInt(refrigerator);
		if (msg.length == 0) {
			if(freezer != '') {
				if (intFreezer != freezer) {
					msg = 'Tủ mát phải là số nguyên dương có tối đa 17 chữ số';
					err = '#freezer';
				} else if (intFreezer < 0  || intFreezer > 99999999999999999) {
					msg = 'Tủ mát phải là số nguyên dương có tối đa 17 chữ số';
					err = '#freezer';
				}
			} else {
				//tamvnm: 07/07/2015 bo rang buoc ds tu mat
				//msg = 'Tủ mát phải là số nguyên dương có tối đa 17 chữ số';
				//err = '#freezer';
			}
		}
		if (msg.length == 0) {
			if (refrigerator != '') {
				if (intRefrigerator != refrigerator) {
					msg = 'Tủ đông phải là số nguyên dương có tối đa 17 chữ số';
					err = '#refrigerator';
				} else if (intRefrigerator < 0  || intRefrigerator > 99999999999999999) {
					msg = 'Tủ đông phải là số nguyên dương có tối đa 17 chữ số';
					err = '#refrigerator';
				}
			} else {
				//tamvnm: 07/07/2015 bo rang buoc ds tu dong
				//msg = 'Tủ đông phải là số nguyên dương có tối đa 17 chữ số';
				//err = '#refrigerator';
			}
		}

		if(msg.length > 0){
			$('#errMsgInfo').html(msg).show();
			$(err).focus();
			return false;
		}


		
		var rowEquip = $('#gridEquipmentDelivery').datagrid('getRows');
		if( rowEquip.length == 0 ){
			$('#errMsgInfo').html('Không có thiết bị để cập nhật').show();
			return false;
		}
		
		var lstContentDelivery = new Array();
		var lstGroup = new Array();
		var lstProvider = new Array();
		var lstEquipCode = new Array();
		var lstStock = new Array();
		var lstSeriNumber = new Array();
		var lstDepreciation = new Array();
		var lstEquipPrice = new Array();
		var lstHealth = new Array();
		var lstManufacturingYear = new Array();
		
		if (EquipmentManagerDelivery._editIndex == undefined) {
			for (var i = 0, isize = rowEquip.length; i < isize; i++) {
				var row = rowEquip[i];
				if (row.contentDelivery == EquipmentManagerDelivery._DELIVERY_CONTENT_NEW) {
					lstGroup.push(row.groupEquipmentCode);
					lstProvider.push(row.equipmentProviderCode);
					lstStock.push(row.stockCode);
					lstEquipPrice.push(Utils.returnMoneyValue(row.price));
					lstHealth.push(row.healthStatus);
					lstManufacturingYear.push(row.yearManufacture);
					if (row.equipmentCode != '' && row.equipmentCode != null) {
						lstEquipCode.push(row.equipmentCode);
					} else {
						lstEquipCode.push('');
					}
				} else {
					lstGroup.push('');
					lstProvider.push('');
					lstStock.push('');
					lstEquipPrice.push(0);
					lstHealth.push('');
					lstManufacturingYear.push(0);
					lstEquipCode.push(row.equipmentCode);
				}
				lstContentDelivery.push(row.contentDelivery);
				
				var seri = null;
				if (row.seriNumber != null && row.seriNumber != '') {
					seri = row.seriNumber;
				} else {
					if ($('#seriNumberInGrid' + i).val() != undefined) {
						seri = $('#seriNumberInGrid' + i).val();
					} 
				}
				lstSeriNumber.push(seri);
				lstDepreciation.push(row.depreciation); 
			}
		} else {
			for (var i = 0, isize = rowEquip.length-1; i < isize; i++) {
				var row = rowEquip[i];
				if (row.contentDelivery == EquipmentManagerDelivery._DELIVERY_CONTENT_NEW) {
					lstGroup.push(row.groupEquipmentCode);
					lstProvider.push(row.equipmentProviderCode);
					lstStock.push(row.stockCode);
					lstEquipPrice.push(Utils.returnMoneyValue(row.price));
					lstHealth.push(row.healthStatus);
					lstManufacturingYear.push(row.yearManufacture);
					if (row.equipmentCode != '' && row.equipmentCode != null) {
						lstEquipCode.push(row.equipmentCode);
					} else {
						lstEquipCode.push('');
					}
				} else {
					lstGroup.push('');
					lstProvider.push('');
					lstStock.push('');
					lstEquipPrice.push(0);
					lstHealth.push('');
					lstManufacturingYear.push(0);
					lstEquipCode.push(row.equipmentCode);
				}
				lstContentDelivery.push(row.contentDelivery);
				
				var seri = null;
				if (row.seriNumber != null && row.seriNumber != '') {
					seri = row.seriNumber;
				} else {
					if ($('#seriNumberInGrid' + i).val() != undefined) {
						seri = $('#seriNumberInGrid' + i).val();
					} 
				}
				lstSeriNumber.push(seri);
				lstDepreciation.push(row.depreciation);
			}
		}

		//xu ly dong cuoi
		var index = EquipmentManagerDelivery._editIndex;
		if (index == rowEquip.length-1) {
			if ($('#contentDelivery').val() != undefined && $('#contentDelivery').val() != null) {
				var curContent = $('#contentDelivery').val();
				var curGroup = null;
				var curProvider = null;
				var curStock = null;
				var curEquipCode = $('#equipmentCodeInGrid' + index).val().trim();
				
				var curSeri = null;
				if (rowEquip[index].seriNumber != null && rowEquip[index].seriNumber != '') {
					curSeri = rowEquip[index].seriNumber;
				} else if ($('#seriNumberInGrid' + index).val() != undefined) {
					curSeri = $('#seriNumberInGrid' + index).val();
				}
				var curDeprec = $('#depreciationInGrid' + index).val().trim();
				var curPrice = null;
				var curHealth = null;
				var curManufacturingYear = null;
				if (curContent != undefined && curContent == EquipmentManagerDelivery._DELIVERY_CONTENT_NEW) {
					curGroup = $('#groupEquipCBX' + index).combobox('getValue').trim();
					curProvider = $('#providerEquipCBX' + index).combobox('getValue').trim();
					curStock = $('#stockEquipCBX' + index).combobox('getValue').trim();
					curPrice = $('#price' + index).val().trim();
					curHealth = $('#healthStatus' + index).val().trim();
					curManufacturingYear = $('#yearManufacture' + index).val().trim();

					if (curGroup == '') {
						$('#errMsgInfo').html('Bạn chưa chọn nhóm thiết bị').show();
						$('#groupEquipCBX' + index).focus();
						return false;
					} else {

					}

					if (curProvider == '') {
						$('#errMsgInfo').html('Bạn chưa chọn nhà cung cấp').show();
						$('#providerEquipCBX' + index).focus();
						return false;
					} else {

					}
					if (curStock == '') {
						$('#errMsgInfo').html('Bạn chưa chọn kho').show();
						$('#stockEquipCBX' + index).focus();
						return false;
					}

					if (curEquipCode != '') {
						$('#errMsgInfo').html('Bạn không được nhập mã thiết bị').show();
						return false;
					} else {

					}

					if (curDeprec == '') {
						$('#errMsgInfo').html('Bạn chưa nhập số tháng khấu hao').show();
						return false;
					} else {
						var mes = Utils.getMessageOfInvaildNumberNew(curDeprec);
						if (mes.length > 0) {
							$('#errMsgInfo').html('Số tháng khấu hao phải là số nguyên dương').show();
							return false;
						}
					}

					if (curPrice == '') {
						$('#errMsgInfo').html('Bạn chưa nhập giá trị TBBH').show();
						return false;
					} else {
						var mes = Utils.getMessageOfInvaildNumber('price' + index, 'Giát trị TBBH');
						if (mes.length > 0) {
							$('#errMsgInfo').html('Giá trị TBBH phải là số nguyên dương').show();
							return false;
						}
					}

					if (curManufacturingYear == '') {
						$('#errMsgInfo').html('Bạn chưa nhập giá trị năm sản xuất').show();
						return false;
					} else {
						var mes = Utils.getMessageOfInvaildNumber('yearManufacture' + index, 'Năm sản xuất');
						if (mes.length > 0) {
							$('#errMsgInfo').html('Năm sản xuất phải là số nguyên dương').show();
							return false;
						}
					}

					if (curHealth == '') {
						$('#errMsgInfo').html('Bạn chưa chọn trình trạng thiết bị').show();
						return false;
					} else {

					}
					//khong co loi nao:
					lstGroup.push(curGroup);
					lstProvider.push(curProvider);
					lstStock.push(curStock);
					lstEquipPrice.push(Utils.returnMoneyValue(curPrice));
					lstHealth.push(curHealth);
					lstManufacturingYear.push(curManufacturingYear);
					lstEquipCode.push('');
					lstContentDelivery.push(curContent);
					lstSeriNumber.push(curSeri);
					lstDepreciation.push(curDeprec);
				} else {
					if (curEquipCode == '') {
						$('#errMsgInfo').html('Bạn chưa nhập mã thiết bị').show();
						return false;
					} else {
						curEquipCode = curEquipCode.trim();
						for (var i = 0, isize = lstEquipCode.length; i< isize; i++) {
							if (curEquipCode == lstEquipCode[i]) {		
								$('#errMsgInfo').html('Mã thiết bị ' + curEquipCode + 'bị trùng với dòng ' + i + 1).show();
								return false;
							}
						}
					}
					if (curDeprec == '') {
						$('#errMsgInfo').html('Bạn chưa nhập số tháng khấu hao').show();
						return false;
					} else {
						var mes = Utils.getMessageOfInvaildNumberNew(curDeprec);
						if (mes.length > 0) {
							$('#errMsgInfo').html('Số tháng khấu hao phải là số nguyên dương').show();
							return false;
						}
					}
					//khong co loi nao:
					lstGroup.push('');
					lstProvider.push('');
					lstStock.push('');
					lstEquipPrice.push(0);
					lstHealth.push('');
					lstManufacturingYear.push(0);
					lstEquipCode.push(curEquipCode);
					lstContentDelivery.push(curContent);
					// if (curSeri == null) {
					// 	lstSeriNumber.push('');	
					// } else {
					// 	lstSeriNumber.push(curSeri);
					// }
					lstSeriNumber.push(curSeri);
					
					lstDepreciation.push(curDeprec);
				}
			}
		}

		
		var params=new Object(); 		
		params.numberContract = $('#numberContract').val().trim();
		params.contractDate = $('#contractDate').val().trim();
		params.statusRecord = $('#status').val().trim();
		params.statusDelivery = $('#statusDelivery').val().trim();
		params.idRecordDelivery = $("#idRecordHidden").val().trim(); //khong can check phan quyen
		params.fromObjectAddress = $("#address").val().trim();
		params.fromObjectTax  = $("#taxCode").val().trim();
		params.fromObjectPhone  = $("#numberPhone").val().trim();
		params.fromRepresentative = $("#fromRepresentative").val().trim();
		params.fromObjectPosition = $("#fromPosition").val().trim();
		params.fromPage = $("#fromPage").val().trim();
		params.fromPagePlace = $("#fromPagePlace").val().trim();
		params.fromPageDate = $("#pageDate").val().trim();
		params.fromFax = $('#fromFax').val();
		params.toPermanentAddress = $("#toPermanentAddress").val().trim();
		params.toRepresentative = $("#toRepresentative").val().trim();
		params.toObjectPosition = $("#toPosition").val().trim();
		params.toIdNO = $("#toIdNO").val().trim();
		params.toIdNODate = $("#idNODate").val().trim();
		params.toIdNOPlace = $("#toIdNOPlace").val().trim();
		params.freezer = Utils.returnMoneyValue($('#freezer').val().trim());
		params.refrigerator = Utils.returnMoneyValue($("#refrigerator").val().trim());
		params.fromShopCode = $("#shopFromCode").val().trim();
		params.toShopCode = $("#shopToCode").val().trim();
		params.toBusinessLicense = $('#toBusinessLicense').val().trim();
		params.toBusinessDate = $('#toBusinessDate').val().trim();
		params.toBusinessPlace = $('#toBusinessPlace').val().trim();
		params.createDate = createDate;

		params.addressName = $('#addressName').val().trim();
		params.street = $('#street').val().trim();
		params.provinceName = $('#provinceName').val().trim();
		params.districtName = $('#districtName').val().trim();
		params.wardName = $('#wardName').val().trim();

		params.note = $('#note').val();

		var toShopCodeHidden = $("#fromShopHidden").val().trim();

		if (EquipmentManagerDelivery._customerId == null) {
			if (toShopCodeHidden == $("#shopToCode").val().trim()) {
				params.customerId = $("#customerIdHidden").val().trim();
			}
		} else {
			params.customerId = EquipmentManagerDelivery._customerId;
		}
		params.staffCode = $('#staffMonitorCode').combobox('getValue');

		params.id = params.idRecordDelivery;

		var curLength = lstContentDelivery.length;
		if (lstGroup.length != curLength || lstProvider.length != curLength || lstEquipCode.length != curLength
			|| lstSeriNumber.length != curLength || lstDepreciation.length != curLength || lstEquipPrice.length != curLength
			|| lstHealth.length != curLength || lstStock.length != curLength) {
			$('#errMsgInfo').html("Thiết thông tin thiết bi").show();
			return false;
		}

		if(lstContentDelivery.length > 0){
			params.lstContentDelivery = lstContentDelivery;
			params.lstGroup = lstGroup;
			params.lstProvider = lstProvider;
			params.lstStock = lstStock;
			params.lstEquipCode = lstEquipCode;
			params.lstSeriNumber = lstSeriNumber;
			params.lstDepreciation = lstDepreciation;
			params.lstEquipPrice = lstEquipPrice;
			params.lstHealth = lstHealth;
			params.lstManufacturingYear = lstManufacturingYear;
		}

		if (params.statusRecord == 2 && lstSeriNumber.length > 0) {
			for (var i = 0; i < lstSeriNumber.length; i++) {
				if (lstSeriNumber[i] == null || lstSeriNumber[i] == '') {
					$('#errMsgInfo').html("Vui lòng nhập đầy đủ thông tin seri khi chọn trạng thái Duyệt").show();
					return false;
				}
			}
		}

		if($("#idRecordHidden").val() != "" && EquipmentManagerDelivery._lstEquipInRecord != null){
			params.lstEquipDelete = "";
			for(var i=0; i< EquipmentManagerDelivery._lstEquipInRecord.length; i++){
				params.lstEquipDelete+=EquipmentManagerDelivery._lstEquipInRecord[i]+',';
			}
		}
		
		var arrFile = DropzonePlugin._dropzoneObject.getFilesWithStatus(Dropzone.ADDED);
		if (EquipmentManagerDelivery._arrFileDelete == undefined || EquipmentManagerDelivery._arrFileDelete == null) {
			EquipmentManagerDelivery._arrFileDelete = [];
		}
		if (arrFile != null && arrFile != undefined && arrFile.length > 0) {
			if (EquipmentManagerDelivery._countFile == 5) {
				$('#errMsgInfo').html("Tổng số tập tin đính kèm không được vượt quá 5").show();
				return false;
			}
			var maxLength = 5 - EquipmentManagerDelivery._countFile;
			if(maxLength < 0){
				maxLength = 0;
			}
			if (arrFile.length > maxLength) {
				$('#errMsgInfo').html("Số tập tin thêm mới không được vượt quá " + maxLength).show();
				return false;
			}
			msg = UploadUtil.getMsgByFileRequireCheck(UploadUtil._typeFileRequireCheck.equipRecord);
			if (msg.trim().length > 0) {
				$('#errMsgInfo').html(msg).show();
				return false;
			}			
		}
		params.equipAttachFileStr = EquipmentManagerDelivery._arrFileDelete.join(',');
		
		var isStaff = false;
		var shopToCode = $('#shopToCode').val().trim();
		if(shopToCode != undefined && shopToCode != null && shopToCode != ''){
			 $.getJSON('/equipment-manage-delivery/getListGSNPP?toShopCode='+shopToCode, function(result) {
				 if(result.rows != undefined && result.rows != null && result.rows.length > 0) {
				 	for(var i = 0, sz = result.rows.length; i < sz; i++) {
				 		if ($('#staffMonitorCode').combobox('getValue').trim() == result.rows[i].staffCode) {
							isStaff = true;
						}
				 	}
				 }
				 if (result.rows.length == 0) {
				 	msg = 'Không tìm thấy "Đơn vị" BÊN MƯỢN hoặc Mã giám sát không quản lý "Đơn vị" BÊN MƯỢN';
					err = '#shopToCode';
					$('#errMsgInfo').html(msg).show();
					$(err).focus();
					return false;
				 } else if (!isStaff) {
					msg = 'Mã giám sát không quản lý "Đơn vị" BÊN MƯỢN';
					err = '#staffMonitorCode';
					$('#errMsgInfo').html(msg).show();
					$(err).focus();
					return false;
				} else {
					$.messager.confirm('Xác nhận', "Bạn có muốn cập nhật biên bản này ?", function(r){
						if (r){
							if (arrFile != null && arrFile != undefined && arrFile.length > 0) {
								params = JSONUtil.getSimpleObject2(params);
								UploadUtil.updateAdditionalDataForUpload(params);
								UploadUtil.startUpload('errMsgInfo');
								return false;
							}						
							// ko co chinh sua file upload
							Utils.saveData(params, '/equipment-manage-delivery/update-record-delivery', null, 'errMsgInfo', function(data){	
								if(data.error){
									$('#errMsgInfo').html(data.errMsgInfo).show();
									return false;
								}else{
									$('#successMsgInfo').html("Cập nhật dữ liệu thành công.").show();
									EquipmentManagerDelivery._lstEquipInsert = null;
									EquipmentManagerDelivery._editIndex = null;
									EquipmentManagerDelivery._lstEquipInRecord = null;
									EquipmentManagerDelivery._numberEquipInRecord = null;
									if($('#status').val() != 4){					
										setTimeout(function(){
											$('#successMsgInfo').html("").hide(); 
											window.location.href = '/equipment-manage-delivery/change-record?id='+ $("#idRecordHidden").val().trim();						
										}, 3000);
									}else{
										setTimeout(function(){
											window.location.href = '/equipment-manage-delivery/info';						              		
										}, 3000);
									}
								}
							}, null, null, null, null);
						}else{
							// if($("#idRecordHidden").val()!=''){
							// 	$('#equipmentCodeInGrid' + EquipmentManagerDelivery._editIndex).remove();
							// 	$('#seriNumberInGrid').remove();
							// 	$('#contentDelivery').remove();
							// 	$('#gridEquipmentDelivery').datagrid({url : '/equipment-manage-delivery/search-equipment-in-record',queryParams:{idRecordDelivery:$("#idRecordHidden").val()}});
							// 	EquipmentManagerDelivery._editIndex = null;
							// 	EquipmentManagerDelivery._lstEquipInRecord = null;
							// 	EquipmentManagerDelivery._lstEquipInsert = null;
							// }
						}
					});		
				}
			});
		}
		
	},
	// /**
	//  * Chinh sua bien ban
	//  * @author nhutnn
	//  * @since 18/12/2014
	//  */
	// updateRecordDelivery: function(){
	// 	$('#errMsgInfo').html('').hide();
	// 	var msg ='';
	// 	var err = '';
	// 	var nContractTrim = $('#numberContract').val().trim();
	// 	$('#numberContract').val(nContractTrim);
	// 	if(msg.length == 0 && $('#numberContract').val().indexOf(' ')!=-1){
	// 		msg = 'Số hợp đồng không được có khoảng trắng';
	// 		err = '#numberContract';
	// 	}
	// 	if(msg.length == 0){
	// 		msg = Utils.getMessageOfSpecialCharactersValidate('numberContract','Số hợp đồng',Utils._NAME_CUSTYPE);
	// 		err = '#numberContract';
	// 	}
	// 	if(msg.length == 0){
	// 		msg = Utils.getMessageOfRequireCheck('numberContract','Số hợp đồng');
	// 		err = '#numberContract';
	// 	}	
	// 	if(msg.length == 0){
	// 		msg = Utils.getMessageOfRequireCheck('contractDate','Ngày hợp đồng');
	// 		err = '#contractDate';
	// 	}				
	// 	var contractDate = $('#contractDate').val().trim();
	// 	if(msg.length == 0 && contractDate != '' && !Utils.isDate(contractDate)){
	// 		msg = 'Ngày hợp đồng không tồn tại hoặc không đúng định dạng dd/mm/yyyy.';
	// 		err = '#contractDate';
	// 	}	

	// 	if ($('#shopFromCode').val() == '' && msg.length == 0) {
	// 		msg = 'Bạn chưa nhập giá trị cho trường "Đơn vị" BÊN CHO MƯỢN';
	// 		err = '#shopFromCode';
	// 	}
	// 	if ($('#address').val() == '' && msg.length == 0) {
	// 		msg = 'Bạn chưa nhập giá trị cho trường "Địa chỉ" BÊN CHO MƯỢN';
	// 		err = '#address';
	// 	}
	// 	if ($('#taxCode').val() == '' && msg.length == 0) {
	// 		msg = 'Bạn chưa nhập giá trị cho trường "Mã số thuế" BÊN CHO MƯỢN';
	// 		err = '#taxCode';
	// 	}
	// 	if ($('#numberPhone').val() == '' && msg.length == 0) {
	// 		msg = 'Bạn chưa nhập giá trị cho trường "Điện thoại" BÊN CHO MƯỢN';
	// 		err = '#numberPhone';
	// 	}
	// 	if ($('#fromRepresentative').val() == '' && msg.length == 0) {
	// 		msg = 'Bạn chưa nhập giá trị cho trường "Người đại diện" BÊN CHO MƯỢN';
	// 		err = '#fromRepresentative';
	// 	}
	// 	if ($('#fromPosition').val() == '' && msg.length == 0) {
	// 		msg = 'Bạn chưa nhập giá trị cho trường "Chức vụ" BÊN CHO MƯỢN';
	// 		err = '#fromPosition';
	// 	}
	// 	if ($('#fromPage').val() == '' && msg.length == 0) {
	// 		msg = 'Bạn chưa nhập giá trị cho trường "Giấy ủy quyền" BÊN CHO MƯỢN';
	// 		err = '#fromPage';
	// 	}
	// 	if ($('#fromPagePlace').val() == '' && msg.length == 0) {
	// 		msg = 'Bạn chưa nhập giá trị cho trường "Nơi cấp giấy quỷ quyền" BÊN CHO MƯỢN';
	// 		err = '#fromPagePlace';
	// 	}
	// 	var fromPDate = $('#pageDate').val().trim();
	// 	if(msg.length == 0 && fromPDate != '' && !Utils.isDate(fromPDate)){
	// 		msg = 'Ngày cấp GPKD không tồn tại hoặc không đúng định dạng dd/mm/yyyy.';
	// 		err = '#pageDate';
	// 	}	
	// 	if(msg.length == 0){
	// 		msg = Utils.getMessageOfRequireCheck('pageDate','Ngày cấp giấy ủy quyền" BÊN CHO MƯỢN');
	// 		err = '#pageDate';
	// 	}

	// 	if(msg.length == 0){
	// 		msg = Utils.getMessageOfSpecialCharactersValidate('shopToCode','Mã đơn vị BÊN MƯỢN',Utils._CODE);
	// 		err = '#shopToCode';
	// 	}
	// 	if(msg.length == 0){
	// 		msg = Utils.getMessageOfRequireCheck('shopToCode','Mã đơn vị BÊN MƯỢN');
	// 		err = '#shopToCode';
	// 	}	
	// 	if(msg.length == 0){
	// 		msg = Utils.getMessageOfRequireCheck('customerCode','Mã khách hàng');
	// 		err = '#customerCode';
	// 	}	
	// 	if(msg.length == 0){
	// 		msg = Utils.getMessageOfSpecialCharactersValidate('customerCode','Mã khách hàng',Utils._CODE);
	// 		err = '#customerCode';
	// 	}
	// 	if ($('#toObjectAddress').val() == '' && msg.length == 0) {
	// 		msg = 'Bạn chưa nhập giá trị cho trường "Địa chỉ đặt tủ" BÊN MƯỢN';
	// 		err = '#toObjectAddress';
	// 	}
	// 	// if ($('#toBusinessLicense').val() == '' && msg.length == 0) {
	// 	// 	msg = 'Bạn chưa nhập giá trị cho trường "Số GP ĐKKD" BÊN MƯỢN';
	// 	// 	err = '#toBusinessLicense';
	// 	// }
	// 	// if ($('#toBusinessPlace').val() == '' && msg.length == 0) {
	// 	// 	msg = 'Bạn chưa nhập giá trị cho trường "Nơi cấp GP ĐKKD BÊN MƯỢN';
	// 	// 	err = '#toBusinessPlace';
	// 	// }
	// 	var toBusinessDate = $('#toBusinessDate').val().trim();
	// 	if(msg.length == 0 && toBusinessDate != '' && !Utils.isDate(toBusinessDate)){
	// 		msg = 'Ngày cấp GPKD không tồn tại hoặc không đúng định dạng dd/mm/yyyy.';
	// 		err = '#toBusinessDate';
	// 	}	
	// 	// if(msg.length == 0){
	// 	// 	msg = Utils.getMessageOfRequireCheck('toBusinessDate','Ngày cấp GPKD');
	// 	// 	err = '#toBusinessDate';
	// 	// }
	// 	if ($('#toRepresentative').val() == '' && msg.length == 0) {
	// 		msg = 'Bạn chưa nhập giá trị cho trường "Người đại diện" BÊN MƯỢN';
	// 		err = '#toRepresentative';
	// 	}
	// 	if ($('#toPosition').val() == '' && msg.length == 0) {
	// 		msg = 'Bạn chưa nhập giá trị cho trường "Chức vụ người đại diện" BÊN MƯỢN';
	// 		err = '#toPosition';
	// 	}
	// 	if ($('#staffMonitorCode').combobox('getValue') == '' && msg.length == 0) {
	// 		msg = 'Bạn chưa nhập giá trị cho trường "Mã giám sát" BÊN MƯỢN';
	// 		err = '#staffMonitorCode';
	// 	}
	// 	// var isStaff = false;
	// 	// for (var i = 0, size = EquipmentManagerDelivery._lstGSNPP.length; i < size; i++) {
	// 	// 	if ($('#staffMonitorCode').combobox('getValue') == EquipmentManagerDelivery._lstGSNPP.staffCode) {
	// 	// 		isStaff = true;
	// 	// 	}
	// 	// }
	// 	// if (!isStaff) {
	// 	// 	msg = 'Mã giám sát không quản lý "Đơn vị" BÊN MƯỢN';
	// 	// 	err = '#staffMonitorCode';
	// 	// }

	// 	var freezer = Utils.returnMoneyValue($('#freezer').val().trim());
	// 	var refrigerator = Utils.returnMoneyValue($("#refrigerator").val().trim());
	// 	var intFreezer = parseInt(freezer);
	// 	var intRefrigerator = parseInt(refrigerator);
	// 	if (msg.length == 0) {
	// 		if(freezer != '') {
	// 			if (intFreezer != freezer) {
	// 				msg = 'Tủ mát phải là số nguyên dương có tối đa 17 chữ số';
	// 				err = '#freezer';
	// 			} else if (intFreezer < 0  || intFreezer > 99999999999999999) {
	// 				msg = 'Tủ mát phải là số nguyên dương có tối đa 17 chữ số';
	// 				err = '#freezer';
	// 			}
	// 		} else {
	// 			msg = 'Tủ mát phải là số nguyên dương có tối đa 17 chữ số';
	// 			err = '#freezer';
	// 		}
	// 	}
	// 	if (msg.length == 0) {
	// 		if (refrigerator != '') {
	// 			if (intRefrigerator != refrigerator) {
	// 				msg = 'Tủ đông phải là số nguyên dương có tối đa 17 chữ số';
	// 				err = '#refrigerator';
	// 			} else if (intRefrigerator < 0  || intRefrigerator > 99999999999999999) {
	// 				msg = 'Tủ đông phải là số nguyên dương có tối đa 17 chữ số';
	// 				err = '#refrigerator';
	// 			}
	// 		} else {
	// 			msg = 'Tủ đông phải là số nguyên dương có tối đa 17 chữ số';
	// 				err = '#refrigerator';
	// 		}
	// 	}

	// 	if(msg.length > 0){
	// 		$('#errMsgInfo').html(msg).show();
	// 		$(err).focus();
	// 		return false;
	// 	}


		
	// 	var rowEquip = $('#gridEquipmentDelivery').datagrid('getRows');
	// 	if( rowEquip.length == 0 ){
	// 		$('#errMsgInfo').html('Không có thiết bị để cập nhật').show();
	// 		return false;
	// 	}
	// 	//var lstIdEquip = new Array();
	// 	var lstEquipCode = new Array();
	// 	var lstSeriNumber = new Array();
	// 	var lstContentDelivery = new Array();
	// 	var lstDepreciation = new Array();
		
	// 	// var ij =  EquipmentManagerDelivery._numberEquipInRecord == null ? 0: (EquipmentManagerDelivery._numberEquipInRecord);
	// 	// for (; ij < rowEquip.length-1; ij++) {
	// 	// 	lstEquipCode.push(rowEquip[ij].equipmentCode);
	// 	// 	lstSeriNumber.push(rowEquip[ij].seriNumber);
	// 	// 	lstContentDelivery.push(rowEquip[ij].contentDelivery);
	// 	// 	// if(rowEquip[ij].equipmentId != undefined){
	// 	// 	// 	lstIdEquip.push(rowEquip[ij].equipmentId);
	// 	// 	// }else{
	// 	// 	// 	lstIdEquip.push(-1);
	// 	// 	// }
	// 	// }

	// 	for (var i = 0; i < rowEquip.length; i++) {
	// 		if (rowEquip[i].seriNumber == null || rowEquip[i].depreciation == null) {
	// 			if ((rowEquip[i].equipmentId != null && rowEquip[i].equipmentCode == null) || rowEquip[i].equipmentId == undefined) {
	// 				lstEquipCode.push($('#equipmentCodeInGrid' + EquipmentManagerDelivery._editIndex).val());
	// 			} else {
	// 				lstEquipCode.push(rowEquip[i].equipmentCode);
	// 			}
	// 			if (rowEquip[i].contentDelivery != null) {
	// 				lstContentDelivery.push(rowEquip[i].contentDelivery);
	// 			} else if ($('#contentDelivery').val() != undefined) {
	// 				lstContentDelivery.push($('#contentDelivery').val());
	// 			}
	// 			if (rowEquip[i].seriNumber != null) {
	// 				lstSeriNumber.push(rowEquip[i].seriNumber);
	// 			} else {
	// 				if ($('#seriNumberInGrid' + i).val() != undefined && $('#seriNumberInGrid' + i).val() != null) {
	// 					lstSeriNumber.push($('#seriNumberInGrid' + i).val().trim());	
	// 				} else {
	// 					lstSeriNumber.push($('#seriNumberInGrid' + i).val());
	// 				}
					
	// 			}
	// 			if (rowEquip[i].depreciation != null) {
	// 				lstDepreciation.push(rowEquip[i].depreciation);
	// 			} else {
	// 				var depreciation = null;
	// 				if ($('#depreciationInGrid' + i).val() != undefined && $('#depreciationInGrid' + i).val() != null) {
	// 					depreciation = $('#depreciationInGrid' + i).val().trim();
	// 				} else {
	// 					depreciation = $('#depreciationInGrid' + i).val();
	// 				}
					
	// 				var intDepreciation = parseInt(depreciation);
	// 				if ((depreciation != null && depreciation != intDepreciation) || intDepreciation < 0) {
	// 					msg = 'Vui lòng nhập Số tháng khấu hao là số nguyên dương';
	// 					$('#errMsgInfo').html(msg).show();
	// 					err = '#depreciationInGrid' + i;
	// 					$(err).focus();
	// 					return false;
	// 				} else {
	// 					if ($('#depreciationInGrid' + i).val() != undefined && $('#depreciationInGrid' + i).val() != null) {
	// 						lstDepreciation.push($('#depreciationInGrid' + i).val().trim());
	// 					} else {
	// 						lstDepreciation.push($('#depreciationInGrid' + i).val());
	// 					}
	// 				}
	// 			}				
	// 		} else if (i >= EquipmentManagerDelivery._numberEquipInRecord) {
	// 			if ((rowEquip[i].equipmentId != null && rowEquip[i].equipmentCode == null) || rowEquip[i].equipmentId == undefined) {
	// 				lstEquipCode.push($('#equipmentCodeInGrid' + EquipmentManagerDelivery._editIndex).val());
	// 			} else {
	// 				lstEquipCode.push(rowEquip[i].equipmentCode);
	// 			}
	// 			if (rowEquip[i].contentDelivery != null) {
	// 				lstContentDelivery.push(rowEquip[i].contentDelivery);
	// 			} else if ($('#contentDelivery').val() != undefined) {
	// 				lstContentDelivery.push($('#contentDelivery').val());
	// 			}
	// 			if (rowEquip[i].seriNumber == null) {
	// 				lstSeriNumber.push('');
	// 			} else {
	// 				lstSeriNumber.push(rowEquip[i].seriNumber);
	// 			}
				
	// 			lstDepreciation.push(rowEquip[i].depreciation);			
	// 		}	
	// 	}
	// 	// if (ij == rowEquip.length) {
	// 	// 	for (var i = 0; i < rowEquip.length; i++) {
	// 	// 		if (rowEquip[i].seriNumber == null) {
	// 	// 			lstEquipCode.push(rowEquip[i].equipmentCode);
	// 	// 			//lstSeriNumber.push(rowEquip[i].seriNumber);
	// 	// 			lstContentDelivery.push(rowEquip[i].contentDelivery);
	// 	// 		}
	// 	// 	}
	// 	// }
		
	// 	if ($('#equipmentCodeInGrid' + EquipmentManagerDelivery._editIndex).val() != undefined && $('#equipmentCodeInGrid' + EquipmentManagerDelivery._editIndex).val() != '') {
	// 		var rowEq = $('#gridEquipmentDelivery').datagrid('getRows');
	// 		for (var i=0; i < rowEq.length-1; i++){
	// 			if (rowEq[i].equipmentCode == $('#equipmentCodeInGrid' + EquipmentManagerDelivery._editIndex).val().trim()) {
	// 				$('#errMsgInfo').html("Mã thiết bị "+rowEq[i].equipmentCode+" đã tồn tại!").show();
	// 				return false;
	// 			}
	// 			if ($('#depreciationInGrid' + i).val() == '' && $('#depreciationInGrid' + i) != undefined) {
	// 				$('#errMsgInfo').html('Bạn chưa xử lý xong dòng dữ liệu hiện tại!').show();
	// 				return false;
	// 			}
	// 		}
	// 		for (var i=0; i < rowEq.length; i++){
	// 			if ($('#depreciationInGrid' + i).val() == '' && $('#depreciationInGrid' + i) != undefined) {
	// 				$('#errMsgInfo').html('Bạn chưa xử lý xong dòng dữ liệu hiện tại!').show();
	// 				return false;
	// 			}
	// 		}
	// 		// if (rowEq.length > 1) {
	// 		// 	lstEquipCode.push($('#equipmentCodeInGrid' + EquipmentManagerDelivery._editIndex).val());
	// 		// }
			
	// 	} else if ($('#equipmentCodeInGrid' + EquipmentManagerDelivery._editIndex).val() == '') {
	// 		$('#errMsgInfo').html('Bạn chưa xử lý xong dòng thiết bị hiện tại!').show();
	// 		return false;
	// 	} else if (EquipmentManagerDelivery._numberEquipInRecord == null || 
	// 		rowEquip.length-1 >= EquipmentManagerDelivery._numberEquipInRecord) {
	// 			// lstEquipCode.push(rowEquip[rowEquip.length-1].equipmentCode);
	// 	}




	// 	// if($('#seriNumberInGrid').val() != undefined && $('#seriNumberInGrid').val() != ''){
	// 	// 	lstSeriNumber.push($('#seriNumberInGrid').val());
	// 	// }else if($('#seriNumberInGrid').val() == ''){
	// 	// 	// $('#errMsgInfo').html('Bạn chưa xử lý xong dòng thiết bị hiện tại!').show();
	// 	// 	// return false;
	// 	// }else{
	// 	// 	if(EquipmentManagerDelivery._numberEquipInRecord == null || rowEquip.length-1 >= EquipmentManagerDelivery._numberEquipInRecord){
	// 	// 		lstSeriNumber.push(rowEquip[rowEquip.length-1].seriNumber);
	// 	// 	}
	// 	// }

	// 	if($('#contentDelivery').val() != undefined){
	// 		// lstContentDelivery.push($('#contentDelivery').val());
	// 	} else if (EquipmentManagerDelivery._numberEquipInRecord == null || rowEquip.length-1 >= EquipmentManagerDelivery._numberEquipInRecord){
	// 			lstContentDelivery.push(rowEquip[rowEquip.length-1].contentDelivery);
	// 	}
	// 	// if(rowEquip[rowEquip.length-1].equipmentId != undefined){
	// 	// 	lstIdEquip.push(rowEquip[rowEquip.length-1].equipmentId);
	// 	// }else{
	// 	// 	lstIdEquip.push(-1);
	// 	// }
		
	// 	var params=new Object(); 		
	// 	params.numberContract = $('#numberContract').val().trim();
	// 	params.contractDate = $('#contractDate').val().trim();
	// 	params.statusRecord = $('#status').val().trim();
	// 	params.statusDelivery = $('#statusDelivery').val().trim();
	// 	params.idRecordDelivery = $("#idRecordHidden").val().trim(); //khong can check phan quyen
	// 	params.fromObjectAddress = $("#address").val().trim();
	// 	params.fromObjectTax  = $("#taxCode").val().trim();
	// 	params.fromObjectPhone  = $("#numberPhone").val().trim();
	// 	params.fromRepresentative = $("#fromRepresentative").val().trim();
	// 	params.fromObjectPosition = $("#fromPosition").val().trim();
	// 	params.fromPage = $("#fromPage").val().trim();
	// 	params.fromPagePlace = $("#fromPagePlace").val().trim();
	// 	params.fromPageDate = $("#pageDate").val().trim();
	// 	params.fromFax = $('#fromFax').val();
	// 	params.toObjectAddress = $("#toObjectAddress").val().trim();
	// 	params.toRepresentative = $("#toRepresentative").val().trim();
	// 	params.toObjectPosition = $("#toPosition").val().trim();
	// 	params.toIdNO = $("#toIdNO").val().trim();
	// 	params.toIdNODate = $("#idNODate").val().trim();
	// 	params.toIdNOPlace = $("#toIdNOPlace").val().trim();
	// 	params.freezer = Utils.returnMoneyValue($('#freezer').val().trim());
	// 	params.refrigerator = Utils.returnMoneyValue($("#refrigerator").val().trim());
	// 	params.fromShopCode = $("#shopFromCode").val().trim();
	// 	params.toShopCode = $("#shopToCode").val().trim();
	// 	params.toBusinessLicense = $('#toBusinessLicense').val().trim();
	// 	params.toBusinessDate = $('#toBusinessDate').val().trim();
	// 	params.toBusinessPlace = $('#toBusinessPlace').val().trim();

	// 	var toShopCodeHidden = $("#fromShopHidden").val().trim();

	// 	if (EquipmentManagerDelivery._customerId == null) {
	// 		if (toShopCodeHidden == $("#shopToCode").val().trim()) {
	// 			params.customerId = $("#customerIdHidden").val().trim();
	// 		}
	// 	} else {
	// 		params.customerId = EquipmentManagerDelivery._customerId;
	// 	}
	// 	params.staffCode = $('#staffMonitorCode').combobox('getValue');

	// 	params.id = params.idRecordDelivery;
	// 	// params.lstIdEquip = lstIdEquip;
	// 	if(lstEquipCode.length > 0){
	// 		params.lstEquipCode = lstEquipCode;
	// 		params.lstSeriNumber = lstSeriNumber;
	// 		params.lstContentDelivery = lstContentDelivery;	
	// 		params.lstDepreciation = lstDepreciation;
	// 	}		
		
	// 	if (params.statusRecord == 2 && lstSeriNumber.length > 0) {
	// 		for (var i = 0; i < lstSeriNumber.length; i++) {
	// 			if (lstSeriNumber[i] == null || lstSeriNumber[i] == '') {
	// 				$('#errMsgInfo').html("Vui lòng nhập đầy đủ thông tin seri khi chọn trạng thái Duyệt").show();
	// 				return false;
	// 			}
	// 		}
	// 	}

	// 	if($("#idRecordHidden").val() != "" && EquipmentManagerDelivery._lstEquipInRecord != null){
	// 		params.lstEquipDelete = "";
	// 		for(var i=0; i<EquipmentManagerDelivery._lstEquipInRecord.length; i++){
	// 			params.lstEquipDelete+=EquipmentManagerDelivery._lstEquipInRecord[i]+',';
	// 		}
	// 	}
		
	// 	var arrFile = DropzonePlugin._dropzoneObject.getFilesWithStatus(Dropzone.ADDED);
	// 	if (EquipmentManagerDelivery._arrFileDelete == undefined || EquipmentManagerDelivery._arrFileDelete == null) {
	// 		EquipmentManagerDelivery._arrFileDelete = [];
	// 	}
	// 	if (arrFile != null && arrFile != undefined && arrFile.length > 0) {
	// 		if (EquipmentManagerDelivery._countFile == 5) {
	// 			$('#errMsgInfo').html("Tổng số tập tin đính kèm không được vượt quá 5").show();
	// 			return false;
	// 		}
	// 		var maxLength = 5 - EquipmentManagerDelivery._countFile;
	// 		if(maxLength < 0){
	// 			maxLength = 0;
	// 		}
	// 		if (arrFile.length > maxLength) {
	// 			$('#errMsgInfo').html("Số tập tin thêm mới không được vượt quá " + maxLength).show();
	// 			return false;
	// 		}
	// 		msg = UploadUtil.getMsgByFileRequireCheck(UploadUtil._typeFileRequireCheck.equipRecord);
	// 		if (msg.trim().length > 0) {
	// 			$('#errMsgInfo').html(msg).show();
	// 			return false;
	// 		}			
	// 	}
	// 	params.equipAttachFileStr = EquipmentManagerDelivery._arrFileDelete.join(',');
		
	// 	var isStaff = false;
	// 	var shopToCode = $('#shopToCode').val().trim();
	// 	if(shopToCode != undefined && shopToCode != null && shopToCode != ''){
	// 		 $.getJSON('/equipment-manage-delivery/getListGSNPP?toShopCode='+shopToCode, function(result) {
	// 			 if(result.rows != undefined && result.rows != null && result.rows.length > 0) {
	// 			 	for(var i = 0, sz = result.rows.length; i < sz; i++) {
	// 			 		if ($('#staffMonitorCode').combobox('getValue').trim() == result.rows[i].staffCode) {
	// 						isStaff = true;
	// 					}
	// 			 	}
	// 			 }
	// 			 if (result.rows.length == 0) {
	// 			 	msg = 'Không tìm thấy "Đơn vị" BÊN MƯỢN hoặc Mã giám sát không quản lý "Đơn vị" BÊN MƯỢN';
	// 				err = '#shopToCode';
	// 				$('#errMsgInfo').html(msg).show();
	// 				$(err).focus();
	// 				return false;
	// 			 } else if (!isStaff) {
	// 				msg = 'Mã giám sát không quản lý "Đơn vị" BÊN MƯỢN';
	// 				err = '#staffMonitorCode';
	// 				$('#errMsgInfo').html(msg).show();
	// 				$(err).focus();
	// 				return false;
	// 			} else {
	// 				$.messager.confirm('Xác nhận', "Bạn có muốn cập nhật biên bản này ?", function(r){
	// 					if (r){
	// 						if (arrFile != null && arrFile != undefined && arrFile.length > 0) {
	// 							params = JSONUtil.getSimpleObject2(params);
	// 							UploadUtil.updateAdditionalDataForUpload(params);
	// 							UploadUtil.startUpload('errMsgInfo');
	// 							return false;
	// 						}						
	// 						// ko co chinh sua file upload
	// 						Utils.saveData(params, '/equipment-manage-delivery/update-record-delivery', null, 'errMsgInfo', function(data){	
	// 							if(data.error){
	// 								$('#errMsgInfo').html(data.errMsgInfo).show();
	// 								return false;
	// 							}else{
	// 								$('#successMsgInfo').html("Cập nhật dữ liệu thành công.").show();
	// 								EquipmentManagerDelivery._lstEquipInsert = null;
	// 								EquipmentManagerDelivery._editIndex = null;
	// 								EquipmentManagerDelivery._lstEquipInRecord = null;
	// 								EquipmentManagerDelivery._numberEquipInRecord = null;
	// 								if($('#status').val() != 4){					
	// 									setTimeout(function(){
	// 										$('#successMsgInfo').html("").hide(); 
	// 										window.location.href = '/equipment-manage-delivery/change-record?id='+ $("#idRecordHidden").val().trim();						
	// 									}, 3000);
	// 								}else{
	// 									setTimeout(function(){
	// 										window.location.href = '/equipment-manage-delivery/info';						              		
	// 									}, 3000);
	// 								}
	// 							}
	// 						}, null, null, null, null);
	// 					}else{
	// 						// if($("#idRecordHidden").val()!=''){
	// 						// 	$('#equipmentCodeInGrid' + EquipmentManagerDelivery._editIndex).remove();
	// 						// 	$('#seriNumberInGrid').remove();
	// 						// 	$('#contentDelivery').remove();
	// 						// 	$('#gridEquipmentDelivery').datagrid({url : '/equipment-manage-delivery/search-equipment-in-record',queryParams:{idRecordDelivery:$("#idRecordHidden").val()}});
	// 						// 	EquipmentManagerDelivery._editIndex = null;
	// 						// 	EquipmentManagerDelivery._lstEquipInRecord = null;
	// 						// 	EquipmentManagerDelivery._lstEquipInsert = null;
	// 						// }
	// 					}
	// 				});		
	// 			}
	// 		});
	// 	}




	// 	//@tamvnm: bo ko check ky khi luu. @since: 22/05/2015
	// 	// Utils.getJSONDataByAjax(params, '/equipment-manage-delivery/check-equip-period', function(dt){						
	// 	// 	if(dt.error){
	// 	// 		$('#errMsgInfo').html(dt.errMsgInfo).show();
	// 	// 		return false;
	// 	// 	}else{
	// 	// 		var isStaff = false;
	// 	// 		var shopToCode = $('#shopToCode').val().trim();
	// 	// 		if(shopToCode != undefined && shopToCode != null && shopToCode != ''){
	// 	// 			 $.getJSON('/equipment-manage-delivery/getListGSNPP?toShopCode='+shopToCode, function(result) {
	// 	// 				 if(result.rows != undefined && result.rows != null && result.rows.length > 0) {
	// 	// 				 	for(var i = 0, sz = result.rows.length; i < sz; i++) {
	// 	// 				 		if ($('#staffMonitorCode').combobox('getValue').trim() == result.rows[i].staffCode) {
	// 	// 							isStaff = true;
	// 	// 						}
	// 	// 				 	}
	// 	// 				 }
	// 	// 				 if (!isStaff) {
	// 	// 					msg = 'Mã giám sát không quản lý "Đơn vị" BÊN MƯỢN';
	// 	// 					err = '#staffMonitorCode';
	// 	// 					$('#errMsgInfo').html(msg).show();
	// 	// 					$(err).focus();
	// 	// 					return false;
	// 	// 				} else {
	// 	// 					$.messager.confirm('Xác nhận', "Bạn có muốn cập nhật biên bản này ?", function(r){
	// 	// 						if (r){
	// 	// 							if (arrFile != null && arrFile != undefined && arrFile.length > 0) {
	// 	// 								params = JSONUtil.getSimpleObject2(params);
	// 	// 								UploadUtil.updateAdditionalDataForUpload(params);
	// 	// 								UploadUtil.startUpload('errMsgInfo');
	// 	// 								return false;
	// 	// 							}						
	// 	// 							// ko co chinh sua file upload
	// 	// 							Utils.saveData(params, '/equipment-manage-delivery/update-record-delivery', null, 'errMsgInfo', function(data){	
	// 	// 								if(data.error){
	// 	// 									$('#errMsgInfo').html(data.errMsgInfo).show();
	// 	// 									return false;
	// 	// 								}else{
	// 	// 									$('#successMsgInfo').html("Cập nhật dữ liệu thành công.").show();
	// 	// 									EquipmentManagerDelivery._lstEquipInsert = null;
	// 	// 									EquipmentManagerDelivery._editIndex = null;
	// 	// 									EquipmentManagerDelivery._lstEquipInRecord = null;
	// 	// 									EquipmentManagerDelivery._numberEquipInRecord = null;
	// 	// 									if($('#status').val() != 4){					
	// 	// 										setTimeout(function(){
	// 	// 											$('#successMsgInfo').html("").hide(); 
	// 	// 											window.location.href = '/equipment-manage-delivery/change-record?id='+ $("#idRecordHidden").val().trim();						
	// 	// 										}, 3000);
	// 	// 									}else{
	// 	// 										setTimeout(function(){
	// 	// 											window.location.href = '/equipment-manage-delivery/info';						              		
	// 	// 										}, 3000);
	// 	// 									}
	// 	// 								}
	// 	// 							}, null, null, null, null);
	// 	// 						}else{
	// 	// 							if($("#idRecordHidden").val()!=''){
	// 	// 								$('#equipmentCodeInGrid' + EquipmentManagerDelivery._editIndex).remove();
	// 	// 								$('#seriNumberInGrid').remove();
	// 	// 								$('#contentDelivery').remove();
	// 	// 								$('#gridEquipmentDelivery').datagrid({url : '/equipment-manage-delivery/search-equipment-in-record',queryParams:{idRecordDelivery:$("#idRecordHidden").val()}});
	// 	// 								EquipmentManagerDelivery._editIndex = null;
	// 	// 								EquipmentManagerDelivery._lstEquipInRecord = null;
	// 	// 								EquipmentManagerDelivery._lstEquipInsert = null;
	// 	// 							}
	// 	// 						}
	// 	// 					});		
	// 	// 				}
	// 	// 		});
	// 	// 	}
	// 	// }
	// 	// },null,null);
		
	// },
	/**
	 * thay doi combobox
	 * @author nhutnn
	 * @since 18/12/2014
	 */
	changeCombo: function() {
	    var status = $('#statusRecordLabel').val().trim();
	    var html = '';
	    if(status == '4'){
	    	html = '<option value="" selected="selected">Chọn</option>';
	    }else {
	    	html = '<option value="" selected="selected">Chọn</option> <option value="1" >Đã gửi</option> <option value="2" >Đã nhận</option> ';
	    }
	    $('#statusDeliveryLabel').html(html);
	    $('#statusDeliveryLabel').change();
	},
	/**
	 * thay doi combobox
	 * @author nhutnn
	 * @since 18/12/2014
	 */
	changeComboUpdate: function() {
	    var status = $('#status').val().trim();
	    var html = '';
	    if(status == '2'){
				html = '<option value="0" selected="selected">Chưa gửi</option> <option value="1" >Đã gửi</option>';
	    }else {
	    	html = '<option value="0" selected="selected">Chưa gửi</option>';
	    }
	    $('#statusDelivery').html(html);
	    $('#statusDelivery').change();
	},
	/**
	 * thay doi combobox khi tìm kiếm
	 * @author nhutnn
	 * @since 27/12/2014
	 */
	 
	changeComboSearch: function() {
	    var status = $('#statusRecord').val().trim();
	    var html = '';
	    if(status == '' || status == '2'){
	    	html = '<option value="" selected="selected">Tất cả</option><option value="0" >Chưa gửi</option> <option value="1" >Đã gửi</option> <option value="2" >Đã nhận</option> ';
	    }else if(status == '0' || status == '4'){
	    	html = '<option value="0" >Chưa gửi</option>';
	    }
	    $('#statusDelivery').html(html);
	    $('#statusDelivery').change();
	},
	/**
	 * su kien tren grid
	 * @author nhutnn
	 * @since 18/12/2014
	 */
	editInputInGrid: function(){
		$('#equipmentCodeInGrid' + EquipmentManagerDelivery._editIndex).bind('keyup', function(e){
			if(e.keyCode==keyCode_F9){
				EquipmentManagerDelivery.showPopupSearchEquipment();
			}
		});
		$('#equipmentCodeInGrid' + EquipmentManagerDelivery._editIndex).bind('keyup', function(e){
			if(e.keyCode==keyCodes.ENTER){
				EquipmentManagerDelivery.getEquipInEvent(false);
			}
		});
		$('input[id^=seriNumberInGrid]').bind('keyup', function(e){
			if(e.keyCode==keyCodes.ENTER){
				$('#errMsgInfo').html('').hide();
				var seriNumber = $(this).val().trim();
				var editSeri = $(this);
				var rowRe = $('#gridEquipmentDelivery').datagrid('getRows')[EquipmentManagerDelivery._editIndex];
				if(seriNumber != undefined && seriNumber != null && seriNumber != '' && rowRe.equipmentId == null 
					&& $('#equipmentCodeInGrid' + EquipmentManagerDelivery._editIndex).val()!=undefined && $('#equipmentCodeInGrid' + EquipmentManagerDelivery._editIndex).val()==""){
					var params = {seriNumber:seriNumber};
					if($("#idRecordHidden").val() != "" && EquipmentManagerDelivery._lstEquipInRecord != null){
						params.lstEquipDelete = "";
						for(var i=0; i<EquipmentManagerDelivery._lstEquipInRecord.length; i++){
							params.lstEquipDelete += EquipmentManagerDelivery._lstEquipInRecord[i]+',';
						}					
					}
					if(EquipmentManagerDelivery._lstEquipInsert != null){
						params.lstEquipAdd = "";
						for(var i=0; i<EquipmentManagerDelivery._lstEquipInsert.length; i++){
							params.lstEquipAdd += EquipmentManagerDelivery._lstEquipInsert[i]+',';
						}					
					}
					if ($("#equipLendCode").val() != undefined && $("#equipLendCode").val() != null && $("#equipLendCode").val() != '') {
						params.equipLendCode = $("#equipLendCode").val().trim();
					}
					params.customerId = $("#customerIdHidden").val();
					Utils.getJSONDataByAjaxNotOverlay(params, '/equipment-manage-delivery/getEquipment', function(equip){						
						if(equip != undefined && equip != null) {	
							if(equip.length == 1){
								equip[0].contentDelivery = null;
								var tempDep = null;
								if ($('#depreciationInGrid' + EquipmentManagerDelivery._editIndex) != undefined && $('#depreciationInGrid' + EquipmentManagerDelivery._editIndex).val() != '') {
									tempDep = $('#depreciationInGrid' + EquipmentManagerDelivery._editIndex).val();
								}
								$('#gridEquipmentDelivery').datagrid('updateRow',{
									index: EquipmentManagerDelivery._editIndex,	
									row: equip[0]
								});
								$('#contentDelivery').width($('#contentDelivery').parent().width());
								$('#equipmentCodeInGrid' + EquipmentManagerDelivery._editIndex).width($('#equipmentCodeInGrid' + EquipmentManagerDelivery._editIndex).parent().width());
								$(this).width($(this).parent().width());
								if ($('#depreciationInGrid' + EquipmentManagerDelivery._editIndex) != undefined && tempDep != null) {
									$('#depreciationInGrid' + EquipmentManagerDelivery._editIndex).val(tempDep);
								}

								EquipmentManagerDelivery.editInputInGrid();
							}else if(equip.length > 1){
								EquipmentManagerDelivery.showPopupSelectEquipment(equip);
							}else{
								// EquipmentManagerDelivery.showPopupSearchEquipment();
								editSeri.val('');
							}
						} else{
							editSeri.val('');
						}
						editSeri.change();
						EquipmentManagerDelivery.getEventContent();
					}, null, null);					
				}else if($('#equipmentCodeInGrid' + EquipmentManagerDelivery._editIndex).val()!=undefined && $('#equipmentCodeInGrid' + EquipmentManagerDelivery._editIndex).val()!=""){
					$('#errMsgInfo').html('').hide();
					var msg = Utils.getMessageOfSpecialCharactersValidate(editSeri.attr('id'),'Số serial',Utils._SERIAL);
					if(msg.length==0 && $(this).val().indexOf(" ") != -1 ){
						msg = "Số serial không được có khoảng trắng";
					}
					if(msg.length > 0){
						$('#errMsgInfo').html(msg).show();
						$(err).focus();
						return false;
					}
					if(EquipmentManagerDelivery._editIndex!=null 
						&& $('#gridEquipmentDelivery').datagrid('getRows')[EquipmentManagerDelivery._editIndex].equipmentId != null){
						EquipmentManagerDelivery.insertEquipmentInGrid();	
					}else{
						EquipmentManagerDelivery.getEquipInEvent(true);
					}		
				}		
			}
		});
		// $('input[id*=stockInGrid]').bind('keyup', function(e){
		// 	 var txt = 'stockInGrid';	
		// 	if(e.keyCode==keyCode_F9){
		// 		VCommonJS.showDialogSearch2({
		// 			inputs : [
		// 		        {id:'code', maxlength:50, label:'Mã đơn vị'},
		// 		        {id:'name', maxlength:250, label:'Tên đơn vị'},
		// 		    ],
		// 		    url : '/equipment-stock-change-manage/get-list-equip-stock-vo',
		// 		    columns : [[
		// 		        {field:'codeName', title:'Đơn vị', align:'left', width: 110, sortable:false, resizable:false, formatter:function(value,row,index) {
		// 		        	var nameCode = row.shopCode + '-' + row.shopName;
		// 		        	return Utils.XSSEncode(nameCode);         
		// 		        }},
		// 		        {field:'code', title:'Mã kho', align:'left', width: 200, sortable:false, resizable:false},
		// 		        {field:'choose', title:'', align:'center', width:40, sortable:false, resizable:false, formatter:function(value,row,index) {
		// 		        	return '<a href="javascript:void(0)" onclick="Utils.fillCodeForInputTextByDialogSearch2(\''+txt+'\', ' + row.equipStockId +', \''+row.code+'\', \''+row.name+'\','+Utils._isShowInputTextByDlSearch2.code+');">chọn</a>';            
		// 		        }}
		// 		    ]]
		// 		});
		// 	}
		// });
		$('#contentDelivery').bind('keyup', function(e){
			if(e.keyCode==keyCodes.ENTER){
				if($('#equipmentCodeInGrid' + EquipmentManagerDelivery._editIndex).val()!=undefined && $('#equipmentCodeInGrid' + EquipmentManagerDelivery._editIndex).val()!=""){
					if($('#seriNumberInGrid').val()==undefined
						||($('#seriNumberInGrid').val()!=undefined && $('#seriNumberInGrid').val()!="")){
						//EquipmentManagerDelivery.insertEquipmentInGrid();
						if(EquipmentManagerDelivery._editIndex!=null 
							&& $('#gridEquipmentDelivery').datagrid('getRows')[EquipmentManagerDelivery._editIndex].equipmentId != null){
							EquipmentManagerDelivery.insertEquipmentInGrid();	
						}else{
							EquipmentManagerDelivery.getEquipInEvent(true);
						}
					}
				}
			}
		});
	},
	/**
	 * lay thiet bi khi xu ly su kien tren grid
	 * @author nhutnn
	 * @since 29/01/2015
	 */
	getEquipInEvent: function(isNew){
		$('#errMsgInfo').html('').hide();
		var equipCode = $('#equipmentCodeInGrid' + EquipmentManagerDelivery._editIndex).val().trim();
		if(equipCode != undefined && equipCode != null && equipCode != ''){
			var content = $('#contentDelivery').val();
			var seri = $('#seriNumberInGrid').val();
			var params = {
				equipCode: equipCode,
				equipLendCode: $("#equipLendCode").val() != null ? $("#equipLendCode").val() : "",
				customerId: $("#customerIdHidden").val()
			};
			if($("#idRecordHidden").val() != "" && EquipmentManagerDelivery._lstEquipInRecord != null){
				params.lstEquipDelete = "";
				for(var i=0; i<EquipmentManagerDelivery._lstEquipInRecord.length; i++){
					params.lstEquipDelete += EquipmentManagerDelivery._lstEquipInRecord[i]+',';
				}					
			}
			if(EquipmentManagerDelivery._lstEquipInsert != null){
				params.lstEquipAdd = "";
				for(var i=0; i<EquipmentManagerDelivery._lstEquipInsert.length; i++){
					params.lstEquipAdd += EquipmentManagerDelivery._lstEquipInsert[i]+',';
				}					
			}
			Utils.getJSONDataByAjaxNotOverlay(params, '/equipment-manage-delivery/getEquipment', function(equip){						
				if(equip != undefined && equip != null && equip.length == 1) {
					equip[0].contentDelivery = null;
					var tempDep = null;
					if ($('#depreciationInGrid' + EquipmentManagerDelivery._editIndex) != undefined && $('#depreciationInGrid' + EquipmentManagerDelivery._editIndex).val() != '') {
						tempDep = $('#depreciationInGrid' + EquipmentManagerDelivery._editIndex).val();
					}
					$('#gridEquipmentDelivery').datagrid('updateRow',{
						index: EquipmentManagerDelivery._editIndex,	
						row: equip[0]
					});
					$('#contentDelivery').val(content).width($('#contentDelivery').parent().width());
					$('#equipmentCodeInGrid' + EquipmentManagerDelivery._editIndex).width($('#equipmentCodeInGrid' + EquipmentManagerDelivery._editIndex).parent().width());
					$('#seriNumberInGrid').width($('#seriNumberInGrid').parent().width());					
					EquipmentManagerDelivery.editInputInGrid();
					if($('#seriNumberInGrid').val() == ""){
						$('#seriNumberInGrid').val(seri);
					}
					if ($('#depreciationInGrid' + EquipmentManagerDelivery._editIndex) != undefined && tempDep != null) {
						$('#depreciationInGrid' + EquipmentManagerDelivery._editIndex).val(tempDep);
					}
					if(isNew!= null && isNew && $('#seriNumberInGrid').val() != ""){
						EquipmentManagerDelivery.insertEquipmentInGrid();	
					}
				} else{
					$('#errMsgInfo').html("Mã thiết bị "+$('#equipmentCodeInGrid' + EquipmentManagerDelivery._editIndex).val()+" không có trên hệ thống hoặc không thuộc quyền kho quản lý!").show();
					$('#gridEquipmentDelivery').datagrid('deleteRow',EquipmentManagerDelivery._editIndex);
					EquipmentManagerDelivery.addEquipmentInRecord();
					$('#equipmentCodeInGrid' + EquipmentManagerDelivery._editIndex).val('');
					$('#equipmentCodeInGrid' + EquipmentManagerDelivery._editIndex).focus();
				}
				$('#equipmentCodeInGrid' + EquipmentManagerDelivery._editIndex).change();	
				EquipmentManagerDelivery.getEventContent();
			}, null, null);
		}	
	},
	/**
	 * them thiet bi vao bien ban
	 * @author nhutnn
	 * @since 30/01/2015
	 */
	addEquipmentInRecord: function(){
		if(EquipmentManagerDelivery._editIndex!=null){
			var equipCode = null;
			if ($('#equipmentCodeInGrid' + EquipmentManagerDelivery._editIndex).val() != undefined) {
				equipCode = $('#equipmentCodeInGrid' + EquipmentManagerDelivery._editIndex).val().trim();
			}
			if(equipCode != undefined && equipCode != null && equipCode != ''){
				if(EquipmentManagerDelivery._editIndex!=null 
					&& $('#gridEquipmentDelivery').datagrid('getRows')[EquipmentManagerDelivery._editIndex].equipmentId != null){
					if ($('#depreciationInGrid' + EquipmentManagerDelivery._editIndex) != undefined 
						&& $('#depreciationInGrid' + EquipmentManagerDelivery._editIndex).val() != '') {
						var meg = Utils.getMessageOfInvaildInteger('depreciationInGrid' + EquipmentManagerDelivery._editIndex, 'Số tháng khấu hao');
						if (meg.length > 0) {
							$('#errMsgInfo').html(meg).show();
							$('#depreciationInGrid' + EquipmentManagerDelivery._editIndex).focus();
							return;	
						}
					}
					EquipmentManagerDelivery.insertEquipmentInGrid();	
				}else{
					EquipmentManagerDelivery.getEquipInEvent(true);
				}	
			}else{			
				// $('#errMsgInfo').html("Xin nhập mã thiết bị tại dòng xử lý hiện tại!").show();
				// $('#equipmentCodeInGrid' + EquipmentManagerDelivery._editIndex).focus();
				// return;	
				EquipmentManagerDelivery.insertEquipmentInGrid();				
			}	
		}else{
			EquipmentManagerDelivery.insertEquipmentInGrid();	
		}			
	},
	/**
	 * Xoa tap tin dinh kem
	 * 
	 * @author nhutnn
	 * @since 10/02/2015
	 * */
	removeEquipAttachFile: function(id){
		if (id == undefined || id == null || isNaN(id)) {
			return;
		}
		if (EquipmentManagerDelivery._arrFileDelete == null || EquipmentManagerDelivery._arrFileDelete == undefined) {
			EquipmentManagerDelivery._arrFileDelete = [];
		}
		EquipmentManagerDelivery._arrFileDelete.push(id);
		$('#divEquipAttachFile'+id).remove();
		if (EquipmentManagerDelivery._countFile > 0) {
			EquipmentManagerDelivery._countFile = EquipmentManagerDelivery._countFile - 1;
		}
	},

	selectStock: function (stockId, shortCode ) {
		if (shortCode != null && shortCode != undefined && stockId != null && stockId != undefined) {
			$.messager.confirm('Xác nhận', 'Bạn quyết định chọn kho ?', function(r){
					if (r){
						$('#stockInGrid').html(shortCode);
						$('#common-dialog-search-2-textbox').dialog('close');
						$('#common-dialog-search-2-textbox').remove();
					}
				});	
		}
	},
	disableField: function() {
		if ($("#statusRecordHidden").val() == EquipmentManagerDelivery._approved) { // da duyet
			disableDateTimePicker('createDate');
			disabled('numberContract');
			disableDateTimePicker('contractDate');
			disableSelectbox('statusDelivery');
			disabled('shopFromCode');
			disabled('address');
			disabled('taxCode');
			disabled('numberPhone');
			disabled('fromRepresentative');
			disabled('fromPosition');
			disabled('fromPage');
			disabled('fromPagePlace');
			disableDateTimePicker('pageDate');
			disabled('fromFax');
			disabled('shopToCode');
			disabled('customerCode');
			disabled('toPermanentAddress');
			disabled('toBusinessLicense');
			disabled('toBusinessPlace');
			disableDateTimePicker('toBusinessDate');
			disabled('toRepresentative');
			disabled('toPosition');
			disabled('toIdNO');
			disabled('toIdNOPlace');
			disableDateTimePicker('idNODate');
			disabled('cbxStaffCode');
			disabled('freezer');
			disabled('refrigerator');
			disabled('note');
			// disabled('imgAddFile');
			$('img[id^=imgAddFile]').hide();
			$('a[id^=imgDelete]').hide();
			/** Cho phep cap nhat Giam sat */
			//disableCombo('staffMonitorCode');
			//disabled('btnUpdate');
			//tach dia chi dat tu
			disabled('addressName');
			disabled('street');
			disabled('provinceName');
			disabled('districtName');
			disabled('wardName');
			// $('img[id=imgFolder]').unbind('click');
			// $('img[id=imgFolder]').bind('click',function(){
			// 	var recordId = $('#idRecordHidden').val();
			// 	var objectId = $(this).attr("alt");
			// 	General.downloadEquipAttachFileProcess(objectId, recordId);
			// });

		}
		if ($('#shopToCode').is('[disabled=disabled]') == false && $('#idRecordHidden').val() != '') {
			disabled('shopToCode');
			disabled('customerCode');
		}
	},
	initUploadFile: function(urlUpdate) {
		if ($('#imgFolder').length == 0 || $('#imgFolder').is('[disabled=disabled]') == false) {
			UploadUtil.initUploadFileByEquipment({
					url: urlUpdate,	
					elementSelector: 'body',
					elementId: 'body',
					clickableElement: '.addFileBtn',
					parallelUploads: 5
					//maxFilesize: 5
				}, function (data) {
					if(data.error){
						$('#errMsgInfo').html(data.errMsg).show();
						return false;
					}else{
						if($("#idRecordHidden").val() == ""){
							$('#successMsgInfo').html("Cập nhật dữ liệu thành công.").show();	
							EquipmentManagerDelivery._lstEquipInsert = null;
							EquipmentManagerDelivery._editIndex = null;				
							setTimeout(function(){
								$('#successMsgInfo').html("").hide(); 
								window.location.href = '/equipment-manage-delivery/info';						              		
							}, 3000);
						}else{
							$('#successMsgInfo').html("Cập nhật dữ liệu thành công.").show();
							EquipmentManagerDelivery._lstEquipInsert = null;
							EquipmentManagerDelivery._editIndex = null;
							EquipmentManagerDelivery._lstEquipInRecord = null;
							EquipmentManagerDelivery._numberEquipInRecord = null;
							if($('#status').val() != 4){					
								setTimeout(function(){
									$('#successMsgInfo').html("").hide(); 
									window.location.href = '/equipment-manage-delivery/change-record?id='+ $("#idRecordHidden").val().trim();						
								}, 3000);
							}else{
								setTimeout(function(){
									window.location.href = '/equipment-manage-delivery/info';						              		
								}, 3000);
							}
						}
					}
				});
		} else {
			$('#imgCancel').hide();
			$('img[id=imgFolder]').unbind('click');
			$('img[id=imgFolder]').bind('click',function(){
				var recordId = $('#idRecordHidden').val();
				var objectId = $(this).attr("alt");
				General.downloadEquipAttachFileProcess(objectId, recordId);
			});


		}
		
	},
	getValueEquipGroup: function(rec) {
		var index = EquipmentManagerDelivery._editIndex;
		$('tr[datagrid-row-index=' + index +'] td[field=typeEquipment] div').text(rec.equipCategoryName);
		$('tr[datagrid-row-index=' + index +'] td[field=capacity] div').text(rec.capacityStr);
		EquipmentManagerDelivery._categoryName = rec.equipCategoryName;
		EquipmentManagerDelivery._capacity = rec.capacityStr;
	},

	/**
	* Lay danh sach kho cua Npp tu action
	*
	* @author tamvnm
	* @since Oct 21 2015
	*/
	getDataEquipStock: function(isSetStockCombo) {
		var params = new Object();
		if (EquipmentManagerDelivery._shopToCode != '') {
			params.toShopCode = EquipmentManagerDelivery._shopToCode;
			Utils.getJSONDataByAjaxNotOverlay(params, '/equipment-manage-delivery/get-equipment-stock-combobox', function(result) {
				if (result.rows != undefined && result.rows != null && result.rows.length > 0) {
					EquipmentManagerDelivery._lstEquipStock = result.rows;
				}
				if (isSetStockCombo != undefined && isSetStockCombo == EquipmentManagerDelivery._isSetStockCombo) {
					var rows = [{
						stockCode: "",
						stockName: "",
						searchText: ""
					}];
					$('#stockEquipCBX' + EquipmentManagerDelivery._editIndex).combobox("loadData", rows);
					EquipmentManagerDelivery.setEquipStock();
				}
			}, null, null);
		} else {
			EquipmentManagerDelivery._lstEquipStock = new Object();
			if (isSetStockCombo != undefined && isSetStockCombo == EquipmentManagerDelivery._isSetStockCombo) {
				var rows = [{
					stockCode: "",
					stockName: "",
					searchText: ""
				}];
				$('#stockEquipCBX' + EquipmentManagerDelivery._editIndex).combobox("loadData", rows);
				EquipmentManagerDelivery.setEquipStock();
			}
		}
	}
};

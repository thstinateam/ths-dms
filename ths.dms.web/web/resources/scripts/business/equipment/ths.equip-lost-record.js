/**
 * JS thuc hien nghiep vu Danh Sach Thiet Bi
 * 
 * Cap nhat ghi chu: vuongmq; 04/05/2015;  JS thuc hien nghiep vu Ql bao mat
 * @author hunglm16
 * @since December 17,2014
 * */
var EquipLostRecord = {
		/**Khai bao thuoc tinh Bien Toan Cuc
		 * 
		 * @author hunglm16
		 * @since December 17,214
		 * */
		_mapEquip: null, // vuongmq; 06/05/2015; _mapEquip: khong xai nua
		_lstEquip: null,
		_listEquipLostRecordDg: null,
		_mapData: null,
		_params: null,
		_equipChange: null,
		_stockIdChange: null,
		_equipIdChange: null,
		_tracingPlace: null,
		_tracingResult: null,
		_recordStatus: null,
		_deliveryStatus: null,
		_eqLostRecId: null,
		_eqLostRec: null,
		_mapEquipByLostRec: null,
		_isLevelShopRoot: null,
		_eqLostMobiRecId: null,
		_isFlagMobile: null,
		_contractNumber: '',
		_sysDateStr: '',
		_arrFileDelete: null,
		_countFile: 0,
		_rowsDetail: null,
		// vuongmq 05/05/2015
		_isFlaglostRecord: null,
		_phoneShop: null,
		_addressShop: null,
		_msgGiaTriConlai: null,
		_TAOMOI: 0,
		_DUYET: 1,
		_FLAG_CHANGE_PHE_DUYET: null, // vuongmq; 15/05/2015; trang chinh sua phe duyet bao mat != null

		showDialogUploadImages : function() {
			$('#popup-images-upload').dialog({
				title : 'Upload Images',
				width : 750,
				height : 400,
				closed : false,
				cache : false,
				modal : true,
				onOpen : function() {
					$('.ErrorMsgStyle').html('').hide();
					$('.SuccessMsgStyle').html('').hide();
					UploadUtil.clearAllAddUploadedFile(); /** open lai popup thi remove het tat ca cac file*/
//					$("#fileQueue").html('');
//					$('#programId').focus();
//					$('#ndpId').html('').change();
//					$("#ndpId").removeAttr('disabled');
//					$('#errMsgUpload').hide();
//					$("#programId").val("-1").change();
//					$(window).bind('keyup',function(event){
//						if(event.keyCode == 13){
//							$('#btnUpload').click();
//						}
//					});
				},
				onClose : function() {
				}
			});
		},
		uploadImageFile: function() {
			$('.ErrorMsgStyle').html('').hide();
			$('.SuccessMsgStyle').html('').hide();
			var errView = 'errMsgUpload';
			EquipLostRecord.updateAdditionalDataForUpload();
			UploadUtil.startUpload(errView);
		},
		updateAdditionalDataForUpload: function() {
			var programId = $('#programId').val();
			var ndpId = $('#ndpId').val();
			
			var data = JSONUtil.getSimpleObject2({
				programId: programId,
				ndpId: ndpId,
			});
			UploadUtil.updateAdditionalDataForUpload(data);
		},
		/**
		* Khoi tao tham so cho Grid Equipment
		* 
		* @author hunglm16
		* @since January 06,2014
		* */
		getQueryParamsDetail: function () {
			var params = {};
			if (EquipLostRecord._stockIdChange != null && Number(EquipLostRecord._stockIdChange) > 0) {
				params.stockId = Number(EquipLostRecord._stockIdChange);
			}
			if (EquipLostRecord._equipIdChange != null && Number(EquipLostRecord._equipIdChange) > 0) {
				params.equipId = Number(EquipLostRecord._equipIdChange);
			}
			params.flagPagination = false;
			return params;
		},
		
		/**
		* Luu vet cac tham so tim kiem
		* 
		* @author hunglm16
		* @since December 06, 2015
		* */
		createParamanterBySearch: function(taoMoi) {
			EquipLostRecord._params = {
					code: $('#txtCode').val(),
					serial: $('#txtSerial').val(),
					fromDateStr: $('#txtFromDate').val(),
					toDateStr: $('#txtToDate').val(),
					equipCode: $('#txtEquipCode').val(),
					//shortCode: $('#txtShortCode').val(),
					//customerName: $('#txtCustomerName').val(), 
					//address: $('#txtAddress').val(),
					customerSearch: $('#customerSearch').val(),
					//stockType: 3,
					stockType: Number($('#cboStockType').val()), // vuongmq; 25/04/2015
					recordStatus : Number($('#ddlStatusEquipRecord').val()),
					// deliveryStatus : Number($('#ddlDeliveryStatus').val()), /*** 15/05/2015; tim kiem tao moi co trang thai giao nhan */
			};
			 /*** 15/05/2015; tim kiem tao moi ben chuc năng QL bao mat co trang thai giao nhan; nguoc lai phe duyet bao mat thi khong co */
			if (taoMoi != undefined && taoMoi != null && taoMoi == EquipLostRecord._TAOMOI) {
				EquipLostRecord._params.deliveryStatus = Number($('#ddlDeliveryStatus').val());
			}
			if (taoMoi != undefined && taoMoi != null && taoMoi == EquipLostRecord._DUYET) {
				 EquipLostRecord._params.flagRecordStatus = EquipLostRecord._DUYET;
			}
			var multiShop = $("#txtShop").data("kendoMultiSelect");
			var dataShop = null;
			if (multiShop != null) {
				dataShop = multiShop.dataItems();
			}
			var arrShopId = [];
			if (dataShop != null) {
				for (var i=0, size = dataShop.length; i < size; i++) {
					arrShopId.push(dataShop[i].id);
				}
			}
			EquipLostRecord._params.arrShopTxt = arrShopId.join(',');
		},
		
		/**
		 * Tim kiem danh sach bien ban bao mat
		 * 
		 * @author hunglm16
		 * @since December 06, 2015
		 * */
		searchListEquipLostRecord: function (taoMoi) {
			$('.SuccessMsgStyle').html('').hide();
			$('.ErrorMsgStyle').html('').hide();
			var msg = '';
			var err = '';
			var multiShop = $("#txtShop").data("kendoMultiSelect");
			var dataShop = null;
			if (multiShop != null) {
				dataShop = multiShop.dataItems();
			}
			if (msg.length == 0 && dataShop == null || (dataShop != null && dataShop.length <= 0)) {
				msg = 'Vui lòng nhập Đơn vị';
				err = '#txtShop';
			}
			if (msg.length == 0) {
				msg = Utils.getMessageOfInvalidFormatDate("txtFromDate", "Từ ngày");
				err = "#txtFromDate";
			}
			if (msg.length == 0) {
				msg = Utils.getMessageOfInvalidFormatDate("txtToDate", "Đến ngày");
				err = "#txtToDate";
			}
			var fDate = $('#txtFromDate').val();
			var tDate = $('#txtToDate').val();
			if (msg.length == 0 && fDate.length > 0 && tDate.length > 0 && !Utils.compareDate(fDate, tDate)) {
				msg = 'Từ ngày phải là ngày trước hoặc cùng ngày với đến ngày';
				err = '#txtFromDate';
			}
			if (msg.length > 0) {
				$('#errMsg').html(msg).show();
				$(err).focus();
				setTimeout(function(){$('#errMsg').hide();},1500);
				return false;
			}
			EquipLostRecord.createParamanterBySearch(taoMoi);
			
			$('#lostRecordDg').datagrid("load", EquipLostRecord._params);
		},
		
		/**
		 * Xuat Excel Theo Tim kiem bien ban bao mat
		 * 
		 * @author hunglm16
		 * @since January 01,214
		 * */
		exportBySearchEquipLostRecord: function() {
			$('.SuccessMsgStyle').html('').hide();
			$('.ErrorMsgStyle').html('').hide();
			var msg = '';
			if(EquipLostRecord._params != null){
				var rows = $('#lostRecordDg').datagrid("getData");
				if(rows == null || rows.length==0){
					msg = "Không có dữ liệu xuất Excel";
				}
			}else{
				msg = "Không có dữ liệu xuất Excel";
			}
			if(msg.length > 0){
				$('#errMsg').html(msg).show();
				return false;
			}
			var msg = 'Bạn có muốn xuất Excel?';
			$.messager.confirm('Xác nhận', msg, function(r){
				if (r){
					ReportUtils.exportReport('/equipLostRecord/exportBySearchEquipLostRecordVO', EquipLostRecord._params, 'errMsg');
				}
			});
		},
		/**
		 * Thay doi cap nhat gia tri Bien Ban Bao Mat theo filter
		 * Ben MH QUan ly bao mat
		 * @author hunglm16
		 * @since January 06, 2015
		 * */
		changeByListEquipLostRecord: function () {
			$('.SuccessMsgStyle').html('').hide();
			$('.ErrorMsgStyle').html('').hide();
			var msg = '';
			var recordStatus = Number($('#ddlStatusEquipRecordImport').val());
			var deliveryStatus = Number($('#ddlDeliveryStatusImport').val());
			var arrEquipLostRecord = [];
			
			var rows = $('#lostRecordDg').datagrid('getChecked');
			if (rows == undefined || rows == null || rows.length == 0) {
				msg = "Chưa có Biên bản báo mất nào được chọn";
			}
			if (msg.length == 0) {
				if ((isNaN(recordStatus) || recordStatus< 0) && (isNaN(deliveryStatus) || deliveryStatus < 0)) {
					msg = "Phải có ít nhất một trong hai trạng thái phải được chọn";
				}
			}
			if (msg.length > 0) {
				$('#errMsg').html(msg).show();
				return false;
			}
			
			for (var i= 0, size = rows.length; i< size; i++) {
				arrEquipLostRecord.push(rows[i].id);
			}
			
			//Xu ly nghiep vu
			var params = {
					recordStatus: recordStatus,
					deliveryStatus: deliveryStatus,
					arrEquipLostRecordTxt: arrEquipLostRecord.join(',').trim()
			};
			
			Utils.addOrSaveData(params, "/equipLostRecord/changeByListEquipLoadRecord", null, 'errMsg', function(data) {
				if (data.rows != null && data.rows.length > 0) {
					VCommonJS.showDialogList({
						dialogInfo: {
							title: 'Danh sách lỗi'
						},
						data: data.rows,
						columns : [[
							{field:'code', title:'Mã biên bản', align:'left', width: 110, sortable:false, resizable:false},
							{field:'recordStatus', title:'Trạng thái biên bản', align:'center', width: 110, sortable:false, resizable:false, formatter: function(value, row, index){
								if (Number($('#ddlStatusEquipRecordImport').val()) > -1) {
									return '<a href="javascript:void(0)"><img src="/resources/images/icon-error.png" width="18" heigh="18" /></a>';
								}
								return '';
							}},
							{field:'deliveryStatus', title:'Trạng thái giao nhận', align:'center', width: 110, sortable:false, resizable:false, formatter: function(value, row, index){
								if (Number($('#ddlDeliveryStatusImport').val()) > -1) {
									return '<a href="javascript:void(0)"><img src="/resources/images/icon-error.png" width="18" heigh="18" /></a>';
								}
								return '';
							}}
						]]
					});
				} else {
					//$("#successMsg").html("Lưu dữ liệu thành công").show();
					var htmlView = 'Lưu dữ liệu thành công.';
					if (data.errMsgMail != undefined && data.errMsgMail != '') {
						htmlView += '<span style="color:#f00"> Chưa gửi mail, ' + data.errMsgMail + '</span>';
					}
					$("#successMsg").html(htmlView).show();
				}
				setTimeout(function() {
					$('.SuccessMsgStyle').html("").hide();
					$('#btnSearch').click(); 
				 }, 1500);
			}, null, null, null, null, null, true);
			return false;
		},

		/**
		 * Cap nhat gia tri bien ban de phe duyet
		 * @author vuongmq
		 * @since 15/05/2015
		 * */
		changeByListEquipLostRecordApprove: function (pheDuyetChange) {
			$('.SuccessMsgStyle').html('').hide();
			$('.ErrorMsgStyle').html('').hide();
			var msg = '';
			var recordStatus = '';
			var arrEquipLostRecord = [];
			if (pheDuyetChange == undefined || pheDuyetChange == null) {
				// truong hop cap nhat o danh sach chinh
				recordStatus = Number($('#ddlStatusEquipRecordImport').val());
				var rows = $('#lostRecordDg').datagrid('getChecked');
				if (rows == undefined || rows == null || rows.length == 0) {
					msg = "Chưa có Biên bản báo mất nào được chọn";
				}
				if (msg.length == 0) {
					if ((isNaN(recordStatus) || recordStatus < 0)) {
						msg = "Vui lòng chọn trạng thái để lưu.";
					}
				}
				if (msg.length > 0) {
					$('#errMsg').html(msg).show();
					return false;
				}
				
				for (var i= 0, size = rows.length; i< size; i++) {
					arrEquipLostRecord.push(rows[i].id);
				}
			} else {
				// truong hop cap nhat o detail
				recordStatus = Number($('#ddlStatusEquipRecord').val()); // gia tri nay co
				if (msg.length == 0) {
					if (EquipLostRecord._eqLostRecId != null && EquipLostRecord._eqLostRecId.toString().trim().length > 0) {
						arrEquipLostRecord.push(Number(EquipLostRecord._eqLostRecId));
					} else {
						msg = "Vui lòng chọn lại phiếu báo mất.";
					}
				}
				if (msg.length > 0) {
					$('#errMsg').html(msg).show();
					return false;
				}
			}
			//Xu ly nghiep vu
			var params = {
					recordStatus: recordStatus,
					arrEquipLostRecordTxt: arrEquipLostRecord.join(',').trim()
			};
			if (pheDuyetChange == undefined || pheDuyetChange == null) {
				params.flagError = 0;
			} else {
				params.flagError = 1;
			}
			Utils.addOrSaveData(params, "/equipLostRecord/changeByListEquipLostRecordApprove", null, 'errMsg', function(data) {
				if (data.rows != null && data.rows.length > 0) {
					VCommonJS.showDialogList({
						dialogInfo: {
							title: 'Danh sách lỗi'
						},
						data: data.rows,
						columns : [[
						            {field:'code', title:'Mã biên bản', align:'left', width: 110, sortable:false, resizable:false},
						            {field:'recordStatus', title:'Trạng thái biên bản', align:'center', width: 110, sortable:false, resizable:false, formatter: function(value, row, index){
						            	if (Number($('#ddlStatusEquipRecordImport').val()) > -1) {
						            		return '<a href="javascript:void(0)"><img src="/resources/images/icon-error.png" width="18" heigh="18" /></a>';
						            	}
						            	return '';
						            }},
						            {field:'deliveryStatus', title:'Trạng thái giao nhận', align:'center', width: 110, sortable:false, resizable:false, formatter: function(value, row, index){
						            	if (Number($('#ddlDeliveryStatusImport').val()) > -1) {
						            		return '<a href="javascript:void(0)"><img src="/resources/images/icon-error.png" width="18" heigh="18" /></a>';
						            	}
						            	return '';
						            }}
						            ]]
					});						
				} else {
					$("#successMsg").html("Lưu dữ liệu thành công").show();
					var tm = setTimeout(function() {
						$('.SuccessMsgStyle').html("").hide();
						if (pheDuyetChange == undefined || pheDuyetChange == null) {
							// truong hop cap nhat man hinh chinh phe duyet bao mat; 
							$('#btnSearch').click(); 
						} else {
							// truong hop cap nhat o detail, chinh sua phe duyet
							window.location.href = '/equipLostRecord/approve';
						}
						clearTimeout(tm);
					}, 1500);
				}
			}, null, null, null, msg, null, true);
			return false;
		},

		/**
		 * Mo Dialog Import Bien Ban Mat
		 * 
		 * @author hunglm16
		 * @since January 06, 2015
		 * */
		openDialogImportEquipLostRecord: function (){
			$('.SuccessMsgStyle').html('').hide();
			$('.ErrorMsgStyle').html('').hide();
			$('#equipHistoryTemplateDiv').css("visibility","hidden").hide();
			$('#eqLostRec_import_easyuiPopupImportExcel').dialog({
				title: 'Nhập Excel Danh sách thiết bị',
				width: 465, 
				height: 'auto',
				closed: false,
				cache: false,
				modal: true,
				onOpen: function() {
					
				},
				onClose: function() {
					$('.ErrorMsgStyle').html('').hide();
					$('#fakefileExcelpc').val("").change();
				}
			});
		},
		
		/**
		 * changeDdlStatusEqRecImpPBrowser su kien Change
		 * 
		 * @author hunglm16
		 * @since January 04,2014
		 * */
		changeDdlStatusEqRecImpPBrowser : function (cbx) {
			var dllValue = Number($(cbx).val());
			var isLevel = Number(EquipLostRecord._isLevelShopRoot);
			if (statusRecordsEquip.ALL != dllValue) {
				$('#ddlDeliveryStatusImport').html('');
				$('#ddlDeliveryStatusImport').append('<option value="-2" selected="selected">Chọn</option>');
				$('#ddlDeliveryStatusImport').change();
				selectedDropdowlist('ddlDeliveryStatusImport', statusRecordsEquip.ALL);
			} else {
				$('#ddlDeliveryStatusImport').html('');
				$('#ddlDeliveryStatusImport').append('<option value="-2" selected="selected">Chọn</option>');
				$('#ddlDeliveryStatusImport').append('<option value="2">Đã nhận</option>');
				$('#ddlDeliveryStatusImport').change();
			}
		},
		
		/**
		 * changeDdlStatusEqRecImpPCreate su kien Change
		 * 
		 * @author hunglm16
		 * @since January 04,2014
		 * */
		/*changeDdlStatusEqRecImpPCreate : function (cbx) {
			var dllValue = Number($(cbx).val());
			var isLevel = Number(EquipLostRecord._isLevelShopRoot);
			if (statusRecordsEquip.ALL != dllValue) {
				$('#ddlDeliveryStatusImport').html('');
				$('#ddlDeliveryStatusImport').append('<option value="-2" selected="selected">Chọn</option>');
				$('#ddlDeliveryStatusImport').change();
				selectedDropdowlist('ddlDeliveryStatusImport', statusRecordsEquip.ALL);
			} else {
				$('#ddlDeliveryStatusImport').html('');
				$('#ddlDeliveryStatusImport').append('<option value="-2" selected="selected">Chọn</option>');
				$('#ddlDeliveryStatusImport').append('<option value="1" >Đã gửi</option>');
				$('#ddlDeliveryStatusImport').change();
			}
		},*/

		/**
		 * changeDdlStatusEqRecImpPCreate su kien Change
		 * khi TNKD phan quyen thi co trang thai giao nhan da gui da nha; ngc lai la ktoan: da gui
		 * @author vuongmq
		 * @since 20/05/2014
		 * */
		changeDdlStatusEqRecImpPCreate : function (cbx) {
			var dllValue = Number($(cbx).val());
			if (statusRecordsEquip.ALL != dllValue) {
				$('#ddlDeliveryStatusImport').html('');
				$('#ddlDeliveryStatusImport').append('<option value="-2" selected="selected">Chọn</option>');
				$('#ddlDeliveryStatusImport').change();
				selectedDropdowlist('ddlDeliveryStatusImport', statusRecordsEquip.ALL);
			} else {
				$('#ddlDeliveryStatusImport').html('');
				$('#ddlDeliveryStatusImport').append('<option value="-2" selected="selected">Chọn</option>');
				$('#ddlDeliveryStatusImport').append('<option value="1" >Đã gửi</option>');
				/** truong hop phan quyen co combobox da nhan; khi change thi lay them da nhan*/
				if ($('#permisionDeliverySendAndReceived') != null && $('#permisionDeliverySendAndReceived').length > 0) {
					$('#ddlDeliveryStatusImport').append('<option value="2" >Đã nhận</option>');
				}
				$('#ddlDeliveryStatusImport').change();
			}
		},
		/**
		 * changeDdlStatusEquipRecordDt su kien Change
		 * 
		 * @author hunglm16
		 * @since January 04,2014
		 * */
		changeDdlStatusEquipRecordDt : function (cbx) {
			var dllValue = Number($(cbx).val());
			if (dllValue == 0) {
				$('#ddlDeliveryStatus').html('');
				$('#ddlDeliveryStatus').append('<option value="0">Chưa gửi</option>');
				$('#ddlDeliveryStatus').change();
			} else {
				$('#ddlDeliveryStatus').html('');
				$('#ddlDeliveryStatus').append('<option value="0">Chưa gửi</option>');
				$('#ddlDeliveryStatus').append('<option value="1">Đã gửi</option>');
				$('#ddlDeliveryStatus').change();
			}
		},
		
		/**
		 * changeDdlDeliveryStatusImport su kien Change
		 * 
		 * @author hunglm16
		 * @since January 04,2014
		 * */
		changeDdlDeliveryStatusImport : function (cbx) {
			var dllValue = Number($(cbx).val());
			if (statusRecordsEquip.ALL != dllValue) {
				selectedDropdowlist('ddlStatusEquipRecordImport', statusRecordsEquip.ALL);
			} else {
				
			}
		},
		 
		 /**
		 * Dien gia tri lay shop gan vao EquipLostRecord._phoneShop; EquipLostRecord._addressShop;
		 * @author vuongmq
		 * @since 14/05/2015
		 * */
		 changetxtShopCode: function(shopCode) {
		 	// bao mat cua NPP; lay shop gan vao EquipLostRecord._phoneShop; EquipLostRecord._addressShop; ben js ham fillTextBoxByShopCode gan lai
		 	/** vuongmq; 06/06/2015; reset lai tat cac textbox */
		 	$('#txtCustomerCode').val('');
			$('#txtCustomerName').val('');
			$('#txtRepresentor').val('');
			$('#txtPhone').val('');
			$('#txtAddress').val('');
			EquipLostRecord._rowsDetail = [];
			EquipLostRecord.createDataGidEquipment();
			
			var params = new Object();
			params.shopCode = shopCode;
			Utils.getJSONDataByAjaxNotOverlay(params, '/equipLostRecord/searchShopLost',function (data) {
				if (data.rows != undefined && data.rows != '') {
					EquipLostRecord._phoneShop = data.phoneShop;
					EquipLostRecord._addressShop = data.addressShop;
					if ($('#cboStockType').val() == activeType.RUNNING) {
						// bao mat cua NPP
						$('#txtPhone').val(EquipLostRecord._phoneShop);
						$('#txtAddress').val(EquipLostRecord._addressShop);
					}
				}
			});
		 },
		/**
		 * Dien gia tri Ma Don vi vao txtShopCode
		 * 
		 * @author hunglm16
		 * @since January 05,2015
		 * */

		 /**
		 * Dien gia tri Ma Don vi vao txtShopCode
		 * @author hunglm16
		 * @since January 05,2015
		 * */
		 fillTextBoxByShopCode: function (shopCode, shopId) {
			$('#txtShopCode').val(shopCode);
			$('#txtShopCode').change();
			$('#txtShopCode').focus();
			$('#common-dialog-search-2-textbox').dialog("close");
			$('#common-dialog-search-2-textbox').remove();
		 },
		 
		 /**
		 * Dien gia tri Ma Khach Hang vao txtCustomerCode
		 * 
		 * @author hunglm16
		 * @since January 05,2015
		 * */
		 fillTextBoxByCustomerCode: function (shortCode) {
			$('#txtCustomerCode').val(shortCode);
			$('#txtCustomerCode').focus();
			$('#common-dialog-search-2-textbox').dialog("close");
			$('#common-dialog-search-2-textbox').remove();
			$('#txtCustomerCode').change();
		 },

		 /**
		 * Bat su kien Onchange cau txtCustomerCode
		 * Cap nhat lay cthong tin customer khi change textbox
		 * @author vuongmq
		 * @since 21/05/2015
		 * */
		 onchangeTxtCustomerCode: function (txt) {
			var shortCode = $(txt).val().trim();
			if (shortCode.length > 2) {
				Utils.getJSONDataByAjax({
					shortCode: shortCode,
					shopCode: $('#txtShopCode').val(),
				}, '/equipLostRecord/searchCustomerLost', function(data) {
					$('#txtCustomerName').val('');
					$('#txtPhone').val('');
					$('#txtAddress').val('');
					//EquipLostRecord._mapEquip = new Map();
					if (data.rows != undefined && data.rows != '') {
						$('#txtCustomerName').val(data.nameCustomer);
						$('#txtPhone').val(data.phoneCustomer);
						$('#txtAddress').val(data.addressCustomer);
						//$('#gridEquipment').datagrid('loadData', []);
						EquipLostRecord._rowsDetail = [];
						EquipLostRecord.searchEquipByEquipDeliRec();
					}
				}, null, null);
			} else {
				//$('#gridEquipment').datagrid('loadData', []);
				EquipLostRecord._rowsDetail = [];
				EquipLostRecord.createDataGidEquipment();
				//EquipLostRecord._mapEquip = new Map();
				$('#cusInfFormDiv input').val('');
				$('#txtCustomerCode').val(shortCode);				
			}
			$('#txtRepresentor').focus();
		 },
		 
		 /**
		 * Lay danh sach thiet bi co ban hop dong moi nhat
		 * 
		 * @author hunglm16
		 * @since January 05,2015
		 * */

		  /**
		 * Lay danh sach thiet bi co ban hop dong moi nhat; luc tao moi
		 * @author vuongmq
		 * @since 06/05,2015
		 * cap nhat: lay theo bao mat cua NPP or KH, chon thiet bi cua NPP or KH
		 * // cai nay goi khi chọn bao mat NPP chon thiet bị và khi chon KH se load len grid 1 thiet bi dau tien
		 * */
		 searchEquipByEquipDeliRec: function (idEquipment) {
		 	// cho nay xu ly chon thiet bi cua KH;
		 	//EquipLostRecord._rowsDetail = rows;
		 	var baoMat = $('#cboStockType').val();
		 	/*** 1: activeType.RUNNING la NPP; 2: activeType.WAITING la KH*/
			//EquipLostRecord._mapEquip = new Map();
			var shortCode = $('#txtCustomerCode').val().trim().toUpperCase();
			if (idEquipment == undefined || idEquipment == null) {
				idEquipment = '';
			}
			var params = {
				equipId: idEquipment, // vuongmq; 06/05/2015
				shortCode: shortCode,
				//stockType: 3,
				stockType: Number(baoMat), // vuongmq; 27/04/2015
				tradeStatus: 0,
				flagPagination: false,
				shopCode: $('#txtShopCode').val().trim(), // vuongmq; 14/05/2015
			};
			if (hdFlagLiquidationStatus != undefined && hdFlagLiquidationStatus != null) {
				params.flagUpdate = $('#hdFlagLiquidationStatus').val();
			}
			Utils.getJSONDataByAjaxNotOverlay(params,'/equipLostRecord/searchEquipByEquipDeliveryRecordNew', function(data){
					if (data.rows != undefined && data.rows.length > 0) {
						var rows = data.rows;
						/*for (var i = 0, size = rows.length; i< size; i++) {
							EquipLostRecord._mapEquip.put(rows[i].equipId, rows[i]);
						}*/
						if (idEquipment != '') {
							EquipLostRecord._rowsDetail = rows;
							$('#easyuiPopupSearchEquipment').dialog('close'); // khi nay co popup thi close
						} else {
							if (baoMat == activeType.WAITING) {
								//2: activeType.WAITING la KH
								var tmpFirst = new Array();
								tmpFirst.push(rows[0]);
								EquipLostRecord._rowsDetail = tmpFirst; // tao grid dong dau tien
							}
						}
					} else {
						//vuongmq; 09/05/2015; truong hop bat kha khang; check DB khong co: idEquipment
						if (idEquipment != null) {
							$('#easyuiPopupSearchEquipment').dialog('close'); // khi nay co popup thi close
						}
					}
					//EquipLostRecord.getDataComboboxEquipmentDg(); // vuongmq 06/05/2015; khong xai combobox thiet bi trong grid nua
					EquipLostRecord.createDataGidEquipment();
				}, null, null);
		 },
		 
		 /**
		 * Lay danh sach thiet bi co ban hop dong moi nhat
		 * 
		 * @author hunglm16
		 * @since January 05,2015
		 * */

		 /***
		 * Truong hop cap nhat bao mat se co danh sach nay
		 */
		 loadEquipByEquipLostRec: function () {
			var idEqLostRec = -1;
			var params = {};
			EquipLostRecord._mapEquipByLostRec = new Map();
			if (EquipLostRecord._eqLostRecId != undefined && EquipLostRecord._eqLostRecId != null && EquipLostRecord._eqLostRecId.toString().trim().length > 0) {
				params.eqLostRecId = Number(EquipLostRecord._eqLostRecId);
			}
			if (EquipLostRecord._equipIdChange != undefined && EquipLostRecord._equipIdChange != null && EquipLostRecord._equipIdChange.toString().trim().length > 0) {
				params.equipId = Number(EquipLostRecord._equipIdChange);
			}
			/** vuongmq; 27/05/2015; truong hop chinh sua; cho bao mat cua mobile */
			if (EquipLostRecord._isFlagMobile != undefined && EquipLostRecord._isFlagMobile != null && EquipLostRecord._isFlagMobile.toString().trim().length > 0) {
				params.flagMobile = Number(EquipLostRecord._isFlagMobile);
			}
			
			/** phai truyen shopCode cho ham nay; truong hop chinh sua */
			var shopCode = $('#txtShopCode').val().trim();
			if (shopCode.length <= 0) {
				$('#errMsgInfo').html('Vui lòng chọn Mã NPP để lấy danh sách thiết bị.').show();
				return false;
			}
			if (Number(params.eqLostRecId) > 0 || Number(params.equipId)) {
				params.shopCode = shopCode;
				params.stockType = EquipLostRecord._isFlaglostRecord; //bao mat stock Type
				if (EquipLostRecord._isFlaglostRecord == activeType.WAITING) {
					//bao mat stock Type:2  khach hang
					params.shortCode = $('#txtCustomerCode').val().trim();
				}
				Utils.getJSONDataByAjaxNotOverlay(params,'/equipLostRecord/searchEquipByEquipDeliveryRecordNew', function(data){
					if (data.rows != null && data.rows.length > 0) {
						var rows = data.rows;
						for (var i = 0, size = rows.length; i< size; i++) {
							EquipLostRecord._mapEquipByLostRec.put(rows[i].equipId, rows[i]);
						}
						//$('#gridEquipment').datagrid("loadData", rows);
						EquipLostRecord._rowsDetail = rows;
						EquipLostRecord.createDataGidEquipment();
					}
				}, null, null);
			}
		 },
		 
		 /**
		 * Them moi 2 dong trong vao Datagrid Stock Trans Detail
		 * 
		 * @author hunglm16
		 * @Since January 06, 2015
		 * */
		insertRowEquipmentDg: function () {
			var indexMax = $('#gridEquipment').datagrid('getRows').length;
			$('#gridEquipment').datagrid('insertRow',{
				index: indexMax,
				row: {
					equipId: 0,
					contractNumber: '',
					lostDateStr: '',
					equipCategoryCode: '',
					equipGroupCode: '',
					equipCode: '',
					serial: '',
					price: 0, //
					priceActually: 0, //
					quantity: 0,
					manufacturingYear: 0
				}
			});
			$('#gridEquipment').datagrid('selectRow', indexMax).datagrid('beginEdit', indexMax);
			/*var t = setTimeout(function(){
				var comboboxEquip = $('#gridEquipment').datagrid('getEditors', indexMax);
				if (comboboxEquip != undefined && comboboxEquip != null) {
					var rowsCbx = EquipLostRecord.getDataComboboxEquipmentDg();
					$(comboboxEquip[0].target).combobox("loadData", rowsCbx);
				}
				clearTimeout(t);
			 }, 70);*/
		},
		/**
		 * Lay danh sach Combobox danh sach thiet bi
		 * 
		 * @author hunglm16
		 * @Since January 06, 2015
		 * */
		 /** vuongmq; 06/05/2015; cai nay khong xai nua; */
		getDataComboboxEquipmentDg: function () {
			var rows = [];
			var arrEquipId = null;
			if (EquipLostRecord._mapEquip != null && EquipLostRecord._mapEquip.size() > 0) {
				arrEquipId = EquipLostRecord._mapEquip.keyArray;
				for (var i = 0, size = arrEquipId.length; i < size; i++) {
					var obj = EquipLostRecord._mapEquip.get(arrEquipId[i]);
					var objnew = {
							equipId: obj.equipId,
							equipCode: obj.equipCode
					};
					rows.push(obj);
				}
			}
			if (EquipLostRecord._mapEquipByLostRec != undefined && EquipLostRecord._mapEquipByLostRec != null && EquipLostRecord._mapEquipByLostRec.size() > 0) {
				var arrayKey = EquipLostRecord._mapEquipByLostRec.keyArray;
				if (arrEquipId == null) {
					arrEquipId = [];
				}
				for (var i = 0, size = arrayKey.length; i < size; i++) {
					if (arrEquipId.indexOf(arrayKey[i]) < 0) {
						var obj = EquipLostRecord._mapEquipByLostRec.get(arrayKey[i]);
						var objnew = {
								equipId: obj.equipId,
								equipCode: obj.equipCode
						};
						rows.push(obj);
					}
				}
			}
			return rows;
		},
/////////// BEGIN VUONGMQ; 04/05/2015; cap nhat lap hieu bao mat /////////////	
	/**
	 * su kien tren F9 grid
	 * @author vuongmq
	 * @since 04/05/2015
	 */
	
	editInputInGridLost: function(){
		$('#equipItemCodeInGrid').bind('keyup', function(e){
			if(e.keyCode==keyCode_F9){
				$('.SuccessMsgStyle').html('').hide();
				$('.ErrorMsgStyle').html('').hide();
				if ($('#txtShopCode').val().trim().length <= 0) { 
		 	    	setTimeout(function() {
						$('#errMsgInfo').html('Vui lòng chọn Mã NPP để lấy danh sách thiết bị.').show();
			 		}, 1500);
					return false;
				}
				if ($('#cboStockType').val() == activeType.RUNNING) {
					// bao mat NPP
					EquipLostRecord.showPopupSearchEquipmentLost();
				} else if ($('#cboStockType').val() == activeType.WAITING) {
					// bao mat KH
					if ($('#txtCustomerCode').val().trim().length > 0) { 
		 	    		EquipLostRecord.showPopupSearchEquipmentLost();
					} else {
						setTimeout(function(){
							$('#errMsgInfo').html('Vui lòng chọn khách hàng để lấy danh sách thiết bị.').show();
				 		}, 1500);
						return false;
					}
				}
			}
		});
	},

	/**
	 * Lay danh muc loai, ncc thiet bi
	 * @author nhutnn
	 * @since 20/12/2014
	 */

	 /**
	 * Lay danh muc loai, ncc thiet bi
	 * @author vuongmq
	 * @since 04/05/2015
	 * ghi chu: van lay danh muc theo nhu popup thiet bi cua sua chua
	 */
	getEquipCatalogLost: function(){
		$('#typeEquipment').html('<option value="" selected="selected">Tất cả</option>');
    	// $('#providerId').html('<option value=""  selected="selected">Tất cả</option>');
    	// $('#groupEquipment').html('<option value="" selected="selected">Tất cả</option>'); // vuongmq; 16/06/2015; reset lai
		$.getJSON('/equipment-repair-manage/get-equipment-catalog', function(result) {				
			if(result.equipsCategory != undefined && result.equipsCategory != null && result.equipsCategory.length > 0) {					
				for(var i=0; i < result.equipsCategory.length; i++){
					$('#typeEquipment').append('<option value="'+ result.equipsCategory[i].id +'">'+ Utils.XSSEncode(result.equipsCategory[i].code) +' - ' +Utils.XSSEncode(result.equipsCategory[i].name) +'</option>');  
				}
			} 
			$('#typeEquipment').change();
			// if(result.equipProviders != undefined && result.equipProviders != null && result.equipProviders.length > 0) {					
			// 	for(var i=0; i < result.equipProviders.length; i++){
			// 		$('#providerId').append('<option value="'+ result.equipProviders[i].id +'">'+ Utils.XSSEncode(result.equipProviders[i].code) +' - '+Utils.XSSEncode(result.equipProviders[i].name)+'</option>');  
			// 	}
			// } 
			// $('#providerId').change();
			if(result.equipProviders != undefined && result.equipProviders != null && result.equipProviders.length > 0) {					
				for (var i = 0, isize = result.equipProviders.length; i < isize; i++) {
					result.equipProviders[i].searchText = unicodeToEnglish(result.equipProviders[i].code + " " + result.equipProviders[i].name);
 					result.equipProviders[i].searchText = result.equipProviders[i].searchText.toUpperCase();
				}
				var obj = {
					id: -1,
					code: "Tất cả",
					name: "Tất cả",
					codeName: "Tất cả",
					searchText: unicodeToEnglish("Tất cả").toUpperCase()
				};
				result.equipProviders.splice(0, 0, obj);
				$('#providerId').combobox("loadData", result.equipProviders);
		 		$('#providerId').combobox("setValue", -1);
			}

			// if(result.lstEquipGroup != undefined && result.lstEquipGroup != null && result.lstEquipGroup.length > 0) {					
			// 	for(var i=0; i < result.lstEquipGroup.length; i++){
			// 		$('#groupEquipment').append('<option value="'+ result.lstEquipGroup[i].id +'">'+ Utils.XSSEncode(result.lstEquipGroup[i].code) +' - '+Utils.XSSEncode(result.lstEquipGroup[i].name)+'</option>');  
			// 	}
			// } 
			// $('#groupEquipment').change();				
			if(result.lstEquipGroup != undefined && result.lstEquipGroup != null && result.lstEquipGroup.length > 0) {	
				for (var i = 0, isize = result.lstEquipGroup.length; i < isize; i++) {
					result.lstEquipGroup[i].searchText = unicodeToEnglish(result.lstEquipGroup[i].code + " " + result.lstEquipGroup[i].name);
	 				result.lstEquipGroup[i].searchText = result.lstEquipGroup[i].searchText.toUpperCase();
				}				
				var obj = {
				id: -1,
				code: "Tất cả",
				name: "Tất cả",
				codeName: "Tất cả",
				searchText: unicodeToEnglish("Tất cả").toUpperCase()
				};
				result.lstEquipGroup.splice(0, 0, obj);
				$('#groupEquipment').combobox("loadData", result.lstEquipGroup);
		 		$('#groupEquipment').combobox("setValue", -1);
			} 
		});
	},

	/**
	 * Xu ly trên input loai thiet bi
	 * @author nhutnn
	 * @since 19/12/2014
	 */
	  /**
	 * Xu ly trên input loai thiet bi
	 * @author vuongmq
	 * @since 04/05/2015
	 * ghi chu: van lay change danh muc theo nhu popup thiet bi cua sua chua
	 */
	onChangeTypeEquipLost: function(cb){
		var typeEquip = $(cb).val().trim();
		// $('#groupEquipment').html('<option value="" selected="selected">Tất cả</option>');
		if(typeEquip != undefined && typeEquip != null && typeEquip != ''){
			$.getJSON('/equipment-manage-delivery/get-equipment-group?id='+typeEquip, function(result) {				
				// if(result.equipGroups != undefined && result.equipGroups != null && result.equipGroups.length > 0) {					
				// 	for(var i=0; i < result.equipGroups.length; i++){
				// 		$('#groupEquipment').append('<option value="'+ result.equipGroups[i].id +'">'+ Utils.XSSEncode(result.equipGroups[i].code) + ' - '+Utils.XSSEncode(result.equipGroups[i].name)+'</option>');  
				// 	}
				// } 
				// $('#groupEquipment').change();	
				if(result.equipGroups != undefined && result.equipGroups != null && result.equipGroups.length > 0) {	
				for (var i = 0, isize = result.equipGroups.length; i < isize; i++) {
					result.equipGroups[i].searchText = unicodeToEnglish(result.equipGroups[i].code + " " + result.equipGroups[i].name);
	 				result.equipGroups[i].searchText = result.equipGroups[i].searchText.toUpperCase();
				}				
				var obj = {
				id: -1,
				code: "Tất cả",
				name: "Tất cả",
				codeName: "Tất cả",
				searchText: unicodeToEnglish("Tất cả").toUpperCase()
				};
				result.equipGroups.splice(0, 0, obj);
				$('#groupEquipment').combobox("loadData", result.equipGroups);
		 		$('#groupEquipment').combobox("setValue", -1);
			} 
			});
		}
	},

	/**
	 * Show popup tim kiem thiet bi
	 * @author vuongmq
	 * @since 04/05/2015
	 */
	showPopupSearchEquipmentLost: function() {
		$('#easyuiPopupSearchEquipment').show();
		$('#easyuiPopupSearchEquipment').dialog({  
			closed: false,  
			cache: false,  
			modal: true,
			width : 1000,
	//	        height : 'auto',
			onOpen: function(){	    
				//setDateTimePicker('yearManufacturing');
				Utils.bindComboboxEquipGroupEasyUICodeName('groupEquipment', null, 193);
	        	Utils.bindComboboxEquipGroupEasyUICodeName('providerId', null, 193);

				var year = new Date();
				year = year.getFullYear(); 
				var op = '<option value="" selected="selected"></option><option value="'+year+'">'+year+'</option>';
				for(var i=1; i<100; i++){
					op+='<option value="'+(year-i)+'">'+(year-i)+'</option>';
				}
				$('#yearManufacturing').html(op);
				$('#equipmentCode, #seriNumber, #typeEquipment').bind('keyup',function(event){
					if(event.keyCode == keyCodes.ENTER){
						$('#btnSeachEquipmentDlg').click(); 
					}
				});
				$('#yearManufacturing').bind('keyup',function(event){
	        		if(event.keyCode == keyCodes.ENTER){
	        			$('#btnSeachEquipmentDlg').click(); 
	        		}
	        	});
	        	$('#groupEquipment, #providerId').next().find(".combo-text.validatebox-text").bind('keyup',function(event){
	        		if(event.keyCode == keyCodes.ENTER){
	        			$('#btnSeachEquipmentDlg').click(); 
	        		}
	        	});
				$('#stockCode').bind('keyup',function(event){
					if(event.keyCode == keyCodes.ENTER){
						$('#btnSeachEquipmentDlg').click(); 
					}
				});
				if ($('#cboStockType').val() == activeType.RUNNING) {
					// bao mat NPP
					enable('stockCode');
				} else if ($('#cboStockType').val() == activeType.WAITING) {
					// bao mat KH
					disabled('stockCode');
				}
	//	        	$('#stockCode').bind('keyup',function(event){
	//	        		if(event.keyCode == keyCodes.F9){
	//	        			RepairEquipmentManageCatalog.showPopupStock();
	//	        		}
	//	        	});
				EquipLostRecord.getEquipCatalogLost();
				var params = new Object();
				//tamvnm: thay doi thanh autoCompleteCombobox
				var groupId = $('#groupEquipment').combobox('getValue');
	        	if (groupId == -1 || groupId == "") {
	        		groupId = "";
	        	}
	        	var providerId = $('#providerId').combobox('getValue');
	        	if (providerId == -1 || providerId == "") {
	        		providerId = "";
	        	}
				params = {
						equipCode: $('#equipmentCode').val().trim(),
						seriNumber: $('#seriNumber').val().trim(),
						categoryId: $('#typeEquipment').val().trim(),
						// groupId: $('#groupEquipment').val().trim(),
						// providerId: $('#providerId').val().trim(),
						groupId: groupId,
						providerId: providerId,
						yearManufacture: $('#yearManufacturing').val().trim(),
						stockCode: $('#stockCode').val().trim(),
						stockType: Number($('#cboStockType').val()), // vuongmq; 05/05/2015
						shortCode: $('#txtCustomerCode').val().trim(), // vuongmq; 05/05/2015
						tradeStatus: 0, // vuongmq; 08/05/2015
						shopCode : $('#txtShopCode').val().trim(), // vuongmq; 14/05/2015
					};
				if (hdFlagLiquidationStatus != undefined && hdFlagLiquidationStatus != null) {
					params.flagUpdate = $('#hdFlagLiquidationStatus').val();
				}
				$('#equipmentGridDialog').datagrid({
					url : '/equipLostRecord/search-equipment-delivery-lost',
					autoRowHeight : true,
					rownumbers : true, 
					//fitColumns:true,
					pagination:true,
					scrollbarSize:0,
					pageSize:10,
					pageList: [10],
					width: $('#equipmentGridDialogContainer').width(),
					height: 'auto',
					autoWidth: true,	
					queryParams: params,
					columns:[[
						{field: 'select',title:'',width: 50,sortable:false,resizable:false, align: 'center', formatter: function(value, row, index) {
							return '<a href="javascript:void(0);" onclick="EquipLostRecord.searchEquipByEquipDeliRec('+row.equipmentId+');">Chọn</a>';
						}},	      
						{field: 'equipmentCode',title:'Mã thiết bị',width: 200,sortable:false,resizable:false, align: 'left', formatter: function(value, row, index) {
							return Utils.XSSEncode(value);
						}},	
						{field: 'seriNumber',title:'Số serial',width: 100,sortable:false,resizable:false, align: 'left', formatter: function(value, row, index) {
							return Utils.XSSEncode(value);
						}},	
						{field: 'healthStatus',title:'Tình trạng thiết bị',width: 100,sortable:false,resizable:false, align: 'left', formatter: function(value, row, index) {
							return Utils.XSSEncode(value);
						}},	
						{field: 'stock',title:'Kho',width: 120,sortable:false,resizable:false, align: 'left', formatter: function(value, row, index) {
							var html="";
							if(row != null && row.stockCode != null && row.stockName!= null){
								html = row.stockCode+' - '+row.stockName; 
							}
							return Utils.XSSEncode(html);
						}},	
						{field: 'typeEquipment',title:'Loại thiết bị',width: 80,sortable:false,resizable:false, align: 'left', formatter: function(value, row, index) {
							return Utils.XSSEncode(value);
						}},	
						{field: 'groupEquipment',title:'Nhóm thiết bị',width: 200,sortable:false,resizable:false, align: 'left', formatter: function(value, row, index) {
							var html="";
							if(row != null && row.groupEquipmentCode != null && row.groupEquipmentName!= null){
								html = row.groupEquipmentCode+' - '+row.groupEquipmentName; 
							}
							return Utils.XSSEncode(html);
						}},	
						{field: 'capacity',title:'Dung tích (lít)',width: 80,sortable:false,resizable:false, align: 'right', formatter: function(value, row, index) {
							return Utils.XSSEncode(value);
						}},	
						{field: 'equipmentBrand',title:'Hiệu',width: 80,sortable:false,resizable:false, align: 'left', formatter: function(value, row, index) {
							return Utils.XSSEncode(value);
						}},	
						{field: 'equipmentProvider',title:'NCC',width: 80,sortable:false,resizable:false, align: 'left', formatter: function(value, row, index) {
							return Utils.XSSEncode(value);
						}},	
						{field: 'yearManufacture',title:'Năm sản xuất',width: 80,sortable:false,resizable:false, align: 'left', formatter: function(value, row, index) {
							return Utils.XSSEncode(value);
						}}
					]],
					onBeforeLoad:function(param){											 
					},
					onLoadSuccess :function(data){   			 
						if (data == null || data.total == 0) {
							//$("#errMsgInfo").html("Không tìm thấy danh sách biên bản giao nhận thiết bị!").show();
						}else{
							$('.datagrid-header-rownumber').html('STT');		    	
						}	    		
					}
				});
			},
			onClose: function(){
				$('#easyuiPopupSearchEquipment').hide();
				$('#equipmentCode').val('').change();
				$('#seriNumber').val('').change();
				$('#typeEquipment').html('<option value="" selected="selected">Tất cả</option>');
				// $('#groupEquipment').html('<option value="" selected="selected">Tất cả</option>').change();
				// $('#providerId').html('<option value="" selected="selected">Tất cả</option>');
				$('#yearManufacturing').val('').change();
				$('#stockCode').val('').change();
				$('#equipmentGridDialog').datagrid("loadData",[]);
			}
		});
	},

	/**
	 * Chon thiet bi fill vao cac textbox
	 * @author phuongvm
	 * @since 06/01/2015
	 */
	 /** cai nay khong dung chon thiet bi nua; se goi ham: searchEquipByEquipDeliRec() de lay thiet bi filter vao grid*/
	/*selectEquipmentLost: function(index){
		var rowPopup = $('#equipmentGridDialog').datagrid('getRows')[index];
		var kho="";
		if(rowPopup.stockCode != undefined && rowPopup.stockCode != null && rowPopup.stockName != undefined && rowPopup.stockName!= null){
			kho = rowPopup.stockCode+' - '+rowPopup.stockName; 
		}
		$('#txtEquipCode').val(rowPopup.equipmentCode);
		$('#lblEquipName').html(rowPopup.groupEquipmentName);
		$('#txtStock').val(kho);
		$('#txtExpiredDate').val(rowPopup.warrantyExpiredDate); //06/04/2015; vuongmq; ngay het han bh 

		$('#txtRepairCount').val(rowPopup.repairCount);
		var rows = new Array();
    	EquipLostRecord._lstEquipItemId = new Array();
    	$('#gridDetail').datagrid('loadData',rows);
		
		$('#easyuiPopupSearchEquipment').dialog('close');
	},*/

	/**
	 * Tim kiem thiet bi
	 * @author vuongmq
	 * @since 04/05/2015
	 */
	searchEquipmentLost: function(){
		var params=new Object(); 
		params.equipCode = $('#equipmentCode').val().trim();
		params.seriNumber = $('#seriNumber').val().trim();
		params.categoryId = $('#typeEquipment').val().trim();
		
		//tamvnm: thay doi thanh autoCompleteCombobox: -1 la truong hop tim group ko co trong BD
		// params.groupId = $('#groupEquipment').val().trim();
		if ($('#groupEquipment').combobox('getValue') == "") {
			$('#groupEquipment').combobox('setValue', -1);
			params.groupId = "";
		} else if ($('#groupEquipment').combobox('getValue') == -1) {
			params.groupId = "";
		} else {
			if (Utils.getMessageOfInvaildNumberNew($('#groupEquipment').combobox("getValue")).length == 0) {
				params.groupId = $('#groupEquipment').combobox("getValue");
			} else {
				params.groupId = -1;
			}
		}
		// params.providerId = $('#providerId').val().trim();
		if ($('#providerId').combobox('getValue') == "") {
			$('#providerId').combobox('setValue', -1);
			params.providerId = "";
		} else if ($('#providerId').combobox('getValue') == -1) {
			params.providerId = "";
		} else {
			if (Utils.getMessageOfInvaildNumberNew($('#providerId').combobox("getValue")).length == 0) {
				params.providerId = $('#providerId').combobox("getValue");
			} else {
				params.providerId = -1;
			}
		}

		params.yearManufacture = $('#yearManufacturing').val().trim();
		params.stockCode = $('#stockCode').val().trim();
		params.stockType = Number($('#cboStockType').val()); // vuongmq; 05/05/2015
		params.shortCode = $('#txtCustomerCode').val().trim(); // vuongmq; 05/05/2015
		params.shopCode = $('#txtShopCode').val().trim(); // vuongmq; 14/05/2015
		var hdFlagLiquidationStatus = $('#hdFlagLiquidationStatus');
		if (hdFlagLiquidationStatus != undefined && hdFlagLiquidationStatus != null) {
			params.flagUpdate = $('#hdFlagLiquidationStatus').val();
		}
		$('#equipmentGridDialog').datagrid('load',params);
	},

	/**
	 * Tim kiem kho
	 * @author vuongmq
	 * @since 04/05/2015
	 */
	searchStockLost: function(){
		var params=new Object(); 
		params.arrShopTxt = $('#txtShopCode').val().trim(); // vuongmq; 19/05/2015
		params.shopCode = $('#shopCode').val().trim();
		params.shopName = $('#shopName').val().trim();
		$('#stockGridDialog').datagrid('load',params);
	},

	/**
	 * chon kho
	 * 
	 * @return the string
	 * @author vuongmq
	 * @since 04/05/2015
	 */
	selectStockLost: function(shopCode){
		$('#stockCode').val(shopCode);		
		$('#easyuiPopupSearchStock').dialog('close');
	},
	/**
	 * Show popup chon kho
	 * @author vuongmq
	 * @since 04/05/2015
	 */
	showPopupStockLost:function(){
	$('#easyuiPopupSearchStock').show();
		$('#easyuiPopupSearchStock').dialog({  
	        closed: false,  
	        cache: false,  
	        modal: true,
	        width : 800,
	        height : 'auto',
	        onOpen: function(){	
				$('#shopCode').focus();
				$('#shopCode, #shopName').bind('keyup',function(event){
					if(event.keyCode == keyCodes.ENTER){
						$('#btnSearchStockDlg').click(); 
					}
				});	
				$('#stockGridDialog').datagrid({
					url : '/equipLostRecord/searchStockLost',
					autoRowHeight : true,
					rownumbers : true, 
					fitColumns:true,
					scrollbarSize:0,
					pagination:true,
					pageSize: 10,
					pageList: [10],
					width: $('#stockGridDialogContainer').width(),
					height: 'auto',
					autoWidth: true,	
					queryParams:{
						arrShopTxt: $('#txtShopCode').val().trim(), // vuongmq; 14/05/2015
						shopCode: $('#shopCode').val().trim(),
						shopName: $('#shopName').val().trim()
					},
					columns:[[
						{field:'shopCode', title:'Đơn vị', align:'left', width: 140, sortable:false, resizable:false, formatter:function(value,row,index) {
			        	return row.shopCode + " - "+row.shopName ;        
			        }},
			        {field:'stockCode', title:'Mã kho', align:'left', width: 80, sortable:false, resizable:false},
			        {field:'choose', title:'', align:'center', width:40, sortable:false, resizable:false, formatter:function(value,row,index) {
			        	return "<a href='javascript:void(0)' onclick=\"return EquipLostRecord.selectStockLost('"+ row.stockCode+"');\">Chọn</a>";        
			        }}			
					]],
					onBeforeLoad:function(param){											 
					},
					onLoadSuccess :function(data){   			 
						if (data == null || data.total == 0) {
							//$("#errMsgInfo").html("Không tìm thấy danh sách biên bản giao nhận thiết bị!").show();
						}else{
							$('.datagrid-header-rownumber').html('STT');		    	
						}	    		
					}
				});				
	        },
			onClose: function(){
				$('#easyuiPopupSearchStock').hide();
				$('#shopCode').val('').change();
				$('#shopName').val('').change();
				$('#stockGridDialog').datagrid('loadData',[]);
				$('#stockCode').focus();
			}
		});		
		return false;
	},

	 /**
	 * cap nhat; bind su kien khi thay doi text box gridDetail chi tiet bao mat
	 * @author vuongmq
	 * @since 06/05/2015
	 */
	bindEventChangeTextLost: function() {
	/** chon so luong*/
		$('.priceActuallyInGridzz').blur(function () {
		    var idQuan = $(this).attr('id');
		    var value = $(this).val();
		    var id = idQuan.split('_')[1];
	     	var index = $(this).attr('gridIndex');
	     	/** 16/05/2015; vuongmq; vi grid 1 thiet bi bao mat*/
	     	if (index == '') {
	     		index = 0;
	     	}
		    if (index != undefined && index != null) {
		    	var data = $('#gridEquipment').datagrid ('getRows')[index];
		    	if (data != undefined && data != null) {
		    		/** value nhap phai nho hon = nguyen gia (price)*/
		    		var nguyenGia = 0;
		    		if (data.price != null) {
		    			nguyenGia  = data.price; 
		    		}
		    		if (Number(value) > Number(nguyenGia)) {
		    			var msg = 'Vui lòng nhập giá trị còn lại nhỏ hơn hoặc bằng Nguyên giá.';
		    			EquipLostRecord._msgGiaTriConlai = msg;
		    			setTimeout(function() {
							$('#errMsgInfo').html(msg).show();
				 		}, 1500);
						return false;
		    		} else {
		    			$('.ErrorMsgStyle').html('').hide();
		    			EquipLostRecord._msgGiaTriConlai = '';
		    		}
		    	}
		    }
		});
		$('.vinput-money').each(function() {
			var objectId = $(this).attr('id');
			if (objectId!=null && objectId!=undefined && objectId.length>0) {
				Utils.formatCurrencyFor(objectId);
			}			
		});
		Utils.bindFormatOnTextfieldInputCss('vinput-money',Utils._TF_NUMBER);
	},
/////////// END VUONGMQ; 04/05/2015; cap nhat lap hieu bao mat /////////////
		/**
		 * Cap nhat bien ban bao mat
		 * 
		 * @author hunglm16
		 * @since January 06, 2015
		 * */
		insertOrUpdateEquipLostRecord : function () {
			$('.SuccessMsgStyle').html('').hide();
			$('.ErrorMsgStyle').html('').hide();
			var comboboxEquip = $('#gridEquipment').datagrid('getEditors', 0);
			if (comboboxEquip != undefined && comboboxEquip != null && comboboxEquip.length > 0) {
				$('#errMsg').html('Bạn chưa chọn thiết bị hoặc thiết bị lựa chọn không hợp lệ').show();
				return false;
			}
			var msg = '';
			var params = {};
			//var shopCode = $('#txtShopCode').val().trim();
			var shortCode = $('#txtCustomerCode').val().trim();
			var representor =  $('#txtRepresentor').val().trim();
			var lostDate = $('#txtLostDate').val().trim();
			var fDate = $('#txtLastArisingSalesDate').val().trim();
			var tDate = $('#txtLostDate').val().trim();
			var baoMat = $('#cboStockType').val(); // vuongmq; 06/05/2015
			var shopCode = $('#txtShopCode').val().trim(); // vuongmq; 14/05/2015
			//Datpv4 26/06/2015 fix nam bao mat >= nsx
			var rows = $('#gridEquipment').datagrid('getRows');
			var firstRowDataGrid = rows[0];
			var manufacturingYear= firstRowDataGrid.manufacturingYear;
			var createDate = $('#createDate').val();
			var note = $('#note').val();
			if (msg.length == 0) {
				msg = Utils.getMessageOfRequireCheck('txtShopCode', 'Mã NPP');
			}

			if(msg.length == 0 && $('#createDate').val().trim().length > 0 && !Utils.isDate($('#createDate').val()) && $('#createDate').val() != '__/__/____' ){
				msg = 'Ngày biên bản không tồn tại hoặc không đúng định dạng dd/mm/yyyy.';
				err = '#createDate';
			}

			var curDate = ReportUtils.getCurrentDateString(); //Lay ngay hien tai sysDate
			if (msg.length == 0 && !Utils.compareDate($('#createDate').val().trim(), curDate)) {
				msg = 'Ngày biên bản không được lớn hơn ngày hiện tại. Vui lòng nhập lại';
				$('#createDate').focus();
			}

			if (msg.length == 0 && $('#note').attr('maxlength') > 500) {
				msg = 'Bạn nhập quá giới hạn của trường note';
			}


			if (baoMat != null && baoMat == activeType.WAITING) {
				// bao mat KH
				if (msg.length == 0) {
					msg = Utils.getMessageOfRequireCheck('txtCustomerCode', 'Mã khách hàng');
				}
			}
			if (msg.length == 0) {
				msg = Utils.getMessageOfRequireCheck('txtRepresentor', 'Người đại diện');
			}
			if (msg.length == 0) {
				msg = Utils.getMessageOfRequireCheck('txtLostDate', 'Thời gian mất');
			}
			if (msg.length == 0 && lostDate.length > 0) {
				msg = Utils.getMessageOfInvalidFormatDate('txtLostDate', 'Thời gian mất');
				$('#txtLostDate').focus();
			}
			if(msg.length==0 && tDate.length>0 && !Utils.compareDate(tDate, EquipLostRecord._sysDateStr)){
				msg = 'Thời gian mất phải nhỏ hơn hoặc bằng Ngày hiện tại của hệ thống: ' + EquipLostRecord._sysDateStr.trim();
				$('#txtLostDate').focus();
			}
			//Datpv4 26/06/2015 fix nam bao mat >= nsx
			if (msg.length==0) {
				var res = tDate.split("/");
				if (Number(res[2]) < Number(manufacturingYear)) {
					msg = 'Năm báo mất phải cùng hoặc sau năm sản xuất' ;
					$('#txtLostDate').focus();
				}
			}
			//vuongmq 21/07/2015 fix : Ngày hết phát sinh doanh số >= năm sản xuất.; 2. Ngày báo mất  >= Ngày hết phát sinh doanh số
			if (msg.length==0) {
				var res = fDate.split("/");
				if (Number(res[2]) < Number(manufacturingYear)) {
					msg = 'Năm phát sinh doanh số phải cùng hoặc sau năm sản xuất' ;
					$('#txtLastArisingSalesDate').focus();
				}
			}
			// if (msg.length == 0 && fDate.length > 0 && tDate.length > 0 && !Utils.compareDate(fDate, tDate)){
			// 	msg = 'Ngày hết phát sinh doanh số phải nhỏ hơn hoặc bằng Thời gian mất';
			// 	$('#txtLastArisingSalesDate').focus();
			// }

			var lastArisingSalesDate = $('#txtLastArisingSalesDate').val().trim();
			if (msg.length == 0) {
				msg = Utils.getMessageOfRequireCheck('txtLastArisingSalesDate', 'Ngày hết phát sinh doanh số');
			}
			if (msg.length == 0 && lastArisingSalesDate.length > 0) {
				msg = Utils.getMessageOfInvalidFormatDate('txtLastArisingSalesDate', 'Ngày hết phát sinh doanh số');
				$('#txtLastArisingSalesDate').focus();
			}
			if (msg.length == 0 && fDate.length > 0 && !Utils.compareDate(fDate, EquipLostRecord._sysDateStr)) {
				msg = 'Ngày hết phát sinh doanh số phải nhỏ hơn hoặc bằng Ngày hiện tại của hệ thống: ' + EquipLostRecord._sysDateStr.trim();
				$('#txtLostDate').focus();
			}
			if (msg.length == 0 && fDate.length > 0 && tDate.length > 0 && !Utils.compareDate(fDate, tDate)) {
				msg = 'Ngày hết phát sinh doanh số phải trước hoặc cùng Thời gian mất';
				$('#txtLastArisingSalesDate').focus();
			}
			/** vuongmq; 06/05/2015; kiem tra gia tri con lai */
			if (msg.length == 0 && EquipLostRecord._msgGiaTriConlai != null && EquipLostRecord._msgGiaTriConlai.length > 0) {
				msg = EquipLostRecord._msgGiaTriConlai;
				$('.priceActuallyInGridzz').focus();
			}

			var tracingResult = -1;
			var tracingPlace = -1;
			var dem = 0;
			if(msg.length == 0 && !$('[name="radTracingPlace"]').is(':checked')) {
				msg = "GS và NPP truy tìm tủ mất chưa được lựa chọn";
			} else  if (msg.length == 0) {
				var arrChkRadTracingPlace = $('[name="radTracingPlace"]');
				dem = 0;
				for (var i = 0, size = arrChkRadTracingPlace.length; i < size; i++) {
					if($(arrChkRadTracingPlace[i]).is(':checked')) {
						tracingPlace = Number($(arrChkRadTracingPlace[i]).val());
						dem++;
					}
				}
				if (dem > 1) {
					msg = "GS và NPP truy tìm tủ mất chỉ lựa chọn là duy nhất";
					$('#txtConclusion').focus();
				}
			}
			if(!$('[name="radTracingResult"]').is(':checked')) {
				msg = "Kết quả truy tìm chưa được lựa chọn";
			} else if (msg.length == 0) {
				var arrChktracingResult = $('[name="radTracingResult"]');
				dem = 0;
				for (var i = 0, size = arrChktracingResult.length; i < size; i++) {
					if($(arrChktracingResult[i]).is(':checked')) {
						tracingResult = Number($(arrChktracingResult[i]).val());
						dem++;
					}
				}
				if (dem > 1) {
					msg = "Kết quả truy tìm chỉ lựa chọn là duy nhất";
					$('#txtConclusion').focus();
				}
			}
			var conclusion = $('#txtConclusion').val().trim();
			if (msg.length == 0) {
				msg = Utils.getMessageOfSpecialCharactersValidate('txtConclusion', 'Kết luận', Utils._NAME);
			}
			var recommendedTreatment = $('#txtRecommendedTreatment').val().trim();
			if (msg.length == 0) {
				msg = Utils.getMessageOfSpecialCharactersValidate('txtRecommendedTreatment', 'Hướng đề xuất xử lý của công ty IDP', Utils._NAME);
			}
			var statusEquipRecord = $('#ddlStatusEquipRecord').val();
			var deliveryStatus = $('#ddlDeliveryStatus').val();
			var arrEquip = [];
			if (msg.length == 0) {
				var rows = $('#gridEquipment').datagrid('getRows');
				if (rows != undefined && rows != null && rows.length > 0) {
					for (var i = 0, size = rows.length; i < size; i++) {
						var row = rows [i];
						if (row.equipId != undefined && row.equipId != null && row.equipId > 0) {
							arrEquip.push(row.equipId);
						}
					}
				}
				if (arrEquip.length == 0) {
					msg = 'Mã Thiết Bị không hợp lệ';
					$("#equipItemCodeInGrid").focus();
				}
			}
			var arrPriceActually = []; // vuongmq; 06/05/2015
			$('.priceActuallyInGridzz').each(function(){
				if ($(this).val() != null && $(this).val() != '') {
					arrPriceActually.push(Utils.returnMoneyValue($(this).val()));
				} else {
					arrPriceActually.push(0);
				}
			});
			if (msg.length > 0){
				$('#errMsg').html(msg).show();
				return false;
			}
			var eqLostRecId = Number(EquipLostRecord._eqLostRecId);
			
			var arrFile = DropzonePlugin._dropzoneObject.getFilesWithStatus(Dropzone.ADDED);
			if (EquipLostRecord._arrFileDelete == undefined || EquipLostRecord._arrFileDelete == null) {
				EquipLostRecord._arrFileDelete = [];
			}
			if (arrFile != null && arrFile != undefined && arrFile.length > 0) {
				if (EquipLostRecord._countFile == 5) {
					$('#errMsg').html("Tổng số tập tin đính kèm không được vượt quá 5").show();
					return false;
				}
				var maxLength = 5 - EquipLostRecord._countFile;
				if (maxLength < 0) {
					maxLength = 0;
				}
				if (arrFile.length > maxLength) {
					$('#errMsg').html("Số tập tin thêm mới không được vượt quá " + maxLength).show();
					return false;
				}
				msg = UploadUtil.getMsgByFileRequireCheck(UploadUtil._typeFileRequireCheck.equipRecord);
				if (msg.trim().length > 0) {
					$('#errMsg').html(msg).show();
					return false;
				}
				var data = JSONUtil.getSimpleObject2({
					flagMobile: EquipLostRecord._isFlagMobile,
					eqLostMobiRecId: Number(EquipLostRecord._eqLostMobiRecId),
					shortCode: shortCode,
					lostDateStr: lostDate,
					lastArisingSalesDateStr: lastArisingSalesDate,
					tracingResult: tracingResult,
					tracingPlace: tracingPlace,
					conclusion: conclusion,
					recommendedTreatment: recommendedTreatment,
					recordStatus: statusEquipRecord,
					deliveryStatus: deliveryStatus,
					representor: representor,
					eqLostRecId: eqLostRecId,
					equipAttachFileStr: EquipLostRecord._arrFileDelete.join(','),
					arrEquip: arrEquip.join(','),
					arrPriceActually: arrPriceActually.join(','),
					flagLostRecord: baoMat, // vuongmq; 06/05/2015
					shopCode: shopCode, // vuongmq; 14/05/2015
					createDate: createDate,
					note: note,


				});
				$.messager.confirm('Xác nhận', 'Bạn có muốn lưu dữ liệu?', function(r){
					if (r){
						UploadUtil.updateAdditionalDataForUpload(data);
						UploadUtil.startUpload('errMsg');
						return false;
					}
				});
				return false;
			}
			
			var params = {
					flagMobile: EquipLostRecord._isFlagMobile,
					eqLostMobiRecId: Number(EquipLostRecord._eqLostMobiRecId),
					shortCode: shortCode,
					lostDateStr: lostDate,
					lastArisingSalesDateStr: lastArisingSalesDate,
					tracingResult: tracingResult,
					tracingPlace: tracingPlace,
					conclusion: conclusion,
					recommendedTreatment: recommendedTreatment,
					recordStatus: statusEquipRecord,
					deliveryStatus: deliveryStatus,
					representor: representor,
					eqLostRecId: eqLostRecId,
					equipAttachFileStr: EquipLostRecord._arrFileDelete.join(','),
					arrEquip: arrEquip.join(','),
					arrPriceActually: arrPriceActually.join(','),
					flagLostRecord: baoMat, // vuongmq; 06/05/2015
					shopCode: shopCode, // vuongmq; 14/05/2015
					createDate: createDate,
					note: note,
			};
			Utils.addOrSaveData(params, "/equipLostRecord/insertOrUpdateLoadRecord", null, 'errMsg', function(data) {
				//$("#successMsg").html("Lưu dữ liệu thành công").show();
				var htmlView = 'Lưu dữ liệu thành công.';
				if (data.errMsgMail != undefined && data.errMsgMail != '') {
					htmlView += '<span style="color:#f00"> Chưa gửi mail, ' + data.errMsgMail + '</span>';
				}
				$("#successMsg").html(htmlView).show();

				var t = setTimeout(function() {
					$('.SuccessMsgStyle').html("").hide();
					if (EquipLostRecord._isFlagMobile != null && EquipLostRecord._isFlagMobile == 0) {
						window.location.href = '/equipLostRecord/info';	
					} else {
						// bao mat cua mobile
						window.location.href = '/equipLostRecord/mobile/info';
					}					
					clearTimeout(t);
				 }, 1500);
			}, null, null, null, msg);
			return false;
		},
		
		/**
		 * Xay dung ddlStatusEquipRecord theo tung quyen va nghiep vu
		 * 
		 * @author hunglm16
		 * @Since January 08, 2015
		 * */
		createDdlStatusEquipRecord: function (idEqLostRec, isLevel) {
			$('#ddlStatusEquipRecord').html('');
			$('#ddlStatusEquipRecord').append('<option value="0" selected="selected">Dự thảo</option>');
			$('#ddlStatusEquipRecord').append('<option value="1" >Chờ duyệt</option>');
			$('#ddlStatusEquipRecord').change();
		},
		
		/**
		 * Xay dung ddlDeliveryStatus theo tung quyen va nghiep vu
		 * 
		 * @author hunglm16
		 * @Since January 08, 2015
		 * */
		createDdlDeliveryStatus: function (idEqLostRec, isLevel) {
			$('#ddlDeliveryStatus').html('');
			if (ShopDecentralizationSTT.VNM == isLevel) {
				$('#ddlDeliveryStatus').append('<option value="0" selected="selected">Chưa gửi</option>');
				$('#ddlDeliveryStatus').append('<option value="2">Đã nhận</option>');
			} else if (ShopDecentralizationSTT.NPP == isLevel) {
				$('#ddlDeliveryStatus').append('<option value="0">Chưa gửi</option>');
				$('#ddlDeliveryStatus').append('<option value="1" >Đã gửi</option>');
			}
			$('#ddlDeliveryStatus').change();
		},
		
		/**
		 * Xay dung ddlStatusEquipRecord Search theo tung quyen va nghiep vu
		 * 
		 * @author hunglm16
		 * @Since January 08, 2015
		 * */
		createDdlStatusEquipRecordSearch: function (idEqLostRec, isLevel) {
			$('#ddlStatusEquipRecordImport').html('');
			$('#ddlStatusEquipRecordImport').append('<option value="-2" selected="selected">Chọn</option>');
			if (ShopDecentralizationSTT.VNM == isLevel) {
				$('#ddlStatusEquipRecordImport').append('<option value="2">Duyệt</option>');
				$('#ddlStatusEquipRecordImport').append('<option value="3">Không duyệt</option>');
			} else if (ShopDecentralizationSTT.NPP == isLevel) {
				$('#ddlStatusEquipRecordImport').append('<option value="1" >Chờ duyệt</option>');
				$('#ddlStatusEquipRecordImport').append('<option value="4">Hủy</option>');
			}
			$('#ddlStatusEquipRecordImport').change();
		},
		
		/** Chi tiet thiet bi */
		equipmentDetail : function(id, dateStr, lastArisingSalesDateStr){
			$('#lostRecordDgContainerDetail').show();
			var params = new Object();
			params.id = id;
			$('#lostRecordDgDetail').datagrid({		
				url: '/equipLostRecord/mobile/equip-detail',
				width : $('#lostRecordDgContainerDetail').parent().width() - 10,    								        
				pagination : false,
		        rownumbers : true,
		        queryParams: params,
		        scrollbarSize: 0,
		        fitColumns : true,	
		        singleSelect : true,		        			        			        
		        singleSelect : true,			        
				columns:[[						  
					{field:'code', title:'Mã TB', width:120, align:'left', formatter: function(value, row, index){
				    	return Utils.XSSEncode(row.code);
				    }},
					{field:'serial', title:'Serial', width:120, align:'left', formatter: function(value, row, index){
				    	return Utils.XSSEncode(row.serial);
				    }},															    
					{field:'equipGroupName', title:'Tên nhóm TB', width:120, align:'left', formatter: function(value, row, index){
				    	return Utils.XSSEncode(row.equipGroupName);
				    }},
					{field:'equipCategoryName', title:'Tên loại TB', width:120, align:'left', formatter: function(value, row, index){
				    	return Utils.XSSEncode(row.equipCategoryName);
				    }},
				    {field:'healthStatus', title:'Tình trạng TB', width:150, align:'left', formatter: function(value, row, index){
				    	return Utils.XSSEncode(row.healthStatus);
				    }},
				    {field:'dateStr', title:'Ngày mất', width:60, align:'center', formatter: function(value, row, index){
				    	return Utils.XSSEncode(dateStr);
				    }},
				    {field:'lastArisingSaleDateStr', title:'Ngày hết PSDS', width:60, align:'center', formatter: function(value, row, index){
				    	return Utils.XSSEncode(lastArisingSalesDateStr);
				    }}
				]],					
		        onLoadSuccess :function(data){
					$('#lostRecordDgContainer .datagrid-header-rownumber').html('STT');											 	
				 	$(window).resize();
				 	$('html,body').animate({scrollTop: $('#lostRecordDgContainerDetail').offset().top}, 2000);
		        }
			});
		},
		
		/**
		 * Xay dung ddlDeliveryStatus Search theo tung quyen va nghiep vu
		 * 
		 * @author hunglm16
		 * @Since January 08, 2015
		 * */
		createDdlDeliveryStatusSearch: function (idEqLostRec, isLevel) {
			$('#ddlDeliveryStatusImport').html('');
			$('#ddlDeliveryStatusImport').append('<option value="-2" selected="selected">Chọn</option>');
			if (ShopDecentralizationSTT.VNM == isLevel) {
				$('#ddlDeliveryStatusImport').append('<option value="2">Đã nhận</option>');
			} else if (ShopDecentralizationSTT.NPP == isLevel) {
				$('#ddlDeliveryStatusImport').append('<option value="1" >Đã gửi</option>');
			}
			$('#ddlDeliveryStatusImport').change();
		},
		
		/**
		 * Xay dung ddlDeliveryStatus Search theo tung quyen va nghiep vu
		 * 
		 * @author hunglm16
		 * @Since January 08, 2015
		 * */
		editRowGridEquipmentChangePage: function (index) {
			var rowIndex = 0;
			if (index != undefined && index != null &&  !isNaN(index)) {
				rowIndex = index;
			}
			$('#gridEquipment').datagrid('beginEdit', 0);
			setTimeout(function(){
				var comboboxEquip = $('#gridEquipment').datagrid('getEditors', 0);
				$(comboboxEquip[0].target).combobox("loadData", EquipLostRecord.getDataComboboxEquipmentDg());
			 }, 50);
		},
		
		/**
		 * Import Excel danh sach thiet bi
		 * 
		 * @author hunglm16
		 * @since December 18,214
		 * */
		importEquipLostRec : function(){
			$('.SuccessMsgStyle').html('').hide();
			$('.ErrorMsgStyle').html('').hide();
			if ($('#fakefileExcelpc').val().length <= 0 ) {
				$('#errImpExcelMsg').html('Vui lòng chọn tập tin Excel').show();
				return false;
			}
			Utils.importExcelUtils(function(data){
				$('#btnSearch').click();
				$('#fakefileExcelpc').val("").change();
				if (data.message!=undefined && data.message!=null && data.message.trim().length >0){
					$('#errImpExcelMsg').html(data.message.trim()).change().show();
				} else {
					$('#successImpExcelMsg').html("Lưu dữ liệu thành công").show();
					var tm = setTimeout(function(){
						$('.SuccessMsgStyle').html('').hide();
						clearTimeout(tm);
						$('#eqLostRec_import_easyuiPopupImportExcel').dialog('close');
					 }, 1500);
				}
			}, 'importExcelFrm', 'excelFile', 'errImpExcelMsg');
			
		},
		
		/**
		 * In Bien Ban Bao Mat
		 * 
		 * @author hunglm16
		 * @since January 18,214
		 * */
		printEquipLostRec: function(index){
			if (index == undefined || index == null || isNaN(index)) {
				return;
			}
			var rows = $('#lostRecordDg').datagrid('getRows');
			var indexMax = rows.length;
			if(indexMax == 0) {
				$('#errMsg').html('Không có biên bản báo mất nào được chọn').show();
				return;
			}
			if (index >= 0 && index < indexMax) {
				$('#lostRecordDg').datagrid('uncheckAll');
				$('#lostRecordDg').datagrid('checkRow', index);
			}
			$('.ErrorMsgStyle').html("").hide();
			var rowData = $('#lostRecordDg').datagrid('getChecked');
			//lay danh sach bien ban
			if (rowData.length == 0) {
				$('#errMsg').html('Chưa có Biên bản báo mất nào được chọn').show();
				return;
			}
			var arrId = [];
			for(var i = 0, size = rowData.length; i < size; i++){
				arrId.push(rowData[i].id);
			}
			var params = {
				arrEquipLostRec: arrId.join(',')
			};
			$.messager.confirm('Xác nhận','Bạn có muốn xem bản in biên bản?',function(r){
				if(r){
					ReportUtils.exportReport('/equipLostRecord/printLostRecord', params);
					/*Utils.getHtmlDataByAjax(params, '/equipLostRecord/printLostRecord', function(data){
						console.log(data);
						try {
							var _data = JSON.parse(data);
							if(_data.error && _data.errMsg != undefined) {
								$('#errMsg').html(_data.errMsg).show();
							} else {
								var width = $(window).width();
								var height = $(window).height();
								window.open('/commons/open-tax-view?windowWidth='+width+'&windowHeight='+height);
							}
						} catch (e) {
						}
					}, null, null);*/
				}
			});		
			return false;
		},
		
		/**
		 * Xoa tap tin dinh kem
		 * 
		 * @author hunglm16
		 * @since Feb 18,214
		 * */
		removeEquipAttachFile: function(id){
			if (id == undefined || id == null || isNaN(id)) {
				return;
			}
			if (EquipLostRecord._arrFileDelete == null || EquipLostRecord._arrFileDelete == undefined) {
				EquipLostRecord._arrFileDelete = [];
			}
			EquipLostRecord._arrFileDelete.push(id);
			$('#divEquipAttachFile'+id).remove();
			if (EquipLostRecord._countFile != undefined && EquipLostRecord._countFile != null && EquipLostRecord._countFile > 0) {
				EquipLostRecord._countFile = EquipLostRecord._countFile - 1;
			} else {
				EquipLostRecord._countFile = 0;
			}
		},
		
		/**
		 * Xu ly lam moi Grid
		 * 
		 * @author hunglm16
		 * @since Feb 6,2015
		 * */
		createDataGidEquipment: function () {
			if (EquipLostRecord._rowsDetail == undefined || EquipLostRecord._rowsDetail == null) {
				EquipLostRecord._rowsDetail = [];
			}
			$('#gridEquipment').datagrid({
				data: EquipLostRecord._rowsDetail,
				width : $('#gridEquipment').width() - 15,
		        autoWidth: true,
		        rownumbers : true,
		        fitColumns : true,
		        autoRowHeight : true,
		        scrollbarSize: 0,
				columns:[[
					{field:'contractNumber', title:'Số hợp đồng', width:120, sortable: false, resizable: false, align:'left', formatter: function(value, row, index){
						if (Number(EquipLostRecord._equipIdChange) == row.equipId && Number(EquipLostRecord._eqLostMobiRecId) <= 0){
							return Utils.XSSEncode(EquipLostRecord._contractNumber);
						}
						return Utils.XSSEncode(row.contractNumber);
					}},  
				    {field:'equipCategoryCode', title:'Loại thiết bị', width:80, sortable: false, resizable: false, align:'left', formatter: function(value, row, index){
				    	if (row.equipId != undefined && row.equipId != null && row.equipId > 0) {
				    		return Utils.XSSEncode(row.equipCategoryCode +' - '+ row.equipCategoryName);		    		
				    	}
				    	return '';
				    }},
				    {field:'equipGroupCode', title:'Nhóm thiết bị', width:90, sortable: false, resizable: false, align:'left', formatter: function(value, row, index){
				    	if (row.equipId != undefined && row.equipId != null && row.equipId > 0) {
					    	return Utils.XSSEncode(row.equipGroupCode +' - '+ row.equipGroupName);
				    	}
				    	return '';
				    }},
				    {field:'equipCode', title:'Mã thiết bị (F9)', width:100, sortable: false, resizable: false, align:'left', formatter: function(value, row, index){
				    	//return Utils.XSSEncode(value);
				    	var html = '<input type="text" class="equipItemCodeInGridClzz InputTextStyle" id="equipItemCodeInGrid" style="margin-bottom:2px; width:94%" maxlength="50" readonly="true">';
				    	if (value != null) {
				    		html = '<input type="text" class="equipItemCodeInGridClzz InputTextStyle" id="equipItemCodeInGrid" style="margin-bottom:2px; width:94%" maxlength="50" readonly="true" value ="'+value+'">';
				    	}
		    			return html;
				    }},
				    {field:'serial', title:'Số serial', width:100, sortable: false, resizable: false, align:'left', formatter: function(value, row, index){
				    	return Utils.XSSEncode(value);
				    }},
				    {field:'price', title:'Nguyên giá', width:60, sortable: false, resizable: false, align:'right', formatter: function(value, row, index){
				    	return Utils.XSSEncode(formatCurrency(value));
				    }},
				    {field:'priceActually', title:'Giá trị còn lại', width:60, sortable: false, resizable: false, align:'right', formatter: function(value, row, index){
			    		var id = '';
						if (row.equipId != null) {
							id = row.equipId;
						}
						var idx = '';
						if (index != null) {
							idx = index;
						}
						var html = '<input type="text" class="priceActuallyInGridzz InputTextStyle vinput-money" id="priceActuallyInGrid_'+id+'" style="margin-bottom:2px; width: 92%; text-align: right;" maxlength="17" gridIndex="'+idx+'">';
						if (value != null) {
							html = '<input type="text" class="priceActuallyInGridzz InputTextStyle vinput-money" id="priceActuallyInGrid_'+id+'" style="margin-bottom:2px; width: 92%; text-align: right;" maxlength="17" gridIndex="'+idx+'" value="'+formatCurrency(value)+'">';
						}
						return html;
				    }},
				    {field:'quantity', title:'Số lượng', width:40, sortable: false, resizable: false, align:'right', formatter: function(value, row, index){
				    	if (row.equipId != undefined && row.equipId != null && row.equipId > 0) {
					    	return 1;
				    	}
				    	return '';
				    }},
				    {field:'manufacturingYear', title:'Năm sản xuất', width:55, sortable: false, resizable: false, align:'right', formatter: function(value, row, index){
				    	if (row.manufacturingYear != undefined && row.manufacturingYear != null && row.manufacturingYear > 0) {
					    	return Utils.XSSEncode(row.manufacturingYear);
				    	}
				    	return '';
				    }},
				    {field:'liquidationStatus', title:'Trạng thái thanh lý', width:60, sortable: false, resizable: false, align:'left', formatter: function(value, row, index){
				    	if (value != null && value != undefined) {
				    		if (value == 1) {
				    			return 'Đã thanh lý';
				    		}
				    		if (value == 2) {
				    			return 'Đang thanh lý';
				    		}
				    		if (value == 0) {
				    			return 'Không thanh lý';
				    		}
				    	}
				    	return '';
				    }}
				    /*{field:'changeRow', title:'', width:60, sortable: false, resizable: false, align:'center', formatter: function(value, row, index){
						if (row.equipId > 0) {
							return '<a id="permisionCreate_GridChange_'+row.equipId+'" class="cmsiscontrol" href="javascript:void(0)" onclick="EquipLostRecord.editRowGridEquipmentChangePage();"><img title="Chỉnh sửa" src="/resources/images/icon-edit.png" width="18" height="18" style="padding-left: 5px;"></a>';
						}
				    }}*/
				]],
		        onLoadSuccess: function(data){
					$('#gridEquipContainer .datagrid-header-rownumber').html('STT');
					Utils.updateRownumWidthAndHeightForDataGrid('gridEquipment');
					$(window).resize();
				 	var rows = $('#gridEquipment').datagrid('getRows');
		 	    	if(rows == null || rows.length == 0){
						//$('#gridEquipment').datagrid("hideColumn", "changeRow");
						/*if ($('#txtCustomerCode').val().trim().length > 0) { 
		 	    			EquipLostRecord.insertRowEquipmentDg();
						}*/
						/** cai nay insert grid luc dau khi qua them moi or chinh sua phieu bao mat; khong co record va cho 1 dong mac dinh*/
						EquipLostRecord.insertRowEquipmentDg();
			    	} else {
						if (EquipLostRecord._eqLostMobiRecId != undefined && EquipLostRecord._eqLostMobiRecId != null && EquipLostRecord._eqLostMobiRecId.toString().trim() > 0) {
							//$('#gridEquipment').datagrid("hideColumn", "changeRow");
							disabled('equipItemCodeInGrid'); // gia tri code thiet bi trong grid; nut cong tu chuc nang bao mat may tinh bang
						} else {
							if (EquipLostRecord._recordStatus != undefined && EquipLostRecord._recordStatus != null && EquipLostRecord._recordStatus.toString().trim() > 0 && statusRecordsEquip.KD == Number(EquipLostRecord._recordStatus)) {
								//$('#gridEquipment').datagrid("hideColumn", "changeRow");
							} else {
								//$('#gridEquipment').datagrid("showColumn", "changeRow");
							}
						}
						/*** vuongmq; 27/05/2015; lay du lieu dada */
						if (rows[0] != null && rows[0].codeMobile != null && rows[0].codeMobile != '' ) {
			    			// truong hop chinh sua nhan biet sua bao mat cua mobile thi disable 
			    			disabled('equipItemCodeInGrid');
			    		}
			    	}
		 	    	Utils.functionAccessFillControl('gridEquipContainer');
		 	    	if($('#gridEquipContainer .cmsiscontrol[id^="permisionCreate_GridChange"]').length == 0){
						//$('#gridEquipment').datagrid("hideColumn", "changeRow");
					} else {
						//$('#gridEquipment').datagrid("showColumn", "changeRow");
					}
					$('.ErrorMsgStyle').html('').hide();
					//vuongmq; 15/05/2015 khi chinh sua ben chuc nang phe duyet bao mat; cho disable grid
					if (EquipLostRecord._FLAG_CHANGE_PHE_DUYET != undefined && EquipLostRecord._FLAG_CHANGE_PHE_DUYET != null && EquipLostRecord._FLAG_CHANGE_PHE_DUYET == activeType.RUNNING) {
						disabled('equipItemCodeInGrid'); // gia tri code thiet bi trong grid
						$('.priceActuallyInGridzz').attr('disabled',true); // gia tri con lai
					}
					/*su kien tren F9 grid*/
					EquipLostRecord.editInputInGridLost();
					EquipLostRecord.bindEventChangeTextLost();
		        }
			});
		}
};
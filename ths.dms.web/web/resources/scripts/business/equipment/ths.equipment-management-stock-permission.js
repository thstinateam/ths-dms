/**
 * JS thuc hien nghiep vu Quan ly quyen kho thiet bi
 * 
 * @author hoanv25
 * @since  March 09, 2015
 * */
var EquipmentStockPermission = {
		
/**Khai bao thuoc tinh Bien Toan Cuc
 * 
 * @author hoanv25
 * @since March 12, 2015
 * */
 	_ADD: 'ADD',
 	_EDIT: 'EDIT',
 	_lstAddStock: null,
 	_lstEditStock: null,
	_lstEquipStockDel:null,
	_lstStockNow:null,
	_lstCheckParenId:null,
	_lstEquipStockGirdDel:null,
	_lstEquipStockInsert:null,
	_lstAddStockInsert:null,
	_lstEditStockInsert:null,	
	_checkListEdit:null,
	fromStockCodeGlobal:null,
	_mapStock:null,
	_mapIdStock:null,
	_mapIdEditStock:null,
	_mapIdEditRoleStock:null,
	_params: null, /**vuongmq; 25/03/2015; de dung xuat excel theo dk search*/
/**
 * Tim kiem danh sach quyen kho thiet bi
 * 
 * @author hoanv25
 * @since March 12, 2015
 * */
 searchStockPermission: function(){
 	var msg=''; 	
 	var code = $('#code').val().trim();
	var name = $('#name').val().trim();
	var status = $('#status').val().trim();
	/**vuongmq; 25/03/2015; de dung xuat excel theo dk search*/
	EquipmentStockPermission._params = new Object();
	EquipmentStockPermission._params.name = $('#name').val().trim();
	EquipmentStockPermission._params.code = $('#code').val().trim();
	EquipmentStockPermission._params.status = $('#status').val().trim();
	if(msg.length>0){
			$('#errMsg').html(msg).show();	
			setTimeout(function(){$('#errMsg').hide();},3000);
			return false;
		}
	var params =  new Object();
		params.code = code;
		params.name = name;		
		params.status = status;	
 	$('#gridStockPermission').datagrid("load", params);
 	$('#stockTransDetailTable').hide();		
 	return false;
 }, 
/**
 * Opend dialog them moi quyen kho thiet bi
 * 
 * @author hoanv25
 * @since March 12, 2015
 * */
 openDialogChangeAdd: function(){
		$(".ErrorMsgStyle").hide();
		$('#dialogParam').dialog({
			title: "Thêm mới quyền",
			closed: false,
			cache: false,
			modal: true,
	        width: 800,
	        height: 'auto',
	        onOpen: function(){
	        	$('.easyui-dialog .InputTextStyle').val('');
	    		$('.easyui-dialog #errorSearchMsg').hide();
	    		$('.easyui-dialog #statusAdd').val(1).change();
	    		$('#codeAdd').attr('disabled', false);	    
	    		$('#codeAdd').focus();
	    		/**combobox status*/
	    		$('#dialogParam .MySelectBoxClass').css('width', '173');
	    		$('#dialogParam .CustomStyleSelectBox').css('width', '140');
	    		$('#dialogParam #btnSaveAddStock').html('Cập nhật');
	    		var params = new Object();
				params.id = 0;
	    		$('#gridStock').datagrid({					
					url :"/equipment-manager-stock-permission/searchListStock",
					autoRowHeight : true,
					rownumbers : true, 
					checkOnSelect :true,		
					pagination:true,
					pageSize :10,
					pageList : [10],
					checkOnSelect :true,	
					scrollbarSize:0,
					fitColumns:true,			
					queryParams: params,		
					width: $('#gridContainerStock').width(),
					autoWidth: true,
					columns:[[
					    {field: 'shopCode',title:'Đơn vị',width: 100,sortable:false,resizable:false, align: 'left' },	
					    {field: 'stockCode',title:'Kho',width: 100,sortable:false,resizable:false, align: 'left' },						 
					    {field: 'isUnder',title:'Kho đơn vị cấp dưới',width: 100,sortable:false,resizable:false, align: 'center', formatter: function(value, row, index) {
					    	 		if(row.isUnder != undefined && row.isUnder != null && row.isUnder != 0 ){
					    				return "<input type='checkbox' value='"+row.id+"' id='ckr"+index+"'/>"; /** value: luc nay la id cua record*/
					    			}				    	
						}},
						{field:'edit', title:'<a href="javascript:void(0);" onclick="EquipmentStockPermission.openDialogChangeAddStock(\'ADD\');"><img src="/resources/images/icon_add.png" title="Thêm"/></a>', width:50, align:'center', formatter: function(value, row, index) {
			    			return '<a href="javascript:void(0);" onclick="EquipmentStockPermission.deleteStockInGrid('+index+','+row.id+')"><img src="/resources/images/icon-delete.png" title="Xóa"></a>';
						}},			    
						{field: 'id', hidden: true}		
					]],
					onBeforeLoad:function(param){											 
					},
					onLoadSuccess :function(data){   			 
						$('.datagrid-header-rownumber').html('STT');				
					}
				});
	    		$('#codeAdd, #nameAdd, #descAdd, #statusAdd').bind('keyup',function(event){
	    			if(event.keyCode == keyCodes.ENTER){
	    				$('#dialogParam #btnSaveAddStock').click(); 
	    				$('#dialogParam #btnSaveAddStock').unbind('click');
	    			}
	    		}); 
	    		$('#dialogParam #btnSaveAddStock').unbind('click');	    		
	    		$('#dialogParam #btnSaveAddStock').bind('click',function(event) {
	    			EquipmentStockPermission.add();
				});
	        },
	        onClose:function() {
	        	
	        }
		});
	},
/**
 * Them danh sach quyen kho thiet bi
 * 
 * @author hoanv25
 * @since March 12, 2015
 * */
	add: function(){
		EquipmentStockPermission._mapIdStock = [];
		$('.ErrorMsgStyle').html('').hide();
 		$('.SuccessMsgStyle').html('').hide(); 
		var msg ="";
		if(msg.length == 0){
			msg = Utils.getMessageOfRequireCheck('codeAdd', 'Mã quyền');
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfRequireCheck('nameAdd', 'Tên quyền');
		}
		if (msg.length == 0) {
			msg = Utils.getMessageOfSpecialCharactersValidate('codeAdd', 'Mã quyền',Utils._CODE);
		}
		if(msg.length >0){
			$('#errorMsgDl').html(msg).show();
			return false;
		}
		var params = new Object();		
		params.code = $('#codeAdd').val().trim();
		params.name = $('#nameAdd').val().trim();		
		params.description = $('#descAdd').val().trim();		
		params.status = $('#statusAdd').val().trim();
		var lstEquipId = new Array();	
		/** lay gia tri Id check va no Check*/
		var checkListStock = new Array();
   		var noCheckListStock = new Array();
		$('#gridContainerStock input[id^=ckr]').each(function() {	
			   if($(this).is(":checked")){			 
			     checkListStock.push($(this).val());
			     EquipmentStockPermission._lstAddStockInsert.push($(this).val());		
			   }else{
			     noCheckListStock.push($(this).val());
			   }
		 });
		var rows = $('#gridStock').datagrid('getRows');
		if (rows != undefined && rows != null && rows.length > 0){
				for( var i = 0, size = rows.length; i < size; i++){						
					if(rows[i].isUnder != undefined && rows[i].isUnder != null ){
						if(Number(rows[i].isUnder) == 0){
							EquipmentStockPermission._mapIdStock.push(rows[i].id);													
						}
					  }
					}					
			   }
		params.checkListStock = checkListStock.join(',');
		params.noCheckListStock = noCheckListStock.join(',');		
		params.mapIdStock = 	EquipmentStockPermission._mapIdStock.join(',');	
		var msgDialog = 'Bạn có muốn thêm mới quyền kho thiết bị?';
		JSONUtil.saveData2(params, "/equipment-manager-stock-permission/add", msgDialog, "errorMsgDl", function(data) {
			if(!data.error) {
				$('#dialogParam #successMsgDl').html("Thêm mới dữ liệu thành công").show();
				var tm = setTimeout(function(){
					$('#dialogParam').dialog('close');				
					$('#gridStockPermission').datagrid('reload');
					$('.SuccessMsgStyle').hide();
					$('#stockTransDetailTable').hide();		
					clearTimeout(tm);
			 }, 1500);
			}
		}, "", function (data) { // callBackFail
			if(data.error && data.errMsg != undefined ){
				$('#dialogParam #errorMsgDl').html(data.errMsg).show();
				return;
			}
		});		
		return false;
	},
/**
 * Opend dialog chinh sua quyen kho thiet bi
 * 
 * @author hoanv25
 * @since March 12, 2015
 * */
 openDialogChangeEdit: function(index, id){
	 	EquipmentStockPermission._lstEquipStockGirdDel = [];
		$(".ErrorMsgStyle").html('').hide();
		var data = $('#gridStockPermission').datagrid('getRows')[index];
		$('#dialogParamEdit').dialog({
			title: "Cập nhật quyền "+ '<span style="color:#199700">' + data.code + '</span>',
			closed: false,
			cache: false,
			modal: true,
	        width: 910,
	        height: 'auto',
	        onOpen: function(){
	        	$('.easyui-dialog .InputTextStyle').val('');
	    		$('.easyui-dialog #errorSearchMsg').hide();
	    		$('#nameEdit').focus();
	    		/**combobox status*/
	    		$('#dialogParamEdit .MySelectBoxClass').css('width', '173');
	    		$('#dialogParamEdit .CustomStyleSelectBox').css('width', '140');
	    		$('#dialogParamEdit #btnSaveAddStock').html('Cập nhật');				
	    		$('#codeEdit').attr('disabled', true);	    		
	    		$('#codeEdit').val(data.code);
	    		$('#nameEdit').val(data.name);	    	
	    		$('#descEdit').val(data.description);	    		
	    		$('#statusEdit').val(data.status).change();
	    		var params = new Object();
				params.id = id;	    		
	    		$('#gridStockRoleEdit').datagrid({					
					url :"/equipment-manager-stock-permission/searchListStock",
					autoRowHeight : true,
					rownumbers : true, 
					checkOnSelect :true,		
					pagination:true,
					pageSize :10,
					pageList : [10],
					checkOnSelect :true,	
					scrollbarSize:0,
					fitColumns:true,	
					queryParams: params,		
					width: $('#gridContainerStockEdit').width(),
					autoWidth: true,
					columns:[[
					    {field: 'shopCode',title:'Đơn vị',width: 100,sortable:false,resizable:false, align: 'left' },	
					    {field: 'stockCode',title:'Kho',width: 100,sortable:false,resizable:false, align: 'left' },						 
					    {field: 'isUnder',title:'Kho đơn vị cấp dưới',width: 100,sortable:false,resizable:false, align: 'center', formatter: function(value, row, index) {
					    		if(row.isUnder != undefined && row.isUnder != null){
					    			if(row.isUnder == 1){
					    				return "<input type='checkbox' checked='checked' value='"+row.id+"' id='ckr"+index+"'/>"; 
					    			} else if (row.isUnder != 1 && row.isUnder != 0){
										return "<input type='checkbox' value='"+row.id+"' id='ckr"+index+"' />"; 
					    			}else if(row.isUnder == 0 && row.ch != SpecificType.NPP){
										return "<input type='checkbox' value='"+row.id+"' id='ckr"+index+"' />"; 
					    			}					    		
					    		} 				    	
						}},
						{field:'edit', title:'<a href="javascript:void(0);" onclick="EquipmentStockPermission.openDialogChangeAddStock(\'EDIT\');"><img src="/resources/images/icon_add.png" title="Thêm"/></a>', width:50, align:'center', formatter: function(value, row, index) {
			    			return '<a href="javascript:void(0);" onclick="EquipmentStockPermission.deleteGridStockRoleEdit('+index+','+row.idStock+')"><img src="/resources/images/icon-delete.png" title="Xóa"></a>';
						}},			    
						{field: 'id', hidden: true},
						{field: 'idStock', hidden: true}					    
					]],
					onBeforeLoad:function(param){											 
					},
					onLoadSuccess :function(data){   			 
						$('.datagrid-header-rownumber').html('STT');							
					}
				});
	    		$('#codeEdit, #nameEdit, #descEdit, #statusEdit').bind('keyup',function(event){
	    			if(event.keyCode == keyCodes.ENTER){	    			
	    				$('#dialogParamEdit #btnSaveAddStock').click(); 
	    				$('#dialogParamEdit #btnSaveAddStock').unbind('click');
	    			}
	    		}); 
	    		$('#dialogParamEdit #btnSaveAddStock').unbind('click');
	    		$('#dialogParamEdit #btnSaveAddStock').bind('click',function(event) {
	    			EquipmentStockPermission.update(index, id);
				});
	        },
	        onClose:function() {	        	
	        }
		});
	},
/**
 * Chinh sua quyen kho thiet bi
 * 
 * @author hoanv25
 * @since March 12, 2015
 * */
 update: function(index, id){
 		EquipmentStockPermission._mapIdEditStock = [];
 		EquipmentStockPermission._mapIdEditRoleStock = [];
 		EquipmentStockPermission._lstAddStockInsert = [];	
 		$('.ErrorMsgStyle').html('').hide();
 		$('.SuccessMsgStyle').html('').hide(); 			
		var params = new Object();			
		params.code = $('#codeEdit').val().trim();
		params.name = $('#nameEdit').val().trim();	
		params.description = $('#descEdit').val().trim();
		params.status = $('#statusEdit').val().trim();
		params.id = id; /** id: bang equip_role_id*/
		var data = $('#gridStockPermission').datagrid('getRows')[index];
		
		if(data.name== null){
			data.name = '';
		}		
		if(data.description== null){
			data.description = '';
		}
		var checkListStock = new Array();
   		var noCheckListStock = new Array();
		$('#gridContainerStockEdit input[id^=ckr]').each(function() {			 
				var str = $(this).attr('id');
			 	var indexRecord = str.substring(3, str.length);
			 	var dataEdit = $('#gridStockRoleEdit').datagrid('getRows')[indexRecord];
			 	var idTable = 0; /** id: bang EQUIP_ROLE_DETAIL_ID */
			 	if(dataEdit != undefined && dataEdit != null){
			 		idTable = dataEdit.idStock;
			 	}
			    if($(this).is(":checked")){				    
				     var objectVO = new Object();
				     objectVO.idDetail = idTable;
				     objectVO.idStock = $(this).val();
				     checkListStock.push(objectVO);
				     EquipmentStockPermission._lstAddStockInsert.push(objectVO.idStock);	
			   	}else{		       	
		       		var objectVO = new Object();
			  	 	objectVO.idDetail = idTable;
			   		objectVO.idStock = $(this).val();
		       		noCheckListStock.push(objectVO);
			   }
		 });			
		var rows = $('#gridStockRoleEdit').datagrid('getRows');
		if (rows != undefined && rows != null && rows.length > 0){
				for( var i = 0, size = rows.length; i < size; i++){		
					if(rows[i].isUnder != undefined && rows[i].isUnder != null ){
						if(Number(rows[i].isUnder) == 0 && (rows[i].idStock == undefined || rows[i].idStock == null ||rows[i].idStock =="") ){
							EquipmentStockPermission._mapIdEditStock.push(rows[i].id);													
						}
						if(Number(rows[i].isUnder) == 0 && (rows[i].idStock != undefined || rows[i].idStock != null ||rows[i].idStock !="") ){
							EquipmentStockPermission._mapIdEditRoleStock.push(rows[i].id);													
						}
					  }
					}					
			   }
	 	params.mapIdEditStock = EquipmentStockPermission._mapIdEditStock.join(',');	
	 	params.mapIdEditRoleStock = EquipmentStockPermission._mapIdEditRoleStock.join(',');
	 	if(EquipmentStockPermission._lstEquipStockGirdDel != undefined && EquipmentStockPermission._lstEquipStockGirdDel != null && EquipmentStockPermission._lstEquipStockGirdDel != ""){
	 		params.lstEquipStockGirdDel = EquipmentStockPermission._lstEquipStockGirdDel.join(',');	
	 	}		
		params.checkListStockVO = checkListStock;
		params.noCheckListStockVO = noCheckListStock;
		var msgDialog = 'Bạn có muốn cập nhật quyền kho thiết bị?';
		JSONUtil.saveData2(params, "/equipment-manager-stock-permission/save", msgDialog, "errorEditMsgDl", function(data) {
			if(!data.error) {
				$('#dialogParamEdit #successMsgDl').html("Lưu dữ liệu thành công").show();
				var tm = setTimeout(function(){
					$('#dialogParamEdit').dialog('close');				
					$('#gridStockPermission').datagrid('reload');
					$('.SuccessMsgStyle').hide();
					$('#stockTransDetailTable').hide();		
					clearTimeout(tm);
			 }, 1500);
			}
		}, "", function (data) { // callBackFail
			if(data.error && data.errMsg != undefined ){
				$('#dialogParamEdit #errorEditMsgDl').html(data.errMsg).show();
				return;
			}
		});
		return false;		
	},
/**
 * Opend dialog copy quyen kho thiet bi
 * 
 * @author hoanv25
 * @since March 12, 2015
 * */
 openDialogChangeCopy: function(index, id){
		$(".ErrorMsgStyle").hide();
		$('#dialogParamCopy').dialog({
			title: "Copy quyền",
			closed: false,
			cache: false,
			modal: true,
	        width: 800,
	        height: 'auto',
	        onOpen: function(){
	        	$('.easyui-dialog .InputTextStyle').val('');
	    		$('.easyui-dialog #errorSearchMsg').hide();
	    		$('.easyui-dialog #statusCopy').val(1).change();
	    		$('#codeCopy').attr('disabled', false);
	    		$('#codeCopy').focus();
	    		/**combobox status*/
	    		$('#dialogParamCopy .MySelectBoxClass').css('width', '173');
	    		$('#dialogParamCopy .CustomStyleSelectBox').css('width', '140');
	    		$('#dialogParamCopy #btnSaveCopyStock').html('Cập nhật');	    	
	    		$('#dialogParamCopy #btnSaveCopyStock').unbind('click');
	    		$('#dialogParamCopy #btnSaveCopyStock').bind('click',function(event) {
	    			EquipmentStockPermission.copy(index, id);
				});
	        },
	        onClose:function() {
	        	
	        }
		});
	},
/**
 * Copy quyen kho thiet bi
 * 
 * @author hoanv25
 * @since March 12, 2015
 * */
 copy: function(index, id){
 	$('.ErrorMsgStyle').html('').hide();
 	$('.SuccessMsgStyle').html('').hide();
 	var msg ="";
		if(msg.length == 0){
			msg = Utils.getMessageOfRequireCheck('codeCopy', 'Mã quyền');
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfRequireCheck('nameCopy', 'Tên quyền');
		}
			// if(msg.length == 0){
			// msg = Utils.getMessageOfRequireCheck('descCopy', 'Mô tả');
		// }
		if (msg.length == 0) {
			msg = Utils.getMessageOfSpecialCharactersValidate('codeCopy', 'Mã quyền',Utils._CODE);
		}
		if(msg.length >0){
			$('#dialogParamCopy #errorMsgDl').html(msg).show();
			return false;
		}
		var params = new Object();			
		params.code = $('#codeCopy').val().trim();
		params.name = $('#nameCopy').val().trim();	
		params.description = $('#descCopy').val().trim();
		params.status = $('#statusCopy').val().trim();
		params.id = id;		
		var msgDialog = 'Bạn có muốn copy quyền kho thiết bị?';
		JSONUtil.saveData2(params, "/equipment-manager-stock-permission/copy", msgDialog, "errorEditMsgDl", function(data) {
			if(!data.error) {
				$('#dialogParamCopy #successMsgDl').html("Lưu dữ liệu thành công").show();
				var tm = setTimeout(function(){
					$('#dialogParamCopy').dialog('close');				
					$('#gridStockPermission').datagrid('reload');
					$('.SuccessMsgStyle').hide();
					$('#stockTransDetailTable').hide();		
					clearTimeout(tm);
			 }, 1500);
			}
		}, "", function (data) { // callBackFail
			if(data.error && data.errMsg != undefined ){
				$('#dialogParamCopy #errorMsgDl').html(data.errMsg).show();
				return false;
			}
		});	
		return false;
	},
/**
 * Opend dialog tim kiem kho thiet bi
 * 
 * @author hoanv25
 * @since March 12, 2015
 * */
 openDialogChangeAddStock: function(type){
	 $(".ErrorMsgStyle").hide();
		var html = '<div id="add-shopPopupDiv" style="display:none;">\
			<div id="add-shopPopup">\
			<div class="PopupContentMid">\
				<div class="GeneralForm Search1Form">\
					<label class="LabelStyle" style="width:100px;">Mã đơn vị</label>\
					<input id="shopCode" class="InputTextStyle" style="width:150px;" maxlength="50" />\
					<label class="LabelStyle" style="width:100px;">Tên đơn vị</label>\
					<input id="shopName" class="InputTextStyle" style="width:230px;" maxlength="250" />\
					<div class="Clear"></div>\
					<label class="LabelStyle" style="width:100px;">Mã kho</label>\
					<input id="stockCode" class="InputTextStyle" style="width:150px;" maxlength="50" />\
					<label class="LabelStyle" style="width:100px;">Tên kho</label>\
					<input id="stockName" class="InputTextStyle" style="width:230px;" maxlength="250" />\
					<div class="Clear"></div>\
					<div class="BtnCenterSection">\
					    <button class="BtnGeneralStyle" id="btnSearchDlg">Tìm kiếm</button>\
					</div>\
					\
					<h2 class="Title2Style">Danh sách đơn vị</h2>\
					<div class="GridSection" id="gridDlgDiv">\
						<div id="gridDlg"></div>\
					</div>\
					\
					<p id="erMsgDlg" class="ErrorMsgStyle" style="padding-left:5px;display:none;"></p>\
					<div class="Clear"></div>\
					<div class="BtnCenterSection">\
					    <button class="BtnGeneralStyle" id="btnChooseDlg">Chọn</button>\
						<button class="BtnGeneralStyle" id="btnCloseDlg" onclick="javascript:$(\'#add-shopPopup\').dialog(\'close\');">Bỏ qua</button>\
					</div>\
				</div>\
			</div>\
			</div>\
		</div>';
		$("body").append(html);
		
		$("#add-shopPopup").dialog({
			title: "Tìm kiếm kho",
			width: 850,
			heigth: "auto",
			modal: true, close: false, cache: false,
			onOpen: function() {
				$("#add-shopPopup").addClass("easyui-dialog");
				$("#add-shopPopup #shopCode").focus();	
				var params = new Object();
				params.shopCode = $("#add-shopPopup #shopCode").val().trim();
				params.shopName = $("#add-shopPopup #shopName").val().trim();
				params.stockCode = $("#add-shopPopup #stockCode").val().trim();
				params.stockName = $("#add-shopPopup #stockName").val().trim();	

				var lstEquipId = new Array(); 
				var lstObjectId = new Array();	
				var lstCheckSearchStockAdd = new Array();
				EquipmentStockPermission._lstAddStockInsert = [];				
				EquipmentStockPermission._lstEditStockInsert = [];	
				EquipmentStockPermission._lstCheckParenId = [];
				/*** lay tu Add: type = 0 */
				if(type != undefined && type == EquipmentStockPermission._ADD){
		        	var rows = $('#gridStock').datagrid('getRows');
					if (rows != undefined && rows != null && rows.length > 0){
						for( var i = 0, size = rows.length; i < size; i++){					
							if(rows[i].id != undefined){
								lstEquipId.push(rows[i].id);	
								lstObjectId.push(rows[i].objectId);						
							}					
						}
						params.lstEquipId = lstEquipId.join(',');
						params.lstObjectId = lstObjectId.join(',');
					}
					$('#gridContainerStock input[id^=ckr]').each(function() {	
						   if ($(this).is(":checked")){
							   lstCheckSearchStockAdd.push($(this).val());	
						   }
					});
					params.lstCheckSearchStockAdd = lstCheckSearchStockAdd.join(',');
				} else if (type != undefined && type == EquipmentStockPermission._EDIT){
					/*** lay tu edit: type = 1 */
					var row = $('#gridStockRoleEdit').datagrid('getRows');
					if (row != undefined && row != null && row.length > 0){
						for( var i = 0, size = row.length; i < size; i++){					
							if(row[i].id != undefined){
								lstEquipId.push(row[i].id);	
								lstObjectId.push(row[i].objectId);						
							}					
						}
						params.lstEquipId = lstEquipId.join(',');
						params.lstObjectId = lstObjectId.join(',');
					}
					$('#gridContainerStockEdit input[id^=ckr]').each(function() {	
						   if ($(this).is(":checked")){
							   lstCheckSearchStockAdd.push($(this).val());	
						   }
					});
					params.lstCheckSearchStockAdd = lstCheckSearchStockAdd.join(',');
				}
				$("#add-shopPopup #gridDlg").datagrid({
					url :"/equipment-manager-stock-permission/searchStock",
					autoRowHeight : true,
					rownumbers : true, 
					checkOnSelect :true,		
					pagination:true,
					pageSize :10,
					pageList : [10],
					checkOnSelect :true,	
					scrollbarSize:0,
					fitColumns:true,
					width:  $('#gridDlgDiv').width(),
					queryParams: params,						
					columns: [[
					    {field: 'shopCode',title:'Mã đơn vị',width: 80,sortable:false,resizable:false, align: 'left' },	
					    {field: 'shopName',title:'Tên đơn vị',width: 100,sortable:false,resizable:false, align: 'left', formatter: function(v, r, i) {							
							return Utils.XSSEncode(v);
						}},	
						{field: 'stock',title:'Kho',width: 100,sortable:false,resizable:false, align: 'left', formatter: function(v, r, i) {							
							return Utils.XSSEncode(v);
						}},							
						{field: 'ckeck', title:"Chọn", sortable: false, resizable: false, width:50, fixed:true, align:"center", formatter: function(v, r, i) {							
							return "<input type='checkbox' value='"+i+"' id='ck"+r.id+"'/>";
						}}
					]],
					onLoadSuccess: function(data) {
						$("#add-shopPopup #gridDlg").datagrid("resize");
						Utils.bindFormatOnTextfieldInputCss('qttClass', Utils._TF_NUMBER, "#add-shopPopup");					
						setTimeout(function(){
							var hDlg=parseInt($("#add-shopPopup").parent().height());
							var hW=parseInt($(window).height());
							var d=hW-hDlg;
							d=d/2+document.documentElement.scrollTop;
							if (d < 0) { d = 0; }
							$("#add-shopPopup").parent().css('top',d);
			    		},1000);
					}
				});
				
				$("#add-shopPopup #btnSearchDlg").click(function() {
					var params = {
							shopCode: $("#add-shopPopup #shopCode").val().trim(),
							shopName: $("#add-shopPopup #shopName").val().trim(),
							stockCode: $("#add-shopPopup #stockCode").val().trim(),
							stockName: $("#add-shopPopup #stockName").val().trim(),
							lstEquipId: lstEquipId.join(','),
							lstObjectId: lstObjectId.join(','),
							lstCheckSearchStockAdd: lstCheckSearchStockAdd.join(','),
					};
					$("#add-shopPopup #gridDlg").datagrid("reload", params);
				});
				$("#add-shopPopup #shopCode, #add-shopPopup #shopName, #add-shopPopup #stockCode, #add-shopPopup #stockName").parent().keyup(function(event) {
					if (event.keyCode == keyCodes.ENTER) {
						$("#add-shopPopup #btnSearchDlg").click();
					}
				});		
				$("#add-shopPopup #btnChooseDlg").click(function() {
					if ($("#add-shopPopup input[id^=ck]:checked").length == 0) {
						$("#erMsgDlg").html("Không có kho đơn vị nào được chọn").show();
						return;
					}	
					if(type != undefined && type == EquipmentStockPermission._ADD){				
						EquipmentStockPermission.addShopQtt();
				 	}else if (type != undefined && type == EquipmentStockPermission._EDIT){
				 		EquipmentStockPermission.editShopQtt();
				 	}
				});
			},
			onClose: function() {
				$("#add-shopPopup").dialog("destroy");
				$("#add-shopPopupDiv").remove();
			}
		});
	},
	/**hoanv25; remove value khoi listArray: array */
	removeArrayJS: function(array, value) {
		if(array != null && value != null){
			for(var i = array.length - 1; i >= 0; i--) {
			    if(array[i] === value) {
			       array.splice(i, 1);
			    }
			}
		}
	},
	
	addShopQtt: function() {
		$(".ErrorMsgStyle").hide();
		EquipmentStockPermission._lstAddStock = [];		
		$.messager.confirm("Xác nhận", "Bạn có muốn thêm kho đơn vị?", function(r) {
			if (r) {
				var lstTmp = new Map();			
				$("#add-shopPopup input[id^=ck]").each(function() {					    
				        var str = $(this).attr('id');
				        var id = str.substring(2, str.length);
				        var data = $("#add-shopPopup #gridDlg").datagrid('getRows')[ $(this).val()];	
				        if($(this).is(":checked")){					        
					        lstTmp.put(id, data);
					        EquipmentStockPermission._lstAddStock.push(id);
					        EquipmentStockPermission._lstEquipStockInsert.push(id);
				       	}else{
				       		/**hoanv25; remove value khoi listArray: array /// remove id khoi EquipmentStockPermission._lstAddStock */
				       		EquipmentStockPermission.removeArrayJS(EquipmentStockPermission._lstAddStock, id);					       
				       	}
					});	
					/** remove Id trung trong lstTmp*/
					var dataList = $('#gridStock').datagrid('getRows');
					for (var i = 0; i< dataList.length; i ++) {
						if(lstTmp.findIt(dataList[i].id) == 0){ /**id nay ton tai trong map*/
							lstTmp.remove(dataList[i].id);
						}
					}
					/**Kiem tra kho da chọn có cả kho cha va kho con hay khong*/
					/*$('#gridContainerStock input[id^=ckr]').each(function() {	
					   if($(this).is(":checked")){					   
						     EquipmentStockPermission._lstAddStockInsert.push($(this).val());		
						   }
						});
					var rows = $('#gridStock').datagrid('getRows');
					if (rows != undefined && rows != null && rows.length > 0){
							for( var i = 0, size = rows.length; i < size; i++){					
								if(rows[i].isUnder != undefined && rows[i].isUnder != null ){
									if(Number(rows[i].isUnder) == 0){
										EquipmentStockPermission._lstAddStockInsert.push(rows[i].id);													
									}
								  }
								}					
						   }

					var params = new Object();					
					params.lstAddStock = EquipmentStockPermission._lstAddStock.join(',');				
					Utils.getJSONDataByAjaxNotOverlay(params, '/equipment-manager-stock-permission/checkAddStock', 
					function(equipments){
							if(equipments != undefined && equipments != null && equipments.length > 0) {
									$('#erMsgDlg').html('Không được chọn cả kho cha và kho cấp con!').show();
									return false;									
							} else {
								*//**Kiem tra xem kho co bi trung chom hay khong*//*
								var modelParams = new Object();					
								modelParams.lstAddStock = EquipmentStockPermission._lstAddStock.join(',');	
								modelParams.lstAddStockInsert = EquipmentStockPermission._lstAddStockInsert.join(',');			
								Utils.getJSONDataByAjaxNotOverlay(modelParams, '/equipment-manager-stock-permission/checkAddStockChild', 
									function(check){
									if(check != undefined && check != null && check.length > 0) {
											$('#erMsgDlg').html('Không được chọn kho trùng chờm!').show();
											return false;
									} else {
										
									}							
								}, null, null);
							}							
					}, null, null);		*/		
					/** Add kho vao gird danh sach*/
					for (var i = 0; i< lstTmp.keyArray.length; i ++) {
						var object = lstTmp.get(lstTmp.keyArray[i]);
						var newRow = {
							id: object.id,
							shopCode: Utils.XSSEncode(object.shopCode),
							stockCode: Utils.XSSEncode(object.stock),
							isUnder: object.isUnder,
							objectId: object.objectId
							};
						$('#gridStock').datagrid('appendRow', newRow);
					};
					$("#add-shopPopup").dialog("close");
			}
		});
	},
	editShopQtt: function() {
		$(".ErrorMsgStyle").hide();
		EquipmentStockPermission._lstEditStock = [];	
		$.messager.confirm("Xác nhận", "Bạn có muốn thêm kho đơn vị?", function(r) {
			if (r) {
				var lstTmp = new Map();
				$("#add-shopPopup input[id^=ck]:checked").each(function() {					    
				        var str = $(this).attr('id');
				        var id = str.substring(2, str.length);
				        var data = $("#add-shopPopup #gridDlg").datagrid('getRows')[ $(this).val()];				        
				        lstTmp.put(id, data);
				        EquipmentStockPermission._lstEditStock.push(id);
				        EquipmentStockPermission._lstEquipStockInsert.push(id);
				       
				});
				/** remove Id trung trong lstTmp*/
				var dataList = $('#gridStockRoleEdit').datagrid('getRows');
				for (var i = 0; i< dataList.length; i ++) {
					if(lstTmp.findIt(dataList[i].id) == 0){ /**id nay ton tai trong map*/
						lstTmp.remove(dataList[i].id);
					}
				}
				/**Kiem tra kho da chọn có cả kho cha va kho con hay khong*/
				/*	$('#gridContainerStockEdit input[id^=ckr]').each(function() {	
					   if($(this).is(":checked")){					   
						     EquipmentStockPermission._lstEditStockInsert.push($(this).val());		
						   }
						});
					var rows = $('#gridStockRoleEdit').datagrid('getRows');
					if (rows != undefined && rows != null && rows.length > 0){
							for( var i = 0, size = rows.length; i < size; i++){					
								if(rows[i].isUnder != undefined && rows[i].isUnder != null ){
									if(Number(rows[i].isUnder) == 0 ){
										EquipmentStockPermission._lstCheckParenId.push(rows[i].id);	
										var modParams = new Object();											
										modParams.lstAddStock = EquipmentStockPermission._lstCheckParenId.join(',');			
										Utils.getJSONDataByAjaxNotOverlay(modParams, '/equipment-manager-stock-permission/checkParenIdStock', 
												function(check){
												if(check != undefined && check != null && check.length > 0) {
												} else {
													EquipmentStockPermission._lstEditStockInsert.push(rows[i].id);	
												}							
											}, null, null);
																						
										}
								  }
								}					
						   }
					var params = new Object();					
					params.lstAddStock = EquipmentStockPermission._lstEditStock.join(',');				
					Utils.getJSONDataByAjaxNotOverlay(params, '/equipment-manager-stock-permission/checkAddStock', 
					function(equipments){
							if(equipments != undefined && equipments != null && equipments.length > 0) {
									$('#erMsgDlg').html('Không được chọn cả kho cha và kho cấp con!').show();
									return false;									
							} else {
								*//**Kiem tra xem kho co bi trung chom hay khong*//*
								var modelParams = new Object();					
								modelParams.lstAddStock = EquipmentStockPermission._lstEditStock.join(',');	
								modelParams.lstAddStockInsert = EquipmentStockPermission._lstEditStockInsert.join(',');			
								Utils.getJSONDataByAjaxNotOverlay(modelParams, '/equipment-manager-stock-permission/checkAddStockChild', 
									function(check){
									if(check != undefined && check != null && check.length > 0) {
											$('#erMsgDlg').html('Không được chọn kho trùng chờm!').show();
											return false;
									} else {
										
									}							
								}, null, null);
							}							
					}, null, null);			*/	
				/** insert row*/
				for (var i = 0; i< lstTmp.keyArray.length; i ++) {
					 var object = lstTmp.get(lstTmp.keyArray[i]);
					 var newRow = {
							id: object.id,
							shopCode: Utils.XSSEncode(object.shopCode),
							stockCode: Utils.XSSEncode(object.stock),
							isUnder: object.isUnder,
							objectId: object.objectId,
							ch:object.ch
							};
						$('#gridStockRoleEdit').datagrid('appendRow', newRow);
						};				
					$("#add-shopPopup").dialog("close");
			}
		});
	},
	/**
	 * Xoa mot dong quyen kho thiet bi trong gird
	 * @author hoanv25
	 * @since March 3, 2015
	 */
	deletePermissionStockInGrid: function(index, id){
		if (id == undefined || id == null) {
			return;
		}
		if (id > 0) {
			EquipmentStockPermission._lstEquipStockDel.push(id);
		}	
		var params = new Object();			
		params.id = id;		
		var msgDialog = 'Bạn có muốn xóa quyền?';
		JSONUtil.saveData2(params, "/equipment-manager-stock-permission/delete", msgDialog, "errMsgSearch", function(data) {
			if(!data.error) {
				$('#successMsgDl').html("Xóa dữ liệu thành công").show();
				var tm = setTimeout(function(){
					$('#dialogParam').dialog('close');				
					$('#gridStockPermission').datagrid('reload');
					$('#stockTransDetailTable').hide();		
					$('.SuccessMsgStyle').hide();
					clearTimeout(tm);
			 }, 1500);
			}
		}, "", function (data) { // callBackFail
			if(data.error && data.errMsg != undefined ){
				$('#errMsgSearch').html(data.errMsg).show();
				return false;
			}
		});	
	/*	Utils.addOrSaveData(params, '/equipment-manager-stock-permission/delete', null,  'errorMsgDl', function(data) {
			$('#successMsgDl').html("Xóa dữ liệu thành công").show();
			var tm = setTimeout(function(){
				$('#dialogParam').dialog('close');				
				$('#gridStockPermission').datagrid('reload');
				$('#stockTransDetailTable').hide();		
				$('.SuccessMsgStyle').hide();
				clearTimeout(tm);
			 }, 1200);
		}, null, null, null,msg);*/
		return false;
	},
	/**
	 * Xem chi tiet mot dong quyen kho thiet bi trong gird
	 * @author hoanv25
	 * @since March 3, 2015
	 */
	 viewDetail : function(index, id) {	
	 	if(id == null && id == undefined){
	 		return false;
	 	}	 	
		$('#stockTransDetailTable').show();
		var params = new Object();
		params.id = id;
		var data = $('#gridStockPermission').datagrid('getRows')[index];
		$('#title').html('Danh sách kho - ' + '<span style="color:#dc143c">' + data.code + '</span>').show();	
		$('#productDetailGrid').html('<table id="gridDetail" class="easyui-datagrid"></table>');
		$('#gridDetail').datagrid({
			url : '/equipment-manager-stock-permission/viewdetailstock',
			method: 'GET',
			fitColumns:true,
			width: $('#productDetailGrid').width(),
			autoWidth: true,
			autoRowHeight : true,
			singleSelect: true,
			rownumbers : true, 
			checkOnSelect :true,
			pagination:true,
			pageSize:10,
			pageList  : [10,20,30],
			scrollbarSize:0,
			queryParams:params,
			columns:[[
			        {field:'shopCode', title: 'Đơn vị', width: 100, sortable:false,resizable:false , align: 'left', formatter:function(value, row, index) {
			        	  return Utils.XSSEncode(value);
			          }},
			  	    {field:'stockCode', title: 'Kho', width: 130, sortable:false,resizable:false , align: 'left', formatter:function(value, row, index) {
			        	  return Utils.XSSEncode(value);
			          }},
			         {field: 'isUnder', title:'Kho đơn vị cấp dưới', sortable: false, resizable: false, width:250, fixed:true, align:"center", formatter: function(v, r, i) {	
			         	if(r.isUnder != undefined && r.isUnder != null && r.isUnder == 1){
			         		var html = '<a onclick=""><span style="cursor:pointer"><img title="" src="/resources/images/icon-tick.png" width="16" height="16"/></span></a>';
			           	   return html;
			           	}else{
			         	
			         		}							
						}}			      	       
			]],	    
			onLoadSuccess :function(data){				
				 $('.datagrid-header-rownumber').html('STT');	    	
				 updateRownumWidthForJqGrid('.easyui-dialog');
				 $(window).resize();    				
			}
		});		
 		$('html,body').animate({scrollTop: $('#productDetailGrid').offset().top}, 1500);
		return false;
	},
	/**
	 * Xoa mot dong danh sach kho trong gird
	 * @author hoanv25
	 * @since March 13, 2015
	 */
	deleteGridStockRoleEdit: function(index, id){		
		if (id != undefined && id != null && id > 0) {
			EquipmentStockPermission._lstEquipStockGirdDel.push(id);
			/** id: bang quyen*/
		}
		$('#gridStockRoleEdit').datagrid("deleteRow", index);
		var rowEquip = $('#gridStockRoleEdit').datagrid('getRows');
		var nSize = rowEquip.length;
		for(var i=index; i<nSize; i++){
			var row = rowEquip[i];
			$('#gridStockRoleEdit').datagrid('updateRow',{
				index: i,	
				row: row
			});	
		}
	 },
	deleteStockInGrid: function(index, id){			 
		EquipmentStockPermission._lstEquipStockGirdDel = [];
		if (id != undefined && id != null && id > 0) {
			EquipmentStockPermission._lstEquipStockGirdDel.push(id);
			/** id: bang quyen*/
		}
		$('#gridStock').datagrid("deleteRow", index);
		var rowEquip = $('#gridStock').datagrid('getRows');
		var nSize = rowEquip.length;
		for(var i=index; i<nSize; i++){
			var row = rowEquip[i];
			$('#gridStock').datagrid('updateRow',{
				index: i,	
				row: row
			});	
		}		
	},

	/**
	 * Mo Dialog Nhap Excel quyen kho thiet bi
	 * @author VuongMQ
	 * @date Mar 16,215
	 * */
	openDialogImportEquipmentStockPermission: function (){
		//alert('Nhập Excel nè');
		$('.SuccessMsgStyle').html('').hide();
		$('.ErrorMsgStyle').html('').hide();
		$('#easyuiPopupImportExcel').dialog({
			title: 'Nhập Excel Quyền kho thiết bị',
			width: 500, 
			height: 'auto',
			closed: false,
			cache: false,
			modal: true,
			onOpen: function() {
				$('#fakefilepcEquipStockPermission').val('');
			},
			onClose: function() {
				$('.ErrorMsgStyle').html('').hide();
			}
		});
	},
	/**
	 * Import Excel quyen kho thiet bi
	 * @author vuongmq
	 * @date Mar 23, 2015
	 * */
	importEquipmentStockPermission: function() {
		$('.SuccessMsgStyle').html('').hide();
		$('.ErrorMsgStyle').html('').hide();
		if($('#fakefilepcEquipStockPermission').val().length <= 0 ){
			$('#errExcelMsg').html('Vui lòng chọn tập tin Excel').show();
			return false;
		}
		Utils.importExcelUtils(function(data){
			//EquipmentListEquipment.searchListEquipment();
			$('#fakefilepcEquipStockPermission').val("").change();
			if (data.message != undefined && data.message != null && data.message.trim().length > 0) {
				$('#errExcelMsg').html(data.message.trim()).change().show();
				/**Mong muon du thanh cong hay that bai van vao day, giu lai popup cho nguoi dung xem*/
			} else {
				//$('#successExcelMsg').html("Lưu dữ liệu thành công").show();
				var tm = setTimeout(function(){
					$('.SuccessMsgStyle').html('').hide();
					clearTimeout(tm);
					$('#easyuiPopupImportExcel').dialog('close');
					//$('#listEquipmentDg').datagrid("load", EquipmentStockChange._params);
				 }, 1500);
			}
		}, 'importFrmEquipStockPermission', 'excelFileEquipStockPermission', 'errExcelMsg');
		
	},
	/**
	 * xuat file excel quyen kho thiet bi
	 * @author vuongmq
	 * @date March 23, 2015
	 */
	exportExcel: function() {		
		$('.SuccessMsgStyle').html('').hide();
		$('.ErrorMsgStyle').html('').hide();
		var msg ='';
		var data = $('#gridStockPermission').datagrid('getRows');	
		if(data == null || data.length <=0){
			msg = "Không có dữ liệu xuất Excel";
		}
		if(msg.length > 0){
			$('#errMsgSearch').html(msg).show();
			return false;
		}
 		/*var code = $('#code').val().trim();
		var name = $('#name').val().trim();
		var status = $('#status').val().trim();	
		var params =  new Object();
			params.code = code;
			params.name = name;		
			params.status = status;	*/
					
		$.messager.confirm('Xác nhận','Bạn có muốn xuất file excel?',function(r){
			if(r){
				ReportUtils.exportReport('/equipment-manager-stock-permission/exportExcelBySearchEquipmentStockPermission', EquipmentStockPermission._params, 'errMsgSearch');					
			}
		});		
		return false;
	},
};
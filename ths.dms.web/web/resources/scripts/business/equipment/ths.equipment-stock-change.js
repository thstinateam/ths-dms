var EquipmentStockChange = {
	_FormAdd: 'ADD',
	_FormSearch: 'SEARCH',		
	_lstEquipStockChangeDel: null,
	_lstEquipStockChangeInsert:null,
	_lstToStockCodeInsert:null,
	_editIndex:null,
	_lstEquipInRecord:null,
	_lstEquipDel:new Array(),
	_numberEquipInRecord:null,
	_lstEquipInsert:null,
	_lstRecordError: null,
	_lstRecordSuccess: null,
	_arrFileDelete: null,
	isHavingInvalidRowOnGrid: false,
	_countFile: 0,
	_arrFileDelete: null,
	_toStockDefault:null,
	editIndex:undefined,
	_params: null, /*** param dung de search va xuat bao cao theo dk search/
	deletedEquipmentCodes: new Array(),
	

	/**
	 * Event kho nguon - kho dich
	 * 
	 * @return the string
	 * @author hoanv25
	 * @since 24/12/2014
	 */
	eventStockFunction: function(){
		var msg='';
		if (msg.length == 0) {
			msg = Utils.getMessageOfSpecialCharactersValidate('fStock', 'Kho giao',Utils._CODE);
		}
		if (msg.length == 0) {
			msg = Utils.getMessageOfSpecialCharactersValidate('tStock', 'Kho nhận',Utils._CODE);
		}
		if(msg.length>0){
			$('#errMsg').html(msg).show();	
			setTimeout(function(){$('#errMsg').hide();},3000);
			return false;
		}
	},
	/**
	 * Xu ly trên input loai thiet bi
	 * @author phuongvm
	 * @since 12/05/2015
	 */
	onChangeTypeEquip: function(cb){
		var typeEquip = $(cb).val().trim();
		EquipmentStockChange.getEquipGroup(typeEquip);
	},
	/**
	 * Lay nhom thiet bi
	 * @author phuongvm
	 * @since 12/05/2015
	 */
	getEquipGroup: function(typeEquip){
		var params = new Object();
		if(typeEquip != undefined && typeEquip != null && typeEquip != ''){
			params.id = typeEquip;
		}
		$('#groupEquipment').html('<option value="" selected="selected">Tất cả</option>');
    	Utils.getJSONDataByAjaxNotOverlay(params, '/equipment-manage-delivery/get-equipment-group', function(result){	
		// $.getJSON('/equipment-manage-liquidation/get-equipment-catalog', function(result) {				
			// if(result.equipGroups != undefined && result.equipGroups != null && result.equipGroups.length > 0) {					
			// 	for(var i=0; i < result.equipGroups.length; i++){
			// 		$('#groupEquipment').append('<option value="'+ result.equipGroups[i].id +'">'+ Utils.XSSEncode(result.equipGroups[i].code+' - '+result.equipGroups[i].name) +'</option>');  
			// 	}
			// } 
			// $('#groupEquipment').change();	
			//tamvnm: thay doi thanh autoCompleteCombobox
			if(result.equipGroups != undefined && result.equipGroups != null && result.equipGroups.length > 0) {	
				for (var i = 0, isize = result.equipGroups.length; i < isize; i++) {
					result.equipGroups[i].searchText = unicodeToEnglish(result.equipGroups[i].code + " " + result.equipGroups[i].name);
	 				result.equipGroups[i].searchText = result.equipGroups[i].searchText.toUpperCase();
				}				
				var obj = {
				id: -1,
				code: "Tất cả",
				name: "Tất cả",
				codeName: "Tất cả",
				searchText: unicodeToEnglish("Tất cả").toUpperCase()
				};
				result.equipGroups.splice(0, 0, obj);
				$('#groupEquipment').combobox("loadData", result.equipGroups);
		 		$('#groupEquipment').combobox("setValue", -1);
			} 			
		},null,null);
	},
	/**
	 * Xoa tap tin dinh kem
	 * 
	 * @author hunglm16
	 * @since Feb 18,214
	 * */
	removeEquipAttachFile: function(id){
			if (id == undefined || id == null || isNaN(id)) {
				return;
			}
			if (EquipmentStockChange._arrFileDelete == null || EquipmentStockChange._arrFileDelete == undefined) {
				EquipmentStockChange._arrFileDelete = [];
			}
			EquipmentStockChange._arrFileDelete.push(id);
			$('#divEquipAttachFile'+id).remove();
			if (EquipmentStockChange._countFile != undefined && EquipmentStockChange._countFile != null && EquipmentStockChange._countFile > 0) {
				EquipmentStockChange._countFile = EquipmentStockChange._countFile - 1;
			} else {
				EquipmentStockChange._countFile = 0;
			}
		},
		
	/**
	 * Tim kiem danh sach bien ban chuyen kho
	 * 
	 * @return the string
	 * @author hoanv25
	 * @since 24/12/2014
	 */
	searchStockChange : function() {
		var msg='';
		var code =$('#codeStockTran').val().trim();
		var status =$('#status').val().trim();
		var statusPerform =$('#statusPerform').val().trim();
		var fDate = $('#fDate').val();
		var tDate =$('#tDate').val();
		var cFDate = $('#cFDate').val();
		var cTDate =$('#cTDate').val();
		var tStock = $('#tStock').val().trim();
		var fStock = $('#fStock').val().trim();
		if (msg.length == 0) {
			msg = Utils.getMessageOfSpecialCharactersValidate('codeStockTran', 'Mã biên bản',Utils._CODE);
		}
		if(msg.length == 0 && $('#fDate').val().trim().length>0 && !Utils.isDate($('#fDate').val(),'/') && $('#fDate').val() != '__/__/____' ){
			msg = 'Từ ngày không tồn tại hoặc không đúng định dạng dd/mm/yyyy.';
			err = '#fDate';
		}
		if(msg.length == 0 && $('#tDate').val().trim().length>0 && !Utils.isDate($('#tDate').val(),'/') && $('#tDate').val() != '__/__/____'){
			msg = 'Đến ngày không tồn tại hoặc không đúng định dạng dd/mm/yyyy.';
			err = '#tDate';
		}
		if(msg.length == 0 && fDate.length > 0 && tDate.length > 0 && !Utils.compareDate(fDate, tDate)) {
			msg = 'Từ ngày tạo phải là ngày trước hoặc cùng ngày với đến ngày tạo';
		}

		if(msg.length == 0 && $('#cFDate').val().trim().length > 0 && !Utils.isDate($('#cFDate').val(), '/') && $('#cFDate').val() != '__/__/____' ) {
			msg = 'Từ ngày không tồn tại hoặc không đúng định dạng dd/mm/yyyy.';
			err = '#cFDate';
		}
		if(msg.length == 0 && $('#cTDate').val().trim().length > 0 && !Utils.isDate($('#cTDate').val(), '/') && $('#cTDate').val() != '__/__/____') {
			msg = 'Đến ngày không tồn tại hoặc không đúng định dạng dd/mm/yyyy.';
			err = '#cTDate';
		}
		if(msg.length == 0 && cFDate.length > 0 && tDate.length > 0 && !Utils.compareDate(cFDate, cTDate)) {
			msg = 'Từ ngày phải là ngày trước hoặc cùng ngày với đến ngày';
		}
		if(msg.length>0){
			$('#errMsg').html(msg).show();	
			setTimeout(function(){$('#errMsg').hide();},3000);
			return false;
		}
		var params =  new Object();
		params.code = code;
		params.status = status;
		params.statusPerform = statusPerform;
		params.fromDate = fDate;
		params.toDate = tDate;
		params.createFromDate = cFDate;
		params.createToDate = cTDate;
		params.toStock = tStock;
		params.fromStock = fStock;
		$('#gridStockChange').datagrid('load',params);	
		return false;
	},	
	
	/**
	 * Luu thay doi trang thai bien ban 
	 * @author hoanv25
	 * @since 24/12/2014
	 */
	saveStockChange: function(){
		var rows = $('#gridStockChange').datagrid('getRows');
		if(rows.length == 0){
			$('#errMsg').html('Không có dữ liệu để lưu').show();
			return false;
		}
		var lstRecordSuccess = new Array();
		//var lstIdRecord = new Array();
		var lstCheck = $('#gridStockChange').datagrid('getChecked');
		if(lstCheck.length > 0){
			var statusRecordChange = $('#statusRecordLabel').val().trim();	
			if( statusRecordChange == -2){
				$('#errMsg').html('Bạn chưa chọn trạng thái để chuyển biên bản!').show();
				return false;
			}
			for(var i=0; i < lstCheck.length; i++){				
				if(statusRecordChange != "" && statusRecordChange!= -2){
					lstRecordSuccess.push(lstCheck[i].id);
				}
			}	
			if(lstRecordSuccess.length <= 0){
				$('#errMsg').html('Bạn chưa chọn trạng thái để lưu!').show();
				return;
			}		
			var params = new Object();
			params.lstIdRecord = lstRecordSuccess;		
			params.statusRecord = statusRecordChange;			
			Utils.addOrSaveData(params, '/equipment-stock-change-manage/save-record-stock', null, 'errMsg', function(data) {
				if(data.error){
					$('#errMsg').html(data.errMsg).show();
					return;
				}
				if(data.lstRecordError != null && data.lstRecordError.length>0){					
					VCommonJS.showDialogList({		  
						title:'LỖI',
						data : data.lstRecordError,			   
						columns : [[
							{field:'formCode', title:'Mã biên bản', align:'left', width: 150, sortable:false, resizable:false},
							{field:'formStatusErr', title:'Trạng thái biên bản', align:'center', width: 80, sortable:false, resizable:false,formatter: function(value, row, index){
								var html="";
								if(row.formStatusErr == 1){
									html = '<img src="/resources/images/icon_delete.png" height="20" width="20" style="margin-right:5px;" title="Error">';
								}
								return html;
								
							}},							
						/*	{field:'periodError', title:'Kỳ của biên bản', align:'center', width:80, sortable:false, resizable:false,formatter: function(value, row, index){
								var html="";
								if(row.periodError == 1){
									html = '<img src="/resources/images/icon_delete.png" height="20" width="20" style="margin-right:5px;" title="Error">';
								}
								return html;
								
							}}*/
						]]
					});
				}else{
					$('#successMsgInfo').html('Lưu dữ liệu thành công').show();
				}
				setTimeout(function(){ 
					$('#successMsgInfo').html('').hide();
				}, 3000);
				
				$('#gridStockChange').datagrid('reload');
				$('#gridStockChange').datagrid('uncheckAll');
			}, null, null, null, "Bạn có muốn cập nhật ?", null);			
		}else{
			$('#errMsg').html('Bạn chưa chọn biên bản để lưu').show();
			return false;
		}		
	},
	/**
	 * Cap nhat trang thai thuc hien cua bien ban chuyen kho thanh hoan thanh 
	 * @author dungnt19
	 * @since 29/03/2016
	 */
	donePerformStatusStockChange: function(){
		var rows = $('#gridStockChange').datagrid('getRows');
		if(rows.length == 0){
			$('#errMsg').html('Không có dữ liệu để lưu').show();
			return false;
		}
		var lstRecordSuccess = new Array();
		var lstStatus = new Array();
		var lstPerformStatus = new Array();
		//var lstIdRecord = new Array();
		var lstCheck = $('#gridStockChange').datagrid('getChecked');
		if(lstCheck.length > 0){
			for(var i=0; i < lstCheck.length; i++){
				lstRecordSuccess.push(lstCheck[i].id);
				
				if(lstCheck[i].status == 2) {
					lstStatus.push(lstCheck[i].id);
				}
				
				if(lstCheck[i].performStatus == 2) {
					lstPerformStatus.push(lstCheck[i].id);
				}
			}
			
			if(lstRecordSuccess.length != lstStatus.length){
				$('#errMsg').html('Bạn chỉ được hoàn thành cho biên bản có trạng thái duyệt!').show();
				return;
			}
			
			if(lstRecordSuccess.length != lstPerformStatus.length){
				$('#errMsg').html('Bạn chỉ được hoàn thành cho biên bản có trạng thái thực hiện đang thực hiện!').show();
				return;
			}
			
			var params = new Object();
			params.lstIdRecord = lstRecordSuccess;		
			params.perfromStatusRecord = 1;			
			Utils.addOrSaveData(params, '/equipment-stock-change-manage/save-done-perform-status-record-stock', null, 'errMsg', function(data) {
				if(data.error){
					$('#errMsg').html(data.errMsg).show();
					return;
				}
				if(data.lstRecordError != null && data.lstRecordError.length>0){					
					VCommonJS.showDialogList({		  
						title:'LỖI',
						data : data.lstRecordError,			   
						columns : [[
							{field:'formCode', title:'Mã biên bản', align:'left', width: 150, sortable:false, resizable:false},
							{field:'formStatusErr', title:'Trạng thái biên bản', align:'center', width: 80, sortable:false, resizable:false,formatter: function(value, row, index){
								var html="";
								if(row.formStatusErr == 1){
									html = '<img src="/resources/images/icon_delete.png" height="20" width="20" style="margin-right:5px;" title="Error">';
								}
								return html;
								
							}},							
						/*	{field:'periodError', title:'Kỳ của biên bản', align:'center', width:80, sortable:false, resizable:false,formatter: function(value, row, index){
								var html="";
								if(row.periodError == 1){
									html = '<img src="/resources/images/icon_delete.png" height="20" width="20" style="margin-right:5px;" title="Error">';
								}
								return html;
								
							}}*/
						]]
					});
				}else{
					$('#successMsgInfo').html('Lưu dữ liệu thành công').show();
				}
				setTimeout(function(){ 
					$('#successMsgInfo').html('').hide();
				}, 3000);
				
				$('#gridStockChange').datagrid('reload');
				$('#gridStockChange').datagrid('uncheckAll');
			}, null, null, null, "Bạn có muốn cập nhật ?", null);			
		}else{
			$('#errMsg').html('Bạn chưa chọn biên bản để lưu').show();
			return false;
		}		
	},
	/**
	 * Luu bien ban chuyen kho
	 * @author hoanv25
	 * @since 6/1/2014
	 */
	 createRecordStockChange: function(){
		var msg = '';
		var id = $('#id').val().trim();
//		var fStock = $('#fStock').val().trim();
		
//		var tStock = $('#tStock').val().trim();
		var status = $('#status').val().trim();
		var statusPerform =$('#statusPerform').val().trim();
		$('#errMsg').html('').hide();
//		if($('#fStock').val() == ''){
//				$('#errMsg').html('Bạn chưa nhập giá trị cho Kho giao!').show();
//				return false;
//			}
//		if($('#tStock').val() == ''){
//				$('#errMsg').html('Bạn chưa nhập giá trị cho Kho nhận!').show();
//				return false;
//			}
//		if (msg.length == 0) {
//			msg = Utils.getMessageOfSpecialCharactersValidate('fStock', 'Kho giao',Utils._CODE);
//		}
//		if (msg.length == 0) {
//			msg = Utils.getMessageOfSpecialCharactersValidate('tStock', 'Kho nhận',Utils._CODE);
//		}
//		if(fStock == tStock){
//			$('#errMsg').html('Kho giao không được trùng với Kho nhận').show();
//			return false;
//		}

		var createDate = $('#createDate').val();
		if (msg.length == 0 && $('#createDate').val().trim().length > 0 && !Utils.isDate($('#createDate').val()) && $('#createDate').val() != '__/__/____' ) {
			msg = 'Ngày biên bản không tồn tại hoặc không đúng định dạng dd/mm/yyyy.';
			err = '#createDate';
		}
		var curDate = ReportUtils.getCurrentDateString(); //Lay ngay hien tai sysDate
		if (msg.length == 0 && !Utils.compareDate($('#createDate').val().trim(), curDate)) {
			msg = 'Ngày biên bản không được sau ngày hiện tại. Vui lòng nhập lại';
			$('#createDate').focus();
		}

		if (msg.length == 0 && $('#note').attr('maxlength') > 500) {
			msg = 'Bạn nhập quá giới hạn của trường note';
		}
		if(msg.length>0){
			$('#errMsg').html(msg).show();	
			setTimeout(function(){$('#errMsg').hide();},3000);
			return false;
		}
		if($('#equipmentCodeInGrid').val() == ''){
			$('#errMsg').html('Bạn chưa xử lý xong dòng thiết bị hiện tại!').show();
			return false;
		}
		if($('#seriNumberInGrid').val() == ''){
			$('#errMsg').html('Bạn chưa xử lý xong dòng thiết bị hiện tại!').show();
			return false;
		}
		if($('#toStockInGrid').val() == ''){
			$('#errMsg').html('Bạn chưa xử lý xong dòng thiết bị hiện tại!').show();
			return false;
		}
		var lstId = [];
		var lstEquipCode = new Array();
		var lstSeriNumber = new Array();
		var lstToStockCode = new Array();
		if(EquipmentStockChange._editIndex!=null){
			$('#gridEquipmentStockChange').datagrid('endEdit', EquipmentStockChange._editIndex);
		}
		var rows = $('#gridEquipmentStockChange').datagrid('getRows');
		if (rows != undefined && rows != null && rows.length > 0) {
			for ( var i = 0, size = rows.length; i < size; i++) {
				if(rows[i].idStockRecordDtl == null){
					var equipmentCode = rows[i].equipmentCode;
					if (equipmentCode != null && equipmentCode != undefined) {
						equipmentCode = rows[i].equipmentCode;
					} else {
						try {
							equipmentCode = $('#gridContainer td[field=equipmentCode] input#equipmentCodeInGrid').val();
						} catch(e) {
							// pass through
						}
					}
					lstEquipCode.push(equipmentCode);					
				
					var serialNumber = '';
					if (rows[i].seriNumber) {
						serialNumber = rows[i].seriNumber;
					} else {
						try {
							serialNumber = $('#gridContainer td[field=seriNumber] input#seriNumberInGrid').val();
						} catch(e) {
							// pass through
						}
					}
					lstSeriNumber.push(serialNumber != null && serialNumber != undefined ? serialNumber : '');
					
					var toStockCode = '';
					if (rows[i].toStock) {
						toStockCode = rows[i].toStock;
					} else {
						try {
							toStockCode = $('#gridContainer td[field=toStock] input#toStockInGrid').val().trim();
						} catch(e) {
							// pass through
						}
					}
					lstToStockCode.push(toStockCode != null && toStockCode != undefined ? toStockCode : '');
					
					if(rows[i].equipmentId != undefined){
						lstId.push(rows[i].equipmentId);
					}else{
						lstId.push(-1);
					}
				} else {					
					lstEquipCode.push(rows[i].equipmentCode);
					if (rows[i].seriNumber) {
						lstSeriNumber.push(rows[i].seriNumber);
					}
					lstToStockCode.push(rows[i].toStock);
					if(rows[i].equipmentId != undefined){
						lstId.push(rows[i].equipmentId);
					}else{
						lstId.push(-1);
					}
				}
				if(rows[i].stock!=null){
					var fromStock = rows[i].stock.split('-')[0].trim();
					var toStock = lstToStockCode[i];
					if(fromStock == toStock){
						$('#errMsg').html('Kho giao không được trùng với Kho nhận.').show();
						return false;
					}
				}
			}	
		}else{
			$('#errMsg').html('Vui lòng chọn thiết bị!').show();
			return false;
		}
		
		var dataModel = new Object();	
		dataModel.id = id;
//		dataModel.fromStock = fStock;
//		dataModel.toStock = tStock;
		dataModel.status = status;
		dataModel.createDate = createDate;
		dataModel.note = $('#note').val();
		dataModel.statusPerform = statusPerform;
		if (lstId != undefined && lstId != null && lstId.length >0 ) {
			dataModel.lstId = lstId;
		}
		if(lstEquipCode != null && lstEquipCode.length >0){
			dataModel.lstEquipCode = lstEquipCode;
		}
		if(lstSeriNumber != null && lstSeriNumber.length >0){
			dataModel.lstSeriNumber = lstSeriNumber;	
		}									
		if(lstToStockCode != null && lstToStockCode.length >0){
			dataModel.lstToStockCode = lstToStockCode;	
		}									
		dataModel.lstDell = EquipmentStockChange._lstEquipStockChangeDel.join(',');	
		//dataModel.deletedEquipmentCodes = EquipmentStockChange.deletedEquipmentCodes;

		var arrFile = DropzonePlugin._dropzoneObject.getFilesWithStatus(Dropzone.ADDED);
		if (EquipmentStockChange._arrFileDelete == undefined || EquipmentStockChange._arrFileDelete == null) {
			EquipmentStockChange._arrFileDelete = [];
		}
		dataModel.equipAttachFileStr = EquipmentStockChange._arrFileDelete.join(',');
		if (arrFile != null && arrFile != undefined && arrFile.length > 0) {
				if (EquipmentStockChange._countFile == 5) {
					$('#errMsg').html("Tổng số tập tin đính kèm không được vượt quá 5").show();
					return false;
				}
				var maxLength = 5 - EquipmentStockChange._countFile;
				if (arrFile.length > maxLength) {
					$('#errMsg').html("Số tập tin thêm mới không được vượt quá " + maxLength).show();
					return false;
				}
				msg = UploadUtil.getMsgByFileRequireCheck(UploadUtil._typeFileRequireCheck.equipRecord);
				if (msg.trim().length > 0) {
					$('#errMsg').html(msg).show();
					return false;
				}
				var data = JSONUtil.getSimpleObject2(dataModel);
				$.messager.confirm('Xác nhận', 'Bạn có muốn lưu dữ liệu?', function(r){
					if (r){
						UploadUtil.updateAdditionalDataForUpload(data);
						UploadUtil.startUpload('errMsg');
						return false;
					}
				});
				return false;
		}

		Utils.addOrSaveData(dataModel, '/equipment-stock-change-manage/create-record-stock', null, 'errMsg', function(data){	
			if(data.error){
				$('#errMsg').html(data.errMsg).show();
				return false;
			}else{
				$('#successMsgInfo').html("Lưu dữ liệu thành công").show();	
				EquipmentStockChange._lstEquipInsert = null;
				EquipmentStockChange._editIndex = null;				
				setTimeout(function(){
					$('#successMsgInfo').html("").hide(); 
					window.location.href = '/equipment-stock-change-manage/info';						              		
				}, 1000);
			}
		}, null, null, null, "Bạn có muốn cập nhật biên bản này ?", null);	
		return false;
	},
	/**
	 * Luu bien ban chuyen kho
	 * @author phuongvm
	 * @since 3/4/2015
	 */
	 createRecordStockChangeApproved: function(){
		var msg = '';
		var id = $('#id').val().trim();
		var statusPerform =$('#statusPerform').val().trim();
		$('#errMsg').html('').hide();
	
		var lstId = new Array();
		var lstPerformStatus = new Array();
		var lstToStockCode = new Array();
		
		EquipmentStockChange.editIndex = undefined; 
		var rows = $('#gridEquipmentStockChange').datagrid('getRows');
		if (rows != undefined && rows != null && rows.length > 0) {
			for ( var i = 0, size = rows.length; i < size; i++) {
				$('#gridEquipmentStockChange').datagrid('endEdit', i);
				if($('#toStockInGrid'+i).val()!=undefined){
					$('#errMsg').html('Bạn chưa xử lý xong dòng thiết bị hiện tại!').show();
					return false;
				}
				lstPerformStatus.push(rows[i].performStatus);
				lstToStockCode.push(rows[i].toStock);
				lstId.push(rows[i].idStockRecordDtl);
					
				if(rows[i].stock!=null){
					var fromStock = rows[i].stock.split('-')[0].trim();
					var toStock = lstToStockCode[i];
					if(fromStock == toStock){
						$('#errMsg').html('Kho giao không được trùng với Kho nhận.').show();
						return false;
					}
				}
			}	
		}	
		
		var dataModel = new Object();	
		dataModel.id = id;
		dataModel.statusPerform = statusPerform;
		if (lstId != undefined && lstId != null && lstId.length >0 ) {
			dataModel.lstId = lstId;
		}
		if(lstPerformStatus != null && lstPerformStatus.length >0){
			dataModel.lstPerformStatus = lstPerformStatus;	
		}									
		if(lstToStockCode != null && lstToStockCode.length >0){
			dataModel.lstToStockCode = lstToStockCode;	
		}									
		var arrFile = DropzonePlugin._dropzoneObject.getFilesWithStatus(Dropzone.ADDED);
		if (EquipmentStockChange._arrFileDelete == undefined || EquipmentStockChange._arrFileDelete == null) {
			EquipmentStockChange._arrFileDelete = [];
		}
		dataModel.equipAttachFileStr = EquipmentStockChange._arrFileDelete.join(',');
		if (arrFile != null && arrFile != undefined && arrFile.length > 0) {
				if (EquipmentStockChange._countFile == 5) {
					$('#errMsg').html("Tổng số tập tin đính kèm không được vượt quá 5").show();
					return false;
				}
				var maxLength = 5 - EquipmentStockChange._countFile;
				if (arrFile.length > maxLength) {
					$('#errMsg').html("Số tập tin thêm mới không được vượt quá " + maxLength).show();
					return false;
				}
				msg = UploadUtil.getMsgByFileRequireCheck(UploadUtil._typeFileRequireCheck.equipRecord);
				if (msg.trim().length > 0) {
					$('#errMsg').html(msg).show();
					return false;
				}
				var data = JSONUtil.getSimpleObject2(dataModel);
				$.messager.confirm('Xác nhận', 'Bạn có muốn lưu dữ liệu?', function(r){
					if (r){
						UploadUtil.updateAdditionalDataForUpload(data);
						UploadUtil.startUpload('errMsg');
						return false;
					}
				});
				return false;
		}

		Utils.addOrSaveData(dataModel, '/equipment-stock-change-manage/create-record-stock-approved', null, 'errMsg', function(data){	
			if(data.error){
				$('#errMsg').html(data.errMsg).show();
				return false;
			}else{
				$('#successMsgInfo').html("Lưu dữ liệu thành công").show();	
				EquipmentStockChange._lstEquipInsert = null;
				EquipmentStockChange._editIndex = null;				
				setTimeout(function(){
					$('#successMsgInfo').html("").hide(); 
					window.location.href = '/equipment-stock-change-manage/info';						              		
				}, 1000);
			}
		}, null, null, null, "Bạn có muốn cập nhật biên bản này ?", null);	
		return false;
	},
	/**
	 * Chon kho nguon - kho dich
	 * @author hoanv25
	 * @since 24/12/2014
	 */
	selectStock: function(txt, shortCode, stockId){
		$(txt).val(shortCode);
		if( stockId !=null){
			var equipmentStockTransRecordCode = $('#codeStockTran').val();
			$('#gridEquipmentStockChange').datagrid('load', {fromStockId:  stockId, code: equipmentStockTransRecordCode ? equipmentStockTransRecordCode : ''} );

		}		
//		EquipmentStockChange._toStockDefault = shortCode;
		$('#common-dialog-search-2-textbox').dialog('close');
		$('#common-dialog-search-2-textbox').remove();
//		 var rows = $('#gridEquipmentStockChange').datagrid('getRows');
//		 if (rows != undefined && rows != null && rows.length > 0) {
//			 $.messager.confirm('Xác nhận', 'Bạn có muốn chọn lại Kho nhận ?', function(r){
//					if (r){
//						$(txt).val(shortCode);
//						EquipmentStockChange._toStockDefault = shortCode;
//						$('#common-dialog-search-2-textbox').dialog('close');
//						$('#common-dialog-search-2-textbox').remove();
//						for ( var i = 0, size = rows.length; i < size; i++) {
//							rows[i].toStock = shortCode;
//						}
//						$('#gridEquipmentStockChange').datagrid('loadData', rows );
//					}
//				});	
//		 }else{
//			 $(txt).val(shortCode);
//				if( stockId !=null){
//					var equipmentStockTransRecordCode = $('#codeStockTran').val();
//					$('#gridEquipmentStockChange').datagrid('load', {fromStockId:  stockId, code: equipmentStockTransRecordCode ? equipmentStockTransRecordCode : ''} );
//
//				}		
//				EquipmentStockChange._toStockDefault = shortCode;
//				$('#common-dialog-search-2-textbox').dialog('close');
//				$('#common-dialog-search-2-textbox').remove();
//		 }
	},
	updateAdditionalDataForUpload: function() {
			var programId = $('#programId').val();
			var ndpId = $('#ndpId').val();
			
			var data = JSONUtil.getSimpleObject2({
				programId: programId,
				ndpId: ndpId
			});
			UploadUtil.updateAdditionalDataForUpload(data);
		},
	/**
	 * Xoa mot dong thiet bi trong gird
	 * @author hoanv25
	 * @since 5/1/2015
	 */
	deleteEquipmentStockTransInGrid: function(index, idStockRecordDtl){
		EquipmentStockChange.isHavingInvalidRowOnGrid = false;
		if (idStockRecordDtl > 0) {
			EquipmentStockChange._lstEquipStockChangeDel.push(idStockRecordDtl);
		}		
		var row = $('#gridEquipmentStockChange').datagrid("getRows")[index];
		$('#gridEquipmentStockChange').datagrid("deleteRow", index);
		if(row != null){
			EquipmentStockChange._lstEquipDel.push(row.equipmentCode);
		}
		var rowEquip = $('#gridEquipmentStockChange').datagrid('getRows');
		var nSize = rowEquip.length;
		var equipCode = $('#equipmentCodeInGrid').val();
		var seri = $('#seriNumberInGrid').val();
		var toStockInGrid = $('#toStockInGrid').val();
		
		if($('#equipmentCodeInGrid').val() != undefined){
			nSize = rowEquip.length-1;
			if (!EquipmentStockChange.deletedEquipmentCodes) {
				EquipmentStockChange.deletedEquipmentCodes = new Array();
			}
			EquipmentStockChange.deletedEquipmentCodes.push(rowEquip[rowEquip.length-1].equipmentCode);
			rowEquip[rowEquip.length-1].equipmentCode = null;			
			$('#equipmentCodeInGrid').remove();
			$('#seriNumberInGrid').remove();			
		}
		for(var i=index; i<nSize; i++){
			var row = rowEquip[i];
			$('#gridEquipmentStockChange').datagrid('updateRow',{
				index: i,	
				row: row
			});	
		}
		if( EquipmentStockChange._editIndex!= null && index < EquipmentStockChange._editIndex){
			EquipmentStockChange._editIndex--;
		}else{
			EquipmentStockChange._editIndex=null;
		}
		if(equipCode != undefined || toStockInGrid != undefined){
			$('#gridEquipmentStockChange').datagrid('updateRow',{
				index: rowEquip.length-1,	
				row: rowEquip[rowEquip.length-1]
			});
			if($('#equipmentCodeInGrid').val() != undefined){
				$('#equipmentCodeInGrid').val(equipCode);
			}
			if($('#seriNumberInGrid').val() != undefined){
				$('#seriNumberInGrid').val(seri);
			}		
			if($('#toStockInGrid').val() != undefined){
				$('#toStockInGrid').val(toStockInGrid);
			}		
			$('#equipmentCodeInGrid').width($('#equipmentCodeInGrid').parent().width());
			$('#seriNumberInGrid').width($('#seriNumberInGrid').parent().width());
			EquipmentStockChange.editInputGrid();		
		}
	},

	/**
	 * Them mot dong theit bi
	 * @author hoanv25
	 * @since 25/12/2014
	 */
	insertEquipmentStockTransInGrid: function(){	
		if (EquipmentStockChange.isHavingInvalidRowOnGrid) {
			return;
		}
		$('#errMsg').html('').hide();
		var sz = $("#gridEquipmentStockChange").datagrid("getRows").length;

		var insertNewRow = function() {
			EquipmentStockChange._editIndex = sz;	
			$('#gridEquipmentStockChange').datagrid("appendRow", {});	
			EquipmentStockChange.editInputGrid();
			$("#gridEquipmentStockChange").datagrid("selectRow", EquipmentStockChange._editIndex);	
			$('#equipmentCodeInGrid').focus();
		};

		var isCheckingIfEquipmentCodeValid = false;

		if(EquipmentStockChange._editIndex != null && EquipmentStockChange._editIndex == sz-1){
			var row = $('#gridEquipmentStockChange').datagrid('getRows')[EquipmentStockChange._editIndex];
			if($('#seriNumberInGrid').val() == ''){
				$('#errMsg').html('Bạn chưa xử lý xong dòng thiết bị thêm mới!').show();
				return false;
			}else if($('#seriNumberInGrid').val() != undefined){
				row.seriNumber = $('#seriNumberInGrid').val();
			}	
			
			if($('#toStockInGrid').val() == ''){
				$('#errMsg').html('Bạn chưa xử lý xong dòng thiết bị thêm mới!').show();
				return false;
			}else if($('#toStockInGrid').val() != undefined){
				var toStockCode = $('#toStockInGrid').val().trim();			
				if(!/^[0-9a-zA-Z-_.,]+$/.test(toStockCode)){
					$('#errMsg').html('Mã Kho nhận không hợp lệ!').show();
					return false;
				}
				row.toStock = $('#toStockInGrid').val();
			}	
			if($('#equipmentCodeInGrid').val() == ''){
				$('#errMsg').html('Bạn chưa xử lý xong dòng thiết bị thêm mới!').show();
				return false;
			}else if($('#equipmentCodeInGrid').val() != undefined){
				isCheckingIfEquipmentCodeValid = true;
				var equipCode = $('#equipmentCodeInGrid').val().trim();							
				var params = {equipCode:equipCode};	
				Utils.getJSONDataByAjaxNotOverlay(params, '/equipment-stock-change-manage/getEquipment', 
							function(equipments){
								if(equipments != undefined && equipments != null && equipments.length > 0) {
									insertNewRow();
									EquipmentStockChange.isHavingInvalidRowOnGrid = false;
								} else{
									$('#errMsg').html('Mã thiết bị nhập không đúng!').show();							
									$('#equipmentCodeInGrid').val() == '';
									$('#equipmentCodeInGrid').focus();
									EquipmentStockChange.isHavingInvalidRowOnGrid = true;									
																
									return false;
								}								
				}, null, null);	
				row.equipmentCode = $('#equipmentCodeInGrid').val();
			}		
			
			$('#equipmentCodeInGrid').remove();
			$('#seriNumberInGrid').remove();		
			$('#gridEquipmentStockChange').datagrid('updateRow',{
				index: EquipmentStockChange._editIndex,	
				row: row
			});
			if(EquipmentStockChange._lstEquipInsert == null){
				EquipmentStockChange._lstEquipInsert = new Array();
			}
			if(EquipmentStockChange._lstToStockCodeInsert == null){
				EquipmentStockChange._lstToStockCodeInsert = new Array();
			}
			EquipmentStockChange._lstEquipInsert.push(row.equipmentCode);		
			EquipmentStockChange._lstToStockCodeInsert.push(row.toStock);		
		}
		if (!isCheckingIfEquipmentCodeValid) {
			insertNewRow();
		}		
	},
	/**
	 * popup ma va so seri thiet bi
	 * @author hoanv25
	 * @since 26/12/2014
	 */
	showPopupSelectEquipment:function(dataGrid){
		$('#easyuiPopupSelectEquipment').show();
		$('#easyuiPopupSelectEquipment').dialog({  
	        closed: false,  
	        cache: false,  
	        modal: true,
	        width : 400,
	        height : 'auto',
	        onOpen: function(){	        	
	        	var params = new Object();
	        	var lstEquipId = new Array(); 
	        	var rows = $('#gridEquipmentStockChange').datagrid('getRows');
				if (rows != undefined && rows != null && rows.length > 1){
					for( var i = 0, size = rows.length; i < size; i++){					
						if(rows[i].equipmentId != undefined){
							lstEquipId.push(rows[i].equipmentId);							
						}					
					}
					params.lstEquipId = lstEquipId.join(',');
				}
				$('#gridSelectEquipment').datagrid({					
					data: dataGrid,
					autoRowHeight : true,
					fitColumns:true,
					scrollbarSize:0,			
					queryParams: params,		
					width: $('#gridSelectEquipmentContainer').width(),
					autoWidth: true,
					columns:[[
					    {field: 'equipmentCode',title:'Mã thiết bị',width: 100,sortable:false,resizable:false, align: 'left' },	
					    {field: 'seriNumber',title:'Số serial',width: 100,sortable:false,resizable:false, align: 'left' },	
					    {field: 'select',title:'',width: 100,sortable:false,resizable:false, align: 'left', formatter: function(value, row, index) {
					    	return '<a href="javascript:void(0);" onclick="EquipmentStockChange.selectEquipmentCode('+index+');">Chọn</a>';
						}}			    
					]],
					onBeforeLoad:function(param){											 
					},
					onLoadSuccess :function(data){   			 
						$('.datagrid-header-rownumber').html('STT');				
					}
				});
	        },
	        onClose: function(){
	        	$('#easyuiPopupSelectEquipment').hide();
	        }
		});
	},
	/**
	 * Chon ma, so seri thiet bi
	 * @author hoanv25
	 * @since 25/12/2014
	 */
	selectEquipmentCode: function(index){
		var rowPopup = $('#gridSelectEquipment').datagrid('getRows')[index];
		rowPopup.contentDelivery = null;
		if(EquipmentStockChange._editIndex!=null){
			$('#gridEquipmentStockChange').datagrid('updateRow',{
				index: EquipmentStockChange._editIndex,	// index start with 0
				row: rowPopup
			});
			$('#equipmentCodeInGrid').width($('#equipmentCodeInGrid').parent().width());
			$('#seriNumberInGrid').width($('#seriNumberInGrid').parent().width());
			EquipmentStockChange.editInputGrid();
		}
		$('#easyuiPopupSelectEquipment').dialog('close');
	},
	/**
	 * In bien ban chuyen kho thiet bi
	 * @author hoanv25
	 * @since 16/1/2014
	 */
	printRecodeStockTran: function(){
			$('#errMsg').html("").hide();
		var lstPrintRecodeStockTran = new Array();
		var rows = $('#gridStockChange').datagrid('getRows');		
		if(rows.length == 0){
			$('#errMsg').html('Không có dữ liệu để In').show();
			return false;
		}
		var lstCheck = $('#gridStockChange').datagrid('getChecked');
		if(lstCheck.length > 0){
			for (var i=0, sz = lstCheck.length; i < sz; i++) {
				lstPrintRecodeStockTran.push(lstCheck[i].id);
			}
			var dataModel = new Object();
			dataModel.lstPrintRecodeStockTran = lstPrintRecodeStockTran;
		} else if(lstCheck.length==0){
			$('#errMsg').html('Bạn chưa chọn biên bản').show();
			return false;
		}
		$.messager.confirm('Xác nhận','Bạn có muốn xem bản in biên bản?',function(r){
			if(r){				
				Utils.getHtmlDataByAjax(dataModel, '/equipment-stock-change-manage/print', function(data){
					console.log(data);
					try {
						var _data = JSON.parse(data);
						if(_data.error && _data.errMsg != undefined) {
							$('#errMsg').html(_data.errMsg).show();
						} else {
							var width = $(window).width();
							var height = $(window).height();
							window.open('/equipment-stock-change-manage/open-view?windowWidth='+width+'&windowHeight='+height);
						}
					} catch (e) {
					}
				}, null, null);
			}
		});		
		return false;
	},
	/**
	 * Xem chi tiet bien ban chuyen kho thiet bi
	 * @author hoanv25
	 * @since 16/1/2014
	 */
	viewRecodeStockTran: function(id){
		$('#errMsg').html("").hide();
		var lstPrintRecodeStockTran = new Array();
		if(id != null){			
				lstPrintRecodeStockTran.push(id);		
			var dataModel = new Object();
			dataModel.lstPrintRecodeStockTran = lstPrintRecodeStockTran;
		} else {
			$('#errMsg').html('Bạn chưa chọn biên bản').show();
			return false;
		}
		$.messager.confirm('Xác nhận','Bạn có muốn xem bản in biên bản?',function(r){
			if(r){				
				Utils.getHtmlDataByAjax(dataModel, '/equipment-stock-change-manage/print', function(data){
					console.log(data);
					try {
						var _data = JSON.parse(data);
						if(_data.error && _data.errMsg != undefined) {
							$('#errMsg').html(_data.errMsg).show();
						} else {
							var width = $(window).width();
							var height = $(window).height();
							window.open('/equipment-stock-change-manage/open-view?windowWidth='+width+'&windowHeight='+height);
						}
					} catch (e) {
					}
				}, null, null);
			}
		});		
		return false;
	},
	/**
	 * Show popup tim kiem thiet bi
	 * @author hoanv25
	 * @since 25/12/2014
	 */
	showPopupSearchEquipment: function(){
		$('#easyuiPopupSearchEquipment').show();
		$('#easyuiPopupSearchEquipment').dialog({  
	        closed: false,  
	        cache: false,  
	        modal: true,
	        width : 1200,
	        height : 'auto',
	        onOpen: function(){	 	        
	        	Utils.bindComboboxEquipGroupEasyUICodeName('groupEquipment', null, 193);
	        	Utils.bindComboboxEquipGroupEasyUICodeName('providerId', null, 193);	
	        	var year = new Date();
	        	year = year.getFullYear(); 
	        	var op = '<option value="" selected="selected"></option><option value="'+year+'">'+year+'</option>';
	        	for(var i=1; i<100; i++){
	        		op+='<option value="'+(year-i)+'">'+(year-i)+'</option>';
	        	}
	        	$('#yearManufacturing').html(op);
	        	$('#equipmentCode, #seriNumber, #typeEquipment').bind('keyup',function(event){
	        		if(event.keyCode == keyCodes.ENTER){
	        			$('#btnSeachEquipmentDlg').click(); 
	        		}
	        	});
	        	$('#yearManufacturing').bind('keyup',function(event){
	        		if(event.keyCode == keyCodes.ENTER){
	        			$('#btnSeachEquipmentDlg').click(); 
	        		}
	        	});
	        	$('#groupEquipment, #providerId').next().find(".combo-text.validatebox-text").bind('keyup',function(event){
	        		if(event.keyCode == keyCodes.ENTER){
	        			$('#btnSeachEquipmentDlg').click(); 
	        		}
	        	});      
	        	$('#equipmentCode').focus();	
//				$("#stockCode").val($("#fStock").val().trim());
	        	EquipmentStockChange.getEquipCatalog();
	        	var params = new Object();
	        	var groupId = $('#groupEquipment').combobox('getValue');
	        	if (groupId == -1 || groupId == "") {
	        		groupId = "";
	        	}
	        	var providerId = $('#providerId').combobox('getValue');
	        	if (providerId == -1 || providerId == "") {
	        		providerId = "";
	        	}
				params = {
						//equipId :$('#equipmentId').val().trim(),
						//fromStock : $('#fStock').val().trim(),
						equipCode: $('#equipmentCode').val().trim(),
						seriNumber: $('#seriNumber').val().trim(),
						categoryId: $('#typeEquipment').val().trim(),
						// groupId: $('#groupEquipment').val().trim(),
						// providerId: $('#providerId').val().trim(),
						groupId: groupId,
						providerId: providerId,
						yearManufacture: $('#yearManufacturing').val().trim(),
						stockCode: $('#stockCode').val().trim()
					};	
				var lstEquipId = new Array(); 
	        	var rows = $('#gridEquipmentStockChange').datagrid('getRows');
				if (rows != undefined && rows != null && rows.length > 1){
					for( var i = 0, size = rows.length; i < size; i++){					
						if(rows[i].equipmentId != undefined){
							lstEquipId.push(rows[i].equipmentId);							
						}					
					}
					params.lstEquipId = lstEquipId.join(',');
				}
				if($("#id").val() != "" && EquipmentStockChange._lstEquipInRecord != null){
					params.lstEquipDelete = "";
					for(var i=0; i<EquipmentStockChange._lstEquipInRecord.length; i++){
						params.lstEquipDelete += EquipmentStockChange._lstEquipInRecord[i]+',';
					}					
				}
				if(EquipmentStockChange._lstEquipInsert != null){
					params.lstEquipAdd = "";
					for(var i=0; i<EquipmentStockChange._lstEquipInsert.length; i++){
						params.lstEquipAdd += EquipmentStockChange._lstEquipInsert[i]+',';
					}			
				}
				if(EquipmentStockChange._lstEquipDel != null){
					params.lstEquipDelete = "";
					for(var i=0; i<EquipmentStockChange._lstEquipDel.length; i++){
						params.lstEquipDelete += EquipmentStockChange._lstEquipDel[i]+',';
					}			
				}
				$('#equipmentGridDialog').datagrid({
					url : '/equipment-stock-change-manage/search-equipment',	
					autoRowHeight : true,
					rownumbers : true, 
					//fitColumns:true,
					pagination:true,
					scrollbarSize:0,
					pageSize:10,
					pageList: [10],
					width: $('#equipmentGridDialogContainer').width(),
					height: 'auto',
					autoWidth: true,	
					queryParams: params,
					columns:[[
						{field: 'select',title:'',width: 50,sortable:false,resizable:false, align: 'left', formatter: function(value, row, index) {
					    	return '<a href="javascript:void(0);" onclick="EquipmentStockChange.selectEquipment('+index+');">Chọn</a>';
						}},
					    {field: 'equipmentCode',title:'Mã thiết bị',width: 180,sortable:false,resizable:false, align: 'left', formatter: function(value, row, index) {
					    	var html="";
							if(row.equipmentCode != null){
								html = row.equipmentCode; 
							}
							return Utils.XSSEncode(html);
						}},	
					    {field: 'seriNumber',title:'Số serial',width: 100,sortable:false,resizable:false, align: 'left', formatter: function(value, row, index) {
					    	var html="";
							if(row.seriNumber != null){
								html = row.seriNumber; 
							}
							return Utils.XSSEncode(html);
						}},	
					    {field: 'healthStatus',title:'Tình trạng thiết bị',width: 85,sortable:false,resizable:false, align: 'left', formatter: function(value, row, index) {
					    	var html="";
							if(row.healthStatus != null){
								html = row.healthStatus; 
							}
							return Utils.XSSEncode(html);
						}},	
					    {field: 'stock',title:'Kho',width: 120,sortable:false,resizable:false, align: 'left', formatter: function(value, row, index) {
					    	var html="";
							if(row.stock != null){
								html = row.stock; 
							}
							return Utils.XSSEncode(html);
						}},	
					    {field: 'typeEquipment',title:'Loại thiết bị',width: 80,sortable:false,resizable:false, align: 'left', formatter: function(value, row, index) {
					    	var html="";
							if(row.typeEquipment != null){
								html = row.typeEquipment; 
							}
							return Utils.XSSEncode(html);
						}},	
					    {field: 'groupEquipment',title:'Nhóm thiết bị',width: 200,sortable:false,resizable:false, align: 'left', formatter: function(value, row, index) {
					    	var html="";
							if(row.groupEquipmentCode != null && row.groupEquipmentName!= null){
								html = row.groupEquipmentCode+' - '+row.groupEquipmentName; 
							}
							return Utils.XSSEncode(html);
						}},	
					    {field: 'capacity',title:'Dung tích (lít)',width: 80,sortable:false,resizable:false, align: 'right' },	
					    {field: 'equipmentBrand',title:'Hiệu',width: 80,sortable:false,resizable:false, align: 'left', formatter: function(value, row, index) {
					    	var html="";
							if(row.equipmentBrand != null){
								html = row.equipmentBrand; 
							}
							return Utils.XSSEncode(html);
						}},	
					    {field: 'equipmentProvider',title:'NCC',width: 80,sortable:false,resizable:false, align: 'left', formatter: function(value, row, index) {
					    	var html="";
							if(row.equipmentProvider != null){
								html = row.equipmentProvider; 
							}
							return Utils.XSSEncode(html);
						} },	
					    {field: 'year',title:'Năm sản xuất',width: 80,sortable:false,resizable:false, align: 'right' },											
					]],
					onBeforeLoad:function(param){											 
					},
					onLoadSuccess :function(data){   			 
						if (data == null || data.total == 0) {						
						}else{							
							$('.datagrid-header-rownumber').html('STT');		    	
						}	    		
					}
				});
	        },
			onClose: function(){
				$('#easyuiPopupSearchEquipment').hide();
				$('#equipmentCode').val('').change();
				$('#seriNumber').val('').change();
				$('#typeEquipment').html('<option value="" selected="selected">Tất cả</option>');
				$('#groupEquipment').html('<option value="" selected="selected">Tất cả</option>').change();
				$('#providerId').html('<option value="" selected="selected">Tất cả</option>');
				$('#yearManufacturing').val('').change();
				$('#stockCode').val('').change();
			}
		});
		 $('#stockCode').bind('keyup', function(event) {
				 if(event.keyCode == keyCode_F9){
					 EquipmentStockChange.showPopupSearchStock('stockCode');
				}
		});
	},
	/**
	 * Show popup tim kiem kho
	 * @author phuongvm
	 * @since 30/03/2015
	 */
	showPopupSearchStock: function(txtInput){
		var txt = "toStockInGrid";
		if(txtInput!=undefined){
			txt = txtInput;
		}
		try {
			$('#common-dialog-search-2-textbox').dialog("close");
			$('#common-dialog-search-2-textbox').remove();
		} catch (e) {
			console.log('Khong mo duoc dialog');
		} 

		VCommonJS.showDialogSearch2({
			inputs : [
		        {id:'code', maxlength:50, label:'Mã đơn vị'},
		        {id:'name', maxlength:250, label:'Tên đơn vị'},
		    ],
		    url : '/equipment-stock-change-manage/get-list-equip-stock-vo',
		    columns : [[
		        {field:'codeName', title:'Đơn vị', align:'left', width: 110, sortable:false, resizable:false, formatter:function(value,row,index) {
		        	var nameCode = row.shopCode + '-' + row.shopName;
		        	return Utils.XSSEncode(nameCode);         
		        }},
		        {field:'code', title:'Mã kho', align:'left', width: 200, sortable:false, resizable:false},
		        {field:'choose', title:'', align:'center', width:40, sortable:false, resizable:false, formatter:function(value,row,index) {
//			        	var nameCode = row.shopCode + '-' + row.name;
		            return '<a href="javascript:void(0)" onclick="Utils.fillCodeForInputTextByDialogSearch2(\''+txt+'\', ' + row.equipStockId +', \''+ Utils.XSSEncode(row.code)+'\', \''+ Utils.XSSEncode(row.name) +'\','+Utils._isShowInputTextByDlSearch2.code+');">chọn</a>';         
		        }}
		    ]]
		});
	},
	/**
	 * Chon thiet bi
	 * @author hoanv25
	 * @since 6/1/2014
	 */
	selectEquipment: function(index){
		var rowPopup = $('#equipmentGridDialog').datagrid('getRows')[index];
		rowPopup.contentDelivery = null;
		if(EquipmentStockChange._editIndex!=null){
			$('#gridEquipmentStockChange').datagrid('updateRow',{
				index: EquipmentStockChange._editIndex,	// index start with 0
				row: rowPopup
			});
			$('#equipmentCodeInGrid').width($('#equipmentCodeInGrid').parent().width());
			$('#seriNumberInGrid').width($('#seriNumberInGrid').parent().width());
			EquipmentStockChange.editInputGrid();		
		}
		$('#easyuiPopupSearchEquipment').dialog('close');
	},
	/**
	 * Lay loai, ncc thiet bi
	 * @author hoanv25
	 * @since 25/12/2014
	 */
	getEquipCatalog: function(){
		$('#typeEquipment').html('<option value="" selected="selected">Tất cả</option>');
    	$('#provider').html('<option value=""  selected="selected">Tất cả</option>');
    	
		$.getJSON('/equipment-manage-delivery/get-equipment-catalog', function(result) {				
			if(result.equipsCategory != undefined && result.equipsCategory != null && result.equipsCategory.length > 0) {					
				for(var i=0; i < result.equipsCategory.length; i++){
					$('#typeEquipment').append('<option value="'+ result.equipsCategory[i].id +'">'+Utils.XSSEncode(result.equipsCategory[i].code)+' - '+Utils.XSSEncode(result.equipsCategory[i].name) +'</option>');  
				}
			} 
			$('#typeEquipment').change();
			// if(result.equipProviders != undefined && result.equipProviders != null && result.equipProviders.length > 0) {					
			// 	for(var i=0; i < result.equipProviders.length; i++){
			// 		$('#providerId').append('<option value="'+ result.equipProviders[i].id +'">'+ Utils.XSSEncode(result.equipProviders[i].name) +'</option>');  
			// 	}
			// } 
			// $('#providerId').change();	
			//tamvnm: thay doi thanh autoCompleteCombobox
			if(result.equipProviders != undefined && result.equipProviders != null && result.equipProviders.length > 0) {					
				for (var i = 0, isize = result.equipProviders.length; i < isize; i++) {
					result.equipProviders[i].searchText = unicodeToEnglish(result.equipProviders[i].code + " " + result.equipProviders[i].name);
 					result.equipProviders[i].searchText = result.equipProviders[i].searchText.toUpperCase();
				}
				var obj = {
					id: -1,
					code: "Tất cả",
					name: "Tất cả",
					codeName: "Tất cả",
					searchText: unicodeToEnglish("Tất cả").toUpperCase()
				};
				result.equipProviders.splice(0, 0, obj);
				$('#providerId').combobox("loadData", result.equipProviders);
		 		$('#providerId').combobox("setValue", -1);
			}			
		});
	},	
		/**
	 * Tim kiem thiet bi tren popup
	 * @author hoanv25
	 * @since 12/1/2015
	 */
	searchEquipmentStock: function(){
		var params=new Object(); 
		params.equipCode = $('#equipmentCode').val().trim();
		params.seriNumber = $('#seriNumber').val().trim();
		params.categoryId = $('#typeEquipment').val().trim();
		//tamvnm: thay doi thanh autoCompleteCombobox: -1 la truong hop tim group ko co trong BD
		// params.groupId = $('#groupEquipment').val().trim();
		if ($('#groupEquipment').combobox('getValue') == "") {
			$('#groupEquipment').combobox('setValue', -1);
			params.groupId = "";
		} else if ($('#groupEquipment').combobox('getValue') == -1) {
			params.groupId = "";
		} else {
			if (Utils.getMessageOfInvaildNumberNew($('#groupEquipment').combobox("getValue")).length == 0) {
				params.groupId = $('#groupEquipment').combobox("getValue");
			} else {
				params.groupId = -1;
			}
		}
		// params.providerId = $('#providerId').val().trim();
		if ($('#providerId').combobox('getValue') == "") {
			$('#providerId').combobox('setValue', -1);
			params.providerId = "";
		} else if ($('#providerId').combobox('getValue') == -1) {
			params.providerId = "";
		} else {
			if (Utils.getMessageOfInvaildNumberNew($('#providerId').combobox("getValue")).length == 0) {
				params.providerId = $('#providerId').combobox("getValue");
			} else {
				params.providerId = -1;
			}
		}
		params.yearManufacture = $('#yearManufacturing').val().trim();
		params.stockCode = $('#stockCode').val().trim();
		params.fromStock = $('#stockCode').val().trim();
		var lstEquipId = new Array(); 
	        	var rows = $('#gridEquipmentStockChange').datagrid('getRows');
				if (rows != undefined && rows != null && rows.length > 1){
					for( var i = 0, size = rows.length; i < size; i++){					
						if(rows[i].equipmentId != undefined){
							lstEquipId.push(rows[i].equipmentId);							
						}					
					}
					params.lstEquipId = lstEquipId.join(',');
				}
		$('#equipmentGridDialog').datagrid('load',params);
	},
	/**
	 * su kien tren grid
	 * @author hoanv25
	 * @since 25/12/2014
	 */
	editInputGrid: function(){	
			//Event F9 ma thiet bi
			$('input[id*=equipmentCodeInGrid]').bind('keyup', function(e){
				var equipmentCodeInput = this;
				if(e.keyCode==keyCode_F9){			
				$('#errMsg').html('').show();				
//					var fromStock = $('#fStock').val().trim();
//					if(fromStock != undefined && fromStock != null && fromStock != ''){
//					var fromStock = "";
//						var params = {fromStock:fromStock};					
//						Utils.getJSONDataByAjaxNotOverlay(params, '/equipment-stock-change-manage/getCheckFromStockEquipment', 
//							function(equipments){
//								if(equipments != undefined && equipments != null && equipments.length > 0) {
									EquipmentStockChange.showPopupSearchEquipment();
//								} else{
//									$('#errMsg').html('Kho giao không tồn tại thiết bị!').show();
//									return false;
//								}								
//							}, null, null);
//					}else{
//						$('#errMsg').html('Bạn chưa nhập Kho giao!').show();
//						return false;
//					}					
				} else if(e.keyCode==keyCodes.ENTER){
				$('#errMsg').html('').show();				
//					var fromStock = $('#fStock').val().trim();
//					if(fromStock != undefined && fromStock != null && fromStock != ''){
//						var params = {fromStock:fromStock};					
//						Utils.getJSONDataByAjaxNotOverlay(params, '/equipment-stock-change-manage/getCheckFromStockEquipment', 
//							function(equipments){
//							if(equipments != undefined && equipments != null && equipments.length > 0) {
//									var equipCode = $(equipmentCodeInput).val().trim();									
//								if(equipCode != undefined && equipCode != null && equipCode != ''){
//									var params = {equipCode:equipCode};		
//									var lstEquipId = new Array(); 
//	        						var rows = $('#gridEquipmentStockChange').datagrid('getRows');
//										if (rows != undefined && rows != null && rows.length > 1){
//										for( var i = 0, size = rows.length; i < size; i++){					
//											if(rows[i].equipmentId != undefined){
//												lstEquipId.push(rows[i].equipmentId);							
//												}					
//											}
//											params.lstEquipId = lstEquipId.join(',');
//										}			
//										var fromStock = $('#fStock').val().trim();
//											if(fromStock != undefined && fromStock != null && fromStock != ''){		
//											params.fromStock = fromStock;	
//											}
//									Utils.getJSONDataByAjaxNotOverlay(params, '/equipment-stock-change-manage/getEquipment', function(equipments){						
//									if(equipments != undefined && equipments != null && equipments.length > 0) {										
//											$('#gridEquipmentStockChange').datagrid('updateRow',{
//											index: EquipmentStockChange._editIndex,	
//											row: equipments[0]
//											});	
//											EquipmentStockChange.editInputGrid();
//										} else{
//										$('#errMsg').html('Mã thiết bị không hợp lệ!').show();											
//										$('#equipmentCodeInGrid').val('');
//										return false;
//										}
//										$('#equipmentCodeInGrid').change();	
//									}, null, null);
//								}	
//							}else{
//									$('#errMsg').html('Kho giao không tồn tại thiết bị!').show();
//									return false;
//								} 
//								//		EquipmentStockChange.editInputGrid();
//							}, null, null);
//					}else{
//						$('#errMsg').html('Bạn chưa nhập Kho giao!').show();
//						return false;
//					}	
					var equipCode = Utils.XSSEncode($(equipmentCodeInput).val().trim());									
					if(equipCode != undefined && equipCode != null && equipCode != ''){
						var params = {equipCode:equipCode};		
						var lstEquipId = new Array(); 
						var rows = $('#gridEquipmentStockChange').datagrid('getRows');
							if (rows != undefined && rows != null && rows.length > 1){
							for( var i = 0, size = rows.length; i < size; i++){					
								if(rows[i].equipmentId != undefined){
									lstEquipId.push(rows[i].equipmentId);							
									}					
								}
								params.lstEquipId = lstEquipId.join(',');
							}			
							
						Utils.getJSONDataByAjaxNotOverlay(params, '/equipment-stock-change-manage/getEquipment', function(equipments){						
						if(equipments != undefined && equipments != null && equipments.length > 0) {										
								$('#gridEquipmentStockChange').datagrid('updateRow',{
								index: EquipmentStockChange._editIndex,	
								row: equipments[0]
								});	
								EquipmentStockChange.editInputGrid();
							} else{
							$('#errMsg').html('Mã thiết bị không hợp lệ!').show();											
							$('#equipmentCodeInGrid').val('');
							return false;
							}
							$('#equipmentCodeInGrid').change();	
						}, null, null);
					}
				}
			});	
			
			//Event F9 ma Kho nhận
			$('input[id*=toStockInGrid]').bind('keyup', function(e){
				var toStockCodeInput = this;
				if(e.keyCode==keyCode_F9){			
					$('#errMsg').html('').show();				
					EquipmentStockChange.showPopupSearchStock();
				} else if(e.keyCode==keyCodes.ENTER){
//					$('#errMsg').html('').show();				
//					var fromStock = $('#fStock').val().trim();
//					if(fromStock != undefined && fromStock != null && fromStock != ''){
//						var params = {fromStock:fromStock};					
//						Utils.getJSONDataByAjaxNotOverlay(params, '/equipment-stock-change-manage/getCheckFromStockEquipment', 
//							function(equipments){
//							if(equipments != undefined && equipments != null && equipments.length > 0) {
//									var equipCode = $(equipmentCodeInput).val().trim();									
//								if(equipCode != undefined && equipCode != null && equipCode != ''){
//									var params = {equipCode:equipCode};		
//									var lstEquipId = new Array(); 
//	        						var rows = $('#gridEquipmentStockChange').datagrid('getRows');
//										if (rows != undefined && rows != null && rows.length > 1){
//										for( var i = 0, size = rows.length; i < size; i++){					
//											if(rows[i].equipmentId != undefined){
//												lstEquipId.push(rows[i].equipmentId);							
//												}					
//											}
//											params.lstEquipId = lstEquipId.join(',');
//										}			
//										var fromStock = $('#fStock').val().trim();
//											if(fromStock != undefined && fromStock != null && fromStock != ''){		
//											params.fromStock = fromStock;	
//											}
//									Utils.getJSONDataByAjaxNotOverlay(params, '/equipment-stock-change-manage/getEquipment', function(equipments){						
//									if(equipments != undefined && equipments != null && equipments.length > 0) {										
//											$('#gridEquipmentStockChange').datagrid('updateRow',{
//											index: EquipmentStockChange._editIndex,	
//											row: equipments[0]
//											});	
//											EquipmentStockChange.editInputGrid();
//										} else{
//										$('#errMsg').html('Mã thiết bị không hợp lệ!').show();											
//										$('#equipmentCodeInGrid').val('');
//										return false;
//										}
//										$('#equipmentCodeInGrid').change();	
//									}, null, null);
//								}	
//							}else{
//									$('#errMsg').html('Kho giao không tồn tại thiết bị!').show();
//									return false;
//								} 
//								//		EquipmentStockChange.editInputGrid();
//							}, null, null);
//					}else{
//						$('#errMsg').html('Bạn chưa nhập Kho giao!').show();
//						return false;
//					}									
				}
			});	
			
			var serialInputEventHandler = function(element) {
				var serialInput = element;
				$('#errMsg').html('').show();	
				var fromStock = '';
//				var fromStock = $('#fStock').val().trim();
//				if(fromStock != undefined && fromStock != null && fromStock != ''){
					var params = {fromStock:fromStock};					
					Utils.getJSONDataByAjaxNotOverlay(params, '/equipment-stock-change-manage/getCheckFromStockEquipment', 
						function(equipments){
							if(equipments != undefined && equipments != null && equipments.length > 0) {
								// if(e.keyCode==keyCodes.ENTER){
									var seriNumber = $(serialInput).val().trim();
									if(seriNumber != undefined && seriNumber != null && seriNumber != ''){
									var params = {seriNumber:seriNumber};	
									var lstEquipId = new Array(); 
        							var rows = $('#gridEquipmentStockChange').datagrid('getRows');
									if (rows != undefined && rows != null && rows.length > 1){
									for( var i = 0, size = rows.length; i < size; i++){					
										if(rows[i].equipmentId != undefined){
											lstEquipId.push(rows[i].equipmentId);							
											}					
										}
										params.lstEquipId = lstEquipId.join(',');
									}	
//									var fromStock = $('#fStock').val().trim();
										if(fromStock != undefined && fromStock != null && fromStock != ''){		
										params.fromStock = fromStock;	
										}
									Utils.getJSONDataByAjaxNotOverlay(params, '/equipment-stock-change-manage/getEquipment', function(equipments){						
										if(equipments != undefined && equipments != null) {	
										if(equipments.length == 1){
											equipments[0].contentDelivery = null;
											$('#gridEquipmentStockChange').datagrid('updateRow',{
											index: EquipmentStockChange._editIndex,	
											row: equipments[0]
										});							
										EquipmentStockChange.editInputGrid();
										}else if(equipments.length > 1){
											EquipmentStockChange.showPopupSelectEquipment(equipments);
											}else{
											$('#errMsg').html('Mã seri không tồn tại!').show();
											$('#seriNumberInGrid').val('');
											return false;
											}
										} else{
											$('#seriNumberInGrid').val('');
										}
										$('#seriNumberInGrid').change();
									}, null, null);					
									}				
								// }
							} else{
								$('#errMsg').html('Kho giao không tồn tại!').show();
								return false;
							}
							//		EquipmentStockChange.editInputGrid();
						}, null, null);
//				}else{
//					$('#errMsg').html('Bạn chưa nhập Kho giao!').show();
//					return false;
//				}
			};
			//Event Enter so seri	
			if($('#equipmentCodeInGrid').val() == ''){
					$('#seriNumberInGrid')
				/*.bind('blur', function(e){
					if (!$(this).hasClass('keyup-ing')) {
						serialInputEventHandler(this);
					}
					$(this).removeClass('keyup-ing');
				})*/
			.bind('keyup', function(e){
				if (e.keyCode == keyCodes.ENTER) {
					$(this).addClass('keyup-ing');
					serialInputEventHandler(this);
				}				
			});	
			}					
	}, 
	/**
	 * show popup chon file excel
	 * @author phuongvm
	 * @since 06/04/2015
	 */
	showDlgCatalogImportExcel: function(){
		$('#easyuiPopupImportExcel').show();
		$('.ErrorMsgStyle').hide();
		$('#easyuiPopupImportExcel').dialog({  
	        closed: false,  
	        cache: false,  
	        modal: true,
	        onOpen: function(){	 
	        	$('.easyui-dialog .BtnGeneralStyle').css('color','');
	        },
	        onClose: function(){
	        	EquipmentManageCatalog.searchinfo();
				$("#grid").datagrid('uncheckAll');
				$("#fakefilepcCatalog").val("");
				$("#excelFileCatalog").val("");				
	        }
		});
	},
	/**
	 * nhap file excel 
	 * @author phuongvm
	 * @since 06/04/2015
	 */
	importExcelCatalog: function(){
		$('.ErrorMsgStyle').hide();
		VTUtilJS.importExcelUtils(function(data) {
			$("#errExcelMsgCatalog").html(data.message).show();		
			if(data.fileNameFail == null || data.fileNameFail == "" ){					
				setTimeout(function(){
					$('#errExcelMsgCatalog').html("").hide();
					$('#easyuiPopupImportExcel').dialog('close');
				}, 3000);				
			}else{
				$("#fakefilepcCatalog").val("");
				$("#excelFileCatalog").val("");			
				$('#gridStockChange').datagrid('reload');	
			}						
		}, "importFrmCatalog", "excelFileCatalog", null, "errExcelMsgCatalog");
		return false;
	},
	/**
	 * xuat file excel 
	 * @author phuongvm
	 * @since 06/04/2015
	 */
	exportExcel: function(){
		var msg = '';
		var code = $('#codeStockTran').val().trim();
		var status = $('#status').val().trim();
		var statusPerform = $('#statusPerform').val().trim();
		var fDate = $('#fDate').val();
		var tDate = $('#tDate').val();
		var cFDate = $('#cFDate').val();
		var cTDate = $('#cTDate').val();
		var tStock = $('#tStock').val().trim();
		var fStock = $('#fStock').val().trim();
		if (msg.length == 0) {
			msg = Utils.getMessageOfSpecialCharactersValidate('codeStockTran', 'Mã biên bản', Utils._CODE);
		}
		if (msg.length == 0 && $('#fDate').val().trim().length > 0 && !Utils.isDate($('#fDate').val(), '/') && $('#fDate').val() != '__/__/____') {
			msg = 'Từ ngày không tồn tại hoặc không đúng định dạng dd/mm/yyyy.';
			err = '#fDate';
		}
		if (msg.length == 0 && $('#tDate').val().trim().length > 0 && !Utils.isDate($('#tDate').val(), '/') && $('#tDate').val() != '__/__/____') {
			msg = 'Đến ngày không tồn tại hoặc không đúng định dạng dd/mm/yyyy.';
			err = '#tDate';
		}
		if (msg.length == 0 && fDate.length > 0 && tDate.length > 0 && !Utils.compareDate(fDate, tDate)) {
			msg = 'Từ ngày tạo phải là ngày trước hoặc cùng ngày với đến ngày tạo';
		}

		if (msg.length == 0 && $('#cFDate').val().trim().length > 0 && !Utils.isDate($('#cFDate').val(), '/') && $('#cFDate').val() != '__/__/____') {
			msg = 'Từ ngày không tồn tại hoặc không đúng định dạng dd/mm/yyyy.';
			err = '#cFDate';
		}
		if (msg.length == 0 && $('#cTDate').val().trim().length > 0 && !Utils.isDate($('#cTDate').val(), '/') && $('#cTDate').val() != '__/__/____') {
			msg = 'Đến ngày không tồn tại hoặc không đúng định dạng dd/mm/yyyy.';
			err = '#cTDate';
		}
		if (msg.length == 0 && cFDate.length > 0 && tDate.length > 0 && !Utils.compareDate(cFDate, cTDate)) {
			msg = 'Từ ngày phải là ngày trước hoặc cùng ngày với đến ngày';
		}
		if (msg.length > 0) {
			$('#errMsg').html(msg).show();	
			setTimeout(function() {
				$('#errMsg').hide();
			}, 3000);
			return false;
		}
		var params =  new Object();
		params.code = code;
		params.status = status;
		params.statusPerform = statusPerform;
		params.fromDate = fDate;
		params.toDate = tDate;
		params.createFromDate = cFDate;
		params.createToDate = cTDate;
		params.toStock = tStock;
		params.fromStock = fStock;
		$.messager.confirm('Xác nhận', 'Bạn có muốn xuất file excel?', function(r) {
			if (r) {
				ReportUtils.exportReport('/equipment-stock-change-manage/export-excel', params, 'errMsg');					
			}
		});		
		return false;
	},
	/*** BEGIN VUONGMQ * @date Mar 16,215 QUAN LY KHO THIET BI */
	/**
	 * Tim kiem danh sach thiet bi
	 * @author VuongMQ
	 * @date Mar 16,215
	 * */
	searchListEquipmentStock: function () {
		$('.SuccessMsgStyle').html('').hide();
		$('.ErrorMsgStyle').html('').hide();
		$('#equipHistoryTemplateDiv').css("visibility","hidden").hide();
		var msg = '';
		var err = '';
		var fDate = $('#fDate').val();
		var tDate = $('#tDate').val();
		if(msg.length == 0 && fDate.length > 0 && tDate.length > 0 && !Utils.compareDate(fDate, tDate)){
			msg = 'Từ ngày phải là ngày trước hoặc cùng ngày với đến ngày';
			err = '#fDate';
		}
		if(msg.length > 0){
			$('#errMsg').html(msg).show();
			$(err).focus();
			$('#listEquipmentDg').datagrid('loadData', []);
			return false;
		}
		EquipmentStockChange._params = {
			shopCode: $('#txtShopCode').val(),
			code: $('#txtCode').val(),
			name: $('#txtName').val(),
			status: $('#ddlUsageStatus').val(),
			fromDate: $('#fDate').val(),
			toDate: $('#tDate').val()
		};
		$('#listEquipmentDg').datagrid('load', EquipmentStockChange._params);
	},
	/**
	 * Xuat Excel Theo Tim kiem danh sach kho thiet bi
	 * @author VuongMQ
	 * @date Mar 16,215
	 * */
	exportExcelBySearchEquipmentStock: function() {
		//alert('Xuất Excel nè');
		$('.SuccessMsgStyle').html('').hide();
		$('.ErrorMsgStyle').html('').hide();
		var msg = '';
		if(EquipmentStockChange._params != null){
			var rows = $('#listEquipmentDg').datagrid('getRows');
			if(rows == null || rows.length==0){
				msg = "Không có dữ liệu xuất Excel";
			}
		}else{
			msg = "Không có dữ liệu xuất Excel";
		}
		if(msg.length > 0){
			$('#errMsg').html(msg).show();
			return false;
		}
		var msg = 'Bạn có muốn xuất Excel?';
		$.messager.confirm('Xác nhận', msg, function(r){
			if (r){
				ReportUtils.exportReport('/equipment-manage/exportExcelBySearchEquipmentStock', EquipmentStockChange._params, 'errMsg');
			}
		});
	},

	/**
	 * Mo Dialog Nhap Excel kho thiet bi
	 * @author VuongMQ
	 * @date Mar 16,215
	 * */
	openDialogImportListEquipmentStock: function (){
		//alert('Nhập Excel nè');
		$('.SuccessMsgStyle').html('').hide();
		$('.ErrorMsgStyle').html('').hide();
		$('#easyuiPopupImportExcel').dialog({
			title: 'Nhập Excel Kho thiết bị',
			width: 500, 
			height: 'auto',
			closed: false,
			cache: false,
			modal: true,
			onOpen: function() {
				$('#fakefilepcEquipStock').val('');
			},
			onClose: function() {
				$('.ErrorMsgStyle').html('').hide();
			}
		});
	},
	/**
	 * Import Excel kho thiet bi
	 * @author vuongmq
	 * @date Mar 20, 2015
	 * */
	importEquipmentStock: function(){
		$('.SuccessMsgStyle').html('').hide();
		$('.ErrorMsgStyle').html('').hide();
		if($('#fakefilepcEquipStock').val().length <= 0 ){
			$('#errExcelMsgEquipStock').html('Vui lòng chọn tập tin Excel').show();
			return false;
		}
		Utils.importExcelUtils(function(data){
			//EquipmentListEquipment.searchListEquipment();
			$('#fakefilepcEquipStock').val("").change();
			if (data.message!=undefined && data.message!=null && data.message.trim().length >0){
				$('#errExcelMsgEquipStock').html(data.message.trim()).change().show();
			}else{
				$('#successExcelMsgEquipStock').html("Lưu dữ liệu thành công").show();
				var tm = setTimeout(function(){
					$('.SuccessMsgStyle').html('').hide();
					clearTimeout(tm);
					$('#easyuiPopupImportExcel').dialog('close');
					$('#listEquipmentDg').datagrid("load", EquipmentStockChange._params);
				 }, 1500);
			}
		}, 'importFrmEquipStock', 'excelFileEquipStock', 'errExcelMsgEquipStock');
		
	},
	/**
	 * Tai tap tin Excel Import Danh sach kho thiet bi
	 * @author VuongMQ
	 * @date Mar 16,215
	 * @return Excel
	 * */
	/*downloadTemplateImportListEquipStock: function() {
		//alert('Tải file mẫu nè');
		$('.SuccessMsgStyle').html('').hide();
		$('.ErrorMsgStyle').html('').hide();
		var msg = 'Bạn có muốn tải tập tin Import?';
		ReportUtils.exportReport('/equipment-manage/downloadTemplateImporEquipment', {}, 'errMsg');
		
	},*/
	/**
	 *  view popup unit tree cho kho thiet bi
	 * @author VuongMQ
	 * @date Mar 16,215
	 * @return Excel
	 * */
	viewPopupUnit: function(type) {
		//EquipmentStockChange.searchUnit();
		// $('#popup-unit-tree .dialogUnitTreeContent').show();
		// $('#popup-unit-tree .ErrorMsgStyle').html('').hide();
		// $('#popup-unit-tree .SuccessMsgStyle').html('').hide();
		// $('#popup-unit-tree').dialog({
		// 	title : 'Tìm kiếm đơn vị',
		// 	width : 650,
		// 	height : 'auto',
		// 	closed : false,
		// 	cache : false,
		// 	modal : true,
		// 	onOpen : function() {
				//$('.easyui-dialog .InputTextStyle').val('');
				// $('#popup-unit-tree .InputTextStyle').val('');
				// $("#popup-unit-tree").addClass("easyui-dialog");
				// $("#popup-unit-tree #codeDlg").focus();
				
				// $("#popup-unit-tree #gridDlg").treegrid({
				// 	url: "/equipment-manage/search-shop-dialog?shopId=0",
				// 	rownumbers: false,
				// 	width: $("#popup-unit-tree #gridDlgDiv").width(),
				// 	height: 320,
				// 	fitColumns: true,
				// 	idField: "nodeId",
				// 	treeField: "text",
				// 	selectOnCheck: false,
				// 	checkOnSelect: false,
				// 	columns: [[
				// 		{field:"text", title:"Mã đơn vị", sortable: false, resizable: false, width:120, align:"left", formatter:CommonFormatter.formatNormalCell},
				// 		/*{field:"quantity", title:"Số suất", sortable: false, resizable: false, width:130, fixed:true, align:"left", formatter: function(v, r) {
				// 			if (r.attr.isNPP) {
				// 				if (r.attr.isExists) {
				// 					return "<input type='text' id='txt-p"+r.attr.id+"' class='qttClass' maxlength='9' disabled='disabled' exists='1' />";
				// 				}
				// 				return "<input type='text' id='txt-p"+r.attr.id+"' class='qttClass' maxlength='9' exists='0' />";
				// 			}
				// 			return "";
				// 		}},*/
				// 		{field:"ck", title:"", sortable: false, resizable: false, width:60, fixed:true, align:"center", formatter: function(v, r) {
				// 			var pId = r.attr.parentId;
				// 			if (pId == undefined || pId == null) {
				// 				pId = 0;
				// 			}
				// 			return '<a href="javascript:void(0)" onclick="return EquipmentStockChange.selectShopCode(\''+type+'\',\''+r.attr.shopCode+'\');">Chọn</a>';
				// 		}}
				// 	]],
				// 	onLoadSuccess: function(data) {
				// 		$("#popup-unit-tree #gridDlg").datagrid("resize");
				// 		Utils.bindFormatOnTextfieldInputCss('qttClass', Utils._TF_NUMBER, "#popup-unit-tree");
				// 		$('#popup-unit-tree .qttClass').each(function() {
				//       		//_formatCurrencyFor($(this).attr('id'));
				//       	});
				// 		setTimeout(function(){
				// 			var hDlg=parseInt($("#popup-unit-tree").parent().height());
				// 			var hW=parseInt($(window).height());
				// 			var d=hW-hDlg;
				// 			d=d/2+document.documentElement.scrollTop;
				// 			if (d < 0) { d = 0; }
				// 			$("#popup-unit-tree").parent().css('top',d);
			 //    		},1000);
				// 	}
				// });
				
				
				// $("#popup-unit-tree #btnSearchDlg").click(function() {
				// 	var p = {
				// 			code: $("#popup-unit-tree #codeDlg").val().trim(),
				// 			name: $("#popup-unit-tree #nameDlg").val().trim()
				// 	};
				// 	$("#popup-unit-tree #gridDlg").treegrid("reload", p);
				// });
				// $("#popup-unit-tree #codeDlg").parent().keyup(function(event) {
				// 	if (event.keyCode == keyCodes.ENTER) {
				// 		$("#popup-unit-tree #btnSearchDlg").click();
				// 	}
				// });
		// 	},
		// 	onClose : function() {
		// 		// $("#popup-unit-tree #gridDlg").treegrid('options').queryParams = null;
		// 		//$('#popup-unit-tree').html(html);
		// 		//$('#popup-unit-tree').hide();
		// 	}
		// });
				var txt = "shopCodeDl";
				$('#common-dialog-search-tree-in-grid-choose-single').dialog({
					title: "Tìm kiếm Đơn vị",
					closed: false,
			        cache: false, 
			        modal: true,
			        width: 680,
			        height: 550,
			        onOpen: function(){
			        	$('#txtShopCodeDlg').val("");
			        	$('#txtShopNameDlg').val("");
			        	//Tao cay don vi
			        	$('#commonDialogTreeShopG').treegrid({  
			        		data: StockManager._arrRreeShopTocken,
			        		url : '/cms/searchTreeShop',
			    		    width: 650,
			    		    height: 325,
			    		    queryParams: {code: $('#txtShopCodeDlg').val().trim(), name: $('#txtShopNameDlg').val().trim()},
			    			fitColumns : true,
			    			checkOnSelect: false,
			    			selectOnCheck: false,
			    		    rownumbers : true,
			    	        idField: 'id',
			    	        treeField: 'code',
			    		    columns:[[
			    		        {field:'code',title:work_date_unit_code,resizable:false, width:200, align:'left', formatter:function(value, row, index){
			    		        	return Utils.XSSEncode(value);
			    		        }},
			    		        {field:'name',title:work_date_unit_name,resizable:false, width:200, align:'left', formatter:function(value, row, index){
			    		        	return Utils.XSSEncode(value);
			    		        }},
			    		        {field:'choose',title:'',resizable:false, width:60, align:'center', fixed:true, formatter:function(value, row, index){
			    		        	return '<a href="javascript:void(0)" onclick="WorkingDatePlan.fillCodeForInputTextByDialogSearch2(\''+Utils.XSSEncode(txt)+'\', ' + row.id +', \''+Utils.XSSEncode(row.code)+'\', \''+Utils.XSSEncode(row.name)+'\' ,\''+Utils.XSSEncode(Utils._isShowInputTextByDlSearch2.code)+'\');">'+work_date_chon+'</a>';   
			    		        }}
			    		    ]],
			    	        onLoadSuccess :function(data){
			    				$('.datagrid-header-rownumber').html('STT');
			    				Utils.updateRownumWidthAndHeightForDataGrid('commonDialogTreeShopG');
			    	        }
			    		});
			        	$('#txtShopCodeDlg').focus();
			        },
			        onClose:function() {
			        	
			        }
				});
	},
	selectShopCode: function(type, code){
		if(type == EquipmentStockChange._FormSearch){
			$('#shopCode').val(code);
		} else{
			$('#shopCodeDl').val(code);
		}
		$('#popup-unit-tree').dialog('close');
		return false;
	},
	/**
	 *  view dialog Them moi kho thiet bi
	 * @author VuongMQ
	 * @date Mar 16,215
	 * */
	dialogAddOrUpdateEquipmentStock: function(id, index){
		$(".ErrorMsgStyle").html('').hide();
		$(".SuccessMsgStyle").html('').hide();
		//$('#gridSearchBillContGrid').html('<table id="gridSearchBill"></table>').change();
		var titleView = 'Thêm mới kho';
		if(id != undefined && id != null && index != undefined && index != null){
			titleView = 'Chỉnh sửa kho';
		}
		$('#dialogEquipStockChange').dialog({
			title: titleView,
			closed: false,
			cache: false,
			modal: true,
	        width: 670,
	        //height: 'auto',
	        height: 250,
	        onOpen: function(){
	        	$('.easyui-dialog .InputTextStyle').val('');
	    		/**shopCodeDl don vị*/
	    		/**combobox status*/
	    		$('#dialogEquipStockChange .MySelectBoxClass').css('width', '173');
	    		$('#dialogEquipStockChange .CustomStyleSelectBox').css('width', '140');
	    		/** truong hop chinh sua*/
	    		if(id != undefined && id != null && index != undefined && index != null){
	    			$('#idStock').val(id);
	    			var data = $('#listEquipmentDg').datagrid('getRows')[index];
	    			$('#shopCodeDl').attr('disabled', 'disabled');
	    			$('#txtCodeDl').attr('disabled', 'disabled');
	    			if(data.shop != undefined && data.shop != null){
	    				$('#shopCodeDl').val(data.shop.shopCode);	
	    			}
	    			$('#txtCodeDl').val(data.code);
					$('#txtNameDl').val(data.name);
					$('#txtDescriptionDl').val(data.description);
					$('#txtOrdinalDl').val(data.ordinal);
					if(data.status != null && activeType.parseValue(data.status) == activeType.RUNNING){
		    			$('#statusDl').val(activeType.RUNNING).change();
			    	} else if(data.status != null && activeType.parseValue(data.status) == activeType.STOPPED){
			    		$('#statusDl').val(activeType.STOPPED).change();
			    	}
			    	$('#txtNameDl').focus();
	    		} else {
	    			/** truong hop them moi*/
	    			$('#shopCodeDl').removeAttr('disabled');
	    			$('#txtCodeDl').removeAttr('disabled');
	    			$('#idStock').val('');
	    			$('#statusDl').val(activeType.RUNNING).change();
	    			$('#shopCodeDl').focus();
	    		}
	    		
	    		
	    		$('#dialogEquipStockChange #btnSaveEquipStock').unbind('click');
	    		$('#dialogEquipStockChange #btnSaveEquipStock').bind('click',function(event) {
	    			EquipmentStockChange.addOrUpdateEquipmentStock(id);
				});
				$('#shopCodeDl').bind('keyup', function(event) {
					 if(event.keyCode == keyCode_F9){
						 EquipmentStockChange.viewPopupUnit(EquipmentStockChange._FormAdd);
					}
				});
	        },
	        onClose:function() {
	        	
	        }
		});
	},
	/**
	 *  Them moi kho thiet bi
	 * @author VuongMQ
	 * @date Mar 16,215
	 * */
	addOrUpdateEquipmentStock: function(id){
		$(".ErrorMsgStyle").html('').hide();
		$(".SuccessMsgStyle").html('').hide();
		var msg ="";
		if(msg.length == 0){
			msg = Utils.getMessageOfRequireCheck('shopCodeDl', 'Mã đơn vị');
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfRequireCheck('txtCodeDl', 'Mã kho');
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfSpecialCharactersValidate('txtCodeDl', 'Mã kho', Utils._CODE);
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfRequireCheck('txtNameDl', 'Tên kho');
		}
		if(msg.length == 0){
			if($('#statusDl').val() == undefined || $('#statusDl').val() == null){
				msg = 'Trạng thái không được để trống';
			}
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfRequireCheck('txtOrdinalDl', 'Thứ tự');
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfInvaildInteger('txtOrdinalDl', 'Thứ tự');
			if(msg.length == 0){
				if($('#txtOrdinalDl').val() != undefined &&  $('#txtOrdinalDl').val() != null && $('#txtOrdinalDl').val() <=0){
					msg = 'Giá trị thứ tự phải lớn hơn 0';
				}
			}
		}

		if(msg.length >0){
			$('#errorMsgDl').html(msg).show();
			return false;
		}
		var params = new Object();	
		if(id != undefined && id != null){
			params.id = id;
		}
		params.shopCode = $('#shopCodeDl').val().trim();
		params.code = $('#txtCodeDl').val().trim();
		params.name = $('#txtNameDl').val().trim();
		params.status = $('#statusDl').val().trim();
		params.description = $('#txtDescriptionDl').val().trim();
		params.ordinal = $('#txtOrdinalDl').val().trim();
		var msgConfirm = 'Bạn có muốn thêm mới kho thiết bị?';
		Utils.addOrSaveData(params, '/equipment-manage/saveOrUpdateStock', null,  'errorMsgDl', function(data) {
			$('#successMsgDl').html("Lưu dữ liệu thành công").show();
			var tm = setTimeout(function(){
				//Load lai danh sach kho thiet bi
				$('#dialogEquipStockChange').dialog('close');
				//$('#btnSearch').click();
				$('#listEquipmentDg').datagrid('reload');
				$('.SuccessMsgStyle').html("").hide();
				clearTimeout(tm);
			 }, 1500);
		}, null, null, null,msgConfirm);
		return false;
	},

	/**
	 *  view dialog chinh sua kho thiet bi
	 * @author VuongMQ
	 * @date Mar 16,215
	 * */
	dialogChangeEquipmentStock: function(id, index){

	},
	/**
	 *  chinh sua kho thiet bi
	 * @author VuongMQ
	 * @date Mar 16,215
	 * */
	changeEquipmentStock: function(){

	},
	/*** END VUONGMQ * @date Mar 16,215 QUAN LY KHO THIET BI */
};

var EquipmentManagerLiquidation = {
		_editIndex:null,
		_eIndex: null,
		_lstEquipInRecord:null,
		_numberEquipInRecord:null,
		_lstEquipInsert:null,	
		_lstRowId: null,
		_countFile:0,
		_arrFileDelete:null,
		_DISABLED: 'DISABLED',
		_ENABLE: 'ENABLE',
		
	/**
	* Validate gird
	* @author hoanv25
	* @since May 29,2015
	*/	
	endEditing: function() {
			if (EquipmentManagerLiquidation._eIndex == undefined) {
				return true;
			}
			if ($('#gridEquipmentLiquidation').datagrid('validateRow', EquipmentManagerLiquidation._eIndex)){
				$('#gridEquipmentLiquidation').datagrid('endEdit', EquipmentManagerLiquidation._eIndex);
				EquipmentManagerLiquidation._eIndex = undefined;
				return true;
			} else {
				return false;
			}
	},
	/**
	 * Tim kiem danh sach bien ban giao nhan
	 * @author nhutnn
	 * @since 12/12/2014
	 */
	searchLiquidation: function(){

		$('#errMsgInfo').html('').hide();
		var msg ='';
		var fdate = $('#fDate').val().trim();
		if(msg.length == 0 && fdate != '' && !Utils.isDate(fdate) && fdate != '__/__/____'){
			msg = 'Từ ngày không tồn tại hoặc không đúng định dạng dd/mm/yyyy.';
		}
		var tdate = $('#tDate').val().trim();
		if(msg.length == 0 && tdate != '' && !Utils.isDate(tdate) && tdate != '__/__/____'){
			msg = 'Đến ngày không tồn tại hoặc không đúng định dạng dd/mm/yyyy.';
		}
		if(msg.length == 0 && fdate != '' && tdate != '' && !Utils.compareDate(fdate, tdate)){
			msg = 'Đến ngày không được nhỏ hơn Từ ngày';	
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfSpecialCharactersValidate('recordCode','Mã biên bản');
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfSpecialCharactersValidate('docNumber','Số CV/ Tờ trình thanh lý');
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfSpecialCharactersValidate('equipCode','Mã thiết bị');
		}
		
		if(msg.length > 0){
			$('#errMsgInfo').html(msg).show();
			return false;
		}
		
		var params=new Object(); 
		params.fromDate = $('#fDate').val().trim();
		params.toDate = $('#tDate').val().trim();
		params.docNumber = $('#docNumber').val().trim();		
		params.code = $('#recordCode').val().trim();
		params.equipCode = $('#equipCode').val().trim();
		params.status = $('#statusRecord').val().trim();
		
		EquipmentManagerLiquidation._lstRowId = new Array();
		$('#gridLiquidation').datagrid('load',params);
		$('#gridLiquidation').datagrid('uncheckAll');
	},
	/**
	 * show popup import excel
	 * @author nhutnn
	 * @since 12/12/2014
	 */
	showDlgImportExcel: function(){
		$('#easyuiPopupImportExcel').show();
		$('.ErrorMsgStyle').hide();
		$('#easyuiPopupImportExcel').dialog({  
	        closed: false,  
	        cache: false,  
	        modal: true,
	        height: 'auto',
	        onOpen: function(){	 
	        	$('.easyui-dialog .BtnGeneralStyle').css('color','');
	        },
	        onClose: function(){
	        	EquipmentManagerLiquidation.searchLiquidation();
				$("#gridLiquidation").datagrid('uncheckAll');
				$("#fakefilepcBBTL").val("");
				$("#excelFileBBTL").val("");				
	        }
		});
	},
	/**
	 * import excel BBTL
	 * @author nhutnn
	 * @since 12/12/2014
	 */
	importExcelBBTL: function(){
		$('.ErrorMsgStyle').hide();
		VTUtilJS.importExcelUtils(function(data) {
			$("#errExcelMsgBBTL").html(data.message).show();		
			if (data.fileNameFail == null || data.fileNameFail == "" ) {					
				setTimeout(function(){
					$('#errExcelMsgBBTL').html("").hide();
					$('#easyuiPopupImportExcel').dialog('close');
				}, 3000);				
			} else {
				$("#fakefilepcBBTL").val("");
				$("#excelFileBBTL").val("");			
				//$('#gridLiquidation').datagrid('reload');	
			}						
		}, "importFrmBBTL", "excelFileBBTL", null, "errExcelMsgBBTL");
		return false;
	},
	/**
	 * xuat file excel BBTL
	 * @author nhutnn
	 * @since 12/12/2014
	 */
	exportExcelBBTL: function() {
		$('.ErrorMsgStyle').html("").hide();
		var lstChecked = EquipmentManagerLiquidation._lstRowId;
		var rows = $('#gridLiquidation').datagrid('getRows');	
		if (rows.length==0) {
			$('#errMsgInfo').html('Không có biên bản để xuất excel').show();
			return;
		}
		if (lstChecked.length == 0) {
			$('#errMsgInfo').html('Bạn chưa chọn biên bản để xuất excel. Vui lòng chọn biên bản').show();
			return;
		}	
		
		var params = new Object();
		params.lstIdRecord = lstChecked;
		$.messager.confirm('Xác nhận','Bạn có muốn xuất file excel?',function(r){
			if(r){
				ReportUtils.exportReport('/equipment-manage-liquidation/export-record-liquidation', params, 'errMsgInfo');					
			}
		});		
		return false;
	},
	
	/**
	 * Luu bien ban ban giao
	 * @author nhutnn
	 * @since 12/12/2014
	 */
	saveLiquidation: function() {		
		$('#errMsgInfo').html('').hide();
		var rows = $('#gridLiquidation').datagrid('getRows');
		if(rows.length == 0){
			$('#errMsgInfo').html('Không có dữ liệu để lưu').show();
			return false;
		}
		var lstRecordSuccess = new Array();
		//var lstCheck = $('#gridLiquidation').datagrid('getChecked');
		var lstCheck = EquipmentManagerLiquidation._lstRowId;
		if(lstCheck.length > 0){
			var statusRecordChange = $('#statusRecordLabel').val().trim();
			for(var i=0; i < lstCheck.length; i++){		
				if(statusRecordChange != "" ){
					// lstRecordSuccess.push(lstCheck[i].idLiquidation);
					lstRecordSuccess.push(lstCheck[i]);
				}
			}	
			if(lstRecordSuccess.length <= 0){
				$('#errMsgInfo').html('Bạn chưa chọn trạng thái để lưu!').show();
				return;
			}
			var params = new Object();
			params.lstIdForm = lstRecordSuccess;
			params.status = statusRecordChange;
			
			Utils.addOrSaveData(params, '/equipment-manage-liquidation/save-liquidation', null, 'errMsgInfo', function(data) {
				if(data.error){
					$('#errMsgInfo').html(data.errMsg).show();
					return;
				}
				if (data.lstRecordNotSendMail != undefined && data.lstRecordNotSendMail != null && data.lstRecordNotSendMail != '') {
					$('#errMsgInfo').html('Gửi Email không thành công biên bản báo mất: ' + data.lstRecordNotSendMail).show();
				}
				if(data.lstRecordError != null && data.lstRecordError.length>0){					
					VCommonJS.showDialogList({	
						dialogInfo: {
							title: 'Lỗi'
						},	   
						data : data.lstRecordError,			   
						columns : [[
							{field:'formCode', title:'Mã biên bản', align:'left', width: 150, sortable:false, resizable:false},
							{field:'formStatusErr', title:'Trạng thái biên bản', align:'center', width: 80, sortable:false, resizable:false,formatter: function(value, row, index){
								var html="";
								if(row.formStatusErr == 1){
									html = '<img src="/resources/images/icon_delete.png" height="20" width="20" style="margin-right:5px;" title="Error">';
								}
								return html;
								
							}}
							// {field:'periodError', title:'Kỳ của biên bản', align:'center', width:80, sortable:false, resizable:false,formatter: function(value, row, index){
							// 	var html="";
							// 	if(row.periodError == 1){
							// 		html = '<img src="/resources/images/icon_delete.png" height="20" width="20" style="margin-right:5px;" title="Error">';
							// 	}
							// 	return html;
								
							// }}
						]]
					});
				}else{
					$('#successMsgInfo').html('Lưu dữ liệu thành công').show();
				}
				setTimeout(function(){ 
					$('#successMsgInfo').html('').hide();
				}, 5000);
				
				$('#gridLiquidation').datagrid('reload');
				$('#gridLiquidation').datagrid('uncheckAll');
			}, null, null, null, "Bạn có muốn cập nhật ?", null);	
		}else{
			$('#errMsgInfo').html('Bạn chưa chọn biên bản để lưu').show();
			return false;
		}		
	},
	
	/**
	 * validate tong gia tri thanh ly va thu hoi cua gridEquipmentLiquidation so voi tong
	 * @author vuongmq
	 @date: 07/07/2015
	 */
	validateTongGiaTriThanhLyThuHoi: function(value) {
		//vi kiem tra tong gia tri thanh ly va thu hoi khong duoc lon hon tong nguyen gia, ko can type
		value = Utils.returnMoneyValue(value);
		var rs = $("#gridEquipmentLiquidation").datagrid("getRows");
		var price = 0;
		if (rs != null && rs.length > 0) {
			var r = null;
			for (var i = 0, sz = rs.length; i < sz; i++) {
				// cong don nguyen gia
				r = rs[i];
				if (r.price) {
					price = price + Number(r.price);
				}
			}
			if (Number(value) > Number(price)) {
				return false;
			}
		} else {
			// truong hop xoa khong con dong nao
			if (Number(value) > 0 ) {
				return false;
			}
		}
		return true;
	},

	/**
	 * Load lai footer cua gridEquipmentLiquidation
	 * @author vuongmq
	 @date: 07/07/2015
	 */
	refreshFooter: function() {
		var rs = $("#gridEquipmentLiquidation").datagrid("getRows");
		var price = 0;
		var liquidationValue = 0; 
		var evictionValue = 0;
		if (rs != null && rs.length > 0) {
			var r = null;
			for (var i = 0, sz = rs.length; i < sz; i++) {
				r = rs[i];
				if (r.price) {
					price = price + Number(r.price);
				}
				// cong don theo gia tri thanh ly
				var valEquipTL = $("#gridEquipmentLiquidation").datagrid('getEditor', {index:i,field:'liquidationValue'});
				if (valEquipTL != undefined && valEquipTL !=null) {
					var gtThanhLy = $(valEquipTL.target).val();
					if (gtThanhLy != undefined && gtThanhLy != null && gtThanhLy != '') {
						liquidationValue = liquidationValue + Number(Utils.returnMoneyValue(gtThanhLy));
					}
				} else {
					var gtThanhLy = r.liquidationValue;
					if (gtThanhLy != undefined && gtThanhLy != null && gtThanhLy != '') {
						liquidationValue = liquidationValue + Number(Utils.returnMoneyValue(gtThanhLy));
					}
				}
				// cong don theo gia tri thu hoi
				var valEquipTH = $("#gridEquipmentLiquidation").datagrid('getEditor', {index:i,field:'evictionValue'});
				if (valEquipTH !=null) {
					var gtThuHoi = $(valEquipTH.target).val();
					if (gtThuHoi != undefined && gtThuHoi != null && gtThuHoi != '') {
						evictionValue = evictionValue + Number(Utils.returnMoneyValue(gtThuHoi));
					}
				} else {
					var gtThuHoi = r.evictionValue;
					if (gtThuHoi != undefined && gtThuHoi != null && gtThuHoi != '') {
						evictionValue = evictionValue + Number(Utils.returnMoneyValue(gtThuHoi));
					}
				}
			}
			if ($("#gridEquipmentLiquidation").datagrid("getData").footer != undefined && $("#gridEquipmentLiquidation").datagrid("getData").footer != null) {
				var ft = $("#gridEquipmentLiquidation").datagrid("getData").footer[0];
				ft.price = price;
				ft.liquidationValue = liquidationValue;
				ft.evictionValue = evictionValue;
				$("#gridEquipmentLiquidation").datagrid("reloadFooter");
			} else {
				// truong hop qua chinh sua, view danh sach tong cua grid lai
				var footer = [{isFooter: true, price: price, liquidationValue: liquidationValue, evictionValue: evictionValue}];
				$("#gridEquipmentLiquidation").datagrid("getData").footer = footer;
				var data = $('#gridEquipmentLiquidation').datagrid('getData');
				//data.footer = footer;
				$('#gridEquipmentLiquidation').datagrid('loadData', data);
				EquipmentManagerLiquidation.editInputInGrid();
			}
		} else {
			// truong hop xoa khong con dong nao
			if ($("#gridEquipmentLiquidation").datagrid("getData").footer != undefined && $("#gridEquipmentLiquidation").datagrid("getData").footer != null) {
				var ft = $("#gridEquipmentLiquidation").datagrid("getData").footer[0];
				ft.price = price;
				ft.liquidationValue = liquidationValue;
				ft.evictionValue = evictionValue;
				$("#gridEquipmentLiquidation").datagrid("reloadFooter");
			} else {
				// truong hop qua chinh sua, view danh sach tong cua grid lai
				var footer = [{isFooter: true, price: price, liquidationValue: liquidationValue, evictionValue: evictionValue}];
				$("#gridEquipmentLiquidation").datagrid("getData").footer = footer;
				var data = $('#gridEquipmentLiquidation').datagrid('getData');
				//data.footer = footer;
				$('#gridEquipmentLiquidation').datagrid('loadData', data);
				EquipmentManagerLiquidation.editInputInGrid();
			}
		}
	},

	/**
	 * Load lai footer cua gridEquipmentLiquidation
	 * @author vuongmq
	 @date: 07/07/2015
	 */
	checkGiaTriThanhLyThuHoi: function(type) {
		var rs = $("#gridEquipmentLiquidation").datagrid("getRows");
		if (rs != null && rs.length > 0) {
			if (type == EquipmentManagerLiquidation._ENABLE) {
				for (var i = 0, sz = rs.length; i < sz; i++) {
					// enable gia tri thanh ly
					var valEquipTL = $("#gridEquipmentLiquidation").datagrid('getEditor', {index:i,field:'liquidationValue'});
					if (valEquipTL != undefined && valEquipTL !=null) {
						$(valEquipTL.target).attr('disabled',false);
						// $(valEquipTL.target).val(''); // nhung cai nao disable = true thi moi gan bang ''
					}
					// enable gia tri thu hoi
					var valEquipTH = $("#gridEquipmentLiquidation").datagrid('getEditor', {index:i,field:'evictionValue'});
					if (valEquipTH != undefined && valEquipTH !=null) {
						$(valEquipTH.target).attr('disabled',false);
						//$(valEquipTH.target).val(''); // nhung cai nao disable = true thi moi gan bang ''
					}
				}
			} else if (type == EquipmentManagerLiquidation._DISABLED) {
				for (var i = 0, sz = rs.length; i < sz; i++) {
					// disabled gia tri thanh ly
					var valEquipTL = $("#gridEquipmentLiquidation").datagrid('getEditor', {index:i,field:'liquidationValue'});
					if (valEquipTL != undefined && valEquipTL !=null) {
						$(valEquipTL.target).attr('disabled',true);
						$(valEquipTL.target).val('');
					}
					// disabled gia tri thu hoi
					var valEquipTH = $("#gridEquipmentLiquidation").datagrid('getEditor', {index:i,field:'evictionValue'});
					if (valEquipTH != undefined && valEquipTH !=null) {
						$(valEquipTH.target).attr('disabled',true);
						$(valEquipTH.target).val('');
					}
				}
			}	
		}
	},

	/**
	 * Xoa thiet bi trong bien ban ban giao
	 * @author nhutnn
	 * @since 12/12/2014
	 */
	deleteEquipment: function(index){		
		if(EquipmentManagerLiquidation._numberEquipInRecord != null && index < EquipmentManagerLiquidation._numberEquipInRecord){
			if(EquipmentManagerLiquidation._lstEquipInRecord == null){
				EquipmentManagerLiquidation._lstEquipInRecord = new Array();
			}
			var row = $('#gridEquipmentLiquidation').datagrid('getRows')[index];
			EquipmentManagerLiquidation._lstEquipInRecord.push(row.equipmentCode);
			EquipmentManagerLiquidation._numberEquipInRecord--;
		}else if(EquipmentManagerLiquidation._lstEquipInsert != null && index >= EquipmentManagerLiquidation._numberEquipInRecord){
			var row = $('#gridEquipmentLiquidation').datagrid('getRows')[index];
			var ix = EquipmentManagerLiquidation._lstEquipInsert.indexOf(row.equipmentCode);
			if (ix > -1) {
				EquipmentManagerLiquidation._lstEquipInsert.splice(ix, 1);
			}
		}
		$('#gridEquipmentLiquidation').datagrid("deleteRow", index);	
		// 
		var rowEquip = $('#gridEquipmentLiquidation').datagrid('getRows');
		var nSize = rowEquip.length;
		var equipCode = $('#equipmentCodeInGrid').val();
		if($('#equipmentCodeInGrid').val() != undefined){
			nSize = rowEquip.length-1;
			rowEquip[rowEquip.length-1].equipmentCode = null;
			$('#equipmentCodeInGrid').remove();
		}
		/*for(var i=index; i<nSize; i++){
			var row = rowEquip[i];
			var valEquip = $("#gridEquipmentLiquidation").datagrid('getEditor', {index:i,field:'valueEquip'});
			row.valEquip = $(valEquip.target).val();
			$('#gridEquipmentLiquidation').datagrid('updateRow',{
				index: i,	
				row: row
			});
			$('#gridEquipmentLiquidation').datagrid('beginEdit', i);
		}*/

		if( EquipmentManagerLiquidation._editIndex!= null && index < EquipmentManagerLiquidation._editIndex){
			EquipmentManagerLiquidation._editIndex--;
		}else{
			EquipmentManagerLiquidation._editIndex=null;
		}	
		if(equipCode != undefined){
			/*var valEquip = $("#gridEquipmentLiquidation").datagrid('getEditor', {index:rowEquip.length-1,field:'valueEquip'});
			rowEquip[rowEquip.length-1].valueEquip = $(valEquip.target).val();

			$('#gridEquipmentLiquidation').datagrid('updateRow',{
				index: rowEquip.length-1,	// index start with 0
				row: rowEquip[rowEquip.length-1]
			});*/
			if($('#equipmentCodeInGrid').val() != undefined){
				$('#equipmentCodeInGrid').val(equipCode);
			}
			$('#gridEquipmentLiquidation').datagrid('beginEdit', rowEquip.length-1);
			$('#equipmentCodeInGrid').width($('#equipmentCodeInGrid').parent().width());
			EquipmentManagerLiquidation.editInputInGrid();
		}
		//update lai index cho Onclick Xoa
		EquipmentManagerLiquidation.loadIndexDelete();
		//update lai dong footer: dong tong cua cac gia tri
		EquipmentManagerLiquidation.refreshFooter();
	},

	/**
	 * load lai su kien click index cho nut delete
	 * @author vuongmq
	 * @since 07/07/2015
	 */
	loadIndexDelete: function() {
		var i = 0;
		$('.deleteClass').each(function(){
		   $(this).attr('onclick', 'EquipmentManagerLiquidation.deleteEquipment('+i+')');
		   i++;
		})
	},

	/**
	 * Them mot dong thiet bi
	 * @author nhutnn
	 * @since 06/01/2015
	 */
	insertEquipmentInGrid: function(){	
		$('.SuccessMsgStyle').html('').hide();
		$('.ErrorMsgStyle').html('').hide();
		var sz = $("#gridEquipmentLiquidation").datagrid("getRows").length;
		if(EquipmentManagerLiquidation._editIndex != null && EquipmentManagerLiquidation._editIndex == sz-1){
			var row = $('#gridEquipmentLiquidation').datagrid('getRows')[EquipmentManagerLiquidation._editIndex];
			
			if ($('#equipmentCodeInGrid').val() == '') {
				$('#errMsgInfo').html('Bạn chưa xử lý xong dòng thiết bị thêm mới!').show();
				return false;
			} else if($('#equipmentCodeInGrid').val() != undefined) {
				var rowEq = $('#gridEquipmentLiquidation').datagrid('getRows');
				for(var i=0; i < rowEq.length-1; i++){
					if(rowEq[i].equipmentCode == $('#equipmentCodeInGrid').val().trim()){
						$('#errMsgInfo').html("Mã thiết bị "+rowEq[i].equipmentCode+" đã tồn tại!").show();
						return false;
					}
				}
				if (row.equipmentCode == undefined || row.equipmentCode == null) {
					$('#errMsgInfo').html("Sau khi nhập thiết bị vui lòng enter textbox để load thông tin thiết bị nhập vào! ").show();
					return false;
				}
				row.equipmentCode = $('#equipmentCodeInGrid').val();
			}
			/*var valEquip = $("#gridEquipmentLiquidation").datagrid('getEditor', {index:EquipmentManagerLiquidation._editIndex,field:'valueEquip'});
			row.valueEquip = $(valEquip.target).val();*/

//			$('#equipmentCodeInGrid').remove();			
//			$('#gridEquipmentLiquidation').datagrid('updateRow',{
//				index: EquipmentManagerLiquidation._editIndex,	
//				row: row
//			});
			$('#gridEquipmentLiquidation').datagrid('beginEdit', EquipmentManagerLiquidation._editIndex);
			/*var valEquip = $("#gridEquipmentLiquidation").datagrid('getEditor', {index:EquipmentManagerLiquidation._editIndex,field:'valueEquip'});
			$(valEquip.target).bind('keydown', function(e){
				if(e.keyCode==110){
					e.preventDefault();
				}
			});*/
			if(EquipmentManagerLiquidation._lstEquipInsert == null){
				EquipmentManagerLiquidation._lstEquipInsert = new Array();
			}
			if (row != undefined && row != null) {
				EquipmentManagerLiquidation._lstEquipInsert.push(row.equipmentCode);
			}
		}		
		//EquipmentManagerLiquidation._editIndex = sz -1; // sz - 1: la tru dong tong ra	
		/*EquipmentManagerLiquidation._editIndex = sz -1;
		var dataGridLiquidation = $("#gridEquipmentLiquidation").datagrid("getRows");
		if (dataGridLiquidation[EquipmentManagerLiquidation._editIndex].equipmentCode != undefined && dataGridLiquidation[EquipmentManagerLiquidation._editIndex].equipmentCode != null) {
			// truong hop them dong TB co du lieu thanh cong
			EquipmentManagerLiquidation._editIndex = sz;
			$('#gridEquipmentLiquidation').datagrid("appendRow", {});
		} else {
			EquipmentManagerLiquidation._editIndex = sz -1;
		}*/
		EquipmentManagerLiquidation._editIndex = sz;
		$('#gridEquipmentLiquidation').datagrid("appendRow", {});	
		$('#gridEquipmentLiquidation').datagrid('beginEdit', EquipmentManagerLiquidation._editIndex);

		// F9 tren grid
		EquipmentManagerLiquidation.editInputInGrid();
		$("#gridEquipmentLiquidation").datagrid("selectRow", EquipmentManagerLiquidation._editIndex);	
		$('#equipmentCodeInGrid').focus();
		$('#equipmentCodeInGrid').width($('#equipmentCodeInGrid').parent().width());
		EquipmentManagerLiquidation.refreshFooter();
	},

	addFooterFrist: function() {
		var footer = [{isFooter: true, price:0, liquidationValue:0, evictionValue: 0}];
		$("#gridEquipmentLiquidation").datagrid("getData").footer = footer;
		var data = $('#gridEquipmentLiquidation').datagrid('getData');
		$('#gridEquipmentLiquidation').datagrid('loadData', data);
		EquipmentManagerLiquidation.editInputInGrid();
	},
	/**
	 * Show popup tim kiem thiet bi
	 * @author nhutnn
	 * @since 12/12/2014
	 * 
	 * @author hunglm16
	 * @sine JUNE 13,2015
	 * @description Ra soat noi dung phan quyen CMS
	 */
	
	 /***
		vuongmq; 09/07/2015; khong su dung nua; dung showPopupSearchEquipment
	 */
	/*showPopupSearchEquipmentEx: function(isSearch) {
		$('.ErrorMsgStyle').html('').hide();
		$('.SuccessMsgStyle').html('').hide();
		$('#easyuiPopupSearchEquipment').show();
		$('#easyuiPopupSearchEquipment').dialog({  
	        closed: false,  
	        cache: false,  
	        modal: true,
	        width : 1000,
	        height : 'auto',
	        onOpen: function(){
	        	//setDateTimePicker('yearManufacturing');
	        	var year = new Date();
	        	year = year.getFullYear(); 
	        	var op = '<option value="" selected="selected">Chọn</option><option value="'+year+'">'+year+'</option>';
	        	for(var i=1; i<100; i++){
	        		op+='<option value="'+(year-i)+'">'+(year-i)+'</option>';
	        	}
	        	$('#yearManufacturing').html(op).change();
	        	$('#equipmentCode, #seriNumber, #typeEquipment').bind('keyup',function(event){
	        		if(event.keyCode == keyCodes.ENTER){
	        			$('#btnSeachEquipmentDlg').click(); 
	        		}
	        	});
	        	$('#groupEquipment, #providerCode, #yearManufacturing').bind('keyup',function(event){
	        		if(event.keyCode == keyCodes.ENTER){
	        			$('#btnSeachEquipmentDlg').click(); 
	        		}
	        	});
	        	EquipmentManagerLiquidation.getEquipCatalog();
	        	EquipmentManagerLiquidation.getEquipGroup(null);
	        	var params = new Object();
				params = {
						equipCode: $('#equipmentCode').val().trim(),
						seriNumber: $('#seriNumber').val().trim(),
						categoryId: $('#typeEquipment').val().trim(),
						groupId: $('#groupEquipment').val().trim(),
						providerId: $('#providerId').val().trim(),
						yearManufacture: $('#yearManufacturing').val().trim(),
						stockCode: $('#stockCode').val().trim(),
						status: activeType.RUNNING,
						//stockType: 1, //Don vi
						//tradeStatus: 0,
						//usageStatus: usageStatus.SHOWING_WAREHOUSE,//Dang o kho
						flagPagination: true
					};
				if($("#idRecordHidden").val() != "" && EquipmentManagerLiquidation._lstEquipInRecord != null){
					params.lstEquipDelete = "";
					for(var i=0; i<EquipmentManagerLiquidation._lstEquipInRecord.length; i++){
						params.lstEquipDelete += EquipmentManagerLiquidation._lstEquipInRecord[i]+',';
					}					
				}
				if(EquipmentManagerLiquidation._lstEquipInsert != null){
					params.lstEquipAdd = "";
					for(var i=0; i<EquipmentManagerLiquidation._lstEquipInsert.length; i++){
						params.lstEquipAdd += EquipmentManagerLiquidation._lstEquipInsert[i]+',';
					}					
				}
				$('#equipmentGridDialog').datagrid({
//					url : '/equipment-manage-liquidation/search-equipment-liquidation',
					url : '/commons/search-equip-by-shop-root-in-stock',
					autoRowHeight : true,
					rownumbers : true, 
					//fitColumns:true,
					pagination:true,
					scrollbarSize:0,
					pageSize:10,
					pageList: [10],
					width: $('#equipmentGridDialogContainer').width(),
					height: 'auto',
					autoWidth: true,	
					queryParams: params,
					columns:[[
						{field: 'select',title:'',width: 50,sortable:false,resizable:false, align: 'center', formatter: function(value, row, index) {
							var html="";
							if(isSearch){
								html = '<a href="javascript:void(0);" onclick="EquipmentManagerLiquidation.selectEquipmentSearch('+index+');">Chọn</a>';
							}else{
								html = '<a href="javascript:void(0);" onclick="EquipmentManagerLiquidation.selectEquipment('+index+');">Chọn</a>';
							}
					    	return html;
						}},
					    {field: 'equipmentCode', title:'Mã thiết bị', width: 200, sortable:false, resizable:false, align: 'left'},	
					    {field: 'seriNumber', title:'Số serial', width: 100, sortable:false, resizable:false, align: 'left'},	
					    {field: 'healthStatus', title:'Tình trạng thiết bị', width: 100, sortable:false, resizable:false, align: 'left'},	
					    {field: 'stock',title:'Kho', width: 120, sortable:false, resizable:false, align: 'left', formatter: function(value, row, index) {
					    	var html = "";
							if(row.stockCode != null && row.stockName!= null){
								html = row.stockCode+' - '+row.stockName; 
							}
							return html;
						}},	
					    {field: 'typeEquipment', title:'Loại thiết bị', width: 80, sortable:false, resizable:false, align: 'left'},	
					    {field: 'groupEquipment', title:'Nhóm thiết bị', width: 200, sortable:false, resizable:false, align: 'left', formatter: function(value, row, index) {
					    	var html="";
							if(row.groupEquipmentCode != null && row.groupEquipmentName!= null){
								html = row.groupEquipmentCode+' - '+row.groupEquipmentName; 
							}
							return html;
						}},	
					    {field: 'capacity',title:'Dung tích (lít)', width: 80, sortable:false, resizable:false, align: 'center'},	
					    {field: 'equipmentBrand',title:'Hiệu', width: 80, sortable:false, resizable:false, align: 'left'},	
					    {field: 'equipmentProvider',title:'NCC',width: 80, sortable:false, resizable:false, align: 'left'},	
					    {field: 'yearManufacture', title:'Năm sản xuất', width: 80, sortable:false, resizable:false, align: 'right'},												
					]],
					onBeforeLoad:function(param){											 
					},
					onLoadSuccess :function(data){   			 
						if (data == null || data.total == 0) {
							//$("#errMsgInfo").html("Không tìm thấy danh sách biên bản giao nhận thiết bị!").show();
						}else{
							$('.datagrid-header-rownumber').html('STT');		    	
						}	    
						$('#equipmentCode').focus();			
					}
				});
	        },
			onClose: function(){
				$('#easyuiPopupSearchEquipment').hide();
				$('#equipmentCode').val('').change();
				$('#seriNumber').val('').change();
				$('#typeEquipment').html('<option value="" selected="selected">Tất cả</option>');
				$('#groupEquipment').html('<option value="" selected="selected">Tất cả</option>').change();
				$('#providerId').html('<option value="" selected="selected">Tất cả</option>');
				$('#yearManufacturing').val('').change();
				$('#equipmentGridDialog').datagrid('loadData',[]);
			}
		});
	},*/

	/**
	 * Chon thiet bi de tim kiem
	 * @author nhutnn
	 * @since 12/12/2014
	 */
	selectEquipmentSearch: function(index){
		var rowPopup = $('#equipmentGridDialog').datagrid('getRows')[index];
		$('#equipCode').val(rowPopup.equipmentCode);
		
		$('#easyuiPopupSearchEquipment').dialog('close');
	},
	/**
	 * Lay danh muc loai, ncc thiet bi
	 * @author nhutnn
	 * @since 20/12/2014
	 */
	getEquipCatalog: function(){
		$('#typeEquipment').html('<option value="" selected="selected">Tất cả</option>');
    	$('#provider').html('<option value=""  selected="selected">Tất cả</option>');
    	var params = new Object();
    	Utils.getJSONDataByAjaxNotOverlay(params, '/equipment-manage-liquidation/get-equipment-catalog', function(result){	
		// $.getJSON('/equipment-manage-liquidation/get-equipment-catalog', function(result) {				
			if(result.equipsCategory != undefined && result.equipsCategory != null && result.equipsCategory.length > 0) {					
				for(var i=0; i < result.equipsCategory.length; i++){
					$('#typeEquipment').append('<option value="'+ result.equipsCategory[i].id +'">'+ Utils.XSSEncode(result.equipsCategory[i].code) +' - '+ Utils.XSSEncode(result.equipsCategory[i].name) +'</option>');  
				}
			} 
			$('#typeEquipment').change();
			// if(result.equipProviders != undefined && result.equipProviders != null && result.equipProviders.length > 0) {					
			// 	for(var i=0; i < result.equipProviders.length; i++){
			// 		$('#providerId').append('<option value="'+ result.equipProviders[i].id +'">'+ Utils.XSSEncode(result.equipProviders[i].name) +'</option>');  
			// 	}
			// } 
			// $('#providerId').change();	

			//tamvnm: doi thanh autoCompleteCombobox
			if(result.equipProviders != undefined && result.equipProviders != null && result.equipProviders.length > 0) {					
				for (var i = 0, isize = result.equipProviders.length; i < isize; i++) {
					result.equipProviders[i].searchText = unicodeToEnglish(result.equipProviders[i].code + " " + result.equipProviders[i].name);
 					result.equipProviders[i].searchText = result.equipProviders[i].searchText.toUpperCase();
				}
				var obj = {
					id: -1,
					code: "Tất cả",
					name: "Tất cả",
					codeName: "Tất cả",
					searchText: unicodeToEnglish("Tất cả").toUpperCase()
				};
				result.equipProviders.splice(0, 0, obj);
				$('#providerId').combobox("loadData", result.equipProviders);
		 		$('#providerId').combobox("setValue", -1);
			}			
		},null,null);
	},
	/**
	 * Lay nhom thiet bi
	 * @author nhutnn
	 * @since 12/01/2015
	 */
	getEquipGroup: function(typeEquip){
		var params = new Object();
		if(typeEquip != undefined && typeEquip != null && typeEquip != ''){
			params.id = typeEquip;
		}

		// $('#groupEquipment').html('<option value="" selected="selected">Tất cả</option>');
    	Utils.getJSONDataByAjaxNotOverlay(params, '/equipment-manage-liquidation/get-equipment-group', function(result){	
		// $.getJSON('/equipment-manage-liquidation/get-equipment-catalog', function(result) {				
			// if(result.equipGroups != undefined && result.equipGroups != null && result.equipGroups.length > 0) {					
			// 	for(var i=0; i < result.equipGroups.length; i++){
			// 		$('#groupEquipment').append('<option value="'+ result.equipGroups[i].id +'">'+ Utils.XSSEncode(result.equipGroups[i].code+' - '+result.equipGroups[i].name) +'</option>');  
			// 	}
			// } 
			// $('#groupEquipment').change();	
			//tamvnm: doi thanh autoCompleteCombobox
			if(result.equipGroups != undefined && result.equipGroups != null && result.equipGroups.length > 0) {	
				for (var i = 0, isize = result.equipGroups.length; i < isize; i++) {
					result.equipGroups[i].searchText = unicodeToEnglish(result.equipGroups[i].code + " " + result.equipGroups[i].name);
	 				result.equipGroups[i].searchText = result.equipGroups[i].searchText.toUpperCase();
				}				
				var obj = {
				id: -1,
				code: "Tất cả",
				name: "Tất cả",
				codeName: "Tất cả",
				searchText: unicodeToEnglish("Tất cả").toUpperCase()
				};
				result.equipGroups.splice(0, 0, obj);
				$('#groupEquipment').combobox("loadData", result.equipGroups);
		 		$('#groupEquipment').combobox("setValue", -1);
			} 			
		},null,null);
	},
	/**
	 * Xu ly trên input loai thiet bi
	 * @author nhutnn
	 * @since 19/12/2014
	 */
	onChangeTypeEquip: function(cb){
		var typeEquip = $(cb).val().trim();
		EquipmentManagerLiquidation.getEquipGroup(typeEquip);
		// $('#groupEquipment').html('<option value="" selected="selected">Tất cả</option>');
		//if(typeEquip != undefined && typeEquip != null && typeEquip != ''){
			// $.getJSON('/equipment-manage-liquidation/get-equipment-group?id='+typeEquip, function(result) {				
			// 	if(result.equipGroups != undefined && result.equipGroups != null && result.equipGroups.length > 0) {					
			// 		for(var i=0; i < result.equipGroups.length; i++){
			// 			$('#groupEquipment').append('<option value="'+ result.equipGroups[i].id +'">'+ Utils.XSSEncode(result.equipGroups[i].name) +'</option>');  
			// 		}
			// 	} 
			// 	$('#groupEquipment').change();				
			// });			
		//}
	},
	/**
	 * Chon thiet bi
	 * @author nhutnn
	 * @since 12/12/2014
	 */
	/*selectEquipment: function(index){
		var rowPopup = $('#equipmentGridDialog').datagrid('getRows')[index];
		if(EquipmentManagerLiquidation._editIndex!=null){			
			var valEquip = $("#gridEquipmentLiquidation").datagrid('getEditor', {index:EquipmentManagerLiquidation._editIndex,field:'valueEquip'});
			rowPopup.valueEquip = $(valEquip.target).val();
			$('#gridEquipmentLiquidation').datagrid('updateRow',{
				index: EquipmentManagerLiquidation._editIndex,	// index start with 0
				row: rowPopup
			});
			$('#gridEquipmentLiquidation').datagrid('beginEdit', EquipmentManagerLiquidation._editIndex);
			$('#equipmentCodeInGrid').width($('#equipmentCodeInGrid').parent().width());
			EquipmentManagerLiquidation.editInputInGrid();		
		}
		
		$('#easyuiPopupSearchEquipment').dialog('close');
	},*/
	/**
	 * Tim kiem thiet bi
	 * @author nhutnn
	 * @since 12/12/2014
	 * 
	 * @author hunglm16
	 * @sine JUNE 13,2015
	 * @description Ra soat noi dung phan quyen CMS
	 */
	searchEquipment: function(isSearch) {
//		var params=new Object(); 
//		params.equipCode = $('#equipmentCode').val().trim();
//		params.seriNumber = $('#seriNumber').val().trim();
//		params.categoryId = $('#typeEquipment').val().trim();
//		params.groupId = $('#groupEquipment').val().trim();
//		params.providerId = $('#providerId').val().trim();
//		params.yearManufacture = $('#yearManufacturing').val().trim();
//		params.stockCode = $('#stockCode').val().trim();
//		if($("#idRecordHidden").val() != "" && EquipmentManagerLiquidation._lstEquipInRecord != null){
//			params.lstEquipDelete = "";
//			for(var i=0; i<EquipmentManagerLiquidation._lstEquipInRecord.length; i++){
//				params.lstEquipDelete += EquipmentManagerLiquidation._lstEquipInRecord[i]+',';
//			}					
//		}
//		if(EquipmentManagerLiquidation._lstEquipInsert != null){
//			params.lstEquipAdd = "";
//			for(var i=0; i<EquipmentManagerLiquidation._lstEquipInsert.length; i++){
//				params.lstEquipAdd += EquipmentManagerLiquidation._lstEquipInsert[i]+',';
//			}					
//		}
		//tamvnm: thay doi thanh autoCompleteCombobox: -1 la truong hop tim group ko co trong BD
		var groupId = "";
		if ($('#groupEquipment').combobox('getValue') == "") {
			$('#groupEquipment').combobox('setValue', -1);
			groupId = "";
		} else if ($('#groupEquipment').combobox('getValue') == -1) {
			groupId = "";
		} else {
			if (Utils.getMessageOfInvaildNumberNew($('#groupEquipment').combobox("getValue")).length == 0) {
				groupId = $('#groupEquipment').combobox("getValue");
			} else {
				groupId = -1;
			}
		}
		var providerId = "";
		if ($('#providerId').combobox('getValue') == "") {
			$('#providerId').combobox('setValue', -1);
			providerId = "";
		} else if ($('#providerId').combobox('getValue') == -1) {
			providerId = "";
		} else {
			if (Utils.getMessageOfInvaildNumberNew($('#providerId').combobox("getValue")).length == 0) {
				providerId = $('#providerId').combobox("getValue");
			} else {
				providerId = -1;
			}
		}
		var params = {
				equipCode: $('#equipmentCode').val().trim(),
				seriNumber: $('#seriNumber').val().trim(),
				categoryId: $('#typeEquipment').val().trim(),
				// groupId: $('#groupEquipment').val().trim(),
				// providerId: $('#providerId').val().trim(),
				groupId: groupId,
				providerId: providerId,
				yearManufacture: $('#yearManufacturing').val().trim(),
				stockCode: $('#stockCode').val().trim(),
				status: activeType.RUNNING,
				//tradeStatus: 0,
				//stockType: 1, //Don vi;
				//usageStatus: usageStatus.SHOWING_WAREHOUSE,//Dang o kho
				flagPagination: true,
				shopCode: $('#shopCode').val().trim(),
				shortCode: $('#shortCode').val().trim(),
				isSearch: isSearch,
		};
		$('#equipmentGridDialog').datagrid('load', params);
	},
	/**
	 * kiem tra ki
	 * @author nhutnn
	 * @since 19/12/2014
	 */
	changeForm: function(id){
//		Utils.getJSONDataByAjaxNotOverlay({id:id},'/equipment-manage-liquidation/check-equip-period', function(dt){						
//			if(dt.error){
//				$('#errMsgInfo').html(dt.errMsg).show();
//				return false;
//			}else{
//				if(id > 0){
//					window.location.href="/equipment-manage-liquidation/change-record?id="+id;
//				}else{
//					window.location.href="/equipment-manage-liquidation/change-record";
//				}
//			}
//		}, null, null);	
		if(id > 0){
			window.location.href="/equipment-manage-liquidation/change-record?id="+id;
		}else{
			window.location.href="/equipment-manage-liquidation/change-record";
		}
	},

	/**
	 * Tao moi bien ban thanh ly
	 * @author nhutnn
	 * @since 07/01/2015
	 */

	 /**
	 * Tao moi bien ban thanh ly
	 * @author vuongmq
	 * @since 08/07/2015
	 */
	createLiquidation: function(){
		$('.ErrorMsgStyle').html('').hide();
		$('.SuccessMsgStyle').html('').hide();
		var msg ='';
		var err = '';
		if (msg.length == 0) {
			msg = Utils.getMessageOfRequireCheck('docNumber','Số công văn/Tờ trình thanh lý');
			err = '#docNumber';
		}
		if (msg.length == 0) {
			msg = Utils.getMessageOfSpecialCharactersValidate('docNumber','Số công văn/Tờ trình thanh lý',Utils._NAME_CUSTYPE);
			err = '#docNumber';
		}

		//tamvnm: them ngay lap
		var createDate = $('#createDate').val();
		if(msg.length == 0 && $('#createDate').val().trim().length > 0 && !Utils.isDate($('#createDate').val()) && $('#createDate').val() != '__/__/____' ){
			msg = 'Ngày biên bản không tồn tại hoặc không đúng định dạng dd/mm/yyyy.';
			err = '#createDate';
		}
		var curDate = ReportUtils.getCurrentDateString(); //Lay ngay hien tai sysDate
		if (msg.length == 0 && !Utils.compareDate($('#createDate').val().trim(), curDate)) {
			msg = 'Ngày biên bản không được sau ngày hiện tại. Vui lòng nhập lại';
			$('#createDate').focus();
		}
		
		// kiem tra tong gia tri thanh ly, thu hoi
		if ($('#checkLot').is(':checked')) {
			if ($('#tongThanhLy').val() != null && $('#tongThanhLy').val().trim().length > 0) {
				// kiem tra gia tri thanh ly			
				if (msg.length == 0) {
					msg = Utils.getMessageOfSpecialCharactersValidateEx('tongThanhLy', 'Tổng giá trị thanh lý', Utils._TF_NUMBER_COMMA);
					err = '#tongThanhLy';
				}
				if (msg.length == 0 && !isNaN($('#tongThanhLy').val().trim()) && Number($('#tongThanhLy').val()) < 0) {
					msg = 'Tổng giá trị thanh lý phải lớn hơn 0';
					err = '#tongThanhLy';
				}
				if (!EquipmentManagerLiquidation.validateTongGiaTriThanhLyThuHoi($('#tongThanhLy').val())) {
					msg = 'Tổng giá trị thanh lý phải nhỏ hơn hoặc bằng tổng nguyên giá';
					err = '#tongThanhLy';
				}
			}
			if ($('#tongThuHoi').val() != null && $('#tongThuHoi').val().trim().length > 0) {
				// kiem tra gia tri thu hoi
				if (msg.length == 0) {
					msg = Utils.getMessageOfSpecialCharactersValidateEx('tongThuHoi', 'Tổng giá trị thu hồi', Utils._TF_NUMBER_COMMA);
					err = '#tongThuHoi';
				}
				if (msg.length == 0 && !isNaN($('#tongThuHoi').val().trim()) && Number($('#tongThuHoi').val()) < 0) {
					msg = 'Tổng giá trị thu hồi phải lớn hơn 0';
					err = '#tongThuHoi';
				}
				if (!EquipmentManagerLiquidation.validateTongGiaTriThanhLyThuHoi($('#tongThuHoi').val())) {
					msg = 'Tổng giá trị thu hồi phải nhỏ hơn hoặc bằng tổng nguyên giá';
					err = '#tongThuHoi';
				}
			}
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfSpecialCharactersValidate('buyerName','Tên người mua', Utils._NAME);
			err = '#buyerName';
		}
		if (msg.length == 0) {
			msg = Utils.getMessageOfSpecialCharactersValidateEx('buyerPhone','Điện thoại', Utils._TF_PHONE_NUMBER);
			err = '#buyerPhone';
		}
		if (msg.length == 0) {
			msg = Utils.getMessageOfSpecialCharactersValidate('buyerAddress','Địa chỉ', Utils._ADDRESS);
			err = '#buyerAddress';
		}	
		if (msg.length == 0) {
			msg = Utils.getMessageOfSpecialCharactersValidate('reason','Lý do', Utils._SPECIAL);
			err = '#reason';
		}
		if (msg.length == 0) {
			msg = Utils.getMessageOfSpecialCharactersValidate('note','Ghi chú', Utils._SPECIAL);
			err = '#note';
		}
		if (msg.length == 0 && $('#note').attr('maxlength') > 500) {
			msg = 'Bạn nhập quá giới hạn của trường Ghi chú';
			err = '#note';
		}

		if (msg.length > 0) {
			$('#errMsgInfo').html(msg).show();
			$(err).focus();
			return false;
		}
		
		var rowEquip = $('#gridEquipmentLiquidation').datagrid('getRows');
		if (rowEquip.length == 0) {
			$('#errMsgInfo').html('Không có thiết bị để cập nhật').show();
			return false;
		}

		/*var lstEquipCode = new Array();
		var lstValueEquip = new Array();
		var nSize = rowEquip.length;
		if ($('#equipmentCodeInGrid').val() != undefined) {
			nSize = rowEquip.length-1;
		}
		for (var i = 0; i < nSize; i++) {
			lstEquipCode.push(rowEquip[i].equipmentCode);
			var valEquip = $("#gridEquipmentLiquidation").datagrid('getEditor', {index:i,field:'valueEquip'});
			if($(valEquip.target).val() == null || $(valEquip.target).val() == "" || $(valEquip.target).val() < 1){
				$('#errMsgInfo').html('Mã thiết bị '+rowEquip[i].equipmentCode+' chưa có giá trị hợp lệ!').show();				
				return;
			}else if(rowEquip[i].price != null && $(valEquip.target).val() != "" && $(valEquip.target).val() > rowEquip[i].price){
				$('#errMsgInfo').html('Mã thiết bị '+rowEquip[i].equipmentCode+' có giá trị phải nhỏ hơn nguyên giá!').show();				
				return;
			}
			lstValueEquip.push(Utils.returnMoneyValue($(valEquip.target).val()));			
		}*/
		
		if ($('#equipmentCodeInGrid').val() != undefined && $('#equipmentCodeInGrid').val() != '') {
			var rowEq = $('#gridEquipmentLiquidation').datagrid('getRows');
			for (var i = 0; i < rowEq.length-1; i++) {
				if(rowEq[i].equipmentCode == $('#equipmentCodeInGrid').val().trim()){
					$('#errMsgInfo').html("Mã thiết bị "+rowEq[i].equipmentCode+" đã tồn tại!").show();
					return false;
				}
			}
			//lstEquipCode.push($('#equipmentCodeInGrid').val().trim());
		} else if ($('#equipmentCodeInGrid').val() == '') {
			$('#errMsgInfo').html('Bạn chưa xử lý xong dòng thiết bị hiện tại!').show();
			return false;
		}
		if ($('#equipmentCodeInGrid').val() != undefined) {
			/*var valEquip = $("#gridEquipmentLiquidation").datagrid('getEditor', {index:rowEquip.length-1,field:'valueEquip'});
			if($(valEquip.target).val() == "" || $(valEquip.target).val() < 1){
				$('#errMsgInfo').html('Mã thiết bị '+$('#equipmentCodeInGrid').val()+' chưa có giá trị hợp lệ!').show();				
				return;
			}else if(rowEquip[rowEquip.length-1].price != null && $(valEquip.target).val() != "" && Number(Utils.returnMoneyValue($(valEquip.target).val())) > rowEquip[rowEquip.length-1].price){
				$('#errMsgInfo').html('Mã thiết bị '+rowEquip[i].equipmentCode+' có giá trị phải nhỏ hơn nguyên giá!').show();				
				return;
			}
			lstValueEquip.push(Utils.returnMoneyValue($(valEquip.target).val()));		*/
		}
		// them cac params vô de them moi
		

		var checkLot = activeType.STOPPED;
		var liquidationValue = activeType.STOPPED;
		var evictionValue = activeType.STOPPED;

		var lstEquipCode = new Array();
		var lstLiquidationValue = new Array();
		var lstEvictionValue = new Array();
		var nSize = rowEquip.length;
		if ($('#equipmentCodeInGrid').val() != undefined) {
			nSize = rowEquip.length-1;
		}

		if ($('#checkLot').is(':checked')) {
			checkLot = activeType.RUNNING;
			liquidationValue = Utils.returnMoneyValue($('#tongThanhLy').val());
			evictionValue = Utils.returnMoneyValue($('#tongThuHoi').val());

			for (var i = 0; i < nSize; i++) {
				lstEquipCode.push(rowEquip[i].equipmentCode);	
			}
		} else {
			// khong check lo thi moi them  lst danh sach cac gia tri thanh ly va thu hoi
			for (var i = 0; i < nSize; i++) {
				lstEquipCode.push(rowEquip[i].equipmentCode);
				//add gia tri thanh ly
				var valEquipTL = $("#gridEquipmentLiquidation").datagrid('getEditor', {index:i,field:'liquidationValue'});
				if (valEquipTL != undefined && valEquipTL != null) {
					var gtThanhLyTmp = $(valEquipTL.target).val();
					var giaTriThanhLy = Utils.returnMoneyValue($(valEquipTL.target).val());
					if (gtThanhLyTmp == null || gtThanhLyTmp.trim().length == 0 || giaTriThanhLy == null || giaTriThanhLy.length == 0) {
						giaTriThanhLy = -1;
					} else if (!isNaN(giaTriThanhLy) && giaTriThanhLy < 1) {
						$('#errMsgInfo').html('Mã thiết bị '+rowEquip[i].equipmentCode+' giá trị thanh lý phải lớn hơn 0!').show();
						$(valEquipTL.target).focus();			
						return false;
					} else if (rowEquip[i].price != null && giaTriThanhLy != "" && Number(giaTriThanhLy) > Number(rowEquip[i].price)) {
						$('#errMsgInfo').html('Mã thiết bị '+rowEquip[i].equipmentCode+' có giá trị thanh lý phải nhỏ hơn hoặc bằng nguyên giá!').show();				
						$(valEquipTL.target).focus();
						return false;
					}
					lstLiquidationValue.push(giaTriThanhLy);
				}
				//add gia tri thu hoi
				var valEquipTH = $("#gridEquipmentLiquidation").datagrid('getEditor', {index:i,field:'evictionValue'});
				if (valEquipTH != undefined && valEquipTH != null) {
					var gtThuHoiTmp = $(valEquipTH.target).val();
					var giaTriThuHoi = Utils.returnMoneyValue($(valEquipTH.target).val());
					if (gtThuHoiTmp == null || gtThuHoiTmp.trim().length == 0 || giaTriThuHoi == null || giaTriThuHoi.length == 0) {
						giaTriThuHoi = -1;
					} else if (!isNaN(giaTriThuHoi) && giaTriThuHoi < 1) {
						$('#errMsgInfo').html('Mã thiết bị '+rowEquip[i].equipmentCode+' giá trị thu hồi phải lớn hơn 0!').show();				
						$(valEquipTH.target).focus();
						return false;
					} else if (rowEquip[i].price != null && giaTriThuHoi != "" && Number(giaTriThuHoi) > Number(rowEquip[i].price)) {
						$('#errMsgInfo').html('Mã thiết bị '+rowEquip[i].equipmentCode+' có giá trị thu hồi phải nhỏ hơn hoặc bằng nguyên giá!').show();				
						$(valEquipTH.target).focus();
						return false;
					}
					lstEvictionValue.push(giaTriThuHoi);
				}	
			}
		}

		var params = new Object(); 
		params.docNumber = $('#docNumber').val().trim();
		params.buyerName = $('#buyerName').val().trim();
		params.buyerAddress = $('#buyerAddress').val().trim();
		params.buyerPhone = $('#buyerPhone').val().trim();
		params.reason = $('#reason').val().trim();
		params.status = $('#status').val().trim();
		params.createDate = createDate;
		params.note = $('#note').val();
		// them ma thiet bi vao neu nhap vao textbox thiet bi de validate tren server
		if ($('#equipmentCodeInGrid').val() != undefined && $('#equipmentCodeInGrid').val() != '' && $('#equipmentCodeInGrid').val().trim() != '') {
			lstEquipCode.push($('#equipmentCodeInGrid').val().trim());
		}
		// them các gia tri check lo
		params.checkLot = checkLot;
		params.liquidationValue = liquidationValue;
		params.evictionValue = evictionValue;
		if (lstEquipCode.length > 0) {
			params.lstEquipCode = lstEquipCode;
			params.lstLiquidationValue = lstLiquidationValue;			
			params.lstEvictionValue = lstEvictionValue;
		}

		//upload file
		var arrFile = DropzonePlugin._dropzoneObject.getFilesWithStatus(Dropzone.ADDED);
		if (EquipmentManagerLiquidation._arrFileDelete == undefined || EquipmentManagerLiquidation._arrFileDelete == null) {
			EquipmentManagerLiquidation._arrFileDelete = [];
		}
		if (arrFile != null && arrFile != undefined && arrFile.length > 0) {
			if (EquipmentManagerLiquidation._countFile == 5) {
				$('#errMsgInfo').html("Tổng số tập tin đính kèm không được vượt quá 5").show();
				return false;
			}
			var maxLength = 5 - EquipmentManagerLiquidation._countFile;
			if(maxLength<0){
				maxLength = 0;
			}
			if (arrFile.length > maxLength) {
				$('#errMsgInfo').html("Số tập tin thêm mới không được vượt quá " + maxLength).show();
				return false;
			}
			msg = UploadUtil.getMsgByFileRequireCheck(UploadUtil._typeFileRequireCheck.equipRecord);
			if (msg.trim().length > 0) {
				$('#errMsgInfo').html(msg).show();
				return false;
			}			
		}
		params.equipAttachFileStr= EquipmentManagerLiquidation._arrFileDelete.join(',');
		
//		Utils.getJSONDataByAjaxNotOverlay({id : -1}, '/equipment-manage-liquidation/check-equip-period', function (dt) {						
//			if (dt.error) {
//				$('#errMsgInfo').html(dt.errMsg).show();
//				return false;
//			} else {
//				$.messager.confirm('Xác nhận', 'Bạn có muốn lưu dữ liệu?', function (r) {
//					if (r) {
//						if (arrFile != null && arrFile != undefined && arrFile.length > 0) {
//							params = JSONUtil.getSimpleObject2(params);
//							UploadUtil.updateAdditionalDataForUpload(params);
//							UploadUtil.startUpload('errMsgInfo');
//							return false;
//						}					
//						Utils.saveData(params, '/equipment-manage-liquidation/create-liquidation', null, 'errMsgInfo', function (data) {	
//							if(data.error){
//								$('#errMsgInfo').html(data.errMsg).show();
//								return false;
//							}else{
//								if (data.lstRecordNotSendMail != undefined && data.lstRecordNotSendMail != null && data.lstRecordNotSendMail != '') {
//									$('#errMsgInfo').html('Gửi Email không thành công biên bản báo mất: ' + data.lstRecordNotSendMail).show();
//								}
//								$('#successMsgInfo').html("Cập dữ liệu thành công.").show();	
//								EquipmentManagerLiquidation._lstEquipInsert = null;
//								EquipmentManagerLiquidation._editIndex = null;				
//								setTimeout(function(){
//									$('#successMsgInfo').html("").hide(); 
//									window.location.href = '/equipment-manage-liquidation/info';						              		
//								}, 1500);
//							}
//						}, null, null, null, null);		
//					}
//				});
//			}
//		}, null, null);	
		$.messager.confirm('Xác nhận', 'Bạn có muốn lưu dữ liệu?', function (r) {
			if (r) {
				if (arrFile != null && arrFile != undefined && arrFile.length > 0) {
					params = JSONUtil.getSimpleObject2(params);
					UploadUtil.updateAdditionalDataForUpload(params);
					UploadUtil.startUpload('errMsgInfo');
					return false;
				}					
				Utils.saveData(params, '/equipment-manage-liquidation/create-liquidation', null, 'errMsgInfo', function (data) {	
					if(data.error){
						$('#errMsgInfo').html(data.errMsg).show();
						return false;
					}else{
						if (data.lstRecordNotSendMail != undefined && data.lstRecordNotSendMail != null && data.lstRecordNotSendMail != '') {
							$('#errMsgInfo').html('Gửi Email không thành công biên bản báo mất: ' + data.lstRecordNotSendMail).show();
						}
						$('#successMsgInfo').html("Cập dữ liệu thành công.").show();	
						EquipmentManagerLiquidation._lstEquipInsert = null;
						EquipmentManagerLiquidation._editIndex = null;				
						setTimeout(function(){
							$('#successMsgInfo').html("").hide(); 
							window.location.href = '/equipment-manage-liquidation/info';						              		
						}, 1500);
					}
				}, null, null, null, null);		
			}
		});
	},

	/**
	 * Chinh sua bien ban thanh ly
	 * @author nhutnn
	 * @since 07/01/2015
	 */

	 /**
	 * Chinh sua bien ban thanh ly
	 * @author vuongmq
	 * @since 08/07/2015
	 */
	updateLiquidation: function() {
		$('.ErrorMsgStyle').html('').hide();
		$('.SuccessMsgStyle').html('').hide();
		var msg ='';
		var err = '';
		
		if (msg.length == 0) {
			msg = Utils.getMessageOfRequireCheck('docNumber','Số công văn/Tờ trình thanh lý');
			err = '#docNumber';
		}
		if (msg.length == 0) {
			msg = Utils.getMessageOfSpecialCharactersValidate('docNumber','Số công văn/Tờ trình thanh lý',Utils._NAME_CUSTYPE);
			err = '#docNumber';
		}

		//tamvnm: them ngay lap
		var createDate = $('#createDate').val();
		if(msg.length == 0 && $('#createDate').val().trim().length > 0 && !Utils.isDate($('#createDate').val()) && $('#createDate').val() != '__/__/____' ){
			msg = 'Ngày biên bản không tồn tại hoặc không đúng định dạng dd/mm/yyyy.';
			err = '#createDate';
		}
		var curDate = ReportUtils.getCurrentDateString(); //Lay ngay hien tai sysDate
		if (msg.length == 0 && !Utils.compareDate($('#createDate').val().trim(), curDate)) {
			msg = 'Ngày biên bản không được sau ngày hiện tại. Vui lòng nhập lại';
			$('#createDate').focus();
		}

		// kiem tra tong gia tri thanh ly, thu hoi
		if ($('#checkLot').is(':checked')) {
			if ($('#tongThanhLy').val() != null && $('#tongThanhLy').val().trim().length > 0) {
				// kiem tra gia tri thanh ly			
				if (msg.length == 0) {
					msg = Utils.getMessageOfSpecialCharactersValidateEx('tongThanhLy', 'Tổng giá trị thanh lý', Utils._TF_NUMBER_COMMA);
					err = '#tongThanhLy';
				}
				if (msg.length == 0 && !isNaN($('#tongThanhLy').val().trim()) && Number($('#tongThanhLy').val()) < 0) {
					msg = 'Tổng giá trị thanh lý phải lớn hơn 0';
					err = '#tongThanhLy';
				}
				if (!EquipmentManagerLiquidation.validateTongGiaTriThanhLyThuHoi($('#tongThanhLy').val())) {
					msg = 'Tổng giá trị thanh lý phải nhỏ hơn hoặc bằng tổng nguyên giá';
					err = '#tongThanhLy';
				}
			}
			if ($('#tongThuHoi').val() != null && $('#tongThuHoi').val().trim().length > 0) {
				// kiem tra gia tri thu hoi
				if (msg.length == 0) {
					msg = Utils.getMessageOfSpecialCharactersValidateEx('tongThuHoi', 'Tổng giá trị thu hồi', Utils._TF_NUMBER_COMMA);
					err = '#tongThuHoi';
				}
				if (msg.length == 0 && !isNaN($('#tongThuHoi').val().trim()) && Number($('#tongThuHoi').val()) < 0) {
					msg = 'Tổng giá trị thu hồi phải lớn hơn 0';
					err = '#tongThuHoi';
				}
				if (!EquipmentManagerLiquidation.validateTongGiaTriThanhLyThuHoi($('#tongThuHoi').val())) {
					msg = 'Tổng giá trị thu hồi phải nhỏ hơn hoặc bằng tổng nguyên giá';
					err = '#tongThuHoi';
				}
			}
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfSpecialCharactersValidate('buyerName','Tên người mua', Utils._NAME);
			err = '#buyerName';
		}
		if (msg.length == 0) {
			msg = Utils.getMessageOfSpecialCharactersValidateEx('buyerPhone','Điện thoại', Utils._TF_PHONE_NUMBER);
			err = '#buyerPhone';
		}
		if (msg.length == 0){
			msg = Utils.getMessageOfSpecialCharactersValidate('buyerAddress','Địa chỉ', Utils._ADDRESS);
			err = '#buyerAddress';
		}
		if (msg.length == 0) {
			msg = Utils.getMessageOfSpecialCharactersValidate('reason','Lý do', Utils._SPECIAL);
			err = '#reason';
		}
		if (msg.length == 0) {
			msg = Utils.getMessageOfSpecialCharactersValidate('note','Ghi chú', Utils._SPECIAL);
			err = '#note';
		}
		if (msg.length == 0 && $('#note').attr('maxlength') > 500) {
			msg = 'Bạn nhập quá giới hạn của trường Ghi chú';
			err = '#note';
		}

		if (msg.length > 0) {
			$('#errMsgInfo').html(msg).show();
			$(err).focus();
			return false;
		}
		
		var rowEquip = $('#gridEquipmentLiquidation').datagrid('getRows');
		if (rowEquip.length == 0) {
			$('#errMsgInfo').html('Không có thiết bị để cập nhật').show();
			return false;
		}

		/*var lstEquipCode = new Array();
		var lstValueEquip = new Array();
		var nSize = rowEquip.length;
		if($('#equipmentCodeInGrid').val() != undefined){
			nSize = rowEquip.length-1;
		}
		for(var i=0; i<nSize; i++){
			lstEquipCode.push(rowEquip[i].equipmentCode);
			var valEquip = $("#gridEquipmentLiquidation").datagrid('getEditor', {index:i,field:'valueEquip'});
			if($(valEquip.target).val() == "" || $(valEquip.target).val() < 1){
				$('#errMsgInfo').html('Mã thiết bị '+rowEquip[i].equipmentCode+' chưa có giá trị hợp lệ!').show();				
				return;
			}else if(rowEquip[i].price != null && $(valEquip.target).val() != "" && Number(Utils.returnMoneyValue($(valEquip.target).val())) > rowEquip[i].price){
				$('#errMsgInfo').html('Mã thiết bị '+rowEquip[i].equipmentCode+' có giá trị phải nhỏ hơn nguyên giá!').show();				
				return;
			}
			lstValueEquip.push(Utils.returnMoneyValue($(valEquip.target).val()));			
		}*/
		
		if ($('#equipmentCodeInGrid').val() != undefined && $('#equipmentCodeInGrid').val() != '') {
			var rowEq = $('#gridEquipmentLiquidation').datagrid('getRows');
			for (var i = 0; i < rowEq.length-1; i++) {
				if (rowEq[i].equipmentCode == $('#equipmentCodeInGrid').val().trim()) {
					$('#errMsgInfo').html("Mã thiết bị "+rowEq[i].equipmentCode+" đã tồn tại!").show();
					return false;
				}
			}
			//lstEquipCode.push($('#equipmentCodeInGrid').val());
		} else if ($('#equipmentCodeInGrid').val() == '') {
			$('#errMsgInfo').html('Bạn chưa xử lý xong dòng thiết bị hiện tại!').show();
			return false;
		}
		if ($('#equipmentCodeInGrid').val() != undefined) {
			/*var valEquip = $("#gridEquipmentLiquidation").datagrid('getEditor', {index:rowEquip.length-1,field:'valueEquip'});
			if($(valEquip.target).val() == "" || $(valEquip.target).val() < 1){
				$('#errMsgInfo').html('Mã thiết bị '+$('#equipmentCodeInGrid').val()+' chưa có giá trị hợp lệ!').show();				
				return;
			}else if(rowEquip[rowEquip.length-1].price != null && $(valEquip.target).val() != "" && $(valEquip.target).val() > rowEquip[rowEquip.length-1].price){
				$('#errMsgInfo').html('Mã thiết bị '+rowEquip[i].equipmentCode+' có giá trị phải nhỏ hơn nguyên giá!').show();				
				return;
			}
			lstValueEquip.push(Utils.returnMoneyValue($(valEquip.target).val()));		*/
		}
		
		var checkLot = activeType.STOPPED;
		var liquidationValue = activeType.STOPPED;
		var evictionValue = activeType.STOPPED;

		// update author nhutnn, 06/08/2015
		var validationValueTL = function (i, rowEquip, giaTriThanhLy){
			if (giaTriThanhLy != null && giaTriThanhLy != undefined && !isNaN(giaTriThanhLy) && giaTriThanhLy < 1) {
				$('#errMsgInfo').html('Mã thiết bị '+rowEquip[i].equipmentCode+' giá trị thanh lý phải lớn hơn 0!').show();
				$(valEquipTL.target).focus();			
				return false;
			} else if (rowEquip[i].price != null && giaTriThanhLy != "" && Number(giaTriThanhLy) > Number(rowEquip[i].price)) {
				$('#errMsgInfo').html('Mã thiết bị '+rowEquip[i].equipmentCode+' có giá trị thanh lý phải nhỏ hơn hoặc bằng nguyên giá!').show();				
				$(valEquipTL.target).focus();
				return false;
			}
		}
		var validationValueTH = function (i, rowEquip, giaTriThuHoi){
			if (giaTriThuHoi != null && giaTriThuHoi != null && !isNaN(giaTriThuHoi) && giaTriThuHoi < 1) {
				$('#errMsgInfo').html('Mã thiết bị '+rowEquip[i].equipmentCode+' giá trị thu hồi phải lớn hơn 0!').show();				
				$(valEquipTH.target).focus();
				return false;
			} else if (rowEquip[i].price != null && giaTriThuHoi != "" && Number(giaTriThuHoi) > Number(rowEquip[i].price)) {
				$('#errMsgInfo').html('Mã thiết bị '+rowEquip[i].equipmentCode+' có giá trị thu hồi phải nhỏ hơn hoặc bằng nguyên giá!').show();				
				$(valEquipTH.target).focus();
				return false;
			}
		}

		var lstEquipCode = new Array();
		var lstLiquidationValue = new Array();
		var lstEvictionValue = new Array();
		var nSize = rowEquip.length;
		if ($('#equipmentCodeInGrid').val() != undefined) {
			nSize = rowEquip.length-1;
		}
		if ($('#checkLot').is(':checked')) {
			checkLot = activeType.RUNNING;
			liquidationValue = Utils.returnMoneyValue($('#tongThanhLy').val());
			evictionValue = Utils.returnMoneyValue($('#tongThuHoi').val());
			
			for (var i = 0; i < nSize; i++) {
				lstEquipCode.push(rowEquip[i].equipmentCode);				
			}
		} else {
			// khong check lo thi moi them  lst danh sach cac gia tri thanh ly va thu hoi
			for (var i = 0; i < nSize; i++) {
				lstEquipCode.push(rowEquip[i].equipmentCode);
				//add gia tri thanh ly
				var valEquipTL = $("#gridEquipmentLiquidation").datagrid('getEditor', {index:i,field:'liquidationValue'});
				if (valEquipTL != undefined && valEquipTL != null) {
					var gtThanhLyTmp = $(valEquipTL.target).val();
					var giaTriThanhLy = Utils.returnMoneyValue($(valEquipTL.target).val());
					if (gtThanhLyTmp == null || gtThanhLyTmp.trim().length == 0 || giaTriThanhLy == null || giaTriThanhLy.length == 0) {
						giaTriThanhLy = -1;
					} else if (!isNaN(giaTriThanhLy) && giaTriThanhLy < 1) {
						$('#errMsgInfo').html('Mã thiết bị ' + rowEquip[i].equipmentCode + ' giá trị thanh lý phải lớn hơn 0!').show();
						$(valEquipTL.target).focus();
						return false;
					} else if (rowEquip[i].price != null && giaTriThanhLy != "" && Number(giaTriThanhLy) > Number(rowEquip[i].price)) {
						$('#errMsgInfo').html('Mã thiết bị ' + rowEquip[i].equipmentCode + ' có giá trị thanh lý phải nhỏ hơn hoặc bằng nguyên giá!').show();
						$(valEquipTL.target).focus();
						return false;
					}
					lstLiquidationValue.push(giaTriThanhLy);
				} else {
					var gtThanhLyTmp = rowEquip[i].liquidationValue;
					var giaTriThanhLy = Utils.returnMoneyValue(rowEquip[i].liquidationValue);
					if (gtThanhLyTmp == null || gtThanhLyTmp.trim().length == 0 || giaTriThanhLy == null || giaTriThanhLy.length == 0) {
						giaTriThanhLy = -1;
					} else if (!isNaN(giaTriThanhLy) && giaTriThanhLy < 1) {
						$('#errMsgInfo').html('Mã thiết bị ' + rowEquip[i].equipmentCode + ' giá trị thanh lý phải lớn hơn 0!').show();
						$(valEquipTL.target).focus();
						return false;
					} else if (rowEquip[i].price != null && giaTriThanhLy != "" && Number(giaTriThanhLy) > Number(rowEquip[i].price)) {
						$('#errMsgInfo').html('Mã thiết bị ' + rowEquip[i].equipmentCode + ' có giá trị thanh lý phải nhỏ hơn hoặc bằng nguyên giá!').show();
						$(valEquipTL.target).focus();
						return false;
					}
					lstLiquidationValue.push(giaTriThanhLy);
				}
				//add gia tri thu hoi
				var valEquipTH = $("#gridEquipmentLiquidation").datagrid('getEditor', {index:i,field:'evictionValue'});
				if (valEquipTH != undefined && valEquipTH != null) {
					var gtThuHoiTmp = $(valEquipTH.target).val();
					var giaTriThuHoi = Utils.returnMoneyValue($(valEquipTH.target).val());
					if (gtThuHoiTmp == null || gtThuHoiTmp.trim().length == 0 || giaTriThuHoi == null || giaTriThuHoi.length == 0) {
						giaTriThuHoi = -1;
					} else if (!isNaN(giaTriThuHoi) && giaTriThuHoi < 1) {
						$('#errMsgInfo').html('Mã thiết bị '+rowEquip[i].equipmentCode+' giá trị thu hồi phải lớn hơn 0!').show();				
						$(valEquipTH.target).focus();
						return false;
					} else if (rowEquip[i].price != null && giaTriThuHoi != "" && Number(giaTriThuHoi) > Number(rowEquip[i].price)) {
						$('#errMsgInfo').html('Mã thiết bị '+rowEquip[i].equipmentCode+' có giá trị thu hồi phải nhỏ hơn hoặc bằng nguyên giá!').show();				
						$(valEquipTH.target).focus();
						return false;
					}
					lstEvictionValue.push(giaTriThuHoi);
				} else {
					var gtThuHoiTmp = rowEquip[i].evictionValue;
					var giaTriThuHoi = Utils.returnMoneyValue(rowEquip[i].evictionValue);
					if (gtThuHoiTmp == null || gtThuHoiTmp.trim().length == 0 || giaTriThuHoi == null || giaTriThuHoi.length == 0) {
						giaTriThuHoi = -1;
					} else if (!isNaN(giaTriThuHoi) && giaTriThuHoi < 1) {
						$('#errMsgInfo').html('Mã thiết bị '+rowEquip[i].equipmentCode+' giá trị thu hồi phải lớn hơn 0!').show();				
						$(valEquipTH.target).focus();
						return false;
					} else if (rowEquip[i].price != null && giaTriThuHoi != "" && Number(giaTriThuHoi) > Number(rowEquip[i].price)) {
						$('#errMsgInfo').html('Mã thiết bị '+rowEquip[i].equipmentCode+' có giá trị thu hồi phải nhỏ hơn hoặc bằng nguyên giá!').show();				
						$(valEquipTH.target).focus();
						return false;
					}
					lstEvictionValue.push(giaTriThuHoi);
				}
			}
		}

		var params = new Object(); 
		params.idLiquidation = $('#idRecordHidden').val().trim();
		params.docNumber = $('#docNumber').val().trim();
		params.buyerName = $('#buyerName').val().trim();
		params.buyerAddress = $('#buyerAddress').val().trim();
		params.buyerPhone = $('#buyerPhone').val().trim();
		params.reason = $('#reason').val().trim();
		params.status = $('#status').val().trim();
		params.createDate = createDate;
		params.note = $('#note').val();

		// them ma thiet bi vao neu nhap vao textbox thiet bi de validate tren server
		if ($('#equipmentCodeInGrid').val() != undefined && $('#equipmentCodeInGrid').val() != '' && $('#equipmentCodeInGrid').val().trim() != '') {
			lstEquipCode.push($('#equipmentCodeInGrid').val().trim());
			// update author nhutnn, 06/08/2015
			//add gia tri thanh ly
			var valEquipTL = $("#gridEquipmentLiquidation").datagrid('getEditor', {index:i,field:'liquidationValue'});
			if (valEquipTL != undefined && valEquipTL != null) {
				var giaTriThanhLy = Utils.returnMoneyValue($(valEquipTL.target).val());
				validationValueTL(i,rowEquip, giaTriThanhLy);
				lstLiquidationValue.push(giaTriThanhLy);
			} else {
				var giaTriThanhLy = Utils.returnMoneyValue(rowEquip[i].liquidationValue);
				validationValueTL(i,rowEquip, giaTriThanhLy);
				lstLiquidationValue.push(giaTriThanhLy);
			}
			//add gia tri thu hoi
			var valEquipTH = $("#gridEquipmentLiquidation").datagrid('getEditor', {index:i,field:'evictionValue'});
			if (valEquipTH != undefined && valEquipTH != null) {
				var giaTriThuHoi = Utils.returnMoneyValue($(valEquipTH.target).val());
				validationValueTH(i,rowEquip, giaTriThuHoi);
				lstEvictionValue.push(giaTriThuHoi);
			} else {
				var giaTriThuHoi = Utils.returnMoneyValue(rowEquip[i].evictionValue);
				validationValueTH(i,rowEquip, giaTriThuHoi);
				lstEvictionValue.push(giaTriThuHoi);
			}
		}
		// them các gia tri check lo
		params.checkLot = checkLot;
		params.liquidationValue = liquidationValue;
		params.evictionValue = evictionValue;
		if (lstEquipCode.length > 0) {
			params.lstEquipCode = lstEquipCode;
			params.lstLiquidationValue = lstLiquidationValue;			
			params.lstEvictionValue = lstEvictionValue;
		}
		
		// if($("#idRecordHidden").val() != "" && EquipmentManagerLiquidation._lstEquipInRecord != null){
		// 	params.lstEquipDelete = "";
		// 	for(var i=0; i<EquipmentManagerLiquidation._lstEquipInRecord.length; i++){
		// 		params.lstEquipDelete+=EquipmentManagerLiquidation._lstEquipInRecord[i]+',';
		// 	}
		// }

		//upload file
		var arrFile = DropzonePlugin._dropzoneObject.getFilesWithStatus(Dropzone.ADDED);
		if (EquipmentManagerLiquidation._arrFileDelete == undefined || EquipmentManagerLiquidation._arrFileDelete == null) {
			EquipmentManagerLiquidation._arrFileDelete = [];
		}
		if (arrFile != null && arrFile != undefined && arrFile.length > 0) {
			if (EquipmentManagerLiquidation._countFile == 5) {
				$('#errMsgInfo').html("Tổng số tập tin đính kèm không được vượt quá 5").show();
				return false;
			}
			var maxLength = 5 - EquipmentManagerLiquidation._countFile;
			if(maxLength<0){
				maxLength = 0;
			}
			if (arrFile.length > maxLength) {
				$('#errMsgInfo').html("Số tập tin thêm mới không được vượt quá " + maxLength).show();
				return false;
			}
			msg = UploadUtil.getMsgByFileRequireCheck(UploadUtil._typeFileRequireCheck.equipRecord);
			if (msg.trim().length > 0) {
				$('#errMsgInfo').html(msg).show();
				return false;
			}			
		}
		params.equipAttachFileStr= EquipmentManagerLiquidation._arrFileDelete.join(',');
//		Utils.getJSONDataByAjaxNotOverlay({id:$('#idRecordHidden').val().trim()}, '/equipment-manage-liquidation/check-equip-period', function (dt) {						
//			if (dt.error) {
//				$('#errMsgInfo').html(dt.errMsg).show();
//				return false;
//			} else {
//				$.messager.confirm('Xác nhận', "Bạn có muốn cập nhật biên bản này ?", function(r) {
//					if (r) {
//						if (arrFile != null && arrFile != undefined && arrFile.length > 0) {
//							params = JSONUtil.getSimpleObject2(params);
//							UploadUtil.updateAdditionalDataForUpload(params);
//							UploadUtil.startUpload('errMsgInfo');
//							return false;
//						}	
//						Utils.saveData(params, '/equipment-manage-liquidation/update-liquidation', null, 'errMsgInfo', function(data){	
//							if (data.error) {
//								$('#errMsgInfo').html(data.errMsg).show();
//								return false;
//							} else {
//								if (data.lstRecordNotSendMail != undefined && data.lstRecordNotSendMail != null && data.lstRecordNotSendMail != '') {
//									$('#errMsgInfo').html('Gửi Email không thành công biên bản báo mất: ' + data.lstRecordNotSendMail).show();
//								}
//								$('#successMsgInfo').html("Cập dữ liệu thành công.").show();
//								EquipmentManagerLiquidation._lstEquipInsert = null;
//								EquipmentManagerLiquidation._editIndex = null;
//								EquipmentManagerLiquidation._lstEquipInRecord = null;
//								EquipmentManagerLiquidation._numberEquipInRecord = null;
//								if($('#status').val() == statusRecordsEquip.DT){					
//									setTimeout(function(){
//										$('#successMsgInfo').html("").hide(); 
//										window.location.href = '/equipment-manage-liquidation/change-record?id='+ $("#idRecordHidden").val().trim();						
//									}, 3000);
//								}else{
//									setTimeout(function(){
//										window.location.href = '/equipment-manage-liquidation/info';						              		
//									}, 3000);
//								}
//							}
//						}, null, null, null, null);	
//					} else {
//						if ($("#idRecordHidden").val()!='') {
//							$('#equipmentCodeInGrid').remove();
//							$('#gridEquipmentLiquidation').datagrid({url : '/equipment-manage-liquidation/search-equipment-in-form',queryParams:{idLiquidation:$("#idRecordHidden").val()}});
//							EquipmentManagerLiquidation._editIndex = null;
//							EquipmentManagerLiquidation._lstEquipInRecord = null;
//							EquipmentManagerLiquidation._lstEquipInsert = null;
//						}
//					}
//				});
//			}
//		}, null, null);		
		$.messager.confirm('Xác nhận', "Bạn có muốn cập nhật biên bản này ?", function(r) {
			if (r) {
				if (arrFile != null && arrFile != undefined && arrFile.length > 0) {
					params = JSONUtil.getSimpleObject2(params);
					UploadUtil.updateAdditionalDataForUpload(params);
					UploadUtil.startUpload('errMsgInfo');
					return false;
				}	
				Utils.saveData(params, '/equipment-manage-liquidation/update-liquidation', null, 'errMsgInfo', function(data){	
					if (data.error) {
						$('#errMsgInfo').html(data.errMsg).show();
						return false;
					} else {
						if (data.lstRecordNotSendMail != undefined && data.lstRecordNotSendMail != null && data.lstRecordNotSendMail != '') {
							$('#errMsgInfo').html('Gửi Email không thành công biên bản báo mất: ' + data.lstRecordNotSendMail).show();
						}
						$('#successMsgInfo').html("Cập dữ liệu thành công.").show();
						EquipmentManagerLiquidation._lstEquipInsert = null;
						EquipmentManagerLiquidation._editIndex = null;
						EquipmentManagerLiquidation._lstEquipInRecord = null;
						EquipmentManagerLiquidation._numberEquipInRecord = null;
						if($('#status').val() == statusRecordsEquip.DT){					
							setTimeout(function(){
								$('#successMsgInfo').html("").hide(); 
								window.location.href = '/equipment-manage-liquidation/change-record?id='+ $("#idRecordHidden").val().trim();						
							}, 3000);
						}else{
							setTimeout(function(){
								window.location.href = '/equipment-manage-liquidation/info';						              		
							}, 3000);
						}
					}
				}, null, null, null, null);	
			} else {
				if ($("#idRecordHidden").val()!='') {
					$('#equipmentCodeInGrid').remove();
					$('#gridEquipmentLiquidation').datagrid({url : '/equipment-manage-liquidation/search-equipment-in-form',queryParams:{idLiquidation:$("#idRecordHidden").val()}});
					EquipmentManagerLiquidation._editIndex = null;
					EquipmentManagerLiquidation._lstEquipInRecord = null;
					EquipmentManagerLiquidation._lstEquipInsert = null;
				}
			}
		});
	},
	/**
	 * su kien tren grid
	 * @author nhutnn
	 * @since 18/12/2014
	 */
	editInputInGrid: function(){
		$('#equipmentCodeInGrid').bind('keyup', function(e){
			if(e.keyCode==keyCode_F9){
				EquipmentManagerLiquidation.showPopupSearchEquipment();
			}
		});

		var enterHandler = function(e) {
			if(e.keyCode==keyCodes.ENTER){
				$('#errMsgInfo').html('').hide();
				var equipCode = $('#equipmentCodeInGrid').val().trim();				
				if(equipCode != undefined && equipCode != null && equipCode != ''){
					var rowEq = $('#gridEquipmentLiquidation').datagrid('getRows');
					for(var i=0; i < rowEq.length-1; i++){
						if(rowEq[i].equipmentCode == $('#equipmentCodeInGrid').val().trim()){
							$('#errMsgInfo').html("Mã thiết bị "+rowEq[i].equipmentCode+" đã tồn tại!").show();
							return false;
						}
					}
					var params = {equipCode:equipCode};
					if($("#idRecordHidden").val() != "" && EquipmentManagerLiquidation._lstEquipInRecord != null){
						params.lstEquipDelete = "";
						for(var i=0; i<EquipmentManagerLiquidation._lstEquipInRecord.length; i++){
							params.lstEquipDelete += EquipmentManagerLiquidation._lstEquipInRecord[i]+',';
						}					
					}
					if(EquipmentManagerLiquidation._lstEquipInsert != null){
						params.lstEquipAdd = "";
						for(var i=0; i<EquipmentManagerLiquidation._lstEquipInsert.length; i++){
							params.lstEquipAdd += EquipmentManagerLiquidation._lstEquipInsert[i]+',';
						}					
					}
					Utils.getJSONDataByAjaxNotOverlay(params, '/equipment-manage-liquidation/getEquipment', function(equip){						
						if(equip != undefined && equip != null && equip.length == 1) {
							var valEquipTL = $("#gridEquipmentLiquidation").datagrid('getEditor', {index:EquipmentManagerLiquidation._editIndex,field:'liquidationValue'});
							if (valEquipTL != undefined) {
								equip[0].liquidationValue = $(valEquipTL.target).val();
							}
							var valEquipTH = $("#gridEquipmentLiquidation").datagrid('getEditor', {index:EquipmentManagerLiquidation._editIndex,field:'evictionValue'});
							if (valEquipTH != undefined) {
								equip[0].evictionValue = $(valEquipTH.target).val();
							}
							$('#gridEquipmentLiquidation').datagrid('updateRow',{
								index: EquipmentManagerLiquidation._editIndex,	
								row: equip[0]
							});
							$('#gridEquipmentLiquidation').datagrid('beginEdit', EquipmentManagerLiquidation._editIndex);
							$('#equipmentCodeInGrid').width($('#equipmentCodeInGrid').parent().width());
							EquipmentManagerLiquidation.editInputInGrid();
							EquipmentManagerLiquidation.insertEquipmentInGrid();
							// kiem tra khi them mot dong thiet bi, nhu cho change checkLot
							if ($('#checkLot').is(':checked')) {
								EquipmentManagerLiquidation.checkGiaTriThanhLyThuHoi(EquipmentManagerLiquidation._DISABLED);
							} else {
								EquipmentManagerLiquidation.checkGiaTriThanhLyThuHoi(EquipmentManagerLiquidation._ENABLE);
							}
						} else{
							$('#errMsgInfo').html("Mã thiết bị " + Utils.XSSEncode($('#equipmentCodeInGrid').val()) + " không tồn tại hoặc không được phân quyền kho!").show();
							$('#equipmentCodeInGrid').val('');
						}
						$('#equipmentCodeInGrid').change();	
					}, null, null);
				}									
			}
		};

		$('#equipmentCodeInGrid').bind('keyup', function(e){
			enterHandler(e);
		});
		if (EquipmentManagerLiquidation._editIndex != null) {
			/*var valEquip = $("#gridEquipmentLiquidation").datagrid('getEditor', {index:EquipmentManagerLiquidation._editIndex,field:'valueEquip'});
			if(valEquip!=null){
				$(valEquip.target).bind('keydown', function(e){
					if(e.keyCode==110){
						e.preventDefault();
					}
					enterHandler(e);
				});
			}*/
			// bin su kien change cho textbox thanh ly
			var valEquipTL = $("#gridEquipmentLiquidation").datagrid('getEditor', {index:EquipmentManagerLiquidation._editIndex,field:'liquidationValue'});
			if (valEquipTL != null) {
				$(valEquipTL.target).unbind('change');
				Utils.bindFormatOntextfieldCurrencyFor(Utils._TF_NUMBER_DOT);
				$(valEquipTL.target).bind('change', function(e){
					//e.preventDefault();
					//enterHandler(e);
					$('.SuccessMsgStyle').html('').hide();
					$('.ErrorMsgStyle').html('').hide();
					var row = $("#gridEquipmentLiquidation").datagrid('getRows')[EquipmentManagerLiquidation._editIndex];
					// truong hop da them dong vao
					if (row != null && (row.equipmentCode == null || row.equipmentCode == '')) {
						if ($('#equipmentCodeInGrid').val() == undefined || $('#equipmentCodeInGrid').val() == '') {
							$('#errMsgInfo').html('Bạn chưa nhập Mã thiết bị').show();
							return false;
						}
					}					
					if (row != null && row.price != null) {
						if (Number($(this).val()) > Number(row.price)) {
							$('#errMsgInfo').html('Giá trị thanh lý không được lớn hơn nguyên giá').show();
							return false;
						}
					}					
					EquipmentManagerLiquidation.refreshFooter();
				});
				// update author nhutnn, 06/08/2015
				$(valEquipTL.target).attr('id', 'inputTL' + EquipmentManagerLiquidation._editIndex).attr('maxlength', '22');
				Utils.bindFormatOnTextfield('inputTL' + EquipmentManagerLiquidation._editIndex, Utils._TF_NUMBER);
				Utils.formatCurrencyFor('inputTL' + EquipmentManagerLiquidation._editIndex);
			}
			// bin su kien change cho textbox thu hoi
			var valEquipTH = $("#gridEquipmentLiquidation").datagrid('getEditor', {index:EquipmentManagerLiquidation._editIndex,field:'evictionValue'});
			if (valEquipTH != null) {
				$(valEquipTH.target).unbind('change');
				Utils.bindFormatOntextfieldCurrencyFor(Utils._TF_NUMBER_DOT);
				$(valEquipTH.target).bind('change', function(e){
					//e.preventDefault();
					//enterHandler(e);
					$('.SuccessMsgStyle').html('').hide();
					$('.ErrorMsgStyle').html('').hide();
					var row = $("#gridEquipmentLiquidation").datagrid('getRows')[EquipmentManagerLiquidation._editIndex];
					// truong hop da them dong vao
					if (row != null && (row.equipmentCode == null || row.equipmentCode == '')) {
						if ($('#equipmentCodeInGrid').val() == undefined || $('#equipmentCodeInGrid').val() == '') {
							$('#errMsgInfo').html('Bạn chưa nhập Mã thiết bị').show();
							return false;
						}
					}
					if (row != null && row.price != null) {
						if (Number($(this).val()) > Number(row.price)) {
							$('#errMsgInfo').html('Giá trị thu hồi không được lớn hơn nguyên giá').show();
							return false;
						}
					}
					EquipmentManagerLiquidation.refreshFooter();
				});
				$(valEquipTH.target).attr('id', 'inputTH' + EquipmentManagerLiquidation._editIndex).attr('maxlength', '22');
				Utils.bindFormatOnTextfield('inputTH' + EquipmentManagerLiquidation._editIndex, Utils._TF_NUMBER);
				Utils.formatCurrencyFor('inputTH' + EquipmentManagerLiquidation._editIndex);
			}
			
		}		
	},
	/**
	 * su kien tren grid
	 * @author nhutnn
	 * @since 18/12/2014
	 */
	viewInfoFormLiquidation: function(id) {
		Utils.getHtmlDataByAjax({id:id, isView:1}, '/equipment-manage-liquidation/change-record', function(html){						
			$('#easyuiPopupViewInfo').html(html);
			$('#easyuiPopupViewInfo').show();
			$('#easyuiPopupViewInfo').dialog({  
		        closed: false,  
		        cache: false,  
		        modal: true,
		        width : 1200,
		        height : 'auto',
		        onOpen: function(){
		        	$('#statusView').customStyle();
					$('#gridEquipmentLiquidation').datagrid({
						url : '/equipment-manage-liquidation/search-equipment-in-form',
						autoRowHeight : true,
						rownumbers : true, 
						singleSelect: true,
						//pagination:true,
						fitColumns:true,
						// pageSize :20,
						// pageList  : [20],
						scrollbarSize:0,
						width: $('#gridContainerEquip').width(),
						autoWidth: true,	
						queryParams:{
							idLiquidation: id
						},
						columns:[[		  
							{field: 'typeEquipment',title:'Loại thiết bị', width: 80, sortable:false,resizable:false, align: 'left', formatter: function(v, r, i){
								  return Utils.XSSEncode(r.typeEquipment);		
							}},
							{field: 'groupEquipment',title:'Nhóm thiết bị', width: 220, sortable:false,resizable:false, align: 'left', formatter: function(value, row, index) {
						    	var html="";
						    	if(row.groupEquipmentCode != null && row.groupEquipmentName!= null){
									html = row.groupEquipmentCode+' - '+row.groupEquipmentName; 
						    	}
								return Utils.XSSEncode(html);
							}},			
							{field: 'equipmentCode',title:'Mã thiết bị',width: 220,sortable:false,resizable:false, align: 'left',formatter: function(v, r, i){
								  return Utils.XSSEncode(r.equipmentCode);		
							}},
							{field: 'seriNumber',title:'Số serial',width: 120,sortable:false,resizable:false, align: 'left',formatter: function(v, r, i){
								  return Utils.XSSEncode(r.seriNumber);		
							}},
							{field: 'price',title:'Nguyên giá',width: 150,sortable:false,resizable:false, align: 'right',formatter: function(v, r, i){
								  return formatCurrency(r.price);		
							}},
							/*{field: 'valueEquip',title:'Giá trị',width: 100,sortable:false,resizable:false, align: 'left',formatter: function(v, r, i){
								  return formatCurrency(r.valueEquip);		
							}},*/
							{field: 'liquidationValue',title:'Giá trị thanh lý',width: 100,sortable:false,resizable:false, align: 'right',formatter: function(v, r, i){
								  return formatCurrency(r.liquidationValue);		
							}},
							{field: 'evictionValue',title:'Giá trị',width: 100,sortable:false,resizable:false, align: 'right',formatter: function(v, r, i){
								  return formatCurrency(r.evictionValue);		
							}},
						    {field: 'healthStatus', title: 'Tình trạng thiết bị', width: 100, sortable:false,resizable:false,align: 'left',formatter: function(v, r, i){
								  return Utils.XSSEncode(r.healthStatus);		
							}},
							{field: 'yearManufacture', title: 'Năm sản xuất', width: 60, sortable:false,resizable:false,align: 'right',formatter: function(v, r, i){
								  return Utils.XSSEncode(r.yearManufacture);		
							}},
						]],
						onLoadSuccess :function(data){   			 	    	
					    	$(window).resize();	
					    	if (data == null || data.total == 0) {
								//$("#errMsgInfo").html("Không tìm thấy danh sách biên bản giao nhận thiết bị!").show();
							}else{
								$('.datagrid-header-rownumber').html('STT');					
							}
							// view footer; popup view
							//EquipmentManagerLiquidation.addFooterFrist();
							EquipmentManagerLiquidation.refreshFooter();
							/*if (data.footer == undefined || data.footer == null) {
								EquipmentManagerLiquidation.refreshFooter();
							}*/
						}
					});
				}, 
				onClose: function(){
					$('#easyuiPopupViewInfo').html("");
				}
			});
		}, null, null);		
	},
	/**
	 * Xoa tap tin dinh kem
	 * 
	 * @author nhutnn
	 * @since 11/02/2015
	 * */
	removeEquipAttachFile: function(id){
		if (id == undefined || id == null || isNaN(id)) {
			return;
		}
		if (EquipmentManagerLiquidation._arrFileDelete == null || EquipmentManagerLiquidation._arrFileDelete == undefined) {
			EquipmentManagerLiquidation._arrFileDelete = [];
		}
		EquipmentManagerLiquidation._arrFileDelete.push(id);
		$('#divEquipAttachFile'+id).remove();
		if (EquipmentManagerLiquidation._countFile > 0) {
			EquipmentManagerLiquidation._countFile = EquipmentManagerLiquidation._countFile - 1;
		}
	},
	/**
	 * Show popup tim kiem thiet bi
	 * @author phuongvm
	 * @since 27/05/2015
	 */

	 /**
	 * Show popup tim kiem thiet bi MH them noi thanh ly tai san
	 * @author vuongmq
	 * @since 08/07/2015
	 */
	showPopupSearchEquipment: function (isSearch) {
		$('.ErrorMsgStyle').html('').hide();
		$('.SuccessMsgStyle').html('').hide();
		$('#easyuiPopupSearchEquipment').show();
		$('#easyuiPopupSearchEquipment').dialog({  
	        closed: false,  
	        cache: false, 
	        modal: true,
	        width : 1000,
	        height: 'auto',
	        onOpen: function() {	    
	        	//setDateTimePicker('yearManufacturing');
	        	Utils.bindComboboxEquipGroupEasyUICodeName('groupEquipment', null, 193);
	        	Utils.bindComboboxEquipGroupEasyUICodeName('providerId', null, 193);

	        	var year = new Date();
	        	year = year.getFullYear(); 
	        	var op = '<option value="" selected="selected">Chọn</option><option value="'+year+'">'+year+'</option>';
	        	for(var i=1; i<100; i++){
	        		op+='<option value="'+(year-i)+'">'+(year-i)+'</option>';
	        	}
	        	$('#yearManufacturing').html(op).change();
	        	$('#equipmentCode, #seriNumber, #typeEquipment').bind('keyup',function(event){
	        		if(event.keyCode == keyCodes.ENTER){
	        			$('#btnSeachEquipmentDlg').click(); 
	        		}
	        	});
	        	$('#yearManufacturing').bind('keyup',function(event){
	        		if(event.keyCode == keyCodes.ENTER){
	        			$('#btnSeachEquipmentDlg').click(); 
	        		}
	        	});
	        	$('#groupEquipment, #providerId').next().find(".combo-text.validatebox-text").bind('keyup',function(event){
	        		if(event.keyCode == keyCodes.ENTER){
	        			$('#btnSeachEquipmentDlg').click(); 
	        		}
	        	});
	        	$('#shopCode, #shortCode, #customerName, #address').bind('keyup',function(event){
	        		if(event.keyCode == keyCodes.ENTER){
	        			$('#btnSeachEquipmentDlg').click(); 
	        		}
	        	});
	        	
	        	$('#stockCode').bind('keyup',function(event){
	        		if(event.keyCode == keyCodes.ENTER){
	        			$('#btnSeachEquipmentDlg').click(); 
	        		}
	        	});
	        	//vuongmq; 07/08/2015; khong cho F9 nua; vi lay ca NPP va KH
	        	/*$('#stockCode').bind('keyup',function(event){
	        		if(event.keyCode == keyCodes.F9){
	        			EquipmentManagerLiquidation.showPopupStock('stockCode');
	        		}
	        	});*/

	        	if ($('#stockInGrid').val() != undefined && $('#stockInGrid').val() != null && $('#stockInGrid').val() != '') {
	        		$('#stockCode').val($('#stockInGrid').val());
	        	}

	        	EquipmentManagerLiquidation.getEquipCatalog();
	        	var params = new Object();
	        	var groupId = $('#groupEquipment').combobox('getValue');
	        	if (groupId == -1 || groupId == "") {
	        		groupId = "";
	        	}
	        	var providerId = $('#providerId').combobox('getValue');
	        	if (providerId == -1 || providerId == "") {
	        		providerId = "";
	        	}
				params = {
						equipCode: $('#equipmentCode').val().trim(),
						seriNumber: $('#seriNumber').val().trim(),
						categoryId: $('#typeEquipment').val(),
						// groupId: $('#groupEquipment').val(),
						groupId: groupId,
						// providerId: $('#providerId').val(),
						providerId: providerId,
						yearManufacture: $('#yearManufacturing').val(),
						stockCode: $('#stockCode').val().trim(),
						shopCode: $('#shopCode').val().trim(),
						shortCode: $('#shortCode').val().trim()
					};
				// truong hop tim kiem: isSearch: true, 
				if (isSearch == undefined || isSearch == null) {
					var lstEquipId = new Array(); 
		        	var rows = $('#gridEquipmentLiquidation').datagrid('getRows');
					if (rows != undefined && rows != null && rows.length > 1){
						for( var i = 0, size = rows.length; i < size; i++){					
							if(rows[i].equipmentId != undefined){
								lstEquipId.push(rows[i].equipmentId);							
							}					
						}
						params.lstEquipId = lstEquipId.join(',');
					}
					if ($("#idRecordHidden").val() != "" && EquipmentManagerLiquidation._lstEquipInRecord != null) {
						params.lstEquipDelete = "";
						for(var i=0; i<EquipmentManagerLiquidation._lstEquipInRecord.length; i++){
							params.lstEquipDelete += EquipmentManagerLiquidation._lstEquipInRecord[i]+',';
						}					
					}
					if (EquipmentManagerLiquidation._lstEquipInsert != null) {
						params.lstEquipAdd = "";
						for(var i=0; i<EquipmentManagerLiquidation._lstEquipInsert.length; i++){
							params.lstEquipAdd += EquipmentManagerLiquidation._lstEquipInsert[i]+',';
						}					
					}
					params.isSearch = activeType.STOPPED;
					$('#btnSeachEquipmentDlg').attr('onclick','EquipmentManagerLiquidation.searchEquipment('+activeType.STOPPED+');');
				} else {
					params.isSearch = activeType.RUNNING;
					$('#btnSeachEquipmentDlg').attr('onclick','EquipmentManagerLiquidation.searchEquipment('+activeType.RUNNING+');');
				}
				$('#equipmentGridDialog').datagrid({
					//url : '/equipment-manage-delivery/search-equipment-delivery', // 08/07/2015; khong xai nua
					url : '/equipment-manage-liquidation/search-equipment-liquidation-popup', // vuongmq; 08/07/2015; lay danh sach popup cua thiet bi
					autoRowHeight : true,
					rownumbers : true, 
					//fitColumns:true,
					pagination:true,
					scrollbarSize:15,
					pageSize:10,
					pageList: [10],
					width: $('#equipmentGridDialogContainer').width(),
					//height: 'auto',
					autoWidth: true,	
					queryParams: params,

					frozenColumns:[[					
						{field: 'select',title:'',width: 50,sortable:false,resizable:false, align: 'left', formatter: function(value, row, index) {
					    	//return '<a href="javascript:void(0);" onclick="EquipmentManagerLiquidation.selectEquipment('+index+');">Chọn</a>';
					    	var html="";
							if (isSearch) {
								html = '<a href="javascript:void(0);" onclick="EquipmentManagerLiquidation.selectEquipmentSearch('+index+');">Chọn</a>';
							} else {
								html = '<a href="javascript:void(0);" onclick="EquipmentManagerLiquidation.selectEquipment('+index+');">Chọn</a>';
							}
					    	return html;
						}},
						{field: 'shopCode',title:'Đơn vị',width: 150,sortable:false,resizable:false, align: 'left', formatter: function(value, row, index) {
							var html = '';
							if (row.shopCode != null && row.shopName != null) {
								html += row.shopCode + ' - ' + row.shopName;
							} else if (row.shopCode != null) {
								html += row.shopCode;
							} else if (row.shopName != null) {
								html += row.shopName;
							}
							return Utils.XSSEncode(html);
						}},	
						{field: 'stockCode',title:'Kho',width: 100,sortable:false,resizable:false, align: 'left', formatter: function(value, row, index) {
							return Utils.XSSEncode(value);
						}},
						{field: 'shortCode',title:'Mã KH',width: 50,sortable:false,resizable:false, align: 'left', formatter: function(value, row, index) {
							return Utils.XSSEncode(value);
						}},
						{field: 'customerName',title:'Tên KH',width: 100,sortable:false,resizable:false, align: 'left', formatter: function(value, row, index) {
							return Utils.XSSEncode(value);
						}},
						{field: 'address',title:'Địa chỉ',width: 120,sortable:false,resizable:false, align: 'left', formatter: function(value, row, index) {
							return Utils.XSSEncode(value);
						}},
					    {field: 'equipmentCode',title:'Mã thiết bị',width: 120,sortable:false,resizable:false, align: 'left', formatter: function(value, row, index) {
							return Utils.XSSEncode(value);
						}},
					]],
					columns:[[
					    {field: 'seriNumber',title:'Số serial',width: 100,sortable:false,resizable:false, align: 'left', formatter: function(value, row, index) {
							return Utils.XSSEncode(value);
						}},		
					    {field: 'healthStatus',title:'Tình trạng thiết bị',width: 100,sortable:false,resizable:false, align: 'left', formatter: function(value, row, index) {
							return Utils.XSSEncode(value);
						}},	
						 {field: 'groupEquipment',title:'Nhóm thiết bị',width: 120,sortable:false,resizable:false, align: 'left', formatter: function(value, row, index) {
					    	var html="";
							if(row.groupEquipmentCode != null && row.groupEquipmentName!= null){
								html = row.groupEquipmentCode+' - '+row.groupEquipmentName; 
							}
							return Utils.XSSEncode(html);
						}},	
					    {field: 'typeEquipment',title:'Loại',width: 80,sortable:false,resizable:false, align: 'left', formatter: function(value, row, index) {
							return Utils.XSSEncode(value);
						}},
					    {field: 'capacity',title:'Dung tích (lít)',width: 80,sortable:false,resizable:false, align: 'right', formatter: function(value, row, index) {
							return Utils.XSSEncode(value);
						}},		
					    {field: 'equipmentBrand',title:'Hiệu',width: 80,sortable:false,resizable:false, align: 'left', formatter: function(value, row, index) {
							return Utils.XSSEncode(value);
						}},		
					    {field: 'equipmentProvider',title:'NCC',width: 80,sortable:false,resizable:false, align: 'left', formatter: function(value, row, index) {
							return Utils.XSSEncode(value);
						}},		
					    {field: 'yearManufacture',title:'Năm sản xuất',width: 80,sortable:false,resizable:false, align: 'left' },											
					]],
					onBeforeLoad:function(param){											 
					},
					onLoadSuccess :function(data){   			 
						if (data == null || data.total == 0) {
							//$("#errMsgInfo").html("Không tìm thấy danh sách biên bản giao nhận thiết bị!").show();
						}else{
							$('.datagrid-header-rownumber').html('STT');		    	
						}	    	
						$('#equipmentCode').focus();	
					}
				});
	        },
			onClose: function(){
				$('#easyuiPopupSearchEquipment').hide();
				$('#equipmentCode').val('').change();
				$('#seriNumber').val('').change();
				$('#typeEquipment').html('<option value="" selected="selected">Tất cả</option>');
				// $('#groupEquipment').html('<option value="" selected="selected">Tất cả</option>').change();
				// $('#providerId').html('<option value="" selected="selected">Tất cả</option>');
				$('#yearManufacturing').val('').change();
				$('#stockCode').val('').change();
				$('#shopCode').val('').change();
				$('#shortCode').val('').change();
				$('#customerName').val('').change();
				$('#address').val('').change();
				$('#equipmentGridDialog').datagrid('loadData',[]);
			}
		});
	},
	/**
	 * Lay loai, ncc thiet bi
	 * @author phuongvm
	 * @since 27/05/2015
	 */
	getEquipCatalog: function(){
		$('#typeEquipment').html('<option value="" selected="selected">Tất cả</option>');
    	$('#provider').html('<option value=""  selected="selected">Tất cả</option>');
    	
		$.getJSON('/equipment-manage-delivery/get-equipment-catalog', function(result) {				
			if(result.equipsCategory != undefined && result.equipsCategory != null && result.equipsCategory.length > 0) {					
				for(var i=0; i < result.equipsCategory.length; i++){
					$('#typeEquipment').append('<option value="'+ result.equipsCategory[i].id +'">'+Utils.XSSEncode(result.equipsCategory[i].code)+' - '+Utils.XSSEncode(result.equipsCategory[i].name) +'</option>');  
				}
			} 
			$('#typeEquipment').change();
			//tamvnm: doi thanh autoCompleteCombobox
			if(result.equipProviders != undefined && result.equipProviders != null && result.equipProviders.length > 0) {					
				for (var i = 0, isize = result.equipProviders.length; i < isize; i++) {
					result.equipProviders[i].searchText = unicodeToEnglish(result.equipProviders[i].code + " " + result.equipProviders[i].name);
 					result.equipProviders[i].searchText = result.equipProviders[i].searchText.toUpperCase();
				}
				var obj = {
					id: -1,
					code: "Tất cả",
					name: "Tất cả",
					codeName: "Tất cả",
					searchText: unicodeToEnglish("Tất cả").toUpperCase()
				};
				result.equipProviders.splice(0, 0, obj);
				$('#providerId').combobox("loadData", result.equipProviders);
		 		$('#providerId').combobox("setValue", -1);
			}							
		});
	},
	/**
	 * Chon thiet bi
	 * @author phuongvm
	 * @since 27/05/2015
	 */
	selectEquipment: function(index) {
		var rowPopup = $('#equipmentGridDialog').datagrid('getRows')[index];
		rowPopup.contentDelivery = null;
		if (EquipmentManagerLiquidation._editIndex!=null) {
			/*var valEquip = $("#gridEquipmentLiquidation").datagrid('getEditor', {index:EquipmentManagerLiquidation._editIndex,field:'valueEquip'});
			rowPopup.valueEquip = $(valEquip.target).val();
			$('#gridEquipmentLiquidation').datagrid('updateRow',{
				index: EquipmentManagerLiquidation._editIndex,	// index start with 0
				row: rowPopup
			});*/
			$('#gridEquipmentLiquidation').datagrid('updateRow',{
				index: EquipmentManagerLiquidation._editIndex,	// index start with 0
				row: rowPopup
			});
			EquipmentManagerLiquidation.refreshFooter();
			$('#contentDelivery').width($('#contentDelivery').parent().width());
			$('#equipmentCodeInGrid').width($('#equipmentCodeInGrid').parent().width());
			$('#seriNumberInGrid').width($('#seriNumberInGrid').parent().width());
			$('#stock').width($('#stock').parent().width());
			
			$('#gridEquipmentLiquidation').datagrid('beginEdit', EquipmentManagerLiquidation._editIndex);
			EquipmentManagerLiquidation.editInputInGrid();
//			EquipmentManagerLiquidation.insertEquipmentInGrid();

			// kiem tra khi them mot dong thiet bi, nhu cho change checkLot
			if ($('#checkLot').is(':checked')) {
				EquipmentManagerLiquidation.checkGiaTriThanhLyThuHoi(EquipmentManagerLiquidation._DISABLED);
			} else {
				EquipmentManagerLiquidation.checkGiaTriThanhLyThuHoi(EquipmentManagerLiquidation._ENABLE);
			}
		}
		
		$('#easyuiPopupSearchEquipment').dialog('close');
	},
	/**
	 * Show popup chon kho
	 * @author phuongvm
	 * @since 27/05/2015
	 */
	showPopupStock:function(txtInput){
		var txt = "stockInGrid";
		if(txtInput!=undefined){
			txt = txtInput;
		}
		$('#common-dialog-search-2-textbox').dialog("close");
		$('#common-dialog-search-2-textbox').remove();
		VCommonJS.showDialogSearch2({
			inputs : [
		        {id:'code', maxlength:50, label:'Mã đơn vị'},
		        {id:'name', maxlength:250, label:'Tên đơn vị'},
		    ],
		    url : '/commons/get-equipment-stock',
		    isCenter : true,
		    columns : [[
		        {field:'codeName', title:'Đơn vị', align:'left', width: 110, sortable:false, resizable:false, formatter:function(value,row,index) {
		        	var nameCode = row.shopCode + '-' + row.shopName;
		        	return Utils.XSSEncode(nameCode);         
		        }},
		        {field:'equipStockCode', title:'Mã kho', align:'left', width: 200, sortable:false, resizable:false},
		        {field:'choose', title:'', align:'center', width:40, sortable:false, resizable:false, formatter:function(value,row,index) {
//			        	var nameCode = row.shopCode + '-' + row.name;
		            return '<a href="javascript:void(0)" onclick="Utils.fillCodeForInputTextByDialogSearch2(\''+txt+'\', ' + row.equipStockId +', \''+row.equipStockCode+'\', \''+row.name+'\','+Utils._isShowInputTextByDlSearch2.code+');">chọn</a>';         
		        }}
		    ]]
		});
	},
	/**
	 * Chon kho
	 * @author phuongvm
	 * @since 27/05/2015
	 */
	selectStock: function(shopCode){
		$('#stockCode').val(shopCode);		
		$('#easyuiPopupSearchStock').dialog('close');
	},
	/**
	 * Tim kiem kho
	 * @author phuongvm
	 * @since 27/05/2015
	 */
	searchStock: function(){
		var params=new Object(); 
		params.status=1;
		params.isUnionShopRoot = true;
		params.code = $('#shopCodePopupStock').val().trim();
		params.name = $('#shopNamePopupStock').val().trim();
		
		$('#stockGridDialog').datagrid('load',params);
	}
};

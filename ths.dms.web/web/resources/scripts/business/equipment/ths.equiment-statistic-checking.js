var EquipStatisticChecking = {
	__listCheckId:null,
	_lstRowId: null,
	filter:null,
	_selectingEquipmentStatistics: new Map(),	// danh sach cac kiem ke dang duoc chon
	_lentghGridGroupProduct: 0,
	_lengthGridShelf: 0,
	_lstEquipId: new Map(),
	_equipHtml:'',
	_equipGroupHtml:'',
	status:0,
	type: null,
	ALL_EQUI_TYPE: '0',
	GROUP_EQUI_TYPE: '1',
	LIST_EQUI_TYPE: '3',
	_paramListStatistic: null,
	detailChecking : function(id) {
		VTUtilJS.getFormJson({id:id}, '/equipment-checking/statistic-detail', function(list) {
			$('#gridDetail').html('<h2 class="Title2Style">Danh sách nhóm thiết bị</h2>\
					<div class="SearchInSection SProduct1Form">\
					<div class="SearchInSection SProduct1Form">\
					<div class="GridSection" id=gridDetailContainer>\
					<table id="detailGrid"></table>\
					</div>\
					</div>	\
					</div>').show();
			$('#detailGrid').datagrid({
				data : list,
				autoRowHeight : true,
				rownumbers : true, 
				singleSelect: true,
				pagination:true,
				fitColumns:true,
				scrollbarSize:0,
				width: $('#gridDetailContainer').width(),
				autoWidth: true,
				columns:[[	        
				    {field : 'code' , title : 'Mã nhóm', width : 80, align : 'left', formatter : CommonFormatter.formatNormalCell},
				    {field : 'name' , title : 'Tên nhóm', width : 130, align : 'left', formatter : CommonFormatter.formatNormalCell},
				    {field : 'catName' , title : 'Loại thiết bị', width : 80, align : 'left', formatter : CommonFormatter.formatNormalCell},
				    {field : 'total' , title : 'Số lượng tổng', width : 80, align : 'right', formatter : CommonFormatter.formatNormalCell},
				    {field : 'numInstn' , title : 'Số lượng còn', width : 80, align : 'right', formatter : CommonFormatter.formatNormalCell},
				    {field : 'numLost' , title : 'Số lượng mất', width : 80, align : 'right', formatter : CommonFormatter.formatNormalCell}
			    ]],	    
			    onLoadSuccess :function(data){	    	
					$('.datagrid-header-rownumber').html('STT');			
					$(window).resize();
				}
			});
		});
	},
	/*
	* save record
	*/
	save: function() {
		$('#errorMsg').html('').hide();
		var msg ='';
		if(msg.length == 0){
			msg = Utils.getMessageOfSpecialCharactersValidate('checkingCode','Mã kiểm kê', Utils._CODE);
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfSpecialCharactersValidate('checkingName','Tên kiểm kê');
		}		
		var fdate = $('#fromDate').val().trim();
		if(msg.length == 0 && fdate != '' && !Utils.isDate(fdate) && fdate != '__/__/____'){
			msg = 'Từ ngày không tồn tại hoặc không đúng định dạng dd/mm/yyyy.';
		}
		var tdate = $('#toDate').val().trim();
		if(msg.length == 0 && tdate != '' && !Utils.isDate(tdate) && tdate != '__/__/____'){
			msg = 'Đến ngày không tồn tại hoặc không đúng định dạng dd/mm/yyyy.';
		}
		if(msg.length == 0 && fdate != '' && tdate != '' && !Utils.compareDate(fdate, tdate)){
			msg = 'Đến ngày không được nhỏ hơn Từ ngày';	
		}
		if($('#type').val().trim().length == 0) {
			$('#errorMsg').html('Bạn chưa chọn loại kiểm kê').show();
			return;
		}
		if(msg.length > 0){
			$('#errorMsg').html(msg).show();
			return false;
		}
		var warning = 'Bạn có muốn lưu thông tin?';
		var id = $('#id').val();
		var oldType = $('#oldType').val();
		var newType = $('#type').val();
		var oldStr = "";
		var newStr = "";
		if(id != null && id != undefined && id != '') {
			if (oldType != newType && oldType != EquipStatisticChecking.ALL_EQUI_TYPE) {
				if (oldType == EquipStatisticChecking.LIST_EQUI_TYPE) {
					oldStr = 'danh sách thiết bị';
				} else if (oldType == EquipStatisticChecking.GROUP_EQUI_TYPE) {
					oldStr = 'nhóm thiết bị';
				}
				if (newType == EquipStatisticChecking.ALL_EQUI_TYPE) {
					newStr = '"Tất cả thiết bị"';
				} else if (newType == EquipStatisticChecking.LIST_EQUI_TYPE) {
					newStr = '"Danh sách thiết bị"';
				} else if (newType == EquipStatisticChecking.GROUP_EQUI_TYPE) {
					newStr = '"Nhóm thiết bị"';
				}
				warning = ('Nếu chuyển Loại kiểm kê thành ' + newStr + ' thì tất cả ' + oldStr + ' sẽ bị xóa. \n' + warning);
			}
		}
		$.messager.confirm("Xác nhận", warning, function(r) {
			if(r) {
				$('#errorMsg').html('').hide();
				VTUtilJS.postFormJson('search-from', '/equipment-checking/save', function(data) {
					var idLoad = $('#id').val();
					if(data.error) {
						$('#errorMsg').html(data.errMsg).show();
					} else {
						if(data.confirmCus){
							$.messager.confirm("Xác nhận", "Mã CTHTTM hiện không có khách hàng. Bạn có muốn lưu thông tin?", function(r) {
								if(r) {
									VTUtilJS.postFormJson('search-from', '/equipment-checking/saveEx', function(data) {
										if(data.error) {
											$('#errorMsg').html(data.errMsg).show();
										} else {
											window.location.href = '/equipment-checking/edit?id='+idLoad;
										}
									});
								}else{
									window.location.href = '/equipment-checking/edit?id='+idLoad;
								}
							});
						}else{
							window.location.href = '/equipment-checking/edit?id='+data.instance.id;
						}
					}
				});
			}
		});
	},
	/*
	* event fro checkbox on shop dialog. propagration from parent to child
	*/
	onCheckShop: function(t) {
		var ck = $(t).prop("checked");
		if (ck) {
			var p = "#ck" + $(t).attr("parentId");
			EquipStatisticChecking.removeParentShopCheck(p);
			EquipStatisticChecking.removeChildShopCheck($(t).val());
		} else {
			//var p = "#ck" + $(t).attr("parentId");
			//EquipStatisticChecking.enableParentShopCheck(p);
		}
	},
	removeParentShopCheck: function(t) {
		if ($(t).length == 0) {
			return;
		}
		$(t).removeAttr("checked");
		//$(t).attr("disabled", "disabled");
		var p = "#ck" + $(t).attr("parentId");
		EquipStatisticChecking.removeParentShopCheck(p);
	},
	enableParentShopCheck: function(t) {
		if ($(t).length == 0 || $(t).attr("exists") == "1") {
			return;
		}
		$(t).removeAttr("disabled");
		var p = "#ck" + $(t).attr("parentId");
		EquipStatisticChecking.enableParentShopCheck(p);
	},
	enableChildShopCheck: function(parentId) {
		var listChild = $('input[type=checkbox][parentId='+parentId+']');
		for(var i = 0; i < listChild.length; i++) {
			var child = listChild[i];
			$(child).attr('checked', 'checked');
			EquipStatisticChecking.enableChildShopCheck($(child).val());
		}
	},
	removeChildShopCheck: function(parentId) {
		var listChild = $('input[type=checkbox][parentId='+parentId+']');
		for(var i = 0; i < listChild.length; i++) {
			var child = listChild[i];
			$(child).removeAttr("checked");
			EquipStatisticChecking.removeChildShopCheck($(child).val());
		}
	},
	/*
	* show Shop dialog
	*/
	showShopDlg: function(shopId) {
		if (shopId == undefined || shopId == null) {
			shopId = 0;
		}
		$(".ErrorMsgStyle").hide();
		var html = '<div id="add-shopPopupDiv" style="display:none;">\
			<div id="add-shopPopup">\
			<div class="PopupContentMid">\
				<div class="GeneralForm Search1Form">\
					<label class="LabelStyle" style="width:100px;">Mã đơn vị</label>\
					<input id="codeDlg" class="InputTextStyle" style="width:150px;" maxlength="40" />\
					<label class="LabelStyle" style="width:100px;">Tên đơn vị</label>\
					<input id="nameDlg" class="InputTextStyle" style="width:230px;" maxlength="100" />\
					<div class="Clear"></div>\
					<div class="BtnCenterSection">\
					    <button class="BtnGeneralStyle" id="btnSearchDlg">Tìm kiếm</button>\
					</div>\
					\
					<div class="GridSection" id="gridDlgDiv">\
						<div id="gridDlg"></div>\
					</div>\
					\
					<p id="erMsgDlg" class="ErrorMsgStyle" style="padding-left:5px;display:none;"></p>\
					<div class="Clear"></div>\
					<div class="BtnCenterSection">\
					    <button class="BtnGeneralStyle" id="btnChooseDlg">Chọn</button>\
						<button class="BtnGeneralStyle" id="btnCloseDlg" onclick="javascript:$(\'#add-shopPopup\').dialog(\'close\');">Đóng</button>\
					</div>\
				</div>\
			</div>\
			</div>\
		</div>';
		$("body").append(html);
		 
		$("#add-shopPopup").dialog({
			title: "Chọn đơn vị",
			width: 700,
			heigth: "auto",
			modal: true, close: false, cache: false,
			onOpen: function() {
				$("#add-shopPopup").addClass("easyui-dialog");
				$("#add-shopPopup #codeDlg").focus();
				
				$("#add-shopPopup #gridDlg").treegrid({
					url: "/equipment-checking/edit-shop/search-shop-dlg",
					queryParams : {recordId : $("#id").val(), id : shopId},
					rownumbers: false,
					width: $("#add-shopPopup #gridDlgDiv").width(),
					height: 350,
					fitColumns: true,
					idField: "nodeId",
					treeField: "text",
					selectOnCheck: false,
					checkOnSelect: false,
					columns: [[
						{field:"text", title:"Mã Đơn vị", sortable: false, resizable: false, width:100, align:"left", formatter:CommonFormatter.formatNormalCell},
						{field:"ck", title:"", sortable: false, resizable: false, width:35, fixed:true, align:"center", formatter: function(v, r) {
							var pId = r.attr.parentId;
							if (pId == undefined || pId == null) {
								pId = 0;
							}
							var isCheck = r.attr.isCheck;
							if(isCheck != null && isCheck != undefined && Number(isCheck) > 0) {
								return "<input type='checkbox' checked='checked' disabled='disabled' id='ck"+r.nodeId+"' value='"+r.attr.id+"' exists='0' parentId='"+pId+"' onchange='EquipStatisticChecking.onCheckShop(this);' />";
							} else {
								return "<input type='checkbox' id='ck"+r.nodeId+"' value='"+r.attr.id+"' exists='0' parentId='"+pId+"' onchange='EquipStatisticChecking.onCheckShop(this);' />";
							}
						}}
					]],
					onLoadSuccess: function(data) {
						$("#add-shopPopup #gridDlg").datagrid("resize");
						setTimeout(function() {
			    			CommonSearchEasyUI.fitEasyDialog('add-shopPopup');
			    		}, 500);
					}
				});
				
				$("#add-shopPopup #btnSearchDlg").click(function() {
					var p = {
							recordId : $("#id").val(),
							shopSearchCode: $("#add-shopPopup #codeDlg").val().trim(),
							shopSearchName: $("#add-shopPopup #nameDlg").val().trim(),
							id : shopId
					};
					//$("#add-shopPopup #gridDlg").treegrid("load", p);
					$("#add-shopPopup #gridDlg").treegrid({url: "/equipment-checking/edit-shop/search-shop-dlg",queryParams : p});
				});
				$("#add-shopPopup #codeDlg, #add-shopPopup #nameDlg").parent().keyup(function(event) {
					if (event.keyCode == keyCodes.ENTER) {
						$("#add-shopPopup #btnSearchDlg").click();
					}
				});
				$("#add-shopPopup #btnChooseDlg").click(function() {
					if ($("#add-shopPopup input[id^=ck]:checked").length == 0) {
						$("#erMsgDlg").html("Không có đơn vị nào được chọn").show();
						return;
					}
					var lstTmp = new Map();
					var v;
					$("#add-shopPopup input[id^=ck]:checked").each(function() {
						if($(this).attr('disabled') != 'disabled') {
							v = $(this).val().trim();
							lstTmp.put(v, v);
						}
					});
					EquipStatisticChecking.addShopQtt(lstTmp);
				});
				$("#add-shopPopup #codeDlg").focus();
			},
			onClose: function() {
				$("#add-shopPopup").dialog("destroy");
				$("#add-shopPopupDiv").remove();
			}
		});
	},
	addShopQtt: function(lstShopTmp) {
		$(".ErrorMsgStyle").hide();
		$.messager.confirm("Xác nhận", "Bạn có muốn thêm đơn vị?", function(r) {
			if (r) {
				var params = {
						id: $("#id").val(),
						listId : lstShopTmp.keyArray
				};
				$("#add-shopPopup").dialog("close");
				VTUtilJS.postFormJson(params, '/equipment-checking/edit-shop/save-shop', function(data) {
					if(data.error) {
						$('#errorMsg').html(data.errMsg).show();
					} else {
						$('#successMsg').html('Lưu dữ liệu thành công').show();
						setTimeout(function() {
							$('#successMsg').html('').hide();
						}, 3000);
						$("#gridShop").treegrid("reload", {recordId:$("#id").val()});
					}
				});
			}
		});
	},
	/*
	* delete shop of record
	*/
	deleteShopMap: function(shopId) {
		$(".ErrorMsgStyle").hide();
		var params = {
				id: $("#id").val().trim(),
				shopId: shopId
		};
		VTUtilJS.postFormJson(params, '/equipment-checking/edit-shop/check-child-shop', function(data) {
			if(data.deleteChild) {
				$.messager.confirm("Xác nhận", "Xóa đơn vị sẽ thì sẽ xóa các thiết bị liên quan đến đơn vị này.\nBạn có muốn xóa đơn vị và các đơn vị con?", function(r) {
					if (r) {
						params.deleteChild = true;
						VTUtilJS.postFormJson(params, '/equipment-checking/edit-shop/delete-shop', function(data) {
							if(data.error) {
								$('errorMsg').html(data.errMsg).show();
							} else {
								$('#successMsg').html('Lưu dữ liệu thành công').show();
								setTimeout(function() {
									$('#successMsg').html('').hide();
								}, 3000);
								$("#gridShop").treegrid("reload", {recordId:$("#id").val()});
							}
						});
					} else {
						params.deleteChild = false;
						VTUtilJS.postFormJson(params, '/equipment-checking/edit-shop/delete-shop', function(data) {
							if(data.error) {
								$('errorMsg').html(data.errMsg).show();
							} else {
								$('#successMsg').html('Lưu dữ liệu thành công').show();
								setTimeout(function() {
									$('#successMsg').html('').hide();
								}, 3000);
								$("#gridShop").treegrid("reload", {recordId:$("#id").val()});
							}
						});
					}
				});
			} else {
				$.messager.confirm("Xác nhận", "Xóa đơn vị sẽ thì sẽ xóa các thiết bị liên quan đến đơn vị này.\nBạn có muốn xóa đơn vị?", function(r) {
					if (r) {
						params.deleteChild = false;
						VTUtilJS.postFormJson(params, '/equipment-checking/edit-shop/delete-shop', function(data) {
							if(data.error) {
								$('errorMsg').html(data.errMsg).show();
							} else {
								$('#successMsg').html('Lưu dữ liệu thành công').show();
								setTimeout(function() {
									$('#successMsg').html('').hide();
								}, 3000);
								$("#gridShop").treegrid("reload", {recordId:$("#id").val()});
							}
						});
					}
				});
			}
		});
	},
	updateEquipmentStatisticStatus: function() {
		$('#errMsg').html('').hide();	
		var statusForm = $('#cbxStatusUpdate').val().trim();
		if(statusForm < 0) {
			$('#errMsg').html('Vui lòng chọn trạng thái').show();
			return;
		}
		var arra = EquipStatisticChecking._selectingEquipmentStatistics.valArray;
		var lstId = new Array();
		if(arra == null || arra.length == 0) {
			$('#errMsg').html('Bạn chưa chọn kiểm kê để cập nhật. Vui lòng chọn kiểm kê').show();
			return;
		}		
		for(var i=0, size = arra.length; i < size; ++i) {
			lstId.push(arra[i].id);
		}
		var params = new Object();
		params.listId = lstId;
		params.equipmentStatisticRecordStatus = statusForm;
		Utils.addOrSaveData(params, '/equipment-checking/update-statistic-status', null, 'errMsg', function(data) {
			if(data.updateFailedRecords!=undefined && data.updateFailedRecords!=null && data.updateFailedRecords.length > 0){
				VCommonJS.showDialogList({
				    data : data.updateFailedRecords,
				    columns : [[
				        {field:'formCode', title:'Mã biên bản', align:'left', width: 110, sortable:false, resizable:false},
				        {field:'formStatusErr', title:'Trạng thái biên bản', align:'center', width: 110, sortable:false, resizable:false, formatter: function(value,row,index){
				        	var html = "";
							if(value == 1){
								html = '<img src="/resources/images/icon_delete.png" height="20" width="20" style="margin-right:5px;" title="Error">';
							}
							return html;
						}},
						{field:'notGroupOrShop', title:'Không có nhóm và đơn vị', align:'center', width: 110, sortable:false, resizable:false, formatter: function(value,row,index){
				        	var html = "";
							if(value == 1){
								html = '<img src="/resources/images/icon_delete.png" height="20" width="20" style="margin-right:5px;" title="Error">';
							}
							return html;
						}},
						{field:'groupInActive', title:'Các nhóm không hoạt động', align:'left', width: 110, sortable:false, resizable:false, formatter: function(value,row,index){
				        	var html = "";
							if(value != ""){
								html = value;//'<img src="/resources/images/icon_delete.png" height="20" width="20" style="margin-right:5px;" title="Error">';
							}
							return html;
						}},
						{field:'groupNotEquip', title:'Các nhóm không có thiết bị thuộc đơn vị để kiểm kê', align:'left', width: 110, sortable:false, resizable:false, formatter: function(value,row,index){
				        	var html = "";
							if(value != ""){
								html = value;//'<img src="/resources/images/icon_delete.png" height="20" width="20" style="margin-right:5px;" title="Error">';
							}
							return html;
						}},
						{field:'shopInActive', title:'Các đơn vị không hoạt động', align:'left', width: 110, sortable:false, resizable:false, formatter: function(value,row,index){
				        	var html = "";
							if(value != ""){
								html = value;//'<img src="/resources/images/icon_delete.png" height="20" width="20" style="margin-right:5px;" title="Error">';
							}
							return html;
						}}
					]]
				});	
			}
			EquipStatisticChecking._selectingEquipmentStatistics = new Map();
			$('#grid').datagrid('reload');
		}, null, true,null, 'Bạn có muốn cập nhật trạng thái của kiểm kê ?');
	},
	/*
	* save group checking
	*/
	saveGroupChecking : function(recordId) {
		$('#detailGrid').datagrid('acceptChanges');
		var rows = $('#detailGrid').datagrid('getRows');
		var listDetailId = new Array();
		var listExists = new Array();
		for(var i = 0; i < rows.length; i++) {
			if(rows[i].isBelong != null && rows[i].isBelong != undefined && (rows[i].isBelong == 0 || rows[i].isBelong == 1)) {
				listDetailId.push(rows[i].detailId);
				listExists.push(rows[i].isBelong);
			}
		}
		
		if(listDetailId.length == 0 || listExists.length == 0) {
			$('#errorMsg').html('Bạn chưa chọn kiểm kê để lưu').show();
			return;
		}
		
		for(var i = 0; i < rows.length; i++) {
			$('#detailGrid').datagrid('beginEdit', i);
		}
		$.messager.confirm('Xác nhận','Bạn có muốn lưu thông tin?',function(r){
			if(r) {
				VTUtilJS.postFormJson({id: $('#id').val(),listDetailId : listDetailId, listExists : listExists}, '/equipment-checking/save-group-checking', function(data) {
					if(data.error) {
						$('#errorMsg').html(data.errMsg).show();
					} else {
						$('#successMsg').html('Lưu dữ liệu thành công').show();
						setTimeout(function() {
							$('#successMsg').html('').hide();
						}, 3000);
						var shopId = $('#selectShop').val();
						var equipGroupId = $('#selectEquipGroup').val();
						var equipCode = $('#equipCode').val().trim();
						var stockId = $('#selectStock').val();
						var soLan = $('#soLan').val();
						var seri = $('#seri').val();
						var checkDate = $('#checkDate').val();
						var obj = new Object();
						if(shopId != null && shopId != -1) {
							obj.shopId = shopId;
						}
						if(equipGroupId != null && equipGroupId != -1) {
							obj.equipGroupId = equipGroupId;
						}
						obj.equipCode = equipCode;
						if(stockId != null && stockId != -1) {
							obj.stockId = stockId;
						}
						if(soLan != null) {
							obj.stepCheck = soLan;
						}
						if(checkDate!=null){
							obj.checkDate = checkDate;
						}
						obj.seri = seri;
						obj.isFirstLoad = false;
						$('#detailGrid').datagrid('load', obj);
					}
				});
			}
		});
	},
	/*
	* save shelf checking
	* update author nhutnn
	* update since 05/08/2015
	*/
	saveShelfChecking : function(recordId) {
		$('.ErrorMsgStyle ').html('').hide();
		$('#detailGrid').datagrid('acceptChanges');
		var rows = $('#detailGrid').datagrid('getRows');
		var listDetailId = new Array();
		var listActQty = new Array();
		for(var i = 0; i < rows.length; i++) {
			// if(rows[i].actualQuantity != '' && !isNaN(rows[i].actualQuantity)) {
				listDetailId.push(rows[i].detailId);
				listActQty.push(rows[i].actualQuantity);
			// }
		}
		for(var i = 0; i < rows.length; i++) {
			$('#detailGrid').datagrid('beginEdit', i);
		}
		if(listDetailId.length == 0 || listActQty.length == 0) {
			$('#errorMsg').html('Bạn chưa chọn kiểm kê để lưu').show();
			return;
		}
		EquipStatisticChecking.bindEventInput();
		$.messager.confirm('Xác nhận','Bạn có muốn lưu thông tin?',function(r){
			if(r) {
				VTUtilJS.postFormJson({id: $('#id').val(),listDetailId : listDetailId, listActQty : listActQty}, '/equipment-checking/save-shelf-checking', function(data) {
					if(data.error) {
						$('#errorMsg').html(data.errMsg).show();
					} else {
						$('#successMsg').html('Lưu dữ liệu thành công').show();
						setTimeout(function() {
							$('#successMsg').html('').hide();
						}, 3000);
						var shopId = $('#selectShop').val();
						var equipGroupId = $('#selectEquipGroup').val();
						var equipCode = $('#equipCode').val();
						var stockId = $('#selectStock').val();
						var soLan = $('#soLan').val();
						var seri = $('#seri').val();
						var checkDate = $('#checkDate').val();
						var obj = new Object();
						if(shopId != null && shopId != -1) {
							obj.shopId = shopId;
						}
						if(equipGroupId != null && equipGroupId != -1) {
							obj.equipGroupId = equipGroupId;
						}
						obj.equipCode = equipCode;
						if(stockId != null && stockId != -1) {
							obj.stockId = stockId;
						}
						if(soLan != null) {
							obj.stepCheck = soLan;
						}
						if(checkDate!=null){
							obj.checkDate = checkDate;
						}
						obj.seri = seri;
						obj.isFirstLoad = false;
						$('#detailGrid').datagrid('load', obj);
					}
				});
			}
		});
	},
	detailImageRecord: function(id, detailId, shopCode, shortCode, equipCode, cusCode) {
		EquipStatisticChecking._paramListStatistic.recordId = id;
		EquipStatisticChecking.recordId = id;
//		EquipStatisticChecking.detailId = detailId;
//		EquipStatisticChecking.shopCodeSearch = shopCode;
		EquipStatisticChecking.customerCodeSearch = cusCode;
//		EquipStatisticChecking.equipCodeSearch = equipCode;
		EquipStatisticChecking.showDialogFancy();
	},
	/*
	* upload image
	*/
	uploadingImageRecord : function(id, detailId, existsNumber) {
		$('#errExcelMsgDVKK').html('').hide();
		var msg = ''; 
		if(msg.length == 0){
			msg = Utils.getMessageOfSpecialCharactersValidate('imageFile','Tên hình ảnh',Utils._NAME);
		}
		//Datpv4 check number images
		var params = EquipStatisticChecking._paramListStatistic;
		params.equipId = id;
		params.detailId = detailId;
		params.max = 20;
		params.page = 1;
		Utils.getJSONDataByAjaxNotOverlay(params, '/equipment-checking/get-images-for-popup', function(data){						
			existsNumber = data.lstImage.length;
			var size = existsNumber + EquipStatisticChecking.countArray.size();		

			if(size > 5){
				msg = 'Chỉ cho phép upload tối đa 5 hình cho 1 sản phẩm.';
			}
			if(msg.length == 0 && $('#fileQueue').html().length == 0 && EquipStatisticChecking.countArray.size() == 0){
				msg = 'Không có hình nào được chọn.';
			}
			var sizeMax = 5 * 1024 * 1024;
			if(msg.length == 0 && EquipStatisticChecking.countArray.size() > 0) {
				var rowsKeyArray = EquipStatisticChecking.countArray.keyArray;
				for (var i = 0, size = rowsKeyArray.length; i < size; i ++) {
					var rowImage = EquipStatisticChecking.countArray.get(rowsKeyArray[i]);
					if (rowImage.size > sizeMax) {
						msg = rowImage.name + ' có kích thước phải nhỏ hơn hoặc bằng 5Mb';
						break;
					}
				}
			}
			if(msg.length > 0) {
				$('#errExcelMsgDVKK').html(msg).show();
				return false;
			}
			
			$('#divOverlay').show();
			$('#imageFile').uploadifySettings('scriptData', {
				'recordId' : id,
				'detailId' : detailId,
				'currentUser' : currentUser
			});
			$('#imageFile').uploadifyUpload();
			return false;
		});	
	},
	countArray : null,
	/*
	* upload dialog
	*/
	uploadImageRecord : function(id, detailId, existsNumber) {
		var html = $('#easyuiPopupImportExcelContainer').html();
		var __html = $('#easyuiPopupImportExcelEx').html();
		$('#easyuiPopupImportExcelContainer').show();
		$('.ErrorMsgStyle').hide();
		EquipStatisticChecking.countArray = new Map();
		$('#easyuiPopupImportExcelEx').dialog({
	        closed: false,  
	        cache: false,  
	        modal: true,
	        height: 'auto',
	        onOpen: function(){
	        	$('.easyui-dialog .BtnGeneralStyle').css('color','');
	        	var countSuccess = 0;
	        	var countFail = 0;
	        	$('#easyuiPopupImportExcelEx #imageFile').uploadify({
	    			'auto'      : false,
	    			'script'    : '/upload-record-img',
	    			'method'	: 'GET',
	    			'cancelImg' : '/resources/scripts/plugins/uploadify2.1/cancel.png',
	    			'buttonImg' : '/resources/images/bg_multiBtn.jpg',
	    			'moverImg'  : '/resources/images/bg_multiBtn.jpg',
	    			'width': 139,
	    			'height': 30,
	    			'fileDesc'	: 'Image Files *.jpg; *.jpeg; *.png',			
	    			'fileExt': '*.jpg;*.jpeg;*.png',
	    			'multi': true,
	    			'queueClass': 'uploadifyQueue',
	    			'queueID'	: 'fileQueue',
	    			'displayData': 'percentage',
	    			//'sizeLimit':'5242880',
	    			'removeCompleted':true,
	    			'onInit' 	: function() {},
	    			'onSelect'  : function(event, queueID, fileObj,data) {
	    				$('#imageMsg').html('').hide();
	    				/*
	    				var sizemax = 5 * 1024 * 1024;
	    				if (fileObj.size <= sizemax) {				
	    					EquipStatisticChecking.countArray.put(queueID,fileObj);
	    				}
	    				*/
	    				EquipStatisticChecking.countArray.put(queueID,fileObj);
	    			},			
	    		    'onCancel'  : function(event, queueID, fileObj,data) {		    	
	    		    	EquipStatisticChecking.countArray.remove(queueID);
	    		    },
	    			'onQueueFull': function(event,queueSizeLimit){},
	    			'onComplete' : function(event, queueID, fileObj, response, data) {				
	    				eval("var obj=" + response);
	    				if(obj.errMsg!=null && obj.errMsg.length > 0){
	    					$('#errExcelMsgDVKK').html(obj.errMsg).show();
	    					return;
	    				}
	    				if(obj.status == 0) {
	    					countFail++;
	    					$('#errExcelMsgDVKK').html(obj.errMsg).show();
	    					/*setTimeout(function(){
	    					},300);*/
	    				} else {
	    					countSuccess++;
	    				}
	    				$('#countFail').val(countFail);
	    				$('#countSuccess').val(countSuccess);
	    				$('#'+detailId).show();
	    			},
	    			'onAllComplete': function(event,data){
	    				var msg = '';
	    				$('#divOverlay').hide();
	    				if($('#countSuccess').val() == '0' && $('#countFail').val() == '0') {
	    					return;
	    				}
    					if(parseInt($('#errorValue').val().trim()) == 1){							
    						msg = "Tải thành công <b>"+ $('#countSuccess').val() +"</b> hình ảnh,tải thất bại <b>"+ $('#countFail').val() +"</b> hình ảnh";							
    					} else if($('#mediaType').val() == '' || $('#mediaType').val() == 1){								
    						msg = "Tải thành công <b>"+ $('#countSuccess').val() +"</b> hình ảnh,tải thất bại <b>"+ $('#countFail').val() +"</a> hình ảnh";							
    					}
    					var flagError = false;
    					if($('#countSuccess').val() == '0') {
    						$('#errExcelMsgDVKK').html(msg).show();
    						flagError = true;
    					} else {
    						$('#successExcelMsgDVKK').html(msg).show();
    					}
    					$('#countSuccess').val(0);
    					$('#countFail').val(0);
    					if (flagError) {
    						return;
    					}
    					var tm = setTimeout(function(){
    						$('.ErrorMsgStyle').html('').hide();
    						$('.SuccessMsgStyle').html('').hide();
    						$('#easyuiPopupImportExcelEx').window('close');
							clearTimeout(tm);
						}, 1500);
	    			},
	    			'onError': function (event, queueID, fileObj, errorObj) {	
	    				if(fileObj.size>5242880){
	    					$('#imageFile'+queueID).find('.percentage').addClass('ErrorMsgStyle').text("-  Kích thước file vượt quá 5 MB");					
	    				}								
	    			},
	    			'onUploadSuccess':function(file,data,response){				
	    			}			
	    		});
	        	
	        	$('#uploadImageFile').bind('click', function() {
	        		EquipStatisticChecking.uploadingImageRecord(id, detailId, existsNumber);
	        	});
	        },
	        onClose: function(){
	        	$('#easyuiPopupImportExcelContainer').hide();
	        	$('#easyuiPopupImportExcelEx').html(__html);
	        	$('#easyuiPopupImportExcelContainer').html(html);
	        }
		});
	},
	pagePopup : 0,
	callBack : null,
	shopCodeSearch:null,
	customerCodeSearch:null,
	equipCodeSearch:null,
	detailId:null,
	recordId:null,
	lstImages:null,
	indexCurrent:null,
	downloadFile:function(temp, ow){
		var url = temp.urlEx;
		var customerInfo = temp.customerCode+' - '+temp.customerName+' - '+temp.customerAddress;
		var shopInfo = temp.shopCode+' - '+temp.shopName;
		var dateInfo = temp.dmyDate+' - '+temp.hhmmDate;
		var data=new Object();
		data.id = temp.id;
		data.urlImage=url;
		data.customerInfo=customerInfo;
		data.shopInfo=shopInfo;
		data.dateInfo=dateInfo;
		data.overwrite=true;
		if(temp.monthSeq!=undefined && temp.monthSeq!=null){
			data.monthSeqDownload=temp.monthSeq;
		}
		$('.loadingPopup').show();			
		$('.fancybox-inner .DownloadPhotoLink').hide();
		var kData = $.param(data, true);
		$.ajax({
			type : 'GET',
			url : '/equipment-checking/download/image',
			data :(kData),
			dataType : "json",
			success : function(data) {
				$('.loadingPopup').hide();
				$('.fancybox-inner .DownloadPhotoLink').show();
				if(!data.error && data.errMsg!= null && data.errMsg.length > 0){
					$('.fancybox-inner .DownloadPhotoLink').attr('href',data.errMsg);
				}else{
					$('.fancybox-inner .DownloadPhotoLink').attr('href',imgServerSOPath+url);
				}
			},
		error:function(XMLHttpRequest, textStatus, errorThrown) {$('.loadingPopup').hide();}});
	},
	imageBigShow : function() {
		$('.fancybox-inner #loadingImages').hide();
		$('.fancybox-inner #bigImages').fadeIn(1000);
	},
	imageLoadingShow : function() {
		$('.fancybox-inner #bigImages').hide();
		$('.fancybox-inner #loadingImages').show();
	},
	lazyLoadImages:function(object,container){
		$(object).lazyload({
			event: "scrollstop",
			container : $(container),
			effect: "fadeIn",
	        failurelimit: 1,
	        threshold: -20
		});
		$(object).each(function() {
		    $(this).attr("src", $(this).attr("data-original"));
		    $(this).removeAttr("data-original");
		});
	},
	focusStyleThumbPopup:function(id){
		$('.fancybox-inner .FocusStyle').removeClass('FocusStyle');
		$('.fancybox-inner #li'+id).addClass('FocusStyle');
	},
	imageLoadingShow : function() {
		$('.fancybox-inner #bigImages').hide();
		$('.fancybox-inner #loadingImages').show();
	},
	openMapOnImage : function() {
		VTMapUtil.loadMapResource(function(){
			var temp = EquipStatisticChecking.lstImages.get(EquipStatisticChecking.indexCurrent);
			if(temp!=null){
				if($('.OnFucnStyle').is(':visible')){					
					//$('.OffFucnStyle').hide();
					//$('.OffFucnStyle').css('display','none');
					//$('.OnFucnStyle').show();
					//$('.OnFucnStyle').css('display','block');
					$('.fancybox-inner #divMapSection').removeClass('SmallMapSection');
					$('.fancybox-inner #divMapSection').addClass('LargeMapSection');
					$('.fancybox-inner #bigMap').css('width','488px');
					$('.fancybox-inner #bigMap').css('height','409px');
					ViettelMap.loadMapForEquipmentImages('bigMap',temp.lat,temp.lng,13,1, temp.custLat, temp.custLng);
				}
				else {
					//$('.OnFucnStyle').hide();
					//$('.OnFucnStyle').css('display','none');
					//$('.OffFucnStyle').show();
					//$('.OffFucnStyle').css('display','block');
					$('.fancybox-inner #divMapSection').removeClass('LargeMapSection');
					$('.fancybox-inner #divMapSection').addClass('SmallMapSection');
					$('.fancybox-inner #bigMap').css('width','140px');
					$('.fancybox-inner #bigMap').css('height','120px');
					ViettelMap.loadMapForEquipmentImages('bigMap',temp.lat,temp.lng,13,0, temp.custLat, temp.custLng);
				}
			}
		});
	},
	showBigImagePopup:function(id){
		try {
			var temp=EquipStatisticChecking.lstImages.get(id);
			if(temp!=null){
				EquipStatisticChecking.indexCurrent = temp.id;
				EquipStatisticChecking.focusStyleThumbPopup(temp.id);
				EquipStatisticChecking.imageLoadingShow();
				//EquipStatisticChecking.openMapOnImage();
				$('.fancybox-inner .DownloadPhotoLink').hide();
				$('.fancybox-inner #shopImage').html(Utils.XSSEncode(temp.shopCode)+' - '+Utils.XSSEncode(temp.shopName));
				$('.fancybox-inner #staffImage').html(Utils.XSSEncode(temp.staffCode)+' - '+Utils.XSSEncode(temp.staffName));
				$('.fancybox-inner #codeEquipImage').html('Mã thiết bị: ' + Utils.XSSEncode(temp.equipCode) + ' / Số serial: ' + Utils.XSSEncode(temp.seriNumber));
				$('.fancybox-inner #typeEquipImage').html('Loại thiết bị: ' + Utils.XSSEncode(temp.equipCategoryName) + ' / Nhóm thiết bị: ' + Utils.XSSEncode(temp.equipNameGroup));
				$('.fancybox-inner #timeImage').html(temp.dmyDate+' - '+temp.hhmmDate);
				$('.fancybox-inner .DownloadPhotoLink').attr('href',temp.urlImage);
				$('.fancybox-inner .DownloadPhotoLink').attr('download',temp.urlEx.substring(temp.urlEx.lastIndexOf("/")+1));
//				if($('#tab4').hasClass('Active')){
//					$('.fancybox-inner #divDetail').attr('value',temp.id);
//					if (temp.result != null && temp.result == 1) {
//						$('.fancybox-inner #chkResult').attr('checked', 'checked');
//					} else if(temp.result != null && temp.result == 0) {
//						$('.fancybox-inner #chkNotResult').attr('checked', 'checked');
//					} else {
//						$('.fancybox-inner #chkNotResult').removeAttr('checked');
//						$('.fancybox-inner #chkResult').removeAttr('checked');
//					}
//					if (temp.isInSpected != null && temp.isInSpected == 1) {
//						$('.fancybox-inner #chkInspect').attr("checked", "checked");
//						$('.fancybox-inner #chkInspect').attr("disabled", "disabled");
//						$('.fancybox-inner #chkResult').attr("disabled", "disabled");
//						$('.fancybox-inner #chkNotResult').attr("disabled", "disabled");
//					} 
//					if (temp.numberProduct != null) {
//						$('.fancybox-inner #txtNumberFace').val(temp.numberProduct);
//					} else {
//						$('.fancybox-inner #txtNumberFace').val("");
//					}
//					if (temp.POSM != null && temp.POSM != "") {
//						$('.fancybox-inner #txtPOSM').val(temp.POSM);
//					} else {
//						$('.fancybox-inner #txtPOSM').val("");
//					}
//					if (temp.displayNote != null && temp.displayNote != "") {
//						$('.fancybox-inner #txtNoted').val(temp.displayNote);
//					} else {
//						$('.fancybox-inner #txtNoted').val("");
//					}
//				}
				EquipStatisticChecking.downloadFile(temp, false);//lay base64
				$('.DeletePhotoLink').attr('value',temp.id);
				var currentMil = new Date().getTime();
				var image = temp.urlImage+'?v='+currentMil;
				if (image != null && image != '' && image != undefined) {
					if (document.images) {
						var img = new Image();
						img.src = image;
						img.onload = function() {
							$('.fancybox-inner #bigImages').attr('src',image);
							EquipStatisticChecking.imageBigShow();
						};
					}
				}
			}
		} catch (err) {
			return;
		}
	},
	imageSelect:function() {
		var id=$(this).attr('value');
		if($('#minimizePopup').length != 0) {//dialog nho
			var func = $('#minimizePopup img').attr('onclick');
			if(func.indexOf('Full') != -1) {
				func = 'EquipStatisticChecking.showDialogFancyFull()';
			} else {
				func = 'EquipStatisticChecking.showDialogFancy()';
			}
			$('#minimizePopup img').attr('onclick', func);
		} else {//dialog lon
			var func = $('#maximizePopup img').attr('onclick');
			if(func.indexOf('Full') != -1) {
				func = 'EquipStatisticChecking.showDialogFancyFull()';
			} else {
				func = 'EquipStatisticChecking.showDialogFancy()';
			}
			$('#maximizePopup img').attr('onclick', func);
		}
		EquipStatisticChecking.showBigImagePopup(id);
	},
	fillThumbnails:function(list){
		var listImage = '';
		for (var i = 0; i < list.keyArray.length; i++) {
			var temp = list.get(list.keyArray[i]);
			if(temp!=null){
				var currentMil = new Date().getTime();
				listImage += '<li id="li'+temp.id+'">';
				listImage +='<div class="PhotoThumbnails BoxMiddle"><span class="BoxFrame"><span class="BoxMiddle"><img id="img'+temp.id+'" class="imageSelect ImageFrame2" src="/resources/images/grey.jpg"'; 
				listImage +=' data-original="'+(temp.urlThum+'?v='+currentMil)+'" width="142" height="102" value="'+temp.id+'" /></span></span></div>';
				listImage +='<p>'+temp.dmyDate+' – '+temp.hhmmDate+'</p>';
				listImage += "</li>";
			}
		}
		$('.fancybox-inner #listImageUL').append(listImage);
		EquipStatisticChecking.lazyLoadImages(".fancybox-inner .imageSelect",".fancybox-inner #listImage");
		$('.imageSelect').unbind('click');
		$('.imageSelect').click(EquipStatisticChecking.imageSelect);
		if(list.keyArray.length>0){
			setTimeout(function(){EquipStatisticChecking.showBigImagePopup(list.keyArray[0]);},500);
		}
	},
	getImagesForPopup:function(callBack,countPage){
		var data = EquipStatisticChecking._paramListStatistic;
		if (countPage != undefined && countPage != null && countPage != 0) {
			data.max = 20;
			data.page = 1;
			EquipStatisticChecking.pagePopup = countPage;
		} else {
			data.max = 20;
			data.page = EquipStatisticChecking.pagePopup;
		}
		if (data.page == 0) { 
			scrollIntoViewPopup=1;
		}
		if (callBack != undefined && callBack != null) {
			EquipStatisticChecking.callBack=callBack;
		}
		$('.loadingPopup').show();
		var kData = $.param(data, true);
		$.ajax({
			type : 'POST',
			url : '/equipment-checking/get-images-for-popup',
			data :(kData),
			dataType : "json",
			success : function(data) {
				$('.loadingPopup').hide();
				if(data.error){
					
				}else{
					var lstImage = data.lstImage;
					var listNew=new Map();
					if(lstImage!=undefined && lstImage!=null && lstImage.length>0) {
						EquipStatisticChecking.pagePopup++;//nếu có dữ liệu thì + trang lên 1
						for(var i=0;i<lstImage.length;i++){
							var currentMil = new Date().getTime();
							lstImage[i].urlEx=lstImage[i].urlImage;
							lstImage[i].urlImage=imgServerSOPath+lstImage[i].urlImage+'?v='+currentMil;
							lstImage[i].urlThum=imgServerSOPath+lstImage[i].urlThum+'?v='+currentMil;
							if(lstImage[i].shopCode==undefined || lstImage[i].shopCode==null) lstImage[i].shopCode='';
							if(lstImage[i].shopName==undefined || lstImage[i].shopName==null) lstImage[i].shopName='';
							if(lstImage[i].staffCode==undefined || lstImage[i].staffCode==null) lstImage[i].staffCode='';
							if(lstImage[i].staffName==undefined || lstImage[i].staffName==null) lstImage[i].staffName='';
							if(EquipStatisticChecking.lstImages.get(lstImage[i].id)==null){
								listNew.put(lstImage[i].id,lstImage[i]);
							}
							if(i==0){
								EquipStatisticChecking.indexCurrent = lstImage[i].id;
							}
							EquipStatisticChecking.lstImages.put(lstImage[i].id,lstImage[i]);							
							var temp = EquipStatisticChecking.lstImages.get(EquipStatisticChecking.indexCurrent);
							if (EquipStatisticChecking.customerCodeSearch != null && EquipStatisticChecking.customerCodeSearch != '') {
								$('#titlePopup').html(Utils.XSSEncode(EquipStatisticChecking.customerCodeSearch));
							} else {
								$('#titlePopup').html(Utils.XSSEncode(temp.shopCode + ' - ' + temp.shopName));
							};
						}
					} else {
						if(EquipStatisticChecking.pagePopup == 0){
							$.fancybox.close();
						}
						if(EquipStatisticChecking.callBack != undefined && EquipStatisticChecking.callBack != null){
							EquipStatisticChecking.callBack.call();
						}
					}
					EquipStatisticChecking.fillThumbnails(listNew);
				}
			},
			error:function(XMLHttpRequest, textStatus, errorThrown) {
			}
		});
	},
	showDialogFancy:function() {
		/*if($('.OffFucnStyle').is(':visible')){
			$('.OffFucnStyle').hide();
			$('.OffFucnStyle').css('display','none');
		}else{
			if($('.OnFucnStyle').is(':visible')){
				$('.OnFucnStyle').hide();
				$('.OnFucnStyle').css('display','none');	
			}
		}*/
		var nameId='popup-images';
		var title = '';
		title +='<span id="titlePopup"></span>';
		title += '<img class="loadingPopup" src="/resources/images/loading.gif" width="16" height="16" alt="Đang tải" class="LoadingStyle" />';
		title += '<span id="maximizePopup" class="Maximize"><img onclick="EquipStatisticChecking.showDialogFancyFull()" src="/resources/images/dialog/maximize-window.gif" width="19" height="19" alt="Phóng to" style="float: right; cursor: pointer; margin-top: -5px; padding-right: 33px;" /></span>';
		var html = $('#' + nameId).html();
		$('#' + nameId).html('');
		
		$.fancybox(html, {
			'title' : title,
			'closeBtn' : true,
			width : '1300px',
			height : '900px',
			helpers : {
				title : {
					type : 'inside'
				}
			},
			'afterShow' : function() {
				EquipStatisticChecking.pagePopup = 1;
				EquipStatisticChecking.lstImages = new Map();
				EquipStatisticChecking.getImagesForPopup();
				//EquipStatisticChecking.openMapOnImage();
				EquipStatisticChecking.scrollGetImageForPopup = 1;
				Utils.bindFormatOnTextfield("txtNumberFace", Utils._TF_NUMBER, ".fancybox-inner ");
				$('.fancybox-inner #listImage').scroll(function(){
				   if (EquipStatisticChecking.scrollGetImageForPopup == 1 && EquipStatisticChecking.isEndImages == 0 && $(this).outerHeight() > ($(this).get(0).scrollHeight - $(this).scrollTop()-2)){
					   EquipStatisticChecking.scrollGetImageForPopup = 0;
					   EquipStatisticChecking.getImagesForPopup();
				   }
				});				
			},
			'afterClose' : function() {
				$('#' + nameId).html(html);
			},
			'beforeClose': function () {
			}
		});
		return false;
	},
	onOffMap:function(i){
		if(i==0){
			$('.OffFucnStyle').hide();
			$('.OffFucnStyle').css('display','none');
			$('.OnFucnStyle').show();
			$('.OnFucnStyle').css('display','block');
			$('.fancybox-inner #divMapSection').removeClass('SmallMapSection');
			$('.fancybox-inner #divMapSection').addClass('LargeMapSection');
			$('.fancybox-inner #bigMap').css('width','488px');
			$('.fancybox-inner #bigMap').css('height','409px');
		}else{
			$('.OnFucnStyle').hide();
			$('.OnFucnStyle').css('display','none');
			$('.OffFucnStyle').show();
			$('.OffFucnStyle').css('display','block');
			$('.fancybox-inner #divMapSection').removeClass('LargeMapSection');
			$('.fancybox-inner #divMapSection').addClass('SmallMapSection');
			$('.fancybox-inner #bigMap').css('width','140px');
			$('.fancybox-inner #bigMap').css('height','120px');
		}
		EquipStatisticChecking.openMapOnImage();
	},
	showDialogFancyFull:function(mediaItemId,customerId) {
		if($('.OffFucnStyle').is(':visible')){
			$('.OffFucnStyle').hide();
			$('.OffFucnStyle').css('display','none');
		} else {
			if($('.OnFucnStyle').is(':visible')){
				$('.OnFucnStyle').hide();
				$('.OnFucnStyle').css('display','none');	
			}
		}
		
		var nameId='popup-images-full';
		var title = '';
		title +='<span id="titlePopup"></span>';
		title += '<img class="loadingPopup" src="/resources/images/loading.gif" width="16" height="16" alt="Đang tải" class="LoadingStyle" />';
		title += '<span id="minimizePopup" class="Minimize"><img src="/resources/images/dialog/minimize-window.gif" onclick="EquipStatisticChecking.showDialogFancy()" width="19" height="19" alt="Phóng to" style="float: right; cursor: pointer; margin-top: -5px; padding-right: 33px;" /></span>';
		var html = $('#' + nameId).html();
		$('#' + nameId).html('');
		
		$.fancybox(html, {
			'title' : title,
			'closeBtn' : true,
			width : '1300px',
			height : '900px',
			helpers : {
				title : {
					type : 'inside'
				}
			},
			'afterShow' : function() {
				EquipStatisticChecking.pagePopup = 1;
				EquipStatisticChecking.lstImages = new Map();
				EquipStatisticChecking.getImagesForPopup();
				//EquipStatisticChecking.openMapOnImage();		
				EquipStatisticChecking.scrollGetImageForPopup = 1;
				Utils.bindFormatOnTextfield("txtNumberFace", Utils._TF_NUMBER, ".fancybox-inner ");
				$('.fancybox-inner #listImage').scroll(function(){
				   if (EquipStatisticChecking.scrollGetImageForPopup == 1 && EquipStatisticChecking.isEndImages == 0 && $(this).outerHeight() > ($(this).get(0).scrollHeight - $(this).scrollTop()-2)){
					   EquipStatisticChecking.scrollGetImageForPopup = 0;
					   EquipStatisticChecking.getImagesForPopup();
				   }
				});
			},
			'afterClose' : function() {
				$('#' + nameId).html(html);
			},
			'beforeClose': function () {
			}
		});
		return false;
	},
	showImageNextPre:function(type){//1 là next, 0 là pre
		var flag=0;
		for(var i=0;i<EquipStatisticChecking.lstImages.keyArray.length;i++){
			if(EquipStatisticChecking.lstImages.keyArray[i]==EquipStatisticChecking.indexCurrent){
				if(type==1 && i+1<EquipStatisticChecking.lstImages.keyArray.length){//next
					scrollIntoViewPopup=1;
					EquipStatisticChecking.showBigImagePopup(EquipStatisticChecking.lstImages.keyArray[i+1]);
					flag=1;
				}else if(type==0 && i>0){
					scrollIntoViewPopup=1;
					EquipStatisticChecking.showBigImagePopup(EquipStatisticChecking.lstImages.keyArray[i-1]);
				}
				break;
			}
		}
		if(type==1 && flag==0 && EquipStatisticChecking.isEndImages==0){//nếu bấm next mà trong ds hiện tại ko có thì request lấy thêm hình
			EquipStatisticChecking.indexCurrent=null;
			scrollIntoViewPopup=1;
			EquipStatisticChecking.getImagesForPopup();
		}
	},
	rotateImage:function(t){
		var id=EquipStatisticChecking.indexCurrent;
		var temp = EquipStatisticChecking.lstImages.get(id);		
		if(temp!=null){			
			var data = new Object();
			data.id=temp.id;				
			kData = $.param(data, true);
			$('#divOverlay').show();
			$.ajax({
				type : "POST",
				url : '/equipment-checking/rotate-image', 
				data :(kData),
				dataType : "json",
				success : function(result) {
					$('#divOverlay').hide();
					if(!result.error){
						var currentMil = new Date().getTime();
						var url = result.url + '?v=' + currentMil;
						var urlThumbnail = result.thumbnailUrl + '?v=' + currentMil;
						$('#img'+EquipStatisticChecking.indexCurrent).attr('data-original', urlThumbnail);
						$('#img'+EquipStatisticChecking.indexCurrent).attr('src', urlThumbnail);
						$('#imagesOfAlbum_'+EquipStatisticChecking.indexCurrent).attr('data-original', urlThumbnail);
						$('#imagesOfAlbum_'+EquipStatisticChecking.indexCurrent).attr('src', urlThumbnail);
						$('#imagesOfAlbulmDetail_'+EquipStatisticChecking.indexCurrent).attr('data-original', urlThumbnail);
						$('#imagesOfAlbulmDetail_'+EquipStatisticChecking.indexCurrent).attr('src', urlThumbnail);
						$('#bigImages').attr('data-original', urlThumbnail);
						$('#bigImages').attr('src',urlThumbnail);
						var image = url;
						if (image != null && image != '' && image != undefined) {
							if (document.images) {
								var img = new Image();
								img.src = image;
								img.onload = function() {
									$('.fancybox-inner #bigImages').attr('src',image);
									EquipStatisticChecking.imageBigShow();
								};
							}
						}						
						EquipStatisticChecking.downloadFile(temp, true);
					}else{
						$('.fancybox-inner #errMsgPopup').html('Lỗi hệ thống').show();
						setTimeout(function(){$('.fancybox-inner #errMsgPopup').hide();},5000);
					}
				}
			});
			
		}
	},
	/**
	 * xuat file excel bien ban kiem ke
	 * @author tamvnm
	 * @since 16/01/2015
	 */
	exportExcel: function(){
		$('.errMsg').html("").hide();
		var arra = EquipStatisticChecking._selectingEquipmentStatistics.valArray;
		var lstChecked = new Array();
		if(arra == null || arra.length == 0) {
			$('#errMsg').html('Vui lòng chọn kiểm kê đã hoàn thành để xuất excel.').show();
			return;
		}		
		for(var i=0, size = arra.length; i < size; ++i) {
			lstChecked.push(arra[i].id);
		}
		var rows = $('#grid').datagrid('getRows');	
		if(rows.length==0) {
			$('#errMsg').html('Không có biên bản để xuất excel').show();
			return;
		}
//		if(lstChecked.length == 0) {
//			$('#errMsg').html('Vui lòng chọn kiểm kê đã hoàn thành để xuất excel.').show();
//			return;
//		}	
		
		
		var params = new Object();
		params.lstIdRecord = lstChecked;
		$.messager.confirm('Xác nhận','Bạn có muốn xuất file excel?',function(r){
			if(r){
				ReportUtils.exportReport('/equipment-checking/export-record-checking', params, 'errMsg');					
			}
		});		
		return false;
	},
	/**
	 * show popup import excel
	 * @author nhutnn
	 * @since 27/01/2015
	 */
	showDlgImportExcel: function(){
//		if ($("#easyuiPopupImportExcel1").length > 1) {
//			$("#easyuiPopupImportExcel1").dialog("destroy");
//		}
		$('#easyuiPopupImportExcel1').show();
		$('.ErrorMsgStyle').hide();
		$('#easyuiPopupImportExcel1').dialog({  
	        closed: false,  
	        cache: false,  
	        modal: true,
	        height: 'auto',
	        onOpen: function(){	 
	        	$('.easyui-dialog .BtnGeneralStyle').css('color','');
	        },
	        onClose: function(){
				$("#gridLiquidation").datagrid('uncheckAll');
				$("#fakefilepcDVKK1").val("");
				$("#excelFileDVKK1").val("");				
	        }
		});
	},
	showDlgImportCustomerExcel: function(){
//		if ($("#easyuiPopupImportExcel").length > 1) {
//			$("#easyuiPopupImportExcel").dialog("destroy");
//		}
		$('#easyuiPopupImportExcel').show();
		$('.ErrorMsgStyle').hide();
		$('#easyuiPopupImportExcel').dialog({  
	        closed: false,  
	        cache: false,  
	        modal: true,
	        height: 'auto',
	        onOpen: function(){	 
	        	$('.easyui-dialog .BtnGeneralStyle').css('color','');
	        },
	        onClose: function(){
				$("#gridLiquidation").datagrid('uncheckAll');
				$("#fakefilepcDVKK").val("");
				$("#excelFileDVKK").val("");				
	        }
		});
	},
	showDlgImportStaffExcel: function(){
//		if ($("#easyuiPopupImportExcelStaff").length > 1) {
//			$("#easyuiPopupImportExcelStaff").dialog("destroy");
//		}
		$('#easyuiPopupImportExcelStaff').show();
		$('.ErrorMsgStyle').hide();
		$('#easyuiPopupImportExcelStaff').dialog({  
	        closed: false,  
	        cache: false,  
	        modal: true,
	        height: 'auto',
	        onOpen: function(){	 
	        	$('.easyui-dialog .BtnGeneralStyle').css('color','');
	        },
	        onClose: function(){
//				$("#gridLiquidation").datagrid('uncheckAll');
				$("#fakefilepcDVKKStaff").val("");
				$("#excelFileDVKKStaff").val("");				
	        }
		});
	},
	showDlgImportCustomerExcelEx: function(){
		$('#easyuiPopupImportExcelEx').show();
		$('.ErrorMsgStyle').hide();
		$('#easyuiPopupImportExcelEx').dialog({  
			closed: false,  
			cache: false,  
			modal: true,
			height: 'auto',
			onOpen: function(){	 
				$('.easyui-dialog .BtnGeneralStyle').css('color','');
			},
			onClose: function(){
				$("#gridLiquidation").datagrid('uncheckAll');
				$("#fakefilepcDVKKEx").val("");
				$("#excelFileDVKKEx").val("");				
			}
		});
	},
	/**
	 * import excel DVKK
	 * @author nhutnn
	 * @since 27/01/2015
	 */
	importExcelDVKK: function(){
		$('.ErrorMsgStyle').hide();
		var params = new Object();
		params.id = $("#id").val();
		VTUtilJS.importExcelUtils(function(data) {
			$("#errExcelMsgDVKK1").html(data.message).show();		
			if(data.fileNameFail == null || data.fileNameFail == "" ){					
				setTimeout(function(){
					$('#errExcelMsgDVKK1').html("").hide();
					$('#easyuiPopupImportExcel1').dialog('close');
				}, 3000);				
			}else{
				$("#fakefilepcDVKK1").val("");
				$("#excelFileDVKK1").val("");			
			}
			var p = {
				recordId : $("#id").val(),
				shopSearchCode: $("#txtCodeShop").val().trim(),
				shopSearchName: $("#txtNameShop").val().trim()
			};
			$("#gridShop").treegrid({url: "/equipment-checking/edit-shop/search",queryParams : p});
		}, "importFrmDVKK1", "excelFileDVKK1", null, "errExcelMsgDVKK1", params);
		return false;
	},
	importExcelCustomer: function(){
		$('.ErrorMsgStyle').hide();
		var params = new Object();
		params.id = $("#id").val();
		VTUtilJS.importExcelUtils(function(data) {
			$("#errExcelMsgDVKK").html(data.message).show();		
			if(data.fileNameFail == null || data.fileNameFail == "" ){					
				setTimeout(function(){
					$('#errExcelMsgDVKK').html("").hide();
					$('#easyuiPopupImportExcel').dialog('close');
				}, 3000);				
			}else{
				$("#fakefilepcDVKK").val("");
				$("#excelFileDVKK").val("");			
			}
			EquipStatisticChecking.searchCustomer();
		}, "importFrmDVKK", "excelFileDVKK", null, "errExcelMsgDVKK", params);
		return false;
	},
	importExcelStaff: function(){
		$('.ErrorMsgStyle').hide();
		var params = new Object();
		params.id = $("#id").val();
		VTUtilJS.importExcelUtils(function(data) {
			$("#errExcelMsgDVKKStaff").html(data.message).show();		
			if(data.fileNameFail == null || data.fileNameFail == "" ){					
				setTimeout(function(){
					$('#errExcelMsgDVKKStaff').html("").hide();
					$('#easyuiPopupImportExcelStaff').dialog('close');
				}, 3000);				
			}else{
				$("#fakefilepcDVKKStaff").val("");
				$("#excelFileDVKKStaff").val("");			
			}
			EquipStatisticChecking.searchCustomer();
		}, "importFrmDVKKStaff", "excelFileDVKKStaff", null, "errExcelMsgDVKKStaff", params);
		return false;
	},
	importExcelEquip: function(){
		$('.ErrorMsgStyle').hide();
		var params = new Object();
		params.id = $("#id").val();
		VTUtilJS.importExcelUtils(function(data) {
			$("#errExcelMsgDVKKEx").html(data.message).show();		
			if(data.fileNameFail == null || data.fileNameFail == "" ){					
				setTimeout(function(){
					$('#errExcelMsgDVKKEx').html("").hide();
					$('#easyuiPopupImportExcelEx').dialog('close');
				}, 3000);				
			}else{
				$("#fakefilepcDVKKEx").val("");
				$("#excelFileDVKKEx").val("");			
			}
			EquipStatisticChecking.searchGridEquip();
		}, "importFrmDVKKEx", "excelFileDVKKEx", null, "errExcelMsgDVKKEx", params);
		return false;
	},
	/**
	 * xuat file excel DVKK
	 * @author nhutnn
	 * @since 27/01/2015
	 */
	exportExcelDVKK: function() {
		$('.ErrorMsgStyle').html("").hide();
		// var rows = $('#gridLiquidation').datagrid('getRows');	
		// if(rows.length==0) {
		// 	$('#errMsgInfo').html('Không có biên bản để xuất excel').show();
		// 	return;
		// }
		var params = {
			id: $("#id").val(),
			shopSearchCode: $("#txtCodeShop").val(),
			shopSearchName: $("#txtNameShop").val()
		};
		$.messager.confirm('Xác nhận', 'Bạn có muốn xuất file excel?', function(r) {
			if (r) {
				ReportUtils.exportReport('/equipment-checking/edit-shop-export-excel', params, 'errorMsg');
			}
		});
		return false;
	},
	/*
	* export customer tab
	*/
	exportExcelCustomer: function(){
		$('.ErrorMsgStyle').html("").hide();
		var params = new Object();
		params.id = $("#id").val();
		params.customerCode = $('#customerCode').val().trim();
		params.customerName =  $('#customerName').val().trim();
		params.statusCus =  $('#statusCus').val().trim();
		$.messager.confirm('Xác nhận','Bạn có muốn xuất file excel?',function(r){
			if(r){
				ReportUtils.exportReport('/equipment-checking/export-customer?recordId=' + $('#id').val(), params, 'errorMsg');					
			}
		});
		return false;
	},
	exportExcelStaff: function(){
		$('.ErrorMsgStyle').html("").hide();
		var params = new Object();
		params.id = $("#id").val();
		$.messager.confirm('Xác nhận','Bạn có muốn xuất file excel?',function(r){
			if(r){
				ReportUtils.exportReport('/equipment-checking/export-staff', params, 'errorMsg');					
			}
		});
		return false;
	},
	exportExcelEquip: function(){
		$('.ErrorMsgStyle').html("").hide();
		var params = new Object();
		params.equipCode = $('#equipCode').val().trim();	
		params.equipGroupName = $('#equipGroupName').val().trim();	
		params.id = $('#id').val();
		params.statusCus = $('#statusCustomer').val().trim();
		$.messager.confirm('Xác nhận','Bạn có muốn xuất file excel?',function(r){
			if(r){
				ReportUtils.exportReport('/equipment-checking/export-equip', params, 'errorMsg');					
			}
		});
		return false;
	},
	deleteCustomerRecord : function(id) {
		$(".ErrorMsgStyle").hide();
		$.messager.confirm("Xác nhận", "Bạn có muốn xóa khách hàng?", function(r) {
			if (r) {
				var params = {
					id: id
				};
				VTUtilJS.postFormJson(params, '/equipment-checking/delete-customer', function(data) {
					if(data.error) {
						$('errorMsg').html(data.errMsg).show();
					} else {
						$('#successMsg').html('Lưu dữ liệu thành công').show();
						setTimeout(function() {
							$('#successMsg').html('').hide();
						}, 3000);
						EquipStatisticChecking.searchCustomer();
					}
				});
			}
		});
	},
	showDlgImportExcelKK: function(){
//		if ($("#easyuiPopupImportExcel").length > 1) {
//			$("#easyuiPopupImportExcel").dialog("destroy");
//		}
		$('#easyuiPopupImportExcel').show();
		$('.ErrorMsgStyle').hide();
		$('#easyuiPopupImportExcel').dialog({  
	        closed: false,  
	        cache: false,  
	        modal: true,
	        height: 'auto',
	        onOpen: function(){	 
	        	$('.easyui-dialog .BtnGeneralStyle').css('color','');
	        },
	        onClose: function(){
				$("#fakefilepcBBTL").val("");
				$("#excelFileBBTL").val("");				
	        }
		});
	},
	/**
	 * import excel kiem ke thiet bi so lan
	 * @author phuongvm
	 * @since 24/04/2015
	 */
	importExcelProductSoLan: function(){
		$('.ErrorMsgStyle').hide();
		var params = new Object();
		params.id = $("#id").val();
		VTUtilJS.importExcelUtils(function(data) {
			$("#errExcelMsgBBTL").html(data.message).show();		
			if(data.fileNameFail == null || data.fileNameFail == "" ){					
				setTimeout(function(){
					$('#errExcelMsgBBTL').html("").hide();
					$('#easyuiPopupImportExcel').dialog('close');
				}, 3000);				
			}else{
				$("#fakefilepcBBTL").val("");
				$("#excelFileBBTL").val("");			
			}						
		}, "importFrmBBTL", "excelFileBBTL", null, "errExcelMsgBBTL",params);
		return false;
	},
	/**
	 * import excel kiem ke thiet bi theo tuyen
	 * @author phuongvm
	 * @since 27/04/2015
	 */
	importExcelProductTuyen: function(){
		$('.ErrorMsgStyle').hide();
		var params = new Object();
		params.id = $("#id").val();
		VTUtilJS.importExcelUtils(function(data) {
			$("#errExcelMsgBBTL").html(data.message).show();		
			if(data.fileNameFail == null || data.fileNameFail == "" ){					
				setTimeout(function(){
					$('#errExcelMsgBBTL').html("").hide();
					$('#easyuiPopupImportExcel').dialog('close');
				}, 3000);				
			}else{
				$("#fakefilepcBBTL").val("");
				$("#excelFileBBTL").val("");			
			}						
		}, "importFrmBBTL", "excelFileBBTL", null, "errExcelMsgBBTL",params);
		return false;
	},
	/**
	 * import excel kiem ke u ke theo so lan
	 * @author phuongvm
	 * @since 27/04/2015
	 */
	importExcelShelfSoLan: function(){
		$('.ErrorMsgStyle').hide();
		var params = new Object();
		params.id = $("#id").val();
		VTUtilJS.importExcelUtils(function(data) {
			$("#errExcelMsgBBTL").html(data.message).show();		
			if(data.fileNameFail == null || data.fileNameFail == "" ){					
				setTimeout(function(){
					$('#errExcelMsgBBTL').html("").hide();
					$('#easyuiPopupImportExcel').dialog('close');
				}, 3000);				
			}else{
				$("#fakefilepcBBTL").val("");
				$("#excelFileBBTL").val("");			
			}						
		}, "importFrmBBTL", "excelFileBBTL", null, "errExcelMsgBBTL",params);
		return false;
	},
	/**
	 * import excel kiem ke u ke theo tuyen
	 * @author phuongvm
	 * @since 04/05/2015
	 */
	importExcelShelfTuyen: function(){
		$('.ErrorMsgStyle').hide();
		var params = new Object();
		params.id = $("#id").val();
		VTUtilJS.importExcelUtils(function(data) {
			$("#errExcelMsgBBTL").html(data.message).show();		
			if(data.fileNameFail == null || data.fileNameFail == "" ){					
				setTimeout(function(){
					$('#errExcelMsgBBTL').html("").hide();
					$('#easyuiPopupImportExcel').dialog('close');
				}, 3000);				
			}else{
				$("#fakefilepcBBTL").val("");
				$("#excelFileBBTL").val("");			
			}						
		}, "importFrmBBTL", "excelFileBBTL", null, "errExcelMsgBBTL",params);
		return false;
	},
	/**
	 * xuat file excel chi tiet tung bien ban kiem ke
	 * @author phuongvm
	 * @since 24/04/2015
	 */
	exportExcelDetail: function(){
		$('.errMsg').html("").hide();
		var lstChecked = new Array();
		if($('#id').val()!=null && $('#id').val()!=undefined){
			lstChecked.push($('#id').val().trim());
		}
		var params = new Object();
		params.lstIdRecord = lstChecked;
		$.messager.confirm('Xác nhận','Bạn có muốn xuất file excel?',function(r){
			if(r){
				ReportUtils.exportReport('/equipment-checking/export-record-checking', params, 'errMsg');					
			}
		});		
		return false;
	},
	/**
	 * xuat file excel chi tiet tung bien ban kiem ke nhom thiet bi - so lan
	 * @author phuongvm
	 * @since 22/05/2015
	 */
	exportExcelDetailGroupSoLan: function(){
		$('.ErrorMsgStyle').html("").hide();
		var lstChecked = new Array();
		if($('#id').val()!=null && $('#id').val()!=undefined){
			lstChecked.push($('#id').val().trim());
		}
		if(EquipStatisticChecking.filter!=null){
			EquipStatisticChecking.filter.lstIdRecord = lstChecked;
		}else{
			$('#errorMsg').html("Không có dữ liệu xuất !").show();
			return false;
		}
		$.messager.confirm('Xác nhận','Bạn có muốn xuất file excel?',function(r){
			if(r){
				ReportUtils.exportReport('/equipment-checking/export-record-checking-group-solan', EquipStatisticChecking.filter, 'errMsg');					
			}
		});		
		return false;
	},
	/**
	 * xuat file excel chi tiet tung bien ban kiem ke nhom thiet bi - tuyen
	 * @author phuongvm
	 * @since 22/05/2015
	 */
	exportExcelDetailGroupTuyen: function(){
		$('.ErrorMsgStyle').html("").hide();
		var lstChecked = new Array();
		if($('#id').val()!=null && $('#id').val()!=undefined){
			lstChecked.push($('#id').val().trim());
		}
		if(EquipStatisticChecking.filter!=null){
			EquipStatisticChecking.filter.lstIdRecord = lstChecked;
		}else{
			$('#errorMsg').html("Không có dữ liệu xuất !").show();
			return false;
		}
		$.messager.confirm('Xác nhận','Bạn có muốn xuất file excel?',function(r){
			if(r){
				ReportUtils.exportReport('/equipment-checking/export-record-checking-group-tuyen', EquipStatisticChecking.filter, 'errMsg');					
			}
		});		
		return false;
	},
	/**
	 * xuat file excel chi tiet tung bien ban kiem ke u ke - so lan
	 * @author phuongvm
	 * @since 22/05/2015
	 */
	exportExcelDetailUKeSoLan: function(){
		$('.ErrorMsgStyle').html("").hide();
		var lstChecked = new Array();
		if($('#id').val()!=null && $('#id').val()!=undefined){
			lstChecked.push($('#id').val().trim());
		}
		if(EquipStatisticChecking.filter!=null){
			EquipStatisticChecking.filter.lstIdRecord = lstChecked;
		}else{
			$('#errorMsg').html("Không có dữ liệu xuất !").show();
			return false;
		}
		$.messager.confirm('Xác nhận','Bạn có muốn xuất file excel?',function(r){
			if(r){
				ReportUtils.exportReport('/equipment-checking/export-record-checking-uke-solan', EquipStatisticChecking.filter, 'errMsg');					
			}
		});		
		return false;
	},
	/**
	 * xuat file excel chi tiet tung bien ban kiem ke u ke - so lan
	 * @author phuongvm
	 * @since 22/05/2015
	 */
	exportExcelDetailUKeTuyen: function(){
		$('.ErrorMsgStyle').html("").hide();
		var lstChecked = new Array();
		if($('#id').val()!=null && $('#id').val()!=undefined){
			lstChecked.push($('#id').val().trim());
		}
		if(EquipStatisticChecking.filter!=null){
			EquipStatisticChecking.filter.lstIdRecord = lstChecked;
		}else{
			$('#errorMsg').html("Không có dữ liệu xuất !").show();
			return false;
		}
		$.messager.confirm('Xác nhận','Bạn có muốn xuất file excel?',function(r){
			if(r){
				ReportUtils.exportReport('/equipment-checking/export-record-checking-uke-tuyen', EquipStatisticChecking.filter, 'errMsg');					
			}
		});		
		return false;
	},
	searchCustomer : function() {
		$("#gridCustomer").datagrid('load', {customerCode : $('#customerCode').val().trim(), customerName : $('#customerName').val().trim(), statusCus: $('#statusCus').val().trim()});
	},
	
	/**
	 * Tim kiem thong tin kiem ke
	 * 
	 * @author hunglm16
	 * @since May 31,2015
	 * */
	searchListStatisticReturnThietBi: function () {
		$('.ErrorMsgStyle').html("").hide();
		var params = {
				id: $('#id').val(),
				shopCode: $('#shopCode').val(),
				customerCode: $('#customerCode').val(),
				staffCode: $('#staffCode').val(),
				deviceCode: $('#deviceCode').val(),
				seri: $('#seri').val(),
				status: $('#status').val(),
				fromDate: $('#fromDate').val(),
				toDate: $('#toDate').val(),
			};
		EquipStatisticChecking._paramListStatistic = params;
		$('#grid').datagrid('load', params);
	},
	// update author nhutnn, 05/08/2015
	bindEventInput: function(){
       $('td input').each(function(i,e){
             $(this).attr('id','inputText'+i).attr('maxlength','22');
             Utils.bindFormatOnTextfield('inputText'+i, Utils._TF_NUMBER);
             Utils.formatCurrencyFor('inputText'+i);
       });
       $('td input').keypress(function() {
       	Utils.onchangeFormatCurrencyKeyPress(this, 17);
       });
	},
	/**
	 * Show popup tim kiem thiet bi
	 * @author phuongvm
	 * @since 07/07/2015
	 */
	showPopupSearchEquipment: function(isSearch){
		$(".ErrorMsgStyle").hide();
		$('#easyuiPopupSearchEquipment').show();
		$('#easyuiPopupSearchEquipment').dialog({  
	        closed: false,  
	        cache: false,  
	        modal: true,
	        width : 1000,
	        height : 'auto',
	        onOpen: function(){
	        	//setDateTimePicker('yearManufacturing');1
	        	Utils.bindComboboxEquipGroupEasyUICodeName('groupEquipment', null, 193);
	        	Utils.bindComboboxEquipGroupEasyUICodeName('providerId', null, 193);
	        	var year = new Date();
	        	year = year.getFullYear(); 
	        	var op = '<option value="" selected="selected">Chọn</option><option value="'+year+'">'+year+'</option>';
	        	for(var i=1; i<100; i++){
	        		op+='<option value="'+(year-i)+'">'+(year-i)+'</option>';
	        	}
	        	$('#yearManufacturing').html(op).change();
	        	$('#equipmentCode, #seriNumber, #typeEquipment, #customerCode, #customerName, #customerAddress').bind('keyup',function(event){
	        		if(event.keyCode == keyCodes.ENTER){
	        			$('#btnSeachEquipmentDlg').click(); 
	        		}
	        	});
	        	$('#yearManufacturing').bind('keyup',function(event){
	        		if(event.keyCode == keyCodes.ENTER){
	        			$('#btnSeachEquipmentDlg').click(); 
	        		}
	        	});
	        	$('#groupEquipment, #providerId').next().find(".combo-text.validatebox-text").bind('keyup',function(event){
	        		if(event.keyCode == keyCodes.ENTER){
	        			$('#btnSeachEquipmentDlg').click(); 
	        		}
	        	});
	        	EquipmentManagerLiquidation.getEquipCatalog();
	        	EquipmentManagerLiquidation.getEquipGroup(null);
	        	var params = new Object();
	        	var groupId = $('#groupEquipment').combobox('getValue');
	        	if (groupId == -1 || groupId == "") {
	        		groupId = "";
	        	}
	        	var providerId = $('#providerId').combobox('getValue');
	        	if (providerId == -1 || providerId == "") {
	        		providerId = "";
	        	}
				params = {
						id: $("#id").val(),
						equipCode: $('#equipmentCode').val().trim(),
						seriNumber: $('#seriNumber').val().trim(),
						categoryId: $('#typeEquipment').val().trim(),
						// groupId: $('#groupEquipment').val().trim(),
						groupId: groupId,
						// providerId: $('#providerId').val().trim(),
						providerId: providerId,
						yearManufacture: $('#yearManufacturing').val().trim(),
//						stockCode: $('#stockCode').val().trim(),
						customerCode: $('#customerCode').val().trim(),
//						customerName: $('#customerName').val().trim(),
//						shopCode: $('#shopCode').val().trim(),
//						customerAddress: $('#customerAddress').val().trim(),
						status: activeType.RUNNING,
//						stockType: 1, //Don vi
//						tradeStatus: 0,
//						usageStatus: usageStatus.SHOWING_WAREHOUSE,//Dang o kho
						flagPagination: true
					};
				if($("#idRecordHidden").val() != "" && EquipmentManagerLiquidation._lstEquipInRecord != null){
					params.lstEquipDelete = "";
					for(var i=0; i<EquipmentManagerLiquidation._lstEquipInRecord.length; i++){
						params.lstEquipDelete += EquipmentManagerLiquidation._lstEquipInRecord[i]+',';
					}					
				}
				if(EquipmentManagerLiquidation._lstEquipInsert != null){
					params.lstEquipAdd = "";
					for(var i=0; i<EquipmentManagerLiquidation._lstEquipInsert.length; i++){
						params.lstEquipAdd += EquipmentManagerLiquidation._lstEquipInsert[i]+',';
					}					
				}
				$('#equipmentGridDialog').datagrid({
//					url : '/equipment-manage-liquidation/search-equipment-liquidation',
					url : '/commons/search-equip-by-shop-root-in-stock-ex',
					autoRowHeight : true,
					rownumbers : true, 
					//fitColumns:true,
					pagination:true,
					scrollbarSize:0,
					pageSize:10,
					pageList: [10],
					width: $('#equipmentGridDialogContainer').width(),
					height: 'auto',
					autoWidth: true,	
					queryParams: params,
					frozenColumns:[[
						{field: 'check',checkbox:true, width: 50, sortable:false,resizable:false, align: 'left'},
						{field: 'shopCode', title:'Đơn vị', width: 70, sortable:false, resizable:false, align: 'left', formatter: function(value, row, index) {
							return Utils.XSSEncode(value);
						}},
//						{field: 'stock',title:'Kho', width: 120, sortable:false, resizable:false, align: 'left', formatter: function(value, row, index) {
//							var html = "";
//							if(row.stockCode != null && row.stockName!= null){
//								html = row.stockCode+' - '+row.stockName; 
//							}
//							return Utils.XSSEncode(html);
//						}},	
						{field: 'shortCode', title:'Mã KH', width: 70, sortable:false, resizable:false, align: 'left', formatter: function(value, row, index) {
							return Utils.XSSEncode(value);
						}},
						{field: 'customerName', title:'Tên KH', width: 120, sortable:false, resizable:false, align: 'left', formatter: function(value, row, index) {
							return Utils.XSSEncode(value);
						}},	
						{field: 'address', title:'Địa chỉ', width: 200, sortable:false, resizable:false, align: 'left', formatter: function(value, row, index) {
							return Utils.XSSEncode(value);
						}},
						{field: 'equipmentCode', title:'Mã thiết bị', width: 120, sortable:false, resizable:false, align: 'left', formatter: function(value, row, index) {
							return Utils.XSSEncode(value);
						}},
					]],
					columns:[[
//						{field: 'seriNumber', title:'Số serial', width: 100, sortable:false, resizable:false, align: 'left', formatter: function(value, row, index) {
//							return Utils.XSSEncode(value);
//						}},
//						{field: 'healthStatus', title:'Tình trạng thiết bị', width: 100, sortable:false, resizable:false, align: 'left', formatter: function(value, row, index) {
//							return Utils.XSSEncode(value);
//						}},
					    {field: 'groupEquipment', title:'Nhóm thiết bị', width: 200, sortable:false, resizable:false, align: 'left', formatter: function(value, row, index) {
					    	var html="";
							if(row.groupEquipmentCode != null && row.groupEquipmentName!= null){
								html = row.groupEquipmentCode+' - '+row.groupEquipmentName; 
							}
							return Utils.XSSEncode(html);
						}},
						{field: 'typeEquipment', title:'Loại thiết bị', width: 120, sortable:false, resizable:false, align: 'left', formatter: function(value, row, index) {
							return Utils.XSSEncode(value);
						}},
//					    {field: 'capacity',title:'Dung tích (lít)', width: 80, sortable:false, resizable:false, align: 'center', formatter: function(value, row, index) {
//							return Utils.XSSEncode(value);
//						}},
//					    {field: 'equipmentBrand',title:'Hiệu', width: 80, sortable:false, resizable:false, align: 'left', formatter: function(value, row, index) {
//							return Utils.XSSEncode(value);
//						}},
//					    {field: 'equipmentProvider',title:'NCC',width: 80, sortable:false, resizable:false, align: 'left', formatter: function(value, row, index) {
//							return Utils.XSSEncode(value);
//						}},
//					    {field: 'yearManufacture', title:'Năm sản xuất', width: 80, sortable:false, resizable:false, align: 'right', formatter: function(value, row, index) {
//							return Utils.XSSEncode(value);
//						}},
					]],
					onBeforeLoad:function(param){											 
					},
					onLoadSuccess :function(data){   			 
						if (data == null || data.total == 0) {
							//$("#errMsgInfo").html("Không tìm thấy danh sách biên bản giao nhận thiết bị!").show();
						}else{
							$('.datagrid-header-rownumber').html('STT');		    	
						}	    
						$('#equipmentCode').focus();
						 updateRownumWidthForDataGrid('.easyui-dialog');
						var rows = $('#equipmentGridDialog').datagrid('getRows');
				    	$('.datagrid-header-check input').removeAttr('checked');
				    	for(var i = 0; i < rows.length; i++) {
				    		if(EquipStatisticChecking._lstEquipId.get(rows[i].equipmentId)!=null) {
				    			$('#equipmentGridDialog').datagrid('checkRow', i);
				    		}
				    	}
				    	setTimeout(function() {
			    			CommonSearchEasyUI.fitEasyDialog('easyuiPopupSearchEquipment');
			    		}, 1000);
					},
					onCheck:function(index,row){
						EquipStatisticChecking._lstEquipId.put(row.equipmentId,row);  
						
				    },
				    onUncheck:function(index,row){
				    	EquipStatisticChecking._lstEquipId.remove(row.equipmentId);
				    },
				    onCheckAll:function(rows){
					 	for(var i = 0;i<rows.length;i++){
				    		var row = rows[i];
				    		EquipStatisticChecking._lstEquipId.put(row.equipmentId,row);
				    	}
				    },
				    onUncheckAll:function(rows){
				    	for(var i = 0;i<rows.length;i++){
				    		var row = rows[i];
				    		EquipStatisticChecking._lstEquipId.remove(row.equipmentId);	
				    	}
				    }
				});
	        },
			onClose: function(){
				$('#easyuiPopupSearchEquipment').hide();
				$('#equipmentCode').val('').change();
				$('#seriNumber').val('').change();
				$('#typeEquipment').html('<option value="" selected="selected">Tất cả</option>');
				// $('#groupEquipment').html('<option value="" selected="selected">Tất cả</option>').change();
				// $('#providerId').html('<option value="" selected="selected">Tất cả</option>');
				$('#yearManufacturing').val('').change();
				$('#equipmentGridDialog').datagrid('loadData',[]);
				EquipStatisticChecking._lstEquipId = new Map();
			}
		});
	},
	/**
	 * Xu ly trên input loai thiet bi
	 * @author nhutnn
	 * @since 19/12/2014
	 */
	onChangeTypeEquip: function(cb){
		var typeEquip = $(cb).val().trim();
		EquipStatisticChecking.getEquipGroup(typeEquip);
	},
	/**
	 * Lay nhom thiet bi
	 * @author nhutnn
	 * @since 12/01/2015
	 */
	getEquipGroup: function(typeEquip){
		var params = new Object();
		if(typeEquip != undefined && typeEquip != null && typeEquip != ''){
			params.id = typeEquip;
		}
		// $('#groupEquipment').html('<option value="" selected="selected">Tất cả</option>');
    	Utils.getJSONDataByAjaxNotOverlay(params, '/equipment-manage-liquidation/get-equipment-group', function(result){	
		// $.getJSON('/equipment-manage-liquidation/get-equipment-catalog', function(result) {				
			// if(result.equipGroups != undefined && result.equipGroups != null && result.equipGroups.length > 0) {					
			// 	for(var i=0; i < result.equipGroups.length; i++){
			// 		$('#groupEquipment').append('<option value="'+ result.equipGroups[i].id +'">'+ Utils.XSSEncode(result.equipGroups[i].code+' - '+result.equipGroups[i].name) +'</option>');  
			// 	}
			// } 
			// $('#groupEquipment').change();			
			//tamvnm: doi thanh autoCompleteCombobox
			if(result.equipGroups != undefined && result.equipGroups != null && result.equipGroups.length > 0) {	
				for (var i = 0, isize = result.equipGroups.length; i < isize; i++) {
					result.equipGroups[i].searchText = unicodeToEnglish(result.equipGroups[i].code + " " + result.equipGroups[i].name);
	 				result.equipGroups[i].searchText = result.equipGroups[i].searchText.toUpperCase();
				}				
				var obj = {
				id: -1,
				code: "Tất cả",
				name: "Tất cả",
				codeName: "Tất cả",
				searchText: unicodeToEnglish("Tất cả").toUpperCase()
				};
				result.equipGroups.splice(0, 0, obj);
				$('#groupEquipment').combobox("loadData", result.equipGroups);
		 		$('#groupEquipment').combobox("setValue", -1);
			} 				
		},null,null);
	},
	/**
	 * Lay loai, ncc thiet bi
	 * @author phuongvm
	 * @since 27/05/2015
	 */
	getEquipCatalog: function(){
		$('#typeEquipment').html('<option value="" selected="selected">Tất cả</option>');
    	$('#provider').html('<option value=""  selected="selected">Tất cả</option>');
    	
		$.getJSON('/equipment-manage-delivery/get-equipment-catalog', function(result) {				
			if(result.equipsCategory != undefined && result.equipsCategory != null && result.equipsCategory.length > 0) {					
				for(var i=0; i < result.equipsCategory.length; i++){
					$('#typeEquipment').append('<option value="'+ result.equipsCategory[i].id +'">'+Utils.XSSEncode(result.equipsCategory[i].code)+' - '+Utils.XSSEncode(result.equipsCategory[i].name) +'</option>');  
				}
			} 
			$('#typeEquipment').change();
			//tamvnm: doi thanh autoCompleteCombobox
			if(result.equipProviders != undefined && result.equipProviders != null && result.equipProviders.length > 0) {					
				for (var i = 0, isize = result.equipProviders.length; i < isize; i++) {
					result.equipProviders[i].searchText = unicodeToEnglish(result.equipProviders[i].code + " " + result.equipProviders[i].name);
 					result.equipProviders[i].searchText = result.equipProviders[i].searchText.toUpperCase();
				}
				var obj = {
					id: -1,
					code: "Tất cả",
					name: "Tất cả",
					codeName: "Tất cả",
					searchText: unicodeToEnglish("Tất cả").toUpperCase()
				};
				result.equipProviders.splice(0, 0, obj);
				$('#providerId').combobox("loadData", result.equipProviders);
		 		$('#providerId').combobox("setValue", -1);
			}						
		});
	},
	/**
	 * Tim kiem thiet bi
	 * @author nhutnn
	 * @since 12/12/2014
	 * 
	 * @author hunglm16
	 * @sine JUNE 13,2015
	 * @description Ra soat noi dung phan quyen CMS
	 */
	searchEquipment: function(){
		$(".ErrorMsgStyle").hide();
		EquipStatisticChecking._lstEquipId = new Map();

		//tamvnm: thay doi thanh autoCompleteCombobox: -1 la truong hop tim group ko co trong BD
		var groupId = "";
		if ($('#groupEquipment').combobox('getValue') == "") {
			$('#groupEquipment').combobox('setValue', -1);
			groupId = "";
		} else if ($('#groupEquipment').combobox('getValue') == -1) {
			groupId = "";
		} else {
			if (Utils.getMessageOfInvaildNumberNew($('#groupEquipment').combobox("getValue")).length == 0) {
				groupId = $('#groupEquipment').combobox("getValue");
			} else {
				groupId = -1;
			}
		}
		var providerId = "";
		if ($('#providerId').combobox('getValue') == "") {
			$('#providerId').combobox('setValue', -1);
			providerId = "";
		} else if ($('#providerId').combobox('getValue') == -1) {
			providerId = "";
		} else {
			if (Utils.getMessageOfInvaildNumberNew($('#providerId').combobox("getValue")).length == 0) {
				providerId = $('#providerId').combobox("getValue");
			} else {
				providerId = -1;
			}
		}

		var params = {
				id: $("#id").val(),
				equipCode: $('#equipmentCode').val().trim(),
				seriNumber: $('#seriNumber').val().trim(),
				categoryId: $('#typeEquipment').val().trim(),
				// groupId: $('#groupEquipment').val().trim(),
				// providerId: $('#providerId').val().trim(),
				groupId: groupId,
				providerId: providerId,
				yearManufacture: $('#yearManufacturing').val().trim(),
//				stockCode: $('#stockCode').val().trim(),
				customerCode: $('#customerCode').val().trim(),
//				customerName: $('#customerName').val().trim(),
//				shopCode: $('#shopCode').val().trim(),
//				customerAddress: $('#customerAddress').val().trim(),
//				status: activeType.RUNNING,
//				tradeStatus: 0,
//				stockType: 1, //Don vi
//				usageStatus: usageStatus.SHOWING_WAREHOUSE,//Dang o kho
				flagPagination: true
		};
		$('#equipmentGridDialog').datagrid('load', params);
	},
	addEquipment: function() {
		$(".ErrorMsgStyle").hide();
		if(EquipStatisticChecking._lstEquipId.keyArray.length == 0) {
			$('#errMsgEquipmentDlg').html('Bạn chưa chọn thiết bị nào. Vui lòng check chọn thiết bị').show();
			return;
		}
		$.messager.confirm("Xác nhận", "Bạn có muốn thêm thiết bị?", function(r) {
			if (r) {
				var params = {
						id: $("#id").val(),
						listId : EquipStatisticChecking._lstEquipId.keyArray
				};
				VTUtilJS.postFormJson(params, '/equipment-checking/edit-equip/save', function(data) {
					if(data.error) {
						$('#errMsgEquipmentDlg').html(data.errMsg).show();
					} else {
						$('#easyuiPopupSearchEquipment').dialog('close');
						$('#successMsg').html('Lưu dữ liệu thành công').show();
						setTimeout(function() {
							$('#successMsg').html('').hide();
						}, 3000);
						$('#gridEquipStatistic').datagrid('reload');
					}
				});
			}
		});
	},
	/*
	 * Xu ly khi nhan radio dat, khong dat
	 * @author phuongvm
	 * @since 16/09/2015
	 */
	changeCheckDisplay: function(t) {
		$('.fancybox-inner #chkInspect').attr("checked", "checked");
		$('.fancybox-inner #chkInspect').attr("disabled", "disabled");
	},
	/* update Luu them mot so thong tin
	 * @author phuongvm
	 * @since update 16/09/2015
	 */
	updateResult: function() {
		$(".ErrorMsgStyle").hide();
		var id = $('.fancybox-inner #divDetail').attr('value');	
		var resultImg = null;
		if (id != undefined && id != null && id != "")  {			
			var data = new Object();
			if ($('.fancybox-inner #chkResult').is(':checked')) {
				resultImg = 1;
			} else if ($('.fancybox-inner #chkNotResult').is(':checked')) {
				resultImg = 0;
			} else {
				resultImg =  null;
			}
			data.resultImg = resultImg; 
			var inspect = null;
			if ($('.fancybox-inner #chkInspect').is(':checked')) {
				if (resultImg == null) {
					$(".fancybox-inner #errMsgPopup").html("Vui lòng nhập kết quả kiểm tra trưng bày!").show();
					$(".fancybox-inner #errMsgPopup").focus();
					return false;
				} else {
					inspect = 1;
				}
			}
			data.inspect = inspect;
			if ($('.fancybox-inner #txtNumberFace').val() != undefined && $('.fancybox-inner #txtNumberFace').val() != null) {
				if($('.fancybox-inner #txtNumberFace').val() != "" && $('.fancybox-inner #txtNumberFace').val() <= 0) {
					$(".fancybox-inner #errMsgPopup").html("Số mặt phải là số nguyên dương!").show();
					$(".fancybox-inner #errMsgPopup").focus();
					return false;
				}
				data.numberProduct = $('.fancybox-inner #txtNumberFace').val().trim();
			}
			if($('.fancybox-inner #txtPOSM').val() != undefined && $('.fancybox-inner #txtPOSM').val() != null) {
				data.strPOSM = $('.fancybox-inner #txtPOSM').val().trim();
			}
			if($('.fancybox-inner #txtNoted').val() != undefined && $('.fancybox-inner #txtNoted').val() != null) {
				data.strNoted = $('.fancybox-inner #txtNoted').val().trim();
			}
			var params = new Object();
			params.id = id;
			if (data.resultImg != null) {
				params.resultImg = data.resultImg;
			}
			params.numberProduct = data.numberProduct;
			params.strPOSM = data.strPOSM;
			params.strNoted = data.strNoted;
			params.isInspeck = data.inspect;
			Utils.addOrSaveData(params, '/equipment-checking/updateResult', null, 'errMsgPopup', function(data) {
				if (data.error) {
					$(".fancybox-inner #errMsgPopup").html(data.errMsg).show();
				} else {
					var temp = EquipStatisticChecking.lstImages.get(id);
					if (params.resultImg != -1) {
						temp.result = params.resultImg;
					}
					if (params.isInspeck != null) {
						temp.isInSpected = params.isInspeck;
					}
					if (params.numberProduct != null) {
						temp.numberProduct = params.numberProduct;
					} 
					if (params.strPOSM != null) {
						temp.POSM = params.strPOSM;
					} 
					if (params.strNoted != null) {
						temp.displayNote = params.strNoted;
					}  
					$(".fancybox-inner #successMsgPopup").html("Lưu dữ liệu thành công").show();					
					setTimeout(function() {
						$(".fancybox-inner #successMsgPopup").html("").hide();
						EquipStatisticChecking.showBigImagePopup(id);
					}, 3000);
				}
				
			}, null, ".fancybox-inner ", null, null, null , null);
		}
	},
	searchGridEquip: function(){
		var params=new Object(); 
		params.equipCode = $('#equipCode').val().trim();	
		params.equipGroupName = $('#equipGroupName').val().trim();
		params.id = $('#id').val();
		params.statusCus = $('#statusCustomer').val().trim();
		$('#gridEquipStatistic').datagrid('load',params);
	},
	
	/**
	 * hien popup so lan kiem ke
	 * @author trietptm
	 * @since Mar 10, 2016
	 */
	showDlgNumStatistic: function(equipId) {
		var params = EquipStatisticChecking._paramListStatistic;
		params.equipId = equipId;
		$('#easyuiPopupNumStatistic').dialog({
	        closed: false,  
	        cache: false,  
	        modal: true,
	        width: 'auto',
	        height: 'auto',
	        onOpen: function () {
				$('#gridNumStatistic').datagrid({
					url: '/equipment-checking/getListStatisticTime',
					autoRowHeight : true,
					queryParams: params,
					rownumbers : true, 
					singleSelect: true,
					pagination: true,
					pageSize: 10,
					pageList: [10, 20, 50],
					fitColumns: true,
					scrollbarSize: 0,
					width: $('#gridNumStatisticDiv').width(),
					autoWidth: true,
					columns: [[
					    {field: 'statisticTime',title: 'Lần kiểm kê', width: 80, sortable: false, resizable: false, align: 'center'},
						{field: 'statisticDate',title: 'Ngày', width: 80, sortable: false, resizable: false, align: 'center', formatter: function(value, row, index){
							return Utils.XSSEncode(value);
						}},
						{field: 'status', title: 'Trạng thái', width: 80, sortable: false, resizable: false, align: 'left', formatter: function(value, row, index) {
							return InventoryStatus.parseValue(row.status);
						}}
					]],
					onBeforeLoad: function(param) {
					},
					onLoadSuccess: function(data) {
				    	$('.easyui-dialog .datagrid-header-rownumber').html('STT');
				    	setTimeout(function(){
			    			CommonSearchEasyUI.fitEasyDialog('easyuiPopupNumStatistic');
			    		},1000);
					}
				});
	        }
		});
	},
};
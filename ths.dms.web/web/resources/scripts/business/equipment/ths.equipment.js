/*
 * JavaScript file created by Rockstarapps Concatenation
*/

/*
 * START OF FILE - /web/web/resources/scripts/business/equipment/vnm.equip-lost-record.js
 */
/**
 * JS thuc hien nghiep vu Danh Sach Thiet Bi
 * 
 * Cap nhat ghi chu: vuongmq; 04/05/2015;  JS thuc hien nghiep vu Ql bao mat
 * @author hunglm16
 * @since December 17,2014
 * */
var EquipLostRecord = {
		/**Khai bao thuoc tinh Bien Toan Cuc
		 * 
		 * @author hunglm16
		 * @since December 17,214
		 * */
		_mapEquip: null, // vuongmq; 06/05/2015; _mapEquip: khong xai nua
		_lstEquip: null,
		_listEquipLostRecordDg: null,
		_mapData: null,
		_params: null,
		_equipChange: null,
		_stockIdChange: null,
		_equipIdChange: null,
		_tracingPlace: null,
		_tracingResult: null,
		_recordStatus: null,
		_deliveryStatus: null,
		_eqLostRecId: null,
		_eqLostRec: null,
		_mapEquipByLostRec: null,
		_isLevelShopRoot: null,
		_eqLostMobiRecId: null,
		_isFlagMobile: null,
		_contractNumber: '',
		_sysDateStr: '',
		_arrFileDelete: null,
		_countFile: 0,
		_rowsDetail: null,
		// vuongmq 05/05/2015
		_isFlaglostRecord: null,
		_phoneShop: null,
		_addressShop: null,
		_msgGiaTriConlai: null,
		_TAOMOI: 0,
		_DUYET: 1,
		_FLAG_CHANGE_PHE_DUYET: null, // vuongmq; 15/05/2015; trang chinh sua phe duyet bao mat != null

		showDialogUploadImages : function() {
			$('#popup-images-upload').dialog({
				title : 'Upload Images',
				width : 750,
				height : 400,
				closed : false,
				cache : false,
				modal : true,
				onOpen : function() {
					$('.ErrorMsgStyle').html('').hide();
					$('.SuccessMsgStyle').html('').hide();
					UploadUtil.clearAllAddUploadedFile(); /** open lai popup thi remove het tat ca cac file*/
//					$("#fileQueue").html('');
//					$('#programId').focus();
//					$('#ndpId').html('').change();
//					$("#ndpId").removeAttr('disabled');
//					$('#errMsgUpload').hide();
//					$("#programId").val("-1").change();
//					$(window).bind('keyup',function(event){
//						if(event.keyCode == 13){
//							$('#btnUpload').click();
//						}
//					});
				},
				onClose : function() {
				}
			});
		},
		uploadImageFile: function() {
			$('.ErrorMsgStyle').html('').hide();
			$('.SuccessMsgStyle').html('').hide();
			var errView = 'errMsgUpload';
			EquipLostRecord.updateAdditionalDataForUpload();
			UploadUtil.startUpload(errView);
		},
		updateAdditionalDataForUpload: function() {
			var programId = $('#programId').val();
			var ndpId = $('#ndpId').val();
			
			var data = JSONUtil.getSimpleObject2({
				programId: programId,
				ndpId: ndpId,
			});
			UploadUtil.updateAdditionalDataForUpload(data);
		},
		/**
		* Khoi tao tham so cho Grid Equipment
		* 
		* @author hunglm16
		* @since January 06,2014
		* */
		getQueryParamsDetail: function () {
			var params = {};
			if (EquipLostRecord._stockIdChange != null && Number(EquipLostRecord._stockIdChange) > 0) {
				params.stockId = Number(EquipLostRecord._stockIdChange);
			}
			if (EquipLostRecord._equipIdChange != null && Number(EquipLostRecord._equipIdChange) > 0) {
				params.equipId = Number(EquipLostRecord._equipIdChange);
			}
			params.flagPagination = false;
			return params;
		},
		
		/**
		* Luu vet cac tham so tim kiem
		* 
		* @author hunglm16
		* @since December 06, 2015
		* */
		createParamanterBySearch: function(taoMoi) {
			EquipLostRecord._params = {
					code: $('#txtCode').val(),
					serial: $('#txtSerial').val(),
					fromDateStr: $('#txtFromDate').val(),
					toDateStr: $('#txtToDate').val(),
					equipCode: $('#txtEquipCode').val(),
					//shortCode: $('#txtShortCode').val(),
					//customerName: $('#txtCustomerName').val(), 
					//address: $('#txtAddress').val(),
					customerSearch: $('#customerSearch').val(),
					//stockType: 3,
					stockType: Number($('#cboStockType').val()), // vuongmq; 25/04/2015
					recordStatus : Number($('#ddlStatusEquipRecord').val()),
					// deliveryStatus : Number($('#ddlDeliveryStatus').val()), /*** 15/05/2015; tim kiem tao moi co trang thai giao nhan */
			};
			 /*** 15/05/2015; tim kiem tao moi ben chuc năng QL bao mat co trang thai giao nhan; nguoc lai phe duyet bao mat thi khong co */
			if (taoMoi != undefined && taoMoi != null && taoMoi == EquipLostRecord._TAOMOI) {
				EquipLostRecord._params.deliveryStatus = Number($('#ddlDeliveryStatus').val());
			}
			if (taoMoi != undefined && taoMoi != null && taoMoi == EquipLostRecord._DUYET) {
				 EquipLostRecord._params.flagRecordStatus = EquipLostRecord._DUYET;
			}
			var multiShop = $("#txtShop").data("kendoMultiSelect");
			var dataShop = null;
			if (multiShop != null) {
				dataShop = multiShop.dataItems();
			}
			var arrShopId = [];
			if (dataShop != null) {
				for (var i=0, size = dataShop.length; i < size; i++) {
					arrShopId.push(dataShop[i].id);
				}
			}
			EquipLostRecord._params.arrShopTxt = arrShopId.join(',');
		},
		
		/**
		 * Tim kiem danh sach bien ban bao mat
		 * 
		 * @author hunglm16
		 * @since December 06, 2015
		 * */
		searchListEquipLostRecord: function (taoMoi) {
			$('.SuccessMsgStyle').html('').hide();
			$('.ErrorMsgStyle').html('').hide();
			var msg = '';
			var err = '';
			var multiShop = $("#txtShop").data("kendoMultiSelect");
			var dataShop = null;
			if (multiShop != null) {
				dataShop = multiShop.dataItems();
			}
			if (msg.length == 0 && dataShop == null || (dataShop != null && dataShop.length <= 0)) {
				msg = 'Vui lòng nhập Đơn vị';
				err = '#txtShop';
			}
			if (msg.length == 0) {
				msg = Utils.getMessageOfInvalidFormatDate("txtFromDate", "Từ ngày");
				err = "#txtFromDate";
			}
			if (msg.length == 0) {
				msg = Utils.getMessageOfInvalidFormatDate("txtToDate", "Đến ngày");
				err = "#txtToDate";
			}
			var fDate = $('#txtFromDate').val();
			var tDate = $('#txtToDate').val();
			if (msg.length == 0 && fDate.length > 0 && tDate.length > 0 && !Utils.compareDate(fDate, tDate)) {
				msg = 'Từ ngày phải là ngày trước hoặc cùng ngày với đến ngày';
				err = '#txtFromDate';
			}
			if (msg.length > 0) {
				$('#errMsg').html(msg).show();
				$(err).focus();
				setTimeout(function(){$('#errMsg').hide();},1500);
				return false;
			}
			EquipLostRecord.createParamanterBySearch(taoMoi);
			
			$('#lostRecordDg').datagrid("load", EquipLostRecord._params);
		},
		
		/**
		 * Xuat Excel Theo Tim kiem bien ban bao mat
		 * 
		 * @author hunglm16
		 * @since January 01,214
		 * */
		exportBySearchEquipLostRecord: function() {
			$('.SuccessMsgStyle').html('').hide();
			$('.ErrorMsgStyle').html('').hide();
			var msg = '';
			if(EquipLostRecord._params != null){
				var rows = $('#lostRecordDg').datagrid("getData");
				if(rows == null || rows.length==0){
					msg = "Không có dữ liệu xuất Excel";
				}
			}else{
				msg = "Không có dữ liệu xuất Excel";
			}
			if(msg.length > 0){
				$('#errMsg').html(msg).show();
				return false;
			}
			var msg = 'Bạn có muốn xuất Excel?';
			$.messager.confirm('Xác nhận', msg, function(r){
				if (r){
					ReportUtils.exportReport('/equipLostRecord/exportBySearchEquipLostRecordVO', EquipLostRecord._params, 'errMsg');
				}
			});
		},
		/**
		 * Thay doi cap nhat gia tri Bien Ban Bao Mat theo filter
		 * Ben MH QUan ly bao mat
		 * @author hunglm16
		 * @since January 06, 2015
		 * */
		changeByListEquipLostRecord: function () {
			$('.SuccessMsgStyle').html('').hide();
			$('.ErrorMsgStyle').html('').hide();
			var msg = '';
			var recordStatus = Number($('#ddlStatusEquipRecordImport').val());
			var deliveryStatus = Number($('#ddlDeliveryStatusImport').val());
			var arrEquipLostRecord = [];
			
			var rows = $('#lostRecordDg').datagrid('getChecked');
			if (rows == undefined || rows == null || rows.length == 0) {
				msg = "Chưa có Biên bản báo mất nào được chọn";
			}
			if (msg.length == 0) {
				if ((isNaN(recordStatus) || recordStatus< 0) && (isNaN(deliveryStatus) || deliveryStatus < 0)) {
					msg = "Phải có ít nhất một trong hai trạng thái phải được chọn";
				}
			}
			if (msg.length > 0) {
				$('#errMsg').html(msg).show();
				return false;
			}
			
			for (var i= 0, size = rows.length; i< size; i++) {
				arrEquipLostRecord.push(rows[i].id);
			}
			
			//Xu ly nghiep vu
			var params = {
					recordStatus: recordStatus,
					deliveryStatus: deliveryStatus,
					arrEquipLostRecordTxt: arrEquipLostRecord.join(',').trim()
			};
			
			Utils.addOrSaveData(params, "/equipLostRecord/changeByListEquipLoadRecord", null, 'errMsg', function(data) {
				if (data.rows != null && data.rows.length > 0) {
					VCommonJS.showDialogList({
						dialogInfo: {
							title: 'Danh sách lỗi'
						},
						data: data.rows,
						columns : [[
							{field:'code', title:'Mã biên bản', align:'left', width: 110, sortable:false, resizable:false},
							{field:'recordStatus', title:'Trạng thái biên bản', align:'center', width: 110, sortable:false, resizable:false, formatter: function(value, row, index){
								if (Number($('#ddlStatusEquipRecordImport').val()) > -1) {
									return '<a href="javascript:void(0)"><img src="/resources/images/icon-error.png" width="18" heigh="18" /></a>';
								}
								return '';
							}},
							{field:'deliveryStatus', title:'Trạng thái giao nhận', align:'center', width: 110, sortable:false, resizable:false, formatter: function(value, row, index){
								if (Number($('#ddlDeliveryStatusImport').val()) > -1) {
									return '<a href="javascript:void(0)"><img src="/resources/images/icon-error.png" width="18" heigh="18" /></a>';
								}
								return '';
							}}
						]]
					});
				} else {
					//$("#successMsg").html("Lưu dữ liệu thành công").show();
					var htmlView = 'Lưu dữ liệu thành công.';
					if (data.errMsgMail != undefined && data.errMsgMail != '') {
						htmlView += '<span style="color:#f00"> Chưa gửi mail, ' + data.errMsgMail + '</span>';
					}
					$("#successMsg").html(htmlView).show();
				}
				setTimeout(function() {
					$('.SuccessMsgStyle').html("").hide();
					$('#btnSearch').click(); 
				 }, 1500);
			}, null, null, null, null, null, true);
			return false;
		},

		/**
		 * Cap nhat gia tri bien ban de phe duyet
		 * @author vuongmq
		 * @since 15/05/2015
		 * */
		changeByListEquipLostRecordApprove: function (pheDuyetChange) {
			$('.SuccessMsgStyle').html('').hide();
			$('.ErrorMsgStyle').html('').hide();
			var msg = '';
			var recordStatus = '';
			var arrEquipLostRecord = [];
			if (pheDuyetChange == undefined || pheDuyetChange == null) {
				// truong hop cap nhat o danh sach chinh
				recordStatus = Number($('#ddlStatusEquipRecordImport').val());
				var rows = $('#lostRecordDg').datagrid('getChecked');
				if (rows == undefined || rows == null || rows.length == 0) {
					msg = "Chưa có Biên bản báo mất nào được chọn";
				}
				if (msg.length == 0) {
					if ((isNaN(recordStatus) || recordStatus < 0)) {
						msg = "Vui lòng chọn trạng thái để lưu.";
					}
				}
				if (msg.length > 0) {
					$('#errMsg').html(msg).show();
					return false;
				}
				
				for (var i= 0, size = rows.length; i< size; i++) {
					arrEquipLostRecord.push(rows[i].id);
				}
			} else {
				// truong hop cap nhat o detail
				recordStatus = Number($('#ddlStatusEquipRecord').val()); // gia tri nay co
				if (msg.length == 0) {
					if (EquipLostRecord._eqLostRecId != null && EquipLostRecord._eqLostRecId.toString().trim().length > 0) {
						arrEquipLostRecord.push(Number(EquipLostRecord._eqLostRecId));
					} else {
						msg = "Vui lòng chọn lại phiếu báo mất.";
					}
				}
				if (msg.length > 0) {
					$('#errMsg').html(msg).show();
					return false;
				}
			}
			//Xu ly nghiep vu
			var params = {
					recordStatus: recordStatus,
					arrEquipLostRecordTxt: arrEquipLostRecord.join(',').trim()
			};
			if (pheDuyetChange == undefined || pheDuyetChange == null) {
				params.flagError = 0;
			} else {
				params.flagError = 1;
			}
			Utils.addOrSaveData(params, "/equipLostRecord/changeByListEquipLostRecordApprove", null, 'errMsg', function(data) {
				if (data.rows != null && data.rows.length > 0) {
					VCommonJS.showDialogList({
						dialogInfo: {
							title: 'Danh sách lỗi'
						},
						data: data.rows,
						columns : [[
						            {field:'code', title:'Mã biên bản', align:'left', width: 110, sortable:false, resizable:false},
						            {field:'recordStatus', title:'Trạng thái biên bản', align:'center', width: 110, sortable:false, resizable:false, formatter: function(value, row, index){
						            	if (Number($('#ddlStatusEquipRecordImport').val()) > -1) {
						            		return '<a href="javascript:void(0)"><img src="/resources/images/icon-error.png" width="18" heigh="18" /></a>';
						            	}
						            	return '';
						            }},
						            {field:'deliveryStatus', title:'Trạng thái giao nhận', align:'center', width: 110, sortable:false, resizable:false, formatter: function(value, row, index){
						            	if (Number($('#ddlDeliveryStatusImport').val()) > -1) {
						            		return '<a href="javascript:void(0)"><img src="/resources/images/icon-error.png" width="18" heigh="18" /></a>';
						            	}
						            	return '';
						            }}
						            ]]
					});						
				} else {
					$("#successMsg").html("Lưu dữ liệu thành công").show();
					var tm = setTimeout(function() {
						$('.SuccessMsgStyle').html("").hide();
						if (pheDuyetChange == undefined || pheDuyetChange == null) {
							// truong hop cap nhat man hinh chinh phe duyet bao mat; 
							$('#btnSearch').click(); 
						} else {
							// truong hop cap nhat o detail, chinh sua phe duyet
							window.location.href = '/equipLostRecord/approve';
						}
						clearTimeout(tm);
					}, 1500);
				}
			}, null, null, null, msg, null, true);
			return false;
		},

		/**
		 * Mo Dialog Import Bien Ban Mat
		 * 
		 * @author hunglm16
		 * @since January 06, 2015
		 * */
		openDialogImportEquipLostRecord: function (){
			$('.SuccessMsgStyle').html('').hide();
			$('.ErrorMsgStyle').html('').hide();
			$('#equipHistoryTemplateDiv').css("visibility","hidden").hide();
			$('#eqLostRec_import_easyuiPopupImportExcel').dialog({
				title: 'Nhập Excel Danh sách thiết bị',
				width: 465, 
				height: 'auto',
				closed: false,
				cache: false,
				modal: true,
				onOpen: function() {
					
				},
				onClose: function() {
					$('.ErrorMsgStyle').html('').hide();
					$('#fakefileExcelpc').val("").change();
				}
			});
		},
		
		/**
		 * changeDdlStatusEqRecImpPBrowser su kien Change
		 * 
		 * @author hunglm16
		 * @since January 04,2014
		 * */
		changeDdlStatusEqRecImpPBrowser : function (cbx) {
			var dllValue = Number($(cbx).val());
			var isLevel = Number(EquipLostRecord._isLevelShopRoot);
			if (statusRecordsEquip.ALL != dllValue) {
				$('#ddlDeliveryStatusImport').html('');
				$('#ddlDeliveryStatusImport').append('<option value="-2" selected="selected">Chọn</option>');
				$('#ddlDeliveryStatusImport').change();
				selectedDropdowlist('ddlDeliveryStatusImport', statusRecordsEquip.ALL);
			} else {
				$('#ddlDeliveryStatusImport').html('');
				$('#ddlDeliveryStatusImport').append('<option value="-2" selected="selected">Chọn</option>');
				$('#ddlDeliveryStatusImport').append('<option value="2">Đã nhận</option>');
				$('#ddlDeliveryStatusImport').change();
			}
		},
		
		/**
		 * changeDdlStatusEqRecImpPCreate su kien Change
		 * 
		 * @author hunglm16
		 * @since January 04,2014
		 * */
		/*changeDdlStatusEqRecImpPCreate : function (cbx) {
			var dllValue = Number($(cbx).val());
			var isLevel = Number(EquipLostRecord._isLevelShopRoot);
			if (statusRecordsEquip.ALL != dllValue) {
				$('#ddlDeliveryStatusImport').html('');
				$('#ddlDeliveryStatusImport').append('<option value="-2" selected="selected">Chọn</option>');
				$('#ddlDeliveryStatusImport').change();
				selectedDropdowlist('ddlDeliveryStatusImport', statusRecordsEquip.ALL);
			} else {
				$('#ddlDeliveryStatusImport').html('');
				$('#ddlDeliveryStatusImport').append('<option value="-2" selected="selected">Chọn</option>');
				$('#ddlDeliveryStatusImport').append('<option value="1" >Đã gửi</option>');
				$('#ddlDeliveryStatusImport').change();
			}
		},*/

		/**
		 * changeDdlStatusEqRecImpPCreate su kien Change
		 * khi TNKD phan quyen thi co trang thai giao nhan da gui da nha; ngc lai la ktoan: da gui
		 * @author vuongmq
		 * @since 20/05/2014
		 * */
		changeDdlStatusEqRecImpPCreate : function (cbx) {
			var dllValue = Number($(cbx).val());
			if (statusRecordsEquip.ALL != dllValue) {
				$('#ddlDeliveryStatusImport').html('');
				$('#ddlDeliveryStatusImport').append('<option value="-2" selected="selected">Chọn</option>');
				$('#ddlDeliveryStatusImport').change();
				selectedDropdowlist('ddlDeliveryStatusImport', statusRecordsEquip.ALL);
			} else {
				$('#ddlDeliveryStatusImport').html('');
				$('#ddlDeliveryStatusImport').append('<option value="-2" selected="selected">Chọn</option>');
				$('#ddlDeliveryStatusImport').append('<option value="1" >Đã gửi</option>');
				/** truong hop phan quyen co combobox da nhan; khi change thi lay them da nhan*/
				if ($('#permisionDeliverySendAndReceived') != null && $('#permisionDeliverySendAndReceived').length > 0) {
					$('#ddlDeliveryStatusImport').append('<option value="2" >Đã nhận</option>');
				}
				$('#ddlDeliveryStatusImport').change();
			}
		},
		/**
		 * changeDdlStatusEquipRecordDt su kien Change
		 * 
		 * @author hunglm16
		 * @since January 04,2014
		 * */
		changeDdlStatusEquipRecordDt : function (cbx) {
			var dllValue = Number($(cbx).val());
			if (dllValue == 0) {
				$('#ddlDeliveryStatus').html('');
				$('#ddlDeliveryStatus').append('<option value="0">Chưa gửi</option>');
				$('#ddlDeliveryStatus').change();
			} else {
				$('#ddlDeliveryStatus').html('');
				$('#ddlDeliveryStatus').append('<option value="0">Chưa gửi</option>');
				$('#ddlDeliveryStatus').append('<option value="1">Đã gửi</option>');
				$('#ddlDeliveryStatus').change();
			}
		},
		
		/**
		 * changeDdlDeliveryStatusImport su kien Change
		 * 
		 * @author hunglm16
		 * @since January 04,2014
		 * */
		changeDdlDeliveryStatusImport : function (cbx) {
			var dllValue = Number($(cbx).val());
			if (statusRecordsEquip.ALL != dllValue) {
				selectedDropdowlist('ddlStatusEquipRecordImport', statusRecordsEquip.ALL);
			} else {
				
			}
		},
		 
		 /**
		 * Dien gia tri lay shop gan vao EquipLostRecord._phoneShop; EquipLostRecord._addressShop;
		 * @author vuongmq
		 * @since 14/05/2015
		 * */
		 changetxtShopCode: function(shopCode) {
		 	// bao mat cua NPP; lay shop gan vao EquipLostRecord._phoneShop; EquipLostRecord._addressShop; ben js ham fillTextBoxByShopCode gan lai
		 	/** vuongmq; 06/06/2015; reset lai tat cac textbox */
		 	$('#txtCustomerCode').val('');
			$('#txtCustomerName').val('');
			$('#txtRepresentor').val('');
			$('#txtPhone').val('');
			$('#txtAddress').val('');
			EquipLostRecord._rowsDetail = [];
			EquipLostRecord.createDataGidEquipment();
			
			var params = new Object();
			params.shopCode = shopCode;
			Utils.getJSONDataByAjaxNotOverlay(params, '/equipLostRecord/searchShopLost',function (data) {
				if (data.rows != undefined && data.rows != '') {
					EquipLostRecord._phoneShop = data.phoneShop;
					EquipLostRecord._addressShop = data.addressShop;
					if ($('#cboStockType').val() == activeType.RUNNING) {
						// bao mat cua NPP
						$('#txtPhone').val(EquipLostRecord._phoneShop);
						$('#txtAddress').val(EquipLostRecord._addressShop);
					}
				}
			});
		 },
		/**
		 * Dien gia tri Ma Don vi vao txtShopCode
		 * 
		 * @author hunglm16
		 * @since January 05,2015
		 * */

		 /**
		 * Dien gia tri Ma Don vi vao txtShopCode
		 * @author hunglm16
		 * @since January 05,2015
		 * */
		 fillTextBoxByShopCode: function (shopCode, shopId) {
			$('#txtShopCode').val(shopCode);
			$('#txtShopCode').change();
			$('#txtShopCode').focus();
			$('#common-dialog-search-2-textbox').dialog("close");
			$('#common-dialog-search-2-textbox').remove();
		 },
		 
		 /**
		 * Dien gia tri Ma Khach Hang vao txtCustomerCode
		 * 
		 * @author hunglm16
		 * @since January 05,2015
		 * */
		 fillTextBoxByCustomerCode: function (shortCode) {
			$('#txtCustomerCode').val(shortCode);
			$('#txtCustomerCode').focus();
			$('#common-dialog-search-2-textbox').dialog("close");
			$('#common-dialog-search-2-textbox').remove();
			$('#txtCustomerCode').change();
		 },

		 /**
		 * Bat su kien Onchange cau txtCustomerCode
		 * Cap nhat lay cthong tin customer khi change textbox
		 * @author vuongmq
		 * @since 21/05/2015
		 * */
		 onchangeTxtCustomerCode: function (txt) {
			var shortCode = $(txt).val().trim();
			if (shortCode.length > 2) {
				Utils.getJSONDataByAjax({
					shortCode: shortCode,
					shopCode: $('#txtShopCode').val(),
				}, '/equipLostRecord/searchCustomerLost', function(data) {
					$('#txtCustomerName').val('');
					$('#txtPhone').val('');
					$('#txtAddress').val('');
					//EquipLostRecord._mapEquip = new Map();
					if (data.rows != undefined && data.rows != '') {
						$('#txtCustomerName').val(data.nameCustomer);
						$('#txtPhone').val(data.phoneCustomer);
						$('#txtAddress').val(data.addressCustomer);
						//$('#gridEquipment').datagrid('loadData', []);
						EquipLostRecord._rowsDetail = [];
						EquipLostRecord.searchEquipByEquipDeliRec();
					}
				}, null, null);
			} else {
				//$('#gridEquipment').datagrid('loadData', []);
				EquipLostRecord._rowsDetail = [];
				EquipLostRecord.createDataGidEquipment();
				//EquipLostRecord._mapEquip = new Map();
				$('#cusInfFormDiv input').val('');
				$('#txtCustomerCode').val(shortCode);				
			}
			$('#txtRepresentor').focus();
		 },
		 
		 /**
		 * Lay danh sach thiet bi co ban hop dong moi nhat
		 * 
		 * @author hunglm16
		 * @since January 05,2015
		 * */

		  /**
		 * Lay danh sach thiet bi co ban hop dong moi nhat; luc tao moi
		 * @author vuongmq
		 * @since 06/05,2015
		 * cap nhat: lay theo bao mat cua NPP or KH, chon thiet bi cua NPP or KH
		 * // cai nay goi khi chọn bao mat NPP chon thiet bị và khi chon KH se load len grid 1 thiet bi dau tien
		 * */
		 searchEquipByEquipDeliRec: function (idEquipment) {
		 	// cho nay xu ly chon thiet bi cua KH;
		 	//EquipLostRecord._rowsDetail = rows;
		 	var baoMat = $('#cboStockType').val();
		 	/*** 1: activeType.RUNNING la NPP; 2: activeType.WAITING la KH*/
			//EquipLostRecord._mapEquip = new Map();
			var shortCode = $('#txtCustomerCode').val().trim().toUpperCase();
			if (idEquipment == undefined || idEquipment == null) {
				idEquipment = '';
			}
			var params = {
				equipId: idEquipment, // vuongmq; 06/05/2015
				shortCode: shortCode,
				//stockType: 3,
				stockType: Number(baoMat), // vuongmq; 27/04/2015
				tradeStatus: 0,
				flagPagination: false,
				shopCode: $('#txtShopCode').val().trim(), // vuongmq; 14/05/2015
			};
			if (hdFlagLiquidationStatus != undefined && hdFlagLiquidationStatus != null) {
				params.flagUpdate = $('#hdFlagLiquidationStatus').val();
			}
			Utils.getJSONDataByAjaxNotOverlay(params,'/equipLostRecord/searchEquipByEquipDeliveryRecordNew', function(data){
					if (data.rows != undefined && data.rows.length > 0) {
						var rows = data.rows;
						/*for (var i = 0, size = rows.length; i< size; i++) {
							EquipLostRecord._mapEquip.put(rows[i].equipId, rows[i]);
						}*/
						if (idEquipment != '') {
							EquipLostRecord._rowsDetail = rows;
							$('#easyuiPopupSearchEquipment').dialog('close'); // khi nay co popup thi close
						} else {
							if (baoMat == activeType.WAITING) {
								//2: activeType.WAITING la KH
								var tmpFirst = new Array();
								tmpFirst.push(rows[0]);
								EquipLostRecord._rowsDetail = tmpFirst; // tao grid dong dau tien
							}
						}
					} else {
						//vuongmq; 09/05/2015; truong hop bat kha khang; check DB khong co: idEquipment
						if (idEquipment != null) {
							$('#easyuiPopupSearchEquipment').dialog('close'); // khi nay co popup thi close
						}
					}
					//EquipLostRecord.getDataComboboxEquipmentDg(); // vuongmq 06/05/2015; khong xai combobox thiet bi trong grid nua
					EquipLostRecord.createDataGidEquipment();
				}, null, null);
		 },
		 
		 /**
		 * Lay danh sach thiet bi co ban hop dong moi nhat
		 * 
		 * @author hunglm16
		 * @since January 05,2015
		 * */

		 /***
		 * Truong hop cap nhat bao mat se co danh sach nay
		 */
		 loadEquipByEquipLostRec: function () {
			var idEqLostRec = -1;
			var params = {};
			EquipLostRecord._mapEquipByLostRec = new Map();
			if (EquipLostRecord._eqLostRecId != undefined && EquipLostRecord._eqLostRecId != null && EquipLostRecord._eqLostRecId.toString().trim().length > 0) {
				params.eqLostRecId = Number(EquipLostRecord._eqLostRecId);
			}
			if (EquipLostRecord._equipIdChange != undefined && EquipLostRecord._equipIdChange != null && EquipLostRecord._equipIdChange.toString().trim().length > 0) {
				params.equipId = Number(EquipLostRecord._equipIdChange);
			}
			/** vuongmq; 27/05/2015; truong hop chinh sua; cho bao mat cua mobile */
			if (EquipLostRecord._isFlagMobile != undefined && EquipLostRecord._isFlagMobile != null && EquipLostRecord._isFlagMobile.toString().trim().length > 0) {
				params.flagMobile = Number(EquipLostRecord._isFlagMobile);
			}
			
			/** phai truyen shopCode cho ham nay; truong hop chinh sua */
			var shopCode = $('#txtShopCode').val().trim();
			if (shopCode.length <= 0) {
				$('#errMsgInfo').html('Vui lòng chọn Mã NPP để lấy danh sách thiết bị.').show();
				return false;
			}
			if (Number(params.eqLostRecId) > 0 || Number(params.equipId)) {
				params.shopCode = shopCode;
				params.stockType = EquipLostRecord._isFlaglostRecord; //bao mat stock Type
				if (EquipLostRecord._isFlaglostRecord == activeType.WAITING) {
					//bao mat stock Type:2  khach hang
					params.shortCode = $('#txtCustomerCode').val().trim();
				}
				Utils.getJSONDataByAjaxNotOverlay(params,'/equipLostRecord/searchEquipByEquipDeliveryRecordNew', function(data){
					if (data.rows != null && data.rows.length > 0) {
						var rows = data.rows;
						for (var i = 0, size = rows.length; i< size; i++) {
							EquipLostRecord._mapEquipByLostRec.put(rows[i].equipId, rows[i]);
						}
						//$('#gridEquipment').datagrid("loadData", rows);
						EquipLostRecord._rowsDetail = rows;
						EquipLostRecord.createDataGidEquipment();
					}
				}, null, null);
			}
		 },
		 
		 /**
		 * Them moi 2 dong trong vao Datagrid Stock Trans Detail
		 * 
		 * @author hunglm16
		 * @Since January 06, 2015
		 * */
		insertRowEquipmentDg: function () {
			var indexMax = $('#gridEquipment').datagrid('getRows').length;
			$('#gridEquipment').datagrid('insertRow',{
				index: indexMax,
				row: {
					equipId: 0,
					contractNumber: '',
					lostDateStr: '',
					equipCategoryCode: '',
					equipGroupCode: '',
					equipCode: '',
					serial: '',
					price: 0, //
					priceActually: 0, //
					quantity: 0,
					manufacturingYear: 0
				}
			});
			$('#gridEquipment').datagrid('selectRow', indexMax).datagrid('beginEdit', indexMax);
			/*var t = setTimeout(function(){
				var comboboxEquip = $('#gridEquipment').datagrid('getEditors', indexMax);
				if (comboboxEquip != undefined && comboboxEquip != null) {
					var rowsCbx = EquipLostRecord.getDataComboboxEquipmentDg();
					$(comboboxEquip[0].target).combobox("loadData", rowsCbx);
				}
				clearTimeout(t);
			 }, 70);*/
		},
		/**
		 * Lay danh sach Combobox danh sach thiet bi
		 * 
		 * @author hunglm16
		 * @Since January 06, 2015
		 * */
		 /** vuongmq; 06/05/2015; cai nay khong xai nua; */
		getDataComboboxEquipmentDg: function () {
			var rows = [];
			var arrEquipId = null;
			if (EquipLostRecord._mapEquip != null && EquipLostRecord._mapEquip.size() > 0) {
				arrEquipId = EquipLostRecord._mapEquip.keyArray;
				for (var i = 0, size = arrEquipId.length; i < size; i++) {
					var obj = EquipLostRecord._mapEquip.get(arrEquipId[i]);
					var objnew = {
							equipId: obj.equipId,
							equipCode: obj.equipCode
					};
					rows.push(obj);
				}
			}
			if (EquipLostRecord._mapEquipByLostRec != undefined && EquipLostRecord._mapEquipByLostRec != null && EquipLostRecord._mapEquipByLostRec.size() > 0) {
				var arrayKey = EquipLostRecord._mapEquipByLostRec.keyArray;
				if (arrEquipId == null) {
					arrEquipId = [];
				}
				for (var i = 0, size = arrayKey.length; i < size; i++) {
					if (arrEquipId.indexOf(arrayKey[i]) < 0) {
						var obj = EquipLostRecord._mapEquipByLostRec.get(arrayKey[i]);
						var objnew = {
								equipId: obj.equipId,
								equipCode: obj.equipCode
						};
						rows.push(obj);
					}
				}
			}
			return rows;
		},
/////////// BEGIN VUONGMQ; 04/05/2015; cap nhat lap hieu bao mat /////////////	
	/**
	 * su kien tren F9 grid
	 * @author vuongmq
	 * @since 04/05/2015
	 */
	
	editInputInGridLost: function(){
		$('#equipItemCodeInGrid').bind('keyup', function(e){
			if(e.keyCode==keyCode_F9){
				$('.SuccessMsgStyle').html('').hide();
				$('.ErrorMsgStyle').html('').hide();
				if ($('#txtShopCode').val().trim().length <= 0) { 
		 	    	setTimeout(function() {
						$('#errMsgInfo').html('Vui lòng chọn Mã NPP để lấy danh sách thiết bị.').show();
			 		}, 1500);
					return false;
				}
				if ($('#cboStockType').val() == activeType.RUNNING) {
					// bao mat NPP
					EquipLostRecord.showPopupSearchEquipmentLost();
				} else if ($('#cboStockType').val() == activeType.WAITING) {
					// bao mat KH
					if ($('#txtCustomerCode').val().trim().length > 0) { 
		 	    		EquipLostRecord.showPopupSearchEquipmentLost();
					} else {
						setTimeout(function(){
							$('#errMsgInfo').html('Vui lòng chọn khách hàng để lấy danh sách thiết bị.').show();
				 		}, 1500);
						return false;
					}
				}
			}
		});
	},

	/**
	 * Lay danh muc loai, ncc thiet bi
	 * @author nhutnn
	 * @since 20/12/2014
	 */

	 /**
	 * Lay danh muc loai, ncc thiet bi
	 * @author vuongmq
	 * @since 04/05/2015
	 * ghi chu: van lay danh muc theo nhu popup thiet bi cua sua chua
	 */
	getEquipCatalogLost: function(){
		$('#typeEquipment').html('<option value="" selected="selected">Tất cả</option>');
    	// $('#providerId').html('<option value=""  selected="selected">Tất cả</option>');
    	// $('#groupEquipment').html('<option value="" selected="selected">Tất cả</option>'); // vuongmq; 16/06/2015; reset lai
		$.getJSON('/equipment-repair-manage/get-equipment-catalog', function(result) {				
			if(result.equipsCategory != undefined && result.equipsCategory != null && result.equipsCategory.length > 0) {					
				for(var i=0; i < result.equipsCategory.length; i++){
					$('#typeEquipment').append('<option value="'+ result.equipsCategory[i].id +'">'+ Utils.XSSEncode(result.equipsCategory[i].code) +' - ' +Utils.XSSEncode(result.equipsCategory[i].name) +'</option>');  
				}
			} 
			$('#typeEquipment').change();
			// if(result.equipProviders != undefined && result.equipProviders != null && result.equipProviders.length > 0) {					
			// 	for(var i=0; i < result.equipProviders.length; i++){
			// 		$('#providerId').append('<option value="'+ result.equipProviders[i].id +'">'+ Utils.XSSEncode(result.equipProviders[i].code) +' - '+Utils.XSSEncode(result.equipProviders[i].name)+'</option>');  
			// 	}
			// } 
			// $('#providerId').change();
			if(result.equipProviders != undefined && result.equipProviders != null && result.equipProviders.length > 0) {					
				for (var i = 0, isize = result.equipProviders.length; i < isize; i++) {
					result.equipProviders[i].searchText = unicodeToEnglish(result.equipProviders[i].code + " " + result.equipProviders[i].name);
 					result.equipProviders[i].searchText = result.equipProviders[i].searchText.toUpperCase();
				}
				var obj = {
					id: -1,
					code: "Tất cả",
					name: "Tất cả",
					codeName: "Tất cả",
					searchText: unicodeToEnglish("Tất cả").toUpperCase()
				};
				result.equipProviders.splice(0, 0, obj);
				$('#providerId').combobox("loadData", result.equipProviders);
		 		$('#providerId').combobox("setValue", -1);
			}

			// if(result.lstEquipGroup != undefined && result.lstEquipGroup != null && result.lstEquipGroup.length > 0) {					
			// 	for(var i=0; i < result.lstEquipGroup.length; i++){
			// 		$('#groupEquipment').append('<option value="'+ result.lstEquipGroup[i].id +'">'+ Utils.XSSEncode(result.lstEquipGroup[i].code) +' - '+Utils.XSSEncode(result.lstEquipGroup[i].name)+'</option>');  
			// 	}
			// } 
			// $('#groupEquipment').change();				
			if(result.lstEquipGroup != undefined && result.lstEquipGroup != null && result.lstEquipGroup.length > 0) {	
				for (var i = 0, isize = result.lstEquipGroup.length; i < isize; i++) {
					result.lstEquipGroup[i].searchText = unicodeToEnglish(result.lstEquipGroup[i].code + " " + result.lstEquipGroup[i].name);
	 				result.lstEquipGroup[i].searchText = result.lstEquipGroup[i].searchText.toUpperCase();
				}				
				var obj = {
				id: -1,
				code: "Tất cả",
				name: "Tất cả",
				codeName: "Tất cả",
				searchText: unicodeToEnglish("Tất cả").toUpperCase()
				};
				result.lstEquipGroup.splice(0, 0, obj);
				$('#groupEquipment').combobox("loadData", result.lstEquipGroup);
		 		$('#groupEquipment').combobox("setValue", -1);
			} 
		});
	},

	/**
	 * Xu ly trên input loai thiet bi
	 * @author nhutnn
	 * @since 19/12/2014
	 */
	  /**
	 * Xu ly trên input loai thiet bi
	 * @author vuongmq
	 * @since 04/05/2015
	 * ghi chu: van lay change danh muc theo nhu popup thiet bi cua sua chua
	 */
	onChangeTypeEquipLost: function(cb){
		var typeEquip = $(cb).val().trim();
		// $('#groupEquipment').html('<option value="" selected="selected">Tất cả</option>');
		if(typeEquip != undefined && typeEquip != null && typeEquip != ''){
			$.getJSON('/equipment-manage-delivery/get-equipment-group?id='+typeEquip, function(result) {				
				// if(result.equipGroups != undefined && result.equipGroups != null && result.equipGroups.length > 0) {					
				// 	for(var i=0; i < result.equipGroups.length; i++){
				// 		$('#groupEquipment').append('<option value="'+ result.equipGroups[i].id +'">'+ Utils.XSSEncode(result.equipGroups[i].code) + ' - '+Utils.XSSEncode(result.equipGroups[i].name)+'</option>');  
				// 	}
				// } 
				// $('#groupEquipment').change();	
				if(result.equipGroups != undefined && result.equipGroups != null && result.equipGroups.length > 0) {	
				for (var i = 0, isize = result.equipGroups.length; i < isize; i++) {
					result.equipGroups[i].searchText = unicodeToEnglish(result.equipGroups[i].code + " " + result.equipGroups[i].name);
	 				result.equipGroups[i].searchText = result.equipGroups[i].searchText.toUpperCase();
				}				
				var obj = {
				id: -1,
				code: "Tất cả",
				name: "Tất cả",
				codeName: "Tất cả",
				searchText: unicodeToEnglish("Tất cả").toUpperCase()
				};
				result.equipGroups.splice(0, 0, obj);
				$('#groupEquipment').combobox("loadData", result.equipGroups);
		 		$('#groupEquipment').combobox("setValue", -1);
			} 
			});
		}
	},

	/**
	 * Show popup tim kiem thiet bi
	 * @author vuongmq
	 * @since 04/05/2015
	 */
	showPopupSearchEquipmentLost: function() {
		$('#easyuiPopupSearchEquipment').show();
		$('#easyuiPopupSearchEquipment').dialog({  
			closed: false,  
			cache: false,  
			modal: true,
			width : 1000,
	//	        height : 'auto',
			onOpen: function(){	    
				//setDateTimePicker('yearManufacturing');
				Utils.bindComboboxEquipGroupEasyUICodeName('groupEquipment', null, 193);
	        	Utils.bindComboboxEquipGroupEasyUICodeName('providerId', null, 193);

				var year = new Date();
				year = year.getFullYear(); 
				var op = '<option value="" selected="selected"></option><option value="'+year+'">'+year+'</option>';
				for(var i=1; i<100; i++){
					op+='<option value="'+(year-i)+'">'+(year-i)+'</option>';
				}
				$('#yearManufacturing').html(op);
				$('#equipmentCode, #seriNumber, #typeEquipment').bind('keyup',function(event){
					if(event.keyCode == keyCodes.ENTER){
						$('#btnSeachEquipmentDlg').click(); 
					}
				});
				$('#yearManufacturing').bind('keyup',function(event){
	        		if(event.keyCode == keyCodes.ENTER){
	        			$('#btnSeachEquipmentDlg').click(); 
	        		}
	        	});
	        	$('#groupEquipment, #providerId').next().find(".combo-text.validatebox-text").bind('keyup',function(event){
	        		if(event.keyCode == keyCodes.ENTER){
	        			$('#btnSeachEquipmentDlg').click(); 
	        		}
	        	});
				$('#stockCode').bind('keyup',function(event){
					if(event.keyCode == keyCodes.ENTER){
						$('#btnSeachEquipmentDlg').click(); 
					}
				});
				if ($('#cboStockType').val() == activeType.RUNNING) {
					// bao mat NPP
					enable('stockCode');
				} else if ($('#cboStockType').val() == activeType.WAITING) {
					// bao mat KH
					disabled('stockCode');
				}
	//	        	$('#stockCode').bind('keyup',function(event){
	//	        		if(event.keyCode == keyCodes.F9){
	//	        			RepairEquipmentManageCatalog.showPopupStock();
	//	        		}
	//	        	});
				EquipLostRecord.getEquipCatalogLost();
				var params = new Object();
				//tamvnm: thay doi thanh autoCompleteCombobox
				var groupId = $('#groupEquipment').combobox('getValue');
	        	if (groupId == -1 || groupId == "") {
	        		groupId = "";
	        	}
	        	var providerId = $('#providerId').combobox('getValue');
	        	if (providerId == -1 || providerId == "") {
	        		providerId = "";
	        	}
				params = {
						equipCode: $('#equipmentCode').val().trim(),
						seriNumber: $('#seriNumber').val().trim(),
						categoryId: $('#typeEquipment').val().trim(),
						// groupId: $('#groupEquipment').val().trim(),
						// providerId: $('#providerId').val().trim(),
						groupId: groupId,
						providerId: providerId,
						yearManufacture: $('#yearManufacturing').val().trim(),
						stockCode: $('#stockCode').val().trim(),
						stockType: Number($('#cboStockType').val()), // vuongmq; 05/05/2015
						shortCode: $('#txtCustomerCode').val().trim(), // vuongmq; 05/05/2015
						tradeStatus: 0, // vuongmq; 08/05/2015
						shopCode : $('#txtShopCode').val().trim(), // vuongmq; 14/05/2015
					};
				if (hdFlagLiquidationStatus != undefined && hdFlagLiquidationStatus != null) {
					params.flagUpdate = $('#hdFlagLiquidationStatus').val();
				}
				$('#equipmentGridDialog').datagrid({
					url : '/equipLostRecord/search-equipment-delivery-lost',
					autoRowHeight : true,
					rownumbers : true, 
					//fitColumns:true,
					pagination:true,
					scrollbarSize:0,
					pageSize:10,
					pageList: [10],
					width: $('#equipmentGridDialogContainer').width(),
					height: 'auto',
					autoWidth: true,	
					queryParams: params,
					columns:[[
						{field: 'select',title:'',width: 50,sortable:false,resizable:false, align: 'center', formatter: function(value, row, index) {
							return '<a href="javascript:void(0);" onclick="EquipLostRecord.searchEquipByEquipDeliRec('+row.equipmentId+');">Chọn</a>';
						}},	      
						{field: 'equipmentCode',title:'Mã thiết bị',width: 200,sortable:false,resizable:false, align: 'left', formatter: function(value, row, index) {
							return Utils.XSSEncode(value);
						}},	
						{field: 'seriNumber',title:'Số serial',width: 100,sortable:false,resizable:false, align: 'left', formatter: function(value, row, index) {
							return Utils.XSSEncode(value);
						}},	
						{field: 'healthStatus',title:'Tình trạng thiết bị',width: 100,sortable:false,resizable:false, align: 'left', formatter: function(value, row, index) {
							return Utils.XSSEncode(value);
						}},	
						{field: 'stock',title:'Kho',width: 120,sortable:false,resizable:false, align: 'left', formatter: function(value, row, index) {
							var html="";
							if(row != null && row.stockCode != null && row.stockName!= null){
								html = row.stockCode+' - '+row.stockName; 
							}
							return Utils.XSSEncode(html);
						}},	
						{field: 'typeEquipment',title:'Loại thiết bị',width: 80,sortable:false,resizable:false, align: 'left', formatter: function(value, row, index) {
							return Utils.XSSEncode(value);
						}},	
						{field: 'groupEquipment',title:'Nhóm thiết bị',width: 200,sortable:false,resizable:false, align: 'left', formatter: function(value, row, index) {
							var html="";
							if(row != null && row.groupEquipmentCode != null && row.groupEquipmentName!= null){
								html = row.groupEquipmentCode+' - '+row.groupEquipmentName; 
							}
							return Utils.XSSEncode(html);
						}},	
						{field: 'capacity',title:'Dung tích (lít)',width: 80,sortable:false,resizable:false, align: 'right', formatter: function(value, row, index) {
							return Utils.XSSEncode(value);
						}},	
						{field: 'equipmentBrand',title:'Hiệu',width: 80,sortable:false,resizable:false, align: 'left', formatter: function(value, row, index) {
							return Utils.XSSEncode(value);
						}},	
						{field: 'equipmentProvider',title:'NCC',width: 80,sortable:false,resizable:false, align: 'left', formatter: function(value, row, index) {
							return Utils.XSSEncode(value);
						}},	
						{field: 'yearManufacture',title:'Năm sản xuất',width: 80,sortable:false,resizable:false, align: 'left', formatter: function(value, row, index) {
							return Utils.XSSEncode(value);
						}}
					]],
					onBeforeLoad:function(param){											 
					},
					onLoadSuccess :function(data){   			 
						if (data == null || data.total == 0) {
							//$("#errMsgInfo").html("Không tìm thấy danh sách biên bản giao nhận thiết bị!").show();
						}else{
							$('.datagrid-header-rownumber').html('STT');		    	
						}	    		
					}
				});
			},
			onClose: function(){
				$('#easyuiPopupSearchEquipment').hide();
				$('#equipmentCode').val('').change();
				$('#seriNumber').val('').change();
				$('#typeEquipment').html('<option value="" selected="selected">Tất cả</option>');
				// $('#groupEquipment').html('<option value="" selected="selected">Tất cả</option>').change();
				// $('#providerId').html('<option value="" selected="selected">Tất cả</option>');
				$('#yearManufacturing').val('').change();
				$('#stockCode').val('').change();
				$('#equipmentGridDialog').datagrid("loadData",[]);
			}
		});
	},

	/**
	 * Chon thiet bi fill vao cac textbox
	 * @author phuongvm
	 * @since 06/01/2015
	 */
	 /** cai nay khong dung chon thiet bi nua; se goi ham: searchEquipByEquipDeliRec() de lay thiet bi filter vao grid*/
	/*selectEquipmentLost: function(index){
		var rowPopup = $('#equipmentGridDialog').datagrid('getRows')[index];
		var kho="";
		if(rowPopup.stockCode != undefined && rowPopup.stockCode != null && rowPopup.stockName != undefined && rowPopup.stockName!= null){
			kho = rowPopup.stockCode+' - '+rowPopup.stockName; 
		}
		$('#txtEquipCode').val(rowPopup.equipmentCode);
		$('#lblEquipName').html(rowPopup.groupEquipmentName);
		$('#txtStock').val(kho);
		$('#txtExpiredDate').val(rowPopup.warrantyExpiredDate); //06/04/2015; vuongmq; ngay het han bh 

		$('#txtRepairCount').val(rowPopup.repairCount);
		var rows = new Array();
    	EquipLostRecord._lstEquipItemId = new Array();
    	$('#gridDetail').datagrid('loadData',rows);
		
		$('#easyuiPopupSearchEquipment').dialog('close');
	},*/

	/**
	 * Tim kiem thiet bi
	 * @author vuongmq
	 * @since 04/05/2015
	 */
	searchEquipmentLost: function(){
		var params=new Object(); 
		params.equipCode = $('#equipmentCode').val().trim();
		params.seriNumber = $('#seriNumber').val().trim();
		params.categoryId = $('#typeEquipment').val().trim();
		
		//tamvnm: thay doi thanh autoCompleteCombobox: -1 la truong hop tim group ko co trong BD
		// params.groupId = $('#groupEquipment').val().trim();
		if ($('#groupEquipment').combobox('getValue') == "") {
			$('#groupEquipment').combobox('setValue', -1);
			params.groupId = "";
		} else if ($('#groupEquipment').combobox('getValue') == -1) {
			params.groupId = "";
		} else {
			if (Utils.getMessageOfInvaildNumberNew($('#groupEquipment').combobox("getValue")).length == 0) {
				params.groupId = $('#groupEquipment').combobox("getValue");
			} else {
				params.groupId = -1;
			}
		}
		// params.providerId = $('#providerId').val().trim();
		if ($('#providerId').combobox('getValue') == "") {
			$('#providerId').combobox('setValue', -1);
			params.providerId = "";
		} else if ($('#providerId').combobox('getValue') == -1) {
			params.providerId = "";
		} else {
			if (Utils.getMessageOfInvaildNumberNew($('#providerId').combobox("getValue")).length == 0) {
				params.providerId = $('#providerId').combobox("getValue");
			} else {
				params.providerId = -1;
			}
		}

		params.yearManufacture = $('#yearManufacturing').val().trim();
		params.stockCode = $('#stockCode').val().trim();
		params.stockType = Number($('#cboStockType').val()); // vuongmq; 05/05/2015
		params.shortCode = $('#txtCustomerCode').val().trim(); // vuongmq; 05/05/2015
		params.shopCode = $('#txtShopCode').val().trim(); // vuongmq; 14/05/2015
		var hdFlagLiquidationStatus = $('#hdFlagLiquidationStatus');
		if (hdFlagLiquidationStatus != undefined && hdFlagLiquidationStatus != null) {
			params.flagUpdate = $('#hdFlagLiquidationStatus').val();
		}
		$('#equipmentGridDialog').datagrid('load',params);
	},

	/**
	 * Tim kiem kho
	 * @author vuongmq
	 * @since 04/05/2015
	 */
	searchStockLost: function(){
		var params=new Object(); 
		params.arrShopTxt = $('#txtShopCode').val().trim(); // vuongmq; 19/05/2015
		params.shopCode = $('#shopCode').val().trim();
		params.shopName = $('#shopName').val().trim();
		$('#stockGridDialog').datagrid('load',params);
	},

	/**
	 * chon kho
	 * 
	 * @return the string
	 * @author vuongmq
	 * @since 04/05/2015
	 */
	selectStockLost: function(shopCode){
		$('#stockCode').val(shopCode);		
		$('#easyuiPopupSearchStock').dialog('close');
	},
	/**
	 * Show popup chon kho
	 * @author vuongmq
	 * @since 04/05/2015
	 */
	showPopupStockLost:function(){
	$('#easyuiPopupSearchStock').show();
		$('#easyuiPopupSearchStock').dialog({  
	        closed: false,  
	        cache: false,  
	        modal: true,
	        width : 800,
	        height : 'auto',
	        onOpen: function(){	
				$('#shopCode').focus();
				$('#shopCode, #shopName').bind('keyup',function(event){
					if(event.keyCode == keyCodes.ENTER){
						$('#btnSearchStockDlg').click(); 
					}
				});	
				$('#stockGridDialog').datagrid({
					url : '/equipLostRecord/searchStockLost',
					autoRowHeight : true,
					rownumbers : true, 
					fitColumns:true,
					scrollbarSize:0,
					pagination:true,
					pageSize: 10,
					pageList: [10],
					width: $('#stockGridDialogContainer').width(),
					height: 'auto',
					autoWidth: true,	
					queryParams:{
						arrShopTxt: $('#txtShopCode').val().trim(), // vuongmq; 14/05/2015
						shopCode: $('#shopCode').val().trim(),
						shopName: $('#shopName').val().trim()
					},
					columns:[[
						{field:'shopCode', title:'Đơn vị', align:'left', width: 140, sortable:false, resizable:false, formatter:function(value,row,index) {
			        	return row.shopCode + " - "+row.shopName ;        
			        }},
			        {field:'stockCode', title:'Mã kho', align:'left', width: 80, sortable:false, resizable:false},
			        {field:'choose', title:'', align:'center', width:40, sortable:false, resizable:false, formatter:function(value,row,index) {
			        	return "<a href='javascript:void(0)' onclick=\"return EquipLostRecord.selectStockLost('"+ row.stockCode+"');\">Chọn</a>";        
			        }}			
					]],
					onBeforeLoad:function(param){											 
					},
					onLoadSuccess :function(data){   			 
						if (data == null || data.total == 0) {
							//$("#errMsgInfo").html("Không tìm thấy danh sách biên bản giao nhận thiết bị!").show();
						}else{
							$('.datagrid-header-rownumber').html('STT');		    	
						}	    		
					}
				});				
	        },
			onClose: function(){
				$('#easyuiPopupSearchStock').hide();
				$('#shopCode').val('').change();
				$('#shopName').val('').change();
				$('#stockGridDialog').datagrid('loadData',[]);
				$('#stockCode').focus();
			}
		});		
		return false;
	},

	 /**
	 * cap nhat; bind su kien khi thay doi text box gridDetail chi tiet bao mat
	 * @author vuongmq
	 * @since 06/05/2015
	 */
	bindEventChangeTextLost: function() {
	/** chon so luong*/
		$('.priceActuallyInGridzz').blur(function () {
		    var idQuan = $(this).attr('id');
		    var value = $(this).val();
		    var id = idQuan.split('_')[1];
	     	var index = $(this).attr('gridIndex');
	     	/** 16/05/2015; vuongmq; vi grid 1 thiet bi bao mat*/
	     	if (index == '') {
	     		index = 0;
	     	}
		    if (index != undefined && index != null) {
		    	var data = $('#gridEquipment').datagrid ('getRows')[index];
		    	if (data != undefined && data != null) {
		    		/** value nhap phai nho hon = nguyen gia (price)*/
		    		var nguyenGia = 0;
		    		if (data.price != null) {
		    			nguyenGia  = data.price; 
		    		}
		    		if (Number(value) > Number(nguyenGia)) {
		    			var msg = 'Vui lòng nhập giá trị còn lại nhỏ hơn hoặc bằng Nguyên giá.';
		    			EquipLostRecord._msgGiaTriConlai = msg;
		    			setTimeout(function() {
							$('#errMsgInfo').html(msg).show();
				 		}, 1500);
						return false;
		    		} else {
		    			$('.ErrorMsgStyle').html('').hide();
		    			EquipLostRecord._msgGiaTriConlai = '';
		    		}
		    	}
		    }
		});
		$('.vinput-money').each(function() {
			var objectId = $(this).attr('id');
			if (objectId!=null && objectId!=undefined && objectId.length>0) {
				Utils.formatCurrencyFor(objectId);
			}			
		});
		Utils.bindFormatOnTextfieldInputCss('vinput-money',Utils._TF_NUMBER);
	},
/////////// END VUONGMQ; 04/05/2015; cap nhat lap hieu bao mat /////////////
		/**
		 * Cap nhat bien ban bao mat
		 * 
		 * @author hunglm16
		 * @since January 06, 2015
		 * */
		insertOrUpdateEquipLostRecord : function () {
			$('.SuccessMsgStyle').html('').hide();
			$('.ErrorMsgStyle').html('').hide();
			var comboboxEquip = $('#gridEquipment').datagrid('getEditors', 0);
			if (comboboxEquip != undefined && comboboxEquip != null && comboboxEquip.length > 0) {
				$('#errMsg').html('Bạn chưa chọn thiết bị hoặc thiết bị lựa chọn không hợp lệ').show();
				return false;
			}
			var msg = '';
			var params = {};
			//var shopCode = $('#txtShopCode').val().trim();
			var shortCode = $('#txtCustomerCode').val().trim();
			var representor =  $('#txtRepresentor').val().trim();
			var lostDate = $('#txtLostDate').val().trim();
			var fDate = $('#txtLastArisingSalesDate').val().trim();
			var tDate = $('#txtLostDate').val().trim();
			var baoMat = $('#cboStockType').val(); // vuongmq; 06/05/2015
			var shopCode = $('#txtShopCode').val().trim(); // vuongmq; 14/05/2015
			//Datpv4 26/06/2015 fix nam bao mat >= nsx
			var rows = $('#gridEquipment').datagrid('getRows');
			var firstRowDataGrid = rows[0];
			var manufacturingYear= firstRowDataGrid.manufacturingYear;
			var createDate = $('#createDate').val();
			var note = $('#note').val();
			if (msg.length == 0) {
				msg = Utils.getMessageOfRequireCheck('txtShopCode', 'Mã NPP');
			}

			if(msg.length == 0 && $('#createDate').val().trim().length > 0 && !Utils.isDate($('#createDate').val()) && $('#createDate').val() != '__/__/____' ){
				msg = 'Ngày biên bản không tồn tại hoặc không đúng định dạng dd/mm/yyyy.';
				err = '#createDate';
			}

			var curDate = ReportUtils.getCurrentDateString(); //Lay ngay hien tai sysDate
			if (msg.length == 0 && !Utils.compareDate($('#createDate').val().trim(), curDate)) {
				msg = 'Ngày biên bản không được lớn hơn ngày hiện tại. Vui lòng nhập lại';
				$('#createDate').focus();
			}

			if (msg.length == 0 && $('#note').attr('maxlength') > 500) {
				msg = 'Bạn nhập quá giới hạn của trường note';
			}


			if (baoMat != null && baoMat == activeType.WAITING) {
				// bao mat KH
				if (msg.length == 0) {
					msg = Utils.getMessageOfRequireCheck('txtCustomerCode', 'Mã khách hàng');
				}
			}
			if (msg.length == 0) {
				msg = Utils.getMessageOfRequireCheck('txtRepresentor', 'Người đại diện');
			}
			if (msg.length == 0) {
				msg = Utils.getMessageOfRequireCheck('txtLostDate', 'Thời gian mất');
			}
			if (msg.length == 0 && lostDate.length > 0) {
				msg = Utils.getMessageOfInvalidFormatDate('txtLostDate', 'Thời gian mất');
				$('#txtLostDate').focus();
			}
			if(msg.length==0 && tDate.length>0 && !Utils.compareDate(tDate, EquipLostRecord._sysDateStr)){
				msg = 'Thời gian mất phải nhỏ hơn hoặc bằng Ngày hiện tại của hệ thống: ' + EquipLostRecord._sysDateStr.trim();
				$('#txtLostDate').focus();
			}
			//Datpv4 26/06/2015 fix nam bao mat >= nsx
			if (msg.length==0) {
				var res = tDate.split("/");
				if (Number(res[2]) < Number(manufacturingYear)) {
					msg = 'Năm báo mất phải cùng hoặc sau năm sản xuất' ;
					$('#txtLostDate').focus();
				}
			}
			//vuongmq 21/07/2015 fix : Ngày hết phát sinh doanh số >= năm sản xuất.; 2. Ngày báo mất  >= Ngày hết phát sinh doanh số
			if (msg.length==0) {
				var res = fDate.split("/");
				if (Number(res[2]) < Number(manufacturingYear)) {
					msg = 'Năm phát sinh doanh số phải cùng hoặc sau năm sản xuất' ;
					$('#txtLastArisingSalesDate').focus();
				}
			}
			// if (msg.length == 0 && fDate.length > 0 && tDate.length > 0 && !Utils.compareDate(fDate, tDate)){
			// 	msg = 'Ngày hết phát sinh doanh số phải nhỏ hơn hoặc bằng Thời gian mất';
			// 	$('#txtLastArisingSalesDate').focus();
			// }

			var lastArisingSalesDate = $('#txtLastArisingSalesDate').val().trim();
			if (msg.length == 0) {
				msg = Utils.getMessageOfRequireCheck('txtLastArisingSalesDate', 'Ngày hết phát sinh doanh số');
			}
			if (msg.length == 0 && lastArisingSalesDate.length > 0) {
				msg = Utils.getMessageOfInvalidFormatDate('txtLastArisingSalesDate', 'Ngày hết phát sinh doanh số');
				$('#txtLastArisingSalesDate').focus();
			}
			if (msg.length == 0 && fDate.length > 0 && !Utils.compareDate(fDate, EquipLostRecord._sysDateStr)) {
				msg = 'Ngày hết phát sinh doanh số phải nhỏ hơn hoặc bằng Ngày hiện tại của hệ thống: ' + EquipLostRecord._sysDateStr.trim();
				$('#txtLostDate').focus();
			}
			if (msg.length == 0 && fDate.length > 0 && tDate.length > 0 && !Utils.compareDate(fDate, tDate)) {
				msg = 'Ngày hết phát sinh doanh số phải trước hoặc cùng Thời gian mất';
				$('#txtLastArisingSalesDate').focus();
			}
			/** vuongmq; 06/05/2015; kiem tra gia tri con lai */
			if (msg.length == 0 && EquipLostRecord._msgGiaTriConlai != null && EquipLostRecord._msgGiaTriConlai.length > 0) {
				msg = EquipLostRecord._msgGiaTriConlai;
				$('.priceActuallyInGridzz').focus();
			}

			var tracingResult = -1;
			var tracingPlace = -1;
			var dem = 0;
			if(msg.length == 0 && !$('[name="radTracingPlace"]').is(':checked')) {
				msg = "GS và NPP truy tìm tủ mất chưa được lựa chọn";
			} else  if (msg.length == 0) {
				var arrChkRadTracingPlace = $('[name="radTracingPlace"]');
				dem = 0;
				for (var i = 0, size = arrChkRadTracingPlace.length; i < size; i++) {
					if($(arrChkRadTracingPlace[i]).is(':checked')) {
						tracingPlace = Number($(arrChkRadTracingPlace[i]).val());
						dem++;
					}
				}
				if (dem > 1) {
					msg = "GS và NPP truy tìm tủ mất chỉ lựa chọn là duy nhất";
					$('#txtConclusion').focus();
				}
			}
			if(!$('[name="radTracingResult"]').is(':checked')) {
				msg = "Kết quả truy tìm chưa được lựa chọn";
			} else if (msg.length == 0) {
				var arrChktracingResult = $('[name="radTracingResult"]');
				dem = 0;
				for (var i = 0, size = arrChktracingResult.length; i < size; i++) {
					if($(arrChktracingResult[i]).is(':checked')) {
						tracingResult = Number($(arrChktracingResult[i]).val());
						dem++;
					}
				}
				if (dem > 1) {
					msg = "Kết quả truy tìm chỉ lựa chọn là duy nhất";
					$('#txtConclusion').focus();
				}
			}
			var conclusion = $('#txtConclusion').val().trim();
			if (msg.length == 0) {
				msg = Utils.getMessageOfSpecialCharactersValidate('txtConclusion', 'Kết luận', Utils._NAME);
			}
			var recommendedTreatment = $('#txtRecommendedTreatment').val().trim();
			if (msg.length == 0) {
				msg = Utils.getMessageOfSpecialCharactersValidate('txtRecommendedTreatment', 'Hướng đề xuất xử lý của công ty IDP', Utils._NAME);
			}
			var statusEquipRecord = $('#ddlStatusEquipRecord').val();
			var deliveryStatus = $('#ddlDeliveryStatus').val();
			var arrEquip = [];
			if (msg.length == 0) {
				var rows = $('#gridEquipment').datagrid('getRows');
				if (rows != undefined && rows != null && rows.length > 0) {
					for (var i = 0, size = rows.length; i < size; i++) {
						var row = rows [i];
						if (row.equipId != undefined && row.equipId != null && row.equipId > 0) {
							arrEquip.push(row.equipId);
						}
					}
				}
				if (arrEquip.length == 0) {
					msg = 'Mã Thiết Bị không hợp lệ';
					$("#equipItemCodeInGrid").focus();
				}
			}
			var arrPriceActually = []; // vuongmq; 06/05/2015
			$('.priceActuallyInGridzz').each(function(){
				if ($(this).val() != null && $(this).val() != '') {
					arrPriceActually.push(Utils.returnMoneyValue($(this).val()));
				} else {
					arrPriceActually.push(0);
				}
			});
			if (msg.length > 0){
				$('#errMsg').html(msg).show();
				return false;
			}
			var eqLostRecId = Number(EquipLostRecord._eqLostRecId);
			
			var arrFile = DropzonePlugin._dropzoneObject.getFilesWithStatus(Dropzone.ADDED);
			if (EquipLostRecord._arrFileDelete == undefined || EquipLostRecord._arrFileDelete == null) {
				EquipLostRecord._arrFileDelete = [];
			}
			if (arrFile != null && arrFile != undefined && arrFile.length > 0) {
				if (EquipLostRecord._countFile == 5) {
					$('#errMsg').html("Tổng số tập tin đính kèm không được vượt quá 5").show();
					return false;
				}
				var maxLength = 5 - EquipLostRecord._countFile;
				if (maxLength < 0) {
					maxLength = 0;
				}
				if (arrFile.length > maxLength) {
					$('#errMsg').html("Số tập tin thêm mới không được vượt quá " + maxLength).show();
					return false;
				}
				msg = UploadUtil.getMsgByFileRequireCheck(UploadUtil._typeFileRequireCheck.equipRecord);
				if (msg.trim().length > 0) {
					$('#errMsg').html(msg).show();
					return false;
				}
				var data = JSONUtil.getSimpleObject2({
					flagMobile: EquipLostRecord._isFlagMobile,
					eqLostMobiRecId: Number(EquipLostRecord._eqLostMobiRecId),
					shortCode: shortCode,
					lostDateStr: lostDate,
					lastArisingSalesDateStr: lastArisingSalesDate,
					tracingResult: tracingResult,
					tracingPlace: tracingPlace,
					conclusion: conclusion,
					recommendedTreatment: recommendedTreatment,
					recordStatus: statusEquipRecord,
					deliveryStatus: deliveryStatus,
					representor: representor,
					eqLostRecId: eqLostRecId,
					equipAttachFileStr: EquipLostRecord._arrFileDelete.join(','),
					arrEquip: arrEquip.join(','),
					arrPriceActually: arrPriceActually.join(','),
					flagLostRecord: baoMat, // vuongmq; 06/05/2015
					shopCode: shopCode, // vuongmq; 14/05/2015
					createDate: createDate,
					note: note,


				});
				$.messager.confirm('Xác nhận', 'Bạn có muốn lưu dữ liệu?', function(r){
					if (r){
						UploadUtil.updateAdditionalDataForUpload(data);
						UploadUtil.startUpload('errMsg');
						return false;
					}
				});
				return false;
			}
			
			var params = {
					flagMobile: EquipLostRecord._isFlagMobile,
					eqLostMobiRecId: Number(EquipLostRecord._eqLostMobiRecId),
					shortCode: shortCode,
					lostDateStr: lostDate,
					lastArisingSalesDateStr: lastArisingSalesDate,
					tracingResult: tracingResult,
					tracingPlace: tracingPlace,
					conclusion: conclusion,
					recommendedTreatment: recommendedTreatment,
					recordStatus: statusEquipRecord,
					deliveryStatus: deliveryStatus,
					representor: representor,
					eqLostRecId: eqLostRecId,
					equipAttachFileStr: EquipLostRecord._arrFileDelete.join(','),
					arrEquip: arrEquip.join(','),
					arrPriceActually: arrPriceActually.join(','),
					flagLostRecord: baoMat, // vuongmq; 06/05/2015
					shopCode: shopCode, // vuongmq; 14/05/2015
					createDate: createDate,
					note: note,
			};
			Utils.addOrSaveData(params, "/equipLostRecord/insertOrUpdateLoadRecord", null, 'errMsg', function(data) {
				//$("#successMsg").html("Lưu dữ liệu thành công").show();
				var htmlView = 'Lưu dữ liệu thành công.';
				if (data.errMsgMail != undefined && data.errMsgMail != '') {
					htmlView += '<span style="color:#f00"> Chưa gửi mail, ' + data.errMsgMail + '</span>';
				}
				$("#successMsg").html(htmlView).show();

				var t = setTimeout(function() {
					$('.SuccessMsgStyle').html("").hide();
					if (EquipLostRecord._isFlagMobile != null && EquipLostRecord._isFlagMobile == 0) {
						window.location.href = '/equipLostRecord/info';	
					} else {
						// bao mat cua mobile
						window.location.href = '/equipLostRecord/mobile/info';
					}					
					clearTimeout(t);
				 }, 1500);
			}, null, null, null, msg);
			return false;
		},
		
		/**
		 * Xay dung ddlStatusEquipRecord theo tung quyen va nghiep vu
		 * 
		 * @author hunglm16
		 * @Since January 08, 2015
		 * */
		createDdlStatusEquipRecord: function (idEqLostRec, isLevel) {
			$('#ddlStatusEquipRecord').html('');
			$('#ddlStatusEquipRecord').append('<option value="0" selected="selected">Dự thảo</option>');
			$('#ddlStatusEquipRecord').append('<option value="1" >Chờ duyệt</option>');
			$('#ddlStatusEquipRecord').change();
		},
		
		/**
		 * Xay dung ddlDeliveryStatus theo tung quyen va nghiep vu
		 * 
		 * @author hunglm16
		 * @Since January 08, 2015
		 * */
		createDdlDeliveryStatus: function (idEqLostRec, isLevel) {
			$('#ddlDeliveryStatus').html('');
			if (ShopDecentralizationSTT.VNM == isLevel) {
				$('#ddlDeliveryStatus').append('<option value="0" selected="selected">Chưa gửi</option>');
				$('#ddlDeliveryStatus').append('<option value="2">Đã nhận</option>');
			} else if (ShopDecentralizationSTT.NPP == isLevel) {
				$('#ddlDeliveryStatus').append('<option value="0">Chưa gửi</option>');
				$('#ddlDeliveryStatus').append('<option value="1" >Đã gửi</option>');
			}
			$('#ddlDeliveryStatus').change();
		},
		
		/**
		 * Xay dung ddlStatusEquipRecord Search theo tung quyen va nghiep vu
		 * 
		 * @author hunglm16
		 * @Since January 08, 2015
		 * */
		createDdlStatusEquipRecordSearch: function (idEqLostRec, isLevel) {
			$('#ddlStatusEquipRecordImport').html('');
			$('#ddlStatusEquipRecordImport').append('<option value="-2" selected="selected">Chọn</option>');
			if (ShopDecentralizationSTT.VNM == isLevel) {
				$('#ddlStatusEquipRecordImport').append('<option value="2">Duyệt</option>');
				$('#ddlStatusEquipRecordImport').append('<option value="3">Không duyệt</option>');
			} else if (ShopDecentralizationSTT.NPP == isLevel) {
				$('#ddlStatusEquipRecordImport').append('<option value="1" >Chờ duyệt</option>');
				$('#ddlStatusEquipRecordImport').append('<option value="4">Hủy</option>');
			}
			$('#ddlStatusEquipRecordImport').change();
		},
		
		/** Chi tiet thiet bi */
		equipmentDetail : function(id, dateStr, lastArisingSalesDateStr){
			$('#lostRecordDgContainerDetail').show();
			var params = new Object();
			params.id = id;
			$('#lostRecordDgDetail').datagrid({		
				url: '/equipLostRecord/mobile/equip-detail',
				width : $('#lostRecordDgContainerDetail').parent().width() - 10,    								        
				pagination : false,
		        rownumbers : true,
		        queryParams: params,
		        scrollbarSize: 0,
		        fitColumns : true,	
		        singleSelect : true,		        			        			        
		        singleSelect : true,			        
				columns:[[						  
					{field:'code', title:'Mã TB', width:120, align:'left', formatter: function(value, row, index){
				    	return Utils.XSSEncode(row.code);
				    }},
					{field:'serial', title:'Serial', width:120, align:'left', formatter: function(value, row, index){
				    	return Utils.XSSEncode(row.serial);
				    }},															    
					{field:'equipGroupName', title:'Tên nhóm TB', width:120, align:'left', formatter: function(value, row, index){
				    	return Utils.XSSEncode(row.equipGroupName);
				    }},
					{field:'equipCategoryName', title:'Tên loại TB', width:120, align:'left', formatter: function(value, row, index){
				    	return Utils.XSSEncode(row.equipCategoryName);
				    }},
				    {field:'healthStatus', title:'Tình trạng TB', width:150, align:'left', formatter: function(value, row, index){
				    	return Utils.XSSEncode(row.healthStatus);
				    }},
				    {field:'dateStr', title:'Ngày mất', width:60, align:'center', formatter: function(value, row, index){
				    	return Utils.XSSEncode(dateStr);
				    }},
				    {field:'lastArisingSaleDateStr', title:'Ngày hết PSDS', width:60, align:'center', formatter: function(value, row, index){
				    	return Utils.XSSEncode(lastArisingSalesDateStr);
				    }}
				]],					
		        onLoadSuccess :function(data){
					$('#lostRecordDgContainer .datagrid-header-rownumber').html('STT');											 	
				 	$(window).resize();
				 	$('html,body').animate({scrollTop: $('#lostRecordDgContainerDetail').offset().top}, 2000);
		        }
			});
		},
		
		/**
		 * Xay dung ddlDeliveryStatus Search theo tung quyen va nghiep vu
		 * 
		 * @author hunglm16
		 * @Since January 08, 2015
		 * */
		createDdlDeliveryStatusSearch: function (idEqLostRec, isLevel) {
			$('#ddlDeliveryStatusImport').html('');
			$('#ddlDeliveryStatusImport').append('<option value="-2" selected="selected">Chọn</option>');
			if (ShopDecentralizationSTT.VNM == isLevel) {
				$('#ddlDeliveryStatusImport').append('<option value="2">Đã nhận</option>');
			} else if (ShopDecentralizationSTT.NPP == isLevel) {
				$('#ddlDeliveryStatusImport').append('<option value="1" >Đã gửi</option>');
			}
			$('#ddlDeliveryStatusImport').change();
		},
		
		/**
		 * Xay dung ddlDeliveryStatus Search theo tung quyen va nghiep vu
		 * 
		 * @author hunglm16
		 * @Since January 08, 2015
		 * */
		editRowGridEquipmentChangePage: function (index) {
			var rowIndex = 0;
			if (index != undefined && index != null &&  !isNaN(index)) {
				rowIndex = index;
			}
			$('#gridEquipment').datagrid('beginEdit', 0);
			setTimeout(function(){
				var comboboxEquip = $('#gridEquipment').datagrid('getEditors', 0);
				$(comboboxEquip[0].target).combobox("loadData", EquipLostRecord.getDataComboboxEquipmentDg());
			 }, 50);
		},
		
		/**
		 * Import Excel danh sach thiet bi
		 * 
		 * @author hunglm16
		 * @since December 18,214
		 * */
		importEquipLostRec : function(){
			$('.SuccessMsgStyle').html('').hide();
			$('.ErrorMsgStyle').html('').hide();
			if ($('#fakefileExcelpc').val().length <= 0 ) {
				$('#errImpExcelMsg').html('Vui lòng chọn tập tin Excel').show();
				return false;
			}
			Utils.importExcelUtils(function(data){
				$('#btnSearch').click();
				$('#fakefileExcelpc').val("").change();
				if (data.message!=undefined && data.message!=null && data.message.trim().length >0){
					$('#errImpExcelMsg').html(data.message.trim()).change().show();
				} else {
					$('#successImpExcelMsg').html("Lưu dữ liệu thành công").show();
					var tm = setTimeout(function(){
						$('.SuccessMsgStyle').html('').hide();
						clearTimeout(tm);
						$('#eqLostRec_import_easyuiPopupImportExcel').dialog('close');
					 }, 1500);
				}
			}, 'importExcelFrm', 'excelFile', 'errImpExcelMsg');
			
		},
		
		/**
		 * In Bien Ban Bao Mat
		 * 
		 * @author hunglm16
		 * @since January 18,214
		 * */
		printEquipLostRec: function(index){
			if (index == undefined || index == null || isNaN(index)) {
				return;
			}
			var rows = $('#lostRecordDg').datagrid('getRows');
			var indexMax = rows.length;
			if(indexMax == 0) {
				$('#errMsg').html('Không có biên bản báo mất nào được chọn').show();
				return;
			}
			if (index >= 0 && index < indexMax) {
				$('#lostRecordDg').datagrid('uncheckAll');
				$('#lostRecordDg').datagrid('checkRow', index);
			}
			$('.ErrorMsgStyle').html("").hide();
			var rowData = $('#lostRecordDg').datagrid('getChecked');
			//lay danh sach bien ban
			if (rowData.length == 0) {
				$('#errMsg').html('Chưa có Biên bản báo mất nào được chọn').show();
				return;
			}
			var arrId = [];
			for(var i = 0, size = rowData.length; i < size; i++){
				arrId.push(rowData[i].id);
			}
			var params = {
				arrEquipLostRec: arrId.join(',')
			};
			$.messager.confirm('Xác nhận','Bạn có muốn xem bản in biên bản?',function(r){
				if(r){
					ReportUtils.exportReport('/equipLostRecord/printLostRecord', params);
					/*Utils.getHtmlDataByAjax(params, '/equipLostRecord/printLostRecord', function(data){
						console.log(data);
						try {
							var _data = JSON.parse(data);
							if(_data.error && _data.errMsg != undefined) {
								$('#errMsg').html(_data.errMsg).show();
							} else {
								var width = $(window).width();
								var height = $(window).height();
								window.open('/commons/open-tax-view?windowWidth='+width+'&windowHeight='+height);
							}
						} catch (e) {
						}
					}, null, null);*/
				}
			});		
			return false;
		},
		
		/**
		 * Xoa tap tin dinh kem
		 * 
		 * @author hunglm16
		 * @since Feb 18,214
		 * */
		removeEquipAttachFile: function(id){
			if (id == undefined || id == null || isNaN(id)) {
				return;
			}
			if (EquipLostRecord._arrFileDelete == null || EquipLostRecord._arrFileDelete == undefined) {
				EquipLostRecord._arrFileDelete = [];
			}
			EquipLostRecord._arrFileDelete.push(id);
			$('#divEquipAttachFile'+id).remove();
			if (EquipLostRecord._countFile != undefined && EquipLostRecord._countFile != null && EquipLostRecord._countFile > 0) {
				EquipLostRecord._countFile = EquipLostRecord._countFile - 1;
			} else {
				EquipLostRecord._countFile = 0;
			}
		},
		
		/**
		 * Xu ly lam moi Grid
		 * 
		 * @author hunglm16
		 * @since Feb 6,2015
		 * */
		createDataGidEquipment: function () {
			if (EquipLostRecord._rowsDetail == undefined || EquipLostRecord._rowsDetail == null) {
				EquipLostRecord._rowsDetail = [];
			}
			$('#gridEquipment').datagrid({
				data: EquipLostRecord._rowsDetail,
				width : $('#gridEquipment').width() - 15,
		        autoWidth: true,
		        rownumbers : true,
		        fitColumns : true,
		        autoRowHeight : true,
		        scrollbarSize: 0,
				columns:[[
					{field:'contractNumber', title:'Số hợp đồng', width:120, sortable: false, resizable: false, align:'left', formatter: function(value, row, index){
						if (Number(EquipLostRecord._equipIdChange) == row.equipId && Number(EquipLostRecord._eqLostMobiRecId) <= 0){
							return Utils.XSSEncode(EquipLostRecord._contractNumber);
						}
						return Utils.XSSEncode(row.contractNumber);
					}},  
				    {field:'equipCategoryCode', title:'Loại thiết bị', width:80, sortable: false, resizable: false, align:'left', formatter: function(value, row, index){
				    	if (row.equipId != undefined && row.equipId != null && row.equipId > 0) {
				    		return Utils.XSSEncode(row.equipCategoryCode +' - '+ row.equipCategoryName);		    		
				    	}
				    	return '';
				    }},
				    {field:'equipGroupCode', title:'Nhóm thiết bị', width:90, sortable: false, resizable: false, align:'left', formatter: function(value, row, index){
				    	if (row.equipId != undefined && row.equipId != null && row.equipId > 0) {
					    	return Utils.XSSEncode(row.equipGroupCode +' - '+ row.equipGroupName);
				    	}
				    	return '';
				    }},
				    {field:'equipCode', title:'Mã thiết bị (F9)', width:100, sortable: false, resizable: false, align:'left', formatter: function(value, row, index){
				    	//return Utils.XSSEncode(value);
				    	var html = '<input type="text" class="equipItemCodeInGridClzz InputTextStyle" id="equipItemCodeInGrid" style="margin-bottom:2px; width:94%" maxlength="50" readonly="true">';
				    	if (value != null) {
				    		html = '<input type="text" class="equipItemCodeInGridClzz InputTextStyle" id="equipItemCodeInGrid" style="margin-bottom:2px; width:94%" maxlength="50" readonly="true" value ="'+value+'">';
				    	}
		    			return html;
				    }},
				    {field:'serial', title:'Số serial', width:100, sortable: false, resizable: false, align:'left', formatter: function(value, row, index){
				    	return Utils.XSSEncode(value);
				    }},
				    {field:'price', title:'Nguyên giá', width:60, sortable: false, resizable: false, align:'right', formatter: function(value, row, index){
				    	return Utils.XSSEncode(formatCurrency(value));
				    }},
				    {field:'priceActually', title:'Giá trị còn lại', width:60, sortable: false, resizable: false, align:'right', formatter: function(value, row, index){
			    		var id = '';
						if (row.equipId != null) {
							id = row.equipId;
						}
						var idx = '';
						if (index != null) {
							idx = index;
						}
						var html = '<input type="text" class="priceActuallyInGridzz InputTextStyle vinput-money" id="priceActuallyInGrid_'+id+'" style="margin-bottom:2px; width: 92%; text-align: right;" maxlength="17" gridIndex="'+idx+'">';
						if (value != null) {
							html = '<input type="text" class="priceActuallyInGridzz InputTextStyle vinput-money" id="priceActuallyInGrid_'+id+'" style="margin-bottom:2px; width: 92%; text-align: right;" maxlength="17" gridIndex="'+idx+'" value="'+formatCurrency(value)+'">';
						}
						return html;
				    }},
				    {field:'quantity', title:'Số lượng', width:40, sortable: false, resizable: false, align:'right', formatter: function(value, row, index){
				    	if (row.equipId != undefined && row.equipId != null && row.equipId > 0) {
					    	return 1;
				    	}
				    	return '';
				    }},
				    {field:'manufacturingYear', title:'Năm sản xuất', width:55, sortable: false, resizable: false, align:'right', formatter: function(value, row, index){
				    	if (row.manufacturingYear != undefined && row.manufacturingYear != null && row.manufacturingYear > 0) {
					    	return Utils.XSSEncode(row.manufacturingYear);
				    	}
				    	return '';
				    }},
				    {field:'liquidationStatus', title:'Trạng thái thanh lý', width:60, sortable: false, resizable: false, align:'left', formatter: function(value, row, index){
				    	if (value != null && value != undefined) {
				    		if (value == 1) {
				    			return 'Đã thanh lý';
				    		}
				    		if (value == 2) {
				    			return 'Đang thanh lý';
				    		}
				    		if (value == 0) {
				    			return 'Không thanh lý';
				    		}
				    	}
				    	return '';
				    }}
				    /*{field:'changeRow', title:'', width:60, sortable: false, resizable: false, align:'center', formatter: function(value, row, index){
						if (row.equipId > 0) {
							return '<a id="permisionCreate_GridChange_'+row.equipId+'" class="cmsiscontrol" href="javascript:void(0)" onclick="EquipLostRecord.editRowGridEquipmentChangePage();"><img title="Chỉnh sửa" src="/resources/images/icon-edit.png" width="18" height="18" style="padding-left: 5px;"></a>';
						}
				    }}*/
				]],
		        onLoadSuccess: function(data){
					$('#gridEquipContainer .datagrid-header-rownumber').html('STT');
					Utils.updateRownumWidthAndHeightForDataGrid('gridEquipment');
					$(window).resize();
				 	var rows = $('#gridEquipment').datagrid('getRows');
		 	    	if(rows == null || rows.length == 0){
						//$('#gridEquipment').datagrid("hideColumn", "changeRow");
						/*if ($('#txtCustomerCode').val().trim().length > 0) { 
		 	    			EquipLostRecord.insertRowEquipmentDg();
						}*/
						/** cai nay insert grid luc dau khi qua them moi or chinh sua phieu bao mat; khong co record va cho 1 dong mac dinh*/
						EquipLostRecord.insertRowEquipmentDg();
			    	} else {
						if (EquipLostRecord._eqLostMobiRecId != undefined && EquipLostRecord._eqLostMobiRecId != null && EquipLostRecord._eqLostMobiRecId.toString().trim() > 0) {
							//$('#gridEquipment').datagrid("hideColumn", "changeRow");
							disabled('equipItemCodeInGrid'); // gia tri code thiet bi trong grid; nut cong tu chuc nang bao mat may tinh bang
						} else {
							if (EquipLostRecord._recordStatus != undefined && EquipLostRecord._recordStatus != null && EquipLostRecord._recordStatus.toString().trim() > 0 && statusRecordsEquip.KD == Number(EquipLostRecord._recordStatus)) {
								//$('#gridEquipment').datagrid("hideColumn", "changeRow");
							} else {
								//$('#gridEquipment').datagrid("showColumn", "changeRow");
							}
						}
						/*** vuongmq; 27/05/2015; lay du lieu dada */
						if (rows[0] != null && rows[0].codeMobile != null && rows[0].codeMobile != '' ) {
			    			// truong hop chinh sua nhan biet sua bao mat cua mobile thi disable 
			    			disabled('equipItemCodeInGrid');
			    		}
			    	}
		 	    	Utils.functionAccessFillControl('gridEquipContainer');
		 	    	if($('#gridEquipContainer .cmsiscontrol[id^="permisionCreate_GridChange"]').length == 0){
						//$('#gridEquipment').datagrid("hideColumn", "changeRow");
					} else {
						//$('#gridEquipment').datagrid("showColumn", "changeRow");
					}
					$('.ErrorMsgStyle').html('').hide();
					//vuongmq; 15/05/2015 khi chinh sua ben chuc nang phe duyet bao mat; cho disable grid
					if (EquipLostRecord._FLAG_CHANGE_PHE_DUYET != undefined && EquipLostRecord._FLAG_CHANGE_PHE_DUYET != null && EquipLostRecord._FLAG_CHANGE_PHE_DUYET == activeType.RUNNING) {
						disabled('equipItemCodeInGrid'); // gia tri code thiet bi trong grid
						$('.priceActuallyInGridzz').attr('disabled',true); // gia tri con lai
					}
					/*su kien tren F9 grid*/
					EquipLostRecord.editInputInGridLost();
					EquipLostRecord.bindEventChangeTextLost();
		        }
			});
		}
};
/*
 * END OF FILE - /web/web/resources/scripts/business/equipment/vnm.equip-lost-record.js
 */

/*
 * START OF FILE - /web/web/resources/scripts/business/equipment/vnm.equipment-list-equipment.js
 */
/**
 * JS thuc hien nghiep vu Danh Sach Thiet Bi
 * 
 * @author hunglm16
 * @since December 17,2014
 * */
var EquipmentListEquipment = {
		/**
		 * Khai bao thuoc tinh Bien Toan Cuc
		 * 
		 * @author hunglm16
		 * @since December 17,214
		 * */
		_listEquipmentDg: null,
		_mapData: null,
		_params: null,
		_equipChange: null,
		_countFile:0,
		_arrFileDelete:null,
		_lstEquipGroup: null,
		_lstEquipProvider: null,
		_curShopId: null,
		/**
		 * Tim kiem danh sach thiet bi
		 * 
		 * @author hunglm16
		 * @since December 17,214
		 * */
		searchListEquipment: function () {
			$('.SuccessMsgStyle').html('').hide();
			$('.ErrorMsgStyle').html('').hide();
			$('#equipHistoryTemplateDiv').css("visibility","hidden").hide();
			var msg = '';
			var fromDate = $('#fDate').val().trim();
			var toDate = $('#tDate').val().trim();
			if(msg.length == 0){
				msg = Utils.getMessageOfInvalidFormatDate('fDate', 'Ngày cấp từ');
			}
			if(msg.length ==0){
				msg = Utils.getMessageOfInvalidFormatDate('tDate', 'Ngày cấp đến');
			}
			if(msg.length == 0 && !Utils.compareDate(fromDate, toDate)){
				msg = "Ngày cấp từ phải là ngày trước hoặc cùng ngày với ngày cấp đến.";
				$('#fromDate').focus();
			}
			if(msg.length > 0){
				$('#errMsg').html(msg).show();
				return false;
			}
			//tamvnm: thay doi thanh autoCompleteCombobox: -2 la truong hop tim group ko co trong BD
			var providerId = -1;
			var groupId = -1;
			if ($('#ddlEquipProviderId').combobox("getValue") != "") {
				if ($('#ddlEquipProviderId').combobox("getValue") != -1) {
					if (Utils.getMessageOfInvaildNumberNew($('#ddlEquipProviderId').combobox("getValue")).length == 0) {
						providerId = $('#ddlEquipProviderId').combobox("getValue");
					} else {
						providerId = -2;
					}
				}
			} else {
				$('#ddlEquipProviderId').combobox("setValue", -1);
			}

			if ($('#ddlEquipGroupId').combobox("getValue") != "") {
				if ($('#ddlEquipGroupId').combobox("getValue") != -1) {
					if (Utils.getMessageOfInvaildNumberNew($('#ddlEquipGroupId').combobox("getValue")).length == 0) {
						groupId = $('#ddlEquipGroupId').combobox("getValue");
					} else {
						groupId = -2;
					}
				}
			} else {
				$('#ddlEquipGroupId').combobox("setValue", -1);
			}

			var multiShop = $("#shop").data("kendoMultiSelect");
			var dataShop = multiShop.dataItems();
			var arrShopId = [];
			if (dataShop != undefined && dataShop != null) {
				for (var i = 0, size = dataShop.length; i < size; i++) {
					arrShopId.push(dataShop[i].shopId);
				}
			}
			EquipmentListEquipment._params = {
					equipCategoryId: $('#ddlEquipCategory').val(),
					// equipGroupId: $('#ddlEquipGroupId').val(),
					equipGroupId: groupId,
					tradeType: $('#ddlTradeType').val(),
					tradeStatus: $('#ddlTradeStatus').val(),
					usageStatus: $('#ddlUsageStatus').val(), 
					// equipProviderId: $('#ddlEquipProviderId').val(), 
					equipProviderId: providerId, 
					code: $('#txtCode').val(),
					seriNumber: $('#txtSerial').val(),
//					equipImportRecordCode: $('#txtEquipImportRecordCode').val(),
					stockCode: $('#txtStockCode').val().trim(),
					stockName: $('#txtStockName').val().trim(),
					customerCode: $('#txtCustomerCode').val().trim(),
					customerName: $('#txtCustomerName').val().trim(),
					customerAddress: $('#txtCustomerAddress').val().trim(),
					fromDateStr: fromDate, 
					toDateStr: toDate,
					arrShop: arrShopId.toString()
					// arrShop: $('#shop').combobox('getValue')
			};
			$('#listEquipmentDg').datagrid("load", EquipmentListEquipment._params);
		},
		/**
		 * Xuat Excel Theo Tim kiem danh sach thiet bi
		 * 
		 * @author hunglm16
		 * @since December 18,214
		 * */
		exportBySearchListEquipment: function() {
			$('.SuccessMsgStyle').html('').hide();
			$('.ErrorMsgStyle').html('').hide();
			$('#equipHistoryTemplateDiv').css("visibility","hidden").hide();
			var msg = '';
			if(EquipmentListEquipment._params != null){
				var rows = $('#listEquipmentDg').datagrid("getRows");
				if(rows == null || rows.length==0){
					msg = "Không có dữ liệu xuất Excel";
				}
			}else{
				msg = "Không có dữ liệu xuất Excel";
			}
			if(msg.length > 0){
				$('#errMsg').html(msg).show();
				return false;
			}
			var msg = 'Bạn có muốn xuất Excel?';
			$.messager.confirm('Xác nhận', msg, function(r){
				if (r){
					ReportUtils.exportReport('/equipment-list-manage/exportBySearchEquipmentVO', EquipmentListEquipment._params, 'errMsg');
				}
			});
			
		},
		
		/**
		 * Tai tap tin Excel Import Danh sach thiet bi
		 * 
		 * @author hunglm16
		 * @since December 22,2014
		 * @return Excel
		 * */
		downloadTemplateImportListEquip: function() {
			$('.SuccessMsgStyle').html('').hide();
			$('.ErrorMsgStyle').html('').hide();
			var msg = 'Bạn có muốn tải tập tin Import?';
			ReportUtils.exportReport('/equipment-list-manage/downloadTemplateImportListEquip', {}, 'errMsg');
		},
		
		/**
		 * Mo Dialog Nhap Excel danh sach thiet bi
		 * 
		 * @author hunglm16
		 * @since December 18,214
		 * */
		// openDialogImportListEquipment: function (){
		// 	$('.SuccessMsgStyle').html('').hide();
		// 	$('.ErrorMsgStyle').html('').hide();
		// 	$('#equipHistoryTemplateDiv').css("visibility","hidden").hide();
		// 	$('#easyuiPopupImportExcel').dialog({
		// 		title: 'Nhập Excel Danh sách thiết bị',
		// 		width: 465, 
		// 		height: 'auto',
		// 		closed: false,
		// 		cache: false,
		// 		modal: true,
		// 		onOpen: function() {
					
		// 		},
		// 		onClose: function() {
		// 			$('.ErrorMsgStyle').html('').hide();
		// 		}
		// 	});
		// },
		/**
		 * Import Excel danh sach thiet bi
		 * 
		 * @author hunglm16
		 * @since December 18,214
		 * */
		importListEquipment : function(){
			$('.SuccessMsgStyle').html('').hide();
			$('.ErrorMsgStyle').html('').hide();
			
			Utils.importExcelUtils(function(data){
				EquipmentListEquipment.searchListEquipment();
				$('#fakefilepcListEquip').val("").change();
				if (data.message!=undefined && data.message!=null && data.message.trim().length >0){
					$('#errExcelMsgListEquip').html(data.message.trim()).change().show();
				}else{
					$('#successExcelMsgListEquip').html("Lưu dữ liệu thành công").show();
					var tm = setTimeout(function(){
						$('.SuccessMsgStyle').html('').hide();
						clearTimeout(tm);
						$('#easyuiPopupImportExcel').dialog('close');
					 }, 1500);
				}
			}, 'importFrmListEquip', 'excelFileListEquip', 'errExcelMsgListEquip');
			
		},
		
		/**
		 * Import Excel danh sach thiet bi
		 * 
		 * @author hunglm16
		 * @since December 18,214
		 * */
		viewEquipmentHistory : function(id, code){
			$('.SuccessMsgStyle').html('').hide();
			$('.ErrorMsgStyle').html('').hide();
			if (code != undefined && code != null && code.trim().length > 0) {
				$('#equipmentCodeFromSpan').html(code);
			} else {
				$('#equipmentCodeFromSpan').html("");
			}
			$('#equipHistoryTemplateDiv').css("visibility","visible").show();
			$('#equipHistoryDg').datagrid({
				url : "/equipment-list-manage/getEquipRFVOByListEquipment",
				pagination : true,
		        rownumbers : true,
		        pageNumber : 1,
		        scrollbarSize: 0,
		        autoWidth: true,
		        pageList: [20],
		        autoRowHeight : true,
		        fitColumns : true,
				queryParams: {id: id},
				width: $('#equipHistoryDgContainerGrid').width() - 15,
			    columns:[[	        
				    {field:'equipItemName', title: 'Hạng mục', width: 150, sortable: false, resizable: false , align: 'left', formatter: function(value,row,index){
				    	return Utils.XSSEncode(value);
				    }},
				    {field:'repairCount', title: 'Lần sửa', width: 90, sortable: false, resizable: false , align: 'rigth', formatter: function(value,row,index){
				    	return value;
				    }},
				    {field:'completeDateStr', title: 'Ngày sửa', width: 110, sortable: false, resizable: false , align: 'center', formatter: function(value,row,index){
				    	return Utils.XSSEncode(value);
				    }},
				    {field:'warranty', title: 'Bảo hành - tháng', width: 60, align: 'rigth', sortable: false, resizable: false, formatter: function(value,row,index){
				    	if (value != undefined && value != null) {
				    		return formatCurrency(value);
				    	}
				    	return '';
				    }},
				    {field:'materialPrice', title: 'Đơn giá vật tư', width: 120, align: 'right', sortable: false, resizable: false, formatter: function(value,row,index){
				    	if (value != undefined && value != null) {
				    		return formatCurrency(value);
				    	}
				    	return '';
				    }},
				    {field:'workerPrice', title: 'Đơn giá nhân công', width: 90, align: 'right',sortable: false, resizable: false, formatter: function(value,row,index){
				    	if (value != undefined && value != null) {
				    		return formatCurrency(value);
				    	}
				    	return '';
				    }},
				    {field:'quantity', title: 'Số lượng', width:100, sortable: false, resizable: false , align: 'right', formatter: function(value,row,index){
				    	if (value != undefined && value != null) {
				    		return formatCurrency(value);
				    	}
				    	return '';
				    }},	
				    {field:'totalAmount',title: 'Tổng tiền', width: 100, sortable: false, resizable: false, align: 'right', formatter:function(value,row,index){
				    	if (value != undefined && value != null) {
				    		return formatCurrency(value);
				    	}
				    	return '';
				    }}
			    ]],
			    onLoadSuccess :function(data){	    	
			    	$('.datagrid-header-rownumber').html('STT');
			    	//Utils.functionAccessFillControl('equipHistoryDg');
		   		 	$(window).resize();    		 
			    }
			});
		},
		/**
		 * Xoa tap tin dinh kem
		 * 
		 * @author liemtpt
		 * @since 25/03/2015
		 * */
		removeEquipAttachFile: function(id){
			if (id == undefined || id == null || isNaN(id)) {
				return;
			}
			if (EquipmentListEquipment._arrFileDelete == null || EquipmentListEquipment._arrFileDelete == undefined) {
				EquipmentListEquipment._arrFileDelete = [];
			}
			EquipmentListEquipment._arrFileDelete.push(id);
			$('#divEquipAttachFile'+id).remove();
			if (EquipmentListEquipment._countFile > 0) {
				EquipmentListEquipment._countFile = EquipmentListEquipment._countFile - 1;
			}
		},
		/**
		 * Cap moi Thiet bi
		 * 
		 * @author hunglm16
		 * @since December 20,2014
		 * */
		addOrUpdateEquipment : function (id) {
			$('.SuccessMsgStyle').html('').hide();
			$('.ErrorMsgStyle').html('').hide();
			var msg = '';
			var txtQuantity = '';
			var seriNumber = '';
			if (!isNaN(id) && id > 0) {
				seriNumber = $('#txtSerial').val().trim();
				var arrSeriNumber = seriNumber.split(" ");
				if (arrSeriNumber.length > 1) {
					msg = "Số serial không được phép tồn tại khoảng trắng và chữ có dấu";
				} else {
					msg = Utils.getMessageOfSpecialCharactersValidate('txtSerial', 'Số lượng', Utils._SERIAL);
					if (msg.length > 0) {
						msg = "Số serial không được phép tồn tại khoảng trắng và chữ có dấu";
					}
				}
			} else {
				//tamvnm: bo cap moi
				//So luong
				// txtQuantity = $('#txtQuantity').val().trim();
				// if (msg.length == 0) {
				// 	msg = Utils.getMessageOfRequireCheck('txtQuantity', 'Số lượng');
				// }
				// if (msg.length == 0) {
				// 	msg = Utils.getMessageOfSpecialCharactersValidate('txtQuantity', 'Số lượng', Utils._TF_NUMBER_COMMA);
				// }
				// if (msg.length == 0) {
				// 	if (Number(txtQuantity.replace(/,/g,'')) < 1 || Number(txtQuantity) > 9999) {
				// 		msg = "Số lượng phải là số nguyên lớn hơn 0 và nhỏ hơn 10,000";
				// 	}
				// }
					msg = "Bạn không được phép tạo mới thiết bị!";
					$('#errMsg').html(msg).show();
					return false;

			}
			//Nhom thiet bi
			var ddlEquipGroupId = $('#ddlEquipGroupId').val();
			if (msg.length == 0) {
				msg = Utils.getMessageOfRequireCheck('ddlEquipGroupId', 'Nhóm thiết bị');
			}
			//Nha cung cap
			//tamvnm: thay doi thanh autoCompleteCombobox
			// var ddlEquipProviderId = $('#ddlEquipProviderId').val();
			var ddlEquipProviderId = $('#ddlEquipProviderId').combobox('getValue');
			if (msg.length == 0) {
				// msg = Utils.getMessageOfRequireCheck('ddlEquipProviderId', 'Nhà cung cấp');
				if (ddlEquipProviderId == undefined || ddlEquipProviderId == "") {
					msg = 'Nhà cung cấp không được để trống';
				}
			}
			//Nguyen gia
			var txtPrice = $('#txtPrice').val().trim();
			if (msg.length == 0) {
				msg = Utils.getMessageOfRequireCheck('txtPrice', 'Nguyên giá');
			}
			if (msg.length == 0) {
				msg = Utils.getMessageOfSpecialCharactersValidate('txtPrice', 'Nguyên giá', Utils._TF_NUMBER_COMMA);
			}
			if (msg.length == 0) {
				if (Number(txtPrice.replace(/,/g,'')) < 100 || txtPrice.trim().length > 22) {
					msg = "Nguyên giá phải là kiểu số nguyên dương lớn hơn hoặc bằng 100 và có tối đa 17 ký tự số";
					$('#txtPrice').focus();
				}
			}
			//Ngay het bao hanh
			var txtWarrantyExpiredDate = $('#txtWarrantyExpiredDate').val().trim();
			// if (msg.length == 0) {
			// 	msg = Utils.getMessageOfRequireCheck('txtWarrantyExpiredDate', 'Ngày hết hạn bảo hành');
			// }
			// if (msg.length == 0 && txtWarrantyExpiredDate.length > 0) {
			// 	msg = Utils.getMessageOfInvalidFormatDate('txtWarrantyExpiredDate', 'Ngày hết hạn bảo hành');
			// 	$('#txtWarrantyExpiredDate').focus();
			// }
			//Nam san xuat
			var txtManufacturingYear = $('#txtManufacturingYear').val().trim();
			if (msg.length == 0) {
				msg = Utils.getMessageOfRequireCheck('txtManufacturingYear', 'Năm sản xuất');
			}
			if (msg.length == 0) {
				if (txtManufacturingYear.length != 4 || Number(txtManufacturingYear) < 1000 || Number(txtManufacturingYear) > 9999) {
					msg = 'Năm sản xuất phải có 4 ký tự số';
					$('#txtManufacturingYear').focus();
				}
			}
			
			if(msg.length == 0){
				var yearWarrent = Number(txtWarrantyExpiredDate.split("/")[2]);
				if (yearWarrent < Number(txtManufacturingYear)) {
					msg = 'Năm sản xuất không được lớn hơn năm của Ngày hết hạn bảo hành';
				}
			}
			
			//Trang thai su dung
			var ddlUsageStatus = $('#ddlUsageStatus').val().trim();
			if (msg.length == 0) {
				msg = Utils.getMessageOfRequireCheck('ddlUsageStatus', 'Số lượng');
			}
			//Tinh trang thiet bi
			var ddlHealthStatus = $('#ddlHealthStatus').val().trim();
			if (msg.length == 0) {
				msg = Utils.getMessageOfRequireCheck('ddlHealthStatus', 'Tình trạng thiết bị');
			}
			if (msg.length == 0) {
				msg = Utils.getMessageOfSpecialCharactersValidate('ddlHealthStatus', 'Tình trạng thiết bị', Utils._NAME);
			}
			//Kho(F9)
			var stockCode = $('#stockCode').val().trim();
			if (msg.length == 0) {
				msg = Utils.getMessageOfRequireCheck('stockCode', 'Kho');
			}
			if (msg.length == 0) {
				msg = Utils.getMessageOfSpecialCharactersValidate('stockCode', 'Kho', Utils._CODE);
			}
			if (msg.length > 0){
				$('#errMsg').html(msg).show();
				return false;
			}
			
			//Xu ly nghiep vu
			var params = {
					equipGroupId: ddlEquipGroupId,
					equipProviderId: ddlEquipProviderId,
					price: Number(txtPrice.replace(/,/g,'').trim()),
					warrantyExpiredDateStr: txtWarrantyExpiredDate,
					yearManufacture: Number(txtManufacturingYear),
					usageStatus: Number(ddlUsageStatus),
					healthStatus: ddlHealthStatus,
					stockCode:stockCode
			};
			if (id != undefined && id != null && !isNaN(id) && id > 0) {
				params.id = id;
				params.seriNumber = seriNumber;
			} else {
				params.quantity = Number(txtQuantity.replace(/,/g,'').trim());
				var arrFile = DropzonePlugin._dropzoneObject.getFilesWithStatus(Dropzone.ADDED);
				if (arrFile != null && arrFile != undefined && arrFile.length > 0) {
					if (EquipmentListEquipment._countFile == 5) {
						$('#errMsg').html("Tổng số tập tin đính kèm không được vượt quá 5").show();
						return false;
					}
					var maxLength = 5 - EquipmentListEquipment._countFile;
					if (arrFile.length > maxLength) {
						$('#errMsg').html("Số tập tin thêm mới không được vượt quá " + maxLength).show();
						return false;
					}
					msg = UploadUtil.getMsgByFileRequireCheck(UploadUtil._typeFileRequireCheck.equipRecord);
					if (msg.trim().length > 0) {
						$('#errMsg').html(msg).show();
						return false;
					}
					$.messager.confirm('Xác nhận', 'Bạn có muốn lưu dữ liệu?', function(r){
						if (r){
							var data = JSONUtil.getSimpleObject2(params);
							UploadUtil.updateAdditionalDataForUpload(data);
							UploadUtil.startUpload('errMsg');
							return false;
						}
					});
					return false;
				}
			}
			Utils.addOrSaveData(params, "/equipment-list-manage/addOrUpdateEquipment", null, 'errMsg', function(data) {
				$("#successMsg").html("Lưu dữ liệu thành công").show();
				if (id != undefined && id != null && !isNaN(id) && id > 0) {
					setTimeout(function(){
						$('.SuccessMsgStyle').html("").hide();
					 }, 1500);
				} else {
					setTimeout(function(){
						$('.SuccessMsgStyle').html("").hide();
						window.location.href='/equipment-list-manage/info';
					 }, 700);
				}
			}, null, null, null, msg);
			return false;
		},
		
		/**
		 * Thay doi du lieu nhom thiet bi khi thay doi loai thiet bi
		 * 
		 * @author hunglm16
		 * @since December 21,2014
		 * */
		onchangeDdlEquipCategory : function(cbx) {
			var id = Number($(cbx).val());
			var html = '';
			if (id == undefined || id == null){
				equipCategoryId = 0;
			}
			Utils.getJSONDataByAjax({
				equipCategoryId: id
			}, '/equipment-list-manage/getListEquipGroupVOByCategry', function(data) {
				if (data.rows != undefined && data.rows != null && data.rows.length > 0){
					$('#ddlEquipGroupId').html('<option value="-1" selected="selected">Tất cả</option>');
					var rows = data.rows;
					for (var i = 0; i < rows.length; i++) {
						$('#ddlEquipGroupId').append('<option value="'+rows[i].id+'">'+rows[i].code+' - '+rows[i].name+'</option>');
					}
				} else {
					$('#ddlEquipGroupId').html('<option value="-2" selected="selected">Không có dữ liệu</option>').change();
				}
				$('#ddlEquipGroupId').change();
			}, null, null);
		},
		/**
		 * Thay doi du lieu nhom thiet bi khi thay doi loai thiet bi
		 * 
		 * @author tamvnm
		 * @since 23/07/2015
		 * */
		onchangeDdlEquipCategoryAuto : function(cbx) {
			var id = Number($(cbx).val());
			var html = '';
			if (id == undefined || id == null){
				equipCategoryId = 0;
			}
			Utils.getJSONDataByAjax({
				equipCategoryId: id
			}, '/equipment-list-manage/getListEquipGroupVOByCategry', function(data) {
				if (data.rows != undefined && data.rows != null && data.rows.length > 0){
					var equipGroup = data.rows;
					var obj = {
						id: -1,
						code: "Tất cả",
						name: "Tất cả",
						codeName: "Tất cả",
						searchText: "Tất cả"
					};
					equipGroup.splice(0, 0, obj);
					for(var i = 0, sz = equipGroup.length; i < sz; i++) {
				 		equipGroup[i].searchText = unicodeToEnglish(equipGroup[i].code + " " + equipGroup[i].name);
				 		equipGroup[i].searchText = equipGroup[i].searchText.toUpperCase();
					}
				 	$('#ddlEquipGroupId').combobox("loadData", equipGroup);
				 	$('#ddlEquipGroupId').combobox("setValue", -1);
				} else {
					
				}
			}, null, null);
		},
		
		/**
		 * Thay doi du lieu nhom thiet bi khi thay doi loai thiet bi
		 * 
		 * @author hunglm16
		 * @since December 21,2014
		 * */
		onchangeDdlTradeStatusDiv : function(cbx) {
			var id = Number($(cbx).val());
			var html = '';
			if (id == undefined || id == null){
				$('#ddlTradeType').html('<option value="" selected="selected">Không xác định</option>');
			} else if (id == 0) {
				selectedDropdowlist('ddlTradeType', -1);
				disableSelectbox('ddlTradeType');
			} else {
				enableSelectbox('ddlTradeType');
			}
		},
	 /**
	 * tao gia tri cho autoComplete Combobox Nha cung cap
	 * @author tamnm
	 * @since 17/06/2015
	 */
	setEquipProvider: function(index) {
		var equipProvider = EquipmentListEquipment._lstEquipProvider;
		var obj = {
			id: -1,
			code: "Tất cả",
			name: "Tất cả",
			codeName: "Tất cả",
			searchText: "Tất cả"
		};
		equipProvider.splice(0, 0, obj);
		for(var i = 0, sz = equipProvider.length; i < sz; i++) {
	 		equipProvider[i].searchText = unicodeToEnglish(equipProvider[i].code + " " + equipProvider[i].name);
	 		equipProvider[i].searchText = equipProvider[i].searchText.toUpperCase();
		}
	 	$('#ddlEquipProviderId').combobox("loadData", equipProvider);
	 	$('#ddlEquipProviderId').combobox("setValue", -1);
	},
	/**
	 * tao gia tri cho autoComplete Combobox Nhom thiet bi
	 * @author tamnm
	 * @since 17/06/2015
	 */
	setEquipGroup: function(index) {
		var equipGroup = EquipmentListEquipment._lstEquipGroup;
		var obj = {
			id: -1,
			code: "Tất cả",
			name: "Tất cả",
			codeName: "Tất cả",
			searchText: "Tất cả"
		};
		equipGroup.splice(0, 0, obj);
		for(var i = 0, sz = equipGroup.length; i < sz; i++) {
	 		equipGroup[i].searchText = unicodeToEnglish(equipGroup[i].code + " " + equipGroup[i].name);
	 		equipGroup[i].searchText = equipGroup[i].searchText.toUpperCase();
		}
	 	$('#ddlEquipGroupId').combobox("loadData", equipGroup);
	 	$('#ddlEquipGroupId').combobox("setValue", -1);
	}
};
/*
 * END OF FILE - /web/web/resources/scripts/business/equipment/vnm.equipment-list-equipment.js
 */

/*
 * START OF FILE - /web/web/resources/scripts/business/equipment/vnm.equipment-manage-catalog.js
 */
var EquipmentManageCatalog = {
	lstId : null,
	_xhrDel :null,
	_pMap:null,
	isImportSuccess : false,
	_mapEviction:new Map(),
	paramForDelete:new Object(),
	_xhrSave : null,
	_editIndex: null,
	_mapEquip: null,
	_lstEquipAvailable:[],
	_mapDetail: null,
	_rowDgTmp: null,
	_mapEquipSaleplanCurrent: null,
	_mapEquipSaleplanNew: null,
	_mapEquipSaleplanDelete: null,
	_mapEquipSaleplanAdd: null,
	_lstEquipSaleplan:null,
	_lstEquipSaleplanDel:null,
	_customerCode: null,
	_editIndex:null,
	_lstEquipInRecord:null,
	_numberEquipInRecord:null,
	_lstEquipInsert:null,
	_idNewRow: -1,
	_lstRowID: null,
	_arrFileDelete: null,
	_countFile: 0,
	_isPressF9Customer: null,
	_curShopCode: null,
	_categoryType: 1,
	_lstCustomerType: null,
	/**
	 * Search danh muc thiet bi
	 * 
	 * @return the string
	 * @author hoanv25
	 * @since 14/12/2014
	 */
	searchinfo : function() {
		var msg = '';
		$('#errMsg').html('').hide();	
		var code =$('#code').val().trim();
		var name =$('#name').val().trim();
		var type = $('#type').val().trim();		
		var status = $('#status').val().trim();			
		if (msg.length > 0) {
			$('#errMsg').html(msg).show();
			return false;
		}
		var params =  new Array();
		params.code = code;
		params.name = name;
		params.type = type;		
		params.status = status;
		$('#grid').datagrid('load',params);
		return false;
	},	
	/**
	 * Luu quan ly danh muc thiet bi
	 * 
	 * @return the string
	 * @author hoanv25
	 * @since 14/12/2014
	 */
	save: function(){
		var msg = '';
		$('#errMsg').html('').hide();
		msg = Utils.getMessageOfRequireCheck('code', 'Mã');
		if (msg.length == 0) {
			msg = Utils.getMessageOfSpecialCharactersValidate('code', 'Mã', Utils._CODE);
		}		
		if (msg.length == 0) {
			msg = Utils.getMessageOfRequireCheck('name', 'Tên');
		}
		if (msg.length > 0) {
			$('#errMsg').html(msg).show();
			return false;
		}
		var id = $('#idChange').val().trim();
		var code = $('#code').val().trim();
		var name = $('#name').val().trim();
		var status = $('#status').val().trim();
		var type = $('#type').val().trim();
		var typeItem = $('#typeItem').val().trim();
		var warranty = Utils.returnMoneyValue($('#warranty').val().trim());
		var dataModel = new Object();	
		dataModel.id = id;
		dataModel.code = code;
		dataModel.name = name;
		dataModel.status = status;	
		dataModel.type = type;		
		dataModel.typeItem = typeItem;		
		dataModel.warranty = warranty;		
	//	var err="";
		Utils.addOrSaveData(dataModel, "/equipment-manage-catalog/save", null, 'errMsg', function(data) {
			if (data.error) {
				$('#errMsg').html(data.errMsg).show();
			}else {
				$('#successMsg').html('Cập nhật dữ liệu thành công').change().show();
				setTimeout(function(){
					$('.SuccessMsgStyle').html('').change().show();
					window.location.href='/equipment-manage-catalog/info';
				}, 700);
			  }
			}, null, null);		
		return false;
	},
	
	/**
	 * Them mot dong nhom thiet bi
	 * @author hoanv25
	 * @since 25/12/2014
	 */
	insertEquipmentGroupInGrid: function(){	
		$('#errMsg').html('').hide();
		if(EquipmentStockChange._lstEquipInsert == null){
			EquipmentStockChange._lstEquipInsert = new Array();
		}
		EquipmentStockChange._idNewRow = EquipmentStockChange._idNewRow - 1;
		$('#grid').datagrid("appendRow", {id: EquipmentStockChange._idNewRow});
		$("#grid").datagrid("selectRow", $("#grid").datagrid("getRows").length - 1);		
	},
	/**
	 * Xoa mot dong thiet bi trong luoi
	 * @author hoanv25
	 * @since 12/12/2014
	 */
	deleteEquipment: function(index, id){
		if (id == undefined || id == null) {
			return;
		}
		if (id > 0) {
			EquipmentStockChange._lstEquipSaleplanDel.push(id);
		}
		$('#grid').datagrid("deleteRow", index);
		var rowEquip = $('#grid').datagrid('getRows');
		var nSize = rowEquip.length;
		for(var i=index; i<nSize; i++){
			var row = rowEquip[i];
			$('#grid').datagrid('updateRow',{
				index: i,	
				row: row
			});	
		}
	},
	/**
	 * Luu quan ly danh muc nhom thiet bi
	 * 
	 * @return the string
	 * @author hoanv25
	 * @since 14/12/2014
	 * 
	 * @author hunglm16
	 * @since May 2,2015
	 * @descrption validate dang truc so cho chuc nang
	 */
	saveEquipGroup: function(){
		var msg = '';
		$('#errMsg').html('').hide();
		$('#btnCapNhat').focus();
		msg = Utils.getMessageOfRequireCheck('code', 'Mã nhóm');
		if (msg.length == 0) {
			msg = Utils.getMessageOfSpecialCharactersValidate('code', 'Mã nhóm', Utils._CODE);
		}		
		if (msg.length == 0) {
			msg = Utils.getMessageOfRequireCheck('name', 'Tên nhóm');
		}
		if (msg.length == 0) {
			msg = Utils.getMessageOfRequireCheck('brand', 'Hiệu');
		}
		if (msg.length == 0) {
			msg = Utils.getMessageOfSpecialCharactersValidateEx('toCapacity','Dung tích', Utils._TF_NUMBER_DOT);
		}		
		if (msg.length == 0) {
			msg = Utils.getMessageOfSpecialCharactersValidateEx('fromCapacity','Dung tích', Utils._TF_NUMBER_DOT);
		}
		if (msg.length > 0) {
			$('#errMsg').html(msg).show();
			return false;
		}		
		var code = $('#code').val().trim();
		var name = $('#name').val().trim();		
		var status = $('#status').val().trim();
		var type = $('#type').val().trim();
		var id = $('#idChange').val().trim();
		var equipBrandName = $('#brand').val().trim();
		var toCapacity = $('#toCapacity').val().trim();	
		var fromCapacity = $('#fromCapacity').val().trim();	
		if(toCapacity =='' && fromCapacity == ''){
			  $('#errMsg').html('Bạn chưa nhập giá trị Dung tích!').show();
			  $('#fromCapacity').focus();
			  return false;
		}
		if(fromCapacity != '' && fromCapacity != undefined && Number(fromCapacity) <=0) {
			  $('#errMsg').html('Dung tích từ không hợp lệ!').show();
			  $('#fromCapacity').focus();
			  return false;
		}
		if(toCapacity != '' && toCapacity != undefined && Number(toCapacity) <=0) {
			  $('#errMsg').html('Dung tích đến không hợp lệ!').show();
			  $('#toCapacity').focus();
			  return false;
		}
		if(Number(toCapacity) != '' && Number(toCapacity) > 0 && Number(fromCapacity) >0 && (Number(toCapacity) < Number(fromCapacity))) {
			  $('#errMsg').html('Dung tích từ không được lớn hơn dung tích đến!').show();
			  $('#toCapacity').focus();
			  return false;
		}
		//Dong cac row con dang mo
		EquipmentManageCatalog.endEditing();
		//Khai bao cac tham so mac dinh
		var arrTT = [];
		var arrDT = [];
		var arrKH = [];
		var arrDS = [];
		
		var rows = $('#grid').datagrid('getRows');
		
		if (rows != undefined && rows != null && rows.length > 0) {
			// check them cung loai KH
			var mapCheckType = new Map();
			for (var i = 0, size = rows.length; i < size; i++) {
				var row = rows[i];
				for (var j = i - 1; j >= 0; j --) {
					var rowp = rows[j];
					//Kiem tra tinh chung chom cua Loai Khach Hang
					if (row.customerTypeId != undefined && Number(row.customerTypeId) == 0 && Number(rowp.customerTypeId) != 0) {
						$('#errMsg').html('Loại khách hàng để trống hết hoặc có giá trị hết các tất cả các dòng').show();
						return false;
					} else if (rowp.customerTypeId != undefined && Number(row.customerTypeId) != 0 && Number(rowp.customerTypeId) == 0) {
						$('#errMsg').html('Loại khách hàng để trống hết hoặc có giá trị hết các tất cả các dòng').show();
						return false;
					}
				}
				// add vao mapCheckType cac loai KH co tren grid; ko co trong mapCheckType thi put vao
				// if (Number(row.customerTypeId) == 0) {
				// 	if (mapCheckType.findIt(Number(row.customerTypeId)) == -1) {
				// 		mapCheckType.put(Number(row.customerTypeId), 'Với loại KH Chưa xác định');
				// 	}
				// } else if (Number(row.customerTypeId) == 1) {
				// 	if (mapCheckType.findIt(Number(row.customerTypeId)) == -1) {
				// 		mapCheckType.put(Number(row.customerTypeId), 'Với loại KH Thành thị');
				// 	}
				// } else if (Number(row.customerTypeId) == 2) {
				// 	if (mapCheckType.findIt(Number(row.customerTypeId)) == -1) {
				// 		mapCheckType.put(Number(row.customerTypeId), 'Với loại KH Nông thôn');
				// 	}
				// }
				if (row.customerTypeId == 0) {
					if (mapCheckType.findIt(Number(row.customerTypeId)) == -1) {
						mapCheckType.put(Number(row.customerTypeId), 'Với loại KH Chưa xác định');
					}
				} else {
					if (mapCheckType.findIt(Number(row.customerTypeId)) == -1) {
						mapCheckType.put(Number(row.customerTypeId), 'Với loại KH');
					}
				}
			}
			// kiem tra tung loai KH:
			var szMapCheckType = mapCheckType.keyArray.length;
			for (var n = 0; n < szMapCheckType; n++) {				
				//Xay dung truc so du tren truc so
				var mapMonthFromTo = new Map();
				var flagTT1 = false; // {flagTT1 = true: thoa man 1 dong co thang bat dau tu 1}
				//Tim Tu Thang Lon Nhat va Den Thang lon nhat khac rong
				var countDtRong = 0;
				var fMonthMax = 1;
				var tMonthMax = 1;
				var indexRowTMonthNull = 0;
				for (var i = 0, size = rows.length; i < size; i++) {
					//Lay doi tuong de xu ly
					var row = rows[i];
					if (Number(row.customerTypeId) == mapCheckType.keyArray[n]) {
						//Xu ly cac doi tuong ban dau
						var tMontI = Number(row.toMonth.replace(/,/g, '').trim());
						var fMontI = Number(row.fromMonth.replace(/,/g, '').trim());
						//Kiem tra tinh hop le cua dong
						var dong = i + 1;
						if (row.fromMonth == undefined || row.fromMonth == null || isNaN(row.fromMonth.replace(/,/g, '').trim()) || fMontI < 1) {
							$('#errMsg').html('Từ tháng dòng thứ ' + dong + ' không hợp lệ').show();
							return false;
						} else if (isNaN(row.toMonth) || (row.toMonth.toString().trim().length > 0 && tMontI < 1)) {
							$('#errMsg').html('Đến tháng dòng thứ ' + dong + ' không hợp lệ').show();
							return false;
						} if (isNaN(row.amount) || Number(row.amount.replace(/,/g, '').trim()) <= 0) {
							$('#errMsg').html('Dòng thứ ' + dong + ' doanh số không hợp lệ').show();
							return false;
						} else if (fMontI > 0 && tMontI > 0 && tMontI < fMontI) {
							$('#errMsg').html('Dòng thứ ' + dong + ' Đến tháng nhỏ hơn Từ tháng').show();
							return false;
						} else if (fMontI == 1){
							flagTT1 = true;
						}
						if (row.toMonth == undefined || row.toMonth == null) {
							indexRowTMonthNull = i;
							countDtRong ++;
							if (countDtRong > 1) {
								$('#errMsg').html('Không được tồn tại 2 dòng có Đến tháng để trống').show();
								return false;
							}
						}
						for (var j = i - 1; j >= 0; j --) {
							var rowp = rows[j];
							if (Number(rowp.customerTypeId) == mapCheckType.keyArray[n]) {
								var dongp = j + 1;
								var tMontJ = Number(rowp.toMonth.toString().replace(/,/g, '').trim());
								var fMontJ = Number(rowp.fromMonth.toString().replace(/,/g, '').trim());
								
								if (fMontI == 1 && fMontJ == 1) {
									if (tMontI != tMontJ && tMontI != 1 && tMontJ != 1) {
										$('#errMsg').html('Dòng thứ ' + dong + ' và dòng thứ '+ dongp + ' giao nhau và Từ tháng, Đến tháng').show();
										//$('#grid').datagrid('beginEdit', i);
										return false;
									} else if (tMontI == tMontJ ) {
										$('#errMsg').html('Dòng thứ ' + dong + ' và dòng thứ '+ dongp + ' giao nhau và Từ tháng, Đến tháng').show();
										//$('#grid').datagrid('beginEdit', i);
										return false;
									}
								} else {
									//Kiem tra Chung chom khoang Tu thang, Den thang
									if (rowp.toMonth.trim().length == 0 && row.toMonth.trim().length == 0) {
										if (fMontI <= tMontJ) {
											$('#errMsg').html('Dòng thứ ' + dong + ' và dòng thứ '+ dongp + ' giao nhau Từ tháng, Đến tháng').show();
											return false;
										}
									} else if (rowp.toMonth.trim().length == 0 || row.toMonth.trim().length == 0) {
										if (rowp.toMonth.trim().length == 0) {
											if (fMontJ <= tMontI) {
												$('#errMsg').html('Dòng thứ ' + dong + ' và dòng thứ '+ dongp + ' giao nhau Từ tháng, Đến tháng').show();
												return false;
											}
										} else {
											if (fMontI <= tMontJ) {
												$('#errMsg').html('Dòng thứ ' + dong + ' và dòng thứ '+ dongp + ' giao nhau Từ tháng, Đến tháng').show();
												return false;
											}
										}
									} else if (tMontJ <= tMontI && tMontJ >= fMontI) {
										/**
										 * den thang J <= den thang I
										 * den thang J >= tu thang I
										 * */
										$('#errMsg').html('Dòng thứ ' + dong + ' và dòng thứ '+ dongp + ' giao nhau Từ tháng, Đến tháng').show();
										return false;
									} else if (fMontJ <= tMontI && fMontJ >= fMontI) {
										/**
										 * tu thang J <= den thang I
										 * tu thang J >= tu thang I
										 * */
										$('#errMsg').html('Dòng thứ ' + dong + ' và dòng thứ '+ dongp + ' giao nhau Từ tháng, Đến tháng').show();
										return false;
									}
								} // end if (Number(rowp.customerType) == mapCheckType.keyArray[n])
							}
						}
						
						/**
						 * Xu dung Map de luu vet theo fromMoth
						 * Mac dinh cac gia tri tren da dung sau khi validate
						 * **/
						var rowValue = mapMonthFromTo.get(fMontI);
						if (rowValue != null && Number(row.fromMonth.replace(/,/g, '').trim()) == 1 && fMontI == 1) {
							if (tMontI > Number(row.toMonth.replace(/,/g, '').trim())) {
								mapMonthFromTo.put(fMontI, tMontI);
							}
						} else {
							mapMonthFromTo.put(fMontI, tMontI);
						}
					} // end if (Number(row.customerType) == mapCheckType.keyArray[n])
				} // end for (var i = 0, size = rows.length; i < size; i++) 
				if (!flagTT1) {
					$('#errMsg').html('Phải tồn tại ít nhất 1 dòng có Từ tháng bằng 1, ' + mapCheckType.get(mapCheckType.keyArray[n])).show();
					return false;
				}
				//Sap xep Tu thang theo thu tu tang dan
				var keyValArr = mapMonthFromTo.keyArray;
				if (keyValArr != null && keyValArr.length > 1) {
					var keyValArrTmp = [];
					for (var j = 0, size = keyValArr.length; j < size; j++) {
						keyValArrTmp.push(keyValArr[j]);
					}
					var arrTTTmp = sortArray.quickSort(keyValArrTmp);
					var lenthT = arrTTTmp.length - 1;
					for (var i = 0; i < lenthT; i++) {
						var tMonth = mapMonthFromTo.get(arrTTTmp[i]);
						var tMonthN = mapMonthFromTo.get(arrTTTmp[i + 1]);
						if (tMonthN == 0 && i < lenthT - 1) {
							$('#errMsg').html('Chỉ được để trống Từ tháng ở dòng đang có Từ tháng lớn nhất ' + mapCheckType.get(mapCheckType.keyArray[n])).show();
							return false;
						}
						if (arrTTTmp[i + 1] - tMonth > 1) {
							$('#errMsg').html('Từ tháng ' + arrTTTmp[i] + ' phải có Đến tháng liền kề với Từ tháng ' + arrTTTmp[i + 1] + ', ' + mapCheckType.get(mapCheckType.keyArray[n])).show();
							return false;
						}
					}
				}
			} // end for (var n = 0; n < szMapCheckType; n++)
			/**
			 * Xu ly gan cac gi tri vao bien tam
			 * */
			for (var i = 0, size = rows.length; i < size; i++) {
				var row = rows[i];
				arrTT.push(i + '_' + row.fromMonth.trim());
				if (row.toMonth.trim().length > 0) {
					arrDT.push(i + '_' + row.toMonth.trim());
				} else {
					arrDT.push(i + '_0');
				}
				if (row.customerTypeId != undefined && String(row.customerTypeId).trim().length > 0) {
					arrKH.push(i + '_' + String(row.customerTypeId).trim());
				} else {
					arrKH.push(i + '_0');
				}
				arrDS.push(i + '_' + row.amount.trim());
			}
		} // end if (rows != undefined && rows != null && rows.length > 0) 
		
		
		/**
		 * Khoi tao cac tham so
		 * */
		var dataModel = new Object();	
		dataModel.id = id;
		dataModel.code = code;
		dataModel.name = name;
		dataModel.status = status;	
		dataModel.type = type;
		dataModel.equipBrandName = equipBrandName;
		dataModel.toCapacity = toCapacity;
		dataModel.fromCapacity = fromCapacity;		
		dataModel.arrDelStr = EquipmentStockChange._lstEquipSaleplanDel.join(',');
		dataModel.arrFromMonthStr = arrTT.join(',');
		dataModel.arrToMonthStr = arrDT.join(',');
		dataModel.arrCustomerTypeStr = arrKH.join(',');
		dataModel.arrAmountStr = arrDS.join(',');
		/**
		 * Xu ly goi ham AJAX
		 * */
		var msgDialog = "Bạn có muốn Cập nhật hay không?";
		JSONUtil.saveData2(dataModel, "/equipment-group-manage/saveEquipGroup", msgDialog, "errMsg", function(data) {
			if (data.error) {
				$('#errMsg').html(data.errMsg).show();
			}else {
				$('#successMsg').html('Cập nhật dữ liệu thành công').change().show();				
				setTimeout(function(){
					$('.SuccessMsgStyle').html('').change().show();				
				}, 1000);
				window.location.href='/equipment-group-manage/info';
			  }
		},null,null);		
		return false;
	},
	
	checkValidateColumnGrid: function (dem) {
		if (dem == undefined || dem == null) {
			return '';
		} else if (dem == 1) {
			return 'Từ tháng không được trống';
		}
		return '';
	},
	
	/**
	* Kiem tra du lieu khong thay doi
	* 
	* @author hoanv25
	*/
	checkChangeDataRow: function (row) {
		var rowOld = EquipmentManageCatalog._mapEquipSaleplanNew.get(row.id);
		if (rowOld != undefined && rowOld != null && rowOld.length > 0) {
			if (rowOld.fromMonth != row.fromMonth) {
				return true;
			} else if (rowOld.toMonth != row.toMonth){
				return true;
			}else if (rowOld.customerType != row.customerType){
				return true;
			}else if (rowOld.amount != row.amount){
				return true;
			}
			//Tiep tuc cac truong co the thay doi
		}
		return false;
	},
	/**
	 * Search danh muc nhom thiet bi
	 * 
	 * @return the string
	 * @author hoanv25
	 * @since 17/12/2014
	 */
	searchEquipGroup : function() {
		var msg = '';
		if (msg.length == 0) {
			msg = Utils.getMessageOfSpecialCharactersValidate('code', 'Mã',Utils._CODE);
		}	
		if (msg.length > 0) {
			$('#errMsg').html(msg).show();
			return false;
		}
		$('#errMsg').html('').hide();	
		var code =$('#code').val().trim();
		var name =$('#name').val().trim();
		var type = $('#type').val().trim();		
		var status = $('#status').val().trim();	
		var toCapacity = $('#toCapacity').val().trim();		
		var fromCapacity = $('#fromCapacity').val().trim();	
		var equipBrandName = $('#brand').val().trim();		
		var params =  new Array();
		params.code = code;
		params.name = name;
		params.type = type;		
		params.status = status;
		params.toCapacity = toCapacity;
		params.fromCapacity = fromCapacity;
		params.equipBrandName = equipBrandName;
		if (params.toCapacity.length > 0) {
			if (!/\d$/.test(params.toCapacity)) {
				$('#errMsg').html("Dung tích đến không hợp lệ!").show();
				$('#toCapacity').focus();
				return false;
			}
		}
		if (params.fromCapacity.length > 0) {
			if (!/\d$/.test(params.fromCapacity)) {
				$('#errMsg').html("Dung tích từ không hợp lệ!").show();
				$('#fromCapacity').focus();
				return false;
			}
		}
		if (params.fromCapacity.length > 0 && params.toCapacity.length > 0) {
			var fromCapacity = parseInt(params.fromCapacity);
			var toCapacity = parseInt(params.toCapacity);
			if (fromCapacity > toCapacity) {
				$('#errMsg').html("Dung tích từ không được lớn hơn dung tích đến!").show();
				$('#toCapacity').focus();
				return false;
			}
		}
		$('#grid').datagrid('load',params);
		return false;
	},
	
	endEditing: function() {
		if (EquipmentManageCatalog._editIndex == undefined) {
			return true;
		}
		if ($('#grid').datagrid('validateRow', EquipmentManageCatalog._editIndex)){
			$('#grid').datagrid('endEdit', EquipmentManageCatalog._editIndex);
			EquipmentManageCatalog._editIndex = undefined;
			return true;
		} else {
			return false;
		}
	},
	
	removeRowOnGrid:function(index){		
    	$('#errMsgProduct').html('').hide();
    	if(index != undefined && index != null ){
	    	$.messager.confirm('Xác nhận', 'Bạn có muốn xóa dòng dữ liệu này?', function(r){
				if (r){
					$('#grid').datagrid('deleteRow', index);			
				}
	    	});
    	}
    },
    
    /**
	 * Tai tap tin excel import danh muc nhom thiet bi
	 * 
	 * @author liemtpt
	 * @since 16/03/2015
	 * @return 
	 * */
	downloadTemplateImportEquipGroup: function() {
		$('.SuccessMsgStyle').html('').hide();
		$('.ErrorMsgStyle').html('').hide();
		var msg = 'Bạn có muốn tải tập tin Import?';
		ReportUtils.exportReport('/equipment-group-manage/downloadTemplateImportEquipGroup', {}, 'errMsg');
	},
	
	/**
	 * Mo dialog nhap excel danh muc nhom thiet bi
	 * 
	 * @author liemtpt
	 * @since 16/03/2015
	 * */
	openDialogImportEquipGroup: function (){
		$('.SuccessMsgStyle').html('').hide();
		$('.ErrorMsgStyle').html('').hide();
		$('#equipHistoryTemplateDiv').css("visibility","hidden").hide();
		$('#easyuiPopupImportExcel').dialog({
			title: 'Nhập excel danh mục nhóm thiết bị',
			width: 465, 
			height: 'auto',
			closed: false,
			cache: false,
			modal: true,
			onOpen: function() {
				
			},
			onClose: function() {
				$('.ErrorMsgStyle').html('').hide();
			}
		});
	},
	/**
	 * Import excel danh muc nhom thiet bi thiet bi
	 * 
	 * @author liemtpt
	 * @since 16/03/2015
	 * */
	importEquipGroup : function(){
		$('.SuccessMsgStyle').html('').hide();
		$('.ErrorMsgStyle').html('').hide();
		
		Utils.importExcelUtils(function(data){
			EquipmentManageCatalog.searchEquipGroup();
			$('#fakefilepc').val("").change();
			if (data.message!=undefined && data.message!=null && data.message.trim().length >0){
				$('#errExcelMsg').html(data.message.trim()).change().show();
			}else{
				$('#successExcelMsg').html("Lưu dữ liệu thành công").show();
				var tm = setTimeout(function(){
					$('.SuccessMsgStyle').html('').hide();
					clearTimeout(tm);
					$('#easyuiPopupImportExcel').dialog('close');
				 }, 1500);
			}
		}, 'importFrm', 'excelFile', 'errExcelMsg');
		
	},
	
	/**
	 * Xuat excel Theo Tim kiem danh muc nhom thiet bi
	 * 
	 * @author liemtpt
	 * @since 16/03/2015 
	 * 
	 **/
	exportBySearchEquipGroup: function() {
		$('.SuccessMsgStyle').html('').hide();
		$('.ErrorMsgStyle').html('').hide();
		$('#errMsg').html('').hide();	
		$('#equipHistoryTemplateDiv').css("visibility","hidden").hide();
		var msg = '';
//		if(EquipmentListEquipment._params != null){
			var rows = $('#grid').datagrid("getRows");
			if(rows.length==0) {
				$('#errMsg').html('Không có dữ liệu xuất Excel').show();
				return false;
			}
			
			/*if(rows == null || rows.length==0){
				msg = "Không có dữ liệu xuất Excel";
			}*/
//		}else{
//			msg = "Không có dữ liệu xuất Excel";
//		}
		/*if(msg.length > 0){
			$('#errMsg').html(msg).show();
			return false;
		}*/
		var code =$('#code').val().trim();
		var name =$('#name').val().trim();
		var type = $('#type').val().trim();		
		var status = $('#status').val().trim();	
		var toCapacity = $('#toCapacity').val().trim();		
		var fromCapacity = $('#fromCapacity').val().trim();	
		var equipBrandName = $('#brand').val().trim();		
		var params =  new Object();
		params.code = code;
		params.name = name;
		params.type = type;		
		params.status = status;
		params.toCapacity = toCapacity;
		params.fromCapacity = fromCapacity;
		params.equipBrandName = equipBrandName;
		
		var msg = 'Bạn có muốn xuất Excel?';
		$.messager.confirm('Xác nhận', msg, function(r){
			if (r){
				ReportUtils.exportReport('/equipment-group-manage/exportBySearchEquipmentVO',params, 'errMsg');
			}
		});
		
	},
	/**
	 * tai file excel template
	 * @author tamvnm
	 * @since 31/05/2015
	 */
	downloadTemplate:function () {
	 	var params = new Object();
	 	ReportUtils.exportReport('/equipment-eviction-manage/download-template', params, 'errMsg');
	 },
	//phuongvm thu hoi va thanh ly
    /**
	 * in BBTHTL
	 * @author phuongvm
	 * @since 16/01/2015
	 */
	printBBTHTL: function(id){
		$('.ErrorMsgStyle').html('').hide();
		var arra = EquipmentManageCatalog._mapEviction.valArray;		
		//lay danh sach bien ban
		var lstIdRecord = new Array();
		if(id != null){			
			lstIdRecord.push(id);
		}else{
			for(var i=0,size=arra.length;i<size;++i){
				if(arra[i].trangThaiBienBan == "Đã duyệt"){
					lstIdRecord.push(arra[i].id);	
				}
			}
			if(lstIdRecord.length == 0) {
				$('#errMsg').html('Vui lòng chọn biên bản đã duyệt để xem bản in!').show();
				return;
			}	
		}		
		
		var params = new Object();
		params.lstIdRecord = lstIdRecord;
		try {
			params.reportCode = $('#function_code').val();
		} catch(e) {
			// Oop! Error
		}
		$.messager.confirm('Xác nhận','Bạn có muốn xem bản in biên bản?',function(r){
			if(r){
				ReportUtils.exportReport('/equipment-eviction-manage/print', params, 'errMsg');
			}
		});		
		return false;
	},

	searchEviction:function(){
		EquipmentManageCatalog._mapEviction = new Map();
		var err = "";
		$('#errMsg').html('').hide();
		var msg="";
		var fDate = $('#fDate').val();
		var tDate =$('#tDate').val();
		var shopKendo = $("#shop").data("kendoMultiSelect");
		var dataShop = shopKendo.dataItems();

		if(msg.length == 0){
			msg = Utils.getMessageOfSpecialCharactersValidate('equipSugEvictionCode','Mã biên bản đề nghị thu hồi');
		}

		if(msg.length == 0 && $('#fDate').val().trim().length > 0 && !Utils.isDate($('#fDate').val()) && $('#fDate').val() != '__/__/____' ){
			msg = 'Từ ngày không tồn tại hoặc không đúng định dạng dd/mm/yyyy.';
			err = '#fDate';
		}
		if(msg.length == 0 && $('#tDate').val().trim().length>0 && !Utils.isDate($('#tDate').val()) && $('#tDate').val() != '__/__/____' ){
			msg = 'Đến ngày không tồn tại hoặc không đúng định dạng dd/mm/yyyy.';
			err = '#tDate';
		}
		if(msg.length==0 && fDate.length>0 && tDate.length>0 && !Utils.compareDate(fDate, tDate)){
			msg = 'Từ ngày phải là ngày trước hoặc cùng ngày với đến ngày';
		}
		if(msg.length>0){
			$('#errMsg').html(msg).show();
			$(err).focus();
			setTimeout(function(){$('#errMsg').hide();},3000);
			return false;
		}
		var params = new Object(); 
		params.equipCode = $('#equipCode').val().trim();
		params.seri = $('#seri').val().trim();
		params.customerCode = $('#customerCode').val().trim();
		params.statusForm = $('#status').val().trim();
		params.statusAction = $('#statusDelivery').val().trim();
		params.equipSugEvictionCode = $('#equipSugEvictionCode').val().trim();
		params.toDate = tDate;
		params.fromDate = fDate;
		if (dataShop == null || dataShop.length == 0) {
			strListShopId = '';
		} else {
			var lstShopId = dataShop[0].shopId;
			for(var i = 1; i < dataShop.length; i++){
				lstShopId += "," + dataShop[i].shopId;
			}
			params.strListShopId = lstShopId;
		}		
		$('#grid').datagrid('reload',params);
		// $('#grid').datagrid('load',params);
	},
	
	/**
	 * xuat file excel thu hoi va thanh ly
	 * @author tamvnm
	 * @since 14/01/2015
	 */
	exportExcelTHTL: function(){
		$('.errMsg').html("").hide();
		var lstChecked = EquipmentManageCatalog._lstRowID;
		var rows = $('#grid').datagrid('getRows');	
		if(rows.length==0) {
			$('#errMsg').html('Không có biên bản để xuất excel').show();
			return;
		}
		if(lstChecked.length == 0) {
			$('#errMsg').html('Vui lòng chọn biên bản đã duyệt để xuất excel!').show();
			return;
		}	

		var params = new Object();
		params.lstIdRecord = lstChecked;
		$.messager.confirm('Xác nhận','Bạn có muốn xuất file excel?',function(r){
			if(r){
				ReportUtils.exportReport('/equipment-eviction-manage/export-record-eviction', params, 'errMsg');					
			}
		});		
		return false;
	},
	
	getHistory: function(idRecord){
		$('#easyuiPopupHistory').dialog({  
	        closed: false,  
	        cache: false,  
	        modal: true,
	        width : 'auto',
	        height : 'auto',
	        onOpen: function(){	       
				$('#gridHistory').datagrid({
					url : '/equipment-eviction-manage/searchHistoryEviction',
					autoRowHeight : true,
					rownumbers:true,
					fitColumns:true,
					scrollbarSize:0,
					width: $('#gridHitoryDiv').width(),
					autoWidth: true,
					queryParams:{
						id : idRecord
					},		
					columns:[[
					    {field: 'ngay',title:'Ngày',width: 100,sortable:false,resizable:false, align: 'center', formatter: function(value,row,index){
							return Utils.XSSEncode(value);
						} },					
						{field: 'trangThaiGiaoNhan', title: 'Trạng thái biên bản', width: 100, sortable:false,resizable:false,align: 'left', formatter: function(value,row,index){
							return Utils.XSSEncode(value);
						}}			    
					]],
					onBeforeLoad:function(param){											 
					},
					onLoadSuccess :function(data){   			 
				    	$('.easyui-dialog .datagrid-header-rownumber').html('STT');	    		
					}
				});
	        }
		});
	},
	updateStatus: function() {
		$('#errMsg').html('').hide();		
		var arra = EquipmentManageCatalog._mapEviction.valArray;		
		var lstId = new Array();
		if(arra == null || arra.length == 0) {
			$('#errMsg').html('Bạn chưa chọn biên bản để cập nhật. Vui lòng chọn biên bản').show();
			return;
		}		
		for(var i=0,size=arra.length;i<size;++i){
			lstId.push(arra[i].id);			
		}
		var params = new Object();
		var statusForm = $('#statusRecordLabel').val().trim();
		var statusAction = $('#statusDeliveryLabel').val().trim();
		if(statusForm<0 && statusAction<1){
			$('#errMsg').html('Vui lòng chọn trạng thái').show();
			return;
		}
		params.lstId = lstId;
		params.statusAction = statusAction;
		params.statusForm = statusForm;
		Utils.addOrSaveData(params, '/equipment-eviction-manage/update', null, 'errMsg', function(data) {
			if(data.lstErr!=undefined && data.lstErr!=null && data.lstErr.length>0){
				VCommonJS.showDialogList({
				    data : data.lstErr,
				    columns : [[
				        {field:'formCode', title:'Mã biên bản', align:'left', width: 110, sortable:false, resizable:false},
				        {field:'formStatusErr', title:'Trạng thái biên bản', align:'center', width: 110, sortable:false, resizable:false, formatter: function(value,row,index){
				        	var html="";
							if(value == 1){
								html = '<img src="/resources/images/icon_delete.png" height="20" width="20" style="margin-right:5px;" title="Error">';
							}
							return html;
						}},
						 {field:'deliveryStatusErr', title:'Trạng thái giao nhận', align:'center', width: 200, sortable:false, resizable:false, formatter: function(value,row,index) {
				        	var html="";
							if(value == 1){
								html = '<img src="/resources/images/icon_delete.png" height="20" width="20" style="margin-right:5px;" title="Error">';
							}
							return html;
						} },
						{field:'recordNotComplete', title:'Chi tiết biên bản', align:'center', width:80, sortable:false, resizable:false, formatter: function(value, row, index) {
								return VTUtilJS.XSSEncode(value);
						}}			    
					]]
				});	
			}
			EquipmentManageCatalog._mapEviction = new Map();
			$('#grid').datagrid('reload');
			setSelectBoxValue('statusRecordLabel', -1);
			setSelectBoxValue('statusDeliveryLabel', 0);
		}, null, true,null, 'Bạn có muốn cập nhật biên bản?');
		
	},
	showDlgImportExcel: function(){
		$('#easyuiPopupImportExcel').show();
		$('.ErrorMsgStyle').hide();
		$('#easyuiPopupImportExcel').dialog({  
	        closed: false,  
	        cache: false,  
	        modal: true,
	        onOpen: function(){	 
	        	$('.easyui-dialog .BtnGeneralStyle').css('color','');
	        },
	        onClose: function(){
	        	EquipmentManageCatalog.searchEviction();
				$("#grid").datagrid('uncheckAll');
				$("#fakefilepcBBGN").val("");
				$("#excelFileBBGN").val("");				
	        }
		});
	},
	importExcelBBTHTL: function(){
		$('.ErrorMsgStyle').hide();
		VTUtilJS.importExcelUtils(function(data) {
			$("#errExcelMsgBBGN").html(data.message).show();		
			if(data.fileNameFail == null || data.fileNameFail == "" ){					
				setTimeout(function(){
					$('#errExcelMsgBBGN').html("").hide();
					$('#easyuiPopupImportExcel').dialog('close');
				}, 3000);				
			}else{
				$("#fakefilepcBBGN").val("");
				$("#excelFileBBGN").val("");			
				$('#grid').datagrid('reload');	
			}						
		}, "importFrmBBGN", "excelFileBBGN", null, "errExcelMsgBBGN");
		return false;
	},
	/**
	 * Chon don vi
	 * @author phuongvm
	 * @since 21/12/2014
	 */
	selectShop: function(id,code,name){
		$('#shopCode').val(code).change();	
		$('#customerName').val('');	
		$('#customerCode').val('');
		$('#address').val('');		
		$('#phone').val('');	
		$('.easyui-dialog').dialog('close');
		$('#shopCode').focus();	
		EquipmentManageCatalog._curShopCode = code;
	},
	/**
	 * Chon don vi
	 * @author phuongvm
	 * @since 21/12/2014
	 */
	selectNewShop: function(id,code,name){
		$('#newShopCode').val(code).change();
		$('#newCustomerCode').val('');
		$('#newAddress').val('');	
		$('.easyui-dialog').dialog('close');
		$('#newShopCode').focus();	
	},
	/**
	 * chon khach hang moi tu popup khach hang
	 * 
	 * @return the string
	 * @author phuongvm
	 * @since 21/12/2014
	 */
	selectNewCustomer: function(shortCode,customerName,address,phone){		
		$('#newCustomerCode').val(shortCode);
		$('#newAddress').val(address);		
		$('.easyui-dialog').dialog('close');
		$('#newCustomerCode').focus();	
	},
	/**
	 * chon khach hang tu popup khach hang dong thoi lay danh sach thiet bi trong bien ban giao nhan
	 * 
	 * @return the string
	 * @author phuongvm
	 * @since 20/12/2014
	 */
	selectCustomer: function(shortCode,customerName,address,phone){		
		$('#customerCode').val(shortCode);		
		$('#customerName').val(customerName);		
		$('#address').val(address);		
		$('#phone').val(phone);		
		EquipmentManageCatalog.eventselectCustomer(shortCode);
		$('.easyui-dialog').dialog('close');
		EquipmentManageCatalog._isPressF9Customer = false;
		EquipmentManageCatalog._customerCode = shortCode;
	},
	/**
	 * Xu ly su kien khi chon khach hang tu popup khach hang
	 * 
	 * @author phuongvm
	 * @since 20/12/2014
	 * */
	eventselectCustomer: function (shortCode,idEviction) {
		$('.ErrorMsgStyle').html('').hide();
		$('.SuccessMsgStyle').html('').hide();
		EquipmentManageCatalog._rowDgTmp = {};
		var id = -1;
		if(idEviction!=undefined && idEviction!=null){
			id = idEviction;
		}
		Utils.getJSONDataByAjaxNotOverlay ({customerCode: shortCode,shopCode:$('#shopCode').val().trim(),id:id}, '/equipment-eviction-manage/getListEquipInCustomer', function (data){
			EquipmentManageCatalog._mapEquip = new Map();
			EquipmentManageCatalog._mapDetail = new Map();
			EquipmentManageCatalog._lstEquipAvailable = new Array();
			if (data != undefined && data != null && data.rows != undefined && data.rows != null && data.rows.length >0) {
				if(idEviction!=undefined && idEviction!=null){
					if(data.lstEquipId != undefined && data.lstEquipId != null && data.lstEquipId.length >0){
						for (var i = 0; i < data.rows.length; i++) {
							EquipmentManageCatalog._mapEquip.put(data.rows[i].id, data.rows[i]);
//							EquipmentManageCatalog._lstEquipAvailable.push(data.rows[i]);
						}
//			   		 	for (var j = data.lstEquipId.length - 1; j >= 0; j--) {
			   		 	for (var j = 0; j < data.lstEquipId.length; j++) {	
			   		 		var prdId = data.lstEquipId[j];
			   				EquipmentManageCatalog._mapDetail.put(prdId,EquipmentManageCatalog._mapEquip.get(prdId));
//			   				EquipmentManageCatalog._mapEquip.remove(prdId);
			   			}
			   		 	for (var k = 0; k< EquipmentManageCatalog._mapEquip.keyArray.length; k++) {
			   		 		var idE = EquipmentManageCatalog._mapEquip.keyArray[k];
			   		 		var row = EquipmentManageCatalog._mapEquip.get(idE);
			   		 		EquipmentManageCatalog._lstEquipAvailable.push(row);
			   			}
					}
				}else{
					for (var i = 0; i < data.rows.length; i++) {
						EquipmentManageCatalog._mapEquip.put(data.rows[i].id, data.rows[i]);
						EquipmentManageCatalog._lstEquipAvailable.push(data.rows[i]);
					}
				}
			}
			$('#grid').datagrid({
				data: EquipmentManageCatalog.getDataInPrdStockOutputDg(),
				width :  $('#gridContainer').width() - 15,
				fitColumns : true,
			    rownumbers : true,
			    singleSelect:true,
			    height: 275,
			    columns:[[
				    {field:'soHopDong', title:'Số hợp đồng', width:120, align:'left', formatter: function(value, row, index){
				    	var prdName = 'dgSoHopDong'+ row.id;
				    	return '<label id="'+prdName+'" style="width: 100%; text-align: left;">'+Utils.XSSEncode(value)+'</label>';
				    }},
				    {field:'loaiThietBi',title:'Loại thiết bị', width:80,align:'left', formatter: function(value, row, index){
				    	var idLot = "dgLoaiThietBi"+ row.id;
				    	return '<label id="'+idLot+'" style="width: 100%; text-align: left;">'+Utils.XSSEncode(value)+'</label>';
				    }},
				    {field:'nhomThietBi', title:'Nhóm thiết bị', width:80, align:'left', formatter: function(value, row, index){
				    	var idAvlQ = "dgNhomThietBi"+ row.id;
				    	return '<label id="'+idAvlQ+'" style="width: 100%; text-align: left;">'+Utils.XSSEncode(value)+'</label>';
				    }},
				    {field:'maThietBi',title:'Mã thiết bị', width:120,align:'left',formatter: function(value, row, index){
						return Utils.XSSEncode(value);
					}
					, editor: {
						type:'combobox',
							options:{ 
							valueField:'id',
						    textField:'maThietBi',
						    data: EquipmentManageCatalog._lstEquipAvailable,
						    width: 200,
						    formatter: function(row) {
						    	return '<span style="font-weight:bold">' + row.maThietBi  + '</span>';
						    },
						    onSelect:function(rec){
						    	EquipmentManageCatalog._rowDgTmp = {
					    			id: rec.id,
									maThietBi: rec.maThietBi,
									soHopDong: rec.soHopDong,
									loaiThietBi: rec.loaiThietBi,
									nhomThietBi: rec.nhomThietBi,
									soSeri: rec.soSeri,
									soLuong: rec.soLuong,
									tinhTrangThietBi: rec.tinhTrangThietBi
						    	};
						    	var soHopDong = '';
						    	if(rec.soHopDong!=null){
						    		soHopDong = rec.soHopDong.trim();
						    	}
						    	var soSeri = '';
						    	if(rec.soSeri!=null){
						    		soSeri = rec.soSeri.trim();
						    	}
						    	$('#dgSoHopDong0').html(Utils.XSSEncode(soHopDong)).change();
						    	$('#dgLoaiThietBi0').html(Utils.XSSEncode(rec.loaiThietBi)).change();
						    	$('#dgNhomThietBi0').html(Utils.XSSEncode(rec.nhomThietBi.trim())).change();
						    	$('#dgSoSeri0').html(Utils.XSSEncode(soSeri)).change();
						    	$('#dgSoLuong0').html(Utils.XSSEncode(rec.soLuong)).change();
						    	$('#dgTinhTrangThietBi0').html(Utils.XSSEncode(rec.tinhTrangThietBi.trim())).change();
						    	
						    	EquipmentManageCatalog._mapDetail.put(EquipmentManageCatalog._rowDgTmp.id, EquipmentManageCatalog._rowDgTmp);
						    	$('#grid').datagrid("loadData", EquipmentManageCatalog.getDataInPrdStockOutputDg());
						    	try {
						    		$('.combo-panel').each(function(){
										if(!$(this).is(':hidden')){
											$(this).css('display','none');
										}			
									});
						    	} catch(err)  {
						    		//nothing.
						    	}
						    	
						    },
						    onChange:function(pCode){
//						    	$('.combo-panel').each(function(){
//									if(!$(this).is(':hidden')){
//										$(this).css('display','none');
//									}			
//								});
						    },
						    filter: function(q, row){
								q = new String(q).toUpperCase();
								var opts = $(this).combobox('options');
								return row[opts.textField].indexOf(q)>=0;
							}
						}
					}},
				    {field:'soSeri', title:'Số serial', width:100, align:'left', formatter: function(value, row, index){
				    	var idQuan = "dgSoSeri"+ row.id;
				    	return '<label id="'+idQuan+'" style="width: 100%; text-align: left;">'+Utils.XSSEncode(value)+'</label>';
				    }},
				    {field:'soLuong', title:'Số lượng', width:80, align:'right', formatter: function(value, row, index){
				    	var idPrice= "dgSoLuong"+ row.id;
				    	return '<label id="'+idPrice+'" style="width: 100%; text-align: right;">'+Utils.XSSEncode(value)+'</label>';
				    }},
				    {field:'tinhTrangThietBi', title:'Tình trạng thiết bị', width:80, align:'left', formatter: function(value, row, index){
				    	var idAmount = "dgTinhTrangThietBi"+ row.id;
				    	return '<label id="'+idAmount+'" style="width: 100%; text-align: left;">'+Utils.XSSEncode(value)+'</label>';
				    }},
				    {field:'detelte',title:'',width:50, align:'center', formatter: function(value, row, index){
				    	if (row.id != undefined && row.id != null && row.id > 0 && ($('#equipSugEvictionCode').val() == undefined ||  $('#equipSugEvictionCode').val() == '')) { 
				    		return '<a href="javascript:void(0)" id="dg_prdStockOutputDg_delete" onclick="return EquipmentManageCatalog.deleteRowInPrdStockOutputDg('+row.id+');"><img title="Xóa" src="/resources/images/icon_delete.png" width="15" heigh="15"></a>';
				    	}
				    }}
				]],
				onClickRow: function(rowIndex, rowData) {
					
				},
				onAfterEdit: function(rowIndex, rowData, changes) {
				},
				onBeforeEdit : function(rowIndex, rowData, changes){
				},
		        onLoadSuccess :function(data){
			    	 $('.datagrid-header-rownumber').html('STT');
			    	 Utils.updateRownumWidthAndHeightForDataGrid('grid');
			    	 //Xu ly cho hien thi
			    	 var arrInput = '';
			    	 EquipmentManageCatalog.getDataComboboxInPrdStockOutputDg();
			    	 var rows = $('#grid').datagrid('getRows');
			    	 if ($('#equipSugEvictionCode').val() != "") {
			    		$('#grid').datagrid("hideColumn", "detelte");
			    	 } else if(rows == null || rows.length == 0 || rows[rows.length-1].id != null){
			    		 EquipmentManageCatalog.insertRowStockTransDetailDg();
			    	 }
			    	
			    	 
			    	 
			    	 if (EquipmentManageCatalog._mapDetail != null && EquipmentManageCatalog._mapDetail.size() > 0) {
//			    		 for(var i=0; i < EquipmentManageCatalog._mapDetail.keyArray.length; i++){
//			    			 var productTmp = EquipmentManageCatalog._mapDetail.get(EquipmentManageCatalog._mapDetail.keyArray[i]);
//				    		 sumAmountRow += productTmp.amount;
//				    		 weight += productTmp.grossWeight;
//				    	 }
			    	 } else {
			    		 EquipmentManageCatalog._mapDetail = new Map();
			    	 }
			    	$('.combobox-f.combo-f').combobox("loadData", EquipmentManageCatalog._lstEquipAvailable);
			    	if ($('#equipSugEvictionCode').val() != "") {
			    		$('#grid').datagrid("hideColumn", "detelte");
			    	}
		    	}
			});
		}, null, null);
	},
	/**
	 * Lay du lieu cho PrdStockOutputDg
	 * 
	 * @author phuongvm
	 * @since 20/12/2014
	 * */
	getDataInPrdStockOutputDg : function() {
		var rows = [];
		if ($('#equipSugEvictionCode').val() != "") {
			if (EquipmentManageCatalog._mapDetail != null || EquipmentManageCatalog._mapDetail.size() > 0){
				for (var i=0; i< EquipmentManageCatalog._mapDetail.keyArray.length; i++) {
					rows.push(EquipmentManageCatalog._mapDetail.get(EquipmentManageCatalog._mapDetail.keyArray[i]));
					rows[i].equipSugEvictionCode = $('#equipSugEvictionCode').val();
				}
			}
		} else {
			if (EquipmentManageCatalog._mapDetail != null || EquipmentManageCatalog._mapDetail.size() > 0){
				for (var i=0; i< EquipmentManageCatalog._mapDetail.keyArray.length; i++) {
					rows.push(EquipmentManageCatalog._mapDetail.get(EquipmentManageCatalog._mapDetail.keyArray[i]));
				}
			}
		}

		
		return rows;
	},
	/**
	 * Lay danh sach thiet bi sau loai tru trong Grid
	 * 
	 * @author phuongvm
	 * @since 21/12/2014
	 * */
	getDataComboboxInPrdStockOutputDg: function () {
		if (EquipmentManageCatalog._mapDetail != null && EquipmentManageCatalog._mapDetail.size() > 0) {
   		 	for (var i = 0; i < EquipmentManageCatalog._mapDetail.keyArray.length; i++) {
	   		 	for (var j = EquipmentManageCatalog._lstEquipAvailable.length - 1; j >= 0; j--) {
	   		 		var prdId = EquipmentManageCatalog._mapDetail.keyArray[i];
	   				if (EquipmentManageCatalog._lstEquipAvailable[j].id == prdId) { 
	   					EquipmentManageCatalog._lstEquipAvailable.splice(j, true);
	   				}
	   			}
	    	}
   	 	}
	},
	/**
	 * Xoa mot dong thiet bi
	 * 
	 * @author phuongvm
	 * @since 20/12/2014
	 * */
	deleteRowInPrdStockOutputDg: function (id) {
		if (id == null || id == null || id == 0) {
			return;
		}
		if (EquipmentManageCatalog._mapDetail.keyArray.indexOf(id) > -1) {
			for (var i = 0; i< EquipmentManageCatalog._mapEquip.keyArray.length; i++) {
				if (EquipmentManageCatalog._mapEquip.keyArray[i] == id) {
					EquipmentManageCatalog._lstEquipAvailable.push(EquipmentManageCatalog._mapEquip.get(id));
					//Sap xep lai
		   		 	var arrSort = [];
		   		 	for (var i = 0; i < EquipmentManageCatalog._lstEquipAvailable.length; i++) {
		   		 		var value = EquipmentManageCatalog._lstEquipAvailable[i].maThietBi + '_-_-_' + EquipmentManageCatalog._lstEquipAvailable[i].id;
		   		 		arrSort.push(value.trim());
					}
		   		 	//lenh sap xep Order by ASC
		   		 	arrSort = arrSort.sort();
		   		 	//sap xep lai mang
		   		 	var data = [];
		   		 	for (var t = 0; t < arrSort.length; t++) {
		   		 		for (var i = 0; i < EquipmentManageCatalog._lstEquipAvailable.length; i++) {
		   		 			if(Number(arrSort[t].split('_-_-_')[1]) == EquipmentManageCatalog._lstEquipAvailable[i].id) {
		   		 				data.push(EquipmentManageCatalog._lstEquipAvailable[i]);
		   		 			}
		   		 		}
		   		 	}
		   		 	//gan lai mang vua sap xep 
		   		 	EquipmentManageCatalog._lstEquipAvailable = new Array();
		   		 	for (var t = 0; t < data.length; t++) {
		   		 		EquipmentManageCatalog._lstEquipAvailable.push(data[t]);		   		 		
		   		 	}
					break;
				}
			}
			EquipmentManageCatalog._mapDetail.remove(id);
			
			$('#grid').datagrid("loadData", EquipmentManageCatalog.getDataInPrdStockOutputDg());
		} else {
			$.messager.alert('Lỗi dữ liệu','Không tìm thấy id thiết bị muốn xóa.','error');
		}
	},
	/**
	 * Them moi 2 dong trong vao Datagrid 
	 * 
	 * @author phuongvm
	 * @Since 20/12/2014
	 * */
	insertRowStockTransDetailDg: function () {
		var indexMax = $('#grid').datagrid('getRows').length;
		$('#grid').datagrid('insertRow',{
			index: indexMax,
			row: {
				id: 0,
				maThietBi: '',
				soHopDong: '',
				loaiThietBi: '',
				nhomThietBi: '',
				soSeri: '',
				soLuong: '',
				tinhTrangThietBi: ''
			}
		});
		EquipmentManageCatalog._rowDgTmp = {
    			id: 0,
				maThietBi: '',
				soHopDong: '',
				loaiThietBi: '',
				nhomThietBi: '',
				soSeri: '',
				soLuong: '',
				tinhTrangThietBi: ''
	    	};
		$('#grid').datagrid('selectRow', indexMax).datagrid('beginEdit', indexMax);
	},
	/**
	 * Xoa tap tin dinh kem
	 * 
	 * @author hunglm16
	 * @since Feb 10,214
	 * */
	removeEquipAttachFile: function(id){
		if (id == undefined || id == null || isNaN(id)) {
			return;
		}
		if (EquipmentManageCatalog._arrFileDelete == null || EquipmentManageCatalog._arrFileDelete == undefined) {
			EquipmentManageCatalog._arrFileDelete = [];
		}
		EquipmentManageCatalog._arrFileDelete.push(id);
		$('#divEquipAttachFile'+id).remove();
		if (EquipmentManageCatalog._countFile != undefined && EquipmentManageCatalog._countFile != null && EquipmentManageCatalog._countFile > 0) {
			EquipmentManageCatalog._countFile = EquipmentManageCatalog._countFile - 1;
		} else {
			EquipmentManageCatalog._countFile = 0;
		}
	},
	/**
	 * Lap bien ban thu hoi thanh ly
	 * 
	 * @author phuongvm
	 * @Since 22/12/2014
	 * */
	saveEviction : function(){
		$(".ErrorMsgStyle").html('').hide();
		var msg = "";
		var err = ""; 
		var arrDataInsert = [];
		var createDate = $('#createDate').val();
		msg = Utils.getMessageOfRequireCheck('shopCode','Mã NPP');
		err = '#shopCode';
		if(msg.length == 0){
			msg = Utils.getMessageOfSpecialCharactersValidate('shopCode','Mã NPP',Utils._CODE);
			err = '#shopCode';
		}
		if(msg.length == 0 && $('#createDate').val().trim().length > 0 && !Utils.isDate($('#createDate').val()) && $('#createDate').val() != '__/__/____' ){
			msg = 'Ngày biên bản không tồn tại hoặc không đúng định dạng dd/mm/yyyy.';
			err = '#createDate';
		}

		var curDate = ReportUtils.getCurrentDateString(); //Lay ngay hien tai sysDate
		if (msg.length == 0 && !Utils.compareDate($('#createDate').val().trim(), curDate)) {
			msg = 'Ngày biên bản không được sau ngày hiện tại. Vui lòng nhập lại';
			$('#createDate').focus();
		}

		if(msg.length == 0){
			msg = Utils.getMessageOfRequireCheck('customerCode','Mã KH');
			err = '#customerCode';
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfSpecialCharactersValidate('customerCode','Mã KH',Utils._CODE);
			err = '#customerCode';
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfRequireCheck('fromRePresentor','Người đại diện');
			err = '#fromRePresentor';
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfSpecialCharactersValidate('fromRePresentor','Người đại diện', Utils._NAME); 
			err = '#fromRePresentor';
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfRequireCheck('toRePresentor','Bên cho mượn');
			err = '#toRePresentor';
		}
		if (msg.length == 0) {
		 	msg = Utils.getMessageOfSpecialCharactersValidate('toRePresentor', 'Bên cho mượn', Utils._NAME); 
		 	err = '#toRePresentor';
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfRequireCheck('warehouse','Kho nhập thu hồi');
			err = '#warehouse';
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfSpecialCharactersValidate('warehouse','Kho nhập thu hồi',Utils._CODE);
			err = '#warehouse';
		}
		if (msg.length == 0) {
		 	msg = Utils.getMessageOfSpecialCharactersValidate('note', 'Ghi chú', Utils._NAME); 
		 	err = '#note';
		}
		// if(msg.length == 0){
		// 	if($("input[name='currentPosition']:checked").val() == '3'){
		// 		if(msg.length == 0){
		// 			msg = Utils.getMessageOfRequireCheck('newShopCode','Mã NPP mới');
		// 			err = '#newShopCode';
		// 		}
		// 		if(msg.length == 0){
		// 			msg = Utils.getMessageOfSpecialCharactersValidate('newShopCode','Mã NPP mới',Utils._CODE);
		// 			err = '#newShopCode';
		// 		}
		// 		if(msg.length == 0){
		// 			msg = Utils.getMessageOfRequireCheck('newCustomerCode','Mã KH mới');
		// 			err = '#newCustomerCode';
		// 		}
		// 		if(msg.length == 0){
		// 			msg = Utils.getMessageOfSpecialCharactersValidate('newCustomerCode','Mã KH mới',Utils._CODE);
		// 			err = '#newCustomerCode';
		// 		}
		// 	}
		// }
		// var newCus = $('#newCustomerCode').val().trim();
		var cus = $('#customerCode').val().trim();
		var shop = $('#shopCode').val().trim();
		// var newShop = $('#newShopCode').val().trim();
		// if(msg.length == 0){
		// 	if(cus.toLowerCase() == newCus.toLowerCase() && shop.toLowerCase() == newShop.toLowerCase()){
		// 		msg = "Mã KH mới không được trùng với Mã KH";
		// 		err = '#newCustomerCode';
		// 	}
		// }
		if (msg.length == 0) {
			if (EquipmentManageCatalog._mapDetail.size() == 0) {
				msg = "Chưa có thiết bị nào được chọn";
			} else {
				for (var i = 0; i< EquipmentManageCatalog._mapDetail.keyArray.length; i++) {
					var equip = EquipmentManageCatalog._mapDetail.get(EquipmentManageCatalog._mapDetail.keyArray[i]);
					if (equip.id > 0) {
						var paramsPrd = equip.id;
						arrDataInsert.push(paramsPrd);
					}
				}
			}
		}
		if (msg.trim().length == 0 && arrDataInsert.length == 0) {
			msg = "Chưa có thiết bị nào được chọn";
		}
		if (msg.trim().length > 0) {
			$('#errMsg').html(msg).show();
			return false;
		}
		var params = new Object();
		params.shopCode = $('#shopCode').val().trim();
		params.customerCode = $('#customerCode').val().trim();
		params.fromRePresentor = $('#fromRePresentor').val().trim();
		params.toRePresentor = $('#toRePresentor').val().trim();
		params.stockCode = $('#warehouse').val().trim(); // ma kho
		params.resultBB = $("#reason").val();//ly do thu hoi
		// params.viTri = $("input[name='currentPosition']:checked").val();
		// if(params.viTri == '3'){
		// 	params.newShopCode = $('#newShopCode').val().trim();
		// 	params.newCustomerCode = $('#newCustomerCode').val().trim();
		// }
		params.lstId = arrDataInsert;
		params.status = $('#status').val().trim();
		params.statusDelivery = $('#statusDelivery').val().trim();
		params.createDate = createDate;
		params.note = $('#note').val();
		if($('#idForm').val() !='' && $('#idForm').val() != '0'){
			if(idForm!=null){
				params.idForm = $('#idForm').val(); 
			}
		}
		if (EquipmentManageCatalog._arrFileDelete == undefined || EquipmentManageCatalog._arrFileDelete == null) {
			EquipmentManageCatalog._arrFileDelete = [];
		}
		params.equipAttachFileStr = EquipmentManageCatalog._arrFileDelete.join(',');
		
		var msg = "Bạn có muốn lưu biên bản thu hồi thanh lý?";
		var arrFile = DropzonePlugin._dropzoneObject.getFilesWithStatus(Dropzone.ADDED);
		if (arrFile != null && arrFile != undefined && arrFile.length > 0) {
			if (EquipmentManageCatalog._countFile == 5) {
				$('#errMsg').html("Tổng số tập tin đính kèm không được vượt quá 5").show();
				return false;
			}
			var maxLength = 5 - EquipmentManageCatalog._countFile;
			if (arrFile.length > maxLength) {
				$('#errMsg').html("Số tập tin thêm mới không được vượt quá " + maxLength).show();
				return false;
			}
			msg = UploadUtil.getMsgByFileRequireCheck(UploadUtil._typeFileRequireCheck.equipRecord);
			if (msg.trim().length > 0) {
				$('#errMsg').html(msg).show();
				return false;
			}
			$.messager.confirm('Xác nhận', 'Bạn có muốn lưu dữ liệu?', function(r){
				if (r){
					var data = JSONUtil.getSimpleObject2(params);
					UploadUtil.updateAdditionalDataForUpload(data);
					UploadUtil.startUpload('errMsg');
					return false;
				}
			});
			return false;
		}
		
		Utils.addOrSaveData(params, "/equipment-eviction-manage/saveEviction", null, 'errMsg', function(data) {
			if (data.error != undefined && !data.error) {
				$("#successMsg").html("Lưu dữ liệu thành công").show();
				setTimeout(function(){
					window.location.href = '/equipment-eviction-manage/info';
				}, 1500);				
			}else{
				if(data.errMsg!=undefined && data.errMsg!= null && data.errMsg!=''){
					$('#errMsg').html(data.errMsg).show();
				}
			}			
		}, null, null, null, msg);
	},
	/**
	 * Chon kho
	 * @author phuongvm
	 * @since 22/12/2014
	 */
	selectStock: function(shortCode){
		$('#warehouse').val(shortCode);		
		$('#easyuiPopupSearchStock').dialog('close');
	},
	exportExcel : function(notificationId){
		$('#errMsg').html('').hide();
		$.messager.confirm('Xác nhận','Bạn có muốn xuất file?',function(r){
			if(r){	
				var dataModel = new Object();
				dataModel.notificationId = notificationId;
				ReportUtils.exportReport('/adsprogram/notification/exportExcel',dataModel,'errMsg');									
				}			
		});	
 		return false;

	},
	importExcel : function(){
		//mo fancybox
		$('#errMsg').html('').hide();
		$('#easyuiPopup').show();
		$('#errExcelMsg').html('').hide();
		$('#resultExcelMsg').html('').hide();
		ReportUtils._callBackAfterImport = function(){
			if (EquipmentManageCatalog.isImportSuccess){
				 $('.easyui-dialog #btnClose').unbind('click');
	   			 $('.easyui-dialog #btnClose').bind('click',function(){
	   				 $('#grid').datagrid('reload');
	   				 $('#easyuiPopup').dialog('close');
	   			 });
	   		 }else{
	   			$('.easyui-dialog #btnClose').unbind('click');
	   			 $('.easyui-dialog #btnClose').bind('click',function(){
	   				$('#grid').datagrid('reload');
	   				 $('#easyuiPopup').dialog('close');
	   			 });
	   		 }
			$('#grid').datagrid('reload');
			ReportUtils._callBackAfterImport=null;
		};
		$('#easyuiPopup').dialog({  
	        closed: false,  
	        cache: false,  
	        modal: true,
	        onOpen: function(){
	        	$('#contentTxtDl').val('');
	        	$('#contentTxtDl').focus();
	        	$('#descriptionDl').val('');
	        	$('#fromHour').val('09').change();
	        	$('#toHour').val('09').change();
	        	$('#fromMinute').val('00').change();
	        	$('#toMinute').val('00').change();
	        	$('#excelFile').val('');
				$('#fakefilepc').val('');
	        	$('#checkLogin').attr('checked',false);
	        	$('#checkUploadDB').attr('checked',false);
	        	$('.easyui-dialog #downloadTemplate').attr('href',excel_template_path + 'promotion/Bieu_mau_thong_bao_MTB.xls');
	        	var tabindex = -1;
				$('.InputTextStyle, select, input, button , li , tr, td, th, label, ul, a, img').each(function () {
					if (this.type != 'hidden') {
						$(this).attr("tabindex", tabindex);
						tabindex -=1;
					}
				});
				tabindex = 1;
	    		 $('.easyui-dialog input,.easyui-dialog select,.easyui-dialog button').each(function () {
		    		 if (this.type != 'hidden') {
			    	     $(this).attr("tabindex", '');
						 tabindex++;
		    		 }
				 });
	        },
	        onClose:function(){
	        	$('#contentTxt').focus();
	        	var tabindex = 1;
				$('.InputTextStyle, select, input, button , li , tr, td, th, label, ul, a, img').each(function () {
					if (this.type != 'hidden') {
						$(this).attr("tabindex", tabindex);
						tabindex ++;
					}
				});
	        }
		});
		
	},
	upload : function(){ //upload trong fancybox
		$('#resultExcelMsg').html('').hide();
		$('#errExcelMsg').html('').hide();
		var msg="";
		var err="";
		var fDate = $('#fromDateDl').val().trim();	
		var content = $('#contentTxtDl').val().trim();
		var description = $('#descriptionDl').val().trim();
		if(msg.length == 0){
			msg = Utils.getMessageOfRequireCheck('contentTxtDl','Nội dung');
			err = '#contentTxtDl';
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfRequireCheck('descriptionDl','Mô tả');
			err = '#descriptionDl';
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfRequireCheck('fromDateDl','Ngày');
			err = '#fromDateDl';
		}
		if(msg.length == 0 && $('#fromDateDl').val().trim().length>0 && !Utils.isDate($('#fromDateDl').val(),'/')){
			msg = 'Ngày không tồn tại hoặc không đúng định dạng dd/mm/yyyy.';
			err = '#fromDateDl';
		}
		if(msg.length == 0) {
			msg = Utils.compareCurrentDateEx2('fromDateDl','Ngày');
			if(msg.length !=0){
				err = '#fromDateDl';
			}
		}
		if(msg.length>0){
			$('#errExcelMsg').html(msg).show();
			$(err).focus();
			setTimeout(function(){$('#errMsg').hide();},3000);
			return false;
		}
		if($('#excelFile').val() ==  ""){
			$('#errExcelMsg').html("Vui lòng chọn file Excel").show();
			return false;
		}
		var isLogin= 0;
		var isUploadDB= 0;
		if($('#checkLogin').is(':checked')){
			isLogin =1;
		}
		if($('#checkUploadDB').is(':checked')){
			isUploadDB =1;
		}
		var fH= Number($('#fromHour').val().trim()) *60 + Number($('#fromMinute').val().trim());
		var tH= Number($('#toHour').val().trim()) *60 + Number($('#toMinute').val().trim());
		if(fH>tH){
			$('#errExcelMsg').html("Từ giờ phải nhỏ hơn hoặc bằng đến giờ.").show();
			return false;
		}
		var fromH = $('#fromHour').val().trim()+':'+$('#fromMinute').val().trim();
		var toH = $('#toHour').val().trim()+':'+$('#toMinute').val().trim();
		fDate = $('#fromDateDl').val().trim();	
		var options = { 
				beforeSubmit: ReportUtils.beforeImportExcel,   
		 		success:      EquipmentManageCatalog.afterImportExcel, 
		 		type: "POST",
		 		dataType: 'html',
		 		data:({isUploadDB:isUploadDB,isLogin:isLogin,fromDate:fDate,content:content,description:description,fromH:fromH,toH:toH})
		 	}; 
		$('#easyuiPopup #importFrm').ajaxForm(options);
		$.messager.confirm('Xác nhận','Bạn có muốn nhập từ file?',function(r){
			if(r){				
				$('#easyuiPopup #importFrm').submit();
			}
		});		
 		return false;
	},
	afterImportExcel: function(responseText, statusText, xhr, $form){		
		$('#divOverlay').hide();
		if(('#staffGrid').length>0){
			$('#staffGrid').datagrid('reload');
		}
		if (statusText == 'success') {				
	    	$("#responseDiv").html(responseText);
	    	updateTokenImport();
	    	if($('#errorExcel').html().trim() == 'true' || $('#errorExcelMsg').html().trim().length > 0){
	    		$('.panel-body #errExcelMsg').html($('#errorExcelMsg').html()).show();
	    	} else {
    			var totalRow = parseInt($('#totalRow').html().trim());
    			var numFail = parseInt($('#numFail').html().trim());
    			var fileNameFail = $('#fileNameFail').html();
    			var mes = 'Import không thành công do có '+numFail+' dòng lỗi';	    		
    			if(numFail > 0){
    				mes+= ' <a href="'+ fileNameFail +'">Xem chi tiết lỗi</a>';
    			}else{
    				mes = 'Lưu dữ liệu thành công';
    			}
    			if(numFail==0){
    				$('#resultExcelMsg').removeClass('ErrorMsgStyle SpriteErr').addClass('SuccessMsgStyle');
    				EquipmentManageCatalog.isImportSuccess = true;
    				$('#easyuiPopup').window('close');
    				$('#successMsg').html(mes).show();
    				EquipmentManageCatalog.search();
    				setTimeout(function(){
    					$('#successMsg').html('').hide();
					}, 3000);
    			}else{
    				$('#resultExcelMsg').removeClass('SuccessMsgStyle').addClass('ErrorMsgStyle SpriteErr');
    				if(numFail != totalRow) EquipmentManageCatalog.isImportSuccess = true;
    			}
    			if($('#excelFile').length!=0 && $('#fakefilepc').length!=0){//xóa link khi import
    				try{
    					$('#excelFile').val('');
    					$('#fakefilepc').val('');
    				}catch(err){}
    			}
    			
    			$('#resultExcelMsg').html(mes).show();
    			if(ReportUtils._callBackAfterImport != null){
    				ReportUtils._callBackAfterImport.call(this);
	    		}
	    	}
	    }	
	},
	//phuongvm thu hoi va thanh ly
	//phuongvm quan ly danh muc
	showDlgCatalogImportExcel: function(){
		$('#easyuiPopupImportExcel').show();
		$('.ErrorMsgStyle').hide();
		$('#easyuiPopupImportExcel').dialog({  
	        closed: false,  
	        cache: false,  
	        modal: true,
	        onOpen: function(){	 
	        	$('.easyui-dialog .BtnGeneralStyle').css('color','');
	        },
	        onClose: function(){
	        	EquipmentManageCatalog.searchinfo();
				$("#grid").datagrid('uncheckAll');
				$("#fakefilepcCatalog").val("");
				$("#excelFileCatalog").val("");				
	        }
		});
	},
	importExcelCatalog: function(){
		$('.ErrorMsgStyle').hide();
		VTUtilJS.importExcelUtils(function(data) {
			$("#errExcelMsgCatalog").html(data.message).show();		
			if(data.fileNameFail == null || data.fileNameFail == "" ){					
				setTimeout(function(){
					$('#errExcelMsgCatalog').html("").hide();
					$('#easyuiPopupImportExcel').dialog('close');
				}, 3000);				
			}else{
				$("#fakefilepcCatalog").val("");
				$("#excelFileCatalog").val("");			
				$('#grid').datagrid('reload');	
			}						
		}, "importFrmCatalog", "excelFileCatalog", null, "errExcelMsgCatalog");
		return false;
	},
	/**
	 * xuat file excel 
	 * @author phuongvm
	 * @since 16/03/2015
	 */
	exportExcel: function(){
		$('#errMsg').html('').hide();
		var msg ='';
		var err = '';
		if(msg.length == 0){
			msg = Utils.getMessageOfSpecialCharactersValidate('code','Mã');
			err = 'code';
		}
		if(msg.length > 0){
			$('#errMsg').html(msg).show();
			$('#'+err).focus();
			return false;
		}
		
		var params=new Object(); 
		params.code = $('#code').val().trim();
		params.name = $('#name').val().trim();
		params.type = $('#type').val().trim();
		params.status = $('#status').val().trim();
		
		$.messager.confirm('Xác nhận','Bạn có muốn xuất file excel?',function(r){
			if(r){
				ReportUtils.exportReport('/equipment-manage-catalog/export-excel', params, 'errMsg');					
			}
		});		
		return false;
	}
};
/*
 * END OF FILE - /web/web/resources/scripts/business/equipment/vnm.equipment-manage-catalog.js
 */

/*
 * START OF FILE - /web/web/resources/scripts/business/equipment/vnm.equipment-manage-delivery.js
 */
var EquipmentManagerDelivery = {
	_editIndex:null,
	_lstEquipInRecord:null,
	_numberEquipInRecord:null,
	_lstEquipInsert:null,
	_mapDelivery:null,
	_countFile:0,
	_arrFileDelete:null,
	_lstShop: null,
	_customerId: null,
	_lstStockInsert: null,
	_approved: 2,
	_paramsSearch: null,
	_shopToCode: null,
	_lstEquipGroup: null,
	_lstEquipProvider: null,
	_lstEquipStock: null,
	_DELIVERY_CONTENT_NEW: 'DELIVERY_CONTENT_NEW',
	_equipNumberDeprec: 0,
	_categoryName: null,
	_capacity: null,
	_isSetStockCombo: 1,
	_curShopId: null,
	/**
	 * Lay lich sua giao nhan
	 * @author nhutnn
	 * @since 12/12/2014
	 */
	getHistoryDelivery: function(idRecord){
		$('#easyuiPopupHistory').show();
		$('#easyuiPopupHistory').dialog({  
	        closed: false,  
	        cache: false,  
	        modal: true,
	        width : 240,	        
	        height : 'auto',
	        onOpen: function(){	      
				$('#gridHistoryDelivery').datagrid({
					url : '/equipment-manage-delivery/search-history-delivery',
					autoRowHeight : true,
					fitColumns:true,
					width: $('#gridHistory').width(),
					height: "auto",
					scrollbarSize: 0,
					rownumbers:true,
					queryParams:{
						idRecordDelivery : idRecord
					},		
					columns:[[
					    {field: 'ngay', title: 'Ngày', width: 75, sortable:false, resizable: false, align: 'left'},					
						{field: 'trangThaiGiaoNhan', title: 'Trạng thái', width: 100, sortable: false, resizable: false,align: 'left'}			    
					]],
					onBeforeLoad: function(param){											 
					},
					onLoadSuccess: function(data){   			 
				   		$('.easyui-dialog .datagrid-header-rownumber').html('STT');	   		
					}
				});
	        }
		});
	},
	createParamsSearch: function () {
		EquipmentManagerDelivery._paramsSearch = null;
	},
	/**
	 * Tim kiem danh sach bien ban giao nhan
	 * @author nhutnn
	 * @since 12/12/2014
	 */
	searchRecordDelivery: function(){
		
		$('#errMsgInfo').html('').hide();
		var msg ='';
		if(msg.length == 0){
			msg = Utils.getMessageOfSpecialCharactersValidate('staffCode','Mã nhân viên');
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfSpecialCharactersValidate('equipCode','Mã thiết bị');
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfSpecialCharactersValidate('equipLendCode','Mã biên bản đề nghị cho mượn');
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfSpecialCharactersValidate('customerCode','Mã khách hàng');
		}
//		if(msg.length == 0){
//			msg = Utils.getMessageOfSpecialCharactersValidate('customerName','Tên khách hàng');
//		}		
		var fdate = $('#fDate').val().trim();
		if(msg.length == 0 && fdate != '' && !Utils.isDate(fdate) && fdate != '__/__/____'){
			msg = 'Ngày biên bản từ không tồn tại hoặc không đúng định dạng dd/mm/yyyy.';
		}
		var tdate = $('#tDate').val().trim();
		if(msg.length == 0 && fdate != '' && tdate != '' && !Utils.isDate(tdate) && tdate != '__/__/____'){
			msg = 'Đến ngày không tồn tại hoặc không đúng định dạng dd/mm/yyyy.';
		}
		var fdateContract = $('#fDateContract').val().trim();
		if(msg.length == 0 && fdateContract != '' && !Utils.isDate(fdateContract) && fdateContract != '__/__/____'){
			msg = 'Ngày hợp đồng từ không tồn tại hoặc không đúng định dạng dd/mm/yyyy.';
		}
		var tdateContract = $('#tDateContract').val().trim();
		if(msg.length == 0 && fdateContract != '' && tdateContract != '' && !Utils.isDate(tdateContract) && tdateContract != '__/__/____'){
			msg = 'Đến ngày không tồn tại hoặc không đúng định dạng dd/mm/yyyy.';
		}
		if(msg.length == 0 && fdate != '' && tdate != '' && !Utils.compareDate(fdate, tdate)){
			msg = 'Đến ngày biên bản không được nhỏ hơn Từ ngày biên bản';	
		}
		if(msg.length == 0 && fdateContract != '' && tdateContract != '' && !Utils.compareDate(fdateContract, tdateContract)){
			msg = 'Đến ngày hợp đồng không được nhỏ hơn Từ ngày hợp đồng';	
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfSpecialCharactersValidate('numberContract','Số hợp đồng');
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfSpecialCharactersValidate('recordCode','Mã biên bản');
		}
		if(msg.length > 0){
			$('#errMsgInfo').html(msg).show();
			return false;
		}
		
		var params=new Object(); 
		params.staffCode = $('#staffCode').val().trim();
		params.customerCode = $('#customerCode').val().trim();
//		params.customerName = $('#customerName').val().trim();
		params.fromDate = $('#fDate').val().trim();
		params.toDate = $('#tDate').val().trim();
		params.fromContractDate = $('#fDateContract').val().trim();
		params.toContractDate = $('#tDateContract').val().trim();
		params.numberContract = $('#numberContract').val().trim();
		params.recordCode = $('#recordCode').val().trim();
		params.statusRecord = $('#statusRecord').val().trim();
		params.statusDelivery = $('#statusDelivery').val().trim();
		params.statusPrint = $('#statusPrint').val().trim();
		params.equipLendCode = $('#equipLendCode').val().trim();
		params.equipCode = $('#equipCode').val().trim();

		var multiShop = $("#shop").data("kendoMultiSelect");
		var dataShop = multiShop.dataItems();
		if(dataShop!=null && dataShop.length>0) {
			var lstShop = new Array();
			var curShopCode = $('#curShopCode').val();
			for(var i=0;i<dataShop.length;i++){
				if(dataShop[i].shopCode != curShopCode) {
					lstShop.push(dataShop[i].shopCode);
				}
			}
			params.lstShop = lstShop.join(",");
		}
		
		// if (!AppData.hasOwnProperty('equipment')) {
		// 	AppData.equipment = {};
		// }
		// if (!AppData.equipment.hasOwnProperty('delivery')) {
		// 	AppData.equipment.delivery = {};
		// }

		// $.extend(AppData.equipment.delivery.search_param, params);
		// AppData.equipment.delivery.search_param.mutilShop = new Object();
		// AppData.equipment.delivery.search_param.mutilShop = $("#shop").data("kendoMultiSelect").value();


		// if (AppData.hasOwnProperty('equipment') 
		// 	&& AppData.equipment.hasOwnProperty('delivery')
		// 	&& AppData.equipment.delivery.hasOwnProperty('search_param')) {
			
		// }

		
		EquipmentManagerDelivery._mapDelivery = new Map();
		$('#gridRecordDelivery').datagrid('load',params);
		$('#gridRecordDelivery').datagrid('uncheckAll');
	},
	/**
	 * show popup import excel
	 * @author nhutnn
	 * @since 12/12/2014
	 */
	showDlgImportExcel: function(){
		$('#easyuiPopupImportExcel').show();
		$('.ErrorMsgStyle').hide();
		$('#easyuiPopupImportExcel').dialog({  
	        closed: false,  
	        cache: false,  
	        modal: true,
			height: 'auto',
	        onOpen: function(){	 
	        	$('.easyui-dialog .BtnGeneralStyle').css('color','');
	        },
	        onClose: function(){
	        	EquipmentManagerDelivery.searchRecordDelivery();
				$("#gridRecordDelivery").datagrid('uncheckAll');
				$("#fakefilepcBBGN").val("");
				$("#excelFileBBGN").val("");				
	        }
		});
	},
	/**
	 * import excel BBGN
	 * @author nhutnn
	 * @since 12/12/2014
	 */
	importExcelBBGN: function(){
		$('.ErrorMsgStyle').hide();
		VTUtilJS.importExcelUtils(function(data) {
			$("#errExcelMsgBBGN").html(data.message).show();		
			if(data.fileNameFail == null || data.fileNameFail == "" ){					
				setTimeout(function(){
					$('#errExcelMsgBBGN').html("").hide();
					$('#easyuiPopupImportExcel').dialog('close');
				}, 3000);				
			}else{
				$("#fakefilepcBBGN").val("");
				$("#excelFileBBGN").val("");			
				//$('#gridRecordDelivery').datagrid('reload');	
			}						
		}, "importFrmBBGN", "excelFileBBGN", null, "errExcelMsgBBGN");
		return false;
	},
	/**
	 * xuat file excel BBGN
	 * @author tamvnm
	 * @since 14/04/2015
	 */
	exportExcelBBGN: function(){
		
		$('#errMsgInfo').html('').hide();
		var msg ='';
		var lstRowsChecked = $('#gridRecordDelivery').datagrid('getChecked');
		var rows =$('#gridRecordDelivery').datagrid('getRows');	
		var lstIdChecked = new Array();
		if (rows.length == 0) {
			msg = 'Không có biên bản để xuất excel';
		}
		if (lstRowsChecked.length == 0 && msg.length == 0) {
			//msg = 'Bạn chưa chọn biên bản để xuất excel. Vui lòng chọn biên bản';
			// for (var i = 0, isize = rows.length; i < isize; i++) {
			// 	lstIdChecked.push(rows[i].idRecord);
			// }
		} else {
			for (var i = 0; i < lstRowsChecked.length; i++) {
				// if (i < lstRowsChecked.length - 1) {
				// 	lstIdChecked += lstRowsChecked[i].idRecord + ',';
				// } else {
				// 	lstIdChecked += lstRowsChecked[i].idRecord;
				// }
				
				lstIdChecked.push(lstRowsChecked[i].idRecord);

			}
		}
		if (msg.length > 0) {
			$('#errMsgInfo').html(msg).show();
			return false;
		}
		
		var params = new Object();
		if (lstRowsChecked.length > 0) {
			params.lstIdChecked = lstIdChecked;
		} else {
			params.staffCode = $('#staffCode').val().trim();
			params.customerCode = $('#customerCode').val().trim();
//			params.customerName = $('#customerName').val().trim();
			params.fromDate = $('#fDate').val().trim();
			params.toDate = $('#tDate').val().trim();
			params.fromContractDate = $('#fDateContract').val().trim();
			params.toContractDate = $('#tDateContract').val().trim();
			params.numberContract = $('#numberContract').val().trim();
			params.recordCode = $('#recordCode').val().trim();
			params.statusRecord = $('#statusRecord').val().trim();
			params.statusDelivery = $('#statusDelivery').val().trim();
			params.statusPrint = $('#statusPrint').val().trim();
			params.equipLendCode = $('#equipLendCode').val().trim();
			params.equipCode = $('#equipCode').val().trim();

			var multiShop = $("#shop").data("kendoMultiSelect");
			var dataShop = multiShop.dataItems();
			if (dataShop != null && dataShop.length > 0) {
				var lstShop = new Array();
				var curShopCode = $('#curShopCode').val();
				for (var i = 0, isize = dataShop.length; i < isize; i++) {
					if (dataShop[i].shopCode != curShopCode) {
						lstShop.push(dataShop[i].shopCode);
					}
				}
				params.lstShop = lstShop.join(",");
			}
		}
		$.messager.confirm('Xác nhận', 'Bạn có muốn xuất file excel?', function(r) {
			if (r) {
				ReportUtils.exportReport('/equipment-manage-delivery/export-record-delivery', params, 'errMsgInfo');
			}
		});
		return false;
	},

	// /**
	//  * xuat file excel BBGN
	//  * @author nhutnn
	//  * @since 12/12/2014
	//  */
	// exportExcelBBGN: function(){
	// 	// $('.ErrorMsgStyle').html("").hide();
	// 	// //var lstChecked = $('#gridRecordDelivery').datagrid('getChecked');
	// 	// var rows = $('#gridRecordDelivery').datagrid('getRows');	
	// 	// if(rows.length==0) {
	// 	// 	$('#errMsgInfo').html('Không có biên bản để xuất excel').show();
	// 	// 	return;
	// 	// }
	// 	// // if(lstChecked.length == 0) {
	// 	// // 	$('#errMsgInfo').html('Bạn chưa chọn biên bản để xuất excel. Vui lòng chọn biên bản').show();
	// 	// // 	return;
	// 	// // }	
	// 	// // lay danh sach bien ban
	// 	// var lstIdRecord = new Array();
	// 	// for(var i=0; i<rows.length; i++){
	// 	// 	lstIdRecord.push(rows[i].idRecord);
	// 	// }
	// 	$('#errMsgInfo').html('').hide();
	// 	var msg ='';
	// 	if(msg.length == 0){
	// 		msg = Utils.getMessageOfSpecialCharactersValidate('staffCode','Mã nhân viên');
	// 	}
	// 	if(msg.length == 0){
	// 		msg = Utils.getMessageOfSpecialCharactersValidate('customerCode','Mã khách hàng');
	// 	}
	// 	if(msg.length == 0){
	// 		msg = Utils.getMessageOfSpecialCharactersValidate('customerName','Tên khách hàng');
	// 	}		
	// 	var fdate = $('#fDate').val().trim();
	// 	if(msg.length == 0 && fdate != '' && !Utils.isDate(fdate)){
	// 		msg = 'Từ ngày không tồn tại hoặc không đúng định dạng dd/mm/yyyy.';
	// 	}
	// 	var tdate = $('#tDate').val().trim();
	// 	if(msg.length == 0 && fdate != '' && !Utils.isDate(tdate)){
	// 		msg = 'Đến ngày không tồn tại hoặc không đúng định dạng dd/mm/yyyy.';
	// 	}
	// 	if(msg.length == 0 && fdate != '' && tdate != '' && !Utils.compareDate(fdate, tdate)){
	// 		msg = 'Đến ngày không được nhỏ hơn Từ ngày';	
	// 	}
	// 	if(msg.length == 0){
	// 		msg = Utils.getMessageOfSpecialCharactersValidate('numberContract','Số hợp đồng');
	// 	}
	// 	if(msg.length == 0){
	// 		msg = Utils.getMessageOfSpecialCharactersValidate('recordCode','Mã biên bản');
	// 	}
	// 	if(msg.length > 0){
	// 		$('#errMsgInfo').html(msg).show();
	// 		return false;
	// 	}
		
	// 	var params=new Object(); 
	// 	params.staffCode = $('#staffCode').val().trim();
	// 	params.customerCode = $('#customerCode').val().trim();
	// 	params.customerName = $('#customerName').val().trim();
	// 	params.fromDate = $('#fDate').val().trim();
	// 	params.toDate = $('#tDate').val().trim();
	// 	params.numberContract = $('#numberContract').val().trim();
	// 	params.recordCode = $('#recordCode').val().trim();
	// 	params.statusRecord = $('#statusRecord').val().trim();
	// 	params.statusDelivery = $('#statusDelivery').val().trim();
		
	// 	$.messager.confirm('Xác nhận','Bạn có muốn xuất file excel?',function(r){
	// 		if(r){
	// 			ReportUtils.exportReport('/equipment-manage-delivery/export-record-delivery', params, 'errMsgInfo');					
	// 		}
	// 	});		
	// 	return false;
	// },

	 /**
	 * tai file excel template
	 * @author tamvnm
	 * @since 31/05/2015
	 */
	 downloadTemplate:function () {
	 	var params = new Object();
	 	ReportUtils.exportReport('/equipment-manage-delivery/download-template', params, 'errMsg');
	 },
	/**
	 * in BBGN
	 * @author nhutnn
	 * @since 14/01/2015
	 */
	 ShowPopUpPrint: function(id, recordStatus){
	 	var lstIdRecord = new Array();
		if(id != null){		
			// if (recordStatus != 4) {
			// 	lstIdRecord.push(id);
			// } else {
			// 	$('#errMsgInfo').html('Không thể in biên bản đã Hủy. Vui lòng chọn biên bản khác!').show();
			// 	return;
			// }
			lstIdRecord.push(id);
		}else{
			if(EquipmentManagerDelivery._mapDelivery.keyArray.length == 0) {
				$('#errMsgInfo').html('Bạn chưa chọn biên bản để xem bản in. Vui lòng chọn biên bản!').show();
				return;
			}	
			for(var i=0; i<EquipmentManagerDelivery._mapDelivery.keyArray.length; i++){
				lstIdRecord.push(EquipmentManagerDelivery._mapDelivery.keyArray[i]);
			}
		}		
	 	$('#easyuiPopupPrint').show();
		$('.ErrorMsgStyle').hide();
		$('#easyuiPopupPrint').dialog({  
	        closed: false,  
	        cache: false,  
	        modal: true,
			height: 'auto',
	        onOpen: function(){	 
	        	$('.easyui-dialog .BtnGeneralStyle').css('color','');
	        	$('#printDelivery').attr('checked', true);
	        	$('#printRecord').attr('checked', true);
	        	$('#printLend').attr('checked', true);
	        	$("#statusIn").prop("disabled", false);
				$("#comboboxPrint").prop("disabled", false);
	        	$('#printLend').on( "click", function(){
				    if ($(this).is(":checked")) {
				      $("#statusIn").prop("disabled", false);
				      $("#comboboxPrint").prop("disabled", false);
				   	} else {
				      $("#statusIn").prop("disabled", true); 
				      $("#comboboxPrint").prop("disabled", true);
				   	}
				});
	        },
	        onClose: function(){
	        	EquipmentManagerDelivery.searchRecordDelivery();
				$("#gridRecordDelivery").datagrid('uncheckAll');				
	        }
		});
		
	 },

	printBBGN: function(id){
		$('.ErrorMsgStyle').html("").hide();	
		//lay danh sach bien ban
		var lstIdRecord = new Array();
		if(id != null){			
			lstIdRecord.push(id);
		}else{
			if(EquipmentManagerDelivery._mapDelivery.keyArray.length == 0) {
				$('#errMsgInfo').html('Bạn chưa chọn biên bản để xem bản in. Vui lòng chọn biên bản!').show();
				return;
				// var rows = $('#gridRecordDelivery').datagrid('getRows');
				
				// for (var i = 0, isize = rows.length; i < isize; i++) {
				// 	lstIdRecord.push(rows[i].idRecord);
				// }
			} else {
				for(var i=0; i<EquipmentManagerDelivery._mapDelivery.keyArray.length; i++){
					lstIdRecord.push(EquipmentManagerDelivery._mapDelivery.keyArray[i]);
				}
			}
			
		}

		var params = new Object();
		params.lstIdRecord = lstIdRecord;
		try {
			params.reportCode = $('#function_code').val();
		} catch(e) {
			// Oop! Error
		}
		var isPrintDelivery = false;
		var isPrintRecord = false;
		var isPrintLend = false;

		if ($('#printDelivery').is(":checked")) {
			// params.lstPrint.push(4);
			isPrintDelivery = true;
		}
		if ($('#printRecord').is(":checked")) {
			// params.lstPrint.push(5);
			isPrintRecord = true;
		}
		if ($('#printLend').is(":checked")) {
			// params.lstPrint.push($('#statusIn').val());
			params.printType = $('#statusIn').val();
			isPrintLend = true;
		}
		
		$.messager.confirm('Xác nhận','Bạn có muốn xem bản in biên bản?',function(r){
			if(r){
				if (isPrintDelivery) {
					ReportUtils.exportReportWithCallBack(params, '/equipment-manage-delivery/print-delivery', 'errMsg', function(data){					
						if (isPrintRecord) {
							ReportUtils.exportReportWithCallBack(params, '/equipment-manage-delivery/print-record', 'errMsg', function(data) {					
								if (isPrintLend) {
									ReportUtils.exportReport('/equipment-manage-delivery/print-lend', params, 'errMsg');	
								}
							}, null, null);
						} else if (isPrintLend) {
								ReportUtils.exportReport('/equipment-manage-delivery/print-lend', params, 'errMsg');	
						}
					}, null, null);
					// ReportUtils.exportReport('/equipment-manage-delivery/print-delivery', params, 'errMsgInfo');
				} else if (isPrintRecord) {
					ReportUtils.exportReportWithCallBack(params, '/equipment-manage-delivery/print-record', 'errMsg', function(data){					
						if (isPrintLend) {
							ReportUtils.exportReport('/equipment-manage-delivery/print-lend', params, 'errMsg');	
						}
					}, null, null);
				} else if (isPrintLend) {
					ReportUtils.exportReport('/equipment-manage-delivery/print-lend', params, 'errMsg');
				}
				
			}
		});		
		
		return false;
	},
	/**
	 * Luu bien ban ban giao
	 * @author nhutnn
	 * @since 12/12/2014
	 */
	saveRecordDelivery: function(){
		$('#errMsgInfo').html('').hide();
		// var rows = $('#gridRecordDelivery').datagrid('getRows');
		// if(rows.length == 0){
		// 	$('#errMsgInfo').html('Không có dữ liệu để lưu').show();
		// 	return false;
		// }
		var lstRecordSuccess = new Array();
		var lstCheck = new Array();
		if(EquipmentManagerDelivery._mapDelivery.keyArray.length == 0) {
			$('#errMsgInfo').html('Bạn chưa chọn biên bản. Vui lòng chọn biên bản!').show();
			return;
		}	
		for(var i=0; i<EquipmentManagerDelivery._mapDelivery.keyArray.length; i++){
			lstCheck.push(EquipmentManagerDelivery._mapDelivery.keyArray[i]);
		}
		//var lstCheck = $('#gridRecordDelivery').datagrid('getChecked');
		if(lstCheck.length > 0){
			var statusRecordChange = $('#statusRecordLabel').val().trim();
			var statusDeliveryChange = $('#statusDeliveryLabel').val().trim();
			for(var i=0; i < lstCheck.length; i++){				
				if(statusRecordChange != "" || statusDeliveryChange != ""){
					//lstRecordSuccess.push(lstCheck[i].idRecord);
					lstRecordSuccess.push(lstCheck[i]);
				}
			}	
			if(lstRecordSuccess.length <= 0){
				$('#errMsgInfo').html('Bạn chưa chọn trạng thái để lưu!').show();
				return;
			}
			var params = new Object();
			params.lstIdRecord = lstRecordSuccess;
			params.statusDelivery = statusDeliveryChange;
			params.statusRecord = statusRecordChange;
			
			Utils.addOrSaveData(params, '/equipment-manage-delivery/save-record-delivery', null, 'errMsgInfo', function(data) {
				if(data.error){
					$('#errMsgInfo').html(data.errMsgInfo).show();
					return;
				}
				if(data.lstRecordError != null && data.lstRecordError.length>0){					
					VCommonJS.showDialogList({		  
						dialogInfo: {
							title: 'Lỗi'
						},
						data : data.lstRecordError,			   
						columns : [[
							{field:'formCode', title:'Mã biên bản', align:'left', width: 150, sortable:false, resizable:false},
							{field:'formStatusErr', title:'Trạng thái biên bản', align:'center', width: 80, sortable:false, resizable:false,formatter: function(value, row, index){
								var html="";
								if(row.formStatusErr == 1){
									html = '<img src="/resources/images/icon_delete.png" height="20" width="20" style="margin-right:5px;" title="Error">';
								}
								return html;
								
							}},
							{field:'deliveryStatusErr', title:'Trạng thái giao nhận', align:'center', width: 80, sortable:false, resizable:false,formatter: function(value, row, index){
								var html="";
								if(row.deliveryStatusErr == 1){
									html = '<img src="/resources/images/icon_delete.png" height="20" width="20" style="margin-right:5px;" title="Error">';
								}
								return html;
								
							}},
							// {field:'periodError', title:'Kỳ của biên bản', align:'center', width:80, sortable:false, resizable:false,formatter: function(value, row, index){
							// 	var html="";
							// 	if(row.periodError == 1){
							// 		html = '<img src="/resources/images/icon_delete.png" height="20" width="20" style="margin-right:5px;" title="Error">';
							// 	}
							// 	return html;
							// }},
							{field:'recordNotComplete', title:'Chi tiết biên bản', align:'left', width:80, sortable:false, resizable:false,formatter: function(value, row, index){
								return VTUtilJS.XSSEncode(value);
							}}
						]]
					});
				}else{
					$('#successMsgInfo').html('Lưu dữ liệu thành công').show();
					EquipmentManagerDelivery.searchRecordDelivery();
				}
				//if(lstRecordSuccess.length > 0){
				//	var msg = 'Lưu dữ liệu thành công các biên bản ';
				//	for(var i=0; i<lstRecordSuccess.length; i++){
				//		msg += lstRecordSuccess[i]+', ';
				//	}
				//	msg = msg.substring(0,msg.length - 2);
				//	msg += '!';
				//	$('#errMsgInfo').html(msg).show();
				//}
				setTimeout(function(){ 
					$('#errMsgInfo').html('').hide();
				}, 5000);
				
				$('#gridRecordDelivery').datagrid('reload');
				$('#gridRecordDelivery').datagrid('uncheckAll');
			}, null, null, null, "Bạn có muốn cập nhật ?", null);	
		}else{
			$('#errMsgInfo').html('Bạn chưa chọn biên bản để lưu').show();
			return false;
		}		
	},
	/**
	 * kiem tra ki
	 * @author nhutnn
	 * @since 19/12/2014
	 */
	checkPeriod: function(id){
		// Utils.getJSONDataByAjaxNotOverlay({id : id}, '/equipment-manage-delivery/check-equip-period', function(data){						
		// 	if(data.error){
		// 		$('#errMsgInfo').html(data.errMsg).show();
		// 		return false;
		// 	}else{
		// 		if(id > 0){
		// 			window.location.href="/equipment-manage-delivery/change-record?id="+id;
		// 		}else{
		// 			window.location.href="/equipment-manage-delivery/change-record";
		// 		}				
		// 	}
		// }, null, null);		
		if (id > 0) {
			window.location.href = "/equipment-manage-delivery/change-record?id=" + id;
		} else {
			window.location.href = "/equipment-manage-delivery/change-record";
		}
	},

	getLstEquipInfo: function () {
		var params = new Object();
		var equipLendCode = $("#lendCode").val().trim();
		var customerId = $('#customerIdHidden').val();
		if (equipLendCode != undefined && equipLendCode != null && equipLendCode != '') {
			params.equipLendCode = equipLendCode;
		}
		if (customerId != undefined && customerId != null) {
			params.customerId = customerId;
		} 

		Utils.getJSONDataByAjaxNotOverlay(params,'/equipment-manage-delivery/get-equipment-info', function(result) {
			 if (result.lstEquipGroup != undefined && result.lstEquipGroup != null && result.lstEquipGroup.length > 0) {
			 	EquipmentManagerDelivery._lstEquipGroup = result.lstEquipGroup;
			 }
			  if (result.lstEquipProvider != undefined && result.lstEquipProvider != null && result.lstEquipProvider.length > 0) {
			 	EquipmentManagerDelivery._lstEquipProvider = result.lstEquipProvider;
			 }
		}, null, null);
	},


	/**
	 * tao gia tri cho autoComplete Combobox Nhom thiet bi
	 * @author tamnm
	 * @since 17/06/2015
	 */
	setEquipGroup: function(index) {
		var curIndex = null;
		if (index != undefined || index == null) {
			curIndex = EquipmentManagerDelivery._editIndex;
		} else {
			curIndex = index;
		}
		var equipGroup = EquipmentManagerDelivery._lstEquipGroup;
		for(var i = 0, sz = equipGroup.length; i < sz; i++) {
	 		equipGroup[i].searchText = unicodeToEnglish(equipGroup[i].code + " " + equipGroup[i].name);
	 		equipGroup[i].searchText = equipGroup[i].searchText.toUpperCase();
		}
	 	// equipGroup.splice(0, 0, { code: "", name: "", searchText:"" });
	 	$('#groupEquipCBX' + curIndex).combobox("loadData", equipGroup);
	},
	/**
	 * tao gia tri cho autoComplete Combobox Nha cung cap
	 * @author tamvnm
	 * @since 17/06/2015
	 */
	setEquipProvider: function(index) {
		var curIndex = null;
		if (index != undefined || index == null) {
			curIndex = EquipmentManagerDelivery._editIndex;
		} else {
			curIndex = index;
		}
		var equipProvider = EquipmentManagerDelivery._lstEquipProvider;
		for(var i = 0, sz = equipProvider.length; i < sz; i++) {
	 		equipProvider[i].searchText = unicodeToEnglish(equipProvider[i].code + " " + equipProvider[i].name);
	 		equipProvider[i].searchText = equipProvider[i].searchText.toUpperCase();
		}
	 	// equipGroup.splice(0, 0, { code: "", name: "", searchText:"" });
	 	$('#providerEquipCBX' + curIndex).combobox("loadData", equipProvider);
	},
	/**
	 * tao gia tri cho autoComplete Combobox kho NPP
	 * @author tamvnm
	 * @since Oct 21 2015
	 */
	setEquipStock: function(index) {
		var curIndex = index;
		if (index != undefined || index == null) {
			curIndex = EquipmentManagerDelivery._editIndex;
		}
		var equipStocks = EquipmentManagerDelivery._lstEquipStock;
		for (var i = 0, sz = equipStocks.length; i < sz; i++) {
			var searchText = unicodeToEnglish(equipStocks[i].stockCode + " " + equipStocks[i].stockName);
			equipStocks[i].searchText = searchText.toUpperCase();
		}
		$('#stockEquipCBX' + curIndex).combobox("loadData", equipStocks);
	},
	/**
	 * Xu ly trên input Don vi
	 * @author nhutnn
	 * @since 19/12/2014
	 */
	onChangeShopCode: function(code) {
		var shopToCode = $('#shopToCode').val().trim();
		EquipmentManagerDelivery._shopToCode = shopToCode;
		if (shopToCode != undefined && shopToCode != null && shopToCode != '') {
			$.getJSON('/equipment-manage-delivery/getListGSNPP?toShopCode=' + shopToCode, function(result) {
				if (result.rows != undefined && result.rows != null && result.rows.length > 0) {
					for (var i = 0, sz = result.rows.length; i < sz; i++) {
						result.rows[i].searchText = unicodeToEnglish(result.rows[i].staffCode + " " + result.rows[i].staffName);
						result.rows[i].searchText = result.rows[i].searchText.toUpperCase();
					}
					result.rows.splice(0, 0, {
						staffCode: "",
						staffName: "",
						searchText: ""
					});
					$('#staffMonitorCode').combobox("loadData", result.rows);
					if (result.rows.length > 1) {
						if (code != undefined && code != '' && code != null) {
							$('#staffMonitorCode').combobox("setValue", code);

						} else {
							$('#staffMonitorCode').combobox("setValue", result.rows[1].staffCode);
						}

					} else {
						$('#staffMonitorCode').combobox("setValue", "");
					}

				} else {
					var rows = [{
						staffCode: "",
						staffName: "",
						searchText: ""
					}];
					$('#staffMonitorCode').combobox("loadData", rows);
					$('#staffMonitorCode').combobox("setValue", "");
				}
				EquipmentManagerDelivery.getDataEquipStock(EquipmentManagerDelivery._isSetStockCombo);
			});
		}
	},
	/**
	 * Chon don vi
	 * @author nhutnn
	 * @since 12/12/2014
	 */
	selectFromShop: function(id,code,name){
		$('#shopFromCode').val(code).change();
		 // $('#customerCode').val("").change();	
		//$('#stockCode').val(code).change();		
		$('.easyui-dialog').dialog('close');
	},
	/**
	 * Chon don vi
	 * @author nhutnn
	 * @since 12/12/2014
	 * 
	 * @author hunglm16
	 * @since August 18, 2015
	 * @description Bo sung luong them moi
	 */
	selectToShop: function(id, code, name) {
		if (code != undefined) {
			$('#shopToCode').val(code).change();
			$('#shopToId').val(id);
			$('#customerCode').val("").change();
			$('#staffMonitorCode').combobox("setValue", "");
			//Xu lu luong bo sung
			$('#addressName').val("");
			$('#street').val("");
			$('#wardName').val("");
			$('#districtName').val("");
			$('#provinceName').val("");
			EquipmentManagerDelivery._customerId = null;
		}

		//$('#stockCode').val(code).change();
		try {
			$('.easyui-dialog').dialog('close');
			EquipmentManagerDelivery._shopToCode = $('#shopToCode').val().trim();
		} catch (e) {
			console.log('Dialog chưa mở đã gọi đóng.');
		}
		var shopToCode = $('#shopToCode').val();
		if (shopToCode != undefined && shopToCode != null && shopToCode != '') {
			$.getJSON('/equipment-manage-delivery/getListGSNPP?toShopCode=' + shopToCode, function(result) {
				if (result.rows != undefined && result.rows != null && result.rows.length > 0) {
					for (var i = 0, sz = result.rows.length; i < sz; i++) {
						result.rows[i].searchText = unicodeToEnglish(result.rows[i].staffCode + " " + result.rows[i].staffName);
						result.rows[i].searchText = result.rows[i].searchText.toUpperCase();
					}
					result.rows.splice(0, 0, {
						staffCode: "",
						staffName: "",
						searchText: ""
					});
					$('#staffMonitorCode').combobox("loadData", result.rows);
					// $('#staffMonitorCode').combobox("setValue", "");
					if (result.rows.length > 1) {
						$('#staffMonitorCode').combobox("setValue", result.rows[1].staffCode);
					} else {
						$('#staffMonitorCode').combobox("setValue", "");
					}
				} else {
					var rows = [{
						staffCode: "",
						staffName: "",
						searchText: ""
					}];
					$('#staffMonitorCode').combobox("loadData", rows);
				}
				EquipmentManagerDelivery.getDataEquipStock(EquipmentManagerDelivery._isSetStockCombo);
			});
		} else {
			var rows = [{
				staffCode: "",
				staffName: "",
				searchText: ""
			}];
			$('#staffMonitorCode').combobox("loadData", rows);
			EquipmentManagerDelivery.getDataEquipStock(EquipmentManagerDelivery._isSetStockCombo);
		}
	},
	/**
	 * Chon khach hang
	 * @author nhutnn
	 * @since 12/12/2014
	 * 
	 * @author hunglm16
	 * @since August 18, 2015
	 * @description ra soat va hotfix
	 */
	selectCustomer: function(shortCode, id, street, houseNumber, areaId){		
		$('#customerCode').val(shortCode);		
		if(id != undefined && id != null) {	
			EquipmentManagerDelivery._customerId = id;
		}
		$('#addressName').val('');
        $('#street').val('');
        if (!isNullOrEmpty(houseNumber)) {
        	$('#addressName').val(houseNumber);	
        }
        if (!isNullOrEmpty(street)) {
        	$('#street').val(street);	
        }
		var flag = false;
		var params = {};
		if (areaId != undefined && areaId != null) {
			params.id = areaId;
			flag = true;
		} else {
			params.customerId = id;
			flag = true;
		}
		$('#wardName').val("");
		$('#districtName').val("");
		$('#provinceName').val("");
		if (flag) {
			Utils.getJSONDataByAjax(params, '/commons/getAreaEntity', function (res){
				if (res != undefined && res != null) {
					if (res.area != undefined && res.area != null) {
						$('#wardName').val(res.area.areaName);
						if (res.area.parentArea != undefined && res.area.parentArea != null) {
							$('#districtName').val(res.area.parentArea.areaName);
							if (res.area.parentArea.parentArea != undefined && res.area.parentArea.parentArea != null) {
							  $('#provinceName').val(res.area.parentArea.parentArea.areaName);
							}
						}
					}
				}
				$('.easyui-dialog').dialog('close');
			});			
		} else {
			$('.easyui-dialog').dialog('close');
		}
	},
	/**
	 * Su kiem thay doi Ma khach hang
	 * 
	 * @author hunglm16
	 * @since August 17,2015
	 * */
	changeEventCustomerCode: function (txt) {
		$('#wardName').val("");
		$('#districtName').val("");
		$('#provinceName').val("");
		var shopToCode = $('#shopToCode').val().trim();
		var shortCode = $('#customerCode').val().trim();
		if (shopToCode.length > 0 && shortCode.length > 0) {
			var params = {};
			params.shortCode = shortCode;
			params.shopCode = shopToCode;
			Utils.getJSONDataByAjax(params, '/commons/getCustomerEntity', function (res){
				if (res != undefined && res != null && res.customer != null && res.customer != undefined) {
					$('#addressName').val(res.customer.housenumber);
					$('#street').val(res.customer.street);
					if (res.customer.area != undefined && res.customer.area != null) {
						$('#wardName').val(res.customer.area.areaName);
						if (res.customer.area.parentArea != undefined && res.customer.area.parentArea != null) {
							$('#districtName').val(res.customer.area.parentArea.areaName);
							if (res.customer.area.parentArea.parentArea != undefined && res.customer.area.parentArea.parentArea != null) {
							  $('#provinceName').val(res.customer.area.parentArea.parentArea.areaName);
							}
						}
					}
				}
			});
		}
	},
	/**
	 * Xoa thiet bi trong bien ban ban giao
	 * @author nhutnn
	 * @since 12/12/2014
	 */
	deleteEquipmentForDelivery: function(index){		
		var rowDelete = $('#gridEquipmentDelivery').datagrid('getRows')[index];
		if(EquipmentManagerDelivery._numberEquipInRecord != null && index < EquipmentManagerDelivery._numberEquipInRecord){
			if(EquipmentManagerDelivery._lstEquipInRecord == null){
				EquipmentManagerDelivery._lstEquipInRecord = new Array();
			}
			// var row = $('#gridEquipmentDelivery').datagrid('getRows')[index];
			EquipmentManagerDelivery._lstEquipInRecord.push(rowDelete.equipmentCode);
			EquipmentManagerDelivery._numberEquipInRecord--;
		}else if(EquipmentManagerDelivery._lstEquipInsert != null && index >= EquipmentManagerDelivery._numberEquipInRecord){
			// var row = $('#gridEquipmentDelivery').datagrid('getRows')[index];
			var ix = EquipmentManagerDelivery._lstEquipInsert.indexOf(rowDelete.equipmentCode);
			if (ix > -1) {
				EquipmentManagerDelivery._lstEquipInsert.splice(ix, 1);
			}
		}
		var rows = $('#gridEquipmentDelivery').datagrid('getRows');
		$('#gridEquipmentDelivery').datagrid("deleteRow", index);

		if (EquipmentManagerDelivery._editIndex == index) {
			EquipmentManagerDelivery._editIndex = null;
			return;
		}
		if (EquipmentManagerDelivery._editIndex == null) {
			var nSize = rows.length;
			for(var i = 0; i < nSize; i++) {
				var row = rows[i];
				var seriNumber = null;
				if (index == 0 || i >= index) {
					var curIndex = i + 1;
					seriNumber = $('#seriNumberInGrid' + curIndex).val();
				} else {
					seriNumber = $('#seriNumberInGrid' + i).val();
				}
				
				$('#gridEquipmentDelivery').datagrid('updateRow',{
					index: i,	
					row: row
				});	
				if ((row.seriNumber == null || row.seriNumber == '') && seriNumber != null && seriNumber != '') {
					$('#seriNumberInGrid' + i).val(seriNumber)
				}
			}
		} else {
			var curRow = $('#gridEquipmentDelivery').datagrid('getRows')[EquipmentManagerDelivery._editIndex - 1];
			var content = $('#contentDelivery').val();
			
			var equipCode = $('#equipmentCodeInGrid' + EquipmentManagerDelivery._editIndex).val();
			var seri = $('#seriNumberInGrid' + EquipmentManagerDelivery._editIndex).val();
			var dep = $('#depreciationInGrid' + EquipmentManagerDelivery._editIndex).val();
			var group = null;
			var provider = null;
			var stock = null;
			var price = null;
			var health = null;
			var manufacturingYear = null;

			if (curRow.groupEquipmentCode == undefined ) {
				group = $('#groupEquipCBX' + EquipmentManagerDelivery._editIndex).combobox('getValue');
				provider = $('#providerEquipCBX' + EquipmentManagerDelivery._editIndex).combobox('getValue');
				stock = $('#stockEquipCBX' + EquipmentManagerDelivery._editIndex).combobox('getValue');
				price = $('#price' + EquipmentManagerDelivery._editIndex).val();
				health = $('#healthStatus' + EquipmentManagerDelivery._editIndex).val();
				manufacturingYear = $('#yearManufacture' + EquipmentManagerDelivery._editIndex).val();
			}

			var nSize = rows.length - 1;
			for(var i = 0; i < nSize; i++) {
				var row = rows[i];
				var seriNumber = null;
				if (index == 0 || i >= index) {
					var curIndex = i + 1;
					seriNumber = $('#seriNumberInGrid' + curIndex).val();
				} else {
					seriNumber = $('#seriNumberInGrid' + i).val();
				}
				
				$('#gridEquipmentDelivery').datagrid('updateRow',{
					index: i,	
					row: row
				});	
				if ((row.seriNumber == null || row.seriNumber == '') && seriNumber != null && seriNumber != '') {
					$('#seriNumberInGrid' + i).val(seriNumber)
				}
			}

			EquipmentManagerDelivery._editIndex--;
			if (curRow.groupEquipmentCode != undefined ) {
				$('#gridEquipmentDelivery').datagrid("deleteRow", EquipmentManagerDelivery._editIndex);
				$('#gridEquipmentDelivery').datagrid("appendRow", {});
				
				$('#gridEquipmentDelivery').datagrid('updateRow',{
					index: EquipmentManagerDelivery._editIndex,	
					row: curRow
				});
				$('#contentDelivery').val(content);
				if (seri != undefined) {
					$('#seriNumberInGrid' + EquipmentManagerDelivery._editIndex).val(seri);
				}
				if (dep != undefined) {
					$('#depreciationInGrid' + EquipmentManagerDelivery._editIndex).val(dep);
				}

				EquipmentManagerDelivery.getEventContent();
				
			} else {
				$('#gridEquipmentDelivery').datagrid("deleteRow", EquipmentManagerDelivery._editIndex);
				$('#gridEquipmentDelivery').datagrid("appendRow", {});
				
				
				$('#contentDelivery').val(content);
				EquipmentManagerDelivery.setUIEquipInRecord();
				EquipmentManagerDelivery.getEventContent();
				if (content == EquipmentManagerDelivery._DELIVERY_CONTENT_NEW) {
					enableCombo('groupEquipCBX' + EquipmentManagerDelivery._editIndex);
				    enableCombo('providerEquipCBX' + EquipmentManagerDelivery._editIndex);
				    enableCombo('stockEquipCBX' + EquipmentManagerDelivery._editIndex);
				    enable('price' + EquipmentManagerDelivery._editIndex);
		            enable('healthStatus' + EquipmentManagerDelivery._editIndex);
		            enable('yearManufacture' + EquipmentManagerDelivery._editIndex);
		            disabled('equipmentCodeInGrid' + EquipmentManagerDelivery._editIndex);
				} else {
					disableCombo('groupEquipCBX' + EquipmentManagerDelivery._editIndex);
				    disableCombo('providerEquipCBX' + EquipmentManagerDelivery._editIndex);
				    disableCombo('stockEquipCBX' + EquipmentManagerDelivery._editIndex);
				    disabled('price' + EquipmentManagerDelivery._editIndex);
		            disabled('healthStatus' + EquipmentManagerDelivery._editIndex);
		            disabled('yearManufacture' + EquipmentManagerDelivery._editIndex);
		            enable('equipmentCodeInGrid' + EquipmentManagerDelivery._editIndex);
				}

	            EquipmentManagerDelivery.editInputInGrid();
				$('#groupEquipCBX' + EquipmentManagerDelivery._editIndex).combobox('setValue', group);
				$('#providerEquipCBX' + EquipmentManagerDelivery._editIndex).combobox('setValue', provider);
				$('#stockEquipCBX' + EquipmentManagerDelivery._editIndex).combobox('setValue', stock);
				$('#equipmentCodeInGrid' + EquipmentManagerDelivery._editIndex).val(equipCode);
				if (seri != undefined) {
					$('#seriNumberInGrid' + EquipmentManagerDelivery._editIndex).val(seri);
				}
				if (dep != undefined) {
					$('#depreciationInGrid' + EquipmentManagerDelivery._editIndex).val(dep);
				}
				if (price != undefined) {
					$('#price' + EquipmentManagerDelivery._editIndex).val(Utils.returnMoneyValue(price));
				}
				if (health != undefined) {
					$('#healthStatus' + EquipmentManagerDelivery._editIndex).val(health);
				}
				if (manufacturingYear != undefined) {
					$('#yearManufacture' + EquipmentManagerDelivery._editIndex).val(manufacturingYear);
				}

				
			}

		}
		
	},
	/**
	 * Them mot dong thiet bi
	 * @author nhutnn
	 * @since 19/12/2014
	 */
	insertEquipmentInGrid: function(){	
		$('#errMsgInfo').html('').hide();
		var sz = $("#gridEquipmentDelivery").datagrid("getRows").length;
		if(EquipmentManagerDelivery._editIndex != null && EquipmentManagerDelivery._editIndex == sz-1){
			var row = $('#gridEquipmentDelivery').datagrid('getRows')[EquipmentManagerDelivery._editIndex];
			if ($('#contentDelivery').val() != 'DELIVERY_CONTENT_NEW') {
				if($('#equipmentCodeInGrid' + EquipmentManagerDelivery._editIndex).val() == ''){
					$('#errMsgInfo').html('Bạn chưa nhập mã thiết bị').show();
					$('#equipmentCodeInGrid' + EquipmentManagerDelivery._editIndex).focus();
					return false;
				}else if($('#equipmentCodeInGrid' + EquipmentManagerDelivery._editIndex).val() != undefined){
					var rowEq = $('#gridEquipmentDelivery').datagrid('getRows');
					for(var i=0; i < rowEq.length-1; i++){
						if(rowEq[i].equipmentCode == $('#equipmentCodeInGrid' + EquipmentManagerDelivery._editIndex).val().trim()){
							$('#errMsgInfo').html("Mã thiết bị "+rowEq[i].equipmentCode+" đã tồn tại!").show();
							return false;
						}
					}
					row.equipmentCode = $('#equipmentCodeInGrid' + EquipmentManagerDelivery._editIndex).val();
				}
				if($('#seriNumberInGrid').val() == ''){
					// $('#errMsgInfo').html('Bạn chưa xử lý xong dòng thiết bị thêm mới!').show();
					// return false;
				}else if($('#seriNumberInGrid' + EquipmentManagerDelivery._editIndex).val() != undefined){
					row.seriNumber = $('#seriNumberInGrid' + EquipmentManagerDelivery._editIndex).val();
				}
				if($('#depreciationInGrid' + EquipmentManagerDelivery._editIndex).val() == ''){
					$('#errMsgInfo').html('Bạn chưa nhập số tháng khấu hao').show();
					$('#depreciationInGrid' + EquipmentManagerDelivery._editIndex).focus();
					return false;
				}else if($('#depreciationInGrid' + EquipmentManagerDelivery._editIndex).val() != undefined){
					row.depreciation = $('#depreciationInGrid' + EquipmentManagerDelivery._editIndex).val();
				}
				// if($('#stockInGrid').val() == ''){
				// 	$('#errMsgInfo').html('Bạn chưa xử lý xong dòng thiết bị thêm mới!').show();
				// 	return false;
				// }else if($('#stockInGrid').val() != undefined){
				// 	var toStockCode = $('#stockInGrid').val().trim();
				// 	row.toStock = $('#stockInGrid').val();
				// }
				if($('#contentDelivery').val() != undefined){
					row.contentDelivery = $('#contentDelivery').val();
				}
				
				$('#equipmentCodeInGrid' + EquipmentManagerDelivery._editIndex).remove();

				
				$('#seriNumberInGrid' + EquipmentManagerDelivery._editIndex).remove();

				
				// $('#stockInGrid').remove();
				$('#contentDelivery').remove();
				$('#gridEquipmentDelivery').datagrid('updateRow',{
					index: EquipmentManagerDelivery._editIndex,	
					row: row
				});
				if(EquipmentManagerDelivery._lstEquipInsert == null){
					EquipmentManagerDelivery._lstEquipInsert = new Array();
				}
				
				EquipmentManagerDelivery._lstEquipInsert.push(row.equipmentCode);
			} else {
				if($('#equipmentCodeInGrid' + EquipmentManagerDelivery._editIndex).val() != '') {
					$('#errMsgInfo').html('Bạn không được nhập mã thiết bị').show();
					$('#equipmentCodeInGrid' + EquipmentManagerDelivery._editIndex).focus();
					return false;
				} else if ($('#equipmentCodeInGrid' + EquipmentManagerDelivery._editIndex).val() != undefined) {
					// var rowEq = $('#gridEquipmentDelivery').datagrid('getRows');
					// for(var i=0; i < rowEq.length-1; i++){
					// 	if(rowEq[i].equipmentCode == $('#equipmentCodeInGrid' + EquipmentManagerDelivery._editIndex).val().trim()){
					// 		$('#errMsgInfo').html("Mã thiết bị "+rowEq[i].equipmentCode+" đã tồn tại!").show();
					// 		return false;
					// 	}
					// }
					// row.equipmentCode = $('#equipmentCodeInGrid' + EquipmentManagerDelivery._editIndex).val();
				}

				// nhom thiet bi
				if ($('#groupEquipCBX' + EquipmentManagerDelivery._editIndex).combobox('getValue') == '') {
					$('#errMsgInfo').html('Bạn chưa chọn nhóm thiết bị').show();
					$('#groupEquipCBX' + EquipmentManagerDelivery._editIndex).focus();
					return false;
				} else {
					row.groupEquipmentCode = $('#groupEquipCBX' + EquipmentManagerDelivery._editIndex).combobox('getValue');
					row.groupEquipmentName = $('#groupEquipCBX' + EquipmentManagerDelivery._editIndex).combobox('getText');
					if (EquipmentManagerDelivery._categoryName != null) {
						row.typeEquipment = EquipmentManagerDelivery._categoryName;
						EquipmentManagerDelivery._categoryName = null;
					}
					if (EquipmentManagerDelivery._capacity != null) {
						row.capacity = EquipmentManagerDelivery._capacity;
						EquipmentManagerDelivery._capacity = null;
					}
				}
				// Nha Cung cap
				if ($('#providerEquipCBX' + EquipmentManagerDelivery._editIndex).combobox('getValue') == '') {
					$('#errMsgInfo').html('Bạn chưa chọn nhà cung cấp').show();
					$('#providerEquipCBX' + EquipmentManagerDelivery._editIndex).focus();
					return false;
				} else {
					row.equipmentProviderCode = $('#providerEquipCBX' + EquipmentManagerDelivery._editIndex).combobox('getValue');
					row.equipmentProvider = $('#providerEquipCBX' + EquipmentManagerDelivery._editIndex).combobox('getText');
					
				}

				// kho NPP
				if ($('#stockEquipCBX' + EquipmentManagerDelivery._editIndex).combobox('getValue') == '') {
					$('#errMsgInfo').html('Bạn chưa chọn Kho').show();
					$('#stockEquipCBX' + EquipmentManagerDelivery._editIndex).focus();
					return false;
				} else {
					row.stockCode = $('#stockEquipCBX' + EquipmentManagerDelivery._editIndex).combobox('getValue');
					row.stockName = $('#stockEquipCBX' + EquipmentManagerDelivery._editIndex).combobox('getText');
					
				}

				if($('#seriNumberInGrid').val() == ''){
					// $('#errMsgInfo').html('Bạn chưa xử lý xong dòng thiết bị thêm mới!').show();
					// return false;
				}else if($('#seriNumberInGrid' + EquipmentManagerDelivery._editIndex).val() != undefined){
					row.seriNumber = $('#seriNumberInGrid' + EquipmentManagerDelivery._editIndex).val();
				}

				if($('#depreciationInGrid' + EquipmentManagerDelivery._editIndex).val() == ''){
					$('#errMsgInfo').html('Bạn chưa nhập số tháng khấu hao').show();
					$('#depreciationInGrid' + EquipmentManagerDelivery._editIndex).focus();
					return false;
				}else if($('#depreciationInGrid' + EquipmentManagerDelivery._editIndex).val() != undefined){
					row.depreciation = $('#depreciationInGrid' + EquipmentManagerDelivery._editIndex).val();
				}

				if($('#price' + EquipmentManagerDelivery._editIndex).val() == ''){
					$('#errMsgInfo').html('Bạn chưa nhập giá trị TBBH').show();
					$('#price' + EquipmentManagerDelivery._editIndex).focus();
					return false;
				} else if ($('#price' + EquipmentManagerDelivery._editIndex).val() < 100) {
					$('#errMsgInfo').html('Giá trị TBBH phải lớn hơn 100').show();
					$('#price' + EquipmentManagerDelivery._editIndex).focus();
					return false;
				} else if($('#price' + EquipmentManagerDelivery._editIndex).val() != undefined){
					var price = $('#price' + EquipmentManagerDelivery._editIndex).val();
					if (price != undefined) {
						row.price = Utils.returnMoneyValue(price);
					}
					
				}
				// if($('#stockInGrid').val() == ''){
				// 	$('#errMsgInfo').html('Bạn chưa xử lý xong dòng thiết bị thêm mới!').show();
				// 	return false;
				// }else if($('#stockInGrid').val() != undefined){
				// 	var toStockCode = $('#stockInGrid').val().trim();
				// 	row.toStock = $('#stockInGrid').val();
				// }
				if($('#contentDelivery').val() != undefined){
					row.contentDelivery = $('#contentDelivery').val();
				}

				if ($('#healthStatus' + EquipmentManagerDelivery._editIndex).val() == '') {
					$('#errMsgInfo').html('Bạn chưa chọn trình trạng thiết bị').show();
					$('#healthStatus' + EquipmentManagerDelivery._editIndex).focus();
					return false;
				} else {
					row.healthStatus = $('#healthStatus' + EquipmentManagerDelivery._editIndex).val();
					
				}

				if ($('#yearManufacture' + EquipmentManagerDelivery._editIndex).val() == '') {
					$('#errMsgInfo').html('Bạn chưa nhập năm sản xuất').show();
					$('#yearManufacture' + EquipmentManagerDelivery._editIndex).focus();
					return false;
				} else if ($('#yearManufacture' + EquipmentManagerDelivery._editIndex).val() > Utils.getCurrentYear) {
					$('#errMsgInfo').html('Năm sản xuất phải nhỏ hơn hoặc bằng năm hiện tại').show();
					$('#yearManufacture' + EquipmentManagerDelivery._editIndex).focus();
				} else {
					row.yearManufacture = $('#yearManufacture' + EquipmentManagerDelivery._editIndex).val();
				}

				$('#equipmentCodeInGrid' + EquipmentManagerDelivery._editIndex).remove();

				$('#seriNumberInGrid' + EquipmentManagerDelivery._editIndex).remove();

				// $('#stockInGrid').remove();
				$('#contentDelivery').remove();
				$('#gridEquipmentDelivery').datagrid('updateRow',{
					index: EquipmentManagerDelivery._editIndex,	
					row: row
				});
				if(EquipmentManagerDelivery._lstEquipInsert == null){
					EquipmentManagerDelivery._lstEquipInsert = new Array();
				}
				disabled('equipmentCodeInGrid' + EquipmentManagerDelivery._editIndex);

				EquipmentManagerDelivery._lstEquipInsert.push(row.equipmentCode);
			}
			
		}		
		EquipmentManagerDelivery._editIndex = sz;	
		$('#gridEquipmentDelivery').datagrid("appendRow", {});	
		// F9 tren grid
		EquipmentManagerDelivery.editInputInGrid();
		$("#gridEquipmentDelivery").datagrid("selectRow", EquipmentManagerDelivery._editIndex);	
		$('#equipmentCodeInGrid' + EquipmentManagerDelivery._editIndex).focus();
		// $('#contentDelivery').width($('#contentDelivery').parent().width());	
		// $('#equipmentCodeInGrid' + EquipmentManagerDelivery._editIndex).width($('#equipmentCodeInGrid' + EquipmentManagerDelivery._editIndex).parent().width());
		// $('#seriNumberInGrid').width($('#seriNumberInGrid').parent().width());
		// $('#stockInGrid').width($('#stockInGrid').parent().width());

		Utils.bindComboboxEquipGroupEasyUI('groupEquipCBX' + EquipmentManagerDelivery._editIndex);
		EquipmentManagerDelivery.setEquipGroup();
		Utils.bindComboboxEquipGroupEasyUI('providerEquipCBX' + EquipmentManagerDelivery._editIndex);
		EquipmentManagerDelivery.setEquipProvider();
		Utils.bindComboboxEquipGroupEasyUI('stockEquipCBX' + EquipmentManagerDelivery._editIndex);
		EquipmentManagerDelivery.setEquipStock();
		disableCombo('groupEquipCBX' + EquipmentManagerDelivery._editIndex);
		disableCombo('providerEquipCBX' + EquipmentManagerDelivery._editIndex);
		disableCombo('stockEquipCBX' + EquipmentManagerDelivery._editIndex);


		if ($('#price'+ EquipmentManagerDelivery._editIndex).val().indexOf(',')==-1) {
			$('#price'+ EquipmentManagerDelivery._editIndex).val(formatCurrency($('#price'+ EquipmentManagerDelivery._editIndex).val()));
			Utils.formatCurrencyFor('price'+ EquipmentManagerDelivery._editIndex);
			Utils.bindFormatOnTextfield('price'+ EquipmentManagerDelivery._editIndex,Utils._TF_NUMBER_COMMA);
		}
		disabled('price' + EquipmentManagerDelivery._editIndex);
		disabled('healthStatus' + EquipmentManagerDelivery._editIndex);
		disabled('yearManufacture' + EquipmentManagerDelivery._editIndex);
		// $('#contentDelivery').change(function() {
		//   if (this.value != 'DELIVERY_CONTENT_NEW') {
		//       $('#gridEquipmentDelivery').datagrid('deleteRow',EquipmentManagerDelivery._editIndex);
		//   	  EquipmentManagerDelivery.addEquipmentInRecord();
		//       $('#contentDelivery').val(this.value);
		//       disableCombo('groupEquipCBX' + EquipmentManagerDelivery._editIndex);
		//       disableCombo('providerEquipCBX' + EquipmentManagerDelivery._editIndex);
		//       console.log('khong phai cap moi');
		//       disabled('price' + EquipmentManagerDelivery._editIndex);
		//       disabled('healthStatus' + EquipmentManagerDelivery._editIndex);
		//       enable('equipmentCodeInGrid' + EquipmentManagerDelivery._editIndex);
		//   } else {
		//   	$('#gridEquipmentDelivery').datagrid('deleteRow',EquipmentManagerDelivery._editIndex);
		//   	EquipmentManagerDelivery.addEquipmentInRecord();
		//     $('#contentDelivery').val(this.value);
		//     enableCombo('groupEquipCBX' + EquipmentManagerDelivery._editIndex);
		//     enableCombo('providerEquipCBX' + EquipmentManagerDelivery._editIndex);
		//     console.log('cap moi');
		//    	enable('price' + EquipmentManagerDelivery._editIndex);
		//     enable('healthStatus' + EquipmentManagerDelivery._editIndex);
		//     disabled('equipmentCodeInGrid' + EquipmentManagerDelivery._editIndex);
		//   }
		EquipmentManagerDelivery.getEventContent();

	},
	/**
	 * Show popup tim kiem thiet bi
	 * @author nhutnn
	 * @since 12/12/2014
	 */
	showPopupSearchEquipment: function(){
		$('#easyuiPopupSearchEquipment').show();
		$('#easyuiPopupSearchEquipment').dialog({  
	        closed: false,  
	        cache: false, 
	        modal: true,
	        width : 1000,
	        height: 'auto',
	        onOpen: function(){	    
	        	//setDateTimePicker('yearManufacturing');
	        	Utils.bindComboboxEquipGroupEasyUICodeName('groupEquipment', null, 193);
	        	Utils.bindComboboxEquipGroupEasyUICodeName('providerId', null, 193);


	        	var year = new Date();
	        	year = year.getFullYear(); 
	        	var op = '<option value="" selected="selected">Chọn</option><option value="'+year+'">'+year+'</option>';
	        	for(var i=1; i<100; i++){
	        		op+='<option value="'+(year-i)+'">'+(year-i)+'</option>';
	        	}
	        	$('#yearManufacturing').html(op).change();
	        	$('#equipmentCode, #seriNumber, #typeEquipment').bind('keyup',function(event){
	        		if(event.keyCode == keyCodes.ENTER){
	        			$('#btnSeachEquipmentDlg').click(); 
	        		}
	        	});
	        	$('#yearManufacturing').bind('keyup',function(event){
	        		if(event.keyCode == keyCodes.ENTER){
	        			$('#btnSeachEquipmentDlg').click(); 
	        		}
	        	});
	        	$('#groupEquipment, #providerId').next().find(".combo-text.validatebox-text").bind('keyup',function(event){
	        		if(event.keyCode == keyCodes.ENTER){
	        			$('#btnSeachEquipmentDlg').click(); 
	        		}
	        	});
	        	$('#stockCode').bind('keyup',function(event){
	        		if(event.keyCode == keyCodes.ENTER){
	        			$('#btnSeachEquipmentDlg').click(); 
	        		}
	        	});
	        	$('#stockCode').bind('keyup',function(event){
	        		if(event.keyCode == keyCodes.F9){
	        			EquipmentManagerDelivery.showPopupStock('stockCode');
	        		}
	        	});

	        	if ($('#stockInGrid').val() != undefined && $('#stockInGrid').val() != null && $('#stockInGrid').val() != '') {
	        		$('#stockCode').val($('#stockInGrid').val());
	        	}

	        	EquipmentManagerDelivery.getEquipCatalog();
	        	//EquipmentManagerDelivery.getEquipGroup(null);
	        	var rowEquip = $('#gridEquipmentDelivery').datagrid('getRows');
	        	var lstEquipmentCode = '';
	        	for (var i = 0; i < rowEquip.length; i++) {
	        		// lstEquipmentCode.push(rowEquip[i].equipmentCode);
	        		if (rowEquip[i].equipmentCode != null) {
		        		if (i < rowEquip.length - 1) {
		        			lstEquipmentCode += rowEquip[i].equipmentCode + ', ';
		        		} else {
		        			lstEquipmentCode += rowEquip[i].equipmentCode;
		        		}
	        		}
	        	}
	        	var params = new Object();
	        	var groupId = $('#groupEquipment').combobox('getValue');
	        	if (groupId == -1 || groupId == "") {
	        		groupId = "";
	        	}
	        	var providerId = $('#providerId').combobox('getValue');
	        	if (providerId == -1 || providerId == "") {
	        		providerId = "";
	        	}
				params = {
						customerId: $('#customerIdHidden').val(),
						equipCode: $('#equipmentCode').val().trim(),
						seriNumber: $('#seriNumber').val().trim(),
						categoryId: $('#typeEquipment').val(),
						// groupId: $('#groupEquipment').val(),
						groupId: groupId,
						// providerId: $('#providerId').val(),
						providerId: providerId,
						yearManufacture: $('#yearManufacturing').val(),
						stockCode: $('#stockCode').val().trim(),
						equipLendCode: $("#equipLendCode").val().trim(),
						lstEquipmentCode: lstEquipmentCode
					}
				if($("#idRecordHidden").val() != "" && EquipmentManagerDelivery._lstEquipInRecord != null){
					params.lstEquipDelete = "";
					for(var i=0; i<EquipmentManagerDelivery._lstEquipInRecord.length; i++){
						params.lstEquipDelete += EquipmentManagerDelivery._lstEquipInRecord[i]+',';
					}					
				}
				if(EquipmentManagerDelivery._lstEquipInsert != null){
					params.lstEquipAdd = "";
					for(var i=0; i<EquipmentManagerDelivery._lstEquipInsert.length; i++){
						params.lstEquipAdd += EquipmentManagerDelivery._lstEquipInsert[i]+',';
					}					
				}
				$('#equipmentGridDialog').datagrid({
					url : '/equipment-manage-delivery/search-equipment-delivery',
					autoRowHeight : true,
					rownumbers : true, 
					//fitColumns:true,
					pagination:true,
					scrollbarSize:0,
					pageSize:10,
					width: $('#equipmentGridDialogContainer').width(),
					height: 'auto',
					autoWidth: true,	
					queryParams: params,
					columns:[[
						{field: 'select',title:'',width: 50,sortable:false,resizable:false, align: 'left', formatter: function(value, row, index) {
					    	return '<a href="javascript:void(0);" onclick="EquipmentManagerDelivery.selectEquipment('+index+');">Chọn</a>';
						}},
					    {field: 'equipmentCode',title:'Mã thiết bị',width: 200,sortable:false,resizable:false, align: 'left', formatter: function(value, row, index) {
							return Utils.XSSEncode(row.equipmentCode);
						}},	
					    {field: 'seriNumber',title:'Số serial',width: 100,sortable:false,resizable:false, align: 'left', formatter: function(value, row, index) {
							return Utils.XSSEncode(row.seriNumber);
						}},	
					    {field: 'healthStatus',title:'Tình trạng thiết bị',width: 100,sortable:false,resizable:false, align: 'left', formatter: function(value, row, index) {
							return Utils.XSSEncode(row.healthStatus);
						}},	
					    {field: 'stock',title:'Kho',width: 120,sortable:false,resizable:false, align: 'left', formatter: function(value, row, index) {
					    	var html="";
							if(row.stockCode != null && row.stockName!= null){
								html = row.stockCode+' - '+row.stockName; 
							}
							return Utils.XSSEncode(html);
						}},	
					    {field: 'typeEquipment',title:'Loại thiết bị',width: 80,sortable:false,resizable:false, align: 'left', formatter: function(value, row, index) {
							return Utils.XSSEncode(row.typeEquipment);
						}},	
					    {field: 'groupEquipment',title:'Nhóm thiết bị',width: 200,sortable:false,resizable:false, align: 'left', formatter: function(value, row, index) {
					    	var html="";
							if(row.groupEquipmentCode != null && row.groupEquipmentName!= null){
								html = row.groupEquipmentCode+' - '+row.groupEquipmentName; 
							}
							return Utils.XSSEncode(html);
						}},	
					    {field: 'capacity',title:'Dung tích (lít)',width: 80,sortable:false,resizable:false, align: 'center', formatter: function(value, row, index) {
							return Utils.XSSEncode(row.capacity);
						}},	
					    {field: 'equipmentBrand',title:'Hiệu',width: 80,sortable:false,resizable:false, align: 'left', formatter: function(value, row, index) {
							return Utils.XSSEncode(row.equipmentBrand);
						}},	
					    {field: 'equipmentProvider',title:'NCC',width: 80,sortable:false,resizable:false, align: 'left', formatter: function(value, row, index) {
							return Utils.XSSEncode(row.equipmentProvider);
						}},	
					    {field: 'yearManufacture',title:'Năm sản xuất',width: 80,sortable:false,resizable:false, align: 'left', formatter: function(value, row, index) {
							return Utils.XSSEncode(row.yearManufacture);
						}},											
					]],
					onBeforeLoad:function(param){											 
					},
					onLoadSuccess :function(data){   			 
						if (data == null || data.total == 0) {
							//$("#errMsgInfo").html("Không tìm thấy danh sách biên bản giao nhận thiết bị!").show();
						}else{
							$('.datagrid-header-rownumber').html('STT');		    	
						}	    	
						$('#equipmentCode').focus();	
						$('#easyuiPopupSearchEquipment').dialog('move', {top : 50});
					}
				});
	        },
			onClose: function(){
				$('#easyuiPopupSearchEquipment').hide();
				$('#equipmentCode').val('').change();
				$('#seriNumber').val('').change();
				$('#typeEquipment').html('<option value="" selected="selected">Tất cả</option>');
				// $('#groupEquipment').html('<option value="" selected="selected">Tất cả</option>').change();
				// $('#providerId').html('<option value="" selected="selected">Tất cả</option>');
				$('#yearManufacturing').val('').change();
				$('#stockCode').val('').change();
				$('#equipmentGridDialog').datagrid('loadData',[]);
			}
		});
	},
	/**
	 * Lay danh muc loai, ncc thiet bi
	 * @author nhutnn
	 * @since 20/12/2014
	 */
	getEquipCatalog: function(){
		$('#typeEquipment').html('<option value="" selected="selected">Tất cả</option>');
    	$('#provider').html('<option value=""  selected="selected">Tất cả</option>');
    	var params = new Object();
    	var equipLendCode = $("#equipLendCode").val().trim();
		if(equipLendCode != undefined && equipLendCode != null && equipLendCode != '') {
			params.equipLendCode = equipLendCode;
		}
		params.customerId = $("#customerIdHidden").val();
    	Utils.getJSONDataByAjaxNotOverlay(params, '/equipment-manage-delivery/get-equipment-catalog', function(result){	
		//$.getJSON('/equipment-manage-delivery/get-equipment-catalog', function(result) {				
			if(result.equipsCategory != undefined && result.equipsCategory != null && result.equipsCategory.length > 0) {					
				for(var i=0; i < result.equipsCategory.length; i++){
					$('#typeEquipment').append('<option value="'+ result.equipsCategory[i].id +'">'+ Utils.XSSEncode(result.equipsCategory[i].code) +' - '+ Utils.XSSEncode(result.equipsCategory[i].name) +'</option>');  
				}
			} 
			$('#typeEquipment').change();

			// if(result.equipProviders != undefined && result.equipProviders != null && result.equipProviders.length > 0) {					
			// 	for(var i=0; i < result.equipProviders.length; i++){
			// 		$('#providerId').append('<option value="'+ result.equipProviders[i].id +'">'+ Utils.XSSEncode(result.equipProviders[i].code)+' - '+ Utils.XSSEncode(result.equipProviders[i].name) +'</option>');  
			// 	}
			// } 
			// $('#providerId').change();	
					
			if(result.equipProviders != undefined && result.equipProviders != null && result.equipProviders.length > 0) {					
				for (var i = 0, isize = result.equipProviders.length; i < isize; i++) {
					result.equipProviders[i].searchText = unicodeToEnglish(result.equipProviders[i].code + " " + result.equipProviders[i].name);
 					result.equipProviders[i].searchText = result.equipProviders[i].searchText.toUpperCase();
				}
				var obj = {
					id: -1,
					code: "Tất cả",
					name: "Tất cả",
					codeName: "Tất cả",
					searchText: unicodeToEnglish("Tất cả").toUpperCase()
				};
				result.equipProviders.splice(0, 0, obj);
				$('#providerId').combobox("loadData", result.equipProviders);
		 		$('#providerId').combobox("setValue", -1);
			}
		},null,null);
	},
	// /**
	//  * Lay nhom thiet bi
	//  * @author nhutnn
	//  * @since 12/01/2015
	//  */
	// getEquipGroup: function(typeEquip){
	// 	var params = new Object();
	// 	if(typeEquip != undefined && typeEquip != null && typeEquip != ''){
	// 		params.id = typeEquip;
	// 	}
	// 	var equipLendCode = $("#equipLendCode").val().trim();
	// 	if(equipLendCode != undefined && equipLendCode != null && equipLendCode != '') {
	// 		params.equipLendCode = equipLendCode;
	// 	}
	// 	$('#groupEquipment').html('<option value="" selected="selected">Tất cả</option>');
 //    	Utils.getJSONDataByAjaxNotOverlay(params, '/equipment-manage-delivery/get-equipment-group', function(result){	
	// 	// $.getJSON('/equipment-manage-liquidation/get-equipment-catalog', function(result) {				
	// 		if(result.equipGroups != undefined && result.equipGroups != null && result.equipGroups.length > 0) {					
	// 			for(var i=0; i < result.equipGroups.length; i++){
	// 				$('#groupEquipment').append('<option value="'+ result.equipGroups[i].id +'">'+ Utils.XSSEncode(result.equipGroups[i].code+' - '+result.equipGroups[i].name) +'</option>');  
	// 			}
	// 		} 
	// 		$('#groupEquipment').change();				
	// 	},null,null);
	// },
		/**
	 * Lay nhom thiet bi autocompleteCombobox
	 * @author tamvnm
	 * @since 23/07/2015
	 */
	getEquipGroup: function(typeEquip){
		
		var params = new Object();
		if(typeEquip != undefined && typeEquip != null && typeEquip != ''){
			params.id = typeEquip;
		}
		var equipLendCode = $("#equipLendCode").val().trim();
		var customerId =  $("#customerIdHidden").val();
		if (equipLendCode != undefined && equipLendCode != null && equipLendCode != '') {
			params.equipLendCode = equipLendCode;
		}
		if (customerId != undefined && customerId != null) {
			params.customerId = customerId;
		}

    	Utils.getJSONDataByAjaxNotOverlay(params, '/equipment-manage-delivery/get-equipment-group', function(result){				
			if(result.equipGroups != undefined && result.equipGroups != null && result.equipGroups.length > 0) {	
				for (var i = 0, isize = result.equipGroups.length; i < isize; i++) {
					result.equipGroups[i].searchText = unicodeToEnglish(result.equipGroups[i].code + " " + result.equipGroups[i].name);
	 				result.equipGroups[i].searchText = result.equipGroups[i].searchText.toUpperCase();
				}				
				var obj = {
				id: -1,
				code: "Tất cả",
				name: "Tất cả",
				codeName: "Tất cả",
				searchText: unicodeToEnglish("Tất cả").toUpperCase()
				};
				result.equipGroups.splice(0, 0, obj);
				$('#groupEquipment').combobox("loadData", result.equipGroups);
		 		$('#groupEquipment').combobox("setValue", -1);
			} 
			
		},null,null);
	},
	/**
	 * Xu ly trên input loai thiet bi
	 * @author nhutnn
	 * @since 19/12/2014
	 */
	onChangeTypeEquip: function(cb){
		var typeEquip = $(cb).val().trim();
		EquipmentManagerDelivery.getEquipGroup(typeEquip);
		// $('#groupEquipment').html('<option value="" selected="selected">Tất cả</option>');
		// if(typeEquip != undefined && typeEquip != null && typeEquip != ''){
			// $.getJSON('/equipment-manage-delivery/get-equipment-group?id='+typeEquip, function(result) {				
			// 	if(result.equipGroups != undefined && result.equipGroups != null && result.equipGroups.length > 0) {					
			// 		for(var i=0; i < result.equipGroups.length; i++){
			// 			$('#groupEquipment').append('<option value="'+ result.equipGroups[i].id +'">'+ Utils.XSSEncode(result.equipGroups[i].name) +'</option>');  
			// 		}
			// 	} 
			// 	$('#groupEquipment').change();				
			// });			
		// }
	},

	/**
	 * Tao su kien nut Noi Dung
	 * @author tamvnm
	 * @since 25/06/2015
	 */
	getEventContent: function() {
		$('#contentDelivery').change(function() {
			var seri = $('#seriNumberInGrid' + EquipmentManagerDelivery._editIndex).val();
			var depreciation = $('#depreciationInGrid' + EquipmentManagerDelivery._editIndex).val();
		  	if (this.value != 'DELIVERY_CONTENT_NEW') {
		  		if (EquipmentManagerDelivery._shopToCode == "") {
		  			$('#errMsg').html('Bạn chưa chọn Mã NPP bên mượn').show();
		  			$('#shopToCode').focus();
		  		}
			    disableCombo('groupEquipCBX' + EquipmentManagerDelivery._editIndex);
			    disableCombo('providerEquipCBX' + EquipmentManagerDelivery._editIndex);
			    disableCombo('stockEquipCBX' + EquipmentManagerDelivery._editIndex);
			    disabled('price' + EquipmentManagerDelivery._editIndex);
	      		disabled('healthStatus' + EquipmentManagerDelivery._editIndex);
	      		disabled('yearManufacture' + EquipmentManagerDelivery._editIndex);
	      		enable('equipmentCodeInGrid' + EquipmentManagerDelivery._editIndex);
			} else {
			  	$('#gridEquipmentDelivery').datagrid('deleteRow',EquipmentManagerDelivery._editIndex);
			  	EquipmentManagerDelivery.addEquipmentInRecord();
			    $('#contentDelivery').val(this.value);
			    enableCombo('groupEquipCBX' + EquipmentManagerDelivery._editIndex);
			    enableCombo('providerEquipCBX' + EquipmentManagerDelivery._editIndex);
			    enableCombo('stockEquipCBX' + EquipmentManagerDelivery._editIndex);
			    enable('price' + EquipmentManagerDelivery._editIndex);
	            enable('healthStatus' + EquipmentManagerDelivery._editIndex);
	            enable('yearManufacture' + EquipmentManagerDelivery._editIndex);
	            disabled('equipmentCodeInGrid' + EquipmentManagerDelivery._editIndex);
			}  
			if (seri != undefined) {
				$('#seriNumberInGrid' + EquipmentManagerDelivery._editIndex).val(seri);
			}
			if (depreciation != undefined) {
				$('#depreciationInGrid' + EquipmentManagerDelivery._editIndex).val(depreciation);
			}
		});
		
		if ($('#price'+ EquipmentManagerDelivery._editIndex).val() != undefined && $('#price'+ EquipmentManagerDelivery._editIndex).val().indexOf(',')==-1) {
			$('#price'+ EquipmentManagerDelivery._editIndex).val(formatCurrency($('#price'+ EquipmentManagerDelivery._editIndex).val()));
			Utils.formatCurrencyFor('price'+ EquipmentManagerDelivery._editIndex);
			Utils.bindFormatOnTextfield('price'+ EquipmentManagerDelivery._editIndex,Utils._TF_NUMBER_COMMA);
		}
	},
	/**
	 * Khoi tao lai giao dien
	 * @author tamvnm
	 * @since 25/06/2015
	 */
	setUIEquipInRecord: function () {
		Utils.bindComboboxEquipGroupEasyUI('groupEquipCBX' + EquipmentManagerDelivery._editIndex);
		EquipmentManagerDelivery.setEquipGroup();
		Utils.bindComboboxEquipGroupEasyUI('providerEquipCBX' + EquipmentManagerDelivery._editIndex);
		EquipmentManagerDelivery.setEquipProvider();
		Utils.bindComboboxEquipGroupEasyUI('stockEquipCBX' + EquipmentManagerDelivery._editIndex);
		EquipmentManagerDelivery.setEquipStock();
		disableCombo('groupEquipCBX' + EquipmentManagerDelivery._editIndex);
		disableCombo('providerEquipCBX' + EquipmentManagerDelivery._editIndex);
		disableCombo('stockEquipCBX' + EquipmentManagerDelivery._editIndex);

		if ($('#price'+ EquipmentManagerDelivery._editIndex).val().indexOf(',') == -1) {
			$('#price'+ EquipmentManagerDelivery._editIndex).val(formatCurrency($('#price'+ EquipmentManagerDelivery._editIndex).val()));
			Utils.formatCurrencyFor('price'+ EquipmentManagerDelivery._editIndex);
			Utils.bindFormatOnTextfield('price'+ EquipmentManagerDelivery._editIndex,Utils._TF_NUMBER_COMMA);
		}
		disabled('price' + EquipmentManagerDelivery._editIndex);
		disabled('healthStatus' + EquipmentManagerDelivery._editIndex);
	},
	/**
	 * Chon thiet bi
	 * @author nhutnn
	 * @since 12/12/2014
	 */
	selectEquipment: function(index){
		var rowPopup = $('#equipmentGridDialog').datagrid('getRows')[index];
		var tempDep = null;
		var temContent = $('#contentDelivery').val();
		rowPopup.contentDelivery = null;

		if ($('#depreciationInGrid' + EquipmentManagerDelivery._editIndex) != undefined && $('#depreciationInGrid' + EquipmentManagerDelivery._editIndex).val() != '') {
			tempDep = $('#depreciationInGrid' + EquipmentManagerDelivery._editIndex).val();
		}
		
		if(EquipmentManagerDelivery._editIndex!=null){
			$('#gridEquipmentDelivery').datagrid('updateRow',{
				index: EquipmentManagerDelivery._editIndex,	// index start with 0
				row: rowPopup
			});
			$('#contentDelivery').width($('#contentDelivery').parent().width());
			$('#equipmentCodeInGrid' + EquipmentManagerDelivery._editIndex).width($('#equipmentCodeInGrid' + EquipmentManagerDelivery._editIndex).parent().width());
			$('#seriNumberInGrid').width($('#seriNumberInGrid').parent().width());
			$('#stock').width($('#stock').parent().width());
			if ($('#depreciationInGrid' + EquipmentManagerDelivery._editIndex) != undefined && tempDep != null) {
				$('#depreciationInGrid' + EquipmentManagerDelivery._editIndex).val(tempDep);
			}
			EquipmentManagerDelivery.editInputInGrid();		
			EquipmentManagerDelivery.getEventContent();
			$('#contentDelivery').val(temContent);
			// $('#contentDelivery').change(function() {
			// 	  if (this.value != 'DELIVERY_CONTENT_NEW') {
			// 	      $('#gridEquipmentDelivery').datagrid('deleteRow',EquipmentManagerDelivery._editIndex);
			// 	  	  EquipmentManagerDelivery.addEquipmentInRecord();
			// 	      $('#contentDelivery').val(this.value);
			// 	      disableCombo('groupEquipCBX' + EquipmentManagerDelivery._editIndex);
			// 	      disableCombo('providerEquipCBX' + EquipmentManagerDelivery._editIndex);
			// 	      console.log('khong phai cap moi');
			// 	      disabled('price' + EquipmentManagerDelivery._editIndex);
		 //      		  disabled('healthStatus' + EquipmentManagerDelivery._editIndex);
		 //      		  enable('equipmentCodeInGrid' + EquipmentManagerDelivery._editIndex);
			// 	  } else {
			// 	  	$('#gridEquipmentDelivery').datagrid('deleteRow',EquipmentManagerDelivery._editIndex);
			// 	  	EquipmentManagerDelivery.addEquipmentInRecord();
			// 	    $('#contentDelivery').val(this.value);
			// 	    enableCombo('groupEquipCBX' + EquipmentManagerDelivery._editIndex);
			// 	    enableCombo('providerEquipCBX' + EquipmentManagerDelivery._editIndex);
			// 	    console.log('cap moi');
			// 	    enable('price' + EquipmentManagerDelivery._editIndex);
		 //            enable('healthStatus' + EquipmentManagerDelivery._editIndex);
		 //            disabled('equipmentCodeInGrid' + EquipmentManagerDelivery._editIndex);
			// 	  }  
			// });
		}
		
		$('#easyuiPopupSearchEquipment').dialog('close');
	},
	/**
	 * Tim kiem thiet bi
	 * @author nhutnn
	 * @since 12/12/2014
	 */
	searchEquipment: function(){
		var params=new Object(); 
		params.equipCode = $('#equipmentCode').val().trim();
		params.seriNumber = $('#seriNumber').val().trim();
		params.categoryId = $('#typeEquipment').val().trim();
		params.customerId = $('#customerIdHidden').val();

		//tamvnm: thay doi thanh autoCompleteCombobox: -1 la truong hop tim group ko co trong BD
		// params.groupId = $('#groupEquipment').val().trim();
		if ($('#groupEquipment').combobox('getValue') == "") {
			$('#groupEquipment').combobox('setValue', -1);
			params.groupId = "";
		} else if ($('#groupEquipment').combobox('getValue') == -1) {
			params.groupId = "";
		} else {
			if (Utils.getMessageOfInvaildNumberNew($('#groupEquipment').combobox("getValue")).length == 0) {
				params.groupId = $('#groupEquipment').combobox("getValue");
			} else {
				params.groupId = -1;
			}
		}
		// params.providerId = $('#providerId').val().trim();
		if ($('#providerId').combobox('getValue') == "") {
			$('#providerId').combobox('setValue', -1);
			params.providerId = "";
		} else if ($('#providerId').combobox('getValue') == -1) {
			params.providerId = "";
		} else {
			if (Utils.getMessageOfInvaildNumberNew($('#providerId').combobox("getValue")).length == 0) {
				params.providerId = $('#providerId').combobox("getValue");
			} else {
				params.providerId = -1;
			}
		}
		
		params.yearManufacture = $('#yearManufacturing').val().trim();
		params.stockCode = $('#stockCode').val().trim();
		params.equipLendCode = $("#equipLendCode").val().trim();
		var rowEquip = $('#gridEquipmentDelivery').datagrid('getRows');
    	var lstEquipmentCode = '';
    	for (var i = 0; i < rowEquip.length; i++) {
    		// lstEquipmentCode.push(rowEquip[i].equipmentCode);
    		if (rowEquip[i].equipmentCode != null) {
        		if (i < rowEquip.length - 1) {
        			lstEquipmentCode += rowEquip[i].equipmentCode + ', ';
        		} else {
        			lstEquipmentCode += rowEquip[i].equipmentCode;
        		}
    		}
    	}
    	params.lstEquipmentCode = lstEquipmentCode;
		if($("#idRecordHidden").val() != "" && EquipmentManagerDelivery._lstEquipInRecord != null){
			params.lstEquipDelete = "";
			for(var i=0; i<EquipmentManagerDelivery._lstEquipInRecord.length; i++){
				params.lstEquipDelete += EquipmentManagerDelivery._lstEquipInRecord[i]+',';
			}					
		}
		if(EquipmentManagerDelivery._lstEquipInsert != null){
			params.lstEquipAdd = "";
			for(var i=0; i<EquipmentManagerDelivery._lstEquipInsert.length; i++){
				params.lstEquipAdd += EquipmentManagerDelivery._lstEquipInsert[i]+',';
			}					
		}
		$('#equipmentGridDialog').datagrid('load',params);
	},

	/**
	 * Show popup chon kho
	 * @author nhutnn
	 * @since 12/12/2014
	 */
	showPopupStock:function(txtInput){
	// $('#easyuiPopupSearchStock').show();
	// 	$('#easyuiPopupSearchStock').dialog({  
	//         closed: false,  
	//         cache: false,  
	//         modal: true,
	//         width : 600,
	//         height : 'auto',
	//         onOpen: function(){	
	// 			$('#shopCodePopupStock').focus();
	// 			$('#shopCodePopupStock, #shopNamePopupStock').bind('keyup',function(event){
	// 				if(event.keyCode == keyCodes.ENTER){
	// 					$('#btnSearchStockDlg').click(); 
	// 				}
	// 			});	
	// 			$('#stockGridDialog').datagrid({
	// 				url : '/commons/search-shop-show-list-NPP',
	// 				autoRowHeight : true,
	// 				rownumbers : true, 
	// 				fitColumns:true,
	// 				scrollbarSize:0,
	// 				pagination:true,
	// 				pageSize: 10,
	// 				pageList: [10],
	// 				width: $('#stockGridDialogContainer').width(),
	// 				height: 'auto',
	// 				autoWidth: true,	
	// 				queryParams:{
	// 					status : 1,
	// 					isUnionShopRoot: true,
	// 					code: $('#shopCodePopupStock').val().trim(),
	// 					name: $('#shopNamePopupStock').val().trim()
	// 				},
	// 				columns:[[
	// 					{field:'shopCode', title:'Mã Đơn vị', align:'left', width: 120, sortable:false, resizable:false},
	// 					{field:'shopName', title:'Tên Đơn vị', align:'left', width: 200, sortable:false, resizable:false},
	// 					{field:'choose', title:'', align:'center', width:40, sortable:false, resizable:false, formatter:function(value,row,index) {
	// 						var html =  "<a href='javascript:void(0)' onclick=\"return EquipmentManagerDelivery.selectStock('"+ row.shopCode + "');\">Chọn</a>";
	// 						return html;
	// 					}}				
	// 				]],
	// 				onBeforeLoad:function(param){											 
	// 				},
	// 				onLoadSuccess :function(data){   			 
	// 					if (data == null || data.total == 0) {
	// 						//$("#errMsgInfo").html("Không tìm thấy danh sách biên bản giao nhận thiết bị!").show();
	// 					}else{
	// 						$('.datagrid-header-rownumber').html('STT');		    	
	// 					}	    		
	// 				}
	// 			});				
	//         },
	// 		onClose: function(){
	// 			$('#easyuiPopupSearchStock').hide();
	// 			$('#shopCodePopupStock').val('').change();
	// 			$('#shopNamePopupStock').val('').change();
	// 		}
	// 	});		
	// 	return false;
		var txt = "stockInGrid";
		if(txtInput!=undefined){
			txt = txtInput;
		}
		$('#common-dialog-search-2-textbox').dialog("close");
		$('#common-dialog-search-2-textbox').remove();
		VCommonJS.showDialogSearch2({
			inputs : [
		        {id:'code', maxlength:50, label:'Mã đơn vị'},
		        {id:'name', maxlength:250, label:'Tên đơn vị'},
		    ],
		    url : '/commons/get-equipment-stock',
		    //isCenter : true,
		    columns : [[
		        {field:'codeName', title:'Đơn vị', align:'left', width: 110, sortable:false, resizable:false, formatter:function(value,row,index) {
		        	var nameCode = row.shopCode + '-' + row.shopName;
		        	return Utils.XSSEncode(nameCode);         
		        }},
		        {field:'equipStockCode', title:'Mã kho', align:'left', width: 200, sortable:false, resizable:false},
		        {field:'choose', title:'', align:'center', width:40, sortable:false, resizable:false, formatter:function(value,row,index) {
//			        	var nameCode = row.shopCode + '-' + row.name;
		            return '<a href="javascript:void(0)" onclick="Utils.fillCodeForInputTextByDialogSearch2(\''+txt+'\', ' + row.equipStockId +', \''+row.equipStockCode+'\', \''+row.name+'\','+Utils._isShowInputTextByDlSearch2.code+');">chọn</a>';         
		        }}
		    ]]
		});
	},
	/**
	 * Chon kho
	 * @author nhutnn
	 * @since 12/12/2014
	 */
	selectStock: function(shopCode){
		$('#stockCode').val(shopCode);		
		$('#easyuiPopupSearchStock').dialog('close');
	},
	/**
	 * Tim kiem kho
	 * @author nhutnn
	 * @since 12/12/2014
	 */
	searchStock: function(){
		var params=new Object(); 
		params.status=1;
		params.isUnionShopRoot = true;
		params.code = $('#shopCodePopupStock').val().trim();
		params.name = $('#shopNamePopupStock').val().trim();
		
		$('#stockGridDialog').datagrid('load',params);
	},
	/**
	 * Show popup ma va so seri thiet bi
	 * @author nhutnn
	 * @since 12/12/2014
	 */
	showPopupSelectEquipment:function(dataGrid){
		$('#easyuiPopupSelectEquipment').show();
		$('#easyuiPopupSelectEquipment').dialog({  
	        closed: false,  
	        cache: false,  
	        modal: true,
	        width : 450,
	        height : 'auto',
	        onOpen: function(){	        	
				$('#gridSelectEquipment').datagrid({
					//url : '/equipment-manage-delivery/search-seri-equipment',
					data: dataGrid,
					autoRowHeight : true,
					rownumbers:true,
					fitColumns:true,
					scrollbarSize:0,					
					width: $('#gridSelectEquipmentContainer').width(),
					autoWidth: true,
					columns:[[
					    {field: 'equipmentCode',title:'Mã thiết bị',width: 250,sortable:false,resizable:false, align: 'left' },	
					    {field: 'seriNumber',title:'Số serial',width: 100,sortable:false,resizable:false, align: 'left' },	
					    {field: 'select',title:'',width: 50,sortable:false,resizable:false, align: 'center', formatter: function(value, row, index) {
					    	return '<a href="javascript:void(0);" onclick="EquipmentManagerDelivery.selectEquipmentCode('+index+');">Chọn</a>';
						}}			    
					]],
					onBeforeLoad:function(param){											 
					},
					onLoadSuccess :function(data){   			 
						$('.easyui-dialog .datagrid-header-rownumber').html('STT');				
					}
				});
	        },
	        onClose: function(){
	        	$('#easyuiPopupSelectEquipment').hide();
	        }
		});
	},
	/**
	 * Chon ma, so seri thiet bi
	 * @author nhutnn
	 * @since 12/12/2014
	 */
	selectEquipmentCode: function(index){
		var rowPopup = $('#gridSelectEquipment').datagrid('getRows')[index];
		rowPopup.contentDelivery = null;
		var tempDep = null;
		if ($('#depreciationInGrid' + EquipmentManagerDelivery._editIndex) != undefined && $('#depreciationInGrid' + EquipmentManagerDelivery._editIndex).val() != '') {
			tempDep = $('#depreciationInGrid' + EquipmentManagerDelivery._editIndex).val();
		}
		if(EquipmentManagerDelivery._editIndex!=null){
			$('#gridEquipmentDelivery').datagrid('updateRow',{
				index: EquipmentManagerDelivery._editIndex,	// index start with 0
				row: rowPopup
			});
			$('#contentDelivery').width($('#contentDelivery').parent().width());
			$('#equipmentCodeInGrid' + EquipmentManagerDelivery._editIndex).width($('#equipmentCodeInGrid' + EquipmentManagerDelivery._editIndex).parent().width());
			$('#seriNumberInGrid').width($('#seriNumberInGrid').parent().width());
			$('#stock').width($('#stock').parent().width());
			if ($('#depreciationInGrid' + EquipmentManagerDelivery._editIndex) != undefined && tempDep != null) {
				$('#depreciationInGrid' + EquipmentManagerDelivery._editIndex).val(tempDep);
			}
			EquipmentManagerDelivery.editInputInGrid();
		}
		$('#easyuiPopupSelectEquipment').dialog('close');
	},
	/**
	 * Tao moi bien ban
	 * @author nhutnn
	 * @since 18/12/2014
	 */
	// createRecordDelivery: function(){
	// 	$('#errMsgInfo').html('').hide();
	// 	var msg ='';
	// 	var err = '';
	// 	var nContractTrim = $('#numberContract').val().trim();
	// 	$('#numberContract').val(nContractTrim);
	// 	if(msg.length == 0 && $('#numberContract').val().indexOf(' ')!=-1){
	// 		msg = 'Số hợp đồng không được có khoảng trắng';
	// 		err = '#numberContract';
	// 	}
	// 	if(msg.length == 0){
	// 		msg = Utils.getMessageOfSpecialCharactersValidate('numberContract','Số hợp đồng',Utils._NAME_CUSTYPE);
	// 		err = '#numberContract';
	// 	}
	// 	if(msg.length == 0){
	// 		msg = Utils.getMessageOfRequireCheck('numberContract','Số hợp đồng');
	// 		err = '#numberContract';
	// 	}	
	// 	if(msg.length == 0){
	// 		msg = Utils.getMessageOfRequireCheck('contractDate','Ngày hợp đồng');
	// 		err = '#contractDate';
	// 	}				
	// 	var contractDate = $('#contractDate').val().trim();
	// 	if(msg.length == 0 && contractDate != '' && !Utils.isDate(contractDate)){
	// 		msg = 'Ngày hợp đồng không tồn tại hoặc không đúng định dạng dd/mm/yyyy.';
	// 		err = '#contractDate';
	// 	}	

	// 	if ($('#shopFromCode').val() == '' && msg.length == 0) {
	// 		msg = 'Bạn chưa nhập giá trị cho trường "Đơn vị" BÊN CHO MƯỢN';
	// 		err = '#shopFromCode';
	// 	}
	// 	if ($('#address').val() == '' && msg.length == 0) {
	// 		msg = 'Bạn chưa nhập giá trị cho trường "Địa chỉ" BÊN CHO MƯỢN';
	// 		err = '#address';
	// 	}
	// 	if ($('#taxCode').val() == '' && msg.length == 0) {
	// 		msg = 'Bạn chưa nhập giá trị cho trường "Mã số thuế" BÊN CHO MƯỢN';
	// 		err = '#taxCode';
	// 	}
	// 	if ($('#numberPhone').val() == '' && msg.length == 0) {
	// 		msg = 'Bạn chưa nhập giá trị cho trường "Điện thoại" BÊN CHO MƯỢN';
	// 		err = '#numberPhone';
	// 	}
	// 	if ($('#fromRepresentative').val() == '' && msg.length == 0) {
	// 		msg = 'Bạn chưa nhập giá trị cho trường "Người đại diện" BÊN CHO MƯỢN';
	// 		err = '#fromRepresentative';
	// 	}
	// 	if ($('#fromPosition').val() == '' && msg.length == 0) {
	// 		msg = 'Bạn chưa nhập giá trị cho trường "Chức vụ" BÊN CHO MƯỢN';
	// 		err = '#fromPosition';
	// 	}
	// 	if ($('#fromPage').val() == '' && msg.length == 0) {
	// 		msg = 'Bạn chưa nhập giá trị cho trường "Giấy ủy quyền" BÊN CHO MƯỢN';
	// 		err = '#fromPage';
	// 	}
	// 	if ($('#fromPagePlace').val() == '' && msg.length == 0) {
	// 		msg = 'Bạn chưa nhập giá trị cho trường "Nơi cấp giấy quỷ quyền" BÊN CHO MƯỢN';
	// 		err = '#fromPagePlace';
	// 	}
	// 	var fromPDate = $('#pageDate').val().trim();
	// 	if(msg.length == 0 && fromPDate != '' && !Utils.isDate(fromPDate)){
	// 		msg = 'Ngày cấp GPKD không tồn tại hoặc không đúng định dạng dd/mm/yyyy.';
	// 		err = '#pageDate';
	// 	}	
	// 	if(msg.length == 0){
	// 		msg = Utils.getMessageOfRequireCheck('pageDate','Ngày cấp giấy ủy quyền" BÊN CHO MƯỢN');
	// 		err = '#pageDate';
	// 	}

	// 	if(msg.length == 0){
	// 		msg = Utils.getMessageOfSpecialCharactersValidate('shopToCode','Mã đơn vị BÊN MƯỢN',Utils._CODE);
	// 		err = '#shopToCode';
	// 	}
	// 	if(msg.length == 0){
	// 		msg = Utils.getMessageOfRequireCheck('shopToCode','Mã đơn vị BÊN MƯỢN');
	// 		err = '#shopToCode';
	// 	}	
	// 	if(msg.length == 0){
	// 		msg = Utils.getMessageOfRequireCheck('customerCode','Mã khách hàng');
	// 		err = '#customerCode';
	// 	}	
	// 	if(msg.length == 0){
	// 		msg = Utils.getMessageOfSpecialCharactersValidate('customerCode','Mã khách hàng',Utils._CODE);
	// 		err = '#customerCode';
	// 	}
	// 	if ($('#toObjectAddress').val() == '' && msg.length == 0) {
	// 		msg = 'Bạn chưa nhập giá trị cho trường "Địa chỉ đặt tủ" BÊN MƯỢN';
	// 		err = '#toObjectAddress';
	// 	}
	// 	// if ($('#toBusinessLicense').val() == '' && msg.length == 0) {
	// 	// 	msg = 'Bạn chưa nhập giá trị cho trường "Số GP ĐKKD" BÊN MƯỢN';
	// 	// 	err = '#toBusinessLicense';
	// 	// }
	// 	// if ($('#toBusinessPlace').val() == '' && msg.length == 0) {
	// 	// 	msg = 'Bạn chưa nhập giá trị cho trường "Nơi cấp GP ĐKKD BÊN MƯỢN';
	// 	// 	err = '#toBusinessPlace';
	// 	// }
	// 	var toBusinessDate = $('#toBusinessDate').val().trim();
	// 	if(msg.length == 0 && toBusinessDate != '' && !Utils.isDate(toBusinessDate)){
	// 		msg = 'Ngày cấp GPKD không tồn tại hoặc không đúng định dạng dd/mm/yyyy.';
	// 		err = '#toBusinessDate';
	// 	}	
	// 	// if(msg.length == 0){
	// 	// 	msg = Utils.getMessageOfRequireCheck('toBusinessDate','Ngày cấp GPKD');
	// 	// 	err = '#toBusinessDate';
	// 	// }
	// 	if ($('#toRepresentative').val() == '' && msg.length == 0) {
	// 		msg = 'Bạn chưa nhập giá trị cho trường "Người đại diện" BÊN MƯỢN';
	// 		err = '#toRepresentative';
	// 	}
	// 	if ($('#toPosition').val() == '' && msg.length == 0) {
	// 		msg = 'Bạn chưa nhập giá trị cho trường "Chức vụ người đại diện" BÊN MƯỢN';
	// 		err = '#toPosition';
	// 	}
	// 	if ($('#staffMonitorCode').combobox('getValue') == '' && msg.length == 0) {
	// 		msg = 'Bạn chưa nhập giá trị cho trường "Mã giám sát" BÊN MƯỢN';
	// 		err = '#staffMonitorCode';
	// 	}

	// 	// var isStaff = false;
	// 	// var shopToCode = $('#shopToCode').val().trim();
	// 	// if(shopToCode != undefined && shopToCode != null && shopToCode != ''){
	// 	// 	 $.getJSON('/equipment-manage-delivery/getListGSNPP?toShopCode='+shopToCode, function(result) {
	// 	// 		 if(result.rows != undefined && result.rows != null && result.rows.length > 0) {
	// 	// 		 	for(var i = 0, sz = result.rows.length; i < sz; i++) {
	// 	// 		 		if ($('#staffMonitorCode').combobox('getValue').trim() == result.rows[i].staffCode) {
	// 	// 					isStaff = true;
	// 	// 				}
	// 	// 		 	}
	// 	// 		 }

	// 	// 	});
	// 	// }
	// 	// if (!isStaff) {
	// 	// 	msg = 'Mã giám sát không quản lý "Đơn vị" BÊN MƯỢN';
	// 	// 	err = '#staffMonitorCode';
	// 	// }

	// 	var freezer = Utils.returnMoneyValue($('#freezer').val().trim());
	// 	var refrigerator = Utils.returnMoneyValue($("#refrigerator").val().trim());
	// 	var intFreezer = parseInt(freezer);
	// 	var intRefrigerator = parseInt(refrigerator);
	// 	if (msg.length == 0) {
	// 		if(freezer != '') {
	// 			if (intFreezer != freezer) {
	// 				msg = 'Tủ mát phải là số nguyên dương có tối đa 17 chữ số';
	// 				err = '#freezer';
	// 			} else if (intFreezer < 0  || intFreezer > 99999999999999999) {
	// 				msg = 'Tủ mát phải là số nguyên dương có tối đa 17 chữ số';
	// 				err = '#freezer';
	// 			}
	// 		} else {
	// 			msg = 'Tủ mát phải là số nguyên dương có tối đa 17 chữ số';
	// 			err = '#freezer';
	// 		}
	// 	}
	// 	if (msg.length == 0) {
	// 		if (refrigerator != '') {
	// 			if (intRefrigerator != refrigerator) {
	// 				msg = 'Tủ đông phải là số nguyên dương có tối đa 17 chữ số';
	// 				err = '#refrigerator';
	// 			} else if (intRefrigerator < 0  || intRefrigerator > 99999999999999999) {
	// 				msg = 'Tủ đông phải là số nguyên dương có tối đa 17 chữ số';
	// 				err = '#refrigerator';
	// 			}
	// 		} else {
	// 			msg = 'Tủ đông phải là số nguyên dương có tối đa 17 chữ số';
	// 				err = '#refrigerator';
	// 		}
	// 	}


	// 	if(msg.length > 0){
	// 		$('#errMsgInfo').html(msg).show();
	// 		$(err).focus();
	// 		return false;
	// 	}
		
	// 	var rowEquip = $('#gridEquipmentDelivery').datagrid('getRows');
	// 	if(rowEquip.length == 0){
	// 		$('#errMsgInfo').html('Không có thiết bị để cập nhật').show();
	// 		return false;
	// 	}
	// 	//var lstIdEquip = new Array();
	// 	var lstEquipCode = new Array();
	// 	var lstSeriNumber = new Array();
	// 	var lstContentDelivery = new Array();
	// 	var lstStockCode = new Array();
	// 	var lstDepreciation = new Array();
	// 	for (var i = 0; i < rowEquip.length; i++) {
	// 		if (rowEquip[i].seriNumber == null || rowEquip[i].depreciation == null) {
	// 			if ((rowEquip[i].equipmentId != null && rowEquip[i].equipmentCode == null) || rowEquip[i].equipmentId == undefined) {
	// 				lstEquipCode.push($('#equipmentCodeInGrid' + EquipmentManagerDelivery._editIndex).val());
	// 			} else {
	// 				lstEquipCode.push(rowEquip[i].equipmentCode);
	// 			}
	// 			if (rowEquip[i].contentDelivery != null) {
	// 				lstContentDelivery.push(rowEquip[i].contentDelivery);
	// 			} else if ($('#contentDelivery').val() != undefined) {
	// 				lstContentDelivery.push($('#contentDelivery').val());
	// 			}
	// 			if (rowEquip[i].seriNumber != null) {
	// 				lstSeriNumber.push(rowEquip[i].seriNumber);
	// 			} else {
	// 				if ($('#seriNumberInGrid' + i).val() != undefined && $('#seriNumberInGrid' + i).val() != null) {
	// 					lstSeriNumber.push($('#seriNumberInGrid' + i).val().trim());	
	// 				} else {
	// 					lstSeriNumber.push($('#seriNumberInGrid' + i).val());
	// 				}
	// 			}
	// 			if (rowEquip[i].depreciation != null) {
	// 				lstDepreciation.push(rowEquip[i].depreciation);
	// 			} else {
	// 				var depreciation = null;
	// 				if ($('#depreciationInGrid' + i).val() != undefined && $('#depreciationInGrid' + i).val() != null) {
	// 					depreciation = $('#depreciationInGrid' + i).val().trim();
	// 				} else {
	// 					depreciation = $('#depreciationInGrid' + i).val();
	// 				}
					 
	// 				var intDepreciation = parseInt(depreciation);
	// 				if ((depreciation != null && depreciation != intDepreciation) || intDepreciation < 0) {
	// 					msg = 'Vui lòng nhập Số tháng khấu hao là số nguyên dương';
	// 					err = '#depreciationInGrid' + i;
	// 					$('#errMsgInfo').html(msg).show();
	// 					$(err).focus();
	// 					return false;
	// 				} else {
	// 					if ($('#depreciationInGrid' + i).val() != undefined && $('#depreciationInGrid' + i).val() != null) {
	// 						lstDepreciation.push($('#depreciationInGrid' + i).val().trim());
	// 					} else {
	// 						lstDepreciation.push($('#depreciationInGrid' + i).val());
	// 					}
						
	// 				}
	// 			}				
	// 		} else if (i >= EquipmentManagerDelivery._numberEquipInRecord) {
	// 			if ((rowEquip[i].equipmentId != null && rowEquip[i].equipmentCode == null) || rowEquip[i].equipmentId == undefined) {
	// 				lstEquipCode.push($('#equipmentCodeInGrid' + EquipmentManagerDelivery._editIndex).val());
	// 			} else {
	// 				lstEquipCode.push(rowEquip[i].equipmentCode);
	// 			}
	// 			if (rowEquip[i].contentDelivery != null) {
	// 				lstContentDelivery.push(rowEquip[i].contentDelivery);
	// 			} else if ($('#contentDelivery').val() != undefined) {
	// 				lstContentDelivery.push($('#contentDelivery').val());
	// 			}
	// 			if (rowEquip[i].seriNumber == null) {
	// 				lstSeriNumber.push('');
	// 			} else {
	// 				lstSeriNumber.push(rowEquip[i].seriNumber);
	// 			}
				
	// 			lstDepreciation.push(rowEquip[i].depreciation);			
	// 		}	
	// 	}
		
	// 	// if ($('#equipmentCodeInGrid' + EquipmentManagerDelivery._editIndex).val() != undefined && $('#equipmentCodeInGrid' + EquipmentManagerDelivery._editIndex).val() != '') {
	// 	// 	var rowEq = $('#gridEquipmentDelivery').datagrid('getRows');
	// 	// 	for (var i=0; i < rowEq.length-1; i++){
	// 	// 		if (rowEq[i].equipmentCode == $('#equipmentCodeInGrid' + EquipmentManagerDelivery._editIndex).val().trim()) {
	// 	// 			$('#errMsgInfo').html("Mã thiết bị "+rowEq[i].equipmentCode+" đã tồn tại!").show();
	// 	// 			return false;
	// 	// 		}
	// 	// 	}
	// 	// 	lstEquipCode.push($('#equipmentCodeInGrid' + EquipmentManagerDelivery._editIndex).val());
	// 	// } else if ($('#equipmentCodeInGrid' + EquipmentManagerDelivery._editIndex).val() == '') {
	// 	// 	$('#errMsgInfo').html('Bạn chưa xử lý xong dòng thiết bị hiện tại!').show();
	// 	// 	return false;
	// 	// } else if (EquipmentManagerDelivery._numberEquipInRecord == null || 
	// 	// 	rowEquip.length-1 >= EquipmentManagerDelivery._numberEquipInRecord) {
	// 	// 		lstEquipCode.push(rowEquip[rowEquip.length-1].equipmentCode);
	// 	// }

	// 	// if ($('#contentDelivery').val() != undefined){
	// 	// 	lstContentDelivery.push($('#contentDelivery').val());
	// 	// } else {
	// 	// 	lstContentDelivery.push(rowEquip[rowEquip.length-1].contentDelivery);
	// 	// }
	// 	if ($('#stockInGrid').val() != undefined && $('#stock').val() != '') {
	// 		lstStockCode.push($('#stockInGrid').val());
	// 	} else {
	// 		lstStockCode.push(rowEquip[rowEquip.length-1].stockCode);
	// 	}
	// 	// if(rowEquip[rowEquip.length-1].equipmentId != undefined){
	// 	// 	lstIdEquip.push(rowEquip[rowEquip.length-1].equipmentId);
	// 	// }else{
	// 	// 	lstIdEquip.push(-1);
	// 	// }
		
	// 	var params=new Object(); 
	// 	params.shopFromCode = $('#shopFromCode').val().trim();
	// 	params.shopToCode = $('#shopToCode').val().trim();
	// 	params.customerCode = $('#customerCode').val().trim();
	// 	params.numberContract = $('#numberContract').val().trim();
	// 	params.contractDate = $('#contractDate').val().trim();
	// 	params.statusRecord = $('#status').val().trim();
	// 	params.statusDelivery = $('#statusDelivery').val().trim();
		
	// 	params.fromObjectAddress = $("#address").val().trim();
	// 	params.fromObjectTax  = $("#taxCode").val().trim();
	// 	params.fromObjectPhone  = $("#numberPhone").val().trim();
	// 	params.fromRepresentative = $("#fromRepresentative").val().trim();
	// 	params.fromObjectPosition = $("#fromPosition").val().trim();
	// 	params.fromPage = $("#fromPage").val().trim();
	// 	params.fromPagePlace = $("#fromPagePlace").val().trim();
	// 	params.fromPageDate = $("#pageDate").val().trim();
	// 	params.fromFax = $('#fromFax').val();
	// 	params.toObjectAddress = $("#toObjectAddress").val().trim();
	// 	params.toRepresentative = $("#toRepresentative").val().trim();
	// 	params.toObjectPosition = $("#toPosition").val().trim();
	// 	params.toIdNO = $("#toIdNO").val().trim();
	// 	params.toIdNODate = $("#idNODate").val().trim();
	// 	params.toIdNOPlace = $("#toIdNOPlace").val().trim();
	// 	params.freezer = Utils.returnMoneyValue($('#freezer').val().trim());
	// 	params.refrigerator = Utils.returnMoneyValue($("#refrigerator").val().trim());
	// 	params.fromShopCode = $("#shopFromCode").val().trim();
	// 	params.toShopCode = $("#shopToCode").val().trim();
	// 	params.staffCode = $('#staffMonitorCode').combobox('getValue').trim();
	// 	params.toBusinessLicense = $('#toBusinessLicense').val().trim();
	// 	params.toBusinessDate = $('#toBusinessDate').val().trim();
	// 	params.toBusinessPlace = $('#toBusinessPlace').val().trim();
		
	// 	// params.lstIdEquip = lstIdEquip;
	// 	if(lstEquipCode.length > 0){
	// 		params.lstEquipCode = lstEquipCode;
	// 		params.lstSeriNumber = lstSeriNumber;
	// 		params.lstContentDelivery = lstContentDelivery;	
	// 		params.lstDepreciation = lstDepreciation;
	// 	}	

	// 	if (params.statusRecord == 2 && lstSeriNumber.length > 0) {
	// 		for (var i = 0; i < lstSeriNumber.length; i++) {
	// 			if (lstSeriNumber[i] == null || lstSeriNumber[i] == '') {
	// 				$('#errMsgInfo').html("Vui lòng nhập đầy đủ thông tin seri khi chọn trạng thái Duyệt").show();
	// 				return false;
	// 			}
	// 		}
	// 	}
		
	// 	if ($('#equipmentCodeInGrid' + EquipmentManagerDelivery._editIndex).val() == '') {
	// 			$('#errMsgInfo').html('Bạn chưa nhập mã thiết bị').show();
	// 			$('#equipmentCodeInGrid' + EquipmentManagerDelivery._editIndex).focus();
	// 			return false;
	// 	}
	// 	if ($('#depreciationInGrid' + EquipmentManagerDelivery._editIndex).val() == '') {
	// 		$('#errMsgInfo').html('Bạn chưa nhập số tháng khấu hao').show();
	// 		$('#depreciationInGrid' + EquipmentManagerDelivery._editIndex).focus();
	// 		return false;
	// 	}
	// 	if ($('#stockInGrid').val() == '') {
	// 		$('#errMsgInfo').html('Bạn chưa xử lý xong dòng thiết bị thêm mới!').show();
	// 		return false;
	// 	}		
	// 	//upload file
	// 	var arrFile = DropzonePlugin._dropzoneObject.getFilesWithStatus(Dropzone.ADDED);
	// 	if (EquipmentManagerDelivery._arrFileDelete == undefined || EquipmentManagerDelivery._arrFileDelete == null) {
	// 		EquipmentManagerDelivery._arrFileDelete = [];
	// 	}
	// 	if (arrFile != null && arrFile != undefined && arrFile.length > 0) {
	// 		if (EquipmentManagerDelivery._countFile == 5) {
	// 			$('#errMsgInfo').html("Tổng số tập tin đính kèm không được vượt quá 5").show();
	// 			return false;
	// 		}
	// 		var maxLength = 5 - EquipmentManagerDelivery._countFile;
	// 		if(maxLength<0){
	// 			maxLength = 0;
	// 		}
	// 		if (arrFile.length > maxLength) {
	// 			$('#errMsgInfo').html("Số tập tin thêm mới không được vượt quá " + maxLength).show();
	// 			return false;
	// 		}
	// 		msg = UploadUtil.getMsgByFileRequireCheck(UploadUtil._typeFileRequireCheck.equipRecord);
	// 		if (msg.trim().length > 0) {
	// 			$('#errMsgInfo').html(msg).show();
	// 			return false;
	// 		}			
	// 	}
	// 	params.equipAttachFileStr= EquipmentManagerDelivery._arrFileDelete.join(',');
	// 	Utils.getJSONDataByAjaxNotOverlay({id : -1}, '/equipment-manage-delivery/check-equip-period', function(dt){						
	// 		if(dt.error){
	// 			$('#errMsgInfo').html(dt.errMsgInfo).show();
	// 			return false;
	// 		}else{
	// 			var isStaff = false;
	// 			var shopToCode = $('#shopToCode').val().trim();
	// 			if(shopToCode != undefined && shopToCode != null && shopToCode != ''){
	// 				 $.getJSON('/equipment-manage-delivery/getListGSNPP?toShopCode='+shopToCode, function(result) {
	// 					 if(result.rows != undefined && result.rows != null && result.rows.length > 0) {
	// 					 	for(var i = 0, sz = result.rows.length; i < sz; i++) {
	// 					 		if ($('#staffMonitorCode').combobox('getValue').trim() == result.rows[i].staffCode) {
	// 								isStaff = true;
	// 							}
	// 					 	}
	// 					 }
	// 					 if (result.rows.length == 0) {
	// 					 	msg = 'Không tìm thấy "Đơn vị" BÊN MƯỢN hoặc Mã giám sát không quản lý "Đơn vị" BÊN MƯỢN';
	// 						err = '#shopToCode';
	// 						$('#errMsgInfo').html(msg).show();
	// 						$(err).focus();
	// 						return false;
	// 					 } else if (!isStaff) {
	// 						msg = 'Mã giám sát không quản lý "Đơn vị" BÊN MƯỢN';
	// 						err = '#staffMonitorCode';
	// 						$('#errMsgInfo').html(msg).show();
	// 						$(err).focus();
	// 						return false;
	// 					} else {
	// 						$.messager.confirm('Xác nhận', 'Bạn có muốn lưu dữ liệu?', function(r){
	// 							if (r){
	// 								if (arrFile != null && arrFile != undefined && arrFile.length > 0) {
	// 									params = JSONUtil.getSimpleObject2(params);
	// 									UploadUtil.updateAdditionalDataForUpload(params);
	// 									UploadUtil.startUpload('errMsgInfo');
	// 									return false;
	// 								}					
	// 								Utils.saveData(params, '/equipment-manage-delivery/create-record-delivery', null, 'errMsgInfo', function(data){	
	// 									if(data.error){
	// 										$('#errMsgInfo').html(data.errMsgInfo).show();
	// 										return false;
	// 									}else{
	// 										$('#successMsgInfo').html("Cập nhật dữ liệu thành công.").show();	
	// 										EquipmentManagerDelivery._lstEquipInsert = null;
	// 										EquipmentManagerDelivery._lstStockInsert = null;
	// 										EquipmentManagerDelivery._editIndex = null;				
	// 										setTimeout(function(){
	// 											$('#successMsgInfo').html("").hide(); 
	// 											window.location.href = '/equipment-manage-delivery/info';						              		
	// 										}, 3000);
	// 									}
	// 								}, null, null, null, null);		
	// 							}
	// 						});
	// 					}
	// 				});
	// 			}
	// 		}
	// 	}, null, null);			
	// },
	/**
	 * Tao moi bien ban
	 * @author tamvnm
	 * @since 25/06/2015
	 */
	createRecordDelivery: function(){
		$('#errMsgInfo').html('').hide();
		var msg ='';
		var err = '';
		var nContractTrim = $('#numberContract').val().trim();
		$('#numberContract').val(nContractTrim);
		//if(msg.length == 0 && $('#numberContract').val().indexOf(' ')!=-1){
		//	msg = 'Số hợp đồng không được có khoảng trắng';
		//	err = '#numberContract';
		//}
		// if(msg.length == 0){
		// 	msg = Utils.getMessageOfSpecialCharactersValidate('numberContract','Số hợp đồng',Utils._NAME_CUSTYPE);
		// 	err = '#numberContract';
		// }
		var createDate = $('#createDate').val();
		if(msg.length == 0 && $('#createDate').val().trim().length > 0 && !Utils.isDate($('#createDate').val()) && $('#createDate').val() != '__/__/____' ){
			msg = 'Ngày biên bản không tồn tại hoặc không đúng định dạng dd/mm/yyyy.';
			err = '#createDate';
		}
		var curDate = ReportUtils.getCurrentDateString(); //Lay ngay hien tai sysDate
		if (msg.length == 0 && !Utils.compareDate($('#createDate').val().trim(), curDate)) {
			msg = 'Ngày biên bản không được sau ngày hiện tại. Vui lòng nhập lại';
			$('#createDate').focus();
		}

		if(msg.length == 0){
			msg = Utils.getMessageOfRequireCheck('numberContract','Số hợp đồng');
			err = '#numberContract';
		}	
		if(msg.length == 0){
			msg = Utils.getMessageOfRequireCheck('contractDate','Ngày hợp đồng');
			err = '#contractDate';
		}				
		var contractDate = $('#contractDate').val().trim();
		if(msg.length == 0 && contractDate != '' && !Utils.isDate(contractDate)){
			msg = 'Ngày hợp đồng không tồn tại hoặc không đúng định dạng dd/mm/yyyy.';
			err = '#contractDate';
		}	

		if ($('#shopFromCode').val() == '' && msg.length == 0) {
			msg = 'Bạn chưa nhập giá trị cho trường "Đơn vị" BÊN CHO MƯỢN';
			err = '#shopFromCode';
		}
		if ($('#address').val() == '' && msg.length == 0) {
			msg = 'Bạn chưa nhập giá trị cho trường "Địa chỉ" BÊN CHO MƯỢN';
			err = '#address';
		}
		if ($('#taxCode').val() == '' && msg.length == 0) {
			msg = 'Bạn chưa nhập giá trị cho trường "Mã số thuế" BÊN CHO MƯỢN';
			err = '#taxCode';
		}
		if ($('#numberPhone').val() == '' && msg.length == 0) {
			msg = 'Bạn chưa nhập giá trị cho trường "Điện thoại" BÊN CHO MƯỢN';
			err = '#numberPhone';
		}
		if ($('#fromRepresentative').val() == '' && msg.length == 0) {
			msg = 'Bạn chưa nhập giá trị cho trường "Người đại diện" BÊN CHO MƯỢN';
			err = '#fromRepresentative';
		}
		if ($('#fromPosition').val() == '' && msg.length == 0) {
			msg = 'Bạn chưa nhập giá trị cho trường "Chức vụ" BÊN CHO MƯỢN';
			err = '#fromPosition';
		}
		if ($('#fromPage').val() == '' && msg.length == 0) {
			msg = 'Bạn chưa nhập giá trị cho trường "Giấy ủy quyền" BÊN CHO MƯỢN';
			err = '#fromPage';
		}
		if ($('#fromPagePlace').val() == '' && msg.length == 0) {
			msg = 'Bạn chưa nhập giá trị cho trường "Nơi cấp giấy quỷ quyền" BÊN CHO MƯỢN';
			err = '#fromPagePlace';
		}
		var fromPDate = $('#pageDate').val().trim();
		if(msg.length == 0 && fromPDate != '' && !Utils.isDate(fromPDate)){
			msg = 'Ngày cấp GPKD không tồn tại hoặc không đúng định dạng dd/mm/yyyy.';
			err = '#pageDate';
		}	
		if(msg.length == 0){
			msg = Utils.getMessageOfRequireCheck('pageDate','Ngày cấp giấy ủy quyền" BÊN CHO MƯỢN');
			err = '#pageDate';
		}

		if(msg.length == 0){
			msg = Utils.getMessageOfSpecialCharactersValidate('shopToCode','Mã đơn vị BÊN MƯỢN',Utils._CODE);
			err = '#shopToCode';
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfRequireCheck('shopToCode','Mã đơn vị BÊN MƯỢN');
			err = '#shopToCode';
		}	
		if(msg.length == 0){
			msg = Utils.getMessageOfRequireCheck('customerCode','Mã khách hàng');
			err = '#customerCode';
		}	
		if(msg.length == 0){
			msg = Utils.getMessageOfSpecialCharactersValidate('customerCode','Mã khách hàng',Utils._CODE);
			err = '#customerCode';
		}
		if ($('#toObjectAddress').val() == '' && msg.length == 0) {
			msg = 'Bạn chưa nhập giá trị cho trường "Địa chỉ đặt tủ" BÊN MƯỢN';
			err = '#toObjectAddress';
		}
		// if ($('#toBusinessLicense').val() == '' && msg.length == 0) {
		// 	msg = 'Bạn chưa nhập giá trị cho trường "Số GP ĐKKD" BÊN MƯỢN';
		// 	err = '#toBusinessLicense';
		// }
		// if ($('#toBusinessPlace').val() == '' && msg.length == 0) {
		// 	msg = 'Bạn chưa nhập giá trị cho trường "Nơi cấp GP ĐKKD BÊN MƯỢN';
		// 	err = '#toBusinessPlace';
		// }
		var toBusinessDate = $('#toBusinessDate').val().trim();
		if(msg.length == 0 && toBusinessDate != '' && !Utils.isDate(toBusinessDate)){
			msg = 'Ngày cấp GPKD không tồn tại hoặc không đúng định dạng dd/mm/yyyy.';
			err = '#toBusinessDate';
		}	
		// if(msg.length == 0){
		// 	msg = Utils.getMessageOfRequireCheck('toBusinessDate','Ngày cấp GPKD');
		// 	err = '#toBusinessDate';
		// }
		if ($('#toRepresentative').val() == '' && msg.length == 0) {
			msg = 'Bạn chưa nhập giá trị cho trường "Người đại diện" BÊN MƯỢN';
			err = '#toRepresentative';
		}
		if ($('#toPosition').val() == '' && msg.length == 0) {
			msg = 'Bạn chưa nhập giá trị cho trường "Chức vụ người đại diện" BÊN MƯỢN';
			err = '#toPosition';
		}
		if ($('#staffMonitorCode').combobox('getValue') == '' && msg.length == 0) {
			msg = 'Bạn chưa nhập giá trị cho trường "Mã giám sát" BÊN MƯỢN';
			err = '#staffMonitorCode';
		}
		//tamvnm:09/09/2015 update yeu cau ko check so nha, phuong/xa
		// if ($('#addressName').val().trim() == '' && msg.length == 0) {
		// 	msg = 'Bạn chưa nhập giá trị cho trường Số nhà';
		// 	err = '#addressName';
		// }
		if ($('#street').val().trim() == '' && msg.length == 0) {
			msg = 'Bạn chưa nhập giá trị cho trường Tên đường phố/Thôn ấp';
			err = '#street';
		}
		if ($('#provinceName').val().trim() == '' && msg.length == 0) {
			msg = 'Bạn chưa nhập giá trị cho trường Tỉnh/TP';
			err = '#provinceName';
		}
		if ($('#districtName').val().trim() == '' && msg.length == 0) {
			msg = 'Bạn chưa nhập giá trị cho trường Quận/Huyện';
			err = '#districtName';
		}
		// if ($('#wardName').val().trim() == '' && msg.length == 0) {
		// 	msg = 'Bạn chưa nhập giá trị cho trường Phường/Xã';
		// 	err = '#wardName';
		// }

		var freezer = Utils.returnMoneyValue($('#freezer').val().trim());
		var refrigerator = Utils.returnMoneyValue($("#refrigerator").val().trim());
		var intFreezer = parseInt(freezer);
		var intRefrigerator = parseInt(refrigerator);
		if (msg.length == 0) {
			if(freezer != '') {
				if (intFreezer != freezer) {
					msg = 'Tủ mát phải là số nguyên dương có tối đa 17 chữ số';
					err = '#freezer';
				} else if (intFreezer < 0  || intFreezer > 99999999999999999) {
					msg = 'Tủ mát phải là số nguyên dương có tối đa 17 chữ số';
					err = '#freezer';
				}
			} else {
				//tamvnm: 07/07/2015 bo rang buoc ds tu mat
				//msg = 'Tủ mát phải là số nguyên dương có tối đa 17 chữ số';
				//err = '#freezer';
			}
		}
		if (msg.length == 0) {
			if (refrigerator != '') {
				if (intRefrigerator != refrigerator) {
					msg = 'Tủ đông phải là số nguyên dương có tối đa 17 chữ số';
					err = '#refrigerator';
				} else if (intRefrigerator < 0  || intRefrigerator > 99999999999999999) {
					msg = 'Tủ đông phải là số nguyên dương có tối đa 17 chữ số';
					err = '#refrigerator';
				}
			} else {
				//tamvnm: 07/07/2015 bo rang buoc ds tu dong
				//msg = 'Tủ đông phải là số nguyên dương có tối đa 17 chữ số';
				//err = '#refrigerator';
			}
		}


		if(msg.length > 0){
			$('#errMsgInfo').html(msg).show();
			$(err).focus();
			return false;
		}
		
		var rowEquip = $('#gridEquipmentDelivery').datagrid('getRows');
		if(rowEquip.length == 0){
			$('#errMsgInfo').html('Không có thiết bị để cập nhật').show();
			return false;
		}

		var lstContentDelivery = new Array();
		var lstGroup = new Array();
		var lstProvider = new Array();
		var lstStock = new Array();
		var lstEquipCode = new Array();
		var lstSeriNumber = new Array();
		var lstDepreciation = new Array();
		var lstEquipPrice = new Array();
		var lstHealth = new Array();
		var lstManufacturingYear = new Array();
		
		if (EquipmentManagerDelivery._editIndex == undefined) {
			for (var i = 0, isize = rowEquip.length; i < isize; i++) {
				var row = rowEquip[i];
				if (row.contentDelivery == EquipmentManagerDelivery._DELIVERY_CONTENT_NEW) {
					lstGroup.push(row.groupEquipmentCode);
					lstProvider.push(row.equipmentProviderCode);
					lstStock.push(row.stockCode);
					lstEquipPrice.push(Utils.returnMoneyValue(row.price));
					lstHealth.push(row.healthStatus);
					lstManufacturingYear.push(row.yearManufacture);
					if (row.equipmentCode != '' && row.equipmentCode != null) {
						lstEquipCode.push(row.equipmentCode);
					} else {
						lstEquipCode.push('');
					}
				} else {
					lstGroup.push('');
					lstProvider.push('');
					lstStock.push('');
					lstEquipPrice.push(0);
					lstHealth.push('');
					lstManufacturingYear.push(0);
					lstEquipCode.push(row.equipmentCode);
				}
				lstContentDelivery.push(row.contentDelivery);
				
				var seri = null;
				if (row.seriNumber != null && row.seriNumber != '') {
					seri = row.seriNumber;
				} else {
					if ($('#seriNumberInGrid' + i).val() != undefined) {
						seri = $('#seriNumberInGrid' + i).val();
					} 
				}
				lstSeriNumber.push(seri);
				lstDepreciation.push(row.depreciation); 
			}
		} else {
			for (var i = 0, isize = rowEquip.length-1; i < isize; i++) {
				var row = rowEquip[i];
				if (row.contentDelivery == EquipmentManagerDelivery._DELIVERY_CONTENT_NEW) {
					lstGroup.push(row.groupEquipmentCode);
					lstProvider.push(row.equipmentProviderCode);
					lstStock.push(row.stockCode);
					lstEquipPrice.push(Utils.returnMoneyValue(row.price));
					lstHealth.push(row.healthStatus);
					lstManufacturingYear.push(row.yearManufacture);
					if (row.equipmentCode != '' && row.equipmentCode != null) {
						lstEquipCode.push(row.equipmentCode);
					} else {
						lstEquipCode.push('');
					}
				} else {
					lstGroup.push('');
					lstProvider.push('');
					lstStock.push('');
					lstEquipPrice.push(0);
					lstHealth.push('');
					lstManufacturingYear.push(0);
					lstEquipCode.push(row.equipmentCode);
				}
				lstContentDelivery.push(row.contentDelivery);
				
				var seri = null;
				if (row.seriNumber != null && row.seriNumber != '') {
					seri = row.seriNumber;
				} else {
					if ($('#seriNumberInGrid' + i).val() != undefined) {
						seri = $('#seriNumberInGrid' + i).val();
					} 
				}
				lstSeriNumber.push(seri);
				lstDepreciation.push(row.depreciation);
			}
		}

		//xu ly dong cuoi
		var index = EquipmentManagerDelivery._editIndex;
		if (index == rowEquip.length-1) {
			if ($('#contentDelivery').val() != undefined && $('#contentDelivery').val() != null) {
				var curContent = $('#contentDelivery').val();
				var curGroup = null;
				var curProvider = null;
				var curStock = null;
				var curEquipCode = $('#equipmentCodeInGrid' + index).val().trim();
				
				var curSeri = null;
				if (rowEquip[index].seriNumber != null && rowEquip[index].seriNumber != '') {
					curSeri = rowEquip[index].seriNumber;
				} else if ($('#seriNumberInGrid' + index).val() != undefined) {
					curSeri = $('#seriNumberInGrid' + index).val();
				}

				// if (curContent == EquipmentManagerDelivery._DELIVERY_CONTENT_NEW) {
				// 	curSeri = $('#seriNumberInGrid' + index).val().trim();
				// } else {
				// 	if ($('#seriNumberInGrid' + index).val() == undefined) {
				// 		curSeri = rowEquip[index].seriNumber;
				// 	} else {
				// 		curSeri = $('#seriNumberInGrid' + index).val();
				// 	}
					
				// }
				
				var curDeprec = $('#depreciationInGrid' + index).val().trim();
				var curPrice = null;
				var curHealth = null;
				var curManufacturingYear = null;
				if (curContent != undefined && curContent == EquipmentManagerDelivery._DELIVERY_CONTENT_NEW) {
					curGroup = $('#groupEquipCBX' + index).combobox('getValue').trim();
					curProvider = $('#providerEquipCBX' + index).combobox('getValue').trim();
					curStock = $('#stockEquipCBX' + index).combobox('getValue').trim();
					curPrice = $('#price' + index).val().trim();
					curHealth = $('#healthStatus' + index).val().trim();
					curManufacturingYear = $('#yearManufacture' + index).val().trim();

					if (curGroup == '') {
						$('#errMsgInfo').html('Bạn chưa chọn nhóm thiết bị').show();
						$('#groupEquipCBX' + index).focus();
						return false;
					} else {

					}

					if (curProvider == '') {
						$('#errMsgInfo').html('Bạn chưa chọn nhà cung cấp').show();
						$('#providerEquipCBX' + index).focus();
						return false;
					} else {

					}

					if (curStock == '') {
						$('#errMsgInfo').html('Bạn chưa chọn kho').show();
						$('#stockEquipCBX' + index).focus();
						return false;
					}

					if (curEquipCode != '') {
						$('#errMsgInfo').html('Bạn không được nhập mã thiết bị').show();
						return false;
					} else {

					}

					if (curDeprec == '') {
						$('#errMsgInfo').html('Bạn chưa nhập số tháng khấu hao').show();
						return false;
					} else {
						var mes = Utils.getMessageOfInvaildNumberNew(curDeprec);
						if (mes.length > 0) {
							$('#errMsgInfo').html('Số tháng khấu hao phải là số nguyên dương').show();
							return false;
						}
					}

					if (curPrice == '') {
						$('#errMsgInfo').html('Bạn chưa nhập giá trị TBBH').show();
						return false;
					} else {
						var mes = Utils.getMessageOfInvaildNumber('price' + index, 'Giát trị TBBH');
						if (mes.length > 0) {
							$('#errMsgInfo').html('Giá trị TBBH phải là số nguyên dương').show();
							return false;
						}
					}

					if (curManufacturingYear == '') {
						$('#errMsgInfo').html('Bạn chưa nhập giá trị năm sản xuất').show();
						return false;
					} else {
						var mes = Utils.getMessageOfInvaildNumber('yearManufacture' + index, 'Năm sản xuất');
						if (mes.length > 0) {
							$('#errMsgInfo').html('Năm sản xuất phải là số nguyên dương').show();
							return false;
						}
					}

					if (curHealth == '') {
						$('#errMsgInfo').html('Bạn chưa chọn trình trạng thiết bị').show();
						return false;
					} else {

					}
					//khong co loi nao:
					lstGroup.push(curGroup);
					lstProvider.push(curProvider);
					lstStock.push(curStock);
					lstEquipPrice.push(Utils.returnMoneyValue(curPrice));
					lstHealth.push(curHealth);
					lstManufacturingYear.push(curManufacturingYear);
					lstEquipCode.push('');
					lstContentDelivery.push(curContent);
					lstSeriNumber.push(curSeri);
					lstDepreciation.push(curDeprec);
				} else {
					if (curEquipCode == '') {
						$('#errMsgInfo').html('Bạn chưa nhập mã thiết bị').show();
						return false;
					} else {
						curEquipCode = curEquipCode.trim();
						for (var i = 0, isize = lstEquipCode.length; i< isize; i++) {
							if (curEquipCode == lstEquipCode[i]) {		
								$('#errMsgInfo').html('Mã thiết bị ' + curEquipCode + 'bị trùng với dòng ' + i + 1).show();
								return false;
							}
						}
					}
					if (curDeprec == '') {
						$('#errMsgInfo').html('Bạn chưa nhập số tháng khấu hao').show();
						return false;
					} else {
						var mes = Utils.getMessageOfInvaildNumberNew(curDeprec);
						if (mes.length > 0) {
							$('#errMsgInfo').html('Số tháng khấu hao phải là số nguyên dương').show();
							return false;
						}
					}
					//khong co loi nao:
					lstGroup.push('');
					lstProvider.push('');
					lstStock.push('');
					lstEquipPrice.push(0);
					lstHealth.push('');
					lstManufacturingYear.push(0);
					lstEquipCode.push(curEquipCode);
					lstContentDelivery.push(curContent);
					// if (curSeri == null) {
					// 	lstSeriNumber.push('');	
					// } else {
					// 	lstSeriNumber.push(curSeri);
					// }
					lstSeriNumber.push(curSeri);
					
					lstDepreciation.push(curDeprec);
				}
			}
		}

		var params=new Object(); 
		params.shopFromCode = $('#shopFromCode').val().trim();
		params.shopToCode = $('#shopToCode').val().trim();
		params.customerCode = $('#customerCode').val().trim();
		params.numberContract = $('#numberContract').val().trim();
		params.contractDate = $('#contractDate').val().trim();
		params.statusRecord = $('#status').val().trim();
		params.statusDelivery = $('#statusDelivery').val().trim();
		
		params.fromObjectAddress = $("#address").val().trim();
		params.fromObjectTax  = $("#taxCode").val().trim();
		params.fromObjectPhone  = $("#numberPhone").val().trim();
		params.fromRepresentative = $("#fromRepresentative").val().trim();
		params.fromObjectPosition = $("#fromPosition").val().trim();
		params.fromPage = $("#fromPage").val().trim();
		params.fromPagePlace = $("#fromPagePlace").val().trim();
		params.fromPageDate = $("#pageDate").val().trim();
		params.fromFax = $('#fromFax').val();
		params.toPermanentAddress = $("#toPermanentAddress").val().trim();
		params.toRepresentative = $("#toRepresentative").val().trim();
		params.toObjectPosition = $("#toPosition").val().trim();
		params.toIdNO = $("#toIdNO").val().trim();
		params.toIdNODate = $("#idNODate").val().trim();
		params.toIdNOPlace = $("#toIdNOPlace").val().trim();
		params.freezer = Utils.returnMoneyValue($('#freezer').val().trim());
		params.refrigerator = Utils.returnMoneyValue($("#refrigerator").val().trim());
		params.fromShopCode = $("#shopFromCode").val().trim();
		params.toShopCode = $("#shopToCode").val().trim();
		params.staffCode = $('#staffMonitorCode').combobox('getValue').trim();
		params.toBusinessLicense = $('#toBusinessLicense').val().trim();
		params.toBusinessDate = $('#toBusinessDate').val().trim();
		params.toBusinessPlace = $('#toBusinessPlace').val().trim();
		params.createDate = createDate;
		
		params.addressName = $('#addressName').val().trim();
		params.street = $('#street').val().trim();
		params.provinceName = $('#provinceName').val().trim();
		params.districtName = $('#districtName').val().trim();
		params.wardName = $('#wardName').val().trim();
		params.note = $('#note').val();


		var curLength = lstContentDelivery.length;
		if (lstGroup.length != curLength || lstProvider.length != curLength || lstEquipCode.length != curLength
			|| lstSeriNumber.length != curLength || lstDepreciation.length != curLength || lstEquipPrice.length != curLength
			|| lstHealth.length != curLength || lstStock.length != curLength) {
			$('#errMsgInfo').html("Thiết thông tin thiết bi").show();
			return false;
		}

		if(lstContentDelivery.length > 0){
			params.lstContentDelivery = lstContentDelivery;
			params.lstGroup = lstGroup;
			params.lstProvider = lstProvider;
			params.lstStock = lstStock;
			params.lstEquipCode = lstEquipCode;
			params.lstSeriNumber = lstSeriNumber;
			params.lstDepreciation = lstDepreciation;
			params.lstEquipPrice = lstEquipPrice;
			params.lstHealth = lstHealth;
			params.lstManufacturingYear = lstManufacturingYear;
		}

		if (params.statusRecord == 2 && lstSeriNumber.length > 0) {
			for (var i = 0; i < lstSeriNumber.length; i++) {
				if (lstSeriNumber[i] == null || lstSeriNumber[i] == '') {
					$('#errMsgInfo').html("Vui lòng nhập đầy đủ thông tin seri khi chọn trạng thái Duyệt").show();
					return false;
				}
			}
		}
		
		//upload file
		var arrFile = DropzonePlugin._dropzoneObject.getFilesWithStatus(Dropzone.ADDED);
		if (EquipmentManagerDelivery._arrFileDelete == undefined || EquipmentManagerDelivery._arrFileDelete == null) {
			EquipmentManagerDelivery._arrFileDelete = [];
		}
		if (arrFile != null && arrFile != undefined && arrFile.length > 0) {
			if (EquipmentManagerDelivery._countFile == 5) {
				$('#errMsgInfo').html("Tổng số tập tin đính kèm không được vượt quá 5").show();
				return false;
			}
			var maxLength = 5 - EquipmentManagerDelivery._countFile;
			if(maxLength<0){
				maxLength = 0;
			}
			if (arrFile.length > maxLength) {
				$('#errMsgInfo').html("Số tập tin thêm mới không được vượt quá " + maxLength).show();
				return false;
			}
			msg = UploadUtil.getMsgByFileRequireCheck(UploadUtil._typeFileRequireCheck.equipRecord);
			if (msg.trim().length > 0) {
				$('#errMsgInfo').html(msg).show();
				return false;
			}			
		}
		params.equipAttachFileStr = EquipmentManagerDelivery._arrFileDelete.join(',');

		var isStaff = false;
		var shopToCode = $('#shopToCode').val().trim();
		if (shopToCode != undefined && shopToCode != null && shopToCode != '') {
			$.getJSON('/equipment-manage-delivery/getListGSNPP?toShopCode=' + shopToCode, function(result) {
				if (result.rows != undefined && result.rows != null && result.rows.length > 0) {
					for (var i = 0, sz = result.rows.length; i < sz; i++) {
						if ($('#staffMonitorCode').combobox('getValue').trim() == result.rows[i].staffCode) {
							isStaff = true;
						}
					}
				}
				if (result.rows.length == 0) {
					msg = 'Không tìm thấy "Đơn vị" BÊN MƯỢN hoặc Mã giám sát không quản lý "Đơn vị" BÊN MƯỢN';
					err = '#shopToCode';
					$('#errMsgInfo').html(msg).show();
					$(err).focus();
					return false;
				} else if (!isStaff) {
					msg = 'Mã giám sát không quản lý "Đơn vị" BÊN MƯỢN';
					err = '#staffMonitorCode';
					$('#errMsgInfo').html(msg).show();
					$(err).focus();
					return false;
				} else {
					$.messager.confirm('Xác nhận', 'Bạn có muốn lưu dữ liệu?', function(r) {
						if (r) {
							if (arrFile != null && arrFile != undefined && arrFile.length > 0) {
								params = JSONUtil.getSimpleObject2(params);
								UploadUtil.updateAdditionalDataForUpload(params);
								UploadUtil.startUpload('errMsgInfo');
								return false;
							}
							Utils.saveData(params, '/equipment-manage-delivery/create-record-delivery', null, 'errMsgInfo', function(data) {
								if (data.error) {
									$('#errMsgInfo').html(data.errMsgInfo).show();
									return false;
								} else {
									$('#successMsgInfo').html("Cập nhật dữ liệu thành công.").show();
									EquipmentManagerDelivery._lstEquipInsert = null;
									EquipmentManagerDelivery._lstStockInsert = null;
									EquipmentManagerDelivery._editIndex = null;
									setTimeout(function() {
										$('#successMsgInfo').html("").hide();
										window.location.href = '/equipment-manage-delivery/info';
									}, 3000);
								}
							}, null, null, null, null);
						}
					});
				}
			});
		}
	},
	/**
	 * Cap nhat bien ban voi tinh trang da duyet
	 * 
	 * @author hunglm16
	 * @since August 18,2015
	 * */
	updateRecordDeliveryByApproved: function() {
		var params = new Object();
		var staffMonitorCode = $('#staffMonitorCode').combobox('getValue').trim();
		if (staffMonitorCode != undefined && staffMonitorCode != null && staffMonitorCode.length > 0) {
			params.staffCode = staffMonitorCode;
			params.idRecordDelivery = $("#idRecordHidden").val().trim();
			$.messager.confirm('Xác nhận', "Bạn có muốn cập nhật biên bản này ?", function(r) {
				 if (r) {
					 Utils.saveData(params, '/equipment-manage-delivery/update-record-delivery', null, 'errMsgInfo', function(data) {	
						 if (data.error) {
							 $('#errMsgInfo').html(data.errMsgInfo).show();
							 return false;
						 } else {
							 $('#successMsgInfo').html("Cập nhật dữ liệu thành công.").show();
							 setTimeout(function() {
								 $('#successMsgInfo').html("").hide(); 
								 //window.location.href = '/equipment-manage-delivery/change-record?id='+ $("#idRecordHidden").val().trim();						
							 }, 1500);
						 }
					 }, null, null, null, null);
				 }
			 });		
		} else {
			$('#errMsgInfo').html('Không xác định được giám sát').show();
		}
		return false;
	},
	
	/**
	 * Chinh sua bien ban
	 * @author tamvnm
	 * @since 26/06/2015
	 */
	updateRecordDelivery: function(){
		$('#errMsgInfo').html('').hide();
		var msg ='';
		var err = '';
		//Xu ly cho truong hop da duyet
		if ($("#statusRecordHidden").val() == EquipmentManagerDelivery._approved) {
			return EquipmentManagerDelivery.updateRecordDeliveryByApproved();
		}
		
		var nContractTrim = $('#numberContract').val().trim();
		$('#numberContract').val(nContractTrim);
		//if(msg.length == 0 && $('#numberContract').val().indexOf(' ')!=-1){
		//	msg = 'Số hợp đồng không được có khoảng trắng';
		//	err = '#numberContract';
		//}
		// if(msg.length == 0){
		// 	msg = Utils.getMessageOfSpecialCharactersValidate('numberContract','Số hợp đồng',Utils._NAME_CUSTYPE);
		// 	err = '#numberContract';
		// }
		var createDate = $('#createDate').val();
		if(msg.length == 0 && $('#createDate').val().trim().length > 0 && !Utils.isDate($('#createDate').val()) && $('#createDate').val() != '__/__/____' ){
			msg = 'Ngày biên bản không tồn tại hoặc không đúng định dạng dd/mm/yyyy.';
			err = '#createDate';
		}
		var curDate = ReportUtils.getCurrentDateString(); //Lay ngay hien tai sysDate
		if (msg.length == 0 && !Utils.compareDate($('#createDate').val().trim(), curDate)) {
			msg = 'Ngày biên bản không được sau ngày hiện tại. Vui lòng nhập lại';
			$('#createDate').focus();
		}

		if(msg.length == 0){
			msg = Utils.getMessageOfRequireCheck('numberContract','Số hợp đồng');
			err = '#numberContract';
		}	
		if(msg.length == 0){
			msg = Utils.getMessageOfRequireCheck('contractDate','Ngày hợp đồng');
			err = '#contractDate';
		}				
		var contractDate = $('#contractDate').val().trim();
		if(msg.length == 0 && contractDate != '' && !Utils.isDate(contractDate)){
			msg = 'Ngày hợp đồng không tồn tại hoặc không đúng định dạng dd/mm/yyyy.';
			err = '#contractDate';
		}	

		if ($('#shopFromCode').val() == '' && msg.length == 0) {
			msg = 'Bạn chưa nhập giá trị cho trường "Đơn vị" BÊN CHO MƯỢN';
			err = '#shopFromCode';
		}
		if ($('#address').val() == '' && msg.length == 0) {
			msg = 'Bạn chưa nhập giá trị cho trường "Địa chỉ" BÊN CHO MƯỢN';
			err = '#address';
		}
		if ($('#taxCode').val() == '' && msg.length == 0) {
			msg = 'Bạn chưa nhập giá trị cho trường "Mã số thuế" BÊN CHO MƯỢN';
			err = '#taxCode';
		}
		if ($('#numberPhone').val() == '' && msg.length == 0) {
			msg = 'Bạn chưa nhập giá trị cho trường "Điện thoại" BÊN CHO MƯỢN';
			err = '#numberPhone';
		}
		if ($('#fromRepresentative').val() == '' && msg.length == 0) {
			msg = 'Bạn chưa nhập giá trị cho trường "Người đại diện" BÊN CHO MƯỢN';
			err = '#fromRepresentative';
		}
		if ($('#fromPosition').val() == '' && msg.length == 0) {
			msg = 'Bạn chưa nhập giá trị cho trường "Chức vụ" BÊN CHO MƯỢN';
			err = '#fromPosition';
		}
		if ($('#fromPage').val() == '' && msg.length == 0) {
			msg = 'Bạn chưa nhập giá trị cho trường "Giấy ủy quyền" BÊN CHO MƯỢN';
			err = '#fromPage';
		}
		if ($('#fromPagePlace').val() == '' && msg.length == 0) {
			msg = 'Bạn chưa nhập giá trị cho trường "Nơi cấp giấy quỷ quyền" BÊN CHO MƯỢN';
			err = '#fromPagePlace';
		}
		var fromPDate = $('#pageDate').val().trim();
		if(msg.length == 0 && fromPDate != '' && !Utils.isDate(fromPDate)){
			msg = 'Ngày cấp GPKD không tồn tại hoặc không đúng định dạng dd/mm/yyyy.';
			err = '#pageDate';
		}	
		if(msg.length == 0){
			msg = Utils.getMessageOfRequireCheck('pageDate','Ngày cấp giấy ủy quyền" BÊN CHO MƯỢN');
			err = '#pageDate';
		}

		if(msg.length == 0){
			msg = Utils.getMessageOfSpecialCharactersValidate('shopToCode','Mã đơn vị BÊN MƯỢN',Utils._CODE);
			err = '#shopToCode';
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfRequireCheck('shopToCode','Mã đơn vị BÊN MƯỢN');
			err = '#shopToCode';
		}	
		if(msg.length == 0){
			msg = Utils.getMessageOfRequireCheck('customerCode','Mã khách hàng');
			err = '#customerCode';
		}	
		if(msg.length == 0){
			msg = Utils.getMessageOfSpecialCharactersValidate('customerCode','Mã khách hàng',Utils._CODE);
			err = '#customerCode';
		}
		if ($('#toObjectAddress').val() == '' && msg.length == 0) {
			msg = 'Bạn chưa nhập giá trị cho trường "Địa chỉ đặt tủ" BÊN MƯỢN';
			err = '#toObjectAddress';
		}
		// if ($('#toBusinessLicense').val() == '' && msg.length == 0) {
		// 	msg = 'Bạn chưa nhập giá trị cho trường "Số GP ĐKKD" BÊN MƯỢN';
		// 	err = '#toBusinessLicense';
		// }
		// if ($('#toBusinessPlace').val() == '' && msg.length == 0) {
		// 	msg = 'Bạn chưa nhập giá trị cho trường "Nơi cấp GP ĐKKD BÊN MƯỢN';
		// 	err = '#toBusinessPlace';
		// }
		var toBusinessDate = $('#toBusinessDate').val().trim();
		if(msg.length == 0 && toBusinessDate != '' && !Utils.isDate(toBusinessDate)){
			msg = 'Ngày cấp GPKD không tồn tại hoặc không đúng định dạng dd/mm/yyyy.';
			err = '#toBusinessDate';
		}	
		// if(msg.length == 0){
		// 	msg = Utils.getMessageOfRequireCheck('toBusinessDate','Ngày cấp GPKD');
		// 	err = '#toBusinessDate';
		// }
		if ($('#toRepresentative').val() == '' && msg.length == 0) {
			msg = 'Bạn chưa nhập giá trị cho trường "Người đại diện" BÊN MƯỢN';
			err = '#toRepresentative';
		}
		if ($('#toPosition').val() == '' && msg.length == 0) {
			msg = 'Bạn chưa nhập giá trị cho trường "Chức vụ người đại diện" BÊN MƯỢN';
			err = '#toPosition';
		}
		if ($('#staffMonitorCode').combobox('getValue') == '' && msg.length == 0) {
			msg = 'Bạn chưa nhập giá trị cho trường "Mã giám sát" BÊN MƯỢN';
			err = '#staffMonitorCode';
		}
		//tamvnm:09/09/2015 update yeu cau ko check so nha, phuong/xa
		// if ($('#addressName').val().trim() == '' && msg.length == 0) {
		// 	msg = 'Bạn chưa nhập giá trị cho trường Số nhà';
		// 	err = '#addressName';
		// }
		if ($('#street').val().trim() == '' && msg.length == 0) {
			msg = 'Bạn chưa nhập giá trị cho trường Tên đường phố/Thôn ấp';
			err = '#street';
		}
		if ($('#provinceName').val().trim() == '' && msg.length == 0) {
			msg = 'Bạn chưa nhập giá trị cho trường Tỉnh/TP';
			err = '#provinceName';
		}
		if ($('#districtName').val().trim() == '' && msg.length == 0) {
			msg = 'Bạn chưa nhập giá trị cho trường Quận/Huyện';
			err = '#districtName';
		}
		// if ($('#wardName').val().trim() == '' && msg.length == 0) {
		// 	msg = 'Bạn chưa nhập giá trị cho trường Phường/Xã';
		// 	err = '#wardName';
		// }

		var freezer = Utils.returnMoneyValue($('#freezer').val().trim());
		var refrigerator = Utils.returnMoneyValue($("#refrigerator").val().trim());
		var intFreezer = parseInt(freezer);
		var intRefrigerator = parseInt(refrigerator);
		if (msg.length == 0) {
			if(freezer != '') {
				if (intFreezer != freezer) {
					msg = 'Tủ mát phải là số nguyên dương có tối đa 17 chữ số';
					err = '#freezer';
				} else if (intFreezer < 0  || intFreezer > 99999999999999999) {
					msg = 'Tủ mát phải là số nguyên dương có tối đa 17 chữ số';
					err = '#freezer';
				}
			} else {
				//tamvnm: 07/07/2015 bo rang buoc ds tu mat
				//msg = 'Tủ mát phải là số nguyên dương có tối đa 17 chữ số';
				//err = '#freezer';
			}
		}
		if (msg.length == 0) {
			if (refrigerator != '') {
				if (intRefrigerator != refrigerator) {
					msg = 'Tủ đông phải là số nguyên dương có tối đa 17 chữ số';
					err = '#refrigerator';
				} else if (intRefrigerator < 0  || intRefrigerator > 99999999999999999) {
					msg = 'Tủ đông phải là số nguyên dương có tối đa 17 chữ số';
					err = '#refrigerator';
				}
			} else {
				//tamvnm: 07/07/2015 bo rang buoc ds tu dong
				//msg = 'Tủ đông phải là số nguyên dương có tối đa 17 chữ số';
				//err = '#refrigerator';
			}
		}

		if(msg.length > 0){
			$('#errMsgInfo').html(msg).show();
			$(err).focus();
			return false;
		}


		
		var rowEquip = $('#gridEquipmentDelivery').datagrid('getRows');
		if( rowEquip.length == 0 ){
			$('#errMsgInfo').html('Không có thiết bị để cập nhật').show();
			return false;
		}
		
		var lstContentDelivery = new Array();
		var lstGroup = new Array();
		var lstProvider = new Array();
		var lstEquipCode = new Array();
		var lstStock = new Array();
		var lstSeriNumber = new Array();
		var lstDepreciation = new Array();
		var lstEquipPrice = new Array();
		var lstHealth = new Array();
		var lstManufacturingYear = new Array();
		
		if (EquipmentManagerDelivery._editIndex == undefined) {
			for (var i = 0, isize = rowEquip.length; i < isize; i++) {
				var row = rowEquip[i];
				if (row.contentDelivery == EquipmentManagerDelivery._DELIVERY_CONTENT_NEW) {
					lstGroup.push(row.groupEquipmentCode);
					lstProvider.push(row.equipmentProviderCode);
					lstStock.push(row.stockCode);
					lstEquipPrice.push(Utils.returnMoneyValue(row.price));
					lstHealth.push(row.healthStatus);
					lstManufacturingYear.push(row.yearManufacture);
					if (row.equipmentCode != '' && row.equipmentCode != null) {
						lstEquipCode.push(row.equipmentCode);
					} else {
						lstEquipCode.push('');
					}
				} else {
					lstGroup.push('');
					lstProvider.push('');
					lstStock.push('');
					lstEquipPrice.push(0);
					lstHealth.push('');
					lstManufacturingYear.push(0);
					lstEquipCode.push(row.equipmentCode);
				}
				lstContentDelivery.push(row.contentDelivery);
				
				var seri = null;
				if (row.seriNumber != null && row.seriNumber != '') {
					seri = row.seriNumber;
				} else {
					if ($('#seriNumberInGrid' + i).val() != undefined) {
						seri = $('#seriNumberInGrid' + i).val();
					} 
				}
				lstSeriNumber.push(seri);
				lstDepreciation.push(row.depreciation); 
			}
		} else {
			for (var i = 0, isize = rowEquip.length-1; i < isize; i++) {
				var row = rowEquip[i];
				if (row.contentDelivery == EquipmentManagerDelivery._DELIVERY_CONTENT_NEW) {
					lstGroup.push(row.groupEquipmentCode);
					lstProvider.push(row.equipmentProviderCode);
					lstStock.push(row.stockCode);
					lstEquipPrice.push(Utils.returnMoneyValue(row.price));
					lstHealth.push(row.healthStatus);
					lstManufacturingYear.push(row.yearManufacture);
					if (row.equipmentCode != '' && row.equipmentCode != null) {
						lstEquipCode.push(row.equipmentCode);
					} else {
						lstEquipCode.push('');
					}
				} else {
					lstGroup.push('');
					lstProvider.push('');
					lstStock.push('');
					lstEquipPrice.push(0);
					lstHealth.push('');
					lstManufacturingYear.push(0);
					lstEquipCode.push(row.equipmentCode);
				}
				lstContentDelivery.push(row.contentDelivery);
				
				var seri = null;
				if (row.seriNumber != null && row.seriNumber != '') {
					seri = row.seriNumber;
				} else {
					if ($('#seriNumberInGrid' + i).val() != undefined) {
						seri = $('#seriNumberInGrid' + i).val();
					} 
				}
				lstSeriNumber.push(seri);
				lstDepreciation.push(row.depreciation);
			}
		}

		//xu ly dong cuoi
		var index = EquipmentManagerDelivery._editIndex;
		if (index == rowEquip.length-1) {
			if ($('#contentDelivery').val() != undefined && $('#contentDelivery').val() != null) {
				var curContent = $('#contentDelivery').val();
				var curGroup = null;
				var curProvider = null;
				var curStock = null;
				var curEquipCode = $('#equipmentCodeInGrid' + index).val().trim();
				
				var curSeri = null;
				if (rowEquip[index].seriNumber != null && rowEquip[index].seriNumber != '') {
					curSeri = rowEquip[index].seriNumber;
				} else if ($('#seriNumberInGrid' + index).val() != undefined) {
					curSeri = $('#seriNumberInGrid' + index).val();
				}
				var curDeprec = $('#depreciationInGrid' + index).val().trim();
				var curPrice = null;
				var curHealth = null;
				var curManufacturingYear = null;
				if (curContent != undefined && curContent == EquipmentManagerDelivery._DELIVERY_CONTENT_NEW) {
					curGroup = $('#groupEquipCBX' + index).combobox('getValue').trim();
					curProvider = $('#providerEquipCBX' + index).combobox('getValue').trim();
					curStock = $('#stockEquipCBX' + index).combobox('getValue').trim();
					curPrice = $('#price' + index).val().trim();
					curHealth = $('#healthStatus' + index).val().trim();
					curManufacturingYear = $('#yearManufacture' + index).val().trim();

					if (curGroup == '') {
						$('#errMsgInfo').html('Bạn chưa chọn nhóm thiết bị').show();
						$('#groupEquipCBX' + index).focus();
						return false;
					} else {

					}

					if (curProvider == '') {
						$('#errMsgInfo').html('Bạn chưa chọn nhà cung cấp').show();
						$('#providerEquipCBX' + index).focus();
						return false;
					} else {

					}
					if (curStock == '') {
						$('#errMsgInfo').html('Bạn chưa chọn kho').show();
						$('#stockEquipCBX' + index).focus();
						return false;
					}

					if (curEquipCode != '') {
						$('#errMsgInfo').html('Bạn không được nhập mã thiết bị').show();
						return false;
					} else {

					}

					if (curDeprec == '') {
						$('#errMsgInfo').html('Bạn chưa nhập số tháng khấu hao').show();
						return false;
					} else {
						var mes = Utils.getMessageOfInvaildNumberNew(curDeprec);
						if (mes.length > 0) {
							$('#errMsgInfo').html('Số tháng khấu hao phải là số nguyên dương').show();
							return false;
						}
					}

					if (curPrice == '') {
						$('#errMsgInfo').html('Bạn chưa nhập giá trị TBBH').show();
						return false;
					} else {
						var mes = Utils.getMessageOfInvaildNumber('price' + index, 'Giát trị TBBH');
						if (mes.length > 0) {
							$('#errMsgInfo').html('Giá trị TBBH phải là số nguyên dương').show();
							return false;
						}
					}

					if (curManufacturingYear == '') {
						$('#errMsgInfo').html('Bạn chưa nhập giá trị năm sản xuất').show();
						return false;
					} else {
						var mes = Utils.getMessageOfInvaildNumber('yearManufacture' + index, 'Năm sản xuất');
						if (mes.length > 0) {
							$('#errMsgInfo').html('Năm sản xuất phải là số nguyên dương').show();
							return false;
						}
					}

					if (curHealth == '') {
						$('#errMsgInfo').html('Bạn chưa chọn trình trạng thiết bị').show();
						return false;
					} else {

					}
					//khong co loi nao:
					lstGroup.push(curGroup);
					lstProvider.push(curProvider);
					lstStock.push(curStock);
					lstEquipPrice.push(Utils.returnMoneyValue(curPrice));
					lstHealth.push(curHealth);
					lstManufacturingYear.push(curManufacturingYear);
					lstEquipCode.push('');
					lstContentDelivery.push(curContent);
					lstSeriNumber.push(curSeri);
					lstDepreciation.push(curDeprec);
				} else {
					if (curEquipCode == '') {
						$('#errMsgInfo').html('Bạn chưa nhập mã thiết bị').show();
						return false;
					} else {
						curEquipCode = curEquipCode.trim();
						for (var i = 0, isize = lstEquipCode.length; i< isize; i++) {
							if (curEquipCode == lstEquipCode[i]) {		
								$('#errMsgInfo').html('Mã thiết bị ' + curEquipCode + 'bị trùng với dòng ' + i + 1).show();
								return false;
							}
						}
					}
					if (curDeprec == '') {
						$('#errMsgInfo').html('Bạn chưa nhập số tháng khấu hao').show();
						return false;
					} else {
						var mes = Utils.getMessageOfInvaildNumberNew(curDeprec);
						if (mes.length > 0) {
							$('#errMsgInfo').html('Số tháng khấu hao phải là số nguyên dương').show();
							return false;
						}
					}
					//khong co loi nao:
					lstGroup.push('');
					lstProvider.push('');
					lstStock.push('');
					lstEquipPrice.push(0);
					lstHealth.push('');
					lstManufacturingYear.push(0);
					lstEquipCode.push(curEquipCode);
					lstContentDelivery.push(curContent);
					// if (curSeri == null) {
					// 	lstSeriNumber.push('');	
					// } else {
					// 	lstSeriNumber.push(curSeri);
					// }
					lstSeriNumber.push(curSeri);
					
					lstDepreciation.push(curDeprec);
				}
			}
		}

		
		var params=new Object(); 		
		params.numberContract = $('#numberContract').val().trim();
		params.contractDate = $('#contractDate').val().trim();
		params.statusRecord = $('#status').val().trim();
		params.statusDelivery = $('#statusDelivery').val().trim();
		params.idRecordDelivery = $("#idRecordHidden").val().trim(); //khong can check phan quyen
		params.fromObjectAddress = $("#address").val().trim();
		params.fromObjectTax  = $("#taxCode").val().trim();
		params.fromObjectPhone  = $("#numberPhone").val().trim();
		params.fromRepresentative = $("#fromRepresentative").val().trim();
		params.fromObjectPosition = $("#fromPosition").val().trim();
		params.fromPage = $("#fromPage").val().trim();
		params.fromPagePlace = $("#fromPagePlace").val().trim();
		params.fromPageDate = $("#pageDate").val().trim();
		params.fromFax = $('#fromFax').val();
		params.toPermanentAddress = $("#toPermanentAddress").val().trim();
		params.toRepresentative = $("#toRepresentative").val().trim();
		params.toObjectPosition = $("#toPosition").val().trim();
		params.toIdNO = $("#toIdNO").val().trim();
		params.toIdNODate = $("#idNODate").val().trim();
		params.toIdNOPlace = $("#toIdNOPlace").val().trim();
		params.freezer = Utils.returnMoneyValue($('#freezer').val().trim());
		params.refrigerator = Utils.returnMoneyValue($("#refrigerator").val().trim());
		params.fromShopCode = $("#shopFromCode").val().trim();
		params.toShopCode = $("#shopToCode").val().trim();
		params.toBusinessLicense = $('#toBusinessLicense').val().trim();
		params.toBusinessDate = $('#toBusinessDate').val().trim();
		params.toBusinessPlace = $('#toBusinessPlace').val().trim();
		params.createDate = createDate;

		params.addressName = $('#addressName').val().trim();
		params.street = $('#street').val().trim();
		params.provinceName = $('#provinceName').val().trim();
		params.districtName = $('#districtName').val().trim();
		params.wardName = $('#wardName').val().trim();

		params.note = $('#note').val();

		var toShopCodeHidden = $("#fromShopHidden").val().trim();

		if (EquipmentManagerDelivery._customerId == null) {
			if (toShopCodeHidden == $("#shopToCode").val().trim()) {
				params.customerId = $("#customerIdHidden").val().trim();
			}
		} else {
			params.customerId = EquipmentManagerDelivery._customerId;
		}
		params.staffCode = $('#staffMonitorCode').combobox('getValue');

		params.id = params.idRecordDelivery;

		var curLength = lstContentDelivery.length;
		if (lstGroup.length != curLength || lstProvider.length != curLength || lstEquipCode.length != curLength
			|| lstSeriNumber.length != curLength || lstDepreciation.length != curLength || lstEquipPrice.length != curLength
			|| lstHealth.length != curLength || lstStock.length != curLength) {
			$('#errMsgInfo').html("Thiết thông tin thiết bi").show();
			return false;
		}

		if(lstContentDelivery.length > 0){
			params.lstContentDelivery = lstContentDelivery;
			params.lstGroup = lstGroup;
			params.lstProvider = lstProvider;
			params.lstStock = lstStock;
			params.lstEquipCode = lstEquipCode;
			params.lstSeriNumber = lstSeriNumber;
			params.lstDepreciation = lstDepreciation;
			params.lstEquipPrice = lstEquipPrice;
			params.lstHealth = lstHealth;
			params.lstManufacturingYear = lstManufacturingYear;
		}

		if (params.statusRecord == 2 && lstSeriNumber.length > 0) {
			for (var i = 0; i < lstSeriNumber.length; i++) {
				if (lstSeriNumber[i] == null || lstSeriNumber[i] == '') {
					$('#errMsgInfo').html("Vui lòng nhập đầy đủ thông tin seri khi chọn trạng thái Duyệt").show();
					return false;
				}
			}
		}

		if($("#idRecordHidden").val() != "" && EquipmentManagerDelivery._lstEquipInRecord != null){
			params.lstEquipDelete = "";
			for(var i=0; i< EquipmentManagerDelivery._lstEquipInRecord.length; i++){
				params.lstEquipDelete+=EquipmentManagerDelivery._lstEquipInRecord[i]+',';
			}
		}
		
		var arrFile = DropzonePlugin._dropzoneObject.getFilesWithStatus(Dropzone.ADDED);
		if (EquipmentManagerDelivery._arrFileDelete == undefined || EquipmentManagerDelivery._arrFileDelete == null) {
			EquipmentManagerDelivery._arrFileDelete = [];
		}
		if (arrFile != null && arrFile != undefined && arrFile.length > 0) {
			if (EquipmentManagerDelivery._countFile == 5) {
				$('#errMsgInfo').html("Tổng số tập tin đính kèm không được vượt quá 5").show();
				return false;
			}
			var maxLength = 5 - EquipmentManagerDelivery._countFile;
			if(maxLength < 0){
				maxLength = 0;
			}
			if (arrFile.length > maxLength) {
				$('#errMsgInfo').html("Số tập tin thêm mới không được vượt quá " + maxLength).show();
				return false;
			}
			msg = UploadUtil.getMsgByFileRequireCheck(UploadUtil._typeFileRequireCheck.equipRecord);
			if (msg.trim().length > 0) {
				$('#errMsgInfo').html(msg).show();
				return false;
			}			
		}
		params.equipAttachFileStr = EquipmentManagerDelivery._arrFileDelete.join(',');
		
		var isStaff = false;
		var shopToCode = $('#shopToCode').val().trim();
		if(shopToCode != undefined && shopToCode != null && shopToCode != ''){
			 $.getJSON('/equipment-manage-delivery/getListGSNPP?toShopCode='+shopToCode, function(result) {
				 if(result.rows != undefined && result.rows != null && result.rows.length > 0) {
				 	for(var i = 0, sz = result.rows.length; i < sz; i++) {
				 		if ($('#staffMonitorCode').combobox('getValue').trim() == result.rows[i].staffCode) {
							isStaff = true;
						}
				 	}
				 }
				 if (result.rows.length == 0) {
				 	msg = 'Không tìm thấy "Đơn vị" BÊN MƯỢN hoặc Mã giám sát không quản lý "Đơn vị" BÊN MƯỢN';
					err = '#shopToCode';
					$('#errMsgInfo').html(msg).show();
					$(err).focus();
					return false;
				 } else if (!isStaff) {
					msg = 'Mã giám sát không quản lý "Đơn vị" BÊN MƯỢN';
					err = '#staffMonitorCode';
					$('#errMsgInfo').html(msg).show();
					$(err).focus();
					return false;
				} else {
					$.messager.confirm('Xác nhận', "Bạn có muốn cập nhật biên bản này ?", function(r){
						if (r){
							if (arrFile != null && arrFile != undefined && arrFile.length > 0) {
								params = JSONUtil.getSimpleObject2(params);
								UploadUtil.updateAdditionalDataForUpload(params);
								UploadUtil.startUpload('errMsgInfo');
								return false;
							}						
							// ko co chinh sua file upload
							Utils.saveData(params, '/equipment-manage-delivery/update-record-delivery', null, 'errMsgInfo', function(data){	
								if(data.error){
									$('#errMsgInfo').html(data.errMsgInfo).show();
									return false;
								}else{
									$('#successMsgInfo').html("Cập nhật dữ liệu thành công.").show();
									EquipmentManagerDelivery._lstEquipInsert = null;
									EquipmentManagerDelivery._editIndex = null;
									EquipmentManagerDelivery._lstEquipInRecord = null;
									EquipmentManagerDelivery._numberEquipInRecord = null;
									if($('#status').val() != 4){					
										setTimeout(function(){
											$('#successMsgInfo').html("").hide(); 
											window.location.href = '/equipment-manage-delivery/change-record?id='+ $("#idRecordHidden").val().trim();						
										}, 3000);
									}else{
										setTimeout(function(){
											window.location.href = '/equipment-manage-delivery/info';						              		
										}, 3000);
									}
								}
							}, null, null, null, null);
						}else{
							// if($("#idRecordHidden").val()!=''){
							// 	$('#equipmentCodeInGrid' + EquipmentManagerDelivery._editIndex).remove();
							// 	$('#seriNumberInGrid').remove();
							// 	$('#contentDelivery').remove();
							// 	$('#gridEquipmentDelivery').datagrid({url : '/equipment-manage-delivery/search-equipment-in-record',queryParams:{idRecordDelivery:$("#idRecordHidden").val()}});
							// 	EquipmentManagerDelivery._editIndex = null;
							// 	EquipmentManagerDelivery._lstEquipInRecord = null;
							// 	EquipmentManagerDelivery._lstEquipInsert = null;
							// }
						}
					});		
				}
			});
		}
		
	},
	// /**
	//  * Chinh sua bien ban
	//  * @author nhutnn
	//  * @since 18/12/2014
	//  */
	// updateRecordDelivery: function(){
	// 	$('#errMsgInfo').html('').hide();
	// 	var msg ='';
	// 	var err = '';
	// 	var nContractTrim = $('#numberContract').val().trim();
	// 	$('#numberContract').val(nContractTrim);
	// 	if(msg.length == 0 && $('#numberContract').val().indexOf(' ')!=-1){
	// 		msg = 'Số hợp đồng không được có khoảng trắng';
	// 		err = '#numberContract';
	// 	}
	// 	if(msg.length == 0){
	// 		msg = Utils.getMessageOfSpecialCharactersValidate('numberContract','Số hợp đồng',Utils._NAME_CUSTYPE);
	// 		err = '#numberContract';
	// 	}
	// 	if(msg.length == 0){
	// 		msg = Utils.getMessageOfRequireCheck('numberContract','Số hợp đồng');
	// 		err = '#numberContract';
	// 	}	
	// 	if(msg.length == 0){
	// 		msg = Utils.getMessageOfRequireCheck('contractDate','Ngày hợp đồng');
	// 		err = '#contractDate';
	// 	}				
	// 	var contractDate = $('#contractDate').val().trim();
	// 	if(msg.length == 0 && contractDate != '' && !Utils.isDate(contractDate)){
	// 		msg = 'Ngày hợp đồng không tồn tại hoặc không đúng định dạng dd/mm/yyyy.';
	// 		err = '#contractDate';
	// 	}	

	// 	if ($('#shopFromCode').val() == '' && msg.length == 0) {
	// 		msg = 'Bạn chưa nhập giá trị cho trường "Đơn vị" BÊN CHO MƯỢN';
	// 		err = '#shopFromCode';
	// 	}
	// 	if ($('#address').val() == '' && msg.length == 0) {
	// 		msg = 'Bạn chưa nhập giá trị cho trường "Địa chỉ" BÊN CHO MƯỢN';
	// 		err = '#address';
	// 	}
	// 	if ($('#taxCode').val() == '' && msg.length == 0) {
	// 		msg = 'Bạn chưa nhập giá trị cho trường "Mã số thuế" BÊN CHO MƯỢN';
	// 		err = '#taxCode';
	// 	}
	// 	if ($('#numberPhone').val() == '' && msg.length == 0) {
	// 		msg = 'Bạn chưa nhập giá trị cho trường "Điện thoại" BÊN CHO MƯỢN';
	// 		err = '#numberPhone';
	// 	}
	// 	if ($('#fromRepresentative').val() == '' && msg.length == 0) {
	// 		msg = 'Bạn chưa nhập giá trị cho trường "Người đại diện" BÊN CHO MƯỢN';
	// 		err = '#fromRepresentative';
	// 	}
	// 	if ($('#fromPosition').val() == '' && msg.length == 0) {
	// 		msg = 'Bạn chưa nhập giá trị cho trường "Chức vụ" BÊN CHO MƯỢN';
	// 		err = '#fromPosition';
	// 	}
	// 	if ($('#fromPage').val() == '' && msg.length == 0) {
	// 		msg = 'Bạn chưa nhập giá trị cho trường "Giấy ủy quyền" BÊN CHO MƯỢN';
	// 		err = '#fromPage';
	// 	}
	// 	if ($('#fromPagePlace').val() == '' && msg.length == 0) {
	// 		msg = 'Bạn chưa nhập giá trị cho trường "Nơi cấp giấy quỷ quyền" BÊN CHO MƯỢN';
	// 		err = '#fromPagePlace';
	// 	}
	// 	var fromPDate = $('#pageDate').val().trim();
	// 	if(msg.length == 0 && fromPDate != '' && !Utils.isDate(fromPDate)){
	// 		msg = 'Ngày cấp GPKD không tồn tại hoặc không đúng định dạng dd/mm/yyyy.';
	// 		err = '#pageDate';
	// 	}	
	// 	if(msg.length == 0){
	// 		msg = Utils.getMessageOfRequireCheck('pageDate','Ngày cấp giấy ủy quyền" BÊN CHO MƯỢN');
	// 		err = '#pageDate';
	// 	}

	// 	if(msg.length == 0){
	// 		msg = Utils.getMessageOfSpecialCharactersValidate('shopToCode','Mã đơn vị BÊN MƯỢN',Utils._CODE);
	// 		err = '#shopToCode';
	// 	}
	// 	if(msg.length == 0){
	// 		msg = Utils.getMessageOfRequireCheck('shopToCode','Mã đơn vị BÊN MƯỢN');
	// 		err = '#shopToCode';
	// 	}	
	// 	if(msg.length == 0){
	// 		msg = Utils.getMessageOfRequireCheck('customerCode','Mã khách hàng');
	// 		err = '#customerCode';
	// 	}	
	// 	if(msg.length == 0){
	// 		msg = Utils.getMessageOfSpecialCharactersValidate('customerCode','Mã khách hàng',Utils._CODE);
	// 		err = '#customerCode';
	// 	}
	// 	if ($('#toObjectAddress').val() == '' && msg.length == 0) {
	// 		msg = 'Bạn chưa nhập giá trị cho trường "Địa chỉ đặt tủ" BÊN MƯỢN';
	// 		err = '#toObjectAddress';
	// 	}
	// 	// if ($('#toBusinessLicense').val() == '' && msg.length == 0) {
	// 	// 	msg = 'Bạn chưa nhập giá trị cho trường "Số GP ĐKKD" BÊN MƯỢN';
	// 	// 	err = '#toBusinessLicense';
	// 	// }
	// 	// if ($('#toBusinessPlace').val() == '' && msg.length == 0) {
	// 	// 	msg = 'Bạn chưa nhập giá trị cho trường "Nơi cấp GP ĐKKD BÊN MƯỢN';
	// 	// 	err = '#toBusinessPlace';
	// 	// }
	// 	var toBusinessDate = $('#toBusinessDate').val().trim();
	// 	if(msg.length == 0 && toBusinessDate != '' && !Utils.isDate(toBusinessDate)){
	// 		msg = 'Ngày cấp GPKD không tồn tại hoặc không đúng định dạng dd/mm/yyyy.';
	// 		err = '#toBusinessDate';
	// 	}	
	// 	// if(msg.length == 0){
	// 	// 	msg = Utils.getMessageOfRequireCheck('toBusinessDate','Ngày cấp GPKD');
	// 	// 	err = '#toBusinessDate';
	// 	// }
	// 	if ($('#toRepresentative').val() == '' && msg.length == 0) {
	// 		msg = 'Bạn chưa nhập giá trị cho trường "Người đại diện" BÊN MƯỢN';
	// 		err = '#toRepresentative';
	// 	}
	// 	if ($('#toPosition').val() == '' && msg.length == 0) {
	// 		msg = 'Bạn chưa nhập giá trị cho trường "Chức vụ người đại diện" BÊN MƯỢN';
	// 		err = '#toPosition';
	// 	}
	// 	if ($('#staffMonitorCode').combobox('getValue') == '' && msg.length == 0) {
	// 		msg = 'Bạn chưa nhập giá trị cho trường "Mã giám sát" BÊN MƯỢN';
	// 		err = '#staffMonitorCode';
	// 	}
	// 	// var isStaff = false;
	// 	// for (var i = 0, size = EquipmentManagerDelivery._lstGSNPP.length; i < size; i++) {
	// 	// 	if ($('#staffMonitorCode').combobox('getValue') == EquipmentManagerDelivery._lstGSNPP.staffCode) {
	// 	// 		isStaff = true;
	// 	// 	}
	// 	// }
	// 	// if (!isStaff) {
	// 	// 	msg = 'Mã giám sát không quản lý "Đơn vị" BÊN MƯỢN';
	// 	// 	err = '#staffMonitorCode';
	// 	// }

	// 	var freezer = Utils.returnMoneyValue($('#freezer').val().trim());
	// 	var refrigerator = Utils.returnMoneyValue($("#refrigerator").val().trim());
	// 	var intFreezer = parseInt(freezer);
	// 	var intRefrigerator = parseInt(refrigerator);
	// 	if (msg.length == 0) {
	// 		if(freezer != '') {
	// 			if (intFreezer != freezer) {
	// 				msg = 'Tủ mát phải là số nguyên dương có tối đa 17 chữ số';
	// 				err = '#freezer';
	// 			} else if (intFreezer < 0  || intFreezer > 99999999999999999) {
	// 				msg = 'Tủ mát phải là số nguyên dương có tối đa 17 chữ số';
	// 				err = '#freezer';
	// 			}
	// 		} else {
	// 			msg = 'Tủ mát phải là số nguyên dương có tối đa 17 chữ số';
	// 			err = '#freezer';
	// 		}
	// 	}
	// 	if (msg.length == 0) {
	// 		if (refrigerator != '') {
	// 			if (intRefrigerator != refrigerator) {
	// 				msg = 'Tủ đông phải là số nguyên dương có tối đa 17 chữ số';
	// 				err = '#refrigerator';
	// 			} else if (intRefrigerator < 0  || intRefrigerator > 99999999999999999) {
	// 				msg = 'Tủ đông phải là số nguyên dương có tối đa 17 chữ số';
	// 				err = '#refrigerator';
	// 			}
	// 		} else {
	// 			msg = 'Tủ đông phải là số nguyên dương có tối đa 17 chữ số';
	// 				err = '#refrigerator';
	// 		}
	// 	}

	// 	if(msg.length > 0){
	// 		$('#errMsgInfo').html(msg).show();
	// 		$(err).focus();
	// 		return false;
	// 	}


		
	// 	var rowEquip = $('#gridEquipmentDelivery').datagrid('getRows');
	// 	if( rowEquip.length == 0 ){
	// 		$('#errMsgInfo').html('Không có thiết bị để cập nhật').show();
	// 		return false;
	// 	}
	// 	//var lstIdEquip = new Array();
	// 	var lstEquipCode = new Array();
	// 	var lstSeriNumber = new Array();
	// 	var lstContentDelivery = new Array();
	// 	var lstDepreciation = new Array();
		
	// 	// var ij =  EquipmentManagerDelivery._numberEquipInRecord == null ? 0: (EquipmentManagerDelivery._numberEquipInRecord);
	// 	// for (; ij < rowEquip.length-1; ij++) {
	// 	// 	lstEquipCode.push(rowEquip[ij].equipmentCode);
	// 	// 	lstSeriNumber.push(rowEquip[ij].seriNumber);
	// 	// 	lstContentDelivery.push(rowEquip[ij].contentDelivery);
	// 	// 	// if(rowEquip[ij].equipmentId != undefined){
	// 	// 	// 	lstIdEquip.push(rowEquip[ij].equipmentId);
	// 	// 	// }else{
	// 	// 	// 	lstIdEquip.push(-1);
	// 	// 	// }
	// 	// }

	// 	for (var i = 0; i < rowEquip.length; i++) {
	// 		if (rowEquip[i].seriNumber == null || rowEquip[i].depreciation == null) {
	// 			if ((rowEquip[i].equipmentId != null && rowEquip[i].equipmentCode == null) || rowEquip[i].equipmentId == undefined) {
	// 				lstEquipCode.push($('#equipmentCodeInGrid' + EquipmentManagerDelivery._editIndex).val());
	// 			} else {
	// 				lstEquipCode.push(rowEquip[i].equipmentCode);
	// 			}
	// 			if (rowEquip[i].contentDelivery != null) {
	// 				lstContentDelivery.push(rowEquip[i].contentDelivery);
	// 			} else if ($('#contentDelivery').val() != undefined) {
	// 				lstContentDelivery.push($('#contentDelivery').val());
	// 			}
	// 			if (rowEquip[i].seriNumber != null) {
	// 				lstSeriNumber.push(rowEquip[i].seriNumber);
	// 			} else {
	// 				if ($('#seriNumberInGrid' + i).val() != undefined && $('#seriNumberInGrid' + i).val() != null) {
	// 					lstSeriNumber.push($('#seriNumberInGrid' + i).val().trim());	
	// 				} else {
	// 					lstSeriNumber.push($('#seriNumberInGrid' + i).val());
	// 				}
					
	// 			}
	// 			if (rowEquip[i].depreciation != null) {
	// 				lstDepreciation.push(rowEquip[i].depreciation);
	// 			} else {
	// 				var depreciation = null;
	// 				if ($('#depreciationInGrid' + i).val() != undefined && $('#depreciationInGrid' + i).val() != null) {
	// 					depreciation = $('#depreciationInGrid' + i).val().trim();
	// 				} else {
	// 					depreciation = $('#depreciationInGrid' + i).val();
	// 				}
					
	// 				var intDepreciation = parseInt(depreciation);
	// 				if ((depreciation != null && depreciation != intDepreciation) || intDepreciation < 0) {
	// 					msg = 'Vui lòng nhập Số tháng khấu hao là số nguyên dương';
	// 					$('#errMsgInfo').html(msg).show();
	// 					err = '#depreciationInGrid' + i;
	// 					$(err).focus();
	// 					return false;
	// 				} else {
	// 					if ($('#depreciationInGrid' + i).val() != undefined && $('#depreciationInGrid' + i).val() != null) {
	// 						lstDepreciation.push($('#depreciationInGrid' + i).val().trim());
	// 					} else {
	// 						lstDepreciation.push($('#depreciationInGrid' + i).val());
	// 					}
	// 				}
	// 			}				
	// 		} else if (i >= EquipmentManagerDelivery._numberEquipInRecord) {
	// 			if ((rowEquip[i].equipmentId != null && rowEquip[i].equipmentCode == null) || rowEquip[i].equipmentId == undefined) {
	// 				lstEquipCode.push($('#equipmentCodeInGrid' + EquipmentManagerDelivery._editIndex).val());
	// 			} else {
	// 				lstEquipCode.push(rowEquip[i].equipmentCode);
	// 			}
	// 			if (rowEquip[i].contentDelivery != null) {
	// 				lstContentDelivery.push(rowEquip[i].contentDelivery);
	// 			} else if ($('#contentDelivery').val() != undefined) {
	// 				lstContentDelivery.push($('#contentDelivery').val());
	// 			}
	// 			if (rowEquip[i].seriNumber == null) {
	// 				lstSeriNumber.push('');
	// 			} else {
	// 				lstSeriNumber.push(rowEquip[i].seriNumber);
	// 			}
				
	// 			lstDepreciation.push(rowEquip[i].depreciation);			
	// 		}	
	// 	}
	// 	// if (ij == rowEquip.length) {
	// 	// 	for (var i = 0; i < rowEquip.length; i++) {
	// 	// 		if (rowEquip[i].seriNumber == null) {
	// 	// 			lstEquipCode.push(rowEquip[i].equipmentCode);
	// 	// 			//lstSeriNumber.push(rowEquip[i].seriNumber);
	// 	// 			lstContentDelivery.push(rowEquip[i].contentDelivery);
	// 	// 		}
	// 	// 	}
	// 	// }
		
	// 	if ($('#equipmentCodeInGrid' + EquipmentManagerDelivery._editIndex).val() != undefined && $('#equipmentCodeInGrid' + EquipmentManagerDelivery._editIndex).val() != '') {
	// 		var rowEq = $('#gridEquipmentDelivery').datagrid('getRows');
	// 		for (var i=0; i < rowEq.length-1; i++){
	// 			if (rowEq[i].equipmentCode == $('#equipmentCodeInGrid' + EquipmentManagerDelivery._editIndex).val().trim()) {
	// 				$('#errMsgInfo').html("Mã thiết bị "+rowEq[i].equipmentCode+" đã tồn tại!").show();
	// 				return false;
	// 			}
	// 			if ($('#depreciationInGrid' + i).val() == '' && $('#depreciationInGrid' + i) != undefined) {
	// 				$('#errMsgInfo').html('Bạn chưa xử lý xong dòng dữ liệu hiện tại!').show();
	// 				return false;
	// 			}
	// 		}
	// 		for (var i=0; i < rowEq.length; i++){
	// 			if ($('#depreciationInGrid' + i).val() == '' && $('#depreciationInGrid' + i) != undefined) {
	// 				$('#errMsgInfo').html('Bạn chưa xử lý xong dòng dữ liệu hiện tại!').show();
	// 				return false;
	// 			}
	// 		}
	// 		// if (rowEq.length > 1) {
	// 		// 	lstEquipCode.push($('#equipmentCodeInGrid' + EquipmentManagerDelivery._editIndex).val());
	// 		// }
			
	// 	} else if ($('#equipmentCodeInGrid' + EquipmentManagerDelivery._editIndex).val() == '') {
	// 		$('#errMsgInfo').html('Bạn chưa xử lý xong dòng thiết bị hiện tại!').show();
	// 		return false;
	// 	} else if (EquipmentManagerDelivery._numberEquipInRecord == null || 
	// 		rowEquip.length-1 >= EquipmentManagerDelivery._numberEquipInRecord) {
	// 			// lstEquipCode.push(rowEquip[rowEquip.length-1].equipmentCode);
	// 	}




	// 	// if($('#seriNumberInGrid').val() != undefined && $('#seriNumberInGrid').val() != ''){
	// 	// 	lstSeriNumber.push($('#seriNumberInGrid').val());
	// 	// }else if($('#seriNumberInGrid').val() == ''){
	// 	// 	// $('#errMsgInfo').html('Bạn chưa xử lý xong dòng thiết bị hiện tại!').show();
	// 	// 	// return false;
	// 	// }else{
	// 	// 	if(EquipmentManagerDelivery._numberEquipInRecord == null || rowEquip.length-1 >= EquipmentManagerDelivery._numberEquipInRecord){
	// 	// 		lstSeriNumber.push(rowEquip[rowEquip.length-1].seriNumber);
	// 	// 	}
	// 	// }

	// 	if($('#contentDelivery').val() != undefined){
	// 		// lstContentDelivery.push($('#contentDelivery').val());
	// 	} else if (EquipmentManagerDelivery._numberEquipInRecord == null || rowEquip.length-1 >= EquipmentManagerDelivery._numberEquipInRecord){
	// 			lstContentDelivery.push(rowEquip[rowEquip.length-1].contentDelivery);
	// 	}
	// 	// if(rowEquip[rowEquip.length-1].equipmentId != undefined){
	// 	// 	lstIdEquip.push(rowEquip[rowEquip.length-1].equipmentId);
	// 	// }else{
	// 	// 	lstIdEquip.push(-1);
	// 	// }
		
	// 	var params=new Object(); 		
	// 	params.numberContract = $('#numberContract').val().trim();
	// 	params.contractDate = $('#contractDate').val().trim();
	// 	params.statusRecord = $('#status').val().trim();
	// 	params.statusDelivery = $('#statusDelivery').val().trim();
	// 	params.idRecordDelivery = $("#idRecordHidden").val().trim(); //khong can check phan quyen
	// 	params.fromObjectAddress = $("#address").val().trim();
	// 	params.fromObjectTax  = $("#taxCode").val().trim();
	// 	params.fromObjectPhone  = $("#numberPhone").val().trim();
	// 	params.fromRepresentative = $("#fromRepresentative").val().trim();
	// 	params.fromObjectPosition = $("#fromPosition").val().trim();
	// 	params.fromPage = $("#fromPage").val().trim();
	// 	params.fromPagePlace = $("#fromPagePlace").val().trim();
	// 	params.fromPageDate = $("#pageDate").val().trim();
	// 	params.fromFax = $('#fromFax').val();
	// 	params.toObjectAddress = $("#toObjectAddress").val().trim();
	// 	params.toRepresentative = $("#toRepresentative").val().trim();
	// 	params.toObjectPosition = $("#toPosition").val().trim();
	// 	params.toIdNO = $("#toIdNO").val().trim();
	// 	params.toIdNODate = $("#idNODate").val().trim();
	// 	params.toIdNOPlace = $("#toIdNOPlace").val().trim();
	// 	params.freezer = Utils.returnMoneyValue($('#freezer').val().trim());
	// 	params.refrigerator = Utils.returnMoneyValue($("#refrigerator").val().trim());
	// 	params.fromShopCode = $("#shopFromCode").val().trim();
	// 	params.toShopCode = $("#shopToCode").val().trim();
	// 	params.toBusinessLicense = $('#toBusinessLicense').val().trim();
	// 	params.toBusinessDate = $('#toBusinessDate').val().trim();
	// 	params.toBusinessPlace = $('#toBusinessPlace').val().trim();

	// 	var toShopCodeHidden = $("#fromShopHidden").val().trim();

	// 	if (EquipmentManagerDelivery._customerId == null) {
	// 		if (toShopCodeHidden == $("#shopToCode").val().trim()) {
	// 			params.customerId = $("#customerIdHidden").val().trim();
	// 		}
	// 	} else {
	// 		params.customerId = EquipmentManagerDelivery._customerId;
	// 	}
	// 	params.staffCode = $('#staffMonitorCode').combobox('getValue');

	// 	params.id = params.idRecordDelivery;
	// 	// params.lstIdEquip = lstIdEquip;
	// 	if(lstEquipCode.length > 0){
	// 		params.lstEquipCode = lstEquipCode;
	// 		params.lstSeriNumber = lstSeriNumber;
	// 		params.lstContentDelivery = lstContentDelivery;	
	// 		params.lstDepreciation = lstDepreciation;
	// 	}		
		
	// 	if (params.statusRecord == 2 && lstSeriNumber.length > 0) {
	// 		for (var i = 0; i < lstSeriNumber.length; i++) {
	// 			if (lstSeriNumber[i] == null || lstSeriNumber[i] == '') {
	// 				$('#errMsgInfo').html("Vui lòng nhập đầy đủ thông tin seri khi chọn trạng thái Duyệt").show();
	// 				return false;
	// 			}
	// 		}
	// 	}

	// 	if($("#idRecordHidden").val() != "" && EquipmentManagerDelivery._lstEquipInRecord != null){
	// 		params.lstEquipDelete = "";
	// 		for(var i=0; i<EquipmentManagerDelivery._lstEquipInRecord.length; i++){
	// 			params.lstEquipDelete+=EquipmentManagerDelivery._lstEquipInRecord[i]+',';
	// 		}
	// 	}
		
	// 	var arrFile = DropzonePlugin._dropzoneObject.getFilesWithStatus(Dropzone.ADDED);
	// 	if (EquipmentManagerDelivery._arrFileDelete == undefined || EquipmentManagerDelivery._arrFileDelete == null) {
	// 		EquipmentManagerDelivery._arrFileDelete = [];
	// 	}
	// 	if (arrFile != null && arrFile != undefined && arrFile.length > 0) {
	// 		if (EquipmentManagerDelivery._countFile == 5) {
	// 			$('#errMsgInfo').html("Tổng số tập tin đính kèm không được vượt quá 5").show();
	// 			return false;
	// 		}
	// 		var maxLength = 5 - EquipmentManagerDelivery._countFile;
	// 		if(maxLength < 0){
	// 			maxLength = 0;
	// 		}
	// 		if (arrFile.length > maxLength) {
	// 			$('#errMsgInfo').html("Số tập tin thêm mới không được vượt quá " + maxLength).show();
	// 			return false;
	// 		}
	// 		msg = UploadUtil.getMsgByFileRequireCheck(UploadUtil._typeFileRequireCheck.equipRecord);
	// 		if (msg.trim().length > 0) {
	// 			$('#errMsgInfo').html(msg).show();
	// 			return false;
	// 		}			
	// 	}
	// 	params.equipAttachFileStr = EquipmentManagerDelivery._arrFileDelete.join(',');
		
	// 	var isStaff = false;
	// 	var shopToCode = $('#shopToCode').val().trim();
	// 	if(shopToCode != undefined && shopToCode != null && shopToCode != ''){
	// 		 $.getJSON('/equipment-manage-delivery/getListGSNPP?toShopCode='+shopToCode, function(result) {
	// 			 if(result.rows != undefined && result.rows != null && result.rows.length > 0) {
	// 			 	for(var i = 0, sz = result.rows.length; i < sz; i++) {
	// 			 		if ($('#staffMonitorCode').combobox('getValue').trim() == result.rows[i].staffCode) {
	// 						isStaff = true;
	// 					}
	// 			 	}
	// 			 }
	// 			 if (result.rows.length == 0) {
	// 			 	msg = 'Không tìm thấy "Đơn vị" BÊN MƯỢN hoặc Mã giám sát không quản lý "Đơn vị" BÊN MƯỢN';
	// 				err = '#shopToCode';
	// 				$('#errMsgInfo').html(msg).show();
	// 				$(err).focus();
	// 				return false;
	// 			 } else if (!isStaff) {
	// 				msg = 'Mã giám sát không quản lý "Đơn vị" BÊN MƯỢN';
	// 				err = '#staffMonitorCode';
	// 				$('#errMsgInfo').html(msg).show();
	// 				$(err).focus();
	// 				return false;
	// 			} else {
	// 				$.messager.confirm('Xác nhận', "Bạn có muốn cập nhật biên bản này ?", function(r){
	// 					if (r){
	// 						if (arrFile != null && arrFile != undefined && arrFile.length > 0) {
	// 							params = JSONUtil.getSimpleObject2(params);
	// 							UploadUtil.updateAdditionalDataForUpload(params);
	// 							UploadUtil.startUpload('errMsgInfo');
	// 							return false;
	// 						}						
	// 						// ko co chinh sua file upload
	// 						Utils.saveData(params, '/equipment-manage-delivery/update-record-delivery', null, 'errMsgInfo', function(data){	
	// 							if(data.error){
	// 								$('#errMsgInfo').html(data.errMsgInfo).show();
	// 								return false;
	// 							}else{
	// 								$('#successMsgInfo').html("Cập nhật dữ liệu thành công.").show();
	// 								EquipmentManagerDelivery._lstEquipInsert = null;
	// 								EquipmentManagerDelivery._editIndex = null;
	// 								EquipmentManagerDelivery._lstEquipInRecord = null;
	// 								EquipmentManagerDelivery._numberEquipInRecord = null;
	// 								if($('#status').val() != 4){					
	// 									setTimeout(function(){
	// 										$('#successMsgInfo').html("").hide(); 
	// 										window.location.href = '/equipment-manage-delivery/change-record?id='+ $("#idRecordHidden").val().trim();						
	// 									}, 3000);
	// 								}else{
	// 									setTimeout(function(){
	// 										window.location.href = '/equipment-manage-delivery/info';						              		
	// 									}, 3000);
	// 								}
	// 							}
	// 						}, null, null, null, null);
	// 					}else{
	// 						// if($("#idRecordHidden").val()!=''){
	// 						// 	$('#equipmentCodeInGrid' + EquipmentManagerDelivery._editIndex).remove();
	// 						// 	$('#seriNumberInGrid').remove();
	// 						// 	$('#contentDelivery').remove();
	// 						// 	$('#gridEquipmentDelivery').datagrid({url : '/equipment-manage-delivery/search-equipment-in-record',queryParams:{idRecordDelivery:$("#idRecordHidden").val()}});
	// 						// 	EquipmentManagerDelivery._editIndex = null;
	// 						// 	EquipmentManagerDelivery._lstEquipInRecord = null;
	// 						// 	EquipmentManagerDelivery._lstEquipInsert = null;
	// 						// }
	// 					}
	// 				});		
	// 			}
	// 		});
	// 	}




	// 	//@tamvnm: bo ko check ky khi luu. @since: 22/05/2015
	// 	// Utils.getJSONDataByAjax(params, '/equipment-manage-delivery/check-equip-period', function(dt){						
	// 	// 	if(dt.error){
	// 	// 		$('#errMsgInfo').html(dt.errMsgInfo).show();
	// 	// 		return false;
	// 	// 	}else{
	// 	// 		var isStaff = false;
	// 	// 		var shopToCode = $('#shopToCode').val().trim();
	// 	// 		if(shopToCode != undefined && shopToCode != null && shopToCode != ''){
	// 	// 			 $.getJSON('/equipment-manage-delivery/getListGSNPP?toShopCode='+shopToCode, function(result) {
	// 	// 				 if(result.rows != undefined && result.rows != null && result.rows.length > 0) {
	// 	// 				 	for(var i = 0, sz = result.rows.length; i < sz; i++) {
	// 	// 				 		if ($('#staffMonitorCode').combobox('getValue').trim() == result.rows[i].staffCode) {
	// 	// 							isStaff = true;
	// 	// 						}
	// 	// 				 	}
	// 	// 				 }
	// 	// 				 if (!isStaff) {
	// 	// 					msg = 'Mã giám sát không quản lý "Đơn vị" BÊN MƯỢN';
	// 	// 					err = '#staffMonitorCode';
	// 	// 					$('#errMsgInfo').html(msg).show();
	// 	// 					$(err).focus();
	// 	// 					return false;
	// 	// 				} else {
	// 	// 					$.messager.confirm('Xác nhận', "Bạn có muốn cập nhật biên bản này ?", function(r){
	// 	// 						if (r){
	// 	// 							if (arrFile != null && arrFile != undefined && arrFile.length > 0) {
	// 	// 								params = JSONUtil.getSimpleObject2(params);
	// 	// 								UploadUtil.updateAdditionalDataForUpload(params);
	// 	// 								UploadUtil.startUpload('errMsgInfo');
	// 	// 								return false;
	// 	// 							}						
	// 	// 							// ko co chinh sua file upload
	// 	// 							Utils.saveData(params, '/equipment-manage-delivery/update-record-delivery', null, 'errMsgInfo', function(data){	
	// 	// 								if(data.error){
	// 	// 									$('#errMsgInfo').html(data.errMsgInfo).show();
	// 	// 									return false;
	// 	// 								}else{
	// 	// 									$('#successMsgInfo').html("Cập nhật dữ liệu thành công.").show();
	// 	// 									EquipmentManagerDelivery._lstEquipInsert = null;
	// 	// 									EquipmentManagerDelivery._editIndex = null;
	// 	// 									EquipmentManagerDelivery._lstEquipInRecord = null;
	// 	// 									EquipmentManagerDelivery._numberEquipInRecord = null;
	// 	// 									if($('#status').val() != 4){					
	// 	// 										setTimeout(function(){
	// 	// 											$('#successMsgInfo').html("").hide(); 
	// 	// 											window.location.href = '/equipment-manage-delivery/change-record?id='+ $("#idRecordHidden").val().trim();						
	// 	// 										}, 3000);
	// 	// 									}else{
	// 	// 										setTimeout(function(){
	// 	// 											window.location.href = '/equipment-manage-delivery/info';						              		
	// 	// 										}, 3000);
	// 	// 									}
	// 	// 								}
	// 	// 							}, null, null, null, null);
	// 	// 						}else{
	// 	// 							if($("#idRecordHidden").val()!=''){
	// 	// 								$('#equipmentCodeInGrid' + EquipmentManagerDelivery._editIndex).remove();
	// 	// 								$('#seriNumberInGrid').remove();
	// 	// 								$('#contentDelivery').remove();
	// 	// 								$('#gridEquipmentDelivery').datagrid({url : '/equipment-manage-delivery/search-equipment-in-record',queryParams:{idRecordDelivery:$("#idRecordHidden").val()}});
	// 	// 								EquipmentManagerDelivery._editIndex = null;
	// 	// 								EquipmentManagerDelivery._lstEquipInRecord = null;
	// 	// 								EquipmentManagerDelivery._lstEquipInsert = null;
	// 	// 							}
	// 	// 						}
	// 	// 					});		
	// 	// 				}
	// 	// 		});
	// 	// 	}
	// 	// }
	// 	// },null,null);
		
	// },
	/**
	 * thay doi combobox
	 * @author nhutnn
	 * @since 18/12/2014
	 */
	changeCombo: function() {
	    var status = $('#statusRecordLabel').val().trim();
	    var html = '';
	    if(status == '4'){
	    	html = '<option value="" selected="selected">Chọn</option>';
	    }else {
	    	html = '<option value="" selected="selected">Chọn</option> <option value="1" >Đã gửi</option> <option value="2" >Đã nhận</option> ';
	    }
	    $('#statusDeliveryLabel').html(html);
	    $('#statusDeliveryLabel').change();
	},
	/**
	 * thay doi combobox
	 * @author nhutnn
	 * @since 18/12/2014
	 */
	changeComboUpdate: function() {
	    var status = $('#status').val().trim();
	    var html = '';
	    if(status == '2'){
				html = '<option value="0" selected="selected">Chưa gửi</option> <option value="1" >Đã gửi</option>';
	    }else {
	    	html = '<option value="0" selected="selected">Chưa gửi</option>';
	    }
	    $('#statusDelivery').html(html);
	    $('#statusDelivery').change();
	},
	/**
	 * thay doi combobox khi tìm kiếm
	 * @author nhutnn
	 * @since 27/12/2014
	 */
	 
	changeComboSearch: function() {
	    var status = $('#statusRecord').val().trim();
	    var html = '';
	    if(status == '' || status == '2'){
	    	html = '<option value="" selected="selected">Tất cả</option><option value="0" >Chưa gửi</option> <option value="1" >Đã gửi</option> <option value="2" >Đã nhận</option> ';
	    }else if(status == '0' || status == '4'){
	    	html = '<option value="0" >Chưa gửi</option>';
	    }
	    $('#statusDelivery').html(html);
	    $('#statusDelivery').change();
	},
	/**
	 * su kien tren grid
	 * @author nhutnn
	 * @since 18/12/2014
	 */
	editInputInGrid: function(){
		$('#equipmentCodeInGrid' + EquipmentManagerDelivery._editIndex).bind('keyup', function(e){
			if(e.keyCode==keyCode_F9){
				EquipmentManagerDelivery.showPopupSearchEquipment();
			}
		});
		$('#equipmentCodeInGrid' + EquipmentManagerDelivery._editIndex).bind('keyup', function(e){
			if(e.keyCode==keyCodes.ENTER){
				EquipmentManagerDelivery.getEquipInEvent(false);
			}
		});
		$('input[id^=seriNumberInGrid]').bind('keyup', function(e){
			if(e.keyCode==keyCodes.ENTER){
				$('#errMsgInfo').html('').hide();
				var seriNumber = $(this).val().trim();
				var editSeri = $(this);
				var rowRe = $('#gridEquipmentDelivery').datagrid('getRows')[EquipmentManagerDelivery._editIndex];
				if(seriNumber != undefined && seriNumber != null && seriNumber != '' && rowRe.equipmentId == null 
					&& $('#equipmentCodeInGrid' + EquipmentManagerDelivery._editIndex).val()!=undefined && $('#equipmentCodeInGrid' + EquipmentManagerDelivery._editIndex).val()==""){
					var params = {seriNumber:seriNumber};
					if($("#idRecordHidden").val() != "" && EquipmentManagerDelivery._lstEquipInRecord != null){
						params.lstEquipDelete = "";
						for(var i=0; i<EquipmentManagerDelivery._lstEquipInRecord.length; i++){
							params.lstEquipDelete += EquipmentManagerDelivery._lstEquipInRecord[i]+',';
						}					
					}
					if(EquipmentManagerDelivery._lstEquipInsert != null){
						params.lstEquipAdd = "";
						for(var i=0; i<EquipmentManagerDelivery._lstEquipInsert.length; i++){
							params.lstEquipAdd += EquipmentManagerDelivery._lstEquipInsert[i]+',';
						}					
					}
					if ($("#equipLendCode").val() != undefined && $("#equipLendCode").val() != null && $("#equipLendCode").val() != '') {
						params.equipLendCode = $("#equipLendCode").val().trim();
					}
					params.customerId = $("#customerIdHidden").val();
					Utils.getJSONDataByAjaxNotOverlay(params, '/equipment-manage-delivery/getEquipment', function(equip){						
						if(equip != undefined && equip != null) {	
							if(equip.length == 1){
								equip[0].contentDelivery = null;
								var tempDep = null;
								if ($('#depreciationInGrid' + EquipmentManagerDelivery._editIndex) != undefined && $('#depreciationInGrid' + EquipmentManagerDelivery._editIndex).val() != '') {
									tempDep = $('#depreciationInGrid' + EquipmentManagerDelivery._editIndex).val();
								}
								$('#gridEquipmentDelivery').datagrid('updateRow',{
									index: EquipmentManagerDelivery._editIndex,	
									row: equip[0]
								});
								$('#contentDelivery').width($('#contentDelivery').parent().width());
								$('#equipmentCodeInGrid' + EquipmentManagerDelivery._editIndex).width($('#equipmentCodeInGrid' + EquipmentManagerDelivery._editIndex).parent().width());
								$(this).width($(this).parent().width());
								if ($('#depreciationInGrid' + EquipmentManagerDelivery._editIndex) != undefined && tempDep != null) {
									$('#depreciationInGrid' + EquipmentManagerDelivery._editIndex).val(tempDep);
								}

								EquipmentManagerDelivery.editInputInGrid();
							}else if(equip.length > 1){
								EquipmentManagerDelivery.showPopupSelectEquipment(equip);
							}else{
								// EquipmentManagerDelivery.showPopupSearchEquipment();
								editSeri.val('');
							}
						} else{
							editSeri.val('');
						}
						editSeri.change();
						EquipmentManagerDelivery.getEventContent();
					}, null, null);					
				}else if($('#equipmentCodeInGrid' + EquipmentManagerDelivery._editIndex).val()!=undefined && $('#equipmentCodeInGrid' + EquipmentManagerDelivery._editIndex).val()!=""){
					$('#errMsgInfo').html('').hide();
					var msg = Utils.getMessageOfSpecialCharactersValidate(editSeri.attr('id'),'Số serial',Utils._SERIAL);
					if(msg.length==0 && $(this).val().indexOf(" ") != -1 ){
						msg = "Số serial không được có khoảng trắng";
					}
					if(msg.length > 0){
						$('#errMsgInfo').html(msg).show();
						$(err).focus();
						return false;
					}
					if(EquipmentManagerDelivery._editIndex!=null 
						&& $('#gridEquipmentDelivery').datagrid('getRows')[EquipmentManagerDelivery._editIndex].equipmentId != null){
						EquipmentManagerDelivery.insertEquipmentInGrid();	
					}else{
						EquipmentManagerDelivery.getEquipInEvent(true);
					}		
				}		
			}
		});
		// $('input[id*=stockInGrid]').bind('keyup', function(e){
		// 	 var txt = 'stockInGrid';	
		// 	if(e.keyCode==keyCode_F9){
		// 		VCommonJS.showDialogSearch2({
		// 			inputs : [
		// 		        {id:'code', maxlength:50, label:'Mã đơn vị'},
		// 		        {id:'name', maxlength:250, label:'Tên đơn vị'},
		// 		    ],
		// 		    url : '/equipment-stock-change-manage/get-list-equip-stock-vo',
		// 		    columns : [[
		// 		        {field:'codeName', title:'Đơn vị', align:'left', width: 110, sortable:false, resizable:false, formatter:function(value,row,index) {
		// 		        	var nameCode = row.shopCode + '-' + row.shopName;
		// 		        	return Utils.XSSEncode(nameCode);         
		// 		        }},
		// 		        {field:'code', title:'Mã kho', align:'left', width: 200, sortable:false, resizable:false},
		// 		        {field:'choose', title:'', align:'center', width:40, sortable:false, resizable:false, formatter:function(value,row,index) {
		// 		        	return '<a href="javascript:void(0)" onclick="Utils.fillCodeForInputTextByDialogSearch2(\''+txt+'\', ' + row.equipStockId +', \''+row.code+'\', \''+row.name+'\','+Utils._isShowInputTextByDlSearch2.code+');">chọn</a>';            
		// 		        }}
		// 		    ]]
		// 		});
		// 	}
		// });
		$('#contentDelivery').bind('keyup', function(e){
			if(e.keyCode==keyCodes.ENTER){
				if($('#equipmentCodeInGrid' + EquipmentManagerDelivery._editIndex).val()!=undefined && $('#equipmentCodeInGrid' + EquipmentManagerDelivery._editIndex).val()!=""){
					if($('#seriNumberInGrid').val()==undefined
						||($('#seriNumberInGrid').val()!=undefined && $('#seriNumberInGrid').val()!="")){
						//EquipmentManagerDelivery.insertEquipmentInGrid();
						if(EquipmentManagerDelivery._editIndex!=null 
							&& $('#gridEquipmentDelivery').datagrid('getRows')[EquipmentManagerDelivery._editIndex].equipmentId != null){
							EquipmentManagerDelivery.insertEquipmentInGrid();	
						}else{
							EquipmentManagerDelivery.getEquipInEvent(true);
						}
					}
				}
			}
		});
	},
	/**
	 * lay thiet bi khi xu ly su kien tren grid
	 * @author nhutnn
	 * @since 29/01/2015
	 */
	getEquipInEvent: function(isNew){
		$('#errMsgInfo').html('').hide();
		var equipCode = $('#equipmentCodeInGrid' + EquipmentManagerDelivery._editIndex).val().trim();
		if(equipCode != undefined && equipCode != null && equipCode != ''){
			var content = $('#contentDelivery').val();
			var seri = $('#seriNumberInGrid').val();
			var params = {
				equipCode: equipCode,
				equipLendCode: $("#equipLendCode").val() != null ? $("#equipLendCode").val() : "",
				customerId: $("#customerIdHidden").val()
			};
			if($("#idRecordHidden").val() != "" && EquipmentManagerDelivery._lstEquipInRecord != null){
				params.lstEquipDelete = "";
				for(var i=0; i<EquipmentManagerDelivery._lstEquipInRecord.length; i++){
					params.lstEquipDelete += EquipmentManagerDelivery._lstEquipInRecord[i]+',';
				}					
			}
			if(EquipmentManagerDelivery._lstEquipInsert != null){
				params.lstEquipAdd = "";
				for(var i=0; i<EquipmentManagerDelivery._lstEquipInsert.length; i++){
					params.lstEquipAdd += EquipmentManagerDelivery._lstEquipInsert[i]+',';
				}					
			}
			Utils.getJSONDataByAjaxNotOverlay(params, '/equipment-manage-delivery/getEquipment', function(equip){						
				if(equip != undefined && equip != null && equip.length == 1) {
					equip[0].contentDelivery = null;
					var tempDep = null;
					if ($('#depreciationInGrid' + EquipmentManagerDelivery._editIndex) != undefined && $('#depreciationInGrid' + EquipmentManagerDelivery._editIndex).val() != '') {
						tempDep = $('#depreciationInGrid' + EquipmentManagerDelivery._editIndex).val();
					}
					$('#gridEquipmentDelivery').datagrid('updateRow',{
						index: EquipmentManagerDelivery._editIndex,	
						row: equip[0]
					});
					$('#contentDelivery').val(content).width($('#contentDelivery').parent().width());
					$('#equipmentCodeInGrid' + EquipmentManagerDelivery._editIndex).width($('#equipmentCodeInGrid' + EquipmentManagerDelivery._editIndex).parent().width());
					$('#seriNumberInGrid').width($('#seriNumberInGrid').parent().width());					
					EquipmentManagerDelivery.editInputInGrid();
					if($('#seriNumberInGrid').val() == ""){
						$('#seriNumberInGrid').val(seri);
					}
					if ($('#depreciationInGrid' + EquipmentManagerDelivery._editIndex) != undefined && tempDep != null) {
						$('#depreciationInGrid' + EquipmentManagerDelivery._editIndex).val(tempDep);
					}
					if(isNew!= null && isNew && $('#seriNumberInGrid').val() != ""){
						EquipmentManagerDelivery.insertEquipmentInGrid();	
					}
				} else{
					$('#errMsgInfo').html("Mã thiết bị "+$('#equipmentCodeInGrid' + EquipmentManagerDelivery._editIndex).val()+" không có trên hệ thống hoặc không thuộc quyền kho quản lý!").show();
					$('#gridEquipmentDelivery').datagrid('deleteRow',EquipmentManagerDelivery._editIndex);
					EquipmentManagerDelivery.addEquipmentInRecord();
					$('#equipmentCodeInGrid' + EquipmentManagerDelivery._editIndex).val('');
					$('#equipmentCodeInGrid' + EquipmentManagerDelivery._editIndex).focus();
				}
				$('#equipmentCodeInGrid' + EquipmentManagerDelivery._editIndex).change();	
				EquipmentManagerDelivery.getEventContent();
			}, null, null);
		}	
	},
	/**
	 * them thiet bi vao bien ban
	 * @author nhutnn
	 * @since 30/01/2015
	 */
	addEquipmentInRecord: function(){
		if(EquipmentManagerDelivery._editIndex!=null){
			var equipCode = null;
			if ($('#equipmentCodeInGrid' + EquipmentManagerDelivery._editIndex).val() != undefined) {
				equipCode = $('#equipmentCodeInGrid' + EquipmentManagerDelivery._editIndex).val().trim();
			}
			if(equipCode != undefined && equipCode != null && equipCode != ''){
				if(EquipmentManagerDelivery._editIndex!=null 
					&& $('#gridEquipmentDelivery').datagrid('getRows')[EquipmentManagerDelivery._editIndex].equipmentId != null){
					if ($('#depreciationInGrid' + EquipmentManagerDelivery._editIndex) != undefined 
						&& $('#depreciationInGrid' + EquipmentManagerDelivery._editIndex).val() != '') {
						var meg = Utils.getMessageOfInvaildInteger('depreciationInGrid' + EquipmentManagerDelivery._editIndex, 'Số tháng khấu hao');
						if (meg.length > 0) {
							$('#errMsgInfo').html(meg).show();
							$('#depreciationInGrid' + EquipmentManagerDelivery._editIndex).focus();
							return;	
						}
					}
					EquipmentManagerDelivery.insertEquipmentInGrid();	
				}else{
					EquipmentManagerDelivery.getEquipInEvent(true);
				}	
			}else{			
				// $('#errMsgInfo').html("Xin nhập mã thiết bị tại dòng xử lý hiện tại!").show();
				// $('#equipmentCodeInGrid' + EquipmentManagerDelivery._editIndex).focus();
				// return;	
				EquipmentManagerDelivery.insertEquipmentInGrid();				
			}	
		}else{
			EquipmentManagerDelivery.insertEquipmentInGrid();	
		}			
	},
	/**
	 * Xoa tap tin dinh kem
	 * 
	 * @author nhutnn
	 * @since 10/02/2015
	 * */
	removeEquipAttachFile: function(id){
		if (id == undefined || id == null || isNaN(id)) {
			return;
		}
		if (EquipmentManagerDelivery._arrFileDelete == null || EquipmentManagerDelivery._arrFileDelete == undefined) {
			EquipmentManagerDelivery._arrFileDelete = [];
		}
		EquipmentManagerDelivery._arrFileDelete.push(id);
		$('#divEquipAttachFile'+id).remove();
		if (EquipmentManagerDelivery._countFile > 0) {
			EquipmentManagerDelivery._countFile = EquipmentManagerDelivery._countFile - 1;
		}
	},

	selectStock: function (stockId, shortCode ) {
		if (shortCode != null && shortCode != undefined && stockId != null && stockId != undefined) {
			$.messager.confirm('Xác nhận', 'Bạn quyết định chọn kho ?', function(r){
					if (r){
						$('#stockInGrid').html(shortCode);
						$('#common-dialog-search-2-textbox').dialog('close');
						$('#common-dialog-search-2-textbox').remove();
					}
				});	
		}
	},
	disableField: function() {
		if ($("#statusRecordHidden").val() == EquipmentManagerDelivery._approved) { // da duyet
			disableDateTimePicker('createDate');
			disabled('numberContract');
			disableDateTimePicker('contractDate');
			disableSelectbox('statusDelivery');
			disabled('shopFromCode');
			disabled('address');
			disabled('taxCode');
			disabled('numberPhone');
			disabled('fromRepresentative');
			disabled('fromPosition');
			disabled('fromPage');
			disabled('fromPagePlace');
			disableDateTimePicker('pageDate');
			disabled('fromFax');
			disabled('shopToCode');
			disabled('customerCode');
			disabled('toPermanentAddress');
			disabled('toBusinessLicense');
			disabled('toBusinessPlace');
			disableDateTimePicker('toBusinessDate');
			disabled('toRepresentative');
			disabled('toPosition');
			disabled('toIdNO');
			disabled('toIdNOPlace');
			disableDateTimePicker('idNODate');
			disabled('cbxStaffCode');
			disabled('freezer');
			disabled('refrigerator');
			disabled('note');
			// disabled('imgAddFile');
			$('img[id^=imgAddFile]').hide();
			$('a[id^=imgDelete]').hide();
			/** Cho phep cap nhat Giam sat */
			//disableCombo('staffMonitorCode');
			//disabled('btnUpdate');
			//tach dia chi dat tu
			disabled('addressName');
			disabled('street');
			disabled('provinceName');
			disabled('districtName');
			disabled('wardName');
			// $('img[id=imgFolder]').unbind('click');
			// $('img[id=imgFolder]').bind('click',function(){
			// 	var recordId = $('#idRecordHidden').val();
			// 	var objectId = $(this).attr("alt");
			// 	General.downloadEquipAttachFileProcess(objectId, recordId);
			// });

		}
		if ($('#shopToCode').is('[disabled=disabled]') == false && $('#idRecordHidden').val() != '') {
			disabled('shopToCode');
			disabled('customerCode');
		}
	},
	initUploadFile: function(urlUpdate) {
		if ($('#imgFolder').length == 0 || $('#imgFolder').is('[disabled=disabled]') == false) {
			UploadUtil.initUploadFileByEquipment({
					url: urlUpdate,	
					elementSelector: 'body',
					elementId: 'body',
					clickableElement: '.addFileBtn',
					parallelUploads: 5
					//maxFilesize: 5
				}, function (data) {
					if(data.error){
						$('#errMsgInfo').html(data.errMsg).show();
						return false;
					}else{
						if($("#idRecordHidden").val() == ""){
							$('#successMsgInfo').html("Cập nhật dữ liệu thành công.").show();	
							EquipmentManagerDelivery._lstEquipInsert = null;
							EquipmentManagerDelivery._editIndex = null;				
							setTimeout(function(){
								$('#successMsgInfo').html("").hide(); 
								window.location.href = '/equipment-manage-delivery/info';						              		
							}, 3000);
						}else{
							$('#successMsgInfo').html("Cập nhật dữ liệu thành công.").show();
							EquipmentManagerDelivery._lstEquipInsert = null;
							EquipmentManagerDelivery._editIndex = null;
							EquipmentManagerDelivery._lstEquipInRecord = null;
							EquipmentManagerDelivery._numberEquipInRecord = null;
							if($('#status').val() != 4){					
								setTimeout(function(){
									$('#successMsgInfo').html("").hide(); 
									window.location.href = '/equipment-manage-delivery/change-record?id='+ $("#idRecordHidden").val().trim();						
								}, 3000);
							}else{
								setTimeout(function(){
									window.location.href = '/equipment-manage-delivery/info';						              		
								}, 3000);
							}
						}
					}
				});
		} else {
			$('#imgCancel').hide();
			$('img[id=imgFolder]').unbind('click');
			$('img[id=imgFolder]').bind('click',function(){
				var recordId = $('#idRecordHidden').val();
				var objectId = $(this).attr("alt");
				General.downloadEquipAttachFileProcess(objectId, recordId);
			});


		}
		
	},
	getValueEquipGroup: function(rec) {
		var index = EquipmentManagerDelivery._editIndex;
		$('tr[datagrid-row-index=' + index +'] td[field=typeEquipment] div').text(rec.equipCategoryName);
		$('tr[datagrid-row-index=' + index +'] td[field=capacity] div').text(rec.capacityStr);
		EquipmentManagerDelivery._categoryName = rec.equipCategoryName;
		EquipmentManagerDelivery._capacity = rec.capacityStr;
	},

	/**
	* Lay danh sach kho cua Npp tu action
	*
	* @author tamvnm
	* @since Oct 21 2015
	*/
	getDataEquipStock: function(isSetStockCombo) {
		var params = new Object();
		if (EquipmentManagerDelivery._shopToCode != '') {
			params.toShopCode = EquipmentManagerDelivery._shopToCode;
			Utils.getJSONDataByAjaxNotOverlay(params, '/equipment-manage-delivery/get-equipment-stock-combobox', function(result) {
				if (result.rows != undefined && result.rows != null && result.rows.length > 0) {
					EquipmentManagerDelivery._lstEquipStock = result.rows;
				}
				if (isSetStockCombo != undefined && isSetStockCombo == EquipmentManagerDelivery._isSetStockCombo) {
					var rows = [{
						stockCode: "",
						stockName: "",
						searchText: ""
					}];
					$('#stockEquipCBX' + EquipmentManagerDelivery._editIndex).combobox("loadData", rows);
					EquipmentManagerDelivery.setEquipStock();
				}
			}, null, null);
		} else {
			EquipmentManagerDelivery._lstEquipStock = new Object();
			if (isSetStockCombo != undefined && isSetStockCombo == EquipmentManagerDelivery._isSetStockCombo) {
				var rows = [{
					stockCode: "",
					stockName: "",
					searchText: ""
				}];
				$('#stockEquipCBX' + EquipmentManagerDelivery._editIndex).combobox("loadData", rows);
				EquipmentManagerDelivery.setEquipStock();
			}
		}
	}
};

/*
 * END OF FILE - /web/web/resources/scripts/business/equipment/vnm.equipment-manage-delivery.js
 */

/*
 * START OF FILE - /web/web/resources/scripts/business/equipment/vnm.equipment-manage-period.js
 */
var EquipmentManagePeriod = {
	
};
/*
 * END OF FILE - /web/web/resources/scripts/business/equipment/vnm.equipment-manage-period.js
 */

/*
 * START OF FILE - /web/web/resources/scripts/business/equipment/vnm.repair-equipment-manage-catalog.js
 */
var RepairEquipmentManageCatalog = {
	_VATTU: 'VATTU',  /** kiểm tra đơn giá vật tư; Chuc nang: QL sua chua*/
	_NHANCONG: 'NHANCONG', /** kiểm tra đơn giá nhân công; Chuc nang: QL sua chua*/
	_flagPagePheDuyet: false, /** co nay de biet trang phe duyet*/
	_paramsSearch: null, /** param nay cho search va export excel theo grid*/
	_valueTruMot: -1,
	_rolePermission: null,
	_roleKTNPP: 'KTNPP',
	_roleTNKD: 'TNKD',
	_maxWorkerPriceDetail: 0,
	_msgGiaNhanCongMax: '',
	_msgGiaNhanCongMaxFocus: '',
	_PHEDUYET: 1, /** Phe duyet; khi cap nhat phieu sua chua */
	lstId : null,
	_xhrDel :null,
	_pMap:null,
	isImportSuccess : false,
	_mapRepair:new Map(),
	paramForDelete:new Object(),
	_xhrSave : null,
	_editIndex: null,
	_mapEquip: null,
	_lstEquipAvailable:[],
	_mapDetail: null,
	_rowDgTmp: null,
	_mapEquipSaleplanCurrent: null,
	_mapEquipSaleplanNew: null,
	_params:null,
	_editIndex:null,
	_lstEquipInRecord:null,
	_numberEquipInRecord:null,
	_lstEquipInsert:null,
	_editIndex:null,
	_colum1Width:null,
	_colum1_1Width: null, //
	_colum2Width:null,
	_colum2_1Width: null, //
	_colum2_2Width: null, //
	_colum3Width:null,
	_colum3_1Width: null, //
	_colum4Width:null,
	_colum5Width:null,
	_countFile: 0,
	_arrFileDelete: null,
	_lstEquipGroup: null,
	_lstEquipItemId:new Array(),
	//tamvnm
	/*** vuongmq; 29/05/2015; lay danh sach nhom thiet bi khi chon loai thiet bi; ham getListEquipGroupVOByCategry tao trong common*/
	onchangeDdlEquipCategory : function(cbx) {
		var id = Number($(cbx).val());
		var html = '';
		if (id == undefined || id == null){
			equipCategoryId = 0;
		}
		Utils.getJSONDataByAjax({
			equipCategoryId: id
		//}, '/equipment-list-manage/getListEquipGroupVOByCategry', function(data) {
		}, '/commons/getListEquipGroupVOByCategry', function(data) {
			if (data.rows != undefined && data.rows != null && data.rows.length > 0){
				$('#ddlEquipGroupId').html('<option value="-1" selected="selected">Tất cả</option>');
				var rows = data.rows;
				for (var i = 0, size = rows.length; i < size; i++) {
					$('#ddlEquipGroupId').append('<option value="' + rows[i].id + '">' + Utils.XSSEncode(rows[i].code) + ' - ' + Utils.XSSEncode(rows[i].name) + '</option>');
				}
			} else {
				$('#ddlEquipGroupId').html('<option value="-2" selected="selected">Không có dữ liệu</option>').change();
			}
			$('#ddlEquipGroupId').change();
		}, null, null);
	},

	/*** tamvnm; 22/07/2015; lay danh sach nhom thiet bi khi chon loai thiet bi; ham getListEquipGroupVOByCategry tao trong common*/
	onchangeDdlEquipCategoryAuto : function(cbx) {
		var id = Number($(cbx).val());
		var html = '';
		if (id == undefined || id == null){
			equipCategoryId = 0;
		}
		Utils.getJSONDataByAjax({
			equipCategoryId: id
		}, '/commons/getListEquipGroupVOByCategry', function(data) {
			if (data.rows != undefined && data.rows != null && data.rows.length > 0){
				var equipGroup = data.rows;
				var obj = {
					id: -1,
					code: "Tất cả",
					name: "Tất cả",
					codeName: "Tất cả",
					searchText: unicodeToEnglish("Tất cả").toUpperCase(),
				};
				equipGroup.splice(0, 0, obj);
				for(var i = 0, sz = equipGroup.length; i < sz; i++) {
			 		equipGroup[i].searchText = unicodeToEnglish(equipGroup[i].code + " " + equipGroup[i].name);
			 		equipGroup[i].searchText = equipGroup[i].searchText.toUpperCase();
			 		equipGroup[i].code = Utils.XSSEncode(equipGroup[i].code);
			 		equipGroup[i].name = Utils.XSSEncode(equipGroup[i].name);
				}
			 	$('#ddlEquipGroupId').combobox("loadData", equipGroup);
			 	$('#ddlEquipGroupId').combobox("setValue", -1);
			} else {
				
			}

		}, null, null);
	},
	/** phuongvm, cai nay search trong MH: Ql sua chua  */
	searchRepair: function() {
		RepairEquipmentManageCatalog._mapRepair = new Map();
		$('#equipHistoryTemplateDiv').css("visibility","hidden").hide();
		var err = "";
		$('#errMsg').html('').hide();
		var msg="";
		var fDate = $('#fDate').val();
		var tDate =$('#tDate').val();
		if (msg.length == 0 && $('#fDate').val().trim().length > 0 && !Utils.isDate($('#fDate').val(),'/') && $('#fDate').val() != '__/__/____') {
			msg = 'Ngày tạo từ không tồn tại hoặc không đúng định dạng dd/mm/yyyy.';
			err = '#fDate';
		}
		if (msg.length == 0 && $('#tDate').val().trim().length > 0 && !Utils.isDate($('#tDate').val(),'/') && $('#tDate').val() != '__/__/____') {
			msg = 'Đến ngày không tồn tại hoặc không đúng định dạng dd/mm/yyyy.';
			err = '#tDate';
		}
		if (msg.length==0 && fDate.length > 0 && tDate.length>0 && !Utils.compareDate(fDate, tDate)) {
			msg = 'Ngày tạo từ phải là ngày trước hoặc cùng ngày với đến ngày';
		}
		if(msg.length>0){
			$('#errMsg').html(msg).show();
			$(err).focus();
			setTimeout(function(){$('#errMsg').hide();},3000);
			return false;
		}
		var equipCategoryId = $('#ddlEquipCategory').val();
		//tamvnm: thay doi thanh autoCompleteCombobox: -2 la truong hop tim group ko co trong BD
		// var equipGroupId = $('#ddlEquipGroupId').combobox("getValue");
		var equipGroupId = -1;
		if ($('#ddlEquipGroupId').combobox("getValue") != "") {
			if ($('#ddlEquipGroupId').combobox("getValue") != -1) {
				if (Utils.getMessageOfInvaildNumberNew($('#ddlEquipGroupId').combobox("getValue")).length == 0) {
					equipGroupId = $('#ddlEquipGroupId').combobox("getValue");
				} else {
					equipGroupId = -2;
				}
			}
		} else {
			$('#ddlEquipGroupId').combobox("setValue", -1);
		}
		var statusPayment = $('#cbxStatusPayment').val();
		var status = $('#cbxStatus').val();
		var equipCode = $('#txtCode').val();
		var formCode = $('#txtRepairCode').val();
		var fromDate = $('#fDate').val();
		var toDate = $('#tDate').val();
		var seri = $('#txtSerial').val();
		/** put params vao; xu dung cho export */
		RepairEquipmentManageCatalog._paramsSearch = new Object();
		RepairEquipmentManageCatalog._paramsSearch.equipCategoryId = equipCategoryId;
		RepairEquipmentManageCatalog._paramsSearch.equipGroupId = equipGroupId;
		RepairEquipmentManageCatalog._paramsSearch.statusPayment = statusPayment;
		RepairEquipmentManageCatalog._paramsSearch.status = status;
		RepairEquipmentManageCatalog._paramsSearch.equipCode = equipCode;
		RepairEquipmentManageCatalog._paramsSearch.formCode = formCode;
		RepairEquipmentManageCatalog._paramsSearch.fromDate = fromDate;
		RepairEquipmentManageCatalog._paramsSearch.toDate = toDate;
		RepairEquipmentManageCatalog._paramsSearch.seri = seri;
		$('#grid').datagrid('load',{
			page:1,
			equipCategoryId:equipCategoryId,
			equipGroupId:equipGroupId,
			statusPayment:statusPayment,
			status:status,
			equipCode:equipCode,
			formCode:formCode,
			fromDate:fromDate,
			toDate:toDate,
			seri:seri
			});
		
	},
	viewDetail:function(id, code){
		$('.SuccessMsgStyle').html('').hide();
		$('.ErrorMsgStyle').html('').hide();
		if (code != undefined && code != null && code.trim().length > 0) {
			$('#equipmentCodeFromSpan').html(code);
		} else {
			$('#equipmentCodeFromSpan').html("");
		}
		$('#equipHistoryTemplateDiv').css("visibility","visible").show();
		$('#equipHistoryDg').datagrid({
			url : "/equipment-repair-manage/searchRepairDetail",
			pagination : true,
	        rownumbers : true,
	        pageNumber : 1,
	        scrollbarSize: 0,
	        autoWidth: true,
	        pageList: [10],
	        autoRowHeight : true,
	        fitColumns : true,
			queryParams: {equipRepairId: id},
			width: $('#equipHistoryDgContainerGrid').width() - 15,
		    columns:[[	        
			    {field:'equipItemName', title: 'Hạng mục', width: 260, sortable: false, resizable: false , align: 'left', formatter: function(value,row,index){
			    	var hangmuc = "";
			    	if(row.equipItem!=null){
			    		hangmuc = row.equipItem.name;
			    	}
			    	return Utils.XSSEncode(hangmuc);
			    }},
			    {field:'repairCount', title: 'Lần sửa', width: 60, sortable: false, resizable: false , align: 'right', formatter: function(value,row,index){
			    	return value;
			    }},
			    {field:'createDateString', title: 'Ngày sửa', width: 80, sortable: false, resizable: false , align: 'center', formatter: function(value,row,index){
			    	return Utils.XSSEncode(value);
			    }},
			    {field:'warranty', title: 'Bảo hành (tháng)', width: 80, align: 'right', sortable: false, resizable: false, formatter: function(value,row,index){
			    	if (value != undefined && value != null) {
			    		return formatCurrency(value);
			    	}
			    	return '';
			    }},
			    {field:'materialPrice', title: 'Đơn giá vật tư', width: 90, align: 'right', sortable: false, resizable: false, formatter: function(value,row,index){
			    	if (value != undefined && value != null) {
			    		return formatCurrency(value);
			    	}
			    	return '';
			    }},
			    {field:'workerPrice', title: 'Đơn giá nhân công', width: 90, align: 'right',sortable: false, resizable: false, formatter: function(value,row,index){
			    	if (value != undefined && value != null) {
			    		return formatCurrency(value);
			    	}
			    	return '';
			    }},
			    {field:'quantity', title: 'Số lượng', width:50, sortable: false, resizable: false , align: 'right', formatter: function(value,row,index){
			    	if (value != undefined && value != null) {
			    		return formatCurrency(value);
			    	}
			    	return '';
			    }},	
			    {field:'totalAmount',title: 'Tổng tiền', width: 100, sortable: false, resizable: false, align: 'right', formatter:function(value,row,index){
			    	if (value != undefined && value != null) {
			    		return formatCurrency(value);
			    	}
			    	return '';
			    }}
		    ]],
		    onLoadSuccess :function(data){	    	
		    	$('.datagrid-header-rownumber').html('STT');
		    	//Utils.functionAccessFillControl('equipHistoryDg');
	   		 	$(window).resize();    		 
		    }
		});
	},
	/** phuongvm, QLsua chua-> cap nhat trang thai cua phieu sua chua;*/
	updateStatus: function() {
		$('#errMsg').html('').hide();	
		var statusForm = $('#cbxStatusUpdate').val().trim();
		if (statusForm < 0) {
			$('#errMsg').html('Vui lòng chọn trạng thái').show();
			return;
		}
		var arra = RepairEquipmentManageCatalog._mapRepair.valArray;		
		var lstId = new Array();
		if (arra == null || arra.length == 0) {
			$('#errMsg').html('Bạn chưa chọn phiếu sửa chữa để cập nhật. Vui lòng chọn phiếu').show();
			return;
		}		
		for (var i=0, size=arra.length; i<size; ++i) {
			lstId.push(arra[i].id);			
		}
		var params = new Object();
		params.lstId = lstId;
		params.statusForm = statusForm;
		if ($("#rejectReason").val()!=null && $("#rejectReason").val()!='') {
			params.rejectReason = $("#rejectReason").val();
		}
		var title = {title:'Thông tin lỗi ',pageList:10};
		Utils.addOrSaveData(params, '/equipment-repair-manage/update', null, 'errMsg', function(data) {
			if ( data.lstErr != undefined && data.lstErr != null && data.lstErr.length > 0 ) {
				VCommonJS.showDialogList({
				    data : data.lstErr,
				    dialogInfo: title,
				    columns : [[
				        {field:'formCode', title:'Mã biên bản', align:'left', width: 110, sortable:false, resizable:false, formatter: function(value, row, index) {
					    	return Utils.XSSEncode(value);
					    }},
				        {field:'formStatusErr', title:'Trạng thái biên bản', align:'center', width: 110, sortable:false, resizable:false, formatter: function(value,row,index){
				        	var html="";
							if(value == 1){
								html = '<img src="/resources/images/icon_delete.png" height="20" width="20" style="margin-right:5px;" title="Error">';
							}
							return html;
						} }
					]]
				});	
			} else {
				$('#successMsg').html('Lưu dữ liệu thành công').show();
				setTimeout(function(){$('#successMsg').html('').hide();},1500);
			}
			RepairEquipmentManageCatalog._mapRepair = new Map();
			$('#grid').datagrid('reload');
		}, null, true,null, 'Bạn có muốn cập nhật phiếu sửa chữa?', null, true);
		
	},
	/** phuongvm, cai nay search trong MH: phe duyet sua chua*/
	searchRepairEx:function(){
		RepairEquipmentManageCatalog._mapRepair = new Map();
		$('#equipHistoryTemplateDiv').css("visibility","hidden").hide();
		var err = "";
		$('#errMsg').html('').hide();
		var msg="";
		var fDate = $('#fDate').val();
		var tDate =$('#tDate').val();
		if(msg.length == 0 && $('#fDate').val().trim().length>0 && !Utils.isDate($('#fDate').val(),'/') && $('#fDate').val() != '__/__/____') {
			msg = 'Ngày tạo từ không tồn tại hoặc không đúng định dạng dd/mm/yyyy.';
			err = '#fDate';
		}
		if(msg.length == 0 && $('#tDate').val().trim().length>0 && !Utils.isDate($('#tDate').val(),'/') && $('#tDate').val() != '__/__/____') {
			msg = 'Đến ngày không tồn tại hoặc không đúng định dạng dd/mm/yyyy.';
			err = '#tDate';
		}
		if(msg.length==0 && fDate.length>0 && tDate.length>0 && !Utils.compareDate(fDate, tDate)){
			msg = 'Ngày tạo từ phải là ngày trước hoặc cùng ngày với đến ngày';
		}
		if(msg.length>0){
			$('#errMsg').html(msg).show();
			$(err).focus();
			setTimeout(function(){$('#errMsg').hide();},3000);
			return false;
		}
		var equipCode = $('#txtCode').val();
		var formCode = $('#txtRepairCode').val();
		var fromDate = $('#fDate').val();
		var toDate = $('#tDate').val();
		var seri = $('#txtSerial').val();
		var customerCode = $('#customerCode').val();
		var customerName = $('#customerName').val();
		var shopKendo = $("#shop").data("kendoMultiSelect");
		var dataShop = shopKendo.dataItems();
		var lstShopId = '';
		if (dataShop != null && dataShop.length > 0) {
			lstShopId = dataShop[0].shopId;
			for (var i = 1, sz = dataShop.length; i < sz; i++) {
				lstShopId += "," + dataShop[i].shopId;
			}
		}
		$('#grid').datagrid('load',{
			page:1,
			customerCode:customerCode,
			customerName:customerName,
			equipCode:equipCode,
			formCode:formCode,
			fromDate:fromDate,
			toDate:toDate,
			strListShopId:lstShopId,
			seri:seri
			});
		
	},
	/**
	 * Them mot dong hang muc
	 * @author phuongvm
	 * @since 05/01/2015
	 */
	insertEquipmentItemInGrid: function(){	
		$('#errMsg').html('').hide();
		var sz = $("#gridDetail").datagrid("getRows").length;
//		if(RepairEquipmentManageCatalog._editIndex != null && RepairEquipmentManageCatalog._editIndex == sz-1){
//			var row = $('#gridDetail').datagrid('getRows')[RepairEquipmentManageCatalog._editIndex];
//			
//			if($('#equipmentCodeInGrid').val() == ''){
//				$('#errMsgInfo').html('Bạn chưa xử lý xong dòng thiết bị thêm mới!').show();
//				return false;
//			}else if($('#equipmentCodeInGrid').val() != undefined){
//				row.equipmentCode = $('#equipmentCodeInGrid').val();
//			}
//			if($('#seriNumberInGrid').val() == ''){
//				$('#errMsgInfo').html('Bạn chưa xử lý xong dòng thiết bị thêm mới!').show();
//				return false;
//			}else if($('#seriNumberInGrid').val() != undefined){
//				row.seriNumber = $('#seriNumberInGrid').val();
//			}
//			if($('#contentDelivery').val() != undefined){
//				row.contentDelivery = $('#contentDelivery').val();
//			}
//			
//			$('#equipmentCodeInGrid').remove();
//			$('#seriNumberInGrid').remove();
//			$('#contentDelivery').remove();
//			$('#gridDetail').datagrid('updateRow',{
//				index: RepairEquipmentManageCatalog._editIndex,	
//				row: row
//			});
//			if(RepairEquipmentManageCatalog._lstEquipInsert == null){
//				RepairEquipmentManageCatalog._lstEquipInsert = new Array();
//			}
//			RepairEquipmentManageCatalog._lstEquipInsert.push(row.equipmentCode);
//		}		
		RepairEquipmentManageCatalog._editIndex = sz;	
		$('#gridDetail').datagrid("appendRow", {});	
		// F9 tren grid
		RepairEquipmentManageCatalog.editInputInGrid();
		$("#gridDetail").datagrid("selectRow", RepairEquipmentManageCatalog._editIndex);	
		$('#equipItemCodeInGrid_').focus();
		
		/*RepairEquipmentManageCatalog._colum1Width =  $('#equipItemCodeInGrid_').parent().width();
		RepairEquipmentManageCatalog._colum1_1Width = $('#warantyDateInGrid_').parent().width(); //
		RepairEquipmentManageCatalog._colum2Width =  $('#warantyInGrid_').parent().width();
		RepairEquipmentManageCatalog._colum2_1Width =  $('#warantyExpiredDateInGrid_').parent().width(); //
		RepairEquipmentManageCatalog._colum2_2Width =  $('#materialPriceDefaultInGrid_').parent().width(); //
		RepairEquipmentManageCatalog._colum3Width =  $('#materialPriceInGrid_').parent().width();
		RepairEquipmentManageCatalog._colum3_1Width = $('#wokerPriceDefaultInGrid_').parent().width(); //
		RepairEquipmentManageCatalog._colum4Width =  $('#wokerPriceInGrid_').parent().width();
		RepairEquipmentManageCatalog._colum5Width =  $('#quantityInGrid_').parent().width();
		$('#equipItemCodeInGrid_').width($('#equipItemCodeInGrid_').parent().width());
		$('#warantyDateInGrid_').width($('#warantyDateInGrid_').parent().width());	//
		$('#warantyInGrid_').width($('#warantyInGrid_').parent().width());	
		$('#warantyExpiredDateInGrid_').width($('#warantyExpiredDateInGrid_').parent().width());// 
		$('#materialPriceDefaultInGrid_').width($('#materialPriceDefaultInGrid_').parent().width());//
		$('#materialPriceInGrid_').width($('#materialPriceInGrid_').parent().width());	
		$('#wokerPriceDefaultInGrid_').width($('#wokerPriceDefaultInGrid_').parent().width()); //
		$('#wokerPriceInGrid_').width($('#wokerPriceInGrid_').parent().width());
		$('#quantityInGrid_').width($('#quantityInGrid_').parent().width());*/
		RepairEquipmentManageCatalog.loadDateTimerPickerNgayBaoHanh();
	},
	/**
	 * su kien tren grid
	 * @author phuongvm
	 * @since 05/01/2015
	 */
	
	editInputInGrid: function(){
		$('#equipItemCodeInGrid_').bind('keyup', function(e){
			if(e.keyCode==keyCode_F9){
				RepairEquipmentManageCatalog._lstEquipItemId = new Array();
				var rows = $('#gridDetail').datagrid('getRows');
				if(rows!=null && rows.length > 0){
					for(var i =0;i<rows.length;i++){
						RepairEquipmentManageCatalog._lstEquipItemId.push(rows[i].id);
					}
				}
				RepairEquipmentManageCatalog.showPopupSearchEquipItem();
			}
		});
	},
	/**
	 * Show popup tim kiem hang muc
	 * @author phuongvm
	 * @since 05/01/2015
	 */
	showPopupSearchEquipItem: function() {
		$('.SuccessMsgStyle').html('');
		$('.ErrorMsgStyle').html('');
		var msg = '';
		if (msg.length == 0) {
			msg = Utils.getMessageOfRequireCheck('txtEquipCode', 'Mã thiết bị');
			err = '#txtEquipCode';
			if (msg.trim().length > 0) {
				$('#errMsg').html('Vui lòng nhập Mã thiết bị hợp lệ (nhấn F9)').show();
				$(err).focus();
				return;
			}
		}
		var equipCode = $('#txtEquipCode').val().trim();
		VCommonJS.showDialogSearch2WithCheckbox({
			params : {
				equipCode: equipCode,
				strEquip: RepairEquipmentManageCatalog._lstEquipItemId.join(','),
				status: 1
			},
			inputs : [
		        {id:'name', maxlength:250, label:'Tên hạng mục',width:450},
		    ],
		    chooseCallback : function(listObj) {
		        if (listObj != undefined && listObj != null && listObj.length > 0) {
		        	//var rows = new Array();
		        	for (var i=0;i<listObj.length ; i++) {
		        		var donGiaVTDM = RepairEquipmentManageCatalog.getVatTuNhanCongHangMuc(listObj[i], RepairEquipmentManageCatalog._VATTU);
		        		var donGiaNCDM = RepairEquipmentManageCatalog.getVatTuNhanCongHangMuc(listObj[i], RepairEquipmentManageCatalog._NHANCONG);
		        		var row = {
		        			id:	listObj[i].id,
		        			hangMuc:listObj[i].name,	
		        			ngayBatDauBaoHanh:'',//
		        			baoHanh:'',	
		        			ngayHetHanBaoHanh:'',//
		        			donGiaVatTuDinhMuc: donGiaVTDM , //
		        			donGiaVatTu:'',	
		        			donGiaNhanCongDinhMuc: donGiaNCDM, //
		        			donGiaNhanCong:'',	
		        			soLuong:'',
		        			description: '', //01/07/2015; them ghi chu vuot dinh muc	
		        			tongTien:'',	
		        			lanSua:listObj[i].repairCount,
		        			dataView: listObj[i]
		        		};
		        		var idx1 = 0;
		        		var rowsss = $('#gridDetail').datagrid("getRows");
		        		if (rowsss.length > 0) {
			        		idx1 = rowsss.length - 1;
			        	}
		        		$('#gridDetail').datagrid('insertRow',{row:row,index:idx1});
		        		RepairEquipmentManageCatalog._lstEquipItemId.push(row.id);
		        	}
		        	RepairEquipmentManageCatalog.loadDateTimerPickerNgayBaoHanh();
		        	RepairEquipmentManageCatalog.bindEventChangeText();
		        } 
		        $('#common-dialog-search-2-textbox').dialog("close");
		    },
		    //url : '/commons/search-equipItem',
		    url: '/commons/search-equipItem-popup-repair',
		    title: 'Chọn hạng mục',
		    columns : [[
		        {field:'code', title:'Mã hạng mục', align:'left', width: 100, sortable:false, resizable:false, formatter: function(value, row, index) {
			    	return Utils.XSSEncode(value);
			    }},
		        {field:'name', title:'Tên hạng mục', align:'left', width: 200, sortable:false, resizable:false, formatter: function(value, row, index) {
			    	return Utils.XSSEncode(value);
			    }},
		        {field:'cb', checkbox:true, align:'center', width:80,sortable : false,resizable : false},
		    ]]
		});
	},
	/**
	 * loadDateTimerPickerNgayBaoHanh va ngay het han bao hanh luong
	 * @author vuongmq
	 * @since 07/04/2015
	 */
	loadDateTimerPickerNgayBaoHanh: function() {
		$(".warantyDateInGridClzz:not(.hasDatepicker)").each(function() {
			setDateTimePicker($(this).attr("id"));
		});
		/** dateTimePicker cho ngay het han bao hanh luong*/
		$(".warantyExpiredDateInGridClzz:not(.hasDatepicker)").each(function() {
			setDateTimePicker($(this).attr("id"));
			var valueCheckNgayHetHanBaoHanh = $('#checkExpiredDate').val();
			if (valueCheckNgayHetHanBaoHanh == 0 || valueCheckNgayHetHanBaoHanh == '0') {
				disableDateTimePicker($(this).attr("id"));
			}
			//enableDateTimePicker($(this).attr('id'));
		});
	},
	/**
	 * tra ve don gia vat tu, don gia nhan cong, de view len
	 * @author vuongmq
	 * @since 07/04/2015
	 */
	getVatTuNhanCongHangMuc: function(row, type){
		var value ='';
		if (row != undefined && row != null) {
			if (type == RepairEquipmentManageCatalog._VATTU) {
				if (row.fromMaterialPrice != null && row.toWorkerPrice != null) {
					//value = formatCurrency(row.fromMaterialPrice) +' - '+ formatCurrency(row.toMaterialPrice);
					if (!isNaN(row.fromMaterialPrice) && !isNaN(row.toMaterialPrice)) {
						if (Number(row.fromMaterialPrice) != Number(row.toMaterialPrice)) {
							value = formatCurrency(row.fromMaterialPrice) +' - '+ formatCurrency(row.toMaterialPrice);
						} else {
							// truong hop bang nhau lay 1 gia tri
							value = formatCurrency(row.fromMaterialPrice);
						}
					}					
				} else if (row.fromMaterialPrice != null) {
					value = '>= '+formatCurrency(row.fromMaterialPrice);
				} else if (row.toMaterialPrice != null) {
					value = '<= '+formatCurrency(row.toMaterialPrice);
				}
			} else if (type == RepairEquipmentManageCatalog._NHANCONG) {
				if (row.fromWorkerPrice != null && row.toWorkerPrice != null) {
					//value = formatCurrency(row.fromWorkerPrice) +' - '+ formatCurrency(row.toWorkerPrice);
					if (!isNaN(row.fromWorkerPrice) && !isNaN(row.toWorkerPrice)) {
						if (Number(row.fromWorkerPrice) != Number(row.toWorkerPrice)) {
							value = formatCurrency(row.fromWorkerPrice) +' - '+ formatCurrency(row.toWorkerPrice);
						} else {
							// truong hop bang nhau lay 1 gia tri
							value = formatCurrency(row.fromWorkerPrice);
						}
					}
				} else if (row.fromWorkerPrice != null) {
					value = '>= '+formatCurrency(row.fromWorkerPrice);
				} else if (row.toWorkerPrice != null) {
					value = '<= '+formatCurrency(row.toWorkerPrice);
				}
			}
		}
		return value;
	},
	
	/**
	 * Show popup tim kiem thiet bi
	 * @author phuongvm
	 * @since 06/01/2015
	 */
	showPopupSearchEquipment: function() {
		$('.ErrorMsgStyle').html('').hide();
		$('#easyuiPopupSearchEquipment').show();
		$('#easyuiPopupSearchEquipment').dialog({  
	        closed: false,  
	        cache: false,  
	        modal: true,
	        width : 1000,
//	        height : 'auto',
	        onOpen: function(){	    
	        	//setDateTimePicker('yearManufacturing');
	        	Utils.bindComboboxEquipGroupEasyUICodeName('groupEquipment', null, 193);
	        	Utils.bindComboboxEquipGroupEasyUICodeName('providerId', null, 193);
	        	var year = new Date();
	        	year = year.getFullYear(); 
	        	var op = '<option value="" selected="selected"></option><option value="'+year+'">'+year+'</option>';
	        	for(var i=1; i<100; i++){
	        		op+='<option value="'+(year-i)+'">'+(year-i)+'</option>';
	        	}
	        	$('#yearManufacturing').html(op);
	        	$('#equipmentCode, #seriNumber, #typeEquipment').bind('keyup',function(event){
	        		if(event.keyCode == keyCodes.ENTER){
	        			$('#btnSeachEquipmentDlg').click(); 
	        		}
	        	});
	        	$('#yearManufacturing').bind('keyup',function(event){
	        		if(event.keyCode == keyCodes.ENTER){
	        			$('#btnSeachEquipmentDlg').click(); 
	        		}
	        	});
	        	$('#groupEquipment, #providerId').next().find(".combo-text.validatebox-text").bind('keyup',function(event){
	        		if(event.keyCode == keyCodes.ENTER){
	        			$('#btnSeachEquipmentDlg').click(); 
	        		}
	        	});
	        	$('#stockCode').bind('keyup',function(event){
	        		if(event.keyCode == keyCodes.ENTER){
	        			$('#btnSeachEquipmentDlg').click(); 
	        		}
	        	});
//	        	$('#stockCode').bind('keyup',function(event){
//	        		if(event.keyCode == keyCodes.F9){
//	        			RepairEquipmentManageCatalog.showPopupStock();
//	        		}
//	        	});
	        	RepairEquipmentManageCatalog.getEquipCatalog();
	        	var params = new Object();
	        	var groupId = $('#groupEquipment').combobox('getValue');
	        	if (groupId == -1 || groupId == "") {
	        		groupId = "";
	        	}
	        	var providerId = $('#providerId').combobox('getValue');
	        	if (providerId == -1 || providerId == "") {
	        		providerId = "";
	        	}
				params = {
						equipCode: $('#equipmentCode').val().trim(),
						seriNumber: $('#seriNumber').val().trim(),
						categoryId: $('#typeEquipment').val().trim(),
						// groupId: $('#groupEquipment').val().trim(),
						groupId: groupId,
						// providerId: $('#providerId').val().trim(),
						providerId: providerId,
						yearManufacture: $('#yearManufacturing').val().trim(),
						stockCode: $('#stockCode').val().trim()
					};
				$('#equipmentGridDialog').datagrid({
					url : '/equipment-repair-manage/search-equipment-delivery',
					autoRowHeight : true,
					rownumbers : true, 
					//fitColumns:true,
					pagination:true,
					scrollbarSize:0,
					pageSize:10,
					pageList: [10],
					width: $('#equipmentGridDialogContainer').width(),
					height: 'auto',
					autoWidth: true,	
					queryParams: params,
					columns:[[
						{field: 'select',title:'',width: 50,sortable:false,resizable:false, align: 'center', formatter: function(value, row, index) {
							return '<a href="javascript:void(0);" onclick="RepairEquipmentManageCatalog.selectEquipment('+index+');">Chọn</a>';
						}},	      
					    {field: 'equipmentCode',title:'Mã thiết bị',width: 200,sortable:false,resizable:false, align: 'left', formatter: function(value, row, index) {
							return Utils.XSSEncode(value);
						}},	
					    {field: 'seriNumber',title:'Số serial',width: 100,sortable:false,resizable:false, align: 'left', formatter: function(value, row, index) {
							return Utils.XSSEncode(value);
						}},	
					    {field: 'healthStatus',title:'Tình trạng thiết bị',width: 100,sortable:false,resizable:false, align: 'left', formatter: function(value, row, index) {
							return Utils.XSSEncode(value);
						}},	
					    {field: 'stock',title:'Kho',width: 120,sortable:false,resizable:false, align: 'left', formatter: function(value, row, index) {
					    	var html="";
							if(row != null && row.stockCode != null && row.stockName!= null){
								html = row.stockCode+' - '+row.stockName; 
							}
							return Utils.XSSEncode(html);
						}},	
					    {field: 'typeEquipment',title:'Loại thiết bị',width: 80,sortable:false,resizable:false, align: 'left', formatter: function(value, row, index) {
							return Utils.XSSEncode(value);
						}},	
					    {field: 'groupEquipment',title:'Nhóm thiết bị',width: 200,sortable:false,resizable:false, align: 'left', formatter: function(value, row, index) {
					    	var html="";
							if(row != null && row.groupEquipmentCode != null && row.groupEquipmentName!= null){
								html = row.groupEquipmentCode+' - '+row.groupEquipmentName; 
							}
							return Utils.XSSEncode(html);
						}},	
					    {field: 'capacity',title:'Dung tích (lít)',width: 80,sortable:false,resizable:false, align: 'right', formatter: function(value, row, index) {
							return Utils.XSSEncode(value);
						}},	
					    {field: 'equipmentBrand',title:'Hiệu',width: 80,sortable:false,resizable:false, align: 'left', formatter: function(value, row, index) {
							return Utils.XSSEncode(value);
						}},	
					    {field: 'equipmentProvider',title:'NCC',width: 80,sortable:false,resizable:false, align: 'left', formatter: function(value, row, index) {
							return Utils.XSSEncode(value);
						}},	
					    {field: 'yearManufacture',title:'Năm sản xuất',width: 80,sortable:false,resizable:false, align: 'left', formatter: function(value, row, index) {
							return Utils.XSSEncode(value);
						}}
					]],
					onBeforeLoad:function(param){											 
					},
					onLoadSuccess :function(data){   			 
						if (data == null || data.total == 0) {
							//$("#errMsgInfo").html("Không tìm thấy danh sách biên bản giao nhận thiết bị!").show();
						}else{
							$('.datagrid-header-rownumber').html('STT');		    	
						}	    		
					}
				});
	        },
			onClose: function(){
				$('#easyuiPopupSearchEquipment').hide();
				$('#equipmentCode').val('').change();
				$('#seriNumber').val('').change();
				$('#typeEquipment').html('<option value="" selected="selected">Tất cả</option>');
				// $('#groupEquipment').html('<option value="" selected="selected">Tất cả</option>').change();
				// $('#providerId').html('<option value="" selected="selected">Tất cả</option>');
				$('#yearManufacturing').val('').change();
				$('#stockCode').val('').change();
				$('#equipmentGridDialog').datagrid("loadData",[]);
			}
		});
	},
	/**
	 * Tim kiem thiet bi
	 * @author nhutnn
	 * @since 12/12/2014
	 */
	searchEquipment: function(){
		var params=new Object(); 
		params.equipCode = $('#equipmentCode').val().trim();
		params.seriNumber = $('#seriNumber').val().trim();
		params.categoryId = $('#typeEquipment').val().trim();
		//tamvnm: thay doi thanh autoCompleteCombobox: -1 la truong hop tim group ko co trong BD
		// params.groupId = $('#groupEquipment').val().trim();
		if ($('#groupEquipment').combobox('getValue') == "") {
			$('#groupEquipment').combobox('setValue', -1);
			params.groupId = "";
		} else if ($('#groupEquipment').combobox('getValue') == -1) {
			params.groupId = "";
		} else {
			if (Utils.getMessageOfInvaildNumberNew($('#groupEquipment').combobox("getValue")).length == 0) {
				params.groupId = $('#groupEquipment').combobox("getValue");
			} else {
				params.groupId = -1;
			}
		}
		// params.providerId = $('#providerId').val().trim();
		if ($('#providerId').combobox('getValue') == "") {
			$('#providerId').combobox('setValue', -1);
			params.providerId = "";
		} else if ($('#providerId').combobox('getValue') == -1) {
			params.providerId = "";
		} else {
			if (Utils.getMessageOfInvaildNumberNew($('#providerId').combobox("getValue")).length == 0) {
				params.providerId = $('#providerId').combobox("getValue");
			} else {
				params.providerId = -1;
			}
		}
		params.yearManufacture = $('#yearManufacturing').val().trim();
		params.stockCode = $('#stockCode').val().trim();
		$('#equipmentGridDialog').datagrid('load',params);
	},
	/**
	 * Chon thiet bi fill vao cac textbox
	 * @author phuongvm
	 * @since 06/01/2015
	 */
	selectEquipment: function(index){
		var rowPopup = $('#equipmentGridDialog').datagrid('getRows')[index];
		var kho="";
		if(rowPopup.stockCode != undefined && rowPopup.stockCode != null && rowPopup.stockName != undefined && rowPopup.stockName!= null){
			kho = rowPopup.stockCode+' - '+rowPopup.stockName; 
		}
		$('#txtEquipCode').val(rowPopup.equipmentCode);
		$('#lblEquipName').html(Utils.XSSEncode(rowPopup.groupEquipmentName));
		$('#txtStock').val(kho);
		$('#txtExpiredDate').val(rowPopup.warrantyExpiredDate); /** 06/04/2015; vuongmq; ngay het han bh*/
		$('#txtRepairCount').val(rowPopup.repairCount);
		var rows = new Array();
    	RepairEquipmentManageCatalog._lstEquipItemId = new Array();
    	$('#gridDetail').datagrid('loadData',rows);
		
		$('#easyuiPopupSearchEquipment').dialog('close');
	},
	/**
	 * Lay danh muc loai, ncc thiet bi
	 * @author nhutnn
	 * @since 20/12/2014
	 */
	getEquipCatalog: function(){
		$('#typeEquipment').html('<option value="" selected="selected">Tất cả</option>');
    	$('#providerId').html('<option value=""  selected="selected">Tất cả</option>');
    	$('#groupEquipment').html('<option value="" selected="selected">Tất cả</option>'); // vuongmq; 16/06/2015; reset lai
		$.getJSON('/equipment-repair-manage/get-equipment-catalog', function(result) {				
			if(result.equipsCategory != undefined && result.equipsCategory != null && result.equipsCategory.length > 0) {					
				for(var i=0; i < result.equipsCategory.length; i++){
					$('#typeEquipment').append('<option value="'+ result.equipsCategory[i].id +'">'+ Utils.XSSEncode(result.equipsCategory[i].code) +' - ' +Utils.XSSEncode(result.equipsCategory[i].name) +'</option>');  
				}
			} 
			$('#typeEquipment').change();
			// if(result.equipProviders != undefined && result.equipProviders != null && result.equipProviders.length > 0) {					
			// 	for(var i=0; i < result.equipProviders.length; i++){
			// 		$('#providerId').append('<option value="'+ result.equipProviders[i].id +'">'+ Utils.XSSEncode(result.equipProviders[i].code) +' - '+Utils.XSSEncode(result.equipProviders[i].name)+'</option>');  
			// 	}
			// } 
			// $('#providerId').change();	

			if(result.equipProviders != undefined && result.equipProviders != null && result.equipProviders.length > 0) {					
				for (var i = 0, isize = result.equipProviders.length; i < isize; i++) {
					result.equipProviders[i].searchText = unicodeToEnglish(result.equipProviders[i].code + " " + result.equipProviders[i].name);
 					result.equipProviders[i].searchText = result.equipProviders[i].searchText.toUpperCase();
 					result.equipProviders[i].code = Utils.XSSEncode(result.equipProviders[i].code);
 					result.equipProviders[i].name = Utils.XSSEncode(result.equipProviders[i].name);
				}
				var obj = {
					id: -1,
					code: "Tất cả",
					name: "Tất cả",
					codeName: "Tất cả",
					searchText: unicodeToEnglish("Tất cả").toUpperCase()
				};
				result.equipProviders.splice(0, 0, obj);
				$('#providerId').combobox("loadData", result.equipProviders);
		 		$('#providerId').combobox("setValue", -1);
			}

			// if(result.lstEquipGroup != undefined && result.lstEquipGroup != null && result.lstEquipGroup.length > 0) {					
			// 	for(var i=0; i < result.lstEquipGroup.length; i++){
			// 		$('#groupEquipment').append('<option value="'+ result.lstEquipGroup[i].id +'">'+ Utils.XSSEncode(result.lstEquipGroup[i].code) +' - '+Utils.XSSEncode(result.lstEquipGroup[i].name)+'</option>');  
			// 	}
			// } 
			// $('#groupEquipment').change();
			if(result.lstEquipGroup != undefined && result.lstEquipGroup != null && result.lstEquipGroup.length > 0) {	
				for (var i = 0, isize = result.lstEquipGroup.length; i < isize; i++) {
					result.lstEquipGroup[i].searchText = unicodeToEnglish(result.lstEquipGroup[i].code + " " + result.lstEquipGroup[i].name);
	 				result.lstEquipGroup[i].searchText = result.lstEquipGroup[i].searchText.toUpperCase();
	 				// result.lstEquipGroup[i].code = Utils.XSSEncode(result.lstEquipGroup[i].code);
	 				// result.lstEquipGroup[i].name = Utils.XSSEncode(result.lstEquipGroup[i].name);
				}				
				var obj = {
				id: -1,
				code: "Tất cả",
				name: "Tất cả",
				codeName: "Tất cả",
				searchText: unicodeToEnglish("Tất cả").toUpperCase()
				};
				result.lstEquipGroup.splice(0, 0, obj);
				$('#groupEquipment').combobox("loadData", result.lstEquipGroup);
		 		$('#groupEquipment').combobox("setValue", -1);
			} 				
		});
	},
	/**
	 * Xu ly trên input loai thiet bi
	 * @author nhutnn
	 * @since 19/12/2014
	 */
	onChangeTypeEquip: function(cb){
		var typeEquip = $(cb).val().trim();
		$('#groupEquipment').html('<option value="" selected="selected">Tất cả</option>');
		if(typeEquip != undefined && typeEquip != null && typeEquip != ''){
			$.getJSON('/equipment-manage-delivery/get-equipment-group?id='+typeEquip, function(result) {				
				if(result.equipGroups != undefined && result.equipGroups != null && result.equipGroups.length > 0) {					
					for(var i=0; i < result.equipGroups.length; i++){
						$('#groupEquipment').append('<option value="'+ result.equipGroups[i].id +'">'+ Utils.XSSEncode(result.equipGroups[i].code) + ' - '+Utils.XSSEncode(result.equipGroups[i].name)+'</option>');  
					}
				} 
				$('#groupEquipment').change();				
			});
		}
	},

	/**
	 * bind su kien khi thay doi text box ngay bat dau bao hanh, hien thi so
	 * @author phuongvm
	 * @since 06/01/2015
	 */
	changeNgayBatDauBaoHanh: function(index, id){
		if (id != null && id != RepairEquipmentManageCatalog._valueTruMot) {
			var ngayBatDauBaoHanh = $('#warantyDateInGrid_'+id).val();
			if (ngayBatDauBaoHanh != null && ngayBatDauBaoHanh != '') {
				var data = $('#gridDetail').datagrid('getRows')[index];
				if (data != null) {
					var soThangBaoHanhMacDinh = 0;
					if (data.dataView != undefined && data.dataView != null) {
						soThangBaoHanhMacDinh = data.dataView.warranty;
						$('#warantyInGrid_'+id).val(soThangBaoHanhMacDinh);
					}
					var ngayGiaHan = soThangBaoHanhMacDinh*value30NgayInfoJS;
					var ngayHetHanBaoHanh = getNextDateForNumber(ngayBatDauBaoHanh, ngayGiaHan);
					if(ngayHetHanBaoHanh != undefined && ngayHetHanBaoHanh != null){
						$('#warantyExpiredDateInGrid_'+id).val(ngayHetHanBaoHanh);	
					}
				}
			}
		}
	},
	/**
	 * bind su kien khi thay doi text box gridDetail chi tiet han muc
	 * @author phuongvm
	 * @since 06/01/2015
	 */

	 /**
	 * cap nhat; bind su kien khi thay doi text box gridDetail chi tiet han muc
	 * @author vuongmq
	 * @since 14/04/2015
	 */
	bindEventChangeText: function(){
		/** chon so thang bao hanh */
		$('.warantyInGridClzz').blur(function(){
			var idTmp = $(this).attr('id');
			var id = idTmp.split('_')[1];
			if (id != undefined && id != null && id != '') {
				var ngayBatDauBaoHanh = $('#warantyDateInGrid_'+id).val();
				if (ngayBatDauBaoHanh != undefined && ngayBatDauBaoHanh != null && Utils.isDate(ngayBatDauBaoHanh)) {
					var ngay = 0;
					if ($(this).val() != undefined && $(this).val() != null) {
						ngay = $(this).val();
					}
					var ngayGiaHan = ngay*value30NgayInfoJS;
					var ngayHetHanBaoHanh = getNextDateForNumber(ngayBatDauBaoHanh, ngayGiaHan);
					if(ngayHetHanBaoHanh != undefined && ngayHetHanBaoHanh != null){
						$('#warantyExpiredDateInGrid_'+id).val(ngayHetHanBaoHanh);	
					}
				}
			}
		});
		/** chon don gia vat tu; lay tong vat tu detail */
		$('.materialPriceInGridClzz').blur(function(){
			$('.ErrorMsgStyle').html('').hide();
			var idTmp = $(this).attr('id');
			var id = idTmp.split('_')[1];
			if (id != undefined && id != null && id != '') {
				RepairEquipmentManageCatalog.tongVatTuDetail(id);
			}
		});
		/** chon don gia nhan cong; lay max gia nhan cong */
		$('.wokerPriceInGridClzz').blur(function(){
			$('.ErrorMsgStyle').html('').hide();
			RepairEquipmentManageCatalog.maxWorkerPriceChangeTextDetail();
		});
		/** chon so luong*/
		$('.quantityInGridClzz').blur(function () {
		    var idQuan = $(this).attr('id');
		    var soLuong = $(this).val();
		    var id = idQuan.split('_')[1];
		    if (id != undefined && id != null && id != '') {
			    var donGiaVT = $('#materialPriceInGrid_'+id).val();
			    var donGiaNC = $('#wokerPriceInGrid_'+id).val();
			    if (donGiaVT==null) {
			      donGiaVT = 0;
			    }
			    if (donGiaNC==null) {
			      donGiaNC = 0;
			    }		
			    //var tong = soLuong*donGiaVT+Number(donGiaNC);
			    var tong = soLuong*donGiaVT;
			    tong = formatCurrency(tong);
			    $('#tongTien_'+id).val(tong);
			    RepairEquipmentManageCatalog.totalAmountChangeText();
			}
		});
		$('.vinput-money').each(function(){
			var objectId = $(this).attr('id');
			if (objectId!=null && objectId!=undefined && objectId.length>0) {
				Utils.formatCurrencyFor(objectId);
			}			
		});
		Utils.bindFormatOnTextfieldInputCss('vinput-money',Utils._TF_NUMBER);
//		Utils.bindFormatOnTextfieldInputCss('warantyInGridClzz',Utils._TF_NUMBER);
//		Utils.bindFormatOnTextfieldInputCss('materialPriceInGridClzz',Utils._TF_NUMBER);
//		Utils.bindFormatOnTextfieldInputCss('wokerPriceInGridClzz',Utils._TF_NUMBER);
//		Utils.bindFormatOnTextfieldInputCss('quantityInGridClzz',Utils._TF_NUMBER);
	},
	totalAmountChangeText: function(){
		var tongCong = 0; 
		var row = $('#gridDetail').datagrid('getRows');
		var i = 1;
	    $('.tongTienClzz').each( function() {
	    	if (!RepairEquipmentManageCatalog._flagPagePheDuyet) {
	    		// khong phai trang phe duyet; 
				if (i != row.length) {
					var value = $(this).attr('value');
					value = Utils.returnMoneyValue(value);
					if(!isNaN(value)){
						tongCong +=Number(value);
					}
				}
			} else {
				var value = $(this).attr('value');
				value = Utils.returnMoneyValue(value);
				if(!isNaN(value)){
					tongCong +=Number(value);
				}
			}
			i++;
		});
		var nhanCong = $('#txtWorkerPrice').val();
		var value = Utils.returnMoneyValue(nhanCong);
		if (!isNaN(value)) {
			tongCong += Number(value);
		}
		/** tongCong += gia nhan cong*/
	    $('#txtTotalAmount').val(formatCurrency(tongCong));
	},
	tongVatTuDetail: function(id) {
	    var soLuong = $('#quantityInGrid_'+id).val();
	    if (soLuong==null) {
	    	soLuong = 0;
	    }
	    var donGiaVT = $('#materialPriceInGrid_'+id).val();
	    if (donGiaVT==null) {
	      donGiaVT = 0;
	    }
	    var tong = soLuong*donGiaVT;
	    tong = formatCurrency(tong);
	    $('#tongTien_'+id).val(tong);
	    RepairEquipmentManageCatalog.totalAmountChangeText();
	},
	/**
	 * bind su kien khi thay doi text box don gia nhan cong
	 * lay don gia nhan cong max -> Gia nhan cong (txtWorkerPrice)
	 * @author vuongmq
	 * @since 14/04/2015
	 */
	 maxWorkerPriceChangeTextDetail: function(isCheck) {
		var row = $('#gridDetail').datagrid('getRows');
		var i = 1;
		var maxNhanCong = 0;
	    $('.wokerPriceInGridClzz').each( function() {
	    	if (!RepairEquipmentManageCatalog._flagPagePheDuyet) {
		    	// khong phai trang phe duyet; 
				if (i != row.length) {
					var value = $(this).val();
					value = Utils.returnMoneyValue(value);
					if (!isNaN(value)) {
						if (parseInt(value) > parseInt(maxNhanCong)) {
							maxNhanCong = value;
						}
					}
				}
			} else {
				var value = $(this).val();
				value = Utils.returnMoneyValue(value);
				if (!isNaN(value)) {
					if (parseInt(value) > parseInt(maxNhanCong)) {
						maxNhanCong = value;
					}
				}
			}
			i++;
		});
		RepairEquipmentManageCatalog._maxWorkerPriceDetail = maxNhanCong;
		if (isCheck == undefined || isCheck == null) {
			// truong hop nhap gia tri duoi gridDetail don gia nhan cong thi gan gia tri
	    	$('#txtWorkerPrice').val(formatCurrency(maxNhanCong));
	    }
	    RepairEquipmentManageCatalog.totalAmountChangeText();
	},

	/**
	 * Xoa mot dong hang muc
	 * 
	 * @author phuongvm
	 * @since 06/01/2015
	 * */
	deleteRow: function (index) {
		var row = $('#gridDetail').datagrid('getRows')[index];
		if(row.id!=undefined){
			$('#gridDetail').datagrid("deleteRow", index);
			RepairEquipmentManageCatalog.totalAmountChangeText();
			for(var i = 0;i<RepairEquipmentManageCatalog._lstEquipItemId.length;i++){
				if(RepairEquipmentManageCatalog._lstEquipItemId[i] == row.id){
					RepairEquipmentManageCatalog._lstEquipItemId.splice(i,1);
					break;
				}
			}
			RepairEquipmentManageCatalog.reloadGridDetail();
		}
	},

	/**
	 * thuc hien load lai danh sach khi add hoac delete row
	 * 
	 * @author phuongvm
	 * @since 09/01/2015
	 * */
	reloadGridDetail:function(){
		var rows = new Array();
		$('.quantityInGridClzz').each(function () {
			var row = new Object();
			var idQuan = $(this).attr('id');
		    var id = idQuan.split('_')[1];
		    if(id!=undefined && id != null && id!= ''){
		    	row.id = id;
		    	row.hangMuc = $('#hangMuc_'+id).html();
		    	row.ngayBatDauBaoHanh = $('#warantyDateInGrid_'+id).val(); //
		    	setDateTimePicker('warantyDateInGrid_'+id);
		    	row.baoHanh = $('#warantyInGrid_'+id).val();
		    	row.ngayHetHanBaoHanh = $('#warantyExpiredDateInGrid_'+id).val();//
		    	setDateTimePicker('warantyExpiredDateInGrid_'+id);
		    	row.donGiaVatTuDinhMuc = $('#materialPriceDefaultInGrid_'+id).html(); //
		    	row.donGiaVatTu = $('#materialPriceInGrid_'+id).val();
		    	row.donGiaNhanCongDinhMuc = $('#wokerPriceDefaultInGrid_'+id).html(); //
			    row.donGiaNhanCong = $('#wokerPriceInGrid_'+id).val();
			    row.soLuong = $('#quantityInGrid_'+id).val();
			    row.description = $('#descriptionInGrid_'+id).val(); // 01/07/2015; them ghi chu vuot dinh muc
			    row.lanSua = $('#lanSua_'+id).val();
			    row.tongTien = $('#tongTien_'+id).val();
			    /** lay dataview gan lai*/
			    var index = $('#materialPriceInGrid_'+id).attr('gridIndex');
			    if (index != undefined && index != null) {
			    	var data = $('#gridDetail').datagrid ('getRows')[index];
			    	if (data != undefined && data != null) {
			    		row.dataView = data.dataView; /** lay record cua dataView gan lai*/
			    	}
			    }
			    rows.push(row);
		    }
		});
		$('#gridDetail').datagrid("loadData", rows);
	},
	showDlgImportExcel: function(){
		$('#easyuiPopupImportExcel').show();
		$('.ErrorMsgStyle').hide();
		$('#easyuiPopupImportExcel').dialog({  
	        closed: false,  
	        cache: false,  
	        modal: true,
	        onOpen: function(){	 
	        	$('.easyui-dialog .BtnGeneralStyle').css('color','');
	        },
	        onClose: function(){
	        	EquipmentListEquipment.searchListEquipment();
				$("#grid").datagrid('uncheckAll');
				$("#fakefilepcBBGN").val("");
				$("#excelFileBBGN").val("");				
	        }
		});
	},
	/**
	 * Xoa tap tin dinh kem
	 * 
	 * @author hunglm16
	 * @since Feb 10,214
	 * */
	removeEquipAttachFile: function(id){
		if (id == undefined || id == null || isNaN(id)) {
			return;
		}
		if (RepairEquipmentManageCatalog._arrFileDelete == null || RepairEquipmentManageCatalog._arrFileDelete == undefined) {
			RepairEquipmentManageCatalog._arrFileDelete = [];
		}
		RepairEquipmentManageCatalog._arrFileDelete.push(id);
		$('#divEquipAttachFile'+id).remove();
		if (RepairEquipmentManageCatalog._countFile != undefined && RepairEquipmentManageCatalog._countFile != null && RepairEquipmentManageCatalog._countFile > 0) {
			RepairEquipmentManageCatalog._countFile = RepairEquipmentManageCatalog._countFile - 1;
		} else {
			RepairEquipmentManageCatalog._countFile = 0;
		}
	},
	/**
	 * Lap bien ban sua chua
	 * 
	 * @author phuongvm
	 * @Since 07/01/2015
	 * */
	saveRepair: function(pheDuyet) {
		$(".ErrorMsgStyle").html('').hide();
		var msg = "";
		var err = ""; 
		var lstEquipItemId = new Array();
		var lstNgayBatDauBaoHanh = new Array();  // vuongmq 14/04/2015
		var lstNgayHetHanBaoHanh = new Array(); // vuongmq 14/04/2015
		var lstBaoHanh = new Array();
		var lstDonGiaVT = new Array();
		var lstDonGiaNC = new Array();
		var lstSoLuong = new Array();
		var lstDescription = new Array(); // vuongmq; 01/07/2015; them ghi chu vuot dinh muc
		var lstSoLanSuaChua = new Array();
		var lstTongTienDetail = new Array(); // vuongmq 14/04/2015

		var lstDonGiaVTDinhMucTu = new Array(); // vuongmq 24/04/2015
		var lstDonGiaVTDinhMucDen = new Array(); // vuongmq 24/04/2015
		var lstDonGiaNCDinhMucTu = new Array(); // vuongmq 24/04/2015
		var lstDonGiaNCDinhMucDen = new Array(); // vuongmq 24/04/2015
		
		
		msg = Utils.getMessageOfRequireCheck('txtEquipCode','Mã thiết bị');
		err = '#txtEquipCode';
		if (msg.length == 0) {
			msg = Utils.getMessageOfSpecialCharactersValidate('txtEquipCode', 'Mã thiết bị', Utils._CODE);
			err = '#txtEquipCode';
		}
		if (msg.length == 0) {
			msg = Utils.getMessageOfRequireCheck('txtRepairCount', 'Sửa lần thứ');
			err = '#txtEquipCode';
			if (msg.trim().length > 0) {
				$('#errMsgEquipDetail').html('Vui lòng nhập Mã thiết bị hợp lệ (nhấn F9)').show();
				$(err).focus();
				return;
			}
		}

		if(msg.length == 0 && $('#createDate').val().trim().length > 0 && !Utils.isDate($('#createDate').val()) && $('#createDate').val() != '__/__/____' ){
			msg = 'Ngày biên bản không tồn tại hoặc không đúng định dạng dd/mm/yyyy.';
			err = '#createDate';
		}

		var curDate = ReportUtils.getCurrentDateString(); //Lay ngay hien tai sysDate
		if (msg.length == 0 && !Utils.compareDate($('#createDate').val().trim(), curDate)) {
			msg = 'Ngày biên bản không được sau ngày hiện tại. Vui lòng nhập lại';
			$('#createDate').focus();
		}
		if (msg.length == 0 && $('#note').attr('maxlength') > 500) {
			msg = 'Bạn nhập quá giới hạn của trường note';
		}
		
		if (msg.length == 0) {
			if (RepairEquipmentManageCatalog._msgGiaNhanCongMax != null && RepairEquipmentManageCatalog._msgGiaNhanCongMax.length > 0) {
				msg = RepairEquipmentManageCatalog._msgGiaNhanCongMax;
				err = RepairEquipmentManageCatalog._msgGiaNhanCongMaxFocus;
			}
		}
		var rows = $('#gridDetail').datagrid('getRows');
		var length = rows.length;
		if(pheDuyet !=undefined && pheDuyet != null && pheDuyet == RepairEquipmentManageCatalog._PHEDUYET){ // neu man hinh phe duyet link qua thi phai + them 1 dong
			length+=1;
		}
		if (msg.length == 0) {
			if (length == 1) {
				//msg = "Chưa có hạng mục nào được chọn";
				msg = "Vui lòng Chọn hạng mục bằng F9";
			} else {
				$('.quantityInGridClzz').each(function () {
					var idQuan = $(this).attr('id');
				    var id = idQuan.split('_')[1];
				    if (id != undefined && id != null && id != '') {
				    	var ngayBatDauBaoHanh = $('#warantyDateInGrid_'+id).val().trim();
				    	var ngayHetHanBaoHanh = $('#warantyExpiredDateInGrid_'+id).val().trim();
				    	var soLuong = Utils.returnMoneyValue($('#quantityInGrid_'+id).val());
					    var donGiaVT = Utils.returnMoneyValue($('#materialPriceInGrid_'+id).val());
					    var donGiaNC = Utils.returnMoneyValue($('#wokerPriceInGrid_'+id).val());
					    var baoHanh = Utils.returnMoneyValue($('#warantyInGrid_'+id).val());
					    var lanSua = Utils.returnMoneyValue($('#lanSua_'+id).val());
					    var tongtienct = Utils.returnMoneyValue($('#tongTien_'+id).val());
					    var description = $('#descriptionInGrid_'+id).val(); // vuongmq; 01/07/2015; them ghi chu vuot dinh muc; no trim(0)
					    /** vuongmq; 24/04/2015; lay them don gia vat tu, nhan cong, tu va den theo dinh muc*/	
					    var donGiaVTDMTu = '';				    
					    var donGiaVTDMDen = '';
					    var donGiaNCDMTu = '';
					    var donGiaNCDMDen = '';
					    var index = $(this).attr('gridIndex');
						if (index != undefined && index > RepairEquipmentManageCatalog._valueTruMot) {
							var row = $('#gridDetail').datagrid('getRows')[index];
							if (row != undefined && row != null) {
								var data = row.dataView;
								if (data != undefined && data != null) {
									if (data.fromMaterialPrice != null) {
										donGiaVTDMTu = data.fromMaterialPrice;
									}
									if (data.toMaterialPrice != null) {
										donGiaVTDMDen = data.toMaterialPrice;
									}
									if (data.fromWorkerPrice != null) {
										donGiaNCDMTu = data.fromWorkerPrice;
									}
									if (data.toWorkerPrice != null) {
										donGiaNCDMDen = data.toWorkerPrice;
									}
								}
							}
						}
						if (ngayBatDauBaoHanh != null && ngayBatDauBaoHanh != '') {
							/** fix: 220117; vuongmq; 19/05/2015 kiem tra so thang bao hanh khi nhap ngay bat dau bao hanh*/
							if (baoHanh =='' || baoHanh =='0') {
								msg  = 'Vui lòng nhập Số tháng bảo hành lớn hơn 0';
						    	err = '#warantyInGrid_'+id;
						    	$('#errMsgEquipDetail').html(msg).show();
								$(err).focus();
								return false;
							}
						}
//					    if ((donGiaNC== ''|| donGiaNC== '0' ) && (donGiaVT== ''|| donGiaVT== '0' )) {
//					    	msg  = 'Vui lòng nhập đơn giá vật tư hoặc đơn giá nhân công lớn hơn 0';
//					    	err = '#materialPriceInGrid_'+id;
//					    	$('#errMsgEquipDetail').html(msg).show();
//							$(err).focus();
//							return false;
//					    }
					    //if ((donGiaVT== ''|| donGiaVT== '0' )&& soLuong!='') {
						if (donGiaVT == '0' && donGiaNC!= '' && donGiaNC!= '0') {
						    	msg  = 'Vui lòng nhập đơn giá vật tư lớn hơn 0';
						    	err = '#materialPriceInGrid_'+id;
						    	$('#errMsgEquipDetail').html(msg).show();
								$(err).focus();
								return false;
						}	
						if (donGiaVT != '' && donGiaVT != '0' && donGiaNC== '0') {
					    	msg  = 'Vui lòng nhập đơn giá nhân công lớn hơn 0';
					    	err = '#wokerPriceInGrid_'+id;
					    	$('#errMsgEquipDetail').html(msg).show();
							$(err).focus();
							return false;
						}						
					    if ((donGiaVT== ''|| donGiaVT== '0') && (donGiaNC== ''|| donGiaNC== '0')) {
					    	msg  = 'Vui lòng nhập đơn giá vật tư hoặc đơn giá nhân công lớn hơn 0';
					    	if(donGiaVT== ''|| donGiaVT== '0'){
					    		err = '#materialPriceInGrid_'+id;
					    	} else {
					    		err = '#wokerPriceInGrid_'+id;
					    	}					    	
					    	$('#errMsgEquipDetail').html(msg).show();
							$(err).focus();
							return false;
					    }
					    
						if ((donGiaVT != '' && donGiaVT != '0') && (soLuong == '' || soLuong == '0')) {
							msg = 'Vui lòng nhập số lượng lớn hơn 0';
							err = '#quantityInGrid_' + id;
							$('#errMsgEquipDetail').html(msg).show();
							$(err).focus();
							return false;
						}
					    // vuongmq; 01/07/2015; bat buoc phai nhap so luong
					  //   if(soLuong== ''|| soLuong== '0') {
					  //   	msg  = 'Vui lòng nhập số lượng lớn hơn 0';
					  //   	err = '#quantityInGrid_'+id;
					  //   	$('#errMsgEquipDetail').html(msg).show();
							// $(err).focus();
							// return false;
					  //   }
						if (donGiaVT == null) {
							donGiaVT = "";
						}
						if (donGiaNC == null) {
							donGiaNC = "";
						}
					    if (baoHanh==null) {
					    	baoHanh = "";
					    }
					    if (lanSua==null) {
					    	lanSua = "";
					    }
					    if (tongtienct == null) {
					    	tongtienct = "";
					    }
					    lstEquipItemId.push(id);
					    lstNgayBatDauBaoHanh.push(ngayBatDauBaoHanh); //
					    lstNgayHetHanBaoHanh.push(ngayHetHanBaoHanh); //
					    lstBaoHanh.push(baoHanh);
					    lstDonGiaVT.push(donGiaVT);
					    lstDonGiaNC.push(donGiaNC);
					    lstSoLuong.push(soLuong);
					    lstSoLanSuaChua.push(lanSua);
					    lstTongTienDetail.push(tongtienct); // tong tien dong chi tiet
					    lstDonGiaVTDinhMucTu.push(donGiaVTDMTu); //
					    lstDonGiaVTDinhMucDen.push(donGiaVTDMDen); //
					    lstDonGiaNCDinhMucTu.push(donGiaNCDMTu); //
					    lstDonGiaNCDinhMucDen.push(donGiaNCDMDen); //
					    lstDescription.push(description); // vuongmq; 01/07/2015; them ghi chu vuot dinh muc
				    }
				});
			}
		}
		if (msg.trim().length > 0) {
			$('#errMsgEquipDetail').html(msg).show();
			$(err).focus();
			return false;
		}
		var params = new Object();
		params.equipCode = $('#txtEquipCode').val().trim(); // ma thiet bi
		params.status = $('#cbxStatus').val().trim(); // trang thai
		params.expiredDate = $('#txtExpiredDate').val().trim() // ngay het han bao hanh
		params.workerPrice = $('#txtWorkerPrice').val().trim(); // giá nhan cong
		params.totalAmount = Utils.returnMoneyValue($('#txtTotalAmount').val().trim()); // tong tien
		params.repairCountForm = $('#txtRepairCount').val().trim(); // sua lan thu
		params.condition = $('#txtCondition').val().trim(); // tinh trang hu hong
		params.reason = $('#txtReason').val().trim(); // ly do de nghi
		params.createDate = $('#createDate').val();
		params.note = $('#note').val();
		if($('#txtRejectReason').val()!=undefined && $('#txtRejectReason').val()!= null){
			params.rejectReason = $("#txtRejectReason").val();
		}
		
		params.lstEquipItemId = lstEquipItemId;
		params.lstNgayBatDauBaoHanh = lstNgayBatDauBaoHanh; //
		params.lstNgayHetHanBaoHanh = lstNgayHetHanBaoHanh; //
		params.lstBaoHanh = lstBaoHanh;
		params.lstDonGiaVT = lstDonGiaVT;
		params.lstDonGiaNC = lstDonGiaNC;
		params.lstSoLuong = lstSoLuong;
		params.lstSoLanSuaChua = lstSoLanSuaChua;
		params.lstTongTienDetail = lstTongTienDetail; //
		params.lstDonGiaVTDinhMucTu = lstDonGiaVTDinhMucTu; // vuongmq; 24/04/2015
		params.lstDonGiaVTDinhMucDen = lstDonGiaVTDinhMucDen; // vuongmq; 24/04/2015
		params.lstDonGiaNCDinhMucTu = lstDonGiaNCDinhMucTu; // vuongmq; 24/04/2015
		params.lstDonGiaNCDinhMucDen = lstDonGiaNCDinhMucDen; // vuongmq; 24/04/2015
		params.lstDescription = lstDescription; // vuongmq; 01/07/2015; them ghi chu vuot dinh muc
		var idForm = $("#idRecordHidden").val();
		if(idForm!=null && idForm != ''){
			params.idForm = idForm;
		}
		if (RepairEquipmentManageCatalog._arrFileDelete == undefined || RepairEquipmentManageCatalog._arrFileDelete == null) {
			RepairEquipmentManageCatalog._arrFileDelete = [];
		}
		params.equipAttachFileStr = RepairEquipmentManageCatalog._arrFileDelete.join(',');
		var msgDialog = "Bạn có muốn lưu phiếu sửa chữa?";
		var href = '/equipment-repair-manage/info';
		if(pheDuyet !=undefined && pheDuyet != null && pheDuyet == RepairEquipmentManageCatalog._PHEDUYET){
			href = '/equipment-repair-manage/approved/info';
		}
		if (DropzonePlugin._dropzoneObject != null) {
			var arrFile = DropzonePlugin._dropzoneObject.getFilesWithStatus(Dropzone.ADDED);
			if (arrFile != null && arrFile != undefined && arrFile.length > 0) {
				if (RepairEquipmentManageCatalog._countFile == 5) {
					$('#errMsgEquipDetail').html("Tổng số tập tin đính kèm không được vượt quá 5").show();
					return false;
				}
				var maxLength = 5 - RepairEquipmentManageCatalog._countFile;
				if (arrFile.length > maxLength) {
					$('#errMsgEquipDetail').html("Số tập tin thêm mới không được vượt quá " + maxLength).show();
					return false;
				}
				msg = UploadUtil.getMsgByFileRequireCheck(UploadUtil._typeFileRequireCheck.equipRecord);
				if (msg.trim().length > 0) {
					$('#errMsgEquipDetail').html(msg).show();
					return false;
				}
				$.messager.confirm('Xác nhận', msgDialog, function(r){
					if (r){
						var data = JSONUtil.getSimpleObject2(params);
						UploadUtil.updateAdditionalDataForUpload(data);
						UploadUtil.startUpload('errMsgEquipDetail');
						return false;
					}
				});
				return false;
			}
		}
		Utils.addOrSaveData(params, "/equipment-repair-manage/saveRepair", null, 'errMsgEquipDetail', function(data) {
			if (data.error != undefined && !data.error) {
				//$("#successMsg").html("Lưu dữ liệu thành công").show();
				$("#successMsgEquipDetail").html("Lưu dữ liệu thành công").show();
				setTimeout(function(){
					window.location.href = href;
				}, 1500);				
			}else{
				if(data.errMsg!=undefined && data.errMsg!= null && data.errMsg!=''){
					$('#errMsgEquipDetail').html(data.errMsg).show();
				}
			}			
		}, null, null, null, msgDialog, function(data) {
			if (data.error != undefined && data.error) {
				$('#errMsgEquipDetail').html(data.errMsg).show();
			}		
		});
		return false;
	},
	/**
	 * Chon kho
	 * @author phuongvm
	 * @since 22/12/2014
	 */
	selectStock: function(shortCode){
		$('#warehouse').val(shortCode);		
		$('#easyuiPopupSearchStock').dialog('close');
	},
	exportExcel : function(notificationId){
		$('#errMsg').html('').hide();
		$.messager.confirm('Xác nhận','Bạn có muốn xuất file?',function(r){
			if(r){	
				var dataModel = new Object();
				dataModel.notificationId = notificationId;
				ReportUtils.exportReport('/adsprogram/notification/exportExcel',dataModel,'errMsg');									
				}			
		});	
 		return false;

	},

	/**
	 * In phieu Quan ly sua chua
	 * @author vuongmq
	 * @since 22/04/2015
	 * */
	printRepair: function() {
		$('.SuccessMsgStyle').html('').hide();
		$('.ErrorMsgStyle').html('').hide();
		var dataModel = new Object();
		var arra = RepairEquipmentManageCatalog._mapRepair.valArray;		
		var lstId = new Array();
		if (arra == null || arra.length == 0) {
			$('#errMsg').html('Bạn chưa chọn phiếu sửa chữa để in. Vui lòng chọn phiếu').show();
			return;
		}		
		for (var i=0, size = arra.length; i<size; ++i) {
			lstId.push(arra[i].id);			
		}
		dataModel.lstId = lstId; /** danh sach phieu sua chua de lay danh sach phieu sua chua*/
		$.messager.confirm('Xác nhận','Bạn có muốn in phiếu sửa chữa?',function(r){
			if(r){				
				ReportUtils.exportReport('/equipment-repair-manage/print-Repair', dataModel);
			}
		});	
		
	},
	/**
	 * Mo Dialog Nhap Excel
	 * 
	 * @author phuongvm
	 * @since 14/01/2015
	 * */

	 /**
	 * Mo Dialog Nhap Excel Quan ly sua chua
	 * @author vuongmq
	 * @since 20/04/2015
	 * */
	openDialogImportRepair: function (){
		$('.SuccessMsgStyle').html('').hide();
		$('.ErrorMsgStyle').html('').hide();
		$('#equipHistoryTemplateDiv').css("visibility","hidden").hide();
		$('#easyuiPopupImportExcel').dialog({
			title: 'Nhập Excel Quản lý sửa chữa',
			width: 465, 
			height: 'auto',
			closed: false,
			cache: false,
			modal: true,
			onOpen: function() {
				
			},
			onClose: function() {
				$('.ErrorMsgStyle').html('').hide();
				$("#grid").datagrid('uncheckAll');
				$("#fakefilepcListEquip").val("");
				$("#excelFileListEquip").val("");
			}
		});
	},
	/**
	 * Import Excel phieu sua chua
	 * 
	 * @author phuongvm
	 * @since 14/01/2015
	 * */

	 /**
	 * Import Excel phieu sua chua;
	 * cap nhat khong dong popup; khong thong bao luu du lieu thanh cong
	 * @author vuongmq
	 * @since 21/04/2015
	 * */
	importExcelRepair: function(){
		$('.SuccessMsgStyle').html('').hide();
		$('.ErrorMsgStyle').html('').hide();
		if($('#fakefilepcListEquip').val().length <= 0 ){
			$('#errExcelMsgListEquip').html('Vui lòng chọn tập tin Excel').show();
			return false;
		}
		Utils.importExcelUtils(function(data){
			RepairEquipmentManageCatalog.searchRepair();
			$('#fakefilepcListEquip').val("").change();
			if (data.message != undefined && data.message != null && data.message.trim().length > 0) {
				$('#errExcelMsgListEquip').html(data.message.trim()).change().show();
			}else{
				/*$('#successExcelMsgListEquip').html("Lưu dữ liệu thành công").show();
				var tm = setTimeout(function(){
					$('.SuccessMsgStyle').html('').hide();
					clearTimeout(tm);
					$('#easyuiPopupImportExcel').dialog('close');
				 }, 1500);*/
			}
		}, 'importFrmListEquip', 'excelFileListEquip', 'errExcelMsgListEquip');
		
	},
	upload : function(){ //upload trong fancybox
		$('#resultExcelMsg').html('').hide();
		$('#errExcelMsg').html('').hide();
		var msg="";
		var err="";
		var fDate = $('#fromDateDl').val().trim();	
		var content = $('#contentTxtDl').val().trim();
		var description = $('#descriptionDl').val().trim();
		if(msg.length == 0){
			msg = Utils.getMessageOfRequireCheck('contentTxtDl','Nội dung');
			err = '#contentTxtDl';
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfRequireCheck('descriptionDl','Mô tả');
			err = '#descriptionDl';
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfRequireCheck('fromDateDl','Ngày');
			err = '#fromDateDl';
		}
		if(msg.length == 0 && $('#fromDateDl').val().trim().length>0 && !Utils.isDate($('#fromDateDl').val(),'/')){
			msg = 'Ngày không tồn tại hoặc không đúng định dạng dd/mm/yyyy.';
			err = '#fromDateDl';
		}
		if(msg.length == 0) {
			msg = Utils.compareCurrentDateEx2('fromDateDl','Ngày');
			if(msg.length !=0){
				err = '#fromDateDl';
			}
		}
		if(msg.length>0){
			$('#errExcelMsg').html(msg).show();
			$(err).focus();
			setTimeout(function(){$('#errMsg').hide();},3000);
			return false;
		}
		if($('#excelFile').val() ==  ""){
			$('#errExcelMsg').html("Vui lòng chọn file Excel").show();
			return false;
		}
		var isLogin= 0;
		var isUploadDB= 0;
		if($('#checkLogin').is(':checked')){
			isLogin =1;
		}
		if($('#checkUploadDB').is(':checked')){
			isUploadDB =1;
		}
		var fH= Number($('#fromHour').val().trim()) *60 + Number($('#fromMinute').val().trim());
		var tH= Number($('#toHour').val().trim()) *60 + Number($('#toMinute').val().trim());
		if(fH>tH){
			$('#errExcelMsg').html("Từ giờ phải nhỏ hơn hoặc bằng đến giờ.").show();
			return false;
		}
		var fromH = $('#fromHour').val().trim()+':'+$('#fromMinute').val().trim();
		var toH = $('#toHour').val().trim()+':'+$('#toMinute').val().trim();
		fDate = $('#fromDateDl').val().trim();	
		var options = { 
				beforeSubmit: ReportUtils.beforeImportExcel,   
		 		success:      RepairEquipmentManageCatalog.afterImportExcel, 
		 		type: "POST",
		 		dataType: 'html',
		 		data:({isUploadDB:isUploadDB,isLogin:isLogin,fromDate:fDate,content:content,description:description,fromH:fromH,toH:toH})
		 	}; 
		$('#easyuiPopup #importFrm').ajaxForm(options);
		$.messager.confirm('Xác nhận','Bạn có muốn nhập từ file?',function(r){
			if(r){				
				$('#easyuiPopup #importFrm').submit();
			}
		});		
 		return false;
	},
	
	/**
	 * khoi tao grid danh sach cac hang muc sua chua
	 * @author tuannd20
	 * @param long equipmentRepairFormId id cua phieu sua chua
	 * @param viewActionType thao tac tren trang: CREATE, EDIT, VIEW
	 * @return void
	 */
	initializeRepairItemsGrid: function(equipmentRepairFormId, viewActionType) {
		$('#gridRepairItem').datagrid({
			url : "/equipment-repair-manage/searchRepairDetail",
			//pagination : true,
			rownumbers : true,
			pageNumber : 1,
			scrollbarSize: 0,
			autoWidth: true,
			//pageList: [10],
			autoRowHeight : true,
			fitColumns : true,
			singleSelect: true,
			queryParams: {equipRepairId: equipmentRepairFormId},
			width: $('#gridRepairItemContainer').width() - 15,
			columns: [[
				{field: 'equipItemName', title: 'Hạng mục', width: 200, sortable: false, resizable: false, align: 'left', formatter: function(value, row, index){
					var hangmuc = "";
					if(row.equipItem != null) {
						hangmuc = row.equipItem.name;
					}
					return Utils.XSSEncode(hangmuc);
				}},
				/*{field: 'warranty', title: 'Bảo hành (tháng)', width: 80, align: 'right', sortable: false, resizable: false, formatter: function(value, row, index){
					if (value != undefined && value != null) {
						return formatCurrency(value);
					}
					return '';
				}},*/
				{field: 'warrantyExpireDate', title: 'Ngày hết hạn bảo hành', width: 80, align: 'right', sortable: false, resizable: false, formatter: function(value, row, index){
					if (value != undefined && value != null) {
						return $.datepicker.formatDate('dd/mm/yy', new Date(value.substr(0,10)));
					}
					return '';
				}},
				{field: 'materialPrice', title: 'Đơn giá vật tư', width: 90, align: 'right', sortable: false, resizable: false, formatter: function(value, row, index){
					if (value != undefined && value != null) {
						return formatCurrency(value);
					}
					return '';
				}},
				/*{field: 'workerPrice', title: 'Đơn giá nhân công', width: 90, align: 'right',sortable: false, resizable: false, formatter: function(value, row, index){
					if (value != undefined && value != null) {
						return formatCurrency(value);
					}
					return '';
				}},*/
				{field: 'quantity', title: 'Số lượng', width:60, sortable: false, resizable: false , align: 'right', formatter: function(value, row, index){
					if (value != undefined && value != null) {
						return formatCurrency(value);
					}
					return '';
				}},
				{field: 'repairCount', title: 'Lần sửa', width: 50, sortable: false, resizable: false , align: 'right', formatter: function(value, row, index){
					return isNaN(value) ? Utils.XSSEncode(value) : value;
				}},
				{field: 'totalAmount', title: 'Tổng tiền (VNĐ)', width: 100, sortable: false, resizable: false, align: 'right', formatter: function(value, row, index) {
					if (value != undefined && value != null) {
						return formatCurrency(value);
					}
					return '';
				}},
				/*{field: 'totalActualAmount', title: 'Tổng tiền thực tế (VNĐ)', width: 120, sortable: false, resizable: false , align: 'right',
					formatter: function(value, row, index) {
						if (viewActionType === ViewActionType.CREATE || viewActionType === ViewActionType.EDIT) {
							return '<input id="totalActualAmount_' + row.id + '" class="InputTextStyle" type="text" style="margin-bottom: 2px; width: 96%; text-align: right;" value="' + (value ? formatCurrency(Utils.XSSEncode(value)) : 0) + '"/>';
						}
						return value ? formatCurrency(Utils.XSSEncode(value)) : 0;
					}
				},*/
			]],
			onLoadSuccess:function(data){
				$('.datagrid-header-rownumber').html('STT');
				//Utils.functionAccessFillControl('equipHistoryDg');
				if (viewActionType === ViewActionType.CREATE || viewActionType === ViewActionType.EDIT) {
					if (data && data.rows && data.rows.length) {
						/*var $grid = $('#gridRepairItem');
						for (var i = 0; i < data.rows.length; i++) {
							$grid.datagrid('beginEdit', i);
							var totalActualAmountEditor = $grid.datagrid('getEditor', {index: i, field: 'totalActualAmount'});
							$(totalActualAmountEditor.target).attr('maxlength', 22);
						}*/
						/** vuongmq: 20/04/2015; comment lai; khong cho nhap gia tri tien nua*/
						/*$('#gridRepairItemContainer td[field=totalActualAmount] input').each(function() {
						    var totalActualAmountInputId = $(this).attr('id');
						    Utils.bindFormatOnTextfield(totalActualAmountInputId, Utils._TF_NUMBER);
						    Utils.formatCurrencyFor(totalActualAmountInputId);
						    var maxlength = 17 + Math.floor(17/3);
						    $(this).attr('maxlength', maxlength);
						    $(this)
						    .on('paste blur', function () {
								var element = this;
								setTimeout(function () {
									var text = $(element).val();
									var rawText = text.replace(/,/g, '');
									if (rawText.length > 17) {
										rawText = rawText.substr(0, 17);
										console.log(rawText);
										$(element).val(formatCurrency(rawText));
									}
								}, 50);
							});
						});*/
						RepairEquipmentManageCatalog.loadInfoPayment(data.rows);
					}
				}
	   		 	$(window).resize();			 
			}
		});
	},

	/**
	 * lay cac gia tri tien len thong tin chung
	 * @param: rows: nhieu dong EquipRepairFormDtl
	 * @return void
	 */
	loadInfoPayment: function(rows) {
		if (rows != null) {
			var giaNhanCong = 0;
			var giaVatTu = 0;
			var tongTien = 0;
			//lay o equipRepairForm
			if (rows[0] != null && rows[0].equipRepairForm != null) {
				// gia nhan cong
				if (rows[0].equipRepairForm.workerPrice != null) {
					giaNhanCong = rows[0].equipRepairForm.workerPrice;
				}
				$('#txtWorkerPrice').val(formatCurrency(giaNhanCong));
				/** gia vat tu $('#txtMaterialPrice').val();  cai nay duyet danh sach detail cong tong tien lai*/
				/** tong tien */
				/*tongTien = rows[0].equipRepairForm.totalAmount;
				$('#txtTotalAmount').val(formatCurrency(tongTien));*/
			}
			var len = rows.length;
			for (var i = 0; i < len; i++) {
				if (rows[i].totalAmount != null && !isNaN(rows[i].totalAmount)) {
					giaVatTu += Number(rows[i].totalAmount);
				}
			}
			//  gia vat tu
			$('#txtMaterialPrice').val(formatCurrency(giaVatTu));
			if (!isNaN(giaNhanCong) && !isNaN(giaVatTu)) {
				tongTien += Number(giaNhanCong) + Number(giaVatTu);
			}
			// tong tien
			$('#txtTotalAmount').val(formatCurrency(tongTien));
		}
	},

	/**
	 * khoi tao control tren trang
	 * @param  viewActionType loai hanh dong tren trang: create, edit, read-only
	 * @return void
	 */
	initializePageControl: function(viewActionType) {
		if (viewActionType === ViewActionType.EDIT) {
			disabled('txtPaymentRecordCode');
			disableDateTimePicker('txtPaymentDate');
		} else if (viewActionType === ViewActionType.READONLY) {
			disabled('txtPaymentRecordCode');
			disableDateTimePicker('txtPaymentDate');
			$('#btnUpdate').remove();
		}
	},
	/**
	 * cap nhat phieu thanh toan
	 * @author tuannd20
	 * @param  long equipmentRepairRecordId id cua phieu sua chua
	 * @return void
	 */

	 /**
	 * cap nhat phieu thanh toan; co them cac loai tien
	 * @author vuongmq
	 * @param  long equipmentRepairRecordId id cua phieu sua chua
	 * @return void
	 */
	updateEquipmentRepairPaymentRecord: function(equipmentRepairRecordId) {
		var showErrorMsg = function(msg, errorFieldId) {
			$('#errMsg').html(msg).show();
			$(errorFieldId).focus();
			setTimeout(function(){
				$('#errMsg').hide();
			} ,3000);
			return;
		};
		var paymentRecordCode = $('#txtPaymentRecordCode').val();
		var paymentDate = $('#txtPaymentDate').val();
		var workerPrice = $('#txtWorkerPrice').val();
		var materialPrice = $('#txtMaterialPrice').val();
		var totalAmount = $('#txtTotalAmount').val();
		var msg = '';
		msg = Utils.getMessageOfRequireCheck('txtPaymentRecordCode','Mã phiếu');
		var errorFieldId = '#txtEquipCode';
		if (msg.length == 0){
			msg = Utils.getMessageOfSpecialCharactersValidate('txtPaymentRecordCode', 'Mã phiếu', Utils._CODE);
			errorFieldId = '#txtEquipCode';
		}

		if (isNullOrEmpty(msg) && 
			(isNullOrEmpty(paymentDate) 
			|| (paymentDate.trim().length>0 && !Utils.isDate(paymentDate,'/')))){
			msg = 'Ngày thanh toán không tồn tại hoặc không đúng định dạng dd/mm/yyyy.';
			errorFieldId = '#txtPaymentDate';
		}

		if (!isNullOrEmpty(msg)) {
			showErrorMsg(msg, errorFieldId);
			return;
		}

		var $grid = $('#gridRepairItem');
		var paymentRecordDetailsParam = [];
		var paymentRecordDetails = $grid.datagrid('getRows');
		for (var i = 0; i < paymentRecordDetails.length; i++) {
			var paymentRecordDetail = paymentRecordDetails[i];
			/*var totalActualAmountEditor = $grid.datagrid('getEditor', {index: i, field: 'totalActualAmount'});
			var totalRepairAmountCurrency = $(totalActualAmountEditor.target).val();*/
			/*var totalActualAmountInput = $('#gridRepairItemContainer td[field=totalActualAmount] input[id*=' + paymentRecordDetail.id + ']');
			var totalRepairAmountCurrency = '';
			try {
				//totalRepairAmountCurrency = $(totalActualAmountInput).val();
			} catch(e) {
				// pass through
			}
			
			if (isNullOrEmpty(totalRepairAmountCurrency)) {
				msg = 'Vui lòng nhập thanh toán cho hạng mục: ' + paymentRecordDetail.equipItem.name;
				errorFieldId = $(totalActualAmountInput).attr('id');
				showErrorMsg(msg, errorFieldId);
				return;
			}*/
			/** vuongmq; 20/04/2015; Gia trị totalAmount khong duoc nhap nua*/
			var totalRepairAmountCurrency = 0;
			if (paymentRecordDetails[i].totalAmount != null) {
				totalRepairAmountCurrency = paymentRecordDetails[i].totalAmount;
			}
			//var totalRepairAmount = totalRepairAmountCurrency.replace(/,/g, '');
			var totalRepairAmount = Utils.returnMoneyValue(totalRepairAmountCurrency);
			paymentRecordDetailsParam.push({
				itemId: paymentRecordDetail.id,
				totalRepairAmount: totalRepairAmount
			});
		}

		var dataModel = {
			paymentRecordCode: paymentRecordCode,
			paymentDateInDDMMYYYFormat: paymentDate,
			workerPrice: workerPrice ? Utils.returnMoneyValue(workerPrice): 0, //
			materialPrice: materialPrice ? Utils.returnMoneyValue(materialPrice): 0, //
			totalAmount: totalAmount ? Utils.returnMoneyValue(totalAmount): 0, //
			paymentRecordDetails: paymentRecordDetailsParam
		};

		var flatDataModel = {};
		convertToSimpleObject(flatDataModel, dataModel, 'equipmentRepairPaymentRecord');
		flatDataModel.equipRepairId = equipmentRepairRecordId;
		Utils.addOrSaveRowOnGrid(flatDataModel, '/equipment-repair-manage/update-payment-record', null, null, 'errMsg', function(data){
			if (data.error == false) {
				$("#successMsg").html("Lưu dữ liệu thành công").show();
				try {
					$('#btnUpdate').remove();
					try {
						$('#gridRepairItemContainer td[field=totalActualAmount] input').each(function() {
						    $(this).attr('disabled', 'disabled');
						});
					} catch(e) {
						// pass through
					}
					disabled('txtPaymentRecordCode');
					disableDateTimePicker('txtPaymentDate');
				} catch (e) {
					// pass through
				}
				setTimeout(function() {
					window.location.href = '/equipment-repair-manage/info';
				}, 3000);
			}
		});
	},
	/**
	 * chon kho
	 * 
	 * @return the string
	 * @author phuongvm
	 * @since 19/01/2015
	 */
	selectStock: function(shopCode){
		$('#stockCode').val(shopCode);		
		$('#easyuiPopupSearchStock').dialog('close');
	},
	/**
	 * Show popup chon kho
	 * @author phuongvm
	 * @since 19/01/2015
	 */
	showPopupStock:function(){
	$('#easyuiPopupSearchStock').show();
		$('#easyuiPopupSearchStock').dialog({  
	        closed: false,  
	        cache: false,  
	        modal: true,
	        width : 800,
	        height : 'auto',
	        onOpen: function(){	
				$('#shopCode').focus();
				$('#shopCode, #shopName,#customerCode, #customerName').bind('keyup',function(event){
					if(event.keyCode == keyCodes.ENTER){
						$('#btnSearchStockDlg').click(); 
					}
				});	
				$('#stockGridDialog').datagrid({
					url : '/equipment-repair-manage/searchStock',
					autoRowHeight : true,
					rownumbers : true, 
					fitColumns:true,
					scrollbarSize:0,
					pagination:true,
					pageSize: 10,
					pageList: [10],
					width: $('#stockGridDialogContainer').width(),
					height: 'auto',
					autoWidth: true,	
					queryParams:{
						shopCode: $('#shopCode').val().trim(),
						shopName: $('#shopName').val().trim()
					},
					columns:[[
						{field:'shopCode', title:'Đơn vị', align:'left', width: 140, sortable:false, resizable:false, formatter:function(value,row,index) {
			        	return Utils.XSSEncode(row.shopCode + " - " + row.shopName) ;        
			        }},
			        {field:'customerCode', title:'Khách hàng', align:'left', width: 140, sortable:false, resizable:false, formatter:function(value,row,index) {
			        	var html = '';
			        	if(row.customerCode != null){
			        		html = row.customerCode + ' - ' + row.customerName ;
			        	}   
			        	return Utils.XSSEncode(html);
			        }},
			        {field:'address', title:'Địa chỉ', align:'left', width: 150, sortable:false, resizable:false, formatter: function(value, row, index) {
				    	return Utils.XSSEncode(value);
				    }},
			        {field:'stockCode', title:'Mã kho', align:'left', width: 80, sortable:false, resizable:false, formatter: function(value, row, index) {
				    	return Utils.XSSEncode(value);
				    }},
			        {field:'choose', title:'', align:'center', width:40, sortable:false, resizable:false, formatter:function(value,row,index) {
			        	return "<a href='javascript:void(0)' onclick=\"return RepairEquipmentManageCatalog.selectStock('"+ Utils.XSSEncode(row.stockCode) +"');\">Chọn</a>";        
			        }}			
					]],
					onBeforeLoad:function(param){											 
					},
					onLoadSuccess :function(data){   			 
						if (data == null || data.total == 0) {
							//$("#errMsgInfo").html("Không tìm thấy danh sách biên bản giao nhận thiết bị!").show();
						}else{
							$('.datagrid-header-rownumber').html('STT');		    	
						}	    		
					}
				});				
	        },
			onClose: function(){
				$('#easyuiPopupSearchStock').hide();
				$('#shopCode').val('').change();
				$('#shopName').val('').change();
				$('#customerCode').val('').change();
				$('#customerName').val('').change();
				$('#stockGridDialog').datagrid('loadData',[]);
				$('#stockCode').focus();
			}
		});		
		return false;
	},
	/**
	 * Tim kiem kho
	 * @author phuongvm
	 * @since 19/01/2015
	 */
	searchStock: function(){
		var params=new Object(); 
		params.shopCode = $('#shopCode').val().trim();
		params.shopName = $('#shopName').val().trim();
		params.customerCode = $('#customerCode').val().trim();
		params.customerName = $('#customerName').val().trim();
		$('#stockGridDialog').datagrid('load',params);
	},
	/**
	 * xuat file excel BBSC
	 * @author nhutnn
	 * @since 16/01/2015
	 */
	exportBySearchListRepair: function(){
		$('.ErrorMsgStyle').html("").hide();
		var err = "";
		$('#errMsg').html('').hide();
		var msg="";
		var fDate = $('#fDate').val();
		var tDate =$('#tDate').val();
		if (msg.length == 0 && $('#fDate').val().trim().length > 0 && !Utils.isDate($('#fDate').val(),'/')) {
			msg = 'Ngày tạo từ không tồn tại hoặc không đúng định dạng dd/mm/yyyy.';
			err = '#fDate';
		}
		if (msg.length == 0 && $('#tDate').val().trim().length > 0 && !Utils.isDate($('#tDate').val(),'/')) {
			msg = 'Đến ngày không tồn tại hoặc không đúng định dạng dd/mm/yyyy.';
			err = '#tDate';
		}
		if (msg.length==0 && fDate.length > 0 && tDate.length > 0 && !Utils.compareDate(fDate, tDate)) {
			msg = 'Ngày tạo từ phải là ngày trước hoặc cùng ngày với đến ngày';
		}
		var data = $('#grid').datagrid('getRows');
		if (data != null && data.length <= 0 ) {
			msg = 'Không có dữ liệu xuất báo cáo';
		}
		if (msg.length > 0) {
			$('#errMsg').html(msg).show();
			$(err).focus();
			setTimeout(function(){$('#errMsg').hide();},3000);
			return false;
		}
		
		/** vuongmq; 20/04/2015; luc nay chi can lay paramsSearch la xong: RepairEquipmentManageCatalog._paramsSearch*/
		$.messager.confirm('Xác nhận','Bạn có muốn xuất file excel?',function(r) {
			if (r) {
				ReportUtils.exportReport('/equipment-repair-manage/export-repair-record', RepairEquipmentManageCatalog._paramsSearch, 'errMsg');					
			}
		});		
		return false;
	},
	/**
	 * tao gia tri cho autoComplete Combobox Nhom thiet bi
	 * @author tamnm
	 * @since 17/06/2015
	 */
	setEquipGroup: function(index) {
		var equipGroup = RepairEquipmentManageCatalog._lstEquipGroup;
		var obj = {
			id: -1,
			code: "Tất cả",
			name: "Tất cả",
			codeName: "Tất cả",
			searchText: unicodeToEnglish("Tất cả").toUpperCase(),
		};
		equipGroup.splice(0, 0, obj);
		for(var i = 0, sz = equipGroup.length; i < sz; i++) {
	 		equipGroup[i].searchText = unicodeToEnglish(equipGroup[i].code + " " + equipGroup[i].name);
	 		equipGroup[i].searchText = equipGroup[i].searchText.toUpperCase();
		}
	 	// equipGroup.splice(0, 0, { code: "", name: "", searchText:"" });
	 	$('#ddlEquipGroupId').combobox("loadData", equipGroup);
	 	$('#ddlEquipGroupId').combobox("setValue", -1);
	},
};
/*
 * END OF FILE - /web/web/resources/scripts/business/equipment/vnm.repair-equipment-manage-catalog.js
 */

/*
 * START OF FILE - /web/web/resources/scripts/business/equipment/vnm.equipment-manage-liquidation.js
 */
var EquipmentManagerLiquidation = {
		_editIndex:null,
		_eIndex: null,
		_lstEquipInRecord:null,
		_numberEquipInRecord:null,
		_lstEquipInsert:null,	
		_lstRowId: null,
		_countFile:0,
		_arrFileDelete:null,
		_DISABLED: 'DISABLED',
		_ENABLE: 'ENABLE',
		
	/**
	* Validate gird
	* @author hoanv25
	* @since May 29,2015
	*/	
	endEditing: function() {
			if (EquipmentManagerLiquidation._eIndex == undefined) {
				return true;
			}
			if ($('#gridEquipmentLiquidation').datagrid('validateRow', EquipmentManagerLiquidation._eIndex)){
				$('#gridEquipmentLiquidation').datagrid('endEdit', EquipmentManagerLiquidation._eIndex);
				EquipmentManagerLiquidation._eIndex = undefined;
				return true;
			} else {
				return false;
			}
	},
	/**
	 * Tim kiem danh sach bien ban giao nhan
	 * @author nhutnn
	 * @since 12/12/2014
	 */
	searchLiquidation: function(){

		$('#errMsgInfo').html('').hide();
		var msg ='';
		var fdate = $('#fDate').val().trim();
		if(msg.length == 0 && fdate != '' && !Utils.isDate(fdate) && fdate != '__/__/____'){
			msg = 'Từ ngày không tồn tại hoặc không đúng định dạng dd/mm/yyyy.';
		}
		var tdate = $('#tDate').val().trim();
		if(msg.length == 0 && tdate != '' && !Utils.isDate(tdate) && tdate != '__/__/____'){
			msg = 'Đến ngày không tồn tại hoặc không đúng định dạng dd/mm/yyyy.';
		}
		if(msg.length == 0 && fdate != '' && tdate != '' && !Utils.compareDate(fdate, tdate)){
			msg = 'Đến ngày không được nhỏ hơn Từ ngày';	
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfSpecialCharactersValidate('recordCode','Mã biên bản');
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfSpecialCharactersValidate('docNumber','Số CV/ Tờ trình thanh lý');
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfSpecialCharactersValidate('equipCode','Mã thiết bị');
		}
		
		if(msg.length > 0){
			$('#errMsgInfo').html(msg).show();
			return false;
		}
		
		var params=new Object(); 
		params.fromDate = $('#fDate').val().trim();
		params.toDate = $('#tDate').val().trim();
		params.docNumber = $('#docNumber').val().trim();		
		params.code = $('#recordCode').val().trim();
		params.equipCode = $('#equipCode').val().trim();
		params.status = $('#statusRecord').val().trim();
		
		EquipmentManagerLiquidation._lstRowId = new Array();
		$('#gridLiquidation').datagrid('load',params);
		$('#gridLiquidation').datagrid('uncheckAll');
	},
	/**
	 * show popup import excel
	 * @author nhutnn
	 * @since 12/12/2014
	 */
	showDlgImportExcel: function(){
		$('#easyuiPopupImportExcel').show();
		$('.ErrorMsgStyle').hide();
		$('#easyuiPopupImportExcel').dialog({  
	        closed: false,  
	        cache: false,  
	        modal: true,
	        height: 'auto',
	        onOpen: function(){	 
	        	$('.easyui-dialog .BtnGeneralStyle').css('color','');
	        },
	        onClose: function(){
	        	EquipmentManagerLiquidation.searchLiquidation();
				$("#gridLiquidation").datagrid('uncheckAll');
				$("#fakefilepcBBTL").val("");
				$("#excelFileBBTL").val("");				
	        }
		});
	},
	/**
	 * import excel BBTL
	 * @author nhutnn
	 * @since 12/12/2014
	 */
	importExcelBBTL: function(){
		$('.ErrorMsgStyle').hide();
		VTUtilJS.importExcelUtils(function(data) {
			$("#errExcelMsgBBTL").html(data.message).show();		
			if (data.fileNameFail == null || data.fileNameFail == "" ) {					
				setTimeout(function(){
					$('#errExcelMsgBBTL').html("").hide();
					$('#easyuiPopupImportExcel').dialog('close');
				}, 3000);				
			} else {
				$("#fakefilepcBBTL").val("");
				$("#excelFileBBTL").val("");			
				//$('#gridLiquidation').datagrid('reload');	
			}						
		}, "importFrmBBTL", "excelFileBBTL", null, "errExcelMsgBBTL");
		return false;
	},
	/**
	 * xuat file excel BBTL
	 * @author nhutnn
	 * @since 12/12/2014
	 */
	exportExcelBBTL: function() {
		$('.ErrorMsgStyle').html("").hide();
		var lstChecked = EquipmentManagerLiquidation._lstRowId;
		var rows = $('#gridLiquidation').datagrid('getRows');	
		if (rows.length==0) {
			$('#errMsgInfo').html('Không có biên bản để xuất excel').show();
			return;
		}
		if (lstChecked.length == 0) {
			$('#errMsgInfo').html('Bạn chưa chọn biên bản để xuất excel. Vui lòng chọn biên bản').show();
			return;
		}	
		
		var params = new Object();
		params.lstIdRecord = lstChecked;
		$.messager.confirm('Xác nhận','Bạn có muốn xuất file excel?',function(r){
			if(r){
				ReportUtils.exportReport('/equipment-manage-liquidation/export-record-liquidation', params, 'errMsgInfo');					
			}
		});		
		return false;
	},
	
	/**
	 * Luu bien ban ban giao
	 * @author nhutnn
	 * @since 12/12/2014
	 */
	saveLiquidation: function() {		
		$('#errMsgInfo').html('').hide();
		var rows = $('#gridLiquidation').datagrid('getRows');
		if(rows.length == 0){
			$('#errMsgInfo').html('Không có dữ liệu để lưu').show();
			return false;
		}
		var lstRecordSuccess = new Array();
		//var lstCheck = $('#gridLiquidation').datagrid('getChecked');
		var lstCheck = EquipmentManagerLiquidation._lstRowId;
		if(lstCheck.length > 0){
			var statusRecordChange = $('#statusRecordLabel').val().trim();
			for(var i=0; i < lstCheck.length; i++){		
				if(statusRecordChange != "" ){
					// lstRecordSuccess.push(lstCheck[i].idLiquidation);
					lstRecordSuccess.push(lstCheck[i]);
				}
			}	
			if(lstRecordSuccess.length <= 0){
				$('#errMsgInfo').html('Bạn chưa chọn trạng thái để lưu!').show();
				return;
			}
			var params = new Object();
			params.lstIdForm = lstRecordSuccess;
			params.status = statusRecordChange;
			
			Utils.addOrSaveData(params, '/equipment-manage-liquidation/save-liquidation', null, 'errMsgInfo', function(data) {
				if(data.error){
					$('#errMsgInfo').html(data.errMsg).show();
					return;
				}
				if (data.lstRecordNotSendMail != undefined && data.lstRecordNotSendMail != null && data.lstRecordNotSendMail != '') {
					$('#errMsgInfo').html('Gửi Email không thành công biên bản báo mất: ' + data.lstRecordNotSendMail).show();
				}
				if(data.lstRecordError != null && data.lstRecordError.length>0){					
					VCommonJS.showDialogList({	
						dialogInfo: {
							title: 'Lỗi'
						},	   
						data : data.lstRecordError,			   
						columns : [[
							{field:'formCode', title:'Mã biên bản', align:'left', width: 150, sortable:false, resizable:false},
							{field:'formStatusErr', title:'Trạng thái biên bản', align:'center', width: 80, sortable:false, resizable:false,formatter: function(value, row, index){
								var html="";
								if(row.formStatusErr == 1){
									html = '<img src="/resources/images/icon_delete.png" height="20" width="20" style="margin-right:5px;" title="Error">';
								}
								return html;
								
							}}
							// {field:'periodError', title:'Kỳ của biên bản', align:'center', width:80, sortable:false, resizable:false,formatter: function(value, row, index){
							// 	var html="";
							// 	if(row.periodError == 1){
							// 		html = '<img src="/resources/images/icon_delete.png" height="20" width="20" style="margin-right:5px;" title="Error">';
							// 	}
							// 	return html;
								
							// }}
						]]
					});
				}else{
					$('#successMsgInfo').html('Lưu dữ liệu thành công').show();
				}
				setTimeout(function(){ 
					$('#successMsgInfo').html('').hide();
				}, 5000);
				
				$('#gridLiquidation').datagrid('reload');
				$('#gridLiquidation').datagrid('uncheckAll');
			}, null, null, null, "Bạn có muốn cập nhật ?", null);	
		}else{
			$('#errMsgInfo').html('Bạn chưa chọn biên bản để lưu').show();
			return false;
		}		
	},
	
	/**
	 * validate tong gia tri thanh ly va thu hoi cua gridEquipmentLiquidation so voi tong
	 * @author vuongmq
	 @date: 07/07/2015
	 */
	validateTongGiaTriThanhLyThuHoi: function(value) {
		//vi kiem tra tong gia tri thanh ly va thu hoi khong duoc lon hon tong nguyen gia, ko can type
		value = Utils.returnMoneyValue(value);
		var rs = $("#gridEquipmentLiquidation").datagrid("getRows");
		var price = 0;
		if (rs != null && rs.length > 0) {
			var r = null;
			for (var i = 0, sz = rs.length; i < sz; i++) {
				// cong don nguyen gia
				r = rs[i];
				if (r.price) {
					price = price + Number(r.price);
				}
			}
			if (Number(value) > Number(price)) {
				return false;
			}
		} else {
			// truong hop xoa khong con dong nao
			if (Number(value) > 0 ) {
				return false;
			}
		}
		return true;
	},

	/**
	 * Load lai footer cua gridEquipmentLiquidation
	 * @author vuongmq
	 @date: 07/07/2015
	 */
	refreshFooter: function() {
		var rs = $("#gridEquipmentLiquidation").datagrid("getRows");
		var price = 0;
		var liquidationValue = 0; 
		var evictionValue = 0;
		if (rs != null && rs.length > 0) {
			var r = null;
			for (var i = 0, sz = rs.length; i < sz; i++) {
				r = rs[i];
				if (r.price) {
					price = price + Number(r.price);
				}
				// cong don theo gia tri thanh ly
				var valEquipTL = $("#gridEquipmentLiquidation").datagrid('getEditor', {index:i,field:'liquidationValue'});
				if (valEquipTL != undefined && valEquipTL !=null) {
					var gtThanhLy = $(valEquipTL.target).val();
					if (gtThanhLy != undefined && gtThanhLy != null && gtThanhLy != '') {
						liquidationValue = liquidationValue + Number(Utils.returnMoneyValue(gtThanhLy));
					}
				} else {
					var gtThanhLy = r.liquidationValue;
					if (gtThanhLy != undefined && gtThanhLy != null && gtThanhLy != '') {
						liquidationValue = liquidationValue + Number(Utils.returnMoneyValue(gtThanhLy));
					}
				}
				// cong don theo gia tri thu hoi
				var valEquipTH = $("#gridEquipmentLiquidation").datagrid('getEditor', {index:i,field:'evictionValue'});
				if (valEquipTH !=null) {
					var gtThuHoi = $(valEquipTH.target).val();
					if (gtThuHoi != undefined && gtThuHoi != null && gtThuHoi != '') {
						evictionValue = evictionValue + Number(Utils.returnMoneyValue(gtThuHoi));
					}
				} else {
					var gtThuHoi = r.evictionValue;
					if (gtThuHoi != undefined && gtThuHoi != null && gtThuHoi != '') {
						evictionValue = evictionValue + Number(Utils.returnMoneyValue(gtThuHoi));
					}
				}
			}
			if ($("#gridEquipmentLiquidation").datagrid("getData").footer != undefined && $("#gridEquipmentLiquidation").datagrid("getData").footer != null) {
				var ft = $("#gridEquipmentLiquidation").datagrid("getData").footer[0];
				ft.price = price;
				ft.liquidationValue = liquidationValue;
				ft.evictionValue = evictionValue;
				$("#gridEquipmentLiquidation").datagrid("reloadFooter");
			} else {
				// truong hop qua chinh sua, view danh sach tong cua grid lai
				var footer = [{isFooter: true, price: price, liquidationValue: liquidationValue, evictionValue: evictionValue}];
				$("#gridEquipmentLiquidation").datagrid("getData").footer = footer;
				var data = $('#gridEquipmentLiquidation').datagrid('getData');
				//data.footer = footer;
				$('#gridEquipmentLiquidation').datagrid('loadData', data);
				EquipmentManagerLiquidation.editInputInGrid();
			}
		} else {
			// truong hop xoa khong con dong nao
			if ($("#gridEquipmentLiquidation").datagrid("getData").footer != undefined && $("#gridEquipmentLiquidation").datagrid("getData").footer != null) {
				var ft = $("#gridEquipmentLiquidation").datagrid("getData").footer[0];
				ft.price = price;
				ft.liquidationValue = liquidationValue;
				ft.evictionValue = evictionValue;
				$("#gridEquipmentLiquidation").datagrid("reloadFooter");
			} else {
				// truong hop qua chinh sua, view danh sach tong cua grid lai
				var footer = [{isFooter: true, price: price, liquidationValue: liquidationValue, evictionValue: evictionValue}];
				$("#gridEquipmentLiquidation").datagrid("getData").footer = footer;
				var data = $('#gridEquipmentLiquidation').datagrid('getData');
				//data.footer = footer;
				$('#gridEquipmentLiquidation').datagrid('loadData', data);
				EquipmentManagerLiquidation.editInputInGrid();
			}
		}
	},

	/**
	 * Load lai footer cua gridEquipmentLiquidation
	 * @author vuongmq
	 @date: 07/07/2015
	 */
	checkGiaTriThanhLyThuHoi: function(type) {
		var rs = $("#gridEquipmentLiquidation").datagrid("getRows");
		if (rs != null && rs.length > 0) {
			if (type == EquipmentManagerLiquidation._ENABLE) {
				for (var i = 0, sz = rs.length; i < sz; i++) {
					// enable gia tri thanh ly
					var valEquipTL = $("#gridEquipmentLiquidation").datagrid('getEditor', {index:i,field:'liquidationValue'});
					if (valEquipTL != undefined && valEquipTL !=null) {
						$(valEquipTL.target).attr('disabled',false);
						// $(valEquipTL.target).val(''); // nhung cai nao disable = true thi moi gan bang ''
					}
					// enable gia tri thu hoi
					var valEquipTH = $("#gridEquipmentLiquidation").datagrid('getEditor', {index:i,field:'evictionValue'});
					if (valEquipTH != undefined && valEquipTH !=null) {
						$(valEquipTH.target).attr('disabled',false);
						//$(valEquipTH.target).val(''); // nhung cai nao disable = true thi moi gan bang ''
					}
				}
			} else if (type == EquipmentManagerLiquidation._DISABLED) {
				for (var i = 0, sz = rs.length; i < sz; i++) {
					// disabled gia tri thanh ly
					var valEquipTL = $("#gridEquipmentLiquidation").datagrid('getEditor', {index:i,field:'liquidationValue'});
					if (valEquipTL != undefined && valEquipTL !=null) {
						$(valEquipTL.target).attr('disabled',true);
						$(valEquipTL.target).val('');
					}
					// disabled gia tri thu hoi
					var valEquipTH = $("#gridEquipmentLiquidation").datagrid('getEditor', {index:i,field:'evictionValue'});
					if (valEquipTH != undefined && valEquipTH !=null) {
						$(valEquipTH.target).attr('disabled',true);
						$(valEquipTH.target).val('');
					}
				}
			}	
		}
	},

	/**
	 * Xoa thiet bi trong bien ban ban giao
	 * @author nhutnn
	 * @since 12/12/2014
	 */
	deleteEquipment: function(index){		
		if(EquipmentManagerLiquidation._numberEquipInRecord != null && index < EquipmentManagerLiquidation._numberEquipInRecord){
			if(EquipmentManagerLiquidation._lstEquipInRecord == null){
				EquipmentManagerLiquidation._lstEquipInRecord = new Array();
			}
			var row = $('#gridEquipmentLiquidation').datagrid('getRows')[index];
			EquipmentManagerLiquidation._lstEquipInRecord.push(row.equipmentCode);
			EquipmentManagerLiquidation._numberEquipInRecord--;
		}else if(EquipmentManagerLiquidation._lstEquipInsert != null && index >= EquipmentManagerLiquidation._numberEquipInRecord){
			var row = $('#gridEquipmentLiquidation').datagrid('getRows')[index];
			var ix = EquipmentManagerLiquidation._lstEquipInsert.indexOf(row.equipmentCode);
			if (ix > -1) {
				EquipmentManagerLiquidation._lstEquipInsert.splice(ix, 1);
			}
		}
		$('#gridEquipmentLiquidation').datagrid("deleteRow", index);	
		// 
		var rowEquip = $('#gridEquipmentLiquidation').datagrid('getRows');
		var nSize = rowEquip.length;
		var equipCode = $('#equipmentCodeInGrid').val();
		if($('#equipmentCodeInGrid').val() != undefined){
			nSize = rowEquip.length-1;
			rowEquip[rowEquip.length-1].equipmentCode = null;
			$('#equipmentCodeInGrid').remove();
		}
		/*for(var i=index; i<nSize; i++){
			var row = rowEquip[i];
			var valEquip = $("#gridEquipmentLiquidation").datagrid('getEditor', {index:i,field:'valueEquip'});
			row.valEquip = $(valEquip.target).val();
			$('#gridEquipmentLiquidation').datagrid('updateRow',{
				index: i,	
				row: row
			});
			$('#gridEquipmentLiquidation').datagrid('beginEdit', i);
		}*/

		if( EquipmentManagerLiquidation._editIndex!= null && index < EquipmentManagerLiquidation._editIndex){
			EquipmentManagerLiquidation._editIndex--;
		}else{
			EquipmentManagerLiquidation._editIndex=null;
		}	
		if(equipCode != undefined){
			/*var valEquip = $("#gridEquipmentLiquidation").datagrid('getEditor', {index:rowEquip.length-1,field:'valueEquip'});
			rowEquip[rowEquip.length-1].valueEquip = $(valEquip.target).val();

			$('#gridEquipmentLiquidation').datagrid('updateRow',{
				index: rowEquip.length-1,	// index start with 0
				row: rowEquip[rowEquip.length-1]
			});*/
			if($('#equipmentCodeInGrid').val() != undefined){
				$('#equipmentCodeInGrid').val(equipCode);
			}
			$('#gridEquipmentLiquidation').datagrid('beginEdit', rowEquip.length-1);
			$('#equipmentCodeInGrid').width($('#equipmentCodeInGrid').parent().width());
			EquipmentManagerLiquidation.editInputInGrid();
		}
		//update lai index cho Onclick Xoa
		EquipmentManagerLiquidation.loadIndexDelete();
		//update lai dong footer: dong tong cua cac gia tri
		EquipmentManagerLiquidation.refreshFooter();
	},

	/**
	 * load lai su kien click index cho nut delete
	 * @author vuongmq
	 * @since 07/07/2015
	 */
	loadIndexDelete: function() {
		var i = 0;
		$('.deleteClass').each(function(){
		   $(this).attr('onclick', 'EquipmentManagerLiquidation.deleteEquipment('+i+')');
		   i++;
		})
	},

	/**
	 * Them mot dong thiet bi
	 * @author nhutnn
	 * @since 06/01/2015
	 */
	insertEquipmentInGrid: function(){	
		$('.SuccessMsgStyle').html('').hide();
		$('.ErrorMsgStyle').html('').hide();
		var sz = $("#gridEquipmentLiquidation").datagrid("getRows").length;
		if(EquipmentManagerLiquidation._editIndex != null && EquipmentManagerLiquidation._editIndex == sz-1){
			var row = $('#gridEquipmentLiquidation').datagrid('getRows')[EquipmentManagerLiquidation._editIndex];
			
			if ($('#equipmentCodeInGrid').val() == '') {
				$('#errMsgInfo').html('Bạn chưa xử lý xong dòng thiết bị thêm mới!').show();
				return false;
			} else if($('#equipmentCodeInGrid').val() != undefined) {
				var rowEq = $('#gridEquipmentLiquidation').datagrid('getRows');
				for(var i=0; i < rowEq.length-1; i++){
					if(rowEq[i].equipmentCode == $('#equipmentCodeInGrid').val().trim()){
						$('#errMsgInfo').html("Mã thiết bị "+rowEq[i].equipmentCode+" đã tồn tại!").show();
						return false;
					}
				}
				if (row.equipmentCode == undefined || row.equipmentCode == null) {
					$('#errMsgInfo').html("Sau khi nhập thiết bị vui lòng enter textbox để load thông tin thiết bị nhập vào! ").show();
					return false;
				}
				row.equipmentCode = $('#equipmentCodeInGrid').val();
			}
			/*var valEquip = $("#gridEquipmentLiquidation").datagrid('getEditor', {index:EquipmentManagerLiquidation._editIndex,field:'valueEquip'});
			row.valueEquip = $(valEquip.target).val();*/

//			$('#equipmentCodeInGrid').remove();			
//			$('#gridEquipmentLiquidation').datagrid('updateRow',{
//				index: EquipmentManagerLiquidation._editIndex,	
//				row: row
//			});
			$('#gridEquipmentLiquidation').datagrid('beginEdit', EquipmentManagerLiquidation._editIndex);
			/*var valEquip = $("#gridEquipmentLiquidation").datagrid('getEditor', {index:EquipmentManagerLiquidation._editIndex,field:'valueEquip'});
			$(valEquip.target).bind('keydown', function(e){
				if(e.keyCode==110){
					e.preventDefault();
				}
			});*/
			if(EquipmentManagerLiquidation._lstEquipInsert == null){
				EquipmentManagerLiquidation._lstEquipInsert = new Array();
			}
			if (row != undefined && row != null) {
				EquipmentManagerLiquidation._lstEquipInsert.push(row.equipmentCode);
			}
		}		
		//EquipmentManagerLiquidation._editIndex = sz -1; // sz - 1: la tru dong tong ra	
		/*EquipmentManagerLiquidation._editIndex = sz -1;
		var dataGridLiquidation = $("#gridEquipmentLiquidation").datagrid("getRows");
		if (dataGridLiquidation[EquipmentManagerLiquidation._editIndex].equipmentCode != undefined && dataGridLiquidation[EquipmentManagerLiquidation._editIndex].equipmentCode != null) {
			// truong hop them dong TB co du lieu thanh cong
			EquipmentManagerLiquidation._editIndex = sz;
			$('#gridEquipmentLiquidation').datagrid("appendRow", {});
		} else {
			EquipmentManagerLiquidation._editIndex = sz -1;
		}*/
		EquipmentManagerLiquidation._editIndex = sz;
		$('#gridEquipmentLiquidation').datagrid("appendRow", {});	
		$('#gridEquipmentLiquidation').datagrid('beginEdit', EquipmentManagerLiquidation._editIndex);

		// F9 tren grid
		EquipmentManagerLiquidation.editInputInGrid();
		$("#gridEquipmentLiquidation").datagrid("selectRow", EquipmentManagerLiquidation._editIndex);	
		$('#equipmentCodeInGrid').focus();
		$('#equipmentCodeInGrid').width($('#equipmentCodeInGrid').parent().width());
		EquipmentManagerLiquidation.refreshFooter();
	},

	addFooterFrist: function() {
		var footer = [{isFooter: true, price:0, liquidationValue:0, evictionValue: 0}];
		$("#gridEquipmentLiquidation").datagrid("getData").footer = footer;
		var data = $('#gridEquipmentLiquidation').datagrid('getData');
		$('#gridEquipmentLiquidation').datagrid('loadData', data);
		EquipmentManagerLiquidation.editInputInGrid();
	},
	/**
	 * Show popup tim kiem thiet bi
	 * @author nhutnn
	 * @since 12/12/2014
	 * 
	 * @author hunglm16
	 * @sine JUNE 13,2015
	 * @description Ra soat noi dung phan quyen CMS
	 */
	
	 /***
		vuongmq; 09/07/2015; khong su dung nua; dung showPopupSearchEquipment
	 */
	/*showPopupSearchEquipmentEx: function(isSearch) {
		$('.ErrorMsgStyle').html('').hide();
		$('.SuccessMsgStyle').html('').hide();
		$('#easyuiPopupSearchEquipment').show();
		$('#easyuiPopupSearchEquipment').dialog({  
	        closed: false,  
	        cache: false,  
	        modal: true,
	        width : 1000,
	        height : 'auto',
	        onOpen: function(){
	        	//setDateTimePicker('yearManufacturing');
	        	var year = new Date();
	        	year = year.getFullYear(); 
	        	var op = '<option value="" selected="selected">Chọn</option><option value="'+year+'">'+year+'</option>';
	        	for(var i=1; i<100; i++){
	        		op+='<option value="'+(year-i)+'">'+(year-i)+'</option>';
	        	}
	        	$('#yearManufacturing').html(op).change();
	        	$('#equipmentCode, #seriNumber, #typeEquipment').bind('keyup',function(event){
	        		if(event.keyCode == keyCodes.ENTER){
	        			$('#btnSeachEquipmentDlg').click(); 
	        		}
	        	});
	        	$('#groupEquipment, #providerCode, #yearManufacturing').bind('keyup',function(event){
	        		if(event.keyCode == keyCodes.ENTER){
	        			$('#btnSeachEquipmentDlg').click(); 
	        		}
	        	});
	        	EquipmentManagerLiquidation.getEquipCatalog();
	        	EquipmentManagerLiquidation.getEquipGroup(null);
	        	var params = new Object();
				params = {
						equipCode: $('#equipmentCode').val().trim(),
						seriNumber: $('#seriNumber').val().trim(),
						categoryId: $('#typeEquipment').val().trim(),
						groupId: $('#groupEquipment').val().trim(),
						providerId: $('#providerId').val().trim(),
						yearManufacture: $('#yearManufacturing').val().trim(),
						stockCode: $('#stockCode').val().trim(),
						status: activeType.RUNNING,
						//stockType: 1, //Don vi
						//tradeStatus: 0,
						//usageStatus: usageStatus.SHOWING_WAREHOUSE,//Dang o kho
						flagPagination: true
					};
				if($("#idRecordHidden").val() != "" && EquipmentManagerLiquidation._lstEquipInRecord != null){
					params.lstEquipDelete = "";
					for(var i=0; i<EquipmentManagerLiquidation._lstEquipInRecord.length; i++){
						params.lstEquipDelete += EquipmentManagerLiquidation._lstEquipInRecord[i]+',';
					}					
				}
				if(EquipmentManagerLiquidation._lstEquipInsert != null){
					params.lstEquipAdd = "";
					for(var i=0; i<EquipmentManagerLiquidation._lstEquipInsert.length; i++){
						params.lstEquipAdd += EquipmentManagerLiquidation._lstEquipInsert[i]+',';
					}					
				}
				$('#equipmentGridDialog').datagrid({
//					url : '/equipment-manage-liquidation/search-equipment-liquidation',
					url : '/commons/search-equip-by-shop-root-in-stock',
					autoRowHeight : true,
					rownumbers : true, 
					//fitColumns:true,
					pagination:true,
					scrollbarSize:0,
					pageSize:10,
					pageList: [10],
					width: $('#equipmentGridDialogContainer').width(),
					height: 'auto',
					autoWidth: true,	
					queryParams: params,
					columns:[[
						{field: 'select',title:'',width: 50,sortable:false,resizable:false, align: 'center', formatter: function(value, row, index) {
							var html="";
							if(isSearch){
								html = '<a href="javascript:void(0);" onclick="EquipmentManagerLiquidation.selectEquipmentSearch('+index+');">Chọn</a>';
							}else{
								html = '<a href="javascript:void(0);" onclick="EquipmentManagerLiquidation.selectEquipment('+index+');">Chọn</a>';
							}
					    	return html;
						}},
					    {field: 'equipmentCode', title:'Mã thiết bị', width: 200, sortable:false, resizable:false, align: 'left'},	
					    {field: 'seriNumber', title:'Số serial', width: 100, sortable:false, resizable:false, align: 'left'},	
					    {field: 'healthStatus', title:'Tình trạng thiết bị', width: 100, sortable:false, resizable:false, align: 'left'},	
					    {field: 'stock',title:'Kho', width: 120, sortable:false, resizable:false, align: 'left', formatter: function(value, row, index) {
					    	var html = "";
							if(row.stockCode != null && row.stockName!= null){
								html = row.stockCode+' - '+row.stockName; 
							}
							return html;
						}},	
					    {field: 'typeEquipment', title:'Loại thiết bị', width: 80, sortable:false, resizable:false, align: 'left'},	
					    {field: 'groupEquipment', title:'Nhóm thiết bị', width: 200, sortable:false, resizable:false, align: 'left', formatter: function(value, row, index) {
					    	var html="";
							if(row.groupEquipmentCode != null && row.groupEquipmentName!= null){
								html = row.groupEquipmentCode+' - '+row.groupEquipmentName; 
							}
							return html;
						}},	
					    {field: 'capacity',title:'Dung tích (lít)', width: 80, sortable:false, resizable:false, align: 'center'},	
					    {field: 'equipmentBrand',title:'Hiệu', width: 80, sortable:false, resizable:false, align: 'left'},	
					    {field: 'equipmentProvider',title:'NCC',width: 80, sortable:false, resizable:false, align: 'left'},	
					    {field: 'yearManufacture', title:'Năm sản xuất', width: 80, sortable:false, resizable:false, align: 'right'},												
					]],
					onBeforeLoad:function(param){											 
					},
					onLoadSuccess :function(data){   			 
						if (data == null || data.total == 0) {
							//$("#errMsgInfo").html("Không tìm thấy danh sách biên bản giao nhận thiết bị!").show();
						}else{
							$('.datagrid-header-rownumber').html('STT');		    	
						}	    
						$('#equipmentCode').focus();			
					}
				});
	        },
			onClose: function(){
				$('#easyuiPopupSearchEquipment').hide();
				$('#equipmentCode').val('').change();
				$('#seriNumber').val('').change();
				$('#typeEquipment').html('<option value="" selected="selected">Tất cả</option>');
				$('#groupEquipment').html('<option value="" selected="selected">Tất cả</option>').change();
				$('#providerId').html('<option value="" selected="selected">Tất cả</option>');
				$('#yearManufacturing').val('').change();
				$('#equipmentGridDialog').datagrid('loadData',[]);
			}
		});
	},*/

	/**
	 * Chon thiet bi de tim kiem
	 * @author nhutnn
	 * @since 12/12/2014
	 */
	selectEquipmentSearch: function(index){
		var rowPopup = $('#equipmentGridDialog').datagrid('getRows')[index];
		$('#equipCode').val(rowPopup.equipmentCode);
		
		$('#easyuiPopupSearchEquipment').dialog('close');
	},
	/**
	 * Lay danh muc loai, ncc thiet bi
	 * @author nhutnn
	 * @since 20/12/2014
	 */
	getEquipCatalog: function(){
		$('#typeEquipment').html('<option value="" selected="selected">Tất cả</option>');
    	$('#provider').html('<option value=""  selected="selected">Tất cả</option>');
    	var params = new Object();
    	Utils.getJSONDataByAjaxNotOverlay(params, '/equipment-manage-liquidation/get-equipment-catalog', function(result){	
		// $.getJSON('/equipment-manage-liquidation/get-equipment-catalog', function(result) {				
			if(result.equipsCategory != undefined && result.equipsCategory != null && result.equipsCategory.length > 0) {					
				for(var i=0; i < result.equipsCategory.length; i++){
					$('#typeEquipment').append('<option value="'+ result.equipsCategory[i].id +'">'+ Utils.XSSEncode(result.equipsCategory[i].code) +' - '+ Utils.XSSEncode(result.equipsCategory[i].name) +'</option>');  
				}
			} 
			$('#typeEquipment').change();
			// if(result.equipProviders != undefined && result.equipProviders != null && result.equipProviders.length > 0) {					
			// 	for(var i=0; i < result.equipProviders.length; i++){
			// 		$('#providerId').append('<option value="'+ result.equipProviders[i].id +'">'+ Utils.XSSEncode(result.equipProviders[i].name) +'</option>');  
			// 	}
			// } 
			// $('#providerId').change();	

			//tamvnm: doi thanh autoCompleteCombobox
			if(result.equipProviders != undefined && result.equipProviders != null && result.equipProviders.length > 0) {					
				for (var i = 0, isize = result.equipProviders.length; i < isize; i++) {
					result.equipProviders[i].searchText = unicodeToEnglish(result.equipProviders[i].code + " " + result.equipProviders[i].name);
 					result.equipProviders[i].searchText = result.equipProviders[i].searchText.toUpperCase();
				}
				var obj = {
					id: -1,
					code: "Tất cả",
					name: "Tất cả",
					codeName: "Tất cả",
					searchText: unicodeToEnglish("Tất cả").toUpperCase()
				};
				result.equipProviders.splice(0, 0, obj);
				$('#providerId').combobox("loadData", result.equipProviders);
		 		$('#providerId').combobox("setValue", -1);
			}			
		},null,null);
	},
	/**
	 * Lay nhom thiet bi
	 * @author nhutnn
	 * @since 12/01/2015
	 */
	getEquipGroup: function(typeEquip){
		var params = new Object();
		if(typeEquip != undefined && typeEquip != null && typeEquip != ''){
			params.id = typeEquip;
		}

		// $('#groupEquipment').html('<option value="" selected="selected">Tất cả</option>');
    	Utils.getJSONDataByAjaxNotOverlay(params, '/equipment-manage-liquidation/get-equipment-group', function(result){	
		// $.getJSON('/equipment-manage-liquidation/get-equipment-catalog', function(result) {				
			// if(result.equipGroups != undefined && result.equipGroups != null && result.equipGroups.length > 0) {					
			// 	for(var i=0; i < result.equipGroups.length; i++){
			// 		$('#groupEquipment').append('<option value="'+ result.equipGroups[i].id +'">'+ Utils.XSSEncode(result.equipGroups[i].code+' - '+result.equipGroups[i].name) +'</option>');  
			// 	}
			// } 
			// $('#groupEquipment').change();	
			//tamvnm: doi thanh autoCompleteCombobox
			if(result.equipGroups != undefined && result.equipGroups != null && result.equipGroups.length > 0) {	
				for (var i = 0, isize = result.equipGroups.length; i < isize; i++) {
					result.equipGroups[i].searchText = unicodeToEnglish(result.equipGroups[i].code + " " + result.equipGroups[i].name);
	 				result.equipGroups[i].searchText = result.equipGroups[i].searchText.toUpperCase();
				}				
				var obj = {
				id: -1,
				code: "Tất cả",
				name: "Tất cả",
				codeName: "Tất cả",
				searchText: unicodeToEnglish("Tất cả").toUpperCase()
				};
				result.equipGroups.splice(0, 0, obj);
				$('#groupEquipment').combobox("loadData", result.equipGroups);
		 		$('#groupEquipment').combobox("setValue", -1);
			} 			
		},null,null);
	},
	/**
	 * Xu ly trên input loai thiet bi
	 * @author nhutnn
	 * @since 19/12/2014
	 */
	onChangeTypeEquip: function(cb){
		var typeEquip = $(cb).val().trim();
		EquipmentManagerLiquidation.getEquipGroup(typeEquip);
		// $('#groupEquipment').html('<option value="" selected="selected">Tất cả</option>');
		//if(typeEquip != undefined && typeEquip != null && typeEquip != ''){
			// $.getJSON('/equipment-manage-liquidation/get-equipment-group?id='+typeEquip, function(result) {				
			// 	if(result.equipGroups != undefined && result.equipGroups != null && result.equipGroups.length > 0) {					
			// 		for(var i=0; i < result.equipGroups.length; i++){
			// 			$('#groupEquipment').append('<option value="'+ result.equipGroups[i].id +'">'+ Utils.XSSEncode(result.equipGroups[i].name) +'</option>');  
			// 		}
			// 	} 
			// 	$('#groupEquipment').change();				
			// });			
		//}
	},
	/**
	 * Chon thiet bi
	 * @author nhutnn
	 * @since 12/12/2014
	 */
	/*selectEquipment: function(index){
		var rowPopup = $('#equipmentGridDialog').datagrid('getRows')[index];
		if(EquipmentManagerLiquidation._editIndex!=null){			
			var valEquip = $("#gridEquipmentLiquidation").datagrid('getEditor', {index:EquipmentManagerLiquidation._editIndex,field:'valueEquip'});
			rowPopup.valueEquip = $(valEquip.target).val();
			$('#gridEquipmentLiquidation').datagrid('updateRow',{
				index: EquipmentManagerLiquidation._editIndex,	// index start with 0
				row: rowPopup
			});
			$('#gridEquipmentLiquidation').datagrid('beginEdit', EquipmentManagerLiquidation._editIndex);
			$('#equipmentCodeInGrid').width($('#equipmentCodeInGrid').parent().width());
			EquipmentManagerLiquidation.editInputInGrid();		
		}
		
		$('#easyuiPopupSearchEquipment').dialog('close');
	},*/
	/**
	 * Tim kiem thiet bi
	 * @author nhutnn
	 * @since 12/12/2014
	 * 
	 * @author hunglm16
	 * @sine JUNE 13,2015
	 * @description Ra soat noi dung phan quyen CMS
	 */
	searchEquipment: function(isSearch) {
//		var params=new Object(); 
//		params.equipCode = $('#equipmentCode').val().trim();
//		params.seriNumber = $('#seriNumber').val().trim();
//		params.categoryId = $('#typeEquipment').val().trim();
//		params.groupId = $('#groupEquipment').val().trim();
//		params.providerId = $('#providerId').val().trim();
//		params.yearManufacture = $('#yearManufacturing').val().trim();
//		params.stockCode = $('#stockCode').val().trim();
//		if($("#idRecordHidden").val() != "" && EquipmentManagerLiquidation._lstEquipInRecord != null){
//			params.lstEquipDelete = "";
//			for(var i=0; i<EquipmentManagerLiquidation._lstEquipInRecord.length; i++){
//				params.lstEquipDelete += EquipmentManagerLiquidation._lstEquipInRecord[i]+',';
//			}					
//		}
//		if(EquipmentManagerLiquidation._lstEquipInsert != null){
//			params.lstEquipAdd = "";
//			for(var i=0; i<EquipmentManagerLiquidation._lstEquipInsert.length; i++){
//				params.lstEquipAdd += EquipmentManagerLiquidation._lstEquipInsert[i]+',';
//			}					
//		}
		//tamvnm: thay doi thanh autoCompleteCombobox: -1 la truong hop tim group ko co trong BD
		var groupId = "";
		if ($('#groupEquipment').combobox('getValue') == "") {
			$('#groupEquipment').combobox('setValue', -1);
			groupId = "";
		} else if ($('#groupEquipment').combobox('getValue') == -1) {
			groupId = "";
		} else {
			if (Utils.getMessageOfInvaildNumberNew($('#groupEquipment').combobox("getValue")).length == 0) {
				groupId = $('#groupEquipment').combobox("getValue");
			} else {
				groupId = -1;
			}
		}
		var providerId = "";
		if ($('#providerId').combobox('getValue') == "") {
			$('#providerId').combobox('setValue', -1);
			providerId = "";
		} else if ($('#providerId').combobox('getValue') == -1) {
			providerId = "";
		} else {
			if (Utils.getMessageOfInvaildNumberNew($('#providerId').combobox("getValue")).length == 0) {
				providerId = $('#providerId').combobox("getValue");
			} else {
				providerId = -1;
			}
		}
		var params = {
				equipCode: $('#equipmentCode').val().trim(),
				seriNumber: $('#seriNumber').val().trim(),
				categoryId: $('#typeEquipment').val().trim(),
				// groupId: $('#groupEquipment').val().trim(),
				// providerId: $('#providerId').val().trim(),
				groupId: groupId,
				providerId: providerId,
				yearManufacture: $('#yearManufacturing').val().trim(),
				stockCode: $('#stockCode').val().trim(),
				status: activeType.RUNNING,
				//tradeStatus: 0,
				//stockType: 1, //Don vi;
				//usageStatus: usageStatus.SHOWING_WAREHOUSE,//Dang o kho
				flagPagination: true,
				shopCode: $('#shopCode').val().trim(),
				shortCode: $('#shortCode').val().trim(),
				isSearch: isSearch,
		};
		$('#equipmentGridDialog').datagrid('load', params);
	},
	/**
	 * kiem tra ki
	 * @author nhutnn
	 * @since 19/12/2014
	 */
	changeForm: function(id){
//		Utils.getJSONDataByAjaxNotOverlay({id:id},'/equipment-manage-liquidation/check-equip-period', function(dt){						
//			if(dt.error){
//				$('#errMsgInfo').html(dt.errMsg).show();
//				return false;
//			}else{
//				if(id > 0){
//					window.location.href="/equipment-manage-liquidation/change-record?id="+id;
//				}else{
//					window.location.href="/equipment-manage-liquidation/change-record";
//				}
//			}
//		}, null, null);	
		if(id > 0){
			window.location.href="/equipment-manage-liquidation/change-record?id="+id;
		}else{
			window.location.href="/equipment-manage-liquidation/change-record";
		}
	},

	/**
	 * Tao moi bien ban thanh ly
	 * @author nhutnn
	 * @since 07/01/2015
	 */

	 /**
	 * Tao moi bien ban thanh ly
	 * @author vuongmq
	 * @since 08/07/2015
	 */
	createLiquidation: function(){
		$('.ErrorMsgStyle').html('').hide();
		$('.SuccessMsgStyle').html('').hide();
		var msg ='';
		var err = '';
		if (msg.length == 0) {
			msg = Utils.getMessageOfRequireCheck('docNumber','Số công văn/Tờ trình thanh lý');
			err = '#docNumber';
		}
		if (msg.length == 0) {
			msg = Utils.getMessageOfSpecialCharactersValidate('docNumber','Số công văn/Tờ trình thanh lý',Utils._NAME_CUSTYPE);
			err = '#docNumber';
		}

		//tamvnm: them ngay lap
		var createDate = $('#createDate').val();
		if(msg.length == 0 && $('#createDate').val().trim().length > 0 && !Utils.isDate($('#createDate').val()) && $('#createDate').val() != '__/__/____' ){
			msg = 'Ngày biên bản không tồn tại hoặc không đúng định dạng dd/mm/yyyy.';
			err = '#createDate';
		}
		var curDate = ReportUtils.getCurrentDateString(); //Lay ngay hien tai sysDate
		if (msg.length == 0 && !Utils.compareDate($('#createDate').val().trim(), curDate)) {
			msg = 'Ngày biên bản không được sau ngày hiện tại. Vui lòng nhập lại';
			$('#createDate').focus();
		}
		
		// kiem tra tong gia tri thanh ly, thu hoi
		if ($('#checkLot').is(':checked')) {
			if ($('#tongThanhLy').val() != null && $('#tongThanhLy').val().trim().length > 0) {
				// kiem tra gia tri thanh ly			
				if (msg.length == 0) {
					msg = Utils.getMessageOfSpecialCharactersValidateEx('tongThanhLy', 'Tổng giá trị thanh lý', Utils._TF_NUMBER_COMMA);
					err = '#tongThanhLy';
				}
				if (msg.length == 0 && !isNaN($('#tongThanhLy').val().trim()) && Number($('#tongThanhLy').val()) < 0) {
					msg = 'Tổng giá trị thanh lý phải lớn hơn 0';
					err = '#tongThanhLy';
				}
				if (!EquipmentManagerLiquidation.validateTongGiaTriThanhLyThuHoi($('#tongThanhLy').val())) {
					msg = 'Tổng giá trị thanh lý phải nhỏ hơn hoặc bằng tổng nguyên giá';
					err = '#tongThanhLy';
				}
			}
			if ($('#tongThuHoi').val() != null && $('#tongThuHoi').val().trim().length > 0) {
				// kiem tra gia tri thu hoi
				if (msg.length == 0) {
					msg = Utils.getMessageOfSpecialCharactersValidateEx('tongThuHoi', 'Tổng giá trị thu hồi', Utils._TF_NUMBER_COMMA);
					err = '#tongThuHoi';
				}
				if (msg.length == 0 && !isNaN($('#tongThuHoi').val().trim()) && Number($('#tongThuHoi').val()) < 0) {
					msg = 'Tổng giá trị thu hồi phải lớn hơn 0';
					err = '#tongThuHoi';
				}
				if (!EquipmentManagerLiquidation.validateTongGiaTriThanhLyThuHoi($('#tongThuHoi').val())) {
					msg = 'Tổng giá trị thu hồi phải nhỏ hơn hoặc bằng tổng nguyên giá';
					err = '#tongThuHoi';
				}
			}
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfSpecialCharactersValidate('buyerName','Tên người mua', Utils._NAME);
			err = '#buyerName';
		}
		if (msg.length == 0) {
			msg = Utils.getMessageOfSpecialCharactersValidateEx('buyerPhone','Điện thoại', Utils._TF_PHONE_NUMBER);
			err = '#buyerPhone';
		}
		if (msg.length == 0) {
			msg = Utils.getMessageOfSpecialCharactersValidate('buyerAddress','Địa chỉ', Utils._ADDRESS);
			err = '#buyerAddress';
		}	
		if (msg.length == 0) {
			msg = Utils.getMessageOfSpecialCharactersValidate('reason','Lý do', Utils._SPECIAL);
			err = '#reason';
		}
		if (msg.length == 0) {
			msg = Utils.getMessageOfSpecialCharactersValidate('note','Ghi chú', Utils._SPECIAL);
			err = '#note';
		}
		if (msg.length == 0 && $('#note').attr('maxlength') > 500) {
			msg = 'Bạn nhập quá giới hạn của trường Ghi chú';
			err = '#note';
		}

		if (msg.length > 0) {
			$('#errMsgInfo').html(msg).show();
			$(err).focus();
			return false;
		}
		
		var rowEquip = $('#gridEquipmentLiquidation').datagrid('getRows');
		if (rowEquip.length == 0) {
			$('#errMsgInfo').html('Không có thiết bị để cập nhật').show();
			return false;
		}

		/*var lstEquipCode = new Array();
		var lstValueEquip = new Array();
		var nSize = rowEquip.length;
		if ($('#equipmentCodeInGrid').val() != undefined) {
			nSize = rowEquip.length-1;
		}
		for (var i = 0; i < nSize; i++) {
			lstEquipCode.push(rowEquip[i].equipmentCode);
			var valEquip = $("#gridEquipmentLiquidation").datagrid('getEditor', {index:i,field:'valueEquip'});
			if($(valEquip.target).val() == null || $(valEquip.target).val() == "" || $(valEquip.target).val() < 1){
				$('#errMsgInfo').html('Mã thiết bị '+rowEquip[i].equipmentCode+' chưa có giá trị hợp lệ!').show();				
				return;
			}else if(rowEquip[i].price != null && $(valEquip.target).val() != "" && $(valEquip.target).val() > rowEquip[i].price){
				$('#errMsgInfo').html('Mã thiết bị '+rowEquip[i].equipmentCode+' có giá trị phải nhỏ hơn nguyên giá!').show();				
				return;
			}
			lstValueEquip.push(Utils.returnMoneyValue($(valEquip.target).val()));			
		}*/
		
		if ($('#equipmentCodeInGrid').val() != undefined && $('#equipmentCodeInGrid').val() != '') {
			var rowEq = $('#gridEquipmentLiquidation').datagrid('getRows');
			for (var i = 0; i < rowEq.length-1; i++) {
				if(rowEq[i].equipmentCode == $('#equipmentCodeInGrid').val().trim()){
					$('#errMsgInfo').html("Mã thiết bị "+rowEq[i].equipmentCode+" đã tồn tại!").show();
					return false;
				}
			}
			//lstEquipCode.push($('#equipmentCodeInGrid').val().trim());
		} else if ($('#equipmentCodeInGrid').val() == '') {
			$('#errMsgInfo').html('Bạn chưa xử lý xong dòng thiết bị hiện tại!').show();
			return false;
		}
		if ($('#equipmentCodeInGrid').val() != undefined) {
			/*var valEquip = $("#gridEquipmentLiquidation").datagrid('getEditor', {index:rowEquip.length-1,field:'valueEquip'});
			if($(valEquip.target).val() == "" || $(valEquip.target).val() < 1){
				$('#errMsgInfo').html('Mã thiết bị '+$('#equipmentCodeInGrid').val()+' chưa có giá trị hợp lệ!').show();				
				return;
			}else if(rowEquip[rowEquip.length-1].price != null && $(valEquip.target).val() != "" && Number(Utils.returnMoneyValue($(valEquip.target).val())) > rowEquip[rowEquip.length-1].price){
				$('#errMsgInfo').html('Mã thiết bị '+rowEquip[i].equipmentCode+' có giá trị phải nhỏ hơn nguyên giá!').show();				
				return;
			}
			lstValueEquip.push(Utils.returnMoneyValue($(valEquip.target).val()));		*/
		}
		// them cac params vô de them moi
		

		var checkLot = activeType.STOPPED;
		var liquidationValue = activeType.STOPPED;
		var evictionValue = activeType.STOPPED;

		var lstEquipCode = new Array();
		var lstLiquidationValue = new Array();
		var lstEvictionValue = new Array();
		var nSize = rowEquip.length;
		if ($('#equipmentCodeInGrid').val() != undefined) {
			nSize = rowEquip.length-1;
		}

		if ($('#checkLot').is(':checked')) {
			checkLot = activeType.RUNNING;
			liquidationValue = Utils.returnMoneyValue($('#tongThanhLy').val());
			evictionValue = Utils.returnMoneyValue($('#tongThuHoi').val());

			for (var i = 0; i < nSize; i++) {
				lstEquipCode.push(rowEquip[i].equipmentCode);	
			}
		} else {
			// khong check lo thi moi them  lst danh sach cac gia tri thanh ly va thu hoi
			for (var i = 0; i < nSize; i++) {
				lstEquipCode.push(rowEquip[i].equipmentCode);
				//add gia tri thanh ly
				var valEquipTL = $("#gridEquipmentLiquidation").datagrid('getEditor', {index:i,field:'liquidationValue'});
				if (valEquipTL != undefined && valEquipTL != null) {
					var gtThanhLyTmp = $(valEquipTL.target).val();
					var giaTriThanhLy = Utils.returnMoneyValue($(valEquipTL.target).val());
					if (gtThanhLyTmp == null || gtThanhLyTmp.trim().length == 0 || giaTriThanhLy == null || giaTriThanhLy.length == 0) {
						giaTriThanhLy = -1;
					} else if (!isNaN(giaTriThanhLy) && giaTriThanhLy < 1) {
						$('#errMsgInfo').html('Mã thiết bị '+rowEquip[i].equipmentCode+' giá trị thanh lý phải lớn hơn 0!').show();
						$(valEquipTL.target).focus();			
						return false;
					} else if (rowEquip[i].price != null && giaTriThanhLy != "" && Number(giaTriThanhLy) > Number(rowEquip[i].price)) {
						$('#errMsgInfo').html('Mã thiết bị '+rowEquip[i].equipmentCode+' có giá trị thanh lý phải nhỏ hơn hoặc bằng nguyên giá!').show();				
						$(valEquipTL.target).focus();
						return false;
					}
					lstLiquidationValue.push(giaTriThanhLy);
				}
				//add gia tri thu hoi
				var valEquipTH = $("#gridEquipmentLiquidation").datagrid('getEditor', {index:i,field:'evictionValue'});
				if (valEquipTH != undefined && valEquipTH != null) {
					var gtThuHoiTmp = $(valEquipTH.target).val();
					var giaTriThuHoi = Utils.returnMoneyValue($(valEquipTH.target).val());
					if (gtThuHoiTmp == null || gtThuHoiTmp.trim().length == 0 || giaTriThuHoi == null || giaTriThuHoi.length == 0) {
						giaTriThuHoi = -1;
					} else if (!isNaN(giaTriThuHoi) && giaTriThuHoi < 1) {
						$('#errMsgInfo').html('Mã thiết bị '+rowEquip[i].equipmentCode+' giá trị thu hồi phải lớn hơn 0!').show();				
						$(valEquipTH.target).focus();
						return false;
					} else if (rowEquip[i].price != null && giaTriThuHoi != "" && Number(giaTriThuHoi) > Number(rowEquip[i].price)) {
						$('#errMsgInfo').html('Mã thiết bị '+rowEquip[i].equipmentCode+' có giá trị thu hồi phải nhỏ hơn hoặc bằng nguyên giá!').show();				
						$(valEquipTH.target).focus();
						return false;
					}
					lstEvictionValue.push(giaTriThuHoi);
				}	
			}
		}

		var params = new Object(); 
		params.docNumber = $('#docNumber').val().trim();
		params.buyerName = $('#buyerName').val().trim();
		params.buyerAddress = $('#buyerAddress').val().trim();
		params.buyerPhone = $('#buyerPhone').val().trim();
		params.reason = $('#reason').val().trim();
		params.status = $('#status').val().trim();
		params.createDate = createDate;
		params.note = $('#note').val();
		// them ma thiet bi vao neu nhap vao textbox thiet bi de validate tren server
		if ($('#equipmentCodeInGrid').val() != undefined && $('#equipmentCodeInGrid').val() != '' && $('#equipmentCodeInGrid').val().trim() != '') {
			lstEquipCode.push($('#equipmentCodeInGrid').val().trim());
		}
		// them các gia tri check lo
		params.checkLot = checkLot;
		params.liquidationValue = liquidationValue;
		params.evictionValue = evictionValue;
		if (lstEquipCode.length > 0) {
			params.lstEquipCode = lstEquipCode;
			params.lstLiquidationValue = lstLiquidationValue;			
			params.lstEvictionValue = lstEvictionValue;
		}

		//upload file
		var arrFile = DropzonePlugin._dropzoneObject.getFilesWithStatus(Dropzone.ADDED);
		if (EquipmentManagerLiquidation._arrFileDelete == undefined || EquipmentManagerLiquidation._arrFileDelete == null) {
			EquipmentManagerLiquidation._arrFileDelete = [];
		}
		if (arrFile != null && arrFile != undefined && arrFile.length > 0) {
			if (EquipmentManagerLiquidation._countFile == 5) {
				$('#errMsgInfo').html("Tổng số tập tin đính kèm không được vượt quá 5").show();
				return false;
			}
			var maxLength = 5 - EquipmentManagerLiquidation._countFile;
			if(maxLength<0){
				maxLength = 0;
			}
			if (arrFile.length > maxLength) {
				$('#errMsgInfo').html("Số tập tin thêm mới không được vượt quá " + maxLength).show();
				return false;
			}
			msg = UploadUtil.getMsgByFileRequireCheck(UploadUtil._typeFileRequireCheck.equipRecord);
			if (msg.trim().length > 0) {
				$('#errMsgInfo').html(msg).show();
				return false;
			}			
		}
		params.equipAttachFileStr= EquipmentManagerLiquidation._arrFileDelete.join(',');
		
//		Utils.getJSONDataByAjaxNotOverlay({id : -1}, '/equipment-manage-liquidation/check-equip-period', function (dt) {						
//			if (dt.error) {
//				$('#errMsgInfo').html(dt.errMsg).show();
//				return false;
//			} else {
//				$.messager.confirm('Xác nhận', 'Bạn có muốn lưu dữ liệu?', function (r) {
//					if (r) {
//						if (arrFile != null && arrFile != undefined && arrFile.length > 0) {
//							params = JSONUtil.getSimpleObject2(params);
//							UploadUtil.updateAdditionalDataForUpload(params);
//							UploadUtil.startUpload('errMsgInfo');
//							return false;
//						}					
//						Utils.saveData(params, '/equipment-manage-liquidation/create-liquidation', null, 'errMsgInfo', function (data) {	
//							if(data.error){
//								$('#errMsgInfo').html(data.errMsg).show();
//								return false;
//							}else{
//								if (data.lstRecordNotSendMail != undefined && data.lstRecordNotSendMail != null && data.lstRecordNotSendMail != '') {
//									$('#errMsgInfo').html('Gửi Email không thành công biên bản báo mất: ' + data.lstRecordNotSendMail).show();
//								}
//								$('#successMsgInfo').html("Cập dữ liệu thành công.").show();	
//								EquipmentManagerLiquidation._lstEquipInsert = null;
//								EquipmentManagerLiquidation._editIndex = null;				
//								setTimeout(function(){
//									$('#successMsgInfo').html("").hide(); 
//									window.location.href = '/equipment-manage-liquidation/info';						              		
//								}, 1500);
//							}
//						}, null, null, null, null);		
//					}
//				});
//			}
//		}, null, null);	
		$.messager.confirm('Xác nhận', 'Bạn có muốn lưu dữ liệu?', function (r) {
			if (r) {
				if (arrFile != null && arrFile != undefined && arrFile.length > 0) {
					params = JSONUtil.getSimpleObject2(params);
					UploadUtil.updateAdditionalDataForUpload(params);
					UploadUtil.startUpload('errMsgInfo');
					return false;
				}					
				Utils.saveData(params, '/equipment-manage-liquidation/create-liquidation', null, 'errMsgInfo', function (data) {	
					if(data.error){
						$('#errMsgInfo').html(data.errMsg).show();
						return false;
					}else{
						if (data.lstRecordNotSendMail != undefined && data.lstRecordNotSendMail != null && data.lstRecordNotSendMail != '') {
							$('#errMsgInfo').html('Gửi Email không thành công biên bản báo mất: ' + data.lstRecordNotSendMail).show();
						}
						$('#successMsgInfo').html("Cập dữ liệu thành công.").show();	
						EquipmentManagerLiquidation._lstEquipInsert = null;
						EquipmentManagerLiquidation._editIndex = null;				
						setTimeout(function(){
							$('#successMsgInfo').html("").hide(); 
							window.location.href = '/equipment-manage-liquidation/info';						              		
						}, 1500);
					}
				}, null, null, null, null);		
			}
		});
	},

	/**
	 * Chinh sua bien ban thanh ly
	 * @author nhutnn
	 * @since 07/01/2015
	 */

	 /**
	 * Chinh sua bien ban thanh ly
	 * @author vuongmq
	 * @since 08/07/2015
	 */
	updateLiquidation: function() {
		$('.ErrorMsgStyle').html('').hide();
		$('.SuccessMsgStyle').html('').hide();
		var msg ='';
		var err = '';
		
		if (msg.length == 0) {
			msg = Utils.getMessageOfRequireCheck('docNumber','Số công văn/Tờ trình thanh lý');
			err = '#docNumber';
		}
		if (msg.length == 0) {
			msg = Utils.getMessageOfSpecialCharactersValidate('docNumber','Số công văn/Tờ trình thanh lý',Utils._NAME_CUSTYPE);
			err = '#docNumber';
		}

		//tamvnm: them ngay lap
		var createDate = $('#createDate').val();
		if(msg.length == 0 && $('#createDate').val().trim().length > 0 && !Utils.isDate($('#createDate').val()) && $('#createDate').val() != '__/__/____' ){
			msg = 'Ngày biên bản không tồn tại hoặc không đúng định dạng dd/mm/yyyy.';
			err = '#createDate';
		}
		var curDate = ReportUtils.getCurrentDateString(); //Lay ngay hien tai sysDate
		if (msg.length == 0 && !Utils.compareDate($('#createDate').val().trim(), curDate)) {
			msg = 'Ngày biên bản không được sau ngày hiện tại. Vui lòng nhập lại';
			$('#createDate').focus();
		}

		// kiem tra tong gia tri thanh ly, thu hoi
		if ($('#checkLot').is(':checked')) {
			if ($('#tongThanhLy').val() != null && $('#tongThanhLy').val().trim().length > 0) {
				// kiem tra gia tri thanh ly			
				if (msg.length == 0) {
					msg = Utils.getMessageOfSpecialCharactersValidateEx('tongThanhLy', 'Tổng giá trị thanh lý', Utils._TF_NUMBER_COMMA);
					err = '#tongThanhLy';
				}
				if (msg.length == 0 && !isNaN($('#tongThanhLy').val().trim()) && Number($('#tongThanhLy').val()) < 0) {
					msg = 'Tổng giá trị thanh lý phải lớn hơn 0';
					err = '#tongThanhLy';
				}
				if (!EquipmentManagerLiquidation.validateTongGiaTriThanhLyThuHoi($('#tongThanhLy').val())) {
					msg = 'Tổng giá trị thanh lý phải nhỏ hơn hoặc bằng tổng nguyên giá';
					err = '#tongThanhLy';
				}
			}
			if ($('#tongThuHoi').val() != null && $('#tongThuHoi').val().trim().length > 0) {
				// kiem tra gia tri thu hoi
				if (msg.length == 0) {
					msg = Utils.getMessageOfSpecialCharactersValidateEx('tongThuHoi', 'Tổng giá trị thu hồi', Utils._TF_NUMBER_COMMA);
					err = '#tongThuHoi';
				}
				if (msg.length == 0 && !isNaN($('#tongThuHoi').val().trim()) && Number($('#tongThuHoi').val()) < 0) {
					msg = 'Tổng giá trị thu hồi phải lớn hơn 0';
					err = '#tongThuHoi';
				}
				if (!EquipmentManagerLiquidation.validateTongGiaTriThanhLyThuHoi($('#tongThuHoi').val())) {
					msg = 'Tổng giá trị thu hồi phải nhỏ hơn hoặc bằng tổng nguyên giá';
					err = '#tongThuHoi';
				}
			}
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfSpecialCharactersValidate('buyerName','Tên người mua', Utils._NAME);
			err = '#buyerName';
		}
		if (msg.length == 0) {
			msg = Utils.getMessageOfSpecialCharactersValidateEx('buyerPhone','Điện thoại', Utils._TF_PHONE_NUMBER);
			err = '#buyerPhone';
		}
		if (msg.length == 0){
			msg = Utils.getMessageOfSpecialCharactersValidate('buyerAddress','Địa chỉ', Utils._ADDRESS);
			err = '#buyerAddress';
		}
		if (msg.length == 0) {
			msg = Utils.getMessageOfSpecialCharactersValidate('reason','Lý do', Utils._SPECIAL);
			err = '#reason';
		}
		if (msg.length == 0) {
			msg = Utils.getMessageOfSpecialCharactersValidate('note','Ghi chú', Utils._SPECIAL);
			err = '#note';
		}
		if (msg.length == 0 && $('#note').attr('maxlength') > 500) {
			msg = 'Bạn nhập quá giới hạn của trường Ghi chú';
			err = '#note';
		}

		if (msg.length > 0) {
			$('#errMsgInfo').html(msg).show();
			$(err).focus();
			return false;
		}
		
		var rowEquip = $('#gridEquipmentLiquidation').datagrid('getRows');
		if (rowEquip.length == 0) {
			$('#errMsgInfo').html('Không có thiết bị để cập nhật').show();
			return false;
		}

		/*var lstEquipCode = new Array();
		var lstValueEquip = new Array();
		var nSize = rowEquip.length;
		if($('#equipmentCodeInGrid').val() != undefined){
			nSize = rowEquip.length-1;
		}
		for(var i=0; i<nSize; i++){
			lstEquipCode.push(rowEquip[i].equipmentCode);
			var valEquip = $("#gridEquipmentLiquidation").datagrid('getEditor', {index:i,field:'valueEquip'});
			if($(valEquip.target).val() == "" || $(valEquip.target).val() < 1){
				$('#errMsgInfo').html('Mã thiết bị '+rowEquip[i].equipmentCode+' chưa có giá trị hợp lệ!').show();				
				return;
			}else if(rowEquip[i].price != null && $(valEquip.target).val() != "" && Number(Utils.returnMoneyValue($(valEquip.target).val())) > rowEquip[i].price){
				$('#errMsgInfo').html('Mã thiết bị '+rowEquip[i].equipmentCode+' có giá trị phải nhỏ hơn nguyên giá!').show();				
				return;
			}
			lstValueEquip.push(Utils.returnMoneyValue($(valEquip.target).val()));			
		}*/
		
		if ($('#equipmentCodeInGrid').val() != undefined && $('#equipmentCodeInGrid').val() != '') {
			var rowEq = $('#gridEquipmentLiquidation').datagrid('getRows');
			for (var i = 0; i < rowEq.length-1; i++) {
				if (rowEq[i].equipmentCode == $('#equipmentCodeInGrid').val().trim()) {
					$('#errMsgInfo').html("Mã thiết bị "+rowEq[i].equipmentCode+" đã tồn tại!").show();
					return false;
				}
			}
			//lstEquipCode.push($('#equipmentCodeInGrid').val());
		} else if ($('#equipmentCodeInGrid').val() == '') {
			$('#errMsgInfo').html('Bạn chưa xử lý xong dòng thiết bị hiện tại!').show();
			return false;
		}
		if ($('#equipmentCodeInGrid').val() != undefined) {
			/*var valEquip = $("#gridEquipmentLiquidation").datagrid('getEditor', {index:rowEquip.length-1,field:'valueEquip'});
			if($(valEquip.target).val() == "" || $(valEquip.target).val() < 1){
				$('#errMsgInfo').html('Mã thiết bị '+$('#equipmentCodeInGrid').val()+' chưa có giá trị hợp lệ!').show();				
				return;
			}else if(rowEquip[rowEquip.length-1].price != null && $(valEquip.target).val() != "" && $(valEquip.target).val() > rowEquip[rowEquip.length-1].price){
				$('#errMsgInfo').html('Mã thiết bị '+rowEquip[i].equipmentCode+' có giá trị phải nhỏ hơn nguyên giá!').show();				
				return;
			}
			lstValueEquip.push(Utils.returnMoneyValue($(valEquip.target).val()));		*/
		}
		
		var checkLot = activeType.STOPPED;
		var liquidationValue = activeType.STOPPED;
		var evictionValue = activeType.STOPPED;

		// update author nhutnn, 06/08/2015
		var validationValueTL = function (i, rowEquip, giaTriThanhLy){
			if (giaTriThanhLy != null && giaTriThanhLy != undefined && !isNaN(giaTriThanhLy) && giaTriThanhLy < 1) {
				$('#errMsgInfo').html('Mã thiết bị '+rowEquip[i].equipmentCode+' giá trị thanh lý phải lớn hơn 0!').show();
				$(valEquipTL.target).focus();			
				return false;
			} else if (rowEquip[i].price != null && giaTriThanhLy != "" && Number(giaTriThanhLy) > Number(rowEquip[i].price)) {
				$('#errMsgInfo').html('Mã thiết bị '+rowEquip[i].equipmentCode+' có giá trị thanh lý phải nhỏ hơn hoặc bằng nguyên giá!').show();				
				$(valEquipTL.target).focus();
				return false;
			}
		}
		var validationValueTH = function (i, rowEquip, giaTriThuHoi){
			if (giaTriThuHoi != null && giaTriThuHoi != null && !isNaN(giaTriThuHoi) && giaTriThuHoi < 1) {
				$('#errMsgInfo').html('Mã thiết bị '+rowEquip[i].equipmentCode+' giá trị thu hồi phải lớn hơn 0!').show();				
				$(valEquipTH.target).focus();
				return false;
			} else if (rowEquip[i].price != null && giaTriThuHoi != "" && Number(giaTriThuHoi) > Number(rowEquip[i].price)) {
				$('#errMsgInfo').html('Mã thiết bị '+rowEquip[i].equipmentCode+' có giá trị thu hồi phải nhỏ hơn hoặc bằng nguyên giá!').show();				
				$(valEquipTH.target).focus();
				return false;
			}
		}

		var lstEquipCode = new Array();
		var lstLiquidationValue = new Array();
		var lstEvictionValue = new Array();
		var nSize = rowEquip.length;
		if ($('#equipmentCodeInGrid').val() != undefined) {
			nSize = rowEquip.length-1;
		}
		if ($('#checkLot').is(':checked')) {
			checkLot = activeType.RUNNING;
			liquidationValue = Utils.returnMoneyValue($('#tongThanhLy').val());
			evictionValue = Utils.returnMoneyValue($('#tongThuHoi').val());
			
			for (var i = 0; i < nSize; i++) {
				lstEquipCode.push(rowEquip[i].equipmentCode);				
			}
		} else {
			// khong check lo thi moi them  lst danh sach cac gia tri thanh ly va thu hoi
			for (var i = 0; i < nSize; i++) {
				lstEquipCode.push(rowEquip[i].equipmentCode);
				//add gia tri thanh ly
				var valEquipTL = $("#gridEquipmentLiquidation").datagrid('getEditor', {index:i,field:'liquidationValue'});
				if (valEquipTL != undefined && valEquipTL != null) {
					var gtThanhLyTmp = $(valEquipTL.target).val();
					var giaTriThanhLy = Utils.returnMoneyValue($(valEquipTL.target).val());
					if (gtThanhLyTmp == null || gtThanhLyTmp.trim().length == 0 || giaTriThanhLy == null || giaTriThanhLy.length == 0) {
						giaTriThanhLy = -1;
					} else if (!isNaN(giaTriThanhLy) && giaTriThanhLy < 1) {
						$('#errMsgInfo').html('Mã thiết bị ' + rowEquip[i].equipmentCode + ' giá trị thanh lý phải lớn hơn 0!').show();
						$(valEquipTL.target).focus();
						return false;
					} else if (rowEquip[i].price != null && giaTriThanhLy != "" && Number(giaTriThanhLy) > Number(rowEquip[i].price)) {
						$('#errMsgInfo').html('Mã thiết bị ' + rowEquip[i].equipmentCode + ' có giá trị thanh lý phải nhỏ hơn hoặc bằng nguyên giá!').show();
						$(valEquipTL.target).focus();
						return false;
					}
					lstLiquidationValue.push(giaTriThanhLy);
				} else {
					var gtThanhLyTmp = rowEquip[i].liquidationValue;
					var giaTriThanhLy = Utils.returnMoneyValue(rowEquip[i].liquidationValue);
					if (gtThanhLyTmp == null || gtThanhLyTmp.trim().length == 0 || giaTriThanhLy == null || giaTriThanhLy.length == 0) {
						giaTriThanhLy = -1;
					} else if (!isNaN(giaTriThanhLy) && giaTriThanhLy < 1) {
						$('#errMsgInfo').html('Mã thiết bị ' + rowEquip[i].equipmentCode + ' giá trị thanh lý phải lớn hơn 0!').show();
						$(valEquipTL.target).focus();
						return false;
					} else if (rowEquip[i].price != null && giaTriThanhLy != "" && Number(giaTriThanhLy) > Number(rowEquip[i].price)) {
						$('#errMsgInfo').html('Mã thiết bị ' + rowEquip[i].equipmentCode + ' có giá trị thanh lý phải nhỏ hơn hoặc bằng nguyên giá!').show();
						$(valEquipTL.target).focus();
						return false;
					}
					lstLiquidationValue.push(giaTriThanhLy);
				}
				//add gia tri thu hoi
				var valEquipTH = $("#gridEquipmentLiquidation").datagrid('getEditor', {index:i,field:'evictionValue'});
				if (valEquipTH != undefined && valEquipTH != null) {
					var gtThuHoiTmp = $(valEquipTH.target).val();
					var giaTriThuHoi = Utils.returnMoneyValue($(valEquipTH.target).val());
					if (gtThuHoiTmp == null || gtThuHoiTmp.trim().length == 0 || giaTriThuHoi == null || giaTriThuHoi.length == 0) {
						giaTriThuHoi = -1;
					} else if (!isNaN(giaTriThuHoi) && giaTriThuHoi < 1) {
						$('#errMsgInfo').html('Mã thiết bị '+rowEquip[i].equipmentCode+' giá trị thu hồi phải lớn hơn 0!').show();				
						$(valEquipTH.target).focus();
						return false;
					} else if (rowEquip[i].price != null && giaTriThuHoi != "" && Number(giaTriThuHoi) > Number(rowEquip[i].price)) {
						$('#errMsgInfo').html('Mã thiết bị '+rowEquip[i].equipmentCode+' có giá trị thu hồi phải nhỏ hơn hoặc bằng nguyên giá!').show();				
						$(valEquipTH.target).focus();
						return false;
					}
					lstEvictionValue.push(giaTriThuHoi);
				} else {
					var gtThuHoiTmp = rowEquip[i].evictionValue;
					var giaTriThuHoi = Utils.returnMoneyValue(rowEquip[i].evictionValue);
					if (gtThuHoiTmp == null || gtThuHoiTmp.trim().length == 0 || giaTriThuHoi == null || giaTriThuHoi.length == 0) {
						giaTriThuHoi = -1;
					} else if (!isNaN(giaTriThuHoi) && giaTriThuHoi < 1) {
						$('#errMsgInfo').html('Mã thiết bị '+rowEquip[i].equipmentCode+' giá trị thu hồi phải lớn hơn 0!').show();				
						$(valEquipTH.target).focus();
						return false;
					} else if (rowEquip[i].price != null && giaTriThuHoi != "" && Number(giaTriThuHoi) > Number(rowEquip[i].price)) {
						$('#errMsgInfo').html('Mã thiết bị '+rowEquip[i].equipmentCode+' có giá trị thu hồi phải nhỏ hơn hoặc bằng nguyên giá!').show();				
						$(valEquipTH.target).focus();
						return false;
					}
					lstEvictionValue.push(giaTriThuHoi);
				}
			}
		}

		var params = new Object(); 
		params.idLiquidation = $('#idRecordHidden').val().trim();
		params.docNumber = $('#docNumber').val().trim();
		params.buyerName = $('#buyerName').val().trim();
		params.buyerAddress = $('#buyerAddress').val().trim();
		params.buyerPhone = $('#buyerPhone').val().trim();
		params.reason = $('#reason').val().trim();
		params.status = $('#status').val().trim();
		params.createDate = createDate;
		params.note = $('#note').val();

		// them ma thiet bi vao neu nhap vao textbox thiet bi de validate tren server
		if ($('#equipmentCodeInGrid').val() != undefined && $('#equipmentCodeInGrid').val() != '' && $('#equipmentCodeInGrid').val().trim() != '') {
			lstEquipCode.push($('#equipmentCodeInGrid').val().trim());
			// update author nhutnn, 06/08/2015
			//add gia tri thanh ly
			var valEquipTL = $("#gridEquipmentLiquidation").datagrid('getEditor', {index:i,field:'liquidationValue'});
			if (valEquipTL != undefined && valEquipTL != null) {
				var giaTriThanhLy = Utils.returnMoneyValue($(valEquipTL.target).val());
				validationValueTL(i,rowEquip, giaTriThanhLy);
				lstLiquidationValue.push(giaTriThanhLy);
			} else {
				var giaTriThanhLy = Utils.returnMoneyValue(rowEquip[i].liquidationValue);
				validationValueTL(i,rowEquip, giaTriThanhLy);
				lstLiquidationValue.push(giaTriThanhLy);
			}
			//add gia tri thu hoi
			var valEquipTH = $("#gridEquipmentLiquidation").datagrid('getEditor', {index:i,field:'evictionValue'});
			if (valEquipTH != undefined && valEquipTH != null) {
				var giaTriThuHoi = Utils.returnMoneyValue($(valEquipTH.target).val());
				validationValueTH(i,rowEquip, giaTriThuHoi);
				lstEvictionValue.push(giaTriThuHoi);
			} else {
				var giaTriThuHoi = Utils.returnMoneyValue(rowEquip[i].evictionValue);
				validationValueTH(i,rowEquip, giaTriThuHoi);
				lstEvictionValue.push(giaTriThuHoi);
			}
		}
		// them các gia tri check lo
		params.checkLot = checkLot;
		params.liquidationValue = liquidationValue;
		params.evictionValue = evictionValue;
		if (lstEquipCode.length > 0) {
			params.lstEquipCode = lstEquipCode;
			params.lstLiquidationValue = lstLiquidationValue;			
			params.lstEvictionValue = lstEvictionValue;
		}
		
		// if($("#idRecordHidden").val() != "" && EquipmentManagerLiquidation._lstEquipInRecord != null){
		// 	params.lstEquipDelete = "";
		// 	for(var i=0; i<EquipmentManagerLiquidation._lstEquipInRecord.length; i++){
		// 		params.lstEquipDelete+=EquipmentManagerLiquidation._lstEquipInRecord[i]+',';
		// 	}
		// }

		//upload file
		var arrFile = DropzonePlugin._dropzoneObject.getFilesWithStatus(Dropzone.ADDED);
		if (EquipmentManagerLiquidation._arrFileDelete == undefined || EquipmentManagerLiquidation._arrFileDelete == null) {
			EquipmentManagerLiquidation._arrFileDelete = [];
		}
		if (arrFile != null && arrFile != undefined && arrFile.length > 0) {
			if (EquipmentManagerLiquidation._countFile == 5) {
				$('#errMsgInfo').html("Tổng số tập tin đính kèm không được vượt quá 5").show();
				return false;
			}
			var maxLength = 5 - EquipmentManagerLiquidation._countFile;
			if(maxLength<0){
				maxLength = 0;
			}
			if (arrFile.length > maxLength) {
				$('#errMsgInfo').html("Số tập tin thêm mới không được vượt quá " + maxLength).show();
				return false;
			}
			msg = UploadUtil.getMsgByFileRequireCheck(UploadUtil._typeFileRequireCheck.equipRecord);
			if (msg.trim().length > 0) {
				$('#errMsgInfo').html(msg).show();
				return false;
			}			
		}
		params.equipAttachFileStr= EquipmentManagerLiquidation._arrFileDelete.join(',');
//		Utils.getJSONDataByAjaxNotOverlay({id:$('#idRecordHidden').val().trim()}, '/equipment-manage-liquidation/check-equip-period', function (dt) {						
//			if (dt.error) {
//				$('#errMsgInfo').html(dt.errMsg).show();
//				return false;
//			} else {
//				$.messager.confirm('Xác nhận', "Bạn có muốn cập nhật biên bản này ?", function(r) {
//					if (r) {
//						if (arrFile != null && arrFile != undefined && arrFile.length > 0) {
//							params = JSONUtil.getSimpleObject2(params);
//							UploadUtil.updateAdditionalDataForUpload(params);
//							UploadUtil.startUpload('errMsgInfo');
//							return false;
//						}	
//						Utils.saveData(params, '/equipment-manage-liquidation/update-liquidation', null, 'errMsgInfo', function(data){	
//							if (data.error) {
//								$('#errMsgInfo').html(data.errMsg).show();
//								return false;
//							} else {
//								if (data.lstRecordNotSendMail != undefined && data.lstRecordNotSendMail != null && data.lstRecordNotSendMail != '') {
//									$('#errMsgInfo').html('Gửi Email không thành công biên bản báo mất: ' + data.lstRecordNotSendMail).show();
//								}
//								$('#successMsgInfo').html("Cập dữ liệu thành công.").show();
//								EquipmentManagerLiquidation._lstEquipInsert = null;
//								EquipmentManagerLiquidation._editIndex = null;
//								EquipmentManagerLiquidation._lstEquipInRecord = null;
//								EquipmentManagerLiquidation._numberEquipInRecord = null;
//								if($('#status').val() == statusRecordsEquip.DT){					
//									setTimeout(function(){
//										$('#successMsgInfo').html("").hide(); 
//										window.location.href = '/equipment-manage-liquidation/change-record?id='+ $("#idRecordHidden").val().trim();						
//									}, 3000);
//								}else{
//									setTimeout(function(){
//										window.location.href = '/equipment-manage-liquidation/info';						              		
//									}, 3000);
//								}
//							}
//						}, null, null, null, null);	
//					} else {
//						if ($("#idRecordHidden").val()!='') {
//							$('#equipmentCodeInGrid').remove();
//							$('#gridEquipmentLiquidation').datagrid({url : '/equipment-manage-liquidation/search-equipment-in-form',queryParams:{idLiquidation:$("#idRecordHidden").val()}});
//							EquipmentManagerLiquidation._editIndex = null;
//							EquipmentManagerLiquidation._lstEquipInRecord = null;
//							EquipmentManagerLiquidation._lstEquipInsert = null;
//						}
//					}
//				});
//			}
//		}, null, null);		
		$.messager.confirm('Xác nhận', "Bạn có muốn cập nhật biên bản này ?", function(r) {
			if (r) {
				if (arrFile != null && arrFile != undefined && arrFile.length > 0) {
					params = JSONUtil.getSimpleObject2(params);
					UploadUtil.updateAdditionalDataForUpload(params);
					UploadUtil.startUpload('errMsgInfo');
					return false;
				}	
				Utils.saveData(params, '/equipment-manage-liquidation/update-liquidation', null, 'errMsgInfo', function(data){	
					if (data.error) {
						$('#errMsgInfo').html(data.errMsg).show();
						return false;
					} else {
						if (data.lstRecordNotSendMail != undefined && data.lstRecordNotSendMail != null && data.lstRecordNotSendMail != '') {
							$('#errMsgInfo').html('Gửi Email không thành công biên bản báo mất: ' + data.lstRecordNotSendMail).show();
						}
						$('#successMsgInfo').html("Cập dữ liệu thành công.").show();
						EquipmentManagerLiquidation._lstEquipInsert = null;
						EquipmentManagerLiquidation._editIndex = null;
						EquipmentManagerLiquidation._lstEquipInRecord = null;
						EquipmentManagerLiquidation._numberEquipInRecord = null;
						if($('#status').val() == statusRecordsEquip.DT){					
							setTimeout(function(){
								$('#successMsgInfo').html("").hide(); 
								window.location.href = '/equipment-manage-liquidation/change-record?id='+ $("#idRecordHidden").val().trim();						
							}, 3000);
						}else{
							setTimeout(function(){
								window.location.href = '/equipment-manage-liquidation/info';						              		
							}, 3000);
						}
					}
				}, null, null, null, null);	
			} else {
				if ($("#idRecordHidden").val()!='') {
					$('#equipmentCodeInGrid').remove();
					$('#gridEquipmentLiquidation').datagrid({url : '/equipment-manage-liquidation/search-equipment-in-form',queryParams:{idLiquidation:$("#idRecordHidden").val()}});
					EquipmentManagerLiquidation._editIndex = null;
					EquipmentManagerLiquidation._lstEquipInRecord = null;
					EquipmentManagerLiquidation._lstEquipInsert = null;
				}
			}
		});
	},
	/**
	 * su kien tren grid
	 * @author nhutnn
	 * @since 18/12/2014
	 */
	editInputInGrid: function(){
		$('#equipmentCodeInGrid').bind('keyup', function(e){
			if(e.keyCode==keyCode_F9){
				EquipmentManagerLiquidation.showPopupSearchEquipment();
			}
		});

		var enterHandler = function(e) {
			if(e.keyCode==keyCodes.ENTER){
				$('#errMsgInfo').html('').hide();
				var equipCode = $('#equipmentCodeInGrid').val().trim();				
				if(equipCode != undefined && equipCode != null && equipCode != ''){
					var rowEq = $('#gridEquipmentLiquidation').datagrid('getRows');
					for(var i=0; i < rowEq.length-1; i++){
						if(rowEq[i].equipmentCode == $('#equipmentCodeInGrid').val().trim()){
							$('#errMsgInfo').html("Mã thiết bị "+rowEq[i].equipmentCode+" đã tồn tại!").show();
							return false;
						}
					}
					var params = {equipCode:equipCode};
					if($("#idRecordHidden").val() != "" && EquipmentManagerLiquidation._lstEquipInRecord != null){
						params.lstEquipDelete = "";
						for(var i=0; i<EquipmentManagerLiquidation._lstEquipInRecord.length; i++){
							params.lstEquipDelete += EquipmentManagerLiquidation._lstEquipInRecord[i]+',';
						}					
					}
					if(EquipmentManagerLiquidation._lstEquipInsert != null){
						params.lstEquipAdd = "";
						for(var i=0; i<EquipmentManagerLiquidation._lstEquipInsert.length; i++){
							params.lstEquipAdd += EquipmentManagerLiquidation._lstEquipInsert[i]+',';
						}					
					}
					Utils.getJSONDataByAjaxNotOverlay(params, '/equipment-manage-liquidation/getEquipment', function(equip){						
						if(equip != undefined && equip != null && equip.length == 1) {
							var valEquipTL = $("#gridEquipmentLiquidation").datagrid('getEditor', {index:EquipmentManagerLiquidation._editIndex,field:'liquidationValue'});
							if (valEquipTL != undefined) {
								equip[0].liquidationValue = $(valEquipTL.target).val();
							}
							var valEquipTH = $("#gridEquipmentLiquidation").datagrid('getEditor', {index:EquipmentManagerLiquidation._editIndex,field:'evictionValue'});
							if (valEquipTH != undefined) {
								equip[0].evictionValue = $(valEquipTH.target).val();
							}
							$('#gridEquipmentLiquidation').datagrid('updateRow',{
								index: EquipmentManagerLiquidation._editIndex,	
								row: equip[0]
							});
							$('#gridEquipmentLiquidation').datagrid('beginEdit', EquipmentManagerLiquidation._editIndex);
							$('#equipmentCodeInGrid').width($('#equipmentCodeInGrid').parent().width());
							EquipmentManagerLiquidation.editInputInGrid();
							EquipmentManagerLiquidation.insertEquipmentInGrid();
							// kiem tra khi them mot dong thiet bi, nhu cho change checkLot
							if ($('#checkLot').is(':checked')) {
								EquipmentManagerLiquidation.checkGiaTriThanhLyThuHoi(EquipmentManagerLiquidation._DISABLED);
							} else {
								EquipmentManagerLiquidation.checkGiaTriThanhLyThuHoi(EquipmentManagerLiquidation._ENABLE);
							}
						} else{
							$('#errMsgInfo').html("Mã thiết bị " + Utils.XSSEncode($('#equipmentCodeInGrid').val()) + " không tồn tại hoặc không được phân quyền kho!").show();
							$('#equipmentCodeInGrid').val('');
						}
						$('#equipmentCodeInGrid').change();	
					}, null, null);
				}									
			}
		};

		$('#equipmentCodeInGrid').bind('keyup', function(e){
			enterHandler(e);
		});
		if (EquipmentManagerLiquidation._editIndex != null) {
			/*var valEquip = $("#gridEquipmentLiquidation").datagrid('getEditor', {index:EquipmentManagerLiquidation._editIndex,field:'valueEquip'});
			if(valEquip!=null){
				$(valEquip.target).bind('keydown', function(e){
					if(e.keyCode==110){
						e.preventDefault();
					}
					enterHandler(e);
				});
			}*/
			// bin su kien change cho textbox thanh ly
			var valEquipTL = $("#gridEquipmentLiquidation").datagrid('getEditor', {index:EquipmentManagerLiquidation._editIndex,field:'liquidationValue'});
			if (valEquipTL != null) {
				$(valEquipTL.target).unbind('change');
				Utils.bindFormatOntextfieldCurrencyFor(Utils._TF_NUMBER_DOT);
				$(valEquipTL.target).bind('change', function(e){
					//e.preventDefault();
					//enterHandler(e);
					$('.SuccessMsgStyle').html('').hide();
					$('.ErrorMsgStyle').html('').hide();
					var row = $("#gridEquipmentLiquidation").datagrid('getRows')[EquipmentManagerLiquidation._editIndex];
					// truong hop da them dong vao
					if (row != null && (row.equipmentCode == null || row.equipmentCode == '')) {
						if ($('#equipmentCodeInGrid').val() == undefined || $('#equipmentCodeInGrid').val() == '') {
							$('#errMsgInfo').html('Bạn chưa nhập Mã thiết bị').show();
							return false;
						}
					}					
					if (row != null && row.price != null) {
						if (Number($(this).val()) > Number(row.price)) {
							$('#errMsgInfo').html('Giá trị thanh lý không được lớn hơn nguyên giá').show();
							return false;
						}
					}					
					EquipmentManagerLiquidation.refreshFooter();
				});
				// update author nhutnn, 06/08/2015
				$(valEquipTL.target).attr('id', 'inputTL' + EquipmentManagerLiquidation._editIndex).attr('maxlength', '22');
				Utils.bindFormatOnTextfield('inputTL' + EquipmentManagerLiquidation._editIndex, Utils._TF_NUMBER);
				Utils.formatCurrencyFor('inputTL' + EquipmentManagerLiquidation._editIndex);
			}
			// bin su kien change cho textbox thu hoi
			var valEquipTH = $("#gridEquipmentLiquidation").datagrid('getEditor', {index:EquipmentManagerLiquidation._editIndex,field:'evictionValue'});
			if (valEquipTH != null) {
				$(valEquipTH.target).unbind('change');
				Utils.bindFormatOntextfieldCurrencyFor(Utils._TF_NUMBER_DOT);
				$(valEquipTH.target).bind('change', function(e){
					//e.preventDefault();
					//enterHandler(e);
					$('.SuccessMsgStyle').html('').hide();
					$('.ErrorMsgStyle').html('').hide();
					var row = $("#gridEquipmentLiquidation").datagrid('getRows')[EquipmentManagerLiquidation._editIndex];
					// truong hop da them dong vao
					if (row != null && (row.equipmentCode == null || row.equipmentCode == '')) {
						if ($('#equipmentCodeInGrid').val() == undefined || $('#equipmentCodeInGrid').val() == '') {
							$('#errMsgInfo').html('Bạn chưa nhập Mã thiết bị').show();
							return false;
						}
					}
					if (row != null && row.price != null) {
						if (Number($(this).val()) > Number(row.price)) {
							$('#errMsgInfo').html('Giá trị thu hồi không được lớn hơn nguyên giá').show();
							return false;
						}
					}
					EquipmentManagerLiquidation.refreshFooter();
				});
				$(valEquipTH.target).attr('id', 'inputTH' + EquipmentManagerLiquidation._editIndex).attr('maxlength', '22');
				Utils.bindFormatOnTextfield('inputTH' + EquipmentManagerLiquidation._editIndex, Utils._TF_NUMBER);
				Utils.formatCurrencyFor('inputTH' + EquipmentManagerLiquidation._editIndex);
			}
			
		}		
	},
	/**
	 * su kien tren grid
	 * @author nhutnn
	 * @since 18/12/2014
	 */
	viewInfoFormLiquidation: function(id) {
		Utils.getHtmlDataByAjax({id:id, isView:1}, '/equipment-manage-liquidation/change-record', function(html){						
			$('#easyuiPopupViewInfo').html(html);
			$('#easyuiPopupViewInfo').show();
			$('#easyuiPopupViewInfo').dialog({  
		        closed: false,  
		        cache: false,  
		        modal: true,
		        width : 1200,
		        height : 'auto',
		        onOpen: function(){
		        	$('#statusView').customStyle();
					$('#gridEquipmentLiquidation').datagrid({
						url : '/equipment-manage-liquidation/search-equipment-in-form',
						autoRowHeight : true,
						rownumbers : true, 
						singleSelect: true,
						//pagination:true,
						fitColumns:true,
						// pageSize :20,
						// pageList  : [20],
						scrollbarSize:0,
						width: $('#gridContainerEquip').width(),
						autoWidth: true,	
						queryParams:{
							idLiquidation: id
						},
						columns:[[		  
							{field: 'typeEquipment',title:'Loại thiết bị', width: 80, sortable:false,resizable:false, align: 'left', formatter: function(v, r, i){
								  return Utils.XSSEncode(r.typeEquipment);		
							}},
							{field: 'groupEquipment',title:'Nhóm thiết bị', width: 220, sortable:false,resizable:false, align: 'left', formatter: function(value, row, index) {
						    	var html="";
						    	if(row.groupEquipmentCode != null && row.groupEquipmentName!= null){
									html = row.groupEquipmentCode+' - '+row.groupEquipmentName; 
						    	}
								return Utils.XSSEncode(html);
							}},			
							{field: 'equipmentCode',title:'Mã thiết bị',width: 220,sortable:false,resizable:false, align: 'left',formatter: function(v, r, i){
								  return Utils.XSSEncode(r.equipmentCode);		
							}},
							{field: 'seriNumber',title:'Số serial',width: 120,sortable:false,resizable:false, align: 'left',formatter: function(v, r, i){
								  return Utils.XSSEncode(r.seriNumber);		
							}},
							{field: 'price',title:'Nguyên giá',width: 150,sortable:false,resizable:false, align: 'right',formatter: function(v, r, i){
								  return formatCurrency(r.price);		
							}},
							/*{field: 'valueEquip',title:'Giá trị',width: 100,sortable:false,resizable:false, align: 'left',formatter: function(v, r, i){
								  return formatCurrency(r.valueEquip);		
							}},*/
							{field: 'liquidationValue',title:'Giá trị thanh lý',width: 100,sortable:false,resizable:false, align: 'right',formatter: function(v, r, i){
								  return formatCurrency(r.liquidationValue);		
							}},
							{field: 'evictionValue',title:'Giá trị',width: 100,sortable:false,resizable:false, align: 'right',formatter: function(v, r, i){
								  return formatCurrency(r.evictionValue);		
							}},
						    {field: 'healthStatus', title: 'Tình trạng thiết bị', width: 100, sortable:false,resizable:false,align: 'left',formatter: function(v, r, i){
								  return Utils.XSSEncode(r.healthStatus);		
							}},
							{field: 'yearManufacture', title: 'Năm sản xuất', width: 60, sortable:false,resizable:false,align: 'right',formatter: function(v, r, i){
								  return Utils.XSSEncode(r.yearManufacture);		
							}},
						]],
						onLoadSuccess :function(data){   			 	    	
					    	$(window).resize();	
					    	if (data == null || data.total == 0) {
								//$("#errMsgInfo").html("Không tìm thấy danh sách biên bản giao nhận thiết bị!").show();
							}else{
								$('.datagrid-header-rownumber').html('STT');					
							}
							// view footer; popup view
							//EquipmentManagerLiquidation.addFooterFrist();
							EquipmentManagerLiquidation.refreshFooter();
							/*if (data.footer == undefined || data.footer == null) {
								EquipmentManagerLiquidation.refreshFooter();
							}*/
						}
					});
				}, 
				onClose: function(){
					$('#easyuiPopupViewInfo').html("");
				}
			});
		}, null, null);		
	},
	/**
	 * Xoa tap tin dinh kem
	 * 
	 * @author nhutnn
	 * @since 11/02/2015
	 * */
	removeEquipAttachFile: function(id){
		if (id == undefined || id == null || isNaN(id)) {
			return;
		}
		if (EquipmentManagerLiquidation._arrFileDelete == null || EquipmentManagerLiquidation._arrFileDelete == undefined) {
			EquipmentManagerLiquidation._arrFileDelete = [];
		}
		EquipmentManagerLiquidation._arrFileDelete.push(id);
		$('#divEquipAttachFile'+id).remove();
		if (EquipmentManagerLiquidation._countFile > 0) {
			EquipmentManagerLiquidation._countFile = EquipmentManagerLiquidation._countFile - 1;
		}
	},
	/**
	 * Show popup tim kiem thiet bi
	 * @author phuongvm
	 * @since 27/05/2015
	 */

	 /**
	 * Show popup tim kiem thiet bi MH them noi thanh ly tai san
	 * @author vuongmq
	 * @since 08/07/2015
	 */
	showPopupSearchEquipment: function (isSearch) {
		$('.ErrorMsgStyle').html('').hide();
		$('.SuccessMsgStyle').html('').hide();
		$('#easyuiPopupSearchEquipment').show();
		$('#easyuiPopupSearchEquipment').dialog({  
	        closed: false,  
	        cache: false, 
	        modal: true,
	        width : 1000,
	        height: 'auto',
	        onOpen: function() {	    
	        	//setDateTimePicker('yearManufacturing');
	        	Utils.bindComboboxEquipGroupEasyUICodeName('groupEquipment', null, 193);
	        	Utils.bindComboboxEquipGroupEasyUICodeName('providerId', null, 193);

	        	var year = new Date();
	        	year = year.getFullYear(); 
	        	var op = '<option value="" selected="selected">Chọn</option><option value="'+year+'">'+year+'</option>';
	        	for(var i=1; i<100; i++){
	        		op+='<option value="'+(year-i)+'">'+(year-i)+'</option>';
	        	}
	        	$('#yearManufacturing').html(op).change();
	        	$('#equipmentCode, #seriNumber, #typeEquipment').bind('keyup',function(event){
	        		if(event.keyCode == keyCodes.ENTER){
	        			$('#btnSeachEquipmentDlg').click(); 
	        		}
	        	});
	        	$('#yearManufacturing').bind('keyup',function(event){
	        		if(event.keyCode == keyCodes.ENTER){
	        			$('#btnSeachEquipmentDlg').click(); 
	        		}
	        	});
	        	$('#groupEquipment, #providerId').next().find(".combo-text.validatebox-text").bind('keyup',function(event){
	        		if(event.keyCode == keyCodes.ENTER){
	        			$('#btnSeachEquipmentDlg').click(); 
	        		}
	        	});
	        	$('#shopCode, #shortCode, #customerName, #address').bind('keyup',function(event){
	        		if(event.keyCode == keyCodes.ENTER){
	        			$('#btnSeachEquipmentDlg').click(); 
	        		}
	        	});
	        	
	        	$('#stockCode').bind('keyup',function(event){
	        		if(event.keyCode == keyCodes.ENTER){
	        			$('#btnSeachEquipmentDlg').click(); 
	        		}
	        	});
	        	//vuongmq; 07/08/2015; khong cho F9 nua; vi lay ca NPP va KH
	        	/*$('#stockCode').bind('keyup',function(event){
	        		if(event.keyCode == keyCodes.F9){
	        			EquipmentManagerLiquidation.showPopupStock('stockCode');
	        		}
	        	});*/

	        	if ($('#stockInGrid').val() != undefined && $('#stockInGrid').val() != null && $('#stockInGrid').val() != '') {
	        		$('#stockCode').val($('#stockInGrid').val());
	        	}

	        	EquipmentManagerLiquidation.getEquipCatalog();
	        	var params = new Object();
	        	var groupId = $('#groupEquipment').combobox('getValue');
	        	if (groupId == -1 || groupId == "") {
	        		groupId = "";
	        	}
	        	var providerId = $('#providerId').combobox('getValue');
	        	if (providerId == -1 || providerId == "") {
	        		providerId = "";
	        	}
				params = {
						equipCode: $('#equipmentCode').val().trim(),
						seriNumber: $('#seriNumber').val().trim(),
						categoryId: $('#typeEquipment').val(),
						// groupId: $('#groupEquipment').val(),
						groupId: groupId,
						// providerId: $('#providerId').val(),
						providerId: providerId,
						yearManufacture: $('#yearManufacturing').val(),
						stockCode: $('#stockCode').val().trim(),
						shopCode: $('#shopCode').val().trim(),
						shortCode: $('#shortCode').val().trim()
					};
				// truong hop tim kiem: isSearch: true, 
				if (isSearch == undefined || isSearch == null) {
					var lstEquipId = new Array(); 
		        	var rows = $('#gridEquipmentLiquidation').datagrid('getRows');
					if (rows != undefined && rows != null && rows.length > 1){
						for( var i = 0, size = rows.length; i < size; i++){					
							if(rows[i].equipmentId != undefined){
								lstEquipId.push(rows[i].equipmentId);							
							}					
						}
						params.lstEquipId = lstEquipId.join(',');
					}
					if ($("#idRecordHidden").val() != "" && EquipmentManagerLiquidation._lstEquipInRecord != null) {
						params.lstEquipDelete = "";
						for(var i=0; i<EquipmentManagerLiquidation._lstEquipInRecord.length; i++){
							params.lstEquipDelete += EquipmentManagerLiquidation._lstEquipInRecord[i]+',';
						}					
					}
					if (EquipmentManagerLiquidation._lstEquipInsert != null) {
						params.lstEquipAdd = "";
						for(var i=0; i<EquipmentManagerLiquidation._lstEquipInsert.length; i++){
							params.lstEquipAdd += EquipmentManagerLiquidation._lstEquipInsert[i]+',';
						}					
					}
					params.isSearch = activeType.STOPPED;
					$('#btnSeachEquipmentDlg').attr('onclick','EquipmentManagerLiquidation.searchEquipment('+activeType.STOPPED+');');
				} else {
					params.isSearch = activeType.RUNNING;
					$('#btnSeachEquipmentDlg').attr('onclick','EquipmentManagerLiquidation.searchEquipment('+activeType.RUNNING+');');
				}
				$('#equipmentGridDialog').datagrid({
					//url : '/equipment-manage-delivery/search-equipment-delivery', // 08/07/2015; khong xai nua
					url : '/equipment-manage-liquidation/search-equipment-liquidation-popup', // vuongmq; 08/07/2015; lay danh sach popup cua thiet bi
					autoRowHeight : true,
					rownumbers : true, 
					//fitColumns:true,
					pagination:true,
					scrollbarSize:15,
					pageSize:10,
					pageList: [10],
					width: $('#equipmentGridDialogContainer').width(),
					//height: 'auto',
					autoWidth: true,	
					queryParams: params,

					frozenColumns:[[					
						{field: 'select',title:'',width: 50,sortable:false,resizable:false, align: 'left', formatter: function(value, row, index) {
					    	//return '<a href="javascript:void(0);" onclick="EquipmentManagerLiquidation.selectEquipment('+index+');">Chọn</a>';
					    	var html="";
							if (isSearch) {
								html = '<a href="javascript:void(0);" onclick="EquipmentManagerLiquidation.selectEquipmentSearch('+index+');">Chọn</a>';
							} else {
								html = '<a href="javascript:void(0);" onclick="EquipmentManagerLiquidation.selectEquipment('+index+');">Chọn</a>';
							}
					    	return html;
						}},
						{field: 'shopCode',title:'Đơn vị',width: 150,sortable:false,resizable:false, align: 'left', formatter: function(value, row, index) {
							var html = '';
							if (row.shopCode != null && row.shopName != null) {
								html += row.shopCode + ' - ' + row.shopName;
							} else if (row.shopCode != null) {
								html += row.shopCode;
							} else if (row.shopName != null) {
								html += row.shopName;
							}
							return Utils.XSSEncode(html);
						}},	
						{field: 'stockCode',title:'Kho',width: 100,sortable:false,resizable:false, align: 'left', formatter: function(value, row, index) {
							return Utils.XSSEncode(value);
						}},
						{field: 'shortCode',title:'Mã KH',width: 50,sortable:false,resizable:false, align: 'left', formatter: function(value, row, index) {
							return Utils.XSSEncode(value);
						}},
						{field: 'customerName',title:'Tên KH',width: 100,sortable:false,resizable:false, align: 'left', formatter: function(value, row, index) {
							return Utils.XSSEncode(value);
						}},
						{field: 'address',title:'Địa chỉ',width: 120,sortable:false,resizable:false, align: 'left', formatter: function(value, row, index) {
							return Utils.XSSEncode(value);
						}},
					    {field: 'equipmentCode',title:'Mã thiết bị',width: 120,sortable:false,resizable:false, align: 'left', formatter: function(value, row, index) {
							return Utils.XSSEncode(value);
						}},
					]],
					columns:[[
					    {field: 'seriNumber',title:'Số serial',width: 100,sortable:false,resizable:false, align: 'left', formatter: function(value, row, index) {
							return Utils.XSSEncode(value);
						}},		
					    {field: 'healthStatus',title:'Tình trạng thiết bị',width: 100,sortable:false,resizable:false, align: 'left', formatter: function(value, row, index) {
							return Utils.XSSEncode(value);
						}},	
						 {field: 'groupEquipment',title:'Nhóm thiết bị',width: 120,sortable:false,resizable:false, align: 'left', formatter: function(value, row, index) {
					    	var html="";
							if(row.groupEquipmentCode != null && row.groupEquipmentName!= null){
								html = row.groupEquipmentCode+' - '+row.groupEquipmentName; 
							}
							return Utils.XSSEncode(html);
						}},	
					    {field: 'typeEquipment',title:'Loại',width: 80,sortable:false,resizable:false, align: 'left', formatter: function(value, row, index) {
							return Utils.XSSEncode(value);
						}},
					    {field: 'capacity',title:'Dung tích (lít)',width: 80,sortable:false,resizable:false, align: 'right', formatter: function(value, row, index) {
							return Utils.XSSEncode(value);
						}},		
					    {field: 'equipmentBrand',title:'Hiệu',width: 80,sortable:false,resizable:false, align: 'left', formatter: function(value, row, index) {
							return Utils.XSSEncode(value);
						}},		
					    {field: 'equipmentProvider',title:'NCC',width: 80,sortable:false,resizable:false, align: 'left', formatter: function(value, row, index) {
							return Utils.XSSEncode(value);
						}},		
					    {field: 'yearManufacture',title:'Năm sản xuất',width: 80,sortable:false,resizable:false, align: 'left' },											
					]],
					onBeforeLoad:function(param){											 
					},
					onLoadSuccess :function(data){   			 
						if (data == null || data.total == 0) {
							//$("#errMsgInfo").html("Không tìm thấy danh sách biên bản giao nhận thiết bị!").show();
						}else{
							$('.datagrid-header-rownumber').html('STT');		    	
						}	    	
						$('#equipmentCode').focus();	
					}
				});
	        },
			onClose: function(){
				$('#easyuiPopupSearchEquipment').hide();
				$('#equipmentCode').val('').change();
				$('#seriNumber').val('').change();
				$('#typeEquipment').html('<option value="" selected="selected">Tất cả</option>');
				// $('#groupEquipment').html('<option value="" selected="selected">Tất cả</option>').change();
				// $('#providerId').html('<option value="" selected="selected">Tất cả</option>');
				$('#yearManufacturing').val('').change();
				$('#stockCode').val('').change();
				$('#shopCode').val('').change();
				$('#shortCode').val('').change();
				$('#customerName').val('').change();
				$('#address').val('').change();
				$('#equipmentGridDialog').datagrid('loadData',[]);
			}
		});
	},
	/**
	 * Lay loai, ncc thiet bi
	 * @author phuongvm
	 * @since 27/05/2015
	 */
	getEquipCatalog: function(){
		$('#typeEquipment').html('<option value="" selected="selected">Tất cả</option>');
    	$('#provider').html('<option value=""  selected="selected">Tất cả</option>');
    	
		$.getJSON('/equipment-manage-delivery/get-equipment-catalog', function(result) {				
			if(result.equipsCategory != undefined && result.equipsCategory != null && result.equipsCategory.length > 0) {					
				for(var i=0; i < result.equipsCategory.length; i++){
					$('#typeEquipment').append('<option value="'+ result.equipsCategory[i].id +'">'+Utils.XSSEncode(result.equipsCategory[i].code)+' - '+Utils.XSSEncode(result.equipsCategory[i].name) +'</option>');  
				}
			} 
			$('#typeEquipment').change();
			//tamvnm: doi thanh autoCompleteCombobox
			if(result.equipProviders != undefined && result.equipProviders != null && result.equipProviders.length > 0) {					
				for (var i = 0, isize = result.equipProviders.length; i < isize; i++) {
					result.equipProviders[i].searchText = unicodeToEnglish(result.equipProviders[i].code + " " + result.equipProviders[i].name);
 					result.equipProviders[i].searchText = result.equipProviders[i].searchText.toUpperCase();
				}
				var obj = {
					id: -1,
					code: "Tất cả",
					name: "Tất cả",
					codeName: "Tất cả",
					searchText: unicodeToEnglish("Tất cả").toUpperCase()
				};
				result.equipProviders.splice(0, 0, obj);
				$('#providerId').combobox("loadData", result.equipProviders);
		 		$('#providerId').combobox("setValue", -1);
			}							
		});
	},
	/**
	 * Chon thiet bi
	 * @author phuongvm
	 * @since 27/05/2015
	 */
	selectEquipment: function(index) {
		var rowPopup = $('#equipmentGridDialog').datagrid('getRows')[index];
		rowPopup.contentDelivery = null;
		if (EquipmentManagerLiquidation._editIndex!=null) {
			/*var valEquip = $("#gridEquipmentLiquidation").datagrid('getEditor', {index:EquipmentManagerLiquidation._editIndex,field:'valueEquip'});
			rowPopup.valueEquip = $(valEquip.target).val();
			$('#gridEquipmentLiquidation').datagrid('updateRow',{
				index: EquipmentManagerLiquidation._editIndex,	// index start with 0
				row: rowPopup
			});*/
			$('#gridEquipmentLiquidation').datagrid('updateRow',{
				index: EquipmentManagerLiquidation._editIndex,	// index start with 0
				row: rowPopup
			});
			EquipmentManagerLiquidation.refreshFooter();
			$('#contentDelivery').width($('#contentDelivery').parent().width());
			$('#equipmentCodeInGrid').width($('#equipmentCodeInGrid').parent().width());
			$('#seriNumberInGrid').width($('#seriNumberInGrid').parent().width());
			$('#stock').width($('#stock').parent().width());
			
			$('#gridEquipmentLiquidation').datagrid('beginEdit', EquipmentManagerLiquidation._editIndex);
			EquipmentManagerLiquidation.editInputInGrid();
//			EquipmentManagerLiquidation.insertEquipmentInGrid();

			// kiem tra khi them mot dong thiet bi, nhu cho change checkLot
			if ($('#checkLot').is(':checked')) {
				EquipmentManagerLiquidation.checkGiaTriThanhLyThuHoi(EquipmentManagerLiquidation._DISABLED);
			} else {
				EquipmentManagerLiquidation.checkGiaTriThanhLyThuHoi(EquipmentManagerLiquidation._ENABLE);
			}
		}
		
		$('#easyuiPopupSearchEquipment').dialog('close');
	},
	/**
	 * Show popup chon kho
	 * @author phuongvm
	 * @since 27/05/2015
	 */
	showPopupStock:function(txtInput){
		var txt = "stockInGrid";
		if(txtInput!=undefined){
			txt = txtInput;
		}
		$('#common-dialog-search-2-textbox').dialog("close");
		$('#common-dialog-search-2-textbox').remove();
		VCommonJS.showDialogSearch2({
			inputs : [
		        {id:'code', maxlength:50, label:'Mã đơn vị'},
		        {id:'name', maxlength:250, label:'Tên đơn vị'},
		    ],
		    url : '/commons/get-equipment-stock',
		    isCenter : true,
		    columns : [[
		        {field:'codeName', title:'Đơn vị', align:'left', width: 110, sortable:false, resizable:false, formatter:function(value,row,index) {
		        	var nameCode = row.shopCode + '-' + row.shopName;
		        	return Utils.XSSEncode(nameCode);         
		        }},
		        {field:'equipStockCode', title:'Mã kho', align:'left', width: 200, sortable:false, resizable:false},
		        {field:'choose', title:'', align:'center', width:40, sortable:false, resizable:false, formatter:function(value,row,index) {
//			        	var nameCode = row.shopCode + '-' + row.name;
		            return '<a href="javascript:void(0)" onclick="Utils.fillCodeForInputTextByDialogSearch2(\''+txt+'\', ' + row.equipStockId +', \''+row.equipStockCode+'\', \''+row.name+'\','+Utils._isShowInputTextByDlSearch2.code+');">chọn</a>';         
		        }}
		    ]]
		});
	},
	/**
	 * Chon kho
	 * @author phuongvm
	 * @since 27/05/2015
	 */
	selectStock: function(shopCode){
		$('#stockCode').val(shopCode);		
		$('#easyuiPopupSearchStock').dialog('close');
	},
	/**
	 * Tim kiem kho
	 * @author phuongvm
	 * @since 27/05/2015
	 */
	searchStock: function(){
		var params=new Object(); 
		params.status=1;
		params.isUnionShopRoot = true;
		params.code = $('#shopCodePopupStock').val().trim();
		params.name = $('#shopNamePopupStock').val().trim();
		
		$('#stockGridDialog').datagrid('load',params);
	}
};

/*
 * END OF FILE - /web/web/resources/scripts/business/equipment/vnm.equipment-manage-liquidation.js
 */

/*
 * START OF FILE - /web/web/resources/scripts/business/equipment/vnm.equiment-statistic-checking.js
 */
var EquipStatisticChecking = {
	__listCheckId:null,
	_lstRowId: null,
	filter:null,
	_selectingEquipmentStatistics: new Map(),	// danh sach cac kiem ke dang duoc chon
	_lentghGridGroupProduct: 0,
	_lengthGridShelf: 0,
	_lstEquipId: new Map(),
	_equipHtml:'',
	_equipGroupHtml:'',
	status:0,
	type: null,
	ALL_EQUI_TYPE: '0',
	GROUP_EQUI_TYPE: '1',
	LIST_EQUI_TYPE: '3',
	_paramListStatistic: null,
	detailChecking : function(id) {
		VTUtilJS.getFormJson({id:id}, '/equipment-checking/statistic-detail', function(list) {
			$('#gridDetail').html('<h2 class="Title2Style">Danh sách nhóm thiết bị</h2>\
					<div class="SearchInSection SProduct1Form">\
					<div class="SearchInSection SProduct1Form">\
					<div class="GridSection" id=gridDetailContainer>\
					<table id="detailGrid"></table>\
					</div>\
					</div>	\
					</div>').show();
			$('#detailGrid').datagrid({
				data : list,
				autoRowHeight : true,
				rownumbers : true, 
				singleSelect: true,
				pagination:true,
				fitColumns:true,
				scrollbarSize:0,
				width: $('#gridDetailContainer').width(),
				autoWidth: true,
				columns:[[	        
				    {field : 'code' , title : 'Mã nhóm', width : 80, align : 'left', formatter : CommonFormatter.formatNormalCell},
				    {field : 'name' , title : 'Tên nhóm', width : 130, align : 'left', formatter : CommonFormatter.formatNormalCell},
				    {field : 'catName' , title : 'Loại thiết bị', width : 80, align : 'left', formatter : CommonFormatter.formatNormalCell},
				    {field : 'total' , title : 'Số lượng tổng', width : 80, align : 'right', formatter : CommonFormatter.formatNormalCell},
				    {field : 'numInstn' , title : 'Số lượng còn', width : 80, align : 'right', formatter : CommonFormatter.formatNormalCell},
				    {field : 'numLost' , title : 'Số lượng mất', width : 80, align : 'right', formatter : CommonFormatter.formatNormalCell}
			    ]],	    
			    onLoadSuccess :function(data){	    	
					$('.datagrid-header-rownumber').html('STT');			
					$(window).resize();
				}
			});
		});
	},
	/*
	* save record
	*/
	save: function() {
		$('#errorMsg').html('').hide();
		var msg ='';
		if(msg.length == 0){
			msg = Utils.getMessageOfSpecialCharactersValidate('checkingCode','Mã kiểm kê', Utils._CODE);
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfSpecialCharactersValidate('checkingName','Tên kiểm kê');
		}		
		var fdate = $('#fromDate').val().trim();
		if(msg.length == 0 && fdate != '' && !Utils.isDate(fdate) && fdate != '__/__/____'){
			msg = 'Từ ngày không tồn tại hoặc không đúng định dạng dd/mm/yyyy.';
		}
		var tdate = $('#toDate').val().trim();
		if(msg.length == 0 && tdate != '' && !Utils.isDate(tdate) && tdate != '__/__/____'){
			msg = 'Đến ngày không tồn tại hoặc không đúng định dạng dd/mm/yyyy.';
		}
		if(msg.length == 0 && fdate != '' && tdate != '' && !Utils.compareDate(fdate, tdate)){
			msg = 'Đến ngày không được nhỏ hơn Từ ngày';	
		}
		if($('#type').val().trim().length == 0) {
			$('#errorMsg').html('Bạn chưa chọn loại kiểm kê').show();
			return;
		}
		if(msg.length > 0){
			$('#errorMsg').html(msg).show();
			return false;
		}
		var warning = 'Bạn có muốn lưu thông tin?';
		var id = $('#id').val();
		var oldType = $('#oldType').val();
		var newType = $('#type').val();
		var oldStr = "";
		var newStr = "";
		if(id != null && id != undefined && id != '') {
			if (oldType != newType && oldType != EquipStatisticChecking.ALL_EQUI_TYPE) {
				if (oldType == EquipStatisticChecking.LIST_EQUI_TYPE) {
					oldStr = 'danh sách thiết bị';
				} else if (oldType == EquipStatisticChecking.GROUP_EQUI_TYPE) {
					oldStr = 'nhóm thiết bị';
				}
				if (newType == EquipStatisticChecking.ALL_EQUI_TYPE) {
					newStr = '"Tất cả thiết bị"';
				} else if (newType == EquipStatisticChecking.LIST_EQUI_TYPE) {
					newStr = '"Danh sách thiết bị"';
				} else if (newType == EquipStatisticChecking.GROUP_EQUI_TYPE) {
					newStr = '"Nhóm thiết bị"';
				}
				warning = ('Nếu chuyển Loại kiểm kê thành ' + newStr + ' thì tất cả ' + oldStr + ' sẽ bị xóa. \n' + warning);
			}
		}
		$.messager.confirm("Xác nhận", warning, function(r) {
			if(r) {
				$('#errorMsg').html('').hide();
				VTUtilJS.postFormJson('search-from', '/equipment-checking/save', function(data) {
					var idLoad = $('#id').val();
					if(data.error) {
						$('#errorMsg').html(data.errMsg).show();
					} else {
						if(data.confirmCus){
							$.messager.confirm("Xác nhận", "Mã CTHTTM hiện không có khách hàng. Bạn có muốn lưu thông tin?", function(r) {
								if(r) {
									VTUtilJS.postFormJson('search-from', '/equipment-checking/saveEx', function(data) {
										if(data.error) {
											$('#errorMsg').html(data.errMsg).show();
										} else {
											window.location.href = '/equipment-checking/edit?id='+idLoad;
										}
									});
								}else{
									window.location.href = '/equipment-checking/edit?id='+idLoad;
								}
							});
						}else{
							window.location.href = '/equipment-checking/edit?id='+data.instance.id;
						}
					}
				});
			}
		});
	},
	/*
	* event fro checkbox on shop dialog. propagration from parent to child
	*/
	onCheckShop: function(t) {
		var ck = $(t).prop("checked");
		if (ck) {
			var p = "#ck" + $(t).attr("parentId");
			EquipStatisticChecking.removeParentShopCheck(p);
			EquipStatisticChecking.removeChildShopCheck($(t).val());
		} else {
			//var p = "#ck" + $(t).attr("parentId");
			//EquipStatisticChecking.enableParentShopCheck(p);
		}
	},
	removeParentShopCheck: function(t) {
		if ($(t).length == 0) {
			return;
		}
		$(t).removeAttr("checked");
		//$(t).attr("disabled", "disabled");
		var p = "#ck" + $(t).attr("parentId");
		EquipStatisticChecking.removeParentShopCheck(p);
	},
	enableParentShopCheck: function(t) {
		if ($(t).length == 0 || $(t).attr("exists") == "1") {
			return;
		}
		$(t).removeAttr("disabled");
		var p = "#ck" + $(t).attr("parentId");
		EquipStatisticChecking.enableParentShopCheck(p);
	},
	enableChildShopCheck: function(parentId) {
		var listChild = $('input[type=checkbox][parentId='+parentId+']');
		for(var i = 0; i < listChild.length; i++) {
			var child = listChild[i];
			$(child).attr('checked', 'checked');
			EquipStatisticChecking.enableChildShopCheck($(child).val());
		}
	},
	removeChildShopCheck: function(parentId) {
		var listChild = $('input[type=checkbox][parentId='+parentId+']');
		for(var i = 0; i < listChild.length; i++) {
			var child = listChild[i];
			$(child).removeAttr("checked");
			EquipStatisticChecking.removeChildShopCheck($(child).val());
		}
	},
	/*
	* show Shop dialog
	*/
	showShopDlg: function(shopId) {
		if (shopId == undefined || shopId == null) {
			shopId = 0;
		}
		$(".ErrorMsgStyle").hide();
		var html = '<div id="add-shopPopupDiv" style="display:none;">\
			<div id="add-shopPopup">\
			<div class="PopupContentMid">\
				<div class="GeneralForm Search1Form">\
					<label class="LabelStyle" style="width:100px;">Mã đơn vị</label>\
					<input id="codeDlg" class="InputTextStyle" style="width:150px;" maxlength="40" />\
					<label class="LabelStyle" style="width:100px;">Tên đơn vị</label>\
					<input id="nameDlg" class="InputTextStyle" style="width:230px;" maxlength="100" />\
					<div class="Clear"></div>\
					<div class="BtnCenterSection">\
					    <button class="BtnGeneralStyle" id="btnSearchDlg">Tìm kiếm</button>\
					</div>\
					\
					<div class="GridSection" id="gridDlgDiv">\
						<div id="gridDlg"></div>\
					</div>\
					\
					<p id="erMsgDlg" class="ErrorMsgStyle" style="padding-left:5px;display:none;"></p>\
					<div class="Clear"></div>\
					<div class="BtnCenterSection">\
					    <button class="BtnGeneralStyle" id="btnChooseDlg">Chọn</button>\
						<button class="BtnGeneralStyle" id="btnCloseDlg" onclick="javascript:$(\'#add-shopPopup\').dialog(\'close\');">Đóng</button>\
					</div>\
				</div>\
			</div>\
			</div>\
		</div>';
		$("body").append(html);
		 
		$("#add-shopPopup").dialog({
			title: "Chọn đơn vị",
			width: 700,
			heigth: "auto",
			modal: true, close: false, cache: false,
			onOpen: function() {
				$("#add-shopPopup").addClass("easyui-dialog");
				$("#add-shopPopup #codeDlg").focus();
				
				$("#add-shopPopup #gridDlg").treegrid({
					url: "/equipment-checking/edit-shop/search-shop-dlg",
					queryParams : {recordId : $("#id").val(), id : shopId},
					rownumbers: false,
					width: $("#add-shopPopup #gridDlgDiv").width(),
					height: 350,
					fitColumns: true,
					idField: "nodeId",
					treeField: "text",
					selectOnCheck: false,
					checkOnSelect: false,
					columns: [[
						{field:"text", title:"Mã Đơn vị", sortable: false, resizable: false, width:100, align:"left", formatter:CommonFormatter.formatNormalCell},
						{field:"ck", title:"", sortable: false, resizable: false, width:35, fixed:true, align:"center", formatter: function(v, r) {
							var pId = r.attr.parentId;
							if (pId == undefined || pId == null) {
								pId = 0;
							}
							var isCheck = r.attr.isCheck;
							if(isCheck != null && isCheck != undefined && Number(isCheck) > 0) {
								return "<input type='checkbox' checked='checked' disabled='disabled' id='ck"+r.nodeId+"' value='"+r.attr.id+"' exists='0' parentId='"+pId+"' onchange='EquipStatisticChecking.onCheckShop(this);' />";
							} else {
								return "<input type='checkbox' id='ck"+r.nodeId+"' value='"+r.attr.id+"' exists='0' parentId='"+pId+"' onchange='EquipStatisticChecking.onCheckShop(this);' />";
							}
						}}
					]],
					onLoadSuccess: function(data) {
						$("#add-shopPopup #gridDlg").datagrid("resize");
						setTimeout(function() {
			    			CommonSearchEasyUI.fitEasyDialog('add-shopPopup');
			    		}, 500);
					}
				});
				
				$("#add-shopPopup #btnSearchDlg").click(function() {
					var p = {
							recordId : $("#id").val(),
							shopSearchCode: $("#add-shopPopup #codeDlg").val().trim(),
							shopSearchName: $("#add-shopPopup #nameDlg").val().trim(),
							id : shopId
					};
					//$("#add-shopPopup #gridDlg").treegrid("load", p);
					$("#add-shopPopup #gridDlg").treegrid({url: "/equipment-checking/edit-shop/search-shop-dlg",queryParams : p});
				});
				$("#add-shopPopup #codeDlg, #add-shopPopup #nameDlg").parent().keyup(function(event) {
					if (event.keyCode == keyCodes.ENTER) {
						$("#add-shopPopup #btnSearchDlg").click();
					}
				});
				$("#add-shopPopup #btnChooseDlg").click(function() {
					if ($("#add-shopPopup input[id^=ck]:checked").length == 0) {
						$("#erMsgDlg").html("Không có đơn vị nào được chọn").show();
						return;
					}
					var lstTmp = new Map();
					var v;
					$("#add-shopPopup input[id^=ck]:checked").each(function() {
						if($(this).attr('disabled') != 'disabled') {
							v = $(this).val().trim();
							lstTmp.put(v, v);
						}
					});
					EquipStatisticChecking.addShopQtt(lstTmp);
				});
				$("#add-shopPopup #codeDlg").focus();
			},
			onClose: function() {
				$("#add-shopPopup").dialog("destroy");
				$("#add-shopPopupDiv").remove();
			}
		});
	},
	addShopQtt: function(lstShopTmp) {
		$(".ErrorMsgStyle").hide();
		$.messager.confirm("Xác nhận", "Bạn có muốn thêm đơn vị?", function(r) {
			if (r) {
				var params = {
						id: $("#id").val(),
						listId : lstShopTmp.keyArray
				};
				$("#add-shopPopup").dialog("close");
				VTUtilJS.postFormJson(params, '/equipment-checking/edit-shop/save-shop', function(data) {
					if(data.error) {
						$('#errorMsg').html(data.errMsg).show();
					} else {
						$('#successMsg').html('Lưu dữ liệu thành công').show();
						setTimeout(function() {
							$('#successMsg').html('').hide();
						}, 3000);
						$("#gridShop").treegrid("reload", {recordId:$("#id").val()});
					}
				});
			}
		});
	},
	/*
	* delete shop of record
	*/
	deleteShopMap: function(shopId) {
		$(".ErrorMsgStyle").hide();
		var params = {
				id: $("#id").val().trim(),
				shopId: shopId
		};
		VTUtilJS.postFormJson(params, '/equipment-checking/edit-shop/check-child-shop', function(data) {
			if(data.deleteChild) {
				$.messager.confirm("Xác nhận", "Xóa đơn vị sẽ thì sẽ xóa các thiết bị liên quan đến đơn vị này.\nBạn có muốn xóa đơn vị và các đơn vị con?", function(r) {
					if (r) {
						params.deleteChild = true;
						VTUtilJS.postFormJson(params, '/equipment-checking/edit-shop/delete-shop', function(data) {
							if(data.error) {
								$('errorMsg').html(data.errMsg).show();
							} else {
								$('#successMsg').html('Lưu dữ liệu thành công').show();
								setTimeout(function() {
									$('#successMsg').html('').hide();
								}, 3000);
								$("#gridShop").treegrid("reload", {recordId:$("#id").val()});
							}
						});
					} else {
						params.deleteChild = false;
						VTUtilJS.postFormJson(params, '/equipment-checking/edit-shop/delete-shop', function(data) {
							if(data.error) {
								$('errorMsg').html(data.errMsg).show();
							} else {
								$('#successMsg').html('Lưu dữ liệu thành công').show();
								setTimeout(function() {
									$('#successMsg').html('').hide();
								}, 3000);
								$("#gridShop").treegrid("reload", {recordId:$("#id").val()});
							}
						});
					}
				});
			} else {
				$.messager.confirm("Xác nhận", "Xóa đơn vị sẽ thì sẽ xóa các thiết bị liên quan đến đơn vị này.\nBạn có muốn xóa đơn vị?", function(r) {
					if (r) {
						params.deleteChild = false;
						VTUtilJS.postFormJson(params, '/equipment-checking/edit-shop/delete-shop', function(data) {
							if(data.error) {
								$('errorMsg').html(data.errMsg).show();
							} else {
								$('#successMsg').html('Lưu dữ liệu thành công').show();
								setTimeout(function() {
									$('#successMsg').html('').hide();
								}, 3000);
								$("#gridShop").treegrid("reload", {recordId:$("#id").val()});
							}
						});
					}
				});
			}
		});
	},
	updateEquipmentStatisticStatus: function() {
		$('#errMsg').html('').hide();	
		var statusForm = $('#cbxStatusUpdate').val().trim();
		if(statusForm < 0) {
			$('#errMsg').html('Vui lòng chọn trạng thái').show();
			return;
		}
		var arra = EquipStatisticChecking._selectingEquipmentStatistics.valArray;
		var lstId = new Array();
		if(arra == null || arra.length == 0) {
			$('#errMsg').html('Bạn chưa chọn kiểm kê để cập nhật. Vui lòng chọn kiểm kê').show();
			return;
		}		
		for(var i=0, size = arra.length; i < size; ++i) {
			lstId.push(arra[i].id);
		}
		var params = new Object();
		params.listId = lstId;
		params.equipmentStatisticRecordStatus = statusForm;
		Utils.addOrSaveData(params, '/equipment-checking/update-statistic-status', null, 'errMsg', function(data) {
			if(data.updateFailedRecords!=undefined && data.updateFailedRecords!=null && data.updateFailedRecords.length > 0){
				VCommonJS.showDialogList({
				    data : data.updateFailedRecords,
				    columns : [[
				        {field:'formCode', title:'Mã biên bản', align:'left', width: 110, sortable:false, resizable:false},
				        {field:'formStatusErr', title:'Trạng thái biên bản', align:'center', width: 110, sortable:false, resizable:false, formatter: function(value,row,index){
				        	var html = "";
							if(value == 1){
								html = '<img src="/resources/images/icon_delete.png" height="20" width="20" style="margin-right:5px;" title="Error">';
							}
							return html;
						}},
						{field:'notGroupOrShop', title:'Không có nhóm và đơn vị', align:'center', width: 110, sortable:false, resizable:false, formatter: function(value,row,index){
				        	var html = "";
							if(value == 1){
								html = '<img src="/resources/images/icon_delete.png" height="20" width="20" style="margin-right:5px;" title="Error">';
							}
							return html;
						}},
						{field:'groupInActive', title:'Các nhóm không hoạt động', align:'left', width: 110, sortable:false, resizable:false, formatter: function(value,row,index){
				        	var html = "";
							if(value != ""){
								html = value;//'<img src="/resources/images/icon_delete.png" height="20" width="20" style="margin-right:5px;" title="Error">';
							}
							return html;
						}},
						{field:'groupNotEquip', title:'Các nhóm không có thiết bị thuộc đơn vị để kiểm kê', align:'left', width: 110, sortable:false, resizable:false, formatter: function(value,row,index){
				        	var html = "";
							if(value != ""){
								html = value;//'<img src="/resources/images/icon_delete.png" height="20" width="20" style="margin-right:5px;" title="Error">';
							}
							return html;
						}},
						{field:'shopInActive', title:'Các đơn vị không hoạt động', align:'left', width: 110, sortable:false, resizable:false, formatter: function(value,row,index){
				        	var html = "";
							if(value != ""){
								html = value;//'<img src="/resources/images/icon_delete.png" height="20" width="20" style="margin-right:5px;" title="Error">';
							}
							return html;
						}}
					]]
				});	
			}
			EquipStatisticChecking._selectingEquipmentStatistics = new Map();
			$('#grid').datagrid('reload');
		}, null, true,null, 'Bạn có muốn cập nhật trạng thái của kiểm kê ?');
	},
	/*
	* save group checking
	*/
	saveGroupChecking : function(recordId) {
		$('#detailGrid').datagrid('acceptChanges');
		var rows = $('#detailGrid').datagrid('getRows');
		var listDetailId = new Array();
		var listExists = new Array();
		for(var i = 0; i < rows.length; i++) {
			if(rows[i].isBelong != null && rows[i].isBelong != undefined && (rows[i].isBelong == 0 || rows[i].isBelong == 1)) {
				listDetailId.push(rows[i].detailId);
				listExists.push(rows[i].isBelong);
			}
		}
		
		if(listDetailId.length == 0 || listExists.length == 0) {
			$('#errorMsg').html('Bạn chưa chọn kiểm kê để lưu').show();
			return;
		}
		
		for(var i = 0; i < rows.length; i++) {
			$('#detailGrid').datagrid('beginEdit', i);
		}
		$.messager.confirm('Xác nhận','Bạn có muốn lưu thông tin?',function(r){
			if(r) {
				VTUtilJS.postFormJson({id: $('#id').val(),listDetailId : listDetailId, listExists : listExists}, '/equipment-checking/save-group-checking', function(data) {
					if(data.error) {
						$('#errorMsg').html(data.errMsg).show();
					} else {
						$('#successMsg').html('Lưu dữ liệu thành công').show();
						setTimeout(function() {
							$('#successMsg').html('').hide();
						}, 3000);
						var shopId = $('#selectShop').val();
						var equipGroupId = $('#selectEquipGroup').val();
						var equipCode = $('#equipCode').val().trim();
						var stockId = $('#selectStock').val();
						var soLan = $('#soLan').val();
						var seri = $('#seri').val();
						var checkDate = $('#checkDate').val();
						var obj = new Object();
						if(shopId != null && shopId != -1) {
							obj.shopId = shopId;
						}
						if(equipGroupId != null && equipGroupId != -1) {
							obj.equipGroupId = equipGroupId;
						}
						obj.equipCode = equipCode;
						if(stockId != null && stockId != -1) {
							obj.stockId = stockId;
						}
						if(soLan != null) {
							obj.stepCheck = soLan;
						}
						if(checkDate!=null){
							obj.checkDate = checkDate;
						}
						obj.seri = seri;
						obj.isFirstLoad = false;
						$('#detailGrid').datagrid('load', obj);
					}
				});
			}
		});
	},
	/*
	* save shelf checking
	* update author nhutnn
	* update since 05/08/2015
	*/
	saveShelfChecking : function(recordId) {
		$('.ErrorMsgStyle ').html('').hide();
		$('#detailGrid').datagrid('acceptChanges');
		var rows = $('#detailGrid').datagrid('getRows');
		var listDetailId = new Array();
		var listActQty = new Array();
		for(var i = 0; i < rows.length; i++) {
			// if(rows[i].actualQuantity != '' && !isNaN(rows[i].actualQuantity)) {
				listDetailId.push(rows[i].detailId);
				listActQty.push(rows[i].actualQuantity);
			// }
		}
		for(var i = 0; i < rows.length; i++) {
			$('#detailGrid').datagrid('beginEdit', i);
		}
		if(listDetailId.length == 0 || listActQty.length == 0) {
			$('#errorMsg').html('Bạn chưa chọn kiểm kê để lưu').show();
			return;
		}
		EquipStatisticChecking.bindEventInput();
		$.messager.confirm('Xác nhận','Bạn có muốn lưu thông tin?',function(r){
			if(r) {
				VTUtilJS.postFormJson({id: $('#id').val(),listDetailId : listDetailId, listActQty : listActQty}, '/equipment-checking/save-shelf-checking', function(data) {
					if(data.error) {
						$('#errorMsg').html(data.errMsg).show();
					} else {
						$('#successMsg').html('Lưu dữ liệu thành công').show();
						setTimeout(function() {
							$('#successMsg').html('').hide();
						}, 3000);
						var shopId = $('#selectShop').val();
						var equipGroupId = $('#selectEquipGroup').val();
						var equipCode = $('#equipCode').val();
						var stockId = $('#selectStock').val();
						var soLan = $('#soLan').val();
						var seri = $('#seri').val();
						var checkDate = $('#checkDate').val();
						var obj = new Object();
						if(shopId != null && shopId != -1) {
							obj.shopId = shopId;
						}
						if(equipGroupId != null && equipGroupId != -1) {
							obj.equipGroupId = equipGroupId;
						}
						obj.equipCode = equipCode;
						if(stockId != null && stockId != -1) {
							obj.stockId = stockId;
						}
						if(soLan != null) {
							obj.stepCheck = soLan;
						}
						if(checkDate!=null){
							obj.checkDate = checkDate;
						}
						obj.seri = seri;
						obj.isFirstLoad = false;
						$('#detailGrid').datagrid('load', obj);
					}
				});
			}
		});
	},
	detailImageRecord: function(id, detailId, shopCode, shortCode, equipCode, cusCode) {
		EquipStatisticChecking._paramListStatistic.recordId = id;
		EquipStatisticChecking.recordId = id;
//		EquipStatisticChecking.detailId = detailId;
//		EquipStatisticChecking.shopCodeSearch = shopCode;
		EquipStatisticChecking.customerCodeSearch = cusCode;
//		EquipStatisticChecking.equipCodeSearch = equipCode;
		EquipStatisticChecking.showDialogFancy();
	},
	/*
	* upload image
	*/
	uploadingImageRecord : function(id, detailId, existsNumber) {
		$('#errExcelMsgDVKK').html('').hide();
		var msg = ''; 
		if(msg.length == 0){
			msg = Utils.getMessageOfSpecialCharactersValidate('imageFile','Tên hình ảnh',Utils._NAME);
		}
		//Datpv4 check number images
		var params = EquipStatisticChecking._paramListStatistic;
		params.equipId = id;
		params.detailId = detailId;
		params.max = 20;
		params.page = 1;
		Utils.getJSONDataByAjaxNotOverlay(params, '/equipment-checking/get-images-for-popup', function(data){						
			existsNumber = data.lstImage.length;
			var size = existsNumber + EquipStatisticChecking.countArray.size();		

			if(size > 5){
				msg = 'Chỉ cho phép upload tối đa 5 hình cho 1 sản phẩm.';
			}
			if(msg.length == 0 && $('#fileQueue').html().length == 0 && EquipStatisticChecking.countArray.size() == 0){
				msg = 'Không có hình nào được chọn.';
			}
			var sizeMax = 5 * 1024 * 1024;
			if(msg.length == 0 && EquipStatisticChecking.countArray.size() > 0) {
				var rowsKeyArray = EquipStatisticChecking.countArray.keyArray;
				for (var i = 0, size = rowsKeyArray.length; i < size; i ++) {
					var rowImage = EquipStatisticChecking.countArray.get(rowsKeyArray[i]);
					if (rowImage.size > sizeMax) {
						msg = rowImage.name + ' có kích thước phải nhỏ hơn hoặc bằng 5Mb';
						break;
					}
				}
			}
			if(msg.length > 0) {
				$('#errExcelMsgDVKK').html(msg).show();
				return false;
			}
			
			$('#divOverlay').show();
			$('#imageFile').uploadifySettings('scriptData', {
				'recordId' : id,
				'detailId' : detailId,
				'currentUser' : currentUser
			});
			$('#imageFile').uploadifyUpload();
			return false;
		});	
	},
	countArray : null,
	/*
	* upload dialog
	*/
	uploadImageRecord : function(id, detailId, existsNumber) {
		var html = $('#easyuiPopupImportExcelContainer').html();
		var __html = $('#easyuiPopupImportExcelEx').html();
		$('#easyuiPopupImportExcelContainer').show();
		$('.ErrorMsgStyle').hide();
		EquipStatisticChecking.countArray = new Map();
		$('#easyuiPopupImportExcelEx').dialog({
	        closed: false,  
	        cache: false,  
	        modal: true,
	        height: 'auto',
	        onOpen: function(){
	        	$('.easyui-dialog .BtnGeneralStyle').css('color','');
	        	var countSuccess = 0;
	        	var countFail = 0;
	        	$('#easyuiPopupImportExcelEx #imageFile').uploadify({
	    			'auto'      : false,
	    			'script'    : '/upload-record-img',
	    			'method'	: 'GET',
	    			'cancelImg' : '/resources/scripts/plugins/uploadify2.1/cancel.png',
	    			'buttonImg' : '/resources/images/bg_multiBtn.jpg',
	    			'moverImg'  : '/resources/images/bg_multiBtn.jpg',
	    			'width': 139,
	    			'height': 30,
	    			'fileDesc'	: 'Image Files *.jpg; *.jpeg; *.png',			
	    			'fileExt': '*.jpg;*.jpeg;*.png',
	    			'multi': true,
	    			'queueClass': 'uploadifyQueue',
	    			'queueID'	: 'fileQueue',
	    			'displayData': 'percentage',
	    			//'sizeLimit':'5242880',
	    			'removeCompleted':true,
	    			'onInit' 	: function() {},
	    			'onSelect'  : function(event, queueID, fileObj,data) {
	    				$('#imageMsg').html('').hide();
	    				/*
	    				var sizemax = 5 * 1024 * 1024;
	    				if (fileObj.size <= sizemax) {				
	    					EquipStatisticChecking.countArray.put(queueID,fileObj);
	    				}
	    				*/
	    				EquipStatisticChecking.countArray.put(queueID,fileObj);
	    			},			
	    		    'onCancel'  : function(event, queueID, fileObj,data) {		    	
	    		    	EquipStatisticChecking.countArray.remove(queueID);
	    		    },
	    			'onQueueFull': function(event,queueSizeLimit){},
	    			'onComplete' : function(event, queueID, fileObj, response, data) {				
	    				eval("var obj=" + response);
	    				if(obj.errMsg!=null && obj.errMsg.length > 0){
	    					$('#errExcelMsgDVKK').html(obj.errMsg).show();
	    					return;
	    				}
	    				if(obj.status == 0) {
	    					countFail++;
	    					$('#errExcelMsgDVKK').html(obj.errMsg).show();
	    					/*setTimeout(function(){
	    					},300);*/
	    				} else {
	    					countSuccess++;
	    				}
	    				$('#countFail').val(countFail);
	    				$('#countSuccess').val(countSuccess);
	    				$('#'+detailId).show();
	    			},
	    			'onAllComplete': function(event,data){
	    				var msg = '';
	    				$('#divOverlay').hide();
	    				if($('#countSuccess').val() == '0' && $('#countFail').val() == '0') {
	    					return;
	    				}
    					if(parseInt($('#errorValue').val().trim()) == 1){							
    						msg = "Tải thành công <b>"+ $('#countSuccess').val() +"</b> hình ảnh,tải thất bại <b>"+ $('#countFail').val() +"</b> hình ảnh";							
    					} else if($('#mediaType').val() == '' || $('#mediaType').val() == 1){								
    						msg = "Tải thành công <b>"+ $('#countSuccess').val() +"</b> hình ảnh,tải thất bại <b>"+ $('#countFail').val() +"</a> hình ảnh";							
    					}
    					var flagError = false;
    					if($('#countSuccess').val() == '0') {
    						$('#errExcelMsgDVKK').html(msg).show();
    						flagError = true;
    					} else {
    						$('#successExcelMsgDVKK').html(msg).show();
    					}
    					$('#countSuccess').val(0);
    					$('#countFail').val(0);
    					if (flagError) {
    						return;
    					}
    					var tm = setTimeout(function(){
    						$('.ErrorMsgStyle').html('').hide();
    						$('.SuccessMsgStyle').html('').hide();
    						$('#easyuiPopupImportExcelEx').window('close');
							clearTimeout(tm);
						}, 1500);
	    			},
	    			'onError': function (event, queueID, fileObj, errorObj) {	
	    				if(fileObj.size>5242880){
	    					$('#imageFile'+queueID).find('.percentage').addClass('ErrorMsgStyle').text("-  Kích thước file vượt quá 5 MB");					
	    				}								
	    			},
	    			'onUploadSuccess':function(file,data,response){				
	    			}			
	    		});
	        	
	        	$('#uploadImageFile').bind('click', function() {
	        		EquipStatisticChecking.uploadingImageRecord(id, detailId, existsNumber);
	        	});
	        },
	        onClose: function(){
	        	$('#easyuiPopupImportExcelContainer').hide();
	        	$('#easyuiPopupImportExcelEx').html(__html);
	        	$('#easyuiPopupImportExcelContainer').html(html);
	        }
		});
	},
	pagePopup : 0,
	callBack : null,
	shopCodeSearch:null,
	customerCodeSearch:null,
	equipCodeSearch:null,
	detailId:null,
	recordId:null,
	lstImages:null,
	indexCurrent:null,
	downloadFile:function(temp, ow){
		var url = temp.urlEx;
		var customerInfo = temp.customerCode+' - '+temp.customerName+' - '+temp.customerAddress;
		var shopInfo = temp.shopCode+' - '+temp.shopName;
		var dateInfo = temp.dmyDate+' - '+temp.hhmmDate;
		var data=new Object();
		data.id = temp.id;
		data.urlImage=url;
		data.customerInfo=customerInfo;
		data.shopInfo=shopInfo;
		data.dateInfo=dateInfo;
		data.overwrite=true;
		if(temp.monthSeq!=undefined && temp.monthSeq!=null){
			data.monthSeqDownload=temp.monthSeq;
		}
		$('.loadingPopup').show();			
		$('.fancybox-inner .DownloadPhotoLink').hide();
		var kData = $.param(data, true);
		$.ajax({
			type : 'GET',
			url : '/equipment-checking/download/image',
			data :(kData),
			dataType : "json",
			success : function(data) {
				$('.loadingPopup').hide();
				$('.fancybox-inner .DownloadPhotoLink').show();
				if(!data.error && data.errMsg!= null && data.errMsg.length > 0){
					$('.fancybox-inner .DownloadPhotoLink').attr('href',data.errMsg);
				}else{
					$('.fancybox-inner .DownloadPhotoLink').attr('href',imgServerSOPath+url);
				}
			},
		error:function(XMLHttpRequest, textStatus, errorThrown) {$('.loadingPopup').hide();}});
	},
	imageBigShow : function() {
		$('.fancybox-inner #loadingImages').hide();
		$('.fancybox-inner #bigImages').fadeIn(1000);
	},
	imageLoadingShow : function() {
		$('.fancybox-inner #bigImages').hide();
		$('.fancybox-inner #loadingImages').show();
	},
	lazyLoadImages:function(object,container){
		$(object).lazyload({
			event: "scrollstop",
			container : $(container),
			effect: "fadeIn",
	        failurelimit: 1,
	        threshold: -20
		});
		$(object).each(function() {
		    $(this).attr("src", $(this).attr("data-original"));
		    $(this).removeAttr("data-original");
		});
	},
	focusStyleThumbPopup:function(id){
		$('.fancybox-inner .FocusStyle').removeClass('FocusStyle');
		$('.fancybox-inner #li'+id).addClass('FocusStyle');
	},
	imageLoadingShow : function() {
		$('.fancybox-inner #bigImages').hide();
		$('.fancybox-inner #loadingImages').show();
	},
	openMapOnImage : function() {
		VTMapUtil.loadMapResource(function(){
			var temp = EquipStatisticChecking.lstImages.get(EquipStatisticChecking.indexCurrent);
			if(temp!=null){
				if($('.OnFucnStyle').is(':visible')){					
					//$('.OffFucnStyle').hide();
					//$('.OffFucnStyle').css('display','none');
					//$('.OnFucnStyle').show();
					//$('.OnFucnStyle').css('display','block');
					$('.fancybox-inner #divMapSection').removeClass('SmallMapSection');
					$('.fancybox-inner #divMapSection').addClass('LargeMapSection');
					$('.fancybox-inner #bigMap').css('width','488px');
					$('.fancybox-inner #bigMap').css('height','409px');
					ViettelMap.loadMapForEquipmentImages('bigMap',temp.lat,temp.lng,13,1, temp.custLat, temp.custLng);
				}
				else {
					//$('.OnFucnStyle').hide();
					//$('.OnFucnStyle').css('display','none');
					//$('.OffFucnStyle').show();
					//$('.OffFucnStyle').css('display','block');
					$('.fancybox-inner #divMapSection').removeClass('LargeMapSection');
					$('.fancybox-inner #divMapSection').addClass('SmallMapSection');
					$('.fancybox-inner #bigMap').css('width','140px');
					$('.fancybox-inner #bigMap').css('height','120px');
					ViettelMap.loadMapForEquipmentImages('bigMap',temp.lat,temp.lng,13,0, temp.custLat, temp.custLng);
				}
			}
		});
	},
	showBigImagePopup:function(id){
		try {
			var temp=EquipStatisticChecking.lstImages.get(id);
			if(temp!=null){
				EquipStatisticChecking.indexCurrent = temp.id;
				EquipStatisticChecking.focusStyleThumbPopup(temp.id);
				EquipStatisticChecking.imageLoadingShow();
				//EquipStatisticChecking.openMapOnImage();
				$('.fancybox-inner .DownloadPhotoLink').hide();
				$('.fancybox-inner #shopImage').html(Utils.XSSEncode(temp.shopCode)+' - '+Utils.XSSEncode(temp.shopName));
				$('.fancybox-inner #staffImage').html(Utils.XSSEncode(temp.staffCode)+' - '+Utils.XSSEncode(temp.staffName));
				$('.fancybox-inner #codeEquipImage').html('Mã thiết bị: ' + Utils.XSSEncode(temp.equipCode) + ' / Số serial: ' + Utils.XSSEncode(temp.seriNumber));
				$('.fancybox-inner #typeEquipImage').html('Loại thiết bị: ' + Utils.XSSEncode(temp.equipCategoryName) + ' / Nhóm thiết bị: ' + Utils.XSSEncode(temp.equipNameGroup));
				$('.fancybox-inner #timeImage').html(temp.dmyDate+' - '+temp.hhmmDate);
				$('.fancybox-inner .DownloadPhotoLink').attr('href',temp.urlImage);
				$('.fancybox-inner .DownloadPhotoLink').attr('download',temp.urlEx.substring(temp.urlEx.lastIndexOf("/")+1));
//				if($('#tab4').hasClass('Active')){
//					$('.fancybox-inner #divDetail').attr('value',temp.id);
//					if (temp.result != null && temp.result == 1) {
//						$('.fancybox-inner #chkResult').attr('checked', 'checked');
//					} else if(temp.result != null && temp.result == 0) {
//						$('.fancybox-inner #chkNotResult').attr('checked', 'checked');
//					} else {
//						$('.fancybox-inner #chkNotResult').removeAttr('checked');
//						$('.fancybox-inner #chkResult').removeAttr('checked');
//					}
//					if (temp.isInSpected != null && temp.isInSpected == 1) {
//						$('.fancybox-inner #chkInspect').attr("checked", "checked");
//						$('.fancybox-inner #chkInspect').attr("disabled", "disabled");
//						$('.fancybox-inner #chkResult').attr("disabled", "disabled");
//						$('.fancybox-inner #chkNotResult').attr("disabled", "disabled");
//					} 
//					if (temp.numberProduct != null) {
//						$('.fancybox-inner #txtNumberFace').val(temp.numberProduct);
//					} else {
//						$('.fancybox-inner #txtNumberFace').val("");
//					}
//					if (temp.POSM != null && temp.POSM != "") {
//						$('.fancybox-inner #txtPOSM').val(temp.POSM);
//					} else {
//						$('.fancybox-inner #txtPOSM').val("");
//					}
//					if (temp.displayNote != null && temp.displayNote != "") {
//						$('.fancybox-inner #txtNoted').val(temp.displayNote);
//					} else {
//						$('.fancybox-inner #txtNoted').val("");
//					}
//				}
				EquipStatisticChecking.downloadFile(temp, false);//lay base64
				$('.DeletePhotoLink').attr('value',temp.id);
				var currentMil = new Date().getTime();
				var image = temp.urlImage+'?v='+currentMil;
				if (image != null && image != '' && image != undefined) {
					if (document.images) {
						var img = new Image();
						img.src = image;
						img.onload = function() {
							$('.fancybox-inner #bigImages').attr('src',image);
							EquipStatisticChecking.imageBigShow();
						};
					}
				}
			}
		} catch (err) {
			return;
		}
	},
	imageSelect:function() {
		var id=$(this).attr('value');
		if($('#minimizePopup').length != 0) {//dialog nho
			var func = $('#minimizePopup img').attr('onclick');
			if(func.indexOf('Full') != -1) {
				func = 'EquipStatisticChecking.showDialogFancyFull()';
			} else {
				func = 'EquipStatisticChecking.showDialogFancy()';
			}
			$('#minimizePopup img').attr('onclick', func);
		} else {//dialog lon
			var func = $('#maximizePopup img').attr('onclick');
			if(func.indexOf('Full') != -1) {
				func = 'EquipStatisticChecking.showDialogFancyFull()';
			} else {
				func = 'EquipStatisticChecking.showDialogFancy()';
			}
			$('#maximizePopup img').attr('onclick', func);
		}
		EquipStatisticChecking.showBigImagePopup(id);
	},
	fillThumbnails:function(list){
		var listImage = '';
		for (var i = 0; i < list.keyArray.length; i++) {
			var temp = list.get(list.keyArray[i]);
			if(temp!=null){
				var currentMil = new Date().getTime();
				listImage += '<li id="li'+temp.id+'">';
				listImage +='<div class="PhotoThumbnails BoxMiddle"><span class="BoxFrame"><span class="BoxMiddle"><img id="img'+temp.id+'" class="imageSelect ImageFrame2" src="/resources/images/grey.jpg"'; 
				listImage +=' data-original="'+(temp.urlThum+'?v='+currentMil)+'" width="142" height="102" value="'+temp.id+'" /></span></span></div>';
				listImage +='<p>'+temp.dmyDate+' – '+temp.hhmmDate+'</p>';
				listImage += "</li>";
			}
		}
		$('.fancybox-inner #listImageUL').append(listImage);
		EquipStatisticChecking.lazyLoadImages(".fancybox-inner .imageSelect",".fancybox-inner #listImage");
		$('.imageSelect').unbind('click');
		$('.imageSelect').click(EquipStatisticChecking.imageSelect);
		if(list.keyArray.length>0){
			setTimeout(function(){EquipStatisticChecking.showBigImagePopup(list.keyArray[0]);},500);
		}
	},
	getImagesForPopup:function(callBack,countPage){
		var data = EquipStatisticChecking._paramListStatistic;
		if (countPage != undefined && countPage != null && countPage != 0) {
			data.max = 20;
			data.page = 1;
			EquipStatisticChecking.pagePopup = countPage;
		} else {
			data.max = 20;
			data.page = EquipStatisticChecking.pagePopup;
		}
		if (data.page == 0) { 
			scrollIntoViewPopup=1;
		}
		if (callBack != undefined && callBack != null) {
			EquipStatisticChecking.callBack=callBack;
		}
		$('.loadingPopup').show();
		var kData = $.param(data, true);
		$.ajax({
			type : 'POST',
			url : '/equipment-checking/get-images-for-popup',
			data :(kData),
			dataType : "json",
			success : function(data) {
				$('.loadingPopup').hide();
				if(data.error){
					
				}else{
					var lstImage = data.lstImage;
					var listNew=new Map();
					if(lstImage!=undefined && lstImage!=null && lstImage.length>0) {
						EquipStatisticChecking.pagePopup++;//nếu có dữ liệu thì + trang lên 1
						for(var i=0;i<lstImage.length;i++){
							var currentMil = new Date().getTime();
							lstImage[i].urlEx=lstImage[i].urlImage;
							lstImage[i].urlImage=imgServerSOPath+lstImage[i].urlImage+'?v='+currentMil;
							lstImage[i].urlThum=imgServerSOPath+lstImage[i].urlThum+'?v='+currentMil;
							if(lstImage[i].shopCode==undefined || lstImage[i].shopCode==null) lstImage[i].shopCode='';
							if(lstImage[i].shopName==undefined || lstImage[i].shopName==null) lstImage[i].shopName='';
							if(lstImage[i].staffCode==undefined || lstImage[i].staffCode==null) lstImage[i].staffCode='';
							if(lstImage[i].staffName==undefined || lstImage[i].staffName==null) lstImage[i].staffName='';
							if(EquipStatisticChecking.lstImages.get(lstImage[i].id)==null){
								listNew.put(lstImage[i].id,lstImage[i]);
							}
							if(i==0){
								EquipStatisticChecking.indexCurrent = lstImage[i].id;
							}
							EquipStatisticChecking.lstImages.put(lstImage[i].id,lstImage[i]);							
							var temp = EquipStatisticChecking.lstImages.get(EquipStatisticChecking.indexCurrent);
							if (EquipStatisticChecking.customerCodeSearch != null && EquipStatisticChecking.customerCodeSearch != '') {
								$('#titlePopup').html(Utils.XSSEncode(EquipStatisticChecking.customerCodeSearch));
							} else {
								$('#titlePopup').html(Utils.XSSEncode(temp.shopCode + ' - ' + temp.shopName));
							};
						}
					} else {
						if(EquipStatisticChecking.pagePopup == 0){
							$.fancybox.close();
						}
						if(EquipStatisticChecking.callBack != undefined && EquipStatisticChecking.callBack != null){
							EquipStatisticChecking.callBack.call();
						}
					}
					EquipStatisticChecking.fillThumbnails(listNew);
				}
			},
			error:function(XMLHttpRequest, textStatus, errorThrown) {
			}
		});
	},
	showDialogFancy:function() {
		/*if($('.OffFucnStyle').is(':visible')){
			$('.OffFucnStyle').hide();
			$('.OffFucnStyle').css('display','none');
		}else{
			if($('.OnFucnStyle').is(':visible')){
				$('.OnFucnStyle').hide();
				$('.OnFucnStyle').css('display','none');	
			}
		}*/
		var nameId='popup-images';
		var title = '';
		title +='<span id="titlePopup"></span>';
		title += '<img class="loadingPopup" src="/resources/images/loading.gif" width="16" height="16" alt="Đang tải" class="LoadingStyle" />';
		title += '<span id="maximizePopup" class="Maximize"><img onclick="EquipStatisticChecking.showDialogFancyFull()" src="/resources/images/dialog/maximize-window.gif" width="19" height="19" alt="Phóng to" style="float: right; cursor: pointer; margin-top: -5px; padding-right: 33px;" /></span>';
		var html = $('#' + nameId).html();
		$('#' + nameId).html('');
		
		$.fancybox(html, {
			'title' : title,
			'closeBtn' : true,
			width : '1300px',
			height : '900px',
			helpers : {
				title : {
					type : 'inside'
				}
			},
			'afterShow' : function() {
				EquipStatisticChecking.pagePopup = 1;
				EquipStatisticChecking.lstImages = new Map();
				EquipStatisticChecking.getImagesForPopup();
				//EquipStatisticChecking.openMapOnImage();
				EquipStatisticChecking.scrollGetImageForPopup = 1;
				Utils.bindFormatOnTextfield("txtNumberFace", Utils._TF_NUMBER, ".fancybox-inner ");
				$('.fancybox-inner #listImage').scroll(function(){
				   if (EquipStatisticChecking.scrollGetImageForPopup == 1 && EquipStatisticChecking.isEndImages == 0 && $(this).outerHeight() > ($(this).get(0).scrollHeight - $(this).scrollTop()-2)){
					   EquipStatisticChecking.scrollGetImageForPopup = 0;
					   EquipStatisticChecking.getImagesForPopup();
				   }
				});				
			},
			'afterClose' : function() {
				$('#' + nameId).html(html);
			},
			'beforeClose': function () {
			}
		});
		return false;
	},
	onOffMap:function(i){
		if(i==0){
			$('.OffFucnStyle').hide();
			$('.OffFucnStyle').css('display','none');
			$('.OnFucnStyle').show();
			$('.OnFucnStyle').css('display','block');
			$('.fancybox-inner #divMapSection').removeClass('SmallMapSection');
			$('.fancybox-inner #divMapSection').addClass('LargeMapSection');
			$('.fancybox-inner #bigMap').css('width','488px');
			$('.fancybox-inner #bigMap').css('height','409px');
		}else{
			$('.OnFucnStyle').hide();
			$('.OnFucnStyle').css('display','none');
			$('.OffFucnStyle').show();
			$('.OffFucnStyle').css('display','block');
			$('.fancybox-inner #divMapSection').removeClass('LargeMapSection');
			$('.fancybox-inner #divMapSection').addClass('SmallMapSection');
			$('.fancybox-inner #bigMap').css('width','140px');
			$('.fancybox-inner #bigMap').css('height','120px');
		}
		EquipStatisticChecking.openMapOnImage();
	},
	showDialogFancyFull:function(mediaItemId,customerId) {
		if($('.OffFucnStyle').is(':visible')){
			$('.OffFucnStyle').hide();
			$('.OffFucnStyle').css('display','none');
		} else {
			if($('.OnFucnStyle').is(':visible')){
				$('.OnFucnStyle').hide();
				$('.OnFucnStyle').css('display','none');	
			}
		}
		
		var nameId='popup-images-full';
		var title = '';
		title +='<span id="titlePopup"></span>';
		title += '<img class="loadingPopup" src="/resources/images/loading.gif" width="16" height="16" alt="Đang tải" class="LoadingStyle" />';
		title += '<span id="minimizePopup" class="Minimize"><img src="/resources/images/dialog/minimize-window.gif" onclick="EquipStatisticChecking.showDialogFancy()" width="19" height="19" alt="Phóng to" style="float: right; cursor: pointer; margin-top: -5px; padding-right: 33px;" /></span>';
		var html = $('#' + nameId).html();
		$('#' + nameId).html('');
		
		$.fancybox(html, {
			'title' : title,
			'closeBtn' : true,
			width : '1300px',
			height : '900px',
			helpers : {
				title : {
					type : 'inside'
				}
			},
			'afterShow' : function() {
				EquipStatisticChecking.pagePopup = 1;
				EquipStatisticChecking.lstImages = new Map();
				EquipStatisticChecking.getImagesForPopup();
				//EquipStatisticChecking.openMapOnImage();		
				EquipStatisticChecking.scrollGetImageForPopup = 1;
				Utils.bindFormatOnTextfield("txtNumberFace", Utils._TF_NUMBER, ".fancybox-inner ");
				$('.fancybox-inner #listImage').scroll(function(){
				   if (EquipStatisticChecking.scrollGetImageForPopup == 1 && EquipStatisticChecking.isEndImages == 0 && $(this).outerHeight() > ($(this).get(0).scrollHeight - $(this).scrollTop()-2)){
					   EquipStatisticChecking.scrollGetImageForPopup = 0;
					   EquipStatisticChecking.getImagesForPopup();
				   }
				});
			},
			'afterClose' : function() {
				$('#' + nameId).html(html);
			},
			'beforeClose': function () {
			}
		});
		return false;
	},
	showImageNextPre:function(type){//1 là next, 0 là pre
		var flag=0;
		for(var i=0;i<EquipStatisticChecking.lstImages.keyArray.length;i++){
			if(EquipStatisticChecking.lstImages.keyArray[i]==EquipStatisticChecking.indexCurrent){
				if(type==1 && i+1<EquipStatisticChecking.lstImages.keyArray.length){//next
					scrollIntoViewPopup=1;
					EquipStatisticChecking.showBigImagePopup(EquipStatisticChecking.lstImages.keyArray[i+1]);
					flag=1;
				}else if(type==0 && i>0){
					scrollIntoViewPopup=1;
					EquipStatisticChecking.showBigImagePopup(EquipStatisticChecking.lstImages.keyArray[i-1]);
				}
				break;
			}
		}
		if(type==1 && flag==0 && EquipStatisticChecking.isEndImages==0){//nếu bấm next mà trong ds hiện tại ko có thì request lấy thêm hình
			EquipStatisticChecking.indexCurrent=null;
			scrollIntoViewPopup=1;
			EquipStatisticChecking.getImagesForPopup();
		}
	},
	rotateImage:function(t){
		var id=EquipStatisticChecking.indexCurrent;
		var temp = EquipStatisticChecking.lstImages.get(id);		
		if(temp!=null){			
			var data = new Object();
			data.id=temp.id;				
			kData = $.param(data, true);
			$('#divOverlay').show();
			$.ajax({
				type : "POST",
				url : '/equipment-checking/rotate-image', 
				data :(kData),
				dataType : "json",
				success : function(result) {
					$('#divOverlay').hide();
					if(!result.error){
						var currentMil = new Date().getTime();
						var url = result.url + '?v=' + currentMil;
						var urlThumbnail = result.thumbnailUrl + '?v=' + currentMil;
						$('#img'+EquipStatisticChecking.indexCurrent).attr('data-original', urlThumbnail);
						$('#img'+EquipStatisticChecking.indexCurrent).attr('src', urlThumbnail);
						$('#imagesOfAlbum_'+EquipStatisticChecking.indexCurrent).attr('data-original', urlThumbnail);
						$('#imagesOfAlbum_'+EquipStatisticChecking.indexCurrent).attr('src', urlThumbnail);
						$('#imagesOfAlbulmDetail_'+EquipStatisticChecking.indexCurrent).attr('data-original', urlThumbnail);
						$('#imagesOfAlbulmDetail_'+EquipStatisticChecking.indexCurrent).attr('src', urlThumbnail);
						$('#bigImages').attr('data-original', urlThumbnail);
						$('#bigImages').attr('src',urlThumbnail);
						var image = url;
						if (image != null && image != '' && image != undefined) {
							if (document.images) {
								var img = new Image();
								img.src = image;
								img.onload = function() {
									$('.fancybox-inner #bigImages').attr('src',image);
									EquipStatisticChecking.imageBigShow();
								};
							}
						}						
						EquipStatisticChecking.downloadFile(temp, true);
					}else{
						$('.fancybox-inner #errMsgPopup').html('Lỗi hệ thống').show();
						setTimeout(function(){$('.fancybox-inner #errMsgPopup').hide();},5000);
					}
				}
			});
			
		}
	},
	/**
	 * xuat file excel bien ban kiem ke
	 * @author tamvnm
	 * @since 16/01/2015
	 */
	exportExcel: function(){
		$('.errMsg').html("").hide();
		var arra = EquipStatisticChecking._selectingEquipmentStatistics.valArray;
		var lstChecked = new Array();
		if(arra == null || arra.length == 0) {
			$('#errMsg').html('Vui lòng chọn kiểm kê đã hoàn thành để xuất excel.').show();
			return;
		}		
		for(var i=0, size = arra.length; i < size; ++i) {
			lstChecked.push(arra[i].id);
		}
		var rows = $('#grid').datagrid('getRows');	
		if(rows.length==0) {
			$('#errMsg').html('Không có biên bản để xuất excel').show();
			return;
		}
//		if(lstChecked.length == 0) {
//			$('#errMsg').html('Vui lòng chọn kiểm kê đã hoàn thành để xuất excel.').show();
//			return;
//		}	
		
		
		var params = new Object();
		params.lstIdRecord = lstChecked;
		$.messager.confirm('Xác nhận','Bạn có muốn xuất file excel?',function(r){
			if(r){
				ReportUtils.exportReport('/equipment-checking/export-record-checking', params, 'errMsg');					
			}
		});		
		return false;
	},
	/**
	 * show popup import excel
	 * @author nhutnn
	 * @since 27/01/2015
	 */
	showDlgImportExcel: function(){
//		if ($("#easyuiPopupImportExcel1").length > 1) {
//			$("#easyuiPopupImportExcel1").dialog("destroy");
//		}
		$('#easyuiPopupImportExcel1').show();
		$('.ErrorMsgStyle').hide();
		$('#easyuiPopupImportExcel1').dialog({  
	        closed: false,  
	        cache: false,  
	        modal: true,
	        height: 'auto',
	        onOpen: function(){	 
	        	$('.easyui-dialog .BtnGeneralStyle').css('color','');
	        },
	        onClose: function(){
				$("#gridLiquidation").datagrid('uncheckAll');
				$("#fakefilepcDVKK1").val("");
				$("#excelFileDVKK1").val("");				
	        }
		});
	},
	showDlgImportCustomerExcel: function(){
//		if ($("#easyuiPopupImportExcel").length > 1) {
//			$("#easyuiPopupImportExcel").dialog("destroy");
//		}
		$('#easyuiPopupImportExcel').show();
		$('.ErrorMsgStyle').hide();
		$('#easyuiPopupImportExcel').dialog({  
	        closed: false,  
	        cache: false,  
	        modal: true,
	        height: 'auto',
	        onOpen: function(){	 
	        	$('.easyui-dialog .BtnGeneralStyle').css('color','');
	        },
	        onClose: function(){
				$("#gridLiquidation").datagrid('uncheckAll');
				$("#fakefilepcDVKK").val("");
				$("#excelFileDVKK").val("");				
	        }
		});
	},
	showDlgImportStaffExcel: function(){
//		if ($("#easyuiPopupImportExcelStaff").length > 1) {
//			$("#easyuiPopupImportExcelStaff").dialog("destroy");
//		}
		$('#easyuiPopupImportExcelStaff').show();
		$('.ErrorMsgStyle').hide();
		$('#easyuiPopupImportExcelStaff').dialog({  
	        closed: false,  
	        cache: false,  
	        modal: true,
	        height: 'auto',
	        onOpen: function(){	 
	        	$('.easyui-dialog .BtnGeneralStyle').css('color','');
	        },
	        onClose: function(){
//				$("#gridLiquidation").datagrid('uncheckAll');
				$("#fakefilepcDVKKStaff").val("");
				$("#excelFileDVKKStaff").val("");				
	        }
		});
	},
	showDlgImportCustomerExcelEx: function(){
		$('#easyuiPopupImportExcelEx').show();
		$('.ErrorMsgStyle').hide();
		$('#easyuiPopupImportExcelEx').dialog({  
			closed: false,  
			cache: false,  
			modal: true,
			height: 'auto',
			onOpen: function(){	 
				$('.easyui-dialog .BtnGeneralStyle').css('color','');
			},
			onClose: function(){
				$("#gridLiquidation").datagrid('uncheckAll');
				$("#fakefilepcDVKKEx").val("");
				$("#excelFileDVKKEx").val("");				
			}
		});
	},
	/**
	 * import excel DVKK
	 * @author nhutnn
	 * @since 27/01/2015
	 */
	importExcelDVKK: function(){
		$('.ErrorMsgStyle').hide();
		var params = new Object();
		params.id = $("#id").val();
		VTUtilJS.importExcelUtils(function(data) {
			$("#errExcelMsgDVKK1").html(data.message).show();		
			if(data.fileNameFail == null || data.fileNameFail == "" ){					
				setTimeout(function(){
					$('#errExcelMsgDVKK1').html("").hide();
					$('#easyuiPopupImportExcel1').dialog('close');
				}, 3000);				
			}else{
				$("#fakefilepcDVKK1").val("");
				$("#excelFileDVKK1").val("");			
			}
			var p = {
				recordId : $("#id").val(),
				shopSearchCode: $("#txtCodeShop").val().trim(),
				shopSearchName: $("#txtNameShop").val().trim()
			};
			$("#gridShop").treegrid({url: "/equipment-checking/edit-shop/search",queryParams : p});
		}, "importFrmDVKK1", "excelFileDVKK1", null, "errExcelMsgDVKK1", params);
		return false;
	},
	importExcelCustomer: function(){
		$('.ErrorMsgStyle').hide();
		var params = new Object();
		params.id = $("#id").val();
		VTUtilJS.importExcelUtils(function(data) {
			$("#errExcelMsgDVKK").html(data.message).show();		
			if(data.fileNameFail == null || data.fileNameFail == "" ){					
				setTimeout(function(){
					$('#errExcelMsgDVKK').html("").hide();
					$('#easyuiPopupImportExcel').dialog('close');
				}, 3000);				
			}else{
				$("#fakefilepcDVKK").val("");
				$("#excelFileDVKK").val("");			
			}
			EquipStatisticChecking.searchCustomer();
		}, "importFrmDVKK", "excelFileDVKK", null, "errExcelMsgDVKK", params);
		return false;
	},
	importExcelStaff: function(){
		$('.ErrorMsgStyle').hide();
		var params = new Object();
		params.id = $("#id").val();
		VTUtilJS.importExcelUtils(function(data) {
			$("#errExcelMsgDVKKStaff").html(data.message).show();		
			if(data.fileNameFail == null || data.fileNameFail == "" ){					
				setTimeout(function(){
					$('#errExcelMsgDVKKStaff').html("").hide();
					$('#easyuiPopupImportExcelStaff').dialog('close');
				}, 3000);				
			}else{
				$("#fakefilepcDVKKStaff").val("");
				$("#excelFileDVKKStaff").val("");			
			}
			EquipStatisticChecking.searchCustomer();
		}, "importFrmDVKKStaff", "excelFileDVKKStaff", null, "errExcelMsgDVKKStaff", params);
		return false;
	},
	importExcelEquip: function(){
		$('.ErrorMsgStyle').hide();
		var params = new Object();
		params.id = $("#id").val();
		VTUtilJS.importExcelUtils(function(data) {
			$("#errExcelMsgDVKKEx").html(data.message).show();		
			if(data.fileNameFail == null || data.fileNameFail == "" ){					
				setTimeout(function(){
					$('#errExcelMsgDVKKEx').html("").hide();
					$('#easyuiPopupImportExcelEx').dialog('close');
				}, 3000);				
			}else{
				$("#fakefilepcDVKKEx").val("");
				$("#excelFileDVKKEx").val("");			
			}
			EquipStatisticChecking.searchGridEquip();
		}, "importFrmDVKKEx", "excelFileDVKKEx", null, "errExcelMsgDVKKEx", params);
		return false;
	},
	/**
	 * xuat file excel DVKK
	 * @author nhutnn
	 * @since 27/01/2015
	 */
	exportExcelDVKK: function() {
		$('.ErrorMsgStyle').html("").hide();
		// var rows = $('#gridLiquidation').datagrid('getRows');	
		// if(rows.length==0) {
		// 	$('#errMsgInfo').html('Không có biên bản để xuất excel').show();
		// 	return;
		// }
		var params = {
			id: $("#id").val(),
			shopSearchCode: $("#txtCodeShop").val(),
			shopSearchName: $("#txtNameShop").val()
		};
		$.messager.confirm('Xác nhận', 'Bạn có muốn xuất file excel?', function(r) {
			if (r) {
				ReportUtils.exportReport('/equipment-checking/edit-shop-export-excel', params, 'errorMsg');
			}
		});
		return false;
	},
	/*
	* export customer tab
	*/
	exportExcelCustomer: function(){
		$('.ErrorMsgStyle').html("").hide();
		var params = new Object();
		params.id = $("#id").val();
		params.customerCode = $('#customerCode').val().trim();
		params.customerName =  $('#customerName').val().trim();
		params.statusCus =  $('#statusCus').val().trim();
		$.messager.confirm('Xác nhận','Bạn có muốn xuất file excel?',function(r){
			if(r){
				ReportUtils.exportReport('/equipment-checking/export-customer?recordId=' + $('#id').val(), params, 'errorMsg');					
			}
		});
		return false;
	},
	exportExcelStaff: function(){
		$('.ErrorMsgStyle').html("").hide();
		var params = new Object();
		params.id = $("#id").val();
		$.messager.confirm('Xác nhận','Bạn có muốn xuất file excel?',function(r){
			if(r){
				ReportUtils.exportReport('/equipment-checking/export-staff', params, 'errorMsg');					
			}
		});
		return false;
	},
	exportExcelEquip: function(){
		$('.ErrorMsgStyle').html("").hide();
		var params = new Object();
		params.equipCode = $('#equipCode').val().trim();	
		params.equipGroupName = $('#equipGroupName').val().trim();	
		params.id = $('#id').val();
		params.statusCus = $('#statusCustomer').val().trim();
		$.messager.confirm('Xác nhận','Bạn có muốn xuất file excel?',function(r){
			if(r){
				ReportUtils.exportReport('/equipment-checking/export-equip', params, 'errorMsg');					
			}
		});
		return false;
	},
	deleteCustomerRecord : function(id) {
		$(".ErrorMsgStyle").hide();
		$.messager.confirm("Xác nhận", "Bạn có muốn xóa khách hàng?", function(r) {
			if (r) {
				var params = {
					id: id
				};
				VTUtilJS.postFormJson(params, '/equipment-checking/delete-customer', function(data) {
					if(data.error) {
						$('errorMsg').html(data.errMsg).show();
					} else {
						$('#successMsg').html('Lưu dữ liệu thành công').show();
						setTimeout(function() {
							$('#successMsg').html('').hide();
						}, 3000);
						EquipStatisticChecking.searchCustomer();
					}
				});
			}
		});
	},
	showDlgImportExcelKK: function(){
//		if ($("#easyuiPopupImportExcel").length > 1) {
//			$("#easyuiPopupImportExcel").dialog("destroy");
//		}
		$('#easyuiPopupImportExcel').show();
		$('.ErrorMsgStyle').hide();
		$('#easyuiPopupImportExcel').dialog({  
	        closed: false,  
	        cache: false,  
	        modal: true,
	        height: 'auto',
	        onOpen: function(){	 
	        	$('.easyui-dialog .BtnGeneralStyle').css('color','');
	        },
	        onClose: function(){
				$("#fakefilepcBBTL").val("");
				$("#excelFileBBTL").val("");				
	        }
		});
	},
	/**
	 * import excel kiem ke thiet bi so lan
	 * @author phuongvm
	 * @since 24/04/2015
	 */
	importExcelProductSoLan: function(){
		$('.ErrorMsgStyle').hide();
		var params = new Object();
		params.id = $("#id").val();
		VTUtilJS.importExcelUtils(function(data) {
			$("#errExcelMsgBBTL").html(data.message).show();		
			if(data.fileNameFail == null || data.fileNameFail == "" ){					
				setTimeout(function(){
					$('#errExcelMsgBBTL').html("").hide();
					$('#easyuiPopupImportExcel').dialog('close');
				}, 3000);				
			}else{
				$("#fakefilepcBBTL").val("");
				$("#excelFileBBTL").val("");			
			}						
		}, "importFrmBBTL", "excelFileBBTL", null, "errExcelMsgBBTL",params);
		return false;
	},
	/**
	 * import excel kiem ke thiet bi theo tuyen
	 * @author phuongvm
	 * @since 27/04/2015
	 */
	importExcelProductTuyen: function(){
		$('.ErrorMsgStyle').hide();
		var params = new Object();
		params.id = $("#id").val();
		VTUtilJS.importExcelUtils(function(data) {
			$("#errExcelMsgBBTL").html(data.message).show();		
			if(data.fileNameFail == null || data.fileNameFail == "" ){					
				setTimeout(function(){
					$('#errExcelMsgBBTL').html("").hide();
					$('#easyuiPopupImportExcel').dialog('close');
				}, 3000);				
			}else{
				$("#fakefilepcBBTL").val("");
				$("#excelFileBBTL").val("");			
			}						
		}, "importFrmBBTL", "excelFileBBTL", null, "errExcelMsgBBTL",params);
		return false;
	},
	/**
	 * import excel kiem ke u ke theo so lan
	 * @author phuongvm
	 * @since 27/04/2015
	 */
	importExcelShelfSoLan: function(){
		$('.ErrorMsgStyle').hide();
		var params = new Object();
		params.id = $("#id").val();
		VTUtilJS.importExcelUtils(function(data) {
			$("#errExcelMsgBBTL").html(data.message).show();		
			if(data.fileNameFail == null || data.fileNameFail == "" ){					
				setTimeout(function(){
					$('#errExcelMsgBBTL').html("").hide();
					$('#easyuiPopupImportExcel').dialog('close');
				}, 3000);				
			}else{
				$("#fakefilepcBBTL").val("");
				$("#excelFileBBTL").val("");			
			}						
		}, "importFrmBBTL", "excelFileBBTL", null, "errExcelMsgBBTL",params);
		return false;
	},
	/**
	 * import excel kiem ke u ke theo tuyen
	 * @author phuongvm
	 * @since 04/05/2015
	 */
	importExcelShelfTuyen: function(){
		$('.ErrorMsgStyle').hide();
		var params = new Object();
		params.id = $("#id").val();
		VTUtilJS.importExcelUtils(function(data) {
			$("#errExcelMsgBBTL").html(data.message).show();		
			if(data.fileNameFail == null || data.fileNameFail == "" ){					
				setTimeout(function(){
					$('#errExcelMsgBBTL').html("").hide();
					$('#easyuiPopupImportExcel').dialog('close');
				}, 3000);				
			}else{
				$("#fakefilepcBBTL").val("");
				$("#excelFileBBTL").val("");			
			}						
		}, "importFrmBBTL", "excelFileBBTL", null, "errExcelMsgBBTL",params);
		return false;
	},
	/**
	 * xuat file excel chi tiet tung bien ban kiem ke
	 * @author phuongvm
	 * @since 24/04/2015
	 */
	exportExcelDetail: function(){
		$('.errMsg').html("").hide();
		var lstChecked = new Array();
		if($('#id').val()!=null && $('#id').val()!=undefined){
			lstChecked.push($('#id').val().trim());
		}
		var params = new Object();
		params.lstIdRecord = lstChecked;
		$.messager.confirm('Xác nhận','Bạn có muốn xuất file excel?',function(r){
			if(r){
				ReportUtils.exportReport('/equipment-checking/export-record-checking', params, 'errMsg');					
			}
		});		
		return false;
	},
	/**
	 * xuat file excel chi tiet tung bien ban kiem ke nhom thiet bi - so lan
	 * @author phuongvm
	 * @since 22/05/2015
	 */
	exportExcelDetailGroupSoLan: function(){
		$('.ErrorMsgStyle').html("").hide();
		var lstChecked = new Array();
		if($('#id').val()!=null && $('#id').val()!=undefined){
			lstChecked.push($('#id').val().trim());
		}
		if(EquipStatisticChecking.filter!=null){
			EquipStatisticChecking.filter.lstIdRecord = lstChecked;
		}else{
			$('#errorMsg').html("Không có dữ liệu xuất !").show();
			return false;
		}
		$.messager.confirm('Xác nhận','Bạn có muốn xuất file excel?',function(r){
			if(r){
				ReportUtils.exportReport('/equipment-checking/export-record-checking-group-solan', EquipStatisticChecking.filter, 'errMsg');					
			}
		});		
		return false;
	},
	/**
	 * xuat file excel chi tiet tung bien ban kiem ke nhom thiet bi - tuyen
	 * @author phuongvm
	 * @since 22/05/2015
	 */
	exportExcelDetailGroupTuyen: function(){
		$('.ErrorMsgStyle').html("").hide();
		var lstChecked = new Array();
		if($('#id').val()!=null && $('#id').val()!=undefined){
			lstChecked.push($('#id').val().trim());
		}
		if(EquipStatisticChecking.filter!=null){
			EquipStatisticChecking.filter.lstIdRecord = lstChecked;
		}else{
			$('#errorMsg').html("Không có dữ liệu xuất !").show();
			return false;
		}
		$.messager.confirm('Xác nhận','Bạn có muốn xuất file excel?',function(r){
			if(r){
				ReportUtils.exportReport('/equipment-checking/export-record-checking-group-tuyen', EquipStatisticChecking.filter, 'errMsg');					
			}
		});		
		return false;
	},
	/**
	 * xuat file excel chi tiet tung bien ban kiem ke u ke - so lan
	 * @author phuongvm
	 * @since 22/05/2015
	 */
	exportExcelDetailUKeSoLan: function(){
		$('.ErrorMsgStyle').html("").hide();
		var lstChecked = new Array();
		if($('#id').val()!=null && $('#id').val()!=undefined){
			lstChecked.push($('#id').val().trim());
		}
		if(EquipStatisticChecking.filter!=null){
			EquipStatisticChecking.filter.lstIdRecord = lstChecked;
		}else{
			$('#errorMsg').html("Không có dữ liệu xuất !").show();
			return false;
		}
		$.messager.confirm('Xác nhận','Bạn có muốn xuất file excel?',function(r){
			if(r){
				ReportUtils.exportReport('/equipment-checking/export-record-checking-uke-solan', EquipStatisticChecking.filter, 'errMsg');					
			}
		});		
		return false;
	},
	/**
	 * xuat file excel chi tiet tung bien ban kiem ke u ke - so lan
	 * @author phuongvm
	 * @since 22/05/2015
	 */
	exportExcelDetailUKeTuyen: function(){
		$('.ErrorMsgStyle').html("").hide();
		var lstChecked = new Array();
		if($('#id').val()!=null && $('#id').val()!=undefined){
			lstChecked.push($('#id').val().trim());
		}
		if(EquipStatisticChecking.filter!=null){
			EquipStatisticChecking.filter.lstIdRecord = lstChecked;
		}else{
			$('#errorMsg').html("Không có dữ liệu xuất !").show();
			return false;
		}
		$.messager.confirm('Xác nhận','Bạn có muốn xuất file excel?',function(r){
			if(r){
				ReportUtils.exportReport('/equipment-checking/export-record-checking-uke-tuyen', EquipStatisticChecking.filter, 'errMsg');					
			}
		});		
		return false;
	},
	searchCustomer : function() {
		$("#gridCustomer").datagrid('load', {customerCode : $('#customerCode').val().trim(), customerName : $('#customerName').val().trim(), statusCus: $('#statusCus').val().trim()});
	},
	
	/**
	 * Tim kiem thong tin kiem ke
	 * 
	 * @author hunglm16
	 * @since May 31,2015
	 * */
	searchListStatisticReturnThietBi: function () {
		$('.ErrorMsgStyle').html("").hide();
		var params = {
				id: $('#id').val(),
				shopCode: $('#shopCode').val(),
				customerCode: $('#customerCode').val(),
				staffCode: $('#staffCode').val(),
				deviceCode: $('#deviceCode').val(),
				seri: $('#seri').val(),
				status: $('#status').val(),
				fromDate: $('#fromDate').val(),
				toDate: $('#toDate').val(),
			};
		EquipStatisticChecking._paramListStatistic = params;
		$('#grid').datagrid('load', params);
	},
	// update author nhutnn, 05/08/2015
	bindEventInput: function(){
       $('td input').each(function(i,e){
             $(this).attr('id','inputText'+i).attr('maxlength','22');
             Utils.bindFormatOnTextfield('inputText'+i, Utils._TF_NUMBER);
             Utils.formatCurrencyFor('inputText'+i);
       });
       $('td input').keypress(function() {
       	Utils.onchangeFormatCurrencyKeyPress(this, 17);
       });
	},
	/**
	 * Show popup tim kiem thiet bi
	 * @author phuongvm
	 * @since 07/07/2015
	 */
	showPopupSearchEquipment: function(isSearch){
		$(".ErrorMsgStyle").hide();
		$('#easyuiPopupSearchEquipment').show();
		$('#easyuiPopupSearchEquipment').dialog({  
	        closed: false,  
	        cache: false,  
	        modal: true,
	        width : 1000,
	        height : 'auto',
	        onOpen: function(){
	        	//setDateTimePicker('yearManufacturing');1
	        	Utils.bindComboboxEquipGroupEasyUICodeName('groupEquipment', null, 193);
	        	Utils.bindComboboxEquipGroupEasyUICodeName('providerId', null, 193);
	        	var year = new Date();
	        	year = year.getFullYear(); 
	        	var op = '<option value="" selected="selected">Chọn</option><option value="'+year+'">'+year+'</option>';
	        	for(var i=1; i<100; i++){
	        		op+='<option value="'+(year-i)+'">'+(year-i)+'</option>';
	        	}
	        	$('#yearManufacturing').html(op).change();
	        	$('#equipmentCode, #seriNumber, #typeEquipment, #customerCode, #customerName, #customerAddress').bind('keyup',function(event){
	        		if(event.keyCode == keyCodes.ENTER){
	        			$('#btnSeachEquipmentDlg').click(); 
	        		}
	        	});
	        	$('#yearManufacturing').bind('keyup',function(event){
	        		if(event.keyCode == keyCodes.ENTER){
	        			$('#btnSeachEquipmentDlg').click(); 
	        		}
	        	});
	        	$('#groupEquipment, #providerId').next().find(".combo-text.validatebox-text").bind('keyup',function(event){
	        		if(event.keyCode == keyCodes.ENTER){
	        			$('#btnSeachEquipmentDlg').click(); 
	        		}
	        	});
	        	EquipmentManagerLiquidation.getEquipCatalog();
	        	EquipmentManagerLiquidation.getEquipGroup(null);
	        	var params = new Object();
	        	var groupId = $('#groupEquipment').combobox('getValue');
	        	if (groupId == -1 || groupId == "") {
	        		groupId = "";
	        	}
	        	var providerId = $('#providerId').combobox('getValue');
	        	if (providerId == -1 || providerId == "") {
	        		providerId = "";
	        	}
				params = {
						id: $("#id").val(),
						equipCode: $('#equipmentCode').val().trim(),
						seriNumber: $('#seriNumber').val().trim(),
						categoryId: $('#typeEquipment').val().trim(),
						// groupId: $('#groupEquipment').val().trim(),
						groupId: groupId,
						// providerId: $('#providerId').val().trim(),
						providerId: providerId,
						yearManufacture: $('#yearManufacturing').val().trim(),
//						stockCode: $('#stockCode').val().trim(),
						customerCode: $('#customerCode').val().trim(),
//						customerName: $('#customerName').val().trim(),
//						shopCode: $('#shopCode').val().trim(),
//						customerAddress: $('#customerAddress').val().trim(),
						status: activeType.RUNNING,
//						stockType: 1, //Don vi
//						tradeStatus: 0,
//						usageStatus: usageStatus.SHOWING_WAREHOUSE,//Dang o kho
						flagPagination: true
					};
				if($("#idRecordHidden").val() != "" && EquipmentManagerLiquidation._lstEquipInRecord != null){
					params.lstEquipDelete = "";
					for(var i=0; i<EquipmentManagerLiquidation._lstEquipInRecord.length; i++){
						params.lstEquipDelete += EquipmentManagerLiquidation._lstEquipInRecord[i]+',';
					}					
				}
				if(EquipmentManagerLiquidation._lstEquipInsert != null){
					params.lstEquipAdd = "";
					for(var i=0; i<EquipmentManagerLiquidation._lstEquipInsert.length; i++){
						params.lstEquipAdd += EquipmentManagerLiquidation._lstEquipInsert[i]+',';
					}					
				}
				$('#equipmentGridDialog').datagrid({
//					url : '/equipment-manage-liquidation/search-equipment-liquidation',
					url : '/commons/search-equip-by-shop-root-in-stock-ex',
					autoRowHeight : true,
					rownumbers : true, 
					//fitColumns:true,
					pagination:true,
					scrollbarSize:0,
					pageSize:10,
					pageList: [10],
					width: $('#equipmentGridDialogContainer').width(),
					height: 'auto',
					autoWidth: true,	
					queryParams: params,
					frozenColumns:[[
						{field: 'check',checkbox:true, width: 50, sortable:false,resizable:false, align: 'left'},
						{field: 'shopCode', title:'Đơn vị', width: 70, sortable:false, resizable:false, align: 'left', formatter: function(value, row, index) {
							return Utils.XSSEncode(value);
						}},
//						{field: 'stock',title:'Kho', width: 120, sortable:false, resizable:false, align: 'left', formatter: function(value, row, index) {
//							var html = "";
//							if(row.stockCode != null && row.stockName!= null){
//								html = row.stockCode+' - '+row.stockName; 
//							}
//							return Utils.XSSEncode(html);
//						}},	
						{field: 'shortCode', title:'Mã KH', width: 70, sortable:false, resizable:false, align: 'left', formatter: function(value, row, index) {
							return Utils.XSSEncode(value);
						}},
						{field: 'customerName', title:'Tên KH', width: 120, sortable:false, resizable:false, align: 'left', formatter: function(value, row, index) {
							return Utils.XSSEncode(value);
						}},	
						{field: 'address', title:'Địa chỉ', width: 200, sortable:false, resizable:false, align: 'left', formatter: function(value, row, index) {
							return Utils.XSSEncode(value);
						}},
						{field: 'equipmentCode', title:'Mã thiết bị', width: 120, sortable:false, resizable:false, align: 'left', formatter: function(value, row, index) {
							return Utils.XSSEncode(value);
						}},
					]],
					columns:[[
//						{field: 'seriNumber', title:'Số serial', width: 100, sortable:false, resizable:false, align: 'left', formatter: function(value, row, index) {
//							return Utils.XSSEncode(value);
//						}},
//						{field: 'healthStatus', title:'Tình trạng thiết bị', width: 100, sortable:false, resizable:false, align: 'left', formatter: function(value, row, index) {
//							return Utils.XSSEncode(value);
//						}},
					    {field: 'groupEquipment', title:'Nhóm thiết bị', width: 200, sortable:false, resizable:false, align: 'left', formatter: function(value, row, index) {
					    	var html="";
							if(row.groupEquipmentCode != null && row.groupEquipmentName!= null){
								html = row.groupEquipmentCode+' - '+row.groupEquipmentName; 
							}
							return Utils.XSSEncode(html);
						}},
						{field: 'typeEquipment', title:'Loại thiết bị', width: 120, sortable:false, resizable:false, align: 'left', formatter: function(value, row, index) {
							return Utils.XSSEncode(value);
						}},
//					    {field: 'capacity',title:'Dung tích (lít)', width: 80, sortable:false, resizable:false, align: 'center', formatter: function(value, row, index) {
//							return Utils.XSSEncode(value);
//						}},
//					    {field: 'equipmentBrand',title:'Hiệu', width: 80, sortable:false, resizable:false, align: 'left', formatter: function(value, row, index) {
//							return Utils.XSSEncode(value);
//						}},
//					    {field: 'equipmentProvider',title:'NCC',width: 80, sortable:false, resizable:false, align: 'left', formatter: function(value, row, index) {
//							return Utils.XSSEncode(value);
//						}},
//					    {field: 'yearManufacture', title:'Năm sản xuất', width: 80, sortable:false, resizable:false, align: 'right', formatter: function(value, row, index) {
//							return Utils.XSSEncode(value);
//						}},
					]],
					onBeforeLoad:function(param){											 
					},
					onLoadSuccess :function(data){   			 
						if (data == null || data.total == 0) {
							//$("#errMsgInfo").html("Không tìm thấy danh sách biên bản giao nhận thiết bị!").show();
						}else{
							$('.datagrid-header-rownumber').html('STT');		    	
						}	    
						$('#equipmentCode').focus();
						 updateRownumWidthForDataGrid('.easyui-dialog');
						var rows = $('#equipmentGridDialog').datagrid('getRows');
				    	$('.datagrid-header-check input').removeAttr('checked');
				    	for(var i = 0; i < rows.length; i++) {
				    		if(EquipStatisticChecking._lstEquipId.get(rows[i].equipmentId)!=null) {
				    			$('#equipmentGridDialog').datagrid('checkRow', i);
				    		}
				    	}
				    	setTimeout(function() {
			    			CommonSearchEasyUI.fitEasyDialog('easyuiPopupSearchEquipment');
			    		}, 1000);
					},
					onCheck:function(index,row){
						EquipStatisticChecking._lstEquipId.put(row.equipmentId,row);  
						
				    },
				    onUncheck:function(index,row){
				    	EquipStatisticChecking._lstEquipId.remove(row.equipmentId);
				    },
				    onCheckAll:function(rows){
					 	for(var i = 0;i<rows.length;i++){
				    		var row = rows[i];
				    		EquipStatisticChecking._lstEquipId.put(row.equipmentId,row);
				    	}
				    },
				    onUncheckAll:function(rows){
				    	for(var i = 0;i<rows.length;i++){
				    		var row = rows[i];
				    		EquipStatisticChecking._lstEquipId.remove(row.equipmentId);	
				    	}
				    }
				});
	        },
			onClose: function(){
				$('#easyuiPopupSearchEquipment').hide();
				$('#equipmentCode').val('').change();
				$('#seriNumber').val('').change();
				$('#typeEquipment').html('<option value="" selected="selected">Tất cả</option>');
				// $('#groupEquipment').html('<option value="" selected="selected">Tất cả</option>').change();
				// $('#providerId').html('<option value="" selected="selected">Tất cả</option>');
				$('#yearManufacturing').val('').change();
				$('#equipmentGridDialog').datagrid('loadData',[]);
				EquipStatisticChecking._lstEquipId = new Map();
			}
		});
	},
	/**
	 * Xu ly trên input loai thiet bi
	 * @author nhutnn
	 * @since 19/12/2014
	 */
	onChangeTypeEquip: function(cb){
		var typeEquip = $(cb).val().trim();
		EquipStatisticChecking.getEquipGroup(typeEquip);
	},
	/**
	 * Lay nhom thiet bi
	 * @author nhutnn
	 * @since 12/01/2015
	 */
	getEquipGroup: function(typeEquip){
		var params = new Object();
		if(typeEquip != undefined && typeEquip != null && typeEquip != ''){
			params.id = typeEquip;
		}
		// $('#groupEquipment').html('<option value="" selected="selected">Tất cả</option>');
    	Utils.getJSONDataByAjaxNotOverlay(params, '/equipment-manage-liquidation/get-equipment-group', function(result){	
		// $.getJSON('/equipment-manage-liquidation/get-equipment-catalog', function(result) {				
			// if(result.equipGroups != undefined && result.equipGroups != null && result.equipGroups.length > 0) {					
			// 	for(var i=0; i < result.equipGroups.length; i++){
			// 		$('#groupEquipment').append('<option value="'+ result.equipGroups[i].id +'">'+ Utils.XSSEncode(result.equipGroups[i].code+' - '+result.equipGroups[i].name) +'</option>');  
			// 	}
			// } 
			// $('#groupEquipment').change();			
			//tamvnm: doi thanh autoCompleteCombobox
			if(result.equipGroups != undefined && result.equipGroups != null && result.equipGroups.length > 0) {	
				for (var i = 0, isize = result.equipGroups.length; i < isize; i++) {
					result.equipGroups[i].searchText = unicodeToEnglish(result.equipGroups[i].code + " " + result.equipGroups[i].name);
	 				result.equipGroups[i].searchText = result.equipGroups[i].searchText.toUpperCase();
				}				
				var obj = {
				id: -1,
				code: "Tất cả",
				name: "Tất cả",
				codeName: "Tất cả",
				searchText: unicodeToEnglish("Tất cả").toUpperCase()
				};
				result.equipGroups.splice(0, 0, obj);
				$('#groupEquipment').combobox("loadData", result.equipGroups);
		 		$('#groupEquipment').combobox("setValue", -1);
			} 				
		},null,null);
	},
	/**
	 * Lay loai, ncc thiet bi
	 * @author phuongvm
	 * @since 27/05/2015
	 */
	getEquipCatalog: function(){
		$('#typeEquipment').html('<option value="" selected="selected">Tất cả</option>');
    	$('#provider').html('<option value=""  selected="selected">Tất cả</option>');
    	
		$.getJSON('/equipment-manage-delivery/get-equipment-catalog', function(result) {				
			if(result.equipsCategory != undefined && result.equipsCategory != null && result.equipsCategory.length > 0) {					
				for(var i=0; i < result.equipsCategory.length; i++){
					$('#typeEquipment').append('<option value="'+ result.equipsCategory[i].id +'">'+Utils.XSSEncode(result.equipsCategory[i].code)+' - '+Utils.XSSEncode(result.equipsCategory[i].name) +'</option>');  
				}
			} 
			$('#typeEquipment').change();
			//tamvnm: doi thanh autoCompleteCombobox
			if(result.equipProviders != undefined && result.equipProviders != null && result.equipProviders.length > 0) {					
				for (var i = 0, isize = result.equipProviders.length; i < isize; i++) {
					result.equipProviders[i].searchText = unicodeToEnglish(result.equipProviders[i].code + " " + result.equipProviders[i].name);
 					result.equipProviders[i].searchText = result.equipProviders[i].searchText.toUpperCase();
				}
				var obj = {
					id: -1,
					code: "Tất cả",
					name: "Tất cả",
					codeName: "Tất cả",
					searchText: unicodeToEnglish("Tất cả").toUpperCase()
				};
				result.equipProviders.splice(0, 0, obj);
				$('#providerId').combobox("loadData", result.equipProviders);
		 		$('#providerId').combobox("setValue", -1);
			}						
		});
	},
	/**
	 * Tim kiem thiet bi
	 * @author nhutnn
	 * @since 12/12/2014
	 * 
	 * @author hunglm16
	 * @sine JUNE 13,2015
	 * @description Ra soat noi dung phan quyen CMS
	 */
	searchEquipment: function(){
		$(".ErrorMsgStyle").hide();
		EquipStatisticChecking._lstEquipId = new Map();

		//tamvnm: thay doi thanh autoCompleteCombobox: -1 la truong hop tim group ko co trong BD
		var groupId = "";
		if ($('#groupEquipment').combobox('getValue') == "") {
			$('#groupEquipment').combobox('setValue', -1);
			groupId = "";
		} else if ($('#groupEquipment').combobox('getValue') == -1) {
			groupId = "";
		} else {
			if (Utils.getMessageOfInvaildNumberNew($('#groupEquipment').combobox("getValue")).length == 0) {
				groupId = $('#groupEquipment').combobox("getValue");
			} else {
				groupId = -1;
			}
		}
		var providerId = "";
		if ($('#providerId').combobox('getValue') == "") {
			$('#providerId').combobox('setValue', -1);
			providerId = "";
		} else if ($('#providerId').combobox('getValue') == -1) {
			providerId = "";
		} else {
			if (Utils.getMessageOfInvaildNumberNew($('#providerId').combobox("getValue")).length == 0) {
				providerId = $('#providerId').combobox("getValue");
			} else {
				providerId = -1;
			}
		}

		var params = {
				id: $("#id").val(),
				equipCode: $('#equipmentCode').val().trim(),
				seriNumber: $('#seriNumber').val().trim(),
				categoryId: $('#typeEquipment').val().trim(),
				// groupId: $('#groupEquipment').val().trim(),
				// providerId: $('#providerId').val().trim(),
				groupId: groupId,
				providerId: providerId,
				yearManufacture: $('#yearManufacturing').val().trim(),
//				stockCode: $('#stockCode').val().trim(),
				customerCode: $('#customerCode').val().trim(),
//				customerName: $('#customerName').val().trim(),
//				shopCode: $('#shopCode').val().trim(),
//				customerAddress: $('#customerAddress').val().trim(),
//				status: activeType.RUNNING,
//				tradeStatus: 0,
//				stockType: 1, //Don vi
//				usageStatus: usageStatus.SHOWING_WAREHOUSE,//Dang o kho
				flagPagination: true
		};
		$('#equipmentGridDialog').datagrid('load', params);
	},
	addEquipment: function() {
		$(".ErrorMsgStyle").hide();
		if(EquipStatisticChecking._lstEquipId.keyArray.length == 0) {
			$('#errMsgEquipmentDlg').html('Bạn chưa chọn thiết bị nào. Vui lòng check chọn thiết bị').show();
			return;
		}
		$.messager.confirm("Xác nhận", "Bạn có muốn thêm thiết bị?", function(r) {
			if (r) {
				var params = {
						id: $("#id").val(),
						listId : EquipStatisticChecking._lstEquipId.keyArray
				};
				VTUtilJS.postFormJson(params, '/equipment-checking/edit-equip/save', function(data) {
					if(data.error) {
						$('#errMsgEquipmentDlg').html(data.errMsg).show();
					} else {
						$('#easyuiPopupSearchEquipment').dialog('close');
						$('#successMsg').html('Lưu dữ liệu thành công').show();
						setTimeout(function() {
							$('#successMsg').html('').hide();
						}, 3000);
						$('#gridEquipStatistic').datagrid('reload');
					}
				});
			}
		});
	},
	/*
	 * Xu ly khi nhan radio dat, khong dat
	 * @author phuongvm
	 * @since 16/09/2015
	 */
	changeCheckDisplay: function(t) {
		$('.fancybox-inner #chkInspect').attr("checked", "checked");
		$('.fancybox-inner #chkInspect').attr("disabled", "disabled");
	},
	/* update Luu them mot so thong tin
	 * @author phuongvm
	 * @since update 16/09/2015
	 */
	updateResult: function() {
		$(".ErrorMsgStyle").hide();
		var id = $('.fancybox-inner #divDetail').attr('value');	
		var resultImg = null;
		if (id != undefined && id != null && id != "")  {			
			var data = new Object();
			if ($('.fancybox-inner #chkResult').is(':checked')) {
				resultImg = 1;
			} else if ($('.fancybox-inner #chkNotResult').is(':checked')) {
				resultImg = 0;
			} else {
				resultImg =  null;
			}
			data.resultImg = resultImg; 
			var inspect = null;
			if ($('.fancybox-inner #chkInspect').is(':checked')) {
				if (resultImg == null) {
					$(".fancybox-inner #errMsgPopup").html("Vui lòng nhập kết quả kiểm tra trưng bày!").show();
					$(".fancybox-inner #errMsgPopup").focus();
					return false;
				} else {
					inspect = 1;
				}
			}
			data.inspect = inspect;
			if ($('.fancybox-inner #txtNumberFace').val() != undefined && $('.fancybox-inner #txtNumberFace').val() != null) {
				if($('.fancybox-inner #txtNumberFace').val() != "" && $('.fancybox-inner #txtNumberFace').val() <= 0) {
					$(".fancybox-inner #errMsgPopup").html("Số mặt phải là số nguyên dương!").show();
					$(".fancybox-inner #errMsgPopup").focus();
					return false;
				}
				data.numberProduct = $('.fancybox-inner #txtNumberFace').val().trim();
			}
			if($('.fancybox-inner #txtPOSM').val() != undefined && $('.fancybox-inner #txtPOSM').val() != null) {
				data.strPOSM = $('.fancybox-inner #txtPOSM').val().trim();
			}
			if($('.fancybox-inner #txtNoted').val() != undefined && $('.fancybox-inner #txtNoted').val() != null) {
				data.strNoted = $('.fancybox-inner #txtNoted').val().trim();
			}
			var params = new Object();
			params.id = id;
			if (data.resultImg != null) {
				params.resultImg = data.resultImg;
			}
			params.numberProduct = data.numberProduct;
			params.strPOSM = data.strPOSM;
			params.strNoted = data.strNoted;
			params.isInspeck = data.inspect;
			Utils.addOrSaveData(params, '/equipment-checking/updateResult', null, 'errMsgPopup', function(data) {
				if (data.error) {
					$(".fancybox-inner #errMsgPopup").html(data.errMsg).show();
				} else {
					var temp = EquipStatisticChecking.lstImages.get(id);
					if (params.resultImg != -1) {
						temp.result = params.resultImg;
					}
					if (params.isInspeck != null) {
						temp.isInSpected = params.isInspeck;
					}
					if (params.numberProduct != null) {
						temp.numberProduct = params.numberProduct;
					} 
					if (params.strPOSM != null) {
						temp.POSM = params.strPOSM;
					} 
					if (params.strNoted != null) {
						temp.displayNote = params.strNoted;
					}  
					$(".fancybox-inner #successMsgPopup").html("Lưu dữ liệu thành công").show();					
					setTimeout(function() {
						$(".fancybox-inner #successMsgPopup").html("").hide();
						EquipStatisticChecking.showBigImagePopup(id);
					}, 3000);
				}
				
			}, null, ".fancybox-inner ", null, null, null , null);
		}
	},
	searchGridEquip: function(){
		var params=new Object(); 
		params.equipCode = $('#equipCode').val().trim();	
		params.equipGroupName = $('#equipGroupName').val().trim();
		params.id = $('#id').val();
		params.statusCus = $('#statusCustomer').val().trim();
		$('#gridEquipStatistic').datagrid('load',params);
	},
	
	/**
	 * hien popup so lan kiem ke
	 * @author trietptm
	 * @since Mar 10, 2016
	 */
	showDlgNumStatistic: function(equipId) {
		var params = EquipStatisticChecking._paramListStatistic;
		params.equipId = equipId;
		$('#easyuiPopupNumStatistic').dialog({
	        closed: false,  
	        cache: false,  
	        modal: true,
	        width: 'auto',
	        height: 'auto',
	        onOpen: function () {
				$('#gridNumStatistic').datagrid({
					url: '/equipment-checking/getListStatisticTime',
					autoRowHeight : true,
					queryParams: params,
					rownumbers : true, 
					singleSelect: true,
					pagination: true,
					pageSize: 10,
					pageList: [10, 20, 50],
					fitColumns: true,
					scrollbarSize: 0,
					width: $('#gridNumStatisticDiv').width(),
					autoWidth: true,
					columns: [[
					    {field: 'statisticTime',title: 'Lần kiểm kê', width: 80, sortable: false, resizable: false, align: 'center'},
						{field: 'statisticDate',title: 'Ngày', width: 80, sortable: false, resizable: false, align: 'center', formatter: function(value, row, index){
							return Utils.XSSEncode(value);
						}},
						{field: 'status', title: 'Trạng thái', width: 80, sortable: false, resizable: false, align: 'left', formatter: function(value, row, index) {
							return InventoryStatus.parseValue(row.status);
						}}
					]],
					onBeforeLoad: function(param) {
					},
					onLoadSuccess: function(data) {
				    	$('.easyui-dialog .datagrid-header-rownumber').html('STT');
				    	setTimeout(function(){
			    			CommonSearchEasyUI.fitEasyDialog('easyuiPopupNumStatistic');
			    		},1000);
					}
				});
	        }
		});
	},
};
/*
 * END OF FILE - /web/web/resources/scripts/business/equipment/vnm.equiment-statistic-checking.js
 */

/*
 * START OF FILE - /web/web/resources/scripts/business/equipment/vnm.equipment_group_product.js
 */
/**
 * Copyright 2015 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *
 * Doi tuong thao tac voi giao dien cua chuc nang: Danh sach san pham cho nhom thiet bi
 * @author tuannd20
 * @version 1.0
 * @date 05/01/2015 
 */
var EquipmentGroupProduct = {
	_mapNode: new Map(),
	_mapCheck: new Map(),
	_mainGridOption: null,
	_mapDataDdv: new Map(),
	_selectingEquipmentGroupCode: null,
	_selectingEquipmentGroupIndex: null,
	_idExpendRow: 0,
	_rowGroupEquipIndex: 0,
	TREE_TYPE_EQUIPMENT_GROUP_PRODUCT: 5,
	/**
	 * khoi tao control tren trang
	 * @return {void}
	 */
	initializePageControl: function(option) {
		if (option) {
			EquipmentGroupProduct._mainGridOption = option;
			if (option.gridId) {
				$('#' + option.gridId).datagrid({
					view: detailview,
					url: option.url,
					pagination: true,
					pageList: [50],
					fitColumns: true,
					width: $(window).width() - 55,
					scrollbarSize: 0,
					rownumbers: true,			
					autoRowHeight: true,
					nowrap: false,
					singleSelect: true,
					columns: [[  
						{field: 'code', title: 'Mã nhóm thiết bị', width: 200, align:'left', sortable : false, resizable : false, 
							formatter: function(value, row, index) {
								return Utils.XSSEncode(value);
							}
						},
						{field: 'name', title: 'Tên nhóm thiết bị', width: 500, align: 'left', sortable : false, resizable : false,
							formatter: function(value, row, index){
								return '<div style="word-wrap: break-word">' + Utils.XSSEncode(value) +'</div>';
							}
						},
						{field: 'status', title: 'Trạng thái', width: 100, align: 'center', sortable : false, resizable : false,
							formatter: function(value, row, index){
								return '<div style="word-wrap: break-word">' + Utils.XSSEncode(statusTypeText.parseValue(value)) +'</div>';
							}
						},
					]],
					detailFormatter:function(index, row){  
						return '<div style="padding:2px; width:850"><table id="ddv-' + index + '" class="ddv"></table></div>';
					},
					onBeforeLoad: function(){
					},
					onLoadSuccess: function(){
						$('#' + option.gridId).datagrid('resize');
						$('.datagrid-header-rownumber').html('STT');
					},
					onExpandRow: function(index, row){
						var gridDetailId = '';
						EquipmentGroupProduct.initializeEquipmentGroupSubGrid(option.gridId, index, row);
						$('#' + option.gridId).datagrid('fixDetailRowHeight',index);
						$('#' + option.gridId).datagrid('resize');
					}
				});
			}
		}
	},

	/**
	 * tai file excel template
	 * @author tamvnm
	 * @since Sep 15 2015
	 */
	 downloadTemplate:function () {
		$('.downloadTemplateReport').attr('href', excel_template_path + '/equipment/Bieu_mau_import_danh_sach_san_pham_thiet_bi.xlsx');
	 },

	/**
	 * show popup import excel
	 * @author tamvnm
	 * @since Sep 15 2015
	 */
	showDlgImportExcel: function() {
		$('#easyuiPopupImportExcel').show();
		$('.ErrorMsgStyle').hide();
		$('#easyuiPopupImportExcel').dialog({  
	        closed: false,  
	        cache: false,  
	        modal: true,
			height: 'auto',
	        onOpen: function() {	 
	        	$('.easyui-dialog .BtnGeneralStyle').css('color','');
	        },
	        onClose: function() {
				$("#fakefilepcBBGN").val("");
				$("#excelFileBBGN").val("");				
	        }
		});
	},
	/**
	 * import excel san pham thiet bi
	 * @author tamvnm
	 * @since sep 15 2015
	 */
	importExcelGroupProduct: function() {
		$('.ErrorMsgStyle').hide();
		VTUtilJS.importExcelUtils(function(data) {
			$("#errExcelMsgBBGN").html(data.message).show();
			if (data.fileNameFail == null || data.fileNameFail == "" ) {					
				setTimeout(function() {
					$('#errExcelMsgBBGN').html("").hide();
					$('#easyuiPopupImportExcel').dialog('close');
				}, 3000);				
			} else {
				$("#fakefilepcBBGN").val("");
				$("#excelFileBBGN").val("");			
			}
		}, "importFrmBBGN", "excelFileBBGN", null, "errExcelMsgBBGN");
		return false;
	},

	exportExcelGroupProduct: function() {

		$('#errMsg').html('').hide();
		var msg ='';
		var EquipGroupCode = $('#equipmentGroupCode').val();
		var EquipGroupName = $('#equipmentGroupName').val();
		var status = $('#equipmentGroupStatus').val();

		if (EquipGroupCode != undefined && EquipGroupCode.length > 0) {
			EquipGroupCode = EquipGroupCode.trim();
		}
		if (EquipGroupName != undefined && EquipGroupName.length > 0) {
			EquipGroupName = EquipGroupName.trim();
		}

		
		var params=new Object(); 
		params.equipmentGroupCode = EquipGroupCode;
		params.equipmentGroupName = EquipGroupName;
		params.equipmentGroupStatus = status;
		$.messager.confirm('Xác nhận','Bạn có muốn xuất file excel?',function(r) {
			if (r) {
				ReportUtils.exportReport('/equipment-group-product/export-excel', params, 'errMsg');					
			}
		});		
		return false;
	},

	initializeEquipmentGroupSubGrid: function(mainGridId, expandingIndex, expandingRow) {
		EquipmentGroupProduct._selectingEquipmentGroupCode = expandingRow && expandingRow.code ? expandingRow.code : '';
		var editColumnTitle = '<a href="javascript:void(0);" \
							onclick=\"return EquipmentGroupProduct.openSearchProductDialog(' + (expandingRow && expandingRow.id ? expandingRow.id : 0) + ', ' + expandingIndex + ');\"> \
							<img width="15" height="16" src="/resources/images/icon_add.png"></a>';

		var ddv = $('#' + mainGridId).datagrid('getRowDetail', expandingIndex).find('table.ddv');
		
		var url = '/equipment-group-product/retrieve-products-in-equipment-group?equipmentGroupCode=' + expandingRow.code;
		var paramDetail = {
			equipmentGroupCode: expandingRow.code
		};
		ddv.datagrid({
			url: url,
			fitColumns: true,
			singleSelect: true,
			pagination: true,
			pageList: [10, 20, 30, 50],
			rownumbers: true,
			scrollbarSize: 0,
			loadMsg: '',
			height: 'auto',
			width: $(window).width() - 200,
			columns:[[
				{field: 'productCode', title: 'Mã sản phẩm', width: 110, sortable : false, resizable : false, 
					formatter: function(value, row, index) {
						return Utils.XSSEncode(value);
					}
				},
				{field: 'productName', title: 'Tên sản phẩm', width: 180, sortable : false, resizable : false, 
					formatter: function(value, row, index) {
						return Utils.XSSEncode(value);
					}
				},
				{field: 'fromDate', title: 'Ngày bắt đầu', width: 110, sortable : false, resizable : false, 
					formatter: function(value, row, index) {
						return Utils.XSSEncode(row.fromDateStr);						
					}
				},
				{field: 'toDate', title: 'Ngày kết thúc', width: 110, sortable : false, resizable : false, 
					formatter: function(value, row, index) {
						return Utils.XSSEncode(row.toDateStr);
					}
				},
				{field: 'edit', title: editColumnTitle, width: 30, align: 'center', sortable : false, resizable : false,
					formatter: function(value, row, index) {
						if (row.flagSys == 1) {
							return "<a href='javascript:void(0)' onclick=\"return EquipmentGroupProduct.deleteProductInEquipmentGroup('" + ddv.attr('id') + "', '" + row.productCode + "', '" + expandingRow.code + "', "+row.equipGroupProductId+");\"><img width='15' height='16' src='/resources/images/icon_delete.png'/></a>";							
						}
						return '';
					}
				}
			]],
			onResize:function(){
				$('#' + mainGridId).datagrid('fixDetailRowHeight',expandingIndex);
			},
			onLoadSuccess:function(){
				setTimeout(function(){
					$('#' + mainGridId).datagrid('fixDetailRowHeight', expandingIndex);
					$('#' + mainGridId).datagrid('resize');
				},0);
			}
		});
		$('#' + mainGridId).datagrid('fixDetailRowHeight', expandingIndex);
	},
	/**
	 * mo dialog tim kiem san pham
	 * @return {void}
	 * 
	 * @author hunglm16
	 * @description ra soat va update
	 */
	openSearchProductDialog: function(rowDataId, rowIndex) {
		$('.ErrorMsgStyle').html('').hide();
		$('.SuccessMsgStyle').html('').hide();
		EquipmentGroupProduct._idExpendRow = 0;
		EquipmentGroupProduct._rowGroupEquipIndex = 0;
		if (rowDataId) {
			EquipmentGroupProduct._idExpendRow = rowDataId;
		}
		if (rowIndex) {
			EquipmentGroupProduct._rowGroupEquipIndex = rowIndex;
		}
		EquipmentGroupProduct._selectingEquipmentGroupIndex = rowIndex;
		EquipmentGroupProduct._selectingEquipmentGroupCode = $('#' + EquipmentGroupProduct._mainGridOption.gridId).datagrid('getRows')[rowIndex].code;
		$("#productCodeDl").val('');
		$("#productNameDl").val('');
		$(".easyui-dialog #errMsgProductDlg").html('').hide();
		//EquipmentGroupProduct._mapNode = new Map();
		EquipmentGroupProduct._mapCheck = new Map();
		$("#popupProductSearch").dialog({
			onOpen:function() {
				selectedDropdowlist('productInforDl', activeType.ALL);
				$('#productStatusDl option[value="'+activeType.RUNNING+'"]').attr('selected','selected').change();
				$('#productCodeDl, #productNameDl').width($('#productInforDl').width() - 5);
				$('#productCodeDl').focus();
				$('#gridProductInsert').datagrid({
					url: '/equipment-group-product/search-product-by-insert',
					width : 860,
					queryParams: {
						productCode: $('#productCodeDl').val().trim(),
						productName: $('#productNameDl').val().trim(),
						productInforId: $('#productInforDl').val(),
						status: $('#productStatusDl').val()
					},
					pagination: true,
					scrollbarSize: 0,
					fitColumns : true,
					checkOnSelect: false,
					selectOnCheck: false,
				    rownumbers : true,
				    height: 275,
				    pageSize:10,
				    pageList: [10],
					columns:[[
						{field: 'productId', checkbox:true},
						{field: 'productInfoCode', title: 'Ngành hàng', width: 80, align: 'left', formatter: function(value, row, index){
							return Utils.XSSEncode(value);
						}}, 
						{field: 'productCode', title: 'Mã sản phẩm', width:90, align: 'left', formatter: function(value, row, index){
							return Utils.XSSEncode(value);
						}},  
					    {field: 'productName', title: 'Tên sản phẩm', width:180, align:'left', formatter: function(value, row, index){
					    	return Utils.XSSEncode(value);
					    }},
					    {field: 'fromDate', title:'Ngày bắt đầu', width:110,align:'center', formatter: function(value, row, index){
					    	return '<input type="text" class="InputTextStyle InputText1Style vinput-date" value="'+row.dateSysStr.trim()+'" onchange="EquipmentGroupProduct.onchangeDateInputDl(this);" name ="fDateGridDl" id="fDateGridDl_'+row.productCode+'" style="width: 90px; margin-top: 5px;">';
					    }},
					    {field: 'toDate', title:'Ngày kết thúc', width:110,align:'center', formatter: function(value, row, index){
					    	return '<input type="text" class="InputTextStyle InputText1Style vinput-date" onchange="EquipmentGroupProduct.onchangeDateInputDl(this);" name ="fDateGridDl" id="tDateGridDl_'+row.productCode+'" style="width: 110px; margin-top: 5px;">';
					    }},
					    {field: 'productStatus', title:'Trạng thái', width:80, align:'left', formatter: function(value, row, index){
					    	return statusTypeText.parseValue(value);
					    }},
					    {field: 'P', hidden: true}
					]],
					onCheck: function(rowIndex, rowData) {
						var productCode = rowData.productCode;
						EquipmentGroupProduct._mapCheck.put(rowData.productCode, {
							productCode: rowData.productCode,
							fDate: $('#fDateGridDl_' + productCode).val().trim(),
							tDate: $('#tDateGridDl_' + productCode).val().trim()
						});
						$('#gridProductInsert').datagrid('selectRow', rowIndex);
					},
					onUncheck: function(rowIndex, rowData) {
						var productCode = rowData.productCode;
						if (EquipmentGroupProduct._mapCheck.keyArray.indexOf(productCode) > -1) {
							EquipmentGroupProduct._mapCheck.remove(productCode);
						}
						$('#gridProductInsert').datagrid('unselectRow', rowIndex);
					},
					onCheckAll: function(rows) {
						$('#gridProductInsert').datagrid('selectAll');
						var dataGrid = $('#gridProductInsert').datagrid('getData').rows;
						for(var i=0, size = dataGrid.length; i < size; i++){
							var productCode = dataGrid[i].productCode;
							EquipmentGroupProduct._mapCheck.put(productCode, {
								productCode: dataGrid[i].productCode,
								fDate: $('#fDateGridDl_' + productCode).val().trim(),
								tDate: $('#tDateGridDl_' + productCode).val().trim()
							});						
						}
					},
					onUncheckAll: function(rows) {
						EquipmentGroupProduct._mapCheck = new Map();
						$('#gridProductInsert').datagrid('unselectAll');
					},
					onClickRow: function(rowIndex, rowData) {
						var rowCbx = $('[name=productId]')[rowIndex];
						if (rowCbx != undefined && rowCbx != null && !rowCbx.checked) {
							$('#gridProductInsert').datagrid('unselectRow', rowIndex);
						}else{
							$('#gridProductInsert').datagrid('selectRow', rowIndex);
						}
						return true;
					},
			        onLoadSuccess: function(data){
						$('#permissionContainerGrid .datagrid-header-rownumber').html('STT');
						updateRownumWidthForDataGrid('gridProductInsert');
						$('.vinput-date').each(function() {
							$(this).width(90);
							VTUtilJS.applyDateTimePicker($(this));
						});
					 	$('#gridProductInsert').datagrid('uncheckAll').datagrid('unselectAll');
					 	$(window).resize();
			        }
				});
			}
		});
		$("#popupProductSearch").dialog('open');
	},
	
	/**
	 * Xu ly su kien on change dialog insert product
	 * 
	 * @author hunglm16
	 * @since May 22, 2015
	 * 
	 * @param {input} -> txt
	 * */
	onchangeDateInputDl: function (txt) {
		var productCode = $(txt).prop('id').replace(/fDateGridDl_/g,'').replace(/tDateGridDl_/g,'');
		if (EquipmentGroupProduct._mapCheck.keyArray.indexOf(productCode) > -1) {
			var rowItem = EquipmentGroupProduct._mapCheck.get(productCode);
			EquipmentGroupProduct._mapCheck.put(productCode, {
				productCode: rowItem.productCode,
				fDate: $('#fDateGridDl_' + productCode).val().trim(),
				tDate: $('#tDateGridDl_' + productCode).val().trim()
			});
		}
	},
	
	/**
	 * tim kiem san pham tren cay
	 * @author  tuannd20
	 * @return cay san pham theo tieu chi tim kiem
	 * 
	 * @author hunglm16
	 * @since May 21,2015
	 * @description Ra soat va update
	 * @return data Grid
	 */
	searchProductOnTree: function() {
		$('.ErrorMsgStyle').html('').hide();
		$('.SuccessMsgStyle').html('').hide();
		$(".easyui-dialog #errMsgProductDlg").html('').hide();
		var paramsSearch = {
				productCode: $('#productCodeDl').val().trim(),
				productName: $('#productNameDl').val().trim(),
				productInforId: $('#productInforDl').val(),
				status: $('#productStatusDl').val()
		};
		$('#gridProductInsert').datagrid('load', paramsSearch);
	},
	/**
	 * ham xu ly su kien bam button tim kiem nhom thiet bi
	 * @author tuannd20
	 * @return {void} 
	 * @date 05/01/2015
	 */
	searchEquipmentGroupHandler: function() {
		var equipmentGroupFilter = {
			equipmentGroupCode: $('#equipmentGroupCode').val().trim(),
			equipmentGroupName: $('#equipmentGroupName').val().trim(),
			status: $('#equipmentGroupStatus').val()
		};
		if (equipmentGroupFilter.equipmentGroupStatus == 'ALL') {
			delete equipmentGroupFilter.equipmentGroupStatus;
		}
		var params = {};
		convertToSimpleObject(params, equipmentGroupFilter, "equipmentGroupFilter");
		$('#' + EquipmentGroupProduct._mainGridOption.gridId).datagrid('load', params);
	},
	/**
	 * them san pham vao equipment_group
	 * @author tuannd20
	 * @return {void}
	 */
	addProductsToEquipmentGroup: function() {
		$('.ErrorMsgStyle').html('').hide();
		$('.SuccessMsgStyle').html('').hide();
		if (EquipmentGroupProduct._mapCheck == null || EquipmentGroupProduct._mapCheck.size() == 0) {
			$('#errMsgPopup3').html('Bạn chưa chọn sản phẩm nào.').show();
			return false;
		}
		var msg = '';
		var mapProductFDateStr = 'hunglm@16';
		var mapProductToDateStr = 'hunglm@16';
		var arr = EquipmentGroupProduct._mapCheck.keyArray;
		//Sap xep lai mang tang dang theo Ma San Pham
		var rowKeys = arr.sort();
		//Xu ly cho rows map dau tien
		var productItem = null;
		for (var i = 0, sizei = rowKeys.length; i < sizei; i++) {
			productItem = EquipmentGroupProduct._mapCheck.get(rowKeys[i]);
			msg = EquipmentGroupProduct.validateProductDateSingleRowDl(productItem.productCode, productItem.fDate, productItem.tDate);
			if (msg.length > 0) {
				$('#errMsgPopup3').html(msg).show();
				return false;
			}
			mapProductFDateStr = mapProductFDateStr + ',' + productItem.productCode.trim() + '_' + productItem.fDate.trim();
			if (productItem.tDate.trim().length > 0) {
				mapProductToDateStr = mapProductToDateStr + ',' + productItem.productCode.trim() + '_' + productItem.tDate.trim();
			} else {
				mapProductToDateStr = mapProductToDateStr + ',' + productItem.productCode.trim() + '_ddMMyyyy';
			}
		}
		mapProductFDateStr = mapProductFDateStr.replace(/hunglm@16,/g, '').trim();
		mapProductToDateStr = mapProductToDateStr.replace(/hunglm@16,/g, '').trim();
		if (mapProductFDateStr.length == 0) {
			$('#errMsgPopup3').html('Không xác định được Sản phẩm với Ngày bắt đầu').show();
			return false;
		}
		if (mapProductToDateStr.length == 0) {
			$('#errMsgPopup3').html('Không xác định được Sản phẩm với Ngày kết thúc').show();
			return false;
		}
		var dataModel = {};
		dataModel.mapProductFDate = mapProductFDateStr;
		dataModel.mapProductTDate = mapProductToDateStr; 
		dataModel.equipGroupId = EquipmentGroupProduct._idExpendRow;
		EquipmentGroupProduct.addProductsToEquipmentGroupEvent(dataModel, '/equipment-group-product/add-products');
		
		return false;
	},
	
	/**
	 * Cap nhat du lieu mang tinh chat lap de qui
	 * 
	 * @author hunglm16
	 * @sing May 22,2015
	 * 
	 * @description Danh cho su kien them san pham theo ham addProductsToEquipmentGroup
	 * */
	addProductsToEquipmentGroupEvent: function(dataModel, url, message) {
		$('.ErrorMsgStyle').html('').hide();
		$('.SuccessMsgStyle').html('').hide();
		var msg = 'Bạn có muốn cập nhật?';
		if (message != undefined && message != null && message.trim().length > 0) {
			msg = message.trim();
		}
		$.messager.confirm('Xác nhận', msg, function(r) {
			if (r) {
				Utils.saveData(dataModel, url, null, 'errMsgPopup3', function (data) {
					var ddv = $('#' + EquipmentGroupProduct._mainGridOption.gridId).datagrid('getRowDetail', EquipmentGroupProduct._rowGroupEquipIndex).find('table.ddv');
					$(ddv).datagrid("reload");
					var countMsg = '';
					if (data.countSuccess) {
						countMsg = data.countSuccess.toString() + ' dòng';
					}
					//Xu ly cho thanh cong
					var tm = setTimeout(function() {
						$('#successMsg').html('Lưu dữ liệu thành công ' + countMsg).show();
						$("#popupProductSearch").dialog("close");
					}, 2000);
					return false;
				}, null, null, null, function (data) {
					var ddv = $('#' + EquipmentGroupProduct._mainGridOption.gridId).datagrid('getRowDetail', EquipmentGroupProduct._rowGroupEquipIndex).find('table.ddv');
					$(ddv).datagrid("reload");
					//Xu ly ho that bai
					var countMsg = '';
					if (data.countSuccess) {
						countMsg = data.countSuccess.toString() + ' dòng';
					}
					if (data.flag == 1) {
						if (data.excProductCodes) {
							dataModel.excProductCodes = data.excProductCodes;							
						}
						//Xu ly cho thanh cong
						var msgDltmp = data.errMsg;
						if (countMsg.length > 0) {
							msgDltmp = 'Lưu dữ liệu thành công' + countMsg + '. ' + msgDltmp;
						}
						EquipmentGroupProduct.addProductsToEquipmentGroupEvent(dataModel, url, msgDltmp);
					} else {
						if (countMsg.length > 0) {
							$('#successMsgPopup3').html('Lưu dữ liệu thành công' + countMsg).show();
							var tm = setTimeout(function(){
								$('.SuccessMsgStyle').html('').hide();
								clearTimeout(tm);
							}, 2000);						
						}
						$('#errMsgPopup3').html(data.errMsg).show();
						return false;
					}
				});
			} else {
				$("#popupProductSearch").dialog("close");
			}
		});
	},
	
	/**
	 * Validate tinh hop le cua fDate, tDate cua san pham
	 * 
	 * @author hunglm16
	 * @since May 23,2015
	 * 
	 * @param {string} productCode
	 * @param {string} fDate
	 * @param {string} tDate
	 * */
	validateProductDateSingleRowDl: function (productCode, fDate, tDate) {
		if (productCode == undefined || productCode == null) {
			return 'Không xác định được Sản phẩm';
		}
		if (fDate == undefined || fDate == null ) {
			return 'Ngày bắt đầu của Sản phẩm ' + productCode + ' không hợp lệ';
		}
		if (fDate.trim().length == 0) {
			return 'Ngày bắt đầu của Sản phẩm ' + productCode + ' không được để trống';
		}
		if (fDate.trim().length >0 && !Utils.isDate(fDate.trim(), '/')) {
			return 'Ngày bắt đầu của Sản phẩm ' + productCode + ' không đúng định dạng dd/MM/yyyy';
		}
		if (tDate == undefined || tDate == null ) {
			return 'Ngày kết thúc của Sản phẩm ' + productCode + ' không hợp lệ';
		}
		if (tDate.trim().length >0 && !Utils.isDate(tDate.trim(), '/')) {
			return 'Ngày kết thúc của Sản phẩm ' + productCode + ' không đúng định dạng dd/MM/yyyy';
		}
		if (fDate.trim().length > 0 && tDate.trim().length > 0 && !Utils.compareDate(fDate, tDate)) {
			return 'Ngày bắt đầu của Sản phẩm ' + productCode + ' phải nhỏ hơn Ngày kết thúc ';
		}
		return '';
	},
	
	/**
	 * Validate tinh chung chom ngay cua cac san pham
	 * 
	 * @author hunglm16
	 * @since May 23,2015
	 * 
	 * @param {string} productCodei
	 * @param {string} fDatei
	 * @param {string} tDatei
	 * 
	 * @param {string} productCodej
	 * @param {string} fDatej
	 * @param {string} tDatej
	 * 
	 * @description Chi su dung ham nay khi da validate qua ham EquipmentGroupProduct.validateProductDateSingleRowDl(...)
	 * */
	validateProductDateDoublrRowDl: function (productCodei, productCodej, fDatei, tDatei, fDatej, tDatej) {
		if (productCodei == undefined || productCodei == null || productCodej == undefined || productCodej == null) {
			return 'Không xác định được Sản phẩm so sánh';
		}
		if (fDatei == undefined || fDatei == null) {
			return 'Không xác định được Ngày bắt đầu của Sản phẩm ' + productCodei;
		}
		if (tDatei == undefined || tDatei == null) {
			return 'Không xác định được Ngày kêt thúc của Sản phẩm ' + productCodei;
		}
		if (fDatej == undefined || fDatej == null) {
			return 'Không xác định được Ngày bắt đầu của Sản phẩm ' + productCodej;
		}
		if (tDatej == undefined || tDatej == null) {
			return 'Không xác định được Ngày kêt thúc của Sản phẩm ' + productCodej;
		}
		/**
		 * Xet truong hop productCodei
		 * @param fDatei is null
		 * @param tDatei is null
		 * */
		if (fDatei.trim().length == 0 && tDatei.trim().length == 0) {
			return 'Sản phẩm ' + productCodei + ' và ' + productCodej + ' có khoảng đoạn Ngày giao nhau';
		}
		if (fDatej.trim().length == 0 && tDatej.trim().length == 0) {
			return 'Sản phẩm ' + productCodei + ' và ' + productCodej + ' có khoảng đoạn Ngày giao nhau';
		}
		
		if ((Utils.compareDateForTowDate(tDatei, fDatej) < 0) || (Utils.compareDateForTowDate(tDatej, fDatei) < 0)) {
			return '';
		}
		return 'Sản phẩm ' + productCodei + ' và ' + productCodej + ' có khoảng đoạn Ngày giao nhau';
	},
	
	/**
	 * xoa san pham trong nhom thiet bi
	 * @author tuannd20
	 * @param  {string} productCode		ma san pham can xoa
	 * @param  {string} equipmentGroupCode ma nhom thiet bi
	 * @return {void}
	 * @date 05/01/2015
	 * 
	 * @author hunglm16
	 * @description ra soat va update
	 */
	deleteProductInEquipmentGroup: function(gridId, productCode, equipmentGroupCode, equipGroupProductId) {
		var dataModel = {
			productCodes: [productCode],
			equipGroupProductIds: [equipGroupProductId],
			equipmentGroupCode: equipmentGroupCode
		};

		Utils.addOrSaveRowOnGrid(dataModel, "/equipment-group-product/remove-product", null, gridId, 'errMsg', function(data) {
			$("#successMsg").html("Lưu dữ liệu thành công").show();
		});
	},
	/**
	 * an cac thong bao thanh cong/that bai
	 * @author tuannd20
	 * @return void
	 */
	hideMessages: function() {
		setTimeout(function() {
			$("#successMsg, #errMsg").html('').hide();
		}, 3000);
	}
};
/*
 * END OF FILE - /web/web/resources/scripts/business/equipment/vnm.equipment_group_product.js
 */

/*
 * START OF FILE - /web/web/resources/scripts/business/equipment/vnm.equipment-stock-change.js
 */
var EquipmentStockChange = {
	_FormAdd: 'ADD',
	_FormSearch: 'SEARCH',		
	_lstEquipStockChangeDel: null,
	_lstEquipStockChangeInsert:null,
	_lstToStockCodeInsert:null,
	_editIndex:null,
	_lstEquipInRecord:null,
	_lstEquipDel:new Array(),
	_numberEquipInRecord:null,
	_lstEquipInsert:null,
	_lstRecordError: null,
	_lstRecordSuccess: null,
	_arrFileDelete: null,
	isHavingInvalidRowOnGrid: false,
	_countFile: 0,
	_arrFileDelete: null,
	_toStockDefault:null,
	editIndex:undefined,
	_params: null, /*** param dung de search va xuat bao cao theo dk search/
	deletedEquipmentCodes: new Array(),
	

	/**
	 * Event kho nguon - kho dich
	 * 
	 * @return the string
	 * @author hoanv25
	 * @since 24/12/2014
	 */
	eventStockFunction: function(){
		var msg='';
		if (msg.length == 0) {
			msg = Utils.getMessageOfSpecialCharactersValidate('fStock', 'Kho giao',Utils._CODE);
		}
		if (msg.length == 0) {
			msg = Utils.getMessageOfSpecialCharactersValidate('tStock', 'Kho nhận',Utils._CODE);
		}
		if(msg.length>0){
			$('#errMsg').html(msg).show();	
			setTimeout(function(){$('#errMsg').hide();},3000);
			return false;
		}
	},
	/**
	 * Xu ly trên input loai thiet bi
	 * @author phuongvm
	 * @since 12/05/2015
	 */
	onChangeTypeEquip: function(cb){
		var typeEquip = $(cb).val().trim();
		EquipmentStockChange.getEquipGroup(typeEquip);
	},
	/**
	 * Lay nhom thiet bi
	 * @author phuongvm
	 * @since 12/05/2015
	 */
	getEquipGroup: function(typeEquip){
		var params = new Object();
		if(typeEquip != undefined && typeEquip != null && typeEquip != ''){
			params.id = typeEquip;
		}
		$('#groupEquipment').html('<option value="" selected="selected">Tất cả</option>');
    	Utils.getJSONDataByAjaxNotOverlay(params, '/equipment-manage-delivery/get-equipment-group', function(result){	
		// $.getJSON('/equipment-manage-liquidation/get-equipment-catalog', function(result) {				
			// if(result.equipGroups != undefined && result.equipGroups != null && result.equipGroups.length > 0) {					
			// 	for(var i=0; i < result.equipGroups.length; i++){
			// 		$('#groupEquipment').append('<option value="'+ result.equipGroups[i].id +'">'+ Utils.XSSEncode(result.equipGroups[i].code+' - '+result.equipGroups[i].name) +'</option>');  
			// 	}
			// } 
			// $('#groupEquipment').change();	
			//tamvnm: thay doi thanh autoCompleteCombobox
			if(result.equipGroups != undefined && result.equipGroups != null && result.equipGroups.length > 0) {	
				for (var i = 0, isize = result.equipGroups.length; i < isize; i++) {
					result.equipGroups[i].searchText = unicodeToEnglish(result.equipGroups[i].code + " " + result.equipGroups[i].name);
	 				result.equipGroups[i].searchText = result.equipGroups[i].searchText.toUpperCase();
				}				
				var obj = {
				id: -1,
				code: "Tất cả",
				name: "Tất cả",
				codeName: "Tất cả",
				searchText: unicodeToEnglish("Tất cả").toUpperCase()
				};
				result.equipGroups.splice(0, 0, obj);
				$('#groupEquipment').combobox("loadData", result.equipGroups);
		 		$('#groupEquipment').combobox("setValue", -1);
			} 			
		},null,null);
	},
	/**
	 * Xoa tap tin dinh kem
	 * 
	 * @author hunglm16
	 * @since Feb 18,214
	 * */
	removeEquipAttachFile: function(id){
			if (id == undefined || id == null || isNaN(id)) {
				return;
			}
			if (EquipmentStockChange._arrFileDelete == null || EquipmentStockChange._arrFileDelete == undefined) {
				EquipmentStockChange._arrFileDelete = [];
			}
			EquipmentStockChange._arrFileDelete.push(id);
			$('#divEquipAttachFile'+id).remove();
			if (EquipmentStockChange._countFile != undefined && EquipmentStockChange._countFile != null && EquipmentStockChange._countFile > 0) {
				EquipmentStockChange._countFile = EquipmentStockChange._countFile - 1;
			} else {
				EquipmentStockChange._countFile = 0;
			}
		},
		
	/**
	 * Tim kiem danh sach bien ban chuyen kho
	 * 
	 * @return the string
	 * @author hoanv25
	 * @since 24/12/2014
	 */
	searchStockChange : function() {
		var msg='';
		var code =$('#codeStockTran').val().trim();
		var status =$('#status').val().trim();
		var statusPerform =$('#statusPerform').val().trim();
		var fDate = $('#fDate').val();
		var tDate =$('#tDate').val();
		var cFDate = $('#cFDate').val();
		var cTDate =$('#cTDate').val();
		var tStock = $('#tStock').val().trim();
		var fStock = $('#fStock').val().trim();
		if (msg.length == 0) {
			msg = Utils.getMessageOfSpecialCharactersValidate('codeStockTran', 'Mã biên bản',Utils._CODE);
		}
		if(msg.length == 0 && $('#fDate').val().trim().length>0 && !Utils.isDate($('#fDate').val(),'/') && $('#fDate').val() != '__/__/____' ){
			msg = 'Từ ngày không tồn tại hoặc không đúng định dạng dd/mm/yyyy.';
			err = '#fDate';
		}
		if(msg.length == 0 && $('#tDate').val().trim().length>0 && !Utils.isDate($('#tDate').val(),'/') && $('#tDate').val() != '__/__/____'){
			msg = 'Đến ngày không tồn tại hoặc không đúng định dạng dd/mm/yyyy.';
			err = '#tDate';
		}
		if(msg.length == 0 && fDate.length > 0 && tDate.length > 0 && !Utils.compareDate(fDate, tDate)) {
			msg = 'Từ ngày tạo phải là ngày trước hoặc cùng ngày với đến ngày tạo';
		}

		if(msg.length == 0 && $('#cFDate').val().trim().length > 0 && !Utils.isDate($('#cFDate').val(), '/') && $('#cFDate').val() != '__/__/____' ) {
			msg = 'Từ ngày không tồn tại hoặc không đúng định dạng dd/mm/yyyy.';
			err = '#cFDate';
		}
		if(msg.length == 0 && $('#cTDate').val().trim().length > 0 && !Utils.isDate($('#cTDate').val(), '/') && $('#cTDate').val() != '__/__/____') {
			msg = 'Đến ngày không tồn tại hoặc không đúng định dạng dd/mm/yyyy.';
			err = '#cTDate';
		}
		if(msg.length == 0 && cFDate.length > 0 && tDate.length > 0 && !Utils.compareDate(cFDate, cTDate)) {
			msg = 'Từ ngày phải là ngày trước hoặc cùng ngày với đến ngày';
		}
		if(msg.length>0){
			$('#errMsg').html(msg).show();	
			setTimeout(function(){$('#errMsg').hide();},3000);
			return false;
		}
		var params =  new Object();
		params.code = code;
		params.status = status;
		params.statusPerform = statusPerform;
		params.fromDate = fDate;
		params.toDate = tDate;
		params.createFromDate = cFDate;
		params.createToDate = cTDate;
		params.toStock = tStock;
		params.fromStock = fStock;
		$('#gridStockChange').datagrid('load',params);	
		return false;
	},	
	
	/**
	 * Luu thay doi trang thai bien ban 
	 * @author hoanv25
	 * @since 24/12/2014
	 */
	saveStockChange: function(){
		var rows = $('#gridStockChange').datagrid('getRows');
		if(rows.length == 0){
			$('#errMsg').html('Không có dữ liệu để lưu').show();
			return false;
		}
		var lstRecordSuccess = new Array();
		//var lstIdRecord = new Array();
		var lstCheck = $('#gridStockChange').datagrid('getChecked');
		if(lstCheck.length > 0){
			var statusRecordChange = $('#statusRecordLabel').val().trim();	
			if( statusRecordChange == -2){
				$('#errMsg').html('Bạn chưa chọn trạng thái để chuyển biên bản!').show();
				return false;
			}
			for(var i=0; i < lstCheck.length; i++){				
				if(statusRecordChange != "" && statusRecordChange!= -2){
					lstRecordSuccess.push(lstCheck[i].id);
				}
			}	
			if(lstRecordSuccess.length <= 0){
				$('#errMsg').html('Bạn chưa chọn trạng thái để lưu!').show();
				return;
			}		
			var params = new Object();
			params.lstIdRecord = lstRecordSuccess;		
			params.statusRecord = statusRecordChange;			
			Utils.addOrSaveData(params, '/equipment-stock-change-manage/save-record-stock', null, 'errMsg', function(data) {
				if(data.error){
					$('#errMsg').html(data.errMsg).show();
					return;
				}
				if(data.lstRecordError != null && data.lstRecordError.length>0){					
					VCommonJS.showDialogList({		  
						title:'LỖI',
						data : data.lstRecordError,			   
						columns : [[
							{field:'formCode', title:'Mã biên bản', align:'left', width: 150, sortable:false, resizable:false},
							{field:'formStatusErr', title:'Trạng thái biên bản', align:'center', width: 80, sortable:false, resizable:false,formatter: function(value, row, index){
								var html="";
								if(row.formStatusErr == 1){
									html = '<img src="/resources/images/icon_delete.png" height="20" width="20" style="margin-right:5px;" title="Error">';
								}
								return html;
								
							}},							
						/*	{field:'periodError', title:'Kỳ của biên bản', align:'center', width:80, sortable:false, resizable:false,formatter: function(value, row, index){
								var html="";
								if(row.periodError == 1){
									html = '<img src="/resources/images/icon_delete.png" height="20" width="20" style="margin-right:5px;" title="Error">';
								}
								return html;
								
							}}*/
						]]
					});
				}else{
					$('#successMsgInfo').html('Lưu dữ liệu thành công').show();
				}
				setTimeout(function(){ 
					$('#successMsgInfo').html('').hide();
				}, 3000);
				
				$('#gridStockChange').datagrid('reload');
				$('#gridStockChange').datagrid('uncheckAll');
			}, null, null, null, "Bạn có muốn cập nhật ?", null);			
		}else{
			$('#errMsg').html('Bạn chưa chọn biên bản để lưu').show();
			return false;
		}		
	},
	/**
	 * Cap nhat trang thai thuc hien cua bien ban chuyen kho thanh hoan thanh 
	 * @author dungnt19
	 * @since 29/03/2016
	 */
	donePerformStatusStockChange: function(){
		var rows = $('#gridStockChange').datagrid('getRows');
		if(rows.length == 0){
			$('#errMsg').html('Không có dữ liệu để lưu').show();
			return false;
		}
		var lstRecordSuccess = new Array();
		var lstStatus = new Array();
		var lstPerformStatus = new Array();
		//var lstIdRecord = new Array();
		var lstCheck = $('#gridStockChange').datagrid('getChecked');
		if(lstCheck.length > 0){
			for(var i=0; i < lstCheck.length; i++){
				lstRecordSuccess.push(lstCheck[i].id);
				
				if(lstCheck[i].status == 2) {
					lstStatus.push(lstCheck[i].id);
				}
				
				if(lstCheck[i].performStatus == 2) {
					lstPerformStatus.push(lstCheck[i].id);
				}
			}
			
			if(lstRecordSuccess.length != lstStatus.length){
				$('#errMsg').html('Bạn chỉ được hoàn thành cho biên bản có trạng thái duyệt!').show();
				return;
			}
			
			if(lstRecordSuccess.length != lstPerformStatus.length){
				$('#errMsg').html('Bạn chỉ được hoàn thành cho biên bản có trạng thái thực hiện đang thực hiện!').show();
				return;
			}
			
			var params = new Object();
			params.lstIdRecord = lstRecordSuccess;		
			params.perfromStatusRecord = 1;			
			Utils.addOrSaveData(params, '/equipment-stock-change-manage/save-done-perform-status-record-stock', null, 'errMsg', function(data) {
				if(data.error){
					$('#errMsg').html(data.errMsg).show();
					return;
				}
				if(data.lstRecordError != null && data.lstRecordError.length>0){					
					VCommonJS.showDialogList({		  
						title:'LỖI',
						data : data.lstRecordError,			   
						columns : [[
							{field:'formCode', title:'Mã biên bản', align:'left', width: 150, sortable:false, resizable:false},
							{field:'formStatusErr', title:'Trạng thái biên bản', align:'center', width: 80, sortable:false, resizable:false,formatter: function(value, row, index){
								var html="";
								if(row.formStatusErr == 1){
									html = '<img src="/resources/images/icon_delete.png" height="20" width="20" style="margin-right:5px;" title="Error">';
								}
								return html;
								
							}},							
						/*	{field:'periodError', title:'Kỳ của biên bản', align:'center', width:80, sortable:false, resizable:false,formatter: function(value, row, index){
								var html="";
								if(row.periodError == 1){
									html = '<img src="/resources/images/icon_delete.png" height="20" width="20" style="margin-right:5px;" title="Error">';
								}
								return html;
								
							}}*/
						]]
					});
				}else{
					$('#successMsgInfo').html('Lưu dữ liệu thành công').show();
				}
				setTimeout(function(){ 
					$('#successMsgInfo').html('').hide();
				}, 3000);
				
				$('#gridStockChange').datagrid('reload');
				$('#gridStockChange').datagrid('uncheckAll');
			}, null, null, null, "Bạn có muốn cập nhật ?", null);			
		}else{
			$('#errMsg').html('Bạn chưa chọn biên bản để lưu').show();
			return false;
		}		
	},
	/**
	 * Luu bien ban chuyen kho
	 * @author hoanv25
	 * @since 6/1/2014
	 */
	 createRecordStockChange: function(){
		var msg = '';
		var id = $('#id').val().trim();
//		var fStock = $('#fStock').val().trim();
		
//		var tStock = $('#tStock').val().trim();
		var status = $('#status').val().trim();
		var statusPerform =$('#statusPerform').val().trim();
		$('#errMsg').html('').hide();
//		if($('#fStock').val() == ''){
//				$('#errMsg').html('Bạn chưa nhập giá trị cho Kho giao!').show();
//				return false;
//			}
//		if($('#tStock').val() == ''){
//				$('#errMsg').html('Bạn chưa nhập giá trị cho Kho nhận!').show();
//				return false;
//			}
//		if (msg.length == 0) {
//			msg = Utils.getMessageOfSpecialCharactersValidate('fStock', 'Kho giao',Utils._CODE);
//		}
//		if (msg.length == 0) {
//			msg = Utils.getMessageOfSpecialCharactersValidate('tStock', 'Kho nhận',Utils._CODE);
//		}
//		if(fStock == tStock){
//			$('#errMsg').html('Kho giao không được trùng với Kho nhận').show();
//			return false;
//		}

		var createDate = $('#createDate').val();
		if (msg.length == 0 && $('#createDate').val().trim().length > 0 && !Utils.isDate($('#createDate').val()) && $('#createDate').val() != '__/__/____' ) {
			msg = 'Ngày biên bản không tồn tại hoặc không đúng định dạng dd/mm/yyyy.';
			err = '#createDate';
		}
		var curDate = ReportUtils.getCurrentDateString(); //Lay ngay hien tai sysDate
		if (msg.length == 0 && !Utils.compareDate($('#createDate').val().trim(), curDate)) {
			msg = 'Ngày biên bản không được sau ngày hiện tại. Vui lòng nhập lại';
			$('#createDate').focus();
		}

		if (msg.length == 0 && $('#note').attr('maxlength') > 500) {
			msg = 'Bạn nhập quá giới hạn của trường note';
		}
		if(msg.length>0){
			$('#errMsg').html(msg).show();	
			setTimeout(function(){$('#errMsg').hide();},3000);
			return false;
		}
		if($('#equipmentCodeInGrid').val() == ''){
			$('#errMsg').html('Bạn chưa xử lý xong dòng thiết bị hiện tại!').show();
			return false;
		}
		if($('#seriNumberInGrid').val() == ''){
			$('#errMsg').html('Bạn chưa xử lý xong dòng thiết bị hiện tại!').show();
			return false;
		}
		if($('#toStockInGrid').val() == ''){
			$('#errMsg').html('Bạn chưa xử lý xong dòng thiết bị hiện tại!').show();
			return false;
		}
		var lstId = [];
		var lstEquipCode = new Array();
		var lstSeriNumber = new Array();
		var lstToStockCode = new Array();
		if(EquipmentStockChange._editIndex!=null){
			$('#gridEquipmentStockChange').datagrid('endEdit', EquipmentStockChange._editIndex);
		}
		var rows = $('#gridEquipmentStockChange').datagrid('getRows');
		if (rows != undefined && rows != null && rows.length > 0) {
			for ( var i = 0, size = rows.length; i < size; i++) {
				if(rows[i].idStockRecordDtl == null){
					var equipmentCode = rows[i].equipmentCode;
					if (equipmentCode != null && equipmentCode != undefined) {
						equipmentCode = rows[i].equipmentCode;
					} else {
						try {
							equipmentCode = $('#gridContainer td[field=equipmentCode] input#equipmentCodeInGrid').val();
						} catch(e) {
							// pass through
						}
					}
					lstEquipCode.push(equipmentCode);					
				
					var serialNumber = '';
					if (rows[i].seriNumber) {
						serialNumber = rows[i].seriNumber;
					} else {
						try {
							serialNumber = $('#gridContainer td[field=seriNumber] input#seriNumberInGrid').val();
						} catch(e) {
							// pass through
						}
					}
					lstSeriNumber.push(serialNumber != null && serialNumber != undefined ? serialNumber : '');
					
					var toStockCode = '';
					if (rows[i].toStock) {
						toStockCode = rows[i].toStock;
					} else {
						try {
							toStockCode = $('#gridContainer td[field=toStock] input#toStockInGrid').val().trim();
						} catch(e) {
							// pass through
						}
					}
					lstToStockCode.push(toStockCode != null && toStockCode != undefined ? toStockCode : '');
					
					if(rows[i].equipmentId != undefined){
						lstId.push(rows[i].equipmentId);
					}else{
						lstId.push(-1);
					}
				} else {					
					lstEquipCode.push(rows[i].equipmentCode);
					if (rows[i].seriNumber) {
						lstSeriNumber.push(rows[i].seriNumber);
					}
					lstToStockCode.push(rows[i].toStock);
					if(rows[i].equipmentId != undefined){
						lstId.push(rows[i].equipmentId);
					}else{
						lstId.push(-1);
					}
				}
				if(rows[i].stock!=null){
					var fromStock = rows[i].stock.split('-')[0].trim();
					var toStock = lstToStockCode[i];
					if(fromStock == toStock){
						$('#errMsg').html('Kho giao không được trùng với Kho nhận.').show();
						return false;
					}
				}
			}	
		}else{
			$('#errMsg').html('Vui lòng chọn thiết bị!').show();
			return false;
		}
		
		var dataModel = new Object();	
		dataModel.id = id;
//		dataModel.fromStock = fStock;
//		dataModel.toStock = tStock;
		dataModel.status = status;
		dataModel.createDate = createDate;
		dataModel.note = $('#note').val();
		dataModel.statusPerform = statusPerform;
		if (lstId != undefined && lstId != null && lstId.length >0 ) {
			dataModel.lstId = lstId;
		}
		if(lstEquipCode != null && lstEquipCode.length >0){
			dataModel.lstEquipCode = lstEquipCode;
		}
		if(lstSeriNumber != null && lstSeriNumber.length >0){
			dataModel.lstSeriNumber = lstSeriNumber;	
		}									
		if(lstToStockCode != null && lstToStockCode.length >0){
			dataModel.lstToStockCode = lstToStockCode;	
		}									
		dataModel.lstDell = EquipmentStockChange._lstEquipStockChangeDel.join(',');	
		//dataModel.deletedEquipmentCodes = EquipmentStockChange.deletedEquipmentCodes;

		var arrFile = DropzonePlugin._dropzoneObject.getFilesWithStatus(Dropzone.ADDED);
		if (EquipmentStockChange._arrFileDelete == undefined || EquipmentStockChange._arrFileDelete == null) {
			EquipmentStockChange._arrFileDelete = [];
		}
		dataModel.equipAttachFileStr = EquipmentStockChange._arrFileDelete.join(',');
		if (arrFile != null && arrFile != undefined && arrFile.length > 0) {
				if (EquipmentStockChange._countFile == 5) {
					$('#errMsg').html("Tổng số tập tin đính kèm không được vượt quá 5").show();
					return false;
				}
				var maxLength = 5 - EquipmentStockChange._countFile;
				if (arrFile.length > maxLength) {
					$('#errMsg').html("Số tập tin thêm mới không được vượt quá " + maxLength).show();
					return false;
				}
				msg = UploadUtil.getMsgByFileRequireCheck(UploadUtil._typeFileRequireCheck.equipRecord);
				if (msg.trim().length > 0) {
					$('#errMsg').html(msg).show();
					return false;
				}
				var data = JSONUtil.getSimpleObject2(dataModel);
				$.messager.confirm('Xác nhận', 'Bạn có muốn lưu dữ liệu?', function(r){
					if (r){
						UploadUtil.updateAdditionalDataForUpload(data);
						UploadUtil.startUpload('errMsg');
						return false;
					}
				});
				return false;
		}

		Utils.addOrSaveData(dataModel, '/equipment-stock-change-manage/create-record-stock', null, 'errMsg', function(data){	
			if(data.error){
				$('#errMsg').html(data.errMsg).show();
				return false;
			}else{
				$('#successMsgInfo').html("Lưu dữ liệu thành công").show();	
				EquipmentStockChange._lstEquipInsert = null;
				EquipmentStockChange._editIndex = null;				
				setTimeout(function(){
					$('#successMsgInfo').html("").hide(); 
					window.location.href = '/equipment-stock-change-manage/info';						              		
				}, 1000);
			}
		}, null, null, null, "Bạn có muốn cập nhật biên bản này ?", null);	
		return false;
	},
	/**
	 * Luu bien ban chuyen kho
	 * @author phuongvm
	 * @since 3/4/2015
	 */
	 createRecordStockChangeApproved: function(){
		var msg = '';
		var id = $('#id').val().trim();
		var statusPerform =$('#statusPerform').val().trim();
		$('#errMsg').html('').hide();
	
		var lstId = new Array();
		var lstPerformStatus = new Array();
		var lstToStockCode = new Array();
		
		EquipmentStockChange.editIndex = undefined; 
		var rows = $('#gridEquipmentStockChange').datagrid('getRows');
		if (rows != undefined && rows != null && rows.length > 0) {
			for ( var i = 0, size = rows.length; i < size; i++) {
				$('#gridEquipmentStockChange').datagrid('endEdit', i);
				if($('#toStockInGrid'+i).val()!=undefined){
					$('#errMsg').html('Bạn chưa xử lý xong dòng thiết bị hiện tại!').show();
					return false;
				}
				lstPerformStatus.push(rows[i].performStatus);
				lstToStockCode.push(rows[i].toStock);
				lstId.push(rows[i].idStockRecordDtl);
					
				if(rows[i].stock!=null){
					var fromStock = rows[i].stock.split('-')[0].trim();
					var toStock = lstToStockCode[i];
					if(fromStock == toStock){
						$('#errMsg').html('Kho giao không được trùng với Kho nhận.').show();
						return false;
					}
				}
			}	
		}	
		
		var dataModel = new Object();	
		dataModel.id = id;
		dataModel.statusPerform = statusPerform;
		if (lstId != undefined && lstId != null && lstId.length >0 ) {
			dataModel.lstId = lstId;
		}
		if(lstPerformStatus != null && lstPerformStatus.length >0){
			dataModel.lstPerformStatus = lstPerformStatus;	
		}									
		if(lstToStockCode != null && lstToStockCode.length >0){
			dataModel.lstToStockCode = lstToStockCode;	
		}									
		var arrFile = DropzonePlugin._dropzoneObject.getFilesWithStatus(Dropzone.ADDED);
		if (EquipmentStockChange._arrFileDelete == undefined || EquipmentStockChange._arrFileDelete == null) {
			EquipmentStockChange._arrFileDelete = [];
		}
		dataModel.equipAttachFileStr = EquipmentStockChange._arrFileDelete.join(',');
		if (arrFile != null && arrFile != undefined && arrFile.length > 0) {
				if (EquipmentStockChange._countFile == 5) {
					$('#errMsg').html("Tổng số tập tin đính kèm không được vượt quá 5").show();
					return false;
				}
				var maxLength = 5 - EquipmentStockChange._countFile;
				if (arrFile.length > maxLength) {
					$('#errMsg').html("Số tập tin thêm mới không được vượt quá " + maxLength).show();
					return false;
				}
				msg = UploadUtil.getMsgByFileRequireCheck(UploadUtil._typeFileRequireCheck.equipRecord);
				if (msg.trim().length > 0) {
					$('#errMsg').html(msg).show();
					return false;
				}
				var data = JSONUtil.getSimpleObject2(dataModel);
				$.messager.confirm('Xác nhận', 'Bạn có muốn lưu dữ liệu?', function(r){
					if (r){
						UploadUtil.updateAdditionalDataForUpload(data);
						UploadUtil.startUpload('errMsg');
						return false;
					}
				});
				return false;
		}

		Utils.addOrSaveData(dataModel, '/equipment-stock-change-manage/create-record-stock-approved', null, 'errMsg', function(data){	
			if(data.error){
				$('#errMsg').html(data.errMsg).show();
				return false;
			}else{
				$('#successMsgInfo').html("Lưu dữ liệu thành công").show();	
				EquipmentStockChange._lstEquipInsert = null;
				EquipmentStockChange._editIndex = null;				
				setTimeout(function(){
					$('#successMsgInfo').html("").hide(); 
					window.location.href = '/equipment-stock-change-manage/info';						              		
				}, 1000);
			}
		}, null, null, null, "Bạn có muốn cập nhật biên bản này ?", null);	
		return false;
	},
	/**
	 * Chon kho nguon - kho dich
	 * @author hoanv25
	 * @since 24/12/2014
	 */
	selectStock: function(txt, shortCode, stockId){
		$(txt).val(shortCode);
		if( stockId !=null){
			var equipmentStockTransRecordCode = $('#codeStockTran').val();
			$('#gridEquipmentStockChange').datagrid('load', {fromStockId:  stockId, code: equipmentStockTransRecordCode ? equipmentStockTransRecordCode : ''} );

		}		
//		EquipmentStockChange._toStockDefault = shortCode;
		$('#common-dialog-search-2-textbox').dialog('close');
		$('#common-dialog-search-2-textbox').remove();
//		 var rows = $('#gridEquipmentStockChange').datagrid('getRows');
//		 if (rows != undefined && rows != null && rows.length > 0) {
//			 $.messager.confirm('Xác nhận', 'Bạn có muốn chọn lại Kho nhận ?', function(r){
//					if (r){
//						$(txt).val(shortCode);
//						EquipmentStockChange._toStockDefault = shortCode;
//						$('#common-dialog-search-2-textbox').dialog('close');
//						$('#common-dialog-search-2-textbox').remove();
//						for ( var i = 0, size = rows.length; i < size; i++) {
//							rows[i].toStock = shortCode;
//						}
//						$('#gridEquipmentStockChange').datagrid('loadData', rows );
//					}
//				});	
//		 }else{
//			 $(txt).val(shortCode);
//				if( stockId !=null){
//					var equipmentStockTransRecordCode = $('#codeStockTran').val();
//					$('#gridEquipmentStockChange').datagrid('load', {fromStockId:  stockId, code: equipmentStockTransRecordCode ? equipmentStockTransRecordCode : ''} );
//
//				}		
//				EquipmentStockChange._toStockDefault = shortCode;
//				$('#common-dialog-search-2-textbox').dialog('close');
//				$('#common-dialog-search-2-textbox').remove();
//		 }
	},
	updateAdditionalDataForUpload: function() {
			var programId = $('#programId').val();
			var ndpId = $('#ndpId').val();
			
			var data = JSONUtil.getSimpleObject2({
				programId: programId,
				ndpId: ndpId
			});
			UploadUtil.updateAdditionalDataForUpload(data);
		},
	/**
	 * Xoa mot dong thiet bi trong gird
	 * @author hoanv25
	 * @since 5/1/2015
	 */
	deleteEquipmentStockTransInGrid: function(index, idStockRecordDtl){
		EquipmentStockChange.isHavingInvalidRowOnGrid = false;
		if (idStockRecordDtl > 0) {
			EquipmentStockChange._lstEquipStockChangeDel.push(idStockRecordDtl);
		}		
		var row = $('#gridEquipmentStockChange').datagrid("getRows")[index];
		$('#gridEquipmentStockChange').datagrid("deleteRow", index);
		if(row != null){
			EquipmentStockChange._lstEquipDel.push(row.equipmentCode);
		}
		var rowEquip = $('#gridEquipmentStockChange').datagrid('getRows');
		var nSize = rowEquip.length;
		var equipCode = $('#equipmentCodeInGrid').val();
		var seri = $('#seriNumberInGrid').val();
		var toStockInGrid = $('#toStockInGrid').val();
		
		if($('#equipmentCodeInGrid').val() != undefined){
			nSize = rowEquip.length-1;
			if (!EquipmentStockChange.deletedEquipmentCodes) {
				EquipmentStockChange.deletedEquipmentCodes = new Array();
			}
			EquipmentStockChange.deletedEquipmentCodes.push(rowEquip[rowEquip.length-1].equipmentCode);
			rowEquip[rowEquip.length-1].equipmentCode = null;			
			$('#equipmentCodeInGrid').remove();
			$('#seriNumberInGrid').remove();			
		}
		for(var i=index; i<nSize; i++){
			var row = rowEquip[i];
			$('#gridEquipmentStockChange').datagrid('updateRow',{
				index: i,	
				row: row
			});	
		}
		if( EquipmentStockChange._editIndex!= null && index < EquipmentStockChange._editIndex){
			EquipmentStockChange._editIndex--;
		}else{
			EquipmentStockChange._editIndex=null;
		}
		if(equipCode != undefined || toStockInGrid != undefined){
			$('#gridEquipmentStockChange').datagrid('updateRow',{
				index: rowEquip.length-1,	
				row: rowEquip[rowEquip.length-1]
			});
			if($('#equipmentCodeInGrid').val() != undefined){
				$('#equipmentCodeInGrid').val(equipCode);
			}
			if($('#seriNumberInGrid').val() != undefined){
				$('#seriNumberInGrid').val(seri);
			}		
			if($('#toStockInGrid').val() != undefined){
				$('#toStockInGrid').val(toStockInGrid);
			}		
			$('#equipmentCodeInGrid').width($('#equipmentCodeInGrid').parent().width());
			$('#seriNumberInGrid').width($('#seriNumberInGrid').parent().width());
			EquipmentStockChange.editInputGrid();		
		}
	},

	/**
	 * Them mot dong theit bi
	 * @author hoanv25
	 * @since 25/12/2014
	 */
	insertEquipmentStockTransInGrid: function(){	
		if (EquipmentStockChange.isHavingInvalidRowOnGrid) {
			return;
		}
		$('#errMsg').html('').hide();
		var sz = $("#gridEquipmentStockChange").datagrid("getRows").length;

		var insertNewRow = function() {
			EquipmentStockChange._editIndex = sz;	
			$('#gridEquipmentStockChange').datagrid("appendRow", {});	
			EquipmentStockChange.editInputGrid();
			$("#gridEquipmentStockChange").datagrid("selectRow", EquipmentStockChange._editIndex);	
			$('#equipmentCodeInGrid').focus();
		};

		var isCheckingIfEquipmentCodeValid = false;

		if(EquipmentStockChange._editIndex != null && EquipmentStockChange._editIndex == sz-1){
			var row = $('#gridEquipmentStockChange').datagrid('getRows')[EquipmentStockChange._editIndex];
			if($('#seriNumberInGrid').val() == ''){
				$('#errMsg').html('Bạn chưa xử lý xong dòng thiết bị thêm mới!').show();
				return false;
			}else if($('#seriNumberInGrid').val() != undefined){
				row.seriNumber = $('#seriNumberInGrid').val();
			}	
			
			if($('#toStockInGrid').val() == ''){
				$('#errMsg').html('Bạn chưa xử lý xong dòng thiết bị thêm mới!').show();
				return false;
			}else if($('#toStockInGrid').val() != undefined){
				var toStockCode = $('#toStockInGrid').val().trim();			
				if(!/^[0-9a-zA-Z-_.,]+$/.test(toStockCode)){
					$('#errMsg').html('Mã Kho nhận không hợp lệ!').show();
					return false;
				}
				row.toStock = $('#toStockInGrid').val();
			}	
			if($('#equipmentCodeInGrid').val() == ''){
				$('#errMsg').html('Bạn chưa xử lý xong dòng thiết bị thêm mới!').show();
				return false;
			}else if($('#equipmentCodeInGrid').val() != undefined){
				isCheckingIfEquipmentCodeValid = true;
				var equipCode = $('#equipmentCodeInGrid').val().trim();							
				var params = {equipCode:equipCode};	
				Utils.getJSONDataByAjaxNotOverlay(params, '/equipment-stock-change-manage/getEquipment', 
							function(equipments){
								if(equipments != undefined && equipments != null && equipments.length > 0) {
									insertNewRow();
									EquipmentStockChange.isHavingInvalidRowOnGrid = false;
								} else{
									$('#errMsg').html('Mã thiết bị nhập không đúng!').show();							
									$('#equipmentCodeInGrid').val() == '';
									$('#equipmentCodeInGrid').focus();
									EquipmentStockChange.isHavingInvalidRowOnGrid = true;									
																
									return false;
								}								
				}, null, null);	
				row.equipmentCode = $('#equipmentCodeInGrid').val();
			}		
			
			$('#equipmentCodeInGrid').remove();
			$('#seriNumberInGrid').remove();		
			$('#gridEquipmentStockChange').datagrid('updateRow',{
				index: EquipmentStockChange._editIndex,	
				row: row
			});
			if(EquipmentStockChange._lstEquipInsert == null){
				EquipmentStockChange._lstEquipInsert = new Array();
			}
			if(EquipmentStockChange._lstToStockCodeInsert == null){
				EquipmentStockChange._lstToStockCodeInsert = new Array();
			}
			EquipmentStockChange._lstEquipInsert.push(row.equipmentCode);		
			EquipmentStockChange._lstToStockCodeInsert.push(row.toStock);		
		}
		if (!isCheckingIfEquipmentCodeValid) {
			insertNewRow();
		}		
	},
	/**
	 * popup ma va so seri thiet bi
	 * @author hoanv25
	 * @since 26/12/2014
	 */
	showPopupSelectEquipment:function(dataGrid){
		$('#easyuiPopupSelectEquipment').show();
		$('#easyuiPopupSelectEquipment').dialog({  
	        closed: false,  
	        cache: false,  
	        modal: true,
	        width : 400,
	        height : 'auto',
	        onOpen: function(){	        	
	        	var params = new Object();
	        	var lstEquipId = new Array(); 
	        	var rows = $('#gridEquipmentStockChange').datagrid('getRows');
				if (rows != undefined && rows != null && rows.length > 1){
					for( var i = 0, size = rows.length; i < size; i++){					
						if(rows[i].equipmentId != undefined){
							lstEquipId.push(rows[i].equipmentId);							
						}					
					}
					params.lstEquipId = lstEquipId.join(',');
				}
				$('#gridSelectEquipment').datagrid({					
					data: dataGrid,
					autoRowHeight : true,
					fitColumns:true,
					scrollbarSize:0,			
					queryParams: params,		
					width: $('#gridSelectEquipmentContainer').width(),
					autoWidth: true,
					columns:[[
					    {field: 'equipmentCode',title:'Mã thiết bị',width: 100,sortable:false,resizable:false, align: 'left' },	
					    {field: 'seriNumber',title:'Số serial',width: 100,sortable:false,resizable:false, align: 'left' },	
					    {field: 'select',title:'',width: 100,sortable:false,resizable:false, align: 'left', formatter: function(value, row, index) {
					    	return '<a href="javascript:void(0);" onclick="EquipmentStockChange.selectEquipmentCode('+index+');">Chọn</a>';
						}}			    
					]],
					onBeforeLoad:function(param){											 
					},
					onLoadSuccess :function(data){   			 
						$('.datagrid-header-rownumber').html('STT');				
					}
				});
	        },
	        onClose: function(){
	        	$('#easyuiPopupSelectEquipment').hide();
	        }
		});
	},
	/**
	 * Chon ma, so seri thiet bi
	 * @author hoanv25
	 * @since 25/12/2014
	 */
	selectEquipmentCode: function(index){
		var rowPopup = $('#gridSelectEquipment').datagrid('getRows')[index];
		rowPopup.contentDelivery = null;
		if(EquipmentStockChange._editIndex!=null){
			$('#gridEquipmentStockChange').datagrid('updateRow',{
				index: EquipmentStockChange._editIndex,	// index start with 0
				row: rowPopup
			});
			$('#equipmentCodeInGrid').width($('#equipmentCodeInGrid').parent().width());
			$('#seriNumberInGrid').width($('#seriNumberInGrid').parent().width());
			EquipmentStockChange.editInputGrid();
		}
		$('#easyuiPopupSelectEquipment').dialog('close');
	},
	/**
	 * In bien ban chuyen kho thiet bi
	 * @author hoanv25
	 * @since 16/1/2014
	 */
	printRecodeStockTran: function(){
			$('#errMsg').html("").hide();
		var lstPrintRecodeStockTran = new Array();
		var rows = $('#gridStockChange').datagrid('getRows');		
		if(rows.length == 0){
			$('#errMsg').html('Không có dữ liệu để In').show();
			return false;
		}
		var lstCheck = $('#gridStockChange').datagrid('getChecked');
		if(lstCheck.length > 0){
			for (var i=0, sz = lstCheck.length; i < sz; i++) {
				lstPrintRecodeStockTran.push(lstCheck[i].id);
			}
			var dataModel = new Object();
			dataModel.lstPrintRecodeStockTran = lstPrintRecodeStockTran;
		} else if(lstCheck.length==0){
			$('#errMsg').html('Bạn chưa chọn biên bản').show();
			return false;
		}
		$.messager.confirm('Xác nhận','Bạn có muốn xem bản in biên bản?',function(r){
			if(r){				
				Utils.getHtmlDataByAjax(dataModel, '/equipment-stock-change-manage/print', function(data){
					console.log(data);
					try {
						var _data = JSON.parse(data);
						if(_data.error && _data.errMsg != undefined) {
							$('#errMsg').html(_data.errMsg).show();
						} else {
							var width = $(window).width();
							var height = $(window).height();
							window.open('/equipment-stock-change-manage/open-view?windowWidth='+width+'&windowHeight='+height);
						}
					} catch (e) {
					}
				}, null, null);
			}
		});		
		return false;
	},
	/**
	 * Xem chi tiet bien ban chuyen kho thiet bi
	 * @author hoanv25
	 * @since 16/1/2014
	 */
	viewRecodeStockTran: function(id){
		$('#errMsg').html("").hide();
		var lstPrintRecodeStockTran = new Array();
		if(id != null){			
				lstPrintRecodeStockTran.push(id);		
			var dataModel = new Object();
			dataModel.lstPrintRecodeStockTran = lstPrintRecodeStockTran;
		} else {
			$('#errMsg').html('Bạn chưa chọn biên bản').show();
			return false;
		}
		$.messager.confirm('Xác nhận','Bạn có muốn xem bản in biên bản?',function(r){
			if(r){				
				Utils.getHtmlDataByAjax(dataModel, '/equipment-stock-change-manage/print', function(data){
					console.log(data);
					try {
						var _data = JSON.parse(data);
						if(_data.error && _data.errMsg != undefined) {
							$('#errMsg').html(_data.errMsg).show();
						} else {
							var width = $(window).width();
							var height = $(window).height();
							window.open('/equipment-stock-change-manage/open-view?windowWidth='+width+'&windowHeight='+height);
						}
					} catch (e) {
					}
				}, null, null);
			}
		});		
		return false;
	},
	/**
	 * Show popup tim kiem thiet bi
	 * @author hoanv25
	 * @since 25/12/2014
	 */
	showPopupSearchEquipment: function(){
		$('#easyuiPopupSearchEquipment').show();
		$('#easyuiPopupSearchEquipment').dialog({  
	        closed: false,  
	        cache: false,  
	        modal: true,
	        width : 1200,
	        height : 'auto',
	        onOpen: function(){	 	        
	        	Utils.bindComboboxEquipGroupEasyUICodeName('groupEquipment', null, 193);
	        	Utils.bindComboboxEquipGroupEasyUICodeName('providerId', null, 193);	
	        	var year = new Date();
	        	year = year.getFullYear(); 
	        	var op = '<option value="" selected="selected"></option><option value="'+year+'">'+year+'</option>';
	        	for(var i=1; i<100; i++){
	        		op+='<option value="'+(year-i)+'">'+(year-i)+'</option>';
	        	}
	        	$('#yearManufacturing').html(op);
	        	$('#equipmentCode, #seriNumber, #typeEquipment').bind('keyup',function(event){
	        		if(event.keyCode == keyCodes.ENTER){
	        			$('#btnSeachEquipmentDlg').click(); 
	        		}
	        	});
	        	$('#yearManufacturing').bind('keyup',function(event){
	        		if(event.keyCode == keyCodes.ENTER){
	        			$('#btnSeachEquipmentDlg').click(); 
	        		}
	        	});
	        	$('#groupEquipment, #providerId').next().find(".combo-text.validatebox-text").bind('keyup',function(event){
	        		if(event.keyCode == keyCodes.ENTER){
	        			$('#btnSeachEquipmentDlg').click(); 
	        		}
	        	});      
	        	$('#equipmentCode').focus();	
//				$("#stockCode").val($("#fStock").val().trim());
	        	EquipmentStockChange.getEquipCatalog();
	        	var params = new Object();
	        	var groupId = $('#groupEquipment').combobox('getValue');
	        	if (groupId == -1 || groupId == "") {
	        		groupId = "";
	        	}
	        	var providerId = $('#providerId').combobox('getValue');
	        	if (providerId == -1 || providerId == "") {
	        		providerId = "";
	        	}
				params = {
						//equipId :$('#equipmentId').val().trim(),
						//fromStock : $('#fStock').val().trim(),
						equipCode: $('#equipmentCode').val().trim(),
						seriNumber: $('#seriNumber').val().trim(),
						categoryId: $('#typeEquipment').val().trim(),
						// groupId: $('#groupEquipment').val().trim(),
						// providerId: $('#providerId').val().trim(),
						groupId: groupId,
						providerId: providerId,
						yearManufacture: $('#yearManufacturing').val().trim(),
						stockCode: $('#stockCode').val().trim()
					};	
				var lstEquipId = new Array(); 
	        	var rows = $('#gridEquipmentStockChange').datagrid('getRows');
				if (rows != undefined && rows != null && rows.length > 1){
					for( var i = 0, size = rows.length; i < size; i++){					
						if(rows[i].equipmentId != undefined){
							lstEquipId.push(rows[i].equipmentId);							
						}					
					}
					params.lstEquipId = lstEquipId.join(',');
				}
				if($("#id").val() != "" && EquipmentStockChange._lstEquipInRecord != null){
					params.lstEquipDelete = "";
					for(var i=0; i<EquipmentStockChange._lstEquipInRecord.length; i++){
						params.lstEquipDelete += EquipmentStockChange._lstEquipInRecord[i]+',';
					}					
				}
				if(EquipmentStockChange._lstEquipInsert != null){
					params.lstEquipAdd = "";
					for(var i=0; i<EquipmentStockChange._lstEquipInsert.length; i++){
						params.lstEquipAdd += EquipmentStockChange._lstEquipInsert[i]+',';
					}			
				}
				if(EquipmentStockChange._lstEquipDel != null){
					params.lstEquipDelete = "";
					for(var i=0; i<EquipmentStockChange._lstEquipDel.length; i++){
						params.lstEquipDelete += EquipmentStockChange._lstEquipDel[i]+',';
					}			
				}
				$('#equipmentGridDialog').datagrid({
					url : '/equipment-stock-change-manage/search-equipment',	
					autoRowHeight : true,
					rownumbers : true, 
					//fitColumns:true,
					pagination:true,
					scrollbarSize:0,
					pageSize:10,
					pageList: [10],
					width: $('#equipmentGridDialogContainer').width(),
					height: 'auto',
					autoWidth: true,	
					queryParams: params,
					columns:[[
						{field: 'select',title:'',width: 50,sortable:false,resizable:false, align: 'left', formatter: function(value, row, index) {
					    	return '<a href="javascript:void(0);" onclick="EquipmentStockChange.selectEquipment('+index+');">Chọn</a>';
						}},
					    {field: 'equipmentCode',title:'Mã thiết bị',width: 180,sortable:false,resizable:false, align: 'left', formatter: function(value, row, index) {
					    	var html="";
							if(row.equipmentCode != null){
								html = row.equipmentCode; 
							}
							return Utils.XSSEncode(html);
						}},	
					    {field: 'seriNumber',title:'Số serial',width: 100,sortable:false,resizable:false, align: 'left', formatter: function(value, row, index) {
					    	var html="";
							if(row.seriNumber != null){
								html = row.seriNumber; 
							}
							return Utils.XSSEncode(html);
						}},	
					    {field: 'healthStatus',title:'Tình trạng thiết bị',width: 85,sortable:false,resizable:false, align: 'left', formatter: function(value, row, index) {
					    	var html="";
							if(row.healthStatus != null){
								html = row.healthStatus; 
							}
							return Utils.XSSEncode(html);
						}},	
					    {field: 'stock',title:'Kho',width: 120,sortable:false,resizable:false, align: 'left', formatter: function(value, row, index) {
					    	var html="";
							if(row.stock != null){
								html = row.stock; 
							}
							return Utils.XSSEncode(html);
						}},	
					    {field: 'typeEquipment',title:'Loại thiết bị',width: 80,sortable:false,resizable:false, align: 'left', formatter: function(value, row, index) {
					    	var html="";
							if(row.typeEquipment != null){
								html = row.typeEquipment; 
							}
							return Utils.XSSEncode(html);
						}},	
					    {field: 'groupEquipment',title:'Nhóm thiết bị',width: 200,sortable:false,resizable:false, align: 'left', formatter: function(value, row, index) {
					    	var html="";
							if(row.groupEquipmentCode != null && row.groupEquipmentName!= null){
								html = row.groupEquipmentCode+' - '+row.groupEquipmentName; 
							}
							return Utils.XSSEncode(html);
						}},	
					    {field: 'capacity',title:'Dung tích (lít)',width: 80,sortable:false,resizable:false, align: 'right' },	
					    {field: 'equipmentBrand',title:'Hiệu',width: 80,sortable:false,resizable:false, align: 'left', formatter: function(value, row, index) {
					    	var html="";
							if(row.equipmentBrand != null){
								html = row.equipmentBrand; 
							}
							return Utils.XSSEncode(html);
						}},	
					    {field: 'equipmentProvider',title:'NCC',width: 80,sortable:false,resizable:false, align: 'left', formatter: function(value, row, index) {
					    	var html="";
							if(row.equipmentProvider != null){
								html = row.equipmentProvider; 
							}
							return Utils.XSSEncode(html);
						} },	
					    {field: 'year',title:'Năm sản xuất',width: 80,sortable:false,resizable:false, align: 'right' },											
					]],
					onBeforeLoad:function(param){											 
					},
					onLoadSuccess :function(data){   			 
						if (data == null || data.total == 0) {						
						}else{							
							$('.datagrid-header-rownumber').html('STT');		    	
						}	    		
					}
				});
	        },
			onClose: function(){
				$('#easyuiPopupSearchEquipment').hide();
				$('#equipmentCode').val('').change();
				$('#seriNumber').val('').change();
				$('#typeEquipment').html('<option value="" selected="selected">Tất cả</option>');
				$('#groupEquipment').html('<option value="" selected="selected">Tất cả</option>').change();
				$('#providerId').html('<option value="" selected="selected">Tất cả</option>');
				$('#yearManufacturing').val('').change();
				$('#stockCode').val('').change();
			}
		});
		 $('#stockCode').bind('keyup', function(event) {
				 if(event.keyCode == keyCode_F9){
					 EquipmentStockChange.showPopupSearchStock('stockCode');
				}
		});
	},
	/**
	 * Show popup tim kiem kho
	 * @author phuongvm
	 * @since 30/03/2015
	 */
	showPopupSearchStock: function(txtInput){
		var txt = "toStockInGrid";
		if(txtInput!=undefined){
			txt = txtInput;
		}
		try {
			$('#common-dialog-search-2-textbox').dialog("close");
			$('#common-dialog-search-2-textbox').remove();
		} catch (e) {
			console.log('Khong mo duoc dialog');
		} 

		VCommonJS.showDialogSearch2({
			inputs : [
		        {id:'code', maxlength:50, label:'Mã đơn vị'},
		        {id:'name', maxlength:250, label:'Tên đơn vị'},
		    ],
		    url : '/equipment-stock-change-manage/get-list-equip-stock-vo',
		    columns : [[
		        {field:'codeName', title:'Đơn vị', align:'left', width: 110, sortable:false, resizable:false, formatter:function(value,row,index) {
		        	var nameCode = row.shopCode + '-' + row.shopName;
		        	return Utils.XSSEncode(nameCode);         
		        }},
		        {field:'code', title:'Mã kho', align:'left', width: 200, sortable:false, resizable:false},
		        {field:'choose', title:'', align:'center', width:40, sortable:false, resizable:false, formatter:function(value,row,index) {
//			        	var nameCode = row.shopCode + '-' + row.name;
		            return '<a href="javascript:void(0)" onclick="Utils.fillCodeForInputTextByDialogSearch2(\''+txt+'\', ' + row.equipStockId +', \''+ Utils.XSSEncode(row.code)+'\', \''+ Utils.XSSEncode(row.name) +'\','+Utils._isShowInputTextByDlSearch2.code+');">chọn</a>';         
		        }}
		    ]]
		});
	},
	/**
	 * Chon thiet bi
	 * @author hoanv25
	 * @since 6/1/2014
	 */
	selectEquipment: function(index){
		var rowPopup = $('#equipmentGridDialog').datagrid('getRows')[index];
		rowPopup.contentDelivery = null;
		if(EquipmentStockChange._editIndex!=null){
			$('#gridEquipmentStockChange').datagrid('updateRow',{
				index: EquipmentStockChange._editIndex,	// index start with 0
				row: rowPopup
			});
			$('#equipmentCodeInGrid').width($('#equipmentCodeInGrid').parent().width());
			$('#seriNumberInGrid').width($('#seriNumberInGrid').parent().width());
			EquipmentStockChange.editInputGrid();		
		}
		$('#easyuiPopupSearchEquipment').dialog('close');
	},
	/**
	 * Lay loai, ncc thiet bi
	 * @author hoanv25
	 * @since 25/12/2014
	 */
	getEquipCatalog: function(){
		$('#typeEquipment').html('<option value="" selected="selected">Tất cả</option>');
    	$('#provider').html('<option value=""  selected="selected">Tất cả</option>');
    	
		$.getJSON('/equipment-manage-delivery/get-equipment-catalog', function(result) {				
			if(result.equipsCategory != undefined && result.equipsCategory != null && result.equipsCategory.length > 0) {					
				for(var i=0; i < result.equipsCategory.length; i++){
					$('#typeEquipment').append('<option value="'+ result.equipsCategory[i].id +'">'+Utils.XSSEncode(result.equipsCategory[i].code)+' - '+Utils.XSSEncode(result.equipsCategory[i].name) +'</option>');  
				}
			} 
			$('#typeEquipment').change();
			// if(result.equipProviders != undefined && result.equipProviders != null && result.equipProviders.length > 0) {					
			// 	for(var i=0; i < result.equipProviders.length; i++){
			// 		$('#providerId').append('<option value="'+ result.equipProviders[i].id +'">'+ Utils.XSSEncode(result.equipProviders[i].name) +'</option>');  
			// 	}
			// } 
			// $('#providerId').change();	
			//tamvnm: thay doi thanh autoCompleteCombobox
			if(result.equipProviders != undefined && result.equipProviders != null && result.equipProviders.length > 0) {					
				for (var i = 0, isize = result.equipProviders.length; i < isize; i++) {
					result.equipProviders[i].searchText = unicodeToEnglish(result.equipProviders[i].code + " " + result.equipProviders[i].name);
 					result.equipProviders[i].searchText = result.equipProviders[i].searchText.toUpperCase();
				}
				var obj = {
					id: -1,
					code: "Tất cả",
					name: "Tất cả",
					codeName: "Tất cả",
					searchText: unicodeToEnglish("Tất cả").toUpperCase()
				};
				result.equipProviders.splice(0, 0, obj);
				$('#providerId').combobox("loadData", result.equipProviders);
		 		$('#providerId').combobox("setValue", -1);
			}			
		});
	},	
		/**
	 * Tim kiem thiet bi tren popup
	 * @author hoanv25
	 * @since 12/1/2015
	 */
	searchEquipmentStock: function(){
		var params=new Object(); 
		params.equipCode = $('#equipmentCode').val().trim();
		params.seriNumber = $('#seriNumber').val().trim();
		params.categoryId = $('#typeEquipment').val().trim();
		//tamvnm: thay doi thanh autoCompleteCombobox: -1 la truong hop tim group ko co trong BD
		// params.groupId = $('#groupEquipment').val().trim();
		if ($('#groupEquipment').combobox('getValue') == "") {
			$('#groupEquipment').combobox('setValue', -1);
			params.groupId = "";
		} else if ($('#groupEquipment').combobox('getValue') == -1) {
			params.groupId = "";
		} else {
			if (Utils.getMessageOfInvaildNumberNew($('#groupEquipment').combobox("getValue")).length == 0) {
				params.groupId = $('#groupEquipment').combobox("getValue");
			} else {
				params.groupId = -1;
			}
		}
		// params.providerId = $('#providerId').val().trim();
		if ($('#providerId').combobox('getValue') == "") {
			$('#providerId').combobox('setValue', -1);
			params.providerId = "";
		} else if ($('#providerId').combobox('getValue') == -1) {
			params.providerId = "";
		} else {
			if (Utils.getMessageOfInvaildNumberNew($('#providerId').combobox("getValue")).length == 0) {
				params.providerId = $('#providerId').combobox("getValue");
			} else {
				params.providerId = -1;
			}
		}
		params.yearManufacture = $('#yearManufacturing').val().trim();
		params.stockCode = $('#stockCode').val().trim();
		params.fromStock = $('#stockCode').val().trim();
		var lstEquipId = new Array(); 
	        	var rows = $('#gridEquipmentStockChange').datagrid('getRows');
				if (rows != undefined && rows != null && rows.length > 1){
					for( var i = 0, size = rows.length; i < size; i++){					
						if(rows[i].equipmentId != undefined){
							lstEquipId.push(rows[i].equipmentId);							
						}					
					}
					params.lstEquipId = lstEquipId.join(',');
				}
		$('#equipmentGridDialog').datagrid('load',params);
	},
	/**
	 * su kien tren grid
	 * @author hoanv25
	 * @since 25/12/2014
	 */
	editInputGrid: function(){	
			//Event F9 ma thiet bi
			$('input[id*=equipmentCodeInGrid]').bind('keyup', function(e){
				var equipmentCodeInput = this;
				if(e.keyCode==keyCode_F9){			
				$('#errMsg').html('').show();				
//					var fromStock = $('#fStock').val().trim();
//					if(fromStock != undefined && fromStock != null && fromStock != ''){
//					var fromStock = "";
//						var params = {fromStock:fromStock};					
//						Utils.getJSONDataByAjaxNotOverlay(params, '/equipment-stock-change-manage/getCheckFromStockEquipment', 
//							function(equipments){
//								if(equipments != undefined && equipments != null && equipments.length > 0) {
									EquipmentStockChange.showPopupSearchEquipment();
//								} else{
//									$('#errMsg').html('Kho giao không tồn tại thiết bị!').show();
//									return false;
//								}								
//							}, null, null);
//					}else{
//						$('#errMsg').html('Bạn chưa nhập Kho giao!').show();
//						return false;
//					}					
				} else if(e.keyCode==keyCodes.ENTER){
				$('#errMsg').html('').show();				
//					var fromStock = $('#fStock').val().trim();
//					if(fromStock != undefined && fromStock != null && fromStock != ''){
//						var params = {fromStock:fromStock};					
//						Utils.getJSONDataByAjaxNotOverlay(params, '/equipment-stock-change-manage/getCheckFromStockEquipment', 
//							function(equipments){
//							if(equipments != undefined && equipments != null && equipments.length > 0) {
//									var equipCode = $(equipmentCodeInput).val().trim();									
//								if(equipCode != undefined && equipCode != null && equipCode != ''){
//									var params = {equipCode:equipCode};		
//									var lstEquipId = new Array(); 
//	        						var rows = $('#gridEquipmentStockChange').datagrid('getRows');
//										if (rows != undefined && rows != null && rows.length > 1){
//										for( var i = 0, size = rows.length; i < size; i++){					
//											if(rows[i].equipmentId != undefined){
//												lstEquipId.push(rows[i].equipmentId);							
//												}					
//											}
//											params.lstEquipId = lstEquipId.join(',');
//										}			
//										var fromStock = $('#fStock').val().trim();
//											if(fromStock != undefined && fromStock != null && fromStock != ''){		
//											params.fromStock = fromStock;	
//											}
//									Utils.getJSONDataByAjaxNotOverlay(params, '/equipment-stock-change-manage/getEquipment', function(equipments){						
//									if(equipments != undefined && equipments != null && equipments.length > 0) {										
//											$('#gridEquipmentStockChange').datagrid('updateRow',{
//											index: EquipmentStockChange._editIndex,	
//											row: equipments[0]
//											});	
//											EquipmentStockChange.editInputGrid();
//										} else{
//										$('#errMsg').html('Mã thiết bị không hợp lệ!').show();											
//										$('#equipmentCodeInGrid').val('');
//										return false;
//										}
//										$('#equipmentCodeInGrid').change();	
//									}, null, null);
//								}	
//							}else{
//									$('#errMsg').html('Kho giao không tồn tại thiết bị!').show();
//									return false;
//								} 
//								//		EquipmentStockChange.editInputGrid();
//							}, null, null);
//					}else{
//						$('#errMsg').html('Bạn chưa nhập Kho giao!').show();
//						return false;
//					}	
					var equipCode = Utils.XSSEncode($(equipmentCodeInput).val().trim());									
					if(equipCode != undefined && equipCode != null && equipCode != ''){
						var params = {equipCode:equipCode};		
						var lstEquipId = new Array(); 
						var rows = $('#gridEquipmentStockChange').datagrid('getRows');
							if (rows != undefined && rows != null && rows.length > 1){
							for( var i = 0, size = rows.length; i < size; i++){					
								if(rows[i].equipmentId != undefined){
									lstEquipId.push(rows[i].equipmentId);							
									}					
								}
								params.lstEquipId = lstEquipId.join(',');
							}			
							
						Utils.getJSONDataByAjaxNotOverlay(params, '/equipment-stock-change-manage/getEquipment', function(equipments){						
						if(equipments != undefined && equipments != null && equipments.length > 0) {										
								$('#gridEquipmentStockChange').datagrid('updateRow',{
								index: EquipmentStockChange._editIndex,	
								row: equipments[0]
								});	
								EquipmentStockChange.editInputGrid();
							} else{
							$('#errMsg').html('Mã thiết bị không hợp lệ!').show();											
							$('#equipmentCodeInGrid').val('');
							return false;
							}
							$('#equipmentCodeInGrid').change();	
						}, null, null);
					}
				}
			});	
			
			//Event F9 ma Kho nhận
			$('input[id*=toStockInGrid]').bind('keyup', function(e){
				var toStockCodeInput = this;
				if(e.keyCode==keyCode_F9){			
					$('#errMsg').html('').show();				
					EquipmentStockChange.showPopupSearchStock();
				} else if(e.keyCode==keyCodes.ENTER){
//					$('#errMsg').html('').show();				
//					var fromStock = $('#fStock').val().trim();
//					if(fromStock != undefined && fromStock != null && fromStock != ''){
//						var params = {fromStock:fromStock};					
//						Utils.getJSONDataByAjaxNotOverlay(params, '/equipment-stock-change-manage/getCheckFromStockEquipment', 
//							function(equipments){
//							if(equipments != undefined && equipments != null && equipments.length > 0) {
//									var equipCode = $(equipmentCodeInput).val().trim();									
//								if(equipCode != undefined && equipCode != null && equipCode != ''){
//									var params = {equipCode:equipCode};		
//									var lstEquipId = new Array(); 
//	        						var rows = $('#gridEquipmentStockChange').datagrid('getRows');
//										if (rows != undefined && rows != null && rows.length > 1){
//										for( var i = 0, size = rows.length; i < size; i++){					
//											if(rows[i].equipmentId != undefined){
//												lstEquipId.push(rows[i].equipmentId);							
//												}					
//											}
//											params.lstEquipId = lstEquipId.join(',');
//										}			
//										var fromStock = $('#fStock').val().trim();
//											if(fromStock != undefined && fromStock != null && fromStock != ''){		
//											params.fromStock = fromStock;	
//											}
//									Utils.getJSONDataByAjaxNotOverlay(params, '/equipment-stock-change-manage/getEquipment', function(equipments){						
//									if(equipments != undefined && equipments != null && equipments.length > 0) {										
//											$('#gridEquipmentStockChange').datagrid('updateRow',{
//											index: EquipmentStockChange._editIndex,	
//											row: equipments[0]
//											});	
//											EquipmentStockChange.editInputGrid();
//										} else{
//										$('#errMsg').html('Mã thiết bị không hợp lệ!').show();											
//										$('#equipmentCodeInGrid').val('');
//										return false;
//										}
//										$('#equipmentCodeInGrid').change();	
//									}, null, null);
//								}	
//							}else{
//									$('#errMsg').html('Kho giao không tồn tại thiết bị!').show();
//									return false;
//								} 
//								//		EquipmentStockChange.editInputGrid();
//							}, null, null);
//					}else{
//						$('#errMsg').html('Bạn chưa nhập Kho giao!').show();
//						return false;
//					}									
				}
			});	
			
			var serialInputEventHandler = function(element) {
				var serialInput = element;
				$('#errMsg').html('').show();	
				var fromStock = '';
//				var fromStock = $('#fStock').val().trim();
//				if(fromStock != undefined && fromStock != null && fromStock != ''){
					var params = {fromStock:fromStock};					
					Utils.getJSONDataByAjaxNotOverlay(params, '/equipment-stock-change-manage/getCheckFromStockEquipment', 
						function(equipments){
							if(equipments != undefined && equipments != null && equipments.length > 0) {
								// if(e.keyCode==keyCodes.ENTER){
									var seriNumber = $(serialInput).val().trim();
									if(seriNumber != undefined && seriNumber != null && seriNumber != ''){
									var params = {seriNumber:seriNumber};	
									var lstEquipId = new Array(); 
        							var rows = $('#gridEquipmentStockChange').datagrid('getRows');
									if (rows != undefined && rows != null && rows.length > 1){
									for( var i = 0, size = rows.length; i < size; i++){					
										if(rows[i].equipmentId != undefined){
											lstEquipId.push(rows[i].equipmentId);							
											}					
										}
										params.lstEquipId = lstEquipId.join(',');
									}	
//									var fromStock = $('#fStock').val().trim();
										if(fromStock != undefined && fromStock != null && fromStock != ''){		
										params.fromStock = fromStock;	
										}
									Utils.getJSONDataByAjaxNotOverlay(params, '/equipment-stock-change-manage/getEquipment', function(equipments){						
										if(equipments != undefined && equipments != null) {	
										if(equipments.length == 1){
											equipments[0].contentDelivery = null;
											$('#gridEquipmentStockChange').datagrid('updateRow',{
											index: EquipmentStockChange._editIndex,	
											row: equipments[0]
										});							
										EquipmentStockChange.editInputGrid();
										}else if(equipments.length > 1){
											EquipmentStockChange.showPopupSelectEquipment(equipments);
											}else{
											$('#errMsg').html('Mã seri không tồn tại!').show();
											$('#seriNumberInGrid').val('');
											return false;
											}
										} else{
											$('#seriNumberInGrid').val('');
										}
										$('#seriNumberInGrid').change();
									}, null, null);					
									}				
								// }
							} else{
								$('#errMsg').html('Kho giao không tồn tại!').show();
								return false;
							}
							//		EquipmentStockChange.editInputGrid();
						}, null, null);
//				}else{
//					$('#errMsg').html('Bạn chưa nhập Kho giao!').show();
//					return false;
//				}
			};
			//Event Enter so seri	
			if($('#equipmentCodeInGrid').val() == ''){
					$('#seriNumberInGrid')
				/*.bind('blur', function(e){
					if (!$(this).hasClass('keyup-ing')) {
						serialInputEventHandler(this);
					}
					$(this).removeClass('keyup-ing');
				})*/
			.bind('keyup', function(e){
				if (e.keyCode == keyCodes.ENTER) {
					$(this).addClass('keyup-ing');
					serialInputEventHandler(this);
				}				
			});	
			}					
	}, 
	/**
	 * show popup chon file excel
	 * @author phuongvm
	 * @since 06/04/2015
	 */
	showDlgCatalogImportExcel: function(){
		$('#easyuiPopupImportExcel').show();
		$('.ErrorMsgStyle').hide();
		$('#easyuiPopupImportExcel').dialog({  
	        closed: false,  
	        cache: false,  
	        modal: true,
	        onOpen: function(){	 
	        	$('.easyui-dialog .BtnGeneralStyle').css('color','');
	        },
	        onClose: function(){
	        	EquipmentManageCatalog.searchinfo();
				$("#grid").datagrid('uncheckAll');
				$("#fakefilepcCatalog").val("");
				$("#excelFileCatalog").val("");				
	        }
		});
	},
	/**
	 * nhap file excel 
	 * @author phuongvm
	 * @since 06/04/2015
	 */
	importExcelCatalog: function(){
		$('.ErrorMsgStyle').hide();
		VTUtilJS.importExcelUtils(function(data) {
			$("#errExcelMsgCatalog").html(data.message).show();		
			if(data.fileNameFail == null || data.fileNameFail == "" ){					
				setTimeout(function(){
					$('#errExcelMsgCatalog').html("").hide();
					$('#easyuiPopupImportExcel').dialog('close');
				}, 3000);				
			}else{
				$("#fakefilepcCatalog").val("");
				$("#excelFileCatalog").val("");			
				$('#gridStockChange').datagrid('reload');	
			}						
		}, "importFrmCatalog", "excelFileCatalog", null, "errExcelMsgCatalog");
		return false;
	},
	/**
	 * xuat file excel 
	 * @author phuongvm
	 * @since 06/04/2015
	 */
	exportExcel: function(){
		var msg = '';
		var code = $('#codeStockTran').val().trim();
		var status = $('#status').val().trim();
		var statusPerform = $('#statusPerform').val().trim();
		var fDate = $('#fDate').val();
		var tDate = $('#tDate').val();
		var cFDate = $('#cFDate').val();
		var cTDate = $('#cTDate').val();
		var tStock = $('#tStock').val().trim();
		var fStock = $('#fStock').val().trim();
		if (msg.length == 0) {
			msg = Utils.getMessageOfSpecialCharactersValidate('codeStockTran', 'Mã biên bản', Utils._CODE);
		}
		if (msg.length == 0 && $('#fDate').val().trim().length > 0 && !Utils.isDate($('#fDate').val(), '/') && $('#fDate').val() != '__/__/____') {
			msg = 'Từ ngày không tồn tại hoặc không đúng định dạng dd/mm/yyyy.';
			err = '#fDate';
		}
		if (msg.length == 0 && $('#tDate').val().trim().length > 0 && !Utils.isDate($('#tDate').val(), '/') && $('#tDate').val() != '__/__/____') {
			msg = 'Đến ngày không tồn tại hoặc không đúng định dạng dd/mm/yyyy.';
			err = '#tDate';
		}
		if (msg.length == 0 && fDate.length > 0 && tDate.length > 0 && !Utils.compareDate(fDate, tDate)) {
			msg = 'Từ ngày tạo phải là ngày trước hoặc cùng ngày với đến ngày tạo';
		}

		if (msg.length == 0 && $('#cFDate').val().trim().length > 0 && !Utils.isDate($('#cFDate').val(), '/') && $('#cFDate').val() != '__/__/____') {
			msg = 'Từ ngày không tồn tại hoặc không đúng định dạng dd/mm/yyyy.';
			err = '#cFDate';
		}
		if (msg.length == 0 && $('#cTDate').val().trim().length > 0 && !Utils.isDate($('#cTDate').val(), '/') && $('#cTDate').val() != '__/__/____') {
			msg = 'Đến ngày không tồn tại hoặc không đúng định dạng dd/mm/yyyy.';
			err = '#cTDate';
		}
		if (msg.length == 0 && cFDate.length > 0 && tDate.length > 0 && !Utils.compareDate(cFDate, cTDate)) {
			msg = 'Từ ngày phải là ngày trước hoặc cùng ngày với đến ngày';
		}
		if (msg.length > 0) {
			$('#errMsg').html(msg).show();	
			setTimeout(function() {
				$('#errMsg').hide();
			}, 3000);
			return false;
		}
		var params =  new Object();
		params.code = code;
		params.status = status;
		params.statusPerform = statusPerform;
		params.fromDate = fDate;
		params.toDate = tDate;
		params.createFromDate = cFDate;
		params.createToDate = cTDate;
		params.toStock = tStock;
		params.fromStock = fStock;
		$.messager.confirm('Xác nhận', 'Bạn có muốn xuất file excel?', function(r) {
			if (r) {
				ReportUtils.exportReport('/equipment-stock-change-manage/export-excel', params, 'errMsg');					
			}
		});		
		return false;
	},
	/*** BEGIN VUONGMQ * @date Mar 16,215 QUAN LY KHO THIET BI */
	/**
	 * Tim kiem danh sach thiet bi
	 * @author VuongMQ
	 * @date Mar 16,215
	 * */
	searchListEquipmentStock: function () {
		$('.SuccessMsgStyle').html('').hide();
		$('.ErrorMsgStyle').html('').hide();
		$('#equipHistoryTemplateDiv').css("visibility","hidden").hide();
		var msg = '';
		var err = '';
		var fDate = $('#fDate').val();
		var tDate = $('#tDate').val();
		if(msg.length == 0 && fDate.length > 0 && tDate.length > 0 && !Utils.compareDate(fDate, tDate)){
			msg = 'Từ ngày phải là ngày trước hoặc cùng ngày với đến ngày';
			err = '#fDate';
		}
		if(msg.length > 0){
			$('#errMsg').html(msg).show();
			$(err).focus();
			$('#listEquipmentDg').datagrid('loadData', []);
			return false;
		}
		EquipmentStockChange._params = {
			shopCode: $('#txtShopCode').val(),
			code: $('#txtCode').val(),
			name: $('#txtName').val(),
			status: $('#ddlUsageStatus').val(),
			fromDate: $('#fDate').val(),
			toDate: $('#tDate').val()
		};
		$('#listEquipmentDg').datagrid('load', EquipmentStockChange._params);
	},
	/**
	 * Xuat Excel Theo Tim kiem danh sach kho thiet bi
	 * @author VuongMQ
	 * @date Mar 16,215
	 * */
	exportExcelBySearchEquipmentStock: function() {
		//alert('Xuất Excel nè');
		$('.SuccessMsgStyle').html('').hide();
		$('.ErrorMsgStyle').html('').hide();
		var msg = '';
		if(EquipmentStockChange._params != null){
			var rows = $('#listEquipmentDg').datagrid('getRows');
			if(rows == null || rows.length==0){
				msg = "Không có dữ liệu xuất Excel";
			}
		}else{
			msg = "Không có dữ liệu xuất Excel";
		}
		if(msg.length > 0){
			$('#errMsg').html(msg).show();
			return false;
		}
		var msg = 'Bạn có muốn xuất Excel?';
		$.messager.confirm('Xác nhận', msg, function(r){
			if (r){
				ReportUtils.exportReport('/equipment-manage/exportExcelBySearchEquipmentStock', EquipmentStockChange._params, 'errMsg');
			}
		});
	},

	/**
	 * Mo Dialog Nhap Excel kho thiet bi
	 * @author VuongMQ
	 * @date Mar 16,215
	 * */
	openDialogImportListEquipmentStock: function (){
		//alert('Nhập Excel nè');
		$('.SuccessMsgStyle').html('').hide();
		$('.ErrorMsgStyle').html('').hide();
		$('#easyuiPopupImportExcel').dialog({
			title: 'Nhập Excel Kho thiết bị',
			width: 500, 
			height: 'auto',
			closed: false,
			cache: false,
			modal: true,
			onOpen: function() {
				$('#fakefilepcEquipStock').val('');
			},
			onClose: function() {
				$('.ErrorMsgStyle').html('').hide();
			}
		});
	},
	/**
	 * Import Excel kho thiet bi
	 * @author vuongmq
	 * @date Mar 20, 2015
	 * */
	importEquipmentStock: function(){
		$('.SuccessMsgStyle').html('').hide();
		$('.ErrorMsgStyle').html('').hide();
		if($('#fakefilepcEquipStock').val().length <= 0 ){
			$('#errExcelMsgEquipStock').html('Vui lòng chọn tập tin Excel').show();
			return false;
		}
		Utils.importExcelUtils(function(data){
			//EquipmentListEquipment.searchListEquipment();
			$('#fakefilepcEquipStock').val("").change();
			if (data.message!=undefined && data.message!=null && data.message.trim().length >0){
				$('#errExcelMsgEquipStock').html(data.message.trim()).change().show();
			}else{
				$('#successExcelMsgEquipStock').html("Lưu dữ liệu thành công").show();
				var tm = setTimeout(function(){
					$('.SuccessMsgStyle').html('').hide();
					clearTimeout(tm);
					$('#easyuiPopupImportExcel').dialog('close');
					$('#listEquipmentDg').datagrid("load", EquipmentStockChange._params);
				 }, 1500);
			}
		}, 'importFrmEquipStock', 'excelFileEquipStock', 'errExcelMsgEquipStock');
		
	},
	/**
	 * Tai tap tin Excel Import Danh sach kho thiet bi
	 * @author VuongMQ
	 * @date Mar 16,215
	 * @return Excel
	 * */
	/*downloadTemplateImportListEquipStock: function() {
		//alert('Tải file mẫu nè');
		$('.SuccessMsgStyle').html('').hide();
		$('.ErrorMsgStyle').html('').hide();
		var msg = 'Bạn có muốn tải tập tin Import?';
		ReportUtils.exportReport('/equipment-manage/downloadTemplateImporEquipment', {}, 'errMsg');
		
	},*/
	/**
	 *  view popup unit tree cho kho thiet bi
	 * @author VuongMQ
	 * @date Mar 16,215
	 * @return Excel
	 * */
	viewPopupUnit: function(type) {
		//EquipmentStockChange.searchUnit();
		// $('#popup-unit-tree .dialogUnitTreeContent').show();
		// $('#popup-unit-tree .ErrorMsgStyle').html('').hide();
		// $('#popup-unit-tree .SuccessMsgStyle').html('').hide();
		// $('#popup-unit-tree').dialog({
		// 	title : 'Tìm kiếm đơn vị',
		// 	width : 650,
		// 	height : 'auto',
		// 	closed : false,
		// 	cache : false,
		// 	modal : true,
		// 	onOpen : function() {
				//$('.easyui-dialog .InputTextStyle').val('');
				// $('#popup-unit-tree .InputTextStyle').val('');
				// $("#popup-unit-tree").addClass("easyui-dialog");
				// $("#popup-unit-tree #codeDlg").focus();
				
				// $("#popup-unit-tree #gridDlg").treegrid({
				// 	url: "/equipment-manage/search-shop-dialog?shopId=0",
				// 	rownumbers: false,
				// 	width: $("#popup-unit-tree #gridDlgDiv").width(),
				// 	height: 320,
				// 	fitColumns: true,
				// 	idField: "nodeId",
				// 	treeField: "text",
				// 	selectOnCheck: false,
				// 	checkOnSelect: false,
				// 	columns: [[
				// 		{field:"text", title:"Mã đơn vị", sortable: false, resizable: false, width:120, align:"left", formatter:CommonFormatter.formatNormalCell},
				// 		/*{field:"quantity", title:"Số suất", sortable: false, resizable: false, width:130, fixed:true, align:"left", formatter: function(v, r) {
				// 			if (r.attr.isNPP) {
				// 				if (r.attr.isExists) {
				// 					return "<input type='text' id='txt-p"+r.attr.id+"' class='qttClass' maxlength='9' disabled='disabled' exists='1' />";
				// 				}
				// 				return "<input type='text' id='txt-p"+r.attr.id+"' class='qttClass' maxlength='9' exists='0' />";
				// 			}
				// 			return "";
				// 		}},*/
				// 		{field:"ck", title:"", sortable: false, resizable: false, width:60, fixed:true, align:"center", formatter: function(v, r) {
				// 			var pId = r.attr.parentId;
				// 			if (pId == undefined || pId == null) {
				// 				pId = 0;
				// 			}
				// 			return '<a href="javascript:void(0)" onclick="return EquipmentStockChange.selectShopCode(\''+type+'\',\''+r.attr.shopCode+'\');">Chọn</a>';
				// 		}}
				// 	]],
				// 	onLoadSuccess: function(data) {
				// 		$("#popup-unit-tree #gridDlg").datagrid("resize");
				// 		Utils.bindFormatOnTextfieldInputCss('qttClass', Utils._TF_NUMBER, "#popup-unit-tree");
				// 		$('#popup-unit-tree .qttClass').each(function() {
				//       		//_formatCurrencyFor($(this).attr('id'));
				//       	});
				// 		setTimeout(function(){
				// 			var hDlg=parseInt($("#popup-unit-tree").parent().height());
				// 			var hW=parseInt($(window).height());
				// 			var d=hW-hDlg;
				// 			d=d/2+document.documentElement.scrollTop;
				// 			if (d < 0) { d = 0; }
				// 			$("#popup-unit-tree").parent().css('top',d);
			 //    		},1000);
				// 	}
				// });
				
				
				// $("#popup-unit-tree #btnSearchDlg").click(function() {
				// 	var p = {
				// 			code: $("#popup-unit-tree #codeDlg").val().trim(),
				// 			name: $("#popup-unit-tree #nameDlg").val().trim()
				// 	};
				// 	$("#popup-unit-tree #gridDlg").treegrid("reload", p);
				// });
				// $("#popup-unit-tree #codeDlg").parent().keyup(function(event) {
				// 	if (event.keyCode == keyCodes.ENTER) {
				// 		$("#popup-unit-tree #btnSearchDlg").click();
				// 	}
				// });
		// 	},
		// 	onClose : function() {
		// 		// $("#popup-unit-tree #gridDlg").treegrid('options').queryParams = null;
		// 		//$('#popup-unit-tree').html(html);
		// 		//$('#popup-unit-tree').hide();
		// 	}
		// });
				var txt = "shopCodeDl";
				$('#common-dialog-search-tree-in-grid-choose-single').dialog({
					title: "Tìm kiếm Đơn vị",
					closed: false,
			        cache: false, 
			        modal: true,
			        width: 680,
			        height: 550,
			        onOpen: function(){
			        	$('#txtShopCodeDlg').val("");
			        	$('#txtShopNameDlg').val("");
			        	//Tao cay don vi
			        	$('#commonDialogTreeShopG').treegrid({  
			        		data: StockManager._arrRreeShopTocken,
			        		url : '/cms/searchTreeShop',
			    		    width: 650,
			    		    height: 325,
			    		    queryParams: {code: $('#txtShopCodeDlg').val().trim(), name: $('#txtShopNameDlg').val().trim()},
			    			fitColumns : true,
			    			checkOnSelect: false,
			    			selectOnCheck: false,
			    		    rownumbers : true,
			    	        idField: 'id',
			    	        treeField: 'code',
			    		    columns:[[
			    		        {field:'code',title:work_date_unit_code,resizable:false, width:200, align:'left', formatter:function(value, row, index){
			    		        	return Utils.XSSEncode(value);
			    		        }},
			    		        {field:'name',title:work_date_unit_name,resizable:false, width:200, align:'left', formatter:function(value, row, index){
			    		        	return Utils.XSSEncode(value);
			    		        }},
			    		        {field:'choose',title:'',resizable:false, width:60, align:'center', fixed:true, formatter:function(value, row, index){
			    		        	return '<a href="javascript:void(0)" onclick="WorkingDatePlan.fillCodeForInputTextByDialogSearch2(\''+Utils.XSSEncode(txt)+'\', ' + row.id +', \''+Utils.XSSEncode(row.code)+'\', \''+Utils.XSSEncode(row.name)+'\' ,\''+Utils.XSSEncode(Utils._isShowInputTextByDlSearch2.code)+'\');">'+work_date_chon+'</a>';   
			    		        }}
			    		    ]],
			    	        onLoadSuccess :function(data){
			    				$('.datagrid-header-rownumber').html('STT');
			    				Utils.updateRownumWidthAndHeightForDataGrid('commonDialogTreeShopG');
			    	        }
			    		});
			        	$('#txtShopCodeDlg').focus();
			        },
			        onClose:function() {
			        	
			        }
				});
	},
	selectShopCode: function(type, code){
		if(type == EquipmentStockChange._FormSearch){
			$('#shopCode').val(code);
		} else{
			$('#shopCodeDl').val(code);
		}
		$('#popup-unit-tree').dialog('close');
		return false;
	},
	/**
	 *  view dialog Them moi kho thiet bi
	 * @author VuongMQ
	 * @date Mar 16,215
	 * */
	dialogAddOrUpdateEquipmentStock: function(id, index){
		$(".ErrorMsgStyle").html('').hide();
		$(".SuccessMsgStyle").html('').hide();
		//$('#gridSearchBillContGrid').html('<table id="gridSearchBill"></table>').change();
		var titleView = 'Thêm mới kho';
		if(id != undefined && id != null && index != undefined && index != null){
			titleView = 'Chỉnh sửa kho';
		}
		$('#dialogEquipStockChange').dialog({
			title: titleView,
			closed: false,
			cache: false,
			modal: true,
	        width: 670,
	        //height: 'auto',
	        height: 250,
	        onOpen: function(){
	        	$('.easyui-dialog .InputTextStyle').val('');
	    		/**shopCodeDl don vị*/
	    		/**combobox status*/
	    		$('#dialogEquipStockChange .MySelectBoxClass').css('width', '173');
	    		$('#dialogEquipStockChange .CustomStyleSelectBox').css('width', '140');
	    		/** truong hop chinh sua*/
	    		if(id != undefined && id != null && index != undefined && index != null){
	    			$('#idStock').val(id);
	    			var data = $('#listEquipmentDg').datagrid('getRows')[index];
	    			$('#shopCodeDl').attr('disabled', 'disabled');
	    			$('#txtCodeDl').attr('disabled', 'disabled');
	    			if(data.shop != undefined && data.shop != null){
	    				$('#shopCodeDl').val(data.shop.shopCode);	
	    			}
	    			$('#txtCodeDl').val(data.code);
					$('#txtNameDl').val(data.name);
					$('#txtDescriptionDl').val(data.description);
					$('#txtOrdinalDl').val(data.ordinal);
					if(data.status != null && activeType.parseValue(data.status) == activeType.RUNNING){
		    			$('#statusDl').val(activeType.RUNNING).change();
			    	} else if(data.status != null && activeType.parseValue(data.status) == activeType.STOPPED){
			    		$('#statusDl').val(activeType.STOPPED).change();
			    	}
			    	$('#txtNameDl').focus();
	    		} else {
	    			/** truong hop them moi*/
	    			$('#shopCodeDl').removeAttr('disabled');
	    			$('#txtCodeDl').removeAttr('disabled');
	    			$('#idStock').val('');
	    			$('#statusDl').val(activeType.RUNNING).change();
	    			$('#shopCodeDl').focus();
	    		}
	    		
	    		
	    		$('#dialogEquipStockChange #btnSaveEquipStock').unbind('click');
	    		$('#dialogEquipStockChange #btnSaveEquipStock').bind('click',function(event) {
	    			EquipmentStockChange.addOrUpdateEquipmentStock(id);
				});
				$('#shopCodeDl').bind('keyup', function(event) {
					 if(event.keyCode == keyCode_F9){
						 EquipmentStockChange.viewPopupUnit(EquipmentStockChange._FormAdd);
					}
				});
	        },
	        onClose:function() {
	        	
	        }
		});
	},
	/**
	 *  Them moi kho thiet bi
	 * @author VuongMQ
	 * @date Mar 16,215
	 * */
	addOrUpdateEquipmentStock: function(id){
		$(".ErrorMsgStyle").html('').hide();
		$(".SuccessMsgStyle").html('').hide();
		var msg ="";
		if(msg.length == 0){
			msg = Utils.getMessageOfRequireCheck('shopCodeDl', 'Mã đơn vị');
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfRequireCheck('txtCodeDl', 'Mã kho');
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfSpecialCharactersValidate('txtCodeDl', 'Mã kho', Utils._CODE);
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfRequireCheck('txtNameDl', 'Tên kho');
		}
		if(msg.length == 0){
			if($('#statusDl').val() == undefined || $('#statusDl').val() == null){
				msg = 'Trạng thái không được để trống';
			}
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfRequireCheck('txtOrdinalDl', 'Thứ tự');
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfInvaildInteger('txtOrdinalDl', 'Thứ tự');
			if(msg.length == 0){
				if($('#txtOrdinalDl').val() != undefined &&  $('#txtOrdinalDl').val() != null && $('#txtOrdinalDl').val() <=0){
					msg = 'Giá trị thứ tự phải lớn hơn 0';
				}
			}
		}

		if(msg.length >0){
			$('#errorMsgDl').html(msg).show();
			return false;
		}
		var params = new Object();	
		if(id != undefined && id != null){
			params.id = id;
		}
		params.shopCode = $('#shopCodeDl').val().trim();
		params.code = $('#txtCodeDl').val().trim();
		params.name = $('#txtNameDl').val().trim();
		params.status = $('#statusDl').val().trim();
		params.description = $('#txtDescriptionDl').val().trim();
		params.ordinal = $('#txtOrdinalDl').val().trim();
		var msgConfirm = 'Bạn có muốn thêm mới kho thiết bị?';
		Utils.addOrSaveData(params, '/equipment-manage/saveOrUpdateStock', null,  'errorMsgDl', function(data) {
			$('#successMsgDl').html("Lưu dữ liệu thành công").show();
			var tm = setTimeout(function(){
				//Load lai danh sach kho thiet bi
				$('#dialogEquipStockChange').dialog('close');
				//$('#btnSearch').click();
				$('#listEquipmentDg').datagrid('reload');
				$('.SuccessMsgStyle').html("").hide();
				clearTimeout(tm);
			 }, 1500);
		}, null, null, null,msgConfirm);
		return false;
	},

	/**
	 *  view dialog chinh sua kho thiet bi
	 * @author VuongMQ
	 * @date Mar 16,215
	 * */
	dialogChangeEquipmentStock: function(id, index){

	},
	/**
	 *  chinh sua kho thiet bi
	 * @author VuongMQ
	 * @date Mar 16,215
	 * */
	changeEquipmentStock: function(){

	},
	/*** END VUONGMQ * @date Mar 16,215 QUAN LY KHO THIET BI */
};

/*
 * END OF FILE - /web/web/resources/scripts/business/equipment/vnm.equipment-stock-change.js
 */

/*
 * START OF FILE - /web/web/resources/scripts/business/equipment/vnm.equipment-management-proposal-borrow.js
 */
/**
 * JS thuc hien nghiep vu Quan Ly De Nghi Muon Thiet Bi
 *
 * @author hoanv25
 * @since  March 09,2015
 * */
var EquipmentProposalBorrow = {
	_mapRecored: null,
	_editIndex: null,
	_lstEquipCategory: null,
	_lstHealthStatus: null,
	_lstProvince: null,
	_mapDelivery: null,
	/**
	 * Tim kiem danh sach de nghi muon tu
	 *
	 * @author hunglm16
	 * @since December 06, 2015
	 *
	 * @author update nhutnn
	 * @since update 02/07/2015
	 * */
	searchProposalBorrow: function() {
		$('.ErrorMsgStyle').html("").hide();
		var msg = '';
		var params = new Object();
		var multiShop = $("#shop").data("kendoMultiSelect");
		var dataShop = multiShop.dataItems();
		if (dataShop != null && dataShop.length > 0) {
			var lstShop = new Array();
			//var curShopCode = $('#curShopCode').val();
			for (var i = 0; i < dataShop.length; i++) {
				//if (dataShop[i].shopCode != curShopCode) {
					lstShop.push(dataShop[i].shopCode);
				//}
			}
			params.lstShop = lstShop.join(",");
		} else {
			msg = "Bạn chưa chọn Đơn vị!";
			err = '#shop';
		}
		if (msg.length == 0) {
			msg = Utils.getMessageOfSpecialCharactersValidate('code', 'Mã biên bản', Utils._CODE);
			err = "#code";
		}
		if (msg.length == 0) {
			msg = Utils.getMessageOfInvalidFormatDate("fDate","Từ ngày");
			err = "#fDate";
		}
		if (msg.length == 0) {
			msg = Utils.getMessageOfInvalidFormatDate("tDate","Đến ngày");
			err = "#tDate";
		}
		var fDate = $('#fDate').val().trim();
		var tDate = $('#tDate').val().trim();
		if (msg.length == 0 && fDate.length > 0 && tDate.length > 0 && !Utils.compareDate(fDate, tDate)) {
			msg = 'Từ ngày phải là ngày trước hoặc cùng ngày với đến ngày';
			err = '#fDate';
		}
		if (msg.length > 0) {
			$('#errLendMsg').html(msg).show();
			$(err).focus();
			return false;
		}
		params.code = $('#code').val().trim();
		params.superviseCode = $('#superviseCode').val().trim();
		params.statusRecord = $('#status').val().trim();
		params.flagRecordStatus = $('#statusApproved_flag').val();
		params.fromDate = $('#fDate').val().trim();
		params.toDate = $('#tDate').val().trim();
		params.statusDelivery = $('#statusDelivery').val().trim();
		EquipmentProposalBorrow._mapRecored = new Map();
		$('#gridLendEquip').datagrid("uncheckAll");
		$('#gridLendEquip').datagrid("load", params);
		return false;
	},
	/**
	 * Luu thay doi trang thai bien ban
	 * @author hoanv25
	 * @since March 10,2015
	 * 
	 * @author update nhutnn
	 * @since 03/07/2015
	 */
	saveRecordChange: function() {
		$('.ErrorMsgStyle').html("").hide();
		var rows = $('#gridLendEquip').datagrid('getRows');
		if (rows.length == 0) {
			$('#errLendMsg').html('Không có dữ liệu để lưu').show();
			return false;
		}
		if (EquipmentProposalBorrow._mapRecored.keyArray.length == 0) {
			$('#errLendMsg').html('Bạn chưa chọn biên bản. Vui lòng chọn biên bản!').show();
			return false;
		}
		var statusRecordChange = $('#statusRecordLabel').val();
		if (statusRecordChange == undefined || statusRecordChange == null || statusRecordChange == "") {
			$('#errLendMsg').html('Bạn chưa chọn trạng thái để lưu biên bản!').show();
			return false;
		}

		var lstIdRecord = new Array();
		for (var i = 0; i < EquipmentProposalBorrow._mapRecored.keyArray.length; i++) {
			lstIdRecord.push(EquipmentProposalBorrow._mapRecored.keyArray[i]);
		}
		var params = new Object();
		params.lstIdRecord = lstIdRecord;
		params.statusRecord = statusRecordChange;
		Utils.addOrSaveData(params, '/equipment-management-proposal-borrow/save-record-change', null, 'errLendMsg', function(data) {
			if (data.error) {
				$('#errLendMsg').html(data.errMsg).show();
				return;
			}
			if (data.lstRecordError != null && data.lstRecordError.length > 0) {
				VCommonJS.showDialogList({
					dialogInfo: {
						title: 'Lỗi'
					},
					data: data.lstRecordError,
					columns: [
						[{field: 'formCode', title: 'Mã biên bản', align: 'left', width: 150, sortable: false, resizable: false}, 
						{field: 'formStatusErr', title: 'Trạng thái biên bản', align: 'center', width: 80, sortable: false, resizable: false, formatter: function(value, row, index) {
							var html = "";
							if (row.formStatusErr == 1) {
								html = '<img src="/resources/images/icon_delete.png" height="20" width="20" style="margin-right:5px;" title="Error">';
							}
							return html;
						}},
						/*{field:'periodError', title:'Kỳ của biên bản', align:'center', width:80, sortable:false, resizable:false,formatter: function(value, row, index){
							var html="";
							if(row.periodError == 1){
								html = '<img src="/resources/images/icon_delete.png" height="20" width="20" style="margin-right:5px;" title="Error">';
							}
							return html;
							
						}}*/
						]
					]
				});
			} else {
				$('#successMsgLend').html('Lưu dữ liệu thành công').show();
				setTimeout(function() {
					$('#successMsgLend').html('').hide();
				}, 3000);
			}
			EquipmentProposalBorrow._mapRecored = new Map();
			$('#gridLendEquip').datagrid('reload');
			$('#gridLendEquip').datagrid('uncheckAll');
		}, null, null, null, "Bạn có muốn cập nhật ?", null);
	},
	/**
	 * Xuat file excel
	 * @author nhutnn
	 * @since 03/07/2015
	 */
	exportEquipLend: function() {
		$('.ErrorMsgStyle').html("").hide();
		if (EquipmentProposalBorrow._mapRecored == null || EquipmentProposalBorrow._mapRecored.keyArray.length == 0) {
			$('#errLendMsg').html('Bạn chưa chọn biên bản. Vui lòng chọn biên bản!').show();
			return false;
		}

		var lstIdRecord = new Array();
		for (var i = 0; i < EquipmentProposalBorrow._mapRecored.keyArray.length; i++) {
			lstIdRecord.push(EquipmentProposalBorrow._mapRecored.keyArray[i]);
		}
		var params = new Object();
		params.lstIdRecord = lstIdRecord;		
		ReportUtils.exportReport('/equipment-management-proposal-borrow/export-excel', params, "errLendMsg");
		$('#gridLendEquip').datagrid('uncheckAll');
		EquipmentProposalBorrow._mapRecored = new Map();
	},
	/**
	 * Xuat template import excel
	 * @author nhutnn
	 * @since 03/07/2015
	 */
	downloadTemplate: function() {
		var params = new Object();
		ReportUtils.exportReport('/equipment-management-proposal-borrow/download-template', params, 'errLendMsg');
	},
	/**
	 * Show popup import
	 * @author nhutnn
	 * @since 03/07/2015
	 */
	showDlgImportExcel: function(){
		$('#easyuiPopupImportExcel').show();
		$('.ErrorMsgStyle').hide();
		$('#easyuiPopupImportExcel').dialog({  
	        closed: false,  
	        cache: false,  
	        modal: true,
	        height:'auto',
	        onOpen: function(){	 
	        	$('.easyui-dialog .BtnGeneralStyle').css('color','');
	        },
	        onClose: function(){
	        	EquipmentProposalBorrow.searchProposalBorrow();
				$("#gridLendEquip").datagrid('uncheckAll');
				$("#fakefilepcBBDNM").val("");
				$("#excelFileBBDNM").val("");				
	        }
		});
	},
	/**
	 * import excel BBGN
	 * @author nhutnn
	 * @since 06/07/2015
	 */
	importExcelBBDNM: function() {
		$('.ErrorMsgStyle').hide();
		VTUtilJS.importExcelUtils(function(data) {
			$("#errExcelMsgBBDNM").html(data.message).show();
			if ($("#errExcelMsgBBDNM span").length > 0 && (data.fileNameFail == null || data.fileNameFail == "")) {
				setTimeout(function() {
					$('#errExcelMsgBBDNM').html("").hide();
					$('#easyuiPopupImportExcel').dialog('close');
				}, 3000);
			} else {
				$("#fakefilepcBBDNM").val("");
				$("#excelFileBBDNM").val("");
				//$('#gridRecordDelivery').datagrid('reload');	
			}
		}, "importFrmBBDNM", "excelFileBBDNM", null, "errExcelMsgBBDNM");
		return false;
	},
	/**
	 * Lay thong tin thiet bi
	 * @author nhutnn
	 * @since 07/07/2015
	 */	
	getLstInfo: function() {
		Utils.getJSONDataByAjax({},'/equipment-management-proposal-borrow/get-list-info',function(result) {
			if (result.lstEquipCategory != undefined && result.lstEquipCategory != null && result.lstEquipCategory.length > 0) {				
				for (var i = 0, sz = result.lstEquipCategory.length; i < sz; i++) {
					var obj = result.lstEquipCategory[i];
					obj.searchText = unicodeToEnglish(obj.code + " " + obj.name);
					obj.searchText = obj.searchText.toUpperCase();
				}	
				EquipmentProposalBorrow._lstEquipCategory = result.lstEquipCategory;
			}
			if (result.lstHealthy != undefined && result.lstHealthy != null && result.lstHealthy.length > 0) {
				for (var i = 0, sz = result.lstHealthy.length; i < sz; i++) {
					var obj = result.lstHealthy[i];
					obj.searchText = unicodeToEnglish(obj.apParamCode + " " + obj.apParamName);
					obj.searchText = obj.searchText.toUpperCase();
				}	
				EquipmentProposalBorrow._lstHealthStatus = result.lstHealthy;
			}
			if (result.lstProvince != undefined && result.lstProvince != null && result.lstProvince.length > 0) {
				for (var i = 0, sz = result.lstProvince.length; i < sz; i++) {
					var obj = result.lstProvince[i];
					obj.searchText = unicodeToEnglish(obj.areaCode + obj.areaName);
					obj.searchText = obj.searchText.toUpperCase();
				}
				EquipmentProposalBorrow._lstProvince = result.lstProvince;
			}
		}, null, null);
	},
	/**
	 * Them khach hang vao dong chi tiet
	 * @author nhutnn
	 * @since 07/07/2015
	 */
	//addCustomerInfo: function(customerCode, customerName, phone, mobiphone, street, housenumber, wardCode, districtCode, provinceCode, amount){
	addCustomerInfo: function(customerCode, customerName, phone, mobiphone, street, housenumber, wardCode, districtCode, provinceCode) {
		$('#divOverlay').show();		
		// var rows = $("#gridEquipLendChange").datagrid("getRows");	
		if(EquipmentProposalBorrow._editIndex != null ){
			var row = EquipmentProposalBorrow.getDetailEdit();
			row.customerCode = customerCode;
			row.customerName = customerName;
			if(phone != "" && mobiphone != "") {
				row.phone = phone +" / "+ mobiphone;	
			} else if(phone != "") {
				row.phone = phone;
			} else if(mobiphone != ""){
				row.phone = mobiphone;
			} else {
				row.phone = "";
			}
			row.street = street;
			row.address = housenumber;
			row.wardCode = wardCode;
			row.districtCode = districtCode;
			row.provinceCode = provinceCode;

			//row.amount = Math.floor(amount);
			// them dong moi			
			$('#gridEquipLendChange').datagrid("updateRow", {
				index: EquipmentProposalBorrow._editIndex,
				row: row
			});
			$('#gridEquipLendChange').datagrid('beginEdit', EquipmentProposalBorrow._editIndex);			
			EquipmentProposalBorrow.addEditorInGridChangeLend();
		}
		$('#divOverlay').hide();
		$('.easyui-dialog').dialog('close');
	},
	/**
	 * Xoa dong chi tiet
	 * @author nhutnn
	 * @since 07/07/2015
	 */
	deleteRowEquipLendDetail: function(index){		
		$.messager.confirm('Xác nhận', "Bạn có muốn xóa?", function(r){
			if(r){
				if(EquipmentProposalBorrow._editIndex != null){
					if(EquipmentProposalBorrow._editIndex == index){		
						EquipmentProposalBorrow._editIndex = null;
					} else if (EquipmentProposalBorrow._editIndex > index){
						// them dong moi
						var row = EquipmentProposalBorrow.getDetailEdit();
						$('#gridEquipLendChange').datagrid("updateRow", {
							index: EquipmentProposalBorrow._editIndex,
							row: row
						});
						
						EquipmentProposalBorrow._editIndex--;
					}
				}
				// xoa
				$('#gridEquipLendChange').datagrid('deleteRow', index);
				var rows = $("#gridEquipLendChange").datagrid("getRows");	
				$("#gridEquipLendChange").datagrid("loadData",rows);

				if(EquipmentProposalBorrow._editIndex != null){
					$('#gridEquipLendChange').datagrid('beginEdit', EquipmentProposalBorrow._editIndex);
					EquipmentProposalBorrow.addEditorInGridChangeLend();
				}
			}
		});
	},
	/**
	 * Them editor tren dong them moi
	 * @author nhutnn
	 * @since 08/07/2015
	 */
	addEditorInGridChangeLend: function() {
		var ed = $('#gridEquipLendChange').datagrid('getEditors', EquipmentProposalBorrow._editIndex);
		var row = $('#gridEquipLendChange').datagrid('getRows')[EquipmentProposalBorrow._editIndex];
    	
    	// Loai thiet bi
    	$(ed[0].target).combobox("loadData",EquipmentProposalBorrow._lstEquipCategory);			
		if(row.equipCategoryCode != undefined && row.equipCategoryCode != null && row.equipCategoryCode != "" ){
			$(ed[0].target).combobox("select", row.equipCategoryCode);
		}

		var getCustomer = function(){
			$(".ErrorMsgStyle").html('').hide();
			if($(ed[3].target).text('getValue').val() != null && $(ed[3].target).text('getValue').val() != ""){
				if($(ed[4].target).text('getValue').val() != null && $(ed[4].target).text('getValue').val() != ""){
					var params = new Object();
					params.shopCode = $(ed[3].target).text('getValue').val().trim();
					params.isAmountEquip = true;
					params.page = 1;
					params.max = 10;
					params.code = $(ed[4].target).text('getValue').val().trim();

					Utils.getJSONDataByAjax(params, '/commons/search-Customer-Show-List',function(result) {
						if(result.rows != null && result.rows.length > 0){
							var row = result.rows[0];
							var phone = row.phone == null ? "" : row.phone;
				        	var mobiphone = row.mobiphone == null ? "" : row.mobiphone;
				        	var street = row.street == null ? "" : row.street;
				        	var housenumber = row.housenumber == null ? "" : row.housenumber;
				        	var wardCode = row.wardCode == null ? "" : row.wardCode;
							var districtCode = row.districtCode == null ? "" : row.districtCode;
							var provinceCode = row.provinceCode == null ? "" : row.provinceCode;
							
				        	//EquipmentProposalBorrow.addCustomerInfo(row.shortCode, row.customerName, phone, mobiphone, street, housenumber, wardCode, districtCode, provinceCode, row.amountEquip);
				        	EquipmentProposalBorrow.addCustomerInfo(row.shortCode, row.customerName, phone, mobiphone, street, housenumber, wardCode, districtCode, provinceCode);
						}else {							
				        	//EquipmentProposalBorrow.addCustomerInfo("", "", "", "", "", "", "", "", "", "");
				        	EquipmentProposalBorrow.addCustomerInfo("", "", "", "", "", "", "", "", "");
				        	$('#errMsgChangeLend').html("Không tìm thấy mã khách hàng trong hệ thống!").show();								
							return false;
						}
						$(ed[4].target).focus();
					});
				}
			} else {
				$('#errMsgChangeLend').html("Bạn chưa nhập mã đơn vị!").show();
				$(ed[3].target).focus();
				return false;
			}
		};
    	// Ma khach hang
		$(ed[4].target).bind('keyup', function(event) {					
			if (event.keyCode == keyCodes.F9) {
				$(".ErrorMsgStyle").html('').hide();
				if($(ed[3].target).text('getValue').val() != null && $(ed[3].target).text('getValue').val() != ""){
					VCommonJS.showDialogSearch2({
						params : {
							shopCode: $(ed[3].target).text('getValue').val().trim(),
							isAmountEquip: true
						},
					    inputs : [
					        {id:'code', maxlength:50, label:'Mã KH'},
					        {id:'name', maxlength:250, label:'Tên KH'},
					        {id:'address', maxlength:250, label:'Địa chỉ', width:410}
					    ],			   
					    url : '/commons/search-Customer-Show-List',			   
					    columns : [[
					        {field:'shortCode', title:'Mã KH', align:'left', width: 110, sortable:false, resizable:false},
					        {field:'customerName', title:'Tên KH', align:'left', width: 150, sortable:false, resizable:false},
					        {field:'address', title:'Địa chỉ', align:'left', width: 170, sortable:false, resizable:false},
					        {field:'choose', title:'', align:'center', width:40, sortable:false, resizable:false, formatter:function(value,row,index) {
					        	var phone = row.phone == null ? "" : row.phone;
					        	var mobiphone = row.mobiphone == null ? "" : row.mobiphone;
					        	var street = row.street == null ? "" : row.street;
					        	var housenumber = row.housenumber == null ? "" : row.housenumber;
					        	var wardCode = row.wardCode == null ? "" : row.wardCode;
								var districtCode = row.districtCode == null ? "" : row.districtCode;
								var provinceCode = row.provinceCode == null ? "" : row.provinceCode;
								
					        	//return "<a href='javascript:void(0)' onclick=\"return EquipmentProposalBorrow.addCustomerInfo('"+row.shortCode+"','"+row.customerName+"','"+phone+"','"+mobiphone+"','"+street+"','"+housenumber+"','"+wardCode+"','"+districtCode+"','"+provinceCode+"','"+row.amountEquip+"');\">Chọn</a>";        
					        	return "<a href='javascript:void(0)' onclick=\"return EquipmentProposalBorrow.addCustomerInfo('"+row.shortCode+"','"+row.customerName+"','"+phone+"','"+mobiphone+"','"+street+"','"+housenumber+"','"+wardCode+"','"+districtCode+"','"+provinceCode+"');\">Chọn</a>";        
					        }}
					    ]]
					});
					$(".easyui-dialog #code").focus();
				} else {
					$('#errMsgChangeLend').html("Bạn chưa nhập mã đơn vị!").show();
					$(ed[3].target).focus();
					return false;
				}
			} else if (event.keyCode == keyCodes.ENTER) {
				getCustomer();
			}
		});				

		$(ed[4].target).keydown(function (e) {
	        var keyCode = e.which || e.keyCode; // for cross-browser compatibility
	        if(keyCode == keyCodes.TAB){
				getCustomer();
			}
	    });

		// tinh, thanh pho
    	$(ed[8].target).combobox("loadData", EquipmentProposalBorrow._lstProvince);
		if(row.provinceCode != undefined && row.provinceCode != null && row.provinceCode != "" ){
			$(ed[8].target).combobox("select", row.provinceCode);
		}
		
		// tinh trang tu
    	$(ed[19].target).combobox("loadData",EquipmentProposalBorrow._lstHealthStatus);
    	if(row.healthStatusCode != undefined && row.healthStatusCode != null && row.healthStatusCode != "" ){
			$(ed[19].target).combobox("select", row.healthStatusCode);
		}
		if(row.stockTypeValue != undefined && row.stockTypeValue != null && row.stockTypeValue != "" ){
			$(ed[20].target).combobox("select", row.stockTypeValue);
		}

		// ngay lap CMND
		$($(ed[14].target).datebox('textbox')).attr('id', 'cmndDate' + EquipmentProposalBorrow._editIndex).attr("maxlength","10");
		Utils.bindFormatOnTextfield('cmndDate' + EquipmentProposalBorrow._editIndex, Utils._TF_NUMBER_CONVFACT);

		// ngay DKKD
		$($(ed[17].target).datebox('textbox')).attr('id', 'dkkdDate' + EquipmentProposalBorrow._editIndex).attr("maxlength","10");
		Utils.bindFormatOnTextfield('dkkdDate' + EquipmentProposalBorrow._editIndex, Utils._TF_NUMBER_CONVFACT);

		// thoi gian muon tu
		$($(ed[18].target).datebox('textbox')).attr('id', 'lendDate' + EquipmentProposalBorrow._editIndex).attr("maxlength","10");
		Utils.bindFormatOnTextfield('lendDate' + EquipmentProposalBorrow._editIndex, Utils._TF_NUMBER_CONVFACT);
	},
	/**
	 * Validate dong them moi
	 * @author nhutnn
	 * @since 08/07/2015
	 */
	validateEquipmentInGrid: function() {
		var currentDate = Utils.currentDate();
		var ed = $('#gridEquipLendChange').datagrid('getEditors', EquipmentProposalBorrow._editIndex);
		var msg = "";
		if (msg.length == 0 && ($(ed[0].target).combobox('getValue') == null || $(ed[0].target).combobox('getValue') == "")) {
			msg = "Bạn chưa chọn loại thiết bị!";
			$(ed[0].target).focus();
		}
		if (msg.length == 0 && ($(ed[1].target).text('getValue').val() == null || $(ed[1].target).text('getValue').val() == "")) {
			msg = "Bạn chưa nhập dung tích!";
			$(ed[1].target).focus();
		}
		if (msg.length == 0 && ($(ed[2].target).numberbox('getValue') == null || $(ed[2].target).numberbox('getValue') == "")) {
			msg ="Bạn chưa nhập số lượng!";
			$(ed[2].target).focus();
		}
		if (msg.length == 0 && ($(ed[2].target).numberbox('getValue') <= 0)) {
			msg = "Bạn phải nhập số lượng là số nguyên dương!";
			$(ed[2].target).focus();
		}
		if (msg.length == 0 && ($(ed[3].target).text('getValue').val() == null || $(ed[3].target).text('getValue').val() == "")) {
			msg = "Bạn chưa nhập mã đơn vị!";
			$(ed[3].target).focus();
		}
		if (msg.length == 0 && ($(ed[4].target).text('getValue').val() == null || $(ed[4].target).text('getValue').val() == "")) {
			msg = "Bạn chưa nhập mã khách hàng!";
			$(ed[4].target).focus();
		}
		if (msg.length == 0 && ($(ed[5].target).text('getValue').val() == null || $(ed[5].target).text('getValue').val() == "")) {
			msg = "Bạn chưa nhập người đứng tên mượn thiết bị!";
			$(ed[5].target).focus();
		}
		if (msg.length == 0 && ($(ed[6].target).text('getValue').val() == null || $(ed[6].target).text('getValue').val() == "")) {
			msg = "Bạn chưa nhập quan hệ!";
			$(ed[6].target).focus();
		}
		if (msg.length == 0 && ($(ed[7].target).text('getValue').val() == null || $(ed[7].target).text('getValue').val() == "")) {
			msg = "Bạn chưa nhập số nhà!";
			$(ed[7].target).focus();
		}
		if (msg.length == 0 && ($(ed[8].target).combobox('getValue') == null || $(ed[8].target).combobox('getValue') == "")) {
			msg = "Bạn chưa nhập Tỉnh/Thành phố!";
			$(ed[8].target).focus();
		}
		if (msg.length == 0 && ($(ed[9].target).combobox('getValue') == null || $(ed[9].target).combobox('getValue') == "")) {
			msg = "Bạn chưa nhập quận/huyện!";
			$(ed[9].target).focus();
		}
		if (msg.length == 0 && ($(ed[10].target).combobox('getValue') == null || $(ed[10].target).combobox('getValue') == "")) {
			msg = "Bạn chưa nhập phường/xã!";
			$(ed[10].target).focus();
		}
		if (msg.length == 0 && ($(ed[11].target).text('getValue').val() == null || $(ed[11].target).text('getValue').val() == "")) {
			msg = "Bạn chưa nhập tên đường/phố/thôn!";
			$(ed[11].target).focus();
		}
		if (msg.length == 0 && ($(ed[12].target).text('getValue').val() == null || $(ed[12].target).text('getValue').val() == "")) {
			msg = "Bạn chưa nhập số CMND!";
			$(ed[12].target).focus();
		}
		if (msg.length == 0 && !/^[0-9]+$/.test($(ed[12].target).text('getValue').val())){
			msg = format(msgErr_invalid_format_num,"Số CMND");
			$(ed[12].target).focus();
		}
		if (msg.length == 0 && ($(ed[13].target).text('getValue').val() == null || $(ed[13].target).text('getValue').val() == "")) {
			msg = "Bạn chưa nhập nơi cấp CMND!";
			$(ed[13].target).focus();
		}
		if (msg.length == 0 && ($(ed[14].target).datebox('getValue') == null || $(ed[14].target).datebox('getValue') == "")) {
			msg = "Bạn chưa nhập ngày cấp CMND!";
			$(ed[14].target).focus();
		}		
		if (msg.length == 0 && $(ed[14].target).datebox('getValue').length > 0 && !Utils.isDate($(ed[14].target).datebox('getValue'), '/')) {
			msg = format(msgErr_invalid_format_date,"Ngày cấp CMND");
			$(ed[14].target).focus();
		}
		if (msg.length == 0 && $(ed[14].target).datebox('getValue').length > 0 && Utils.compareDateForTowDate($(ed[14].target).datebox('getValue'), currentDate) >= 0) {
			msg = 'Ngày cấp CMND phải là ngày trước ngày hiện tại';
			$(ed[14].target).focus();
		}
		// if (msg.length == 0 && $(ed[14].target).text('getValue').val() != "" && !/^[0-9]+$/.test($(ed[14].target).text('getValue').val())){
		// 	msg = format(msgErr_invalid_format_num,"Số ĐKKD");
		// 	$(ed[14].target).focus();
		// }
		if (msg.length == 0 && $(ed[17].target).datebox('getValue').length > 0 && !Utils.isDate($(ed[17].target).datebox('getValue'), '/')) {
			msg = format(msgErr_invalid_format_date,"Ngày cấp số ĐKKD");
			$(ed[17].target).focus();
		}
		if (msg.length == 0 && $(ed[17].target).datebox('getValue').length > 0 && Utils.compareDateForTowDate($(ed[17].target).datebox('getValue'), currentDate) >= 0) {
			msg = 'Ngày cấp số ĐKKD phải là ngày trước ngày hiện tại';
			$(ed[17].target).focus();
		}
		if (msg.length == 0 && ($(ed[18].target).datebox('getValue') == null || $(ed[18].target).datebox('getValue') == "")) {
			msg = "Bạn chưa nhập thời gian muốn nhận thiết bị!";
			$(ed[18].target).focus();
		}		
		if (msg.length == 0 && $(ed[18].target).datebox('getValue').length > 0 && !Utils.isDate($(ed[18].target).datebox('getValue'), '/')) {
			msg = format(msgErr_invalid_format_date,"Thời gian muốn nhận thiết bị");
			$(ed[18].target).focus();
		}		
		if (msg.length == 0 && $(ed[18].target).datebox('getValue').length > 0 && Utils.compareDateForTowDate($(ed[18].target).datebox('getValue'), currentDate) < 0) {
			msg = 'Thời gian muốn nhận thiết bị phải là ngày sau hoặc bằng ngày hiện tại';
			$(ed[18].target).focus();
		}
		if (msg.length == 0 && ($(ed[19].target).combobox('getValue') == null || $(ed[19].target).combobox('getValue') == "")) {
			msg = "Bạn chưa nhập tình trạng thiết bị!";
			$(ed[19].target).focus();
		}
		if (msg.length == 0 && ($(ed[20].target).combobox('getValue') == null || $(ed[20].target).combobox('getValue') == "")) {
			msg = "Bạn chưa nhập kho xuất!";
			$(ed[20].target).focus();
		}
		return msg;
	},
	/**
	 * Them dong chi tiet
	 * @author nhutnn
	 * @since 07/07/2015
	 */
	insertEquipmentInGrid: function() {
		$('.ErrorMsgStyle').html('').hide();
		if (EquipmentProposalBorrow._editIndex != null) {
			var msg = EquipmentProposalBorrow.validateEquipmentInGrid();
			if (msg.length > 0) {
				$('#errMsgChangeLend').html(msg).show();
				return false;
			}
			// them dong moi
			var row = EquipmentProposalBorrow.getDetailEdit();
			$('#gridEquipLendChange').datagrid("updateRow", {
				index: EquipmentProposalBorrow._editIndex,
				row: row
			});
		}

		var rowNew = {};
		if ($("#isLevelShop").val() == ShopDecentralizationSTT.NPP && $("#curShopCode").val() != null && $("#curShopCode").val() != "") {
			rowNew.shopCode = $("#curShopCode").val().trim();
		}
		$('#gridEquipLendChange').datagrid("appendRow", rowNew);
		var sz = $("#gridEquipLendChange").datagrid("getRows").length;
		EquipmentProposalBorrow._editIndex = sz - 1;
		$('#gridEquipLendChange').datagrid('beginEdit', EquipmentProposalBorrow._editIndex);
		EquipmentProposalBorrow.addEditorInGridChangeLend();
	},
	/**
	 * Lay thong dong chi tiet them
	 * @author nhutnn
	 * @since 08/07/2015
	 */
	getDetailEdit: function(){
		var ed = $('#gridEquipLendChange').datagrid('getEditors', EquipmentProposalBorrow._editIndex);
		var row = $('#gridEquipLendChange').datagrid("getRows")[EquipmentProposalBorrow._editIndex];
		row.equipCategory = $(ed[0].target).combobox("getText").trim();
		row.equipCategoryCode = $(ed[0].target).combobox("getValue");
		row.capacity = $(ed[1].target).text("getValue").val().trim();
		row.quantity = $(ed[2].target).numberbox("getValue").trim();

		row.shopCode = $(ed[3].target).text("getValue").val().trim();
		row.customerCode = $(ed[4].target).text("getValue").val().trim();
		row.representative = $(ed[5].target).text("getValue").val().trim();
		row.relation = $(ed[6].target).text("getValue").val().trim();

		row.address = $(ed[7].target).text("getValue").val().trim();
		row.province = $(ed[8].target).combobox("getText").trim();
		row.provinceCode = $(ed[8].target).combobox("getValue");

		row.district = $(ed[9].target).combobox("getText").trim();
		row.districtCode = $(ed[9].target).combobox("getValue");

		row.ward = $(ed[10].target).combobox("getText").trim();
		row.wardCode = $(ed[10].target).combobox("getValue");
		row.street = $(ed[11].target).text("getValue").val().trim();

		row.idNo = $(ed[12].target).text("getValue").val().trim();
		row.idNoPlace = $(ed[13].target).text("getValue").val().trim();
		row.idNoDate = $(ed[14].target).datebox("getValue").trim();

		row.businessNo = $(ed[15].target).text("getValue").val().trim();
		row.businessNoPlace = $(ed[16].target).text("getValue").val().trim();
		row.businessNoDate = $(ed[17].target).datebox("getValue").trim();

		row.timeLend = $(ed[18].target).datebox("getValue").trim();
		row.healthStatus = $(ed[19].target).combobox("getText").trim();
		row.healthStatusCode = $(ed[19].target).combobox("getValue");
		row.stockType = $(ed[20].target).combobox("getText").trim();
		row.stockTypeValue = $(ed[20].target).combobox("getValue");

		return row;
	},
	/**
	 * Chinh sua bien ban
	 * @author nhutnn
	 * @since 08/07/2015
	 */
	updateRecord: function(){
		$('.ErrorMsgStyle').html('').hide();
		var msg = "";
		var err = "";
		if (msg.length == 0 && ($("#status").val() == undefined || $("#status").val() == null || $("#status").val() == "")) {
			msg = 'Bạn chưa chọn trạng thái!';
			err = '#status';
		}
		if (msg.length == 0 && $('#staffCode').val() == 0) {
			msg = 'Bạn chưa chọn giám sát';
		}
		if (msg.length == 0 && $('#createFormDate').val().trim().length == 0) {
			msg = 'Bạn chưa nhập giá trị cho trường Ngày biên bản';
			err = '#createFormDate';
		}
		if (msg.length == 0 && $('#createFormDate').val().trim().length > 0 && !Utils.isDate($('#createFormDate').val(), '/')) {
			msg = 'Ngày biên bản không tồn tại hoặc không đúng định dạng dd/mm/yyyy.';
			err = '#createFormDate';
		}
		var currentDate = Utils.currentDate();
		if (msg.length == 0 && $('#createFormDate').val().length > 0 && Utils.compareDateForTowDate($('#createFormDate').val(), currentDate) > 0) {
			msg = 'Ngày biên bản phải là ngày trước hoặc cùng ngày với ngày hiện tại';
			err = '#createFormDate';
		}
		var rows = $('#gridEquipLendChange').datagrid("getRows");
		if(msg.length == 0 && rows.length == 0){
			msg = 'Bạn chưa thêm chi tiết biên bản';
		}
		if (msg.length == 0 && $('#note').attr('maxlength') > 500) {
			msg = 'Bạn nhập quá giới hạn của trường note';
		}
		if (msg.length == 0){
			if(EquipmentProposalBorrow._editIndex != null){
				msg = EquipmentProposalBorrow.validateEquipmentInGrid();
				if(msg.length > 0){
					$('#errMsgChangeLend').html(msg).show();
					return false;
				}				
				// them dong moi
				var row = EquipmentProposalBorrow.getDetailEdit();
				rows[EquipmentProposalBorrow._editIndex] = row;
				//$('#gridEquipLendChange').datagrid("updateRow", {index: EquipmentProposalBorrow._editIndex, row: row});
			}			
		}	

		if(msg.length > 0){
			$('#errMsgChangeLend').html(msg).show();
			$(err).focus();
			return false;
		}
		
		var params = new Object();
		params.id = $("#idEquipLend").val();
		params.lstDetails = rows;
		params.staffCode = $("#staffCode").val();
		params.statusRecord = $("#status").val();
		params.createFormDate = $("#createFormDate").val();
		params.note = $('#note').val();
		//params = JSONUtil.getSimpleObject(params);
		JSONUtil.saveData2(params, "/equipment-management-proposal-borrow/update", "Bạn có muốn cập nhật?", "errMsgChangeLend", function(data){
			if(data.error){
				$("errMsgChangeLend").html(data.errMsg).show();
			} else {
				$("#successMsgChangeLend").html("Lưu dữ liệu thành công").show();
				setTimeout(function(){						
					if(window.location.pathname == "/equipment-management-proposal-borrow/edit"){
						window.location.href = '/equipment-management-proposal-borrow/info';	
					} else if(window.location.pathname == "/equipment-management-proposal-borrow/change"){
						window.location.href = '/equipment-management-proposal-borrow/approve';	
					}					
				}, 3500);
			}
		},null,null);
	},
	/**
	 * Xuat bien ban hop dong giao nhan
	 * 
	 * @author update nhutnn
	 * @since 10/07/2015
	 */
	exportDeliveryRecord: function(id) {
		$('.ErrorMsgStyle').html("").hide();
		var rows = $('#gridLendEquip').datagrid('getRows');
		if (rows.length == 0) {
			$('#errLendMsg').html('Không có dữ liệu để xuất').show();
			return false;
		}

		if(id == null) {
			if (EquipmentProposalBorrow._mapRecored.keyArray.length == 0) {
				$('#errLendMsg').html('Bạn chưa chọn biên bản. Vui lòng chọn biên bản!').show();
				return false;
			}
		}	

		var lstIdRecord = new Array();
		if(id == null) {
			for (var i = 0; i < EquipmentProposalBorrow._mapRecored.keyArray.length; i++) {
				lstIdRecord.push(EquipmentProposalBorrow._mapRecored.keyArray[i]);
			}
		}else{
			lstIdRecord.push(id);
		}

		var params = new Object();
		params.lstIdRecord = lstIdRecord;
		Utils.addOrSaveData(params, '/equipment-management-proposal-borrow/export-delivery-record', null, 'errLendMsg', function(data) {
			if (data.error) {
				$('#errLendMsg').html(data.errMsg).show();
				return;
			}
			if (data.lstRecordError != null && data.lstRecordError.length > 0) {
				VCommonJS.showDialogList({
					title: 'LỖI',
					data: data.lstRecordError,
					columns: [[
						{field: 'formCode', title: 'Mã biên bản', align: 'left', width: 120, sortable: false, resizable: false}, 
						{field: 'formStatusErr', title: 'Biên bản không ở trạng thái duyệt', align: 'center', width: 60, sortable: false, resizable: false, formatter: function(value, row, index) {
							var html = "";
							if (row.formStatusErr == 1) {
								html = '<img src="/resources/images/icon_delete.png" height="20" width="20" style="margin-right:5px;" title="Error">';
							}
							return html;
						}},
						{field: 'deliveryStatusErr', title: 'Biên bản đã xuất hợp đồng giao nhận', align: 'center', width: 60, sortable: false, resizable: false, formatter: function(value, row, index) {
							var html = "";
							if (row.deliveryStatusErr == 1) {
								html = '<img src="/resources/images/icon_delete.png" height="20" width="20" style="margin-right:5px;" title="Error">';
							}
							return html;
						}}
					]]
				});
			} else {
				$('#successMsgLend').html('Xuất hợp đồng giao nhận thành công!').show();
				setTimeout(function() {
					$('#successMsgLend').html('').hide();
				}, 3000);
			}
			EquipmentProposalBorrow.searchProposalBorrow();
		}, null, null, null, "Bạn có muốn xuất biên bản hợp đồng giao nhận?", null);
	},
	/**
	 * Lay danh sach bien ban hop dong giao bi huy
	 *
	 * @author nhutnn
	 * @since 10/07/2015
	 */
	getDeliveryRecordAgain: function(id) {
		$('.ErrorMsgStyle').html("").hide();
		if(id == null) {
			$('#errLendMsg').html('Bạn chưa chọn biên bản. Vui lòng chọn biên bản!').show();
		}	

		var params = new Object();
		params.id = id;
		Utils.getJSONDataByAjax(params, '/equipment-management-proposal-borrow/get-list-delivery-export-again', function(data) {
			if (data.error) {
				$('#errLendMsg').html(data.errMsg).show();
				return;
			}else {
				if (data.total > 1) {
					EquipmentProposalBorrow._mapDelivery = new Map();  
					$("#idRecordPopup").val(id); 
					$('#easyuiPopupLstDelivery').show();
					$('#easyuiPopupLstDelivery').dialog({  
				        closed: false,  
				        cache: false,  
				        modal: true,
				        width : 350,	        
				        height : 'auto',
				        onOpen: function(){	      
							$('#gridDelivery').datagrid({
								data: data.rows,
								autoRowHeight : true,
								fitColumns:true,
								width: 310,
								height: "auto",
								scrollbarSize: 0,
								rownumbers:true,
								columns:[[
									{field: 'shopCode', title: 'Mã đơn vị', width: 80, sortable:false, resizable: false, align: 'left'},					
								    {field: 'customerCode', title: 'Mã khách hàng', width: 80, sortable:false, resizable: false, align: 'left'},					
									{field: 'recordCode', title: 'Mã biên bản giao nhận', width: 120, sortable: false, resizable: false,align: 'left'},		    
									{field: 'check',checkbox:true, width: 50, sortable:false,resizable:false, align: 'center'}
								]],
								onBeforeLoad: function(param){											 
								},
								onLoadSuccess: function(data){   			 
							   		$('.easyui-dialog .datagrid-header-rownumber').html('STT');	   	

								},onCheck:function(index,row){
									EquipmentProposalBorrow._mapDelivery.put(row.idRecord,row);   
							    },
							    onUncheck:function(index,row){
							    	EquipmentProposalBorrow._mapDelivery.remove(row.idRecord);
							    },
							    onCheckAll:function(rows){
							    	for(var i = 0;i<rows.length;i++){
							    		var row = rows[i];
							    		EquipmentProposalBorrow._mapDelivery.put(row.idRecord,row);
							    	}
							    },
							    onUncheckAll:function(rows){
							    	for(var i = 0;i<rows.length;i++){
							    		var row = rows[i];
							    		EquipmentProposalBorrow._mapDelivery.remove(row.idRecord);	
							    	}
							    }
							});
				        }, 
				        onClose: function(){
				        	EquipmentProposalBorrow._mapDelivery = null;
				        }
					});
				} else {
					EquipmentProposalBorrow.exportDeliveryRecordAgain(id, data.rows[0].customerId);
				}
			}
		}, null, null);
	},
	/**
	 * Xuat lai bien ban hop dong giao nhan lan nua
	 *
	 * @author nhutnn
	 * @since 10/07/2015
	 */
	exportDeliveryRecordAgain: function(id, customerId){
		$('.ErrorMsgStyle').html("").hide();
		if(customerId == null && id == null) {
			if (EquipmentProposalBorrow._mapDelivery.keyArray.length == 0) {
				$('#errMsgPopupLstDelivery').html('Bạn chưa chọn biên bản. Vui lòng chọn biên bản!').show();
				return false;
			}
		}	

		var lstCustomerId = new Array();
		if(customerId == null && id == null) {
			for (var i = 0; i < EquipmentProposalBorrow._mapDelivery.keyArray.length; i++) {
				var idDelivery = EquipmentProposalBorrow._mapDelivery.keyArray[i];
				lstCustomerId.push(EquipmentProposalBorrow._mapDelivery.get(idDelivery).customerId);
			}
		}else{
			lstCustomerId.push(customerId);
		}
		var errId = customerId == null?"errMsgPopupLstDelivery":"errLendMsg";		
		var params = new Object();
		if(id != null){ 
			params.id = id;
		} else {
			params.id = $("#idRecordPopup").val();
		}	
		params.lstCustomerId = lstCustomerId;
		Utils.addOrSaveData(params, '/equipment-management-proposal-borrow/export-delivery-export-again', null, errId, function(data) {
			if (data.error) {
				$('#'+errId).html(data.errMsg).show();
				return;
			}else {
				$('#successMsgLend').html('Xuất lại hợp đồng giao nhận thành công!').show();
				$("#idRecordPopup").val("");
				setTimeout(function() {
					$('#successMsgLend').html('').hide();					
				}, 3000);
				$('#easyuiPopupLstDelivery').window('close');
			}
			EquipmentProposalBorrow.searchProposalBorrow();
		}, null, null, null, "Bạn có muốn xuất lại biên bản hợp đồng giao nhận?", null);
	},
	/**
	 * Xuat lai bien ban hop dong giao nhan lan nua
	 *
	 * @author nhutnn
	 * @since 10/07/2015
	 */
	viewPrintEquipLend: function(id){
		$('.ErrorMsgStyle').html('').hide();
		
		var lstIdRecord = new Array();
		if(id != null){			
			lstIdRecord.push(id);
		}else{
			$('#errLendMsg').html('Vui lòng chọn biên bản đã duyệt để xem bản in!').show();
		}		
		
		var params = new Object();
		params.lstIdRecord = lstIdRecord;	
		ReportUtils.exportReport('/equipment-management-proposal-borrow/view-print', params);
		return false;
	},
	/**
	 * Quay về trang tìm kiếm
	 *
	 * @author nhutnn
	 * @since 27/07/2015
	 */
	returnSearchPage: function(){
		$.messager.confirm('Xác nhận', "Bạn có muốn quay lại trang tìm kiếm và không lưu dữ liệu thay đổi?", function(r){
			if (r) {
				if(window.location.pathname == "/equipment-management-proposal-borrow/edit"){
					window.location.href = '/equipment-management-proposal-borrow/info';	
				} else if(window.location.pathname == "/equipment-management-proposal-borrow/change"){
					window.location.href = '/equipment-management-proposal-borrow/approve';	
				}				
			}
		});
	},
};
/*
 * END OF FILE - /web/web/resources/scripts/business/equipment/vnm.equipment-management-proposal-borrow.js
 */

/*
 * START OF FILE - /web/web/resources/scripts/business/equipment/vnm.equipment-management-stock-permission.js
 */
/**
 * JS thuc hien nghiep vu Quan ly quyen kho thiet bi
 * 
 * @author hoanv25
 * @since  March 09, 2015
 * */
var EquipmentStockPermission = {
		
/**Khai bao thuoc tinh Bien Toan Cuc
 * 
 * @author hoanv25
 * @since March 12, 2015
 * */
 	_ADD: 'ADD',
 	_EDIT: 'EDIT',
 	_lstAddStock: null,
 	_lstEditStock: null,
	_lstEquipStockDel:null,
	_lstStockNow:null,
	_lstCheckParenId:null,
	_lstEquipStockGirdDel:null,
	_lstEquipStockInsert:null,
	_lstAddStockInsert:null,
	_lstEditStockInsert:null,	
	_checkListEdit:null,
	fromStockCodeGlobal:null,
	_mapStock:null,
	_mapIdStock:null,
	_mapIdEditStock:null,
	_mapIdEditRoleStock:null,
	_params: null, /**vuongmq; 25/03/2015; de dung xuat excel theo dk search*/
/**
 * Tim kiem danh sach quyen kho thiet bi
 * 
 * @author hoanv25
 * @since March 12, 2015
 * */
 searchStockPermission: function(){
 	var msg=''; 	
 	var code = $('#code').val().trim();
	var name = $('#name').val().trim();
	var status = $('#status').val().trim();
	/**vuongmq; 25/03/2015; de dung xuat excel theo dk search*/
	EquipmentStockPermission._params = new Object();
	EquipmentStockPermission._params.name = $('#name').val().trim();
	EquipmentStockPermission._params.code = $('#code').val().trim();
	EquipmentStockPermission._params.status = $('#status').val().trim();
	if(msg.length>0){
			$('#errMsg').html(msg).show();	
			setTimeout(function(){$('#errMsg').hide();},3000);
			return false;
		}
	var params =  new Object();
		params.code = code;
		params.name = name;		
		params.status = status;	
 	$('#gridStockPermission').datagrid("load", params);
 	$('#stockTransDetailTable').hide();		
 	return false;
 }, 
/**
 * Opend dialog them moi quyen kho thiet bi
 * 
 * @author hoanv25
 * @since March 12, 2015
 * */
 openDialogChangeAdd: function(){
		$(".ErrorMsgStyle").hide();
		$('#dialogParam').dialog({
			title: "Thêm mới quyền",
			closed: false,
			cache: false,
			modal: true,
	        width: 800,
	        height: 'auto',
	        onOpen: function(){
	        	$('.easyui-dialog .InputTextStyle').val('');
	    		$('.easyui-dialog #errorSearchMsg').hide();
	    		$('.easyui-dialog #statusAdd').val(1).change();
	    		$('#codeAdd').attr('disabled', false);	    
	    		$('#codeAdd').focus();
	    		/**combobox status*/
	    		$('#dialogParam .MySelectBoxClass').css('width', '173');
	    		$('#dialogParam .CustomStyleSelectBox').css('width', '140');
	    		$('#dialogParam #btnSaveAddStock').html('Cập nhật');
	    		var params = new Object();
				params.id = 0;
	    		$('#gridStock').datagrid({					
					url :"/equipment-manager-stock-permission/searchListStock",
					autoRowHeight : true,
					rownumbers : true, 
					checkOnSelect :true,		
					pagination:true,
					pageSize :10,
					pageList : [10],
					checkOnSelect :true,	
					scrollbarSize:0,
					fitColumns:true,			
					queryParams: params,		
					width: $('#gridContainerStock').width(),
					autoWidth: true,
					columns:[[
					    {field: 'shopCode',title:'Đơn vị',width: 100,sortable:false,resizable:false, align: 'left' },	
					    {field: 'stockCode',title:'Kho',width: 100,sortable:false,resizable:false, align: 'left' },						 
					    {field: 'isUnder',title:'Kho đơn vị cấp dưới',width: 100,sortable:false,resizable:false, align: 'center', formatter: function(value, row, index) {
					    	 		if(row.isUnder != undefined && row.isUnder != null && row.isUnder != 0 ){
					    				return "<input type='checkbox' value='"+row.id+"' id='ckr"+index+"'/>"; /** value: luc nay la id cua record*/
					    			}				    	
						}},
						{field:'edit', title:'<a href="javascript:void(0);" onclick="EquipmentStockPermission.openDialogChangeAddStock(\'ADD\');"><img src="/resources/images/icon_add.png" title="Thêm"/></a>', width:50, align:'center', formatter: function(value, row, index) {
			    			return '<a href="javascript:void(0);" onclick="EquipmentStockPermission.deleteStockInGrid('+index+','+row.id+')"><img src="/resources/images/icon-delete.png" title="Xóa"></a>';
						}},			    
						{field: 'id', hidden: true}		
					]],
					onBeforeLoad:function(param){											 
					},
					onLoadSuccess :function(data){   			 
						$('.datagrid-header-rownumber').html('STT');				
					}
				});
	    		$('#codeAdd, #nameAdd, #descAdd, #statusAdd').bind('keyup',function(event){
	    			if(event.keyCode == keyCodes.ENTER){
	    				$('#dialogParam #btnSaveAddStock').click(); 
	    				$('#dialogParam #btnSaveAddStock').unbind('click');
	    			}
	    		}); 
	    		$('#dialogParam #btnSaveAddStock').unbind('click');	    		
	    		$('#dialogParam #btnSaveAddStock').bind('click',function(event) {
	    			EquipmentStockPermission.add();
				});
	        },
	        onClose:function() {
	        	
	        }
		});
	},
/**
 * Them danh sach quyen kho thiet bi
 * 
 * @author hoanv25
 * @since March 12, 2015
 * */
	add: function(){
		EquipmentStockPermission._mapIdStock = [];
		$('.ErrorMsgStyle').html('').hide();
 		$('.SuccessMsgStyle').html('').hide(); 
		var msg ="";
		if(msg.length == 0){
			msg = Utils.getMessageOfRequireCheck('codeAdd', 'Mã quyền');
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfRequireCheck('nameAdd', 'Tên quyền');
		}
		if (msg.length == 0) {
			msg = Utils.getMessageOfSpecialCharactersValidate('codeAdd', 'Mã quyền',Utils._CODE);
		}
		if(msg.length >0){
			$('#errorMsgDl').html(msg).show();
			return false;
		}
		var params = new Object();		
		params.code = $('#codeAdd').val().trim();
		params.name = $('#nameAdd').val().trim();		
		params.description = $('#descAdd').val().trim();		
		params.status = $('#statusAdd').val().trim();
		var lstEquipId = new Array();	
		/** lay gia tri Id check va no Check*/
		var checkListStock = new Array();
   		var noCheckListStock = new Array();
		$('#gridContainerStock input[id^=ckr]').each(function() {	
			   if($(this).is(":checked")){			 
			     checkListStock.push($(this).val());
			     EquipmentStockPermission._lstAddStockInsert.push($(this).val());		
			   }else{
			     noCheckListStock.push($(this).val());
			   }
		 });
		var rows = $('#gridStock').datagrid('getRows');
		if (rows != undefined && rows != null && rows.length > 0){
				for( var i = 0, size = rows.length; i < size; i++){						
					if(rows[i].isUnder != undefined && rows[i].isUnder != null ){
						if(Number(rows[i].isUnder) == 0){
							EquipmentStockPermission._mapIdStock.push(rows[i].id);													
						}
					  }
					}					
			   }
		params.checkListStock = checkListStock.join(',');
		params.noCheckListStock = noCheckListStock.join(',');		
		params.mapIdStock = 	EquipmentStockPermission._mapIdStock.join(',');	
		var msgDialog = 'Bạn có muốn thêm mới quyền kho thiết bị?';
		JSONUtil.saveData2(params, "/equipment-manager-stock-permission/add", msgDialog, "errorMsgDl", function(data) {
			if(!data.error) {
				$('#dialogParam #successMsgDl').html("Thêm mới dữ liệu thành công").show();
				var tm = setTimeout(function(){
					$('#dialogParam').dialog('close');				
					$('#gridStockPermission').datagrid('reload');
					$('.SuccessMsgStyle').hide();
					$('#stockTransDetailTable').hide();		
					clearTimeout(tm);
			 }, 1500);
			}
		}, "", function (data) { // callBackFail
			if(data.error && data.errMsg != undefined ){
				$('#dialogParam #errorMsgDl').html(data.errMsg).show();
				return;
			}
		});		
		return false;
	},
/**
 * Opend dialog chinh sua quyen kho thiet bi
 * 
 * @author hoanv25
 * @since March 12, 2015
 * */
 openDialogChangeEdit: function(index, id){
	 	EquipmentStockPermission._lstEquipStockGirdDel = [];
		$(".ErrorMsgStyle").html('').hide();
		var data = $('#gridStockPermission').datagrid('getRows')[index];
		$('#dialogParamEdit').dialog({
			title: "Cập nhật quyền "+ '<span style="color:#199700">' + data.code + '</span>',
			closed: false,
			cache: false,
			modal: true,
	        width: 910,
	        height: 'auto',
	        onOpen: function(){
	        	$('.easyui-dialog .InputTextStyle').val('');
	    		$('.easyui-dialog #errorSearchMsg').hide();
	    		$('#nameEdit').focus();
	    		/**combobox status*/
	    		$('#dialogParamEdit .MySelectBoxClass').css('width', '173');
	    		$('#dialogParamEdit .CustomStyleSelectBox').css('width', '140');
	    		$('#dialogParamEdit #btnSaveAddStock').html('Cập nhật');				
	    		$('#codeEdit').attr('disabled', true);	    		
	    		$('#codeEdit').val(data.code);
	    		$('#nameEdit').val(data.name);	    	
	    		$('#descEdit').val(data.description);	    		
	    		$('#statusEdit').val(data.status).change();
	    		var params = new Object();
				params.id = id;	    		
	    		$('#gridStockRoleEdit').datagrid({					
					url :"/equipment-manager-stock-permission/searchListStock",
					autoRowHeight : true,
					rownumbers : true, 
					checkOnSelect :true,		
					pagination:true,
					pageSize :10,
					pageList : [10],
					checkOnSelect :true,	
					scrollbarSize:0,
					fitColumns:true,	
					queryParams: params,		
					width: $('#gridContainerStockEdit').width(),
					autoWidth: true,
					columns:[[
					    {field: 'shopCode',title:'Đơn vị',width: 100,sortable:false,resizable:false, align: 'left' },	
					    {field: 'stockCode',title:'Kho',width: 100,sortable:false,resizable:false, align: 'left' },						 
					    {field: 'isUnder',title:'Kho đơn vị cấp dưới',width: 100,sortable:false,resizable:false, align: 'center', formatter: function(value, row, index) {
					    		if(row.isUnder != undefined && row.isUnder != null){
					    			if(row.isUnder == 1){
					    				return "<input type='checkbox' checked='checked' value='"+row.id+"' id='ckr"+index+"'/>"; 
					    			} else if (row.isUnder != 1 && row.isUnder != 0){
										return "<input type='checkbox' value='"+row.id+"' id='ckr"+index+"' />"; 
					    			}else if(row.isUnder == 0 && row.ch != SpecificType.NPP){
										return "<input type='checkbox' value='"+row.id+"' id='ckr"+index+"' />"; 
					    			}					    		
					    		} 				    	
						}},
						{field:'edit', title:'<a href="javascript:void(0);" onclick="EquipmentStockPermission.openDialogChangeAddStock(\'EDIT\');"><img src="/resources/images/icon_add.png" title="Thêm"/></a>', width:50, align:'center', formatter: function(value, row, index) {
			    			return '<a href="javascript:void(0);" onclick="EquipmentStockPermission.deleteGridStockRoleEdit('+index+','+row.idStock+')"><img src="/resources/images/icon-delete.png" title="Xóa"></a>';
						}},			    
						{field: 'id', hidden: true},
						{field: 'idStock', hidden: true}					    
					]],
					onBeforeLoad:function(param){											 
					},
					onLoadSuccess :function(data){   			 
						$('.datagrid-header-rownumber').html('STT');							
					}
				});
	    		$('#codeEdit, #nameEdit, #descEdit, #statusEdit').bind('keyup',function(event){
	    			if(event.keyCode == keyCodes.ENTER){	    			
	    				$('#dialogParamEdit #btnSaveAddStock').click(); 
	    				$('#dialogParamEdit #btnSaveAddStock').unbind('click');
	    			}
	    		}); 
	    		$('#dialogParamEdit #btnSaveAddStock').unbind('click');
	    		$('#dialogParamEdit #btnSaveAddStock').bind('click',function(event) {
	    			EquipmentStockPermission.update(index, id);
				});
	        },
	        onClose:function() {	        	
	        }
		});
	},
/**
 * Chinh sua quyen kho thiet bi
 * 
 * @author hoanv25
 * @since March 12, 2015
 * */
 update: function(index, id){
 		EquipmentStockPermission._mapIdEditStock = [];
 		EquipmentStockPermission._mapIdEditRoleStock = [];
 		EquipmentStockPermission._lstAddStockInsert = [];	
 		$('.ErrorMsgStyle').html('').hide();
 		$('.SuccessMsgStyle').html('').hide(); 			
		var params = new Object();			
		params.code = $('#codeEdit').val().trim();
		params.name = $('#nameEdit').val().trim();	
		params.description = $('#descEdit').val().trim();
		params.status = $('#statusEdit').val().trim();
		params.id = id; /** id: bang equip_role_id*/
		var data = $('#gridStockPermission').datagrid('getRows')[index];
		
		if(data.name== null){
			data.name = '';
		}		
		if(data.description== null){
			data.description = '';
		}
		var checkListStock = new Array();
   		var noCheckListStock = new Array();
		$('#gridContainerStockEdit input[id^=ckr]').each(function() {			 
				var str = $(this).attr('id');
			 	var indexRecord = str.substring(3, str.length);
			 	var dataEdit = $('#gridStockRoleEdit').datagrid('getRows')[indexRecord];
			 	var idTable = 0; /** id: bang EQUIP_ROLE_DETAIL_ID */
			 	if(dataEdit != undefined && dataEdit != null){
			 		idTable = dataEdit.idStock;
			 	}
			    if($(this).is(":checked")){				    
				     var objectVO = new Object();
				     objectVO.idDetail = idTable;
				     objectVO.idStock = $(this).val();
				     checkListStock.push(objectVO);
				     EquipmentStockPermission._lstAddStockInsert.push(objectVO.idStock);	
			   	}else{		       	
		       		var objectVO = new Object();
			  	 	objectVO.idDetail = idTable;
			   		objectVO.idStock = $(this).val();
		       		noCheckListStock.push(objectVO);
			   }
		 });			
		var rows = $('#gridStockRoleEdit').datagrid('getRows');
		if (rows != undefined && rows != null && rows.length > 0){
				for( var i = 0, size = rows.length; i < size; i++){		
					if(rows[i].isUnder != undefined && rows[i].isUnder != null ){
						if(Number(rows[i].isUnder) == 0 && (rows[i].idStock == undefined || rows[i].idStock == null ||rows[i].idStock =="") ){
							EquipmentStockPermission._mapIdEditStock.push(rows[i].id);													
						}
						if(Number(rows[i].isUnder) == 0 && (rows[i].idStock != undefined || rows[i].idStock != null ||rows[i].idStock !="") ){
							EquipmentStockPermission._mapIdEditRoleStock.push(rows[i].id);													
						}
					  }
					}					
			   }
	 	params.mapIdEditStock = EquipmentStockPermission._mapIdEditStock.join(',');	
	 	params.mapIdEditRoleStock = EquipmentStockPermission._mapIdEditRoleStock.join(',');
	 	if(EquipmentStockPermission._lstEquipStockGirdDel != undefined && EquipmentStockPermission._lstEquipStockGirdDel != null && EquipmentStockPermission._lstEquipStockGirdDel != ""){
	 		params.lstEquipStockGirdDel = EquipmentStockPermission._lstEquipStockGirdDel.join(',');	
	 	}		
		params.checkListStockVO = checkListStock;
		params.noCheckListStockVO = noCheckListStock;
		var msgDialog = 'Bạn có muốn cập nhật quyền kho thiết bị?';
		JSONUtil.saveData2(params, "/equipment-manager-stock-permission/save", msgDialog, "errorEditMsgDl", function(data) {
			if(!data.error) {
				$('#dialogParamEdit #successMsgDl').html("Lưu dữ liệu thành công").show();
				var tm = setTimeout(function(){
					$('#dialogParamEdit').dialog('close');				
					$('#gridStockPermission').datagrid('reload');
					$('.SuccessMsgStyle').hide();
					$('#stockTransDetailTable').hide();		
					clearTimeout(tm);
			 }, 1500);
			}
		}, "", function (data) { // callBackFail
			if(data.error && data.errMsg != undefined ){
				$('#dialogParamEdit #errorEditMsgDl').html(data.errMsg).show();
				return;
			}
		});
		return false;		
	},
/**
 * Opend dialog copy quyen kho thiet bi
 * 
 * @author hoanv25
 * @since March 12, 2015
 * */
 openDialogChangeCopy: function(index, id){
		$(".ErrorMsgStyle").hide();
		$('#dialogParamCopy').dialog({
			title: "Copy quyền",
			closed: false,
			cache: false,
			modal: true,
	        width: 800,
	        height: 'auto',
	        onOpen: function(){
	        	$('.easyui-dialog .InputTextStyle').val('');
	    		$('.easyui-dialog #errorSearchMsg').hide();
	    		$('.easyui-dialog #statusCopy').val(1).change();
	    		$('#codeCopy').attr('disabled', false);
	    		$('#codeCopy').focus();
	    		/**combobox status*/
	    		$('#dialogParamCopy .MySelectBoxClass').css('width', '173');
	    		$('#dialogParamCopy .CustomStyleSelectBox').css('width', '140');
	    		$('#dialogParamCopy #btnSaveCopyStock').html('Cập nhật');	    	
	    		$('#dialogParamCopy #btnSaveCopyStock').unbind('click');
	    		$('#dialogParamCopy #btnSaveCopyStock').bind('click',function(event) {
	    			EquipmentStockPermission.copy(index, id);
				});
	        },
	        onClose:function() {
	        	
	        }
		});
	},
/**
 * Copy quyen kho thiet bi
 * 
 * @author hoanv25
 * @since March 12, 2015
 * */
 copy: function(index, id){
 	$('.ErrorMsgStyle').html('').hide();
 	$('.SuccessMsgStyle').html('').hide();
 	var msg ="";
		if(msg.length == 0){
			msg = Utils.getMessageOfRequireCheck('codeCopy', 'Mã quyền');
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfRequireCheck('nameCopy', 'Tên quyền');
		}
			// if(msg.length == 0){
			// msg = Utils.getMessageOfRequireCheck('descCopy', 'Mô tả');
		// }
		if (msg.length == 0) {
			msg = Utils.getMessageOfSpecialCharactersValidate('codeCopy', 'Mã quyền',Utils._CODE);
		}
		if(msg.length >0){
			$('#dialogParamCopy #errorMsgDl').html(msg).show();
			return false;
		}
		var params = new Object();			
		params.code = $('#codeCopy').val().trim();
		params.name = $('#nameCopy').val().trim();	
		params.description = $('#descCopy').val().trim();
		params.status = $('#statusCopy').val().trim();
		params.id = id;		
		var msgDialog = 'Bạn có muốn copy quyền kho thiết bị?';
		JSONUtil.saveData2(params, "/equipment-manager-stock-permission/copy", msgDialog, "errorEditMsgDl", function(data) {
			if(!data.error) {
				$('#dialogParamCopy #successMsgDl').html("Lưu dữ liệu thành công").show();
				var tm = setTimeout(function(){
					$('#dialogParamCopy').dialog('close');				
					$('#gridStockPermission').datagrid('reload');
					$('.SuccessMsgStyle').hide();
					$('#stockTransDetailTable').hide();		
					clearTimeout(tm);
			 }, 1500);
			}
		}, "", function (data) { // callBackFail
			if(data.error && data.errMsg != undefined ){
				$('#dialogParamCopy #errorMsgDl').html(data.errMsg).show();
				return false;
			}
		});	
		return false;
	},
/**
 * Opend dialog tim kiem kho thiet bi
 * 
 * @author hoanv25
 * @since March 12, 2015
 * */
 openDialogChangeAddStock: function(type){
	 $(".ErrorMsgStyle").hide();
		var html = '<div id="add-shopPopupDiv" style="display:none;">\
			<div id="add-shopPopup">\
			<div class="PopupContentMid">\
				<div class="GeneralForm Search1Form">\
					<label class="LabelStyle" style="width:100px;">Mã đơn vị</label>\
					<input id="shopCode" class="InputTextStyle" style="width:150px;" maxlength="50" />\
					<label class="LabelStyle" style="width:100px;">Tên đơn vị</label>\
					<input id="shopName" class="InputTextStyle" style="width:230px;" maxlength="250" />\
					<div class="Clear"></div>\
					<label class="LabelStyle" style="width:100px;">Mã kho</label>\
					<input id="stockCode" class="InputTextStyle" style="width:150px;" maxlength="50" />\
					<label class="LabelStyle" style="width:100px;">Tên kho</label>\
					<input id="stockName" class="InputTextStyle" style="width:230px;" maxlength="250" />\
					<div class="Clear"></div>\
					<div class="BtnCenterSection">\
					    <button class="BtnGeneralStyle" id="btnSearchDlg">Tìm kiếm</button>\
					</div>\
					\
					<h2 class="Title2Style">Danh sách đơn vị</h2>\
					<div class="GridSection" id="gridDlgDiv">\
						<div id="gridDlg"></div>\
					</div>\
					\
					<p id="erMsgDlg" class="ErrorMsgStyle" style="padding-left:5px;display:none;"></p>\
					<div class="Clear"></div>\
					<div class="BtnCenterSection">\
					    <button class="BtnGeneralStyle" id="btnChooseDlg">Chọn</button>\
						<button class="BtnGeneralStyle" id="btnCloseDlg" onclick="javascript:$(\'#add-shopPopup\').dialog(\'close\');">Bỏ qua</button>\
					</div>\
				</div>\
			</div>\
			</div>\
		</div>';
		$("body").append(html);
		
		$("#add-shopPopup").dialog({
			title: "Tìm kiếm kho",
			width: 850,
			heigth: "auto",
			modal: true, close: false, cache: false,
			onOpen: function() {
				$("#add-shopPopup").addClass("easyui-dialog");
				$("#add-shopPopup #shopCode").focus();	
				var params = new Object();
				params.shopCode = $("#add-shopPopup #shopCode").val().trim();
				params.shopName = $("#add-shopPopup #shopName").val().trim();
				params.stockCode = $("#add-shopPopup #stockCode").val().trim();
				params.stockName = $("#add-shopPopup #stockName").val().trim();	

				var lstEquipId = new Array(); 
				var lstObjectId = new Array();	
				var lstCheckSearchStockAdd = new Array();
				EquipmentStockPermission._lstAddStockInsert = [];				
				EquipmentStockPermission._lstEditStockInsert = [];	
				EquipmentStockPermission._lstCheckParenId = [];
				/*** lay tu Add: type = 0 */
				if(type != undefined && type == EquipmentStockPermission._ADD){
		        	var rows = $('#gridStock').datagrid('getRows');
					if (rows != undefined && rows != null && rows.length > 0){
						for( var i = 0, size = rows.length; i < size; i++){					
							if(rows[i].id != undefined){
								lstEquipId.push(rows[i].id);	
								lstObjectId.push(rows[i].objectId);						
							}					
						}
						params.lstEquipId = lstEquipId.join(',');
						params.lstObjectId = lstObjectId.join(',');
					}
					$('#gridContainerStock input[id^=ckr]').each(function() {	
						   if ($(this).is(":checked")){
							   lstCheckSearchStockAdd.push($(this).val());	
						   }
					});
					params.lstCheckSearchStockAdd = lstCheckSearchStockAdd.join(',');
				} else if (type != undefined && type == EquipmentStockPermission._EDIT){
					/*** lay tu edit: type = 1 */
					var row = $('#gridStockRoleEdit').datagrid('getRows');
					if (row != undefined && row != null && row.length > 0){
						for( var i = 0, size = row.length; i < size; i++){					
							if(row[i].id != undefined){
								lstEquipId.push(row[i].id);	
								lstObjectId.push(row[i].objectId);						
							}					
						}
						params.lstEquipId = lstEquipId.join(',');
						params.lstObjectId = lstObjectId.join(',');
					}
					$('#gridContainerStockEdit input[id^=ckr]').each(function() {	
						   if ($(this).is(":checked")){
							   lstCheckSearchStockAdd.push($(this).val());	
						   }
					});
					params.lstCheckSearchStockAdd = lstCheckSearchStockAdd.join(',');
				}
				$("#add-shopPopup #gridDlg").datagrid({
					url :"/equipment-manager-stock-permission/searchStock",
					autoRowHeight : true,
					rownumbers : true, 
					checkOnSelect :true,		
					pagination:true,
					pageSize :10,
					pageList : [10],
					checkOnSelect :true,	
					scrollbarSize:0,
					fitColumns:true,
					width:  $('#gridDlgDiv').width(),
					queryParams: params,						
					columns: [[
					    {field: 'shopCode',title:'Mã đơn vị',width: 80,sortable:false,resizable:false, align: 'left' },	
					    {field: 'shopName',title:'Tên đơn vị',width: 100,sortable:false,resizable:false, align: 'left', formatter: function(v, r, i) {							
							return Utils.XSSEncode(v);
						}},	
						{field: 'stock',title:'Kho',width: 100,sortable:false,resizable:false, align: 'left', formatter: function(v, r, i) {							
							return Utils.XSSEncode(v);
						}},							
						{field: 'ckeck', title:"Chọn", sortable: false, resizable: false, width:50, fixed:true, align:"center", formatter: function(v, r, i) {							
							return "<input type='checkbox' value='"+i+"' id='ck"+r.id+"'/>";
						}}
					]],
					onLoadSuccess: function(data) {
						$("#add-shopPopup #gridDlg").datagrid("resize");
						Utils.bindFormatOnTextfieldInputCss('qttClass', Utils._TF_NUMBER, "#add-shopPopup");					
						setTimeout(function(){
							var hDlg=parseInt($("#add-shopPopup").parent().height());
							var hW=parseInt($(window).height());
							var d=hW-hDlg;
							d=d/2+document.documentElement.scrollTop;
							if (d < 0) { d = 0; }
							$("#add-shopPopup").parent().css('top',d);
			    		},1000);
					}
				});
				
				$("#add-shopPopup #btnSearchDlg").click(function() {
					var params = {
							shopCode: $("#add-shopPopup #shopCode").val().trim(),
							shopName: $("#add-shopPopup #shopName").val().trim(),
							stockCode: $("#add-shopPopup #stockCode").val().trim(),
							stockName: $("#add-shopPopup #stockName").val().trim(),
							lstEquipId: lstEquipId.join(','),
							lstObjectId: lstObjectId.join(','),
							lstCheckSearchStockAdd: lstCheckSearchStockAdd.join(','),
					};
					$("#add-shopPopup #gridDlg").datagrid("reload", params);
				});
				$("#add-shopPopup #shopCode, #add-shopPopup #shopName, #add-shopPopup #stockCode, #add-shopPopup #stockName").parent().keyup(function(event) {
					if (event.keyCode == keyCodes.ENTER) {
						$("#add-shopPopup #btnSearchDlg").click();
					}
				});		
				$("#add-shopPopup #btnChooseDlg").click(function() {
					if ($("#add-shopPopup input[id^=ck]:checked").length == 0) {
						$("#erMsgDlg").html("Không có kho đơn vị nào được chọn").show();
						return;
					}	
					if(type != undefined && type == EquipmentStockPermission._ADD){				
						EquipmentStockPermission.addShopQtt();
				 	}else if (type != undefined && type == EquipmentStockPermission._EDIT){
				 		EquipmentStockPermission.editShopQtt();
				 	}
				});
			},
			onClose: function() {
				$("#add-shopPopup").dialog("destroy");
				$("#add-shopPopupDiv").remove();
			}
		});
	},
	/**hoanv25; remove value khoi listArray: array */
	removeArrayJS: function(array, value) {
		if(array != null && value != null){
			for(var i = array.length - 1; i >= 0; i--) {
			    if(array[i] === value) {
			       array.splice(i, 1);
			    }
			}
		}
	},
	
	addShopQtt: function() {
		$(".ErrorMsgStyle").hide();
		EquipmentStockPermission._lstAddStock = [];		
		$.messager.confirm("Xác nhận", "Bạn có muốn thêm kho đơn vị?", function(r) {
			if (r) {
				var lstTmp = new Map();			
				$("#add-shopPopup input[id^=ck]").each(function() {					    
				        var str = $(this).attr('id');
				        var id = str.substring(2, str.length);
				        var data = $("#add-shopPopup #gridDlg").datagrid('getRows')[ $(this).val()];	
				        if($(this).is(":checked")){					        
					        lstTmp.put(id, data);
					        EquipmentStockPermission._lstAddStock.push(id);
					        EquipmentStockPermission._lstEquipStockInsert.push(id);
				       	}else{
				       		/**hoanv25; remove value khoi listArray: array /// remove id khoi EquipmentStockPermission._lstAddStock */
				       		EquipmentStockPermission.removeArrayJS(EquipmentStockPermission._lstAddStock, id);					       
				       	}
					});	
					/** remove Id trung trong lstTmp*/
					var dataList = $('#gridStock').datagrid('getRows');
					for (var i = 0; i< dataList.length; i ++) {
						if(lstTmp.findIt(dataList[i].id) == 0){ /**id nay ton tai trong map*/
							lstTmp.remove(dataList[i].id);
						}
					}
					/**Kiem tra kho da chọn có cả kho cha va kho con hay khong*/
					/*$('#gridContainerStock input[id^=ckr]').each(function() {	
					   if($(this).is(":checked")){					   
						     EquipmentStockPermission._lstAddStockInsert.push($(this).val());		
						   }
						});
					var rows = $('#gridStock').datagrid('getRows');
					if (rows != undefined && rows != null && rows.length > 0){
							for( var i = 0, size = rows.length; i < size; i++){					
								if(rows[i].isUnder != undefined && rows[i].isUnder != null ){
									if(Number(rows[i].isUnder) == 0){
										EquipmentStockPermission._lstAddStockInsert.push(rows[i].id);													
									}
								  }
								}					
						   }

					var params = new Object();					
					params.lstAddStock = EquipmentStockPermission._lstAddStock.join(',');				
					Utils.getJSONDataByAjaxNotOverlay(params, '/equipment-manager-stock-permission/checkAddStock', 
					function(equipments){
							if(equipments != undefined && equipments != null && equipments.length > 0) {
									$('#erMsgDlg').html('Không được chọn cả kho cha và kho cấp con!').show();
									return false;									
							} else {
								*//**Kiem tra xem kho co bi trung chom hay khong*//*
								var modelParams = new Object();					
								modelParams.lstAddStock = EquipmentStockPermission._lstAddStock.join(',');	
								modelParams.lstAddStockInsert = EquipmentStockPermission._lstAddStockInsert.join(',');			
								Utils.getJSONDataByAjaxNotOverlay(modelParams, '/equipment-manager-stock-permission/checkAddStockChild', 
									function(check){
									if(check != undefined && check != null && check.length > 0) {
											$('#erMsgDlg').html('Không được chọn kho trùng chờm!').show();
											return false;
									} else {
										
									}							
								}, null, null);
							}							
					}, null, null);		*/		
					/** Add kho vao gird danh sach*/
					for (var i = 0; i< lstTmp.keyArray.length; i ++) {
						var object = lstTmp.get(lstTmp.keyArray[i]);
						var newRow = {
							id: object.id,
							shopCode: Utils.XSSEncode(object.shopCode),
							stockCode: Utils.XSSEncode(object.stock),
							isUnder: object.isUnder,
							objectId: object.objectId
							};
						$('#gridStock').datagrid('appendRow', newRow);
					};
					$("#add-shopPopup").dialog("close");
			}
		});
	},
	editShopQtt: function() {
		$(".ErrorMsgStyle").hide();
		EquipmentStockPermission._lstEditStock = [];	
		$.messager.confirm("Xác nhận", "Bạn có muốn thêm kho đơn vị?", function(r) {
			if (r) {
				var lstTmp = new Map();
				$("#add-shopPopup input[id^=ck]:checked").each(function() {					    
				        var str = $(this).attr('id');
				        var id = str.substring(2, str.length);
				        var data = $("#add-shopPopup #gridDlg").datagrid('getRows')[ $(this).val()];				        
				        lstTmp.put(id, data);
				        EquipmentStockPermission._lstEditStock.push(id);
				        EquipmentStockPermission._lstEquipStockInsert.push(id);
				       
				});
				/** remove Id trung trong lstTmp*/
				var dataList = $('#gridStockRoleEdit').datagrid('getRows');
				for (var i = 0; i< dataList.length; i ++) {
					if(lstTmp.findIt(dataList[i].id) == 0){ /**id nay ton tai trong map*/
						lstTmp.remove(dataList[i].id);
					}
				}
				/**Kiem tra kho da chọn có cả kho cha va kho con hay khong*/
				/*	$('#gridContainerStockEdit input[id^=ckr]').each(function() {	
					   if($(this).is(":checked")){					   
						     EquipmentStockPermission._lstEditStockInsert.push($(this).val());		
						   }
						});
					var rows = $('#gridStockRoleEdit').datagrid('getRows');
					if (rows != undefined && rows != null && rows.length > 0){
							for( var i = 0, size = rows.length; i < size; i++){					
								if(rows[i].isUnder != undefined && rows[i].isUnder != null ){
									if(Number(rows[i].isUnder) == 0 ){
										EquipmentStockPermission._lstCheckParenId.push(rows[i].id);	
										var modParams = new Object();											
										modParams.lstAddStock = EquipmentStockPermission._lstCheckParenId.join(',');			
										Utils.getJSONDataByAjaxNotOverlay(modParams, '/equipment-manager-stock-permission/checkParenIdStock', 
												function(check){
												if(check != undefined && check != null && check.length > 0) {
												} else {
													EquipmentStockPermission._lstEditStockInsert.push(rows[i].id);	
												}							
											}, null, null);
																						
										}
								  }
								}					
						   }
					var params = new Object();					
					params.lstAddStock = EquipmentStockPermission._lstEditStock.join(',');				
					Utils.getJSONDataByAjaxNotOverlay(params, '/equipment-manager-stock-permission/checkAddStock', 
					function(equipments){
							if(equipments != undefined && equipments != null && equipments.length > 0) {
									$('#erMsgDlg').html('Không được chọn cả kho cha và kho cấp con!').show();
									return false;									
							} else {
								*//**Kiem tra xem kho co bi trung chom hay khong*//*
								var modelParams = new Object();					
								modelParams.lstAddStock = EquipmentStockPermission._lstEditStock.join(',');	
								modelParams.lstAddStockInsert = EquipmentStockPermission._lstEditStockInsert.join(',');			
								Utils.getJSONDataByAjaxNotOverlay(modelParams, '/equipment-manager-stock-permission/checkAddStockChild', 
									function(check){
									if(check != undefined && check != null && check.length > 0) {
											$('#erMsgDlg').html('Không được chọn kho trùng chờm!').show();
											return false;
									} else {
										
									}							
								}, null, null);
							}							
					}, null, null);			*/	
				/** insert row*/
				for (var i = 0; i< lstTmp.keyArray.length; i ++) {
					 var object = lstTmp.get(lstTmp.keyArray[i]);
					 var newRow = {
							id: object.id,
							shopCode: Utils.XSSEncode(object.shopCode),
							stockCode: Utils.XSSEncode(object.stock),
							isUnder: object.isUnder,
							objectId: object.objectId,
							ch:object.ch
							};
						$('#gridStockRoleEdit').datagrid('appendRow', newRow);
						};				
					$("#add-shopPopup").dialog("close");
			}
		});
	},
	/**
	 * Xoa mot dong quyen kho thiet bi trong gird
	 * @author hoanv25
	 * @since March 3, 2015
	 */
	deletePermissionStockInGrid: function(index, id){
		if (id == undefined || id == null) {
			return;
		}
		if (id > 0) {
			EquipmentStockPermission._lstEquipStockDel.push(id);
		}	
		var params = new Object();			
		params.id = id;		
		var msgDialog = 'Bạn có muốn xóa quyền?';
		JSONUtil.saveData2(params, "/equipment-manager-stock-permission/delete", msgDialog, "errMsgSearch", function(data) {
			if(!data.error) {
				$('#successMsgDl').html("Xóa dữ liệu thành công").show();
				var tm = setTimeout(function(){
					$('#dialogParam').dialog('close');				
					$('#gridStockPermission').datagrid('reload');
					$('#stockTransDetailTable').hide();		
					$('.SuccessMsgStyle').hide();
					clearTimeout(tm);
			 }, 1500);
			}
		}, "", function (data) { // callBackFail
			if(data.error && data.errMsg != undefined ){
				$('#errMsgSearch').html(data.errMsg).show();
				return false;
			}
		});	
	/*	Utils.addOrSaveData(params, '/equipment-manager-stock-permission/delete', null,  'errorMsgDl', function(data) {
			$('#successMsgDl').html("Xóa dữ liệu thành công").show();
			var tm = setTimeout(function(){
				$('#dialogParam').dialog('close');				
				$('#gridStockPermission').datagrid('reload');
				$('#stockTransDetailTable').hide();		
				$('.SuccessMsgStyle').hide();
				clearTimeout(tm);
			 }, 1200);
		}, null, null, null,msg);*/
		return false;
	},
	/**
	 * Xem chi tiet mot dong quyen kho thiet bi trong gird
	 * @author hoanv25
	 * @since March 3, 2015
	 */
	 viewDetail : function(index, id) {	
	 	if(id == null && id == undefined){
	 		return false;
	 	}	 	
		$('#stockTransDetailTable').show();
		var params = new Object();
		params.id = id;
		var data = $('#gridStockPermission').datagrid('getRows')[index];
		$('#title').html('Danh sách kho - ' + '<span style="color:#dc143c">' + data.code + '</span>').show();	
		$('#productDetailGrid').html('<table id="gridDetail" class="easyui-datagrid"></table>');
		$('#gridDetail').datagrid({
			url : '/equipment-manager-stock-permission/viewdetailstock',
			method: 'GET',
			fitColumns:true,
			width: $('#productDetailGrid').width(),
			autoWidth: true,
			autoRowHeight : true,
			singleSelect: true,
			rownumbers : true, 
			checkOnSelect :true,
			pagination:true,
			pageSize:10,
			pageList  : [10,20,30],
			scrollbarSize:0,
			queryParams:params,
			columns:[[
			        {field:'shopCode', title: 'Đơn vị', width: 100, sortable:false,resizable:false , align: 'left', formatter:function(value, row, index) {
			        	  return Utils.XSSEncode(value);
			          }},
			  	    {field:'stockCode', title: 'Kho', width: 130, sortable:false,resizable:false , align: 'left', formatter:function(value, row, index) {
			        	  return Utils.XSSEncode(value);
			          }},
			         {field: 'isUnder', title:'Kho đơn vị cấp dưới', sortable: false, resizable: false, width:250, fixed:true, align:"center", formatter: function(v, r, i) {	
			         	if(r.isUnder != undefined && r.isUnder != null && r.isUnder == 1){
			         		var html = '<a onclick=""><span style="cursor:pointer"><img title="" src="/resources/images/icon-tick.png" width="16" height="16"/></span></a>';
			           	   return html;
			           	}else{
			         	
			         		}							
						}}			      	       
			]],	    
			onLoadSuccess :function(data){				
				 $('.datagrid-header-rownumber').html('STT');	    	
				 updateRownumWidthForJqGrid('.easyui-dialog');
				 $(window).resize();    				
			}
		});		
 		$('html,body').animate({scrollTop: $('#productDetailGrid').offset().top}, 1500);
		return false;
	},
	/**
	 * Xoa mot dong danh sach kho trong gird
	 * @author hoanv25
	 * @since March 13, 2015
	 */
	deleteGridStockRoleEdit: function(index, id){		
		if (id != undefined && id != null && id > 0) {
			EquipmentStockPermission._lstEquipStockGirdDel.push(id);
			/** id: bang quyen*/
		}
		$('#gridStockRoleEdit').datagrid("deleteRow", index);
		var rowEquip = $('#gridStockRoleEdit').datagrid('getRows');
		var nSize = rowEquip.length;
		for(var i=index; i<nSize; i++){
			var row = rowEquip[i];
			$('#gridStockRoleEdit').datagrid('updateRow',{
				index: i,	
				row: row
			});	
		}
	 },
	deleteStockInGrid: function(index, id){			 
		EquipmentStockPermission._lstEquipStockGirdDel = [];
		if (id != undefined && id != null && id > 0) {
			EquipmentStockPermission._lstEquipStockGirdDel.push(id);
			/** id: bang quyen*/
		}
		$('#gridStock').datagrid("deleteRow", index);
		var rowEquip = $('#gridStock').datagrid('getRows');
		var nSize = rowEquip.length;
		for(var i=index; i<nSize; i++){
			var row = rowEquip[i];
			$('#gridStock').datagrid('updateRow',{
				index: i,	
				row: row
			});	
		}		
	},

	/**
	 * Mo Dialog Nhap Excel quyen kho thiet bi
	 * @author VuongMQ
	 * @date Mar 16,215
	 * */
	openDialogImportEquipmentStockPermission: function (){
		//alert('Nhập Excel nè');
		$('.SuccessMsgStyle').html('').hide();
		$('.ErrorMsgStyle').html('').hide();
		$('#easyuiPopupImportExcel').dialog({
			title: 'Nhập Excel Quyền kho thiết bị',
			width: 500, 
			height: 'auto',
			closed: false,
			cache: false,
			modal: true,
			onOpen: function() {
				$('#fakefilepcEquipStockPermission').val('');
			},
			onClose: function() {
				$('.ErrorMsgStyle').html('').hide();
			}
		});
	},
	/**
	 * Import Excel quyen kho thiet bi
	 * @author vuongmq
	 * @date Mar 23, 2015
	 * */
	importEquipmentStockPermission: function() {
		$('.SuccessMsgStyle').html('').hide();
		$('.ErrorMsgStyle').html('').hide();
		if($('#fakefilepcEquipStockPermission').val().length <= 0 ){
			$('#errExcelMsg').html('Vui lòng chọn tập tin Excel').show();
			return false;
		}
		Utils.importExcelUtils(function(data){
			//EquipmentListEquipment.searchListEquipment();
			$('#fakefilepcEquipStockPermission').val("").change();
			if (data.message != undefined && data.message != null && data.message.trim().length > 0) {
				$('#errExcelMsg').html(data.message.trim()).change().show();
				/**Mong muon du thanh cong hay that bai van vao day, giu lai popup cho nguoi dung xem*/
			} else {
				//$('#successExcelMsg').html("Lưu dữ liệu thành công").show();
				var tm = setTimeout(function(){
					$('.SuccessMsgStyle').html('').hide();
					clearTimeout(tm);
					$('#easyuiPopupImportExcel').dialog('close');
					//$('#listEquipmentDg').datagrid("load", EquipmentStockChange._params);
				 }, 1500);
			}
		}, 'importFrmEquipStockPermission', 'excelFileEquipStockPermission', 'errExcelMsg');
		
	},
	/**
	 * xuat file excel quyen kho thiet bi
	 * @author vuongmq
	 * @date March 23, 2015
	 */
	exportExcel: function() {		
		$('.SuccessMsgStyle').html('').hide();
		$('.ErrorMsgStyle').html('').hide();
		var msg ='';
		var data = $('#gridStockPermission').datagrid('getRows');	
		if(data == null || data.length <=0){
			msg = "Không có dữ liệu xuất Excel";
		}
		if(msg.length > 0){
			$('#errMsgSearch').html(msg).show();
			return false;
		}
 		/*var code = $('#code').val().trim();
		var name = $('#name').val().trim();
		var status = $('#status').val().trim();	
		var params =  new Object();
			params.code = code;
			params.name = name;		
			params.status = status;	*/
					
		$.messager.confirm('Xác nhận','Bạn có muốn xuất file excel?',function(r){
			if(r){
				ReportUtils.exportReport('/equipment-manager-stock-permission/exportExcelBySearchEquipmentStockPermission', EquipmentStockPermission._params, 'errMsgSearch');					
			}
		});		
		return false;
	},
};
/*
 * END OF FILE - /web/web/resources/scripts/business/equipment/vnm.equipment-management-stock-permission.js
 */

/*
 * START OF FILE - /web/web/resources/scripts/business/equipment/vnm.equip-item-config.js
 */
/**
 * Thiet lap dinh muc hang muc
 * 
 * @author liemtpt
 * @since 30/3/2015
 * 
 * @author hunglm16
 * @since May 07,2015
 * @description Ra soat va hotfix
 * */
var EquipItemConfig = {
		/**
		 * Khai bao thuoc tinh Bien Toan Cuc
		 * 
		 * */
		_listEquipmentDg: null,
		_mapData: null,
		_params: new Object(),
		_equipChange: null,
		_countFile: 0,
		_arrFileDelete: null,
		_equipItemConfig: null,
		_mapRowDetailChange: null,
		
		/**
		 * Tim kiem danh sach dinh muc hang muc
		 * @author liemtpt
		 * @since 07/04/2015
		 * 
		 * @author hunglm16
		 * @since 07/05/2015
		 * @description bo sung validate tim kiem
		 * */
		search: function () {
			$('.SuccessMsgStyle').html('').hide();
			$('.ErrorMsgStyle').html('').hide();
			var fromCapacity= $('#txtFromCapacity').val().trim().replace(/,/g, '');
			var toCapacity = $('#txtToCapacity').val().trim().replace(/,/g, '');
			var status = $('#cbxStatus').val().trim();
			var msg = '';
			if (isNaN(fromCapacity)) {
				$('#txtFromCapacity').focus();
				msg = 'Dung tích từ phải là kiểu số nguyên';
			}
			if (msg.length == 0 && isNaN(toCapacity)) {
				$('#txtToCapacity').focus();
				msg = 'Đến phải là kiểu số nguyên';
			}
			if (msg.trim().length > 0) {
				$('#errMsg').html(msg.trim()).show();
				$('#grid').datagrid("loadData", []);
				return;
			}
			EquipItemConfig._params = {
				fromCapacity: fromCapacity
				,toCapacity: toCapacity
				,status: status
			};
			$('#grid').datagrid("load", EquipItemConfig._params);
		},
		
		/**
		 * dialog add or update equip item
		 * @author liemtpt
		 * @since 10/04/2015
		 * 
		 * @author hunglm16
		 * @since 10/05/2015
		 * @description Ra soat va update
		 */
		dialogAddOrUpdateEquipmentItem: function(id, index){
			$(".ErrorMsgStyle").html('').hide();
			$(".SuccessMsgStyle").html('').hide();
			$('#fromCapacity').val("");				    			
			$('#toCapacity').val("");
			var titleView = 'Thêm mới định mức';
			var row = $('#grid').datagrid('getRows')[index];
	    	if (row != undefined && row != null && id == row.id) {
	    		EquipItemConfig._equipItemConfig = {
	    				id: row.id,
	    				status: row.status,
	    				equipItemConfigCode: row.equipItemConfigCode,
	    				toCapacity: row.toCapacity,
	    				fromCapacity: row.fromCapacity
	    		};
	    		titleView = 'Chỉnh sửa định mức ' + row.equipItemConfigCode;
	    		
	    	} else {
	    		EquipItemConfig._equipItemConfig = null;
	    	}
			$('#dialogEquipItemConfig').dialog({
				title: titleView,
				closed: false,
				cache: false,
				modal: true,
		        width: 460,
		        height: 'auto',
		        onOpen: function(){
		        	$('.easyui-dialog .InputTextStyle').val('');
		    		$('#dialogEquipItemConfig .MySelectBoxClass').css('width', '173');
		    		$('#dialogEquipItemConfig .CustomStyleSelectBox').css('width', '140');
		    		/** truong hop chinh sua*/
		    		if(!isNaN(id) && !isNaN(index) && Number(id) > 0 && Number(index) >= 0){
				    	$('#fromCapacity').focus();
				    	if (row.fromCapacity != undefined && row.fromCapacity != null) {
			    			$('#fromCapacity').val(row.fromCapacity);				    			
			    		}
			    		if (row.toCapacity != undefined && row.toCapacity != null) {
			    			$('#toCapacity').val(row.toCapacity);				    			
			    		}
		    		} 
		    		$('#fromCapacity').focus();
		    		$('#dialogEquipItemConfig #btnSaveEquipItemConfig').unbind('click');
		    		$('#dialogEquipItemConfig #btnSaveEquipItemConfig').bind('click', function(event) {
		    			EquipItemConfig.saveOrUpdateEquipItemConfig(id);
					});
		        },
		        onClose:function() {
		        	$(".ErrorMsgStyle").html('').hide();
					$(".SuccessMsgStyle").html('').hide();
					$('#fromCapacity').val("");				    			
					$('#toCapacity').val("");			    			
					EquipItemConfig._equipItemConfig = null;
		        }
			});
		},
		/**
		 * Them moi hay cap nhat dinh muc
		 * 
		 * @author liemtpt
		 * @since 11/04/2015
		 * 
		 * @author hunglm16
		 * @since May 07, 2015
		 * @description Ra soat va hotfix
		 * */
		saveOrUpdateEquipItemConfig : function (id) {
			$(".ErrorMsgStyle").html('').hide();
			$(".SuccessMsgStyle").html('').hide();
			var msg ="";
			var fromCapacity = $('#fromCapacity').val().trim().replace(/,/g,'');
			var toCapacity = $('#toCapacity').val().trim().replace(/,/g,'');
			if (isNaN(fromCapacity)) {
				$('#fromCapacity').focus();
				msg = 'Dung tích từ phải là kiểu số nguyên';
			}
			if (msg.length == 0 && isNaN(toCapacity)) {
				$('#toCapacity').focus();
				msg = 'Đến phải là kiểu số nguyên';
			}
			if (msg.length == 0 && fromCapacity.length == 0 && toCapacity.length == 0) {
				$('#fromCapacity').focus();
				msg = 'Dung tích từ và Đến phải ít nhất có một giá trị được nhập';
			} 
			if (msg.length == 0 && fromCapacity.length > 0 && toCapacity.length > 0 && Number(toCapacity) < Number(fromCapacity)) {
				$('#fromCapacity').focus();
				msg = 'Dung tích từ phải nhỏ hơn hoặc bằng Đến';
			}
			if (msg.length == 0 && !isNaN(id) && Number(id) > 0 && EquipItemConfig._equipItemConfig != null && EquipItemConfig._equipItemConfig.id == id) {
				if (EquipItemConfig._equipItemConfig.fromCapacity == Number(fromCapacity) && EquipItemConfig._equipItemConfig.toCapacity == Number(toCapacity)) {
					msg = 'Không thấy có giá trị nào được thay đổi';
				}
			}
			if (msg.trim().length > 0) {
				$('#errorMsgDl').html(msg).show();
				return false;
			}
			var msgConfirm = 'Bạn có muốn thêm mới định mức?';
			var params = new Object();	
			if (!isNaN(id) && Number(id) > 0) {
				params.id = id;
				msgConfirm = 'Bạn có muốn Cập nhật định mức?';
			}
			params.fromCapacity = $('#fromCapacity').val().trim();
			params.toCapacity = $('#toCapacity').val().trim();
//			params.status = $('#cbxStatusDl').val().trim();
			
			Utils.addOrSaveData(params, '/equip-item-config/save-equip-item-config', null,  'errorMsgDl', function(data) {
				$('#successMsgDl').html("Lưu dữ liệu thành công").show();
				$('#grid').datagrid('reload');
				var tm = setTimeout(function(){
					$('#dialogEquipItemConfig').dialog('close');
					$('.SuccessMsgStyle').html("").hide();
					clearTimeout(tm);
				 }, 1500);
			}, null, null, null, msgConfirm);
			return false;
		},
		
		/**
		 * Xoa dinh muc
		 * 
		 * @author liemtpt
		 * @since 13/04/2015
		 * */
		removedEquipmentItemConfig: function(id, code) {
			var msg = 'Bạn có muốn Xóa Định mức ' + code;
			$.messager.confirm('Xác nhận', msg, function(r){
	  			if (r){
	  				var params = new Object();
	  				params.id = id;
	  				Utils.saveData(params,'/equip-item-config/removed-equip-item-config', null, null, function(){
	  					$('#grid').datagrid('reload');			
	  				});
	  			}
	  		});
			return false;
		},
		/**
		 * Xoa dinh muc
		 * 
		 * @author hunglm16
		 * @since May 07,2015
		 * */
		stopEquipmentItemConfig: function(id, code) {
			var msg = 'Bạn có muốn Tạm ngưng Định mức ' + code;
			$.messager.confirm('Xác nhận', msg, function(r){
	  			if (r){
	  				var params = new Object();
	  				params.id = id;
	  				Utils.saveData(params,'/equip-item-config/stop-equip-item-config', null, null, function(){
	  					$('#grid').datagrid('reload');			
	  				});
	  			}
	  		});
			return false;
		},
		/**
		 * edit equip item config detail 
		 * 
		 * @author liemtpt
		 * @since 13/04/2015
		 * @return
		 * @description chinh sua du lieu row
		 * */
		editEquipItemConfigDtl: function(key, idx, indexRows){
			var idDiv = idx + '_' + indexRows;
			$('#dg_chidrent_edit_'+idDiv).show();
			$('#dg_chidrent_save_'+idDiv).show();
			$('#dg_chidrent_back_'+idDiv).show();
			var ddv = $('#grid').datagrid('getRowDetail', indexRows).find('table.ddv');
			var mapKeyId = indexRows.toString() +'_'+ idx.toString();
			var rowTmp = EquipItemConfig._mapRowDetailChange.get(mapKeyId);
			if (rowTmp == null || rowTmp == undefined) {
				EquipItemConfig._mapRowDetailChange.put(mapKeyId, jQuery.extend(true, {}, mapKeyId, ddv.datagrid('getRows')[idx]));			
			}
			ddv.datagrid('beginEdit', idx);
			EquipItemConfig.bindEventInput();
		},
		/**
		 * edit equip item config detail 
		 * 
		 * @author liemtpt
		 * @since 13/04/2015
		 * @return
		 * @description chinh sua du lieu row
		 * */
		editAllEquipItemConfigDtl: function(indexRows){
			var ddv = $('#grid').datagrid('getRowDetail', indexRows).find('table.ddv');
			var rows = ddv.datagrid('getRows');
			for(var i = 0; i < rows.length; i++){
				ddv.datagrid('beginEdit',i);
			}
			 EquipItemConfig.bindEventInput();
			$('.child_edit_all').show();
			$('.child_save_all').show();
			$('.child_back_all').show();
			$('.child_edit').hide();
			$('.child_save').hide();
			$('.child_back').hide();
		},
		/**
		 * back equip item config detail 
		 * 
		 * @author liemtpt
		 * @since 14/04/2015
		 * @return
		 * @description tro lai
		 * */
		backEquipItemConfigDtl: function(key, idx, indexRows){
			var ddv = $('#grid').datagrid('getRowDetail', indexRows).find('table.ddv');
			var mapKeyId = indexRows.toString() +'_'+ idx.toString();
			var rowTmp = EquipItemConfig._mapRowDetailChange.get(mapKeyId);
			$(ddv).datagrid('updateRow',{
				index: idx,
				row: rowTmp
			});
			ddv.datagrid('endEdit', idx);
			var divKey = idx + '_' + indexRows;
			$('#dg_chidrent_edit_'+divKey).show();
			$('#dg_chidrent_save_'+divKey).hide();
			$('#dg_chidrent_back_'+divKey).hide();
		},
		/**
		 * back all equip item config detail 
		 * 
		 * @author liemtpt
		 * @since 13/04/2015
		 * @return
		 * @description chinh sua tat ca du lieu sub grid
		 * */
		backAllEquipItemConfigDtl: function(indexRows){
			var ddv = $('#grid').datagrid('getRowDetail', indexRows).find('table.ddv');
			$(ddv).datagrid('reload');
			/*var rows = ddv.datagrid('getRows');
			for(var i = 0; i < rows.length; i++){
				ddv.datagrid('endEdit',i);
			}
			$('.child_edit_all').show();
			$('.child_save_all').hide();
			$('.child_back_all').hide();
			$('.child_edit').show();
			$('.child_save').hide();
			$('.child_back').hide();
			*/
		},
		
		/**
		 * cap nhat gia tri cho tat ca cac hang muc trong dinh muc
		 * 
		 * @author liemtpt
		 * @since 11/04/2015
		 * 
		 * @author hunglm16
		 * @since 08/05/2015
		 * @description ra soat va update
		 * */
		saveAllEquipItemConfigDtl: function (groupId, indexRows) {
			$(".ErrorMsgStyle").html('').hide();
			$(".SuccessMsgStyle").html('').hide();
			var obj = new Object();
			var lstObj = new Array();
			var ddv = $('#grid').datagrid('getRowDetail', indexRows).find('table.ddv');
			$(ddv).datagrid('acceptChanges');
			var rows = $(ddv).datagrid('getRows');
			$('.child_edit').hide();
			$('.child_save').hide();
			$('.child_back').hide();
			var flagException = false;
			for (var i = 0, size = rows.length; i < size; i++) {
				flagException = false;
				var row = rows[i];
				var msg = '';
				if (row.fromMaterialPrice != null && isNaN(row.fromMaterialPrice.toString().trim().replace(/,/g, ''))) {
					msg = row.equipItemCode.trim() + ' phải có Đơn giá vật tư từ là kiểu số nguyên';
				}
				if (row.fromMaterialPrice != null && row.fromMaterialPrice.toString().trim().length > 0 && Number(row.fromMaterialPrice.toString().trim().replace(/,/g, '')) < 1) {
					msg = row.equipItemCode.trim() + ' phải có Đơn giá vật tư từ là kiểu số nguyên';
				}
				if (msg.length == 0 && row.toMaterialPrice != null && isNaN(row.toMaterialPrice.toString().trim().replace(/,/g, ''))) {
					msg = row.equipItemCode.trim() + ' phải có Đơn giá vật tư đến là kiểu số nguyên';
				}
				if (msg.length == 0 && row.toMaterialPrice != null && row.toMaterialPrice.toString().trim().length > 0 && Number(row.toMaterialPrice.toString().trim().replace(/,/g, '')) < 1) {
					msg = row.equipItemCode.trim() + ' phải có Đơn giá vật tư đến là kiểu số nguyên';
				}
				if (row.fromMaterialPrice != null && !isNaN(row.fromMaterialPrice.toString().trim().replace(/,/g, ''))) {
					if (msg.length == 0 && row.toMaterialPrice != null && !isNaN(row.toMaterialPrice.toString().trim().replace(/,/g, ''))) {
						if (Number(row.fromMaterialPrice.toString().trim().replace(/,/g, '')) > 0 && Number(row.toMaterialPrice.toString().trim().replace(/,/g, '')) && Number(row.fromMaterialPrice.toString().trim().replace(/,/g, '')) > Number(row.toMaterialPrice.toString().trim().replace(/,/g, ''))) {
							msg = row.equipItemCode.trim() + ' phải có Đơn giá vật tư từ nhỏ hơn hoặc bằng Đến';
						}
					}
				}
				if ((row.fromMaterialPrice == null || row.fromMaterialPrice.toString().trim().length == 0) && (row.toMaterialPrice == null || row.toMaterialPrice.toString().trim().length == 0)) {
					msg = row.equipItemCode.trim() + ' phải có Đơn giá vật tư từ hoặc bằng Đến có giá trị';
				}
				if (msg.length == 0 && row.fromWorkerPrice != null && isNaN(row.fromWorkerPrice.toString().trim().replace(/,/g, ''))) {
					msg = row.equipItemCode.trim() + ' phải có Đơn giá nhân công từ là kiểu số nguyên';
				}
				if (msg.length == 0 && row.fromWorkerPrice != null && row.fromWorkerPrice.toString().trim().length > 0 && Number(row.fromWorkerPrice.toString().trim().replace(/,/g, '')) < 1) {
					msg = row.equipItemCode.trim() + ' phải có Đơn giá nhân công từ là kiểu số nguyên';
				}
				if (msg.length == 0 && row.toWorkerPrice != null && isNaN(row.toWorkerPrice.toString().trim().replace(/,/g, ''))) {
					msg = row.equipItemCode.trim() + ' phải có Đơn giá nhân công đến là kiểu số nguyên';
				}
				if (msg.length == 0 && row.toWorkerPrice != null && row.toWorkerPrice.toString().trim().length > 0 && Number(row.toWorkerPrice.toString().trim().replace(/,/g, '')) < 1) {
					msg = row.equipItemCode.trim() + ' phải có Đơn giá nhân công đến là kiểu số nguyên';
				}
				if (row.fromWorkerPrice != null && !isNaN(row.toWorkerPrice.toString().trim().replace(/,/g, ''))) {
					if (msg.length == 0 && row.toMaterialPrice != null && !isNaN(row.toWorkerPrice.toString().trim().replace(/,/g, ''))) {
						if (Number(row.fromWorkerPrice.toString().trim().replace(/,/g, '')) > 0 && Number(row.toWorkerPrice.toString().trim().replace(/,/g, '')) && Number(row.fromWorkerPrice.toString().trim().replace(/,/g, '')) > Number(row.toWorkerPrice.toString().trim().replace(/,/g, ''))) {
							msg = row.equipItemCode.trim() + ' phải có Đơn giá nhân công từ nhỏ hơn hoặc bằng Đến';
						}
					}
				}
				if ((row.fromWorkerPrice == null || row.fromWorkerPrice.toString().trim().length == 0) && (row.toWorkerPrice == null || row.toWorkerPrice.toString().trim().length == 0)) {
					msg = row.equipItemCode.trim() + ' phải có Đơn giá nhân công từ hoặc bằng Đến có giá trị';
				}
				
				if (row.id == undefined || row.id == null || row.id < 0) {
					if (row.fromWorkerPrice == null || row.fromWorkerPrice == undefined || row.fromWorkerPrice.toString().trim().length == 0) {
						if (row.fromMaterialPrice == null || row.fromMaterialPrice == undefined || row.fromMaterialPrice.toString().trim().length == 0) {
							if (row.toWorkerPrice == null || row.toWorkerPrice == undefined || row.toWorkerPrice.toString().trim().length == 0) {
								if (row.toMaterialPrice == null || row.toMaterialPrice == undefined || row.toMaterialPrice.toString().trim().length == 0) {
									msg = '';
									flagException = true;
								}
							}
						}
					}
				}
				
				if (msg.trim().length > 0) {
					$('#errMsg').html(msg.trim()).show();
					$(ddv).datagrid('beginEdit', i);
					return;
				}
				if (!flagException) {
					obj = new Object();
					obj.equipItemId = row.equipItemId;
					obj.fromMaterialPrice = Utils.returnMoneyValue(row.fromMaterialPrice);
					obj.toMaterialPrice = Utils.returnMoneyValue(row.toMaterialPrice);
					obj.fromWorkerPrice = Utils.returnMoneyValue(row.fromWorkerPrice);
					obj.toWorkerPrice = Utils.returnMoneyValue(row.toWorkerPrice);
					lstObj.push(obj);					
				}
			}
			if (lstObj.length == 0) {
				$('#errMsg').html('Không tìm thấy Hạng mục muốn cập nhật cho định mức' + $('#grid').datagrid('getRows')[indexRows].equipItemConfigCode).show();
				if (rows.length > 0) {
					$(ddv).datagrid('beginEdit', 0);				
				}
				return;
			}
			var params = new Object();	
			if(groupId != undefined && groupId != null){
				params.id = groupId;
			}
			params.lstEquipItemConfigDtlVO = lstObj;
			params.flagCheckAll = 1;
			var dataModel = {};
			convertToSimpleObject(dataModel, params, 'equipItemConfigVO');
			Utils.addOrSaveData(dataModel, '/equip-item-config/save-equip-item-config-dtl', null,  null, function(data) {
					ddv.datagrid('reload');
					$('.SuccessMsgStyle').html("").hide();
			}, null, null, null,null);
			return false;
		},
		/**
		 * Them moi hang muc trong dinh muc
		 * 
		 * @author liemtpt
		 * @since 11/04/2015
		 * 
		 * @author hunglm16
		 * @since 07/05/2015
		 * @description Ra soat va hotfix
		 * */
		saveEquipItemConfigDtl : function (groupId, idx, indexRows) {
			$(".ErrorMsgStyle").html('').hide();
			$(".SuccessMsgStyle").html('').hide();
			var obj = new Object();
			var lstObj = new Array();
			var ddv = $('#grid').datagrid('getRowDetail', indexRows).find('table.ddv');
			$(ddv).datagrid('acceptChanges');
			var row = ddv.datagrid('getRows')[idx];
			var msg = '';
			if (row.fromMaterialPrice != null && isNaN(row.fromMaterialPrice.toString().trim().replace(/,/g, ''))) {
				msg = row.equipItemCode.trim() + ' phải có Đơn giá vật tư từ là kiểu số nguyên';
			}
			if (row.fromMaterialPrice != null && row.fromMaterialPrice.toString().trim().length > 0 && Number(row.fromMaterialPrice.toString().trim().replace(/,/g, '')) < 1) {
				msg = row.equipItemCode.trim() + ' phải có Đơn giá vật tư từ là kiểu số nguyên';
			}
			if (msg.length == 0 && row.toMaterialPrice != null && isNaN(row.toMaterialPrice.toString().trim().replace(/,/g, ''))) {
				msg = row.equipItemCode.trim() + ' phải có Đơn giá vật tư đến là kiểu số nguyên';
			}
			if (msg.length == 0 && row.toMaterialPrice != null && row.toMaterialPrice.toString().trim().length > 0 && Number(row.toMaterialPrice.toString().trim().replace(/,/g, '')) < 1) {
				msg = row.equipItemCode.trim() + ' phải có Đơn giá vật tư đến là kiểu số nguyên';
			}
			if (row.fromMaterialPrice != null && !isNaN(row.fromMaterialPrice.toString().trim().replace(/,/g, ''))) {
				if (msg.length == 0 && row.toMaterialPrice != null && !isNaN(row.toMaterialPrice.toString().trim().replace(/,/g, ''))) {
					if (Number(row.fromMaterialPrice.toString().trim().replace(/,/g, '')) > 0 && Number(row.toMaterialPrice.toString().trim().replace(/,/g, '')) && Number(row.fromMaterialPrice.toString().trim().replace(/,/g, '')) > Number(row.toMaterialPrice.toString().trim().replace(/,/g, ''))) {
						msg = row.equipItemCode.trim() + ' phải có Đơn giá vật tư từ nhỏ hơn hoặc bằng Đến';
					}
				}
			}
			if ((row.fromMaterialPrice == null || row.fromMaterialPrice.toString().trim().length == 0) && (row.toMaterialPrice == null || row.toMaterialPrice.toString().trim().length == 0)) {
				msg = row.equipItemCode.trim() + ' phải có Đơn giá vật tư từ hoặc bằng Đến có giá trị';
			}
			if (msg.length == 0 && row.fromWorkerPrice != null && isNaN(row.fromWorkerPrice.toString().trim().replace(/,/g, ''))) {
				msg = row.equipItemCode.trim() + ' phải có Đơn giá nhân công từ là kiểu số nguyên';
			}
			if (msg.length == 0 && row.fromWorkerPrice != null && row.fromWorkerPrice.toString().trim().length > 0 && Number(row.fromWorkerPrice.toString().trim().replace(/,/g, '')) < 1) {
				msg = row.equipItemCode.trim() + ' phải có Đơn giá nhân công từ là kiểu số nguyên';
			}
			if (msg.length == 0 && row.toWorkerPrice != null && isNaN(row.toWorkerPrice.toString().trim().replace(/,/g, ''))) {
				msg = row.equipItemCode.trim() + ' phải có Đơn giá nhân công đến là kiểu số nguyên';
			}
			if (msg.length == 0 && row.toWorkerPrice != null && row.toWorkerPrice.toString().trim().length > 0 && Number(row.toWorkerPrice.toString().trim().replace(/,/g, '')) < 1) {
				msg = row.equipItemCode.trim() + ' phải có Đơn giá nhân công đến là kiểu số nguyên';
			}
			if (row.fromWorkerPrice != null && !isNaN(row.toWorkerPrice.toString().trim().replace(/,/g, ''))) {
				if (msg.length == 0 && row.toMaterialPrice != null && !isNaN(row.toWorkerPrice.toString().trim().replace(/,/g, ''))) {
					if (Number(row.fromWorkerPrice.toString().trim().replace(/,/g, '')) > 0 && Number(row.toWorkerPrice.toString().trim().replace(/,/g, '')) && Number(row.fromWorkerPrice.toString().trim().replace(/,/g, '')) > Number(row.toWorkerPrice.toString().trim().replace(/,/g, ''))) {
						msg = row.equipItemCode.trim() + ' phải có Đơn giá nhân công từ nhỏ hơn hoặc bằng Đến';
					}
				}
			}
			if ((row.fromWorkerPrice == null || row.fromWorkerPrice.toString().trim().length == 0) && (row.toWorkerPrice == null || row.toWorkerPrice.toString().trim().length == 0)) {
				msg = row.equipItemCode.trim() + ' phải có Đơn giá nhân công từ hoặc bằng Đến có giá trị';
			}
			if (msg.trim().length > 0) {
				$('#errMsg').html(msg.trim()).show();
				ddv.datagrid('beginEdit', idx);
				return;
			}
			var key = idx + 1;
//			$('#dg_chidrent_edit_'+key).hide();
			obj = new Object();
			obj.equipItemId = row.equipItemId;
			obj.fromMaterialPrice = Utils.returnMoneyValue(row.fromMaterialPrice);
			obj.toMaterialPrice = Utils.returnMoneyValue(row.toMaterialPrice);
			obj.fromWorkerPrice = Utils.returnMoneyValue(row.fromWorkerPrice);
			obj.toWorkerPrice = Utils.returnMoneyValue(row.toWorkerPrice);
			lstObj.push(obj);
			var params = new Object();	
			if(groupId != undefined && groupId != null){
				params.id = groupId;
			}
			params.lstEquipItemConfigDtlVO = lstObj;
			params.flagCheckAll = 0;
			var dataModel = {};
			convertToSimpleObject(dataModel, params, 'equipItemConfigVO');
			Utils.addOrSaveData(dataModel, '/equip-item-config/save-equip-item-config-dtl', null,  null, function(data) {
					ddv.datagrid('reload');
					$('.SuccessMsgStyle').html("").hide();
			}, null, null, null, null);
			
			return false;
		},
		bindEventInput:function(){
            $('td input').each(function(i,e){
                  $(this).attr('id','inputText'+i).attr('maxlength','22').css('text-align','right');
                  Utils.bindFormatOnTextfield('inputText'+i, Utils._TF_NUMBER_DOT);
                  Utils.formatCurrencyFor('inputText'+i);
            });
            $('td input').keypress(function() {
            	Utils.onchangeFormatCurrencyKeyPress(this, 17);
            });
      },
     
     /**
  	 * Xuat excel Thiet lap dinh muc hang muc
  	 * 
  	 * @author liemtpt
  	 * @since 16/03/2015 
  	 * 
  	 * @author hunglm16
  	 * @since May 05,2015
  	 */
      exportBySearchEquipItemConfig : function() {
  		$('.SuccessMsgStyle').html('').hide();
		$('.ErrorMsgStyle').html('').hide();
		var rows = $('#grid').datagrid('getRows');
		if (rows.length == 0) {
			$('#errMsg').html('Không có dữ liệu').show();
			return;
		}
		if (EquipItemConfig._params == undefined || EquipItemConfig._params == null) {
			var fromCapacity= $('#txtFromCapacity').val().trim();
			var toCapacity = $('#txtToCapacity').val().trim();
			var status = $('#cbxStatus').val().trim();
			
			EquipItemConfig._params = {
				fromCapacity: fromCapacity,
				toCapacity: toCapacity,
				status: status,
				reportCode: $('#function_code').val()
			};
		} else {
			EquipItemConfig._params.reportCode = $('#function_code').val();
		}
  		var msg = 'Bạn có muốn xuất Excel?';
  		$.messager.confirm('Xác nhận', msg, function(r){
  			if (r){
  				ReportUtils.exportReport('/equip-item-config/exportBySearchEquipItemConfig',EquipItemConfig._params, 'errMsg');
  			}
  		});
  		
  	},
  	/**
	 * Mo dialog nhap excel thiet lap dinh muc
	 * 
	 * @author liemtpt
	 * @since 16/04/2015
	 * */
  	openDialogImportEquipItemConfig : function (){
		$('.SuccessMsgStyle').html('').hide();
		$('.ErrorMsgStyle').html('').hide();
		$('#eqItem_import_easyuiPopupImportExcel').dialog({
			title: 'Nhập excel thiết lập định mức',
			width: 465, 
			height: 'auto',
			closed: false,
			cache: false,
			modal: true,
			onOpen: function() {
				$('#fakefileExcelpc').val('');
				$('#excelFile').val('');
			},
			onClose: function() {
				$('.ErrorMsgStyle').html('').hide();
				$('.ErrorMsgStyle').html('').hide();
			}
		});
	},
	/**
	 * Import excel thiet lap dinh muc hang muc
	 * 
	 * @author liemtpt
	 * @since 16/04/2015
	 * 
	 * @author hunglm16
	 * @since May 05,2015
	 * @description Ra soat va update
	 * */
	importEquipItemConfig : function(){
		$('.ErrorMsgStyle').html('').hide();
		$('.ErrorMsgStyle').html('').hide();
		Utils.importExcelUtils(function(data) {
			EquipItemConfig.search();//Refesh lai luong tim kiem
			$('#fakefileExcelpc').val("").change();
			if (data.message != undefined && data.message != null && data.message.trim().length >0) {
				$('#errImpExcelMsg').html(data.message.trim()).change().show();
			} else {
				$('#successImpExcelMsg').html("Lưu dữ liệu thành công").show();
				var tm = setTimeout(function() {
					$('.SuccessMsgStyle').html('').hide();
					clearTimeout(tm);
					$('#eqItem_import_easyuiPopupImportExcel').dialog('close');
				 }, 1500);
			}
		}, 'importExcelFrm', 'excelFile', 'errImpExcelMsg');
	},
	
     /**
  	 * Tai tap tin excel import thiet lap dinh muc hang muc
  	 * 
  	 * @author liemtpt
  	 * @since 16/04/2015
  	 * 
  	 * @author hunglm16
  	 * @since May 04,2015
  	 * @description thay doi phuong thuc xuat tap tin excel
  	 * */
  	downloadTemplateImportEquipItemConfig: function() {
  		$('.SuccessMsgStyle').html('').hide();
  		$('.ErrorMsgStyle').html('').hide();
  		var msg = 'Bạn có muốn tải tập tin Import?';
  		$.messager.confirm('Xác nhận', msg, function(r) {
			if (r){
		  		var dataModel = {};
		  		dataModel.reportCode = $('#function_code').val();
		  		ReportUtils.exportReport('/equip-item-config/downloadTemplateImportEquipItemConfig', dataModel);
			}
		});
  	},
	/**
	 * Xu ly su kien Change Input Dung tich
	 * 
	 * @author hunglm16
  	 * @since May 09,2015
	 * */
	onchangeCapacity: function(txt) {
		var kq = $(txt).val();
		kq = kq.replace(/,/g, '').trim();
		var len = kq.length;
		if (len > 10) {
			kq = kq.substring(len - 10);
			$(txt).val(VTUtilJS.formatCurrency(kq)).change();
		}
		return;
	}
};
/*
 * END OF FILE - /web/web/resources/scripts/business/equipment/vnm.equip-item-config.js
 */

/*
 * START OF FILE - /web/web/resources/scripts/business/equipment/vnm.equiment-manage-permission.js
 */
var EquipManagePermission = {
	_params: null, /**dung cho dieu kien xuat excel theo dk search*/
	_staffId: null,
	_staffCode: null,
	_lstRoleId: null,
	_mapRole: null,
	_lstRoleUserId: null,
	searchStaff : function() {
		$('#errMsg').html('').hide();

		var params = new Object();
		params.personalCode = $('#personalCode').val().trim();
		params.personalName = $('#personalName').val().trim();
		params.personalStatus = $('#personalStatus').val();
		
		/**dung cho dieu kien xuat excel theo dk search*/		
		EquipManagePermission._params = new Object();
		EquipManagePermission._params.personalCode = $('#personalCode').val().trim();
		EquipManagePermission._params.personalName = $('#personalName').val().trim();
		EquipManagePermission._params.personalStatus = $('#personalStatus').val();

		var multiShop = $("#shop").data("kendoMultiSelect");
		var dataShop = multiShop.dataItems();
		var arrShopId = [];
		if (dataShop != undefined && dataShop != null) {
			for (var i = 0, size = dataShop.length; i < size; i++) {
				arrShopId.push(dataShop[i].shopId);
			}
		}
		params.listShopId = arrShopId.toString();
		EquipManagePermission._params.listShopId = arrShopId.toString();
		$('#gridPersonal').datagrid('reload',params);
		$('#gridPermission').datagrid('reload',new Object());
		$('#titleStaff').html('');
	},
	/**
	 * Show dialog import gan quyen cho nguoi dung
	 * @author vuongmq
	 * @date Mar 26, 2015
	 * */
	showDlgImportExcel: function(){
		$('#easyuiPopupImportExcel').show();
		$('.SuccessMsgStyle').html('').hide();
		$('.ErrorMsgStyle').hide();
		$('#easyuiPopupImportExcel').dialog({  
			width: 500,
			height: 'auto',
	        closed: false,  
	        cache: false,  
	        modal: true,
	        onOpen: function(){	 
	        	$('.easyui-dialog .BtnGeneralStyle').css('color','');
	        },
	        onClose: function(){
	        	/*EquipmentManageCatalog.searchEviction();
				$("#grid").datagrid('uncheckAll');*/
				$("#fakefilepcStaffPermission").val("");
				$("#excelFileStaffPermission").val("");				
	        }
		});
	},
	/**
	 * Import Excel gan quyen cho nguoi dung
	 * @author vuongmq
	 * @date Mar 26, 2015
	 * */
	importExcelStaffPermission: function(){
		$('.SuccessMsgStyle').html('').hide();
		$('.ErrorMsgStyle').html('').hide();
		if($('#fakefilepcStaffPermission').val().length <= 0 ){
			$('#errExcelMsg').html('Vui lòng chọn tập tin Excel').show();
			return false;
		}
		Utils.importExcelUtils(function(data){
			//EquipmentListEquipment.searchListEquipment();
			$('#fakefilepcStaffPermission').val("").change();
			if (data.message!=undefined && data.message!=null && data.message.trim().length >0){
				$('#errExcelMsg').html(data.message.trim()).change().show();
			}else{
				$('#successExcelMsg').html("Lưu dữ liệu thành công").show();
				var tm = setTimeout(function(){
					$('.SuccessMsgStyle').html('').hide();
					clearTimeout(tm);
					$('#easyuiPopupImportExcel').dialog('close');
					//$('#listEquipmentDg').datagrid("load", EquipmentStockChange._params);
				 }, 1500);
			}
		}, 'importFrmStaffPermission', 'excelFileStaffPermission', 'errExcelMsg');
		
	},
	/**
	 * xuat file excel gan quyen cho nguoi dung
	 * @author vuongmq
	 * @date March 26, 2015
	 */
	exportExcel: function(){		
		$('.SuccessMsgStyle').html('').hide();
		$('.ErrorMsgStyle').html('').hide();
		var msg ='';
		var data = $('#gridPersonal').datagrid('getRows');	
		if(data == null || data.length <=0){
			msg = "Không có dữ liệu xuất Excel";
		}
		if(msg.length > 0){
			$('#errMsg').html(msg).show();
			return false;
		}
		$.messager.confirm('Xác nhận','Bạn có muốn xuất file excel?',function(r){
			if(r){
				ReportUtils.exportReport('/equipManagePermission/exportExcelBySearchEquipStaffPermission', EquipManagePermission._params, 'errMsg');					
			}
		});		
		return false;
	},
	viewStaffPermission: function(staffId, staffCode){
		var params = new Object();
		if(staffId != undefined && staffId != null ) {
			EquipManagePermission._staffId = staffId;
			EquipManagePermission._staffCode = staffCode
		} else {
			staffId = EquipManagePermission._staffId;
			staffCode = EquipManagePermission._staffCode;
		}
		$('#panelPermission').css('display','block');
		$('#titleStaff').html(staffCode);
		params.staffId = staffId;
		params.permissionCode = $('#permissionCode').val().trim();
		params.permissionName = $('#permissionName').val().trim();
		params.permissionStatus = $('#permissionStatus').val();
		
		$('#gridPermission').datagrid('reload',params);
		$('html,body').animate({scrollTop: $('#gridPermissionContainer').offset().top}, 1500);
		$('#gridPermission').datagrid("resize");
	},
	showPopupRole: function() {
		$('.ErrorMsgStyle').html('').hide();
		$('#imgOverlay').css('position','fixed');
		var msg ='';
		if(EquipManagePermission._staffId == null || EquipManagePermission._staffId == undefined) {
			msg = "Bạn chưa chọn nhân viên!";
			$('#errMsgPermission').html(msg).show();
			return false;
		}
		VCommonJS.showDialogSearch2WithCheckbox({
	    inputs : [
	        {id:'roleCode', maxlength:50, label:'Mã quyền'},
	        {id:'roleName', maxlength:250, label:'Tên quyền'}
	    ],
	    chooseCallback : function(listObj) {
	    	console.log(listObj);
	    	if(listObj.length == 0) {
	    		msg = 'Vui lòng chọn quyền cần thêm';
	    		$('#errMsgInfo').html(msg).show();
	    	}
	    	EquipManagePermission._lstRoleId = new Array();
	        if($.isArray(listObj) && listObj.length > 0) {
	        	for(var i = 0; i < listObj.length; i++) {
	        		EquipManagePermission._lstRoleId.push(listObj[i].id);
	        	}
	        	EquipManagePermission.addRoleToUser(EquipManagePermission._lstRoleId);
	        }
	    },
	    url : '/commons/search-equipRole-show-list',
	    params : {
			staffRoleId : EquipManagePermission._staffId,
			statusRole : 1
		},
		// isCenter : true,
	    columns : [[
	        {field:'code', title:'Mã quyền', align:'left', width: 110, sortable:false, resizable:false, formatter: function(value, row, index){
		    	return Utils.XSSEncode(value);
		    }},
	        {field:'name', title:'Tên quyền', align:'left', width: 150, sortable:false, resizable:false, formatter: function(value, row, index){
		    	return Utils.XSSEncode(value);
		    }},
	        {field:'cb', checkbox:true, align:'center', width:80,sortable : false,resizable : false, formatter: function(value, row, index){
		    	return Utils.XSSEncode(value);
		    }},
	    ]]
	});
		var wHeight = $(window).height();
		var ptop = wHeight - 150;
		$('#common-dialog-search-2-textbox').dialog('move', {top :ptop});
		var html = '<p id="errMsgInfo" class="ErrorMsgStyle" style="display: none;"></p>';
		$('#common-dialog-search-2-textbox').append(html);
	},
	addRoleToUser: function(lstRoleId){
		$('#imgOverlay').css('position','fixed');
		var params = new Object();
		params.lstRoleId = lstRoleId;
		params.staffId = EquipManagePermission._staffId;

		Utils.addOrSaveData(params, '/equipManagePermission/add-permission', null, 'errMsgDialog', function(){
			setTimeout(function(){		
				$('#common-dialog-search-2-textbox').dialog('close');
				$('#gridPermission').datagrid('reload');				
			}, 1500);			

		});	

	},
	updateRoleUser: function (status) {
		$('.SuccessMsgStyle').html('').hide();
		$('.ErrorMsgStyle').html('').hide();
		$('#imgOverlay').css('position','fixed');
		var msg ='';
		var rows = $('#gridPermission').datagrid('getRows');
		if(rows == null || rows.length <=0){
			msg = "Vui lòng chọn quyền cần cập nhật!";
		}
		
		var isDupStatus = true;
		EquipManagePermission._lstRoleUserId = new Array();
		for(var i = 0; i < rows.length; i++) {
    		if(EquipManagePermission._mapRole.get(rows[i].id) != null) {
    			EquipManagePermission._lstRoleUserId.push(rows[i].equipRoleUserId);
    			if(status != rows[i].status) {
    				isDupStatus = false;
    			}
    		}
		}
		if(isDupStatus && status == 0) {
			msg = "Trường này đang bị khóa";
		}

		if(isDupStatus && status == 1) {
			msg = "Trường này đã được mở khóa";
		}

		if(EquipManagePermission._lstRoleUserId.length == 0) {
			msg = "Vui lòng chọn quyền cần cập nhật!";
		}


		if(msg.length > 0 || isDupStatus){
			$('#errMsgPermission').html(msg).show();
			return false;
		}
		//status: -1:xoa, 0:tam ngung, 1:hoat dong
		var params = new Object();
		params.lstRoleUserId = EquipManagePermission._lstRoleUserId;
		params.status = status;
		if(status == -1) {
			if(params.lstRoleUserId.length > 0) {
				$.messager.confirm('Xác nhận','Bạn có muốn xuất file excel?',function(r){
					if(r){
						Utils.addOrSaveData(params, '/equipManagePermission/update-permission', null, 'errMsgDialog', function(){
							setTimeout(function(){		
								$('#gridPermission').datagrid('reload');			
							}, 1500);			
						}, null, null, true);
					}
				});	
			} else {
				msg = "Vui lòng chọn quyền cần cập nhật!";
				$('#errMsgPermission').html(msg).show();
				return false;
			}
		} else {
			Utils.addOrSaveData(params, '/equipManagePermission/update-permission', null, 'errMsgDialog', function(rs){
					$('#gridPermission').datagrid('reload');			
							
			}, null, null, null, null, function(rf){
				if (rf != undefined && rf != null && rf.errMsg != undefined && rf.errMsg != null && rf.errMsg.trim().length > 0) {
					$('#errMsgPermission').html(rf.errMsg).show();
					$('#gridPermission').datagrid('reload');
					return false;
				}

			});	
		}


	} 
};
/*
 * END OF FILE - /web/web/resources/scripts/business/equipment/vnm.equiment-manage-permission.js
 */

/*
 * START OF FILE - /web/web/resources/scripts/business/equipment/vnm.equipment-report.js
 */
/**
 * JS thuc hien nghiep vu Bao cao thiet bi
 * 
 * @author hoanv25
 * @since  May 05, 2015
 * */
var EquipmentReport = {		
		
	/**
	* Bc 20.2 Xuat nhap ton thiet bi
	* 
	* @author thangnv31
	* @since 2016-03-16
	* */	
		exportXnttb: function() {
		var msg = '';
		$('#errMsg').html('').hide();
		if (msg.length == 0) {
			msg = Utils.getMessageOfRequireCheck('fDate','Từ ngày');
		}
		if (msg.length == 0) {
			msg = Utils.getMessageOfInvalidFormatDate('fDate','Từ ngày');
		}
		if (msg.length == 0) {
			msg = Utils.getMessageOfRequireCheck('tDate','Từ ngày');
		}
		if (msg.length == 0) {
			msg = Utils.getMessageOfInvalidFormatDate('tDate','Đến ngày');
		}
		var fDate = $('#fDate').val();
		var tDate = $('#tDate').val();
		if (!Utils.compareDate(fDate, tDate)) {
			msg = msgErr_fromdate_greater_todate;		
			$('#fDate').focus();
		}
		var currentTime = new Date();
		var month = currentTime.getMonth() + 1;
		var day = currentTime.getDate();
		var year = currentTime.getFullYear();
		if (!Utils.compareDate(fDate, day + '/' + month + '/' + year)) {
			msg = 'Từ ngày không được lớn hơn ngày hiện tại';
			$('#fDate').focus();
		}
		if (!Utils.compareDate(tDate, day + '/' + month + '/' + year)) {
			msg = 'Đến ngày không được lớn hơn ngày hiện tại';
			$('#tDate').focus();
		}
		if (msg.length > 0) {
			$('#errMsg').html(msg).show();
			return false;
		}
		var dataModel = new Object();		
		dataModel.fromDate = $('#fDate').val().trim();
		dataModel.toDate = $('#tDate').val().trim();
		ReportUtils.exportReport('/report/equipment/xnttb/export', dataModel);
	},
		
	/**
	 * Load danh sach nhom thiet bi theo don vi chon
	 * @author hoanv25
	 * @since May 05,2015
	 **/
	loadComboEquipGroup:function(url){
		$("#equipGroup").kendoMultiSelect({
			dataSource: {
				transport: {
	                read: {
	                    dataType: "json",
	                    url: url
	                }
	            }
			},
			filter: "contains",
	        dataTextField: "equipGroupCode",
	        dataValueField: "equipGroupId",
			itemTemplate: function(data, e, s, h, q) {
				return '<div class="tree-vnm" node-id="'+Utils.XSSEncode(data.equipGroupId)+'" style="width:230px"><span class="tree-vnm-text">'+Utils.XSSEncode(data.equipGroupCode) +'-' + Utils.XSSEncode(data.equipGroupName)+'</span></div>';	
			},
	        tagTemplate:  '#: data.equipGroupCode #',
	        value: [$('#equipGroupId').val()]
	    });	 
	},
	
	/**
	 * Load danh sach nhom thiet bi theo nam thay doi
	 * @author hoanv25
	 * @since May 05,2015
	 * 
	 * @author hunglm16
	 * @since May 15,2015
	 * @description ra soat va bo sung
	 **/
	changeYearByOnload: function() {
		$('#errMsg').hide();
		var msg = '';
		var year = $('#year').val().trim();
		if(msg.length == 0){
			msg = Utils.getMessageOfRequireCheck('year', 'Năm');
		}
		if (msg.length == 0) {
			msg = Utils.getMessageOfSpecialCharactersValidateEx('year', 'Năm', Utils._TF_NUMBER);
		}
		if(msg.length >0){
			$('#errMsg').html(msg).show();
			return false;
		}
		var params = new Object();		
		params.year = $('#year').val().trim();
		Utils.getJSONDataByAjaxNotOverlay(params, '/report/equipment/getPeriod', function(data){						
			$('#period').html("").change();
			if (data != undefined && data != null && data.rows != undefined && data.rows != null) {
				for (var i = 0, size = data.rows.length; i < size; i++) {
					var row = '';
					if (data.rows[i].status && data.rows[i].status == 2) {
						row = '<option value="'+data.rows[i].id+'" selected="selected">' + data.rows[i].name + ' 222222xxx</option>';
					} else {
						row = '<option value="'+data.rows[i].id+'">' + data.rows[i].name + '</option>';
					}
					$('#period').append(row);
				}
			}
			$('#period').change().show();
		}, null, null);	
	},
	
	/**
	 * Bao cao thu hoi thiet bi
	 * @author hoanv25
	 * @since May 05,2015
	 **/
	exportEQ_3_1_7_1: function() {
		$('#errMsg').hide();
		var msg = '';
		var shopId = $('#shopId').val(); // shop select combobox
		if (shopId == null || shopId.length <= 0) {
			msg = format(msgErr_required_field, 'Đơn vị');
		}
		var fDate = $('#fDate').val();
		var tDate =$('#tDate').val();
		
		if(msg.length == 0 && $('#fDate').val().trim().length == 0){
			msg = 'Giá trị "từ ngày" chưa được nhập';
			err = '#fDate';
		}
		
		if(msg.length == 0 && $('#fDate').val().trim().length>0 && !Utils.isDate($('#fDate').val(),'/') && $('#fDate').val() != '__/__/____' ){
			msg = 'Từ ngày không tồn tại hoặc không đúng định dạng dd/mm/yyyy.';
			err = '#fDate';
		}
		
		if(msg.length == 0 && $('#tDate').val().trim().length == 0){
			msg = 'Giá trị "đến ngày" chưa được nhập';
			err = '#tDate';
		}
		
		if(msg.length == 0 && $('#tDate').val().trim().length>0 && !Utils.isDate($('#tDate').val(),'/') && $('#tDate').val() != '__/__/____'){
			msg = 'Đến ngày không tồn tại hoặc không đúng định dạng dd/mm/yyyy.';
			err = '#tDate';
		}
		if(msg.length == 0 && fDate.length > 0 && tDate.length > 0 && !Utils.compareDate(fDate, tDate)) {
			msg = 'Khoảng thời gian từ ngày - đến ngày không hợp lệ. Giá trị đến ngày phải cùng hoặc sau giá trị từ ngày';
		}
		
		if (msg.length > 0) {
			$('#errMsg').html(msg).show();
			return false;
		}
		var dataModel = new Object();
		dataModel.lstShopId = shopId; // combobox chon 1
		// dataModel.lstEquipId = lstEquipId.join(',');
		var multidataGroupEquip = $("#equipGroup").data("kendoMultiSelect");
		var dataGroup = multidataGroupEquip.dataItems();
		var lstEquipId = null;
		if (dataGroup != null && dataGroup.length > 0) {
			lstEquipId = dataGroup[0].id;
			for (var i = 1, size = dataGroup.length; i < size; i++) {
				lstEquipId += "," + dataGroup[i].id;
			}
			dataModel.lstEquipId = lstEquipId;
		}
		var multidataCategoryEquip = $("#categoryEquip").data("kendoMultiSelect");
		var dataCategory = multidataCategoryEquip.dataItems();
		if (dataCategory != null && dataCategory.length > 0) {
			var categoryIdMultil = dataCategory[0].id;
			for (var i = 1, size = dataCategory.length; i < size; i++) {
				categoryIdMultil += "," + dataCategory[i].id;
			}
			dataModel.categoryIdMultil = categoryIdMultil;
		}
		dataModel.shopId = $('#curShopId').val();
		dataModel.fromDate = fDate;
		dataModel.toDate = tDate;
		ReportUtils.exportReport('/report/equipment/wvkd03f9/export', dataModel);
	},

	/**
	 * Xuat bao bao thiet bi 3.1.1.8 danh sach tu dong tu mat sua chua theo ky
	 * @author hoanv25
	 * @since May 06,2015
	 **/
	/**
	 * modified
	 * @author vuongmq
	 * @since 07/04/2016
	 **/
	exportEQ_3_1_1_8: function() {
		$('.ErrorMsgStyle').html('').hide();
		var msg = '';
		var shopId = $('#shopId').val(); // shop select combobox
		if (shopId == null || shopId.length <= 0) {
			msg = format(msgErr_required_field, 'Đơn vị');
		}
		if (msg.length == 0) {
			msg = Utils.getMessageOfRequireCheck('fDate', 'Từ ngày');
		}
		if (msg.length == 0) {
			msg = Utils.getMessageOfInvalidFormatDate('fDate', 'Từ ngày');
		}
		if (msg.length == 0) {
			msg = Utils.getMessageOfRequireCheck('tDate', 'Đến ngày');
		}
		if (msg.length == 0) {
			msg = Utils.getMessageOfInvalidFormatDate('tDate', 'Đến ngày');
		}
		var fromDate = $('#fDate').val();
		var toDate = $('#tDate').val();
		if (msg.length == 0 && !Utils.compareDate(fromDate, toDate)) {
			msg = 'Từ ngày không được lớn hơn đến ngày. Vui lòng nhập lại';
			$('#fDate').focus();
		}
		var curDate = ReportUtils.getCurrentDateString();
		if (msg.length == 0 && !Utils.compareDate(toDate, curDate)) {
			msg = 'Đến ngày không được lớn hơn ngày hiện tại. Vui lòng nhập lại';
			$('#tDate').focus();
		}
		if (msg.length > 0) {
			$('#errMsg').html(msg).show();
			return false;
		}
		var dataModel = new Object();
		dataModel.lstShopId = shopId; // combobox chon 1
		var multidataGroupEquip = $("#equipGroup").data("kendoMultiSelect");
		var dataGroup = multidataGroupEquip.dataItems();
		var groupMultil = null;
		if (dataGroup != null && dataGroup.length > 0) {
			groupMultil = dataGroup[0].id;
			for (var i = 1, size = dataGroup.length; i < size; i++) {
				groupMultil += "," + dataGroup[i].id;
			}
			dataModel.groupMultil = groupMultil;
		}
		var multidataCategoryEquip = $("#categoryEquip").data("kendoMultiSelect");
		var dataCategory = multidataCategoryEquip.dataItems();
		if (dataCategory != null && dataCategory.length > 0) {
			var categoryIdMultil = dataCategory[0].id;
			for (var i = 1, size = dataCategory.length; i < size; i++) {
				categoryIdMultil += "," + dataCategory[i].id;
			}
			dataModel.categoryIdMultil = categoryIdMultil;
		}
		dataModel.shopId = $('#curShopId').val();
		dataModel.fromDate = fromDate;
		dataModel.toDate = toDate;
		ReportUtils.exportReport('/report/equipment/wvkd03f10/export', dataModel);
	},
	
	/**
	 * Xuat bao cao thiet bi 3.1.1.10 danh sach tong hop tu dong tu mat theo ky
	 * @author hoanv25
	 * @since May 13,2015
	 **/
	exportEQ_3_1_1_10: function() {
		$('#errMsg').hide();
		var msg = '';
		var shopId = $('#shopId').val();
		if (shopId.trim().length == 0 || shopId.trim() == activeType.ALL) {
			msg = format(msgErr_required_field, 'Đơn vị');
		}
		var fDate = $('#fDate').val();
		var tDate =$('#tDate').val();
		if(msg.length == 0 && $('#fDate').val().trim().length>0 && !Utils.isDate($('#fDate').val(),'/') && $('#fDate').val() != '__/__/____' ){
			msg = 'Từ ngày không tồn tại hoặc không đúng định dạng dd/mm/yyyy.';
			err = '#fDate';
		}
		if(msg.length == 0 && $('#tDate').val().trim().length>0 && !Utils.isDate($('#tDate').val(),'/') && $('#tDate').val() != '__/__/____'){
			msg = 'Đến ngày không tồn tại hoặc không đúng định dạng dd/mm/yyyy.';
			err = '#tDate';
		}
		if(msg.length == 0 && fDate.length > 0 && tDate.length > 0 && !Utils.compareDate(fDate, tDate)) {
			msg = 'Từ ngày tạo phải là ngày trước hoặc cùng ngày với đến ngày tạo';
		}
		if (msg.length > 0) {
			$('#errMsg').html(msg).show();
			return false;
		}
		var dataModel = new Object();
		dataModel.shopId = shopId;
		dataModel.fromDate = fDate;
		dataModel.toDate = tDate;
		var multidataGroupEquip = $("#equipGroup").data("kendoMultiSelect");
		var dataGroup = multidataGroupEquip.dataItems();
		var lstEquipId = null;
		if(dataGroup != null && dataGroup.length >0 ){
			lstEquipId = dataGroup[0].id;
			for(var i = 1, size = dataGroup.length; i < size; i++){
				lstEquipId += "," + dataGroup[i].id;
			}
			dataModel.lstEquipId = lstEquipId;
		}
		var multidataCategoryEquip = $("#categoryEquip").data("kendoMultiSelect");
		var dataCategory = multidataCategoryEquip.dataItems();
		if(dataCategory != null && dataCategory.length > 0 ) {
			var categoryIdMultil = dataCategory[0].id;
			for (var i = 1, size = dataCategory.length; i < size; i++) {
				categoryIdMultil += "," + dataCategory[i].id;
			}
			dataModel.categoryIdMultil = categoryIdMultil;
		}
		dataModel.reportCode= $('#function_code').val(), 
		ReportUtils.exportReport('/report/equipment/wvkd03f12/export', dataModel);
	},
	
	/**
	 * Xuat bao cao thiet bi 3.1.1.1 danh sach tong hop kiem ke tu dong tu mat theo ky
	 * @author hoanv25
	 * @since May 20,2015
	 **/
	exportEQ_3_1_1_1: function() {
		$('#errMsg').hide();
		var msg = '';
		var multiShop = $("#shop").data("kendoMultiSelect");
		var dataShop = null;
		if (multiShop != null) {
			dataShop = multiShop.dataItems();
		}
		if (dataShop == null || dataShop.length <= 0) {
			msg = format(msgErr_required_field, 'Đơn vị');
		}	
		if(msg.length == 0){
			msg = Utils.getMessageOfRequireCheck('year', 'Năm');
		}
		if (msg.length == 0) {
			msg = Utils.getMessageOfSpecialCharactersValidateEx('year', 'Năm',Utils._TF_NUMBER);
		}
		if (msg.length == 0) {
			if ($('#period').val() == undefined || $('#period').val() == -1) {
				msg = "Không có kỳ xuất báo cáo";
			}
		}
		if (msg.length == 0 && $('#statistic').val().trim() == -1) {
			msg = "Vui lòng chọn Kiểm kê";
		}

		if (msg.length > 0) {
			$('#errMsg').html(msg).show();
			return false;
		}
		var dataModel = new Object();
		var lstShopId = [];
		var lstEquipId = [];
		for (var i = 0; i < dataShop.length; i++) {
			lstShopId.push(dataShop[i].id);
		}
		dataModel.lstShopId = lstShopId.join(',');
		dataModel.shopId = $('#curShopId').val();
		dataModel.period = $('#period').val().trim();
		if ($('#statistic').val().trim() != -1) {
			dataModel.statisticRecord = $('#statistic').val().trim();
		}
		dataModel.formatType = $('input[type=radio]:checked').val();
		var ck = $('input[type=radio]:checked').val();
		if (ck!= undefined && ck!=null && 'XLS'== ck) {
			ReportUtils.exportReport('/report/equipment/kk11/export', dataModel);
		} else {
			ReportUtils.exportReport('/report/equipment/kk11/pdf', dataModel);
		}
	},
	/**
	 * Xuat bao cao hinh anh kiem ke kk1.2
	 * @author phuongvm
	 * @since 18/09/2015
	 **/
	exportKK1_2: function() {
		var msg = '';
		$('#errMsg').html('').hide(); 
		
		var shopKendo = $("#shop").data("kendoMultiSelect");
		var lstShopId = shopKendo.value();
		var shopCttKendo = $("#cttb").data("kendoMultiSelect");
		var lstCTTId = shopCttKendo.value();
		if (lstShopId.length <= 0) {
			msg = format(msgErr_required_field, 'Đơn vị');
		}
		
		if (msg.length == 0) {
			msg = Utils.getMessageOfRequireCheck('fDate', 'Ngày chụp từ');
		}
		if (msg.length == 0) {
			msg = Utils.getMessageOfRequireCheck('tDate', 'Ngày chụp đến');
		}
		if (msg.length == 0) {
			msg = Utils.getMessageOfInvalidFormatDateNew('fDate', 'Ngày chụp từ');
		}
		if (msg.length == 0) {
			msg = Utils.getMessageOfInvalidFormatDateNew('tDate', 'Ngày chụp đến');
		}
		var fDate = $('#fDate').val().trim();
		var tDate = $('#tDate').val().trim();
		var tmpFDate = '';
		with (arr = fDate.split('/')) {
			tmpFDate = arr[0] + '/' + (Number(arr[1]) - 1) + '/' + arr[2];
		}
		var tmpTDate = '';
		with (arr = tDate.split('/')) {
			tmpTDate = arr[0] + '/' + (Number(arr[1]) - 1) + '/' + arr[2];
		}

		if (!Utils.compareDateNew(tmpFDate, tmpTDate)) {
			msg = msgErr_fromdate_greater_todate;
			$('#fDate').focus();
		}
		var currentTime = new Date();
		var month = currentTime.getMonth() + 1;
		var day = currentTime.getDate();
		var year = currentTime.getFullYear();
		var hour = currentTime.getHours();
		var minute = currentTime.getMinutes();
		if (month < 10) {
			month = '0' + month;
		}
		if (day < 10) {
			day = '0' + day;
		}
		if (hour < 10) {
			hour = '0' + hour;
		}
		if (minute < 10) {
			minute = '0' + minute;
		}
		if (!Utils.compareDateNew(tmpTDate, day + '/' + month + '/' + year + ' 23:59')) {
			msg = 'Ngày chụp đến phải là ngày trước hoặc cùng với ngày hiện tại';
			$('#fDate').focus();
		}
		
		if (msg.length > 0) {
			$('#errMsg').html(msg).show();
			return false;
		}
		
		var dataModel = new Object();

		dataModel.strListShopId = lstShopId.toString().trim();
		dataModel.staffCode = $('#staffCode').val().trim(); 
		dataModel.fromDate = fDate;
		dataModel.toDate = tDate;
		if (lstCTTId != undefined && lstCTTId != null && lstCTTId != "") {
			dataModel.lstCheck = lstCTTId.toString().trim();
		}
		ReportUtils.exportReport('/report/equipment/kk12/export', dataModel);
	},
	/**
	 * Xuat bao cao kiem ke kk1.3
	 * @author tamvnm
	 * @since Sep 29 2015
	 **/
	exportKK1_3: function() {
		var msg = '';
		$('#errMsg').html('').hide(); 
		var shopKendo = $("#shop").data("kendoMultiSelect");
		var lstShopId = shopKendo.value();
		if (lstShopId.length <= 0) {
			msg = format(msgErr_required_field, 'Đơn vị');
		}
		if (msg.length == 0) {
			msg = Utils.getMessageOfRequireCheck('fDate', 'Từ ngày');
		}
		if (msg.length == 0) {
			msg = Utils.getMessageOfInvalidFormatDate('fDate', 'Từ ngày');
		}
		if (msg.length == 0) {
			msg = Utils.getMessageOfRequireCheck('tDate', 'Đến ngày');
		}
		if (msg.length == 0) {
			msg = Utils.getMessageOfInvalidFormatDate('tDate', 'Đến ngày');
		}
		var fDate = $('#fDate').val();
		var tDate = $('#tDate').val();
		var curDate = ReportUtils.getCurrentDateString();
		if (msg.length == 0 && !Utils.compareDate(fDate, curDate)) {
			msg = 'Từ ngày phải nhỏ hơn hoặc bằng ngày hiện tại';
			$('#fDate').focus();
		}
		// if(msg.length == 0 && !Utils.compareDate(tDate, curDate)){
		// 	msg = 'Đến ngày phải nhỏ hơn hoặc bằng ngày hiện tại';
		// 	$('#tDate').focus();
		// }
		if (!Utils.compareDate(fDate, tDate)) {
			msg = 'Từ ngày phải nhỏ hơn đến ngày.';
			$('#fDate').focus();
		}
		if (msg.length > 0) {
			$('#errMsg').html(msg).show();
			return false;
		}
		
		var dataModel = new Object();
		dataModel.strListShopId = lstShopId.toString().trim();
		dataModel.staffCode = $('#staffCode').val().trim();
		dataModel.statisticCode = $('#statisticCode').val().trim();
		dataModel.fromDate = $('#fDate').val();
		dataModel.toDate = $('#tDate').val();
		if ($('#chkIsStop').length > 0) {
			dataModel.isStop = $('#chkIsStop').is(':checked');
		}
		ReportUtils.exportReport('/report/equipment/kk13/export', dataModel);
	},

	/**
	 * Xuat bao bao thiet bi 3.1.1.5; bao cao mat tu theo ky
	 * @author vuongmq
	 * @since 18/05/2015
	 **/
	exportBCMATTU1_5: function() {
		$('#errMsg').hide();
		var msg = '';
		var shopId = $('#shopId').val(); // shop select combobox
		if (shopId == null || shopId.length <= 0) {
			msg = format(msgErr_required_field, 'Đơn vị');
		}
		var fDate = $('#fDate').val();
		var tDate =$('#tDate').val();
		
		if(msg.length == 0 && $('#fDate').val().trim().length == 0){
			msg = 'Giá trị "từ ngày" chưa được nhập';
			err = '#fDate';
		}
		
		if(msg.length == 0 && $('#fDate').val().trim().length>0 && !Utils.isDate($('#fDate').val(),'/') && $('#fDate').val() != '__/__/____' ){
			msg = 'Từ ngày không tồn tại hoặc không đúng định dạng dd/mm/yyyy.';
			err = '#fDate';
		}
		
		if(msg.length == 0 && $('#tDate').val().trim().length == 0){
			msg = 'Giá trị "đến ngày" chưa được nhập';
			err = '#tDate';
		}
		
		if(msg.length == 0 && $('#tDate').val().trim().length>0 && !Utils.isDate($('#tDate').val(),'/') && $('#tDate').val() != '__/__/____'){
			msg = 'Đến ngày không tồn tại hoặc không đúng định dạng dd/mm/yyyy.';
			err = '#tDate';
		}
		if(msg.length == 0 && fDate.length > 0 && tDate.length > 0 && !Utils.compareDate(fDate, tDate)) {
			msg = 'Khoảng thời gian từ ngày - đến ngày không hợp lệ. Giá trị đến ngày phải cùng hoặc sau giá trị từ ngày';
		}
		// khong can kiem tra vi lay tat cac cac nhom thiet bi
		/*if (dataCustomer == null || (dataCustomer != null && dataCustomer.length <= 0)) {
			msg = format(msgErr_required_field, 'Nhóm thiết bị');
		}*/
		if (msg.length > 0) {
			$('#errMsg').html(msg).show();
			return false;
		}
		var dataModel = new Object();
		// var lenCus = dataCustomer.length;
		var multidataGroupEquip = $("#equipGroup").data("kendoMultiSelect");
		var dataGroup = multidataGroupEquip.dataItems();
		var lstEquipId = null;
		if (dataGroup != null && dataGroup.length > 0) {
			lstEquipId = dataGroup[0].id;
			for (var i = 1, size = dataGroup.length; i < size; i++) {
				lstEquipId += "," + dataGroup[i].id;
			}
			dataModel.lstEquipId = lstEquipId;
		}

		var multidataCategoryEquip = $("#categoryEquip").data("kendoMultiSelect");
		var dataCategory = multidataCategoryEquip.dataItems();
		if (dataCategory != null && dataCategory.length > 0) {
			var categoryIdMultil = dataCategory[0].id;
			for (var i = 1, size = dataCategory.length; i < size; i++) {
				categoryIdMultil += "," + dataCategory[i].id;
			}
			dataModel.categoryIdMultil = categoryIdMultil;
		}
		// for (var i = 0; i < lenCus; i++) {
		// 	lstEquipId.push(dataCustomer[i].equipGroupId);
		// }
		dataModel.lstShopId = shopId; // combobox chon 1
		// dataModel.lstEquipId = lstEquipId.join(',');
		dataModel.shopId = $('#curShopId').val();
		dataModel.fromDate = fDate;
		dataModel.toDate = tDate;
		ReportUtils.exportReport('/report/equipment/bcmattu1_5/export', dataModel);
	},

	/**
	 * 3.1.1.3	[TL1.1] Danh sách tủ đông, tủ mát thanh lý theo kỳ 
	 * @author tamvnm
	 * @since 15/05/2015
	 **/
	exportTL1_1: function() {
		$('#errMsg').hide();
		var msg = '';
		var shopId = $('#shopId').val();
		if (shopId.trim().length == 0 || shopId.trim() == activeType.ALL) {
			msg = format(msgErr_required_field, 'Đơn vị');
		}
		var fDate = $('#fDate').val();
		var tDate =$('#tDate').val();
		if(msg.length == 0 && $('#fDate').val().trim().length>0 && !Utils.isDate($('#fDate').val(),'/') && $('#fDate').val() != '__/__/____' ){
			msg = 'Từ ngày không tồn tại hoặc không đúng định dạng dd/mm/yyyy.';
			err = '#fDate';
		}
		if(msg.length == 0 && $('#tDate').val().trim().length>0 && !Utils.isDate($('#tDate').val(),'/') && $('#tDate').val() != '__/__/____'){
			msg = 'Đến ngày không tồn tại hoặc không đúng định dạng dd/mm/yyyy.';
			err = '#tDate';
		}
		if(msg.length == 0 && fDate.length > 0 && tDate.length > 0 && !Utils.compareDate(fDate, tDate)) {
			msg = 'Từ ngày tạo phải là ngày trước hoặc cùng ngày với đến ngày tạo';
		}
		if (msg.length > 0) {
			$('#errMsg').html(msg).show();
			return false;
		}
		var dataModel = new Object();
		dataModel.shopId = shopId;
		dataModel.fromDate = fDate;
		dataModel.toDate = tDate;
		var multidataGroupEquip = $("#equipGroup").data("kendoMultiSelect");
		var dataGroup = multidataGroupEquip.dataItems();
		var lstEquipId = null;
		if(dataGroup != null && dataGroup.length >0 ){
			lstEquipId = dataGroup[0].id;
			for(var i = 1, size = dataGroup.length; i < size; i++){
				lstEquipId += "," + dataGroup[i].id;
			}
			dataModel.lstEquipId = lstEquipId;
		}
		var multidataCategoryEquip = $("#categoryEquip").data("kendoMultiSelect");
		var dataCategory = multidataCategoryEquip.dataItems();
		if(dataCategory != null && dataCategory.length > 0 ) {
			var categoryIdMultil = dataCategory[0].id;
			for (var i = 1, size = dataCategory.length; i < size; i++) {
				categoryIdMultil += "," + dataCategory[i].id;
			}
			dataModel.categoryIdMultil = categoryIdMultil;
		}
		dataModel.reportCode= $('#function_code').val(), 
		ReportUtils.exportReport('/report/equipment/tl1_1/export', dataModel);
	},

	/**
	 * 3.1.1.9	[WV-KD-03-F11] Danh sách khách hàng mượn tủ 
	 * @author tamvnm
	 * @since 20/05/2015
	 **/
	exportWVKD03F11: function() {
		$('#errMsg').hide();
		var msg = '';
		var multiShop = $("#shop").data("kendoMultiSelect");
		var dataShop = null;
		if (multiShop != null) {
			dataShop = multiShop.dataItems();
		}
		if (dataShop == null || dataShop.length <= 0) {
			msg = format(msgErr_required_field, 'Đơn vị');
		}
		var typeGG = $('#infoExport').val();
		var fromDate = '';
		var toDate = '';
		if (activeType.RUNNING == typeGG) {
			fromDate = $('#fromDate').val();
			if (msg.length == 0 && fromDate != '' && !Utils.isDate(fromDate)) {
				msg = 'Từ ngày không tồn tại hoặc không đúng định dạng dd/mm/yyyy.';
				$('#fromDate').focus();
			}
			toDate = $('#toDate').val();
			if (msg.length == 0 && toDate != '' && !Utils.isDate(toDate)) {
				msg = 'Đến ngày không tồn tại hoặc không đúng định dạng dd/mm/yyyy.';
				$('#toDate').focus();
			}
			if (msg.length == 0 && !Utils.compareDate(fromDate, toDate)) {
				msg = 'Từ ngày không được lớn hơn đến ngày. Vui lòng nhập lại';
				$('#fromDate').focus();
			}
			var curDate = ReportUtils.getCurrentDateString();
			if (msg.length == 0 && !Utils.compareDate(toDate, curDate)) {
				msg = 'Đến ngày không được lớn hơn ngày hiện tại. Vui lòng nhập lại';
				$('#toDate').focus();
			}
		}
		if (msg.length > 0) {
			$('#errMsg').html(msg).show();
			return false;
		}
		var dataModel = new Object();
		var lstShopId = [];
		for (var i = 0; i < dataShop.length; i++) {
			lstShopId.push(dataShop[i].shopId);
		}
		dataModel.lstShopId = lstShopId.join(',');
		dataModel.typeGG = typeGG;
		dataModel.fromDate = fromDate;
		dataModel.toDate = toDate;
		dataModel.shopId = $('#curShopId').val();
		ReportUtils.exportReport('/report/equipment/wvkd03f11/export', dataModel);
	},
	
	/**
	 * Xuat baocao 3.1.1.4 [SC1.1] Báo cáo phiếu sửa chữa chưa thực hiện
	 * @author Datpv4
	 * @since May 05,2015
	 **/
	/**
	 * modified
	 * @author vuongmq
	 * @since 04/04/2016
	 **/
	exportBCPSCCHT: function() {
		$('.ErrorMsgStyle').html('').hide();
		var msg = '';
		var shopId = $('#shopId').val(); // shop select combobox
		if (shopId == null || shopId.length <= 0) {
			msg = format(msgErr_required_field, 'Đơn vị');
		}
		if (msg.length == 0) {
			msg = Utils.getMessageOfRequireCheck('fDate', 'Từ ngày');
		}
		if (msg.length == 0) {
			msg = Utils.getMessageOfInvalidFormatDate('fDate', 'Từ ngày');
		}
		if (msg.length == 0) {
			msg = Utils.getMessageOfRequireCheck('tDate', 'Đến ngày');
		}
		if (msg.length == 0) {
			msg = Utils.getMessageOfInvalidFormatDate('tDate', 'Đến ngày');
		}
		var fromDate = $('#fDate').val();
		var toDate = $('#tDate').val();
		if (msg.length == 0 && !Utils.compareDate(fromDate, toDate)) {
			msg = 'Từ ngày không được lớn hơn đến ngày. Vui lòng nhập lại';
			$('#fDate').focus();
		}
		var curDate = ReportUtils.getCurrentDateString();
		if (msg.length == 0 && !Utils.compareDate(toDate, curDate)) {
			msg = 'Đến ngày không được lớn hơn ngày hiện tại. Vui lòng nhập lại';
			$('#tDate').focus();
		}
		if (msg.length > 0) {
			$('#errMsg').html(msg).show();
			return false;
		}
		var dataModel = new Object();
		var multidataGroupEquip = $("#equipGroup").data("kendoMultiSelect");
		var dataGroup = multidataGroupEquip.dataItems();
		var groupMultil = null;
		if (dataGroup != null && dataGroup.length > 0) {
			groupMultil = dataGroup[0].id;
			for (var i = 1, size = dataGroup.length; i < size; i++) {
				groupMultil += "," + dataGroup[i].id;
			}
			dataModel.groupMultil = groupMultil;
		}

		var multidataCategoryEquip = $("#categoryEquip").data("kendoMultiSelect");
		var dataCategory = multidataCategoryEquip.dataItems();
		if (dataCategory != null && dataCategory.length > 0) {
			var categoryIdMultil = dataCategory[0].id;
			for (var i = 1, size = dataCategory.length; i < size; i++) {
				categoryIdMultil += "," + dataCategory[i].id;
			}
			dataModel.categoryIdMultil = categoryIdMultil;
		}
		dataModel.lstShopId = shopId; // combobox chon 1
		dataModel.shopId = $('#curShopId').val();
		dataModel.fromDate = fromDate;
		dataModel.toDate = toDate;
		ReportUtils.exportReport('/report/equipment/cth14/export', dataModel);
	},
	
	/**
	 * Xuat baocao 3.1.1.6 [WV-KD-03-F5] Bao cao muon thiet bi
	 * @author Datpv4
	 * @since May 13,2015
	 **/
	exportEQ_3_1_1_6: function() {
		$('#errMsg').hide();
		var msg = '';
		var shopId = $('#shopId').val(); // shop select combobox
		if (shopId == null || shopId.length <= 0) {
			msg = format(msgErr_required_field, 'Đơn vị');
		}
		
		var fDate = $('#fDate').val();
		var tDate =$('#tDate').val();
		
		if(msg.length == 0 && $('#fDate').val().trim().length == 0){
			msg = 'Giá trị "từ ngày" chưa được nhập';
			err = '#fDate';
		}
		
		if(msg.length == 0 && $('#fDate').val().trim().length>0 && !Utils.isDate($('#fDate').val(),'/') && $('#fDate').val() != '__/__/____' ){
			msg = 'Từ ngày không tồn tại hoặc không đúng định dạng dd/mm/yyyy.';
			err = '#fDate';
		}
		
		if(msg.length == 0 && $('#tDate').val().trim().length == 0){
			msg = 'Giá trị "đến ngày" chưa được nhập';
			err = '#tDate';
		}
		
		if(msg.length == 0 && $('#tDate').val().trim().length>0 && !Utils.isDate($('#tDate').val(),'/') && $('#tDate').val() != '__/__/____'){
			msg = 'Đến ngày không tồn tại hoặc không đúng định dạng dd/mm/yyyy.';
			err = '#tDate';
		}
		if(msg.length == 0 && fDate.length > 0 && tDate.length > 0 && !Utils.compareDate(fDate, tDate)) {
			msg = 'Khoảng thời gian từ ngày - đến ngày không hợp lệ. Giá trị đến ngày phải cùng hoặc sau giá trị từ ngày';
		}
		
		if (msg.length > 0) {
			$('#errMsg').html(msg).show();
			return false;
		}
		
		var dataModel = new Object();
		dataModel.lstShopId = shopId;
		dataModel.shopId = $('#curShopId').val();
		dataModel.fromDate = fDate;
		dataModel.toDate = tDate;
		// dataModel.lstEquipId = lstEquipId.join(',');
		var multidataGroupEquip = $("#equipGroup").data("kendoMultiSelect");
		var dataGroup = multidataGroupEquip.dataItems();
		var lstEquipId = null;
		if(dataGroup != null && dataGroup.length >0 ){
			lstEquipId = dataGroup[0].id;
			for(var i = 1, size = dataGroup.length; i < size; i++){
				lstEquipId += "," + dataGroup[i].id;
			}
			dataModel.lstEquipId = lstEquipId;
		}
		var multidataCategoryEquip = $("#categoryEquip").data("kendoMultiSelect");
		var dataCategory = multidataCategoryEquip.dataItems();
		if(dataCategory != null && dataCategory.length > 0 ) {
			var categoryIdMultil = dataCategory[0].id;
			for (var i = 1, size = dataCategory.length; i < size; i++) {
				categoryIdMultil += "," + dataCategory[i].id;
			}
			dataModel.categoryIdMultil = categoryIdMultil;
		}
		dataModel.reportCode= $('#function_code').val(), 
		
		ReportUtils.exportReport('/report/equipment/wvkd03f5/export', dataModel);
	},
	
	/**
	 * Bao cao nhap xuat ton tu mat, tu dong ky [ten ky]
	 * 
	 * @author hunglm16
	 * @since May 18, 2015
	 * */
	exportXNT1D1: function(){
		var msg = '';
		$('#errMsg').html('').hide();
		var periodId = $('#period').val();
		if (isNaN(periodId) || Number(periodId) < 0) {
			msg = 'Kỳ không hợp lệ';
		}
		if (msg.length > 0) {
			$('#errMsg').html(msg).show();
			return false;
		}

		var multiStockEquip = $("#stockEquip").data("kendoMultiSelect");
		var dataStock = multiStockEquip.dataItems();

		var multidataGroupEquip = $("#groupEquip").data("kendoMultiSelect");
		var dataGroup = multidataGroupEquip.dataItems();

		var multidataCategoryEquip = $("#categoryEquip").data("kendoMultiSelect");
		var dataCategory = multidataCategoryEquip.dataItems();

		var dataModel = new Object();

		if (dataStock != null && dataStock.length > 0) {
			var stockIdMultil = dataStock[0].equipStockId;
			for (var i = 1, size = dataStock.length; i < size; i++) {
				stockIdMultil += "," + dataStock[i].equipStockId;
			}
			dataModel.stockMultil = stockIdMultil;
		}

		if (dataGroup != null && dataGroup.length > 0) {
			var groupIdMultil = dataGroup[0].id;
			for (var i = 1, size = dataGroup.length; i < size; i++) {
				groupIdMultil += "," + dataGroup[i].id;
			}
			dataModel.groupMultil = groupIdMultil;
		}
		if (dataCategory != null && dataCategory.length > 0) {
			var categoryIdMultil = dataCategory[0].id;
			for (var i = 1, size = dataCategory.length; i < size; i++) {
				categoryIdMultil += "," + dataCategory[i].id;
			}
			dataModel.categoryIdMultil = categoryIdMultil;
		}
		dataModel.equipPeriodId = periodId;
		dataModel.status = $('#period').val();
		//		dataModel.formatType = $('input[type=radio]:checked').val();
		ReportUtils.exportReport('/report/equipment/equipReportXnt1d1/export', dataModel);
	},
	
	/**
	 * Bao cao nhap xuat ton tu mat, tu dong theo trang thai ky [ten ky]
	 * 
	 * @author hunglm16
	 * @since July 03, 2015
	 * */
	exportXNT1D2: function(){
		var msg = '';
		$('#errMsg').html('').hide();
		var periodId = $('#period').val();
		if (isNaN(periodId) || Number(periodId) < 0) {
			msg = 'Kỳ không hợp lệ';
		}
		if (msg.length > 0) {
			$('#errMsg').html(msg).show();
			return false;
		}

		var multiStockEquip = $("#stockEquip").data("kendoMultiSelect");
		var dataStock = multiStockEquip.dataItems();

		var multidataGroupEquip = $("#groupEquip").data("kendoMultiSelect");
		var dataGroup = multidataGroupEquip.dataItems();

		var multidataCategoryEquip = $("#categoryEquip").data("kendoMultiSelect");
		var dataCategory = multidataCategoryEquip.dataItems();

		var dataModel = new Object();

		if (dataStock != null && dataStock.length > 0) {
			var stockIdMultil = dataStock[0].equipStockId;
			for (var i = 1, size = dataStock.length; i < size; i++) {
				stockIdMultil += "," + dataStock[i].equipStockId;
			}
			dataModel.stockMultil = stockIdMultil;
		}

		if (dataGroup != null && dataGroup.length > 0) {
			var groupIdMultil = dataGroup[0].id;
			for (var i = 1, size = dataGroup.length; i < size; i++) {
				groupIdMultil += "," + dataGroup[i].id;
			}
			dataModel.groupMultil = groupIdMultil;
		}
		if (dataCategory != null && dataCategory.length > 0) {
			var categoryIdMultil = dataCategory[0].id;
			for (var i = 1, size = dataCategory.length; i < size; i++) {
				categoryIdMultil += "," + dataCategory[i].id;
			}
			dataModel.categoryIdMultil = categoryIdMultil;
		}
		dataModel.equipPeriodId = periodId;
		dataModel.status = $('#status').val();
		//		dataModel.formatType = $('input[type=radio]:checked').val();
		ReportUtils.exportReport('/report/equipment/equipReportXnt1d2/export', dataModel);
	},
	
	/**
	 * Bao cao phat sinh doanh so tu mat tu dong ds1.1
	 * 
	 * @author phuongvm
	 * @since 16/12/2015
	 * */
	exportDS1_1: function() {
		var msg = '';
		$('#errMsg').hide();
		var multiShop = $("#shop").data("kendoMultiSelect");
		var dataShop = null;
		if (multiShop != null) {
			dataShop = multiShop.dataItems();
		}
		if (dataShop == null || dataShop.length <= 0) {
			msg = format(msgErr_required_field, 'Đơn vị');
		}
		if (msg.length == 0) {
			msg = Utils.getMessageOfRequireCheck('tDate', 'Đến ngày');
		}
		if (msg.length == 0) {
			msg = Utils.getMessageOfInvalidFormatDate('tDate', 'Đến ngày');
		}
		var tDate = $('#tDate').val().trim();
		// if (msg.length == 0 && !Utils.compareCurrentDate(tDate)) {
		// 	msg = 'Đến ngày phải nhỏ hơn hoặc bằng ngày hiện tại';
		// 	$('#tDate').focus();
		// }

		if (msg.length > 0) {
			$('#errMsg').html(msg).show();
			return false;
		}

		var dataModel = new Object();

		var lstShopId = [];
		for (var i = 0, sz = dataShop.length; i < sz; i++) {
			lstShopId.push(dataShop[i].id);
		}
		dataModel.lstShopId = lstShopId.join(',');
		dataModel.toDate = tDate;
		ReportUtils.exportReport('/report/equipment/ds1_1/export', dataModel);
	},
	
	/**
	 * GD 1.1 Bao cao cac giao dich phat sinh
	 * 
	 * @author hunglm16
	 * @since August 24, 2015
	 * */
	reportGD1D1BCCGDPS: function(){
		var msg = '';
		$('#errMsg').html('').hide();
		var shopKendo = $("#shop").data("kendoMultiSelect");
		var lstShopId = shopKendo.value();
		if (msg.length == 0 && lstShopId.length == 0) {
			msg = 'Vui lòng chọn đơn vị';
		}
		if (msg.length == 0) {
			msg = Utils.getMessageOfRequireCheck('fromDate','Từ ngày');
		}
		if (msg.length == 0) {
			msg = Utils.getMessageOfInvalidFormatDate('fromDate','Từ ngày');
		}
		if (msg.length == 0) {
			msg = Utils.getMessageOfRequireCheck('toDate','Đến ngày');
		}
		if (msg.length == 0) {
			msg = Utils.getMessageOfInvalidFormatDate('toDate','Đến ngày');
		}
		var fDate = $('#fromDate').val();
		var tDate = $('#toDate').val();
		if (!Utils.compareDate(fDate, tDate)) {
			msg = msgErr_fromdate_greater_todate;		
			$('#fDate').focus();
		}
		if (msg.length > 0) {
			$('#errMsg').html(msg).show();
			return false;
		}
		

		
		var dataModel = new Object();
		dataModel.lstShopId = lstShopId.toString().trim();
		dataModel.fromDate = $('#fromDate').val();
		dataModel.toDate = $('#toDate').val();
		dataModel.typeGG = $('#typeGG').val();
//		dataModel.formatType = $('input[type=radio]:checked').val();
		ReportUtils.exportReport('/report/equipment/equipReportCgdps/export', dataModel);
	},
	
	/**
	 * Kiem ke thiet bi 1.1
	 * @author vuongmq
	 * @since 28/03/2016 
	 */
	exportKKTB11: function() {
		$('.ErrorMsgStyle').html('').hide();
		var msg = '';
		var shopId = $('#shopId').val();
		if (shopId.trim().length == 0 || shopId.trim() == activeType.ALL) {
			msg = format(msgErr_required_field, 'Đơn vị');
		}
		var statisticCode = $('#cbxProgramKK').combobox('getValue');
		if (msg.length == 0) {
			if (isNullOrEmpty(statisticCode)) {
				msg = 'Vui lòng chọn chương trình';
			}
		}
		/*if (msg.length == 0) {
			msg = Utils.getMessageOfRequireCheck('fDate', 'Từ ngày');
		}
		if (msg.length == 0) {
			msg = Utils.getMessageOfRequireCheck('tDate', 'Đến ngày');
		}
		if (msg.length == 0) {
			msg = Utils.getMessageOfInvalidFormatDate('fDate', 'Từ ngày');
		}
		if (msg.length == 0) {
			msg = Utils.getMessageOfInvalidFormatDate('tDate', 'Đến ngày');
		}
		var fDate = $('#fDate').val();
		var tDate = $('#tDate').val();
		if (!Utils.compareDate(fDate, tDate)) {
			msg = js_fromdate_todate_validate;		
			$('#fDate').focus();
		}
		if (msg.length == 0) {
			msg = Utils.compareCurrentDateEx('tDate', 'Đến ngày');
		}*/
		if (msg.length > 0) {
			$('#errMsg').html(msg).show();
			return false;
		}
		var dataModel = new Object();
		dataModel.lstShopId = shopId;
		dataModel.statisticCode = statisticCode;
		ReportUtils.exportReport('/report/equipment/kktb11/export', dataModel);
	},
};
/*
 * END OF FILE - /web/web/resources/scripts/business/equipment/vnm.equipment-report.js
 */

/*
 * START OF FILE - /web/web/resources/scripts/business/equipment/vnm.repair-equipment-payment.js
 */
var RepairEquipmentPayment = {
	_paramsSearch: null, /** param nay cho search va export excel theo grid*/
	_paramsSearchPopup: null, /** param nay cho search va export excel theo gridRepairPopup*/
	_mapRepair: null,
	_mapRepairPopup: null,

	// vuongmq; 22/06/2015; set param search quan ly thanh toan phieu sua chua
	setParamSearchPayForm: function() {
		var formCode = $('#txtRepairCode').val();
		var fromDate = $('#fDate').val();
		var status = $('#cbxStatus').val();
		/** put params vao; xu dung cho export */
		RepairEquipmentPayment._paramsSearch = new Object();
		RepairEquipmentPayment._paramsSearch.formCode = formCode;
		RepairEquipmentPayment._paramsSearch.fromDate = fromDate;
		RepairEquipmentPayment._paramsSearch.status = status;
	},

	/** vuongmq, 22/06/2015; cai nay search trong MH: Ql thanh toan sua chua  */
	searchRepairPayForm: function() {
		$('.SuccessMsgStyle').html('').hide();
		$('.ErrorMsgStyle').html('').hide();
		RepairEquipmentPayment._mapRepair = new Map();
		$('#equipHistoryTemplateDiv').css("visibility","hidden").hide();
		var err = "";
		$('#errMsg').html('').hide();
		var msg="";
		var fDate = $('#fDate').val();
		if (msg.length == 0 && fDate.trim().length > 0 && !Utils.isDate(fDate,'/') && fDate != '__/__/____') {
			msg = 'Ngày biên bản từ không tồn tại hoặc không đúng định dạng dd/mm/yyyy.';
			err = '#fDate';
		}
		if (msg.length > 0) {
			$('#errMsg').html(msg).show();
			$(err).focus();
			setTimeout(function(){$('#errMsg').hide();},3000);
			return false;
		}
		RepairEquipmentPayment.setParamSearchPayForm();
		$('#grid').datagrid('load',{
			page:1,
			//RepairEquipmentPayment._paramsSearch
			formCode: RepairEquipmentPayment._paramsSearch.formCode,
			fromDate: RepairEquipmentPayment._paramsSearch.fromDate,
			status: RepairEquipmentPayment._paramsSearch.status,
			});
		
	},
	/** vuongmq, 22/06/2015; MH chi thiet thanh toan sua chua  */
	viewDetail:function(id, code){
		$('.SuccessMsgStyle').html('').hide();
		$('.ErrorMsgStyle').html('').hide();
		if (code != undefined && code != null && code.trim().length > 0) {
			$('#equipmentCodeFromSpan').html(code);
		} else {
			$('#equipmentCodeFromSpan').html("");
		}
		$('#equipHistoryTemplateDiv').css("visibility","visible").show();
		$('#equipHistoryDg').datagrid({
			url : "/equipment-repair-payment/searchRepairPayDetail",
			pagination : true,
	        rownumbers : true,
	        pageNumber : 1,
	        scrollbarSize: 0,
	        autoWidth: true,
	        pageList: [10],
	        autoRowHeight : true,
	        fitColumns : true,
			queryParams: {id: id},
			width: $('#equipHistoryDgContainerGrid').width() - 15,
		    columns:[[
				/*{field: 'check',checkbox:true, width: 40, sortable:false,resizable:false, align: 'left'},*/
				{field:'maPhieu', title: 'Mã phiếu', width: 120, sortable: false, resizable: false , align: 'left', formatter:function(value,row,index){
			    	return Utils.XSSEncode(value);
			    }},
			    {field:'ngayLap', title: 'Ngày lập', width: 80, sortable: false, resizable: false , align: 'center', formatter:function(value,row,index){
			    	return Utils.XSSEncode(value);
			    }},	
				{field:'tongTien', title: 'Tổng tiền', width: 120, sortable:false, resizable: false, align: 'right', formatter:function(value,row,index){
					if (value != undefined && value != null) {
			    		return formatCurrency(value);
			    	}	
			    	return '';	
			    }},
		    ]],
		    onLoadSuccess :function(data){	    	
		    	$('.datagrid-header-rownumber').html('STT');
		    	//Utils.functionAccessFillControl('equipHistoryDg');
	   		 	$(window).resize();    		 
		    }
		});
	},

	/**
	 * Them danh sach các phieu sua chua
	 * @author vuongmq
	 * @since 22/06/2015
	 * */
	viewPopupRepair: function() {
	 	$('#easyuiPopupSearchRepair').show();
		$('#easyuiPopupSearchRepair').dialog({  
	        closed: false,  
	        cache: false,  
	        modal: true,
	        width : 1000,
//	        height : 'auto',
	        onOpen: function(){
	        	$('#shop, #txtRepairCodePopup, #txtCode, #fromDate, #toDate, #customerCode, #customerName, #txtSerial,').bind('keyup',function(event){
	        		if(event.keyCode == keyCodes.ENTER){
	        			$('#btnSearchRepairPopup').click(); 
	        		}
	        	});
	        	$('#shop').focus();
	        	$("#fromDate").val(getLastWeek());//Lay truoc 7 ngay
				ReportUtils.setCurrentDateForCalendar("toDate");
	        	RepairEquipmentPayment.viewPopupShop();
	        	RepairEquipmentPayment.viewGridPopupRepair();
	        },
			onClose: function(){
				$('#easyuiPopupSearchRepair').hide();
				$('#shop').val('').change();
				$('#txtCode').val('').change();
				$('#txtRepairCodePopup').val('').change();
				$('#fromDate').val('').change();
				$('#toDate').val('').change();
				$('#customerCode').val('').change();
				$('#customerName').val('').change();
				$('#txtSerial').val('').change();
				$('#gridRepairPopup').datagrid("loadData",[]);

				$('.SuccessMsgStyle').html('').hide();
				$('.ErrorMsgStyle').html('').hide();
			}
		});
	},

	/* vuongmq; 23/06/2015; show popup NPP phan quyen CMS*/
	viewPopupShop: function() {
		$('#shop').bind('keyup', function(event) {
			$('.SuccessMsgStyle').html('').hide();
			$('.ErrorMsgStyle').html('').hide();
			if (event.keyCode == keyCode_F9) {
				VCommonJS.showDialogSearch2({
					dialogInfo: {
						title: 'Tìm kiếm đơn vị'
					},
					/*params : {
						shopCode : $('#txtShopCode').val().trim(),
						status : activeType.RUNNING
					},*/
					inputs : [
					{id:'code', maxlength:50, label:'Mã đơn vị'},
					{id:'name', maxlength:250, label:'Tên đơn vị'},
					      ],
					url : '/commons/search-shop-show-list-NPP',
					columns : [[
						{field:'shopCode', title:'Mã đơn vị', align:'left', width: 120, sortable:false, resizable:false, formatter: function(value, row, index) {
					    	return Utils.XSSEncode(value);
					    }},
						{field:'shopName', title:'Tên đơn vị', align:'left', width: 180, sortable:false, resizable:false, formatter: function(value, row, index) {
					    	return Utils.XSSEncode(value);
					    }},
						{field:'choose', title:'', align:'center', width:40, sortable:false, resizable:false, formatter:function(value,row,index) {
							return '<a href="javascript:void(0)" onclick="RepairEquipmentPayment.fillTextBoxByShopCode(\''+ Utils.XSSEncode(row.shopCode) +'\',\''+row.shopId+'\')">chọn</a>';         
						}}
					]]
				});
			}
		});
	},

	 /**
	 * Dien gia tri Ma Don vi vao shop
	 * @author vuongmq
	 * @since 23/06/2015
	 * */
	 fillTextBoxByShopCode: function (shopCode, shopId) {
		$('#shop').val(shopCode);
		$('#shop').change();
		$('#shop').focus();
		$('#common-dialog-search-2-textbox').dialog("close");
		$('#common-dialog-search-2-textbox').remove();
	 },

	 /** vuongmq; 23/06/2015; set Param search Grid popup danh sach phieu sua chua*/
	 setParamSearchPopupRepair: function() {
	 	/* add them dieu kien tru cac phieu sua chua duoi danh sach gridRepairItem */
		var data = $('#gridRepairItem').datagrid('getRows');
		var lstRepairId = [];
		if (data != undefined && data != null && data.length > 0) {
			for (var i = 0; i < data.length; i++) {
				if (data[i].id != null && data[i].id != '') {
					lstRepairId.push(data[i].id);
				}
			}
		}
		// lay them dieu kien la danh sach phieu sua chua not in lstId; datagrid khong truyen len duoc mang[]
	 	RepairEquipmentPayment._paramsSearchPopup = {
			shopCode: $('#shop').val(), // ma NPP
			equipCode: $('#txtCode').val(),
			formCode: $('#txtRepairCodePopup').val(),
			fromDate: $('#fromDate').val(),
			toDate: $('#toDate').val(),
			customerCode: $('#customerCode').val(),
			customerName: $('#customerName').val(),
			seri: $('#txtSerial').val(),
			status: $('#statusPopup').val(),
			statusPayment: $('#statusPayment').val(),
			lstIdStr: lstRepairId.join(','),
		};
	 },

	/** vuongmq; 23/06/2015; search Grid popup danh sach phieu sua chua*/
	viewGridPopupRepair: function() {
		RepairEquipmentPayment.setParamSearchPopupRepair();
		RepairEquipmentPayment._mapRepairPopup = new Map();
		$('#gridRepairPopup').datagrid({
			url : "/equipment-repair-payment/searchRepairPopup",
			pagination : true,
	        rownumbers : true,
	        pageNumber : 1,
	        scrollbarSize: 0,
	        autoWidth: true,
	        pageList: [10, 20, 50],
	        autoRowHeight : true,
	        //fitColumns : true,
			queryParams: RepairEquipmentPayment._paramsSearchPopup,
			width: $('#gridContainer').width() - 15,
			checkOnSelect: false, // vuongmq; 19/05/2015; chi cho check checkox

			frozenColumns:[[        
				{field: 'check',checkbox:true, width: 50, sortable:false,resizable:false, align: 'left'},
				{field:'donVi', title: 'Đơn vị', width: 180, align: 'left',sortable: false, resizable: false, formatter:function(value,row,index){
			    	return Utils.XSSEncode(value);
			    }},
			    {field:'maPhieu', title: 'Mã phiếu', width: 120, sortable: false, resizable: false , align: 'left', formatter:function(value,row,index){
			    	return Utils.XSSEncode(value);
			    }},
			    {field:'ngayTao', title: 'Ngày tạo', width: 80, sortable: false, resizable: false , align: 'centre', formatter:function(value,row,index){
			    	return Utils.XSSEncode(value);
			    }},
			]],
			columns:[[
			    {field:'loaiThietBi', title: 'Loại thiết bị', width: 160, sortable: false, resizable: false , align: 'left', formatter:function(value,row,index){
			    	return Utils.XSSEncode(value);
			    }},
			    {field:'nhomThietBi', title: 'Nhóm thiết bị', width: 180, sortable: false, resizable: false , align: 'left', formatter:function(value,row,index){
			    	return Utils.XSSEncode(value);
			    }},
			    {field:'maThietBi', title: 'Mã thiết bị', width: 180, align: 'left', sortable: false, resizable: false, formatter:function(value,row,index){
			    	return Utils.XSSEncode(value);
			    }},
			    {field:'soSeri', title: 'Số serial', width: 120, align: 'left', sortable: false, resizable: false, formatter:function(value,row,index){
			    	return Utils.XSSEncode(value);
			    }},
			    {field:'namSanXuat', title: 'Năm sản xuất', width: 80, sortable:false, resizable: false, align: 'right', formatter:function(value,row,index){
					if (value != undefined && value != null) {
			    		return value;
			    	}	
			    	return '';	
			    }},
			    {field:'khachHang', title: 'Khách hàng', width: 160, sortable:false, resizable: false, align: 'left', formatter:function(value,row,index){
			    	return Utils.XSSEncode(value);	
			    }},
			    {field:'diaChi', title: 'Địa chỉ', width: 200, sortable:false, resizable: false, align: 'left', formatter:function(value,row,index){
			    	return Utils.XSSEncode(value);	
			    }},
			    /*{field:'trangThai',title: 'Trạng thái', width: 50, sortable: false, resizable: false, align: 'left', formatter:function(value,row,index){
			    	var status = "";
			    	if(value == 0){
			    		status = "Dự thảo";
			    	}else if(value == 1){
			    		status = "Chờ duyệt";
			    	}else if(value == 2){
			    		status = "Đã Duyệt";
			    	}else if(value == 3){
			    		status = "Không duyệt";
			    	}else if(value == 4){
			    		status = "Hủy";
			    	}else if(value == 5){
			    		status = "Sửa chữa";
			    	}else if(value == 6){
			    		status = "Hoàn tất";
			    	}
			    	return Utils.XSSEncode(status);
			    }},*/
				/*{field:'tinhTrangHuHong', title: 'Tình trạng hư hỏng', width: 150, sortable: false, resizable: false, align: 'left', formatter:function(value,row,index){
					return Utils.XSSEncode(value);
			    }},	
				{field:'lyDoDeNghi', title: 'Lý do/Đề nghị', width: 150, sortable: false, resizable: false, align: 'left', formatter:function(value,row,index){
			    	return Utils.XSSEncode(value);
			    }},*/	
				{field:'tongTien', title: 'Tổng tiền', width: 120, sortable:false, resizable: false, align: 'right', formatter:function(value,row,index){
					if (value != undefined && value != null) {
			    		return formatCurrency(value);
			    	}	
			    	return '';	
			    }},
		    ]],
		    onLoadSuccess :function(data){	    	
		    	$('.datagrid-header-rownumber').html('STT');
		    	$('.datagrid-header-check input').removeAttr('checked');
	        	var rows = $('#gridRepairPopup').datagrid('getRows');
		    	for(var i = 0; i < rows.length; i++) {
		    		if(RepairEquipmentPayment._mapRepairPopup.get(rows[i].id)!=null) {
		    			$('#gridRepairPopup').datagrid('checkRow', i);
		    		}
		    	}
	   		 	$(window).resize();    		 
		    },
		    onCheck:function(index,row){
				RepairEquipmentPayment._mapRepairPopup.put(row.id,row);   
		    },
		    onUncheck:function(index,row){
		    	RepairEquipmentPayment._mapRepairPopup.remove(row.id);
		    },
		    onCheckAll:function(rows){
		    	for(var i = 0;i<rows.length;i++){
		    		var row = rows[i];
		    		RepairEquipmentPayment._mapRepairPopup.put(row.id,row);
		    	}
		    },
		    onUncheckAll:function(rows){
		    	for(var i = 0;i<rows.length;i++){
		    		var row = rows[i];
		    		RepairEquipmentPayment._mapRepairPopup.remove(row.id);		    		
		    	}
		    }
		});
	},

	/**
	 * Tim kiem danh sach phieu sua chua tren popup
	 * @author vuongmq
	 * @since 23/06/2014
	 */
	searchPopupRepair: function() {
		$('.SuccessMsgStyle').html('').hide();
		$('.ErrorMsgStyle').html('').hide();
		var msg = '';
		if (msg.length == 0) {
			msg = Utils.getMessageOfInvalidFormatDate('fromDate','Từ ngày');
		}
		if (msg.length == 0) {
			msg = Utils.getMessageOfInvalidFormatDate('toDate','Đến ngày');
		}
		var fDate = $('#fromDate').val();
		var tDate = $('#toDate').val();
		var curDate = ReportUtils.getCurrentDateString();
		if(msg.length == 0 && !Utils.compareDate(tDate, curDate)){
			msg = "Đến ngày không được lớn hơn ngày hiện tại";
			$('#toDate').focus();
		}
		if (msg.length == 0) {
			if (!Utils.compareDate(fDate, tDate)) {
				msg = msgErr_fromdate_greater_todate;		
				$('#fromDate').focus();
			}
		}
		if (msg.length > 0) {
			$('#errMsgPopup').html(msg).show();
			return false;
		}
		RepairEquipmentPayment.setParamSearchPopupRepair();
		$('#gridRepairPopup').datagrid('load',RepairEquipmentPayment._paramsSearchPopup);
	},

	/**
	 * Chon danh sach phieu sua chua tren popup; insert vao grid gridRepairItem cua phieu thanh toan
	 * @author vuongmq
	 * @since 23/06/2014
	 */
	chooseRepairPopup: function() {
		if (RepairEquipmentPayment._mapRepairPopup != undefined && RepairEquipmentPayment._mapRepairPopup != null && RepairEquipmentPayment._mapRepairPopup.keyArray != null && RepairEquipmentPayment._mapRepairPopup.keyArray.length > 0) {
        	//var rows = new Array();
        	var lengthMap = RepairEquipmentPayment._mapRepairPopup.keyArray.length;
        	for (var i = 0; i < lengthMap ; i++) {
        		var key = RepairEquipmentPayment._mapRepairPopup.keyArray[i];
        		var dataRow =  RepairEquipmentPayment._mapRepairPopup.get(key);
        		var row = {
        			id:	dataRow.id,
        			maPhieu: dataRow.maPhieu,
        			ngayLap: dataRow.ngayTao, // ngay tao tren popup phieu sua chua, dua xuong la ngay lap cua phieu sua chua
        			tongTien: dataRow.tongTien,
        		};
        		var idx1 = 0;
        		var rowsss = $('#gridRepairItem').datagrid("getRows");
        		if (rowsss.length > 0) {
	        		idx1 = rowsss.length - 1;
	        	}
        		$('#gridRepairItem').datagrid('insertRow',{row:row,index:idx1});
        		//RepairEquipmentPayment._lstEquipItemId.push(row.id);
        	}
        	$('#easyuiPopupSearchRepair').dialog('close');
        } else {
        	$('#errMsgPopup').html("Bạn chưa chọn thiết bị.").show();
        }
	},

	/**
	 * Xoa mot dong cua phieu sua chua trong phieu thanh toan
	 * @author vuongmq
	 * @since 22/06/2015
	 * */
	deleteRow: function (index) {
		var row = $('#gridRepairItem').datagrid('getRows')[index];
		if (row != null && row.id != undefined && row.id != null) {
			$('#gridRepairItem').datagrid("deleteRow", index);
			/*for (var i = 0; i < RepairEquipmentPayment._lstEquipItemId.length; i++) {
				if (RepairEquipmentPayment._lstEquipItemId[i] == row.id) {
					RepairEquipmentPayment._lstEquipItemId.splice(i,1);
					break;
				}
			}
			RepairEquipmentPayment.reloadGridDetail();*/
			/** vuongmq; 30/06/2015 ;cap nhat lai danh dach grid theo index moi */
			var data = $('#gridRepairItem').datagrid('getRows');
			$('#gridRepairItem').datagrid('loadData', data);
		}
	},

	/**
	 * khoi tao grid danh sach cac hang muc sua chua; cua phieu thanh toan
	 * @author vuongmq
	 * @date 23/06/2015
	 */
	initializeRepairItemsGrid: function(idPayment, status, statusPayment) {
		$('#gridRepairItem').datagrid({
			url : "/equipment-repair-payment/searchRepairPayDetail",
			//pagination : true,
			rownumbers : true,
			pageNumber : 1,
			scrollbarSize: 0,
			autoWidth: true,
			//pageList: [10],
			autoRowHeight : true,
			fitColumns : true,
			singleSelect: true,
			queryParams: {id: idPayment},
			width: $('#gridRepairItemContainer').width() - 15,
			columns:[[
				/*{field: 'check',checkbox:true, width: 40, sortable:false,resizable:false, align: 'left'},*/
				{field:'maPhieu', title: 'Mã phiếu', width: 120, sortable: false, resizable: false , align: 'left', formatter:function(value,row,index){
			    	return Utils.XSSEncode(value);
			    }},
			    {field:'ngayLap', title: 'Ngày lập', width: 80, sortable: false, resizable: false , align: 'center', formatter:function(value,row,index){
			    	return Utils.XSSEncode(value);
			    }},	
				{field:'tongTien', title: 'Tổng tiền', width: 120, sortable:false, resizable: false, align: 'right', formatter:function(value,row,index){
					if (value != undefined && value != null) {
			    		return formatCurrency(value);
			    	}	
			    	return '';	
			    }},
			    {field:'edit', title:'<a onclick="RepairEquipmentPayment.viewPopupRepair()" title="Thêm phiếu sửa chữa"><img src="/resources/images/icon_add.png"/></a>', width: 70, align: 'center', sortable: false, resizable: true, formatter: function(value,row,index){
			    	return '<a onclick="RepairEquipmentPayment.deleteRow('+index+',\''+row.id+'\',\''+ Utils.XSSEncode(row.maPhieu) +'\')" href="javascript:void(0)"><img width="15" height="15" src="/resources/images/icon_delete.png" title="Xóa phiếu sửa chữa '+row.maPhieu+'"></a>';
				}},
				{field: 'id', sortable:false, hidden: true},
		    ]],
			onLoadSuccess:function(data){
				$('.datagrid-header-rownumber').html('STT');
				//Utils.functionAccessFillControl('equipHistoryDg');
				/*if (viewActionType === ViewActionType.CREATE || viewActionType === ViewActionType.EDIT) {
					if (data && data.rows && data.rows.length) {
					
						RepairEquipmentPayment.loadInfoPayment(data.rows);
					}
				}*/
				if (statusPayment == '' || statusPayment != StatusRecordsEquip.APPROVED) {
					// quyen Ql tao moi, chinh sua
					if (status == StatusRecordsEquip.WAITING_APPROVAL || status == StatusRecordsEquip.APPROVED || status == StatusRecordsEquip.CANCELLATION) {
						$("#gridRepairItem").datagrid('hideColumn', 'edit');
					}
				} else {
					// quyen duyet
					$("#gridRepairItem").datagrid('hideColumn', 'edit');
				}
	   		 	$(window).resize();			 
			}
		});
	},

	 /**
	 * cap nhat phieu thanh toan; them tao moi phieu thanh toan
	 * @author vuongmq
	 * @param  long idPayment id cua phieu sua chua
	 * @return void
	 */
	updateEquipmentRepairPaymentRecord: function(idPayment) {
		$('.SuccessMsgStyle').html('').hide();
		$('.ErrorMsgStyle').html('').hide();
		var msg = '';
		if (msg.length == 0) {
			msg = Utils.getMessageOfRequireCheck('fDate','Ngày thanh toán');
		}
		if (msg.length == 0) {
			msg = Utils.getMessageOfInvalidFormatDate('fDate','Ngày thanh toán');
		}
		var fDate = $('#fDate').val();
		var curDate = ReportUtils.getCurrentDateString();
		if(msg.length == 0 && !Utils.compareDate(fDate, curDate)){
			msg = "Ngày thanh toán không được lớn hơn ngày hiện tại";
			$('#fDate').focus();
		}
		var row = $('#gridRepairItem').datagrid('getRows');
		if (msg.length == 0) {
			if (row == null || row.length == 0) {
				msg = 'Vui lòng chọn phiếu sửa chữa để thanh toán';
			}
		}
		if (msg.length > 0) {
			$('#errMsgDetail').html(msg).show();
			return false;
		}
		var row = $('#gridRepairItem').datagrid('getRows');
		var lstRepairId = [];
		for (var i = 0; i < row.length; i++) {
			if (row[i] != null && row[i].id != null) {
				lstRepairId.push(row[i].id);
			}
		}
		var id = '';
		if (idPayment != undefined && idPayment != null) {
			id = idPayment;
		}
		var status = $('#cbxStatus').val();
		var dataModel = {};
		dataModel.id = id; // truong hop cap nhat phiwu thanh toan thi co Id
		dataModel.paymentDate = fDate;
		dataModel.status = status;
		dataModel.lstId = lstRepairId;
		var titleErr = {title:'Lỗi các phiếu sửa chữa ',pageList:10};
		Utils.addOrSaveData(dataModel, '/equipment-repair-payment/update-payment-record', null, 'errMsgDetail', function(data) {
			if (data.error == false) {
				if (data.maPhieu != undefined && data.maPhieu != '') {
					$('#txtRepairCode').val(data.maPhieu);
					// cap nhat lai Id cho btnUpdate neu muon chuyen, cap nhat lai phieu
					$('#btnUpdate').attr('onclick','RepairEquipmentPayment.updateEquipmentRepairPaymentRecord('+data.idPhieu+');')
				}
				if (data.status != undefined && data.status != '' && data.status == StatusRecordsEquip.WAITING_APPROVAL) {
					// quyen Ql tao moi, chinh sua
					var htmlCombo = '<option value="1">Chờ duyệt</option> <option value="4">Hủy</option>';
					$('#cbxStatus').html(htmlCombo).change();
					$('#cbxStatus').val(status).change();
					
					RepairEquipmentPayment.initializeRepairItemsGrid(idPayment, data.status);
					disableDateTimePicker('fDate');
				}
				if (data.lstErr != undefined && data.lstErr != null && data.lstErr.length > 0 ) {
					VCommonJS.showDialogList({
						data : data.lstErr,
						dialogInfo: titleErr,
						columns : [[
							{field:'formCode', title:'Mã phiếu sửa chữa', align:'left', width: 60, sortable:false, resizable:false, formatter: function(value, row, index) {
						    	return Utils.XSSEncode(value);
						    }},
							{field:'formStatusErr', title:'Trạng thái', align:'left', width: 180, sortable:false, resizable:false, formatter: function(value,row,index){
								var html="";
								if(value == 1){
									html = '<img src="/resources/images/icon_delete.png" height="20" width="20" style="margin-right:5px;" title="Error"> ' + row.recordNotComplete;
								}
								return html;
							} }
						]]
					});	
				} else {
					var status = $('#cbxStatus').val();
					if (status != undefined && status != '' && (status == StatusRecordsEquip.APPROVAL || status == StatusRecordsEquip.NO_APPROVAL)) {
						disableSelectbox('cbxStatus');
						$('#btnUpdate').remove();
					}
					$('#successMsgDetail').html('Lưu dữ liệu thành công').show();
					setTimeout(function(){$('#successMsgDetail').html('').hide();}, 1500);
				}
			}
		}, null, null, null, null, null, true);
	},
	
	/** vuongmq, QLthanh toan sua chua-> cap nhat trang thai cua phieu thanh toan sua chua;*/
	updateStatus: function() {
		$('.SuccessMsgStyle').html('').hide();
		$('.ErrorMsgStyle').html('').hide();	
		var statusForm = $('#cbxStatusUpdate').val().trim();
		if (statusForm < 0) {
			$('#errMsg').html('Vui lòng chọn trạng thái').show();
			return;
		}
		var arra = RepairEquipmentPayment._mapRepair.valArray;		
		var lstId = new Array();
		if (arra == null || arra.length == 0) {
			$('#errMsg').html('Bạn chưa chọn phiếu thanh toán sửa chữa để cập nhật. Vui lòng chọn phiếu.').show();
			return;
		}		
		for (var i=0, size = arra.length; i < size; ++i) {
			lstId.push(arra[i].id);			
		}
		var params = new Object();
		params.lstId = lstId;
		params.statusForm = statusForm;
		/*if ($("#rejectReason").val()!=null && $("#rejectReason").val()!='') {
			params.rejectReason = $("#rejectReason").val();
		}*/
		var title = {title:'Lỗi lưu trạng thái phiếu thanh toán',pageList:10};
		Utils.addOrSaveData(params, '/equipment-repair-payment/updateStatus', null, 'errMsg', function(data) {
			if ( data.lstErr != undefined && data.lstErr != null && data.lstErr.length > 0 ) {
				VCommonJS.showDialogList({
				    data : data.lstErr,
				    dialogInfo: title,
				    columns : [[
				        {field:'formCode', title:'Mã biên bản', align:'left', width: 120, sortable:false, resizable:false, formatter: function(value, row, index) {
					    	return Utils.XSSEncode(value);
					    }},
				        {field:'formStatusErr', title:'Trạng thái biên bản', align:'left', width: 180, sortable:false, resizable:false, formatter: function(value,row,index){
				        	var html="";
							if(value == 1){
								html = '<img src="/resources/images/icon_delete.png" height="20" width="20" style="margin-right:5px;" title="Error"> ' + row.recordNotComplete;
							}
							return html;
						} }
					]]
				});	
			} else {
				$('#successMsg').html('Lưu dữ liệu thành công').show();
				setTimeout(function(){$('#successMsg').html('').hide();},1500);
			}
			RepairEquipmentPayment._mapRepair = new Map();
			$('#grid').datagrid('reload');
			$('#equipHistoryTemplateDiv').css("visibility", "hidden").hide();
		}, null, true,null, 'Bạn có muốn cập nhật phiếu thanh toán?', null, true);
		
	},

	/**
	 * xuat file excel Phieu thanh toan sua chua
	 * @author nhutnn
	 * @since 16/01/2015
	 */
	exportListRepairPayForm: function() {
		$('.ErrorMsgStyle').html("").hide();
		var msg = '';
		/*var data = $('#grid').datagrid('getRows');
		if (data != null && data.length <= 0 ) {
			msg = 'Không có dữ liệu xuất báo cáo';
		}*/
		var arra = RepairEquipmentPayment._mapRepair.valArray;		
		var lstId = new Array();
		if (msg.length == 0) {
			if (arra == null || arra.length == 0) {
				msg = 'Bạn chưa chọn phiếu thanh toán sửa chữa để xuất file excel. Vui lòng chọn phiếu.';
			}
		}
		if (msg.length > 0) {
			$('#errMsg').html(msg).show();
			setTimeout(function(){$('#errMsg').hide();},3000);
			return false;
		}
		for (var i=0, size = arra.length; i < size; ++i) {
			lstId.push(arra[i].id);			
		}
		var params = new Object();
		params.lstId = lstId;
		$.messager.confirm('Xác nhận','Bạn có muốn xuất file excel?',function(r) {
			if (r) {
				ReportUtils.exportReport('/equipment-repair-payment/export-repair-payform', params, 'errMsg');					
			}
		});		
		return false;
	},
	///////////////////////////////////////////// END VUONGMQ ///////////////////////////////////////
};
/*
 * END OF FILE - /web/web/resources/scripts/business/equipment/vnm.repair-equipment-payment.js
 */

/*
 * START OF FILE - /web/web/resources/scripts/business/equipment/vnm.equip-stock-adjust.js
 */
var EquipStockAdjustForm = {
	lstId : null ,
	currentGridIndexStockAdjustDtl: null,
	_isCreater:0,
	_status:null,
	_mapEquipStockAdjust:new Map(),
	

	/**
	 * @author Datpv4
	 * @since July 02,2015
	 * @description tim kiem phieu sua chua
	 */
	searchEquipStockAdjustForm: function() {		
		$('#equipStockAdjustDtlTemplateDiv').hide();
		var err = "";
		$('#errMsg').html('').hide();
		var msg="";
		
		var fDate = $('#fDate').val();
		var tDate =$('#tDate').val();
		if (msg.length == 0 && $('#fDate').val().trim().length > 0 && !Utils.isDate($('#fDate').val(),'/') && $('#fDate').val() != '__/__/____') {
			msg = 'Ngày tạo từ không tồn tại hoặc không đúng định dạng dd/mm/yyyy.';
			err = '#fDate';
		}
		if (msg.length == 0 && $('#tDate').val().trim().length > 0 && !Utils.isDate($('#tDate').val(),'/') && $('#tDate').val() != '__/__/____') {
			msg = 'Đến ngày không tồn tại hoặc không đúng định dạng dd/mm/yyyy.';
			err = '#tDate';
		}
		if (msg.length==0 && fDate.length > 0 && tDate.length>0 && !Utils.compareDate(fDate, tDate)) {
			msg = 'Ngày tạo từ phải là ngày trước hoặc cùng ngày với đến ngày';
		}
		if(msg.length>0){
			$('#errMsg').html(msg).show();
			$(err).focus();
			setTimeout(function(){$('#errMsg').hide();},3000);
			return false;
		}
		var status = $('#cbxStatus').val();
		var code = $('#txtCode').val();
		var fromDate = $('#fDate').val();
		var toDate = $('#tDate').val();
		
		$('#grid').datagrid('load',{
			page:1,
			code: code,			
			status:status , 			
			fromDate:fromDate,
			toDate:toDate,
			isCreate:EquipStockAdjustForm._isCreater
			});
	},
	
	/**
	 * @author Datpv4
	 * @since July 02,2015
	 * @description chi tiet phieu sua chua
	 */
	initializeEquipStockAdjustDtlGrid: function(id,code) {
		 if (code!=undefined) {
			 	$('#equipStockAdjustCode').text(code);
				$('#equipStockAdjustDtlTemplateDiv').css("visibility","visible").show();
			}
		$('#gridEquipStockAdjustDtlItem').datagrid({
			url : "/equip-stock-adjust/searchEquipStockAdjustDtl",
			pagination : false,
	        rownumbers : true,
	        /*pageNumber : 1,*/
	        scrollbarSize: true,
	        autoWidth: true,
	       /* pageSize: 20,*/
	        /*pageList: [20, 30, 50],*/
	       /* autoRowHeight : true,*/
	        height:400,
			queryParams: {id: id},
			width: $('#gridEquipStockAdjustDtlItemContainer').width(),
			frozenColumns:[[
	            {field:'edit', title:'<a id= "dg_add" class="cmsiscontrol" onclick="EquipStockAdjustForm.viewPopupStockAdjust()"  title="Nhập thông tin thiết bị"><img src="/resources/images/icon_add.png"/></a>', width: 80, align: 'center', sortable: false, resizable: true, formatter: function(value,row,index){
				    	return '<a style = "margin-right: 5px;" onclick="EquipStockAdjustForm.viewPopupStockAdjust('+index+')" href="javascript:void(0)"><img width="18" height="18" src="/resources/images/icon-edit.png" title="Xem phiếu cập nhật kho điều chỉnh "></a><a onclick="EquipStockAdjustForm.deleteRow('+index+')" href="javascript:void(0)"><img width="15" height="15" src="/resources/images/icon_delete.png" title="Xóa phiếu cập nhật kho điều chỉnh"></a>';
				 }},
				{field:'equipId', title: 'ID thiết bị', width: 80,hidden:true, sortable: false, resizable: false , align: 'left', formatter:function(value,row,index){
			    	return Utils.XSSEncode(value);
			    }},
			    {field:'equipCode', title: 'Mã thiết bị', width: 120, sortable: false, resizable: false , align: 'left', formatter:function(value,row,index){
			    	return Utils.XSSEncode(value);
			    }},
			    {field:'serial', title: 'Số serial', width: 180, sortable: false, resizable: false , align: 'left', formatter:function(value,row,index){
			    	return Utils.XSSEncode(value);
			    }},	
			    
			    {field:'healthStatus', title: 'ID Tình trạng thiết bị', hidden:true, width: 120, sortable:false, resizable: false, align: 'left', formatter:function(value,row,index){
			    	return Utils.XSSEncode(value);
			    	
			    }},
				{field:'healthStatusStr', title: 'Tình trạng thiết bị', width: 120, sortable:false, resizable: false, align: 'left', formatter:function(value,row,index){
			    	return Utils.XSSEncode(value);
			    	
			    }},
			    
			    {field:'equipGroupId', title: 'Id Nhóm thiết bị', hidden:true, width: 120, sortable:false, resizable: false, align: 'left', formatter:function(value,row,index){
			    	return Utils.XSSEncode(value);
			    }},
			    
			    {field:'equipGroupCode', title: 'Mã nhóm thiết bị', hidden:true, width: 120, sortable:false, resizable: false, align: 'left', formatter:function(value,row,index){
			    	return Utils.XSSEncode(value);
			    }},
			    
			    {field:'equipGroupName', title: 'Nhóm thiết bị', width: 180, sortable:false, resizable: false, align: 'left', formatter:function(value,row,index){
			    	if(row.equipGroupCode==undefined){
			    		return Utils.XSSEncode(row.equipGroupName);
			    	}else {
			    		return Utils.XSSEncode(row.equipGroupCode +"-"+row.equipGroupName);
			    	}
			    }}
	    		]],	
			columns:[[
		        
			    {field:'equipCateGogyName', title: 'Loại', width: 150, sortable:false, resizable: false, align: 'left', formatter:function(value,row,index){
					return Utils.XSSEncode(value);
			    }},
			    {field:'capacity', title: 'Dung tích (lít)', width: 120, sortable:false, resizable: false, align: 'left', formatter:function(value,row,index){
			    	return Utils.XSSEncode(value);
			    }},
			    {field:'equipBrandName', title: 'Hiệu', width: 180, sortable:false, resizable: false, align: 'left', formatter:function(value,row,index){
			    	return Utils.XSSEncode(value);
			    	
			    }},
			    {field:'equipProviderId', title: 'Id NCC',hidden:true, width: 120, sortable:false, resizable: false, align: 'left', formatter:function(value,row,index){
			    	return Utils.XSSEncode(value);
			    
			    }},
			    {field:'equipProviderCode', title: 'Code NCC', hidden:true , width: 120, sortable:false, resizable: false, align: 'left', formatter:function(value,row,index){
			    	return Utils.XSSEncode(value);
			    
			    }},
			    {field:'equipProviderName', title: 'NCC', width: 120, sortable:false, resizable: false, align: 'left', formatter:function(value,row,index){
			    	if(row.equipProviderCode==undefined){
			    		return Utils.XSSEncode(row.equipProviderName);
			    	} else {
			    		return Utils.XSSEncode(row.equipProviderCode+"-"+row.equipProviderName);
			    	}
			    }},
			    {field:'manufacturingYear', title: 'Năm sản xuất', width: 80, sortable:false, resizable: false, align: 'center', formatter:function(value,row,index){
			    	return Utils.XSSEncode(value);
			    	
			    }},
			    {field:'price', title: 'Nguyên giá', width: 120, sortable:false, resizable: false, align: 'right', formatter:function(value,row,index){
			    	return formatCurrency(value);
			    	
			    }},
			    {field:'warrantyExpiredDate', title: 'Ngày hết hạn bảo hành', width: 100, sortable:false, resizable: false, align: 'center', formatter:function(value,row,index){
			    	return Utils.XSSEncode(value);
			    	
			    }},
			    {field:'stockId', title: 'Id Kho',hidden:true, width: 100, sortable:false, resizable: false, align: 'left', formatter:function(value,row,index){
			    	return Utils.XSSEncode(value);
			    }},
			   /* {field:'stockCode', title: 'Type Kho',hidden:true, width: 100, sortable:false, resizable: false, align: 'left', formatter:function(value,row,index){
			    	return Utils.XSSEncode(value);
			    }},*/
			    
			    {field:'stockCode', title: 'Ma Kho', width: 180, hidden:true, sortable:false, resizable: false, align: 'left', formatter:function(value,row,index){
			    	return Utils.XSSEncode(value);
			    }},
			    
			    {field:'stockName', title: 'Kho', width: 180, sortable:false, resizable: false, align: 'left', formatter:function(value,row,index){
			    	return Utils.XSSEncode(row.stockCode +"-"+row.stockName);
			    }},
			    
				{field: 'id', sortable:false, hidden: true},
		    ]],
			onLoadSuccess:function(data){
				$(window).resize();
		   		 if (code!=undefined) {//Neu view chi tiet trong man hinh Quan ly cap nhat kho dieu chinh
		   			$("#gridEquipStockAdjustDtlItem").datagrid('hideColumn', 'edit');
				} else if($('#gridEquipStockAdjustDtlItemContainer .cmsiscontrol[id^="dg_"]').length == 0){//Phan quyen chuc nang
					disabled('txtDescription');
					//trang thai la cho duyet Neu va la quyen duyet, khi xem chi tiet dc chuyen sang Duyet va khong duyet
					if(EquipStockAdjustForm._status==1){
						html = '<option value="1" >Chờ duyệt</option> <option value="2">Duyệt</option><option value="3">Không duyệt</option>';
						$('#cbxStatus').html(html).change();
						$("#gridEquipStockAdjustDtlItem").datagrid('hideColumn', 'edit');
					} else {
						disabled('btnUpdate');
					}
				}
			}
		});
	},
	
	/**
	 * @author Datpv4
	 * @since July 03,2015
	 * @description Delete data grid row by Index
	 */
	deleteRow: function (index) {
		var row = $('#gridEquipStockAdjustDtlItem').datagrid('getRows')[index];
		$('#gridEquipStockAdjustDtlItem').datagrid("deleteRow", index);
		var data = $('#gridEquipStockAdjustDtlItem').datagrid('getRows');
		$('#gridEquipStockAdjustDtlItem').datagrid('loadData', data);
		
	},
	/**
	 * @author Datpv4
	 * @since July 03,2015
	 * @description man hinh them moi, chinh sua thiet bi
	 */
	viewPopupStockAdjust: function(gridIndex) {
	 	$('#easyuiPopupSearchStockAdjust').show();
		$('#easyuiPopupSearchStockAdjust').dialog({  
	        closed: false,  
	        cache: false,  
	        modal: true,
	        width :960,
	        onOpen: function(){
	        	if(gridIndex!=undefined){
	        		EquipStockAdjustForm.currentGridIndexStockAdjustDtl = gridIndex;
		        	var rows = $('#gridEquipStockAdjustDtlItem').datagrid('getRows');    // get current page rows
		        	var row = rows[gridIndex]; 
		        	
		        	$("#ddlEquipGroupId").data("kendoComboBox").value(row.equipGroupId);
		        	$("#ddlEquipProviderId").data("kendoComboBox").value(row.equipProviderId);
		        	
		        	$('#ddlPrice').val(row.price);
		        	$('#ddlManufacturing').val(row.manufacturingYear);
		        	$('#ddlHealthStatus').val(row.healthStatus).change();
		        	$('#stockCode').val(row.stockCode);
		        	$('#ddlStockId').val(row.stockId);
					$('#ddlStockName').val(row.stockName);
		        	$('#ddlSeri').val(row.serial);
		        	$('#ddlExpireDate').val(row.warrantyExpiredDate);
		        	$('#ddlNo').val(EquipStockAdjustForm.currentGridIndexStockAdjustDtl+1);
		        	$('#ddlEquipId').val(row.equipId);
	        	} 
	        	$('#ddlPrice').focus();
	        	$('#errMsgEquipmentDlg').html("").hide();
	        },
			onClose: function(){
				EquipStockAdjustForm.currentGridIndexStockAdjustDtl = null;
				$('#ddlSeri').val("");
				$("#ddlHealthStatus").val($("#ddlHealthStatus option:first").val()).change();
				$("#ddlEquipCategoryName").val($("#ddlEquipCategoryName option:first").val()).change();
				$('#ddlManufacturing').val("");
				$('#ddlStockId').val("");
				$('#ddlStockName').val("");
				$('#ddlPrice').val("");
				$('#stockCode').val("");
				$('#ddlNo').val("");
	        	$('#ddlEquipId').val("");
				$('#errMsgEquipmentDlg').html("").hide();
				ReportUtils.setCurrentDateForCalendar('ddlExpireDate');
				
			  var widgetEquipGroupId = $("#ddlEquipGroupId").data("kendoComboBox");
	          widgetEquipGroupId.select(0);
	          var widgetEquipProviderId = $("#ddlEquipProviderId").data("kendoComboBox");
	          widgetEquipProviderId.select(0);   
		              
		          
			}
		});
	},
	/**
	 * @author TamVNM
	 * @Updated Datpv4
	 * @since July 06,2015
	 * @description Tim kiem kho phan quyen
	 */
	showPopupSearchStock: function(txtInput){
		var txt = "stockCode";
		VCommonJS.showDialogSearch2({
			inputs : [
		        {id:'code', maxlength:50, label:'Mã đơn vị'},
		        {id:'name', maxlength:250, label:'Tên đơn vị'},
		    ],
		    url : '/commons/get-equipment-stock',
		    columns : [[
		        {field:'codeName', title:'Đơn vị', align:'left', width: 110, sortable:false, resizable:false, formatter:function(value,row,index) {
		        	var nameCode = row.shopCode + '-' + row.shopName;
		        	return Utils.XSSEncode(nameCode);         
		        }},
		        {field:'equipStockCode', title:'Mã kho', align:'left', width: 200, sortable:false, resizable:false},
		        {field:'choose', title:'', align:'center', width:40, sortable:false, resizable:false, formatter:function(value,row,index) {
		            return '<a href="javascript:void(0)" onclick="EquipStockAdjustForm.chooseStock(\''+txt+'\', ' + row.equipStockId +', \''+Utils.XSSEncode(row.equipStockCode)+'\', \''+Utils.XSSEncode(row.equipStockName)+'\','+Utils.XSSEncode(Utils._isShowInputTextByDlSearch2.code)+');">chọn</a>';         
		        }}
		    ]]
		});
	},
	
	/**
	 * @author Datpv4
	 * @since July 06,2015
	 * @description chon kho
	 */
	chooseStock: function (txt,stockId,stockCode,stockName){
		$('#'+txt).val(stockCode);
		$('#ddlStockName').val(stockName);
		$('#ddlStockId').val(stockId);
		$('#common-dialog-search-2-textbox').dialog("close");
		$('#common-dialog-search-2-textbox').remove();
	},
	
	/**
	 * @author Datpv4
	 * @since July 08,2015
	 * @description chon thiet bi
	 */
	chooseStockAdjustPopup: function(txtInput){		
		var msg = Utils.getMessageOfInvalidFormatDate('ddlExpireDate','Ngày hết hạn bảo hành');
		var iDFocus ;
		if(msg.length==0){
			msg = Utils.getMessageOfRequireCheck('ddlEquipGroupIdDiv .k-input', 'Nhóm thiết bị');
			iDFocus = "#ddlEquipGroupIdDiv .k-input";
		}
		if(msg.length==0){
			msg = Utils.getMessageOfRequireCheck('ddlEquipProviderIdDiv .k-input', 'NCC');
			iDFocus = "#ddlEquipProviderIdDiv .k-input";
		}
		if(msg.length==0){
			msg = Utils.getMessageOfRequireCheck('ddlPrice', 'Nguyên giá');
			iDFocus ="#ddlPrice";
		}
		if(msg.length==0){
			msg = Utils.getMessageOfRequireCheck('ddlManufacturing', 'Năm sản xuất');
			iDFocus = "#ddlManufacturing";
		}
		if(msg.length==0){
			msg = Utils.getMessageOfRequireCheck('ddlStockId', 'Kho');
			var stockCode = $("#stockCode").val().trim().toUpperCase();
			var stockName ="" ;
			try {
				stockName = $('#ddlStock option[value='+stockCode+']').text();
			} catch(e){
				msg = "Kho không tồn tại";
				stockName ="";
			}
			if(stockName.trim()!=""){
				$('#ddlStockName').val(stockName.trim());
				msg ="";
			} else {
				msg = "Kho không tồn tại";
				stockName ="";
			}
		}
		iDFocus = "#stockCode";
		if(msg.length==0){
			var curDate = ReportUtils.getCurrentDateString();
			var curDateStr = curDate.split("/");
			if($("#ddlManufacturing").val()>curDateStr[2]){
				msg = "Năm sản xuất phải cùng hoặc trước năm hiện tại";
				iDFocus = "#ddlManufacturing";
			}
			
		}
		if(msg.length==0){
			var ddlExpireDateStr = $('#ddlExpireDate').val().split("/");
			if(ddlExpireDateStr[2]<$("#ddlManufacturing").val()){
				msg = "Năm hết hạn bảo hành phải cùng hoặc sau năm sản xuất";
				iDFocus = "#ddlExpireDate";
			}
		}
		if(msg.length > 0){
			$('#errMsgEquipmentDlg').html(msg).show();
			setTimeout(function(){$('#errMsgEquipmentDlg').html('').hide();},1500);
			$(iDFocus).focus();
			
			return false;
		}
		var equipGroupID = $("#ddlEquipGroupId").data("kendoComboBox").value();
		var equipProviderId = $("#ddlEquipProviderId").data("kendoComboBox").value();
		
		var row = {
				serial:$('#ddlSeri').val(),
				healthStatus:$('#ddlHealthStatus').val(),
				healthStatusStr:$('#ddlHealthStatus option:selected').text(),
				equipGroupId:equipGroupID,
				equipGroupName:$('#ddlEquipGroupId option:selected').text(),
				equipCateGogyName:$('#ddlEquipCategoryName option[value='+equipGroupID+']').text(),
				capacity:$('#ddlCapacity option[value='+equipGroupID+']').text(),
				equipBrandName:$('#ddlBrandName option[value='+equipGroupID+']').text(),
				equipProviderId:equipProviderId,
				equipProviderName:$('#ddlEquipProviderId option[value='+equipProviderId+']').text(),
				manufacturingYear:$('#ddlManufacturing').val(),
				stockCode:$('#stockCode').val(),
				stockName:$('#ddlStockName').val(),
				stockId:$('#ddlStockId').val(),
				price:$('#ddlPrice').val(),
				warrantyExpiredDate:$('#ddlExpireDate').val()
		};
		
		var action = 'updateRow';
		var idx = EquipStockAdjustForm.currentGridIndexStockAdjustDtl;
		if(idx==null ){
			action ='insertRow';
			var rows = $('#gridEquipStockAdjustDtlItem').datagrid("getRows");
			if (rows.length > 0) {
	    		idx = rows.length - 1;
	    	}
		}
		
		$('#gridEquipStockAdjustDtlItem').datagrid(action,{row:row,index:idx});
		var data = $('#gridEquipStockAdjustDtlItem').datagrid('getRows');
		$('#gridEquipStockAdjustDtlItem').datagrid('loadData', data);
		$('#easyuiPopupSearchStockAdjust').dialog('close');
	},
	
	/**
	 * @author Datpv4
	 * @since July 09,2015
	 * @description them moi va cap nhat phieu 
	 */
	updateEquipStockAdjustRecord: function(id) {
		$('.SuccessMsgStyle').html('').hide();
		$('.ErrorMsgStyle').html('').hide();
		var row = $('#gridEquipStockAdjustDtlItem').datagrid('getRows');
		var rowsLength = row.length; 
		if(rowsLength==0){
			var msg="Vui lòng nhập thiết bị";
			if(msg.length > 0){
				$('.ErrorMsgStyle').html(msg).show();
				$('#errMsgEquipmentDlg').html(msg).show();
				setTimeout(function(){$('.ErrorMsgStyle').html('').hide();},1500);
				return false;
			}
		}
		
		var lstEquipStockAdjustDtlId = new Array();
		var lstEquipId = new Array();
		var lstSerial = new Array();
		var lstHealthStatus = new Array();
		var lstEquipGroupId = new Array();
		var lstEquipGroupName = new Array();
		var lstEquipCateGoryName = new Array();
		var lstCapacity = new Array();
		var lstEquipProviderId = new Array();
		var lstEquipProviderName = new Array();
		var lstManufacturingYear = new Array();
		var lstWarrantyExpiredDate = new Array();
		var lstStockId = new Array();
		var lstStockType = new Array();
		var lstStockName = new Array();
		var lstStockCode = new Array();
		var lstPrice = new Array();
		
		for (var i = 0; i < rowsLength; i++) {
			var msg ="";
			if (row[i] != null) {
				if(row[i].id!=undefined){
					lstEquipStockAdjustDtlId.push(row[i].id);
				}
				lstSerial.push(row[i].serial==undefined?" ":row[i].serial);
				
				lstHealthStatus.push(row[i].healthStatus==undefined?" ":row[i].healthStatus);
				if(row[i].equipGroupId!=undefined){
					lstEquipGroupId.push(row[i].equipGroupId);
				} 
				lstEquipGroupName.push(row[i].equipGroupName==undefined?" ":row[i].equipGroupName);
				lstEquipCateGoryName.push(row[i].equipCateGoryName==undefined?" ":row[i].equipCateGoryName);
				if(row[i].equipProviderId!=undefined){
					lstEquipProviderId.push(row[i].equipProviderId);
				} 
					
				lstEquipProviderName.push(row[i].equipProviderName==undefined?" ":row[i].equipProviderName);
				if(row[i].manufacturingYear!=undefined){
					lstManufacturingYear.push(row[i].manufacturingYear);
				} 
				if(row[i].warrantyExpiredDate!=undefined){
					lstWarrantyExpiredDate.push(row[i].warrantyExpiredDate);
				}
				if(row[i].stockId!=undefined){
					lstStockId.push(row[i].stockId==undefined?null:row[i].stockId);
				}
				lstStockName.push(row[i].stockName=undefined?" ":row[i].stockName);
				if(row[i].price!=undefined){
					lstPrice.push(row[i].price==undefined?null:row[i].price);
				}
				if(row[i].stockCode!=undefined){
					lstStockCode.push(row[i].stockCode==undefined?null:row[i].stockCode);
				}
			}
		}		
		var status = $('#cbxStatus').val();
		
		var dataModel = {};
		if(id!=undefined){
			dataModel.id = id;
		}
		
		if ($('#note').attr('maxlength') > 500) {
			$('#errMsgEquipmentDlg').html('Bạn nhập quá giới hạn của trường note').show();
			return false;
		}

		dataModel.status = status;
		dataModel.description = $('#txtDescription').val();
		dataModel.rowsLength = rowsLength;
		dataModel.lstEquipStockAdjustDtlId = lstEquipStockAdjustDtlId;
		dataModel.lstEquipId =lstEquipId;
		dataModel.lstSerial = lstSerial;
		dataModel.lstHealthStatus = lstHealthStatus;
		dataModel.lstEquipGroupId = lstEquipGroupId;
		dataModel.lstEquipGroupName = lstEquipGroupName;
		dataModel.lstEquipCateGoryName = lstEquipCateGoryName;
		dataModel.lstEquipProviderId = lstEquipProviderId;
		dataModel.lstEquipProviderName = lstEquipProviderName;
		dataModel.lstManufacturingYear = lstManufacturingYear;
		dataModel.lstWarrantyExpiredDate = lstWarrantyExpiredDate;
		dataModel.lstStockId = lstStockId;
		dataModel.lstStockType = lstStockType;
		dataModel.lstStockName = lstStockName;
		dataModel.lstStockCode = lstStockCode;
		dataModel.lstPrice = lstPrice;
		dataModel.note = $('#note').val();
		var titleErr = {title:'Lỗi các phiếu ',pageList:10};
		Utils.addOrSaveData(dataModel, '/equip-stock-adjust/update-stock-adjust-record', null, 'errMsgDetail', function(data) {
			if (data.error == false) {
				if (data.equipStockAdjustCode != undefined && data.equipStockAdjustCode != '') {
					$('#txtEquipStockAdjustCode').val(data.equipStockAdjustCode);
					// cap nhat lai Id cho btnUpdate neu muon chuyen, cap nhat lai phieu
					$('#btnUpdate').attr('onclick','EquipStockAdjustForm.updateEquipStockAdjustRecord('+data.equipStockAdjustId+');');
				}														
				if ( data.lstErr != undefined && data.lstErr != null && data.lstErr.length > 0 ) {
					VCommonJS.showDialogList({
						data : data.lstErr,
						dialogInfo: titleErr,
						columns : [[
							{field:'Index', title:'Dòng', align:'left', width: 60, sortable:false, resizable:false},
							{field:'formStatusErr', title:'Trạng thái', align:'left', width: 180, sortable:false, resizable:false, formatter: function(value,row,index){
								var html="";
								if(value == 1){
									html = '<img src="/resources/images/icon_delete.png" height="20" width="20" style="margin-right:5px;" title="Error"> ' + row.recordNotComplete;
								}
								return html;
							} }
						]]
					});	
				} else {
					$('#successMsgDetail').html('Lưu dữ liệu thành công').show();
					setTimeout(function(){$('#successMsgDetail').html('').hide();},1500);
					window.location.href="/equip-stock-adjust/info";
				}
			}
		}, null, null, null, null, null, true);
		
	},
	/**
	 * @author Datpv4
	 * @since July 09,2015
	 * @description cap nhat trang thai phieu
	 */
	updateStatus: function(id) {
		$('.SuccessMsgStyle').html('').hide();
		$('.ErrorMsgStyle').html('').hide();	
		$('#equipStockAdjustDtlTemplateDiv').hide();
		var statusForm = $('#cbxStatusUpdate').val().trim();
		if (statusForm < 0) {
			$('#errMsg').html('Vui lòng chọn trạng thái').show();
			return;
		}
		var arra = EquipStockAdjustForm._mapEquipStockAdjust.valArray;		
		var lstId = new Array();
		if (arra == null || arra.length == 0) {
			$('#errMsg').html('Bạn chưa chọn phiếu cập nhật kho điều chỉnh để cập nhật. Vui lòng chọn phiếu.').show();
			return;
		}		
		for (var i=0, size = arra.length; i < size; ++i) {
			lstId.push(arra[i].id);			
		}
		var params = new Object();
		params.lstId = lstId;
		params.statusForm = statusForm;
		var title = {title:'Lỗi lưu trạng thái cập nhật kho điều chỉnh',pageList:10};
		Utils.addOrSaveData(params, '/equip-stock-adjust/updateStatus', null, 'errMsg', function(data) {
			if ( data.lstErr != undefined && data.lstErr != null && data.lstErr.length > 0 ) {
				VCommonJS.showDialogList({
				    data : data.lstErr,
				    dialogInfo: title,
				    columns : [[
				        {field:'formCode', title:'Mã biên bản', align:'left', width: 120, sortable:false, resizable:false},
				        {field:'formStatusErr', title:'Trạng thái biên bản', align:'left', width: 180, sortable:false, resizable:false, formatter: function(value,row,index){
				        	var html="";
							if(value == 1){
								html = '<img src="/resources/images/icon_delete.png" height="20" width="20" style="margin-right:5px;" title="Error"> ' + row.recordNotComplete;
							}
							return html;
						} }
					]]
				});	
			} else {
				$('#successMsg').html('Lưu dữ liệu thành công').show();
				setTimeout(function(){$('#successMsg').html('').hide();},1500);
			}
			EquipStockAdjustForm._mapEquipStockAdjust = new Map();
			$('#grid').datagrid('reload');
		}, null, true,null, 'Bạn có muốn cập nhật phiếu cập nhật kho điều chỉnh?', null, true);
	},
	
	/**
	 * @author Datpv4
	 * @since July 10,2015
	 * @description Export phieu 
	 */
	exportEquipStockAdjust : function(){
		$('#equipStockAdjustDtlTemplateDiv').hide();
		var status = $('#cbxStatus').val();
		var code = $('#txtCode').val();
		var fDate = $('#fDate').val();
		var tDate =$('#tDate').val();
		var msg ='';
		if (msg.length == 0 && $('#fDate').val().trim().length > 0 && !Utils.isDate($('#fDate').val(),'/')) {
			msg = 'Ngày tạo từ không tồn tại hoặc không đúng định dạng dd/mm/yyyy.';
			err = '#fDate';
		}
		if (msg.length == 0 && $('#tDate').val().trim().length > 0 && !Utils.isDate($('#tDate').val(),'/')) {
			msg = 'Đến ngày không tồn tại hoặc không đúng định dạng dd/mm/yyyy.';
			err = '#tDate';
		}
		if (msg.length==0 && fDate.length > 0 && tDate.length>0 && !Utils.compareDate(fDate, tDate)) {
			msg = 'Ngày tạo từ phải là ngày trước hoặc cùng ngày với đến ngày';
		}
		if(msg.length>0){
			$('#errMsg').html(msg).show();
			$(err).focus();
			setTimeout(function(){$('#errMsg').hide();},3000);
			return false;
		}
		var arra = EquipStockAdjustForm._mapEquipStockAdjust.valArray;		
		var lstId = new Array();
		
		for (var i=0, size = arra.length; i < size; ++i) {
			lstId.push(arra[i].id);			
		}
		var dataModel = new Object();		
		dataModel.code=code;		
		dataModel.status=status; 			
		dataModel.fromDate=fDate;
		dataModel.toDate=tDate;
		dataModel.isCreate=EquipStockAdjustForm._isCreater;
		dataModel.lstId = lstId; 
		ReportUtils.exportReport('/equip-stock-adjust/exportEquipStockAdjust',dataModel);
	},
	
	/**
	 * @author Datpv4
	 * @since July 10,2015
	 * @description Download file template
	 */
	downloadTemplateExcel : function(){
		$('#equipStockAdjustDtlTemplateDiv').hide();
		var dataModel = new Object();	
		ReportUtils.exportReport('/equip-stock-adjust/downloadTemplateExcel',dataModel);
	},
	
	openDialogImportStockAdjust: function (){
		$('#equipStockAdjustDtlTemplateDiv').hide();
		$('.SuccessMsgStyle').html('').hide();
		$('.ErrorMsgStyle').html('').hide();
		$('#easyuiPopupImportExcel').dialog({
			title: 'Nhập excel Quản lý cập nhật kho điều chỉnh',
			width: 465, 
			height: 'auto',
			closed: false,
			cache: false,
			modal: true,
			onOpen: function() {
				
			},
			onClose: function() {
				$('.ErrorMsgStyle').html('').hide();
				$("#grid").datagrid('uncheckAll');
				$("#fakefilepcListEquip").val("");
				$("#excelFileListEquip").val("");
			}
		});
	},
	
	/**
	 * @author Datpv4
	 * @since July 10,2015
	 * @description import moi mot phieu cap nhat kho dieu chinh
	 */
	importExcelEquipStockAdjust: function(){
		$('.SuccessMsgStyle').html('').hide();
		$('.ErrorMsgStyle').html('').hide();
		if($('#fakefilepcListEquip').val().length <= 0 ){
			$('#errExcelMsgListEquip').html('Vui lòng chọn tập tin Excel').show();
			return false;
		}
		Utils.importExcelUtils(function(data){
			$('#btnSearch').click();
			$('#fakefilepcListEquip').val("").change();
			if (data.message != undefined && data.message != null && data.message.trim().length > 0) {
				$('#errExcelMsgListEquip').html(data.message.trim()).change().show();
			}
		}, 'importFrmListEquip', 'excelFileListEquip', 'errExcelMsgListEquip');
		
		
	},
};
/*
 * END OF FILE - /web/web/resources/scripts/business/equipment/vnm.equip-stock-adjust.js
 */

/*
 * START OF FILE - /web/web/resources/scripts/business/equipment/vnm.equipment-suggest-eviction.js
 */
/**
 * JS thuc hien nghiep vu Quan Ly De Nghi Thu Hoi Thiet Bi
 *
 * @author nhutnn
 * @since 13/07/2015
 * */
var EquipmentSuggestEviction = {
	_mapRecored: null,
	_editIndex: null,
	_lstEquip: null,
	_lstStaff: null,
	_mapEviction: null,

	/**
	 * Tim kiem danh sach de nghi thu hoi
	 *
	 * @author nhutnn
	 * @since 13/07/2015
	 * */
	searchSuggestEviction: function() {
		$('.ErrorMsgStyle').html("").hide();
		var msg = '';
		var params = new Object();
		var multiShop = $("#shop").data("kendoMultiSelect");
		var dataShop = multiShop.dataItems();
		if (dataShop != null && dataShop.length > 0) {
			var lstShop = new Array();
			//var curShopCode = $('#curShopCode').val();
			for (var i = 0; i < dataShop.length; i++) {
				//if (dataShop[i].shopCode != curShopCode) {
					lstShop.push(dataShop[i].shopCode);
				//}
			}
			params.lstShop = lstShop.join(",");
		} else {
			msg = "Bạn chưa chọn Đơn vị!";
			err = '#shop';
		}
		if (msg.length == 0) {
			msg = Utils.getMessageOfSpecialCharactersValidate('code', 'Mã biên bản', Utils._CODE);
			err = "#code";
		}
		if (msg.length == 0) {
			msg = Utils.getMessageOfInvalidFormatDate("fDate","Từ ngày");
			err = "#fDate";
		}
		if (msg.length == 0) {
			msg = Utils.getMessageOfInvalidFormatDate("tDate","Đến ngày");
			err = "#tDate";
		}		
		
		var fDate = $('#fDate').val().trim();
		var tDate = $('#tDate').val().trim();		
		if (msg.length == 0 && fDate.length > 0 && tDate.length > 0 && !Utils.compareDate(fDate, tDate)) {
			msg = 'Từ ngày phải là ngày trước hoặc cùng ngày với đến ngày';
			err = '#fDate';
		}
		if (msg.length > 0) {
			$('#errMsgSuggestEviction').html(msg).show();
			$(err).focus();
			return false;
		}
		params.code = $('#code').val().trim();
		params.statusRecord = $('#status').val().trim();
		params.flagRecordStatus = $('#statusApproved_flag').val();
		params.fromDate = $('#fDate').val().trim();
		params.toDate = $('#tDate').val().trim();
		params.statusDelivery = $('#statusDelivery').val().trim();
		EquipmentSuggestEviction._mapRecored = new Map();
		$('#gridSuggestEviction').datagrid("uncheckAll");
		$('#gridSuggestEviction').datagrid("load", params);
		return false;
	},
	/**
	 * Luu thay doi trang thai bien ban
	 * 
	 * @author nhutnn
	 * @since 15/07/2015
	 */
	saveRecordChange: function() {
		$('.ErrorMsgStyle').html("").hide();
		var rows = $('#gridSuggestEviction').datagrid('getRows');
		if (rows.length == 0) {
			$('#errMsgSuggestEviction').html('Không có dữ liệu để lưu').show();
			return false;
		}
		if (EquipmentSuggestEviction._mapRecored.keyArray.length == 0) {
			$('#errMsgSuggestEviction').html('Bạn chưa chọn biên bản. Vui lòng chọn biên bản!').show();
			return false;
		}
		var statusRecordChange = $('#statusRecordLabel').val();
		if (statusRecordChange == undefined || statusRecordChange == null || statusRecordChange == "") {
			$('#errMsgSuggestEviction').html('Bạn chưa chọn trạng thái để lưu biên bản!').show();
			return false;
		}

		var lstIdRecord = new Array();
		for (var i = 0; i < EquipmentSuggestEviction._mapRecored.keyArray.length; i++) {
			lstIdRecord.push(EquipmentSuggestEviction._mapRecored.keyArray[i]);
		}
		var params = new Object();
		params.lstIdRecord = lstIdRecord;
		params.statusRecord = statusRecordChange;
		Utils.addOrSaveData(params, '/equip-suggest-eviction/save-record-change', null, 'errMsgSuggestEviction', function(data) {
			if (data.error) {
				$('#errMsgSuggestEviction').html(data.errMsg).show();
				return;
			}
			if (data.lstRecordError != null && data.lstRecordError.length > 0) {
				VCommonJS.showDialogList({
					dialogInfo:  { title: 'Thông tin lỗi'},
					data: data.lstRecordError,
					columns: [[
						{field: 'formCode', title: 'Mã biên bản', align: 'left', width: 120, sortable: false, resizable: false}, 
						{field: 'formStatusErr', title: 'Trạng thái biên bản', align: 'center', width: 60, sortable: false, resizable: false, formatter: function(value, row, index) {
							var html = "";
							if (row.formStatusErr == 1) {
								html = '<img src="/resources/images/icon_delete.png" height="20" width="20" style="margin-right:5px;" title="Error">';
							}
							return html;
						}},
						{field:'equipTrading', title:'Danh sách thiết bị đang tham gia giao dịch', align:'left', width:120, sortable:false, resizable:false,formatter: function(value, row, index){
							var html = "";
							if (row.equipTrading != undefined && row.equipTrading != null) {
								html = Utils.XSSEncode(row.equipTrading);
							}
							return html;
						}}
					]]
				});
			} else {
				$('#successMsgSuggestEviction').html('Lưu dữ liệu thành công').show();
				setTimeout(function() {
					$('#successMsgSuggestEviction').html('').hide();
				}, 3000);
			}
			EquipmentSuggestEviction._mapRecored = new Map();
			$('#gridSuggestEviction').datagrid('reload');
			$('#gridSuggestEviction').datagrid('uncheckAll');
		}, null, null, null, "Bạn có muốn cập nhật dữ liệu?", null);
	},
	/**
	 * Xuat file excel
	 * @author nhutnn
	 * @since 20/07/2015
	 */
	exportEquipSuggestEviction: function() {
		$('.ErrorMsgStyle').html("").hide();
		if (EquipmentSuggestEviction._mapRecored == null || EquipmentSuggestEviction._mapRecored.keyArray.length == 0) {
			$('#errMsgSuggestEviction').html('Bạn chưa chọn biên bản. Vui lòng chọn biên bản!').show();
			return false;
		}

		var lstIdRecord = new Array();
		for (var i = 0; i < EquipmentSuggestEviction._mapRecored.keyArray.length; i++) {
			lstIdRecord.push(EquipmentSuggestEviction._mapRecored.keyArray[i]);
		}
		var params = new Object();
		params.lstIdRecord = lstIdRecord;		
		ReportUtils.exportReport('/equip-suggest-eviction/export-excel', params, "errMsgSuggestEviction");
		$('#gridSuggestEviction').datagrid('uncheckAll');
		EquipmentSuggestEviction._mapRecored = new Map();
	},
	/**
	 * Show popup import
	 * @author nhutnn
	 * @since 22/07/2015
	 */
	showDlgImportExcel: function(){
		$('#easyuiPopupImportExcel').show();
		$('.ErrorMsgStyle').hide();
		$('#easyuiPopupImportExcel').dialog({  
	        closed: false,  
	        cache: false,  
	        modal: true,
	        height:'auto',
	        onOpen: function(){	 
	        	$('.easyui-dialog .BtnGeneralStyle').css('color','');
	        },
	        onClose: function(){
	        	EquipmentSuggestEviction.searchSuggestEviction();
				$("#gridSuggestEviction").datagrid('uncheckAll');
				$("#fakefilepcBBDNTH").val("");
				$("#excelFileBBDNTH").val("");				
	        }
		});
	},
	/**
	 * import excel
	 * @author nhutnn
	 * @since 22/07/2015
	 */
	importExcelBBDNTH: function(){
		$('.ErrorMsgStyle').hide();
		VTUtilJS.importExcelUtils(function(data) {
			$("#errExcelMsgBBDNTH").html(data.message).show();		
			if ($("#errExcelMsgBBDNTH span").length > 0 && (data.fileNameFail == null || data.fileNameFail == "")) {
				setTimeout(function(){
					$('#errExcelMsgBBDNTH').html("").hide();
					$('#easyuiPopupImportExcel').dialog('close');
				}, 3000);				
			}else{
				$("#fakefilepcBBDNTH").val("");
				$("#excelFileBBDNTH").val("");							
			}						
		}, "importFrmBBDNTH", "excelFileBBDNTH", null, "errExcelMsgBBDNTH");
		return false;
	},	
	/**
	 * Xoa dong chi tiet
	 * @author nhutnn
	 * @since 23/07/2015
	 */
	deleteRowEquipLendDetail: function(index){	
		$('.ErrorMsgStyle').html('').hide();	
		$.messager.confirm('Xác nhận', "Bạn có muốn xóa dữ liệu?", function(r){
			if(r){
				if(EquipmentSuggestEviction._editIndex != null){
					if(EquipmentSuggestEviction._editIndex == index){		
						EquipmentSuggestEviction._editIndex = null;
					} else if (EquipmentSuggestEviction._editIndex > index){
						// them dong moi
						var row = EquipmentSuggestEviction.getDetailEdit();
						$('#gridEquipSuggestEvictionChange').datagrid('updateRow', {
							index: EquipmentSuggestEviction._editIndex,
							row: row
						});
						
						EquipmentSuggestEviction._editIndex--;
					}
				}
				// xoa
				$('#gridEquipSuggestEvictionChange').datagrid('deleteRow', index);
				var rows = $("#gridEquipSuggestEvictionChange").datagrid("getRows");	
				$("#gridEquipSuggestEvictionChange").datagrid("loadData",rows);

				if(EquipmentSuggestEviction._editIndex != null){
					$('#gridEquipSuggestEvictionChange').datagrid('beginEdit', EquipmentSuggestEviction._editIndex);
					EquipmentSuggestEviction.addEditorInGridChangeLend();
				}
			}
		});
	},
	/**
	 * Them khach hang vao dong chi tiet
	 * @author nhutnn
	 * @since 23/07/2015
	 */
	addCustomerInfo: function(id, customerCode, customerCodeLong, customerName, phone, mobilephone, shopMienName, shopKenhName, shopCode, shopName){
		$('#divOverlay').show();		
		if (EquipmentSuggestEviction._editIndex != null) {
			var row = EquipmentSuggestEviction.getDetailEdit();
			row.customerId = id;	
			row.customerCode = customerCode;		
			row.customerCodeLong = customerCodeLong;
			row.customerName = customerName;
			if (phone != null && phone != "" && mobilephone != null && mobilephone != "") {
				row.mobile = phone + " / " + mobilephone;
			} else if (phone != null && phone != "") {
				row.mobile = phone;
			} else if (mobilephone != null && mobilephone != "") {
				row.mobile = mobilephone;
			} else {
				row.mobile = "";
			}
			row.shopCode = shopCode;
			row.shopName = shopName;
			row.shopCodeMien = shopMienName;
			//row.shopCodeKenh = shopKenhName;

			row.equipCode = "";
			row.equipSeri = "";
			row.equipGroup = "";
			row.healthStatus = "";

			row.relation = "";
			row.representative = "";
			row.ward = "";
			row.district = "";
			row.province = "";
			row.address = "";
			row.street = "";

			row.staffCode = "";
			row.staffName = "";
			row.staffPhone = "";
			row.staffMobile = "";
			row.phone = "";

			// them dong moi			
			$('#gridEquipSuggestEvictionChange').datagrid('updateRow', {
				index: EquipmentSuggestEviction._editIndex,
				row: row
			});

			$('#gridEquipSuggestEvictionChange').datagrid('beginEdit', EquipmentSuggestEviction._editIndex);
			EquipmentSuggestEviction.addEditorInGridChangeLend();
			var ed = $('#gridEquipSuggestEvictionChange').datagrid('getEditors', EquipmentSuggestEviction._editIndex);
			$(ed[2].target).combobox('clear');
			$(ed[6].target).combobox('clear');
			if(customerCode.length > 0 && shopCode.length > 0 && id != null) {
				// EquipmentSuggestEviction.getLstEquip(customerCode, shopCode, id);
				EquipmentSuggestEviction.getLstGSNPP(customerCode, shopCode, id);
			}		
		}
		$('#divOverlay').hide();
		$('.easyui-dialog').dialog('close');
	},
	
	/**
	 * Lay thong tin thiet bi
	 * @author nhutnn
	 * @since 23/07/2015
	 */	
	getLstEquip: function(shortCode, shopCode, id) {
		var params = new Object();
		params.shopCode = shopCode;
		params.shortCode = shortCode;
		params.stockId = id;
		var ed = $('#gridEquipSuggestEvictionChange').datagrid('getEditors', EquipmentSuggestEviction._editIndex);
		Utils.getJSONDataByAjax(params, '/equip-suggest-eviction/get-list-equip', function(result) {
			var row = $('#gridEquipSuggestEvictionChange').datagrid('getRows')[EquipmentSuggestEviction._editIndex];
			if (result.lstEquip != undefined && result.lstEquip != null && result.lstEquip.length > 0) {
				// co thiet bi trong kho khach hang						
				EquipmentSuggestEviction._lstEquip = result.lstEquip;
				if (row.equipCode != undefined && row.equipCode != null && row.equipCode != "") {
					var isExist = false;
					for (var i = 0; i < result.lstEquip.length; i++) {
						if (result.lstEquip[i].code == row.equipCode) {
							isExist = true;
							break;
						}
					}
					if (!isExist) {
						var t = {
							code: row.equipCode,
							serial: row.equipSeri,
							equipGroupName: row.equipGroup,
							healthStatus: row.healthStatus
						};
						result.lstEquip.push(t);
					}
				}
				$(ed[2].target).combobox('loadData', result.lstEquip);
				if (row.equipCode != undefined && row.equipCode != null && row.equipCode != "") {
					$(ed[2].target).combobox("select", row.equipCode);
				} else {
					$(ed[2].target).combobox("select", result.lstEquip[0].code);
				}
			} else {
				// khong thay thiet bi trong kho khach hang
				if (row.equipCode != undefined && row.equipCode != null && row.equipCode != "") {
					result.lstEquip = new Array();
					EquipmentSuggestEviction._lstEquip = result.lstEquip;
					var t = {
						code: row.equipCode,
						serial: row.equipSeri,
						equipGroupName: row.equipGroup,
						healthStatus: row.healthStatus
					};
					result.lstEquip.push(t);
					$(ed[2].target).combobox('loadData', result.lstEquip);
					$(ed[2].target).combobox("select", row.equipCode);
				} else {
					$(ed[2].target).combobox('loadData', []);
					EquipmentSuggestEviction._lstEquip = [];
				}
			}
		}, null, null);
	},
	/**
	 * Lay thong tin giam sat
	 * @author nhutnn
	 * @since 23/07/2015
	 */	
	getLstGSNPP: function(shortCode, shopCode, id) {
		var params = new Object();
		params.shopCode = shopCode;
		var ed = $('#gridEquipSuggestEvictionChange').datagrid('getEditors', EquipmentSuggestEviction._editIndex);
		Utils.getJSONDataByAjax(params,'/equip-suggest-eviction/get-list-gsnpp',function(result) {
			if (result.lstStaff != undefined && result.lstStaff != null && result.lstStaff.length > 0) {
				for (var i = 0, sz = result.lstStaff.length; i < sz; i++) {
					var obj = result.lstStaff[i];
					obj.nameText = obj.staffCode +" - "+ obj.staffName;					
				}							
				EquipmentSuggestEviction._lstStaff = result.lstStaff;
				var row = $('#gridEquipSuggestEvictionChange').datagrid('getRows')[EquipmentSuggestEviction._editIndex];
				$(ed[6].target).combobox('loadData', result.lstStaff);
				if(row.staffCode != undefined && row.staffCode != null && row.staffCode != "" ){
					$(ed[6].target).combobox("select", row.staffCode);
				}
			} else {
				EquipmentSuggestEviction._lstStaff = [];
				$(ed[6].target).combobox('loadData', []);
			}	
			
			EquipmentSuggestEviction.getLstEquip(shortCode, shopCode, id);	
		}, null, null);
	},
	/**
	 * Them editor tren dong them moi
	 * @author nhutnn
	 * @since 08/07/2015
	 */
	addEditorInGridChangeLend: function() {
		var ed = $('#gridEquipSuggestEvictionChange').datagrid('getEditors', EquipmentSuggestEviction._editIndex);
		var row = $('#gridEquipSuggestEvictionChange').datagrid('getRows')[EquipmentSuggestEviction._editIndex];
    	
		var getCustomer = function() {
			$(".ErrorMsgStyle").html('').hide();
			if ($(ed[0].target).text('getValue').val() != null && $(ed[0].target).text('getValue').val() != "") {
				if ($(ed[1].target).text('getValue').val() != null && $(ed[1].target).text('getValue').val() != "") {
					var params = new Object();
					params.isNPP = true;
					params.shopCode = $(ed[0].target).text('getValue').val();
					params.page = 1;
					params.max = 10;
					params.shortCode = $(ed[1].target).text('getValue').val();

					Utils.getJSONDataByAjax(params, '/equip-suggest-eviction/search-customer', function(result) {
						if (result.rows != null && result.rows.length > 0) {
							var row = result.rows[0];
							var phone = row.phone == null ? "" : row.phone;
							var mobilephone = row.mobiphone == null ? "" : row.mobiphone;

							EquipmentSuggestEviction.addCustomerInfo(row.id, row.shortCode, row.customerCode, row.customerName, phone, mobilephone, row.shopMienCode, row.shopKenhCode, row.shopCode, row.shopName);
						} else {
							EquipmentSuggestEviction.addCustomerInfo("", "", "", "", "", "", "", "", $(ed[0].target).text('getValue').val(), "");
							$('#errMsgChangeSuggestEviction').html("Không tìm thấy mã khách hàng trong hệ thống!").show();
							return false;
						}
						$(ed[1].target).focus();
					});
				}
			} else {
				$('#errMsgChangeSuggestEviction').html("Bạn chưa nhập mã NPP!").show();
				$(ed[0].target).focus();
				return false;
			}
		};
    	// Ma khach hang
		$(ed[1].target).bind('keyup', function(event) {
			if (event.keyCode == keyCode_F9) {
				$(".ErrorMsgStyle").html('').hide();
				// if($(ed[0].target).text('getValue').val() != null && $(ed[0].target).text('getValue').val() != ""){
					VCommonJS.showDialogSearch2({
						 params : {
						 	shopCodeEditor: $(ed[0].target).text('getValue').val() == null? "" : $(ed[0].target).text('getValue').val().trim(),
						 },
					    inputs : [
					    	{id:'shopCode', maxlength:50, label:'Mã đơn vị'},
					        {id:'shopName', maxlength:250, label:'Tên đơn vị'},
					        {id:'shortCode', maxlength:50, label:'Mã KH'},
					        {id:'customerName', maxlength:250, label:'Tên KH'},				       
					    ],			   
					    url : '/equip-suggest-eviction/search-customer',			   
					    columns : [[
					    	{field:'shopCode', title:'Đơn vị', align:'left', width: 150, sortable:false, resizable:false, formatter:function(value,row,index) {
					        	return Utils.XSSEncode(row.shopCode + " - " + row.shopName);
					        }},
					        {field:'shortCode', title:'Khách hàng', align:'left', width: 150, sortable:false, resizable:false, formatter:function(value,row,index) {
					        	return Utils.XSSEncode(row.shortCode + " - " + row.customerName);
					        }},
					        {field:'address', title:'Địa chỉ', align:'left', width: 150, sortable:false, resizable:false},				        
					        {field:'choose', title:'', align:'center', width:40, sortable:false, resizable:false, formatter:function(value,row,index) {
					        	var phone = row.phone == null ? "" : row.phone;
					        	var mobilephone = row.mobiphone == null ? "" : row.mobiphone;
					        	return "<a href='javascript:void(0)' onclick=\"return EquipmentSuggestEviction.addCustomerInfo('"+row.id+"','"+row.shortCode+"','"+row.customerCode+"','"+row.customerName+"','"+phone+"','"+mobilephone+"','"+row.shopMienCode+"','"+row.shopKenhCode+"','"+row.shopCode+"','"+row.shopName+"');\">Chọn</a>";        
					        }}
					    ]]
					});
					$(".easyui-dialog #code").focus();
				// } else {
				// 	$('#errMsgChangeSuggestEviction').html("Bạn chưa nhập mã đơn vị!").show();
				// 	$(ed[0].target).focus();
				// 	return false;
				// }
			} else if (event.keyCode == keyCodes.ENTER) {
				getCustomer();
			}
		});		

		$(ed[1].target).keydown(function(e) {
			var keyCode = e.which || e.keyCode; // for cross-browser compatibility
			if (keyCode == keyCodes.TAB) {
				getCustomer();
			}
		});

		if ($(ed[2].target).combobox('getData').length == 0 && EquipmentSuggestEviction._lstEquip != null) {
			$(ed[2].target).combobox('loadData', EquipmentSuggestEviction._lstEquip);
			if (row.equipCode != undefined && row.equipCode != null && row.equipCode != "") {
				$(ed[2].target).combobox("select", row.equipCode);
			}
		}
		if (row.stockTypeValue != undefined && row.stockTypeValue != null && row.stockTypeValue != "") {
			$(ed[4].target).combobox("select", row.stockTypeValue);
		}
		if ($(ed[6].target).combobox('getData').length == 0 && EquipmentSuggestEviction._lstStaff != null) {
			$(ed[6].target).combobox('loadData', EquipmentSuggestEviction._lstStaff);
			if (row.staffCode != undefined && row.staffCode != null && row.staffCode != "") {
				$(ed[6].target).combobox("select", row.staffCode);
			}
		}

		// thoi gian thu hoi tu
		$($(ed[3].target).datebox('textbox')).attr('id', 'evictionDate' + EquipmentSuggestEviction._editIndex).attr("maxlength", "10");
		Utils.bindFormatOnTextfield('evictionDate' + EquipmentSuggestEviction._editIndex, Utils._TF_NUMBER_CONVFACT);
	},
	/**
	 * Validate dong them moi
	 * @author nhutnn
	 * @since 08/07/2015
	 */
	validateEquipmentInGrid: function() {
		var currentDate = Utils.currentDate();
		var ed = $('#gridEquipSuggestEvictionChange').datagrid('getEditors', EquipmentSuggestEviction._editIndex);
		var msg = "";
		// ma NPP
		if (msg.length == 0 && ($(ed[0].target).text('getValue').val() == null || $(ed[0].target).text('getValue').val() == "")) {
			msg = "Bạn chưa nhập mã NPP!";
			$(ed[0].target).focus();
		}
		// ma kh
		if (msg.length == 0 && ($(ed[1].target).text('getValue').val() == null || $(ed[1].target).text('getValue').val() == "")) {
			msg = "Bạn chưa nhập mã khách hàng!";
			$(ed[1].target).focus();
		}
		// ma thiet bi
		if (msg.length == 0 && ($(ed[2].target).combobox('getValue') == null || $(ed[2].target).combobox('getValue') == "")) {
			msg = "Bạn chưa chọn thiết bị!";
			$(ed[2].target).focus();
		}
		// thoi gian thu hoi
		if (msg.length == 0 && ($(ed[3].target).datebox('getValue') == null || $(ed[3].target).datebox('getValue') == "")) {
			msg = "Bạn chưa nhập thời gian muốn thu hồi thiết bị!";
			$(ed[3].target).focus();
		}		
		if (msg.length == 0 && $(ed[3].target).datebox('getValue').length > 0 && !Utils.isDate($(ed[3].target).datebox('getValue'), '/')) {
			msg = format(msgErr_invalid_format_date,"Thời gian muốn thu hồi thiết bị");
			$(ed[3].target).focus();
		}		
		if (msg.length == 0 && $(ed[3].target).datebox('getValue').length > 0 && Utils.compareDateForTowDate($(ed[3].target).datebox('getValue'), currentDate) < 0) {
			msg = 'Thời gian muốn thu hồi thiết bị phải là ngày sau hoặc bằng ngày hiện tại';
			$(ed[3].target).focus();
		}
		// kho nhan
		if (msg.length == 0 && ($(ed[4].target).combobox('getValue') == null || $(ed[4].target).combobox('getValue') == "")) {
			msg = "Bạn chưa nhập kho nhận!";
			$(ed[4].target).focus();
		}
		// ly do
		if (msg.length == 0 && ($(ed[5].target).text('getValue').val() == null || $(ed[5].target).text('getValue').val() == "")) {
			msg = "Bạn chưa nhập lý do!";
			$(ed[5].target).focus();
		}
		// ma giam sat
		if (msg.length == 0 && ($(ed[6].target).combobox('getValue') == null || $(ed[6].target).combobox('getValue') == "")) {
			msg = "Bạn chưa chọn mã giám sát!";
			$(ed[6].target).focus();
		}

		return msg;
	},
	/**
	 * Them dong chi tiet
	 * @author nhutnn
	 * @since 07/07/2015
	 */
	insertEquipmentInGrid: function() {
		$('.ErrorMsgStyle').html('').hide();		
		if (EquipmentSuggestEviction._editIndex != null ) {
			var msg = EquipmentSuggestEviction.validateEquipmentInGrid();
			if (msg.length > 0) {
				$('#errMsgChangeSuggestEviction').html(msg).show();
				return false;
			}
			// them dong moi
			var row = EquipmentSuggestEviction.getDetailEdit();
			$('#gridEquipSuggestEvictionChange').datagrid('updateRow', {
				index: EquipmentSuggestEviction._editIndex,
				row: row
			});
		}
		var newRow = {timeEviction: getCurrentDate()};
		$('#gridEquipSuggestEvictionChange').datagrid("appendRow", newRow);
		EquipmentSuggestEviction._lstStaff = null;
		EquipmentSuggestEviction._lstEquip = null;
		var sz = $("#gridEquipSuggestEvictionChange").datagrid("getRows").length;
		EquipmentSuggestEviction._editIndex = sz-1;
		$('#gridEquipSuggestEvictionChange').datagrid('beginEdit', EquipmentSuggestEviction._editIndex);
		EquipmentSuggestEviction.addEditorInGridChangeLend();

	},
	/**
	 * Lay thong dong chi tiet them
	 * @author nhutnn
	 * @since 08/07/2015
	 */
	getDetailEdit: function(){
		var ed = $('#gridEquipSuggestEvictionChange').datagrid('getEditors', EquipmentSuggestEviction._editIndex);
		var row = $('#gridEquipSuggestEvictionChange').datagrid("getRows")[EquipmentSuggestEviction._editIndex];

		row.shopCode = $(ed[0].target).text("getValue").val().trim();
		row.customerCode = $(ed[1].target).text("getValue").val().trim();
		if($(ed[2].target).combobox("getValue") != null && $(ed[2].target).combobox("getValue") != ""){
			row.equipCode = $(ed[2].target).combobox("getValue");
		}
		
		row.timeEviction = $(ed[3].target).datebox("getValue").trim();

		row.stockType = $(ed[4].target).combobox("getText").trim();
		if($(ed[4].target).combobox("getValue") != null && $(ed[4].target).combobox("getValue") != ""){
			row.stockTypeValue = $(ed[4].target).combobox("getValue");
		}		

		row.reason = $(ed[5].target).text("getValue").val().trim();
		if($(ed[6].target).combobox("getValue") != null && $(ed[6].target).combobox("getValue") != ""){
			row.staffCode = $(ed[6].target).combobox("getValue");
		}		

		return row;
	},
	/**
	 * Chinh sua bien ban
	 * @author nhutnn
	 * @since 08/07/2015
	 */
	updateRecord: function(){
		$('.ErrorMsgStyle').html('').hide();
		var msg = "";
		var err = "";
		if (msg.length == 0 && ($("#status").val() == undefined || $("#status").val() == null || $("#status").val() == "")) {
			msg = 'Bạn chưa chọn trạng thái!';
			err = '#status';
		}
		if (msg.length == 0 && $('#createFormDate').val().trim().length > 0 && !Utils.isDate($('#createFormDate').val(), '/')) {
			msg = 'Ngày biên bản không tồn tại hoặc không đúng định dạng dd/mm/yyyy.';
			err = '#createFormDate';
		}
		var currentDate = Utils.currentDate();
		if (msg.length == 0 && $('#createFormDate').val().length > 0 && Utils.compareDateForTowDate($('#createFormDate').val(), currentDate) > 0) {
			msg = 'Ngày biên bản phải là ngày trước hoặc cùng ngày với ngày hiện tại';
			err = '#createFormDate';
		}
		var rows = $('#gridEquipSuggestEvictionChange').datagrid("getRows");
		if(msg.length == 0 && rows.length == 0){
			msg = 'Bạn chưa thêm chi tiết biên bản';
		}
		if (msg.length == 0){
			if(EquipmentSuggestEviction._editIndex != null){
				msg = EquipmentSuggestEviction.validateEquipmentInGrid();
				if(msg.length > 0){
					$('#errMsgChangeSuggestEviction').html(msg).show();
					return false;
				}				
				// them dong moi
				var row = EquipmentSuggestEviction.getDetailEdit();
				rows[EquipmentSuggestEviction._editIndex] = row;
				//$('#gridEquipSuggestEvictionChange').datagrid("updateRow", {index: EquipmentSuggestEviction._editIndex, row: row});
			}			
		}	

		if (msg.length == 0 && $('#note').attr('maxlength') > 500) {
			msg = 'Bạn nhập quá giới hạn của trường note';
		}

		if(msg.length > 0){
			$('#errMsgChangeSuggestEviction').html(msg).show();
			$(err).focus();
			return false;
		}
		
		var params = new Object();
		params.id = $("#idEquipSuggestEviction").val();
		params.lstDetails = rows;
		params.statusRecord = $("#status").val();
		params.createFormDate = $("#createFormDate").val();
		params.note = $('#note').val();
		//params = JSONUtil.getSimpleObject(params);
		JSONUtil.saveData2(params, "/equip-suggest-eviction/update", "Bạn có muốn cập nhật?", "errMsgChangeSuggestEviction", function(data){
			if(data.error){
				$("errMsgChangeSuggestEviction").html(data.errMsg).show();
			} else {
				$("#successMsgChangeSuggestEviction").html("Lưu dữ liệu thành công").show();
				setTimeout(function(){					
					if(window.location.pathname == "/equip-suggest-eviction/edit"){
						window.location.href = '/equip-suggest-eviction/info';	
					} else if(window.location.pathname == "/equip-suggest-eviction/change"){
						window.location.href = '/equip-suggest-eviction/approve';	
					}					
				}, 3500);
			}
		},null,null);
	},
	/**
	 * Xuat lai bien ban hop dong giao nhan lan nua
	 *
	 * @author nhutnn
	 * @since 10/07/2015
	 */
	viewPrintEquipSuggestEviction: function(id){
		$('.ErrorMsgStyle').html('').hide();
		
		var lstIdRecord = null;
		if(id != null){			
			lstIdRecord = new Array();
			lstIdRecord.push(id);
		}else{
			$('#errMsgSuggestEviction').html('Vui lòng chọn biên bản đã duyệt để xem bản in!').show();
		}		
		
		var params = new Object();
		params.lstIdRecord = lstIdRecord;	
		ReportUtils.exportReport('/equip-suggest-eviction/view-print', params);
		return false;
	},
	/**
	 * Xuat bien ban thu hoi
	 * 
	 * @author update nhutnn
	 * @since 10/07/2015
	 */
	exportEvictionForm: function(id) {
		$('.ErrorMsgStyle').html("").hide();
		var rows = $('#gridSuggestEviction').datagrid('getRows');
		if (rows.length == 0) {
			$('#errMsgSuggestEviction').html('Không có dữ liệu để xuất').show();
			return false;
		}

		if(id == null) {
			if (EquipmentSuggestEviction._mapRecored.keyArray.length == 0) {
				$('#errMsgSuggestEviction').html('Bạn chưa chọn biên bản. Vui lòng chọn biên bản!').show();
				return false;
			}
		}	

		var lstIdRecord = new Array();
		if(id == null) {
			for (var i = 0; i < EquipmentSuggestEviction._mapRecored.keyArray.length; i++) {
				lstIdRecord.push(EquipmentSuggestEviction._mapRecored.keyArray[i]);
			}
		}else{
			lstIdRecord.push(id);
		}

		var params = new Object();
		params.lstIdRecord = lstIdRecord;
		Utils.addOrSaveData(params, '/equip-suggest-eviction/export-eviction-record', null, 'errMsgSuggestEviction', function(data) {
			if (data.error) {
				$('#errMsgSuggestEviction').html(data.errMsg).show();
				return;
			}
			if (data.lstRecordError != null && data.lstRecordError.length > 0) {
				VCommonJS.showDialogList({
					dialogInfo: {title: 'Thông tin lỗi'},
					data: data.lstRecordError,
					columns: [[
						{field: 'formCode', title: 'Mã biên bản', align: 'left', width: 120, sortable: false, resizable: false}, 
						{field: 'formStatusErr', title: 'Biên bản không ở trạng thái duyệt', align: 'center', width: 60, sortable: false, resizable: false, formatter: function(value, row, index) {
							var html = "";
							if (row.formStatusErr == 1) {
								html = '<img src="/resources/images/icon_delete.png" height="20" width="20" style="margin-right:5px;" title="Error">';
							}
							return html;
						}},
						{field: 'deliveryStatusErr', title: 'Biên bản đã xuất biên bản thu hồi', align: 'center', width: 60, sortable: false, resizable: false, formatter: function(value, row, index) {
							var html = "";
							if (row.deliveryStatusErr == 1) {
								html = '<img src="/resources/images/icon_delete.png" height="20" width="20" style="margin-right:5px;" title="Error">';
							}
							return html;
						}}
					]]
				});
			} else {
				$('#successMsgSuggestEviction').html('Xuất biên bản thu hồi thành công!').show();
				setTimeout(function() {
					$('#successMsgSuggestEviction').html('').hide();
				}, 3000);
			}
			EquipmentSuggestEviction.searchSuggestEviction();
		}, null, null, null, "Bạn có muốn xuất biên bản thu hồi ?", null);
	},	
	/**
	 * Lay danh sach bien ban hop dong giao bi huy
	 *
	 * @author nhutnn
	 * @since 10/07/2015
	 */
	getEvictionFormAgain: function(id) {
		$('.ErrorMsgStyle').html("").hide();
		if(id == null) {
			$('#errMsgSuggestEviction').html('Bạn chưa chọn biên bản. Vui lòng chọn biên bản!').show();
		}	

		var params = new Object();
		params.id = id;
		Utils.getJSONDataByAjax(params, '/equip-suggest-eviction/get-list-eviction-export-again', function(data) {
			if (data.error) {
				$('#errMsgSuggestEviction').html(data.errMsg).show();
				return;
			}else {
				if (data.total > 1) {
					EquipmentSuggestEviction._mapEviction = new Map();  
					$("#idRecordPopup").val(id); 
					$('#easyuiPopupLstEviction').show();
					$('#easyuiPopupLstEviction').dialog({  
				        closed: false,  
				        cache: false,  
				        modal: true,
				        width : 430,	        
				        height : 'auto',
				        onOpen: function(){	      
							$('#gridDelivery').datagrid({
								data: data.rows,
								autoRowHeight : true,
								fitColumns:true,
								width: 390,
								height: "auto",
								scrollbarSize: 0,
								rownumbers:true,
								columns:[[
								 	{field: 'maNPP', title: 'Mã NPP', width: 80, sortable:false, resizable: false, align: 'left'},					
								    {field: 'maKH', title: 'Mã khách hàng', width: 80, sortable:false, resizable: false, align: 'left'},					
									{field: 'maPhieu', title: 'Mã biên bản thu hồi', width: 120, sortable: false, resizable: false,align: 'left'},		    
									{field: 'check',checkbox:true, width: 50, sortable:false,resizable:false, align: 'center'}
								]],
								onBeforeLoad: function(param){											 
								},
								onLoadSuccess: function(data){   			 
							   		$('.easyui-dialog .datagrid-header-rownumber').html('STT');						   		
								},onCheck:function(index,row){
									EquipmentSuggestEviction._mapEviction.put(row.id,row);   
							    },
							    onUncheck:function(index,row){
							    	EquipmentSuggestEviction._mapEviction.remove(row.id);
							    },
							    onCheckAll:function(rows){
							    	for(var i = 0;i<rows.length;i++){
							    		var row = rows[i];
							    		EquipmentSuggestEviction._mapEviction.put(row.id,row);
							    	}
							    },
							    onUncheckAll:function(rows){
							    	for(var i = 0;i<rows.length;i++){
							    		var row = rows[i];
							    		EquipmentSuggestEviction._mapEviction.remove(row.id);	
							    	}
							    }
							});
				        }, 
				        onClose: function(){
				        	EquipmentSuggestEviction._mapEviction = null;
				        }
					});
				} else {
					EquipmentSuggestEviction.exportEvictionFormAgain(id, data.rows[0].customerId);
				}
			}
		}, null, null);
	},
	/**
	 * Xuat lai bien ban hop dong giao nhan lan nua
	 *
	 * @author nhutnn
	 * @since 10/07/2015
	 */
	exportEvictionFormAgain: function(id, customerId){
		$('.ErrorMsgStyle').html("").hide();
		if(customerId == null && id == null) {
			if (EquipmentSuggestEviction._mapEviction.keyArray.length == 0) {
				$('#errMsgPopupLstEviction').html('Bạn chưa chọn biên bản. Vui lòng chọn biên bản!').show();
				return false;
			}
		}	

		var lstCustomerId = new Array();
		if(customerId == null && id == null) {
			for (var i = 0; i < EquipmentSuggestEviction._mapEviction.keyArray.length; i++) {
				var idEviction = EquipmentSuggestEviction._mapEviction.keyArray[i];
				lstCustomerId.push(EquipmentSuggestEviction._mapEviction.get(idEviction).customerId);
			}
		}else{
			lstCustomerId.push(customerId);
		}
		var errId = customerId == null?"errMsgPopupLstEviction":"errMsgSuggestEviction";
		var successId = customerId == null?"successMsgPopupLstEviction":"successMsgSuggestEviction";
		var params = new Object();
		if(id != null){ 
			params.id = id;
		} else {
			params.id = $("#idRecordPopup").val();
		}	
		params.lstCustomerId = lstCustomerId;
		Utils.addOrSaveData(params, '/equip-suggest-eviction/export-eviction-record-again', null, errId, function(data) {
			if (data.error) {
				$('#'+errId).html(data.errMsg).show();
				return;
			}else {
				$('#' + successId).html('Xuất lại biên bản thu hồi thành công!').show();
				$("#idRecordPopup").val("");
				setTimeout(function() {
					$('#' + successId).html('').hide();					
				}, 3000);
				$('#easyuiPopupLstEviction').window('close');
			}
			EquipmentSuggestEviction.searchSuggestEviction();
		}, null, null, null, "Bạn có muốn xuất lại biên bản thu hồi ?", null);
	},
	/**
	 * Quay về trang tìm kiếm
	 *
	 * @author nhutnn
	 * @since 27/07/2015
	 */
	returnSearchPage: function(){
		$.messager.confirm('Xác nhận', "Bạn có muốn quay lại trang tìm kiếm và không lưu dữ liệu thay đổi?", function(r){
			if(r){
				if(window.location.pathname == "/equip-suggest-eviction/edit"){
					window.location.href = '/equip-suggest-eviction/info';	
				} else if(window.location.pathname == "/equip-suggest-eviction/change"){
					window.location.href = '/equip-suggest-eviction/approve';	
				}					
			}
		});
	},
};
/*
 * END OF FILE - /web/web/resources/scripts/business/equipment/vnm.equipment-suggest-eviction.js
 */

/*
 * JavaScript file created by Rockstarapps Concatenation
*/

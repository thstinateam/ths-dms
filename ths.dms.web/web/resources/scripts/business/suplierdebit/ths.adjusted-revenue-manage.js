var AdjustedRevenueManage = {
	_mapSaleOrder: null,
	_mapSaleOrderDetail: null,
	_mapSODDialogSearch: null, 
	_flagG: null,
	_lockDate:null,
	_productCodeFirt:null,
	_indexTmp: null,
	_lstSelectedId: null,
	_arrDelete:null,
	_arrInsert:null,
	_txtIsJoinOderNumberC: null,
	approvedReturnText : function (value){
		if(value == undefined || value == null){
			return '';
		}else if(value == 1){
			return 'Đã duyệt';
		}else if(value == 0){
			return 'Chưa duyệt';
		}else{
			return '';
		}
	},
	searchSaleOrder : function (){
		$('#adjustedRevenueDetailDiv').html("").change();
		//Cap nhat lai du lieu voi tham so tim kiem
		$('#adjustedRevenueDg').datagrid("load",{
			shopCode: $('#ddlShopCode').combobox('getValue'),
			oderNumber: $('#txtOderNumber').val(),
			orderType: $('#dllOrderType').val().trim(),
			isJoinOderNumber: $('#txtIsJoinOderNumber').val(),
			fromDateStr: $('#txtFromDate').val(),
			toDateStr: $('#txtToDate').val(),
			//approved: $('#dllApproved').val()
		});
	},
	
	viewDetailSaleOrderAIAD : function (saleOrderId, orderNumber){
		var html = '<h2 class="Title2Style">Chi tiết phiếu điều chỉnh <span id="adjustedRevenueDetailSpan" style="color:#199700">' + Utils.XSSEncode(orderNumber) +'</span></h2>';
		html += ' <div class="GridSection" id="adjRevenueContDetailGrid"><table id="adjustedRevenueDetailDg"></table></div>';
		$('#adjustedRevenueDetailDiv').html(html).change();
		$('#adjustedRevenueDetailSpan').focus();
		//Map grid form
		$('#adjustedRevenueDetailDg').datagrid({
			url: '/adjusted-revenua/search-saleorder-detail-ai-ad',
			width : $('#adjRevenueContDetailGrid').width() - 5,
			queryParams: {saleOrderId: saleOrderId},
			fitColumns : true,
		    rownumbers : true,
		    height: 275,
			columns:[[
				{field:'productCode',title:'Mã sản phẩm',width:180,align:'left',formatter: function(value, row, index){
					return Utils.XSSEncode(value);
				}},  
			    {field:'productName',title:'Tên sản phẩm',width:300,align:'left',formatter: function(value, row, index){
			    	return Utils.XSSEncode(value);
			    }},
			    {field:'price',title:'Đơn giá',width:130,align:'right',formatter: function(value, row, index){
			    	return VTUtilJS.formatCurrency(Utils.formatDoubleValue(value));
			    }},
			    {field:'quantity',title:'Số lượng',width:130,align:'right',formatter: function(value, row, index){
			    	return VTUtilJS.formatCurrency(Utils.formatDoubleValue(value));
			    }},
			    {field:'amount',title:'Thành tiền',width:130,align:'right',formatter: function(value, row, index){
			    	return VTUtilJS.formatCurrency(Utils.formatDoubleValue(value));
			    }}
			]],
	        onLoadSuccess :function(data){
		    	 $('#adjustedRevenueDetailDg .datagrid-header-rownumber').html('STT');
		    	 Utils.updateRownumWidthAndHeightForDataGrid('adjustedRevenueDetailDg');
	    	}
		});
	},
	
	insertRowAdjustedRevenueDetailDg: function(){
		var indexMax = $('#adjustedRevenueDetailDg').datagrid('getRows').length;
		$('#adjustedRevenueDetailDg').datagrid('insertRow',{
			index: indexMax,
			row: {
				id: 0,
				productCode: '',
				productName: '',
				price: 0,
				quantity: 0,
				amount: 0,
				isAttr:0
			}
		});
//		for(var i=0; i<indexMax; i++){
//			$('#adjustedRevenueDetailDg').datagrid('unselectRow').datagrid('endEdit', i);
//		}
		$('#adjustedRevenueDetailDg').datagrid('selectRow', indexMax).datagrid('beginEdit', indexMax);
		$('.datagrid-editable-input').bind('keyup', function(event) {
			if (event.keyCode == keyCode_F9) {
				AdjustedRevenueManage.openDialogSearchProductInChange();
			}
		});
		$('.datagrid-editable-input').focus();
		$('.datagrid-editable-input').bind('keyup',function(event){
 			if(event.keyCode == keyCodes.ENTER){
 				$('#btnChangeSaleOderAIAD').click();
 			}
 		});
		$('#dgPrice0').bind('keyup',function(event){
 			if(event.keyCode == keyCodes.ENTER){
 				$('#btnChangeSaleOderAIAD').click();
 			}
 		});
	},
	
	redirectInsertOrUpdate: function(flag, id){
		if(flag==1){
			window.location.assign('/adjusted-revenua/viewSaleOrderAIADInsertOrUpdate');
		}else if(flag = 2){
			window.location.assign('/adjusted-revenua/viewSaleOrderAIADInsertOrUpdate?id=' +id+'');
		}
	},
	
	///@author hunglm16; @since: September 10,2014; @description Mo pupup sao Tim kiem don hang
	openDialogSearchSaleOrder: function(){
		$(".ErrorMsgStyle").html('').hide();
		$('#gridSearchBillContGrid').html('<table id="gridSearchBill"></table>').change();
		$('#dialogSearchSaleOrder').dialog({
			title: "Tìm kiếm đơn hàng",
			closed: false,
			cache: false,
			modal: true,
	        width: 980,
	        height: 'auto',
	        onOpen: function(){
	        	$('.InputTextStyle').val('');
	    		$('.easyui-dialog #errorSearchMsg').hide();
	    		if(AdjustedRevenueManage._lockDate==null || AdjustedRevenueManage._lockDate ===""){
	    			var fullDate = new Date();
	    			AdjustedRevenueManage._lockDate = fullDate.getDate() + "/" + twoDigitMonth + "/" + fullDate.getFullYear();
	    		}
	    		$('#txtFromDateDl').val(AdjustedRevenueManage._lockDate);
	    		$('#txtToDateDl').val(AdjustedRevenueManage._lockDate);
	    		$('#txtOrderNumberDl').focus();
	    		$('#gridSearchBillContGrid').html('<table id="gridSearchBill"></table>').change();
	    		var params = new Object();
	    		params = AdjustedRevenueManage.getParamDialogSearchSaleOrder();
	    		params.flag = false;
	    		$('#gridSearchBill').datagrid({
		    		width: $('#gridSearchBillContGrid').width()- 15,
		  			url: '/adjusted-revenua/searchDialogSaleOrder',
		  			queryParams: params,
		  			autoRowHeight : true,
					rownumbers : true,
					checkOnSelect :true,
					pagination:true,
					rowNum : 10,
					pageSize:10,
					scrollbarSize : 0,
					singleSelect:true,
					pageNumber:1,
			        fitColumns : true,
			        columns:[[
						{field: 'orderNumber', title: 'Số ĐH', width: 110, sortable:false,resizable:false, align: 'left',formatter: function(value, row, index){
						  	return Utils.XSSEncode(value);
						  }},
						{field: 'orderDate', title: 'Ngày', width: 80, sortable:false,resizable:false, align: 'center',formatter:function(value,row,index){
							if(row.orderDate==null) return '';
							return $.datepicker.formatDate('dd/mm/yy', new Date(row.orderDate));
						}},
						{field: 'customer.shortCode', title: 'Khách hàng', width: 130, sortable:false,resizable:false, align: 'left',formatter: function(value,row, index){
							return (row.customer == null)? "" : Utils.XSSEncode(row.customer.shortCode) +" - " + Utils.XSSEncode(row.customer.customerName); 
						}},             
						{field: 'customer.address', title: 'Địa chỉ', width: 180, sortable:false,resizable:false, align: 'left',formatter: function(value,row, index){
							return (row.customer == null)? "" :row.customer.address; 
						}},	  			    
						{field: 'staff', title: 'NVBH', width: 100, sortable:false,resizable:false, align: 'left',formatter: function(value,row, index){
						  	return (row.staff == null)? "" :row.staff.staffCode; 
						}},
						{field: 'delivery', title: 'NVGH', width: 100, sortable:false,resizable:false, align: 'left',formatter: function(value,row, index){
						  	return (row.delivery == null)? "" :row.delivery.staffCode; 
						}},
						{field: 'amount', title: 'Thành tiền', width: 100, sortable:false,resizable:false, align: 'left',formatter: function(value,row, index){
						  	return (row.amount == null)? "" :formatCurrencyInterger(row.amount); 
						}},
						{field:'chose', title:'', width:60, align:'center',sortable : false,resizable : false,formatter : function(value, row, index) {
								return '<a href="javascript:void(0)" onclick="return AdjustedRevenueManage.choseIsJoinOderInDiglogSearch( \'' + Utils.XSSEncode(row.orderNumber) +'\');">Chọn lựa</a>';
						}}
		  			]],
		  			onLoadSuccess :function(data){
		  				$('#gridSearchBill .datagrid-header-rownumber').html('STT');
						Utils.updateRownumWidthAndHeightForDataGrid('gridSearchBill');
					 	$(window).resize();
			        }
	    		});
	        },
	        onClose:function() {
	        	$("input[id$='Dl']").val("");
	        	var txtIsJoin = $('#txtIsJoinOderNumber').val().trim();
	        	if(txtIsJoin.length==0){
	        		$('#txtIsJoinOderNumber').val(AdjustedRevenueManage._txtIsJoinOderNumberC);	        		
	        	}
	        }
		});
	},
	
	choseIsJoinOderInDiglogSearch : function(ordernumber){
		var isJoinOderNunber = '';
		if(ordernumber!=undefined && ordernumber!=null && ordernumber.trim().length > 0){
			isJoinOderNunber = ordernumber;
		}
		var msg = 'Bạn có muốn chọn Số Đơn Gốc ' + Utils.XSSEncode(isJoinOderNunber);
		$.messager.confirm('Xác nhận', msg, function(r){
			if (r){
				$('#txtIsJoinOderNumber').val(isJoinOderNunber);
				$('#dialogSearchSaleOrder').dialog("close");
				$('#adjustedRevenueDetailDg').datagrid("load", {orderNumber: $('#txtOderNumber').val().trim()});
				$('#txtIsJoinOderNumber').focus();
			}
		});
		
		
	},
	
	///@author hunglm16; @since: September 10,2014; @description Tim kiem don hang
	searchSaleOrderDialog: function(){
		var params = AdjustedRevenueManage.getParamDialogSearchSaleOrder();
		$('#gridSearchBill').datagrid("load", params);
	},
	///@author hunglm16; @since: September 10,2014; @description Lay tham so tu ca Field tren Dialog tim kiem don hang
	getParamDialogSearchSaleOrder : function() {
		var orderNumber = $('#txtOrderNumberDl').val().trim();
		var shortCode = $('#txtShortCodeDl').val().trim();
		var customerName = $('#txtCustomerNameDl').val().trim();
		var fDate = $('#txtFromDateDl').val().trim();
		var tDate = $('#txtToDateDl').val().trim();
		var staffCode = $('#txtStaffCodeDl').val().trim();
		var deliveryCode = $('#txtDeliveryCodeDl').val().trim();
		var data = new Object;
		if (orderNumber.length > 0 && orderNumber.length <= 20) {
			data.orderNumber = orderNumber;
		}
		if (shortCode.length > 0 && shortCode.length <= 10) {
			data.shortCode = shortCode;
		}
		if (customerName.length > 0 && customerName.length <= 250) {
			data.customerName = customerName;
		}
		if (staffCode.length > 0 && staffCode.length <= 50) {
			data.staffCode = staffCode;
		}
		if (deliveryCode.length > 0 && deliveryCode.length <= 50) {
			data.deliveryCode = deliveryCode;
		}
		if (fDate.length > 0 && fDate.length <= 50) {
			data.fromDateStr = fDate;
		}
		if (tDate.length > 0 && tDate.length <= 50) {
			data.toDateStr = tDate;
		}
		data.orderType = $('#dllOrderTypeDl').val().trim();
		data.approved = 1; // APPROVED
		data.type = 1; // TYPE
		data.shopId = $('#ddlShopId').combobox('getValue');
		return data;
	},
	
	viewDetailSaleOrderAIADChange : function (txt){
		var txtId = $('#txtOderNumber');
		if(txt!=undefined && txt != null){
			txtId = txt;
		}
		var isJoinOderNumber = $(txtId).val().trim();
		var rows = [];
		if(isJoinOderNumber==undefined || isJoinOderNumber==null || isJoinOderNumber===""){
			isJoinOderNumber = "";
			$('#adjustedRevenueDetailSpan').html(isJoinOderNumber);
			$('#adjustedRevenueDetailDg').datagrid("loadData", rows);
		}else{
			$('#adjustedRevenueDetailSpan').html(isJoinOderNumber);
			Utils.getJSONDataByAjax ({
				saleOrderNumber: isJoinOderNumber
			},'/adjusted-revenua/search-saleorder-ai-ad',function(data){
				$('#adjustedRevenueDetailDg').datagrid("loadData", data.rows);
			}, null, null);
		}
	},
	
	///@author hunglm16; @since: September 10,2014; @description Xoa Don Hang (AI, AD)
	deleteSaleorderAIAD: function(id, orderNumber){
		$(".ErrorMsgStyle").html('').hide();
		var msg = "";
		var code = "";
		if(orderNumber == undefined || orderNumber == null){
			code = "";
		}else{
			code = orderNumber.trim();
		}
		code = code.toString().trim();
		var params = {
				id: id
		};
		var msg = "Bạn có muốn xóa Phiếu điều chỉnh " + Utils.XSSEncode(code) + "?";
		Utils.addOrSaveData(params, "/adjusted-revenua/deleteSaleOrderAIAD", null, 'errMsg', function(data) {
			$("#successMsg").html("Lưu dữ liệu thành công").show();
			$('#btnSearchSaleOderAIAD').click();
			setTimeout(function(){
				$('.SuccessMsgStyle').html("").hide();
			 }, 1500);
		}, null, null, null, msg);
	},
	
	///@author hunglm16; @since: September 10,2014; @description Xoa Chi tiet Don Hang (AI, AD)
	deleteSaleorderDetailAIAD: function(id, isAttr, rowIndex, productCode){
		$(".ErrorMsgStyle").html('').hide();
		var msg = "";
		var code = "";
		if(productCode == undefined || productCode == null || productCode.trim().length==0){
			return;
		}
		code = productCode.trim();
		code = code.toString().trim();
		$.messager.confirm('Xác nhận', 'Bạn có muốn xóa sản phẩm ' + Utils.XSSEncode(code) +' này?', function(r){
			if (r){
				var index = AdjustedRevenueManage._arrDelete.indexOf(id);
				if(index <0 && isAttr >0){
					AdjustedRevenueManage._arrDelete.push(id);
				}
				$('#adjustedRevenueDetailDg').datagrid('deleteRow', rowIndex);
				$('#adjustedRevenueDetailDg').datagrid("deleteRow", $('#adjustedRevenueDetailDg').datagrid("getRows").length - 1);
				$('#adjustedRevenueDetailDg').datagrid("loadData", $('#adjustedRevenueDetailDg').datagrid("getRows"));
			}
		});
	},
	///@author hunglm16; @since: September 10,2014; @description Cap nhat lai don hang goc moi vao textbox
	fillTxtDatagridEditableInput : function (obj){
		var indexMax = $('#adjustedRevenueDetailDg').length;
		$('#adjustedRevenueDetailDg').datagrid('deleteRow', indexMax - 1);
		$('#adjustedRevenueDetailDg').datagrid('insertRow',{
			index: indexMax - 1,
			row: obj
		});
		AdjustedRevenueManage.insertRowAdjustedRevenueDetailDg();
	},
	
	onchangePriceInRowsDetail: function(txt){
		var value = $(txt);
		if(value == undefined || value == null || value.trim().length ==0){
			$(txt).val(0);
			value = 0;
		}
		var rows = $('#adjustedRevenueDetailDg').datagrid('getRows');
		for(var i=0; i< rows.length; i++){
		 	sumAmountRow += data.rows[i].amount;
    	}
		$('#lblSumAmountDg').html(VTUtilJS.formatCurrency(Utils.formatDoubleValue(sumAmountRow)));
	},
	openDialogSearchProductInChange : function() {
		$('.ErrorMsgStyle').html('').hide();
		$('#common-dialog-grid-container').html('<table id="common-dialog-grid-search"></table>').change();
		$('#common-dialog-search-2-textbox').dialog({
			title: 'Thông tin tìm kiếm',
			width: 600, 
			height: 'auto',
			closed: false,
			cache: false,
			modal: true,
			onOpen: function() {
				AdjustedRevenueManage._lstSelectedId = new Array();
				var arrCodeStr = "IFIRT";
				var rows = $('#adjustedRevenueDetailDg').datagrid('getRows');
				if(rows!=null && rows.length>0){
					for(var i=0; i<rows.length; i++){
						arrCodeStr += "ESC" + Utils.XSSEncode(rows[i].productCode);
					}
				}
				arrCodeStr = arrCodeStr.replace(/IFIRTESC/g, "").replace(/IFIRT/g, "").trim();
				var isJoinSO = $('#txtIsJoinOderNumber').val().trim();
				if(isJoinSO==null || isJoinSO.length==0){
					isJoinSO = "-1UNDEFINED";
				}
				$('#common-dialog-grid-search').datagrid({
				    url: '/adjusted-revenua/search-saleorder-detail-Ijoin',
					autoRowHeight : true,
					rownumbers : true,
					scrollbarSize: 0,
					height: 275,
					//singleSelect:true,
					queryParams:{
						orderNumber: isJoinSO,
						productCode: $('#txtProductCodeDl').val(),
						productName: $('#txtProductNameDl').val(),
						textG: arrCodeStr,
						isAttr: false
					},
					fitColumns:true,
					width: ($('#common-dialog-search-2-textbox').width() - 40),
					columns:[[
					    {field:'productId',checkbox:true},
					    {field:'productCode', title:'Mã Sản Phẩm', align:'left', width: 110, sortable:false, resizable:false},
				        {field:'productName', title:'Tên Sản Phẩm', align:'left', width: 200, sortable:false, resizable:false}
					]],
					onCheck: function(rowIndex, rowData) {
						var index = AdjustedRevenueManage._lstSelectedId.indexOf(rowData.id);
						if(index < 0){
							AdjustedRevenueManage._lstSelectedId.push(rowData.id);
						}
					},
					onUncheck: function(rowIndex, rowData) {
						var index = AdjustedRevenueManage._lstSelectedId.indexOf(rowData.id);
						if(index > -1){
							AdjustedRevenueManage._lstSelectedId.splice(index, true);
						}
					},
					onCheckAll: function(rows) {
						AdjustedRevenueManage._lstSelectedId = new Array();
						var dataGrid = $('#common-dialog-grid-search').datagrid('getRows');
						if($.isArray(dataGrid)) {
							for(var i=0; i<dataGrid.length; i++){
								var index = AdjustedRevenueManage._lstSelectedId.indexOf(dataGrid[i].id);
								if(index < 0){
									AdjustedRevenueManage._lstSelectedId.push(dataGrid[i].id);
								}
							}
						}
					},
					onUncheckAll: function(rows) {
						AdjustedRevenueManage._lstSelectedId = new Array();
					},
					onLoadSuccess :function(data){
						$('#common-dialog-search-2-textbox .datagrid-header-rownumber').html('STT');
						var dHeight = $('#common-dialog-search-2-textbox').height();
						var wHeight = $(window).height();
						var ptop = (wHeight - dHeight) / 2 > 180 ? (wHeight - dHeight) / 2 - 180 : (wHeight - dHeight) / 2;
						$('#common-dialog-search-2-textbox').dialog('move', {top : ptop});
						
						AdjustedRevenueManage._mapSODDialogSearch = new Map();
						for(var i=0; i<data.rows.length; i++){
							AdjustedRevenueManage._mapSODDialogSearch.put(data.rows[i].id, data.rows[i]);
						}
					}
				});
				
				$('#txtProductCodeDl').focus();
			},
			onClose: function() {
				$('#txtProductCodeDl, #txtProductNameDl').val('');
				$('.SuccessMsgStyle').html('').hide();
				$('.ErrorMsgStyle').html('').hide();
			}
		});
	},
	
	commonDialogGridSearch : function (){
		var arrId = [];
		var arrCodeStr = "IFIRT";
		var rows = $('#adjustedRevenueDetailDg').datagrid('getRows');
		if(rows!=null && rows.length>0){
			for(var i=0; i<rows.length; i++){
				arrCodeStr += "ESC" + Utils.XSSEncode(rows[i].productCode);
			}
		}
		arrCodeStr = arrCodeStr.replace(/IFIRTESC/g, "").replace(/IFIRT/g, "").trim();
		var isJoinSO = $('#txtIsJoinOderNumber').val().trim();
		if(isJoinSO==null || isJoinSO.length==0){
			isJoinSO = "-1UNDEFINED";
		}
		
		$('#common-dialog-grid-search').datagrid("load", {
			orderNumber: isJoinSO,
			productCode: $('#txtProductCodeDl').val(),
			productName: $('#txtProductNameDl').val(),
			textG: arrCodeStr,
			isAttr: false
		});
	},
	onchangeCheckProductSearch : function(cbx){
		var ck = $(cbx).prop('checked');
		if(ck){
			var index = AdjustedRevenueManage._lstSelectedId.indexOf($(cbx).prop("value").trim());
			if(index < 0){
				AdjustedRevenueManage._lstSelectedId.push($(cbx).prop("value").trim());
			}
		}else{
			var index = AdjustedRevenueManage._lstSelectedId.indexOf($(cbx).prop("value").trim());
			if(index > -1){
				AdjustedRevenueManage._lstSelectedId.splice(index, true);
			}
		}
	},
	
	choseArrProductInDialogSearch: function(){
		var msg = '';
		if(AdjustedRevenueManage._lstSelectedId == null || AdjustedRevenueManage._lstSelectedId.length==0){
			msg = 'Chưa sản phẩm nào được lựa chọn';
			$('#errMsgDialogChosePrd').html(msg).show();
		}else{
			var indexMax = $('#adjustedRevenueDetailDg').datagrid('getRows').length;
			indexMax = indexMax - 1;
			$('#adjustedRevenueDetailDg').datagrid('deleteRow', indexMax);
			for(var i=0; i< AdjustedRevenueManage._lstSelectedId.length; i++){
				var rowIndex = AdjustedRevenueManage._mapSODDialogSearch.get(AdjustedRevenueManage._lstSelectedId[i]);
				rowIndex.isAttr = 0;
				$('#adjustedRevenueDetailDg').datagrid('insertRow',{
					index: indexMax,
					row: rowIndex
				});
				indexMax++;
			}
			AdjustedRevenueManage.insertRowAdjustedRevenueDetailDg();
			$('#adjustedRevenueDetailDg').datagrid("deleteRow", $('#adjustedRevenueDetailDg').datagrid("getRows").length - 1);
			$('#adjustedRevenueDetailDg').datagrid("loadData", $('#adjustedRevenueDetailDg').datagrid("getRows"));
			$('#successMsgDialogChosePrd').html('Lưu dữ liệu thành công').show();
			var tm = setTimeout(function(){
				$('.SuccessMsgStyle').html('').hide();
				$('#common-dialog-search-2-textbox').dialog("close");
				clearTimeout(tm);
			}, 1000);
		}
	},
	
	onchangeTxtPriceInGridDetail: function (txt){
		var priceTxt = $(txt).val().replace(/,/g,"").trim();
		var price = 0;
		var index = $(txt).attr("indexrow");
		if(priceTxt.length == 0){
			$('#adjustedRevenueDetailDg').datagrid("getRows")[index].amount = 0;
			$('#adjustedRevenueDetailDg').datagrid("getRows")[index].price = 0;
			$(txt).parent().parent().next().next().children().html(0);
			$(txt).val(0);
		}else{
			price = parseFloat(priceTxt);
			if(price.toString()==="NaN"){
				$('#adjustedRevenueDetailDg').datagrid("getRows")[index].amount = 0;
				$('#adjustedRevenueDetailDg').datagrid("getRows")[index].price = 0;
				$(txt).parent().parent().next().next().children().html(0);
			}else{
				var amount = parseFloat(price) * parseFloat($('#adjustedRevenueDetailDg').datagrid("getRows")[index].quantity);
				$('#adjustedRevenueDetailDg').datagrid("getRows")[index].price = price;
				
				$('#adjustedRevenueDetailDg').datagrid("getRows")[index].amount = amount;
				$(txt).parent().parent().next().next().children().html(VTUtilJS.formatCurrency(Utils.formatDoubleValue(amount)));
			}
		}
		$('#adjustedRevenueDetailDg').datagrid("deleteRow", $('#adjustedRevenueDetailDg').datagrid("getRows").length - 1);
		$('#adjustedRevenueDetailDg').datagrid("loadData", $('#adjustedRevenueDetailDg').datagrid("getRows"));
	},
	
	createOrUpdateSaleOrderAIAD: function(){
		/** flag = 0: update; falg = 1: insert @author hunglm16 **/
		$('.SuccessMsgStyle').html('').hide();
		$('.ErrorMsgStyle').html('').hide();
		
		var msg = "";
		
		var shopId = $('#ddlShopId').combobox('getValue');
		if (shopId == undefined || shopId == null || shopId.length == 0) {
			msg = 'Không xác định được Đơn vị';
		}
		if (msg.length == 0) {
			msg = Utils.getMessageOfRequireCheck("txtIsJoinOderNumber", "Số đơn gốc");
		}
		if (msg.length == 0) {
			msg = Utils.getMessageOfSpecialCharactersValidate("txtIsJoinOderNumber", "Số đơn gốc", Utils._NAME);
		}
		if (msg.length == 0) {
			msg = Utils.getMessageOfSpecialCharactersValidate("txtDescription", "Lý do", Utils._NAME);
		}
		if (msg.length > 0) {
			$("#errMsgChange").html(msg).show();
			return false;
		}
		
		msg = '';
		var msgerr = '';
		var isFlag = true;
		
		var arrStrUpdateData = 'PRDCODE_0';
		var arrStrInsertData = 'PRDCODE_0';
		var arrStrDeleteData = 'ID';
		var rows = $('#adjustedRevenueDetailDg').datagrid('getRows');
		var arrayDel = []; 
		if(rows!=null && rows.length>0){
			for(var i=0; i<rows.length - 1; i++){
				if(rows[i].isAttr!=undefined && rows[i].isAttr!=null && rows[i].isAttr>0){
					arrStrUpdateData += 'ESC' + Utils.XSSEncode(rows[i].productCode + '_' + rows[i].price);
				}else{
					arrStrInsertData += 'ESC' + Utils.XSSEncode(rows[i].productCode +'_' + rows[i].price);
				}
			}
		}
		arrStrUpdateData = arrStrUpdateData.trim().replace(/PRDCODE_0ESC/g, "").replace(/PRDCODE_0/g, "").trim();
		arrStrInsertData = arrStrInsertData.trim().replace(/PRDCODE_0ESC/g, "").replace(/PRDCODE_0/g, "").trim();
		if(AdjustedRevenueManage._arrDelete!=null && AdjustedRevenueManage._arrDelete.length>0){
			for(var i=0; i<AdjustedRevenueManage._arrDelete.length; i++){
				if(AdjustedRevenueManage._arrDelete[i] > 0){
					arrStrDeleteData += 'ESC' + AdjustedRevenueManage._arrDelete[i];
				}
			}
		}
		arrStrDeleteData = arrStrDeleteData.trim().replace(/IDESC/g, "").replace(/ID/g, "").trim();
		
		var kt = $('#txtOderNumber').val().trim();
		if (kt.length > 0) {
			isFlag = false;
			msg = "Bạn có muốn cập nhật Phiếu điều chỉnh doanh thu " + Utils.XSSEncode($('#txtOderNumber').val().trim()) + "?";
		} else {
			msg = "Bạn có muốn lập Phiếu điều chỉnh doanh thu?";
		}
		if (msgerr.length > 0) {
			$('#errMsgChange').html(msgerr).show();
			return;
		}
		var params = {
			shopId: $('#ddlShopId').combobox('getValue'),
			saleOrderId: $('#txtOderNumber').attr('saleorderid'),
			isJoinOderNumber: $('#txtIsJoinOderNumber').val().trim(),
			typeStr: $('#dllOrderType').val().trim(),
			description: $('#txtDescription').val().trim(),
			arrStrUpdateData: arrStrUpdateData,
			arrStrInsertData:arrStrInsertData,
			arrStrDeleteData: arrStrDeleteData,
			flag: isFlag
		};
		Utils.addOrSaveData(params, "/adjusted-revenua/createOrUpdateSaleOrderAIAD", null, 'errMsgChange', function(data) {
			$("#successMsgChange").html("Lưu dữ liệu thành công").show();
			if(!isFlag){
				//var approvedDr = $('#dllApproved').val();
//				if (approvedDr != undefined && approvedDr != null && Number(approvedDr) == 1) {
//					window.location.href = '/adjusted-revenua/adjustRevenuaInfo';
//				} else {
//				}
				var urlChange = window.location.href.split("?");
				if(urlChange[1] == undefined || urlChange[1] == null || urlChange[1].trim().length == 0){
					window.location.href = window.location.href + '?id=' +data.saleOrderId;
				} else {
					$('#adjustedRevenueDetailDg').datagrid("load", {orderNumber: $('#txtOderNumber').val().trim()});
				}
				var tm = setTimeout(function(){
					$('.SuccessMsgStyle').html("").hide();
					clearTimeout(tm);
				}, 1500);
			}else{
				if(data!=undefined && data!=null && data.saleOrderId!=undefined && data.saleOrderId!=null && data.saleOrderId>0){
					//var approvedDr = $('#dllApproved').val();
					if (approvedDr != undefined && approvedDr != null && Number(approvedDr) == 1) {
						window.location.href = '/adjusted-revenua/adjustRevenuaInfo';
					} else {
						var urlChange = window.location.href.split("?");
						if(urlChange[1] == undefined || urlChange[1] == null || urlChange[1].trim().length == 0){
							window.location.href = window.location.href + '?id=' +data.saleOrderId;
						} else {
							$('#adjustedRevenueDetailDg').datagrid("load", {orderNumber: $('#txtOderNumber').val().trim()});
						}
					}
				}
			}
		}, null, null, null, msg);
	},
	
	fillTxtShortCodeDlByF9 : function(code, name) {
		$('#txtShortCodeDl').val(code);
		$('#txtCustomerNameDl').val(name);
		$('#common-dialog-search-2-textbox').dialog("close");
		return false;
	}
};
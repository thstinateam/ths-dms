var ChequeManualDebit = {
	poVnmId:null,
	debitDetailId:null,
	_type: 0,
	_indexDetail: null,
	search:function(){
		var msg = '';
		$('#errMsgSearch').html('').hide();
		$('#errMsg').hide();
		var fDate = $('#fromDate').val().trim();
		var tDate =$('#toDate').val().trim();
		if(fDate == '__/__/____'){
			$('#fromDate').val('');
			fDate = "";
		}
		if(tDate == '__/__/____'){
			$('#toDate').val('');
			tDate = "";
		}
		msg = Utils.getMessageOfRequireCheck('type', "Loại", true);
		if(msg.length == 0 && fDate.length > 0 && !Utils.isDate(fDate, '/')) {			
				$('#errMsgSearch').html('Nhập sai định dạng. Vui lòng nhập lại').show();
				$('#fromDate').focus();
				return false;
		}
		if(msg.length == 0 && tDate.length > 0 && !Utils.isDate(tDate, '/')) {
			$('#errMsgSearch').html('Nhập sai định dạng. Vui lòng nhập lại').show();
			$('#toDate').focus();
			return false;
		}
		if(msg.length == 0 && !Utils.compareDate(fDate, tDate)){
			msg = msgErr_fromdate_greater_todate;		
			$('#fDate').focus();
		}
		if(msg.length > 0){
			$('#errMsgSearch').html(msg).show();
			return false;
		}
		var data = new Object();
		data.fromDate = fDate;
		data.toDate = tDate;
		data.poConfirmNumber = $('#poConfirmNumber').val().trim();
		data.type = $('#type').val().trim();
		data.shopCode = $('#shopCode').val().trim();
		$('#dg').datagrid('load',data);
		$('.dgInput').hide();
	},
	payOrder:function(){
		$('.ErrorMsgStyle').hide();
		var msg = '';
		var kq = 0;
		msg = Utils.getMessageOfRequireCheck('soUyNhiem','Số ủy nhiệm');
		if(msg.length == 0){
			msg = Utils.getMessageOfSpecialCharactersValidate('soUyNhiem','Số ủy nhiệm',Utils._CODE);
		}			
		if(msg.length==0){
			msg = Utils.getMessageOfRequireCheck('totalMoney','Số tiền');
			kq = 1;
		}		
		if(msg.length==0){
			var mes = Utils.getMessageOfInvaildNumber('totalMoney','Số tiền');
			if(mes.length>0){
				msg = "Số tiền nhập vào phải là số nguyên dương > 0";
				kq = 1;			
			}
		}
		var totalMoney = Utils.returnMoneyValue($('#totalMoney').val().trim());
		var remainHidden = Utils.returnMoneyValue($('#remainHidden').val().trim());
		var value = Number(remainHidden) - Number(totalMoney);
		$('#remainAfter').val(CommonFormatter.numberFormatter(value));

		var remainAfter = value;
		if((msg.length==0 && Number(remainHidden)<Number(totalMoney))|| remainAfter < 0){
			msg = 'Số tiền nhập vào phải <= số tiền chưa thanh toán của đơn hàng';			
			kq = 1;
		}
		if(msg.length>0){
			$('#errMsg').html(msg).show();
			if(kq == 1){
				$('#totalMoney').focus();
			}
			return false;
		}
		if(totalMoney <= 0) {
			$('#errMsg').html('Số tiền nhập vào phải là số nguyên dương > 0').show();
			$('#totalMoney').focus();
			return false;
		}
		if(!/^[0-9a-zA-Z_]+$/.test(Utils.returnMoneyValue($('#totalMoney').val().trim()))) {
			$('#errMsg').html('Số tiền nhập vào phải là số nguyên dương > 0').show();
			$('#totalMoney').focus();
			return false;
		}
		
		var params = new Object();
		params.soUyNhiem = $('#soUyNhiem').val().trim();
		params.totalMoney = Utils.returnMoneyValue($('#totalMoney').val().trim());
		params.poVnmId = ChequeManualDebit.poVnmId;
		params.debitDetailId = ChequeManualDebit.debitDetailId;
		params.type = ChequeManualDebit._type;
		if (Number($('#bankId').val())) {
			params.bankId = $('#bankId').val().trim();
		}
		$.messager.confirm('Xác nhận', 'Bạn có muốn lưu thông tin này?', function(r){
			if (r){
				Utils.saveData(params, '/suplierdebit/manage/sec-save', null, 'errMsg', function(){
					ChequeManualDebit.search();
				});
				if($('#remainAfter').val()== 0){
					$('#remainAfter').val('');
					$('#totalMoney').val('');
					$('#soUyNhiem').val('');
					jQuery('div#showCheque').hide();
//					$('#btnPayOrder').attr('disabled', true);
				}
				else{
					$('#soUyNhiem').val('');
					$('#totalMoney').val('');
//					$('#btnPayOrder').attr('disabled', false);
				}
			}
		});
	},
	showDetail:function(index){
//		$('#btnPayOrder').attr('disabled', false);
		$('#errMsg').hide();
		//var indexEx=index;
		jQuery('div#showCheque').show();
		$('#bankCode').attr('autocomplete','off');
		var row = $('#dg').datagrid('getRows')[index];
		if(row==null){
			return false;
		}
		if (row.poVnmId != undefined && row.poVnmId != null) {
			ChequeManualDebit.poVnmId = row.poVnmId;
		}
		ChequeManualDebit.debitDetailId = row.debitDetailId;
		$('#title').html('Nhập sec sổ tay - ' + Utils.XSSEncode(row.poConfirmNumber));
		$('.dgInput').show();
		$('#soUyNhiem').val('');
		$('#totalMoney').val('');
		$('#remainAfter').val('');
		$('#remainHidden').val(Math.abs(row.remain));
		ChequeManualDebit._indexDetail = index; 
		if (row.remain > 0) {
			ChequeManualDebit._type = 0;
		} else {
			ChequeManualDebit._type = 1;
		}
		$('#soUyNhiem').focus();
		$('html, body').animate({ scrollTop: $(document).height() }, 1000);
	}
};

var SuplierDebit = {
	paidDebit: function() {
		var params = new Object();
		params.invoiceNumber = $('#invoiceNumberHide').val();
		params.invoiceTypeChoice = $('#invoiceTypeHide').val();
		params.money = $('#moneyHide').val();
		params.shopCode = $('#shopCode').val();
		
		Utils.addOrSaveData(params, '/suplierdebit/manage/paid-debit', null, 'errMsg', function(data) {
			window.location.href = '/suplierdebit/manage/info';
		}, 'paidLoading',null, null, null,function(dataErr) {
			if(dataErr.errorReloadPage == true){
				window.location.href = '/suplierdebit/manage/info';
			}
		});
	},	
	watchDistribution: function() {
		$('.ErrorMsgStyle').html('').hide();		
		var invoiceNumber = $('#invoiceNumber').val();
		var invoiceTypeChoice = $('#invoiceType').val();
		if(invoiceTypeChoice != 2 && invoiceTypeChoice != 3) {
			$('#errorDistributeMsg').html('Vui lòng chọn loại chứng từ').show();
			$('#paidDebit').attr('disabled', 'disabled');
			return;
		}
		var money = $('#money').val();
		money = Utils.returnMoneyValue(money);
		if(isNaN(money)) {
			$('#errorDistributeMsg').html('Số tiền nhập không hợp lệ. Vui lòng nhập lại số tiền').show();
			$('#money').focus();
			$('#paidDebit').attr('disabled', 'disabled');
			return;
		}
		var msg = '';
		msg = Utils.getMessageOfSpecialCharactersValidate('invoiceNumber','Số chứng từ', Utils._CODE);
		if(msg.length == 0 && $('#money').val()!= undefined && $('#money').val().length == 0){
			msg = 'Bạn chưa nhập số tiền';
		}
		if(msg.length == 0 && $('#money').val()!= undefined && ($('#money').val() == 0 || $('#money').val() === '0')) {
			msg = 'Số tiền phải lớn hơn 0';
		}
		if(msg.length > 0) {
			$('#errorDistributeMsg').html(msg).show();
			$('#paidDebit').attr('disabled', 'disabled');
			return;
		}
		msg = Utils.getMessageOfRequireCheck('invoiceNumber', 'Số chứng từ');
		if(msg.length == 0) {
			msg = Utils.getMessageOfRequireCheck('money', 'Số tiền');
		} else {
			$('#errorDistributeMsg').html(msg).show();
			$('#paidDebit').attr('disabled', 'disabled');
			return;
		}
		enable('paidDebit');
		money = Utils.returnMoneyValue(money);		
		var shopCode = $('#shopCode').val();
		var params = new Object();
		params.invoiceNumber = invoiceNumber;
		params.invoiceTypeChoice = invoiceTypeChoice;
		params.money = money;
		params.isPageLoad = false;
		params.shopCode = shopCode;
		$('#invoiceNumberHide').val(invoiceNumber);
		$('#invoiceTypeHide').val(invoiceTypeChoice);
		$('#moneyHide').val(money);
		$('#dg').datagrid('load',params);		
	},
	onChangeSelectInvoiceType: function() {
		$('.ErrorMsgStyle').html('').hide();
		var chooseStatus = $('#invoiceType').val();
		if (chooseStatus == 2) {
			var debit = $("#debitNPPToVNM").val();
			$("#labelDebit").html('Nợ NPP với công ty:');
			$("#currentDebit").html(formatCurrency(debit));
		} else {
			var debit1 = $("#debitVNMToNPP").val();
			$("#labelDebit").html('Nợ công ty với NPP:');
			$("#currentDebit").html(formatCurrency(debit1));
			
		}
		var shopCode = $('#shopCode').val();
		var params = new Object();
		params.invoiceTypeChoice = chooseStatus;
		params.shopCode = shopCode;
		$('#dg').datagrid('load',params);
	},
	exportExcelDebit: function(){
		var rows = $('#dg').datagrid('getRows');
		if (rows != null && rows.length > 0) {
			var dataModel = new Object();
			dataModel.listDebitId = new Array();
			for (var i = 0; i < rows.length; i++) {
				dataModel.listDebitId[i] = rows[i].debitId;
			}
			dataModel.shopCode = $('#shopCode').val();
			ReportUtils.exportReport('/suplierdebit/manage/export-excel-debit',dataModel);
		}
	}
};
var OrganizationSystem = {
	_xhrSave : null,
	_xhrDel: null,
	_idSearch:null,
	_node_tree:null,
	_nodeType:0,
	_mapStaffCbx: new Map(),
	_shop: 'SHOP',
	_staff: 'STAFF',
	_lstOrgIdSort: null,
	_lstOrgSortNodeOrdinal: null,
	_organizationId: 0,
	uploadFilePlugin: null,
	_Id : 0,
	MANAGE : {
		IS_MANAGE : 1,
		IS_NOT_MANAGE : 0
	},
	/** BEGIN VUONGMQ*/
	resetMsgErrorAndSuccess: function(){
		$('.ErrorMsgStyle').html('').hide();
		$('.SuccessMsgStyle').html('').hide();
	},
	/**vuongmq; 15/02/2015; check cay to chuc, ton tai hay chua*/
	checkOrganization:function(idCheck){
		if (idCheck != 0) {
			$('#viewNote').remove();
			OrganizationSystem.loadUnitTree();
		} else {
			$('#viewNote').css('visibility','visible');
		}
	},
	/**vuongmq; 15/02/2015; load cay to chuc*/
	loadUnitTree:function(idSearch,idSearchStaff, orgId){
		if(idSearch!=undefined && idSearch!=null && idSearch!=0){//search đến shop cần tìm
			OrganizationSystem._idSearch=idSearch;
		}
		$('#tree').tree({  
		    url:'/catalog/organization/get-list-organization-system' ,
		    animate:true,
		    dnd:true,
		    lines:true,
		    onBeforeLoad: function(n,p){
		    	if(OrganizationSystem._idSearch!=null && OrganizationSystem._idSearch!=0){
		    		p.idSearch=OrganizationSystem._idSearch;
		    		$('#divOverlay').show(); 
		    		$('#imgOverlay').show(); /** 31/03/2015*/
		    	}
		    },
			formatter: function(node){
			    	var tmpText = Utils.XSSEncode(node.text);
			    	if (node.attributes.shop != null && node.attributes.shop.status == 'RUNNING'){
			    		if(node.iconCls != null ){
			    			if (node.attributes.shop.isManage != null && node.attributes.shop.isManage == OrganizationSystem.MANAGE.IS_MANAGE) {
			    				return ('<img src="' +node.iconCls+'" width="15px" height="15px"> ' +tmpText + '<span style="color: red;">&nbsp;(M)</span>');
			    			} else {
			    				return ('<img src="' +node.iconCls+'" width="15px" height="15px"> ' +tmpText );
			    			}
			    		} else {
			    			if (node.attributes.shop.isManage != null && node.attributes.shop.isManage == OrganizationSystem.MANAGE.IS_MANAGE) {
			    				return (tmpText + '<span style="color: red;">&nbsp;(M)</span>');
			    			} else {
			    				return (tmpText);
			    			}
			    		}
			    	}
			    	if (node.attributes.shop != null && node.attributes.shop.status == 'STOPPED'){
			    		if(node.iconCls != null){
			    			return ('<img src="' +node.iconCls+'" width="15px" height="15px"> ' +tmpText);
			    		} else {
			    			return (tmpText);
			    		}
			    	}
			 },
		    onLoadSuccess:function(parent,data){
		    	if(OrganizationSystem._idSearch!=null){
		    		try{
		    			var id=OrganizationSystem._idSearch;
		    			var node = $('#tree').tree('find', id);
			    		if(node!=null){
			    			$('#tree').tree('select', node.target);
			    		}
		    		}catch(e){}
		    		OrganizationSystem._idSearch=null;
		    		$('#imgOverlay').hide(); /** 31/03/2015*/
		    		$('#divOverlay').hide();
		    	} else if (orgId != undefined && orgId > 0) {
									var node = $('#tree').tree('find', orgId);
									if (node != null) {
										$('#tree').tree('select', node.target);
									}
									var parentNode = $('#tree').tree(
											'getParent', node.target);
									if (parentNode != null) {
										$('#tree').tree('expand',
												parentNode.target);
									}
								} 
		    	OrganizationSystem.resize();
		    },
		    /**vuongmq; 15/02/2015; click right menu cay to chuc*/
		    onContextMenu:function(e, node){
				e.preventDefault();
				$('#tree').tree('select', node.target);
				if(OrganizationSystem.filterContextMenu(node)){
					$('#contextMenu').menu('show', {left: e.pageX,top: e.pageY});
				}
			},
			onCollapse:function(node){
				OrganizationSystem.resetMsgErrorAndSuccess();
				OrganizationSystem.resize();
			},
			onExpand :function(node) {
				OrganizationSystem.resetMsgErrorAndSuccess();
				OrganizationSystem.resize();
			},
			onClick:function(node){
				if($('#searchStaffUnitTreeGrid').length==1 && node!=null && node.attributes!=null && node.attributes.shop!=null){
					OrganizationSystem.search(0);
				}
	        },
	        onDblClick:function(node){
	        	
	        },
			onBeforeDrag:function(node){
				if(node.attributes==null || node.attributes.shop==null){
					return false;
				}
				return true;
			},
			onBeforeDrop:function(target,source,point){
				/*if(point!=undefined && point != "append") {
					return false;
				}*/
				if(point!=undefined && (point != "top" && point != "bottom")) {
					return false;
				}
				var nodeTarget=$('#tree').tree('getNode', target);
				if(nodeTarget==null) {
					return false;
				}
				return OrganizationSystem.doDrop(nodeTarget, source);
			},
			onDrop:function(target,source,point){
				/**vuongmq; 24/02/2015; lay danh sach id node va danh sach nodeOrdinal de sort( bat buoc nodeOrdinal phai khac null) */
				OrganizationSystem.saveDropNodeOrdinalOrganization(source);
			}
		});
	},
	doDrop: function(nodeTarget, source) {
		if(!OrganizationSystem.checkDropEnter(source,nodeTarget)) {
			return false;
		}
		var node = source.attributes.shop;
		if(node != null){
			OrganizationSystem._callBack = function() {
				OrganizationSystem._callBack = null;
				OrganizationSystem.loadUnitTree();
				//OrganizationSystem.loadUnitTree(node.id);
			};
		}
		return true;
	},
	/**vuongmq; 24/02/2015; kiem tra có cho thả xuống dung vi tri cho hay ko */
	checkDropEnter:function(node,target){
		if(node.attributes==undefined || node.attributes==null || target.attributes==undefined || target.attributes==null){
			return false;
		}
		// khong co parent shop Id thi khong cho tha
		if(node.attributes.shop.parentShop==undefined || node.attributes.shop.parentShop==null || target.attributes.shop.parentShop==undefined || target.attributes.shop.parentShop==null){
			return false;
		}
		// chi cho tha vao cung shop parent; //nếu parent shopid khác với parent shopId muốn thả thì ko cho
		if(node.attributes.shop.parentShop.id!=target.attributes.shop.parentShop.id) {
			return false;
		}
		// chi cho tha cung nodeType
		if(node.attributes.shop.nodeType!=target.attributes.shop.nodeType) {
			return false;
		}
		return true;
	},
	/**vuongmq; 24/02/2015; lay danh sach id node va danh sach nodeOrdinal de sort( bat buoc nodeOrdinal phai khac null) */
	saveDropNodeOrdinalOrganization: function(source){
		OrganizationSystem._lstOrgIdSort = new Array();
		OrganizationSystem._lstOrgSortNodeOrdinal = new Array();
		var node = source.attributes.shop;
		var nodeParent = node.parentShop;
		if(nodeParent != null){
			$($('[node-id=' +nodeParent.id+']').next()[0]).find('>li>[node-id]').each(function() {
			    OrganizationSystem._lstOrgIdSort.push($(this).attr('node-id'));
			    var node = $('#tree').tree('find', $(this).attr('node-id'));
			    var org = node.attributes.shop;
			    if(org != null){
					OrganizationSystem._lstOrgSortNodeOrdinal.push(org.nodeOrdinal);
			    }
			});
			OrganizationSystem.saveTree(); // luu truc tiep nodeOrdinal sau khi keo tha
		}
	},
	/**vuongmq; 15/02/2015; resize lai cay to chuc*/
	resize: function() {
		var hSidebar3=$('#tree').height();
		var hSidebar1=$('.Content3Section').height();
		
		if(hSidebar3> 650) {
			$('.ReportTree2Section').css('height', 'auto');	
		} else {
			if(hSidebar3 <  hSidebar1) {
				if(hSidebar1 < 700) {
					$('.ReportTree2Section').height(hSidebar1-60);
				} else {
					$('.ReportTree2Section').height(650);
				}
			} else {
				if(hSidebar3 < 300) {
					$('.ReportTree2Section').height(300);
				} else {
					$('.ReportTree2Section').css('height', 'auto');	
				}
			}
		}
		var hSidebar2=$('.Sidebar1Section').height();
		if( hSidebar1 > hSidebar2 ) {
			$('.Content1Section').height(hSidebar1);
		}
		else $('.Content1Section').height(hSidebar2);
		return ;
	},
	hideAllContextMenu:function(){
		$('#cmTaoLoaiDonViCon').hide();
		$('#cmTaoChucVu').hide();
		$('#cmTaoLoaiDonViCungCap').hide();
		$('#cmXoaLoaiDonVi').hide();
		$('#cmXoaChucVu').hide();
		$('#cmThemLaQuanLy').hide();
		$('#cmXoaLaQuanLy').hide();
	},
	showContextMenu:function(nodeId,type,nodeAttributer,status){//Typetype---1:shop, 2:staff
		OrganizationSystem.resetMsgErrorAndSuccess();
		var node = $('#tree').tree('getSelected');
		if(type==nodeType.SHOP){//shop
			//node=UnitTreeCatalog.getNodeFromListNode(node);
			if(nodeAttributer.parentShop != null){
				$('#cmTaoLoaiDonViCon').show();
				$('#cmTaoChucVu').show();
				$('#cmTaoLoaiDonViCungCap').show();
				$('#cmXoaLoaiDonVi').show();
			} else {
				$('#cmTaoLoaiDonViCon').show();
				$('#cmTaoChucVu').show();
				$('#cmXoaLoaiDonVi').show();
			}
		}else if(type==nodeType.STAFF){//staff
			$('#cmXoaChucVu').show();
			if (node.attributes.shop.isManage != null && node.attributes.shop.isManage == OrganizationSystem.MANAGE.IS_MANAGE) { // is Manage = True
				$('#cmXoaLaQuanLy').show();
			} else {
				$('#cmThemLaQuanLy').show();
			}
			
		} else {
			return false;
		}
		return true;
	},
	filterContextMenu:function(node){ // show context menu tuong ung voi tung loai node
		if(node.attributes==null){
			return false;
		}
		OrganizationSystem.hideAllContextMenu();
		if(node.attributes.shop!=null){ // all dung: node.attributes.shop
			if (node.attributes.shop.nodeType == null) {
				return OrganizationSystem.showContextMenu(node.id, -1, node.attributes.shop, node.attributes.shop.status);
			} else {
				if (nodeType.parseValue(node.attributes.shop.nodeType) == nodeType.SHOP) { // node shop
					return OrganizationSystem.showContextMenu(node.id, nodeType.SHOP, node.attributes.shop, node.attributes.shop.status);
				} else if(nodeType.parseValue(node.attributes.shop.nodeType) == nodeType.STAFF){ // node staff
					return OrganizationSystem.showContextMenu(node.id, nodeType.STAFF, node.attributes.shop, node.attributes.shop.status);
				} else {
					return false;		
				}
			} 
		}else {
			return false;
		}
	},
	reloadUnitTree:function(){
		var node = $('#tree').tree('getSelected');
		if(node!=null){
			OrganizationSystem.loadUnitTree(node.id);
		}else{
			OrganizationSystem.loadUnitTree();
		}
	},
	saveTree:function(confirm){
		var param = new Object();
		if (OrganizationSystem._lstOrgIdSort != null && OrganizationSystem._lstOrgSortNodeOrdinal != null) {
			param.lstOrgIdSort = OrganizationSystem._lstOrgIdSort;
			var a = OrganizationSystem._lstOrgSortNodeOrdinal;
			a.sort(function(first, second) {
				  return first - second;
				});
			param.lstOrgSortNodeOrdinal = a;
		}
		if(confirm!=undefined && confirm!=null && confirm==1){
			Utils.addOrSaveData(param, '/catalog/organization/save-organization', null, 'errMsg', function(result){
				OrganizationSystem.saveTreeSuccess(result);
			});
		}else{
			Utils.saveData(param, '/catalog/organization/save-organization', null, 'errMsg', function(result){
				OrganizationSystem.saveTreeSuccess(result);
			});
		}
	},
	saveTreeSuccess:function(result){
		if(!result.error){
			$('#successMsg').html('Lưu dữ liệu thành công').show();
			setTimeout(function(){$('#successMsg').html('').hide();},4000);
			OrganizationSystem._lstOrgIdSort = new Array();
			OrganizationSystem._lstOrgSortNodeOrdinal = new Array();
		}
	},
	/** vuongmq; 14/02/2015; dung cho: xoa loai don vi va xoa chuc vu*/
	deleteShopAndStaffTree: function(type){
		var id = '';
		var titleDelete ='';
		var node=$('#tree').tree('getSelected');
		if(node !=undefined && node!=null){
			id = node.id;
		}
		var dataModel = new Object();
		dataModel.nodeTypeId = id;
		if(type != null && type == OrganizationSystem._shop){	
			dataModel.nodeType = nodeType.SHOP; // 1: loai don vi
			titleDelete = organization_ban_xoa_loai_don_vi;
		} else {
			dataModel.nodeType = nodeType.STAFF; // 2: loai chuc vu
			titleDelete = organization_ban_xoa_chuc_vu;
		}
		Utils.addOrSaveData(dataModel, "/catalog/organization/delete-shop-and-staff-tree", OrganizationSystem._xhrDel, null, function(data){
			if (!data.error){
				var tm = setTimeout(function(){
					$('#successMsg').html('').hide();
					OrganizationSystem.loadUnitTree();
				}, 2000);
			}else{
				$('#errMsg').val(data.errMsg);
			}
		},null,null, null, titleDelete);
		return false;	
	},
	/** phuocdh2; 09/03/2015; dung cho: Danh dau la quan ly*/
	checkIsManageStaffTree: function(type,obj){
		var id = '';
		var titleDelete ='';
		var node=$('#tree').tree('getSelected');
		if(node !=undefined && node!=null){
			id = node.id;
		}
		if(node.attributes.shop.isManage != undefined && node.attributes.shop.isManage != null){
			isManage = node.attributes.shop.isManage;
			if (obj != null && obj =='isManage') {
				isManage = OrganizationSystem.MANAGE.IS_MANAGE;
				titleDelete = organization_ban_co_muon_danh_dau_quan_ly_don_vi;
			} else {
				isManage = OrganizationSystem.MANAGE.IS_NOT_MANAGE;
				titleDelete = organization_ban_co_muon_bo_danh_dau_quan_ly_don_vi;
			}
		}
		var dataModel = new Object();
		dataModel.nodeTypeId = id;
		dataModel.isManage = isManage;
		if(type != null && type == OrganizationSystem._shop){	
			//dataModel.nodeType = nodeType.SHOP; // 1: loai don vi
			// titleDelete = organization_ban_xoa_loai_don_vi;
		} else {
			dataModel.nodeType = nodeType.STAFF; // 2: loai chuc vu
			//titleDelete = organization_ban_co_muon_danh_dau_quan_ly_don_vi;
		}
		Utils.addOrSaveData(dataModel, "/catalog/organization/check-is-manage-staff-tree", OrganizationSystem._xhrDel, null, function(data){
			if (!data.error){
				OrganizationSystem.loadUnitTree();
				var tm = setTimeout(function(){
					$('#successMsg').html('').hide();
				}, 2000);
			}else{
				var tm = setTimeout(function(){
					$('#errMsg').val(data.errMsg);
				}, 2000);
				
			}
		},null,null, null, titleDelete);
		return false;	
	},
	/** phuocdh2; 09/03/2015; openDialogIsManage : popup Ban co muon danh dau quan ly don vi nay */
	openDialogIsManage : function(index, nodeType,index2,organizationId,nodeType2) {
		$('.ErrorMsgStyle').html('').hide();
		$('.SuccessMsgStyle').html('').hide();
		var titleView = '';
		if(nodeType != undefined && nodeType != null && nodeType == 2){
			titleView = organization_title_popup_is_Manage;
		} else {
			titleView = organization_title_popup_is_Manage;
		}
		$('#popupIsManage').dialog({
			title: titleView,
			closed : false,
			cache : false,
			modal : true,
			width : 380,
			height : 200,//400,
			onOpen: function(){
				$('input:radio[name=radioIsManage]').attr('checked',false);
				$('#popupIsManage #btnUpload').unbind('click');
				$('#popupIsManage #btnUpload').bind('click', function() {
	 					OrganizationSystem.chooseOrganizationType(index2,organizationId,nodeType2); 
				});
				
				$('#popupIsManage btnUpload').html(jsp_common_dongy);
				$('#popupIsManage btnCancelUpload').html(jsp_common_bo_qua);
				$('#popupIsManage btnUpload').show();
				
			},
		 	onBeforeClose: function() {

		 	},
	        onClose : function(){
	        	$('#popupIsManage #errMsg').hide();
	        	//OrganizationSystem.uploadFilePlugin.getFilesWithStatus(Dropzone.SUCCESS);
	        	//OrganizationSystem.uploadFilePlugin.removeAllFiles();
	        }
		});
	},
	
	/** vuongmq; 14/02/2015; dung cho: them loai don vi con, cung cap*/
	addNewShop:function(typeShop){
		var node=$('#tree').tree('getSelected');
		if(node != null && node.attributes!=null && node.attributes.shop!=null){
			//xử lý thêm loại đơn vị
//			if(node.attributes.shop.nodeType != null && node.attributes.shop.nodeType == 'SHOP'){
				OrganizationSystem._nodeType = nodeType.SHOP;
//			}
			// Neu la Them loai don vi con typeShop: 1
			if(typeShop == 1){
				OrganizationSystem._organizationId = node.attributes.shop.id;
			}else if(typeShop == 2){ // Them loai don vi cung cap typeShop: 2
				if(node.attributes.shop.parentShop!=null){
					OrganizationSystem._organizationId = node.attributes.shop.parentShop.id;
				}
			}
			OrganizationSystem.openDialogOrganization();
		} else {
			/**vuongmq; 25/02/2015; xu ly lan dau tien chua co Organization, them nut don vi dau tien la nut goc, parentShop = null*/
			/** luc nay la dang dung typeShop = 2; // Them loai don vi cung cap */
			OrganizationSystem._nodeType = nodeType.SHOP;
			OrganizationSystem._organizationId = 0;
			OrganizationSystem.openDialogOrganization();
		}
	},
	/** vuongmq; 14/02/2015; dung cho: them chuc vu*/
	addNewStaff:function(){
		var node=$('#tree').tree('getSelected');
		if(node.attributes!=null && node.attributes.shop!=null){
			//xử lý thêm loại đơn vị
//			if(node.attributes.shop.nodeType != null && node.attributes.shop.nodeType == 'STAFF'){
				OrganizationSystem._nodeType = nodeType.STAFF;
//			}
			// Neu la Them loai don vi con
			OrganizationSystem._organizationId = node.attributes.shop.id;
			OrganizationSystem.openDialogOrganization();
		}
	},
	/**END VUONGMQ*/
	
	/**
	 * load grid organization unit type
	 * @author liemtpt
	 * @description: load danh sach them loai thuoc tinh
	 * @createDate 22/01/2015
	 */
	loadGridOrganizationUnitType : function() {
		var organizationUnitType = $('#txtOrganizationUnitType').val().trim();
		var params = new Object();
		params.codeOrName = organizationUnitType;
		var titleName= '';
		if(OrganizationSystem._nodeType == nodeType.SHOP){
			params.nodeType = OrganizationSystem._nodeType;
			titleName = organization_title_popup_loai_don_vi;
		}else{
			params.nodeType = OrganizationSystem._nodeType;
			titleName = organization_title_popup_loai_chuc_vu;
		}
		var url = '/catalog/organization/search-organization'; 
		$("#popupOrganizationUnitType #idOrganizationGrid").datagrid({
		  url:url,
		  pageList  : [10,20,30,50],
		  height:'auto',
		  scrollbarSize : 0,
		  pagination:true,
		  fitColumns:true,
		  singleSelect  :true,
		  method : 'GET',
		  rownumbers: true,	  
		  width:  $('#popupOrganizationUnitType').width() - 50,
		  queryParams:params,
		  pageNumber: 1,
		  pageSize: 10,
		  columns:[[	
			{field:'name',title:titleName, width: 150, sortable: true,resizable:true , align: 'left',
				formatter: function(val, row) {
					if (val != undefined && val != null) {
						return Utils.XSSEncode(val);
					}
					return '';
				}
			},
			{field:'prefix', title:organization_prefix, width: 80, sortable:true,resizable:true , align: 'left',
				formatter: function(val, row) {
					if (val != undefined && val != null) {
						return Utils.XSSEncode(val);
					}
					return '';
				}
			},
			{field:'iconUrl', title:'icon', width: 50, sortable:true,resizable:true , align: 'center',
				formatter: function(val, row) {
					if(val!=null && val!=undefined){
						return '<a><img src="' +imgServerPath + val+'" style = "width:20px;heigh:20px"/></a>';
					}
					return '';
				}
			},
			{field:'choose', title:work_date_chon, width: 60, sortable:false,resizable:true , align: 'center',
				formatter: function(val, row , index) {
					if ( OrganizationSystem._nodeType == nodeType.SHOP ){
						return '<a href="javascript:void(0)" onclick="return OrganizationSystem.chooseOrganizationType(' +index+',' +OrganizationSystem._organizationId+',' +OrganizationSystem._nodeType+');">' +work_date_chon+'</a>';
					} else {
						return '<a href="javascript:void(0)" onclick="return OrganizationSystem.openDialogIsManage(' +null+',' +nodeType.STAFF+',' +index+',' +OrganizationSystem._organizationId+',' +OrganizationSystem._nodeType+');">' +work_date_chon+'</a>';
					}
				}
			},
			
			{field:'edit', title: '<a id="btnAddAttribute" title="' +msgText2+'"  href="javascript:void(0)" onclick= "return OrganizationSystem.openDialogOrganizationDetail(null,' +OrganizationSystem._nodeType+');"><img src="/resources/images/icon_add.png"/></a>',width: 70, align: 'center',sortable:false,resizable:true,
				formatter: function(val, row, index) {
					var value= '';
					value += '<a href="javascript:void(0);" title="' +msgText3+'" onclick="OrganizationSystem.openDialogOrganizationDetail(' +index+',' +OrganizationSystem._nodeType+');"><img src="/resources/images/icon-edit.png"/></a>    ';
					value += '   <a href="javascript:void(0)" title="' +msgText4+'" class="btnEditAttribute" onclick="OrganizationSystem.deleteOrganizationType(' +row.nodeTypeId+')"><img src="/resources/images/icon_delete.png"/></a>';
					return value;
				}
			},       
		    {field:'description', hidden : true},
		    {field:'P', hidden : true}
		  ]],	  
		  onLoadSuccess:function(){
			  $('.datagrid-header-rownumber').html(jsp_common_numerical_order);
			  var lstHeaderStyle = ["name","prefix","iconUrl"];
			  Utils.updateCellHeaderStyleSort('idDialogOrganizationContainer',lstHeaderStyle);
	      }
		});
		$('#txtOrganizationUnitType').focus(); 
	},
	/**
	 * load grid organization unit type
	 * @author liemtpt
	 * @description: load danh sach them loai thuoc tinh
	 * @createDate 22/01/2015
	 */
	searchOrganizationUnitType : function() {
		$('#popupOrganizationUnitType #errMsg').hide();
		var organizationUnitType = $('#txtOrganizationUnitType').val().trim();
		var params = new Object();
		params.codeOrName = organizationUnitType;
		var titleName= '';
		if(OrganizationSystem._nodeType == 1){
			params.nodeType = OrganizationSystem._nodeType;
			titleName = organization_title_popup_loai_don_vi;
		}else{
			params.nodeType = OrganizationSystem._nodeType;
			titleName = organization_title_popup_loai_chuc_vu;
		}
		$("#popupOrganizationUnitType #idOrganizationGrid").datagrid('options').sortName = null; // search thi load lai sort binh thuong
		$("#popupOrganizationUnitType #idOrganizationGrid").datagrid('load',params);
	},
	
	
	/**
	 * open dialog shop organization
	 * @author liemtpt
	 * @description: Mo popup them loai don vi
	 * @createDate 22/01/2015
	 */
	openDialogOrganization : function() {
		$('#popupOrganizationUnitType #successMsg').html('').hide();
		$('#popupOrganizationUnitType').dialog({
			closed : false,
			cache : false,
			modal : true,
			width : 620,
			title : OrganizationSystem._nodeType == 1? organization_title_popup_them_don_vi:organization_title_popup_loai_chuc_vu,
			height : 550,
			onOpen: function(){	
				$('#txtOrganizationUnitType').val("");
				$('#txtOrganizationUnitType').change();
				$('#txtOrganizationUnitType').focus(); 
				Utils.bindAutoSearch();
//				$("#popupOrganizationUnitType #idOrganizationGrid").datagrid('options').sortName = null; // search thi load lai sort binh thuong
				OrganizationSystem.loadGridOrganizationUnitType();
			},
		 	onBeforeClose: function() {

		 	},
	        onClose : function(){
	        	$('#popupOrganizationUnitType #errMsg').hide();
	        }
		});
	},
	/**
	 * open dialog shop organization
	 * @author liemtpt
	 * @description: Mo popup them loai don vi
	 * @createDate 22/01/2015
	 */
	openDialogOrganizationDetail : function(index, nodeType) {
		//$('#popupOrganizationDetail #successMsg').html('').hide();
		$('.ErrorMsgStyle').html('').hide();
		$('.SuccessMsgStyle').html('').hide();
		var titleView = '';
		if(nodeType != undefined && nodeType != null && nodeType == 2){
			titleView = organization_title_popup_information_position;
		} else {
			titleView = organization_title_popup_information_organization;
		}
		$('#popupOrganizationDetail').dialog({
			title: titleView,
			closed : false,
			cache : false,
			modal : true,
			width : 680,
			height : 260,//400,
			onOpen: function(){
				if(nodeType != undefined && nodeType != null && nodeType == 2){
					$('#popupOrganizationDetail #lblName').html(organization_title_popup_loai_chuc_vu + '<span class="ReqiureStyle"> * </span>');
				} else {
					$('#popupOrganizationDetail #lblName').html(organization_title_popup_loai_don_vi + '<span class="ReqiureStyle"> * </span>');
				}
				$('#popupOrganizationDetail #lblkyHieu').html(organization_prefix);
				$('#popupOrganizationDetail #lblDescription').html(organization_title_popup_description);
				$('#btnUpload').html(jsp_common_capnhat);
				$('#btnCancelUpload').html(jsp_common_bo_qua);
				$('#btnUpload').show();
				OrganizationSystem.uploadFilePlugin.enable();
				
				if (index!=undefined && index!=null && index >= 0) {
					row =$('#idOrganizationGrid').datagrid('getRows')[index];
					if (row != null) {
						if (row.name != null) {
							$('#OrganizationDetailCodePop').val(row.name);
						}else {
							$('#OrganizationDetailCodePop').val("");
						}

						$('#OrganizationDetailCodePop').change();
						if ( row.prefix != null) {
							$('#OrganizationDetailNamePop').val(row.prefix);
						}else {
							$('#OrganizationDetailNamePop').val("");
						}
						$('#OrganizationDetailNamePop').change();
						if ( row.description != null){
							$('#descriptionPop').val(row.description);
						}else {
							$('#descriptionPop').val("");
						}
						$('#descriptionPop').change();
						// hien thi hinh anh background
						$('div#abc').html('');
						if(row.iconUrl){
							var backgroundIconImageUrl = imgServerPath + row.iconUrl;
							$('div#abc').css('background-image', 'url(' + backgroundIconImageUrl + ')').css('background-size', '75px 75px');
						} else {
							$('div#abc').css('background-image', '').change();
							$('div#abc').html('<span style=" position: absolute; font-weight: bold; padding: 30px 0px; margin-left: 11px"><s:text name="organization_title_popup_choose_image"/></span>');
						}
						$('#popupOrganizationDetail #btnUpload').unbind('click');
						$('#popupOrganizationDetail #btnUpload').bind('click', function() {
       	 					OrganizationSystem.saveOrganizationUnitType(row.nodeTypeId);   
    					});
						//OrganizationSystem._Id = row.nodeTypeId;
						//$('#popupOrganizationDetail #btnUpload').click(OrganizationSystem.saveOrganizationUnitType(row.nodeTypeId));
					}else{/**row bang null them moi chuc vu**/
						//$('#popupOrganizationDetail #btnUpload').click(OrganizationSystem.saveOrganizationUnitType());
						
						if(OrganizationSystem._nodeType == 1){
							OrganizationSystem.saveOrganizationUnitType(1);
						}else{
							OrganizationSystem.saveOrganizationUnitType(2);
						}
					}
				}else{
					$('#OrganizationDetailCodePop').val("");
					$('#OrganizationDetailCodePop').change();
					$('#OrganizationDetailNamePop').val("");
					$('#OrganizationDetailNamePop').change();
					$('#descriptionPop').val("");
					$('#descriptionPop').change();
					$('div#abc').css('background-image', '').change();
					$('div#abc').html('<span style=" position: absolute; font-weight: bold; padding: 30px 0px; margin-left: 11px"><s:text name="organization_title_popup_choose_image"/></span>');
					$('#popupOrganizationDetail #btnUpload').unbind('click');
					$('#popupOrganizationDetail #btnUpload').bind('click', function() {
   	 					OrganizationSystem.saveOrganizationUnitType();   
					});
				}
			},
		 	onBeforeClose: function() {

		 	},
	        onClose : function(){
	        	$('#popupOrganizationDetail #errMsg').hide();
	        	//OrganizationSystem.uploadFilePlugin.getFilesWithStatus(Dropzone.SUCCESS);
	        	OrganizationSystem.uploadFilePlugin.removeAllFiles();
	        }
		});
	},
	deleteOrganizationType : function(id) {
		$('#popupOrganizationUnitType #successMsg').html('').hide();
		$('#popupOrganizationUnitType #errMsg').html('').hide();
		var dataModel = new Object();
		dataModel.nodeTypeId = id;
		dataModel.nodeType = OrganizationSystem._nodeType;
		var titleDelete = '';
		if(OrganizationSystem._nodeType == 1){
			titleDelete = organization_title_delete_loai_don_vi;
		}else{
			titleDelete = organization_title_delete_loai_chuc_vu;
		}
		Utils.deleteSelectRowOnGrid(dataModel, titleDelete, "/catalog/organization/delete-organization-type", OrganizationSystem._xhrDel, null, null,function(data){
			if (!data.error){
				var tm = setTimeout(function(){
					$('#popupOrganizationUnitType #successMsg').html('').hide();
				}, 2000);
				OrganizationSystem.searchOrganizationUnitType();	
			}else{
				$('#popupOrganizationUnitType #errMsg').val(data.errMsg);
			}
		},null,'#popupOrganizationUnitType');
		return false;		
	},
	/**
	 * choose organization type 
	 * @author liemtpt
	 * @description: chon loai (don vi hay chuc vu) luu vao organization
	 * @createDate 22/01/2015
	 */
	chooseOrganizationType : function(index,organizationId,nodeType) {
		var msg = '';
		$('#popupOrganizationUnitType #errMsg').html('').hide();
		var row = null;
		var params = new Object();
		if(index!=undefined && index!=null && index>=0){
			row = $('#popupOrganizationUnitType #idOrganizationGrid').datagrid('getRows')[index];
			params.organizationId = organizationId;
			params.nodeType = nodeType;
			params.nodeTypeId = row.nodeTypeId;
			// xac nhan chuc vu co phai la quan ly
			if ($('input[name=radioIsManage]')[0].checked) {
				params.isManage = 1; // La quan ly
			} else {
				params.isManage = 0; // ko la quan ly 
			}
			//Gui yeu cau len server de luu du lieu
			var url = "/catalog/organization/choose-organization-type";
			params[Utils.VAR_NAME] = 'organizationVO';
			var dataModel = getSimpleObject(params);
			Utils.addOrSaveData(dataModel,url,null, 'errMsg', function(data){
				$('#popupIsManage').dialog('close');
				var tm = setTimeout(function(){
					$('.easyui-dialog').dialog('close'); 
					//load lai cay
					clearTimeout(tm);
				}, 2000);
				 OrganizationSystem.loadUnitTree(data.organizationId);
				/** OrganizationSystem.loadUnitTree(null, null ,data.organizationId); */
				/** vuongmq; lan dau them moi organization; them xong remove buton them */
				if(organizationId == 0){
					$('#viewNote').remove();
				}
				$('#errMsg').html('').hide();
			},null,'#popupOrganizationUnitType',null,null,function(data){
				if(data.error != undefined  && data.error && data.errMsg != undefined){
					$('#popupIsManage').dialog('close');
					$('#popupOrganizationUnitType #errMsg').html(data.errMsg).show();
				}
				$('#errMsg').html('').hide();
	//			var errorMsg = data.errMsg;
	//			setTimeout(function(){$('#errMsg').html('').hide();}, 3000);
			});
			return false;
		}
	},
	/**
	 * init control upload file 
	 * @author phuocdh2
	 * @description: khoi tao control upload file icon
	 * @createDate 22/01/2015
	 */
	_fileAddedIcon: null,
	initializeUploadIconElement: function() {
		var divId = 'div#abc';
		var url = '/catalog/organization/save-shopType';
		
		OrganizationSystem.uploadFilePlugin = new Dropzone(divId, {
		    url: url,
		    uploadMultiple: false,
		    paramName: function(fileOrderNumber) {
		    	return 'icon';
		    },
		    autoQueue: false,
		    clickable: divId,
		    thumbnailWidth: 75,
		    thumbnailHeight: 75,
		    previewTemplate: '<div class="dz-preview dz-file-preview"><img data-dz-thumbnail /></div>',
		    init: function() {
		        this.on("addedfile", function(file) {
		            while (OrganizationSystem.uploadFilePlugin.getFilesWithStatus(Dropzone.ADDED).length != 1) {
		                OrganizationSystem.uploadFilePlugin.removeFile(OrganizationSystem.uploadFilePlugin.getFilesWithStatus(Dropzone.ADDED)[0]);
		            }
		            $('div#abc span').html(''); // su dung  $('div#abc').html(''); thi se mat hinh anh vua uplen 
		            $('div#abc').css('background-image', ''); // khong change
		            OrganizationSystem._fileAddedIcon = OrganizationSystem.uploadFilePlugin.getFilesWithStatus(Dropzone.ADDED);
		        });
		        this.on('success', function(files, responseData) {
		        	if (responseData != undefined && responseData.hasOwnProperty('error') && !responseData.error) {
		        		OrganizationSystem.uploadFilePlugin.disable();
		        		/*$('#sucMsgUpload').html('Thanh Cong').show();
		        		$('#btnUpload').hide();
		        		$('#idOrganizationGrid').datagrid('reload');*/
//		        		$('#btnUpload').hide();
						$('#popupOrganizationDetail #sucMsgUpload').html(msgCommon1).show();
						var tm = setTimeout(function(){
							$('#popupOrganizationDetail').dialog('close'); 
							clearTimeout(tm);
						}, 2000);
		        		$('#idOrganizationGrid').datagrid('reload');
		        	} else {
		        		if(responseData.errMsg != undefined && responseData.errMsg != null){
		        			$('#errMsgUpload').html(responseData.errMsg).show();
		        		}
		        		var files = OrganizationSystem._fileAddedIcon;
		        		for (var i = 0; i < files.length; i++) {
		        			files[i].status = Dropzone.ADDED;
		        		}
		        		//$('#errMsgUpload').html(jsp_common_err_data_update).show();
		        		/*if(data.errMsg != undefined && data.errMsg != null){

						//$('#errMsgUpload').html('Lưu dữ liệu thất bại').show();
		        		if(data.errMsg != undefined && data.errMsg != null){
						$('#popupOrganizationDetail #errMsgUpload').html(data.errMsg).show();
						setTimeout(function(){$('#popupOrganizationDetail #errMsgUpload').html('').hide();}, 3000);
						}
						*/
		        		/*var files = OrganizationSystem.uploadFilePlugin.getFilesWithStatus(Dropzone.ERROR);
		        		for (var i = 0; i < files.length; i++) {
		        			files[i].status = Dropzone.ADDED;
		        		}
		        		var files = OrganizationSystem.uploadFilePlugin.getFilesWithStatus(Dropzone.ADDED);
		        		for (var i = 0; i < files.length; i++) {
		        			files[i].status = Dropzone.ADDED;
		        		}*/
		        	}
		        });
		    }
		});
	},
	/**
	 * bat su kien dialog chon file  hinh icon cho chuc vu, don vi
	 * @author phuocdh2
	 * @description: trigger su kien bat dialog chon file
	 * @createDate 13/02/2015
	 */
	changeImage: function() {
		document.querySelector('div#abc').click();
	},
	/**
	 * them moi, cap nhat  chuc vu, don vi
	 * @author phuocdh2
	 * @description: them moi, cap nhat  chuc vu, don vi
	 * @createDate 12/02/2015
	 */
	saveOrganizationUnitType: function(id) {
		var msg = '';
		var descriptionPop = '';
		var prefix ='';
		var name='';
		$('.ErrorMsgStyle').html('').hide();
		$('.SuccessMsgStyle').html('').hide();
		// xu ly du lieu
		descriptionPop = $('#descriptionPop').val().trim();
		name = $('#OrganizationDetailCodePop').val().trim();
		prefix = $('#OrganizationDetailNamePop').val().trim();
		/*var msg = '';
		if(msg.length ==0){
			msg = Utils.getMessageOfSpecialCharactersValidate('OrganizationDetailNamePop',organization_prefix,Utils._CODE);		}
		if (msg.length > 0) {
			$('#popupOrganizationDetail #errMsgUpload').html(organization_err_prefix_not_unicode).show();
			setTimeout(function(){$('#popupOrganizationDetail #errMsgUpload').html('').hide();}, 3000);
			return false;
		}*/
		dataModel = new Object();
		dataModel.description = descriptionPop;
		dataModel.name	      = name;
		dataModel.prefix      = prefix;
		if(id!=null && id!=undefined && id > 0){
			dataModel.nodeTypeId  = id;
		}

		// bo sung tham so upload file
		var data = JSONUtil.getSimpleObject2(dataModel);
		OrganizationSystem.uploadFilePlugin.options.params = data;

		if (OrganizationSystem._nodeType == 2) {
			OrganizationSystem.uploadFilePlugin.options.url = '/catalog/organization/save-staffType';
		} else {
			OrganizationSystem.uploadFilePlugin.options.url = '/catalog/organization/save-shopType';
		}
		if (OrganizationSystem.uploadFilePlugin) {
			
			var files = OrganizationSystem.uploadFilePlugin.getFilesWithStatus(Dropzone.ADDED);
			if (files && files instanceof Array && files.length != 0) {
				$.messager.confirm(jsp_common_xacnhan,jsp_common_ban_luu_thong_tin_nay, function(r){
					if (r){
						OrganizationSystem.uploadFilePlugin.enqueueFiles(files);
						OrganizationSystem.uploadFilePlugin.processQueue();	
					}
				});
							
			} else {
				//Gui yeu cau len server de luu du lieu
				var url ='';
				if(OrganizationSystem._nodeType == 2) {
					url = '/catalog/organization/save-staffType';
				}else{
					url = '/catalog/organization/save-shopType';
				}
				Utils.addOrSaveData(dataModel,url, null, 'errorMsg', function(result){
					if(!result.error){
						$('#btnUpload').hide();
						$('#popupOrganizationDetail #sucMsgUpload').html(msgCommon1).show();
						var tm = setTimeout(function(){
							$('#popupOrganizationDetail').dialog('close'); 
							clearTimeout(tm);
						}, 2000);
		        		$('#idOrganizationGrid').datagrid('reload');
					}
					
				}, 'loading',null, false, null,function(data){
					if(data.errMsg != undefined && data.errMsg != null){
						$('#popupOrganizationDetail #errMsgUpload').html(data.errMsg).show();
						//setTimeout(function(){$('#popupOrganizationDetail #errMsgUpload').html('').hide();}, 3000);
					}
				});
			}
		}
	},
	cancelSavingManage: function(){
		$('#popupIsManage').dialog('close');
	},
	cancelSavingOrganizationUnitType: function(){
		$('#popupOrganizationDetail').dialog('close'); 
	}
};
/*
 * JavaScript file created by Rockstarapps Concatenation
*/

/*
 * START OF FILE - /web/web/resources/scripts/utils/vnm.utils.js
 */
var Utils = {
	VAR_NAME: '_var_name_',
	_currentSearchCallback: null,	
	_CODE: 111,
	_NAME: 2,
	_ADDRESS: 3,
	_SPECIAL: 99,
	_SERIAL: 21,
	_CTRL_PRESS: false,
	_SHIFT_PRESS: false,
	_TF_A_Z: 1,
	_TF_NUMBER: 2,
	_TF_NUMBER_DOT: 3,
	_TF_NUMBER_COMMA: 4,
	_TF_NUMBER_SIGN: 5,
	_TF_BAN_CHAR: 6,
	_TF_NUMBER_CONVFACT: 7,
	_TF_NUMBER_COMMA_AND_DOT: 8,
	_DATE_DD_MM_YYYY: 9,
	_DATE_MM_YYYY:10,
	_TF_CAR_NUMBER: 11,
	_TF_PHONE_NUMBER: 20,
	_arr_Chk_State: null,
	_NUMBER_TYPE:2,
	_STRING_TYPE:1,
	_DATE_TYPE:3,
	_CHOICE:4,
	_MULTI_CHOICE:5,
	_LOCATION : 6,
	_PAYROLL:12,
	_NAME_CUSTYPE:13,
	_MapControl:new Map(),
	_excelFile: null,
	_errExcelMsg: null,
	_inputTextFileExcel: null,
	_formIdFileExcel: null,
	_xhrReport:null,
	_formIdFileExcel: null,
	_inputText : null,
	_MATH_ROUND: 0,
	_MATH_FLOOR: -1,
	_MATH_CEIL: 1,
	_isShowInputTextByDlSearch2 : {
		id : 1,
		code : 2,
		name : 3,
		text : 4 //Code - Name
	},
	
	/**
	 * Lay ngay he thong
	 * 
	 * @author hunglm16
	 * @since October 7,2014
	 * @description Yeu cau khong coa ham nay vi dung den nhieu chuc nang
	 * */
	currentDate:function() {
		var now = new Date();
		var cYear = now.getFullYear();
		var cMonth = now.getMonth() + 1;
		var cDate = now.getDate();
		var currentDate = cDate+'/'+cMonth+'/'+cYear;
		return currentDate; // b
	},
	
	/**
	 * Xu ly su kien so sanh ngay tra ve cac gia tri so
	 * 
	 * @return 0: Bang nhau
	 * @return 1: d1 > d2
	 * @return -1: d1 < d2
	 * 
	 * @author hunglm16
	 * @since May 22, 2015
	 * */
	compareDateForTowDate: function(d1, d2) {
		if(d1.length == 0 || d2.length == 0){
			return true;			
		}
		var arrD1 = d1.split('/');
		var arrD2 = d2.split('/');
		var d1Obj = dates.convert(arrD1[1] + '/' + arrD1[0] + '/' + arrD1[2]);
		var d2Obj = dates.convert(arrD2[1] + '/' + arrD2[0] + '/' + arrD2[2]);
		
		return dates.compare(d1Obj, d2Obj);
	},
	
	language_vi:function(objectName){//20150602
		if(objectName!=null && objectName.length>0){
			var tmp=objectName.replace(/\./g,'_');
			if(tmp.length>0 && window[tmp])objectName=window[tmp];
		}
		return objectName;
	},
	compareCurrentMonthEx: function(monthTxt) {//neu monthTxt >= thang hien tai => true. ko thi false
		var currentDateTxt = toDateString(new Date());
		var arrCurrentDate = currentDateTxt.split('/');
		var arrMonthTxt = monthTxt.split('/');
		if((Number(arrMonthTxt[1]) > Number(arrCurrentDate[2])) || (Number(arrMonthTxt[1]) == Number(arrCurrentDate[2]) && arrMonthTxt[0]>=arrCurrentDate[1])) {
			return true;
		}
		return false;
	},
	/**
	 *Bat su kien choose khi su dung DialogSearch2 cua viettel-utils-js.js
	 *
	 *@author hunglm16
	 *@since September 28,2014
	 * */
	fillCodeForInputTextByDialogSearch2 : function (txt, id, code, name, isShow) {
		if (isShow == undefined || isShow == null) {
			$('#'+txt.trim()).val(Utils.XSSEncode(code+' - '+name));
			$('#'+txt.trim()).attr(txt + '_' + id, id);
		} else if (Utils._isShowInputTextByDlSearch2.code == isShow) {
			$('#'+txt.trim()).val(Utils.XSSEncode(code));
			$('#'+txt.trim()).attr(txt + '_' + id, id);
		} else if (Utils._isShowInputTextByDlSearch2.name == isShow) {
			$('#'+txt.trim()).val(Utils.XSSEncode(name));
			$('#'+txt.trim()).attr(txt + '_' + id, id);
		} else if (Utils._isShowInputTextByDlSearch2.id == isShow) {
			$('#'+txt.trim()).val(id);
			$('#'+txt.trim()).attr(txt + '_' + id, id);
		} else {
			$('#'+txt.trim()).val("");
			$('#'+txt.trim()).attr(txt + '_0', 0);
		}
		$('#common-dialog-search-2-textbox').dialog("close");
		$('#common-dialog-search-2-textbox').remove();
	},
	/**
	 *Bat su kien choose khi su dung DialogSearch2 cua viettel-utils-js.js
	 *
	 *@author longnh15 - override hunglm16
	 *@since September 28,2014
	 * */
	fillCodeForInputTextByDialogSearchWithUnder : function (txt, txtUnder, id, code, name, isShow) {
		if (isShow == undefined || isShow == null) {
			$('#'+txt.trim()).val(Utils.XSSEncode(code+' - '+name));
			$('#'+txt.trim()).attr(txt + '_' + id, id);
		} else if (Utils._isShowInputTextByDlSearch2.code == isShow) {
			$('#'+txt.trim()).val(Utils.XSSEncode(code));
			$('#'+txt.trim()).attr(txt + '_' + id, id);
		} else if (Utils._isShowInputTextByDlSearch2.name == isShow) {
			$('#'+txt.trim()).val(Utils.XSSEncode(name));
			$('#'+txt.trim()).attr(txt + '_' + id, id);
		} else if (Utils._isShowInputTextByDlSearch2.id == isShow) {
			$('#'+txt.trim()).val(id);
			$('#'+txt.trim()).attr(txt + '_' + id, id);
		} else {
			$('#'+txt.trim()).val("");
			$('#'+txt.trim()).attr(txt + '_0', 0);
		}
		$('#'+txtUnder.trim()).val($('#'+txt.trim()).val());
		$('#common-dialog-search-2-textbox').dialog("close");
		$('#common-dialog-search-2-textbox').remove();
	},
	/**
	 * Phan quyen control tren cac request Ajax
	 * @author HungLm16; 
	 * @param idDiv
	 * @param filterCallback
	 * @since September 14,2014
	 */
	functionAccessFillControl: function (idDiv, filterCallback) {
		var selector = '';
		if (idDiv != undefined && idDiv != null && idDiv.trim().length > 0) {
			idDiv = '#' + idDiv.trim();
			selector = idDiv + ' .cmsiscontrol';
		} else {
			selector = '.cmsiscontrol';
		}
		$(selector).hide();
		if (!_isFullPrivilege) {
			if (_MapControl != undefined && _MapControl != null && _MapControl.size() > 0) {
				var arrayId = _MapControl.keyArray;
				for (var i = 0, size = arrayId.length; i < size; i++) {
					var id = arrayId[i].trim();
					var status = Number(_MapControl.get(id));
					if (status == undefined || status == null || status == 0) {
						$(selector + '[id^="'+id+'"]').remove();
						$(selector + "#" + id).remove();
					} else if (status != 1) {
						$(selector + "#" + id).show();
						$(selector + '[id^="'+id+'"]').show();
						$(selector + '[id^="'+id+'"]').attr("cmsByMapControl", "cmsByMapControl");
						if (status == 2) {
							enable(id);
							$(selector + '[id^="'+id+'"]').each(function() {
								$(this).removeAttr('disabled', 'disabled').removeClass('BtnGeneralDStyle');
							});
							$(selector + '[id^="'+id+'"] input').each(function() {
								$(this).removeAttr('disabled', 'disabled').removeClass('BtnGeneralDStyle');
							});
							$(selector + '[id^="'+id+'"] button').each(function() {
								$(this).removeAttr('disabled', 'disabled').removeClass('BtnGeneralDStyle');
							});
						} else if (status == 3) {
							disabled(id);
							$(selector + '[id^="'+id+'"]').attr('disabled', 'disabled').addClass('BtnGeneralDStyle');
							$(selector + '[id^="'+id+'"]').addClass("isNotPointer");
							$(selector + '[id^="'+id+'"] input').each(function() {
								$(this).attr('disabled', 'disabled').addClass('BtnGeneralDStyle').addClass('isNotPointer');
							});
							$(selector + '[id^="'+id+'"] button').each(function() {
								$(this).attr('disabled', 'disabled').addClass('BtnGeneralDStyle').addClass('isNotPointer');
							});
						}
					} else {
						$(selector + '[id^="'+id+'"]').attr("cmsByMapControl", "cmsByMapControl");
						$(selector + '[id^="'+id+'"]').addClass("isCMSInvisible");
						$(selector + '[id^="'+id+'"]').each(function() {
							$(this).hide();
						});
					}
				}
			}
			var arrCmsHiden = $(selector + ':hidden');
			var size = arrCmsHiden.length;
			for (var i = 0; i < size; i++) {
			  if (!$(arrCmsHiden[i]).attr("cmsByMapControl")) {
				  $(arrCmsHiden[i]).remove();
			  } else {
				  $(arrCmsHiden[i]).removeAttr("cmsByMapControl");
			  }
			}
		} else {
			$(selector + ':hidden').show();
		}
		if (filterCallback != undefined && filterCallback != null) {
			filterCallback.call(this);
		}
	},
	
	///@author Hung Lm16; @since JULY 02,2014; @description Ham Kiem tra file Excel
	previewImportExcelFileUtils: function(fileObj, formId, inputText){
		var inputId = 'fakefilepcDisplay';
		if(inputText != null || inputText != undefined){
			inputId = inputText;
			Utils._inputText= inputText;
		}
		var fileTypes = ["xls", "xlsx", "xlsm"];
		if (fileObj.value != '') {
			if (!/(\.xls|\.xlsx|\.xlsm)$/i.test(fileObj.value)) {
				$('#'+Utils._errExcelMsg.trim()).html(msgErr_excel_file_invalid_type).show();
				fileObj.value = '';
				fileObj.focus();
				document.getElementById(inputId).value ='';			
				return false;
			} else {
				$('.ErrorMsgStyle').html('').hide();
				var src = '';
				if ($.browser.msie){
				    var path = encodeURI(what.value);
				    path = path.replace("C:%5Cfakepath%5C","");
				    var path = decodeURI(path);
				    $('#'+inputId.trim()).val(Utils.XSSEncode(path));
				}else{
					$('#'+inputId.trim()).val(Utils.XSSEncode(fileObj.value));
				}	
			}
		}else{
			$('#'+ Utils._errExcelMsg.trim()).html(common_result_import_excel_not_file_exist).show();		
			return false;
		}
		return true;
	},

	/**
	 * Import excel utils
	 * @author hunglm16
	 * @since 04/09/2015
	 *
	 * @author vuongmq; modify
	 * @params isNotOpenConfirm
	 * @since 22/10/2015 
	 */
	importExcelUtils: function(callBack, objectId, inputByObjectId, errExcelMsg, isNotOpenConfirm) {
 		if (callBack != undefined && callBack != null) {
 			Utils._importCallback = callBack;
 		}
 		if (errExcelMsg == undefined || errExcelMsg == null || errExcelMsg.trim().length == 0) {
 			Utils._errExcelMsg = 'errExcelMsg';
 		} else {
 			Utils._errExcelMsg = errExcelMsg;
 		}
 		var excelURI = $('#'+inputByObjectId).val();
 		if (excelURI == undefined || excelURI == null || excelURI.trim().length == 0) {
 			$('#'+Utils._errExcelMsg.trim()).html('Vui lòng chọn tập tin Excel').change().show();
 			return false;
 		} else {
 			Utils._inputTextFileExcel = inputByObjectId;
 		}
 		
 		if (objectId == undefined || objectId == null || objectId.trim().length == 0) {
 			$('#'+Utils._errExcelMsg.trim()).html('Không tìm thấy form dữ liệu chứa File Excel').change().show();
 			return false;
 		} else {
 			Utils._formIdFileExcel = objectId;
 		}
 		if (isNotOpenConfirm == undefined || isNotOpenConfirm == null || isNotOpenConfirm == false) {
	 		$.messager.confirm('Xác nhận', 'Bạn có muốn nhập file không?', function(r) {  
				if (r) {
					var params = {};
					if ($('#excelType').length > 0) {
						params.excelType = $('#excelType').val();
					}
					if ($('#proType').length > 0) {
						params.proType = $('#proType').val();
					}
					params.token = $("#token").val().trim();
					var options = { 
						beforeSubmit: Utils.beforeImportExcel,   
				 		success:      Utils.afterImportExcel, 
				 		type: "POST",
				 		dataType: 'html',
				 		data:(params)
				 	};
					$('#'+objectId).ajaxForm(options);
			 		$('#'+objectId).submit();
			 		return false;
				} else {
					$('#'+inputByObjectId).val("");
			 		return false;
				}
			});
		} else {
			var params = {};
			if ($('#excelType').length > 0) {
				params.excelType = $('#excelType').val();
			}
			if ($('#proType').length > 0) {
				params.proType = $('#proType').val();
			}
			params.token = $("#token").val().trim();
			var options = { 
				beforeSubmit: Utils.beforeImportExcel,   
		 		success:      Utils.afterImportExcel, 
		 		type: "POST",
		 		dataType: 'html',
		 		data:(params)
		 	};
			$('#'+objectId).ajaxForm(options);
	 		$('#'+objectId).submit();
	 		return false;
		}
 		return false;
 		
 	},
 	///@author HungLm16; @since JULY 02,2014; @description Xu ly truoc khi Import Excel
	beforeImportExcel: function(){
		var idDiv = Utils._inputTextFileExcel;
		if (idDiv==undefined || idDiv==null) {
			idDiv = "excelFile";
		}
		if (!Utils.previewImportExcelFileUtils(document.getElementById(idDiv), Utils._formIdFileExcel,Utils._inputText)) {
			return false;
		}
		$('#errExcelMsg').html('').hide();
		$('#successExcelMsg').html('').hide();
		showLoadingIcon();
	},
	
	/**
	 * Xu ly sau khi Import Excel
	 * 
	 * @author HungLm16
	 * @since JULY 02,2014
	 * 
	 * @author HungLm16
	 * @since May 05,2015
	 * @description Bo sung numFinish
	 * Muc dich bo sung: Import nhieu nhom long nhau, can dem so dong o muc chi tiet 
	 * */
	afterImportExcel: function(responseText, statusText, xhr, $form) {//ProductLevelCatalog
		hideLoadingIcon();
		var data = {};
		var mes = "";
		if (statusText == 'success') {
		    	$("#responseDiv").html(responseText);
		    	
		    	var newToken = $('#responseDiv #newToken').val();
		    	if (newToken != null && newToken != undefined && newToken != '') {
		    		$('#token').val(newToken);
		    		if ($("#tokenImport").length > 0) {
		    			$('#tokenImport').val(newToken);
		    		}
		    		if ($('#importTokenVal').length > 0) {
		    			$('#importTokenVal').val(newToken);
		    		}
		    	}
	    		if ($('#errorExcel').html().trim() == 'true' || $('#errorExcelMsg').html().trim().length > 0) {
	    			data.message = $('#errorExcelMsg').html();
	    			data.flag = false;
		    		$('#fakefilepc').val('');		
					Utils._importCallback.call(this, data);
					return ;
		    	} else {
		    		var totalRow = parseInt($('#totalRow').html().trim());
		    		var numFail = parseInt($('#numFail').html().trim());
		    		var numFinish = parseInt($('#numFinish').html().trim());
		    		var fileNameFail = $('#fileNameFail').html();
		    		
		    		if (!numFail) {
		    			numFail = 0;
		    		}
		    		if (!numFinish) {
		    			numFinish = 0;
		    		}
		    		if (numFinish > 0) {
		    			mes += format(msgErr_result_import_excel, numFinish, numFail);
		    		} else {
		    			mes += format(msgErr_result_import_excel, totalRow - numFail, numFail);
		    		}
		    		if (numFail > 0) {
		    			mes+= ' <a href="'+ fileNameFail +'">Xem chi tiết lỗi</a>';
		    		} else {
		    			mes = '<span style="color:#0587BB;">' + mes + '</span>';
		    		}
		    		if (Utils._importCallback != undefined && Utils._importCallback != null) {
		    			data.message = mes;
		    			data.flag = false;
		    			data.numFail = numFail; 
		    			data.numFinish = numFinish;
		    			Utils._importCallback.call(this, data);
		    		}
		    	}
	    		$('#fakefilepc').val('');
			$('#excelFile').val('');
	    }	
	},
	afterImportExcelNew : function(responseText, statusText, xhr, $form) {
		$('#divOverlay').removeClass('Overlay');
		$('#imgOverlay').hide();
		$('#divOverlay').hide();
		if (statusText == 'success') {
			$("#responseDiv").html(responseText);
			var newToken = $('#responseDiv #newToken').val();
	    	if(newToken != null && newToken != undefined && newToken != '') {
	    		$('#token').val(newToken);
	    		$('#tokenImport').val(newToken);
	    	}
			if ($('#errorExcel').html().trim() == 'true'
					|| $('#errorExcelMsg').html().trim().length > 0) {
				$('#errExcelMsg').html($('#errorExcelMsg').html()).show();
			} else {
				if ($('#typeView').html().trim() == 'false') {
					var totalRow = parseInt($('#totalRow').html().trim());
					var numFail = parseInt($('#numFail').html().trim());
					var fileNameFail = $('#fileNameFail').html();
					var mes = format(msgErr_result_import_excel,
							(totalRow - numFail), numFail);
					if (numFail > 0) {
						mes += ' <a href="' + fileNameFail
								+ '">Xem chi tiết lỗi</a>';
					}
					$('#errExcelMsg').html(mes).show();
				} else {
					$('#excelDialog').html(responseText);
					var html = $('#excelDialog').html();
					$.fancybox(html, {
						modal : true,
						title : catalog_customer_import_cat_info,
						afterShow : function() {
							$('.fancybox-inner #scrollExcelDialog')
									.jScrollPane();
							$('.fancybox-inner .ScrollSection').jScrollPane()
									.data().jsp;
						},
						afterClose : function() {
							$('#excelDialog').html(html);
						}
					});
				}
			}
		}
		$("#file").val("");
		$("#fakefilepc").val("");
		if (Utils._currentSearchCallback != undefined && Utils._currentSearchCallback != null) {
			Utils._currentSearchCallback.call(this);
		}
	},
	///@author hunglm16; @since JULY 28,2014; @description Update auto height For Row Grid
	updateRownumWidthAndHeightForDataGrid:function(parentId){	
		var pId = '';
		if(parentId!= null && parentId!= undefined){
			pId = '#'+parentId + ' ';
		}
		var lastValue=$(pId + '.datagrid-cell-rownumber').last().text().trim().length;
		if(lastValue > 0){
			var extWidth = 25;
			if(lastValue > 2){
				extWidth += (lastValue - 2) * 9;
			}  
			$(pId + '.datagrid-cell-rownumber').css('width',extWidth);
			$(pId + '.datagrid-header-rownumber').css('width',extWidth);
		}
		var length = $(pId).datagrid('getData').rows.length;
		if(length!=undefined && length!=null && length > 0){
			$(pId).datagrid('resize');
			$(pId + '.datagrid-header').css('height','42px');
			$(pId + '.datagrid-header-row').css('height','42px');
			$(pId + '.datagrid-header-row div.datagrid-cell').css({"word-wrap":"break-word", "white-space":"normal", "height":"auto"});
		}
		$(window).resize();
	},
	// Encodes the basic 4 characters used to malform HTML in XSS hacks
	XSSEncode : function(s,en){
		if (!isNullOrEmpty(s)) {
			s = s.toString();
		}
		if(!Utils.isEmpty(s)){
			en = en || true;
			// do we convert to numerical or html entity?
			s = s.replace(/&/g,"&amp;");
			if(en){
				s = s.replace(/'/g,"&#39;"); //no HTML equivalent as &apos is not cross browser supported
				s = s.replace(/"/g,"&quot;");
				s = s.replace(/</g,"&lt;");
				s = s.replace(/>/g,"&gt;");
			}else{
				s = s.replace(/'/g,"&#39;"); //no HTML equivalent as &apos is not cross browser supported
				s = s.replace(/"/g,"&#34;");
				s = s.replace(/</g,"&#60;");
				s = s.replace(/>/g,"&#62;");
			}
			return s;
		}else{
			return "";
		}
	},
	//hunglm16
	isEmpty : function(val){
		if(val){
			return ((val===null) || val.length==0 || /^\s+$/.test(val));
		}else{
			return true;
		}
	},

	previewImportImageFile:function(imgId,fakeId, errMsgId, imgTag){
		if (errMsgId == undefined || errMsgId == null) {
			errMsgId = "errMsg";
		}
		errMsgId = "#" + errMsgId;
		var fileObj = document.getElementById(imgId);
		if (fileObj.value != '') {
			if (!/(\.jpg|\.jpeg|\.png)$/i.test(fileObj.value)) {
				$(errMsgId).html(msgErr_image_file_invalid_type).show();
				fileObj.value = '';
				document.getElementById(fakeId).value ='';
				return false;
			} else {
				$(errMsgId).hide();
				if ($.browser.msie){
				    var path = encodeURI(what.value);
				    path = path.replace("C:%5Cfakepath%5C","");
				    path = decodeURI(path);
				    document.getElementById(fakeId).value = path;
				}else{
					document.getElementById(fakeId).value = fileObj.value;
				}
			}
		}
		if (imgTag != undefined && imgTag != null && imgTag.length > 0) {
			$("#" + imgTag).hide();
		}
		return true;
	},
	deleteFileExcelExport:function(excelFileName){
	},
	exportExcelTemplate:function(url){
 		$('#divOverlay').show();
		if(CommonSearch._xhrReport != null){
			CommonSearch._xhrReport.abort();
			CommonSearch._xhrReport = null;
		}
		CommonSearch._xhrReport = $.ajax({
			type : "GET",
			url : url,
			dataType: "json",
			success : function(data) {
				$('#divOverlay').hide();
				if(data.error && data.errMsg!= undefined){
					$('#'+ errMsg).html(formatString(msgCommon5, escapeSpecialChar(data.errMsg))).show();
				} else{
					if(data.hasData!= undefined && !data.hasData) {
						$('#'+ errMsg).html(msgCommon6).show();
					} else{
						var filePath = ReportUtils.buildReportFilePath(data.view);
						window.location.href = filePath;
						setTimeout(function(){ //Set timeout để đảm bảo file load lên hoàn tất
	                        CommonSearch.deleteFileExcelExport(data.view);
						},30000);
					}
				}
			},
			error:function(XMLHttpRequest, textStatus, errorDivThrown) {				
			}
		});
		return false;
	},
	communicationBetweenTwoTime : function(fD1, tD1, fD2, tD2){
		//Xet fD1 : -oo && tD1
		if(fD1==undefined || fD1==null || fD1.length ==0){
			if(tD1!=undefined && tD1!=null && tD1.trim().length>0){
				if(fD2!=undefined && fD2!=null && fD2.length>0){
					//neu fD2 > tD1 => false
					if(fD2!=undefined && fD2!=null && fD2.length>0 && !Utils.compareDate(fD2, tD1)){
						return false;
					}
				}
			}
		}else{
			//Xet fD1 && tD1 : +oo
			if(tD1==undefined || tD1==null || tD1.length ==0){
				if(tD2!=undefined && tD2!=null && tD2.length>0){
					//neu tD2 < fD1 => fase
					if(fD1!=undefined && fD1!=null && fD1.length>0 && !Utils.compareDate(fD1, tD2)){
						return false;
					}
				}
			}else{
				//fD1, fD2
				//Xet fD2 && tD2: +oo
				if(tD2==undefined || tD2==null || tD2.length ==0){
					if(!Utils.compareDate(fD2, tD1)){
						return false;
					}
				}else{
					//xet fD2:-oo && tD2
					if(fD2==undefined || fD2==null || fD2.length ==0){
						if(!Utils.compareDate(fD1, tD2)){
							return false;
						}
					}else{
						//fD1, tD1, fD2, tD2
						if(!Utils.compareDate(fD2, fD1) && !Utils.compareDate(fD2, tD1)){
							return false;
						}else if(!Utils.compareDate(fD1, fD2) && !Utils.compareDate(fD1,tD2)){
							return false;
						}
					}
				}
			}
		}
		//hai khoang thoi gian giao nhau
		return true;
	},
	
	getMessageOfRequireCheck: function(objectId, objectName,isSelectBox,checkMaxLengh){
		objectName=Utils.language_vi(objectName);
		if($('#' + objectId).val()==null || $('#' + objectId).val().trim() == '__/__/____'){
			$('#' + objectId).val('');
		}
		if(isSelectBox != undefined && isSelectBox && $('#' + objectId).hasClass('easyui-combobox') && $('#' + objectId).combobox('getValue').trim().length == 0) {
			$('#' + objectId).next().attr('id',objectId+'easyuicomboxId');
			$('#'+objectId+'easyuicomboxId input.combo-text').focus();
			return format(msgErr_required_field,objectName);
		
		} else if(!$('#' + objectId).hasClass('easyui-combobox') && 
						((isSelectBox!= undefined && isSelectBox && parseInt($('#' + objectId).val().trim()) < 0) 
					|| ((isSelectBox== undefined || !isSelectBox) && $('#' + objectId).val().trim().length == 0))){
			$('#' + objectId).focus();
			if(!isSelectBox){
				return format(msgErr_required_field,objectName);
			}else{
				return format(msgErr_required_choose_format,objectName);
			}			
		}
		if(checkMaxLengh==undefined || checkMaxLengh==null){
			var maxlength = $('#' + objectId).attr('maxlength');
			if(maxlength!= undefined && maxlength!= null && maxlength.length > 0){
				if($('#' + objectId).val().trim().replace(",","").length > maxlength){
					$('#' + objectId).focus();
					return format(msgErr_invalid_maxlength,objectName,maxlength);
				}
			}
		}		
		return '';
	},
	getMessageOfInvaildInteger:function(objectId, objectName){
		objectName=Utils.language_vi(objectName);
		var strString = $('#' + objectId).val().trim();
		strString = strString.replace(/,/g, '');
		strString = strString.trim();
		var strValidChars = "0123456789";		
		var blnResult = true;		
		for (var i = 0; i < strString.length && blnResult == true; i++)		      {
		      var strChar = strString.charAt(i);
		      if (strValidChars.indexOf(strChar) == -1){
		         blnResult = false;
		       }
		}
		if(!blnResult){
			return objectName + ' chỉ chứa các giá trị số nguyên.';
		}else{
			return '';
		}		
	},
	getMessageOfSpecialCharactersValidate: function(objectId, objectName,type){
		objectName=Utils.language_vi(objectName);
		var selector = $('#' + objectId);
		var value = '';
		if(selector.hasClass('easyui-combobox')){
			value = $('#' + objectId).combobox('getValue').trim();
		}else{
			value = $('#' + objectId).val().trim();
		}	
		if(type == null || type == undefined){
			type = Utils._NAME;
		}
		var errMsg = '';
		if(value.length > 0){
			switch (type) {
			case Utils._CODE:
				if(!/^[a-zA-Z0-9_]+$/.test(value)){
					errMsg = format(msgErr_invalid_format_code,objectName);
				}
				break;
			case Utils._NAME:
				if(!/^[^<|>|?|\\|\'|\"|&|~#|$|%|@|*|(|)|^|`]+$/.test(value)){
					errMsg = format(msgErr_invalid_format_name,objectName);
				}
				break;
			case Utils._ADDRESS:
				if(!/^[^<|>|?|\\|\'|\"|&|~]+$/.test(value)){
					errMsg = format(msgErr_invalid_format_address,objectName);
				}
				break;
			case Utils._SPECIAL:
				if (!/^[^<|>|?|\\|\'|\"|&|~]+$/.test(value)) {
					errMsg = format(msgErr_invalid_format_special, objectName);
				}
				break;
			case Utils._PAYROLL:
				value = value.substring(3);
				if(!/^[0-9a-zA-Z]+$/.test(value)){
					errMsg = format(msgErr_invalid_format_payroll,objectName);
				}
				break;
			case Utils._TF_NUMBER_COMMA:
				if(!/^[0-9a-zA-Z_]+$/.test(value)){
					errMsg = format(msgErr_invalid_format_tf_id_number,objectName);
				}
				break;
			case Utils._NAME_CUSTYPE:
				if(!/^[^<|>|?|\\|\'|\"|&|~#|$|%|@|*|^|`]+$/.test(value)){
					errMsg = format(msgErr_invalid_format_name,objectName);
				}
				break;
			case Utils._SERIAL:
				if(!/^[0-9a-zA-Z-_.,^<|>|?|\\|\'|\"|&|~#|$|%|@|*|(|)|^|`]+$/.test(value)){
					errMsg = format(msgErr_invalid_format_tf_id_number,objectName);
				}
				break;
			default:
				break;
			}
		}	
		if(errMsg.length !=0){
			$('#' + objectId).focus();
		}
		return errMsg;
	},
	getMessageValidateLot:function(objectId){
		var lot = $('#'+objectId).val();
		if((lot==null || lot==undefined)|| (lot!=null && lot!=undefined && lot.length!=6)){
			$('#' + objectId).focus();
			return 'Số lô phải đủ 6 ký tự theo định dạng ddMMyy. Ví dụ: 011012';			
		}
		var date = new Date();
		var yy = date.getFullYear().toString().substring(0, 2);
		var dd = lot.substring(0, 2);
		var mm = lot.substring(2, 4);
		var happy_new_year = dd.toString() +"/" + mm.toString() + "/" + yy + lot.substring(4);		
		if(!Utils.isDateEx(happy_new_year, '/',Utils._DATE_DD_MM_YYYY)){
			$('#' + objectId).focus();
			return 'Số lô không đúng định dạng ddMMyy. Ví dụ: 011012';
		}
		return '';
	},
	getMessageValidateLotNew:function(lot){
		if((lot==null || lot==undefined)|| (lot!=null && lot!=undefined && lot.length!=6)){
			return 'Số lô phải đủ 6 ký tự theo định dạng ddMMyy. Ví dụ: 011012';			
		}
		var msg = Utils.getMessageOfInvaildNumberNew(lot);
		if(msg.length>0){
			return 'Số lô phải đủ 6 ký tự theo định dạng ddMMyy. Ví dụ: 011012';
		}
		var date = new Date();
		var yy = date.getFullYear().toString().substring(0, 2);
		var dd = lot.substring(0, 2);
		var mm = lot.substring(2, 4);
		var happy_new_year = dd.toString() +"/" + mm.toString() + "/" + yy + lot.substring(4);		
		if(!Utils.isDateEx(happy_new_year, '/',Utils._DATE_DD_MM_YYYY)){
			return 'Số lô không đúng định dạng ddMMyy. Ví dụ: 011012';
		}
		return '';
	},
	getMessageOfSpecialCharactersValidateEx: function(objectId, objectName,type, prefix){
		objectName=Utils.language_vi(objectName);
		var input ='';
		if(prefix!= undefined && prefix!= null && prefix.length > 0){
			input = prefix + '#'+ objectId;
		}else{
			input = '#'+ objectId;
		}
		var value =$(input).val().trim();
		if(type == null || type == undefined){
			type = Utils._TF_A_Z;
		}
		var errMsg = '';
		var reg = /[^0-9]/;
		var regAll = /[^0-9]/g;
		if(value.length > 0){
			switch (type) {
			case Utils._TF_A_Z:	
				if(!/^[A-Za-z]+$/.test(value)){
					errMsg = format(msgErr_invalid_format_az,objectName);
				}
				break;
			case Utils._TF_NUMBER_DOT:		
				if(!/^[0-9.]+$/.test(value)){
					errMsg = format(msgErr_invalid_format_num_dot,objectName);
				}
				break;
			case Utils._TF_NUMBER:			
				if(!/^[0-9]+$/.test(value)){
					errMsg = format(msgErr_invalid_format_num,objectName);
				}
				break;
			case Utils._TF_NUMBER_COMMA:			
				if(!/^[0-9,]+$/.test(value)){
					errMsg = format(msgErr_invalid_format_comma,objectName);
				}
				break;
			case Utils._TF_NUMBER_SIGN:			
				if(!/^[0-9-]+$/.test(value)){
					errMsg = format(msgErr_invalid_format_sign,objectName);
				}
				break;
			case Utils._TF_NUMBER_CONVFACT:				 
				if(!/^[0-9\/]+$/.test(value)){
					errMsg = format(msgErr_invalid_format_confact,objectName);
				}else if(value.indexOf('/')!=-1 && value.split('/').length >= 2){
					errMsg = format(msgErr_invalid_format_confact,objectName);
				}
				break;
			case Utils._TF_NUMBER_COMMA_AND_DOT:			
				if(!/^[0-9.,]+$/.test(value)){
					errMsg = format(msgErr_invalid_format_comma_dot,objectName);
				}
				break;
			case Utils._TF_CAR_NUMBER:			
				if(!/^[A-Za-z0-9-]+$/.test(value)){
					errMsg = format(msgErr_invalid_format_car_number,objectName);
				}
				break;
			case Utils._TF_PHONE_NUMBER:
				if(!/^[0-9-.() ]+$/.test(value)){
					errMsg = format(msgErr_invalid_format_num,objectName);
				}
				break;
			default:
				break;
			}
		}
		if(errMsg.length !=0){
			$('#' + objectId).focus();
		}
		return errMsg;
	},
	getMessageOfFloatValidate: function(objectId, objectName){
		objectName=Utils.language_vi(objectName);
		if($('#' + objectId).val().trim() != '' && !/^[0-9]+\.[0-9]+$|^[0-9]+$/.test($('#' + objectId).val().trim())){
			$('#' + objectId).focus();
			return objectName + ' chỉ gồm số và dấu "." ';
		}
		return '';
	},
	getMessageOfMaxlengthValidate: function(objectId, objectName,maxlength){
		objectName=Utils.language_vi(objectName);
		if($('#' + objectId).val().trim() != '' && $('#' + objectId).val().trim().length > maxlength){
			$('#' + objectId).focus();
			return objectName + ' không vượt quá '+maxlength +' ký tự.';
		}
		return '';
	},
	getMessageOfNegativeNumberCheck: function(objectId, objectName,type){
		objectName=Utils.language_vi(objectName);
		if(type == null || type == undefined){
			type = Utils._TF_NUMBER_COMMA_AND_DOT;
		}
		if($('#' + objectId).val().trim() != '' && $('#' + objectId).val().trim().length > 0){
			var errMsg = Utils.getMessageOfSpecialCharactersValidateEx(objectId, objectName,type);
			if(errMsg.length !=0){
				return errMsg;
			}else if($('#' + objectId).val().replace(/,/g,'') <= 0){
				$('#' + objectId).focus();
				return objectName + ' phải lớn hơn 0';
			}
		}
		return '';
	},
	getMessageOfInvalidFormatDate: function(objectId, objectName){
		objectName=Utils.language_vi(objectName);
		if($('#' + objectId).val().trim() == '__/__/____'){
			$('#' + objectId).val('');
			return '';
		}
		if($('#' + objectId).val().trim().length > 0 && !Utils.isDate($('#' + objectId).val().trim(), '/')){
			$('#' + objectId).focus();			
			return format(msgErr_invalid_format_date,objectName);
		}
		return '';
	},
	getMessageOfInvalidFormatDateNew: function(objectId, objectName){
		objectName=Utils.language_vi(objectName);
		if($('#' + objectId).val().trim().length > 0 && !Utils.isDateNew($('#' + objectId).val().trim())){
			$('#' + objectId).focus();			
			return format(msgErr_invalid_format_date_new,objectName);
		}
		return '';
	},
	getMessageOfEmptyDate: function(objectId, objectName){
		objectName=Utils.language_vi(objectName);
		if($('#' + objectId).val().trim().length == 0 ){
			$('#' + objectId).focus();			
			return format(msgErr_empty_date,objectName);
		}
		return '';
	},
	getMessageOfInvalidFormatLot: function(objectId, objectName,maxlength){
		objectName=Utils.language_vi(objectName);
		var lot = $('#' + objectId).val().trim();
		if(lot.length > 0){
			if( lot.length != maxlength) {
				$('#' + objectId).focus();
				return objectName + ' phải bao gồm '+maxlength +' ký tự.';
			} else {
				var date = new Date();
				var yy = date.getFullYear().toString().substring(0, 2);
				var dd = lot.substring(0, 2);
				var mm = lot.substring(2, 4);
				var happy_new_year = dd.toString() +"/" + mm.toString() + "/" + yy + lot.substring(4);		
				if(!Utils.isDateEx(happy_new_year, '/',Utils._DATE_DD_MM_YYYY)){
					$('#' + objectId).focus();
					return objectName +' không đúng định dạng ddmmyy.';
				}	
			}
		}
		return '';
	},
	getMessageOfInvalidFormatDateEx: function(objectId, objectName,type){
		objectName=Utils.language_vi(objectName);
		if($('#' + objectId).val().trim().length > 0 && !Utils.isDateEx($('#' + objectId).val().trim(), '/',type)){
			$('#' + objectId).focus();	
			if(type == null || type == undefined){
				type = Utils._DATE_DD_MM_YYYY;
			}
		    switch (type) {
				case Utils._DATE_DD_MM_YYYY:
					return format(msgErr_invalid_format_date,objectName);
					break;
				case Utils._DATE_MM_YYYY:
					return format(msgErr_invalid_format_month,objectName);
					break;
				default:
					break;
			}
		}
		return '';
	},
	getMessageOfInvalidEmailFormat: function(objectId, objectName){
		objectName=Utils.language_vi(objectName);
		if($('#' + objectId).val().trim() != '' && !/^[a-zA-Z0-9._-]{1,64}@[A-Za-z0-9]{2,64}(\.[A-Za-z0-9]{2,64})*(\.[A-Za-z]{2,4})$/.test($('#' + objectId).val().trim())){
			$('#' + objectId).focus();
			return format(msgErr_invalid_format_email,objectName);			
		}
		return '';
	},
	getMessageOfInvalidMaxLength:function(objectId, objectName){
		objectName=Utils.language_vi(objectName);
		var maxlength = $('#' + objectId).attr('maxlength');
		if(maxlength!= undefined && maxlength!= null && maxlength.length > 0){
			if($('#' + objectId).val().trim().replace(",","").length > maxlength){
				$('#' + objectId).focus();
				return format(msgErr_invalid_maxlength,objectName,maxlength);
			}
		}
		return '';
	},
	getMessageOfInvaildNumber:function(objectId, objectName){
		objectName=Utils.language_vi(objectName);
		var strString = $('#' + objectId).val().trim();
		var strValidChars = "0123456789,.";		
		var blnResult = true;		
		for (i = 0; i < strString.length && blnResult == true; i++)		      {
		      var strChar = strString.charAt(i);
		      if (strValidChars.indexOf(strChar) == -1){
		         blnResult = false;
		       }
		}
		if(!blnResult){
			return objectName + ' chỉ chứa các giá trị số và dấu ,';
		}else{
			return '';
		}		
	},
	getMessageOfInvaildNumberStartWithZero:function(objectId, objectName){
		objectName=Utils.language_vi(objectName);
		var strString = $('#' + objectId).val().trim();
		var strValidChars = "0123456789,.";		
		var blnResult = true;		
		for (i = 0; i < strString.length && blnResult == true; i++)		      {
		      var strChar = strString.charAt(i);
		      if (strValidChars.indexOf(strChar) == -1){
		         blnResult = false;
		       }
		}
		if(!blnResult){
			return objectName + ' chỉ chứa các giá trị số và dấu ,';
		}else{
			if(strString.length != 0 && strString.charAt(0) != '0'){
				return objectName + ' phải bắt đầu là 0';
			}else{
				return '';
			}
		}		
	},
	getMessageOfInvaildNumberNew:function(strString){
		if(strString==undefined){
			strString = '';
		}
		var strValidChars = "0123456789";		
		var blnResult = true;		
		for (i = 0; i < strString.length && blnResult == true; i++)		      {
		      var strChar = strString.charAt(i);
		      if (strValidChars.indexOf(strChar) == -1){
		         blnResult = false;
		       }
		}
		if(!blnResult){
			return 'chỉ chứa các giá trị số';
		}else{
			return '';
		}		
	},
	getMessageOfInvaildNumberEx:function(objectId, objectName, precision,scale ,isPossitive,maxlength, formatType){
		objectName=Utils.language_vi(objectName);
		var value = $('#' + objectId).val().trim();
		switch (formatType) {
		case Utils._TF_NUMBER_DOT:
			reg = /^[0-9.]+$/;
			msg = 'chỉ gồm số và dấu "."';
			break;
		case Utils._TF_NUMBER:
			reg = /^[0-9]+$/;
			msg = 'chỉ gồm số';
			break;
		case Utils._TF_NUMBER_COMMA:
			reg = /^[0-9,]+$/;
			msg = 'chỉ gồm số và dấu ","';
			break;
		case Utils._TF_NUMBER_SIGN:
			reg = /^[0-9-]+$/;
			msg = 'chỉ gồm số và dấu "-"';
			break;
		case Utils._TF_NUMBER_COMMA_AND_DOT:
			reg = /^[0-9.,]+$/;
			msg = 'chỉ gồm số và dấu "." và dấu","';
			break;
		default:
			break;
		}
		if(value != '' && value.length > maxlength){
			$('#' + objectId).focus();
			return objectName + ' không vượt quá '+maxlength +' ký tự.';
		}
		if( value != '' && !reg.test(value)){
			$('#' + objectId).focus();
			return objectName + msg;
		}
		if(value.indexOf(',')>-1){
			value = value.split(',').join('');
		}
		value = Number(value);
		if(isNaN(value)) {
			$('#' + objectId).focus();
			return objectName + ' không phải là số';
		}
		if(isPossitive == true){
			if(Number(value)<=0) {
				$('#' + objectId).focus();
				return objectName + ' phải lớn hơn 0';
			}
		}
		if(value.indexOf('.')>-1){
			var decimal='';
			Num = value.split('.')[0];
			decimal = value.split('.')[1];
		}else{
			Num = value.toString();
		}
		var count = precision-scale;
		if(Number(Num) >= Math.pow(10,count)){
			$('#' + objectId).focus();
			return objectName + 'phải có phần nguyên không được vượt quá '+count +'kí tự';
		}
		return '';
	},

	/**
	 * check change pass
	 * @author vuongmq
	 * @param objectId
	 * @param objectName
	 * @param maxlength
	 * @return String
	 * @since 04/09/2015 
	 */
	getMessageOfPolicyPassWord: function(objectId, objectName, maxlength) {
		var value = $('#' + objectId).val();
		if ((value != '' && value.length < maxlength) || !/([A-Z]+)/.test(value) || !/([a-z]+)/.test(value)) {
			$('#' + objectId).focus();
			return objectName + ' bắt buộc phải có ít nhất 6 ký tự, có chữ hoa và chữ thường';
		}
		return '';
	},

	updateTokenForJSON: function(data){
		$('#token').val(data.token);
		var importTokenVal = $('#importTokenVal').val();
		if(importTokenVal!=null && importTokenVal!=undefined && importTokenVal.trim().length>0){
			$('#importTokenVal').val(data.token);
		}else{
			//Trong phan import chuong trinh khuyen mai truoc gio su dung $('#tokenImport')
			importTokenVal = $('#tokenImport').val();
			if(importTokenVal!=null && importTokenVal!=undefined && importTokenVal.trim().length>0){
				$('#tokenImport').val(data.token);
			}
		}
		
	},
	getToken: function(){
		return $('#token').val();		
	},
	setToken:function(importTokenVal){
		$('#token').val(importTokenVal);
	},
	deleteSelectRowOnGrid: function(dataModel,functionText,url,xhrDel,gridId,errMsgId,callback,loading,prefix){
		var gId = 'grid';
		if(gridId!= undefined && gridId!= null && gridId.length > 0){
			gId = gridId;
		}
		var errMsg = 'errMsg';
		if(errMsgId!= undefined && errMsgId!= null && errMsgId.length > 0){
			errMsg = errMsgId;
		}
		$.messager.confirm(jsp_common_xacnhan,msgCommon2, function(r){ // 'Bạn có muốn xóa'+ functionText +' này?'
			if (r){
				Utils.saveData(dataModel, url, xhrDel, errMsg, function(data){
					$("#"+gId).datagrid("reload");
					if(callback!= null && callback!= undefined){
						callback.call(this,data);
					}
				},loading,prefix,true);
			}else{
				var isCheck = false;
				$('.InputTextStyle').each(function(){
				    if(!$(this).is(':hidden') && !$(this).is(':disabled') && !isCheck){
				        isCheck = true;
				        $(this).focus();
				    }
				});
			}
		});		
		return false;
	},
	deleteMapProductGrid: function(dataModel,functionText,url,xhrDel,gridId,errMsgId,callback,loading,prefix){
		var gId = 'grid';
		if(gridId!= undefined && gridId!= null && gridId.length > 0){
			gId = gridId;
		}
		var errMsg = 'errMsg';
		if(errMsgId!= undefined && errMsgId!= null && errMsgId.length > 0){
			errMsg = errMsgId;
		}
		$.messager.confirm('Xác nhận', 'Bạn có muốn xóa '+ Utils.XSSEncode(functionText) +' ?', function(r){
			if (r){
				Utils.addOrSaveData(dataModel, url, xhrDel, errMsgId, function(data){
					$("#"+ gId).trigger("reloadGrid");
					if(callback!= null && callback!= undefined){
						callback.call(this,data);
					}
				},loading,prefix,true);
			}else{
				var isCheck = false;
				$('.InputTextStyle').each(function(){
				    if(!$(this).is(':hidden') && !$(this).is(':disabled') && !isCheck){
				        isCheck = true;
				        $(this).focus();
				    }
				});
			}
		});		
		return false;
	},
	addOrSaveRowOnGrid: function(dataModel,url,xhrSave,gridId,errMsgId,callback,loading){
		var gId = 'grid';
		if(gridId!= undefined && gridId!= null && gridId.length > 0){
			gId = gridId;
		}
		Utils.addOrSaveData(dataModel, url, xhrSave, errMsgId, function(data){
			$("#"+ gId).datagrid('reload');
			if(callback!= null && callback!= undefined){
				callback.call(this,data);
			}
		},loading);		
		return false;
	},
	/*addOrSaveData: function(dataModel, url, xhrSave, errMsgId, callback,loading,prefix,isDelete,message,callBackFail,hideSuccessMsg){
		if(isDelete == undefined || isDelete == null || isDelete == false){
			var msg;
			if(message == null || message == '' || message == undefined) {
				//msg = 'Bạn có muốn lưu thông tin này?';
				msg = jsp_common_ban_luu_thong_tin_nay;
			} else {
				msg = message;
			}
			var isDialogMsg = $('.panel.window.messager-window').html();
			if(isDialogMsg==undefined || isDialogMsg==null){
				$.messager.confirm(jsp_common_xacnhan, msg, function(r){
					if (r){
						if(dataModel.changeCus != undefined && dataModel.changeCus == 3){
							dataModel.changeCus = 1;
						}
						Utils.saveData(dataModel, url, xhrSave, errMsgId, callback,loading,prefix,isDelete,callBackFail,hideSuccessMsg);
					}else{// lampv-for map product to device
						$('.fancybox-inner #btnCreate').removeAttr("disabled");
						$('.fancybox-inner #btnClose').removeAttr("disabled");
					}
				});
			}
		} else {
			Utils.saveData(dataModel, url, xhrSave, errMsgId, callback,loading,prefix,isDelete,callBackFail,hideSuccessMsg);
		}
		return false;
	},*/
	
	//trungtm6
	addOrSaveData: function(dataModel, url, xhrSave, errMsgId, callback,loading,prefix,isDelete,message,callBackFail, hideSuccessMsg){
		if(isDelete == undefined || isDelete == null || isDelete == false){
			var msg;
			if(message == null || message == '' || message == undefined) {
				msg = jsp_common_ban_luu_thong_tin_nay;
			} else {
				msg = message;
			}
			$.messager.confirm(jsp_common_xacnhan, msg, function(r){
				if (r){
					if(dataModel.changeCus != undefined && dataModel.changeCus == 3){
						dataModel.changeCus = 1;
					}
					if(dataModel.indexRow != undefined && dataModel.indexRow !=null){
						$('#grid').datagrid('endEdit', dataModel.indexRow);
					}
					Utils.saveData(dataModel, url, xhrSave, errMsgId, callback,loading,prefix,isDelete,callBackFail, hideSuccessMsg);
				}else{// lampv-for map product to device
					$('.fancybox-inner #btnCreate').removeAttr("disabled");
					$('.fancybox-inner #btnClose').removeAttr("disabled");
					if(dataModel.indexRow != undefined && dataModel.indexRow !=null){
						$('#grid').datagrid('cancelEdit', dataModel.indexRow);
					}
				}
			});			
		} else {
			$.messager.confirm(jsp_common_xacnhan, jsp_common_xacnhan_xoa, function(r){
				if (r){
					Utils.saveData(dataModel, url, xhrSave, errMsgId, callback,loading,prefix,isDelete,callBackFail, hideSuccessMsg);
				}else{// lampv-for map product to device
					$('.fancybox-inner #btnCreate').removeAttr("disabled");
					$('.fancybox-inner #btnClose').removeAttr("disabled");
				}
			});	
		}
		return false;
	},
	
	saveData: function(dataModel, url, xhrSave, errMsgId, callback,loading,prefix,isDelete,callBackFail,hideSuccessMsg){
		var errMsg = 'errMsg';
		if(errMsgId!= undefined && errMsgId!= null && errMsgId.length > 0){
			errMsg = errMsgId;
		}
		if(prefix!= undefined && prefix!= null && prefix.length > 0){
			$(prefix + ' #'+ errMsg).html('').hide();
		}else{
			$('#'+ errMsg).html('').hide();
		}
		if(prefix!= undefined && prefix!= null && prefix.length > 0){
			$(prefix + '#successMsg').html('').hide();
		}else{
			$('#successMsg').html('').hide();
		}	
		
		if(dataModel == null || dataModel == undefined){
			dataModel = new Object();
		}
		dataModel.token = Utils.getToken();
		var kData = $.param(dataModel, true);
		if(xhrSave!=null){
			xhrSave.abort();
		}
		// showLoading(loading);
		$('#divOverlay').show();
		$('#imgOverlay').show();
		var errType = jsp_common_luu;
		if(isDelete != undefined && isDelete != null && isDelete == true){
			errType = msgText4; // xoa
		}
		xhrSave = $.ajax({
			type : "POST",
			url : url,
			data :(kData),
			dataType : "json",
			success : function(data) {				
				$('#divOverlay').hide();
				$('#imgOverlay').hide();
				// hideLoading(loading);
				Utils.updateTokenForJSON(data);
				if(data.hasMessageListCustomer != undefined && data.hasMessageListCustomer){
					var messageCus = data.hasMessageListCustomer;
					dataModel.changeCus = 3;
					hideSuccessMsg = true;
					Utils.addOrSaveData(dataModel, url, xhrSave, errMsgId, callback,loading,prefix,isDelete,messageCus,callBackFail);
				}
				if(data.error && data.errMsg!= undefined){
					if(prefix!= undefined && prefix!= null && prefix.length > 0){
						$(prefix + ' #'+ errMsg).html(Utils.XSSEncode(data.errMsg)).show();
					}else{
						$('#'+ errMsg).html(escapeSpecialChar(data.errMsg)).show();
					}
					if(callBackFail!=null && callBackFail!=undefined){
						callBackFail.call(this,data);
					}
				} else {					
					if(callback!= null && callback!= undefined){
						callback.call(this,data);
					}
					if(hideSuccessMsg == null || hideSuccessMsg == undefined || hideSuccessMsg == ""){
						if(prefix!= undefined && prefix!= null && prefix.length > 0){
							$(prefix + ' #successMsg').html(errType + ' '+ jsp_common_du_lieu_thanh_cong).show();
						}else{
							$('#successMsg').html(errType + ' '+jsp_common_du_lieu_thanh_cong).show();
						}
						var tm = setTimeout(function(){
							if(prefix!= undefined && prefix!= null && prefix.length > 0){
								$(prefix + '#successMsg').html('').hide();
							}else{
								$('#successMsg').html('').hide();
							}
							clearTimeout(tm);
						}, 3000);
					}
					var isFocus = false;
					$('.GeneralMilkInBox .InputTextStyle').each(function(){
					    if(!$(this).is(':hidden') && !isFocus){
					    	isFocus = true;
					        $(this).focus();
					    }
					});
				}
				xhrSave = null;				
			},
			error:function(XMLHttpRequest, textStatus, errorThrown) {
			}
		});
	},
	getHtmlDataByAjaxNotOverlay: function(params,url,callback,loading,requestType){
		var rt = 'POST';
		var kData = $.param(params, true);
		if(requestType!= null && requestType!= undefined && requestType!=''){
			rt = requestType;
		}
		$.ajax({
			type : rt,
			url : url,
			data :(kData),
			dataType : "html",
			success : function(data) {
				if(callback!= null && callback!= undefined){
					callback.call(this,data);
				}
			},
			error:function(XMLHttpRequest, textStatus, errorThrown) {
			}
		});
		return false;
	},
	getHtmlDataByAjax: function(params,url,callback,loading,requestType){
		var rt = 'POST';
		var kData = $.param(params, true);
		if(requestType!= null && requestType!= undefined && requestType!=''){
			rt = requestType;
		}
		$('#divOverlay').show();
		$.ajax({
			type : rt,
			url : url,
			data :(kData),
			dataType : "html",
			success : function(data) {
				$('#divOverlay').hide();
				if(callback!= null && callback!= undefined){
					callback.call(this,data);
				}
			},
			error:function(XMLHttpRequest, textStatus, errorThrown) {
				$('#divOverlay').hide();
				hideLoading(loading);
			}
		});
		return false;
	},
	getJSONDataByAjax: function(params,url,callback,loading,requestType){
		var rt = 'POST';
		var kData = $.param(params, true);
		if(requestType!= null && requestType!= undefined && requestType!=''){
			rt = requestType;
		}
		$('#divOverlay').addClass('Overlay');
		$('#divOverlay').show();
		$('#imgOverlay').show();
		$.ajax({
			type : rt,
			url : url,
			data :(kData),
			dataType : "json",
			success : function(data) {
				$('#divOverlay').removeClass('Overlay');
				$('#imgOverlay').hide();
				$('#divOverlay').hide();
				if(callback!= null && callback!= undefined){
					callback.call(this,data);
				}
			},
			error:function(XMLHttpRequest, textStatus, errorThrown) {
				$('#divOverlay').removeClass('Overlay');
				$('#divOverlay').hide();
				hideLoading(loading);
			}
		});
		return false;
	},
	getJSONDataByAjaxNotOverlay: function(params,url,callback,loading,requestType){
		var rt = 'POST';
		var kData = $.param(params, true);
		if(requestType!= null && requestType!= undefined && requestType!=''){
			rt = requestType;
		}
		$.ajax({
			type : rt,
			url : url,
			data :(kData),
			dataType : "json",
			success : function(data) {
				if(callback!= null && callback!= undefined){
					callback.call(this,data);
				}
			},
			error:function(XMLHttpRequest, textStatus, errorThrown) {
			}
		});
		return false;
	},
	checkIsJSON:function(object) {
		var stringConstructor = "test".constructor;
		var arrayConstructor = [].constructor;
		var objectConstructor = {}.constructor;
		if (object === null) {
	        return false;
	    }
	    else if (object === undefined) {
	        return false;
	    }
	    else if (object.constructor === stringConstructor) {
	        return false;
	    }
	    else if (object.constructor === arrayConstructor) {
	        return false;
	    }
	    else if (object.constructor === objectConstructor) {
	        return true;
	    }
	    else {
	        return false;
	    }
	},
	formatDoubleValue: function(value){
		if(value == null || value==""){return value;}
		var num = new Number(value);
		var s = num.toFixed(2);
		var index = s.lastIndexOf('.00');
		if(index>-1){
			return num.toFixed(0);
		}else{
			return num.toFixed(2);
		}
	},
	doubleNumbersOnly: function(e) {
		var key;
		var keychar;
		if (window.event) {
		   key = window.event.keyCode;
		}else if (e) {
		   key = e.which;
		}else {
		   return true;
		}
		keychar = String.fromCharCode(key);

		if ((key==null) || (key==0) || (key==8) ||  (key==9) || (key==13) || (key==27) ) {
		   return true;
		}else if ((("0123456789.").indexOf(keychar) > -1)) {
			return true;
		}else{
		   return false;
		}
	},
	compareCurrentDateEx: function(objectId, objectName) {
		var date = $('#' + objectId).val().trim();
		//date < now => true || date > now => false
		if (date.length == 0) {
			return '';
		}
		var now = new Date();
		var cYear = now.getFullYear();
		var cMonth = now.getMonth() + 1;
		var cDate = now.getDate();
		var currentDate = cDate+'/'+cMonth+'/'+cYear;
		if (!Utils.compareDate(date, currentDate)) {
			$('#'+objectId).focus();
			return objectName + ' phải nhỏ hơn hoặc bằng ngày hiện tại. Vui lòng nhập lại';
		}
		return '';
	},
	compareCurrentDateGreaterEx:function(objectId, objectName) {
		var date = $('#' + objectId).val().trim();
		//date < now => true || date > now => false
		if(date.length == 0) {
			return '';
		}
		var now = new Date();
		var cYear = now.getFullYear();
		var cMonth = now.getMonth() + 1;
		var cDate = now.getDate();
		var currentDate = cDate+'/'+cMonth+'/'+cYear;
		if(Utils.compareDate(date, currentDate)) {
			$('#'+objectId).focus();
			return objectName + ' phải lớn hơn ngày hiện tại. Vui lòng nhập lại';
		}
		return '';
	},
	compareCurrentDate:function(date) {
		// date < now => true || date > now => false
		if(date.length == 0) {
			return false;
		}
		var now = new Date();
		var cYear = now.getFullYear();
		var cMonth = now.getMonth() + 1;
		var cDate = now.getDate();
		var currentDate = cDate+'/'+cMonth+'/'+cYear;
		return Utils.compareDate(date, currentDate);
	},
	compareCurrentDate1:function(date) {
		// date < now => true || date > now => false
		if(date.length == 0) {
			return false;
		}
		var now = new Date();
		var cYear = now.getFullYear();
		var cMonth = now.getMonth() + 1;
		var cDate = now.getDate();
		var currentDate = cDate+'/'+cMonth+'/'+cYear;
		return Utils.compareDate1(date, currentDate);
	},
	compareCurrentDateExReturn:function(startDate,endDate) {		
		if(startDate.length == 0 || endDate.length == 0){
			return true;			
		}
		var arrStartDate = startDate.split('/');
		var arrEndDate = endDate.split('/');
		var startDateObj = dates.convert(arrStartDate[1] + '/' + arrStartDate[0] + '/' + arrStartDate[2]);
		var endDateObj = dates.convert(arrEndDate[1] + '/' + arrEndDate[0] + '/' + arrEndDate[2]);
		return dates.compare(startDateObj,endDateObj);
	},
	compareDate: function(startDate, endDate){
		if(startDate.length == 0 || endDate.length == 0){
			return true;			
		}
		var arrStartDate = startDate.split('/');
		var arrEndDate = endDate.split('/');
		var startDateObj = dates.convert(arrStartDate[1] + '/' + arrStartDate[0] + '/' + arrStartDate[2]);
		var endDateObj = dates.convert(arrEndDate[1] + '/' + arrEndDate[0] + '/' + arrEndDate[2]);
		if(dates.compare(startDateObj,endDateObj) > 0){
			return false;
		}
		return true;
	},
	compareDateInt: function(startDate, endDate){
		if(startDate.length == 0 || endDate.length == 0){
			return true;			
		}
		var arrStartDate = startDate.split('/');
		var arrEndDate = endDate.split('/');
		var startDateObj = dates.convert(arrStartDate[1] + '/' + arrStartDate[0] + '/' + arrStartDate[2]);
		var endDateObj = dates.convert(arrEndDate[1] + '/' + arrEndDate[0] + '/' + arrEndDate[2]);
		return dates.compare(startDateObj,endDateObj);
	},
	compareDateNew: function(startDate, endDate){
		if(startDate.length == 0 || endDate.length == 0){
			return true;			
		}
		var __arrStartDate = startDate.split(' ');
		var __arrEndDate = endDate.split(' ');
		var startDateObj = new Date(__arrStartDate[0].split('/')[2], __arrStartDate[0].split('/')[1], __arrStartDate[0].split('/')[0], __arrStartDate[1].split(':')[0], __arrStartDate[1].split(':')[1]);
		var endDateObj = new Date(__arrEndDate[0].split('/')[2], __arrEndDate[0].split('/')[1], __arrEndDate[0].split('/')[0], __arrEndDate[1].split(':')[0], __arrEndDate[1].split(':')[1]);
		if(dates.compare(startDateObj,endDateObj) > 0){
			return false;
		}
		return true;
	},
	compareDate1: function(startDate, endDate){
		if(startDate.length == 0 || endDate.length == 0){
			return true;			
		}
		var arrStartDate = startDate.split('/');
		var arrEndDate = endDate.split('/');
		var startDateObj = dates.convert(arrStartDate[1] + '/' + arrStartDate[0] + '/' + arrStartDate[2]);
		var endDateObj = dates.convert(arrEndDate[1] + '/' + arrEndDate[0] + '/' + arrEndDate[2]);
		if(dates.compare(startDateObj,endDateObj) >= 0){
			return false;
		}
		return true;
	},
	compareMonth: function(startMonth, endMonth){
		if(startMonth.length == 0 || endMonth.length == 0){
			return true;			
		}
		var arrStartMonth = startMonth.split('/');
		var arrEndMonth = endMonth.split('/');
		var startDateObj = dates.convert(arrStartMonth[0] + '/' + '01' + '/' + arrStartMonth[1]);
		var endDateObj = dates.convert(arrEndMonth[0] + '/' + arrEndMonth[0] + '/' + arrEndMonth[1]);
		if(dates.compare(startDateObj,endDateObj) > 0){
			return false;
		}
		return true;
	},
	// Don't delete!!
	compareCurrentMonthEx: function(monthTxt) {//neu monthTxt >= thang hien tai => true. ko thi false
		var currentDateTxt = toDateString(new Date());
		var arrCurrentDate = currentDateTxt.split('/');
		var arrMonthTxt = monthTxt.split('/');
		if((Number(arrMonthTxt[1]) > Number(arrCurrentDate[2])) || (Number(arrMonthTxt[1]) == Number(arrCurrentDate[2]) && arrMonthTxt[0]>=arrCurrentDate[1])) {
			return true;
		}
		return false;
	},
	/**
	 * So sanh 2 thang 
	 * input : String dd/mm/yyyy
	 * output:
	 * -2 : khong hop le
	 * -1 : thang a truoc thang b (a < b)
	 * 0 : cung thang (a = b)
	 * 1 : thang a sau thang b (a > b)
	 * 
	 */
	compareMonthString: function(startMonth, endMonth){ 
		if(startMonth.length == 0 || endMonth.length == 0){
			return -2;		
		}
		var arrStartMonth = startMonth.split('/');
		var arrEndMonth = endMonth.split('/');
		
		if(parseInt(arrStartMonth[2], 10) == parseInt(arrEndMonth[2], 10)){
			if(parseInt(arrStartMonth[1], 10) == parseInt(arrEndMonth[1], 10)){
				return 0; 
			}else if(parseInt(arrStartMonth[1], 10) < parseInt(arrEndMonth[1], 10)){
				return -1;
			}else{
				return 1;
			}
		}else if(parseInt(arrStartMonth[2], 10) < parseInt(arrEndMonth[2], 10)){
			return -1; 
		}else{
			return 1; 
		}
	},
	isDate:function(txtDate, separator) {
	    var aoDate,           // needed for creating array and object
	        ms,               // date in milliseconds
	        month, day, year; // (integer) month, day and year
	    // if separator is not defined then set '/'
	    if (separator == undefined) {
	        separator = '/';
	    }
	    // split input date to month, day and year
	    aoDate = txtDate.split(separator);
	    // array length should be exactly 3 (no more no less)
	    if (aoDate.length !== 3) {
	        return false;
	    }
	    // define month, day and year from array (expected format is m/d/yyyy)
	    // subtraction will cast variables to integer implicitly
	    day = aoDate[0] - 0; // because months in JS start from 0
	    month = aoDate[1] - 1;
	    year = aoDate[2] - 0;
	    // test year range
	    if (year < 1000 || year > 3000) {
	        return false;
	    }
	    // convert input date to milliseconds
	    ms = (new Date(year, month, day)).getTime();
	    // initialize Date() object from milliseconds (reuse aoDate variable)
	    aoDate = new Date();
	    aoDate.setTime(ms);
	    // compare input date and parts from Date() object
	    // if difference exists then input date is not valid
	    if (aoDate.getFullYear() !== year ||
	        aoDate.getMonth() !== month ||
	        aoDate.getDate() !== day) {
	        return false;
	    }
	    // date is OK, return true
	    return true;
	},
	isDateNew:function(txtDate) {
	    var __str = txtDate.split(' ');
	    if($.isArray(__str) && __str.length == 2) {
	    	if(!Utils.isDate(__str[0])) {
	    		return false;
	    	}
	    	var __ex = __str[1].split(':');
	    	if(!(0 <= Number(__ex[0]) && Number(__ex[0]) <= 24)) {
	    		return false;
	    	}
	    	if(!(0 <= Number(__ex[1]) && Number(__ex[1]) <= 59)) {
	    		return false;
	    	}
	    	return true;
	    } else {
	    	return false;
	    }
	},
	getDataComboboxCustomer:function(objectId,prefix){
		var data = new Array();		
		var parent = '';
		if(prefix!=undefined && prefix!=null){
			parent = prefix;
		}
		$(parent + '#' + objectId +' option').each(function(){
			var obj = new Object();
			obj.shortCode = $(this).val().trim();
			obj.customerName = $(this).text().trim();
			obj.displayText = $(this).val().trim() + ' - ' + $(this).text().trim(); 
			obj.searchText = unicodeToEnglish($(this).val().trim()+ $(this).text().trim());
			obj.searchText = obj.searchText.toUpperCase();
			data.push(obj);
		});
		return data;
	},
	getDataComboboxStaff:function(objectId,prefix){
		var data = new Array();		
		var parent = '';
		if(prefix!=undefined && prefix!=null){
			parent = prefix;
		}
		$(parent + '#' + objectId +' option').each(function(){
			var obj = new Object();
			obj.staffCode = $(this).val().trim();
			obj.staffName = $(this).text().trim();
			obj.displayText = $(this).val().trim() + ' - ' + $(this).text().trim(); 
			obj.searchText = unicodeToEnglish($(this).val().trim()+ $(this).text().trim());
			obj.searchText = obj.searchText.toUpperCase();
			data.push(obj);
		});
		return data;
	},
	getDataComboboxCar:function(objectId,prefix){
		var data = new Array();		
		var parent = '';
		if(prefix!=undefined && prefix!=null){
			parent = prefix;
		}
		$(parent + '#' + objectId +' option').each(function(){
			var obj = new Object();
			obj.id = $(this).val().trim();
			obj.carNumber = $(this).text().trim();
			obj.displayText = $(this).text().trim(); 
			obj.searchText = unicodeToEnglish($(this).text().trim());
			obj.searchText = obj.searchText.toUpperCase();
			data.push(obj);
		});
		return data;
	},
	bindComboboxCarEasyUI:function(objectId,prefix){
		var parent = '';
		if(prefix!=undefined && prefix!=null){
			parent = prefix;
		}			
		$(parent + '#'+objectId).combobox({			
			valueField : 'id',
			textField : 'carNumber',
			data : Utils.getDataComboboxCar(objectId,prefix),
			//width:250,
			panelWidth:206,
			formatter: function(row) {
				return '<span style="font-weight:bold">' + Utils.XSSEncode(row.carNumber) + '</span>';
			},
			filter: function(q, row){
				q = new String(q).toUpperCase().trim();
				var opts = $(this).combobox('options');
				return row['searchText'].indexOf(unicodeToEnglish(q))>=0;
			},			
			onChange:function(newvalue,oldvalue){		    	
			}
		});
		$('.combo-arrow').unbind('click');
	},
	bindComboboxCustomerEasyUI:function(objectId,prefix){
		var parent = '';
		if(prefix!=undefined && prefix!=null){
			parent = prefix;
		}			
		$(parent + '#'+objectId).combobox({			
			valueField : 'shortCode',
			textField : 'customerName',
			data : Utils.getDataComboboxCustomer(objectId,prefix),
			//width:250,
			panelWidth:206,
			formatter: function(row) {
				return '<span style="font-weight:bold">' + Utils.XSSEncode(row.shortCode) + '</span><br/>' + '<span style="color:#888">' + Utils.XSSEncode(row.customerName) + '</span>';
			},
			filter: function(q, row){
				q = new String(q).toUpperCase();
				var opts = $(this).combobox('options');
				return row['searchText'].indexOf(unicodeToEnglish(q))>=0;
			},			
			onChange:function(newvalue,oldvalue){		    	
			}
		});
		$('.combo-arrow').unbind('click');
	},
	bindComboboxStaffEasyUI:function(objectId,prefix){
		var parent = '';
		if(prefix!=undefined && prefix!=null){
			parent = prefix;
		}			
		$(parent + '#'+objectId).combobox({			
			valueField : 'staffCode',
			textField : 'staffCode',
			data : Utils.getDataComboboxStaff(objectId,prefix),
			//width:250,
			panelWidth:206,
			formatter: function(row) {
				return '<span style="font-weight:bold">' + Utils.XSSEncode(row.staffCode) + '</span><br/>' + '<span style="color:#888">' + Utils.XSSEncode(row.staffName) + '</span>';
			},
			filter: function(q, row){
				q = new String(q).toUpperCase().trim();
				var opts = $(this).combobox('options');
				return row['searchText'].indexOf(unicodeToEnglish(q))>=0;
			},			
			onChange:function(newvalue,oldvalue){		    	
			}
		});
		$('.combo-arrow').unbind('click');
	},
	bindComboboxEquipGroupEasyUI:function(objectId, prefix){
		var parent = '';
		if (prefix != undefined && prefix != null) {
			parent = prefix;
		}			
		$(parent + '#'+objectId).combobox({			
			valueField : 'code',
			textField : 'name',
			data : Utils.getDataComboboxStaff(objectId, prefix),
			panelWidth:206,
			formatter: function(row) {
				return '<span style="font-weight:bold">' + VTUtilJS.XSSEncode(row.name) + '</span><br/>' + '<span style="color:#888">' + VTUtilJS.XSSEncode(row.code) + '</span>';
			},
			filter: function(q, row){
				q = new String(q).toUpperCase().trim();
				var opts = $(this).combobox('options');
				return row['searchText'].indexOf(unicodeToEnglish(q))>=0;
			},			
			onChange:function(newvalue, oldvalue){		    	
			}
		});
	},
	bindComboboxEquipGroupEasyUICodeName:function(objectId,prefix, width){
		var parent = '';
		if(prefix!=undefined && prefix!=null){
			parent = prefix;
		}			
		if (width == undefined) {
			width = 206;
		}
		$(parent + '#'+objectId).combobox({			
			valueField : 'id',
			// textField : 'code' + '-' + 'name',
			textField : 'codeName',
			data : Utils.getDataComboboxStaff(objectId,prefix),
			//width:250,
			panelWidth:width,
			formatter: function(row) {
				// return '<span style="font-weight:bold">' + row.code + '</span><br/>' + '<span style="color:#888">' + row.name + '</span>';
				return '<span style="font-weight:bold">' + VTUtilJS.XSSEncode(row.code) + '</span><br/>' + '<span style="color:#888">' + VTUtilJS.XSSEncode(row.name) + '</span>';
			},
			filter: function(q, row){
				q = new String(q).toUpperCase().trim();
				var opts = $(this).combobox('options');
				return row['searchText'].indexOf(unicodeToEnglish(q))>=0;
			},			
			onChange:function(newvalue,oldvalue){		    	
				//$('.combo-panel').each(function(){if(!$(this).is(':hidden')){$(this).parent().css('width','250px');$(this).css('width','248px');}});
			}
		});
		// $('.combo-arrow').unbind('click');
		//$('.combo-arrow').live('click',function(){$('.combo-panel').each(function(){if(!$(this).is(':hidden')){$(this).parent().css('width','250px');$(this).css('width','248px');}});});
	},
	/**
	 * trungtm6
	 * date: 24/04/2015
	 * bind easy ui combobox
	 * selector: selector
	 * dataArr: lst staff
	 */
	bindStaffCbx: function(selector, dataArr, idDefault, cbxWidth, selectCall){	
		var wid = 'auto';
		if (cbxWidth != undefined && cbxWidth != null && cbxWidth > 0){
			wid = cbxWidth;
		}
		$(selector).combobox({
		    data:dataArr,
		    valueField:'staffCode',
		    textField:'staffName',
		    panelWidth: wid,
		    formatter: function(row) {
				return '<span style="font-weight:bold">' + Utils.XSSEncode(row.staffCode) + '</span><br/>' + '<span style="color:#888">' + Utils.XSSEncode(row.staffName) + '</span>';
			},
			filter: function(q, row) {
				var opts = $(this).combobox('options');
                var x = unicodeToEnglish(row[opts.textField]).indexOf(unicodeToEnglish(q));
                var y = unicodeToEnglish(row[opts.valueField]).indexOf(unicodeToEnglish(q))
				return (x >= 0 | y >= 0) ;
			},
			onSelect: function(rec){
	        	if (selectCall != undefined && selectCall != null){
	        		selectCall.call(this, rec);
	        	}
	        },
			onLoadSuccess:function(){
				if (idDefault != undefined && idDefault != null) {
					$(selector).combobox('setValue',idDefault);
				}
			}
		});
	},
	/**
	 * trietptm
	 * date: 12/08/2015
	 * bind easy ui combobox set valueField: id
	 * selector: selector
	 * dataArr: lst staff
	 */
	bindStaffCbxId: function(selector, dataArr, idDefault, cbxWidth, selectCall){	
		var wid = 'auto';
		if (cbxWidth != undefined && cbxWidth != null && cbxWidth > 0){
			wid = cbxWidth;
		}
		$(selector).combobox({
		    data:dataArr,
		    valueField:'id',
		    textField:'staffName',
		    panelWidth: wid,
		    formatter: function(row) {
				return '<span style="font-weight:bold">' + Utils.XSSEncode(row.staffCode) + '</span><br/>' + '<span style="color:#888">' + Utils.XSSEncode(row.staffName) + '</span>';
			},
			filter: function(q, row) {
				var opts = $(this).combobox('options');
                var x = unicodeToEnglish(row[opts.textField]).indexOf(unicodeToEnglish(q));
                var y = unicodeToEnglish(row[opts.valueField]).indexOf(unicodeToEnglish(q))
				return (x >= 0 | y >= 0) ;
			},
			onSelect: function(rec){
	        	if (selectCall != undefined && selectCall != null){
	        		selectCall.call(this, rec);
	        	}
	        },
			onLoadSuccess:function(){
				if (idDefault != undefined && idDefault != null) {
					$(selector).combobox('setValue',idDefault);
				}
			}
		});
	},
	/**
	 * @author: trungtm6
	 */
	bindCarCbx: function(selector, dataArr, idDefault, cbxWidth, selectCall){	
		var wid = 'auto';
		if (cbxWidth != undefined && cbxWidth != null && cbxWidth > 0){
			wid = cbxWidth;
		}
		$(selector).combobox({
		    data:dataArr,
		    valueField:'id',
		    textField:'carNumber',
		    panelWidth: wid,
		    formatter: function(row) {
				return '<span style="font-weight:bold">' + Utils.XSSEncode(row.carNumber) + '</span>';
			},
			filter: function(q, row) {
//				q = new String(q).toUpperCase().trim();
				var opts = $(this).combobox('options');
				return ((unicodeToEnglish(row[opts.textField]).indexOf(unicodeToEnglish(q)) >= 0) || unicodeToEnglish(row[opts.valueField]).indexOf(unicodeToEnglish(q)) >= 0) ;
			},
			onSelect: function(rec){
	        	if (selectCall != undefined && selectCall != null){
	        		selectCall.call(this, rec);
	        	}
	        },
			onLoadSuccess:function(){
				if (idDefault != undefined && idDefault != null) {
					$(selector).combobox('setValue',idDefault);
				}
			}
		});
	},
	
	/**
	 * @author: trungtm6
	 */
	bindPeriodCbx: function(selector, dataArr, idDefault, cbxWidth, selectCall, loadSuccessCall, value, text) {
		var nameText = 'num';
		var wid = 'auto';
		var valueField = 'num';
		var textField = 'cycleName';
		if (value != undefined && value != null) {
			valueField = value;
		}
		if (text != undefined && text != null) {
			textField = text;
		}
		if (cbxWidth != undefined && cbxWidth != null && cbxWidth > 0) {
			wid = cbxWidth;
		}
		$(selector).combobox({
		    data:dataArr,
		    valueField: valueField,
		    textField: textField,
		    panelWidth: wid,
		    formatter: function(row) {
		    	if (text != undefined && text != null && text == nameText) {
		    		return '<span style="font-weight:bold">' + Utils.XSSEncode(row.num) + '</span>';
		    	}
				return '<span style="font-weight:bold">' + Utils.XSSEncode(row.cycleName) + '</span>';
			},
			filter: function(q, row) {
				var opts = $(this).combobox('options');
				return ((unicodeToEnglish(row[opts.textField]).indexOf(unicodeToEnglish(q)) >= 0) || unicodeToEnglish(row[opts.valueField]).indexOf(unicodeToEnglish(q)) >= 0) ;
			},
			onSelect: function(rec){
	        	if (selectCall != undefined && selectCall != null){
	        		selectCall.call(this, rec);
	        	}
	        },
			onLoadSuccess:function() {
				if (idDefault != undefined && idDefault != null) {
					$(selector).combobox('setValue',idDefault);
				}
				if (loadSuccessCall != undefined && loadSuccessCall != null) {
					loadSuccessCall.call(this);
	        	}
			}
		});
	},
	/**
	 *  khoi tao combobox don vi
	 */
	bindShopCbx: function(selector, dataArr, shopCodeDefault, cbxWidth, selectCall, successCall, valueField, textField) {
		var value = 'shopCode';
		var text = 'shopName';
		var wid = 206;
		if (cbxWidth != undefined && cbxWidth != null && cbxWidth > 0) {
			wid = cbxWidth;
		}
		if (valueField != undefined && valueField != null) {
			value = valueField;
		}
		if (textField != undefined && textField != null) {
			text = textField;
		}
		$(selector).combobox({
			valueField: value,
			textField:  text,
			data: dataArr,
			panelWidth: wid,
			formatter: function(row) {
				return '<span style="font-weight:bold">' + Utils.XSSEncode(row.shopCode) + '</span><br/>' + '<span style="color:#888">' + Utils.XSSEncode(row.shopName) + '</span>';
			},
			filter: function(q, row) {
				q = new String(q).toUpperCase().trim();
				var opts = $(this).combobox('options');
				return ((unicodeToEnglish(row[opts.textField]).indexOf(unicodeToEnglish(q)) == 0) || unicodeToEnglish(row[opts.valueField]).indexOf(unicodeToEnglish(q)) == 0) ;
			},	
	        onSelect: function(rec) {
	        	if (selectCall != undefined && selectCall != null) {
	        		selectCall.call(this, rec);
	        	}
	        },
	        onLoadSuccess: function() {
	        	var arr = $(selector).combobox('getData');
	        	if (arr != null && arr.length > 0) {			        		
	        		if (shopCodeDefault != undefined && shopCodeDefault != null && shopCodeDefault.length > 0) {
        				$(selector).combobox('select', shopCodeDefault);			        				
        			} else if (value == 'shopCode'){
        				$(selector).combobox('select', arr[0].shopCode);
        			} else if (value == 'id'){
        				$(selector).combobox('select', arr[0].id);
        			}
	        	}
	        	if (successCall != undefined && successCall != null) {
	        		successCall.call(this, arr);
				}
	        }
		});
	},
	/**
	 *  load combobox don vi co phan quyen
	 * @author: trietptm
	 */
	loadShopCbx: function(selector, shopCodeDefault, cbxWidth, selectCall, successCall, valueField, textField){
		var params = {};
		$.ajax({
			type : "POST",
			url:'/commons/list-child-shop',
			data :($.param(params, true)),
			dataType : "json",
			success : function(data) {
				Utils.bindShopCbx(selector, data.rows, shopCodeDefault, cbxWidth, selectCall, successCall, valueField, textField);
			}
		});	
	},
	isDateEx:function(txtDate, separator, type) {
	    var aoDate,           // needed for creating array and object
	        ms,               // date in milliseconds
	        isDate,			  // checkDate
	        month, day, year; // (integer) month, day and year
	    	
	    // if separator is not defined then set '/'
	    if (separator == undefined) {
	        separator = '/';
	    }
	    // split input date to month, day and year
	    aoDate = txtDate.split(separator);
	    // array length should be exactly 3 (no more no less)
	   
	    if(type == null || type == undefined){
			type = Utils._DATE_DD_MM_YYYY;
		}
	    switch (type) {
			case Utils._DATE_DD_MM_YYYY:
				if (aoDate.length !== 3) {
					return false;
				}
				 // define month, day and year from array (expected format is
					// m/d/yyyy)
			    // subtraction will cast variables to integer implicitly
			    day = aoDate[0] - 0; // because months in JS start from 0
			    month = aoDate[1] - 1;
			    year = aoDate[2] - 0;
				break;
			case Utils._DATE_MM_YYYY:
				if (aoDate.length !== 2) {
					return false;
				}
				day = 1;
			    month = aoDate[0]-1;
			    year = aoDate[1]-0;
				break;
			default:
				break;
		}
	   
	    // test year range
	    if (year < 1000 || year > 3000) {
	        return false;
	    }
	    // convert input date to milliseconds
	    ms = (new Date(year, month, day)).getTime();
	    // initialize Date() object from milliseconds (reuse aoDate variable)
	    aoDate = new Date();
	    aoDate.setTime(ms);
	    // compare input date and parts from Date() object
	    // if difference exists then input date is not valid
	    if (aoDate.getFullYear() !== year ||
	        aoDate.getMonth() !== month ||
	        aoDate.getDate() !== day) {
	        return false;
	    }
	    // date is OK, return true
	    return true;
	},
	numbersOnly: function(e) {
		var key;
		var keychar;
		if (window.event) {
		   key = window.event.keyCode;
		}else if (e) {
		   key = e.which;
		}else {
		   return true;
		}
		keychar = String.fromCharCode(key);

		if ((key==null) || (key==0) || (key==8) ||  (key==9) || (key==13) || (key==27) ) {
		   return true;
		}else if ((("0123456789").indexOf(keychar) > -1)) {
			return true;
		}else{
		   return false;
		}
	},
	unitNumberOnly: function(e){
		var key;
		var keychar;
		if (window.event) {
		   key = window.event.keyCode;
		}else if (e) {
		   key = e.which;
		}else {
		   return true;
		}
		keychar = String.fromCharCode(key);

		if ((key==null) || (key==0) || (key==8) ||  (key==9) || (key==13) || (key==27) ) {
		   return true;
		}else if ((("0123456789/").indexOf(keychar) > -1)) {
			return true;
		}else{
		   return false;
		}
	},
	// CuongND
	ChangeSelectByValueOrText : function(ddlID, value,Text) {
	    var ddl = document.getElementById(ddlID);
	    for (var i = 0; i < ddl.options.length; i++) {
	    	if (Text=="value"){
	    		if (ddl.options[i].value == value) {
		                ddl.selectedIndex = i;
		            break;
		        }
	    	}else if (Text=="text"){
	    		if (ddl.options[i].text == value) {
		                ddl.selectedIndex = i;
		                break;
		        }
	    	}
	    }
	},
	unitNotKEY35:function(e){
		var key;
		var keychar;
		if (window.event) {
		   key = window.event.keyCode;
		}else if (e) {
		   key = e.which;
		}else {
		   return true;
		}
		keychar = String.fromCharCode(key);

		if ((key==null) || (key==0) || (key==8) ||  (key==9) || (key==13) || (key==27) ) {
		   return true;
		}else if(key==35){
			return false;
		}
		return true;
	},
	unitCharactersAz094Code:function(e){
		var key;
		var keychar;
		if (window.event) {
		   key = window.event.keyCode;
		}else if (e) {
		   key = e.which;
		}else {
		   return true;
		}
		keychar = String.fromCharCode(key);
		if ((key==null) || (key==0) || (key==8) ||  (key==9) || (key==13) || (key==27) ) {
			   return true;
		}		
	    if(!/^[0-9a-zA-Z-_.]+$/.test(keychar)){
			return false;
	   }
	    return true;
	},
	unitCharactersAz094Name:function(e){
		var key;
		var keychar;
		if (window.event) {
		   key = window.event.keyCode;
		}else if (e) {
		   key = e.which;
		}else {
		   return true;
		}
		
		keychar = String.fromCharCode(key);
		if ((key==null) || (key==0) || (key==8) ||  (key==9) || (key==13) || (key==27) ) {
			   return true;
		}		
		if(/^[\\|\/|~]/.test(keychar)){
			  return false;
		} 
		return true;
	},
	unitNumberOnly4Paste: function()
	{
		var ctrlDown = false;
	    var ctrlKey = 17, vKey = 86;
	    
	    $(this).keydown(function(e)
	    {
	        if (e.keyCode == ctrlKey) ctrlDown = true;
	    }).keyup(function(e)
	   	{
	        if (e.keyCode == ctrlKey) ctrlDown = false;
	    });
	    
	    $(this).keydown(function(e)
   	     {
   	        if (ctrlDown)
   	        {
   	        	if (e.keyCode == vKey) return true;
   	        	else return false;
   	        }else
   	        {
				return Utils.numberOnly(e);  	        	
   	        }
   	     });
	},
	integerNumberOnly: function(e){
		var key;
		var keychar;
		if (window.event) {
		   key = window.event.keyCode;
		}else if (e) {
		   key = e.which;
		}else {
		   return true;
		}
		keychar = String.fromCharCode(key);

		if ((key==null) || (key==0) || (key==8) ||  (key==9) || (key==13) || (key==27) ) {
		   return true;
		}else if ((("0123456789-").indexOf(keychar) > -1)) {
			return true;
		}else{
		   return false;
		}
	},
	getStatusValue : function(status){
		if(status == 'DELETED'){
			return -1;
		} else if(status == 'STOPPED'){
			return 0;
		}
		return 1;
	},
	bindAutoCombineKey:function(){
		$('.GeneralMilkInBox .InputTextStyle ').each(function(){
	   		$(this).bind('keyup',function(event){
	   			if(event.ctrlKey && ($.inArray(event.keyCode,keyCombine) > -1)){
	   				$('.LabelStyle u').each(function(){
						if($(this).html() == String.fromCharCode(event.keyCode)){
							var div = $(this).parent().next().attr("id");
							if(div == undefined){
								div = $(this).parent().next().children().attr("id");
							}
							$('#'+div).focus();
							return false;
						}
					});
	   			}
	   		});    
		});
	},
	bindAutoSearch: function(){
		var check = false;
		if (!check){
			$('.easyui-window .InputTextStyle').each(function(){
			    if(!$(this).is(':hidden')){
			    	check=true;			    			    	
			   		$(this).bind('keyup',function(event){
			   			if(event.keyCode == keyCodes.ENTER){		   				
			   				$('.easyui-window .BtnSearchOnDialog').each(function(){	   					
			   					if(!$(this).is(':hidden')){
			   						$(this).click();
			   					}
			   				});
			   			}
			   		});    
			    }	    
			});
		}
		if (!check){
			$('.easyui-dialog .panel .InputTextStyle').each(function(){
			    if(!$(this).is(':hidden')){
			    	check=true;
			   		$(this).bind('keyup',function(event){
			   			if(event.keyCode == keyCodes.ENTER){		   				
			   				$('.easyui-dialog .BtnSearchOnDialog').each(function(){	   					
			   					if(!$(this).is(':hidden')){
			   						$(this).click();
			   					}
			   				});
			   			}
			   		});    
			    }	    
			});
		}
		if (!check){
			$('.ContentSection .InputTextStyle,.SearchSection .InputTextStyle').each(function(){
			    if(!$(this).is(':hidden')){
			   		$(this).bind('keyup',function(event){
			   			if(event.keyCode == keyCodes.ENTER){		   				
			   				if($('#btnSearch')!= null && $('#btnSearch').html()!= null && $('#btnSearch').html().trim().length > 0 && !$('#btnSearch').is(':hidden')){
			   					$('#btnSearch').click();
			   				}
			   				$('.BtnSearch').each(function(){	   					
			   					if(!$(this).is(':hidden')){
			   						$(this).click();
			   					}
			   				});
			   			}
			   		});    
			    }	    
			});
		}		
	},
	bindAutoSearchEx: function(parentDiv){
		$(parentDiv+' .InputTextStyle ').each(function(){
		    if(!$(this).is(':hidden')){
		   		$(this).bind('keyup',function(event){
		   			if(event.keyCode == keyCodes.ENTER){		   				
		   				if($('#btnSearch')!= null && $('#btnSearch').html()!= null && $('#btnSearch').html().trim().length > 0 && !$('#btnSearch').is(':hidden')){
		   					$('#btnSearch').click();
		   				}
		   				$('.BtnSearch').each(function(){	   					
		   					if(!$(this).is(':hidden')){
		   						$(this).click();
		   					}
		   				});
		   			}
		   		});    
		    }	    
		});	
	},	
	bindAutoSearchUpdateBoxSelect: function(){
		$('.BoxSelect2, .BoxSelect3').each(function(){
			if(!$(this).is(':hidden') && !$(this).is(':disabled')){
				$(this).bind('keyup',function(event){
					if(event.keyCode == keyCodes.ENTER){
						if($('#btnUpdate').length > 0 && !$('#btnUpdate').is(':hidden') && !$('btnUpdate').is(':disabled')){
							$('#btnUpdate').click();
						}else{
							if($('#btnSearch').length > 0 && !$('#btnSearch').is(':hidden') && !$('btnSearch').is(':disabled')){
								$('#btnSearch').click();
							}
						}
					}
				});
			}
		});
	},
	bindAutoButtonEx: function(parentDiv, idButton){
		var flag = false;
		$(parentDiv+' .InputTextStyle ').each(function(){
			if(!flag){
				if(!$(this).is(':hidden')){
					$(this).bind('keyup',function(event){
						if(event.keyCode == keyCodes.ENTER){		   				
							if($('#' +idButton)!= null && $('#' +idButton).html()!= null && $('#' +idButton).html().trim().length > 0 && !$('#' +idButton).is(':hidden')){
								$('#' +idButton).click();
								flag = true;
							}
						}
					});    
				}	    
			}
		});	
	},	
	bindQuicklyAutoSearch:function(){
		$('.ContentSection .InputTextStyle ').each(function(){
		    if(!$(this).is(':hidden')){
		   		$(this).bind('keyup',function(event){
		   			if(event.keyCode == keyCodes.ENTER){		   				
		   				if($('#btnSearch')!= null && $('#btnSearch').html()!= null && $('#btnSearch').html().trim().length > 0 && !$('#btnSearch').is(':hidden')){
		   					$('#btnSearch').click();		   					
		   				}		   				
		   			}
		   		});    
		    }	    
		});
	},	
	bindAutoSave: function(){
		$('.GeneralMilkInBox .InputTextStyle ').each(function(){
		    if(!$(this).is(':hidden')){
		   		$(this).bind('keyup',function(event){
		   			if(event.keyCode == keyCodes.ENTER){		   				
		   				if($('#btnSave')!= null && $('#btnSave').html()!= null && $('#btnSave').html().trim().length > 0 && !$('#btnSave').is(':hidden')){
		   					$('#btnSave').click();
		   				}
		   			}
		   		});    
		    }	    
		});
	},
	bindAutoSearchForEasyUI: function(){
		$('.easyui-window .GeneralMilkInBox .InputTextStyle ').each(function(){
		    if(!$(this).is(':hidden')){
		   		$(this).bind('keyup',function(event){
		   			if(event.keyCode == keyCodes.ENTER){		   				
		   				$('.easyui-window .BtnSearchOnDialog').each(function(){	   					
		   					if(!$(this).is(':hidden')){
		   						$(this).click();
		   					}
		   				});
		   			}
		   		});    
		    }	    
		});
	},
	bindAutoSearchPage: function(idDialogTag){
		$(document).bind('keyup',function(event){
			if(event.keyCode == keyCodes.ENTER){		   	
				if($('#' + idDialogTag) == undefined ||($('#' + idDialogTag) != null && $('#' + idDialogTag).is(':hidden'))){
					if($('#btnSearch')!= null && $('#btnSearch').html()!= null && $('#btnSearch').html().trim().length > 0 && !$('#btnSearch').is(':hidden')){
						$('#btnSearch').click();
					}
				}							
			}
		});	
	},
	bindingBanCharacter: function(){
		$('.InputTextStyle').each(function(){
			$(this).bind('keypress', function(e){
				var key;
				var keychar;
				if (window.event) {
				   key = window.event.keyCode;
				}else if (e) {
				   key = e.which;
				}else {
				   return true;
				}
				keychar = String.fromCharCode(key);
				if ((key==null) || (key==0) || (key== keyCodes.BACK_SPACE) ||  (key == keyCodes.TAB) || (key==keyCodes.ENTER) || (key==keyCodes.ESCAPE) ) {
					   return true;
					}else if ((("?#").indexOf(keychar) > -1)) {
						return false;
					}else{
					   return true;
					}
			});
			$(this).bind('paste', function(){
				var id = $(this).attr('id');
				var tmBanChar = setTimeout(function(){
					$('#' + id).val($('#' + id).val().replace(/\?/g,'').replace(/\#/g,''));
					clearTimeout(tmBanChar);
				},200);
			});
		});		
	},
	unbindFormatOnTextfield: function(id){
		$("#"+id).unbind("keyup");
		$("#"+id).unbind("keydown");
		$("#"+id).unbind("paste");
	},
	bindFormatOntextfieldCurrencyFor: function(input,formatType){
		Utils.formatCurrencyFor(input);
		Utils.bindFormatOnTextfield(input,formatType);
	},
	bindFormatOnTextfieldInputCss: function(nameClassCss, formatType,prefix){
		var clazz='';
		if(prefix!=undefined && prefix!=null){
			clazz = prefix + ' '; 
		}
		$(clazz + '.'+nameClassCss).each(function(){
			var objectId = $(this).attr('id');
			if(objectId!=null && objectId!=undefined && objectId.length>0){
				Utils.bindFormatOnTextfield(objectId, formatType);
			}			
		});
	},
	bindFormatCalenderOnTextfieldInputCss: function(nameClassCss){
		$('.'+nameClassCss).each(function(){
			var objectId = $(this).attr('id');
			if(objectId!=null && objectId!=undefined && objectId.length>0){
				applyDateTimePicker("#"+objectId);
			}			
		});
	},
	bindFormatOnTextfield: function(id, formatType,prefix,isCtrlPress){
		var reg = /[^0-9]/;
		var regAll = /[^0-9]/g;
		switch (formatType) {
		case Utils._TF_A_Z:
			reg = /[^A-Z]/;
			regAll = /[^A-Za-z]/g; 
			break;
		case Utils._TF_NUMBER_DOT:
			reg = /[^0-9.]/;
			regAll = /[^0-9.]/g; 
			break;
		case Utils._TF_NUMBER:
			reg = /[^0-9]/;
			regAll = /[^0-9]/g; 
			break;
		case Utils._TF_NUMBER_COMMA:
			reg = /[^0-9,]/;
			regAll = /[^0-9,]/g; 
			break;
		case Utils._TF_NUMBER_SIGN:
			reg = /[^0-9-]/;
			regAll = /[^0-9-]/g; 
			break;
		case Utils._TF_NUMBER_CONVFACT:
			reg = /[^0-9\/]/;
			regAll = /[^0-9\/]/g; 
			break;
		case Utils._TF_NUMBER_COMMA_AND_DOT:
			reg = /[^0-9.,]/;
			regAll = /[^0-9.,]/g; 
			break;
		case Utils._TF_PHONE_NUMBER:
			reg = /[^0-9-.() ]/;
			regAll = /[^0-9-.() ]/g; 
			break;
		case Utils._TF_PHONE_NUMBER_SPECIAL:
			reg = /[^0-9-.() ^<|>|?|\\|\'|\"|&|~#|$|%|@|*|(|)|^|`|, ]/;
			regAll = /[^0-9-.() ^<|>|?|\\|\'|\"|&|~#|$|%|@|*|(|)|^|`|, ]/g; 
			break;	
		case Utils._CODE:
			reg = /[^0-9a-zA-Z_]/;
			regAll = /[^0-9a-zA-Z_]/g; 
			break;
		default:
			break;
		}
		var prefixTmp ='';
		if(prefix != undefined && prefix!= null && prefix != ''){
			prefixTmp = prefix;
		}
		$(prefixTmp+'#' + id).bind('keyup', function(e){
			var code;
			if (!e) var e = window.event;
			if (e.keyCode) code = e.keyCode;
			else if (e.which) code = e.which;
			if(code == keyCodes.CTRL){
				Utils._CTRL_PRESS = false;
			}
			if(code == keyCodes.SHIFT) {
				Utils._SHIFT_PRESS = false;
			}
		});
		$(prefixTmp +'#' + id).bind('keydown', function(e){
			if(formatType == Utils._TF_NUMBER || formatType == Utils._TF_NUMBER_DOT){
				if(Utils._SHIFT_PRESS){
					return false;
				}
			}
			var code;
			if (!e) var e = window.event;
			if (e.keyCode) code = e.keyCode;
			else if (e.which) code = e.which;
			var character =(code == null || code == undefined)? fromKeyCode(32).split(' ')[0]:fromKeyCode(code).split(' ')[0];	
			if (isCtrlPress != undefined && isCtrlPress) {
				if ((code >=96 && code <= 105) || (code>=48 && code<=57) || code==null || code==0 || code== keyCodes.BACK_SPACE || 
						code == keyCodes.TAB || code==keyCodes.ENTER || code==keyCodes.ESCAPE || code == keyCodes.DELETE ||
						(Utils._SHIFT_PRESS && code == keyCodes.HOME) || code == keyCodes.SHIFT || code == keyCodes.HOME || code == keyCodes.END ||
						code==keyCodes.CTRL || code == keyCodes.ARROW_LEFT || code == keyCodes.ARROW_RIGHT || code == keyCodes.ARROW_UP || code == keyCodes.ARROW_DOWN ||
						(Utils._CTRL_PRESS && (character  == 'v' || character  == 'V'))
						|| (e.ctrlKey && e.altKey)){
					if(code == keyCodes.CTRL){
						Utils._CTRL_PRESS = true;
					}
					if(code == keyCodes.SHIFT) {
						Utils._SHIFT_PRESS = true;
					}
					return true;
				} else if (reg.test(character) || (Utils._SHIFT_PRESS && !/[^0-9]/.test(character))) {
					return false;
				}else{
					return true;
				}
			} else {
				if ((code >=96 && code <= 105) || (code>=48 && code<=57) || code==null || code==0 || code== keyCodes.BACK_SPACE || 
						code == keyCodes.TAB || code==keyCodes.ENTER || code==keyCodes.ESCAPE || code == keyCodes.DELETE ||
						(Utils._SHIFT_PRESS && code == keyCodes.HOME) || code == keyCodes.SHIFT || code == keyCodes.HOME || code == keyCodes.END ||
						code==keyCodes.CTRL || code == keyCodes.ARROW_LEFT || code == keyCodes.ARROW_RIGHT || code == keyCodes.ARROW_UP || code == keyCodes.ARROW_DOWN ||
						(Utils._CTRL_PRESS && (character  == 'v' || character  == 'V'))){
					if(code == keyCodes.CTRL){
						Utils._CTRL_PRESS = true;
					}
					if(code == keyCodes.SHIFT) {
						Utils._SHIFT_PRESS = true;
					}
					return true;
				} else if (reg.test(character) || (Utils._SHIFT_PRESS && !/[^0-9]/.test(character))) {
					return false;
				}else{
					return true;
				}
			}
			
		});
		$(prefixTmp+ '#' + id).bind('paste', function(){			
			var tmAZ = setTimeout(function(){
				$(prefixTmp+ '#' + id).val($(prefixTmp+ '#' + id).val().replace(regAll,''));
				clearTimeout(tmAZ);
			},200);
		});
	},
	formatCurrencyFor: function(idInput) {
		$('#'+idInput).bind('keyup', function(e) {
			var valMoneyInput = $('#'+idInput).val();
			valMoneyInput = Utils.returnMoneyValue(valMoneyInput);
			if(isNaN(valMoneyInput) || valMoneyInput == "" || valMoneyInput == '') {
				return false;
			}
			var _valMoneyInput = formatCurrency(valMoneyInput);
			$('#'+idInput).val(_valMoneyInput);
		});
		$('#'+idInput).bind('paste', function(){
		    var tm = setTimeout(function(){
		    	var valMoneyInput = $('#'+idInput).val();
		        valMoneyInput = Utils.returnMoneyValue(valMoneyInput);
		        if(isNaN(valMoneyInput) || valMoneyInput == "" || valMoneyInput == '') {
		        	return false;
		        }
		        var _valMoneyInput = formatCurrency(valMoneyInput);
		        $('#'+idInput).val(_valMoneyInput); 
		        	clearTimeout(tm);
		    },200);
		});
	},
	bindTRbackground:function(divT){		
		$('.GeneralTable tr').each(function(){
			$(this).bind('click',function(){				
				$('.GeneralTable tr').each(function(){
					$(this).css('background-color','');
				});
				$(this).css('background-color','#FFFF66');
			});
		});
	},
	formatCurrencyForInterger: function(idInput) {
		$('#'+idInput).bind('keyup', function(e) {
			var valMoneyInput = $('#'+idInput).val();
			valMoneyInput = Utils.returnMoneyValue(valMoneyInput);
			if(isNaN(valMoneyInput) || valMoneyInput == "" || valMoneyInput == '') {
				return false;
			}
			var _valMoneyInput = formatCurrencyInterger(Number(valMoneyInput));
			$('#'+idInput).val(_valMoneyInput);
		});
		$('#'+idInput).bind('paste', function(){
		    var tm = setTimeout(function(){
		    	var valMoneyInput = $('#'+idInput).val();
		        valMoneyInput = Utils.returnMoneyValue(valMoneyInput);
		        if(isNaN(valMoneyInput) || valMoneyInput == "" || valMoneyInput == '') {
		        	return false;
		        }
		        var _valMoneyInput = formatCurrencyInterger(Number(valMoneyInput));
		        $('#'+idInput).val(_valMoneyInput); 
		        	clearTimeout(tm);
		    },200);
		});
	},
	returnMoneyValue: function(valMoney) {
		var _valMoney = valMoney + '';
		var value = _valMoney.split(',').join('');
		var i=0;
		for(i=0;i<value.length-1;i++){// Nếu tất cả từ 0 -> length-2 là 0 thì
										// lấy số cuối
			if(value[i]!='0'){
				break;
			}
		}
		return value.substring(i,value.length);
	},
	applyLiveUpdateImage:function (obj) {
		$(obj).addClass('liveUpdateImage');
		return false;
	},
	liveUpdateImage:function (object,mediaType,mediaItemId){
		$('#mediaItemId').val(mediaItemId);
		if(mediaType == 0){
			$('#player').html('').hide();
			$('#imageBox').show();			
			var orgSrc = object.attr('data');
			var source = imgServerPath +orgSrc;
			$('#imageBox').html('<img width="715" height="551" alt="" src="'+source+'" />');
			$('#imageBox').append('<a class="Sprite1 HideText DeleteLinkStyle" onclick="return ProductCatalog.deleteMediaItem();" href="javascript:void(0)">Xóa</a>');
		}else{
			$('#player').show();
			$('#imageBox').hide();
			var orgSrc = object.attr('data');
			var source = imgServerPath +orgSrc;
			if(orgSrc.substring(orgSrc.length - 3,orgSrc.length) == 'avi'){
				$('#player').html('<a class="media {width:715, height:551}" href="'+source+'"></a>');
				$('a.media').media();
			}else{
				flowplayer("player", "/resources/scripts/plugins/flowplayer/flowplayer-3.2.16.swf",{
					plugins: {
						audio: {
							url: '/resources/scripts/plugins/flowplayer/audio/flowplayer.audio-3.2.10.swf'
						}
					},
					clip: {
						url: source,
						autoPlay: true,
						autoBuffering: true
					},
					play: {
						replayLabel: 'Xem lại'
					}
				});			
			}
		}
	},
	liveUpdateImage:function (object,mediaType,mediaItemId){
		$('#mediaItemId').val(mediaItemId);
		if(mediaType == 0){
			$('#player').html('').hide();
			$('#imageBox').show();			
			var orgSrc = object.attr('data');
			var source = imgServerPath +orgSrc;
			$('#imageBox').html('<img width="715" height="551" alt="" src="'+source+'" />');
			$('#imageBox').append('<a class="Sprite1 HideText DeleteLinkStyle" onclick="return ProductCatalog.deleteMediaItem();" href="javascript:void(0)">Xóa</a>');
		}else{
			$('#player').show();
			$('#imageBox').hide();
			var orgSrc = object.attr('data');
			var source = imgServerPath +orgSrc;
			if(orgSrc.substring(orgSrc.length - 3,orgSrc.length) == 'avi'){
				$('#player').html('<a class="media {width:715, height:551}" href="'+source+'"></a>');
				$('a.media').media();
			}else{
				flowplayer("player", "/resources/scripts/plugins/flowplayer/flowplayer-3.2.16.swf",{
					plugins: {
						audio: {
							url: '/resources/scripts/plugins/flowplayer/audio/flowplayer.audio-3.2.10.swf'
						}
					},
					clip: {
						url: source,
						autoPlay: true,
						autoBuffering: true
					},
					play: {
						replayLabel: 'Xem lại'
					}
				});			
			}
		}
	},
	liveUpdateImageForProduct:function (object,mediaType,mediaItemId,checkPermission){
		$('#mediaItemId').val(mediaItemId);
		if(mediaType == 0){
			$('#player').html('').hide();
			$('#imageBox').show();			
			var orgSrc = object.attr('data');
			var source = imgServerProdcutPath +orgSrc;
			$('#imageBox').html('<img width="715" height="551" alt="" src="'+source+'" />');
			if (checkPermission == 1) {
				$('#imageBox').append('<a class="Sprite1 HideText DeleteLinkStyle" onclick="return ProductCatalog.deleteMediaItem();" href="javascript:void(0)">Xóa</a>');
			}
		}else{
			$('#player').show();
			$('#imageBox').hide();
			var orgSrc = object.attr('data');
			var source = imgServerProdcutPath +orgSrc;
			if(orgSrc.substring(orgSrc.length - 3,orgSrc.length) == 'avi'){
				$('#player').html('<a class="media {width:715, height:551}" href="'+source+'"></a>');
				$('a.media').media();
			}else{
				flowplayer("player", "/resources/scripts/plugins/flowplayer/flowplayer-3.2.16.swf",{
					plugins: {
						audio: {
							url: '/resources/scripts/plugins/flowplayer/audio/flowplayer.audio-3.2.10.swf'
						}
					},
					clip: {
						url: source,
						autoPlay: true,
						autoBuffering: true
					},
					play: {
						replayLabel: 'Xem lại'
					}
				});			
			}
		}
	},
	updateImageStatus:function (obj, timestamp){
		if(obj != null){
			obj.removeClass('liveUpdateImage');
			var orgSrc = obj.attr('data');
			if(orgSrc.indexOf("?") > -1){
				orgSrc =orgSrc.substring(0,orgSrc.indexOf("?"));
			}
			// obj.attr('src',imgServerPath +orgSrc + '?' + timestamp);
			$('#imageBox').attr('src',imgServerPath +orgSrc + '?' + timestamp);
		}
	},
	comboboxChange:function abc(id,title){
		$('#'+id).next().children().first().html(title);
		$('#'+id).children().first().html(title);
	},
	getChangedForm: function(divHidden,divSaleType, parentId,name,isMap){
		$('#errMsg').hide();
		$('.RequireStyle').show();
		$('#btnSearch,#btnCreate').hide();
		$('#btnUpdate,#btnDismiss').show();
		Utils.comboboxChange('label','Chọn hiệu xe');
		Utils.comboboxChange('type','Chọn loại xe');
		Utils.comboboxChange('category','Chọn chủng loại');
		Utils.comboboxChange('origin','Chọn nguồn gốc');
		if(divHidden != null && divHidden == true){
			$('.divBankHidden').show();
			$('.divHidden').hide();
		}
		$('span#errIU').html('*');
		$('#titleCarId').html('Thông tin xe');
		$('#title').html('Thông tin thêm mới');
		if(divSaleType != null && divSaleType == true){
			$('#staffTypeCode').val('');
			$('#staffTypeName').val('');			
			// $('#parentCode').combotree('setValue', parentId);
		}
		var isCheck = false;
		$('.InputTextStyle').each(function(){
			if (!$(this).is(':hidden') && !$(this).is(':disabled') && !isCheck){
				$(this).focus();
				isCheck = true;
			}
		});	
		
		$('.InputTextStyle').each(function(){
			$(this).unbind();
		});
		
		$('.InputTextStyle').each(function(){
			if(!$(this).is(':hidden') && !$(this).is(':disabled')){
				$(this).bind('keyup',function(event){
					if(event.keyCode == keyCodes.ENTER){
						if(!$('#btnUpdate').is(':hidden') && !$('btnUpdate').is(':disabled')){
							$('#btnUpdate').click();
						}
					}
				});
			}
		});
		
		if(isMap!=null && isMap!=undefined && isMap==true){			         	  
			MapUtil.loadMapResource(function(){
				Vietbando.loadMap('mapContainer',Vietbando._DEF_LAT,Vietbando._DEF_LNG,Vietbando._DEF_ZOOM, function(pt){					
					$('#lat').val(pt.latitude);
					$('#lng').val(pt.longitude);										
				}, false);	
			});			  
		}		

	},
	onCheckAllChange: function(checkAll, className) {
		if(checkAll) {
			$('.' + className).each(function() {
				if(this.disabled == true) {
					
				} else {
					this.checked = true;
				}
			});
		} else {
			$('.'+className).each(function() {
				if(this.disabled == true) {
					
				} else {
					this.checked = false;
				}
			});
		}
	},
	onCheckboxChangeForCheckAll: function(isCheck, className, checkAllId) {
		if(isCheck == null || isCheck == undefined) {
			return;
		} 
		if(isCheck) {
			// kiem tra xem co phai check all ko?
			var checkAll = true;
			$('.' + className).each(function() {
				if(this.disabled == true) {
					
				} else {
					if(this.checked == false) {
						checkAll = false;
					}
				}
			});
			if(checkAll == true) {
				$('#' + checkAllId).attr('checked', 'checked');
			}
		} else {
			// bo check All
			$('#' + checkAllId).removeAttr('checked');
		}
	},
	getCheckboxStateOfTree: function(){
		if(Utils._arr_Chk_State == null){
			Utils._arr_Chk_State = new Array();			
		}
		$('.jstree-leaf').each(function(){
			if($(this).hasClass('jstree-checked')){
				Utils._arr_Chk_State.push($(this).attr('id'));
			}			
		});
	},
	applyCheckboxStateOfTree: function(){
		if(Utils._arr_Chk_State!= null && Utils._arr_Chk_State.length > 0){
			for(var i=0;i<Utils._arr_Chk_State.length;i++){
				if($('#' + Utils._arr_Chk_State[i]).html()!= null && $('#' + Utils._arr_Chk_State[i]).html().trim().length > 0){
					$('#' + Utils._arr_Chk_State[i] + ' a ins').click();
				}
			}
			Utils._arr_Chk_State == new Array();
		}
	},
	resetCheckboxStateOfTree: function(){
		Utils._arr_Chk_State == new Array();
	},
	removeItemFromArray: function(array, item) {
		return $.grep(array, function(value) {
			return value != item;
		});
	},
	resizeHeightAuto:function(obj){
		if($(obj+' tbody').height() < 250){
			$('.ScrollBodySection').css({'height':'auto'});
		}else{
			$('.ScrollBodySection').css({'height':'250px'});
			$('.ScrollBodySection').jScrollPane();
		}		
	},
	resizeHeightAutoScrollCross:function(obj){
		if($('#'+ obj+' tbody').height() < 250){
			$('#'+ obj).css({'height':'auto'});
			if($('#'+ obj).data('jsp') != undefined || $('#'+ obj).data('jsp') != null) {
				$('#'+ obj).data('jsp').destroy();
			}
		}else{
			$('#'+ obj).css({'height':'250px'});
			$('#'+ obj).jScrollPane();
		}		
	},
	resizeHeightAutoScrollVertical:function(obj){
		$('#'+ obj).css({'height':'auto'});
		$('#'+ obj).jScrollPane();
	},
	afterViewImportExcel: function(responseText, statusText, xhr, $form){
		$('#divOverlay').removeClass('Overlay');
		$('#imgOverlay').hide();
		if (statusText == 'success') {				
	    	$("#responseDiv").html(responseText);		   
	    	if($('#errorExcel').html().trim() == 'true' || $('#errorExcelMsg').html().trim().length > 0){
	    		$('#errExcelMsg').html($('#errorExcelMsg').html()).show();
	    	} else {
	    		if($('#typeView').html().trim() == 'false'){
	    			var totalRow = parseInt($('#totalRow').html().trim());
	    			var numFail = parseInt($('#numFail').html().trim());
	    			var fileNameFail = $('#fileNameFail').html();
	    			var mes = format(msgErr_result_import_excel,(totalRow - numFail),numFail);		    		
	    			if(numFail > 0){
	    				mes+= ' <a href="'+ fileNameFail +'">' + msgCommon4 + '</a>';
	    			}
	    			$('#errExcelMsg').html(mes).show();
	    			$("#grid").trigger("reloadGrid");
	    			if(ProductLevelCatalog._callBackAfterImport != null){
		    			ProductLevelCatalog._callBackAfterImport.call(this);
		    		}
	    		}else{
	    			var html = $('#popup1').html();
	    			$.fancybox(html,
    					{
    						modal: true,
    						title: 'Danh sách mức từ excel',
    						afterShow: function(){
    							$('#popup1').html('');
    						},
    						afterClose: function(){
    							$('#popup1').html(html);
    						}
    					}
    				);
	    		}
	    	}
	    }	
	},
	validateAttributeData:function(dataModel,errMsgId,parentDiv){
		var parentId = '';
		if(parentDiv!= null && parentDiv!= undefined){
			parentId = parentDiv;
		}
		var msg = '';
		$(errMsgId).html('').hide();
		if($(parentId + '#lstAttributeSize').val() > 0){
			var flag = true;
			var lstAttributeId = new Array();
			var lstAttributeValue = new Array();
			var lstAttributeColumnType = new Array();
			var lstAttributeColumnValueType = new Array();
			for(var i = 1;i <= $(parentId +'#lstAttributeSize').val();i++){				
				
				var textSelector = 'attributeCode_'+i;
				var selector = $(parentId + '#' + textSelector);
				var idSelector = $(parentId + '#attributeId_'+i);
				var selectorColType = $(parentId + '#attributeColumnValueType_'+i).val();

				selector.focus();
				// Kiem tra bat buoc nhap
				var require = selector.attr('require');
				if((!isNullOrEmpty(require) && require != '0') &&  selectorColType != Utils._MULTI_CHOICE){															
					msg = Utils.getMessageOfRequireCheck(textSelector,selector.attr('attr-name'),selector.hasClass('easyui-combobox'));
					if(msg.length > 0){
						selector.focus();
						$(errMsgId).html(msg).show();
						return false;
					}					
				}
				if((selector.hasClass('easyui-combobox') && selector.combobox('getValue') != null && selector.combobox('getValue') != '' && selector.combobox('getValue').length > 0) 
					|| (selector.hasClass('MultipeSelectBox'))
					|| (selector.val() != null 	&& selector.val() != '' && selector.val().length > 0)){

					if(selectorColType == Utils._NUMBER_TYPE){
						msg = Utils.getMessageOfInvalidMaxLength(textSelector, 'Kiểu số');
						if(msg.length > 0){ 
							$(errMsgId).html(msg).show();
							return false;
						}
						if(selector.val() != undefined) {
							var valMoneyInput = Utils.returnMoneyValue(selector.val().trim());
							var maxValue = selector.attr('maxValue');
							var minValue = selector.attr('minValue');
							if(!isNullOrEmpty(maxValue) && Number(maxValue)< Number(valMoneyInput)){
								$(errMsgId).html(format(qltt_kiem_tra_maxvalue,selector.attr('attr-name'),maxValue)).show();
								return false;
							}
							if(!isNullOrEmpty(minValue) && Number(minValue)> Number(valMoneyInput)){
								$(errMsgId).html(format(qltt_kiem_tra_minvalue,selector.attr('attr-name'),minValue)).show();
								return false;
							}
							lstAttributeValue.push(Number(valMoneyInput));							
						} else {
							lstAttributeValue.push('');
						}						

					} else if(selectorColType == Utils._STRING_TYPE){
						msg = Utils.getMessageOfInvalidMaxLength(textSelector, 'Kiểu ký tự');
						if (msg.length == 0) {
							msg = Utils.getMessageOfSpecialCharactersValidate(textSelector,'Kiểu ký tự',Utils._NAME,parentId);
						}
						if(msg.length > 0){
							$(errMsgId).html(msg).show();							
							return false;
						}
						if(selector.val() != undefined) {
							lstAttributeValue.push(selector.val().trim());							
						} else {
							lstAttributeValue.push('');							
						}						
					
					}else if(selectorColType == Utils._LOCATION){
						if(selector.val() != undefined) {
							lstAttributeValue.push(selector.val().trim());							
						} else {
							lstAttributeValue.push('');							
						}						
					
					} else if(selectorColType == Utils._DATE_TYPE) {
						var date = selector.val().trim();
						if(date != '__/__/____' && !Utils.isDate(selector.val().trim())){
							msg = 'Định dạng ngày không hợp lệ, vui lòng nhập theo định dạng dd/MM/yyyy';
						}

						if(msg.length > 0){
							$(errMsgId).html(msg).show();
							return false;
						}
						if(selector.val() != undefined) {
							lstAttributeValue.push(date != '__/__/____' ? date : '');							
						} else {
							lstAttributeValue.push('');							
						}												

					} else if(selectorColType == Utils._MULTI_CHOICE) {						
						var arrValue = selector.val();
						if(arrValue != null && arrValue != undefined && $.isArray(arrValue) && arrValue.length > 0) {
							var strValue = "";
							for(var a = 0; a < arrValue.length; a++) {
								var __arrValue = arrValue[a];
								if(a == 0) {
									strValue = __arrValue;
								} else {
									strValue += ";" + __arrValue;
								}
							}
							lstAttributeValue.push(strValue);														
						} else {			
							if(!isNullOrEmpty(selector.attr('require')) && require != '0'){
								selector.focus();
								$(errMsgId).html(format(msgErr_required_field,selector.attr('attr-name'))).show();
								return false;
							}
							lstAttributeValue.push("");							
						}						
					} else {
						if(selector.combobox('getValue').trim() != '-1' && selector.combobox('getValue').trim() != -1) {							
							lstAttributeValue.push(selector.combobox('getValue').trim());							
						} else {
							lstAttributeValue.push('');							
						}						
					}
				} else {
					lstAttributeValue.push('');					
				}
				lstAttributeId.push(idSelector.val());
				lstAttributeColumnValueType.push(selectorColType.trim());
			}
			dataModel.lstAttributeId = lstAttributeId;
			dataModel.lstAttributeValue = lstAttributeValue;				
			dataModel.lstAttributeColumnValueType = lstAttributeColumnValueType;
			return true;
		}else{
			return true;
		}
	},
	isMonth:function(txtDate, separator) {
	    var aoDate,           // needed for creating array and object
	        ms,               // date in milliseconds
	        month, day, year; // (integer) month, day and year
	    // if separator is not defined then set '/'
	    if (separator == undefined) {
	        separator = '/';
	    }
	    // split input date to month, day and year
	    aoDate = txtDate.split(separator);
	    // array length should be exactly 3 (no more no less)
	    if (aoDate.length !== 2) {
	        return false;
	    }
	    // define month, day and year from array (expected format is m/d/yyyy)
	    // subtraction will cast variables to integer implicitly
	    day = '01'; // because months in JS start from 0
	    month = aoDate[0] - 1;
	    year = aoDate[1] - 0;
	    // test year range
	    if (year < 1000 || year > 3000) {
	        return false;
	    }
	    // convert input date to milliseconds
	    ms = (new Date(year, month, day)).getTime();
	    // initialize Date() object from milliseconds (reuse aoDate variable)
	    aoDate = new Date();
	    aoDate.setTime(ms);
	    // compare input date and parts from Date() object
	    // if difference exists then input date is not valid
	    if (aoDate.getFullYear() !== year ||
	        aoDate.getMonth() !== month) {
	        return false;
	    }
	    // date is OK, return true
	    return true;
	},
	loadAttributeData:function(parentDiv, parentDivCss){
		var parentId = '';
		if(parentDiv!= undefined && parentDiv!= null &&  parentDiv.length > 0){
			parentId = parentDiv;
		}
		for(var i = 1;i<=$(parentId + '#lstAttributeSize').val();i++){
			var columnValueType = $(parentId +'#attributeColumnValueType_'+i).val();
			var columType = $(parentId +'#attributeColumnType_'+i).val();
			if(columnValueType != null && columnValueType == 1){
				if(columType != null && columType == 1){
					Utils.bindFormatOnTextfield('attributeCode_'+i, Utils._TF_NUMBER_DOT, parentId);
				} else if(columType != null && columType == 2){
					
				} else{
					setDateTimePicker('attributeCode_'+i,parentId);
				}
			} else{
				$(parentId +'#attributeCode_'+i).customStyle();
				setSelectBoxValue('attributeCode_'+i, $(parentId + '#objectValue_'+i).val(),parentId);
			}
			$(parentId +'#attributeCode_'+i).val($(parentId + '#objectValue_'+i).val());
		}
		if(parentDivCss!= undefined && parentDivCss!= null && parentDivCss.length > 0){
			$(parentId+ '#parentDivCss').addClass(parentDivCss);
		}
		return false;
	},
	importExcel:function(){
		$('#isView').val(0);
		$('#importFrm').submit();
		return false;
	},
	viewExcel:function(){
		$('#isView').val(1);
		$('#importFrm').submit();
		return false;
	},
	loadComboShopTree: function(controlId, storeCode){
		var _url='/rest/catalog/shop/combotree/1/0.json';
		$('#' + controlId).combotree({url: _url});
		var t = $('#'+ controlId).combotree('tree');	
		t.tree('options').url = '';
		t.tree({			
			formatter: function(node) {
				return Utils.XSSEncode(node.text);
			},
			onSelect:function(node){			
				$('#'+storeCode).val(node.id);	
			},	            				
			onBeforeExpand:function(result){	            					
				var nodes = t.tree('getChildren',result.target);
				for(var i=0;i<nodes.length;++i){
					t.tree('remove',nodes[i].target);
				}
				$.ajax({
					type : "POST",
					url : '/rest/catalog/shop/combotree/2/'+ result.id +'.json',					
					dataType : "json",
					success : function(r) {						
						$(r).each(function(i,e){
							e.text = Utils.XSSEncode(e.text);
						});
						t.tree('append',{
							parent:result.target,
							data:r
						});
						$('.panel-body-noheader ul').css('display','block');
					}
				});
			},
			onExpand: function(result){	
				$('.panel-body-noheader ul').css('display','block');			
		}});
		$('#' + controlId).combotree('setValue',activeType.ALL);
	},
	formatBreakLine:function(){
		var size = $('#sizeList').val();
		if(size != ''){
			for(var i= 0;i<size;i++){
				var num = $('.BreakLine_' + i).html();
				if(num!=null && num!=undefined){
					num = $('.BreakLine_' + i).html().split('.').length;
					for(var j = 0;j<num;j++){
						if($('.BreakLine_' + i).html().length > 20){
							$('.BreakLine_' + i).html($('.BreakLine_' + i).html().replace('.','</br>'));
						}
					}
				}
				
			}
		}
	},
	formatBreakLineEx:function(){
		$('.BreakLine').each(function(){
			$(this).html($(this).text().replace(/\&newline;/g,'</br>'));
		});
	},
	getMessageOfSpecialCharactersValidateEx1: function(objectId, objectName,type){
		var value = $(objectId).val().trim();
		if(type == null || type == undefined){
			type = Utils._NAME;
		}
		var errMsg = '';
		if(value.length > 0){
			switch (type) {
			case Utils._CODE:
				if(!/^[0-9a-zA-Z-_.]+$/.test(value)){
					errMsg = format(msgErr_invalid_format_code,objectName);
				}
				break;
			case Utils._NAME:
				if(!/^[^<|>|?|\\|\'|\"|&|~#|$|%|@|*|(|)|^|`]+$/.test(value)){
					errMsg = format(msgErr_invalid_format_name,objectName);
				}
				break;
			case Utils._ADDRESS:
				if(!/^[^<|>|?|\\|\'|\"|&|~]+$/.test(value)){
					errMsg = format(msgErr_invalid_format_address,objectName);
				}
				break;
			case Utils._SPECIAL:
				if (!/^[^<|>|?|\\|\'|\"|&|~]+$/.test(value)) {
					errMsg = format(msgErr_invalid_format_special, objectName);
				}
				break;
			case Utils._PAYROLL:
				value = value.substring(3);
				if(!/^[0-9a-zA-Z]+$/.test(value)){
					errMsg = format(msgErr_invalid_format_payroll,objectName);
				}
				break;
			case Utils._TF_NUMBER_COMMA:
				if(!/^[0-9a-zA-Z_]+$/.test(value)){
					errMsg = format(msgErr_invalid_format_tf_id_number,objectName);
				}
				break;
			case Utils._NAME_CUSTYPE:
				if(!/^[^<|>|?|\\|\'|\"|&|~#|$|%|@|*|^|`]+$/.test(value)){
					errMsg = format(msgErr_invalid_format_name,objectName);
				}
				break;
			default:
				break;
			}
		}	
		if(errMsg.length !=0){
			$(objectId).focus();
		}
		return errMsg;
	},
	updateRownumWidthForJqGridEX1:function(parentId,colReduceWidth,arraySize,dialog){
		var pId = '';
		if(parentId!= null && parentId!= undefined){
			pId = parentId + ' ';
		}
		var lastValue=$(pId + '.datagrid-cell-rownumber').last().text().trim().length;
		var widthSTT = $('.datagrid-cell-rownumber').width();
		var s = $('.datagrid-header-row td[field='+colReduceWidth+'] div').width();
		if(dialog != null && dialog != undefined){
			widthSTT = $('.easyui-dialog .datagrid-cell-rownumber').width();
			s = $('.easyui-dialog .datagrid-header-row td[field='+colReduceWidth+'] div').width();
		}
		if(arraySize != null && arraySize != undefined){ 
			s = arraySize[0];
		}
		if(lastValue > 0){
			var extWidth = 25;
			if(lastValue > 2){
				extWidth += (lastValue - 2) * 9;
			}
			var value = extWidth - widthSTT;
			s = s - value;
			$(pId + '.datagrid-cell-rownumber').css('width',extWidth);
			$(pId + '.datagrid-header-rownumber').css('width',extWidth);
			if(parentId!= null && parentId!= undefined){
				$(parentId + ' .datagrid-row').each(function(){
					$(parentId + ' .datagrid-header-row td[field='+colReduceWidth+'] div').width(s);
					$(parentId + ' .datagrid-row td[field='+colReduceWidth+'] div').width(s);
				});
			}else{
				$('.datagrid-row').each(function(){
					$('.datagrid-header-row td[field='+colReduceWidth+'] div').width(s-1);
					$('.datagrid-row td[field='+colReduceWidth+'] div').width(s-1);
				});
			}
		}
	},
	bindFormatOnTextfieldEx1: function(id, formatType,prefix,errMsg,msg){
		var reg = /[^0-9]/;
		var regAll = /[^0-9]/g;
		switch (formatType) {
		case Utils._TF_A_Z:
			reg = /[^A-Z]/;
			regAll = /[^A-Za-z]/g; 
			break;
		case Utils._TF_NUMBER_DOT:
			reg = /[^0-9.]/;
			regAll = /[^0-9.]/g; 
			break;
		case Utils._TF_NUMBER:
			reg = /[^0-9]/;
			regAll = /[^0-9]/g; 
			break;
		case Utils._TF_NUMBER_COMMA:
			reg = /[^0-9,]/;
			regAll = /[^0-9,]/g; 
			break;
		case Utils._TF_NUMBER_SIGN:
			reg = /[^0-9-]/;
			regAll = /[^0-9-]/g; 
			break;
		case Utils._TF_NUMBER_CONVFACT:
			reg = /[^0-9\/]/;
			regAll = /[^0-9\/]/g; 
			break;
		case Utils._TF_NUMBER_COMMA_AND_DOT:
			reg = /[^0-9.,]/;
			regAll = /[^0-9.,]/g; 
			break;
		default:
			break;
		}
		var prefixTmp ='';
		if(prefix != undefined && prefix!= null && prefix != ''){
			prefixTmp = prefix;
		}
		$(prefixTmp+'#' + id).bind('keyup', function(e){
			var code;
			if (!e) var e = window.event;
			if (e.keyCode) code = e.keyCode;
			else if (e.which) code = e.which;
			if(code == keyCodes.CTRL){
				Utils._CTRL_PRESS = false;
			}
			if(code == keyCodes.SHIFT) {
				Utils._SHIFT_PRESS = false;
			}
		});
		$(prefixTmp +'#' + id).bind('keydown', function(e){
			var code;
			if (!e) var e = window.event;
			if (e.keyCode) code = e.keyCode;
			else if (e.which) code = e.which;
			if(code == null || code == undefined){
				$(errMsg).html(msg).show();
				//return false;
			}
			var character =(code == null || code == undefined)? fromKeyCode(32).split(' ')[0]:fromKeyCode(code).split(' ')[0];			
			if ((code >=96 && code <= 105) || (code>=48 && code<=57) || code==null || code==0 || code== keyCodes.BACK_SPACE || 
					code == keyCodes.TAB || code==keyCodes.ENTER || code==keyCodes.ESCAPE || code == keyCodes.DELETE ||
					(Utils._SHIFT_PRESS && code == keyCodes.HOME) || code == keyCodes.SHIFT || code == keyCodes.HOME || code == keyCodes.END ||
					code==keyCodes.CTRL || code == keyCodes.ARROW_LEFT || code == keyCodes.ARROW_RIGHT || code == keyCodes.ARROW_UP || code == keyCodes.ARROW_DOWN ||
					(Utils._CTRL_PRESS && (character  == 'v' || character  == 'V'))){
//				var s = $(errMsg).html();
//				if(s.length >0){
//					return false;
//				}
				if(code == keyCodes.CTRL){
					Utils._CTRL_PRESS = true;
				}
				if(code == keyCodes.SHIFT) {
					Utils._SHIFT_PRESS = true;
				}
				return true;
			} else if (reg.test(character) || (Utils._SHIFT_PRESS && !/[^0-9]/.test(character))) {
				return false;
			}else{
				return true;
			}
		});
		$(prefixTmp+ '#' + id).bind('paste', function(){			
			var tmAZ = setTimeout(function(){
				$(prefixTmp+ '#' + id).val($(prefixTmp+ '#' + id).val().replace(regAll,''));
				clearTimeout(tmAZ);
			},200);
		});
	},
	/**
	 * check if quantity-iniput is valid
	 * @author tuannd20
	 * @param  {String}  inputStr quantity input string
	 * @return {Boolean}          true: valid, false: invalid
	 * @date 17/10/2014
	 */
	isValidQuantityInput: function(inputStr) {
		inputStr = (inputStr || "").toString();
		return !((!/^[0-9\/]+$/.test(inputStr)) || (String(inputStr).indexOf('/')!=-1 && String(inputStr).split('/').length > 2));		
	},
	
	/**
	 * An, hien khu vuc tim kiem
	 *
	 * @author lacnv1
	 * @since Apr 24, 2015
	 * copy tu vnm.utility-report-schedule.js
	 */
	toggleSearchInput: function(t) {
		$(".ErrorMsgStyle").hide();
		$(".SuccessMsgStyle").hide();

		if ($(t).hasClass("searchHidden")) {
			$(t).removeClass("searchHidden");
			$(t).addClass("searchShow");
			$(t).parent().parent().next().slideDown();
		} else {
			$(t).addClass("searchHidden");
			$(t).removeClass("searchShow");
			$(t).parent().parent().next().slideUp();
		}
	},
	
	/**
	 * update stype header row cursor:ponter
	 * @date 27/01/2015
	 */
	 updateCellHeaderStyleSort: function(parentId, lstHeader) {
	 	if(	lstHeader instanceof Array && lstHeader.length != 0){
			for(var i in lstHeader){
			  $('#'+parentId+' .datagrid-header-row td[field='+lstHeader[i]+']').css('cursor', 'pointer');
			}
		}
	},
	/***
	 * Load distributor combobox and choose the first item
	 * @author: trungtm6
	 * @since: 09/07/2015
	 */
	initUnitCbx: function(cbxId, params, cbxWidth, selectCall, successCall, shopCodeDefault, valueField, textField){				
		var par = {};
		if (params != undefined && params != null){
			par = params;
		}
		$.ajax({
			type : "POST",
			url:'/commons/list-child-shop',
			data :($.param(par, true)),
			dataType : "json",
			success : function(data) {
				Utils.bindShopCbx('#' + cbxId, data.rows, shopCodeDefault, cbxWidth, selectCall, successCall, valueField, textField);
			}
		});
	},

	/***
	 * Load distributor combobox and choose the first item
	 * @author: vuongmq
	 * @param cbxId
	 * @param url
	 * @param params
	 * @param cbxWidth
	 * @param selectCall
	 * @param successCall
	 * @param valueDfault
	 * @param valueField
	 * @param textField
	 * @since: 28/10/2015
	 */
	initProgramCbx: function(cbxId, url, params, cbxWidth, selectCall, successCall, valueDfault, valueField, textField) {				
		var par = {};
		if (params != undefined && params != null){
			par = params;
		}
		$.ajax({
			type : "POST",
			url : url,
			data : ($.param(par, true)),
			dataType : "json",
			success : function(data) {
				Utils.bindProgramCbx('#' + cbxId, data.rows, valueDfault, cbxWidth, selectCall, successCall, valueField, textField);
			}
		});
	},
	
	/**
	 * khoi tao combobox program
	 * @author: vuongmq
	 * @param selector
	 * @param dataArr
	 * @param valueDfault
	 * @param cbxWidth
	 * @param selectCall
	 * @param successCall
	 * @param valueField
	 * @param textField
	 * @since: 28/10/2015
	 */
	bindProgramCbx: function(selector, dataArr, valueDfault, cbxWidth, selectCall, successCall, valueField, textField) {
		var value = 'ksCode';
		var text = 'ksName';
		var wid = 206;
		if (cbxWidth != undefined && cbxWidth != null && cbxWidth > 0) {
			wid = cbxWidth;
		}
		if (valueField != undefined && valueField != null) {
			value = valueField;
		}
		if (textField != undefined && textField != null) {
			text = textField;
		}
		$(selector).combobox({
			valueField: value,
			textField:  text,
			data: dataArr,
			panelWidth: wid,
			formatter: function(row) {
				return '<span style="font-weight:bold">' + Utils.XSSEncode(row.ksCode) + '</span><br/>' + '<span style="color:#888">' + Utils.XSSEncode(row.ksName) + '</span>';
			},
			filter: function(q, row) {
				q = new String(q).toUpperCase().trim();
				var opts = $(this).combobox('options');
				return ((unicodeToEnglish(row[opts.textField]).indexOf(unicodeToEnglish(q)) == 0) || unicodeToEnglish(row[opts.valueField]).indexOf(unicodeToEnglish(q)) == 0) ;
			},	
	        onSelect: function(rec) {
	        	if (selectCall != undefined && selectCall != null) {
	        		selectCall.call(this, rec);
	        	}
	        },
	        onLoadSuccess: function() {
	        	var arr = $(selector).combobox('getData');
	        	if (arr != null && arr.length > 0) {			        		
	        		if (valueDfault != undefined && valueDfault != null && valueDfault.length > 0) {
						$(selector).combobox('select', valueDfault);			        				
        			} else if (value == 'ksCode'){
        				$(selector).combobox('select', arr[0].ksCode);
        			} else if (value == 'id'){
        				$(selector).combobox('select', arr[0].ksId);
        			}
	        	}
	        	if (successCall != undefined && successCall != null) {
	        		successCall.call(this, arr);
				}
	        }
		});
	},

	/***
	 * Load distributor combobox and choose the first item Statistic
	 * @author: vuongmq
	 * @param cbxId
	 * @param url
	 * @param params
	 * @param cbxWidth
	 * @param selectCall
	 * @param successCall
	 * @param valueDfault
	 * @param valueField
	 * @param textField
	 * @since: 28/03/2016
	 */
	initProgramStatistic: function(cbxId, url, params, cbxWidth, selectCall, successCall, valueDfault, valueField, textField) {				
		var par = {};
		if (params != undefined && params != null){
			par = params;
		}
		$.ajax({
			type : "POST",
			url : url,
			data : ($.param(par, true)),
			dataType : "json",
			success : function(data) {
				Utils.bindProgramStatistic('#' + cbxId, data.rows, valueDfault, cbxWidth, selectCall, successCall, valueField, textField);
			}
		});
	},
	
	/**
	 * khoi tao combobox program Statistic
	 * @author: vuongmq
	 * @param selector
	 * @param dataArr
	 * @param valueDfault
	 * @param cbxWidth
	 * @param selectCall
	 * @param successCall
	 * @param valueField
	 * @param textField
	 * @since: 28/03/2016
	 */
	bindProgramStatistic: function(selector, dataArr, valueDfault, cbxWidth, selectCall, successCall, valueField, textField) {
		var value = 'code';
		var text = 'name';
		var wid = 206;
		if (cbxWidth != undefined && cbxWidth != null && cbxWidth > 0) {
			wid = cbxWidth;
		}
		if (valueField != undefined && valueField != null) {
			value = valueField;
		}
		if (textField != undefined && textField != null) {
			text = textField;
		}
		$(selector).combobox({
			valueField: value,
			textField:  text,
			data: dataArr,
			panelWidth: wid,
			formatter: function(row) {
				return '<span style="font-weight:bold">' + Utils.XSSEncode(row.code) + '</span><br/>' + '<span style="color:#888">' + Utils.XSSEncode(row.name) + '</span>';
			},
			filter: function(q, row) {
				q = new String(q).toUpperCase().trim();
				var opts = $(this).combobox('options');
				return ((unicodeToEnglish(row[opts.textField]).indexOf(unicodeToEnglish(q)) == 0) || unicodeToEnglish(row[opts.valueField]).indexOf(unicodeToEnglish(q)) == 0) ;
			},	
	        onSelect: function(rec) {
	        	if (selectCall != undefined && selectCall != null) {
	        		selectCall.call(this, rec);
	        	}
	        },
	        onLoadSuccess: function() {
	        	var arr = $(selector).combobox('getData');
	        	if (arr != null && arr.length > 0) {			        		
	        		if (valueDfault != undefined && valueDfault != null && valueDfault.length > 0) {
						$(selector).combobox('select', valueDfault);			        				
        			} else if (value == 'code'){
        				$(selector).combobox('select', arr[0].code);
        			} else if (value == 'id'){
        				$(selector).combobox('select', arr[0].id);
        			}
	        	}
	        	if (successCall != undefined && successCall != null) {
	        		successCall.call(this, arr);
				}
	        }
		});
	},
	
	/***
	 * Load distributor combobox and choose the first item
	 * @author: vuongmq
	 * @param cbxId
	 * @param url
	 * @param params
	 * @param cbxWidth
	 * @param selectCall
	 * @since: 13/11/2015
	 */
	initStaffCbx: function(cbxId, url, params, cbxWidth, selectCall) {				
		var par = {};
		if (params != undefined && params != null){
			par = params;
		}
		$.ajax({
			type : "POST",
			url : url,
			data : ($.param(par, true)),
			dataType : "json",
			success : function(data) {
				Utils.bindStaffCbx('#' + cbxId, data.rows, null, cbxWidth, selectCall);
			}
		});
	},

	/***
	 * Load Warehouse khi change shop
	 * @author: vuongmq
	 * @param selector
	 * @param url
	 * @param params
	 * @param cbxWidth
	 * @param selectCall
	 * @since: 07/12/2015
	 */
	changeShopGetWareHouse: function(selector, url, params, cbxWidth, selectCall) {
		var par = {};
		if (params != undefined && params != null) {
			par = params;
		}
		var wid = 206;
		if (cbxWidth != undefined && cbxWidth != null && cbxWidth > 0) {
			wid = cbxWidth;
		}
		$.ajax({
			type: "POST",
			//url : '/transfer-internal-warehouse/change-shop',
			url: url,
			data: ($.param(par, true)),
			dataType: "json",
			success: function(data) {
				if (data.lstWarehouseVO != undefined && data.lstWarehouseVO != null) {
					$(selector).combobox({
						valueField: 'warehouseId',
						textField: 'warehouseName',
						data: data.lstWarehouseVO,
						width: 250,
						panelWidth: wid,
						formatter: function(row) {
							return '<span style="font-weight:bold">' + Utils.XSSEncode(row.warehouseCode) + ' - ' + Utils.XSSEncode(row.warehouseName) + '</span>';
						},
						filter: function(q, row) {
							q = new String(q).toUpperCase().trim();
							var opts = $(this).combobox('options');
							return unicodeToEnglish(row[opts.textField]).indexOf(unicodeToEnglish(q)) >= 0;
						},
						onSelect:function(rec) {
							if (selectCall != undefined && selectCall != null) {
				        		selectCall.call(this, rec);
				        	}
						},
				        onLoadSuccess: function() {
				        	var arr = $(selector).combobox('getData');
				        	if (arr != null && arr.length > 0){
				        		$(selector).combobox('select', arr[0].warehouseId);
				        	}
				        }
					});
				}
			}
		});
	},

	/**
	 * Import Excel
	 * @author: tientv11
	 * 
	 */
	uploadExcel:function(callback,formId,tit,fileObject,prefix,errMsgId,pFakefilepc,params){
        var frm = 'importFrm';
        if(formId!=null && formId!=undefined){
        	frm = formId;
        }                       
        var title = 'Bạn có muốn nhập dữ liệu từ file';
        if(tit!=null && tit!=undefined){
              title = tit;
        }
        var file = 'excelFile';
        var fakefilepc = 'fakefilepc'; 
        if(pFakefilepc!=null && pFakefilepc!=undefined){
              fakefilepc = pFakefilepc;
        }
        if(fileObject!=null && fileObject!=undefined){
              file = fileObject;
        }                 
        if(!previewImportExcelFile(document.getElementById(file), frm, fakefilepc)){
              return false;
        }
        var objectErrorSelector = '#errExcelMsg';
        if(prefix!=null && prefix!=undefined && errMsgId!=null && errMsgId!=undefined){
//              objectErrorSelector = prefix + " " + errMsgId;
        	objectErrorSelector = prefix + " #" + errMsgId;
        
        }else if(prefix!=null && prefix!=undefined && (errMsgId==null || errMsgId==undefined)){
              objectErrorSelector = prefix + " #errExcelMsg";
              
        }else if((prefix==null || prefix==undefined) && (errMsgId!=null && errMsgId!=undefined)){
//              objectErrorSelector =  errMsgId;
              objectErrorSelector =  '#' + errMsgId;
        }           
        var options = {
                    beforeSubmit : function(){                            
                          $('.ErrorMsgStyle').html('').hide();
                          $('#divOverlay').show();
                          return true;
                    },                      
                    success : function(responseText, statusText, xhr, $form){
                          $('#divOverlay').hide();
                          if (statusText == 'success') {      
                                $("#responseDiv").html(responseText);
                                $('#token').val($('#tokenValue').html().trim());
                                if($('#errorExcel').html().trim() == 'true' || $('#errorExcelMsg').html().trim().length > 0){
                                      $(objectErrorSelector).html($('#errorExcelMsg').html()).show();
                                } else {
                                var obj = new Object();
                                
                                obj.totalRow = Number($('#totalRow').html().trim());
                                obj.numFail = Number($('#numFail').html().trim());
                                obj.fileNameFail = $('#fileNameFail').html();
                                obj.colFails = $('#colFails').html().trim();
                                obj.errMsg = $('#errorExcelMsg').html().trim();
                                obj.token = $('#tokenValue').html().trim();
                                var message = format(msgErr_result_import_excel,(obj.totalRow - obj.numFail),obj.numFail);                    
                                if(obj.numFail > 0){
                                      message+= ' <a href="'+ obj.fileNameFail +'">Xem chi tiết lỗi</a>';
                                }                                   
                                if($('#'+file).length!=0 && $('#'+fakefilepc).length!=0){
                                      try{
                                            $('#'+file).val('');
                                            $('#'+fakefilepc).val('');
                                      }catch(err){
                                            
                                      }
                                }                                   
                                $(objectErrorSelector).html(message).show();
                                if(callback!= null && callback!= undefined){
                                   callback.call(this,obj);
                                }
                          }
                          }
                    },
                    type : "POST",
                    dataType : 'html',
                    data : params
        };
        $('#'+frm).ajaxForm(options);
        $.messager.confirm('Xác nhận', title, function(r){
              if (r){
                    $('#'+frm).submit();
              }
        });
  }
};
function encodeChar(tst){
	var result = "";
	if(tst!=null && tst!= undefined){
		tst=tst.toString();
		var char="!@#$%^&*()_+-:;<>?|\'\"\/";
		for(var i=0;i<tst.length;i++){
			if(char.indexOf(tst[i])!=-1){
				if(escape(tst[i])==tst[i]){
					result += encodeURIComponent(tst[i]);			
				}else{
					result += escape(tst[i]);
				}
			}else{
				result += encodeURI(tst[i]);
			}
		}
	}
	return result;
}
function escapex(value)// tungmt show tag len htmnl
{
	var reGT = />/gi;
	var reLT = /</gi;
	var reQUOT = /"/gi;
	var reAMP = /&/gi;
	var reAPOS = /'/gi;
	var strXML = value.replace(reAMP,"&amp;").replace(reQUOT,"&quot;").replace(reLT,"&lt;").replace(reGT, "&gt;").replace(reAPOS,"&apos;");
	return strXML;
} 
function jConfirm(msg, title, callback, isClose, params, closedCallback) {
	var ret;
	$.fancybox(	
					'<div class="GeneralDialog BoxDialogConfirm"><p class="Text1Style">' + msg + '</p><div class="ButtonSection"><a href="javascript:void(0);" id="fancyConfirm_ok" class="BtnGeneralStyle Sprite2"><span class="Sprite2">Có</span></a><a href="javascript:void(0);" id="fancyConfirm_cancel" onclick="$.fancybox.close();" class="BtnGeneralStyle BtnGeneralCStyle BtnGeneralMStyle Sprite2"><span class="Sprite2">Không</span></a></div></div>',
					{
						modal : true,
						'title': "Xác nhận",
						afterShow : function() {
							var tm = setTimeout(function(){
								$('.fancybox-wrap.fancybox-desktop.fancybox-type-html.fancybox-opened').css('top', document.documentElement.scrollTop + 200);
								clearTimeout(tm);
							}, 500);
							$("#fancyConfirm_ok").click(function(){
								ret = true;
										if (callback != null) {
											callback.call(this, true);
										}
										$.fancybox.close();																				
										
							});
							$("#fancyConfirm_ok").focus();
							$("#fancybox-close").hide();
							$("#fancyConfirm_cancel").click(									
									function() {
										ret = false;
										if (closedCallback != null) {
											closedCallback.call(this, false);
										}
										$.fancybox.close();																
									});
							
							
						},
						afterClose : function() {							
						}
					});
}
function jAlert(msg)
{
	$.fancybox(
			'<div class="GeneralDialog BoxDialogConfirm"><p class="Text1Style">' + msg + '</p><div class="ButtonSection"><a href="javascript:void(0);" id="fancyAlert" class="BtnGeneralStyle Sprite2"><span class="Sprite2">Đóng</span></a></div></div>',
			{
				modal : true,
				'title' : "Thông báo",
				afterShow : function(){
					$("#fancyAlert").click(function(){
						$.fancybox.close();
					});
					$("#fancyAlert").focus();
					$("#fancybox-close").hide();
					$("#fancyAlert").click(
						function(){
							$.fancybox.close();
					});
					setTimeout(function(){
						$('.fancybox-wrap.fancybox-desktop.fancybox-type-html.fancybox-opened').css('top',document.documentElement.scrollTop +  $(window).height()/2);
					},500);
				},
				afterClose : function() {
					isOpenJAlert = false;
				}
			});
}
function Alert(title,msg,callback){
	$.messager.alert(title,msg,null,function(){
		if (callback!= undefined && callback != null) {
			callback.call(this, true);
		}
	});
	return false;
}
function jConfirmAdv(msg, title, callback) {
	var ret;
	$.fancybox(	
					'<div class="GeneralDialog BoxDialogConfirm"><p class="Text1Style">' + msg + '</p><div class="ButtonSection"><a href="javascript:void(0);" id="fancyConfirm_ok" class="BtnGeneralStyle Sprite2"><span class="Sprite2">Có</span></a><a href="javascript:void(0);" id="fancyConfirm_cancel" onclick="$.fancybox.close();" class="BtnGeneralStyle BtnGeneralCStyle BtnGeneralMStyle Sprite2"><span class="Sprite2">Không</span></a></div></div>',
					{
						modal : true,
						'title': "Xác nhận",
						afterShow : function() {
							$("#fancyConfirm_ok").click(function(){
										ret = true;
										if (callback != null) {
											callback.call(this, true);
										}
										$.fancybox.close();																				
										
							});
							$("#fancyConfirm_ok").focus();
							$("#fancybox-close").hide();
							$("#fancyConfirm_cancel").click(
									function() {										
										$.fancybox.close();																
							});
						},
						afterClose : function() {							
						}
					});
	if (ret == true) return true; else return false;
}
function showLoading(id){
	var objId = 'loading';
	if(id!= undefined && id!= null && id.length > 0){
		objId = id;
	}
	$('#' + objId).css('visibility','visible');
	return false;
}
function hideLoading(id){
	var objId = 'loading';
	if(id!= undefined && id!= null && id.length > 0){
		objId = id;
	}
	$('#' + objId).css('visibility','hidden');
	return false;
}
function isNullOrEmpty(value){
	if(value!=undefined && value!=null && !isNaN(value)){
		value = new String(value);
	}
	if(value == undefined || value == null || value == 'null' || value.trim()=='' || value.trim().length == 0){
		return true;
	}
	return false;
}
function setSelectBoxValue(objectId,defaultValue,prefix){
	var value = -2;
	var prefixTmp ='';
	if(defaultValue!= undefined && defaultValue!= null){
		value = defaultValue;
	}
	if(prefix != undefined && prefix!= null && prefix != ''){
		prefixTmp = prefix;
	}
	$(prefixTmp +'#' + objectId).val(Utils.XSSEncode(value));
	$(prefixTmp +'#' + objectId).change();
}
function setTextboxValue(objectId,value,prefix){
	var vl = '';
	var prefixTmp ='';
	if(!isNullOrEmpty(value)){
		vl = value;
	}
	$(prefixTmp +'#' + objectId).val(Utils.XSSEncode(value));
	$(prefixTmp +'#' + objectId).change();
	$('#' + objectId).val(vl);
}
function setCheckBoxValue(objectId,value){
	if(value==1){
		$('#' + objectId).attr('checked', 'checked');
	}
}
function focusFirstTextbox(){
	var isCheck = false;
	$('.InputTextStyle').each(function(){
	    if(!$(this).is(':hidden') && !$(this).is(':disabled') && !isCheck){
	        isCheck = true;
	        $(this).focus();
	    }
	});
}
function uploadPdfFile(fileObj,formId,inputText){
	$('#resultPdfMsg').html('').hide();
	var importTokenVal =   $('#importTokenVal').val();
	if(importTokenVal!=null && importTokenVal!=undefined){
		$('#importTokenVal').val($('#token').val());
	}
	var inputId = 'fakePdffilepc';
	if(inputText != null || inputText != undefined){
		inputId = inputText;
	}
	var fileTypes=["pdf"];
	if (fileObj.value != '') {
		if (!/(\.pdf)$/i.test(fileObj.value)) {
			//Tập tin chọn phải có đuôi mở rộng là .pdf 
			$('#errPdfMsg').html(msgErr_pdf_file_invalid_type).show();
			fileObj.value = '';
			fileObj.focus();
			document.getElementById(inputId).value ='';			
			return false;
		} else {
			$('#errPdfMsg').html('').hide();
			var src = '';
			if ($.browser.msie){
			    var path = encodeURI(what.value);
			    path = path.replace("C:%5Cfakepath%5C","");
			    var path = decodeURI(path);
			    document.getElementById(inputId).value = path;
			}else{
				document.getElementById(inputId).value = fileObj.value;
			}	
			// $('#' + formId).submit();
			// return true;
		}
	}else{
		$('#errPdfMsg').html('Vui lòng chọn file PDF').show();		
		return false;
	}
	return true;
}
//upload file ZIP
function uploadZIPFile(fileObj,formId,inputText, errView){
	$('#errMsg').html('').hide();
	if(errView != undefined && errView != '') {
		$('#'+ errView).html('').hide();
	}
	var importTokenVal =   $('#importTokenVal').val();
	if(importTokenVal!=null && importTokenVal!=undefined){
		$('#importTokenVal').val($('#token').val());
	}
	var inputId = 'fakefilepc';
	if(inputText != null || inputText != undefined){
		inputId = inputText;
	}
	var fileTypes=["zip"];
	if (fileObj.value != '') {
		if (!/(\.zip)$/i.test(fileObj.value)) {
			if(errView != undefined && errView != '') {
				//Tập tin chọn phải có đuôi mở rộng là .pdf ; hien thi len popUp
				$('#'+ errView).html(msgErr_zip_file_invalid_type).show();
			}else {
				//Tập tin chọn phải có đuôi mở rộng là .pdf 
				$('#errMsg').html(msgErr_zip_file_invalid_type).show();
			}
			fileObj.value = '';
			fileObj.focus();
			document.getElementById(inputId).value ='';			
			return false;
		} else {
			$('#'+ errView).html('').hide();
			$('#errMsg').html('').hide();
			var src = '';
			if ($.browser.msie){
			    var path = encodeURI(what.value);
			    path = path.replace("C:%5Cfakepath%5C","");
			    var path = decodeURI(path);
			    document.getElementById(inputId).value = path;
			}else{
				document.getElementById(inputId).value = fileObj.value;
			}	
			// $('#' + formId).submit();
			// return true;
		}
	}else{
		$('#errMsg').html('Vui lòng chọn file ZIP!').show();		
		return false;
	}
	return true;
}
//upload file bao cao vnm.report-manager.js, load ca file .jrxml
function uploadJasperFile(fileObj,formId,inputText, errView){
	$('#errMsg').html('').hide();
	if(errView != undefined && errView != '') {
		$('#'+ errView).html('').hide();
	}
	var importTokenVal =   $('#importTokenVal').val();
	if(importTokenVal!=null && importTokenVal!=undefined){
		$('#importTokenVal').val($('#token').val());
	}
	var inputId = 'fakefilepc';
	if(inputText != null || inputText != undefined){
		inputId = inputText;
	}
	var fileTypes=["zip"];
	if (fileObj.value != '') {
		if (!/(\.jrxml)$/i.test(fileObj.value)) {
			if(errView != undefined && errView != '') {
				//Tập tin chọn phải có đuôi mở rộng là .jrxml ; hien thi len popUp
				$('#'+ errView).html(msgErr_report_file_invalid_type).show();
			}else {
				//Tập tin chọn phải có đuôi mở rộng là .jrxml 
				$('#errMsg').html(msgErr_report_file_invalid_type).show();
			}
			fileObj.value = '';
			fileObj.focus();
			document.getElementById(inputId).value ='';			
			return false;
		} else {
			$('#' + errView).html('').hide();
			$('#errMsg').html('').hide();
			var src = '';
			if ($.browser.msie){
			    var path = encodeURI(what.value);
			    path = path.replace("C:%5Cfakepath%5C","");
			    var path = decodeURI(path);
			    document.getElementById(inputId).value = path;
			}else{
				document.getElementById(inputId).value = fileObj.value;
			}	
			// $('#' + formId).submit();
			// return true;
		}
	}else{
		$('#errMsg').html('Vui lòng chọn Report file .jrxml !').show();		
		return false;
	}
	return true;
}


function loadDataForTree(url,treeId){
	var tId = 'tree';
	if(treeId!= null && treeId!= undefined){
		tId = treeId;
	}
	$('#' + tId).jstree({
        "plugins": ["themes", "json_data","ui"],
        "themes": {
            "theme": "classic",
            "icons": false,
            "dots": true
        },
        "json_data": {
        	"ajax" : {
                "url" : url,
                "data" : function (n) {
                        return { id : n.attr ? n.attr("id") : 0, name: n.attr ? n.attr("name") : "", url: n.attr ? n.attr("url") : ""};
                    }
            }        	
        }
	});	
}

function loadDataForTreeWithCheckbox(url,treeId){
	var tId = 'tree';
	if(treeId!= null && treeId!= undefined){
		tId = treeId;
	}
	$('#' + tId).jstree({
        "plugins": ["themes", "json_data","ui","checkbox"],
        "themes": {
            "theme": "classic",
            "icons": false,
            "dots": true
        },
        "json_data": {
        	"ajax" : {
        		"method": "post",
                "url" : url,
                "data" : function (n) {
                        return { id : n.attr ? n.attr("id") : 0 };
                    }
            }        	
        }
	});	
}
var datepickerTriggerTimeout = null;
function applyDateTimePicker(selector, dateFormat, hideYear, memuMonthShow, menuYearShow, yearRangeFlag, showFocus, timeShow, yearRangeFuture, monthYearShow, onlyMonthCurrent, onSelectCallback) {
	var format = "dd/mm/yy";
	if (dateFormat != null) {
		format = dateFormat;		
	}
	if(monthYearShow != null){
		MaskManager.maskMonth(selector);
	}else{
		MaskManager.maskDate(selector);
	}
	var menuMonth = false;
	if (memuMonthShow != null) {
		menuMonth = memuMonthShow;
	}
	var menuYear = false;
	if (menuYearShow != null) {
		menuYear = menuYearShow;
	}
	var minDate = null;
	var maxDate = null;
	if (hideYear != null && hideYear == true) {
		minDate = new Date(2012, 0, 1);
		maxDate = new Date(2012, 11, 31);
	}
	var yearRange = null;
	if (yearRangeFlag != null && yearRangeFlag != undefined) {
		var now = new Date();
		var t = now.getFullYear();
		var f = t -100;
		if(yearRangeFuture != null && yearRangeFuture != undefined){
			t = t + 100;
			yearRange = f+":"+t;
		}else{
			yearRange = f+":"+t;
		}
		
		
	}
	var showOn = 'button';
	if (showFocus != null && showFocus == true) {
		showOn = 'focus';
	}
	if (!timeShow)
	{
		$(selector).datepicker(
			{
				//monthNames: ["Tháng 1", "Tháng 2", "Tháng 3", "Tháng 4", "Tháng 5", "Tháng 6", "Tháng 7", "Tháng 8", "Tháng 9", "Tháng 10", "Tháng 11", "Tháng 12"],
				//monthNamesShort: ["Tháng 1", "Tháng 2", "Tháng 3", "Tháng 4", "Tháng 5", "Tháng 6", "Tháng 7", "Tháng 8", "Tháng 9", "Tháng 10", "Tháng 11", "Tháng 12"],
				monthNames: [msgThang1, msgThang2, msgThang3, msgThang4, msgThang5, msgThang6, msgThang7, msgThang8, msgThang9, msgThang10, msgThang11, msgThang12],
				monthNamesShort: [msgThang1, msgThang2, msgThang3, msgThang4, msgThang5, msgThang6, msgThang7, msgThang8, msgThang9, msgThang10, msgThang11, msgThang12],
				//dayNamesMin: ["CN", "T2", "T3", "T4", "T5", "T6", "T7"],
				dayNamesMin: [msgCN, msgThu2, msgThu3, msgThu4, msgThu5, msgThu6, msgThu7],
				showOn: showOn,
				dateFormat: format,
				buttonImage: '/resources/images/icon_calendar.jpg',
				buttonImageOnly: true,
				changeMonth: menuMonth,
				changeYear: menuYear,
				minDate: minDate,
				maxDate: maxDate,
				buttonText: '',
				useThisTheme: 'start',
				yearRange: yearRange,
				create: function(event, ui) {
				},
				onClose: function(dateText, inst) {
				},
				beforeShow: function(input, inst) {
					setTimeout(function() {
						$('.ui-datepicker-month').css('width', '60%');
						$('#ui-datepicker-div').css('z-index', 1000000);
					}, 1);
					if ((hideYear != null && hideYear == true)||(onlyMonthCurrent != null && onlyMonthCurrent == true)) {
						setTimeout(function() {
							$('.ui-datepicker-next').hide();
							$('.ui-datepicker-prev').hide();
							$('.ui-datepicker-year').hide();
						}, 1);
					}
				},
				onChangeMonthYear: function(year, month, inst) {
					setTimeout(function() {
						$('.ui-datepicker-month').css('width', '60%');
					}, 1);
					if ((hideYear != null && hideYear == true)||(onlyMonthCurrent != null && onlyMonthCurrent == true)) {
						setTimeout(function() {
							$('.ui-datepicker-next').hide();
							$('.ui-datepicker-prev').hide();
							$('.ui-datepicker-year').hide();
						}, 1);
					}
				},
				onSelect: function(data) {
					if (onSelectCallback != undefined && onSelectCallback != null) {
						onSelectCallback.call(this, data);
					}					
				}
			}
		);
	}
}
var AttributeUtil = {
	disabledAttributes:function(){
		for(var i = 1;i<=$('#lstAttributeSize').val();i++){
			var obj = $('#attributeCode_'+i);
			if(obj.hasClass('HasClassSelected')){
				disableSelectbox('attributeCode_'+i);
			}else{
				disabled('attributeCode_'+i);
			}
		}
	},
	enabledAttributes:function(){
		for(var i = 1;i<=$('#lstAttributeSize').val();i++){
			var obj = $('#attributeCode_'+i);
			if(obj.hasClass('HasClassSelected')){
				enableSelectbox('attributeCode_'+i);
			}else{
				enable('attributeCode_'+i);
			}
		}
	}
};
function showMonthAndYear(selector){
	$(selector).datepicker({
		dateFormat: 'mm/yy',
	    changeMonth: true,
	    changeYear: true,
	    showButtonPanel: true,
		monthNames: ["Tháng 1", "Tháng 2", "Tháng 3", "Tháng 4", "Tháng 5", "Tháng 6", "Tháng 7", "Tháng 8", "Tháng 9", "Tháng 10", "Tháng 11", "Tháng 12"],
		monthNamesShort: ["T1", "T2", "T3", "T4", "T5", "T6", "T7", "T8", "T9", "T10", "T11", "T12"],
		showOn: 'focus',
		buttonImage: '/resources/images/icon-calendar.png',
		buttonImageOnly: true,
		buttonText: '',
		useThisTheme: 'start',
		create: function(event, ui) {
		},
		onClose: function(dateText, inst) {
	        var month = $("#ui-datepicker-div .ui-datepicker-month :selected").val();
	        var year = $("#ui-datepicker-div .ui-datepicker-year :selected").val();
	        $(this).val($.datepicker.formatDate('mm/yy', new Date(year, month, 1)));
	    }
	});
}
function setDateTimePicker(id, prefix, onlyMonthCurrent){
	// $('#' +id).attr('readonly','readonly');
	var prefixTmp ='';
	if(prefix != undefined && prefix!= null && prefix != ''){
		prefixTmp = prefix;
	}
	if(onlyMonthCurrent) {
		applyDateTimePicker(prefixTmp+'#' + id, null, null, null, null, null, null, null, null,null,true);
	} else{
		applyDateTimePicker(prefixTmp+'#' + id);
	}
	$(prefixTmp+'#' + id).keypress(function(event){
		var keycode = (event.keyCode ? event.keyCode : event.which);
		if (keycode == keyCodes.BACK_SPACE || keycode == keyCodes.DELETE) {
			$(this).val('');
			$(this).focus();
		}
	});
}
function setDateTimePickerTrigger(id, prefix, onlyMonthCurrent){
	// $('#' +id).attr('readonly','readonly');
	var prefixTmp ='';
	if(prefix != undefined && prefix!= null && prefix != ''){
		prefixTmp = prefix;
	}
	var fTrigger = function(object) {
		$(object).trigger("change");
	};
	if(onlyMonthCurrent) {
		applyDateTimePicker(prefixTmp+'#' + id, null, null, null, null, null, null, null, null,null,true,fTrigger('#'+id));
	} else{
		applyDateTimePicker(prefixTmp+'#' + id, null, null, null, null, null, null, null, null,null,undefined,fTrigger('#'+id));
	}
	$(prefixTmp+'#' + id).keypress(function(event){
		var keycode = (event.keyCode ? event.keyCode : event.which);
		if (keycode == keyCodes.BACK_SPACE || keycode == keyCodes.DELETE) {
			$(this).val('');
			$(this).focus();
		}
	});
}
function removeDateTimePicker(id, prefix){
	// $('#' +id).attr('readonly','readonly');
	var prefixTmp ='';
	if(prefix != undefined && prefix!= null && prefix != ''){
		prefixTmp = prefix;
	}
	$(prefixTmp+'#' + id).datepicker( "destroy" );
	$(prefixTmp+'#' + id).removeClass("hasDatepicker");
}
function disableDateTimePicker(id, prefix){
	var prefixTmp ='';
	if(prefix != undefined && prefix!= null && prefix != ''){
		prefixTmp = prefix;
	}
	$(prefixTmp+'#' + id).datepicker( "disable");
}
function setAndDisableDateTimePicker(id,prefix){
	var prefixTmp ='';
	if(prefix != undefined && prefix!= null && prefix != ''){
		prefixTmp = prefix;
	}
	$(prefixTmp+'#' + id).attr('disabled',true);
	setDateTimePicker(id,prefix);
	disableDateTimePicker(id,prefix);
}
function enableDateTimePicker(id, prefix){
	var prefixTmp ='';
	if(prefix != undefined && prefix!= null && prefix != ''){
		prefixTmp = prefix;
	}
	$(prefixTmp+'#' + id).datepicker( "enable");
}
function numbersonly(e, id) {
	var key;
	var keychar;
	if (window.event) {
	   key = window.event.keyCode;
	}else if (e) {
	   key = e.which;
	}else {
	   return true;
	}
	keychar = String.fromCharCode(key);

	if ((key==null) || (key==0) || (key==8) ||  (key==9) || (key==13) || (key==27) ) {
	   return true;
	}else if ((("0123456789").indexOf(keychar) > -1)) {
		return true;
	}else{
	   return false;
	}
}
function updateTokenImport(){
	var tokenHiddenFiled = $('#tokenHiddenFiled').val();
	if(tokenHiddenFiled!=null && tokenHiddenFiled!=undefined){
		Utils.setToken(tokenHiddenFiled.trim());
		$('#importTokenVal').val(tokenHiddenFiled.trim());
	}	
}
var dates = {
	    convert:function(d) {
	        // Converts the date in d to a date-object. The input can be:
	        // a date object: returned without modification
	        // an array : Interpreted as [year,month,day]. NOTE: month is 0-11.
	        // a number : Interpreted as number of milliseconds
	        // since 1 Jan 1970 (a timestamp)
	        // a string : Any format supported by the javascript engine, like
	        // "YYYY/MM/DD", "MM/DD/YYYY", "Jan 31 2009" etc.
	        // an object : Interpreted as an object with year, month and date
	        // attributes. **NOTE** month is 0-11.
	        return (
	            d.constructor === Date ? d :
	            d.constructor === Array ? new Date(d[0],d[1],d[2]) :
	            d.constructor === Number ? new Date(d) :
	            d.constructor === String ? new Date(d) :
	            typeof d === "object" ? new Date(d.year,d.month,d.date) :
	            NaN
	        );
	    },
	    compare:function(a,b) {
	        // Compare two dates (could be of any type supported by the convert
	        // function above) and returns:
	        // -1 : if a < b
	        // 0 : if a = b
	        // 1 : if a > b
	        // NaN : if a or b is an illegal date
	        // NOTE: The code inside isFinite does an assignment (=).
	        return (
	            isFinite(a=this.convert(a).valueOf()) &&
	            isFinite(b=this.convert(b).valueOf()) ?
	            (a>b)-(a<b) :
	            NaN
	        );
	    },
	    inRange:function(d,start,end) {
	        // Checks if date in d is between dates in start and end.
	        // Returns a boolean or NaN:
	        // true : if d is between start and end (inclusive)
	        // false : if d is before start or after end
	        // NaN : if one or more of the dates is illegal.
	        // NOTE: The code inside isFinite does an assignment (=).
	       return (
	            isFinite(d=this.convert(d).valueOf()) &&
	            isFinite(start=this.convert(start).valueOf()) &&
	            isFinite(end=this.convert(end).valueOf()) ?
	            start <= d && d <= end :
	            NaN
	        );
	    },
	    addDays:function(n){
	    	var curdate = new Date();
	    	var time = curdate.getTime();
	        var changedDate = new Date(time + (n * 24 * 60 * 60 * 1000));
	        curdate.setTime(changedDate.getTime());	        
	        return $.datepicker.formatDate('dd/mm/yy', curdate);
	    }
	};
function checkNum(data) { 
	var valid = "0123456789.";
	var ok = 1; var checktemp;
	for (var i=0; i<data.length; i++) {
		checktemp = "" + data.substring(i, i+1);
		if (valid.indexOf(checktemp) == "-1") return 0; 
	}
	return 1;
}
function formatCurrency(num) {
		if(num == undefined || num == null) {
			return '';
		}
		num = num.toString().split('.');
		var ints = num[0].split('').reverse();
		for (var out=[],len=ints.length,i=0; i < len; i++) {
			if (i > 0 && (i % 3) === 0){
				out.push(',');	
			}
			out.push(ints[i]);
		}
	  out = out.reverse() && out.join('');
	  if (num.length === 2) out += '.' + num[1];
	  if(out.length > 1 && out[0] == '-' && out[1] == ','){
		  out = out.replace(',','');
	  }
	  return out;
}
function formatCurrencyInterger(value) {
	if(value < 0) {
		var valuetmp = Math.abs(value);
		var valuetest = '-'+ formatCurrency(valuetmp);
		return valuetest;
	}
	else {
		return formatCurrency(value);
	}
}
function formatCurrencyNegativeToInteger(value) {
	if(value < 0) {
		var valuetmp = Math.abs(value);
		var valuetest = formatCurrency(valuetmp);
		return valuetest;
	}
	else {
		return formatCurrency(value);
	}
}
function roundNumber(value,ext){
	var tmp=Math.pow(10, Math.round(ext));
	return (Math.round(Number(value)*tmp)/tmp);
}

/**
* Convert gia tri theo ham, ung voi typeMath va lay extention cua gia tri
* default: typemMath: Utils._MATH_ROUND, ext: 0
* @author vuongmq
* @params value: gia tri truyen vao; 
* @params typeMath: loai convert Value; 
* @param ext: lay gia tri bao nhieu so thap phan
* @return value, ngc lai return 0
* @since 20/08/2015
*/
function convertValueMath(value, typeMath, ext) {
	if (value == undefined || value == null || value == '') {
		return 0;
	}
	if (typeMath == undefined || typeMath == null || typeMath == '') {
		typeMath = Utils._MATH_ROUND;
	}
	if (ext == undefined || ext == null || ext == '') {
		ext = 0;
	}
	if (typeMath == Utils._MATH_ROUND) {
		var tmp = Math.pow(10, Math.round(ext));
		return (Math.round(Number(value) * tmp) / tmp);
	} else if (typeMath == Utils._MATH_FLOOR) {
		var tmp = Math.pow(10, Math.floor(ext));
		return (Math.floor(Number(value) * tmp) / tmp);
	} else if (typeMath == Utils._MATH_CEIL) {
		var tmp = Math.pow(10, Math.ceil(ext));
		return (Math.ceil(Number(value) * tmp) / tmp);
	}
}

function formatFloatValue(value,toF){	
	var  fixed = toF;
	if(toF==null ||toF.length==0||toF == undefined){
		fixed = 0;
	}
	if(value==undefined || value==null){
		return 0;
	}
	if(value.toString().indexOf('.')==-1){
		return formatCurrency(value);
	}	
	var result = parseFloat(value).toFixed(fixed);
	if(result.toString().indexOf('.')==-1){
		return formatCurrency(result);
	}else{
		var strResult = result.toString().split('.');
		if(strResult[1].length==0){
			return formatCurrency(strResult[0]);
		}
		var last = strResult[1].toString().substring(0,fixed);
		if(parseInt(last,10)==0){
			return formatCurrency(strResult[0]);
		}else{
			//last = last.replace('0','');
			var i = last.length - 1;
			for (; i > -1; i--) {
			    if (last[i] != '0') {
			        break;
			    }
			}
			last = last.substring(0, i+1);
			return formatCurrency(strResult[0]) + '.' + last;
		}
	}
	
}
function disabled(objId){
	if($('#' + objId).length!=0){
		$('#' + objId).attr('disabled','disabled');
		$('#' + objId).addClass('BtnGeneralDStyle');
	}
}
function enable(objId){
	if($('#' + objId).length!=0){
		$('#' + objId).removeAttr('disabled');
		$('#' + objId).removeClass('BtnGeneralDStyle');
	}
}
function readonly(objId){
	$('#' + objId).attr('readonly','readonly');
}
String.format = function( text ){
    if ( arguments.length <= 1 ){return text;}
    var tokenCount = arguments.length - 2;
    for( var token = 0; token <= tokenCount; token++ ){
        text = text.replace( new RegExp( "\\{" + token + "\\}", "gi" ), arguments[ token + 1 ] );
    }
    return text;
};
String.prototype.endsWith = function (a) {
    return this.substr(this.length - a.length) === a;
};
String.prototype.startsWith = function (a) {
    return this.substr(0, a.length) === a;
};
String.prototype.trim = function () {
    return this.replace(/^\s*/, "").replace(/\s*$/, "");
    };
String.prototype.count=function(s1) { 
    return (s1 == undefined || s1 == null || s1 == '') ? 0 : (this.length - this.replace(new RegExp(s1,"g"), '').length) / s1.length;
};
String.prototype.bool = function() {
    return (/^true$/i).test(this);
};
function format(text){
	// check if there are two arguments in the arguments list
    if ( arguments.length <= 1 ){
        // if there are not 2 or more arguments there's nothing to replace
        // just return the original text
        return text;
    }
    // decrement to move to the second argument in the array
    var tokenCount = arguments.length - 2;
    for( var token = 0; token <= tokenCount; token++ ){
        // iterate through the tokens and replace their placeholders from the
		// original text in order
        text = text.replace(new RegExp( "\\{" + token + "\\}", "gi" ), Utils.XSSEncode(arguments[ token + 1 ]));
    }
    return text;
};
function resetAllOptions(objId){
	$('#' + objId + ' option').show();
}
function hideOption(selId, optionValue){
	resetAllOptions(selId);
	$('#' + selId + ' option[value='+ optionValue +']').hide();
}
function getCurrentDate(){
	var d=new Date();
	var dStr= (d.getDate().toString().length>1?d.getDate():'0'+d.getDate())+'/'+((d.getMonth()+1).toString().length>1?(d.getMonth()+1):'0'+(d.getMonth()+1))+'/'+d.getFullYear();
	return dStr;
}
function getCurrentMonth(){
	var d=new Date();
	var dStr= ((d.getMonth()+1).toString().length>1?(d.getMonth()+1):'0'+(d.getMonth()+1))+'/'+d.getFullYear();
	return dStr;
}
function getCurrentMonthAddOne(){
	var d=new Date();
	var dStr= ((d.getMonth()+2).toString().length>1?(d.getMonth()+2):'0'+(d.getMonth()+2))+'/'+d.getFullYear();
	return dStr;
}
function getCurrentMonthAddOneNextYear(){
	var d=new Date();
	var dStr= ((d.getMonth()+2).toString().length>1?(d.getMonth()+2):'0'+(d.getMonth()+2))+'/'+(d.getFullYear()+1);
	return dStr;
}
function getCurrentYear(){
	var d=new Date();
	return d.getFullYear();
}
function getLastWeek(){
	var d = new Date();
	var previousWeek= new Date(d.getTime() - 7 * 24 * 60 * 60 * 1000);
	var lastWeek= (previousWeek.getDate().toString().length>1?previousWeek.getDate():'0'+previousWeek.getDate())+'/'+((previousWeek.getMonth()+1).toString().length>1?(previousWeek.getMonth()+1):'0'+(previousWeek.getMonth()+1))+'/'+previousWeek.getFullYear();
	return lastWeek;
}
function resizeFancybox(){
	$('.fancybox-inner').css({'height':'auto'});
	$('#fancyData').css({'height':'auto'});
	$.fancybox.update();
}
function getQuantity(amount,convfact){
	return StockValidateInput.getQuantity(amount, convfact);
}
function getShopCodeByShopCodeAndName(str){
	var value = str.trim().split("-");
	return value[0];
}
function formatQuantity(amount,convfact){
	return StockValidateInput.formatStockQuantity(amount, convfact);
}
function formatQuantityEx(amount,convfact){		
	var quantity = Number(getQuantity(amount,convfact));
	if(quantity<0){
		quantity = quantity * (-1);
		var str = formatQuantity(quantity,convfact);
		return '-' + str.split('/')[0] + '/' + '-' + str.split('/')[1];
	}else{
		return formatQuantity(quantity, convfact);
	}
}
function NextAndPrevTextField(e,selector,clazz){	
	var index = $('input.'+clazz).index(selector);	
	var shiftKey = e.shiftKey && e.keyCode == keyCodes.TAB;	
	//Co the them keyCodes.ENTER vo 
	if((e.keyCode == keyCodes.ARROW_DOWN)){	
		++index;
		var nextSelector = $('input.'+clazz).eq(index);
		if($(nextSelector).hasClass(clazz)){				
			var tm = setTimeout(function(){
				$('input.'+clazz).eq(index).focus();
				clearTimeout(tm);
			}, 20);
		}
	}else if(e.keyCode == keyCodes.ARROW_UP || shiftKey){
		--index;
		var nextSelector = $('input.'+clazz).eq(index);
		if($(nextSelector).hasClass(clazz)){	
			var tm = setTimeout(function(){
				$('input.'+clazz).eq(index).focus();
				clearTimeout(tm);
			}, 20);
		}			
	}
}
/**
 * map data structure
 * @param {array} initData init data for map, array of object(key, value).
 *                         Example: [{key: k1, value: v1}, {key: k2, value: v2}]
 */
function Map(initData) {
    this.keyArray = (function(initData){
	    /*
	     * initialize data
	     */
	    try {
		    if (initData !== null && initData !== undefined && initData instanceof Array) {
		    	var key = "key", value = "value";
		    	var initKeyArray = new Array();
		    	initData.forEach(function(item) {
		    		if (item[key] !== undefined && item[key] !== null) {
		    			initKeyArray.push(item[key]);
		    		}
		    	});
		    	return initKeyArray;
		    }
		    return new Array();
	    } catch (e) {
	    	return new Array();
	    }    	
    })(initData); // Keys
    
    this.valArray = (function(initData){
	    /*
	     * initialize data
	     */
	    try {
		    if (initData !== null && initData !== undefined && initData instanceof Array) {
		    	var key = "key", value = "value";
		    	var initValArray = new Array();
		    	initData.forEach(function(item) {
		    		if (item[value] !== undefined && item[value] !== null) {
		    			initValArray.push(item[value]);
		    		}
		    	});
		    	return initValArray;
		    }
		    return new Array();
	    } catch (e) {
	    	return new Array();
	    }    	
    })(initData);; // Values

    this.put = put;
    this.get = get;
    this.size = size;  
    this.clear = clear;
    this.keySet = keySet;
    this.valSet = valSet;
    this.showMe = showMe;   // returns a string with all keys and values in map.
    this.findIt = findIt;
    this.remove = remove;	
    this.findIt4Val = findIt4Val;
	function put(key, val) {
    	var elementIndex = this.findIt(key);
	    if(elementIndex == (-1)) {
	        this.keyArray.push(key);
	        this.valArray.push(val);
	    } else {
	        this.valArray[elementIndex] = val;
	    }
	}
	function get(key) {
	    var result = null;
	    var elementIndex = this.findIt(key);
	    if (elementIndex != (-1)) {
	        result = this.valArray[elementIndex];
	    }
	    return result;
	}	
	function remove(key) {
	    var result = null;
	    var elementIndex = this.findIt(key);
	    if(elementIndex != (-1)) {
	        // this.keyArray = this.keyArray.myRemoveAt(elementIndex);
	    	this.keyArray.splice(elementIndex,1);
	        // this.valArray = this.valArray.myRemoveAt(elementIndex);
	    	this.valArray.splice(elementIndex,1);
	    }  
	    return ;
	}
	function size() {
	    return (this.keyArray.length);
	}
	function clear() {
        this.keyArray = new Array();
		this.valArray = new Array();
	}
	function keySet() {
	    return (this.keyArray);
	}
	function valSet() {
	    return (this.valArray);
	}
	function showMe() {
	    var result = "";
	    for( var i = 0; i < this.keyArray.length; i++ ) {
	        result += "Key: " + this.keyArray[ i ] + "\tValues: " + this.valArray[ i ] + "\n";
	    }
	    return result;
	}
	function findIt(key) {
	    var result = (-1);
	    for(var i = 0; i < this.keyArray.length; i++) {
	        if(this.keyArray[ i ] == key) {
	            result = i;
	            break;
	        }
	    }
	    return result;
	}
	function findIt4Val(val){
		var result = (-1);
	    for(var i = 0; i < this.valArray.length; i++) {
	        if(this.valArray[ i ] == val) {
	            result = i;
	            break;
	        }
	    }
	    return result; 
	}
}
function showSuccessMsg(id,data,callback,timeOut, message){
	var time = 3000;
	var msg = 'Lưu dữ liệu thành công';
	if (timeOut!= undefined && timeOut != null && timeOut>0) {
		time = timeOut;
	}
	if (message!= undefined && message != null && message != ''){
		msg = message;
	}
	if(!data.error){
		$('#' + id).html(msg).show();
		var tm = setTimeout(function(){
			$('#' + id).html('').hide();
			if (callback!= undefined && callback != null) {
				callback.call(this, true);
			}
			clearTimeout(tm);
		}, time);
	}	
}
function showMsgTimeOut(id,msg,time,callback){
	$('#' + id).html(msg).show();
	var tm = setTimeout(function(){
		$('#' + id).html('').hide();
		if (callback!= undefined && callback != null) {
			callback.call(this, true);
		}
		clearTimeout(tm);
	}, time);
}
function setTitleSearch(id){
	var objId = 'title';
	if(id!= undefined && id!= null && id.length > 0){
		objId = id;
	}
	$('#' + objId).html(unit_tree_search_unit_search_header);
}
function setTitleUpdate(id){
	var objId = 'title';
	if(id!= undefined && id!= null && id.length > 0){
		objId = id;
	}
	$('#' + objId).html(organization_information);
}
function setTitleAdd(id){
	var objId = 'title';
	if(id!= undefined && id!= null && id.length > 0){
		objId = id;
	}
	$('#' + objId).html('Thêm mới thông tin');
}
function formatDate(d){
	if(d != null || d != undefined){
		d = d.substring(0,10);
		d = d.split('-');
		d = d[2] + '/' + d[1] + '/' + d[0];
	}
	return d;
}
function formatMonth(d){
	if(d != null || d != undefined){
		d = d.substring(0,10);
		d = d.split('-');
		d = d[1] + '/' + d[0];
	}
	return d;
}
function gotoPage(link){
	window.location.href = link;
}
var loadedResources = new Array();
function loadResource(filename, filetype, callback){	
		if (filetype=="js"){ // if filename is a external JavaScript file
			var fileref=document.createElement('script');
			fileref.setAttribute("type","text/javascript");
			fileref.setAttribute("src", filename);
		}
		else if (filetype=="css"){ // if filename is an external CSS file
			var fileref=document.createElement("link");
			fileref.setAttribute("rel", "stylesheet");
			fileref.setAttribute("type", "text/css");
			fileref.setAttribute("href", filename);
		}
		if (typeof fileref != "undefined"){
			if(callback != undefined){
				if(fileref.readyState){
					fileref.onreadystatechange = function (){
						if (fileref.readyState == 'loaded' || fileref.readyState == 'complete'){
							callback();
						}
					};
				} else {
					fileref.onload = callback;
				}
			}
			document.getElementsByTagName("head")[0].appendChild(fileref);
		}
		loadedResources.push(filename);	
}
function hideColumnOfJqGrid(gridId,columnName){
	// $('#' + gridId).hideCol(columnName);
};
function checkPermissionEdit(){
	var status = $('#hidStatus').val();
	if(status !=undefined){
		if($('#hidStatus').val().trim()=='1'){
			return false;
		}
	}
	if($('#csPer').val().trim() == 'true' && $('#ePer').val().trim() == 'true'){
		return true;
	}
	return false;
}
function checkPermissionDev(){
	if(($('#dPer').val().trim() == 'true')&&($('#dEdit').val().trim() == 'true')){
		return true;
	}
	return false;
}
function bindAutoHideErrMsg(){
	$('.ui-jqgrid-view a').bind('click', function(){
		$('.ErrorMsgStyle').each(function(){
			if(!$(this).is(':hidden')){
				$(this).html('').hide();
			}
		});		
	});
}
function setComboValue(objectId,defaultValue,prefix){
	var value = -2;
	var prefixTmp ='';
	if(defaultValue!= undefined && defaultValue!= null){
		value = defaultValue;
	}
	if(prefix != undefined && prefix!= null && prefix != ''){
		prefixTmp = prefix;
	}
	$(prefixTmp +'#' + objectId).combobox('setValue',value);
}
function disableSelectbox(id){
	disabled(id);
	$('#' + id).change();
	$('#' + id).parent().addClass('BoxDisSelect');
}
function enableSelectbox(id){
	enable(id);
	$('#' + id).change();
	$('#' + id).parent().removeClass('BoxDisSelect');
}
function disableCombo(id){
	$('#' + id).combobox('disable');
	$('#' + id).parent().addClass('BoxDisSelect');
}
function enableCombo(id){
	$('#' + id).combobox('enable');
	$('#' + id).parent().removeClass('BoxDisSelect');
}
function disableComboTree(id){
	$('#' + id).combotree('disable');  
	$('#' + id).parent().addClass('BoxDisSelect');
}
function enableComboTree(id){
	$('#' + id).combotree('enable');  
	$('#' + id).parent().removeClass('BoxDisSelect');
}
function showCodeAndName(code , name) {
	if(isNullOrEmpty(code)&&!isNullOrEmpty(name)) {
		return name;
	}
	if(!isNullOrEmpty(code)&&isNullOrEmpty(name)) {
		return code;
	}
	if(!isNullOrEmpty(code)&&!isNullOrEmpty(name)) {
		return code +  ' - ' + name ;
	}
	return '';
}
function fromKeyCode (n) {
	if( 47<=n && n<=90 ) return unescape('%'+(n).toString(16));
	if( 96<=n && n<=105) return 'NUM '+(n-96);
	if(112<=n && n<=135) return 'F'+(n-111);

	if(n==3)  return 'Cancel'; // DOM_VK_CANCEL
	if(n==6)  return 'Help';   // DOM_VK_HELP
	if(n==8)  return 'Backspace';
	if(n==9)  return 'Tab';
	if(n==12) return 'NUM 5';  // DOM_VK_CLEAR
	if(n==13) return 'Enter';
	if(n==16) return 'Shift';
	if(n==17) return 'Ctrl';
	if(n==18) return 'Alt';
	if(n==19) return 'Pause|Break';
	if(n==20) return 'CapsLock';
	if(n==27) return 'Esc';
	if(n==32) return 'Space';
	if(n==33) return 'PageUp';
	if(n==34) return 'PageDown';
	if(n==35) return 'End';
	if(n==36) return 'Home';
	if(n==37) return 'Left Arrow';
	if(n==38) return 'Up Arrow';
	if(n==39) return 'Right Arrow';
	if(n==40) return 'Down Arrow';
	if(n==42) return '*'; // Opera
	if(n==43) return '+'; // Opera
	if(n==44) return 'PrntScrn';
	if(n==45) return 'Insert';
	if(n==46) return 'Delete';

	if(n==91) return 'WIN Start';
	if(n==92) return 'WIN Start Right';
	if(n==93) return 'WIN Menu';
	if(n==106) return '*';
	if(n==107) return '+';
	if(n==108) return 'Separator'; // DOM_VK_SEPARATOR
	if(n==109) return '-';
	if(n==110) return '.';
	if(n==111) return '/';
	if(n==144) return 'NumLock';
	if(n==145) return 'ScrollLock';

	// Media buttons (Inspiron laptops)
	if(n==173) return 'Media Mute On|Off';
	if(n==174) return 'Media Volume Down';
	if(n==175) return 'Media Volume Up';
	if(n==176) return 'Media >>';
	if(n==177) return 'Media <<';
	if(n==178) return 'Media Stop';
	if(n==179) return 'Media Pause|Resume';

	if(n==182) return 'WIN My Computer';
	if(n==183) return 'WIN Calculator';
	if(n==186) return '; :';
	if(n==187) return '= +';
	if(n==188) return ', <';
	if(n==189) return '- _';
	if(n==190) return '. >';
	if(n==191) return '/ ?';
	if(n==192) return '\` ~';
	if(n==219) return '[ {';
	if(n==220) return '\\ |';
	if(n==221) return '] }';
	if(n==222) return '\' "';
	if(n==224) return 'META|Command';
	if(n==229) return 'WIN IME';

	if(n==255) return 'Device-specific'; // Dell Home button (Inspiron
											// laptops)

	return null;
	}
function updateRownumWidthForJqGrid(parentId){	
	var pId = '';
	if(parentId!= null && parentId!= undefined){
		pId = parentId + ' ';
	}
	if($(pId + '.jqgrid-rownum').last()!= null){
		var lastValue = $(pId + '.jqgrid-rownum').last().text().trim();		
		if(lastValue.length > 0){
			var extWidth = 25;
			if(lastValue.length > 2){
				extWidth += (lastValue.length - 2) * 9;
			}  
			$(pId +  '.jqgfirstrow td').first().css('width',extWidth);
			$(pId + 'tr[role=rowheader] th').first().css('width',extWidth);
		}
	}
}
function updateRownumWidthForDataGrid(parentId){	
	var pId = '';
	if(parentId!= null && parentId!= undefined){
		pId = parentId + ' ';
	}
	var lastValue=$(pId + '.datagrid-cell-rownumber').last().text().trim().length;
	if(lastValue > 0){
		var extWidth = 25;
		if(lastValue > 2){
			extWidth += (lastValue - 2) * 9;
		}  
		$(pId + '.datagrid-cell-rownumber').css('width',extWidth);
		$(pId + '.datagrid-header-rownumber').css('width',extWidth);
	}
}
function escapeSpecialChar(content){
	if(content!= ""){
		content += "";
		content = content.replace(/\'/g,"&apos;").replace(/\"/g,"&quot;").replace(/</g,"&lt;").replace(/>/g,"&gt;").replace(/>/g,"&amp;");
	}
	return content;
}

function convertDate(txtDate, separator){
	// if separator is not defined then set '/'
    if (separator == undefined) {
        separator = '/';
    }
    // split input date to month, day and year
    aoDate = txtDate.split(separator);
    // array length should be exactly 3 (no more no less)
    if (aoDate.length !== 3) {
        return false;
    }
    // define month, day and year from array (expected format is m/d/yyyy)
    // subtraction will cast variables to integer implicitly
    day = aoDate[0] - 0; // because months in JS start from 0
    month = aoDate[1] - 1;
    year = aoDate[2] - 0;
    // test year range
    if (year < 1000 || year > 3000) {
        return null;
    }
    // convert input date to milliseconds
    ms = (new Date(year, month, day)).getTime();
    // initialize Date() object from milliseconds (reuse aoDate variable)
    aoDate = new Date();
    aoDate.setTime(ms);
    // compare input date and parts from Date() object
    // if difference exists then input date is not valid
    if (aoDate.getFullYear() !== year ||
        aoDate.getMonth() !== month ||
        aoDate.getDate() !== day) {
        return null;
    }
    // date is OK, return true
    return aoDate;
}
function toDateString(d){
	var dStr= (d.getDate().toString().length>1?d.getDate():'0'+d.getDate())+'/'+((d.getMonth()+1).toString().length>1?(d.getMonth()+1):'0'+(d.getMonth()+1))+'/'+d.getFullYear();
	return dStr;
}
function getNumWeekOfYear(txtDate){
	var date = convertDate(txtDate, '/');
	return getWeekOfYear(date);
}
function toMonthYearString(d){
	var dStr= ((d.getMonth()+1).toString().length>1?(d.getMonth()+1):'0'+(d.getMonth()+1))+'/'+d.getFullYear();
	return dStr;
}
function getWeekOfYear(d) {
	// Create a copy of this date object
	var target = new Date(d.valueOf());
	console.log(target);
	// ISO week date weeks start on monday
	// so correct the day number
	var dayNr = (d.getDay() + 6) % 7;
	console.log(target);
	// Set the target to the thursday of this week so the
	// target date is in the right year
	target.setDate(target.getDate() - dayNr + 3);
	// ISO 8601 states that week 1 is the week
	// with january 4th in it
	var jan4 = new Date(target.getFullYear(), 0, 4);
	// Number of days between target date and january 4th
	var dayDiff = (target - jan4) / 86400000;
	// Calculate week number: Week 1 (january 4th) plus the
	// number of weeks between target date and january 4th
	var weekNr = 1 + Math.ceil(dayDiff / 7);
	return weekNr;
}
function loadDataForStaffTree(url,treeId){
	var tId = 'tree';
	if(treeId!= null && treeId!= undefined){
		tId = treeId;
	}
	$('#' + tId).jstree({
        "plugins": ["themes", "json_data","ui"],
        "themes": {
            "theme": "classic",
            "icons": false,
            "dots": true
        },
        "json_data": {
        	"ajax" : {
                "url" : '/rest/report/staff-for-shop/tree/'+$('#shopId').val()+'.json',
                "data" : function (n) {
                        return { id : n.attr ? n.attr("id") : 0, lhl: !$('#cbLHL').is(':disabled') && $('#cbLHL').is(':checked')?1:0 };
                    }
            }        	
        }
	});	
}
function getFirstDayOfWeek(week,year){
	var dateTmp;
	if(week == null || week == undefined || week ==''){
		return '';
	}
	if(year == null || year == undefined || year == ''){
		var currentDate =new Date();
		dateTmp = new Date (currentDate.getFullYear(),0,1);
	} else{
		dateTmp = new Date(year.toString(),0,1); // toString first so it
													// parses correctly year
													// numbers
	}
	var daysToFirstDay = (1 - dateTmp.getDay()); // Note that this can be
													// also negative
	var firstDayOfFirstWeek = new Date(dateTmp.getTime() + daysToFirstDay * 86400000);
	var firstDate = new Date(firstDayOfFirstWeek.getTime() + (7 * (week - 1) * 86400000));
	return toDateString(firstDate);
};
//@author hunglm16; @since April 14, 2014; @description Lay ngay so voi ngay hien bang khoang cach ngay (-: ngay truoc; +: ngay sau) 
function getNextBySysDateForNumber(number){
	var dateTmp;
	var result = null;
	if(number == null || number == undefined || number ==''){
		number = 0;
	}
	var currentDate = new Date();
	if(number > 0 || number <0){
		result = new Date(currentDate.getTime() + number* 86400000);
	}else{
		var kq = Utils.currentDate();
		return kq.toString();
	}
	return toDateString(result);
};

function hrefPost(URL, PARAMS) {
    var temp=document.createElement("form");
    temp.action=URL;
    temp.method="POST";
    temp.style.display="none";
    for(var x in PARAMS) {
          var opt=document.createElement("textarea");
          opt.name=x;
          opt.value=PARAMS[x];
          temp.appendChild(opt);
    }
    document.body.appendChild(temp);
    temp.submit();
    return temp;
};
var MaskManager= {
	maskDate: function(selector){
		$(selector).mask("99/99/9999");
	},
	maskMonth: function(selector){
		$(selector).mask("99/9999");
	}	
};
function applyMonthPicker(selector) {
	var date = new Date();
	var cYear = date.getFullYear();
	var options = {
	    selectedYear: cYear,
	    startYear: cYear - 3,
	    finalYear: cYear + 7,
	    openOnFocus: true,
	    //monthNames: ['T1', 'T2', 'T3', 'T4', 'T5', 'T6', 'T7', 'T8', 'T9', 'T10', 'T11', 'T12']
	    monthNames: [jsp_common_thang_mot, jsp_common_thang_hai, jsp_common_thang_ba, jsp_common_thang_bon, jsp_common_thang_nam, jsp_common_thang_sau, jsp_common_thang_bay, jsp_common_thang_tam, jsp_common_thang_chin, jsp_common_thang_muoi, jsp_common_thang_muoimot, jsp_common_thang_muoihai]
	};
	
	var addHtml = '<a href="javascript:void(0);" class="CalendarLink"><img src="/resources/images/icon_calendar.jpg" width="15" height="16" /></a>';
	$('#'+selector).after(addHtml);
	$('#'+selector).monthpicker(options);
	$('#'+selector).monthpicker().bind('monthpicker-change-year', function (e, year) {
	});
	$('#'+selector).next().bind('click', function () {
		$('#'+selector).monthpicker('show');
	});
};
/**vuongmq; add them selector of nam;
 * selector: id cua input,
 * number: so nam cong them
 * flag: load lai khi mac dinh click lai dialog
 * */
function applyMonthPickerDialog(selector, numberYear, flag) {
	var date = new Date();
	if(numberYear == undefined || numberYear == null){
		numberYear = 0;
	}
	var cYear = date.getFullYear() + numberYear;
	var options = {
	    selectedYear: cYear,
	    startYear: cYear - 3,
	    finalYear: cYear + 7,
	    openOnFocus: true,
	    //monthNames: ['T1', 'T2', 'T3', 'T4', 'T5', 'T6', 'T7', 'T8', 'T9', 'T10', 'T11', 'T12']
	    monthNames: [jsp_common_thang_mot, jsp_common_thang_hai, jsp_common_thang_ba, jsp_common_thang_bon, jsp_common_thang_nam, jsp_common_thang_sau, jsp_common_thang_bay, jsp_common_thang_tam, jsp_common_thang_chin, jsp_common_thang_muoi, jsp_common_thang_muoimot, jsp_common_thang_muoihai]
	};
	if(flag == undefined || (flag != null && flag == true )){
		var addHtml = '<a href="javascript:void(0);" class="CalendarLink"><img src="/resources/images/icon_calendar.jpg" width="15" height="16" /></a>';
		$('#'+selector).after(addHtml);
	}
	$('#'+selector).monthpicker(options);
	$('#'+selector).monthpicker().bind('monthpicker-change-year', function (e, year) {
	});
	$('#'+selector).next().bind('click', function () {
		$('#'+selector).monthpicker('show');
	});
};
function toTimeString(d){
	var isOpera = !!window.opera || navigator.userAgent.indexOf(' OPR/') >= 0;
    // Opera 8.0+ (UA detection to detect Blink/v8-powered Opera)
	var isFirefox = typeof InstallTrigger !== 'undefined';   // Firefox 1.0+
	var isSafari = Object.prototype.toString.call(window.HTMLElement).indexOf('Constructor') > 0;
    // At least Safari 3+: "[object HTMLElementConstructor]"
	var isChrome = !!window.chrome && !isOpera;              // Chrome 1+
	var isIE = /*@cc_on!@*/false || !!document.documentMode; // At least IE6

	var dStr = '';
	
	if(isFirefox) {
		dStr = (d.getHours().toString().length>1?d.getHours():'0'+d.getHours())+':'+(d.getMinutes().toString().length>1?d.getMinutes():'0'+d.getMinutes());
	} else {
		dStr= (d.getUTCHours().toString().length>1?d.getUTCHours():'0'+d.getUTCHours())+':'+(d.getUTCMinutes().toString().length>1?d.getUTCMinutes():'0'+d.getUTCMinutes());
	}
	
	return dStr;
}
//quan ly mau bao cao: reportManagerPage.jsp
function formatStringUrl (str) {
	var a= str.length -1;
	for(var i = str.length ; i > 0 ; i--) {
	    if(str[a] == '/') {
	        break;
	    }
	    a--;
	}
	return str.substring(a+1,str.length);
}

function getFormatterControlGrid(id, data){
	if(_isFullPrivilege){
		return data;
	}else{
		var status = parseInt(_MapControl.get(id));
		if(status == 1){
			return '';
		}else if(status == 2){
			return data;
		}else if(status == 3){
			return '';
		}
	}
}

function showIconAddOfFirstControlGrid(idControl, idAddIconControl){
	if(_isFullPrivilege){
		$('#' + idAddIconControl).show();
	}else{
		var status = parseInt(_MapControl.get(idControl));
		if(status == 1){
			$('#' + idAddIconControl).hide();
		}else if(status == 2){
			$('#' + idAddIconControl).show();
		}else if(status == 3){
			$('#' + idAddIconControl).hide();
		}
	}
}

/**
 * @author sangtn
 * @description Kiểm tra control này có phân quyền enable hay ko, nếu có thì được phép enable, nếu ko thì ko thay đổi
 * @params: 
 * @note Hàm này để thay thế hàm enable hiện tại
 * */
function enableWidthAuthorize(idControl){
	if(_MapControl != undefined && _MapControl != null && _MapControl.valArray.length > 0){
		if(_MapControl.get(idControl) != null){
			var status = parseInt(_MapControl.get(idControl));
			if(status == 1){				
				
			}else if(status == 2){
				enable(idControl);
			}else if(status == 3){
				
			}
		}else{
			enable(idControl); 
		}
	}
}

var sortArray = function(arr){
	//Sap xep bang thuat toan nhi phan Quicksort
	//@author HungLM16; @retunr Array; @description Sap xep ASC
	var quickSort = function(arr){
		var left = [];
		var right = [];

		if(!$.isArray(arr)){
			return "yêu cầu nhập và phải là mảng Array";
		}

		if (arr.length <= 1) return arr;

        var pivot = arr[0];
        arr.shift();
        for (var i = 0; i < arr.length; i++) {
            arr[i] <= pivot ? left.push(arr[i]) : right.push(arr[i]);
        }
        return $.merge($.merge(quickSort(left), $.makeArray(pivot)), quickSort(right));
    };

    return {
        quickSort: quickSort
    };
    
    //cach goi ham : var kq = sortArray.quickSort(arr);
}();

/**
 * copy all data-properties from source-object to destination-object
 * @author tuannd20
 * @param source
 * @param destination
 * @returns
 * @since 26/09/2014
 */
function copyObjectProperties(source, destination) {
	if (source !== null && source !== undefined) {
		for (var p in source) {
			destination[p] = source[p];
		}
	} else {
		destination = source;
	}
}

function convertToSimpleObject(out, obj, prefix) {
	if (obj instanceof Array){
        for (var index=0; index < obj.length; index++){
            var item = obj[index];
            var tmpPrefix = prefix + "[" + index + "]";
            if (item instanceof Array || item instanceof Object){
                arguments.callee(out, item, tmpPrefix);
            } else {
                out[tmpPrefix] = item;
            }
        }
    } else if (obj instanceof Object){
        for(var propName in obj){	            
            var tmpPrefix = prefix + "." + propName;
            if (!(obj[propName] instanceof Array || obj[propName] instanceof Object)){
                out[tmpPrefix] = obj[propName];
            } else {
                arguments.callee(out, obj[propName], tmpPrefix);
            }
        }
    }
}	

function convertToSimpleObject(out, obj, prefix){
    if (obj instanceof Array){
        for (var index=0; index < obj.length; index++){
            var item = obj[index];
            var tmpPrefix = prefix + "[" + index + "]";
            if (item instanceof Array || item instanceof Object){
                arguments.callee(out, item, tmpPrefix);
            } else {
                out[tmpPrefix] = item;
            }
        }
    } else if (obj instanceof Object){
        for(var propName in obj){
			if (propName.toString() !== Utils.VAR_NAME){
				var tmpPrefix = prefix + "." + propName;
				if (!(obj[propName] instanceof Array || obj[propName] instanceof Object)){
					out[tmpPrefix] = obj[propName];
				} else {
					arguments.callee(out, obj[propName], tmpPrefix);
				}
			}
        }
    }
}

function getPrefix(data){
	var prefix = null;
	try{
		data.hasOwnProperty(Utils.VAR_NAME);
	} catch(e){
		throw 'Please check your data or browser support.';
	}
	if (data.hasOwnProperty(Utils.VAR_NAME)){
		try{
			prefix = data[Utils.VAR_NAME];
			delete data[Utils.VAR_NAME];
		} catch(e){
		}
	} else {
		throw 'Missing "_varname_". Please check your data.';
	}
	if (prefix === null || prefix === undefined || prefix.toString().trim().length === 0){
		throw 'Missing "_varname_". Please check your data.';
	}
	return prefix;
}

function getSimpleObject(data){
	var prefix = getPrefix(data);
	var out = {};
	convertToSimpleObject(out, data, prefix);
	return out;
}
function formatString(str, arr) {
	if(	arr instanceof Array && arr.length != 0){
		for(var i = 0 ; i < arr.length ; i++) {
			str = str.replace('{' + i + '}', arr[i]);
		}
	} else {
		str = str.replace('{0}', arr);
	}
	return str;
}
function hideAllMessage(){//trungtm6
	$('.ErrorMsgStyle,.SuccessMsgStyle').each(function(index){
		$(this).html('').hide();
	});
}
function previewImportExcelFile(fileObj,formId,inputText){//tientv11
	// if($('#btnImport').length != 0) {
	// disabled('btnImport');
	// }
		var inputId = 'fakefilepc';
		if(inputText != null || inputText != undefined){
			inputId = inputText;
		}
		var fileTypes=["xls","xslx"];
		if (fileObj.value != '') {
			if (!/(\.xls|\.xlsx)$/i.test(fileObj.value)) {
				$('#errExcelMsg').html(msgErr_excel_file_invalid_type).show();
				fileObj.value = '';
				fileObj.focus();
				document.getElementById(inputId).value ='';			
				return false;
			} else {
				$('#errExcelMsg').html('').hide();
				var src = '';
				if ($.browser.msie){
				    var path = encodeURI(what.value);
				    path = path.replace("C:%5Cfakepath%5C","");
				    var path = decodeURI(path);
				    document.getElementById(inputId).value = path;
				}else{
					document.getElementById(inputId).value = fileObj.value;
				}	
				// $('#' + formId).submit();
				// return true;
			}
		}else{
			$('#errExcelMsg').html('Vui lòng chọn file Excel').show();		
			return false;
		}
		return true;
}

/**
 * Thay the ki tu ' thanh \', " thanh \"
 *
 * @author lacnv1
 * @param val - gia tri can escape
 * @since Nov 23, 2015
 */
function escapeQuot(val) {
	if (val == null || val.trim().length == 0) {
		return val;
	}
	return val.replace(/'/g, "\\'").replace(/"/g, "\\\"");
}
/*
 * END OF FILE - /web/web/resources/scripts/utils/vnm.utils.js
 */

/*
 * START OF FILE - /web/web/resources/scripts/utils/vnm.tree-utils.js
 */
/**
 * 
 */
var TreeUtils = {
	/*****************************	COMBO TREE ****************************************/
	/**
	 * @see COMBO CAY DON VI
	*/
		
	loadComboTreeShop: function(controlId, storeCode,defaultValue,callback,params,returnCode,isTitle){		 
		var _url= '/rest/catalog/shop/combotree/1/0.json';
		if(isTitle!=undefined && isTitle!=null && isTitle == true){
			_url= '/rest/catalog/shop/combotree/1/1/0.json';
		}
		$('#' + controlId).combotree({url: _url,
			formatter: function(node) {
				return Utils.XSSEncode(node.text);
			},
			onChange: function(newValue,oldValue){
				if(params != null && params.length >0){
					for(var i=0;i<params.length;i++){
						$('#'+params[i]).val('');
					}
				}
			}
		});
		var t = $('#'+ controlId).combotree('tree');
		t.tree('options').url = '';
		t.tree({			
			formatter: function(node) {
				return Utils.XSSEncode(node.text);
			},
			onSelect:function(node){
				var data = new Object();
				if(node.attributes != null) {
					data = node.attributes.shop;
				} else {
					data.id= node.id;
				}
				if(callback!=undefined && callback!=null){
					callback.call(this, data);
				}
				if(returnCode) {
					$('#'+storeCode).val((data.shopCode == null || data.shopCode == undefined) ? '' : Utils.XSSEncode(data.shopCode));
				} else {
					$('#'+storeCode).val(node.id);	
				}	
				$('#'+controlId).combotree('setValue', Utils.XSSEncode(data.shopCode));
			},
			onBeforeExpand:function(result){	            					
				var nodes = t.tree('getChildren',result.target);
				for(var i=0;i<nodes.length;++i){
					t.tree('remove',nodes[i].target);
				}
				var ___jaxurl = '/rest/catalog/shop/combotree/0/2/';
				$.ajax({
					type : "POST",
					url : ___jaxurl + result.id +'.json',					
					dataType : "json",
					success : function(r) {						
						$(r).each(function(i,e){
							e.text = Utils.XSSEncode(e.text);
						});
						t.tree('append',{
							parent:result.target,
							data:r
						});
						var node=t.tree('getNode',result.target);
						$(node.target).next().css('display','block');
					}
				});
			},
			onExpand: function(result){	
			}
		});
		if(defaultValue == undefined || defaultValue == null){
			$('#' + controlId).combotree('setValue',activeType.ALL);
		} else {
			$('#' + controlId).combotree('setValue',defaultValue);
		}
	},
	loadComboTreeShopHasTitle: function(controlId, storeCode,callback, returnCode){
		TreeUtils.loadComboTreeShop(controlId, storeCode, null, callback, null, returnCode, true);
	},
	
	/**
	 * @see CAY DIA BAN
	*/
	loadComboTreeArea: function(controlId, storeCode,defaultValue,callback,params,isTitle){		 
		var _url= '/rest/catalog/area/combotree/0/0.json';
		if(isTitle!=undefined && isTitle!=null && isTitle == true){
			_url= '/rest/catalog/area/combotree/1/0.json';
		}
		$('#' + controlId).combotree({url: _url,
			formatter: function(node) {
				return Utils.XSSEncode(node.text);
			},
			onChange: function(newValue,oldValue){
				if(params != null && params.length >0){
					for(var i=0;i<params.length;i++){
						$('#'+params[i]).val('');
					}
				}
			}
		});
		var t = $('#'+ controlId).combotree('tree');	
		t.tree('options').url = '';
		t.tree({			
			onSelect:function(node){
				var data = new Object();
				if(node.attributes != null) {
					data = node.attributes.area;
				} else {
					data.id= node.id;
				}
				if(callback!=undefined && callback!=null){
					callback.call(this, data);
				}
				$('#'+storeCode).val(node.id);				
			},	            				
			onBeforeExpand:function(result){	            					
				var nodes = t.tree('getChildren',result.target);
				for(var i=0;i<nodes.length;++i){
					t.tree('remove',nodes[i].target);
				}
				var ___jaxurl = '/rest/catalog/area/combotree/0/';
				$.ajax({
					type : "POST",
					url : ___jaxurl + result.id +'.json',					
					dataType : "json",
					success : function(r) {						
						$(r).each(function(i,e){
							e.text = Utils.XSSEncode(e.text);
						});
						t.tree('append',{
							parent:result.target,
							data:r
						});
						var node=t.tree('getNode',result.target);
						$(node.target).next().css('display','block');
					}
				});
			},
			onExpand: function(result){	
			}
		});
		if(defaultValue == undefined || defaultValue == null){
			$('#' + controlId).combotree('setValue',activeType.ALL);
		} else {
			$('#' + controlId).combotree('setValue',defaultValue);
		}
	},
	loadComboTreeAreaHasTitle: function(controlId, storeCode,defaultValue,callback){
		TreeUtils.loadComboTreeArea(controlId, storeCode, defaultValue, callback, null, true);
	},
	loadComboTreeAreaForUnitTree: function(controlId, storeCode,defaultValue,callback,params,isTitle,callbackSuccess){	
		var _urlLstAreaId ='';
		if(defaultValue == undefined || defaultValue == null){
			_urlLstAreaId = '/rest/catalog/area/combotree/0/list.json';
		} else {
			_urlLstAreaId =  '/rest/catalog/area/combotree/'+ defaultValue +'/list.json';
		}
		$.getJSON(_urlLstAreaId, function(lstAreaId){
			TreeUtils._lstAreaId = new Array();
			if(lstAreaId.length > 0){
				for(var i = 0; i<lstAreaId.length; ++i) {
					TreeUtils._lstAreaId.push(lstAreaId[i]);
				}
			}
			var _url= '/rest/catalog/area/combotree/0/0.json';
			if(isTitle!=undefined && isTitle!=null && isTitle == true){
				_url= '/rest/catalog/area/combotree/1/0.json';
			}
			$('#' + controlId).combotree({url: _url,
				formatter: function(node) {
					return Utils.XSSEncode(node.text);
				},
				onChange: function(newValue,oldValue){
					if(params != null && params.length >0){
						for(var i=0;i<params.length;i++){
							$('#'+params[i]).val('');
						}
					}
					if(UnitTreeCatalog._isLoadArea){
						UnitTreeCatalog._isLoadArea=false;
					}else{
						UnitTreeCatalog._isChange=true;
					}
				}
			});
			var t = $('#'+ controlId).combotree('tree');	
			t.tree('options').url = '';
			t.tree({
				lines: true,
				onSelect:function(node){
					var data = new Object();
					if(node.attributes != null) {
						data = node.attributes.area;
					} else {
						data.id= node.id;
					}
					if(callback!=undefined && callback!=null){
						callback.call(this, data);
					}
					$('#'+storeCode).val(node.id);		
				},	            				
				onBeforeExpand:function(result){	            					
					var nodes = t.tree('getChildren',result.target);
					for(var i=0;i<nodes.length;++i){
						t.tree('remove',nodes[i].target);
					}
					var ___jaxurl = '/rest/catalog/area/combotree/0/';
					$.ajax({
						type : "POST",
						url : ___jaxurl + result.id +'.json',					
						dataType : "json",
						success : function(r) {						
							$(r).each(function(i,e){
								e.text = Utils.XSSEncode(e.text);
							});
							t.tree('append',{
								parent:result.target,
								data:r
							});
							var node=t.tree('getNode',result.target);
							$(node.target).next().css('display','block');
						}
					});
				},
				onLoadSuccess : function(node,data) {
					if(TreeUtils._lstAreaId.length> 1) {
						var node = t.tree('find', TreeUtils._lstAreaId.pop());
			    		t.tree('expand',node.target);
					} else if (TreeUtils._lstAreaId.length== 1){
						$('#' + controlId).combotree('setValue',defaultValue);
						if(callbackSuccess!=undefined && callbackSuccess!=null){
							callbackSuccess.call(this);
						}
					} else {
						$('#' + controlId).combotree('setValue',activeType.ALL);
						if(callbackSuccess!=undefined && callbackSuccess!=null){
							callbackSuccess.call(this);
						}
					}
					
				},
				onExpand: function(result){	
					
				}
			});
			
		});
	},
	loadComboTreeAreaForUnitTreeHasTitle: function(controlId, storeCode,defaultValue,callback,callbackSuccess){
		TreeUtils.loadComboTreeAreaForUnitTree(controlId, storeCode, defaultValue, callback, null, true,callbackSuccess);
	},
	
	/**
	 * @see CAY TUYEN
	*/
	isLoad :null,
	loadRoutingTree: function(controlId,storeCode, defaultValue,callback, callBackAll, shopCode){
		var str = '-1';
		if (shopCode != undefined && shopCode != null) {
			str = shopCode;
		}
		$('#'+ controlId).tree({ 
			url:'/rest/catalog/routing/' + str + '/tree.json' ,
			lines: true,
			formatter: function(node) {
				return Utils.XSSEncode(node.text);
			},
			onSelect:function(node){
				if (node !=null && node.id != '-1'){
					if(TreeUtils.isLoad == null) {
						if(callback!=undefined && callback!=null){
							callback.call(this, node.id);
						}
						if(storeCode != undefined  && storeCode != null) {
							$('#'+storeCode).val(node.id);				
						}
					} else {
						if(storeCode != undefined  && storeCode != null) {
							$('#'+storeCode).val(node.id);				
						}
						TreeUtils.isLoad = null;
					}
				} else if (callBackAll != undefined && callBackAll != null) {
					callBackAll.call(this, node);
				}
			},	 
			onLoadSuccess:function(node,data){
				if(defaultValue != undefined  && defaultValue != null) {
					var node = $('#'+ controlId).tree('find', defaultValue);
					TreeUtils.isLoad = 1;
		    		if(node!=null){
		    			 $('#'+ controlId).tree('select', node.target);
		    		}
				}
			}
		}); 
	}
};
/*
 * END OF FILE - /web/web/resources/scripts/utils/vnm.tree-utils.js
 */

/*
 * START OF FILE - /web/web/resources/scripts/utils/customSelect.jquery.js
 */
(function($){
 $.fn.extend({ 
 	customStyle : function(options) {
	  if(!$.browser.msie || ($.browser.msie&&$.browser.version>6)){
	  return this.each(function() {	  
			var currentSelected = $(this).find(':selected');
			$(this).after('<span class="CustomStyleSelectBox"><span class="CustomStyleSelectBoxInner">'+currentSelected.text()+'</span></span>').css({opacity:0});
			$(this).keypress(function(e){
				  if (e.keyCode>= 37 && e.keyCode  <= 40) {
						$(this).trigger('change');
				  }
			});
			var selectBoxSpan = $(this).next();		
			var selectBoxSpanInner = selectBoxSpan.find(':first-child');
			selectBoxSpan.css({display:'inline-block'});
			selectBoxSpanInner.css({display:'inline-block'});
			var selectBoxHeight = parseInt(selectBoxSpan.height()) + parseInt(selectBoxSpan.css('padding-top')) + parseInt(selectBoxSpan.css('padding-bottom'));
			$(this).height(selectBoxHeight).change(function(){
				// selectBoxSpanInner.text($(this).val()).parent().addClass('changed');   This was not ideal
			selectBoxSpanInner.text($(this).find(':selected').text()).parent().addClass('Changed');
				// Thanks to Juarez Filho & PaddyMurphy
			});
			
	  	});
	  }
	}
 });
})(jQuery);
(function($) {
    $.fn.focusToEnd = function() {
        return this.each(function() {
            var v = $(this).val();
            $(this).focus().val("").val(v);
        });
    };
})(jQuery);
/*
 * END OF FILE - /web/web/resources/scripts/utils/customSelect.jquery.js
 */

/*
 * START OF FILE - /web/web/resources/scripts/utils/vnm.gridFormatter.js
 */
var CustomerDebitAutoFormatter = {
		signedNumberFormatter:function(value,row,index) {
			if(value!=undefined && value!=null){
				if(value<0){
					return formatCurrency(value*(-1));
				}
				return formatCurrency(value);
			}
			return '';
		}
	};
var CustomerSearchEasyUIFormatter = {
		selectCellIconFormatterEasyUIDialog : function(value, rowData, rowIndex) {
			return "<a href='javascript:void(0)' onclick=\"return SPReturnOrder.getResultSeachStyle1EasyUIDialog("+ rowIndex + ");\">Chọn</a>";
		}
	};

var CommonSearchEasyUIFormatter = {
	selectSearchStyle2EasyUIDialog : function(value, rowData, rowIndex) {
		return "<a href='javascript:void(0)' onclick=\"CommonSearchEasyUI.getResultSearchStyle2EasyUIDialog("+ rowIndex + ");\">Chọn</a>";
	}
};
var PoAutoGroupFomatter = {
	statusFormatter : function(value, rowData, rowIndex) {
		if (value == activeStatus) {
			return jsp_common_status_active;
		} else if (value == stoppedStatus) {
			return jsp_common_status_stoped;
		}
		return '';
	}	
};
var CommonFormatter = {
		numberFormatter:function(cellValue, options, rowObject) {
			if(cellValue!=undefined && cellValue!=null){
				return formatCurrencyInterger(cellValue);
			}
			return '';
		},
		dateTimeFormatter:function(cellValue, options, rowObject) {
			if(cellValue!=undefined && cellValue!=null){				
				return $.datepicker.formatDate('dd/mm/yy', new Date(cellValue));
			}
			return '';
		},
		dateTimeFormatterEx:function(cellValue, options, rowObject) {
			if(cellValue!=undefined && cellValue!=null){	
				var d =  new Date(cellValue);
				var hh = d.getHours()<10? '0'+d.getHours():d.getHours();
				var mm = d.getMinutes()<10? '0'+d.getMinutes():d.getMinutes();
				var ss = d.getSeconds()<10? '0'+d.getSeconds():d.getSeconds();
				return $.datepicker.formatDate('dd/mm/yy', d) + ' ' +hh + ':' + mm + ':'+ ss;
			}
			return '';
		},
		formatNormalCell: function(val, row, index) {
			if (val != undefined && val != null && isNaN(val)) {
				return VTUtilJS.XSSEncode(val);
			} else if(val != undefined && val != null && !isNaN(val)) {
				return VTUtilJS.formatCurrency(val);
			}
			return "";
		},
		checkPermissionHideColumn: function(gridId, colunmField, idCheck) {
			if (_isFullPrivilege) {
				return;
			}
			var status = Number(_MapControl.get(idCheck));
			if(status == 1 || status == 3){
				$('td[field='+colunmField+'] .' + idCheck).remove();
				$("#"+gridId).datagrid("hideColumn", colunmField);
			}
		},
		checkPermissionHideGridButton: function(idCheck, isCssClass, prefix) {
			if (_isFullPrivilege) {
				return 0;
			}
			var t = "#";
			if (isCssClass) {
				t = ".";
			}
			if (prefix == undefined || prefix == null) {
				t = t + idCheck;
			} else {
				t = "#" + prefix + " " + t + idCheck;
			}
			var status = Number(_MapControl.get(idCheck));
			if(status == 1 || status == 3){
				$(t).remove();
				return 1;
			}
			return 0;
		}
};
var ReasonGroupCatalogFormatter = {
	editCellIconFormatter: function(cellvalue, options, rowObject){
		var status = 1;		
		if(rowObject.status != activeStatus){
			status = 0;
		}
		return "<a href='javascript:void(0)' onclick=\"return ReasonGroupCatalog.getSelectedReasonGroup("+ options.rowId + "," + status + ");\"><img src='/resources/images/icon-edit.png'/></a>";
	},
	delCellIconFormatter: function(cellvalue, options, rowObject){
		return "<a href='javascript:void(0)' onclick=\"return ReasonGroupCatalog.deleteReasonGroup('"+ Utils.XSSEncode(rowObject.reasonGroupCode) +"')\"><img src='/resources/images/icon-delete.png'/></a>";
	}
};

var GroupStaffPayrollFormatter = {
		viewDetail:function(cellValue, options, rowObject){
			if(rowObject.objectApply == 1){
				return "";
			}
			return "<span style='cursor:pointer' onclick=\"return GroupStaffPayroll.viewDetail('"+ Utils.XSSEncode(rowObject.groupStaffPrCode) + "','" + Utils.XSSEncode(rowObject.groupStaffPrName) + "');\"><img src='/resources/images/icon-view.png'/></span>";
																								//Tam thoi lay groupStaffPrCode
																								//Nhung minh nghi lay id se dung hon.
		},
		editCellIconFormatter: function(cellvalue, options, rowObject){
			/*var status = 1;		
			if(rowObject.status != activeStatus){
				status = 0;
			}*///Thuong nguoi ta edit record co trang thai "Du Thao", chu ko edit record trang thai "Hoat dong". Minh ko co nghiep vu nay. Hi hi.
			if(rowObject.isHasUsed == 0){
				return "<a href='javascript:void(0)' onclick=\"return GroupStaffPayroll.getSelectedRow("+ options.rowId + ");\"><img src='/resources/images/icon-edit.png'/></a>";
			}
			return "";
		},
		delGroupStaffPayrollFormatter: function(cellvalue, options, rowObject){
			if(rowObject.isHasUsed == 0){
				return "<a href='javascript:void(0)' onclick=\"return GroupStaffPayroll.deleteGroupStaffPayroll('"+ Utils.XSSEncode(rowObject.groupStaffPrCode) +"')\"><img src='/resources/images/icon-delete.png'/></a>";
			}
			return "";
		},
		delCellIconFormatter: function(cellvalue, options, rowObject){
			return "<a href='javascript:void(0)' onclick=\"return GroupStaffPayroll.deleteReasonGroup('"+ Utils.XSSEncode(rowObject.groupStaffPrCode) +"')\"><img src='/resources/images/icon-delete.png'/></a>";
		},
		checkInputFormatter: function(cellvalue, options, rowObject){
			return '<input type="checkbox" />';
		},
		forcusFormatter:function(cellvalue, options, rowObject){
			if(rowObject.isFocus=='0'|| rowObject.isFocus==0){
				return '<input type="checkbox"  />';
			}else{
				return '<input type="checkbox" checked="checked"  />';
			}
		},
		selectCellIconFormatter: function(cellvalue, options, rowObject){//
			return "<a href='javascript:void(0)' onclick=\"return PayrollTable.getgroupStaff('"+ Utils.XSSEncode(rowObject.groupStaffPrCode) +"');\">Chọn</a>";
		},
		formatObjectApply: function(cellvalue, options, rowObject){
			if(rowObject.objectApply == 1){
				return "Đơn vị";
			}else if(rowObject.objectApply == 2){
				return "Nhân viên";
			}
		},
		formatStaffType: function(cellvalue, options, rowObject){
			if(rowObject.channelTypeCode == null || rowObject.channelTypeCode == ""){
				return "Tất cả";
			}else{
				return Utils.XSSEncode(rowObject.channelTypeName);
			}
		}
	};


var GridFormatterUtils = {
	dateFromdateTodate :function(fromDate, toDate) {
		var kq = "";
		if(fromDate!=undefined && fromDate!=null && fromDate.length>0){
			kq = formatDate(fromDate.toString().trim());
			if(toDate!=undefined && toDate!=null && toDate.length>0){
				kq = kq + " - " + formatDate(toDate.toString().trim());
			}
		}else{
			if(toDate!=undefined && toDate!=null && toDate.length>0){
				kq = formatDate(toDate.toString().trim());
			}
		}
		return kq.trim();
	},
	statusCellIconFormatter: function(cellvalue, options, rowObject){		
		if(cellvalue == activeStatus){
			return jsp_common_status_active;
		} else if(cellvalue == stoppedStatus){
			return jsp_common_status_stoped;
		} else if(cellvalue == waitingStatus){
			return "Dự thảo";
		}
	},
	statusCellFormatter: function(cellvalue, options, rowObject){
		if(cellvalue == "ACTIVE"){
			return "Hoạt động";
		} else if(cellvalue == "ENDED"){
			return "Kết thúc";
		} else if(cellvalue == "DRAFT"){
			return "Dự thảo";
		}
	},
	currencyCellFormatter: function(cellvalue, rowObject, options){		
		if(cellvalue != null ||cellvalue != undefined ){
			return formatCurrency(cellvalue);
		}else{
			return "";
		}		
	},
	parentChannelTypeCellFormatter: function(data, row, index){
		if(row.parentChannelType != undefined && row.parentChannelType.channelTypeCode!= undefined){
			return Utils.XSSEncode(row.parentChannelType.channelTypeCode);
		} else{
			return '';
		}
	},
	staffObjectTypeCellFormatter: function(data, row, index){
		if(data!=null && data != undefined){
			if(data == 1){
				return "PreSale";
			}else if(data == 2){
				return "VanSale";
			}else if(data == 3){
				return "NVTT";
			}else if(data == 4){
				return "NVGH";
			}else if(data == 5){
				return "NVGS";
			}else if(data == 6){
				return "TBHM";
			}else if(data == 7){
				return "TBHV";
			}
		}
	}
};
var PackingFormatter = {
	edit: function(value, row, index){
		var status = 1;		
		if(row != activeStatus){
			status = 0;
		}
		return "<a href='javascript:void(0)' onclick=\"return PackingCatalog.getSelectedPacking("+ row.id + ",'"+ Utils.XSSEncode(row.productInfoCode) + "','"+ Utils.XSSEncode(row.productInfoName) + "','"+ Utils.XSSEncode(row.description) + "'," + status + ");\"><img src='/resources/images/icon-edit.png'/></a>";
	},
	remove: function(cellvalue, options, rowObject){
		return "<a href='javascript:void(0)' onclick=\"return PackingCatalog.deletePacking('"+ Utils.XSSEncode(rowObject.productInfoCode) +"')\"><img src='/resources/images/icon-delete.png'/></a>";
	}
};
var ReasonCatalogFormatter = {
	editCellIconFormatter: function(cellvalue, options, rowObject){
		var status = 1;
		var reasonGroupId = null;
		if(rowObject.status != activeStatus){
			status = 0;
		}
		if(rowObject.reasonGroup!= undefined){
			reasonGroupId = rowObject.reasonGroup.id;
		}
		return "<a href='javascript:void(0)' onclick=\"return ReasonCatalog.getSelectedReason("+ options.rowId + "," + status + ");\"><img src='/resources/images/icon-edit.png'/></a>";
	},
	delCellIconFormatter: function(cellvalue, options, rowObject){
		return "<a href='javascript:void(0)' onclick=\"return ReasonCatalog.deleteReason('"+ Utils.XSSEncode(rowObject.reasonCode) +"')\"><img src='/resources/images/icon-delete.png'/></a>";
	}
};
var BrandCatalogFormatter = {
	editCellIconFormatter: function(value, row, index){
		var status = 1;		
		if(row.status != activeStatus){
			status = 0;
		}
		return "<a href='javascript:void(0)' onclick=\"return BrandCatalog.getSelectedBrand("+ row.id + ",'"+ Utils.XSSEncode(row.productInfoCode) + "','"+ Utils.XSSEncode(row.productInfoName) + "','"+ Utils.XSSEncode(row.description) + "'," + status + ");\"><img src='/resources/images/icon-edit.png'/></a>";
	},
	delCellIconFormatter: function(cellvalue, options, rowObject){
		return "<a href='javascript:void(0)' onclick=\"return BrandCatalog.deleteBrand('"+ Utils.XSSEncode(rowObject.productInfoCode) +"')\"><img src='/resources/images/icon-delete.png'/></a>";
	}
};
var CategoryCatalogFormatter = {
	editCellIconFormatter: function(value, row, index){
		var status = 1;		
		if(row.status != activeStatus){
			status = 0;
		}
		return "<a href='javascript:void(0)' onclick=\"return CategoryCatalog.getSelectedCategory("+ row.id + ",'"+ Utils.XSSEncode(row.productInfoCode) + "','"+ Utils.XSSEncode(row.productInfoName) + "','"+ Utils.XSSEncode(row.description) + "'," + status + ");\"><img src='/resources/images/icon-edit.png'/></a>";
	},
	delCellIconFormatter: function(cellvalue, options, rowObject){
		return "<a href='javascript:void(0)' onclick=\"return CategoryCatalog.deleteCategory('"+ rowObject.productInfoCode +"')\"><img src='/resources/images/icon-delete.png'/></a>";
	}
};
var SubCategoryCatalogFormatter = {
		
	editCellIconFormatter: function(value, row, index){
		var status = 1;
		if(row.status != activeStatus){
			status = 0;
		}
		return "<a href='javascript:void(0)' onclick=\"return SubCategoryCatalog.getSelectedSubCategory("+ row.id + ",'"+ Utils.XSSEncode(row.productInfoCode) + "','"+ Utils.XSSEncode(row.productInfoName) + "','"+ Utils.XSSEncode(row.description) + "'," + status + ");\"><img src='/resources/images/icon-edit.png'/></a>";
	},
	delCellIconFormatter: function(cellvalue, options, rowObject){
		return "<a href='javascript:void(0)' onclick=\"return SubCategoryCatalog.deleteSubCategory('"+ Utils.XSSEncode(rowObject.productInfoCode) +"')\"><img src='/resources/images/icon-delete.png'/></a>";
	}
};
var SubCategorySecondaryFormatter = {
		editCellIconFormatter: function(value, row, index){
			var status = 1;
			if(row.status != activeStatus){
				status = 0;
			}
			var data = "<a href='javascript:void(0)' onclick=\"return SubCategorySecondary.getSelectedSubCategorySecondary("+ row.productInfoId + ",'"+ Utils.XSSEncode(row.parentCode) + "','"+ Utils.XSSEncode(row.productInfoCode) + "','"+ Utils.XSSEncode(row.productInfoName) + "'," + status + ");\"><img src='/resources/images/icon-edit.png'/></a>";
			return getFormatterControlGrid('btnEditGrid',data);
			//return "<a href='javascript:void(0)' onclick=\"return SubCategorySecondary.getSelectedSubCategorySecondary("+ row.productInfoId + ",'"+ row.parentCode + "','"+ row.productInfoCode + "','"+ row.productInfoName + "'," + status + ");\"><img src='/resources/images/icon-edit.png'/></a>";
		}
	};
var FlavourCatalogFormatter = {
	editCellIconFormatter: function(value, row, index){
		var status = 1;		
		if(row.status != activeStatus){
			status = 0;
		}
		return "<a href='javascript:void(0)' onclick=\"return FlavourCatalog.getSelectedRow("+ row.id + ",'"+ Utils.XSSEncode(row.productInfoCode) + "','"+ Utils.XSSEncode(row.productInfoName) + "','"+ Utils.XSSEncode(row.description) + "'," + status + ");\"><img src='/resources/images/icon-edit.png'/></a>";
	},
	delCellIconFormatter: function(cellvalue, options, rowObject){
		return "<a href='javascript:void(0)' onclick=\"return FlavourCatalog.deleteRow('"+ Utils.XSSEncode(rowObject.productInfoCode) +"')\"><img src='/resources/images/icon-delete.png'/></a>";
	}
};
var contractManagerFormatter = {
		editCellIconFormatter: function(cellvalue, options, rowObject){
			var status;
			var contractType;
			if (rowObject.contractType == "DEVICE_CONTRACT") {
				contractType = 1;
			} else {
				contractType = 0;
			}
			if(rowObject.status == "DRAFT"){
				status = 0;
				return "<a href='javascript:void(0)' " +
						"onclick=\"return ContractManagers.initContractDetail("+ options.rowId + ");\"><img src='/resources/images/icon-edit.png'/></a>";
			} else {
				if (rowObject.status == "ACTIVE") {
					status = 1;
				} else {
					status = 2;
				}
				return "<a href='javascript:void(0)' " +
				"onclick=\"return ContractManagers.initContractDetail("+ options.rowId +");\"><img src='/resources/images/icon-edit.png'/></a>";
			} 
		},
		customerFormatter: function(cellvalue, options, rowObject){
			if(rowObject.customer == null) {
				return "";
			} else if(rowObject.customer.shortCode == null && rowObject.customer.customerName != null) {
				return Utils.XSSEncode(rowObject.customer.customerName);
			} else if(rowObject.customer.shortCode != null && rowObject.customer.customerName == null) {
				return Utils.XSSEncode(rowObject.customer.shortCode);
			} else {
				return Utils.XSSEncode(rowObject.customer.shortCode + "-" + rowObject.customer.customerName);
			}
		},
		staffFormatter: function(cellvalue, options, rowObject){
			if(rowObject.staff == null) {
				return "";
			} else if(rowObject.staff.staffCode == null && rowObject.staff.staffName != null) {
				return Utils.XSSEncode(rowObject.staff.staffName);
			} else if(rowObject.staff.staffCode != null && rowObject.staff.staffName == null) {
				return Utils.XSSEncode(rowObject.staff.staffCode);
			} else {
				return Utils.XSSEncode(rowObject.staff.staffCode) + "-" + "<br>" + Utils.XSSEncode(rowObject.staff.staffName);
			}
		},
		delCellIconFormatter: function(cellvalue, options, rowObject){
			if (rowObject.status == "DRAFT") {
				return "<a href='javascript:void(0)' onclick=\"return ContractManagers.deleteRow('"+ rowObject.id +"')\"><img src='/resources/images/icon-delete.png'/></a>";
			} else {
				return "";
			}
		}
	};
var UnitTreeFormatter = {
	editCellIconFormatter: function(cellvalue, options, rowObject){
		var status = 1;		
		if((rowObject != null || rowObject != undefined) && (rowObject.parentShop != null || rowObject.parentShop != undefined) &&rowObject.parentShop.status == "STOPPED") {
			return "";
		}
		if(rowObject.status != activeStatus){
			status = 0;
		}
		var parentShopId = -1;
		var parentName = '';
		if(rowObject.parentShop == null ||rowObject.parentShop == undefined ){
			parentShopId = -1;
			parentName = '';
		}else{
			parentShopId = rowObject.parentShop.id;
			parentName =  rowObject.parentShop.shopName;
		}
		
		var unitType = -1;
		if(rowObject.type != null && rowObject.type != undefined){
			unitType = rowObject.type.id;
		}
		var provinceCode = '';
		var districtCode = '';
		var wardCode = '';
		if(rowObject.area != null && rowObject.area != undefined){
			provinceCode = rowObject.area.province;
			districtCode = rowObject.area.district;
			wardCode = rowObject.area.precinct;
		}
		if(rowObject.shipTo!=null && rowObject.shipTo!=undefined){
			rowObject.shipTo = rowObject.shipTo.replace(/'/g,"\\'");
		}
		return "<a href='javascript:void(0)' onclick=\"return UnitTreeCatalog.getSelectedUnit("+ rowObject.id + ",'"+ Utils.XSSEncode(rowObject.shopCode) + "'," + parentShopId + ",'" + Utils.XSSEncode(rowObject.shopName) + "','"+unitType+"'," + status + ",'"+ Utils.XSSEncode(rowObject.taxNum) + "','" + Utils.XSSEncode(rowObject.invoiceNumberAccount) + "','" + Utils.XSSEncode(rowObject.invoiceBankName) + "','" + Utils.XSSEncode(rowObject.billTo) + "','" + Utils.XSSEncode(rowObject.phone) + "','" + Utils.XSSEncode(rowObject.mobiphone) + "','" + Utils.XSSEncode(rowObject.email) + "','" 
		+ Utils.XSSEncode(rowObject.shipTo) + "','" + Utils.XSSEncode(rowObject.contactName) + "','" + Utils.XSSEncode(rowObject.fax) + "','" + Utils.XSSEncode(provinceCode) + "','" + Utils.XSSEncode(districtCode) + "','" + Utils.XSSEncode(wardCode) + "','" + Utils.XSSEncode(rowObject.address) + "','" + rowObject.lat + "','" + rowObject.lng 
		+ "','" + Utils.XSSEncode(parentName) + "');\"><img src='/resources/images/icon-edit.png'/></a>";
	},
	addCellIconFormatter: function(cellvalue, options, rowObject){
		var channelTypeCode = '';
		if((rowObject != null || rowObject != undefined) && (rowObject.parentShop != null || rowObject.parentShop != undefined) &&rowObject.parentShop.status == "STOPPED") {
			return "";
		}
		if(rowObject.type != null && rowObject.type.channelTypeCode != null){
			channelTypeCode = rowObject.type.channelTypeCode;
		}
		var parentShopId = '';
		var parentName = '';
		if(rowObject.parentShop == null ||rowObject.parentShop == undefined ){
			parentShopId = rowObject.id;
			parentName = rowObject.shopName;
		}else{
			parentShopId = rowObject.parentShop.id;
			parentName =  rowObject.parentShop.shopName;
		}
		if(rowObject.status != activeStatus){
			return "";
		}else{
			return "<a href='javascript:void(0)' onclick=\"return UnitTreeCatalog.addChildShop("+ rowObject.id + ",'"+ Utils.XSSEncode(rowObject.shopCode) + "','"+ Utils.XSSEncode(channelTypeCode) +"',"+ Utils.XSSEncode(parentShopId) +",'"+ Utils.XSSEncode(parentName)+"','"+ Utils.XSSEncode(rowObject.shopName) +"');\"><img src='/resources/images/icon-add.png'/></a>";
		}
	},
	unitTypeFormatter: function(cellvalue, options, rowObject){
		if(rowObject.type != null && rowObject.type != undefined && rowObject.type.channelTypeCode != null){
			if(rowObject.type.channelTypeCode == 'VNM'){
				return "Nutifood";
			}else if(rowObject.type.channelTypeCode == 'MIEN'){
				return "Miền";
			}else if(rowObject.type.channelTypeCode == 'VUNG'){
				return "Vùng";
			}else if(rowObject.type.channelTypeCode == 'NPP'){
				return "NPP";
			}
		}else{
			return "";
		}
	},
	actionTypeFormatter:function(cellvalue, options, rowObject){
		var content = '';
		if(rowObject.actionType == 'INSERT'){
			return 'Tạo mới ';
		} else if(rowObject.actionType == 'UPDATE'){
			return 'Cập nhật';
		} else if(rowObject.actionType == 'DELETE'){
			return 'Xóa';
		}				
		return cellvalue;
	},
	viewDetail: function(cellvalue, options, rowObject){
		return '<a href="javascript:void(0)" onclick="return UnitTreeCatalog.searchChangeInfoDetail('+rowObject.id+');" ><span style="cursor:pointer"><img src="/resources/images/icon-view.png"></span></a>';
	},
	
	//NEW
	backMainPage : function(value, rowData, rowIndex) {
		return '<a href="javascript:void(0)" onclick="UnitTreeCatalog.showCollapseTab(-1,'+rowData.id+')"><img src="/resources/images/icon-view.png" width="15" height="16"></a>';
	},
	statusShopFormatter :function(value, rowData, rowIndex){
		if(value == activeStatus){
			return jsp_common_status_active;
		} else if(value == stoppedStatus){
			return jsp_common_status_stoped;
		} 
		return '';
	}
};
var AreaTreeFormatter = {
	editCellIconFormatter: function(value, row, index){
		AreaTreeCatalog.isUpdate = true;
		var parentId = 0;
		var parentAreaCode = '';
		var parentAreaName = '';
		if((row != null || row != undefined) && (row.parentArea != null || row.parentArea != undefined) &&row.parentArea.status == "STOPPED") {
			return "";
		}
		if(row.parentArea == null ||row.parentArea == undefined ){
			parentId = 0;
		}else{
			parentId = row.parentArea.id;
		}
		var area_name = '';		
		if(parentId!=0){
			area_name = row.parentArea.areaName;	
			parentAreaCode = row.parentArea.areaCode;
			parentAreaName = row.parentArea.areaName;
		}		
		$('#areaParentCode').attr("disabled","disabled");
		$('#areaNameParent').attr("disabled","disabled");
		var status = 1;		
		if(row.status != activeStatus){
			status = 0;
		}		
		var name = row.areaName;
		name = name.replace(/'/g,"\\'");
		row.areaCode = row.areaCode.replace(/'/g,"\\'");
		area_name = area_name.replace(/'/g,"\\'");
		return "<a href='javascript:void(0)' onclick=\"return AreaTreeCatalog.getSelectedArea("+ row.id + ",'"+ Utils.XSSEncode(row.areaCode) + "'," + parentId + ",'" + Utils.XSSEncode(name) +"','"+status+"','"+ Utils.XSSEncode(area_name) +"','"+ Utils.XSSEncode(parentAreaCode) +"','"+ Utils.XSSEncode(parentAreaName) +"');\"><img src='/resources/images/icon-edit.png'/></a>";
	},
	viewCellIconFormatter:function(cellvalue, options, rowObject){
		var value='';
		if(cellvalue != '' && cellvalue.trim().length > 12){
			value = cellvalue.substring(0,12);
			value+='...';
		}else{
			value = cellvalue;
		}
		return value;
	}
};
var SaleTypeCatalogFormatter = {
	editCellIconFormatter: function(cellvalue, options, rowObject){
		var parentShopName = -1;
		if(rowObject.parentChannelType == null ||rowObject.parentChannelType == undefined ){
			parentShopName = -1;
		}else{
			parentShopName = rowObject.parentChannelType.id;
		}
		var status = 1;		
		if(rowObject.status != activeStatus){
			status = 0;
		}
		return "<a href='javascript:void(0)' onclick=\"return SaleTypeCatalog.getSelectedStaffType("+ rowObject.id + ",'"+ Utils.XSSEncode(rowObject.channelTypeCode) + "','" + Utils.XSSEncode(parentShopName) + "','" + Utils.XSSEncode(rowObject.channelTypeName) + "','"+status+"');\"><img src='/resources/images/icon-edit.png'/></a>";
	},
	delCellIconFormatter: function(cellvalue, options, rowObject){
		return "<a href='javascript:void(0)' onclick=\"return SaleTypeCatalog.deleteStaffType('"+ Utils.XSSEncode(rowObject.channelTypeCode) +"')\"><img src='/resources/images/icon-delete.png'/></a>";
	}
};
var StaffTypeCatalogFormatter = {
		editCellIconFormatter: function(value, row, index){
			var parentShopName = -1;
			if(row.parentChannelType == null ||row.parentChannelType == undefined ){
				parentShopName = -1;
			}else{
				parentShopName = row.parentChannelType.id;
			}
			var status = 1;		
			if(row.status != activeStatus){
				status = 0;
			}
			return "<a href='javascript:void(0)' onclick=\"return StaffTypeCatalog.getSelectedStaffType("+ row.id + ",'"+ Utils.XSSEncode(row.channelTypeCode) + "','" + Utils.XSSEncode(parentShopName) + "','" + Utils.XSSEncode(row.channelTypeName) + "','"+status+"','" + Utils.XSSEncode(row.objectType) + "');\"><img src='/resources/images/icon-edit.png'/></a>";
		},
		delCellIconFormatter: function(cellvalue, options, rowObject){
			return "<a href='javascript:void(0)' onclick=\"return StaffTypeCatalog.deleteStaffType('"+ Utils.XSSEncode(rowObject.channelTypeCode) +"')\"><img src='/resources/images/icon-delete.png'/></a>";
		}
		
	};
var ProductLevelCatalogFormatter = {
		editCellIconFormatter: function(cellvalue, options, rowObject){
			var status = 1;
			if(rowObject.status != activeStatus){
				status = 0;
			}					
			return "<a href='javascript:void(0)' onclick=\"return ProductLevelCatalog.getSelectedRow("+ options.rowId + ","+ status +");\"><img src='/resources/images/icon-edit.png'/></a>";
		},
		delCellIconFormatter: function(cellvalue, options, rowObject){
			return "<a href='javascript:void(0)' onclick=\"return ProductLevelCatalog.deleteRow('"+ Utils.XSSEncode(rowObject.productLevelCode) +"')\"><img src='/resources/images/icon-delete.png'/></a>";
		}
	};
var CustomerTypeCatalogFormatter = {
	editCellIconFormatter: function(data, row, index){					
		var parentChannelTypeId = -2;		
		if(row.parentChannelType != null && row.parentChannelType != undefined){
			parentChannelTypeId = row.parentChannelType.id;
		}
		var data = "<a title= " + catalog_customer_type_edit + " href='javascript:void(0)' onclick=\"return CustomerTypeCatalog.getSelectedRow("+ row.id + ","+ parentChannelTypeId +");\"><img src='/resources/images/icon-edit.png'/></a>";
		return data;
	},
	delCellIconFormatter: function(cellvalue, options, rowObject){
		return "<a title="+action_type_status_delete+"  href='javascript:void(0)' onclick=\"return CustomerTypeCatalog.deleteRow('"+ Utils.XSSEncode(rowObject.channelTypeCode) +"')\"><img src='/resources/images/icon-delete.png'/></a>";
	}
};
var TaxTypeCatalogFormatter = {
		editCellIconFormatter: function(cellvalue, options, rowObject){
			var status = 1;		
			if(rowObject.status != activeStatus){
				status = 0;
			}
			return "<a href='javascript:void(0)' onclick=\"return TaxTypeCatalog.getSelectedRow("+ options.rowId + "," + status + ");\"><img src='/resources/images/icon-edit.png'/></a>";
		},
		delCellIconFormatter: function(cellvalue, options, rowObject){
			return "<a href='javascript:void(0)' onclick=\"return TaxTypeCatalog.deleteRow('"+ Utils.XSSEncode(rowObject.vatTypeCode) +"')\"><img src='/resources/images/icon-delete.png'/></a>";
		},
		rateCellFormatter: function(cellvalue, options, rowObject){			
			return Utils.formatDoubleValue(cellvalue) + '%';
		}
	};
var CarTypeCatalogFormatter = {
	editCellIconFormatter: function(cellvalue, rowObject,options){
		var carTrademark = '';
		if(rowObject.label == null ||rowObject.label == undefined){
			carTrademark = '';
		}else{
			carTrademark = rowObject.label.id;
		}
		var shop = '';
		if(rowObject.shop == null ||rowObject.shop == undefined ){
			shop = '';
		}else{
			shop = rowObject.shop.shopCode;
		}
		var status = '';
		if(rowObject.status == null){
			status = '';
		}else{
			if(rowObject.status == 'STOPPED'){
				status = 0;
			}else{
				status = 1;
			}
		}
		return "<a title='Sửa' href='javascript:void(0)' onclick=\"return CarTypeCatalog.getSelectedCarType("+ rowObject.id + ",'"+ Utils.XSSEncode(rowObject.carNumber) + "','" + Utils.XSSEncode(rowObject.category) + "','" + Utils.XSSEncode(rowObject.gross) + "','" + carTrademark + "','"+ Utils.XSSEncode(rowObject.origin) + "','"+ Utils.XSSEncode(shop) + "','"+ status + "','"+ rowObject.type + "');\"><img src='/resources/images/icon-edit.png'/></a>";
	},
	originFormat: function(cellvalue, rowObject,options){
		if(rowObject.origin != null ||rowObject.origin != undefined ){
			if(rowObject.origin == 'MUO' ){
				return "Xe mượn";
			}else if(rowObject.origin == 'NPP' ){
				return "Xe đơn vị";
			}
		}else{
			return "";
		}
	},
	categoryFormat: function(cellvalue, rowObject,options){
		if(rowObject.category != null ||rowObject.category != undefined ){
			if(rowObject.category == '0'){
				return "Lạnh";
			}else{
				return "Khô";
			}
		}else{
			return "";
		}
	},
	channelTypeNameFormat: function(cellvalue, rowObject,options){
		if(rowObject.label!=undefined && rowObject.label!=undefined && rowObject.label.channelTypeName!=undefined && rowObject.label.channelTypeName.length>0){
			return Utils.XSSEncode(rowObject.label.channelTypeName);
		}
	},
	grossFormat: function(cellvalue, rowObject,options){
		if(rowObject.gross != null ||rowObject.gross != undefined ){
			if(rowObject.gross == 0){
				return '';
			}else{
				return Utils.XSSEncode(rowObject.gross);
			}
		}
	},
	typeFormat: function(cellvalue, rowObject,options){
		if(rowObject.type != null ||rowObject.type != undefined ){
			if(rowObject.type == '4B' || rowObject.type == '4b'){
				return "Xe 4 bánh";
			}else if(rowObject.type == '3B' || rowObject.type == '3b'){
				return "Xe 3 bánh";
			}else{
				return "Xe 2 bánh";
			}
		}else{
			return "";
		}
	},
	statusFormat: function(cellvalue, rowObject,options){
		if(rowObject.status != null ||rowObject.status != undefined ){
			if(rowObject.status == 'STOPPED' ){
				return "Tạm ngưng";
			}else{
				return "Hoạt động";
			}
		}else{
			return "";
		}
	},
	delCellIconFormatter: function(cellvalue, rowObject,options){
		return "<a href='javascript:void(0)' onclick=\"return CarTypeCatalog.deleteCarType('"+ rowObject.id +"')\"><img src='/resources/images/icon-delete.png'/></a>";
	}
};
var DeliveryGroupCatalogFormatter = {
	editCellIconFormatter: function(cellvalue, options, rowObject){		
		return "<a href='javascript:void(0)' onclick=\"return DeliveryGroupCatalog.getSelectedRow("+ rowObject.id +");\"><img src='/resources/images/icon-edit.png'/></a>";
	},
	delCellIconFormatter: function(cellvalue, options, rowObject){
		return "<a href='javascript:void(0)' onclick=\"return DeliveryGroupCatalog.deleteRow('"+ rowObject.id +"')\"><img src='/resources/images/icon-delete.png'/></a>";
	},
	delCellIconCustomerFormatter: function(cellvalue, options, rowObject){
		return "<a href='javascript:void(0)' onclick=\"return DeliveryGroupCatalog.deleteCustomerRow('"+ rowObject.id +"')\"><img src='/resources/images/icon-delete.png'/></a>";
	}
};
var PromotionCatalogFormatter = {
	viewCellIconFormatter: function(val,row){		
		if(row.type != null && (row.type.substring(0,2) == 'ZM' || row.type.substring(0,2) == 'ZT' || row.type.substring(0,2) == 'ZD'|| row.type.substring(0,2) == 'ZH')){
			return "<a href='/catalog/promotion/viewdetail?promotionId="+row.id+"&proType=2'><span style='cursor:pointer'><img width='15' src='/resources/images/icon-edit.png'/></span></a>";
		}else{
			return "<a href='/catalog/promotion/viewdetail?promotionId="+row.id+"&proType=1'><span style='cursor:pointer'><img width='15' src='/resources/images/icon-edit.png'/></span></a>";
		}
	},
	editProductFormatter: function(cellvalue, options, rowObject){
		var freeProduct = '';
		if(rowObject.freeProduct != null || rowObject.freeProduct != undefined){
			freeProduct = rowObject.freeProduct.productCode;
		}
		var productName = '';
		var productCode = '';
		if(rowObject.product != null || rowObject.product != undefined){
			productName = rowObject.product.productName;
			productCode = rowObject.product.productCode;
		}
		if(checkPermissionEdit()){
			var statusPromotion = 1;		
			if(rowObject.promotionProgram != null && rowObject.promotionProgram.status != null && rowObject.promotionProgram.status == "WAITING"){
				statusPromotion = 2;
			}
			if(statusPromotion == 2){
				return "<a href='javascript:void(0)' onclick=\"return PromotionCatalog.getSelectedProduct('"+ rowObject.id +"','"+ Utils.XSSEncode(productCode) +"','"+ Utils.XSSEncode(productName) +"','"+ rowObject.saleQty +"','"+ rowObject.saleAmt +"','"+ rowObject.discPer +"','"+ rowObject.discAmt +"','"+ Utils.XSSEncode(freeProduct) +"','"+ rowObject.freeQty +"','"+ rowObject.required +"');\"><img src='/resources/images/icon-edit.png'/></a>";
			}else{
				return "";
			}
		}else{
			return "";
		}
	},
	delProductFormatter: function(cellvalue, options, rowObject){
		if(checkPermissionEdit()){
			var statusPromotion = 1;		
			if(rowObject.promotionProgram != null && rowObject.promotionProgram.status != null && rowObject.promotionProgram.status == "WAITING"){
				statusPromotion = 2;
			}
			if(statusPromotion == 2){
				return "<a href='javascript:void(0)' onclick=\"return PromotionCatalog.deleteProduct('"+ rowObject.id +"')\"><img src='/resources/images/icon-delete.png'/></a>";
			}else{
				return "";
			}
		}else{
			return "";
		}
	},
	editShopFormatter: function(cellvalue, options, rowObject){
		var shopCode = '';
		if(rowObject.shop == null ||rowObject.shop == undefined ){
			shopCode = '';
		}else{
			shopCode = rowObject.shop.shopCode;
		}
		var shopName = '';
		if(rowObject.shop == null ||rowObject.shop == undefined ){
			shopName = '';
		}else{
			shopName = rowObject.shop.shopName;
		}
		var status = 1;		
		if(rowObject.status != activeStatus){
			status = 0;
		}
		var value = 0;
		if(rowObject.objectApply != null || rowObject.objectApply != undefined){
			if(rowObject.objectApply == 'SHOP'){
				value = 1;
			}else if(rowObject.objectApply == 'SHOP_AND_CUSTOMER_TYPE'){
				value = 2;
			}else if(rowObject.objectApply == 'CUSTOMER'){
				value =3;
			}
		}
		if(checkPermissionEdit()){
			var statusPromotion = 1;		
			if(rowObject.promotionProgram != null && rowObject.promotionProgram.status != null&& rowObject.promotionProgram.status == "WAITING"){
				statusPromotion = 2;
			}
			if(statusPromotion == 2){
				return "<a href='javascript:void(0)' onclick=\"return PromotionCatalog.getSelectedShop('"+ rowObject.id +"','"+ Utils.XSSEncode(shopCode) +"','"+ Utils.XSSEncode(shopName) +"','"+ rowObject.quantityMax +"','"+status+"','"+value+"');\"><img src='/resources/images/icon-edit.png'/></a>";
			}else{
				return "";
			}
		}else{
			return "";
		}
	},
	delShopFormatter: function(cellvalue, options, rowObject){
		if(checkPermissionEdit()){
			var statusPromotion = 1;		
			if(rowObject.promotionProgram != null && rowObject.promotionProgram.status != null && rowObject.promotionProgram.status == "WAITING"){
				statusPromotion = 2;
			}
			if(statusPromotion == 2){
				return "<a href='javascript:void(0)' onclick=\"return PromotionCatalog.deleteShop('"+ rowObject.id +"')\"><img src='/resources/images/icon-delete.png'/></a>";
			}else{
				return "";
			}
		}else{
			return "";
		}
	},
	viewShopFormatter:function(value, row, index){
		var value = 0;
		if(row.objectApply != null || row.objectApply != undefined){
			if(row.objectApply == 'SHOP'){
				value = 1;
			}else if(row.objectApply == 'SHOP_AND_CUSTOMER_TYPE'){
				value = 2;
			}else if(row.objectApply == 'CUSTOMER'){
				value =3;
			}
		}
		var status = 1;		
		if(row.status != activeStatus){
			status = 0;
		}
		if(value == 2 || value == 3){
			return "<a href='javascript:void(0)' onclick=\"return PromotionCatalog.getDetailGridUrl('"+ row.shop.id +"','"+value+"','"+ Utils.XSSEncode(row.shop.shopName) +"','"+status+"');\"><img src='/resources/images/icon-view.png'/></a>";
		}else{
			return "";
		}
	},
	editCustomerFormatter: function(cellvalue, options, rowObject){
		if(checkPermissionEdit()){
			var statusPromotion = 1;		
			if(rowObject.promotionShopMap != null && rowObject.promotionShopMap.promotionProgram != null && rowObject.promotionShopMap.promotionProgram.status != null && rowObject.promotionShopMap.promotionProgram.status == "WAITING"){
				statusPromotion = 2;
			}
			if(statusPromotion == 2){
				return "<a href='javascript:void(0)' onclick=\"return PromotionCatalog.showDialogCustomerUpdate('"+ rowObject.id +"');\"><img src='/resources/images/icon-edit.png'/></a>";
			}else{
				return "";
			}
		}else{
			return "";
		}
	},
	delCustomerFormatter: function(cellvalue, options, rowObject){
		if(checkPermissionEdit()){
			var statusPromotion = 1;		
			if(rowObject.promotionShopMap != null && rowObject.promotionShopMap.promotionProgram != null && rowObject.promotionShopMap.promotionProgram.status != null && rowObject.promotionShopMap.promotionProgram.status == "WAITING"){
				statusPromotion = 2;
			}
			if(statusPromotion == 2){
				return "<a href='javascript:void(0)' onclick=\"return PromotionCatalog.deleteCustomer('"+ rowObject.id +"')\"><img src='/resources/images/icon-delete.png'/></a>";
			}else{
				return "";
			}
		}else{
			return "";
		}
	},
	delCustomerTypeFormatter: function(cellvalue, options, rowObject){
		if(checkPermissionEdit()){
			var statusPromotion = 1;		
			if(rowObject.promotionShopMap != null && rowObject.promotionShopMap.promotionProgram != null && rowObject.promotionShopMap.promotionProgram.status != null && rowObject.promotionShopMap.promotionProgram.status == "WAITING"){
				statusPromotion = 2;
			}
			if(statusPromotion == 2){
				return "<a href='javascript:void(0)' onclick=\"return PromotionCatalog.deleteCustomerType('"+ rowObject.id +"')\"><img src='/resources/images/icon-delete.png'/></a>";
			}else{
				return "";
			}
		}else{
			return "";
		}
	},
	moneyFormat: function(cellvalue, options, rowObject){
		if(cellvalue != null ||cellvalue != undefined ){
			return formatCurrency(cellvalue);
		}else{
			return "";
		}
	},
	statusFormat: function(val,row){
		if(row.status != null ||row.status != undefined ){
			if(row.status == 'STOPPED' ){
				return "Tạm ngưng";
			}else if(row.status == 'RUNNING'){
				return "Hoạt động";
			}else{
				return "Dự thảo";
			}
		}else{
			return "";
		}
	},
	actionTypeFormatter:function(cellvalue, options, rowObject){		
		if(rowObject.actionType == 'INSERT'){
			return 'Tạo mới ';
		} else if(rowObject.actionType == 'UPDATE'){
			return 'Cập nhật';
		} else if(rowObject.actionType == 'DELETE'){
			return 'Xóa';
		}				
		return cellvalue;
	},
	editCellIconFormatter: function(cellvalue, options, rowObject){
		var actionId = 0;
		if(rowObject == undefined || rowObject == null ) {
			return "";
		}
		if(rowObject.id == null ||rowObject.id == undefined ){
			actionId = 0;
		}else{
			actionId = rowObject.id;
		}
		return "<a href='javascript:void(0)' onclick='return PromotionCatalog.viewActionAudit("+actionId+")'><img src='/resources/images/icon-view.png'/></a>";
	},
	typeFormat: function(val,row){
		if(row.type != null ||row.type != undefined ){
			return Utils.XSSEncode(row.type) + '-' + Utils.XSSEncode(row.proFormat);
		}else{
			return "";
		}
	}
};
var ProductCatalogFormatter = {
		editCellIconFormatter: function(value,row,index){		
			return "<a href='/catalog/product/viewdetail?productId="+row.id+"'><span style='cursor:pointer'><img src='/resources/images/icon-view.png'/></span></a>";
		},
		delCellIconFormatter: function(cellvalue, options, rowObject){
			return "<a href='javascript:void(0)' onclick=\"return ProductCatalog.deleteProduct('"+ rowObject.id +"')\"><img src='/resources/images/icon-delete.png'/></a>";
		},
		editGroupFormatter: function(cellvalue, options, rowObject){
//			var departmentCode = -1;
//			if(rowObject.departmentCode != null && rowObject.departmentCode != undefined){
//				departmentCode = rowObject.department.id;
//			}
//			var levelId = -1;
//			if(rowObject.level != null && rowObject.level != undefined){
//				levelId = rowObject.level.id;
//			}
			var status = 1;		
			if(rowObject.status != activeStatus){
				status = 0;
			}
			return "<a href='javascript:void(0)' onclick=\"return ProductCatalog.getSelectedGroup('"+ rowObject.id +"','"+ Utils.XSSEncode(rowObject.departmentCode) +"','"+ rowObject.levelId +"','"+ status +"');\"><img src='/resources/images/icon-edit.png'/></a>";
		},
		delGroupFormatter: function(cellvalue, options, rowObject){
			return "<a href='javascript:void(0)' onclick=\"return ProductCatalog.deleteGroup('"+ rowObject.id +"')\"><img src='/resources/images/icon-delete.png'/></a>";
		},
		editShopFormatter: function(cellvalue, options, rowObject){
			var shopCode = "";
			if(rowObject.shop != null && rowObject.shop != undefined){
				shopCode = rowObject.shop.shopCode;
			}
			return "<a href='javascript:void(0)' onclick=\"return ProductCatalog.getSelectedShop('"+ rowObject.id +"','"+ Utils.XSSEncode(shopCode) +"','"+ Utils.XSSEncode(rowObject.value) +"');\"><img src='/resources/images/icon-edit.png'/></a>";
		},
		delShopFormatter: function(cellvalue, options, rowObject){
			return "<a href='javascript:void(0)' onclick=\"return ProductCatalog.deleteShop('"+ rowObject.id +"')\"><img src='/resources/images/icon-delete.png'/></a>";
		},
		editPriceFormatter: function(cellvalue, options, rowObject){
			var status = 0;
			if(rowObject.status == 'STOPPED'){
				status = 0;
			}else{
				status = 1;
			}
			var fromDate = "";
			if(rowObject.fromDate != null || rowObject.fromDate != undefined){
				fromDate = formatDate(rowObject.fromDate);
			}
			var toDate = "";
			if(rowObject.toDate != null || rowObject.toDate != undefined){
				toDate = formatDate(rowObject.toDate);
			}
			var tax = -1;
			if(rowObject.vat != null || rowObject.vat != undefined ){
				tax = parseInt(rowObject.vat.toString().replace('.0',''));
			}
			return "<a href='javascript:void(0)' onclick=\"return ProductCatalog.getSelectedPrice('"+ rowObject.id +"','"+ rowObject.price +"','"+ fromDate +"','"+ toDate +"','"+ status +"','"+ rowObject.priceNotVat +"','"+tax+"');\"><img src='/resources/images/icon-edit.png'/></a>";
		},
		delPriceFormatter: function(cellvalue, options, rowObject){
			return "<a href='javascript:void(0)' onclick=\"return ProductCatalog.deletePrice('"+ rowObject.id +"')\"><img src='/resources/images/icon-delete.png'/></a>";
		},
		statusFormat: function(value,row,index){
			if(row.status != null ||row.status != undefined ){
				if(row.status == 'STOPPED' ){
					return jsp_common_status_stoped;
				}else{
					return jsp_common_status_active;
				}
			}else{
				return "";
			}
		},
		expireFormat: function(value,row,index){
			if(row.expiryDate != null ||row.expiryDate != undefined ){
				if(row.expiryType != null ||row.expiryType != undefined ){
					if(row.expiryType == 1){
						return row.expiryDate+" "+jsp_common_ngay;
					}else{
						return row.expiryDate+" "+jsp_common_thang;
					}
				}
			}else{
				return "";
			}
		},
		lotFormat: function(value,row,index){
			if(row.checkLot != null ||row.checkLot != undefined ){
				if(row.checkLot == 1 ){
					return jsp_common_co;
				}else{
					return jsp_common_khong;
				}
			}else{
				return "";
			}
		},
		priceFormat: function(value,row,index){
			if(value != null ||value != undefined ){
				return formatCurrency(value);
			}else{
				return "";
			}
		},
		viewChangeDetail:function(cellvalue, options, rowObject){			
			return "<a href='javascript:void(0)' onclick='return ProductCatalog.getSearchActionGridUrl("+rowObject.id+")' ><span style='cursor:pointer'><img src='/resources/images/icon-view.png'/></span></a>";			
		},
		taxFormat: function(cellvalue, options, rowObject){
			if(cellvalue != null ||cellvalue != undefined ){
				return cellvalue.toString().replace('.0','');
			}else{
				return "";
			}
		}
	};
var ProgrammeDisplayCatalogFormatter = {
	relationCellIconFormatter: function(cellvalue, options, rowObject){		
		if(cellvalue == "AND"){
			return "Và";
		} else {
			return "Hoặc";
		}		
	},
	detailCellIconFormatter: function(cellvalue, options, rowObject){		
		return "<a href='javascript:void(0)' onclick=\"return ProgrammeDisplayCatalog.getSelectedProgram('"+ rowObject.id +"');\"><img src='/resources/images/icon-view.png'/></a>";
	},
	editCellIconLevelFormatter: function(cellvalue, options, rowObject){
		if(!checkPermissionEdit()){
			return '';
		}
		return "<a href='javascript:void(0)' onclick=\"ProgrammeDisplayCatalog.showDialogCreateLevel(true,"+ rowObject.id +");\"><img src='/resources/images/icon-edit.png'/></a>";
	},
	delCellIconLevelFormatter: function(cellvalue, options, rowObject){
		if(!checkPermissionEdit()){
			return '';
		}
		return "<a href='javascript:void(0)' onclick=\"return ProgrammeDisplayCatalog.deleteLevelRow('"+ rowObject.id +"')\"><img src='/resources/images/icon-delete.png'/></a>";
	},
	editCellIconProductFormatter: function(cellvalue, options, rowObject){		
		if(!checkPermissionEdit()){
			return '';
		}
		return "<a href='javascript:void(0)' onclick=\"return ProgrammeDisplayCatalog.showUpdateProductDialog("+ options.rowId +");\"><img src='/resources/images/icon-edit.png'/></a>";
	},
	delCellIconProductFormatter: function(cellvalue, options, rowObject){
		if(!checkPermissionEdit()){
			return '';
		}
		return "<a href='javascript:void(0)' onclick=\"return ProgrammeDisplayCatalog.deleteProductRow('"+ rowObject.id +"')\"><img src='/resources/images/icon-delete.png'/></a>";
	},
	editCellIconQuotaGroupFormatter: function(cellvalue, options, rowObject){
		if(!checkPermissionEdit()){
			return '';
		}
		return "<a href='javascript:void(0)' onclick=\"return ProgrammeDisplayCatalog.getChangedQuotaForm(true,"+  options.rowId +");\"><img src='/resources/images/icon-edit.png'/></a>";
	},
	delCellIconQuotaGroupFormatter: function(cellvalue, options, rowObject){
		if(!checkPermissionEdit()){
			return '';
		}
		return "<a href='javascript:void(0)' onclick=\"return ProgrammeDisplayCatalog.deleteQuotaGroupRow('"+ rowObject.id +"')\"><img src='/resources/images/icon-delete.png'/></a>";
	},
	addCellIconQuotaGroupFormatter: function(cellvalue, options, rowObject){
		if(!checkPermissionEdit()){
			return '';
		}
		if(Utils.getStatusValue(rowObject.status) == 1){
			return "<a href='javascript:void(0)' onclick=\"return ProgrammeDisplayCatalog.addSubQuotaGroupRow('"+ rowObject.id +"'," + options.rowId +","+ options.gid +");\"><img src='/resources/images/icon-add.png'/></a>";
		}
		return '';
	},
	delCellIconStaffFormatter: function(cellvalue, options, rowObject){
		if(!checkPermissionDev()){
			return '';
		}
		return "<a href='javascript:void(0)' onclick=\"return ProgrammeDisplayCatalog.deleteStaffRow('"+ rowObject.id +"')\"><img src='/resources/images/icon-delete.png'/></a>";
	},
	viewCellIconStaffFormatter: function(cellvalue, options, rowObject){
		return "<a href='javascript:void(0)' onclick=\"return ProgrammeDisplayCatalog.viewCustomerByStaff('"+ rowObject.id +"',"+ options.rowId +")\"><img src='/resources/images/icon-view.png'/></a>";
	},
	delCellIconSubQuotaGroupFormatter: function(cellvalue, options, rowObject){
		if(!checkPermissionEdit()){
			return '';
		}
		return "<a href='javascript:void(0)' onclick=\"return ProgrammeDisplayCatalog.deleteSubQuotaGroupRow('"+ rowObject.id + "','"+ options.gid +"')\"><img src='/resources/images/icon-delete.png'/></a>";
	},
	deleteCellIconCustomerFormatter: function(cellvalue, options, rowObject){
		if(!checkPermissionDev()){
			return '';
		}
		return "<a href='javascript:void(0)' onclick=\"return ProgrammeDisplayCatalog.deleteCustomerRow('"+ rowObject.id +"')\"><img src='/resources/images/icon-delete.png'/></a>";
	},
	editCellIconCustomerFormatter: function(cellvalue, options, rowObject){
		if(!checkPermissionDev()){
			return '';
		}
		return "<a href='javascript:void(0)' onclick=\"return ProgrammeDisplayCatalog.showUpdateCustomerDetailDialog('"+ options.rowId +"','"+ rowObject.displayProgramLevel.id +"')\"><img src='/resources/images/icon-edit.png'/></a>";
	},
	detailInfoChangeCellFormatter: function(cellvalue, options, rowObject){		
		return "<a href='javascript:void(0)' onclick=\"return ProgrammeDisplayCatalog.viewDetailActionLog('"+ rowObject.id +"')\"><img src='/resources/images/icon-view.png'/></a>";
	},
	actionTypeCellFormatter: function(cellvalue, options, rowObject){
		var content = '';
		if(cellvalue == 'INSERT'){
			cellvalue = 'Tạo mới ';
		} else if(cellvalue == 'UPDATE'){
			cellvalue = 'Cập nhật';
		} else if(cellvalue == 'DELETE'){
			cellvalue = 'Xóa';
		}				
		return cellvalue;
	},
	editCellIconShopFormatter: function(cellvalue, options, rowObject){
		if(!checkPermissionEdit()){
			return '';
		}
		return "<a href='javascript:void(0)' onclick=\"return ProgrammeDisplayCatalog.showUpdateShopDetailDialog('"+ options.rowId +"','"+ rowObject.status +"');\"><img src='/resources/images/icon-edit.png'/></a>";
	},
	deleteCellIconShopFormatter: function(cellvalue, options, rowObject){
		if(!checkPermissionEdit()){
			return '';
		}
		return "<a href='javascript:void(0)' onclick=\"return ProgrammeDisplayCatalog.deleteShopRow('"+ rowObject.id +"');\"><img src='/resources/images/icon-delete.png'/></a>";
	}
};
var CommonSearchFormatter = {
	selectCellIconFormatterEasyUIDialog : function(value, rowData, rowIndex) {
			return "<a href='javascript:void(0)' onclick=\"return CommonSearch.getResultSeachStyle1EasyUIDialog("+ rowIndex + ");\">Chọn</a>";
	},
	selectCellIconFormatter: function(cellvalue, options, rowObject){
		return "<a href='javascript:void(0)' onclick=\"return CommonSearch.getResultSeachStyle1("+ options.rowId +");\">Chọn</a>";
	},
	myCode: function(cellvalue, options, rowObject){
		if ($('.fancybox-inner #loai').val() ==4 ){
			return Utils.XSSEncode(rowObject.customerCode);
		}
		return Utils.XSSEncode(rowObject.staffCode);
	},
	myName: function(cellvalue, options, rowObject){
		if ($('.fancybox-inner #loai').val() ==4 ){
			return Utils.XSSEncode(rowObject.customerName);
		}
		return Utils.XSSEncode(rowObject.staffName);
	},
	selectCellIconFormatter_nvbh_1: function(cellvalue, options, rowObject){
		if ($('.fancybox-inner #loai').val() ==4 ){
			return "<input type=\"checkbox\" id=\"fgid_"+rowObject.id+"\" onclick=\"SuperviseCustomer.selectCheckbox("+rowObject.id+",'"+ Utils.XSSEncode(rowObject.customerCode) +"',"+ $('.fancybox-inner #loai').val()+")\"/>";
		}
		return "<input type=\"checkbox\" id=\"fgid_"+rowObject.id+"\" onclick=\"SuperviseCustomer.selectCheckbox("+rowObject.id+",'"+ Utils.XSSEncode(rowObject.staffCode) +"',"+ $('.fancybox-inner #loai').val()+")\"/>";
	},
	selectCellIconFormatterForCustomer:function(cellvalue, options, rowObject){
		var flag=false;
		if(DebitPayReport._lstCustomer!=null && DebitPayReport._lstCustomer!= undefined){
			if(DebitPayReport._lstCustomer.get(rowObject.shortCode)!=undefined && DebitPayReport._lstCustomer.get(rowObject.shortCode)!=null){
				flag=true;
			}
		}
		if(flag){
			return "<input checked='checked' type='checkbox' onclick=\"DebitPayReport.selectCustomer(this,\'"+ Utils.XSSEncode(rowObject.shortCode) +"\');\">";
		}else{
			return "<input type='checkbox' onclick=\"DebitPayReport.selectCustomer(this,\'"+ Utils.XSSEncode(rowObject.shortCode) +"\');\">";
		}
	},
	selectCellIconFormatterForStaff:function(cellvalue, options, rowObject){
		var flag=false;
		if(DebitPayReport._lstStaff!=null && DebitPayReport._lstStaff!= undefined){
			if(DebitPayReport._lstStaff.get(rowObject.staffCode)!=undefined && DebitPayReport._lstStaff.get(rowObject.staffCode)!=null){
				flag=true;
			}
		}
		if(flag){
			return "<input checked='checked' type='checkbox' onclick=\"DebitPayReport.selectStaff(this,\'"+ Utils.XSSEncode(rowObject.staffCode) +"\');\">";
		}else{
			return "<input type='checkbox' onclick=\"DebitPayReport.selectStaff(this,\'"+ Utils.XSSEncode(rowObject.staffCode) +"\');\">";
		}
	},
	selectCellIconFormatterForSupervisorStaff:function(cellvalue, options, rowObject){
		var flag=false;
		if(DebitPayReport._lstStaffOwner!=null && DebitPayReport._lstStaffOwner!= undefined){
			if(DebitPayReport._lstStaffOwner.get(rowObject.staffCode)!=undefined && DebitPayReport._lstStaffOwner.get(rowObject.staffCode)!=null){
				flag=true;
			}
		}
		if(flag){
			return "<input checked='checked' type='checkbox' onclick=\"DebitPayReport.selectSupervisorStaff(this,\'"+ Utils.XSSEncode(rowObject.staffCode) +"\');\">";
		}else{
			return "<input type='checkbox' onclick=\"DebitPayReport.selectSupervisorStaff(this,\'"+ Utils.XSSEncode(rowObject.staffCode) +"\');\">";
		}
	},
	selectCellIconFormatterForProduct:function(cellvalue, options, rowObject){
		var flag=false;
		if(DebitPayReport._lstProduct!=null && DebitPayReport._lstProduct!= undefined){
			if(DebitPayReport._lstProduct.get(rowObject.productCode)!=undefined && DebitPayReport._lstProduct.get(rowObject.productCode)!=null){
				flag=true;
			}
		}
		if(flag){
			return "<input checked='checked' type='checkbox' onclick=\"CRMReportDSPPTNH.selectProduct(this,\'"+ Utils.XSSEncode(rowObject.productCode) +"\');\">";
		}else{
			return "<input type='checkbox' onclick=\"CRMReportDSPPTNH.selectProduct(this,\'"+ Utils.XSSEncode(rowObject.productCode) +"\');\">";
		}
	},
	selectCellIconFormatterForSubCat:function(cellvalue, options, rowObject){
		var flag=false;
		if(DebitPayReport._lstSubCat!=null && DebitPayReport._lstSubCat!= undefined){
			if(DebitPayReport._lstSubCat.get(rowObject.productInfoCode)!=undefined && DebitPayReport._lstSubCat.get(rowObject.productInfoCode)!=null){
				flag=true;
			}
		}
		if(flag){
			return "<input checked='checked' type='checkbox' onclick=\"DebitPayReport.selectSubCat(this,\'"+ Utils.XSSEncode(rowObject.productInfoCode) +"\');\">";
		}else{
			return "<input type='checkbox' onclick=\"DebitPayReport.selectSubCat(this,\'"+ Utils.XSSEncode(rowObject.productInfoCode) +"\');\">";
		}
	},
	selectCellIconFormatter4: function(cellvalue, options, rowObject){
		return "<a href='javascript:void(0)' onclick=\"return CommonSearch.getResultSeachStyle4("+ options.rowId +");\">Chọn</a>";
	},
	selectCellIconFormatterForDisplay: function(cellvalue, options, rowObject){
		var flag=false;
		if(ProgrammeDisplayCatalog._mapCheckSubCat!=null && ProgrammeDisplayCatalog._mapCheckSubCat!= undefined){
			if(ProgrammeDisplayCatalog._mapCheckSubCat.get(rowObject.id)!=undefined && ProgrammeDisplayCatalog._mapCheckSubCat.get(rowObject.id)!=null){
				flag=true;
			}
		}
		if(flag){
			return "<input checked='checked' type='checkbox' onclick='ProgrammeDisplayCatalog.selectSubCat(this,"+rowObject.id+");' class='cbCategoryProduct' value='"+ Utils.XSSEncode(rowObject.productInfoCode) +"'>";
		}else{
			return "<input type='checkbox' onclick='ProgrammeDisplayCatalog.selectSubCat(this,"+rowObject.id+");' class='cbCategoryProduct' value='"+ Utils.XSSEncode(rowObject.productInfoCode) +"'>";
		}
	},
	selectCell: function(cellvalue, options, rowObject){
		return "<a href='javascript:void(0)' onclick=\"return CommonSearch.getResultSeachStyle2("+ options.rowId +");\">Chọn</a>";
	},
	statusFormat:function(cellvalue, options, rowObject){
		if(rowObject.cycleCountStatus == 'ONGOING'){
			return "Đang thực hiện";
		}else if(rowObject.cycleCountStatus == 'COMPLETED'){
			return "Hoàn thành";
		}else if(rowObject.cycleCountStatus == 'CANCELLED'){
			return "Hủy bỏ";
		}else{
			return "";
		}
	},
	selectCellDistrictIconFormatter: function(cellvalue, options, rowObject){
		return "<a href='javascript:void(0)' onclick=\"return CommonSearch.getResultSeachStyleDistrict("+ options.rowId +");\">Chọn</a>";
	}
};
var StaffCatalogFormatter = {
		editCellIconFormatter: function(cellvalue, rowObject, options){		
			return "<a href='/catalog_staff_manager/viewdetail?staffId="+rowObject.id+"'><span style='cursor:pointer'><img src='/resources/images/icon-edit.png'/></span></a>";
		},
		searchCellIconFormatter: function(cellvalue, rowObject, options){		
			return "<a href='/catalog_staff_manager/viewdetail?staffId="+rowObject.id+"'><span style='cursor:pointer'><img src='/resources/images/icon-view.png'/></span></a>";
		},
		statusFormat: function(cellvalue, rowObject, options){
			if(rowObject.status != null ||rowObject.status != undefined ){
				if(rowObject.status == 'STOPPED' ){
					return "Tạm ngưng";
				}else{
					return "Hoạt động";
				}
			}else{
				return "";
			}
		},
		editCategoryFormatter: function(cellvalue, options, rowObject){
			return "<a href='javascript:void(0)' onclick=\"return StaffCatalog.getSelectedCategory('"+ rowObject.id +"');\"><img src='/resources/images/icon-edit.png'/></a>";
		},
		delCategoryFormatter: function(cellvalue, options, rowObject){
			return "<a href='javascript:void(0)' onclick=\"return StaffCatalog.deleteCategory('"+ rowObject.id +"')\"><img src='/resources/images/icon-delete.png'/></a>";
		},
		priceFormat: function(cellvalue, options, rowObject){			
			if(rowObject.total != null ||rowObject.total != undefined ){
				return formatCurrency(rowObject.total);
			}else{
				return "";
			}
		},
		typeFormatter: function(cellvalue, options, rowObject){
			var content = '';
			if(rowObject.actionType == 'INSERT'){
				return 'Tạo mới ';
			} else if(rowObject.actionType == 'UPDATE'){
				return 'Cập nhật';
			} else if(rowObject.actionType == 'DELETE'){
				return 'Xóa';
			}				
			return cellvalue;
		},
		viewCellIconFormatter: function(cellvalue, options, rowObject){
			return "<a href='javascript:void(0)' onclick=\"return StaffCatalog.getDetailChangedGridUrl('"+ rowObject.id +"');\"><img src='/resources/images/icon-view.png'/></a>";
		},
		delSupervisorFormatter: function(cellvalue, options, rowObject){
			if ($('#staffSuper').val() != "1") return "";
			return "<a href='javascript:void(0)' onclick=\"return StaffCatalog.deleteSupervisor('"+ rowObject.id +"')\"><img src='/resources/images/icon-delete.png'/></a>";
		}
	};
var StaffUnitTreeFormatter = {
	editCellIconFormatter: function(cellvalue, rowObject, options){		
		return "<a href='javascript:void(0)' onclick=\"return UnitTreeCatalog.showStaffEditScreen("+ rowObject.id +","+ rowObject.staffType.objectType+","+rowObject.shop.id+");\"><span style='cursor:pointer'><img src='/resources/images/icon-edit.png'/></span></a>";
	},
	searchCellIconFormatter: function(cellvalue, rowObject, options){		
		return "<a href='javascript:void(0)' onclick=\"return UnitTreeCatalog.showSearchTreePosition("+ rowObject.id +");\"><span style='cursor:pointer'><img src='/resources/images/icon-edit.png'/></span></a>";
	},
	backMainPage : function(value, rowData, rowIndex) {
		if (rowData.staff != null || rowData.staff != undefined) {
			return '<a href="javascript:void(0)" onclick="return UnitTreeCatalog.showCollapseTab(-1,'+rowData.shop.id+',null,null,null,null,'+rowData.id+')"><img src="/resources/images/icon-view.png" width="15" height="16"></a>';
		}
	},
	shopFormat : function(value, rowData, rowIndex) {
		return Utils.XSSEncode(rowData.shop.shopCode);
	},
	statusFormat: function(cellvalue, rowObject, options){
		if(rowObject.status != null ||rowObject.status != undefined ){
			if(rowObject.status == 'STOPPED' ){
				return jsp_common_status_stoped;
			}else{
				return "Hoạt động";
			}
		}else{
			return "";
		}
	},
	typeFormatter: function(cellvalue, options, rowObject){
		var content = '';
		if(rowObject.actionType == 'INSERT'){
			return 'Tạo mới ';
		} else if(rowObject.actionType == 'UPDATE'){
			return 'Cập nhật';
		} else if(rowObject.actionType == 'DELETE'){
			return 'Xóa';
		}				
		return cellvalue;
	}
};
var BaryCentricProgramCatalogFormatter = {
	detailCellIconFormatter: function(cellvalue, options, rowObject){		
		return "<a href='javascript:void(0)' onclick=\"return BaryCentricProgrammeCatalog.getSelectedProgram('"+ rowObject.id +"');\"><img src='/resources/images/icon-view.png'/></a>";
	},
	editCellIconShopFormatter: function(cellvalue, options, rowObject){
		if(!checkPermissionEdit()){
			return '';
		}
		return "<a href='javascript:void(0)' onclick=\"return BaryCentricProgrammeCatalog.showUpdateShopDetailDialog('"+ options.rowId +"','"+ rowObject.status +"');\"><img src='/resources/images/icon-edit.png'/></a>";
	},
	deleteCellIconShopFormatter: function(cellvalue, options, rowObject){
		if(!checkPermissionEdit()){
			return '';
		}
		return "<a href='javascript:void(0)' onclick=\"return BaryCentricProgrammeCatalog.deleteShopRow('"+ rowObject.id +"');\"><img src='/resources/images/icon-delete.png'/></a>";
	},
	deleteCellIconProductFormatter: function(cellvalue, options, rowObject){
		if(!checkPermissionEdit()){
			return '';
		}
		return "<a href='javascript:void(0)' onclick=\"return BaryCentricProgrammeCatalog.deleteProductRow('"+ rowObject.id +"');\"><img src='/resources/images/icon-delete.png'/></a>";
	},
	editCellIconProductFormatter: function(cellvalue, options, rowObject){
		if(!checkPermissionEdit()){
			return '';
		}
		return "<a href='javascript:void(0)' onclick=\"return BaryCentricProgrammeCatalog.showUpdateProductDialog('"+ options.rowId +"');\"><img src='/resources/images/icon-edit.png'/></a>";
	}
};
var CategoryTypeCatalogFormatter = {
	delCellIconFormatter: function(value, row, index){
		return "<a href='javascript:void(0)' class='btnGridDelete' onclick=\"CategoryTypeCatalog.deleteRow('"+ row.id +"')\"><img src='/resources/images/icon-delete.png' title='Xóa'/></a>";
	},
	statusFormat: function(value, row, index){
		if(row.status != null ||row.status != undefined ){
			if(row.status == 'STOPPED' ){
				return "Tạm ngưng";
			}else{
				return "Hoạt động";
			}
		}else{
			return "";
		}
	},
	saleDayFormat:function(value, row, index){
		return CategoryTypeCatalog.parseValueToChar(value);
	}
};
var CategoryTypeProductCatalogFormatter = {
		editCellIconFormatter: function(value, row, index){		
			return "<a href='/product-distributer/viewdetail?productId="+row.id+"'><span style='cursor:pointer'><img src='/resources/images/icon-edit.png'/></span></a>";
		},
		delCellIconFormatter: function(value, row, index){
			var data = "<a href='javascript:void(0)' onclick=\"return CategoryTypeProductCatalog.deleteRow('"+ row.id +"')\"><img src='/resources/images/icon-delete.png' title='Xóa'/></a>";
			return getFormatterControlGrid('btnDeleteGrid', data);
		},
		statusFormat: function(value, row, index){
			if(row.status != null ||row.status != undefined ){
				if(row.status == 'STOPPED' ){
					return "Tạm ngưng";
				}else{
					return "Hoạt động";
				}
			}else{
				return "";
			}
		},
		saleDayFormat:function(value, row, index){
			return CategoryTypeCatalog.parseValueToChar(value);
		}
	};
var CustomerCatalogFormatter = {
	editCellIconFormatter: function(cellvalue, options, rowObject){		
			return "<a href='javascript:void(0)' onclick=\"return CustomerCatalog.getSelectedCustomer('"+ rowObject.id +"');\"><img src='/resources/images/icon-edit.png' height='16'/></a>";
		
	},
	editCellIconLevelCatFormatter: function(cellvalue, options, rowObject){
		if($('#hasEditPermission').val().trim() == 'true'){
			return "<a href='javascript:void(0)' onclick=\"return CustomerCatalog.getSelectedLevelCatRow('"+ options.rowId +"');\"><img src='/resources/images/icon-edit.png'/></a>";
		}
		return '';
	},
	deleteCellIconLevelCatFormatter: function(cellvalue, options, rowObject){
		if($('#hasEditPermission').val().trim() == 'true'){
			return "<a href='javascript:void(0)' onclick=\"return CustomerCatalog.deleteLevelCatRow('"+ rowObject.id +"');\"><img src='/resources/images/icon-delete.png'/></a>";
		}
		return '';
	},
	detailInfoChangeCellFormatter: function(cellvalue, options, rowObject){		
		return "<a href='javascript:void(0)' onclick=\"return CustomerCatalog.viewDetailActionLog('"+ rowObject.id +"')\"><img src='/resources/images/icon-view.png'/></a>";
	},
	actionTypeCellFormatter: function(cellvalue, options, rowObject){
		var content = '';
		if(cellvalue == 'INSERT'){
			cellvalue = 'Tạo mới ';
		} else if(cellvalue == 'UPDATE'){
			cellvalue = 'Cập nhật';
		} else if(cellvalue == 'DELETE'){
			cellvalue = 'Xóa';
		}
		if(rowObject.columnName != undefined){
			content+= rowObject.columnName;
		}		
		return Utils.XSSEncode(cellvalue);
	}
};
var SalesDayCatalogFormatter = {
		editCellIconFormatter: function(cellvalue, options, rowObject){		
			return "<a href='/catalog/sales-day/viewdetail?salesDayId="+rowObject.id+"'><span style='cursor:pointer'><img src='/resources/images/icon-edit.png'/></span></a>";
		},
		delCellIconFormatter: function(cellvalue, options, rowObject){
			return "<a href='javascript:void(0)' onclick=\"return SalesDayCatalog.deleteRow('"+ rowObject.id +"')\"><img src='/resources/images/icon-delete.png'/></a>";
		},
		statusFormat: function(cellvalue, options, rowObject){
			if(rowObject.status != null ||rowObject.status != undefined ){
				if(rowObject.status == 'STOPPED' ){
					return "Tạm ngưng";
				}else{
					return "Hoạt động";
				}
			}else{
				return "";
			}
		}
	};
var LevelIncomeCatalogFormatter = {
		editCellIconFormatter: function(cellvalue, row, index){
			var proInfoCode = 0;
			var saleLevelName ='';
			var proInfoId = 0;
			var status = 0;
			if(row.status != null && row.status != undefined){
				if(row.status == 'STOPPED'){
					status = 0;
				}else{
					status = 1;
				}
			}
			if(row.cat != null){
				proInfoCode = row.cat.productInfoCode ;
				proInfoId = row.cat.id;
			}
			if(row.saleLevelName != null){
				saleLevelName = row.saleLevelName;
			}
			
			return "<a title= " + catalog_customer_type_edit + " href='javascript:void(0);' class='btnGridEdit' onclick=\"LevelIncomeCatalog.getSelectedLevelIncome('"+row.id+"','"+ Utils.XSSEncode(proInfoCode) +"',"+proInfoId+",'"+ Utils.XSSEncode(row.saleLevelCode) + "','" + Utils.XSSEncode(saleLevelName) + "','" + Utils.XSSEncode(row.fromAmountString) + "','" + Utils.XSSEncode(row.toAmountString) + "','"+status+"');\"><img src='/resources/images/icon-edit.png'/></a>";
		},
		delCellIconFormatter: function(cellvalue, row, index){
			return "<a href='javascript:void(0)' onclick='LevelIncomeCatalog.deleteRow("+ row.id +");'><img src='/resources/images/icon-delete.png'/></a>";
		},
		statusFormat: function(cellvalue, row, index){
			if(row.status != null ||row.status != undefined ){
				if(row.status == 'STOPPED' ){
					return jsp_common_status_stoped;
				}else{
					return jsp_common_status_active;
				}
			}else{
				return "";
			}
		},
		fromAmountFormat: function(cellvalue, row, index){
			if(row.fromAmount != null ||row.fromAmount != undefined ){
				return formatCurrency(row.fromAmountString);
			}else{
				return "";
			}
		},
		toAmountFormat: function(cellvalue, row, index){
			if(row.toAmount != null ||row.toAmount != undefined ){
				return formatCurrency(row.toAmountString);
			}else{
				return "";
			}
		},
		productInfoCodeFormat:function(cellvalue, row, index){
			if(row.cat != null ||row.cat != undefined ){
				return Utils.XSSEncode(row.cat.productInfoCode);
			}else{
				return '';
			}
		},
		productInfoNameFormat:function(cellvalue, row, index){
			if(row.cat != null ||row.cat != undefined ){
				return Utils.XSSEncode(row.cat.productInfoName);
			}else{
				return '';
			}
		}
	};
var SKUCatalogFormatter = {
		editCellIconFormatter: function(cellvalue, options, rowObject){
			var status = 1;		
			if(rowObject.statusSku != activeStatus){
				status = 0;
			}
			return "<a href='javascript:void(0)' onclick=\"return SkuCatalog.getSelectedRow("+ options.rowId + ");\"><img src='/resources/images/icon-edit.png'/></a>";
		},
		delCellIconFormatter: function(cellvalue, options, rowObject){
			return "<a href='javascript:void(0)' onclick=\"return SkuCatalog.deleteSKU("+ rowObject.id +")\"><img src='/resources/images/icon-delete.png'/></a>";
		},
		skuAmountFormatter: function(cellvalue, options, rowObject){
			return formatCurrency(cellvalue);
		}
	};
var TotalMoneyQuotaCatalogFormatter = {
		editCellIconFormatter: function(cellvalue, options, rowObject){
			var status = 1;		
			if(rowObject.statusAmount != activeStatus){
				status = 0;
			}
			return "<a href='javascript:void(0)' onclick=\"return TotalMoneyQuotaCatalog.getSelectedRow("+ options.rowId + ");\"><img src='/resources/images/icon-edit.png'/></a>";
		},
		delCellIconFormatter: function(cellvalue, options, rowObject){
			return "<a href='javascript:void(0)' onclick=\"return TotalMoneyQuotaCatalog.deleteSale("+ rowObject.id +")\"><img src='/resources/images/icon-delete.png'/></a>";
		}
};
var SaleProductGridFormatter = {
		forcusFormatter:function(cellvalue, options, rowObject){
			if(rowObject.isFocus=='0'|| rowObject.isFocus==0){
				return '<input class="focusCS" type="checkbox"  />';
			}else{
				return '<input  class="focusCS" type="checkbox" checked="checked"  />';
			}
		},
		commercialSupporFormatter:function(cellvalue, options, rowObject){
			var productId = rowObject.productId;
			var productIdCommercialSupport = SPCreateOrder._mapProduct.get(productId);
			if(productIdCommercialSupport == null || productIdCommercialSupport == undefined) {
				return '<input type="text" class="ccSupportCode" onkeypress="return SPCreateOrder.moveFocusByKeyForCS(event, '+ options.rowId+');" size="10" id="commercialSupport_'+options.rowId +'" />';
			} else {
				var obj = SPCreateOrder._mapCommercialSupport.get(productIdCommercialSupport);
				if(obj.commercialSupportType != 'ZV' && obj.commercialSupportType != '' && obj.commercialSupportType != null && obj.commercialSupportType != undefined) {
					return '<input type="text" class="ccSupportCode" value="'+obj.commercialSupport+'" name="'+obj.commercialSupportType+'" onkeypress="return SPCreateOrder.moveFocusByKeyForCS(event, '+ options.rowId+');" size="10" id="commercialSupport_'+options.rowId +'" />';
				} else {
					return '<input type="text" class="ccSupportCode" onkeypress="return SPCreateOrder.moveFocusByKeyForCS(event, '+ options.rowId+');" size="10" id="commercialSupport_'+options.rowId +'" />';
				}
			}
		},
		converMoney:function(cellvalue, options, rowObject){
			
		},
		quantityFormatter:function(cellvalue, options, rowObject){
			if(cellvalue>0){
				var productId = rowObject.productId;
				var productIdCommercialSupport = SPCreateOrder._mapProduct.get(productId);
				if(productIdCommercialSupport == null || productIdCommercialSupport == undefined) {
					return '<input type="text" class="clsProductVOQuantity" size="10" id="productRow_'+options.rowId +'" onchange="SPCreateOrder.amountBlured('+ options.rowId+');" onblur="SPCreateOrder.amountBlured('+ options.rowId+');" />';
				} else {
					var obj = SPCreateOrder._mapCommercialSupport.get(productIdCommercialSupport);
					return '<input type="text" value="'+obj.quantity_input+'" class="clsProductVOQuantity" size="10" id="productRow_'+options.rowId +'" onchange="SPCreateOrder.amountBlured('+ options.rowId+');" onblur="SPCreateOrder.amountBlured('+ options.rowId+');" />';
				}
			}
			else {
				return '<input readonly="readonly" type="text" class="clsProductVOQuantity" size="10" id="productRow_'+options.rowId +'" onchange="SPCreateOrder.amountBlured('+ options.rowId+');" onblur="SPCreateOrder.amountBlured('+ options.rowId+');" />';
			}
		}
};
var DisplayToolCatalogFormatter = {
	editCellIconFormatter: function(cellvalue, options, rowObject){
		var shopCode = '';
		if(rowObject.shop!= null ||rowObject.shop!=undefined ){
			shopCode = rowObject.shop.shopCode;
		}
		var supervisorStaffCode = '';
		if(rowObject.staff!= null ||rowObject.staff!=undefined ){
			supervisorStaffCode = rowObject.staff.staffCode;
		}
		var saleStaffCode = '';
		if(rowObject.staff!= null ||rowObject.staff!=undefined ){
			saleStaffCode = rowObject.staff.staffCode;
		}
		var customerCode = '';
		if(rowObject.customer!= null ||rowObject.customer!=undefined ){
			customerCode = rowObject.customer.shortCode;
		}
		var displayToolCode = '';
		if(rowObject.product!= null ||rowObject.product!=undefined ){
			displayToolCode = rowObject.product.productCode;
		}
		var inMonth = "";
		if(rowObject.inMonth != null || rowObject.inMonth != undefined){
			inMonth = formatMonth(rowObject.inMonth);
		}
		return "<a href='javascript:void(0)' onclick=\"return DisplayToolCatalog.getSelectedDisplayTool("+ rowObject.id + ",'"+ Utils.XSSEncode(shopCode) + "','" + Utils.XSSEncode(supervisorStaffCode) + "','" + Utils.XSSEncode(saleStaffCode) + "','" + Utils.XSSEncode(customerCode) + "','"+ Utils.XSSEncode(displayToolCode) + "','"+ rowObject.quantity + "','"+ rowObject.amount + "','"+ inMonth + "');\"><img src='/resources/images/icon-edit.png'/></a>";
	},
	delCellIconFormatter: function(cellvalue, options, rowObject){
		return "<a href='javascript:void(0)' onclick=\"return DisplayToolCatalog.deleteDisplayTool('"+ rowObject.id +"')\"><img src='/resources/images/icon-delete.png'/></a>";
	},
	monthFormatter: function(cellvalue, options, rowObject){
		var inMonth = "";
		if(rowObject.inMonth != null || rowObject.inMonth != undefined){
			inMonth = formatMonth(rowObject.inMonth);
		}
		return inMonth;
	}
};
var DisplayToolProductFormatter = {
	getToolCodeFormatter: function(cellValue, options, rowObject) {
		return Utils.XSSEncode(rowObject.tool.productCode);
	},
	getToolNameFormatter: function(cellValue, options, rowObject) {
		return Utils.XSSEncode(rowObject.tool.productName);
	},
	getProductCodeFormatter: function(cellValue, options, rowObject) {
		return Utils.XSSEncode(rowObject.product.productCode);
	},
	getProductNameFormatter: function(cellValue, options, rowObject) {
		return Utils.XSSEncode(rowObject.product.productName);
	},
	deleteIconFormatter: function(cellValue, options, rowObject) {
		return "<a href='javascript:void(0)' onclick=\"return DisplayToolProductCatalog.del('"+rowObject.id+"')\"><img src='/resources/images/icon-delete.png'/></a>";
	}
};
var AttributeManagerFormatter = {
	editCellIconFormatter: function(cellvalue, options, rowObject){
		var colunmType = AttributeColumnType.parseValue(rowObject.columnType);
		var colunmValueType = AttributeColumnValue.parseValue(rowObject.columnValueType);
		var status = activeType.parseValue(rowObject.status);
		return "<a href='javascript:void(0)' onclick=\"return AttributesManager.getSelectedRow("+ options.rowId +"," + colunmType + "," + colunmValueType + "," + status + ");\"><img src='/resources/images/icon-edit.png'/></a>";
	},
	listValueCellIconFormatter: function(cellvalue, options, rowObject){
		var colunmType = AttributeColumnType.parseValue(rowObject.columnType);
		if(rowObject.columnValueType == 'EXIST_BEFORE'){
			return "<a href='javascript:void(0)' onclick=\"return AttributesManager.getListValue("+ rowObject.id +","+colunmType+");\"><img src='/resources/images/icon-view.png'/></a>";
		}
		return '';
	},
	columnTypeCellIconFormatter: function(cellvalue, options, rowObject){
		if(cellvalue!= undefined){
			if(cellvalue == 'NUMBER'){
				return attr_col_type_number;
			} else if(cellvalue == 'CHARACTER'){
				return attr_col_type_char;
			} else if(cellvalue == 'DATE_TIME'){
				return attr_col_type_date;
			}
		}		
		return '';
	},
	columnValueTypeCellIconFormatter: function(cellvalue, options, rowObject){
		if(cellvalue == 'SELF_INPUT'){
			return attr_col_val_type_manual;
		} else if(cellvalue == 'EXIST_BEFORE'){
			return attr_col_val_type_list;
		}
		return '';
	},
	editDetailCellIconFormatter: function(cellvalue, options, rowObject){		
		return "<a href='javascript:void(0)' onclick=\"return AttributesManager.getSelectedDetailRow("+ options.rowId + ");\"><img src='/resources/images/icon-edit.png'/></a>";
	},
	deleteDetailCellIconFormatter: function(cellvalue, options, rowObject){		
		return "<a href='javascript:void(0)' onclick=\"return AttributesManager.deleteDetailValue("+ options.rowId + ");\"><img src='/resources/images/icon-delete.png'/></a>";
	}
};
var IncentiveProgramCatalogFormatter = {
	delShop: function(cellValue, options, rowObject) {
		return "<a href='javascript:void(0)' onclick=\"return IncentiveProgramCatalog.delShop('"+rowObject.id+"')\"><img src='/resources/images/icon-delete.png'/></a>";
	},
	editProductCellIconFormatter: function(cellvalue, options, rowObject){		
		return "<a href='javascript:void(0)' onclick=\"return IncentiveProgramCatalog.getSelectedProduct('"+ rowObject.id +"');\"><img src='/resources/images/icon-edit.png'/></a>";
	},
	deleteProductCellIconFormatter: function(cellvalue, options, rowObject){		
		return "<a href='javascript:void(0)' onclick=\"return IncentiveProgramCatalog.deleteSelectedProduct('"+ rowObject.id +"');\"><img src='/resources/images/icon-delete.png'/></a>";
	},
	typeFormat: function(cellvalue, options, rowObject){
		if(rowObject.actionType != null ||rowObject.actionType != undefined ){
			if(rowObject.actionType == 'INSERT' ){
				return "Thêm mới";
			}else if(rowObject.actionType == 'UPDATE' ){
				return "Cập nhật";
			}else{
				return "Xóa";
			}
		}else{
			return "";
		}
	},
	viewCellIconFormatter: function(cellvalue, options, rowObject){
		return "<a href='javascript:void(0)' onclick=\"return IncentiveProgramCatalog.getDetailChangedGridUrl('"+ rowObject.id +"');\"><img src='/resources/images/icon-view.png'/></a>";
	}
};
var PayrollElementFormatter = {
		editCellIconFormatter: function(cellvalue, options, rowObject){
			return "<a href='javascript:void(0)' onclick=\"return PayrollElement.getSelectedElement('"+ rowObject.id +"','" + Utils.XSSEncode(rowObject.payrollElementCode) + "','" + Utils.XSSEncode(rowObject.payrollElementName) + "','" + rowObject.type + "','" + rowObject.roundNum + "','" + rowObject.defaultValue + "','" + rowObject.valueType + "');\"><img src='/resources/images/icon-edit.png'/></a>";
		},
		delCellIconFormatter: function(cellvalue, options, rowObject){		
			if(rowObject.countUse == 0){
				return "<a href='javascript:void(0)' onclick=\"return PayrollElement.deletePaymentElement('"+ rowObject.id + "');\"><img src='/resources/images/icon-delete.png'/></a>";
			}else{
				return "";
			}
		},
		typeFormatter: function(cellvalue, options, rowObject){		
			if(cellvalue != undefined){
				if(cellvalue == 0){
					return 'Đầu vào';
				}else if(cellvalue == 1){
					return 'Kết quả';
				}
			}
			return '';
		}
	};
var PayrollTableFormatter = {
		editCellIconFormatter: function(cellvalue, options, rowObject){
			/*var colunmType = -1;
			if(rowObject.type != undefined){
				if(rowObject.type == 'INPUT'){
					colunmType = 0;
				}else{
					colunmType = 1;
				}
			}
			return "<a href='javascript:void(0)' onclick=\"return PayrollTable.getSelectedElement('"+ rowObject.id +"','" + rowObject.payrollElementCode + "','" + rowObject.payrollElementName + "','" + colunmType + "','" + rowObject.roundNum + "','" + rowObject.defaultValue + "');\"><img src='/resources/images/icon-edit.png'/></a>";*/
			var url = '/payroll/table/info?id='+rowObject.payrollId;
			return "<a href=\""+url+"\"><img src='/resources/images/icon-edit.png'/></a>";
		},
		delCellIconFormatter: function(cellvalue, options, rowObject){		
			return "<a href='javascript:void(0)' onclick=\"return PayrollTable.deletePaymentTable('"+ rowObject.payrollId + "');\"><img src='/resources/images/icon-delete.png'/></a>";
		},
		statusFormatter :function(cellvalue, options, rowObject){
			if(cellvalue != null){
				if(cellvalue == 1){
					return jsp_common_status_active;
				}else{
					return jsp_common_status_stoped;
				}
			}else{
				return '';
			}
		}
	};
var PayrollPatternFormatter = {
		editCellIconFormatter: function(cellvalue, options, rowObject){
			var colunmType = -1;
			if(rowObject.type != undefined){
				if(rowObject.type == 'INPUT'){
					colunmType = 0;
				}else{
					colunmType = 1;
				}
			}
			if (rowObject.count > 0){
				return '';
			}
			return "<a href='javascript:void(0)' onclick=\"return PayrollPattern.viewAdd(false,"+ options.rowId +");\"><img src='/resources/images/icon-edit.png'/></a>";
		},
		delCellIconFormatter: function(cellvalue, options, rowObject){		
			if (rowObject.count == 0){
				return "<a href='javascript:void(0)' onclick=\"return PayrollPattern.deletePayrollT('"+ rowObject.payrollTId + "');\"><img src='/resources/images/icon-delete.png'/></a>";
			}
			else return '';
		},
		statusFormatter :function(cellvalue, options, rowObject){
			if(cellvalue != null){
				if(cellvalue == 1){
					return jsp_common_status_active;
				}else{
					return jsp_common_status_stoped;
				}
			}else{
				return '';
			}
		}
	};
var PayrollInfoElementFormatter = {
		selectCellIconFormatter: function(cellvalue, options, rowObject){//
			return "<a href='javascript:void(0)' onclick=\"return PayrollInfoElement.getRowPayRollInfoElement('"+ Utils.XSSEncode(rowObject.payrollCode) +"');\">Chọn</a>";
		},
		inputCellInGirdStaff: function(cellvalue, options, rowObject){//
			if(null!=rowObject && null!=rowObject.value){ //co gia tri
				if(rowObject.payrollElementType==0){
					return '<input value="'+rowObject.value+'" style="text-align:right" type="text" maxlength="17" class="value_payroll" id="txt_'+options.rowId +'" row-id="'+options.rowId+'"/>';
				}
				if(rowObject.payrollElementType==1){
					return '<input value="'+rowObject.value+'" style="text-align:right" type="text" maxlength="2000" class="value_payroll" id="txt_'+options.rowId +'" row-id="'+options.rowId+'"/>';
				}
			}
			else{// chua co gia tri (null)
				if(rowObject.payrollElementType==0){
					if(null!=rowObject.payrollElementDefaultValue){
						return '<input value="'+rowObject.payrollElementDefaultValue+'"style="text-align:right" type="text" maxlength="17" class="value_payroll" id="txt_'+options.rowId +'" row-id="'+options.rowId+'"/>';
					}
					else{
						return '<input style="text-align:right" type="text" maxlength="17" class="value_payroll" id="txt_'+options.rowId +'" row-id="'+options.rowId+'"/>';
					}
				}
				if(rowObject.payrollElementType==1){
					if(null!=rowObject.payrollElementDefaultValue){
						return '<input value="'+rowObject.payrollElementDefaultValue+'"style="text-align:right" type="text" maxlength="2000" class="value_payroll" id="txt_'+options.rowId +'" row-id="'+options.rowId+'"/>';
					}
					else{
						return '<input style="text-align:right" type="text" maxlength="2000" class="value_payroll" id="txt_'+options.rowId +'" row-id="'+options.rowId+'"/>';
					}
				}
			}
			
		}
	};
var DpPayPeriodPeriodFormatter = {
		editCellFormatter:function(cellvalue, options, rowObject){
			if(rowObject.status=='CREATE_NEW'){
				return "<a href='javascript:void(0)' onclick=\"return DpPayPeriodPeriodManager.getSelectedRow("+options.rowId+");\"><img src='/resources/images/icon-edit.png'/></a>";
			}			
			return '';
		},
		deleteCellFormatter:function(cellvalue, options, rowObject){
			if(rowObject.status=='CREATE_NEW'){
				return "<a href='javascript:void(0)' onclick=\"return DpPayPeriodPeriodManager.deleteRow("+ rowObject.id + ");\"><img src='/resources/images/icon-delete.png'/></a>";
			}
			return '';
		}		
};
var CalculatePeriodFormatter = {
		viewDetailCellFormatter: function(cellvalue,option,rowObject){
			return '<a href="/dppayperiod/calculate/detail?periodId='+rowObject.id+'"><span style="cursor:pointer"><img src="/resources/images/icon-view.png"></span></a>';
		},
		statusCellFormatter:function(cellvalue,option,rowObject){
			var DpPayPeriodStatus = {
				deleted:'DELETED',
				create_new:'CREATE_NEW',
				applied_value:'APPLIED_VALUE',
				caculated:'CACULATED',
				approved:'APPROVED',
				stop:'STOP',
				destroyed:'DESTROYED'
			};
			var status = rowObject.status;
			if(status==DpPayPeriodStatus.deleted || status==null){
				return 'Xóa';
			}else if(status==DpPayPeriodStatus.create_new){
				return 'Mới tạo';
			}else if(status==DpPayPeriodStatus.applied_value){
				return 'Đã nhập';
			}else if(status==DpPayPeriodStatus.caculated){
				return 'Đã tính';
			}else if(status==DpPayPeriodStatus.approved){
				return 'Đã chốt';
			}else if(status==DpPayPeriodStatus.stop){
				return jsp_common_status_stoped;
			}else if(status==DpPayPeriodStatus.destroyed){
				return 'Hủy';
			}
		},
		displayResultCellFormatter:function(cellvalue,option,rowObject){
			if(rowObject.displayResult==1){
				return 'Đạt';
			}else if(rowObject.displayResult==0){
				return 'Không đạt';
			}
			return '';
						
		},
		revenueCellFormatter:function(cellvalue,option,rowObject){
			if(rowObject.revenue!=null){
				return formatCurrency(rowObject.revenue);
			}
			return '0';
		},
		bonusCellFormatter:function(cellvalue,option,rowObject){
			if(rowObject.bonus!=null){
				return formatCurrency(rowObject.bonus);
			}
			return '0';
		}
		
};
var DeviceManagerFormatter = {
	viewCellIconFormatter: function(cellvalue, options, rowObject){		
		var contractId = 0;
		if(rowObject.contractId != null){
			contractId = rowObject.contractId;
		}
		return "<a href='/device/manager/viewdetail?deviceId="+rowObject.equipmentId+"&contractId="+contractId+"'><span style='cursor:pointer'><img src='/resources/images/icon-view.png'/></span></a>";
	},
	delCellIconFormatter: function(cellvalue, options, rowObject){
		return "<a href='javascript:void(0)' onclick=\"return DeviceManager.deleteRow('"+ rowObject.equipmentId +"')\"><img src='/resources/images/icon-delete.png'/></a>";
	},
	statusFormatter : function(cellvalue, options, rowObject){
		if(cellvalue != undefined){
			if(cellvalue == 1){
				return "Tốt";
			}else if(cellvalue == 2){
				return "Hỏng";
			}else if(cellvalue == 3){
				return "Đang bảo hành";
			}else if(cellvalue == 4){
				return "Đang sửa chữa";
			}else if(cellvalue == 5){
				return "Đã thanh lý";
			}else if(cellvalue == 6){
				return "Tốt đã thanh lý";
			}else if(cellvalue == 7){
				return "Mất chưa xử lý";
			}else if(cellvalue == 8){
				return "Mất";
			} else{
				return "";
			}
		}else{
			return "";
		}
	},
	typeFormatter:function(cellvalue, options, rowObject){
		if(cellvalue != undefined){
			if(cellvalue == 1){
				return "Cập nhật";
			}else if(cellvalue == 2){
				return "Điều chuyển";
			}else if(cellvalue == 3){
				return "Hợp đồng";
			}else if(cellvalue == 4){
				return "Kết thúc";
			}
		}else{
			return "";
		}
	},
	fromObjectFormatter:function(cellvalue, options, rowObject){
		if(rowObject.actionType != undefined){
			if(rowObject.actionType == 1){
				if(rowObject.fromObjectId == 1){
					return "Tốt";
				}else if(rowObject.fromObjectId == 2){
					return "Hỏng";
				}else if(rowObject.fromObjectId == 3){
					return "Đang bảo hành";
				}else if(rowObject.fromObjectId == 4){
					return "Đang sửa chữa";
				}else if(rowObject.fromObjectId == 5){
					return "Đã thanh lý";
				}else if(rowObject.fromObjectId == 6){
					return "Hủy";
				}else if(rowObject.fromObjectId == 7){
					return "Mất chưa xử lý";
				}else if(rowObject.fromObjectId == 8){
					return "Mất";
				}
			}else if(rowObject.actionType == 2){
				if(rowObject.fromShopName != undefined){
					return Utils.XSSEncode(rowObject.fromShopName);
				}else{
					return "";
				}
			}else if(rowObject.actionType == 3){
				if(rowObject.fromShopName != undefined){
					return Utils.XSSEncode(rowObject.fromShopName);
				}else{
					return "";
				}
			}else if(rowObject.actionType == 4){
				if(rowObject.fromCustomerName != undefined){
					return Utils.XSSEncode(rowObject.fromCustomerName);
				}else{
					return "";
				}
			}
		}else{
			return "";
		}
	},
	toObjectFormatter:function(cellvalue, options, rowObject){
		if(rowObject.actionType != undefined){
			if(rowObject.actionType == 1){
				if(rowObject.toObjectId == 1){
					return "Tốt";
				}else if(rowObject.toObjectId == 2){
					return "Hỏng";
				}else if(rowObject.toObjectId == 3){
					return "Đang bảo hành";
				}else if(rowObject.toObjectId == 4){
					return "Đang sửa chữa";
				}else if(rowObject.toObjectId == 5){
					return "Đã thanh lý";
				}else if(rowObject.toObjectId == 6){
					return "Hủy";
				}else if(rowObject.toObjectId == 7){
					return "Mất chưa xử lý";
				}else if(rowObject.toObjectId == 8){
					return "Mất";
				}
			}else if(rowObject.actionType == 2){
				if(rowObject.toShopName != undefined){
					return Utils.XSSEncode(rowObject.toShopName);
				}else{
					return "";
				}
			}else if(rowObject.actionType == 3){
				if(rowObject.toCustomerName != undefined){
					return Utils.XSSEncode(rowObject.toCustomerName);
				}else{
					return "";
				}
			}else if(rowObject.actionType == 4){
				if(rowObject.toShopName != undefined){
					return Utils.XSSEncode(rowObject.toShopName);
				}else{
					return "";
				}
			}
		}else{
			return "";
		}
	},
	contractNumberFormatter:function(cellvalue, options, rowObject){
		if(cellvalue != undefined){
			return "<a href='/contract/manager/contractdetail?id="+rowObject.contractId+"&type=1'><span style='cursor:pointer'>"+ Utils.XSSEncode(rowObject.contractNo) +"</span></a>";
		}else{
			return "";
		}
	},
	contractNumberTransferFormatter:function(cellvalue, options, rowObject){
		if(rowObject.actionType == 1 ||rowObject.actionType == 2){
			return "";
		}else{
			if(cellvalue != undefined){
				return "<a href='/contract/manager/contractdetail?id="+rowObject.contractId+"&type=1'><span style='cursor:pointer'>"+ Utils.XSSEncode(rowObject.contractNo) +"</span></a>";
			}else{
				return "";
			}
		}
	},
	customerCodeFormatter:function(cellvalue, options, rowObject){
		if(cellvalue != undefined){
			return Utils.XSSEncode(cellvalue + '-' + rowObject.customerName); 
		}else if(cellvalue == undefined && rowObject.customerName != undefined){
			return Utils.XSSEncode(rowObject.customerName);
		}else{
			return "";
		}
	}
};
var MapProductFormatter = {
		delCellIconFormatter: function(cellvalue, options, rowObject){
			return "<a href='javascript:void(0)' onclick=\"return MapProduct.deleteMapProduct('"+ rowObject.id +"')\"><img src='/resources/images/icon-delete.png'/></a>";
		}
};
var PayPedriodFormatter = {
		viewDetailCellFormatter: function(cellvalue,options,rowObject){
			return '<a href="/dppayperiod/pay/viewDetail?periodId='+rowObject.id+'"><span style="cursor:pointer"><img src="/resources/images/icon-view.png"></span></a>';
		},		
		payPeriodStatusCellFormatter:function(cellvalue,options,rowObject){
			if(rowObject.saleOrderId!=null){
				return '<input type="checkbox" checked="checked" disabled="disabled" />';
			}else if(true){//don hang da xuat
				return '<input type="checkbox" class="check_box_selected" row-id="'+options.rowId+'"  />';
			}
			return '';
		},
		quantityCellFormatter:function(cellvalue,options,rowObject){
			return formatQuantity(rowObject.quantity, rowObject.convfact);
		},
		lotCellFormatter:function(cellvalue,options,rowObject){
			if(rowObject.checkLot!=null && rowObject.checkLot==1 && rowObject.saleOrderId==null){
				return '<a href="javascript:void(0)" onclick="return DpPayPeriodPay.chooseLotDialog('+options.rowId+',\''+ Utils.XSSEncode(rowObject.shopCode) +'\','+rowObject.productId+','+rowObject.quantity+');" >Chọn lô</a>';
			}
			return '';
		},
		saleStaffCellFormatter:function(cellvalue,options,rowObject){
			var lstSaleStaff = rowObject.lstSaleStaff;
			if(lstSaleStaff.length==1){
				return '<p id="selectSaleStaff'+options.rowId+'" class="'+ Utils.XSSEncode(lstSaleStaff[0].staffCode) +'">'+ Utils.XSSEncode(lstSaleStaff[0].staffCode) +'</p>';
			}
			var SELECT = new Array();
			SELECT.push('<select id="selectSaleStaff'+options.rowId+'" style="width: 138px;" class="class_selected BoxSelect" >');
			for(var i=0;i<lstSaleStaff.length;++i){
				SELECT.push('<option value="'+ Utils.XSSEncode(lstSaleStaff[i].staffCode) +'">'+ Utils.XSSEncode(lstSaleStaff[i].staffCode) +'</option>');
			}
			SELECT.push("</select>");
			return SELECT.join("");
		},
		statusCellFormatter:function(cellvalue,options,rowObject){
			if(rowObject.isPaid=='true' || rowObject.isPaid==true ){
				return 'Đã trả hết';
			}
			return 'Chưa trả hết';
		}
};
var ActionTypeGridFormater = {
		actionTypeFormatter:function(cellvalue,options,rowObject){
		if(rowObject.actionType == 'INSERT'){
			return 'Tạo mới ';
		} else if(rowObject.actionType == 'UPDATE'){
			return 'Cập nhật';
		} else if(rowObject.actionType == 'DELETE'){
			return 'Xóa';
		}				
		return cellvalue;
	}
};
var ManagePoAutoNCCFormatter = {
	getShopCodeAndName : function(cellValue, rowObject, options) {
		if(rowObject.shopCode == null || rowObject.shopCode == 'null')
    		rowObject.shopCode = "";
    	if(rowObject.shopName == null || rowObject.shopName == 'null')
    		rowObject.shopName = "";
    	if(rowObject.shopCode != "" && rowObject.shopName != "")
    		return Utils.XSSEncode(rowObject.shopCode + ' - ' + rowObject.shopName);
    	return "";
	},
	getStatus : function(cellValue, rowObject, options) {
		if(rowObject.status  == 'APPROVED')
			return 'Chưa duyệt';
		if(rowObject.status  == 'NCC_APPROVED')
			return 'Đã duyệt';
		if(rowObject.status  == 'REJECT')
			return 'Từ chối';
		return "";
	},
	moneyFormat:function(cellValue, rowObject, options){
		return formatCurrency(cellValue);
	},
	quantityFormat:function(cellValue, rowObject, options){
		return formatQuantity(cellValue, rowObject.convfact);
	},
	viewDetail:function(cellvalue,rowObject, options){
		return "<span style='cursor:pointer' onclick=\"return POAutoNCCManage.detail("+ rowObject.poAutoId+",'"+ Utils.XSSEncode(rowObject.poAutoNumber) +"',"+ Utils.XSSEncode(rowObject.amount) +");\"><img src='/resources/images/icon-view.png'/></span>";
	},
	checkPOAuto : function(cellValue,rowObject, options ) {
		if(rowObject.status  == 'APPROVED') {
			var poAutoId = rowObject.poAutoId ;
			var obj = POAutoNCCManage._mapPOAutoIdSelect.get(poAutoId);
			if(obj == null) {
				return '<input type="checkbox" id="'+poAutoId +'" onchange="POAutoNCCManage.changeCheckBoxPOAuto(this.checked ,'+poAutoId+')"/>';
			} else{
				return '<input checked="checked" type="checkbox"  id="'+poAutoId +'" onchange="POAutoNCCManage.changeCheckBoxPOAuto(this.checked ,'+poAutoId+')"/>';
			}
		}
		return "";
	},
	viewDetailApprove:function(cellvalue, rowObject, options){
		if(rowObject.status == 'APPROVED') {
			return "<span style='cursor:pointer' onclick=\"return POAutoNCCManage.openApprovePOAutoDialog('"+ Utils.XSSEncode(rowObject.shopCode) +"', '"+ Utils.XSSEncode(rowObject.shopName) +"', '"+ Utils.XSSEncode(rowObject.poAutoDateStr) +"','"+ Utils.XSSEncode(rowObject.poAutoNumber) +"', "+rowObject.poAutoId+");\"><img src='/resources/images/icon-down.png'/></span>";
		} else {
			return '';
		}
	},
	instockCellFormatter: function(cellvalue, rowObject, options){
		var bigUnit = 0;
		var smallUnit = 0;
		var convfact = 1;
		if(rowObject.convfact!= null && rowObject.convfact!= undefined){
			convfact = rowObject.convfact;
		}
		bigUnit = parseInt(rowObject.availableQuantity/convfact);
		smallUnit = rowObject.availableQuantity%convfact;
		return bigUnit + '/' + smallUnit;	
	},
	amountCellFormatter: function(cellvalue, rowObject, options){		
		return '<input row-id="'+options.rowId+'" id="productRow_'+ options.rowId +'" maxlength="7" size="7" type="text" style="text-align:right" onkeypress="return NextAndPrevTextField(event, this,\'SelProduct\');" class="IssuedStock_AmountValue InputTextStyle InputText1Style SelProduct" onblur="StockIssued.amountBlured('+ options.rowId +');" onchange="StockIssued.amountChanged('+ options.rowId +');">';
	}
};
var PODVKHNCCFormatter = {
//	statusFormat:function(cellvalue,options,rowObject){
//		if(cellvalue != undefined){
//			if(cellvalue == 'CHUA_CO_PO_CONFIRM'){
//				return 'Chưa có PO Confirm ';
//			} else if(cellvalue == 'DANG_XUAT_PO_CONFIRM'){
//				return 'Đang xuất PO Confirm';
//			} else if(cellvalue == 'HOAN_TAT'){
//				return 'Hoàn tất';
//			}else {
//				return 'Treo';
//			}	
//		}else{
//			return "";
//		}
//	},
//	viewFormat: function(cellvalue,options,rowObject){
//		return "<a href='javascript:void(0)' onclick=\"return POCustomerServiceManage.getDetailGridUrl('"+ rowObject.poVnmId +"','"+ rowObject.poNumber +"');\"><img src='/resources/images/icon-view.png'/></a>";
//	},
//	createFormat: function(cellvalue,options,rowObject){
//		//if (rowObject.disStatus == "DA_CHUYEN") return "";
//		return "<a onclick='return POConfirm.getSelectedPOConfirm(" + rowObject.poVnmId+")' href='javascript:void(0)'><img src='/resources/images/icon-close.png'/></a>";
//	},
//	changeFormat: function(cellvalue,options,rowObject){
//		if(rowObject.disStatus != undefined && rowObject.disStatus == 'DA_CHUYEN'){
//			return "";
//		}else{
//			if(rowObject.poDvStatus != undefined && (rowObject.poDvStatus == 'CHUA_CO_PO_CONFIRM' || rowObject.poDvStatus == 'DANG_XUAT_PO_CONFIRM')){
//				return "<a href='javascript:void(0)' onclick=\" return POCustomerServiceManage.changePODVKH('"+ rowObject.poVnmId +"');\"><img src='/resources/images/icon-down.png'/></a>";
//			}else{
//				return "";
//			}
//		}
//	},
//	moneyFormat:function(cellvalue,options,rowObject){
//		if(cellvalue != undefined){
//			POCustomerServiceManage.tongtien += cellvalue;
//			return formatCurrency(cellvalue);
//		}else{
//			return "";
//		}
//	},
//	priceFormat:function(cellvalue,options,rowObject){
//		if(rowObject.priceValue != undefined){
//			return formatCurrency(rowObject.priceValue);
//		}else{
//			return "";
//		}
//	},
//	quantityFormat:function(cellvalue,options,rowObject){
//		if(rowObject.quantity != undefined && rowObject.product != undefined && rowObject.product.convfact != undefined){
//			return Math.floor(rowObject.quantity/rowObject.product.convfact) + "/" +(rowObject.quantity%rowObject.product.convfact);
//		}else{
//			return "";
//		}
//	}
	statusFormat:function(value,rows){
		if(value != undefined){
			if(value == 'CHUA_CO_PO_CONFIRM'){
				return 'Chưa có PO Confirm ';
			} else if(value == 'DANG_XUAT_PO_CONFIRM'){
				return 'Đang xuất PO Confirm';
			} else if(value == 'HOAN_TAT'){
				return 'Hoàn tất';
			}else {
				return 'Treo';
			}	
		}else{
			return "";
		}
	},
	viewFormat: function(index,rows){
		return "<a href='javascript:void(0)' onclick=\"return POCustomerServiceManage.getDetailGridUrl('"+ rows.poVnmId +"','"+ rows.poNumber +"');\"><img src='/resources/images/icon-view.png'/></a>";
	},
	createFormat: function(index,rows){
		//if (rowObject.disStatus == "DA_CHUYEN") return "";
		return "<a onclick='return POConfirm.getSelectedPOConfirm(" + rows.poVnmId+")' href='javascript:void(0)'><img src='/resources/images/icon-close.png'/></a>";
	},
	changeFormat: function(index,rows){
		if(rowa.disStatus != undefined && rowa.disStatus == 'DA_CHUYEN'){
			return "";
		}else{
			if(rows.poDvStatus != undefined && (rows.poDvStatus == 'CHUA_CO_PO_CONFIRM' || rows.poDvStatus == 'DANG_XUAT_PO_CONFIRM')){
				return "<a href='javascript:void(0)' onclick=\" return POCustomerServiceManage.changePODVKH('"+ rows.poVnmId +"');\"><img src='/resources/images/icon-down.png'/></a>";
			}else{
				return "";
			}
		}
	},
	moneyFormat:function(index,rows){
		if(rows != undefined){
			POCustomerServiceManage.tongtien += rows.amount;
			return formatCurrency(rows.amount);
		}else{
			return "";
		}
	},
	priceFormat:function(index,rows){
		if(rows.priceValue != undefined){
			return formatCurrency(rows.priceValue);
		}else{
			return "";
		}
	},
	quantityFormat:function(index,rows){
		if(rows.quantity != undefined && rows.product != undefined && rows.product.convfact != undefined){
			return Math.floor(rows.quantity/rows.product.convfact) + "/" +(rows.quantity%rows.product.convfact);
		}else{
			return "";
		}
	}
};
var POConfirmFormatter = {
	statusFormatter:function(cellvalue,options,rowObject){
		if(rowObject.status=='NOT_IMPORT'){
			return 'Chưa nhập';
		}else if(rowObject.status=='IMPORTING'){
			return 'Đang nhập';
		}else if(rowObject.status=='IMPORTED'){
			return 'Đã nhập';
		}else if(rowObject.status=='PENDING'){
			return 'Có thể treo';
		}else if(rowObject.status=='SUSPEND'){
			return 'Đã treo';
		}
		return '';
	},
	disStatusFormatter:function(cellvalue,rowObject,options){
		if(rowObject.disStatus=='CHUA_CHUYEN'){
			return 'Chưa chuyển';
		}else if(rowObject.disStatus=='DA_CHUYEN'){
			return 'Đã chuyển';
		}
		return '';
	},
	quantityFormatter:function(cellvalue,rowObject,options){
		return formatCurrency(cellvalue);
	},
	moneyFormatter:function(cellvalue,rowObject,options){
		return formatCurrency(cellvalue);
		
	},//liem
	viewFormat: function(cellvalue,rowObject,options){
		return "<a href='javascript:void(0)'onclick=\"return POConfirm.getListPOVnmDetailEx('"+ rowObject.id +"','"+rowObject.poCoNumber+"');\"><img src='/resources/images/icon-view.png'/></a>";
	},
	createFormat: function(cellvalue,rowObject,options){
		if(rowObject.disStatus=='CHUA_CHUYEN'){
			return "<a href=\"javascript:void(0)\" onclick=\"return POConfirm.getListPOVnmDetail('"+ rowObject.id +"');\"><img src=\"/resources/images/icon-edit.png\"></a>";
		}
		return '';
	},
	changeFormat: function(cellvalue,rowObject,options){
		if(rowObject.disStatus=='CHUA_CHUYEN'){
			return "<a href='javascript:void(0)' onclick=\"return POConfirm.transferPOConfirm('"+ rowObject.id +"');\" onclick=\"\"><img src='/resources/images/icon-down.png'/></a>";
		}	
		return '';
	},
	poVnmLotCellFormatter: function(cellvalue,rowObject, options){
		var bigUnit = 0;
		var smallUnit = 0;
		var convfact = 1;
		if(rowObject.convfact!= null && rowObject.convfact!= undefined){
			convfact = rowObject.convfact;
		}
		bigUnit = parseInt(rowObject.quantity/convfact);
		smallUnit = rowObject.quantity%convfact;
		return bigUnit + '/' + smallUnit;	
	},
	formatQuantityInCreatePOConfirm:function(cellvalue, rowObject, index){
		return '<input row-id="'+index+'" id="productRow_'+ index +'" size="5" type="text" style="text-align:right" onkeypress="return NextAndPrevTextField(event, this,\'SelProduct\');" class="IssuedStock_AmountValue InputTextStyle InputText1Style SelProduct" onblur="POConfirm.amountBlured('+ index +');" onchange="POConfirm.amountChanged('+ index +');">';
	}
};
var POAutoGroupFormatter = {
		delCellIconFormatter: function(value, row, index){
			var data = "<a href='javascript:void(0)' onclick=\"return GroupPOAuto.deleteRow('"+ row.id +"')\"><img src='/resources/images/icon-delete.png'/></a>";
	    	return getFormatterControlGrid('btnDeleteGrid2', data);
		},
		delCellIconFormatterCat: function(value, row, index){
			var data = "<a href='javascript:void(0)' onclick=\"return GroupPOAuto.deleteCategoryRow('"+ row.id +"')\"><img src='/resources/images/icon-delete.png'/></a>";
	    	return getFormatterControlGrid('btnDeleteGrid', data);
		},
		delCellIconFormatterSubCat: function(value, row, index){
			var data = "<a href='javascript:void(0)' onclick=\"return GroupPOAuto.deleteSubCategoryRow('"+ row.id +"')\"><img src='/resources/images/icon-delete.png'/></a>";
	    	return getFormatterControlGrid('btnDeleteGrid1', data);
		}
	};

var AttributeCustemerCatalogFormatter = {
		editCellIconFormatter: function(cellvalue, rowObject, options){		
			return "<a href='javascript:void(0)' class='btnGridEdit' onclick='AttributeCustomerManager.openAttributeCustomer("+options+");'><img src='/resources/images/icon-edit.png'/></a>";
		},
		editCellDetailIconFormatter: function(cellvalue, rowObject, options){		
			return "<a href='javascript:void(0)' class='btnGridDetailEdit' onclick='AttributeCustomerManager.openAttributeDetailCustomer("+options+");'><img src='/resources/images/icon-edit.png'/></a>";
		},
		viewCellDetailIconFormatter:function(cellvalue, rowObject, options){
			if(rowObject.valueType!=null || rowObject.valueType!=undefined){
				if(rowObject.valueType == 4 || rowObject.valueType == 5){
					return "<a href='javascript:void(0)' onclick='AttributeCustomerManager.loadDetail("+rowObject.attributeId+");'><img src='/resources/images/icon-view.png'/></a>";
				}
			}
		},
		statusFormat: function(cellvalue, rowObject, options){
			if(rowObject.status != null ||rowObject.status != undefined ){
				if(rowObject.status == 0 ){
					return "Tạm ngưng";
				}else{
					return "Hoạt động";
				}
			}else{
				return "";
			}
		},
		valueTypeFormat:function(cellvalue, rowObject, options){
			if(rowObject.valueType!=null || rowObject.valueType != undefined){
				if(rowObject.valueType == 1){
					return "Chữ";
				}else if(rowObject.valueType==2){
					return "Số";
				}else if(rowObject.valueType == 3){
					return "Ngày tháng";
				}else if(rowObject.valueType == 4){
					return "Danh sách(chọn một)";
				}else if(rowObject.valueType == 5){
					return "Danh sách(chọn nhiều)";
				}else{
					return "";
				}
			}
		},
		objectFormat:function(cellvalue, rowObject, options) {
			if (rowObject.tableName != null || rowObject.tableName != undefined) {
				if (rowObject.tableName == 'CUSTOMER') {
					return "Khách hàng";
				} else if (rowObject.tableName == 'PRODUCT') {
					return "Sản phẩm";
				} else if (rowObject.tableName == 'STAFF') {
					return "Nhân viên";
				} else {
					return "";
				}
			}
		}
//		editCategoryFormatter: function(cellvalue, options, rowObject){
//			return "<a href='javascript:void(0)' onclick=\"return StaffCatalog.getSelectedCategory('"+ rowObject.id +"');\"><img src='/resources/images/icon-edit.png'/></a>";
//		},
//		delCategoryFormatter: function(cellvalue, options, rowObject){
//			return "<a href='javascript:void(0)' onclick=\"return StaffCatalog.deleteCategory('"+ rowObject.id +"')\"><img src='/resources/images/icon-delete.png'/></a>";
//		},
//		priceFormat: function(cellvalue, options, rowObject){			
//			if(rowObject.total != null ||rowObject.total != undefined ){
//				return formatCurrency(rowObject.total);
//			}else{
//				return "";
//			}
//		},
//		typeFormatter: function(cellvalue, options, rowObject){
//			var content = '';
//			if(rowObject.actionType == 'INSERT'){
//				return 'Tạo mới ';
//			} else if(rowObject.actionType == 'UPDATE'){
//				return 'Cập nhật';
//			} else if(rowObject.actionType == 'DELETE'){
//				return 'Xóa';
//			}				
//			return cellvalue;
//		},
//		viewCellIconFormatter: function(cellvalue, options, rowObject){
//			return "<a href='javascript:void(0)' onclick=\"return StaffCatalog.getDetailChangedGridUrl('"+ rowObject.id +"');\"><img src='/resources/images/icon-view.png'/></a>";
//		},
//		delSupervisorFormatter: function(cellvalue, options, rowObject){
//			if ($('#staffSuper').val() != "1") return "";
//			return "<a href='javascript:void(0)' onclick=\"return StaffCatalog.deleteSupervisor('"+ rowObject.id +"')\"><img src='/resources/images/icon-delete.png'/></a>";
//		}
	};
var SalePlanFormatter = {
	quantityLeftFormatter: function(value, rowData, rowIndex) {
		var staffCode = $('#staffCode').val().trim();
			if (value != undefined && rowData.staffQuantity != undefined) {
				return (rowData.shopQuantity - rowData.shopQuantityAssign) + "/" + rowData.shopQuantity;
			} else {
				return '';
			}
	}
};
var SearchSaleTransaction = {
	editOrder:function(cellValue, rowObject,index) {
		var locked = $('#lock').val();
		if(locked == "false"){
			return "<a class='orderDetailTooltip' id='soDetail"+index+"' index='"+index+"' href='/sale-product/adjust-order/info?orderId="+rowObject.id+"'><img width='16' height='16' src='/resources/images/icon_edit.png'/></a>";
		}
	},
	customer:function(c,r,i){
		return '<a href="javascript:void(0)" id="custDetail'+i+'" index="'+i+'" class="easyui-tooltip" onclick="SPSearchSale.showCustomerInfor('+r.customer.id+')" class="aTagCustomerCode" name="'+r.customer.id+'">&nbsp&nbsp'+ Utils.XSSEncode(r.customer.shortCode) +' - '+ Utils.XSSEncode(r.customer.customerName) +'</a>';
	},
	status:function(c,r,i){
		if(r.type=='RETURNED'){
			return 'Đã trả';
		}else if(r.approved=='APPROVED'){
			return 'Đã duyệt';
		}else if(r.approved=='NOT_YET_APPROVE'){
			return 'Chưa duyệt';
		}else return 'Khác';
	},
	total:function(c,r,i){
		return formatCurrency(r.total);
	},
	nvbh:function(c,r,i){
		if(r.staff != null && r.staff != undefined) {
			return Utils.XSSEncode(r.staff.staffCode);
		} else {
			return '';
		}
	},
	nvgh:function(c,r,i){
		if(r.delivery != null && r.delivery != undefined) {
			return Utils.XSSEncode(r.delivery.staffCode);
		} else {
			return '';
		}
	},
	orderType:function(c,r,i){
		if(r.orderType=='IN'){
			return 'Đơn bán Presale';
		}else if(r.orderType=='SO'){
			return 'Đơn bán Vansale ';
		}else if(r.orderType=='CM'){
			return 'Đơn trả Presale';
		}else if(r.orderType=='TT'){
			return 'Trả thưởng';
		}else return 'Đơn trả Vansale ';
	}
};
/**
 * Loai giao dich
 * 
 * @author hunglm16
 * @since December 17,2014
 * @description Quan Ly Thiet Bi
 * @params: 0: Giao nhan, 1: Chuyen kho, 2: Sua chua, 3: Bao mat, 4: Bao mat tu mobile, 5: Thu hoi thanh ly, 6: Thanh ly
 * */
var tradeTypeEquipment = {ALL: -2, GN: 0, CK: 1, SC: 2, BM: 3, BMTMB: 4, THTL: 5, TL: 6, KK: 7, TTSC: 8,
	parseValue: function(value){
		if(value == undefined || value == null){
			return '';
		}else if(value == tradeTypeEquipment.ALL){
			return 'Tất cả';
		} else if(value == tradeTypeEquipment.GN){
			return 'Giao nhận';
		} else if(value == tradeTypeEquipment.CK){
			return 'Chuyển kho';
		} else if(value == tradeTypeEquipment.SC){
			return 'Sửa chữa';
		} else if(value == tradeTypeEquipment.BM){
			return 'Báo mất';
		} else if(value == tradeTypeEquipment.BMTMB){
			return 'Báo mất từ Mobile';
		} else if(value == tradeTypeEquipment.THTL){
			return 'Thu hồi thanh lý';
		} else if(value == tradeTypeEquipment.TL){
			return 'Thanh lý';
		} else if(value == tradeTypeEquipment.KK){
			return 'Kiểm kê';
		} else if(value == tradeTypeEquipment.TTSC){
			return 'Thanh toán sửa chữa';
		}
		return '';
	}
};

/**
 * Trang thai giao dich
 * 
 * @author hunglm16
 * @since December 17,2014
 * @description Quan Ly Thiet Bi
 * @params: 0: Khong giao dich, 1: Dang giao dich
 * */
var tradeStatusEquipment = {ALL: -2, STOPPED: 0, RUNNING: 1,
	parseValue: function(value){
		if(value == undefined || value == null){
			return '';
		}else if(value == tradeStatusEquipment.ALL){
			return 'Tất cả';
		} else if(value == tradeStatusEquipment.STOPPED){
			return 'Không giao dịch';
		} else if(value == tradeStatusEquipment.RUNNING){
			return 'Đang giao dịch';
		}
		return '';
	}
};

/**
 * Trang thai thiet bi
 * 
 * @author hunglm16
 * @since December 17,2014
 * @description Quan Ly Thiet Bi
 * @params: 0: Đã mất, 1: Đã thanh lý, 2: Đang ở kho, 3: Đang sử dụng, 4: Đang sửa chữa
 * */
var usageStatusEquipment = {ALL: -2, DM: 0, DTL: 1, DOK: 2, DSD: 3, DSC: 4,
	parseValue: function(value){
		if(value == undefined || value == null){
			return '';
		} else if(value == usageStatusEquipment.ALL){
			return 'Tất cả';
		} else if(value == usageStatusEquipment.DM){
			return 'Đã mất';
		} else if(value == usageStatusEquipment.DTL){
			return 'Đã thanh lý';
		} else if(value == usageStatusEquipment.DOK){
			return 'Đang ở kho';
		} else if(value == usageStatusEquipment.DSD){
			return 'Đang sử dụng';
		} else if(value == usageStatusEquipment.DSC){
			return 'Đang sửa chữa';
		}
		return '';
	},
	parseValueText: function(value){
		if(value == undefined || value == null){
			return '';
		} else if(value.trim() === 'LOST'){
			return 'Đã mất';
		} else if(value.trim() === 'LIQUIDATED'){
			return 'Đã thanh lý';
		} else if(value.trim() === 'SHOWING_WAREHOUSE'){
			return 'Đang ở kho';
		} else if(value.trim() === 'IS_USED'){
			return 'Đang sử dụng';
		} else if(value.trim() === 'SHOWING_REPAIR'){
			return 'Đang sửa chữa';
		}
		return '';
	},
	parseName: function(value){
		if(value == undefined || value == null){
			return -2;
		} else if(value.trim() === 'LOST'){
			return usageStatusEquipment.DM;
		} else if(value.trim() === 'LIQUIDATED'){
			return usageStatusEquipment.DTL;
		} else if(value.trim() === 'SHOWING_WAREHOUSE'){
			return usageStatusEquipment.DOK;
		} else if(value.trim() === 'IS_USED'){
			return usageStatusEquipment.DSD;
		} else if(value.trim() === 'SHOWING_REPAIR'){
			return usageStatusEquipment.DSC;
		}
		return -2;
	}
};

/**
 * Trang thai bien ban
 * 
 * @author hunglm16
 * @since December 30,2014
 * @description Quan Ly Thiet Bi
 * @params: 0: Du thao, 1: Chua duyet, 2: Da duyet, 3: Khong duyet, 4: Huy, 5: Dang sua chua, 6: Hoan tat sua chua , 7: Thanh lý
 * */
var statusRecordsEquip = {ALL: -2, DT: 0, CD: 1, DD: 2, KD: 3, HUY: 4, DSC: 5, HTSC: 6, TL: 7,
		parseValue: function(value){
			if(value == undefined || value == null){
				return '';
			} else if(value == statusRecordsEquip.ALL){
				return 'Tất cả';
			} else if(value == statusRecordsEquip.DT){
				return 'Dự thảo';
			} else if(value == statusRecordsEquip.CD){
				return 'Chờ duyệt';
			} else if(value == statusRecordsEquip.DD){
				return 'Duyệt';
			} else if(value == statusRecordsEquip.KD){
				return 'Không duyệt';
			} else if(value == statusRecordsEquip.HUY){
				return 'Hủy';
			}else if(value == statusRecordsEquip.DSC){
				return 'Đang sửa chữa';
			}else if(value == statusRecordsEquip.HTSC){
				return 'Hoàn tất sửa chữa';
			}else if(value == statusRecordsEquip.TL){
				return 'Đã thanh lý';
			}
			return '';
		},
		parseName: function(value){
			if(value == undefined || value == null){
				return -2;
			} else if(value == 'ALL'){
				return 'Tất cả';
			} else if(value == 'DRAFT'){
				return 'Dự thảo';
			} else if(value == 'WAITING_APPROVAL'){
				return 'Chưa duyệt';
			} else if(value == 'APPROVED'){
				return 'Đã duyệt';
			} else if(value == 'NO_APPROVAL'){
				return 'Duyệt';
			} else if(value == 'CANCELLATION'){
				return 'Hủy';
			}else if(value == 'ARE_CORRECTED'){
				return 'Đang sửa chữa';
			}else if(value == 'COMPLETION_REPAIRS'){
				return 'Hoàn tất sửa chữa';
			}else if(value == 'LIQUIDATED'){
				return 'Đã thanh lý';
			}
			return -2;
		}
	};
/**
 * Trang thai giao nhan
 * 
 * @author hunglm16
 * @since December 30,2014
 * @description Quan Ly Thiet Bi
 * @params: 0: Chua gui, 1: Da gui, 2: Da nhan
 * */
var deliveryStatus = {ALL: -2, CG: 0, DG: 1, DN: 2,
		parseValue: function(value){
			if(value == undefined || value == null){
				return '';
			} else if(value == deliveryStatus.ALL){
				return 'Tất cả';
			} else if(value == deliveryStatus.CG){
				return 'Chưa gửi';
			} else if(value == deliveryStatus.DG){
				return 'Đã gửi';
			} else if(value == deliveryStatus.DN){
				return 'Đã nhận';
			}
			return '';
		},
		parseName: function(value){
			if(value == undefined || value == null){
				return -2;
			} else if(value == 'ALL'){
				return 'Tất cả';
			} else if(value == 'NOTSEND'){
				return 'Chưa gửi';
			} else if(value == 'SENT'){
				return 'Đã gửi';
			} else if(value == 'RECEIVED'){
				return 'Đã nhận';
			}
			return -2;
		}
	};
/**
 * Trang thai bien ban kiem ke
 * 
 * @author nhutnn
 * @since 28/01/2015
 */
var statisticStatus = {DT: 0, HD: 1, HT: 2, HUY: 3,
		parseValue: function(value){
			if(value == undefined || value == null){
				return '';
			} else if(value == statisticStatus.DT){
				return 'Dự thảo';
			} else if(value == statisticStatus.HD){
				return 'Hoạt động';
			} else if(value == statisticStatus.HT){
				return 'Hoàn thành';
			} else if(value == statisticStatus.HUY){
				return 'Hủy';
			}
			return '';
		},
		parseName: function(value){
			if(value == undefined || value == null){
				return -2;
			} else if(value == 'DRAFT'){
				return 'Dự thảo';
			} else if(value == 'RUNNING'){
				return 'Hoạt động';
			} else if(value == 'DONE'){
				return 'Hoàn thành';
			} else if(value == "DELETED"){
				return 'Hủy';
			}
			return -2;
		}
	};
/**
 * Trang thai kiem ke
 * 
 * @author nhutnn
 * @since 02/02/2015
 */
var InventoryStatus = {CKK: 0, CON: 2, MAT: 3,
		parseValue: function(value){
			if(value == undefined || value == null){
				return '';
			} else if(value == InventoryStatus.CKK){
				return 'Chưa kiểm kê';
			} else if(value == InventoryStatus.CON){
				return 'Còn';
			} else if(value == InventoryStatus.MAT){
				return 'Mất';
			} 
			return '';
		},
		parseName: function(value){
			if(value == undefined || value == null){
				return -2;
			} else if(value == 'NOT_INVENTORY'){
				return 'Chưa kiểm kê';
			} else if(value == 'STILL'){
				return 'Còn';
			} else if(value == 'LOST'){
				return 'Mất';
			}
			return -2;
		}
	};

/**
 * Loai chi tiet nhom san pham doanh so CTTB
 * 
 * @author nhutnn
 * @since 04/03/2015
 */
var DisplayGroupDtlObjectType = {SP: 1, NG: 2, 
		parseValue: function(value){
			if(value == undefined || value == null){
				return '';
			} else if(value == statisticStatus.SP){
				return 'Sản phẩm';
			} else if(value == statisticStatus.NG){
				return 'Ngành';
			} 
			return '';
		},
		parseName: function(value){
			if(value == undefined || value == null){
				return -2;
			} else if(value == 'PRODUCT'){
				return 'Sản phẩm';
			} else if(value == 'PRODUCT_INFO'){
				return 'Ngành';
			}
			return -2;
		}
	};

/*
 * END OF FILE - /web/web/resources/scripts/utils/vnm.gridFormatter.js
 */

/*
 * START OF FILE - /web/web/resources/scripts/utils/vnm.stockGridFormatter.js
 */
var IssuedStockGridFormatter = {
	instockCellFormatter: function(cellvalue, rowObject,options){
		var bigUnit = 0;
		var smallUnit = 0;
		var convfact = 1;
		if(rowObject.convfact!= null && rowObject.convfact!= undefined){
			convfact = rowObject.convfact;
		}
		bigUnit = parseInt(rowObject.availableQuantity/convfact);
		smallUnit = rowObject.availableQuantity%convfact;
		return bigUnit + '/' + smallUnit;	
	},
	instockCellFormatterNew: function(cellvalue, rowObject,options){
		var bigUnit = 0;
		var smallUnit = 0;
		var convfact = 1;
		if(rowObject.convfact!= null && rowObject.convfact!= undefined){
			convfact = rowObject.convfact;
		}
		bigUnit = parseInt(rowObject.quantity/convfact);
		smallUnit = rowObject.quantity%convfact;
		return bigUnit + '/' + smallUnit;	
	},
	amountCellFormatter: function(value, row, index){		
		return '<input index='+index+' row-id="'+row.id+'" id="productRow_'+ row.id +'" size="5" type="text" maxlength="8" style="text-align:right;margin: 0;width:77px;" onkeypress="return NextAndPrevTextField(event, this,\'SelProduct\');" class="IssuedStock_AmountValue InputTextStyle InputText1Style SelProduct" onblur="StockIssued.amountBlured('+ index +');" onchange="StockIssued.amountChanged('+ index +');">';
	}
};
var UpdateStockGridFormatter = {
	amountCellFormatter: function(value, row, index){		
		return '<input index='+index+' row-id="'+row.id+'" id="productRow_'+ row.id +'" size="5" type="text" maxlength="8" style="text-align:right;margin: 0;width:77px;" onkeypress="return NextAndPrevTextField(event, this,\'SelProduct\');" class="IssuedStock_AmountValue InputTextStyle InputText1Style SelProduct" onblur="StockUpdate.qtyChanged('+ index +');" onchange="StockUpdate.qtyChanged('+ index +');">';
	},
	instockCellFormatter: function(cellvalue, rowObject,options){	
		var convfact = 1;
		if(rowObject.convfact!= null && rowObject.convfact!= undefined){
			convfact = rowObject.convfact;
		}
		var amount = formatQuantity(rowObject.availableQuantity,convfact);
		if(amount.length==0 || amount=='0/0' || amount=='0'){
			return '';
		}
		return amount;	
	}
};
var UntilityStockedLock = {
		vanLock:function(cellvalue, rowObject,options){		
			 if(rowObject.vanLock != null && rowObject.vanLock == '1'){
				return "Khóa";
			}else if(rowObject.vanLock == null || rowObject.vanLock == '0'){
				return "Không khóa";
			}else{
				return rowObject.vanLock;
			}
		},
};
var ManageTransFormatter = {
	viewDetail:function(cellValue, rowObject,options){
		var stockTransType = "";
		if(rowObject.fromOwnerType == 1 && rowObject.toOwnerType == 2){
			stockTransType =  "XUAT_KHO_NHAN_VIEN";
		}else if(rowObject.fromOwnerType == 2 && rowObject.toOwnerType == 1){
			stockTransType = "NHAP_KHO_NHAN_VIEN";
		}else if(rowObject.fromOwnerType == 1 && rowObject.toOwnerType == null){
			stockTransType =   "XUAT_KHO_DIEU_CHINH";
		}else if(rowObject.fromOwnerType == null && rowObject.toOwnerType == 1){
			stockTransType =   "NHAP_KHO_DIEU_CHINH";
		} else if (rowObject.fromOwnerType == 1 && rowObject.toOwnerType == 1) {
			stockTransType =   "DIEU_CHUYEN_KHO";
		}
		return "<span style='cursor:pointer' title =\"Xem chi tiết " + Utils.XSSEncode(rowObject.stockTransCode) + "\" onclick=\"return StockManageTrans.viewDetail('"+ Utils.XSSEncode(rowObject.stockTransCode) +"','"+ Utils.XSSEncode(rowObject.fromStockCode) +"','"+ Utils.XSSEncode(rowObject.toStockCode) +"','"+ Utils.XSSEncode(rowObject.stockTransDateStr) +"','"+ Utils.XSSEncode(stockTransType) +"','" + Utils.XSSEncode(rowObject.stockTransId) + "');\"><img src='/resources/images/icon-view.png'/></span>";
	},
	transTypeFormat:function(cellvalue, rowObject,options){
		if(rowObject.fromOwnerType == 1 && rowObject.toOwnerType == 2){
			return "Đơn bán hàng vansale(DB)";
		}else if(rowObject.fromOwnerType == 2 && rowObject.toOwnerType == 1){
			return "Đơn trả hàng vansale(GO)";
		}else if(rowObject.fromOwnerType == 1 && rowObject.toOwnerType == null){
			return "Xuất kho điều chỉnh";
		}else if(rowObject.fromOwnerType == null && rowObject.toOwnerType == 1){
			return "Nhập kho điều chỉnh";
		} else if (rowObject.fromOwnerType == 1 && rowObject.toOwnerType == 1) {
			return "Điều chuyển kho";
		} else {
			return "";
		}
	},
	amountFormat:function(cellValue, options, rowObject){
		return formatCurrency(cellValue);
	}
};
var ReceivedStockFormatter = {
		amountCellFormatter: function(cellvalue, options, rowObject){		
			return '<input product-id="'+options.rowId+'" id="productRow_'+ rowObject.productId +'" size="12" type="text" style="text-align:right" onkeypress="return NextAndPrevTextField(event, this,\'SelProduct\');" class="IssuedStock_AmountValue InputTextStyle InputText1Style SelProduct" onblur="StockIssued.amountBlured('+ rowObject.productId +');" onchange="StockIssued.amountChanged('+ rowObject.productId +');">';
		}	
};
var CountingFormatter = {
	viewDetail:function(cellvalue, rowObject,options){
		return "<a href='javascript:void(0);'><span style='cursor:pointer' onclick=\"return StockCounting.viewDetail('"+ rowObject.id +"');\">"+ Utils.XSSEncode(rowObject.cycleCountCode) +"</span></a>";
	},
	editFormat:function(cellvalue, rowObject,options){
		if(rowObject.status != null && rowObject.status == '1'|| rowObject.status == 1){
			return "";
		}
		if(rowObject.status != null && rowObject.status == '2'|| rowObject.status == 2){
			return "";
		}
		if(rowObject.status != null && (rowObject.status != '1' || rowObject.status == 1) && (rowObject.status != '2' || rowObject.status != 2)){
			if(rowObject.canUpdate == '1' || rowObject.canUpdate == 1){
				return '<span style="cursor:pointer" onclick="window.location.href=\'/stock/counting/changed?cycleCountId='+rowObject.id+'\'"><img src="/resources/images/icon-edit.png"/></span>';
			}
		}
		return "";
	},
	deleteFormat:function(cellvalue, rowObject,options){
		if(rowObject.status != null && (rowObject.status == '2' || rowObject.status == 2) || (rowObject.status == '3' || rowObject.status == 3)){
			return "<span style='cursor:pointer' onclick=\"return StockCounting.deleteRow('"+ rowObject.id +"');\"><img src='/resources/images/icon-delete.png'/></span>";
		} else {
			return "";
		}
//		if(rowObject.status != null){
//			if(rowObject.canUpdate == '1' || rowObject.canUpdate == 1){
//				return "<span style='cursor:pointer' onclick=\"return StockCounting.deleteRow('"+ rowObject.id +"');\"><img src='/resources/images/icon-delete.png'/></span>";
//			}
//		}
		return "";
	},
	statusFormat:function(cellvalue, rowObject,options){		
		if(rowObject.status != null && rowObject.status == '0'|| rowObject.status == 0){
			return "Đang thực hiện";
		}else if(rowObject.status != null && rowObject.status == '1'|| rowObject.status == 1){
			return "Đã duyệt";
		}else if(rowObject.status != null && rowObject.status == '2'|| rowObject.status == 2){
			return "Hủy bỏ";
		}else if(rowObject.status != null && rowObject.status == '3'|| rowObject.status == 3){
			return "Từ chối";
		}else if(rowObject.status != null && rowObject.status == '4'|| rowObject.status == 4){
			return "Chờ duyệt";
		}else if(rowObject.status != null && rowObject.status == '5'|| rowObject.status == 5){
			return "Chưa kiểm kê";
		}else{
			return rowObject.status;
		}
	},
	statusFormat2:function(cellvalue, rowObject,options){		
		if(rowObject.status == 'ONGOING'){
			return "Đang thực hiện";
		}else if(rowObject.status == 'COMPLETED'){
			return "Đã duyệt";
		}else if(rowObject.status == 'CANCELLED'){
			return "Hủy bỏ";
		}else if(rowObject.status == 'REJECTED'){
			return "Từ chối";
		}else if(rowObject.status == 'WAIT_APPROVED'){
			return "Chờ duyệt";
		}else if(rowObject.status == 'NOT_COUNTING'){
			return "Chưa kiểm kê";
		}else{
			return rowObject.status;
		}
	},
	quantityConvert:function(cellvalue, rowObject,options ){
		return formatQuantityEx(rowObject.quantity,rowObject.convfact);
	}
};
var StockCategoryFormatter = {
	delProductFormatter: function(cellvalue, options, rowObject){
		return "<span style='cursor:pointer' onclick=\"return StockCategory.delSelectedRow('"+ rowObject.id +"')\"><img src='/resources/images/icon-delete.png'/></span>";
	},
	chooseCellFormatter: function(cellvalue, options, rowObject){		
		return '<input id="productRow_'+ options.rowId +'" type="checkbox" style="text-align:right" class="InputTextStyle InputText1Style SelProduct selectProduct" value="'+options.rowId +'" >';
	}
};
var StockValidateInput = {
		getQuantity: function(amount, convfact){
			if(amount==undefined || amount==null){
				return 0;
			}
			if(convfact != undefined && convfact == 0){
				convfact = 1;
			}			
			var bigUnit = 0;
			var smallUnit = 0;	
			amount = new String(amount);
			if(amount.indexOf('/') == -1){			
				smallUnit = Number(Utils.returnMoneyValue(amount).trim());
			}else{
				var arrCount = amount.split('/');
				if(arrCount.length > 0){
					if(arrCount[0].trim().length==0){
						bigUnit = 0;
					}else{
						bigUnit = parseInt(Utils.returnMoneyValue(arrCount[0]).trim());
					}
					if(arrCount[1].trim().length==0){
						smallUnit = 0;
					}else{
						smallUnit = parseInt(Utils.returnMoneyValue(arrCount[1]).trim());
						if(smallUnit>=convfact){
							var addBig = parseInt(smallUnit/convfact,10);
							var mod = smallUnit%convfact;
							bigUnit = bigUnit + addBig;
							smallUnit = mod;
						}	
					}					
				}
			}		
			return bigUnit*convfact + smallUnit;
		},
		formatStockQuantity:function(amount,convfact){
			if(amount==null || amount==undefined || amount.toString().length==0){
				return '';
			}
			if(convfact==null || convfact==undefined || convfact.toString().length==0){
				return '';
			}
			amount = amount.toString();
			if(amount.indexOf('/')>=0){
				var arrCount = amount.split('/');
				if(arrCount.length>=3){
					return '';
				}else{
					if(amount.split('/').length <= 1){
						return parseInt(amount,10) + '/0';						
					}
					if(amount.split('/')[0].length==0 || isNaN(amount.split('/')[0])){
						return '0' + '/' + parseInt(amount.split('/')[1],10);
					}
					if(amount.split('/')[1].length==0 || isNaN(amount.split('/')[1])){
						return parseInt(amount.split('/')[0],10) + '/0';
					}
					if(isNaN(amount.split('/')[0]) || isNaN(amount.split('/')[1])){
						return '0/0';
					}
					if(amount.split('/')[1]>convfact){
						var amount_temp = parseInt(amount.split('/')[0],10) + parseInt(amount.split('/')[1]/convfact);
						return amount_temp + '/' + parseInt(amount.split('/')[1])%convfact;
					}					
					return parseInt(amount.split('/')[0],10) + '/' + parseInt(amount.split('/')[1],10);	
				}
			}else{	
				if(isNaN(amount)){
					return '0/0';
				}
				if(Math.abs(parseInt(amount))>=convfact){
					var amount_temp = parseInt(amount/convfact,10);
					var amount_mod = amount%convfact;
					amount = parseInt(amount_temp,10) + '/' + parseInt(amount_mod,10);	
				}else{
					amount = '0/' + parseInt(amount,10);		
				}				
			}	
			return amount;
		},
		formatStockQuantityNotConvert:function(amount){
			if(amount==null || amount==undefined || amount.toString().length==0){
				return '';
			}
			amount = amount.toString();
			if(amount.indexOf('/') < 0){
				amount = '0/' + parseInt(amount,10);			
			}	
			return amount;
		}
};
/*
 * END OF FILE - /web/web/resources/scripts/utils/vnm.stockGridFormatter.js
 */

/*
 * START OF FILE - /web/web/resources/scripts/utils/vnm.superviseshopGridFormatter.js
 */
var ManageRouteCreateFormatter = {
	getStaffSale : function(cellValue, rowObject, index) {
		if(rowObject.staffCode == null || rowObject.staffCode == 'null')
    		rowObject.staffCode = "";
    	if(rowObject.staffName == null || rowObject.stafName == 'null')
    		rowObject.staffName = "";
    	if(rowObject.staffCode != "" && rowObject.staffName != "")
    		return Utils.XSSEncode(rowObject.staffCode + ' - ' + rowObject.staffName);
    	return "";
	},
	setOrderRoute:function(v,r,i){
		return "<a href='/superviseshop/manageroute-create/order/info?routingId="+r.routingId+"' title='" + Utils.XSSEncode(create_route_set_order_permission) + "'><img width='16' height='16' src='/resources/images/icon_mapedit.png'/></a>";
	},
	assignRoute:function(v,r,i){
		return "<a href='/superviseshop/manageroute-create/assign/info?routingId="+r.routingId+"' title='" + Utils.XSSEncode(create_route_assign_staff_route) + "'><img width='16' height='16' src='/resources/images/icon_addTBHV.png'/></a>";
	},
	deleteRoute : function(cellValue, rowObject,index) {
		return "<a href='javascript:void(0)' onclick=\"return SuperviseManageRouteCreate.deleteRouting("+ rowObject.routingId +",\'"+ Utils.XSSEncode(rowObject.routingCode) +"\',\'"+ Utils.XSSEncode(rowObject.staffCode) +"\')\" title='" + jsp_common_xoa + "'><img src='/resources/images/icon-delete.png' width='16' height='16' /></a>";
	},
	editRoute : function(cellValue, rowObject,index) {
		return "<a href='/superviseshop/manageroute-create/editroute?routeId="+rowObject.routingId+"&shopCode=" + Utils.XSSEncode(rowObject.shopCode) + "' title='" + jsp_common_sua + "'><img width='16' height='16' src='/resources/images/icon_edit.png'/></a>";
	},
	statusFormatter :function(value, rowData, rowIndex){
		if(value == activeType.RUNNING){
			return jsp_common_status_active;
		} else if(value == activeType.STOPPED){
			return jsp_common_status_stoped;
		} 
		return '';
	},
	checkCustomerDialog : function(value, rowData, rowIndex) {
		var param = '\''+ Utils.XSSEncode(rowData.shortCode) +'\',' + rowData.id +',\''+ Utils.XSSEncode(rowData.customerCode) +'\',\'' 
		+ Utils.XSSEncode(rowData.customerName) +'\',\'' + Utils.XSSEncode(rowData.address) +'\','+ rowData.lat +',' + rowData.lng ;
		var obj = SuperviseManageRouteCreate._mapCustomerDialog.get(rowData.shortCode);
		if(obj == null) {
			return '<input type="checkbox" id="'+ Utils.XSSEncode(rowData.shortCode) +'" onchange="SuperviseManageRouteCreate.changeCheckBoxCustomerDialog(this.checked ,'+param+')"/>';
		} else{
			return '<input checked="checked" type="checkbox"  id="'+ Utils.XSSEncode(rowData.shortCode) +'" onchange="SuperviseManageRouteCreate.changeCheckBoxCustomerDialog(this.checked ,'+param+')"/>';
		}
	}
};
var ManageSalePlanFormatter = {
	getTrain : function(cellValue, options, rowObject) {
	   	if(cellValue == 'GO'){
	   		return "<img src='/resources/images/icon-datach.png' />";
	   	}
	   	return '';
	}
};
var ManageRouteAssign = {
	editCellIconFormatter : function(cellValue, options, rowObject) {
		return "<a title='Xem thông tin tuyến' href='/superviseshop/manageroute-create/assign/routing-detail?routeId="+rowObject.routingId+"'><span style='cursor:pointer'><img src='/resources/images/icon-edit.png'/></span></a>";
	},
	statusStaffFormatter :function(value, rowData, rowIndex){
		if(value == activeStatus){
			return jsp_common_status_active;
		} else if(value == stoppedStatus){
			return jsp_common_status_stoped;
		} 
		return '';
	},
	editStaffInRouting: function(value, rowData, rowIndex) {
		var fromDate = ''; 
		var toDate = '';
		var status = 1;
		if(rowData.fromDate != null && rowData.fromDate != '') fromDate = $.datepicker.formatDate('dd/mm/yy', new Date(rowData.fromDate));
		if(rowData.toDate != null && rowData.toDate != '') toDate = $.datepicker.formatDate('dd/mm/yy', new Date(rowData.toDate));
		if(rowData.status == stoppedStatus) status =0 ;
		if(rowData.toDate != null && Utils.compareCurrentDateExReturn($.datepicker.formatDate('dd/mm/yy', new Date()),toDate) >0){
			return '';
		}
		var param = rowData.id+','+rowData.staff.id + ',\''+ fromDate+ '\',\''+ toDate+'\','+status;
		return '<a href="javascript:void(0)" onclick="SuperviseManageRouteAssign.editStaff('+param+')"><img src="/resources/images/icon_edit.png" width="15" height="16" title="' + jsp_common_sua + '"></a>';
	},
	deleteStaffInRouting : function(value, rowData, rowIndex) {
		return '<a href="javascript:void(0)" onclick="SuperviseManageRouteAssign.deleteStaff('+ rowData.id+')"><img src="/resources/images/icon_delete.png" width="15" height="16" title="' + jsp_common_xoa + '"></a>';
	},
	checkDate  : function(value, rowData, rowIndex) {
		if(value =='GO') {
			return '<img src="/resources/images/icon_check.png"/>';
		}
		return '';
	},
	formatStartWeek :function(value, rowData, rowIndex) {
		var date = new Date();
		if(value != undefined && value != null) {
			return  getFirstDayOfWeek(value,date.getFullYear());
		}
		return '';
	}
};
var VisitPlanFormatter ={
	showMap:function(cellValue, options, rowObject){
		return "<span style='cursor:pointer' onclick=\"return VisitResult.showMap("+"'"+ Utils.XSSEncode(rowObject.staffCode) +"'"+");\"><img src=\"/resources/images/xembando.png\" /></span>";
	}
};
var SellerPositionFormatter ={
	getSellerPositionDetail:function(cellValue, options, rowObject){
		if(rowObject.hasPosition=='1'){
			return "<a href='javascript:void(0);' onclick=\"return SellerPosition.getSellerPositionDetail('"+ rowObject.staffId + "','" + Utils.XSSEncode(rowObject.staffCode) + "','" + Utils.XSSEncode(rowObject.staffName) + "','" + Utils.XSSEncode(rowObject.shopCode) + "',"+ Utils.XSSEncode(rowObject.objectType) +");\">" + Utils.XSSEncode(cellValue) + "</a>";
		}else{
			return Utils.XSSEncode(cellValue);
		}
	},
	getSellerType : function(cellValue, options, rowObject) {
	   	if(cellValue == "1"){
	   		return "NVBH";
	   	}else 	if(cellValue == "5"){
	   		return "GSNPP";
	   	}else 	if(cellValue == "7"){
	   		return "TBHV";
	   	}
	   	return "";
	}
};
/*
 * END OF FILE - /web/web/resources/scripts/utils/vnm.superviseshopGridFormatter.js
 */

/*
 * JavaScript file created by Rockstarapps Concatenation
*/

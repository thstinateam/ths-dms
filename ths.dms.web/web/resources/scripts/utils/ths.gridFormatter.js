var CustomerDebitAutoFormatter = {
		signedNumberFormatter:function(value,row,index) {
			if(value!=undefined && value!=null){
				if(value<0){
					return formatCurrency(value*(-1));
				}
				return formatCurrency(value);
			}
			return '';
		}
	};
var CustomerSearchEasyUIFormatter = {
		selectCellIconFormatterEasyUIDialog : function(value, rowData, rowIndex) {
			return "<a href='javascript:void(0)' onclick=\"return SPReturnOrder.getResultSeachStyle1EasyUIDialog("+ rowIndex + ");\">Chọn</a>";
		}
	};

var CommonSearchEasyUIFormatter = {
	selectSearchStyle2EasyUIDialog : function(value, rowData, rowIndex) {
		return "<a href='javascript:void(0)' onclick=\"CommonSearchEasyUI.getResultSearchStyle2EasyUIDialog("+ rowIndex + ");\">Chọn</a>";
	}
};
var PoAutoGroupFomatter = {
	statusFormatter : function(value, rowData, rowIndex) {
		if (value == activeStatus) {
			return jsp_common_status_active;
		} else if (value == stoppedStatus) {
			return jsp_common_status_stoped;
		}
		return '';
	}	
};
var CommonFormatter = {
		numberFormatter:function(cellValue, options, rowObject) {
			if(cellValue!=undefined && cellValue!=null){
				return formatCurrencyInterger(cellValue);
			}
			return '';
		},
		dateTimeFormatter:function(cellValue, options, rowObject) {
			if(cellValue!=undefined && cellValue!=null){				
				return $.datepicker.formatDate('dd/mm/yy', new Date(cellValue));
			}
			return '';
		},
		dateTimeFormatterEx:function(cellValue, options, rowObject) {
			if(cellValue!=undefined && cellValue!=null){	
				var d =  new Date(cellValue);
				var hh = d.getHours()<10? '0'+d.getHours():d.getHours();
				var mm = d.getMinutes()<10? '0'+d.getMinutes():d.getMinutes();
				var ss = d.getSeconds()<10? '0'+d.getSeconds():d.getSeconds();
				return $.datepicker.formatDate('dd/mm/yy', d) + ' ' +hh + ':' + mm + ':'+ ss;
			}
			return '';
		},
		formatNormalCell: function(val, row, index) {
			if (val != undefined && val != null && isNaN(val)) {
				return VTUtilJS.XSSEncode(val);
			} else if(val != undefined && val != null && !isNaN(val)) {
				return VTUtilJS.formatCurrency(val);
			}
			return "";
		},
		checkPermissionHideColumn: function(gridId, colunmField, idCheck) {
			if (_isFullPrivilege) {
				return;
			}
			var status = Number(_MapControl.get(idCheck));
			if(status == 1 || status == 3){
				$('td[field='+colunmField+'] .' + idCheck).remove();
				$("#"+gridId).datagrid("hideColumn", colunmField);
			}
		},
		checkPermissionHideGridButton: function(idCheck, isCssClass, prefix) {
			if (_isFullPrivilege) {
				return 0;
			}
			var t = "#";
			if (isCssClass) {
				t = ".";
			}
			if (prefix == undefined || prefix == null) {
				t = t + idCheck;
			} else {
				t = "#" + prefix + " " + t + idCheck;
			}
			var status = Number(_MapControl.get(idCheck));
			if(status == 1 || status == 3){
				$(t).remove();
				return 1;
			}
			return 0;
		}
};
var ReasonGroupCatalogFormatter = {
	editCellIconFormatter: function(cellvalue, options, rowObject){
		var status = 1;		
		if(rowObject.status != activeStatus){
			status = 0;
		}
		return "<a href='javascript:void(0)' onclick=\"return ReasonGroupCatalog.getSelectedReasonGroup("+ options.rowId + "," + status + ");\"><img src='/resources/images/icon-edit.png'/></a>";
	},
	delCellIconFormatter: function(cellvalue, options, rowObject){
		return "<a href='javascript:void(0)' onclick=\"return ReasonGroupCatalog.deleteReasonGroup('"+ Utils.XSSEncode(rowObject.reasonGroupCode) +"')\"><img src='/resources/images/icon-delete.png'/></a>";
	}
};

var GroupStaffPayrollFormatter = {
		viewDetail:function(cellValue, options, rowObject){
			if(rowObject.objectApply == 1){
				return "";
			}
			return "<span style='cursor:pointer' onclick=\"return GroupStaffPayroll.viewDetail('"+ Utils.XSSEncode(rowObject.groupStaffPrCode) + "','" + Utils.XSSEncode(rowObject.groupStaffPrName) + "');\"><img src='/resources/images/icon-view.png'/></span>";
																								//Tam thoi lay groupStaffPrCode
																								//Nhung minh nghi lay id se dung hon.
		},
		editCellIconFormatter: function(cellvalue, options, rowObject){
			/*var status = 1;		
			if(rowObject.status != activeStatus){
				status = 0;
			}*///Thuong nguoi ta edit record co trang thai "Du Thao", chu ko edit record trang thai "Hoat dong". Minh ko co nghiep vu nay. Hi hi.
			if(rowObject.isHasUsed == 0){
				return "<a href='javascript:void(0)' onclick=\"return GroupStaffPayroll.getSelectedRow("+ options.rowId + ");\"><img src='/resources/images/icon-edit.png'/></a>";
			}
			return "";
		},
		delGroupStaffPayrollFormatter: function(cellvalue, options, rowObject){
			if(rowObject.isHasUsed == 0){
				return "<a href='javascript:void(0)' onclick=\"return GroupStaffPayroll.deleteGroupStaffPayroll('"+ Utils.XSSEncode(rowObject.groupStaffPrCode) +"')\"><img src='/resources/images/icon-delete.png'/></a>";
			}
			return "";
		},
		delCellIconFormatter: function(cellvalue, options, rowObject){
			return "<a href='javascript:void(0)' onclick=\"return GroupStaffPayroll.deleteReasonGroup('"+ Utils.XSSEncode(rowObject.groupStaffPrCode) +"')\"><img src='/resources/images/icon-delete.png'/></a>";
		},
		checkInputFormatter: function(cellvalue, options, rowObject){
			return '<input type="checkbox" />';
		},
		forcusFormatter:function(cellvalue, options, rowObject){
			if(rowObject.isFocus=='0'|| rowObject.isFocus==0){
				return '<input type="checkbox"  />';
			}else{
				return '<input type="checkbox" checked="checked"  />';
			}
		},
		selectCellIconFormatter: function(cellvalue, options, rowObject){//
			return "<a href='javascript:void(0)' onclick=\"return PayrollTable.getgroupStaff('"+ Utils.XSSEncode(rowObject.groupStaffPrCode) +"');\">Chọn</a>";
		},
		formatObjectApply: function(cellvalue, options, rowObject){
			if(rowObject.objectApply == 1){
				return "Đơn vị";
			}else if(rowObject.objectApply == 2){
				return "Nhân viên";
			}
		},
		formatStaffType: function(cellvalue, options, rowObject){
			if(rowObject.channelTypeCode == null || rowObject.channelTypeCode == ""){
				return "Tất cả";
			}else{
				return Utils.XSSEncode(rowObject.channelTypeName);
			}
		}
	};


var GridFormatterUtils = {
	dateFromdateTodate :function(fromDate, toDate) {
		var kq = "";
		if(fromDate!=undefined && fromDate!=null && fromDate.length>0){
			kq = formatDate(fromDate.toString().trim());
			if(toDate!=undefined && toDate!=null && toDate.length>0){
				kq = kq + " - " + formatDate(toDate.toString().trim());
			}
		}else{
			if(toDate!=undefined && toDate!=null && toDate.length>0){
				kq = formatDate(toDate.toString().trim());
			}
		}
		return kq.trim();
	},
	statusCellIconFormatter: function(cellvalue, options, rowObject){		
		if(cellvalue == activeStatus){
			return jsp_common_status_active;
		} else if(cellvalue == stoppedStatus){
			return jsp_common_status_stoped;
		} else if(cellvalue == waitingStatus){
			return "Dự thảo";
		}
	},
	statusCellFormatter: function(cellvalue, options, rowObject){
		if(cellvalue == "ACTIVE"){
			return "Hoạt động";
		} else if(cellvalue == "ENDED"){
			return "Kết thúc";
		} else if(cellvalue == "DRAFT"){
			return "Dự thảo";
		}
	},
	currencyCellFormatter: function(cellvalue, rowObject, options){		
		if(cellvalue != null ||cellvalue != undefined ){
			return formatCurrency(cellvalue);
		}else{
			return "";
		}		
	},
	parentChannelTypeCellFormatter: function(data, row, index){
		if(row.parentChannelType != undefined && row.parentChannelType.channelTypeCode!= undefined){
			return Utils.XSSEncode(row.parentChannelType.channelTypeCode);
		} else{
			return '';
		}
	},
	staffObjectTypeCellFormatter: function(data, row, index){
		if(data!=null && data != undefined){
			if(data == 1){
				return "PreSale";
			}else if(data == 2){
				return "VanSale";
			}else if(data == 3){
				return "NVTT";
			}else if(data == 4){
				return "NVGH";
			}else if(data == 5){
				return "NVGS";
			}else if(data == 6){
				return "TBHM";
			}else if(data == 7){
				return "TBHV";
			}
		}
	}
};
var PackingFormatter = {
	edit: function(value, row, index){
		var status = 1;		
		if(row != activeStatus){
			status = 0;
		}
		return "<a href='javascript:void(0)' onclick=\"return PackingCatalog.getSelectedPacking("+ row.id + ",'"+ Utils.XSSEncode(row.productInfoCode) + "','"+ Utils.XSSEncode(row.productInfoName) + "','"+ Utils.XSSEncode(row.description) + "'," + status + ");\"><img src='/resources/images/icon-edit.png'/></a>";
	},
	remove: function(cellvalue, options, rowObject){
		return "<a href='javascript:void(0)' onclick=\"return PackingCatalog.deletePacking('"+ Utils.XSSEncode(rowObject.productInfoCode) +"')\"><img src='/resources/images/icon-delete.png'/></a>";
	}
};
var ReasonCatalogFormatter = {
	editCellIconFormatter: function(cellvalue, options, rowObject){
		var status = 1;
		var reasonGroupId = null;
		if(rowObject.status != activeStatus){
			status = 0;
		}
		if(rowObject.reasonGroup!= undefined){
			reasonGroupId = rowObject.reasonGroup.id;
		}
		return "<a href='javascript:void(0)' onclick=\"return ReasonCatalog.getSelectedReason("+ options.rowId + "," + status + ");\"><img src='/resources/images/icon-edit.png'/></a>";
	},
	delCellIconFormatter: function(cellvalue, options, rowObject){
		return "<a href='javascript:void(0)' onclick=\"return ReasonCatalog.deleteReason('"+ Utils.XSSEncode(rowObject.reasonCode) +"')\"><img src='/resources/images/icon-delete.png'/></a>";
	}
};
var BrandCatalogFormatter = {
	editCellIconFormatter: function(value, row, index){
		var status = 1;		
		if(row.status != activeStatus){
			status = 0;
		}
		return "<a href='javascript:void(0)' onclick=\"return BrandCatalog.getSelectedBrand("+ row.id + ",'"+ Utils.XSSEncode(row.productInfoCode) + "','"+ Utils.XSSEncode(row.productInfoName) + "','"+ Utils.XSSEncode(row.description) + "'," + status + ");\"><img src='/resources/images/icon-edit.png'/></a>";
	},
	delCellIconFormatter: function(cellvalue, options, rowObject){
		return "<a href='javascript:void(0)' onclick=\"return BrandCatalog.deleteBrand('"+ Utils.XSSEncode(rowObject.productInfoCode) +"')\"><img src='/resources/images/icon-delete.png'/></a>";
	}
};
var CategoryCatalogFormatter = {
	editCellIconFormatter: function(value, row, index){
		var status = 1;		
		if(row.status != activeStatus){
			status = 0;
		}
		return "<a href='javascript:void(0)' onclick=\"return CategoryCatalog.getSelectedCategory("+ row.id + ",'"+ Utils.XSSEncode(row.productInfoCode) + "','"+ Utils.XSSEncode(row.productInfoName) + "','"+ Utils.XSSEncode(row.description) + "'," + status + ");\"><img src='/resources/images/icon-edit.png'/></a>";
	},
	delCellIconFormatter: function(cellvalue, options, rowObject){
		return "<a href='javascript:void(0)' onclick=\"return CategoryCatalog.deleteCategory('"+ rowObject.productInfoCode +"')\"><img src='/resources/images/icon-delete.png'/></a>";
	}
};
var SubCategoryCatalogFormatter = {
		
	editCellIconFormatter: function(value, row, index){
		var status = 1;
		if(row.status != activeStatus){
			status = 0;
		}
		return "<a href='javascript:void(0)' onclick=\"return SubCategoryCatalog.getSelectedSubCategory("+ row.id + ",'"+ Utils.XSSEncode(row.productInfoCode) + "','"+ Utils.XSSEncode(row.productInfoName) + "','"+ Utils.XSSEncode(row.description) + "'," + status + ");\"><img src='/resources/images/icon-edit.png'/></a>";
	},
	delCellIconFormatter: function(cellvalue, options, rowObject){
		return "<a href='javascript:void(0)' onclick=\"return SubCategoryCatalog.deleteSubCategory('"+ Utils.XSSEncode(rowObject.productInfoCode) +"')\"><img src='/resources/images/icon-delete.png'/></a>";
	}
};
var SubCategorySecondaryFormatter = {
		editCellIconFormatter: function(value, row, index){
			var status = 1;
			if(row.status != activeStatus){
				status = 0;
			}
			var data = "<a href='javascript:void(0)' onclick=\"return SubCategorySecondary.getSelectedSubCategorySecondary("+ row.productInfoId + ",'"+ Utils.XSSEncode(row.parentCode) + "','"+ Utils.XSSEncode(row.productInfoCode) + "','"+ Utils.XSSEncode(row.productInfoName) + "'," + status + ");\"><img src='/resources/images/icon-edit.png'/></a>";
			return getFormatterControlGrid('btnEditGrid',data);
			//return "<a href='javascript:void(0)' onclick=\"return SubCategorySecondary.getSelectedSubCategorySecondary("+ row.productInfoId + ",'"+ row.parentCode + "','"+ row.productInfoCode + "','"+ row.productInfoName + "'," + status + ");\"><img src='/resources/images/icon-edit.png'/></a>";
		}
	};
var FlavourCatalogFormatter = {
	editCellIconFormatter: function(value, row, index){
		var status = 1;		
		if(row.status != activeStatus){
			status = 0;
		}
		return "<a href='javascript:void(0)' onclick=\"return FlavourCatalog.getSelectedRow("+ row.id + ",'"+ Utils.XSSEncode(row.productInfoCode) + "','"+ Utils.XSSEncode(row.productInfoName) + "','"+ Utils.XSSEncode(row.description) + "'," + status + ");\"><img src='/resources/images/icon-edit.png'/></a>";
	},
	delCellIconFormatter: function(cellvalue, options, rowObject){
		return "<a href='javascript:void(0)' onclick=\"return FlavourCatalog.deleteRow('"+ Utils.XSSEncode(rowObject.productInfoCode) +"')\"><img src='/resources/images/icon-delete.png'/></a>";
	}
};
var contractManagerFormatter = {
		editCellIconFormatter: function(cellvalue, options, rowObject){
			var status;
			var contractType;
			if (rowObject.contractType == "DEVICE_CONTRACT") {
				contractType = 1;
			} else {
				contractType = 0;
			}
			if(rowObject.status == "DRAFT"){
				status = 0;
				return "<a href='javascript:void(0)' " +
						"onclick=\"return ContractManagers.initContractDetail("+ options.rowId + ");\"><img src='/resources/images/icon-edit.png'/></a>";
			} else {
				if (rowObject.status == "ACTIVE") {
					status = 1;
				} else {
					status = 2;
				}
				return "<a href='javascript:void(0)' " +
				"onclick=\"return ContractManagers.initContractDetail("+ options.rowId +");\"><img src='/resources/images/icon-edit.png'/></a>";
			} 
		},
		customerFormatter: function(cellvalue, options, rowObject){
			if(rowObject.customer == null) {
				return "";
			} else if(rowObject.customer.shortCode == null && rowObject.customer.customerName != null) {
				return Utils.XSSEncode(rowObject.customer.customerName);
			} else if(rowObject.customer.shortCode != null && rowObject.customer.customerName == null) {
				return Utils.XSSEncode(rowObject.customer.shortCode);
			} else {
				return Utils.XSSEncode(rowObject.customer.shortCode + "-" + rowObject.customer.customerName);
			}
		},
		staffFormatter: function(cellvalue, options, rowObject){
			if(rowObject.staff == null) {
				return "";
			} else if(rowObject.staff.staffCode == null && rowObject.staff.staffName != null) {
				return Utils.XSSEncode(rowObject.staff.staffName);
			} else if(rowObject.staff.staffCode != null && rowObject.staff.staffName == null) {
				return Utils.XSSEncode(rowObject.staff.staffCode);
			} else {
				return Utils.XSSEncode(rowObject.staff.staffCode) + "-" + "<br>" + Utils.XSSEncode(rowObject.staff.staffName);
			}
		},
		delCellIconFormatter: function(cellvalue, options, rowObject){
			if (rowObject.status == "DRAFT") {
				return "<a href='javascript:void(0)' onclick=\"return ContractManagers.deleteRow('"+ rowObject.id +"')\"><img src='/resources/images/icon-delete.png'/></a>";
			} else {
				return "";
			}
		}
	};
var UnitTreeFormatter = {
	editCellIconFormatter: function(cellvalue, options, rowObject){
		var status = 1;		
		if((rowObject != null || rowObject != undefined) && (rowObject.parentShop != null || rowObject.parentShop != undefined) &&rowObject.parentShop.status == "STOPPED") {
			return "";
		}
		if(rowObject.status != activeStatus){
			status = 0;
		}
		var parentShopId = -1;
		var parentName = '';
		if(rowObject.parentShop == null ||rowObject.parentShop == undefined ){
			parentShopId = -1;
			parentName = '';
		}else{
			parentShopId = rowObject.parentShop.id;
			parentName =  rowObject.parentShop.shopName;
		}
		
		var unitType = -1;
		if(rowObject.type != null && rowObject.type != undefined){
			unitType = rowObject.type.id;
		}
		var provinceCode = '';
		var districtCode = '';
		var wardCode = '';
		if(rowObject.area != null && rowObject.area != undefined){
			provinceCode = rowObject.area.province;
			districtCode = rowObject.area.district;
			wardCode = rowObject.area.precinct;
		}
		if(rowObject.shipTo!=null && rowObject.shipTo!=undefined){
			rowObject.shipTo = rowObject.shipTo.replace(/'/g,"\\'");
		}
		return "<a href='javascript:void(0)' onclick=\"return UnitTreeCatalog.getSelectedUnit("+ rowObject.id + ",'"+ Utils.XSSEncode(rowObject.shopCode) + "'," + parentShopId + ",'" + Utils.XSSEncode(rowObject.shopName) + "','"+unitType+"'," + status + ",'"+ Utils.XSSEncode(rowObject.taxNum) + "','" + Utils.XSSEncode(rowObject.invoiceNumberAccount) + "','" + Utils.XSSEncode(rowObject.invoiceBankName) + "','" + Utils.XSSEncode(rowObject.billTo) + "','" + Utils.XSSEncode(rowObject.phone) + "','" + Utils.XSSEncode(rowObject.mobiphone) + "','" + Utils.XSSEncode(rowObject.email) + "','" 
		+ Utils.XSSEncode(rowObject.shipTo) + "','" + Utils.XSSEncode(rowObject.contactName) + "','" + Utils.XSSEncode(rowObject.fax) + "','" + Utils.XSSEncode(provinceCode) + "','" + Utils.XSSEncode(districtCode) + "','" + Utils.XSSEncode(wardCode) + "','" + Utils.XSSEncode(rowObject.address) + "','" + rowObject.lat + "','" + rowObject.lng 
		+ "','" + Utils.XSSEncode(parentName) + "');\"><img src='/resources/images/icon-edit.png'/></a>";
	},
	addCellIconFormatter: function(cellvalue, options, rowObject){
		var channelTypeCode = '';
		if((rowObject != null || rowObject != undefined) && (rowObject.parentShop != null || rowObject.parentShop != undefined) &&rowObject.parentShop.status == "STOPPED") {
			return "";
		}
		if(rowObject.type != null && rowObject.type.channelTypeCode != null){
			channelTypeCode = rowObject.type.channelTypeCode;
		}
		var parentShopId = '';
		var parentName = '';
		if(rowObject.parentShop == null ||rowObject.parentShop == undefined ){
			parentShopId = rowObject.id;
			parentName = rowObject.shopName;
		}else{
			parentShopId = rowObject.parentShop.id;
			parentName =  rowObject.parentShop.shopName;
		}
		if(rowObject.status != activeStatus){
			return "";
		}else{
			return "<a href='javascript:void(0)' onclick=\"return UnitTreeCatalog.addChildShop("+ rowObject.id + ",'"+ Utils.XSSEncode(rowObject.shopCode) + "','"+ Utils.XSSEncode(channelTypeCode) +"',"+ Utils.XSSEncode(parentShopId) +",'"+ Utils.XSSEncode(parentName)+"','"+ Utils.XSSEncode(rowObject.shopName) +"');\"><img src='/resources/images/icon-add.png'/></a>";
		}
	},
	unitTypeFormatter: function(cellvalue, options, rowObject){
		if(rowObject.type != null && rowObject.type != undefined && rowObject.type.channelTypeCode != null){
			if(rowObject.type.channelTypeCode == 'VNM'){
				return "Nutifood";
			}else if(rowObject.type.channelTypeCode == 'MIEN'){
				return "Miền";
			}else if(rowObject.type.channelTypeCode == 'VUNG'){
				return "Vùng";
			}else if(rowObject.type.channelTypeCode == 'NPP'){
				return "NPP";
			}
		}else{
			return "";
		}
	},
	actionTypeFormatter:function(cellvalue, options, rowObject){
		var content = '';
		if(rowObject.actionType == 'INSERT'){
			return 'Tạo mới ';
		} else if(rowObject.actionType == 'UPDATE'){
			return 'Cập nhật';
		} else if(rowObject.actionType == 'DELETE'){
			return 'Xóa';
		}				
		return cellvalue;
	},
	viewDetail: function(cellvalue, options, rowObject){
		return '<a href="javascript:void(0)" onclick="return UnitTreeCatalog.searchChangeInfoDetail('+rowObject.id+');" ><span style="cursor:pointer"><img src="/resources/images/icon-view.png"></span></a>';
	},
	
	//NEW
	backMainPage : function(value, rowData, rowIndex) {
		return '<a href="javascript:void(0)" onclick="UnitTreeCatalog.showCollapseTab(-1,'+rowData.id+')"><img src="/resources/images/icon-view.png" width="15" height="16"></a>';
	},
	statusShopFormatter :function(value, rowData, rowIndex){
		if(value == activeStatus){
			return jsp_common_status_active;
		} else if(value == stoppedStatus){
			return jsp_common_status_stoped;
		} 
		return '';
	}
};
var AreaTreeFormatter = {
	editCellIconFormatter: function(value, row, index){
		AreaTreeCatalog.isUpdate = true;
		var parentId = 0;
		var parentAreaCode = '';
		var parentAreaName = '';
		if((row != null || row != undefined) && (row.parentArea != null || row.parentArea != undefined) &&row.parentArea.status == "STOPPED") {
			return "";
		}
		if(row.parentArea == null ||row.parentArea == undefined ){
			parentId = 0;
		}else{
			parentId = row.parentArea.id;
		}
		var area_name = '';		
		if(parentId!=0){
			area_name = row.parentArea.areaName;	
			parentAreaCode = row.parentArea.areaCode;
			parentAreaName = row.parentArea.areaName;
		}		
		$('#areaParentCode').attr("disabled","disabled");
		$('#areaNameParent').attr("disabled","disabled");
		var status = 1;		
		if(row.status != activeStatus){
			status = 0;
		}		
		var name = row.areaName;
		name = name.replace(/'/g,"\\'");
		row.areaCode = row.areaCode.replace(/'/g,"\\'");
		area_name = area_name.replace(/'/g,"\\'");
		return "<a href='javascript:void(0)' onclick=\"return AreaTreeCatalog.getSelectedArea("+ row.id + ",'"+ Utils.XSSEncode(row.areaCode) + "'," + parentId + ",'" + Utils.XSSEncode(name) +"','"+status+"','"+ Utils.XSSEncode(area_name) +"','"+ Utils.XSSEncode(parentAreaCode) +"','"+ Utils.XSSEncode(parentAreaName) +"');\"><img src='/resources/images/icon-edit.png'/></a>";
	},
	viewCellIconFormatter:function(cellvalue, options, rowObject){
		var value='';
		if(cellvalue != '' && cellvalue.trim().length > 12){
			value = cellvalue.substring(0,12);
			value+='...';
		}else{
			value = cellvalue;
		}
		return value;
	}
};
var SaleTypeCatalogFormatter = {
	editCellIconFormatter: function(cellvalue, options, rowObject){
		var parentShopName = -1;
		if(rowObject.parentChannelType == null ||rowObject.parentChannelType == undefined ){
			parentShopName = -1;
		}else{
			parentShopName = rowObject.parentChannelType.id;
		}
		var status = 1;		
		if(rowObject.status != activeStatus){
			status = 0;
		}
		return "<a href='javascript:void(0)' onclick=\"return SaleTypeCatalog.getSelectedStaffType("+ rowObject.id + ",'"+ Utils.XSSEncode(rowObject.channelTypeCode) + "','" + Utils.XSSEncode(parentShopName) + "','" + Utils.XSSEncode(rowObject.channelTypeName) + "','"+status+"');\"><img src='/resources/images/icon-edit.png'/></a>";
	},
	delCellIconFormatter: function(cellvalue, options, rowObject){
		return "<a href='javascript:void(0)' onclick=\"return SaleTypeCatalog.deleteStaffType('"+ Utils.XSSEncode(rowObject.channelTypeCode) +"')\"><img src='/resources/images/icon-delete.png'/></a>";
	}
};
var StaffTypeCatalogFormatter = {
		editCellIconFormatter: function(value, row, index){
			var parentShopName = -1;
			if(row.parentChannelType == null ||row.parentChannelType == undefined ){
				parentShopName = -1;
			}else{
				parentShopName = row.parentChannelType.id;
			}
			var status = 1;		
			if(row.status != activeStatus){
				status = 0;
			}
			return "<a href='javascript:void(0)' onclick=\"return StaffTypeCatalog.getSelectedStaffType("+ row.id + ",'"+ Utils.XSSEncode(row.channelTypeCode) + "','" + Utils.XSSEncode(parentShopName) + "','" + Utils.XSSEncode(row.channelTypeName) + "','"+status+"','" + Utils.XSSEncode(row.objectType) + "');\"><img src='/resources/images/icon-edit.png'/></a>";
		},
		delCellIconFormatter: function(cellvalue, options, rowObject){
			return "<a href='javascript:void(0)' onclick=\"return StaffTypeCatalog.deleteStaffType('"+ Utils.XSSEncode(rowObject.channelTypeCode) +"')\"><img src='/resources/images/icon-delete.png'/></a>";
		}
		
	};
var ProductLevelCatalogFormatter = {
		editCellIconFormatter: function(cellvalue, options, rowObject){
			var status = 1;
			if(rowObject.status != activeStatus){
				status = 0;
			}					
			return "<a href='javascript:void(0)' onclick=\"return ProductLevelCatalog.getSelectedRow("+ options.rowId + ","+ status +");\"><img src='/resources/images/icon-edit.png'/></a>";
		},
		delCellIconFormatter: function(cellvalue, options, rowObject){
			return "<a href='javascript:void(0)' onclick=\"return ProductLevelCatalog.deleteRow('"+ Utils.XSSEncode(rowObject.productLevelCode) +"')\"><img src='/resources/images/icon-delete.png'/></a>";
		}
	};
var CustomerTypeCatalogFormatter = {
	editCellIconFormatter: function(data, row, index){					
		var parentChannelTypeId = -2;		
		if(row.parentChannelType != null && row.parentChannelType != undefined){
			parentChannelTypeId = row.parentChannelType.id;
		}
		var data = "<a title= " + catalog_customer_type_edit + " href='javascript:void(0)' onclick=\"return CustomerTypeCatalog.getSelectedRow("+ row.id + ","+ parentChannelTypeId +");\"><img src='/resources/images/icon-edit.png'/></a>";
		return data;
	},
	delCellIconFormatter: function(cellvalue, options, rowObject){
		return "<a title="+action_type_status_delete+"  href='javascript:void(0)' onclick=\"return CustomerTypeCatalog.deleteRow('"+ Utils.XSSEncode(rowObject.channelTypeCode) +"')\"><img src='/resources/images/icon-delete.png'/></a>";
	}
};
var TaxTypeCatalogFormatter = {
		editCellIconFormatter: function(cellvalue, options, rowObject){
			var status = 1;		
			if(rowObject.status != activeStatus){
				status = 0;
			}
			return "<a href='javascript:void(0)' onclick=\"return TaxTypeCatalog.getSelectedRow("+ options.rowId + "," + status + ");\"><img src='/resources/images/icon-edit.png'/></a>";
		},
		delCellIconFormatter: function(cellvalue, options, rowObject){
			return "<a href='javascript:void(0)' onclick=\"return TaxTypeCatalog.deleteRow('"+ Utils.XSSEncode(rowObject.vatTypeCode) +"')\"><img src='/resources/images/icon-delete.png'/></a>";
		},
		rateCellFormatter: function(cellvalue, options, rowObject){			
			return Utils.formatDoubleValue(cellvalue) + '%';
		}
	};
var CarTypeCatalogFormatter = {
	editCellIconFormatter: function(cellvalue, rowObject,options){
		var carTrademark = '';
		if(rowObject.label == null ||rowObject.label == undefined){
			carTrademark = '';
		}else{
			carTrademark = rowObject.label.id;
		}
		var shop = '';
		if(rowObject.shop == null ||rowObject.shop == undefined ){
			shop = '';
		}else{
			shop = rowObject.shop.shopCode;
		}
		var status = '';
		if(rowObject.status == null){
			status = '';
		}else{
			if(rowObject.status == 'STOPPED'){
				status = 0;
			}else{
				status = 1;
			}
		}
		return "<a title='Sửa' href='javascript:void(0)' onclick=\"return CarTypeCatalog.getSelectedCarType("+ rowObject.id + ",'"+ Utils.XSSEncode(rowObject.carNumber) + "','" + Utils.XSSEncode(rowObject.category) + "','" + Utils.XSSEncode(rowObject.gross) + "','" + carTrademark + "','"+ Utils.XSSEncode(rowObject.origin) + "','"+ Utils.XSSEncode(shop) + "','"+ status + "','"+ rowObject.type + "');\"><img src='/resources/images/icon-edit.png'/></a>";
	},
	originFormat: function(cellvalue, rowObject,options){
		if(rowObject.origin != null ||rowObject.origin != undefined ){
			if(rowObject.origin == 'MUO' ){
				return "Xe mượn";
			}else if(rowObject.origin == 'NPP' ){
				return "Xe đơn vị";
			}
		}else{
			return "";
		}
	},
	categoryFormat: function(cellvalue, rowObject,options){
		if(rowObject.category != null ||rowObject.category != undefined ){
			if(rowObject.category == '0'){
				return "Lạnh";
			}else{
				return "Khô";
			}
		}else{
			return "";
		}
	},
	channelTypeNameFormat: function(cellvalue, rowObject,options){
		if(rowObject.label!=undefined && rowObject.label!=undefined && rowObject.label.channelTypeName!=undefined && rowObject.label.channelTypeName.length>0){
			return Utils.XSSEncode(rowObject.label.channelTypeName);
		}
	},
	grossFormat: function(cellvalue, rowObject,options){
		if(rowObject.gross != null ||rowObject.gross != undefined ){
			if(rowObject.gross == 0){
				return '';
			}else{
				return Utils.XSSEncode(rowObject.gross);
			}
		}
	},
	typeFormat: function(cellvalue, rowObject,options){
		if(rowObject.type != null ||rowObject.type != undefined ){
			if(rowObject.type == '4B' || rowObject.type == '4b'){
				return "Xe 4 bánh";
			}else if(rowObject.type == '3B' || rowObject.type == '3b'){
				return "Xe 3 bánh";
			}else{
				return "Xe 2 bánh";
			}
		}else{
			return "";
		}
	},
	statusFormat: function(cellvalue, rowObject,options){
		if(rowObject.status != null ||rowObject.status != undefined ){
			if(rowObject.status == 'STOPPED' ){
				return "Tạm ngưng";
			}else{
				return "Hoạt động";
			}
		}else{
			return "";
		}
	},
	delCellIconFormatter: function(cellvalue, rowObject,options){
		return "<a href='javascript:void(0)' onclick=\"return CarTypeCatalog.deleteCarType('"+ rowObject.id +"')\"><img src='/resources/images/icon-delete.png'/></a>";
	}
};
var DeliveryGroupCatalogFormatter = {
	editCellIconFormatter: function(cellvalue, options, rowObject){		
		return "<a href='javascript:void(0)' onclick=\"return DeliveryGroupCatalog.getSelectedRow("+ rowObject.id +");\"><img src='/resources/images/icon-edit.png'/></a>";
	},
	delCellIconFormatter: function(cellvalue, options, rowObject){
		return "<a href='javascript:void(0)' onclick=\"return DeliveryGroupCatalog.deleteRow('"+ rowObject.id +"')\"><img src='/resources/images/icon-delete.png'/></a>";
	},
	delCellIconCustomerFormatter: function(cellvalue, options, rowObject){
		return "<a href='javascript:void(0)' onclick=\"return DeliveryGroupCatalog.deleteCustomerRow('"+ rowObject.id +"')\"><img src='/resources/images/icon-delete.png'/></a>";
	}
};
var PromotionCatalogFormatter = {
	viewCellIconFormatter: function(val,row){		
		if(row.type != null && (row.type.substring(0,2) == 'ZM' || row.type.substring(0,2) == 'ZT' || row.type.substring(0,2) == 'ZD'|| row.type.substring(0,2) == 'ZH')){
			return "<a href='/catalog/promotion/viewdetail?promotionId="+row.id+"&proType=2'><span style='cursor:pointer'><img width='15' src='/resources/images/icon-edit.png'/></span></a>";
		}else{
			return "<a href='/catalog/promotion/viewdetail?promotionId="+row.id+"&proType=1'><span style='cursor:pointer'><img width='15' src='/resources/images/icon-edit.png'/></span></a>";
		}
	},
	editProductFormatter: function(cellvalue, options, rowObject){
		var freeProduct = '';
		if(rowObject.freeProduct != null || rowObject.freeProduct != undefined){
			freeProduct = rowObject.freeProduct.productCode;
		}
		var productName = '';
		var productCode = '';
		if(rowObject.product != null || rowObject.product != undefined){
			productName = rowObject.product.productName;
			productCode = rowObject.product.productCode;
		}
		if(checkPermissionEdit()){
			var statusPromotion = 1;		
			if(rowObject.promotionProgram != null && rowObject.promotionProgram.status != null && rowObject.promotionProgram.status == "WAITING"){
				statusPromotion = 2;
			}
			if(statusPromotion == 2){
				return "<a href='javascript:void(0)' onclick=\"return PromotionCatalog.getSelectedProduct('"+ rowObject.id +"','"+ Utils.XSSEncode(productCode) +"','"+ Utils.XSSEncode(productName) +"','"+ rowObject.saleQty +"','"+ rowObject.saleAmt +"','"+ rowObject.discPer +"','"+ rowObject.discAmt +"','"+ Utils.XSSEncode(freeProduct) +"','"+ rowObject.freeQty +"','"+ rowObject.required +"');\"><img src='/resources/images/icon-edit.png'/></a>";
			}else{
				return "";
			}
		}else{
			return "";
		}
	},
	delProductFormatter: function(cellvalue, options, rowObject){
		if(checkPermissionEdit()){
			var statusPromotion = 1;		
			if(rowObject.promotionProgram != null && rowObject.promotionProgram.status != null && rowObject.promotionProgram.status == "WAITING"){
				statusPromotion = 2;
			}
			if(statusPromotion == 2){
				return "<a href='javascript:void(0)' onclick=\"return PromotionCatalog.deleteProduct('"+ rowObject.id +"')\"><img src='/resources/images/icon-delete.png'/></a>";
			}else{
				return "";
			}
		}else{
			return "";
		}
	},
	editShopFormatter: function(cellvalue, options, rowObject){
		var shopCode = '';
		if(rowObject.shop == null ||rowObject.shop == undefined ){
			shopCode = '';
		}else{
			shopCode = rowObject.shop.shopCode;
		}
		var shopName = '';
		if(rowObject.shop == null ||rowObject.shop == undefined ){
			shopName = '';
		}else{
			shopName = rowObject.shop.shopName;
		}
		var status = 1;		
		if(rowObject.status != activeStatus){
			status = 0;
		}
		var value = 0;
		if(rowObject.objectApply != null || rowObject.objectApply != undefined){
			if(rowObject.objectApply == 'SHOP'){
				value = 1;
			}else if(rowObject.objectApply == 'SHOP_AND_CUSTOMER_TYPE'){
				value = 2;
			}else if(rowObject.objectApply == 'CUSTOMER'){
				value =3;
			}
		}
		if(checkPermissionEdit()){
			var statusPromotion = 1;		
			if(rowObject.promotionProgram != null && rowObject.promotionProgram.status != null&& rowObject.promotionProgram.status == "WAITING"){
				statusPromotion = 2;
			}
			if(statusPromotion == 2){
				return "<a href='javascript:void(0)' onclick=\"return PromotionCatalog.getSelectedShop('"+ rowObject.id +"','"+ Utils.XSSEncode(shopCode) +"','"+ Utils.XSSEncode(shopName) +"','"+ rowObject.quantityMax +"','"+status+"','"+value+"');\"><img src='/resources/images/icon-edit.png'/></a>";
			}else{
				return "";
			}
		}else{
			return "";
		}
	},
	delShopFormatter: function(cellvalue, options, rowObject){
		if(checkPermissionEdit()){
			var statusPromotion = 1;		
			if(rowObject.promotionProgram != null && rowObject.promotionProgram.status != null && rowObject.promotionProgram.status == "WAITING"){
				statusPromotion = 2;
			}
			if(statusPromotion == 2){
				return "<a href='javascript:void(0)' onclick=\"return PromotionCatalog.deleteShop('"+ rowObject.id +"')\"><img src='/resources/images/icon-delete.png'/></a>";
			}else{
				return "";
			}
		}else{
			return "";
		}
	},
	viewShopFormatter:function(value, row, index){
		var value = 0;
		if(row.objectApply != null || row.objectApply != undefined){
			if(row.objectApply == 'SHOP'){
				value = 1;
			}else if(row.objectApply == 'SHOP_AND_CUSTOMER_TYPE'){
				value = 2;
			}else if(row.objectApply == 'CUSTOMER'){
				value =3;
			}
		}
		var status = 1;		
		if(row.status != activeStatus){
			status = 0;
		}
		if(value == 2 || value == 3){
			return "<a href='javascript:void(0)' onclick=\"return PromotionCatalog.getDetailGridUrl('"+ row.shop.id +"','"+value+"','"+ Utils.XSSEncode(row.shop.shopName) +"','"+status+"');\"><img src='/resources/images/icon-view.png'/></a>";
		}else{
			return "";
		}
	},
	editCustomerFormatter: function(cellvalue, options, rowObject){
		if(checkPermissionEdit()){
			var statusPromotion = 1;		
			if(rowObject.promotionShopMap != null && rowObject.promotionShopMap.promotionProgram != null && rowObject.promotionShopMap.promotionProgram.status != null && rowObject.promotionShopMap.promotionProgram.status == "WAITING"){
				statusPromotion = 2;
			}
			if(statusPromotion == 2){
				return "<a href='javascript:void(0)' onclick=\"return PromotionCatalog.showDialogCustomerUpdate('"+ rowObject.id +"');\"><img src='/resources/images/icon-edit.png'/></a>";
			}else{
				return "";
			}
		}else{
			return "";
		}
	},
	delCustomerFormatter: function(cellvalue, options, rowObject){
		if(checkPermissionEdit()){
			var statusPromotion = 1;		
			if(rowObject.promotionShopMap != null && rowObject.promotionShopMap.promotionProgram != null && rowObject.promotionShopMap.promotionProgram.status != null && rowObject.promotionShopMap.promotionProgram.status == "WAITING"){
				statusPromotion = 2;
			}
			if(statusPromotion == 2){
				return "<a href='javascript:void(0)' onclick=\"return PromotionCatalog.deleteCustomer('"+ rowObject.id +"')\"><img src='/resources/images/icon-delete.png'/></a>";
			}else{
				return "";
			}
		}else{
			return "";
		}
	},
	delCustomerTypeFormatter: function(cellvalue, options, rowObject){
		if(checkPermissionEdit()){
			var statusPromotion = 1;		
			if(rowObject.promotionShopMap != null && rowObject.promotionShopMap.promotionProgram != null && rowObject.promotionShopMap.promotionProgram.status != null && rowObject.promotionShopMap.promotionProgram.status == "WAITING"){
				statusPromotion = 2;
			}
			if(statusPromotion == 2){
				return "<a href='javascript:void(0)' onclick=\"return PromotionCatalog.deleteCustomerType('"+ rowObject.id +"')\"><img src='/resources/images/icon-delete.png'/></a>";
			}else{
				return "";
			}
		}else{
			return "";
		}
	},
	moneyFormat: function(cellvalue, options, rowObject){
		if(cellvalue != null ||cellvalue != undefined ){
			return formatCurrency(cellvalue);
		}else{
			return "";
		}
	},
	statusFormat: function(val,row){
		if(row.status != null ||row.status != undefined ){
			if(row.status == 'STOPPED' ){
				return "Tạm ngưng";
			}else if(row.status == 'RUNNING'){
				return "Hoạt động";
			}else{
				return "Dự thảo";
			}
		}else{
			return "";
		}
	},
	actionTypeFormatter:function(cellvalue, options, rowObject){		
		if(rowObject.actionType == 'INSERT'){
			return 'Tạo mới ';
		} else if(rowObject.actionType == 'UPDATE'){
			return 'Cập nhật';
		} else if(rowObject.actionType == 'DELETE'){
			return 'Xóa';
		}				
		return cellvalue;
	},
	editCellIconFormatter: function(cellvalue, options, rowObject){
		var actionId = 0;
		if(rowObject == undefined || rowObject == null ) {
			return "";
		}
		if(rowObject.id == null ||rowObject.id == undefined ){
			actionId = 0;
		}else{
			actionId = rowObject.id;
		}
		return "<a href='javascript:void(0)' onclick='return PromotionCatalog.viewActionAudit("+actionId+")'><img src='/resources/images/icon-view.png'/></a>";
	},
	typeFormat: function(val,row){
		if(row.type != null ||row.type != undefined ){
			return Utils.XSSEncode(row.type) + '-' + Utils.XSSEncode(row.proFormat);
		}else{
			return "";
		}
	}
};
var ProductCatalogFormatter = {
		editCellIconFormatter: function(value,row,index){		
			return "<a href='/catalog/product/viewdetail?productId="+row.id+"'><span style='cursor:pointer'><img src='/resources/images/icon-view.png'/></span></a>";
		},
		delCellIconFormatter: function(cellvalue, options, rowObject){
			return "<a href='javascript:void(0)' onclick=\"return ProductCatalog.deleteProduct('"+ rowObject.id +"')\"><img src='/resources/images/icon-delete.png'/></a>";
		},
		editGroupFormatter: function(cellvalue, options, rowObject){
//			var departmentCode = -1;
//			if(rowObject.departmentCode != null && rowObject.departmentCode != undefined){
//				departmentCode = rowObject.department.id;
//			}
//			var levelId = -1;
//			if(rowObject.level != null && rowObject.level != undefined){
//				levelId = rowObject.level.id;
//			}
			var status = 1;		
			if(rowObject.status != activeStatus){
				status = 0;
			}
			return "<a href='javascript:void(0)' onclick=\"return ProductCatalog.getSelectedGroup('"+ rowObject.id +"','"+ Utils.XSSEncode(rowObject.departmentCode) +"','"+ rowObject.levelId +"','"+ status +"');\"><img src='/resources/images/icon-edit.png'/></a>";
		},
		delGroupFormatter: function(cellvalue, options, rowObject){
			return "<a href='javascript:void(0)' onclick=\"return ProductCatalog.deleteGroup('"+ rowObject.id +"')\"><img src='/resources/images/icon-delete.png'/></a>";
		},
		editShopFormatter: function(cellvalue, options, rowObject){
			var shopCode = "";
			if(rowObject.shop != null && rowObject.shop != undefined){
				shopCode = rowObject.shop.shopCode;
			}
			return "<a href='javascript:void(0)' onclick=\"return ProductCatalog.getSelectedShop('"+ rowObject.id +"','"+ Utils.XSSEncode(shopCode) +"','"+ Utils.XSSEncode(rowObject.value) +"');\"><img src='/resources/images/icon-edit.png'/></a>";
		},
		delShopFormatter: function(cellvalue, options, rowObject){
			return "<a href='javascript:void(0)' onclick=\"return ProductCatalog.deleteShop('"+ rowObject.id +"')\"><img src='/resources/images/icon-delete.png'/></a>";
		},
		editPriceFormatter: function(cellvalue, options, rowObject){
			var status = 0;
			if(rowObject.status == 'STOPPED'){
				status = 0;
			}else{
				status = 1;
			}
			var fromDate = "";
			if(rowObject.fromDate != null || rowObject.fromDate != undefined){
				fromDate = formatDate(rowObject.fromDate);
			}
			var toDate = "";
			if(rowObject.toDate != null || rowObject.toDate != undefined){
				toDate = formatDate(rowObject.toDate);
			}
			var tax = -1;
			if(rowObject.vat != null || rowObject.vat != undefined ){
				tax = parseInt(rowObject.vat.toString().replace('.0',''));
			}
			return "<a href='javascript:void(0)' onclick=\"return ProductCatalog.getSelectedPrice('"+ rowObject.id +"','"+ rowObject.price +"','"+ fromDate +"','"+ toDate +"','"+ status +"','"+ rowObject.priceNotVat +"','"+tax+"');\"><img src='/resources/images/icon-edit.png'/></a>";
		},
		delPriceFormatter: function(cellvalue, options, rowObject){
			return "<a href='javascript:void(0)' onclick=\"return ProductCatalog.deletePrice('"+ rowObject.id +"')\"><img src='/resources/images/icon-delete.png'/></a>";
		},
		statusFormat: function(value,row,index){
			if(row.status != null ||row.status != undefined ){
				if(row.status == 'STOPPED' ){
					return jsp_common_status_stoped;
				}else{
					return jsp_common_status_active;
				}
			}else{
				return "";
			}
		},
		expireFormat: function(value,row,index){
			if(row.expiryDate != null ||row.expiryDate != undefined ){
				if(row.expiryType != null ||row.expiryType != undefined ){
					if(row.expiryType == 1){
						return row.expiryDate+" "+jsp_common_ngay;
					}else{
						return row.expiryDate+" "+jsp_common_thang;
					}
				}
			}else{
				return "";
			}
		},
		lotFormat: function(value,row,index){
			if(row.checkLot != null ||row.checkLot != undefined ){
				if(row.checkLot == 1 ){
					return jsp_common_co;
				}else{
					return jsp_common_khong;
				}
			}else{
				return "";
			}
		},
		priceFormat: function(value,row,index){
			if(value != null ||value != undefined ){
				return formatCurrency(value);
			}else{
				return "";
			}
		},
		viewChangeDetail:function(cellvalue, options, rowObject){			
			return "<a href='javascript:void(0)' onclick='return ProductCatalog.getSearchActionGridUrl("+rowObject.id+")' ><span style='cursor:pointer'><img src='/resources/images/icon-view.png'/></span></a>";			
		},
		taxFormat: function(cellvalue, options, rowObject){
			if(cellvalue != null ||cellvalue != undefined ){
				return cellvalue.toString().replace('.0','');
			}else{
				return "";
			}
		}
	};
var ProgrammeDisplayCatalogFormatter = {
	relationCellIconFormatter: function(cellvalue, options, rowObject){		
		if(cellvalue == "AND"){
			return "Và";
		} else {
			return "Hoặc";
		}		
	},
	detailCellIconFormatter: function(cellvalue, options, rowObject){		
		return "<a href='javascript:void(0)' onclick=\"return ProgrammeDisplayCatalog.getSelectedProgram('"+ rowObject.id +"');\"><img src='/resources/images/icon-view.png'/></a>";
	},
	editCellIconLevelFormatter: function(cellvalue, options, rowObject){
		if(!checkPermissionEdit()){
			return '';
		}
		return "<a href='javascript:void(0)' onclick=\"ProgrammeDisplayCatalog.showDialogCreateLevel(true,"+ rowObject.id +");\"><img src='/resources/images/icon-edit.png'/></a>";
	},
	delCellIconLevelFormatter: function(cellvalue, options, rowObject){
		if(!checkPermissionEdit()){
			return '';
		}
		return "<a href='javascript:void(0)' onclick=\"return ProgrammeDisplayCatalog.deleteLevelRow('"+ rowObject.id +"')\"><img src='/resources/images/icon-delete.png'/></a>";
	},
	editCellIconProductFormatter: function(cellvalue, options, rowObject){		
		if(!checkPermissionEdit()){
			return '';
		}
		return "<a href='javascript:void(0)' onclick=\"return ProgrammeDisplayCatalog.showUpdateProductDialog("+ options.rowId +");\"><img src='/resources/images/icon-edit.png'/></a>";
	},
	delCellIconProductFormatter: function(cellvalue, options, rowObject){
		if(!checkPermissionEdit()){
			return '';
		}
		return "<a href='javascript:void(0)' onclick=\"return ProgrammeDisplayCatalog.deleteProductRow('"+ rowObject.id +"')\"><img src='/resources/images/icon-delete.png'/></a>";
	},
	editCellIconQuotaGroupFormatter: function(cellvalue, options, rowObject){
		if(!checkPermissionEdit()){
			return '';
		}
		return "<a href='javascript:void(0)' onclick=\"return ProgrammeDisplayCatalog.getChangedQuotaForm(true,"+  options.rowId +");\"><img src='/resources/images/icon-edit.png'/></a>";
	},
	delCellIconQuotaGroupFormatter: function(cellvalue, options, rowObject){
		if(!checkPermissionEdit()){
			return '';
		}
		return "<a href='javascript:void(0)' onclick=\"return ProgrammeDisplayCatalog.deleteQuotaGroupRow('"+ rowObject.id +"')\"><img src='/resources/images/icon-delete.png'/></a>";
	},
	addCellIconQuotaGroupFormatter: function(cellvalue, options, rowObject){
		if(!checkPermissionEdit()){
			return '';
		}
		if(Utils.getStatusValue(rowObject.status) == 1){
			return "<a href='javascript:void(0)' onclick=\"return ProgrammeDisplayCatalog.addSubQuotaGroupRow('"+ rowObject.id +"'," + options.rowId +","+ options.gid +");\"><img src='/resources/images/icon-add.png'/></a>";
		}
		return '';
	},
	delCellIconStaffFormatter: function(cellvalue, options, rowObject){
		if(!checkPermissionDev()){
			return '';
		}
		return "<a href='javascript:void(0)' onclick=\"return ProgrammeDisplayCatalog.deleteStaffRow('"+ rowObject.id +"')\"><img src='/resources/images/icon-delete.png'/></a>";
	},
	viewCellIconStaffFormatter: function(cellvalue, options, rowObject){
		return "<a href='javascript:void(0)' onclick=\"return ProgrammeDisplayCatalog.viewCustomerByStaff('"+ rowObject.id +"',"+ options.rowId +")\"><img src='/resources/images/icon-view.png'/></a>";
	},
	delCellIconSubQuotaGroupFormatter: function(cellvalue, options, rowObject){
		if(!checkPermissionEdit()){
			return '';
		}
		return "<a href='javascript:void(0)' onclick=\"return ProgrammeDisplayCatalog.deleteSubQuotaGroupRow('"+ rowObject.id + "','"+ options.gid +"')\"><img src='/resources/images/icon-delete.png'/></a>";
	},
	deleteCellIconCustomerFormatter: function(cellvalue, options, rowObject){
		if(!checkPermissionDev()){
			return '';
		}
		return "<a href='javascript:void(0)' onclick=\"return ProgrammeDisplayCatalog.deleteCustomerRow('"+ rowObject.id +"')\"><img src='/resources/images/icon-delete.png'/></a>";
	},
	editCellIconCustomerFormatter: function(cellvalue, options, rowObject){
		if(!checkPermissionDev()){
			return '';
		}
		return "<a href='javascript:void(0)' onclick=\"return ProgrammeDisplayCatalog.showUpdateCustomerDetailDialog('"+ options.rowId +"','"+ rowObject.displayProgramLevel.id +"')\"><img src='/resources/images/icon-edit.png'/></a>";
	},
	detailInfoChangeCellFormatter: function(cellvalue, options, rowObject){		
		return "<a href='javascript:void(0)' onclick=\"return ProgrammeDisplayCatalog.viewDetailActionLog('"+ rowObject.id +"')\"><img src='/resources/images/icon-view.png'/></a>";
	},
	actionTypeCellFormatter: function(cellvalue, options, rowObject){
		var content = '';
		if(cellvalue == 'INSERT'){
			cellvalue = 'Tạo mới ';
		} else if(cellvalue == 'UPDATE'){
			cellvalue = 'Cập nhật';
		} else if(cellvalue == 'DELETE'){
			cellvalue = 'Xóa';
		}				
		return cellvalue;
	},
	editCellIconShopFormatter: function(cellvalue, options, rowObject){
		if(!checkPermissionEdit()){
			return '';
		}
		return "<a href='javascript:void(0)' onclick=\"return ProgrammeDisplayCatalog.showUpdateShopDetailDialog('"+ options.rowId +"','"+ rowObject.status +"');\"><img src='/resources/images/icon-edit.png'/></a>";
	},
	deleteCellIconShopFormatter: function(cellvalue, options, rowObject){
		if(!checkPermissionEdit()){
			return '';
		}
		return "<a href='javascript:void(0)' onclick=\"return ProgrammeDisplayCatalog.deleteShopRow('"+ rowObject.id +"');\"><img src='/resources/images/icon-delete.png'/></a>";
	}
};
var CommonSearchFormatter = {
	selectCellIconFormatterEasyUIDialog : function(value, rowData, rowIndex) {
			return "<a href='javascript:void(0)' onclick=\"return CommonSearch.getResultSeachStyle1EasyUIDialog("+ rowIndex + ");\">Chọn</a>";
	},
	selectCellIconFormatter: function(cellvalue, options, rowObject){
		return "<a href='javascript:void(0)' onclick=\"return CommonSearch.getResultSeachStyle1("+ options.rowId +");\">Chọn</a>";
	},
	myCode: function(cellvalue, options, rowObject){
		if ($('.fancybox-inner #loai').val() ==4 ){
			return Utils.XSSEncode(rowObject.customerCode);
		}
		return Utils.XSSEncode(rowObject.staffCode);
	},
	myName: function(cellvalue, options, rowObject){
		if ($('.fancybox-inner #loai').val() ==4 ){
			return Utils.XSSEncode(rowObject.customerName);
		}
		return Utils.XSSEncode(rowObject.staffName);
	},
	selectCellIconFormatter_nvbh_1: function(cellvalue, options, rowObject){
		if ($('.fancybox-inner #loai').val() ==4 ){
			return "<input type=\"checkbox\" id=\"fgid_"+rowObject.id+"\" onclick=\"SuperviseCustomer.selectCheckbox("+rowObject.id+",'"+ Utils.XSSEncode(rowObject.customerCode) +"',"+ $('.fancybox-inner #loai').val()+")\"/>";
		}
		return "<input type=\"checkbox\" id=\"fgid_"+rowObject.id+"\" onclick=\"SuperviseCustomer.selectCheckbox("+rowObject.id+",'"+ Utils.XSSEncode(rowObject.staffCode) +"',"+ $('.fancybox-inner #loai').val()+")\"/>";
	},
	selectCellIconFormatterForCustomer:function(cellvalue, options, rowObject){
		var flag=false;
		if(DebitPayReport._lstCustomer!=null && DebitPayReport._lstCustomer!= undefined){
			if(DebitPayReport._lstCustomer.get(rowObject.shortCode)!=undefined && DebitPayReport._lstCustomer.get(rowObject.shortCode)!=null){
				flag=true;
			}
		}
		if(flag){
			return "<input checked='checked' type='checkbox' onclick=\"DebitPayReport.selectCustomer(this,\'"+ Utils.XSSEncode(rowObject.shortCode) +"\');\">";
		}else{
			return "<input type='checkbox' onclick=\"DebitPayReport.selectCustomer(this,\'"+ Utils.XSSEncode(rowObject.shortCode) +"\');\">";
		}
	},
	selectCellIconFormatterForStaff:function(cellvalue, options, rowObject){
		var flag=false;
		if(DebitPayReport._lstStaff!=null && DebitPayReport._lstStaff!= undefined){
			if(DebitPayReport._lstStaff.get(rowObject.staffCode)!=undefined && DebitPayReport._lstStaff.get(rowObject.staffCode)!=null){
				flag=true;
			}
		}
		if(flag){
			return "<input checked='checked' type='checkbox' onclick=\"DebitPayReport.selectStaff(this,\'"+ Utils.XSSEncode(rowObject.staffCode) +"\');\">";
		}else{
			return "<input type='checkbox' onclick=\"DebitPayReport.selectStaff(this,\'"+ Utils.XSSEncode(rowObject.staffCode) +"\');\">";
		}
	},
	selectCellIconFormatterForSupervisorStaff:function(cellvalue, options, rowObject){
		var flag=false;
		if(DebitPayReport._lstStaffOwner!=null && DebitPayReport._lstStaffOwner!= undefined){
			if(DebitPayReport._lstStaffOwner.get(rowObject.staffCode)!=undefined && DebitPayReport._lstStaffOwner.get(rowObject.staffCode)!=null){
				flag=true;
			}
		}
		if(flag){
			return "<input checked='checked' type='checkbox' onclick=\"DebitPayReport.selectSupervisorStaff(this,\'"+ Utils.XSSEncode(rowObject.staffCode) +"\');\">";
		}else{
			return "<input type='checkbox' onclick=\"DebitPayReport.selectSupervisorStaff(this,\'"+ Utils.XSSEncode(rowObject.staffCode) +"\');\">";
		}
	},
	selectCellIconFormatterForProduct:function(cellvalue, options, rowObject){
		var flag=false;
		if(DebitPayReport._lstProduct!=null && DebitPayReport._lstProduct!= undefined){
			if(DebitPayReport._lstProduct.get(rowObject.productCode)!=undefined && DebitPayReport._lstProduct.get(rowObject.productCode)!=null){
				flag=true;
			}
		}
		if(flag){
			return "<input checked='checked' type='checkbox' onclick=\"CRMReportDSPPTNH.selectProduct(this,\'"+ Utils.XSSEncode(rowObject.productCode) +"\');\">";
		}else{
			return "<input type='checkbox' onclick=\"CRMReportDSPPTNH.selectProduct(this,\'"+ Utils.XSSEncode(rowObject.productCode) +"\');\">";
		}
	},
	selectCellIconFormatterForSubCat:function(cellvalue, options, rowObject){
		var flag=false;
		if(DebitPayReport._lstSubCat!=null && DebitPayReport._lstSubCat!= undefined){
			if(DebitPayReport._lstSubCat.get(rowObject.productInfoCode)!=undefined && DebitPayReport._lstSubCat.get(rowObject.productInfoCode)!=null){
				flag=true;
			}
		}
		if(flag){
			return "<input checked='checked' type='checkbox' onclick=\"DebitPayReport.selectSubCat(this,\'"+ Utils.XSSEncode(rowObject.productInfoCode) +"\');\">";
		}else{
			return "<input type='checkbox' onclick=\"DebitPayReport.selectSubCat(this,\'"+ Utils.XSSEncode(rowObject.productInfoCode) +"\');\">";
		}
	},
	selectCellIconFormatter4: function(cellvalue, options, rowObject){
		return "<a href='javascript:void(0)' onclick=\"return CommonSearch.getResultSeachStyle4("+ options.rowId +");\">Chọn</a>";
	},
	selectCellIconFormatterForDisplay: function(cellvalue, options, rowObject){
		var flag=false;
		if(ProgrammeDisplayCatalog._mapCheckSubCat!=null && ProgrammeDisplayCatalog._mapCheckSubCat!= undefined){
			if(ProgrammeDisplayCatalog._mapCheckSubCat.get(rowObject.id)!=undefined && ProgrammeDisplayCatalog._mapCheckSubCat.get(rowObject.id)!=null){
				flag=true;
			}
		}
		if(flag){
			return "<input checked='checked' type='checkbox' onclick='ProgrammeDisplayCatalog.selectSubCat(this,"+rowObject.id+");' class='cbCategoryProduct' value='"+ Utils.XSSEncode(rowObject.productInfoCode) +"'>";
		}else{
			return "<input type='checkbox' onclick='ProgrammeDisplayCatalog.selectSubCat(this,"+rowObject.id+");' class='cbCategoryProduct' value='"+ Utils.XSSEncode(rowObject.productInfoCode) +"'>";
		}
	},
	selectCell: function(cellvalue, options, rowObject){
		return "<a href='javascript:void(0)' onclick=\"return CommonSearch.getResultSeachStyle2("+ options.rowId +");\">Chọn</a>";
	},
	statusFormat:function(cellvalue, options, rowObject){
		if(rowObject.cycleCountStatus == 'ONGOING'){
			return "Đang thực hiện";
		}else if(rowObject.cycleCountStatus == 'COMPLETED'){
			return "Hoàn thành";
		}else if(rowObject.cycleCountStatus == 'CANCELLED'){
			return "Hủy bỏ";
		}else{
			return "";
		}
	},
	selectCellDistrictIconFormatter: function(cellvalue, options, rowObject){
		return "<a href='javascript:void(0)' onclick=\"return CommonSearch.getResultSeachStyleDistrict("+ options.rowId +");\">Chọn</a>";
	}
};
var StaffCatalogFormatter = {
		editCellIconFormatter: function(cellvalue, rowObject, options){		
			return "<a href='/catalog_staff_manager/viewdetail?staffId="+rowObject.id+"'><span style='cursor:pointer'><img src='/resources/images/icon-edit.png'/></span></a>";
		},
		searchCellIconFormatter: function(cellvalue, rowObject, options){		
			return "<a href='/catalog_staff_manager/viewdetail?staffId="+rowObject.id+"'><span style='cursor:pointer'><img src='/resources/images/icon-view.png'/></span></a>";
		},
		statusFormat: function(cellvalue, rowObject, options){
			if(rowObject.status != null ||rowObject.status != undefined ){
				if(rowObject.status == 'STOPPED' ){
					return "Tạm ngưng";
				}else{
					return "Hoạt động";
				}
			}else{
				return "";
			}
		},
		editCategoryFormatter: function(cellvalue, options, rowObject){
			return "<a href='javascript:void(0)' onclick=\"return StaffCatalog.getSelectedCategory('"+ rowObject.id +"');\"><img src='/resources/images/icon-edit.png'/></a>";
		},
		delCategoryFormatter: function(cellvalue, options, rowObject){
			return "<a href='javascript:void(0)' onclick=\"return StaffCatalog.deleteCategory('"+ rowObject.id +"')\"><img src='/resources/images/icon-delete.png'/></a>";
		},
		priceFormat: function(cellvalue, options, rowObject){			
			if(rowObject.total != null ||rowObject.total != undefined ){
				return formatCurrency(rowObject.total);
			}else{
				return "";
			}
		},
		typeFormatter: function(cellvalue, options, rowObject){
			var content = '';
			if(rowObject.actionType == 'INSERT'){
				return 'Tạo mới ';
			} else if(rowObject.actionType == 'UPDATE'){
				return 'Cập nhật';
			} else if(rowObject.actionType == 'DELETE'){
				return 'Xóa';
			}				
			return cellvalue;
		},
		viewCellIconFormatter: function(cellvalue, options, rowObject){
			return "<a href='javascript:void(0)' onclick=\"return StaffCatalog.getDetailChangedGridUrl('"+ rowObject.id +"');\"><img src='/resources/images/icon-view.png'/></a>";
		},
		delSupervisorFormatter: function(cellvalue, options, rowObject){
			if ($('#staffSuper').val() != "1") return "";
			return "<a href='javascript:void(0)' onclick=\"return StaffCatalog.deleteSupervisor('"+ rowObject.id +"')\"><img src='/resources/images/icon-delete.png'/></a>";
		}
	};
var StaffUnitTreeFormatter = {
	editCellIconFormatter: function(cellvalue, rowObject, options){		
		return "<a href='javascript:void(0)' onclick=\"return UnitTreeCatalog.showStaffEditScreen("+ rowObject.id +","+ rowObject.staffType.objectType+","+rowObject.shop.id+");\"><span style='cursor:pointer'><img src='/resources/images/icon-edit.png'/></span></a>";
	},
	searchCellIconFormatter: function(cellvalue, rowObject, options){		
		return "<a href='javascript:void(0)' onclick=\"return UnitTreeCatalog.showSearchTreePosition("+ rowObject.id +");\"><span style='cursor:pointer'><img src='/resources/images/icon-edit.png'/></span></a>";
	},
	backMainPage : function(value, rowData, rowIndex) {
		if (rowData.staff != null || rowData.staff != undefined) {
			return '<a href="javascript:void(0)" onclick="return UnitTreeCatalog.showCollapseTab(-1,'+rowData.shop.id+',null,null,null,null,'+rowData.id+')"><img src="/resources/images/icon-view.png" width="15" height="16"></a>';
		}
	},
	shopFormat : function(value, rowData, rowIndex) {
		return Utils.XSSEncode(rowData.shop.shopCode);
	},
	statusFormat: function(cellvalue, rowObject, options){
		if(rowObject.status != null ||rowObject.status != undefined ){
			if(rowObject.status == 'STOPPED' ){
				return jsp_common_status_stoped;
			}else{
				return "Hoạt động";
			}
		}else{
			return "";
		}
	},
	typeFormatter: function(cellvalue, options, rowObject){
		var content = '';
		if(rowObject.actionType == 'INSERT'){
			return 'Tạo mới ';
		} else if(rowObject.actionType == 'UPDATE'){
			return 'Cập nhật';
		} else if(rowObject.actionType == 'DELETE'){
			return 'Xóa';
		}				
		return cellvalue;
	}
};
var BaryCentricProgramCatalogFormatter = {
	detailCellIconFormatter: function(cellvalue, options, rowObject){		
		return "<a href='javascript:void(0)' onclick=\"return BaryCentricProgrammeCatalog.getSelectedProgram('"+ rowObject.id +"');\"><img src='/resources/images/icon-view.png'/></a>";
	},
	editCellIconShopFormatter: function(cellvalue, options, rowObject){
		if(!checkPermissionEdit()){
			return '';
		}
		return "<a href='javascript:void(0)' onclick=\"return BaryCentricProgrammeCatalog.showUpdateShopDetailDialog('"+ options.rowId +"','"+ rowObject.status +"');\"><img src='/resources/images/icon-edit.png'/></a>";
	},
	deleteCellIconShopFormatter: function(cellvalue, options, rowObject){
		if(!checkPermissionEdit()){
			return '';
		}
		return "<a href='javascript:void(0)' onclick=\"return BaryCentricProgrammeCatalog.deleteShopRow('"+ rowObject.id +"');\"><img src='/resources/images/icon-delete.png'/></a>";
	},
	deleteCellIconProductFormatter: function(cellvalue, options, rowObject){
		if(!checkPermissionEdit()){
			return '';
		}
		return "<a href='javascript:void(0)' onclick=\"return BaryCentricProgrammeCatalog.deleteProductRow('"+ rowObject.id +"');\"><img src='/resources/images/icon-delete.png'/></a>";
	},
	editCellIconProductFormatter: function(cellvalue, options, rowObject){
		if(!checkPermissionEdit()){
			return '';
		}
		return "<a href='javascript:void(0)' onclick=\"return BaryCentricProgrammeCatalog.showUpdateProductDialog('"+ options.rowId +"');\"><img src='/resources/images/icon-edit.png'/></a>";
	}
};
var CategoryTypeCatalogFormatter = {
	delCellIconFormatter: function(value, row, index){
		return "<a href='javascript:void(0)' class='btnGridDelete' onclick=\"CategoryTypeCatalog.deleteRow('"+ row.id +"')\"><img src='/resources/images/icon-delete.png' title='Xóa'/></a>";
	},
	statusFormat: function(value, row, index){
		if(row.status != null ||row.status != undefined ){
			if(row.status == 'STOPPED' ){
				return "Tạm ngưng";
			}else{
				return "Hoạt động";
			}
		}else{
			return "";
		}
	},
	saleDayFormat:function(value, row, index){
		return CategoryTypeCatalog.parseValueToChar(value);
	}
};
var CategoryTypeProductCatalogFormatter = {
		editCellIconFormatter: function(value, row, index){		
			return "<a href='/product-distributer/viewdetail?productId="+row.id+"'><span style='cursor:pointer'><img src='/resources/images/icon-edit.png'/></span></a>";
		},
		delCellIconFormatter: function(value, row, index){
			var data = "<a href='javascript:void(0)' onclick=\"return CategoryTypeProductCatalog.deleteRow('"+ row.id +"')\"><img src='/resources/images/icon-delete.png' title='Xóa'/></a>";
			return getFormatterControlGrid('btnDeleteGrid', data);
		},
		statusFormat: function(value, row, index){
			if(row.status != null ||row.status != undefined ){
				if(row.status == 'STOPPED' ){
					return "Tạm ngưng";
				}else{
					return "Hoạt động";
				}
			}else{
				return "";
			}
		},
		saleDayFormat:function(value, row, index){
			return CategoryTypeCatalog.parseValueToChar(value);
		}
	};
var CustomerCatalogFormatter = {
	editCellIconFormatter: function(cellvalue, options, rowObject){		
			return "<a href='javascript:void(0)' onclick=\"return CustomerCatalog.getSelectedCustomer('"+ rowObject.id +"');\"><img src='/resources/images/icon-edit.png' height='16'/></a>";
		
	},
	editCellIconLevelCatFormatter: function(cellvalue, options, rowObject){
		if($('#hasEditPermission').val().trim() == 'true'){
			return "<a href='javascript:void(0)' onclick=\"return CustomerCatalog.getSelectedLevelCatRow('"+ options.rowId +"');\"><img src='/resources/images/icon-edit.png'/></a>";
		}
		return '';
	},
	deleteCellIconLevelCatFormatter: function(cellvalue, options, rowObject){
		if($('#hasEditPermission').val().trim() == 'true'){
			return "<a href='javascript:void(0)' onclick=\"return CustomerCatalog.deleteLevelCatRow('"+ rowObject.id +"');\"><img src='/resources/images/icon-delete.png'/></a>";
		}
		return '';
	},
	detailInfoChangeCellFormatter: function(cellvalue, options, rowObject){		
		return "<a href='javascript:void(0)' onclick=\"return CustomerCatalog.viewDetailActionLog('"+ rowObject.id +"')\"><img src='/resources/images/icon-view.png'/></a>";
	},
	actionTypeCellFormatter: function(cellvalue, options, rowObject){
		var content = '';
		if(cellvalue == 'INSERT'){
			cellvalue = 'Tạo mới ';
		} else if(cellvalue == 'UPDATE'){
			cellvalue = 'Cập nhật';
		} else if(cellvalue == 'DELETE'){
			cellvalue = 'Xóa';
		}
		if(rowObject.columnName != undefined){
			content+= rowObject.columnName;
		}		
		return Utils.XSSEncode(cellvalue);
	}
};
var SalesDayCatalogFormatter = {
		editCellIconFormatter: function(cellvalue, options, rowObject){		
			return "<a href='/catalog/sales-day/viewdetail?salesDayId="+rowObject.id+"'><span style='cursor:pointer'><img src='/resources/images/icon-edit.png'/></span></a>";
		},
		delCellIconFormatter: function(cellvalue, options, rowObject){
			return "<a href='javascript:void(0)' onclick=\"return SalesDayCatalog.deleteRow('"+ rowObject.id +"')\"><img src='/resources/images/icon-delete.png'/></a>";
		},
		statusFormat: function(cellvalue, options, rowObject){
			if(rowObject.status != null ||rowObject.status != undefined ){
				if(rowObject.status == 'STOPPED' ){
					return "Tạm ngưng";
				}else{
					return "Hoạt động";
				}
			}else{
				return "";
			}
		}
	};
var LevelIncomeCatalogFormatter = {
		editCellIconFormatter: function(cellvalue, row, index){
			var proInfoCode = 0;
			var saleLevelName ='';
			var proInfoId = 0;
			var status = 0;
			if(row.status != null && row.status != undefined){
				if(row.status == 'STOPPED'){
					status = 0;
				}else{
					status = 1;
				}
			}
			if(row.cat != null){
				proInfoCode = row.cat.productInfoCode ;
				proInfoId = row.cat.id;
			}
			if(row.saleLevelName != null){
				saleLevelName = row.saleLevelName;
			}
			
			return "<a title= " + catalog_customer_type_edit + " href='javascript:void(0);' class='btnGridEdit' onclick=\"LevelIncomeCatalog.getSelectedLevelIncome('"+row.id+"','"+ Utils.XSSEncode(proInfoCode) +"',"+proInfoId+",'"+ Utils.XSSEncode(row.saleLevelCode) + "','" + Utils.XSSEncode(saleLevelName) + "','" + Utils.XSSEncode(row.fromAmountString) + "','" + Utils.XSSEncode(row.toAmountString) + "','"+status+"');\"><img src='/resources/images/icon-edit.png'/></a>";
		},
		delCellIconFormatter: function(cellvalue, row, index){
			return "<a href='javascript:void(0)' onclick='LevelIncomeCatalog.deleteRow("+ row.id +");'><img src='/resources/images/icon-delete.png'/></a>";
		},
		statusFormat: function(cellvalue, row, index){
			if(row.status != null ||row.status != undefined ){
				if(row.status == 'STOPPED' ){
					return jsp_common_status_stoped;
				}else{
					return jsp_common_status_active;
				}
			}else{
				return "";
			}
		},
		fromAmountFormat: function(cellvalue, row, index){
			if(row.fromAmount != null ||row.fromAmount != undefined ){
				return formatCurrency(row.fromAmountString);
			}else{
				return "";
			}
		},
		toAmountFormat: function(cellvalue, row, index){
			if(row.toAmount != null ||row.toAmount != undefined ){
				return formatCurrency(row.toAmountString);
			}else{
				return "";
			}
		},
		productInfoCodeFormat:function(cellvalue, row, index){
			if(row.cat != null ||row.cat != undefined ){
				return Utils.XSSEncode(row.cat.productInfoCode);
			}else{
				return '';
			}
		},
		productInfoNameFormat:function(cellvalue, row, index){
			if(row.cat != null ||row.cat != undefined ){
				return Utils.XSSEncode(row.cat.productInfoName);
			}else{
				return '';
			}
		}
	};
var SKUCatalogFormatter = {
		editCellIconFormatter: function(cellvalue, options, rowObject){
			var status = 1;		
			if(rowObject.statusSku != activeStatus){
				status = 0;
			}
			return "<a href='javascript:void(0)' onclick=\"return SkuCatalog.getSelectedRow("+ options.rowId + ");\"><img src='/resources/images/icon-edit.png'/></a>";
		},
		delCellIconFormatter: function(cellvalue, options, rowObject){
			return "<a href='javascript:void(0)' onclick=\"return SkuCatalog.deleteSKU("+ rowObject.id +")\"><img src='/resources/images/icon-delete.png'/></a>";
		},
		skuAmountFormatter: function(cellvalue, options, rowObject){
			return formatCurrency(cellvalue);
		}
	};
var TotalMoneyQuotaCatalogFormatter = {
		editCellIconFormatter: function(cellvalue, options, rowObject){
			var status = 1;		
			if(rowObject.statusAmount != activeStatus){
				status = 0;
			}
			return "<a href='javascript:void(0)' onclick=\"return TotalMoneyQuotaCatalog.getSelectedRow("+ options.rowId + ");\"><img src='/resources/images/icon-edit.png'/></a>";
		},
		delCellIconFormatter: function(cellvalue, options, rowObject){
			return "<a href='javascript:void(0)' onclick=\"return TotalMoneyQuotaCatalog.deleteSale("+ rowObject.id +")\"><img src='/resources/images/icon-delete.png'/></a>";
		}
};
var SaleProductGridFormatter = {
		forcusFormatter:function(cellvalue, options, rowObject){
			if(rowObject.isFocus=='0'|| rowObject.isFocus==0){
				return '<input class="focusCS" type="checkbox"  />';
			}else{
				return '<input  class="focusCS" type="checkbox" checked="checked"  />';
			}
		},
		commercialSupporFormatter:function(cellvalue, options, rowObject){
			var productId = rowObject.productId;
			var productIdCommercialSupport = SPCreateOrder._mapProduct.get(productId);
			if(productIdCommercialSupport == null || productIdCommercialSupport == undefined) {
				return '<input type="text" class="ccSupportCode" onkeypress="return SPCreateOrder.moveFocusByKeyForCS(event, '+ options.rowId+');" size="10" id="commercialSupport_'+options.rowId +'" />';
			} else {
				var obj = SPCreateOrder._mapCommercialSupport.get(productIdCommercialSupport);
				if(obj.commercialSupportType != 'ZV' && obj.commercialSupportType != '' && obj.commercialSupportType != null && obj.commercialSupportType != undefined) {
					return '<input type="text" class="ccSupportCode" value="'+obj.commercialSupport+'" name="'+obj.commercialSupportType+'" onkeypress="return SPCreateOrder.moveFocusByKeyForCS(event, '+ options.rowId+');" size="10" id="commercialSupport_'+options.rowId +'" />';
				} else {
					return '<input type="text" class="ccSupportCode" onkeypress="return SPCreateOrder.moveFocusByKeyForCS(event, '+ options.rowId+');" size="10" id="commercialSupport_'+options.rowId +'" />';
				}
			}
		},
		converMoney:function(cellvalue, options, rowObject){
			
		},
		quantityFormatter:function(cellvalue, options, rowObject){
			if(cellvalue>0){
				var productId = rowObject.productId;
				var productIdCommercialSupport = SPCreateOrder._mapProduct.get(productId);
				if(productIdCommercialSupport == null || productIdCommercialSupport == undefined) {
					return '<input type="text" class="clsProductVOQuantity" size="10" id="productRow_'+options.rowId +'" onchange="SPCreateOrder.amountBlured('+ options.rowId+');" onblur="SPCreateOrder.amountBlured('+ options.rowId+');" />';
				} else {
					var obj = SPCreateOrder._mapCommercialSupport.get(productIdCommercialSupport);
					return '<input type="text" value="'+obj.quantity_input+'" class="clsProductVOQuantity" size="10" id="productRow_'+options.rowId +'" onchange="SPCreateOrder.amountBlured('+ options.rowId+');" onblur="SPCreateOrder.amountBlured('+ options.rowId+');" />';
				}
			}
			else {
				return '<input readonly="readonly" type="text" class="clsProductVOQuantity" size="10" id="productRow_'+options.rowId +'" onchange="SPCreateOrder.amountBlured('+ options.rowId+');" onblur="SPCreateOrder.amountBlured('+ options.rowId+');" />';
			}
		}
};
var DisplayToolCatalogFormatter = {
	editCellIconFormatter: function(cellvalue, options, rowObject){
		var shopCode = '';
		if(rowObject.shop!= null ||rowObject.shop!=undefined ){
			shopCode = rowObject.shop.shopCode;
		}
		var supervisorStaffCode = '';
		if(rowObject.staff!= null ||rowObject.staff!=undefined ){
			supervisorStaffCode = rowObject.staff.staffCode;
		}
		var saleStaffCode = '';
		if(rowObject.staff!= null ||rowObject.staff!=undefined ){
			saleStaffCode = rowObject.staff.staffCode;
		}
		var customerCode = '';
		if(rowObject.customer!= null ||rowObject.customer!=undefined ){
			customerCode = rowObject.customer.shortCode;
		}
		var displayToolCode = '';
		if(rowObject.product!= null ||rowObject.product!=undefined ){
			displayToolCode = rowObject.product.productCode;
		}
		var inMonth = "";
		if(rowObject.inMonth != null || rowObject.inMonth != undefined){
			inMonth = formatMonth(rowObject.inMonth);
		}
		return "<a href='javascript:void(0)' onclick=\"return DisplayToolCatalog.getSelectedDisplayTool("+ rowObject.id + ",'"+ Utils.XSSEncode(shopCode) + "','" + Utils.XSSEncode(supervisorStaffCode) + "','" + Utils.XSSEncode(saleStaffCode) + "','" + Utils.XSSEncode(customerCode) + "','"+ Utils.XSSEncode(displayToolCode) + "','"+ rowObject.quantity + "','"+ rowObject.amount + "','"+ inMonth + "');\"><img src='/resources/images/icon-edit.png'/></a>";
	},
	delCellIconFormatter: function(cellvalue, options, rowObject){
		return "<a href='javascript:void(0)' onclick=\"return DisplayToolCatalog.deleteDisplayTool('"+ rowObject.id +"')\"><img src='/resources/images/icon-delete.png'/></a>";
	},
	monthFormatter: function(cellvalue, options, rowObject){
		var inMonth = "";
		if(rowObject.inMonth != null || rowObject.inMonth != undefined){
			inMonth = formatMonth(rowObject.inMonth);
		}
		return inMonth;
	}
};
var DisplayToolProductFormatter = {
	getToolCodeFormatter: function(cellValue, options, rowObject) {
		return Utils.XSSEncode(rowObject.tool.productCode);
	},
	getToolNameFormatter: function(cellValue, options, rowObject) {
		return Utils.XSSEncode(rowObject.tool.productName);
	},
	getProductCodeFormatter: function(cellValue, options, rowObject) {
		return Utils.XSSEncode(rowObject.product.productCode);
	},
	getProductNameFormatter: function(cellValue, options, rowObject) {
		return Utils.XSSEncode(rowObject.product.productName);
	},
	deleteIconFormatter: function(cellValue, options, rowObject) {
		return "<a href='javascript:void(0)' onclick=\"return DisplayToolProductCatalog.del('"+rowObject.id+"')\"><img src='/resources/images/icon-delete.png'/></a>";
	}
};
var AttributeManagerFormatter = {
	editCellIconFormatter: function(cellvalue, options, rowObject){
		var colunmType = AttributeColumnType.parseValue(rowObject.columnType);
		var colunmValueType = AttributeColumnValue.parseValue(rowObject.columnValueType);
		var status = activeType.parseValue(rowObject.status);
		return "<a href='javascript:void(0)' onclick=\"return AttributesManager.getSelectedRow("+ options.rowId +"," + colunmType + "," + colunmValueType + "," + status + ");\"><img src='/resources/images/icon-edit.png'/></a>";
	},
	listValueCellIconFormatter: function(cellvalue, options, rowObject){
		var colunmType = AttributeColumnType.parseValue(rowObject.columnType);
		if(rowObject.columnValueType == 'EXIST_BEFORE'){
			return "<a href='javascript:void(0)' onclick=\"return AttributesManager.getListValue("+ rowObject.id +","+colunmType+");\"><img src='/resources/images/icon-view.png'/></a>";
		}
		return '';
	},
	columnTypeCellIconFormatter: function(cellvalue, options, rowObject){
		if(cellvalue!= undefined){
			if(cellvalue == 'NUMBER'){
				return attr_col_type_number;
			} else if(cellvalue == 'CHARACTER'){
				return attr_col_type_char;
			} else if(cellvalue == 'DATE_TIME'){
				return attr_col_type_date;
			}
		}		
		return '';
	},
	columnValueTypeCellIconFormatter: function(cellvalue, options, rowObject){
		if(cellvalue == 'SELF_INPUT'){
			return attr_col_val_type_manual;
		} else if(cellvalue == 'EXIST_BEFORE'){
			return attr_col_val_type_list;
		}
		return '';
	},
	editDetailCellIconFormatter: function(cellvalue, options, rowObject){		
		return "<a href='javascript:void(0)' onclick=\"return AttributesManager.getSelectedDetailRow("+ options.rowId + ");\"><img src='/resources/images/icon-edit.png'/></a>";
	},
	deleteDetailCellIconFormatter: function(cellvalue, options, rowObject){		
		return "<a href='javascript:void(0)' onclick=\"return AttributesManager.deleteDetailValue("+ options.rowId + ");\"><img src='/resources/images/icon-delete.png'/></a>";
	}
};
var IncentiveProgramCatalogFormatter = {
	delShop: function(cellValue, options, rowObject) {
		return "<a href='javascript:void(0)' onclick=\"return IncentiveProgramCatalog.delShop('"+rowObject.id+"')\"><img src='/resources/images/icon-delete.png'/></a>";
	},
	editProductCellIconFormatter: function(cellvalue, options, rowObject){		
		return "<a href='javascript:void(0)' onclick=\"return IncentiveProgramCatalog.getSelectedProduct('"+ rowObject.id +"');\"><img src='/resources/images/icon-edit.png'/></a>";
	},
	deleteProductCellIconFormatter: function(cellvalue, options, rowObject){		
		return "<a href='javascript:void(0)' onclick=\"return IncentiveProgramCatalog.deleteSelectedProduct('"+ rowObject.id +"');\"><img src='/resources/images/icon-delete.png'/></a>";
	},
	typeFormat: function(cellvalue, options, rowObject){
		if(rowObject.actionType != null ||rowObject.actionType != undefined ){
			if(rowObject.actionType == 'INSERT' ){
				return "Thêm mới";
			}else if(rowObject.actionType == 'UPDATE' ){
				return "Cập nhật";
			}else{
				return "Xóa";
			}
		}else{
			return "";
		}
	},
	viewCellIconFormatter: function(cellvalue, options, rowObject){
		return "<a href='javascript:void(0)' onclick=\"return IncentiveProgramCatalog.getDetailChangedGridUrl('"+ rowObject.id +"');\"><img src='/resources/images/icon-view.png'/></a>";
	}
};
var PayrollElementFormatter = {
		editCellIconFormatter: function(cellvalue, options, rowObject){
			return "<a href='javascript:void(0)' onclick=\"return PayrollElement.getSelectedElement('"+ rowObject.id +"','" + Utils.XSSEncode(rowObject.payrollElementCode) + "','" + Utils.XSSEncode(rowObject.payrollElementName) + "','" + rowObject.type + "','" + rowObject.roundNum + "','" + rowObject.defaultValue + "','" + rowObject.valueType + "');\"><img src='/resources/images/icon-edit.png'/></a>";
		},
		delCellIconFormatter: function(cellvalue, options, rowObject){		
			if(rowObject.countUse == 0){
				return "<a href='javascript:void(0)' onclick=\"return PayrollElement.deletePaymentElement('"+ rowObject.id + "');\"><img src='/resources/images/icon-delete.png'/></a>";
			}else{
				return "";
			}
		},
		typeFormatter: function(cellvalue, options, rowObject){		
			if(cellvalue != undefined){
				if(cellvalue == 0){
					return 'Đầu vào';
				}else if(cellvalue == 1){
					return 'Kết quả';
				}
			}
			return '';
		}
	};
var PayrollTableFormatter = {
		editCellIconFormatter: function(cellvalue, options, rowObject){
			/*var colunmType = -1;
			if(rowObject.type != undefined){
				if(rowObject.type == 'INPUT'){
					colunmType = 0;
				}else{
					colunmType = 1;
				}
			}
			return "<a href='javascript:void(0)' onclick=\"return PayrollTable.getSelectedElement('"+ rowObject.id +"','" + rowObject.payrollElementCode + "','" + rowObject.payrollElementName + "','" + colunmType + "','" + rowObject.roundNum + "','" + rowObject.defaultValue + "');\"><img src='/resources/images/icon-edit.png'/></a>";*/
			var url = '/payroll/table/info?id='+rowObject.payrollId;
			return "<a href=\""+url+"\"><img src='/resources/images/icon-edit.png'/></a>";
		},
		delCellIconFormatter: function(cellvalue, options, rowObject){		
			return "<a href='javascript:void(0)' onclick=\"return PayrollTable.deletePaymentTable('"+ rowObject.payrollId + "');\"><img src='/resources/images/icon-delete.png'/></a>";
		},
		statusFormatter :function(cellvalue, options, rowObject){
			if(cellvalue != null){
				if(cellvalue == 1){
					return jsp_common_status_active;
				}else{
					return jsp_common_status_stoped;
				}
			}else{
				return '';
			}
		}
	};
var PayrollPatternFormatter = {
		editCellIconFormatter: function(cellvalue, options, rowObject){
			var colunmType = -1;
			if(rowObject.type != undefined){
				if(rowObject.type == 'INPUT'){
					colunmType = 0;
				}else{
					colunmType = 1;
				}
			}
			if (rowObject.count > 0){
				return '';
			}
			return "<a href='javascript:void(0)' onclick=\"return PayrollPattern.viewAdd(false,"+ options.rowId +");\"><img src='/resources/images/icon-edit.png'/></a>";
		},
		delCellIconFormatter: function(cellvalue, options, rowObject){		
			if (rowObject.count == 0){
				return "<a href='javascript:void(0)' onclick=\"return PayrollPattern.deletePayrollT('"+ rowObject.payrollTId + "');\"><img src='/resources/images/icon-delete.png'/></a>";
			}
			else return '';
		},
		statusFormatter :function(cellvalue, options, rowObject){
			if(cellvalue != null){
				if(cellvalue == 1){
					return jsp_common_status_active;
				}else{
					return jsp_common_status_stoped;
				}
			}else{
				return '';
			}
		}
	};
var PayrollInfoElementFormatter = {
		selectCellIconFormatter: function(cellvalue, options, rowObject){//
			return "<a href='javascript:void(0)' onclick=\"return PayrollInfoElement.getRowPayRollInfoElement('"+ Utils.XSSEncode(rowObject.payrollCode) +"');\">Chọn</a>";
		},
		inputCellInGirdStaff: function(cellvalue, options, rowObject){//
			if(null!=rowObject && null!=rowObject.value){ //co gia tri
				if(rowObject.payrollElementType==0){
					return '<input value="'+rowObject.value+'" style="text-align:right" type="text" maxlength="17" class="value_payroll" id="txt_'+options.rowId +'" row-id="'+options.rowId+'"/>';
				}
				if(rowObject.payrollElementType==1){
					return '<input value="'+rowObject.value+'" style="text-align:right" type="text" maxlength="2000" class="value_payroll" id="txt_'+options.rowId +'" row-id="'+options.rowId+'"/>';
				}
			}
			else{// chua co gia tri (null)
				if(rowObject.payrollElementType==0){
					if(null!=rowObject.payrollElementDefaultValue){
						return '<input value="'+rowObject.payrollElementDefaultValue+'"style="text-align:right" type="text" maxlength="17" class="value_payroll" id="txt_'+options.rowId +'" row-id="'+options.rowId+'"/>';
					}
					else{
						return '<input style="text-align:right" type="text" maxlength="17" class="value_payroll" id="txt_'+options.rowId +'" row-id="'+options.rowId+'"/>';
					}
				}
				if(rowObject.payrollElementType==1){
					if(null!=rowObject.payrollElementDefaultValue){
						return '<input value="'+rowObject.payrollElementDefaultValue+'"style="text-align:right" type="text" maxlength="2000" class="value_payroll" id="txt_'+options.rowId +'" row-id="'+options.rowId+'"/>';
					}
					else{
						return '<input style="text-align:right" type="text" maxlength="2000" class="value_payroll" id="txt_'+options.rowId +'" row-id="'+options.rowId+'"/>';
					}
				}
			}
			
		}
	};
var DpPayPeriodPeriodFormatter = {
		editCellFormatter:function(cellvalue, options, rowObject){
			if(rowObject.status=='CREATE_NEW'){
				return "<a href='javascript:void(0)' onclick=\"return DpPayPeriodPeriodManager.getSelectedRow("+options.rowId+");\"><img src='/resources/images/icon-edit.png'/></a>";
			}			
			return '';
		},
		deleteCellFormatter:function(cellvalue, options, rowObject){
			if(rowObject.status=='CREATE_NEW'){
				return "<a href='javascript:void(0)' onclick=\"return DpPayPeriodPeriodManager.deleteRow("+ rowObject.id + ");\"><img src='/resources/images/icon-delete.png'/></a>";
			}
			return '';
		}		
};
var CalculatePeriodFormatter = {
		viewDetailCellFormatter: function(cellvalue,option,rowObject){
			return '<a href="/dppayperiod/calculate/detail?periodId='+rowObject.id+'"><span style="cursor:pointer"><img src="/resources/images/icon-view.png"></span></a>';
		},
		statusCellFormatter:function(cellvalue,option,rowObject){
			var DpPayPeriodStatus = {
				deleted:'DELETED',
				create_new:'CREATE_NEW',
				applied_value:'APPLIED_VALUE',
				caculated:'CACULATED',
				approved:'APPROVED',
				stop:'STOP',
				destroyed:'DESTROYED'
			};
			var status = rowObject.status;
			if(status==DpPayPeriodStatus.deleted || status==null){
				return 'Xóa';
			}else if(status==DpPayPeriodStatus.create_new){
				return 'Mới tạo';
			}else if(status==DpPayPeriodStatus.applied_value){
				return 'Đã nhập';
			}else if(status==DpPayPeriodStatus.caculated){
				return 'Đã tính';
			}else if(status==DpPayPeriodStatus.approved){
				return 'Đã chốt';
			}else if(status==DpPayPeriodStatus.stop){
				return jsp_common_status_stoped;
			}else if(status==DpPayPeriodStatus.destroyed){
				return 'Hủy';
			}
		},
		displayResultCellFormatter:function(cellvalue,option,rowObject){
			if(rowObject.displayResult==1){
				return 'Đạt';
			}else if(rowObject.displayResult==0){
				return 'Không đạt';
			}
			return '';
						
		},
		revenueCellFormatter:function(cellvalue,option,rowObject){
			if(rowObject.revenue!=null){
				return formatCurrency(rowObject.revenue);
			}
			return '0';
		},
		bonusCellFormatter:function(cellvalue,option,rowObject){
			if(rowObject.bonus!=null){
				return formatCurrency(rowObject.bonus);
			}
			return '0';
		}
		
};
var DeviceManagerFormatter = {
	viewCellIconFormatter: function(cellvalue, options, rowObject){		
		var contractId = 0;
		if(rowObject.contractId != null){
			contractId = rowObject.contractId;
		}
		return "<a href='/device/manager/viewdetail?deviceId="+rowObject.equipmentId+"&contractId="+contractId+"'><span style='cursor:pointer'><img src='/resources/images/icon-view.png'/></span></a>";
	},
	delCellIconFormatter: function(cellvalue, options, rowObject){
		return "<a href='javascript:void(0)' onclick=\"return DeviceManager.deleteRow('"+ rowObject.equipmentId +"')\"><img src='/resources/images/icon-delete.png'/></a>";
	},
	statusFormatter : function(cellvalue, options, rowObject){
		if(cellvalue != undefined){
			if(cellvalue == 1){
				return "Tốt";
			}else if(cellvalue == 2){
				return "Hỏng";
			}else if(cellvalue == 3){
				return "Đang bảo hành";
			}else if(cellvalue == 4){
				return "Đang sửa chữa";
			}else if(cellvalue == 5){
				return "Đã thanh lý";
			}else if(cellvalue == 6){
				return "Tốt đã thanh lý";
			}else if(cellvalue == 7){
				return "Mất chưa xử lý";
			}else if(cellvalue == 8){
				return "Mất";
			} else{
				return "";
			}
		}else{
			return "";
		}
	},
	typeFormatter:function(cellvalue, options, rowObject){
		if(cellvalue != undefined){
			if(cellvalue == 1){
				return "Cập nhật";
			}else if(cellvalue == 2){
				return "Điều chuyển";
			}else if(cellvalue == 3){
				return "Hợp đồng";
			}else if(cellvalue == 4){
				return "Kết thúc";
			}
		}else{
			return "";
		}
	},
	fromObjectFormatter:function(cellvalue, options, rowObject){
		if(rowObject.actionType != undefined){
			if(rowObject.actionType == 1){
				if(rowObject.fromObjectId == 1){
					return "Tốt";
				}else if(rowObject.fromObjectId == 2){
					return "Hỏng";
				}else if(rowObject.fromObjectId == 3){
					return "Đang bảo hành";
				}else if(rowObject.fromObjectId == 4){
					return "Đang sửa chữa";
				}else if(rowObject.fromObjectId == 5){
					return "Đã thanh lý";
				}else if(rowObject.fromObjectId == 6){
					return "Hủy";
				}else if(rowObject.fromObjectId == 7){
					return "Mất chưa xử lý";
				}else if(rowObject.fromObjectId == 8){
					return "Mất";
				}
			}else if(rowObject.actionType == 2){
				if(rowObject.fromShopName != undefined){
					return Utils.XSSEncode(rowObject.fromShopName);
				}else{
					return "";
				}
			}else if(rowObject.actionType == 3){
				if(rowObject.fromShopName != undefined){
					return Utils.XSSEncode(rowObject.fromShopName);
				}else{
					return "";
				}
			}else if(rowObject.actionType == 4){
				if(rowObject.fromCustomerName != undefined){
					return Utils.XSSEncode(rowObject.fromCustomerName);
				}else{
					return "";
				}
			}
		}else{
			return "";
		}
	},
	toObjectFormatter:function(cellvalue, options, rowObject){
		if(rowObject.actionType != undefined){
			if(rowObject.actionType == 1){
				if(rowObject.toObjectId == 1){
					return "Tốt";
				}else if(rowObject.toObjectId == 2){
					return "Hỏng";
				}else if(rowObject.toObjectId == 3){
					return "Đang bảo hành";
				}else if(rowObject.toObjectId == 4){
					return "Đang sửa chữa";
				}else if(rowObject.toObjectId == 5){
					return "Đã thanh lý";
				}else if(rowObject.toObjectId == 6){
					return "Hủy";
				}else if(rowObject.toObjectId == 7){
					return "Mất chưa xử lý";
				}else if(rowObject.toObjectId == 8){
					return "Mất";
				}
			}else if(rowObject.actionType == 2){
				if(rowObject.toShopName != undefined){
					return Utils.XSSEncode(rowObject.toShopName);
				}else{
					return "";
				}
			}else if(rowObject.actionType == 3){
				if(rowObject.toCustomerName != undefined){
					return Utils.XSSEncode(rowObject.toCustomerName);
				}else{
					return "";
				}
			}else if(rowObject.actionType == 4){
				if(rowObject.toShopName != undefined){
					return Utils.XSSEncode(rowObject.toShopName);
				}else{
					return "";
				}
			}
		}else{
			return "";
		}
	},
	contractNumberFormatter:function(cellvalue, options, rowObject){
		if(cellvalue != undefined){
			return "<a href='/contract/manager/contractdetail?id="+rowObject.contractId+"&type=1'><span style='cursor:pointer'>"+ Utils.XSSEncode(rowObject.contractNo) +"</span></a>";
		}else{
			return "";
		}
	},
	contractNumberTransferFormatter:function(cellvalue, options, rowObject){
		if(rowObject.actionType == 1 ||rowObject.actionType == 2){
			return "";
		}else{
			if(cellvalue != undefined){
				return "<a href='/contract/manager/contractdetail?id="+rowObject.contractId+"&type=1'><span style='cursor:pointer'>"+ Utils.XSSEncode(rowObject.contractNo) +"</span></a>";
			}else{
				return "";
			}
		}
	},
	customerCodeFormatter:function(cellvalue, options, rowObject){
		if(cellvalue != undefined){
			return Utils.XSSEncode(cellvalue + '-' + rowObject.customerName); 
		}else if(cellvalue == undefined && rowObject.customerName != undefined){
			return Utils.XSSEncode(rowObject.customerName);
		}else{
			return "";
		}
	}
};
var MapProductFormatter = {
		delCellIconFormatter: function(cellvalue, options, rowObject){
			return "<a href='javascript:void(0)' onclick=\"return MapProduct.deleteMapProduct('"+ rowObject.id +"')\"><img src='/resources/images/icon-delete.png'/></a>";
		}
};
var PayPedriodFormatter = {
		viewDetailCellFormatter: function(cellvalue,options,rowObject){
			return '<a href="/dppayperiod/pay/viewDetail?periodId='+rowObject.id+'"><span style="cursor:pointer"><img src="/resources/images/icon-view.png"></span></a>';
		},		
		payPeriodStatusCellFormatter:function(cellvalue,options,rowObject){
			if(rowObject.saleOrderId!=null){
				return '<input type="checkbox" checked="checked" disabled="disabled" />';
			}else if(true){//don hang da xuat
				return '<input type="checkbox" class="check_box_selected" row-id="'+options.rowId+'"  />';
			}
			return '';
		},
		quantityCellFormatter:function(cellvalue,options,rowObject){
			return formatQuantity(rowObject.quantity, rowObject.convfact);
		},
		lotCellFormatter:function(cellvalue,options,rowObject){
			if(rowObject.checkLot!=null && rowObject.checkLot==1 && rowObject.saleOrderId==null){
				return '<a href="javascript:void(0)" onclick="return DpPayPeriodPay.chooseLotDialog('+options.rowId+',\''+ Utils.XSSEncode(rowObject.shopCode) +'\','+rowObject.productId+','+rowObject.quantity+');" >Chọn lô</a>';
			}
			return '';
		},
		saleStaffCellFormatter:function(cellvalue,options,rowObject){
			var lstSaleStaff = rowObject.lstSaleStaff;
			if(lstSaleStaff.length==1){
				return '<p id="selectSaleStaff'+options.rowId+'" class="'+ Utils.XSSEncode(lstSaleStaff[0].staffCode) +'">'+ Utils.XSSEncode(lstSaleStaff[0].staffCode) +'</p>';
			}
			var SELECT = new Array();
			SELECT.push('<select id="selectSaleStaff'+options.rowId+'" style="width: 138px;" class="class_selected BoxSelect" >');
			for(var i=0;i<lstSaleStaff.length;++i){
				SELECT.push('<option value="'+ Utils.XSSEncode(lstSaleStaff[i].staffCode) +'">'+ Utils.XSSEncode(lstSaleStaff[i].staffCode) +'</option>');
			}
			SELECT.push("</select>");
			return SELECT.join("");
		},
		statusCellFormatter:function(cellvalue,options,rowObject){
			if(rowObject.isPaid=='true' || rowObject.isPaid==true ){
				return 'Đã trả hết';
			}
			return 'Chưa trả hết';
		}
};
var ActionTypeGridFormater = {
		actionTypeFormatter:function(cellvalue,options,rowObject){
		if(rowObject.actionType == 'INSERT'){
			return 'Tạo mới ';
		} else if(rowObject.actionType == 'UPDATE'){
			return 'Cập nhật';
		} else if(rowObject.actionType == 'DELETE'){
			return 'Xóa';
		}				
		return cellvalue;
	}
};
var ManagePoAutoNCCFormatter = {
	getShopCodeAndName : function(cellValue, rowObject, options) {
		if(rowObject.shopCode == null || rowObject.shopCode == 'null')
    		rowObject.shopCode = "";
    	if(rowObject.shopName == null || rowObject.shopName == 'null')
    		rowObject.shopName = "";
    	if(rowObject.shopCode != "" && rowObject.shopName != "")
    		return Utils.XSSEncode(rowObject.shopCode + ' - ' + rowObject.shopName);
    	return "";
	},
	getStatus : function(cellValue, rowObject, options) {
		if(rowObject.status  == 'APPROVED')
			return 'Chưa duyệt';
		if(rowObject.status  == 'NCC_APPROVED')
			return 'Đã duyệt';
		if(rowObject.status  == 'REJECT')
			return 'Từ chối';
		return "";
	},
	moneyFormat:function(cellValue, rowObject, options){
		return formatCurrency(cellValue);
	},
	quantityFormat:function(cellValue, rowObject, options){
		return formatQuantity(cellValue, rowObject.convfact);
	},
	viewDetail:function(cellvalue,rowObject, options){
		return "<span style='cursor:pointer' onclick=\"return POAutoNCCManage.detail("+ rowObject.poAutoId+",'"+ Utils.XSSEncode(rowObject.poAutoNumber) +"',"+ Utils.XSSEncode(rowObject.amount) +");\"><img src='/resources/images/icon-view.png'/></span>";
	},
	checkPOAuto : function(cellValue,rowObject, options ) {
		if(rowObject.status  == 'APPROVED') {
			var poAutoId = rowObject.poAutoId ;
			var obj = POAutoNCCManage._mapPOAutoIdSelect.get(poAutoId);
			if(obj == null) {
				return '<input type="checkbox" id="'+poAutoId +'" onchange="POAutoNCCManage.changeCheckBoxPOAuto(this.checked ,'+poAutoId+')"/>';
			} else{
				return '<input checked="checked" type="checkbox"  id="'+poAutoId +'" onchange="POAutoNCCManage.changeCheckBoxPOAuto(this.checked ,'+poAutoId+')"/>';
			}
		}
		return "";
	},
	viewDetailApprove:function(cellvalue, rowObject, options){
		if(rowObject.status == 'APPROVED') {
			return "<span style='cursor:pointer' onclick=\"return POAutoNCCManage.openApprovePOAutoDialog('"+ Utils.XSSEncode(rowObject.shopCode) +"', '"+ Utils.XSSEncode(rowObject.shopName) +"', '"+ Utils.XSSEncode(rowObject.poAutoDateStr) +"','"+ Utils.XSSEncode(rowObject.poAutoNumber) +"', "+rowObject.poAutoId+");\"><img src='/resources/images/icon-down.png'/></span>";
		} else {
			return '';
		}
	},
	instockCellFormatter: function(cellvalue, rowObject, options){
		var bigUnit = 0;
		var smallUnit = 0;
		var convfact = 1;
		if(rowObject.convfact!= null && rowObject.convfact!= undefined){
			convfact = rowObject.convfact;
		}
		bigUnit = parseInt(rowObject.availableQuantity/convfact);
		smallUnit = rowObject.availableQuantity%convfact;
		return bigUnit + '/' + smallUnit;	
	},
	amountCellFormatter: function(cellvalue, rowObject, options){		
		return '<input row-id="'+options.rowId+'" id="productRow_'+ options.rowId +'" maxlength="7" size="7" type="text" style="text-align:right" onkeypress="return NextAndPrevTextField(event, this,\'SelProduct\');" class="IssuedStock_AmountValue InputTextStyle InputText1Style SelProduct" onblur="StockIssued.amountBlured('+ options.rowId +');" onchange="StockIssued.amountChanged('+ options.rowId +');">';
	}
};
var PODVKHNCCFormatter = {
//	statusFormat:function(cellvalue,options,rowObject){
//		if(cellvalue != undefined){
//			if(cellvalue == 'CHUA_CO_PO_CONFIRM'){
//				return 'Chưa có PO Confirm ';
//			} else if(cellvalue == 'DANG_XUAT_PO_CONFIRM'){
//				return 'Đang xuất PO Confirm';
//			} else if(cellvalue == 'HOAN_TAT'){
//				return 'Hoàn tất';
//			}else {
//				return 'Treo';
//			}	
//		}else{
//			return "";
//		}
//	},
//	viewFormat: function(cellvalue,options,rowObject){
//		return "<a href='javascript:void(0)' onclick=\"return POCustomerServiceManage.getDetailGridUrl('"+ rowObject.poVnmId +"','"+ rowObject.poNumber +"');\"><img src='/resources/images/icon-view.png'/></a>";
//	},
//	createFormat: function(cellvalue,options,rowObject){
//		//if (rowObject.disStatus == "DA_CHUYEN") return "";
//		return "<a onclick='return POConfirm.getSelectedPOConfirm(" + rowObject.poVnmId+")' href='javascript:void(0)'><img src='/resources/images/icon-close.png'/></a>";
//	},
//	changeFormat: function(cellvalue,options,rowObject){
//		if(rowObject.disStatus != undefined && rowObject.disStatus == 'DA_CHUYEN'){
//			return "";
//		}else{
//			if(rowObject.poDvStatus != undefined && (rowObject.poDvStatus == 'CHUA_CO_PO_CONFIRM' || rowObject.poDvStatus == 'DANG_XUAT_PO_CONFIRM')){
//				return "<a href='javascript:void(0)' onclick=\" return POCustomerServiceManage.changePODVKH('"+ rowObject.poVnmId +"');\"><img src='/resources/images/icon-down.png'/></a>";
//			}else{
//				return "";
//			}
//		}
//	},
//	moneyFormat:function(cellvalue,options,rowObject){
//		if(cellvalue != undefined){
//			POCustomerServiceManage.tongtien += cellvalue;
//			return formatCurrency(cellvalue);
//		}else{
//			return "";
//		}
//	},
//	priceFormat:function(cellvalue,options,rowObject){
//		if(rowObject.priceValue != undefined){
//			return formatCurrency(rowObject.priceValue);
//		}else{
//			return "";
//		}
//	},
//	quantityFormat:function(cellvalue,options,rowObject){
//		if(rowObject.quantity != undefined && rowObject.product != undefined && rowObject.product.convfact != undefined){
//			return Math.floor(rowObject.quantity/rowObject.product.convfact) + "/" +(rowObject.quantity%rowObject.product.convfact);
//		}else{
//			return "";
//		}
//	}
	statusFormat:function(value,rows){
		if(value != undefined){
			if(value == 'CHUA_CO_PO_CONFIRM'){
				return 'Chưa có PO Confirm ';
			} else if(value == 'DANG_XUAT_PO_CONFIRM'){
				return 'Đang xuất PO Confirm';
			} else if(value == 'HOAN_TAT'){
				return 'Hoàn tất';
			}else {
				return 'Treo';
			}	
		}else{
			return "";
		}
	},
	viewFormat: function(index,rows){
		return "<a href='javascript:void(0)' onclick=\"return POCustomerServiceManage.getDetailGridUrl('"+ rows.poVnmId +"','"+ rows.poNumber +"');\"><img src='/resources/images/icon-view.png'/></a>";
	},
	createFormat: function(index,rows){
		//if (rowObject.disStatus == "DA_CHUYEN") return "";
		return "<a onclick='return POConfirm.getSelectedPOConfirm(" + rows.poVnmId+")' href='javascript:void(0)'><img src='/resources/images/icon-close.png'/></a>";
	},
	changeFormat: function(index,rows){
		if(rowa.disStatus != undefined && rowa.disStatus == 'DA_CHUYEN'){
			return "";
		}else{
			if(rows.poDvStatus != undefined && (rows.poDvStatus == 'CHUA_CO_PO_CONFIRM' || rows.poDvStatus == 'DANG_XUAT_PO_CONFIRM')){
				return "<a href='javascript:void(0)' onclick=\" return POCustomerServiceManage.changePODVKH('"+ rows.poVnmId +"');\"><img src='/resources/images/icon-down.png'/></a>";
			}else{
				return "";
			}
		}
	},
	moneyFormat:function(index,rows){
		if(rows != undefined){
			POCustomerServiceManage.tongtien += rows.amount;
			return formatCurrency(rows.amount);
		}else{
			return "";
		}
	},
	priceFormat:function(index,rows){
		if(rows.priceValue != undefined){
			return formatCurrency(rows.priceValue);
		}else{
			return "";
		}
	},
	quantityFormat:function(index,rows){
		if(rows.quantity != undefined && rows.product != undefined && rows.product.convfact != undefined){
			return Math.floor(rows.quantity/rows.product.convfact) + "/" +(rows.quantity%rows.product.convfact);
		}else{
			return "";
		}
	}
};
var POConfirmFormatter = {
	statusFormatter:function(cellvalue,options,rowObject){
		if(rowObject.status=='NOT_IMPORT'){
			return 'Chưa nhập';
		}else if(rowObject.status=='IMPORTING'){
			return 'Đang nhập';
		}else if(rowObject.status=='IMPORTED'){
			return 'Đã nhập';
		}else if(rowObject.status=='PENDING'){
			return 'Có thể treo';
		}else if(rowObject.status=='SUSPEND'){
			return 'Đã treo';
		}
		return '';
	},
	disStatusFormatter:function(cellvalue,rowObject,options){
		if(rowObject.disStatus=='CHUA_CHUYEN'){
			return 'Chưa chuyển';
		}else if(rowObject.disStatus=='DA_CHUYEN'){
			return 'Đã chuyển';
		}
		return '';
	},
	quantityFormatter:function(cellvalue,rowObject,options){
		return formatCurrency(cellvalue);
	},
	moneyFormatter:function(cellvalue,rowObject,options){
		return formatCurrency(cellvalue);
		
	},//liem
	viewFormat: function(cellvalue,rowObject,options){
		return "<a href='javascript:void(0)'onclick=\"return POConfirm.getListPOVnmDetailEx('"+ rowObject.id +"','"+rowObject.poCoNumber+"');\"><img src='/resources/images/icon-view.png'/></a>";
	},
	createFormat: function(cellvalue,rowObject,options){
		if(rowObject.disStatus=='CHUA_CHUYEN'){
			return "<a href=\"javascript:void(0)\" onclick=\"return POConfirm.getListPOVnmDetail('"+ rowObject.id +"');\"><img src=\"/resources/images/icon-edit.png\"></a>";
		}
		return '';
	},
	changeFormat: function(cellvalue,rowObject,options){
		if(rowObject.disStatus=='CHUA_CHUYEN'){
			return "<a href='javascript:void(0)' onclick=\"return POConfirm.transferPOConfirm('"+ rowObject.id +"');\" onclick=\"\"><img src='/resources/images/icon-down.png'/></a>";
		}	
		return '';
	},
	poVnmLotCellFormatter: function(cellvalue,rowObject, options){
		var bigUnit = 0;
		var smallUnit = 0;
		var convfact = 1;
		if(rowObject.convfact!= null && rowObject.convfact!= undefined){
			convfact = rowObject.convfact;
		}
		bigUnit = parseInt(rowObject.quantity/convfact);
		smallUnit = rowObject.quantity%convfact;
		return bigUnit + '/' + smallUnit;	
	},
	formatQuantityInCreatePOConfirm:function(cellvalue, rowObject, index){
		return '<input row-id="'+index+'" id="productRow_'+ index +'" size="5" type="text" style="text-align:right" onkeypress="return NextAndPrevTextField(event, this,\'SelProduct\');" class="IssuedStock_AmountValue InputTextStyle InputText1Style SelProduct" onblur="POConfirm.amountBlured('+ index +');" onchange="POConfirm.amountChanged('+ index +');">';
	}
};
var POAutoGroupFormatter = {
		delCellIconFormatter: function(value, row, index){
			var data = "<a href='javascript:void(0)' onclick=\"return GroupPOAuto.deleteRow('"+ row.id +"')\"><img src='/resources/images/icon-delete.png'/></a>";
	    	return getFormatterControlGrid('btnDeleteGrid2', data);
		},
		delCellIconFormatterCat: function(value, row, index){
			var data = "<a href='javascript:void(0)' onclick=\"return GroupPOAuto.deleteCategoryRow('"+ row.id +"')\"><img src='/resources/images/icon-delete.png'/></a>";
	    	return getFormatterControlGrid('btnDeleteGrid', data);
		},
		delCellIconFormatterSubCat: function(value, row, index){
			var data = "<a href='javascript:void(0)' onclick=\"return GroupPOAuto.deleteSubCategoryRow('"+ row.id +"')\"><img src='/resources/images/icon-delete.png'/></a>";
	    	return getFormatterControlGrid('btnDeleteGrid1', data);
		}
	};

var AttributeCustemerCatalogFormatter = {
		editCellIconFormatter: function(cellvalue, rowObject, options){		
			return "<a href='javascript:void(0)' class='btnGridEdit' onclick='AttributeCustomerManager.openAttributeCustomer("+options+");'><img src='/resources/images/icon-edit.png'/></a>";
		},
		editCellDetailIconFormatter: function(cellvalue, rowObject, options){		
			return "<a href='javascript:void(0)' class='btnGridDetailEdit' onclick='AttributeCustomerManager.openAttributeDetailCustomer("+options+");'><img src='/resources/images/icon-edit.png'/></a>";
		},
		viewCellDetailIconFormatter:function(cellvalue, rowObject, options){
			if(rowObject.valueType!=null || rowObject.valueType!=undefined){
				if(rowObject.valueType == 4 || rowObject.valueType == 5){
					return "<a href='javascript:void(0)' onclick='AttributeCustomerManager.loadDetail("+rowObject.attributeId+");'><img src='/resources/images/icon-view.png'/></a>";
				}
			}
		},
		statusFormat: function(cellvalue, rowObject, options){
			if(rowObject.status != null ||rowObject.status != undefined ){
				if(rowObject.status == 0 ){
					return "Tạm ngưng";
				}else{
					return "Hoạt động";
				}
			}else{
				return "";
			}
		},
		valueTypeFormat:function(cellvalue, rowObject, options){
			if(rowObject.valueType!=null || rowObject.valueType != undefined){
				if(rowObject.valueType == 1){
					return "Chữ";
				}else if(rowObject.valueType==2){
					return "Số";
				}else if(rowObject.valueType == 3){
					return "Ngày tháng";
				}else if(rowObject.valueType == 4){
					return "Danh sách(chọn một)";
				}else if(rowObject.valueType == 5){
					return "Danh sách(chọn nhiều)";
				}else{
					return "";
				}
			}
		},
		objectFormat:function(cellvalue, rowObject, options) {
			if (rowObject.tableName != null || rowObject.tableName != undefined) {
				if (rowObject.tableName == 'CUSTOMER') {
					return "Khách hàng";
				} else if (rowObject.tableName == 'PRODUCT') {
					return "Sản phẩm";
				} else if (rowObject.tableName == 'STAFF') {
					return "Nhân viên";
				} else {
					return "";
				}
			}
		}
//		editCategoryFormatter: function(cellvalue, options, rowObject){
//			return "<a href='javascript:void(0)' onclick=\"return StaffCatalog.getSelectedCategory('"+ rowObject.id +"');\"><img src='/resources/images/icon-edit.png'/></a>";
//		},
//		delCategoryFormatter: function(cellvalue, options, rowObject){
//			return "<a href='javascript:void(0)' onclick=\"return StaffCatalog.deleteCategory('"+ rowObject.id +"')\"><img src='/resources/images/icon-delete.png'/></a>";
//		},
//		priceFormat: function(cellvalue, options, rowObject){			
//			if(rowObject.total != null ||rowObject.total != undefined ){
//				return formatCurrency(rowObject.total);
//			}else{
//				return "";
//			}
//		},
//		typeFormatter: function(cellvalue, options, rowObject){
//			var content = '';
//			if(rowObject.actionType == 'INSERT'){
//				return 'Tạo mới ';
//			} else if(rowObject.actionType == 'UPDATE'){
//				return 'Cập nhật';
//			} else if(rowObject.actionType == 'DELETE'){
//				return 'Xóa';
//			}				
//			return cellvalue;
//		},
//		viewCellIconFormatter: function(cellvalue, options, rowObject){
//			return "<a href='javascript:void(0)' onclick=\"return StaffCatalog.getDetailChangedGridUrl('"+ rowObject.id +"');\"><img src='/resources/images/icon-view.png'/></a>";
//		},
//		delSupervisorFormatter: function(cellvalue, options, rowObject){
//			if ($('#staffSuper').val() != "1") return "";
//			return "<a href='javascript:void(0)' onclick=\"return StaffCatalog.deleteSupervisor('"+ rowObject.id +"')\"><img src='/resources/images/icon-delete.png'/></a>";
//		}
	};
var SalePlanFormatter = {
	quantityLeftFormatter: function(value, rowData, rowIndex) {
		var staffCode = $('#staffCode').val().trim();
			if (value != undefined && rowData.staffQuantity != undefined) {
				return (rowData.shopQuantity - rowData.shopQuantityAssign) + "/" + rowData.shopQuantity;
			} else {
				return '';
			}
	}
};
var SearchSaleTransaction = {
	editOrder:function(cellValue, rowObject,index) {
		var locked = $('#lock').val();
		if(locked == "false"){
			return "<a class='orderDetailTooltip' id='soDetail"+index+"' index='"+index+"' href='/sale-product/adjust-order/info?orderId="+rowObject.id+"'><img width='16' height='16' src='/resources/images/icon_edit.png'/></a>";
		}
	},
	customer:function(c,r,i){
		return '<a href="javascript:void(0)" id="custDetail'+i+'" index="'+i+'" class="easyui-tooltip" onclick="SPSearchSale.showCustomerInfor('+r.customer.id+')" class="aTagCustomerCode" name="'+r.customer.id+'">&nbsp&nbsp'+ Utils.XSSEncode(r.customer.shortCode) +' - '+ Utils.XSSEncode(r.customer.customerName) +'</a>';
	},
	status:function(c,r,i){
		if(r.type=='RETURNED'){
			return 'Đã trả';
		}else if(r.approved=='APPROVED'){
			return 'Đã duyệt';
		}else if(r.approved=='NOT_YET_APPROVE'){
			return 'Chưa duyệt';
		}else return 'Khác';
	},
	total:function(c,r,i){
		return formatCurrency(r.total);
	},
	nvbh:function(c,r,i){
		if(r.staff != null && r.staff != undefined) {
			return Utils.XSSEncode(r.staff.staffCode);
		} else {
			return '';
		}
	},
	nvgh:function(c,r,i){
		if(r.delivery != null && r.delivery != undefined) {
			return Utils.XSSEncode(r.delivery.staffCode);
		} else {
			return '';
		}
	},
	orderType:function(c,r,i){
		if(r.orderType=='IN'){
			return 'Đơn bán Presale';
		}else if(r.orderType=='SO'){
			return 'Đơn bán Vansale ';
		}else if(r.orderType=='CM'){
			return 'Đơn trả Presale';
		}else if(r.orderType=='TT'){
			return 'Trả thưởng';
		}else return 'Đơn trả Vansale ';
	}
};
/**
 * Loai giao dich
 * 
 * @author hunglm16
 * @since December 17,2014
 * @description Quan Ly Thiet Bi
 * @params: 0: Giao nhan, 1: Chuyen kho, 2: Sua chua, 3: Bao mat, 4: Bao mat tu mobile, 5: Thu hoi thanh ly, 6: Thanh ly
 * */
var tradeTypeEquipment = {ALL: -2, GN: 0, CK: 1, SC: 2, BM: 3, BMTMB: 4, THTL: 5, TL: 6, KK: 7, TTSC: 8,
	parseValue: function(value){
		if(value == undefined || value == null){
			return '';
		}else if(value == tradeTypeEquipment.ALL){
			return 'Tất cả';
		} else if(value == tradeTypeEquipment.GN){
			return 'Giao nhận';
		} else if(value == tradeTypeEquipment.CK){
			return 'Chuyển kho';
		} else if(value == tradeTypeEquipment.SC){
			return 'Sửa chữa';
		} else if(value == tradeTypeEquipment.BM){
			return 'Báo mất';
		} else if(value == tradeTypeEquipment.BMTMB){
			return 'Báo mất từ Mobile';
		} else if(value == tradeTypeEquipment.THTL){
			return 'Thu hồi thanh lý';
		} else if(value == tradeTypeEquipment.TL){
			return 'Thanh lý';
		} else if(value == tradeTypeEquipment.KK){
			return 'Kiểm kê';
		} else if(value == tradeTypeEquipment.TTSC){
			return 'Thanh toán sửa chữa';
		}
		return '';
	}
};

/**
 * Trang thai giao dich
 * 
 * @author hunglm16
 * @since December 17,2014
 * @description Quan Ly Thiet Bi
 * @params: 0: Khong giao dich, 1: Dang giao dich
 * */
var tradeStatusEquipment = {ALL: -2, STOPPED: 0, RUNNING: 1,
	parseValue: function(value){
		if(value == undefined || value == null){
			return '';
		}else if(value == tradeStatusEquipment.ALL){
			return 'Tất cả';
		} else if(value == tradeStatusEquipment.STOPPED){
			return 'Không giao dịch';
		} else if(value == tradeStatusEquipment.RUNNING){
			return 'Đang giao dịch';
		}
		return '';
	}
};

/**
 * Trang thai thiet bi
 * 
 * @author hunglm16
 * @since December 17,2014
 * @description Quan Ly Thiet Bi
 * @params: 0: Đã mất, 1: Đã thanh lý, 2: Đang ở kho, 3: Đang sử dụng, 4: Đang sửa chữa
 * */
var usageStatusEquipment = {ALL: -2, DM: 0, DTL: 1, DOK: 2, DSD: 3, DSC: 4,
	parseValue: function(value){
		if(value == undefined || value == null){
			return '';
		} else if(value == usageStatusEquipment.ALL){
			return 'Tất cả';
		} else if(value == usageStatusEquipment.DM){
			return 'Đã mất';
		} else if(value == usageStatusEquipment.DTL){
			return 'Đã thanh lý';
		} else if(value == usageStatusEquipment.DOK){
			return 'Đang ở kho';
		} else if(value == usageStatusEquipment.DSD){
			return 'Đang sử dụng';
		} else if(value == usageStatusEquipment.DSC){
			return 'Đang sửa chữa';
		}
		return '';
	},
	parseValueText: function(value){
		if(value == undefined || value == null){
			return '';
		} else if(value.trim() === 'LOST'){
			return 'Đã mất';
		} else if(value.trim() === 'LIQUIDATED'){
			return 'Đã thanh lý';
		} else if(value.trim() === 'SHOWING_WAREHOUSE'){
			return 'Đang ở kho';
		} else if(value.trim() === 'IS_USED'){
			return 'Đang sử dụng';
		} else if(value.trim() === 'SHOWING_REPAIR'){
			return 'Đang sửa chữa';
		}
		return '';
	},
	parseName: function(value){
		if(value == undefined || value == null){
			return -2;
		} else if(value.trim() === 'LOST'){
			return usageStatusEquipment.DM;
		} else if(value.trim() === 'LIQUIDATED'){
			return usageStatusEquipment.DTL;
		} else if(value.trim() === 'SHOWING_WAREHOUSE'){
			return usageStatusEquipment.DOK;
		} else if(value.trim() === 'IS_USED'){
			return usageStatusEquipment.DSD;
		} else if(value.trim() === 'SHOWING_REPAIR'){
			return usageStatusEquipment.DSC;
		}
		return -2;
	}
};

/**
 * Trang thai bien ban
 * 
 * @author hunglm16
 * @since December 30,2014
 * @description Quan Ly Thiet Bi
 * @params: 0: Du thao, 1: Chua duyet, 2: Da duyet, 3: Khong duyet, 4: Huy, 5: Dang sua chua, 6: Hoan tat sua chua , 7: Thanh lý
 * */
var statusRecordsEquip = {ALL: -2, DT: 0, CD: 1, DD: 2, KD: 3, HUY: 4, DSC: 5, HTSC: 6, TL: 7,
		parseValue: function(value){
			if(value == undefined || value == null){
				return '';
			} else if(value == statusRecordsEquip.ALL){
				return 'Tất cả';
			} else if(value == statusRecordsEquip.DT){
				return 'Dự thảo';
			} else if(value == statusRecordsEquip.CD){
				return 'Chờ duyệt';
			} else if(value == statusRecordsEquip.DD){
				return 'Duyệt';
			} else if(value == statusRecordsEquip.KD){
				return 'Không duyệt';
			} else if(value == statusRecordsEquip.HUY){
				return 'Hủy';
			}else if(value == statusRecordsEquip.DSC){
				return 'Đang sửa chữa';
			}else if(value == statusRecordsEquip.HTSC){
				return 'Hoàn tất sửa chữa';
			}else if(value == statusRecordsEquip.TL){
				return 'Đã thanh lý';
			}
			return '';
		},
		parseName: function(value){
			if(value == undefined || value == null){
				return -2;
			} else if(value == 'ALL'){
				return 'Tất cả';
			} else if(value == 'DRAFT'){
				return 'Dự thảo';
			} else if(value == 'WAITING_APPROVAL'){
				return 'Chưa duyệt';
			} else if(value == 'APPROVED'){
				return 'Đã duyệt';
			} else if(value == 'NO_APPROVAL'){
				return 'Duyệt';
			} else if(value == 'CANCELLATION'){
				return 'Hủy';
			}else if(value == 'ARE_CORRECTED'){
				return 'Đang sửa chữa';
			}else if(value == 'COMPLETION_REPAIRS'){
				return 'Hoàn tất sửa chữa';
			}else if(value == 'LIQUIDATED'){
				return 'Đã thanh lý';
			}
			return -2;
		}
	};
/**
 * Trang thai giao nhan
 * 
 * @author hunglm16
 * @since December 30,2014
 * @description Quan Ly Thiet Bi
 * @params: 0: Chua gui, 1: Da gui, 2: Da nhan
 * */
var deliveryStatus = {ALL: -2, CG: 0, DG: 1, DN: 2,
		parseValue: function(value){
			if(value == undefined || value == null){
				return '';
			} else if(value == deliveryStatus.ALL){
				return 'Tất cả';
			} else if(value == deliveryStatus.CG){
				return 'Chưa gửi';
			} else if(value == deliveryStatus.DG){
				return 'Đã gửi';
			} else if(value == deliveryStatus.DN){
				return 'Đã nhận';
			}
			return '';
		},
		parseName: function(value){
			if(value == undefined || value == null){
				return -2;
			} else if(value == 'ALL'){
				return 'Tất cả';
			} else if(value == 'NOTSEND'){
				return 'Chưa gửi';
			} else if(value == 'SENT'){
				return 'Đã gửi';
			} else if(value == 'RECEIVED'){
				return 'Đã nhận';
			}
			return -2;
		}
	};
/**
 * Trang thai bien ban kiem ke
 * 
 * @author nhutnn
 * @since 28/01/2015
 */
var statisticStatus = {DT: 0, HD: 1, HT: 2, HUY: 3,
		parseValue: function(value){
			if(value == undefined || value == null){
				return '';
			} else if(value == statisticStatus.DT){
				return 'Dự thảo';
			} else if(value == statisticStatus.HD){
				return 'Hoạt động';
			} else if(value == statisticStatus.HT){
				return 'Hoàn thành';
			} else if(value == statisticStatus.HUY){
				return 'Hủy';
			}
			return '';
		},
		parseName: function(value){
			if(value == undefined || value == null){
				return -2;
			} else if(value == 'DRAFT'){
				return 'Dự thảo';
			} else if(value == 'RUNNING'){
				return 'Hoạt động';
			} else if(value == 'DONE'){
				return 'Hoàn thành';
			} else if(value == "DELETED"){
				return 'Hủy';
			}
			return -2;
		}
	};
/**
 * Trang thai kiem ke
 * 
 * @author nhutnn
 * @since 02/02/2015
 */
var InventoryStatus = {CKK: 0, CON: 2, MAT: 3,
		parseValue: function(value){
			if(value == undefined || value == null){
				return '';
			} else if(value == InventoryStatus.CKK){
				return 'Chưa kiểm kê';
			} else if(value == InventoryStatus.CON){
				return 'Còn';
			} else if(value == InventoryStatus.MAT){
				return 'Mất';
			} 
			return '';
		},
		parseName: function(value){
			if(value == undefined || value == null){
				return -2;
			} else if(value == 'NOT_INVENTORY'){
				return 'Chưa kiểm kê';
			} else if(value == 'STILL'){
				return 'Còn';
			} else if(value == 'LOST'){
				return 'Mất';
			}
			return -2;
		}
	};

/**
 * Loai chi tiet nhom san pham doanh so CTTB
 * 
 * @author nhutnn
 * @since 04/03/2015
 */
var DisplayGroupDtlObjectType = {SP: 1, NG: 2, 
		parseValue: function(value){
			if(value == undefined || value == null){
				return '';
			} else if(value == statisticStatus.SP){
				return 'Sản phẩm';
			} else if(value == statisticStatus.NG){
				return 'Ngành';
			} 
			return '';
		},
		parseName: function(value){
			if(value == undefined || value == null){
				return -2;
			} else if(value == 'PRODUCT'){
				return 'Sản phẩm';
			} else if(value == 'PRODUCT_INFO'){
				return 'Ngành';
			}
			return -2;
		}
	};

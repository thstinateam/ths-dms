/**
 * 
 */
var DropzonePlugin = {
	_elementId: null,
	_dropzoneObject: null,
	_dropzoneView: null,

	initializeDropzoneUploadElement: function(elementSelector, elementId, options) {
		DropzonePlugin._elementId = elementId;

		var dropzone = new Dropzone(elementSelector, options);
		DropzonePlugin._dropzoneObject = dropzone;
		DropzonePlugin._dropzoneView = dropzone;
		return dropzone;
	},
	updateAdditionalDataForUpload: function(data) {
		if (DropzonePlugin._dropzoneObject) {
			DropzonePlugin._dropzoneObject.options.params = data;
			DropzonePlugin._dropzoneObject.options.params.token = $('#token').val().trim();
		}
	},
	startUpload: function() {
		if (DropzonePlugin._dropzoneObject) {
			var files = DropzonePlugin._dropzoneObject.getFilesWithStatus(Dropzone.ADDED);
			if (files && files instanceof Array && files.length != 0) {
				DropzonePlugin._dropzoneObject.enqueueFiles(files);
				DropzonePlugin._dropzoneObject.processQueue();				
			}
		}
	},
	setupOthersEvent: function() {
		/*DropzonePlugin._dropzoneObject.on("addedfile", function(file) {
			// Hookup the start button
			file.previewElement.querySelector(".start").onclick = function() {
				DropzonePlugin._dropzoneObject.enqueueFile(file);
			};
		});*/

		// Update the total progress bar
		DropzonePlugin._dropzoneObject.on("totaluploadprogress", function(progress) {
			document.querySelector("#total-progress .progress-bar").style.width = progress + "%";
			document.querySelector("#total-progress .progress-bar").innerHTML = Math.round(progress) + '%';
		});
		 
		DropzonePlugin._dropzoneObject.on("sending", function(file) {
			// Show the total progress bar when upload starts
			document.querySelector("#total-progress").style.opacity = "1";
			// And disable the start button
			//file.previewElement.querySelector(".start").setAttribute("disabled", "disabled");
		});
		 
		// Hide the total progress bar when nothing's uploading anymore
		// DropzonePlugin._dropzoneObject.on("queuecomplete", function(progress) {
		// 	document.querySelector("#total-progress").style.opacity = "0";
		// });

		/*document.querySelector("#actions .start").onclick = function() {
			DropzonePlugin._dropzoneObject.enqueueFiles(DropzonePlugin._dropzoneObject.getFilesWithStatus(Dropzone.ADDED));
		};
		document.querySelector("#actions .cancel").onclick = function() {
			DropzonePlugin._dropzoneObject.removeAllFiles(true);
		};*/
	},
	readdFailFileForUpload: function(fileNames) {
		var files = DropzonePlugin._dropzoneObject.getFilesWithStatus(Dropzone.SUCCESS);
		if (files && files.length != 0) {
			files.forEach(function(file) {
				var foundFile = fileNames.filter(function(fileName) {
									return fileName == file.name;
								});
				if (foundFile && foundFile.length == 1) {
					file.status = Dropzone.ADDED;
					DropzonePlugin._dropzoneObject.enqueueFiles(file);
				} else {
					DropzonePlugin._dropzoneObject.removeFile(file);
				}
			});
		}
	},
	clearAllSuccessUploadedFile: function() {
		var files = DropzonePlugin._dropzoneObject.getFilesWithStatus(Dropzone.SUCCESS);
		if (files && files.length != 0) {
			files.forEach(function(file) {
				DropzonePlugin._dropzoneObject.removeFile(file);
			});
		}
	},
	clearAllAddUploadedFile: function() {
		/**Cach 1*/
		DropzonePlugin._dropzoneObject.removeAllFiles();
		/**Cach 2*/
		// var files = DropzonePlugin._dropzoneObject.getFilesWithStatus(Dropzone.ADDED); // nhung file nay file Big Size khong lay duoc.
		/** var files = DropzonePlugin._dropzoneObject.files;	
		if (files && files.length != 0) {
			files.forEach(function(file) {
				DropzonePlugin._dropzoneObject.removeFile(file);
			});
		}
		*/
	},

	/**
	 * load all file upload 
	 * dang dung tao van de
	 * @author vuongmq
	 * @since 25/11/2015
	 */
	loadAllFileUpload: function() {
		var files = DropzonePlugin._dropzoneObject.getFilesWithStatus(Dropzone.SUCCESS);
		files.forEach(function(file) {
	        file.status = Dropzone.ADDED;
	        DropzonePlugin._dropzoneObject.enqueueFiles(file);
		});
	},
}

var UploadUtil = {
	uploadObject: null,
	_uploadCallback: null,
	_maxSize: 20 * 1024 * 1024, // 20MB; 1024KB * 1024B
	_maxSizeTotal: 100 * 1024 * 1024, // 100MB; 1024KB * 1024B
	_SIZE_1024: 1024, //1Byte
	_typeFileRequireCheck: {
		equipRecord: 2,
		equipRecordTxt: 'image/jpg,image/jpeg,image/bmp,image/png,image/gif,application/pdf,application/vnd.ms-excel,application/vnd.openxmlformats-officedocument.spreadsheetml.sheet,application/msword,application/vnd.openxmlformats-officedocument.wordprocessingml.document',
		equpRecMaxsize: 5*1024*1024
	},
	initializeUploadElement: function(elementSelector, elementId, options) {
		UploadUtil.uploadObject = DropzonePlugin.initializeDropzoneUploadElement(elementSelector, elementId, options);
		//UploadUtil.uploadObject
		DropzonePlugin.setupOthersEvent();
	},
	updateAdditionalDataForUpload: function(data) {
		DropzonePlugin.updateAdditionalDataForUpload(data);
	},
	startUpload: function(errMsg) {
		if (DropzonePlugin._dropzoneObject) {
			var filesErr = DropzonePlugin._dropzoneObject.getFilesWithStatus(Dropzone.ERROR);
			if (filesErr && filesErr instanceof Array && filesErr.length != 0) {
				//$('#'+ errMsg).html(catalog_product_video_hinh_anh_lon).show(); 
				return false; // khong cho tai len
			}
			var files = DropzonePlugin._dropzoneObject.getFilesWithStatus(Dropzone.ADDED);
			if (files && files instanceof Array && files.length != 0) {
				//vuongmq: kiem tra total size ton tai > UploadUtil._maxSizeTotal bao loi
				var flagCheck = UploadUtil.checkTotalSizeFileUpload(files , UploadUtil._maxSizeTotal);
				if(flagCheck){
					$('#divOverlay').show(); //vuongmq
					DropzonePlugin.startUpload();
				} else {
					//$('#'+ errMsg).html('You can upload maximum 100MB only!').show();
					$('#'+ errMsg).html(jsp_common_s_product_upload_maximum).show();
				}
			} else {
				//$('#'+ errMsg).html('Please add images to upload!').show();
				$('#'+ errMsg).html(jsp_common_s_product_please_upload_image_video).show();
			}
		}
	},

	/**
	 * Check total size upload
	 * @author vuongmq
	 * @param files
	 * @param maxSizeTotal
	 * @since 28-Nov, 2014
	 */
	checkTotalSizeFileUpload: function(files, maxSizeTotal) {
		var totalSize = 0;
		for (var i = 0; i < files.length ; i++) {
			totalSize += files[i].size;
		}
		if (totalSize <= maxSizeTotal) {
			return true;
		} else {
			return false;
		}
	},

	/**
	 * upload load file; 
	 * dang dung tao van de
	 * @author vuongmq
	 * @param errMsg
	 * @param maxSizeTotal
	 * @since 25/11/2015
	 */
	startUploadFile: function(errMsg, maxSizeTotal) {
		if (DropzonePlugin._dropzoneObject) {
			var filesErr = DropzonePlugin._dropzoneObject.getFilesWithStatus(Dropzone.ERROR);
			if (filesErr && filesErr instanceof Array && filesErr.length != 0) {
				//$('#'+ errMsg).html(catalog_product_video_hinh_anh_lon).show(); 
				return false; // khong cho tai len
			}
			var files = DropzonePlugin._dropzoneObject.getFilesWithStatus(Dropzone.ADDED);
			if (files && files instanceof Array && files.length != 0) {
				//vuongmq: kiem tra total size ton tai > maxSizeTotal bao loi
				var flagCheck = UploadUtil.checkTotalSizeFileUpload(files , maxSizeTotal);
				if (flagCheck) {
					$('#divOverlay').show(); //vuongmq
					DropzonePlugin.startUpload();
				} else {
					//$('#'+ errMsg).html('Vui long upload maximum {0}Mb.').show();
					var errMaxTotal = Math.round(maxSizeTotal / UploadUtil._SIZE_1024 / UploadUtil._SIZE_1024, 2);
					$('#'+ errMsg).html(format(jsp_common_s_upload_maximum, errMaxTotal)).show();
				}
			} else {
				//$('#'+ errMsg).html('Vui long chon file de upload.').show();
				$('#'+ errMsg).html(jsp_common_s_please_upload_select).show();
			}
		}
	},

	removeSuccessUploadFile: function() {
		
	},
	hideTotalProgressBar: function() {
		document.querySelector("#total-progress").style.opacity = "0";
	},
	readdFailFileForUpload: function(fileNames) {
		DropzonePlugin.readdFailFileForUpload(fileNames);
	},
	clearAllSuccessUploadedFile: function() {
		DropzonePlugin.clearAllSuccessUploadedFile();
	},
	clearAllAddUploadedFile: function() {
		DropzonePlugin.clearAllAddUploadedFile();
	},
	
	/**
	 * load all file upload 
	 * dang dung tao van de
	 * @author vuongmq
	 * @since 25/11/2015
	 */
	loadAllFileUpload: function() {
		DropzonePlugin.loadAllFileUpload();
	},
	getMsgByFileRequireCheck: function(value) {
		if (value == undefined || value == null) {
			return 'Không xác định được danh mục mà tập tin muốn thêm';
		}
		var arrFile = DropzonePlugin._dropzoneObject.getFilesWithStatus(Dropzone.ADDED);
		if (arrFile == undefined || arrFile == null) {
			return 'Không có tập tin nào được chọn';
		}
		if (UploadUtil._typeFileRequireCheck.equipRecord == value) {
			var arrTypeFile = UploadUtil._typeFileRequireCheck.equipRecordTxt.split(',');
			for (var i = 0, size = arrFile.length; i < size; i++) {
				var fileItem = arrFile[i];
				var flag = false;
				for (var j = 0, size1 = arrTypeFile.length; j < size1; j++) {
					var typeFile = arrTypeFile[j];
					typeFile = typeFile.trim();
					var type = fileItem.type.trim();
					if (type === typeFile) {
						flag = true;
						break;
					}
				}
				if (!flag) {
					return "Định dạng tập tin " + fileItem.name + " không hợp lệ";
				}
				if (fileItem.size > UploadUtil._typeFileRequireCheck.equpRecMaxsize) {
					return "Tập tin " + fileItem.name + " lớn hơn " + UploadUtil._typeFileRequireCheck.equpRecMaxsize/1024/1024 + 'Mb';
				}
			}
		}
		return '';
	},
	/**
	 * Upload File Dung chung cho danh sach thiet bi
	 * 
	 * @author hunglm16
	 * @since January 29,2015
	 * */
	initUploadFileByEquipment: function(obts, callback) {
		if (obts == undefined || obts == null) {
			return;
		}
		if (callback != undefined && callback != null) {
			UploadUtil._uploadCallback = callback;
		}
		var elementSelector = (obts.elementSelector != null && obts.elementSelector != undefined) ? obts.elementSelector : '';
		var elementId = (obts.elementId != null && obts.elementId != undefined) ? obts.elementId : '';
		var clickableElement = (obts.clickableElement != null && obts.clickableElement != undefined) ? obts.clickableElement : '';
		var previewNode = (obts.previewId != null && obts.previewId != undefined) ? document.querySelector(obts.previewId) : document.querySelector("#template");
		previewNode.id = "";
		var previewTemplate = previewNode.parentNode.innerHTML;
		previewNode.parentNode.removeChild(previewNode);
		UploadUtil.initializeUploadElement(elementSelector, elementId, {
			url: (obts.url != null && obts.url != undefined) ? obts.url : '/commons/uploadFileForEquipment',
			paramName: function(fileOrderNumber) {
				return (obts.lstFile != null && obts.lstFile != undefined) ? obts.lstFile: 'lstFile';
			},
			autoProcessQueue: (obts.autoProcessQueue != null && obts.autoProcessQueue != undefined) ? obts.autoProcessQueue: false,
			parallelUploads: (obts.parallelUploads != null && obts.parallelUploads != undefined) ? obts.parallelUploads: 1000,
			uploadMultiple: (obts.uploadMultiple != null && obts.uploadMultiple != undefined) ? obts.uploadMultiple: true,
			thumbnailWidth: (obts.thumbnailWidth != null && obts.thumbnailWidth != undefined) ? obts.thumbnailWidth: 50,
			thumbnailHeight: (obts.thumbnailHeight != null && obts.thumbnailHeight != undefined) ? obts.thumbnailHeight: 50,
			previewTemplate: previewTemplate,			
			autoQueue: (obts.autoQueue != null && obts.autoQueue != undefined) ? obts.autoQueue: false,
			previewsContainer: (obts.previewsContainer != null && obts.previewsContainer != undefined) ? obts.previewsContainer: "#previews",
			//acceptedFiles: (obts.acceptedFiles != null && obts.acceptedFiles != undefined) ? obts.acceptedFiles: "image/*, application/pdf, .doc, .docx, .xls, .xlsx",
			//maxFilesize: (obts.maxFilesize != null && obts.maxFilesize != undefined) ? obts.maxFilesize: 10,// MB
			clickable: clickableElement,
			successmultiple: function(files, responseData) {
				var data = {};
				data.error = (responseData.error != null && responseData.error != undefined) ? responseData.error : true;
				data.token = (responseData.token != null && responseData.token != undefined) ? responseData.token : "";
				data.errMsg = (responseData.errMsg != null && responseData.errMsg != undefined) ? responseData.errMsg : "";
				UploadUtil.hideTotalProgressBar();
				if (responseData) {
					var failCount = 0;
					if (responseData.fail && responseData.fail.length > 0) {
						failCount = responseData.fail.length;
						UploadUtil.readdFailFileForUpload(responseData.fail);
					} else {
						UploadUtil.clearAllSuccessUploadedFile();
					}
					if (obts.divOverlay != null && obts.divOverlay != undefined) {
						$(obts.divOverlay).hide();
					} else {
						$('#divOverlay').hide();
					}
				}
				if (callback != undefined && callback != null) {
					callback.call(this, data);		
				}
			}
		});
	}
}
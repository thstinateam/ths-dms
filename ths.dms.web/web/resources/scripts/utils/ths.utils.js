var Utils = {
	VAR_NAME: '_var_name_',
	_currentSearchCallback: null,	
	_CODE: 111,
	_NAME: 2,
	_ADDRESS: 3,
	_SPECIAL: 99,
	_SERIAL: 21,
	_CTRL_PRESS: false,
	_SHIFT_PRESS: false,
	_TF_A_Z: 1,
	_TF_NUMBER: 2,
	_TF_NUMBER_DOT: 3,
	_TF_NUMBER_COMMA: 4,
	_TF_NUMBER_SIGN: 5,
	_TF_BAN_CHAR: 6,
	_TF_NUMBER_CONVFACT: 7,
	_TF_NUMBER_COMMA_AND_DOT: 8,
	_DATE_DD_MM_YYYY: 9,
	_DATE_MM_YYYY:10,
	_TF_CAR_NUMBER: 11,
	_TF_PHONE_NUMBER: 20,
	_arr_Chk_State: null,
	_NUMBER_TYPE:2,
	_STRING_TYPE:1,
	_DATE_TYPE:3,
	_CHOICE:4,
	_MULTI_CHOICE:5,
	_LOCATION : 6,
	_PAYROLL:12,
	_NAME_CUSTYPE:13,
	_MapControl:new Map(),
	_excelFile: null,
	_errExcelMsg: null,
	_inputTextFileExcel: null,
	_formIdFileExcel: null,
	_xhrReport:null,
	_formIdFileExcel: null,
	_inputText : null,
	_MATH_ROUND: 0,
	_MATH_FLOOR: -1,
	_MATH_CEIL: 1,
	_isShowInputTextByDlSearch2 : {
		id : 1,
		code : 2,
		name : 3,
		text : 4 //Code - Name
	},
	
	/**
	 * Lay ngay he thong
	 * 
	 * @author hunglm16
	 * @since October 7,2014
	 * @description Yeu cau khong coa ham nay vi dung den nhieu chuc nang
	 * */
	currentDate:function() {
		var now = new Date();
		var cYear = now.getFullYear();
		var cMonth = now.getMonth() + 1;
		var cDate = now.getDate();
		var currentDate = cDate+'/'+cMonth+'/'+cYear;
		return currentDate; // b
	},
	
	/**
	 * Xu ly su kien so sanh ngay tra ve cac gia tri so
	 * 
	 * @return 0: Bang nhau
	 * @return 1: d1 > d2
	 * @return -1: d1 < d2
	 * 
	 * @author hunglm16
	 * @since May 22, 2015
	 * */
	compareDateForTowDate: function(d1, d2) {
		if(d1.length == 0 || d2.length == 0){
			return true;			
		}
		var arrD1 = d1.split('/');
		var arrD2 = d2.split('/');
		var d1Obj = dates.convert(arrD1[1] + '/' + arrD1[0] + '/' + arrD1[2]);
		var d2Obj = dates.convert(arrD2[1] + '/' + arrD2[0] + '/' + arrD2[2]);
		
		return dates.compare(d1Obj, d2Obj);
	},
	
	language_vi:function(objectName){//20150602
		if(objectName!=null && objectName.length>0){
			var tmp=objectName.replace(/\./g,'_');
			if(tmp.length>0 && window[tmp])objectName=window[tmp];
		}
		return objectName;
	},
	compareCurrentMonthEx: function(monthTxt) {//neu monthTxt >= thang hien tai => true. ko thi false
		var currentDateTxt = toDateString(new Date());
		var arrCurrentDate = currentDateTxt.split('/');
		var arrMonthTxt = monthTxt.split('/');
		if((Number(arrMonthTxt[1]) > Number(arrCurrentDate[2])) || (Number(arrMonthTxt[1]) == Number(arrCurrentDate[2]) && arrMonthTxt[0]>=arrCurrentDate[1])) {
			return true;
		}
		return false;
	},
	/**
	 *Bat su kien choose khi su dung DialogSearch2 cua viettel-utils-js.js
	 *
	 *@author hunglm16
	 *@since September 28,2014
	 * */
	fillCodeForInputTextByDialogSearch2 : function (txt, id, code, name, isShow) {
		if (isShow == undefined || isShow == null) {
			$('#'+txt.trim()).val(Utils.XSSEncode(code+' - '+name));
			$('#'+txt.trim()).attr(txt + '_' + id, id);
		} else if (Utils._isShowInputTextByDlSearch2.code == isShow) {
			$('#'+txt.trim()).val(Utils.XSSEncode(code));
			$('#'+txt.trim()).attr(txt + '_' + id, id);
		} else if (Utils._isShowInputTextByDlSearch2.name == isShow) {
			$('#'+txt.trim()).val(Utils.XSSEncode(name));
			$('#'+txt.trim()).attr(txt + '_' + id, id);
		} else if (Utils._isShowInputTextByDlSearch2.id == isShow) {
			$('#'+txt.trim()).val(id);
			$('#'+txt.trim()).attr(txt + '_' + id, id);
		} else {
			$('#'+txt.trim()).val("");
			$('#'+txt.trim()).attr(txt + '_0', 0);
		}
		$('#common-dialog-search-2-textbox').dialog("close");
		$('#common-dialog-search-2-textbox').remove();
	},
	/**
	 *Bat su kien choose khi su dung DialogSearch2 cua viettel-utils-js.js
	 *
	 *@author longnh15 - override hunglm16
	 *@since September 28,2014
	 * */
	fillCodeForInputTextByDialogSearchWithUnder : function (txt, txtUnder, id, code, name, isShow) {
		if (isShow == undefined || isShow == null) {
			$('#'+txt.trim()).val(Utils.XSSEncode(code+' - '+name));
			$('#'+txt.trim()).attr(txt + '_' + id, id);
		} else if (Utils._isShowInputTextByDlSearch2.code == isShow) {
			$('#'+txt.trim()).val(Utils.XSSEncode(code));
			$('#'+txt.trim()).attr(txt + '_' + id, id);
		} else if (Utils._isShowInputTextByDlSearch2.name == isShow) {
			$('#'+txt.trim()).val(Utils.XSSEncode(name));
			$('#'+txt.trim()).attr(txt + '_' + id, id);
		} else if (Utils._isShowInputTextByDlSearch2.id == isShow) {
			$('#'+txt.trim()).val(id);
			$('#'+txt.trim()).attr(txt + '_' + id, id);
		} else {
			$('#'+txt.trim()).val("");
			$('#'+txt.trim()).attr(txt + '_0', 0);
		}
		$('#'+txtUnder.trim()).val($('#'+txt.trim()).val());
		$('#common-dialog-search-2-textbox').dialog("close");
		$('#common-dialog-search-2-textbox').remove();
	},
	/**
	 * Phan quyen control tren cac request Ajax
	 * @author HungLm16; 
	 * @param idDiv
	 * @param filterCallback
	 * @since September 14,2014
	 */
	functionAccessFillControl: function (idDiv, filterCallback) {
		var selector = '';
		if (idDiv != undefined && idDiv != null && idDiv.trim().length > 0) {
			idDiv = '#' + idDiv.trim();
			selector = idDiv + ' .cmsiscontrol';
		} else {
			selector = '.cmsiscontrol';
		}
		$(selector).hide();
		if (!_isFullPrivilege) {
			if (_MapControl != undefined && _MapControl != null && _MapControl.size() > 0) {
				var arrayId = _MapControl.keyArray;
				for (var i = 0, size = arrayId.length; i < size; i++) {
					var id = arrayId[i].trim();
					var status = Number(_MapControl.get(id));
					if (status == undefined || status == null || status == 0) {
						$(selector + '[id^="'+id+'"]').remove();
						$(selector + "#" + id).remove();
					} else if (status != 1) {
						$(selector + "#" + id).show();
						$(selector + '[id^="'+id+'"]').show();
						$(selector + '[id^="'+id+'"]').attr("cmsByMapControl", "cmsByMapControl");
						if (status == 2) {
							enable(id);
							$(selector + '[id^="'+id+'"]').each(function() {
								$(this).removeAttr('disabled', 'disabled').removeClass('BtnGeneralDStyle');
							});
							$(selector + '[id^="'+id+'"] input').each(function() {
								$(this).removeAttr('disabled', 'disabled').removeClass('BtnGeneralDStyle');
							});
							$(selector + '[id^="'+id+'"] button').each(function() {
								$(this).removeAttr('disabled', 'disabled').removeClass('BtnGeneralDStyle');
							});
						} else if (status == 3) {
							disabled(id);
							$(selector + '[id^="'+id+'"]').attr('disabled', 'disabled').addClass('BtnGeneralDStyle');
							$(selector + '[id^="'+id+'"]').addClass("isNotPointer");
							$(selector + '[id^="'+id+'"] input').each(function() {
								$(this).attr('disabled', 'disabled').addClass('BtnGeneralDStyle').addClass('isNotPointer');
							});
							$(selector + '[id^="'+id+'"] button').each(function() {
								$(this).attr('disabled', 'disabled').addClass('BtnGeneralDStyle').addClass('isNotPointer');
							});
						}
					} else {
						$(selector + '[id^="'+id+'"]').attr("cmsByMapControl", "cmsByMapControl");
						$(selector + '[id^="'+id+'"]').addClass("isCMSInvisible");
						$(selector + '[id^="'+id+'"]').each(function() {
							$(this).hide();
						});
					}
				}
			}
			var arrCmsHiden = $(selector + ':hidden');
			var size = arrCmsHiden.length;
			for (var i = 0; i < size; i++) {
			  if (!$(arrCmsHiden[i]).attr("cmsByMapControl")) {
				  $(arrCmsHiden[i]).remove();
			  } else {
				  $(arrCmsHiden[i]).removeAttr("cmsByMapControl");
			  }
			}
		} else {
			$(selector + ':hidden').show();
		}
		if (filterCallback != undefined && filterCallback != null) {
			filterCallback.call(this);
		}
	},
	
	///@author Hung Lm16; @since JULY 02,2014; @description Ham Kiem tra file Excel
	previewImportExcelFileUtils: function(fileObj, formId, inputText){
		var inputId = 'fakefilepcDisplay';
		if(inputText != null || inputText != undefined){
			inputId = inputText;
			Utils._inputText= inputText;
		}
		var fileTypes = ["xls", "xlsx", "xlsm"];
		if (fileObj.value != '') {
			if (!/(\.xls|\.xlsx|\.xlsm)$/i.test(fileObj.value)) {
				$('#'+Utils._errExcelMsg.trim()).html(msgErr_excel_file_invalid_type).show();
				fileObj.value = '';
				fileObj.focus();
				document.getElementById(inputId).value ='';			
				return false;
			} else {
				$('.ErrorMsgStyle').html('').hide();
				var src = '';
				if ($.browser.msie){
				    var path = encodeURI(what.value);
				    path = path.replace("C:%5Cfakepath%5C","");
				    var path = decodeURI(path);
				    $('#'+inputId.trim()).val(Utils.XSSEncode(path));
				}else{
					$('#'+inputId.trim()).val(Utils.XSSEncode(fileObj.value));
				}	
			}
		}else{
			$('#'+ Utils._errExcelMsg.trim()).html(common_result_import_excel_not_file_exist).show();		
			return false;
		}
		return true;
	},

	/**
	 * Import excel utils
	 * @author hunglm16
	 * @since 04/09/2015
	 *
	 * @author vuongmq; modify
	 * @params isNotOpenConfirm
	 * @since 22/10/2015 
	 */
	importExcelUtils: function(callBack, objectId, inputByObjectId, errExcelMsg, isNotOpenConfirm) {
 		if (callBack != undefined && callBack != null) {
 			Utils._importCallback = callBack;
 		}
 		if (errExcelMsg == undefined || errExcelMsg == null || errExcelMsg.trim().length == 0) {
 			Utils._errExcelMsg = 'errExcelMsg';
 		} else {
 			Utils._errExcelMsg = errExcelMsg;
 		}
 		var excelURI = $('#'+inputByObjectId).val();
 		if (excelURI == undefined || excelURI == null || excelURI.trim().length == 0) {
 			$('#'+Utils._errExcelMsg.trim()).html('Vui lòng chọn tập tin Excel').change().show();
 			return false;
 		} else {
 			Utils._inputTextFileExcel = inputByObjectId;
 		}
 		
 		if (objectId == undefined || objectId == null || objectId.trim().length == 0) {
 			$('#'+Utils._errExcelMsg.trim()).html('Không tìm thấy form dữ liệu chứa File Excel').change().show();
 			return false;
 		} else {
 			Utils._formIdFileExcel = objectId;
 		}
 		if (isNotOpenConfirm == undefined || isNotOpenConfirm == null || isNotOpenConfirm == false) {
	 		$.messager.confirm('Xác nhận', 'Bạn có muốn nhập file không?', function(r) {  
				if (r) {
					var params = {};
					if ($('#excelType').length > 0) {
						params.excelType = $('#excelType').val();
					}
					if ($('#proType').length > 0) {
						params.proType = $('#proType').val();
					}
					params.token = $("#token").val().trim();
					var options = { 
						beforeSubmit: Utils.beforeImportExcel,   
				 		success:      Utils.afterImportExcel, 
				 		type: "POST",
				 		dataType: 'html',
				 		data:(params)
				 	};
					$('#'+objectId).ajaxForm(options);
			 		$('#'+objectId).submit();
			 		return false;
				} else {
					$('#'+inputByObjectId).val("");
			 		return false;
				}
			});
		} else {
			var params = {};
			if ($('#excelType').length > 0) {
				params.excelType = $('#excelType').val();
			}
			if ($('#proType').length > 0) {
				params.proType = $('#proType').val();
			}
			params.token = $("#token").val().trim();
			var options = { 
				beforeSubmit: Utils.beforeImportExcel,   
		 		success:      Utils.afterImportExcel, 
		 		type: "POST",
		 		dataType: 'html',
		 		data:(params)
		 	};
			$('#'+objectId).ajaxForm(options);
	 		$('#'+objectId).submit();
	 		return false;
		}
 		return false;
 		
 	},
 	///@author HungLm16; @since JULY 02,2014; @description Xu ly truoc khi Import Excel
	beforeImportExcel: function(){
		var idDiv = Utils._inputTextFileExcel;
		if (idDiv==undefined || idDiv==null) {
			idDiv = "excelFile";
		}
		if (!Utils.previewImportExcelFileUtils(document.getElementById(idDiv), Utils._formIdFileExcel,Utils._inputText)) {
			return false;
		}
		$('#errExcelMsg').html('').hide();
		$('#successExcelMsg').html('').hide();
		showLoadingIcon();
	},
	
	/**
	 * Xu ly sau khi Import Excel
	 * 
	 * @author HungLm16
	 * @since JULY 02,2014
	 * 
	 * @author HungLm16
	 * @since May 05,2015
	 * @description Bo sung numFinish
	 * Muc dich bo sung: Import nhieu nhom long nhau, can dem so dong o muc chi tiet 
	 * */
	afterImportExcel: function(responseText, statusText, xhr, $form) {//ProductLevelCatalog
		hideLoadingIcon();
		var data = {};
		var mes = "";
		if (statusText == 'success') {
		    	$("#responseDiv").html(responseText);
		    	
		    	var newToken = $('#responseDiv #newToken').val();
		    	if (newToken != null && newToken != undefined && newToken != '') {
		    		$('#token').val(newToken);
		    		if ($("#tokenImport").length > 0) {
		    			$('#tokenImport').val(newToken);
		    		}
		    		if ($('#importTokenVal').length > 0) {
		    			$('#importTokenVal').val(newToken);
		    		}
		    	}
	    		if ($('#errorExcel').html().trim() == 'true' || $('#errorExcelMsg').html().trim().length > 0) {
	    			data.message = $('#errorExcelMsg').html();
	    			data.flag = false;
		    		$('#fakefilepc').val('');		
					Utils._importCallback.call(this, data);
					return ;
		    	} else {
		    		var totalRow = parseInt($('#totalRow').html().trim());
		    		var numFail = parseInt($('#numFail').html().trim());
		    		var numFinish = parseInt($('#numFinish').html().trim());
		    		var fileNameFail = $('#fileNameFail').html();
		    		
		    		if (!numFail) {
		    			numFail = 0;
		    		}
		    		if (!numFinish) {
		    			numFinish = 0;
		    		}
		    		if (numFinish > 0) {
		    			mes += format(msgErr_result_import_excel, numFinish, numFail);
		    		} else {
		    			mes += format(msgErr_result_import_excel, totalRow - numFail, numFail);
		    		}
		    		if (numFail > 0) {
		    			mes+= ' <a href="'+ fileNameFail +'">Xem chi tiết lỗi</a>';
		    		} else {
		    			mes = '<span style="color:#0587BB;">' + mes + '</span>';
		    		}
		    		if (Utils._importCallback != undefined && Utils._importCallback != null) {
		    			data.message = mes;
		    			data.flag = false;
		    			data.numFail = numFail; 
		    			data.numFinish = numFinish;
		    			Utils._importCallback.call(this, data);
		    		}
		    	}
	    		$('#fakefilepc').val('');
			$('#excelFile').val('');
	    }	
	},
	afterImportExcelNew : function(responseText, statusText, xhr, $form) {
		$('#divOverlay').removeClass('Overlay');
		$('#imgOverlay').hide();
		$('#divOverlay').hide();
		if (statusText == 'success') {
			$("#responseDiv").html(responseText);
			var newToken = $('#responseDiv #newToken').val();
	    	if(newToken != null && newToken != undefined && newToken != '') {
	    		$('#token').val(newToken);
	    		$('#tokenImport').val(newToken);
	    	}
			if ($('#errorExcel').html().trim() == 'true'
					|| $('#errorExcelMsg').html().trim().length > 0) {
				$('#errExcelMsg').html($('#errorExcelMsg').html()).show();
			} else {
				if ($('#typeView').html().trim() == 'false') {
					var totalRow = parseInt($('#totalRow').html().trim());
					var numFail = parseInt($('#numFail').html().trim());
					var fileNameFail = $('#fileNameFail').html();
					var mes = format(msgErr_result_import_excel,
							(totalRow - numFail), numFail);
					if (numFail > 0) {
						mes += ' <a href="' + fileNameFail
								+ '">Xem chi tiết lỗi</a>';
					}
					$('#errExcelMsg').html(mes).show();
				} else {
					$('#excelDialog').html(responseText);
					var html = $('#excelDialog').html();
					$.fancybox(html, {
						modal : true,
						title : catalog_customer_import_cat_info,
						afterShow : function() {
							$('.fancybox-inner #scrollExcelDialog')
									.jScrollPane();
							$('.fancybox-inner .ScrollSection').jScrollPane()
									.data().jsp;
						},
						afterClose : function() {
							$('#excelDialog').html(html);
						}
					});
				}
			}
		}
		$("#file").val("");
		$("#fakefilepc").val("");
		if (Utils._currentSearchCallback != undefined && Utils._currentSearchCallback != null) {
			Utils._currentSearchCallback.call(this);
		}
	},
	///@author hunglm16; @since JULY 28,2014; @description Update auto height For Row Grid
	updateRownumWidthAndHeightForDataGrid:function(parentId){	
		var pId = '';
		if(parentId!= null && parentId!= undefined){
			pId = '#'+parentId + ' ';
		}
		var lastValue=$(pId + '.datagrid-cell-rownumber').last().text().trim().length;
		if(lastValue > 0){
			var extWidth = 25;
			if(lastValue > 2){
				extWidth += (lastValue - 2) * 9;
			}  
			$(pId + '.datagrid-cell-rownumber').css('width',extWidth);
			$(pId + '.datagrid-header-rownumber').css('width',extWidth);
		}
		var length = $(pId).datagrid('getData').rows.length;
		if(length!=undefined && length!=null && length > 0){
			$(pId).datagrid('resize');
			$(pId + '.datagrid-header').css('height','42px');
			$(pId + '.datagrid-header-row').css('height','42px');
			$(pId + '.datagrid-header-row div.datagrid-cell').css({"word-wrap":"break-word", "white-space":"normal", "height":"auto"});
		}
		$(window).resize();
	},
	// Encodes the basic 4 characters used to malform HTML in XSS hacks
	XSSEncode : function(s,en){
		if (!isNullOrEmpty(s)) {
			s = s.toString();
		}
		if(!Utils.isEmpty(s)){
			en = en || true;
			// do we convert to numerical or html entity?
			s = s.replace(/&/g,"&amp;");
			if(en){
				s = s.replace(/'/g,"&#39;"); //no HTML equivalent as &apos is not cross browser supported
				s = s.replace(/"/g,"&quot;");
				s = s.replace(/</g,"&lt;");
				s = s.replace(/>/g,"&gt;");
			}else{
				s = s.replace(/'/g,"&#39;"); //no HTML equivalent as &apos is not cross browser supported
				s = s.replace(/"/g,"&#34;");
				s = s.replace(/</g,"&#60;");
				s = s.replace(/>/g,"&#62;");
			}
			return s;
		}else{
			return "";
		}
	},
	//hunglm16
	isEmpty : function(val){
		if(val){
			return ((val===null) || val.length==0 || /^\s+$/.test(val));
		}else{
			return true;
		}
	},

	previewImportImageFile:function(imgId,fakeId, errMsgId, imgTag){
		if (errMsgId == undefined || errMsgId == null) {
			errMsgId = "errMsg";
		}
		errMsgId = "#" + errMsgId;
		var fileObj = document.getElementById(imgId);
		if (fileObj.value != '') {
			if (!/(\.jpg|\.jpeg|\.png)$/i.test(fileObj.value)) {
				$(errMsgId).html(msgErr_image_file_invalid_type).show();
				fileObj.value = '';
				document.getElementById(fakeId).value ='';
				return false;
			} else {
				$(errMsgId).hide();
				if ($.browser.msie){
				    var path = encodeURI(what.value);
				    path = path.replace("C:%5Cfakepath%5C","");
				    path = decodeURI(path);
				    document.getElementById(fakeId).value = path;
				}else{
					document.getElementById(fakeId).value = fileObj.value;
				}
			}
		}
		if (imgTag != undefined && imgTag != null && imgTag.length > 0) {
			$("#" + imgTag).hide();
		}
		return true;
	},
	deleteFileExcelExport:function(excelFileName){
	},
	exportExcelTemplate:function(url){
 		$('#divOverlay').show();
		if(CommonSearch._xhrReport != null){
			CommonSearch._xhrReport.abort();
			CommonSearch._xhrReport = null;
		}
		CommonSearch._xhrReport = $.ajax({
			type : "GET",
			url : url,
			dataType: "json",
			success : function(data) {
				$('#divOverlay').hide();
				if(data.error && data.errMsg!= undefined){
					$('#'+ errMsg).html(formatString(msgCommon5, escapeSpecialChar(data.errMsg))).show();
				} else{
					if(data.hasData!= undefined && !data.hasData) {
						$('#'+ errMsg).html(msgCommon6).show();
					} else{
						var filePath = ReportUtils.buildReportFilePath(data.view);
						window.location.href = filePath;
						setTimeout(function(){ //Set timeout để đảm bảo file load lên hoàn tất
	                        CommonSearch.deleteFileExcelExport(data.view);
						},30000);
					}
				}
			},
			error:function(XMLHttpRequest, textStatus, errorDivThrown) {				
			}
		});
		return false;
	},
	communicationBetweenTwoTime : function(fD1, tD1, fD2, tD2){
		//Xet fD1 : -oo && tD1
		if(fD1==undefined || fD1==null || fD1.length ==0){
			if(tD1!=undefined && tD1!=null && tD1.trim().length>0){
				if(fD2!=undefined && fD2!=null && fD2.length>0){
					//neu fD2 > tD1 => false
					if(fD2!=undefined && fD2!=null && fD2.length>0 && !Utils.compareDate(fD2, tD1)){
						return false;
					}
				}
			}
		}else{
			//Xet fD1 && tD1 : +oo
			if(tD1==undefined || tD1==null || tD1.length ==0){
				if(tD2!=undefined && tD2!=null && tD2.length>0){
					//neu tD2 < fD1 => fase
					if(fD1!=undefined && fD1!=null && fD1.length>0 && !Utils.compareDate(fD1, tD2)){
						return false;
					}
				}
			}else{
				//fD1, fD2
				//Xet fD2 && tD2: +oo
				if(tD2==undefined || tD2==null || tD2.length ==0){
					if(!Utils.compareDate(fD2, tD1)){
						return false;
					}
				}else{
					//xet fD2:-oo && tD2
					if(fD2==undefined || fD2==null || fD2.length ==0){
						if(!Utils.compareDate(fD1, tD2)){
							return false;
						}
					}else{
						//fD1, tD1, fD2, tD2
						if(!Utils.compareDate(fD2, fD1) && !Utils.compareDate(fD2, tD1)){
							return false;
						}else if(!Utils.compareDate(fD1, fD2) && !Utils.compareDate(fD1,tD2)){
							return false;
						}
					}
				}
			}
		}
		//hai khoang thoi gian giao nhau
		return true;
	},
	
	getMessageOfRequireCheck: function(objectId, objectName,isSelectBox,checkMaxLengh){
		objectName=Utils.language_vi(objectName);
		if($('#' + objectId).val()==null || $('#' + objectId).val().trim() == '__/__/____'){
			$('#' + objectId).val('');
		}
		if(isSelectBox != undefined && isSelectBox && $('#' + objectId).hasClass('easyui-combobox') && $('#' + objectId).combobox('getValue').trim().length == 0) {
			$('#' + objectId).next().attr('id',objectId+'easyuicomboxId');
			$('#'+objectId+'easyuicomboxId input.combo-text').focus();
			return format(msgErr_required_field,objectName);
		
		} else if(!$('#' + objectId).hasClass('easyui-combobox') && 
						((isSelectBox!= undefined && isSelectBox && parseInt($('#' + objectId).val().trim()) < 0) 
					|| ((isSelectBox== undefined || !isSelectBox) && $('#' + objectId).val().trim().length == 0))){
			$('#' + objectId).focus();
			if(!isSelectBox){
				return format(msgErr_required_field,objectName);
			}else{
				return format(msgErr_required_choose_format,objectName);
			}			
		}
		if(checkMaxLengh==undefined || checkMaxLengh==null){
			var maxlength = $('#' + objectId).attr('maxlength');
			if(maxlength!= undefined && maxlength!= null && maxlength.length > 0){
				if($('#' + objectId).val().trim().replace(",","").length > maxlength){
					$('#' + objectId).focus();
					return format(msgErr_invalid_maxlength,objectName,maxlength);
				}
			}
		}		
		return '';
	},
	getMessageOfInvaildInteger:function(objectId, objectName){
		objectName=Utils.language_vi(objectName);
		var strString = $('#' + objectId).val().trim();
		strString = strString.replace(/,/g, '');
		strString = strString.trim();
		var strValidChars = "0123456789";		
		var blnResult = true;		
		for (var i = 0; i < strString.length && blnResult == true; i++)		      {
		      var strChar = strString.charAt(i);
		      if (strValidChars.indexOf(strChar) == -1){
		         blnResult = false;
		       }
		}
		if(!blnResult){
			return objectName + ' chỉ chứa các giá trị số nguyên.';
		}else{
			return '';
		}		
	},
	getMessageOfSpecialCharactersValidate: function(objectId, objectName,type){
		objectName=Utils.language_vi(objectName);
		var selector = $('#' + objectId);
		var value = '';
		if(selector.hasClass('easyui-combobox')){
			value = $('#' + objectId).combobox('getValue').trim();
		}else{
			value = $('#' + objectId).val().trim();
		}	
		if(type == null || type == undefined){
			type = Utils._NAME;
		}
		var errMsg = '';
		if(value.length > 0){
			switch (type) {
			case Utils._CODE:
				if(!/^[a-zA-Z0-9_]+$/.test(value)){
					errMsg = format(msgErr_invalid_format_code,objectName);
				}
				break;
			case Utils._NAME:
				if(!/^[^<|>|?|\\|\'|\"|&|~#|$|%|@|*|(|)|^|`]+$/.test(value)){
					errMsg = format(msgErr_invalid_format_name,objectName);
				}
				break;
			case Utils._ADDRESS:
				if(!/^[^<|>|?|\\|\'|\"|&|~]+$/.test(value)){
					errMsg = format(msgErr_invalid_format_address,objectName);
				}
				break;
			case Utils._SPECIAL:
				if (!/^[^<|>|?|\\|\'|\"|&|~]+$/.test(value)) {
					errMsg = format(msgErr_invalid_format_special, objectName);
				}
				break;
			case Utils._PAYROLL:
				value = value.substring(3);
				if(!/^[0-9a-zA-Z]+$/.test(value)){
					errMsg = format(msgErr_invalid_format_payroll,objectName);
				}
				break;
			case Utils._TF_NUMBER_COMMA:
				if(!/^[0-9a-zA-Z_]+$/.test(value)){
					errMsg = format(msgErr_invalid_format_tf_id_number,objectName);
				}
				break;
			case Utils._NAME_CUSTYPE:
				if(!/^[^<|>|?|\\|\'|\"|&|~#|$|%|@|*|^|`]+$/.test(value)){
					errMsg = format(msgErr_invalid_format_name,objectName);
				}
				break;
			case Utils._SERIAL:
				if(!/^[0-9a-zA-Z-_.,^<|>|?|\\|\'|\"|&|~#|$|%|@|*|(|)|^|`]+$/.test(value)){
					errMsg = format(msgErr_invalid_format_tf_id_number,objectName);
				}
				break;
			default:
				break;
			}
		}	
		if(errMsg.length !=0){
			$('#' + objectId).focus();
		}
		return errMsg;
	},
	getMessageValidateLot:function(objectId){
		var lot = $('#'+objectId).val();
		if((lot==null || lot==undefined)|| (lot!=null && lot!=undefined && lot.length!=6)){
			$('#' + objectId).focus();
			return 'Số lô phải đủ 6 ký tự theo định dạng ddMMyy. Ví dụ: 011012';			
		}
		var date = new Date();
		var yy = date.getFullYear().toString().substring(0, 2);
		var dd = lot.substring(0, 2);
		var mm = lot.substring(2, 4);
		var happy_new_year = dd.toString() +"/" + mm.toString() + "/" + yy + lot.substring(4);		
		if(!Utils.isDateEx(happy_new_year, '/',Utils._DATE_DD_MM_YYYY)){
			$('#' + objectId).focus();
			return 'Số lô không đúng định dạng ddMMyy. Ví dụ: 011012';
		}
		return '';
	},
	getMessageValidateLotNew:function(lot){
		if((lot==null || lot==undefined)|| (lot!=null && lot!=undefined && lot.length!=6)){
			return 'Số lô phải đủ 6 ký tự theo định dạng ddMMyy. Ví dụ: 011012';			
		}
		var msg = Utils.getMessageOfInvaildNumberNew(lot);
		if(msg.length>0){
			return 'Số lô phải đủ 6 ký tự theo định dạng ddMMyy. Ví dụ: 011012';
		}
		var date = new Date();
		var yy = date.getFullYear().toString().substring(0, 2);
		var dd = lot.substring(0, 2);
		var mm = lot.substring(2, 4);
		var happy_new_year = dd.toString() +"/" + mm.toString() + "/" + yy + lot.substring(4);		
		if(!Utils.isDateEx(happy_new_year, '/',Utils._DATE_DD_MM_YYYY)){
			return 'Số lô không đúng định dạng ddMMyy. Ví dụ: 011012';
		}
		return '';
	},
	getMessageOfSpecialCharactersValidateEx: function(objectId, objectName,type, prefix){
		objectName=Utils.language_vi(objectName);
		var input ='';
		if(prefix!= undefined && prefix!= null && prefix.length > 0){
			input = prefix + '#'+ objectId;
		}else{
			input = '#'+ objectId;
		}
		var value =$(input).val().trim();
		if(type == null || type == undefined){
			type = Utils._TF_A_Z;
		}
		var errMsg = '';
		var reg = /[^0-9]/;
		var regAll = /[^0-9]/g;
		if(value.length > 0){
			switch (type) {
			case Utils._TF_A_Z:	
				if(!/^[A-Za-z]+$/.test(value)){
					errMsg = format(msgErr_invalid_format_az,objectName);
				}
				break;
			case Utils._TF_NUMBER_DOT:		
				if(!/^[0-9.]+$/.test(value)){
					errMsg = format(msgErr_invalid_format_num_dot,objectName);
				}
				break;
			case Utils._TF_NUMBER:			
				if(!/^[0-9]+$/.test(value)){
					errMsg = format(msgErr_invalid_format_num,objectName);
				}
				break;
			case Utils._TF_NUMBER_COMMA:			
				if(!/^[0-9,]+$/.test(value)){
					errMsg = format(msgErr_invalid_format_comma,objectName);
				}
				break;
			case Utils._TF_NUMBER_SIGN:			
				if(!/^[0-9-]+$/.test(value)){
					errMsg = format(msgErr_invalid_format_sign,objectName);
				}
				break;
			case Utils._TF_NUMBER_CONVFACT:				 
				if(!/^[0-9\/]+$/.test(value)){
					errMsg = format(msgErr_invalid_format_confact,objectName);
				}else if(value.indexOf('/')!=-1 && value.split('/').length >= 2){
					errMsg = format(msgErr_invalid_format_confact,objectName);
				}
				break;
			case Utils._TF_NUMBER_COMMA_AND_DOT:			
				if(!/^[0-9.,]+$/.test(value)){
					errMsg = format(msgErr_invalid_format_comma_dot,objectName);
				}
				break;
			case Utils._TF_CAR_NUMBER:			
				if(!/^[A-Za-z0-9-]+$/.test(value)){
					errMsg = format(msgErr_invalid_format_car_number,objectName);
				}
				break;
			case Utils._TF_PHONE_NUMBER:
				if(!/^[0-9-.() ]+$/.test(value)){
					errMsg = format(msgErr_invalid_format_num,objectName);
				}
				break;
			default:
				break;
			}
		}
		if(errMsg.length !=0){
			$('#' + objectId).focus();
		}
		return errMsg;
	},
	getMessageOfFloatValidate: function(objectId, objectName){
		objectName=Utils.language_vi(objectName);
		if($('#' + objectId).val().trim() != '' && !/^[0-9]+\.[0-9]+$|^[0-9]+$/.test($('#' + objectId).val().trim())){
			$('#' + objectId).focus();
			return objectName + ' chỉ gồm số và dấu "." ';
		}
		return '';
	},
	getMessageOfMaxlengthValidate: function(objectId, objectName,maxlength){
		objectName=Utils.language_vi(objectName);
		if($('#' + objectId).val().trim() != '' && $('#' + objectId).val().trim().length > maxlength){
			$('#' + objectId).focus();
			return objectName + ' không vượt quá '+maxlength +' ký tự.';
		}
		return '';
	},
	getMessageOfNegativeNumberCheck: function(objectId, objectName,type){
		objectName=Utils.language_vi(objectName);
		if(type == null || type == undefined){
			type = Utils._TF_NUMBER_COMMA_AND_DOT;
		}
		if($('#' + objectId).val().trim() != '' && $('#' + objectId).val().trim().length > 0){
			var errMsg = Utils.getMessageOfSpecialCharactersValidateEx(objectId, objectName,type);
			if(errMsg.length !=0){
				return errMsg;
			}else if($('#' + objectId).val().replace(/,/g,'') <= 0){
				$('#' + objectId).focus();
				return objectName + ' phải lớn hơn 0';
			}
		}
		return '';
	},
	getMessageOfInvalidFormatDate: function(objectId, objectName){
		objectName=Utils.language_vi(objectName);
		if($('#' + objectId).val().trim() == '__/__/____'){
			$('#' + objectId).val('');
			return '';
		}
		if($('#' + objectId).val().trim().length > 0 && !Utils.isDate($('#' + objectId).val().trim(), '/')){
			$('#' + objectId).focus();			
			return format(msgErr_invalid_format_date,objectName);
		}
		return '';
	},
	getMessageOfInvalidFormatDateNew: function(objectId, objectName){
		objectName=Utils.language_vi(objectName);
		if($('#' + objectId).val().trim().length > 0 && !Utils.isDateNew($('#' + objectId).val().trim())){
			$('#' + objectId).focus();			
			return format(msgErr_invalid_format_date_new,objectName);
		}
		return '';
	},
	getMessageOfEmptyDate: function(objectId, objectName){
		objectName=Utils.language_vi(objectName);
		if($('#' + objectId).val().trim().length == 0 ){
			$('#' + objectId).focus();			
			return format(msgErr_empty_date,objectName);
		}
		return '';
	},
	getMessageOfInvalidFormatLot: function(objectId, objectName,maxlength){
		objectName=Utils.language_vi(objectName);
		var lot = $('#' + objectId).val().trim();
		if(lot.length > 0){
			if( lot.length != maxlength) {
				$('#' + objectId).focus();
				return objectName + ' phải bao gồm '+maxlength +' ký tự.';
			} else {
				var date = new Date();
				var yy = date.getFullYear().toString().substring(0, 2);
				var dd = lot.substring(0, 2);
				var mm = lot.substring(2, 4);
				var happy_new_year = dd.toString() +"/" + mm.toString() + "/" + yy + lot.substring(4);		
				if(!Utils.isDateEx(happy_new_year, '/',Utils._DATE_DD_MM_YYYY)){
					$('#' + objectId).focus();
					return objectName +' không đúng định dạng ddmmyy.';
				}	
			}
		}
		return '';
	},
	getMessageOfInvalidFormatDateEx: function(objectId, objectName,type){
		objectName=Utils.language_vi(objectName);
		if($('#' + objectId).val().trim().length > 0 && !Utils.isDateEx($('#' + objectId).val().trim(), '/',type)){
			$('#' + objectId).focus();	
			if(type == null || type == undefined){
				type = Utils._DATE_DD_MM_YYYY;
			}
		    switch (type) {
				case Utils._DATE_DD_MM_YYYY:
					return format(msgErr_invalid_format_date,objectName);
					break;
				case Utils._DATE_MM_YYYY:
					return format(msgErr_invalid_format_month,objectName);
					break;
				default:
					break;
			}
		}
		return '';
	},
	getMessageOfInvalidEmailFormat: function(objectId, objectName){
		objectName=Utils.language_vi(objectName);
		if($('#' + objectId).val().trim() != '' && !/^[a-zA-Z0-9._-]{1,64}@[A-Za-z0-9]{2,64}(\.[A-Za-z0-9]{2,64})*(\.[A-Za-z]{2,4})$/.test($('#' + objectId).val().trim())){
			$('#' + objectId).focus();
			return format(msgErr_invalid_format_email,objectName);			
		}
		return '';
	},
	getMessageOfInvalidMaxLength:function(objectId, objectName){
		objectName=Utils.language_vi(objectName);
		var maxlength = $('#' + objectId).attr('maxlength');
		if(maxlength!= undefined && maxlength!= null && maxlength.length > 0){
			if($('#' + objectId).val().trim().replace(",","").length > maxlength){
				$('#' + objectId).focus();
				return format(msgErr_invalid_maxlength,objectName,maxlength);
			}
		}
		return '';
	},
	getMessageOfInvaildNumber:function(objectId, objectName){
		objectName=Utils.language_vi(objectName);
		var strString = $('#' + objectId).val().trim();
		var strValidChars = "0123456789,.";		
		var blnResult = true;		
		for (i = 0; i < strString.length && blnResult == true; i++)		      {
		      var strChar = strString.charAt(i);
		      if (strValidChars.indexOf(strChar) == -1){
		         blnResult = false;
		       }
		}
		if(!blnResult){
			return objectName + ' chỉ chứa các giá trị số và dấu ,';
		}else{
			return '';
		}		
	},
	getMessageOfInvaildNumberStartWithZero:function(objectId, objectName){
		objectName=Utils.language_vi(objectName);
		var strString = $('#' + objectId).val().trim();
		var strValidChars = "0123456789,.";		
		var blnResult = true;		
		for (i = 0; i < strString.length && blnResult == true; i++)		      {
		      var strChar = strString.charAt(i);
		      if (strValidChars.indexOf(strChar) == -1){
		         blnResult = false;
		       }
		}
		if(!blnResult){
			return objectName + ' chỉ chứa các giá trị số và dấu ,';
		}else{
			if(strString.length != 0 && strString.charAt(0) != '0'){
				return objectName + ' phải bắt đầu là 0';
			}else{
				return '';
			}
		}		
	},
	getMessageOfInvaildNumberNew:function(strString){
		if(strString==undefined){
			strString = '';
		}
		var strValidChars = "0123456789";		
		var blnResult = true;		
		for (i = 0; i < strString.length && blnResult == true; i++)		      {
		      var strChar = strString.charAt(i);
		      if (strValidChars.indexOf(strChar) == -1){
		         blnResult = false;
		       }
		}
		if(!blnResult){
			return 'chỉ chứa các giá trị số';
		}else{
			return '';
		}		
	},
	getMessageOfInvaildNumberEx:function(objectId, objectName, precision,scale ,isPossitive,maxlength, formatType){
		objectName=Utils.language_vi(objectName);
		var value = $('#' + objectId).val().trim();
		switch (formatType) {
		case Utils._TF_NUMBER_DOT:
			reg = /^[0-9.]+$/;
			msg = 'chỉ gồm số và dấu "."';
			break;
		case Utils._TF_NUMBER:
			reg = /^[0-9]+$/;
			msg = 'chỉ gồm số';
			break;
		case Utils._TF_NUMBER_COMMA:
			reg = /^[0-9,]+$/;
			msg = 'chỉ gồm số và dấu ","';
			break;
		case Utils._TF_NUMBER_SIGN:
			reg = /^[0-9-]+$/;
			msg = 'chỉ gồm số và dấu "-"';
			break;
		case Utils._TF_NUMBER_COMMA_AND_DOT:
			reg = /^[0-9.,]+$/;
			msg = 'chỉ gồm số và dấu "." và dấu","';
			break;
		default:
			break;
		}
		if(value != '' && value.length > maxlength){
			$('#' + objectId).focus();
			return objectName + ' không vượt quá '+maxlength +' ký tự.';
		}
		if( value != '' && !reg.test(value)){
			$('#' + objectId).focus();
			return objectName + msg;
		}
		if(value.indexOf(',')>-1){
			value = value.split(',').join('');
		}
		value = Number(value);
		if(isNaN(value)) {
			$('#' + objectId).focus();
			return objectName + ' không phải là số';
		}
		if(isPossitive == true){
			if(Number(value)<=0) {
				$('#' + objectId).focus();
				return objectName + ' phải lớn hơn 0';
			}
		}
		if(value.indexOf('.')>-1){
			var decimal='';
			Num = value.split('.')[0];
			decimal = value.split('.')[1];
		}else{
			Num = value.toString();
		}
		var count = precision-scale;
		if(Number(Num) >= Math.pow(10,count)){
			$('#' + objectId).focus();
			return objectName + 'phải có phần nguyên không được vượt quá '+count +'kí tự';
		}
		return '';
	},

	/**
	 * check change pass
	 * @author vuongmq
	 * @param objectId
	 * @param objectName
	 * @param maxlength
	 * @return String
	 * @since 04/09/2015 
	 */
	getMessageOfPolicyPassWord: function(objectId, objectName, maxlength) {
		var value = $('#' + objectId).val();
		if ((value != '' && value.length < maxlength) || !/([A-Z]+)/.test(value) || !/([a-z]+)/.test(value)) {
			$('#' + objectId).focus();
			return objectName + ' bắt buộc phải có ít nhất 6 ký tự, có chữ hoa và chữ thường';
		}
		return '';
	},

	updateTokenForJSON: function(data){
		$('#token').val(data.token);
		var importTokenVal = $('#importTokenVal').val();
		if(importTokenVal!=null && importTokenVal!=undefined && importTokenVal.trim().length>0){
			$('#importTokenVal').val(data.token);
		}else{
			//Trong phan import chuong trinh khuyen mai truoc gio su dung $('#tokenImport')
			importTokenVal = $('#tokenImport').val();
			if(importTokenVal!=null && importTokenVal!=undefined && importTokenVal.trim().length>0){
				$('#tokenImport').val(data.token);
			}
		}
		
	},
	getToken: function(){
		return $('#token').val();		
	},
	setToken:function(importTokenVal){
		$('#token').val(importTokenVal);
	},
	deleteSelectRowOnGrid: function(dataModel,functionText,url,xhrDel,gridId,errMsgId,callback,loading,prefix){
		var gId = 'grid';
		if(gridId!= undefined && gridId!= null && gridId.length > 0){
			gId = gridId;
		}
		var errMsg = 'errMsg';
		if(errMsgId!= undefined && errMsgId!= null && errMsgId.length > 0){
			errMsg = errMsgId;
		}
		$.messager.confirm(jsp_common_xacnhan,msgCommon2, function(r){ // 'Bạn có muốn xóa'+ functionText +' này?'
			if (r){
				Utils.saveData(dataModel, url, xhrDel, errMsg, function(data){
					$("#"+gId).datagrid("reload");
					if(callback!= null && callback!= undefined){
						callback.call(this,data);
					}
				},loading,prefix,true);
			}else{
				var isCheck = false;
				$('.InputTextStyle').each(function(){
				    if(!$(this).is(':hidden') && !$(this).is(':disabled') && !isCheck){
				        isCheck = true;
				        $(this).focus();
				    }
				});
			}
		});		
		return false;
	},
	deleteMapProductGrid: function(dataModel,functionText,url,xhrDel,gridId,errMsgId,callback,loading,prefix){
		var gId = 'grid';
		if(gridId!= undefined && gridId!= null && gridId.length > 0){
			gId = gridId;
		}
		var errMsg = 'errMsg';
		if(errMsgId!= undefined && errMsgId!= null && errMsgId.length > 0){
			errMsg = errMsgId;
		}
		$.messager.confirm('Xác nhận', 'Bạn có muốn xóa '+ Utils.XSSEncode(functionText) +' ?', function(r){
			if (r){
				Utils.addOrSaveData(dataModel, url, xhrDel, errMsgId, function(data){
					$("#"+ gId).trigger("reloadGrid");
					if(callback!= null && callback!= undefined){
						callback.call(this,data);
					}
				},loading,prefix,true);
			}else{
				var isCheck = false;
				$('.InputTextStyle').each(function(){
				    if(!$(this).is(':hidden') && !$(this).is(':disabled') && !isCheck){
				        isCheck = true;
				        $(this).focus();
				    }
				});
			}
		});		
		return false;
	},
	addOrSaveRowOnGrid: function(dataModel,url,xhrSave,gridId,errMsgId,callback,loading){
		var gId = 'grid';
		if(gridId!= undefined && gridId!= null && gridId.length > 0){
			gId = gridId;
		}
		Utils.addOrSaveData(dataModel, url, xhrSave, errMsgId, function(data){
			$("#"+ gId).datagrid('reload');
			if(callback!= null && callback!= undefined){
				callback.call(this,data);
			}
		},loading);		
		return false;
	},
	/*addOrSaveData: function(dataModel, url, xhrSave, errMsgId, callback,loading,prefix,isDelete,message,callBackFail,hideSuccessMsg){
		if(isDelete == undefined || isDelete == null || isDelete == false){
			var msg;
			if(message == null || message == '' || message == undefined) {
				//msg = 'Bạn có muốn lưu thông tin này?';
				msg = jsp_common_ban_luu_thong_tin_nay;
			} else {
				msg = message;
			}
			var isDialogMsg = $('.panel.window.messager-window').html();
			if(isDialogMsg==undefined || isDialogMsg==null){
				$.messager.confirm(jsp_common_xacnhan, msg, function(r){
					if (r){
						if(dataModel.changeCus != undefined && dataModel.changeCus == 3){
							dataModel.changeCus = 1;
						}
						Utils.saveData(dataModel, url, xhrSave, errMsgId, callback,loading,prefix,isDelete,callBackFail,hideSuccessMsg);
					}else{// lampv-for map product to device
						$('.fancybox-inner #btnCreate').removeAttr("disabled");
						$('.fancybox-inner #btnClose').removeAttr("disabled");
					}
				});
			}
		} else {
			Utils.saveData(dataModel, url, xhrSave, errMsgId, callback,loading,prefix,isDelete,callBackFail,hideSuccessMsg);
		}
		return false;
	},*/
	
	//trungtm6
	addOrSaveData: function(dataModel, url, xhrSave, errMsgId, callback,loading,prefix,isDelete,message,callBackFail, hideSuccessMsg){
		if(isDelete == undefined || isDelete == null || isDelete == false){
			var msg;
			if(message == null || message == '' || message == undefined) {
				msg = jsp_common_ban_luu_thong_tin_nay;
			} else {
				msg = message;
			}
			$.messager.confirm(jsp_common_xacnhan, msg, function(r){
				if (r){
					if(dataModel.changeCus != undefined && dataModel.changeCus == 3){
						dataModel.changeCus = 1;
					}
					if(dataModel.indexRow != undefined && dataModel.indexRow !=null){
						$('#grid').datagrid('endEdit', dataModel.indexRow);
					}
					Utils.saveData(dataModel, url, xhrSave, errMsgId, callback,loading,prefix,isDelete,callBackFail, hideSuccessMsg);
				}else{// lampv-for map product to device
					$('.fancybox-inner #btnCreate').removeAttr("disabled");
					$('.fancybox-inner #btnClose').removeAttr("disabled");
					if(dataModel.indexRow != undefined && dataModel.indexRow !=null){
						$('#grid').datagrid('cancelEdit', dataModel.indexRow);
					}
				}
			});			
		} else {
			$.messager.confirm(jsp_common_xacnhan, jsp_common_xacnhan_xoa, function(r){
				if (r){
					Utils.saveData(dataModel, url, xhrSave, errMsgId, callback,loading,prefix,isDelete,callBackFail, hideSuccessMsg);
				}else{// lampv-for map product to device
					$('.fancybox-inner #btnCreate').removeAttr("disabled");
					$('.fancybox-inner #btnClose').removeAttr("disabled");
				}
			});	
		}
		return false;
	},
	
	saveData: function(dataModel, url, xhrSave, errMsgId, callback,loading,prefix,isDelete,callBackFail,hideSuccessMsg){
		var errMsg = 'errMsg';
		if(errMsgId!= undefined && errMsgId!= null && errMsgId.length > 0){
			errMsg = errMsgId;
		}
		if(prefix!= undefined && prefix!= null && prefix.length > 0){
			$(prefix + ' #'+ errMsg).html('').hide();
		}else{
			$('#'+ errMsg).html('').hide();
		}
		if(prefix!= undefined && prefix!= null && prefix.length > 0){
			$(prefix + '#successMsg').html('').hide();
		}else{
			$('#successMsg').html('').hide();
		}	
		
		if(dataModel == null || dataModel == undefined){
			dataModel = new Object();
		}
		dataModel.token = Utils.getToken();
		var kData = $.param(dataModel, true);
		if(xhrSave!=null){
			xhrSave.abort();
		}
		// showLoading(loading);
		$('#divOverlay').show();
		$('#imgOverlay').show();
		var errType = jsp_common_luu;
		if(isDelete != undefined && isDelete != null && isDelete == true){
			errType = msgText4; // xoa
		}
		xhrSave = $.ajax({
			type : "POST",
			url : url,
			data :(kData),
			dataType : "json",
			success : function(data) {				
				$('#divOverlay').hide();
				$('#imgOverlay').hide();
				// hideLoading(loading);
				Utils.updateTokenForJSON(data);
				if(data.hasMessageListCustomer != undefined && data.hasMessageListCustomer){
					var messageCus = data.hasMessageListCustomer;
					dataModel.changeCus = 3;
					hideSuccessMsg = true;
					Utils.addOrSaveData(dataModel, url, xhrSave, errMsgId, callback,loading,prefix,isDelete,messageCus,callBackFail);
				}
				if(data.error && data.errMsg!= undefined){
					if(prefix!= undefined && prefix!= null && prefix.length > 0){
						$(prefix + ' #'+ errMsg).html(Utils.XSSEncode(data.errMsg)).show();
					}else{
						$('#'+ errMsg).html(escapeSpecialChar(data.errMsg)).show();
					}
					if(callBackFail!=null && callBackFail!=undefined){
						callBackFail.call(this,data);
					}
				} else {					
					if(callback!= null && callback!= undefined){
						callback.call(this,data);
					}
					if(hideSuccessMsg == null || hideSuccessMsg == undefined || hideSuccessMsg == ""){
						if(prefix!= undefined && prefix!= null && prefix.length > 0){
							$(prefix + ' #successMsg').html(errType + ' '+ jsp_common_du_lieu_thanh_cong).show();
						}else{
							$('#successMsg').html(errType + ' '+jsp_common_du_lieu_thanh_cong).show();
						}
						var tm = setTimeout(function(){
							if(prefix!= undefined && prefix!= null && prefix.length > 0){
								$(prefix + '#successMsg').html('').hide();
							}else{
								$('#successMsg').html('').hide();
							}
							clearTimeout(tm);
						}, 3000);
					}
					var isFocus = false;
					$('.GeneralMilkInBox .InputTextStyle').each(function(){
					    if(!$(this).is(':hidden') && !isFocus){
					    	isFocus = true;
					        $(this).focus();
					    }
					});
				}
				xhrSave = null;				
			},
			error:function(XMLHttpRequest, textStatus, errorThrown) {
			}
		});
	},
	getHtmlDataByAjaxNotOverlay: function(params,url,callback,loading,requestType){
		var rt = 'POST';
		var kData = $.param(params, true);
		if(requestType!= null && requestType!= undefined && requestType!=''){
			rt = requestType;
		}
		$.ajax({
			type : rt,
			url : url,
			data :(kData),
			dataType : "html",
			success : function(data) {
				if(callback!= null && callback!= undefined){
					callback.call(this,data);
				}
			},
			error:function(XMLHttpRequest, textStatus, errorThrown) {
			}
		});
		return false;
	},
	getHtmlDataByAjax: function(params,url,callback,loading,requestType){
		var rt = 'POST';
		var kData = $.param(params, true);
		if(requestType!= null && requestType!= undefined && requestType!=''){
			rt = requestType;
		}
		$('#divOverlay').show();
		$.ajax({
			type : rt,
			url : url,
			data :(kData),
			dataType : "html",
			success : function(data) {
				$('#divOverlay').hide();
				if(callback!= null && callback!= undefined){
					callback.call(this,data);
				}
			},
			error:function(XMLHttpRequest, textStatus, errorThrown) {
				$('#divOverlay').hide();
				hideLoading(loading);
			}
		});
		return false;
	},
	getJSONDataByAjax: function(params,url,callback,loading,requestType){
		var rt = 'POST';
		var kData = $.param(params, true);
		if(requestType!= null && requestType!= undefined && requestType!=''){
			rt = requestType;
		}
		$('#divOverlay').addClass('Overlay');
		$('#divOverlay').show();
		$('#imgOverlay').show();
		$.ajax({
			type : rt,
			url : url,
			data :(kData),
			dataType : "json",
			success : function(data) {
				$('#divOverlay').removeClass('Overlay');
				$('#imgOverlay').hide();
				$('#divOverlay').hide();
				if(callback!= null && callback!= undefined){
					callback.call(this,data);
				}
			},
			error:function(XMLHttpRequest, textStatus, errorThrown) {
				$('#divOverlay').removeClass('Overlay');
				$('#divOverlay').hide();
				hideLoading(loading);
			}
		});
		return false;
	},
	getJSONDataByAjaxNotOverlay: function(params,url,callback,loading,requestType){
		var rt = 'POST';
		var kData = $.param(params, true);
		if(requestType!= null && requestType!= undefined && requestType!=''){
			rt = requestType;
		}
		$.ajax({
			type : rt,
			url : url,
			data :(kData),
			dataType : "json",
			success : function(data) {
				if(callback!= null && callback!= undefined){
					callback.call(this,data);
				}
			},
			error:function(XMLHttpRequest, textStatus, errorThrown) {
			}
		});
		return false;
	},
	checkIsJSON:function(object) {
		var stringConstructor = "test".constructor;
		var arrayConstructor = [].constructor;
		var objectConstructor = {}.constructor;
		if (object === null) {
	        return false;
	    }
	    else if (object === undefined) {
	        return false;
	    }
	    else if (object.constructor === stringConstructor) {
	        return false;
	    }
	    else if (object.constructor === arrayConstructor) {
	        return false;
	    }
	    else if (object.constructor === objectConstructor) {
	        return true;
	    }
	    else {
	        return false;
	    }
	},
	formatDoubleValue: function(value){
		if(value == null || value==""){return value;}
		var num = new Number(value);
		var s = num.toFixed(2);
		var index = s.lastIndexOf('.00');
		if(index>-1){
			return num.toFixed(0);
		}else{
			return num.toFixed(2);
		}
	},
	doubleNumbersOnly: function(e) {
		var key;
		var keychar;
		if (window.event) {
		   key = window.event.keyCode;
		}else if (e) {
		   key = e.which;
		}else {
		   return true;
		}
		keychar = String.fromCharCode(key);

		if ((key==null) || (key==0) || (key==8) ||  (key==9) || (key==13) || (key==27) ) {
		   return true;
		}else if ((("0123456789.").indexOf(keychar) > -1)) {
			return true;
		}else{
		   return false;
		}
	},
	compareCurrentDateEx: function(objectId, objectName) {
		var date = $('#' + objectId).val().trim();
		//date < now => true || date > now => false
		if (date.length == 0) {
			return '';
		}
		var now = new Date();
		var cYear = now.getFullYear();
		var cMonth = now.getMonth() + 1;
		var cDate = now.getDate();
		var currentDate = cDate+'/'+cMonth+'/'+cYear;
		if (!Utils.compareDate(date, currentDate)) {
			$('#'+objectId).focus();
			return objectName + ' phải nhỏ hơn hoặc bằng ngày hiện tại. Vui lòng nhập lại';
		}
		return '';
	},
	compareCurrentDateGreaterEx:function(objectId, objectName) {
		var date = $('#' + objectId).val().trim();
		//date < now => true || date > now => false
		if(date.length == 0) {
			return '';
		}
		var now = new Date();
		var cYear = now.getFullYear();
		var cMonth = now.getMonth() + 1;
		var cDate = now.getDate();
		var currentDate = cDate+'/'+cMonth+'/'+cYear;
		if(Utils.compareDate(date, currentDate)) {
			$('#'+objectId).focus();
			return objectName + ' phải lớn hơn ngày hiện tại. Vui lòng nhập lại';
		}
		return '';
	},
	compareCurrentDate:function(date) {
		// date < now => true || date > now => false
		if(date.length == 0) {
			return false;
		}
		var now = new Date();
		var cYear = now.getFullYear();
		var cMonth = now.getMonth() + 1;
		var cDate = now.getDate();
		var currentDate = cDate+'/'+cMonth+'/'+cYear;
		return Utils.compareDate(date, currentDate);
	},
	compareCurrentDate1:function(date) {
		// date < now => true || date > now => false
		if(date.length == 0) {
			return false;
		}
		var now = new Date();
		var cYear = now.getFullYear();
		var cMonth = now.getMonth() + 1;
		var cDate = now.getDate();
		var currentDate = cDate+'/'+cMonth+'/'+cYear;
		return Utils.compareDate1(date, currentDate);
	},
	compareCurrentDateExReturn:function(startDate,endDate) {		
		if(startDate.length == 0 || endDate.length == 0){
			return true;			
		}
		var arrStartDate = startDate.split('/');
		var arrEndDate = endDate.split('/');
		var startDateObj = dates.convert(arrStartDate[1] + '/' + arrStartDate[0] + '/' + arrStartDate[2]);
		var endDateObj = dates.convert(arrEndDate[1] + '/' + arrEndDate[0] + '/' + arrEndDate[2]);
		return dates.compare(startDateObj,endDateObj);
	},
	compareDate: function(startDate, endDate){
		if(startDate.length == 0 || endDate.length == 0){
			return true;			
		}
		var arrStartDate = startDate.split('/');
		var arrEndDate = endDate.split('/');
		var startDateObj = dates.convert(arrStartDate[1] + '/' + arrStartDate[0] + '/' + arrStartDate[2]);
		var endDateObj = dates.convert(arrEndDate[1] + '/' + arrEndDate[0] + '/' + arrEndDate[2]);
		if(dates.compare(startDateObj,endDateObj) > 0){
			return false;
		}
		return true;
	},
	compareDateInt: function(startDate, endDate){
		if(startDate.length == 0 || endDate.length == 0){
			return true;			
		}
		var arrStartDate = startDate.split('/');
		var arrEndDate = endDate.split('/');
		var startDateObj = dates.convert(arrStartDate[1] + '/' + arrStartDate[0] + '/' + arrStartDate[2]);
		var endDateObj = dates.convert(arrEndDate[1] + '/' + arrEndDate[0] + '/' + arrEndDate[2]);
		return dates.compare(startDateObj,endDateObj);
	},
	compareDateNew: function(startDate, endDate){
		if(startDate.length == 0 || endDate.length == 0){
			return true;			
		}
		var __arrStartDate = startDate.split(' ');
		var __arrEndDate = endDate.split(' ');
		var startDateObj = new Date(__arrStartDate[0].split('/')[2], __arrStartDate[0].split('/')[1], __arrStartDate[0].split('/')[0], __arrStartDate[1].split(':')[0], __arrStartDate[1].split(':')[1]);
		var endDateObj = new Date(__arrEndDate[0].split('/')[2], __arrEndDate[0].split('/')[1], __arrEndDate[0].split('/')[0], __arrEndDate[1].split(':')[0], __arrEndDate[1].split(':')[1]);
		if(dates.compare(startDateObj,endDateObj) > 0){
			return false;
		}
		return true;
	},
	compareDate1: function(startDate, endDate){
		if(startDate.length == 0 || endDate.length == 0){
			return true;			
		}
		var arrStartDate = startDate.split('/');
		var arrEndDate = endDate.split('/');
		var startDateObj = dates.convert(arrStartDate[1] + '/' + arrStartDate[0] + '/' + arrStartDate[2]);
		var endDateObj = dates.convert(arrEndDate[1] + '/' + arrEndDate[0] + '/' + arrEndDate[2]);
		if(dates.compare(startDateObj,endDateObj) >= 0){
			return false;
		}
		return true;
	},
	compareMonth: function(startMonth, endMonth){
		if(startMonth.length == 0 || endMonth.length == 0){
			return true;			
		}
		var arrStartMonth = startMonth.split('/');
		var arrEndMonth = endMonth.split('/');
		var startDateObj = dates.convert(arrStartMonth[0] + '/' + '01' + '/' + arrStartMonth[1]);
		var endDateObj = dates.convert(arrEndMonth[0] + '/' + arrEndMonth[0] + '/' + arrEndMonth[1]);
		if(dates.compare(startDateObj,endDateObj) > 0){
			return false;
		}
		return true;
	},
	// Don't delete!!
	compareCurrentMonthEx: function(monthTxt) {//neu monthTxt >= thang hien tai => true. ko thi false
		var currentDateTxt = toDateString(new Date());
		var arrCurrentDate = currentDateTxt.split('/');
		var arrMonthTxt = monthTxt.split('/');
		if((Number(arrMonthTxt[1]) > Number(arrCurrentDate[2])) || (Number(arrMonthTxt[1]) == Number(arrCurrentDate[2]) && arrMonthTxt[0]>=arrCurrentDate[1])) {
			return true;
		}
		return false;
	},
	/**
	 * So sanh 2 thang 
	 * input : String dd/mm/yyyy
	 * output:
	 * -2 : khong hop le
	 * -1 : thang a truoc thang b (a < b)
	 * 0 : cung thang (a = b)
	 * 1 : thang a sau thang b (a > b)
	 * 
	 */
	compareMonthString: function(startMonth, endMonth){ 
		if(startMonth.length == 0 || endMonth.length == 0){
			return -2;		
		}
		var arrStartMonth = startMonth.split('/');
		var arrEndMonth = endMonth.split('/');
		
		if(parseInt(arrStartMonth[2], 10) == parseInt(arrEndMonth[2], 10)){
			if(parseInt(arrStartMonth[1], 10) == parseInt(arrEndMonth[1], 10)){
				return 0; 
			}else if(parseInt(arrStartMonth[1], 10) < parseInt(arrEndMonth[1], 10)){
				return -1;
			}else{
				return 1;
			}
		}else if(parseInt(arrStartMonth[2], 10) < parseInt(arrEndMonth[2], 10)){
			return -1; 
		}else{
			return 1; 
		}
	},
	isDate:function(txtDate, separator) {
	    var aoDate,           // needed for creating array and object
	        ms,               // date in milliseconds
	        month, day, year; // (integer) month, day and year
	    // if separator is not defined then set '/'
	    if (separator == undefined) {
	        separator = '/';
	    }
	    // split input date to month, day and year
	    aoDate = txtDate.split(separator);
	    // array length should be exactly 3 (no more no less)
	    if (aoDate.length !== 3) {
	        return false;
	    }
	    // define month, day and year from array (expected format is m/d/yyyy)
	    // subtraction will cast variables to integer implicitly
	    day = aoDate[0] - 0; // because months in JS start from 0
	    month = aoDate[1] - 1;
	    year = aoDate[2] - 0;
	    // test year range
	    if (year < 1000 || year > 3000) {
	        return false;
	    }
	    // convert input date to milliseconds
	    ms = (new Date(year, month, day)).getTime();
	    // initialize Date() object from milliseconds (reuse aoDate variable)
	    aoDate = new Date();
	    aoDate.setTime(ms);
	    // compare input date and parts from Date() object
	    // if difference exists then input date is not valid
	    if (aoDate.getFullYear() !== year ||
	        aoDate.getMonth() !== month ||
	        aoDate.getDate() !== day) {
	        return false;
	    }
	    // date is OK, return true
	    return true;
	},
	isDateNew:function(txtDate) {
	    var __str = txtDate.split(' ');
	    if($.isArray(__str) && __str.length == 2) {
	    	if(!Utils.isDate(__str[0])) {
	    		return false;
	    	}
	    	var __ex = __str[1].split(':');
	    	if(!(0 <= Number(__ex[0]) && Number(__ex[0]) <= 24)) {
	    		return false;
	    	}
	    	if(!(0 <= Number(__ex[1]) && Number(__ex[1]) <= 59)) {
	    		return false;
	    	}
	    	return true;
	    } else {
	    	return false;
	    }
	},
	getDataComboboxCustomer:function(objectId,prefix){
		var data = new Array();		
		var parent = '';
		if(prefix!=undefined && prefix!=null){
			parent = prefix;
		}
		$(parent + '#' + objectId +' option').each(function(){
			var obj = new Object();
			obj.shortCode = $(this).val().trim();
			obj.customerName = $(this).text().trim();
			obj.displayText = $(this).val().trim() + ' - ' + $(this).text().trim(); 
			obj.searchText = unicodeToEnglish($(this).val().trim()+ $(this).text().trim());
			obj.searchText = obj.searchText.toUpperCase();
			data.push(obj);
		});
		return data;
	},
	getDataComboboxStaff:function(objectId,prefix){
		var data = new Array();		
		var parent = '';
		if(prefix!=undefined && prefix!=null){
			parent = prefix;
		}
		$(parent + '#' + objectId +' option').each(function(){
			var obj = new Object();
			obj.staffCode = $(this).val().trim();
			obj.staffName = $(this).text().trim();
			obj.displayText = $(this).val().trim() + ' - ' + $(this).text().trim(); 
			obj.searchText = unicodeToEnglish($(this).val().trim()+ $(this).text().trim());
			obj.searchText = obj.searchText.toUpperCase();
			data.push(obj);
		});
		return data;
	},
	getDataComboboxCar:function(objectId,prefix){
		var data = new Array();		
		var parent = '';
		if(prefix!=undefined && prefix!=null){
			parent = prefix;
		}
		$(parent + '#' + objectId +' option').each(function(){
			var obj = new Object();
			obj.id = $(this).val().trim();
			obj.carNumber = $(this).text().trim();
			obj.displayText = $(this).text().trim(); 
			obj.searchText = unicodeToEnglish($(this).text().trim());
			obj.searchText = obj.searchText.toUpperCase();
			data.push(obj);
		});
		return data;
	},
	bindComboboxCarEasyUI:function(objectId,prefix){
		var parent = '';
		if(prefix!=undefined && prefix!=null){
			parent = prefix;
		}			
		$(parent + '#'+objectId).combobox({			
			valueField : 'id',
			textField : 'carNumber',
			data : Utils.getDataComboboxCar(objectId,prefix),
			//width:250,
			panelWidth:206,
			formatter: function(row) {
				return '<span style="font-weight:bold">' + Utils.XSSEncode(row.carNumber) + '</span>';
			},
			filter: function(q, row){
				q = new String(q).toUpperCase().trim();
				var opts = $(this).combobox('options');
				return row['searchText'].indexOf(unicodeToEnglish(q))>=0;
			},			
			onChange:function(newvalue,oldvalue){		    	
			}
		});
		$('.combo-arrow').unbind('click');
	},
	bindComboboxCustomerEasyUI:function(objectId,prefix){
		var parent = '';
		if(prefix!=undefined && prefix!=null){
			parent = prefix;
		}			
		$(parent + '#'+objectId).combobox({			
			valueField : 'shortCode',
			textField : 'customerName',
			data : Utils.getDataComboboxCustomer(objectId,prefix),
			//width:250,
			panelWidth:206,
			formatter: function(row) {
				return '<span style="font-weight:bold">' + Utils.XSSEncode(row.shortCode) + '</span><br/>' + '<span style="color:#888">' + Utils.XSSEncode(row.customerName) + '</span>';
			},
			filter: function(q, row){
				q = new String(q).toUpperCase();
				var opts = $(this).combobox('options');
				return row['searchText'].indexOf(unicodeToEnglish(q))>=0;
			},			
			onChange:function(newvalue,oldvalue){		    	
			}
		});
		$('.combo-arrow').unbind('click');
	},
	bindComboboxStaffEasyUI:function(objectId,prefix){
		var parent = '';
		if(prefix!=undefined && prefix!=null){
			parent = prefix;
		}			
		$(parent + '#'+objectId).combobox({			
			valueField : 'staffCode',
			textField : 'staffCode',
			data : Utils.getDataComboboxStaff(objectId,prefix),
			//width:250,
			panelWidth:206,
			formatter: function(row) {
				return '<span style="font-weight:bold">' + Utils.XSSEncode(row.staffCode) + '</span><br/>' + '<span style="color:#888">' + Utils.XSSEncode(row.staffName) + '</span>';
			},
			filter: function(q, row){
				q = new String(q).toUpperCase().trim();
				var opts = $(this).combobox('options');
				return row['searchText'].indexOf(unicodeToEnglish(q))>=0;
			},			
			onChange:function(newvalue,oldvalue){		    	
			}
		});
		$('.combo-arrow').unbind('click');
	},
	bindComboboxEquipGroupEasyUI:function(objectId, prefix){
		var parent = '';
		if (prefix != undefined && prefix != null) {
			parent = prefix;
		}			
		$(parent + '#'+objectId).combobox({			
			valueField : 'code',
			textField : 'name',
			data : Utils.getDataComboboxStaff(objectId, prefix),
			panelWidth:206,
			formatter: function(row) {
				return '<span style="font-weight:bold">' + VTUtilJS.XSSEncode(row.name) + '</span><br/>' + '<span style="color:#888">' + VTUtilJS.XSSEncode(row.code) + '</span>';
			},
			filter: function(q, row){
				q = new String(q).toUpperCase().trim();
				var opts = $(this).combobox('options');
				return row['searchText'].indexOf(unicodeToEnglish(q))>=0;
			},			
			onChange:function(newvalue, oldvalue){		    	
			}
		});
	},
	bindComboboxEquipGroupEasyUICodeName:function(objectId,prefix, width){
		var parent = '';
		if(prefix!=undefined && prefix!=null){
			parent = prefix;
		}			
		if (width == undefined) {
			width = 206;
		}
		$(parent + '#'+objectId).combobox({			
			valueField : 'id',
			// textField : 'code' + '-' + 'name',
			textField : 'codeName',
			data : Utils.getDataComboboxStaff(objectId,prefix),
			//width:250,
			panelWidth:width,
			formatter: function(row) {
				// return '<span style="font-weight:bold">' + row.code + '</span><br/>' + '<span style="color:#888">' + row.name + '</span>';
				return '<span style="font-weight:bold">' + VTUtilJS.XSSEncode(row.code) + '</span><br/>' + '<span style="color:#888">' + VTUtilJS.XSSEncode(row.name) + '</span>';
			},
			filter: function(q, row){
				q = new String(q).toUpperCase().trim();
				var opts = $(this).combobox('options');
				return row['searchText'].indexOf(unicodeToEnglish(q))>=0;
			},			
			onChange:function(newvalue,oldvalue){		    	
				//$('.combo-panel').each(function(){if(!$(this).is(':hidden')){$(this).parent().css('width','250px');$(this).css('width','248px');}});
			}
		});
		// $('.combo-arrow').unbind('click');
		//$('.combo-arrow').live('click',function(){$('.combo-panel').each(function(){if(!$(this).is(':hidden')){$(this).parent().css('width','250px');$(this).css('width','248px');}});});
	},
	/**
	 * trungtm6
	 * date: 24/04/2015
	 * bind easy ui combobox
	 * selector: selector
	 * dataArr: lst staff
	 */
	bindStaffCbx: function(selector, dataArr, idDefault, cbxWidth, selectCall){	
		var wid = 'auto';
		if (cbxWidth != undefined && cbxWidth != null && cbxWidth > 0){
			wid = cbxWidth;
		}
		$(selector).combobox({
		    data:dataArr,
		    valueField:'staffCode',
		    textField:'staffName',
		    panelWidth: wid,
		    formatter: function(row) {
				return '<span style="font-weight:bold">' + Utils.XSSEncode(row.staffCode) + '</span><br/>' + '<span style="color:#888">' + Utils.XSSEncode(row.staffName) + '</span>';
			},
			filter: function(q, row) {
				var opts = $(this).combobox('options');
                var x = unicodeToEnglish(row[opts.textField]).indexOf(unicodeToEnglish(q));
                var y = unicodeToEnglish(row[opts.valueField]).indexOf(unicodeToEnglish(q))
				return (x >= 0 | y >= 0) ;
			},
			onSelect: function(rec){
	        	if (selectCall != undefined && selectCall != null){
	        		selectCall.call(this, rec);
	        	}
	        },
			onLoadSuccess:function(){
				if (idDefault != undefined && idDefault != null) {
					$(selector).combobox('setValue',idDefault);
				}
			}
		});
	},
	/**
	 * trietptm
	 * date: 12/08/2015
	 * bind easy ui combobox set valueField: id
	 * selector: selector
	 * dataArr: lst staff
	 */
	bindStaffCbxId: function(selector, dataArr, idDefault, cbxWidth, selectCall){	
		var wid = 'auto';
		if (cbxWidth != undefined && cbxWidth != null && cbxWidth > 0){
			wid = cbxWidth;
		}
		$(selector).combobox({
		    data:dataArr,
		    valueField:'id',
		    textField:'staffName',
		    panelWidth: wid,
		    formatter: function(row) {
				return '<span style="font-weight:bold">' + Utils.XSSEncode(row.staffCode) + '</span><br/>' + '<span style="color:#888">' + Utils.XSSEncode(row.staffName) + '</span>';
			},
			filter: function(q, row) {
				var opts = $(this).combobox('options');
                var x = unicodeToEnglish(row[opts.textField]).indexOf(unicodeToEnglish(q));
                var y = unicodeToEnglish(row[opts.valueField]).indexOf(unicodeToEnglish(q))
				return (x >= 0 | y >= 0) ;
			},
			onSelect: function(rec){
	        	if (selectCall != undefined && selectCall != null){
	        		selectCall.call(this, rec);
	        	}
	        },
			onLoadSuccess:function(){
				if (idDefault != undefined && idDefault != null) {
					$(selector).combobox('setValue',idDefault);
				}
			}
		});
	},
	/**
	 * @author: trungtm6
	 */
	bindCarCbx: function(selector, dataArr, idDefault, cbxWidth, selectCall){	
		var wid = 'auto';
		if (cbxWidth != undefined && cbxWidth != null && cbxWidth > 0){
			wid = cbxWidth;
		}
		$(selector).combobox({
		    data:dataArr,
		    valueField:'id',
		    textField:'carNumber',
		    panelWidth: wid,
		    formatter: function(row) {
				return '<span style="font-weight:bold">' + Utils.XSSEncode(row.carNumber) + '</span>';
			},
			filter: function(q, row) {
//				q = new String(q).toUpperCase().trim();
				var opts = $(this).combobox('options');
				return ((unicodeToEnglish(row[opts.textField]).indexOf(unicodeToEnglish(q)) >= 0) || unicodeToEnglish(row[opts.valueField]).indexOf(unicodeToEnglish(q)) >= 0) ;
			},
			onSelect: function(rec){
	        	if (selectCall != undefined && selectCall != null){
	        		selectCall.call(this, rec);
	        	}
	        },
			onLoadSuccess:function(){
				if (idDefault != undefined && idDefault != null) {
					$(selector).combobox('setValue',idDefault);
				}
			}
		});
	},
	
	/**
	 * @author: trungtm6
	 */
	bindPeriodCbx: function(selector, dataArr, idDefault, cbxWidth, selectCall, loadSuccessCall, value, text) {
		var nameText = 'num';
		var wid = 'auto';
		var valueField = 'num';
		var textField = 'cycleName';
		if (value != undefined && value != null) {
			valueField = value;
		}
		if (text != undefined && text != null) {
			textField = text;
		}
		if (cbxWidth != undefined && cbxWidth != null && cbxWidth > 0) {
			wid = cbxWidth;
		}
		$(selector).combobox({
		    data:dataArr,
		    valueField: valueField,
		    textField: textField,
		    panelWidth: wid,
		    formatter: function(row) {
		    	if (text != undefined && text != null && text == nameText) {
		    		return '<span style="font-weight:bold">' + Utils.XSSEncode(row.num) + '</span>';
		    	}
				return '<span style="font-weight:bold">' + Utils.XSSEncode(row.cycleName) + '</span>';
			},
			filter: function(q, row) {
				var opts = $(this).combobox('options');
				return ((unicodeToEnglish(row[opts.textField]).indexOf(unicodeToEnglish(q)) >= 0) || unicodeToEnglish(row[opts.valueField]).indexOf(unicodeToEnglish(q)) >= 0) ;
			},
			onSelect: function(rec){
	        	if (selectCall != undefined && selectCall != null){
	        		selectCall.call(this, rec);
	        	}
	        },
			onLoadSuccess:function() {
				if (idDefault != undefined && idDefault != null) {
					$(selector).combobox('setValue',idDefault);
				}
				if (loadSuccessCall != undefined && loadSuccessCall != null) {
					loadSuccessCall.call(this);
	        	}
			}
		});
	},
	/**
	 *  khoi tao combobox don vi
	 */
	bindShopCbx: function(selector, dataArr, shopCodeDefault, cbxWidth, selectCall, successCall, valueField, textField) {
		var value = 'shopCode';
		var text = 'shopName';
		var wid = 206;
		if (cbxWidth != undefined && cbxWidth != null && cbxWidth > 0) {
			wid = cbxWidth;
		}
		if (valueField != undefined && valueField != null) {
			value = valueField;
		}
		if (textField != undefined && textField != null) {
			text = textField;
		}
		$(selector).combobox({
			valueField: value,
			textField:  text,
			data: dataArr,
			panelWidth: wid,
			formatter: function(row) {
				return '<span style="font-weight:bold">' + Utils.XSSEncode(row.shopCode) + '</span><br/>' + '<span style="color:#888">' + Utils.XSSEncode(row.shopName) + '</span>';
			},
			filter: function(q, row) {
				q = new String(q).toUpperCase().trim();
				var opts = $(this).combobox('options');
				return ((unicodeToEnglish(row[opts.textField]).indexOf(unicodeToEnglish(q)) == 0) || unicodeToEnglish(row[opts.valueField]).indexOf(unicodeToEnglish(q)) == 0) ;
			},	
	        onSelect: function(rec) {
	        	if (selectCall != undefined && selectCall != null) {
	        		selectCall.call(this, rec);
	        	}
	        },
	        onLoadSuccess: function() {
	        	var arr = $(selector).combobox('getData');
	        	if (arr != null && arr.length > 0) {			        		
	        		if (shopCodeDefault != undefined && shopCodeDefault != null && shopCodeDefault.length > 0) {
        				$(selector).combobox('select', shopCodeDefault);			        				
        			} else if (value == 'shopCode'){
        				$(selector).combobox('select', arr[0].shopCode);
        			} else if (value == 'id'){
        				$(selector).combobox('select', arr[0].id);
        			}
	        	}
	        	if (successCall != undefined && successCall != null) {
	        		successCall.call(this, arr);
				}
	        }
		});
	},
	/**
	 *  load combobox don vi co phan quyen
	 * @author: trietptm
	 */
	loadShopCbx: function(selector, shopCodeDefault, cbxWidth, selectCall, successCall, valueField, textField){
		var params = {};
		$.ajax({
			type : "POST",
			url:'/commons/list-child-shop',
			data :($.param(params, true)),
			dataType : "json",
			success : function(data) {
				Utils.bindShopCbx(selector, data.rows, shopCodeDefault, cbxWidth, selectCall, successCall, valueField, textField);
			}
		});	
	},
	isDateEx:function(txtDate, separator, type) {
	    var aoDate,           // needed for creating array and object
	        ms,               // date in milliseconds
	        isDate,			  // checkDate
	        month, day, year; // (integer) month, day and year
	    	
	    // if separator is not defined then set '/'
	    if (separator == undefined) {
	        separator = '/';
	    }
	    // split input date to month, day and year
	    aoDate = txtDate.split(separator);
	    // array length should be exactly 3 (no more no less)
	   
	    if(type == null || type == undefined){
			type = Utils._DATE_DD_MM_YYYY;
		}
	    switch (type) {
			case Utils._DATE_DD_MM_YYYY:
				if (aoDate.length !== 3) {
					return false;
				}
				 // define month, day and year from array (expected format is
					// m/d/yyyy)
			    // subtraction will cast variables to integer implicitly
			    day = aoDate[0] - 0; // because months in JS start from 0
			    month = aoDate[1] - 1;
			    year = aoDate[2] - 0;
				break;
			case Utils._DATE_MM_YYYY:
				if (aoDate.length !== 2) {
					return false;
				}
				day = 1;
			    month = aoDate[0]-1;
			    year = aoDate[1]-0;
				break;
			default:
				break;
		}
	   
	    // test year range
	    if (year < 1000 || year > 3000) {
	        return false;
	    }
	    // convert input date to milliseconds
	    ms = (new Date(year, month, day)).getTime();
	    // initialize Date() object from milliseconds (reuse aoDate variable)
	    aoDate = new Date();
	    aoDate.setTime(ms);
	    // compare input date and parts from Date() object
	    // if difference exists then input date is not valid
	    if (aoDate.getFullYear() !== year ||
	        aoDate.getMonth() !== month ||
	        aoDate.getDate() !== day) {
	        return false;
	    }
	    // date is OK, return true
	    return true;
	},
	numbersOnly: function(e) {
		var key;
		var keychar;
		if (window.event) {
		   key = window.event.keyCode;
		}else if (e) {
		   key = e.which;
		}else {
		   return true;
		}
		keychar = String.fromCharCode(key);

		if ((key==null) || (key==0) || (key==8) ||  (key==9) || (key==13) || (key==27) ) {
		   return true;
		}else if ((("0123456789").indexOf(keychar) > -1)) {
			return true;
		}else{
		   return false;
		}
	},
	unitNumberOnly: function(e){
		var key;
		var keychar;
		if (window.event) {
		   key = window.event.keyCode;
		}else if (e) {
		   key = e.which;
		}else {
		   return true;
		}
		keychar = String.fromCharCode(key);

		if ((key==null) || (key==0) || (key==8) ||  (key==9) || (key==13) || (key==27) ) {
		   return true;
		}else if ((("0123456789/").indexOf(keychar) > -1)) {
			return true;
		}else{
		   return false;
		}
	},
	// CuongND
	ChangeSelectByValueOrText : function(ddlID, value,Text) {
	    var ddl = document.getElementById(ddlID);
	    for (var i = 0; i < ddl.options.length; i++) {
	    	if (Text=="value"){
	    		if (ddl.options[i].value == value) {
		                ddl.selectedIndex = i;
		            break;
		        }
	    	}else if (Text=="text"){
	    		if (ddl.options[i].text == value) {
		                ddl.selectedIndex = i;
		                break;
		        }
	    	}
	    }
	},
	unitNotKEY35:function(e){
		var key;
		var keychar;
		if (window.event) {
		   key = window.event.keyCode;
		}else if (e) {
		   key = e.which;
		}else {
		   return true;
		}
		keychar = String.fromCharCode(key);

		if ((key==null) || (key==0) || (key==8) ||  (key==9) || (key==13) || (key==27) ) {
		   return true;
		}else if(key==35){
			return false;
		}
		return true;
	},
	unitCharactersAz094Code:function(e){
		var key;
		var keychar;
		if (window.event) {
		   key = window.event.keyCode;
		}else if (e) {
		   key = e.which;
		}else {
		   return true;
		}
		keychar = String.fromCharCode(key);
		if ((key==null) || (key==0) || (key==8) ||  (key==9) || (key==13) || (key==27) ) {
			   return true;
		}		
	    if(!/^[0-9a-zA-Z-_.]+$/.test(keychar)){
			return false;
	   }
	    return true;
	},
	unitCharactersAz094Name:function(e){
		var key;
		var keychar;
		if (window.event) {
		   key = window.event.keyCode;
		}else if (e) {
		   key = e.which;
		}else {
		   return true;
		}
		
		keychar = String.fromCharCode(key);
		if ((key==null) || (key==0) || (key==8) ||  (key==9) || (key==13) || (key==27) ) {
			   return true;
		}		
		if(/^[\\|\/|~]/.test(keychar)){
			  return false;
		} 
		return true;
	},
	unitNumberOnly4Paste: function()
	{
		var ctrlDown = false;
	    var ctrlKey = 17, vKey = 86;
	    
	    $(this).keydown(function(e)
	    {
	        if (e.keyCode == ctrlKey) ctrlDown = true;
	    }).keyup(function(e)
	   	{
	        if (e.keyCode == ctrlKey) ctrlDown = false;
	    });
	    
	    $(this).keydown(function(e)
   	     {
   	        if (ctrlDown)
   	        {
   	        	if (e.keyCode == vKey) return true;
   	        	else return false;
   	        }else
   	        {
				return Utils.numberOnly(e);  	        	
   	        }
   	     });
	},
	integerNumberOnly: function(e){
		var key;
		var keychar;
		if (window.event) {
		   key = window.event.keyCode;
		}else if (e) {
		   key = e.which;
		}else {
		   return true;
		}
		keychar = String.fromCharCode(key);

		if ((key==null) || (key==0) || (key==8) ||  (key==9) || (key==13) || (key==27) ) {
		   return true;
		}else if ((("0123456789-").indexOf(keychar) > -1)) {
			return true;
		}else{
		   return false;
		}
	},
	getStatusValue : function(status){
		if(status == 'DELETED'){
			return -1;
		} else if(status == 'STOPPED'){
			return 0;
		}
		return 1;
	},
	bindAutoCombineKey:function(){
		$('.GeneralMilkInBox .InputTextStyle ').each(function(){
	   		$(this).bind('keyup',function(event){
	   			if(event.ctrlKey && ($.inArray(event.keyCode,keyCombine) > -1)){
	   				$('.LabelStyle u').each(function(){
						if($(this).html() == String.fromCharCode(event.keyCode)){
							var div = $(this).parent().next().attr("id");
							if(div == undefined){
								div = $(this).parent().next().children().attr("id");
							}
							$('#'+div).focus();
							return false;
						}
					});
	   			}
	   		});    
		});
	},
	bindAutoSearch: function(){
		var check = false;
		if (!check){
			$('.easyui-window .InputTextStyle').each(function(){
			    if(!$(this).is(':hidden')){
			    	check=true;			    			    	
			   		$(this).bind('keyup',function(event){
			   			if(event.keyCode == keyCodes.ENTER){		   				
			   				$('.easyui-window .BtnSearchOnDialog').each(function(){	   					
			   					if(!$(this).is(':hidden')){
			   						$(this).click();
			   					}
			   				});
			   			}
			   		});    
			    }	    
			});
		}
		if (!check){
			$('.easyui-dialog .panel .InputTextStyle').each(function(){
			    if(!$(this).is(':hidden')){
			    	check=true;
			   		$(this).bind('keyup',function(event){
			   			if(event.keyCode == keyCodes.ENTER){		   				
			   				$('.easyui-dialog .BtnSearchOnDialog').each(function(){	   					
			   					if(!$(this).is(':hidden')){
			   						$(this).click();
			   					}
			   				});
			   			}
			   		});    
			    }	    
			});
		}
		if (!check){
			$('.ContentSection .InputTextStyle,.SearchSection .InputTextStyle').each(function(){
			    if(!$(this).is(':hidden')){
			   		$(this).bind('keyup',function(event){
			   			if(event.keyCode == keyCodes.ENTER){		   				
			   				if($('#btnSearch')!= null && $('#btnSearch').html()!= null && $('#btnSearch').html().trim().length > 0 && !$('#btnSearch').is(':hidden')){
			   					$('#btnSearch').click();
			   				}
			   				$('.BtnSearch').each(function(){	   					
			   					if(!$(this).is(':hidden')){
			   						$(this).click();
			   					}
			   				});
			   			}
			   		});    
			    }	    
			});
		}		
	},
	bindAutoSearchEx: function(parentDiv){
		$(parentDiv+' .InputTextStyle ').each(function(){
		    if(!$(this).is(':hidden')){
		   		$(this).bind('keyup',function(event){
		   			if(event.keyCode == keyCodes.ENTER){		   				
		   				if($('#btnSearch')!= null && $('#btnSearch').html()!= null && $('#btnSearch').html().trim().length > 0 && !$('#btnSearch').is(':hidden')){
		   					$('#btnSearch').click();
		   				}
		   				$('.BtnSearch').each(function(){	   					
		   					if(!$(this).is(':hidden')){
		   						$(this).click();
		   					}
		   				});
		   			}
		   		});    
		    }	    
		});	
	},	
	bindAutoSearchUpdateBoxSelect: function(){
		$('.BoxSelect2, .BoxSelect3').each(function(){
			if(!$(this).is(':hidden') && !$(this).is(':disabled')){
				$(this).bind('keyup',function(event){
					if(event.keyCode == keyCodes.ENTER){
						if($('#btnUpdate').length > 0 && !$('#btnUpdate').is(':hidden') && !$('btnUpdate').is(':disabled')){
							$('#btnUpdate').click();
						}else{
							if($('#btnSearch').length > 0 && !$('#btnSearch').is(':hidden') && !$('btnSearch').is(':disabled')){
								$('#btnSearch').click();
							}
						}
					}
				});
			}
		});
	},
	bindAutoButtonEx: function(parentDiv, idButton){
		var flag = false;
		$(parentDiv+' .InputTextStyle ').each(function(){
			if(!flag){
				if(!$(this).is(':hidden')){
					$(this).bind('keyup',function(event){
						if(event.keyCode == keyCodes.ENTER){		   				
							if($('#' +idButton)!= null && $('#' +idButton).html()!= null && $('#' +idButton).html().trim().length > 0 && !$('#' +idButton).is(':hidden')){
								$('#' +idButton).click();
								flag = true;
							}
						}
					});    
				}	    
			}
		});	
	},	
	bindQuicklyAutoSearch:function(){
		$('.ContentSection .InputTextStyle ').each(function(){
		    if(!$(this).is(':hidden')){
		   		$(this).bind('keyup',function(event){
		   			if(event.keyCode == keyCodes.ENTER){		   				
		   				if($('#btnSearch')!= null && $('#btnSearch').html()!= null && $('#btnSearch').html().trim().length > 0 && !$('#btnSearch').is(':hidden')){
		   					$('#btnSearch').click();		   					
		   				}		   				
		   			}
		   		});    
		    }	    
		});
	},	
	bindAutoSave: function(){
		$('.GeneralMilkInBox .InputTextStyle ').each(function(){
		    if(!$(this).is(':hidden')){
		   		$(this).bind('keyup',function(event){
		   			if(event.keyCode == keyCodes.ENTER){		   				
		   				if($('#btnSave')!= null && $('#btnSave').html()!= null && $('#btnSave').html().trim().length > 0 && !$('#btnSave').is(':hidden')){
		   					$('#btnSave').click();
		   				}
		   			}
		   		});    
		    }	    
		});
	},
	bindAutoSearchForEasyUI: function(){
		$('.easyui-window .GeneralMilkInBox .InputTextStyle ').each(function(){
		    if(!$(this).is(':hidden')){
		   		$(this).bind('keyup',function(event){
		   			if(event.keyCode == keyCodes.ENTER){		   				
		   				$('.easyui-window .BtnSearchOnDialog').each(function(){	   					
		   					if(!$(this).is(':hidden')){
		   						$(this).click();
		   					}
		   				});
		   			}
		   		});    
		    }	    
		});
	},
	bindAutoSearchPage: function(idDialogTag){
		$(document).bind('keyup',function(event){
			if(event.keyCode == keyCodes.ENTER){		   	
				if($('#' + idDialogTag) == undefined ||($('#' + idDialogTag) != null && $('#' + idDialogTag).is(':hidden'))){
					if($('#btnSearch')!= null && $('#btnSearch').html()!= null && $('#btnSearch').html().trim().length > 0 && !$('#btnSearch').is(':hidden')){
						$('#btnSearch').click();
					}
				}							
			}
		});	
	},
	bindingBanCharacter: function(){
		$('.InputTextStyle').each(function(){
			$(this).bind('keypress', function(e){
				var key;
				var keychar;
				if (window.event) {
				   key = window.event.keyCode;
				}else if (e) {
				   key = e.which;
				}else {
				   return true;
				}
				keychar = String.fromCharCode(key);
				if ((key==null) || (key==0) || (key== keyCodes.BACK_SPACE) ||  (key == keyCodes.TAB) || (key==keyCodes.ENTER) || (key==keyCodes.ESCAPE) ) {
					   return true;
					}else if ((("?#").indexOf(keychar) > -1)) {
						return false;
					}else{
					   return true;
					}
			});
			$(this).bind('paste', function(){
				var id = $(this).attr('id');
				var tmBanChar = setTimeout(function(){
					$('#' + id).val($('#' + id).val().replace(/\?/g,'').replace(/\#/g,''));
					clearTimeout(tmBanChar);
				},200);
			});
		});		
	},
	unbindFormatOnTextfield: function(id){
		$("#"+id).unbind("keyup");
		$("#"+id).unbind("keydown");
		$("#"+id).unbind("paste");
	},
	bindFormatOntextfieldCurrencyFor: function(input,formatType){
		Utils.formatCurrencyFor(input);
		Utils.bindFormatOnTextfield(input,formatType);
	},
	bindFormatOnTextfieldInputCss: function(nameClassCss, formatType,prefix){
		var clazz='';
		if(prefix!=undefined && prefix!=null){
			clazz = prefix + ' '; 
		}
		$(clazz + '.'+nameClassCss).each(function(){
			var objectId = $(this).attr('id');
			if(objectId!=null && objectId!=undefined && objectId.length>0){
				Utils.bindFormatOnTextfield(objectId, formatType);
			}			
		});
	},
	bindFormatCalenderOnTextfieldInputCss: function(nameClassCss){
		$('.'+nameClassCss).each(function(){
			var objectId = $(this).attr('id');
			if(objectId!=null && objectId!=undefined && objectId.length>0){
				applyDateTimePicker("#"+objectId);
			}			
		});
	},
	bindFormatOnTextfield: function(id, formatType,prefix,isCtrlPress){
		var reg = /[^0-9]/;
		var regAll = /[^0-9]/g;
		switch (formatType) {
		case Utils._TF_A_Z:
			reg = /[^A-Z]/;
			regAll = /[^A-Za-z]/g; 
			break;
		case Utils._TF_NUMBER_DOT:
			reg = /[^0-9.]/;
			regAll = /[^0-9.]/g; 
			break;
		case Utils._TF_NUMBER:
			reg = /[^0-9]/;
			regAll = /[^0-9]/g; 
			break;
		case Utils._TF_NUMBER_COMMA:
			reg = /[^0-9,]/;
			regAll = /[^0-9,]/g; 
			break;
		case Utils._TF_NUMBER_SIGN:
			reg = /[^0-9-]/;
			regAll = /[^0-9-]/g; 
			break;
		case Utils._TF_NUMBER_CONVFACT:
			reg = /[^0-9\/]/;
			regAll = /[^0-9\/]/g; 
			break;
		case Utils._TF_NUMBER_COMMA_AND_DOT:
			reg = /[^0-9.,]/;
			regAll = /[^0-9.,]/g; 
			break;
		case Utils._TF_PHONE_NUMBER:
			reg = /[^0-9-.() ]/;
			regAll = /[^0-9-.() ]/g; 
			break;
		case Utils._TF_PHONE_NUMBER_SPECIAL:
			reg = /[^0-9-.() ^<|>|?|\\|\'|\"|&|~#|$|%|@|*|(|)|^|`|, ]/;
			regAll = /[^0-9-.() ^<|>|?|\\|\'|\"|&|~#|$|%|@|*|(|)|^|`|, ]/g; 
			break;	
		case Utils._CODE:
			reg = /[^0-9a-zA-Z_]/;
			regAll = /[^0-9a-zA-Z_]/g; 
			break;
		default:
			break;
		}
		var prefixTmp ='';
		if(prefix != undefined && prefix!= null && prefix != ''){
			prefixTmp = prefix;
		}
		$(prefixTmp+'#' + id).bind('keyup', function(e){
			var code;
			if (!e) var e = window.event;
			if (e.keyCode) code = e.keyCode;
			else if (e.which) code = e.which;
			if(code == keyCodes.CTRL){
				Utils._CTRL_PRESS = false;
			}
			if(code == keyCodes.SHIFT) {
				Utils._SHIFT_PRESS = false;
			}
		});
		$(prefixTmp +'#' + id).bind('keydown', function(e){
			if(formatType == Utils._TF_NUMBER || formatType == Utils._TF_NUMBER_DOT){
				if(Utils._SHIFT_PRESS){
					return false;
				}
			}
			var code;
			if (!e) var e = window.event;
			if (e.keyCode) code = e.keyCode;
			else if (e.which) code = e.which;
			var character =(code == null || code == undefined)? fromKeyCode(32).split(' ')[0]:fromKeyCode(code).split(' ')[0];	
			if (isCtrlPress != undefined && isCtrlPress) {
				if ((code >=96 && code <= 105) || (code>=48 && code<=57) || code==null || code==0 || code== keyCodes.BACK_SPACE || 
						code == keyCodes.TAB || code==keyCodes.ENTER || code==keyCodes.ESCAPE || code == keyCodes.DELETE ||
						(Utils._SHIFT_PRESS && code == keyCodes.HOME) || code == keyCodes.SHIFT || code == keyCodes.HOME || code == keyCodes.END ||
						code==keyCodes.CTRL || code == keyCodes.ARROW_LEFT || code == keyCodes.ARROW_RIGHT || code == keyCodes.ARROW_UP || code == keyCodes.ARROW_DOWN ||
						(Utils._CTRL_PRESS && (character  == 'v' || character  == 'V'))
						|| (e.ctrlKey && e.altKey)){
					if(code == keyCodes.CTRL){
						Utils._CTRL_PRESS = true;
					}
					if(code == keyCodes.SHIFT) {
						Utils._SHIFT_PRESS = true;
					}
					return true;
				} else if (reg.test(character) || (Utils._SHIFT_PRESS && !/[^0-9]/.test(character))) {
					return false;
				}else{
					return true;
				}
			} else {
				if ((code >=96 && code <= 105) || (code>=48 && code<=57) || code==null || code==0 || code== keyCodes.BACK_SPACE || 
						code == keyCodes.TAB || code==keyCodes.ENTER || code==keyCodes.ESCAPE || code == keyCodes.DELETE ||
						(Utils._SHIFT_PRESS && code == keyCodes.HOME) || code == keyCodes.SHIFT || code == keyCodes.HOME || code == keyCodes.END ||
						code==keyCodes.CTRL || code == keyCodes.ARROW_LEFT || code == keyCodes.ARROW_RIGHT || code == keyCodes.ARROW_UP || code == keyCodes.ARROW_DOWN ||
						(Utils._CTRL_PRESS && (character  == 'v' || character  == 'V'))){
					if(code == keyCodes.CTRL){
						Utils._CTRL_PRESS = true;
					}
					if(code == keyCodes.SHIFT) {
						Utils._SHIFT_PRESS = true;
					}
					return true;
				} else if (reg.test(character) || (Utils._SHIFT_PRESS && !/[^0-9]/.test(character))) {
					return false;
				}else{
					return true;
				}
			}
			
		});
		$(prefixTmp+ '#' + id).bind('paste', function(){			
			var tmAZ = setTimeout(function(){
				$(prefixTmp+ '#' + id).val($(prefixTmp+ '#' + id).val().replace(regAll,''));
				clearTimeout(tmAZ);
			},200);
		});
	},
	formatCurrencyFor: function(idInput) {
		$('#'+idInput).bind('keyup', function(e) {
			var valMoneyInput = $('#'+idInput).val();
			valMoneyInput = Utils.returnMoneyValue(valMoneyInput);
			if(isNaN(valMoneyInput) || valMoneyInput == "" || valMoneyInput == '') {
				return false;
			}
			var _valMoneyInput = formatCurrency(valMoneyInput);
			$('#'+idInput).val(_valMoneyInput);
		});
		$('#'+idInput).bind('paste', function(){
		    var tm = setTimeout(function(){
		    	var valMoneyInput = $('#'+idInput).val();
		        valMoneyInput = Utils.returnMoneyValue(valMoneyInput);
		        if(isNaN(valMoneyInput) || valMoneyInput == "" || valMoneyInput == '') {
		        	return false;
		        }
		        var _valMoneyInput = formatCurrency(valMoneyInput);
		        $('#'+idInput).val(_valMoneyInput); 
		        	clearTimeout(tm);
		    },200);
		});
	},
	bindTRbackground:function(divT){		
		$('.GeneralTable tr').each(function(){
			$(this).bind('click',function(){				
				$('.GeneralTable tr').each(function(){
					$(this).css('background-color','');
				});
				$(this).css('background-color','#FFFF66');
			});
		});
	},
	formatCurrencyForInterger: function(idInput) {
		$('#'+idInput).bind('keyup', function(e) {
			var valMoneyInput = $('#'+idInput).val();
			valMoneyInput = Utils.returnMoneyValue(valMoneyInput);
			if(isNaN(valMoneyInput) || valMoneyInput == "" || valMoneyInput == '') {
				return false;
			}
			var _valMoneyInput = formatCurrencyInterger(Number(valMoneyInput));
			$('#'+idInput).val(_valMoneyInput);
		});
		$('#'+idInput).bind('paste', function(){
		    var tm = setTimeout(function(){
		    	var valMoneyInput = $('#'+idInput).val();
		        valMoneyInput = Utils.returnMoneyValue(valMoneyInput);
		        if(isNaN(valMoneyInput) || valMoneyInput == "" || valMoneyInput == '') {
		        	return false;
		        }
		        var _valMoneyInput = formatCurrencyInterger(Number(valMoneyInput));
		        $('#'+idInput).val(_valMoneyInput); 
		        	clearTimeout(tm);
		    },200);
		});
	},
	returnMoneyValue: function(valMoney) {
		var _valMoney = valMoney + '';
		var value = _valMoney.split(',').join('');
		var i=0;
		for(i=0;i<value.length-1;i++){// Nếu tất cả từ 0 -> length-2 là 0 thì
										// lấy số cuối
			if(value[i]!='0'){
				break;
			}
		}
		return value.substring(i,value.length);
	},
	applyLiveUpdateImage:function (obj) {
		$(obj).addClass('liveUpdateImage');
		return false;
	},
	liveUpdateImage:function (object,mediaType,mediaItemId){
		$('#mediaItemId').val(mediaItemId);
		if(mediaType == 0){
			$('#player').html('').hide();
			$('#imageBox').show();			
			var orgSrc = object.attr('data');
			var source = imgServerPath +orgSrc;
			$('#imageBox').html('<img width="715" height="551" alt="" src="'+source+'" />');
			$('#imageBox').append('<a class="Sprite1 HideText DeleteLinkStyle" onclick="return ProductCatalog.deleteMediaItem();" href="javascript:void(0)">Xóa</a>');
		}else{
			$('#player').show();
			$('#imageBox').hide();
			var orgSrc = object.attr('data');
			var source = imgServerPath +orgSrc;
			if(orgSrc.substring(orgSrc.length - 3,orgSrc.length) == 'avi'){
				$('#player').html('<a class="media {width:715, height:551}" href="'+source+'"></a>');
				$('a.media').media();
			}else{
				flowplayer("player", "/resources/scripts/plugins/flowplayer/flowplayer-3.2.16.swf",{
					plugins: {
						audio: {
							url: '/resources/scripts/plugins/flowplayer/audio/flowplayer.audio-3.2.10.swf'
						}
					},
					clip: {
						url: source,
						autoPlay: true,
						autoBuffering: true
					},
					play: {
						replayLabel: 'Xem lại'
					}
				});			
			}
		}
	},
	liveUpdateImage:function (object,mediaType,mediaItemId){
		$('#mediaItemId').val(mediaItemId);
		if(mediaType == 0){
			$('#player').html('').hide();
			$('#imageBox').show();			
			var orgSrc = object.attr('data');
			var source = imgServerPath +orgSrc;
			$('#imageBox').html('<img width="715" height="551" alt="" src="'+source+'" />');
			$('#imageBox').append('<a class="Sprite1 HideText DeleteLinkStyle" onclick="return ProductCatalog.deleteMediaItem();" href="javascript:void(0)">Xóa</a>');
		}else{
			$('#player').show();
			$('#imageBox').hide();
			var orgSrc = object.attr('data');
			var source = imgServerPath +orgSrc;
			if(orgSrc.substring(orgSrc.length - 3,orgSrc.length) == 'avi'){
				$('#player').html('<a class="media {width:715, height:551}" href="'+source+'"></a>');
				$('a.media').media();
			}else{
				flowplayer("player", "/resources/scripts/plugins/flowplayer/flowplayer-3.2.16.swf",{
					plugins: {
						audio: {
							url: '/resources/scripts/plugins/flowplayer/audio/flowplayer.audio-3.2.10.swf'
						}
					},
					clip: {
						url: source,
						autoPlay: true,
						autoBuffering: true
					},
					play: {
						replayLabel: 'Xem lại'
					}
				});			
			}
		}
	},
	liveUpdateImageForProduct:function (object,mediaType,mediaItemId,checkPermission){
		$('#mediaItemId').val(mediaItemId);
		if(mediaType == 0){
			$('#player').html('').hide();
			$('#imageBox').show();			
			var orgSrc = object.attr('data');
			var source = imgServerProdcutPath +orgSrc;
			$('#imageBox').html('<img width="715" height="551" alt="" src="'+source+'" />');
			if (checkPermission == 1) {
				$('#imageBox').append('<a class="Sprite1 HideText DeleteLinkStyle" onclick="return ProductCatalog.deleteMediaItem();" href="javascript:void(0)">Xóa</a>');
			}
		}else{
			$('#player').show();
			$('#imageBox').hide();
			var orgSrc = object.attr('data');
			var source = imgServerProdcutPath +orgSrc;
			if(orgSrc.substring(orgSrc.length - 3,orgSrc.length) == 'avi'){
				$('#player').html('<a class="media {width:715, height:551}" href="'+source+'"></a>');
				$('a.media').media();
			}else{
				flowplayer("player", "/resources/scripts/plugins/flowplayer/flowplayer-3.2.16.swf",{
					plugins: {
						audio: {
							url: '/resources/scripts/plugins/flowplayer/audio/flowplayer.audio-3.2.10.swf'
						}
					},
					clip: {
						url: source,
						autoPlay: true,
						autoBuffering: true
					},
					play: {
						replayLabel: 'Xem lại'
					}
				});			
			}
		}
	},
	updateImageStatus:function (obj, timestamp){
		if(obj != null){
			obj.removeClass('liveUpdateImage');
			var orgSrc = obj.attr('data');
			if(orgSrc.indexOf("?") > -1){
				orgSrc =orgSrc.substring(0,orgSrc.indexOf("?"));
			}
			// obj.attr('src',imgServerPath +orgSrc + '?' + timestamp);
			$('#imageBox').attr('src',imgServerPath +orgSrc + '?' + timestamp);
		}
	},
	comboboxChange:function abc(id,title){
		$('#'+id).next().children().first().html(title);
		$('#'+id).children().first().html(title);
	},
	getChangedForm: function(divHidden,divSaleType, parentId,name,isMap){
		$('#errMsg').hide();
		$('.RequireStyle').show();
		$('#btnSearch,#btnCreate').hide();
		$('#btnUpdate,#btnDismiss').show();
		Utils.comboboxChange('label','Chọn hiệu xe');
		Utils.comboboxChange('type','Chọn loại xe');
		Utils.comboboxChange('category','Chọn chủng loại');
		Utils.comboboxChange('origin','Chọn nguồn gốc');
		if(divHidden != null && divHidden == true){
			$('.divBankHidden').show();
			$('.divHidden').hide();
		}
		$('span#errIU').html('*');
		$('#titleCarId').html('Thông tin xe');
		$('#title').html('Thông tin thêm mới');
		if(divSaleType != null && divSaleType == true){
			$('#staffTypeCode').val('');
			$('#staffTypeName').val('');			
			// $('#parentCode').combotree('setValue', parentId);
		}
		var isCheck = false;
		$('.InputTextStyle').each(function(){
			if (!$(this).is(':hidden') && !$(this).is(':disabled') && !isCheck){
				$(this).focus();
				isCheck = true;
			}
		});	
		
		$('.InputTextStyle').each(function(){
			$(this).unbind();
		});
		
		$('.InputTextStyle').each(function(){
			if(!$(this).is(':hidden') && !$(this).is(':disabled')){
				$(this).bind('keyup',function(event){
					if(event.keyCode == keyCodes.ENTER){
						if(!$('#btnUpdate').is(':hidden') && !$('btnUpdate').is(':disabled')){
							$('#btnUpdate').click();
						}
					}
				});
			}
		});
		
		if(isMap!=null && isMap!=undefined && isMap==true){			         	  
			MapUtil.loadMapResource(function(){
				Vietbando.loadMap('mapContainer',Vietbando._DEF_LAT,Vietbando._DEF_LNG,Vietbando._DEF_ZOOM, function(pt){					
					$('#lat').val(pt.latitude);
					$('#lng').val(pt.longitude);										
				}, false);	
			});			  
		}		

	},
	onCheckAllChange: function(checkAll, className) {
		if(checkAll) {
			$('.' + className).each(function() {
				if(this.disabled == true) {
					
				} else {
					this.checked = true;
				}
			});
		} else {
			$('.'+className).each(function() {
				if(this.disabled == true) {
					
				} else {
					this.checked = false;
				}
			});
		}
	},
	onCheckboxChangeForCheckAll: function(isCheck, className, checkAllId) {
		if(isCheck == null || isCheck == undefined) {
			return;
		} 
		if(isCheck) {
			// kiem tra xem co phai check all ko?
			var checkAll = true;
			$('.' + className).each(function() {
				if(this.disabled == true) {
					
				} else {
					if(this.checked == false) {
						checkAll = false;
					}
				}
			});
			if(checkAll == true) {
				$('#' + checkAllId).attr('checked', 'checked');
			}
		} else {
			// bo check All
			$('#' + checkAllId).removeAttr('checked');
		}
	},
	getCheckboxStateOfTree: function(){
		if(Utils._arr_Chk_State == null){
			Utils._arr_Chk_State = new Array();			
		}
		$('.jstree-leaf').each(function(){
			if($(this).hasClass('jstree-checked')){
				Utils._arr_Chk_State.push($(this).attr('id'));
			}			
		});
	},
	applyCheckboxStateOfTree: function(){
		if(Utils._arr_Chk_State!= null && Utils._arr_Chk_State.length > 0){
			for(var i=0;i<Utils._arr_Chk_State.length;i++){
				if($('#' + Utils._arr_Chk_State[i]).html()!= null && $('#' + Utils._arr_Chk_State[i]).html().trim().length > 0){
					$('#' + Utils._arr_Chk_State[i] + ' a ins').click();
				}
			}
			Utils._arr_Chk_State == new Array();
		}
	},
	resetCheckboxStateOfTree: function(){
		Utils._arr_Chk_State == new Array();
	},
	removeItemFromArray: function(array, item) {
		return $.grep(array, function(value) {
			return value != item;
		});
	},
	resizeHeightAuto:function(obj){
		if($(obj+' tbody').height() < 250){
			$('.ScrollBodySection').css({'height':'auto'});
		}else{
			$('.ScrollBodySection').css({'height':'250px'});
			$('.ScrollBodySection').jScrollPane();
		}		
	},
	resizeHeightAutoScrollCross:function(obj){
		if($('#'+ obj+' tbody').height() < 250){
			$('#'+ obj).css({'height':'auto'});
			if($('#'+ obj).data('jsp') != undefined || $('#'+ obj).data('jsp') != null) {
				$('#'+ obj).data('jsp').destroy();
			}
		}else{
			$('#'+ obj).css({'height':'250px'});
			$('#'+ obj).jScrollPane();
		}		
	},
	resizeHeightAutoScrollVertical:function(obj){
		$('#'+ obj).css({'height':'auto'});
		$('#'+ obj).jScrollPane();
	},
	afterViewImportExcel: function(responseText, statusText, xhr, $form){
		$('#divOverlay').removeClass('Overlay');
		$('#imgOverlay').hide();
		if (statusText == 'success') {				
	    	$("#responseDiv").html(responseText);		   
	    	if($('#errorExcel').html().trim() == 'true' || $('#errorExcelMsg').html().trim().length > 0){
	    		$('#errExcelMsg').html($('#errorExcelMsg').html()).show();
	    	} else {
	    		if($('#typeView').html().trim() == 'false'){
	    			var totalRow = parseInt($('#totalRow').html().trim());
	    			var numFail = parseInt($('#numFail').html().trim());
	    			var fileNameFail = $('#fileNameFail').html();
	    			var mes = format(msgErr_result_import_excel,(totalRow - numFail),numFail);		    		
	    			if(numFail > 0){
	    				mes+= ' <a href="'+ fileNameFail +'">' + msgCommon4 + '</a>';
	    			}
	    			$('#errExcelMsg').html(mes).show();
	    			$("#grid").trigger("reloadGrid");
	    			if(ProductLevelCatalog._callBackAfterImport != null){
		    			ProductLevelCatalog._callBackAfterImport.call(this);
		    		}
	    		}else{
	    			var html = $('#popup1').html();
	    			$.fancybox(html,
    					{
    						modal: true,
    						title: 'Danh sách mức từ excel',
    						afterShow: function(){
    							$('#popup1').html('');
    						},
    						afterClose: function(){
    							$('#popup1').html(html);
    						}
    					}
    				);
	    		}
	    	}
	    }	
	},
	validateAttributeData:function(dataModel,errMsgId,parentDiv){
		var parentId = '';
		if(parentDiv!= null && parentDiv!= undefined){
			parentId = parentDiv;
		}
		var msg = '';
		$(errMsgId).html('').hide();
		if($(parentId + '#lstAttributeSize').val() > 0){
			var flag = true;
			var lstAttributeId = new Array();
			var lstAttributeValue = new Array();
			var lstAttributeColumnType = new Array();
			var lstAttributeColumnValueType = new Array();
			for(var i = 1;i <= $(parentId +'#lstAttributeSize').val();i++){				
				
				var textSelector = 'attributeCode_'+i;
				var selector = $(parentId + '#' + textSelector);
				var idSelector = $(parentId + '#attributeId_'+i);
				var selectorColType = $(parentId + '#attributeColumnValueType_'+i).val();

				selector.focus();
				// Kiem tra bat buoc nhap
				var require = selector.attr('require');
				if((!isNullOrEmpty(require) && require != '0') &&  selectorColType != Utils._MULTI_CHOICE){															
					msg = Utils.getMessageOfRequireCheck(textSelector,selector.attr('attr-name'),selector.hasClass('easyui-combobox'));
					if(msg.length > 0){
						selector.focus();
						$(errMsgId).html(msg).show();
						return false;
					}					
				}
				if((selector.hasClass('easyui-combobox') && selector.combobox('getValue') != null && selector.combobox('getValue') != '' && selector.combobox('getValue').length > 0) 
					|| (selector.hasClass('MultipeSelectBox'))
					|| (selector.val() != null 	&& selector.val() != '' && selector.val().length > 0)){

					if(selectorColType == Utils._NUMBER_TYPE){
						msg = Utils.getMessageOfInvalidMaxLength(textSelector, 'Kiểu số');
						if(msg.length > 0){ 
							$(errMsgId).html(msg).show();
							return false;
						}
						if(selector.val() != undefined) {
							var valMoneyInput = Utils.returnMoneyValue(selector.val().trim());
							var maxValue = selector.attr('maxValue');
							var minValue = selector.attr('minValue');
							if(!isNullOrEmpty(maxValue) && Number(maxValue)< Number(valMoneyInput)){
								$(errMsgId).html(format(qltt_kiem_tra_maxvalue,selector.attr('attr-name'),maxValue)).show();
								return false;
							}
							if(!isNullOrEmpty(minValue) && Number(minValue)> Number(valMoneyInput)){
								$(errMsgId).html(format(qltt_kiem_tra_minvalue,selector.attr('attr-name'),minValue)).show();
								return false;
							}
							lstAttributeValue.push(Number(valMoneyInput));							
						} else {
							lstAttributeValue.push('');
						}						

					} else if(selectorColType == Utils._STRING_TYPE){
						msg = Utils.getMessageOfInvalidMaxLength(textSelector, 'Kiểu ký tự');
						if (msg.length == 0) {
							msg = Utils.getMessageOfSpecialCharactersValidate(textSelector,'Kiểu ký tự',Utils._NAME,parentId);
						}
						if(msg.length > 0){
							$(errMsgId).html(msg).show();							
							return false;
						}
						if(selector.val() != undefined) {
							lstAttributeValue.push(selector.val().trim());							
						} else {
							lstAttributeValue.push('');							
						}						
					
					}else if(selectorColType == Utils._LOCATION){
						if(selector.val() != undefined) {
							lstAttributeValue.push(selector.val().trim());							
						} else {
							lstAttributeValue.push('');							
						}						
					
					} else if(selectorColType == Utils._DATE_TYPE) {
						var date = selector.val().trim();
						if(date != '__/__/____' && !Utils.isDate(selector.val().trim())){
							msg = 'Định dạng ngày không hợp lệ, vui lòng nhập theo định dạng dd/MM/yyyy';
						}

						if(msg.length > 0){
							$(errMsgId).html(msg).show();
							return false;
						}
						if(selector.val() != undefined) {
							lstAttributeValue.push(date != '__/__/____' ? date : '');							
						} else {
							lstAttributeValue.push('');							
						}												

					} else if(selectorColType == Utils._MULTI_CHOICE) {						
						var arrValue = selector.val();
						if(arrValue != null && arrValue != undefined && $.isArray(arrValue) && arrValue.length > 0) {
							var strValue = "";
							for(var a = 0; a < arrValue.length; a++) {
								var __arrValue = arrValue[a];
								if(a == 0) {
									strValue = __arrValue;
								} else {
									strValue += ";" + __arrValue;
								}
							}
							lstAttributeValue.push(strValue);														
						} else {			
							if(!isNullOrEmpty(selector.attr('require')) && require != '0'){
								selector.focus();
								$(errMsgId).html(format(msgErr_required_field,selector.attr('attr-name'))).show();
								return false;
							}
							lstAttributeValue.push("");							
						}						
					} else {
						if(selector.combobox('getValue').trim() != '-1' && selector.combobox('getValue').trim() != -1) {							
							lstAttributeValue.push(selector.combobox('getValue').trim());							
						} else {
							lstAttributeValue.push('');							
						}						
					}
				} else {
					lstAttributeValue.push('');					
				}
				lstAttributeId.push(idSelector.val());
				lstAttributeColumnValueType.push(selectorColType.trim());
			}
			dataModel.lstAttributeId = lstAttributeId;
			dataModel.lstAttributeValue = lstAttributeValue;				
			dataModel.lstAttributeColumnValueType = lstAttributeColumnValueType;
			return true;
		}else{
			return true;
		}
	},
	isMonth:function(txtDate, separator) {
	    var aoDate,           // needed for creating array and object
	        ms,               // date in milliseconds
	        month, day, year; // (integer) month, day and year
	    // if separator is not defined then set '/'
	    if (separator == undefined) {
	        separator = '/';
	    }
	    // split input date to month, day and year
	    aoDate = txtDate.split(separator);
	    // array length should be exactly 3 (no more no less)
	    if (aoDate.length !== 2) {
	        return false;
	    }
	    // define month, day and year from array (expected format is m/d/yyyy)
	    // subtraction will cast variables to integer implicitly
	    day = '01'; // because months in JS start from 0
	    month = aoDate[0] - 1;
	    year = aoDate[1] - 0;
	    // test year range
	    if (year < 1000 || year > 3000) {
	        return false;
	    }
	    // convert input date to milliseconds
	    ms = (new Date(year, month, day)).getTime();
	    // initialize Date() object from milliseconds (reuse aoDate variable)
	    aoDate = new Date();
	    aoDate.setTime(ms);
	    // compare input date and parts from Date() object
	    // if difference exists then input date is not valid
	    if (aoDate.getFullYear() !== year ||
	        aoDate.getMonth() !== month) {
	        return false;
	    }
	    // date is OK, return true
	    return true;
	},
	loadAttributeData:function(parentDiv, parentDivCss){
		var parentId = '';
		if(parentDiv!= undefined && parentDiv!= null &&  parentDiv.length > 0){
			parentId = parentDiv;
		}
		for(var i = 1;i<=$(parentId + '#lstAttributeSize').val();i++){
			var columnValueType = $(parentId +'#attributeColumnValueType_'+i).val();
			var columType = $(parentId +'#attributeColumnType_'+i).val();
			if(columnValueType != null && columnValueType == 1){
				if(columType != null && columType == 1){
					Utils.bindFormatOnTextfield('attributeCode_'+i, Utils._TF_NUMBER_DOT, parentId);
				} else if(columType != null && columType == 2){
					
				} else{
					setDateTimePicker('attributeCode_'+i,parentId);
				}
			} else{
				$(parentId +'#attributeCode_'+i).customStyle();
				setSelectBoxValue('attributeCode_'+i, $(parentId + '#objectValue_'+i).val(),parentId);
			}
			$(parentId +'#attributeCode_'+i).val($(parentId + '#objectValue_'+i).val());
		}
		if(parentDivCss!= undefined && parentDivCss!= null && parentDivCss.length > 0){
			$(parentId+ '#parentDivCss').addClass(parentDivCss);
		}
		return false;
	},
	importExcel:function(){
		$('#isView').val(0);
		$('#importFrm').submit();
		return false;
	},
	viewExcel:function(){
		$('#isView').val(1);
		$('#importFrm').submit();
		return false;
	},
	loadComboShopTree: function(controlId, storeCode){
		var _url='/rest/catalog/shop/combotree/1/0.json';
		$('#' + controlId).combotree({url: _url});
		var t = $('#'+ controlId).combotree('tree');	
		t.tree('options').url = '';
		t.tree({			
			formatter: function(node) {
				return Utils.XSSEncode(node.text);
			},
			onSelect:function(node){			
				$('#'+storeCode).val(node.id);	
			},	            				
			onBeforeExpand:function(result){	            					
				var nodes = t.tree('getChildren',result.target);
				for(var i=0;i<nodes.length;++i){
					t.tree('remove',nodes[i].target);
				}
				$.ajax({
					type : "POST",
					url : '/rest/catalog/shop/combotree/2/'+ result.id +'.json',					
					dataType : "json",
					success : function(r) {						
						$(r).each(function(i,e){
							e.text = Utils.XSSEncode(e.text);
						});
						t.tree('append',{
							parent:result.target,
							data:r
						});
						$('.panel-body-noheader ul').css('display','block');
					}
				});
			},
			onExpand: function(result){	
				$('.panel-body-noheader ul').css('display','block');			
		}});
		$('#' + controlId).combotree('setValue',activeType.ALL);
	},
	formatBreakLine:function(){
		var size = $('#sizeList').val();
		if(size != ''){
			for(var i= 0;i<size;i++){
				var num = $('.BreakLine_' + i).html();
				if(num!=null && num!=undefined){
					num = $('.BreakLine_' + i).html().split('.').length;
					for(var j = 0;j<num;j++){
						if($('.BreakLine_' + i).html().length > 20){
							$('.BreakLine_' + i).html($('.BreakLine_' + i).html().replace('.','</br>'));
						}
					}
				}
				
			}
		}
	},
	formatBreakLineEx:function(){
		$('.BreakLine').each(function(){
			$(this).html($(this).text().replace(/\&newline;/g,'</br>'));
		});
	},
	getMessageOfSpecialCharactersValidateEx1: function(objectId, objectName,type){
		var value = $(objectId).val().trim();
		if(type == null || type == undefined){
			type = Utils._NAME;
		}
		var errMsg = '';
		if(value.length > 0){
			switch (type) {
			case Utils._CODE:
				if(!/^[0-9a-zA-Z-_.]+$/.test(value)){
					errMsg = format(msgErr_invalid_format_code,objectName);
				}
				break;
			case Utils._NAME:
				if(!/^[^<|>|?|\\|\'|\"|&|~#|$|%|@|*|(|)|^|`]+$/.test(value)){
					errMsg = format(msgErr_invalid_format_name,objectName);
				}
				break;
			case Utils._ADDRESS:
				if(!/^[^<|>|?|\\|\'|\"|&|~]+$/.test(value)){
					errMsg = format(msgErr_invalid_format_address,objectName);
				}
				break;
			case Utils._SPECIAL:
				if (!/^[^<|>|?|\\|\'|\"|&|~]+$/.test(value)) {
					errMsg = format(msgErr_invalid_format_special, objectName);
				}
				break;
			case Utils._PAYROLL:
				value = value.substring(3);
				if(!/^[0-9a-zA-Z]+$/.test(value)){
					errMsg = format(msgErr_invalid_format_payroll,objectName);
				}
				break;
			case Utils._TF_NUMBER_COMMA:
				if(!/^[0-9a-zA-Z_]+$/.test(value)){
					errMsg = format(msgErr_invalid_format_tf_id_number,objectName);
				}
				break;
			case Utils._NAME_CUSTYPE:
				if(!/^[^<|>|?|\\|\'|\"|&|~#|$|%|@|*|^|`]+$/.test(value)){
					errMsg = format(msgErr_invalid_format_name,objectName);
				}
				break;
			default:
				break;
			}
		}	
		if(errMsg.length !=0){
			$(objectId).focus();
		}
		return errMsg;
	},
	updateRownumWidthForJqGridEX1:function(parentId,colReduceWidth,arraySize,dialog){
		var pId = '';
		if(parentId!= null && parentId!= undefined){
			pId = parentId + ' ';
		}
		var lastValue=$(pId + '.datagrid-cell-rownumber').last().text().trim().length;
		var widthSTT = $('.datagrid-cell-rownumber').width();
		var s = $('.datagrid-header-row td[field='+colReduceWidth+'] div').width();
		if(dialog != null && dialog != undefined){
			widthSTT = $('.easyui-dialog .datagrid-cell-rownumber').width();
			s = $('.easyui-dialog .datagrid-header-row td[field='+colReduceWidth+'] div').width();
		}
		if(arraySize != null && arraySize != undefined){ 
			s = arraySize[0];
		}
		if(lastValue > 0){
			var extWidth = 25;
			if(lastValue > 2){
				extWidth += (lastValue - 2) * 9;
			}
			var value = extWidth - widthSTT;
			s = s - value;
			$(pId + '.datagrid-cell-rownumber').css('width',extWidth);
			$(pId + '.datagrid-header-rownumber').css('width',extWidth);
			if(parentId!= null && parentId!= undefined){
				$(parentId + ' .datagrid-row').each(function(){
					$(parentId + ' .datagrid-header-row td[field='+colReduceWidth+'] div').width(s);
					$(parentId + ' .datagrid-row td[field='+colReduceWidth+'] div').width(s);
				});
			}else{
				$('.datagrid-row').each(function(){
					$('.datagrid-header-row td[field='+colReduceWidth+'] div').width(s-1);
					$('.datagrid-row td[field='+colReduceWidth+'] div').width(s-1);
				});
			}
		}
	},
	bindFormatOnTextfieldEx1: function(id, formatType,prefix,errMsg,msg){
		var reg = /[^0-9]/;
		var regAll = /[^0-9]/g;
		switch (formatType) {
		case Utils._TF_A_Z:
			reg = /[^A-Z]/;
			regAll = /[^A-Za-z]/g; 
			break;
		case Utils._TF_NUMBER_DOT:
			reg = /[^0-9.]/;
			regAll = /[^0-9.]/g; 
			break;
		case Utils._TF_NUMBER:
			reg = /[^0-9]/;
			regAll = /[^0-9]/g; 
			break;
		case Utils._TF_NUMBER_COMMA:
			reg = /[^0-9,]/;
			regAll = /[^0-9,]/g; 
			break;
		case Utils._TF_NUMBER_SIGN:
			reg = /[^0-9-]/;
			regAll = /[^0-9-]/g; 
			break;
		case Utils._TF_NUMBER_CONVFACT:
			reg = /[^0-9\/]/;
			regAll = /[^0-9\/]/g; 
			break;
		case Utils._TF_NUMBER_COMMA_AND_DOT:
			reg = /[^0-9.,]/;
			regAll = /[^0-9.,]/g; 
			break;
		default:
			break;
		}
		var prefixTmp ='';
		if(prefix != undefined && prefix!= null && prefix != ''){
			prefixTmp = prefix;
		}
		$(prefixTmp+'#' + id).bind('keyup', function(e){
			var code;
			if (!e) var e = window.event;
			if (e.keyCode) code = e.keyCode;
			else if (e.which) code = e.which;
			if(code == keyCodes.CTRL){
				Utils._CTRL_PRESS = false;
			}
			if(code == keyCodes.SHIFT) {
				Utils._SHIFT_PRESS = false;
			}
		});
		$(prefixTmp +'#' + id).bind('keydown', function(e){
			var code;
			if (!e) var e = window.event;
			if (e.keyCode) code = e.keyCode;
			else if (e.which) code = e.which;
			if(code == null || code == undefined){
				$(errMsg).html(msg).show();
				//return false;
			}
			var character =(code == null || code == undefined)? fromKeyCode(32).split(' ')[0]:fromKeyCode(code).split(' ')[0];			
			if ((code >=96 && code <= 105) || (code>=48 && code<=57) || code==null || code==0 || code== keyCodes.BACK_SPACE || 
					code == keyCodes.TAB || code==keyCodes.ENTER || code==keyCodes.ESCAPE || code == keyCodes.DELETE ||
					(Utils._SHIFT_PRESS && code == keyCodes.HOME) || code == keyCodes.SHIFT || code == keyCodes.HOME || code == keyCodes.END ||
					code==keyCodes.CTRL || code == keyCodes.ARROW_LEFT || code == keyCodes.ARROW_RIGHT || code == keyCodes.ARROW_UP || code == keyCodes.ARROW_DOWN ||
					(Utils._CTRL_PRESS && (character  == 'v' || character  == 'V'))){
//				var s = $(errMsg).html();
//				if(s.length >0){
//					return false;
//				}
				if(code == keyCodes.CTRL){
					Utils._CTRL_PRESS = true;
				}
				if(code == keyCodes.SHIFT) {
					Utils._SHIFT_PRESS = true;
				}
				return true;
			} else if (reg.test(character) || (Utils._SHIFT_PRESS && !/[^0-9]/.test(character))) {
				return false;
			}else{
				return true;
			}
		});
		$(prefixTmp+ '#' + id).bind('paste', function(){			
			var tmAZ = setTimeout(function(){
				$(prefixTmp+ '#' + id).val($(prefixTmp+ '#' + id).val().replace(regAll,''));
				clearTimeout(tmAZ);
			},200);
		});
	},
	/**
	 * check if quantity-iniput is valid
	 * @author tuannd20
	 * @param  {String}  inputStr quantity input string
	 * @return {Boolean}          true: valid, false: invalid
	 * @date 17/10/2014
	 */
	isValidQuantityInput: function(inputStr) {
		inputStr = (inputStr || "").toString();
		return !((!/^[0-9\/]+$/.test(inputStr)) || (String(inputStr).indexOf('/')!=-1 && String(inputStr).split('/').length > 2));		
	},
	
	/**
	 * An, hien khu vuc tim kiem
	 *
	 * @author lacnv1
	 * @since Apr 24, 2015
	 * copy tu vnm.utility-report-schedule.js
	 */
	toggleSearchInput: function(t) {
		$(".ErrorMsgStyle").hide();
		$(".SuccessMsgStyle").hide();

		if ($(t).hasClass("searchHidden")) {
			$(t).removeClass("searchHidden");
			$(t).addClass("searchShow");
			$(t).parent().parent().next().slideDown();
		} else {
			$(t).addClass("searchHidden");
			$(t).removeClass("searchShow");
			$(t).parent().parent().next().slideUp();
		}
	},
	
	/**
	 * update stype header row cursor:ponter
	 * @date 27/01/2015
	 */
	 updateCellHeaderStyleSort: function(parentId, lstHeader) {
	 	if(	lstHeader instanceof Array && lstHeader.length != 0){
			for(var i in lstHeader){
			  $('#'+parentId+' .datagrid-header-row td[field='+lstHeader[i]+']').css('cursor', 'pointer');
			}
		}
	},
	/***
	 * Load distributor combobox and choose the first item
	 * @author: trungtm6
	 * @since: 09/07/2015
	 */
	initUnitCbx: function(cbxId, params, cbxWidth, selectCall, successCall, shopCodeDefault, valueField, textField){				
		var par = {};
		if (params != undefined && params != null){
			par = params;
		}
		$.ajax({
			type : "POST",
			url:'/commons/list-child-shop',
			data :($.param(par, true)),
			dataType : "json",
			success : function(data) {
				Utils.bindShopCbx('#' + cbxId, data.rows, shopCodeDefault, cbxWidth, selectCall, successCall, valueField, textField);
			}
		});
	},

	/***
	 * Load distributor combobox and choose the first item
	 * @author: vuongmq
	 * @param cbxId
	 * @param url
	 * @param params
	 * @param cbxWidth
	 * @param selectCall
	 * @param successCall
	 * @param valueDfault
	 * @param valueField
	 * @param textField
	 * @since: 28/10/2015
	 */
	initProgramCbx: function(cbxId, url, params, cbxWidth, selectCall, successCall, valueDfault, valueField, textField) {				
		var par = {};
		if (params != undefined && params != null){
			par = params;
		}
		$.ajax({
			type : "POST",
			url : url,
			data : ($.param(par, true)),
			dataType : "json",
			success : function(data) {
				Utils.bindProgramCbx('#' + cbxId, data.rows, valueDfault, cbxWidth, selectCall, successCall, valueField, textField);
			}
		});
	},
	
	/**
	 * khoi tao combobox program
	 * @author: vuongmq
	 * @param selector
	 * @param dataArr
	 * @param valueDfault
	 * @param cbxWidth
	 * @param selectCall
	 * @param successCall
	 * @param valueField
	 * @param textField
	 * @since: 28/10/2015
	 */
	bindProgramCbx: function(selector, dataArr, valueDfault, cbxWidth, selectCall, successCall, valueField, textField) {
		var value = 'ksCode';
		var text = 'ksName';
		var wid = 206;
		if (cbxWidth != undefined && cbxWidth != null && cbxWidth > 0) {
			wid = cbxWidth;
		}
		if (valueField != undefined && valueField != null) {
			value = valueField;
		}
		if (textField != undefined && textField != null) {
			text = textField;
		}
		$(selector).combobox({
			valueField: value,
			textField:  text,
			data: dataArr,
			panelWidth: wid,
			formatter: function(row) {
				return '<span style="font-weight:bold">' + Utils.XSSEncode(row.ksCode) + '</span><br/>' + '<span style="color:#888">' + Utils.XSSEncode(row.ksName) + '</span>';
			},
			filter: function(q, row) {
				q = new String(q).toUpperCase().trim();
				var opts = $(this).combobox('options');
				return ((unicodeToEnglish(row[opts.textField]).indexOf(unicodeToEnglish(q)) == 0) || unicodeToEnglish(row[opts.valueField]).indexOf(unicodeToEnglish(q)) == 0) ;
			},	
	        onSelect: function(rec) {
	        	if (selectCall != undefined && selectCall != null) {
	        		selectCall.call(this, rec);
	        	}
	        },
	        onLoadSuccess: function() {
	        	var arr = $(selector).combobox('getData');
	        	if (arr != null && arr.length > 0) {			        		
	        		if (valueDfault != undefined && valueDfault != null && valueDfault.length > 0) {
						$(selector).combobox('select', valueDfault);			        				
        			} else if (value == 'ksCode'){
        				$(selector).combobox('select', arr[0].ksCode);
        			} else if (value == 'id'){
        				$(selector).combobox('select', arr[0].ksId);
        			}
	        	}
	        	if (successCall != undefined && successCall != null) {
	        		successCall.call(this, arr);
				}
	        }
		});
	},

	/***
	 * Load distributor combobox and choose the first item Statistic
	 * @author: vuongmq
	 * @param cbxId
	 * @param url
	 * @param params
	 * @param cbxWidth
	 * @param selectCall
	 * @param successCall
	 * @param valueDfault
	 * @param valueField
	 * @param textField
	 * @since: 28/03/2016
	 */
	initProgramStatistic: function(cbxId, url, params, cbxWidth, selectCall, successCall, valueDfault, valueField, textField) {				
		var par = {};
		if (params != undefined && params != null){
			par = params;
		}
		$.ajax({
			type : "POST",
			url : url,
			data : ($.param(par, true)),
			dataType : "json",
			success : function(data) {
				Utils.bindProgramStatistic('#' + cbxId, data.rows, valueDfault, cbxWidth, selectCall, successCall, valueField, textField);
			}
		});
	},
	
	/**
	 * khoi tao combobox program Statistic
	 * @author: vuongmq
	 * @param selector
	 * @param dataArr
	 * @param valueDfault
	 * @param cbxWidth
	 * @param selectCall
	 * @param successCall
	 * @param valueField
	 * @param textField
	 * @since: 28/03/2016
	 */
	bindProgramStatistic: function(selector, dataArr, valueDfault, cbxWidth, selectCall, successCall, valueField, textField) {
		var value = 'code';
		var text = 'name';
		var wid = 206;
		if (cbxWidth != undefined && cbxWidth != null && cbxWidth > 0) {
			wid = cbxWidth;
		}
		if (valueField != undefined && valueField != null) {
			value = valueField;
		}
		if (textField != undefined && textField != null) {
			text = textField;
		}
		$(selector).combobox({
			valueField: value,
			textField:  text,
			data: dataArr,
			panelWidth: wid,
			formatter: function(row) {
				return '<span style="font-weight:bold">' + Utils.XSSEncode(row.code) + '</span><br/>' + '<span style="color:#888">' + Utils.XSSEncode(row.name) + '</span>';
			},
			filter: function(q, row) {
				q = new String(q).toUpperCase().trim();
				var opts = $(this).combobox('options');
				return ((unicodeToEnglish(row[opts.textField]).indexOf(unicodeToEnglish(q)) == 0) || unicodeToEnglish(row[opts.valueField]).indexOf(unicodeToEnglish(q)) == 0) ;
			},	
	        onSelect: function(rec) {
	        	if (selectCall != undefined && selectCall != null) {
	        		selectCall.call(this, rec);
	        	}
	        },
	        onLoadSuccess: function() {
	        	var arr = $(selector).combobox('getData');
	        	if (arr != null && arr.length > 0) {			        		
	        		if (valueDfault != undefined && valueDfault != null && valueDfault.length > 0) {
						$(selector).combobox('select', valueDfault);			        				
        			} else if (value == 'code'){
        				$(selector).combobox('select', arr[0].code);
        			} else if (value == 'id'){
        				$(selector).combobox('select', arr[0].id);
        			}
	        	}
	        	if (successCall != undefined && successCall != null) {
	        		successCall.call(this, arr);
				}
	        }
		});
	},
	
	/***
	 * Load distributor combobox and choose the first item
	 * @author: vuongmq
	 * @param cbxId
	 * @param url
	 * @param params
	 * @param cbxWidth
	 * @param selectCall
	 * @since: 13/11/2015
	 */
	initStaffCbx: function(cbxId, url, params, cbxWidth, selectCall) {				
		var par = {};
		if (params != undefined && params != null){
			par = params;
		}
		$.ajax({
			type : "POST",
			url : url,
			data : ($.param(par, true)),
			dataType : "json",
			success : function(data) {
				Utils.bindStaffCbx('#' + cbxId, data.rows, null, cbxWidth, selectCall);
			}
		});
	},

	/***
	 * Load Warehouse khi change shop
	 * @author: vuongmq
	 * @param selector
	 * @param url
	 * @param params
	 * @param cbxWidth
	 * @param selectCall
	 * @since: 07/12/2015
	 */
	changeShopGetWareHouse: function(selector, url, params, cbxWidth, selectCall) {
		var par = {};
		if (params != undefined && params != null) {
			par = params;
		}
		var wid = 206;
		if (cbxWidth != undefined && cbxWidth != null && cbxWidth > 0) {
			wid = cbxWidth;
		}
		$.ajax({
			type: "POST",
			//url : '/transfer-internal-warehouse/change-shop',
			url: url,
			data: ($.param(par, true)),
			dataType: "json",
			success: function(data) {
				if (data.lstWarehouseVO != undefined && data.lstWarehouseVO != null) {
					$(selector).combobox({
						valueField: 'warehouseId',
						textField: 'warehouseName',
						data: data.lstWarehouseVO,
						width: 250,
						panelWidth: wid,
						formatter: function(row) {
							return '<span style="font-weight:bold">' + Utils.XSSEncode(row.warehouseCode) + ' - ' + Utils.XSSEncode(row.warehouseName) + '</span>';
						},
						filter: function(q, row) {
							q = new String(q).toUpperCase().trim();
							var opts = $(this).combobox('options');
							return unicodeToEnglish(row[opts.textField]).indexOf(unicodeToEnglish(q)) >= 0;
						},
						onSelect:function(rec) {
							if (selectCall != undefined && selectCall != null) {
				        		selectCall.call(this, rec);
				        	}
						},
				        onLoadSuccess: function() {
				        	var arr = $(selector).combobox('getData');
				        	if (arr != null && arr.length > 0){
				        		$(selector).combobox('select', arr[0].warehouseId);
				        	}
				        }
					});
				}
			}
		});
	},

	/**
	 * Import Excel
	 * @author: tientv11
	 * 
	 */
	uploadExcel:function(callback,formId,tit,fileObject,prefix,errMsgId,pFakefilepc,params){
        var frm = 'importFrm';
        if(formId!=null && formId!=undefined){
        	frm = formId;
        }                       
        var title = 'Bạn có muốn nhập dữ liệu từ file';
        if(tit!=null && tit!=undefined){
              title = tit;
        }
        var file = 'excelFile';
        var fakefilepc = 'fakefilepc'; 
        if(pFakefilepc!=null && pFakefilepc!=undefined){
              fakefilepc = pFakefilepc;
        }
        if(fileObject!=null && fileObject!=undefined){
              file = fileObject;
        }                 
        if(!previewImportExcelFile(document.getElementById(file), frm, fakefilepc)){
              return false;
        }
        var objectErrorSelector = '#errExcelMsg';
        if(prefix!=null && prefix!=undefined && errMsgId!=null && errMsgId!=undefined){
//              objectErrorSelector = prefix + " " + errMsgId;
        	objectErrorSelector = prefix + " #" + errMsgId;
        
        }else if(prefix!=null && prefix!=undefined && (errMsgId==null || errMsgId==undefined)){
              objectErrorSelector = prefix + " #errExcelMsg";
              
        }else if((prefix==null || prefix==undefined) && (errMsgId!=null && errMsgId!=undefined)){
//              objectErrorSelector =  errMsgId;
              objectErrorSelector =  '#' + errMsgId;
        }           
        var options = {
                    beforeSubmit : function(){                            
                          $('.ErrorMsgStyle').html('').hide();
                          $('#divOverlay').show();
                          return true;
                    },                      
                    success : function(responseText, statusText, xhr, $form){
                          $('#divOverlay').hide();
                          if (statusText == 'success') {      
                                $("#responseDiv").html(responseText);
                                $('#token').val($('#tokenValue').html().trim());
                                if($('#errorExcel').html().trim() == 'true' || $('#errorExcelMsg').html().trim().length > 0){
                                      $(objectErrorSelector).html($('#errorExcelMsg').html()).show();
                                } else {
                                var obj = new Object();
                                
                                obj.totalRow = Number($('#totalRow').html().trim());
                                obj.numFail = Number($('#numFail').html().trim());
                                obj.fileNameFail = $('#fileNameFail').html();
                                obj.colFails = $('#colFails').html().trim();
                                obj.errMsg = $('#errorExcelMsg').html().trim();
                                obj.token = $('#tokenValue').html().trim();
                                var message = format(msgErr_result_import_excel,(obj.totalRow - obj.numFail),obj.numFail);                    
                                if(obj.numFail > 0){
                                      message+= ' <a href="'+ obj.fileNameFail +'">Xem chi tiết lỗi</a>';
                                }                                   
                                if($('#'+file).length!=0 && $('#'+fakefilepc).length!=0){
                                      try{
                                            $('#'+file).val('');
                                            $('#'+fakefilepc).val('');
                                      }catch(err){
                                            
                                      }
                                }                                   
                                $(objectErrorSelector).html(message).show();
                                if(callback!= null && callback!= undefined){
                                   callback.call(this,obj);
                                }
                          }
                          }
                    },
                    type : "POST",
                    dataType : 'html',
                    data : params
        };
        $('#'+frm).ajaxForm(options);
        $.messager.confirm('Xác nhận', title, function(r){
              if (r){
                    $('#'+frm).submit();
              }
        });
  }
};
function encodeChar(tst){
	var result = "";
	if(tst!=null && tst!= undefined){
		tst=tst.toString();
		var char="!@#$%^&*()_+-:;<>?|\'\"\/";
		for(var i=0;i<tst.length;i++){
			if(char.indexOf(tst[i])!=-1){
				if(escape(tst[i])==tst[i]){
					result += encodeURIComponent(tst[i]);			
				}else{
					result += escape(tst[i]);
				}
			}else{
				result += encodeURI(tst[i]);
			}
		}
	}
	return result;
}
function escapex(value)// tungmt show tag len htmnl
{
	var reGT = />/gi;
	var reLT = /</gi;
	var reQUOT = /"/gi;
	var reAMP = /&/gi;
	var reAPOS = /'/gi;
	var strXML = value.replace(reAMP,"&amp;").replace(reQUOT,"&quot;").replace(reLT,"&lt;").replace(reGT, "&gt;").replace(reAPOS,"&apos;");
	return strXML;
} 
function jConfirm(msg, title, callback, isClose, params, closedCallback) {
	var ret;
	$.fancybox(	
					'<div class="GeneralDialog BoxDialogConfirm"><p class="Text1Style">' + msg + '</p><div class="ButtonSection"><a href="javascript:void(0);" id="fancyConfirm_ok" class="BtnGeneralStyle Sprite2"><span class="Sprite2">Có</span></a><a href="javascript:void(0);" id="fancyConfirm_cancel" onclick="$.fancybox.close();" class="BtnGeneralStyle BtnGeneralCStyle BtnGeneralMStyle Sprite2"><span class="Sprite2">Không</span></a></div></div>',
					{
						modal : true,
						'title': "Xác nhận",
						afterShow : function() {
							var tm = setTimeout(function(){
								$('.fancybox-wrap.fancybox-desktop.fancybox-type-html.fancybox-opened').css('top', document.documentElement.scrollTop + 200);
								clearTimeout(tm);
							}, 500);
							$("#fancyConfirm_ok").click(function(){
								ret = true;
										if (callback != null) {
											callback.call(this, true);
										}
										$.fancybox.close();																				
										
							});
							$("#fancyConfirm_ok").focus();
							$("#fancybox-close").hide();
							$("#fancyConfirm_cancel").click(									
									function() {
										ret = false;
										if (closedCallback != null) {
											closedCallback.call(this, false);
										}
										$.fancybox.close();																
									});
							
							
						},
						afterClose : function() {							
						}
					});
}
function jAlert(msg)
{
	$.fancybox(
			'<div class="GeneralDialog BoxDialogConfirm"><p class="Text1Style">' + msg + '</p><div class="ButtonSection"><a href="javascript:void(0);" id="fancyAlert" class="BtnGeneralStyle Sprite2"><span class="Sprite2">Đóng</span></a></div></div>',
			{
				modal : true,
				'title' : "Thông báo",
				afterShow : function(){
					$("#fancyAlert").click(function(){
						$.fancybox.close();
					});
					$("#fancyAlert").focus();
					$("#fancybox-close").hide();
					$("#fancyAlert").click(
						function(){
							$.fancybox.close();
					});
					setTimeout(function(){
						$('.fancybox-wrap.fancybox-desktop.fancybox-type-html.fancybox-opened').css('top',document.documentElement.scrollTop +  $(window).height()/2);
					},500);
				},
				afterClose : function() {
					isOpenJAlert = false;
				}
			});
}
function Alert(title,msg,callback){
	$.messager.alert(title,msg,null,function(){
		if (callback!= undefined && callback != null) {
			callback.call(this, true);
		}
	});
	return false;
}
function jConfirmAdv(msg, title, callback) {
	var ret;
	$.fancybox(	
					'<div class="GeneralDialog BoxDialogConfirm"><p class="Text1Style">' + msg + '</p><div class="ButtonSection"><a href="javascript:void(0);" id="fancyConfirm_ok" class="BtnGeneralStyle Sprite2"><span class="Sprite2">Có</span></a><a href="javascript:void(0);" id="fancyConfirm_cancel" onclick="$.fancybox.close();" class="BtnGeneralStyle BtnGeneralCStyle BtnGeneralMStyle Sprite2"><span class="Sprite2">Không</span></a></div></div>',
					{
						modal : true,
						'title': "Xác nhận",
						afterShow : function() {
							$("#fancyConfirm_ok").click(function(){
										ret = true;
										if (callback != null) {
											callback.call(this, true);
										}
										$.fancybox.close();																				
										
							});
							$("#fancyConfirm_ok").focus();
							$("#fancybox-close").hide();
							$("#fancyConfirm_cancel").click(
									function() {										
										$.fancybox.close();																
							});
						},
						afterClose : function() {							
						}
					});
	if (ret == true) return true; else return false;
}
function showLoading(id){
	var objId = 'loading';
	if(id!= undefined && id!= null && id.length > 0){
		objId = id;
	}
	$('#' + objId).css('visibility','visible');
	return false;
}
function hideLoading(id){
	var objId = 'loading';
	if(id!= undefined && id!= null && id.length > 0){
		objId = id;
	}
	$('#' + objId).css('visibility','hidden');
	return false;
}
function isNullOrEmpty(value){
	if(value!=undefined && value!=null && !isNaN(value)){
		value = new String(value);
	}
	if(value == undefined || value == null || value == 'null' || value.trim()=='' || value.trim().length == 0){
		return true;
	}
	return false;
}
function setSelectBoxValue(objectId,defaultValue,prefix){
	var value = -2;
	var prefixTmp ='';
	if(defaultValue!= undefined && defaultValue!= null){
		value = defaultValue;
	}
	if(prefix != undefined && prefix!= null && prefix != ''){
		prefixTmp = prefix;
	}
	$(prefixTmp +'#' + objectId).val(Utils.XSSEncode(value));
	$(prefixTmp +'#' + objectId).change();
}
function setTextboxValue(objectId,value,prefix){
	var vl = '';
	var prefixTmp ='';
	if(!isNullOrEmpty(value)){
		vl = value;
	}
	$(prefixTmp +'#' + objectId).val(Utils.XSSEncode(value));
	$(prefixTmp +'#' + objectId).change();
	$('#' + objectId).val(vl);
}
function setCheckBoxValue(objectId,value){
	if(value==1){
		$('#' + objectId).attr('checked', 'checked');
	}
}
function focusFirstTextbox(){
	var isCheck = false;
	$('.InputTextStyle').each(function(){
	    if(!$(this).is(':hidden') && !$(this).is(':disabled') && !isCheck){
	        isCheck = true;
	        $(this).focus();
	    }
	});
}
function uploadPdfFile(fileObj,formId,inputText){
	$('#resultPdfMsg').html('').hide();
	var importTokenVal =   $('#importTokenVal').val();
	if(importTokenVal!=null && importTokenVal!=undefined){
		$('#importTokenVal').val($('#token').val());
	}
	var inputId = 'fakePdffilepc';
	if(inputText != null || inputText != undefined){
		inputId = inputText;
	}
	var fileTypes=["pdf"];
	if (fileObj.value != '') {
		if (!/(\.pdf)$/i.test(fileObj.value)) {
			//Tập tin chọn phải có đuôi mở rộng là .pdf 
			$('#errPdfMsg').html(msgErr_pdf_file_invalid_type).show();
			fileObj.value = '';
			fileObj.focus();
			document.getElementById(inputId).value ='';			
			return false;
		} else {
			$('#errPdfMsg').html('').hide();
			var src = '';
			if ($.browser.msie){
			    var path = encodeURI(what.value);
			    path = path.replace("C:%5Cfakepath%5C","");
			    var path = decodeURI(path);
			    document.getElementById(inputId).value = path;
			}else{
				document.getElementById(inputId).value = fileObj.value;
			}	
			// $('#' + formId).submit();
			// return true;
		}
	}else{
		$('#errPdfMsg').html('Vui lòng chọn file PDF').show();		
		return false;
	}
	return true;
}
//upload file ZIP
function uploadZIPFile(fileObj,formId,inputText, errView){
	$('#errMsg').html('').hide();
	if(errView != undefined && errView != '') {
		$('#'+ errView).html('').hide();
	}
	var importTokenVal =   $('#importTokenVal').val();
	if(importTokenVal!=null && importTokenVal!=undefined){
		$('#importTokenVal').val($('#token').val());
	}
	var inputId = 'fakefilepc';
	if(inputText != null || inputText != undefined){
		inputId = inputText;
	}
	var fileTypes=["zip"];
	if (fileObj.value != '') {
		if (!/(\.zip)$/i.test(fileObj.value)) {
			if(errView != undefined && errView != '') {
				//Tập tin chọn phải có đuôi mở rộng là .pdf ; hien thi len popUp
				$('#'+ errView).html(msgErr_zip_file_invalid_type).show();
			}else {
				//Tập tin chọn phải có đuôi mở rộng là .pdf 
				$('#errMsg').html(msgErr_zip_file_invalid_type).show();
			}
			fileObj.value = '';
			fileObj.focus();
			document.getElementById(inputId).value ='';			
			return false;
		} else {
			$('#'+ errView).html('').hide();
			$('#errMsg').html('').hide();
			var src = '';
			if ($.browser.msie){
			    var path = encodeURI(what.value);
			    path = path.replace("C:%5Cfakepath%5C","");
			    var path = decodeURI(path);
			    document.getElementById(inputId).value = path;
			}else{
				document.getElementById(inputId).value = fileObj.value;
			}	
			// $('#' + formId).submit();
			// return true;
		}
	}else{
		$('#errMsg').html('Vui lòng chọn file ZIP!').show();		
		return false;
	}
	return true;
}
//upload file bao cao vnm.report-manager.js, load ca file .jrxml
function uploadJasperFile(fileObj,formId,inputText, errView){
	$('#errMsg').html('').hide();
	if(errView != undefined && errView != '') {
		$('#'+ errView).html('').hide();
	}
	var importTokenVal =   $('#importTokenVal').val();
	if(importTokenVal!=null && importTokenVal!=undefined){
		$('#importTokenVal').val($('#token').val());
	}
	var inputId = 'fakefilepc';
	if(inputText != null || inputText != undefined){
		inputId = inputText;
	}
	var fileTypes=["zip"];
	if (fileObj.value != '') {
		if (!/(\.jrxml)$/i.test(fileObj.value)) {
			if(errView != undefined && errView != '') {
				//Tập tin chọn phải có đuôi mở rộng là .jrxml ; hien thi len popUp
				$('#'+ errView).html(msgErr_report_file_invalid_type).show();
			}else {
				//Tập tin chọn phải có đuôi mở rộng là .jrxml 
				$('#errMsg').html(msgErr_report_file_invalid_type).show();
			}
			fileObj.value = '';
			fileObj.focus();
			document.getElementById(inputId).value ='';			
			return false;
		} else {
			$('#' + errView).html('').hide();
			$('#errMsg').html('').hide();
			var src = '';
			if ($.browser.msie){
			    var path = encodeURI(what.value);
			    path = path.replace("C:%5Cfakepath%5C","");
			    var path = decodeURI(path);
			    document.getElementById(inputId).value = path;
			}else{
				document.getElementById(inputId).value = fileObj.value;
			}	
			// $('#' + formId).submit();
			// return true;
		}
	}else{
		$('#errMsg').html('Vui lòng chọn Report file .jrxml !').show();		
		return false;
	}
	return true;
}


function loadDataForTree(url,treeId){
	var tId = 'tree';
	if(treeId!= null && treeId!= undefined){
		tId = treeId;
	}
	$('#' + tId).jstree({
        "plugins": ["themes", "json_data","ui"],
        "themes": {
            "theme": "classic",
            "icons": false,
            "dots": true
        },
        "json_data": {
        	"ajax" : {
                "url" : url,
                "data" : function (n) {
                        return { id : n.attr ? n.attr("id") : 0, name: n.attr ? n.attr("name") : "", url: n.attr ? n.attr("url") : ""};
                    }
            }        	
        }
	});	
}

function loadDataForTreeWithCheckbox(url,treeId){
	var tId = 'tree';
	if(treeId!= null && treeId!= undefined){
		tId = treeId;
	}
	$('#' + tId).jstree({
        "plugins": ["themes", "json_data","ui","checkbox"],
        "themes": {
            "theme": "classic",
            "icons": false,
            "dots": true
        },
        "json_data": {
        	"ajax" : {
        		"method": "post",
                "url" : url,
                "data" : function (n) {
                        return { id : n.attr ? n.attr("id") : 0 };
                    }
            }        	
        }
	});	
}
var datepickerTriggerTimeout = null;
function applyDateTimePicker(selector, dateFormat, hideYear, memuMonthShow, menuYearShow, yearRangeFlag, showFocus, timeShow, yearRangeFuture, monthYearShow, onlyMonthCurrent, onSelectCallback) {
	var format = "dd/mm/yy";
	if (dateFormat != null) {
		format = dateFormat;		
	}
	if(monthYearShow != null){
		MaskManager.maskMonth(selector);
	}else{
		MaskManager.maskDate(selector);
	}
	var menuMonth = false;
	if (memuMonthShow != null) {
		menuMonth = memuMonthShow;
	}
	var menuYear = false;
	if (menuYearShow != null) {
		menuYear = menuYearShow;
	}
	var minDate = null;
	var maxDate = null;
	if (hideYear != null && hideYear == true) {
		minDate = new Date(2012, 0, 1);
		maxDate = new Date(2012, 11, 31);
	}
	var yearRange = null;
	if (yearRangeFlag != null && yearRangeFlag != undefined) {
		var now = new Date();
		var t = now.getFullYear();
		var f = t -100;
		if(yearRangeFuture != null && yearRangeFuture != undefined){
			t = t + 100;
			yearRange = f+":"+t;
		}else{
			yearRange = f+":"+t;
		}
		
		
	}
	var showOn = 'button';
	if (showFocus != null && showFocus == true) {
		showOn = 'focus';
	}
	if (!timeShow)
	{
		$(selector).datepicker(
			{
				//monthNames: ["Tháng 1", "Tháng 2", "Tháng 3", "Tháng 4", "Tháng 5", "Tháng 6", "Tháng 7", "Tháng 8", "Tháng 9", "Tháng 10", "Tháng 11", "Tháng 12"],
				//monthNamesShort: ["Tháng 1", "Tháng 2", "Tháng 3", "Tháng 4", "Tháng 5", "Tháng 6", "Tháng 7", "Tháng 8", "Tháng 9", "Tháng 10", "Tháng 11", "Tháng 12"],
				monthNames: [msgThang1, msgThang2, msgThang3, msgThang4, msgThang5, msgThang6, msgThang7, msgThang8, msgThang9, msgThang10, msgThang11, msgThang12],
				monthNamesShort: [msgThang1, msgThang2, msgThang3, msgThang4, msgThang5, msgThang6, msgThang7, msgThang8, msgThang9, msgThang10, msgThang11, msgThang12],
				//dayNamesMin: ["CN", "T2", "T3", "T4", "T5", "T6", "T7"],
				dayNamesMin: [msgCN, msgThu2, msgThu3, msgThu4, msgThu5, msgThu6, msgThu7],
				showOn: showOn,
				dateFormat: format,
				buttonImage: '/resources/images/icon_calendar.jpg',
				buttonImageOnly: true,
				changeMonth: menuMonth,
				changeYear: menuYear,
				minDate: minDate,
				maxDate: maxDate,
				buttonText: '',
				useThisTheme: 'start',
				yearRange: yearRange,
				create: function(event, ui) {
				},
				onClose: function(dateText, inst) {
				},
				beforeShow: function(input, inst) {
					setTimeout(function() {
						$('.ui-datepicker-month').css('width', '60%');
						$('#ui-datepicker-div').css('z-index', 1000000);
					}, 1);
					if ((hideYear != null && hideYear == true)||(onlyMonthCurrent != null && onlyMonthCurrent == true)) {
						setTimeout(function() {
							$('.ui-datepicker-next').hide();
							$('.ui-datepicker-prev').hide();
							$('.ui-datepicker-year').hide();
						}, 1);
					}
				},
				onChangeMonthYear: function(year, month, inst) {
					setTimeout(function() {
						$('.ui-datepicker-month').css('width', '60%');
					}, 1);
					if ((hideYear != null && hideYear == true)||(onlyMonthCurrent != null && onlyMonthCurrent == true)) {
						setTimeout(function() {
							$('.ui-datepicker-next').hide();
							$('.ui-datepicker-prev').hide();
							$('.ui-datepicker-year').hide();
						}, 1);
					}
				},
				onSelect: function(data) {
					if (onSelectCallback != undefined && onSelectCallback != null) {
						onSelectCallback.call(this, data);
					}					
				}
			}
		);
	}
}
var AttributeUtil = {
	disabledAttributes:function(){
		for(var i = 1;i<=$('#lstAttributeSize').val();i++){
			var obj = $('#attributeCode_'+i);
			if(obj.hasClass('HasClassSelected')){
				disableSelectbox('attributeCode_'+i);
			}else{
				disabled('attributeCode_'+i);
			}
		}
	},
	enabledAttributes:function(){
		for(var i = 1;i<=$('#lstAttributeSize').val();i++){
			var obj = $('#attributeCode_'+i);
			if(obj.hasClass('HasClassSelected')){
				enableSelectbox('attributeCode_'+i);
			}else{
				enable('attributeCode_'+i);
			}
		}
	}
};
function showMonthAndYear(selector){
	$(selector).datepicker({
		dateFormat: 'mm/yy',
	    changeMonth: true,
	    changeYear: true,
	    showButtonPanel: true,
		monthNames: ["Tháng 1", "Tháng 2", "Tháng 3", "Tháng 4", "Tháng 5", "Tháng 6", "Tháng 7", "Tháng 8", "Tháng 9", "Tháng 10", "Tháng 11", "Tháng 12"],
		monthNamesShort: ["T1", "T2", "T3", "T4", "T5", "T6", "T7", "T8", "T9", "T10", "T11", "T12"],
		showOn: 'focus',
		buttonImage: '/resources/images/icon-calendar.png',
		buttonImageOnly: true,
		buttonText: '',
		useThisTheme: 'start',
		create: function(event, ui) {
		},
		onClose: function(dateText, inst) {
	        var month = $("#ui-datepicker-div .ui-datepicker-month :selected").val();
	        var year = $("#ui-datepicker-div .ui-datepicker-year :selected").val();
	        $(this).val($.datepicker.formatDate('mm/yy', new Date(year, month, 1)));
	    }
	});
}
function setDateTimePicker(id, prefix, onlyMonthCurrent){
	// $('#' +id).attr('readonly','readonly');
	var prefixTmp ='';
	if(prefix != undefined && prefix!= null && prefix != ''){
		prefixTmp = prefix;
	}
	if(onlyMonthCurrent) {
		applyDateTimePicker(prefixTmp+'#' + id, null, null, null, null, null, null, null, null,null,true);
	} else{
		applyDateTimePicker(prefixTmp+'#' + id);
	}
	$(prefixTmp+'#' + id).keypress(function(event){
		var keycode = (event.keyCode ? event.keyCode : event.which);
		if (keycode == keyCodes.BACK_SPACE || keycode == keyCodes.DELETE) {
			$(this).val('');
			$(this).focus();
		}
	});
}
function setDateTimePickerTrigger(id, prefix, onlyMonthCurrent){
	// $('#' +id).attr('readonly','readonly');
	var prefixTmp ='';
	if(prefix != undefined && prefix!= null && prefix != ''){
		prefixTmp = prefix;
	}
	var fTrigger = function(object) {
		$(object).trigger("change");
	};
	if(onlyMonthCurrent) {
		applyDateTimePicker(prefixTmp+'#' + id, null, null, null, null, null, null, null, null,null,true,fTrigger('#'+id));
	} else{
		applyDateTimePicker(prefixTmp+'#' + id, null, null, null, null, null, null, null, null,null,undefined,fTrigger('#'+id));
	}
	$(prefixTmp+'#' + id).keypress(function(event){
		var keycode = (event.keyCode ? event.keyCode : event.which);
		if (keycode == keyCodes.BACK_SPACE || keycode == keyCodes.DELETE) {
			$(this).val('');
			$(this).focus();
		}
	});
}
function removeDateTimePicker(id, prefix){
	// $('#' +id).attr('readonly','readonly');
	var prefixTmp ='';
	if(prefix != undefined && prefix!= null && prefix != ''){
		prefixTmp = prefix;
	}
	$(prefixTmp+'#' + id).datepicker( "destroy" );
	$(prefixTmp+'#' + id).removeClass("hasDatepicker");
}
function disableDateTimePicker(id, prefix){
	var prefixTmp ='';
	if(prefix != undefined && prefix!= null && prefix != ''){
		prefixTmp = prefix;
	}
	$(prefixTmp+'#' + id).datepicker( "disable");
}
function setAndDisableDateTimePicker(id,prefix){
	var prefixTmp ='';
	if(prefix != undefined && prefix!= null && prefix != ''){
		prefixTmp = prefix;
	}
	$(prefixTmp+'#' + id).attr('disabled',true);
	setDateTimePicker(id,prefix);
	disableDateTimePicker(id,prefix);
}
function enableDateTimePicker(id, prefix){
	var prefixTmp ='';
	if(prefix != undefined && prefix!= null && prefix != ''){
		prefixTmp = prefix;
	}
	$(prefixTmp+'#' + id).datepicker( "enable");
}
function numbersonly(e, id) {
	var key;
	var keychar;
	if (window.event) {
	   key = window.event.keyCode;
	}else if (e) {
	   key = e.which;
	}else {
	   return true;
	}
	keychar = String.fromCharCode(key);

	if ((key==null) || (key==0) || (key==8) ||  (key==9) || (key==13) || (key==27) ) {
	   return true;
	}else if ((("0123456789").indexOf(keychar) > -1)) {
		return true;
	}else{
	   return false;
	}
}
function updateTokenImport(){
	var tokenHiddenFiled = $('#tokenHiddenFiled').val();
	if(tokenHiddenFiled!=null && tokenHiddenFiled!=undefined){
		Utils.setToken(tokenHiddenFiled.trim());
		$('#importTokenVal').val(tokenHiddenFiled.trim());
	}	
}
var dates = {
	    convert:function(d) {
	        // Converts the date in d to a date-object. The input can be:
	        // a date object: returned without modification
	        // an array : Interpreted as [year,month,day]. NOTE: month is 0-11.
	        // a number : Interpreted as number of milliseconds
	        // since 1 Jan 1970 (a timestamp)
	        // a string : Any format supported by the javascript engine, like
	        // "YYYY/MM/DD", "MM/DD/YYYY", "Jan 31 2009" etc.
	        // an object : Interpreted as an object with year, month and date
	        // attributes. **NOTE** month is 0-11.
	        return (
	            d.constructor === Date ? d :
	            d.constructor === Array ? new Date(d[0],d[1],d[2]) :
	            d.constructor === Number ? new Date(d) :
	            d.constructor === String ? new Date(d) :
	            typeof d === "object" ? new Date(d.year,d.month,d.date) :
	            NaN
	        );
	    },
	    compare:function(a,b) {
	        // Compare two dates (could be of any type supported by the convert
	        // function above) and returns:
	        // -1 : if a < b
	        // 0 : if a = b
	        // 1 : if a > b
	        // NaN : if a or b is an illegal date
	        // NOTE: The code inside isFinite does an assignment (=).
	        return (
	            isFinite(a=this.convert(a).valueOf()) &&
	            isFinite(b=this.convert(b).valueOf()) ?
	            (a>b)-(a<b) :
	            NaN
	        );
	    },
	    inRange:function(d,start,end) {
	        // Checks if date in d is between dates in start and end.
	        // Returns a boolean or NaN:
	        // true : if d is between start and end (inclusive)
	        // false : if d is before start or after end
	        // NaN : if one or more of the dates is illegal.
	        // NOTE: The code inside isFinite does an assignment (=).
	       return (
	            isFinite(d=this.convert(d).valueOf()) &&
	            isFinite(start=this.convert(start).valueOf()) &&
	            isFinite(end=this.convert(end).valueOf()) ?
	            start <= d && d <= end :
	            NaN
	        );
	    },
	    addDays:function(n){
	    	var curdate = new Date();
	    	var time = curdate.getTime();
	        var changedDate = new Date(time + (n * 24 * 60 * 60 * 1000));
	        curdate.setTime(changedDate.getTime());	        
	        return $.datepicker.formatDate('dd/mm/yy', curdate);
	    }
	};
function checkNum(data) { 
	var valid = "0123456789.";
	var ok = 1; var checktemp;
	for (var i=0; i<data.length; i++) {
		checktemp = "" + data.substring(i, i+1);
		if (valid.indexOf(checktemp) == "-1") return 0; 
	}
	return 1;
}
function formatCurrency(num) {
		if(num == undefined || num == null) {
			return '';
		}
		num = num.toString().split('.');
		var ints = num[0].split('').reverse();
		for (var out=[],len=ints.length,i=0; i < len; i++) {
			if (i > 0 && (i % 3) === 0){
				out.push(',');	
			}
			out.push(ints[i]);
		}
	  out = out.reverse() && out.join('');
	  if (num.length === 2) out += '.' + num[1];
	  if(out.length > 1 && out[0] == '-' && out[1] == ','){
		  out = out.replace(',','');
	  }
	  return out;
}
function formatCurrencyInterger(value) {
	if(value < 0) {
		var valuetmp = Math.abs(value);
		var valuetest = '-'+ formatCurrency(valuetmp);
		return valuetest;
	}
	else {
		return formatCurrency(value);
	}
}
function formatCurrencyNegativeToInteger(value) {
	if(value < 0) {
		var valuetmp = Math.abs(value);
		var valuetest = formatCurrency(valuetmp);
		return valuetest;
	}
	else {
		return formatCurrency(value);
	}
}
function roundNumber(value,ext){
	var tmp=Math.pow(10, Math.round(ext));
	return (Math.round(Number(value)*tmp)/tmp);
}

/**
* Convert gia tri theo ham, ung voi typeMath va lay extention cua gia tri
* default: typemMath: Utils._MATH_ROUND, ext: 0
* @author vuongmq
* @params value: gia tri truyen vao; 
* @params typeMath: loai convert Value; 
* @param ext: lay gia tri bao nhieu so thap phan
* @return value, ngc lai return 0
* @since 20/08/2015
*/
function convertValueMath(value, typeMath, ext) {
	if (value == undefined || value == null || value == '') {
		return 0;
	}
	if (typeMath == undefined || typeMath == null || typeMath == '') {
		typeMath = Utils._MATH_ROUND;
	}
	if (ext == undefined || ext == null || ext == '') {
		ext = 0;
	}
	if (typeMath == Utils._MATH_ROUND) {
		var tmp = Math.pow(10, Math.round(ext));
		return (Math.round(Number(value) * tmp) / tmp);
	} else if (typeMath == Utils._MATH_FLOOR) {
		var tmp = Math.pow(10, Math.floor(ext));
		return (Math.floor(Number(value) * tmp) / tmp);
	} else if (typeMath == Utils._MATH_CEIL) {
		var tmp = Math.pow(10, Math.ceil(ext));
		return (Math.ceil(Number(value) * tmp) / tmp);
	}
}

function formatFloatValue(value,toF){	
	var  fixed = toF;
	if(toF==null ||toF.length==0||toF == undefined){
		fixed = 0;
	}
	if(value==undefined || value==null){
		return 0;
	}
	if(value.toString().indexOf('.')==-1){
		return formatCurrency(value);
	}	
	var result = parseFloat(value).toFixed(fixed);
	if(result.toString().indexOf('.')==-1){
		return formatCurrency(result);
	}else{
		var strResult = result.toString().split('.');
		if(strResult[1].length==0){
			return formatCurrency(strResult[0]);
		}
		var last = strResult[1].toString().substring(0,fixed);
		if(parseInt(last,10)==0){
			return formatCurrency(strResult[0]);
		}else{
			//last = last.replace('0','');
			var i = last.length - 1;
			for (; i > -1; i--) {
			    if (last[i] != '0') {
			        break;
			    }
			}
			last = last.substring(0, i+1);
			return formatCurrency(strResult[0]) + '.' + last;
		}
	}
	
}
function disabled(objId){
	if($('#' + objId).length!=0){
		$('#' + objId).attr('disabled','disabled');
		$('#' + objId).addClass('BtnGeneralDStyle');
	}
}
function enable(objId){
	if($('#' + objId).length!=0){
		$('#' + objId).removeAttr('disabled');
		$('#' + objId).removeClass('BtnGeneralDStyle');
	}
}
function readonly(objId){
	$('#' + objId).attr('readonly','readonly');
}
String.format = function( text ){
    if ( arguments.length <= 1 ){return text;}
    var tokenCount = arguments.length - 2;
    for( var token = 0; token <= tokenCount; token++ ){
        text = text.replace( new RegExp( "\\{" + token + "\\}", "gi" ), arguments[ token + 1 ] );
    }
    return text;
};
String.prototype.endsWith = function (a) {
    return this.substr(this.length - a.length) === a;
};
String.prototype.startsWith = function (a) {
    return this.substr(0, a.length) === a;
};
String.prototype.trim = function () {
    return this.replace(/^\s*/, "").replace(/\s*$/, "");
    };
String.prototype.count=function(s1) { 
    return (s1 == undefined || s1 == null || s1 == '') ? 0 : (this.length - this.replace(new RegExp(s1,"g"), '').length) / s1.length;
};
String.prototype.bool = function() {
    return (/^true$/i).test(this);
};
function format(text){
	// check if there are two arguments in the arguments list
    if ( arguments.length <= 1 ){
        // if there are not 2 or more arguments there's nothing to replace
        // just return the original text
        return text;
    }
    // decrement to move to the second argument in the array
    var tokenCount = arguments.length - 2;
    for( var token = 0; token <= tokenCount; token++ ){
        // iterate through the tokens and replace their placeholders from the
		// original text in order
        text = text.replace(new RegExp( "\\{" + token + "\\}", "gi" ), Utils.XSSEncode(arguments[ token + 1 ]));
    }
    return text;
};
function resetAllOptions(objId){
	$('#' + objId + ' option').show();
}
function hideOption(selId, optionValue){
	resetAllOptions(selId);
	$('#' + selId + ' option[value='+ optionValue +']').hide();
}
function getCurrentDate(){
	var d=new Date();
	var dStr= (d.getDate().toString().length>1?d.getDate():'0'+d.getDate())+'/'+((d.getMonth()+1).toString().length>1?(d.getMonth()+1):'0'+(d.getMonth()+1))+'/'+d.getFullYear();
	return dStr;
}
function getCurrentMonth(){
	var d=new Date();
	var dStr= ((d.getMonth()+1).toString().length>1?(d.getMonth()+1):'0'+(d.getMonth()+1))+'/'+d.getFullYear();
	return dStr;
}
function getCurrentMonthAddOne(){
	var d=new Date();
	var dStr= ((d.getMonth()+2).toString().length>1?(d.getMonth()+2):'0'+(d.getMonth()+2))+'/'+d.getFullYear();
	return dStr;
}
function getCurrentMonthAddOneNextYear(){
	var d=new Date();
	var dStr= ((d.getMonth()+2).toString().length>1?(d.getMonth()+2):'0'+(d.getMonth()+2))+'/'+(d.getFullYear()+1);
	return dStr;
}
function getCurrentYear(){
	var d=new Date();
	return d.getFullYear();
}
function getLastWeek(){
	var d = new Date();
	var previousWeek= new Date(d.getTime() - 7 * 24 * 60 * 60 * 1000);
	var lastWeek= (previousWeek.getDate().toString().length>1?previousWeek.getDate():'0'+previousWeek.getDate())+'/'+((previousWeek.getMonth()+1).toString().length>1?(previousWeek.getMonth()+1):'0'+(previousWeek.getMonth()+1))+'/'+previousWeek.getFullYear();
	return lastWeek;
}
function resizeFancybox(){
	$('.fancybox-inner').css({'height':'auto'});
	$('#fancyData').css({'height':'auto'});
	$.fancybox.update();
}
function getQuantity(amount,convfact){
	return StockValidateInput.getQuantity(amount, convfact);
}
function getShopCodeByShopCodeAndName(str){
	var value = str.trim().split("-");
	return value[0];
}
function formatQuantity(amount,convfact){
	return StockValidateInput.formatStockQuantity(amount, convfact);
}
function formatQuantityEx(amount,convfact){		
	var quantity = Number(getQuantity(amount,convfact));
	if(quantity<0){
		quantity = quantity * (-1);
		var str = formatQuantity(quantity,convfact);
		return '-' + str.split('/')[0] + '/' + '-' + str.split('/')[1];
	}else{
		return formatQuantity(quantity, convfact);
	}
}
function NextAndPrevTextField(e,selector,clazz){	
	var index = $('input.'+clazz).index(selector);	
	var shiftKey = e.shiftKey && e.keyCode == keyCodes.TAB;	
	//Co the them keyCodes.ENTER vo 
	if((e.keyCode == keyCodes.ARROW_DOWN)){	
		++index;
		var nextSelector = $('input.'+clazz).eq(index);
		if($(nextSelector).hasClass(clazz)){				
			var tm = setTimeout(function(){
				$('input.'+clazz).eq(index).focus();
				clearTimeout(tm);
			}, 20);
		}
	}else if(e.keyCode == keyCodes.ARROW_UP || shiftKey){
		--index;
		var nextSelector = $('input.'+clazz).eq(index);
		if($(nextSelector).hasClass(clazz)){	
			var tm = setTimeout(function(){
				$('input.'+clazz).eq(index).focus();
				clearTimeout(tm);
			}, 20);
		}			
	}
}
/**
 * map data structure
 * @param {array} initData init data for map, array of object(key, value).
 *                         Example: [{key: k1, value: v1}, {key: k2, value: v2}]
 */
function Map(initData) {
    this.keyArray = (function(initData){
	    /*
	     * initialize data
	     */
	    try {
		    if (initData !== null && initData !== undefined && initData instanceof Array) {
		    	var key = "key", value = "value";
		    	var initKeyArray = new Array();
		    	initData.forEach(function(item) {
		    		if (item[key] !== undefined && item[key] !== null) {
		    			initKeyArray.push(item[key]);
		    		}
		    	});
		    	return initKeyArray;
		    }
		    return new Array();
	    } catch (e) {
	    	return new Array();
	    }    	
    })(initData); // Keys
    
    this.valArray = (function(initData){
	    /*
	     * initialize data
	     */
	    try {
		    if (initData !== null && initData !== undefined && initData instanceof Array) {
		    	var key = "key", value = "value";
		    	var initValArray = new Array();
		    	initData.forEach(function(item) {
		    		if (item[value] !== undefined && item[value] !== null) {
		    			initValArray.push(item[value]);
		    		}
		    	});
		    	return initValArray;
		    }
		    return new Array();
	    } catch (e) {
	    	return new Array();
	    }    	
    })(initData);; // Values

    this.put = put;
    this.get = get;
    this.size = size;  
    this.clear = clear;
    this.keySet = keySet;
    this.valSet = valSet;
    this.showMe = showMe;   // returns a string with all keys and values in map.
    this.findIt = findIt;
    this.remove = remove;	
    this.findIt4Val = findIt4Val;
	function put(key, val) {
    	var elementIndex = this.findIt(key);
	    if(elementIndex == (-1)) {
	        this.keyArray.push(key);
	        this.valArray.push(val);
	    } else {
	        this.valArray[elementIndex] = val;
	    }
	}
	function get(key) {
	    var result = null;
	    var elementIndex = this.findIt(key);
	    if (elementIndex != (-1)) {
	        result = this.valArray[elementIndex];
	    }
	    return result;
	}	
	function remove(key) {
	    var result = null;
	    var elementIndex = this.findIt(key);
	    if(elementIndex != (-1)) {
	        // this.keyArray = this.keyArray.myRemoveAt(elementIndex);
	    	this.keyArray.splice(elementIndex,1);
	        // this.valArray = this.valArray.myRemoveAt(elementIndex);
	    	this.valArray.splice(elementIndex,1);
	    }  
	    return ;
	}
	function size() {
	    return (this.keyArray.length);
	}
	function clear() {
        this.keyArray = new Array();
		this.valArray = new Array();
	}
	function keySet() {
	    return (this.keyArray);
	}
	function valSet() {
	    return (this.valArray);
	}
	function showMe() {
	    var result = "";
	    for( var i = 0; i < this.keyArray.length; i++ ) {
	        result += "Key: " + this.keyArray[ i ] + "\tValues: " + this.valArray[ i ] + "\n";
	    }
	    return result;
	}
	function findIt(key) {
	    var result = (-1);
	    for(var i = 0; i < this.keyArray.length; i++) {
	        if(this.keyArray[ i ] == key) {
	            result = i;
	            break;
	        }
	    }
	    return result;
	}
	function findIt4Val(val){
		var result = (-1);
	    for(var i = 0; i < this.valArray.length; i++) {
	        if(this.valArray[ i ] == val) {
	            result = i;
	            break;
	        }
	    }
	    return result; 
	}
}
function showSuccessMsg(id,data,callback,timeOut, message){
	var time = 3000;
	var msg = 'Lưu dữ liệu thành công';
	if (timeOut!= undefined && timeOut != null && timeOut>0) {
		time = timeOut;
	}
	if (message!= undefined && message != null && message != ''){
		msg = message;
	}
	if(!data.error){
		$('#' + id).html(msg).show();
		var tm = setTimeout(function(){
			$('#' + id).html('').hide();
			if (callback!= undefined && callback != null) {
				callback.call(this, true);
			}
			clearTimeout(tm);
		}, time);
	}	
}
function showMsgTimeOut(id,msg,time,callback){
	$('#' + id).html(msg).show();
	var tm = setTimeout(function(){
		$('#' + id).html('').hide();
		if (callback!= undefined && callback != null) {
			callback.call(this, true);
		}
		clearTimeout(tm);
	}, time);
}
function setTitleSearch(id){
	var objId = 'title';
	if(id!= undefined && id!= null && id.length > 0){
		objId = id;
	}
	$('#' + objId).html(unit_tree_search_unit_search_header);
}
function setTitleUpdate(id){
	var objId = 'title';
	if(id!= undefined && id!= null && id.length > 0){
		objId = id;
	}
	$('#' + objId).html(organization_information);
}
function setTitleAdd(id){
	var objId = 'title';
	if(id!= undefined && id!= null && id.length > 0){
		objId = id;
	}
	$('#' + objId).html('Thêm mới thông tin');
}
function formatDate(d){
	if(d != null || d != undefined){
		d = d.substring(0,10);
		d = d.split('-');
		d = d[2] + '/' + d[1] + '/' + d[0];
	}
	return d;
}
function formatMonth(d){
	if(d != null || d != undefined){
		d = d.substring(0,10);
		d = d.split('-');
		d = d[1] + '/' + d[0];
	}
	return d;
}
function gotoPage(link){
	window.location.href = link;
}
var loadedResources = new Array();
function loadResource(filename, filetype, callback){	
		if (filetype=="js"){ // if filename is a external JavaScript file
			var fileref=document.createElement('script');
			fileref.setAttribute("type","text/javascript");
			fileref.setAttribute("src", filename);
		}
		else if (filetype=="css"){ // if filename is an external CSS file
			var fileref=document.createElement("link");
			fileref.setAttribute("rel", "stylesheet");
			fileref.setAttribute("type", "text/css");
			fileref.setAttribute("href", filename);
		}
		if (typeof fileref != "undefined"){
			if(callback != undefined){
				if(fileref.readyState){
					fileref.onreadystatechange = function (){
						if (fileref.readyState == 'loaded' || fileref.readyState == 'complete'){
							callback();
						}
					};
				} else {
					fileref.onload = callback;
				}
			}
			document.getElementsByTagName("head")[0].appendChild(fileref);
		}
		loadedResources.push(filename);	
}
function hideColumnOfJqGrid(gridId,columnName){
	// $('#' + gridId).hideCol(columnName);
};
function checkPermissionEdit(){
	var status = $('#hidStatus').val();
	if(status !=undefined){
		if($('#hidStatus').val().trim()=='1'){
			return false;
		}
	}
	if($('#csPer').val().trim() == 'true' && $('#ePer').val().trim() == 'true'){
		return true;
	}
	return false;
}
function checkPermissionDev(){
	if(($('#dPer').val().trim() == 'true')&&($('#dEdit').val().trim() == 'true')){
		return true;
	}
	return false;
}
function bindAutoHideErrMsg(){
	$('.ui-jqgrid-view a').bind('click', function(){
		$('.ErrorMsgStyle').each(function(){
			if(!$(this).is(':hidden')){
				$(this).html('').hide();
			}
		});		
	});
}
function setComboValue(objectId,defaultValue,prefix){
	var value = -2;
	var prefixTmp ='';
	if(defaultValue!= undefined && defaultValue!= null){
		value = defaultValue;
	}
	if(prefix != undefined && prefix!= null && prefix != ''){
		prefixTmp = prefix;
	}
	$(prefixTmp +'#' + objectId).combobox('setValue',value);
}
function disableSelectbox(id){
	disabled(id);
	$('#' + id).change();
	$('#' + id).parent().addClass('BoxDisSelect');
}
function enableSelectbox(id){
	enable(id);
	$('#' + id).change();
	$('#' + id).parent().removeClass('BoxDisSelect');
}
function disableCombo(id){
	$('#' + id).combobox('disable');
	$('#' + id).parent().addClass('BoxDisSelect');
}
function enableCombo(id){
	$('#' + id).combobox('enable');
	$('#' + id).parent().removeClass('BoxDisSelect');
}
function disableComboTree(id){
	$('#' + id).combotree('disable');  
	$('#' + id).parent().addClass('BoxDisSelect');
}
function enableComboTree(id){
	$('#' + id).combotree('enable');  
	$('#' + id).parent().removeClass('BoxDisSelect');
}
function showCodeAndName(code , name) {
	if(isNullOrEmpty(code)&&!isNullOrEmpty(name)) {
		return name;
	}
	if(!isNullOrEmpty(code)&&isNullOrEmpty(name)) {
		return code;
	}
	if(!isNullOrEmpty(code)&&!isNullOrEmpty(name)) {
		return code +  ' - ' + name ;
	}
	return '';
}
function fromKeyCode (n) {
	if( 47<=n && n<=90 ) return unescape('%'+(n).toString(16));
	if( 96<=n && n<=105) return 'NUM '+(n-96);
	if(112<=n && n<=135) return 'F'+(n-111);

	if(n==3)  return 'Cancel'; // DOM_VK_CANCEL
	if(n==6)  return 'Help';   // DOM_VK_HELP
	if(n==8)  return 'Backspace';
	if(n==9)  return 'Tab';
	if(n==12) return 'NUM 5';  // DOM_VK_CLEAR
	if(n==13) return 'Enter';
	if(n==16) return 'Shift';
	if(n==17) return 'Ctrl';
	if(n==18) return 'Alt';
	if(n==19) return 'Pause|Break';
	if(n==20) return 'CapsLock';
	if(n==27) return 'Esc';
	if(n==32) return 'Space';
	if(n==33) return 'PageUp';
	if(n==34) return 'PageDown';
	if(n==35) return 'End';
	if(n==36) return 'Home';
	if(n==37) return 'Left Arrow';
	if(n==38) return 'Up Arrow';
	if(n==39) return 'Right Arrow';
	if(n==40) return 'Down Arrow';
	if(n==42) return '*'; // Opera
	if(n==43) return '+'; // Opera
	if(n==44) return 'PrntScrn';
	if(n==45) return 'Insert';
	if(n==46) return 'Delete';

	if(n==91) return 'WIN Start';
	if(n==92) return 'WIN Start Right';
	if(n==93) return 'WIN Menu';
	if(n==106) return '*';
	if(n==107) return '+';
	if(n==108) return 'Separator'; // DOM_VK_SEPARATOR
	if(n==109) return '-';
	if(n==110) return '.';
	if(n==111) return '/';
	if(n==144) return 'NumLock';
	if(n==145) return 'ScrollLock';

	// Media buttons (Inspiron laptops)
	if(n==173) return 'Media Mute On|Off';
	if(n==174) return 'Media Volume Down';
	if(n==175) return 'Media Volume Up';
	if(n==176) return 'Media >>';
	if(n==177) return 'Media <<';
	if(n==178) return 'Media Stop';
	if(n==179) return 'Media Pause|Resume';

	if(n==182) return 'WIN My Computer';
	if(n==183) return 'WIN Calculator';
	if(n==186) return '; :';
	if(n==187) return '= +';
	if(n==188) return ', <';
	if(n==189) return '- _';
	if(n==190) return '. >';
	if(n==191) return '/ ?';
	if(n==192) return '\` ~';
	if(n==219) return '[ {';
	if(n==220) return '\\ |';
	if(n==221) return '] }';
	if(n==222) return '\' "';
	if(n==224) return 'META|Command';
	if(n==229) return 'WIN IME';

	if(n==255) return 'Device-specific'; // Dell Home button (Inspiron
											// laptops)

	return null;
	}
function updateRownumWidthForJqGrid(parentId){	
	var pId = '';
	if(parentId!= null && parentId!= undefined){
		pId = parentId + ' ';
	}
	if($(pId + '.jqgrid-rownum').last()!= null){
		var lastValue = $(pId + '.jqgrid-rownum').last().text().trim();		
		if(lastValue.length > 0){
			var extWidth = 25;
			if(lastValue.length > 2){
				extWidth += (lastValue.length - 2) * 9;
			}  
			$(pId +  '.jqgfirstrow td').first().css('width',extWidth);
			$(pId + 'tr[role=rowheader] th').first().css('width',extWidth);
		}
	}
}
function updateRownumWidthForDataGrid(parentId){	
	var pId = '';
	if(parentId!= null && parentId!= undefined){
		pId = parentId + ' ';
	}
	var lastValue=$(pId + '.datagrid-cell-rownumber').last().text().trim().length;
	if(lastValue > 0){
		var extWidth = 25;
		if(lastValue > 2){
			extWidth += (lastValue - 2) * 9;
		}  
		$(pId + '.datagrid-cell-rownumber').css('width',extWidth);
		$(pId + '.datagrid-header-rownumber').css('width',extWidth);
	}
}
function escapeSpecialChar(content){
	if(content!= ""){
		content += "";
		content = content.replace(/\'/g,"&apos;").replace(/\"/g,"&quot;").replace(/</g,"&lt;").replace(/>/g,"&gt;").replace(/>/g,"&amp;");
	}
	return content;
}

function convertDate(txtDate, separator){
	// if separator is not defined then set '/'
    if (separator == undefined) {
        separator = '/';
    }
    // split input date to month, day and year
    aoDate = txtDate.split(separator);
    // array length should be exactly 3 (no more no less)
    if (aoDate.length !== 3) {
        return false;
    }
    // define month, day and year from array (expected format is m/d/yyyy)
    // subtraction will cast variables to integer implicitly
    day = aoDate[0] - 0; // because months in JS start from 0
    month = aoDate[1] - 1;
    year = aoDate[2] - 0;
    // test year range
    if (year < 1000 || year > 3000) {
        return null;
    }
    // convert input date to milliseconds
    ms = (new Date(year, month, day)).getTime();
    // initialize Date() object from milliseconds (reuse aoDate variable)
    aoDate = new Date();
    aoDate.setTime(ms);
    // compare input date and parts from Date() object
    // if difference exists then input date is not valid
    if (aoDate.getFullYear() !== year ||
        aoDate.getMonth() !== month ||
        aoDate.getDate() !== day) {
        return null;
    }
    // date is OK, return true
    return aoDate;
}
function toDateString(d){
	var dStr= (d.getDate().toString().length>1?d.getDate():'0'+d.getDate())+'/'+((d.getMonth()+1).toString().length>1?(d.getMonth()+1):'0'+(d.getMonth()+1))+'/'+d.getFullYear();
	return dStr;
}
function getNumWeekOfYear(txtDate){
	var date = convertDate(txtDate, '/');
	return getWeekOfYear(date);
}
function toMonthYearString(d){
	var dStr= ((d.getMonth()+1).toString().length>1?(d.getMonth()+1):'0'+(d.getMonth()+1))+'/'+d.getFullYear();
	return dStr;
}
function getWeekOfYear(d) {
	// Create a copy of this date object
	var target = new Date(d.valueOf());
	console.log(target);
	// ISO week date weeks start on monday
	// so correct the day number
	var dayNr = (d.getDay() + 6) % 7;
	console.log(target);
	// Set the target to the thursday of this week so the
	// target date is in the right year
	target.setDate(target.getDate() - dayNr + 3);
	// ISO 8601 states that week 1 is the week
	// with january 4th in it
	var jan4 = new Date(target.getFullYear(), 0, 4);
	// Number of days between target date and january 4th
	var dayDiff = (target - jan4) / 86400000;
	// Calculate week number: Week 1 (january 4th) plus the
	// number of weeks between target date and january 4th
	var weekNr = 1 + Math.ceil(dayDiff / 7);
	return weekNr;
}
function loadDataForStaffTree(url,treeId){
	var tId = 'tree';
	if(treeId!= null && treeId!= undefined){
		tId = treeId;
	}
	$('#' + tId).jstree({
        "plugins": ["themes", "json_data","ui"],
        "themes": {
            "theme": "classic",
            "icons": false,
            "dots": true
        },
        "json_data": {
        	"ajax" : {
                "url" : '/rest/report/staff-for-shop/tree/'+$('#shopId').val()+'.json',
                "data" : function (n) {
                        return { id : n.attr ? n.attr("id") : 0, lhl: !$('#cbLHL').is(':disabled') && $('#cbLHL').is(':checked')?1:0 };
                    }
            }        	
        }
	});	
}
function getFirstDayOfWeek(week,year){
	var dateTmp;
	if(week == null || week == undefined || week ==''){
		return '';
	}
	if(year == null || year == undefined || year == ''){
		var currentDate =new Date();
		dateTmp = new Date (currentDate.getFullYear(),0,1);
	} else{
		dateTmp = new Date(year.toString(),0,1); // toString first so it
													// parses correctly year
													// numbers
	}
	var daysToFirstDay = (1 - dateTmp.getDay()); // Note that this can be
													// also negative
	var firstDayOfFirstWeek = new Date(dateTmp.getTime() + daysToFirstDay * 86400000);
	var firstDate = new Date(firstDayOfFirstWeek.getTime() + (7 * (week - 1) * 86400000));
	return toDateString(firstDate);
};
//@author hunglm16; @since April 14, 2014; @description Lay ngay so voi ngay hien bang khoang cach ngay (-: ngay truoc; +: ngay sau) 
function getNextBySysDateForNumber(number){
	var dateTmp;
	var result = null;
	if(number == null || number == undefined || number ==''){
		number = 0;
	}
	var currentDate = new Date();
	if(number > 0 || number <0){
		result = new Date(currentDate.getTime() + number* 86400000);
	}else{
		var kq = Utils.currentDate();
		return kq.toString();
	}
	return toDateString(result);
};

function hrefPost(URL, PARAMS) {
    var temp=document.createElement("form");
    temp.action=URL;
    temp.method="POST";
    temp.style.display="none";
    for(var x in PARAMS) {
          var opt=document.createElement("textarea");
          opt.name=x;
          opt.value=PARAMS[x];
          temp.appendChild(opt);
    }
    document.body.appendChild(temp);
    temp.submit();
    return temp;
};
var MaskManager= {
	maskDate: function(selector){
		$(selector).mask("99/99/9999");
	},
	maskMonth: function(selector){
		$(selector).mask("99/9999");
	}	
};
function applyMonthPicker(selector) {
	var date = new Date();
	var cYear = date.getFullYear();
	var options = {
	    selectedYear: cYear,
	    startYear: cYear - 3,
	    finalYear: cYear + 7,
	    openOnFocus: true,
	    //monthNames: ['T1', 'T2', 'T3', 'T4', 'T5', 'T6', 'T7', 'T8', 'T9', 'T10', 'T11', 'T12']
	    monthNames: [jsp_common_thang_mot, jsp_common_thang_hai, jsp_common_thang_ba, jsp_common_thang_bon, jsp_common_thang_nam, jsp_common_thang_sau, jsp_common_thang_bay, jsp_common_thang_tam, jsp_common_thang_chin, jsp_common_thang_muoi, jsp_common_thang_muoimot, jsp_common_thang_muoihai]
	};
	
	var addHtml = '<a href="javascript:void(0);" class="CalendarLink"><img src="/resources/images/icon_calendar.jpg" width="15" height="16" /></a>';
	$('#'+selector).after(addHtml);
	$('#'+selector).monthpicker(options);
	$('#'+selector).monthpicker().bind('monthpicker-change-year', function (e, year) {
	});
	$('#'+selector).next().bind('click', function () {
		$('#'+selector).monthpicker('show');
	});
};
/**vuongmq; add them selector of nam;
 * selector: id cua input,
 * number: so nam cong them
 * flag: load lai khi mac dinh click lai dialog
 * */
function applyMonthPickerDialog(selector, numberYear, flag) {
	var date = new Date();
	if(numberYear == undefined || numberYear == null){
		numberYear = 0;
	}
	var cYear = date.getFullYear() + numberYear;
	var options = {
	    selectedYear: cYear,
	    startYear: cYear - 3,
	    finalYear: cYear + 7,
	    openOnFocus: true,
	    //monthNames: ['T1', 'T2', 'T3', 'T4', 'T5', 'T6', 'T7', 'T8', 'T9', 'T10', 'T11', 'T12']
	    monthNames: [jsp_common_thang_mot, jsp_common_thang_hai, jsp_common_thang_ba, jsp_common_thang_bon, jsp_common_thang_nam, jsp_common_thang_sau, jsp_common_thang_bay, jsp_common_thang_tam, jsp_common_thang_chin, jsp_common_thang_muoi, jsp_common_thang_muoimot, jsp_common_thang_muoihai]
	};
	if(flag == undefined || (flag != null && flag == true )){
		var addHtml = '<a href="javascript:void(0);" class="CalendarLink"><img src="/resources/images/icon_calendar.jpg" width="15" height="16" /></a>';
		$('#'+selector).after(addHtml);
	}
	$('#'+selector).monthpicker(options);
	$('#'+selector).monthpicker().bind('monthpicker-change-year', function (e, year) {
	});
	$('#'+selector).next().bind('click', function () {
		$('#'+selector).monthpicker('show');
	});
};
function toTimeString(d){
	var isOpera = !!window.opera || navigator.userAgent.indexOf(' OPR/') >= 0;
    // Opera 8.0+ (UA detection to detect Blink/v8-powered Opera)
	var isFirefox = typeof InstallTrigger !== 'undefined';   // Firefox 1.0+
	var isSafari = Object.prototype.toString.call(window.HTMLElement).indexOf('Constructor') > 0;
    // At least Safari 3+: "[object HTMLElementConstructor]"
	var isChrome = !!window.chrome && !isOpera;              // Chrome 1+
	var isIE = /*@cc_on!@*/false || !!document.documentMode; // At least IE6

	var dStr = '';
	
	if(isFirefox) {
		dStr = (d.getHours().toString().length>1?d.getHours():'0'+d.getHours())+':'+(d.getMinutes().toString().length>1?d.getMinutes():'0'+d.getMinutes());
	} else {
		dStr= (d.getUTCHours().toString().length>1?d.getUTCHours():'0'+d.getUTCHours())+':'+(d.getUTCMinutes().toString().length>1?d.getUTCMinutes():'0'+d.getUTCMinutes());
	}
	
	return dStr;
}
//quan ly mau bao cao: reportManagerPage.jsp
function formatStringUrl (str) {
	var a= str.length -1;
	for(var i = str.length ; i > 0 ; i--) {
	    if(str[a] == '/') {
	        break;
	    }
	    a--;
	}
	return str.substring(a+1,str.length);
}

function getFormatterControlGrid(id, data){
	if(_isFullPrivilege){
		return data;
	}else{
		var status = parseInt(_MapControl.get(id));
		if(status == 1){
			return '';
		}else if(status == 2){
			return data;
		}else if(status == 3){
			return '';
		}
	}
}

function showIconAddOfFirstControlGrid(idControl, idAddIconControl){
	if(_isFullPrivilege){
		$('#' + idAddIconControl).show();
	}else{
		var status = parseInt(_MapControl.get(idControl));
		if(status == 1){
			$('#' + idAddIconControl).hide();
		}else if(status == 2){
			$('#' + idAddIconControl).show();
		}else if(status == 3){
			$('#' + idAddIconControl).hide();
		}
	}
}

/**
 * @author sangtn
 * @description Kiểm tra control này có phân quyền enable hay ko, nếu có thì được phép enable, nếu ko thì ko thay đổi
 * @params: 
 * @note Hàm này để thay thế hàm enable hiện tại
 * */
function enableWidthAuthorize(idControl){
	if(_MapControl != undefined && _MapControl != null && _MapControl.valArray.length > 0){
		if(_MapControl.get(idControl) != null){
			var status = parseInt(_MapControl.get(idControl));
			if(status == 1){				
				
			}else if(status == 2){
				enable(idControl);
			}else if(status == 3){
				
			}
		}else{
			enable(idControl); 
		}
	}
}

var sortArray = function(arr){
	//Sap xep bang thuat toan nhi phan Quicksort
	//@author HungLM16; @retunr Array; @description Sap xep ASC
	var quickSort = function(arr){
		var left = [];
		var right = [];

		if(!$.isArray(arr)){
			return "yêu cầu nhập và phải là mảng Array";
		}

		if (arr.length <= 1) return arr;

        var pivot = arr[0];
        arr.shift();
        for (var i = 0; i < arr.length; i++) {
            arr[i] <= pivot ? left.push(arr[i]) : right.push(arr[i]);
        }
        return $.merge($.merge(quickSort(left), $.makeArray(pivot)), quickSort(right));
    };

    return {
        quickSort: quickSort
    };
    
    //cach goi ham : var kq = sortArray.quickSort(arr);
}();

/**
 * copy all data-properties from source-object to destination-object
 * @author tuannd20
 * @param source
 * @param destination
 * @returns
 * @since 26/09/2014
 */
function copyObjectProperties(source, destination) {
	if (source !== null && source !== undefined) {
		for (var p in source) {
			destination[p] = source[p];
		}
	} else {
		destination = source;
	}
}

function convertToSimpleObject(out, obj, prefix) {
	if (obj instanceof Array){
        for (var index=0; index < obj.length; index++){
            var item = obj[index];
            var tmpPrefix = prefix + "[" + index + "]";
            if (item instanceof Array || item instanceof Object){
                arguments.callee(out, item, tmpPrefix);
            } else {
                out[tmpPrefix] = item;
            }
        }
    } else if (obj instanceof Object){
        for(var propName in obj){	            
            var tmpPrefix = prefix + "." + propName;
            if (!(obj[propName] instanceof Array || obj[propName] instanceof Object)){
                out[tmpPrefix] = obj[propName];
            } else {
                arguments.callee(out, obj[propName], tmpPrefix);
            }
        }
    }
}	

function convertToSimpleObject(out, obj, prefix){
    if (obj instanceof Array){
        for (var index=0; index < obj.length; index++){
            var item = obj[index];
            var tmpPrefix = prefix + "[" + index + "]";
            if (item instanceof Array || item instanceof Object){
                arguments.callee(out, item, tmpPrefix);
            } else {
                out[tmpPrefix] = item;
            }
        }
    } else if (obj instanceof Object){
        for(var propName in obj){
			if (propName.toString() !== Utils.VAR_NAME){
				var tmpPrefix = prefix + "." + propName;
				if (!(obj[propName] instanceof Array || obj[propName] instanceof Object)){
					out[tmpPrefix] = obj[propName];
				} else {
					arguments.callee(out, obj[propName], tmpPrefix);
				}
			}
        }
    }
}

function getPrefix(data){
	var prefix = null;
	try{
		data.hasOwnProperty(Utils.VAR_NAME);
	} catch(e){
		throw 'Please check your data or browser support.';
	}
	if (data.hasOwnProperty(Utils.VAR_NAME)){
		try{
			prefix = data[Utils.VAR_NAME];
			delete data[Utils.VAR_NAME];
		} catch(e){
		}
	} else {
		throw 'Missing "_varname_". Please check your data.';
	}
	if (prefix === null || prefix === undefined || prefix.toString().trim().length === 0){
		throw 'Missing "_varname_". Please check your data.';
	}
	return prefix;
}

function getSimpleObject(data){
	var prefix = getPrefix(data);
	var out = {};
	convertToSimpleObject(out, data, prefix);
	return out;
}
function formatString(str, arr) {
	if(	arr instanceof Array && arr.length != 0){
		for(var i = 0 ; i < arr.length ; i++) {
			str = str.replace('{' + i + '}', arr[i]);
		}
	} else {
		str = str.replace('{0}', arr);
	}
	return str;
}
function hideAllMessage(){//trungtm6
	$('.ErrorMsgStyle,.SuccessMsgStyle').each(function(index){
		$(this).html('').hide();
	});
}
function previewImportExcelFile(fileObj,formId,inputText){//tientv11
	// if($('#btnImport').length != 0) {
	// disabled('btnImport');
	// }
		var inputId = 'fakefilepc';
		if(inputText != null || inputText != undefined){
			inputId = inputText;
		}
		var fileTypes=["xls","xslx"];
		if (fileObj.value != '') {
			if (!/(\.xls|\.xlsx)$/i.test(fileObj.value)) {
				$('#errExcelMsg').html(msgErr_excel_file_invalid_type).show();
				fileObj.value = '';
				fileObj.focus();
				document.getElementById(inputId).value ='';			
				return false;
			} else {
				$('#errExcelMsg').html('').hide();
				var src = '';
				if ($.browser.msie){
				    var path = encodeURI(what.value);
				    path = path.replace("C:%5Cfakepath%5C","");
				    var path = decodeURI(path);
				    document.getElementById(inputId).value = path;
				}else{
					document.getElementById(inputId).value = fileObj.value;
				}	
				// $('#' + formId).submit();
				// return true;
			}
		}else{
			$('#errExcelMsg').html('Vui lòng chọn file Excel').show();		
			return false;
		}
		return true;
}

/**
 * Thay the ki tu ' thanh \', " thanh \"
 *
 * @author lacnv1
 * @param val - gia tri can escape
 * @since Nov 23, 2015
 */
function escapeQuot(val) {
	if (val == null || val.trim().length == 0) {
		return val;
	}
	return val.replace(/'/g, "\\'").replace(/"/g, "\\\"");
}
var JSONUtil = (function(){
	//var instance;
	
	//var createUtil = function() {
		var _VAR_NAME = "_varname";
		var _xhrSave = null;
		
		var convertToSimpleObject = function(out, obj, prefix) {
		    if (obj instanceof Array) {
		        for (var index=0; index < obj.length; index++) {
		            var item = obj[index];
		            var tmpPrefix = prefix + "[" + index + "]";
		            if (item instanceof Array || item instanceof Object){
		                arguments.callee(out, item, tmpPrefix);
		            } else if (item != undefined && item != null) {
		                out[tmpPrefix] = item;
		            }
		        }
		    } else if (obj instanceof Object){
		        for(var propName in obj){
					if (propName.toString() !== _VAR_NAME){
						var tmpPrefix;
						if (prefix && prefix.length > 0) {
							tmpPrefix = prefix + "." + propName;
						} else {
							tmpPrefix = propName;
						}
						var item = obj[propName];
						if (item instanceof Array || item instanceof Object){
							arguments.callee(out, item, tmpPrefix);
						} else if (item != undefined && item != null) {
							out[tmpPrefix] = item;
						}
					}
		        }
		    }
		};
		
		var getPrefix = function(data) {
			var prefix = null;
			try{
				if (data.hasOwnProperty(_VAR_NAME)){
					prefix = data[_VAR_NAME];
					delete data[_VAR_NAME];
				} else {
					throw 'Missing "_varname_". Please check your data.';
				}
			} catch(e){
				throw 'Please check your data or browser support.';
			}
			if (prefix === null || prefix === undefined || prefix.toString().trim().length === 0){
				throw 'Missing "_varname_". Please check your data.';
			}
			return prefix;
		};
		
		var getSimpleObject = function(data) {
			var prefix = getPrefix(data);
			var out = {};
			convertToSimpleObject(out, data, prefix);
			return out;
		};
		
		var getSimpleObject2 = function(data) {
			var out = {};
			convertToSimpleObject(out, data, "");
			return out;
		};
		
		var saveData = function(dataModel, url, msg, errMsgId, callback, prefix, callbackFail, hideSuccessMsg) {
			$.messager.confirm(jsp_common_xacnhan, msg, function(r) {
				if (r) {
					var data = getSimpleObject(dataModel);
					Utils.saveData(data, url, _xhrSave, errMsgId, callback, "loading2", prefix, false, callbackFail, hideSuccessMsg);
				}
			});
		};
		
		var saveData2 = function(dataModel, url, msg, errMsgId, callback, prefix, callbackFail, hideSuccessMsg) {
			$.messager.confirm(jsp_common_xacnhan, msg, function(r) {
				if (r) {
					var data = getSimpleObject2(dataModel);
					Utils.saveData(data, url, _xhrSave, errMsgId, callback, "loading2", prefix, false, callbackFail, hideSuccessMsg);
				}
			});
		};
		
		return {
			getSimpleObject: getSimpleObject,
			getSimpleObject2: getSimpleObject2,
			saveData: saveData,
			saveData2: saveData2,
			_VAR_NAME: _VAR_NAME
		};
	//};
	
	/*return {
		getInstance: function() {
			if (!instance) {
				instance = createUtil();
			}
			return instance;
		}
	};*/
})();
//var JSONUtil = CJSONUtil.getInstance();
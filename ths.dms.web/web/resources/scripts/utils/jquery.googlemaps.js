/**
 * Google Maps APIs
 * @author tientv11
 * @sine 19/06/2014
 */
var ViettelMap = {
	_DEF_LAT: 10.79418775,
	_DEF_LNG: 106.65682978,
	_DEF_LAT_BIG:  15.744675,
	_DEF_LNG_BIG: 107.515136,
	_DEF_ALL_ZOOM: 4,
	_DEF_ZOOM: 10,
	_DEF_ZOOM_BIG: 6,
	_map: null,
	_marker: null,
	_listMarker: null,
	_indexMarker: 0,
	sellerPositionMap: null,
	visitResultMap:null,
	_currentInfoWindow:null,
	_listOverlay:null,
	loadedResources:new Array(),
	OVERLAY_ZINDEX : 9,
	OVERLAY_ZINDEX_DEFAULT : 1,
	_info: null,
	getBrowserName : function() {
		var browserName = "";
		var ua = navigator.userAgent.toLowerCase();
		if (ua.indexOf("opera") != -1)
			browserName = "opera";
		else if (ua.indexOf("msie") != -1)
			browserName = "msie";
		else if (ua.indexOf("safari") != -1)
			browserName = "safari";
		else if (ua.indexOf("mozilla") != -1) {
			if (ua.indexOf("firefox") != -1)
				browserName = "firefox";
			else
				browserName = "mozilla";
		}
	},
	loadMapForImages: function(objId, lat, lng, zoom,type, custLat, custLng){
		if(!ViettelMap.isValidLatLng(lat,lng)){
			lat = ViettelMap._DEF_LAT_BIG;
			lng = ViettelMap._DEF_LNG_BIG;
			showMarker = false;
			if(zoom == null || zoom == undefined) {
				zoom = ViettelMap._DEF_ZOOM_BIG;
			}
		} else if(zoom == undefined || zoom == 0 || zoom == null){
			zoom = ViettelMap._DEF_ZOOM_BIG;
		}
		ViettelMap.clearOverlays();
		$('.fancybox-inner #bigMap').html('');
		var __object = $('.fancybox-inner #bigMap')[0];
		ViettelMap._map = new google.maps.Map(__object);
		ViettelMap._map.setOptions({
			overviewMapControl : false,
			center: new google.maps.LatLng(lat, lng),
			zoom: zoom
		});
		var map = ViettelMap._map; 
		//var showMarker = true;
		ViettelMap.addMarkerImages(lat,lng, custLat, custLng);
		//var pt = new google.maps.LatLng(lat, lng);
	    //map.setCenter(pt);
	    //map.setZoom(zoom);
	    if(type != undefined && type != null && type == 1){//bản đồ mở rộng
	    	var mapOptions = {
		    		panZoomControl: enable,
		    		panZoomControlOptions : {
		    			position: google.maps.ControlPosition.RIGHT_TOP
		    		}
		    	};
	    	map.setOptions(mapOptions);
	    }
		return map;
	},	
	loadMapResource:function(callback){
		VTMapUtil.loadResource(google_map_url, 'js',callback);
	},
	loadResource:function(filename, filetype, callback){
		/*if ($.inArray(filename, VTMapUtil.loadedResources) == -1){
			if (filetype=="js"){ // if filename is a external JavaScript file
				var fileref=document.createElement('script');
				fileref.setAttribute("type","text/javascript");
				fileref.setAttribute("src", filename);
			}
			if (typeof fileref != "undefined"){
				if(callback != undefined){
					if(fileref.readyState){
						fileref.onreadystatechange = function (){
							if (fileref.readyState == 'loaded' || fileref.readyState == 'complete'){
								callback();
							}
						};
					} else {
						fileref.onload = callback;
					}
				}
				document.getElementsByTagName("head")[0].appendChild(fileref);
			}
			VTMapUtil.loadedResources.push(filename);
		} else {
			if(callback != undefined){
				callback();
			}
		}*/
		if(callback != undefined){
			callback();
		}
	},
	/** Kiem tra LatLng */
	isValidLatLng : function(lat,lng){
		if(lat==undefined || lng == undefined || lat== null || lng== null || lat == 0.0 || lng ==0.0 || lat<=0 || lng<=0){
			return false;
		}
		return true;
	},
	loadBigMap: function(objId, lat, lng, zoom,callback){
		ViettelMap._map = new google.maps.Map(document.getElementById(objId));
		var map = ViettelMap._map; 
		var showMarker = true;
		if(!ViettelMap.isValidLatLng(lat,lng)){
			lat = ViettelMap._DEF_LAT_BIG;
			lng = ViettelMap._DEF_LNG_BIG;
			showMarker = false;
			if(zoom == null || zoom == undefined) {
				zoom = ViettelMap._DEF_ZOOM_BIG;
			}
		} else if(zoom == undefined || zoom == 0 || zoom == null){
			zoom = ViettelMap._DEF_ZOOM_BIG;
		}
		var pt = new google.maps.LatLng(lat, lng);
	    map.setCenter(pt);
	    map.setZoom(zoom);
	    var mapOptions = {
	    		zoomControl : true,
	    		zoomControlOptions : {
	    			position: google.maps.ControlPosition.RIGHT_CENTER
	    		},
	    		panControl : true,
	    		panControlOptions : {
	    			position: google.maps.ControlPosition.RIGHT_CENTER
	    		}
	    	};
    	map.setOptions(mapOptions);
	    if(ViettelMap.isValidLatLng(lat,lng) && showMarker){
	    	var marker = new google.maps.Marker({
				position : pt,
				map : ViettelMap._map,				
				draggable : false				
			});			
		    if(ViettelMap._listOverlay == null || ViettelMap._listOverlay == undefined) {
		    	ViettelMap._listOverlay = new Array();
		    	ViettelMap._listOverlay.push(marker);
			} else {
				ViettelMap._listOverlay.push(marker);
			}
	    }
	    google.maps.event.addListener(map, 'zoom_changed', function() {
	    	ViettelMap.hideShowTitleMarker();
	    });
	    google.maps.event.addListener(map, "click", function(evt) {	    	
			var lat = evt.latLng.lat();
			var lng= evt.latLng.lng();
			if (callback != null)
				callback(lat, lng);
		});
		return map;	    
	},
	loadBigMapEx: function(objId, lat, lng, zoom,callback,clickable, isNotLoadMap){
		if (isNotLoadMap == undefined || isNotLoadMap == null || !isNotLoadMap) {
			ViettelMap._map = new google.maps.Map(document.getElementById(objId));
		}
		if (ViettelMap._marker != null) {
        	ViettelMap._marker.setMap(null);
        }
		var map = ViettelMap._map; 
		var showMarker = true;
		if(!ViettelMap.isValidLatLng(lat,lng)){
			lat = ViettelMap._DEF_LAT_BIG;
			lng = ViettelMap._DEF_LNG_BIG;
			showMarker = false;
			if(zoom == null || zoom == undefined) {
				zoom = ViettelMap._DEF_ZOOM_BIG;
			}
		} else if(zoom == undefined || zoom == 0 || zoom == null){
			zoom = ViettelMap._DEF_ZOOM_BIG;
		}
		var pt = new google.maps.LatLng(lat, lng);
	    map.setCenter(pt);
	    map.setZoom(zoom);
	    var mapOptions = {
	    		zoomControl : true,
	    		zoomControlOptions : {
	    			position: google.maps.ControlPosition.RIGHT_CENTER
	    		},
	    		panControl : true,
	    		panControlOptions : {
	    			position: google.maps.ControlPosition.RIGHT_CENTER
	    		}
	    	};
    	map.setOptions(mapOptions);
    	var marker = new google.maps.Marker({
			position : pt,
			map : showMarker ? ViettelMap._map : null,				
			draggable : false				
		});
		ViettelMap._marker = marker;
	    if (showMarker) {
	    	marker.lat = lat;
			marker.lng = lng;	    				
		    if(ViettelMap._listOverlay == null || ViettelMap._listOverlay == undefined) {
		    	ViettelMap._listOverlay = new Array();
		    	ViettelMap._listOverlay.push(marker);
			} else {
				ViettelMap._listOverlay.push(marker);
			}
	    }
	    
	    google.maps.event.addListener(map, 'zoom_changed', function() {
	    	ViettelMap.hideShowTitleMarker();
	    });
	    if (isNotLoadMap == undefined || isNotLoadMap == null || !isNotLoadMap) {
		    google.maps.event.addListener(map, "click", function(evt) {	    	
				var lat = evt.latLng.lat();
				var lng= evt.latLng.lng();
				 if (clickable != undefined && clickable != null && clickable &&
		            		callback != undefined && callback != null) {
					var pt = new google.maps.LatLng(lat, lng);
					ViettelMap._marker.setMap(null);
					marker = new google.maps.Marker({
							position : pt,
							map : ViettelMap._map,				
							draggable : false				
					 });	
					ViettelMap._marker = marker;
		             callback(lat, lng);
		          }
			});
		}
		return map;	    
	},
	clearOverlays: function() { 
		if(ViettelMap._listOverlay != null && ViettelMap._listOverlay != undefined) {
			if(ViettelMap._listOverlay instanceof Map) {
				for(var i = 0; i < ViettelMap._listOverlay.size(); i++) {
					var obj = ViettelMap._listOverlay.valArray[i];
					obj.setMap(null);
				}
			} else if(ViettelMap._listOverlay instanceof Array) {
				for(var i = 0; i < ViettelMap._listOverlay.length; i++) {
					var obj = ViettelMap._listOverlay[i];
					obj.setMap(null);
				}
			}
		}
		ViettelMap._listOverlay = null;
	},
	fitOverLay: function() {
		var bound = null;
		if(ViettelMap._listOverlay != null && ViettelMap._listOverlay != undefined) {
			if(ViettelMap._listOverlay instanceof Map) {
				for(var i = 0; i < ViettelMap._listOverlay.size(); i++) {
					var obj = ViettelMap._listOverlay.get(ViettelMap._listOverlay.keyArray[i]);
					if(obj instanceof google.maps.MarkerWithLabel) {
						if(ViettelMap.isValidLatLng(obj.getPosition().lat(), obj.getPosition().lng())) {
							var latlng = new google.maps.LatLng(obj.getPosition().lat(), obj.getPosition().lng());
							bound = ViettelMap.getBound(bound, latlng);
						}
					} else {
						if(ViettelMap.isValidLatLng(obj.lat, obj.lng)) {
							var latlng = new google.maps.LatLng(obj.lat, obj.lng);
							bound = ViettelMap.getBound(bound, latlng);
						}
					}
				}
			} else if(ViettelMap._listOverlay instanceof Array) {
				for(var i = 0; i < ViettelMap._listOverlay.length; i++) {
					var obj = ViettelMap._listOverlay[i];
//					if(obj instanceof google.maps.MarkerWithLabel) {
						if(ViettelMap.isValidLatLng(obj.getPosition().lat(), obj.getPosition().lng())) {
							var latlng = new google.maps.LatLng(obj.getPosition().lat(), obj.getPosition().lng());
							bound = ViettelMap.getBound(bound, latlng);
						}
//					} else {
//						if(ViettelMap.isValidLatLng(obj.lat, obj.lng)) {
//							var latlng = new google.maps.LatLng(obj.lat, obj.lng);
//							bound = ViettelMap.getBound(bound, latlng);
//						}
//					}
				}
			}
		}
		if(bound != null) {
			ViettelMap._map.fitBounds(bound);
		}
	},
	fitOverLays: function() { 
		if (ViettelMap._listOverlay == null) return;
		var bounds = new google.maps.LatLngBounds();
		for(i=0;i<ViettelMap._listOverlay.length;i++) {
			bounds.extend(ViettelMap._listOverlay[i].getPosition());
		}

		ViettelMap._map.fitBounds(bounds);
	},
	addMarkerStaff:function(point){
		var map = ViettelMap._map;
		if(ViettelMap.isValidLatLng(point.lat, point.lng)) {
			var pt = new google.maps.LatLng(point.lat, point.lng);
			var info="<div id='info"+point.staffId+"'><img  src='/resources/images/loading-small.gif'/></div><br/>";
			var title=Utils.XSSEncode(point.staffCode+" - "+point.staffName);//+point.visit;
			var image=point.image;
			var accuracy = point.accuracy;
			var createTime = point.createTime;
			
			var level = ViettelMap._map.getZoom();
			/*var markerContent = ''; 
			var markerBotoom = "<div id='bottom' style='position: relative; bottom: -39px;text-align:center;'></div>";
			if(level >= 16) {
				markerBotoom = "<div id='bottom"+point.staffId+"' class='Bottom' style='position: relative; bottom: -39px; text-align:center;'><strong style='color:red;font-size: 12px;' class='titleMarker'>" + Utils.XSSEncode(title) + "</strong></div>";
			} else {
				markerBotoom = "<div id='bottom"+point.staffId+"' class='Bottom' style='position: relative; bottom: -39px; display:none;text-align:center;'><strong style='color:red;font-size: 12px;' class='titleMarker'>" + Utils.XSSEncode(title) + "</strong></div>";
			}
			markerContent = "<div id='marker"+point.staffId+"' style='height: 70px; width: 100px; left:-28px; position: relative; text-align: center; top: -41px;'>"+markerBotoom+"</div>";*/
			var markerContent = "<div id='top" + point.id + "' class='Top' style='text-align:center;position: relative'><strong style='color:red;font-size: 12px;' class='titleMarker'>" + title + "</strong></div>";
			var marker = new MarkerWithLabel({
				icon:{
					url : image,
					size : {height : 39, width : 20},
					scaledSize : {height : 39, width : 20}
				},
				position : pt,
				map : ViettelMap._map,
				draggable: false,
				labelContent: markerContent,
				labelClass: "MarkerLabel",
				labelVisible: true,
				labelAnchor: new google.maps.Point(25, 0)			
			});
			marker.staffId = point.staffId;
			marker.lat = point.lat;
			marker.lng = point.lng;
			marker.accuracy = point.accuracy;
			marker.createTime = point.createTime;
			marker.id = point.staffId;
			$('#marker'+point.staffId).parent().prev().css('z-index', 10000000);			
			google.maps.event.addListener(marker, "click", function(evt) {
				var point = this;
				ViettelMap.showWindowInfo(point.staffId, point.lat, point.lng, point.accuracy, point.createTime); 
			});
			ViettelMap.pushListOverlays(marker);
		}
		ViettelMap.hideShowTitleMarker();
	},
	showInfoWindow:function(pt,html, width){
		if(ViettelMap._currentInfoWindow != null && ViettelMap._currentInfoWindow != undefined) {
			ViettelMap._currentInfoWindow.close();
		}
		var infoWindow = new google.maps.InfoWindow({ 
					content: html, 
					position: pt,
					width: width
		});
		ViettelMap._currentInfoWindow = infoWindow;
		infoWindow.open(ViettelMap._map);
	},
	showWindowInfo:function(objectId,lat,lng,info,cur,time){		
		var pt = new google.maps.LatLng(lat, lng);
		if(objectId>0){
			/*** Begin Tam thoi; chi cho hien thi StaffSpecType.SUPERVISOR; StaffSpecType.STAFF*/
			var temp = SuperviseSales._lstStaffPosition.get(objectId);
			if (temp != null) {
				if (temp.roleType != StaffSpecType.SUPERVISOR && temp.roleType != StaffSpecType.STAFF) {
					return false;
				}
			}
			/*** End Tam thoi; chi cho hien thi StaffSpecType.SUPERVISOR; StaffSpecType.STAFF*/
			if(ViettelMap._currentInfoWindow != null && ViettelMap._currentInfoWindow != undefined) {
				ViettelMap._currentInfoWindow.close();
			}
			ViettelMap.showInfoWindow(pt,"<div style='width:100%;text-align: center;'><img style='position: relative;top:20px;' src='/resources/images/loading-small.gif'/></div><br/>");
			var temp = SuperviseSales._lstStaffPosition.get(objectId);
			if(temp!=null){
				SuperviseSales._idStaffSelect=objectId;
				//SuperviseSales.showDialogStaffInfo(temp.staffId,temp.staffCode,temp.staffName, temp.shopCode,temp.shopName,temp.createTime,temp.accuracy,pt);
				if (temp.roleType == StaffSpecType.MANAGER) { //TBHV; Vuongmq; 11/09/2015; chua su dung
					//SuperviseSales.showDialogTBHVInfo(temp.staffId,temp.staffCode,temp.staffName,temp.createTime,temp.accuracy,pt,temp.staffOwnerId);
				} else if (temp.roleType == StaffSpecType.SUPERVISOR) { // NVGS
					SuperviseSales.showDialogNVGSInfo(temp.staffId,temp.staffCode,temp.staffName,temp.createTime,temp.accuracy,pt,temp.staffOwnerId);
				} else if (temp.roleType == StaffSpecType.STAFF) { // NVBH
					SuperviseSales.showDialogNVBHInfo(temp.staffId,temp.staffCode,temp.staffName,temp.createTime,temp.accuracy,pt,temp.staffOwnerId);
				}
			}
		}
	},
	addMarkerImages:function(lat,lng){
		if(ViettelMap.isValidLatLng(lat, lng)) {
			var pt = new google.maps.LatLng(lat, lng);			
			var marker = new google.maps.Marker({
				position : pt,
				icon:{
					url : '/resources/images/Mappin/red.png',
					size : {height : 39, width : 20},
					scaledSize : {height : 39, width : 20}
				},
				map : ViettelMap._map,				
				draggable : false
			});			
			if(ViettelMap._listOverlay == null || ViettelMap._listOverlay == undefined) {
				ViettelMap._listOverlay = new Array();
				ViettelMap._listOverlay.push(marker);
			} else {
				ViettelMap._listOverlay.push(marker);
			}
		}
	},
	addMutilMarkerStaff:function(){
		var map = ViettelMap._map;
		if(ViettelMap._listMarker!=undefined && ViettelMap._listMarker!=null){
			var flag=0;
			var index=0;
			SuperviseSales._itv =window.setInterval(function(){
				var j=0;
				for(var i=index,j=0;i<ViettelMap._listMarker.valArray.length;i++,index++,j++){
					if(j>100) break;
					var point = ViettelMap._listMarker.valArray[i];
					if(ViettelMap.isValidLatLng(point.lat, point.lng)) {
						var pt = new google.maps.LatLng(point.lat, point.lng);
						var info="<div id='info"+point.staffId+"'><img  src='/resources/images/loading-small.gif'/></div><br/>";
						var title=point.staffName;
						var image=point.image;
						var accuracy = point.accuracy;
						var createTime = point.createTime;
						
						var level = ViettelMap._map.getZoom();
						/*var markerContent = ''; 
						var markerTop = "<div id='top"+point.id+"' class='Top' style='text-align:center;position: relative;top: -15px;'></div>";
						var markerBotoom = "<div id='bottom' style='position: relative; bottom: -28px;text-align:center;'></div>";
						if(level >= 13) {
							markerTop = "<div id='top"+point.id+"' class='Top' style='text-align:center;position: relative;top: -15px;'><strong style='color:red;font-size: 12px;' class='titleShop'>" + Utils.XSSEncode(point.shopCode) + "</strong></div>";
						} else {
							markerTop = "<div id='top"+point.id+"' class='Top' style='display:none;text-align:center;position: relative;top: -15px;'><strong style='color:red;font-size: 12px;' class='titleShop'>" + Utils.XSSEncode(point.shopCode) + "</strong></div>";
						}
						if(level >= 16) {
							markerBotoom = "<div id='bottom"+point.id+"' class='Bottom' style='position: relative; bottom: -28px; text-align:center;'><strong style='color:red;font-size: 12px;' class='titleMarker'>" + Utils.XSSEncode(point.staffName) + "</strong></div>";
						} else {
							markerBotoom = "<div id='bottom"+point.id+"' class='Bottom' style='position: relative; bottom: -28px; display:none;text-align:center;'><strong style='color:red;font-size: 12px;' class='titleMarker'>" + Utils.XSSEncode(point.staffName) + "</strong></div>";
						}
						markerContent = "<div id='marker"+point.id+"' style='height: 70px; width: 100px; left:-25px; position: relative; text-align: center; top: -40px;'>"+markerTop+markerBotoom+"</div>";*/
						var markerContent = "<div id='top" + point.id + "' class='Top' style='text-align:center;position: relative'><strong style='color:red;font-size: 12px;' class='titleMarker'>" + Utils.XSSEncode(point.staffName) + "</strong></div>";
						var marker = new MarkerWithLabel({
							icon:{
								url : image,
								size : {height : 39, width : 20},
								scaledSize : {height : 39, width : 20}
							},
							position: pt,
							map: ViettelMap._map,
							draggable: false,
							labelContent: markerContent,
							labelClass: "MarkerLabel",
							labelVisible: true,
							labelAnchor: new google.maps.Point(25, 0)
						});
						marker.staffId = point.staffId;
						marker.lat = point.lat;
						marker.lng = point.lng;
						marker.accuracy = point.accuracy;
						marker.createTime = point.createTime;
						marker.id=point.id;
						$('#marker'+point.id).parent().prev().css('z-index', 10000000);
						google.maps.event.addListener(marker, "click", function(evt) {
							var point = this;							
							ViettelMap.showWindowInfo(point.id, point.lat, point.lng, point.accuracy, point.createTime);
						});
						ViettelMap.pushListOverlays(marker);
						
						if(SuperviseSales._lstNodeExcep.get(marker.id) != null) {
							$('#marker'+marker.id).parent().parent().hide();
						}
					}
				}
				if(index>=ViettelMap._listMarker.valArray.length){
					ViettelMap.hideShowTitleMarker();
//					SuperviseSales.showCustomer();
					if(SuperviseSales._notFitOverlay==undefined || SuperviseSales._notFitOverlay==null){
						ViettelMap.fitOverLay();
					}else if(SuperviseSales._idStaffSelected!=null && SuperviseSales._idStaffSelect==SuperviseSales._idStaffSelected ){
						SuperviseSales.moveToStaff(SuperviseSales._idStaffSelect);
					}
					SuperviseSales._notFitOverlay=null;
					window.clearInterval(SuperviseSales._itv);
				}
				if(SuperviseSales._idStaffSelect != null || SuperviseSales._idStaffSelected != null) {
					
				} else {
					ViettelMap.fitOverLay();
				}
				ViettelMap.hideShowTitleMarker();
			},300); 
		}
	},
	pushListOverlays:function(marker){
		if(ViettelMap._listOverlay == null || ViettelMap._listOverlay == undefined) {
			ViettelMap._listOverlay = new Array();
			ViettelMap._listOverlay.push(marker);
		} else {
			ViettelMap._listOverlay.push(marker);
		}	
	},
	getBound: function(bound, latlng) {
		if(bound == null || bound == undefined) {
			bound = new google.maps.LatLngBounds(latlng, latlng);
		} else {
			var lat = latlng.lat();
			var lng = latlng.lng();
			
          	var swLat = bound.getSouthWest().lat();
          	var swLng = bound.getSouthWest().lng();
          	var neLat = bound.getNorthEast().lat();
          	var neLng = bound.getNorthEast().lng();
          
			if(bound.getSouthWest().lat() > lat) {
				swLat = lat;
			}
			if(bound.getSouthWest().lng() > lng) {
				swLng = lng;
			}
			if(bound.getNorthEast().lat() < lat) {
				neLat = lat;
			}
			if(bound.getNorthEast().lng() < lng) {
				neLng = lng;
			}
			
			var sw = new google.maps.LatLng(swLat, swLng);
			var ne = new google.maps.LatLng(neLat, neLng);
			
			bound = new google.maps.LatLngBounds(sw, ne);
		}
		return bound;
	},
	hideShowTitleMarker:function(){
		if(ViettelMap._map.getZoom()>=13){
    		$('.Top').show();
    		if(ViettelMap._map.getZoom()>=16){
	    		//$('.Bottom').show();
	    	}else{
	    		//$('.Bottom').hide();
	    	}
    	}else{
    		$('.Top').hide();
    		//$('.Bottom').hide();
    	}
	},
	addMutilMarkerStaffForSetOrder:function(){
		var map = ViettelMap._map;
		var lstPoint = new Array();
		if(SuperviseManageRouteCreate._listMarker!=undefined && SuperviseManageRouteCreate._listMarker!=null && SuperviseManageRouteCreate._listMarker.length >0){
			lstPoint = SuperviseManageRouteCreate._listMarker;
			var flag=0;
			var index=0;
			ViettelMap._info = new Array();
			SuperviseManageRouteCreate._itv =window.setInterval(function(){
				var j=0;
				for(var i=index,j=0;i<lstPoint.length;i++,index++,j++){
					if(j>100) break;
					var point = lstPoint[i];
					if(ViettelMap.isValidLatLng(point.lat, point.lng)) {
						var pt = new google.maps.LatLng(point.lat, point.lng);
						var custId=point.customerId;
						var info="<strong>" + Utils.XSSEncode(point.customerCode) + "</strong> - <strong>" + Utils.XSSEncode(point.customerName) + "</strong><br/>";
						info += "Địa chỉ: ";
						if(point.address != null)
							info += point.address;
						var ttgt = "";
						if(point.ttgt != undefined && point.ttgt != null){
							ttgt=point.ttgt;
						}
						var image="/resources/images/Mappin/icon_circle_red.png";
						//map.addOverlay(new MyOverlay(custId, pt, image, title,info));
						var markerContent = '<span id="marker'+custId+'" style="color: white; font-weight: bold; z-index: 10000001; position: relative; left: 22px; top: -23px;" class="StaffPositionOridnalVisit" ' + 
							'onclick="ViettelMap.showDialogCustomerInfoEx(' + point.lat + ',' + point.lng + ',\'' + Utils.XSSEncode(point.customerCode) + '\',\'' + Utils.XSSEncode(point.customerName) + '\',\'' + Utils.XSSEncode(point.address) + '\');">' + ttgt + '</span>' + 
							'<span style="color: blue; font-weight: bold; position: relative; left: 6px; top: -45px;">'+ point.customerCode +'</span>';
						if(ttgt > 9){
							markerContent = '<span id="marker'+custId+'" style="color: white; font-weight: bold; z-index: 10000001; position: relative; left: 18px; top: -23px;" class="StaffPositionOridnalVisit"' + 
							'onclick="ViettelMap.showDialogCustomerInfoEx(' + point.lat + ',' + point.lng + ',\'' + Utils.XSSEncode(point.customerCode) + '\',\'' + Utils.XSSEncode(point.customerName) + '\',\'' + Utils.XSSEncode(point.address) + '\');">' + ttgt + '</span>' + 
							'<span style="color: blue; font-weight: bold; position: relative; left: 0px; top: -45px;">'+ Utils.XSSEncode(point.customerCode) +'</span>';
						}
						var marker = new google.maps.Marker({
							icon:{
								url : image,
								size : {height : 32, width : 32},
								scaledSize : {height : 32, width : 32}
							},
							labelContent: 'AAA',
							labelAnchor: new google.maps.Point(22, 0),
							labelClass: "MarkerLabel",
							position : pt,
							map : ViettelMap._map,
							title : point.customerCode,			
							draggable : false				
						});
						ViettelMap.addEventClickMarkerSetOrder(marker, info);
						if(ViettelMap._listOverlay == null || ViettelMap._listOverlay == undefined) {
							ViettelMap._listOverlay = new Array();
							ViettelMap._listOverlay.push(marker);
						} else {
							ViettelMap._listOverlay.push(marker);
						}
					}
				}
				if(index>=lstPoint.length){
					window.clearInterval(SuperviseManageRouteCreate._itv);
				}
				ViettelMap.fitOverLay();
				ViettelMap.hideShowTitleMarker();
			},300); 
		}
	},
	addEventClickMarkerSetOrder: function(marker, info) {
		google.maps.event.addListener(marker, "click", function(evt) {
			var infoWindow = new google.maps.InfoWindow({
			      content: info,
			      position: evt.latLng
			});
			infoWindow.open(ViettelMap._map, marker);
		});
	},
	showDialogCustomerInfo: function(data) {
		if (ViettelMap._currentInfoWindow != null && ViettelMap._currentInfoWindow != undefined) {
			ViettelMap._currentInfoWindow.close();
		}
		var pt = new google.maps.LatLng(data.lat, data.lng);
		var info="<strong>" + Utils.XSSEncode(data.customerCode) + "</strong> - <strong>" + Utils.XSSEncode(data.customerName) + "</strong><br/>";
		info += "Địa chỉ: ";
		if(data.address != null)
			info += Utils.XSSEncode(data.address);
		
		var infoWindow = new google.maps.InfoWindow({
		      content: info,
		      position: pt
		});
		ViettelMap._currentInfoWindow = infoWindow;
		infoWindow.open(ViettelMap._map, data);
		
		ViettelMap._map.setCenter(pt);
	},
	/** Giam sat */
	//vuongmq; 21/09/2015; Lay them DK staffId
	addMarkerCust : function(point, staffId){
		var map = ViettelMap._map;
		if(ViettelMap.isValidLatLng(point.lat, point.lng)) {
			var pt = new google.maps.LatLng(point.lat, point.lng);
			var image = '';
			if(point.image == 'Customers1Status') {//co don hang
				image = '/resources/images/Mappin/icon_circle_blue.png';
			} else if(point.image == 'Customers2Status') {//chua ghe tham
				image = '/resources/images/Mappin/icon_circle_green.png';
			} else if(point.image == 'Customers3Status') {//dang ghe tham
				image = '/resources/images/Mappin/icon_circle_orange.png';
			} else if(point.image == 'Customers4Status') {//Ghe tham khong co don dat hang
				image = '/resources/images/Mappin/icon_circle_red.png';
			} else if(point.image == 'Customers5Status') {//kh ngoai tuyen
				image = '/resources/images/Mappin/icon_circle_yellow.png';
			}			
			var marker = null;			
			if(point.ordinalVisit>9) {
				marker = new MarkerWithLabel({
					icon:{
						url : image,
						size : {height : 32, width : 32},
						scaledSize : {height : 32, width : 32}
					},
					position : pt,
					map : ViettelMap._map,
					//labelContent: '<span style="position: inherit; left: 41px; top: -24px;">'+ Utils.XSSEncode(point.ordinalVisit) +'</span>'+'<span style="position: relative; left: 15px; top: -7px;" class="CustomerTimeVisit">'+ Utils.XSSEncode(point.ordinalAndTimeVisitReal) +'</span>',
					labelContent: '<span style="position: relative; color: #09c; font-size: 14px; font-weight: bold; padding-top: -20px; padding-left: 18px;">'+point.ordinalVisit+'</span> <br />' + '<span style="position: relative; color: #f00 !important; font-size: 12px !important; font-weight: bold; padding-top: 5px; ">'+point.ordinalAndTimeVisitReal+'</span>',
					//labelClass: "CustomersStatus",
					labelVisible: true,
					draggable: false,
					labelAnchor: new google.maps.Point(25, 0)
				});
			} else if(0<=point.ordinalVisit<=9) {
				marker = new MarkerWithLabel({
					icon:{
						url : image,
						size : {height : 32, width : 32},
						scaledSize : {height : 32, width : 32}
					},
					position : pt,
					map : ViettelMap._map,
					//labelContent: '<span style="position: inherit; left: 45px; top: -25px; ">'+ Utils.XSSEncode(point.ordinalVisit) +'</span>'+'<span style="position: relative; left: 15px; top: -7px;" class="CustomerTimeVisit">'+ Utils.XSSEncode(point.ordinalAndTimeVisitReal) +'</span>',
					labelContent: '<span style="position: relative; color: #09c; font-size: 14px; font-weight: bold; padding-top: -20px; padding-left: 18px;">'+point.ordinalVisit+'</span> <br />' + '<span style="position: relative; color: #f00 !important; font-size: 12px !important; font-weight: bold; padding-top: 5px; ">'+point.ordinalAndTimeVisitReal+'</span>',
					//labelClass: "CustomersStatus",
					labelVisible: true,
					draggable: false,
					labelAnchor: new google.maps.Point(25, 0)
				});
			} else {
				marker = new MarkerWithLabel({
					icon:{
						url : image,
						size : {height : 32, width : 32},
						scaledSize : {height : 32, width : 32}
					},
					position : pt,
					map : ViettelMap._map,
					//labelContent: '<span style="position: inherit; left: 47px; top: -25px;">'+ Utils.XSSEncode(point.ordinalVisit) +'</span>'+'<span style="position: relative; left: 15px; top: -7px;" class="CustomerTimeVisit">'+ Utils.XSSEncode(point.ordinalAndTimeVisitReal) +'</span>',
					labelContent: '<span style="position: relative; color: #09c; font-size: 14px; font-weight: bold; padding-top: -20px; padding-left: 18px;">'+point.ordinalVisit+'</span> <br />' + '<span style="position: relative; color: #f00 !important; font-size: 12px !important; font-weight: bold; padding-top: 5px; ">'+point.ordinalAndTimeVisitReal+'</span>',
					//labelClass: "CustomersStatus",
					labelVisible: true,
					draggable: false,
					labelAnchor: new google.maps.Point(25, 0)
				});
			}
			marker.customerId = point.id;
			marker.lat = point.lat;
			marker.lng = point.lng;
			
			google.maps.event.addListener(marker, "click", function(evt) {
				//SuperviseSales.showCustomerDetail(this.customerId);
				SuperviseSales.showCustomerDetail(this.customerId, staffId); // vuongmq; 21/09/2015; Lay them DK staffId
			});
			
			if(ViettelMap._listOverlay == null || ViettelMap._listOverlay == undefined) {
				ViettelMap._listOverlay = new Array();
				ViettelMap._listOverlay.push(marker);
			} else {
				ViettelMap._listOverlay.push(marker);
			}
		}
	},
	calculateDistance:function(i,k){
		var n,l,h;var j;var c;var e;var a;var m;
		var d=i.latitude;
		var b=i.longitude;
		var g=k.latitude;
		var f=k.longitude;
		j=d*(Math.PI/180);
		c=b*(Math.PI/180);
		e=g*(Math.PI/180);
		a=f*(Math.PI/180);
		n=b-f;m=n*(Math.PI/180);
		h=Math.sin(j)*Math.sin(e)+Math.cos(j)*Math.cos(e)*Math.cos(m);
		h=Math.acos(h);
		l=h*180/Math.PI;
		l=l*60*1.1515;
		l=l*1.609344*1000;
		return Math.round(l);
	},
	loadMapWithMultiMarkerRouting : function(objId, lstPoint, centerPtLat, centerPtLng, zoom, callback) {
		ViettelMap.clearOverlays();
		$('#'+objId).html('');
		ViettelMap._map = new google.maps.Map(document.getElementById(objId));
		var map = ViettelMap._map;
		ViettelMap.clearOverlays();
		if(ViettelMap._currentInfoWindow != null) {
			ViettelMap._currentInfoWindow.close();
		}
		ViettelMap.fitOverLay();
		var lat=0;
		var lng=0;
	    if(!ViettelMap.isValidLatLng(centerPtLat,centerPtLng)){
	    	centerPtLat = ViettelMap._DEF_LAT;
	    	centerPtLng = ViettelMap._DEF_LNG;
		} else if(zoom == undefined || zoom == 0 || zoom == null){
			 zoom = ViettelMap._DEF_ZOOM;
		}		
	    map.setZoom(zoom);
	    var mapOptions = {
	    	zoomControl : true,
	    	zoomControlOptions : {
	    		position: google.maps.ControlPosition.RIGHT_CENTER
	    	},
	    	panControl : true,
	    	panControlOptions : {
	    		position: google.maps.ControlPosition.RIGHT_CENTER
	    	}
	    };
    	map.setOptions(mapOptions);	    
    	var center = 0;
		if(lstPoint!=undefined && lstPoint!=null){
			for(var i = 0; i < lstPoint.length; i++) {
				var point = lstPoint[i];
				if(!ViettelMap.isValidLatLng(point.lat,point.lng) && center ==0){
					lat = point.lat;
					lng = point.lng;
					center = 1;
				}
				ViettelMap.addMarkerCustomerRouting(point);
			}
		}
		if(center==0){
			lat = ViettelMap._DEF_LAT;
	    	lng = ViettelMap._DEF_LNG;
		}
		var pt = new google.maps.LatLng(lat, lng);
	    map.setCenter(pt);		
		setTimeout(function() {
			ViettelMap.fitOverLay();
		}, 10);		
		return map;
	},
	
	addMarkerCustomerRouting:function(point){
		var map = ViettelMap._map;
		if(ViettelMap.isValidLatLng(point.lat, point.lng)) {
			var pt = new google.maps.LatLng(point.lat, point.lng);
			var marker = null;
			if (point.ordinalVisit == undefined || point.ordinalVisit == null) {
				point.ordinalVisit = 0;
			}
			if(isNaN(point.ordinalVisit) && point.ordinalVisit.length == 1) {
				marker = new google.maps.Marker({
					icon:{
						url : '/resources/images/Mappin/icon_circle_blue.png',
						size : {height : 32, width : 32},
						scaledSize : {height : 32, width : 32}
					},
					position : pt,
					map : ViettelMap._map,
					title : point.ordinalVisit == 0 ? '' : '<span style="position: inherit; left: 47px; top: -25px;">'+point.ordinalVisit+'</span>',
					draggable : false					
				});
			} else if(!isNaN(point.ordinalVisit) && point.ordinalVisit <10) {
				marker = new google.maps.Marker({
					icon:{
						url : '/resources/images/Mappin/icon_circle_blue.png',
						size : {height : 32, width : 32},
						scaledSize : {height : 32, width : 32}
					},
					position : pt,
					map : ViettelMap._map,
					title : point.ordinalVisit == 0 ? '' : '<span style="position: inherit; left: 45px; top: -25px;">'+point.ordinalVisit+'</span>',
					draggable : false
				});
			} else {
				marker = new google.maps.Marker({
					icon:{
						url : '/resources/images/Mappin/icon_circle_blue.png',
						size : {height : 32, width : 32},
						scaledSize : {height : 32, width : 32}
					},
					position : pt,
					map : ViettelMap._map,
					title : point.ordinalVisit == 0 ? '' : '<span style="position: inherit; left: 41px; top: -25px;">'+point.ordinalVisit+'</span>',
					draggable : false
				});
			}
			marker.customerId = point.customerId;
			marker.lat = point.lat;
			marker.lng = point.lng;
			google.maps.event.addListener(marker, "click", function(evt) {
				SetRouting.showCustomerDetail(this.customerId);
			});
			if(ViettelMap._listOverlay == null || ViettelMap._listOverlay == undefined) {
				ViettelMap._listOverlay = new Array();
				ViettelMap._listOverlay.push(marker);
			} else {
				ViettelMap._listOverlay.push(marker);
			}
		}
	}	
	
};
var VTMapUtil = {
	loadedResources:new Array(),
	OVERLAY_ZINDEX : 9,
	OVERLAY_ZINDEX_DEFAULT : 1,
	getBrowserName : function() {
		var browserName = "";
		var ua = navigator.userAgent.toLowerCase();
		if (ua.indexOf("opera") != -1)
			browserName = "opera";
		else if (ua.indexOf("msie") != -1)
			browserName = "msie";
		else if (ua.indexOf("safari") != -1)
			browserName = "safari";
		else if (ua.indexOf("mozilla") != -1) {
			if (ua.indexOf("firefox") != -1)
				browserName = "firefox";
			else
				browserName = "mozilla";
		}
	},
	loadMapResource:function(callback){
		VTMapUtil.loadResource(google_map_url, 'js',callback);
	},
	loadResource:function(filename, filetype, callback){
		/*if ($.inArray(filename, VTMapUtil.loadedResources) == -1){
			if (filetype=="js"){ // if filename is a external JavaScript file
				var fileref=document.createElement('script');
				fileref.setAttribute("type","text/javascript");
				fileref.setAttribute("src", filename);
			}
			if (typeof fileref != "undefined"){
				if(callback != undefined){
					if(fileref.readyState){
						fileref.onreadystatechange = function (){
							if (fileref.readyState == 'loaded' || fileref.readyState == 'complete'){
								callback();
							}
						};
					} else {
						fileref.onload = callback;
					}
				}
				document.getElementsByTagName("head")[0].appendChild(fileref);
			}
			VTMapUtil.loadedResources.push(filename);
		} else {
			if(callback != undefined){
				callback();
			}
		}*/
		if(callback != undefined){
			callback();
		}
	}
};
/**
 * Mot so thao tac co ban tren easyui-datagrid
 */
var GridUtil = (function() {
	var ADD_ACTION = 1, EDIT_ACTION = 2, VIEW_ACTION = 3;
	
	// ds phuong thuc public cua GridUtil
	return {
		/** lay cot stt neu khong muon su dung cot stt mac dinh (rownumbers:false) (vi ly do nao do)
		 *	  tham so: (gridId)
		 *   eg: columns:[[GridUtil.getRowNumField(gridId),]]
		 */
		getRowNumField: getRowNumField,
		
		/** cap nhat chieu rong cot stt sinh ra bang cach goi phuong thuc tren (goi trong onLoadSuccess)
		 *   tham so: prefix, gridId
		 *   eg: GridUtil.updateRowNumWidth(prefix, gridId);
		 */
		updateRowNumWidth: updateRowNumWidth,
		
		/** cap nhat chieu rong cot stt mac dinh (rownumbers:true) (goi trong onLoadSuccess)
		 *   tham so: prefix, gridId
		 *   eg: GridUtil.updateDefaultRowNumWidth(prefix, gridId)
		 */
		updateDefaultRowNumWidth: updateDefaultRowNumWidth,
		
		/** formatter: ham formatter cho datagrid doi voi truong hop chi goi ham XSSEncode doi voi chuoi, formatCurrency doi voi so
		 *   eg: formatter: GridUtil.formatNormalCell
		 */
		formatNormalCell: formatNormalCell,
		
		/** lay 1 cot trong truong hop chi goi ham XSSEncode doi voi chuoi, formatCurrency doi voi so
		 *   tham so: fieldName, title, width, align (mac dinh: left)
		 *   3 tham so dau la bat buoc
		 *   eg: columns:[[GridUtil.getNormalField(fieldName, title, width, align),]]
		 */
		getNormalField: getNormalField,
		
		/** lay cot co icon edit (view) truong hop chi truyen 1 tham so la [id]
		 *  tham so: actionName: ten ham goi khi click (khong chua dau ()), href: duong link moi,
		 *				 addActionName: ten ham cho nut them tren title (khong chua dau ()),
		 *				 fieldTitle: text title cho cot (truyen fieldTitle == null neu muon hien thi title)
		 *  eg: columns:[[GridUtil.getEditField(actionName, href, addActionName, fieldTitle),]]
		 */
		getEditField: getEditField,
		getViewField: getViewField,
		getField: getField
	};
	
	
	// dinh nghia phuong thuc
	
	function getRowNumField(gridId) {
		return {field:"rowno", title:"STT", sortable: false, resizable: false, width:45, fixed:true, align:"center", formatter:function(v, r, i) {
			var p = $('#'+gridId).datagrid("options").pageNumber;
			var n = $('#'+gridId).datagrid("options").pageSize;
			if (p == undefined || p == null || n == undefined || n == null) {
				return (i + 1);
			}
			return (Number(p) - 1) * Number(n) + i + 1;
		}};
	}
	
	function updateRowNumWidth(prefix, gridId){
		var pId = '';
		if(prefix!= null && prefix!= undefined){
			pId = prefix;
		}
		var lastValue=$(pId + ' td[field=rowno] div').last().text().trim().length;
		if(lastValue > 0){
			var extWidth = 25;
			if(lastValue > 2){
				extWidth += (lastValue - 2) * 9 + 3;
			}
			$(pId + ' td[field=rowno] div').css('width',extWidth);
			$(pId + ' td[field=rowno] div').css('width',extWidth);
		}
		if (gridId != undefined && gridId != null && $("#"+gridId).length > 0) {
			$("#"+gridId).datagrid("resize");
		}
	}
	
	function updateDefaultRowNumWidth(prefix, gridId){
		var pId = '';
		if(prefix!= null && prefix!= undefined){
			pId = prefix;
		}
		var lastValue=$(pId + ' .datagrid-cell-rownumber').last().text().trim().length;
		if(lastValue > 0){
			var extWidth = 25;
			if(lastValue > 2){
				extWidth += (lastValue - 2) * 9 + 3;
			}
			$(pId + ' .datagrid-cell-rownumber').css('width',extWidth);
			$(pId + ' .datagrid-header-rownumber').css('width',extWidth);
		}
		if (gridId != undefined && gridId != null && $("#"+gridId).length > 0) {
			$("#"+gridId).datagrid("resize");
		}
	}
	
	function formatNormalCell(v, r, i) {
		if (v == undefined && v == null) {
			return '';
		}
		if (typeof v === "number") {
			return VTUtilJS.formatCurrency(v);
		}
		return Utils.XSSEncode(v);
	}
	
	function getEditField(action, href, addAction, fieldTitle) {
		var tt = "";
		if (addAction != undefined && addAction != null) {
			addAction = addAction + "()";
			tt = getActionIcon(null, addAction, ADD_ACTION);
		} else if (fieldTitle != undefined && fieldTitle != null) {
			tt = fieldTitle;
		}
		return {field:"edit", title:tt, sortable: false, resizable: false, width:35, fixed:true, align:"center", formatter:function(v, r, i) {
			if (action != undefined && action != null) {
				action = action + "("+r.id+")";
			}
			return getActionIcon(href, action, EDIT_ACTION);
		}};
	}
	
	function getViewField(action, href, addAction, fieldTitle) {
		var tt = "";
		if (addAction != undefined && addAction != null) {
			addAction = addAction + "()";
			tt = getActionIcon(null, addAction, ADD_ACTION);
		} else if (fieldTitle != undefined && fieldTitle != null) {
			tt = fieldTitle;
		}
		return {field:"edit", title:tt, sortable: false, resizable: false, width:35, fixed:true, align:"center", formatter:function(v, r, i) {
			if (action != undefined && action != null) {
				action = action + "("+r.id+")";
			}
			if (href != undefined && href != null) {
				href = href + "?id="+r.id;
			}
			return getActionIcon(href, action, VIEW_ACTION);
		}};
	}
	
	function getActionIcon(href, action, type) { //type = 1:add, 2:edit, 3:view
		var ic = '';
		var ht = '';
		if (ADD_ACTION == type) {
			ht = 'Thêm mới';
			ic = '/resources/images/icon_add.png';
		} else if (EDIT_ACTION == type) {
			ht = 'Chỉnh sửa'
			ic = '/resources/images/icon-edit.png';
		} else if (VIEW_ACTION == type) {
			ht = 'Xem chi tiết';
			ic = '/resources/images/icon-view.png';
		}
		if (href == undefined || href == null) {
			href = 'javascript:void(0);'
		}
		var s = '<a href="'+href+'" {action}><img width="15" height="15" alt="icon" src="'+ic+'" title="'+ht+'" /></a>';
		if (action == undefined || action == null) {
			s = s.replace(/{action}/, '');
		} else {
			s = s.replace(/{action}/, 'onclick="'+action+';"');
		}
		return s;
	}
	
	function getNormalField(fieldName, title, width, align) {
		if (align == undefined || align == null) {
			align = "left";
		}
		return {field:fieldName, title:title, sortable: false, resizable: false, width:width, align:align, formatter:formatNormalCell};
	}
	
	function getField(fieldName, title, width, align, formatter) {
		if (align == undefined || align == null) {
			align = "left";
		}
		return {field:fieldName, title:title, sortable: false, resizable: false, width:width, align:align, formatter:formatter};
	}
})();
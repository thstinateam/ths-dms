var ManageRouteCreateFormatter = {
	getStaffSale : function(cellValue, rowObject, index) {
		if(rowObject.staffCode == null || rowObject.staffCode == 'null')
    		rowObject.staffCode = "";
    	if(rowObject.staffName == null || rowObject.stafName == 'null')
    		rowObject.staffName = "";
    	if(rowObject.staffCode != "" && rowObject.staffName != "")
    		return Utils.XSSEncode(rowObject.staffCode + ' - ' + rowObject.staffName);
    	return "";
	},
	setOrderRoute:function(v,r,i){
		return "<a href='/superviseshop/manageroute-create/order/info?routingId="+r.routingId+"' title='" + Utils.XSSEncode(create_route_set_order_permission) + "'><img width='16' height='16' src='/resources/images/icon_mapedit.png'/></a>";
	},
	assignRoute:function(v,r,i){
		return "<a href='/superviseshop/manageroute-create/assign/info?routingId="+r.routingId+"' title='" + Utils.XSSEncode(create_route_assign_staff_route) + "'><img width='16' height='16' src='/resources/images/icon_addTBHV.png'/></a>";
	},
	deleteRoute : function(cellValue, rowObject,index) {
		return "<a href='javascript:void(0)' onclick=\"return SuperviseManageRouteCreate.deleteRouting("+ rowObject.routingId +",\'"+ Utils.XSSEncode(rowObject.routingCode) +"\',\'"+ Utils.XSSEncode(rowObject.staffCode) +"\')\" title='" + jsp_common_xoa + "'><img src='/resources/images/icon-delete.png' width='16' height='16' /></a>";
	},
	editRoute : function(cellValue, rowObject,index) {
		return "<a href='/superviseshop/manageroute-create/editroute?routeId="+rowObject.routingId+"&shopCode=" + Utils.XSSEncode(rowObject.shopCode) + "' title='" + jsp_common_sua + "'><img width='16' height='16' src='/resources/images/icon_edit.png'/></a>";
	},
	statusFormatter :function(value, rowData, rowIndex){
		if(value == activeType.RUNNING){
			return jsp_common_status_active;
		} else if(value == activeType.STOPPED){
			return jsp_common_status_stoped;
		} 
		return '';
	},
	checkCustomerDialog : function(value, rowData, rowIndex) {
		var param = '\''+ Utils.XSSEncode(rowData.shortCode) +'\',' + rowData.id +',\''+ Utils.XSSEncode(rowData.customerCode) +'\',\'' 
		+ Utils.XSSEncode(rowData.customerName) +'\',\'' + Utils.XSSEncode(rowData.address) +'\','+ rowData.lat +',' + rowData.lng ;
		var obj = SuperviseManageRouteCreate._mapCustomerDialog.get(rowData.shortCode);
		if(obj == null) {
			return '<input type="checkbox" id="'+ Utils.XSSEncode(rowData.shortCode) +'" onchange="SuperviseManageRouteCreate.changeCheckBoxCustomerDialog(this.checked ,'+param+')"/>';
		} else{
			return '<input checked="checked" type="checkbox"  id="'+ Utils.XSSEncode(rowData.shortCode) +'" onchange="SuperviseManageRouteCreate.changeCheckBoxCustomerDialog(this.checked ,'+param+')"/>';
		}
	}
};
var ManageSalePlanFormatter = {
	getTrain : function(cellValue, options, rowObject) {
	   	if(cellValue == 'GO'){
	   		return "<img src='/resources/images/icon-datach.png' />";
	   	}
	   	return '';
	}
};
var ManageRouteAssign = {
	editCellIconFormatter : function(cellValue, options, rowObject) {
		return "<a title='Xem thông tin tuyến' href='/superviseshop/manageroute-create/assign/routing-detail?routeId="+rowObject.routingId+"'><span style='cursor:pointer'><img src='/resources/images/icon-edit.png'/></span></a>";
	},
	statusStaffFormatter :function(value, rowData, rowIndex){
		if(value == activeStatus){
			return jsp_common_status_active;
		} else if(value == stoppedStatus){
			return jsp_common_status_stoped;
		} 
		return '';
	},
	editStaffInRouting: function(value, rowData, rowIndex) {
		var fromDate = ''; 
		var toDate = '';
		var status = 1;
		if(rowData.fromDate != null && rowData.fromDate != '') fromDate = $.datepicker.formatDate('dd/mm/yy', new Date(rowData.fromDate));
		if(rowData.toDate != null && rowData.toDate != '') toDate = $.datepicker.formatDate('dd/mm/yy', new Date(rowData.toDate));
		if(rowData.status == stoppedStatus) status =0 ;
		if(rowData.toDate != null && Utils.compareCurrentDateExReturn($.datepicker.formatDate('dd/mm/yy', new Date()),toDate) >0){
			return '';
		}
		var param = rowData.id+','+rowData.staff.id + ',\''+ fromDate+ '\',\''+ toDate+'\','+status;
		return '<a href="javascript:void(0)" onclick="SuperviseManageRouteAssign.editStaff('+param+')"><img src="/resources/images/icon_edit.png" width="15" height="16" title="' + jsp_common_sua + '"></a>';
	},
	deleteStaffInRouting : function(value, rowData, rowIndex) {
		return '<a href="javascript:void(0)" onclick="SuperviseManageRouteAssign.deleteStaff('+ rowData.id+')"><img src="/resources/images/icon_delete.png" width="15" height="16" title="' + jsp_common_xoa + '"></a>';
	},
	checkDate  : function(value, rowData, rowIndex) {
		if(value =='GO') {
			return '<img src="/resources/images/icon_check.png"/>';
		}
		return '';
	},
	formatStartWeek :function(value, rowData, rowIndex) {
		var date = new Date();
		if(value != undefined && value != null) {
			return  getFirstDayOfWeek(value,date.getFullYear());
		}
		return '';
	}
};
var VisitPlanFormatter ={
	showMap:function(cellValue, options, rowObject){
		return "<span style='cursor:pointer' onclick=\"return VisitResult.showMap("+"'"+ Utils.XSSEncode(rowObject.staffCode) +"'"+");\"><img src=\"/resources/images/xembando.png\" /></span>";
	}
};
var SellerPositionFormatter ={
	getSellerPositionDetail:function(cellValue, options, rowObject){
		if(rowObject.hasPosition=='1'){
			return "<a href='javascript:void(0);' onclick=\"return SellerPosition.getSellerPositionDetail('"+ rowObject.staffId + "','" + Utils.XSSEncode(rowObject.staffCode) + "','" + Utils.XSSEncode(rowObject.staffName) + "','" + Utils.XSSEncode(rowObject.shopCode) + "',"+ Utils.XSSEncode(rowObject.objectType) +");\">" + Utils.XSSEncode(cellValue) + "</a>";
		}else{
			return Utils.XSSEncode(cellValue);
		}
	},
	getSellerType : function(cellValue, options, rowObject) {
	   	if(cellValue == "1"){
	   		return "NVBH";
	   	}else 	if(cellValue == "5"){
	   		return "GSNPP";
	   	}else 	if(cellValue == "7"){
	   		return "TBHV";
	   	}
	   	return "";
	}
};
var General = {
		_flagNextRole: false,
		_flagChoseShop: false,
		_mapGridDl : null,
		_urlRedrect: '',
		_shopCode: null,
		mapCycle: null,
		/**
		 * Thay doi Shop tuong tac
		 * 
		 * @author hunglm16
		 * @sine March 21,2014
		 * */
		setCurrentPageIsChange : function () {
			$('.ErrorMsgStyle').html('').hide();
			var msg = "";
			shopId = $('#lstShopVOIsChange').combobox('getValue');
			if (Number(shopId).toString() === 'NaN' || Number(shopId) <= 0) {
				msg = "Chưa chọn Đơn vị";
				$('#errMsgDlIsChange').html(msg).show();
				return false;
			}
			var arrShopId = [];
			arrShopId.push(shopId);
			
			var params = {};
			params.shopId = shopId;
			Utils.addOrSaveData(params, '/cms/setUpdateSessionIsChange', null, 'errMsgDlIsChange', function(data) {
				if (data.error != undefined && data.error != null && data.error) {
					$('#errMsgDlIsChange').html(data.errMsg).show();
				} else {
					window.location.assign(window.location.origin + General._urlRedrect);
				}
			}, null, null, null, msg, function(data){
				if(data.errorType!=undefined && data.errorType!=null && data.errorType == 1){
					$('#lstShopByRole').focus();
				}
			});
			return false;
		},
		/**
		 * Gan Current page Cho man hinh dac trung la KTNPP
		 * 
		 * @author hunglm16
		 * @sine October 8,2014
		 * */
		setCurrentPageIsShopIsLevel5 : function () {
			$('.ErrorMsgStyle').html('').hide();
			var msg = "";
			shopId = $('#lstShopVOIsLevel5').combobox('getValue');
			if (Number(shopId).toString() === 'NaN' || Number(shopId) <= 0) {
				msg = "Chưa chọn đơn vị";
				$('#errMsgDlIslevel5').html(msg).show();
				return false;
			}
			var arrShopId = [];
			arrShopId.push(shopId);
			
			var params = {};
			params.shopId = shopId;
			Utils.addOrSaveData(params, '/cms/setUpdateSessionIsShopIsLevel5', null,  'errMsgDlIslevel5', function(data) {
				if (data.error != undefined && data.error != null && data.error) {
					$('#errMsgDlIslevel5').html(data.errMsg).show();
				} else {
					window.location.assign(window.location.origin + General._urlRedrect);
				}
			}, null, null, true, msg, function(data){
				if(data.errorType!=undefined && data.errorType!=null && data.errorType == 1){
					$('#lstShopByRole').focus();
				}
			});
			return false;
		},
		/**
		 * Dialog chin lai don vi tuong tac
		 * 
		 * @author hunglm16
		 * @since March 22,2015
		 * */
		openDialogSelectShopIsChange: function(){
			if ($('#selectShopForHeader.easyui-dialog').length == 0) { // nhan escape -> thoat
				$('#selectShopForHeader').addClass("easyui-dialog");
			}
			$('#selectShopForHeader').dialog({
				title: "Chọn Đơn vị Tương Tác",
				closed: false,  
		        cache: false,  
		        modal: true,
		        width: 630,
		        onOpen: function(){
		        	$('.ErrorMsgStyle').html('').hide();
		        	General._flagChoseShop = false;
		        	if (General._urlRedrect == undefined || General._urlRedrect == null) {
		        		General._urlRedrect = "";
		        	}
		        	Utils.getJSONDataByAjaxNotOverlay({},'/cms/getListShopIsChange', function(data) {
						$('#lstShopChangeByRoleDiv').html('<select class="easyui-combobox" id="lstShopVOIsChange"></select>').change().show();
						if(data!=undefined && data!=null && !data.error){
							if (data.rows != undefined && data.rows !=null && data.rows.length > 0) {
								var options = "";
								for (var i=0; i<data.rows.length; i++) {
									options = options + '<option value="'+data.rows[i].shopId+'" code="'+Utils.XSSEncode(data.rows[i].shopCode)+'" name="'+Utils.XSSEncode(data.rows[i].shopName)+'"></option>';
								}
								$('#lstShopVOIsChange').html(options.trim()).change().show();
							} else {
								$('#lstShopVOIsChange').html('<option value="0" code="" name=""></option>').change().show();
							}
						}
						General.bindComboboxEasyUI("lstShopVOIsChange", 452);
						$('.combo').css('width', 452);
						$('.combo-text.validatebox-text').css('width', 430);
					});
		        	
		        },
		        onClose:function() {
		        	$('.ErrorMsgStyle').html('').hide();
		        }
			});
		},
		/**
		 * Dialog su dung chung cho cac man hinh dac trung voi Role la KTNPP 
		 * 
		 * @author hunglm16
		 * @since October 8,2014
		 * */
		openDialogSelectShopIslevel5: function(){
			//trungtm6 tam thoi comment
			/*if ($('#selectShopForCurrentPage.easyui-dialog').length == 0) { // nhan escape -> thoat
				$('#selectShopForCurrentPage').addClass("easyui-dialog");
			}
			$('#selectShopForCurrentPage').dialog({
				title: "Chọn Nhà Phân Phối",
				closed: false,  
		        cache: false,  
		        modal: true,
		        width: 630,
		        onOpen: function(){
		        	$('.ErrorMsgStyle').html('').hide();
		        	General._flagChoseShop = false;
		        	if (General._urlRedrect == undefined || General._urlRedrect == null) {
		        		General._urlRedrect = "";
		        	}
		        	Utils.getJSONDataByAjaxNotOverlay({},'/cms/getListShopIsLV5ByShopRoot', function(data) {
						$('#lstShopIsLV5ByRoleDiv').html('<select class="easyui-combobox" id="lstShopVOIsLevel5"></select>').change().show();
						if(data!=undefined && data!=null && !data.error){
							if (data.rows != undefined && data.rows !=null && data.rows.length > 0) {
								var options = "";
								for (var i=0; i<data.rows.length; i++) {
									options = options + '<option value="'+data.rows[i].shopId+'" code="'+data.rows[i].shopCode+'" name="'+data.rows[i].shopName+'"></option>';
								}
								$('#lstShopVOIsLevel5').html(options.trim()).change().show();
							} else {
								$('#lstShopVOIsLevel5').html('<option value="0" code="" name=""></option>').change().show();
							}
						}
						General.bindComboboxEasyUI("lstShopVOIsLevel5", 452);
						$('.combo').css('width', 452);
						$('.combo-text.validatebox-text').css('width', 430);
					});
		        	
		        },
		        onClose:function() {
		        	$('.ErrorMsgStyle').html('').hide();
		        	if(!General._flagChoseShop){
		        		setTimeout(function(){
		        			General.openDialogSelectShopIslevel5();
		    			}, 500);
		        	}
		        }
			});*/
		},
		
		/**
		 * @author hunglm16
		 * @since September 22, 2014
		 **/
		changeCheckboxAllByGridDl : function (cbx, idGrid){
			var check = $(cbx).prop("checked");
			var idDg = 'common-dialog-grid-search';
			if (idGrid != undefined && idGrid != null) {
				idDg = idGrid;
			}
			if (check) {
				$('.changegridcbxDl').prop("checked", true);
				var rows = $('#'+ idDg).datagrid("getRows");
				General._mapGridDl = new Map();
				for (var i=0; i < rows.length; i++) {
					General._mapGridDl.put(Number($($('.changegridcbxDl')[i]).prop("value")), rows[i]);
				}
			} else {
				$('.changegridcbxr').prop("checked", false);
				General._mapGridDl = new Map();
			}
		},
		/**
		 * @author hunglm16
		 * @since September 22, 2014
		 **/
		changeCheckboxByGridDl : function (cbx){
			var check = $(cbx).prop("checked");
			if (check) {
				if ($('.changegridcbxr:checked').length == $('.changegridcbxr').length) {
					$('#ckallPrdPr').click();
				} else {
					PriceManager._arrId.push(Number($(cbx).prop("value")));
				}
			} else {
				$('#ckallPrdPr').prop("checked", false);
				if(PriceManager._arrId != null && PriceManager._arrId.length > 0){
					var i = PriceManager._arrId.indexOf(Number($(cbx).prop("value")));
					if(i != -1) {
						PriceManager._arrId.splice(i, 1);
					}
				}
			}
		},
		///@author HungLM16; @since AUGUST 14,2014; @description Su kien Autocomlete Easy UI
		bindComboboxEasyUI:function(objectId, width, prefix){
			var parent = '';
			if(prefix!=undefined && prefix!=null){
				parent = prefix;
			}
			var widthSize = 250;
			if(width!=undefined && width!=null){
				widthSize = width;
			}
			$(parent + '#'+objectId).combobox({			
				valueField : 'id',
				textField : 'name',
				data : General.getDataComboboxEasyUI(objectId, parent),
				width: widthSize,
				formatter: function(row) {
					return '<span style="font-weight:bold">' + Utils.XSSEncode(row.name) + '</span><br/>' + '<span style="color:#888">' + Utils.XSSEncode(row.code) + '</span>';
				},
				filter: function(q, row){
					q = new String(q).toUpperCase().trim();
					var opts = $(this).combobox('options');
					return row['searchText'].indexOf(unicodeToEnglish(q))>=0;
				},			
				onChange:function(newvalue, oldvalue){
				}
			});
			//$('.combo-arrow').unbind('click');
		},
		
		///@author HungLM16; @since AUGUST 14,2014; @description Su kien Lay du lieu Autocomlete Easy UI
		getDataComboboxEasyUI :function(objectId, prefix){
			var data = new Array();		
			var parent = '';
			if(prefix!=undefined && prefix!=null){
				parent = prefix;
			}
			$(parent + '#' + objectId +' option').each(function(){
				var obj = new Object();
				obj.id = $(this).val().trim();
				obj.code = Utils.XSSEncode($(this).attr("code").trim());
				if(obj.code==undefined || obj.code==null){
					obj.code= "";
				}
				obj.name = Utils.XSSEncode($(this).attr("name").trim());
				if(obj.name==undefined || obj.name==null){
					obj.name= "";
				}
				obj.displayText = $(this).val().trim() + ' - ' + $(this).text().trim(); 
				obj.searchText = unicodeToEnglish(obj.code + obj.name);
				obj.searchText = obj.searchText.toUpperCase();
				data.push(obj);
			});
			return data;
		},
		
		getListShopByRole: function(cbx){
			var roleId = $(cbx).val();
			if(roleId!=undefined && roleId!=null && parseInt(roleId)>0){
				var params = {};
				params.id = roleId;
				Utils.getJSONDataByAjaxNotOverlay(params,'/cms/nextByRole', function(data){
					$('#lstShopByRoleDiv').html('<select class="easyui-combobox" id="lstShopByRole"></select>').change().show();
					if(data!=undefined && data!=null && !data.error){
						/**if(data.rows!=undefined && data.rows!=null && data.rows.length == 1){ 
							General.nextByRole(data.rows[0].shopId);
						} else **/ 
						if(data.rows!=undefined && data.rows !=null && data.rows.length > 0){
							var options = "";
							for(var i=0; i<data.rows.length; i++){
								options = options + '<option value="'+data.rows[i].shopId+'" code="'+data.rows[i].shopCode+'" name="'+data.rows[i].shopName+'"></option>';
							}
							$('#lstShopByRole').html(options.trim()).change().show();
						}else{
							$('#lstShopByRole').html('<option value="0" code="" name=""></option>').change().show();
						}
					}
					General.bindComboboxEasyUI("lstShopByRole", 452);
					$('.combo').css('width', 452);
					$('.combo-text.validatebox-text').css('width', 430);
				});
			}
			else{
				$('#lstShopByRoleDiv').html('<select class="easyui-combobox" id="lstShopByRole"></select>').change().show();
				$('#lstShopByRole').html('<option value="0" code="" name=""></option>').change().show();
				General.bindComboboxEasyUI("lstShopByRole", 452);
				$('.combo').css('width', 452);
				$('.combo-text.validatebox-text').css('width', 430);
			}
		},
		
		nextByRole: function(shopIdDr){
			$('.ErrorMsgStyle').html('').hide();
			var msg = "";
			var roleId = $('#lstRoleToken').val();
			if(roleId == undefined || roleId == null || parseInt(roleId)==0){
				msg = "Chưa chọn vai trò";
			}
			var shopId = 0;
			if (shopIdDr != undefined && shopIdDr != null && shopIdDr > 0) {
				shopId = shopIdDr;
			} else {
				shopId = $('#lstShopByRole').combobox('getValue');
			}
			if(msg.length ==0 && (shopId == undefined || shopId == null || shopId <= 0)){
				msg = "Chưa chọn đơn vị";
			}
			if(msg.length>0){
				$('#errMsgDialogNextRole').html(msg).show();
				return false;
			}
			var arrShopId = [];
			arrShopId.push(shopId);
			
			var params = {};
			params.id = roleId;
			params.lstId = arrShopId;
			Utils.saveData(params, '/cms/update-IsOnline', null,  'errMsgDialogNextRole', function(data) {
				window.location.assign(window.location.origin + '/index.jsp');
			}, null, null, null, function(data){
				if(data.errorType!=undefined && data.errorType!=null && data.errorType == 1){
					$('#lstRoleDiv').focus();
				}
			});
			return false;
			
		},
		
		openDialogSelectNextRole: function(){
			$('#selectRoleAndShopDefault').dialog({
				title: "Chọn Vai trò và Đơn vị tương tác",
				closed: false,  
		        cache: false,  
		        modal: true,
		        width: 630,
		        onOpen: function(){
		        	$('.ErrorMsgStyle').html('').hide();
		        	General._flagNextRole = false;
		        	selectedDropdowlist('lstRoleToken', 0);
		        	$('#lstRoleDiv').focus();
		        },
		        onClose:function() {
		        	$('.ErrorMsgStyle').html('').hide();
		        	selectedDropdowlist('lstRoleToken', 0);
		        	if(!General._flagNextRole){
		        		setTimeout(function(){
		        			General.openDialogSelectNextRole();
		    			}, 500);
		        	}
		        }
			});
		},
		/**
		 * Thoat khoi he thong
		 * 
		 * @author hunglm16
		 * @since October 8,2014
		 * */
		logoutSystem : function(){
			window.location.assign(window.location.origin + '/commons/logout');
		},
		/**
		 * Quay lai trang chu
		 * 
		 * @author hunglm16
		 * @since OCtober 8,2014
		 * */
		goinghome : function (){
			window.location.href = window.location.origin;
		},
		
		/**
		 * Cat bot chuoi de hien thi 
		 * @author hunglm16
		 * @param value
		 * @param perfix
		 * @param perfix
		 * @param gString
		 * @since 25/10/2015
		 */
		subStringByListagg: function (value, perfix, size, gString) {
			if (value == null || value == undefined || value.trim().length == 0) {
				return '';
			}
			if (size == null || size == undefined || size <= 0) {
				return value.trim();
			}
			if (perfix == null || perfix == undefined) {
				perfix = ',';
			}
			var arrTp = value.trim().split(perfix);
			if (arrTp.length > size) {
				var kqArr = [];
				for (var i = 0; i < size; i++) {
					kqArr.push(arrTp[i].trim());
				}
				var kq = kqArr.toString();
				kq = kq.replace(/,/g, perfix);
				if (gString != null && gString != undefined && gString.trim().length > 0) {
					kq = kq + perfix + gString;
				}
				return kq;
			}
			return value;
		},
		/**
		 * Tai tap tin hinh anh Bien ban thiet bi
		 * 
		 * @author hunglm16
		 * @since January 01,2014
		 * */
		downloadEquipAttechFile: function(objectId, objectType) {
			$('.SuccessMsgStyle').html('').hide();
			$('.ErrorMsgStyle').html('').hide();
			var errMsgPopup = 'errMsgShowDialogList';
			VCommonJS.showDialogList({
				dialogInfo: {
					title: 'Danh sách tập tin đính kèm'
				},
				url: '/commons/search-Equip-Attach-File',
				params: {
					objectId : objectId,
					objectType : objectType,
					flagPagination: true
				},
				columns : [[
					{field: 'fileName', title: 'Tên tập tin', align: 'left', width: 110, sortable: false, resizable: false},
					{field: 'download', title: '', align: 'center', width: 40, sortable: false, resizable: false, formatter: function(value, row, index) {
						return '<a href="javascript:void(0)" onclick = "General.downloadEquipAttachFileProcess('+row.id+','+row.objectId+',\''+errMsgPopup+'\');" title="Tải về">Tải về</a>';
					}}
				]]
			});
		},
		
		/**
		 * Tai tap tin hinh anh tu server
		 * 
		 * @author hunglm16
		 * @since Feb 02,2015
		 * */
		downloadEquipAttachFileProcess: function(id, objectId, errMsgPopup) {
			$('.SuccessMsgStyle').html('').hide();
			$('.ErrorMsgStyle').html('').hide();
//			ReportUtils.exportReport('/commons/dowloadEquipAttachFile', {
//				id: id,
//				objectId: objectId
//			});
			var kData = $.param({
						id: id,
						objectId: objectId,
						reportCode: $('#function_code').val(), /** vuongmq; 07/05/2015; ATTT xuat file */
			}, true);
			var url = '/commons/dowloadEquipAttachFile';
			$('#errMsg').html('').hide();
			$('#divOverlay').show();		
			$.ajax({
				type : "POST",
				url : url,
				data : (kData),
				dataType: "json",
				success : function(data) {
					$('#divOverlay').hide();				
					if (!data.error) {
						if(data.hasData != undefined && data.hasData != null && data.hasData == false) {
							$('#'+errMsgPopup).html('Không có dữ liệu tập tin').show();
						} else {
							/** Begin vuongmq; 07/05/2015; ATTT xuat file */
							var filePath = ReportUtils.buildReportFilePath(data.view);
							ReportUtils.openInNewTab(filePath);
							setTimeout(function(){ 
								//Set timeout de dam bao file load len hoan tat
								CommonSearch.deleteFileExcelExport(filePath);
							}, 15000);
							/** End vuongmq; 07/05/2015; ATTT xuat file */
							/*ReportUtils.openInNewTab(data.view);
							setTimeout(function(){ 
								//Set timeout de dam bao file load len hoan tat
								CommonSearch.deleteFileExcelExport(data.view);
							}, 15000);*/
						}
					} else {
						$('#'+errMsg).html(data.errMsg).show();					
					}
				},
				error: function (XMLHttpRequest, textStatus, errorDivThrown) {
					$('#divOverlay').hide();
					StockIssued.xhrExport = null;
				}
			});
		},
		
		/**
		 * Lay danh sach lich su giao nhan
		 * 
		 * @author hunglm16
		 * @since January 05,2014
		 * */
		 openDialogListHistoryEquipRecord:  function(objectId, objectType) {
			$('.SuccessMsgStyle').html('').hide();
			$('.ErrorMsgStyle').html('').hide();
			VCommonJS.showDialogList({
				dialogInfo: {
					title: 'Lịch sử giao nhận'
				},
				url: '/commons/search-history-equip-record-vo',
				params: {
					objectId : objectId,
					objectType : objectType,
					flagPagination: true
				},
				columns : [[
					{field:'actDateStr', title:'Ngày', align:'center', width: 110, sortable:false, resizable:false},
					{field:'deliveryStatusStr', title:'Trạng thái', align:'left', width:40, sortable:false, resizable:false}
					]]
				});
		 }
};
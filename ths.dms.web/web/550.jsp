<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@page import="ths.dms.helper.Configuration"%>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title></title>
	<link rel="shortcut icon" type="image/x-icon" href="<%=Configuration.getImgServerPath()%>/resources/images/favicon.ico"/>		
	<link href="<%=Configuration.getCssServerPath("/resources/styles/style.min.css")%>" rel="stylesheet" type="text/css" />
</head>

<body>
	<div id="container">
        <div id="header" class="Sprite1">
            <h1 class="LogoNotLogin"><a href="/home" class="HideText Sprite1"></a></h1>
        </div>
        <div id="main">
            <div id="mainIn">
                <div class="Content">
                	<p class="Error404500Style"><span>Lỗi 550. Bạn không có quyền thao tác trang này</span></p>
                </div>                
            </div>
        </div>				
        <div id="footer">
        	<div id="footerIn">
            	<p class="CopyrightStyle"></span></p>
            </div>
        </div>
    </div>	
</body>
</html>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@page import="ths.dms.helper.Configuration"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>

<div class="BreadcrumbSection">
    <ul class="ResetList FixFloat BreadcrumbList">
        <li class="Sprite1"><a href="#">Giám sát</a></li>
        <li><span>Giám sát lộ trình trên bản đồ</span></li>
    </ul>
</div>
<div class="CtnOneColSection">
	<div class="ContentSection">
		<div class="ToolBarSection">
			<div class="GuideMapSection">
				<div class="GuideMapInSection">
					<label class="LabelStyle Label1Style"><input type="checkbox" id="cbNVBH" /><span class="Sprite1">NVBH</span> </label> 
					<label class="LabelStyle Label2Style"><input type="checkbox" id="cbGSNPP" /><span class="Sprite1">GSNPP</span> </label>
					<label class="LabelStyle Label3Style"><input type="checkbox" id="cbTBHV" /><span class="Sprite1">QL</span> </label>
					<div class="Clear"></div>
				</div>
			</div>
			<div class="TSSection">
<!-- 				<div class="TSInSection"> -->
					<!-- 					<label class="LabelStyle"><input id="cbLHL" type="checkbox" />Lịch huấn luyện</label> -->
					<input id="cbLHL" type="hidden" />

					<!-- <div class="TSSection SearchInSection SProduct1Form">
						<div class="BtnRSection">
							<button id="btCapNhatViTri" class="BtnGeneralStyle"
								style="height: 23px; padding: 3px 8px !important; width: 120px"
								onclick="SuperviseSales.reloadTreeStaff();">Cập nhật vị trí</button>
						</div>
					</div> -->
					<div class="Clear"></div>
<!-- 				</div> -->
			</div>
			<div class="TSSection SearchInSection SProduct1Form">
				<div class="BtnRSection">
<!-- 					<button id="btCapNhatViTri" class="BtnGeneralStyle" style="height: 23px;padding: 3px 8px !important;width:120px" onclick="SuperviseSales.reloadTreeStaff();">Cập nhật vị trí</button> -->
					<button id="btCapNhatViTri" class="BtnGeneralStyle" style="height: 23px; padding: 3px 8px !important; width:135px"><s:text name="js.sale.supervise.content.header.region.control.refresh.staff.position"></s:text></button>
				</div>
			</div>
			<div id="xemLoTrinhDialog" class="GeneralForm Search1Form" style="display: none;margin-bottom:0px;padding-top: 11px">
				<label style="margin-left: 20px;color:black;font-weight:bold;font-size:0.75em;" id="titleId" class="LabelStyle" ><strong><s:text name="expire.type.0"></s:text></strong></label>
				<label style="margin-left: -5px;color:black;font-weight:bold;font-size:0.75em;" class="LabelStyle" ><s:text name="giam.sat.tu"></s:text></label>
				<input id="fromTime" type="text" class="InputTextStyle InputText1Style" readonly="readonly" style="width: 35px;"/>
				<label style="margin-left: 5px;color:black;font-weight:bold;font-size:0.75em;" class="LabelStyle" ><s:text name="giam.sat.den"></s:text></label>
				<input id="toTime" type="text" class="InputTextStyle InputText1Style" readonly="readonly" style="width: 35px;"/>
				<label style="margin-left: 8px;color:black;font-weight:bold;font-size:0.75em;" class="LabelStyle" ><s:text name="giam.sat.ngay"></s:text></label>
				<input id="startDate" type="text" class="InputTextStyle InputText1Style" readonly="readonly"/>
				<div id="kmDriven" style="display:none;width: 1111px;">
					<label style="margin-left: 20px;color:black;font-weight:bold;font-size:0.75em;" id="titleId" class="LabelStyle" ><s:text name="jsp.so.km.da.di" /></label>
					<span id="resultKmDriven"  style="margin-left: 0px;color:black;font-weight:bold;font-size:0.75em;" id="titleId" class="LabelStyle" ></span>
					<span style="margin-left: 0px;color:black;font-weight:bold;font-size:0.75em;" id="titleResultKmDrivenId" class="LabelStyle">(Km)</span>
					<div style="display: none;">
						<label><input type="radio" value="0" name="kmType" checked="checked"><img title="<s:text name='mes.account.12'/>" width="30" height="30" src="/resources/images/Mappin/line_chart.png"/></label>
						<label><input type="radio" value="1" name="kmType"><img title="<s:text name='mes.account.11'/>" width="15" height="15" src="/resources/images/icon_login.png"/></label>
					</div>
				</div>
			</div>
			<div class="Clear"></div>
			<p class="ErrorMsgStyle" id="errMsg" style="display: none;"></p>
		</div>
		<div class="MapRouteSection" >
			<div class="ShadowBgSection"></div>
			<div id="bigMap" style="width: 100%; height: 500px; position: relative;"></div>
			<div class="NoteCustomersStatus" style="display:none;z-index:1000;">
               	<ul class="ResetList NCSList FixFloat">
                   	<li id="item1">Chưa ghé thăm</li>
                   	<li id="item2">Ghé thăm có ĐH</li>
					<li id="item3">Ghé thăm không có ĐH</li>
					<li id="item4">Đang ghé thăm</li>
					<li id="item5">KH ngoại tuyến</li>
					<li id="item6" style="background-size: 35px auto;cursor: pointer;" onclick="SuperviseSales.showPathViewer()" >Vẽ lộ trình</li>
                   </ul>
                   <a href="javascript:void(0)" onclick="SuperviseSales.reloadMarker();" class="CloseStyle"><img src="/resources/images/icon_close.png" width="12" height="12" /></a>
               </div>
			<div class="StaffSelectSection">
				<div class="StaffSelectBtmSection">
					<p id="titleStaff" onclick="SuperviseSales.toggleListStaff();" class="StaffSelectText Sprite1 OffStyle">Đơn vị quản lý</p>
					<div id="listStaff" class="StaffTreeSection SearchSection GridSection">
						<%-- <div class="BoxSelect BoxSelect11">                    	
							<select class="MySelectBoxClass" style="width: 115px;" id="cbPostion">
								<option value = "-1">Tất cả vị trí</option>
								<option value = "1">Có vị trí</option>
								<option value = "0">Không có vị trí</option>
							</select>
                    	</div>
                    	
                    	<div class="BoxSelect BoxSelect11">                    	
							<select class="MySelectBoxClass" style="width: 115px;" id="cbStaffType">
								<s:iterator value="lstStaffType">
									<option value='<s:property/>'><s:property/></option>
								</s:iterator>
							</select>
                    	</div> --%>
                    	
<!--                     	Tam thoi an di -->
						<!-- Chua xu ly tim kiem -->
						<!-- <input id="shopCode" placeholder="Mã đơn vị" type="text" class="InputTextStyle InputText1Style" style="width: 70px;padding-right:5px"/>
						<input id="shopName" placeholder="Tên đơn vị" type="text" class="InputTextStyle InputText2Style" style="width: 120px"/>
						
						<div class="BtnLSection">
							<button id="btSearchShop" onclick="SuperviseSales.loadTreeStaff(1);" class="BtnGeneralStyle">Tìm kiếm</button>
						</div> -->
						
						<div class="Clear"></div>
<!-- 						<div id="tree" style="max-height:500px;overflow:auto;"></div> -->
						<div id="promotionShopGrid" style="max-height: 358px">
                             <table id="treeGrid" ></table> 
                   		</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="Clear"></div>
</div>
<div class="GeneralMilkBox">
	<div class="GeneralMilkTopBox">
		<div class="GeneralMilkBtmBox"></div>
	</div>
</div>
<div id="contextMenu" class="easyui-menu" style="width:120px;display:none">
    <div id="cmDoanhSoNgay" >Doanh số ngày</div>
    <div id="cmLuyKeThang" >Doanh số lũy kế</div>
    <div id="cmXemLoTrinh" >Xem lộ trình</div>
</div>
<div id="monthAmountPlan" class="easyui-dialog" data-options="closed:true,modal:true" title="Doanh số bán hàng lũy kế tháng: TBHV000012 - Nguyễn Tuấn Việt" style="width:645px;height:500px;">
	<div id="monthAmountPlanContainer"></div>
</div>
<!-- Begin popUp Customer Of NVBH (mau htm)-->
<div id="showCustomerDetail" style="display: none;">
	<div class="MapPopupSection MapPopup4Section">
	<h2 class="Title2Style" id="h2CustomerCode">003 - Nguyễn Thị Thập</h2>
	<div class="MPContent">
		<dl class="Dl1Style FixFloat">
			<dt>Địa chỉ:</dt>
			<dd id="ddAddress"></dd><br />
			<dt>Di động:</dt>
			<dd id="ddMobiPhone">0</dd><br />
			<dt>Cố định:</dt>
			<dd id="ddPhone">0</dd>
		</dl>
		<div class="GeneralTable Table1Section" style="width: 100%;" >
			<div id="tableDoanhSo" class="FixFloat" style="float: left; width: 70%">
				<table width="100%" border="0" cellspacing="0" cellpadding="0">
	           	<colgroup>
	           		<col style="width:77px;" />
	           		<col style="width:55px;" />
	           		<col style="width:70px;" />
	           		<col style="width:70px;" />
	           	</colgroup>
	           	<thead>
		           	<tr>
			           	<th class="FirstThStyle EndTopThStyle" colspan="4" style="padding: 3px 0;">Doanh số</th>
		           	</tr>
					<tr>
						<th class="FirstThStyle" colspan="2" style="padding: 3px 0;">Nội dung</th>
						<th style="padding: 3px 0;">Đã duyệt</th>
						<th style="padding: 3px 0;">Tổng</th>
					</tr>
				</thead>
	               <tbody>
	                <tr style="height: 22px;">
						<td class="FirstTdStyle" rowspan="2" style="padding: 1px;">
							<div class="AlignLCols TextOnMap">Ngày hiện tại</div>
						</td>
						<td  style="padding: 1px;"><div class="TextOnMap" style="padding: 0px 9px;">Doanh số</div></td>
						<td  style="padding: 1px;"><div class="AlignRCols TextOnMap" style="padding: 0px 9px;"></div></td>
						<td  style="padding: 1px;"><div class="AlignRCols TextOnMap" style="padding: 0px 9px;"></div></td>
					</tr>
					<tr style="height: 22px;">
						<td style="padding: 1px;"><div class="TextOnMap" style="padding: 0px 9px;">Sản lượng</div></td>
						<td style="padding: 1px;"><div class="AlignCCols TextOnMap" style="padding: 0px 9px;"></div></td>
						<td style="padding: 1px;"><div class="AlignCCols TextOnMap" style="padding: 0px 9px;"></div></td>
						
					</tr>
					<tr style="height: 22px;">
						<td rowspan="2" style="padding: 1px;">
							<div class="AlignLCols TextOnMap">Lũy kế</div>
						</td>
						<td style="padding: 1px;"><div class="TextOnMap" style="padding: 0px 9px;">Doanh số</div></td>
						<td style="padding: 1px;"><div class="AlignCCols TextOnMap" style="padding: 0px 9px;"></div></td>
						<td style="padding: 1px;"><div class="AlignCCols TextOnMap" style="padding: 0px 9px;"></div></td>
					</tr>
					<tr style="height: 22px;">
						<td style="padding: 1px;"><div class="TextOnMap" style="padding: 0px 9px;">Sản lượng</div></td>
						<td style="padding: 1px;"><div class="AlignCCols TextOnMap" style="padding: 0px 9px;"></div></td>
						<td style="padding: 1px;"><div class="AlignCCols TextOnMap" style="padding: 0px 9px;"></div></td>

					</tr>
	               </tbody>
	           </table>
           </div>
           <div id="tableGheTham" class="FixFloat" style="float: left; width: 30%">
				<table width="100%" border="0" cellspacing="0" cellpadding="0">
	           	<colgroup>
	           		<col style="width:50px;" />
	           		<col style="width:50px;" />
	           	</colgroup>
	           	<thead>
		           	<tr>
			           	<th class="FirstThStyle EndTopThStyle" colspan="2"style="padding: 3px 0;">Ghé thăm</th>
		           	</tr>
					<tr>
						<th class="FirstThStyle"style="padding: 3px 0;">Bắt đầu</th>
						<th style="padding: 3px 0;">Kết thúc</th>
					</tr>
				</thead>
	               <tbody>
	                <tr style="height: 22px;">
						<td class="FirstTdStyle" style="padding: 1px;"><div class="AlignCCols TextOnMap" style="padding: 0px 9px;">VVV</div></td>
						<td style="padding: 1px;"><div class="AlignCCols TextOnMap" style="padding: 0px 9px;">VVV</div></td>
					</tr>
	               </tbody>
	           </table>
           </div>
           <div class="Clear"></div>
			<div class="FixFloat ContentFunc">
				<p class="Text2Style" style="color:red">* Đơn vị (x 1000 vnđ)</p> 
			</div>
		</div>
	</div>
	</div>
</div>
<!-- End popUp Customer Of NVBH (mau htm)-->

<div id="dialogDSNgayGDM" class="easyui-dialog" title="Doanh số bán hàng ngày: NVGS000012 - Nguyễn Tuấn Việt" style="width: 712px; height: 450px;" data-options="closed:true,modal:true">
	<div class="PopupContentMid">
		<div class="GeneralTable Table3Section">
			<table width="100%" border="0" cellspacing="0" cellpadding="0">
				<colgroup>
					<col style="width: 60px;" />
					<col style="width: 72px;" />
					<col style="width: 160px;" />
					<col style="width: 50px;" />
					<col style="width: 50px;" />
					<col style="width: 83px;" />
					<col style="width: 83px;" />
					<col style="width: 83px;" />
					<col style="width: 83px;" />
				</colgroup>
				<thead>
					<tr>
						<th rowspan="2" class="FirstThStyle">Vùng</th>
						<th rowspan="2" class="FirstThStyle">Mã TBHV</th>
						<th rowspan="2">TBHV</th>
						<th colspan="2" class="EndTopThStyle">Đơn hàng</th>
						<th colspan="4" class="EndThStyle EndTopThStyle">Doanh số(ĐVT:1000 VNĐ)</th>
					</tr>
					<tr>
						<th>KH</th>
						<th>TH</th>
						<th>Kế hoạch</th>
						<th>Thực hiện</th>
						<th>Duyệt</th>
						<th class="EndThStyle">Còn lại</th>
					</tr>
				</thead>
				<tbody id="gdmSaleBody">
					
				</tbody>
				<tfoot id="gdmFooter">
					
				</tfoot>
			</table>
		</div>
	</div>
</div>
<div id="dialogDSNgayTBHV" class="easyui-dialog" title="Doanh số bán hàng lũy kế tháng: TBHV000012 - Nguyễn Ngọc Khoa" style="width: 635px; height: 500px;" data-options="closed:true,modal:true">
	<div class="PopupContentMid">
		<div class="GeneralTable Table2Section">
			<table width="100%" border="0" cellspacing="0" cellpadding="0">
				<colgroup>
					<col style="width: 92px;" />
					<col style="width: 178px;" />
					<col style="width: 92px;" />
					<col style="width: 87px;" />
					<col style="width: 87px;" />
					<col style="width: 83px;" />
				</colgroup>
				<thead>
					<tr>
						<th rowspan="2" class="FirstThStyle">NPP</th>
						<th rowspan="2">GSNPP</th>
						<th colspan="4" class="EndThStyle EndTopThStyle">Doanh số - ĐVT: x1000 VNĐ</th>
					</tr>
					<tr>
						<th>Kế hoạch</th>
						<th>Thực hiện</th>
						<th>Duyệt</th>
						<th class="EndThStyle">Còn lại</th>
					</tr>
				</thead>
				<tbody id="tbhvSaleBody">

				</tbody>
				<tfoot id="tbhvSaleFooter">
					
				</tfoot>
			</table>
		</div>
	</div>
</div>

<!-- Begin popUp Doanh so, san luong Ngay, Luy ke NVGS (mau htm)-->
<div id="dialogDSNgayNVGS" class="easyui-dialog" title="Doanh số bán hàng ngày: NVGS000012 - Nguyễn Tuấn Việt" style="width: 1000px; height: 450px;" data-options="closed:true,modal:true">
	<div class="PopupContentMid">
		<div class="GeneralForm Search1Form">
		  	<label class="LabelStyle Label1Style">Mã NPP <span class="ReqiureStyle"> *</span></label>
	    	<div class="BoxSelect BoxSelect2">
	    		<input type="text" class="InputTextStyle InputText4Style" id="cbxShop" maxlength="50">
	    	</div>
		 </div>
		<div style="float:right; margin: -6px 6px 6px 0; color: #215ea2;">
		    <input type="radio" onclick="SuperviseSales.changeDaySaleNVGS()" value="1" name="groupGS" id="rNVGSNgay">
		    <label for="rNVGSNgay">Ngày</label>     
		    &nbsp;&nbsp;&nbsp;&nbsp;
		    <input type="radio" onclick="SuperviseSales.changeDaySaleNVGS()" value="2" name="groupGS" id="rNVGSLuyKe">
		    <label for="rNVGSLuyKe">Lũy kế</label>
	  	</div>
  		<div class="Clear"></div>
		<div class="GeneralTable">
			<table width="100%" border="0" cellspacing="0" cellpadding="0">
				<colgroup>
					<!-- <col style="width: 92px;" />
					<col style="width: 200px;" />
					<col style="width: 83px;" />
					<col style="width: 83px;" />
					<col style="width: 83px;" />
					<col style="width: 83px;" />
					<col style="width: 83px;" />
					<col style="width: 83px;" />
					<col style="width: 83px;" />
					<col style="width: 83px;" /> -->
					<col style="width: 80px;">
					<col style="width: 200px;">
					
<!-- 					Danh so theo tuyen -->
					<s:if test='sysSaleRoute.equals("2")' >
						<col style="width: 80px;">
						<col style="width: 120px;">
					</s:if>
					
					
					<col style="width: 8%;">
					<col style="width: 8%;">
					<col style="width: 8%;">
					<col style="width: 8%;">
					<col style="width: 8%;">
					<col style="width: 8%;">
					<col style="width: 8%;">
					<col style="width: 8%;">
				</colgroup>
				<thead>
					<tr>
						<th rowspan="2" class="FirstThStyle">Mã NVBH</th>
						<th rowspan="2">Tên NVBH</th>
						
						<!-- Danh so theo tuyen -->
						<s:if test='sysSaleRoute.equals("2")'>
							<th rowspan="2">Mã Tuyến</th>
							<th rowspan="2">Tên Tuyến</th>
						</s:if>
						
						
						<th colspan="4" class="EndTopThStyle">Doanh số</th>
						<th colspan="4" class="EndThStyle EndTopThStyle">Sản lượng</th>
					</tr>
					<tr>
						<th>Kế hoạch</th>
						<th>Thực hiện (đã duyệt)</th>
						<th>Thực hiện (tổng)</th>
						<th>Tiến độ</th>
						<th>Kế hoạch</th>
						<th>Thực hiện (đã duyệt)</th>
						<th>Thực hiện (tổng)</th>
						<th class="EndThStyle">Tiến độ</th>
					</tr>
				</thead>
				<tbody id="nvgsSaleBody">
					<!-- <tr>
						<td class="FirstTdStyle"><div class="AlignCCols">1212</div></td>
						<td><div class="AlignCCols">a</div></td>
						<td><div class="AlignLCols">sd</div></td>
						<td><div class="AlignLCols">d</div></td>
						<td><div class="AlignRCols">e</div></td>
						<td><div class="AlignRCols">sd</div></td>
						<td><div class="AlignRCols">s</div></td>
						<td><div class="AlignRCols">s</div></td>
						<td><div class="AlignRCols">s</div></td>
						<td><div class="AlignRCols">s</div></td>
					</tr>
					<tr class="FooterTable">
						<td class="FirstTdStyle" colspan="2"><div class="AlignRCols">Tổng cộng</div></td>
						<td><div class="AlignRCols">E</div></td>
						<td><div class="AlignRCols">R</div></td>
						<td><div class="AlignRCols">R</div></td>
						<td><div class="AlignRCols">E</div></td>
						<td><div class="AlignRCols">R</div></td>
						<td><div class="AlignRCols">R</div></td>
						<td><div class="AlignRCols">R</div></td>
						<td class="EndTdStyle"><div class="AlignRCols">D</div></td>
					</tr> -->
				</tbody>
				<!-- vuongmq; 21/08/2015; khong su sung -->
				<!-- <tfoot id="nvgsFooter">
					<tr class="FooterTable">
						<td class="FirstTdStyle" colspan="2"><div class="AlignRCols">Tổng cộng</div></td>
						<td><div class="AlignRCols">E</div></td>
						<td><div class="AlignRCols">R</div></td>
						<td><div class="AlignRCols">R</div></td>
						<td><div class="AlignRCols">E</div></td>
						<td><div class="AlignRCols">R</div></td>
						<td><div class="AlignRCols">R</div></td>
						<td><div class="AlignRCols">R</div></td>
						<td class="EndTdStyle"><div class="AlignRCols">D</div></td>
					</tr>
				</tfoot> -->
			</table>
		</div>
	</div>
</div>
<!-- End popUp Doanh so, san luong Ngay, Luy ke NVGS (mau htm)-->

<!-- Begin popUp Doanh so, san luong Ngay, Luy ke NVBH (mau htm)-->
<div id="dialogDSNgayNVBH" class="easyui-dialog" title="Doanh số bán hàng ngày: NVBH000012 - Trần Tuấn Hậu" style="width: 821px; height: 450px;" data-options="closed:true,modal:true">
	<div class="PopupContentMid">
		<div style="float:right; margin: -6px 6px 6px 0; color: #215ea2;">
		    <input type="radio" id="rNVBHNgay" name="groupBH" value="1" onclick="SuperviseSales.changeDaySaleNVBH()">                     
		    <label for="rNVBHNgay">Ngày</label>     
		    &nbsp;&nbsp;&nbsp;&nbsp;
		    <input type="radio" id="rNVBHLuyKe" name="groupBH" value="2" onclick="SuperviseSales.changeDaySaleNVBH()">
		    <label for="rNVBHLuyKe">Lũy kế</label>
	  	</div>
		<div class="GeneralTable Table4Section ScrollBodyTable">
			<table width="100%" border="0" cellspacing="0" cellpadding="0">
				<colgroup>
					<col style="width: 40px;" />
					<col style="width: 60px;" />
					<col style="width: 120px;" />
					<col style="width: 190px;" />
					<col style="width: 80px;" />
					<col style="width: 80px;" />
					<col style="width: 80px;" />
					<col style="width: 80px;" />
				</colgroup>
				<thead>
					<tr>
						<th rowspan="2" class="FirstThStyle">STT</th>
						<th rowspan="2">Mã KH</th>
						<th rowspan="2">Tên khách hàng</th>
						<th rowspan="2">Địa chỉ</th>
						<th colspan="2" class="EndTopThStyle">Doanh số</th>
						<th colspan="2" class="EndTopThStyle">Sản lượng</th>
					</tr>
					<tr>
						<th>Đã duyệt</th>
						<th>Tổng</th>
						<th>Đã duyệt</th>
						<th>Tổng</th>
					</tr>
				<thead>
				<tbody id="bodyNVBH">
					<!-- <tr>
						<td class="FirstTdStyle"><div class="AlignCCols">1212</div></td>
						<td><div class="AlignCCols">a</div></td>
						<td><div class="AlignLCols">sd</div></td>
						<td><div class="AlignLCols">d</div></td>
						<td><div class="AlignRCols">e</div></td>
						<td><div class="AlignRCols">sd</div></td>
						<td><div class="AlignRCols">s</div></td>
						<td><div class="AlignRCols">s</div></td>
					</tr>
					<tr class="FooterTable">
						<td class="FirstTdStyle" colspan="4"><div class="AlignRCols">Tổng cộng</div></td>
						<td><div class="AlignRCols">E</div></td>
						<td><div class="AlignRCols">R</div></td>
						<td><div class="AlignRCols">R</div></td>
						<td class="EndTdStyle"><div class="AlignRCols">D</div></td>
					</tr> -->
				</tbody>
			</table>
		</div>
	</div>
</div>
<!-- End popUp Doanh so, san luong Ngay, Luy ke NVBH (mau htm)-->

<!-- Begin popUp Doanh so, san luong Ngay, Luy ke Shop (mau htm)-->
<div id="dialogDSNgayShop" class="easyui-dialog" title="Doanh số bán hàng ngày: TCT - Tổn công ty" style="width: 1000px; height: 450px;" data-options="closed:true,modal:true">
	<div class="PopupContentMid">
		<div id="divUpShopParent" style="float:left; margin: -12px 0px 3px 0px; color: #215ea2; text-decoration: underline;"><a id="upShopParent" href="javascript:void(0)" title="Xem báo cáo đơn vị cha"><img width="30" height="25" src="/resources/images/icon-back1.png"/></a></div>
		<div style="float:right; margin: -6px 6px 6px 0; color: #215ea2;">
		    <input type="radio" onclick="SuperviseSales.changeDaySaleShop()" value="1" name="groupShop" id="rShopNgay">
		    <label for="rShopNgay">Ngày</label>     
		    &nbsp;&nbsp;&nbsp;&nbsp;
		    <input type="radio" onclick="SuperviseSales.changeDaySaleShop()" value="2" name="groupShop" id="rShopLuyKe">
		    <label for="rShopLuyKe">Lũy kế</label>
	  	</div>
  		<div class="Clear"></div>
		<div class="GeneralTable">
			<table width="100%" border="0" cellspacing="0" cellpadding="0">
				<colgroup>
					<!-- <col style="width: 92px;" />
					<col style="width: 200px;" />
					<col style="width: 83px;" />
					<col style="width: 83px;" />
					<col style="width: 83px;" />
					<col style="width: 83px;" />
					<col style="width: 83px;" />
					<col style="width: 83px;" />
					<col style="width: 83px;" />
					<col style="width: 83px;" /> -->
					<col style="width: 80px;">
					<col style="width: 200px;">
					
					<col style="width: 8%;">
					<col style="width: 8%;">
					<col style="width: 8%;">
					<col style="width: 8%;">
					<col style="width: 8%;">
					<col style="width: 8%;">
					<col style="width: 8%;">
					<col style="width: 8%;">
				</colgroup>
				<thead>
					<tr>
						<th rowspan="2" class="FirstThStyle">Mã ĐV</th>
						<th rowspan="2">Tên ĐV</th>						
						
						<th colspan="4" class="EndTopThStyle">Doanh số</th>
						<th colspan="4" class="EndThStyle EndTopThStyle">Sản lượng</th>
					</tr>
					<tr>
						<th>Kế hoạch</th>
						<th>Thực hiện (đã duyệt)</th>
						<th>Thực hiện (tổng)</th>
						<th>Tiến độ</th>
						<th>Kế hoạch</th>
						<th>Thực hiện (đã duyệt)</th>
						<th>Thực hiện (tổng)</th>
						<th class="EndThStyle">Tiến độ</th>
					</tr>
				</thead>
				<tbody id="shopSaleBody">
					<!-- <tr>
						<td class="FirstTdStyle"><div class="AlignCCols">1212</div></td>
						<td><div class="AlignCCols">a</div></td>
						<td><div class="AlignLCols">sd</div></td>
						<td><div class="AlignLCols">d</div></td>
						<td><div class="AlignRCols">e</div></td>
						<td><div class="AlignRCols">sd</div></td>
						<td><div class="AlignRCols">s</div></td>
						<td><div class="AlignRCols">s</div></td>
						<td><div class="AlignRCols">s</div></td>
						<td><div class="AlignRCols">s</div></td>
					</tr>
					<tr class="FooterTable">
						<td class="FirstTdStyle" colspan="2"><div class="AlignRCols">Tổng cộng</div></td>
						<td><div class="AlignRCols">E</div></td>
						<td><div class="AlignRCols">R</div></td>
						<td><div class="AlignRCols">R</div></td>
						<td><div class="AlignRCols">E</div></td>
						<td><div class="AlignRCols">R</div></td>
						<td><div class="AlignRCols">R</div></td>
						<td><div class="AlignRCols">R</div></td>
						<td class="EndTdStyle"><div class="AlignRCols">D</div></td>
					</tr> -->
				</tbody>
				<!-- vuongmq; 21/08/2015; khong su sung -->
				<!-- <tfoot id="nvgsFooter">
					<tr class="FooterTable">
						<td class="FirstTdStyle" colspan="2"><div class="AlignRCols">Tổng cộng</div></td>
						<td><div class="AlignRCols">E</div></td>
						<td><div class="AlignRCols">R</div></td>
						<td><div class="AlignRCols">R</div></td>
						<td><div class="AlignRCols">E</div></td>
						<td><div class="AlignRCols">R</div></td>
						<td><div class="AlignRCols">R</div></td>
						<td><div class="AlignRCols">R</div></td>
						<td class="EndTdStyle"><div class="AlignRCols">D</div></td>
					</tr>
				</tfoot> -->
			</table>
		</div>
	</div>
</div>
<!-- End popUp Doanh so, san luong Ngay, Luy ke Shop (mau htm)-->

<!-- Begin popUp NVBH (mau htm)-->
<div id="markerStaff" style="display: none;">
<div class="GeneralTable Table1Section" style="width: 100%;" >
	<table width="100%" border="0" cellspacing="0" cellpadding="0">
		<colgroup>
			<col style="width: 30%;" />
			<col style="width: 10%;" />
			<col style="width: 20%;" />
			<col style="width: 20%;" />
			<col style="width: 20%;" />
		</colgroup>
		<thead>
			<tr>
				<th class="FirstThStyle" colspan="2" style="padding: 3px 0;"><s:text name="jsp.sale.supervise.saler.marker.content.title"></s:text></th>
				<th style="padding: 3px 0;"><s:text name="jsp.sale.supervise.saler.marker.content.header.plan"></s:text></th>
				<th style="padding: 3px 0;"><s:text name="jsp.sale.supervise.saler.marker.content.header.actual.approved"></s:text></th>
				<th class="EndThStyle" style="padding: 3px 0;"><s:text name="jsp.sale.supervise.saler.marker.content.header.progress"></s:text></th>
			</tr>
		</thead>
		<tbody>
			<tr style="height: 22px;">
				<td class="FirstTdStyle" rowspan="2" style="padding: 1px;">
					<div class="AlignLCols TextOnMap">
						<%-- 
						<a href="javascript:void(0)" data-bind="attr: {onclick: daySaleUrl}">Ngày hiện tại</a>
						--%>
						<s:text name="jsp.sale.supervise.saler.marker.content.data.current"></s:text>
					</div>
				</td>
				<td  style="padding: 1px;"><div class="TextOnMap" style="padding: 0px 9px;"><s:text name="jsp.sale.supervise.saler.marker.content.data.sales"></s:text></div></td>
				<td  style="padding: 1px;"><div class="AlignRCols TextOnMap" style="padding: 0px 9px;" data-bind="text: formatFloatValue(dayAmountPlan(),'<s:property value="numFloat"/>')"></div></td>
								<td  style="padding: 1px;"><div class="AlignRCols TextOnMap" style="padding: 0px 9px;" data-bind="text: formatFloatValue(dayAmount(),'<s:property value="numFloat"/>')"></div></td>
				<td style="padding: 1px;">
					<div class="AlignCCols TextOnMap" style="padding: 0px 9px;"
						data-bind="css: {Color1Style: dayAmountProgress() < standardProgress()}, text: dayAmountProgressText">
					</div>
				</td>
			</tr>
			<tr style="height: 22px;">
				<td style="padding: 1px;"><div class="TextOnMap" style="padding: 0px 9px;"><s:text name="jsp.sale.supervise.saler.marker.content.data.quantity"></s:text></div></td>
				<td style="padding: 1px;"><div class="AlignRCols TextOnMap" style="padding: 0px 9px;" data-bind="text: formatCurrency(dayQuantityPlan())"></div></td>
				<td style="padding: 1px;"><div class="AlignRCols TextOnMap" style="padding: 0px 9px;" data-bind="text: formatCurrency(dayQuantity())"></div></td>
				<td style="padding: 1px;">
					<div class="AlignCCols TextOnMap" style="padding: 0px 9px;" data-bind="css: {Color1Style: dayQuantityProgress() < standardProgress()}, text: dayQuantityProgressText"></div>
				</td>
			</tr>
			<tr style="height: 22px;">
				<td class="FirstTdStyle" rowspan="2" style="padding: 1px;">
					<div class="AlignLCols TextOnMap"><s:text name="jsp.sale.supervise.saler.marker.content.data.month.accumulate"></s:text></div>
				</td>
				<td style="padding: 1px;"><div class="TextOnMap" style="padding: 0px 9px;"><s:text name="jsp.sale.supervise.saler.marker.content.data.sales"></s:text></div></td>
				<td style="padding: 1px;"><div class="AlignRCols TextOnMap" style="padding: 0px 9px;" data-bind="text: monthAmountPlanText()"></div></td>
				<td style="padding: 1px;"><div class="AlignRCols TextOnMap" style="padding: 0px 9px;" data-bind="text: monthAmountText()"></div></td>
				<td style="padding: 1px;"><div class="AlignCCols TextOnMap" style="padding: 0px 9px;"
						data-bind="css: {Color1Style: monthAmountProgress() < standardProgress()}, text: monthAmountProgressText"></div></td>
			</tr>
			<tr style="height: 22px;">
				<td style="padding: 1px;"><div class="TextOnMap" style="padding: 0px 9px;"><s:text name="jsp.sale.supervise.saler.marker.content.data.quantity"></s:text></div></td>
				<td style="padding: 1px;"><div class="AlignRCols TextOnMap" style="padding: 0px 9px;" data-bind="text: formatCurrency(monthQuantityPlan())"></div></td>
				<td style="padding: 1px;"><div class="AlignRCols TextOnMap" style="padding: 0px 9px;" data-bind="text: formatCurrency(monthQuantity())"></div></td>
				<td style="padding: 1px;"><div class="AlignCCols TextOnMap" style="padding: 0px 9px;"
						data-bind="css: {Color1Style: monthQuantityProgress() < standardProgress()}, text: monthQuantityProgressText"></div></td>
			</tr>
		</tbody>
	</table>
</div>
</div>
<!-- End popUp NVBH (mau htm)-->
<s:hidden id="shopId" name="shopId"></s:hidden>
<s:hidden id="type" name="type"></s:hidden>
<script>
$(document).ready(function(){
	$('.MySelectBoxClass').customStyle();
	$('#cbTBHV').change(function(){
		SuperviseSales.reloadMarker();
	});
	$('#cbGSNPP').change(function(){
		SuperviseSales.reloadMarker();
	});
	$('#cbNVBH').change(function(){
		SuperviseSales.reloadMarker();
	});
	
	//trungtm6 comment
	if($('#type').val().trim() != ''){
		var roleType = parseInt($('#type').val().trim());
		if (roleType == StaffSpecType.VIETTEL_ADMIN) {//QL
			$('#cbTBHV').attr('checked','checked');
		} else if (roleType == StaffSpecType.MANAGER) {//QL
			$('#cbGSNPP').attr('checked','checked');
		} else if (roleType == StaffSpecType.SUPERVISOR){//GDM
			$('#cbNVBH').attr('checked','checked');
// 			$('#cbGSNPP').attr('checked','checked');
		}
	}
	
	$('#bigMap').css('height',($(window).height()-167)+'px');
	$(window).resize(function() {
		$('#bigMap').css('height',($(window).height()-167)+'px');
		$('.datagrid-body').css('max-height',($(window).height()-300)+'px');
		if(Vietbando._map!=null){
			Vietbando._map.addControl(new VLargeMapControl(), new VControlPosition(V_ANCHOR_TOP_RIGHT, new VSize(5, 5)));
			$('.ZoomPinLeft').css('left','-50px');
		}
	});
	SuperviseSales._lstNodeExcep = new Map();
	
	$('.datagrid-body').css('max-height',($(window).height()-300)+'px');
	
	VTMapUtil.loadMapResource(function(){
		var map = ViettelMap.loadBigMap('bigMap',null,null,4,null);
		if(map != null && map != undefined) {
			SuperviseSales.getListStaffForShop();
			SuperviseSales.loadTreeStaff();
		}
	});
	
	//trungtm6 comment
	/* $('#cbLHL').change(function(){
		SuperviseSales._isLHL=1;
		if($('.StaffSelectBtmSection .OnStyle').length==1){// show (display:block) danh sách đơn vị quản lý để load lại cây
			$('#titleStaff').addClass('OffStyle');
			$('#titleStaff').removeClass('OnStyle');
			$('#listStaff').toggle();
		}
		if(!$('#cbLHL').is(':disabled') && $('#cbLHL').is(':checked')){
			$('#cbTBHV').attr('disabled','disabled');
			$('#cbGSNPP').attr('disabled','disabled');
			$('#cbNVBH').attr('disabled','disabled');
			SuperviseSales.loadTreeStaff();//load lại cây
		}else{
			SuperviseSales.loadTreeStaff();//load lại cây
			if($('#type').val().trim()!=''){
				var roleType = parseInt($('#type').val().trim());
				if(roleType==6 || roleType==8){
					$('#cbTBHV').removeAttr('disabled');
					$('#cbGSNPP').removeAttr('disabled');
					$('#cbNVBH').removeAttr('disabled');
				}
				if(roleType==7){
					$('#cbGSNPP').removeAttr('disabled');
					$('#cbNVBH').removeAttr('disabled');
				}
				if(roleType==5 || roleType==9){
					$('#cbNVBH').removeAttr('disabled');
				}
			}
		}
		setTimeout(function() {
			SuperviseSales.reloadMarker();			
		}, 500);
	}); */
	
	
	$('#shopCode').bind('keyup',function(event){
			if(event.keyCode == keyCodes.ENTER){		   				
				SuperviseSales.loadTreeStaff(1);
			}
	});
	$('#shopName').bind('keyup',function(event){
		if(event.keyCode == keyCodes.ENTER){		   				
			SuperviseSales.loadTreeStaff(1);
		}
	});
	$('#bigMap_VIWBtnClose').live('click', function() {
		SuperviseSales._idStaffSelected = null;
	});
	$('#fromTime, #toTime').clockpicker();
	$('#fromTime').val('07:00');
	$('#toTime').val('18:00');
	
	SuperviseSales.sysDateFromDB = dates.convert('<s:property value="sysdateMMddYyyyStr" />');
	if (SuperviseSales.sysDateFromDB == null){
		SuperviseSales.sysDateFromDB = new Date();
	}
	$('input[type=radio][name=kmType]').click(function(e){
		var kmType = $(e.target).val();
// 		SuperviseSales.calcNumKm(parseInt(kmType));
		SuperviseSales.numKm = 0;
		SuperviseSales.calcShortestDistance(parseInt(kmType));
	});
	$('#btCapNhatViTri').bind('click', function() {
		SuperviseSales.reloadTreeStaff()
	}); 
// 	SuperviseSales.numPointLine = Number('<s:property value="numPointLine" />');
});
</script>
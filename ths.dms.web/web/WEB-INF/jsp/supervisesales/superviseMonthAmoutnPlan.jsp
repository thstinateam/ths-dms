<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<input type="hidden" id="titleDialogAmount" value="Doanh số bán hàng lũy kế tháng: <s:property value="staff.staffCode" /> - <s:property value="staff.staffName" />" />
		<div class="PopupContentTop">
        	<div class="PCInTop PCInTopF" >
                <dl class="Dl1Style">
                    <dt>Số ngày bán hàng kế hoạch:</dt>
                    <dd><s:property value="soKeHoach" /></dd>
                    <dt>Số ngày bán hàng đã qua:</dt>
                    <dd><s:property value="soBHDaQua" /></dd>
                    <dt>Tiến độ chuẩn:</dt>
                    <dd><s:property value="tienDoChuan" />%</dd>
                </dl>
            </div>
        </div>
        <div class="PopupContentMid">
            <div class="GeneralTable Table2Section">
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <colgroup>
                        <col style="width:92px;" />
                        <s:if test="type==6">
                        	<col style="width:92px;" />
                        </s:if>
                        <col style="width:178px;" />
                        <col style="width:92px;" />
                        <col style="width:87px;" />
                        <col style="width:59px;" />
                        <col style="width:83px;" />
                    </colgroup>
                    <thead>
                        <tr>
                            <th rowspan="2" class="FirstThStyle" id="titleStaffCode" >NPP</th>
                            <s:if test="type == 6">
                            	<th rowspan="2" class="FirstThStyle" id="titleTBHV" >Mã TBHV</th>
                            </s:if>
                            <th rowspan="2" id="titleStaffType" >GSNPP</th>
                            <th colspan="5" class="EndThStyle EndTopThStyle">Doanh số - ĐVT: x1000 VNĐ</th>
                        </tr>
                        <tr>
                            <th>Kế hoạch</th>
                            <th>Thực hiện</th>
                            <th>Duyệt</th>
                            <th>Tiến độ</th>
                            <th class="EndThStyle">Còn lại</th>
                        </tr>
                    </thead>
                    <tbody>
                    	<s:iterator value="amountPlanStaffDetailVOs" >
                    		<tr>
	                            <td class="FirstTdStyle"><div class="AlignLCols">
	                            	<s:if test="type==7 || type == 6">
	                            		<s:property value="shopCode" />
	                            	</s:if>
	                            	<s:else><s:property value="staffCode" /></s:else>
	                            	 
	                            </div></td>
	                            <s:if test="type == 6">
	                            	<td class="FirstTdStyle"><div class="AlignLCols">
	                            	<s:property value="staffCode" />
	                            </div></td>
	                            </s:if>
	                            <td><div class="AlignLCols LCS_STAFF_CHECKED" staffName="<s:property value="staffName" />" staff-id="<s:property value="staffId" />" >                            	
	                            	
	                            </div>
	                            </td>
	                            <td><div class="AlignRCols"><s:property value="convertMoney(monthAmountPlan)" /></div></td>
	                            <td><div class="AlignRCols"><s:property value="convertMoney(monthAmount)" /></div></td>
	                            <td><div class="AlignRCols"><s:property value="convertMoney(monthApprovedAmount)" /></div></td>
	                            <td>
	                            	<s:if test="tienDoChuan>progress">
	                            		<div class="AlignCCols Color1Style"><s:property value="progress" />%</div></s:if>
	                            	<s:else><div class="AlignCCols"><s:property value="progress" />%</div></s:else>	                            	
	                            </td>
	                            <td>
	                            	<div class="AlignRCols">
	                            	<s:if test="0>exist">0</s:if>
	                            	<s:else><s:property value="convertMoney(exist)" /></s:else>
	                            	</div>
	                            </td>
                        	</tr> 
                    	</s:iterator>                                               
                    </tbody>
                    <tfoot>
                        <tr>
                        	<s:if test="type == 6">
                        		<td></td> 
                        	</s:if>
                            <td colspan="2" class="FirstTdStyle"><div class="AlignCCols">Tổng cộng</div></td>
                            <td><div class="AlignRCols"><s:property value="convertMoney(monthAmountPlan)" /> </div></td>
                            <td><div class="AlignRCols"><s:property value="convertMoney(monthAmount)" /></div></td>
                            <td><div class="AlignRCols"><s:property value="convertMoney(monthApprovedAmount)" /></div></td><!-- convertMoney(monthApprovedAmount) -->
                            <td>
                            	<s:if test="tienDoChuan>progress">
                            		<div class="AlignCCols Color1Style"><s:property value="progress" />%</div>
                            	</s:if>
                            	<s:else>
                            		<div class="AlignCCols"><s:property value="progress" />%</div>
                            	</s:else>
                            </td>
                            <td>
                            	<s:if test="exist>0">
                            		<div class="AlignRCols"><s:property value="convertMoney(exist)" /></div>
                            	</s:if>
                            	<s:else>
                            		<div class="AlignRCols">0</div>
                            	</s:else>
                            </td>                            
                        </tr>
                    </tfoot>
                </table>
            </div>        
		</div>
<script type="text/javascript">
$(document).ready(function(){
	$('.LCS_STAFF_CHECKED').each(function(){
		var staffId = $(this).attr('staff-id');
		var staffName = $(this).attr('staffName');
		if(SuperviseSales.checkLatLngOnStaff(staffId)){
			$(this).html('<a href="javascript:void(0)" onclick="return SuperviseSales.selectStaffOnUI('+
					staffId+');" >'+Utils.XSSEncode(staffName)+'</a>');
		}else{
			$(this).html(Utils.XSSEncode(staffName));
		}
	});
});
</script>

<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@page import="ths.dms.helper.Configuration"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>

<div class="BreadcrumbSection">
    <ul class="ResetList FixFloat BreadcrumbList">
        <li class="Sprite1"><a href="javascript:void(0)">Giám sát</a></li>
        <li><span>Giám sát lộ trình NVBH</span></li>
    </ul>
</div>
<div class="CtnOneColSection">
	<div class="ContentSection">
		<div class="ToolBarSection">
			<div class="GuideMapSection">
				<div class="GuideMapInSection">
					<label class="LabelStyle Label1Style"><span class="Sprite1">NVBH</span> </label> 
					<label class="LabelStyle Label2Style"><span class="Sprite1">GSNPP</span> </label>
					<div class="Clear"></div>
				</div>
			</div>
			<div class="Clear"></div>
		</div>
		<div class="MapRouteSection" >
			<div class="ShadowBgSection"></div>
			<div id="bigMap" style="width: 100%; height: 500px; position: relative;"></div>
			<div class="StaffSelectSection">
				<div class="StaffSelectBtmSection">
					<p id="titleStaff" onclick="SupperviseSalesStaff.toggleListStaff();" class="StaffSelectText Sprite1 OffStyle">Đơn vị quản lý</p>
					<div id="listStaff" class="StaffTreeSection SearchSection GridSection">
						<input id="shopCode" placeholder="Mã đơn vị"  type="text" class="InputTextStyle InputText1Style" style="width: 70px !important;padding-right:5px"/>
						<input id="shopName" placeholder="Tên đơn vị" type="text" class="InputTextStyle InputText2Style" style="width: 120px; padding-right: 5px;"/>
						<input id="day" type="text" class="InputTextStyle InputText2Style" style="width: 70px !important;">
						<div class="BtnLSection">
							<button id="btSearchShop" style="margin-left: 10px;" onclick="SupperviseSalesStaff.loadTreeStaff(1);" class="BtnGeneralStyle">Tìm kiếm</button>
						</div>
						<div class="Clear"></div>
<!-- 						<div id="tree" style="max-height:500px;overflow:auto;"></div> -->
						<div id="promotionShopGrid">
                             <table id="treeGrid" ></table> 
                   		</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="Clear"></div>
</div>
<div class="GeneralMilkBox">
	<div class="GeneralMilkTopBox">
		<div class="GeneralMilkBtmBox"></div>
	</div>
</div>
<div id="contextMenu" class="easyui-menu" style="width:120px;display:none">
    <div id="cmDoanhSoNgay" >Doanh số ngày</div>
    <div id="cmLuyKeThang" >Lũy kế tháng</div>
    <div id="cmXemLoTrinh" >Xem lộ trình</div>
</div>
<div id="monthAmountPlan" class="easyui-dialog" data-options="closed:true,modal:true" title="Doanh số bán hàng lũy kế tháng: TBHV000012 - Nguyễn Tuấn Việt" style="width:645px;height:500px;">
	<div id="monthAmountPlanContainer"></div>
</div>
<div id="showCustomerDetail" style="display: none;">
	<div class="MapPopupSection MapPopup1Section">
	<h2 class="Title2Style" id="h2CustomerCode"></h2>
	<div class="MPContent">
		<dl class="Dl2Style FixFloat">
			<dt>Địa chỉ:</dt>
			<dd id="ddAddress"></dd>
			<dt>Di động:</dt>
			<dd id="ddMobiPhone">0<br /></dd>
			<dt>Cố định:</dt>
			<dd id="ddPhone">0</dd>
		</dl>
		<div class="GeneralTable Table1Section">
			<table width="100%" border="0" cellspacing="0" cellpadding="0">
           	<colgroup>
           		<col style="width:77px;" />
           		<col style="width:77px;" />
           		<col style="width:77px;" />
           		<col style="width:50px;" />
           		<col style="width:50px;" />
           	</colgroup>
           	<thead>
	           	<tr>
		           	<th colspan="3" class="EndTopThStyle">Doanh số</th>
		           	<th colspan="2" class="EndTopThStyle">Ghé thăm</th>
	           	</tr>
				<tr>
					<th>Kế hoạch</th>
					<th>Thực tế</th>
					<th>Duyệt</th>
					<th>Bắt đầu</th>
					<th>Kết thúc</th>
				</tr>
			</thead>
               <tbody>
                   <tr>
                       <td class="FirstTdStyle"><div class="AlignRCols" id="ddAmountPlan"></div></td>
                       <td><div class="AlignRCols" id="ddAmount" ></div></td>
                       <td><div class="AlignRCols" id="ddDuyet" ></div></td>
                       <td><div class="AlignCCols" id="ddStartTime"></div></td>
                       <td><div class="AlignCCols" id="ddEndTime"></div></td>
                   </tr>
               </tbody>
           </table>
		</div>
		<div class="FixFloat ContentFunc">
			<p class="Text2Style">* Đơn vị (x 1000 vnđ)</p> 
		</div>
	</div>
	</div>
</div>
<div id="dialogDSNgayGDM" class="easyui-dialog" title="Doanh số bán hàng ngày: NVGS000012 - Nguyễn Tuấn Việt" style="width: 712px; height: 450px;" data-options="closed:true,modal:true">
	<div class="PopupContentMid">
		<div class="GeneralTable Table3Section" style="width: auto;">
			<table style="width: 100%;" border="0" cellspacing="0" cellpadding="0">
				<thead>
					<tr>
						<th style="width: 70px;" rowspan="2" class="FirstThStyle">Vùng</th>
						<th style="width: 120px;" rowspan="2" class="FirstThStyle">Mã TBHV</th>
						<th style="width: 170px;" rowspan="2">TBHV</th>
						<th colspan="2" class="EndTopThStyle">Đơn hàng</th>
						<th colspan="3" class="EndThStyle EndTopThStyle">Doanh số(ĐVT:1000 VNĐ)</th>
					</tr>
					<tr>
						<th>KH</th>
						<th>TH</th>
						<th>Kế hoạch</th>
						<th>Thực hiện</th>
						<th class="EndThStyle">Còn lại</th>
					</tr>
				</thead>
				<tbody id="gdmSaleBody">
					
				</tbody>
				<tfoot id="gdmFooter">
					
				</tfoot>
			</table>
		</div>
	</div>
</div>
<div id="dialogDSNgayTBHV" class="easyui-dialog" title="Doanh số bán hàng lũy kế tháng: TBHV000012 - Nguyễn Ngọc Khoa" style="width: 635px; height: 500px;" data-options="closed:true,modal:true">
	<div class="PopupContentMid">
		<div class="GeneralTable Table2Section" style="width: auto;">
			<table style="width: 100%;" border="0" cellspacing="0" cellpadding="0">
				<colgroup>
					<col style="width: 92px;" />
					<col style="width: 178px;" />
					<col style="width: 92px;" />
					<col style="width: 87px;" />
					<col style="width: 83px;" />
				</colgroup>
				<thead>
					<tr>
						<th rowspan="2" class="FirstThStyle">NPP</th>
						<th rowspan="2">GSNPP</th>
						<th colspan="4" class="EndThStyle EndTopThStyle">Doanh số - ĐVT: x1000 VNĐ</th>
					</tr>
					<tr>
						<th>Kế hoạch</th>
						<th>Thực hiện</th>
						<th class="EndThStyle">Còn lại</th>
					</tr>
				</thead>
				<tbody id="tbhvSaleBody">

				</tbody>
				<tfoot id="tbhvSaleFooter">
					
				</tfoot>
			</table>
		</div>
	</div>
</div>
<div id="dialogDSNgayNVGS" class="easyui-dialog" title="Doanh số bán hàng ngày: NVGS000012 - Nguyễn Tuấn Việt" style="width: 712px; height: 450px;" data-options="closed:true,modal:true">
	<div class="PopupContentMid">
		<div class="GeneralTable Table3Section" style="width: auto;">
			<table style="width: 100%;" border="0" cellspacing="0" cellpadding="0">
				<colgroup>
					<col style="width: 92px;" />
					<col style="width: 200px;" />
					<col style="width: 50px;" />
					<col style="width: 50px;" />
					<col style="width: 83px;" />
					<col style="width: 83px;" />
					<col style="width: 83px;" />
				</colgroup>
				<thead>
					<tr>
						<th rowspan="2" class="FirstThStyle">Mã NVBH</th>
						<th rowspan="2">NVBH</th>
						<th colspan="2" class="EndTopThStyle">Đơn hàng</th>
						<th colspan="3" class="EndThStyle EndTopThStyle">Doanh số(ĐVT:1000 VNĐ)</th>
					</tr>
					<tr>
						<th>KH</th>
						<th>TH</th>
						<th>Kế hoạch</th>
						<th>Thực hiện</th>
						<th class="EndThStyle">Còn lại</th>
					</tr>
				</thead>
				<tbody id="nvgsSaleBody">
					
				</tbody>
				<tfoot id="nvgsFooter">
					
				</tfoot>
			</table>
		</div>
	</div>
</div>
<div id="dialogDSNgayNVBH" class="easyui-dialog" title="Doanh số bán hàng ngày: NVBH000012 - Trần Tuấn Hậu" style="width: 821px; height: 450px;" data-options="closed:true,modal:true">
	<div class="PopupContentMid">
		<div class="GeneralTable Table4Section" style="width: auto;">
			<table style="width: 100%;" border="0" cellspacing="0" cellpadding="0">
				<colgroup>
					<col style="width: 50px;" />
					<col style="width: 80px;" />
					<col style="width: 150px;" />
					<col style="width: 200px;" />
					<col style="width: 80px;" />
					<col style="width: 80px;" />
					<col style="width: 80px;" />
					<col style="width: 60px;" />
				</colgroup>
				<tr>
					<th rowspan="2" class="FirstThStyle">STT</th>
					<th rowspan="2">Mã KH</th>
					<th rowspan="2">Tên khách hàng</th>
					<th rowspan="2">Địa chỉ</th>
					<th colspan="3" class="EndTopThStyle">DS ngày(1000VNĐ)</th>
					<th rowspan="2" class="EndThStyle">Điểm</th>
				</tr>
				<tr>
					<th>Mục tiêu</th>
					<th>Thực hiện</th>
					<th>Tuyến</th>
				</tr>
			</table>
			<div class="ScrollBodyTable">
				<table id="nvbhBody" width="100%" border="0" cellspacing="0" cellpadding="0">
					
				</table>
			</div>
			<div id="nvbhFooter" style="border: 1px solid #C6D5DB;" class="FooterTable">
				
			</div>
		</div>
	</div>
</div>
<s:hidden id="shopId" name="shopId"></s:hidden>
<s:hidden id="type" name="type"></s:hidden>
<script>
$(document).ready(function(){
	setDateTimePicker('day');
	$('#day').val(getCurrentDate());

	VTMapUtil.loadMapResource(function(){
		ViettelMap.loadBigMap('bigMap',null,null,4,null);		
	});	
	$('.MySelectBoxClass').customStyle();
	if($('#type').val().trim()!=''){
		var roleType = parseInt($('#type').val().trim());
		if(roleType==6){
			$('#cbTBHV').removeAttr('disabled');
			$('#cbGSNPP').removeAttr('disabled');
			$('#cbNVBH').removeAttr('disabled');
			$('#cbTBHV').attr('checked','checked');
		}else if(roleType==5 || roleType==7){
			$('#cbTBHV').removeAttr('disabled');
			$('#cbGSNPP').removeAttr('disabled');
			$('#cbNVBH').removeAttr('disabled');
			$('#cbTBHV').attr('checked','checked');
			$('#cbGSNPP').attr('checked','checked');
		}else if(roleType==3){
			$('#cbGSNPP').removeAttr('disabled');
			$('#cbNVBH').removeAttr('disabled');
			$('#cbGSNPP').attr('checked','checked');
		}else if(roleType==2){
			$('#cbNVBH').removeAttr('disabled');
			$('#cbNVBH').attr('checked','checked');
		}else if(roleType==4){
			$('#cbNVBH').removeAttr('disabled');
			$('#cbNVBH').attr('checked','checked');
			$('#shopCode').attr('disabled','disabled');
			$('#shopName').attr('disabled','disabled');
			$('#btSearchShop').attr('disabled','disabled');
		}
	}
	$('#bigMap').css('height',($(window).height()-167)+'px');
	$(window).resize(function() {
		$('#bigMap').css('height',($(window).height()-167)+'px');
		$('.datagrid-body').css('max-height',($(window).height()-300)+'px');
	});
	SupperviseSalesStaff.loadTreeStaff();
	$('.datagrid-body').css('max-height',($(window).height()-300)+'px');	
	
	$('#cbTBHV').change(function(){
		SupperviseSalesStaff.reloadMarker();
	});
	$('#cbGSNPP').change(function(){
		SupperviseSalesStaff.reloadMarker();
	});
	$('#cbNVBH').change(function(){
		SupperviseSalesStaff.reloadMarker();
	});

	$('#shopCode').bind('keyup',function(event){
			if(event.keyCode == keyCodes.ENTER){		   				
				SupperviseSalesStaff.loadTreeStaff(1);
			}
	});
	$('#shopName').bind('keyup',function(event){
		if(event.keyCode == keyCodes.ENTER){		   				
			SupperviseSalesStaff.loadTreeStaff(1);
		}
	});
	$('#divOverlay').show();	
	
});
</script>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<div id="copyWarehouseDialog" class="easyui-dialog" title="" data-options="closed:true, modal:true" style="width:870px;height:650px;">
	<div class="PopupContentMid2">
       	<div class="GeneralForm Search1Form">
  			<div class="CtnTwoColsSection">
			    <div class="SidebarSection" style="float:left; width: 350px;">
			    	<h2 class="Title2Style" style="color: #333333; font-size: 12px;"><s:text name="stock_manage_thong_tin_nguon"/></h2>
			    	<div class="SidebarInSection" style="float:left; width: 335px; padding-left: 15px; padding-top: 13px;">
			    		<label class="LabelStyle "><s:text name= "stock_manage_ma_don_vi_f9"/><span class="ReqiureStyle">(*)</span></label>
            			<input id="txtShopCodeDlCp" type="text" readonly="readonly" value="<s:property value="shopVO.shopCode"/>" class="InputTextStyle InputText1Style" style="width: 196px;" maxlength="50"/>
						<div class="Clear"></div>
						<label class="LabelStyle Label2Style"></label>
						<label class="LabelStyle Label1Style" id="lblShopNameCopy" style="color:#199700; min-width: 120px; margin-top: -5px; padding-bottom: 10px; text-align: left; width: auto;"><s:property value="shopVO.shopName"/></label>
						<div class="Clear"></div>
						<div class="Clear"></div>
						<label class="LabelStyle Label2Style"><s:text name= "stock_manage_status"/></label>
						<div class="BoxSelect BoxSelect2" id="ddlStatusDlCpDiv">
							<select class="MySelectBoxClass" id="ddlStatusDlCp">
								<option value="-2" selected="selected"><s:text name="jsp.common.status.all"/></option>
								<option value="1"><s:text name="jsp.common.status.active"/></option>
								<option value="0"><s:text name="jsp.common.status.stoped"/></option>
							</select>
					 	</div>
						<div class="Clear"></div>
						<p id="errMsgCoppyWarehouseDl" style="display: none; width: 320px; margin-top: 205px; padding-left: 5px;" class="ErrorMsgStyle"></p>
						<p id="successCoppyWarehouseDl" class="SuccessMsgStyle" style="display:none; width: 320; margin-top: 205px; padding-left: 5px;"></p>
						<div class="Clear"></div>
			    	</div>
			    </div>
			    <div class="ContentSection" style="float:left; width: 497px;">
			    	<div class="ReportCtnSection" style="border: none;">
			            <h2 class="Title2Style" style="color: #333333; font-size: 12px;"><s:text name="catalog_unit_tree_title"/></h2>
			            <div class="GeneralorgAccess SearchInSection" style="border: none;">
			                <div class="GridSection" id="shopTreeByCopyContainerGrid" style="padding: 0px; margin: 0px; border: none;">
								<table id="dgTreeShopCopy" class="easyui-treegrid"></table>
			                </div>
			             </div>
			             <div class="Clear"></div>
			        </div>
			    </div>
			</div>
			<div class="BtnCenterSection">
				<button id="group_ctrl_edit_btnCopyWearhouseDl" class="BtnGeneralStyle cmsiscontrol" style="font-size: 12px;" onclick="return StockManager.copyWeahouse();"><s:text name= "stock_manage_button_sao_chep"/></button>
				<button id="btnCopyWearhouseCloseDl" class="BtnGeneralStyle" style="font-size: 12px;" onclick="$('#copyWarehouseDialog').dialog('close');"><s:text name= "stock_manage_tooltip_dong"/></button>
			 </div>
		</div>
      </div>
</div>
<script type="text/javascript">
	$(function(){
		StockManager._warehouseType = $('#ddlWareHouseTypeCpDl').val();
		$('#txtShopCodeDlCp, #ddlStatusDlCpDiv').bind('keyup',function(event){
			if(event.keyCode == keyCodes.ENTER){
				$('#group_ctrl_edit_btnCopyWearhouseDl').click();
			}
		});
		$('#txtShopCodeDlCp').bind('keyup', function(event) {
			if (event.keyCode == keyCode_F9) {
				StockManager.showSearchListShopByCMS(2, '/commons/search-shop-show-list-NPP');
			}
		});
	});
</script>

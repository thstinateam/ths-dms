<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<div class="BreadcrumbSection">
       	<ul class="ResetList FixFloat BreadcrumbList">
           <li class="Sprite1"><a id="aShow" href="javascript:void(0);"><s:text name= "stock_manage_title"/></a></li>
           <li><span><s:text name= "stock_manage_quan_ly_kho"/></span></li>
    	</ul>
</div>
<div class="CtnOneColSection">
	<div class="ContentSection">
		<div class="ToolBarSection">
			<div class="SearchSection GeneralSSection">
				<h2 class="Title2Style"><s:text name="catalog_sales_brand_search_information"/></h2>
				<div class="SearchInSection SProduct1Form">
					<label class="LabelStyle Label1Style"><s:text name= "stock_manage_don_vi"/></label>
					<input id="txtShopCode"  type="text"  value="<s:property value="shopVO.shopCode"/> - <s:property value="shopVO.shopName"/> "  class="InputTextStyle InputText1Style" maxlength="50" onchange="StockManager.txtShopCodeOnchange(this);"/> 
					<label class="LabelStyle Label1Style" id="lblShopName" style="color:#199700; min-width: 120px; padding-left: 10px; text-align: left; width: auto;display:none;"><s:property value="shopVO.shopName"/></label>
					<label class="LabelStyle Label1Style"><s:text name= "stock_manage_ma_kho"/>/<s:text name= "stock_manage_ten_kho"/></label>
					<input id="txtWarehouseCode" type="text" class="InputTextStyle InputText1Style" maxlength="50"/>
					<label class="LabelStyle Label1Style"><s:text name= "stock_manage_loai_kho"/></label>
					<div class="BoxSelect BoxSelect2" id="ddlTypeDiv">
						<select class="MySelectBoxClass" id="cbWarehouseType">
							<option value="-2" selected="selected"><s:text name="jsp.common.status.all"/></option>
							<option value="0"><s:text name="stock_manage_kho_ban"/></option>
							<option value="1"><s:text name="stock_manage_kho_km"/></option>
						</select>                            
					</div>
					<div class="Clear"></div>
					<label class="LabelStyle Label1Style"><s:text name= "stock_manage_tu_ngay"/></label>
					<input id="txtFromDate" type="text" class="InputTextStyle InputText1Style vinput-date"/> 
					<label class="LabelStyle Label1Style"><s:text name= "stock_manage_den_ngay"/></label>
					<input id="txtToDate" type="text" class="InputTextStyle InputText1Style vinput-date"/>
					<label class="LabelStyle Label1Style"><s:text name= "stock_manage_status"/></label>
					<div class="BoxSelect BoxSelect2" id="ddlStatusDiv">
						<select class="MySelectBoxClass" id="ddlStatus">
							<option value="-2"><s:text name="jsp.common.status.all"/></option>
							<option value="1" selected="selected"><s:text name="jsp.common.status.active"/></option>
							<option value="0"><s:text name="jsp.common.status.stoped"/></option>
						</select>                            
					</div>
					<div class="Clear"></div>
					<div class="BtnCenterSection" id="groupDisplayEdit" style="display: none;">
						<button id="btnSearchWarehouse" class="BtnGeneralStyle" onclick="return StockManager.searchWarehouse();"><s:text name= "stock_manage_button_tim_kiem"/></button>
						<%-- <button id="group_ctrl_edit_show_coppy_wh" class="BtnGeneralStyle cmsiscontrol" onclick="return StockManager.dialogCopyStockManager();"><s:text name= "stock_manage_button_sao_chep"/></button> --%>
					</div>
					<div class="Clear"></div>
					<div class="Clear"></div>
				</div>
			</div>
			<p id="errMsgStockManage" style="display: none;" class="ErrorMsgStyle"></p>
			<p id="successMsgStockManage" class="SuccessMsgStyle" style="display:none;"></p>
			<div class="Clear"></div>
		</div>
		<div class="GeneralCntSection">
			<h2 class="Title2Style"><s:text name="stock_manage_title_list_stock"/></h2>
			<div class="GridSection" id="warehouseDgDiv">
				 <table id="dgWarehouse"></table>
			</div>
			<p id="errMsgPermissionEventChange" style="display: none;" class="ErrorMsgStyle"></p>
			<p id="successMsgPermissionEventChange" class="SuccessMsgStyle" style="display:none;"></p>
			<div class="Clear"></div>
		</div>
		<div class="Clear" style="height: 20px;"></div>
	</div>
</div>
<!-- Dialog Form -->
<div class="ContentSection" style="visibility: hidden;" id="changeTemplateDiv">
	<tiles:insertTemplate template="/WEB-INF/jsp/stock/manager/dialogChangeStockManager.jsp" />
</div>
<div class="ContentSection" style="visibility: hidden;" id="copyTemplateDiv">
	<tiles:insertTemplate template="/WEB-INF/jsp/stock/manager/dialogCoppyStockManager.jsp" />
</div>
<div class="ContentSection" style="visibility: hidden;">
	<tiles:insertTemplate template="/WEB-INF/jsp/general/dialogSearchShopTree.jsp" />
</div>
<div class="ContentSection" style="visibility: hidden;">
	<tiles:insertTemplate template="/WEB-INF/jsp/stock/manager/dialogSearchStockManager.jsp" />
</div>

<script type="text/javascript">
	$(document).ready(function() {
		StockManager._shopChoose = {};
		StockManager._shopChoose.shopId = '<s:property value="shopVO.shopId"/>';
		StockManager._shopChoose.shopCode = '<s:property value="shopVO.shopCode"/>';
		StockManager._shopChoose.shopName = '<s:property value="shopVO.shopName" escape="false"/>';
		StockManager._shopRoot = {};
		StockManager._shopRoot.shopId = '<s:property value="shopVO.shopId"/>';
		StockManager._shopRoot.shopCode = '<s:property value="shopVO.shopCode"/>';
		StockManager._shopRoot.shopName = '<s:property value="shopVO.shopName" escape="false"/>';
		$('#txtFromDate, #txtToDate').width($('#txtFromDate').width()-22);
		$('#txtShopCode, #ddlStatusDiv, #ddlWareHouseTypeDiv, #txtFromDate, #txtToDate, #txtWarehouseCode, #txtWarehouseName, #txtDescription, #btnSearchWarehouse').bind('keyup',function(event){
			if(event.keyCode == keyCodes.ENTER){
				StockManager.searchWarehouse();
			}
		});
		Utils.getJSONDataByAjaxNotOverlay({},'/cms/searchTreeShop',function(data){
			StockManager._arrRreeShopTocken = [];
			if(data.rows!=undefined && data.rows!=null && data.rows.length>0){
				StockManager._arrRreeShopTocken = data.rows;
			}
		},null,null);
		StockManager.createWarehouseParamaterSearch();
		//$('#txtShopCode').focus();
		$('#dgWarehouse').datagrid({
			url: '/stock-manage/search-Warehouse',
			width : $('#warehouseDgDiv').width() - 5,
			queryParams: StockManager._paramWarehouseSearch,
			pagination : true,
	        rownumbers : true,
	        pageNumber : 1,
	        scrollbarSize: 0,
	        autoWidth: true,
	        pageList: [10, 20, 50],
	        autoRowHeight : true,
	        fitColumns : true,
			columns:[[
				  
			    {field:'shopCodeM',title:stock_manage_mien,width:50,sortable:true,resizable:true,align:'left',formatter: function(value, row, index){
			    	return Utils.XSSEncode(value);
			    }},
			    {field:'shopCodeV',title:stock_manage_vung,width:50,sortable:true,resizable:true,align:'left',formatter: function(value, row, index){
			    	return Utils.XSSEncode(value);
			    }},
			    {field:'shopCode',title:stock_manage_maNPP ,width:50,sortable:true,resizable:true,align:'left',formatter: function(value, row, index){
			    	return Utils.XSSEncode(value);
			    }},
			    {field:'shopName',title:stock_manage_tenNPP,width:120,sortable:true,resizable:true,align:'left',formatter: function(value, row, index){
			    	return Utils.XSSEncode(value);
			    }},
			    {field:'warehouseCode',title:stock_manage_ma_kho,width:50,sortable:true,resizable:true,align:'left',formatter: function(value, row, index){
			    	return Utils.XSSEncode(value);
			    }},
			    {field:'warehouseName',title:stock_manage_ten_kho ,width:120, sortable:true,resizable:true, align:'left',formatter: function(value, row, index){
			    	return Utils.XSSEncode(value);
			    }},
			    {field:'warehouseType',title:stock_manage_loai_kho,width:50,sortable:true,resizable:true,align:'rigth',formatter: function(value, row, index){
			    	return Utils.XSSEncode(warehouseType.parseValue(value));
			    }},
			    {field:'seq',title:stock_manage_thu_tu,width:30,sortable:true,resizable:true,align:'rigth',formatter: function(value, row, index){
			    	return Utils.XSSEncode(value);
			    }},
				{field:'createDateStr',title:stock_manage_ngay_tao,width:70,sortable:true,resizable:true,align:'center',formatter: function(value, row, index){
					return Utils.XSSEncode(value);
				}},
			    {field:'status',title:stock_manage_status,width:50, sortable:true,resizable:true, align:'left',formatter: function(value, row, index){
			    	if(value == StockManager._activeStatus){
						return jsp_common_status_active;
					} else if(value == StockManager._stopedStatus){
						return jsp_common_status_stoped;
					}
			    }},
			    {field:'change', title:'<a href="javaScript:void(0);" id="dg_stockManage_warehouse_insert" onclick="return StockManager.dialogChangeStockManager(0);"><img title="'+stock_manage_tooltip_them+'" src="/resources/images/icon_add.png"/></a>', width:92, align:'center', fixed:true, formatter: function(value, row, index) {
			    	var html = '';
			    	if(row.status != undefined && row.status != null && row.status == 1){
				    	html += '<a href="javascript:void(0)" id="dg_stockManage_warehouse_change" onclick="return StockManager.dialogChangeStockManager(1, '+row.warehouseId+');"><img title="'+stock_manage_tooltip_chinh_sua+'" src="/resources/images/icon-edit.png" width="16" heigh="16" style="padding-left: 10px;"></a>';
				    	/* html += '<a href="javascript:void(0)" id="dg_stockManage_warehouse_update" onclick="return StockManager.updateStatusStockManager('+row.warehouseId+',\''+row.warehouseCode+'\' ,\''+inActiveStatusText+'\''+');"><img title="'+stock_manage_tooltip_tam_ngung+'" src="/resources/images/icon-stop.png" width="16" heigh="16" style="padding-left: 10px;"></a>'; */
				    	/* html += '<a href="javascript:void(0)" id="dg_stockManage_warehouse_delete" onclick=""><img title="'+stock_manage_tooltip_xoa +'" src="/resources/images/icon_delete_disable.png" width="16" heigh="16" style="padding-left: 5px;"></a>'; */
			    	}else{
			    		html += '<a href="javascript:void(0)" id="dg_stockManage_warehouse_change" onclick=""><img title="'+stock_manage_tooltip_chinh_sua+'" src="/resources/images/icon-edit_disable.png" width="16" heigh="16" style="padding-left: 10px;"></a>';
				    	/* html += '<a href="javascript:void(0)" id="dg_stockManage_warehouse_update" onclick=""><img title="'+stock_manage_tooltip_tam_ngung+'" src="/resources/images/icon-stop_disable.png" width="16" heigh="16" style="padding-left: 10px;"></a>'; */
				    	/* html += '<a href="javascript:void(0)" id="dg_stockManage_warehouse_delete" onclick="return StockManager.deleteStockManager('+row.warehouseId+',\''+row.warehouseCode+'\');"><img title="'+stock_manage_tooltip_xoa+'" src="/resources/images/icon_delete.png" width="16" heigh="16" style="padding-left: 5px;"></a>'; */
			    	}
			    	
			    	return html;
			    }},
			    {field:'P',hidden: true}
			]],
	        onLoadSuccess :function(data){
				$('.datagrid-header-rownumber').html(area_tree_stt);
				Utils.functionAccessFillControl('dgWarehouse');
			 	StockManager._mapWarehouse = new Map();
			 	if(data!=undefined && data!=null && data.rows!=undefined && data.rows!=null){
			 		for(var i=0; i< data.rows.length; i++){
			 			StockManager._mapWarehouse.put(data.rows[i].warehouseId, data.rows[i]);
			 		}
			 	}
			 	if($('td[field="change"] a').length == 0){
			 		$('#dgWarehouse').datagrid("hideColumn", "change");
			 	}else{
			 		$('#dgWarehouse').datagrid("showColumn", "change");
			 	}
			 	//Phan quyen control
			 	var arrChange =  $('#warehouseDgDiv td[field="change"]');
				if (arrChange != undefined && arrChange != null && arrChange.length > 0) {
				  for (var i = 0, size = arrChange.length; i < size; i++) {
				    $(arrChange[i]).prop("id", "group_ctrl_edit_gr_row_change_" + i);
					$(arrChange[i]).addClass("cmsiscontrol");
				  }
				}
				Utils.functionAccessFillControl('warehouseDgDiv', function(data){
					//Xu ly cac su kien lien quan den cac control phan quyen
					var arrTmpLength =  $('#warehouseDgDiv td[id^="group_ctrl_edit_gr_row_change_"]').length;
					var invisibleLenght = $('.isCMSInvisible[id^="group_ctrl_edit_gr_row_change_"]').length;
					if (arrTmpLength > 0 && arrTmpLength != invisibleLenght) {
						$('#dgWarehouse').datagrid("showColumn", "change");
					} else {
						$('#dgWarehouse').datagrid("hideColumn", "change");
					}
				});
	        }
		});
		 
		$('#txtShopCode').bind('keyup', function(event) {
				if (event.keyCode == keyCode_F9) {
					$('#common-dialog-search-tree-in-grid-choose-single').dialog({
						title: stock_manage_tim_kiem_don_vi,
						closed: false,
				        cache: false, 
				        modal: true,
				        width: 680,
				        height: 550,
				        onOpen: function(){
				        	$('#txtShopCodeDlg').val("");
				        	$('#txtShopNameDlg').val("");
				        	//Tao cay don vi
				        	$('#commonDialogTreeShopG').treegrid({  
				        		data: StockManager._arrRreeShopTocken,
				        		url : '/cms/searchTreeShop',
				    		    width: 650,
				    		    height: 325,
				    		    queryParams: {code: $('#txtShopCodeDlg').val().trim(), name: $('#txtShopNameDlg').val().trim()},
				    			fitColumns : true,
				    			checkOnSelect: false,
				    			selectOnCheck: false,
				    		    rownumbers : true,
				    	        idField: 'id',
				    	        treeField: 'code',
				    		    columns:[[
				    		        {field:'code',title:work_date_unit_code,resizable:false, width:200, align:'left', formatter:function(value, row, index){
				    		        	return Utils.XSSEncode(value);
				    		        }},
				    		        {field:'name',title:work_date_unit_name,resizable:false, width:200, align:'left', formatter:function(value, row, index){
				    		        	return Utils.XSSEncode(value);
				    		        }},
				    		        {field:'choose',title:'',resizable:false, width:60, align:'center', fixed:true, formatter:function(value, row, index){
				    		        	return '<a href="javascript:void(0)" onclick="return StockManager.callSelectF9SialogShopSearchWarehouse(0, '+row.id+',\''+Utils.XSSEncode(row.code)+'\',\''+Utils.XSSEncode(row.name)+'\');">'+work_date_chon+'</a>';  
				    		        }}
				    		    ]],
				    	        onLoadSuccess :function(data){
				    				$('.datagrid-header-rownumber').html(area_tree_stt);
				    				Utils.updateRownumWidthAndHeightForDataGrid('commonDialogTreeShopG');
				    	        }
				    		});
				        	$('#txtShopCodeDlg').focus();
				        },
				        onClose:function() {
				        	
				        }
					});
				}
			}).bind('change',function(){
				//shopChanged();
			}).bind('focus', function(event) {
				$('#common-dialog-search-tree-in-grid-choose-single').dialog({
					title: stock_manage_tim_kiem_don_vi,
					closed: false,
			        cache: false, 
			        modal: true,
			        width: 680,
			        height: 550,
			        onOpen: function(){
			        	$('#txtShopCodeDlg').val("");
			        	$('#txtShopNameDlg').val("");
			        	//Tao cay don vi
			        	$('#commonDialogTreeShopG').treegrid({  
			        		data: StockManager._arrRreeShopTocken,
			        		url : '/cms/searchTreeShop',
			    		    width: 650,
			    		    height: 325,
			    		    queryParams: {code: $('#txtShopCodeDlg').val().trim(), name: $('#txtShopNameDlg').val().trim()},
			    			fitColumns : true,
			    			checkOnSelect: false,
			    			selectOnCheck: false,
			    		    rownumbers : true,
			    	        idField: 'id',
			    	        treeField: 'code',
			    		    columns:[[
			    		        {field:'code',title:work_date_unit_code,resizable:false, width:200, align:'left', formatter:function(value, row, index){
			    		        	return Utils.XSSEncode(value);
			    		        }},
			    		        {field:'name',title:work_date_unit_name,resizable:false, width:200, align:'left', formatter:function(value, row, index){
			    		        	return Utils.XSSEncode(value);
			    		        }},
			    		        {field:'choose',title:'',resizable:false, width:60, align:'center', fixed:true, formatter:function(value, row, index){
			    		        	return '<a href="javascript:void(0)" onclick="return StockManager.callSelectF9SialogShopSearchWarehouse(0, '+row.id+',\''+Utils.XSSEncode(row.code)+'\',\''+Utils.XSSEncode(row.name)+'\');">'+work_date_chon+'</a>';  
			    		        }}
			    		    ]],
			    	        onLoadSuccess :function(data){
			    				$('.datagrid-header-rownumber').html(area_tree_stt);
			    				Utils.updateRownumWidthAndHeightForDataGrid('commonDialogTreeShopG');
			    	        }
			    		});
			        	$('#txtShopCodeDlg').focus();
			        },
			        onClose:function() {
			        	
			        }
				});
			});
		$('#groupDisplayEdit').show();
	});
</script>

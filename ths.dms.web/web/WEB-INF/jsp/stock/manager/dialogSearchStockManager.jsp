<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<div id="common-dialog-search-2-textbox" class="easyui-dialog" data-options="closed:true, modal:true" style="width:670px;height:auto;">
	<div class="PopupContentMid2">
		<div class="GeneralForm Search1Form">
			<label id="label-code" class="LabelStyle Label1Style" style="width: 100px;"><s:text name= "stock_manage_ma_don_vi"/></label>
			<input id="txtCmnShopCode" maxlength="50" type="text" style="width: 145px;" value="" class="InputTextStyle InputText1Style" />
			<label id="label-name" class="LabelStyle Label1Style" style="width: 100px;"><s:text name= "stock_manage_ten_don_vi"/></label>
			<input id="txtCmnShopName" maxlength="250" type="text" style="width: 145px;" value="" class="InputTextStyle InputText1Style" />
			<div class="Clear"></div>
			<div class="Clear"></div>
			<div class="BtnCenterSection">
				<button class="BtnGeneralStyle BtnGeneralMStyle" id="common-dialog-button-search" onclick="return StockManager.searchListShopByCMS();"><s:text name= "stock_manage_button_tim_kiem"/></button>
			</div>
			<div class="Clear"></div>
			<div class="GridSection" id="common-dialog-grid-container">
				<table id="common-dialog-grid-search"></table>
			</div>
			<div class="BtnCenterSection">
				<button class="BtnGeneralStyle BtnGeneralMStyle" onclick="$('#common-dialog-search-2-textbox').dialog('close');"><s:text name= "stock_manage_tooltip_dong"/></button>
			</div>
		</div>
	</div>
</div>
<script type="text/javascript">
	$(function(){
		$('#txtCmnShopCode, #txtCmnShopName, #common-dialog-button-search').bind('keyup',function(event){
			if(event.keyCode == keyCodes.ENTER){
				$('#common-dialog-button-search').click();
			}
		});
	});
</script>
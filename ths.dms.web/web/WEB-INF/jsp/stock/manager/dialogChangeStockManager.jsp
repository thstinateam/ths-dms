<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<div id="changeWarehouseDialog" class="easyui-dialog" title="" data-options="closed:true, modal:true" style="width:670px;height:auto;">
	<div class="PopupContentMid2">
       	<div class="GeneralForm Search1Form" style="padding-left: 20px;">
  			<div class="Clear" style="height: 20px;"></div>
			<label class="LabelStyle Label2Style"><s:text name="stock_manage_ma_don_vi"/><span class="ReqiureStyle"> * </span></label>
            <input id="txtShopCodeDl" readonly="readonly" type="text" value="<s:property value="shopVO.shopCode"/>" class="InputTextStyle InputText1Style" style="width: 507px;" maxlength="50"/>
            <label class="LabelStyle Label1Style" id="lblShopNameDl" style="color:#199700; min-width: 120px; padding-left: 10px; text-align: left; width: auto;display:none;"><s:property value="shopVO.shopName"/></label>
            <div class="Clear"></div>
            <label class="LabelStyle Label2Style"><s:text name= "stock_manage_ma_kho"/><span class="ReqiureStyle"> * </span></label>
            <input id="txtWarehouseCodeDl" type="text" class="InputTextStyle InputText1Style" style="width: 196px;" maxlength="50"/>
<!--             <label class="LabelStyle Label2Style">Loại</label> -->
<!--             <div class="BoxSelect BoxSelect2" id="wearhouseTypeDlDiv"> -->
<%-- 				<select class="MySelectBoxClass" id="ddlWareHouseTypeDl"> --%>
<%-- 					<s:iterator value="lstApparam" var="obj" > --%>
<%-- 						<option value="<s:property value="#obj.apParamCode" />"><s:property value="#obj.apParamName" /></option> --%>
<%-- 					</s:iterator> --%>
<%-- 				</select> --%>
<!-- 			</div> -->
            <label class="LabelStyle Label2Style"><s:text name= "stock_manage_ten_kho"/><span class="ReqiureStyle"> * </span></label>
            <input id="txtWarehouseNameDl" type="text" class="InputTextStyle InputText1Style" style="width: 196px;" maxlength="100"/>
			<label class="LabelStyle Label2Style"><s:text name= "stock_manage_status"/></label>
			<div class="BoxSelect BoxSelect2" id="ddlStatusDlDiv">
				<select class="MySelectBoxClass" id="ddlStatusDl">
					<option value="1" selected="selected"><s:text name="jsp.common.status.active"/></option>
					<option value="0"><s:text name="jsp.common.status.stoped"/></option>
				</select>
			</div>
			<label class="LabelStyle Label2Style"><s:text name="stock_manage_tooltip_thu_tu"/></label> 
			<input id="txtSeqDl" type="text" class="InputTextStyle InputText1Style vinput-number-comma" style="width: 196px;" maxlength="5" value="1"/>
			<div class="Clear"></div>
			<label class="LabelStyle Label2Style"><s:text name= "stock_manage_loai_kho"/></label>
			<div class="BoxSelect BoxSelect2" id="ddlWarehouseTypeDlDiv">
				<select class="MySelectBoxClass" id="cbWarehouseTypeDl">
					<option value="0"><s:text name="stock_manage_kho_ban"/></option>
					<option value="1"><s:text name="stock_manage_kho_km"/></option>
				</select>
			</div>
			<div class="Clear"></div>
           	<label class="LabelStyle Label2Style"><s:text name="stock_manage_tooltip_ghi_chu"/></label> 
			<input id="txtDescriptionDl" type="text" class="InputTextStyle InputText1Style" style="width: 507px;" maxlength="500"/>
			<div class="Clear"></div>
          	<div class="BtnCenterSection">
          		<button class="BtnGeneralStyle Sprite2 BtnSearchOnDialog" id="btnSaveDialogWarehouseChange" onclick="return StockManager.changeWarehouseByDialog();">
          			<span class="Sprite2"><s:text name="stock_manage_tooltip_luu"/></span>
          		</button>
          		<button class="BtnGeneralStyle Sprite2 BtnSearchOnDialog" onclick="$('#changeWarehouseDialog').dialog('close');">
          			<span class="Sprite2"><s:text name="stock_manage_tooltip_dong"/></span>
          		</button>
          	</div>
           	<p id="errMsgDialogChangeWarehouse" style="display: none;" class="ErrorMsgStyle"></p>
           	<p id="successMsgDialogChangeWarehouse" class="SuccessMsgStyle" style="display:none;"></p>
           	<input type="hidden" id="permissionIdByChangeDialog" />
			<div class="Clear"></div>
		</div>
     </div>
</div>
<script type="text/javascript">
	$(function(){
		$('#txtShopCodeDl, #txtWarehouseCodeDl, #txtWarehouseNameDl, #ddlStatusDlDiv, #txtDescriptionDl, #txtSeqDl, #btnSaveDialogWarehouseChange').bind('keyup',function(event){
			if(event.keyCode == keyCodes.ENTER){
				$('#btnSaveDialogWarehouseChange').click();
			}
		});
		/* $('#txtShopCodeDl').bind('keyup', function(event) {
			if (event.keyCode == keyCode_F9) {
				StockManager.showSearchListShopByCMS(1, '/commons/search-shop-show-list-NPP');
			}
		}); */
		//phuocdh2 modified
		 var txt = 'txtShopCodeDl';
			$('#txtShopCodeDl').bind('keyup', function(event) {
				if (event.keyCode == keyCode_F9) {
					$('#common-dialog-search-tree-in-grid-choose-single').dialog({
						title: stock_manage_tim_kiem_don_vi,
						closed: false,
				        cache: false, 
				        modal: true,
				        width: 680,
				        height: 550,
				        onOpen: function(){
				        	$('#txtShopCodeDlg').val("");
				        	$('#txtShopNameDlg').val("");
				        	//Tao cay don vi
				        	$('#commonDialogTreeShopG').treegrid({  
				        		data: StockManager._arrRreeShopTocken,
				        		url : '/cms/searchTreeShop',
				    		    width: 650,
				    		    height: 325,
				    		    queryParams: {code: $('#txtShopCodeDlg').val().trim(), name: $('#txtShopNameDlg').val().trim()},
				    			fitColumns : true,
				    			checkOnSelect: false,
				    			selectOnCheck: false,
				    		    rownumbers : true,
				    	        idField: 'id',
				    	        treeField: 'code',
				    		    columns:[[
				    		        {field:'code',title:work_date_unit_code,resizable:false, width:200, align:'left', formatter:function(value, row, index){
				    		        	return Utils.XSSEncode(value);
				    		        }},
				    		        {field:'name',title:work_date_unit_name,resizable:false, width:200, align:'left', formatter:function(value, row, index){
				    		        	return Utils.XSSEncode(value);
				    		        }},
				    		        {field:'choose',title:'',resizable:false, width:60, align:'center', fixed:true, formatter:function(value, row, index){
				    		        	return '<a href="javascript:void(0)" onclick="return StockManager.callSelectF9SialogShopSearchWarehouse(1, '+row.id+',\''+Utils.XSSEncode(row.code)+'\',\''+Utils.XSSEncode(row.name)+'\');">'+work_date_chon+'</a>';  
				    		        }}
				    		    ]],
				    	        onLoadSuccess :function(data){
				    				$('.datagrid-header-rownumber').html(area_tree_stt);
				    				Utils.updateRownumWidthAndHeightForDataGrid('commonDialogTreeShopG');
				    	        }
				    		});
				        	$('#txtShopCodeDlg').focus();
				        },
				        onClose:function() {
				        	
				        }
					});
				}
			}).bind('change',function(){
				//shopChanged();
			}).bind('focus', function(event) {
				$('#common-dialog-search-tree-in-grid-choose-single').dialog({
					title: stock_manage_tim_kiem_don_vi,
					closed: false,
			        cache: false, 
			        modal: true,
			        width: 680,
			        height: 550,
			        onOpen: function(){
			        	$('#txtShopCodeDlg').val("");
			        	$('#txtShopNameDlg').val("");
			        	//Tao cay don vi
			        	$('#commonDialogTreeShopG').treegrid({  
			        		data: StockManager._arrRreeShopTocken,
			        		url : '/cms/searchTreeShop',
			    		    width: 650,
			    		    height: 325,
			    		    queryParams: {code: $('#txtShopCodeDlg').val().trim(), name: $('#txtShopNameDlg').val().trim()},
			    			fitColumns : true,
			    			checkOnSelect: false,
			    			selectOnCheck: false,
			    		    rownumbers : true,
			    	        idField: 'id',
			    	        treeField: 'code',
			    		    columns:[[
			    		        {field:'code',title:work_date_unit_code,resizable:false, width:200, align:'left', formatter:function(value, row, index){
			    		        	return Utils.XSSEncode(value);
			    		        }},
			    		        {field:'name',title:work_date_unit_name,resizable:false, width:200, align:'left', formatter:function(value, row, index){
			    		        	return Utils.XSSEncode(value);
			    		        }},
			    		        {field:'choose',title:'',resizable:false, width:60, align:'center', fixed:true, formatter:function(value, row, index){
			    		        	return '<a href="javascript:void(0)" onclick="return StockManager.callSelectF9SialogShopSearchWarehouse(1, '+row.id+',\''+Utils.XSSEncode(row.code)+'\',\''+Utils.XSSEncode(row.name)+'\');">'+work_date_chon+'</a>';  
			    		        }}
			    		    ]],
			    	        onLoadSuccess :function(data){
			    				$('.datagrid-header-rownumber').html(area_tree_stt);
			    				Utils.updateRownumWidthAndHeightForDataGrid('commonDialogTreeShopG');
			    	        }
			    		});
			        	$('#txtShopCodeDlg').focus();
			        },
			        onClose:function() {
			        	
			        }
				});
			});
	});
</script>

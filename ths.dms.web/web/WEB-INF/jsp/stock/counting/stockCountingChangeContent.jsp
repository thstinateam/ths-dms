<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<div class="BreadcrumbSection">
	<ul class="ResetList FixFloat BreadcrumbList">
		<li class="Sprite1"><a href="/stock/counting/info">Kho</a></li>
		<li>
			<s:if test="cycleCountId==null || cycleCountId==0">
				<span>Tạo mới kiểm kê</span>
			</s:if>
			<s:else><span>Sửa kiểm kê</span></s:else>
		</li>
	</ul>
</div>
<div class="CtnOneColSection">
	<div class="ContentSection">
		<div class="ToolBarSection">
			<div class="SearchSection GeneralSSection" id="cycleChangedDiv">
				<h2 class="Title2Style">Thông tin chung</h2>
				<div class="SearchInSection SProduct1Form">
					<label class="LabelStyle Label1Style"><s:text name="sale_plan_unit"/>
                       	<span class="ReqiureStyle"> *</span>
                    </label>
					<div class="BoxSelect BoxSelect2">
						<div class="Field2">
							<input id="shop" style="width:200px;">
						</div>
					</div>
					<label class="LabelStyle Label1Style">Mã kiểm kê<span class="ReqiureStyle"> *</span></label> 
					<input type="text" maxlength="50" value="<s:property value="cycleCount.cycleCountCode" />" class="InputTextStyle InputText1Style" id="cycleCountCode" /> 
					<label class="LabelStyle Label1Style">Trạng thái</label>				
					<div class="BoxSelect BoxSelect2">
						<select id="cycleCountStatus" class="MySelectBoxClass">		                  	
		                    <option value="0" selected="selected">Đang thực hiện</option>
		                    <option value="1">Hoàn thành</option>
		                    <option value="2">Hủy bỏ</option>
		                    <option value="3">Từ chối</option>
		                    <option value="4">Chờ duyệt</option>
	                  	</select>
					</div>	
					<div class="Clear"></div>
					
					<label class="LabelStyle Label1Style">Ngày tạo</label> 
					<input type="text" disabled="disabled" class="InputTextStyle InputText1Style date" id="createDate" maxlength="10" type="text" />
					<label class="LabelStyle Label1Style">Mô tả<span class="ReqiureStyle"> *</span></label> 
					<input value="<s:property value="cycleCount.description" />" type="text" class="InputTextStyle InputText1Style" id="description"  maxlength="200"/>
					<div class="Clear"></div>
					
					<label class="LabelStyle Label1Style" >Kiểm kê từ ngày <span class="ReqiureStyle"> *</span></label> 
					<input	type="text" class="InputTextStyle InputText1Style date" id="startDate" maxlength="10"  />
					<label class="LabelStyle Label1Style">Chọn kho
						<span class="ReqiureStyle"><font color="red"> *</font></span>
					</label>	
					<div class="BoxSelect BoxSelect2">
						<select class="easyui-combobox" id="cycleCountWareHouse">
                        </select>
<%-- 						<select id="cycleCountWareHouse" class="MySelectBoxClass" > --%>
<%-- 		                  <s:iterator value="lstWarehouseVO"  var="obj" > --%>
<%-- 								<option value="<s:property value="#obj.warehouseId" />"><s:property value="#obj.warehouseCode" /> - <s:property value="#obj.warehouseName" /></option> --%>
<%-- 							</s:iterator> --%>
<%-- 	                  	</select> --%>
					</div>	
					<div class="Clear"></div>
					
					<div class="BtnCenterSection">
						<button class="BtnGeneralStyle" id="btnUpdate" >Cập nhật</button>
						<button class="BtnGeneralStyle" id="btnDismiss" onclick="window.location.href='/stock/counting/info'">Quay lại</button>						
					</div>
					<div class="Clear"></div>
					<p class="ErrorMsgStyle" id="errMsg" style="display: none;">Có lỗi xảy ra khi cập nhật dữ liệu</p>
              		<p id="successMsg" class="SuccessMsgStyle" style="display: none"></p>
				</div>				
			</div>
			<div class="Clear"></div>
		</div>
	</div>
	<div class="Clear"></div>
</div>
<s:hidden id="cycleCountId" name="cycleCountId" ></s:hidden>
<s:hidden id="shopCode" name="shopCode"></s:hidden>
<s:hidden id="wareHouseId" name="wareHouseId" ></s:hidden>
<script type="text/javascript">
$(document).ready(function(){
	StockCounting._isSearch = false;
	//load combobox don vi
	StockCounting.loadComboboxShop();
	var isChanged = '<s:property value="isChanged" />'=='1'? true:false;	
	//$('.MySelectBoxClass').customStyle();	
	$('#cycleCountCode').focus();
	$('#cycleCountCode').bind('keyup',function(event) {
		if (event.keyCode == 13) {
			$('#btnUpdate').click();
		}
	});
	$('#description').bind('keyup',function(event) {
		if (event.keyCode == 13) {
			$('#btnUpdate').click();
		}
	});
		
	var cycleCountId = '<s:property value="cycleCountId" />';
	
	if(cycleCountId != undefined && cycleCountId > 0){
		var cycleCountStatus = '<s:property value="cycleCount.status.value" />';
		$('#cycleCountStatus').val(cycleCountStatus).change();
		$('.date').each(function(){
			var id = $(this).attr('id');
			setDateTimePicker(id);
			$(this).css('width','173px');
		});
		//var date = new Date('<s:property value="cycleCount.createDate" />');
		//$('#createDate').val($.datepicker.formatDate('dd/mm/yy', date));
		$('#createDate').val('<s:date name="cycleCount.createDate" format="dd/MM/yyyy" />');
		$('#startDate').val('<s:date name="cycleCount.startDate" format="dd/MM/yyyy" />');
		$('#cycleCountCode').attr('disabled','disabled');
		$('#btnUpdate').bind('click',StockCounting.update);
		if (cycleCountStatus == 1 && cycleCountStatus == 2) {
			$('#btnUpdate').attr('disabled','disabled');
		}
	}else{
		$('#cycleCountStatus').val(0);
		$('#cycleCountStatus').change();
		$('#btnUpdate').bind('click',StockCounting.change);
	}	
	//StockCounting.resetForm(isChanged);
	$('#cycleCountStatus').attr('disabled','disabled');
	$('#cycleCountStatus').parent().addClass('BoxDisSelect');
	$('#cycleCountStatus').change();	
	$('#createDate').next().unbind('click');
});
</script>
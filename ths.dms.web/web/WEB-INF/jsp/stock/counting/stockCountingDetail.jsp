<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
	<div class="GeneralDialog General1Dialog">
      	<div class="DialogProductSearch">
          	<div class="GeneralMilkBox">
                  <div class="GeneralMilkTopBox">
                      <div class="GeneralMilkBtmBox">
	                        <h3 class="Sprite2"><span class="Sprite2">Thông tin</span></h3>
	                     	<div class="GeneralMilkInBox ResearchSection">
	                     	<div class="GeneralInfoSection Warehouse51Form">
	                        <label class="LabelStyle Label1Style">Mã kiểm kê</label>
	                        <p class="ValueStyle Value1Style">
	                        	<s:property value="cycleCount.cycleCountCode" />
	                        </p>
	                        <label class="LabelStyle Label2Style">Tình trạng</label>
	                        <p class="ValueStyle Value1Style" id="cycleCountStatus001"></p>
	                        <input type="hidden" id="hidden_filed_status" value="<s:property value="cycleCount.status" />" /> 	                                                           
	                        <div class="Clear"></div>
	                        <label class="LabelStyle Label1Style">Mô tả</label>
	                        <p class="ValueStyle Value1Style">
	                        	<s:property value="cycleCount.description" />
	                        </p>
	                        <label class="LabelStyle Label2Style">Đơn vị</label>
	                        <p class="ValueStyle Value1Style">
	                        	<s:property value="shopCode" />
	                        </p>
	                        <div class="Clear"></div>
                       </div>
                     </div>
                 </div>
             </div>
         </div>
         <div class="GeneralMilkBox">
             <div class="GeneralMilkTopBox">
                 <div class="GeneralMilkBtmBox">
                     <h3 class="Sprite2"><span class="Sprite2">Thông tin thẻ kiểm kho</span></h3>
                     <div class="GeneralMilkInBox">
                         <div class="ResultSection">
                             <div class="GeneralTable Table15Section">                                 
                                 <div class="BoxGeneralTBody">
                                 <div class="ResultSection"  id="cyclecountdetailGrid">
                                 		<p id="gridNoResult" style="display: none" id="cyclecountdetailGrid" class="WarningResultStyle">Không có kết quả</p>
                                 		<table id="gridDetails"></table>
										<div id="pagerDetails"></div> 
                                 </div>                                      	
                                 </div>
                             </div>
                         </div>
                     </div>
                 </div>
             </div>
         </div>
         <div class="BoxDialogBtm">
             <div class="ButtonSection">
             	<button onclick="$.fancybox.close();" class="BtnGeneralStyle BtnGeneralCStyle BtnGeneralMStyle Sprite2"><span class="Sprite2">Đóng</span></button></div>
            </div>
    	</div>
    </div>
 <input type="hidden" id="cycleCountCodeDetails" value=${cycleCount.cycleCountCode} />
<script type="text/javascript">
$(document).ready(function(){	
	$("#gridDetails").jqGrid({
		  url:StockCategory.getGridUrl($('#cycleCountCodeDetails').val()),
		  colModel:[		
		    {name:'product.productCode', label: 'Mã MH', width: 100, sortable:false,resizable:false , align: 'left'},
		    {name:'product.productName', label: 'Tên MH', sortable:false,resizable:false , align: 'left'},
		    {name:'product.uom1', label: 'ĐVT', sortable:false,resizable:false , align: 'left'}		    
		  ],	  
		  pager : '#pagerDetails',
		  height: 'auto',
		  rownumbers: true,	  
		  width: ($('#cyclecountdetailGrid').width())	  
		})
	.navGrid('#pagerDetails', {edit:false,add:false,del:false, search: false});
	$('#jqgh_gridDetails_rn').prepend('STT');
	
	$('#cycleCountStatus001').html(CountingFormatter.statusFormat('','',rowObject = {
			status:$('#hidden_filed_status').val()
	}));	
	
});
</script>
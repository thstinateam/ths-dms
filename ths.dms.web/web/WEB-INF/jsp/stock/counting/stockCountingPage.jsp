<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<div class="BreadcrumbSection">
	<ul class="ResetList FixFloat BreadcrumbList">
		<li class="Sprite1"><a href="javascript:void(0)">Kho</a></li>
		<li><span>Quản lý kiểm kê</span></li>
	</ul>
</div>
<div class="CtnOneColSection">
	<div class="ContentSection">
		<div class="ToolBarSection">
			<div class="SearchSection GeneralSSection">
				<h2 class="Title2Style">Thông tin tìm kiếm</h2>
				<div class="SearchInSection SProduct1Form">
					<label class="LabelStyle Label1Style"><s:text name="sale_plan_unit"/>
                       	<span class="ReqiureStyle"><font color="red"> *</font></span>
                       </label>
					<div class="BoxSelect BoxSelect2">
						<div class="Field2">
							<input id="shop" style="width:206px;">
						</div>
					</div>
					<label class="LabelStyle Label1Style">Mã kiểm kê</label> 
					<input type="text" class="InputTextStyle InputText1Style" id="cycleCountCode" />
					<label class="LabelStyle Label1Style">Trạng thái</label>				
					<div class="BoxSelect BoxSelect2">
						<select id="cycleCountStatus" class="MySelectBoxClass" >
							<option value="-1">Tất cả</option>
		                  	<option value="0" selected = "selected">Đang thực hiện</option>
		                    <option value="1">Đã duyệt</option>
		                    <option value="2">Hủy bỏ</option>
		                    <option value="3">Từ chối</option>
		                    <option value="4">Chờ duyệt</option>
	                  	</select>
					</div>					
					<div class="Clear"></div>
					
					<label class="LabelStyle Label1Style">Tạo từ ngày</label> 
					<input type="text" class="InputTextStyle InputText1Style date" id="fromCreated" maxlength="10" type="text" />
					<label class="LabelStyle Label1Style">Đến ngày</label>
					<input type="text" class="InputTextStyle InputText1Style date" id="toCreated" maxlength="10" type="text"  />
					<label class="LabelStyle Label1Style" >Mô tả</label> 
					<input	type="text" class="InputTextStyle InputText1Style" id="description"  maxlength="200"/>
					<div class="Clear"></div>
							
					<label class="LabelStyle Label1Style" >Kiểm kê từ ngày</label> 
					<input	type="text" class="InputTextStyle InputText1Style date" id="startDate" maxlength="10"  /> 
					<label class="LabelStyle Label1Style">Đến ngày</label>
					<input type="text" class="InputTextStyle InputText1Style date" id="endDate" maxlength="10" type="text"  />
					
					<label class="LabelStyle Label1Style">Chọn kho</label>
					<div class="BoxSelect BoxSelect2">
						<select class="easyui-combobox" id="cycleCountWareHouse">
                        </select>
                    </div>
<!-- 					<div class="BoxSelect BoxSelect2"> -->
<%-- 						<select id="cycleCountWareHouse" class="MySelectBoxClass" > --%>
<!-- 							<option value="" selected = "selected">Tất cả</option> -->
<%-- 		                  	<s:iterator value="lstWarehouseVO"  var="obj" > --%>
<%-- 								<option value="<s:property value="#obj.warehouseId" />"><s:property value="#obj.warehouseCode" /> - <s:property value="#obj.warehouseName" /></option> --%>
<%-- 							</s:iterator> --%>
<%-- 	                  	</select> --%>
<!-- 					</div>	 -->
					
					<div class="Clear"></div>
					<div class="BtnCenterSection">
						<button class="BtnGeneralStyle" id="btnSearch" onclick="return StockCounting.search();">Tìm kiếm</button>
						<button class="BtnGeneralStyle" id="btnCreate" onclick="window.location.href='/stock/counting/changed'">Tạo mới</button>						
					</div>
					<div class="Clear"></div>
					<p class="ErrorMsgStyle" id="errMsg" style="display: none;">Có lỗi xảy ra khi cập nhật dữ liệu</p>
              		<p id="successMsg" class="SuccessMsgStyle" style="display: none"></p>
				</div>
				<h2 class="Title2Style">Kết quả tìm kiếm</h2>
				<div class="SearchInSection SProduct1Form">
					<div class="GridSection" id="productGrid">
						<table id="grid"></table>
						<div id="pager"></div>
					</div>
				</div>
				<div class="Clear"></div>
			</div>
			<div class="Clear"></div>
		</div>
	</div>
	<div class="Clear"></div>
</div>
<s:hidden id="cycleCountId" value="0"></s:hidden>
<script type="text/javascript">
$(document).ready(function(){
	StockCounting._isSearch = true;
	StockCounting.loadComboboxShop();
	$('#cycleCountCode').focus();
	//$('.MySelectBoxClass').customStyle();
	$('#cycleCountStatus').val(0);
	$('#cycleCountStatus').change();
});
</script>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>

<input type="hidden" id="function_code" value="<s:property value="systimestamp"/>" />
<s:set id="downloadToken" value="%{generateReportToken()}"></s:set>
<input type="hidden" id="report_token" value="<s:property value="#downloadToken"/>" />
<s:set id="downloadToken" value="%{putSessionValue(systimestamp, #downloadToken)}"></s:set>

<div class="BreadcrumbSection">
	<ul class="ResetList FixFloat BreadcrumbList">
		<li class="Sprite1"><a href="/stock/manage-trans/info">Kho</a></li>
		<li><span>Quản lý giao dịch kho</span>
		</li>
	</ul>
</div>
<div class="CtnOneColSection">
	<div class="ContentSection">
		<div class="ToolBarSection">
			<div class="SearchSection GeneralSSection">
				<!-- <h2 class="Title2Style">Thông tin chung</h2> -->
				<h2 class="Title2Style">Thông tin chung <span style="float:right;margin-right:15px;">
						<a href="javascript:void(0);" title="Ẩn/ hiện" id="searchHiddenLink" class="searchShow" onclick="Utils.toggleSearchInput(this);"></a>
					</span>
				</h2>
				<div class="SearchInSection SProduct1Form">
					<label class="LabelLeft1Style LabelLeftStyle">Đơn vị <span class="ReqiureStyle">*</span></label>
		            <div class="BoxSelect ">
						<input  type="text" class="InputTextStyle InputText1Style" style="width: 206px;" id="cbxUnit" maxlength="50" />
					</div>
		            
					<label class="LabelStyle Label1Style">Từ ngày <span class="ReqiureStyle">*</span></label>
	                <input type="text" class="InputTextStyle InputText6Style" maxlength="10" id="startDate" value="<s:property value="lockDate"/>"  autocomplete="off"/>
	                
	                <label class="LabelStyle Label1Style">Đến ngày <span class="ReqiureStyle">*</span></label>
	                <input type="text" class="InputTextStyle InputText6Style" maxlength="10" id="endDate" value="<s:property value="lockDate"/>" autocomplete="off"/>
	                <div class="Clear"></div>
	                <%-- <label class="LabelStyle Label1Style">Đơn vị</label>
		            <input type="hidden" class="InputTextStyle InputText1Style" disabled="disabled" id="shopCodeAndName" type="text" maxlength="50" value="<s:property value="shopCode"/>" autocomplete="off"/> --%>
					<label class="LabelLeft1Style LabelLeftStyle">Loại</label>
					<div class="BoxSelect BoxSelect9">
						<select id="type" class="MySelectBoxClass" autocomplete="off">
							<option value="-1">Chọn loại</option>
							<option value="0">Xuất kho nhân viên</option>
							<option value="1">Nhập kho nhân viên</option>
							<option value="2">Xuất kho điều chỉnh</option>
							<option value="3">Nhập kho điều chỉnh</option>
							<option value="4">Điều chuyển kho</option>
						</select>
					</div>
					
					<label class="LabelStyle Label1Style">Trạng thái</label>
					<div class="BoxSelect BoxSelect2">
						<select id="status" class="MySelectBoxClass" autocomplete="off">
							<option value="">Tất cả</option>
							<option value="1" selected="selected">Đã cập nhật kho</option>
							<option value="0">Chưa cập nhật kho</option>
							<option value="3">Đã hủy</option>
						</select>
					</div>
					<div class="Clear"></div>
					<div class="BtnCenterSection">
                        <button class="BtnGeneralStyle" id="btnSearch" onclick="StockManageTrans.search();">Tìm kiếm</button>
                    </div>
                    <div class="Clear"></div>
                    <img id="loading" class="LoadingStyle" style="visibility: hidden;" src="/resources/images/loading.gif" />
                    <p class="ErrorMsgStyle" id="errMsgSearch" style="display: none;"></p>
				</div>
				<h2 class="Title2Style">Kết quả tìm kiếm</h2>
				<div class="SearchInSection SProduct1Form">
					<div class="GridSection" id=gridContainer>
						<table id="grid"></table>
					</div>
				</div>
				<div class="GeneralMilkBox" style="display: none;" id="stockTransDetailTable">
					<h2 class="Title2Style" id="title">Thông tin giao dịch kho</h2>
					<div class="SearchInSection SProduct1Form">
						<div class="GridSection" id="productDetailGrid">
							<table id="gridDetail"></table>
							<div id="pagerDetail"></div>
						</div>
					</div>
					<p class="TotalInfo TotalLInfo">
                    	<span class="Tem1Style">Tổng trọng lượng: <span id="totalWeight"></span> <b>kg</b></span>
                        <span class="Tem1Style">Tổng tiền: <span id="totalAmount"></span> <b>VNĐ</b></span>
                    </p>
                    <div class="Clear"></div>
                    <div class="SearchInSection SProduct1Form">
						<div class="BtnCenterSection" style="padding-top: 10px;">					
							<button class="BtnGeneralStyle" id="btnExportExcel"	>In phiếu</button>
							<!-- &nbsp;&nbsp;
							<button class="BtnGeneralStyle" id="btnExportExcelNew" onclick="return StockManageTrans.openStockOutTrainsDialog();">In phiếu xuất kho kiêm vận chuyển nội bộ</button> -->
						</div>	
						<div class="Clear"></div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<s:hidden id="stockTransId" value=""></s:hidden>
<div style="display: none;">
<div id="stockOutVanSalePrintDiv" class="easyui-dialog" title="Phiếu xuất kho kiêm vận chuyển nội bộ" data-options="closed:true, modal:true"
		style="width: 650px; height: auto;">
	<div class="PopupContentMid2">
		<div class="GeneralForm Search1Form">
			<div class="Clear"></div>
			<label class="LabelStyle Label2Style" style="width: 125px;">Tên lệnh điều động</label>
			<input id="ipNameTransDialog" type="text" class="InputTextStyle InputText5Style" maxlength="40" />
			<label class="LabelStyle Label2Style" style="width: 125px;">Ngày lệnh điều động</label>
			<input id="fDateDialog" type="text" class="InputTextStyle InputText5Style" style="width: 140px;" />
			<div class="Clear"></div>
			<label class="LabelStyle Label2Style" style="width: 125px;">Nội dung</label>
			<textarea rows="4" class="InputTextStyle InputText2Style" cols="50" id="ipContentTransDialog" style="font-size: 12px; height: 150px; width: 474px;">Vận chuyển hàng đi bán lưu động</textarea>
			<div class="Clear"></div>
		</div>
		<div class="GeneralForm Search1Form">
			<div class="BtnCenterSection">
				<button class="BtnGeneralStyle Sprite2 BtnSearchOnDialog" id="btnExportExcel" onclick="return StockManageTrans.exportStockOutTrans();">
					<span class="Sprite2">In phiếu</span>
				</button>
			</div>
			<div class="Clear"></div>
			<p id="errMsg2" style="display: none;" class="ErrorMsgStyle"></p>
			<div class="Clear"></div>
		</div>
	</div>
</div>
</div>
<s:hidden id="shopId" name="shopId"></s:hidden>
<s:hidden id="curShopId" name="shopId"></s:hidden>
<script type="text/javascript">
$(document).ready(function(){
	disabled('btnExportExcel');
	disabled('btnExportExcelNew');
	applyDateTimePicker('#fDateDialog');
	$('#fDateDialog').val($('#endDate').val());
	//$('#type').customStyle(); khong cho hien thi double
	applyDateTimePicker("#startDate");
	applyDateTimePicker("#endDate");	
	
	//load Cay don vi cbx
	ReportUtils.loadComboTree('cbxUnit', 'shopId', $('#curShopId').val(), function(shopId) {

	});
	$('.MySelectBoxClass').css('width','206');
	$('.CustomStyleSelectBox').css('width','173');

	$('.ErrorMsgStyle').html('').hide();
	var STOCK_APPROVED = 1; // da cap nhat kho
	var STOCK_CANCELLED = 3; // huy
	var params = new Object();
	params.startDate = $('#startDate').val().trim();
	params.endDate = $('#startDate').val().trim();
	params.type = -1;
	params.status = STOCK_APPROVED;
	$('#grid').datagrid({
		url: "/stock/manage-trans/search",
		autoRowHeight: true,
		rownumbers: true,
		checkOnSelect: true,
		singleSelect: true,
		pagination: true,
		rowNum: 10,
		fitColumns: true,
		pageList: [10, 20, 30],
		scrollbarSize: 0,
		width: $('#gridContainer').width(),
		autoWidth: true,
		queryParams: params,
		columns: [[
			{field: 'shopCode', title: 'Mã đơn vị', width: 75, sortable: false, resizable: false, align: 'left', formatter: function(val, row, idx) {
				return val ? Utils.XSSEncode(val) : "";
			}},
			{field: 'shopName', title: 'Tên đơn vị', width: 150, sortable: false, resizable: false, align: 'left', formatter: function(val, row, idx) {
				return val ? Utils.XSSEncode(val) : "";
			}},
			{field: 'stockTransCode', title: 'Mã giao dịch', width: 80, sortable: false, resizable: false, align: 'left', formatter: function(val, row) {
				return val ? Utils.XSSEncode(val) : "";
			}},
			{field: 'stockTransKinds', title: 'Loại giao dịch', width: 130, sortable: false, resizable: false, align: 'left', formatter: ManageTransFormatter.transTypeFormat},
			{field: 'stockTransDateStr', title: 'Ngày thực hiện', sortable: false, resizable: false, align: 'center'},
			{field: 'amount', title: 'Tổng tiền', width: 120, align: 'right', sortable: false, resizable: false, formatter: ManageTransFormatter.amountFormat},
			{field: 'status', title: 'Trạng thái', width: 120, fixed: true, sortable: false, resizable: false, align: 'left', formatter: function(val, row) {
				if (STOCK_CANCELLED == val) {
					return "Đã hủy";
				}
				return (STOCK_APPROVED == val) ? "Đã cập nhật kho" : "Chưa cập nhật kho";
			}},
			{field: 'edit', title: 'Xem chi tiết', width: 50, align: 'center', sortable: false, resizable: false, formatter: ManageTransFormatter.viewDetail},
		]],
		onLoadSuccess: function(data) {
			$('.datagrid-header-rownumber').html('STT');
			//updateRownumWidthForJqGrid('.easyui-dialog');
			Utils.updateRownumWidthAndHeightForDataGrid('grid');
			$(window).resize();
			/*if (data == null || data.total == 0) {
				$("#errMsgSearch").html("Không tìm thấy dữ liệu").show();
			}*/
		}
	});
	
	/*$('#shopCodeAndName').bind('keyup', function(event){
		if(event.keyCode == keyCode_F9){
			CommonSearch.searchShopOnDialog(function(data){
				var shopCodeAndName = Utils.XSSEncode(data.code +'-'+ data.name);
				$('#shopCodeAndName').val(shopCodeAndName);
			});
		}
	});*/
});
</script>
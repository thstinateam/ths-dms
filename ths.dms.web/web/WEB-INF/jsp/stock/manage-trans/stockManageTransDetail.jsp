<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@taglib prefix="k" uri="/kryptone"%>
<div class="GeneralMilkTopBox">
	<div class="GeneralMilkBtmBox">
		<h3 class="Sprite2"><span class="Sprite2" id="stockTransInfoDetailTitle">Thông tin chi tiết giao dịch kho </span></h3>
		<div class="GeneralMilkInBox">
			<div class="ResultSection">
				<div class="GeneralTable Table12Section">
					<div class="BoxGeneralTTitle">
						<table width="100%" border="0" cellspacing="0" cellpadding="0">
							<colgroup>
								<col style="width: 47px;" />
								<col style="width: 84px;" />
								<col style="width: 169px;" />
								<col style="width: 87px;" />
								<col style="width: 84px;" />
								<col style="width: 110px;" />
								<col style="width: 179px;" />
							</colgroup>
							<thead>
								<tr>
									<th class="ColsThFirst">STT</th>
									<th>Mã sản phẩm</th>
									<th>Tên sản phẩm</th>
									<th>Số lượng</th>
									<th>Số lô</th>
									<th>Giá bán</th>
									<th class="ColsThEnd">Thành tiền</th>
								</tr>
							</thead>
						</table>
					</div>
					<s:if test="lstProduct != null && lstProduct.size() > 0">
					<div class="BoxGeneralTBody">
						<div class="ScrollBodySection" style="height: 260px" id="stockTransDetailScroll">
							<table width="100%" border="0" cellspacing="0" cellpadding="0">
								<colgroup>
									<col style="width: 47px;" />
									<col style="width: 84px;" />
									<col style="width: 169px;" />
									<col style="width: 87px;" />
									<col style="width: 84px;" />
									<col style="width: 110px;" />
									<col style="width: 179px;" />
								</colgroup>
								<tbody>
									<k:repeater value="lstProduct" status="status">
										<k:itemTemplate>
											<tr>
												<td class="ColsTd1"><s:property value="#status.index+1" /></td>
												<td class="ColsTd2"><s:property value="productCode" /></td>
												<td class="ColsTd3"><s:property value="productName" /></td>
												<s:if test="convfact != null && convfact!= undefined && convfact != 0">
													<td class="ColsTd4 AlignRight"><s:property value="quantity/convfact" />/<s:property value="quantity%convfact" /></td>
												</s:if>
												<s:else>
													<td class="ColsTd4 AlignRight"><s:property value="quantity/1" />/<s:property value="quantity%1" /></td>
												</s:else>
												<td class="ColsTd5"><s:property value="lot" /></td>
												<td class="ColsTd6 AlignRight CurrencyNumber "><s:property value="price" /></td>
												<td class="ColsTd7 ColsTdEnd CurrencyNumber" style="padding-right: 15px; text-align: right !important"><s:property value="quantity*price" /></td>
											</tr>
										</k:itemTemplate>
									</k:repeater>
								</tbody>
							</table>
						</div>
					</div>
					</s:if>
					<s:else>
						<p class="NotData">Không có dữ liệu nào</p>
					</s:else>
					<div class="BoxGeneralTFooter">
						<table width="100%" border="0" cellspacing="0" cellpadding="0">
							<colgroup>
								<col style="width: 47px;" />
								<col style="width: 84px;" />
								<col style="width: 169px;" />
								<col style="width: 87px;" />
								<col style="width: 84px;" />
								<col style="width: 110px;" />
								<col style="width: 179px;" />
							</colgroup>
							<tfoot>
								<tr>
									<td class="ColsTd1">&nbsp;</td>
									<td class="ColsTd2">&nbsp;</td>
									<td class="ColsTd3">&nbsp;</td>
									<td class="ColsTd4">Trọng lượng:</td>
									<td id="totalWeight" class="ColsTd5 AlignRight" style="font-weight: normal !important;">
										<s:if test="totalWeight != null"><span ><s:property value="totalWeight" /></span> kg
										</s:if>
									</td>
									<td class="ColsTd6">Tổng tiền:</td>
									<td id="totalAmount" class="ColsTd7 ColsTdEnd AlignRight">
										<s:if test="totalAmount != null"><span class="CurrencyNumber"><s:property value="totalAmount" /></span> VND
										</s:if>
									</td>
								</tr>
							</tfoot>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<script type="text/javascript">
$(document).ready(function(){
	$('.CurrencyNumber').each(function () {
		if($(this).html().length > 0){
			$(this).html(formatCurrency($(this).html()));
		}		
	});
});
</script>
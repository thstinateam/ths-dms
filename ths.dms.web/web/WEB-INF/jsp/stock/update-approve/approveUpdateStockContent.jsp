<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<div class="BreadcrumbSection">
    <ul class="ResetList FixFloat BreadcrumbList">
        <li class="Sprite1"><a href="/stock/update-approve/info">Kho</a></li>
        <li><span>Duyệt điều chỉnh kho (HO)</span></li>
    </ul>
</div>
<div class="CtnOneColSection">
    <div class="ContentSection">
    	<div class="ToolBarSection">
            <div class="SearchSection GeneralSSection">
            	<h2 class="Title2Style">Thông tin chung</h2>
                <div class="SearchInSection SProduct1Form">
                	<label class="LabelStyle Label2Style">Đơn vị</label>
                    <div class="BoxSelect BoxSelect2">
                        <select id="shopCodeCB" style="width: 206px;">
                        </select>
                    </div>
                    <label class="LabelStyle Label2Style">Loại đơn hàng</label>
                    <div class="BoxSelect BoxSelect2">
                           <select class="MySelectBoxClass" id="orderType">
                                <option value="" selected="selected">Tất cả</option>
								<option value="DC" >Điều chuyển (DC)</option>
								<option value="DCT">Điều chỉnh tăng kho (DCT)</option>
								<option value="DCG">Điều chỉnh giảm kho (DCG)</option>
                            </select>
                    </div>
                    <label class="LabelStyle Label2Style">Số đơn hàng</label>
                    <input type="text" id="orderNumber" maxlength="50" class="InputTextStyle InputText1Style" />
                    <div class="Clear"></div>
                    <div class="BtnCenterSection">
                        <button id="btnSearch" class="BtnGeneralStyle" onclick="return StockUpdateApprove.searchOrder();">Tìm kiếm</button>
                    </div>
                    <div class="Clear"></div>
                    
                    <p id="errMsgInfo" class="ErrorMsgStyle" style="display: none"></p>
                    <input type="hidden" id="shopCodeHidden" value="<s:property value="shop.shopCode"/>"/>
                    <input type="hidden" id="shopId" value="<s:property value="shop.Id"/>"/>
                </div>
                <h2 class="Title2Style">Kết quả tìm kiếm</h2> 
                <div class="SearchInSection SProduct1Form">
                	<div class="GridSection" style="padding:0;">
                		<table id="grid" ></table>
                	</div>                	
                    <div class="Clear"></div>
                    
                    <p id="errExcelMsg" class="ErrorMsgStyle" style="display: none"></p>
                    <p id="errMsg" class="ErrorMsgStyle" style="display: none"></p>
					<p id="successMsg" class="SuccessMsgStyle" style="display: none"></p>
					<p id="successMsgProduct" class="SuccessMsgStyle" style="display: none"></p>
					 <div class="Clear"></div>
					 
					<div class="BtnCenterSection">
						<button id="btnCancel" class="BtnGeneralStyle cmsiscontrol" onclick="StockUpdateApprove.cancelStockTrans();" style="float:left;">Hủy</button>
                        <button id="btnConfirm" class="BtnGeneralStyle cmsiscontrol" onclick="return StockUpdateApprove.approveStockOrder();" style="float:right; margin-right:20px;">Xác nhận</button>
                        <div class="Clear"></div>
                    </div>
                </div>
            </div>
            <div class="Clear"></div>
        </div>
    </div>
    <div class="Clear"></div>
    
    <div id ="dialogStockOrder" style="display:none;" >
		<div id="dialogStockOrderEasyUIDialog">
			<div class="SearchInSection SProduct1Form">
				<div class="GridSection">	                 					
					<table id="dgError"></table>
				</div>
				 <div class="BtnCenterSection">
	                  <button class="BtnGeneralStyle" onclick="$('#dialogStockOrderEasyUIDialog').dialog('close');">Đóng</button>
	              </div>                             
	     	</div>    
		</div>
	</div>
</div>
<!-- Popup xem chi tiet -->
<div id="billDetailPopupDiv" style="display:none;">
    <div id="billDetailPopup" class="PopupContentMid2">         
    </div>
</div>
<script>
	$(document).ready(function() {
		var params = new Object();
		StockUpdateApprove.initGrid(params);
		StockUpdateApprove.initUnitCbx();
		$('.MySelectBoxClass').css('width','206');
		$('.CustomStyleSelectBox').css('width','173');
		var lnk = '&lnk=' + window.location.href;
		$('#orderNumber').focus();
	});
</script>


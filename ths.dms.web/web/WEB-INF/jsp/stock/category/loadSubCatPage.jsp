<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@page import="ths.dms.helper.Configuration"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<div class="BoxSelect BoxSelect2" id="subcat">
<select id="sub_category" class="MySelectBoxClass">
	<option value="0">Chọn ngành hàng con</option>
	<s:iterator var="subObj" value="lstSubCategoryCat"> 
	<option value="<s:property value="#subObj.id" />">
	<s:property value="codeNameDisplay(#subObj.productInfoCode,#subObj.productInfoName)" /></option>                        		
	</s:iterator>
</select>
</div>
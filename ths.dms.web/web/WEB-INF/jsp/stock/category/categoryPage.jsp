<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@page import="ths.dms.helper.Configuration"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<div class="BreadcrumbSection">
	<ul class="ResetList FixFloat BreadcrumbList">
		<li class="Sprite1"><a href="/stock/category/info">Kho</a></li>
		<li><span>Tạo danh sách mặt hàng kiểm kê</span></li>
	</ul>
</div>
<div class="CtnOneColSection">
	<div class="ContentSection">
		<div class="ToolBarSection">
			<div class="SearchSection GeneralSSection">
				<h2 class="Title2Style">Thông tin chung</h2>
				<div class="SearchInSection SProduct1Form">
					<label class="LabelStyle Label1Style">Mã kiểm kê</label> 
					<input id="cycleCountCode" maxlength="50" value="<s:property value="cycleCount.cycleCountCode"/>" type="text" class="InputTextStyle InputText1Style" disabled="disabled" /> 
					
					<label class="LabelStyle Label1Style" style="width:200px;">Ngày bắt đầu kiểm kê</label>
					<input id="startDate" value="<s:date name="cycleCount.startDate" format="dd/MM/yyyy"/>" maxlength="10" type="text" class="InputTextStyle InputText1Style" disabled="disabled" /> 
<!-- 					<div class="Clear"></div> -->
					<label class="LabelStyle Label1Style" style="width:80px;">Mô tả</label> 
					<input id="cycleCountDes" disabled="disabled" style="width:150px" type="text" class="InputTextStyle InputText1Style" maxlength="200" value="<s:property value="cycleCount.description"/>"/>					
					<div class="Clear"></div>
					<p id="errMsgInfor" class="ErrorMsgStyle" style="display: none;"></p>										
				</div>
				<h2 class="Title2Style">Thông tin lựa chọn</h2>
				<div class="SearchInSection SProduct1Form">
					<label class="LabelStyle Label1Style">Loại kiểm kê</label>
					<div class="BoxSelect BoxSelect2">
						<select class="MySelectBoxClass" id="lsttype">
							<option value="0">Tất cả</option>
							<option value="1" selected="selected">Lựa chọn</option>
						</select>
					</div>
					<label class="LabelStyle Label1Style" style="width:200px">Số thẻ kiểm kho đầu tiên <span class="ReqiureStyle"> *</span></label> 
					<input maxlength="6" type="text" class="InputTextStyle InputText1Style" id="firstNumber" name="firstNumber" value="<s:property value="cycleCount.firstNumber"/>" /> 
					<div class="Clear"></div>
					<p  id="messageWarning"></p>
					<s:hidden name="shopCode"  id="shopCode" ></s:hidden> 					
				</div>
				<h2 class="Title2Style">Danh sách mặt hàng chọn</h2>
				<div class="SearchInSection SProduct1Form">
					<!--Đặt Grid tại đây-->
					<div class="GeneralTable General1Table">
						<div style="text-align: right; padding-bottom: 3px;">
							<s:if test="status==0">
								<button class="BtnGeneralStyle"  id="addProduct" onclick="StockCategory.openSelectProductDialog();" >Chọn mặt hàng</button>								
							</s:if>							
							<div class="Clear"></div>
						</div>
						<table width="100%" border="0" cellspacing="0" cellpadding="0">
								<colgroup>
									<col style="width: 35px;" />
									<col style="width: 100px;" />
									<col style="width: 130px;" />
									<col style="width: 350px;" />
									<col style="width: 35px;" />
								</colgroup>
								<thead>
									<tr>
										<th class="ColsThFirst">STT</th>
										<th>Thẻ kiểm kho</th>
										<th>Mã mặt hàng</th>
										<th>Tên mặt hàng</th>
										<th class="ColsThEnd">Xoá</th>
									</tr>
								</thead>
								 <tbody id="lstProducts"></tbody>
							</table>	
							<div class="Clear"></div>
							<div style="text-align: right; padding-bottom: 2px;padding-top: 10px;">
								<s:if test="status==0">					
									<button disabled="disabled"  class="BtnGeneralStyle" onclick="return StockCategory.save();">Lưu</button>											
								</s:if>	
								<button class="BtnGeneralStyle" id="printEx" onclick="return StockCategory.showDialogPrint();">In kiểm kê</button>
							</div>
							<div class="Clear"></div>
							<p id="succMsg" class="SuccessMsgStyle" style="display: none"></p>
							<p id="errMsg" class="ErrorMsgStyle" style="display: none;"></p>
					</div>
					
				</div>
				<div class="Clear"></div>
			</div>
			<div class="Clear"></div>
		</div>
	</div>
	<div class="Clear"></div>
</div>
<div id ="productDialog" style="width:600px;visibility: hidden;" >
	<div id="productEasyUIDialog" class="easyui-dialog" title="Thông tin sản phẩm" data-options="closed:true,modal:true">
		<div class="PopupContentMid">
			<div class="GeneralForm Search1Form" >
				<label class="LabelStyle Label1Style" style=" width: 100px;">Mã sản phẩm</label>
				<input id="productCode" maxlength="50" tabindex="1" type="text" style=" width: 196px;" class="InputTextStyle InputText1Style" /> 
				<label class="LabelStyle Label1Style" style=" width: 105px;">Tên sản phẩm</label> 
				<input id="productName" type="text" maxlength="250" tabindex="2" style="width: 196px;"  class="InputTextStyle InputText1Style" />
				<div class="Clear"></div> 
				
				<!-- me -->			
				 						<label class="LabelStyle Label1Style" style=" width: 100px;">Ngành hàng</label>
                                        <div class="BoxSelect BoxSelect2" id="cat"> 
                                        	<select id="category" class="MySelectBoxClass" >  
                                        		<option value="0">Chọn ngành hàng</option>                                      		
                                        		<s:iterator var="obj" value="lstCategoryType"> 
                                        		     <option value="<s:property value="#obj.id" />">
                                        		     	<s:property value="codeNameDisplay(#obj.productInfoCode,#obj.productInfoName)" /></option>                        		
                                        		</s:iterator>
                                        	</select> 
                                        </div>
                                        <label class="LabelStyle Label2Style" style=" width: 100px;">Ngành hàng con</label>
                                        <div class="BoxSelect BoxSelect2" id="subcat">
                                        	<select id="sub_category" class="MySelectBoxClass" >  
                                        		<option value="0">Chọn ngành hàng con</option>
<%--                                         		<s:iterator var="subObj" value="lstSubCategoryType">  --%>
<%--                                         		     <option value="<s:property value="#subObj.id" />"> --%>
<%--                                         		     	<s:property value="codeNameDisplay(#subObj.productInfoCode,#subObj.productInfoName)" /></option>                        		 --%>
<%--                                         		</s:iterator>                                      		 --%>
                                        	</select> 
                                        </div>
                                        <div class="Clear"></div>
                                        <label class="LabelStyle Label1Style" style=" width: 100px;">Số lượng</label>
                                        <input type="text" maxlength="8" id="fAmnt" style=" width:91px" class="InputTextStyle InputText3Style" />
                                        <span class="TempStyle"> - </span>
                                        <input type="text" maxlength="8" id="tAmnt" style=" width:91px" class="InputTextStyle InputText3Style" />
				
				
				
				
				<!-- me -->				
				<div class="Clear"></div> 
				<div class="BtnCenterSection">
					<button id="btnSearch" onclick="return StockCategory.searchProduct();" class="BtnGeneralStyle" tabindex="3" id="issSearchProductDialog">Tìm kiếm</button>
				</div>				
				<div class="Clear"></div>
				<div class="GridSection" id="productGridContainer">					
					<table id="productGrid" class="easyui-datagrid"></table>
					<div id="productPager"></div>
				</div>
				<div class="BtnCenterSection">
					<button onclick="StockCategory.selectListProducts();" class="BtnGeneralStyle">Chọn</button>
					<button class="BtnGeneralStyle BtnGeneralMStyle" onclick="$('.easyui-dialog').dialog('close');">Đóng</button>
				</div>
				 <p id="errMsg" class="ErrorMsgStyle" style="display: none;"></p>
			</div>
		</div>
	</div>
</div>
<div id ="displayChooseCycleCountMapProduct" style="visibility:hidden;" >	
	<div id="CycleCountMapEasyUIDialog" class="easyui-dialog" title="In kiểm kê" data-options="closed:true,modal:true" style="width:670px;height:140px;">
		<div class="PopupContentMid">
			<div class="GeneralForm Search1Form">
				<label class="LabelStyle Label6Style" style="width: 124px;">Dãy số thẻ kiểm kho</label> 
				<input type="text" class="InputTextStyle InputText10Style"  id="fffirstNumber"  style="width: 100px;" maxlength="8"/> 
				<label class="LabelStyle Labe20Style" style="padding-left: 10px;"> - </label>
				<input type="text" class="InputTextStyle InputText10Style" id="lastNumber" style="width: 100px;" maxlength="8"/>
				<div class="Clear"></div>
				<div class="BtnCenterSection" style=" padding-left: 150px;">
					<button id="dlPrintExce" onclick="return StockCategory.printExecute();" class="BtnGeneralStyle">In phiếu kiểm kê</button>
					<button id="dlCloseExce" class="BtnGeneralStyle BtnGeneralMStyle" onclick="$('.easyui-dialog').dialog('close');">Đóng</button>
				</div>
				 <p id="errMsg" class="ErrorMsgStyle" style="display: none;"></p>
			</div>
		</div>
	</div>
</div>
<s:hidden id="shopId" name="shopId"></s:hidden>
<script type="text/javascript">
	var status = '<s:property value="status"/>';	
	$(document).ready(function() {
		$('#fffirstNumber, #lastNumber').bind('keyup',function(event){
			if(event.keyCode == keyCodes.ENTER){
				$('#dlPrintExce').click(); 
			}
		});
		$(window).bind('keyup',function(e){
			if(e.keyCode==keyCodes.F5){
			window.location.href = window.location.pathname;
			}
		});		
		Utils.bindFormatOnTextfield('fAmnt',Utils._TF_NUMBER);
		Utils.bindFormatOnTextfield('tAmnt',Utils._TF_NUMBER);
		StockCategory._pMap = new Map();
	/* 	var cycleTypeInDB = '<s:property value="cycleType"/>';
		if (cycleTypeInDB == '') {
			$('#lsttype').val(1);
			$('#lsttype').change();
		} else {
			$('#lsttype').val(cycleTypeInDB);
			$('#lsttype').change();
		} */
		StockCategory.processAccordingToCycleType();
		Utils.bindFormatOnTextfield('firstNumber',Utils._TF_NUMBER);
		$('#firstNumber').change(function() {
			$('.ErrorMsgStyle').html('').hide();
			if(!StockCategory.checkFirstNumber()){
				return false;
			}
			var fist = parseInt($('#firstNumber').val().trim());	
			for ( var i = 0; i < StockCategory._pMap.size(); i++) {
				var temp = StockCategory._pMap.get(StockCategory._pMap.keyArray[i]);
				if (temp != null && temp.isDelete == 0) {
					temp.stockCardNumber = fist++;
					StockCategory._pMap.put(StockCategory._pMap.keyArray[i],temp);
				}
			}
			StockCategory.fillTable();			
		});
		$('#lsttype').change(function() {
			$('.ErrorMsgStyle').html('').hide();
			if(!StockCategory.checkFirstNumber()){
				$(this).val(StockCategory.lastValue);				
				return false;
			}
			StockCategory.processAccordingToCycleType();			
		});
		$('#lsttype').bind('focus',function(){
			StockCategory.lastValue = $(this).val();
		});
		if (status != '' && status == 0) {
			$('.BoxSelect1').removeClass('BoxDisSelect');
			/* $('#lsttype').customStyle(); */
			$('#firstNumber').removeAttr('disabled');
			$('.BtnGeneralStyle').removeAttr('disabled');
			$('.BtnGeneralStyle').removeClass('BtnGeneralDStyle');
		} else {			
			$('.BoxSelect2').addClass('BoxDisSelect');
		/* 	$('#lsttype').customStyle().attr('disabled','disabled'); */
			$('#firstNumber').attr('disabled', 'disabled');
			disabled('lsttype');			
			//$('.BtnGeneralStyle').attr('disabled', 'disabled');
			//$('.BtnGeneralStyle').addClass('BtnGeneralDStyle');
			//MySelectBoxClass
			//$('.BtnGeneralStyle').hide();
		}
		enable('printEx');
		enable('dlPrintExce');
		enable('dlCloseExce');
		StockCategory._status = status;	
	});						
</script>
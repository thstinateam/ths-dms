<%@ page language="java" contentType="text/html; charset=UTF-8"	pageEncoding="UTF-8"%>
<%@page import="ths.dms.helper.Configuration"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<div class="GeneralTable Table6Section Table29Section">	
	<div class="BoxGeneralTTitle">
		<table width="100%" border="0" cellspacing="0" cellpadding="0">
	       <colgroup>
	           <col style="width:80px;" />
	           <col style="width:330px;" />
	           <col style="width:200px;" />
	           <col style="width:110px;" />
	           <col style="width:200px;" />
	       </colgroup>
	       <thead>
	           <tr>
	               <th class="ColsThFirst">Mã sản phẩm</th>
	               <th>Tên sản phẩm</th>
	               <th>Đơn giá</th>
	               <th>Số lượng</th>
	               <th class="ColsThEnd">Tổng tiền</th>
	           </tr>
	       </thead>
	   </table>
	</div>
	<s:if test="lstPOAutoDetail != null && lstPOAutoDetail.size() > 0">
		<div class="BoxGeneralTBody">
			<div class="ScrollBodySection">
				<table width="100%" border="0" cellspacing="0" cellpadding="0">
					<colgroup>
                        <col style="width:80px;" />
                        <col style="width:330px;" />
                        <col style="width:200px;" />
                        <col style="width:110px;" />
                        <col style="width:200px;" />
                    </colgroup>
					<tbody>						
						<s:iterator id="lst_POAutoDetail" value="lstPOAutoDetail" status="u">
							<tr>
                                <td class="ColsTd1"><s:property value="#lst_POAutoDetail.product.productCode"/></td>
                                <td class="ColsTd2"><s:property value="#lst_POAutoDetail.product.productName"/></td>
                                <td class="ColsTd3">
                                	<span class="currencyPOAutoDetail">
                                		<s:property value="#lst_POAutoDetail.priceValue"/>
                                	</span>
                                </td>	                              
                                <td class="ColsTd4">	                                	
                                	<s:property value="cellQuantityFormatter(#lst_POAutoDetail.quantity,#lst_POAutoDetail.convfact)"/>
                                </td>
                                <td class="ColsTd15 ColsTdEnd">
                                	<span class="currencyPOAutoDetail">
                                		<s:property value="#lst_POAutoDetail.amount"/>
                                	</span>
                                </td>	                                
                            </tr>
						</s:iterator>						
					</tbody>
				</table>
			</div>
		</div>
	</s:if>
	<s:else>
		<p>Không có dữ liệu nào</p>
	</s:else>
</div>
<label class="LabelStyle Label1Style">Tổng tiền:</label>
<p class="ValueStyle Value2Style"><span class="currencyPOAutoDetail"><s:property value="totalAmount"/></span></p>
<div class="Clear"></div>
<script type="text/javascript">
$(document).ready(function(){
	$('.ScrollBodySection').jScrollPane();
	$('.currencyPOAutoDetail').each(function () {		
		if($(this).html().length > 0){
			$(this).html(formatCurrency($(this).html()));	
		}		
	});
});
</script>
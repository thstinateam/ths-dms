<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<div class="BreadcrumbSection">
       	<ul class="ResetList FixFloat BreadcrumbList">
           <li class="Sprite1"><a href="javascript:void(0);">Kho</a></li>
           <li><span>Điều chuyển kho</span></li>
    	</ul>
</div>
<div class="CtnOneColSection">
	<div class="ContentSection">
		<div class="ToolBarSection">
			<div class="SearchSection GeneralSSection">
				<h2 class="Title2Style">Thông tin tìm kiếm</h2>
				<div class="SearchInSection SProduct1Form">
					<label class="LabelStyle Label1Style">Mã phiếu</label> 
					<input id="txtStockTransCode" type="text" class="InputTextStyle InputText1Style" disabled="disabled" /> 
					<label class="LabelStyle Label1Style">Ngày lập</label>
					<input id="txtStockTransDate" type="text" class="InputTextStyle InputText1Style vinput-date" />
					<label class="LabelStyle Label1Style">Người lập</label> 
					<input id="txtStaffRootName" type="text" class="InputTextStyle InputText1Style" value="<s:property value="currentUser.staffRoot.staffName" />" disabled="disabled" />
					<div class="Clear"></div>
					<label class="LabelStyle Label1Style"><s:text name="Từ đơn vị"/></label>
                    <div class="BoxSelect BoxSelect2" id="shopOutputDiv">
                        <select id="shopOutput" style="width: 206px;">
                        </select>
                    </div>
                    <label class="LabelStyle Label1Style">Chọn kho xuất</label> 
					<div class="BoxSelect BoxSelect2" id="dllStockOutputDiv">
						<select id="dllStockOutput" style="width:206px;">
						</select>                            
					</div>
					<div class="Clear"></div>
					<label class="LabelStyle Label1Style"><s:text name="Đến đơn vị"/></label>
					<div class="BoxSelect BoxSelect2" id="shopInputDiv">
                        <select id="shopInput" style="width: 206px;">
                        </select>
                    </div>				
					<label class="LabelStyle Label1Style">Chọn kho nhập</label>
					<div class="BoxSelect BoxSelect2" id="dllStockInputDiv">
						<select id="dllStockInput" style="width:206px;">
						</select>                            
					</div>
				</div>
			</div>
			<div class="Clear"></div>
		</div>
		<div class="GeneralCntSection">
			<div class="Clear"></div>
			<h2 class="Title2Style">Danh sách sản phẩm</h2>
			<div class="GridSection" id="prdStockOpDgContGrid">
				 <table id="prdStockOutputDg"></table>
			</div>
			<div class="GeneralForm GeneralNoTP1Form" style="width:auto;">
                <div class="Func1Section" style="width:auto; min-width: 150px;">
                <label class="LabelStyle Label1Style" style="text-align: left; width: 100px; padding-top: 2px; float: left;">Tổng trọng lượng</label>
                <label class="LabelStyle Label1Style" id="lblSumWeightDg" style="float: left; font-weight: bold; text-align: left; color: red; width: 90px;"></label>
                	<label class="LabelStyle Label1Style" style="text-align: left; width: 55px; padding-top: 2px; float: left;">Tổng tiền</label>
                	<label class="LabelStyle Label1Style" id="lblSumAmountDg" style="float: left; font-weight: bold; text-align: left; color: red; width: 120px;"></label> 
                </div>
                <div class="Clear"></div>
            </div>
		</div>
		<div class="SearchSection GeneralSSection">
				<div class="SearchInSection SProduct1Form">
					<p id="errMsg" style="display: none;" class="ErrorMsgStyle"></p>
					<p id="successMsg" class="SuccessMsgStyle" style="display:none;"></p>
					<div class="Clear"></div>
					<div class="BtnCenterSection">
						<button id="btnTransferWarehouse" class="BtnGeneralStyle cmsiscontrol" onclick="TransferInternalWarehouse.createStockTransDetail();">Lưu</button>
					</div>
				</div>
			</div>
			<div class="Clear"></div>
		</div>
</div>

<!-- Dialog Form -->
<div class="ContentSection" id="divDiaglogForm" style="display: none;">
	<tiles:insertTemplate template="/WEB-INF/jsp/stock/internal-warehouse/dialogSearchProductChangeQuantityManager.jsp" />
</div>

<script type="text/javascript">
	$(document).ready(function() {
		TransferInternalWarehouse.loadShopCbx();
		
 		disableDateTimePicker('txtStockTransDate');
 		$('#txtStockTransDate').width($('#txtStockTransDate').width() - 22);
 		$('#dllStockOutputDiv, #dllStockInputDiv').bind('keyup',function(event){
 			if(event.keyCode == keyCodes.ENTER){
 				$('#btnTransferWarehouse').click();
 			}
 		});
 		$('#shopOutputDiv').bind('keyup',function(event){
 			if(event.keyCode == keyCodes.ENTER){
 				TransferInternalWarehouse.changeShop($('#shopOutput').combobox('getValue'), '#dllStockOutput', true);
 			}
 		});
 		$('#shopInputDiv').bind('keyup',function(event){
 			if(event.keyCode == keyCodes.ENTER){
 				TransferInternalWarehouse.changeShop($('#shopInput').combobox('getValue'), '#dllStockInput', false);
 			}
 		});
 		$('#prdStockOutputDg').datagrid({
			data: [],
			width : $('#prdStockOpDgContGrid').width() - 15,
			fitColumns : true,
		    rownumbers : true,
		    singleSelect:true,
		    height: 275,
		    columns:[[
				{field:'productCode',title:'Mã sản phẩm', width:120,align:'left',formatter: function(value, row, index){
					return Utils.XSSEncode(value);
				}
				, editor: {
					type:'combobox',
						options:{ 
						valueField:'productId',
					    textField:'productCode',
					    data: [],
					    width: 200,
					    formatter: function(row) {
					    	return '<span style="font-weight:bold">' + Utils.XSSEncode(row.productCode) + '</span><br/>' + '<span style="color:#888">' + Utils.XSSEncode(row.productName) + '</span>';
					    },
					    onSelect:function(rec){
					    	TransferInternalWarehouse._rowDgTmp = {
				    			productId: rec.productId,
								productCode: Utils.XSSEncode(rec.productCode),
								productName: Utils.XSSEncode(rec.productName),
								convfact: rec.convfact,
								warehouseId: rec.warehouseId,
								availableQuantity: rec.availableQuantity,
								price: rec.price,
								quantity: 0,
								amount: 0
					    	};
					    	$('#dgProductName0').html(Utils.XSSEncode(rec.productName.trim())).change();
					    	$('#dgAvlQuantity0').val(StockValidateInput.formatStockQuantity(TransferInternalWarehouse._rowDgTmp.availableQuantity, TransferInternalWarehouse._rowDgTmp.convfact));
					    	$('#dgQuantity0').val(StockValidateInput.formatStockQuantity(0, TransferInternalWarehouse._rowDgTmp.convfact));
					    	$('#dgPrice0').html(VTUtilJS.formatCurrency(Utils.formatDoubleValue(TransferInternalWarehouse._rowDgTmp.price)));
					    	$('#dgAmount0').html(VTUtilJS.formatCurrency(Utils.formatDoubleValue(TransferInternalWarehouse._rowDgTmp.amount)));
					    	
					    	$('#dgAvlQuantity0, #dgQuantity0').bind('keyup',function(event) {
					 			if (event.keyCode == keyCodes.ENTER) {
					 				TransferInternalWarehouse._mapStockTransDetail.put(TransferInternalWarehouse._rowDgTmp.productId, TransferInternalWarehouse._rowDgTmp);
					 				$('.ErrorMsgStyle').html('').hide();
					 				$('.SuccessMsgStyle').html('').hide();
					 				$('#prdStockOutputDg').datagrid("loadData", TransferInternalWarehouse.getDataInPrdStockOutputDg());
					 			}
					 		});
					    },
					    onChange:function(pCode){
					    	$('.combo-panel').each(function(){
								if(!$(this).is(':hidden')){
									$(this).parent().css('width','250px');
									$(this).css('width','248px');
								}			
							});
					    },
					    filter: function(q, row){
							q = new String(q).toUpperCase();
							var opts = $(this).combobox('options');
							return row[opts.textField].indexOf(q)>=0;
						}
					}
				}},
			    {field:'productName', title:'Tên sản phẩm', width:120, align:'left', formatter: function(value, row, index){
			    	var prdName = 'dgProductName'+ Number(row.productId);
			    	return '<label id="'+prdName+'" style="width: 100%; text-align: left;">'+Utils.XSSEncode(value)+'</label>';
			    }},
			    {field:'availableQuantity', title:'Tồn kho đáp ứng', width:80, align:'right', formatter: function(value, row, index){
			    	var idAvlQ = "dgAvlQuantity"+ Number(row.productId);
			    	var html = '<input type="text" id= "'+idAvlQ+'" onchange="" value="'+StockValidateInput.formatStockQuantity(row.availableQuantity, row.convfact)+'" style="width: 100%; text-align: right;" maxlength="18" disabled="disabled" />';
			    	return html;
			    }},
			    {field:'quantity', title:'Số lượng', width:80, align:'right', formatter: function(value, row, index){
			    	var idQuan = "dgQuantity"+ Number(row.productId);
			    	var html = '<input type="text" id= "'+idQuan+'" onchange="TransferInternalWarehouse.changeQuanttInPrdStockOutputDg(this);" value="'+StockValidateInput.formatStockQuantity(row.quantity, row.convfact)+'" style="width: 100%; text-align: right;" maxlength="18"/>';
			    	return html;
			    }},
			    {field:'price', title:'Giá bán', width:80, align:'right', formatter: function(value, row, index){
			    	var idPrice= "dgPrice"+ Number(row.productId);
			    	return '<label id="'+idPrice+'" style="width: 100%; text-align: left;">'+ VTUtilJS.formatCurrency(Utils.formatDoubleValue(value))+'</label>';
			    }},
			    {field:'amount', title:'Thành tiền', width:80, align:'right', formatter: function(value, row, index){
			    	var idAmount = "dgAmount"+ Number(row.productId);
			    	return '<label id="'+idAmount+'" style="width: 100%; text-align: left;">'+ VTUtilJS.formatCurrency(Utils.formatDoubleValue(value))+'</label>';
			    }},
			    {field:'detelte',title:'',width:30, align:'center', formatter: function(value, row, index){
			    	if (row.productId != undefined && row.productId != null && row.productId > 0) { 
			    		return '<a href="javascript:void(0)" id="dg_prdStockOutputDg_delete" onclick="return TransferInternalWarehouse.deleteRowInPrdStockOutputDg('+Number(row.productId)+');"><img title="Xóa" src="/resources/images/icon_delete.png" width="15" heigh="15"></a>';
			    	}
			    }}
			]],
			onClickRow: function(rowIndex, rowData) {
				
			},
			onAfterEdit: function(rowIndex, rowData, changes) {
			},
			onBeforeEdit : function(rowIndex, rowData, changes){
			},
	        onLoadSuccess :function(data){
		    	 $('.datagrid-header-rownumber').html('STT');
		    	 Utils.updateRownumWidthAndHeightForDataGrid('prdStockOutputDg');
		    	 var arrDelete =  $('#promotionStaffGrid td[field="delete"]');
		    	 if (arrDelete != undefined && arrDelete != null && arrDelete.length > 0) {
		    	   for (var i = 0, size = arrDelete.length; i < size; i++) {
		    	 	$(arrDelete[i]).prop("id", "gr_table_td_del_" + i);//Khai bao id danh cho phan quyen
		    	 	$(arrDelete[i]).addClass("cmsiscontrol");
		    	   }
		    	 }
		    	 Utils.functionAccessFillControl('prdStockOpDgContGrid', function(data){
		    	 	//Xu ly cac su kien lien quan den cac control phan quyen
		    	 	var arrTmpLength =  $('#prdStockOpDgContGrid td[id^="gr_table_td_del_"]').length;
		    	 	var invisibleLenght = $('.isCMSInvisible[id^="gr_table_td_del_"]').length;
		    	 	if (arrTmpLength > 0 && arrTmpLength != invisibleLenght) {
		    	 		$('#prdStockOutputDg').datagrid("showColumn", "delete");
		    	 	} else {
		    	 		$('#prdStockOutputDg').datagrid("hideColumn", "delete");
		    	 	}
		    	 });
	    	}
		});
		//xu ly su kien ban dau
		$('#dllStockOutput').change();
	});

</script>

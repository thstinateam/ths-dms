<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<div id="cmndl-product-search-2-textbox" class="easyui-dialog" data-options="closed:true, modal:true" style="width:670px;height:auto;">
	<div class="PopupContentMid2">
		<div class="GeneralForm Search1Form">
			<label id="label-code" class="LabelStyle Label1Style" style="width: 140px;">Mã sản phẩm</label>
			<input id="txtCmnProductCode" maxlength="50" type="text" style="width: 145px;" value="" class="InputTextStyle InputText1Style" />
			<label id="label-name" class="LabelStyle Label1Style" style="width: 130px;">Tên sản phẩm</label>
			<input id="txtCmnProductName" maxlength="250" type="text" style="width: 145px;" value="" class="InputTextStyle InputText1Style" />
			<div class="Clear"></div>
			<div class="Clear"></div>
			<div class="BtnCenterSection">
				<button class="BtnGeneralStyle BtnGeneralMStyle" id="cmndl-product-button-search" onclick="TransferInternalWarehouse.searchDialogInsertProduct();">Tìm kiếm</button>
			</div>
			<div class="Clear"></div>
			<div class="GridSection" id="cmndl-product-grid-container">
				<table id="cmndl-product-grid-search"></table>
			</div>
			<div class="BtnCenterSection">
				<button id="btnChooseProduct" class="BtnGeneralStyle BtnGeneralMStyle" onclick="TransferInternalWarehouse.chooseProductInDialogSearch();">Đồng ý</button> &nbsp;
				<button class="BtnGeneralStyle BtnGeneralMStyle" onclick="$('#cmndl-product-search-2-textbox').dialog('close');">Đóng</button>
			</div>
			<p id="errMsgCmndlProduct" style="display: none;" class="ErrorMsgStyle"></p>
			<p id="successMsgCmndlProduct" class="SuccessMsgStyle" style="display:none;"></p>
		</div>
	</div>
</div>
<script type="text/javascript">
	$(function(){
		$('#txtCmnProductCode, #txtCmnProductName').bind('keyup',function(event){
			if(event.keyCode == keyCodes.ENTER){
				$('#cmndl-product-button-search').click();
			}
		});
	});
</script>
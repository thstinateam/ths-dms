<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%-- <h2 class="Title2Style">Thông tin kiểm kê</h2>
<div class="SearchInSection SProduct1Form">
	<label class="LabelStyle Label1Style">Ghi ngày <span class="ReqiureStyle"> * </span></label> 
	<s:if test="cycleCountType == 0">	
		<input maxlength="10" id="approveDate" class="InputTextStyle InputText1Style" style="width:175px;"  />
	</s:if>
	<s:else>
		<input maxlength="10" disabled="disabled" id="approveDate" class="InputTextStyle InputText1Style"  />
	</s:else>
	
	<label class="LabelStyle Label1Style">Diễn giải</label> 
	<input type="text" class="InputTextStyle InputText1Style" disabled="disabled" id="cycleCountDes" maxlength="250" value="<s:property value="cycleCount.description"/>" />
	<div class="Clear"></div>
	
<!-- 	<label class="LabelStyle Label1Style">Số thẻ đầu tiên</label> -->
	<input type="text" class="InputTextStyle InputText1Style" id="firstNumber"  disabled="disabled" value="<s:property value="cycleCount.firstNumber"/>" /> 
	
<!-- 	<label class="LabelStyle Label1Style">Số thẻ mới nhất</label> -->
	<input type="text" class="InputTextStyle InputText1Style" id="lastNumber" disabled="disabled" value="<s:property value="maxNumber"/>" />
<!-- 	<div class="Clear"></div> -->
	
<!-- 	<label class="LabelStyle Label1Style">Tổng số thẻ</label> -->
	<input type="text" class="InputTextStyle InputText1Style" id="totalNumber" disabled="disabled" value="<s:property value="totalNumber"/>" />
	
	<label class="LabelStyle Label1Style">Kho</label>
	<input type="text" class="InputTextStyle InputText1Style" id="warehouseName" disabled="disabled" value="<s:property value="cycleCount.warehouse.warehouseName"/>" />
	<input type="hidden" id="warehouseId" value="<s:property value="cycleCount.warehouse.id"/>" />
<!-- 	<div class="Clear"></div> -->
	
	<label class="LabelStyle Label1Style">Trạng thái <span class="ReqiureStyle"> * </span></label>
	<div class="BoxSelect BoxSelect2">
		<s:if test="cycleCountType == 0 || cycleCountType == null">
			<select id="cycleCountType" class="MySelectBoxClass">
				<option value="-1">---Chọn trạng thái---</option>
				<option value="1">Hoàn thành</option>
				<option value="2">Hủy bỏ</option>
			</select>
		</s:if>
		<s:else>
			<select id="cycleCountType" class="MySelectBoxClass" disabled="disabled">
				<option value="-1">---Chọn trạng thái---</option>
				<s:if test="cycleCountType == 1">
					<option value="1" selected="selected">Hoàn thành</option>
				</s:if>
				<s:else>
					<option value="2" selected="selected">Hủy bỏ</option>
				</s:else>
			</select>
		</s:else>
	</div>
	<div class="Clear"></div>
</div> --%>
<h2 class="Title2Style">Thông tin thẻ kiểm kho</h2>
<div class="SearchInSection SProduct1Form">
	<div class="GeneralTable General1Table">
<!-- 		<div class="ButtonRSection" style="text-align:right;padding-bottom: 5px;"> -->
<%-- 			<s:if test="cycleCountType == 0"> --%>
<!-- 				<button id="btnAddCycleCountMapProduct"	class="BtnGeneralStyle" onclick="StockCountingApprove.openSelectProductDialog();"> -->
<%-- 					<span class="Sprite2">Thêm thẻ kiểm kho</span> --%>
<!-- 				</button> -->
<%-- 			</s:if> --%>
<!-- 		</div> -->
		<table width="100%" border="0" cellspacing="0" cellpadding="0">
			<colgroup>
				<col style="width: 100px;" />
				<col style="width: 90px;" />
				<col style="width: 250px;" />
				<col style="width: 50px;" />
				<col style="width: 87px;" />
				<col style="width: 100px;" />
				<col style="width: 120px;" />
<!-- 				<col style="width: 90px;" /> -->
				<col style="width: 90px;">
<!-- 				<col style="width: 35px;" /> -->
			</colgroup>
			<thead>
				<tr>
					<th class="ColsThFirst">Số thứ tự</th>
					<th>Mã sản phẩm</th>
					<th>Tên sản phẩm</th>
					<th>ĐVT</th>
					<th>Ngày kiểm kê cuối</th>
					<th>Số lượng chốt</th>
					<th>Số lượng thực</th>
<%-- 					<s:if test="cycleCountType == 0|| cycleCountType == null"> --%>
<!-- 						<th>Nhập chi tiết</th> -->
<%-- 					</s:if> --%>
<%-- 					<s:else> --%>
<!-- 						<th>Xem chi tiết</th> -->
<%-- 					</s:else> --%>
<!-- 					<th>Chênh lệch</th> -->
					<th class="ColsThEnd">Chênh lệch</th>
				</tr>
			</thead>
			<tbody id="cycleCountMapProductsTableDetail">
				<tiles:insertTemplate template='/WEB-INF/jsp/stock/counting-approve/approveStockTable.jsp' />
			</tbody>
		</table>
	</div>
	
	<div class="GeneralForm GeneralNoTP1Form" id="divGroupEditDb">
		<div class="Func1Section">		
			<button class="BtnGeneralStyle" id="btnExport" onclick="StockCountingApprove.exportExcel();">Xuất excel</button>
			<s:if test="cycleCountType == 4">
				<button class="BtnGeneralStyle " id="btnApprove" onclick="StockCountingApprove.approve(1);">Duyệt</button>
				<button class="BtnGeneralStyle" id="btnReject" onclick="StockCountingApprove.approve(3);">Từ chối</button>
			</s:if>
			<s:if test="cycleCountType != 2 && cycleCountType != 1 && cycleCountType != 0">
				<button class="BtnGeneralStyle" id="btnReject" onclick="StockCountingApprove.approve(2);">Hủy bỏ</button>
			</s:if>
<%-- 			<s:else> --%>
<!-- 				<button class="BtnGeneralStyle" id="btnApprove" onclick="StockCountingApprove.approve();" style="display:none;">Lưu</button> -->
<%-- 			</s:else>	 --%>
		</div>
		<div class="Clear"></div>
	</div>
</div>

<div id ="reasonDialog" style="display: none" >
	<div id="reasonDialogEasyUIDialog">
		<div class="PopupContentMid">
			<div class="GeneralForm Search1Form" >
<!-- 				<label class="LabelStyle Label1Style" style=" width: 100px;" id='lblQuestion'>Lý do</label> -->
<!-- 				<div class="Clear"></div>	 -->
				<label class="LabelStyle Label1Style">Lý do <span class="ReqiureStyle">*</span></label>
				<input type="text" multiple="multiple" class="InputTextStyle InputText1Style" id="txtReason" maxlength="50" style=" width: 200px;"/>
				<div class="Clear"></div>				
				<div class="BtnCenterSection">
					<button class="BtnGeneralStyle"  id="btnOK" onclick="StockCountingApprove.reject();" >Đồng ý</button>
					<button class="BtnGeneralStyle" onclick="$('.easyui-dialog').dialog('close');" id="btnClose">Hủy bỏ</button>	
				</div>		
				<p id="errMsgReason" style="display: none;" class="ErrorMsgStyle"></p>		                                    
			</div>
		</div>
	</div>
</div>

<p id="errMsg" class="ErrorMsgStyle" style="display: none"></p>
<tiles:insertTemplate template="/WEB-INF/jsp/general/successMsg.jsp" />

<input type="hidden" id="h_type" value="<s:property value='cycleCountType'/>"/>

<script type="text/javascript">
$(document).ready(function(){
// 	$('#cycleCountCode').focus();
// 	if ($("#approveDate:not(:disabled)").length > 0) {
// 		setDateTimePicker('approveDate');
// 	}
// 	$('#approveDate').val(getCurrentDate());
// 	$('#cycleCountType').customStyle();
	//Utils.bindTRbackground();
	$('#cycleCountMapProductsTableDetail td').css('padding','2px');
	Utils.functionAccessFillControl('divGroupEditDb', function () {
		//Xu ly su kien lien quan den phan quyen
	});
});
</script>
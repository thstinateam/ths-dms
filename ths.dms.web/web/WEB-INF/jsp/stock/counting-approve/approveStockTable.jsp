<%@ page language="java" contentType="text/html; charset=UTF-8"	pageEncoding="UTF-8"%>
<%@page import="ths.dms.helper.Configuration"%>
<%@ taglib prefix="s" uri="/struts-tags"%>

<input id="checkListEmpty" type="hidden" value="<s:property value="checkListEmpty"/>" />
<s:if test="lstCycleCountMapProducts != null && lstCycleCountMapProducts.size() > 0">
	<s:iterator value="lstCycleCountMapProducts" status="status">
	<tr id="tr_<s:property value='product.productCode' />">
		<td id="cardNumber" class="ColsTd1 AlignCCols" style="height: 20px;" >
			<s:property value='stockCardNumber' />
			<input type="hidden" class="CycleCountMapProducts" id="<s:property value='id'/>" value="<s:property value='product.productCode'/>"/>
			<input type="hidden" id="lot" value="<s:property value='product.checkLot'/>"/>
			<input type="hidden" id="convfact" value="<s:property value='product.convfact'/>"/>
		</td>
		<td class="ColsTd2"><s:property value='product.productCode' /></td>
		<td class="ColsTd3"><s:property value='product.productName' /></td>
		<td class="ColsTd4"><s:property value='product.uom1' /></td>
		<td class="ColsTd5 AlignCCols"><s:date name="countDate" format="dd/MM/yyyy" /></td>
		<td class="ColsTd6 AlignRight">
			<span id="td_qtyBfCountP_<s:property value='product.productCode' />"><s:property value='cellQuantityFormatter(quantityBeforeCount,product.convfact)'/></span>
		</td>
		<td class="ColsTd7 AlignRight" style="padding-right: 22px; text-align: right !important">
			<span id="td_qtyCountP_<s:property value='product.productCode' />"><s:property value='cellQuantityFormatter(quantityCounted,product.convfact)'/></span>
		</td>
<%-- 		<s:if test="cycleCountType == 0"> --%>
<%-- 			<s:if test="product.checkLot == 0"> --%>
<!-- 				<td class="ColsTd7" style="padding: :2px;"> -->
<%-- 					<input id="td_qtyCount_<s:property value='product.productCode' />" type="text" maxlength="9" onkeypress="return NextAndPrevTextField(event,this,'CCMapInput')" --%>
<%-- 						onchange="return StockCountingApprove.changeCCMap(this,<s:property value='id'/>,'<s:property value='product.productCode'/>',<s:property value='product.checkLot'/>,<s:property value='product.convfact'/>)"  --%>
<%-- 						class="InputTextStyle InputText1Style AlignRightInput BindFormatOnInputCss CCMapInput" value="<s:property value='cellQuantityFormatter(quantityCounted,product.convfact)'/>" /></td> --%>
<%-- 			</s:if>  --%>
<%-- 			<s:else> --%>
<!-- 				<td class="ColsTd7" style="padding-right: 22px; text-align: right !important"> -->
<%-- 					<span id="td_qtyCountP_<s:property value='product.productCode' />"><s:property value='cellQuantityFormatter(quantityCounted,product.convfact)'/></span> --%>
<!-- 				</td> -->
<%-- 			</s:else> --%>
<%-- 		</s:if> --%>
<%-- 		<s:else> --%>
<!-- 			<td class="ColsTd7 AlignRight" style="padding-right: 10px;"> -->
<%-- 			<span id="td_qtyCountP"><s:property value='cellQuantityFormatter(quantityCounted,product.convfact)'/></span></td> --%>
<%-- 		</s:else> --%>
<!-- 		<td class="ColsTd8 AlignCCols"> -->
<%-- 			<s:if test="product.checkLot == 1"> --%>
<%-- 			<a href="javascript:void(0)" onclick="StockCountingApprove.openCCResult(<s:property value='id'/>,<s:property value='product.id'/>, --%>
<%-- 				'<s:property value='product.productCode'/>','<s:property value='product.productName'/>',<s:property value='product.convfact'/>)" > --%>
<%-- 				<s:if test="cycleCountType == 0|| cycleCountType == null">Nhập chi tiết</s:if> --%>
<%-- 				<s:else>Xem chi tiết</s:else> --%>
<!-- 			</a> -->
<%-- 			</s:if> --%>
<!-- 		</td> -->
		<td class="ColsTd9 AlignRight ColsTdEnd" id="td_qtyDifference<s:property value="id"/>">
				<s:property value="convertConvfact(quantityBeforeCount - quantityCounted, product.convfact)"/>
		</td>
<!-- 		<td class="ColsTd9 ColsTdEnd"></td> -->
	</tr>
	</s:iterator>
</s:if>
<s:else>
	<tr><td colspan="9" class="NotData">Không có dữ liệu nào</td></tr>
</s:else>
<script type="text/javascript">
$(document).ready(function(){
	Utils.bindFormatOnTextfieldInputCss('BindFormatOnInputCss', Utils._TF_NUMBER_CONVFACT);
});
</script>

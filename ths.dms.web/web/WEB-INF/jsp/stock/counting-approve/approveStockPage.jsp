<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>

<input type="hidden" id="function_code" value="<s:property value="systimestamp"/>" />
<s:set id="downloadToken" value="%{generateReportToken()}"></s:set>
<input type="hidden" id="report_token" value="<s:property value="#downloadToken"/>" />
<s:set id="downloadToken" value="%{putSessionValue(systimestamp, #downloadToken)}"></s:set>
		
<div class="BreadcrumbSection">
	<ul class="ResetList FixFloat BreadcrumbList">
		<li class="Sprite1"><a href="javascript:void(0);">Kho</a></li>
		<li><span>Duyệt kiểm kho</span></li>
	</ul>
</div>

<div class="CtnOneColSection">
	<div class="ContentSection">
		<div class="ToolBarSection">
			<div class="SearchSection GeneralSSection">
				<h2 class="Title2Style">Thông tin tìm kiếm</h2>
				<div class="SearchInSection SProduct1Form">

					<label class="LabelStyle Label1Style"> Đơn vị </label> 
					<div class="BoxSelect BoxSelect">
						<input id="shop" style="width:206px;" />
					</div>
					<label class="LabelStyle Label1Style"><s:text name="sale_plan_year"/><span class="ReqiureStyle">*</span></label>
					<div class="BoxSelect BoxSelect2">                    	
						<tiles:insertTemplate template="/WEB-INF/jsp/general/yearPeriod.jsp" />
                       </div>
                       <label class="LabelStyle Label1Style"><s:text name="sale_plan_period"/><span class="ReqiureStyle">*</span></label>
					<div class="BoxSelect BoxSelect2">
						<select id="numPeriod" style="width:206px;">
					    </select>
					</div>						
					<div class="Clear"></div>

					<label class="LabelStyle Label1Style">Mã kiểm kê</label> 
					<input type="text" class="InputTextStyle InputText1Style" id="dgCycleCountCode" maxlength="50"/>
					 
					<label class="LabelStyle Label1Style">Trạng thái</label>
					<div class="BoxSelect BoxSelect2">
						<select id="dgCycleCountType" style="width:206px;">
							<option value="-1">Tất cả</option>
							<option value="5">Chưa kiểm kê</option>
							<option value="0">Đang kiểm kê</option>
							<option value="4">Chờ duyệt</option>
			                <option value="1">Đã duyệt</option>
			                <option value="3">Từ chối</option>
			                <option value="2">Hủy bỏ</option>
						</select>
					</div>
					<label class="LabelStyle Label1Style">Mô tả </label> 
					<input type="text" class="InputTextStyle InputText1Style" id="dgCycleCountDes"  maxlength="200" />
					<div class="Clear"></div>
					
					<label class="LabelStyle Label1Style">Từ ngày kiểm kê</label> 
					<input type="text" class="InputTextStyle InputText6Style vinput-date" id="dgCycleCountFromDate" maxlength="10"/>
					<label class="LabelStyle Label1Style">Đến ngày kiểm kê</label> 
					<input type="text" class="InputTextStyle InputText6Style vinput-date" id="dgCycleCountToDate" maxlength="10"/>
					<div class="Clear"></div>
					
					<div class="BtnCenterSection">
						<button class="BtnGeneralStyle" onclick="StockCountingApprove.searchCycleCount();">Tìm kiếm</button>
					</div>					
					<p id="saveMessageError" style="display: none;" class="ErrorMsgStyle"></p>
					<img id="loadingSearch" src="/resources/images/loading.gif" class="LoadingStyle" style="visibility: hidden;" />
					<div class="Clear"></div>
					<h2 class="Title2Style">Danh sách kiểm kê</h2>
					<div class="GridSection" id="productGridContainer">					
						<table id="grid"></table>
						<div id="pager"></div>
					</div>
					<div class="Clear"></div>
				</div>
				<div id="cycleCountInfo" ></div>				
			</div>
			<div class="Clear"></div>
		</div>
	</div>
	<div class="Clear"></div>
</div>

<input type="hidden" id="shopCode" value="<s:property value='shopCode'/>" />
<input type="hidden" id="shopId" value="<s:property value='shopId'/>" />

<div id ="cycleCountDialog" style="display:none;" >
	<div id="cycleCountEasyUIDialog">
		<div class="PopupContentMid">
			<div class="GeneralForm Search1Form" >
				<label class="LabelStyle Label1Style">Mã kiểm kê</label> 
				<input type="text" class="InputTextStyle InputText1Style" id="dgCycleCountCode" maxlength="50" style="width:130px;" />
				 
				<label class="LabelStyle Label1Style">Trạng thái</label>
				<div class="BoxSelect BoxSelect2">
					<select id="dgCycleCountType">
						<option value="-1">Chọn trạng thái</option>
						<option value="0">Đang thực hiện</option>
						<option value="1">Đã duyệt</option>
						<option value="2">Hủy bỏ</option>
		                <option value="3">Từ chối</option>
		                <option value="4">Chờ duyệt</option>
					</select>
				</div>
				<label class="LabelStyle Label1Style">Mô tả </label> 
				<input type="text" class="InputTextStyle InputText10Style" id="dgCycleCountDes"  maxlength="200" />
				<div class="Clear"></div>
				<label class="LabelStyle Label1Style"> Đơn vị </label> 
				<div class="BoxSelect BoxSelect">
					<input id="shop" style="width:140px;" />
				</div>
				
				<label class="LabelStyle Label1Style">Từ ngày tạo</label> 
				<input type="text" class="InputTextStyle InputText1Style" id="dgCycleCountFromDate" style="width: 175px;" type="text" maxlength="10"/>
				
				<label class="LabelStyle Label1Style">Đến ngày tạo</label> 
				<input type="text" class="InputTextStyle InputText1Style" id="dgCycleCountToDate" style="width: 140px;" type="text" maxlength="10"/>
				
				<div class="Clear"></div>
				<label class="LabelStyle Label1Style">Chọn kho</label>
				<div class="BoxSelect BoxSelect">
					<select id="cycleCountWareHouse" style="width:140px;">
                    </select>
                </div>
				<div class="Clear"></div>
				<div class="BtnCenterSection">
					<button class="BtnGeneralStyle" onclick="StockCountingApprove.searchCycleCountOnEsyUI();">Tìm kiếm</button>
				</div>
				
				<p id="saveMessageError" style="display: none;" class="ErrorMsgStyle"></p>
				<div class="Clear"></div>
				
				<div class="GridSection" id="productGridContainer">					
					<table id="grid"></table>
					<div id="pager"></div>
				</div>
				
				<div class="BtnCenterSection">
					<button class="BtnGeneralStyle BtnGeneralMStyle" onclick="$('#cycleCountEasyUIDialog').dialog('close');">Đóng</button>
				</div>				
			</div>
		</div>
	</div>
</div>

<div id ="productDialog" style="display:none;" >
	<div id="productEasyUIDialog">
		<div class="PopupContentMid">
			<div class="GeneralForm Search1Form" >
				<label class="LabelStyle Label1Style" style=" width: 100px;">Mã sản phẩm</label>
				<input id="productCode" maxlength="50" tabindex="1" type="text" style=" width: 145px;" class="InputTextStyle InputText1Style" /> 
				
				<label class="LabelStyle Label1Style" style=" width: 100px;">Tên sản phẩm</label> 
				<input id="productName" type="text" maxlength="250" tabindex="2" style="width: 300px;"  class="InputTextStyle InputText1Style" />
				<div class="Clear"></div> 
				
				<div class="BtnCenterSection">
					<button id="btnSearch"	onclick="StockCountingApprove.searchProduct();" class="BtnGeneralStyle" tabindex="3" id="issSearchProductDialog">Tìm kiếm</button>
				</div>
				<p id="addProductError" class="ErrorMsgStyle" style="display: none;"></p>
				<div class="Clear"></div>
				
				<div class="GridSection" id="productGridContainerEx">
					<table id="productGrid"></table>
					<div id="productPager"></div>
				</div>
				
				<div class="BtnCenterSection">
					<button id="chooseProduct" onclick="StockCountingApprove.selectListProducts();" class="BtnGeneralStyle">Chọn</button>
					<button class="BtnGeneralStyle BtnGeneralMStyle" onclick="$('#productEasyUIDialog').dialog('close');">Đóng</button>
				</div>
				<div class="Clear"></div> 
			</div>
		</div>
	</div>
</div>

<div id ="productLotDialog" style="display: none" >
	<div id="productLotEasyUIDialog" class="easyui-dialog" title="Thông tin lô" data-options="closed:true,modal:true">
		<div class="PopupContentMid">
			<div class="GeneralForm Search1Form" >
				<label class="LabelStyle Label1Style" style=" width: 100px;">Mã sản phẩm</label>
				<p id="d_productCode" class="LabelStyle Label1Style" style="color: #000000;font-weight: bold;text-align: left;"></p> 
				
				<label class="LabelStyle Label1Style" style=" width: 164px;">Tên sản phẩm</label> 
				<p id="d_productName" class="LabelStyle Label1Style" style="color: #000000;font-weight: bold;text-align: left;width: 230px;margin-bottom: 9px;"></p>                                    
				<div class="Clear"></div>
				
				<label class="LabelStyle Label1Style" style=" width: 100px;">Số lô <span class="ReqiureStyle">*</span></label>
				<input maxlength="6" id="d_lot" maxlength="50" type="text" style=" width: 145px;" class="InputTextStyle InputText1Style" />			
				
				<label class="LabelStyle Label1Style" style=" width: 100px;">Số lượng <span class="ReqiureStyle">*</span></label>
				<input id="d_qtyCount" type="text" class="InputTextStyle InputText1Style" maxlength="8"/>
				
				<div class="Clear"></div>
				<div class="BtnCenterSection">
					<button class="BtnGeneralStyle" id="btnSaveCCResult" onclick="StockCountingApprove.addCCResult();">Thêm</button>				
				</div>					
				<input type="text" style="display: none;" id="h_ProductCode" />
				<input type="text" style="display: none;" id="h_Convfact" />
				<div class="GridSection" id="productLotGridContainer">					
					<div class="GeneralTable General1Table">
						<table border="0" cellspacing="0" cellpadding="0">
							<colgroup>
								<col style="width: 80px;" />
								<col style="width: 160px;" />
								<col style="width: 160px;" />
								<col style="width: 160px;" />
								<col style="width: 50px;" />
							</colgroup>
							<thead>
								<tr>
									<th class="ColsThFirst">STT</th>
									<th>Số lô</th>
									<th>Số lượng chốt</th>
									<th>Số lượng đếm</th>
									<th class="ColsThEnd">Xóa</th>
								</tr>
							</thead>
							<tbody id="cycleCountResultsTableDetail">
								
							</tbody>
							<tfoot>
								<tr>
									<td colspan="2" style="border-left: 1px solid #C6D5DB;height: 24px;">Tổng:</td>
									<td style="text-align: right; padding: 6px 8px 0 0;" id="totalBeforeCount"></td>
									<td style="text-align: right; padding: 6px 8px 0 0;" id="totalCount"></td>
									<td style="text-align: center;" class="ColsTd7 ColsTdEnd">&nbsp;</td>
								</tr>
							</tfoot>
						</table>
					</div>					
				</div>		
				<div class="Clear"></div>				
				<div class="BtnCenterSection">
					<button class="BtnGeneralStyle"  id="btnCloseProductLot">Đóng</button>					
				</div>				                                    
				<div class="Clear"></div>
				<p id="errMsg2" style="display: none;"class="ErrorMsgStyle"></p>	              
			</div>
		</div>
	</div>
</div>

<input type="hidden" id="h_type" value="<s:property value='cycleCountType'/>"/>

<script type="text/javascript">
$(document).ready(function() {
	
	//load combobox don vi
	$('#shop').combotree({
		url: '/stock/approve/search-unit-tree',
		lines: true,
		formatter: function(node) {
			return Utils.XSSEncode(node.text);
		},
		onSelect: function(data){
        	if (data != null) {
        		$('#shopCode').val(data.attributes.shop.shopCode);
        		StockCountingApprove.changeShop();
        	}
	    },
	    onLoadSuccess: function(node, data) {
			$('#shop').combotree('setValue', data[0].id);
		}
	});
	
	yearPeriodChange = function (data) {
		var par = {};
 		par.year = $('#yearPeriod').val();
 		Utils.getJSONDataByAjax(par, '/commons/list-cycle-filter', function(data){
 			if (data != null && data.lstCycle != null) {
 				Utils.bindPeriodCbx('#numPeriod', data.lstCycle, data.currentNum, 206);
 			}
 		});
	};
	var yearPeriod = '<s:property value="yearPeriod"/>';
	$('#yearPeriod').val(yearPeriod);
	$('#yearPeriod').change();
	var numPeriod = '<s:property value="numPeriod"/>';
	
	StockCountingApprove.iniGridCycleCount(yearPeriod, numPeriod);
	$('#dgCycleCountCode, #dgCycleCountDes ').parent().bind('keyup', function(event){
		if(event.keyCode == keyCodes.ENTER){
			StockCountingApprove.searchCycleCount();
		}
	});
	$('#cycleCountCode').focus();
	
	$('#btnSaveCCResult').attr('disabled','disabled');
	
	StockCountingApprove._ccMapProductsAndResults = new Map();
	StockCountingApprove._ccMapProducts = new Map();
	StockCountingApprove.mapNewProducts = new Map();
	StockCountingApprove._mapProductSelectCheck = new Map();
	StockCountingApprove._mapProductSelect = new Map();
	$('.GeneralTable tr').live('click', function() {				
		$('.GeneralTable tr').css('background-color','');
		$(this).css('background-color','#FFFF66');
	});
});
</script>
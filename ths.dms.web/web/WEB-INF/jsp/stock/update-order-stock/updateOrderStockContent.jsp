<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<div class="BreadcrumbSection">
    <ul class="ResetList FixFloat BreadcrumbList">
        <li class="Sprite1"><a href="/stock/update-order-stock/info">Kho</a></li>
        <li><span>Cập nhật kho</span></li>
    </ul>
</div>
<div class="CtnOneColSection">
    <div class="ContentSection">
    	<div class="ToolBarSection">
            <div class="SearchSection GeneralSSection">
            	<h2 class="Title2Style">Thông tin chung</h2>
                <div class="SearchInSection SProduct1Form">
                	<label class="LabelStyle Label2Style">Đơn vị</label>
                    <div class="BoxSelect BoxSelect2">
                        <select id="shopCodeCB" style="width: 206px;">
                        </select>
                    </div>
                    <label class="LabelStyle Label2Style">Loại đơn hàng</label>
                    <div class="BoxSelect BoxSelect2">
                           <select class="MySelectBoxClass" id="orderType">
                                <option value="">Tất cả</option>
								<option value="IN" selected="selected">Đơn bán PreSale (IN)</option>
								<option value="CM">Đơn trả PreSale (CM)</option>
								<option value="SO">Đơn bán Vansale (SO)</option>
								<option value="CO">Đơn trả Vansale (CO)</option>
								<option value="DP">Đơn bán hàng Vansale (DP)</option>
								<option value="GO">Đơn trả hàng Vansale (GO)</option>
<!-- 								<option value="DC">Điều chuyển (DC)</option> -->
<!-- 								<option value="DCT">Điều chỉnh tăng kho (DCT)</option> -->
<!-- 								<option value="DCG">Điều chỉnh giảm kho (DCG)</option> -->
                            </select>
                    </div>
                    <label class="LabelStyle Label2Style">Số đơn hàng</label>
                    <input type="text" id="orderNumber" maxlength="50" class="InputTextStyle InputText1Style" />
                    <div class="Clear"></div>
                    
                    <label class="LabelStyle Label2Style">Tạo trên</label>
                     <div class="BoxSelect BoxSelect2">
                         <select class="MySelectBoxClass" id="orderSource">
                            <option value="0" selected="selected">Tất cả</option>
							<option value="1" >Web</option>
							<option value="2">Tablet</option>
                          </select>
                     </div>                     
                    <label class="LabelStyle Label2Style">Mã KH(F9)</label>
                    <input type="text" id="customerCode" maxlength="50" class="InputTextStyle InputText1Style" />
                    <label class="LabelStyle Label2Style">Tên khách hàng</label>
                    <input type="text" id="customerName" maxlength="50" class="InputTextStyle InputText1Style" />
                    <div class="Clear"></div>
                    
                	<label class="LabelStyle Label2Style">Mã NVBH</label>
                    <div class="BoxSelect BoxSelect2">
                        <select id="saleStaffCode" style="width: 206px;">
                        </select>
                    </div>
                    <label class="LabelStyle Label2Style">Mã NVGH</label>
                    <div class="BoxSelect BoxSelect2">
                        <select id="deliveryStaffCode" style="width: 206px;">
                        </select>
                    </div>
                    <div id = "valueOrderDiv">
	                    <label class="LabelStyle Label2Style">Giá trị ĐH</label>
						<div class="BoxSelect BoxSelect2">
		                    <select class="MySelectBoxClass" id="isValueOrder" >
		                         <option value="-1" selected="selected">Tất cả</option>
		                         <option value="1">Có giá trị >=</option>
		                         <option value="0">Có giá trị < </option>
		                    </select>
	                    </div>                    
                    	<label class="LabelStyle LabelStyle"></label>
     					<input type="text" class="InputTextStyle InputText4Style vinput-money" disabled="disabled" id="numberValueOrder" type="text" maxlength="18"/>
     				</div>
     				<div class="Clear"></div>
                    
                    <div class="BtnCenterSection">
                        <button id="btnSearch" class="BtnGeneralStyle" onclick="return StockUpdateOrder.searchOrder();">Tìm kiếm</button>
                    </div>
                    <div class="Clear"></div>
                    
                    <p id="errMsgInfo" class="ErrorMsgStyle" style="display: none"></p>
                    <input type="hidden" id="shopCodeHidden" value="<s:property value="shop.shopCode"/>"/>
                    <input type="hidden" id="shopId" value="<s:property value="shop.Id"/>"/>
                </div>
                <h2 class="Title2Style">Kết quả tìm kiếm</h2> 
                <div class="SearchInSection SProduct1Form">
                	<div class="GridSection" style="padding:0;">
                		<table id="grid" ></table>
                	</div>                	
                    <div class="Clear"></div>
                    
                    <p id="errExcelMsg" class="ErrorMsgStyle" style="display: none"></p>
                    <p id="errMsg" class="ErrorMsgStyle" style="display: none"></p>
					<p id="successMsg" class="SuccessMsgStyle" style="display: none"></p>
					<p id="successMsgProduct" class="SuccessMsgStyle" style="display: none"></p>
					 <div class="Clear"></div>
					 
					<div class="BtnCenterSection">
						<button id="btnCancel" class="BtnGeneralStyle" onclick="StockUpdateOrder.cancelStockTrans();" style="float:left; display: none;">Hủy</button>
                        <button id="btnSearch" class="BtnGeneralStyle" onclick="return StockUpdateOrder.approveStockOrder();" style="float:right; margin-right:20px;">Xác nhận</button>
                        <div class="Clear"></div>
                    </div>
                </div>
            </div>
            <div class="Clear"></div>
        </div>
    </div>
    <div class="Clear"></div>
    
    <div id ="dialogStockOrder" style="display:none;" >
		<div id="dialogStockOrderEasyUIDialog">
			<div class="SearchInSection SProduct1Form">
				<div class="GridSection">	                 					
					<table id="dgError"></table>
				</div>
				<label class="LabelStyle LabelStyle" id="confirmMsg" style="display:none;width:auto !important;"></label>
				 <div class="BtnCenterSection">
				 	  <button class="BtnGeneralStyle" onclick="" id="btnConfirm" style="display:none;">Đồng ý</button>
	                  <button class="BtnGeneralStyle" onclick="$('#dialogStockOrderEasyUIDialog').dialog('close');">Đóng</button>
	              </div>                             
	     	</div>    
		</div>
	</div>
</div>
<!-- Popup xem chi tiet -->
<div id="billDetailPopupDiv" style="display:none;">
    <div id="billDetailPopup" class="PopupContentMid2">         
    </div>
</div>
<script>
	$(document).ready(function() {
		StockUpdateOrder.initUnitCbx();
		
		$('.MySelectBoxClass').css('width','206');
		$('.CustomStyleSelectBox').css('width','173');
		var lnk = '&lnk=' + window.location.href;
		
		//setDateTimePicker('date');
		//$('.MySelectBoxClass').customStyle();
		$('#customerCode').bind('keyup', function(event) {
			if (event.keyCode == keyCode_F9) {
				VCommonJS.showDialogSearch2({
					dialogInfo: {title: 'Thông tin tìm kiếm Khách hàng NPP: ' + '<span style="color: #199700">' + Utils.XSSEncode($('#shopCodeCB').combobox("getValue")) +'</span>' },
					inputs : [
		          	{id:'code', maxlength:50, label:'Mã KH'},
			        {id:'name', maxlength:250, label:'Tên KH'},
			        {id:'address', maxlength:250, label:'Địa chỉ', width:410},
					      ],
					params : {shopCode: $('#shopCodeCB').combobox("getValue")},
					url : '/commons/customer-in-shop/search',
					columns : [[
						{field:'shortCode', title:'Mã KH', align:'left', width: 50, sortable:false, resizable:false, formatter: function(value, row, index) {
							return VTUtilJS.XSSEncode(row.shortCode);
						}},
						{field:'customerName', title:'Tên khách hàng', align:'left', width: 120, sortable:false, resizable:false, formatter: function(value, row, index) {
							return VTUtilJS.XSSEncode(row.customerName);
						}},
						 {field:'address', title:'Địa chỉ', align:'left', width: 200, sortable:false, resizable:false, formatter: function(value, row, index) {
								return VTUtilJS.XSSEncode(row.address);
						}},
						{field:'choose', title:'', align:'center', width:40, sortable:false, resizable:false, formatter:function(value,row,index) {
							return '<a href="javascript:void(0)" onclick="return StockUpdateOrder.callSelectF9CustomerCode('+index+',\''+Utils.XSSEncode(row.shortCode)+'\',\''+Utils.XSSEncode(row.customerName)+'\');">Chọn</a>'; 
						}}
					]]
				});
				
				
			}
		});
		$('#orderNumber').focus();
		Utils.bindComboboxStaffEasyUI('saleStaffCode');
		Utils.bindComboboxStaffEasyUI('deliveryStaffCode');
		
		//xu ly bind value money dau phay
		Utils.formatCurrencyFor('numberValueOrder');
		$('#isValueOrder').bind('change', function(event){
			var value = $(this).val();
			if($('#numberValueOrder').val().trim() != ''){
				SPAdjustmentTax._textChangeBySeach.avg = $('#numberValueOrder').val().replace(/,/g, '');
			}
			if (Number(value) ==  -1) {
				$('#numberValueOrder').val(SPAdjustmentTax._textChangeBySeach.all);
				disabled('numberValueOrder');
			} else {
				enable('numberValueOrder');
				$('#numberValueOrder').val(formatCurrency(SPAdjustmentTax._textChangeBySeach.avg));
			}
		});
	});
</script>


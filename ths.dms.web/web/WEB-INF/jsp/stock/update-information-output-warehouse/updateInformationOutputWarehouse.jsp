<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<div class="BreadcrumbSection">
       	<ul class="ResetList FixFloat BreadcrumbList">
           <li class="Sprite1"><a href="javascript:void(0);">Kho</a></li>
           <li><span>Cập nhật thông tin phiếu xuất kho kiêm vận chuyển nội bộ</span></li>
    	</ul>
</div>
<div class="CtnOneColSection">
	<div class="ContentSection">
		<div class="ToolBarSection">
			<div class="SearchSection GeneralSSection">
				<h2 class="Title2Style">Thông tin tìm kiếm</h2>
				<div class="SearchInSection SProduct1Form">
					<label class="LabelStyle Label1Style">Trạng thái</label>
					<div class="BoxSelect BoxSelect2" id="dllIsPrintDiv">
						<select class="MySelectBoxClass" id="dllIsPrint">
							<option value="1">Chưa có phiếu xuất</option>
							<option value="2" selected="selected">Đã có phiếu xuất</option>
						</select>                            
					</div>
					<label class="LabelStyle Label1Style">Loại</label>
					<div class="BoxSelect BoxSelect2" id="dllTransTypeDiv">
						<select class="MySelectBoxClass" id="dllTransType">
							<option value="DP">Đơn bán</option>
							<option value="POVNM">Đơn trả hàng</option>
							<option value="DC">Phiếu điều chuyển kho</option>
						</select>                            
					</div>
					<div class="Clear"></div>
					<label class="LabelStyle Label1Style">Số chứng từ</label> 
					<input id="txtStockTransCode" type="text" class="InputTextStyle InputText1Style" />
					<label class="LabelStyle Label1Style">Số phiếu</label> 
					<input id="txtStockIssueNumber" type="text" class="InputTextStyle InputText1Style" />
					<label class="LabelStyle Label1Style">Mã NVBH</label>
					<div class="BoxSelect BoxSelect2" id="dllStaffCodeDiv">
						<select class="MySelectBoxClass" id="dllStaffCode">
							<s:iterator value="lstStaffVO"  var="obj" >
								<option value="<s:property value="#obj.id" />"><s:property value="#obj.staffCode" /> - <s:property value="#obj.staffName" /></option>
							</s:iterator>
							<option id="ojbAllDllStaffCode" value="-2" selected="selected"></option>
						</select>                            
					</div>
					<div class="Clear"></div>
					<label class="LabelStyle Label1Style">Từ ngày</label>
					<input id="txtFromDate" type="text" class="InputTextStyle InputText1Style vinput-date" value="<s:property value="sysDateStr" />"  />
					<label class="LabelStyle Label1Style">Đến ngày</label>
					<input id="txtToDate" type="text" class="InputTextStyle InputText1Style vinput-date" value="<s:property value="sysDateStr" />"  />
					<div class="Clear"></div>
					<div class="BtnCenterSection">
						<button id="btnSearchDg" class="BtnGeneralStyle" onclick="UpdateInformationOutputWarehouse.loadPagePrdStockIssueDg();">Tìm kiếm</button>
					</div>
					<div class="Clear"></div>
				</div>
			</div>
			<div class="Clear"></div>
		</div>
		<div class="GeneralCntSection">
			<div class="Clear"></div>
			<h2 class="Title2Style">Kết quả tìm kiếm</h2>
			<div class="GridSection" id="prdStockOpDgContGrid">
				 <table id="prdStockIssueDg"></table>
			</div>
		</div>
		<div class="SearchSection GeneralSSection">
				<div class="SearchInSection SProduct1Form">
					<p id="errMsg" style="display: none;" class="ErrorMsgStyle"></p>
					<p id="successMsg" class="SuccessMsgStyle" style="display:none;"></p>
					<div class="Clear"></div>
					<div class="BtnCenterSection">
						<button id="btnUpdateDg" class="BtnGeneralStyle" onclick="">Lưu</button>
					</div>
				</div>
			</div>
			<div class="Clear"></div>
		</div>
</div>

<!-- Dialog Form -->
<div class="ContentSection" style="display: none;">
	<tiles:insertTemplate template="/WEB-INF/jsp/stock/internal-warehouse/dialogSearchProductChangeQuantityManager.jsp" />
</div>

<script type="text/javascript">
	$(document).ready(function() {
		//UpdateInformationOutputWarehouse._isIndexPrint = Number('<s:property value="isIndexPrint" />');
		UpdateInformationOutputWarehouse._isIndexPrint = 10;
		disableDateTimePicker('txtStockTransDate');
 		$('#txtFromDate, #txtToDate').width($('#txtFromDate').width() - 22);
 		$('#dllStaffCodeDiv, #dllIsPrintDiv').bind('keyup',function(event){
 			if(event.keyCode == keyCodes.ENTER){
 				$('#btnSearchDg').click();
 			}
 		});
 		UpdateInformationOutputWarehouse.loadPagePrdStockIssueDg();
	});

</script>

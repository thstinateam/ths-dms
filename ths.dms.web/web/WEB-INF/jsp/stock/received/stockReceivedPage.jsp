<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<div class="BreadcrumbSection">
	<ul class="ResetList FixFloat BreadcrumbList">
		<li class="Sprite1"><a href="javascript:void(0)">Bán hàng</a></li>
		<li><span>Lập đơn trả hàng vansale(GO)</span></li>
	</ul>
</div>
<div class="CtnOneColSection">
	<div class="ContentSection">
		<div class="ToolBarSection">
			<div class="SearchSection GeneralSSection">
				<h2 class="Title2Style">Thông tin tìm kiếm</h2>
				<div class="SearchInSection SProduct1Form">
					<label class="LabelStyle Label1Style">Nhân viên<span class="ReqiureStyle">*</span></label> 
<!-- 					<input type="text" class="InputTextStyle InputText1Style" id="staffCode" maxlength="22" onChange="myFun()"/> -->
					 <div class="BoxSelect BoxSelect2" id="parentDivStaffCode">
						<select class="easyui-combobox" id="staffCode" name="staffCode">
							<option value=""></option>   							
						</select>
					</div>
					<label class="LabelStyle Label1Style">Tên nhân viên</label> 
					<input type="text" class="InputTextStyle InputText1Style" id="staffName" disabled="disabled" maxlength="250" autocomplete="off"/>
					<label class="LabelStyle Label1Style">Ngày nhập</label> 
					<input type="text" class="InputTextStyle InputText1Style" disabled="disabled" id="inputDate" value="<s:property value="inputDate"/>" autocomplete="off"/>					
					<div class="Clear"></div>
					<label class="LabelStyle Label1Style">Nhân viên nhập</label>
					<input id="userName" class="InputTextStyle InputText1Style" disabled="disabled" value="<s:property value="staff.staffCode"/>" maxlength="100" autocomplete="off"/>				
					<div class="Clear"></div>
					
					<div class="BtnCenterSection">
                       <button class="BtnGeneralStyle" id="btnSearch" onclick="return StockReceived.getInfo();">Lấy thông tin</button>
                    </div>	
                    <div class="Clear"></div>
                    <img id="loading1" style="visibility: hidden;" src="/resources/images/loading.gif" class="LoadingStyle" />
                    <p class="ErrorMsgStyle" id="errMsg" style="display: none;"></p>				
				</div>
				<h2 class="Title2Style">Thông tin sản phẩm nhập</h2>
				<div class="SearchInSection SProduct1Form">						 				
					<div class="GeneralTable General1Table">
						<s:if test="separationLot==0">
	          			<div id="btnAddProduct" style="text-align: right; padding-bottom: 10px;" >
			              <button class="BtnGeneralStyle Sprite2" id="issAddProduct" onclick="StockReceived.openSelectProductDialog();" >
								<span class="Sprite2">Thêm hàng</span>
						  </button>                           		
	                 	</div>
          				</s:if>  
          				<div class="Clear"></div>
          				<div id="productGrid">
          					<tiles:insertTemplate template="/WEB-INF/jsp/stock/received/stockReceivedInfo.jsp"></tiles:insertTemplate>
          				</div>          				
					</div>					
				</div>
				<div class="Clear"></div>
			</div>
			<div class="Clear"></div>
		</div>
	</div>
	<div class="Clear"></div>
</div>
<div id ="productDialog" style="width:600px;visibility: hidden;" >
	<div id="productEasyUIDialog" class="easyui-dialog" title="Thông tin sản phẩm" data-options="closed:true,modal:true">
		<div class="PopupContentMid">
			<div class="GeneralForm Search1Form" >
				<label class="LabelStyle Label1Style" style=" width: 100px;">Mã sản phẩm</label>
				<input id="productCode" maxlength="50" tabindex="1" type="text" style=" width: 145px;" class="InputTextStyle InputText1Style" /> 
				<label class="LabelStyle Label1Style" style=" width: 100px;">Tên sản phẩm</label> 
				<input id="productName" type="text" maxlength="250" tabindex="2" style="width: 145px;"  class="InputTextStyle InputText1Style" />
				<div class="Clear"></div> 
				<div class="BtnCenterSection">
				<button id="btnSearch" onclick="return StockReceived.searchProduct();" class="BtnGeneralStyle BtnSearchOnDialog" tabindex="3" id="issSearchProductDialog">Tìm kiếm</button>
				</div>
				<div class="Clear"></div>
				<div class="GridSection" id="productGridContainer">					
					<table id="grid" class="easyui-datagrid"></table>
					<div id="pager"></div>
				</div>
				<div class="BtnCenterSection">
					<button onclick="return StockReceived.selectListProductsOnDialog();" class="BtnGeneralStyle">Chọn</button>
					<button class="BtnGeneralStyle BtnGeneralMStyle" onclick="$('.easyui-dialog').dialog('close');">Đóng</button>
				</div>
				<div class="Clear"></div> 
				 <p id="errMsg" class="ErrorMsgStyle" style="display: none;"></p>
				 <p class="SuccessMsgStyle" style="display: none"></p> 
			</div>
		</div>
	</div>
</div>
<input type="hidden" id="shopCode" value="<s:property value="shop.shopCode"/>" />
<s:iterator value="lstStaff" var="obj">
<input type="hidden" class="lststaff" staffCode = "<s:property value="staffCode" />" staffName = "<s:property value="staffName" />" />
</s:iterator>
<script type="text/javascript">
$(document).ready(function(){
	$('#parentDivStaffCode, #staffCode, #btnSearch').bind('keyup',function(event){
		if(event.keyCode == keyCodes.ENTER){
			$('#btnSearch').click(); 
		}
	});
	var lststaff = $('.lststaff');
	for(var i=0;i<lststaff.length;++i){		
		 var staff = new Object();
		 staff.staffCode = $(lststaff[i]).attr('staffCode').trim();
		 staff.staffName = $(lststaff[i]).attr('staffName').trim();
		 StaffUtils.listStaffSale.push(staff);	
	}
	$('#staffCode').combobox({valueField : 'staffCode',textField : 'staffCode',panelWidth: 206,data : StaffUtils.listStaffSale});
	$('#staffCode').combobox({
		mode:'local',
		formatter: function(row) {
			return '<span style="font-weight:bold">' + Utils.XSSEncode(row.staffCode) + '</span><br/>' + '<span style="color:#888">' + Utils.XSSEncode(row.staffName) + '</span>';
		},
		filter: function(q, row){
			q = new String(q).toUpperCase();
			var opts = $(this).combobox('options');
			return row[opts.valueField].indexOf(q)>=0;
		},
		onSelect: function(rec){
			$('#staffName').val(rec.staffName);
			//loctt - Oct17, 2013-begin
			$('#issAddProduct').removeAttr('disabled');
			$('#issAddProduct').removeClass('BtnGeneralDStyle');
			$('#productGridBody').html("<tr><td colspan='8' style='border-left: 1px solid #C6D5DB;'><p class='NotData' style='padding:5px;font-weight: normal;'>Không có dữ liệu nào</p></td></tr>");
			$('#btstock').show();
			//$('#stockTransCode').removeAttr('disabled');
			$('.Func1Section').width('636px');
			StockReceived._amtMap = new Map();
			disabled('btnExportExcel');
			$('#successMsg1').hide();
			$('#totalWeight').html(formatFloatValue(0,2));
			$('#totalAmount').html(formatCurrency(0));
			$.ajax({
				type : "POST",
				url : "/stock/received/getTransCode",
				dataType: "json",
				success : function(result) {
					if(result != null)
						$('#stockTransCode').val(result.stockTransCode);
				}
			});	
			//loctt - Oct17, 2013-end			

		},
	    onChange:function(){
			//$('.combo-panel').each(function(){if(!$(this).is(':hidden')){$(this).parent().css('width','250px');$(this).css('width','248px');}});
		}
	});
	StockReceived._amtMap = new Map();	
	$('#staffCode').focus();
	if(sys_separation_auto!=0){
		disabled('issAddProduct');
	}
	//$('.combo-arrow').live('click',function(){$('.combo-panel').each(function(){if(!$(this).is(':hidden')){$(this).parent().css('width','250px');$(this).css('width','248px');}});});
});
</script>
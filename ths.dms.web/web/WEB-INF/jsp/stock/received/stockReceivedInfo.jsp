<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@taglib prefix="k" uri="/kryptone"%>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
	<colgroup>
		<col style="width: 47px;" />
		<col style="width: 140px;" />
		<col style="width: 305px;">
		<col style="width: 73px;">
		<col style="width: 99px;" />
		<col style="width: 110px;" />
		<s:if test="separationLot==0">
			<col style="width: 100px;" />
			<col style="width: 50px;" />
		</s:if>
		<s:else>
			<col style="width: 179px;" />
		</s:else>
	</colgroup>
	<thead>
		<tr>
			<th class="ColsThFirst">STT</th>
			<th>Mã sản phẩm</th>
			<th>Tên sản phẩm</th>
			<th>Số lô</th>
			<th>Số lượng</th>
			<th>Giá bán</th>
			<s:if test="separationLot==0">
				<th>Thành tiền</th>
				<th class="ColsThEnd">Xóa</th>
			</s:if>
			<s:else>
				<th>Thành tiền</th>
			</s:else>
		</tr>
	</thead>
	<tbody id="productGridBody">
					<s:if test="lstProduct != null && lstProduct.size() > 0">
		               	<k:repeater value="lstProduct" status="status">
		               		<k:itemTemplate>
		               		<tr id="ROW_<s:property value="#status.index+1"/>" >	
		                         <td class="AlignCCols RowNumber" style="border-left: 1px solid #C6D5DB;">
		                         	<s:property value="#status.index+1"/>
		                         	<input type="hidden" id="ProductCode<s:property value="#status.index+1"/>" value="<s:property value="productCode"/>" />
		                         	<input type="hidden" id="Convafact<s:property value="#status.index+1"/>" value="<s:property value="convfact"/>" />
		                         </td>
		                         <td class="AlignLCols"><s:property value="productCode"/></td>
		                         <td class="AlignLCols Wordwrap"><s:property value="productName"/></td>
		                         <s:if test="separationLot==0">
		                         	<s:if test="lot!=null || lot.length > 1">
			                         	<td class="AlignRCols" style="padding: 4px 5px;" >
			                         		<input type="hidden" id="CheckLot<s:property value="#status.index+1"/>" value="1" />
			                         		<input maxlength="6" id="Lot<s:property value="#status.index+1"/>" onkeypress="return NextAndPrevTextField(event,this,'Lot')"
			                         		index-row="<s:property value="#status.index+1"/>"
			                         		class="AlignRight Lot <s:property value="productCode"/>" type="text" size="8" value="<s:property value="lot" />" ></td>
		                         	</s:if>
			                         <s:else>
			                         	<td class="AlignRCols" style="padding: 4px 5px;">
			                         		<input type="hidden" id="CheckLot<s:property value="#status.index+1"/>" value="0" /></td>
			                         </s:else>
			                         <s:if test="convfact != null && convfact!= undefined && convfact != 0">
			                         	<td class="AlignRCols" style="padding: 4px 5px;">
			                         		<input onblur="return StockReceived.qtyChanged(this);" onchange="return StockReceived.qtyChanged(this);" price="<s:property value="price"/>" gross-weight="<s:property value="grossWeight"/>" convfact="<s:property value="convfact"/>" id="Quantity<s:property value="#status.index+1"/>" 
			                         		index-row="<s:property value="#status.index+1"/>"		                         		
			                         		value="<s:property value="convertConvfact(quantity,convfact)"/>" maxlength="9"
			                         		onkeypress="return NextAndPrevTextField(event,this,'Quantity')"
			                         		class="AlignRight Quantity" type="text" size="8">
			                         	</td>
			                         </s:if>
		                         </s:if>
		                         <s:else>
		                         	<td class="ColsTd4 AlignRight"><s:property value="lot" /></td>
		                         	<td class="ColsTd5"><s:property value="convertConvfact(quantity,convfact)"/></td>
		                         </s:else>		                         		                         
		                         <td class="ColsTd6 AlignRight CurrencyNumber "><s:property value="price"/></td>
		                         <td class="ColsTd7 AlignRight CurrencyNumber" style="padding-right: 20px !important;"><s:property value="quantity*price"/></td>
		                         <s:if test="separationLot==0">
		                          	 <td class="ColsTd8 ColsTdEnd AlignCenter">
		                          		<a class="DelProduct" onclick="return StockReceived.deleteProduct('<s:property value="#status.index+1"/>');"  
		                          			href="javascript:void(0)">
		                          		<img width="19" height="20" src="/resources/images/icon-delete.png"></a>
		                         	</td>
		                         </s:if>
		                     </tr>
		               		</k:itemTemplate>
		               	</k:repeater>
				    </s:if>
				    <s:else>
				    	<tr>
				    		<td colspan="8" style="border-left: 1px solid #C6D5DB;">
				    			<p class="NotData" style="padding:5px;font-weight: normal;">Không có dữ liệu nào</p></td>
				    	</tr>						
					</s:else>
                </tbody>
            <tfoot>
	           <tr>
	               <td class="ColsTd1">&nbsp;</td>
	               <td class="ColsTd2">&nbsp;</td>	              
	               <td class="AlignLCols" colspan="2" style="padding: 4px 9px;">Trọng lượng</td>
                   <td class="ColsTd5 AlignRight" style="font-weight: bold  !important;"> 
                   		<s:if test="totalWeight != null"><span id="totalWeight"><s:property value="totalWeight"/></span> kg
                   		</s:if>                   		
                   </td>
	               <td class="AlignLCols">Tổng tiền</td>
	               <td  colspan="2" class="AlignRCols ColsTdEnd" style="padding: 4px 5px;">
	               		<s:if test="totalAmount != null">
	               			<span id="totalAmount" ><s:property value="totalAmount"/></span> VNĐ
                   		</s:if>                   		
	               </td>	               
	           	</tr>
			</tfoot>
</table>
<div class="GeneralForm GeneralNoTP1Form" style="padding-top: 10px;">
	<div class="Func1Section" style="width: 450px;">
		<label class="LabelStyle Label1Style" style="padding-top: 8px;width: 93px;">Mã phiếu <span class="ReqiureStyle">*</span></label>		
		<input type="text" class="InputTextStyle InputText1Style" disabled="disabled" style="width: 56%;" id="stockTransCode" maxlength="20" value="<s:property value="stockTransCode"/>"/>
		<s:if test="shopLocked">
			<button id="btstock" class="BtnGeneralStyle BtnGeneralDStyle" disabled="disabled">Lưu</button>	
		</s:if>
		<s:else>
			<button id="btstock" class="BtnGeneralStyle" onclick="return StockReceived.import();">Lưu</button>
		</s:else>
		<!-- <button class="BtnGeneralStyle" id="btnExportExcel"	>In phiếu nhập kho</button> -->	
	</div>	
</div>
<div class="FuncBtmSection">
	<div class="Clear"></div>
	<img id="loading" src="/resources/images/loading.gif" class="LoadingStyle" style="visibility:hidden;"/>
	<s:if test="errMsg!=null">
		<p class="ErrorMsgStyle" id="errMsg1"> <s:property value="errMsg" /> </p>
	</s:if>		
	<s:else>
		<p class="ErrorMsgStyle" id="errMsg1" style="display: none;"> </p>
	</s:else>
	<p id="successMsg1" class="SuccessMsgStyle" style="display: none;">Cập nhật dữ liệu thành công</p>
</div>
<script type="text/javascript">
$(document).ready(function(){	
	disabled('btnExportExcel');
	Utils.bindFormatOnTextfieldInputCss('Quantity', Utils._TF_NUMBER_CONVFACT);
	Utils.bindFormatOnTextfieldInputCss('Lot', Utils._TF_NUMBER);
	$('.CurrencyNumber').each(function () {
		if($(this).html().length > 0){
			$(this).html(formatCurrency($(this).html()));
		}		
	});	
	var totalWeight = '<s:property value="totalWeight"/>';
	
	$('#totalWeight').html(formatFloatValue(totalWeight,2));
	var totalAmount = '<s:property value="totalAmount"/>';
	$('#totalAmount').html(formatCurrency(totalAmount));
});
</script>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<div class="BreadcrumbSection">
	<ul class="ResetList FixFloat BreadcrumbList">
		<li class="Sprite1"><a href="javascript:void(0)">Bán hàng</a></li>
		<li><span>Lập đơn bán hàng vansale(DB)</span></li>
	</ul>
</div>
<div class="CtnOneColSection">
	<div class="ContentSection">
		<div class="ToolBarSection">
			<div class="SearchSection GeneralSSection">
				<h2 class="Title2Style">Thông tin chung</h2>
				<div class="SearchInSection SProduct1Form">
					<label class="LabelStyle Label2Style">Mã phiếu <span class="ReqiureStyle">*</span></label> 
					<input type="text" class="InputTextStyle InputText1Style" id="code" disabled="disabled" maxlength="20" value="<s:property value ="code" />" autocomplete="off"/> 
					
					<label	class="LabelStyle Label1Style">Ngày xuất <span class="ReqiureStyle">*</span></label> 
					<s:if test="issuedDate!=null">
						<input type="text" class="InputTextStyle InputText1Style" disabled="disabled" id="issuedDate" maxlength="10" value="<s:property value='issuedDate' />" autocomplete="off"/>
					</s:if>
					<s:else>
						<input type="text" class="InputTextStyle InputText6Style" id="issuedDate" maxlength="10" />
					</s:else>
					
					
					<%-- <label class="LabelStyle Label1Style">Đơn vị</label> 
					<input  type="text" class="InputTextStyle InputText6Style"  id="shopCode" value="<s:property value ="shopCode" /> - <s:property value ="shopName" />" readonly="readonly" autocomplete="off"/>  --%>
					
					<div class="Clear"></div>
					<label class="LabelStyle Label2Style">Mã nhân viên <span class="ReqiureStyle">*</span></label>
<!-- 					<input type="text" class="InputTextStyle InputText1Style" id="staffCode" maxlength="12" /> -->
					<div id="stCode" class="BoxSelect BoxSelect2">
						<select class="easyui-combobox" id="staffCode" name="staffCode" autocomplete="off">
							<option value=""></option>
   							<s:iterator value="lstStaff" var="obj">
   								<option value="<s:property value="staffCode" />"><s:property value="staffName" /></option>		                			
	                		</s:iterator>
						</select>
					</div>
					<label class="LabelStyle Label1Style">Xe <span class="ReqiureStyle">*</span></label>
					<div class="BoxSelect BoxSelect2">
						<s:select id="car" list="lstCar"  headerKey=""	headerValue=" Chọn xe " listKey="id" listValue="carNumber"	cssClass="MySelectBoxClass" autocomplete="off"></s:select>
					</div>					
					<label class="LabelStyle Label1Style">Nhân viên xuất</label> 
					<input	type="text" class="InputTextStyle InputText6Style" disabled="disabled" id="issuedStaff" value="<s:property value ="staff.staffCode" />" autocomplete="off"/>
					<div class="Clear"></div>
				</div>
				<h2 class="Title2Style">Thông tin sản phẩm xuất kho</h2>
				<div class="SearchInSection SProduct1Form">
					<div class="GridSection">	                 					
						<table id="dg"></table>
						<div class="Clear"></div>
						<div class="BtnRSection" style="padding-top: 10px;">					
							<button class="BtnGeneralStyle" id="btnExportExcelNew" onclick="return StockIssued.openStockOutVanSalePrintDialog();">In phiếu xuất kho kiêm vận chuyển nội bộ</button>
							&nbsp;&nbsp;
							<button class="BtnGeneralStyle" id="btnStockOut" onclick="return StockIssued.stockOutVanSale();">Lưu</button>
						</div>				
					</div>					
					<div class="Clear"></div>
					<img id="loading" src="/resources/images/loading.gif" class="LoadingStyle" style="visibility: hidden;" />
					<tiles:insertTemplate template="/WEB-INF/jsp/general/successMsg.jsp" />
					<p id="errMsg" class="ErrorMsgStyle" style="display: none"><s:property value="errMsg" /></p>
				</div>
			</div>
			<div class="Clear"></div>
		</div>
	</div>
	<div class="Clear"></div>
</div>
<div id ="productDialog" style="width:600px;visibility: hidden;" >
	<div id="productEasyUIDialog" class="easyui-dialog" title="Thông tin sản phẩm" data-options="closed:true,modal:true">
		<div class="PopupContentMid">
			<div class="GeneralForm Search1Form" >
				<label class="LabelStyle Label1Style" style=" width: 100px;">Mã sản phẩm</label>
				<input id="productCode" maxlength="50" tabindex="1" type="text" style=" width: 145px;" class="InputTextStyle InputText1Style" /> 
				<label class="LabelStyle Label1Style" style=" width: 100px;">Tên sản phẩm</label> 
				<input id="productName" type="text" maxlength="250" tabindex="2" style="width: 145px;"  class="InputTextStyle InputText1Style" />
				<div class="Clear"></div> 
				<button  class="BtnGeneralStyle BtnSearchStyle BtnSearchOnDialog" tabindex="3" id="issSearchProductDialog">Tìm kiếm</button>
				
				<div class="Clear"></div>
				<div class="GridSection" id="productGridContainer">					
					<table id="productGrid" class="easyui-datagrid"></table>
					<div id="productPager"></div>
				</div>
				<div class="BtnCenterSection">
					<button onclick="StockIssued.selectListProducts();" class="BtnGeneralStyle">Chọn</button>
					<button class="BtnGeneralStyle BtnGeneralMStyle" onclick="$('.easyui-dialog').dialog('close');">Đóng</button>
				</div>
				 <p id="errMsg" class="ErrorMsgStyle" style="display: none;"></p>
			</div>
		</div>
	</div>
</div>
<div id ="productLotDialog" style="width:600px;visibility: hidden;" >
	<div id="productLotEasyUIDialog" class="easyui-dialog" title="Chọn lô" data-options="closed:true,modal:true">
		<div class="PopupContentMid">
			<div class="GeneralForm Search1Form" >
				<label class="LabelStyle Label1Style" style=" width: 100px;">Mã sản phẩm</label>
				<p id="fancyProductCode" class="LabelStyle Label1Style" style="color: #000000;font-weight: bold;"></p> 
				<label class="LabelStyle Label1Style" style=" width: 100px;">Tên sản phẩm</label> 
				<p id="fancyProductName" class="LabelStyle Label1Style" style="color: #000000;font-weight: bold;text-align: left;width: 230px;"></p>                                    
				<div class="Clear"></div>
				
				<label class="LabelStyle Label1Style" style=" width: 100px;">Số lượng</label>
				<p  id="fancyProductTotalQuantity" class="LabelStyle Label1Style" style="color: #000000;font-weight: bold;"></p>			
				
				<div class="Clear"></div>				
				<div class="GridSection" id="productLotGridContainer">					
					<table id="productLotGrid" class="easyui-datagrid"></table>					
				</div>		
				<div class="Clear"></div>
				<label class="LabelStyle Label1Style" style=" width: 100px;">Tổng tồn kho</label>
				<p id="totalQuantity" class="LabelStyle Label1Style" style="color: #000000;font-weight: bold;"></p> 
				<label class="LabelStyle Label1Style" style=" width: 100px;">Tổng số lượng</label> 
				<p id="totalAvailableQuantity" class="LabelStyle Label1Style" style="color: #000000;font-weight: bold;text-align: left;width: 230px;"></p>                                    
				<div class="Clear"></div>		 
				<div class="BtnCenterSection">
					<button id="btnSelectLot" onclick="return StockIssued.acceptSeparation();" class="BtnGeneralStyle">Chấp nhận</button>
					<button class="BtnGeneralStyle BtnGeneralMStyle" onclick="return StockIssued.closeDiglogLOT();">Đóng</button>
				</div>				                                    
				<div class="Clear"></div>
				 <p id="fancyboxError" style="display: none;" class="ErrorMsgStyle"></p>
			</div>
		</div>
	</div>
</div>
<s:hidden id="shopId" name="shopId"></s:hidden>
<s:hidden id="stockOutVanSale" value=""></s:hidden>
<s:hidden id="staffId" name="staffId"></s:hidden>
<s:hidden id="isOutputStock" name= "isOutputStock"></s:hidden>
<div id="stockOutVanSalePrintDiv" class="easyui-dialog" title="Phiếu xuất kho kiêm vận chuyển nội bộ" data-options="closed:true, modal:true"
		style="width: 650px; height: auto;">
	<div class="PopupContentMid2">
		<div class="GeneralForm Search1Form">
			<div class="Clear"></div>
			<label class="LabelStyle Label2Style" style="width: 125px;">Tên lệnh điều động</label>
			<input id="ipNameTransDialog" type="text" class="InputTextStyle InputText5Style" maxlength="40" />
			<label class="LabelStyle Label2Style" style="width: 125px;">Ngày lệnh điều động</label>
			<input id="fDateDialog" type="text" class="InputTextStyle InputText5Style" style="width: 140px;" />
			<div class="Clear"></div>
			<label class="LabelStyle Label2Style" style="width: 125px;">Nội dung</label>
			<textarea rows="4" class="InputTextStyle InputText2Style" cols="50" id="ipContentTransDialog" style="font-size: 12px; height: 150px; width: 474px;">
			</textarea>
			<div class="Clear"></div>
		</div>
		<div class="GeneralForm Search1Form">
			<div class="BtnCenterSection">
				<button class="BtnGeneralStyle Sprite2 BtnSearchOnDialog" id="btnExportExcel">
					<span class="Sprite2">In phiếu</span>
				</button>
			</div>
			<div class="Clear"></div>
			<p id="errMsg2" style="display: none;" class="ErrorMsgStyle"></p>
			<div class="Clear"></div>
		</div>
	</div>
</div>

<script type="text/javascript">
var editIndex = undefined;  
$(document).ready(function(){
	setDateTimePicker('fDateDialog');
	$('#fDateDialog').val($('#issuedDate').val());
	StockIssued.synchronization(true);	
	StockIssued.setEventInput();
	var isOutputStock = $('#isOutputStock').val();
	if(isOutputStock!=undefined && isOutputStock!=null && isOutputStock.length>0 && parseInt(isOutputStock)!=0){
		$('stockOutVanSalePrintDiv').hide();
		$.messager.alert('Cảnh báo','Không thể thực hiện xuất kho nhân viên do ngày chốt khác ngày hiện tại','info');
		setTimeout(function(){disableCombo('staffCode');}, 3000);
		disableSelectbox('car');
		disabled('btnStockOut');
		$('#errMsg').html('Không thể thực hiện xuất kho nhân viên do ngày chốt khác ngày hiện tại').show();
	}
});
</script>
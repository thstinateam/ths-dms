<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@taglib prefix="k" uri="/kryptone"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<s:if test="cycleCount != null">
<h2 class="Title2Style">Thông tin kiểm kê</h2>
<div class="SearchInSection SProduct1Form">
	<label class="LabelStyle Label6Style">Ghi ngày</label> 
	<input type="text" class="InputTextStyle InputText1Style" disabled="disabled" id="createDate" value="<s:date name="cycleCount.createDate" format="dd/MM/yyyy"/>"/>
	 
	<label class="LabelStyle Label6Style">Trạng	thái</label>
	<s:if test="cycleCount.status.value == 0">
		<input type="text" class="InputTextStyle InputText1Style" value="Đang thực hiện" disabled="disabled" />
	</s:if>
	<input type="hidden" id="status" value="cycleCount.status.value" />
	<s:elseif test="cycleCount.status.value == 1">
		<input type="text" class="InputTextStyle InputText1Style" value="Đã duyệt" disabled="disabled" />
	</s:elseif>
	<s:elseif test="cycleCount.status.value == 2">
		<input type="text" class="InputTextStyle InputText1Style" value="Hủy bỏ" disabled="disabled" />
	</s:elseif>
	<s:elseif test="cycleCount.status.value == 3">
		<input type="text" class="InputTextStyle InputText1Style" value="Từ chối" disabled="disabled" />
	</s:elseif>	
	<s:elseif test="cycleCount.status.value == 4">
		<input type="text" class="InputTextStyle InputText1Style" value="Chờ duyệt" disabled="disabled" />
	</s:elseif>	
	<s:if test="cycleCount.status.value == 0 || ycleCount.status.value == 3">
		<label class="LabelStyle Label6Style" style="width:40px;"></label>
		<button class="BtnGeneralStyle" id="groupEventEditDbInsertProduct">Thêm sản phẩm</button>					
	</s:if>
	<div class="Clear"></div>				 
<!-- 					<label class="LabelStyle Label6Style">Dãy số thẻ đầu tiên</label>  -->
<%-- 					<input type="text"	class="InputTextStyle InputText1Style" id="firstNumber" type="text" disabled="disabled" value="<s:property value="cycleCount.firstNumber"/>" />					 --%>
<!-- 					<label class="LabelStyle Label6Style">Số thẻ mới nhất</label>  -->
<%-- 					<input	type="text" class="InputTextStyle InputText1Style" id="lastNumber"	disabled="disabled" value="<s:property value="maxStockCardNumber"/>"/>  --%>
<!-- 					<div class="Clear"></div>	 -->
<!-- 					<label class="LabelStyle Label6Style">Tổng số thẻ</label>  -->
<%-- 					<input id="totalNumber"  maxlength="8"  type="text" class="InputTextStyle InputText1Style" disabled="disabled" value="<s:property value="totalStockCardNumber"/>"/> --%>
<!-- 					<div class="Clear"></div> -->
</div>

<h2 class="Title2Style">Thông tin thẻ kiểm kho</h2>
<div class="SearchInSection SProduct1Form">
	<!--Đặt Grid tại đây-->
	<div class="GeneralTable General1Table">
		<table width="100%" border="0" cellspacing="0" cellpadding="0">
			<colgroup>
				<col style="width: 55px;">
				<col style="width: 90px;">
				<col style="width: 275px;">
				<col style="width: 50px;">
				<col style="width: 90px;">
				<col style="width: 90px;">
				<col style="width: 120px;">
<!-- 								<col style="width: 90px;"> -->
				<col style="width: 90px;">
				<col style="width: 60px;">
			</colgroup>
			<thead>
				<tr>
<!-- 									<th class="ColsThFirst">Số thẻ</th> -->
					<th class="ColsThFirst">Số thứ tự</th>
					<th>Mã sản phẩm</th>
					<th>Tên sản phẩm</th>
					<th>ĐVT</th>
					<th>Ngày kiểm kê cuối</th>
					<th>Số lượng chốt</th>
					<th>Số lượng thực</th>
<!-- 									<th>Nhập chi tiết</th> -->
					<th>Chênh lệch</th>
					<th class="ColsThEnd">Đã nhập</th>
				</tr>
			</thead>
			<tbody id="lstProducts">
				<tiles:insertTemplate template="/WEB-INF/jsp/stock/counting-input/stockInputDetail.jsp"></tiles:insertTemplate>
			</tbody>
		</table>
	</div>
	
	<div class="GeneralForm GeneralNoTP1Form" id="divGroupImport" style="display: none;">
		<div class="Func1Section" style="width: 745px;">
			<s:if test="cycleCount.status.value == 0 || cycleCount.status.value == 3">
				<div id="groupEventEditDb" class="cmsiscontrol">
					<p class="DownloadSFileStyle DownloadSFile2Style">
						<a href="javascript:void(0)" class="Sprite1" id="downloadTemplate">Tải file excel mẫu</a>
					</p>
					<form action="/stock/input/importexcel" name="importFrm" id="importFrm"  method="post" enctype="multipart/form-data">
						<input type="file" class="InputFileStyle InputText1Style" size="20" name="excelFile" id="excelFile" onchange="previewImportExcelFile(this,'importFrm');" style="width:204px !important;">
						<input type="hidden" id="isView" name="isView">	
						<s:hidden id="cycleCountCodeImp" name="cycleCountCodeImp"></s:hidden>
						<s:hidden id="shopCodeImp" name="shopCodeImp"></s:hidden>
						<div class="FakeInputFile">
			                <input type="text" id="fakefilepc" class="InputTextStyle InputText1Style" />
			            </div>									
					</form>
					<button class="BtnGeneralStyle" onclick="StockCountingInput.importExcel();">Nhập từ Excel</button>	
					<button class="BtnGeneralStyle" style="float: right;" onclick="StockCountingInput.completeCheckStock();">Hoàn thành kiểm kho</button>
					<button class="BtnGeneralStyle" style="float: right;" onclick="StockCountingInput.importData();">Lưu</button>					
				</div>
				<button class="BtnGeneralStyle" style="float: right;" onclick="StockCountingInput.exportExcel();">In Pdf</button>							
			</s:if>
			<s:elseif test="cycleCount.status.value == 1 || cycleCount.status.value == 4">
				<button class="BtnGeneralStyle" style="float: right;" onclick="StockCountingInput.exportExcel();">In Pdf</button>		
			</s:elseif>
		</div>						
	</div>
</div>
<div class="Clear"></div>
</s:if>
<s:hidden id="shopId" name="shopId"></s:hidden>
<s:hidden id="cycleCountType" name="cycleCount.status.value"></s:hidden>
<input type="hidden" id="current_fancy_convfact" />
<div id="responseDiv" style="display: none"></div>
<script type="text/javascript">
$(document).ready(function(){
	Utils.bindTRbackground();
	$('#downloadTemplate').attr('href',excel_template_path + 'stock/Bieu_mau_kho_nhap_kiem_ke.xls');
	Utils.functionAccessFillControl('divGroupImport', function () {
		$('#divGroupImport').show();
	});
	$('#groupEventEditDbInsertProduct').click(function() {
		var arrProductIdExcep = [];
		$('.isDeleteRowTmp').each(function() {
			arrProductIdExcep.push(Number($(this).prop('id').replace(/row_del_tmp_/g, '')));
		});
		
		VCommonJS.showDialogSearch2WithCheckbox({
			params : {
				shopCode : $('#cbxShop').combobox('getValue'),
				cycleCountCode : $('#cycleCountCode').val().trim(),
				productObject: arrProductIdExcep.toString()
			},
			cssHident: ['ErrorMsgStyle', 'SuccessMsgStyle'],
			chooseCallback : function(listObj) {
				$('.ErrorMsgStyle').text('').hide();
				if (listObj == undefined || listObj == null || listObj.length == 0) {
					$('#errMsgInfo').text('Chưa có sản phẩm nào được chọn').show();
					return;
				}
				//alert(listObj.length);
				var arrProductId = [];
		        for (var i = 0, size = listObj.length; i < size; i++) {
		        	arrProductId.push(listObj[i].productId);
		        }
				if (arrProductId.length > 0) {
			        var param = new Object();
					param.cycleCountCode = StockCountingInput._shopCode;
					param.shopCode = StockCountingInput._cycleCountCode;
					param.productObject = arrProductId.toString();
					Utils.getHtmlDataByAjax(param, "/stock/input/getInfoJoinProductInsert", function(data) {
						$('#common-dialog-search-2-textbox').dialog('close');
						try {
							var _data = JSON.parse(data);
							if (_data.error != null && _data.errMsg != null && _data.errMsg != undefined) {
								$('#errMsgSearch').html(_data.errMsg).show();
								$("#lstProducts").html('').hide();
								StockCountingInput.clearMap();
								StockCountingInput._myMap = new Map();
							}
						} catch(e) {
							StockCountingInput.clearMap();
							$("#lstProducts").append(data).show();			
							StockCountingInput._myMap = new Map();
							$('.ProductNotLot').each(function(){
								var textBoxId = $(this).attr('id');
								Utils.bindFormatOnTextfield(textBoxId, Utils._TF_NUMBER_CONVFACT);
							});
						}			
					}, 'loading', 'POST');
				}
				
		    },
		    onShow : function() {
		    	//console.log('OK');
		    },
			inputs : [
		        {id: 'code', maxlength: 50, label: 'Mã Sản Phẩm'},
		        {id: 'name', maxlength: 250, label: 'Tên Sản Phẩm'},
		    ],
		    url : '/stock/input/search-product-insert',
		    pKeyMap: 'productId',
		    columns : [[
		        {field: 'productCode', title: 'Mã Sản phẩm', align: 'left', width: 110, sortable: false, resizable: false, formatter: function(value, row, index) {
		        	return Utils.XSSEncode(value);         
		        }},
		        {field: 'productName', title: 'Tên Sản Phẩm', align: 'left', width: 200, sortable: false, resizable: false, formatter: function(value, row, index) {
		        	return Utils.XSSEncode(value);         
		        }},
		        {field: 'cb', checkbox:true, align: 'center', width:80, sortable : false, resizable : false}
		    ]]
		});
	});
});

</script>
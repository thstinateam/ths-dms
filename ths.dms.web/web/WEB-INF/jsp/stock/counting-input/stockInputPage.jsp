<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>

<input type="hidden" id="function_code" value="<s:property value="systimestamp"/>" />
<s:set id="downloadToken" value="%{generateReportToken()}"></s:set>
<input type="hidden" id="report_token" value="<s:property value="#downloadToken"/>" />
<s:set id="downloadToken" value="%{putSessionValue(systimestamp, #downloadToken)}"></s:set>
		
<div class="BreadcrumbSection">
	<ul class="ResetList FixFloat BreadcrumbList">
		<li class="Sprite1"><a href="javascript:void(0);">Kho</a></li>
		<li><span>Nhập kiểm kho</span></li>
	</ul>
</div>
<div class="CtnOneColSection">
	<div class="ContentSection">
		<div class="ToolBarSection">
			<div class="SearchSection GeneralSSection">
				<h2 class="Title2Style">Thông tin tìm kiếm</h2>
				<div class="SearchInSection SProduct1Form">
					<label class="LabelStyle Label1Style">Đơn vị</label>
					<div class="BoxSelect BoxSelect2">
						<input id="cbxShop" style="width:200px;" />
					</div>
					<label class="LabelStyle Label1Style">Mã kiểm kê (F9)</label>					 
					<input	type="text" class="InputTextStyle InputText1Style" id="cycleCountCode" value="<s:property value="cycleCount.cycleCountCode"/>"/>
					<div class="Clear"></div>
					<div class="BtnCenterSection">
						<button class="BtnGeneralStyle" onclick="return StockCountingInput.getInfo();" id="btnGetInfo">Tìm kiếm</button>
					</div>
					<div class="Clear"></div>
<!-- 					<img id="loadingSearch" src="/resources/images/loading.gif" class="LoadingStyle" style="visibility: hidden;" /> -->
					<p id="errMsgSearch" style="display: none;"class="ErrorMsgStyle"></p>
				</div>
				<div id="infoDiv" style="display: none">
					<tiles:insertTemplate template="/WEB-INF/jsp/stock/counting-input/stockInputInfo.jsp"></tiles:insertTemplate>
				</div>
				<div class="Clear"></div>
				<p id="errExcelMsg" class="ErrorMsgStyle" style="display: none"></p>
				<p id="errMsg" class="ErrorMsgStyle"  style="display: none;">Có lỗi xảy ra khi cập nhật dữ liệu</p>
				<tiles:insertTemplate template="/WEB-INF/jsp/general/successMsg.jsp" />
			</div>
			<div class="Clear"></div>
		</div>
	</div>
	<div class="Clear"></div>
</div>
<div id ="cycleCountDialog" style="display:none;" >
	<div id="cycleCountEasyUIDialog">
		<div class="PopupContentMid">
			<div class="GeneralForm Search1Form" >
				<label class="LabelStyle Label1Style">Mã kiểm kê</label> 
				<input type="text" class="InputTextStyle InputText1Style" id="dgCycleCountCode" maxlength="50" style="width:130px;" />
				 
				<label class="LabelStyle Label1Style">Trạng thái</label>
				<div class="BoxSelect BoxSelect2">
					<select id="dgCycleCountType">						
						<option value="-1">Chọn trạng thái</option>
						<option value="0">Đang thực hiện</option>
						<option value="1">Đã duyệt</option>
						<option value="2">Hủy bỏ</option>
		                <option value="3">Từ chối</option>
		                <option value="4">Chờ duyệt</option>
					</select>
				</div>
				<label class="LabelStyle Label1Style">Mô tả </label> 
				<input type="text" class="InputTextStyle InputText10Style" id="dgCycleCountDes"  maxlength="200" />
				<div class="Clear"></div>
				<label class="LabelStyle Label1Style">Đơn vị </label> 
				<div class="BoxSelect BoxSelect">
					<input id="shop" style="width:140px;" />
				</div>
					
				<label class="LabelStyle Label1Style">Từ ngày tạo</label> 
				<input type="text" class="InputTextStyle InputText1Style" id="dgCycleCountFromDate" style="width: 175px;" type="text" maxlength="10"/>
				
				<label class="LabelStyle Label1Style">Đến ngày tạo</label> 
				<input type="text" class="InputTextStyle InputText1Style" id="dgCycleCountToDate" style="width: 140px;" type="text" maxlength="10"/>
				
				<div class="Clear"></div>
				<label class="LabelStyle Label1Style">Chọn kho</label>
				<div class="BoxSelect BoxSelect">
					<select id="cycleCountWareHouse" style="width:140px;">
                    </select>
                </div>
                <div class="Clear"></div>
				<div class="BtnCenterSection">
					<button class="BtnGeneralStyle" onclick="StockCountingInput.searchCycleCountOnEsyUI();">Tìm kiếm</button>
				</div>
				
				<p id="saveMessageError" style="display: none;" class="ErrorMsgStyle"></p>
				<div class="Clear"></div>
				
				<div class="GridSection" id="productGridContainer">					
					<table id="grid"></table>
					<div id="pager"></div>
				</div>
				
				<div class="BtnCenterSection">
					<button class="BtnGeneralStyle BtnGeneralMStyle" onclick="$('#cycleCountEasyUIDialog').dialog('close');">Đóng</button>
				</div>				
			</div>
		</div>
	</div>
</div>
<div id ="productLotDialog" style="display: none" >
	<div id="productLotEasyUIDialog" class="easyui-dialog" title="Thông tin lô" data-options="closed:true,modal:true">
		<div class="PopupContentMid">
			<div class="GeneralForm Search1Form" >
				<label class="LabelStyle Label1Style" style=" width: 100px;">Mã sản phẩm</label>
				<p id="d_productCode" class="LabelStyle Label1Style" style="color: #000000;font-weight: bold;text-align: left;"></p> 
				
				<label class="LabelStyle Label1Style" style=" width: 164px;">Tên sản phẩm</label> 
				<p id="d_productName" class="LabelStyle Label1Style" style="color: #000000;font-weight: bold;text-align: left;width: 230px;margin-bottom: 9px;"></p>                                    
				<div class="Clear"></div>
				<label class="LabelStyle Label1Style" style=" width: 100px;">Số lô <span class="ReqiureStyle">*</span></label>
				<input maxlength="6" id="productLot" maxlength="50" type="text" style=" width: 145px;" class="InputTextStyle InputText1Style" />			
				
				<label class="LabelStyle Label1Style" style=" width: 100px;">Số lượng <span class="ReqiureStyle">*</span></label>
				<input id="quantityCountOfLot" type="text" class="InputTextStyle InputText1Style" maxlength="8"/>
				
				<div class="Clear"></div>
				<s:if test="cycleCount.status.value == 0">
				<div class="BtnCenterSection">
					<button class="BtnGeneralStyle" id="btnSaveCCResult" onclick="StockCountingInput.saveCCResult();">Thêm</button>					
				</div>					
				</s:if>
				<div class="GridSection" id="productLotGridContainer">					
					<div class="GeneralTable General1Table">
						<table border="0" cellspacing="0" cellpadding="0">
							<colgroup>
								<col style="width: 80px;" />
								<col style="width: 160px;" />
								<col style="width: 160px;" />
								<col style="width: 160px;" />
								<col style="width: 50px;" />
							</colgroup>
							<thead>
								<tr>
									<th class="ColsThFirst">STT</th>
									<th>Số lô</th>
									<th>Số lượng chốt</th>
									<th>Số lượng đếm</th>
									<th class="ColsThEnd">Xóa</th>
								</tr>
							</thead>
							<tbody id="stockCClist">
								<tiles:insertTemplate template="/WEB-INF/jsp/stock/counting-input/stockInputDetailLot.jsp"></tiles:insertTemplate>
							</tbody>
							<tfoot>
								<tr>
									<td colspan="2" style="border-left: 1px solid #C6D5DB;text-align: center;">Tổng:</td>
									<td style="text-align: right; padding: 6px 8px 0 0;" id="totalBeforeCount"></td>
									<td style="text-align: right; padding: 6px 8px 0 0;" id="totalCount"></td>
									<td class="ColsTd7 ColsTdEnd">&nbsp;</td>
								</tr>
							</tfoot>
						</table>
					</div>					
				</div>		
				<div class="Clear"></div>				
				<div class="BtnCenterSection">
					<button class="BtnGeneralStyle"  id="btnCloseProductLot">Đóng</button>					
				</div>				                                    
				<div class="Clear"></div>
				  <p id="errMsg1" style="display: none;" class="ErrorMsgStyle"></p>
	              <p id="succMsg1" style="display: none" class="SuccessMsgStyle">Cập nhật thành công</p>
				  <input type="text" style="display: none;" id="h_CCMapProductId" />
	              <input type="text" style="display: none;" id="h_CCResultId" />
	              <input type="text" style="display: none;" id="h_ttBefore" />
			</div>
		</div>
	</div>
</div>
<s:hidden id="shopCode" value=""></s:hidden>
<script type="text/javascript">
// var status = '<s:property value="cycleCount.status.value"/>';
$(document).ready(function(){
	StockCountingInput.loadComboboxShop('cbxShop', '206');
	//$("#dgCycleCountType").attr('disabled', 'disabled');	
	$('#cycleCountCode').bind('keyup', function(event){
		if(event.keyCode == keyCode_F9){
			StockCountingInput.searchCycleCountOnDialog();
		}
		if(event.keyCode == keyCodes.ENTER){
			StockCountingInput.getInfo();
		}
	});
	StockCountingInput._myMap = new Map();
	$('#cycleCountCode').focus();
});
</script>
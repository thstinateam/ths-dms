<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@taglib prefix="k" uri="/kryptone"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<s:iterator id="lst_CCResult" value="cycleCountResults" status="u">
<tr id="row_<s:property value="lot"/>">
       <td class="ColsTd1 OrderNumber"><s:property value='#u.index + 1'/></td>
       <td class="ColsTd2 AlignRight" id="ftd_lot_<s:property value="lot"/>"><s:property value='#lst_CCResult.lot'/></td>
       <td class="ColsTd3 AlignRight" ><span class="qtyBeforeCount"><s:property value='#lst_CCResult.quantityBeforeCount'/></span></td>
       <td class="ColsTd4 AlignRight" id="ftd_qty_<s:property value="quantityCounted"/>" ><span class="qtyCount"><s:property value='#lst_CCResult.quantityCounted'/></span></td>
       <td class="ColsTd5"><a onclick="StockCountingInput.editCCResult(<s:property value='#lst_CCResult.id'/>,'<s:property value='#lst_CCResult.lot'/>',<s:property value='#lst_CCResult.quantityCounted'/>)" href="javascript:void(0)"><img src="/resources/images/icon-edit.png" width="16" height="17" /></a></td>
		<td class="ColsTd6 ColsTdEnd"><a onclick="StockCountingInput.delCCResult(<s:property value='#lst_CCResult.id'/>,<s:property value="lot"/>)" href="javascript:void(0)"><img src="/resources/images/icon-delete.png" width="19" height="20" /></a></td>
    </tr>
</s:iterator>
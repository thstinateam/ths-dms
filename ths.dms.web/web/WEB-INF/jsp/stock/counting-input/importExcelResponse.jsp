<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="k" uri="/kryptone" %>
<div id="errorExcel" style="display: none"><s:property value="isError"/></div>
<div id="errorExcelMsg" style="display: none"><s:property value="errMsg"/></div>
<div id="totalRow" style="display: none"><s:property value="totalItem"/></div>
<div id="numFail" style="display: none"><s:property value="numFail"/></div>
<div id="fileNameFail" style="display: none"><s:property value="fileNameFail"/></div>
<div id="typeView" style="display: none"><s:property value="typeViewExcel"/></div>
<div id="lstSize" style="display: none"><s:property value="lstView.size()"/></div>
<div id="tokenValue" style="display:none"><s:property value="token" /></div>
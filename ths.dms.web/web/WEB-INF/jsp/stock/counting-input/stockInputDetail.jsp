<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@taglib prefix="k" uri="/kryptone"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<s:if test="lstMapProductVO != null && lstMapProductVO.size() > 0">
	<s:iterator id="lstCCDetail" value="lstMapProductVO"  var="obj" status="status" >
		<tr id="tr_<s:property value="id"/>">
			<td class="ColsTd1 AlignCCols isRowIndex">
				<s:property value='stt' />
			</td>
			<s:if test="flag == 1">
				<td class="Wordwrap AlignLCols isLotProductInsert">
					<s:property value="productCode"/>
					<input type="hidden" value="<s:property value="checkLot"/>" id="lot<s:property value="productId"/>" />
				</td>
			</s:if>
		    <s:else>
		    	<td class="Wordwrap AlignLCols isLotProductCurent">
					<s:property value="productCode"/>
					<input type="hidden" value="<s:property value="checkLot"/>" id="lot<s:property value="id"/>" />
				</td>
		    </s:else>
			<td class="Wordwrap AlignLCols"><s:property value="productName"/></td>
			<td class="Wordwrap AlignLCols"><s:property value="uom1"/></td>
			<td class="AlignCCols" id="countDate<s:property value="id"/>">
<%-- 				<s:date name="countDate" value="#obj.countDate" format="dd/MM/yyyy" /> --%>
 				<s:property value="countDateStr"/>
			</td>
			<td class="AlignRCols" id="td_qtyBeforeCount<s:property value="id"/>">
				<s:property value="convertConvfact(#obj.quantityBeforeCount,#obj.convfact)"/>
			</td>		
			<td class="AlignRCols" style="padding: 4px 5px;">	
				<s:if test="cycleCount.status.value == 0 || cycleCount.status.value == 3">
					<input onkeypress="return NextAndPrevTextField(event,this,'ProductNotLot')" confact="<s:property value="convfact"/>" size="8" maxlength="8" 
						id="td_qtyCount<s:property value="id"/>"	
			       		type="text" class="InputTextStyle InputText1Style ProductNotLot" 		       	
			       		value="<s:property value="convertConvfact(quantityCounted, convfact)"/>"     	
			       		onchange="return myStockInputFun(this, <s:property value="convfact"/>);" />
				</s:if>
				<s:elseif test="cycleCount.status.value == 4">
					<input confact="<s:property value="convfact"/>" size="8" maxlength="8" id="td_qtyCount<s:property value='id'/>"	
			       		type="text" class="InputTextStyle InputText1Style ProductNotLot" 		       	
			       		value="<s:property value="convertConvfact(quantityCounted, convfact)"/>" disabled="disabled"/>
				</s:elseif>
				<s:else>
					<span id="td_qtyCountP<s:property value="id"/>" >
 				 		<s:property value="convertConvfact(quantityCounted, convfact)"/>
 				   	</span>
				</s:else>
			</td>
			<td class="AlignRCols" id="td_qtyDifference<s:property value="id"/>">
				<s:property value="convertConvfact(quantityBeforeCount - quantityCounted, convfact)"/>
			</td>
			<s:if test="flag == 1">
				<td class="ColsTdEnd AlignCCols isFlagInsert" >
				<s:if test="cycleCount.status.value == 0 || cycleCount.status.value == 3">
		    		<input class="cb_element" name="chkApprove" type="checkbox" style="vertical-align: middle;" id="td_chkApprove<s:property value='id'/>" value="<s:property value='productId'/>" />
			    </s:if>
			    <s:elseif test="cycleCount.status.value == 4">
					<input class="cb_element" name="chkApprove" type="checkbox" style="vertical-align: middle;" id="td_chkApprove<s:property value='id'/>" value="<s:property value='productId'/>" disabled="disabled"/>
				</s:elseif>
		    	<a id="row_del_tmp_<s:property value="productId"/>" class="isDeleteRowTmp" style="padding: 0; margin-left: 5px; vertical-align: middle;" onclick="StockCountingInput.removeProductInsert(<s:property value="id"/>, <s:property value="stt"/>)"><span style="cursor:pointer"><img src="/resources/images/icon_delete.png" title="Xóa sản phẩm"></span></a>
				</td>
		    </s:if>
		    <s:else>
		    	<td class="ColsTdEnd AlignCCols isFlagCurent" >
		    	<s:if test="cycleCount.status.value == 0 || cycleCount.status.value == 3">
		    		<input class="cb_element" name="chkApprove" type="checkbox" id="td_chkApprove<s:property value='id'/>" value="<s:property value='id'/>" />
			    </s:if>
			    <s:elseif test="cycleCount.status.value == 4">
					<input class="cb_element" name="chkApprove" type="checkbox" id="td_chkApprove<s:property value='id'/>" value="<s:property value='id'/>" disabled="disabled"/>
				</s:elseif>
				</td>
		    </s:else>
		</tr>
	</s:iterator>
</s:if>
<s:else>
	<tr><td colspan="9" class="NotData">Không có dữ liệu nào</td></tr>
</s:else>
<script type="text/javascript">
//total
function myStockInputFun(txt, convfact){
	var quantityCounted = StockValidateInput.getQuantity($(txt).val().trim(), convfact);
	$(txt).val(StockValidateInput.formatStockQuantity(quantityCounted, convfact));
	if ($(txt).val().trim().length == 0) {
		$(txt).val('0/0');
		$($(txt).parent().parent().children()[8]).children().attr('checked', false);
	} else {
		$($(txt).parent().parent().children()[8]).children().attr('checked', true);
	}	
	
	var quantity = StockValidateInput.getQuantity($($(txt).parent().parent().children()[5]).text().trim(), convfact);
	var difference = Number(quantity) - Number(quantityCounted);
	$($(txt).parent().parent().children()[7]).text(StockValidateInput.formatStockQuantity(difference, convfact));
};
</script>
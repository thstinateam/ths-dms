<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>

<input type="hidden" id="function_code" value="<s:property value="systimestamp"/>" />
<s:set id="downloadToken" value="%{generateReportToken()}"></s:set>
<input type="hidden" id="report_token" value="<s:property value="#downloadToken"/>" />
<s:set id="downloadToken" value="%{putSessionValue(systimestamp, #downloadToken)}"></s:set>

<div class="BreadcrumbSection">
	<ul class="ResetList FixFloat BreadcrumbList">
		<li class="Sprite1"><a href="/stock/update/info"><s:text name="jsp.common.warehouse"/></a></li>
		<li><span><s:text name="stock.update.title.nhap.xuat.kho.dieu.chinh"/></span></li>
	</ul>
</div>
<div class="CtnOneColSection">
	<div class="ContentSection">
		<div class="ToolBarSection">
			<div class="SearchSection GeneralSSection">
				<h2 class="Title2Style"><s:text name="jsp.common.info.common"/></h2>
				<div class="SearchInSection SProduct1Form">
					<label class="LabelStyle Label1Style"><s:text name="common.unit"/></label>
					<div class="BoxSelect BoxSelect2">
						<input id="shopCbx" style="width:206px;">
					</div>
					
					<label class="LabelStyle Label1Style"><s:text name="jsp.common.loai"/></label>
					<div class="BoxSelect BoxSelect2" >
						<select class="MySelectBoxClass" id="typeUpdate" onchange="return StockUpdate.typeUpdateChange();" autocomplete ="off">
							<option value="-1" selected="selected"><s:text name="stock.update.title.chon.loai.kho"/></option>
							<option value="1"><s:text name="stock.update.title.xuat.dieu.chinh"/></option>
							<option value="0"><s:text name="stock.update.title.nhap.dieu.chinh"/></option>
						</select>
					</div>
					<label class="LabelStyle Label1Style"><s:text name="stock.update.title.ma.phieu"/></label>
					<s:textfield id="code" disabled="true" readonly="true" name="code" maxLength="20" cssClass="InputTextStyle InputText1Style" autocomplete ="off"></s:textfield>

					<div class="Clear"></div>
					
					<label class="LabelStyle Label1Style"><s:text name="stock.update.title.nhan.vien"/></label> <input type="text" class="InputTextStyle InputText1Style" disabled="disabled" maxLength="20" id="issuedStaff" value="<s:property value ="staff.staffName" />" />

					<label class="LabelStyle Label1Style"><s:text name="jsp.common.ngay.n"/></label>
					<s:if test="issuedDate!=null">
						<input type="text" class="InputTextStyle InputText1Style" readonly="readonly" disabled="disabled" id="issuedDate" maxlength="10" value="<s:property value='issuedDate' />"  autocomplete="off"/>
					</s:if>
					<s:else>
						<input type="text" class="InputTextStyle InputText6Style" id="issuedDate" maxlength="10" autocomplete="off"/>
					</s:else>
					
					<div class="Clear"></div>
					<%-- <div style="text-align:center; color: red"><s:text name="stock.update.kho.chua.duoc.cap.nhat.cho.den.khi.ho.duyet"/> </div> --%>
					<font color="red"><marquee scrollamount="3"><s:text name="stock.update.kho.chua.duoc.cap.nhat.cho.den.khi.ho.duyet"/></marquee></font>
					<div class="Clear"></div>
				</div>
				<h2 class="Title2Style"><s:text name="stock.update.title.thong.tin.san.pham.dieu.chinh"/></h2>
				<div class="SearchInSection SProduct1Form">
					<div class="GridSection" id="idGridDg">
						<table id="dg" ></table>
					</div>
					<div class="GeneralForm GeneralNoTP1Form" id="groupBtnEventEditDbDiv" style="width: auto; display: none;">
						<div class="Func1Section" style="min-width: 615px; width: auto;">
							<div id="groupEventEditDb" class = "cmsiscontrol">
								<form action="/stock/update/import-excel?staffId=<s:property value='staffId'/>" name="importFrm" id="importFrmStock_importFrm" method="post" enctype="multipart/form-data">
									<p class="DownloadSFileStyle DownloadSFile2Style">
										<a href="javascript:void(0)" class="Sprite1" id="downloadTemplate" onclick="StockUpdate.downloadTemplate();"><s:text name="jsp.common.tai.file.excel"/></a>
									</p>
									<input type="file" class="InputFileStyle InputText1Style" size="20" name="excelFile" id="excelFile" onchange="previewImportExcelFile(this,'importFrmStock_importFrm');">
									<div class="FakeInputFile">
										<input type="text" id="fakefilepc" class="InputTextStyle InputText1Style" />
									</div>
								</form>
								<button class="BtnGeneralStyle" id="btnInputFile_" onclick="return StockUpdate.importExcel();"><s:text name="jsp.common.nhap.excel"/></button>
								<button class="BtnGeneralStyle" id="btnUpdate_" onclick="return StockUpdate.updateStock();"><s:text name="stock.update.title.dieu.chinh"/></button>
							</div>
							<button class="BtnGeneralStyle" style="float: right;" id="btnExportExcel_"><s:text name="stock.update.title.in.phieu"/></button>
<%-- 							<s:if test="shopLocked"> --%>
<%-- 								<button class="BtnGeneralDStyle BtnGeneralStyle isNotPointer" disabled="disabled" id="btnInputFile" onclick="return StockUpdate.importExcel();"><s:text name="jsp.common.nhap.excel"/></button> --%>
<%-- 								<button class="BtnGeneralDStyle BtnGeneralStyle isNotPointer" disabled="disabled" id="btnUpdate" onclick="return StockUpdate.updateStock();"><s:text name="stock.update.title.dieu.chinh"/></button> --%>
<%-- 								<button class="BtnGeneralDStyle BtnGeneralStyle isNotPointer" disabled="disabled" id="btnExportExcel"><s:text name="stock.update.title.in.phieu"/></button> --%>
<%-- 							</s:if> --%>
<%-- 							<s:else> --%>
								
<%-- 							</s:else> --%>
						</div>
						<div class="Clear"></div>
					</div>
				</div>
				<div class="Clear"></div>
				<!-- <p id="errExcelMsg" class="ErrorMsgStyle" style="display: none"></p> -->
				<p id="errMsg" class="ErrorMsgStyle" style="display: none;"><s:text name="jsp.common.err.data.update"/></p>
				<tiles:insertTemplate template="/WEB-INF/jsp/general/successMsg.jsp" />
				<img id="loading" src="/resources/images/loading.gif" class="LoadingStyle" style="visibility: hidden;" />
			</div>
			<div class="Clear"></div>
		</div>
	</div>
	<div class="Clear"></div>
</div>

<p id="lockedMsg" class="ErrorMsgStyle" style="display: none;"></p>
<s:hidden id="stockTransId" value=""></s:hidden>
<s:hidden id="shopId" name="shopId"></s:hidden>
<s:hidden id="staffId" name="staffId"></s:hidden>

<div id ="productDialog" style="width:600px;visibility: hidden;" >
	<div id="productEasyUIDialog" class="easyui-dialog" title="<s:text name="jsp.product.thong.tin.san.pham"/>" data-options="closed:true,modal:true">
		<div class="PopupContentMid">
			<div class="GeneralForm Search1Form" >
				<label class="LabelStyle Label1Style" style=" width: 150px;"><s:text name="jsp.common.product.code"/></label>
				<input id="productCode" maxlength="50" tabindex="1" type="text" style=" width: 145px;" class="InputTextStyle InputText1Style" /> 
				<label class="LabelStyle Label1Style" style=" width: 150px;"><s:text name="jsp.common.product.name"/></label> 
				<input id="productName" type="text" maxlength="250" tabindex="2" style="width: 145px;"  class="InputTextStyle InputText1Style" />
				<div class="BtnCenterSection">
				<button class="BtnGeneralStyle BtnSearchStyle BtnSearchOnDialog" tabindex="3" id="issSearchProductDialog"><s:text name="jsp.common.timkiem"/></button>
				</div>
				<div class="Clear"></div>
				<div class="GridSection" id="productGridContainer">					
					<table id="productGrid" class="easyui-datagrid"></table>
					<div id="productPager"></div>
				</div>
				<div class="BtnCenterSection">
					<button onclick="StockUpdate.selectListProducts();" id="btnChooseProduct" class="BtnGeneralStyle"><s:text name="jsp.common.chon.simple"/></button>
					<button class="BtnGeneralStyle BtnGeneralMStyle" onclick="$('.easyui-dialog').dialog('close');"><s:text name="jsp.common.dong"/></button>
				</div>
				 <p id="errMsg" class="ErrorMsgStyle" style="display: none;"></p>
			</div>
		</div>
	</div>
</div>
<div id ="productLotDialog" style="width:600px;visibility: hidden;" >
	<div id="productLotEasyUIDialog" class="easyui-dialog" title="Chọn lô" data-options="closed:true,modal:true">
		<div class="PopupContentMid">
			<div class="GeneralForm Search1Form" >
				<label class="LabelStyle Label1Style" style=" width: 100px;"><s:text name="jsp.common.product.code"/></label>
				<p id="fancyProductCode" class="LabelStyle Label1Style" style="color: #000000;font-weight: bold;"></p> 
				<label class="LabelStyle Label1Style" style=" width: 100px;"><s:text name="jsp.common.product.name"/></label> 
				<p id="fancyProductName" class="LabelStyle Label1Style" style="color: #000000;font-weight: bold;text-align: left;width: 230px;"></p>                                    
				<div class="Clear"></div>
				
				<label class="LabelStyle Label1Style" style=" width: 100px;"><s:text name="jsp.common.so.luong"/></label>
				<p  id="fancyProductTotalQuantity" class="LabelStyle Label1Style" style="color: #000000;font-weight: bold;"></p>			
				
				<div class="Clear"></div>				
				<div class="GridSection" id="productLotGridContainer">					
					<table id="productLotGrid" class="easyui-datagrid"></table>					
				</div>		
				<div class="Clear"></div>
				<label class="LabelStyle Label1Style" style=" width: 100px;"><s:text name="stock.update.title.tong.ton.kho"/></label>
				<p id="totalQuantity" class="LabelStyle Label1Style" style="color: #000000;font-weight: bold;"></p> 
				<label class="LabelStyle Label1Style" style=" width: 100px;"><s:text name="stock.update.title.tong.so.luong"/></label> 
				<p id="totalAvailableQuantity" class="LabelStyle Label1Style" style="color: #000000;font-weight: bold;text-align: left;width: 230px;"></p>                                    
				<div class="Clear"></div>		 
				<div class="BtnCenterSection">
					<button id="btnSelectLot" onclick="return StockUpdate.acceptSeparation();" class="BtnGeneralStyle"><s:text name="jsp.common.chon.simple"/></button>
					<button class="BtnGeneralStyle BtnGeneralMStyle" onclick="$('.easyui-dialog').dialog('close');"><s:text name="jsp.common.dong"/></button>
				</div>				                                    
				<div class="Clear"></div>
				 <p id="fancyboxError" style="display: none;" class="ErrorMsgStyle"></p>
			</div>
		</div>
	</div>
</div>
<s:hidden id="staffRoleType" name="staffRoleType"></s:hidden>
<s:hidden id="shopCodeNPP" name="shopCodeNPP"></s:hidden>
<s:hidden id="shopCodeStaff" name="shopCode"></s:hidden>
<input type="hidden" id="typeUpdateHide" value="-1">
<input type="hidden" id="totalAvailableQuantityHiden" value="0">
<input type="hidden" id="isShop" value="1">
<input type="hidden" id="hiddenCode" />

<script type="text/javascript">
	$(document).ready(function() {
		Utils.loadShopCbx('#shopCbx', null, null, function(data) {
			StockUpdate.selectShop(data.id, data.shopCode);
		});
		//$('#ipContentTransDialog').val('Vận chuyển hàng đi bán lưu động');
		setDateTimePicker('fDateDialog');
		$('#fDateDialog').val($('#issuedDate').val());
		var typeUpdate = $('#typeUpdate').val();
		$('#code').val('');
		$('#hiddenCode').val($('#code').val().trim());
		disabled('btnExportExcel_');
		disabled('btnExportExcelNew');
		StockUpdate.TYPE= 1;						
		disabled('issuedStaff');
		
		$('.combo-text').live('focus',function(){
			$('.combo-panel').each(function(){if(!$(this).is(':hidden')){$(this).parent().css('width','250px');$(this).css('width','248px');}});
		});
		
		$('#typeUpdate').bind('change',function(){
			StockUpdate.TYPE=parseInt($(this).val().trim());	
		});
		StockUpdate.lstProductUpdate.push(new Object());
		StockUpdate.loadGridProduct();
		
		var staffRoleType = $('#staffRoleType').val();
		var shopCodeStaff = $('#shopCodeStaff').val();
		if(staffRoleType != undefined && staffRoleType != null && parseInt(staffRoleType)== StaffRoleType.KTNPP
				&& shopCodeStaff != undefined && shopCodeStaff.length > 0){
			$('#shopCbx').combobox('setValue', shopCodeStaff);
			StockUpdate.synchronization();
		}else{
			var shopCodeNPP = $('#shopCodeNPP').val();
			if(shopCodeNPP != undefined && shopCodeNPP.length > 0){
				$('#shopCbx').combobox('setValue', shopCodeNPP);
				StockUpdate.synchronization();
			}
		}
		$('#groupBtnEventEditDbDiv').show();
	});		
	StockUpdate.setEventInput();
	$('.combo-arrow').live('click',function(){$('.combo-panel').each(function(){if(!$(this).is(':hidden')){$(this).parent().css('width','250px');$(this).css('width','248px');}});});
	
	function loadDateLock(param, callback){
		Utils.getJSONDataByAjax(param,'/catalog/utility/closing-day/check',function(data){
			if(!data.error) {
				if (data.locked) {
// 					disabled('btnInputFile_');
// 					disabled('btnUpdate_');
					disabled('btnExportExcel_');
					$('#lockedMsg').html('<s:text name="stock.update.title.npp.da.chot.ton.kho.ngay.lam.viec"/>').show();
				} else {
// 					enable('btnInputFile_');
// 					enable('btnUpdate_');
					enable('btnExportExcel_');
					$('#lockedMsg').html('').hide();
					callback.call();
				}
				if(data.appDate!=null){
					$('#issuedDate').val(data.appDate);
				}
			} else {
				$('#errMsg').html(data.errMsg).show();					
			}
		});
	}
</script>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@page import="ths.dms.helper.Configuration"%>
<%@page import="ths.dms.web.constant.ConstantManager" %>
<s:i18n name="message">
	<html xmlns="http://www.w3.org/1999/xhtml">
		<head>
			<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
			<meta http-equiv="X-UA-Compatible" content="IE=8" />
			<meta name="description" content="DMS"/>
			<title> DMS </title>
			<tiles:insertTemplate template="/WEB-INF/jsp/general/multiLanguage.jsp" />
			<link rel="shortcut icon" type="image/x-icon" href="<%=Configuration.getImgServerPath()%>/resources/images/favicon.ico"/>		
			<link href="<%=Configuration.getCssServerPath("/resources/styles/style.min.css")%>" rel="stylesheet" type="text/css" />
			<link href="<%=Configuration.getCssServerPath("/resources/scripts/plugins/jquery-ui.css") %> rel="stylesheet" type="text/css" />
			<link href="<%=Configuration.getCssServerPath("/resources/scripts/plugins/handsontable/jquery.handsontable.full.css")%>" rel="stylesheet" type="text/css" />
			<link href="<%=Configuration.getCssServerPath("/resources/scripts/plugins/handsontable/jquery.handsontable.removeRow.css")%>" rel="stylesheet" type="text/css" />
			<!--[if lt IE 8]>
				<link rel="stylesheet" type="text/css" href="<%=Configuration.getCssServerPath("/resources/styles/fixed_ie7.css")%>" />
			<![endif]-->
			<!--[if IE 8]>
				<link rel="stylesheet" type="text/css" href="<%=Configuration.getCssServerPath("/resources/styles/fixed_ie8.css")%>" />
			<![endif]-->		
			<link rel="stylesheet" type="text/css" href="<%=Configuration.getCssServerPath("/resources/styles/fixed_chrome.css")%>" />
			<script type="text/javascript" src="<%=Configuration.getStaticServerPath("/resources/scripts/general/json-bigint.js")%>"></script>
			<script type="text/javascript" src="<%=Configuration.getStaticServerPath("/resources/scripts/plugins/ckfinder/plugins/gallery/colorbox/jquery.min.js")%>"></script>
<%-- 			<script type="text/javascript" src="<%=Configuration.getStaticServerPath("/resources/scripts/jquery-1.8.0.js")%>"></script> --%>
			<script type="text/javascript" src="<%=Configuration.getStaticServerPath("/resources/scripts/jquery.core-plugin.js")%>"></script>
			<script type="text/javascript" src="<%=Configuration.getStaticServerPath("/resources/scripts/jquery.hotkeys.js")%>"></script>
			<script type="text/javascript" src="<%=Configuration.getStaticServerPath("/resources/scripts/plugins/jquery/jquery.maskedinput-1.3.js")%>"></script>
			<script type="text/javascript" src="<%=Configuration.getStaticServerPath("/resources/scripts/ths.core-api.js")%>"></script>
			<script type="text/javascript" src="<%=Configuration.getStaticServerPath("/resources/scripts/plugins/jqgrid/jquery.jqGrid.min.js")%>"></script>
			<script type="text/javascript" src="<%=Configuration.getStaticServerPath("/resources/scripts/plugins/jqgrid/grid.locale-vn.js")%>"></script>
			<script type="text/javascript" src="<%=Configuration.getStaticServerPath("/resources/scripts/business/catalog/ths.catalog.js")%>"></script>
			<script type="text/javascript" src="<%=Configuration.getStaticServerPath("/resources/scripts/business/program/ths.program.js")%>"></script>
<%-- 			<script type="text/javascript" src="<%=Configuration.getStaticServerPath("/resources/scripts/business/stock/ths.stock-manager.js")%>"></script> --%>
			
			<!-- QUAN LY THIET BI TAI SAN  -->
			<script type="text/javascript" src="<%=Configuration.getStaticServerPath("/resources/scripts/business/equipment/ths.equipment.js")%>"></script>
			<!-- SALE MT -->
			<script type="text/javascript" src="<%=Configuration.getStaticServerPath("/resources/scripts/plugins/fancybox/jquery.fancybox.js")%>"></script>
			<script type="text/javascript" src="<%=Configuration.getStaticServerPath("/resources/scripts/business/common/ths.common.business.js")%>"></script>			
			<script type="text/javascript" src="<%=Configuration.getStaticServerPath("/resources/scripts/plugins/jqtree/tree.jquery.js")%>"></script>
			<script type="text/javascript" src="<%=Configuration.getStaticServerPath("/resources/scripts/plugins/jstree/jquery.jstree.js")%>"></script>
			<script type="text/javascript" src="<%=Configuration.getStaticServerPath("/resources/scripts/utils/customSelect.jquery.js")%>"></script>
			<script type="text/javascript" src="<%=Configuration.getStaticServerPath("/resources/scripts/business/stock/ths.stock.js")%>"></script>
			<script type="text/javascript" src="<%=Configuration.getStaticServerPath("/resources/scripts/business/po/ths.po.js")%>"></script>
			<script type="text/javascript" src="<%=Configuration.getStaticServerPath("/resources/scripts/business/po-manual/ths.po-manual-create.js")%>"></script>
			<script type="text/javascript" src="<%=Configuration.getStaticServerPath("/resources/scripts/business/po-manual/ths.po-manual-manage.js")%>"></script>
			<script type="text/javascript" src="<%=Configuration.getStaticServerPath("/resources/scripts/business/customerdebit/ths.customerdebit.js")%>"></script>
			<script type="text/javascript" src="<%=Configuration.getStaticServerPath("/resources/scripts/business/suplierdebit/ths.suplierdebit.js")%>"></script>
			<script type="text/javascript" src="<%=Configuration.getStaticServerPath("/resources/scripts/business/saleplan/ths.sale-plan.js")%>"></script>
			<script type="text/javascript" src="<%=Configuration.getStaticServerPath("/resources/scripts/business/feedback/ths.feedback.js")%>"></script>
			<script type="text/javascript" src="<%=Configuration.getStaticServerPath("/resources/scripts/business/sale-product/ths.sale-product.js")%>"></script>
			<script type="text/javascript" src="<%=Configuration.getStaticServerPath("/resources/scripts/business/superviseshop/ths.superviseshop.js")%>"></script>
			<script type="text/javascript" src="<%=Configuration.getStaticServerPath("/resources/scripts/plugins/uploadify2.1/jquery.uploadify.v2.1.4.js")%>"></script>
			<script type="text/javascript" src="<%=Configuration.getStaticServerPath("/resources/scripts/business/shop-report/ths.report.js")%>"></script>
			<script type="text/javascript" src="<%=Configuration.getStaticServerPath("/resources/scripts/business/shop-report/ths.vansales-report.js")%>"></script>
			<script type="text/javascript" src="<%=Configuration.getStaticServerPath("/resources/scripts/business/shop-report/ths.debit-retrieve-report.js")%>"></script>
			<script type="text/javascript" src="<%=Configuration.getStaticServerPath("/resources/scripts/business/shop-report/ths.debit-return-report.js")%>"></script>
<%-- 			<script type="text/javascript" src="<%=Configuration.getStaticServerPath("/resources/scripts/business/shop-report/ths.stock-report.js")%>"></script> --%>
			<script type="text/javascript" src="<%=Configuration.getStaticServerPath("/resources/scripts/business/supervise/ths.supervise.js")%>"></script>
			<script type="text/javascript" src="<%=Configuration.getStaticServerPath("/resources/scripts/business/images/ths.images.js")%>"></script>
			<script type="text/javascript" src="<%=Configuration.getStaticServerPath("/resources/scripts/business/workingplan/ths.working-date-plan-manager.js")%>"></script>			
			<script type="text/javascript" src="<%=Configuration.getStaticServerPath("/resources/scripts/business/videos/ths.videos.js")%>"></script>
			<script type="text/javascript" src="<%=Configuration.getStaticServerPath("/resources/scripts/business/displaytool/ths.displaytool.js")%>"></script>
			<script type="text/javascript" src="<%=Configuration.getStaticServerPath("/resources/scripts/business/organization/ths.organization-system.js")%>"></script>
<%-- 			<script type="text/javascript" src="<%=Configuration.getStaticServerPath("/resources/scripts/plugins/uploadify2.1/swfobject.js")%>"></script> --%>
			<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/swfobject/2.2/swfobject.js"></script>			
			<script type="text/javascript" src="<%=Configuration.getStaticServerPath("/resources/scripts/plugins/ckeditor/ckeditor.js")%>"></script>
			<script type="text/javascript" src="<%=Configuration.getStaticServerPath("/resources/scripts/plugins/ckfinder/ckfinder.js")%>"></script>
			<link href="<%=Configuration.getCssServerPath("/resources/scripts/plugins/jquery/themes/datetimepicker/jquery-ui-1.8.22.custom.css")%>" rel="stylesheet" type="text/css" />
			<link href="<%=Configuration.getCssServerPath("/resources/scripts/plugins/ckeditor/skins/kama/editor.css")%>" rel="stylesheet" type="text/css" />
<%-- 			<link href="<%=Configuration.getCssServerPath("/resources/scripts/plugins/jquery-easyui-1.3.2/themes/default/easyui.css")%>" rel="stylesheet" type="text/css" /> --%>
			<script type="text/javascript" src="<%=Configuration.getStaticServerPath("/resources/scripts/plugins/jquery-easyui-1.3.5/easyloader.js")%>"></script>
			<script type="text/javascript" src="<%=Configuration.getStaticServerPath("/resources/scripts/plugins/jquery-easyui-1.3.5/jquery.easyui.min.js")%>"></script>
			<script type="text/javascript" src="<%=Configuration.getStaticServerPath("/resources/scripts/plugins/jquery-easyui-1.3.5/plugins/datagrid-detailview.js")%>"></script>
			<script type="text/javascript" src="<%=Configuration.getStaticServerPath("/resources/scripts/plugins/jquery-easyui-1.3.5/plugins/datagrid-groupview.js")%>"></script>
			<script type="text/javascript" src="<%=Configuration.getStaticServerPath("/resources/scripts/plugins/jquery-easyui-1.3.5/plugins/datagrid-bufferview.js")%>"></script>
			<script type="text/javascript" src="<%=Configuration.getStaticServerPath("/resources/scripts/plugins/jquery-easyui-1.3.5/src/jquery.propertygrid.js")%>"></script>
			<script type="text/javascript" src="<%=Configuration.getStaticServerPath("/resources/scripts/plugins/lazyload/jquery.lazyload.min.js")%>"></script>
			<%-- <script type="text/javascript" src="<%=Configuration.getStaticServerPath("/resources/scripts/business/common/ths.viettelmap.js")%>"></script> --%>
			<link href="<%=Configuration.getCssServerPath("/resources/scripts/plugins/jquery-easyui-1.3.5/themes/default/easyui.css")%>" rel="stylesheet" type="text/css" />
			<link href="<%=Configuration.getCssServerPath("/resources/scripts/plugins/jquery-easyui-1.3.5/themes/icon.css")%>" rel="stylesheet" type="text/css" />		
<!-- 			 OIPlayer -->
<%-- 			 <script src="<%=Configuration.getStaticServerPath("/resources/scripts/plugins/video/oiplayer/plugins/flowplayer-3.2.6.min.js")%>" type="text/javascript"></script> --%>
<%-- 			 <script src="<%=Configuration.getStaticServerPath("/resources/scripts/plugins/video/oiplayer/js/jquery.oiplayer.js")%>" type="text/javascript"></script> --%>
<%-- 			 <link rel="stylesheet" type="text/css" href="<%=Configuration.getCssServerPath("/resources/scripts/plugins/video/oiplayer/css/oiplayer.css")%>"/> --%>
<!-- 			 OIPlayer -->
			 <script type="text/javascript" src="<%=Configuration.getStaticServerPath("/resources/scripts/plugins/swfobject.js")%>"></script>
			 <script src="<%=Configuration.getStaticServerPath("/resources/scripts/plugins/flowplayer-3.2.16/flowplayer/flowplayer-3.2.12.min.js")%>" type="text/javascript"></script>
			 <script src="<%=Configuration.getStaticServerPath("/resources/scripts/plugins/flowplayer/flowplayer-3.2.12.min.js")%>" type="text/javascript"></script>
			 <script src="<%=Configuration.getStaticServerPath("/resources/scripts/plugins/video/jquery.media.js")%>" type="text/javascript"></script>
			 
			 <script type="text/javascript" src="<%=Configuration.getStaticServerPath("/resources/scripts/business/suplierdebit/ths.adjusted-revenue-manage.js")%>"></script>
			 <!-- CONTEXT-MENU -->
			 <link href="<%=Configuration.getCssServerPath("/resources/scripts/plugins/jquery-contextmenu/jquery.contextmenu.css")%>" rel="stylesheet" type="text/css" />
			 <script src="<%=Configuration.getStaticServerPath("/resources/scripts/plugins/jquery-contextmenu/jquery.contextmenu.js")%>" type="text/javascript"></script>
			 <script src="<%=Configuration.getStaticServerPath("/resources/scripts/general/ths.general.js")%>" type="text/javascript"></script>
			
			<link rel="stylesheet" type="text/css" href="<%=Configuration.getStaticServerPath("/resources/scripts/plugins/datetimepicker-master/jquery.datetimepicker.css") %>" />
			<script src="<%=Configuration.getStaticServerPath("/resources/scripts/plugins/datetimepicker-master/jquery.datetimepicker.js")%>" type="text/javascript"></script>

    		<link rel="stylesheet" type="text/css" href="<%=Configuration.getStaticServerPath("/resources/scripts/plugins/dropdown-check-list.1.4/css/ui.dropdownchecklist.themeroller.css") %>" />			
    		<script src="<%=Configuration.getStaticServerPath("/resources/scripts/plugins/dropdown-check-list.1.4/js/ui.dropdownchecklist-1.4-min.js")%>" type="text/javascript"></script>
    		<script src="<%=Configuration.getStaticServerPath("/resources/scripts/plugins/jquery/jquery.multiselect.js")%>" type="text/javascript"></script>
			 <script type="text/javascript" src="<%=Configuration.getStaticServerPath("/resources/scripts/business/common/ths.mapapi.js")%>"></script>
			<script type="text/javascript" src="<%=Configuration.getStaticServerPath("/resources/scripts/plugins/kendou.ui/js/kendo.web.min.js")%>"></script>
			 <link rel="stylesheet" type="text/css" href="<%=Configuration.getStaticServerPath("/resources/scripts/plugins/kendou.ui/styles/kendo.blueopal.css") %>" />
			 <link rel="stylesheet" type="text/css" href="<%=Configuration.getStaticServerPath("/resources/scripts/plugins/kendou.ui/styles/kendo.common.css") %>" />
			 
			 <script type="text/javascript" src="<%=Configuration.getStaticServerPath("/resources/scripts/plugins/handsontable/jquery.handsontable.full.js")%>"></script>
			 <script type="text/javascript" src="<%=Configuration.getStaticServerPath("/resources/scripts/plugins/handsontable/jquery.handsontable.removeRow.js")%>"></script>
<%-- 			 <script type="text/javascript" src="<%=Configuration.getStaticServerPath("/resources/scripts/business/sale-product/ths.create-sale-order.js")%>"></script> --%>
			<script type="text/javascript" src="<%=Configuration.getStaticServerPath("/resources/scripts/business/sale-product/ths.price-manager.js")%>"></script>
			 
			 <script type="text/javascript" src="<%=Configuration.getStaticServerPath("/resources/scripts/utils/viettel-utils-js.js")%>"></script>
			 <script type="text/javascript" src="<%=Configuration.getStaticServerPath("/resources/scripts/utils/json.post.util.js")%>"></script>
			 <script type="text/javascript" src="<%=Configuration.getStaticServerPath("/resources/scripts/utils/datagrid.helper.js")%>"></script>
			 <script type="text/javascript" src="<%=Configuration.getStaticServerPath("/resources/scripts/utils/dms.math.util.js")%>"></script>
			 
			 <!-- UPLOAD DROPZONEJS -->
			 <link href="<%=Configuration.getCssServerPath("/resources/styles/dropzonejs/bootstrap.css")%>" rel="stylesheet" type="text/css" />
			 <script type="text/javascript" src="<%=Configuration.getStaticServerPath("/resources/scripts/plugins/dropzonejs/dropzone.js")%>"></script>
    		 <script type="text/javascript" src="<%=Configuration.getStaticServerPath("/resources/scripts/utils/dms.upload.util.js")%>"></script>
    		
    		<!-- clockpicker -->
			<script type="text/javascript" src="<%=Configuration.getStaticServerPath("/resources/scripts/plugins/clockpicker-gh-pages/dist/jquery-clockpicker.min.js")%>"></script>
			<link rel="stylesheet" type="text/css" href="<%=Configuration.getStaticServerPath("/resources/scripts/plugins/clockpicker-gh-pages/dist/jquery-clockpicker.min.css") %>" />
			
    		<!-- settings manager -->
			<script type="text/javascript" src="<%=Configuration.getStaticServerPath("/resources/scripts/business/catalog/ths.settings.config.js")%>"></script>
			
<!-- 			Trungtm add from bracket -->
<%-- 			<script src="<%=Configuration.getStaticServerPath("/resources/styles/bracket/js/jquery-1.11.1.min.js")%>"></script> --%>
<%-- 			<script src="<%=Configuration.getStaticServerPath("/resources/styles/bracket/js/jquery-migrate-1.2.1.min.js")%>"></script> --%>
<%-- 			<script src="<%=Configuration.getStaticServerPath("/resources/styles/bracket/js/jquery-ui-1.10.3.min.js")%>"></script> --%>
<%-- 			<script src="<%=Configuration.getStaticServerPath("/resources/styles/bracket/js/bootstrap.min.js")%>"></script> --%>
<%-- 			<script src="<%=Configuration.getStaticServerPath("/resources/styles/bracket/js/modernizr.min.js")%>"></script> --%>
<%-- 			<script src="<%=Configuration.getStaticServerPath("/resources/styles/bracket/js/jquery.sparkline.min.js")%>"></script> --%>
<%-- 			<script src="<%=Configuration.getStaticServerPath("/resources/styles/bracket/js/toggles.min.js")%>"></script> --%>
<%-- 			<script src="<%=Configuration.getStaticServerPath("/resources/styles/bracket/js/retina.min.js")%>"></script> --%>
<%-- 			<script src="<%=Configuration.getStaticServerPath("/resources/styles/bracket/js/jquery.cookies.js")%>"></script> --%>
			
<%-- 			<script src="<%=Configuration.getStaticServerPath("/resources/styles/bracket/js/flot/jquery.flot.min.js")%>"></script> --%>
<%-- 			<script src="<%=Configuration.getStaticServerPath("/resources/styles/bracket/js/flot/jquery.flot.resize.min.js")%>"></script> --%>
<%-- 			<script src="<%=Configuration.getStaticServerPath("/resources/styles/bracket/js/flot/jquery.flot.spline.min.js")%>"></script> --%>
<%-- 			<script src="<%=Configuration.getStaticServerPath("/resources/styles/bracket/js/morris.min.js")%>"></script> --%>
<%-- 			<script src="<%=Configuration.getStaticServerPath("/resources/styles/bracket/js/raphael-2.1.0.min.js")%>"></script> --%>
			
<%-- 			<script type="text/javascript" src="<%=Configuration.getStaticServerPath("/resources/styles/bracket/js/custom.js")%>"></script> --%>
<%-- 			<script type="text/javascript" src="<%=Configuration.getStaticServerPath("/resources/styles/bracket/js/dashboard.js")%>"></script> --%>

				<script type="text/javascript" src="<%=Configuration.getStaticServerPath("/resources/styles/bracket/js/ths.custom.js")%>"></script>
<%-- 				<script type="text/javascript" src="<%=Configuration.getStaticServerPath("/resources/styles/bracket/js/dashboard.js")%>"></script> --%>
<!-- End from bracket -->
			
			 <!-- CONTEXT-MENU -->
			 <script type="text/javascript">
				$(document).ready(function(){
					var token = '<s:property value="token"/>';
					if (token!= null && token!= undefined) {
						$('#token').val(token);
					}
					var isView = '<s:property value="isShowChooseShopIsLv5"/>';
					General._urlRedrect = '<s:property value="urlRedrectCurentPage" />';
					if (isView != undefined && isView != null && Number(isView).toString().trim() !== 'NaN' && Number(isView) == 0) {
						General.openDialogSelectShopIslevel5();
						return;
					}
				});
			</script>
			<style type="text/css">
				.isNotPointer {
					pointer-events: none;
				}
				.isCMSInvisible {
					display: none;
					pointer-events: none;
				}
			</style>
			
			<link href="<%=Configuration.getCssServerPath("/resources/styles/bracket/css/style.default.css")%>" rel="stylesheet" type="text/css" />
			
		</head>
		<body class="VinamilkTheme">
			<tiles:insertTemplate template='/WEB-INF/jsp/general/serverInfoJs.jsp'/>
			<div id="container">
				<div class="leftpanel">
					<div id="header">
					<tiles:insertTemplate template="/WEB-INF/jsp/general/header.jsp" />
					</div>
				</div>
				<div class="mainpanel">
					<div class="headerbar">
						<tiles:insertTemplate template="/WEB-INF/jsp/general/headerBar.jsp" />
					</div>
					
					<div id="content" class="contentFilControlCMS">
					<tiles:insertAttribute  name="content" ignore="true"/>
            		<div class="Clear"></div>
	        		</div>
	        		<div id="msgErrorBanner" style="width: 100%; height: 50px; background-color: red; color: white; padding-left: 10px; position: fixed; bottom: 0; display: none;"></div>
	        		<div id="msgSuccessBanner" style="width: 100%; height: 50px; background-color: green; color: white; padding-left: 10px; position: fixed; bottom: 0; display: none;"></div>
	        		<div id="responseDiv" style="display:none;"></div>
	                <div id="footer">
	                	<tiles:insertTemplate template="/WEB-INF/jsp/general/footer.jsp" />
	                </div>
				</div>
				
				
				
                
			</div>	
			<div id="divOverlay" style="display:none;">
				<div class="DlgOverlay" style="display:block;"></div>
				<div class="OverlayLoading" id="imgOverlay" style="display:block;">
					<div class="OverlayInLoading">
						<img width="32" height="32" class="LoadingSection" src="/resources/images/loading.gif"/><s:text name="jsp.common.dangxuly"/>
					</div>
				</div>
			</div>
			<s:if test="currentUser.checkShopLock == null || currentUser.checkShopLock != 1">
				<div style="display:none">
					<div id="warningPopup" class="easyui-dialog" style="width: 266px;" title="Cảnh báo" data-options="closed:true,modal:true">
						<div class="PopupContentMid">
							<div class="GeneralForm" >
				                <div class="icon-warning" style="width: 48px; height: 48px; float: left; margin-right: 20px;"></div>
				                <p style="font-weight: bold; padding: 2px 0px;">Ngày làm việc (<span id="date" style="color: #CC0000;"></span>) đang nhỏ hơn ngày hiện tại (<span id="date1" style="color: #CC0000;"></span>).</p>
				                <div class="Clear"></div>
				                <div class="BtnCenterSection">
									<button class="BtnGeneralStyle" id="btnCancel" onclick="$('#warningPopup').dialog('close');">Đóng</button>
				                </div>
									<p id="errMsgSelectShop" class="ErrorMsgStyle" style="display: none"></p> 
							</div>
				             </div>
					</div>
				</div>
				<script type="text/javascript">
				 $(function() {	
					$.ajax({
							type : "POST",
							url : "/commons/checkshoplock",
							dataType: "json",
							success : function(data) {
								if(data.error == true){
									setTimeout(function(){
										//console.log(data);
										$('#warningPopup #date1').html(data.currentDate);
				 						$('#warningPopup #date').html(data.lockDate);
										$('#warningPopup').dialog('open');
									}, 1500);
								}
							}
						});
				 });
				</script>
			</s:if>
			<tiles:insertTemplate template="/WEB-INF/jsp/general/dialogChoseShopIslevel5.jsp" />
			<tiles:insertTemplate template="/WEB-INF/jsp/general/dialogChoseShopChange.jsp" />
			<tiles:insertTemplate template='/WEB-INF/jsp/general/serverInfo.jsp'/> 
			<!--  Cau hinh GoogleMap && VT Map -->
    		<s:if test="#session.mapType == null || #session.mapType == 1">
    			<script type="text/javascript" src="http://viettelmap.vn/VTMapService/VTMapAPI?api=VTMap&type=main&k=<%=Configuration.getViettelMapKey()%>"></script>
    			<script type="text/javascript" src="<%=Configuration.getStaticServerPath("/resources/scripts/business/supervise/ths.supervise-sales.js")%>"></script>
    			<script type="text/javascript" src="<%=Configuration.getStaticServerPath("/resources/scripts/business/common/ths.viettelmap.js")%>"></script>
    		</s:if>
    		<s:else>
    			<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=<%=Configuration.getGoogleMapsKey()%>"></script>
    			<script type="text/javascript" src="<%=Configuration.getStaticServerPath("/resources/scripts/business/supervise/ths.supervise-sales-googlemaps.js")%>"></script>
    			<script type="text/javascript" src="<%=Configuration.getStaticServerPath("/resources/scripts/utils/jquery.googlemaps.js")%>"></script>
    			<%-- <script type="text/javascript" src="http://google-maps-utility-library-v3.googlecode.com/svn/tags/markerwithlabel/1.1.9/src/markerwithlabel.js"></script> --%>
    			<script type="text/javascript" src="<%=Configuration.getStaticServerPath("/resources/scripts/plugins/googlemaps/markerwithlabel.js")%>"></script>
    		</s:else>
		</body>
	</html>
	<script type="text/javascript">
	var requiresAuth = true;
	$(document).ready( function() {
		/**
		* Xu ly phan quyen control
		* @author hunglm16
		* @since 01/10/2015
		*/
		_MapControl = new Map();
		var isFP = '<s:property value="fullPrivilege" />';
		if (isFP !=undefined && isFP != null && isFP.toString().trim() === 'true') {
			_isFullPrivilege = true;
		} else {
			_isFullPrivilege = false;
		}
		$('.cmsiscontrol').hide();
		if(!_isFullPrivilege){
			<s:iterator value="mapControl">
				var status = Number('<s:property value="value" />');
				var id = '<s:property value="key" />';
				id = id.trim();
				_MapControl.put(id.trim(), status);
				if (status == undefined || status == null || status == 0) {
					$('.cmsiscontrol[id^="'+id+'"]').remove();
					$("#" + id).remove();
				} else if (status != 1) {
					$("#" + id).show();
					$('.cmsiscontrol[id^="'+id+'"]').show();
					$('.cmsiscontrol[id^="'+id+'"]').attr("cmsByMapControl", "cmsByMapControl");
					if (status == 2) {
						enable(id);
						$('.cmsiscontrol[id^="'+id+'"]').removeAttr('disabled', 'disabled').removeClass('BtnGeneralDStyle');
						$('.cmsiscontrol[id^="'+id+'"] input').each(function() {
						  	$(this).removeAttr('disabled', 'disabled').removeClass('BtnGeneralDStyle');
						});
						$('.cmsiscontrol[id^="'+id+'"] button').each(function() {
						  	$(this).removeAttr('disabled', 'disabled').removeClass('BtnGeneralDStyle');
						});
					} else if(status == 3) {
						disabled(id);
						$('.cmsiscontrol[id^="'+id+'"]').attr('disabled', 'disabled').addClass('BtnGeneralDStyle');
						$('.cmsiscontrol[id^="'+id+'"]').addClass("isNotPointer");
						$('.cmsiscontrol[id^="'+id+'"] input').each(function() {
						  	$(this).attr('disabled', 'disabled').addClass('BtnGeneralDStyle').addClass('isNotPointer');
						});
						$('.cmsiscontrol[id^="'+id+'"] button').each(function() {
						  	$(this).attr('disabled', 'disabled').addClass('BtnGeneralDStyle').addClass('isNotPointer');
						});
					}
				} else if(status == 1) {
					$('.cmsiscontrol[id^="'+id+'"]').attr("cmsByMapControl", "cmsByMapControl");
					$('.cmsiscontrol[id^="'+id+'"]').addClass("isCMSInvisible");
					$('.cmsiscontrol[id^="'+id+'"]').each(function() {
					  	$(this).hide();
					});
				}
			</s:iterator>
			var arrCmsHiden = $('.cmsiscontrol:hidden');
			var size = arrCmsHiden.length;
			for (var i = 0; i < size; i++) {
			  if (!$(arrCmsHiden[i]).attr("cmsByMapControl")) {
				  $(arrCmsHiden[i]).remove();
			  } else {
				  $(arrCmsHiden[i]).removeAttr("cmsByMapControl");
			  }
			}
		} else {
			$('.cmsiscontrol:hidden').show();
		}
		$("#content").css("min-height", $(window).height()-145);
		/*goi ham callback da register*/
		if (Utils._functionCallback) {
			Utils._functionCallback.call(this);
		}

		// set content-type mac dinh cho loi goi ham ajax
		$.ajaxSetup({contentType:"application/x-www-form-urlencoded; charset=utf-8"});
	});
	$(document).ajaxComplete(function(event, xhr, options) {
	    if (xhr.getResponseHeader('REQUIRES_AUTH') == '1') {
	       	//window.location = '/login';
	       	if (requiresAuth) {
	       		hideLoadingIcon();
	       		$.messager.alert('Cảnh báo', 'Phiên đăng nhập của bạn đã hết thời gian.<br/>Vui lòng <a href="/login">đăng nhập</a> lại.', 'warning');
	       		requiresAuth = false;
	       		setTimeout(function() {
	       			requiresAuth = true;
	       		}, 1500);
	       	}
	    };
	});
	</script>
	<script type="text/javascript" src="<%=Configuration.getStaticServerPath("/resources/scripts/plugins/bluebird.min.js")%>"></script>
</s:i18n>

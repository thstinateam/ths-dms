<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<input type="hidden" id="function_code" value="<s:property value="systimestamp"/>" />
<s:set id="downloadToken" value="%{generateReportToken()}"></s:set>
<input type="hidden" id="report_token" value="<s:property value="#downloadToken"/>" />
<s:set id="downloadToken" value="%{putSessionValue(systimestamp, #downloadToken)}"></s:set>
<div class="BreadcrumbSection">
	<ul class="ResetList FixFloat BreadcrumbList">
<%-- 		<li class="Sprite1"><a href="javascript:void(0)"><s:text name="kpi_module_text"/></a></li> --%>
		<li class="Sprite1"><a href="javascript:void(0)"><s:text name="kpi_module_text_ex"/></a></li>
		<li><span><s:text name="aso_fun_set_target_text"/></span>
		</li>
	</ul>
</div>
<div class="CtnOneColSection">
	<div class="ContentSection">
		<div class="ToolBarSection">
			<div class="SearchSection GeneralSSection">
				<h2 class="Title2Style"><s:text name="sale_plan_search_info"/></h2>
				<div class="SearchInSection SProduct1Form">
					<label class="LabelStyle Label1Style"><s:text name="image_manager_unit"/><span class="ReqiureStyle"> *</span></label>
					<div class="BoxSelect ">
						<input  type="text" class="InputTextStyle InputText1Style" style="width: 250px;" id="cbxUnit" maxlength="50" />
					</div>
					
                    <label class="LabelStyle Label1Style"><s:text name="tho.rpt.dm.1.1.menu2"/><span class="ReqiureStyle"> *</span></label>
					<div class="BoxSelect BoxSelect12">
						<input  type="text" style="width:226px;" class="easyui-combobox" id="cbxShop" maxlength="50" />
<!-- 						InputTextStyle InputText1Style -->
					</div>
					<label class="LabelStyle Label11Style"><s:text name="sale_plan_nvbh"/>
                   		<span class="ReqiureStyle"><font color="red"> *</font></span>
                   	</label>
                   	<div class="BoxSelect BoxSelect2">
                        <select class="MySelectBoxClass" id="staffCode" name="staff" onchange="return AsoTarget.changeStaff(activeType.STOPPED);">
                       	</select>
                     </div>
                    <label class="LabelStyle Label11Style"><s:text name="jsp.common.tuyen"/>: </label><label class="LabelStyle"> <font color="black"><span id="routingStaff"></span></font></label>
                    <s:hidden id="routingId" name="routing"></s:hidden>
					<div class="Clear"></div>
					<label class="LabelStyle Label1Style"><s:text name="sale_plan_year"/><span class="ReqiureStyle"> *</span></label>
					<div class="BoxSelect BoxSelect11">                    	
						<select class="MySelectBoxClass" style="width: 115px;" id="yearPeriod" onchange="changeYearAso();">
							<s:iterator value="lstYear">
								<option value='<s:property/>'><s:property/></option>
							</s:iterator>
						</select>
                    </div>
                    <label class="LabelStyle Label1Style" style="margin-left: -28px"><s:text name="sale_plan_period"/><span class="ReqiureStyle"> *</span></label>
					<div class="BoxSelect BoxSelect11">
						<select class="easyui-combobox" style="width: 115px;" id="numPeriod">
<!-- 						MySelectBoxClass   onchange="return changeNumPeriod();"-->
					    </select>
					</div>
					<label class="LabelStyle Label1Style"><s:text name="jsp.common.loai"/><span class="ReqiureStyle"> *</span></label>
					<div class="BoxSelect BoxSelect12">                    	
						<select class="MySelectBoxClass" style="width: 135px;" id="typeAso" >
							<option value="1">SKU</option>
							<option value="2">Ngành hàng con</option>
						</select>
                    </div>
					<div class="Clear"></div>
					<div class="BtnCenterSection">
						<button id="btnSearch" class="BtnGeneralStyle" onclick="return AsoTarget.search();"><s:text name="sale_plan_search"/></button>
					</div>				
					<p id="errMsgSearch" class="ErrorMsgStyle"></p>
				</div>
				
				<h2 class="Title2Style"><s:text name="aso_list_text"/></h2>
				<div class="SearchInSection SProduct1Form">
					<!--Đặt Grid tại đây-->					
					<!-- <div class="GridSection">
						<table id="dg" class="easyui-datagrid"></table>
						<table id="dg"></table>
					</div> -->	 	
					<div id="gridContainer" style="height: 500px; overflow-x: scroll;">
					</div>
							
	                <div class="Clear"></div> 				         
					<div style="padding-top: 19px;">
						<label class="LabelStyle Label2Style"><s:text name="sale_plan_year"/><span class="ReqiureStyle">*</span></label>
						<div class="BoxSelect BoxSelect11">                    	
                            <select class="MySelectBoxClass" style="width: 115px;" id="yearPeriodUnder" onchange="return AsoTarget.initCycleInfoUnder();">
	                            <s:iterator value="lstYear">
									<option value='<s:property/>'><s:property/></option>
								</s:iterator>
                            </select>
                        </div>
                        <label class="LabelStyle Label2Style"><s:text name="sale_plan_period"/><span class="ReqiureStyle">*</span></label>
						<div class="BoxSelect BoxSelect11">
							<select class="easyui-combobox" style="width: 115px;" id="numPeriodUnder">
						    </select>
						</div>
						<label class="LabelStyle Label2Style"><s:text name="sale_plan_nvbh"/></label>
                    	<div class="BoxSelect BoxSelect2">
                            <select class="MySelectBoxClass" id="staffCodeUnder" name="staffUnder" onchange="return AsoTarget.changeStaff(activeType.RUNNING);">
	                        </select>
                        </div>
                        <label class="LabelStyle Label11Style"><s:text name="jsp.common.tuyen"/>: </label><label class="LabelStyle"> <font color="black"><span id="routingStaffUnder"></span></font></label>
                    	<s:hidden id="routingIdUnder" name="routingUnder"></s:hidden>
						<div>
<!-- 						class="BtnCenterSection" -->
							<button id="group_insert_btnSetTargetAso" class="BtnGeneralStyle cmsiscontrol"  onclick="AsoTarget.confirmSaveAso();"><s:text name="kpi_fun_but_set_target"/></button>
							<button id="btnResetTargetAso" class="BtnGeneralStyle cmsiscontrol" style="margin-left: 5px;" onclick="AsoTarget.resetAso();"><s:text name="reset_phan_bo"/></button>
						</div>			                        		
	                </div>	        
	                         
					<div class="GeneralForm GeneralNoTP1Form">
						<div class="Func1Section cmsiscontrol" id="group_insert_import">
							<p class="DownloadSFileStyle DownloadSFile2Style">
								<a href="javascript:void(0)" class="Sprite1" id="downloadTemplate"><s:text name="sale_plan_download_template"/></a>
							</p>
							<div class="DivInputFile">
								<form action="/aso/import-aso" name="importFrm" id="importFrm"  method="post" enctype="multipart/form-data">
									<input type="hidden" name="token" id="tokenImport" value='<s:property value="token"/>'>	
									<input type="file" class="InputFileStyle InputText1Style" size="20" name="excelFile" id="excelFile" onchange="previewImportExcelFile(this,'importFrm');">
									<input type="hidden" id="isView" name="isView">
		            				<input type="hidden" id="excelShopCodeUnder" name="shopCodeUnder">
		            				<s:hidden id="curShopId" name="shopId"></s:hidden>
		            				<s:hidden id="typeId" name="type"></s:hidden>
									<div class="FakeInputFile">
						                <input type="text" id="fakefilepc" readonly="readonly" class="InputTextStyle InputText1Style" />
						            </div>									
								</form>
							</div>
							<button id="btnImport" class="BtnGeneralStyle" onclick="return AsoTarget.openImportConfirm();"><s:text name="sale_plan_import"/></button>
						</div>						
						<button id="btnExport" class="BtnGeneralStyle" onclick="return AsoTarget.exportExcel();"><s:text name="sale_plan_export"/></button>							
					</div>
					<div class="Clear"></div>
					<div id="responseDiv"></div>
					<p id="errExcelMsg" class="ErrorMsgStyle" style="display: none"></p>
					<p id="mainErr" class="ErrorMsgStyle" style="display: none;"></p>
				 	<p class="SuccessMsgStyle" id="successMsg" style="display: none"></p> 
				</div>
				<div class="Clear"></div>
			</div>
			<div class="Clear"></div>
		</div>
	</div>
</div>

<!-- Begin popup import phan bo chi tieu aso -->
<div style="display:none">
	<div id="popupImportAso" class="easyui-dialog" title="Import" data-options="closed:true,modal:true">
		<div class="PopupContentMid">
			<div class="GeneralForm" style="margin-left: 20px;padding-top:0px">
				<div style="margin-bottom: 5px">Bạn có muốn chắc chắn import phân bổ aso? </div>
                <input type="radio" name="importRadio" value="1" id="rbTypeSku" checked="checked">
                <label for="rbTypeSku"> Phân bổ SKU</label><br>
				<input type="radio" name="importRadio" value="2" id="rbTypeSubCat">
				<label for="rbTypeSubCat"> Phân bổ ngành hàng con</label><br>
                <div class="Clear"></div>
			</div>
			<div class="GeneralForm BtnCenterSection" style="padding-top:0px" >
            	<button id="group_edit_btnAcceptImport_aso" class="BtnGeneralStyle" onclick="AsoTarget.importExcel();">Đồng ý</button>
            	<button id="btnCancelImport_aso" class="BtnGeneralStyle" style="margin-left: 8px" onclick="$('#popupImportAso').dialog('close');">Hủy</button>
            </div>
    	</div>
	</div>
</div>
<!-- End popup import phan bo chi tieu aso -->
<s:hidden id="curYearPeriod" name="yearPeriod"></s:hidden>
<s:hidden id="curNumPeriod" name="numPeriod"></s:hidden>
<s:hidden id="shopId" name="shopId"></s:hidden>
<s:hidden id="curShopId" name="shopId"></s:hidden>
<s:hidden id="curTypeAso"></s:hidden>
<s:hidden id="curShopCodeAso"></s:hidden>
<script type="text/javascript">
$(document).ready(function() {
	var curYear = '<s:property value="yearPeriod"/>';
	AsoTarget.setCurrentYear(curYear);
	//load Cay don vi cbx
	ReportUtils.loadComboTree('cbxUnit', 'shopId', $('#curShopId').val(), function(shopId){
		var param = {};
		param.shopId = $('#shopId').val().trim();
// 		Utils.initUnitCbx('cbxShop', param, 230, null, AsoTarget.initCycleInfo);
		Utils.initUnitCbx('cbxShop', param, 230, function (rec) {
			if (rec != null && rec.id != null && rec.shopCode != null) {
				AsoTarget.changeShop(rec.id);
				$('#curShopId').val(rec.id);
	    	}	
		});
	});
	
	yearPeriodChange = function (data) {
		$('#yearPeriodUnder').val($('#yearPeriod').val());
		$('#yearPeriodUnder').change();
		AsoTarget.initCycleInfo();
	};
	AsoTarget.initCycleInfo();
	var ti = setTimeout(function(){
		AsoTarget.search();
	}, 2500);
	// attach file import template
	$('#downloadTemplate').attr('href', excel_template_path + 'catalog/Aso_Plan_Import_Template.xlsx');
});

function changeYearAso() {
	if (yearPeriodChange != undefined && yearPeriodChange != null) {
		yearPeriodChange.call(this,$('#yearPeriod').val());
	}
}
</script>
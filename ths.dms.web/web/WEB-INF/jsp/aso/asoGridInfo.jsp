
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>

<div class="GeneralTable" style="width: 100%; overflow-x: scroll; " id="containTable1">
<!--  overflow-x: scroll; overflow-y: visible; -->
	<table width="100%" border="0" cellspacing="0" cellpadding="0">
	<colgroup>
				<col style="width: 3%;" />
				<col style="width: 8%;" />
				<col style="width: 10%;" />
				<col style="width: 10%;" />
				<col style="width: 10%;" />
				<col style="width: 8%;" />
				<col style="width: 10%;" />
				<col style="width: 8%;" />
				<col style="width: 12%;" />
				<col style="width: 10%;" />
	</colgroup>
	<thead>
		<tr>
			<th class="FirstTdStyle">STT</th>
			<th >Mã NPP</th>
			<th >Tên NPP</th>
			<th>Mã nhân viên</th>
			<th>Tên nhân viên</th>
			<th>Mã tuyến</th>
			<th>Tên tuyến</th>
			<s:if test="type == 1" >
				<th>Mã SKU</th>
				<th>Tên SKU</th>
			</s:if>
			<s:else>
				<th>Mã Ngành hàng con</th>
				<th>Tên Ngành hàng con</th>
			</s:else>
			<th>Chỉ tiêu</th>
		</tr>
		
	</thead>
	<tbody>
		<s:iterator value="lstAsoPlanVO" status="kpiInfo" var="kpiVar">
		<tr>
			<td class="FirstTdStyle" style="text-align:center"><s:property value="#kpiInfo.count" /></td>
			<td style="padding: 0 3px 0 3px"><s:property value="shopCode" /></td>
			<td style="padding: 0 3px 0 3px"><s:property value="shopName" /></td>
			<td style="padding: 0 3px 0 3px"><s:property value="staffCode" /></td>
			<td style="padding: 0 3px 0 3px"><s:property value="staffName" /></td>
			<td style="padding: 0 3px 0 3px"><s:property value="routingCode" /></td>
			<td style="padding: 0 3px 0 3px"><s:property value="routingName" /></td>
			<td style="padding: 0 3px 0 3px"><s:property value="code" /></td>
			<td style="padding: 0 3px 0 3px"><s:property value="name" /></td>
			<td>
			<div style="text-align: center;">
				<input class="InputTextStyle aso" style="text-align: right;width: 91%;" tbType="aso" id='<s:property value="objectId" />' value='<s:property value="formatNumber(plan)" />' />
			</div>
			</td>
		</tr>
		</s:iterator>
	</tbody>
	</table>
</div>
<script type="text/javascript">
$(document).ready(function() {
	<s:iterator var="obj" value="lstAsoPlanVO" status="stt" >
		var obj = {};
		obj.objectId = '<s:property value="#obj.objectId"/>';
		AsoTarget._lstStaffInfo.push(obj);
	</s:iterator>	
	//console.log("length: " + AsoTarget._lstStaffInfo.length)
	$('#curTypeAso').val('<s:property value="type"/>');
	$('#curShopCodeAso').val('<s:property value="shopCode"/>');
	AsoTarget.bindFormatInput();
});
</script>

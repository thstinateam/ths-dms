<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%-- <style type="text/css">
    body { font:16px Calibri;}
    table { border-collapse:separate; border-top: 3px solid grey; }
/*     td { */
/*         margin:0; */
/*         border:3px solid grey; */
/*         border-top-width:0px; */
/*         white-space:nowrap; */
/*     } */
    div.KpiInfo {
        width: 600px;
        overflow-x:scroll;  
        margin-left:5em;
        overflow-y:visible;
        padding-bottom:1px;
    }
    .headcol {
        position:absolute;
        width:5em;
        left:0;
        top:auto;
        border-right: 0px none black;
        border-top-width:3px; /*only relevant for first row*/
        margin-top:-3px; /*compensate for top border*/
    }
   .headcol:before {content: 'Row ';} 
    .long { background:yellow; letter-spacing:1em; }
</style> --%>
<%-- <style type="text/css">
.headcol {
        position:absolute;
        width:5em;
        left:0;
        top:auto;
        border-right: 0px none black;
        border-top-width:3px; /*only relevant for first row*/
        margin-top:-3px; /*compensate for top border*/
    }
   .headcol:before {content: 'Row ';} 
</style> --%>
<input type="hidden" id="function_code" value="<s:property value="systimestamp"/>" />
<s:set id="downloadToken" value="%{generateReportToken()}"></s:set>
<input type="hidden" id="report_token" value="<s:property value="#downloadToken"/>" />
<s:set id="downloadToken" value="%{putSessionValue(systimestamp, #downloadToken)}"></s:set>
<div class="BreadcrumbSection">
	<ul class="ResetList FixFloat BreadcrumbList">
<%-- 		<li class="Sprite1"><a href="javascript:void(0)"><s:text name="kpi_module_text"/></a></li> --%>
		<li class="Sprite1"><a href="javascript:void(0)"><s:text name="kpi_module_text_ex"/></a></li>
		<li><span><s:text name="kpi_fun_set_target_text"/></span>
		</li>
	</ul>
</div>
<div class="CtnOneColSection">
	<div class="ContentSection">
		<div class="ToolBarSection">
			<div class="SearchSection GeneralSSection">
				<h2 class="Title2Style"><s:text name="sale_plan_search_info"/></h2>
				<div class="SearchInSection SProduct1Form">
					<label class="LabelStyle Label1Style"><s:text name="image_manager_unit"/><span class="ReqiureStyle">*</span></label>
					<div class="BoxSelect ">
						<input  type="text" class="InputTextStyle InputText1Style" style="width: 250px;" id="cbxUnit" maxlength="50" />
					</div>
					
                    <label class="LabelStyle Label1Style"><s:text name="tho.rpt.dm.1.1.menu2"/><span class="ReqiureStyle">*</span></label>
					<div class="BoxSelect BoxSelect12">
						<input  type="text" style="width:226px;" class="easyui-combobox" id="cbxShop" maxlength="50" />
<!-- 						InputTextStyle InputText1Style -->
					</div>
<!-- 					<div class="Clear"></div> -->
					<label class="LabelStyle Label1Style"><s:text name="sale_plan_year"/><span class="ReqiureStyle">*</span></label>
					<div class="BoxSelect BoxSelect11">                    	
						<select class="MySelectBoxClass" style="width: 115px;" id="yearPeriod" onchange="changeYear();">
							<s:iterator value="lstYear">
								<option value='<s:property/>'><s:property/></option>
							</s:iterator>
						</select>
                    </div>
					
                    <label class="LabelStyle Label1Style"><s:text name="sale_plan_period"/><span class="ReqiureStyle">*</span></label>
					<div class="BoxSelect BoxSelect11">
						<select class="easyui-combobox" style="width: 115px;" id="numPeriod">
<!-- 						MySelectBoxClass   onchange="return changeNumPeriod();"-->
					    </select>
					</div>
						
					<div class="Clear"></div>
					
					<div class="BtnCenterSection">
						<button id="btnSearch" class="BtnGeneralStyle" onclick="return TargetKpi.search();"><s:text name="sale_plan_search"/></button>
					</div>				
					<p id="errMsgSearch" class="ErrorMsgStyle"></p>
				</div>
				
				<h2 class="Title2Style"><s:text name="kpi_list_text"/></h2>
				<div class="SearchInSection SProduct1Form">
					<!--Đặt Grid tại đây-->					
					<!-- <div class="GridSection">
						<table id="dg" class="easyui-datagrid"></table>
						<table id="dg"></table>
					</div> -->	 	
					<div id="gridContainer">
					
					</div>
							
	                <div class="Clear"></div> 				         
					<div style="padding-top: 19px;">
						<label class="LabelStyle Label1Style"><s:text name="sale_plan_year"/><span class="ReqiureStyle">*</span></label>
						<div class="BoxSelect BoxSelect11">                    	
                            <select class="MySelectBoxClass" style="width: 115px;" id="yearPeriodUnder" onchange="return validatePeriod();">
	                            <s:iterator value="lstYear">
									<option value='<s:property/>'><s:property/></option>
								</s:iterator>
                            </select>
                        </div>
                        <label class="LabelStyle Label1Style"><s:text name="sale_plan_period"/><span class="ReqiureStyle">*</span></label>
						<div class="BoxSelect BoxSelect11">
							<select class="easyui-combobox" style="width: 115px;" id="numPeriodUnder">
<!-- 							onchange="return validatePeriod();" -->
						    </select>
						</div>
						
						<div>
<!-- 						class="BtnCenterSection" -->
							<button id="group_insert_btnSetTarget" class="BtnGeneralStyle cmsiscontrol" style="margin-left: 50px;" onclick="TargetKpi.setKpi();"><s:text name="kpi_fun_but_set_target"/></button>
						</div>			                        		
	                </div>	        
	                         
					<div class="GeneralForm GeneralNoTP1Form">
						<div class="Func1Section cmsiscontrol" id="group_insert_import">
							<p class="DownloadSFileStyle DownloadSFile2Style">
								<a href="javascript:void(0)" class="Sprite1" id="downloadTemplate"  onclick="TargetKpi.downloadTemplate();" ><s:text name="sale_plan_download_template"/></a>
							</p>
							<div class="DivInputFile">
								<form action="/kpi/set-target/import-kpi" name="importFrm" id="importFrm"  method="post" enctype="multipart/form-data">
									<input type="hidden" name="token" id="tokenImport" value='<s:property value="token"/>'>								
									<input type="file" class="InputFileStyle InputText1Style" size="20" name="excelFile" id="excelFile" onchange="previewImportExcelFile(this,'importFrm');">
									<input type="hidden" id="isView" name="isView">
		            				<input type="hidden" id="excelShopCodeUnder" name="shopCodeUnder">
									<div class="FakeInputFile">
						                <input type="text" id="fakefilepc" readonly="readonly" class="InputTextStyle InputText1Style" />
						            </div>									
								</form>
							</div>
							<button id="btnImport" class="BtnGeneralStyle" onclick="return TargetKpi.importExcel();"><s:text name="sale_plan_import"/></button>
						</div>						
						<button id="btnView" class="BtnGeneralStyle" onclick="return TargetKpi.exportExcel();"><s:text name="sale_plan_export"/></button>							
					</div>
					<div class="Clear"></div>
					<div id="responseDiv"></div>
					<p id="errExcelMsg" class="ErrorMsgStyle" style="display: none"></p>
					<p id="mainErr" class="ErrorMsgStyle" style="display: none;"></p>
				 	<p class="SuccessMsgStyle" id="successMsg" style="display: none"></p> 
				</div>
				<div class="Clear"></div>
			</div>
			<div class="Clear"></div>
		</div>
	</div>
</div>
<s:hidden id="curYearPeriod" name="yearPeriod"></s:hidden>
<s:hidden id="curNumPeriod" name="numPeriod"></s:hidden>
<s:hidden id="shopId" name="shopId"></s:hidden>
<s:hidden id="curShopId" name="shopId"></s:hidden>
<script type="text/javascript">
$(document).ready(function() {
	var curYear = '<s:property value="yearPeriod"/>';
	TargetKpi.setCurrentYear(curYear);
	//load Cay don vi cbx
	ReportUtils.loadComboTree('cbxUnit', 'shopId', $('#curShopId').val(), function(shopId){
		var param = {};
		param.shopId = $('#shopId').val().trim();
// 		Utils.initUnitCbx('cbxShop', param, 230, null, TargetKpi.initCycleInfo);
		Utils.initUnitCbx('cbxShop', param, 230, null);
	});
	
	yearPeriodChange = function (data) {
		$('#yearPeriodUnder').val($('#yearPeriod').val());
		$('#yearPeriodUnder').change();
		TargetKpi.initCycleInfo();
	};
	TargetKpi.initCycleInfo();
	var ti = setTimeout(function(){
		TargetKpi.search();
	}, 2500);
});

/* function initCycleInfo() {
	$('#yearPeriod').val('<s:property value="yearPeriod"/>');
	$('#yearPeriod').change();
	
	//code combobox period								
	var html = '';
	for(var i = 1; i < 14; i++){
		html += '<option value="'+ i +'">' + i + '</option>';
	}
	$('#numPeriod').html(html);
	$('#numPeriodUnder').html(html);
	
	var numPeriod = '<s:property value="numPeriod"/>';
	$('#numPeriod').val(numPeriod);
	$('#numPeriod').change();
	$('#numPeriodUnder').val(numPeriod);
	$('#numPeriodUnder').change();
} */

function validatePeriod(){
	TargetKpi.initCycleInfoUnder();
	/* if ($('#yearPeriodUnder').val() != null && $('#numPeriodUnder').val() != null) {
		var curYearPeriod = Number($('#curYearPeriod').val().trim());
		var curNumPeriod = Number($('#curNumPeriod').val().trim());
		var yearPeriodCreate = Number($('#yearPeriodUnder').val().trim());
		var numPeriodCreate = Number($('#numPeriodUnder').val().trim());
		
		if(yearPeriodCreate < curYearPeriod || (yearPeriodCreate == curYearPeriod && numPeriodCreate < curNumPeriod)){
			$('#mainErr').html('<s:text name="sale_plan_mainerr"/>');
			
			$('#btnImport').attr('disabled','disabled');
			$('#btnImport').addClass('BtnGeneralDStyle');
			
			$('#fakefilepc').attr('disabled','disabled');
			$('#fakefilepc').addClass('BtnGeneralDStyle');
			
			$('#execute').attr('disabled','disabled');
			$('#execute').addClass('BtnGeneralDStyle');
			
			$('#reset').attr('disabled','disabled');
			$('#reset').addClass('BtnGeneralDStyle');
		}else{
			$('#btnImport').removeAttr('disabled');
			$('#btnImport').removeClass('BtnGeneralDStyle');
			
			$('#fakefilepc').removeAttr('disabled');
			$('#fakefilepc').removeClass('BtnGeneralDStyle');
			
			$('#execute').removeAttr('disabled');
			$('#execute').removeClass('BtnGeneralDStyle');
			
			$('#reset').removeAttr('disabled');
			$('#reset').removeClass('BtnGeneralDStyle');
		}
	} */
};

function changeShop(){
	$('#shopCodeUnder').val($('#shopCode').val());
	$('#shopCodeUnder').change();
};
function changeNumPeriod(){
	$('#numPeriodUnder').val($('#numPeriod').val());
	$('#numPeriodUnder').change();
};
function changeYear(){
	if (yearPeriodChange != undefined && yearPeriodChange != null) {
		yearPeriodChange.call(this,$('#yearPeriod').val());
	}
}
</script>

<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>

<div class="GeneralTable" style="width: 1280px; overflow-x: scroll;" id="containTable1">
<!--  overflow-x: scroll; overflow-y: visible; -->
	<table width="100%" border="0" cellspacing="0" cellpadding="0">
	<colgroup>
				<col style="width: 200px;" />
				<col style="width: 250px;" />
				<col style="width: 200px;" />
				<col style="width: 250px;" />
				<col style="width: 200px;" />
				<s:iterator value="lstKpi" status="kpi">
					<col style="width: 70px;" />
				</s:iterator>
	</colgroup>
	<thead>
		<tr>
			<th class="FirstTdStyle">Mã NPP</th>
			<th >Tên NPP</th>
			<th>Mã nhân viên</th>
			<th>Tên nhân viên</th>
			<th class="headcol">Chu kỳ</th>
			<s:iterator value="lstKpi" status="kpi">
				<th><s:property value="criteria" /></th>
			</s:iterator>
		</tr>
		
	</thead>
	<tbody>
		<s:iterator value="lstKpiInfo" status="kpiInfo" var="kpiVar">
			<s:if test="%{#kpiInfo.last}" >
				<tr>
					<td class="FirstTdStyle TotalText headcol" colspan="5">Bình quân</td>					
					<s:iterator value="lstKpiData" status="data">
						<td><div id="<s:property value="key" />-avg" class="AlignRCols"><s:property value="formatNumber(value)" /></div></td>
					</s:iterator>
				</tr>
			</s:if>
			<s:else>
				<tr>
					<td class="FirstTdStyle"><s:property value="shopCode" /></td>
					<td ><s:property value="shopName" /></td>
					<td><s:property value="staffCode" /></td>
					<td><s:property value="staffName" /></td>
					<td class="AlignCCols headcol"><s:property value="cycleText" /></td>
					<s:iterator value="lstKpiData" status="data">
						<td>
						<div style="text-align: center;">
							<input class="InputTextStyle kpi" style="text-align: right;width: 100px;" tbType="kpi" st='<s:property value="#kpiVar.staffId" />' kpi='<s:property value="key" />' id='<s:property value="#kpiVar.staffId" />-<s:property value="key" />' value='<s:property value="formatNumber(value)" />' />
						</div>
						</td>
						
<!-- 						class="AlignRCols" -->
					</s:iterator>
				</tr>
			</s:else>
			
		</s:iterator>
	</tbody>
	</table>
</div>
<script type="text/javascript">
$(document).ready(function() {
	<s:iterator var="obj" value="lstKpiInfo" status="stt" >
		var obj = {};
		obj.staffId = '<s:property value="#obj.staffId"/>';
		TargetKpi._lstStaffInfo.push(obj);
	</s:iterator>	
	console.log("length: " + TargetKpi._lstStaffInfo.length)
	
	TargetKpi.bindFormatInput();
});
</script>

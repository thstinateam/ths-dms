<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<div class="Breadcrumbs">
	<span class="Breadcrumbs1Item Sprite1">Quản lý kế hoạch tiêu thụ</span>
</div>
<div class="GeneralMilkBox">
	<div class="GeneralMilkTopBox">
			<div class="GeneralMilkBtmBox">
				<h3 class="Sprite2">
					<span class="Sprite2">Tìm kiếm</span>
				</h3>
				<div class="GeneralMilkInBox ResearchSection">
	           <div class="Consumers1Form">			 
				<label class="LabelStyle Label1Style">Mã đơn vị (F9)</label>
				<input id="searchShop" type="text" class="InputTextStyle InputText1Style" maxlength="50" />				
							
	            <label class="LabelStyle Label2Style">Mã SP (F9)</label>
	            <input id="productCode" type="text" class="InputTextStyle InputText2Style" maxlength="50" />
                
                 
                <label class="LabelStyle Label1Style">Mã NVBH (F9)</label>
				<div class="Field2">
					<input id="saleStaffCode" type="text" class="InputTextStyle InputText1Style" maxlength="50" />
					<span class="RequireStyle">(*)</span>
				</div>
				<div class="Clear"></div>
               <label class="LabelStyle Label1Style">Tháng</label>
               <div class="Field2">
					<input id="searchMonth" type="text" class="InputTextStyle InputText1Style" maxlength="10" /> 
					<span class="RequireStyle">(*)</span>
				</div>                       
             
              
              <div class="Clear"></div>
              <div class="ButtonSection" >
	              	<button id="btnSearch" class="BtnGeneralStyle Sprite2" onclick="return SalePlanMgr.search();" >
	              	<span class="Sprite2">Tìm kiếm</span></button>	              	
	              	<img id="loading" class="LoadingStyle" style="visibility: hidden;" src="/resources/images/loading.gif">
              </div>
              <div class="Clear"></div>
              <p id="errMsg" class="ErrorMsgStyle Sprite1"></p></div>
            </div>
		</div>
	</div>
</div>
<div class="GeneralMilkBox">
	<div class="GeneralMilkTopBox">
		<div class="GeneralMilkBtmBox">
			<h3 class="Sprite2">
				<span class="Sprite2">Thông tin kế hoạch tiêu thụ</span>
			</h3>
			<div class="GeneralMilkInBox">
              	<div class="ResultSection" id="saleMgrSelection">
                  	<p id="gridNoResult" style="display: none" class="WarningResultStyle">Không có kết quả</p>
                  	<table id="grid"></table>
					<div id="pager"></div>
					 
              	</div>              	   
          	</div>		
          	<div class="Clear"></div>
	        <div style="height: 30px;overflow: auto;padding-left: 75%">
	        	<label class="LabelStyle Label3Style" style="font-weight: bold;">Tổng tiền: &nbsp;&nbsp;</label>	       
	        	<label id="_totalAmount" class="LabelStyle Label3Style" style="font-weight: bold;">0</label> 
	        </div>
		</div>
	</div>
</div>
<script type="text/javascript">
$(document).ready(function(){
	$('#errMsg').html('').hide();
// 	applyDateTimePicker("#searchMonth", "mm/yy");
// 	$('#searchMonth').bind('blur',function(){
// 		var txtDate = $('#searchMonth').val().trim();
// 		var aoDate,           // needed for creating array and object
// 	        txtDateTmp;
// 	    aoDate = txtDate.split('/');
// 	    if (aoDate.length >= 3) {
// 	    	txtDateTmp = aoDate[1] +'/'+aoDate[2];
// 	    	if(aoDate[1] != null && aoDate[1] != undefined && aoDate[1] != '' && aoDate[2] != null && aoDate[2] != undefined && aoDate[2] != ''){
// 	    		$('#searchMonth').val(txtDateTmp);
// 	    	}
// 	    }
// 	});
	showMonthAndYear('#searchMonth');
	$("#searchMonth").focus(function () {
        $(".ui-datepicker-calendar").hide();
        $("#ui-datepicker-div").position({
            my: "center top",
            at: "center bottom",
            of: $(this)
        });
    });
	$('#searchShop').focus();
	$('#searchMonth').val(getCurrentMonth());
	$('#productCode').bind('keyup', function(event){
		if(event.keyCode == keyCode_F9){
			CommonSearch.searchProductOnDialog(function(data){
				$('#productCode').val(data.code);
			});
		}
	});
	$('#saleStaffCode').bind('keyup', function(event){
		if(event.keyCode == keyCode_F9){
			$('#errMsg').html('').hide();
			var arrParams = new Array();
			var param = new Object();
			param.name='filterStaff';
			param.value=1;
			arrParams.push(param);
			param = new Object();
			param.name='shopCode';			
			param.value=SalePlanMgr.getShopCode();
			arrParams.push(param);
			CommonSearch.searchSaleStaffOnDialog(function(data){
				$('#saleStaffCode').val(data.code);					
			},arrParams);
		}
	});
	$('#searchShop').live('change',function(){
		$('#errMsg').html('').hide();
		SalePlanMgr.searchShop();
	});
	$('#searchShop').bind('keyup', function(event){
		if(event.keyCode == keyCode_F9){
			var arrParams = new Array();
			var param = new Object();
			param.name='filterShop';
			param.value=1;
			arrParams.push(param);
			CommonSearch.searchShopOnDialog(function(data){
				$('#searchShop').val(Utils.XSSEncode(data.code + '-' + data.name));				
			},arrParams);
		}
	});
	Utils.bindAutoSearch();
	$("#grid").jqGrid({
		  url:SalePlanMgr.initURL(),
		  colModel:[		
		    {name:'shop.shopCode', label: 'Mã Đơn vị', width: 100, sortable:false,resizable:false , align: 'left', formatter:function(cellvalue, options, rowObject){
	        	  return Utils.XSSEncode(rowObject.shop.shopCode);
	          }},
		    {name:'staff.staffCode', label: 'Mã NVBH', width: 100, sortable:false,resizable:false , align: 'left', formatter:function(cellvalue, options, rowObject){
	        	  return Utils.XSSEncode(rowObject.staff.staffCode);
	          }},
		    {name:'staff.staffName', label: 'Tên NVBH', width: 150, align: 'left', sortable:false,resizable:false, formatter:function(cellvalue, options, rowObject){
	        	  return Utils.XSSEncode(rowObject.staff.staffName);
	          } },
		    {name:'product.productCode', label: 'Mã SP', width: 120, align: 'left',sortable:false,resizable:false , formatter:function(cellvalue, options, rowObject){
	        	  return Utils.XSSEncode(rowObject.product.productCode);
	          }},
		    {name:'quantity', label: 'Số lượng', width: 80, align: 'right',sortable:false,resizable:false,formatter:function(cellvalue, options, rowObject){
		    	return formatCurrency(rowObject.quantity);
		    }},
		    {name:'amount', label: 'Tổng tiền', width: 80, align: 'right',sortable:false,resizable:false,formatter:function(cellvalue, options, rowObject){
		    	return formatCurrency(rowObject.amount);
		    }},
		    {name:'', label: 'Kế hoạch tháng', width: 80, align: 'right',sortable:false,resizable:false,formatter:function(cellvalue, options, rowObject){
		    	return $('#searchMonth').val().trim();
		    }}
		  ],	  
		  pager : '#pager',
		  height: 'auto',
		  rownumbers: true,	  
		  width: ($('#saleMgrSelection').width()),
		  gridComplete: function(){	
			  updateRownumWidthForJqGrid();
		  }	 
		}).navGrid('#pager', {edit:false,add:false,del:false, search: false});
		$('#jqgh_grid_rn').prepend('STT');
	
});

</script>
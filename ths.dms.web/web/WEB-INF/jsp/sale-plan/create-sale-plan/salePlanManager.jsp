<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<input type="hidden" id="function_code" value="<s:property value="systimestamp"/>" />
<s:set id="downloadToken" value="%{generateReportToken()}"></s:set>
<input type="hidden" id="report_token" value="<s:property value="#downloadToken"/>" />
<s:set id="downloadToken" value="%{putSessionValue(systimestamp, #downloadToken)}"></s:set>

<div class="BreadcrumbSection">
    <ul class="ResetList FixFloat BreadcrumbList">
		<li class="Sprite1"><a href="#"><s:text name="sale_plan_title_ex"/></a></li>
       	<li><span><s:text name="sale_plan_title_list_ex"/></span></li>
    </ul>
</div>
<div class="CtnOneColSection">
	<div class="ContentSection">
		<div class="ToolBarSection">
	        <div class="SearchSection GeneralSSection">
	        	<h2 class="Title2Style"><s:text name="sale_plan_search_info"/></h2>
            		<div class="SearchInSection SProduct1Form">
            			<label class="LabelStyle Label1Style"><s:text name="sale_plan_unit"/>
                        	<span class="ReqiureStyle"><font color="red"> *</font></span>
                        </label>
						<div class="BoxSelect BoxSelect2">
							<div class="Field2">
								<input id="shop" style="width:200px;">
							</div>
						</div>
                    	<label class="LabelStyle Label1Style"><s:text name="sale_plan_nvbh"/>
                    		<span class="ReqiureStyle"><font color="red"> *</font></span>
                    	</label>
                    	<div class="BoxSelect BoxSelect2">
                            <select class="MySelectBoxClass" id="staffCode" name="staff" onchange="return CreateSalePlan.changeStaff();">
	                        </select>
                        </div>
						<label class="LabelStyle Label1Style"><s:text name="sale_plan_year"/><span class="ReqiureStyle"> *</span></label>
						<div class="BoxSelect BoxSelect2">                    	
							<tiles:insertTemplate template="/WEB-INF/jsp/general/yearPeriod.jsp" />
                        </div>
                        
						<div class="Clear"></div>
						
						<label class="LabelStyle Label1Style"><s:text name="sale_plan_branch"/></label>
						<div class="BoxSelect BoxSelect2" style="width:206px;">
							<select id="category" multiple="multiple">
						    </select>
						</div>
						
						<label class="LabelStyle Label1Style"><s:text name="sale_plan_product_f9"/></label>
                        <input id="productCode" type="text" class="InputTextStyle InputText1Style" maxlength="10"/>
                        <label class="LabelStyle Label1Style"><s:text name="sale_plan_period"/><span class="ReqiureStyle"> *</span></label>
						<div class="BoxSelect BoxSelect2">
							<select id="numPeriod" onchange="return CreateSalePlan.changeNumPeriod();">
						    </select>
						</div>				
						
                        <div class="Clear"></div>
                        <div class="BtnCenterSection">
                       		<button id="btnSearch" class="BtnGeneralStyle" onclick="return CreateSalePlan.searchForGSNPP();"><span class="Sprite2"><s:text name="sale_plan_search"/></span></button>
              				<img id="loading" class="LoadingStyle" style="display: none;" src="/resources/images/loading.gif">
                        </div>
                        <p class="ErrorMsgStyle" id="errMsg" style="display: none;"><s:text name="sale_plan_error_update"/></p>
                        <div class="Clear"></div>
					</div>
					<div id="staffTitle"><h2 id="title" class="Title2Style"><s:text name="sale_plan_grid_title"/></h2></div>
		        	 <div class="SearchInSection SProduct1Form">
		        		<div class="GridSection" id="salePlanContainerGrid">
	          				<table id="salePlanGrid"></table>
		          		</div>
		          		<label class="LabelStyle Label1Style"><s:text name="sale_plan_nvbh"/></label>
                    	<div class="BoxSelect BoxSelect2">
                            <select class="MySelectBoxClass" id="staffCodeUnder" name="staff">
	                        </select>
                        </div>
						<label class="LabelStyle Label1Style"><s:text name="sale_plan_year"/><span class="ReqiureStyle"> *</span></label>
						<div class="BoxSelect BoxSelect2">                    	
                            <select class="MySelectBoxClass" id="yearPeriodUnder" onchange="return CreateSalePlan.yearPeriodUnderChange();">
	                            <s:iterator value="lstYear">
									<option value='<s:property/>'><s:property/></option>
								</s:iterator>
                            </select>
                        </div>
                        <label class="LabelStyle Label1Style"><s:text name="sale_plan_period"/><span class="ReqiureStyle"> *</span></label>
						<div class="BoxSelect BoxSelect2">
							<select id="numPeriodUnder" onchange="return CreateSalePlan.validatePeriod();">
						    </select>
						</div>
                        
		            	<div class="ButtonSection cmsiscontrol" id="group_insert_btnDistribute_idDiv" style="float: right; margin-right: 20px;">
		            		<button id="group_insert_btnDistribute" class="BtnGeneralStyle BtnMSection" onclick="return CreateSalePlan.distributeProductNew();"><span class="Sprite2"><s:text name="sale_plan_allocate"/></span></button>
		            		<button id="btnReset" class="BtnGeneralStyle" onclick="return CreateSalePlan.resetAll();"><span class="Sprite2"><s:text name="sale_plan_reset_data"/></span></button>
		        		</div>
		        		<img id="loadingCreate" class="LoadingStyle" style="visibility: hidden;" src="/resources/images/loading.gif">
		        		<div class="Clear"></div>
		        		<div class="GeneralForm GeneralNoTP1Form">
							<div class="Func1Section cmsiscontrol" id="group_insert_import">
								<p class="DownloadSFileStyle DownloadSFile2Style">
									<a id="downloadTemplate" href="javascript:void(0)" onclick="CreateSalePlan.downloadImportSalePlanTemplateFile()"><s:text name="sale_plan_download_template"/></a> 
								</p>		
								<div class="DivInputFile">
									<form action="/sale-plan/create/importexcelfile" name="importFrm" id="importFrm"  method="post" enctype="multipart/form-data">
							            <input type="hidden" name="token" id="tokenImport" value='<s:property value="token"/>'>
							            <input type="hidden" id="isView" name="isView"/>
							            <input type="hidden" id="curShopId" value="39" name="shopId">
							            <input type="file" class="InputFileStyle" size="20" name="excelFile" id="excelFile" onchange="previewImportExcelFile(this,'importFrm');">
							            <div class="FakeInputFile">
							                <input id="fakefilepc" type="text" readonly="readonly" class="InputTextStyle InputText1Style">
							            </div>
									</form>
							    </div>
						        <button id="btnImport" class="BtnGeneralStyle" onclick="return CreateSalePlan.importExcel();"><s:text name="sale_plan_import"/></button>
							</div>
							<button id="btnExport" class="BtnGeneralStyle" onclick="return CreateSalePlan.exportExcel();"><s:text name="sale_plan_export"/></button>
						    <div class="Clear"></div>
						</div>
		        		<p id="errExcelMsg" class="ErrorMsgStyle" style="display: none"></p>
		          		<p id="mainErr" class="ErrorMsgStyle" style="display: none;"></p>
		          		<tiles:insertTemplate template="/WEB-INF/jsp/general/successMsg.jsp" />
		         </div>
            </div>
		</div>
    </div>
</div>
<div id="responseDiv" style="display:none;"></div>
<s:hidden id="totalQuantity" name="totalQuantity"></s:hidden>
<s:hidden id="totalMoney" name="totalMoney"></s:hidden>
<s:hidden id="listSize" name="listSize"></s:hidden>
<s:hidden id="shopCodeHidden" name="shop.shopCode"></s:hidden>
<input type="hidden" id="shopId" autocomplete="off" />
<s:hidden id="curYearPeriod" name="yearPeriod"></s:hidden>
<s:hidden id="curNumPeriod" name="numPeriod"></s:hidden>
<s:hidden id="curShopId" name="shopId"></s:hidden>
<script type="text/javascript">
$(document).ready(function(){
	yearPeriodChange = function (data) {
		CreateSalePlan._isYearPeriodChange = true;
		$('#yearPeriodUnder').val($('#yearPeriod').val());
		$('#yearPeriodUnder').change();
		CreateSalePlan.initCycleInfo();
	};
	$('#yearPeriod').val('<s:property value="yearPeriod"/>');
	$('#yearPeriod').change();
	//code combobox period								
	$('#staffCode').focus();
	$('#staffCodeUnder').val($('#staffCode').val());
	$('#staffCodeUnder').change();
	$('#productCode').bind('keyup', function(event){
		if(event.keyCode == keyCode_F9){
			var catCodes = '';
			if ($('#category').val() != undefined && $('#category').val() != null){
				catCodes = $('#category').val().toString();
			}
			var txt = "productCode";
			VCommonJS.showDialogSearch2({
				params : {
					catCode : catCodes,
					status : 1
				},
				inputs : [
			        {id:'code', maxlength:50, label:'<s:text name="sale_plan_product"/>'},
			        {id:'name', maxlength:250, label:'<s:text name="sale_plan_productname"/>'},
			    ],
			    url : '/commons/product-searchAllProduct',
			    columns : [[
					{field:'categoryCode', title:'<s:text name="sale_plan_branch"/>', align:'left', width:110, sortable : false, resizable : false,
						formatter:function(val, row) {
							return Utils.XSSEncode(val);
						}
					},
					{field:'productCode', title:'<s:text name="sale_plan_product"/>', align:'left', width:110, sortable : false, resizable : false,
						formatter:function(val, row) {
							return Utils.XSSEncode(val);
						}
					},  
					{field:'productName',title:'<s:text name="sale_plan_productname"/>',align:'left', width:150, sortable : false,resizable : false,
						formatter:function(val, row) {
							return Utils.XSSEncode(val);
						}
					},
			        {field:'choose', title:'', align:'center', width:40, sortable:false, resizable:false, formatter:function(value, row, index) {
			            return '<a href="javascript:void(0)" onclick="Utils.fillCodeForInputTextByDialogSearch2(\''+txt+'\', ' + row.id +', \''+Utils.XSSEncode(row.productCode)+'\', \''+Utils.XSSEncode(row.productName)+'\' ,'+Utils._isShowInputTextByDlSearch2.code+');"><s:text name="sale_plan_choose"/></a>';         
			        }}
			    ]]
			});
		}
	});
	
	//load combobox don vi
	Utils.initUnitCbx('shop', null, 206, function(rec) {
    	if (rec != null && rec.shopCode != ReportUtils._currentShopCode) {
    		CreateSalePlan.changeShop(rec.id);
			$('#curShopId').val(rec.id);
			ReportUtils._currentShopCode = rec.shopCode;
    	}
	}, function(arr) {
       	if (arr != null && arr.length > 0) {
       		ReportUtils._currentShopCode = arr[0].shopCode;
       	}
	});
	
	//code combo multiselect
	CreateSalePlan.lstCTTB = new Array();
	CreateSalePlan.lstCTTB_temp = new Array();
	setSelectBoxValue('category',-1);
	$.getJSON('/sale-plan/create/get-list-cat', function(lstCategory){
		$('#category').removeAttr('disabled');
		var html = '';
		if(lstCategory.length >= 2){
			html += '<option value="-1"><s:text name="sale_plan_all"/></option>';
			for(var i=0; i < lstCategory.length; i++){
				CreateSalePlan.lstCTTB.push(lstCategory[i].productInfoCode);
				CreateSalePlan.lstCTTB_temp.push(lstCategory[i].productInfoCode);
				html += '<option value="'+ Utils.XSSEncode(lstCategory[i].productInfoCode) +'">' +
				Utils.XSSEncode(lstCategory[i].productInfoCode) + ' - ' + Utils.XSSEncode(lstCategory[i].productInfoName) + '</option>';  
			}
			$('#category').html(html);
			setSelectBoxValue('category',-1);
			$('#ddcl-category').remove();
			$('#ddcl-category-ddw').remove();
			CreateSalePlan.lstCTTB_temp = CreateSalePlan.lstCTTB;
			$('#category').dropdownchecklist({ 
				emptyText:'Chọn ngành hàng',
				firstItemChecksAll: true,
				maxDropHeight: 350,
				onItemClick:function(checkbox, selector){
					var value = checkbox.context.defaultValue;
					if (checkbox.context.checked) {
						if ( value == -1){
							var i = 0;
							CreateSalePlan.lstCTTB = new Array();
							$('#ddcl-category-ddw input[type=checkbox]').each(function(){
								var valueTmp = ($('#ddcl-category-ddw #ddcl-category-i'+i).val());
								if(valueTmp != -1) CreateSalePlan.lstCTTB.push(valueTmp);
					            i++;
				            });
						}else{
							CreateSalePlan.lstCTTB.push(value);									
						}
					}
					else{
						if (value == -1){
							CreateSalePlan.lstCTTB = new Array();
						}else{
							var index = CreateSalePlan.lstCTTB.indexOf(value);
							if (index!=-1) CreateSalePlan.lstCTTB.splice(index,1);	
						}
					}
				}
			});
		}  else if(lstCategory.length ==1 ) {
			CreateSalePlan.lstCTTB.push(lstCategory[0].productInfoCode);
			html += '<option value="'+ Utils.XSSEncode(lstCategory[0].productInfoCode) +'">' +
			Utils.XSSEncode(lstCategory[0].productInfoCode) + ' - ' + Utils.XSSEncode(lstCategory[0].productInfoName) + '</option>';  
			$('#category').html(html);
			setSelectBoxValue('category', Utils.XSSEncode(lstCategory[0].productInfoCode));
		} else {
			html += '<option value="-2"></option>';
			$('#category').html(html);
			setSelectBoxValue('category',-2);
			$('#category').attr('disabled','disabled');
		}
		$('.ui-dropdownchecklist-text').css({"padding-left":"5px"});
	});	
	
	//grid search staff
	$('#salePlanGrid').datagrid({
		url : "/sale-plan/create/search-new",
		autoRowHeight : true,
		rownumbers : true, 
		checkOnSelect :true,
		scrollbarSize:20,
		pageSize:50,
		autoWidth: true,
		view: bufferview,
		idField:'productId',
		singleSelect:true,
		queryParams: {
	    },
        fitColumns : true,
        showFooter : true,
        width : $(window).width()-50,
        height : 350,
	    columns:[[
		    {field:'productCode', title: '<s:text name="sale_plan_sp"/>', width:120,sortable:false,resizable:false,align: 'left', formatter:function(value, row, index) {
	        	  return Utils.XSSEncode(value);
	        }},
		    {field:'productName', title: '<s:text name="sale_plan_productname"/>', width:300,sortable:false,resizable:false ,align: 'left', formatter:function(value, row, index) {
		    	if(row.amount!=null && new String(row.amount).indexOf('_')>=0){
		    		return value;
		    	} else {
		    		return Utils.XSSEncode(value);		    		
		    	}
	        }},
		    {field:'staffQuantity', title: '<s:text name="sale_plan_quantity"/>', width:190,sortable:false,resizable:false ,align: 'right',formatter:function(value,row,index){
		    	if( value != null && new String(value).indexOf('_') >= 0) {
   				 	return '<b>' + formatCurrency(new String(value).split('_')[0]); + '</b>';
				}	  
		    	if (row.staffQuantity1 == null) { 
		    		row.staffQuantity1 = '';
		    	}
		    	return '<input reset="0" old-quantity="' + row.staffQuantity1 + '" maxlength="8" index="' + index + '" style="text-align: right; width: 118px; margin: 0;" id="'+row.productCode+'" type="text" class="InputTextStyle InputText1Style skuItem" value="' + formatCurrency(row.staffQuantity1) + '" onkeypress="NextAndPrevTextField(event,this,\'skuItem\')" onblur="return CreateSalePlan.qtyChanged(this);" />';;
		    }},
		    {field:'price', title: '<s:text name="sale_plan_giaban"/>', align: 'right',width:180,sortable:false,resizable:false ,
		    		align: 'right',formatter:function(value,row,index){
			    		return formatCurrency(value);	
			    	}
		    },
		    {field:'amount', title: '<s:text name="sale_plan_amount"/>', width: 170, align: 'right', sortable:false,resizable:false ,align: 'right', formatter:function(value,row,index){
		    	 if (value != null && new String(value).indexOf('_') >= 0) {
    				 return '<b>' + formatCurrency(new String(value).split('_')[0]); + '</b>';
				 } else if (value == null) {
					 return '<input maxlength="20" index="' + index + '" old-amount="" style="text-align: right; width: 118px; margin: 0;" id="amount' + index + '" type="text" class="InputTextStyle InputText1Style skuAmount" value="" onblur="return CreateSalePlan.amtChanged(this);" />';
				 }
		    	 return '<input maxlength="20" index="' + index + '" old-amount="' + row.amount + '" style="text-align: right; width: 118px; margin: 0;" id="amount' + index + '" type="text" class="InputTextStyle InputText1Style skuAmount" value="' + formatCurrency(row.amount) + '" onblur="return CreateSalePlan.amtChanged(this);" />';
			}},
		    {field: 'P',hidden: true},
		    {field:'productId',hidden:true}
	    ]],	
        onAfterEdit:function(rowIndex, rowData, changes) {
        },
	    onLoadSuccess :function(data){	 
	    	$('.datagrid-pager').html('').hide();
	    	Utils.bindFormatOnTextfieldInputCss('skuItem', Utils._TF_NUMBER);
	    	Utils.bindFormatOnTextfieldInputCss('skuAmount', Utils._TF_NUMBER_DOT);
	    	$('#loading').hide();
	    	$('.datagrid-header-rownumber').html('<s:text name="sale_plan_stt"/>');
			var totalQuantity = 0;
			var totalAmount = 0;
			
			var rows = $('#salePlanGrid').datagrid('getRows')[0];
			totalQuantity = rows.totalQuantity + CreateSalePlan._quantityPlan;
			totalAmount = rows.totalAmount + CreateSalePlan._amountPlan;
			
	    	if (data != null) {
				$('#salePlanGrid').datagrid('reloadFooter',[
				 	{staffCode:"_",month:"_",productName: '<b><s:text name="sale_plan_tongcong"/></b>',staffQuantity: totalQuantity + "_" , amount: totalAmount + "_"},
				 ]);
			}else{
				$('#salePlanGrid').datagrid('reloadFooter',[
					{staffCode:"_",month:"_",productName: '<b><s:text name="sale_plan_tongcong"/></b>',staffQuantity: 0 + "_", amount: 0 + "_"},
				 ]);
			}
	    	var staffCodeTitle = $('#staffCode option:selected').text();
	    	if (staffCodeTitle == '<s:text name="sale_plan_choose_staff"/>') {
	    		$('#title').html('<s:text name="sale_plan_product_list"/>');
	    	}
	    	$('#title').html('<s:text name="sale_plan_product_list"/> (<s:text name="sale_plan_nvbh"/> : '+ staffCodeTitle +') ');
	    	$('.datagrid-header-row td div').css('text-align','center');
	    	updateRownumWidthForDataGrid('#salePlanContainerGrid');
	    },
		method : 'GET'
 	});
	$('#salePlanGrid').datagrid('reloadFooter',[
		{staffCode:"_",month:"_",productName: '<b><s:text name="sale_plan_tongcong"/></b>',staffQuantity: 0 + "_" , amount: 0 + "_"},
	 ]);
	CreateSalePlan._skuMap = new Map();
		
// 	changeDateCreate();
	$('#staffCode').live('change',function(){
		$('#staffCodeUnder').val($(this).val().trim());
	});
	$('.MySelectBoxClass').css('width','206');
	$('.CustomStyleSelectBox').css('width','173');
});


</script>

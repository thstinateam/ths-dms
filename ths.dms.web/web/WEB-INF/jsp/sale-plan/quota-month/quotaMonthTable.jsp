<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@taglib prefix="k" uri="/kryptone"%>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
	<colgroup>
		<col style="width: 50px;" />
		<col style="width: 100px;" />
		<col style="width: 100px;" />
		<col style="width: 250px;" />
		<col style="width: 100px;" />
		<col style="width: 100px;" />
		<col style="width: 100px;" />
		<col style="width: 100px;" />			
	</colgroup>
	<thead>
		<tr>
			<th class="ColsThFirst">STT</th>
			<th>Mã đơn vị</th>
			<th>Mã SP</th>
			<th>Tên sản phẩm</th>
			<th>Tháng</th>
			<th>Số lượng</th>
			<th>Giá bán</th>
			<th class="ColsThEnd">Thành tiền</th>				
		</tr>
	</thead>
	<tbody id="">
		<s:if test="salesPlans!=null">
			<k:repeater value="salesPlans" status="status">
				<k:itemTemplate>
					<tr id="ruleItem_<s:property value="#status.index+1"/>" class="RuleItemClass" data="<s:property value="#status.index+1"/>">						
						<td class="AlignCCols" style="border-left: 1px solid #C6D5DB;"><s:property value="#status.index+1" />
							<s:hidden id="stt" name="#status.index+1"></s:hidden>
							<s:set name="tmp" value="quantity"></s:set>
							<input class="spProductId" id="productId_<s:property value="#status.index+1"/>" value="<s:property value="productId"/>" type="hidden" />
						</td>
						<td class="AlignLCols"><s:property value="productCode" /></td>
						<td class="AlignLCols"><s:property value="productCode" /></td>
						<td class="AlignLCols"><s:property value="productName" /></td>
						<td class="AlignCCols"></td>
						<td class="ColsTd5" style="padding:0px 3px;">
							<input id="<s:property value="productCode"/>" type="text" class="InputTextStyle InputText1Style skuItem"
								value="<s:property value="quantity"/>" onblur="return QuotaMonthPlan.qtyChanged(this);" /></td>
						<td class="AlignRCols"><s:property value="price" /></td>
						<td class="AlignRCols"><s:property value="amount" /></td>						
					</tr>
				</k:itemTemplate>
			</k:repeater>
		</s:if>
		<s:else>
			<tr>
				<td colspan="8" style="border-left: 1px solid #C6D5DB;">
				    			<p class="NotData" style="padding:5px;font-weight: normal;">Không có dữ liệu nào</p></td>
			</tr>
		</s:else>
	</tbody>
	<tfoot>
		<tr>
			<td class="AlignCCols" style="border-left: 1px solid #C6D5DB;">&nbsp;</td>			
			<td class="AlignCCols" colspan="3">Tổng cộng</td>			
			<td class="ColsTd5">&nbsp;</td>
			<td class="ColsTd6">&nbsp;</td>
			<td class="AlignRCols"><p id="total"></p></td>
			<td class="AlignRCols ColsTdEnd">0</td>			
		</tr>
	</tfoot>
</table>
<%-- <div class="CPurchase11Form"  id="divTable">
      <div class="GeneralTable Table19Section">
          <div id="quotaMonthTable" class="ScrollSection">
              <div class="BoxGeneralTTitle">
                  <table width="100%" border="0" cellspacing="0" cellpadding="0">
                      	<colgroup>
                          <col style="width:50px;" />
	                      <col style="width:100px;" />
	                      <col style="width:134px;" />
	                      <col style="width:100px;" />
	                      <col style="width:100px;" />
	                      <col style="width:150px;" />
	                      <col style="width:130px;" />
	                      <col style="width:110px;" />
	                      <col style="width:50px;" />
                  		</colgroup>
	                  	<thead>
	                      <tr>
	                      		  <th class="ColsThFirst">STT</th>
		                          <th>Mã SP</th>
		                          <th>Tên SP</th>
		                          <th>Ngành hàng</th>
		                          <th>Giá</th>
		                          <th>Còn lại/Chỉ tiêu cấp trên</th>
		                          <th>Số lượng kế hoạch</th>
		                          <th>Thành tiền</th>
		                          <th class="ColsThEnd">&nbsp;</th>
	                      </tr>
                 	 	</thead>
              	</table>
          </div>
          <div class="BoxGeneralTBody">
              <div id="tbContent" class="ScrollBodySection">
                  <table width="100%" border="0" cellspacing="0" cellpadding="0">
                      <colgroup>
                          <col style="width:50px;" />
                          <col style="width:100px;" />
                          <col style="width:134px;" />
                          <col style="width:100px;" />
                          <col style="width:100px;" />
                          <col style="width:150px;" />
                          <col style="width:130px;" />
                          <col style="width:110px;" />
                          <col style="width:50px;" />
                      </colgroup>
                      <tbody id="">
                          	<s:if test="salesPlans!=null">
								<k:repeater value="salesPlans" status="status">
									<k:itemTemplate>
										<tr id="ruleItem_<s:property value="#status.index+1"/>" class="RuleItemClass" data="<s:property value="#status.index+1"/>">
											<s:hidden id="stt" name="#status.index+1"></s:hidden>
											<s:set name="tmp" value="quantity"></s:set>
											<input class="spProductId" id="productId_<s:property value="#status.index+1"/>" 
												value="<s:property value="productId"/>" type="hidden" />
			                          		<td class="ColsTd1"><s:property value="#status.index+1"/></td>
			                              	<td class="ColsTd2"><s:property value="productCode"/></td>
			                              	<td class="ColsTd3"><s:property value="productName"/></td>
			                              	<td class="ColsTd4"><s:property value="product.cat.productInfoName"/></td>
			                              	<td class="ColsTd5"><s:property value="price"/></td>
			                             	<td class="ColsTd6"><s:property value="#tmp"/>/<s:property value="quantity"/></td>
			                              	<td class="ColsTd7"><input id="<s:property value="productCode"/>" type="text" class="InputTextStyle InputText1Style skuItem" value="<s:property value="quantity"/>" onblur="return QuotaMonthPlan.qtyChanged(this);"/></td>
			                              	<td class="ColsTd8"><s:property value=""/></td>
			                              	<td class="ColsTd9 ColsTdEnd"><a href="javascript:void(0);" onclick="return CreateSalePlan.deleteRow(<s:property value="#status.index+1"/>);"><img src="/resources/images/icon-delete.png" width="19" height="20" /></a></td>
			                          	</tr>
									</k:itemTemplate>
								</k:repeater>							
							</s:if>
							<s:else>
								<p class="NotData">Không có dữ liệu nào</p>
							</s:else>							
                      </tbody>
                  </table>
              </div>
          </div>
          <div class="BoxGeneralTFooter">
				<table width="100%" border="0" cellspacing="0" cellpadding="0">
					<colgroup>
						<col style="width:50px;" />
                        <col style="width:100px;" />
                        <col style="width:134px;" />
                        <col style="width:100px;" />
                        <col style="width:100px;" />
                        <col style="width:150px;" />
                        <col style="width:130px;" />
                        <col style="width:110px;" />
                        <col style="width:50px;" />
					</colgroup>
					<tfoot>
						<tr>
							<td class="ColsTd2">&nbsp;</td>
                           	<td class="ColsTd2">&nbsp;</td>
                           	<td class="ColsTd3">&nbsp;</td>
                           	<td class="ColsTd4"><b>Tổng</b></td>
                           	<td class="ColsTd5">&nbsp;</td>
                          	<td class="ColsTd6">&nbsp;</td>
                           	<td class="ColsTd7"><p id="total" style="text-align: right"></p></td>
                           	<td class="ColsTd8">&nbsp;</td>
                           	<td class="ColsTd9 ColsTdEnd">&nbsp;</td>
						</tr>
					</tfoot>
				</table>
			</div>                                                                            
      </div>        
  </div>
</div> --%>
<script type="text/javascript">
	$(function(){
		var total = 0;
		$('.skuItem').each(function(){
			total+=parseInt($(this).val(), 0);
		});
		$('#total').html(formatCurrency(total));
	});
	</script>
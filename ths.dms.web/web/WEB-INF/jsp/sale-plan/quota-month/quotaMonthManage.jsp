<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<input type="hidden" id="function_code" value="<s:property value="systimestamp"/>" />
<s:set id="downloadToken" value="%{generateReportToken()}"></s:set>
<input type="hidden" id="report_token" value="<s:property value="#downloadToken"/>" />
<s:set id="downloadToken" value="%{putSessionValue(systimestamp, #downloadToken)}"></s:set>
<div class="BreadcrumbSection">
	<ul class="ResetList FixFloat BreadcrumbList">
		<li class="Sprite1"><a href="javascript:void(0)"><s:text name="sale_plan_title_ex"/></a></li>
		<li><span><s:text name="sale_plan_title_unit_ex"/></span>
		</li>
	</ul>
</div>
<div class="CtnOneColSection">
	<div class="ContentSection">
		<div class="ToolBarSection">
			<div class="SearchSection GeneralSSection">
				<h2 class="Title2Style"><s:text name="sale_plan_search_info"/></h2>
				<div class="SearchInSection SProduct1Form">
					<label class="LabelStyle Label1Style"><s:text name="sale_plan_unit_f9"/><span class="ReqiureStyle">*</span></label>
					<input  type="text" class="InputTextStyle InputText1Style" id="shopCode" maxlength="50" onchange="return QuotaMonthPlan.changeShop();" />
					<label class="LabelStyle Label1Style"><s:text name="sale_plan_year"/><span class="ReqiureStyle">*</span></label>
					<div class="BoxSelect BoxSelect2">                    	
						<tiles:insertTemplate template="/WEB-INF/jsp/general/yearPeriod.jsp" />
                    </div>
                    <label class="LabelStyle Label1Style"><s:text name="sale_plan_period"/><span class="ReqiureStyle">*</span></label>
					<div class="BoxSelect BoxSelect2">
						<select id="numPeriod">
					    </select>
					</div>
					<div class="Clear"></div>
					<label class="LabelStyle Label1Style"><s:text name="sale_plan_branch"/></label>
					<div class="BoxSelect BoxSelect2" style="width: 206px;">
						<select id="category" class="" multiple="multiple" name="LevelSchool">
							<option value="-1" selected="selected"><s:text name="sale_plan_all"/></option>							
							<s:iterator value="lstCategoryType">
								<option value='<s:property value="productInfoCode"/>'>&nbsp;<s:property value="productInfoCode"/> - <s:property value="productInfoName"/></option>
							</s:iterator>		    
				    	</select>
					</div>
					<label class="LabelStyle Label1Style"><s:text name="sale_plan_product_f9"/></label>					
					<input type="text" class="InputTextStyle InputText1Style" id="productId" maxlength="50" />
					<div class="Clear"></div>
					<div class="BtnCenterSection">
						<button id="btnSearch" class="BtnGeneralStyle" onclick="return QuotaMonthPlan.search();"><s:text name="sale_plan_search"/></button>
					</div>				
					<p id="errMsgSearch" class="ErrorMsgStyle"></p>
				</div>
				<h2 class="Title2Style"><s:text name="sale_plan_product_list"/></h2>
				<div class="SearchInSection SProduct1Form">
					<!--Đặt Grid tại đây-->					
					<div class="GridSection">
						<table id="dg"></table>
					</div>	 			
	                <div class="Clear"></div> 				         
					<div style="padding-top: 19px;">
						<label class="LabelStyle Label1Style"><s:text name="sale_plan_unit_f9"/><span class="ReqiureStyle">*</span></label>
						<input id="shopCodeUnder"  style="width: 196px" type="text" class="InputTextStyle InputText1Style" maxlength="50"/>
						<label class="LabelStyle" style="width: 95px;"><s:text name="sale_plan_year"/><span class="ReqiureStyle">*</span></label>
						<div class="BoxSelect BoxSelect2">                    	
                            <select class="MySelectBoxClass" id="yearPeriodUnder" onchange="return QuotaMonthPlan.yearPeriodUnderChange();">
	                            <s:iterator value="lstYear">
									<option value='<s:property/>'><s:property/></option>
								</s:iterator>
                            </select>
                        </div>
                        <label class="LabelStyle" style="width: 95px;"><s:text name="sale_plan_period"/><span class="ReqiureStyle">*</span></label>
						<div class="BoxSelect BoxSelect2">
							<select id="numPeriodUnder">
						    </select>
						</div>
						<div class="ButtonSection cmsiscontrol" id="group_insert_execute_idDiv" style="float: right; margin-right: 12px;">
							<button class="BtnGeneralStyle BtnMSection" id="group_insert_execute" onclick="return QuotaMonthPlan.execute();"><s:text name="sale_plan_allocate"/></button>
							<button class="BtnGeneralStyle" id="reset" onclick="return QuotaMonthPlan.reset();"><s:text name="sale_plan_reset_data"/></button>
						</div>				                        		
	                </div>	        
					<div class="GeneralForm GeneralNoTP1Form">
						<div class="Func1Section cmsiscontrol" id="group_insert_idDivImport">
							<p class="DownloadSFileStyle DownloadSFile2Style">
								<a href="javascript:void(0)" class="Sprite1" id="downloadTemplate" onclick="QuotaMonthPlan.downloadImportSalePlanTemplateFile()" ><s:text name="sale_plan_download_template"/></a>
							</p>
							<div class="DivInputFile">
								<form action="/sale-plan/quota-month/excel" name="importFrm" id="importFrm"  method="post" enctype="multipart/form-data">
									<input type="hidden" name="token" id="tokenImport" value='<s:property value="token"/>'>								
									<input type="file" class="InputFileStyle InputText1Style" size="20" name="excelFile" id="excelFile" onchange="previewImportExcelFile(this,'importFrm');">
									<input type="hidden" id="isView" name="isView">
		            				<input type="hidden" id="excelShopCodeUnder" name="shopCodeUnder">
									<div class="FakeInputFile">
						                <input type="text" id="fakefilepc" readonly="readonly" class="InputTextStyle InputText1Style" />
						            </div>									
								</form>
							</div>
							<button id="btnImport" class="BtnGeneralStyle" onclick="return QuotaMonthPlan.importExcel();"><s:text name="sale_plan_import"/></button>
						</div>						
						<button id="btnView"   class="BtnGeneralStyle" onclick="return QuotaMonthPlan.exportExcel();"><s:text name="sale_plan_export"/></button>							
					</div>
					<div class="Clear"></div>
					<div id="responseDiv"></div>
					<p id="errExcelMsg" class="ErrorMsgStyle" style="display: none"></p>
					<p id="mainErr" class="ErrorMsgStyle" style="display: none;"></p>
				 	<p class="SuccessMsgStyle" id="successMsg" style="display: none"></p> 
				</div>
				<div class="Clear"></div>
			</div>
			<div class="Clear"></div>
		</div>
	</div>
</div>
<s:hidden id="curYearPeriod" name="yearPeriod"></s:hidden>
<s:hidden id="curNumPeriod" name="numPeriod"></s:hidden>
<script type="text/javascript">
$(document).ready(function() {
	yearPeriodChange = function (data) {
		QuotaMonthPlan._isYearPeriodChange = true;
		$('#yearPeriodUnder').val($('#yearPeriod').val());
		$('#yearPeriodUnder').change();
		QuotaMonthPlan.initCycleInfo();
	};
	$('#yearPeriod').val('<s:property value="yearPeriod"/>');
	$('#yearPeriod').change();
	$('#category').dropdownchecklist({ 
		emptyText:'<s:text name="sale_plan_all"/>',
		firstItemChecksAll: true,
		maxDropHeight: 250,
		onItemClick:function(checkbox, selector){
		},
		textFormatFunction: function(options) {
	        var selectedOptions = options.filter(":selected");
		    var text = '';
	        if(selectedOptions.size() > 0 && parseInt(selectedOptions[0].value) == -1){
	        	text = '<s:text name="sale_plan_all"/>';
	        }else{
		        for(var i = 0;i<selectedOptions.size();i++){
			        if(text.length > 0){
			        	text += ',';
			        }
			        text += selectedOptions[i].text.split('-')[0];
		        }
	        }
	        return text;
		}
	});
	$('#shopCode').focus();
	$('#dg').datagrid({  
	    url:'/sale-plan/quota-month/details',		    
	    view: bufferview,	
	    singleSelect:true,		   
	    width: $(window).width() - 50,
	    height: 350,
	    rownumbers : true,
	    autoRowHeight:false,
	    pageSize:50,  
	    fitColumns:true,
	    autoWidth: true,
	    pagination:true,
	    queryParams:{		    	
	    },
	    showFooter: true,
	    columns:[[ 
			{field:'productCode', title:'<s:text name="sale_plan_product"/>', width:100, align:'left',
				formatter: function(val, row) {
					if (val != undefined && val != null) {
						return Utils.XSSEncode(val);
					}
					return '';
				}
			},
			{field:'productName', title:'<s:text name="sale_plan_productname"/>', width:350, align:'left',
				formatter: function(val, row) {
					if (val != undefined && val != null) {
						if (val.indexOf('Tổng cộng') >= 0) {
							return val;
						}
						return Utils.XSSEncode(val);
					}
					return '';
				}
			},
			{field:'quantity', title:'<s:text name="sale_plan_quantity"/>', width:100, align:'right',formatter:function(value,row,opts){
				if (row.quantity == null) {
					row.quantity = '';
				}else if (new String(row.quantity).indexOf('_') > 0) {
					return '<b id="footerQuantity">' + formatCurrency(new String(row.quantity).split('_')[0]) + '</b>';
				}
				return '<input reset="0" old-quantity="' + row.quantity + '" maxlength="8" index="' + opts + '" style="text-align: right; width: 80px; margin: 0;" id="' + row.productCode + '" type="text" class="InputTextStyle InputText1Style skuItem"	value="' + formatCurrency(row.quantity) + '" onkeypress="NextAndPrevTextField(event,this,\'skuItem\')" onblur="return QuotaMonthPlan.qtyChanged(this);" />';
			}},
			{field:'price', title:'<s:text name="sale_plan_price"/>', width:90, align:'right',formatter:function(value){
				return formatCurrency(value);
			}},
			{field:'amount', title:'<s:text name="sale_plan_amount"/>', width:120, align:'right',formatter:function(value,row,opts){					
				if (row.amount != null && new String(row.amount).indexOf('_') >= 0) {
					return '<b id="footerAmount">' + formatFloatValue(new String(row.amount).split('_')[0],2) + '</b>';
				} else if (row.amount == null) {
					return '<input maxlength="20" old-amount="" style="text-align: right; width: 118px; margin: 0;" id="amount'+row.productCode+'" type="text" index="'+opts+'"class="InputTextStyle InputText1Style skuAmount" value="" onblur="return QuotaMonthPlan.amtChanged(this);" />';
				}
				return '<input maxlength="20" old-amount="' + row.amount + '" style="text-align: right; width: 118px; margin: 0;" id="amount'+row.productCode+'" type="text" index="'+opts+'"class="InputTextStyle InputText1Style skuAmount" value="' + formatCurrency(row.amount) + '" onblur="return QuotaMonthPlan.amtChanged(this);" />';
			}},				
			{field:'productId',hidden:true},
			{field:'convfact',hidden:true}
		]],    
		onLoadSuccess :function(data){			
			if($('#dg').datagrid('getRows')==null ||$('#dg').datagrid('getRows').length==0){
				disabled('btnView');
			}else{
				enable('btnView');
			}	
			
			if(QuotaMonthPlan.isReset){
				$('.skuItem').each(function(){
					if(parseInt($(this).attr('reset'))==0){
						$(this).val('');
						$(this).attr('old-quantity','');
						$('#amount'+$(this).attr('id')).html('');
						$('#amount'+$(this).attr('id')).attr('old-amount','0');
					}						
				});
			}								
			$('.datagrid-pager').html('').hide();
			Utils.bindFormatOnTextfieldInputCss('skuItem', Utils._TF_NUMBER);
			Utils.bindFormatOnTextfieldInputCss('skuAmount', Utils._TF_NUMBER_DOT);
			$('.datagrid-header-rownumber').html('<s:text name="sale_plan_stt"/>');
		   	$('.datagrid-header-row td div').css('text-align','center');		   	
		   	updateRownumWidthForJqGrid('.easyui-dialog');
	   	 	$(window).resize();    		 
		}
	});
	$('#dg').datagrid('reloadFooter',[
	    {shopCode:"_",spMonth:"_",productName: '<b><s:text name="sale_plan_tongcong"/></b>',quantity: 0 + "_" , amount: 0 + "_"},
	 ]);
	QuotaMonthPlan._skuMap = new Map();
	$('#errMsgSearch').html('').hide();
	$('#mainErr').html('').hide();
	$('#shopCode').bind('keyup',function(event) {
		if (event.keyCode == keyCode_F9) {
				var txt = 'shopCode';
				var txtUnder = 'shopCodeUnder';
				VCommonJS.showDialogSearch2({
					inputs : [
				        {id:'code', maxlength:50, label:'<s:text name="sale_plan_unit_code"/>'},
				        {id:'name', maxlength:250, label:'<s:text name="sale_plan_unit_name"/>'},
				    ],
				    url : '/commons/search-shop-show-list-NPP',
				    columns : [[
				        {field:'shopCode', title:'<s:text name="sale_plan_unit_code"/>', align:'left', width: 110, sortable:false, resizable:false, formatter: function(value, row, index) {
							return Utils.XSSEncode(value);
						}},
				        {field:'shopName', title:'<s:text name="sale_plan_unit_name"/>', align:'left', width: 200, sortable:false, resizable:false, formatter: function(value, row, index) {
							return Utils.XSSEncode(value);
						}},
				        {field:'choose', title:'', align:'center', width:40, sortable:false, resizable:false, formatter:function(value,row,index) {
				            return '<a href="javascript:void(0)" onclick="Utils.fillCodeForInputTextByDialogSearchWithUnder(\''+Utils.XSSEncode(txt)+'\',\''+Utils.XSSEncode(txtUnder)+'\', ' + row.shopId +', \''+Utils.XSSEncode(row.shopCode)+'\', \''+Utils.XSSEncode(row.shopName)+'\' ,\''+Utils.XSSEncode(Utils._isShowInputTextByDlSearch2.code)+ '\');"><s:text name="sale_plan_choose"/></a>';         
				        }}
				    ]]
				});
				
			}
		});  
		$('#shopCode').live('change',function() {
			$('#shopCodeUnder').val($(this).val().trim());
			$('#excelShopCodeUnder').val($(this).val().trim());
			options.data = null;
			options.data = {
					shopCode : $('#shopCode').val()
			};
		});
		$('#shopCodeUnder').bind('keyup',function(event) {
			if (event.keyCode == keyCode_F9) {
				var txt = 'shopCodeUnder';
				VCommonJS.showDialogSearch2({
					inputs : [
				        {id:'code', maxlength:50, label:'<s:text name="sale_plan_unit_code"/>'},
				        {id:'name', maxlength:250, label:'<s:text name="sale_plan_unit_name"/>'},
				    ],
				    url : '/commons/search-shop-show-list-NPP',
				    columns : [[
				        {field:'shopCode', title:'Mã Đơn vị', align:'left', width: 110, sortable:false, resizable:false, formatter: function(value, row, index) {
							return Utils.XSSEncode(value);
						}},
				        {field:'shopName', title:'Tên Đơn vị', align:'left', width: 200, sortable:false, resizable:false, formatter: function(value, row, index) {
							return Utils.XSSEncode(value);
						}},
				        {field:'choose', title:'', align:'center', width:40, sortable:false, resizable:false, formatter:function(value,row,index) {
				            return '<a href="javascript:void(0)" onclick="Utils.fillCodeForInputTextByDialogSearch2(\''+Utils.XSSEncode(txt)+'\', ' + row.shopId +', \''+Utils.XSSEncode(row.shopCode)+'\', \''+Utils.XSSEncode(row.shopName)+'\' ,\''+Utils.XSSEncode(Utils._isShowInputTextByDlSearch2.code)+ '\');"><s:text name="sale_plan_choose"/></a>';         
				        }}
				    ]]
				});
			}
		});
		$('#productId').bind('keyup',function(event) {
			if (event.keyCode == keyCode_F9) {
				var txt = 'productId';
				VCommonJS.showDialogSearch2({
					params : {
						longType : $('#category').val(),
						status : 1
					},
					inputs : [
				        {id:'code', maxlength:50, label:'<s:text name="sale_plan_product"/>'},
				        {id:'name', maxlength:250, label:'<s:text name="sale_plan_productname"/>'},
				    ],
				    url : '/commons/product-searchAllProduct',
				    columns : [[
						{field:'cat', title:'Ngành', align:'left', width: 40, sortable:false, resizable:false, formatter:function(value,row,index) {
				            if(row.cat!=null){
				            	return row.cat.productInfoCode;
				            }    
				        }},        
				        {field:'productCode', title:'<s:text name="sale_plan_product"/>', align:'left', width: 75, sortable:false, resizable:false, formatter: function(value, row, index) {
				        	return Utils.XSSEncode(value);         
				        }},
				        {field:'productName', title:'<s:text name="sale_plan_productname"/>', align:'left', width: 200, sortable:false, resizable:false, formatter: function(value, row, index) {
				        	return Utils.XSSEncode(value);         
				        }},
				        {field:'choose', title:'', align:'center', width:40, sortable:false, resizable:false, formatter:function(value,row,index) {
				            return '<a href="javascript:void(0)" onclick="Utils.fillCodeForInputTextByDialogSearch2(\''+txt+'\', ' + row.id +', \''+row.productCode+'\', \''+row.productName+'\' ,'+Utils._isShowInputTextByDlSearch2.code+');"><s:text name="sale_plan_choose"/></a>';         
				        }}
				    ]]
				});
			}
		});
});

</script>
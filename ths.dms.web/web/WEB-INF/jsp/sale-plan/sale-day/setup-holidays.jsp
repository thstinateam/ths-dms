<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<div class="BreadcrumbSection">
    <ul class="ResetList FixFloat BreadcrumbList">        
        <li><span>Thiết lập ngày bán hàng</span></li>
    </ul>
</div>

<div class="CtnOneColSection">
       <div class="ContentSection">
       	<div class="ToolBarSection">
	       		<div class="SearchSection GeneralSSection">
		       	<div class="SearchInSection SProduct1Form">
		       		<label class="LabelStyle Label1Style">Đơn vị(F9)</label>
					<input id="txtShopCode" type="text"  class="InputTextStyle InputText1Style" maxlength="50" value="<s:property value='shopCodeStr' />"/>	
		       		<div class="Clear"></div>
		       		<p id="errMsgSearch" class="ErrorMsgStyle" style="display: none"></p>
		       	</div>
		       	</div>
               <div class="SearchSection GeneralSSection">
               	<div class="TabSection">
                       <ul class="ResetList TabSectionList">                           
                           <li onclick="SaleDayPlan.showYearTab();"><a id="viewByYear" href="javascript:void(0);" class="Sprite1 Active"><span class="Sprite1">Xem theo năm</span></a></li>
                           <li onclick="SaleDayPlan.showMonthTab();"><a id="viewByMonth" href="javascript:void(0);" class="Sprite1"><span class="Sprite1">Xem theo tháng</span></a></li>
                       </ul>
                       <div class="Clear"></div>
                   </div>
               	<div id="lbhtab1" class="SearchInSection SProduct1Form">
                   	<div id="viewYear" class="CalendarYearBox">
                           <div class="BtnCenterSection">
                               <div class="BoxSelect BoxSelect1">
                               		<s:select id="currentYear" name="currentYear" cssClass="MySelectBoxClass" list="lstYears" listKey="name" listValue="value" onchange="SaleDayPlan.currentYearChanged();"></s:select>                                   
                               </div>
                               <div class="Clear"></div>
                           </div>
                           <ul class="ResetList CYList FixFloat">                               
                                <li id="m1"></li>                                            
                               	<li id="m2"></li>
                               	<li id="m3"></li>
                               	<li id="m4" class="EndItem"></li>
                               	<div class="Clear"></div>
                               	<li id="m5"></li>
                               	<li id="m6"></li>
                               	<li id="m7"></li>
                               	<li id="m8" class="EndItem"></li>
                               	<div class="Clear"></div>
                               	<li id="m9"></li>
                               	<li id="m10"></li>
                               	<li id="m11"></li>
                               	<li id="m12" class="EndItem"></li>
                               	<div class="Clear"></div>
                           </ul>
                           <div class="Clear"></div>
                       </div>
                       <div id="viewMonth" class="CalendarMonthBox" style="display: none">
                        	<div class="BtnCenterSection">
                                <div class="BoxSelect BoxSelect1">
                                    <s:select id="currentYearMonthView" name="currentYear" cssClass="MySelectBoxClass" list="lstYears" listKey="name" listValue="value" onchange="SaleDayPlan.currentMonthChanged();"></s:select>
                                </div>
                                <div class="BoxSelect BoxSelect1 BoxSelect2">
                                    <s:select cssClass="MySelectBoxClass" id="currentMonth" name ="currentMonth" list="#{1: 'Tháng 1',2: 'Tháng 2',3: 'Tháng 3',4: 'Tháng 4',5: 'Tháng 5',6: 'Tháng 6',7: 'Tháng 7',8: 'Tháng 8',9: 'Tháng 9',10: 'Tháng 10',11: 'Tháng 11',12: 'Tháng 12'}" onchange="SaleDayPlan.currentMonthChanged();"></s:select>
                                </div>
                                <div class="Clear"></div>
                            </div>
                            <p id="numWorkDayByMonth" class="Warning2Style"></p>
                            <div id="monthContent" class="GeneralTable Table22Section">                                        
                            </div>
                        </div>
                       <div class="GeneralForm GeneralNoTP1Form">
                           <div class="Func1Section">
                               <p class="DownloadSFileStyle DownloadSFile2Style">
                               		<a id="downloadTemplate" href="#" class="Sprite1">Tải file excel mẫu</a>
                               </p>
                               <form action="/sale-plan/day/import-excel" name="importFrm" style="width: 220px; float: left;" id="importFrm"  method="post" enctype="multipart/form-data">
                               		<input type="hidden" name="token" id="tokenImport" value='<s:property value="token"/>'>
		                        	<input type="hidden" id="isView" name="isView"/>
		                        	<input type="file" class="InputFileStyle" size="22" name="excelFile" id="excelFile" onchange="previewImportExcelFile(this,'importFrm');">
		                        	<div class="FakeInputFile">
										<input id="fakefilepc" readonly="readonly" type="text" class="InputTextStyle InputText1Style">
									</div>
		                        </form>                               
                               <button id="btnImport" class="BtnGeneralStyle" onclick="SaleDayPlan.importExcel();">Nhập từ Excel</button>
                               <button id="btnExport" class="BtnGeneralStyle" onclick="SaleDayPlan.exportExcel();">Xuất Excel</button>
                           </div>
                           <div class="Clear"></div>
                       </div>
                   </div>
               </div>
               <div class="Clear"></div>               
               <p id="errExcelMsg" class="ErrorMsgStyle" style="display: none"></p>
				<div id="responseDiv" style="display: none"></div>
	             <div class="Clear"></div>
				 <tiles:insertTemplate template="/WEB-INF/jsp/general/successMsg.jsp" />
           </div>
       </div>
    <div class="Clear"></div>
</div>
<div class="ContentSection" style="visibility: hidden;">
	<tiles:insertTemplate template="/WEB-INF/jsp/general/dialogSearchShopTree.jsp" />
</div>
<div id="dataBox" style="display: none" class="SearchInSection SProduct1Form">
<div class="BoxEditState">
 	<div class="BoxEditStateBtm">
     	<div class="BoxEditStateLoop">
     		<a class="Sprite1 BtnClose" href="javascript:void(0)" onclick="$('#dataBox').hide();"><span class="HideText">Đóng</span></a>
     		<div class="Clear"></div>
         	<label class="LabelStyle Label1Style">Trạng thái</label>
         	<div class="BoxSelect BoxSelect3">
                 <s:select id="dateType" name="dateType" cssClass="MySelectBoxClass" list="#{0: 'Bình thường',1: 'Nghỉ'}" onchange="dateTypeChanged();"></s:select>
             </div>
             <div class="Clear"></div>
             <label class="LabelStyle Label1Style">Lý do</label>
         	<div class="BoxSelect BoxSelect3">
                 <s:select name="reasonId" id="reasonGroup" headerKey="-2" headerValue="-- Chọn lý do --"  cssClass="MySelectBoxClass" list="lstReasons" listKey="apParamCode" listValue="apParamName"></s:select>
             </div>
             <div class="Clear"></div>
             <p id="errMsg" class="ReqiureStyle" style="display: none"></p>
             <div class="Clear"></div>
             <div id="btnUpdateSaleDay" class="BtnCenterSection" style="display: none">
                 <button class="BtnGeneralStyle" onclick="SaleDayPlan.saveDateInfo()">Cập nhật</button>
             </div>
             <div class="Clear"></div>  
             <s:hidden id="dateValue"></s:hidden>           
     	</div>
     </div>
</div>
</div>

<script>
$(document).ready(function(){	
	$('#layoutContent').removeClass('Content');
	$('#layoutContent').addClass('MainContent');
	SaleDayPlan.loadCalendar();
	dateTypeChanged();	
	$('#downloadTemplate').attr('href',excel_template_path + 'sale-plan/Bieu_mau_KHTT_thiet_lap_ngay_ban_hang.xls');
// 	$('#importFrm').ajaxForm(options);
	$('#txtShopCode').focus();
	var txt = 'txtShopCode';
	$('#txtShopCode').bind('keyup', function(event) {
		if (event.keyCode == keyCode_F9) {
			$('#common-dialog-search-tree-in-grid-choose-single').dialog({
				title: "Tìm kiếm Đơn vị",
				closed: false,
		        cache: false, 
		        modal: true,
		        width: 680,
		        height: 550,
		        onOpen: function(){
		        	$('#txtShopCodeDlg').val("");
		        	$('#txtShopNameDlg').val("");
		        	//Tao cay don vi
		        	$('#commonDialogTreeShopG').treegrid({  
		        		data: StockManager._arrRreeShopTocken,
		        		url : '/cms/searchTreeShop',
		    		    width: 650,
		    		    height: 325,
		    		    queryParams: {code: $('#txtShopCodeDlg').val().trim(), name: $('#txtShopNameDlg').val().trim()},
		    			fitColumns : true,
		    			checkOnSelect: false,
		    			selectOnCheck: false,
		    		    rownumbers : true,
		    	        idField: 'id',
		    	        treeField: 'code',
		    		    columns:[[
		    		        {field:'code',title:'Mã đơn vị',resizable:false, width:200, align:'left', formatter:function(value, row, index){
		    		        	return Utils.XSSEncode(value);
		    		        }},
		    		        {field:'name',title:'Tên đơn vị',resizable:false, width:200, align:'left', formatter:function(value, row, index){
		    		        	return Utils.XSSEncode(value);
		    		        }},
		    		        {field:'choose',title:'',resizable:false, width:60, align:'center', fixed:true, formatter:function(value, row, index){
		    		        	return '<a href="javascript:void(0)" onclick="SaleDayPlan.fillCodeForInputTextByDialogSearch2(\''+txt+'\', ' + row.id +', \''+Utils.XSSEncode(row.code)+'\', \''+Utils.XSSEncode(row.name)+'\' ,'+Utils._isShowInputTextByDlSearch2.code+');">chọn</a>';   
		    		        }}
		    		    ]],
		    	        onLoadSuccess :function(data){
		    				$('.datagrid-header-rownumber').html('STT');
		    				Utils.updateRownumWidthAndHeightForDataGrid('commonDialogTreeShopG');
		    	        }
		    		});
		        	$('#txtShopCodeDlg').focus();
		        },
		        onClose:function() {
		        	
		        }
			});
		}
	});
});
function dateTypeChanged(){
	if($('#dateType').val() == -2){
		setSelectBoxValue('reasonGroup');
		disabled('reasonGroup');
		$('#groupTransfer').parent().addClass('BoxDisSelect');
		$('#reasonGroup').change();
	} else if($('#dateType').val() == 0){
		setSelectBoxValue('reasonGroup');
		disabled('reasonGroup');
		$('#groupTransfer').parent().addClass('BoxDisSelect');
		$('#reasonGroup').change();
	} else if($('#dateType').val() == 1){
		enable('reasonGroup');
		$('#groupTransfer').parent().removeClass('BoxDisSelect');
		$('#reasonGroup').change();
	} 
}
</script>
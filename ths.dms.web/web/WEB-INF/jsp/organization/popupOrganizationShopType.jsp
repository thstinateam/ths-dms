<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<style>
	.inputStyle1Pop{
		width: 468px;
		margin-left: 12px;
	}
	
</style>
<div id="searchStyle1EasyUIDialogDiv" style="display: none;">
	<div id="popupOrganizationUnitType" class="easyui-dialog" title=<s:text name="organization_title_popup_them_don_vi" /> style="width:550px;height:220px;" data-options="closed:true,modal:true">
        <div class="PopupContentMid">
        	<div class="GeneralForm Search1Form">
        		<div class="Clear"></div>
                <input id="txtOrganizationUnitType" type="text" class="InputTextStyle inputStyle1Pop"  />
	          	<button class="BtnGeneralStyle Sprite2 BtnSearchOnDialog" style="margin-left: 10px;margin-top: -3px;padding-top: 4px;" id="btnSearchOrganizationInGrid" onclick="return OrganizationSystem.searchOrganizationUnitType();">
	          		<span class="Sprite2"><s:text name="jsp.common.timkiem" /></span>
	          	</button>
	          	<h2 class="Title2Style" style="margin-top: 8px; margin-left:12px; width: 555px"><s:text name="work_date_result_search" /></h2>
				<div class="Clear"></div>
				<div class="GridSection" id="idDialogOrganizationContainer" style="padding-top: 0px;">
					<table id="idOrganizationGrid"></table>
				</div>
				<div class="Clear"></div>
                <div class="BtnCenterSection">
                    <button onclick="$('.easyui-dialog').dialog('close');" class="BtnGeneralStyle" ><s:text name="jsp.common.dong"/></button> 
                </div>
                <div class="Clear"></div>
                <input type="hidden" id="OrganizationUnitTypeIdHid" value="0" />
            </div>
            <div class="Clear"></div>
            <p id="errMsg" style="display: none;" class="ErrorMsgStyle SpriteErr"></p>
            <p id="successMsg" style="display: none;" class="SuccessMsgStyle"></p>
		</div>
    </div>
 </div>   

<script type="text/javascript">
$(document).ready(function() {
	
});

</script>

<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<style>
/* .canhtrenduoi{
	margin: 10px 0px;
	padding-left: 15px;
}
.canhtrai{
	margin: 0px 30px;
} */
</style>
<div id="searchStyle1EasyUIDialogDiv" style="display: none;">
	<div id="popupOrganizationDetail" class="easyui-dialog" title="<s:text name="organization_title_popup_information_organization" />" style="width:550px;height:232px;" data-options="closed:true,modal:true">
        <div class="PopupContentMid">
        	<div class="GeneralForm Search1Form">
				<label class="LabelStyle Label1Style Label1PopupStyle" id="lblName" maxlength><s:text name="organization_title_popup_loai_don_vi"/><span class="ReqiureStyle">*</span></label>
                <input id="OrganizationDetailCodePop" type="text" class="InputTextStyle InputText2Style" maxlength="100" style="width: 250px;"/>
                <label class="LabelStyle Label2Style Label2PopupStyle" id="lblkyHieu" maxlength="100"><s:text name="organization_prefix"/> <span class="ReqiureStyle">*</span></label>
                <input id="OrganizationDetailNamePop" type="text" class="InputTextStyle InputText2Style" style="width: 136px" maxlength="100"/>
                <div class="Clear"></div>
                <label class="LabelStyle Label1Style Label1PopupStyle" id="lblDescription"><s:text name="organization_title_popup_description"/></label>
		        <textarea class="InputTextStyle InputArea1Style" rows="4" cols="50" id="descriptionPop" style="width: 252px;margin-left: 0px" maxlength="500"></textarea>
                <label class="LabelStyle Label2Style Label1PopupStyle"><s:text name="organization_system_icon"/></label>
                <div id="abc" style="width: 75px; height: 75px; background-color: white; border: 1px solid #c4c4c4; display: inline-block;" onclick='return OrganizationSystem.changeImage();'><span style=" position: absolute; font-weight: bold; padding: 30px 0px; margin-left: 11px">Chọn ảnh</span></div>
            </div>
            <div class="UploadMediaSection" style="padding:0px 0px">
                    <div class="MediaBox ">                                                                
                        <div class="UploadMediaBox" ></div>                      
                </div>
                <div class="GeneralForm GeneralNoTPForm">
                    <div class="BtnCenterSection">
                        <button id="btnUpload" class="BtnGeneralStyle" ><s:text name="jsp.common.capnhat"/></button>
                        <button id="btnCancelUpload" class="BtnGeneralStyle" onclick="return OrganizationSystem.cancelSavingOrganizationUnitType();"><s:text name="jsp.common.bo.qua"/></button>
                   </div>                        
                </div>
                <div class="Clear"></div>
            </div>  
            <%-- </s:if>     --%>
            <p id="errMsgUpload" class="ErrorMsgStyle SpriteErr" style="display: none"></p>
            <p id="sucMsgUpload" class="SuccessMsgStyle" style="display: none"></p>
            </div>
            <div class="Clear"></div>
          <!--   <p id="errMsg" style="display: none;" class="ErrorMsgStyle SpriteErr"></p>
            <p id="successMsg" style="display: none;" class="SuccessMsgStyle"></p> -->
		</div>
    </div>
 </div>   
<script type="text/javascript">
var formatUrl = '<s:property value="mediaItem.url"/>';
var myDropzone;
$(document).ready(function() {
	// upload images
/*    if(DropzonePlugin._dropzoneObject != undefined && DropzonePlugin._dropzoneObject != null){
        DropzonePlugin._dropzoneObject.destroy();   
    }
    ProductCatalog.initUploadImageElement('body', 'body', '.addFileBtn');*/

    OrganizationSystem.initializeUploadIconElement();
   
    var w = $('.GeneralMIn.MediaPlayerSection').width();
    /* $('#videoBox').width(w);
    $('#imageBox').width(w);
    $('#imageBox img').width(w); */
    $('#videoBox #player').width(w);
    $('#videoBox #player .media').width(w);
    $('#videoBox #player object').width(w);
    
    var options = { 
            beforeSubmit: ProductCatalog.beforeImportItem,   
            success:      ProductCatalog.afterImportItem,  
            type: "POST",
            dataType: 'html'
     }; 
    $('#importFrm').ajaxForm(options);

    $('#buttonUpload').bind('click', function() {
        var  nodeType = OrganizationSystem._nodeType;
        OrganizationSystem.saveOrganizationUnitType(nodeType);   
    });  
});

function getFileName(){
    $('#errMsgUpload').html('').hide();
    $('#videoFileName').html(document.getElementById("videoFile").value);
}

/* function liveUpdateImageForProduct1(object, mediaType, mediaItemId, checkPermission) { */
function liveUpdateImageForProduct1(object, mediaType, mediaItemId) {
    $('#mediaItemId').val(mediaItemId);
    if (mediaType == 0) {
        $('#player').html('').hide();
        $('#imageBox').show();
        var orgSrc = object.attr('data');
        var source = imgServerPath +orgSrc;
        $('#imageBox').html('<img style="max-width: 715px;max-height: 551px" alt="" src="'+source+'" />');
    } else {
        $('#player').show();
        $('#imageBox').hide();
        var orgSrc = object.attr('data');
        var source = imgServerPath +orgSrc;
        if (orgSrc.substring(orgSrc.length - 3,orgSrc.length) == 'avi') {
            $('#player').html('<a class="media {width:715, height:551}" href="'+source+'"></a>');
            $('a.media').media();
        } else {
            flowplayer("player", "/resources/scripts/plugins/flowplayer/flowplayer-3.2.16.swf",{
                plugins: {
                    audio: {
                        url: '/resources/scripts/plugins/flowplayer/audio/flowplayer.audio-3.2.10.swf'
                    }
                },
                clip: {
                    url: source,
                    autoPlay: true,
                    autoBuffering: true
                },
                play: {
                    replayLabel: jsp_common_s_xem_lai,
                }
            });         
        }
    }
    var w = $('.GeneralMIn.MediaPlayerSection').width();
    /* $('#videoBox').width(w);
    $('#imageBox').width(w);
    $('#imageBox img').width(w);  */
    $('#videoBox #player').width(w);
    $('#videoBox #player .media').width(w);
    $('#videoBox #player object').width(w);
}
</script>

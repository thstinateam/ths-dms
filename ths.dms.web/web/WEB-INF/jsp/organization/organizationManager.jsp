<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@taglib prefix="k" uri="/kryptone" %>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@page import="ths.dms.helper.Configuration"%>
<script type="text/javascript" src="<%=Configuration.getStaticServerPath("/resources/scripts/business/common/vnm.mapapi.js")%>"></script>
<style>
#viewNote {
    color: #04528e;
    font-family: "Arial Bold","Arial";
    font-style: normal;
    font-weight: 700;
    text-align: centReportTree2Sectioner;
     margin-top: 10px;
}
</style>
<%-- <div class="Breadcrumbs">
    <span class="Breadcrumbs1Item Sprite1"><a href="/catalog/organization/info">Thông tin cây tổ chức</a></span>
</div> --%>
<div class="BreadcrumbSection">
       	<ul class="ResetList FixFloat BreadcrumbList">
           <li class="Sprite1"><a href="/catalog/organization/info"><s:text name="organization_system"/></a></li>
           <li><span><s:text name="organization_system_info_title"/></span></li>
    	</ul>
</div>
 <div class="CtnOneColSection">
        <div class="ContentSection">
        	<div class="ToolBarSection">
                <div class="SearchSection GeneralSSection">
                    <div class="Sidebar1Section" style="width:100%">	
                        <h2 class="Title2Style"><s:text name="organization_tree"/></h2>
                       <!--  <p class="ErrorMsgStyle SpriteErr" id="errMsg" style="display: none;">Có lỗi xảy ra khi cập nhật dữ liệu</p>
						<tiles:insertTemplate template="/WEB-INF/jsp/general/successMsg.jsp" /> -->
						
						<div class="GeneralMilkBox">
					     	<div id="viewNote" style="visibility: hidden;">
					     		<p style="text-align: center;"><s:text name="organization_system_not_defined"/></p>
					         	<a href="javascript:void(0);" onclick="return OrganizationSystem.addNewShop(2);"><img src="/resources/images/add-organization.png"></a>
					        </div>
				     	</div>
                        <div id="treeDiv" class="SearchInSection SProduct1Form" style="overflow:auto;height:auto;">
                           <!--  <div class="ReportTreeSection ReportTree2Section"   style="height:auto;" > -->
                            <div class="ReportTreeSection"   style="height:auto;" > 
                                <ul id="tree" style="width:20px"></ul>
                            </div>
                        </div>
                    </div>
                    <div class="Content3Section" id="unitTreeContent" style="display: none;">
                	</div>
                </div>
                <div class="Clear"></div>
	                <p id="errMsg" style="display: none;" class="ErrorMsgStyle SpriteErr"></p>
	            	<p id="successMsg" style="display: none;" class="SuccessMsgStyle"></p>
            	<div class="Clear"></div>
            </div>
				<p id="errExcelMsg" class="ErrorMsgStyle" style="display: none"></p>
				<div id="responseDiv" style="display: none"></div>
       </div>
     <div class="Clear"></div>
</div>
<!-- LIEMTPT POPUP -->
<div class="ContentSection" style="visibility: hidden;">
	<tiles:insertTemplate template="/WEB-INF/jsp/organization/popupOrganizationShopType.jsp" />
</div>
<div class="ContentSection" style="visibility: hidden;">
	<tiles:insertTemplate template="/WEB-INF/jsp/organization/popupOrganizationShopTypeDetail.jsp" />
</div>
<div class="ContentSection" style="visibility: hidden;">
	<tiles:insertTemplate template="/WEB-INF/jsp/organization/popupIsManage.jsp" />
</div> 

<!-- LIEMTPT POPUP -->
<!-- BEGIN VUONGMQ -->   
<div id="contextMenu" style="display:none" style="width:120px;">
    <div id="cmTaoLoaiDonViCon" onclick="OrganizationSystem.addNewShop(1);" data-options="iconCls:'icon-themloaidonvicon'"><s:text name="organization_them_loai_don_vi_con"/></div>
    <div id="cmTaoLoaiDonViCungCap" onclick="OrganizationSystem.addNewShop(2);" data-options="iconCls:'icon-add'"><s:text name="organization_them_loai_don_vi_cung_cap"/></div>
    <div id="cmTaoChucVu" onclick="OrganizationSystem.addNewStaff();" data-options="iconCls:'icon-themchucvu'"><s:text name="organization_them_chuc_vu"/></div>
    <div id="cmXoaLoaiDonVi" onclick="OrganizationSystem.deleteShopAndStaffTree('SHOP');" data-options="iconCls:'icon-remove'"><s:text name="organization_xoa_loai_don_vi"/></div>
    <div id="cmXoaChucVu" onclick="OrganizationSystem.deleteShopAndStaffTree('STAFF');" data-options="iconCls:'icon-remove'"><s:text name="organization_xoa_chuc_vu"/></div>
    <div id="cmThemLaQuanLy" onclick="OrganizationSystem.checkIsManageStaffTree('STAFF','isManage');" ><s:text name="organization_them_la_quan_ly"/></div>
    <div id="cmXoaLaQuanLy" onclick="OrganizationSystem.checkIsManageStaffTree('STAFF','notManage');" ><s:text name="organization_bo_dau_quan_ly"/></div>
</div>
<!-- END VUONGMQ -->
<div style="display:none" id="viewBigMap">
	<div id="fancyboxBigMap" class="GeneralDialog General2Dialog" style="width:970px;height:575px">
		<div id="bigMapContainer" style="width:970px;height:575px"></div>
		<div class="Clear"></div>
	</div>
</div>
<s:hidden id="unitId" value="0"></s:hidden>
<s:hidden id="shopCodeHidden" name="staff.shop.shopCode"></s:hidden>
<s:hidden id="shopIdHidden" name="staff.shop.id"></s:hidden>
<s:hidden id="parentCodeS" value="-1"></s:hidden>
<input type="hidden" id="showDateId" value="<s:property  value="showDate" />" />
<s:hidden id="idCheck" name="id"></s:hidden>
<script type="text/javascript">
$(document).ready(function(){
	/**BEGIN VUONGMQ*/
	var idCheck = $('#idCheck').val();
	OrganizationSystem.checkOrganization(idCheck);
	var tm = setTimeout(function(){
		 UnitTreeCatalog.resize();
		 clearTimeout(tm);
	 }, 150);
	$('#contextMenu').addClass('easyui-menu');
	/**END VUONGMQ*/
	
	
	$('#liemNodeType').bind('change', function() {
		var nodeType = $('#liemNodeType').val().trim();
		if(!Utils.isEmpty(nodeType)){
			OrganizationSystem._nodeType = nodeType;
		}
	});
	$('#liemOrganizationId').bind('change', function() {
		var organizationId = $('#liemOrganizationId').val().trim();
		if(!Utils.isEmpty(organizationId)){
			OrganizationSystem._organizationId = organizationId;
		}
		
	});
});
</script>
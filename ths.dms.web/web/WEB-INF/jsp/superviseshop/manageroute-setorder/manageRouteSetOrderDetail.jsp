<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<h2 class="Title2Style">Tìm kiếm khách hàng</h2>
<div class="GeneralForm SearchInSection">
	<label class="LabelStyle Label1Style">Mã tuyến</label>
	<input type="text" class="InputTextStyle InputText1Style" id="routingCode" maxlength="40" disabled="disabled" value="<s:property value="routing.routingCode" />"/>
    <label class="LabelStyle Label1Style">Tên tuyến</label>
    <input type="text" class="InputTextStyle InputText2Style" id="routingName" maxlength="40" disabled="disabled" value="<s:property value="routing.routingName" />"/>
    <div class="Clear"></div>
    <label class="LabelStyle Label1Style">Ngày BH<span class="ReqiureStyle">*</span></label>
    <div class="BoxSelect BoxSelect6">
        <select id="listDate" class="MySelectBoxClass" onchange="SuperviseManageRouteSetOrder.changeSelectBox();">
			<option value="2"  selected="selected">Thứ Hai</option>
			<option value="3" >Thứ Ba</option>
			<option value="4" >Thứ Tư</option>
			<option value="5" >Thứ Năm</option>
			<option value="6" >Thứ Sáu</option>
			<option value="7" >Thứ Bảy</option>
			<option value="1" >Chủ Nhật</option>
		</select>
    </div>
    <div class="Clear"></div>
</div>
<div class="TabSection">
    <ul class="ResetList TabSectionList">
        <li><a href="javascript:void(0);" id="tabActive1" onclick="SuperviseManageRouteSetOrder.showTab1();" class="Sprite1 Active"><span class="Sprite1">Danh sách khách hàng</span></a></li>
        <li><a href="javascript:void(0);" id="tabActive2" onclick="SuperviseManageRouteSetOrder.showTab2();" class="Sprite1 "><span class="Sprite1">Xem trên bản đồ</span></a></li>
    </ul>
    <div class="Clear"></div>
</div>
<div id="container1">
	<div class="GeneralForm SearchInSection">
        <div class="GridSection" id="routingContainerGrid">
        	<table id="routingGrid"></table>
        </div>
        <div class="Clear"></div>
        <div class="BtnCenterSection">
        	<button class="BtnGeneralStyle" onclick="SuperviseManageRouteSetOrder.updateListRoutingCustomer();">Cập nhật</button>
        </div>
        <div class="Clear"></div>
        <p id="errMsg" class="ErrorMsgStyle SpriteErr" style="display:none;"></p>
		<p id="successMsg" class="SuccessMsgStyle" style="display:none;"></p>
	</div>
</div>
<div id="container2" style="display: none;">
	<div class="GeneralForm SearchInSection">
    	<p id="errMsgMap" class="ErrorMsgStyle SpriteErr" style="display: none">Không có khách hàng.</p>
       	<div id="divMapRoute" style="position:relative;">
               <div id="mapRoute" style="width:100%;height:424px;position: relative;" ></div>
         	</div>
         </div>
	</div>
<input type="hidden" id="routingId" value="<s:property value="routing.id"/>"/>
<script>
$(document).ready(function() {
	$('#listDate').customStyle();
	var params = new Object();
	params.routingId = $('#routingId').val();
	params.saleDate = $('#listDate').val();
	SuperviseManageRouteSetOrder._filterSaleDate = $('#listDate').val();
	SuperviseManageRouteSetOrder.convertSeq();
	$('#routingGrid').datagrid({
		url :'/superviseshop/manageroute-create/order/search',
        queryParams:params,
        rownumbers : true,
        pageNumber : 1,
        singleSelect :true,
        scrollbarSize: 0,
        autoWidth: true,
        autoRowHeight : true,
        fitColumns : true,
		width : ($('#routingContainerGrid').width()),
		columns:[[  
			{field:'shortCode',title:'Mã KH',width:100, align:'left', formatter: function(value, row, index) {
				return Utils.XSSEncode(value);         
			}},
		    {field:'customerName',title:'Tên KH',width:250, align:'left', formatter: function(value, row, index) {
		    	return Utils.XSSEncode(value);         
		    }},
		    {field:'address',title:'Địa chỉ',width:350,align:'left', formatter: function(value, row, index) {
		    	return Utils.XSSEncode(value);         
		    }},
		    {field:	'Thuws 2',title:'TT',width:100,align:'center', formatter: function(value, row, index) {
		    	if(row[SuperviseManageRouteSetOrder._seqTmp] == null) {
		    		return '<input type="text" id="seq_'+row.customerId+'" onkeypress="NextAndPrevTextField(event,this,\'InputTextStyleSetOrder\')" class="seq InputTextStyleSetOrder" value="" maxlength="5"></input>';
		    	} else {
		    		return '<input type="text" id="seq_'+row.customerId+'" onkeypress="NextAndPrevTextField(event,this,\'InputTextStyleSetOrder\')" class="seq InputTextStyleSetOrder" value="'+row[SuperviseManageRouteSetOrder._seqTmp]+'" maxlength="5" ></input>';
		    	}
		    }},
		]],
        onLoadSuccess :function(data){
            $('.InputTextStyleSetOrder').each(function(){
            	$(this).numberbox({ 
 	                min:1,
 	                max: data.total,
 	                precision:0
 	            });  
    		});
	    	$('#routingContainerGrid .datagrid-header-rownumber').html(jsp_common_numerical_order);
	    	updateRownumWidthForDataGrid('#routingContainerGrid');
	    	SuperviseManageRouteSetOrder.resize();
    	}
	});
	$('#listDate').bind('keyup', function(event) {
		if (event.keyCode == keyCodes.ENTER) {
			SuperviseManageRouteSetOrder.changeSelectBox();
		}
	});
});
	
</script>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<div class="BreadcrumbSection">
    <ul class="ResetList FixFloat BreadcrumbList">
        <li class="Sprite1"><a href="/superviseshop/manageroute-create/info">Thiết lập tuyến</a></li>
        <li><span>Thiết lập thứ tự ghé thăm</span></li>
    </ul>
</div>
<div class="CtnTwoColsSection">
    <div class="SidebarSection">
    	<h2 class="Title2Style">Cây tuyến</h2>
    	<div class="SidebarInSection">
    		<div class="ReportTreeSection" id="treeContainer" >
				<div id="routingTree"></div>
			</div>
            <div class="Clear"></div>
    	</div>
    </div>
    <div class="ContentSection">
    	<div class="ReportCtnSection" id="routingContent">
    		<tiles:insertTemplate template="/WEB-INF/jsp/superviseshop/manageroute-setorder/manageRouteSetOrderDetail.jsp"></tiles:insertTemplate>
        </div>
    </div>
    <div class="Clear"></div>
</div>
<script>
$(document).ready(function() {
	TreeUtils.loadRoutingTree('routingTree', 'routingId',$('#routingId').val(), function(data) {
		SuperviseManageRouteSetOrder.detail(data);
	});
});
function MyOverlayCust(custId, custCode, latlng, ttgt, info)
{
    this.latlng = latlng;
    this.latlngs = latlng;

    this.initialize = initialize;
    this.remove = remove;
    this.copy = copy;
    this.redraw = redraw;
    this.vType = vType;
    this.info=info;
    this.custId=custId;
    this.ttgt=ttgt;
    this.custCode = custCode;

    function initialize(map)
    {
    	this.mapObj = map;
        var myDiv = document.createElement("div");
        myDiv.id = map.id + "MyOverlay";
        myDiv.style.position = "absolute";
        var lat=latlng.latitude;
        var lng=latlng.longitude;
        myDiv.innerHTML ="<div style=\"text-align: center;color:red;font-weight: bolder;\">" + this.custCode + "</div><div class='CustomersStatus Customers4Status' onclick=\"Vietbando.showWindowInfo("+custId+","+lat+","+lng+",'"+this.info+"');\"><span>"+this.ttgt+"</span></div>";
        map.getOverlayContainer().appendChild(myDiv);
        this.divObj = myDiv;
        this.redraw();
    }
    
    
    function remove()
    {
        this.divObj.parentNode.removeChild(this.divObj);
    }

    function copy()
    {
        return new MyOverlay(this.latlng, this.img);
    }

    function redraw()
    {
        var pt = this.mapObj.fromLatLngToDivPixel(this.latlng);
        if(this.custId < 0) {
        	this.divObj.style.left = (pt.x-15) + "px";
            this.divObj.style.top = (pt.y-5 -37) + "px";
        } else {
        	this.divObj.style.left = (pt.x-15) + "px";
            this.divObj.style.top = (pt.y-5) + "px";
        }
    }

    function vType()
    {
        return 'MyOverlay';
    }
}
</script>
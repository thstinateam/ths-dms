<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@page import="ths.dms.helper.Configuration"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
	<script type="text/javascript" src="<%=Configuration.getStaticServerPath("/resources/scripts/business/common/vnm.mapapi.js")%>"></script>
<div class="Breadcrumbs">
	<span class="Breadcrumbs1Item Sprite1">Giám sát kết quả ghé thăm</span>
</div>
<div class="GeneralMilkBox">
	<div class="GeneralMilkTopBox">
		<div class="GeneralMilkBtmBox">
			<h3 class="Sprite2"><span class="Sprite2">Thông tin tìm kiếm</span></h3>
			<div class="GeneralMilkInBox ResearchSection">
				<div class="Debt3Form">
					<label class="LabelStyle Label1Style">Đơn vị (F9)</label>
					<div class="Field2">
						<input type="text" id="shopCode" class="InputTextStyle InputText1Style" value ="<s:property value="shop.shopCode"/>"/>
						<span class="RequireStyle">(*)</span>
	            	</div>
					<label class="LabelStyle Label2Style">Mã NVBH (F9)</label>
					<input type="text" id="staffSaleCode" class="InputTextStyle InputText1Style" />
					<label class="LabelStyle Label3Style">Ngày BH</label>
					<input type="text" id="saleDate" class="InputTextStyle InputText1Style" disabled="disabled" value ="<s:property value="saleDate"/>"/>
					<div class="Clear"></div>
					<div class="ButtonSection">
						<button id="btnSearch" class="BtnGeneralStyle Sprite2" onclick="VisitResult.getInfo()">
							<span class="Sprite2">Tìm kiếm</span>
						</button>
						<img id="loading" class="LoadingStyle" style="visibility: hidden;" src="/resources/images/loading.gif">
					</div>
					<p class="ErrorMsgStyle Sprite1" id="errMsg" style="display: none;"></p>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="GeneralMilkBox">
	<div class="GeneralMilkTopBox">
		<div class="GeneralMilkBtmBox">
			<h3 class="Sprite2"><span id="title" class="Sprite2">Kết quả ghé thăm của NVBH</span></h3>
			<div class="GeneralMilkInBox">
				<div class="ResultSection" id="visitResultGrid">
                  	<table id="grid"></table>
					<div id="pager"></div>
              	</div>
              	<div class="Clear"></div>
			</div>
		</div>
	</div>
</div>
<div style="display:none" id="viewMap">
	<div id="fancyboxMap" class="GeneralDialog General2Dialog" style="width:970px;height:575px">
		<div id="bigMapContainer" style="width:970px;height:575px"></div>
		<div class="Clear"></div>
	</div>
</div>
<input id="shopCodeUserLogin" type="hidden" value="<s:property value="shop.shopCode"/>" />
<script type="text/javascript">
$(document).ready(function(){
	$('#shopCode').bind('keyup', function(event){
		var arrParam = new Array();
		var filter = new Object();
		filter.name = 'filterShop';
		filter.value = 1;
		arrParam.push(filter);
		if(event.keyCode == keyCode_F9){
			CommonSearch.searchShopOnDialog(function(data){
				$('#shopCode').val(data.code);
				$('#staffSaleCode').val('');
			},arrParam);
		}
	});
	$('#staffSaleCode').bind('keyup', function(event) {
		if (event.keyCode == keyCode_F9) {
			$('#errMsg').html('').hide();
			var msg = Utils.getMessageOfRequireCheck('shopCode', 'Mã đơn vị');
			if(msg.length>0){
				showMsgTimeOut('errMsg','Bạn chưa chọn đơn vị',1000);
				return false;
			}
			var arrParam = new Array();
			var param = new Object();
			param.name = 'shopCode';
			param.value = $('#shopCode').val().trim();
			arrParam.push(param);
			CommonSearch.searchPreSaleStaffOnDialog(function(data) {
				$('#staffSaleCode').val(data.code);
			}, arrParam);
		}
	});
	var shopCodeUserLoginTmp = $('#shopCodeUserLogin').val().trim();	
	var saleDateTmp = $('#saleDate').val().trim();	
	$("#grid").jqGrid({
	    url:VisitResult.getGridUrl(shopCodeUserLoginTmp,'',saleDateTmp),
	  	colModel:[	
	    	{name:'shopCode', index : 'shopCode', label: 'Mã đơn vị ', width: 75, sortable:false,resizable:false , align: 'left'},
	    	{name:'staffCode', index : 'staffCode', label: 'Mã NVBH', width: 90, sortable:false,resizable:false , align: 'left'},
	    	{name:'staffName', index : 'staffName', label: 'Tên NVBH', sortable:false,resizable:false, align: 'left'},
	    	{name:'totalVisit', index : 'totalVisit', label: 'Số điểm bán', width: 70, align: 'center', sortable:false,resizable:false},
	    	{name:'numCorrectRouting', index : 'numCorrectRouting', label: 'Đúng tuyến', width: 65, align: 'center', sortable:false,resizable:false},
	    	{name:'numWrongRouting', index : 'numWrongRouting', label: 'Sai tuyến', width: 55, align: 'center', sortable:false,resizable:false},
	    	{name:'numOrder', index : 'numOrder', label: 'Đơn hàng', width: 55, align: 'center', sortable:false,resizable:false},
	    	{name:'numLess2Minute', index : 'numLess2Minute', label: '< 2 phút', width: 50, align: 'center', sortable:false,resizable:false},
	    	{name:'numBetween2And30Minute', index : 'numBetween2And30Minute', label: ' 2-30 phút', width: 50, align: 'center', sortable:false,resizable:false},
	    	{name:'numOver30Minute', index : 'numOver30Minute', label: '> 30 phút', width: 50, align: 'center', sortable:false,resizable:false},
	    	{name:'view', label: 'Xem BĐ', width: 45, align: 'center',sortable:false,resizable:false,formatter:VisitPlanFormatter.showMap},
	  	],	  
	  	pager : '#pager',
	  	height: 'auto',
	  	rownumbers: true,	  
	  	width: ($('#visitResultGrid').width()),
	  	gridComplete : function() {
	  		$('#jqgh_grid_rn').html(jsp_common_numerical_order);
		  	bindAutoHideErrMsg();
		  	updateRownumWidthForJqGrid();
		  	$('#shopCode').focus();
		  	$('.ui-th-column-header').each(function(){
				$(this).css('text-align', 'center');
			});
		    $('.ui-jqgrid tr.ui-row-ltr').each(function(){
		   		var id =  $(this).attr('id');
				var numOver30Minute = $("#grid").jqGrid ('getCell', id, 'numOver30Minute');
				var numWrongRouting = $("#grid").jqGrid ('getCell', id, 'numWrongRouting');
				if(numWrongRouting > 0){
					$('.ui-jqgrid #'+ id+' td').each(function(){
	  					$(this).css('background-color', 'yellow');
	  				});
				} else if(numOver30Minute >0){
	  				$('.ui-jqgrid #'+ id+' td').each(function(){
	  					$(this).css('background-color', 'red');
	  				});
			  	}
			});
	  	}
	}).navGrid('#pager', {edit:false,add:false,del:false, search: false});
	$("#grid").jqGrid('setGroupHeaders', {
		useColSpanStyle: true, 
		groupHeaders:[
			{startColumnName: 'totalVisit', numberOfColumns: 4, titleText: 'Điểm bán', align:'center'},
			{startColumnName: 'numLess2Minute', numberOfColumns: 3, titleText: 'Thời gian ghé thăm', align:'center'}
		]
	});
});
</script>
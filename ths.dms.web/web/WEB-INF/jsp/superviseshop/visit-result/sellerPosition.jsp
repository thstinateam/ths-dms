<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@page import="ths.dms.helper.Configuration"%>
<script type="text/javascript" src="<%=Configuration.getStaticServerPath("/resources/scripts/business/common/vnm.mapapi.js")%>"></script>
<style>
	.ui-state-sellerpos-highlight {color: #EDB3B3;}
}
</style>
<div class="Breadcrumbs">
	<span class="Breadcrumbs1Item Sprite1">Giám sát vị trí của nhân viên</span>
</div>
<div class="GeneralMilkBox">
   	<div class="GeneralMilkTopBox">
       	<div class="GeneralMilkBtmBox">
               <h3 class="Sprite2"><span class="Sprite2">Thông tin tìm kiếm</span></h3>
            <div class="GeneralMilkInBox ResearchSection">
            	<div class="Web8Form">
                    <label class="LabelStyle Label1Style">Đơn vị (F9)</label>
                    <input id="shopCode" type="text" class="InputTextStyle InputText1Style" value="<s:property value="shop.shopCode"/>" />
                    <label class="LabelStyle Label2Style">Đối tượng</label>
                    <div class="BoxSelect BoxSelect1">
                     	<select id="staffType" class="MySelectBoxClass">
	                     	<option value="-1" selected="selected">Tất cả</option>
	                     	<option value="7">TBHV</option>
	                     	<option value="5">GSNPP</option>
	                     	<option value="1">NVBH</option>
                     	</select>
                     </div>
                    <label class="LabelStyle Label3Style">Mã NV (F9)</label>
                    <input id="staffCode" type="text" class="InputTextStyle InputText1Style" />
                    <label class="LabelStyle Label4Style"><input id="allLevel" type="checkbox" /> Tất cả các cấp</label>
                    <div class="Clear"></div>
                    <div class="ButtonSection">
                    	<button class="BtnGeneralStyle Sprite2" onclick="return SellerPosition.searchSellerPosition()">
                    		<span class="Sprite2">Tìm kiếm</span>
                    	</button>
                    	<img id="searchLoading" class="LoadingStyle" style="visibility: hidden;" src="/resources/images/loading.gif">
                    </div>
                    <p id="errMsg" class="ErrorMsgStyle Sprite1" style="display: none;"></p>
					<p id="successMsg" class="SuccessMsgStyle" style="display: none"></p>
                </div>
            </div>
		</div>
    </div>
</div>
<div class="GeneralMilkBox">
	<div class="GeneralMilkTopBox">
    	<div class="GeneralMilkBtmBox">
            <h3 class="Sprite2"><span class="Sprite2">Kết quả ghé thăm của NVBH</span></h3>
            <div class="GeneralMilkInBox">
            	<div class="GeneralMilkCol11">
	            	<div class="ResultSection" id="sellerPositionGrid" style="width: 424px;">
	                  	<table id="grid"></table>
						<div id="pager"></div>
	              	</div>
              	</div>
                <div class="GeneralMilkCol12" style="height: 300px;">
                	<div id="sellerPosMapContainer" style="width: 482px;height: 300px;"></div>
                </div>
                <div class="Clear"></div>
            </div>
		</div>
    </div>
</div>
<input id="shopCodeUserLogin" type="hidden" value="<s:property value="shop.shopCode"/>" />
<script type="text/javascript">
$(document).ready(function(){
	var firstLoad = true;
	var shopCode = $('#shopCode').val().trim();
	var staffType = $('#staffType').val().trim();
	var staffCode = $('#staffCode').val().trim();
	var allLevel = $('#allLevel').val().trim();
	$('#staffType').customStyle();
	$('#shopCode').bind('keyup', function(event){
		var arrParam = new Array();
		var param = new Object();
		param.name = 'shopCode';
		param.value = $('#shopCodeUserLogin').val();
		arrParam.push(param);
		var filter = new Object();
		filter.name = 'filterShop';
		filter.value = 1;
		arrParam.push(filter);
		if(event.keyCode == keyCode_F9){
			CommonSearch.searchShopOnDialog(function(data){
				$('#shopCode').val(data.code);
				$('#staffCode').val('');
			},arrParam);
		}
	});
	$('#staffCode').bind('keyup', function(event) {
		var arrParam = new Array();
		var shopCode =  $('#shopCode').val().trim();
		var shopCodeUserLogin = $('#shopCodeUserLogin').val().trim();
		var allLevel = $('#allLevel').attr('checked');
		if(allLevel=="checked"){
			var isGetShopOnly = false;
		}else{
			var isGetShopOnly = true;
		}
		if (shopCode ==''|| shopCode == shopCodeUserLogin) {
			var param = new Object();
			param.name = 'shopCode';
			param.value = shopCodeUserLogin;
			arrParam.push(param);
			var filter = new Object();
			filter.name = 'fromSellerPos';
			filter.value = true;
			arrParam.push(filter);
			var param = new Object();
			param.name = 'isGetShopOnly';
			param.value = isGetShopOnly ;
			arrParam.push(param);
			if (event.keyCode == keyCode_F9) {
				CommonSearch.searchSaleStaffOnDialog(function(data) {
					$('#staffCode').val(data.code);
				}, arrParam);
			}
		}else{
			var param = new Object();
			param.name = 'shopCode';
			param.value = shopCode ;
			arrParam.push(param);
			var filter = new Object();
			filter.name = 'fromSellerPos';
			filter.value = true;
			arrParam.push(filter);
			var param = new Object();
			param.name = 'isGetShopOnly';
			param.value = isGetShopOnly ;
			arrParam.push(param);
			if (event.keyCode == keyCode_F9) {
				CommonSearch.searchSaleStaffOnDialog(function(data) {
					$('#staffCode').val(data.code);
				}, arrParam);
			}
		}
	});
	$("#grid").jqGrid({	 	
	    url:SellerPosition.getGridUrl(shopCode,staffType,allLevel,staffCode),
	  	colModel:[	
	    	{name:'shopCode', index : 'shopCode', label: 'Mã đơn vị ', width: 60, sortable:false,resizable:false , align: 'left'},
	    	{name:'objectType', index : 'objectType', label: 'Đối tượng', width: 84, sortable:false,resizable:false , align: 'left',formatter:SellerPositionFormatter.getSellerType},
	    	{name:'staffCode', index : 'staffCode', label: 'Mã NV', width: 100, sortable:false,resizable:false, align: 'left',formatter:SellerPositionFormatter.getSellerPositionDetail},
	    	{name:'staffId', index : 'staffId', label: 'objectType', width: 150, align: 'right', sortable:false,resizable:false,hidden:true},
	    	{name:'id', index : 'id', label: 'objectType', width: 150, align: 'right', sortable:false,resizable:false,hidden:true},
	    	{name:'hasPosition', index : 'hasPosition', label: 'objectType', width: 150, align: 'right', sortable:false,resizable:false,hidden:true},
	    	{name:'staffName', index : 'staffName', label: 'Tên NV', width: 150, align: 'left', sortable:false,resizable:false},
	  	],	  
	  	pager : '#pager',
	  	height: 'auto',
	  	rownumbers: true,	  
	  	width: ($('#sellerPositionGrid').width()),
	  	gridComplete : function() {
		  	$('#jqgh_grid_rn').html(jsp_common_numerical_order);
		  	bindAutoHideErrMsg();
		  	updateRownumWidthForJqGrid();
		  	if(Vietbando.sellerPositionMap!=null){
		  		Vietbando.sellerPositionMap.clearOverlays();
		  		Vietbando._map.closeInfoWindow();
		  		} 
		  	//focus va search theo staffId dau tien
		  	var staffId = $('#grid tr:nth-child(2)').attr('id');
		  	var hasPosition = $('#grid tr:nth-child(2) td:nth-child(7)').html();
		  	$("#grid").setSelection(staffId, true);
		  	$('#grid tr:nth-child(2)').addClass('ui-state-salepos');
		  	if(firstLoad){
				//load map va search
				SellerPosition.loadMap(staffId,hasPosition);
				firstLoad = false;
		  	}else if(hasPosition == "1"){//first row has today position
		  		//get staff info
				$.ajax({
					type : "POST",
					url : "/superviseshop/visit-result/getstaffinfo",
					data : {staffId:staffId},
					dataType : "json",
					success : function(data) {
						if (data.success)
					    {
							SellerPosition.getSellerPositionDetail(staffId,data.staffCode,data.staffName,data.shopCode,data.objectType);
					    }
					},
					error:function(XMLHttpRequest, textStatus, errorThrown) {
						
					}
				});
		  	}
	  	}
	}).navGrid('#pager', {edit:false,add:false,del:false, search: false});
	$('.ui-th-ltr').each(function(){
		$(this).css('background', 'url(/resources/images/bg-th.jpg)');
	});
});
</script>
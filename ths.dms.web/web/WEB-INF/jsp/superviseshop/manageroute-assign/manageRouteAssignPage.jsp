<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<div class="BreadcrumbSection">
    <ul class="ResetList FixFloat BreadcrumbList">
        <li class="Sprite1"><a href="/superviseshop/manageroute-create/info">Thiết lập tuyến</a></li>
        <li><span>Giao tuyến</span></li>
    </ul>
</div>
<div class="CtnTwoColsSection">
	<div class="SidebarSection">
		<h2 class="Title2Style">Cây tuyến</h2>
		<div class="SidebarInSection">
			<div class="ReportTreeSection" id="treeContainer" >
				<div id="routingTree"></div>
			</div>
			<div class="Clear"></div>
		</div>
	</div>
	<div class="ContentSection">
		<div class="ReportCtnSection" id="routingContent">
			<tiles:insertTemplate template="/WEB-INF/jsp/superviseshop/manageroute-create/assign/manageRouteAssignDetail.jsp"></tiles:insertTemplate>
		</div>
	</div>
	<div class="Clear"></div>
</div>
<script type="text/javascript">
$(document).ready(function() {
	TreeUtils.loadRoutingTree('routingTree', 'routingId',$('#routingId').val(),function(data) {
		SuperviseManageRouteAssign.detail(data);
	});
});
</script>
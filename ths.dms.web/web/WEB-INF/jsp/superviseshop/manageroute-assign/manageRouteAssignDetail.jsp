<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@page import="ths.dms.helper.Configuration"%>
<script type="text/javascript" src="<%=Configuration.getStaticServerPath("/resources/scripts/plugins/jquery-easyui-1.3.2/easyloader.js")%>"></script>
<script type="text/javascript" src="http://viettelmap.vn/VTMapService/VTMapAPI?api=VTMap&type=main&k=fe8be5338b3467c2820dec3c4e00e3d8"></script>
<h2 class="Title2Style">Thông tin tuyến</h2>
<div class="GeneralForm SearchInSection">
	<label class="LabelStyle Label1Style">Mã tuyến</label> 
	<input type="text" id ="routingCode" class="InputTextStyle InputText1Style" disabled="disabled" value="<s:property value="routing.routingCode" />" />
	<label class="LabelStyle Label1Style">Tên tuyến</label>
	<input type="text" id ="routingName" class="InputTextStyle InputText2Style" disabled="disabled" value="<s:property value="routing.routingName" />"/>
	<div class="Clear"></div>
</div>
<h2 class="Title2Style">Thêm mới nhân viên bán hàng</h2>
<div class="GeneralForm SearchInSection">
	<label class="LabelStyle Label1Style">Mã NV<span class="ReqiureStyle">*</span></label>
	<div class="BoxSelect BoxSelect2">
		<select class="easyui-combobox" id="saleStaffId">
			<option value="0" >----Chọn Nhân viên----</option>
			<s:iterator value="lstSaleStaff"  var="obj" >
				<option value="<s:property value="#obj.id" />"><s:property value="codeNameDisplay(#obj.staffCode,#obj.staffName)" /></option>
			</s:iterator>	
		</select>
	</div>
	<!-- <label class="LabelStyle Label1Style" style="width :15%">Trạng thái<span class="ReqiureStyle">*</span></label>
    <div class="BoxSelect BoxSelect2">
    	<select id="status" class="MySelectBoxClass">
    		<option value="1" selected="selected">Hoạt động</option> 
    		<option value="0">Tạm ngưng</option>
    	</select>
    </div>  -->
	<div class="Clear"></div>
	<label class="LabelStyle Label1Style">Từ ngày<span class="ReqiureStyle">*</span></label>
    <input id="fromDate" type="text" class="InputTextStyle InputText1Style" maxlength="10" onchange="SuperviseManageRouteAssign.changeByDateStaff(this, 1)" value="<s:property value="fromDate" /> " style="width: 175px"/>
    <input id="flagFromDate" type="hidden" />
    <label class="LabelStyle Label1Style" style="width:15%">Đến ngày<span class="ReqiureStyle">*</span></label>
    <input id="toDate" type="text" class="InputTextStyle InputText1Style" maxlength="10" onchange="SuperviseManageRouteAssign.changeByDateStaff(this, 2)" value="<s:property value="toDate" />" style="width: 175px" />
    <input id="flagToDate" type="hidden" />
    <div class="Clear"></div>
    <div class="BtnCenterSection">
    	<button class="BtnGeneralStyle" id="btnAdd" onclick="SuperviseManageRouteAssign.addOrUpdateStaff(false)">Thêm</button>
    	<button class="BtnGeneralStyle" id="btnUpdate" onclick="SuperviseManageRouteAssign.addOrUpdateStaff(true)" style="display: none;">Cập nhật</button>
    	<button class="BtnGeneralStyle" id="btnCancel" onclick="SuperviseManageRouteAssign.resetStaff()" style="display: none;">Bỏ qua</button>
    </div>
    <div class="Clear"></div>
    <p id="errMsg" style="display: none;" class="ErrorMsgStyle SpriteErr"></p>
    <p id="successMsg" class="SuccessMsgStyle" style="display:none;"></p>
    <div style="float: left; width: 100%; height: auto;" id="idDivErrorInsert"></div>
</div>
<h2 class="Title2Style">Danh sách nhân viên</h2>
<div class="GeneralForm SearchInSection" >
	<div class="GridSection" id="staffContainerGrid">
		<table id="staffGrid"></table>
	</div>
</div>
<div class="TabSection">
	<ul class="ResetList TabSectionList">
		<li><a href="javascript:void(0)" id="tabActive1" onclick="SuperviseManageRouteAssign.showTab1();" class="Sprite1 Active"><span class="Sprite1">Danh sách khách hàng</span></a></li>
        <li><a href="javascript:void(0)" id="tabActive2" onclick="SuperviseManageRouteAssign.showTab2();" class="Sprite1 "><span class="Sprite1">Xem trên bản đồ</span></a></li>
    </ul>
	<div class="Clear"></div>
</div>
<div id="container1">
	<div class="GeneralForm SearchInSection">
		<div class="GridSection" id="customerContainerGrid">
			<table id="customerGrid"></table>
		</div>
		<div class="Clear"></div>
	</div>
</div>
<div id="container2" style="display:none;">
	<div class="GeneralForm SearchInSection">
		<p id="errMsgMap" class="ErrorMsgStyle SpriteErr" style="display: none">Không có khách hàng.</p>
		<div id="divMapRoute" style="position:relative;">
		<div id="mapRoute" style="width:100%;height:424px;position: relative;" ></div>
		<div id="divDate" style="width:160px;position:absolute;top:10px;right:10px;font-size: 11pt; background-color: ivory;z-index:8000;">
			<div style="padding: 5px 0px 5px 5px;border-style: solid;border-width: 1px; border-color: darkGray;"><img src="/resources/images/icon-datach.png" />
			<strong id="strongDate" style="padding-left:10px;" >Thứ 2</strong><a href="javascript:void(0)">
			<img src="/resources/images/icon-down.png" onclick="SuperviseManageRouteCreate.toggleDivUlDate()" style="float: right;padding-right: 10px" /></a></div>
			<div id="divUlDate" style="display:none;">
				<ul class="ulDateImage" style="width:120px; padding-left:36px;list-style-type:none;">
					<li onclick="SuperviseManageRouteCreate.fillListMarker('t2')"><a href="javascript:void(0)"><div>Thứ 2</div></a></li>
					<li onclick="SuperviseManageRouteCreate.fillListMarker('t3')"><a href="javascript:void(0)"><div>Thứ 3</div></a></li>
					<li onclick="SuperviseManageRouteCreate.fillListMarker('t4')"><a href="javascript:void(0)"><div>Thứ 4</div></a></li>
					<li onclick="SuperviseManageRouteCreate.fillListMarker('t5')"><a href="javascript:void(0)"><div>Thứ 5</div></a></li>
					<li onclick="SuperviseManageRouteCreate.fillListMarker('t6')"><a href="javascript:void(0)"><div>Thứ 6</div></a></li>
					<li onclick="SuperviseManageRouteCreate.fillListMarker('t7')"><a href="javascript:void(0)"><div>Thứ 7</div></a></li>
					<li onclick="SuperviseManageRouteCreate.fillListMarker('cn')"><a href="javascript:void(0)"><div>Chủ nhật</div></a></li>
				</ul>
			</div>
		</div>
		<div class="Clear"></div>
		</div>
	</div>
	<div class="Clear"></div>
</div>
<input type="hidden" id="routingId" value="<s:property value="routing.id"/>"/>
<script type="text/javascript">
$(document).ready(function(){
// 	$('#status').customStyle();
	$('#saleStaffId').focus();
	setDateTimePicker('fromDate');
	setDateTimePicker('toDate');
	$('#fromDate').val(getNextBySysDateForNumber(1)).show();
	$('#toDate').val(getNextBySysDateForNumber(1)).show();
	$('#flagFromDate').val(getNextBySysDateForNumber(1));
	$('#flagToDate').val(getNextBySysDateForNumber(1));
	var params = new Object();
	params.routingId = $('#routingId').val();
	$('#staffGrid').datagrid({
		url :'/superviseshop/manageroute-create/assign/search-staff',
		checkOnSelect : false,
		singleSelect :true,
        pagination : true,
        queryParams:params,
        rownumbers : true,
        pageNumber : 1,
        scrollbarSize: 0,
        autoWidth: true,
        autoRowHeight : true,
        fitColumns : true,
		width : ($('#staffContainerGrid').width()),
		columns:[[  
        	{field: 'staffCode',title:'Mã NV', width:150,align:'left',sortable : false,resizable : false ,formatter:function(value,rowData){
        		if(rowData.staff != undefined && rowData.staff != null) return rowData.staff.staffCode;
        		else return '';
    			} 
    		},  
        	{field: 'staffName',title:'Tên NV', width:300,align:'left',sortable : false,resizable : false,formatter:function(value,rowData){
        		if(rowData.staff != undefined && rowData.staff != null) return rowData.staff.staffName;
        		else return '';
				} 
			},  
        	{field: 'fromDate',title:'Từ ngày', width:120,align:'left',sortable : false,resizable : false, formatter:function(value) {
        		if(value != null && value != '') return $.datepicker.formatDate('dd/mm/yy', new Date(value));
        		else return '';
        		} 
    		},
    		{field: 'toDate',title:'Đến ngày', width:120,align:'left',sortable : false,resizable : false, formatter:function(value) {
    			if(value != null && value != '') return $.datepicker.formatDate('dd/mm/yy', new Date(value));
        		else return '';
        		} 
    		},
        	//{field: 'status',title:'Trạng thái', width:110,align:'left',sortable : false,resizable : false, formatter : ManageRouteAssign.statusStaffFormatter},
        	{field: 'edit', width:70,align:'center', formatter : ManageRouteAssign.editStaffInRouting},
//         	{field: 'delete', width:50,align:'center', formatter :  ManageRouteAssign.deleteStaffInRouting},
        ]],
        onLoadSuccess :function(){
	    	 $('#staffContainerGrid .datagrid-header-rownumber').html(jsp_common_numerical_order);
	    	 updateRownumWidthForDataGrid('#staffContainerGrid');
	    	 SuperviseManageRouteAssign.resize();
    	}
	});
	$('#customerGrid').datagrid({
		url :'/superviseshop/manageroute-create/assign/search-customer',
		checkOnSelect : false,
        queryParams:params,
        rownumbers : true,
        pageNumber : 1,
        scrollbarSize: 0,
        autoWidth: true,
        autoRowHeight : true,
        fitColumns : true,
		width : ($('#customerContainerGrid').width()),
		columns:[[ 
        	{field: 'shortCode',title:'Mã KH', width:120,align:'left',sortable : false,resizable : false ,formatter:function(value,rowData){
                if (rowData.customer!= undefined && rowData.customer != null ) {
                	return Utils.XSSEncode(rowData.customer.shortCode);
                } else { 
                	return '';
    			} 
    		}},  
        	{field: 'customerName',title:'Tên KH', width:270,align:'left',sortable : false,resizable : false,formatter:function(value,rowData){
        		if (rowData.customer != undefined && rowData.customer != null) {
        			return Utils.XSSEncode(rowData.customer.customerName);
        		} else {
        			return '';
				} 
			}},  
        	{field: 'monday',title:'T2', width:70, align:'center',sortable : false,resizable : false, formatter: ManageRouteAssign.checkDate},
        	{field: 'tuesday',title:'T3', width:70, align:'center',sortable : false,resizable : false, formatter: ManageRouteAssign.checkDate},
        	{field: 'wednesday',title:'T4', width:70, align:'center',sortable : false,resizable : false, formatter: ManageRouteAssign.checkDate},
        	{field: 'thursday',title:'T5', width:70, align:'center',sortable : false,resizable : false, formatter: ManageRouteAssign.checkDate},
        	{field: 'friday',title:'T6', width:70, align:'center',sortable : false,resizable : false, formatter: ManageRouteAssign.checkDate},
        	{field: 'saturday',title:'T7', width:70, align:'center',sortable : false,resizable : false, formatter: ManageRouteAssign.checkDate},
        	{field: 'sunday',title:'CN', width:70, align:'center',sortable : false,resizable : false, formatter: ManageRouteAssign.checkDate},
        	{field: 'weekInterval',title:'Tần suất', width:130, align:'right',sortable : false,resizable : false},
        	{field: 'startDate',title:'Từ ngày', width:130, align:'center',sortable : false,resizable : false, formatter:function(value) {
        		if(value != null && value != '') return $.datepicker.formatDate('dd/mm/yy', new Date(value));
        		else return '';
        		} 
    		},
        	{field: 'endDate',title:'Đến ngày', width:130, align:'center',sortable : false,resizable : false, formatter:function(value) {
        		if(value != null && value != '') return $.datepicker.formatDate('dd/mm/yy', new Date(value));
        		else return '';
        		} 
    		},
        	{field: 'P',hidden: true},
        ]],
        onLoadSuccess :function(){
	    	 $('#customerContainerGrid .datagrid-header-rownumber').html(jsp_common_numerical_order);
	    	 updateRownumWidthForDataGrid('#customerContainerGrid');
	    	 var tm = setTimeout(function(){
	    		 SuperviseManageRouteAssign.resize();
	    		 clearTimeout(tm);
			 }, 150);
    	}
	});
	
});
function MyOverlay(custId, latlng, img, title, info)
{
    this.latlng = latlng;
    this.latlngs = latlng;
    this.img = img;    

    this.initialize = initialize;
    this.remove = remove;
    this.copy = copy;
    this.redraw = redraw;
    this.vType = vType;
    this.info=info;
    this.custId=custId;
    this.title=title;

    function initialize(map)
    {
    	this.mapObj = map;
        var myDiv = document.createElement("div");
        myDiv.id = map.id + "MyOverlay";
        myDiv.style.position = "absolute";
        myDiv.style.textAlign = "center";
        var lat=latlng.latitude;
        var lng=latlng.longitude;
        myDiv.innerHTML = "<strong style='text-align: center;color: red;font-weight: bolder;'>" + this.title + "</strong><img alt='" + this.title + "' src='" + this.img + "' onclick=\"Vietbando.showWindowInfo("+custId+","+lat+","+lng+",'"+this.info+"');\"/>";
        map.getOverlayContainer().appendChild(myDiv);
        this.divObj = myDiv;
        this.redraw();
    }
    
    
    function remove()
    {
        this.divObj.parentNode.removeChild(this.divObj);
    }

    function copy()
    {
        return new MyOverlay(this.latlng, this.img);
    }

    function redraw()
    {
        var pt = this.mapObj.fromLatLngToDivPixel(this.latlng);
        if(this.custId < 0) {
        	this.divObj.style.left = (pt.x - 15 - 18 + 5) + "px";
            this.divObj.style.top = (pt.y - 50 + 16 - 39) + "px";
        } else {
        	this.divObj.style.left = (pt.x - 15 - 18) + "px";
            this.divObj.style.top = (pt.y - 50 + 16) + "px";        	
        }
    }

    function vType()
    {
        return 'MyOverlay';
    }
}
</script>
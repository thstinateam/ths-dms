<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<div class="Breadcrumbs">
   <span class="Breadcrumbs1Item Sprite1">Quản lý kế hoạch bán hàng</span>
</div>
<div class="GeneralMilkBox">
   	<div class="GeneralMilkTopBox">
       	<div class="GeneralMilkBtmBox">
           <h3 class="Sprite2"><span class="Sprite2">Tìm kiếm</span></h3>
           <div class="GeneralMilkInBox ResearchSection">
           <div class="Debt3Form">
           		<label class="LabelStyle Label1Style" style="width: 100px">Mã đơn vị(F9)</label>
           		<input id="shopCodeAndName" type="text" class="InputTextStyle InputText1Style" maxlength="201" value ="<s:property value="shop.shopCode"/>-<s:property value="shop.shopName"/>"/>
           		<label class="LabelStyle Label2Style" style="width: 110px ;padding-left: 30px;">Mã NVBH (F9)</label>
               	<div class="Field2">
	               	<input id="staffSaleCodeAndName" type="text" class="InputTextStyle InputText1Style" maxlength="151"/>
	               	<span class="RequireStyle">(*)</span>
               	</div>
               	<label class="LabelStyle Label3Style" style="width: 110px ">Kế hoạch tháng :</label>
                <div class="Field2">
               		<input id="monthPlan" type="text" style="width: 110px" class="InputTextStyle InputText1Style" value ="<s:property value="monthPlan"/>"/>
               		<span class="RequireStyle">(*)</span>
	            </div>
	            <div class="Clear"></div>
	            <div class="ButtonSection">
	            	<button id="btnSearch" class="BtnGeneralStyle Sprite2" onclick="return SuperviseManageSalePlan.getInfo()">
	            		<span class="Sprite2">Tìm kiếm</span>
	            	</button>
                </div>
               	<p class="ErrorMsgStyle Sprite1" id="errMsg" style="display: none;"></p>
            </div>
            </div>
		</div>
    </div>
</div>
<div class="GeneralMilkBox">
	<div class="GeneralMilkTopBox">
    	<div class="GeneralMilkBtmBox">
            <h3 class="Sprite2"><span id="title" class="Sprite2">Danh sách khách hàng của NVBH  trong tháng </span></h3>
          	<div class="GeneralMilkInBox">
              	<div class="ResultSection" id="visitPlanGrid">
                  	<table id="grid"></table>
					<div id="pager"></div>
              	</div>
              	<div class="Clear"></div>
          	</div>
		</div>
    </div>
</div>
<input id="shopCodeUserLogin" type="hidden" value="<s:property value="shop.shopCode"/>" />
<script type="text/javascript">
$(document).ready(function(){
	$('#shopCodeAndName').focus();
	$('#monthPlan').bind('blur',function(){
		var txtDate = $('#monthPlan').val().trim();
		var aoDate,           // needed for creating array and object
	        txtDateTmp;
	    aoDate = txtDate.split('/');
	    if (aoDate.length >= 3) {
	    	txtDateTmp = aoDate[1] +'/'+aoDate[2];
	    	if(aoDate[1] != null && aoDate[1] != undefined && aoDate[1] != '' && aoDate[2] != null && aoDate[2] != undefined && aoDate[2] != ''){
	    		$('#monthPlan').val(txtDateTmp);
	    	}
	    } 
	});
	applyDateTimePicker("#monthPlan", "mm/yy", null, null, null, null, null, null, null);
	$('#shopCodeAndName').bind('keyup', function(event){
		var arrParam = new Array();
		var param = new Object();
		param.name = 'shopCode';
		param.value = $('#shopCodeUserLogin').val();
		arrParam.push(param);
		var filter = new Object();
		filter.name = 'filterShop';
		filter.value = 1;
		arrParam.push(filter);
		if(event.keyCode == keyCode_F9){
			CommonSearch.searchShopOnDialog(function(data){
				$('#shopCodeAndName').val(Utils.XSSEncode(Utils.XSSEncode(data.code+ '-'+ data.name)));
				$('#staffSaleCodeAndName').val('');
			},arrParam);
		}
	});
	$('#staffSaleCodeAndName').bind('keyup', function(event) {
		var arrParam = new Array();
		var shopCode =  SuperviseManageSalePlan.getShopCodeByShopCodeAndName('shopCodeAndName');
		var shopCodeUserLogin = $('#shopCodeUserLogin').val().trim();
		if (shopCode ==''|| shopCode == shopCodeUserLogin) {
			var param = new Object();
			param.name = 'shopCode';
			param.value = shopCodeUserLogin;
			var filter = new Object();
			filter.name = 'filterStaff';
			filter.value = 1;
			arrParam.push(filter);
			if (event.keyCode == keyCode_F9) {
				CommonSearch.searchSaleStaffOnDialog(function(data) {
					$('#staffSaleCodeAndName').val(data.code+'-'+ data.name);
				}, arrParam);
			}
		} else{
			var param = new Object();
			param.name = 'shopCode';
			param.value = shopCode ;
			arrParam.push(param);
			if (event.keyCode == keyCode_F9) {
				CommonSearch.searchSaleStaffOnDialog(function(data) {
					$('#staffSaleCodeAndName').val(Utils.XSSEncode(data.code+'-'+ data.name));
				}, arrParam);
			}
		}
	});
	$("#grid").jqGrid({
		url:SuperviseManageSalePlan.getGridUrl('','',''),
		colModel:[
			{name:'customer.shortCode', label: 'Mã KH', width: 80, sortable:false,resizable:false , align: 'left', formatter: function(value, row, index) {
				return Utils.XSSEncode(value);
			}},
		    {name:'customer.customerName', label: 'Tên khách hàng', sortable:false,resizable:false , align: 'left', formatter: function(value, row, index) {
				return Utils.XSSEncode(value);
			}},
		    {name:'monday', label: 'T2', width: 60, align: 'center', sortable:false,resizable:false, formatter: ManageSalePlanFormatter.getTrain},
		    {name:'tuesday', label: 'T3', width: 60, align: 'center', sortable:false,resizable:false, formatter: ManageSalePlanFormatter.getTrain},
		    {name:'wednesday', label: 'T4', width: 60, align: 'center', sortable:false,resizable:false, formatter: ManageSalePlanFormatter.getTrain},
		    {name:'thursday', label: 'T5', width: 60, align: 'center', sortable:false,resizable:false, formatter: ManageSalePlanFormatter.getTrain},
		    {name:'friday', label: 'T6', width: 60, align: 'center', sortable:false,resizable:false, formatter: ManageSalePlanFormatter.getTrain},
		    {name:'saturday', label: 'T7', width: 60, align: 'center', sortable:false,resizable:false, formatter: ManageSalePlanFormatter.getTrain},
		    {name:'sunday', label: 'CN', width: 60, align: 'center', sortable:false,resizable:false, formatter: ManageSalePlanFormatter.getTrain},
		],	  
		pager : '#pager',
		height: 'auto',			  
		width: ($('#visitPlanGrid').width())
	}).navGrid('#pager', {edit:false,add:false,del:false, search: false});	
	
});
</script>
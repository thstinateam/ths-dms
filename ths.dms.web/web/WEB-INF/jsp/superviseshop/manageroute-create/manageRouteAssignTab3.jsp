<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<div class="ReportCtnSection" id="routingContent"  style="border: medium none;">
	<h2 class="Title2Style"><s:text name="ss.create.route.add.nvbh"/></h2>
	<div class="GeneralForm SearchInSection">
		<label class="LabelStyle Label1Style"><s:text name="ss.create.route.staff.code"/><span class="ReqiureStyle">*</span></label>
		<div class="BoxSelect BoxSelect2">
			<select class="easyui-combobox" id="saleStaffId">
				<option value="0" code="" name="----<s:text name="ss.create.route.choose.staff"/>---- -"></option>
				<s:iterator value="lstSaleStaff"  var="obj" >
					<option value="<s:property value="#obj.id" />" code="<s:property value="staffCode" />" name="<s:property value="staffName" />"><s:property value="codeNameDisplay(#obj.staffCode,#obj.staffName)" /></option>
				</s:iterator>	
			</select>
		</div>
		<div class="Clear"></div>
		<label class="LabelStyle Label1Style"><s:text name="jsp.common.from.date"/><span class="ReqiureStyle">*</span></label>
	    <input id="fromDate" type="text" class="InputTextStyle InputText1Style" maxlength="10" onchange="SuperviseManageRouteAssign.changeByDateStaff(this, 1)" value="" style="width: 175px"/>
	    <input id="flagFromDate" type="hidden" />
	    <label class="LabelStyle Label1Style" style="width:15%"><s:text name="jsp.common.to.date"/></label>
	    <input id="toDate" type="text" class="InputTextStyle InputText1Style" maxlength="10" onchange="SuperviseManageRouteAssign.changeByDateStaff(this, 2)" value="" style="width: 175px" />
	    <input id="flagToDate" type="hidden" />
	    <div class="Clear"></div>
	    <div class="BtnCenterSection cmsiscontrol" id="group_edit_rt_dt_tab_nv_div_grBtnChange">
	    	<button class="BtnGeneralStyle" title="<s:text name="ss.assign.route.hotkey.title"/>" id="btnAdd" onclick="SuperviseManageRouteCreate.addOrUpdateStaff(false)"><s:text name="jsp.common.them"/></button>
	    	<button class="BtnGeneralStyle" id="btnUpdate" onclick="SuperviseManageRouteCreate.addOrUpdateStaff(true)" style="display: none;"><s:text name="jsp.common.capnhat" /></button>
	    	<button class="BtnGeneralStyle" id="btnCancel" onclick="SuperviseManageRouteAssign.resetStaff()" style="display: none;"><s:text name="jsp.common.bo.qua"/></button>
	    </div>
	    <div class="Clear"></div>
	    <p id="errMsgStaff" style="display: none;" class="ErrorMsgStyle SpriteErr"></p>
	    <p id="successMsg" class="SuccessMsgStyle" style="display:none;"></p>
	    <div style="float: left; width: 100%; height: auto;" id="idDivErrorInsert"></div>
	</div>
</div>
<div class="Clear"></div>
<h2 class="Title2Style"><s:text name="ss.create.route.title.list.staff"/></h2>
<div class="GeneralForm SearchInSection"  style="border: medium none;">
	<div class="GridSection" id="staffContainerGrid"  style="border: medium none;">
		<table id="staffGrid"></table>
	</div>
</div>
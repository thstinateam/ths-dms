<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>

<input type="hidden" id="function_code" value="<s:property value="systimestamp"/>" />
<s:set id="downloadToken" value="%{generateReportToken()}"></s:set>
<input type="hidden" id="report_token" value="<s:property value="#downloadToken"/>" />
<s:set id="downloadToken" value="%{putSessionValue(systimestamp, #downloadToken)}"></s:set>

<div class="BreadcrumbSection">
    <ul class="ResetList FixFloat BreadcrumbList">
        <li class="Sprite1"><a href="/superviseshop/manageroute-create/info"><s:text name="ss.create.route.gs.ban.hang"/></a></li>
        <li><span><s:text name="ss.create.route.title"/></span></li>
    </ul>
</div>
<div class="CtnTwoColsSection" id="divRoutingCreatePage">
    <div class="SidebarSection">
    	<h2 class="Title2Style"><s:text name="jsp.common.shop"/></h2>
    	<div class="BoxSelect BoxSelect2" id ="shopIdList" style="margin: 5px" >
            <select id="shopCodeCB" style="width: 200px;"></select>
        </div>
        <div class="Clear"></div>
    	<h2 class="Title2Style"><s:text name="ss.create.route.title.list.route"/></h2>
    	<div class="SidebarInSection">
            <div class="ReportTreeSection" id="treeContainer">
				<div id="routingTree"></div>
			</div>
			<div class="BtnCenterSection">
				<button id="group_edit_rt_if_btnInsert" style="position:inherit" onclick="SuperviseManageRouteCreate.showDialogConfirm();" class="BtnGeneralStyle">+ <s:text name="ss.create.route.add.route"/></button>
			</div>
			<div class="Clear"></div>
    	</div>
    </div>
    <div class="ContentSection">
    	<div class="ReportCtnSection">
        	<h2 class="Title2Style"><s:text name="jsp.common.thongtintimkiem"/></h2>
            <div class="GeneralForm SearchInSection">
            	<label class="LabelStyle Label1Style"><s:text name="ss.routingcode"/></label>
                <input id="routingCode" type="text" class="InputTextStyle InputText1Style" />
                <label class="LabelStyle Label1Style"><s:text name="ss.routingname"/></label>
                <input id="routingName" type="text" class="InputTextStyle InputText8Style" />
                <div class="Clear"></div>
                <label class="LabelStyle Label1Style"><s:text name="jsp.common.nvbh.code"/></label>
                <input id="staffCode" type="text" class="InputTextStyle InputText1Style" />
                <label class="LabelStyle Label1Style"><s:text name="jsp.common.status"/></label>
                <div id="statusDiv" class="BoxSelect BoxSelect3">
                    <select id="status" class="MySelectBoxClass">
                    	<option value="-2" ><s:text name="jsp.common.status.all"/></option>
                        <option value="0"><s:text name="jsp.common.status.stoped"/></option>
                        <option value="1"><s:text name="jsp.common.status.active"/></option>
                    </select>
                </div>
                <div class="Clear"></div>
                <div class="BtnCenterSection">
                    <button onclick="SuperviseManageRouteCreate.searchRoute();" class="BtnGeneralStyle"><s:text name="jsp.common.timkiem"/></button>
                </div>
            </div>
            <h2 class="Title2Style"><s:text name="ss.create.route.title.list.route"/></h2>
            <div class="GeneralForm SearchInSection">
                <!--Đặt Grid tại đây-->
                <div class="GridSection" id="routeGrid">
					<table id="grid"></table>
					<div id="pager"></div>
                </div>
	             <div class="Clear"></div>
	             <div class="GeneralForm Search1Form" style="float: right; width: 100%" id="idDivImpExp">
		             <div class="Func1Section" style="float: right; min-width: 469px; padding-right: 5px;">
		                 <div class="cmsiscontrol" id="group_edit_rt_if_divImport">
			                 <p class="DownloadSFileStyle DownloadSFile2Style">
			                 	<a href="javascript:void(0)" class="Sprite1" id="downloadTemplate" onclick="SuperviseManageRouteCreate.downloadTemplate();"><s:text name="jsp.common.tai.file.excel"/></a>
			                 </p> 
			                 <form action="/superviseshop/manageroute-create/import-tuyen-by-excel" name="importFrm" id="importFrm" method="post" enctype="multipart/form-data">
			                 	<input type="hidden" id="isView" name="isView"/>
			                 	<input type="file" class="InputFileStyle" size="22" name="excelFile" id="excelFile" style="width: 100px !important;" onchange="previewImportExcelFile(this,'importFrm');">
			                 	<div class="FakeInputFile">
									<input id="fakefilepc" style="margin: -3px 5px 0 0;" readonly="readonly" type="text" class="InputTextStyle InputText1Style">
								</div>
			                 </form>
			                 <button class="BtnGeneralStyle" onclick="return SuperviseManageRouteCreate.importTuyenByExcel();" style="margin-top: -7px;" title="<s:text name="jsp.common.nhap.excel"/>"><s:text name="jsp.common.nhap.excel"/></button>
		                 </div>
		             	 <button id="btnExportExcel" onclick="return SuperviseManageRouteCreate.exportTuyenByExcel();" class="BtnGeneralStyle" style="float:right; margin-top: -7px;" title="<s:text name="jsp.common.nhap.excel"/>"><s:text name="jsp.common.xuat.excel"/></button>
		             </div>
	            </div>
	            <p class="ErrorMsgStyle" id="errMsg" style="display: none;"></p>
	            <p class="ErrorMsgStyle" id="errExcelMsg" style="display: none;"></p>
				<tiles:insertTemplate template="/WEB-INF/jsp/general/successMsg.jsp" />
             </div>
        </div>
    </div>
    <div class="Clear"></div>
</div>

<s:hidden id="staffRoleType" name="staffRoleType"></s:hidden>
<input type="hidden" id="staffCodeExport"/>
<input type="hidden" id="flagCheckAllTmp" value="0"/>
<input type="hidden" id="routingCodeExport"/>
<input type="hidden" id="routingNameExport"/>
<s:hidden id="statusLoad" name="status"></s:hidden>
<s:hidden id="shopCode" name="shopCode"></s:hidden>
<script type="text/javascript">
$(document).ready(function(){
	console.log($('#statusLoad').val());
	if ($('#statusLoad').val() != '') {
		$('#status').val($('#statusLoad').val());
		$('#status').change();
	}
	SuperviseManageRouteCreate._rCheckedMap = new Map();
	SuperviseManageRouteCreate._flagCheckAll = 0;
	//$('.MySelectBoxClass').customStyle();
	General._urlRedrect = '<s:property value="urlRedrectCurentPage" />';
	$('#routingCode').focus();
	$('#grid').datagrid({
		url : '/superviseshop/manageroute-create/searchroute',
		checkOnSelect : false,
        pagination : true,
        rownumbers : true,
        pageNumber : 1,
        scrollbarSize: 0,
        autoWidth: true,
        pageList: [10, 20, 50],
        checkOnSelect : true,
        autoRowHeight : true,
        fitColumns : true,
		width : ($('#routeGrid').width()),
		columns:[[  
        	{field: 'routingCode',title:'<s:text name="ss.routingcode"/>', width:120,align:'left',sortable : false,resizable : false, formatter: function(value, row, index) {
        		return Utils.XSSEncode(value);         
        	}},
        	{field: 'routingName',title:'<s:text name="ss.routingname"/>', width:200,align:'left',sortable : false,resizable : false, formatter: function(value, row, index) {
        		return Utils.XSSEncode(value);         
        	}},
        	{field: 'staffSale',title:'<s:text name="ss.create.route.current.nvbh"/>', width:200,align:'left',sortable : false,resizable : false, formatter:ManageRouteCreateFormatter.getStaffSale},
        	{field: 'status',title:'<s:text name="jsp.common.status"/>', width:120,align:'left',sortable : false,resizable : false, formatter:ManageRouteCreateFormatter.statusFormatter},
			{field: 'routingId', title:'', checkbox: true},
        	{field: 'edit',title:'<s:text name="jsp.common.sua"/>', width:50, align:'center',sortable : false,resizable : false, formatter:ManageRouteCreateFormatter.editRoute},
        	{field: 'remove',title:'<s:text name="jsp.common.xoa"/>', width:30,align:'center',sortable : false,resizable : false, formatter:ManageRouteCreateFormatter.deleteRoute},
        ]],
        onLoadSuccess :function(){
        	$('.datagrid-header-rownumber').html(jsp_common_numerical_order);
        	updateRownumWidthForJqGrid('#routeGrid');
        	//Phan quyen control
        	var arrEdit =  $('#routeGrid td[field="edit"]');
			if (arrEdit != undefined && arrEdit != null && arrEdit.length > 0) {
			  for (var i = 0, size = arrEdit.length; i < size; i++) {
			  	$(arrEdit[i]).prop("id", "group_edit_rt_if_gr_td_edit_" + i);//Khai bao id danh cho phan quyen
				$(arrEdit[i]).addClass("cmsiscontrol");
			  }
			}
			var arrDelete =  $('#routeGrid td[field="remove"]');
			if (arrDelete != undefined && arrDelete != null && arrDelete.length > 0) {
			  for (var i = 0, size = arrDelete.length; i < size; i++) {
			    $(arrDelete[i]).prop("id", "group_edit_rt_if_gr_td_remove_" + i);//Khai bao id danh cho phan quyen
				$(arrDelete[i]).addClass("cmsiscontrol");
			  }
			}
			Utils.functionAccessFillControl('routeGrid', function(data){
				//Xu ly cac su kien lien quan den cac control phan quyen
				var arrTmpLength =  $('#routeGrid td[id^="group_edit_rt_if_gr_td_remove_"]').length;
				var invisibleLenght = $('.isCMSInvisible[id^="group_edit_rt_if_gr_td_remove_"]').length;
				if (arrTmpLength > 0 && arrTmpLength != invisibleLenght) {
					$('#grid').datagrid("showColumn", "remove");
				} else {
					$('#grid').datagrid("hideColumn", "remove");
				}
				arrTmpLength =  $('#routeGrid td[id^="group_edit_rt_if_gr_td_edit_"]').length;
				invisibleLenght = $('.isCMSInvisible[id^="group_edit_rt_if_gr_td_edit_"]').length;
				if (arrTmpLength > 0 && arrTmpLength != invisibleLenght) {
					$('#grid').datagrid("showColumn", "edit");
				} else {
					$('#grid').datagrid("hideColumn", "edit");
				}
			});
        	//Xu ly cac du lieu mac dinh
        	var index = 0;
        	if(SuperviseManageRouteCreate._flagCheckAll != 1){
        		flagCheckAll = false;
        		$('td[field=routingId] div.datagrid-header-check input[type=checkbox]')[0].checked = false;
        	}else{
        		$('td[field=routingId] div.datagrid-header-check input[type=checkbox]')[0].checked = true;
        	}
        	var flagCheckAll = $('td[field=routingId] div.datagrid-header-check input[type=checkbox]')[0].checked;
	    	$('td[field=routingId] div.datagrid-cell-check input[type=checkbox]').each(function(){					    		
	    		$(this).attr('index',index);
	    		if (flagCheckAll) {
	    			$('#grid').datagrid('selectRow',index);
	    		} else {
	    			var routing = $('#grid').datagrid('getRows')[index];
	    			var staffId = 0;
					if(routing.staff_id!=undefined && routing.staff_id!=null){
						staffId = routing.staff_id;
					}
		    		if(SuperviseManageRouteCreate._rCheckedMap.get(routing.routingId+staffId)!=null){
		    			$('#grid').datagrid('selectRow',index);
		    		}
	    		}
	    		++index;
	    	});
	       	var tm = setTimeout(function(){
	       		SuperviseManageRouteCreate.resize();
	       		$('#treeContainer').css('min-height',$('.ContentSection').height() - 125);
	    		$('#btnAddRouting').css('bottom', $('#treeContainer').height() - $('.ContentSection').height() + 100);
	       		clearTimeout(tm);
			}, 500);
	    	$(window).resize();
    	},
    	onCheck: function(i, r) {
			var obj = new Object();
			obj.routingId = r.routingId;
			obj.routingCode = r.routingCode;
			if(r.staff_id!=undefined && r.staff_id!=null){
				obj.staff_id = r.staff_id;
			}else{
				obj.staff_id = 0;
			}
			SuperviseManageRouteCreate._rCheckedMap.put(obj.routingId+obj.staff_id, obj);
			if(SuperviseManageRouteCreate._flagCheckAll != 1){
        		$('td[field=routingId] div.datagrid-header-check input[type=checkbox]')[0].checked = false;
        	}else{
        		$('td[field=routingId] div.datagrid-header-check input[type=checkbox]')[0].checked = true;
        	}
		 },
		 onUncheck: function(i, r) {
			var staffId = 0;
			if(r.staff_id!=undefined && r.staff_id!=null){
				staffId = r.staff_id;
			}
			SuperviseManageRouteCreate._rCheckedMap.remove(r.routingId+staffId);
			SuperviseManageRouteCreate._flagCheckAll = 0;
		 },
		 onCheckAll: function(rows) {
			 SuperviseManageRouteCreate._flagCheckAll = 1;
			 for (var i = 0, sz = rows.length; i < sz; i++) {
				var obj = new Object();
				obj.routingId = rows[i].routingId;
				obj.routingCode = rows[i].routingCode;
				if(rows[i].staff_id!=undefined && rows[i].staff_id!=null){
					obj.staff_id = rows[i].staff_id;
				}else{
					obj.staff_id = 0;
				}
				SuperviseManageRouteCreate._rCheckedMap.put(obj.routingId+obj.staff_id, obj);
			 }
		 },
		 onUncheckAll: function(rows) {
			 SuperviseManageRouteCreate._flagCheckAll = 0;
			 SuperviseManageRouteCreate._rCheckedMap = new Map();
		 }
	});
	
	$('#staffCode, #routingCode, #routingName').bind('keyup',function(event){
		if(event.keyCode == 13){
			SuperviseManageRouteCreate.searchRoute();
		}
	});
	
	initUnitCbx();
});
function initUnitCbx(){				
	var params = {};
	$.ajax({
		type : "POST",
		url:'/commons/list-child-shop',
		data :($.param(params, true)),
		dataType : "json",
		success : function(data) {
			$('#shopCodeCB').combobox({
				valueField: 'shopCode',
				textField:  'shopName',
				data: data.rows,
				panelWidth: '200',
				formatter: function(row) {
					return '<span style="font-weight:bold">' + Utils.XSSEncode(row.shopCode) + '</span><br/>' + '<span style="color:#888">' + Utils.XSSEncode(row.shopName) + '</span>';
				},
				filter: function(q, row){
					q = new String(q).toUpperCase().trim();
					var opts = $(this).combobox('options');
					return ((unicodeToEnglish(row[opts.textField]).indexOf(unicodeToEnglish(q)) == 0) || unicodeToEnglish(row[opts.valueField]).indexOf(unicodeToEnglish(q)) == 0) ;
				},	
		        onSelect: function(rec){
		        	$('#shopCode').val($('#shopCodeCB').combobox('getValue'));
		        	SuperviseManageRouteCreate.loadTreeRoute();
		        	SuperviseManageRouteCreate.searchRoute();
		        },
		        onLoadSuccess: function(){
		        	var arr = $('#shopCodeCB').combobox('getData');
		        	if (arr != null && arr.length > 0){
		        		var shopCode = arr[0].shopCode;
		        		if ($('#shopCode').val() != '') {
		        			shopCode = $('#shopCode').val();
		        		}
		        		$('#shopCodeCB').combobox('select', shopCode);
		        	}
		        }
			});
		}
	});
}
</script>
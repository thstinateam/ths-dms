<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
 <!--Đặt Grid tại đây-->
<div class="GeneralTable General1Table" id="gridCustomerContainerDiv" style="border: medium none;">
	<table id="grid" width="100%" border="0" cellspacing="0"
		cellpadding="0">
		<colgroup>
			<col style="width: 20px;" />
			<col style="width: 35px;" />
			<col style="width: 85px;" />
			<col style="width: 125px;" />
			<col style="width: 30px;" />
			<col style="width: 30px;" />
			<col style="width: 30px;" />
			<col style="width: 30px;" />
			<col style="width: 30px;" />
			<col style="width: 30px;" />
			<col style="width: 30px;" />
			<col style="width: 30px;" />
			<col style="width: 30px;" />
			<col style="width: 30px;" />
			<col style="width: 30px;" />
			<col style="width: 30px;" />
			<col style="width: 30px;" />
			<col style="width: 130px;" />
			<col style="width: 130px;" />
			<col style="width: 30px;" />
		</colgroup>
		<thead>
			<tr>
				<th rowspan="2"><s:text name="jsp.common.numerical.order"/></th>
				<th rowspan="2"><s:text name="jsp.common.customer.code"/></th>
				<th rowspan="2"><s:text name="jsp.common.customer.name"/></th>
				<th rowspan="2"><s:text name="jsp.common.address"/></th>
				<th rowspan="2"><s:text name="ss.create.route.tt"/></th>
				<th colspan="7"><s:text name="ss.create.route.import.ngay.ghe.tham"/></th>
				<th colspan="4"><s:text name="ss.create.route.import.chu.ky.ghe.tham"/></th>
				<th rowspan="2"><s:text name="catalog.customer.tansuat"/></th>
				<th rowspan="2"><s:text name="jsp.common.start.date"/></th>
				<th rowspan="2"><s:text name="jsp.common.end.date"/></th>
				<th rowspan="2" class="EndCols" field="edit"><a title="<s:text name="ss.create.route.add.cust"/>"
					onclick="SuperviseManageRouteCreate.openAddCustomerDialog(false);"
					href="javascript:void(0);"><img
						src="/resources/images/icon_add.png" width="17" height="17"
						alt="Add" />
				</a>
				</th>
			</tr>
			<tr>
				<th style="border-top: 1px solid #c6d5db;"><s:text name="jsp.common.thu.2"/></th>
				<th style="border-top: 1px solid #c6d5db;"><s:text name="jsp.common.thu.3"/></th>
				<th style="border-top: 1px solid #c6d5db;"><s:text name="jsp.common.thu.4"/></th>
				<th style="border-top: 1px solid #c6d5db;"><s:text name="jsp.common.thu.5"/></th>
				<th style="border-top: 1px solid #c6d5db;"><s:text name="jsp.common.thu.6"/></th>
				<th style="border-top: 1px solid #c6d5db;"><s:text name="jsp.common.thu.7"/></th>
				<th style="border-top: 1px solid #c6d5db;"><s:text name="jsp.common.thu.cn"/></th>
				<th style="border-top: 1px solid #c6d5db;"><s:text name="ss.create.route.import.w1"/></th>
				<th style="border-top: 1px solid #c6d5db;"><s:text name="ss.create.route.import.w2"/></th>
				<th style="border-top: 1px solid #c6d5db;"><s:text name="ss.create.route.import.w3"/></th>
				<th style="border-top: 1px solid #c6d5db;"><s:text name="ss.create.route.import.w4"/></th>
			</tr>
		</thead>
		<tbody id="lstRoutingCustomerDetail">

		</tbody>
	</table>
</div>
<div class="Clear"></div>
<p id="errMsg" style="display: none;" class="ErrorMsgStyle"></p>
<tiles:insertTemplate template="/WEB-INF/jsp/general/successMsg.jsp" />

<div class="BtnCenterSection" style="border: medium none;">
	<button id="group_edit_rt_dt_btnAOSRouting" class="BtnGeneralStyle cmsiscontrol" onclick="SuperviseManageRouteCreate.addOrSaveRouting();" title="<s:text name="ss.create.route.hotkey.update.cust"/>"><s:text name="jsp.common.capnhat" /></button>
</div>
<div class="Clear"></div>
  <div id="showDialogId">
	<div id="addCustomerDialog" class="easyui-dialog" title="Chọn khách hàng" data-options="closed:true, modal:true" style="width:616px;height:auto;">
	  <div class="PopupContentMid2">
        	<div class="GeneralForm Search1Form">
	  			<div class="Clear"></div>
<!-- 				<h2 class="Title2Style">Thông tin tìm kiếm</h2> -->
				<label class="LabelStyle Label2Style"><s:text name="jsp.common.customer.code"/></label>
                <input id="shortCodeDialog" type="text" class="InputTextStyle InputText5Style" maxlength="40"/>
                <label class="LabelStyle Label2Style"><s:text name="jsp.common.customer.name"/></label>
                <input id="customerNameDialog" type="text" class="InputTextStyle InputText5Style" maxlength="250"/>
                <div class="Clear"></div>
                <div class="BtnCenterSection">
                 	<button class="BtnGeneralStyle Sprite2 BtnSearchOnDialog" onclick="return SuperviseManageRouteCreate.searchCustomerDialog();">
                 		<span class="Sprite2"><s:text name="jsp.common.timkiem"/></span>
                 	</button>
                 </div>
                 <div class="Clear"></div>
			</div>
			<div class="GeneralForm Search1Form">
<!-- 				<h2 class="Title2Style Title2MTStyle"><s:text name="ss.create.route.ds.kh"/></h2> -->
	           	<div class="GridSection">
	            	<table id="gridCustomer"></table>
				</div>	
				<div class="Clear"></div>
                <div class="BtnCenterSection">
                    <button id="group_edit_rt_dt_btnInsertCusDl" class="BtnGeneralStyle cmsiscontrol" onclick="return SuperviseManageRouteCreate.addCustomerDialogInsert();"><s:text name="jsp.common.chon.simple"/></button>
                </div>       
	            <p id="errMsg2" style="display: none;" class="ErrorMsgStyle"></p>
	         </div>
       </div>
       </div>
</div>

<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<script src="/resources/scripts/business/common/vnm.mapapi.js" type="text/javascript"></script>
<script type="text/javascript" src="http://viettelmap.vn/VTMapService/VTMapAPI?api=VTMap&type=main&k=fe8be5338b3467c2820dec3c4e00e3d8"></script>
<div class="BreadcrumbSection">
      <ul class="ResetList FixFloat BreadcrumbList">
          <li class="Sprite1"><a href="/superviseshop/manageroute-create/info"><s:text name="ss.create.route.title"/></a></li>
          <li><span><s:text name="ss.create.route.info"/></span></li>
      </ul>
  </div>
<div class="CtnTwoColsSection" id="divEditRouting">
      <div class="SidebarSection">
      	<h2 class="Title2Style"><s:text name="jsp.common.shop"/></h2>
    	<div class="BoxSelect BoxSelect2" id ="shopIdList" style="margin: 5px" >
            <select id="shopCodeCB" style="width: 200px;"></select>
        </div>
        <div class="Clear"></div>
      	<h2 class="Title2Style"><s:text name="ss.create.route.title.list.route"/></h2>
      	<div class="SidebarInSection">
			<div class="ReportTreeSection" id="treeContainer" style=" overflow: scroll;">
					<div id="routingTree"></div>
			</div>
			<div class="BtnCenterSection">
				<button style="position:inherit" id="group_edit_rt_dt_btnAddRouting" title="<s:text name="ss.create.route.hotkey.add.route"/>" onclick="SuperviseManageRouteCreate.showDialogConfirm();" class="BtnGeneralStyle">+ <s:text name="ss.create.route.add.route"/></button>
			</div>
			<div class="Clear"></div>
      	</div>
      </div>
      <div class="ContentSection">
      	<div class="ReportCtnSection">
          	<h2 class="Title2Style"><s:text name="ss.create.route.info"/></h2>
              <div class="GeneralForm SearchInSection" style="border: medium none;">
              	<label class="LabelStyle Label1Style"><s:text name="ss.routingcode"/></label>
                <input id="routingCode" disabled="disabled" maxlength="40" type="text" value="<s:property value="route.routingCode" />" class="InputTextStyle InputText1Style" />
                <label class="LabelStyle Label1Style"><s:text name="ss.routingname"/></label>
                <input id ="routingName" maxlength="150" type="text" value="<s:property value="route.routingName" />" class="InputTextStyle InputText1Style" />
				<button id="group_edit_rt_dt_btnUpdRouting" onclick="SuperviseManageRouteCreate.updateRoutingNew();" class="BtnGeneralStyle cmsiscontrol" style="margin:-5px 0px 0px 20px;"><s:text name="jsp.common.capnhat" /></button>
                <div class="Clear"></div>
			   	<div class="Clear"></div>
                <p id="errMsgUpdateRouting" class="ErrorMsgStyle SpriteErr" style="display: none"></p>
                <p id="successMsgUpdateRouting" class="SuccessMsgStyle" style="display:none;"></p>
            </div>
             
            <div class="TabSection">
                <ul class="ResetList TabSectionList">
                    <li id="tab3" onclick="SuperviseManageRouteCreate.showTab3();">
                    	<a id="tabActive3" href="javascript:void(0)" href="#ttdbab3" class="Sprite1 ">
                    		<span class="Sprite1"><s:text name="ss.assign.route.title"/></span>
                    	</a>
                    </li>
                    <li id="tab1" onclick="SuperviseManageRouteCreate.showTab1();">
                    	<a id="tabActive1" href="javascript:void(0)" class="Sprite1 Active">
                    		<span class="Sprite1"><s:text name="ss.create.route.ds.kh"/></span>
                    	</a>
                    </li>
                    <li id="tab2" onclick="SuperviseManageRouteCreate.showTab2();">
                    	<a id="tabActive2" href="javascript:void(0)" href="#ttdbab2" class="Sprite1" style="padding: 0 35px 0 0">
                    		<span class="Sprite1"><s:text name="ss.create.route.set.order.perimission"/></span>
                    	</a>
                    </li>
                    <li id="tab4" onclick="SuperviseManageRouteCreate.showTab4();">
                    	<a id="tabActive4" href="javascript:void(0)" href="#ttdbab4" class="Sprite1 ">
                    		<span class="Sprite1"><s:text name="ss.create.route.view.map"/></span>
                    	</a>
                    </li>
                </ul>
                <div class="Clear"></div>
            </div>
	            <div id="container1">
	<!-- 	              <h2 class="Title2Style">Tìm kiếm khách hàng</h2> -->
	            	<div class="GeneralForm SearchInSection"  style="border: medium none;">
	             	<div class="Clear"></div>
	             	<label class="LabelStyle Label1Style"><s:text name="jsp.common.customer.code"/></label>
				<input id="shortCode" maxlength="40" type="text" class="InputTextStyle InputText1Style" />
	               <label class="LabelStyle Label1Style"><s:text name="jsp.common.customer.name"/></label>
	               <input id="customerName" maxlength="150" type="text" class="InputTextStyle InputText1Style" />
	               <label class="LabelStyle Label1Style"><s:text name="jsp.common.address"/></label>
		  		<input id="address" maxlength="400" type="text" class="InputTextStyle InputText1Style"/>
	               <div class="Clear"></div>
	               <label class="LabelStyle Label1Style"><s:text name="jsp.common.from.date"/></label>
				<input id="startDate" maxlength="10" type="text" class="InputTextStyle InputText1Style" />
	               <label class="LabelStyle Label1Style"><s:text name="jsp.common.to.date"/></label>
	               <input id="endDate" maxlength="10" type="text" class="InputTextStyle InputText1Style" />
	               <label class="LabelStyle Label1Style"><s:text name="jsp.common.status"/></label>
	               <div class="BoxSelect BoxSelect4">
	                   <select id="listStatus" class="MySelectBoxClass">
					<option value="1"><s:text name="jsp.common.status.active"/></option>
					<option value="0"><s:text name="jsp.common.status.stoped"/></option>
					</select>
	               </div>
	               <div class="Clear"></div>
	               <div class="BtnCenterSection" style="border: medium none;">
	                   <button id="btnSearchRouCus" onclick="return SuperviseManageRouteCreate.searchCustomerRouting();" class="BtnGeneralStyle"><s:text name="jsp.common.timkiem"/></button>
	               </div>
	               <p id="errMsgSearchRoucus" class="ErrorMsgStyle SpriteErr" style="display:none;"></p>
	               <div class="Clear"></div>
			</div>
<!-- 			<h2 class="Title2Style"><s:text name="ss.create.route.ds.kh"/></h2> -->
			<div class="GeneralForm SearchInSection" id="container1Grid"  style="border: medium none;">
				<tiles:insertTemplate template="/WEB-INF/jsp/superviseshop/manageroute-create/manageRouteEditPageCustomerSearch.jsp" />
			</div>
		</div>
           <div id="container2">
            <div class="GeneralForm SearchInSection"  style="border: medium none;">
			    <label class="LabelStyle Label1Style"><s:text name="ss.create.route.sale.day"/><span class="ReqiureStyle">*</span></label>
			    <div class="BoxSelect BoxSelect6">
			        <select id="listDateNew" class="MySelectBoxClass" onchange="SuperviseManageRouteCreate.changeSelectBox();">
						<option value="2"  selected="selected"><s:text name="jsp.common.thu.hai"/></option>
						<option value="3" ><s:text name="jsp.common.thu.ba"/></option>
						<option value="4" ><s:text name="jsp.common.thu.bon"/></option>
						<option value="5" ><s:text name="jsp.common.thu.nam"/></option>
						<option value="6" ><s:text name="jsp.common.thu.sau"/></option>
						<option value="7" ><s:text name="jsp.common.thu.bay"/></option>
						<option value="1" ><s:text name="jsp.common.thu.chunhat"/></option>
					</select>
			    </div>
			    <div class="Clear"></div>
			</div>
<!-- 			<h2 class="Title2Style"><s:text name="ss.create.route.ds.kh"/></h2> -->
           	<tiles:insertTemplate template="/WEB-INF/jsp/superviseshop/manageroute-create/manageRouteSetOderTab2.jsp" />
           </div>
            <div id="container3">
            	<tiles:insertTemplate template="/WEB-INF/jsp/superviseshop/manageroute-create/manageRouteAssignTab3.jsp" />
            </div>
			<div id="container4" style="display:none">
			 	<div class="GeneralForm SearchInSection"  style="border: medium none;">
				    <label class="LabelStyle Label1Style"><s:text name="ss.create.route.sale.day"/><span class="ReqiureStyle">*</span></label>
				    <div class="BoxSelect BoxSelect6">
				        <select id="listDate" class="MySelectBoxClass" onchange="SuperviseManageRouteCreate.changeSelectBox();">
							<option value="2"  selected="selected"><s:text name="jsp.common.thu.hai"/></option>
							<option value="3" ><s:text name="jsp.common.thu.ba"/></option>
							<option value="4" ><s:text name="jsp.common.thu.bon"/></option>
							<option value="5" ><s:text name="jsp.common.thu.nam"/></option>
							<option value="6" ><s:text name="jsp.common.thu.sau"/></option>
							<option value="7" ><s:text name="jsp.common.thu.bay"/></option>
							<option value="1" ><s:text name="jsp.common.thu.chunhat"/></option>
						</select>
				    </div>
				    <div class="Clear"></div>
				    
				</div>
			 	<div class="GeneralForm SearchInSection"  style="border: medium none;">
			    	<p id="errMsgMap" class="ErrorMsgStyle SpriteErr" style="display: none"><s:text name="ss.create.route.have.no.cust"/></p>
			       	<div id="divMapRoute" style="position:relative; border: medium none;">
			               <div id="mapRoute" style="width:100%;height:600px;position: relative;" ></div>
			         	</div>
			         </div>
				</div>
            </div>
        </div>
        <div class="Clear"></div>
</div>

<input type="hidden" id="routingId" name="routeId" value="<s:property value="routeId" />" />
<input type="hidden" id="sysdateServer" value="<s:property value="sysdateStr" />" />
<input type="hidden" id="yesterdateServer" value="<s:property value="yesterdateStr" />" />
<s:hidden id="flagEdit" value="1"></s:hidden>
<input type="hidden" id="statusRoutingHidden" value="<s:property value="route.status" />" />
<s:hidden id="shopCode" name="shopCode"></s:hidden>
<script type="text/javascript">
$(document).ready(function(){
	SuperviseManageRouteCreate._shopIdIsView = '<s:property value="shopId"/>';
	TreeUtils.loadRoutingTree('routingTree', null, $('#routingId').val() ,function(data) {
		window.location.href= '/superviseshop/manageroute-create/editroute?routeId=' + data + '&shopCode=' + $('#shopCode').val();
	},  function(node) {
		if (node.text == activeStatusText) {
			window.location.href= '/superviseshop/manageroute-create/info?status=' + activeType.RUNNING + '&shopCode=' + $('#shopCode').val();
		} else {
			window.location.href= '/superviseshop/manageroute-create/info?status=' + activeType.STOPPED + '&shopCode=' + $('#shopCode').val();
		}
	}, $('#shopCode').val());
	SuperviseManageRouteCreate.hideAllTab();	
	SuperviseManageRouteCreate._divMap = $('#divMapRoute').html();
	$('#showDialogId').hide();
	//Tab1
	SuperviseManageRouteCreate._index = 0;
	applyDateTimePicker('#startDate');
	applyDateTimePicker('#endDate');
	$('#startDate').val($('#sysdateServer').val().trim());
// 	$('#endDate').val($('#sysdateServer').val().trim());
  	SuperviseManageRouteCreate.searchCustomerRouting();
	$('#startDate').width($('#shortCode').width() - 21);
	$('#endDate').width($('#shortCode').width() - 21);
	$('#layoutContent').removeClass('Content').addClass('MainContent');
	
	//Xay dung KeyUp
	$('#shortCode, #customerName, #address, #startDate, #endDate').bind('keyup',function(event){
		if(event.keyCode == keyCodes.ENTER){
			return SuperviseManageRouteCreate.searchCustomerRouting();
		}
	});
	
	//Xay dung KeyUp
	$('#shortCodeDialog, #customerNameDialog').bind('keyup',function(event){
		if(event.keyCode == keyCodes.ENTER){
			return SuperviseManageRouteCreate.searchCustomerDialog();
		}
	});
	
	$('#divEditRouting').bind('keyup',function(event){
		/* if(event.keyCode == keyups.Alt_r){
			if($('#tabActive1').hasClass('Active')) {
				//Danh sach khach hang
				SuperviseManageRouteCreate.addOrSaveRouting();
			}else if($('#tabActive2').hasClass('Active')){
				//Thiet lap tu tu ghe tham
				SuperviseManageRouteCreate.updateListRoutingCustomer();
			}else if($('#tabActive3').hasClass('Active')){
				//Giao tuyen
				SuperviseManageRouteCreate.addOrUpdateStaff(false);
			}
		}else if(event.keyCode == keyCodes.F9){
			if($('#tabActive1').hasClass('Active')) {
				//Them khach hang vao tuyen
				SuperviseManageRouteCreate.openAddCustomerDialog(false);
			}
		}else if(event.keyCode == keyups.AltPlus){
			$('#group_edit_rt_dt_btnAddRouting').click();
		} */
	});
	//End-KeyUp
		
	var obj = SuperviseManageRouteCreate._mapCustomer;
	
	//tab2
	$('#listDate, #listDateNew').bind('keyup', function(event) {
		if (event.keyCode == keyCodes.ENTER) {
			SuperviseManageRouteSetOrder.changeSelectBox();
		}
	});
	//tab3
	setDateTimePicker('fromDate');
	setDateTimePicker('toDate');
	
	SuperviseManageRouteCreate.bindComboboxStaffSaleEasyUI('saleStaffId');
	
	//tab4
	//general
	$('#address').width(143);
	
	//Xay dung KeyUp
	$('#routingCode, #routingName, #group_edit_rt_dt_btnUpdRouting').bind('keyup',function(event){
		if(event.keyCode == keyCodes.ENTER){
			return SuperviseManageRouteCreate.updateRoutingNew();
		}
	});
	$('#routingCode').focus();
	
	SuperviseManageRouteCreate.showTab3();
	initUnitCbx();
	var tm = setTimeout(function(){
   		SuperviseManageRouteCreate.resize();
   		$('#treeContainer').css('min-height',$('.ContentSection').height() - 125);
		$('#group_edit_rt_dt_btnAddRouting').css('bottom', $('#treeContainer').height() - $('.ContentSection').height() + 100);
   		clearTimeout(tm);
	}, 500);
	$(window).resize();
});

function MyOverlay(custId, latlng, img, title, info)
{
    this.latlng = latlng;
    this.latlngs = latlng;
    this.img = img;    

    this.initialize = initialize;
    this.remove = remove;
    this.copy = copy;
    this.redraw = redraw;
    this.vType = vType;
    this.info=info;
    this.custId=custId;
    this.title=title;

    function initialize(map)
    {
    	this.mapObj = map;
        var myDiv = document.createElement("div");
        myDiv.id = map.id + "MyOverlay";
        myDiv.style.position = "absolute";
        myDiv.style.textAlign = "center";
        var lat=latlng.latitude;
        var lng=latlng.longitude;
        myDiv.innerHTML = "<strong style='text-align: center;color: red;font-weight: bolder;'>" + this.title + "</strong><img alt='" + this.title + "' src='" + this.img + "' onclick=\"Vietbando.showWindowInfo("+custId+","+lat+","+lng+",'"+this.info+"');\"/>";
        map.getOverlayContainer().appendChild(myDiv);
        this.divObj = myDiv;
        this.redraw();
    }
    
    
    function remove()
    {
        this.divObj.parentNode.removeChild(this.divObj);
    }

    function copy()
    {
        return new MyOverlay(this.latlng, this.img);
    }

    function redraw()
    {
        var pt = this.mapObj.fromLatLngToDivPixel(this.latlng);
        if(this.custId < 0) {
        	this.divObj.style.left = (pt.x - 15 - 18 + 5) + "px";
            this.divObj.style.top = (pt.y - 50 + 16 - 39) + "px";
        } else {
        	this.divObj.style.left = (pt.x - 15 - 18) + "px";
            this.divObj.style.top = (pt.y - 50 + 16) + "px";        	
        }
    }

    function vType()
    {
        return 'MyOverlay';
    }
}

function MyOverlayCust(custId, latlng, ttgt, info)
{
    this.latlng = latlng;
    this.latlngs = latlng;

    this.initialize = initialize;
    this.remove = remove;
    this.copy = copy;
    this.redraw = redraw;
    this.vType = vType;
    this.info=info;
    this.custId=custId;
    this.ttgt=ttgt;

    function initialize(map)
    {
    	this.mapObj = map;
        var myDiv = document.createElement("div");
        myDiv.id = map.id + "MyOverlay";
        myDiv.style.position = "absolute";
        var lat=latlng.latitude;
        var lng=latlng.longitude;
        myDiv.innerHTML ="<div class='CustomersStatus Customers4Status' onclick=\"Vietbando.showWindowInfo("+custId+","+lat+","+lng+",'"+this.info+"');\"><span>"+this.ttgt+"</span></div>";
        map.getOverlayContainer().appendChild(myDiv);
        this.divObj = myDiv;
        this.redraw();
    }
    function remove()
    {
        this.divObj.parentNode.removeChild(this.divObj);
    }

    function copy()
    {
        return new MyOverlay(this.latlng, this.img);
    }

    function redraw()
    {
        var pt = this.mapObj.fromLatLngToDivPixel(this.latlng);
        this.divObj.style.left = (pt.x-15) + "px";
        this.divObj.style.top = (pt.y-50) + "px";
    }

    function vType()
    {
        return 'MyOverlay';
    }
}
function initUnitCbx(){				
	var params = {};
	$.ajax({
		type : "POST",
		url:'/commons/list-child-shop',
		data :($.param(params, true)),
		dataType : "json",
		success : function(data) {
			$('#shopCodeCB').combobox({
				valueField: 'shopCode',
				textField:  'shopName',
				data: data.rows,
				panelWidth: '200',
				formatter: function(row) {
					return '<span style="font-weight:bold">' + Utils.XSSEncode(row.shopCode) + '</span><br/>' + '<span style="color:#888">' + Utils.XSSEncode(row.shopName) + '</span>';
				},
				filter: function(q, row){
					q = new String(q).toUpperCase().trim();
					var opts = $(this).combobox('options');
					return ((unicodeToEnglish(row[opts.textField]).indexOf(unicodeToEnglish(q)) == 0) || unicodeToEnglish(row[opts.valueField]).indexOf(unicodeToEnglish(q)) == 0) ;
				},	
		        onSelect: function(rec){
		        	window.location.href= '/superviseshop/manageroute-create/info?shopCode=' + $('#shopCodeCB').combobox('getValue');
		        },
		        onLoadSuccess: function(){
		        	$('#shopCodeCB').combobox('setValue', $('#shopCode').val());
		        }
			});
		}
	});
}
</script>

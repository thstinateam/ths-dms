<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<div class="BreadcrumbSection">
   <ul class="ResetList FixFloat BreadcrumbList">
       <li class="Sprite1"><a href="/superviseshop/manageroute-create/info"><s:text name="ss.create.route.title"/></a></li>
       <li><span><s:text name="ss.create.route.create.title"/></span></li>
   </ul>
</div>
<div class="CtnTwoColsSection">
  	<div class="SidebarSection">
  		<h2 class="Title2Style"><s:text name="jsp.common.shop"/></h2>
    	<div class="BoxSelect BoxSelect2" id ="shopIdList" style="margin: 5px" >
            <select id="shopCodeCB" style="width: 200px;"></select>
        </div>
        <div class="Clear"></div>
  		<h2 class="Title2Style"><s:text name="ss.create.route.title.list.route"/></h2>
  		<div class="SidebarInSection">
			<div class="ReportTreeSection" id="treeContainer" style=" overflow: scroll;">
				<div id="routingTree"></div>
			</div>
			<div class="BtnCenterSection">
				<button id="group_edit_rt_tm_btnAddRouting" class="BtnGeneralStyle" style="position:inherit" onclick="SuperviseManageRouteCreate.showDialogConfirm();">+ <s:text name="ss.create.route.add.route"/></button>
			</div>
			<div class="Clear"></div>
  		</div>
  	</div>
    <div class="ContentSection">
      	<div class="ReportCtnSection">
          	<h2 class="Title2Style"><s:text name="ss.create.route.info"/></h2>
            <div class="GeneralForm SearchInSection">
              	<label class="LabelStyle Label1Style"><s:text name="ss.routingcode"/> <span class="ReqiureStyle">*</span></label>
                <input id="routingCode" maxlength="40" type="text" class="InputTextStyle InputText1Style" />
                <label class="LabelStyle Label1Style"><s:text name="ss.routingname"/> <span class="ReqiureStyle">*</span></label>
                <input id ="routingName" maxlength="150" type="text" class="InputTextStyle InputText1Style" />
				<button id="group_edit_rt_tm_btnInsertRouting" class="BtnGeneralStyle cmsiscontrol" onclick="SuperviseManageRouteCreate.addRoutingNew();" style="margin:-5px 0px 0px 20px;"><s:text name="jsp.common.capnhat" /></button>
                <div class="Clear"></div>
            </div>
     	<div class="Clear"></div>
        <p id="errMsg" style="display: none;" class="ErrorMsgStyle"></p>
 		<tiles:insertTemplate template="/WEB-INF/jsp/general/successMsg.jsp" />
     	</div>
	</div>
	<div class="Clear"></div>
</div>
<s:hidden id="flagEdit" value="0"></s:hidden>
<s:hidden id="shopCode" name="shopCode"></s:hidden>
<script type="text/javascript">
$(document).ready(function(){
	//Xay dung KeyUp
	$('#routingCode, #routingName, #group_edit_rt_tm_btnInsertRouting').bind('keyup',function(event){
		if(event.keyCode == keyCodes.ENTER){
			return SuperviseManageRouteCreate.addRoutingNew();
		}
	});
	TreeUtils.loadRoutingTree('routingTree', null, $('#routingId').val() ,function(data) {
		window.location.href= '/superviseshop/manageroute-create/editroute?routeId=' + data + '&shopCode=' + $('#shopCode').val();
	},  function(node) {
		if (node.text == activeStatusText) {
			window.location.href= '/superviseshop/manageroute-create/info?status=' + activeType.RUNNING + '&shopCode=' + $('#shopCode').val();
		} else {
			window.location.href= '/superviseshop/manageroute-create/info?status=' + activeType.STOPPED + '&shopCode=' + $('#shopCode').val();
		}
	}, $('#shopCode').val());
	var hg = $('.ContentSection').height() - 125; 
	$('#treeContainer').css('height',hg);
	$('#routingCode').focus();
	initUnitCbx();
});

//TungMT
function MyOverlay(custId, latlng, img, title, info)
{
    this.latlng = latlng;
    this.img = img;    

    this.initialize = initialize;
    this.remove = remove;
    this.copy = copy;
    this.redraw = redraw;
    this.vType = vType;
    this.info=info;
    this.custId=custId;
    this.title=title;

    function initialize(map)
    {
    	this.mapObj = map;
        var myDiv = document.createElement("div");
        myDiv.id = map.id + "MyOverlay";
        myDiv.style.position = "absolute";
        var lat=latlng.latitude;
        var lng=latlng.longitude;
        myDiv.innerHTML = "<strong>" + this.title + "</strong><img alt='" + this.title + "' src='" + this.img + "' onclick=\"Vietbando.showWindowInfo("+custId+","+lat+","+lng+",'"+this.info+"');\"/>";
        map.getOverlayContainer().appendChild(myDiv);
        this.divObj = myDiv;
        this.redraw();
    }
    
    
    function remove()
    {
        this.divObj.parentNode.removeChild(this.divObj);
    }

    function copy()
    {
        return new MyOverlay(this.latlng, this.img);
    }

    function redraw()
    {
        var pt = this.mapObj.fromLatLngToDivPixel(this.latlng);
        this.divObj.style.left = (pt.x-15) + "px";
        this.divObj.style.top = (pt.y-50) + "px";
    }

    function vType()
    {
        return 'MyOverlay';
    }
    
}
function initUnitCbx(){				
	var params = {};
	$.ajax({
		type : "POST",
		url:'/commons/list-child-shop',
		data :($.param(params, true)),
		dataType : "json",
		success : function(data) {
			$('#shopCodeCB').combobox({
				valueField: 'shopCode',
				textField:  'shopName',
				data: data.rows,
				panelWidth: '200',
				formatter: function(row) {
					return '<span style="font-weight:bold">' + Utils.XSSEncode(row.shopCode) + '</span><br/>' + '<span style="color:#888">' + Utils.XSSEncode(row.shopName) + '</span>';
				},
				filter: function(q, row){
					q = new String(q).toUpperCase().trim();
					var opts = $(this).combobox('options');
					return ((unicodeToEnglish(row[opts.textField]).indexOf(unicodeToEnglish(q)) == 0) || unicodeToEnglish(row[opts.valueField]).indexOf(unicodeToEnglish(q)) == 0) ;
				},	
		        onSelect: function(rec){
		        	window.location.href= '/superviseshop/manageroute-create/info?shopCode=' + $('#shopCodeCB').combobox('getValue');
		        },
		        onLoadSuccess: function(){
		        	$('#shopCodeCB').combobox('setValue', $('#shopCode').val());
		        }
			});
		}
	});
}
</script>
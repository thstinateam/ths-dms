<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<div class="GeneralForm SearchInSection"  style="border: medium none;">
    <div class="GridSection" id="routingContainerGrid"  style="border: medium none;">
    	<table id="routingGrid"></table>
    </div>
    <div class="Clear"></div>
    <div class="BtnCenterSection"  style="border: medium none;">
    	<button id="group_edit_rt_dt_btnInsertSetoder" class="BtnGeneralStyle cmsiscontrol" title="<s:text name="ss.create.route.hotkey.update.stt"/>" onclick="SuperviseManageRouteCreate.updateListRoutingCustomer();"><s:text name="jsp.common.capnhat" /></button>
    </div>
    <div class="Clear"></div>
    <p id="errMsgRoucus" class="ErrorMsgStyle SpriteErr" style="display:none;"></p>
	<p id="successMsgRoucus" class="SuccessMsgStyle" style="display:none;"></p>
</div>
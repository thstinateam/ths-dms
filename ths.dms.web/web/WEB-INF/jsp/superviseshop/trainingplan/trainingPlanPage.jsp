<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>

<s:hidden id="shopType" name="shopType"></s:hidden>
<input type="hidden" id="loadpage" value="0" autocomplete="off" />

<div class="BreadcrumbSection">
                <ul class="ResetList FixFloat BreadcrumbList">
                    <li><span>Huấn luyện</span></li>
                </ul>
            </div>
            <div class="CtnOneColSection">
                <div class="ContentSection">
                	<div class="ToolBarSection">
                        <div class="SearchSection GeneralSSection">
                            <div class="SearchInSection SProduct1Form">
                                <label class="LabelStyle">Đơn vị
                                	<span class="ReqiureStyle"><font color="red">(*)</font></span>
                                </label>
								<div class="BoxSelect BoxSelect2">
									<div class="Field2">
										<input id="shop" style="width: 200px;">
									</div>
								</div>
								<label class="LabelStyle Label1Style">GSNPP</label>
                                <div class="BoxSelect BoxSelect1" id="idDivGSNPP">
	                                <select class="MySelectBoxClass" id="gsnpp" name="dept">
	                                	
	                                </select>
<%--                                 	<s:select id="gsnpp" list="lstGsnpp" listKey="id" listValue="staffName" headerKey="-1"> </s:select> --%>
                                </div>
                                <label class="LabelStyle Label1Style">Tháng
                                	<span class="ReqiureStyle"><font color="red">(*)</font></span>
                                </label>
								<input type="text" class="InputTextStyle InputText1Style" id="date"	maxlength="10" />
                                <div class="BtnLSection" style="margin-left:15px; margin-top:-2px">
                                    <button class="BtnGeneralStyle" id="btnSearch" onclick="return trainingPlan.search();">Tìm kiếm</button>
                                </div>
                                <div class="Clear"></div>
		                        <p id="errMsg" class="ErrorMsgStyle SpriteErr" style="display: none"></p>
                            </div>
                        </div>
                        <div class="Clear"></div>
                    </div>
                    <div class="GeneralCntSection">
                    	<div class="GeneralForm GeneralNoTPForm">
                        	<div class="Func1Section">
                        		<button class="BtnGeneralStyle" onclick="return trainingPlan.importExcel();">Nhập Excel</button>
								<button class="BtnGeneralStyle" onclick="return trainingPlan.exportExcel();">Xuất Excel</button>
								<p class="DownloadSFileStyle">								
									<a id="downloadTemplate" class="Sprite1" href="javascript:void(0)" onclick="">Tải file excel mẫu</a>
								</p>
                            </div>
                            <div class="Func2Section">
                            	<label id="hideItem" class="LabelStyle Label1Style"><input id="cb_main" type="checkbox" /><span id="strHidden">Chọn toàn bộ...</span></label>
								<button onClick="return trainingPlan.delPlan();" class="BtnGeneralStyle" style="margin-right:5px">Xóa</button>
                            </div>
                            <div class="Clear"></div>
                        </div>
                    	<div id="mainGrid" class="GridSection">
<!--                     	style="width:1000px;height:380px" -->
                        	<table id="grid" class="easyui-datagrid"></table>
                        	<div id="pager"></div>
                        </div>
                        <div class="Clear"></div>             
						<p id="successMsg" class="SuccessMsgStyle" style="display: none"></p>
                    </div>
                </div>
                <div class="Clear"></div>                
            </div>
<!-- <div id = "responseDiv" style="display:none;"></div> -->
<s:hidden id="curShopName" name="shopName"></s:hidden>
<s:hidden id="dv" ></s:hidden>
<s:hidden id="nv" ></s:hidden>
<s:hidden id="th" ></s:hidden>

<div id="easyuiPopup" class="easyui-dialog" title="Nhập WW từ excel" data-options="closed:true,modal:true"  style="width:465px;height:200px;">
      <div class="PopupContentMid">
       	<div class="GeneralForm ImportExcel1Form">
       		<label class="LabelStyle Label1Style">File excel</label>
       		<form action="/plan/import" name="importFrm" id="importFrm" method="post" enctype="multipart/form-data">
                <div class="UploadFileSection Sprite1">
                	<input id="fakefilepc" type="text" class="InputTextStyle InputText1Style">
                    <input type="file" class="UploadFileStyle" size="1" name="excelFile" id="excelFile" onchange="previewImportExcelFile(this,'importFrm');" />
               		<input type="hidden" id="isView" name="isView">
					<input type="hidden" name="token" value="<s:property value="token" />" id="importTokenVal"  />
                </div>
			</form>
              <div class="Clear"></div>
              <p class="DownloadSFileStyle"><a id="downloadTemplate" href="javascript:void(0)" class="Sprite1" onclick="">Tải file excel mẫu</a></p>
              <div class="BtnCenterSection">
                  <button class="BtnGeneralStyle" onclick="return trainingPlan.upload();">Tải lên</button>
                  <button class="BtnGeneralStyle BtnGeneralMStyle" onclick="$('#easyuiPopup').window('close');">Đóng</button>
              </div>
        </div>
        <p style="display:none;margin-top:10px;margin-left: 10px" class="ErrorMsgStyle SpriteErr" id="errExcelMsg"/>
        <p style="display:none;margin-top:10px;margin-left: 10px"  id="resultExcelMsg"/>
	</div>
</div>

<s:hidden id="curShopId" name="shopId"></s:hidden>
<input type="hidden" id="shopId" autocomplete="off" />

<script type="text/javascript">
	$(document).ready(function() {
		applyMonthPicker('date');
		Utils.bindFormatOnTextfield('date',Utils._TF_NUMBER_CONVFACT);	
		$('#date').val(getCurrentMonth());
		Utils.bindAutoSearch();
		
		ReportUtils.loadComboTree('shop', 'shopId', $('#curShopId').val(), function(shopId){
			if(shopId != undefined && shopId != null && $('#shopId').val() != shopId){
				$('#checkPressBtnSearch').val(0);
				$.getJSON('/plan/getListGSNPP?shopId='+shopId, function(list) {
					$('#gsnpp').html('');
					if(list != undefined && list != null && list.length > 0) {
						if (list.length > 1) { 
							$('#gsnpp').append('<option value="-1" selected="selected" >--Tất cả--</option>');
						}
						for(var i=0; i < list.length; i++){
							$('#gsnpp').append('<option value="'+ list[i].id +'">'+ Utils.XSSEncode(list[i].staffName) +'</option>');  
						}
					} else {
						$('#gsnpp').append('<option value="-1">--Không có--</option>');
					}
					$('#gsnpp').change();
					if($('#loadpage').val()=="0"){
						$('#loadpage').val("1");
						trainingPlan.search();
					}
				});
			}
		},null,null);
		
		
		trainingPlan.search();
		
		$('#downloadTemplate').attr('href',excel_template_path + 'trainingplan/Bieu_mau_ke_hoach_huan_luyen_GSNPP.xls');
		$('.easyui-dialog #downloadTemplate').attr('href',excel_template_path + 'trainingplan/Bieu_mau_ke_hoach_huan_luyen_GSNPP.xls');
		//$('.MySelectBoxClass').customStyle();
		//$('#idDivGSNPP').width(290);
		
		$('#dv').val($('#shop').combotree('getValue'));
		$('#nv').val($('#gsnpp').val());
		$('#th').val($('#date').val());
	});
	
	function choseFile() {
		//todo
		$('.easyui-dialog #fakefilepc').click();
		$('.easyui-dialog #importFrm').click();
		return false;
	}
	
</script>

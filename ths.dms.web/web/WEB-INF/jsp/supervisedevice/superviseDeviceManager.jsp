<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@page import="ths.dms.helper.Configuration"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<style>
.olPopup .olPopupContent {min-width:300px;}
.olPopupContent table td {border: 1px solid #CCCCCC; padding: 1px;}
.GeneralTable .AlignLCols, .GeneralTable .AlignCCols, .GeneralTable .AlignRCols {font-size: 14px;padding: 1px 1px;}
.GeneralTable th {font-size: 13px;}
.Dl1Style, .Dl2Style {font-size: 12px;}
.MPContent .Table1Section,.MapPopup1Section {width:300px;}
.Decoration {text-decoration:none;}
.Decoration:hover {text-decoration:underline;}
.StaffSelectSection {width:auto;}
.ToolSelectSection  {filter: alpha(opacity=70);z-index:9000;opacity: 0.7;}
.ToolSelectSection td div{font-weight: bold;}
</style>
<div class="BreadcrumbSection">
    <ul class="ResetList FixFloat BreadcrumbList">
        <li class="Sprite1"><a href="#">Giám sát</a></li>
        <li><span>Giám sát thiết bị</span></li>
    </ul>
</div>
<div class="CtnOneColSection">
	<div class="ContentSection">
		<div class="ToolBarSection">
			<div class="GuideMapSection">
				<div class="GuideMapInSection">
					<label class="LabelStyle Label2Style"><input onclick="SupperviseDevice.rbChange();" type="radio" name="rbSup" value="1">Giám sát nhiệt độ </label>
					<label class="LabelStyle Label1Style"><input onclick="SupperviseDevice.rbChange();" type="radio" name="rbSup" checked="checked" value="0">Giám sát vị trí </label>
					<div class="Clear"></div>
				</div>
			</div>
			<div class="TSSection SearchInSection SProduct1Form">
				<div class="BtnRSection">
					<button onclick="SupperviseDevice.refresh();" style="height: 23px;padding: 3px 8px !important;width:120px" class="BtnGeneralStyle" id="btCapNhatViTri">Cập nhật vị trí</button>
				</div>
			</div>
			<div class="Clear"></div>
		</div>
		<div class="MapRouteSection" >
			<div class="ShadowBgSection"></div>
			<div id="bigMap" style="width: 100%; height: 500px; position: relative;"></div>
			<div class="NoteCustomersStatus" style="display:none;">
               	<ul class="ResetList NCSList FixFloat">
                   	<li id="item1">Bình thường</li>
                      <li id="item3">Cảnh báo</li>
                </ul>
                <a href="javascript:void(0)" onclick="SupperviseDevice.reloadMarker();" class="CloseStyle"><img src="/resources/images/icon_close.png" width="12" height="12" /></a>
               </div>
			<div class="StaffSelectSection">
				<div class="StaffSelectBtmSection">
					<p id="titleStaff" onclick="SupperviseDevice.toggleListShop();" class="StaffSelectText Sprite1 OffStyle">Đơn vị quản lý</p>
					<div id="listStaff" class="StaffTreeSection SearchSection GridSection">
						<input id="shopCode" placeholder="Mã đơn vị" type="text" class="InputTextStyle InputText1Style" style="width: 70px;padding-right:5px"/>
						<input id="shopName" placeholder="Tên đơn vị" type="text" class="InputTextStyle InputText2Style" style="width: 120px"/>
						<div class="BtnLSection">
							<button id="btSearchShop" onclick="SupperviseDevice.loadTreeShop(1);" class="BtnGeneralStyle">Tìm kiếm</button>
						</div>
						<div class="Clear"></div>
						<div id="promotionShopGrid">
                             <table id="treeGrid" ></table> 
                   		</div>
					</div>
				</div>
			</div>
			<div class="StaffSelectSection ToolSelectSection" style="right:2px;left:auto;top:0px;">
				<p id="titleTool" onclick="SupperviseDevice.toggleListTool();" class="StaffSelectText Sprite1 OffStyle">&nbsp;</p>
				<div id="listTool" style="max-height:200px" class="GridSection">
					<table id="toolGridEx" class="easyui-datagrid"></table>
					<div id="searchStyle1PagerEx"></div>
				</div>
			</div>
		</div>
	</div>
	<div class="Clear"></div>
</div>
<div class="GeneralMilkBox">
	<div class="GeneralMilkTopBox">
		<div class="GeneralMilkBtmBox"></div>
	</div>
</div>
<div id="contextMenu" class="easyui-menu" style="width:120px;display:none">
    <div id="cmCountTool" >DS tủ lạnh</div>
    <div id="cmCountWarning" >DS cảnh báo</div>
</div>

<div id="searchToolEasyUIDialog" class="easyui-dialog" title="Danh sách thiết bị" style="width: 712px; height: 450px;" data-options="closed:true,modal:true">
	<div class="PopupContentMid">
		<div class="GeneralForm Search1Form" >
				<label class="LabelStyle Label1Style" style=" width: 100px;">Mã thiết bị</label>
				<input id="toolCode" maxlength="50" type="text" style=" width: 145px;" class="InputTextStyle InputText1Style" /> 
				<label class="LabelStyle Label1Style" style=" width: 100px;">Mã điểm lẻ</label> 
				<input id="customerCode" type="text" maxlength="250" style="width: 145px;"  class="InputTextStyle InputText1Style" />
				<div class="Clear"></div>
				<button class="BtnGeneralStyle BtnSearchStyle BtnSearchOnDialog" id="btnSearchTool">Tìm kiếm</button>
				<div class="Clear"></div>
				<p id="warningMsg" class="SuccessMsgStyle" style="display: none"></p>
				<s:hidden id="shopId"></s:hidden>
				<s:hidden id="isWarning"></s:hidden>
				<div class="Clear"></div>
				<div class="GridSection" id="searchToolContainerGrid">					
					<table id="toolGrid" class="easyui-datagrid"></table>
					<div id="searchStyle1Pager"></div>
				</div>
				<div class="BtnCenterSection">
					<button class="BtnGeneralStyle BtnGeneralMStyle" onclick="$('#searchToolEasyUIDialog').dialog('close');">Đóng</button>
				</div>
				<p id="errMsgSearch" style="display: none;" class="ErrorMsgStyle SpriteErr"></p>
			</div>
	</div>
</div>
<s:hidden id="shopId" name="shopId"></s:hidden>
<s:hidden id="type" name="type"></s:hidden>
<s:hidden id="distance" name="distance"></s:hidden>
<script>
$(document).ready(function(){
	if($('#distance').val()!=''){
		SupperviseDevice._allowDistance=parseInt($('#distance').val());
	}
	$('.MySelectBoxClass').customStyle();
	$('#bigMap').css('height',($(window).height()-172)+'px');
	$(window).resize(function() {
		$('#bigMap').css('height',($(window).height()-172)+'px');
		$('.datagrid-body').css('max-height',($(window).height()-300)+'px');
	});
	SupperviseDevice._lstToolPosition=new Map();
	SupperviseDevice.loadTreeShop();
	SupperviseDevice.getListShop();
	$('.datagrid-body').css('max-height',($(window).height()-300)+'px');
	VTMapUtil.loadMapResource(function(){
		var map = ViettelMap.loadBigMap('bigMap',null,null,4,null);
		viettel.Events.addListener(map, 'zoom_changed', function(a,b,c) {
			if(SupperviseDevice._currentShopId==null){
				var zoom=ViettelMap._map.getZoom();
				if(zoom==7 || zoom==8){
					SupperviseDevice._notFitOverlay=1;
					SupperviseDevice.reloadMarker(1);
				}
			}
	    });
	});
	$('#shopCode').bind('keyup',function(event){
			if(event.keyCode == keyCodes.ENTER){		   				
				SupperviseDevice.loadTreeShop(1);
			}
	});
	$('#shopName').bind('keyup',function(event){
		if(event.keyCode == keyCodes.ENTER){		   				
			SupperviseDevice.loadTreeShop(1);
		}
	});
});
</script>
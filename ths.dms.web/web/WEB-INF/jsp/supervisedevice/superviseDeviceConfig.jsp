<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<style type="text/css">
.datagrid { margin:auto; }
</style>
<div class="BreadcrumbSection">
                <ul class="ResetList FixFloat BreadcrumbList">
                    <li class="Sprite1"><a href="/catalog/product">Giám sát thiết bị</a></li>
                    <li><span>Cấu hình thiết bị</span></li>
                </ul>
</div>
<div class="CtnOneColSection"><!--  -->
   	<div class="ContentSection"><!--  -->
       	<div class="ToolBarSection"><!--  -->
	      <div class="SearchSection GeneralSSection">
	           	<div class="TabSection">
	            	<ul class="ResetList TabSectionList">
	                   	<li><a id="tab1" href="javascript:void(0);" onclick="SupperviseDevice.loadTab1();" class="Sprite1"><span class="Sprite1">Cấu hình nhiệt độ</span></a></li>
	                   	<li><a id="tab2" href="javascript:void(0);" onclick="SupperviseDevice.loadTab2();" class="Sprite1"><span class="Sprite1">Cấu hình vị trí</span></a></li>
		            	<li><a id="tab3" href="javascript:void(0);" onclick="SupperviseDevice.loadTab3();" class="Sprite1"><span class="Sprite1">Cấu hình chung</span></a></li>
		            </ul>
	              	<div class="Clear"></div>
	           </div>
	           <div style="" id="container1">
			      	<h2 class="Title2Style">Danh sách chip</h2>
		      		<div class="SearchInSection SProduct1Form">
		      			<div align="center" style="padding-left: 20%;padding-top: 9px;height: 27px">
		      				<label class="LabelStyle Label1Style">Mã chíp</label>
	                       	<input type="text" class="InputTextStyle InputText4Style" id="chipCodeSearchTemp" maxlength="10" >
	                        <label class="LabelStyle Label1Style">Nhiệt độ từ</label>
	                       	<input type="text" class="InputTextStyle InputText4Style" id="minTempSearch" maxlength="10" >
	                       	<label class="LabelStyle Label1Style">Nhiệt độ đến</label>
	                       	<input type="text" class="InputTextStyle InputText4Style" id="maxTempSearch" maxlength="10" >
				    	</div> 
                       <div class="BtnCenterSection">
                           <button onclick="return SupperviseDevice.searchChipTemp();" class="BtnGeneralStyle" id="btnSearch">Tìm kiếm</button>
                           <p id="errMsgSearch1" class="ErrorMsgStyle" style="display: none"></p>
                       </div>
					   	<div class="GridSection" id="searchChipTempContainerGrid">					
							<table id="chipGridTemp"></table>
						</div>
                       <div class="Clear"></div>
	                </div>
			    	<div class="Clear"></div>
	            </div>
	            <div style="display:none" id="container2">
			      	<h2 class="Title2Style">Danh sách chip</h2>
		      		<div class="SearchInSection SProduct1Form">
		      			<div align="center" style="padding-left: 20%;padding-top: 9px;height: 27px">
		      				<label class="LabelStyle Label1Style">Mã chíp</label>
	                       	<input type="text" class="InputTextStyle InputText4Style" id="chipCodeSearchPosition" maxlength="10" >
	                        <label class="LabelStyle Label1Style">Mã KH</label>
	                       	<input type="text" class="InputTextStyle InputText4Style" id="customerCodeSearchPosition" maxlength="10" >
	                       	<label class="LabelStyle Label1Style">Tên KH</label>
	                       	<input type="text" class="InputTextStyle InputText4Style" id="customerNameSearchPosition" maxlength="50" >
				    	</div> 
                       <div class="BtnCenterSection">
                           <button onclick="return SupperviseDevice.searchChipPosition();" class="BtnGeneralStyle" id="btnSearch">Tìm kiếm</button>
                           <p id="errMsgSearch2" class="ErrorMsgStyle" style="display: none"></p>
                       </div>
					   	<div class="GridSection" id="searchChipPositionContainerGrid">					
							<table id="chipGridPosition"></table>
						</div>
                       <div class="Clear"></div>
	                </div>
			    	<div class="Clear"></div>
	            </div>
	            <div style="display:none" id="container3">
	            	<h2 class="Title2Style">Cấu hình tần suất</h2>
	               	<div class="SearchInSection SProduct1Form">
                       <label class="LabelStyle Label1Style">Vị trí<span class="RequireStyle">(*)</span></label>
                       	<input type="text" class="InputTextStyle InputText1Style" value="<s:property value="positionConfig"/>" id="positionConfig" maxlength="10" >
                       <label class="LabelStyle Label1Style">Nhiệt độ<span class="RequireStyle">(*)</span></label>
                       	<input type="text" class="InputTextStyle InputText1Style" value="<s:property value="tempConfig"/>" id="tempConfig" maxlength="10" >
	                    <button onclick="return SupperviseDevice.updateConfig();" style="margin-left:30px" class="BtnGeneralStyle" id="btnRegister">Cập nhật</button>
                       <div class="Clear"></div>
                       <p id="errMsg1" class="ErrorMsgStyle" style="display: none"></p>
            			<p id="successMsg1" class="SuccessMsgStyle" style="display: none"></p>
	                </div>
	                <div class="Clear"></div>
	            	<h2 class="Title2Style">Cấu hình khoảng cách cảnh báo</h2>
	            	<div class="SearchInSection SProduct1Form">
                       <label class="LabelStyle Label1Style">Khoảng cách<span class="RequireStyle">(*)</span></label>
                       	<input type="text" class="InputTextStyle InputText1Style" value="<s:property value="distanceConfig"/>" id="distanceConfig" maxlength="10" >
	                    <button onclick="return SupperviseDevice.updateDistance();" style="margin-left:30px" class="BtnGeneralStyle" id="btnRegister">Cập nhật</button>
                       <div class="Clear"></div>
                       <p id="errMsg2" class="ErrorMsgStyle" style="display: none"></p>
            			<p id="successMsg2" class="SuccessMsgStyle" style="display: none"></p>
	                </div>
	                <div class="Clear"></div>
	            </div>
	       </div>            
		</div>
    </div>
</div>
<div id="searchChipTempEasyUIDialog" title="Thông tin thiết bị" style="width: 512px;display:none;" data-options="closed:true,modal:true">
	<div class="PopupContentMid">
		<div class="GeneralForm Search1Form" >
             <div class="BtnCenterSection" style="text-align: left;padding-left: 12px;padding-top: 9px;height: 27px">
	        	<label class="LabelStyle Label1Style">Mã chíp<span class="RequireStyle">(*)</span></label>
                  <input type="text" class="InputTextStyle InputText1Style" disabled="disabled" id="chipCodeTemp" maxlength="10" >
                  <div class="Clear"></div>
                <label class="LabelStyle Label1Style">Nhiệt độ từ<span class="RequireStyle">(*)</span></label>
                  <input type="text" class="InputTextStyle InputText1Style" id="minTemp" maxlength="10" >
                <label class="LabelStyle Label2Style">Nhiệt độ đến<span class="RequireStyle">(*)</span></label>
                  <input type="text" class="InputTextStyle InputText1Style" id="maxTemp" maxlength="10" >
                <s:hidden id="chipIdTemp"></s:hidden>
	    	</div>
	    	<div class="Clear"></div>
	    	<div class="BtnCenterSection">
               <button onclick="return SupperviseDevice.updateChipTemp();" class="BtnGeneralStyle" id="btnUpdate">Cập nhật</button>
            </div>
            <p id="errMsgPopup1" class="ErrorMsgStyle" style="display: none"></p>
            <p id="successMsgPopup1" class="SuccessMsgStyle" style="display: none"></p>
		</div>
	</div>
</div>
<div id="searchChipPositionEasyUIDialog"  title="Thông tin thiết bị" style="width: 512px;display:none;" data-options="closed:true,modal:true">
	<div class="PopupContentMid">
		<div class="GeneralForm Search1Form" >
             <div class="BtnCenterSection" style="text-align: left;padding-left: 12px;padding-top: 9px;height: 27px">
	        	<label class="LabelStyle Label1Style">Mã chíp<span class="RequireStyle">(*)</span></label>
                  <input type="text" class="InputTextStyle InputText1Style" disabled="disabled" id="chipCodePosition" maxlength="10" >
                  <div class="Clear"></div>
                <label class="LabelStyle Label1Style">Lat<span class="RequireStyle">(*)</span></label>
                  <input type="text" class="InputTextStyle InputText1Style" id="latPosition" maxlength="20" >
                <label class="LabelStyle Label2Style">Lng<span class="RequireStyle">(*)</span></label>
                  <input type="text" class="InputTextStyle InputText1Style" id="lngPosition" maxlength="20" >
                <s:hidden id="chipIdPosition"></s:hidden>
	    	</div>
	    	<div class="Clear"></div>
	    	<div class="BtnCenterSection">
               <button onclick="return SupperviseDevice.updateChipPosition();" class="BtnGeneralStyle" id="btnUpdate">Cập nhật</button>
            </div>
            <p id="errMsgPopup2" class="ErrorMsgStyle" style="display: none"></p>
            <p id="successMsgPopup2" class="SuccessMsgStyle" style="display: none"></p>
		</div>
	</div>
</div>
<script type="text/javascript">
$(document).ready(function(){
	$('.MySelectBoxClass').customStyle();
	SupperviseDevice.loadTab1();
});
</script>
<%-- <%@ page language="java" contentType="text/html; charset=UTF-8"	pageEncoding="UTF-8"%> --%>
<%-- <%@page import="ths.dms.helper.Configuration"%> --%>
<%-- <%@ taglib prefix="s" uri="/struts-tags"%> --%>
<%-- <%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %> --%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<div class="BreadcrumbSection">
	<ul class="ResetList FixFloat BreadcrumbList">
		<li class="Sprite1"><a href="javascript:void(0)">Công nợ khách hàng</a></li>
		<li><span>Xóa số dư nợ nhỏ</span></li>
	</ul>
</div>
<div class="CtnOneColSection">
	<div class="ContentSection">
		<div class="ToolBarSection">
			<div class="SearchSection GeneralSSection">
				<h2 class="Title2Style">Thông tin tìm kiếm</h2>
				<div class="SearchInSection SProduct1Form">
					<label class="LabelStyle Label1Style"><s:text name="common.shop.name.lable"/>
                       	<span class="ReqiureStyle"><font color="red"> *</font></span>
                    </label>
					<div class="BoxSelect BoxSelect2">
						<div class="Field2">
							<input class="easyui-combobox" id="shop" style="width:206px;">
						</div>
					</div>
					<label class="LabelStyle Label1Style" style="margin-top: -7px;">Số ĐH/<br/>PĐC Công nợ</label> 
					<input type="text" class="InputTextStyle InputText1Style" id="orderNumber" maxlength="50" />
					<label class="LabelStyle Label1Style">Mã KH (F9)</label> 
					<input type="text" class="InputTextStyle InputText1Style" id="shortCode"  maxlength="50" />
					<div class="Clear"></div>
                    <label class="LabelStyle Label1Style">Nợ từ (VNĐ) <span class="ReqiureStyle">*</span></label>
					<input type="text" class="InputTextStyle InputText1Style" id="fromMoney"  maxlength="20" />
					<label class="LabelStyle Label1Style">Nợ đến (VNĐ) <span class="ReqiureStyle">*</span></label>		
					<input type="text" class="InputTextStyle InputText1Style" id="toMoney"  maxlength="20" /> 
<!--                     <div class="Clear"></div> -->					
					<div class="Clear"></div>
					
                    <label class="LabelStyle Label1Style">Từ ngày</label>
                    <input type="text" class="InputTextStyle InputText6Style" value="<s:property value='fromDate'/>" id="fromDate" maxlength="10"/>
                    <label class="LabelStyle Label1Style">Đến ngày</label>
                    <input type="text" class="InputTextStyle InputText6Style" value="<s:property value='toDate'/>" id="toDate" maxlength="10"/>
                    <div class="Clear"></div>
                    
                    <label class="LabelStyle Label1Style">NVBH</label>
					<div class="BoxSelect BoxSelect2" id="idDivStaffCode">
						<select class="easyui-combobox" id="staffCode" style="width: 206px">
						</select>
					</div>
					
                    <label class="LabelStyle Label1Style">NVGH</label> 
					<div class="BoxSelect BoxSelect2">
						<select class="easyui-combobox" id="transferStaff" style="width: 206px">
						</select>
					</div>
					
					<label class="LabelStyle Label1Style">NVTT</label>
					<div class="BoxSelect BoxSelect2">
						<select class="easyui-combobox" id="crashierStaff" style="width: 206px">
						</select>
					</div>
                    <div class="Clear"></div>
					<div class="BtnCenterSection">
						<button id="btnSearch" class="BtnGeneralStyle cmsiscontrol" onClick="return DelSmallDebit.getList();" >Tìm kiếm</button>
					</div>
					<p class="ErrorMsgStyle" id="errMsg" style="display: none;"></p>
                    <p class="ErrorMsgStyle" id="errMsg001" style="display: none;">Mời bạn nhập giá trị hợp lý trường money</p>					
				</div>
			</div>
			<div class="Clear"></div>
		</div>
		<div class="SProduct1Form GeneralCntSection">			
				<h2 class="Title2Style">Danh sách khách hàng cần xóa nợ nhỏ</h2>	
<!-- 				<div class="SearchInSection SProduct1Form"> -->
					<div class="GridSection" id="gridContainer" >
						<div class="ResultSection" id="productGridV">
							<p id="gridNoResult" style="display: none" class="WarningResultStyle">Không có kết quả</p>
		                  	<table id="dg"></table>
							<div id="pager"></div>
						</div>
					</div>
					<div class="BtnCenterSection">
						<button class="BtnGeneralStyle cmsiscontrol" id="btnRemoveDebit" onClick="return DelSmallDebit.removeDebit();">Xóa nợ</button>						
					</div>
					<div class="Clear"></div>					
       				<p class="ErrorMsgStyle" id="errMsg2" style="display: none;"></p>
       				<p id="successMsg" class="SuccessMsgStyle" style="display: none"></p>					       		
<!-- 				</div> -->
				<div class="Clear"></div>
		</div>
	</div>
</div>
<script type="text/javascript">
   	$(document).ready(function() {
		//load combobox don vi
		Utils.initUnitCbx('shop', null, 206, function(rec) {
	    	if (rec != null && rec.shopCode != ReportUtils._currentShopCode) {
				$('#shopCode').val(rec.shopCode);
				ReportUtils._currentShopCode = rec.shopCode;
				DelSmallDebit.changeShop();
	    	}
		}, function(arr) {
	       	if (arr != null && arr.length > 0) {
	       		ReportUtils._currentShopCode = arr[0].shopCode;
	       	}
		});
	   	applyDateTimePicker("#fromDate");
	   	applyDateTimePicker("#toDate");
	   	Utils.bindComboboxStaffEasyUI('crashierStaff');
		Utils.bindComboboxStaffEasyUI('transferStaff');
		Utils.bindComboboxStaffEasyUI('staffCode');
	   	$('.MySelectBoxClass').customStyle();
	   	$('.InputTextStyle').unbind('keyup');
	   	//Utils.bindAutoSearch();
	   	$('#shortCode').parent().bind('keyup',function(event){
			if(event.keyCode == keyCodes.ENTER){		   				
				$('#btnSearch').click();				
			}
		});
	   	$('#orderNumber').focus();
	   	$('#shortCode').bind('keyup', function(event){
			if(event.keyCode == keyCode_F9){
				VCommonJS.showDialogSearch2({
				    inputs : [
				        {id:'code', maxlength:50, label:'Mã KH'},
				        {id:'name', maxlength:250, label:'Tên KH'},
				        {id:'address', maxlength:250, label:'Địa chỉ', width:410}
				    ],
				    params: {shopCode: $('#shop').combobox('getValue')},
				    url : '/commons/customer-in-shop/search',
				    onShow : function() {
			        	$('.easyui-dialog #code').focus();
				    },
				    columns : [[
				        {field:'shortCode', title:'Mã KH', align:'left', width: 110, sortable:false, resizable:false, formatter: function(v, r, i){
				        	if (v) {
				        		return Utils.XSSEncode(v);
				        	}
				        	return "";
				        }},
				        {field:'customerName', title:'Tên KH', align:'left', width: 150, sortable:false, resizable:false, formatter: function(v, r, i){
				        	if (v) {
				        		return Utils.XSSEncode(v);
				        	}
				        	return "";
				        }},
				        {field:'address', title:'Địa chỉ', align:'left', width: 170, sortable:false, resizable:false, formatter: function(v, r, i){
				        	if (v) {
				        		return Utils.XSSEncode(v);
				        	}
				        	return "";
				        }},
				        {field:'choose', title:'', align:'center', width:40, sortable:false, resizable:false, formatter:function(value,row,index) {
				        	return "<a href='javascript:void(0)' onclick='chooseCustomer(\""+ Utils.XSSEncode(row.shortCode) + "\");'>Chọn</a>";        
				        }}
				    ]]
				});
			}
		});
	   	Utils.bindFormatOnTextfield('toMoney',Utils._TF_NUMBER);
	   	Utils.bindFormatOnTextfield('fromMoney',Utils._TF_NUMBER);
 	   	Utils.formatCurrencyFor('toMoney');
 	   	Utils.formatCurrencyFor('fromMoney');
 	  	$('#btnRemoveDebit').hide(); 	  
	});
   
   	function chooseCustomer(code) {
		$('#shortCode').val(code);
		$('.easyui-dialog').dialog('close');
	}
</script>
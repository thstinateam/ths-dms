<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@	taglib prefix="k" uri="/kryptone"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<div class="Debt11Form">
       <div class="GeneralTable Table20Section">
           <div class="ScrollSection">
               <div class="BoxGeneralTTitle">
                   <table width="100%" border="0" cellspacing="0" cellpadding="0">
                      <colgroup>
                           <col style="width:40px;" />
                           <col style="width:90px;" />
                           <col style="width:90px;" />
                           <col style="width:130px;" />
                           <col style="width:200px;" />
                           <col style="width:90px;" />
                           <col style="width:90px;" />
                           <col style="width:90px;" />
                           <col style="width:100px;" />
                          </colgroup>
                       <thead>
                           <tr>
                               <th class="ColsThFirst">STT</th>
                               <th>Ngày nợ</th>
                               <th>Ngày phải trả</th>
                               <th>Số đơn hàng</th>
                               <th>NVBH</th>
                               <th>Số tiền</th>
                               <th>Đã trả</th>
                               <th>Còn nợ</th>
                               <th>Loại</th>
                           </tr>
                       </thead>
                   </table>
               </div>
               <div class="BoxGeneralTBody">
                   <div class="ScrollBodySection" id="delScroll">
                       <table id="tbCustomerDebit" width="100%" border="0" cellspacing="0" cellpadding="0" >
						<colgroup>
                           <col style="width:40px;" />
                           <col style="width:90px;" />
                           <col style="width:90px;" />
                           <col style="width:130px;" />
                           <col style="width:200px;" />
                           <col style="width:90px;" />
                           <col style="width:90px;" />
                           <col style="width:90px;" />
                           <col style="width:100px;" />
                          </colgroup>
                           <tbody>
                           <s:set var="totalAmounts" value="new java.math.BigDecimal(0)" />
							<s:set var="totalAmountPays" value="new java.math.BigDecimal(0)" />
							<s:set var="totalAmountRemains" value="new java.math.BigDecimal(0)" />
                           <s:if test="listdebitCustomer==null || listdebitCustomer.size()==0"><span style="font-size: 15px;">không có dữ liệu nào</span></s:if>
                           <s:else>
                              <s:iterator value="listdebitCustomer" status="stt" var="obj" >
                              	<tr>
									  <td class="ColsTd1">
									  	<input type="hidden" id="tdId_<s:property value="#obj.debitId"/>" value="<s:property value="#obj.debitId"/>"/>
									  	<s:property value="#stt.index+1"/>
									  </td>
									  <td class="ColsTd2"><s:property value="displayDate(#obj.createDate)"/></td>
									  <td class="ColsTd3"><s:property value="displayDate(#obj.payDate)"/></td>
									  <td class="ColsTd4"><s:property value="#obj.orderNumber"/></td>
									  <td class="ColsTd5"><s:property value="#obj.staffName" /></td>
							          <td class="ColsTd6"><s:property value="convertMoney(#obj.totalAmount)" /></td>
							          
							          <s:set var="totalAmounts" value="#totalAmounts.add(#obj.totalAmount)" />
						              <td class="ColsTd7" ><s:property value="convertMoney(#obj.totalAmountPay)" /></td>
						              <s:set var="totalAmountPays" value="#totalAmountPays.add(#obj.totalAmountPay)" />
						              <td class="ColsTd8" ><s:property value="convertMoney(#obj.totalAmountRemain)" /></td>
						              <s:set var="totalAmountRemains" value="#totalAmountRemains.add(#obj.totalAmountRemain)" />
						              <td class="ColsTd9 ColsTdEnd" >
						              <s:if test="getTypeDebitCustomer(#obj.payDate)==1">
						              		Quá hạn
						              </s:if>
						              <s:elseif test="getTypeDebitCustomer(#obj.payDate)==2">
						              		Tới hạn
						              </s:elseif>
						              <s:elseif test="getTypeDebitCustomer(#obj.payDate)==3">
						              		Chưa tới hạn
						              </s:elseif>				              
						              </td>
									</tr>
                              </s:iterator>
							</s:else>		
                           </tbody>
                       </table>
                   </div>
               </div>
               <div class="BoxGeneralTFooter">
               	<table width="100%" border="0" cellspacing="0" cellpadding="0">
                      	<colgroup>
                           <col style="width:40px;" />
                           <col style="width:90px;" />
                           <col style="width:90px;" />
                           <col style="width:130px;" />
                           <col style="width:200px;" />
                           <col style="width:90px;" />
                           <col style="width:90px;" />
                           <col style="width:90px;" />
                           <col style="width:100px;" />
                          </colgroup>
                          <tfoot>
                              <tr>
                                  <td class="ColsT5 ColsAlignRight" colspan="5" style="font-weight:bold;">Tổng :</td>
                                 <td class="ColsTd6" id="totalAmounts"  style="font-weight: normal;"><span id="totalAmounts"><s:property value="convertMoney(#totalAmounts)"/></span></td>
                                 <td class="ColsTd7" id="totalAmountPays"  style="font-weight: normal;"><span id="totalAmountPaysInt"><s:property value="convertMoney(#totalAmountPays)"/></span></td>
                                 <td class="ColsTd8" colspan="2" id="totalAmountRemains"  style="font-weight: normal;"><span id="totalAmountRemainsInt"><s:property value="convertMoney(#totalAmountRemains)"/></span> </td>
                                 
                              </tr>
						</tfoot>
                 </table>
               </div>
           </div>        
       </div>
       <s:if test="listdebitCustomer != null && listdebitCustomer.size() > 0">
	       	<div class="ButtonSection"><button id="btnExportDebit" class="BtnGeneralStyle Sprite2" onClick="return ShowDebit.exportExcel();"><span style="width: 70px;" class="Sprite2">Xuất file</span></button>
       </s:if>
        <s:else>
	   		<div class="ButtonSection"><button disabled="disabled" id="btnExportDebit" class="BtnGeneralStyle BtnGeneralDStyle Sprite2" onClick="return ShowDebit.exportExcel();"><span style="width: 70px;" class="Sprite2">Xuất file</span></button>
	  	</s:else>
       <img id="loading2" style="visibility: hidden;" src="/resources/images/loading.gif" class="LoadingStyle" /></div>
       </div>
       <p class="ErrorMsgStyle Sprite1" id="errMsg2" style="display: none;"></p>
       <input type="hidden" id="checkdata" value="<s:property value="checkdata"/>"/>
	<s:if test="listdebitCustomer!=null && listdebitCustomer.size()>0">
               	<input type="hidden" id="listSize" value="1" />	
	   </s:if>
	   <s:else>
	   	<input type="hidden" id="listSize" value="0" />	
	  </s:else>
<script>
   $(document).ready(function(){
	  
	   
	});
   
   </script>
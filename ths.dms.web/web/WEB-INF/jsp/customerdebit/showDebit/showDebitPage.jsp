<%@ page language="java" contentType="text/html; charset=UTF-8"	pageEncoding="UTF-8"%>
<%@page import="ths.dms.helper.Configuration"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>

<style type="text/css">
#statusPayDiv.BoxSelect .ui-dropdownchecklist-text, #statusPayDiv.BoxSelect .ui-dropdownchecklist-selector {
    width: 173px !important;
}
</style>

<input type="hidden" id="function_code" value="<s:property value="systimestamp"/>" />
<s:set id="downloadToken" value="%{generateReportToken()}"></s:set>
<input type="hidden" id="report_token" value="<s:property value="#downloadToken"/>" />
<s:set id="downloadToken" value="%{putSessionValue(systimestamp, #downloadToken)}"></s:set>

<div class="BreadcrumbSection">
	<ul class="ResetList FixFloat BreadcrumbList">
		<li class="Sprite1"><a href="javascript:void(0)">Công nợ khách hàng</a></li>
		<li><span>Xem công nợ khách hàng</span></li>
	</ul>
</div>

<div class="CtnOneColSection">
	<div class="ContentSection">
		<div class="ToolBarSection">
			<div class="SearchSection GeneralSSection">
				<h2 class="Title2Style">Thông tin chung</h2>
				<div class="SearchInSection SProduct1Form">
					<label class="LabelStyle Label1Style"><s:text name="common.shop.name.lable"/>
                       	<span class="ReqiureStyle"><font color="red"> *</font></span>
                    </label>
					<div class="BoxSelect BoxSelect2">
						<div class="Field2">
							<input class="easyui-combobox" id="shop" style="width:206px;">
						</div>
					</div>
					<label class="LabelStyle Label1Style">Mã KH (F9)</label> 
					<input type="text" class="InputTextStyle InputText1Style" id="shortCode" maxlength="50" onChange="return myFun();"/> 
					
					<label  class="LabelStyle Label1Style">Tên KH</label>
					<input type="text" class="InputTextStyle InputText1Style" id="customerName" disabled="disabled"/> 
					
					<div class="Clear"></div>
					
					<label class="LabelStyle Label1Style">Từ Ngày </label>
					<input type="text" class="InputTextStyle InputText6Style" id="fromDate"  maxlength="10" onchange="return myFunDate();"/>
					
					<label class="LabelStyle Label1Style">Đến Ngày </label>
					<input type="text" class="InputTextStyle InputText6Style" id="toDate"  onchange="return myFunDate();" maxlength="10"/>
					
					<label class="LabelStyle Label1Style">Số đơn hàng</label>
					<input type="text" class="InputTextStyle InputText1Style" id="orderNumber" maxlength="250" autocomplete="off" />
					
					<div class="Clear"></div>
					
					<label class="LabelStyle Label1Style">NVBH</label>
					<div class="BoxSelect BoxSelect2" id="idDivStaffCode">
						<select class="easyui-combobox" id="staffCode" style="width: 206px">
						</select>
					</div>
					
					<label class="LabelStyle Label1Style">NVGH</label> 
					<div class="BoxSelect BoxSelect2" id="idDivtransferStaff">
						<select class="easyui-combobox" id="transferStaff" style="width: 206px">
						</select>
					</div>
					
					<label class="LabelStyle Label1Style">NVTT</label>
					<div class="BoxSelect BoxSelect2" id="idDivCrashierStaff">
						<select class="easyui-combobox" id="crashierStaff" style="width: 206px">
						</select>
					</div>
					<div class="Clear"></div>
					<label class="LabelStyle Label1Style">Loại</label>
					<div class="BoxSelect BoxSelect2">
						<select id="status" class="MySelectBoxClass" style="width: 206px">
	                       	<option value="-1" selected="selected">Tất cả</option>
							<option value="0">Chưa tới hạn</option>
							<option value="1">Tới hạn</option>
							<option value="2">Quá hạn</option>
							<option value="3">Đã tất toán</option>
	                   	</select>
					</div>
					
					<label class="LabelStyle Label1Style">Trạng thái</label>
                    <div class="BoxSelect BoxSelect2" id="statusPayDiv">
                        <select id="cboStatusPay" multiple="multiple" style="width: 206px !important;">
                        	<option value="-2" selected="selected">Tất cả</option>      
         					<option value="0" >Chưa thanh toán</option>      
         					<option value="1" >Đã thanh toán 1 phần</option>
         					<option value="2" >Đã tất toán</option>   					
     					</select>
                   </div>
					<div class="Clear"></div>
					
					<div class="BtnCenterSection">
						<button id="btnSearch" class="BtnGeneralStyle cmsiscontrol" onClick="return ShowDebit.getList();" >Xem</button> &nbsp;
					</div>
					<p class="ErrorMsgStyle" id="errMsg" style="display: none;"></p>
                    <p class="ErrorMsgStyle" id="errMsg001" style="display: none;"></p>
                    <input id="shopCode" type="hidden"  value="<s:property value="shopCode"/>"/>					
				</div>
				<div id = "resultSection" style="display: none;">
					<h2 class="Title2Style">Chi tiết công nợ khách hàng</h2>
					<div class="SearchInSection SProduct1Form">
						<div class="GridSection" id="gridContainer" style="padding: 0px;">
							<table id="dg"></table>
							<div class="BtnCenterSection">
								<button class="BtnGeneralStyle cmsiscontrol" id="btnExportDebit" onClick="return ShowDebit.exportExcel();">Xuất File Excel</button> &nbsp;						
							</div>
							<div class="Clear"></div>
		       				<p id="successMsg" class="SuccessMsgStyle" style="display: none;"></p>
		       				<p class="ErrorMsgStyle" id="errMsg2" style="display: none;"></p>
						</div>
					</div>
					<div class="Clear"></div>
				</div>
				<div class="Clear"></div>
			</div>
		</div>
	</div>
</div>
<script type="text/javascript">
   $(document).ready(function() {
		//load combobox don vi
		Utils.initUnitCbx('shop', null, 206, function(rec) {
	    	if (rec != null && rec.shopCode != ReportUtils._currentShopCode) {
				$('#shopCode').val(rec.shopCode);
				ReportUtils._currentShopCode = rec.shopCode;
				ShowDebit.changeShop();
	    	}
		}, function(arr) {
	       	if (arr != null && arr.length > 0) {
	       		ReportUtils._currentShopCode = arr[0].shopCode;
	       	}
		});
	   	$('#cboStatusPay').dropdownchecklist({emptyText:'Tất cả',firstItemChecksAll: true, width:175});
	   //$('#status').customStyle();
	   applyDateTimePicker('#fromDate');
	   applyDateTimePicker('#toDate');
	   Utils.bindComboboxStaffEasyUI('crashierStaff');
	   Utils.bindComboboxStaffEasyUI('transferStaff');
	   Utils.bindComboboxStaffEasyUI('staffCode');
	   $('#shortCode').focus();
	   $('#shortCode').bind('keyup', function(event){
			if(event.keyCode == keyCode_F9){
				VCommonJS.showDialogSearch2({
				    inputs : [
				        {id:'code', maxlength:50, label:'Mã KH'},
				        {id:'name', maxlength:250, label:'Tên KH'},
				        {id:'address', maxlength:250, label:'Địa chỉ', width:410}
				    ],
				    params:{shopCode:$('#shopCode').val()},
				    url : '/commons/customer-in-shop/search',
				    onShow : function() {
			        	$('.easyui-dialog #code').focus();
				    },
				    columns : [[
				        {field:'shortCode', title:'Mã KH', align:'left', width: 110, sortable:false, resizable:false, formatter: function(v, r, i){
				        	if (v) {
				        		return Utils.XSSEncode(v);
				        	}
				        	return "";
				        }},
				        {field:'customerName', title:'Tên KH', align:'left', width: 150, sortable:false, resizable:false, formatter: function(v, r, i){
				        	if (v) {
				        		return Utils.XSSEncode(v);
				        	}
				        	return "";
				        }},
				        {field:'address', title:'Địa chỉ', align:'left', width: 170, sortable:false, resizable:false, formatter: function(v, r, i){
				        	if (v) {
				        		return Utils.XSSEncode(v);
				        	}
				        	return "";
				        }},
				        {field:'choose', title:'', align:'center', width:40, sortable:false, resizable:false, formatter:function(value,row,index) {
				        	return "<a href='javascript:void(0)' onclick='chooseCustomer(\""+ Utils.XSSEncode(row.shortCode) + "\",\""+Utils.XSSEncode(row.customerName)+"\");'>Chọn</a>";        
				        }}
				    ]]
				});
			}
		});
	   $('#resultSection').show();
	});
   function myFun(){
	   if($('#shortCode').val().trim().length==0){
		   $('#customerName').val('');
	   }else{
		   	$.getJSON('/rest/catalog/showdebit/showdebitGetName/'+ $('#shortCode').val().trim() +'/'+$('#shopCode').val().trim()+'/list.json', function(data){
		   		$('#customerName').val(data.name).show();				
		   	});
	  }
	  $("#tableDetail tbody tr").remove();
	  $("#totalAmounts").html('0');
	  $("#totalAmountPaysInt").html('0');
	  $("#totalAmountRemainsInt").html('0');
	  $('#titleGrid').html('Chi tiết công nợ khách hàng'+ '(Công nợ hiện tại:<font color="red"> 0 VNĐ</font>)');
	  totalDebit = "0";
	  /* if($("#btnExportDebit").attr('disabled')==undefined){
		  $("#btnExportDebit").addClass('BtnGeneralDStyle');
		  $("#btnExportDebit").attr('disabled','disabled');
	  } */
	  return true;
   }
   function myFunDate(){
		  $("#tableDetail tbody tr").remove();
		  $("#tableDetail tbody tr").remove();
		  /* if($("#btnExportDebit").attr('disabled')==undefined){
			  $("#btnExportDebit").addClass('BtnGeneralDStyle');
			  $("#btnExportDebit").attr('disabled','disabled');
		  } */
		  return true;
   }

   function chooseCustomer(code, name) {
	   	$('#shortCode').val(code);
		$('#customerName').val(name);
		$('.easyui-dialog').dialog('close');
   }
   </script>
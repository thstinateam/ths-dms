<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>

<div class="BreadcrumbSection">
	<ul class="ResetList FixFloat BreadcrumbList">
		<li class="Sprite1"><a href="javascript:void(0)">Công nợ khách hàng</a></li>
		<li><span>Lập phiếu điều chỉnh công nợ</span></li>
	</ul>
</div>

<div class="CtnOneColSection">
	<div class="ContentSection">
		<div class="ToolBarSection">
			<div class="SearchSection GeneralSSection">
				<h2 class="Title2Style">Thông tin phiếu</h2>
				<div class="SearchInSection SProduct1Form">
					<label class="LabelStyle Label1Style">Loại phiếu <span class="ReqiureStyle">*</span></label> 
					<div class="BoxSelect BoxSelect2">
						<select id="debitType" class="MySelectBoxClass" autocomplete="off">
							<option value="1">Điều chỉnh tăng nợ</option>
							<option value="2">Điều chỉnh giảm nợ</option>
						</select>
					</div>
								
					<label class="LabelStyle Label1Style">Đối tượng <span class="ReqiureStyle">*</span></label> 
					<div class="BoxSelect BoxSelect2">
						<select id="type" class="MySelectBoxClass" onchange="DebitIncrement.typeChange();" autocomplete="off">
							<option value="1">KH - NPP</option>
							<option value="2">NPP - Công ty</option>
						</select>
					</div>
					
					<label class="LabelStyle Label1Style">Số phiếu <span class="ReqiureStyle">*</span></label> 
					<input type="text" class="InputTextStyle InputText1Style" id="dbNumber" maxlength="40" autocomplete="off" />
					<div class="Clear"></div>
					
					<label class="LabelStyle Label1Style">Số tiền <span	class="ReqiureStyle">*</span></label> 
					<input type="text" class="InputTextStyle InputText1Style" id="amount"  maxlength="17" autocomplete="off" />
					
					<label class="LabelStyle Label1Style">Lý do <span	class="ReqiureStyle">*</span></label> 
					<input type="text" class="InputTextStyle InputText1Style" id="reason"  maxlength="250" autocomplete="off" />
					
					<label class="LabelStyle Label1Style">Khách hàng(F9) <span class="ReqiureStyle">*</span></label> 
					<input type="text" class="InputTextStyle InputText1Style" id="shortCode" maxlength="50" autocomplete="off" />
					<div class="Clear"></div>
                    
                    <div class="BtnCenterSection">
						<button id="btnSearch" class="BtnGeneralStyle" onClick="DebitIncrement.saveDebit();">Lưu</button>
					</div>
					
					<p id="errMsg" class="ErrorMsgStyle" style="display:none;"></p>
					<p id="successMsg" class="SuccessMsgStyle" style="display:none;"></p>
				</div>
			</div>
		</div>
	</div>
</div>

<script type="text/javascript">
$(document).ready(function() {
	$('.ContentSection').unbind('keyup');
	$('.InputTextStyle').unbind('keyup');
	$('#type').customStyle();
	$('#debitType').customStyle();
	$('#dbNumber').focus();
	
	Utils.formatCurrencyFor('amount');
	Utils.bindFormatOnTextfield('amount', Utils._TF_NUMBER_COMMA);
	
	$('#shortCode').parent().bind('keyup',function(event){
		if(event.keyCode == keyCodes.ENTER){		   				
			$('#btnSearch').click();
		}
	});
	
	$('#shortCode').bind('keyup', function(event) {
		var arrParam = new Array();
		var param = new Object();
		param.name = 'shopCode';
		param.value = $('#shopCode').val();
		arrParam.push(param);
		if (event.keyCode == keyCode_F9) {
			CustomerDebitBatch._checkF9 = true;
			CommonSearch.searchCustomerOnDialog(function(data) {
				$('#shortCode').val(data.code);
				$('#shortCode').focus();
			}, arrParam);
		}
	});
});
</script>
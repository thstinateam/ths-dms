<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>

<div class="BreadcrumbSection">
	<ul class="ResetList FixFloat BreadcrumbList">
		<li class="Sprite1"><a href="javascript:void(0);" class="cmsdefault">Công nợ khách hàng</a></li>
		<li><span>Lập phiếu điều chỉnh công nợ</span></li>
	</ul>
</div>

<div class="CtnOneColSection">
	<div class="ContentSection">
		<div class="ToolBarSection">
			<div class="SearchSection GeneralSSection">
				<h2 class="Title2Style">Thông tin phiếu</h2>
				<div class="SearchInSection SProduct1Form">
					<label class="LabelStyle Label1Style"><s:text name="common.shop.name.lable"/>
                       	<span class="ReqiureStyle"><font color="red"> *</font></span>
                    </label>
					<div class="BoxSelect BoxSelect2">
						<div class="Field2">
							<input id="shop" style="width:200px;">
						</div>
					</div>
					<div class="Clear"></div>
					<div class="GridSection" id="gridContainer" style="padding: 0px;max-height:450px;overflow-x:hidden;overflow-y:scroll;">
						<table id="dgrid"></table>
					</div>
					<div class="Clear"></div>
                    
                    <div class="BtnCenterSection">
						<button id="btnSave" class="BtnGeneralStyle cmsiscontrol" onClick="DebitAdjustment.saveDebit();">Lưu</button>
					</div>
					
					<p id="errMsg" class="ErrorMsgStyle" style="display:none;"></p>
					<p id="successMsg" class="SuccessMsgStyle" style="display:none;"></p>
				</div>
			</div>
		</div>
	</div>
</div>

<input type="hidden" id="shopCode" value="<s:property value='shopCode' />" />

<script type="text/javascript">
$(document).ready(function() {
	//load combobox don vi
	Utils.initUnitCbx('shop', null, 206, function(rec) {
    	if (rec != null && rec.shopCode != ReportUtils._currentShopCode) {
			$('#shopCode').val(rec.shopCode);
			ReportUtils._currentShopCode = rec.shopCode;
			DebitAdjustment.initGrid();
    	}
	}, function(arr) {
       	if (arr != null && arr.length > 0) {
       		ReportUtils._currentShopCode = arr[0].shopCode;
       	}
	});
	
	$('.ContentSection').unbind('keyup');
	$('.InputTextStyle').unbind('keyup');

// 	DebitAdjustment.initGrid();
});

function chooseCustomer(shortCode, r, c) {
	var htGrid = $('#dgrid').handsontable('getInstance');
	htGrid.setDataAtCell(r,c,shortCode);
	htGrid.render();
	$(".easyui-dialog").dialog("close");
}
</script>
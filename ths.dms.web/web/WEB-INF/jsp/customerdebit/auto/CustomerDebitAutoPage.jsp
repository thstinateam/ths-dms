<%@ page language="java" contentType="text/html; charset=UTF-8"	pageEncoding="UTF-8"%>
<%@page import="ths.dms.helper.Configuration"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>

<div class="BreadcrumbSection">
	<ul class="ResetList FixFloat BreadcrumbList">
		<li class="Sprite1"><a href="javascript:void(0);" class="cmsdefault">Công nợ khách hàng</a></li>
		<li><span>Thanh toán tự động</span></li>
	</ul>
</div>

<div class="CtnOneColSection">
	<div class="ContentSection">
		<div class="ToolBarSection">
			<div class="SearchSection GeneralSSection">
				<h2 class="Title2Style">Nhập thanh toán</h2>
				<div class="SearchInSection SProduct1Form">
					<label class="LabelStyle Label1Style"><s:text name="common.shop.name.lable"/>
                       	<span class="ReqiureStyle"><font color="red"> *</font></span>
                    </label>
					<div class="BoxSelect BoxSelect2">
						<div class="Field2">
							<input class="easyui-combobox" id="shop" style="width:206px;">
						</div>
					</div>
					<label class="LabelStyle Label1Style">Khách hàng (F9)<span class="ReqiureStyle">*</span></label> 
					<input type="text" class="InputTextStyle InputText1Style" id="shortCode" onChange="CustomerDebitAuto.getCurrentDebit();" maxlength="50" autocomplete="off" /> 
					
					<label class="LabelStyle Label1Style">Loại chứng từ <span class="ReqiureStyle">*</span></label>
					<div class="BoxSelect BoxSelect2" id="idDivStatus">
						<select id="status" class="MySelectBoxClass" autocomplete="off" onchange="CustomerDebitAuto.getCurrentDebit();">
							<!-- <option value="-1" selected="selected">Phiếu thu/Phiếu chi</option> -->
							<option value="2">Phiếu chi</option>
	                       	<option value="1" selected="selected">Phiếu thu</option>
	                   	</select>
					</div>
					<div class="Clear"></div>
					
					<label class="LabelStyle Label1Style">Từ ngày</label>
                    <input type="text" class="InputTextStyle InputText6Style" onchange="CustomerDebitAuto.getCurrentDebit();" id="fromDate" maxlength="10" autocomplete="off" />
                    
                    <label class="LabelStyle Label1Style">Đến ngày</label>
                    <input type="text" class="InputTextStyle InputText6Style" onchange="CustomerDebitAuto.getCurrentDebit();" id="toDate" maxlength="10" autocomplete="off" />
                    
                    <label class="LabelStyle Label1Style">Số chứng từ <span class="ReqiureStyle">*</span></label> 
					<input type="text" class="InputTextStyle InputText1Style" id="payReceivedNumber" maxlength="20" value="<s:property value="payReceivedNumber"/>" autocomplete="off"/> 
					
					<div class="Clear"></div>
					
					<label class="LabelStyle Label1Style">NVGH</label> 
					<div class="BoxSelect BoxSelect2" id = "idDivTransferStaff">
						<select class="easyui-combobox" id="transferStaff" style="width: 206px">
						</select>
					</div>

					<label class="LabelStyle Label1Style">NVTT</label>
					<div class="BoxSelect BoxSelect2" id = "idDivCrashierStaff">
						<select class="easyui-combobox" id="crashierStaff" style="width: 206px">
						</select>
					</div>
					
					<label class="LabelStyle Label1Style">Số tiền <span	class="ReqiureStyle">*</span></label> 
					<input type="text" class="InputTextStyle InputText1Style" id="valuePay"  maxlength="20" autocomplete="off" />
					<div class="Clear"></div>
					
					<label class="LabelStyle Label1Style">Công nợ trước TT</label> 
					<input type="text" class="InputTextStyle InputText1Style" disabled="disabled" id="beforePay" disabled="disabled" value="<s:property value="beforePay"/>" autocomplete="off" /> 
					
					<label class="LabelStyle Label1Style">Công nợ sau TT</label>					 
					<input type="text" class="InputTextStyle InputText1Style" id="afterPay" disabled="disabled" autocomplete="off" />
					<div class="Clear"></div>
					<div class="BtnCenterSection">
						<button id="btnSearch" class="BtnGeneralStyle cmsiscontrol" onClick="return CustomerDebitAuto.getList();" >Phân bổ</button> &nbsp;
					</div>
					<p class="ErrorMsgStyle" id="errMsgTop"	style="display: none;">Có lỗi xảy ra khi cập nhật dữ liệu</p>					
				</div>				
				<h2 class="Title2Style">Danh sách đơn hàng còn nợ</h2>
				<div class="SearchInSection SProduct1Form">
					<div class="GridSection" id="gridContainer" style="padding: 0px;">
						<table id="dg"></table>
					</div>
					<div class="BtnCenterSection">
						<button class="BtnGeneralStyle cmsiscontrol" id="btnAuto" onClick="return CustomerDebitAuto.savePay();" >Lưu thanh toán</button> &nbsp;						
					</div>
					<div class="Clear"></div>	
					<p class="ErrorMsgStyle" id="errMsg" style="display: none;">Có lỗi xảy ra khi cập nhật dữ liệu</p> 
					<p id="successMsg" class="SuccessMsgStyle" style="display: none;"></p>
	                <p id="warning2" class="SuccessMsgStyle" style="display: none;">Số tiền còn nợ <span class="myWarning"><s:property value="ttt"/></span> VND </p>
	                <p class="ErrorMsgStyle" id="warning4" style="display: none;">Khách hàng không tồn tại.</p>
	                <p class="ErrorMsgStyle" id="warning5" style="display: none;">Nhân viên không tồn tại.</p>          		
				</div>
				<div class="Clear"></div>
			</div>
			<div class="Clear"></div>
		</div>
	</div>
</div>


<input id ="shopCode" type="hidden" value="<s:property value="shopCode"/>" autocomplete="off" />
<input id ="debitKHToNPP" type="hidden" value="<s:property value="currentDebit"/>" autocomplete="off" />
<input id ="debitNPPToKH" type="hidden" value="<s:property value="currentDebitNPP"/>" autocomplete="off" />


<script type="text/javascript">
$(document).ready(function() {
	//load combobox don vi
	Utils.initUnitCbx('shop', null, 206, function(rec) {
    	if (rec != null && rec.shopCode != ReportUtils._currentShopCode) {
			$('#shopCode').val(rec.shopCode);
			ReportUtils._currentShopCode = rec.shopCode;
			CustomerDebitAuto.changeShop();
    	}
	}, function(arr) {
       	if (arr != null && arr.length > 0) {
       		ReportUtils._currentShopCode = arr[0].shopCode;
       	}
	});
	$(".InputTextStyle").unbind("keyup");
	$('#shortCode').parent().unbind('keyup');
	VTUtilJS.applyDateTimePicker("fromDate");
	VTUtilJS.applyDateTimePicker("toDate");
	Utils.bindComboboxStaffEasyUI('crashierStaff');
	Utils.bindComboboxStaffEasyUI('transferStaff');
	isFirstLoad = true;
	//$('#status').customStyle();
	$('#shortCode').focus();
	$('#shortCode').parent().bind('keyup',function(event){
		if(event.keyCode == keyCodes.ENTER){		   				
			$('#btnSearch').click();				
		}
	});
	$('#shortCode').bind('keyup',function(event){
		if(event.keyCode == keyCode_F9){
			VCommonJS.showDialogSearch2({
			    inputs : [
			        {id:'code', maxlength:50, label:'Mã KH'},
			        {id:'name', maxlength:250, label:'Tên KH'},
			        {id:'address', maxlength:250, label:'Địa chỉ', width:410}
			    ],
			    params:{shopCode: $('#shopCode').val()},
			    url : '/commons/customer-in-shop/search',
			    onShow : function() {
		        	$('.easyui-dialog #code').focus();
			    },
			    columns : [[
			        {field:'shortCode', title:'Mã KH', align:'left', width: 110, sortable:false, resizable:false, formatter: function(v, r, i){
			        	if (v) {
			        		return Utils.XSSEncode(v);
			        	}
			        	return "";
			        }},
			        {field:'customerName', title:'Tên KH', align:'left', width: 150, sortable:false, resizable:false, formatter: function(v, r, i){
			        	if (v) {
			        		return Utils.XSSEncode(v);
			        	}
			        	return "";
			        }},
			        {field:'address', title:'Địa chỉ', align:'left', width: 170, sortable:false, resizable:false, formatter: function(v, r, i){
			        	if (v) {
			        		return Utils.XSSEncode(v);
			        	}
			        	return "";
			        }},
			        {field:'choose', title:'', align:'center', width:40, sortable:false, resizable:false, formatter:function(value,row,index) {
			        	return "<a href='javascript:void(0)' onclick='chooseCustomer(\""+ row.shortCode + "\");'>Chọn</a>";        
			        }}
			    ]]
			});
		}else{
			$('#btnAuto').hide();
		}
	});
	$('#valuePay').bind('keyup',function(event){
		$('#btnAuto').hide();
	});
	$('#status').bind('change',function(event){
		$('#btnAuto').hide();
	});
	
	Utils.bindFormatOnTextfield('valuePay',Utils._TF_NUMBER);
	Utils.formatCurrencyFor('valuePay');
	Utils.formatCurrencyFor('beforePay');
	Utils.formatCurrencyFor('afterPay');	
});
function myFun01(){
	$("#debitNPPToKH").val("");
	$("#debitKHToNPP").val("");
	$("#beforePay").val("");
	$("#afterPay").val("");
	if($('#shortCode').val().trim().length > 0) {
		$('#beforePay').val('');
		$('#valuePay').val('');
		$('#afterPay').val('');
		var url = 'shopCode='+ $('#shopCode').val() + '&shortCode='+$('#shortCode').val();
		$.getJSON('/customerdebit/auto/currentDebit?'+url, function(data){
			if(data.error) {
				$('#errMsgTop').html(data.errMsg).show();
			} else {
				var chooseStatus = $('#status').val();
				if(chooseStatus == 2){ //dang phieu chi
					$('#beforePay').val(formatCurrency(data.currentDebitNPP));
				}else{
					$('#beforePay').val(formatCurrency(data.currentDebit));
				}
				$('#debitKHToNPP').val(data.currentDebit);
				$('#debitNPPToKH').val(data.currentDebitNPP);						
			}
		});		
	}
}
function myFun(){
	if ($('#valuePay').val().trim()!= ''){
		var a = parseInt(Utils.returnMoneyValue($('#valuePay').val().trim()),10);		
		var b = parseInt(Utils.returnMoneyValue($('#beforePay').val().trim()),10);
		var c; var money;
		/* if($('#status').val()==2) {
			c = b + a;
		} else {
			c  = b - a;
		} */
		c = b - a;
		if(c<0){
			c= (-1)*c;
			money=formatCurrency(c);
			money='-'+money;
		}else{
			money=formatCurrency(c);
		}
		$('#afterPay').val(money);		
	}else{
		$('#afterPay').val('');
	}
}

function chooseCustomer(code) {
	$('#shortCode').val(code);
	$('#btnAuto').hide();
	$('.easyui-dialog').dialog('close');
	CustomerDebitAuto.getCurrentDebit();
// 	var url = 'shopCode='+ $('#shopCode').val() + '&shortCode='+$('#shortCode').val();
// 	$.getJSON('/customerdebit/auto/currentDebit?'+url, function(data){
// 		if(data.error) {
// 			$('#errMsgTop').html(data.errMsg).show();
// 		} else {
// 			var chooseStatus = $('#status').val();
// 			if(chooseStatus == 2){ //dang phieu chi
// 				$('#beforePay').val(formatCurrency(data.currentDebitNPP));
// 			}else{
// 				$('#beforePay').val(formatCurrency(data.currentDebit));
// 			}
// 			$('#debitKHToNPP').val(data.currentDebit);
// 			$('#debitNPPToKH').val(data.currentDebitNPP);
// 		}
// 	});
}
</script>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@	taglib prefix="k" uri="/kryptone"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<div class="Debt11Form">
        <div class="Clear"></div>
           <div class="GeneralTable Table17Section">
               <div class="ScrollSection">
                   <div class="BoxGeneralTTitle">

                       <table width="100%" border="0" cellspacing="0" cellpadding="0">
                           <colgroup>
                               <col style="width:90px;" />
                               <col style="width:90px;" />
                               <col style="width:135px;" />
                               <col style="width:114px;" />
                               <col style="width:160px;" />
                               <col style="width:100px;" />
                               <col style="width:110px;" />

                               <col style="width:121px;" />
                           </colgroup>
                           <thead>
                               <tr>
                                   <th class="ColsThFirst">STT</th>
                                   <th>Mã KH</th>
                                   <th>Đơn hàng số</th>
                                   <th>Ngày</th>
                                   <th>Số tiền</th>
                                   <th>Đã trả</th>
                                   <th><input class="cb_element" name="chkApprove" type="checkbox" id="td_chkApprove" disabled="disabled" checked="checked"/></th>
                                   <th class="ColsThEnd">Giá trị trả</th>
                               </tr>
                           </thead>

                       </table>
                   </div>
                   <div class="BoxGeneralTBody">
                       <div class="ScrollBodySection" id="customerAutoScroll">
                           <table width="100%" border="0" cellspacing="0" cellpadding="0" id="tbDetails">
                               <colgroup>
                                   <col style="width:90px;" />
                                   <col style="width:90px;" />
                                   <col style="width:135px;" />
                                   <col style="width:114px;" />
                                   <col style="width:160px;" />
                                   <col style="width:100px;" />
                                   <col style="width:110px;" />
                                   <col style="width:121px;" />
                               </colgroup>
                               <tbody>
                                   <k:repeater id="listCustomerDebit" value="listCustomerDebit" status="status">
									<k:itemTemplate>
									<tr>
									  <td class="ColsTd1">
									  	<input type="hidden" class="debitId" value="<s:property value="debitId"/>"/>
									  	<s:property value="#status.index+1"/>
									  </td>
									  <td class="ColsTd2"><s:property value="shortCode"/></td>
									  <td class="ColsTd3"><s:property value="orderNumber"/></td>
									  <td class="ColsTd4 AlignRight"><s:date name="orderDate" format="dd/MM/yyyy" /></td>
									  
									  	<s:if test="total >= 0">
											<td class="ColsTd5 AlignRight"><span class="abc"><s:property value="total"/></span></td>
										</s:if>
						            	<s:else>
											<td class="ColsTd5 AlignRight"><span class="abc"><s:property value="-total"/></span></td>
										</s:else>
									  
									  	<s:if test="totalPay >= 0">
											<td class="ColsTd6 AlignRight"><span class="abc"><s:property value="totalPay"/></span></td>
										</s:if>
						            	<s:else>
											<td class="ColsTd6 AlignRight"><span class="abc"><s:property value="-totalPay"/></span></td>
										</s:else>
							            
						            	<td class="ColsTd7 " >
						            		<input class="cb_element" name="ch_debit" type="checkbox" id="ch_debit" checked="checked" disabled="disabled"/>
						            	</td>
						            	<s:if test="payAmt >= 0">
											<td class="ColsTd8 AlignRight ColsTdEnd abc"><s:property value="payAmt"/></td>
										</s:if>
						            	<s:else>
											<td class="ColsTd8 AlignRight ColsTdEnd abc"><s:property value="-payAmt"/></td>
										</s:else>
						            	<td style="display:none;" class="abcd"><s:property value="payAmt"/></td>
									</tr>
									</k:itemTemplate>
									</k:repeater>
                               </tbody>
                           </table>
                       </div>
                   </div>                                                                            
               </div>        
           </div>
           		 <div class="ButtonSection"><button style="display: none;" id="btnAuto" class="BtnGeneralStyle Sprite2" onClick="return CustomerDebitAuto.savePay();"><span class="Sprite2">Lưu thanh toán</span></button>
          		<img id="loading2" style="visibility: hidden;" src="/resources/images/loading.gif" class="LoadingStyle" /></div>
                <p class="ErrorMsgStyle Sprite1" id="errMsg" style="display: none;">Có lỗi xảy ra khi cập nhật dữ liệu</p>
                <p id="warning2" class="SuccessMsgStyle" style="display: none;">Số tiền còn nợ <span class="myWarning"><s:property value="ttt"/></span> VND </p>
                <p class="ErrorMsgStyle Sprite1" id="warning4" style="display: none;">Khách hàng không tồn tại.</p>
                <p class="ErrorMsgStyle Sprite1" id="warning5" style="display: none;">Nhân viên không tồn tại.</p>
       </div>
 <script>
$(document).ready(function(){
	$('#customerAutoScroll').jScrollPane();
	$('.abc').each(function () {
			if($(this).html().length > 0){
				$(this).html(formatCurrency($(this).html()));
			}		
		});
});
	if (<s:property value="warning"/>!=null && <s:property value="warning"/>!= undefined) 
	{
			if (<s:property value="warning"/>==1)
			{
			 $('#warning1').html("Số tiền phải được thanh toán hết.Gợi ý: có thể trả tối đa <span class=\"myWarning\">"+ <s:property value="totalIsReceived"/> + "</span> VND").show();
			 $('#afterPay').val('');
			 $('#valuePay').focus();
			}else if (<s:property value="warning"/>==2){
				if (<s:property value="status"/>==2){
					$('#warning2').html('Số tiền còn phải chi: '+ formatCurrency(parseInt(<s:property value="ttt"/>),10)+' VNĐ').show();
				}else{
					$('#warning2').html('Số tiền còn nợ của KH '+$('#shortCode').val().toUpperCase()+' thông qua NVBH '+$('#staff2Code').val().toUpperCase()+' là: '+formatCurrency(parseInt(<s:property value="ttt"/>),10) +' VNĐ').show();	
				}
				$('#btnAuto').show();
				myFun();
			}else if (<s:property value="warning"/>==3){
				$('#warning3').html('Nợ của KH: <b> '+$('#shortCode').val().toUpperCase()+' </b> với NPP thông qua NVBH: <b> '+$('#staff2Code').val().toUpperCase()+' </b> đã được thanh toán hết.').show();
				$('#afterPay').val('');
				myFun();
			}else if (<s:property value="warning"/>==4){
				$('#errMsgTop').html('Khách hàng không tồn tại').show();
				$('#shortCode').focus();
				$('#afterPay').val('');
			}else if (<s:property value="warning"/>==5){
				$('#errMsgTop').html('Nhân viên không tồn tại').show();
				$('#staff2Code').focus();
				$('#afterPay').val('');
			}
	}
	$('.myWarning').each(function () {
		if($(this).html().length > 0){
			$(this).html(formatCurrency($(this).html()));
		}		
	});
	
	function myFun(){
		if ($('#valuePay').val().trim()!= ''){
			var a = parseInt(Utils.returnMoneyValue($('#valuePay').val().trim()),10);		
			var b = parseInt(Utils.returnMoneyValue($('#beforePay').val().trim()),10);
			var c; var money;
			if($('#status').val()==2) c = b + a;
			else c  = b - a;
			if(c<0){
				c= (-1)*c;
				money=formatCurrency(c);
				money='-'+money;
			}else{
				money=formatCurrency(c);
			}
			$('#afterPay').val(money);		
		}else{
			$('#afterPay').val('');
		}
	}
 </script>
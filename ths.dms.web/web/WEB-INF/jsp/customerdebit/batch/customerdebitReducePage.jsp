<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<div class="BreadcrumbSection">
	<ul class="ResetList FixFloat BreadcrumbList">
		<li class="Sprite1"><a href="javascript:void(0)">Công nợ khách hàng</a></li>
		<li><span>Giảm nợ khách hàng</span></li>
	</ul>
</div>
<div class="CtnOneColSection">
	<div class="ContentSection">
		<div class="ToolBarSection">
			<div class="SearchSection GeneralSSection">
				<h2 class="Title2Style">Thông tin tìm kiếm</h2>
				<div class="SearchInSection SProduct1Form">
					<label class="LabelStyle Label1Style">Khách hàng(F9)</label> 
					<input type="text" class="InputTextStyle InputText1Style" id="shortCode" maxlength="50" autocomplete="off" /> 			
					
					<label class="LabelStyle Label1Style">Loại chứng từ<span class="ReqiureStyle">*</span></label>
					<input type="text" class="InputTextStyle InputText1Style" id="type" value="Phiếu thu" disabled="disabled"/>
					<div class="Clear"></div>
					
					<label class="LabelStyle Label1Style">Từ ngày</label>
                    <input type="text" class="InputTextStyle InputText6Style" value="<s:property value='fromDate'/>" id="fromDate" maxlength="10" autocomplete="off"/>
                    
                    <label class="LabelStyle Label1Style">Đến ngày</label>
                    <input type="text" class="InputTextStyle InputText6Style" value="<s:property value='toDate'/>" id="toDate" maxlength="10" autocomplete="off"/>
					<div class="Clear"></div>
                    
					<label class="LabelStyle Label1Style">NVGH</label> 
					<div class="BoxSelect BoxSelect2" id="idDivtransferStaff">
						<select class="easyui-combobox" class="MySelectBoxClass" id="transferStaff">
							<option value=""></option>
							<s:iterator value="listNVGH" var="obj">
								<option value="<s:property value="#obj.staffCode" />">
									<s:property value="#obj.staffName" />
								</option>
							</s:iterator>
						</select>
					</div>
							
					<label class="LabelStyle Label1Style">NVTT</label>
					<div class="BoxSelect BoxSelect2" id="idDivCrashierStaff">
						<select class="easyui-combobox" id="crashierStaff" style="width: 175px">
							<option value=""></option>
							<s:iterator value="listNVTT" var="obj">
								<option value="<s:property value="#obj.staffCode" />">
									<s:property value="#obj.staffName" />
								</option>
							</s:iterator>
						</select>
					</div>
					
					<label class="LabelStyle Label1Style">NVBH</label>
					<div class="BoxSelect BoxSelect2" id="idDivStaffCode">
						<select class="easyui-combobox" id="staffCode" style="width: 175px">
							<option value=""></option>
							<s:iterator value="listNVBH" var="obj">
								<option value="<s:property value="#obj.staffCode" />">
									<s:property value="#obj.staffName" />
								</option>
							</s:iterator>
						</select>
					</div>
					<div class="Clear"></div>
                    
                    <div class="BtnCenterSection">
						<button id="btnSearch" class="BtnGeneralStyle" onClick="return CustomerDebitReduce.getList();" >Tìm kiếm</button>
					</div>
					<p class="ErrorMsgStyle" id="errMsgTop"	style="display: none;"></p>
				</div>
				<h2 class="Title2Style">Nhập thanh toán</h2>
				<div class="SearchInSection SProduct1Form">
					<div class="Clear"></div>
					<label class="LabelStyle Label1Style">Số chứng từ <span class="ReqiureStyle">*</span></label> 
					<input type="text" class="InputTextStyle InputText1Style" id="payreceiptCode" maxlength="20" value="<s:property value="payreceiptCode"/>" /> 
					<label class="LabelStyle Label6Style">Số tiền <span	class="ReqiureStyle">*</span></label> 
					<input type="text" class="InputTextStyle InputText1Style" id="payreceiptValue"  maxlength="20" autocomplete="off" />
					
					<label class="LabelStyle Label6Style">Tổng tiền đơn hàng</label> 
					<p class="ValueStyle Value1Style" id="cAmount" style="width: auto;">0</p>
					<div class="Clear"></div>
					<div style="display:none;">
					<label class="LabelStyle Label1Style">Công nợ trước TT</label> 
					<input type="text" class="InputTextStyle InputText1Style" disabled="disabled" id="debitPreRemain" value="<s:property value="debitPreRemain"/>" /> 
					<label class="LabelStyle Label6Style">Công nợ sau TT</label> 
					<input type="text" class="InputTextStyle InputText1Style" disabled="disabled" value="<s:property value="debitPostRemain"/>" id="debitPostRemain" />
					</div>
					<div class="Clear"></div>
				</div>
								
				<h2 class="Title2Style">Danh sách đơn hàng còn nợ</h2>
				<div class="SearchInSection SProduct1Form">
					<div class="GridSection" id="gridContainer" style="padding: 0px;">
						<table id="dg"></table>
					</div>
					<div class="BtnCenterSection">
						<button class="BtnGeneralStyle" onclick="return CustomerDebitReduce.import();" >Thanh toán</button> &nbsp;
						<!-- <button class="BtnGeneralStyle" onclick="CustomerDebitReduce.showImportExcelOnDialog();">Nhập từ File</button> -->
					</div>
					<div class="Clear"></div>	
					<p class="ErrorMsgStyle" id="errMsg" style="display: none;"></p>
					<tiles:insertTemplate template="/WEB-INF/jsp/general/successMsg.jsp" />				
				</div>
				<div class="Clear"></div>
			</div>
			<div class="Clear"></div>
		</div>
	</div>
</div>
<div id ="importExcelContent" style="width:600px;visibility: hidden;" >
	<div id="importExcelContenDialog" class="easyui-dialog" title="Nhập thanh toán từ file" data-options="closed:true,modal:true">
		<div class="PopupContentMid">
			<div class="GeneralForm Search1Form Func1Section" style="width: 700px;">
					<p class="DownloadSFileStyle DownloadSFile2Style">
						<a href="javascript:void(0)" class="Sprite1" id="downloadTemplate">Tải file excel mẫu</a>
					</p>
					<form action="/customerdebit/reduce/import-excel" name="importFrm" id="importFrm" method="post" enctype="multipart/form-data">
						<input type="hidden" name="token" id="tokenImport" value='<s:property value="token"/>'>
						<input type="file" class="InputFileStyle InputText1Style" style="width: 190px;" size="20"	name="excelFile" id="excelFile"	onchange="previewImportExcelFile(this,'importFrm');"> 
						<input type="hidden" id="isView" name="isView">
						<div class="FakeInputFile">
							<input type="text" id="fakefilepc" style="width: 190px;" class="InputTextStyle InputText1Style" />
						</div>
					</form>
					<button class="BtnGeneralStyle" id="btnView"	onclick="return CustomerDebitReduce.viewExcel();">Xem</button>
					<button id="btnImport" disabled="disabled" class="BtnGeneralStyle BtnGeneralDStyle"	onclick="return CustomerDebitReduce.importExcel();">Nhập từ Excel</button>
					<div class="Clear" style="margin-bottom: 20px;"></div>					
					<table id="dgDetail" title="Danh sách khách hàng còn nợ" ></table>

				<p class="ErrorMsgStyle" id="errExcelMsg" style="display: none;"></p>				
			</div>
		</div>
	</div>
</div>
<input type="hidden" id="shopCode" value='<s:property value="shop.shopCode"/>'/>
<input type="hidden" id="shopId" value='<s:property value="shop.id"/>'/>
<script type="text/javascript">
$(document).ready(function(){
	$('.ContentSection').unbind('keyup');
	$('.InputTextStyle').unbind('keyup');
	applyDateTimePicker("#fromDate");
	applyDateTimePicker("#toDate");
	Utils.bindComboboxStaffEasyUI('crashierStaff');
	Utils.bindComboboxStaffEasyUI('transferStaff');
	Utils.bindComboboxStaffEasyUI('staffCode');
	$('#shortCode').focus();
	CustomerDebitReduce._checkF9 = false;
	$('#shortCode').parent().bind('keyup',function(event){
		if(event.keyCode == keyCodes.ENTER){		   				
			$('#btnSearch').click();
		}
	});
	$('#shortCode').bind('keyup', function(event) {
		var arrParam = new Array();
		var param = new Object();
		param.name = 'shopCode';
		param.value = $('#shopCode').val();
		arrParam.push(param);
		if (event.keyCode == keyCode_F9) {
			CustomerDebitReduce._checkF9 = true;
			CommonSearch.searchCustomerOnDialog(function(data) {
				$('#shortCode').val(data.code);
				CustomerDebitReduce.getList();
			}, arrParam);
		}
	});
	$('#shortCode').bind('blur',function(){
		if (!CustomerDebitReduce._checkF9 && $('#shortCode').val().trim().length != 0){
			$('#debitPreRemain').val('');
			$('#payreceiptValue').val('');
			CustomerDebitReduce.getList();
		}
	});

	$('#payreceiptValue').bind('blur',function(){
		CustomerDebitReduce.changePayReceiptValue();
	});
	Utils.formatCurrencyFor('payreceiptValue');
	Utils.bindFormatOnTextfield('payreceiptValue',Utils._TF_NUMBER_COMMA);
	Utils.formatCurrencyForInterger('debitPreRemain');
	$('.CurrencyNumberNegative').each(function () {
		if($(this).html().length > 0){
			$(this).html(formatCurrencyInterger($(this).html()));	
		}		
	});
	$('#downloadTemplate').attr('href',excel_template_path + 'customer-debit/Bieu_mau_file_thanh_toan.xls');
	var options = { 
			beforeSubmit : CustomerDebitReduce.beforeImportExcel,
			success : CustomerDebitReduce.afterImportExcelUpdate,  
	 		type: "POST",
	 		dataType: 'html'   
	 	}; 
	$('#importFrm').ajaxForm(options);
	ProductLevelCatalog._callBackAfterImport = CustomerDebitReduce.loadInfo;
	CustomerDebitReduce._lstDebitSelectId = new Map();
	var isLoading = true;
	var params = new Object();
	params.shortCode = $('#shortCode').val().trim();
	params.fromDate = $('#fromDate').val().trim();
	params.toDate = $('#toDate').val().trim();
	params.type = 1;//Phiếu thu - $('#type').val().trim();
	$('#dg').datagrid({  
		 url:'/customerdebit/reduce/getinfo',		    	      
	    width: $(window).width() - 45,
	    height: 350,
	    rownumbers : true,
	    fitColumns:true,
	    checkOnSelect: false,
	    singleSelect: true,
	    selectOnCheck: false,
	    queryParams:(params),
	    columns:[[
	        {field:'debitId',checkbox:true},
			{field:'shortCode', title:'Khách hàng', width:140, align:'left', resizable: false, formatter:function(value,row,index){
				return Utils.XSSEncode(row.shortCode + ' - ' + row.customerName);
			}},  
			{field:'orderNumber', title:'Số đơn hàng/<br/>Số phiếu điều chỉnh công nợ', width:110,resizable:false , align:'left', formatter: function(value, row, index) {
		    	return Utils.XSSEncode(value);
			}},
			{field:'orderDate', title:'Ngày', width:80, align:'center',resizable:false ,formatter:CommonFormatter.dateTimeFormatter},
			{field:'nvbhCodeName', title:'NVBH', width:100,resizable:false , align:'left', formatter: function(value, row, index) {
		    	return Utils.XSSEncode(value);
			}},
			{field:'nvghCodeName', title:'NVGH', width:100,resizable:false , align:'left', formatter: function(value, row, index) {
		    	return Utils.XSSEncode(value);
			}},
			{field:'nvttCodeName', title:'NVTT', width:100,resizable:false , align:'left', formatter: function(value, row, index) {
		    	return Utils.XSSEncode(value);
			}},
			{field:'total', title:'Số tiền', width:100, align:'right',resizable:false ,formatter:function(cellValue, options, rowObject) {
				if(cellValue != undefined && cellValue != null){
					return formatCurrency(Math.abs(cellValue));
				}
				return '';
			}},
			{field:'totalPay', title:'Đã trả', width:100, align:'right',resizable:false ,
				formatter:function(val, row, index){
					if(row != undefined && row != null){
						return formatCurrency(Math.abs(row.totalPay)) + '&nbsp;<span style="color:#f00;">(' + formatCurrency(Math.abs(row.discountAmount)) + ')</span>';
					}
					return '';
				}
			},
			{field:'remain', title:'Giá trị', width:100, align:'right',resizable:false,formatter:function(cellValue, row, index){
				if(cellValue!=undefined && cellValue!=null){
					row.payAmount = cellValue;
					return '<input type="text" class="InputTextStyle remainTextBox" id="remain-' + row.debitId + '" value="' + formatCurrency(Math.abs(cellValue)) + '" idx="' + index + '" style="width:90%;text-align:right;font-size:12.4px;" maxlength="15" />';
				}
				row.payAmount = 0;
				return '<input type="text" class="InputTextStyle remainTextBox" id="remain-' + row.debitId + '" idx="' + index + '" style="width:90%;text-align:right;font-size:12.4px;" maxlength="15" />';
			}}
		]],    
		onCheck: function(rowIndex,rowData) {
			$('#errMsg').hide();
			var payreceiptValueTemp = $('#payreceiptValue').val();
			var payreceiptValue = Number(Utils.returnMoneyValue(payreceiptValueTemp));
			var heSo = 1;
			CustomerDebitReduce._remainMoney = payreceiptValue;
			var cAmount = 0;
			var rows = $('#dg').datagrid('getChecked');
			if (rows == undefined || rows == null) {
				return true;
			}
			for(var i = 0, sz = rows.length; i < sz; i++) {
				var obj = rows[i];
				cAmount += obj.payAmount;//remain;
			}
			if (cAmount < 0) {
				cAmount *= (-1);
			}
			$('#cAmount').html(formatCurrency(cAmount));
			$('#debitPostRemain').val(formatCurrency(CustomerDebitReduce._totalDebit - Math.min(payreceiptValue, cAmount) * heSo));
			return true;
		},
		onUncheck: function(rowIndex,rowData) {
			$('#errMsg').hide();
			var payreceiptValueTemp = $('#payreceiptValue').val();
			var payreceiptValue = Number(Utils.returnMoneyValue(payreceiptValueTemp));
			var heSo = 1;
			CustomerDebitReduce._remainMoney = payreceiptValue;
			var cAmount = 0;
			var rows = $('#dg').datagrid('getChecked');
			if (rows == undefined || rows == null) {
				return true;
			}
			for(var i = 0, sz = rows.length; i < sz; i++) {
				var obj = rows[i];
				cAmount += obj.payAmount;//remain;
			}
			if (cAmount < 0) {
				cAmount *= (-1);
			}
			$('#cAmount').html(formatCurrency(cAmount));
			$('#debitPostRemain').val(formatCurrency(CustomerDebitReduce._totalDebit - Math.min(payreceiptValue, cAmount) * heSo));
			
			return true;
		},
		onCheckAll: function(rows) {
			$('#errMsg').hide();
			var payreceiptValueTemp = $('#payreceiptValue').val();
			var payreceiptValue = Number(Utils.returnMoneyValue(payreceiptValueTemp));
			var heSo = 1;
			CustomerDebitReduce._remainMoney = payreceiptValue;
			var rowIndex = 0;
			var cAmount = 0;
			var payreceiptValueTemp = $('#payreceiptValue').val();
			var payreceiptValue = Number(Utils.returnMoneyValue(payreceiptValueTemp));
			CustomerDebitReduce._remainMoney = payreceiptValue;
			
			for(var sz = rows.length; rowIndex < sz; rowIndex++) {
				var rowData = rows[rowIndex];
				cAmount += rowData.payAmount;//remain;
			}
			if (cAmount < 0) {
				cAmount *= (-1);
			}
			$('#cAmount').html(formatCurrency(cAmount));
			$('#debitPostRemain').val(formatCurrency(CustomerDebitReduce._totalDebit - Math.min(payreceiptValue, cAmount) * heSo));
			return true;
		},
		onUncheckAll: function(rows) {
			var payreceiptValueTemp = $('#payreceiptValue').val();
			var payreceiptValue = Number(Utils.returnMoneyValue(payreceiptValueTemp));
			CustomerDebitReduce._remainMoney = payreceiptValue;
			$('#cAmount').html('0');
			$('#debitPostRemain').val($('#debitPreRemain').val());
			return true;
		},
		onLoadSuccess :function(data){
			$('.datagrid-header-rownumber').html('STT');  
	      	$('#dg').datagrid('resize');
	      	Utils.bindFormatOnTextfieldInputCss('remainTextBox', Utils._TF_NUMBER);
	      	$('.remainTextBox').each(function() {
	      		VTUtilJS.formatCurrencyFor($(this).attr('id'));
	      	});
	      	$('.remainTextBox').parent().css('height', '22px');
			if(data.error) {
				$('#errMsg').html(data.errMsg).show();
				return;
			}
			if(data!=null && data.debitPreRemain!=undefined && data.debitPreRemain!=null){
				$('#debitPreRemain').val(formatCurrencyInterger(data.debitPreRemain));
				$('#debitPostRemain').val(formatCurrencyInterger(data.debitPreRemain));
				CustomerDebitReduce._totalDebit = data.debitPreRemain;
			}
			if(data.rows.length == 0) {
				if(!isLoading){
					$('#errMsg').html('Khách hàng không còn đơn hàng nợ nào').show();	
				}
				
				isLoading = false;
				return;
			}
			if(data!=null && data.payreceiptCode!=null){
				$('#payreceiptCode').val(data.payreceiptCode);
			}	
			$('#dg').datagrid('uncheckAll');
			$('#cAmount').html('0');
			CustomerDebitReduce._checkF9 = false;
			$('.datagrid-header-rownumber').html('STT');
		   	$('.datagrid-header-row td div').css('text-align','center');		   	
		   	 updateRownumWidthForJqGrid('.easyui-dialog');
	   	 	$(window).resize();
		}
	});
	
	$('.remainTextBox').live('change', function() {
		var $this = $(this);
		var val = $this.val();
		val = val.replace(/,/g, '');
		var idx = Number($this.attr('idx'));
		var rows = $('#dg').datagrid('getRows');
		var r = rows[idx];
		var rem = Math.abs(r.remain);
		var pay = Number(val);
		if (pay == 0) {
			pay = rem;
			$this.val(formatCurrency(pay));
		}
		if (pay > rem) {
			pay = rem;
			$this.val(formatCurrency(pay));
			var heSo = 1;
			r.payAmount = pay * heSo;
			var rows = $('#dg').datagrid('getChecked');
			if (rows != undefined && rows != null) {
				var cAmount = 0;
				for(var i = 0, sz = rows.length; i < sz; i++) {
					var obj = rows[i];
					cAmount += obj.payAmount;
				}
				if (cAmount < 0) {
					cAmount *= (-1);
				}
				$('#cAmount').html(formatCurrency(cAmount));
			}
			$.messager.alert('Cảnh báo','Số tiền giảm nợ nhập vào không được vượt quá nợ còn lại của đơn hàng (' + formatCurrency(rem) + ')', 'warning');
		} else {
			var heSo = 1;
			r.payAmount = pay * heSo;
			var rows = $('#dg').datagrid('getChecked');
			if (rows != undefined && rows != null) {
				var cAmount = 0;
				for(var i = 0, sz = rows.length; i < sz; i++) {
					var obj = rows[i];
					cAmount += obj.payAmount;
				}
				if (cAmount < 0) {
					cAmount *= (-1);
				}
				$('#cAmount').html(formatCurrency(cAmount));
			}
		}
	});
	$('.remainTextBox').live('keyup', function(event) {
		if (event.keyCode == keyCodes.ENTER || event.keyCode == keyCodes.ARROW_DOWN) {
			var idx = Number($(this).attr('idx')) + 1;
			var inp = $('td[field=remain] input[idx=' + idx + ']');
			if (inp.length == 1) {
				inp.focus();
				$('#dg').datagrid('selectRow', idx);
			}
		} else if (event.keyCode == keyCodes.ARROW_UP) {
			var idx = Number($(this).attr('idx')) - 1;
			var inp = $('td[field=remain] input[idx=' + idx + ']');
			if (inp.length == 1) {
				inp.focus();
				$('#dg').datagrid('selectRow', idx);
			}
		}
	});
});

function _formatCurrencyFor(idInput) {
	$('#'+idInput).bind('keyup', function(e) {
		var valMoneyInput = $('#'+idInput).val();
		valMoneyInput = Utils.returnMoneyValue(valMoneyInput);
		if(isNaN(valMoneyInput) || valMoneyInput.length == 0) {
			$('#'+idInput).val("");
		} else {
			var _valMoneyInput = formatCurrency(valMoneyInput);
			$('#'+idInput).val(_valMoneyInput);
		}
	});
	$('#'+idInput).bind('paste', function(){
	    var tm = setTimeout(function(){
	    	var valMoneyInput = $('#'+idInput).val();
	        valMoneyInput = Utils.returnMoneyValue(valMoneyInput);
	        if(isNaN(valMoneyInput) || valMoneyInput.length == null) {
	        	$('#'+idInput).val("");
	        } else {
	        	var _valMoneyInput = formatCurrency(valMoneyInput);
	        	$('#'+idInput).val(_valMoneyInput);
	        }
	        clearTimeout(tm);
	    },200);
	});
}
</script>
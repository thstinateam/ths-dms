<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>

<input type="hidden" id="function_code" value="<s:property value="systimestamp"/>" />
<s:set id="downloadToken" value="%{generateReportToken()}"></s:set>
<input type="hidden" id="report_token" value="<s:property value="#downloadToken"/>" />
<s:set id="downloadToken" value="%{putSessionValue(systimestamp, #downloadToken)}"></s:set>

<div class="BreadcrumbSection">
	<ul class="ResetList FixFloat BreadcrumbList">
		<li class="Sprite1"><a href="javascript:void(0)">Công nợ khách hàng</a></li>
		<li><span>Thanh toán theo đơn hàng</span></li>
	</ul>
</div>

<div class="CtnOneColSection">
	<div class="ContentSection">
		<div class="ToolBarSection">
			<div class="SearchSection GeneralSSection">
				<h2 class="Title2Style">Thông tin tìm kiếm</h2>
				<div class="SearchInSection SProduct1Form">
					<label class="LabelStyle Label1Style"><s:text name="common.shop.name.lable"/>
                       	<span class="ReqiureStyle"><font color="red"> *</font></span>
                    </label>
					<div class="BoxSelect BoxSelect2">
						<div class="Field2">
							<input class="easyui-combobox" id="shop" style="width:206px;">
						</div>
					</div>
					<label class="LabelStyle Label1Style">Khách hàng (F9)</label>
					<input type="text" class="InputTextStyle InputText1Style" id="shortCode" maxlength="50" autocomplete="off" />
					
					<label class="LabelStyle Label1Style">Loại chứng từ<span class="ReqiureStyle">*</span></label>
					<div class="BoxSelect BoxSelect2" id="idDivType">
						<select id="type" class="MySelectBoxClass" autocomplete="off">
							<!-- <option value="-1" selected="selected">Phiếu thu/Phiếu chi</option> -->
	                       	<option value="0">Phiếu chi</option>
	                       	<option value="1" selected="selected">Phiếu thu</option>  
	                   	</select>
					</div>
					<div class="Clear"></div>
					
					<label class="LabelStyle Label1Style">Từ ngày</label>
                    <input type="text" class="InputTextStyle InputText6Style vinput-date" value="<s:property value='fromDate'/>" id="fromDate" maxlength="10" autocomplete="off"/>
                    
                    <label class="LabelStyle Label1Style">Đến ngày</label>
                    <input type="text" class="InputTextStyle InputText6Style vinput-date" value="<s:property value='toDate'/>" id="toDate" maxlength="10" autocomplete="off"/>
                    
                    <label class="LabelStyle Label1Style">Số đơn hàng</label>
					<input type="text" class="InputTextStyle InputText1Style" id="orderNumber" maxlength="250" autocomplete="off" />
					<div class="Clear"></div>
					
					<label class="LabelStyle Label1Style">NVBH</label>
					<div class="BoxSelect BoxSelect2" id="idDivStaffCode">
						<select class="easyui-combobox" id="staffCode" style="width: 206px">
						</select>
					</div>
					
					<label class="LabelStyle Label1Style">NVGH</label> 
					<div class="BoxSelect BoxSelect2" id="idDivtransferStaff">
						<select class="easyui-combobox" id="transferStaff" style="width: 206px">
						</select>
					</div>
					
					<label class="LabelStyle Label1Style">NVTT</label>
					<div class="BoxSelect BoxSelect2" id="idDivCrashierStaff">
						<select class="easyui-combobox" id="crashierStaff" style="width: 206px">
						</select>
					</div>
					
					<div class="Clear"></div>
                    
                    <div class="BtnCenterSection">
						<button id="btnSearch" class="BtnGeneralStyle cmsiscontrol" onClick="return CustomerDebitBatchNew.getDataInGrid();" >Tìm kiếm</button>
					</div>
					<p class="ErrorMsgStyle" id="errMsgTop"	style="display: none;"></p>
<!-- 					<div class="Clear"></div> -->
				</div>
				<h2 class="Title2Style">Nhập thanh toán</h2>
				<div class="SearchInSection SProduct1Form">
					<div class="Clear"></div>
					<label class="LabelStyle Label1Style">Số chứng từ <span class="ReqiureStyle">*</span></label> 
					<input type="text" class="InputTextStyle InputText1Style" id="payreceiptCode" maxlength="20" value="<s:property value="payreceiptCode"/>" /> 
					<label class="LabelStyle Label1Style">Số tiền <span	class="ReqiureStyle">*</span></label> 
					<input type="text" class="InputTextStyle InputText1Style" id="payreceiptValue"  maxlength="20" autocomplete="off" />
					<div class="Clear"></div>
					
					<label class="LabelStyle Label1Style">Họ tên người nộp tiền</label>
					<input type="text" id="payerName" class="InputTextStyle InputText1Style" maxlength="250" autocomplete="off" />
					
					<label class="LabelStyle Label1Style">Địa chỉ</label>
					<input type="text" id="payerAddress" class="InputTextStyle InputText1Style" maxlength="250" autocomplete="off" />
					
					<label class="LabelStyle Label1Style">Lý do nộp</label>
					<input type="text" id="paymentReason" class="InputTextStyle InputText1Style" maxlength="250" autocomplete="off" />
					<div class="Clear"></div>
					
					<label class="LabelStyle Label6Style">Tổng tiền thanh toán&nbsp;</label> 
					<p class="ValueStyle Value1Style" id="cAmount" style="width: auto;">0</p>
					
					<label class="LabelStyle Label6Style">Tổng tiền đơn hàng&nbsp;</label> 
					<p class="ValueStyle Value6Style" id="cAmountByGrid" style="width: auto;">0</p>
					
					<label class="LabelStyle Label6Style">Tổng tiền chiết khấu</label> 
					<p class="ValueStyle Value1Style" id="cDiscount" style="width: auto;">0</p>
					<div class="Clear"></div>
					
					<div style="display:none;">
					<label class="LabelStyle Label1Style">Công nợ trước TT</label> 
					<input type="text" class="InputTextStyle InputText1Style" disabled="disabled" id="debitPreRemain" value="<s:property value="debitPreRemain"/>" /> 
					<label class="LabelStyle Label6Style">Công nợ sau TT</label> 
					<input type="text" class="InputTextStyle InputText1Style" disabled="disabled" value="<s:property value="debitPostRemain"/>" id="debitPostRemain" />
					</div>
					<div class="Clear"></div>
				</div>
								
				<h2 class="Title2Style">Danh sách đơn hàng còn nợ
					<img style="display: none;" src="/resources/scripts/plugins/jquery-easyui-1.3.2/themes/default/images/loading.gif" id="loadingDebit">
				</h2>
				<div class="SearchInSection SProduct1Form">
					<div class="GridSection" id="gridContainer"  style="padding: 0px;">
						<table id="dg" style="overflow: scroll; height: 330px;"></table>
					</div>					
					<div class="BtnCenterSection">
						<button id="btnPay" class="BtnGeneralStyle cmsiscontrol" onclick="CustomerDebitBatchNew.import();">Thanh toán</button> &nbsp;
						<button id="btnImportExcel" class="BtnGeneralStyle cmsiscontrol" onclick="CustomerDebitBatchNew.showImportExcelOnDialog();">Nhập từ File</button>
						<button id="btnExportExcel" class="BtnGeneralStyle cmsiscontrol" onclick="CustomerDebitBatchNew.exportExcel();">Xuất File</button>
					</div>
					<div class="cmsiscontrol" style="float: left;margin-top:-29px;" id="staffCodeNVTTDiv">
						<div class="BoxSelect BoxSelect2" id="staffCodeNVTTDiv" style="margin: 3px 0px 0px 3px;">
							<select class="easyui-combobox" id="staffCodeNVTT" style="width: 175px">
							</select>
						</div>	
						<span>&nbsp;</span>
						<button class="BtnGeneralStyle" id="btnUpdate" onclick="CustomerDebitBatchNew.updateSaleOrderDebitDetail();">Thay đổi NVTT</button> &nbsp;						
					</div>
					<div class="Clear"></div>	
					<p class="ErrorMsgStyle" id="errMsg" style="display: none;"></p>
					<tiles:insertTemplate template="/WEB-INF/jsp/general/successMsg.jsp" />
				</div>
				<div class="Clear"></div>
			</div>
			<div class="Clear"></div>
		</div>
	</div>
</div>
<div id ="importExcelContent" style="width:600px;visibility: hidden;" >
	<div id="importExcelContenDialog" class="easyui-dialog" title="Nhập thanh toán từ file" data-options="closed:true,modal:true">
		<div class="PopupContentMid">
			<div class="GeneralForm Search1Form Func1Section" style="width: 450px;">
					<p class="DownloadSFileStyle DownloadSFile2Style">
						<a href="javascript:void(0)" onclick="CustomerDebitBatchNew.downloadImportTemplate();" class="Sprite1" id="downloadTemplate">Tải file excel mẫu</a>
					</p>
					<form action="/customerdebit/batch/import-excel" name="importFrm" id="importFrm" method="post" enctype="multipart/form-data">
						<input type="hidden" name="token" id="tokenImport" value='<s:property value="token"/>'>
						<input type="file" class="InputFileStyle InputText1Style" style="width: 201px !important;" size="20" name="excelFile" id="excelFile"
									onchange="previewImportExcelFile(this,'importFrm');"> 
						<input type="hidden" id="isView" name="isView">
						<div class="FakeInputFile">
							<input type="text" id="fakefilepc" style="width: 190px;" class="InputTextStyle InputText1Style" />
						</div>
					</form>
					<!-- <button class="BtnGeneralStyle" id="btnView"	onclick="return CustomerDebitBatchNew.viewExcel();">Xem</button> -->
					<button id="btnImport" class="BtnGeneralStyle" onclick="CustomerDebitBatchNew.importExcelNew();">Nhập từ Excel</button>
					<div class="Clear" style="margin-bottom: 20px;"></div>
					<!-- <table id="dgDetail" title="Danh sách khách hàng còn nợ" ></table> -->

				<p class="ErrorMsgStyle" id="errExcelMsg" style="display: none;"></p>
			</div>
		</div>
	</div>
</div>
<input type="hidden" id="shopCode"/>
<input type="hidden" id="shopId"/>
<script type="text/javascript">
$(document).ready(function() {
	//load combobox don vi
	Utils.initUnitCbx('shop', null, 206, function(rec) {
    	if (rec != null && rec.shopCode != ReportUtils._currentShopCode) {
			$('#shopId').val(rec.id);
			$('#shopCode').val(rec.shopCode);
			ReportUtils._currentShopCode = rec.shopCode;
			CustomerDebitBatchNew.changeShop(rec.shopCode);
    	}
	}, function(arr) {
       	if (arr != null && arr.length > 0) {
       		ReportUtils._currentShopCode = arr[0].shopCode;
       	}
	});
	$('.ContentSection').unbind('keyup');
	$('.InputTextStyle').unbind('keyup');
	Utils.bindComboboxStaffEasyUI('crashierStaff');
	Utils.bindComboboxStaffEasyUI('transferStaff');
	Utils.bindComboboxStaffEasyUI('staffCode');
	$('#shortCode').focus();
	$('#payreceiptValue').val('').show();
	CustomerDebitBatchNew._checkF9 = false;
	$('#shortCode').parent().bind('keyup',function(event) {
		if (event.keyCode == keyCodes.ENTER) {
			$('#btnSearch').click();
		}
	});
	$('#shortCode').bind('keyup',function(event) {
		if ($('#shopCode').val() != null&& $('#shopCode').val() != ''&& $('#shopCode').val() != undefined) {
			if (event.keyCode == keyCode_F9) {
				VCommonJS.showDialogSearch2({
                    inputs : [
             	 		{id:'code', maxlength:50, label:'Mã KH'},
               	 		{id:'name', maxlength:250, label:'Tên KH'},
                 		{id:'address', maxlength:250, label:'Địa chỉ', width:410},
                          ],
                    params : {shopId: $('#shopId').val()},
                    url : '/commons/customer-in-shop/search',
                    columns : [[
                           {field:'shortCode', title:'Mã KH', align:'left', width: 50, sortable:false, resizable:false, formatter:function(v, r, i) {
                        	   if (v) {
                        		   return Utils.XSSEncode(v);
                        	   }
                        	   return "";
                           }},
                           {field:'customerName', title:'Tên khách hàng', align:'left', width: 120, sortable:false, resizable:false, formatter:function(v, r, i) {
                        	   if (v) {
                        		   return Utils.XSSEncode(v);
                        	   }
                        	   return "";
                           }},
                           {field:'address', title:'Địa chỉ', align:'left', width: 200, sortable:false, resizable:false, formatter:function(v, r, i) {
                        	   if (v) {
                        		   return Utils.XSSEncode(v);
                        	   }
                        	   return "";
                           }},
                           {field:'choose', title:'', align:'center', width:40, sortable:false, resizable:false, formatter:function(value,row,index) {
                        	   return "<a href='javascript:void(0)' onclick='chooseCustomer(1,\""+ Utils.XSSEncode(row.shortCode) + "\");'>Chọn</a>";        
                           }}
                    ]]
              });
			}
		}
	});
	$('#shortCode').bind('blur',function(){
		if (!CustomerDebitBatchNew._checkF9 && $('#shortCode').val().trim().length != 0) {
			$('#debitPreRemain').val('');
			$('#payreceiptValue').val('');
			CustomerDebitBatchNew.getDataInGrid();
		}
	});

	$('#payreceiptValue').bind('blur',function() {
		CustomerDebitBatchNew.changePayReceiptValue();
	});
	Utils.formatCurrencyFor('payreceiptValue');
	Utils.bindFormatOnTextfield('payreceiptValue', Utils._TF_NUMBER_COMMA);
	Utils.formatCurrencyForInterger('debitPreRemain');
	//$('#downloadTemplate').attr('href',excel_template_path + 'customer-debit/Bieu_mau_file_thanh_toan.xls');
	var options = { 
			beforeSubmit : CustomerDebitBatchNew.beforeImportExcel,
			success : CustomerDebitBatchNew.afterImportExcelUpdate,  
	 		type: "POST",
	 		dataType: 'html'   
	 	}; 
	$('#importFrm').ajaxForm(options);
	ProductLevelCatalog._callBackAfterImport = CustomerDebitBatchNew.loadInfo;
	CustomerDebitBatchNew._lstDebitSelectId = new Map();
	CustomerDebitBatchNew._lstNVTT = new Array();
	CustomerDebitBatchNew._lstBank = new Array();
// 	<s:iterator value="listNVTT" var="obj">
// 		var staffCode = '<s:property value="#obj.staffCode"/>';
// 		CustomerDebitBatchNew._lstNVTT.push(staffCode);		
// 	</s:iterator>
// 	CustomerDebitBatchNew._lstBank.push("");
// 	<s:iterator value="listBank" var="obj">
// 		var code = '<s:property value="#obj.bankCode"/>';
// 		CustomerDebitBatchNew._lstBank.push(code);		
// 	</s:iterator>
	CustomerDebitBatchNew._arrayList = new Array();
	CustomerDebitBatchNew._arrayRow = new Array();
	CustomerDebitBatchNew._arrayList.push(-1);
	CustomerDebitBatchNew._mapRowIndex = new Map();
	CustomerDebitBatchNew.loadPage();

	Utils.bindComboboxStaffEasyUI('staffCodeNVTT');
	$('#staffCodeNVTT').combobox('textbox').prop('placeholder', 'Chọn NVTT để thay đổi...');
});
function chooseCustomer(type,code) {
	$('#shortCode').val(code);
	$('.easyui-dialog').dialog('close');
}
</script>
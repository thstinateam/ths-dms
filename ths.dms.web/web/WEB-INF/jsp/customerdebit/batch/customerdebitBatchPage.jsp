<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<div class="BreadcrumbSection">
	<ul class="ResetList FixFloat BreadcrumbList">
		<li class="Sprite1"><a href="javascript:void(0)">Công nợ khách hàng</a></li>
		<li><span>Thanh toán theo đơn hàng</span></li>
	</ul>
</div>
<div class="CtnOneColSection">
	<div class="ContentSection">
		<div class="ToolBarSection">
			<div class="SearchSection GeneralSSection">
				<h2 class="Title2Style">Thông tin tìm kiếm</h2>
				<div class="SearchInSection SProduct1Form">
					<label class="LabelStyle Label1Style">Khách hàng(F9)</label> 
					<input type="text" class="InputTextStyle InputText1Style" id="shortCode" maxlength="50" autocomplete="off" /> 			
					
					<label class="LabelStyle Label1Style">Loại chứng từ<span class="ReqiureStyle">*</span></label>
					<div class="BoxSelect BoxSelect2" id="idDivType">
						<select id="type" class="MySelectBoxClass" autocomplete="off">
							<!-- <option value="-1" selected="selected">Phiếu thu/Phiếu chi</option> -->
	                       	<option value="0">Phiếu chi</option>
	                       	<option value="1" selected="selected">Phiếu thu</option>  
	                   	</select>
					</div>
					<div class="Clear"></div>
					
					<label class="LabelStyle Label1Style">Từ ngày</label>
                    <input type="text" class="InputTextStyle InputText6Style vinput-date" value="<s:property value='fromDate'/>" id="fromDate" maxlength="10" autocomplete="off"/>
                    
                    <label class="LabelStyle Label1Style">Đến ngày</label>
                    <input type="text" class="InputTextStyle InputText6Style vinput-date" value="<s:property value='toDate'/>" id="toDate" maxlength="10" autocomplete="off"/>
					<div class="Clear"></div>
					
					<label class="LabelStyle Label1Style">NVGH</label> 
					<div class="BoxSelect BoxSelect2" id="idDivtransferStaff">
						<select class="easyui-combobox" class="MySelectBoxClass" id="transferStaff">
							<option value=""></option>
							<s:iterator value="listNVGH" var="obj">
								<option value="<s:property value="#obj.staffCode" />">
									<s:property value="#obj.staffName" />
								</option>
							</s:iterator>
						</select>
					</div>
					
					<label class="LabelStyle Label1Style">NVTT</label>
					<div class="BoxSelect BoxSelect2" id="idDivCrashierStaff">
						<select class="easyui-combobox" id="crashierStaff" style="width: 175px">
							<option value=""></option>
							<s:iterator value="listNVTT" var="obj">
								<option value="<s:property value="#obj.staffCode" />">
									<s:property value="#obj.staffName" />
								</option>
							</s:iterator>
						</select>
					</div>
					
					<label class="LabelStyle Label1Style">NVBH</label>
					<div class="BoxSelect BoxSelect2" id="idDivStaffCode">
						<select class="easyui-combobox" id="staffCode" style="width: 175px">
							<option value=""></option>
							<s:iterator value="listNVBH" var="obj">
								<option value="<s:property value="#obj.staffCode" />">
									<s:property value="#obj.staffName" />
								</option>
							</s:iterator>
						</select>
					</div>
					<div class="Clear"></div>
                    
                    <div class="BtnCenterSection">
						<button id="btnSearch" class="BtnGeneralStyle" onClick="return CustomerDebitBatch.getList();" >Tìm kiếm</button>
					</div>
					<p class="ErrorMsgStyle" id="errMsgTop"	style="display: none;"></p>
<!-- 					<div class="Clear"></div> -->
				</div>
				<h2 class="Title2Style">Nhập thanh toán</h2>
				<div class="SearchInSection SProduct1Form">
					<div class="Clear"></div>
					<label class="LabelStyle Label1Style">Số chứng từ <span class="ReqiureStyle">*</span></label> 
					<input type="text" class="InputTextStyle InputText1Style" id="payreceiptCode" maxlength="20" value="<s:property value="payreceiptCode"/>" /> 
					<label class="LabelStyle Label6Style">Số tiền <span	class="ReqiureStyle">*</span></label> 
					<input type="text" class="InputTextStyle InputText1Style" id="payreceiptValue"  maxlength="20" autocomplete="off" />
					
					<div style="float:left;">
						<label class="LabelStyle Label6Style">Tổng tiền thanh toán&nbsp;</label> 
						<p class="ValueStyle Value1Style" id="cAmount" style="width: auto;">0</p>
						<label class="LabelStyle Label6Style">Tổng tiền đơn hàng&nbsp;</label> 
						<p class="ValueStyle Value1Style" id="cAmountByGrid" style="width: auto;">0</p>
						<div class="Clear"></div>
						
						<label class="LabelStyle Label6Style">Tổng tiền chiết khấu</label> 
						<p class="ValueStyle Value1Style" id="cDiscount" style="width: auto;">0</p>
					</div>
					<div class="Clear"></div>
					
					<div style="display:none;">
					<label class="LabelStyle Label1Style">Công nợ trước TT</label> 
					<input type="text" class="InputTextStyle InputText1Style" disabled="disabled" id="debitPreRemain" value="<s:property value="debitPreRemain"/>" /> 
					<label class="LabelStyle Label6Style">Công nợ sau TT</label> 
					<input type="text" class="InputTextStyle InputText1Style" disabled="disabled" value="<s:property value="debitPostRemain"/>" id="debitPostRemain" />
					</div>
					<div class="Clear"></div>
				</div>
								
				<h2 class="Title2Style">Danh sách đơn hàng còn nợ</h2>
				<div class="SearchInSection SProduct1Form">
					<div class="GridSection" id="gridContainer" style="padding: 0px;">
						<table id="dg"></table>
					</div>
					<div class="BtnCenterSection">
						<button class="BtnGeneralStyle" onclick="return CustomerDebitBatch.import();" >Thanh toán</button> &nbsp;
						<button class="BtnGeneralStyle" onclick="CustomerDebitBatch.showImportExcelOnDialog();">Nhập từ File</button>
						<button class="BtnGeneralStyle" onclick="CustomerDebitBatch.exportExcel();">Xuất File</button>
					</div>
					<div class="Clear"></div>	
					<p class="ErrorMsgStyle" id="errMsg" style="display: none;"></p>
					<tiles:insertTemplate template="/WEB-INF/jsp/general/successMsg.jsp" />				
				</div>
				<div class="Clear"></div>
			</div>
			<div class="Clear"></div>
		</div>
	</div>
</div>
<div id ="importExcelContent" style="width:600px;visibility: hidden;" >
	<div id="importExcelContenDialog" class="easyui-dialog" title="Nhập thanh toán từ file" data-options="closed:true,modal:true">
		<div class="PopupContentMid">
			<div class="GeneralForm Search1Form Func1Section" style="width: 700px;">
					<p class="DownloadSFileStyle DownloadSFile2Style">
						<a href="javascript:void(0)" class="Sprite1" id="downloadTemplate">Tải file excel mẫu</a>
					</p>
					<form action="/customerdebit/batch/import-excel" name="importFrm" id="importFrm" method="post" enctype="multipart/form-data">
						<input type="hidden" name="token" id="tokenImport" value='<s:property value="token"/>'>
						<input type="file" class="InputFileStyle InputText1Style" style="width: 190px;" size="20"	name="excelFile" id="excelFile"	onchange="previewImportExcelFile(this,'importFrm');"> 
						<input type="hidden" id="isView" name="isView">
						<div class="FakeInputFile">
							<input type="text" id="fakefilepc" style="width: 190px;" class="InputTextStyle InputText1Style" />
						</div>
					</form>
					<button class="BtnGeneralStyle" id="btnView"	onclick="return CustomerDebitBatch.viewExcel();">Xem</button>
					<button id="btnImport" disabled="disabled" class="BtnGeneralStyle BtnGeneralDStyle"	onclick="return CustomerDebitBatch.importExcel();">Nhập từ Excel</button>
					<div class="Clear" style="margin-bottom: 20px;"></div>					
					<table id="dgDetail" title="Danh sách khách hàng còn nợ" ></table>

				<p class="ErrorMsgStyle" id="errExcelMsg" style="display: none;"></p>				
			</div>
		</div>
	</div>
</div>
<input type="hidden" id="shopCode" value='<s:property value="shop.shopCode"/>'/>
<input type="hidden" id="shopId" value='<s:property value="shop.id"/>'/>
<script type="text/javascript">
$(document).ready(function(){
	$('.ContentSection').unbind('keyup');
	$('.InputTextStyle').unbind('keyup');
	Utils.bindComboboxStaffEasyUI('crashierStaff');
	Utils.bindComboboxStaffEasyUI('transferStaff');
	Utils.bindComboboxStaffEasyUI('staffCode');
	$('#shortCode').focus();
	$('#payreceiptValue').val('').show();
	CustomerDebitBatch._checkF9 = false;
	$('#shortCode').parent().bind('keyup',function(event){
		if(event.keyCode == keyCodes.ENTER){		   				
			$('#btnSearch').click();
		}
	});
	/* $('#staffCode').bind('keyup', function(event){
		var arrParam = new Array();
		var param = new Object();
		param.name = 'shopCode';
		param.value = $('#shopCode').val();
		arrParam.push(param);
		if(event.keyCode == keyCode_F9){
			CustomerDebitBatch._checkF9 = true;
			CommonSearch.searchPreAndValStaffOnDialog(function(data){
				$('#staffCode').val(data.code);
				CustomerDebitBatch.getInfo(true, false);
			},arrParam);
		}
	}); */
	$('#shortCode').bind('keyup', function(event) {
		var arrParam = new Array();
		var param = new Object();
		param.name = 'shopCode';
		param.value = $('#shopCode').val();
		arrParam.push(param);
		if (event.keyCode == keyCode_F9) {
			CustomerDebitBatch._checkF9 = true;
			CommonSearch.searchCustomerOnDialog(function(data) {
				$('#shortCode').val(data.code);
				CustomerDebitBatch.getList();
			}, arrParam);
		}
	});
	$('#shortCode').bind('blur',function(){
		if (!CustomerDebitBatch._checkF9 && $('#shortCode').val().trim().length != 0){
			$('#debitPreRemain').val('');
			$('#payreceiptValue').val('');
			CustomerDebitBatch.getList();
		}
	});

	$('#payreceiptValue').bind('blur',function(){
		CustomerDebitBatch.changePayReceiptValue();
	});
	Utils.formatCurrencyFor('payreceiptValue');
	Utils.bindFormatOnTextfield('payreceiptValue',Utils._TF_NUMBER_COMMA);
	Utils.formatCurrencyForInterger('debitPreRemain');
	$('.CurrencyNumberNegative').each(function () {
		if($(this).html().length > 0){
			$(this).html(formatCurrencyInterger($(this).html()));	
		}		
	});
	$('#downloadTemplate').attr('href',excel_template_path + 'customer-debit/Bieu_mau_file_thanh_toan.xls');
	var options = { 
			beforeSubmit : CustomerDebitBatch.beforeImportExcel,
			success : CustomerDebitBatch.afterImportExcelUpdate,  
	 		type: "POST",
	 		dataType: 'html'   
	 	}; 
	$('#importFrm').ajaxForm(options);
	ProductLevelCatalog._callBackAfterImport = CustomerDebitBatch.loadInfo;
	CustomerDebitBatch._lstDebitSelectId = new Map();
	var isLoading = true;
	//CustomerDebitBatch._rowIndexValueField = -1;
	CustomerDebitBatch._lstNVTT = $('#crashierStaff').combobox('getData');
	CustomerDebitBatch._arrayList = new Array();
	CustomerDebitBatch._arrayRow = new Array();
	CustomerDebitBatch._arrayList.push(-1);
	CustomerDebitBatch._mapRowIndex = new Map();
	var params = new Object();
	params.shortCode = $('#shortCode').val().trim();
	params.fromDate = $('#fromDate').val().trim();
	params.toDate = $('#toDate').val().trim();
	params.type = $('#type').val().trim();
	$('#dg').datagrid({  
		url:'/customerdebit/batch/getinfo',		    	      
	    width: $(window).width() - 45,
	    height: 350,
	    rownumbers : true,
	    //scrollbarSize : 0,
	    fitColumns:true,
	    checkOnSelect: false,
	    //singleSelect: true,
	    selectOnCheck: false,
	    queryParams:(params),
	    columns:[[
	        {field:'debitId',checkbox:true},
	        {field:'orderNumber', title:'Số đơn hàng', width:110,resizable:false , align:'left', formatter: function(value, row, index) {
		    	return Utils.XSSEncode(value);
			}},
			{field:'shortCode', title:'Khách hàng', width:140, align:'left',resizable:false ,formatter:function(value,row,index){
				return return Utils.XSSEncode(row.shortCode + ' - ' + row.customerName);
			}},  
			{field:'orderDate', title:'Ngày', width:80, align:'center',resizable:false ,formatter:CommonFormatter.dateTimeFormatter},
			{field:'nvbhCodeName', title:'NVBH', width:100,resizable:false , align:'left', formatter: function(value, row, index) {
		    	return Utils.XSSEncode(value);
			}},
			{field:'nvghCodeName', title:'NVGH', width:100,resizable:false , align:'left', formatter: function(value, row, index) {
		    	return Utils.XSSEncode(value);
			}},
			{field:'nvttCodeName', title:'NVTT', width:130,resizable:false , align:'left',formatter:function(value,row,index){
				CustomerDebitBatch._arrayRow.push(row.nvttCodeName);
				var arrValue=[];
				arrValue.push(row.nvttCodeName);
				arrValue.push(row.nvttCodeName);
				CustomerDebitBatch._mapRowIndex.put(index, arrValue);
				return row.nvttCodeName;
			},
			editor: {
				type:'combobox',					
					options:{ 
					valueField:'staffCode',
				    textField:'staffName',
				    editable:false,
				    data: CustomerDebitBatch._lstNVTT,
				    width:200,
				    formatter: function(row) {
				    	return '<span style="font-weight:bold">' + Utils.XSSEncode(row.staffCode) + '</span><br/>' + '<span style="color:#888">' + Utils.XSSEncode(row.staffName) + '</span>';
				    },
				    onChange:function(code){
				    },
				    filter: function(q, row){
						q = new String(q).toUpperCase();
						var opts = $(this).combobox('options');
						return row[opts.textField].indexOf(q)>=0;
					}
				}
			}},
			{field:'total', title:'Số tiền', width:80, align:'right',resizable:false ,formatter:function(cellValue, options, rowObject) {
				if(cellValue != undefined && cellValue != null){
					return formatCurrency(Math.abs(cellValue));
				}
				return '';
			}},
			{field:'remain001', title:'Còn lại', width:80, align:'right',resizable:false ,formatter:function(cellValue, row, index) {
				if(row.remain != undefined && row.remain != null){
					return formatCurrency(Math.abs(row.remain));
				}
				return '';
			}},
			{field:'discount', title:'Chiết khấu', width:100, align:'right',resizable:false,formatter:function(cellValue, row, index){
				row.discount = 0;
				return '<input type="text" class="InputTextStyle discountTextBox" id="discount-' + row.debitId + '" idx="' + index + '" style="width:90%;text-align:right;font-size:12.4px;" maxlength="15" />';
			}},
			{field:'totalPay', title:'Đã trả', width:100, align:'right',resizable:false ,
				formatter:function(val, row, index){
					if(row != undefined && row != null){
						return formatCurrency(Math.abs(row.totalPay)) + '&nbsp;<span style="color:#f00;">(' + formatCurrency(Math.abs(row.discountAmount)) + ')</span>';
					}
					return '';
				}
			},
			{field:'remain', title:'Giá trị', width:100, align:'right',resizable:false,formatter:function(cellValue, row, index){
				if(cellValue!=undefined && cellValue!=null){
					row.payAmount = cellValue;
					return '<input type="text" class="InputTextStyle remainTextBox" id="remain-' + row.debitId + '" value="" idx="' + index + '" style="width:90%;text-align:right;font-size:12.4px;" maxlength="15" />';
				}
				row.payAmount = 0;
				return '<input type="text" class="InputTextStyle remainTextBox" id="remain-' + row.debitId + '" idx="' + index + '" style="width:90%;text-align:right;font-size:12.4px;" maxlength="15" />';
			}}
		]], 
		onCheck: function(rowIndex,rowData) {
			$('#dg').datagrid('selectRow', rowIndex).datagrid('beginEdit',rowIndex);
			var ed1 = $('#dg').datagrid('getEditor', {index:rowIndex,field:'nvttCodeName'});
			$(ed1.target).combobox('loadData',CustomerDebitBatch._lstNVTT);
			if(parseInt(CustomerDebitBatch._arrayList[CustomerDebitBatch._arrayList.length - 1]) > -1){
				var indexRow = CustomerDebitBatch._arrayList[CustomerDebitBatch._arrayList.length - 1];
				var cbbIndex = $('#dg').datagrid('getEditor', {index:indexRow, field:'nvttCodeName'});
				if(cbbIndex != undefined && cbbIndex!=null){
					$(ed1.target).combobox('setValue', $(cbbIndex.target).combobox('getValue'));
				}
			}else{
				var arrValue = CustomerDebitBatch._mapRowIndex.get(rowIndex);
				if(arrValue!=undefined && arrValue!=null && arrValue.length>0
						&& arrValue[0] != undefined && arrValue[0]!=null
						&& arrValue[0].split('-')[0] != undefined && arrValue[0].split('-')[0]!=null){
					var value = arrValue[0].split('-')[0].trim();
					var cbbIndex = $('#dg').datagrid('getEditor', {index:indexRow, field:'nvttCodeName'});
					$(ed1.target).combobox('setValue', value);
				}
			}
			
			var flag = false;
			for(var i=0; i<CustomerDebitBatch._arrayList.length; i++){
				if(rowIndex == CustomerDebitBatch._arrayList[i]){
					flag = true;
					break;
				}
			}
			if(!flag){
				CustomerDebitBatch._arrayList.push(rowIndex);
			}
			$('#errMsg').hide();
			var payreceiptValueTemp = $('#payreceiptValue').val();
			var payreceiptValue = Number(Utils.returnMoneyValue(payreceiptValueTemp));
			var heSo = 1;
			if($('#type').val()== 0){
				heSo = -1;
			}
			CustomerDebitBatch._remainMoney = payreceiptValue;
			var cAmount = 0;
			var cDiscount = 0;
			var rows = $('#dg').datagrid('getRows');
			var rowCbx = $('[name=debitId]');//$('#dg').datagrid('getChecked');
			$('#remain-'+rowCbx[rowIndex].value).val(formatCurrency(Math.abs(rows[rowIndex].remain)));
			
			if (rows == undefined || rows == null) {
				return true;
			}
			for(var i = 0, sz = rows.length; i < sz; i++) {
				if(rowCbx[i].checked){
					var obj = rows[i];
					cAmount += obj.payAmount;//remain;
					cDiscount += obj.discount;
				}
			}
			if (cAmount < 0) {
				cAmount *= (-1);
			}
			$('#cAmount').html(formatCurrency(cAmount));
			$('#cDiscount').html(formatCurrency(cDiscount));
			$('#debitPostRemain').val(formatCurrency(CustomerDebitBatch._totalDebit - Math.min(payreceiptValue, cAmount) * heSo));
			return true;
		},
		onUncheck: function(rowIndex,rowData) {
			/*var cbbSelect = $('#dg').datagrid('getEditors', rowIndex)[0];
			$(cbbSelect.target).combobox('setValue', '');*/
			var kq = CustomerDebitBatch._mapRowIndex.get(rowIndex);
			if(parseInt(CustomerDebitBatch._arrayList[CustomerDebitBatch._arrayList.length - 1]) > -1){
				var cbbUncheck = $('#dg').datagrid('getEditor', {index:rowIndex, field:'nvttCodeName'});
				$(cbbUncheck.target).combobox('setValue', kq[0]);
			}
			CustomerDebitBatch._arrayList = jQuery.grep(CustomerDebitBatch._arrayList, function( a ) {
				return a !== rowIndex;
			});
			$('#dg').datagrid('selectRow', rowIndex).datagrid('endEdit',rowIndex);
			$('#errMsg').hide();
			var payreceiptValueTemp = $('#payreceiptValue').val();
			var payreceiptValue = Number(Utils.returnMoneyValue(payreceiptValueTemp));
			var heSo = 1;
			if($('#type').val()== 0){
				heSo = -1;
			}
			CustomerDebitBatch._remainMoney = payreceiptValue;
			var cAmount = 0;
			var cDiscount = 0;
			var rows = $('#dg').datagrid('getRows');
			var rowCbx = $('[name=debitId]');//$('#dg').datagrid('getChecked');
			$('#remain-'+rowCbx[rowIndex].value).val('');
			if (rows == undefined || rows == null) {
				return true;
			}
			for(var i = 0, sz = rows.length; i < sz; i++) {
				if(rowCbx[i].checked){
					var obj = rows[i];
					cAmount += obj.payAmount;//remain;
					cDiscount += obj.discount;
				}
			}
			if (cAmount < 0) {
				cAmount *= (-1);
			}
			$('#cAmount').html(formatCurrency(cAmount));
			$('#cDiscount').html(formatCurrency(cDiscount));
			$('#debitPostRemain').val(formatCurrency(CustomerDebitBatch._totalDebit - Math.min(payreceiptValue, cAmount) * heSo));
			$('#dg').datagrid('unselectRow', rowIndex);
			return true;
		},
		onCheckAll: function(rows) {
			$('#errMsg').hide();
			var payreceiptValueTemp = $('#payreceiptValue').val();
			var payreceiptValue = Number(Utils.returnMoneyValue(payreceiptValueTemp));
			var heSo = 1;
			if($('#type').val()== 0){
				heSo = -1;
			}
			CustomerDebitBatch._remainMoney = payreceiptValue;
			var rowIndex = 0;
			var cAmount = 0;
			var cDiscount = 0;
			var payreceiptValueTemp = $('#payreceiptValue').val();
			var payreceiptValue = Number(Utils.returnMoneyValue(payreceiptValueTemp));
			CustomerDebitBatch._remainMoney = payreceiptValue;
			
			for(var sz = rows.length; rowIndex < sz; rowIndex++) {
				$('#remain-'+rows[rowIndex].debitId).val(formatCurrency(Math.abs(rows[rowIndex].remain)));
				var rowData = rows[rowIndex];
				cAmount += rowData.payAmount;//remain;
				cDiscount += rowData.discount;
			}
			if (cAmount < 0) {
				cAmount *= (-1);
			}
			$('#cAmount').html(formatCurrency(cAmount));
			$('#cDiscount').html(formatCurrency(cDiscount));
			$('#debitPostRemain').val(formatCurrency(CustomerDebitBatch._totalDebit - Math.min(payreceiptValue, cAmount) * heSo));
			$('#dg').datagrid('selectAll');
			return true;
		},
		onUncheckAll: function(rows) {
			var mapKey = CustomerDebitBatch._mapRowIndex.keyArray;
			for(var i=0; i< mapKey.length; i++){
				$('#dg').datagrid('selectRow', mapKey[i]).datagrid('endEdit', mapKey[i]);
				var cbbUncheck = $('#dg').datagrid('getEditors', mapKey[i])[0];
				if(cbbUncheck!=undefined && cbbUncheck!=null){
					$(cbbUncheck.target).combobox('setValue', CustomerDebitBatch._mapRowIndex.get(mapKey[i])[0]);
					$('#dg').datagrid('selectRow', mapKey[i]).datagrid('endEdit', mapKey[i]);
					CustomerDebitBatch._arrayList = jQuery.grep(CustomerDebitBatch._arrayList, function( a ) {
						return a !== i;
					});
				}
			}
			var rowIndex = 0;
			for(var sz = rows.length; rowIndex < sz; rowIndex++) {
				$('#remain-'+rows[rowIndex].debitId).val('');
			}
			var payreceiptValueTemp = $('#payreceiptValue').val();
			var payreceiptValue = Number(Utils.returnMoneyValue(payreceiptValueTemp));
			CustomerDebitBatch._remainMoney = payreceiptValue;
			$('#cAmount').html('0');
			$('#cDiscount').html('0');
			$('#debitPostRemain').val($('#debitPreRemain').val());
			$('#dg').datagrid('unselectAll');
			return true;
		},
		onClickRow:function(rowIndex, rowData) {
// 			HungLM - Change Row
			var rowCheck = $('[name=debitId]');//$('#dg').datagrid('getChecked');
			var rows = $('#dg').datagrid('getRows');
			if (rows != undefined && rows != null && rowCheck[rowIndex]!=undefined && rowCheck[rowIndex]!=null && rowCheck[rowIndex].checked) {
				var ed = $('#dg').datagrid('getEditor', {index:rowIndex, field:'nvttCodeName'});
				var kq = CustomerDebitBatch._mapRowIndex.get(rowIndex);
				if(ed!=undefined && ed!=null){
					$(ed.target).combobox('loadData',CustomerDebitBatch._lstNVTT);
					if(parseInt(CustomerDebitBatch._arrayList[CustomerDebitBatch._arrayList.length - 1]) > -1){
						var indexRow = parseInt(CustomerDebitBatch._arrayList[CustomerDebitBatch._arrayList.length - 1]);
						var cbbIndex = $('#dg').datagrid('getEditor', {index:indexRow, field:'nvttCodeName'});
						$(ed.target).combobox('setValue', $(cbbIndex.target).combobox('getValue'));
					}else{
						$(ed.target).combobox('setValue',kq[1]);
					}
					
				}else{
					$('#dg').datagrid('selectRow', rowIndex).datagrid('beginEdit',rowIndex);
					var ed1 = $('#dg').datagrid('getEditor', {index:rowIndex,field:'nvttCodeName'});
					$(ed1.target).combobox('loadData',CustomerDebitBatch._lstNVTT);
					if(parseInt(CustomerDebitBatch._arrayList[CustomerDebitBatch._arrayList.length - 1]) > -1){
						var indexRow = parseInt(CustomerDebitBatch._arrayList[CustomerDebitBatch._arrayList.length - 1]);
						var cbbIndex = $('#dg').datagrid('getEditor', {index:indexRow, field:'nvttCodeName'});
						if(cbbIndex!=undefined && cbbIndex!=null){
							$(ed1.target).combobox('setValue', $(cbbIndex.target).combobox('getValue'));
						}
					}
				}
				$('#dg').datagrid('selectRow', rowIndex);
			}else{
				var rowCbx = $('[name=debitId]')[rowIndex];
				if(rowCbx!=undefined && rowCbx!=null && !rowCbx.checked){
					$('#dg').datagrid('unselectRow', rowIndex);
				}
			}
			return true;
		},
		onLoadSuccess :function(data){
			$('.datagrid-header-rownumber').html('STT');  
	      	$('#dg').datagrid('resize');
	      	Utils.bindFormatOnTextfieldInputCss('remainTextBox', Utils._TF_NUMBER);
	      	Utils.bindFormatOnTextfieldInputCss('discountTextBox', Utils._TF_NUMBER);
	      	$('.remainTextBox').each(function() {
	      		VTUtilJS.formatCurrencyFor($(this).attr('id'));
	      	});
	      	$('.discountTextBox').each(function() {
	      		VTUtilJS.formatCurrencyFor($(this).attr('id'));
	      	});
	      	$('.remainTextBox').parent().css('height', '22px');
	      	$('.discountTextBox').parent().css('height', '22px');
			if(data.error) {
				$('#errMsg').html(data.errMsg).show();
				return;
			}
			if(data!=null && data.debitPreRemain!=undefined && data.debitPreRemain!=null){
				$('#debitPreRemain').val(formatCurrencyInterger(data.debitPreRemain));
				$('#debitPostRemain').val(formatCurrencyInterger(data.debitPreRemain));
				CustomerDebitBatch._totalDebit = data.debitPreRemain;
			}
			if(data.rows.length == 0) {
				if(!isLoading && $('#type').val() == 0) {//phieu chi
					$('#errMsg').html('Khách hàng không còn đơn hàng trả nào chưa thanh toán').show();
				} else if(!isLoading && $('#type').val() == 1) {//phieu thu
					$('#errMsg').html('Khách hàng không còn đơn hàng nợ nào').show();
				}
				isLoading = false;
				return;
			}
			if(data!=null && data.payreceiptCode!=null){
				$('#payreceiptCode').val(data.payreceiptCode);
			}	
			$('#dg').datagrid('uncheckAll');
			$('#cAmount').html('0');
			var rowIndex = 0;
			var cAmount = 0;
			for(var sz = data.rows.length; rowIndex < sz; rowIndex++) {
				var rowData = data.rows[rowIndex];
				cAmount += rowData.payAmount;//remain;
			}
			if (cAmount < 0) {
				cAmount *= (-1);
			}
			$('#cAmountByGrid').html(formatCurrency(cAmount));
			CustomerDebitBatch._checkF9 = false;
			$('.datagrid-header-rownumber').html('STT');
		   	$('.datagrid-header-row td div').css('text-align','center');		   	
		   	updateRownumWidthForJqGrid('.easyui-dialog');
		   	$('#dg').datagrid('unselectAll');
		   	$(window).resize();
	   	 	
		}
	});
	$('.remainTextBox').live('change', function() {
		var $this = $(this);
		var val = $this.val();
		val = val.replace(/,/g, '');
		var idx = Number($this.attr('idx'));
		var rows = $('#dg').datagrid('getRows');
		var r = rows[idx];
		var rem = Math.abs(r.remain);
		var pay = Number(val);
		var dis = $('#' + $this.attr('id').replace('remain-', 'discount-')).val();
		dis = dis.replace(/,/g, '');
		var discount = Number(dis);
		if (pay == 0) {
			pay = rem - discount;
			$this.val(formatCurrency(pay));
		}
		if (pay > rem - discount) {
			pay = rem - discount;
			$this.val(formatCurrency(pay));
			
			var heSo = 1;
			if($('#type').val()== 0){
				heSo = -1;
			}
			
			r.payAmount = pay * heSo;
			
			var rows = $('#dg').datagrid('getChecked');
			if (rows != undefined && rows != null) {
				var cAmount = 0;
				var cDiscount = 0;
				for(var i = 0, sz = rows.length; i < sz; i++) {
					var obj = rows[i];
					cAmount += obj.payAmount;
					cDiscount += obj.discount;
				}
				if (cAmount < 0) {
					cAmount *= (-1);
				}
				$('#cAmount').html(formatCurrency(cAmount));
				$('#cDiscount').html(formatCurrency(cDiscount));
			}
			$.messager.alert('Cảnh báo','Số tiền thanh toán nhập vào không được vượt quá nợ còn lại của đơn hàng sau chiết khấu (' + formatCurrency(rem - discount) + ')', 'warning');
		} else {
			var heSo = 1;
			if($('#type').val()== 0){
				heSo = -1;
			}
			r.payAmount = pay * heSo;
			
			var rows = $('#dg').datagrid('getChecked');
			if (rows != undefined && rows != null) {
				var cAmount = 0;
				var cDiscount = 0;
				for(var i = 0, sz = rows.length; i < sz; i++) {
					var obj = rows[i];
					cAmount += obj.payAmount;
					cDiscount += obj.discount;
				}
				if (cAmount < 0) {
					cAmount *= (-1);
				}
				$('#cAmount').html(formatCurrency(cAmount));
				$('#cDiscount').html(formatCurrency(cDiscount));
			}
		}
	});
	$('.remainTextBox').live('keyup', function(event) {
		if (event.keyCode == keyCodes.ENTER || event.keyCode == keyCodes.ARROW_DOWN) {
			var idx = Number($(this).attr('idx')) + 1;
			var inp = $('td[field=remain] input[idx=' + idx + ']');
			if (inp.length == 1) {
				inp.focus();
				$('#dg').datagrid('selectRow', idx);
			}
		} else if (event.keyCode == keyCodes.ARROW_UP) {
			var idx = Number($(this).attr('idx')) - 1;
			var inp = $('td[field=remain] input[idx=' + idx + ']');
			if (inp.length == 1) {
				inp.focus();
				$('#dg').datagrid('selectRow', idx);
			}
		}
	});
	$('.discountTextBox').live('change', function() {
		var $this = $(this);
		var val = $this.val();
		val = val.replace(/,/g, '');
		var idx = Number($this.attr('idx'));
		var rows = $('#dg').datagrid('getRows');
		var r = rows[idx];
		var rem = Math.abs(r.remain);
		var pay = rem - Number(val);
		if (pay < 0) {
			pay = 0;
			$this.val(formatCurrency(rem));
			$('#' + $this.attr('id').replace('discount-', 'remain-')).val(formatCurrency(pay));
			
			var heSo = 1;
			if($('#type').val()== 0){
				heSo = -1;
			}
			r.payAmount = pay * heSo;
			///r.discount = Number(val);
			r.discount = Number(rem);			
			
			var rows = $('#dg').datagrid('getChecked');
			if (rows != undefined && rows != null) {
				var cAmount = 0;
				var cDiscount = 0;
				for(var i = 0, sz = rows.length; i < sz; i++) {
					var obj = rows[i];
					cAmount += obj.payAmount;
					cDiscount += obj.discount;
				}
				if (cAmount < 0) {
					cAmount *= (-1);
				}
				$('#cAmount').html(formatCurrency(cAmount));
				$('#cDiscount').html(formatCurrency(cDiscount));
			}
			$.messager.alert('Cảnh báo','Chiết khấu không được vượt quá nợ còn lại của đơn hàng (' + formatCurrency(rem) + ')', 'warning');
		} else {
			$('#' + $this.attr('id').replace('discount-', 'remain-')).val(formatCurrency(pay));
			var heSo = 1;
			if($('#type').val()== 0){
				heSo = -1;
			}
			r.payAmount = pay * heSo;
			r.discount = Number(val);
			
			var rows = $('#dg').datagrid('getChecked');
			if (rows != undefined && rows != null) {
				var cAmount = 0;
				var cDiscount = 0;
				for(var i = 0, sz = rows.length; i < sz; i++) {
					var obj = rows[i];
					cAmount += obj.payAmount;
					cDiscount += obj.discount;
				}
				if (cAmount < 0) {
					cAmount *= (-1);
				}
				$('#cAmount').html(formatCurrency(cAmount));
				$('#cDiscount').html(formatCurrency(cDiscount));
			}
		}
	});
	$('.discountTextBox').live('keyup', function(event) {
		if (event.keyCode == keyCodes.ENTER || event.keyCode == keyCodes.ARROW_DOWN) {
			var idx = Number($(this).attr('idx')) + 1;
			var inp = $('td[field=discount] input[idx=' + idx + ']');
			if (inp.length == 1) {
				inp.focus();
				$('#dg').datagrid('selectRow', idx);
			}
		} else if (event.keyCode == keyCodes.ARROW_UP) {
			var idx = Number($(this).attr('idx')) - 1;
			var inp = $('td[field=discount] input[idx=' + idx + ']');
			if (inp.length == 1) {
				inp.focus();
				$('#dg').datagrid('selectRow', idx);
			}
		}
	});
});

function _formatCurrencyFor(idInput) {
	$('#'+idInput).bind('keyup', function(e) {
		var valMoneyInput = $('#'+idInput).val();
		valMoneyInput = Utils.returnMoneyValue(valMoneyInput);
		if(isNaN(valMoneyInput) || valMoneyInput.length == 0) {
			$('#'+idInput).val("");
		} else {
			var _valMoneyInput = formatCurrency(valMoneyInput);
			$('#'+idInput).val(_valMoneyInput);
		}
	});
	$('#'+idInput).bind('paste', function(){
	    var tm = setTimeout(function(){
	    	var valMoneyInput = $('#'+idInput).val();
	        valMoneyInput = Utils.returnMoneyValue(valMoneyInput);
	        if(isNaN(valMoneyInput) || valMoneyInput.length == null) {
	        	$('#'+idInput).val("");
	        } else {
	        	var _valMoneyInput = formatCurrency(valMoneyInput);
	        	$('#'+idInput).val(_valMoneyInput);
	        }
	        clearTimeout(tm);
	    },200);
	});
}
</script>
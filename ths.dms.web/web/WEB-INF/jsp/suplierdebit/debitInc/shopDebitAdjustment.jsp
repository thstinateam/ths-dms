<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>

<div class="BreadcrumbSection">
	<ul class="ResetList FixFloat BreadcrumbList">
		<li class="Sprite1"><a href="javascript:void(0);" class="cmsdefault">Công nợ NPP</a></li>
		<li><span>Lập phiếu điều chỉnh công nợ</span></li>
	</ul>
</div>

<div class="CtnOneColSection">
	<div class="ContentSection">
		<div class="ToolBarSection">
			<div class="SearchSection GeneralSSection">				
				<h2 class="Title2Style">Thông tin phiếu</h2>
				<div class="SearchInSection SProduct1Form">
					<label class="LabelStyle Label1Style"><s:text name="common.shop.name.lable"/>
                       	<span class="ReqiureStyle"><font color="red"> *</font></span>
                    </label>
					<div class="BoxSelect BoxSelect2">
						<div class="Field2">
							<input id="shop" style="width:200px;">
						</div>
					</div>
					<div class="Clear"></div>
					<div class="GridSection" id="gridContainer" style="padding: 0px;max-height:450px;overflow-x:hidden;overflow-y:scroll;">
						<table id="dgrid"></table>
					</div>
					<div class="Clear"></div>
                    
                    <div class="BtnCenterSection">
						<button id="btnSave" class="BtnGeneralStyle cmsiscontrol" onClick="DebitAdjustment.saveShopDebit();">Lưu</button>
					</div>
					
					<p id="errMsg" class="ErrorMsgStyle" style="display:none;"></p>
					<p id="successMsg" class="SuccessMsgStyle" style="display:none;"></p>
				</div>
			</div>
		</div>
	</div>
</div>

<input type="hidden" id="shopCode" value="<s:property value='shopCode' />" />

<script type="text/javascript">
$(document).ready(function() {
	
	Utils.initUnitCbx('shop', null, 206, function(rec) {
    	if (rec != null && rec.shopCode != ReportUtils._currentShopCode) {
			$('#shopCode').val(rec.shopCode);
			ReportUtils._currentShopCode = rec.shopCode;
			initGrid();
    	}
	}, function(arr) {
       	if (arr != null && arr.length > 0) {
       		ReportUtils._currentShopCode = arr[0].shopCode;
       	}
	});
	
	function initGrid(){
		$('.ContentSection').unbind('keyup');
		$('.InputTextStyle').unbind('keyup');
		
		var w = $("#gridContainer").width() - 60;
		var lst1 = [];
		for (var i = 0; i < 30; i++) {
			lst1.push(new ShopDebitAdjustmentObj());
		}
		$('#dgrid').handsontable({
			data: lst1,
			colWidths: [w/6, 1.5 * w/6, 1.5 * w/6, w/3-27],
			outsideClickDeselects: true,
			multiSelect : false,
			rowHeaders: true,
			manualColumnResize: true,
			fixedRowsTop: 0,
			width: w+70,
			colHeaders: DebitAdjustment._colShopNames,
			columns: [
				{data: "debitType", allowInvalid: false, type: "dropdown", source: DebitAdjustment._lstDebitTypes},
			    {data: "dbNumber"},
			    {data: "amount", type: "numeric", format: "#,##0"/*, allowInvalid: false */},
			    {data: "reason"}
			],
			beforeKeyDown: function(event){
				$(".ErrorMsgStyle").hide();
				var sl = $('#dgrid').handsontable('getSelected');
				var r = sl[0];
				var c = sl[1];
				if (c ==  1) {
					$(".handsontableInputHolder:not(:hidden) .handsontableInput").attr("maxlength", 40);
				} else if (c == 2) {
					$(".handsontableInputHolder:not(:hidden) .handsontableInput").attr("maxlength", 15);
				} else if (c == 3) {
					$(".handsontableInputHolder:not(:hidden) .handsontableInput").attr("maxlength", 250);
				} else {
					$(".handsontableInputHolder:not(:hidden) .handsontableInput").attr("maxlength", 15);
				}
				var keyCode = event.keyCode;
				if (keyCode == keyCodes.TAB && !event.shiftKey) {
					var htGrid = $('#dgrid').handsontable('getInstance');
					var sl = htGrid.getSelected();
					var r = sl[0];
					var c = sl[1];
					var data = htGrid.getData();
					if (c == 3) {
						event.stopImmediatePropagation();
						event.preventDefault();
						if (r == data.length - 1) {
							for (var i = 0; i < 30; i++) {
								data.push(new ShopDebitAdjustmentObj());
							}
							htGrid.render();
						}
						htGrid.selectCell(r + 1, 0);
					}
				}
			},
			afterChange: function(d, s) {
				if (d != null) {
					var sl = d[0];
					if (sl[1] == "amount") {
						var r = sl[0];
						var v = sl[3];
						var htGrid = $('#dgrid').handsontable('getInstance');
						//var meta = htGrid.getCellMeta(r, 4);
						//meta.readOnly = false;
						if (v && (isNaN(v) || Number(v) <= 0 || Math.floor(Number(v)) - Number(v) != 0)) {
							if (Math.floor(Number(v)) - Number(v) != 0) {
								htGrid.setDataAtCell(r,2,null);
							}
							htGrid.selectCell(r, 2);
							$("#errMsg").html("Số tiền phải là số nguyên dương").show();
							htGrid.render();
						} else if (v && (v.toString().length > 15 || Number(v) > 999999999999999)) {
							//htGrid.setDataAtCell(r,2,null);
							htGrid.selectCell(r, 2);
							$("#errMsg").html("Số tiền chỉ được nhập tối đa 15 chữ số").show();
							htGrid.render();
						}
					}
				}
			}
		});
		$('#dgrid').handsontable('selectCell', 0, 0);
		
		$("#dgrid thead th").css({"background-color":"#56ABC7", "color":"#fff", "font-weight":"bold"});
	}
});
</script>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<div class="BreadcrumbSection">
       	<ul class="ResetList FixFloat BreadcrumbList">
           <li class="Sprite1"><a href="/adjusted-revenua/adjustRevenuaInfo">Điều chỉnh doanh thu</a></li>
           <li><span><span><s:property value="title" /></span></li>
    	</ul>
</div>

<div class="CtnOneColSection">
	<div class="ContentSection">
		<div class="ToolBarSection">
			<div class="SearchSection GeneralSSection">
				<h2 class="Title2Style">Thông tin tìm kiếm</h2>
				<div class="SearchInSection SProduct1Form">
					<label class="LabelStyle Label1Style">Đơn vị</label>
					<div class="BoxSelect BoxSelect2" id="ddlShopCodeDiv">
						<select class="easyui-combobox" id="ddlShopId">
							<s:iterator value="lstShopVO"  var="objs" >
								<option value="<s:property value="#objs.shopId" />" code="<s:property value="#objs.shopCode" />" name="<s:property value="#objs.shopName" />"></option>
							</s:iterator>
						</select>                            
					</div>
					<div class="Clear"></div>
					<label class="LabelStyle Label1Style">Số phiếu</label> 
					<input id="txtOderNumber" type="text" readonly="readonly" saleorderid="<s:property value="saleOrder.id" />" disabled="disabled" class="InputTextStyle InputText1Style" value="<s:property value="saleOrder.orderNumber" />" maxlength="50" /> 
					<label class="LabelStyle Label1Style">Số đơn gốc (F9)</label> 
					<input id="txtIsJoinOderNumber" type="text" class="InputTextStyle InputText1Style" value="<s:property value="saleOrder.fromSaleOrder.orderNumber" />" maxlength="100" /> 
					<label class="LabelStyle Label1Style">Loại điều chỉnh</label> 
					<div class="BoxSelect BoxSelect2" id="dllOrderTypeDiv">
						<select class="MySelectBoxClass" id="dllOrderType">
							<option value="AI">Tăng doanh thu</option>
							<option value="AD">Giảm doanh thu</option>
						</select>                            
					</div>
					<div class="Clear"></div>
					<label class="LabelStyle Label1Style">Ngày điều chỉnh</label> 
					<input id="txtLockDate" type="text" disabled="disabled" class="InputTextStyle InputText1Style" value="<s:property value="lockDate" />" />
					<label class="LabelStyle Label1Style">Lý do</label> 
					<input id="txtDescription" type="text" class="InputTextStyle InputText1Style" maxlength="190" value="<s:property value="saleOrder.description" />" /> 
<%-- 					<s:if test="saleOrder.id!=null && saleOrder.id>0"> --%>
<!-- 						<label id="lblStatusText" class="LabelStyle Label1Style">Trạng thái</label>  -->
<!-- 						<div class="BoxSelect BoxSelect2" id="dllApprovedDiv"> -->
<%-- 							<select class="MySelectBoxClass" id="dllApproved"> --%>
<!-- 								<option value="1">Đã duyệt</option> -->
<!-- 								<option value="0" selected="selected">Chưa duyệt</option> -->
<%-- 							</select>                             --%>
<!-- 						</div> -->
<%--        				</s:if> --%>
					<div class="Clear"></div>
				</div>
			</div>
			<div class="Clear"></div>
		</div>
		<!-- Chi tiet don hang goc -->
		<div class="GeneralCntSection" id="adjustedRevenueDetailDiv">
			<h2 class="Title2Style">Danh sách sản phẩm: <span id="adjustedRevenueDetailSpan" style="color:#199700"></span></h2>
			<div class="GridSection" id="adjRevenueContDetailGrid"><table id="adjustedRevenueDetailDg"></table></div>
			<div class="GeneralForm GeneralNoTP1Form" style="width:auto;">
                <div class="Func1Section" style="width:auto; min-width: 150px;">
                	<label class="LabelStyle Label1Style" style="text-align: left; width: 55px; padding-top: 2px; float: left;">Tổng tiền</label>
                	<label class="LabelStyle Label1Style" id="lblSumAmountDg" style="float: left; font-weight: bold; text-align: left; color: red; width: 120px;"></label> 
                </div>
                <div class="Clear"></div>
            </div>
		</div>
		<div class="SearchSection GeneralSSection SearchInSection SProduct1Form" style="background: none; border: none;">
			<div class="BtnCenterSection">
				<button id="btnChangeSaleOderAIAD" class="BtnGeneralStyle" onclick="return AdjustedRevenueManage.createOrUpdateSaleOrderAIAD();">Lưu</button>
			</div>
			<p id="errMsgChange" style="display: none;" class="ErrorMsgStyle"></p>
			<p id="successMsgChange" class="SuccessMsgStyle" style="display:none;"></p>
			<div class="Clear" style="height: 20px;"></div>
		</div>
		
	</div>
</div>
<div class="ContentSection" style="visibility: hidden;" id="dialogSearchSaleOrderDiv">
	<tiles:insertTemplate template="/WEB-INF/jsp/suplierdebit/adjusted-revenue/dialogSearchSaleOrder.jsp" />
</div>
<div class="ContentSection" style="visibility: hidden;">
	<tiles:insertTemplate template="/WEB-INF/jsp/suplierdebit/adjusted-revenue/dialogSearchProductByIsJoinSOManager.jsp" />
</div>
<script type="text/javascript">
	$(document).ready(function() {
		AdjustedRevenueManage._lockDate = '<s:property value="lockDate" />';
		var orderType = '<s:property value="saleOrder.orderType" />';
		if(orderType!=undefined && orderType!=null && orderType.trim().length > 0){
			selectedDropdowlist('dllOrderType', orderType.trim());
		}
		
		var txtOrderNumber = $('#txtOderNumber').val().trim();
		if(txtOrderNumber.length == 0){
			$('#txtIsJoinOderNumber').prop("disabled", false);
			enableSelectbox('dllOrderType');
			//$('#dllApprovedDiv').show();
			$('#lblStatusText').show();
		}else{
			$('#txtIsJoinOderNumber, ').prop("disabled", true);
			disableSelectbox('dllOrderType');
		}
// 		$('#txtLockDate').width($('#txtLockDate').width()-22);
		$('#txtOderNumber').focus();
		General.bindComboboxEasyUI('ddlShopId', 250);
		$('#ddlShopCodeDiv, #txtOderNumber, #txtIsJoinOderNumber, #dllOrderTypeDiv, #txtLockDate, #txtDescription').bind('keyup',function(event){
			if(event.keyCode == keyCodes.ENTER){
				$('#btnChangeSaleOderAIAD').click();
			}
		});
		$('#txtIsJoinOderNumber').bind('keyup', function(event) {
			if (event.keyCode == keyCode_F9) {
				AdjustedRevenueManage._txtIsJoinOderNumberC = $('#txtIsJoinOderNumber').val();
				AdjustedRevenueManage.openDialogSearchSaleOrder();
			}
		});
		AdjustedRevenueManage._arrDelete = new Array();
		$('#adjustedRevenueDetailDg').datagrid({
			url: '/adjusted-revenua/search-saleorder-detail-ai-ad',
			width : $('#adjRevenueContDetailGrid').width() - 5,
			queryParams: {orderNumber: $('#txtOderNumber').val().trim()},
			fitColumns : true,
		    rownumbers : true,
		    singleSelect:true,
		    height: 275,
		    columns:[[
				{field:'productCode',title:'Mã sản phẩm(F9)',width:180,align:'left',formatter: function(value, row, index){
					return Utils.XSSEncode(value);
				}
				, editor: {
					type:'text'
				}
				},  
			    {field:'productName',title:'Tên sản phẩm',width:300,align:'left',formatter: function(value, row, index){
			    	return Utils.XSSEncode(value);
			    }},
			    {field:'price',title:'Đơn giá',width:130,align:'right',formatter: function(value, row, index){
			    	var idIp = "dgPrice"+ row.id;
			    	var html = '<input type="text" id= "'+idIp+'" indexrow="'+index+'" onchange="return AdjustedRevenueManage.onchangeTxtPriceInGridDetail(this);" style="width: 100%; text-align: right;" maxlength="18" value="'+VTUtilJS.formatCurrency(Utils.formatDoubleValue(value))+'" />';
			    	return html;
			    }},
			    {field:'quantity',title:'Số lượng',width:130,align:'right',formatter: function(value, row, index){
			    	return VTUtilJS.formatCurrency(Utils.formatDoubleValue(value));
			    }},
			    {field:'amount',title:'Thành tiền',width:130,align:'right',formatter: function(value, row, index){
			    	return VTUtilJS.formatCurrency(Utils.formatDoubleValue(value));
			    }},
			    {field:'detelte',title:'',width:40,align:'center',formatter: function(value, row, index){
			    	 return '<a href="javascript:void(0)" id="dg_AdjustedRevenueDetail_delete" onclick="return AdjustedRevenueManage.deleteSaleorderDetailAIAD('+row.id+','+row.isAttr+','+index+',\''+row.productCode+'\');"><img title="Xóa" src="/resources/images/icon_delete.png" width="16" heigh="16"></a>';
			    }},
			]],
			onClickRow:function(rowIndex, rowData) {
				var idInput = "dgPrice"+ rowData.id;
				if(rowData.id!=undefined && rowData.id!=null && rowData.id == 0 && $($('.datagrid-editable-input')).val().trim().length == 0){
					$('.datagrid-editable-input').focus();
				} else {
					$('#'+idInput).focus();
				}
			},
			onAfterEdit : function(rowIndex, rowData, changes){
				//Xu ly tinh toan tong tien
				var tong = 0;
				var rows = $('#adjustedRevenueDetailDg').datagrid('getRows');
				for(var i=0; i< rows.length; i++){
	    		 	 sumAmountRow += rows[i].amount;
		    	 }
		    	 $('#lblSumAmountDg').html(VTUtilJS.formatCurrency(Utils.formatDoubleValue(sumAmountRow)));
				
				var idInput = "dgPrice"+ rowData.id;
			},
			onBeforeEdit : function(rowIndex, rowData, changes){
				var indexLength = $('#adjustedRevenueDetailDg').datagrid('getRows').length;
				for(var i=0; i<indexLength; i++){
					$('#adjustedRevenueDetailDg').datagrid('unselectRow', i).datagrid('endEdit', i);
				}
			},
	        onLoadSuccess :function(data){
		    	 $('.datagrid-header-rownumber').html('STT');
		    	 Utils.updateRownumWidthAndHeightForDataGrid('adjustedRevenueDetailDg');
		    	 //Xu ly cho hien thi
		    	 var sumAmountRow = 0;
		    	 var arrInput = '';
		    	 
		    	 var rows = $('#adjustedRevenueDetailDg').datagrid('getRows');
		    	 if(rows==null || rows.length==0 || rows[rows.length-1].id!=null){
		    		 AdjustedRevenueManage.insertRowAdjustedRevenueDetailDg();
		    	 }
		    	 AdjustedRevenueManage._mapSaleOrderDetail = new Map();
		    	 for(var i=0; i< data.rows.length; i++){
		    		 AdjustedRevenueManage._mapSaleOrderDetail.put(data.rows[i].productCode, data.rows[i]);
	    		 	 var idInput = "dgPrice"+ data.rows[i].id;
	    		 	 arrInput += '#'+ idInput+', ';
	    		 	 sumAmountRow += data.rows[i].amount;
		    		 $('#'+idInput).each(function() {
			    	 	VTUtilJS.bindFormatOnTextfield($(this), VTUtilJS._TF_NUMBER_COMMA);
			    	 	VTUtilJS.formatCurrencyFor($(this));
					 });
		    	 }
		    	 $('#lblSumAmountDg').html(VTUtilJS.formatCurrency(Utils.formatDoubleValue(sumAmountRow)));
		    	 arrInput += '#btnChangeSaleOderAIAD';
		    	 $(''+arrInput+'').bind('keyup',function(event){
		 			 if(event.keyCode == keyCodes.ENTER){
		 			 	$('#btnChangeSaleOderAIAD').click();
		 			 }
		 		});
	    	}
		});
		
		AdjustedRevenueManage.viewDetailSaleOrderAIADChange(null);
		
	});

</script>

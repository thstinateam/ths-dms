<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<div class="BreadcrumbSection">
       	<ul class="ResetList FixFloat BreadcrumbList">
           <li class="Sprite1"><a href="javascript:void(0);">Công nợ khách hàng</a></li>
           <li><span>Danh sách phiếu điều chỉnh doanh thu</span></li>
    	</ul>
</div>

<div class="CtnOneColSection">
	<div class="ContentSection">
		<div class="ToolBarSection">
			<div class="SearchSection GeneralSSection">
				<h2 class="Title2Style">Thông tin tìm kiếm</h2>
				<div class="SearchInSection SProduct1Form">
					<label class="LabelStyle Label1Style">Đơn vị</label>
					<div class="BoxSelect BoxSelect2" id="ddlShopCodeDiv">
						<select class="easyui-combobox" id="ddlShopCode">
							<s:iterator value="lstShopVO"  var="objs" >
								<option value="<s:property value="#objs.shopCode" />" code="<s:property value="#objs.shopCode" />" name="<s:property value="#objs.shopName" />"></option>
							</s:iterator>
						</select>                            
					</div>
					<div class="Clear"></div>
					<label class="LabelStyle Label1Style">Số phiếu</label> 
					<input id="txtOderNumber" type="text" class="InputTextStyle InputText1Style" /> 
					<label class="LabelStyle Label1Style">Số đơn gốc</label> 
					<input id="txtIsJoinOderNumber" type="text" class="InputTextStyle InputText1Style" /> 
					<label class="LabelStyle Label1Style">Loại điều chỉnh</label> 
					<div class="BoxSelect BoxSelect2" id="dllOrderTypeDiv">
						<select class="MySelectBoxClass" id="dllOrderType">
							<option value="ISALL">Tất cả</option>
							<option value="AI">Tăng doanh thu</option>
							<option value="AD">Giảm doanh thu</option>
						</select>                            
					</div>
					<div class="Clear"></div>
					<label class="LabelStyle Label1Style">Từ ngày</label> 
					<input id="txtFromDate" type="text" class="InputTextStyle InputText1Style vinput-date" value="<s:property value="lockDate" />"  /> 
					<label class="LabelStyle Label1Style">Đến ngày</label> 
					<input id="txtToDate" type="text" class="InputTextStyle InputText1Style vinput-date" value="<s:property value="lockDate" />" /> 
					<label class="LabelStyle Label1Style">Trạng thái</label>
					<div class="BoxSelect BoxSelect2" id="dllApprovedDiv">
						<select class="MySelectBoxClass" id="dllApproved">
							<option value="-2" selected="selected">Tất cả</option>
							<option value="1">Đã duyệt</option>
							<option value="0">Chưa duyệt</option>
						</select>                            
					</div>
					<div class="Clear"></div>
					<div class="BtnCenterSection">
						<button id="btnSearchSaleOderAIAD" class="BtnGeneralStyle" onclick="return AdjustedRevenueManage.searchSaleOrder();">Tìm kiếm</button>
					</div>
					<div class="Clear"></div>
				</div>
			</div>
			<div class="Clear"></div>
		</div>
		<div class="GeneralCntSection">
			<p id="errMsg" style="display: none;" class="ErrorMsgStyle"></p>
			<p id="successMsg" class="SuccessMsgStyle" style="display:none;"></p>
			<div class="Clear"></div>
			<h2 class="Title2Style">Danh sách điều chỉnh</h2>
			<div class="GridSection" id="adjRevenueContGrid">
				 <table id="adjustedRevenueDg"></table>
			</div>
			<div class="Clear" style="height: 20px;"></div>
		</div>
		<!-- Chi tiet phieu dieu chinh -->
		<div class="GeneralCntSection" id="adjustedRevenueDetailDiv"></div>
	</div>
</div>
<!-- Dialog Form -->

<script type="text/javascript">
	$(document).ready(function() {
		$('#txtFromDate, #txtToDate').width($('#txtFromDate').width()-22);
// 		$('.MySelectBoxClass').customStyle();
		$('#txtOderNumber').focus();
		General.bindComboboxEasyUI('ddlShopCode', 250);
		$('#ddlShopCodeDiv, #txtOderNumber, #txtIsJoinOderNumber, #dllOrderTypeDiv, #txtFromDate, #txtToDate, #dllApproved, #btnSearchSaleOderAIAD').bind('keyup',function(event){
			if(event.keyCode == keyCodes.ENTER){
				$('#btnSearchSaleOderAIAD').click();
			}
		});

		$('#adjustedRevenueDg').datagrid({
			url: '/adjusted-revenua/search-saleorder-ai-ad',
			width : $('#adjRevenueContGrid').width() - 15,
			queryParams: {
				shopCode: $('#ddlShopCode').combobox('getValue'),
				oderNumber: $('#txtOderNumber').val(),
				orderType: $('#dllOrderType').val().trim(),
				isJoinOderNumber: $('#txtIsJoinOderNumber').val(),
				fromDateStr: $('#txtFromDate').val(),
				toDateStr: $('#txtToDate').val(),
				approved: $('#dllApproved').val()
			},
			pagination : true,
	        rownumbers : true,
	        pageNumber : 1,
	        scrollbarSize: 0,
	        autoWidth: true,
	        pageList: [20],
	        singleSelect:true,
	        autoRowHeight : true,
	        fitColumns : true,
			columns:[[
				{field:'orderNumber',title:'Số phiếu',width:120,align:'left',formatter: function(value, row, index){
					return Utils.XSSEncode(value);
				}},  
			    {field:'orderType',title:'Loại điều chỉnh',width:80,align:'left',formatter: function(value, row, index){
			    	if(value != undefined && value !=null && value.trim().toUpperCase() === "AI"){
			    		return 'Tăng doanh thu';
			    	}
			    	if(value != undefined && value !=null && value.trim().toUpperCase() === "AD"){
			    		return 'Giảm doanh thu';
			    	}
			    	return Utils.XSSEncode(value);
			    }},
			    {field:'isJoinOderNumber',title:'Số đơn gốc',width:150,align:'left',formatter: function(value, row, index){
			    	return Utils.XSSEncode(value);
			    }},
			    {field:'orderDateStr',title:'Ngày lập',width:60,align:'center',formatter: function(value, row, index){
			    	return value;
			    }},
			    {field:'amount',title:'Tổng tiền',width:120,align:'right',formatter: function(value, row, index){
			    	return VTUtilJS.formatCurrency(Utils.formatDoubleValue(value));
			    }},
			    {field:'approved',title:'Trạng thái',width:90,align:'left',formatter: function(value, row, index){
			    	return AdjustedRevenueManage.approvedReturnText(value);
			    }},
			    {field:'shopCode',title:'Đơn vị',width:90,align:'left',formatter: function(value, row, index){
			    	return Utils.XSSEncode(value);
			    }},
			    {field:'change', title:'<a href="javaScript:void(0);" id="dg_adjusted_revenue_insert" onclick="return AdjustedRevenueManage.redirectInsertOrUpdate(1);"><img title="Thêm mới" src="/resources/images/icon_add.png"/></a>', width:92, align:'left', fixed:true, formatter: function(value, row, index) {
			    	var html = '';
			    	html += '<a href="javascript:void(0)" id="dg_adjusted_revenue_view" onclick="return AdjustedRevenueManage.viewDetailSaleOrderAIAD('+row.saleOrderId+',\''+row.orderNumber+'\');"><img title="Chi tiết" src="/resources/images/icon-view.png" width="16" heigh="16" style="padding-left: 10px;"></a>';
			    	if(row.approved!=undefined && row.approved!=null && row.approved == 0){
				    	html += '<a href="javascript:void(0)" id="dg_adjusted_revenue_update" onclick="return AdjustedRevenueManage.redirectInsertOrUpdate(2,'+row.saleOrderId+');"><img title="Chỉnh sửa" src="/resources/images/icon-edit.png" width="16" heigh="16" style="padding-left: 5px;"></a>';
				    	html += '<a href="javascript:void(0)" id="dg_adjusted_revenue_delete" onclick="return AdjustedRevenueManage.deleteSaleorderAIAD('+row.saleOrderId+',\''+row.orderNumber+'\')"><img title="Xóa" src="/resources/images/icon_delete.png" width="16" heigh="16" style="padding-left: 5px;"></a>';
			    	}else{
			    		html += '<a href="javascript:void(0)" id="dg_adjusted_revenue_update" onclick=""><img title="Chỉnh sửa" src="/resources/images/icon-edit_disable.png" width="16" heigh="16" style="padding-left: 5px;"></a>';
				    	html += '<a href="javascript:void(0)" id="dg_adjusted_revenue_delete" onclick=""><img title="Xóa" src="/resources/images/icon_delete_disable.png" width="16" heigh="16" style="padding-left: 5px;"></a>';
			    	}
			    	return html;
			    }}
			]],
	        onLoadSuccess :function(data){
	        	$('.datagrid-header-rownumber').html('STT');
				Utils.updateRownumWidthAndHeightForDataGrid('adjustedRevenueDg');
				$(window).resize();
	        }
		});
		
	});

</script>

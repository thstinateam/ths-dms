<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<div id="common-dialog-search-2-textbox" class="easyui-dialog" data-options="closed:true, modal:true" style="width:670px;height:auto;">
	<div class="PopupContentMid2">
		<div class="GeneralForm Search1Form">
			<label id="label-code" class="LabelStyle Label1Style" style="width: 100px;">Mã sản phẩm</label>
			<input id="txtProductCodeDl" maxlength="50" type="text" style="width: 145px;" value="" class="InputTextStyle InputText1Style" />
			<label id="label-name" class="LabelStyle Label1Style" style="width: 100px;">Tên sản phẩm</label>
			<input id="txtProductNameDl" maxlength="250" type="text" style="width: 145px;" value="" class="InputTextStyle InputText1Style" />
			<div class="Clear"></div>
			<div class="Clear"></div>
			<div class="BtnCenterSection">
				<button class="BtnGeneralStyle BtnGeneralMStyle" id="common-dialog-button-search" onclick="return AdjustedRevenueManage.commonDialogGridSearch();">Tìm kiếm</button>
			</div>
			<div class="Clear"></div>
			<div class="GridSection" id="common-dialog-grid-container">
				<table id="common-dialog-grid-search"></table>
			</div>
			<div class="BtnCenterSection">
				<button class="BtnGeneralStyle BtnGeneralMStyle" onclick="AdjustedRevenueManage.choseArrProductInDialogSearch();">Chọn</button>
				<button class="BtnGeneralStyle BtnGeneralMStyle" onclick="$('#common-dialog-search-2-textbox').dialog('close');">Đóng</button>
			</div>
			<p id="errMsgDialogChosePrd" style="display: none;" class="ErrorMsgStyle"></p>
           	<p id="successMsgDialogChosePrd" class="SuccessMsgStyle" style="display:none;"></p>
           	<input type="hidden" id="permissionIdByChangeDialog" />
			<div class="Clear"></div>
		</div>
	</div>
</div>
<script type="text/javascript">
	$(function(){
		$('#txtProductCodeDl, #txtProductNameDl, #common-dialog-button-search').bind('keyup',function(event){
			if(event.keyCode == keyCodes.ENTER){
				$('#common-dialog-button-search').click();
			}
		});
	});
</script>
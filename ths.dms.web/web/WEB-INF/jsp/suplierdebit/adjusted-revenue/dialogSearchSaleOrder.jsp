<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<div id="dialogSearchSaleOrder" class="easyui-dialog" title="Tìm kiếm đơn hàng" data-options="closed:true,modal:true">
	<div class="PopupContentMid2">
		<div class="GeneralForm Search1Form">
			<label class="LabelStyle Label2Style">Số đơn hàng</label>
			<input id="txtOrderNumberDl" type="text" class="InputTextStyle InputText5Style" maxlength="20" />
			<label class="LabelStyle Label7Style">Thực hiện</label>
			<div class="BoxSelect BoxSelect5" id="dllOrderTypeDivDl">
				<select class="MySelectBoxClass" id="dllOrderTypeDl">
					<option value="IN" selected="selected">Presale</option>
					<option value="SO">Vansale</option>
				</select>
			</div>
			<div class="Clear"></div>
			<label class="LabelStyle Label2Style">Mã KH (F9)</label>
			<input id="txtShortCodeDl" type="text" class="InputTextStyle InputText5Style" maxlength="20" />
			<label class="LabelStyle Label7Style">Trạng thái</label>
			<p class="ValueStyle Value1Style">Đã duyệt</p>
			<div class="Clear"></div>
			<label class="LabelStyle Label2Style">Tên KH </label>
			<input id="txtCustomerNameDl" type="text" class="InputTextStyle InputText5Style" maxlength="250" />
			<label class="LabelStyle Label7Style">Loại đơn hàng</label>
			<p class="ValueStyle Value1Style">Đơn bán</p>
			<div class="Clear"></div>
			<label class="LabelStyle Label2Style">Từ ngày</label>
			<input type="text" class="InputTextStyle InputText6Style vinput-date" id="txtFromDateDl" maxlength="10" />
			<label class="LabelStyle Label7Style">Đến ngày</label>
			<input type="text" class="InputTextStyle InputText6Style vinput-date" id="txtToDateDl" maxlength="10" />
			<div class="Clear"></div>
			<label class="LabelStyle Label2Style">NVBH</label>
			<input id="txtStaffCodeDl" type="text" class="InputTextStyle InputText5Style" maxlength="50" />
			<label class="LabelStyle Label7Style">NVGH</label>
			<input id="txtDeliveryCodeDl" type="text" class="InputTextStyle InputText5Style" maxlength="50" />
			<div class="Clear"></div>
			<div class="BtnCenterSection">
				<button class="BtnGeneralStyle BtnSearchOnDialog" id="btnSearchSaleOrderDl" onclick="return AdjustedRevenueManage.searchSaleOrderDialog();">Tìm kiếm</button>
			</div>
			<div class="Clear"></div>
		</div>
		<div class="GeneralForm Search1Form">
			<h2 class="Title2Style Title2MTStyle">Danh sách đơn hàng</h2>
			<div class="GridSection" id="gridSearchBillContGrid">
				<table id="gridSearchBill"></table>
			</div>
			<div class="Clear"></div>
			<div class="BtnCenterSection">
				<button class="BtnGeneralStyle" onclick="$('#dialogSearchSaleOrder').dialog('close');">Đóng</button>
			</div>
		</div>
		<div class="Clear"></div>
		<p class="ErrorMsgStyle" id="errorSearchMsg" style="display: none;"></p>
	</div>
</div>

<!--<div id ="searchCustomerEasyUIDialogDiv" style="display: none" >
	<div id="searchCustomerEasyUIDialog" class="easyui-dialog" style="width: 600px; height: auto;" title="" data-options="closed:true,modal:true">
		<div class="PopupContentMid">
			<div class="GeneralForm Search1Form" >
				<label id="seachStyle1CodeLabel" class="LabelStyle Label1Style" style=" width: 100px;">Mã</label>
				<input id="seachStyle1Code" maxlength="20" tabindex="1" type="text" style=" width: 145px;" class="InputTextStyle InputText1Style" /> 
				<label id="seachStyle1NameLabel" class="LabelStyle Label1Style" style=" width: 100px;">Tên</label> 
				<input id="seachStyle1Name" type="text" maxlength="20" tabindex="2" style="width: 145px;"  class="InputTextStyle InputText1Style" />
				<div class="Clear"></div>
                <label id="seachStyle1AddressLabel" style="display: none;width: 100px;" class="LabelStyle Label1Style">Địa chỉ</label>
                <input id="seachStyle1Address" type="text" style="display: none;width: 409px;"  maxlength="250" tabindex="3" class="InputTextStyle InputText1Style" />
                <div class="Clear"></div>
				<button class="BtnGeneralStyle BtnSearchStyle BtnSearchOnDialog" tabindex="3" id="btnsearchCustomer">Tìm kiếm</button>
				<input type="hidden" name="" id="searchCustomerUrl"/>
				<input type="hidden" name="" id="searchCustomerCodeText"/>
				<input type="hidden" name="" id="searchCustomerNameText"/>
				<input type="hidden" name="" id="searchCustomerIdText"/>
				<s:hidden id="searchCustomerAddressText"></s:hidden>
				<div class="Clear"></div>
				<div class="GridSection" id="searchCustomerContainerGrid">					
					<table id="searchCustomerGrid" class="easyui-datagrid"></table>
					<div id="searchCustomerPager"></div>
				</div>
				<div class="BtnCenterSection">
					<button id="__btnSave" style="display: none;" class="BtnGeneralStyle">Chọn</button>
					<button class="BtnGeneralStyle BtnGeneralMStyle" onclick="$('#searchCustomerEasyUIDialog').dialog('close');">Đóng</button>
				</div>
				<p id="errMsgSearch" style="display: none;" class="ErrorMsgStyle SpriteErr"></p>
			</div>
		</div>
	</div>
</div>-->

<script type="text/javascript">
	$(function(){
		$('#txtOrderNumberDl, #dllOrderTypeDivDl, #txtShortCodeDl, #txtCustomerNameDl, #txtFromDateDl, #txtToDateDl, #txtStaffCodeDl, #txtDeliveryCodeDl, #btnSearchSaleOrderDl').bind('keyup',function(event){
			if(event.keyCode == keyCodes.ENTER){
				$('#btnSearchSaleOrderDl').click();
			}
		});
		$('#txtShortCodeDl').live('keyup', function(event){
			if(event.keyCode == keyCode_F9){
				VCommonJS.showDialogSearch2({
					params : {
						status : 1,
						shopId: $('#ddlShopId').combobox('getValue')
					},
					inputs : [
				        {id:'code', maxlength:50, label:'Mã khách hàng'},
				        {id:'name', maxlength:250, label:'Tên khách hàng'},
				    ],
				    url : '/commons/customer-in-shop-search',
				    columns : [[
				        {field:'shortCode', title:'Mã khách hàng', align:'left', width: 110, sortable:false, resizable:false, formatter: function(value, row, index) {
				        	return Utils.XSSEncode(value);         
				        }},
				        {field:'customerName', title:'Tên khách hàng', align:'left', width: 200, sortable:false, resizable:false, formatter: function(value, row, index) {
				        	return Utils.XSSEncode(value);         
				        }},
				        {field:'choose', title:'', align:'center', width:40, sortable:false, resizable:false, formatter:function(value,row,index) {
				            return '<a href="javascript:void(0)" onclick="AdjustedRevenueManage.fillTxtShortCodeDlByF9(\''+Utils.XSSEncode(row.shortCode)+'\' ,\''+Utils.XSSEncode(row.customerName)+'\');">chọn</a>';         
				        }}
				    ]]
				});
				
			}
		});
	});
</script>

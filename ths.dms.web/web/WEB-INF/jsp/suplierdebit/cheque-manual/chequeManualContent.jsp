<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<div class="BreadcrumbSection">
	<ul class="ResetList FixFloat BreadcrumbList">
		<li class="Sprite1"><a href="javascript:void(0)">Quản lý công nợ NPP</a></li>
		<li><span>Nhập séc sổ tay</span></li>
	</ul>
</div>
<div class="CtnOneColSection">
	<div class="ContentSection">
		<div class="ToolBarSection">
			<div class="SearchSection GeneralSSection">
				<h2 class="Title2Style">Thông tin tìm kiếm</h2>
				<div class="SearchInSection SProduct1Form">
					<label class="LabelStyle Label1Style">Đơn vị</label> 
					<div class="BoxSelect BoxSelect2">
						<div class="Field2">
							<input id="shop" style="width:206px;">
						</div>
					</div>
					<label class="LabelStyle Label1Style">Loại <span class="ReqiureStyle">*</span></label>
					<div class="BoxSelect BoxSelect2">
						<select id="type" class="MySelectBoxClass" autocomplete="off">
							<option value="1">Có</option>
							<option value="0" selected="selected">Nợ</option>
						</select>
					</div>
					
					<label class="LabelStyle Label6Style" style="margin-top:-7px;">Số đơn hàng<br/>Phiếu điều chỉnh nợ</label> 
					<input	type="text" class="InputTextStyle InputText1Style" id="poConfirmNumber" maxlength="40" autocomplete="off" /> 
					<div class="Clear"></div>
					
					<label class="LabelStyle Label1Style">Ngày tạo từ</label> 
					<input type="text" class="InputTextStyle InputText6Style vinput-date"  id="fromDate" maxlength="10" value="<s:property value="fromDate"/>" autocomplete="off" /> 
					
					<label class="LabelStyle Label6Style">Đến</label>
					<input type="text" class="InputTextStyle InputText6Style vinput-date"  id="toDate" maxlength="10" value="<s:property value="toDate"/>" autocomplete="off" /> 
					
					<div class="Clear"></div>
					<div class="BtnCenterSection">
						<button class="BtnGeneralStyle" id="btnSearch" onclick="ChequeManualDebit.search();" >Tìm kiếm</button>
					</div>
					<p id="errMsgSearch" class="ErrorMsgStyle" style="display: none">Có lỗi xảy ra khi cập nhật dữ liệu</p>
					<div class="Clear"></div>
				</div>				
				<h2 class="Title2Style">Danh sách đơn hàng tìm được</h2>
				<div class="SearchInSection SProduct1Form">
					<div class="GridSection" id="gridContainer" style="padding: 0px;">
					<table id="dg"></table>						
					</div>
				<div id="showCheque" style="display: none;">
					<div class="dgInput" style="display: none;">
							<h2 class="Title2Style" id="title"></h2>
                            <div class="SearchInSection SProduct1Form">
                                <label class="LabelStyle Label1Style">Số ủy nhiệm <span class="ReqiureStyle">*</span></label>
                                <input type="text" class="InputTextStyle InputText1Style" id="soUyNhiem" maxlength="20" autocomplete="off" />
                                <label class="LabelStyle Label1Style">Số tiền<span class="ReqiureStyle">*</span></label>
                                <input type="text" class="InputTextStyle InputText1Style" maxlength="20" id="totalMoney" autocomplete="off" />
                                <div class="Clear"></div>
                                <label class="LabelStyle Label1Style">Nợ sau giảm trừ</label>
                                <input type="text" class="InputTextStyle InputText1Style" id="remainAfter" disabled="disabled" autocomplete="off" />
                                <label class="LabelStyle Label1Style">Ngân hàng </label>
                                <div class="BoxSelect BoxSelect2">
			<%-- 						<s:select list="listInvoiceType" id="invoiceType" listKey="value" listValue="name" headerKey="-1" headerValue="Phiếu chi/Phiếu thu" cssClass="MySelectBoxClass"></s:select> --%>
									<select id="bankId" autocomplete="off" class="MySelectBoxClass" style="opacity: 0; height: 20px; width: 206px;"></select>
								</div>                             
                               	<div class="Clear"></div>
                           </div>
					</div>
					<div class="Clear"></div>
					<div class="BtnCenterSection dgInput">
						<button class="BtnGeneralStyle" id="btnPayOrder" onclick="return ChequeManualDebit.payOrder();">Lưu</button>
					</div>
					</div>
					<div class="Clear"></div>
                    <p id="errMsg" class="ErrorMsgStyle" style="display: none">Có lỗi xảy ra khi cập nhật dữ liệu</p>
					<tiles:insertTemplate template="/WEB-INF/jsp/general/successMsg.jsp" />					
				</div>
				<div class="Clear"></div>
			</div>
			<div class="Clear"></div>
		</div>
	</div>
</div>
<input type="hidden" id="remainHidden" value="0" />
<s:hidden id="locked" name="shopLocked"></s:hidden>
<input type="hidden" id="shopCode" value="<s:property value='shopCode' />" />

<script type="text/javascript">
	function init(){
		var locked = $('#locked').val();
		$('.ContentSection').unbind('keyup');
		$('.InputTextStyle').unbind('keyup');
		$('#poConfirmNumber').focus();
		$('.dgInput').hide();
		
		$('.MySelectBoxClass').css('width','206');
		$('.CustomStyleSelectBox').css('width','173');
		//$('.MySelectBoxClass').customStyle();
// 		$('#type').css('width', '226px');
// 		$('#type').next().find('.CustomStyleSelectBoxInner').css('width', '193px');
		//applyDateTimePicker('#fromDate');
		//applyDateTimePicker('#toDate');
		Utils.formatCurrencyFor('totalMoney');
		Utils.bindFormatOnTextfield('totalMoney',Utils._TF_NUMBER_COMMA);
		Utils.formatCurrencyFor('money');
		$('#dg').datagrid({  
		    url:'/suplierdebit/manage/search-sec',		    	    
		    singleSelect:true,		      
		    width: $(window).width() -50,
		    height: 350,
		    //scrollbarSize : 0,
		    rownumbers : true,
		    fitColumns:true,
		    queryParams:{
		    	type : $('#type').val().trim(),
		    	fromDate : $('#fromDate').val().trim(),
		    	toDate : $('#toDate').val().trim(),
		    	shopCode : $('#shopCode').val().trim()
		    },		    
		    columns:[[ 
				{field:'poConfirmNumber', title:'Số đơn hàng/ Số phiếu <br/>điều chỉnh công nợ', width:150, align:'left', resizable:false, sortable:false, formatter: function(value, row, index) {
					return Utils.XSSEncode(value);         
				}},  
				{field:'poVnmDate', title:'Ngày tạo', width:120, resizable:false, sortable:false, align:'center',formatter:CommonFormatter.dateTimeFormatter},
				{field:'amount', title:'Tổng tiền <br/>(Chưa chiết khấu)', width:120, resizable:false, sortable:false, align:'right',
					formatter: function(val, row, idx) {
						if (val != undefined && val != null) {
							return formatCurrency(Math.abs(val));
						}
						return '';
					}
				},
				{field:'discount', title:'Chiết khấu', width:120, resizable:false, sortable:false, align:'right',
					formatter: function(val, row, idx) {
						if (val != undefined && val != null) {
							return formatCurrency(Math.abs(val));
						}
						return '';
					}
				},
				{field:'total', title:'Tổng tiền <br/>(Đã chiết khấu)', width:120, resizable:false, sortable:false, align:'right',
					formatter: function(val, row, idx) {
						if (val != undefined && val != null) {
							return formatCurrency(Math.abs(val));
						}
						return '';
					}
				},
				{field:'totalPay', title:'Đã trả', width:120, resizable:false, sortable:false, align:'right',
					formatter: function(val, row, idx) {
						if (val != undefined && val != null) {
							return formatCurrency(Math.abs(val));
						}
						return '';
					}
				},
				{field:'remain', title:'Còn lại', width:120, resizable:false, sortable:false, align:'right',
					formatter: function(val, row, idx) {
						if (val != undefined && val != null) {
							return formatCurrency(Math.abs(val));
						}
						return '';
					}
				},				
				{field:'input', title:'Nhập Sec', width:80, resizable:false, sortable:false, align:'center',formatter:function(value,row,index){
					if(locked == "false"){
						return '<a href="javascript:void(0);" class="easyui-tooltip" onclick="ChequeManualDebit.showDetail('+index+');" >'+
						'<img width="16" height="16" title="Nhập séc sổ tay" alt="Nhập séc" src="/resources/images/icon_login.png"></a>';	
					}
				}}						
			]],    
			onLoadSuccess :function(data){
				$('.datagrid-header-rownumber').html('STT');
			   	$('.datagrid-header-row td div').css('text-align','center');		   	
			   	$(window).resize();
			}
		});
		
		$('#totalMoney').bind('blur',function(){
			$('.ErrorMsgStyle').hide();
			var row = $('#dg').datagrid('getRows')[ChequeManualDebit._indexDetail];			
			//var checkOut = $('#totalMoney').val().trim();
			var value = Math.abs(row.remain) - Utils.returnMoneyValue($('#totalMoney').val().trim());
			if(isNaN(value)) {
				$('#errMsg').html('Số tiền nhập vào phải là số nguyên dương > 0').show();
				$('#totalMoney').val('');
				return false;
			}
			$('#remainAfter').val(CommonFormatter.numberFormatter(value));
		});
		
		$('#type, #poConfirmNumber, #fromDate, #toDate').bind('keyup', function(event) {
			if(event.keyCode == keyCodes.ENTER){		   				
				$('#btnSearch').click();
			}
		});
		$('#soUyNhiem, #totalMoney').bind('keyup', function(event) {
			if(event.keyCode == keyCodes.ENTER){		   				
				$('#btnPayOrder').click();
			}
		});
	}
	
	var isFirstLoad = true;
	$(document).ready(function() {
		Utils.initUnitCbx('shop', null, 206, function(rec) {
	    	if (rec != null && rec.shopCode != ReportUtils._currentShopCode) {
				$('#shopCode').val(rec.shopCode);
				ReportUtils._currentShopCode = rec.shopCode;
				
				$('#fromDate').val('');
				$('#toDate').val('');
				$('#bankId').html('');
				
				var params = new Object();
				params.shopCode = rec.shopCode;
				var url = '/suplierdebit/manage/get-shop-init-data';
				
				Utils.getJSONDataByAjax(params, url, function(data) {
					if (data != undefined && data != null) {
						$('#fromDate').val(data.fromDate);
						$('#toDate').val(data.toDate);
						
						var row = data.listBank;
						if(row != null && row.length > 0){						
							if (row.length > 1) { 
								$('#bankId').append('<option value = "-1" selected = "selected" >' + Utils.XSSEncode('Chọn ngân hàng') + '</option>');
							}
							for (var i = 0, sz = row.length; i < sz; i++) {
								$('#bankId').append('<option value = "' + Utils.XSSEncode(row[i].value) + '">' + Utils.XSSEncode(row[i].name) + '</option>');  
							}
						} else {
							$('#bankId').append('<option value = "-1">' + Utils.XSSEncode('Không có ngân hàng') + '</option>');
						}
						$('#bankId').change();
						
						if(isFirstLoad){
							isFirstLoad = false;
							init();
						} else{
							ChequeManualDebit.search();
						}
					}
				});
	    	}
		}, function(arr) {
	       	if (arr != null && arr.length > 0) {
	       		ReportUtils._currentShopCode = arr[0].shopCode;
	       	}
		});
	});
</script>
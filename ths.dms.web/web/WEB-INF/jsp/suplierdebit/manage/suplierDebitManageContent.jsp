<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<div class="BreadcrumbSection">
	<ul class="ResetList FixFloat BreadcrumbList">
		<li class="Sprite1"><a href="javascript:void(0)">Quản lý công nợ NPP</a></li>
		<li><span>Thanh toán đơn hàng NPP</span></li>
	</ul>
</div>
<div class="CtnOneColSection">
	<div class="ContentSection">
		<div class="ToolBarSection">
			<div class="SearchSection GeneralSSection">
				<h2 class="Title2Style">Nhập thanh toán</h2>
				<div class="SearchInSection SProduct1Form">
					<label class="LabelStyle Label1Style">Đơn vị</label> 
					<div class="BoxSelect BoxSelect2">
						<div class="Field2">
							<input id="shop" style="width:206px;">
						</div>
					</div>
					<label class="LabelStyle Label1Style">Số chứng từ <span	class="ReqiureStyle">*</span></label> 
					<input type="text" class="InputTextStyle InputText1Style" maxlength="20" value="<s:property value="invoiceNumber"/>" id="invoiceNumber" /> 
					
					<label class="LabelStyle Label1Style" id="labelDebit">Nợ NPP với công ty</label>
					<p class="ValueStyle Value1Style" id="currentDebit"><s:property value="convertMoney(currentDebit)" /></p>
					
					<div class="Clear"></div>
					<label class="LabelStyle Label1Style">Loại chứng từ <span class="ReqiureStyle">*</span></label>
					<div class="BoxSelect BoxSelect2">
<%-- 						<s:select list="listInvoiceType" id="invoiceType" listKey="value" listValue="name" headerKey="-1" headerValue="Phiếu chi/Phiếu thu" cssClass="MySelectBoxClass"></s:select> --%>
						<s:select list="listInvoiceType" id="invoiceType" listKey="value" listValue="name" cssClass="MySelectBoxClass" onchange="return SuplierDebit.onChangeSelectInvoiceType();"></s:select>
					</div>
					<label class="LabelStyle Label1Style">Số tiền <span class="ReqiureStyle">*</span></label>
					<input type="text" class="InputTextStyle InputText1Style"  id="money" value="" maxlength="20"/>					
					<label class="LabelStyle Label1Style">Ngày lập</label> 
					<input type="text" class="InputTextStyle InputText6Style"  id="createDate"	value="<s:property value="lockDate"/>" disabled="disabled" /> 
					<div class="Clear"></div>
					<div class="BtnCenterSection">
						<button class="BtnGeneralStyle" id="btnSearch" onclick="return SuplierDebit.watchDistribution();">Xem phân bổ</button>
					</div>
					<div class="Clear"></div>
					<div id="errorDistributeMsg" class="ErrorMsgStyle" style="display: none;"></div>
				</div>				
				<h2 class="Title2Style">Kết quả tìm kiếm</h2>
				<div class="SearchInSection SProduct1Form">
					<div class="GridSection" id="gridContainer" style="padding: 0px;">
						<table id="dg"></table>
					</div>
					<div style="padding: 10px 0 0;">
						<p class="ValueStyle Value1Style" id="remainAfterPaid" style="color: red; float: right;" >0</p>
						<label class="LabelStyle Label5Style" style="float: right;">Nợ sau thanh toán:</label>
					</div>
					<div class="Clear"></div>
					<div class="BtnCenterSection">
                         <button id="paidDebit" class="BtnGeneralStyle cmsiscontrol" onclick="return SuplierDebit.paidDebit();">Thanh toán</button>
                    </div>
                    <div class="Clear"></div>
                    <p id="errMsg" class="ErrorMsgStyle" style="display: none">Có lỗi xảy ra khi cập nhật dữ liệu</p>
					<tiles:insertTemplate template="/WEB-INF/jsp/general/successMsg.jsp" />					
				</div>
				<div class="Clear"></div>
			</div>
			<div class="Clear"></div>
		</div>
	</div>
</div>
<s:hidden id="invoiceTypeHide" name="invoiceTypeHide"></s:hidden>
<s:hidden id="moneyHide" name="moneyHide"></s:hidden>
<s:hidden id="invoiceNumberHide" name="invoiceNumberHide"></s:hidden>

<s:hidden id="debitNPPToVNM" name="currentDebit"></s:hidden>
<s:hidden id="debitVNMToNPP" name="currentDebitVNMToNPP"></s:hidden>
<input type="hidden" id="shopCode" value="<s:property value='shopCode' />" />

<script type="text/javascript">
	$(document).ready(function() {
		Utils.initUnitCbx('shop', null, 206, function(rec) {
	    	if (rec != null && rec.shopCode != ReportUtils._currentShopCode) {
				$('#shopCode').val(rec.shopCode);
				ReportUtils._currentShopCode = rec.shopCode;
				disabled('paidDebit');
				
				var chooseStatus = $('#invoiceType').val();
				var shopCode = $('#shopCode').val();
				var params = new Object();
				params.invoiceTypeChoice = chooseStatus;
				params.shopCode = shopCode;
				$('#dg').datagrid('load',params);
	    	}
		}, function(arr) {
	       	if (arr != null && arr.length > 0) {
	       		ReportUtils._currentShopCode = arr[0].shopCode;
	       	}
		});
		
		$('#paidDebit').attr('disabled', 'disabled');
		//$('.MySelectBoxClass').customStyle();
		$('.MySelectBoxClass').css('width','206');
		$('.CustomStyleSelectBox').css('width','173');	
		Utils.bindFormatOnTextfield('money', Utils._TF_NUMBER_COMMA);
		Utils.formatCurrencyFor('money');
		var isCheck = false;
		$('.InputTextStyle').each(function(){
		    if(!$(this).is(':hidden') && !$(this).is(':disabled') && !isCheck){
		        isCheck = true;
		        $(this).focus();
		    }
		});
		var params = {};
		params.invoiceTypeChoice = $('#invoiceType').val().trim();
		$('#dg').datagrid({
		    url:'/suplierdebit/manage/watch-distribute',		    	    
		    singleSelect:true,		      
		    width: $(window).width() -35,
		    height: 350,
		    rownumbers : true,
		    fitColumns:true,		    
		    queryParams:params,
		    showFooter: true,
		    //scrollbarSize:0,
		    columns:[[ 
				{field:'invoiceNumber', title:'Số hóa đơn', width:110, align:'left', resizable:false, sortable:false, formatter: function(value, row, index) {
					return Utils.XSSEncode(value);         
				}}, 
				{field:'shopCode', title:'Mã đơn vị', width:90, align:'left', resizable:false, sortable:false, formatter: function(value, row, index) {
					return Utils.XSSEncode(value);         
				}},
				{field:'poCoNumber', title:'ASN Number/<br/>Phiếu điều chỉnh nợ', width:110, align:'left', resizable:false, sortable:false, formatter: function(value, row, index) {
					return Utils.XSSEncode(value);         
				}},
				{field:'createDate', title:'Ngày tạo', width:60, resizable:false, sortable:false, align:'center',formatter:function(value,row,index){
					if(row.createDate==null) return '';
					return $.datepicker.formatDate('dd/mm/yy', new Date(row.createDate));
				}},
				{field:'amount', title:'Thành tiền trước VAT', width:120, resizable:false, sortable:false, align:'right',formatter:function(value){
					return formatCurrency(Math.abs(value));
				}},
				{field:'discount', title:'Chiết khấu', width:80, resizable:false, sortable:false, align:'right',formatter:function(value){
					return formatCurrency(Math.abs(value));
				}},
				{field:'total', title:'Thành tiền sau VAT', width:110, resizable:false, sortable:false, align:'right',formatter:function(value){
					return formatCurrency(Math.abs(value));
				}},
				{field:'remain', title:'Còn nợ', width:90, resizable:false, sortable:false, align:'right',formatter:function(value){
					return formatCurrency(Math.abs(value));
				}},
				{field:'userPaid', hidden: true, title:'Thanh toán', width:120, resizable:false, sortable:false, align:'right', fixed: true, formatter:function(value,row,index){	
					return '<input class="InputTextStyle InputText1Style" style="margin: 0;text-align:right; width: 100px;" type="text" value="'+formatCurrency(Math.abs(value))+'" disabled="disabled"  />';				
				}},
				{field:'remainAfterPaid', hidden: true, title:'Nợ sau thanh toán', resizable:false, sortable:false, width:120, align:'right', formatter:function(value,row,index){
					if(value==null) return  formatCurrency(Math.abs(row.remain));
					return formatCurrency(Math.abs(value));
				}}
			]],    
			onLoadSuccess :function(data){
				if(data!=null){
					if(data.error && data.errMsg.length>0){
						$('#errorDistributeMsg').html(data.errMsg).show();
						disabled('paidDebit');
					}else{
						$('#remainAfterPaid').html(formatCurrency(data.remainAfterPaid));
					}
					if(data.currentDebit != null && data.currentDebit != undefined && data.currentDebitVNMToNPP != null && data.currentDebitVNMToNPP != undefined && data.lockDate && data.invoiceNumber) {
						$("#debitNPPToVNM").val(data.currentDebit);
						$("#debitVNMToNPP").val(data.currentDebitVNMToNPP);
						$("#createDate").val(data.lockDate);
						$("#invoiceNumber").val(data.invoiceNumber);
					} else {
						$("#debitNPPToVNM").val(0);
						$("#debitVNMToNPP").val(0);
						$("#createDate").val("");
						$("#createDate").val("");
					}
					
					var chooseStatus = $('#invoiceType').val();
					if (chooseStatus == 2) {
						var debit = $("#debitNPPToVNM").val();
						$("#labelDebit").html('Nợ NPP với công ty');
						$("#currentDebit").html(formatCurrency(debit));
					} else {
						var debit1 = $("#debitVNMToNPP").val();
						$("#labelDebit").html('Nợ công ty với NPP');
						$("#currentDebit").html(formatCurrency(debit1));	
					}	
					
					//if ((data.isPageLoad == undefined || !data.isPageLoad) && $("td[field=userPaid]").is(":hidden")) {
					if ( (data.isPageLoad == undefined || !data.isPageLoad) ) {
						$('#dg').datagrid("showColumn", "userPaid");
						$('#dg').datagrid("showColumn", "remainAfterPaid");
						//$('#dg').datagrid("fitColumns");
					} else {	
						$('#dg').datagrid("hideColumn", "userPaid");
						$('#dg').datagrid("hideColumn", "remainAfterPaid");
						$('#money').val('');
						disabled('paidDebit');
					}
					$('.datagrid-header-rownumber').html('STT');
				   	$('.datagrid-header-row td div').css('text-align','center');
				   	 updateRownumWidthForJqGrid('.easyui-dialog');
			   	 	$(window).resize();   
				}
			}
		});
	});
</script>
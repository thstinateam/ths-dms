<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<div class="GeneralTable Table16Section">
	<div class="ScrollSection">
		<div class="BoxGeneralTTitle">
			<table width="100%" border="0" cellspacing="0" cellpadding="0">
				<colgroup>
					<col style="width: 30px;" />
					<col style="width: 80px;" />
					<col style="width: 90px;" />
					<col style="width: 135px;" />
					<col style="width: 114px;" />
					<col style="width: 140px;" />
					<col style="width: 100px;" />
					<col style="width: 110px;" />
					<col style="width: 121px;" />
				</colgroup>
				<thead>
					<tr>
						<th class="ColsThFirst">STT</th>
						<th>Số hóa đơn</th>
						<th>Mã đơn vị</th>
						<th>PO confirm number</th>
						<th>Ngày tạo</th>
						<th>Tổng tiền (-chiết khấu)</th>
						<th>Còn nợ</th>
						<th>Thanh toán</th>
						<th class="ColsThEnd">Nợ sau thanh toán</th>
					</tr>
				</thead>
			</table>
		</div>
		<div class="BoxGeneralTBody">
			<div class="ScrollBodySection" id="tableBody">
				<table width="100%" border="0" cellspacing="0" cellpadding="0">
					<colgroup>
						<col style="width: 30px;" />
						<col style="width: 80px;" />
						<col style="width: 90px;" />
						<col style="width: 135px;" />
						<col style="width: 114px;" />
						<col style="width: 140px;" />
						<col style="width: 100px;" />
						<col style="width: 110px;" />
						<col style="width: 121px;" />
					</colgroup>
					<tbody>
						<s:iterator value="listDebitShop" status="status">
							<tr>
								<td class="ColsTd1"><s:property value="#status.index+1" /></td>
								<td class="ColsTd2"><s:property value="invoiceNumber" />
								</td>
								<td class="ColsTd3"><s:property value="shop.shopCode" />
								</td>
								<td class="ColsTd4 AlignLeft"><s:property
										value="poCoNumber" />
								</td>
								<td class="ColsTd5"><s:property
										value="displayDate(createDate)" />
								</td>
								<td class="ColsTd6 AlignRight"><s:if test="total == null">
								0
							</s:if> <s:else>
										<s:property value="convertMoney(displayPositiveNumber(total))" />
									</s:else></td>
								<td class="ColsTd7 AlignRight"><s:if test="remain == null">
								0
							</s:if> <s:else>
										<s:property value="convertMoney(displayPositiveNumber(remain))" />
									</s:else></td>
								<td class="ColsTd8">
									<input type="text"	class="InputStyleStock" 
										style="padding-right: 0px !important;"		value='<s:property value="userPaid == null ? 0 : convertMoney(displayPositiveNumber(userPaid))"/>'
									disabled="disabled" />
								</td>
								<td class="ColsTd9 ColsTdEnd AlignRight"><s:if
										test="remainAfterPaid == null">
										<s:property value="convertMoney(displayPositiveNumber(remain))" />
									</s:if> <s:else>
										<s:property value="convertMoney(displayPositiveNumber(remainAfterPaid))" />
									</s:else></td>
							</tr>
						</s:iterator>
					</tbody>
				</table>
			</div>
		</div>
	</div>
</div>
<label class="LabelStyle Label1Style">Nợ sau thanh toán:</label>
<p class="ValueStyle Value1Style" id="debitAfterPaid">
	<s:property value="convertMoney(displayPositiveNumber(remainAfterPaid))" />
</p>
<div class="Clear"></div>
<div class="ButtonSection">
	<button class="BtnGeneralStyle Sprite2" onclick="return SuplierDebit.paidDebit();">
		<span class="Sprite2">Thanh toán</span>
	</button>
	<img src="/resources/images/loading.gif" id="paidLoading" style="visibility: hidden;" class="LoadingStyle" />
</div>
<p id="errMsg" class="ErrorMsgStyle Sprite1" style="display: none">Cólỗi xảy ra khi cập nhật dữ liệu</p>
<tiles:insertTemplate template="/WEB-INF/jsp/general/successMsg.jsp" />
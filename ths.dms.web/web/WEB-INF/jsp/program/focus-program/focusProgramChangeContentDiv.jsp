<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@taglib prefix="k" uri="/kryptone"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<div class="BreadcrumbSection">
    <ul class="ResetList FixFloat BreadcrumbList">
        <li class="Sprite1"><a href="/catalog/bary-centric-programme">Chương trình trọng tâm</a></li>
        <li><span>Thông tin chương trình trọng tâm</span></li>
    </ul>
</div>
<div class="CtnOneColSection">
  <div class="ContentSection">
  	<div class="ToolBarSection">
		 <div class="SearchSection GeneralSSection">
		        <h2 class="Title2Style">Thông tin CTTT</h2>
		      	<div class="SearchInSection SProduct1Form">
		       	<label class="LabelStyle Label1Style">Mã CTTT<span class="ReqiureStyle">*</span></label>
	       		<s:textfield id="code" name="code" cssClass="InputTextStyle InputText1Style"></s:textfield>
	            <label class="LabelStyle Label1Style">Tên CTTT<span class="ReqiureStyle">*</span></label>
           		<s:textfield id="name" name="name" cssClass="InputTextStyle InputText2Style"></s:textfield>
	            <div class="Clear"></div>
	            <label class="LabelStyle Label1Style">Từ ngày<span class="ReqiureStyle">*</span></label>
           		<input id="fDate" class="InputTextStyle InputText6Style" value="<s:property value="fromDate"/>"/>
	            <label class="LabelStyle Label1Style">Đến ngày</label>	                  
	            <input id="tDate" class="InputTextStyle InputText6Style" value="<s:property value="toDate"/>"/>
	            <s:hidden id="stableToDate" name="toDate"/>	                  		
	            <label class="LabelStyle Label14Style">Trạng thái<span class="ReqiureStyle">*</span></label>
            	<div class="BoxSelect BoxSelect2">
             		 <s:if test="id != null && id > 0 && status == 2">
	             		 <s:select id="status" name="status" 
		              		 cssClass="MySelectBoxClass" list="lstStatusBeanWithWaiting" 
		              		 listKey="value" listValue="name">
	             		 </s:select>
             		 </s:if>
             		 <s:elseif test="id != null && id > 0 && status != 2">
	             		 <s:select id="status" headerKey="-2" 
		              		 headerValue="-- Tất cả --"  name="status" 
		              		 cssClass="MySelectBoxClass" list="lstStatusBean" 
		              		 listKey="value" listValue="name">
	             		 </s:select>
             		 </s:elseif>
             		 <s:else>
             		 	<s:select id="status" headerKey="-2" 
		              		 headerValue="-- Tất cả --"  name="status" 
		              		 cssClass="MySelectBoxClass" list="lstStatusBeanWithWaiting" 
		              		 listKey="value" listValue="name">
             		 	</s:select>
             		 </s:else>
             		 
          		</div>
	            <div class="Clear"></div>
	            <div class="BtnCenterSection" id="divSave">
	             	<s:if test="id != null && id > 0">
                    	<button id="btnUpdate" class="BtnGeneralStyle" onclick="BaryCentricProgrammeCatalog.saveInfo();">Cập nhật</button>
	            	    <button id="btnCopy" class="BtnGeneralStyle" onclick="BaryCentricProgrammeCatalog.openCopyFocusProgram();">Sao chép</button>
	             	</s:if>
	             	<s:else>
                    	<button class="BtnGeneralStyle" onclick="BaryCentricProgrammeCatalog.saveInfo();">Lưu</button>
	              	</s:else>
				    <img id="loading" src="/resources/images/loading.gif" class="LoadingStyle" style="visibility: hidden;" /> 
                </div> 
                <p style="display:none;margin-top:10px;margin-left: 10px" class="ErrorMsgStyle" id="errMsgInfo"/>
                <p id="successMsg1" class="SuccessMsgStyle" style="display: none"></p>  
				</div>							
	            <div id = "subContent" style="display: none;">
	            
	            	<div class="TabSection">
		                <ul class="ResetList TabSectionList">
		                      <li><a href="javascript:void(0);" id="tab1" onclick="BaryCentricProgrammeCatalog.showTab1();" class="Sprite1 Active"><span class="Sprite1">Hình thức bán hàng tham gia</span></a></li>
		                      <li><a href="javascript:void(0);" id="tab2" onclick="BaryCentricProgrammeCatalog.showTab2();" class="Sprite1"><span class="Sprite1">Sản phẩm của CTTT</span></a></li>
		                      <li><a href="javascript:void(0);" id="tab3" onclick="BaryCentricProgrammeCatalog.showTab3();" class="Sprite1"><span class="Sprite1">Đơn vị tham gia</span></a></li>
		                  </ul>
		               	<div class="Clear"></div>   
		            </div>
	            		 <!-- Tab Danh sach Hinh thuc Ban Hang  -->
		            <div id="htbhCTTTTab">
	                    <h2 class="Title2Style">Thông tin tìm kiếm</h2>
	                    <div class="SearchInSection SProduct1Form">
	                        <label class="LabelStyle Label1Style">Mã HTBH</label>
	                        <input id="codeHTBH" type="text" class="InputTextStyle InputText1Style" />
	                        <label class="LabelStyle Label1Style">Tên HTBH</label>
	                        <input id="nameHTBH" type="text" class="InputTextStyle InputText2Style" />
	                        <div class="Clear"></div>
	                        <div class="BtnCenterSection">
	                            <button id="btnSearchSaleType" class="BtnGeneralStyle BtnSearch" onclick="BaryCentricProgrammeCatalog.searchHTBH();">Tìm kiếm</button>
	                        </div>
	                        <div class="Clear"></div>
	                    </div>
	                    <p id="errMsgTmp" class="ErrorMsgStyle" style="display:none" />
	                    <div class="Clear"></div>
	                    <h2 class="Title2Style" id="dsHTBH">Danh sách HTBH</h2>
	                    <div class="SearchInSection SProduct1Form">
	                        <div class="GridSection" id="gridContainerHTBH">
		           				<table id="gridHTBH"></table>
	                        </div>
<%-- 	                        <s:if test="roleType != null && roleType == 2"> --%>
	                        <div class="BtnCenterSection" >
	                            <button id="btnSaveHTBH" class="BtnGeneralStyle " onclick="BaryCentricProgrammeCatalog.saveHTBH();" style="display: none;">Cập nhật</button>
	                        </div>
	                        <div class="Clear"></div>
	                        <p id="errMsgHTBH" class="ErrorMsgStyle" style="display:none" />
	                        <p id="successMsgHTBH" class="SuccessMsgStyle" style="display: none"></p>
<%-- 	                        </s:if> --%>
	                    </div>
	                </div>
	                <!-- End Danh sach Hinh thuc Ban Hang  -->
	                <!-- Tab Danh sach San pham CTTT  -->
	         		<div id="spCTTTTab" style="display : none;">
	                   <h2 class="Title2Style">Thông tin tìm kiếm</h2>
	                   <div class="SearchInSection SProduct1Form">
	                       <label class="LabelStyle Label1Style">Mã sản phẩm</label>
	                       <input id="productCode" type="text" class="InputTextStyle InputText1Style" />
	                       <label class="LabelStyle Label1Style">Tên sản phẩm</label>
	                       <input id="productName" type="text" class="InputTextStyle InputText2Style" />
	                       <div class="Clear"></div>
	                       <div class="BtnCenterSection">
	                           <button id="btnSearchProduct" class="BtnGeneralStyle" onclick="return BaryCentricProgrammeCatalog.searchProductTab();">Tìm kiếm</button>
	                       </div>
	                       <div class="Clear"></div>
	                   </div>
	                   <div class="Clear"></div>
	                   <h2 class="Title2Style">Danh sách SPTT</h2>
	                   <div class="SearchInSection SProduct1Form">
	                       <div class="GridSection">
	                           <div class="ResultSection" id="productFocusGrid">
									<p id="gridProductNoResult" style="display: none" class="WarningResultStyle">Không có kết quả</p>
									<table id="grid"></table>
									<div id="pager"></div>
								</div>
								<div class="Clear"></div>
	                       </div>
	                       <p id="errMsgProduct" class="ErrorMsgStyle" style="display:none" />
	                       <p id="successMsgProduct" class="SuccessMsgStyle" style="display: none"></p> 
	                   </div>
	                </div>
	                <!-- End Danh sach San pham CTTT  -->
	                <!-- Tab Danh sach Don vi tham gia -->
	                <div id="dvtgCTTTTab" style="display :none;">
	                   <h2 class="Title2Style">Thông tin tìm kiếm</h2>
	                   <div class="SearchInSection SProduct1Form">
	                       <label class="LabelStyle Label1Style">Mã đơn vị</label>
	                       <input type="text" class="InputTextStyle InputText1Style" id="shopCode" />
	                       <label class="LabelStyle Label1Style">Tên đơn vị</label>
	                       <input type="text" class="InputTextStyle InputText7Style" id="shopName" />
	                       <div class="Clear"></div>
	                       <div class="BtnCenterSection">
	                           <button id="btnSearchShop" class="BtnGeneralStyle" onclick="return BaryCentricProgrammeCatalog.searchShop();">Tìm kiếm</button>
	                       </div>
	                       <div class="Clear"></div>
	                   </div>
	                   <div class="Clear"></div>
	                   <h2 class="Title2Style">Danh sách đơn vị</h2>
	                   <div class="SearchInSection SProduct1Form">
	                       <div class="GridSection">
	                           <table id="exGrid" class="easyui-treegrid"></table> 
	                       </div>
                       <div class="Clear"></div>
                   </div>
               </div>
               
               <!-- End Danh sach Don vi tham gia  -->
               <div class="SearchInSection SProduct1Form">
	                <div class="GeneralForm GeneralNoTP1Form">
	                 <s:if test="status == 2">
	                     <div id="idImportDiv" class="Func1Section" style="width : 560px;display: none;">
		                       <p class="DownloadSFileStyle DownloadSFile2Style">
	                           		<a href="javascript:void(0)" id="downloadTemplate" class="Sprite1">Tải file excel mẫu</a>
	                           </p>
	                           <form action="/catalog/bary-centric-programme/import-excel" name="importFrm" style="float: left;" id="importFrm"  method="post" enctype="multipart/form-data">
	                           		<input type="hidden" id="tokenImport" name="token" value='<s:property value="token"/>'>
			                      	<input type="hidden" id="isView" name="isView"/>
			                      	<input type="file" class="InputFileStyle" size="10" style="width:210px;" name="excelFile" id="excelFile" onchange="previewImportExcelFile(this,'importFrm');">
			                      	<div class="FakeInputFile">						
			                      			<input id="fakefilepc" style="margin: -3px 5px 0 0;" readonly="readonly" type="text" class="InputTextStyle InputText1Style">
									</div>
			                     </form>
		                       <button id="btnImport" class="BtnGeneralStyle" onclick="return BaryCentricProgrammeCatalog.importExcel();">Nhập từ Excel</button>
		                       <button id="btnExport" class="BtnGeneralStyle" onclick="return BaryCentricProgrammeCatalog.exportExcelData();">Xuất Excel</button>
		                       <div class="Clear"></div>
	                    </div>
	                    </s:if>
	                    <s:else>
	                    	<div id="idImportDiv" class="Func1Section" style="width : 130px;display: none;">   
		                       <button id="btnExport" class="BtnGeneralStyle" onclick="return BaryCentricProgrammeCatalog.exportExcelData();">Xuất Excel</button>
		                       <div class="Clear"></div>
	                    	</div>
	                    </s:else>
	                    <div class="Clear"></div>
	                    <p id="errExcelMsg" class="ErrorMsgStyle" style="display: none;margin-top:10px;margin-left: 10px;padding: 2px 0 5px 9px;"></p>
	                 </div>
                </div>
<%--                 <tiles:insertTemplate template="/WEB-INF/jsp/general/successMsg.jsp" /> --%>
					<p id="successMsg2" class="SuccessMsgStyle" style="display: none"></p>
	        </div>
         </div>
         <div class="Clear"></div>
        <p style="display:none;margin-top:10px;margin-left: 10px" class="ErrorMsgStyle" id="errMsg"/>
        </div>
    </div>
    <div class="Clear"></div>
</div>
<div id="responseDiv" style="display:none;"></div>
<s:hidden id="excelType" name="excelType"></s:hidden>
<s:hidden id="selId" name="id"></s:hidden>
<s:hidden id="subContentId" name="subContentId"></s:hidden>
<s:hidden id="selShopMapId" value="0"></s:hidden>
<s:hidden id="selChannelMapId" value="0"></s:hidden> 
<s:hidden id="tabInfo" value="1"></s:hidden>
<s:hidden id="aPer" name="activePermission"></s:hidden>
<s:hidden id="proStatus" name="status"></s:hidden>
<s:hidden id="proId" name="id"></s:hidden>
<s:hidden id="ePer" name="permissionEdit"></s:hidden>
<s:hidden id="csPer" name="commercialSupportPermission"></s:hidden>
<s:hidden id="hidStatus" name="status"></s:hidden>
<s:hidden id="tabOrder" name="tabOrder"></s:hidden>
<s:hidden id="progId" name="id"></s:hidden>
<s:hidden id="saleTypeCode"></s:hidden>
<s:hidden id="type"></s:hidden>
<input type="hidden" id="statusPermission" value="<s:property value="status"/>"/>
<s:hidden id="roleType" name="roleType"></s:hidden>
<s:hidden id="focusProgramId" name="id"></s:hidden>
<s:hidden id="statusHidden" name="status"></s:hidden>
<div style="display:none;width:100%" id="divWidth"></div>
<div id ="searchStyle1EasyUIDialogDivEx" style="width:600px;display: none" >
	<div id="searchStyle1EasyUIDialogEx" class="easyui-dialog" title="" data-options="closed:true,modal:true">
		<div class="PopupContentMid">
			<div class="GeneralForm Search1Form" >
				<label id="seachStyle1CodeLabel" class="LabelStyle Label1Style" style=" width: 100px;">Mã CTKM <span class="RequireStyle">*</span></label>
				<input id="seachStyle1Code" maxlength="47" tabindex="1" type="text" style=" width: 145px;" class="InputTextStyle InputText1Style" /> 
				<label id="seachStyle1NameLabel" class="LabelStyle Label1Style" style=" width: 100px;">Tên CTKM<span class="RequireStyle">*</span></label> 
				<input id="seachStyle1Name" type="text" maxlength="97" tabindex="2" style="width: 145px;"  class="InputTextStyle InputText1Style" />
				<div class="Clear"></div>
				<input type="hidden" name="" id="searchStyle1Url"/>
				<input type="hidden" name="" id="searchStyle1CodeText"/>
				<input type="hidden" name="" id="searchStyle1NameText"/>
				<input type="hidden" name="" id="searchStyle1IdText"/>
				<s:hidden id="searchStyle1AddressText"></s:hidden>
				<div class="Clear"></div>
				<div class="BtnCenterSection">
					<button id="__btnSaveFocusProgram" class="BtnGeneralStyle" >Sao chép</button>
					<button class="BtnGeneralStyle BtnGeneralMStyle" onclick="$('#searchStyle1EasyUIDialogEx').dialog('close');">Đóng</button>
				</div>
				<p id="errMsgSearch" style="display: none;" class="ErrorMsgStyle SpriteErr"></p>
				<p id="successMsgInfo" class="SuccessMsgStyle" style="display:none;">Lưu dữ liệu thành công.</p>
			</div>
		</div>
	</div>
</div>
<div id="searchStyleShopFocus" style="display:none;">
	<div id="searchStyleShopFocus1" class="easyui-dialog" title="" data-options="closed:true,modal:true">
	 <div class="PopupContentMid2">
        	<div class="GeneralForm Search1Form">
	        	<h2 class="Title2Style">Thông tin tìm kiếm</h2>                                
                   <label id="seachStyleShopCodeLabel" class="LabelStyle Label2Style">Mã</label>
                   <input id="seachStyleShopCode" maxlength="50" tabindex="1" type="text" class="InputTextStyle InputText5Style" />
                   <label id="seachStyleShopNameLabel" class="LabelStyle Label2Style">Tên</label>
                   <input id="seachStyleShopName" type="text" maxlength="250" tabindex="2" class="InputTextStyle InputText5Style" />
                   <div class="Clear"></div>
                   <div class="BtnCenterSection">
                       <button class="BtnGeneralStyle Sprite2" tabindex="4" id="btnSearchStyleShop"><span class="Sprite2">Tìm kiếm</span></button>
                   </div>
                   <s:hidden id="searchStyleShopUrl"></s:hidden>
                   <s:hidden id="searchStyleShopCodeText"></s:hidden>
                   <s:hidden id="searchStyleShopNameText"></s:hidden>
            </div>
            <div class="GeneralForm Search1Form">
            	<h2 class="Title2Style Title2MTStyle">Danh sách đơn vị</h2>
                <div class="GridSection" id="searchStyleShopContainerGrid">                                 
                     <table id="searchStyleShopGrid" class="easyui-treegrid"></table>
                 </div>
                <div class="Clear"></div>
                <div class="BtnCenterSection">
	                <s:if test="headOfficePermission">
	                    <button id="btnSaveShopMap" onclick="return BaryCentricProgrammeCatalog.createFocusShopMap();" class="BtnGeneralStyle">Chọn</button>
                    </s:if>
					<button class="BtnGeneralStyle" onclick="return $('#searchStyleShopFocus1').dialog('close');">Đóng</button>
                </div>
                <p id="errMsgDialog" class="ErrorMsgStyle" style="display: none"></p>
            </div>
		</div>
	</div>
 </div>  
<div id="divDialogSearch" style="display: none;">	 
	<div id="productEasyUIDialog" class="easyui-dialog" title="" data-options="closed:true,modal:true" style="width: 700px;">
       <div class="PopupContentMid2">
       		<div class="GeneralForm Search1Form">
           	<h2 class="Title2Style">Thông tin tìm kiếm<img id="loadingDialog" src="/resources/scripts/plugins/jquery-easyui-1.3.2/themes/default/images/loading.gif"></h2>
               <label class="LabelStyle Label2Style">Mã sản phẩm</label>
               <input id="productCodeDlg" type="text" class="InputTextStyle InputText5Style" style="padding-left: 10px"/>
               <label class="LabelStyle Label2Style">Tên sản phẩm</label>
               <input id="productNameDlg" type="text" class="InputTextStyle InputText5Style"  style="padding-right: 90px" />
               <div class="Clear"></div>
               <div class="BtnCenterSection">
                   <button id="btnSeachProductTree" class="BtnGeneralStyle BtnSearchOnDialog"  onclick="return BaryCentricProgrammeCatalog.searchProductTree();">Tìm kiếm</button>
               </div>
               <div class="Clear"></div>
           </div>
           <div class="GeneralForm Search1Form">
           	<h2 class="Title2Style Title2MTStyle">Cây sản phẩm</h2>
           	<div  style="max-height:300px;overflow:auto;">
                <div class="ReportTreeSection ReportTree2Section" >
                      <ul id="tree" class="easyui-tree" style="visibility:hidden;"></ul>
                </div>
               </div>
               <div class="Clear"></div>
               <label class="LabelStyle Label1Style" id="labelPromotionType">Loại HTBH<span class="ReqiureStyle">(*)</span></label>
               <div class="BoxSelect BoxSelect2">
				   <select id="saleTypeCodeDlg" >
			       		<option value="-1">---Chọn loại HTBH---</option>
			       		<s:iterator value="lstFocusChannelMap">
			       			<option value="<s:property value="id"/>"><s:property value="saleTypeCode"/></option>
			       		</s:iterator>
			       </select>
				</div>
				<div class="Clear"></div>
				<label class="LabelStyle Label1Style" id="labelPromotionType">Loại MHTT<span class="ReqiureStyle">(*)</span></label>
				<div class="BoxSelect BoxSelect2">
				   <select id="typeDlg">
			       		<option value="-1">---Chọn loại MHTT---</option>
			       		<s:iterator value="lstType">
			       			<option value="<s:property value="id"/>"><s:property value="apParamCode"/>-<s:property value="apParamName"/></option>
			       		</s:iterator>
			       </select>
				</div>
				<div class="Clear"></div>
               	<div class="BtnCenterSection">
                   <button id="btnChangeDlg" class="BtnGeneralStyle" onclick="return BaryCentricProgrammeCatalog.changeProductTree();">Chọn</button>
                   <button class="BtnGeneralStyle" onclick="$('.easyui-dialog').dialog('close');">Đóng</button>
               </div>
               <div class="Clear"></div>
               <p id="errMsgDlg"  class="ErrorMsgStyle" style="display: none;"></p>
           </div>
		</div>
   </div>
</div> 
<input type="hidden" id="solg" value=""/>
<input type="hidden" id="width" value=""/>
<script type="text/javascript">
$(document).ready(function(){
	$('#code').focus();
	$('.MySelectBoxClass').customStyle();
	BaryCentricProgrammeCatalog._listShopId = new Array();
	applyDateTimePicker("#fDate");
	applyDateTimePicker("#tDate");
	$('#code').bind('keyup', function(event){
		if(event.keyCode == 13){
			BaryCentricProgrammeCatalog.saveInfo(); 
		}
	});
	$('#name').bind('keyup', function(event){
		if(event.keyCode == 13){
			BaryCentricProgrammeCatalog.searchShop(); 
		}
	});
	$('#shopCode').bind('keyup', function(event){
		if(event.keyCode == 13){
			BaryCentricProgrammeCatalog.searchShop(); 
		}
	});
	$('#shopName').bind('keyup', function(event){
		if(event.keyCode == 13){
			BaryCentricProgrammeCatalog.searchShop(); 
		}
	});
	if($('#selId').val().trim() > 0 ){
// 		if($('#subContentId').val() == 1){
			$('#subContent').show();
			if(BaryCentricProgrammeCatalog._firstCount){
		    	BaryCentricProgrammeCatalog.getJsonListSaleType();
		    	BaryCentricProgrammeCatalog.getJsonListType();
		    	BaryCentricProgrammeCatalog._firstCount = false;
		    }
// 		}else{
// 			$('#subContent').hide();
// 		}
		AttributeUtil.disabledAttributes();	
		//if($('#status').val().trim() != activeType.STOPPED){
			BaryCentricProgrammeCatalog.showOthersTab();
			$('#name').focus();
		//}
// 		if($.cookie('proTabOrder') != undefined || $.cookie('proTabOrder') != null){
// 			BaryCentricProgrammeCatalog.showTab2();	
// 			$.cookie('proTabOrder', null);
// 		}	
		disabled('code');
		if(($('#status').val().trim() == activeType.RUNNING)){
			$('#name').attr('disabled',true);
			$('#code').attr('disabled',true);
			$('#fDate').attr('disabled',true);
			$('#fDate').next().unbind('click');
		} 
		if(($('#status').val().trim() == activeType.STOPPED)){
			disableSelectbox('status');	
			$('#name').attr('disabled',true);
			$('#code').attr('disabled',true);
			$('#fDate').attr('disabled',true);
			$('#tDate').attr('disabled',true);
			$('#fDate').next().unbind('click');
			$('#tDate').next().unbind('click');
		} 
		if($('#status').val().trim() == activeType.STOPPED){
			$('#btnUpdate').hide();
		}
		if($('#status').val().trim() == activeType.WAITING){
			$('#btnSaveHTBH').show();
// 			$('#idImportDiv').show();
		}
		if($('#aPer').val().trim() == 'false' && $('#status').val().trim() == activeType.WAITING){
			disableSelectbox('status');
			$('#status').change();
		}
		if($('#aPer').val().trim() == 'true' && $('#csPer').val().trim() == 'false'){
			if(($('#status').val().trim() == activeType.RUNNING || $('#status').val().trim() == activeType.STOPPED)){
				$('#status option[value='+ activeType.WAITING +']').remove();			
				$('#status').change();	
			} 
		}
		if($('#csPer').val().trim() =='true' && $('#status').val().trim() == activeType.WAITING){
			AttributeUtil.enabledAttributes();
		}
	}else {
		setSelectBoxValue('status', activeType.WAITING);
		disableSelectbox('status');
	} 
	if($('#aPer').val() == 'true' && $('#csPer').val().trim() == 'false') {//httm_active
		if($('#proStatus').val() == activeType.WAITING || $('#proStatus').val() == activeType.STOPPED) {
			//chi dc view
			disabled('fDate');
			$('#fDate').next().unbind('click');
			disabled('tDate');
			$('#tDate').next().unbind('click');
		} else if($('#proStatus').val() == activeType.RUNNING) {
			disabled('fDate');
			$('#fDate').next().unbind('click');
			if($('#tDate').val().trim().length > 0) {
				$('#tDate').change(function() {
					var tDate = $('#tDate').val();
					var currentTime = new Date();
					var month = currentTime.getMonth() + 1;
					var day = currentTime.getDate();
					var year = currentTime.getFullYear();	
					if($('#aPer').val().trim() == 'true' && !Utils.compareDate(day + '/' + month + '/' + year,tDate)){
						msg = 'Ngày kết thúc phải lớn hơn hoặc bằng ngày hiện tại. Bạn không thể thay đổi';
						$('#tDate').val($('#stableToDate').val());
						$('#errMsg').html(msg).show();
						var tm = setTimeout(function() {
							$('#errMsg').hide();
						}, 3000);
					}
				});
			}
		}
	} else if($('#proId').val().trim() > 0) {
	}
	BaryCentricProgrammeCatalog.loadHTBH();
	Utils.bindAutoButtonEx('.ContentSection','btnSearchSaleType');
	var userHo = '<s:property value="roleType" />';
	 if(userHo == '3'){
		$('#tDate').attr('disabled',true);
		$('#tDate').next().unbind('click');
		$('#btnUpdate').hide();
		$('#btnCopy').hide();
		$('#tab3').hide();
		disableSelectbox('status');
	}
});
</script>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@page import="ths.dms.helper.Configuration"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<div class="BreadcrumbSection">
    <ul class="ResetList FixFloat BreadcrumbList">
        <li class="Sprite1"><a href="#">Danh mục</a></li>
        <li><span>Danh sách chương trình trọng tâm</span></li>
    </ul>
</div>
<div class="CtnOneColSection">
    <div class="ContentSection">
    	<div class="ToolBarSection">
            <div class="SearchSection GeneralSSection">
                <h2 class="Title2Style">Thông tin tìm kiếm</h2>
                <div class="SearchInSection SProduct1Form">
                    <label class="LabelStyle Label1Style">Mã CTTT</label>
                    <input id="code" type="text" class="InputTextStyle InputText1Style" maxlength="20"/>
                    <label class="LabelStyle Label1Style">Tên CTTT</label>
                    <input id="name" type="text" maxlength="250" class="InputTextStyle InputText1Style" style="white-space:pre;" />
                    <label id="lblUnitFocus" class="LabelStyle Label1Style">Đơn vị</label>
                    <input type="text" class="InputTextStyle InputText1Style" id="shopTree" style="width:182px;"/>
                    <div class="Clear"></div>
                    <label class="LabelStyle Label1Style">Từ ngày</label>
                    <input type="text" id="fDate" class="InputTextStyle InputText6Style" />
                    <label class="LabelStyle Label1Style">Đến ngày</label>
                    <input type="text" id="tDate" class="InputTextStyle InputText6Style" />
                    <s:if test="headOfficePermission">
	                    <label class="LabelStyle Label1Style">Trạng thái</label>
	                    <div class="BoxSelect BoxSelect2">
	                        <s:select id="status" name="status" cssClass="MySelectBoxClass" list="lstStatusBeanWithWaiting" listKey="value" listValue="name" headerValue="--Chọn trạng thái--" headerKey=""></s:select>
	                    </div>
                    </s:if>
                    <div class="Clear"></div>
                    <div class="BtnCenterSection">
                        <button id="btnSearch" onclick="return FocusProgram.search()" class="BtnGeneralStyle">Tìm kiếm</button>
                    </div>
                    <p style="display:none;" class="ErrorMsgStyle" id="errMsg"/>
                    <div class="Clear"></div>
                    </div>
                    <h2 class="Title2Style">Danh sách CTTT</h2>
            	<div class="GridSection">
                	<table id="listCTTTGrid" title="Danh sách CTTT" class="easyui-datagrid"></table>
                </div>
                <div class="GeneralForm GeneralNoTP1Form">
                <div class="Clear"></div>
          		</div>
                <div class="Clear"></div>
            </div>
            <div class="Clear"></div>
        </div>
    </div>
    <div class="Clear"></div>
</div>
<s:hidden id="selId" value="0"></s:hidden>
<s:hidden id="aPer" name="activePermission"></s:hidden>
<s:hidden id="csPer" name="commercialSupportPermission"></s:hidden>
<input type="hidden" id="shopId"/>

<script type="text/javascript">
$(document).ready(function(){	
	$('#code').focus();
	$('#shopName').val('');
	$('.MySelectBoxClass').customStyle();
	$('#status').width(182);
	$('.BoxSelect .CustomStyleSelectBoxInner').width(150);
	setDateTimePicker('fDate');
	setDateTimePicker('tDate');
	//Utils.loadComboShopTree('shopTree', 'shopId');
	var userHO = '<s:property value="userHo" />';
	if(userHO=='1'){
		$('#userTemp').val(userHO);
		$('#lblUnitFocus').hide();
		$('#shopTree').hide();
		
	}else{
		 TreeUtils.loadComboTreeShopHasTitle('shopTree', 'shopId',function(data) {}, false);
	}
	var params = new Object();
	params.code = $('#code').val().trim();
	params.name = $('#name').val().trim();
	params.shopId = $('#shopId').val().trim();
	params.fDate = $('#fDate').val();
	params.tDate = $('#tDate').val();	
	params.status = 1;
	var title = "";
	if($('#permissionUser').val() == 'true'){
		title = '<a title="Thêm mới" href="javascript:void(0)" onclick=\"return FocusProgram.getSelectedProgram(0);\"><img src="/resources/images/icon_add.png"/></a>';
	}
	$('#listCTTTGrid').datagrid({
		//url: '/focus-program/search',
		url: FocusProgram.getGridUrl('','',-2,'','',1),
		width : $(window).width() - 35,
		pagination:true,
		scrollbarSize:0,
		checkOnSelect :true,
		autoWidth: true,
		fitColumns : true,
		rownumbers : true,
		singleSelect:true,
		columns:[[  
		          {field:'focusProgramCode',title:'Mã CTTT',width:200, sortable : false, resizable : false, align: 'left', formatter:function(value,row,index){
		        	  return Utils.XSSEncode(value);
		          }},  
		          {field:'focusProgramName',title:'Tên CTTT', width:$(window).width() - 70 - 200 - 80 - 80 - 80 - 30,sortable:false,resizable:false, align: 'left',formatter:function(value,row,index){
		        	  return '<p style="white-space:pre;">'+Utils.XSSEncode(row.focusProgramName)+'</p>';
		          }},  
		          {field:'fromDate',title:'Từ ngày',width:80, sortable:false, align: 'center',resizable:false , formatter:function(value, row, index) {
		        	  return toDateString(new Date(row.fromDate));
		          }},
		          {field:'toDate',title:'Đến ngày',width:80, sortable:false, align: 'center',resizable:false , formatter:function(value, row, index) {
		        	  var toDate = formatDate(row.toDate);
			  	    	if(toDate == null || toDate == undefined || toDate == '' || toDate == 'null'){
			  	    		toDate = '';
			  	    	}
			  	    	return toDate;
		          }},
		          {field:'status',title:'Trạng thái', width:82,sortable:false,resizable:false, align: 'center', formatter: function(value,row,index){
					if(row.status == activeStatus){
		      			return "Hoạt động";
		      		} else if(row.status == stoppedStatus){
		      			return "Tạm ngưng";
		      		} else if(row.status == waitingStatus){
		      			return "Dự thảo";
		      		}
		          }},
		          {field:'detail',title:title, width: 30,sortable:false,resizable:false, align: 'center', formatter: function(value, row, index) {
		        	  return "<a title='Xem' href='javascript:void(0)' onclick=\"return FocusProgram.getSelectedProgram('"+ row.id +"');\"><img width='15' height='15' src='/resources/images/icon-edit.png'/></a>";
		          }},
		          {field: 'P', hidden : true}
		]],
	  	onLoadSuccess :function(data){	    	
	    	$('.datagrid-header-rownumber').html('STT');	    	
	    },
	    method : 'GET'
	});
	$('#shopCode').bind('keyup', function(event){
		if(event.keyCode == keyCode_F9){
			var lstParam = new Array();
			var obj = {};
			obj.name = 'filterShop';
			obj.value = 1;
			lstParam.push(obj);
			CommonSearch.searchShopOnDialog(function(data){
				$('#shopCode').val(data.code);				
			},lstParam);
		}
	});
});
if($('#csPer').val().trim() == 'true'){
	$('#downloadTemplate').attr('href',excel_template_path + 'catalog/Bieu_mau_danh_muc_chuong_trinh_trong_tam.xls');
	$('#importFrm').ajaxForm(options);
}
$('#shopCode').bind('keyup', function(event){
	if(event.keyCode == keyCode_F9){
		CommonSearch.searchShopOnDialog(function(data){
			$('#shopCode').val(data.code);
			$('#shopName').val(data.name);
		});
	}
});
function excelTypeChanged(){
	if($('#excelType').val() == 1){
		$('#downloadTemplate').attr('href',excel_template_path + 'catalog/Bieu_mau_danh_muc_chuong_trinh_trong_tam.xls');
	} else if($('#excelType').val() == 2){
		$('#downloadTemplate').attr('href',excel_template_path + 'catalog/Bieu_mau_danh_muc_cttt_donvi.xls');
	} else {
		$('#downloadTemplate').attr('href',excel_template_path + 'catalog/Bieu_mau_danh_muc_cttt_HTBH_TTSP.xls');
	}
	$('#excelFile').val('');
	$('#fakefilepc').val('');
}
</script>
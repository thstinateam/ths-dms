<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@page import="ths.dms.helper.Configuration"%>
<%@ taglib prefix="s" uri="/struts-tags"%>

<div class="BreadcrumbSection">
	<ul class="ResetList FixFloat BreadcrumbList">
		<li class="Sprite1"><a href="javascript:void(0)">Chương trình trưng bày</a></li>
		<li><span>Danh sách CTTB trả thưởng</span>
		</li>
	</ul>
</div>

<div class="CtnOneColSection">
	<div class="ContentSection">
		<div class="ToolBarSection">
			<div class="SearchSection GeneralSSection">
				<h2 class="Title2Style">Thông tin tìm kiếm</h2>
				<div class="SearchInSection SProduct1Form">
					<label class="LabelStyle Label1Style">Mã CTTB</label>
					<input id="code" type="text" class="InputTextStyle InputText1Style" maxlength="50" />
					<label class="LabelStyle Label1Style">Tên CTTB</label>
					<input id="name" type="text" maxlength="100" style="width:196px;" class="InputTextStyle" />
					<label class="LabelStyle Label1Style" style="padding-left: 10px; width:105px">Trạng thái</label>
					<div class="BoxSelect BoxSelect6">
						<%-- <s:select id="status" name="status" cssClass="MySelectBoxClass" list="st" listKey="value" listValue="name" headerValue="--Tất cả--" headerKey=""></s:select> --%>
						<select id="status" class="MySelectBoxClass">
							<option value="-2">Tất cả</option>
							<option value="1" selected="selected">Hoạt động</option>
							<option value="0">Tạm ngưng</option>
						</select>
					</div>
					<div class="Clear"></div>
					<label class="LabelStyle Label1Style">Từ ngày</label>
					<input type="text" id="fDate" class="InputTextStyle InputText6Style" />
					<label class="LabelStyle Label1Style">Đến ngày</label>
					<input type="text" id="tDate" class="InputTextStyle InputText6Style" />
					<div class="Clear"></div>
					<div class="BtnCenterSection">
						<button id="btnSearch" onclick="ProgrammeDisplayCatalog.searchVNM()" class="BtnGeneralStyle">Tìm kiếm</button>
					</div>
					<p style="display: none;" class="ErrorMsgStyle" id="errMsg" />
					<div class="Clear"></div>
				</div>
				<h2 class="Title2Style">Danh sách CTTB trả thưởng</h2>
				<div class="GridSection">
					<table id="listCTTBGridVNM" title="Danh sách CTTB trả thưởng" fitColumns="true" data-options="autoRowHeight:false, pagination:true, pageSize:10"></table>
				</div>
				<div class="GeneralForm GeneralNoTP1Form">
					<div class="Clear"></div>
				</div>
				<div class="Clear"></div>
			</div>
			<div class="Clear"></div>
		</div>
	</div>
	<div class="Clear"></div>
</div>

<s:hidden id="selId" value="0"></s:hidden>
<input type="hidden" id="shopId" />

<script type="text/javascript">
$(document).ready(function(){
	$('#code').focus();
	$('#status').width(165);
	$('.BoxSelect .CustomStyleSelectBoxInner').width(135);
	setDateTimePicker('fDate');
	ReportUtils.setCurrentDateForCalendar('fDate');
	setDateTimePicker('tDate');
	ReportUtils.setCurrentDateForCalendar('tDate');
	
	var params = {};
	params.code = $('#code').val().trim();
	params.name = $('#name').val().trim();
	params.fromDate = $('#fDate').val();
	params.toDate = $('#tDate').val();	
	params.status = 1;
	
	$('#listCTTBGridVNM').datagrid({
		url : '/programme-display/searchvnm',
		//method : 'GET',
		height: 'auto',
		width : $(window).width() - 35,
		scrollbarSize:0,
		rownumbers : true,	
		fitColumns:true,
		pagination:true,
		rowNum : 10,		
		pageList  : [10,20,50],
		rownumbers:true,
		singleSelect : true,
		queryParams:params,
		columns:[[  
				{field:'displayProgramCode', title:'Mã CTTB', align:'left', width: 200,resizable:true ,formatter :function(value,row,index) {
					return Utils.XSSEncode(value);
				}},
				{field:'displayProgramName', title:'Tên CTTB', align:'left', width: 350,formatter :function(value,row,index) {
					return Utils.XSSEncode(value);
				}},
				{field:'fromDate',title:'Từ ngày',width:150, sortable:false, align: 'center',resizable:false , formatter:function(value, row, index) {
					var fromDate = row.fromDate;
					if(fromDate == null || fromDate == undefined || fromDate == '' || fromDate == 'null'){
						fromDate = '';
					}else{
						fromDate = formatDate(Utils.XSSEncode(row.fromDate));
					}
					return fromDate;
					  
				}},
				{field:'toDate',title:'Đến ngày',width:150, sortable:false, align: 'center',resizable:false , formatter:function(value, row, index) {
					  var toDate = row.toDate;
				    	if(toDate == null || toDate == undefined || toDate == '' || toDate == 'null'){
				    		toDate = '';
				    	}else{
				    		toDate = formatDate(Utils.XSSEncode(row.toDate));
				    	}
				    	return toDate;
				}},
				{field:'status',title:'Trạng thái', width:150,sortable:false,resizable:false, align: 'center', formatter: function(value,row,index){
					if(Utils.XSSEncode(row.status) == activeStatus){
						return "Hoạt động";
					} else if(Utils.XSSEncode(row.status) == stoppedStatus){
						return "Tạm ngưng";
					} 
				}},
		]],
	  	onLoadSuccess :function(data){	    	
	    	$('.datagrid-header-rownumber').html('STT');	    	
	    },
	});
});

</script>
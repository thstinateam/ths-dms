<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<div class="BreadcrumbSection">
	<ul class="ResetList FixFloat BreadcrumbList">		
		<li class="Sprite1"><a href="/programme-display/info">Chương trình trưng bày</a></li>
		<li><span>Thông tin chương trình trưng bày</span></li>
	</ul>
</div>
<div class="CtnOneColSection">
	<div class="ContentSection">
		<div class="ToolBarSection">
			<div class="SearchSection GeneralSSection">
				<h2 class="Title2Style">Thông tin CTTB</h2>
				<div class="SearchInSection SProduct1Form">
					<label class="LabelStyle Label1Style">Mã CTTB<span class="ReqiureStyle">*</span></label>
					<s:if test="id != null && id >0">						
						<input value="<s:property value="code" />"  id="code" name="code" maxlength="50" type="text" class="InputTextStyle InputText1Style"  disabled="disabled" />						
					</s:if>
					<s:else>
						<input value="<s:property value="code" />"  id="code" name="code" maxlength="50" type="text" class="InputTextStyle InputText1Style"   />
					</s:else>

					<label class="LabelStyle Label1Style">Tên CTTB<span class="ReqiureStyle">*</span></label>
					<s:if test="id != null && id > 0">
						<s:if test="status == 2">
							<s:textfield id="name" name="name" cssStyle="width:538px;"	cssClass="InputTextStyle InputText7Style" maxlength="100"></s:textfield>
						</s:if>
						<s:if test="status == 1 || status == 0">
							<s:textfield id="name" name="name" cssStyle="width:538px;"	cssClass="InputTextStyle InputText7Style" disabled="true"	maxlength="100"></s:textfield>
						</s:if>
					</s:if>
					<s:else>
						<s:textfield id="name" name="name" cssStyle="width:538px;"	cssClass="InputTextStyle InputText7Style" maxlength="100"></s:textfield>
					</s:else>
					<div class="Clear"></div>
					<label class="LabelStyle Label1Style">Từ ngày<span	class="ReqiureStyle">*</span>
					</label>
					<div id="fromDateDiv">
						<s:if test="id != null && id >0">
							<s:if test="status == 0 || status == 1">
								<input type="text" id="fDate" class="InputTextStyle InputText6Style" disabled="disabled" value="<s:property value="fromDate"/>" />
							</s:if>
							<s:else>
								<input type="text" id="fDate" class="InputTextStyle InputText6Style" value="<s:property value="fromDate"/>" />
							</s:else>
						</s:if>
						<s:if test="id == null || id == 0">
							<input type="text" id="fDate" class="InputTextStyle InputText6Style" value="<s:property value="fromDate"/>" />
						</s:if>
					</div>
					<label class="LabelStyle Label1Style">Đến ngày</label>
					<div id="toDateDiv">
						<s:if test="id != null && id >0">
							<s:if test="status == 0">
								<input type="text" id="tDate" class="InputTextStyle InputText6Style" disabled="disabled" value="<s:property value="toDate"/>" />
							</s:if>
							<s:else>
								<input type="text" id="tDate" class="InputTextStyle InputText6Style" value="<s:property value="toDate"/>" />
							</s:else>
						</s:if>
						<s:if test="id == null || id == 0">
							<input type="text" id="tDate"
								class="InputTextStyle InputText6Style"
								value="<s:property value="toDate"/>" />
						</s:if>
					</div>
					<label class="LabelStyle Label1Style">Trạng	thái<span class="ReqiureStyle">*</span> </label>
					<s:if test="id != null && id >0">
						<s:if test="status == 2">
							<div class="BoxSelect BoxSelect2">
								<select id="status" class="MySelectBoxClass">
									<option value="1">Hoạt động</option>
									<!-- <option value="0">Tạm ngưng</option> loctt-26sep2013 fix bug 0010986-->
									<option value="2" selected="selected">Dự thảo</option>
								</select>
							</div>
						</s:if>
						<s:if test="status == 1">
							<div class="BoxSelect BoxSelect2">
								<select id="status" class="MySelectBoxClass">
									<option value="1" selected="selected">Hoạt động</option>
									<option value="0">Tạm ngưng</option>
								</select>
							</div>
						</s:if>
						<s:if test="status == 0">
							<div class="BoxSelect BoxSelect2 BoxDisSelect">
								<select id="status" class="MySelectBoxClass" disabled="disabled">
									<option value="0">Tạm ngưng</option>
								</select>
							</div>
						</s:if>
					</s:if>
					<s:if test="id == null || id == 0">
						<div class="BoxSelect BoxSelect2 BoxDisSelect" >
							<select id="status" disabled="disabled" class="MySelectBoxClass ">
								<option value="2" >Dự thảo</option>
							</select>
						</div>
					</s:if>					

	                <div class="Clear"></div>
	                
	                 <div class="Clear"></div>
	                
	                <s:if test="headOfficePermission">
		                <div class="BtnCenterSection">
		                	<s:if test="id == null || id == 0">
		                		<button id="btnUpdate" class="BtnGeneralStyle" onclick="ProgrammeDisplayCatalog.saveInfo();">Lưu</button>
		                	</s:if>
						<!--loctt 26sep2013: bug 0010988 : cho phep sao chep CTTB Tam ngung-->
		                	<s:if test="id != null && id >0">
		                		<s:if test="status != 0">
			                		<button id="btnUpdate" class="BtnGeneralStyle" onclick="ProgrammeDisplayCatalog.saveInfo();">Cập nhật</button>
			                	</s:if>
			                		<button id="btnCopy" class="BtnGeneralStyle" onclick="ProgrammeDisplayCatalog.openCopyFocusProgram();">Sao chép</button>
		                	</s:if>
		                		                	
		                </div>
	                </s:if> 
	                 <p id="errMsg" class="ErrorMsgStyle" style="display: none"></p>
	                 <p id="successMsgInfo" class="SuccessMsgStyle" style="display: none"></p>
  					</div>
  				<div id = "subContentEx" style="display: none;">
		           	<div class="TabSection">		                
		               	<div class="Clear"></div>   
			       	</div>   
					<div class="Clear"></div>
					<s:if test="headOfficePermission">
						<div class="BtnCenterSection">
							<s:if test="id == null || id == 0">
								<button id="btnUpdate" class="BtnGeneralStyle"
									onclick="ProgrammeDisplayCatalog.saveInfo();">Lưu</button>
							</s:if>
							<s:if test="id != null && id >0 && status != 0">
								<button id="btnUpdate" class="BtnGeneralStyle"	onclick="ProgrammeDisplayCatalog.saveInfo();">Cập nhật</button>
								<button id="btnCopy" class="BtnGeneralStyle" onclick="ProgrammeDisplayCatalog.openCopyFocusProgram();">Sao	chép</button>
							</s:if>

						</div>
					</s:if>
					<p id="errMsg" class="ErrorMsgStyle" style="display: none"></p>
					<p id="successMsgInfo" class="SuccessMsgStyle" style="display: none"></p>
				</div>
				<div id="subContent" style="display: none;">
					<div class="TabSection">
						<ul class="ResetList TabSectionList">
							<s:if test="staffRole == 8">
								<li><a href="javascript:void(0);" id="tab1"	onclick="ProgrammeDisplayCatalog.showExclusionProgram();" class="Sprite1 Active"><span class="Sprite1">Loại	trừ CTTB</span></a></li>
								<li><a href="javascript:void(0);" id="tab2"	onclick="ProgrammeDisplayCatalog.showProductTab();" class="Sprite1"><span class="Sprite1">Sản phẩm TB</span></a></li>
								<li><a href="javascript:void(0);" id="amountTab" onclick="ProgrammeDisplayCatalog.showAmountTab();" class="Sprite1"><span class="Sprite1">Sản phẩm DS</span></a></li>
								<li><a href="javascript:void(0);" id="tab3" onclick="ProgrammeDisplayCatalog.showLevelTab();" class="Sprite1"><span class="Sprite1">Mức CTTB</span></a></li>
								<li><a href="javascript:void(0);" id="tab4"	onclick="ProgrammeDisplayCatalog.showShopTab();" class="Sprite1"><span class="Sprite1">Đơn vị tham gia</span></a></li>
							</s:if>							
							<s:if test="status!=2">
								<li><a href="javascript:void(0);" id="tab5"	onclick="ProgrammeDisplayCatalog.showStaffTab();" class="Sprite1"><span class="Sprite1 Active">Số suất NVBH</span></a></li>
								<li><a href="javascript:void(0);" id="tab10" onclick="ProgrammeDisplayCatalog.showCustomerTab1();"	class="Sprite1"><span class="Sprite1">Khách hàng</span></a></li>
							</s:if>
						</ul>
						<div class="Clear"></div>
					</div>					
		            <div id="container4" style="display: none">
	                   	<div class="Clear"></div>
	                   	<h2 class="Title2Style">Danh sách nhóm sản phẩm cho trưng bày</h2>                                                                  
	                   	<div class="SearchInSection SProduct1Form">
	                       	<div class="GridSection" id="skuGridContainer">
	                           	<table id="cbGrid"></table>
							</div>
						</div>
						<div class="Clear"></div>
						<p id="errMsgGroup" class="ErrorMsgStyle" style="display: none"></p>
						<p id="successMsgGroup" class="SuccessMsgStyle"
							style="display: none"></p>
					</div>					
					<div id="amountContainer" style="display: none">
			       		<h2 class="Title2Style">Danh sách nhóm sản phẩm cho doanh số</h2>                                                                  
	                   	<div class="SearchInSection SProduct1Form">
	                       	<div class="GridSection" id="amountGridContainer">
	                           	<table id="amountGrid"></table>
							</div>
						</div>
					</div>
					
					<!-- So suat NVBH -->								
					<div id="container5"  style="display: none">
<!-- 						<div class="SearchInSection"> -->
						<div class="SearchInSection SProduct1Form">
								<label class="LabelStyle Label1Style">Đơn vị</label>
							 	<div class="ComboTreeSection">
                                    <input id="shop" type="text"  style="width:226px;" class="InputTextStyle InputText1Style"/> 
                                </div>                               
                                <label class="LabelStyle Label1Style">Tháng</label>
                                <input type="text" class="InputTextStyle InputText1Style" id="month" >
                                <button onclick="ProgrammeDisplayCatalog.searchStaff();" style="margin-left: 100px;" class="BtnGeneralStyle">Tìm kiếm</button>
                                                                
                                <div class="Clear"></div>
								<div class="GridSection" id="staffGridContainer">
									<table id="staffGrid"></table>
									<div id="staffPager"></div>
								</div>	
						</div>
						<div class="Clear"></div>
						<p id="errMsgStaff" class="ErrorMsgStyle" style="display: none"></p>
						<p id="successMsgStaff" class="SuccessMsgStyle"	style="display: none"></p>						
					</div>	
					<!--  import nhan vien phat trien khach hang -->
					<div id = "responseDiv" style="display:none;"></div>
					<div style="display: none">
            		<div id="easyuiPopup" class="easyui-dialog" title="CTTB - Số suất NVBH" data-options="closed:true,modal:true" style="width:465px;height:250px;">
					        <div class="PopupContentMid">
					        	<div class="GeneralForm ImportExcel1Form">
					        		<form action="/programme-display/import-staff" name="importFrm" id="importFrm" method="post" enctype="multipart/form-data">						            	
						                <label class="LabelStyle Label1Style">File excel</label>
						                <div class="UploadFileSection Sprite1">
						                	<s:hidden id="selId" name="id"></s:hidden>
						                	<input id="fakefilepc" type="text" class="InputTextStyle InputText1Style">
						                    <input type="file" class="UploadFileStyle" size="1" name="excelFile" id="excelFile" onchange="previewImportExcelFile(this,'importFrm');" />						               		
						                </div>
									</form>
									<div class="Clear"></div>
									<p class="DownloadSFileStyle">
										<a onclick="" class="Sprite1 downloadTemplateNVBH" >Tải file excel mẫu</a>
									</p>									
					                <div class="Clear"></div>
					                <div class="BtnCenterSection">
					                    <button class="BtnGeneralStyle" onclick="return DisplayToolCustomer.upload();">Tải lên</button>
					                    <button class="BtnGeneralStyle BtnGeneralMStyle" onclick="$('#easyuiPopup').window('close');">Đóng</button>
					                </div>
					            </div>
							</div>
							<p style="display:none;margin-top:10px;margin-left: 10px" class="ErrorMsgStyle SpriteErr" id="errExcelMsg"/>
							<p style="display:none;margin-top:10px;margin-left: 10px" id="resultExcelMsg" class="ErrorMsgStyle SpriteErr"/>
				    </div>	</div>			
					<div id="container2">
						<h2 class="Title2Style">Thông tin tìm kiếm</h2>
						<div class="SearchInSection">
							<label class="LabelStyle Label1Style">Mã CTTB</label> <input
								type="text" class="InputTextStyle InputText1Style"
								id="displayProgramCode" maxlength="50" /> <label
								class="LabelStyle Label1Style">Tên CTTB</label> <input
								type="text" class="InputTextStyle InputText7Style"
								id="displayProgramName" maxlength="100" style="width: 500px;" />
								<button style="margin-left:10px;" id="btnSearchShop" class="BtnGeneralStyle"
									onclick="return ProgrammeDisplayCatalog.searchExclusionProgram();">Tìm
									kiếm</button>
							<div class="Clear"></div>
						</div>
						<div class="Clear"></div>
						<h2 class="Title2Style">Danh sách CTTB loại trừ</h2>
						<div class="SearchInSection SProduct1Form">
							<div class="GridSection">
								<table id="exclusionGrid" class="easyui-datagrid"></table>
							</div>
							<div class="Clear"></div>
							<p id="errMsgExclusion" style="display: none;"
								class="ErrorMsgStyle"></p>
							<p id="successMsgExclusion" class="SuccessMsgStyle"
								style="display: none"></p>
						</div>
					</div>
					<div id="container3" style="display: none">
						<h2 class="Title2Style">Danh sách mức của CTTB</h2>
						<div class="SearchInSection SProduct1Form">
							<div class="GridSection" id="levelGridContainer">
								<table id="levelGrid"></table>
								<div id="levelPager"></div>
							</div>
						</div>
					</div>
					
					<div id="container7" style="display: none;">
					<div class="SearchInSection SProduct1Form" style="padding-right:30px;padding-bottom:10px;">
						<h2 class="Title2Style">Thông tin tìm kiếm</h2>
						<div class="SearchInSection">
							<label class="LabelStyle Label1Style">Mã đơn vị</label> <input
								type="text" class="InputTextStyle InputText1Style" id="shopCode" />
							<label class="LabelStyle Label1Style">Tên đơn vị</label> <input
								type="text" class="InputTextStyle InputText7Style" id="shopName" style="width:500px;"/>
								<button style="margin-left:10px;" id="btnSearchShop" class="BtnGeneralStyle"
									onclick="return ProgrammeDisplayCatalog.searchShop();">Tìm
									kiếm</button>
							<div class="Clear"></div>
						</div>
						</div>
						<div class="Clear"></div>
						<h2 class="Title2Style">Danh sách đơn vị</h2>
						<div class="SearchInSection SProduct1Form" style="padding-right:30px;padding-bottom:10px;">
							<div class="Clear"></div>

							 <s:if test="staffRole==8">
								<div class="GeneralForm GeneralNoTP1Form">
									<s:if test="status == 2 || status == 1">
										<div id="idImportDiv" class="Func1Section" style="width: 660px;padding-right: 250px;">
											<p class="DownloadSFileStyle DownloadSFile2Style">
												<a href="javascript:void(0)" id="downloadTemplate" class="Sprite1">Tải file excel mẫu</a>
											</p>
											<form action="/programme-display/import-excel" name="importFrm" style="float: left;" id="importFrm"	method="post" enctype="multipart/form-data">												
												<input type="hidden" id="isView" name="isView" /> 
												<input type="file" class="InputFileStyle" size="10"	style="width: 170px;" name="excelFile" id="excelFile" onchange="previewImportExcelFile(this,'importFrm');">
												<div class="FakeInputFile">
													<input id="fakefilepc" style="margin: -3px 5px 0 0;" readonly="readonly" type="text" class="InputTextStyle InputText1Style">
												</div>
												<input type="hidden" name="token" value="<s:property value="token" />" id="importTokenVal"  />
											</form>
											<button id="btnImport" class="BtnGeneralStyle" onclick="return ProgrammeDisplayCatalog.importExcelShopMap();">Nhập từ Excel</button>
											<button id="btnExport" class="BtnGeneralStyle" onclick="return ProgrammeDisplayCatalog.exportExcelShopMap();">Xuất Excel</button>
											<s:if test="status == 2">
												<button class="BtnGeneralStyle" onclick="return ProgrammeDisplayCatalog.deleteDisplayProgramShopMap();">Xóa</button>
											</s:if>
											<div class="Clear"></div>
										</div>
									</s:if>
									<s:else>
										<div id="idImportDiv" class="Func1Section"
											style="width: 130px;">
											<button id="btnExport" class="BtnGeneralStyle"
												onclick="return ProgrammeDisplayCatalog.exportExcelShopMap();">Xuất
												Excel</button>
											<div class="Clear"></div>
										</div>
									</s:else>
									<div class="Clear"></div>
									<p id="errExcelMsg" class="ErrorMsgStyle"
										style="display: none; margin-top: 10px; margin-left: 10px; padding: 2px 0 5px 9px;"></p>
								</div>
						</s:if>
						<div class="Clear"></div>
							 							<p id="successMsgShop" class="SuccessMsgStyle"
								style="display: none"></p>
							 <!--  -->
							
							
							
							<div class="GridSection">
								<table id="exGrid" class="easyui-treegrid"></table>
							</div>
							<div class="Clear"></div>

						</div>						
					</div>
					
					<!-- Khach hang -->
					<div id="container10" style="display: none;">
<!-- 						<div class="ToolBarSection"> -->
<!--                         <div class="SearchSection GeneralSSection"> -->
				<div class="SearchInSection SProduct1Form">	
                            <div class="SearchInSection">
<!--                             <div class="SearchInSection"> -->
                                <label class="LabelStyle Label1Style">Đơn vị</label>
                                <div class="ComboTreeSection" id="shopTreeId">
	                            	<input id="shopTree" type="text"  style="width:226px;" class="InputTextStyle InputText1Style" />
	                            </div>                    
                                <label class="LabelStyle Label1Style">Tháng</label>
                                <input type="text" class="InputTextStyle InputText1Style" id="month1" maxlength="7"/>
                                <div class="BtnLSection">
                                    <button class="BtnGeneralStyle" style="margin-left: 100px;" onclick="return ProgrammeDisplayCatalog.searchCustomerTab();">Tìm kiếm</button>
                                </div>
                                 
                                <div class="Clear"></div>
                                
<!--                             </div> -->
                        </div>
                        </div>
                        <div class="Clear"></div>
<!--                     </div> -->
	                    <div class="SearchInSection SProduct1Form">
<!-- 	                    	<div class="GeneralForm GeneralNoTPForm"> -->
<!-- 	                        	<div class="Func1Section"> -->
<%-- 	                        		<s:if test="staffRole==2 && status == 1"> --%>
<!-- 		                        		<button class="BtnGeneralStyle" onclick= "return ProgrammeDisplayCatalog.openPopupImportCustomer();">Nhập từ Excel</button> -->
<!-- 		                        		<button class="BtnGeneralStyle"  onclick="return ProgrammeDisplayCatalog.exportExcelCTTB();">Xuất Excel</button> -->
<!-- 		                        		<p class="DownloadSFileStyle"><a id="downloadTemplateCustomer" href="#" class="Sprite1">Tải file excel mẫu</a></p> -->
<%-- 	                        		</s:if><s:else> --%>
<!-- 	                        			<button class="BtnGeneralStyle"  onclick="return ProgrammeDisplayCatalog.exportExcelCTTB();">Xuất Excel</button> -->
<%-- 	                        		</s:else> --%>
<!-- 	                            </div> -->
	                            
<%-- 								<s:if test="staffRole==2 && status == 1"> --%>
<!-- 									<label class="LabelStyle Label1Style" id="loadShop" style="width:450px;"></label>  -->
<!-- 									<label class="LabelStyle Label1Style" id="loadShop" style="width:100px;"> -->
<!-- 										<input type="checkbox" id="checkDeleteAll" onclick="return ProgrammeDisplayCatalog.uncheckAll();"/>Chọn toàn bộ  -->
<!-- 									</label> -->
<!-- 		                            <div class="Func2Section"> -->
<!-- 		                            	<button class="BtnGeneralStyle" onclick="return ProgrammeDisplayCatalog.deleteCustomerDisplayProgram();">Xóa</button> -->
<!-- 		                            </div> -->
<%-- 		                        </s:if> --%>
<!-- 	                            <div class="Clear"></div> -->
<!-- 	                             <p id="errCustomerMsg" class="ErrorMsgStyle" style="display: none"></p> -->
<!--                                  <p id="successCustomerMsg" class="SuccessMsgStyle" style="display: none"></p> -->
<!-- 	                        </div> -->
	                    	<div class="GridSection" id="gridCustomerGrid">
<!-- 		                    	<div class="ResultSection" id="customerDisplayProgramContainer" > -->
		                    	<table id="customerGrid123"></table>
<!-- 		                    	<div id="pager"></div> -->
<!-- 		                    	</div> -->
	                        	<!--Đặt Grid tại đây-->
	                        	
	                        </div>
	                    </div>
					</div>
					
					
					
				</div>
			</div>
		</div>
	</div>
</div>
<%-- </s:if> --%>
<div id="displayProgramExclusionDialog" style="display: none;">



	<div class="GeneralDialog General2Dialog">
		<div class="DialogProductSearch">
			<div class="GeneralMilkBox">
				<div class="GeneralMilkTopBox">
					<div class="GeneralMilkBtmBox">
						<h3 class="Sprite2">
							<span class="Sprite2">Thông tin tìm kiếm</span>
						</h3>
						<div class="GeneralMilkInBox ResearchSection">
							<div class="SearchSales4Form">
								<label class="LabelStyle Label1Style">Mã CTTB</label> <input
									id="vnmDisplayProgramCode" maxlength="50" type="text"
									class="InputTextStyle InputText1Style" /> <label
									class="LabelStyle Label2Style">Tên CTTB</label> <input
									id="vnmDisplayProgramName" type="text" maxlength="250"
									class="InputTextStyle InputText1Style" />
								<div class="Clear"></div>
								<div class="ButtonSection">
									<button
										onclick="ProgrammeDisplayCatalog.searchDisplayProgramExclusion();"
										class="BtnGeneralStyle Sprite2 BtnSearchOnDialog" tabindex="4"
										id="btnSearchStyle1">
										<span class="Sprite2">Tìm kiếm</span>
									</button>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="GeneralMilkBox">
				<div class="GeneralMilkTopBox">
					<div class="GeneralMilkBtmBox">
						<h3 class="Sprite2">
							<span class="Sprite2">Kết quả tìm kiếm</span>
						</h3>
						<div class="GeneralMilkInBox">
							<div id="searchStyle1ScrollBody" class="">
								<div class="ResultSection" id="vnmDisplayProgramContainerGrid">
									<table id="vnmDisplayProgramGird"></table>
									<div id="vnmDisplayProgramPager"></div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="BoxDialogBtm">
				<div class="ButtonSection">
					<button class="BtnGeneralStyle Sprite2"
						onclick="ProgrammeDisplayCatalog.selectListDisplayProgramExclusion();">
						<span class="Sprite2">Chọn</span>
					</button>
					<button
						class="BtnGeneralStyle BtnGeneralCStyle BtnGeneralMStyle Sprite2"
						onclick="$.fancybox.close();">
						<span class="Sprite2">Đóng</span>
					</button>
				</div>
				<div class="BoxDialogAlert">
					<p style="display: none" id="exclustionErr"
						class="ErrorMsgStyle Sprite1">Vui lòng nhập đúng kết quả</p>
				</div>
			</div>
		</div>
	</div>
</div>
<div id="divGroupContainer" style="display: none">
	<div id="dialogGroup" class="easyui-dialog" title="Thông tin nhóm"
		data-options="closed:true,modal:true" style="width: 600px;">
		<div class="PopupContentMid">
			<div class="GeneralForm Search1Form">
				<label class="LabelStyle Label1Style" style="width: 100px;">Mã
					nhóm <span class="RequireStyle">*</span> </label> <input id="groupCodeDlg"
					maxlength="47" tabindex="1" type="text" style="width: 145px;"
					class="InputTextStyle InputText1Style" /> <label
					class="LabelStyle Label1Style" style="width: 100px;">Tên
					nhóm<span class="RequireStyle">*</span> </label> <input id="groupNameDlg"
					type="text" maxlength="97" tabindex="2" style="width: 145px;"
					class="InputTextStyle InputText1Style" />
				<div class="Clear"></div>
				<div class="BtnCenterSection">
					<button id="btnChangeGroup" class="BtnGeneralStyle"
						onclick="return ProgrammeDisplayCatalog.changeGroup();">Cập
						nhật</button>
				</div>
				<p id="errMsgGroupDlg" style="display: none;"
					class="ErrorMsgStyle SpriteErr"></p>
				<p id="successMsgGroupDlg" style="display: none;"
					class="SuccessMsgStyle"></p>
				<s:hidden id="groupId"></s:hidden>
				<s:hidden id="typeGroup"></s:hidden>
			</div>
		</div>
	</div>
</div>
<div id="divGroupContainer2" style="display: none">
	<div id="dialogGroup2" class="easyui-dialog" title="Thông tin nhóm"
		data-options="closed:true,modal:true" style="width: 600px;">
		<div class="PopupContentMid">
			<div class="GeneralForm Search1Form">
				<label class="LabelStyle Label1Style" style="width: 100px;">Mã
					nhóm <span class="RequireStyle">*</span> </label> <input id="groupCodeDlg2"
					maxlength="47" tabindex="1" type="text" style="width: 145px;"
					class="InputTextStyle InputText1Style" /> <label
					class="LabelStyle Label1Style" style="width: 100px;">Tên
					nhóm<span class="RequireStyle">*</span> </label> <input id="groupNameDlg2"
					type="text" maxlength="97" tabindex="2" style="width: 145px;"
					class="InputTextStyle InputText1Style" />
					<label class="LabelStyle Label1Style" style="width: 100px;">Loại
					nhóm <span class="RequireStyle">*</span> </label>
					<div class="BoxSelect BoxSelect10">
						<select id="typeGroup2" class="MySelectBoxClass">
								<option selected="selected" value="-1">Chọn loại nhóm</option>
								<option value="1">Doanh số</option>
								<option value="2">Sản lượng</option>
								<option value="3">SKU</option>
						</select>
					</div>
				<div class="Clear"></div>
				<div class="BtnCenterSection">
					<button id="btnChangeGroup" class="BtnGeneralStyle"
						onclick="return ProgrammeDisplayCatalog.changeGroup2();">Cập
						nhật</button>
				</div>
				<p id="errMsgGroupDlg2" style="display: none;"
					class="ErrorMsgStyle SpriteErr"></p>
				<p id="successMsgGroupDlg" style="display: none;"
					class="SuccessMsgStyle"></p>
				<s:hidden id="groupId2" value="0"></s:hidden>
			</div>
		</div>
	</div>
</div>
<div id="divProductContainer" style="display: none;">
	<div id="dialogProduct" class="easyui-dialog"
		title="Thông tin tìm kiếm <img id='loadingDialog' src='/resources/scripts/plugins/jquery-easyui-1.3.2/themes/default/images/loading.gif'>"
		data-options="closed:true,modal:true" style="width: 700px;">
		<div class="GeneralForm Search1Form">
			<label class="LabelStyle Label2Style">Mã sản phẩm</label> <input
				id="productCodeDlg" type="text"
				class="InputTextStyle InputText5Style" style="padding-left: 10px" />
			<label class="LabelStyle Label2Style">Tên sản phẩm</label> <input
				id="productNameDlg" type="text"
				class="InputTextStyle InputText5Style" style="padding-right: 90px" />
			<div class="Clear"></div>
			<div class="BtnCenterSection">
				<button id="btnSeachProductTree" class="BtnGeneralStyle">Tìm
					kiếm</button>
			</div>
			<div class="Clear"></div>
		</div>
		<div class="GeneralForm SProduct1Form">
			<h2 class="Title2Style">Danh sách sản phẩm</h2>
			<div class="GridSection" id="treeGridContainer">
				<table id="treeGrid" class="easyui-datagrid"></table>
			</div>
			<div class="Clear"></div>
			<div class="BtnCenterSection">
				<button id="btnChangeProduct" class="BtnGeneralStyle"
					onclick="return ProgrammeDisplayCatalog.changeProduct();">Chọn</button>
					<button id="btnChangeProduct" class="BtnGeneralStyle"
					onclick="$('#dialogProduct').window('close');">Đóng</button>
			</div>
			<div class="Clear"></div>
			<p id="errMsgProductDlg" class="ErrorMsgStyle" style="display: none;"></p>
			<p id="successMsgProductDlg" class="SuccessMsgStyle"
				style="display: none;"></p>
		</div>
		<s:hidden id="isUpdate" value="0"></s:hidden>
		<s:hidden id="productId" value="0"></s:hidden>
		<s:hidden id="gridDetailId"></s:hidden>
	</div>
</div>
<div id="divLevelContainer" style="display: none;">
	<div id="dialogLevel" class="easyui-dialog"
		title="Thông tin mức <img id='loadingLevelDialog' src='/resources/scripts/plugins/jquery-easyui-1.3.2/themes/default/images/loading.gif'>"
		data-options="closed:true,modal:true" style="width: 700px;">
		<div class="PopupContentMid2">
			<div class="GeneralForm Search1Form">
				<label class="LabelStyle Label2Style">Mã mức<span class="RequireStyle">*</span> </label> 
				<input id="levelCodeDlg" type="text" maxlength="50" class="InputTextStyle InputText1Style" /> 
				<label class="LabelStyle Label2Style">Doanh số</label> 
				<input id="amountDlg" type="text" maxlength="18" class="InputTextStyle InputText1Style" style="text-align:right"/>
				
				<label class="LabelStyle Label2Style">Sản lượng</label> 
				<input id="quantityLv" type="text" maxlength="8" class="InputTextStyle InputText1Style" style="text-align:right"/>
				<div class="Clear"></div>
				<label class="LabelStyle Label2Style">Số SKU</label> 
				<input	id="skuDlg" type="text" maxlength="8" class="InputTextStyle InputText1Style" style="text-align:right"/>
			</div>
			<div class="GeneralForm SProduct1Form">
				<h2 class="Title2Style">Sản phẩm doanh số</h2>
				<div class="GridSection" id="amountLevelContainer">
					<table id="amountLevelGrid" class="easyui-datagrid"></table>
				</div>
				<div class="Clear"></div>
<!-- 				<h2 class="Title2Style">Danh sách nhóm sản phẩm cho SKU</h2> -->
<!-- 				<div class="GridSection" id="skuLevelContainer"> -->
<!-- 					<table id="skuLevelGrid" class="easyui-datagrid"></table> -->
<!-- 				</div> -->
				<h2 class="Title2Style">Sản phẩm trưng bày</h2>
				<div class="GridSection" id="displayLevelContainer">
					<table id="displayLevelGrid" class="easyui-datagrid"></table>
				</div>
				<div class="Clear"></div>
				<div class="BtnCenterSection">
					<button id="btnChangeLevel" class="BtnGeneralStyle"
						onclick="return ProgrammeDisplayCatalog.changeLevel();">Cập
						nhật</button>
				</div>
				<div class="Clear"></div>
				<p id="errMsgLevelDlg" class="ErrorMsgStyle" style="display: none;"></p>
				<p id="successMsgLevelDlg" class="SuccessMsgStyle"
					style="display: none;"></p>
			</div>
			<s:hidden id="levelId" value="0"></s:hidden>
		</div>
	</div>
</div>
<div id="divStaffContainer" style="display: none;">
	<div id="dialogStaff" class="easyui-dialog"
		title="Thông tin tìm kiếm <img id='loadingStaffDialog' src='/resources/scripts/plugins/jquery-easyui-1.3.2/themes/default/images/loading.gif'>"
		data-options="closed:true,modal:true" style="width: 700px;">
		<div class="GeneralForm Search1Form">
			<label class="LabelStyle Label2Style">Mã nhân viên</label> <input
				id="staffCodeDlg" type="text" class="InputTextStyle InputText5Style"
				style="padding-left: 10px" maxlength="50" /> <label
				class="LabelStyle Label2Style">Tên nhân viên</label> <input
				id="staffNameDlg" type="text" class="InputTextStyle InputText5Style"
				style="padding-right: 90px" maxlength="100" />
			<div class="Clear"></div>
			<div class="BtnCenterSection">
				<button id="btnSeachStaffDlg" class="BtnGeneralStyle">Tìm
					kiếm</button>
			</div>
			<div class="Clear"></div>
		</div>
		<div class="GeneralForm SProduct1Form">
			<h2 class="Title2Style">Danh sách nhân viên</h2>
			<div class="GridSection" id="staffGridDialogContainer">
				<table id="staffGridDialog" class="easyui-datagrid"></table>
			</div>
			<div class="Clear"></div>
			<div class="BtnCenterSection">
				<button id="btnChangeStaff" class="BtnGeneralStyle"
					onclick="return ProgrammeDisplayCatalog.changeStaff();">Chọn</button>
			</div>
			<div class="Clear"></div>
			<p id="errMsgStaffDlg" class="ErrorMsgStyle" style="display: none;"></p>
			<p id="successMsgStaffDlg" class="SuccessMsgStyle"
				style="display: none;"></p>
		</div>
	</div>
</div>
<div id="divCustomerContainer" style="display: none;">
	<div id="dialogCustomer" class="easyui-dialog"
		title="Thông tin tìm kiếm <img id='loadingCustomerDialog' src='/resources/scripts/plugins/jquery-easyui-1.3.2/themes/default/images/loading.gif'>"
		data-options="closed:true,modal:true" style="width: 700px;">
		<div class="GeneralForm Search1Form">
			<label class="LabelStyle Label2Style">Mã KH</label> <input
				id="customerCodeDlg" type="text"
				class="InputTextStyle InputText5Style" style="padding-left: 10px" />
			<label class="LabelStyle Label2Style">Tên KH</label> <input
				id="customerNameDlg" type="text"
				class="InputTextStyle InputText5Style" style="padding-right: 90px" />
			<div class="Clear"></div>
			<label class="LabelStyle Label2Style">Địa chỉ</label> <input
				id="addressDlg" type="text" class="InputTextStyle InputText3Style"
				style="width: 531px" />
			<div class="Clear"></div>
			<div class="BtnCenterSection">
				<button id="btnSeachCustomerDlg" class="BtnGeneralStyle">Tìm
					kiếm</button>
			</div>
			<div class="Clear"></div>
		</div>
		<div class="GeneralForm Search1Form">
			<h2 class="Title2Style">Danh sách khách hàng</h2>
			<div class="GridSection" id="customerGridDialogContainer">
				<table id="customerGridDialog" class="easyui-datagrid"></table>
			</div>
			<div class="Clear"></div>
			<label class="LabelStyle Label1Style">Mức<span
				class="RequireStyle">(*)</span> </label>
			<div class="BoxSelect BoxSelect3">
				<select id="levelCodeCustomerDlg" class="MySelectBoxClass"
					name="LevelSchool">
					<option value="-1" selected="selected">---Tất cả---</option>
					<s:iterator value="lstLevel">
						<option value="<s:property value="id"/>">
							<s:property value="levelCode" />
						</option>
					</s:iterator>
				</select>
			</div>
			<div class="Clear"></div>
			<label class="LabelStyle Label1Style">Từ ngày<span
				class="RequireStyle">(*)</span> </label> <input id="startDateDlg"
				type="text" class="InputTextStyle InputText5Style" /> <label
				class="LabelStyle Label2Style">Đến ngày<span
				class="RequireStyle">(*)</span> </label> <input id="endDateDlg" type="text"
				class="InputTextStyle InputText5Style" />
			<div class="Clear"></div>
			<div class="BtnCenterSection">
				<button id="btnChangeCustomer" class="BtnGeneralStyle"
					onclick="return ProgrammeDisplayCatalog.changeCustomer();">Chọn</button>
			</div>
			<div class="Clear"></div>
			<p id="errMsgCustomerDlg" class="ErrorMsgStyle"
				style="display: none;"></p>
			<p id="successMsgCustomerDlg" class="SuccessMsgStyle"
				style="display: none;"></p>
		</div>
		<s:hidden id="staffId" value="0"></s:hidden>
		<s:hidden id="customerId" value="0"></s:hidden>
	</div>
</div>
<div id="searchStyle1EasyUIDialogDivEx"
	style="width: 600px; display: none">
	<div id="searchStyle1EasyUIDialogEx" class="easyui-dialog" title=""
		data-options="closed:true,modal:true">
		<div class="PopupContentMid">
			<div class="GeneralForm Search1Form">
				<label id="seachStyle1CodeLabel" class="LabelStyle Label1Style"	style="width: 100px;">Mã CTTB <span class="ReqiureStyle">*</span></label> 
				<input id="seachStyle1Code" maxlength="47" tabindex="1" type="text"	style="width: 145px;" class="InputTextStyle InputText1Style" /> 
				<label id="seachStyle1NameLabel" class="LabelStyle Label1Style"	style="width: 100px;">Tên CTTB<span class="ReqiureStyle">*</span></label> 
				<input id="seachStyle1Name" type="text" maxlength="97" tabindex="2"	style="width: 145px;" class="InputTextStyle InputText1Style" />
				<div class="Clear"></div>				
				<div class="BtnCenterSection">
					<button id="__btnSaveDisplayProgram" class="BtnGeneralStyle">Sao chép</button>
					<button class="BtnGeneralStyle BtnGeneralMStyle" onclick="$('#searchStyle1EasyUIDialogEx').dialog('close');">Đóng</button>
				</div>
				<p id="errMsgSearch" style="display: none;" class="ErrorMsgStyle SpriteErr"></p>
				<p id="successMsgInfo" class="SuccessMsgStyle"	style="display: none;">Lưu dữ liệu thành công.</p>
			</div>
		</div>
	</div>
</div>
<div id="searchStyleExclusionDialogDiv"
	style="width: 650px; display: none">
	<div id="searchStyleExclusionDialog" class="easyui-dialog" title=""
		data-options="closed:true,modal:true">
		<div class="PopupContentMid">
			<div class="GeneralForm Search1Form">
				<label id="seachStyle1CodeLabel" class="LabelStyle Label1Style"
					style="width: 100px;">Mã</label> <input
					id="seachStyleExclusionCode" maxlength="47" tabindex="1"
					type="text" style="width: 145px;"
					class="InputTextStyle InputText1Style" /> <label
					id="seachStyle1NameLabel" class="LabelStyle Label1Style"
					style="width: 100px;">Tên</label> <input
					id="seachStyleExclusionName" type="text" maxlength="97"
					tabindex="2" style="width: 145px;"
					class="InputTextStyle InputText1Style" />
				<div class="Clear"></div>
				<div class="BtnCenterSection">
					<button class="BtnGeneralStyle Sprite2" tabindex="4"
						id="btnSearchStyleExclusion">
						<span class="Sprite2">Tìm kiếm</span>
					</button>
				</div>
				<input type="hidden" name="" id="searchStyleExclusionUrl" /> <input
					type="hidden" name="" id="searchStyle1CodeText" /> <input
					type="hidden" name="" id="searchStyle1NameText" /> <input
					type="hidden" name="" id="searchStyle1IdText" />
				<s:hidden id="searchStyle1AddressText"></s:hidden>
				<div class="Clear"></div>
				<div class="GridSection" id="searchStyleExclusionContainerGrid">
					<table id="searchStyleExclusionGrid" class="easyui-datagrid"
						style="width: 520px;"></table>
				</div>
				<div class="Clear"></div>
				<div class="BtnCenterSection">
					<button id="btnSaveExclusion" class="BtnGeneralStyle">Chọn</button>
					<button class="BtnGeneralStyle BtnGeneralMStyle"
						id="btnCloseExclusion"
						onclick="return $('#searchStyleExclusionDialog').dialog('close');">Đóng</button>
				</div>
				<p id="errMsgSearchExclusion" style="display: none;"
					class="ErrorMsgStyle"></p>
			</div>
		</div>
	</div>
</div>
<div id="searchStyleShopDisplay" style="display: none;">
	<div id="searchStyleShopDisplay1" class="easyui-dialog" title=""
		data-options="closed:true,modal:true">
		<div class="PopupContentMid2">
			<div class="GeneralForm Search1Form">				
				<label id="seachStyleShopCodeLabel" class="LabelStyle Label2Style">Mã</label>
				<input id="seachStyleShopCode" maxlength="50" tabindex="1"
					type="text" class="InputTextStyle InputText5Style" /> <label
					id="seachStyleShopNameLabel" class="LabelStyle Label2Style">Tên</label>
				<input id="seachStyleShopName" type="text" maxlength="250"
					tabindex="2" class="InputTextStyle InputText5Style" />
				<div class="Clear"></div>
				<div class="BtnCenterSection">
					<button class="BtnGeneralStyle Sprite2" tabindex="4"
						id="btnSearchStyleShop">
						<span class="Sprite2">Tìm kiếm</span>
					</button>
				</div>
				<s:hidden id="searchStyleShopUrl"></s:hidden>
				<s:hidden id="searchStyleShopCodeText"></s:hidden>
				<s:hidden id="searchStyleShopNameText"></s:hidden>
			</div>
			<div class="GeneralForm Search1Form">
				<h2 class="Title2Style Title2MTStyle">Danh sách đơn vị</h2>
				<div class="GridSection" id="searchStyleShopContainerGrid">
					<table id="searchStyleShopGrid" class="easyui-treegrid"></table>
				</div>
				<div class="Clear"></div>
				<div class="BtnCenterSection">
					<s:if test="headOfficePermission">
						<button id="btnSaveShopMap"
							onclick="return ProgrammeDisplayCatalog.createDisplayProgramShopMap();"
							class="BtnGeneralStyle">Chọn</button>
					</s:if>
					<button class="BtnGeneralStyle"
						onclick="return $('#searchStyleShopDisplay1').dialog('close');">Đóng</button>
				</div>
				<p id="errMsgDialog" class="ErrorMsgStyle" style="display: none"></p>
			</div>
		</div>
	</div>
</div>
<!-- 
	id: easyuiPopup
	author: LocHP
	16/09/2013
-->
<div style="display: none">
	<div id="easyuiPopup" class="easyui-dialog" title="Upload công văn"
		data-options="closed:true,modal:true"
		style="width: 485px; height: 190px;">
		<div class="PopupContentMid">
			<div class="GeneralForm ImportExcel1Form">
				<label class="LabelStyle Label1Style">File pdf</label>
				<form action="/programme-display/uploadPdfFile" name="importFrm"
					id="uploadFrm" method="post" enctype="multipart/form-data">
					<div class="UploadFileSection Sprite1">
						<input name="fakePdffilepc" id="fakePdffilepc" type="text"
							class="InputTextStyle InputText1Style"> <input
							type="file" class="UploadFileStyle" size="1" name="pdfFile"
							id="pdfFile" onchange="uploadPdfFile(this,'importFrm');" /> <input
							type="hidden" id="isView" name="isView">
					</div>
				</form>
				<div class="Clear"></div>
				<div class="BtnCenterSection">
					<button class="BtnGeneralStyle"
						onclick="return DisplayProgram.uploadPdfFile();">Upload</button>
					<button class="BtnGeneralStyle BtnGeneralMStyle"
						onclick="$('#easyuiPopup').window('close');">Đóng</button>
				</div>
			</div>
			<p style="display: none; margin-top: 10px; margin-left: 10px"
				id="errPdfMsg" />
			<p
				style="display: none; margin-top: 10px; margin-left: 10px; color: red"
				id="resultPdfMsg" />
		</div>
	</div>
</div>
<div style="display: none">
	<div id="easyuiPopup1" class="easyui-dialog" title="Nhập CTTB - Khách hàng Excel" data-options="closed:true,modal:true"  style="width:485px;height:215px;">
	      <div class="PopupContentMid">
	       	<div class="GeneralForm ImportExcel1Form">
	       		<label class="LabelStyle Label1Style">File excel</label>
				<form action="/programme-display/importCustomer" name="importFrmCus" id="importFrmCus" method="post" enctype="multipart/form-data">
					<div class="UploadFileSection Sprite1">
						<input id="fakefilepcCustomer" readonly="readonly" type="text" class="InputTextStyle InputText1Style"> <input type="file" class="UploadFileStyle" size="1" name="excelFileCus" id="excelFileCus"
							onchange="previewImportExcelFile(this,'importFrmCus','fakefilepcCustomer');" /> <input type="hidden" id="isView" name="isView">
					</div>
				</form>
				<div class="Clear"></div>
				<p class="DownloadSFileStyle">
					<a id="downloadTemplateCustomer" href="javascript:void(0)" class="Sprite1" onclick="">Tải file excel mẫu</a>
				</p>
				<div class="BtnCenterSection">
					<button class="BtnGeneralStyle" onclick="return ProgrammeDisplayCatalog.upload();">Tải lên</button>
					<button class="BtnGeneralStyle BtnGeneralMStyle" onclick="$('#easyuiPopup1').window('close');">Đóng</button>
				</div>
				<div class="Clear"></div>
				<p class="ErrorMsgStyle" style="display: none; margin-top: 10px; float: left; text-align: left;" id="errExcelMsg" />
					<p style="display: none; margin-top: 10px; float: left; text-align: left;" id="resultExcelMsg" />
			</div>
	        
		</div>
	</div>
</div>
<div id ="searchProductDialogDiv" style="display: none;width:600px" >
	<div id="searchProductEasyUIDialog">
		<div class="PopupContentMid">
			<div class="GeneralForm Search1Form">
				<label id="seachProductCodeLabel" class="LabelStyle Label1Style" style=" width: 106px;">Mã SP</label>
				<input id="seachProductCode" maxlength="50" tabindex="1" type="text" class="InputTextStyle InputText1Style" />  
				<label id="seachProductNameLabel" class="LabelStyle Label1Style">Tên SP</label> 
				<input id="seachProductName"  type="text" maxlength="250" tabindex="2"  class="InputTextStyle InputText1Style" />
				<button class="BtnGeneralStyle BtnSearchStyle BtnSearchOnDialog" 
						tabindex="3" id="btnSearchProduct">Tìm kiếm</button>
				<div class="Clear"></div>
				<div class="GridSection" id="searchProductContainerGrid">
					<!--Đặt Grid tại đây-->
					<table id="searchProductAmountGrid" style="width: 520px;"></table>
				</div>
				<div class="BtnCenterSection">
					<button id="__btnSave" onclick="ProgrammeDisplayCatalog.saveProductDlgDS();" class="BtnGeneralStyle">Chọn</button>
					<button class="BtnGeneralStyle BtnGeneralMStyle" onclick="$('#searchProductEasyUIDialog').window('close');">Đóng</button>
				</div>
				<input type="hidden" id="subGridId">
				<p id="errProductMsgSearch" class="ErrorMsgStyle SpriteErr"></p>
			</div>
		</div>
	</div>
</div>        
<s:hidden id="shopCode" name="shopCode"></s:hidden>
<s:hidden id="curShopId" name="currentShopId"></s:hidden>

<div style="display: none; width: 100%" id="divWidth"></div>
<input type="hidden" id="solg" value="" />
<input type="hidden" id="width" value="" />
<input type="hidden" id="staffRoleHide"	value='<s:property value="staffRole" />'/>
<input type="hidden" id="statusPermission"	value='<s:property value="status" />'/>
<input type="hidden" id="permissionUser" value="<s:property value="permissionUser"/>" />
<s:hidden id="tmpCheckedListFlag" name="tmpCheckedListFlag"></s:hidden>
<s:hidden id="tmpGroupType" name="tmpGroupType"></s:hidden>
<s:hidden id="tmpGroupType" name="tmpGroupType"></s:hidden>
<s:hidden id="tmpGroupId" name="tmpGroupId"></s:hidden>
<s:hidden id="tmpSubGridId" name="tmpSubGridId"></s:hidden>
<s:hidden id="amountType" name="amountType"></s:hidden>
<s:hidden id="selId" name="id"></s:hidden>
<s:hidden id="selLevelId" value="0"></s:hidden>
<s:hidden id="selProductId" value="0"></s:hidden>
<s:hidden id="selQuotaId" value="0"></s:hidden>
<s:hidden id="selStaffId" value="0"></s:hidden>
<s:hidden id="aPer" name="activePermission"></s:hidden>
<s:hidden id="ePer" name="permissionEdit"></s:hidden>
<s:hidden id="csPer" name="commercialSupportPermission"></s:hidden>
<s:hidden id="dPer" name="developCustomer"></s:hidden>
<s:hidden id="dEdit" name="devIsUpdate"></s:hidden>
<s:hidden id="hidStatus" name="status"></s:hidden>
<s:hidden id="tabOrder"></s:hidden>
<s:hidden id="vnmShopCode" name="vnmShopCode"></s:hidden>
<s:hidden id="shopId" name="staff.shop.shopId" ></s:hidden>
<input type="hidden" id="filterMonth"/>
<input type="hidden" id="isOutOfDate"	value='<s:property value="isOutOfDate" />'/>
<script type="text/javascript">
	$(document).ready(function() {
		var flag = $('#staffRoleHide').val().trim();
		var fstatus = $('#hidStatus').val().trim();
		ReportUtils.loadComboTree('shop', 'shopId', $('#curShopId').val(),function(data){
		});
		ProgrammeDisplayCatalog._params = new Object();
		ProgrammeDisplayCatalog._lstCustomerId = new Map();
		if(flag.length>0 && fstatus.length>0 && parseInt(flag)!=6 && parseInt(fstatus)!=2){
			ProgrammeDisplayCatalog.hideAllTab();
			$('#tab5').show();
			$('#tab10').show();
			$('#tab5').addClass('Active');
			ProgrammeDisplayCatalog.showStaffTab();
		}
		var shopId = '<s:property value="shopId" />';

		
		 
						//$('#name').width(505); //loctt 18sep2013
						$('#displayProgramName').width(505);//loctt 18sep2013
						$('#shopName').width(425);
						ProgrammeDisplayCatalog._listShopId = new Array();
						ProgrammeDisplayCatalog._listShopIdEx = new Array();
						ProgrammeDisplayCatalog._lstSize = new Array();
						ProgrammeDisplayCatalog._lstProduct = new Map();
						$('#downloadTemplateCustomer').attr('href',excel_template_path + 'displayprogram/CTTB_KhachHang.xls');
						$('.easyui-dialog #downloadTemplateCustomer').attr('href',excel_template_path + 'displayprogram/CTTB_KhachHang.xls');
						//$('#displayProgram').customStyle();
						ReportUtils.loadComboReportTreeEx('shopTree', 'shopCode', $('#curShopId').val(),function(data){
							if(data!=$('#shopCode').val().trim()){
// 								checkFun();
							}
						});
						applyMonthPicker('month');
						Utils.bindFormatOnTextfield('month',Utils._TF_NUMBER_CONVFACT);
						var now = new Date();
						var cYear = now.getFullYear();
						var cMonth = now.getMonth() + 1;
						if(cMonth != 10 && cMonth != 11 && cMonth != 12  ){
							cMonth = '0'+cMonth;
						}
						var currentDate = cMonth+'/'+cYear;
						$('#month').val(currentDate);
						applyMonthPicker('month1');
						Utils.bindFormatOnTextfield('month1',Utils._TF_NUMBER_CONVFACT);
						var now = new Date();
						var cYear = now.getFullYear();
						var cMonth = now.getMonth() + 1;
						if(cMonth != 10 && cMonth != 11 && cMonth != 12  ){
							cMonth = '0'+cMonth;
						}
						var currentDate = cMonth+'/'+cYear;
						$('#month1').val(currentDate);
// 						$('#filterMonth').val(currentDate);
						Utils.bindAutoSearch();
						$('.MySelectBoxClass').customStyle();
						setDateTimePicker('fDate');
						setDateTimePicker('tDate');
						setDateTimePicker('fromDate');
						setDateTimePicker('toDate');
						setDateTimePicker('startDate');
						setDateTimePicker('endDate');
						setDateTimePicker('fromDateCus');
						setDateTimePicker('toDateCus');
						setDateTimePicker('startDateDlg');
						setDateTimePicker('endDateDlg');
						ProgrammeDisplayCatalog._listExclusionDialogSize = new Array();
						ProgrammeDisplayCatalog._listExclusion = new Map();
						ProgrammeDisplayCatalog._arrListStaffItem = new Array();
						ProgrammeDisplayCatalog._listShopId = new Array();
						Utils.bindFormatOnTextfield('quantity',
								Utils._TF_NUMBER);
						Utils.bindFormatOnTextfield('levelAmount',
								Utils._TF_NUMBER);
						Utils.bindFormatOnTextfield('fromPercent',
								Utils._TF_NUMBER);
						Utils.bindFormatOnTextfield('toPercent',
								Utils._TF_NUMBER);
						Utils.formatCurrencyFor('levelAmount');
						$('#levelPCode').attr('disabled', 'disabled');
						$('#levelPName').attr('disabled', 'disabled');
						$('#quotaPCode').attr('disabled', 'disabled');
						$('#quotaPName').attr('disabled', 'disabled');
						$('#productPCode').attr('disabled', 'disabled');
						$('#productPName').attr('disabled', 'disabled');
						$('#productCode').bind('keyup',function(event) {
							if (event.keyCode == keyCode_F9) {
								if ($('#selId').val().trim().length == 0) {
									CommonSearch.searchProductOnDialog(function(data) {
										$('#productCode').val(Utils.XSSEncode(data.code));});
								} else {
									CommonSearch.searchProductOnDialog(function(data) {
										$('#productCode').val(Utils.XSSEncode(data.code));
										$('#productCode').focus();
								}, new Array());
							}}
						});

						$('#exclusion').bind(
								'keyup',
								function(event) {
									if (event.keyCode == keyCode_F9) {
										ProgrammeDisplayCatalog.showDisplayProgramExclusion();
									}
								});
						$('#displayProgramCode').bind('keyup',function(event) {
							if (event.keyCode == 13) {
								ProgrammeDisplayCatalog.searchExclusionProgram()();
							}
						}).val('');
						$('#displayProgramName').bind('keyup',function(event) {
 							if (event.keyCode == 13) {
								ProgrammeDisplayCatalog.searchExclusionProgram()();
						}
						}).val('');
						$('#code').focus();
						var status = $('#hidStatus').val();
						var selId = $('#selId').val();
						if (selId == 0) {
							$('#code').removeAttr('disabled');
						}
						if (selId == 0) {
							$('#subContent').hide();
						} else {
							$('#subContent').show();
							disableSelectbox('htad');
						}
						if (status == 1 && selId != 0) {
							$('#fDate').next().unbind('click');
						}
						if (status == 0 && selId != 0) {
							$('#fDate').next().unbind('click');
							$('#tDate').next().unbind('click');
						}
						$('#customerCode').val('');
						var url = '';
						var code = $('#displayProgramCode').val();
						var name = $('#displayProgramName').val();
						var title = '';
						if ($('#statusPermission').val().trim() == activeType.WAITING) {
							title = '<a href="javascript:void(0)" onclick=\"return ProgrammeDisplayCatalog.openExclusionProgram();\"><img src="/resources/images/icon_add.png"/></a>';
						}
						url = '/programme-display/searchExclusionProgram?id=' + encodeChar(selId) + '&code=' + encodeChar(code) + '&name=' + encodeChar(name);
						$('#exclusionGrid').datagrid({
							url : url,
							width : $(window).width() - 45,
							scrollbarSize : 0,
							autoWidth : true,
							fitColumns : true,
							rownumbers : true,
							singleSelect:true,
							collapsible:true,
							columns : [ [
								{field : 'exclusionProgramCode',title : 'Mã CTTB',width : 80,sortable : false,resizable : true, align : 'left',
										formatter : function(value, row,index) {
										return Utils.XSSEncode(row.exDisplayProgram.displayProgramCode);
								}},
								{field : 'exclusionProgramName',title : 'Tên CTTB', width : 150, sortable : false, resizable : true, align : 'left', formatter : function(value, row,index) {
										return Utils.XSSEncode(row.exDisplayProgram.displayProgramName);
								}},
								{field : 'fromDate', title : 'Từ ngày', width : 80, sortable : false, align : 'center', resizable : false,
										formatter : function( value, row, index) { return toDateString(new Date( row.exDisplayProgram.fromDate)); } },
								{field : 'toDate', title : 'Đến ngày', width : 80, sortable : false, align : 'center', resizable : false, formatter : function(value, row, index) {
										var toDate = formatDate(row.exDisplayProgram.toDate);
										if (toDate == null || toDate == undefined || toDate == '' || toDate == 'null') { toDate = '';}
											return toDate;
								}},
								{field : 'status', title : 'Trạng thái', width : 82, sortable : false, resizable : false, align : 'center', formatter : function( value, row, index) {
										if (row.exDisplayProgram.status == activeStatus) {
											return "Hoạt động";
										} else if (row.exDisplayProgram.status == stoppedStatus) {
											return "Tạm ngưng";
										} else if (row.exDisplayProgram.status == waitingStatus) {
											return "Dự thảo";
										}
								}},
								{field : 'detail', title : title, width : 30, sortable : false, resizable : false, align : 'center', formatter : function( value, row, index) {
									if ($('#statusPermission').val().trim() == activeType.WAITING) {
									return "<a href='javascript:void(0)' onclick=\"return ProgrammeDisplayCatalog.deleteExclusionProgram(" + row.id + ");\"><img src='/resources/images/icon_delete.png'/></a>"; }
								}}
							] ],
							onLoadSuccess : function(data) {
								$('.datagrid-header-rownumber').html('STT');
								updateRownumWidthForJqGrid('#exclusionGrid');
							}, 
							method : 'GET'
						});

						
						$('#customerGrid').datagrid({
							url :'/programme-display/customer/search?id='+$('#selId').val(),
							autoRowHeight : true,
							rownumbers : true, 
							checkOnSelect :true,
							pagination:true,
							method:'GET',
							fitColumns:true,
							scrollbarSize : 0,
							pageSize:50,
							singleSelect : true,
							fitColumns : true,
							pageList  : [10,20,30],
							width : $(window).width()-8,
							columns:[[  
					        	{field: 'levelCode',title:'Mức CTTB', width:50,align:'left',sortable : false,resizable : false,formatter :function(value,row,index) {
									return Utils.XSSEncode(value);
								}},  
					        	{field: 'shopCode',title:'Mã NPP', width:90,align:'left',sortable : false,resizable : false,formatter :function(value,row,index) {
									return Utils.XSSEncode(value);
								}},  
					        	{field: 'staffName',title:'NVBH', width:90,align:'left',sortable : false,resizable : false,formatter :function(value,row,index) {
									return Utils.XSSEncode(row.staffCode+"-"+Utils.XSSEncode(value));
								}},  
					        	{field: 'customerName',title:'Khách hàng', width:90,align:'left',sortable : false,resizable : false,formatter :function(value,row,index) {
									return Utils.XSSEncode(value);
								}},  
								{field: 'address',title:'Địa chỉ', width:90,align:'left',sortable : false,resizable : false,formatter :function(value,row,index) {
									return Utils.XSSEncode(value);
								}},
					        	{field: 'month',title:'Tháng', width:50,align:'left',sortable : false,resizable : false,formatter:function(row,index){
					        		$('#totalRowGrid').val('1');
						        		return Utils.XSSEncode(row);
					        		}
					        	},  
					        	{field: 'id', checkbox:true, align:'center', width:80,sortable : false,resizable : false}
					        ]],
					        onLoadSuccess :function(data){
						    	$('.datagrid-header-rownumber').html('STT');
						    	var originalSize = $('.datagrid-header-row td[field=displayProgramCode] div').width();
						    	if(originalSize != null && originalSize != undefined){
						    		ProgrammeDisplayCatalog._lstSize.push(originalSize);
						    	}
						    	$('input[name="id"]').each(function(){
									var temp = ProgrammeDisplayCatalog._lstProduct.get($(this).val());
									if(temp!=null) $(this).attr('checked','checked');
								});
						    	var length = 0;
						    	$('input[name="id"]').each(function(){
						    		if($(this).is(':checked')){
						    			++length;
						    		}	    		
						    	});	    	
						    	if(data.rows.length==length){
						    		$('.datagrid-header-check input').attr('checked',true);
						    	}else{
						    		$('.datagrid-header-check input').attr('checked',false);
						    	}
						    },
						    onCheck:function(i,r){
						    	$('#checkDeleteAll1').removeAttr('checked');
						    	ProgrammeDisplayCatalog._lstProduct.put(r.id,r);
						    },
						    onUncheck:function(i,r){
						    	ProgrammeDisplayCatalog._lstProduct.remove(r.id);
						    },
						    onCheckAll:function(r){
						    	$('#checkDeleteAll1').removeAttr('checked');
						    	for(i=0;i<r.length;i++){
						    		ProgrammeDisplayCatalog._lstProduct.put(r[i].id,r[i]);
						    	}
						    },
						    onUncheckAll:function(r){
						    	for(i=0;i<r.length;i++){
						    		ProgrammeDisplayCatalog._lstProduct.remove(r[i].id);
						    	}
						    }
						});
						
					});
	function statusChanged() {
		enable('toDate');
		if ($('#selId').val().trim().length > 0) {
			if ($('#aPer').val().trim() == 'true') {
				if (($('#statusPermission').val().trim() == activeType.WAITING || $(
						'#statusPermission').val().trim() == activeType.STOPPED)) {
					disabled('tDate');
				} else {
					enable('tDate');
					$('#toDateDiv .ui-datepicker-trigger').bind('click');
				}
			}
		} else {
			enable('tDate');
			$('#toDateDiv .ui-datepicker-trigger').bind('click');
		}
	}
</script>
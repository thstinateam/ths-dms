<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>

<div class="BreadcrumbSection">
	<ul class="ResetList FixFloat BreadcrumbList">		
		<li class="Sprite1"><a href="/programme-display/info">Chương trình trưng bày</a></li>
		<li><span>Khách hàng</span></li>
	</ul>
</div>

<div class="CtnOneColSection">
	<div class="ContentSection">
		<div class="ToolBarSection">
			<div class="SearchSection GeneralSSection">
				<h2 class="Title2Style">Thông tin CTTB</h2>
				<div class="SearchInSection SProduct1Form">	

                        <div class="SearchInSection">
                            <label class="LabelStyle Label1Style">Đơn vị</label>
<!--                             <div class="ComboTreeSection" id="shopTreeId"> -->
<!--                          	<input id="shop" type="text"  style="width:126px;" class="InputTextStyle InputText1Style" /> -->
<!--                          </div>                     -->
							<div style="height: auto;">
								<input id="shop" type="text"  style="width:200px; height: auto;" class="InputTextStyle InputText1Style" />
							</div>
                            <label class="LabelStyle Label1Style">Tháng</label>
                            <input type="text" class="InputTextStyle InputText1Style" id="month1" maxlength="7" style="width:98px;"/>
                            <label class="LabelStyle Label1Style">CTTB</label>
                                <div class="BoxSelect BoxSelect5">
                                    <select class="MySelectBoxClass" id="displayProgram">
                                    	<option value="">Tất cả</option>
                                        <s:iterator id="obj" value="listDisplayProgram">
											<option value="<s:property value="id"/>"><s:property value="displayProgramCode" /></option>
										</s:iterator>
                                    </select>
                                </div>
                            <div class="BtnLSection" style="padding-left:18px;">
                                <button class="BtnGeneralStyle" onclick="ProgrammeDisplayCatalog.searchCustomerTabEX();">Tìm kiếm</button>
                            </div>
                             
                            <div class="Clear"></div>
                                
                        </div>
                        <div class="Clear"></div>

<!-- 	                    <div class="SearchInSection SProduct1Form"> -->
	                    	<div class="GeneralForm GeneralNoTPForm">
	                        	<div class="Func1Section">
	                        		<s:if test="staffRole!=9">
	                        			<button id="btnImportExcel" class="BtnGeneralStyle cmsiscontrol" onclick= "return ProgrammeDisplayCatalog.openPopupImportCustomer();">Nhập từ Excel</button>
	                        		</s:if>
		                        	<button class="BtnGeneralStyle"  onclick="return ProgrammeDisplayCatalog.exportExcelCTTB();">Xuất Excel</button>
		                        	<s:if test="staffRole!=9">
		                        		<p class="DownloadSFileStyle"><a id="downloadTemplateCustomer" href="#" class="Sprite1 cmsiscontrol">Tải file excel mẫu</a></p>
		                        	</s:if>
	                            </div>
	                            
								<label class="LabelStyle Label1Style" id="loadShop" style="width:450px;"></label> 
								<label class="LabelStyle Label1Style" id="loadShop" style="width:100px;">
									<s:if test="staffRole!=9">
										<input type="checkbox" id="checkDeleteAll" onclick="return ProgrammeDisplayCatalog.uncheckAll();" class="cmsiscontrol" />Chọn toàn bộ
									</s:if> 
								</label>
	                            <div class="Func2Section">
	                            	<s:if test="staffRole!=9">
	                            		<button id="btnDelAll" class="BtnGeneralStyle cmsiscontrol" onclick="return ProgrammeDisplayCatalog.deleteCustomerDisplayProgram();">Xóa</button>
	                            	</s:if>
	                            </div>
	                            <div class="Clear"></div>
	                             <p id="errCustomerMsg" class="ErrorMsgStyle" style="display: none"></p>
                                 <p id="successCustomerMsg" class="SuccessMsgStyle" style="display: none"></p>
	                        </div>
	                    	<div class="GridSection" id="gridCustomerGrid">
		                    	<table id="customerGrid123"></table>
	                        </div>
<!-- 	                    </div> -->			
				</div>
			</div>
		</div>
	</div>
</div>
<%-- </s:if> --%>



<div id="searchStyle1EasyUIDialogDivEx"
	style="width: 600px; display: none">
	<div id="searchStyle1EasyUIDialogEx" class="easyui-dialog" title=""
		data-options="closed:true,modal:true">
		<div class="PopupContentMid">
			<div class="GeneralForm Search1Form">
				<label id="seachStyle1CodeLabel" class="LabelStyle Label1Style"	style="width: 100px;">Mã CTTB <span class="ReqiureStyle">*</span></label> 
				<input id="seachStyle1Code" maxlength="47" tabindex="1" type="text"	style="width: 145px;" class="InputTextStyle InputText1Style" /> 
				<label id="seachStyle1NameLabel" class="LabelStyle Label1Style"	style="width: 100px;">Tên CTTB<span class="ReqiureStyle">*</span></label> 
				<input id="seachStyle1Name" type="text" maxlength="97" tabindex="2"	style="width: 145px;" class="InputTextStyle InputText1Style" />
				<div class="Clear"></div>				
				<div class="BtnCenterSection">
					<button id="__btnSaveDisplayProgram" class="BtnGeneralStyle">Sao chép</button>
					<button class="BtnGeneralStyle BtnGeneralMStyle" onclick="$('#searchStyle1EasyUIDialogEx').dialog('close');">Đóng</button>
				</div>
				<p id="errMsgSearch" style="display: none;" class="ErrorMsgStyle SpriteErr"></p>
				<p id="successMsgInfo" class="SuccessMsgStyle"	style="display: none;">Lưu dữ liệu thành công.</p>
			</div>
		</div>
	</div>
</div>


<!-- 
	id: easyuiPopup
	author: LocHP
	16/09/2013
-->
<div style="display: none">
	<div id="easyuiPopup" class="easyui-dialog" title="Upload công văn"
		data-options="closed:true,modal:true"
		style="width: 485px; height: 190px;">
		<div class="PopupContentMid">
			<div class="GeneralForm ImportExcel1Form">
				<label class="LabelStyle Label1Style">File pdf</label>
				<form action="/programme-display/uploadPdfFile" name="importFrm"
					id="uploadFrm" method="post" enctype="multipart/form-data">
					<div class="UploadFileSection Sprite1">
						<input name="fakePdffilepc" id="fakePdffilepc" type="text"
							class="InputTextStyle InputText1Style"> <input
							type="file" class="UploadFileStyle" size="1" name="pdfFile"
							id="pdfFile" onchange="uploadPdfFile(this,'importFrm');" /> <input
							type="hidden" id="isView" name="isView">
					</div>
				</form>
				<div class="Clear"></div>
				<div class="BtnCenterSection">
					<button class="BtnGeneralStyle"
						onclick="return DisplayProgram.uploadPdfFile();">Upload</button>
					<button class="BtnGeneralStyle BtnGeneralMStyle"
						onclick="$('#easyuiPopup').window('close');">Đóng</button>
				</div>
			</div>
			<p style="display: none; margin-top: 10px; margin-left: 10px"
				id="errPdfMsg" />
			<p
				style="display: none; margin-top: 10px; margin-left: 10px; color: red"
				id="resultPdfMsg" />
		</div>
	</div>
</div>
<div style="display: none">
	<div id="easyuiPopup1" class="easyui-dialog" title="Nhập CTTB - Khách hàng Excel" data-options="closed:true,modal:true"  style="width:485px;height:215px;">
	      <div class="PopupContentMid">
	       	<div class="GeneralForm ImportExcel1Form">
	       		<label class="LabelStyle Label1Style">File excel</label>
				<form action="/programme-display/customer/importCustomer" name="importFrmCus" id="importFrmCus" method="post" enctype="multipart/form-data">
					<div class="UploadFileSection Sprite1">
						<input id="fakefilepcCustomer" readonly="readonly" type="text" class="InputTextStyle InputText1Style"> <input type="file" class="UploadFileStyle" size="1" name="excelFileCus" id="excelFileCus"
							onchange="previewImportExcelFile(this,'importFrmCus','fakefilepcCustomer');" /> <input type="hidden" id="isView" name="isView">
						<%-- <input type="hidden" name="token" value="<s:property value="token" />" id="importToken"  /> --%>
					</div>
				</form>
				<div class="Clear"></div>
				<p class="DownloadSFileStyle">
					<a id="downloadTemplateCustomer" href="javascript:void(0)" class="Sprite1" onclick="">Tải file excel mẫu</a>
				</p>
				<div class="BtnCenterSection">
					<s:if test="staffRole!=9">
						<button class="BtnGeneralStyle" onclick="return ProgrammeDisplayCatalog.importExcelCustomer();">Tải lên</button>
					</s:if>
					<button class="BtnGeneralStyle BtnGeneralMStyle" onclick="$('#easyuiPopup1').window('close');">Đóng</button>
				</div>
				<div class="Clear"></div>
				<p class="ErrorMsgStyle" style="display: none; margin-top: 10px; float: left; text-align: left;" id="errExcelMsg" />
					<p style="display: none; margin-top: 10px; float: left; text-align: left;" id="resultExcelMsg" />
			</div>
		</div>
	</div>
</div>
      
<s:hidden id="shopCode" name="shopCode"></s:hidden>
<s:hidden id="curShopId" name="currentShopId"></s:hidden>

<div style="display: none; width: 100%" id="divWidth"></div>
<input type="hidden" id="solg" value="" />
<input type="hidden" id="width" value="" />
<input type="hidden" id="staffRoleHide"	value='<s:property value="staffRole" />'/>
<input type="hidden" id="statusPermission"	value='<s:property value="status" />'/>
<input type="hidden" id="permissionUser" value="<s:property value="permissionUser"/>" />
<s:hidden id="aPer" name="activePermission"></s:hidden>
<s:hidden id="ePer" name="permissionEdit"></s:hidden>
<s:hidden id="csPer" name="commercialSupportPermission"></s:hidden>
<s:hidden id="dPer" name="developCustomer"></s:hidden>
<s:hidden id="dEdit" name="devIsUpdate"></s:hidden>
<s:hidden id="hidStatus" name="status"></s:hidden>
<s:hidden id="shopCode" name="shopCode"></s:hidden>
<s:hidden id="shopId" name="currentShopId"></s:hidden>
<input type="hidden" id="filterMonth"/>
<input type="hidden" id="totalRowGrid" value="0" />

<script type="text/javascript">
	$(document).ready(function() {
		var flag = $('#staffRoleHide').val().trim();
		var fstatus = $('#hidStatus').val().trim();
		
		$("#shop").kendoMultiSelect({
	        dataTextField: "shopCode",
	        dataValueField: "shopId",
	        filter: "contains",
			itemTemplate: function(data, e, s, h, q) {
				var level = data.isLevel;
				if(level == 1) {//VNM
					return '<div class="tree-vnm" node-id="'+Utils.XSSEncode(data.shopId)+'" style="width:230px"><span class="tree-vnm-node"><div style="width: 15px; display: inline-block;"></div></span><span class="tree-vnm-text">'+Utils.XSSEncode(data.shopCode) + ' - ' + Utils.XSSEncode(data.shopName)+'</span></div>';
				} else if(level == 2) {
						return '<div class="tree-vnm" node-id="'+Utils.XSSEncode(data.shopId)+'" style="width:230px"><span class="tree-vnm-node"><div style="width: 15px; display: inline-block;"></div></span><span class="tree-vnm-text">'+Utils.XSSEncode(data.shopCode) + ' - ' + Utils.XSSEncode(data.shopName)+'</span></div>';
				}else if(level == 3){
					return '<div class="tree-mien" node-id="'+Utils.XSSEncode(data.shopId)+'" style="width:220px;"><span class="tree-mien-node"><div style="width: 15px; display: inline-block;"></div></span><span class="tree-mien-text">'+Utils.XSSEncode(data.shopCode) + ' - ' + Utils.XSSEncode(data.shopName)+'</span></div>';
				}else if(level == 4){
					return '<div class="tree-vung" node-id="'+Utils.XSSEncode(data.shopId)+'" style="width:200px"><span class="tree-vung-node"><div style="width: 15px; display: inline-block;"></div></span><span class="tree-vung-text">'+Utils.XSSEncode(data.shopCode) + ' - ' + Utils.XSSEncode(data.shopName)+'</span></div>';
				}else if(level == 5){
					return '<div class="tree-npp" node-id="'+Utils.XSSEncode(data.shopId)+'" style="display:-moz-grid-group"><span class="tree-npp-node"><div style="width: 15px; display: inline-block;"></div></span><span class="tree-npp-text">'+Utils.XSSEncode(data.shopCode) + '-' + Utils.XSSEncode(data.shopName)+'</span></div>';
				} else {
					return '<div class="tree-npp" node-id="'+Utils.XSSEncode(data.shopId)+'" style="display:-moz-grid-group"><span class="tree-npp-node"><div style="width: 15px; display: inline-block;"></div></span><span class="tree-npp-text">'+Utils.XSSEncode(data.shopCode) + '-' + Utils.XSSEncode(data.shopName)+'</span></div>';
				}
			},
	        tagTemplate:  '#: data.shopCode #',
	        dataSource: {
	            transport: {
	                read: {
	                    dataType: "json",
	                    url: "/rest/report/shop/kendo-ui-combobox-ho.json"
	                }
	            }
	        },
	        change: function(e) {
				var params = new Object();
				params.month=$('#month1').val();
				var shopKendo = $("#shop").data("kendoMultiSelect");
                var lstShopId = shopKendo.value();
				params.listShopId = lstShopId;
				Utils.getHtmlDataByAjax(params, '/programme-display/customer/loadDisplayProgram', function(data) {
					try {
						var _data = JSON.parse(data);
					} catch (e) {
						$('#displayProgram').html(data);
						$('.CustomStyleSelectBoxInner').html("Tất cả");
					}
				}, '', 'GET');
            },
	        value: [$('#shopId').val()]
	    });
		
		/* $("#shop").kendoMultiSelect({
            dataTextField: "shopCode",
            dataValueField: "id",
			itemTemplate: function(data, e, s, h, q) {
				//if(data.parentShop == null || data.parentShop == undefined) {//VNM
				if(data.type.channelTypeCode == 'VNM') {//VNM
					return '<div class="tree-vnm" node-id="'+Utils.XSSEncode(data.id)+'" style="width:230px"><span class="tree-vnm-node"><div style="width: 15px; display: inline-block;"></div></span><span class="tree-vnm-text">'+Utils.XSSEncode(data.shopCode) + ' - ' + Utils.XSSEncode(data.shopName)+'</span></div>';
					//return data.shopCode + ' - ' + data.shopName;
				//} else if(data.parentShop != null && data.parentShop != undefined 
						//&& (data.parentShop.parentShop == null || data.parentShop.parentShop == undefined)) {//Mien
				}else if(data.type.channelTypeCode == 'MIEN'){
					return '<div class="tree-mien" node-id="'+Utils.XSSEncode(data.id)+'" style="width:220px;"><span class="tree-mien-node"><div style="width: 15px; display: inline-block;"></div></span><span class="tree-mien-text">'+Utils.XSSEncode(data.shopCode) + ' - ' + Utils.XSSEncode(data.shopName)+'</span></div>';
				//} else if(data.parentShop != null && data.parentShop != undefined && data.parentShop.parentShop != null && data.parentShop.parentShop != undefined 
						//&& (data.parentShop.parentShop.parentShop == null || data.parentShop.parentShop.parentShop == undefined)) {//Vung
				}else if(data.type.channelTypeCode == 'VUNG'){
					return '<div class="tree-vung" node-id="'+Utils.XSSEncode(data.id)+'" style="width:200px"><span class="tree-vung-node"><div style="width: 15px; display: inline-block;"></div></span><span class="tree-vung-text">'+Utils.XSSEncode(data.shopCode) + ' - ' + Utils.XSSEncode(data.shopName)+'</span></div>';
				//} else if(data.parentShop != null && data.parentShop != undefined && data.parentShop.parentShop != null && data.parentShop.parentShop != undefined && data.parentShop.parentShop.parentShop != null && data.parentShop.parentShop.parentShop != undefined 
						//&& (data.parentShop.parentShop.parentShop.parentShop == null || data.parentShop.parentShop.parentShop.parentShop == undefined)) {//NPP
				}else if(data.type.channelTypeCode == 'NPP'){
					return '<div class="tree-npp" node-id="'+Utils.XSSEncode(data.id)+'" style="display:-moz-grid-group"><span class="tree-npp-node"><div style="width: 15px; display: inline-block;"></div></span><span class="tree-npp-text">'+Utils.XSSEncode(data.shopCode) + '-' + Utils.XSSEncode(data.shopName)+'</span></div>';
				}
			},
            tagTemplate:  '#: data.shopCode #',			  
            dataSource: {
                transport: {
                    read: {
                        dataType: "json",
                        url: "/rest/report/shop/kendoui-combobox.json"
                    }
                }
            },
            change: function(e) {
				var params = new Object();
				params.month=$('#month1').val();
				var shopKendo = $("#shop").data("kendoMultiSelect");
                var lstShopId = shopKendo.value();
				params.listShopId = lstShopId;
				Utils.getHtmlDataByAjax(params,
						'/programme-display/customer/loadDisplayProgram',
						function(data) {
							try {
								var _data = JSON.parse(data);
							} catch (e) {

								$('#displayProgram').html(data);
								$('.CustomStyleSelectBoxInner').html("Tất cả");
							}
						}, '', 'GET');
            },
            //value: [{ shopCode: $('#shopCode').val(), id: $('#shopId').val()}]
            value: [$('#shopId').val()]
        }); */
		$('#errCustomerMsg').html('').hide();
		ProgrammeDisplayCatalog._params = new Object();
		ProgrammeDisplayCatalog._lstProduct = new Map();
		ProgrammeDisplayCatalog._lstCustomerId = new Map();
		ProgrammeDisplayCatalog._lstSize = new Array();
		$('#downloadTemplateCustomer').attr('href',excel_template_path + 'display-program/CTTB_KhachHang.xls');
		$('.easyui-dialog #downloadTemplateCustomer').attr('href',excel_template_path + 'display-program/CTTB_KhachHang.xls');
		var params = new Object();
		var now = new Date();
		var cYear = now.getFullYear();
		var cMonth = now.getMonth() + 1;
		if(cMonth != 10 && cMonth != 11 && cMonth != 12  ){
			cMonth = '0'+cMonth;
		}
		var currentDate = cMonth+'/'+cYear;
		$('#month1').val(currentDate);
		var month = $('#month1').val();
		$('#filterMonth').val(month);
		var params = new Object();
		params.month=$('#month1').val();
		var shopKendo = $("#shop").data("kendoMultiSelect");
        var lstShopId = shopKendo.value();
		params.listShopId = lstShopId;
		applyMonthPicker('month1');
		Utils.bindFormatOnTextfield('month1',Utils._TF_NUMBER_CONVFACT);
		Utils.bindAutoSearch();
		
		ProgrammeDisplayCatalog.loadDisplayProgram(params);
		$('#month1').bind('change',function(){
			var params = new Object();
			params.month=$('#month1').val();
			var shopKendo = $("#shop").data("kendoMultiSelect");
            var lstShopId = shopKendo.value();
			params.listShopId = lstShopId;
			Utils.getHtmlDataByAjax(params, '/programme-display/customer/loadDisplayProgram', function(data) {
				try {
					var _data = JSON.parse(data);
				} catch (e) {

					$('#displayProgram').html(data);
					$('.CustomStyleSelectBoxInner').html("Tất cả");
				}
			}, '', 'GET');
		});
		$('#customerGrid123').datagrid({
			url :'/programme-display/customer/searchEX',//?id='+$('#displayProgram').val() +'&listShopIdString='+$("#shop").data("kendoMultiSelect").value().toString()+'&month='+$('#month1').val(),
			autoRowHeight : true,
			rownumbers : true, 
			checkOnSelect :true,
			pagination:true,
			//singleSelect:true,			
			queryParams:{
				id:$('#displayProgram').val(),
				listShopIdString:$("#shop").data("kendoMultiSelect").value().toString(),
				month:$('#month1').val()
			},
			fitColumns:true, 
			scrollbarSize : 0,
			pageSize:50,
			pageNumber:1,
			width : $(window).width()-48, 
			//width: $('#customerDisplayGrid').width(),
			columns:[[  
	        	{field: 'displayProgramCode',title:'Mã CTTB', width:70,align:'left',sortable : false,resizable : false,formatter :function(value,row,index) {
					return Utils.XSSEncode(value);
				}},
	        	{field: 'levelCode',title:'Mức CTTB', width:20,align:'left',sortable : false,resizable : false,formatter :function(value,row,index) {
					return Utils.XSSEncode(value);
				}},  
	        	{field: 'shopCode',title:'Mã NPP', width:50,align:'left',sortable : false,resizable : false,formatter :function(value,row,index) {
					return Utils.XSSEncode(value);
				}},  
	        	{field: 'staffName',title:'NVBH', width:100,align:'left',sortable : false,resizable : false,formatter :function(value,row,index) {
					return Utils.XSSEncode(value);
				}},  
	        	{field: 'customerName',title:'Khách hàng', width:100,align:'left',sortable : false,resizable : false,formatter :function(value,row,index) {
					return Utils.XSSEncode(value);
				}},  
				{field: 'address',title:'Địa chỉ', width:100,align:'left',sortable : false,resizable : false,formatter :function(value,row,index) {
					return Utils.XSSEncode(row.houseNumber) + " " + Utils.XSSEncode(row.street);
				}},
	        	{field: 'fromDate',title:'Tháng', width:50,align:'center',sortable : false,resizable : false,formatter:function(row,index){
		        		return Utils.XSSEncode(row);
	        		}
	        	},
	        	{field: 'oneMonth',title:'Ngày tạo', width:50,align:'center',sortable : false,resizable : false,formatter:function(row,index){
		        		return Utils.XSSEncode(row);
	        		}
	        	}, 
	        	{field: 'updateDate',title:'Ngày điều chỉnh', width:60,align:'center',sortable : false,resizable : false,formatter:function(row,index){
		        		return Utils.XSSEncode(row);
	        		}
	        	}, 
	        	{field: 'id', checkbox:true, align:'center', width:80,sortable : false,resizable : false}
	        ]],
	        onLoadSuccess :function(data){
		    	$('.datagrid-header-rownumber').html('STT');
		    	var originalSize = $('.datagrid-header-row td[field=displayProgramCode] div').width();
		    	if(originalSize != null && originalSize != undefined){
		    		ProgrammeDisplayCatalog._lstSize.push(originalSize);
		    	}
		    	$('input[name="id"]').each(function(){
					var temp = ProgrammeDisplayCatalog._lstProduct.get($(this).val());
					if(temp!=null) $(this).attr('checked','checked');
				});
		    	var length = 0;
		    	$('input[name="id"]').each(function(){
		    		if($(this).is(':checked')){
		    			++length;
		    		}
		    	});	    	
		    	if(data.rows.length==length){
		    		$('.datagrid-header-check input').attr('checked',true);
		    	}else{
		    		$('.datagrid-header-check input').attr('checked',false);
		    	}
				$('#totalRowGrid').val(data.total);
		    },
		    onCheck:function(i,r){
		    	$('#checkDeleteAll').removeAttr('checked');
		    	ProgrammeDisplayCatalog._lstProduct.put(r.id,r);
		    },
		    onUncheck:function(i,r){
		    	ProgrammeDisplayCatalog._lstProduct.remove(r.id);
		    },
		    onCheckAll:function(r){
		    	$('#checkDeleteAll').removeAttr('checked');
		    	for(i=0;i<r.length;i++){
		    		ProgrammeDisplayCatalog._lstProduct.put(r[i].id,r[i]);
		    	}
		    },
		    onUncheckAll:function(r){
		    	for(i=0;i<r.length;i++){
		    		ProgrammeDisplayCatalog._lstProduct.remove(r[i].id);
		    	}
		    }
		});

});
</script>
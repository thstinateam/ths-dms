<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@page import="ths.dms.helper.Configuration"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<s:hidden id="permissionUser" name="permissionUser"></s:hidden>

<div class="BreadcrumbSection">
	<ul class="ResetList FixFloat BreadcrumbList">
		<li class="Sprite1"><a href="javascript:void(0)">Danh mục</a>
		</li>
		<li><span>Danh sách chương trình trưng bày</span>
		</li>
	</ul>
</div>

<div class="CtnOneColSection">
	<div class="ContentSection">
		<div class="ToolBarSection">
			<div class="SearchSection GeneralSSection">
				<h2 class="Title2Style">Thông tin tìm kiếm</h2>
				<div class="SearchInSection SProduct1Form">
					<label class="LabelStyle Label1Style">Mã CTTB</label> <input
						id="code" type="text" class="InputTextStyle InputText1Style"
						maxlength="50" /> <label class="LabelStyle Label1Style">Tên
						CTTB</label> <input id="name" type="text" maxlength="100" style="width:490px;"
						class="InputTextStyle" />
					<div class="Clear"></div>
					<label class="LabelStyle Label1Style">Từ ngày</label> <input
						type="text" id="fDate" class="InputTextStyle InputText6Style" />
					<label class="LabelStyle Label1Style">Đến ngày</label> <input
						type="text" id="tDate" class="InputTextStyle InputText6Style" />

					<label class="LabelStyle Label1Style" style="padding-left: 10px; width:105px">Trạng thái</label>
					<div class="BoxSelect BoxSelect6">
						<s:select id="status" name="status" cssClass="MySelectBoxClass" list="st" listKey="value" listValue="name" headerValue="--Tất cả--" headerKey=""></s:select>
					</div>

					<div class="Clear"></div>
					<div class="BtnCenterSection">
						<button id="btnSearch"
							onclick="return ProgrammeDisplayCatalog.search()"
							class="BtnGeneralStyle">Tìm kiếm</button>
					</div>
					<p style="display: none;" class="ErrorMsgStyle" id="errMsg" />
					<div class="Clear"></div>
				</div>
			</div>
			<div class="GeneralCntSection">
				<h2 class="Title2Style">Danh sách CTTB</h2>
				<div class="GridSection">
					<table id="listCTTBGrid" title="Danh sách CTTB"
						fitColumns="true" data-options="autoRowHeight:false, pagination:true, pageSize:10"></table>
				</div>
				<s:if test="permissionUser">
					<div class="GeneralForm GeneralNoTP1Form">
						<div class="Func1Section">
	                        <p class="DownloadSFileStyle DownloadSFile2Style">
	                        	<a href="javascript:void(0)" id="downloadTemplate" class="Sprite1">Tải file excel mẫu</a>
	                        </p> 
	                        <form action="/programme-display/import-excel" name="importFrm" style="float: left;" id="importFrm"  method="post" enctype="multipart/form-data">
	                        	<input type="file" class="InputFileStyle" size="22" name="excelFile" id="excelFile" onchange="previewImportExcelFile(this,'importFrm');">
	                        	<div class="FakeInputFile">
									<input id="fakefilepc" style="margin: -3px 5px 0 0;" readonly="readonly" type="text" class="InputTextStyle InputText1Style">
								</div>
	                        </form>
	                        <button class="BtnGeneralStyle" onclick="return ProgrammeDisplayCatalog.importExcel();">Nhập từ Excel</button>
	                    </div>
	                    <div class="Clear"></div>
	                    <p style="display: none;" class="ErrorMsgStyle" id="errExcelMsg" />
					</div>
				</s:if>
			</div>
			<div class="Clear"></div>
		</div>
	</div>
	<div class="Clear"></div>
</div>
<s:hidden id="selId" value="0"></s:hidden>
<input type="hidden" id="shopId" />
<script type="text/javascript">
$(document).ready(function(){
	$('#code').focus();
	$('.MySelectBoxClass').customStyle();
	$('#status').width(165);
	//$('#name').width(510);
	$('.BoxSelect .CustomStyleSelectBoxInner').width(135);
	setDateTimePicker('fDate');
	setDateTimePicker('tDate');
	var params = new Object();
	params.code = $('#code').val().trim();
	params.name = $('#name').val().trim();
	params.fDate = $('#fDate').val();
	params.tDate = $('#tDate').val();	
	params.status = 1;
	var title = '';
	if($('#permissionUser').val() == 'true'){
		$('#downloadTemplate').attr('href',excel_template_path + 'catalog/Bieu_mau_import_CTTB.xls');
		var options = {
				beforeSubmit : ProgrammeDisplayCatalog.beforeImportExcel,
				success : ProgrammeDisplayCatalog.afterImportExcelDisplayProgram,
				type : "POST",
				dataType : 'html',
				data : {
					excelType : 1
				}
		};
		$('#importFrm').ajaxForm(options);
		title = '<a href="javascript:void(0)" onclick=\"return ProgrammeDisplayCatalog.getSelectedProgram(0);\"><img width="15" height="16" src="/resources/images/icon_add.png"/></a>';
	}
	$('#listCTTBGrid').datagrid({
		url : "/programme-display/search",
		method : 'GET',
		height: 'auto',
		width : $(window).width() - 35,
		scrollbarSize:0,
		rownumbers:true,
		singleSelect : true,
		queryParams:params,
		columns:[[  
			{field:'displayProgramCode', title:'Mã CTTB', align:'left', width: 250,formatter :function(value,row,index) {
				return Utils.XSSEncode(value);
			}},
			{field:'displayProgramName', title:'Tên CTTB', align:'left', width: 250,formatter :function(value,row,index) {
				return Utils.XSSEncode(value);
			}},
			{field:'fromDate',title:'Từ ngày',width:150, sortable:false, align: 'center',resizable:false , formatter:function(value, row, index) {
				  return toDateString(new Date(Utils.XSSEncode(row.fromDate)));
			}},
			{field:'toDate',title:'Đến ngày',width:150, sortable:false, align: 'center',resizable:false , formatter:function(value, row, index) {
				  var toDate = row.toDate;
			    	if(toDate == null || toDate == undefined || toDate == '' || toDate == 'null'){
			    		toDate = '';
			    	}else{
			    		toDate = formatDate(Utils.XSSEncode(row.toDate));
			    	}
			    	return toDate;
			}},
			{field:'status',title:'Trạng thái', width:150,sortable:false,resizable:false, align: 'center', formatter: function(value,row,index){
				if(Utils.XSSEncode(row.status) == activeStatus){
					return "Hoạt động";
				} else if(Utils.XSSEncode(row.status) == stoppedStatus){
					return "Tạm ngưng";
				} else if(Utils.XSSEncode(row.status) == waitingStatus){
					return "Dự thảo";
				}
			}},
			{field:'detail',title:title, width: 30,sortable:false,resizable:false, align: 'center', formatter: function(value, row, index) {
				  return "<a href='javascript:void(0)' onclick=\"return ProgrammeDisplayCatalog.getSelectedProgram('"+ row.id +"');\"><img width='15' height='16' src='/resources/images/icon-edit.png'/></a>";
			}},
			{field: 'P', hidden : true}
		]],
	  	onLoadSuccess :function(data){	    	
	    	$('.datagrid-header-rownumber').html('STT');	    	
	    },
	    method : 'GET'
	});
});

</script>
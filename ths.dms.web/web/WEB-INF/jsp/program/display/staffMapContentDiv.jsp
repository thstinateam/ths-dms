<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>

<div class="BreadcrumbSection">
	<ul class="ResetList FixFloat BreadcrumbList">		
		<li class="Sprite1"><a href="/programme-display/staff-map/info">Chương trình trưng bày</a></li>
		<li><span>Số suất NVBH</span></li>
	</ul>
</div>

<div class="CtnOneColSection">
	<div class="ContentSection">
		<div class="ToolBarSection">
			<div class="SearchSection GeneralSSection" id="container5">
				<h2 class="Title2Style">Thông tin tìm kiếm</h2>
				<div class="SearchInSection ">	
								<div class="SearchInSection SProduct1Form">	
<!--                         <div class="SearchInSection"> -->
                            <label class="LabelStyle Label1Style" style="width: 50px">Đơn vị</label>                    
							<div style="height: auto;">
								<input id="shop" type="text"  style="width:190px; height: auto;" class="InputTextStyle InputText1Style" />
							</div>
                            <label class="LabelStyle Label1Style">Tháng</label>
                            <input type="text" class="InputTextStyle InputText1Style" id="month" maxlength="7" style="width:90px;"/>
                            <label class="LabelStyle Label1Style">CTTB</label>
                                <div class="BoxSelect BoxSelect5">
                                    <select class="MySelectBoxClass" id="displayProgram">
                                    	<option value="">Tất cả</option>
                                        <s:iterator id="obj" value="listDisplayProgram">
											<option value="<s:property value="displayProgrameId"/>"><s:property value="displayProgrameCode" /></option>
										</s:iterator>
                                    </select>
                                </div>
                            <div class="BtnLSection" style="padding-left:18px;">
                                <button id="btnSearch" class="BtnGeneralStyle" >Tìm kiếm</button>
                            </div>
                            <div class="Clear"></div>                                
<!--                         </div> -->
                        </div>
                        <div class="Clear"></div>
                        <div class="SearchInSection SProduct1Form">	
	                    	<div class="GeneralForm GeneralNoTPForm">
	                        	<div class="Func1Section">
	                        		<s:if test="staffRole!=9">
		                        		<button class="BtnGeneralStyle cmsiscontrol" id="importBtn" onclick= "return ProgrammeDisplayCatalog.showDlgImportExcel();">Nhập từ Excel</button>
		                        	</s:if>
		                        	<button id="btnStaffMapExport" class="BtnGeneralStyle" >Xuất Excel</button>
		                        	<s:if test="staffRole!=9">
		                        		<p class="DownloadSFileStyle"><a id="downloadTemplateCustomer" href="#" class="Sprite1 downloadTemplateNVBH cmsiscontrol">Tải file excel mẫu</a></p>
		                        	</s:if>
	                            </div>
	                           <label class="LabelStyle Label1Style" id="loadShop" style="width:400px;"></label> 
								<label class="LabelStyle Label1Style" id="loadShop" style="width: 100px;margin-top:7px;">
								<s:if test="staffRole!=9">
									<input type="checkbox" id="checkDeleteAll1" onclick="return ProgrammeDisplayCatalog.unCheckDatagrid();" class="cmsiscontrol" />
										<span>&nbsp;Chọn toàn bộ </span>
								</s:if>
								</label>
		                           <div class="Func2Section">
		                           <s:if test="staffRole!=9">
		                            	<button style="width: 70px; margin-right: 15px;" id="deleteBtn" class="BtnGeneralStyle cmsiscontrol" onclick="return ProgrammeDisplayCatalog.deleteQuantityStaff();">Xóa</button>
		                            </s:if>
		                           </div>	                            
	                        </div>
	                        </div>
	                        <div class="Clear"></div>	                        
							<p id="errMsgStaff" class="ErrorMsgStyle" style="display: none"></p>
							<p id="successMsgStaff" class="SuccessMsgStyle"	style="display: none"></p>
	                    	<div class="GridSection" id="staffGridContainer">
								<table id="staffGrid"></table>
								<div id="staffPager"></div>
							</div>					
				</div>			
				
			</div>
		</div>
	</div>
</div>

<!--  import nhan vien phat trien khach hang -->
<!-- <div id = "responseDiv" style="display:none;"></div> -->
<div class="SProduct1Form" style="display: none">
	<div id="easyuiPopup" class="easyui-dialog" title="CTTB - Số suất NVBH" data-options="closed:true,modal:true" style="width:465px;height:250px;">
		<div class="PopupContentMid">
			<div class="GeneralForm ImportExcel1Form">
			<form action="/programme-display/staff-map/import-staff" name="importFrm" id="importFrm" method="post" enctype="multipart/form-data">						            	
				<label class="LabelStyle Label1Style">File excel</label>
					<div class="UploadFileSection Sprite1">
						<input id="fakefilepc" type="text" class="InputTextStyle InputText1Style">
						<input type="file" class="UploadFileStyle" size="1" name="excelFile" id="excelFile" onchange="previewImportExcelFile(this,'importFrm');" />						               		
						<%-- <input type="hidden" name="token" value="<s:property value="token" />" id="importToken"  /> --%>
					</div>
			</form>
			<div class="Clear"></div>
			<p class="DownloadSFileStyle"><a onclick="" class="Sprite1 downloadTemplateNVBH" >Tải file excel mẫu</a></p>
            <div class="Clear"></div>
            <div class="BtnCenterSection">
            <s:if test="staffRole!=9">
   	            <button class="BtnGeneralStyle" onclick="return ProgrammeDisplayCatalog.importExcelStaff();">Tải lên</button>
   	           </s:if>
                <button class="BtnGeneralStyle BtnGeneralMStyle" onclick="$('#easyuiPopup').window('close');">Đóng</button>
	       	</div>
		    </div>
		</div>
	<p style="display:none;margin-top:10px;margin-left: 10px" class="ErrorMsgStyle SpriteErr" id="errExcelMsg"/>
	<p style="display:none;margin-top:10px;margin-left: 10px" id="resultExcelMsg" class="ErrorMsgStyle SpriteErr"/>
	</div>
</div>

<s:hidden id="shopCode" name="shopCode"></s:hidden>
<s:hidden id="curShopId" name="currentShopId"></s:hidden>
<s:hidden id="shopId" name="currentShopId"></s:hidden>
<s:hidden id="staffRole" name="staffRole"></s:hidden>

<script type="text/javascript">
	$(document).ready(function() {
		var StaffRole = $('#staffRole').val();
		$('.BtnGeneralStyle').css('color','#FFF');
		
		$("#shop").kendoMultiSelect({
	        dataTextField: "shopCode",
	        dataValueField: "shopId",
	        filter: "contains",
			itemTemplate: function(data, e, s, h, q) {
				var level = data.isLevel;
				if(level == 1) {//VNM
					return '<div class="tree-vnm" node-id="'+Utils.XSSEncode(data.shopId)+'" style="width:230px"><span class="tree-vnm-node"><div style="width: 15px; display: inline-block;"></div></span><span class="tree-vnm-text">'+Utils.XSSEncode(data.shopCode) + ' - ' + Utils.XSSEncode(data.shopName)+'</span></div>';
				} else if(level == 2) {
						return '<div class="tree-vnm" node-id="'+Utils.XSSEncode(data.shopId)+'" style="width:230px"><span class="tree-vnm-node"><div style="width: 15px; display: inline-block;"></div></span><span class="tree-vnm-text">'+Utils.XSSEncode(data.shopCode) + ' - ' + Utils.XSSEncode(data.shopName)+'</span></div>';
				}else if(level == 3){
					return '<div class="tree-mien" node-id="'+Utils.XSSEncode(data.shopId)+'" style="width:220px;"><span class="tree-mien-node"><div style="width: 15px; display: inline-block;"></div></span><span class="tree-mien-text">'+Utils.XSSEncode(data.shopCode) + ' - ' + Utils.XSSEncode(data.shopName)+'</span></div>';
				}else if(level == 4){
					return '<div class="tree-vung" node-id="'+Utils.XSSEncode(data.shopId)+'" style="width:200px"><span class="tree-vung-node"><div style="width: 15px; display: inline-block;"></div></span><span class="tree-vung-text">'+Utils.XSSEncode(data.shopCode) + ' - ' + Utils.XSSEncode(data.shopName)+'</span></div>';
				}else if(level == 5){
					return '<div class="tree-npp" node-id="'+Utils.XSSEncode(data.shopId)+'" style="display:-moz-grid-group"><span class="tree-npp-node"><div style="width: 15px; display: inline-block;"></div></span><span class="tree-npp-text">'+Utils.XSSEncode(data.shopCode) + '-' + Utils.XSSEncode(data.shopName)+'</span></div>';
				} else {
					return '<div class="tree-npp" node-id="'+Utils.XSSEncode(data.shopId)+'" style="display:-moz-grid-group"><span class="tree-npp-node"><div style="width: 15px; display: inline-block;"></div></span><span class="tree-npp-text">'+Utils.XSSEncode(data.shopCode) + '-' + Utils.XSSEncode(data.shopName)+'</span></div>';
				}
			},
	        tagTemplate:  '#: data.shopCode #',
	        dataSource: {
	            transport: {
	                read: {
	                    dataType: "json",
	                    url: "/rest/report/shop/kendo-ui-combobox-ho.json"
	                }
	            }
	        },
	        change: function(e) {
	        	var params = new Object();
    			params.month=$('#month').val();			
    			params.listShopId = $("#shop").data("kendoMultiSelect").value();
    			Utils.getHtmlDataByAjax(params,'/programme-display/staff-map/loading-displayprogram',
    				function(data) {try {var _data = JSON.parse(data);} catch (e) {
    					$('#displayProgram').html(data);
    					$('.CustomStyleSelectBoxInner').html("Tất cả");
    				}
    			}, '', 'GET');
            },
	        value: [$('#shopId').val()]
	    });
		
		/* $("#shop").kendoMultiSelect({
            dataTextField: "shopCode",
            dataValueField: "id",
			itemTemplate: function(data, e, s, h, q) {
				//if(data.parentShop == null || data.parentShop == undefined) {//VNM
				if(data.type.channelTypeCode == 'VNM') {//VNM
					return '<div class="tree-vnm" node-id="'+Utils.XSSEncode(data.id)+'" style="width:230px"><span class="tree-vnm-node"><div style="width: 15px; display: inline-block;"></div></span><span class="tree-vnm-text">'+Utils.XSSEncode(data.shopCode) + ' - ' + Utils.XSSEncode(data.shopName)+'</span></div>';
					//return data.shopCode + ' - ' + data.shopName;
				//} else if(data.parentShop != null && data.parentShop != undefined 
						//&& (data.parentShop.parentShop == null || data.parentShop.parentShop == undefined)) {//Mien
				}else if(data.type.channelTypeCode == 'MIEN'){
					return '<div class="tree-mien" node-id="'+Utils.XSSEncode(data.id)+'" style="width:220px;"><span class="tree-mien-node"><div style="width: 15px; display: inline-block;"></div></span><span class="tree-mien-text">'+Utils.XSSEncode(data.shopCode) + ' - ' + Utils.XSSEncode(data.shopName)+'</span></div>';
				//} else if(data.parentShop != null && data.parentShop != undefined && data.parentShop.parentShop != null && data.parentShop.parentShop != undefined 
						//&& (data.parentShop.parentShop.parentShop == null || data.parentShop.parentShop.parentShop == undefined)) {//Vung
				}else if(data.type.channelTypeCode == 'VUNG'){
					return '<div class="tree-vung" node-id="'+Utils.XSSEncode(data.id)+'" style="width:200px"><span class="tree-vung-node"><div style="width: 15px; display: inline-block;"></div></span><span class="tree-vung-text">'+Utils.XSSEncode(data.shopCode) + ' - ' + Utils.XSSEncode(data.shopName)+'</span></div>';
				//} else if(data.parentShop != null && data.parentShop != undefined && data.parentShop.parentShop != null && data.parentShop.parentShop != undefined && data.parentShop.parentShop.parentShop != null && data.parentShop.parentShop.parentShop != undefined 
						//&& (data.parentShop.parentShop.parentShop.parentShop == null || data.parentShop.parentShop.parentShop.parentShop == undefined)) {//NPP
				}else if(data.type.channelTypeCode == 'NPP'){
					return '<div class="tree-npp" node-id="'+Utils.XSSEncode(data.id)+'" style="display:-moz-grid-group"><span class="tree-npp-node"><div style="width: 15px; display: inline-block;"></div></span><span class="tree-npp-text">'+Utils.XSSEncode(data.shopCode) + '-' + Utils.XSSEncode(data.shopName)+'</span></div>';
				}
			},
            tagTemplate:  '#: data.shopCode #',			  
            dataSource: {
                transport: {
                    read: {
                        dataType: "json",
                        url: "/rest/report/shop/kendoui-combobox.json"
                    }
                }
            },
            change: function(e) {
            	var params = new Object();
    			params.month=$('#month').val();			
    			params.listShopId = $("#shop").data("kendoMultiSelect").value();
    			Utils.getHtmlDataByAjax(params,'/programme-display/staff-map/loading-displayprogram',
    				function(data) {try {var _data = JSON.parse(data);} catch (e) {
    					$('#displayProgram').html(data);
    					$('.CustomStyleSelectBoxInner').html("Tất cả");
    				}
    			}, '', 'GET');
            },
            //value: [{ shopCode: $('#shopCode').val(), id: $('#shopId').val()}]
            value: [$('#shopId').val()]
        }); */
		applyMonthPicker('month');
		Utils.bindFormatOnTextfield('month',Utils._TF_NUMBER_CONVFACT);		
		$('#month').val(getCurrentMonth());
		Utils.bindAutoSearch();
		
		ProgrammeDisplayCatalog._mapStaff = new Map();
		ProgrammeDisplayCatalog._lstSize = new Array();
		$('#month').bind('change',function(){
			var params = new Object();
			params.month=$('#month').val();			
			params.listShopId = $("#shop").data("kendoMultiSelect").value();
			Utils.getHtmlDataByAjax(params,'/programme-display/staff-map/loading-displayprogram',
				function(data) {try {var _data = JSON.parse(data);} catch (e) {
					$('#displayProgram').html(data);
					$('.CustomStyleSelectBoxInner').html("Tất cả");
				}
			}, '', 'GET');
		});	
		$('#month').change();
		$('.downloadTemplateNVBH').attr('href',excel_template_path+'/display-program/Bieu_mau_import_so_suat_nvbh_cttb.xls');
		$('#btnSearch').bind('click',function(){
			$('.ErrorMsgStyle').hide();
			$('#checkDeleteAll1').removeAttr('checked');
			var month = $('#month').val().trim();
			ProgrammeDisplayCatalog._mapStaff = new Map();
			var params = new Object();
			params.id = $('#displayProgram').val().trim();
			params.strShopIds = $("#shop").data("kendoMultiSelect").value().toString();
			params.month = $('#month').val().trim();
			$("#staffGrid").datagrid('reload',params);
			$("#staffGrid").datagrid('uncheckAll');
			
			if(StaffRole!=StaffRoleType.GSNPP && StaffRole!=StaffRoleType.VNM){
				if(!Utils.compareCurrentMonthEx(month)){
					$('#checkDeleteAll1').attr('disabled',true);
					$('#checkDeleteAll1').attr('checked',false);
				}else{
					$('#checkDeleteAll1').attr('disabled',false);
				}
			}
		});
			
		
		$('#btnStaffMapExport').bind('click',function(){
			$('.ErrorMsgStyle').hide();
			var params = new Object();
			params.id = $('#displayProgram').val().trim();
			params.strShopIds = $("#shop").data("kendoMultiSelect").value().toString();
			params.month = $('#month').val().trim();
			$.messager.confirm('Xác nhận','Bạn có muốn xuất file?',function(r){
				if(r){
					ReportUtils.exportReport('/programme-display/staff-map/export-Excel-Staff', params, 'errMsgStaff');					
				}
			});		
		});
		
		$('#staffGrid').datagrid({
			url : '/programme-display/staff-map/search-staff',
			autoRowHeight : true,
			rownumbers : true, 
			checkOnSelect :true,
			pagination:true,
			fitColumns:true,
			scrollbarSize : 0,
			pageSize:20,
			pageList  :[10,20,30,40,50],
			width : $(window).width() - 55,
			queryParams:{				
				month:getCurrentMonth()
			},
			columns:[[
			{field: 'displayProgramCode',title: 'Mã CTTB', width:120,align:'left',sortable : false,resizable : false,
				formatter: function(value,row,index){
					return Utils.XSSEncode(row.displayProgram.displayProgramCode);
				}
			},
			{field: 'shopCode',title: 'Mã NPP', width:100,align:'left',sortable : false,resizable : false,
				formatter: function(value,row,index){
					return Utils.XSSEncode(row.staff.shop.shopCode);
				}
			},			
			{field: 'staffCode',title: 'Mã NVBH', width:120,align:'left',sortable : false,resizable : false,
				formatter: function(value,row,index){
					return Utils.XSSEncode(row.staff.staffCode); 
				}
			},
			{field: 'staffName',title: 'Tên NVBH', width:220,align:'left',sortable : false,resizable : false,
				formatter: function(value,row,index){
					return Utils.XSSEncode(row.staff.staffName); 
				}
			},
			{field: 'month',title: 'Tháng', width:80,align:'center',sortable : false,resizable : false,
				formatter: function(cellValue,row,index){
					if(cellValue!=undefined && cellValue!=null){				
						return $.datepicker.formatDate('mm/yy', new Date(cellValue));
					} 
				}
			},
			{field: 'quantityMax',title: 'Số suất', width:85,align:'right',sortable : false,resizable : false,
				formatter:function(value){
		    		return formatCurrency(value);	
		    	}
			},	
			{field: 'quantityReceived',title: 'Đã phân bổ', width:100,align:'right',sortable : false,resizable : false,
				formatter:function(value){
		    		return formatCurrency(value);	
		    	}
			},	
			{field: 'uploadDate',title: 'Ngày tạo', width:100,align:'center',sortable : false,resizable : false,
				formatter: function(cellValue,row,index){
					if(cellValue!=undefined && cellValue!=null){				
						return $.datepicker.formatDate('dd/mm/yy', new Date(cellValue));
					} 
				}
			},
			{field: 'updateDate',title: 'Ngày điều chỉnh', width:100,align:'center',sortable : false,resizable : false,
				formatter: function(cellValue,row,index){
					if(cellValue!=undefined && cellValue!=null){				
						return $.datepicker.formatDate('dd/mm/yy', new Date(cellValue));
					} 
				}
			},
			{field:'id', checkbox:true, align:'center', width:50,sortable : false,resizable : false}
            ]],
            onLoadSuccess:function(){
            	$('.datagrid-header-rownumber').html('STT');            	
            	$('.datagrid-header-check input').removeAttr('checked');
            	//$('#checkDeleteAll1').removeAttr('checked');
            	$('.datagrid-header-row td[field=id] input[type=checkbox]').attr('disabled',false);
            	var rows = $('#staffGrid').datagrid('getRows');
		    	for(var i = 0; i < rows.length; i++) {
		    		if(ProgrammeDisplayCatalog._mapStaff.get(rows[i].id)!=null) {
		    			$('#staffGrid').datagrid('checkRow', i);
		    		}
		    	}
		    	var month = $('#month').val().trim();
		    	//var checkRoleType = StaffRole!=StaffRoleType.NVGS && StaffRole!=StaffRoleType.NHVNM;
		    	var checkRoleType = !(_isFullPrivilege || _MapControl.get("deleteBtn") == 2);
            	if(!Utils.compareCurrentMonthEx(month) || checkRoleType) {
            		$('.datagrid-row td[field=id] input[type=checkbox]').attr('disabled','disabled');
            		$('.datagrid-header-row td[field=id] input[type=checkbox]').attr('disabled','disabled');
            	}
            	var originalSize = $('.datagrid-header-row td[field=staffName] div').width();
		    	if(originalSize != null && originalSize != undefined){
		    		ProgrammeDisplayCatalog._lstSize.push(originalSize);
		    	}
		    	ProgrammeDisplayCatalog.updateRownumWidthForJqGridEX1('#staffGrid','staffName');
            	
            },
            onClickRow:function(index,row){
            	var month = $('#month').val().trim();
            	if(!Utils.compareCurrentMonthEx(month) || (StaffRole!=StaffRoleType.GSNPP && StaffRole!=StaffRoleType.VNM)) {   
            		$('#staffGrid').datagrid('uncheckRow',index);
            	}
            },
			onCheck:function(index,row){
           		ProgrammeDisplayCatalog._mapStaff.put(row.id,row);   
           		$('#checkDeleteAll1').removeAttr('checked');
		    },
		    onUncheck:function(index,row){
		    	ProgrammeDisplayCatalog._mapStaff.remove(row.id);
		    },
		    onCheckAll:function(rows){
		    	for(var i = 0;i<rows.length;i++){
		    		var row = rows[i];
		    			ProgrammeDisplayCatalog._mapStaff.put(row.id,row);
		    	}
		    	$('#checkDeleteAll1').removeAttr('checked');
		    },
		    onUncheckAll:function(rows){
		    	for(var i = 0;i<rows.length;i++){
		    		var row = rows[i];
		    		ProgrammeDisplayCatalog._mapStaff.remove(row.id);
		    	}
		    }
		});
		$('#btnSearch').removeAttr('style');
		$('#importBtn').removeAttr('style');
		$('#btnStaffMapExport').removeAttr('style');
		$('#deleteBtn').removeAttr('style').attr('style','width: 70px; margin-right: 15px;');
	});
</script>
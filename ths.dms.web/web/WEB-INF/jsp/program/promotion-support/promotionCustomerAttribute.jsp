<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>

<div id="tab7" class="SearchInSection SProduct3Form">
	<div class="SProductForm1Cols" >
    	<h2 class="Title2Style">Danh sách thuộc tính</h2>
        <div class="PopupContentMid" style="height: 239px;overflow-y: scroll;">
            <ul class="ResetList PropertiesList" id="left">
            	<s:iterator value="lstPromotionCustAttrVO">
            		<s:if test="objectType!=null && (objectType==2 )">
            		<script type="text/javascript" charset="UTF-8">
            			PromotionSupport._hasCustomerType = true;
            		</script>
            		</s:if>
            		<s:elseif test="objectType!=null && (objectType==3 )">
             		<script type="text/javascript" charset="UTF-8">
             		PromotionSupport._hasSaleLevel = true;
             		</script>
            		</s:elseif>
            		<s:elseif test="objectType!=null && (objectType==4 )">
             		<script type="text/javascript" charset="UTF-8">
             		PromotionSupport._hasCustomerCardType = true;
             		</script>
            		</s:elseif>
            		<s:else>
            			<li>
            				<input name="<s:property value="name"/>" objectType="<s:property value="objectType"/>" objectId="<s:property value="objectId"/>" type="checkbox"  class="InputCbxStyle"/><label class="LabelStyle Label3Style" style="width:150px"><s:property value="name"/></label>
            			</li>
            		</s:else>
				</s:iterator>
            </ul>
            <div class="Clear"></div>
        </div>
    </div>
    
    <div class="SProductForm2Cols">
    	<div class="BtnCenterSection">
        	<a href="javascript:void(0);" class="Sprite1 Item1" onclick="PromotionSupport.selectAttributes()"><span class="HideText">Left</span></a>
            <a href="javascript:void(0);" class="Sprite1 Item2" onclick="PromotionSupport.removeAttributes()"><span class="HideText">Right</span></a>
            <div class="Clear"></div>
        </div>
    </div>
    
    <div class="SProductForm3Cols">
    	<h2 class="Title2Style">Thuộc tính áp dụng</h2>
        <div class="PopupContentMid" id="right" style="height: 239px;overflow-y: scroll;position: relative;">
            
        </div>
    	<div class="Clear"></div>
    </div>
    <div class="Clear"></div>
    
    <div class="SearchInSection SProduct1Form">
        <div class="BtnCenterSection" id="customerAttributeMsg">
             <button id="updateCustomerAttribute" class="BtnGeneralStyle cmsiscontrol" onclick="PromotionSupport.saveCustomerAttributes()">Cập nhật</button>
             <p id="errMsgSave" class="ErrorMsgStyle" style="display: none;">Bạn chưa chọn thuộc tính khách hàng nào!</p>
             <p id="successMsg" class="SuccessMsgStyle" style="display: none;"></p>
        </div>
        <div class="Clear"></div>
    </div>
</div>


<script type="text/javascript">
$(document).ready(function(){
	if(PromotionSupport._hasSaleLevel == true){
		PromotionSupport._hasSaleLevel = false;
		var htmlAttributeSaleLevel = '<li> <input name="Mức doanh số" objectType="3" objectId="0" type="checkbox"  class="InputCbxStyle"/><label style="width:150px" class="LabelStyle Label3Style">Mức doanh số</label></li>';
		$('#left').prepend(htmlAttributeSaleLevel);
	}
	if(PromotionSupport._hasCustomerType == true){
		PromotionSupport._hasCustomerType = false;
		var htmlAttrbuteCustomerType = '<li> <input name="Loại khách hàng" objectType="2" objectId="0" type="checkbox"  class="InputCbxStyle"/><label style="width:150px" class="LabelStyle Label3Style">Loại khách hàng</label></li>';
		$('#left').prepend(htmlAttrbuteCustomerType);
	}
	if(PromotionSupport._hasCustomerCardType == true){
		PromotionSupport._hasCustomerCardType = false;
		var htmlAttrbuteCustomerCardType = '<li> <input name="Loại thẻ thành viên" objectType="4" objectId="0" type="checkbox"  class="InputCbxStyle"/><label style="width:150px" class="LabelStyle Label3Style">Loại thẻ thành viên</label></li>';
		$('#left').prepend(htmlAttrbuteCustomerCardType);
	}
	PromotionSupport._flagAjaxTabAttribute = true;
	PromotionSupport.loadAppliedAttributes($('#masterDataArea #id').val().trim());
});
</script>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>

<div class="BreadcrumbSection">
	<ul class="ResetList FixFloat BreadcrumbList">
        <li class="Sprite1"><a href="#">Chương trình</a></li>
        <li><span>Chương trình khuyến mãi thủ công</span></li>
    </ul>
</div>

<div class="CtnOneColSection">
   <div class="ContentSection">
   		<div class="ToolBarSection">
           <div class="SearchSection GeneralSSection">
	           	<h2 class="Title2Style">Thông tin tìm kiếm</h2>
	           	<div class="SearchInSection SProduct1Form" id="searchForm">
           			<label class="LabelStyle Label1Style" id="labelPromotionCode">Mã CTKM</label>
	              	<input id="promotionCode" type="text" class="InputTextStyle InputText1Style" maxlength="50"/>
	              	<label class="LabelStyle Label1Style" id="labelPromotionName">Tên CTKM</label>
	              	<input id="promotionName" type="text" class="InputTextStyle InputText1Style" maxlength="100"/>
	              	<label class="LabelStyle Label1Style">Đơn vị</label>
	              	<div class="BoxSelect BoxSelect2">
<!-- 		              	<input type="text" id="shopTree" class="InputTextStyle InputText1Style" /> -->
		              	<input type="text" id="shopTree" style="width: 204px;" class="InputTextStyle" />
			 			<input type="hidden" id="shopCode"/>
		 			</div>
	              	<div class="Clear"></div>
	              	<label class="LabelStyle Label1Style" id="labelPromotionType">Loại CTKM</label>
	              	<div class="BoxSelect BoxSelect2">
		           		<select id="typeCode" name="LevelSchool" multiple="multiple"> 
		               		<option value="-1" selected="selected">---Tất cả---</option>
		               		<s:iterator value="lstTypeCode">
		               			<option value="<s:property value="id"/>"><s:property value="apParamCode"/>-<s:property value="value"/></option>
		               		</s:iterator>
		               </select>
	              	</div>
	              	<label class="LabelStyle Label1Style">Từ ngày</label>
	              	<input id="fromDate" type="text" class="InputTextStyle InputText6Style vinput-date"/>
	              	<label class="LabelStyle Label1Style">Đến ngày</label>
	              	<input id="toDate" type="text" class="InputTextStyle InputText6Style vinput-date"/>
	              	<div class="Clear"></div>
	               	<label class="LabelStyle Label1Style">Trạng thái</label>
					<div class="BoxSelect BoxSelect2">
<!-- 		                <div class="BoxSelect BoxSelect2 cmsiscontrol" id="permissionCreate_status"> -->
	                   	<div class="BoxSelect BoxSelect2" id="permissionCreate_status">
							<select id="status" class="MySelectBoxClass">
		                       <option value="-2">-- Tất cả --</option>
		                       <option value="0">Tạm ngưng</option>
		                       <option value="1" selected="selected">Hoạt động</option>
		                       <option value="2">Dự thảo</option>
		                   </select>
		                </div>		
	               	</div>
	               	<div class="Clear"></div>	               	
	              <div class="BtnCenterSection">
	              	<button id="btnSearch" class="BtnGeneralStyle Sprite2" onclick="PromotionSupport.searchProgram();"><span class="Sprite2">Tìm kiếm</span></button><%--$('#grid').datagrid('load', VTUtilJS.getFormData('searchForm'));--%>
	              </div>
	           	</div>
	           	<p id="errMsg" class="ErrorMsgStyle" style="display: none;"></p>
	           	<div class="SearchInSection SProduct1Form">
	           	<h2 class="Title2Style">Danh sách chương trình khuyến mãi thủ công</h2>
	           		<div class="GridSection" id="promotionGrid">
	                  	<div id="grid"></div>
	              	</div>
	           	</div>
           </div>
      	</div>
      	<div id="divDetailCommon" class="GeneralCntSection" style="display: none;">
        </div>
   </div>
</div>
<input type="hidden" id="isVNMAdmin" value="<s:property value='isVNMAdmin' />" /> 
<script type="text/javascript">
$(document).ready(function(){
	$('#downloadTemplate').attr('href', excel_template_path + 'catalog/Bieu_mau_danh_muc_import_CTKM.xlsx');
	$('#typeCode').dropdownchecklist({
		emptyText:'Tất cả',
		firstItemChecksAll: true,
		maxDropHeight: 250,
		onItemClick:function(checkbox, selector){
		},
		textFormatFunction: function(options) {
	        var selectedOptions = options.filter(":selected");
		    var text = '';
	        if(selectedOptions.size() > 0 && parseInt(selectedOptions[0].value) == -1){
	        	text = 'Tất cả';
	        }else{
		        for(var i = 0;i<selectedOptions.size();i++){
			        if(text.length > 0){
			        	text += ',';
			        }
			        text += selectedOptions[i].text.split('-')[0];
		        }
	        }
	        return text;
		}
	});
	
	$("#grid").datagrid({
		url: "/promotion-support/search",
		queryParams: {status:1, shopCode : $('#shopCode').val()},
		width: $("#promotionGrid").width()-10,
		height: 'auto',
		singleSelect: true,
		rownumbers: true,
		pagination: true,
		fitColumns: true,
		scrollbarSize: 0,
		columns: [[
			{field:'promotionProgramCode', title:"Mã CTKM", width:150,align:'left',sortable:false,resizable:false, formatter: function(value, row, index) {
				return Utils.XSSEncode(value);
			}},
			{field:'promotionProgramName', title:"Tên CTKM", width:270,align:'left',sortable:false,resizable:false, formatter: function(value, row, index) {
				return Utils.XSSEncode(value);
			}},
			{field:'type', title:"Loại CTKM", width:200,align:'left',sortable:false,resizable:false,formatter:function(value,row,index){
				if(row.type != null ||row.type != undefined ){
					return Utils.XSSEncode(row.type);
				} else {
					return '';
				}
			}},
			{field:'fromDate', title:"Từ ngày", width:80,align:'center',sortable:false,resizable:false,formatter:function(value,row,index){
				if(VTUtilJS.isNullOrEmpty(value)){
					return '';
				} else {
					return $.datepicker.formatDate('dd/mm/yy', new Date(value));
				}
			}},
			{field:'toDate', title:"Đến ngày", width:80,align:'center',sortable:false,resizable:false,formatter:function(value,row,index){
				if(VTUtilJS.isNullOrEmpty(value)){
					return '';
				} else {
					return $.datepicker.formatDate('dd/mm/yy', new Date(value));
				}
			}},
			{field:'status', title:"Trạng thái", width:100,align:'center',sortable:false,resizable:false,formatter:function(value,row,index){
				if(!VTUtilJS.isNullOrEmpty(row.status)){
					if(row.status == 'STOPPED'){
						return 'Tạm ngưng';
					}else if(row.status == 'RUNNING'){
						return 'Hoạt động';
					}else{
						return 'Dự thảo';
					}
				}else{
					return '';
				}
			}},
			{field:'view', title:'<a class="cmsiscontrol" id="permissionCreate_btnAddPromotion" href="/promotion-support/detail"><span style="cursor:pointer"><img title="Thêm mới" src="/resources/images/icon_add.png"/></span></a>',
				width:35, fixed:true, align:'center',sortable:false,resizable:false,formatter:PromotionSupport.gridRowIconFormatter}
		]],
		onLoadSuccess: function(data) {
			$('.datagrid-header-rownumber').html('STT');
			GridUtil.updateDefaultRowNumWidth("#promotionGrid", "grid");
			Utils.functionAccessFillControl();
		}
	});
	
// 	TreeUtils.loadComboTreeShopHasTitle('shopTree', 'shopCode',function(data) {
// 	}, true);
	//load combobox don vi
	$('#shopTree').combotree({
		url: '/commons/search-unit-tree',
		lines: true,
		formatter: function(node) {
			return Utils.XSSEncode(node.text);
		},
		onSelect: function(data){
        	if (data != null) {
        		$('#shopCode').val(data.attributes.shop.shopCode);
        	}
	    },
	    onLoadSuccess: function(node, data) {
			$('#shopTree').combotree('setValue', data[0].id);
		}
	});
	
	$("#promotionCode").parent().unbind("keyup");
	$("#promotionCode").parent().bind("keyup", function(event) {
		if (event.keyCode == keyCodes.ENTER) {
			$("#btnSearch").click();
		}
	});
});
</script>
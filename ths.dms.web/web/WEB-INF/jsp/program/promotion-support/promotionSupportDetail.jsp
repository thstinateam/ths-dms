<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<input type="hidden" id="function_code" value="<s:property value="systimestamp"/>" />
<s:set id="downloadToken" value="%{generateReportToken()}"></s:set>
<input type="hidden" id="report_token" value="<s:property value="#downloadToken"/>" />
<s:set id="downloadToken" value="%{putSessionValue(systimestamp, #downloadToken)}"></s:set>
<div class="BreadcrumbSection">
    <ul class="ResetList FixFloat BreadcrumbList">
	    <li class="Sprite1"><a href="/promotion-support/info">Chương trình khuyến mãi thủ công</a></li><li><span id="tabTitle">Thông tin chương trình khuyến mãi thủ công</span></li>	
    </ul>
</div>
<div class="CtnOneColSection">
   <div class="ContentSection">
   		<div class="ToolBarSection">
           <div class="SearchSection GeneralSSection">
	           	<h2 class="Title2Style">Thông tin chương trình khuyến mãi thủ công</h2>
	           	<div class="SearchInSection SProduct1Form" id="update-form">
	           		<label class="LabelStyle Label1Style" id="labelPromotionCode">Mã CTKM<span class="ReqiureStyle">(*)</span></label>
	           		<s:if test="promotionId != null && promotionId > 0">
	           			<input style="width: 160px;" id="promotionCode" disabled="disabled" type="text" class="InputTextStyle InputText1Style" call-back="VTValidateUtils.getMessageOfRequireCheck('promotionCode', 'Mã CTKM', false, true);VTValidateUtils.getMessageOfSpecialCharactersValidate('promotionCode', 'Mã CTKM', Utils._CODE)" value="<s:property value="promotionProgram.promotionProgramCode"/>" maxlength="50"/>
	           		</s:if>
	           		<s:else>
						<input style="width: 160px;" id="promotionCode" type="text" class="InputTextStyle InputText1Style" call-back="VTValidateUtils.getMessageOfRequireCheck('promotionCode', 'Mã CTKM', false, true);VTValidateUtils.getMessageOfSpecialCharactersValidate('promotionCode', 'Mã CTKM', Utils._CODE)" value="<s:property value="promotionProgram.promotionProgramCode"/>" maxlength="50"/>	           		
	           		</s:else>
					<label class="LabelStyle Label1Style" style="width: 117px;" id="labelPromotionName">Tên CTKM<span class="ReqiureStyle">(*)</span></label>
					<s:if test="promotionProgram.status.getValue() == 1 || promotionProgram.status.getValue() == 0">
						<input id="promotionName" disabled="disabled" style="width: 342px;" type="text" class="InputTextStyle InputText7Style" call-back="VTValidateUtils.getMessageOfRequireCheck('promotionName', 'Tên CTKM', false, true);VTValidateUtils.getMessageOfSpecialCharactersValidate('promotionName', 'Tên CTKM', Utils._NAME)" value="<s:property value="promotionProgram.promotionProgramName"/>" maxlength="100"/>
					</s:if>
					<s:else>
						<input id="promotionName" style="width: 342px;" type="text" class="InputTextStyle InputText7Style" call-back="VTValidateUtils.getMessageOfRequireCheck('promotionName', 'Tên CTKM', false, true);VTValidateUtils.getMessageOfSpecialCharactersValidate('promotionCode', 'Tên CTKM', Utils._NAME)" value="<s:property value="promotionProgram.promotionProgramName"/>" maxlength="100"/>					
					</s:else>
					<div class="Clear"></div>
					<label class="LabelStyle Label1Style">Từ ngày<span class="ReqiureStyle">(*)</span></label>
					<div id="startDatePickerDiv" style="float: left;">
						<s:if test="promotionProgram.status.getValue() == 1 || promotionProgram.status.getValue() == 0">
							<input id="startDate" disabled="disabled" type="text" class="InputTextStyle InputText6Style vinput-date" call-back="VTValidateUtils.getMessageOfEmptyDate('startDate', 'Từ ngày');VTValidateUtils.getMessageOfInvalidFormatDate('startDate', 'Từ ngày')" style="width:138px;" value="<s:date name="promotionProgram.fromDate" format="dd/MM/yyyy"/>"/>
						</s:if>
						<s:else>
							<input id="startDate" type="text" class="InputTextStyle InputText6Style vinput-date" call-back="VTValidateUtils.getMessageOfEmptyDate('startDate', 'Từ ngày');VTValidateUtils.getMessageOfInvalidFormatDate('startDate', 'Từ ngày')" style="width:138px;" value="<s:date name="promotionProgram.fromDate" format="dd/MM/yyyy"/>"/>
						</s:else>
					</div>
					<div id="endDatePickerDiv">
						<label style="width: 117px;" class="LabelStyle Label6Style" id="endDateClass">Đến ngày</label>
						<input id="permissionEditInfo_endDate" style="width:115px;" type="text" class="InputTextStyle InputText6Style vinput-date" call-back="VTValidateUtils.getMessageOfInvalidFormatDate('permissionEditInfo_endDate', 'Đến ngày');VTValidateUtils.getMessageCheckToDate('startDate', 'Từ ngày', 'permissionEditInfo_endDate', 'Đến ngày');VTValidateUtils.getMessageCheckCurrentDate('permissionEditInfo_endDate', 'Đến ngày')" value="<s:date name="promotionProgram.toDate" format="dd/MM/yyyy"/>"/>
					</div>
					<%-- <label class="LabelStyle Label6Style">Trạng thái<span class="ReqiureStyle">(*)</span></label> --%>
					<s:if test="promotionProgram.status.getValue() == 1 || promotionProgram.status.getValue() == 0 || promotionProgram.status.getValue() == 2">
							<label class="LabelStyle Label6Style" style="width: 87px;">Trạng thái<span class="ReqiureStyle">(*)</span></label>
					</s:if>
					<s:else>
							<label class="LabelStyle Label6Style" style="width: 80px;">Trạng thái<span class="ReqiureStyle">(*)</span></label>
					</s:else>
					<div class="BoxSelect BoxSelect11" id="comboStatus">
						<s:if test="promotionProgram.status.getValue() == 2">
							<select id="permissionEditInfo_status" class="MySelectBoxClass cmsiscontrol">
								<option value="1">Hoạt động</option>
								<option value="2" selected="selected">Dự thảo</option>
								<option value="0">Tạm ngưng</option>
							</select>						
						</s:if>
						<s:elseif test="promotionProgram.status.getValue() == 1">
							<select id="permissionEditInfo_status" class="MySelectBoxClass cmsiscontrol">
								<option value="1" selected="selected">Hoạt động</option>
								<option value="0">Tạm ngưng</option>
							</select>
						</s:elseif>
						<s:elseif test="promotionProgram.status.getValue() == 0">
							<select id="permissionEditInfo_status" class="MySelectBoxClass cmsiscontrol">
								<option value="0" selected="selected">Tạm ngưng</option>
							</select>
						</s:elseif>
						<s:else>
							<select id="permissionEditInfo_status" class="MySelectBoxClass cmsiscontrol">
								<option value="2" selected="selected">Dự thảo</option>
							</select>
						</s:else>
					</div>
					<div class="Clear"></div>
					<label class="LabelStyle Label1Style">Loại CTKM<span class="ReqiureStyle">(*)</span></label>
					<s:if test="promotionId != null && promotionId > 0">
						<input id="typeCodeTmp" type="text" class="InputTextStyle InputText1Style" value="<s:property value="promotionProgram.type"/>" maxlength="100" disabled="disabled"/>
						<input id="typeCode" type="hidden" value="<s:property value="promotionProgram.type"/>"/>
					</s:if>
					<s:else>
						<div class="BoxSelect BoxSelect2">
						   	<select id="typeCode">
								<option value="-1" selected="selected">---Chọn Loại CTKM---</option>
					       		<s:iterator value="lstTypeCode">
					       			<option value="<s:property value="apParamCode"/>"><s:property value="apParamCode"/>-<s:property value="value"/></option>
					       		</s:iterator>
					       	</select>
						</div>
					</s:else>
					<div class="Clear"></div>
					<label class="LabelStyle Label1Style">Mô tả</label>
					<s:if test="promotionProgram.status.getValue() == 0">
						<input disabled="disabled" id="description" style="width: 640px;" type="text" class="InputTextStyle InputText1Style" value="<s:property value="promotionProgram.description"/>" maxlength="1024"/>
					</s:if>
					<s:else>
						<input id="description" style="width: 640px;" type="text" class="InputTextStyle InputText1Style" value="<s:property value="promotionProgram.description"/>" maxlength="1024"/>
					</s:else>
					<div class="Clear"></div>
					<div class="BtnCenterSection" id="btnEdit_btnUpdate">
<%-- 						<s:if test="isVNMAdmin"> --%>
							<s:if test="promotionProgram.status.getValue() == 0">
							</s:if>
							<s:else>
								<button id="btnSave" class="BtnGeneralStyle Sprite2 cmsiscontrol" onclick="return PromotionSupport.update();"><span class="Sprite2">Cập nhật</span></button>
					<%-- 			<button id="btnCopy" class="BtnGeneralStyle Sprite2" onclick="return PromotionSupport.openCopyPromotionProgram();"><span class="Sprite2">Sao chép</span></button> --%>
							</s:else>									
<%-- 						</s:if> --%>
					</div>
					<div class="Clear"></div>
					<p class="ErrorMsgStyle SpriteErr" id="errorMsg" style="display: none;"></p>
					<p id="successMsg" class="SuccessMsgStyle" style="display: none"></p>
	           	</div>
	           	<s:if test="promotionProgram != null && promotionProgram.id != null && promotionProgram.id > 0">
					<div id="tabSectionDiv" class="TabSection">
						<ul class="ResetList TabSectionList">
					        <li><a id="shopTab" href="javascript:void(0);" onclick="PromotionSupport.showTab('shopTab', '/promotion-support/detail-shop');" class="Sprite1 Active"><span class="Sprite1 Active">Đơn vị tham gia</span></a></li>
					        <li><a id="attributeTab" href="javascript:void(0);" onclick="PromotionSupport.showTab('attributeTab', '/promotion-support/customer-attribute');" class="Sprite1"><span class="Sprite1">Thuộc tính khách hàng</span></a></li>
					    </ul>
					    <div class="Clear"></div>
					</div>	           	
	           	</s:if>
           </div>
      	</div>
      	<div id="divTab" class="SearchSection GeneralSSection">
        </div>	
   </div>
</div>
<div id="masterDataArea">
	<input type="hidden" id="id" value='<s:property value="promotionProgram.id"/>'>
	<input type="hidden" id="promotionStatus" value="<s:property value='promotionProgram.status.value'/>" />
	<input type="hidden" id="promotionType" value="<s:property value="promotionProgram.type"/>">
	<input type="hidden" id="statusHidden" value="<s:property value="promotionProgram.status.getValue()"/>">
	<input type="hidden" id="isVNMAdmin" value="<s:property value='isVNMAdmin' />" />
</div>
<script type="text/javascript">
$(document).ready(function(){
	var idt = '<s:property value="promotionProgram.id"/>';
	if (idt.length == 0 || Number(idt) <= 0) {
		$('#typeCode').dropdownchecklist({ 
			emptyText:'Tất cả',
			//firstItemChecksAll: true,
			maxDropHeight: 250,
			onItemClick:function(checkbox, selector){
			},
			textFormatFunction: function(options) {
		        var selectedOptions = options.filter(":selected");
			    var text = '';
		        if(selectedOptions.size() > 0 && parseInt(selectedOptions[0].value) == -1){
		        	text = 'Tất cả';
		        }else{
			        for(var i = 0;i<selectedOptions.size();i++){
				        if(text.length > 0){
				        	text += ',';
				        }
				        text += selectedOptions[i].text.split('-')[0];
			        }
		        }
		        return text;
			}
		});
	} else {
		VTUtilJS.getFormHtml('masterDataArea', '/promotion-support/detail-shop', function(html) {
			$('#divTab').html(html);
			Utils.functionAccessFillControl();
		});
	}
});
</script>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>

<input type="hidden" id="function_code" value="<s:property value="systimestamp"/>" />
<s:set id="downloadToken" value="%{generateReportToken()}"></s:set>
<input type="hidden" id="report_token" value="<s:property value="#downloadToken"/>" />
<s:set id="downloadToken" value="%{putSessionValue(systimestamp, #downloadToken)}"></s:set>

<style type="text/css">
.vinput-money {	text-align:right;
	border: 1px solid #c4c4c4;
	height:20px;
	line-height:20px;
	padding:0 4px;
	width: 110px;
}
</style>

<div id="boxSearch2">
	<h2 class="Title2Style">Thông tin tìm kiếm đơn vị</h2>

	<div class="SearchInSection SProduct1Form">
		<label class="LabelStyle Label1Style" id="shopCodeLabel">Mã đơn vị</label>
		<input id="shopCode" type="text" class="InputTextStyle InputText1Style" value="<s:property value="psMap.shop.shopCode"/>" maxlength="50"/>
		<label class="LabelStyle Label2Style">Tên đơn vị</label>
		<input id="shopName" type="text" class="InputTextStyle InputText1Style" value="<s:property value="psMap.shop.shopName"/>"/>
		<label class="LabelStyle Label1Style">Số suất KM</label>
		<input id="quantityMax" type="text" class="InputTextStyle InputText1Style" value="<s:property value="psMap.quantityMax"/>" maxlength="9"/>
		<div class="Clear"></div>
		
		<div class="BtnCenterSection">
			<button id="btnSearch" class="BtnGeneralStyle Sprite2" onclick="PromotionSupport.searchShop();"><span class="Sprite2">Tìm kiếm</span></button>
		 	<img id="loading" class="LoadingStyle" style="visibility: hidden;" src="/resources/images/loading.gif">
		</div>
		<p id="errMsgSearchShop" style="display: none;" class="ErrorMsgStyle SpriteErr"></p>
	</div>
</div>
	<h2 class="Title2Style">Danh sách đơn vị</h2>
	<div class="SearchInSection SProduct1Form">
		<div class="GridSection" id="promotionShopGrid">
			<table id="exGrid"></table> 
		</div>
		<div class="Clear"></div>
		<p id="errMsgShop" class="ErrorMsgStyle" style="display: none"></p>
		<p id="successMsgShop" class="SuccessMsgStyle" style="display: none;"></p>
	</div>

            
<div id="boxSearch1" style="display:none;">
	<h2 class="Title2Style">Thông tin tìm kiếm khách hàng</h2>
	<div class="SearchInSection SProduct1Form">
		<label class="LabelStyle Label1Style">Mã KH</label>
		<input type="text" class="InputTextStyle InputText1Style" id="customerCodeSearch" />
		<label class="LabelStyle Label2Style">Tên KH</label>
		<input type="text" class="InputTextStyle InputText1Style" id="customerNameSearch" />
		<label class="LabelStyle Label1Style">Địa chỉ</label>
		<input type="text" class="InputTextStyle InputText1Style" id="customerAddressSearch"/>
		<div class="Clear"></div>
		<div class="BtnCenterSection">
		    <button class="BtnGeneralStyle" id="btnSearchCustomer">Tìm kiếm</button>
		</div>
		<div class="Clear"></div>
	</div>
</div>

<h2 class="Title2Style" id="labelListCustomer" style="display:none;">Danh sách khách hàng <label id="labelShop" style="color:#3a1;"></label> <a href="javascript:void(0);" onclick="PromotionSupport.toggleCustomerSearch();" class="LinkSearch" id="searchCustomerDiv">Tìm kiếm &gt;&gt;</a></h2>
<div class="SearchInSection SProduct1Form" id="lstCustomer">
	<div class="GridSection" id="promotionCustomerGrid" style="display:none;">
		<table id="promotionCustomerExGrid"></table> 
	</div>
	<div class="Clear"></div>
	<p id="errMsgCustomer" class="ErrorMsgStyle" style="display: none"></p>
	<p id="successMsgCustomer" class="SuccessMsgStyle" style="display: none;"></p>
</div>

<div class="SearchInSection SProduct1Form">
	<div class="GeneralForm GeneralNoTP1Form">
		<div class="Func1Section">
			<div id="importGroup" class="cmsiscontrol" style="float: left;">
				<p class="DownloadSFileStyle DownloadSFile2Style"><a id="downloadTemplateLevel" href="/resources/templates/catalog/Bieu_mau_chuong_trinh_khuyen_mai_thu_cong_tab_don_vi.xls" class="Sprite1">Tải file excel mẫu</a></p>
				<div class="DivInputFile" id="importDiv">
					<form action="/promotion-support/import-promotion-shop" name="importFrm" id="importFrm"  method="post" enctype="multipart/form-data">
						<input type="hidden" name="token" id="tokenImport" value='<s:property value="token"/>'>
						<input type="hidden" name="id" value='<s:property value="id"/>'>									
				    	<input type="file" class="InputFileStyle" size="10" style="width: 170px;" readonly="readonly" name="excelFile" id="excelFile" onchange="previewImportExcelFile(this,'importFrm');">
				    	<!-- <input type="hidden" id="isView" name="isView"> -->
				    	<!-- <input type="hidden" value="2" id="importExcelType" name="importExcelType"/> -->
				    	<input type="hidden" id="promotionIdImport" name="promotionId" value="<s:property value="promotionId" />" />
						<div class="FakeInputFile">
							<input id="fakefilepc" type="text" class="InputTextStyle">
						</div>
					</form>
				</div>
				<button id="btnImport" class="BtnGeneralStyle" onclick="PromotionSupport.importPromotionShop();">Nhập từ Excel</button>
			</div>
			<button id="btnExport" class="BtnGeneralStyle" onclick="PromotionSupport.exportPromotionShop();">Xuất Excel</button>
		</div>
		<div class="Clear"></div>
	</div>
	<div class="Clear"></div>
	<p id="errExcelMsgShop" class="ErrorMsgStyle" style="display: none;margin-top: 10px;margin-left: 10px;padding: 2px 0 5px 9px;"></p>
	<p id="successExcelMsgShop" class="SuccessMsgStyle" style="display: none;margin-top: 10px;margin-left: 10px;padding: 2px 0 5px 9px;"></p>
</div>


<%-- script --%>
<script type="text/javascript">
$(document).ready(function(){
	var status = $("#promotionStatus").val().trim();
	if (status != 2) {
		$("#downloadTemplateLevel").remove();
		$("#importDiv").remove();
		$("#btnImport").remove();
	}
	var pms = true;
	
	var vtitle = "";
	if (status == 2) {
		vtitle = "<a id='tab_shop_grid_row_new_title' class='cmsiscontrol' href='javascript:void(0);' onclick='PromotionSupport.showShopDlg();'><img title='Thêm mới' width='17' height='17' src='/resources/images/icon-add.png'></a>";
	}
	
	$("#exGrid").treegrid({
		url: "/promotion-support/search-shop-of-promotion",
		queryParams: {promotionId:$("#masterDataArea #id").val().trim()},
		rownumbers: false,
		width: $("#promotionShopGrid").width() - 10,
		height: "auto",
		fitColumns: true,
		idField: "nodeId",
		treeField: "text",
		scrollbarSize:0,
		columns: [[
			{field: "no", title: "STT", sortable: false, resizable: false, width: 30, fixed: true, align: "center", formatter:function(v, r) {
				return "";
			}},
			{field: "text", title: "Đơn vị", sortable: false, resizable: false, width: 200, align: "left", formatter:CommonFormatter.formatNormalCell},
			{field: "quantity", title: "Số suất KM", sortable: false, resizable: false, width: 80, fixed: true, align: "right", formatter: function(v, r) {
				var val = "";
				v = r.attr.quantity;
				if (v != undefined && v != null) {
					val = formatCurrency(v);
				}
				if (status == activeType.STOPPED) {
					return val;
				}
				return "<span class='sp_vinput-money' id='shop-quantity-sp-"+r.attr.id+"'>"+val+"</span>" + "<input type='text' id='shop-quantity-ip-"+r.attr.id+"' value='"+val+"' style='display:none;' class='vinput-money' maxlength='12' />";
			}},
			{field: "receivedQtt", title: "Số suất đã KM", sortable: false, resizable: false, width: 80, fixed: true, align: "right", formatter: function(v, r) {
				var val = "0";
				v = r.attr.receivedQtt;
				if (v != undefined && v != null) {
					val = formatCurrency(v);
				} else {
					val = 0;
				}
				return val;
			}},
			{field: "numMax", title: "Số lượng KM", sortable: false, resizable: false, width: 80, fixed: true, align: "right", formatter: function(v, r, i) {
				var val = "";
				v = r.attr.numMax;
				if (v != undefined && v != null) {
					val = formatCurrency(v);
				}
				if (status == activeType.STOPPED) {
					return val;
				}
				return "<span class='sp_vinput-money' id='shop-nummax-sp-"+ r.attr.id +"'>"+ val +"</span>" + "<input type='text' id='shop-nummax-ip-"+ r.attr.id +"' value='"+ val +"' style='display:none; text-align: right;' class='vinput-money' maxlength='12' />";
			}},
			{field: "receivedNum", title: "Số lượng đã KM", sortable: false, resizable: false, width: 80, fixed: true, align: "right", formatter: function(v, r, i) {
				var val = "";
				v = r.attr.receivedNum;
				if (v != undefined && v != null) {
					val = formatCurrency(v);
				} else {
					val = 0;
				}
				return val;
			}},
			{field: "amountMax", title: "Số tiền KM", sortable: false, resizable: false, width: 80, fixed: true, align: "right", formatter: function(v, r, i) {
				var val = "";
				v = r.attr.amountMax;
				if (v != undefined && v != null) {
					val = formatCurrency(v);
				}
				if (status == activeType.STOPPED) {
					return val;
				}
				return "<span class='sp_vinput-money' id='shop-amountmax-sp-"+ r.attr.id +"'>"+ val +"</span>" + "<input type='text' id='shop-amountmax-ip-"+ r.attr.id +"' value='"+ val +"' style='display:none; text-align: right;' class='vinput-money' maxlength='12' />";
			}},
			{field: "receivedAmt", title: "Số tiền đã KM", sortable: false, resizable: false, width: 80, fixed: true, align: "right", formatter: function(v, r, i) {
				var val = "";
				v = r.attr.receivedAmt;
				if (v != undefined && v != null) {
					val = formatCurrency(v);
				} else {
					val = 0;
				}
				return val;
			}},
			{field: "edit", title: "", sortable: false, resizable: false, width: 30, fixed: true, align: "center", formatter: function(v, r) {
				if (pms) {
		        	if (r.attr.isNPP == 1) {
		        		if (status == activeType.RUNNING || status == activeType.WAITING) {
		        			return '<a href="javascript:void(0);" id="link'+r.attr.id+'" onclick="edit('+r.attr.id+','+r.attr.quantity+');"><img title="Sửa" width="17" height="17" src="/resources/images/icon-edit.png"></a>';
		        		}
		        	}
	        	}
			}},
			{field: "view", title: vtitle, sortable: false, resizable: false, width: 30, fixed: true, align: "center", formatter: function(v, r) {
				if (pms) {
		        	if (r.attr.isNPP != 1) {
		        		if (status == activeType.WAITING) {
		        			return '<a href="javascript:void(0);" id="tab_shop_grid_row_new_' + r.attr.id + '" class="cmsiscontrol" onclick="PromotionSupport.showShopDlg('+r.attr.id+');"><img title="Thêm mới" width="17" height="17" src="/resources/images/icon-add.png"></a>';
		        		}
		        	} else {
		        		return '<a href="javascript:void(0);" id="linkesc'+r.attr.id+'" onclick="PromotionSupport.searchCustomerShopMap('+$("#masterDataArea #id").val().trim()+','+r.attr.id+','+status+');"><img title="Xem chi tiết" width="17" height="17" src="/resources/images/icon-view.png"></a>';
		        	}
	        	} else {
	        		if (r.attr.isNPP == 1) {
		        		return '<a href="javascript:void(0);" id="linkesc'+r.attr.id+'" onclick="PromotionSupport.searchCustomerShopMap('+$("#masterDataArea #id").val().trim()+','+r.attr.id+','+status+');"><img title="Xem chi tiết" width="17" height="17" src="/resources/images/icon-view.png"></a>';
		        	}
	        		return '';
	        	}
			}},
			{field: "delete", title: "", sortable: false, resizable: false, width:30, fixed: true, align: "center", formatter: function(v, r) {
				if (pms) {
	        		if (status == activeType.WAITING) {
	        			return '<a href="javascript:void(0);" onclick="PromotionSupport.deleteShopMap('+r.attr.id+');"><img title="Xóa" width="17" height="17" src="/resources/images/icon-delete.png"></a>';
	        		}
	        	}
				return '';
			}}
		]],
		onLoadSuccess: function(row, data) {
			var i = 1;
			$("#promotionShopGrid .datagrid-btable td[field=no] div").each(function() {
				$(this).html(i); // danh STT
				i++;
			});
			$('#promotionShopGrid .vinput-money').each(function() {
				$(this).width($(this).parent().width())
				VTUtilJS.bindFormatOnTextfield($(this), VTUtilJS._TF_NUMBER_COMMA);
				VTUtilJS.formatCurrencyFor($(this).prop('id'));
			});
			var arrEdit =  $('#promotionShopGrid td[field="edit"]');
			if (arrEdit != undefined && arrEdit != null && arrEdit.length > 0) {
			  for (var i = 0, size = arrEdit.length; i < size; i++) {
			    $(arrEdit[i]).prop("id", "tab_shop_grid_row_edit_" + i);//Khai bao id danh cho phan quyen
				$(arrEdit[i]).addClass("cmsiscontrol");
			  }
			}
			var arrDelete =  $('#promotionShopGrid td[field="delete"]');
			if (arrDelete != undefined && arrDelete != null && arrDelete.length > 0) {
			  for (var i = 0, size = arrDelete.length; i < size; i++) {
			    $(arrDelete[i]).prop("id", "tab_shop_grid_row_delete_" + i);//Khai bao id danh cho phan quyen
				$(arrDelete[i]).addClass("cmsiscontrol");
			  }
			}
			Utils.functionAccessFillControl('promotionShopGrid', function(data) {
				//Xu ly cac su kien lien quan den ctl phan quyen
			});
			$("#exGrid").datagrid("resize");
		}
	});
	
	// ds KH, move sang ham khac
	var ctitle = "";
	if (status == 1 || status == 2) {
		ctitle = "<a id='tab_shop_grid_customer_row_new_titleC' href='javascript:void(0);' onclick='PromotionSupport.showCustomerDlg();'><img title='Thêm mới' width='17' height='17' src='/resources/images/icon-add.png'></a>";
	}
	
	$("#promotionCustomerExGrid").datagrid({
		url: "/promotion-support/search-customer-of-promotion",
		rownumbers: false,
		width: $("#promotionShopGrid").width() - 10,
		height: "auto",
		pagination: true,
		pageSize: 10,
		fitColumns: true,
		singleSelect: true,
		idField: "mapId",
		scrollbarSize: 0,
		columns: [[
			{field: "no", title: "STT", sortable: false, resizable: false, width:45, fixed: true, align: "center", formatter:function(v, r, i) {
				var p = $('#promotionCustomerExGrid').datagrid("options").pageNumber;
				var n = $('#promotionCustomerExGrid').datagrid("options").pageSize;
				return (Number(p) - 1) * Number(n) + i + 1;
			}},
			{field: "customerCode", title: "Mã KH", sortable: false, resizable: false, width: 110, align: "left", formatter: function(v, r, i) {
				return VTUtilJS.XSSEncode(v);
			}},
			{field: "customerName", title: "Tên KH", sortable: false, resizable: false, width: 120, align: "left", formatter: function(v, r, i) {
				return VTUtilJS.XSSEncode(v);
			}},
			{field: "address", title: "Địa chỉ", sortable: false, resizable: false, width:180, align: "left", formatter:CommonFormatter.formatNormalCell},
			{field: "quantity", title: "Số suất KM", sortable: false, resizable: false, width: 80, align: "right", formatter: function(v, r, i) {
				var val = "";
				if (v != undefined && v != null) {
					val = formatCurrency(v);
				}
				if (status == activeType.STOPPED) {
					return val;
				}
				return "<span class='sp_vinput-money' id='txtQuantity-sp-"+r.mapId+"'>"+val+"</span>" + "<input type='text' id='txtQuantity-ip-"+r.mapId+"' value='"+val+"' style='display:none; text-align: right;' class='vinput-money' maxlength='12' />";
			}},
			{field: "receivedQtt", title: "Số suất đã KM", sortable: false, resizable: false, width: 80, align: "right", formatter: function(v, r, i) {
				var val = "";
				if (v != undefined && v != null) {
					val = formatCurrency(v);
				} else {
					val = 0;
				}
				return val;
			}},
			{field: "numMax", title: "Số lượng KM", sortable: false, resizable: false, width: 80, align: "right", formatter: function(v, r, i) {
				var val = "";
				if (v != undefined && v != null) {
					val = formatCurrency(v);
				}
				if (status == activeType.STOPPED) {
					return val;
				}
				return "<span class='sp_vinput-money' id='txtQuantityMax-sp-"+r.mapId+"'>"+val+"</span>" + "<input type='text' id='txtQuantityMax-ip-"+r.mapId+"' value='"+val+"' style='display:none; text-align: right;' class='vinput-money' maxlength='12' />";
			}},
			{field: "receivedNum", title: "Số lượng đã KM", sortable: false, resizable: false, width: 80, align: "right", formatter: function(v, r, i) {
				var val = "";
				if (v != undefined && v != null) {
					val = formatCurrency(v);
				} else {
					val = 0;
				}
				return val;
			}},
			{field: "amountMax", title: "Số tiền KM", sortable: false, resizable: false, width: 80, align: "right", formatter: function(v, r, i) {
				var val = "";
				if (v != undefined && v != null) {
					val = formatCurrency(v);
				}
				if (status == activeType.STOPPED) {
					return val;
				}
				return "<span class='sp_vinput-money' id='txtAmountMax-sp-"+r.mapId+"'>"+val+"</span>" + "<input type='text' id='txtAmountMax-ip-"+r.mapId+"' value='"+val+"' style='display:none; text-align: right;' class='vinput-money' maxlength='12' />";
			}},
			{field: "receivedAmt", title: "Số tiền đã KM", sortable: false, resizable: false, width: 80, align: "right", formatter: function(v, r, i) {
				var val = "";
				if (v != undefined && v != null) {
					val = formatCurrency(v);
				} else {
					val = 0;
				}
				return val;
			}},
			{field: "edit", title: "", sortable: false, resizable: false, width: 30, fixed: true, align: "center", formatter: function(v, r) {
				if (pms) {
	        		if (status == 1 || status == 2) {
	        			return '<a href="javascript:void(0);" id="tabEditShop_ed-'+r.mapId+'" onclick="editCust('+r.mapId+','+r.quantity+');"><img title="Sửa" width="17" height="17" src="/resources/images/icon-edit.png"></a>';
	        		}
	        	}					
			}},
			{field: "delete", title: ctitle, sortable: false, resizable: false, width:30, fixed: true, align: "center", formatter: function(v, r) {
				if(pms){
	        		if(status == 2){
	        			return '<a  href="javascript:void(0);" id="tabEditShop_rm-'+r.mapId+'" onclick="PromotionSupport.deleteCustomerMap('+r.mapId+');"><img title="Xóa" width="17" height="17" src="/resources/images/icon-delete.png"></a>';
	        		}
	        	}
				return '<a href="javascript:void(0);" id="tabEditShop_rm-'+r.mapId+'" onclick=""></a>';	
			}}
		]],
		onLoadSuccess: function(data) {
			$("#promotionCustomerExGrid").datagrid("resize");
			$('#lstCustomer .vinput-money').each(function() {
				$(this).width($(this).parent().width())
				VTUtilJS.bindFormatOnTextfield($(this), VTUtilJS._TF_NUMBER_COMMA);
				VTUtilJS.formatCurrencyFor($(this).prop('id'));
			});
			var arrEdit =  $('#promotionCustomerGrid td[field="edit"]');
			if (arrEdit != undefined && arrEdit != null && arrEdit.length > 0) {
			  for (var i = 0, size = arrEdit.length; i < size; i++) {
			    $(arrEdit[i]).prop("id", "tab_shop_grid_customer_row_edit_" + i);//Khai bao id danh cho phan quyen
				$(arrEdit[i]).addClass("cmsiscontrol");
			  }
			}
			var arrDelete =  $('#promotionCustomerGrid td[field="delete"]');
			if (arrDelete != undefined && arrDelete != null && arrDelete.length > 0) {
			  for (var i = 0, size = arrDelete.length; i < size; i++) {
			    $(arrDelete[i]).prop("id", "tab_shop_grid_customer_row_delete_" + i);//Khai bao id danh cho phan quyen
				$(arrDelete[i]).addClass("cmsiscontrol");
			  }
			}
			Utils.functionAccessFillControl('promotionCustomerGrid', function(data){
			});
			$("#promotionCustomerExGrid").datagrid("resize");
		}
	});
	$("#shopCode").parent().unbind("keyup");
	$("#shopCode").parent().bind("keyup", function(event) {
		if (event.keyCode == keyCodes.ENTER) {
			$("#btnSearch").click();
		}
	});
	
	$("#customerCodeSearch").parent().unbind("keyup");
	$("#customerCodeSearch").parent().bind("keyup", function(event) {
		if (event.keyCode == keyCodes.ENTER) {
			$("#btnSearchCustomer").click();
		}
	});
});

function edit(id, qtt) {
	$('.ErrorMsgStyle').hide();
	$('#promotionShopGrid .vinput-money[id*="ip-'+id+'"]').each(function() {
	    var spTxt = '#' + $(this).prop("id").replace(/-ip-/g, '-sp-');
	    $(spTxt).hide();
		$(this).width($(this).parent().width() - 7).val($(spTxt).text().trim()).show();
	});
	$('#link'+id).attr('onclick','PromotionSupport.updateShopQuantity('+id+');');
	$('#link'+id).html('<img width="17" height="17" src="/resources/images/icon-save.png" title="Lưu"/>');
	$('#linkesc'+id).attr('onclick','esc('+id+','+qtt+');');
	$('#linkesc'+id).html('<img width="17" height="17" title="Quay lại" src="/resources/images/icon_esc.png"/>');
}

function esc(id,qtt){
	$('.ErrorMsgStyle').hide();
	$('#promotionShopGrid .vinput-money[id*="ip-'+id+'"]').each(function() {
		var spTxt = '#' + $(this).prop("id").replace(/-ip-/g, '-sp-');
	    $(spTxt).show();
		$(this).val($(spTxt).text().trim()).hide();
	});
   $('#link'+id).attr('onclick','edit('+id+','+qtt+');');
   $('#link'+id).html('<img width="17" height="17" src="/resources/images/icon-edit.png" title="Sửa"/>');
   $('#linkesc'+id).attr('onclick','PromotionSupport.searchCustomerShopMap('+$("#masterDataArea #id").val().trim()+','+id+');');
   $('#linkesc'+id).html('<img width="17" height="17" title="Xem chi tiết" src="/resources/images/icon-view.png"/>');
}

function editCust(id, qtt) {
	$('.ErrorMsgStyle').hide();
	$('#lstCustomer .vinput-money[id*="ip-'+id+'"]').each(function() {
	    var spTxt = '#' + $(this).prop("id").replace(/-ip-/g, '-sp-');
	    $(spTxt).hide();
		$(this).width($(this).parent().width() - 7).val($(spTxt).text().trim()).show();
	});
	$('#tabEditShop_ed-'+id).attr('onclick','PromotionSupport.updateCustomerQuantity('+id+');');
	$('#tabEditShop_ed-'+id).html('<img width="17" height="17" src="/resources/images/icon-save.png" title="Lưu"/>');
	$('#tabEditShop_rm-'+id).attr('onclick','escCust('+id+','+qtt+');');
	$('#tabEditShop_rm-'+id).html('<img width="17" height="17" title="Quay lại" src="/resources/images/icon_esc.png"/>');
}

function escCust(id, qtt) {
	$('.ErrorMsgStyle').hide();
	$('#lstCustomer .vinput-money[id*="ip-'+id+'"]').each(function() {
		var spTxt = '#' + $(this).prop("id").replace(/-ip-/g, '-sp-');
	    $(spTxt).show();
		$(this).val($(spTxt).text().trim()).hide();
	});

   $('#tabEditShop_ed-'+id).attr('onclick','editCust('+id+','+qtt+');');
   $('#tabEditShop_ed-'+id).html('<img width="17" height="17" src="/resources/images/icon-edit.png" title="Sửa"/>');
   var status = $("#masterDataArea #promotionStatus").val().trim();
   if (status == 2) {
   		$('#tabEditShop_rm-'+id).attr('onclick','PromotionSupport.deleteCustomerMap('+id+');');
   		$('#tabEditShop_rm-'+id).html('<img width="17" height="17" title="Xem chi tiết" src="/resources/images/icon-delete.png"/>');
   } else {
	   $('#tabEditShop_rm-'+id).attr('onclick','');
  		$('#tabEditShop_rm-'+id).html('');
   }
}
</script>
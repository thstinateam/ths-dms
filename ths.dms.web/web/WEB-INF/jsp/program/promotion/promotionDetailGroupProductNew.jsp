<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<div id="groupProductTab">
    <div title="Khai báo nhóm">
    	<h2 class="Title2Style">Danh sách nhóm</h2>
    	<div class="GridSection" id="groupGridContainer">
			<div id="groupGrid"></div>
	    </div>
	    <p id="groupErrMsg" class="ErrorMsgStyle" style="display: none"></p>
    </div>
    <div id="kmLevel" style="display: none;">
    	<div class="GridSection">
	    	<label class="LabelStyle Label1Style" id="labelFromLevel">Từ mức</label>
	    	<input class="InputTextStyle InputText1Style" type="text" id="fromLevel">
	    	
	    	<label style="padding-left: 10px;" class="LabelStyle Label1Style" id="labelToLevel">Đến mức</label>
	    	<input class="InputTextStyle InputText1Style" type="text" id="toLevel" style="margin-right: 10px;">
	    	
	    	<button class="BtnGeneralStyle Sprite2" onclick="loadLevelDetail()"><span class="Sprite2">Xem Cơ cấu</span></button>
	    	<div class="Clear"></div>
	    	<p id="levelInfoErrMsg" class="ErrorMsgStyle" style="display: none"></p>
	    	<p id="levelInfoMessage" class="SuccessMsgStyle" style="display: none"></p>
    	</div>
    	<h2 class="Title2Style" id="cocauKM">Cơ cấu khuyến mại</h2>
		<table id="mappingGrid" style="width: 100%;">
			<tbody>
			</tbody>
		</table>
	    <div class="Clear"></div>
	    <p class="ErrorMsgStyle SpriteErr" style="display: none;" id="errorMsgDistribute" ></p>
    </div>
</div>
<script type="text/javascript">
loadLevelDetail = function() {
	$(".ErrorMsgStyle").hide();
	window.scrollTo(0, 0);
	if ($('#fromLevel').val().trim().length == 0 && $('#toLevel').val().trim().length == 0) {
		PromotionProgram.fromLevel = PromotionProgram._DEFAULT_FROM_LEVEL;
		PromotionProgram.toLevel = PromotionProgram._DEFAULT_TO_LEVEL;
		PromotionProgram.isButtonLoad = false;
	} else if ($('#fromLevel').val().trim().length > 0 && $('#toLevel').val().trim().length == 0) {
		if (isNaN($('#fromLevel').val().trim())) {
			$("#levelInfoErrMsg").html("Từ mức không hợp lệ").show();
			$("#fromLevel").focus();
			return;
		}
		PromotionProgram.isButtonLoad = true;
		PromotionProgram.fromLevel = Number($('#fromLevel').val().trim());
		PromotionProgram.toLevel = Number($('#fromLevel').val().trim()) + PromotionProgram._DEFAULT_TO_LEVEL - PromotionProgram._DEFAULT_FROM_LEVEL;
	} else if ($('#fromLevel').val().trim().length == 0 && $('#toLevel').val().trim().length > 0) {
		if (isNaN($('#toLevel').val().trim())) {
			$("#levelInfoErrMsg").html("Đến mức không hợp lệ").show();
			$("#toLevel").focus();
			return;
		}
		$("#levelInfoErrMsg").html("Bạn phải nhập Từ mức").show();
		$("#fromLevel").focus();
		return;
	} else {
		if (isNaN($('#fromLevel').val().trim())) {
			$("#levelInfoErrMsg").html("Từ mức không hợp lệ").show();
			$("#fromLevel").focus();
			return;
		}
		if (isNaN($('#toLevel').val().trim())) {
			$("#levelInfoErrMsg").html("Đến mức không hợp lệ").show();
			$("#toLevel").focus();
			return;
		}
		if (Number($('#fromLevel').val()) <= 0) { 
			$('#fromLevel').val(1);
		}
		if (Number($('#toLevel').val()) <= 0) { 
			$('#toLevel').val(1);
		}
		PromotionProgram.fromLevel = Number($('#fromLevel').val());
		PromotionProgram.toLevel = Number($('#toLevel').val());
		PromotionProgram.isButtonLoad = true;
	}
	var titleCocau = $('#cocauKM').html();
	PromotionProgram.isEndLoad = false;
	PromotionProgram.isFinishLoad = true;
	PromotionProgram.tabAddLevel(PromotionProgram.groupMuaId, PromotionProgram.groupKMId, '', '');
	$('#cocauKM').html(Utils.XSSEncode(titleCocau));
	var tm = setTimeout(function(){
		var arrPanel = $('.panel-tool');
		if (arrPanel != undefined && arrPanel != null && arrPanel.length > 0) {
			for (var i = 0, size = arrPanel.length; i < size; i++) {
				$(arrPanel[i]).prop("id", "tab_product_pn_change_hd_" + i);//Khai bao id danh cho phan quyen
				$(arrPanel[i]).addClass("cmsiscontrol");	
			}
		}
		Utils.functionAccessFillControl('divTab', function(data) {
			//Xu ly cac su kien lien quan den ctl phan quyen
		});
		clearTimeout(tm);
	}, 300);
};

$(document).ready(function(){
	Utils.bindFormatOnTextfield("fromLevel",Utils._TF_NUMBER, null, null);
	Utils.bindFormatOnTextfield("toLevel",Utils._TF_NUMBER, null, null);
	$('#groupGrid').datagrid({
    	url: "/promotion/detail-group-new",
		queryParams: {id:$('#id').val()},
		width: $("#groupGridContainer").width(),
		height: 'auto',
		singleSelect: true,
		fitColumns: true,
		scrollbarSize:0,
		columns: [[
			{field:'promotionId', title:"Nhóm", width:20,align:'center',sortable:false,resizable:false,formatter:function(value,row,index) {
				return index + 1;
			}},
			{field:'groupMuaCode', title:"Mã nhóm", width:100,align:'left',sortable:false,resizable:false, formatter: function(value, row, index) {
				return Utils.XSSEncode(value);
			}},
			{field:'groupMuaName', title:"Tên nhóm", width:210,align:'left',sortable:false,resizable:false, formatter: function(value, row, index) {
				return Utils.XSSEncode(value);
			}},
			{field:'level', title:"", width:30,align:'center',sortable:false,resizable:false,formatter:function(value,row,index){
				return '<a onclick="PromotionProgram.clickTabAddLevel('+row.groupMuaId+', '+row.groupKMId+', \''+Utils.XSSEncode(row.groupMuaCode)+'\', \''+Utils.XSSEncode(row.groupMuaName)+'\')"><span style="cursor:pointer"><img title="Chi tiết mức" src="/resources/images/signal.png"/></span></a>';
			}},
			{field:'edit', title:"", width:30,align:'center',sortable:false,resizable:false,formatter:function(value,row,index){
				return '<a onclick="PromotionProgram.popupEditGroupNew('+row.groupMuaId+','+row.groupKMId+', \''+Utils.XSSEncode(row.groupMuaCode)+'\''+', \''+Utils.XSSEncode(row.groupMuaName)+'\', '+(VTUtilJS.isNullOrEmpty(row.stt)?'\'\'':row.stt)+', '+(VTUtilJS.isNullOrEmpty(row.multiple)?'\'\'':row.multiple)+', '+(VTUtilJS.isNullOrEmpty(row.recursive)?'\'\'':row.recursive)+')"><span style="cursor:pointer"><img title="Sửa nhóm" src="/resources/images/icon_edit.png"/></span></a>';
			}},
			{field:'delete', title:'<a '+(($('#statusHidden').val() == 0 || $('#statusHidden').val() == 1) ? 'style="display:none;"' : '')+' onclick="PromotionProgram.popupAddGroupNew()"><span style="cursor:pointer"><img title="Thêm mới nhóm" src="/resources/images/icon_add.png"/></span></a>', width:30,align:'center',sortable:false,resizable:false,formatter:function(value,row,index){
				return '<a '+(($('#statusHidden').val() == 0 || $('#statusHidden').val() == 1) ? 'style="display:none;"' : '')+' onclick="PromotionProgram.deleteProductGroupNew(\'groupGrid\', '+row.groupMuaId+', '+row.groupKMId+')"><span style="cursor:pointer"><img title="Xóa nhóm" src="/resources/images/icon_delete.png"/></span></a>';
			}}
		]],
		onLoadSuccess: function(data) {
			if ($("#typeCode").length == 0) {
				return;
			}
			var typeCodeTmp = $("#typeCode").val().trim();
			var lstZVMultGroup = [ "ZV01", "ZV02", "ZV03", "ZV04", "ZV05", "ZV06", "ZV09"];
			if (lstZVMultGroup.indexOf(typeCodeTmp) < 0) {
				if ($("#groupGridContainer .datagrid-row td[field=delete] a").length > 0) {
					$("#groupGridContainer .datagrid-header-row td[field=delete] a").hide();
				} else {
					$("#groupGridContainer .datagrid-header-row td[field=delete] a").show();
				}
			}
			var arrEdit =  $('#groupGridContainer td[field="edit"]');
			if (arrEdit != undefined && arrEdit != null && arrEdit.length > 0) {
			  for (var i = 0, size = arrEdit.length; i < size; i++) {
			  	$(arrEdit[i]).prop("id", "tab_product_gr_change_gr_prd_td_edit_" + i);//Khai bao id danh cho phan quyen
				$(arrEdit[i]).addClass("cmsiscontrol");
			  }
			}
			var arrDelete =  $('#groupGridContainer td[field="delete"]');
			if (arrDelete != undefined && arrDelete != null && arrDelete.length > 0) {
			  for (var i = 0, size = arrDelete.length; i < size; i++) {
			    $(arrDelete[i]).prop("id", "tab_product_gr_change_gr_prd_td_del_" + i);//Khai bao id danh cho phan quyen
				$(arrDelete[i]).addClass("cmsiscontrol");
			  }
			}
			Utils.functionAccessFillControl('groupGridContainer', function(data){
				//Xu ly cac su kien lien quan den cac control phan quyen
				var arrTmpLength =  $('#groupGridContainer td[id^="tab_product_gr_change_gr_prd_td_del_"]').length;
				var invisibleLenght = $('.isCMSInvisible[id^="tab_product_gr_change_gr_prd_td_del_"]').length;
				if (arrTmpLength > 0 && arrTmpLength != invisibleLenght) {
					$('#groupGrid').datagrid("showColumn", "delete");
				} else {
					$('#groupGrid').datagrid("hideColumn", "delete");
				}
				arrTmpLength =  $('#groupGridContainer td[id^="tab_product_gr_change_gr_prd_td_edit_"]').length;
				invisibleLenght = $('.isCMSInvisible[id^="tab_product_gr_change_gr_prd_td_edit_"]').length;
				if (arrTmpLength > 0 && arrTmpLength != invisibleLenght) {
					$('#groupGrid').datagrid("showColumn", "edit");
				} else {
					$('#groupGrid').datagrid("hideColumn", "delete");
				}
			});
		}
    });
	
	window.onscroll = function(ev) {
	    if ($(window).scrollTop() == ($(document).height() - $(window).height())) {
	    	if($("#kmLevel").is(':visible') && !PromotionProgram.isButtonLoad) {
	    		if(!PromotionProgram.isEndLoad && PromotionProgram.isFinishLoad) {
	    			PromotionProgram.isButtonLoad = true;	
	    			$('#levelInfoMessage').hide();
	    			var params = {
	    					groupMuaId : PromotionProgram.groupMuaId,
	    					groupKMId : PromotionProgram.groupKMId,
	    					fromLevel : PromotionProgram.fromLevel,
	    					toLevel : PromotionProgram.toLevel
	    			};
	    			VTUtilJS.getFormJson(params, '/promotion/new-list-map-level', function(data) {
	    				if(PromotionProgram.isEndLoad || !PromotionProgram.isFinishLoad) {
	    					return;
	    				}
	    				if(data.listNewMapping != null && data.listNewMapping != undefined && $.isArray(data.listNewMapping) && data.listNewMapping.length == 0) {
	    					PromotionProgram.isEndLoad = true;
	    					return;
	    				}
	    				$.messager.show({
					        title:'Cơ cấu CTKM',
					        msg:'Bạn đang xem cơ cấu CTKM từ mức <span style="color:red; font-weight: bold;">' + Utils.XSSEncode(params.fromLevel) + '</span> đến mức <span style="color:red; font-weight: bold;">' + (params.fromLevel + data.listNewMapping.length-1) + '</span>',
					        timeout:3500,
					        showType:'slide'
					    });
						PromotionProgram.fromLevel = Number(params.toLevel) + 1;
						PromotionProgram.toLevel = Number(params.toLevel) + data.listNewMapping.length;
						PromotionProgram.isFinishLoad = false;
						PromotionProgram.renderPannel(data.listNewMapping, 0, PromotionProgram.groupMuaId, PromotionProgram.groupMuaId);
						PromotionProgram.isButtonLoad = false;
					});	    			
	    		}
		  	}
	    }
	};
});
</script>
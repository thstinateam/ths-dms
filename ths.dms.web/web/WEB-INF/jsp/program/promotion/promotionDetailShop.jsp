<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>

<input type="hidden" id="function_code" value="<s:property value="systimestamp"/>" />
<s:set id="downloadToken" value="%{generateReportToken()}"></s:set>
<input type="hidden" id="report_token" value="<s:property value="#downloadToken"/>" />
<s:set id="downloadToken" value="%{putSessionValue(systimestamp, #downloadToken)}"></s:set>

<style type="text/css">
.qttClass {	text-align:right;
	border:1px solid #c4c4c4;
	height:20px;
	line-height:20px;
	padding:0 4px;
	width:110px;
}
</style>

<div id="boxSearch2">
	<h2 class="Title2Style">Thông tin tìm kiếm đơn vị</h2>

	<div class="SearchInSection SProduct1Form" id="divSearchShop">
		<label class="LabelStyle Label1Style" id="shopCodeLabel">Mã đơn vị</label>
		<input id="shopCode" type="text" class="InputTextStyle InputText1Style" value="<s:property value="psMap.shop.shopCode"/>" maxlength="50"/>
		<label class="LabelStyle Label2Style">Tên đơn vị</label>
		<input id="shopName" type="text" class="InputTextStyle InputText1Style" value="<s:property value="psMap.shop.shopName"/>"/>
		<label class="LabelStyle Label1Style">Số suất KM</label>
		<input id="quantityMax" type="text" class="InputTextStyle InputText1Style" value="<s:property value="psMap.quantityMax"/>" maxlength="9"/>
		<div class="Clear"></div>
		
		<div class="BtnCenterSection">
			<button id="btnSearch" class="BtnGeneralStyle Sprite2" onclick="PromotionProgram.searchShop();"><span class="Sprite2">Tìm kiếm</span></button>
		 	<img id="loading" class="LoadingStyle" style="visibility: hidden;" src="/resources/images/loading.gif">
		</div>
		<p id="errMsgSearchShop" style="display: none;" class="ErrorMsgStyle SpriteErr"></p>
	</div>
	<h2 class="Title2Style">Danh sách đơn vị</h2>
	<div class="SearchInSection SProduct1Form">
		<div class="GridSection" id="promotionShopGrid">
			<table id="exGrid"></table> 
		</div>
		<div class="Clear"></div>
		<p id="errMsgShop" class="ErrorMsgStyle" style="display: none"></p>
		<p id="successMsgShop" class="SuccessMsgStyle" style="display: none;"></p>
	</div>
</div>
<div class="SearchInSection SProduct1Form">
	<div class="GeneralForm GeneralNoTP1Form">
		<div class="Func1Section cmsiscontrol" id="tab_shop_gr_change_div_import_shop">
			<p class="DownloadSFileStyle DownloadSFile2Style"><a id="downloadTemplateLevel" href="/resources/templates/catalog/Bieu_mau_danh_muc_CTKM_Donvi.xls" class="Sprite1">Tải file excel mẫu</a></p>
			<div class="DivInputFile" id="importDiv">
				<form action="/promotion/import-promotion-shop" name="importFrm" id="importFrm"  method="post" enctype="multipart/form-data">
					<input type="hidden" name="token" id="tokenImport" value='<s:property value="token"/>'>										
			    	<input type="file" class="InputFileStyle" size="10" style="width:170px;" readonly="readonly" name="excelFile" id="excelFile" onchange="previewImportExcelFile(this,'importFrm');">
			    	<input type="hidden" id="promotionIdImport" name="promotionId" value="<s:property value="promotionId" />" />
					<div class="FakeInputFile">
						<input id="fakefilepc" type="text" class="InputTextStyle">
					</div>
				</form>
			</div>
			<button id="btnImport" class="BtnGeneralStyle" onclick="PromotionProgram.importPromotionShop();">Nhập từ Excel</button>
		</div>
		<button id="btnExport" class="BtnGeneralStyle" onclick="PromotionProgram.exportPromotionShop();">Xuất Excel</button>
		<div class="Clear"></div>
	</div>
	<div class="Clear"></div>
	<p id="errExcelMsgShop" class="ErrorMsgStyle" style="display: none;margin-top:10px;margin-left: 10px;padding: 2px 0 5px 9px;"></p>
</div>


<%-- script --%>
<script type="text/javascript">
$(document).ready(function(){
	$('#divSearchShop input').bind('keyup', function(event) {
		if (event.keyCode == keyCodes.ENTER) {
			$('#btnSearch').click();
		}
	});
	var status = $("#promotionStatus").val().trim();
	if (status != 2) {
		$("#downloadTemplateLevel").remove();
		$("#importDiv").remove();
		$("#btnImport").remove();
	}
	var pms = true;
	
	var vtitle = "";
	if (status == 2) {
		vtitle = "<a href='javascript:void(0);' onclick='PromotionProgram.showShopDlg();'><img title='Thêm mới' width='17' height='17' src='/resources/images/icon-add.png'></a>";
	}
	
	$("#exGrid").treegrid({
		url: "/promotion/search-shop-of-promotion",
		queryParams: {promotionId:$("#masterDataArea #id").val().trim()},
		rownumbers: false,
		width: $("#promotionShopGrid").width(),
		height: "auto",
		fitColumns: true,
		idField: "nodeId",
		treeField : "text",
		scrollbarSize:0,
		columns: [[
			{field:"no", title:"STT", sortable: false, resizable: false, width: 45, fixed:true, align:"center", formatter:function(v, r) {
				return "";
			}},
			{field:"text", title:"Đơn vị", sortable: false, resizable: false, width: 250, align:"left", formatter:CommonFormatter.formatNormalCell},
			{field:"quantity", title:"Số suất KM", sortable: false, resizable: false, width: 90, fixed:true, align:"right", formatter: function(v, r) {
				var val = "";
				v = r.attr.quantity;
				if (v != undefined && v != null) {
					val = formatCurrency(v);
				}
				if (status == 0) {
					return val;
				}
				return "<span id='qtt-"+r.attr.id+"'>"+val+"</span>" + "<input type='text' id='txt"+r.attr.id+"' value='"+val+"' style='display:none; width:75px;' class='qttClass vinput-money' maxlength='11' />";
			}},
			{field:"isEdit", title:"Sửa số suất KM", sortable: false, resizable: false, width: 105, fixed:true, align:"right", formatter: function(v, r) {
				var val = "";
				v = r.attr.isEdit;
				if (v != undefined && v != null) {
					val = (v != null && v != undefined && v == 1) ? 1 : 0;
				}
				if (status == 0) {
					if(r.attr.isNPP == 1){
						return val;						
					} else {
						return '';
					}
				}
				if(r.attr.isNPP == 1){
					if(val == 1) {
						return "<span id='ck-"+r.attr.id+"'>"+val+"</span>" + "<input type='checkbox' checked='checked' id='ck"+r.attr.id+"' value='"+val+"' style='display:none; width:75px;' class='qttClass vinput-money'/>";
					} else {
						return "<span id='ck-"+r.attr.id+"'>"+val+"</span>" + "<input type='checkbox' id='ck"+r.attr.id+"' value='"+val+"' style='display:none; width:75px;' class='qttClass vinput-money'/>";					
					}					
				} else {
					return '';
				}
			}},
			{field:"receivedQtt", title:"Số suất đã KM", sortable: false, resizable: false, width: 105, fixed:true, align:"right", formatter: function(v, r) {
				var val = "0";
				v = r.attr.receivedQtt;
				if (v != undefined && v != null) {
					val = formatCurrency(v);
				}
				return val;
			}},
			{field:"amountMax", title:"Số tiền KM", sortable: false, resizable: false, width: 90, fixed:true, align:"right", formatter: function(v, r) {
				var val = "";
				v = r.attr.amountMax;
				if (v != undefined && v != null) {
					val = formatCurrency(v);
				}
				if (status == 0) {
					return val;
				}
				return "<span id='amt-"+r.attr.id+"'>"+val+"</span>" + "<input type='text' id='txtamt"+r.attr.id+"' value='"+val+"' style='display:none; width:75px;' class='qttClass vinput-money' maxlength='11' />";
			}},
			{field:"receivedAmt", title:"Số tiền đã KM", sortable: false, resizable: false, width: 105, fixed:true, align:"right", formatter: function(v, r) {
				var val = "0";
				v = r.attr.receivedAmt;
				if (v != undefined && v != null) {
					val = formatCurrency(v);
				}
				return val;
			}},
			{field:"numMax", title:"Số lượng KM", sortable: false, resizable: false, width: 90, fixed:true, align:"right", formatter: function(v, r) {
				var val = "";
				v = r.attr.numMax;
				if (v != undefined && v != null) {
					val = formatCurrency(v);
				}
				if (status == 0) {
					return val;
				}
				return "<span id='num-"+r.attr.id+"'>"+val+"</span>" + "<input type='text' id='txtnum"+r.attr.id+"' value='"+val+"' style='display:none; width:75px;' class='qttClass vinput-money' maxlength='11' />";
			}},
			{field:"receivedNum", title:"Số lượng đã KM", sortable: false, resizable: false, width: 105, fixed:true, align:"right", formatter: function(v, r) {
				var val = "0";
				v = r.attr.receivedNum;
				if (v != undefined && v != null) {
					val = formatCurrency(v);
				}
				return val;
			}},
			{field:"edit", title:vtitle, sortable: false, resizable: false, width: 30, fixed:true, align:"center", formatter: function(v, r) {
				if (pms) {
		        	if (r.attr.isNPP == 1) {
		        		if (status == 1 || status == 2) {
		        			return '<a href="javascript:void(0);" id="link'+r.attr.id+'" onclick="edit('+r.attr.id+','+r.attr.quantity+', '+r.attr.amountMax+', '+r.attr.numMax+', '+r.attr.isEdit+');"><img title="Sửa" width="17" height="17" src="/resources/images/icon-edit.png"></a>';
		        		}
		        	} else {
		        		if (status == 2) {
		        			return '<a href="javascript:void(0);" onclick="PromotionProgram.showShopDlg('+r.attr.id+');"><img title="Thêm mới" width="17" height="17" src="/resources/images/icon-add.png"></a>';
		        		}
		        	}
	        	}
			}},
			{field:"delete", title:"", sortable: false, resizable: false, width: 30, fixed:true, align:"center", formatter: function(v, r) {
				if(pms){
	        		if(status == 2){
	        			return '<a id="delete'+r.attr.id+'" href="javascript:void(0);" onclick="PromotionProgram.deleteShopMap('+r.attr.id+');"><img title="Xóa" width="17" height="17" src="/resources/images/icon-delete.png"></a>'
	        			+'<a style="display: none;" id="linkesc'+r.attr.id+'" href="javascript:void(0);"><img title="Xóa" width="17" height="17" src="/resources/images/icon_esc.png"></a>';
	        		}
	        	}
				return '';
			}}
		]],
		onLoadSuccess: function(data) {
			var i = 1;
			$("#promotionShopGrid .datagrid-btable td[field=no] div").each(function() {
				$(this).html(i); // danh STT
				i++;
			});
			$('.vinput-money').each(function() {
				VTUtilJS.bindFormatOnTextfield($(this), VTUtilJS._TF_NUMBER_COMMA);
				VTUtilJS.formatCurrencyFor($(this).prop('id'));
			});
			PromotionProgram._mapTabProcess = new Map();
			creatMapTabProcessShopInJoin($("#exGrid").treegrid('getRoot'));
			var arrEdit =  $('#promotionShopGrid td[field="edit"]');
			if (arrEdit != undefined && arrEdit != null && arrEdit.length > 0) {
			  for (var i = 0, size = arrEdit.length; i < size; i++) {
			  	$(arrEdit[i]).prop("id", "tab_shop_gr_change_gr_shop_td_edit_" + i);//Khai bao id danh cho phan quyen
				$(arrEdit[i]).addClass("cmsiscontrol");
			  }
			}
			var arrDelete =  $('#promotionShopGrid td[field="delete"]');
			if (arrDelete != undefined && arrDelete != null && arrDelete.length > 0) {
			  for (var i = 0, size = arrDelete.length; i < size; i++) {
			    $(arrDelete[i]).prop("id", "tab_shop_gr_change_gr_shop_td_del_" + i);//Khai bao id danh cho phan quyen
				$(arrDelete[i]).addClass("cmsiscontrol");
			  }
			}
			Utils.functionAccessFillControl('promotionShopGrid', function(data){
				//Xu ly cac su kien lien quan den cac control phan quyen
				var arrTmpLength =  $('#promotionShopGrid td[id^="tab_shop_gr_change_gr_shop_td_del_"]').length;
				var invisibleLenght = $('.isCMSInvisible[id^="tab_shop_gr_change_gr_shop_td_del_"]').length;
				if (arrTmpLength > 0 && arrTmpLength != invisibleLenght) {
					$('#exGrid').treegrid("showColumn", "delete");
				} else {
					$('#exGrid').treegrid("hideColumn", "delete");
				}
				arrTmpLength =  $('#promotionShopGrid td[id^="tab_shop_gr_change_gr_shop_td_edit_"]').length;
				invisibleLenght = $('.isCMSInvisible[id^="tab_shop_gr_change_gr_shop_td_edit_"]').length;
				if (arrTmpLength > 0 && arrTmpLength != invisibleLenght) {
					$('#exGrid').treegrid("showColumn", "edit");
				} else {
					$('#exGrid').treegrid("hideColumn", "edit");
				}
			});
		}
	});
});

/**
 * Tao Map tam de su dung cho edit
 * @author hunglm16
 * @param node
 * @return Tra ve mot Map danh sach cac don vi va thuoc tinh lien quan theo danh sach tim kiem
 * @since September 08, 2015
 */
function creatMapTabProcessShopInJoin (node) {
	//Kiem tra diem dung cua de quy
	if (node == null || node == undefined || node.attr == null || node.attr == undefined) {
		return;
	}
	PromotionProgram._mapTabProcess.put(node.attr.id, node.attr);
	//Xu ly goi de quy
	creatMapTabProcessShopInJoin(node.children);
}

function edit(id, qtt, amt, num, isEdit) {
	$('.ErrorMsgStyle').hide();
	$("#qtt-"+id).hide();
	$("#amt-"+id).hide();
	$("#num-"+id).hide();
	$('#txt'+id).show();
	$('#txtamt'+id).show();
	$('#txtnum'+id).show();
	$("#ck-"+id).hide();
	$('#ck'+id).show();
	
	$("#delete"+id).hide();
	$('#linkesc'+id).show();
	if (PromotionProgram._mapTabProcess != undefined && PromotionProgram._mapTabProcess != null && PromotionProgram._mapTabProcess.size > 0) {
		var attr = PromotionProgram._mapTabProcess.get(id);
		if (attr != null && attr != undefined) {
			$('#txt'+id).val(formatCurrency(attr.quantity));
			$('#txt'+id).val(formatCurrency(attr.amountMax));
			$('#txt'+id).val(formatCurrency(attr.numMax));
		}
	} else {
		$('#txt'+id).val(formatCurrency(qtt));
		$('#txtamt'+id).val(formatCurrency(amt));
		$('#txtnum'+id).val(formatCurrency(num));		
	}
	if(isEdit == 1) {
		$('#ck'+id).attr('checked', 'checked');
	}
	$('#link'+id).attr('onclick','PromotionProgram.updateShopQuantity('+id+');');
	$('#link'+id).html('<img width="17" height="17" src="/resources/images/icon-save.png" title="Lưu"/>');
	$('#linkesc'+id).attr('onclick','esc('+id+','+qtt+', '+amt+', '+num+', '+isEdit+');');
}

function esc(id,qtt, amt, num, isEdit){
	$('.ErrorMsgStyle').hide();
	$('#txt'+id).hide();
	$('#txtamt'+id).hide();
	$('#txtnum'+id).hide();
	$("#qtt-"+id).show();
	$("#amt-"+id).show();
	$("#num-"+id).show();
	$('#ck'+id).hide();
	$("#ck-"+id).show();
  
	$("#delete"+id).show();
	$('#linkesc'+id).hide();

   $('#link'+id).attr('onclick','edit('+id+','+qtt+', '+amt+', '+num+', '+isEdit+');');
   $('#link'+id).html('<img width="17" height="17" src="/resources/images/icon-edit.png" title="Sửa"/>');
   $('#linkesc'+id).removeAttr('onclick');
}

function _formatCurrencyFor(idInput) {
	$('#'+idInput).bind('keyup', function(e) {
		var valMoneyInput = $('#'+idInput).val();
		valMoneyInput = Utils.returnMoneyValue(valMoneyInput);
		if(isNaN(valMoneyInput) || valMoneyInput.length == 0) {
			$('#'+idInput).val("");
		} else {
			var _valMoneyInput = formatCurrency(valMoneyInput);
			$('#'+idInput).val(_valMoneyInput);
		}
	});
	$('#'+idInput).bind('paste', function(){
	    var tm = setTimeout(function(){
	    	var valMoneyInput = $('#'+idInput).val();
	        valMoneyInput = Utils.returnMoneyValue(valMoneyInput);
	        if(isNaN(valMoneyInput) || valMoneyInput.length == null) {
	        	$('#'+idInput).val("");
	        } else {
	        	var _valMoneyInput = formatCurrency(valMoneyInput);
	        	$('#'+idInput).val(_valMoneyInput);
	        }
	        clearTimeout(tm);
	    },200);
	});
}
</script>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>

<input type="hidden" id="function_code" value="<s:property value="systimestamp"/>" />
<s:set id="downloadToken" value="%{generateReportToken()}"></s:set>
<input type="hidden" id="report_token" value="<s:property value="#downloadToken"/>" />
<s:set id="downloadToken" value="%{putSessionValue(systimestamp, #downloadToken)}"></s:set>

<div class="BreadcrumbSection">
	<ul class="ResetList FixFloat BreadcrumbList">
        <li class="Sprite1"><a href="#">Danh mục</a></li>
        <li><span>CTCK mặc định</span></li>
    </ul>
</div>

<div class="CtnOneColSection">
   <div class="ContentSection">
   		<div class="ToolBarSection">
           <div class="SearchSection GeneralSSection">
	           	<h2 class="Title2Style">Thông tin tìm kiếm</h2>
	           	<div class="SearchInSection SProduct1Form" id="searchForm">
           			<label class="LabelStyle Label1Style" id="labelPromotionCode">Mã CTCK</label>
	              	<input id="promotionCode" type="text" class="InputTextStyle InputText1Style" maxlength="50"/>
	              	<label class="LabelStyle Label1Style" id="labelPromotionName">Tên CTCK</label>
	              	<input id="promotionName" type="text" class="InputTextStyle InputText1Style" maxlength="100"/>
	              	<!-- <label class="LabelStyle Label1Style">Đơn vị</label>
	              	<div class="BoxSelect BoxSelect2">
		              	<input type="text" id="shopTree" class="InputTextStyle InputText1Style" />
			 			<input type="hidden" id="shopCode"/>
		 			</div> -->
		 			<label class="LabelStyle Label1Style">Trước/Sau Vat</label>
					<div class="BoxSelect BoxSelect2" id ="typeVat">
		                <select class="MySelectBoxClass" id="calPriceVat" >
		                     <option value="-1" selected="selected">Tất cả</option>
		                     <option value="0">Trước Vat</option>
		                     <option value="1">Sau Vat</option>
		                </select>
	                </div>          	
	              	<div class="Clear"></div>
	              	<label class="LabelStyle Label1Style" id="labelPromotionType">Loại CK</label>
	              	<div class="BoxSelect BoxSelect2">
		           		<select id="typeCode" name="LevelSchool" multiple="multiple"> 
		               		<option value="-1" selected="selected">---Tất cả---</option>
		               		<s:iterator value="lstTypeCode">
		               			<option value="<s:property value="id"/>"><s:property value="apParamCode"/>-<s:property value="value"/></option>
		               		</s:iterator>
		               </select>
	              	</div>
<%-- 	              	<label class="LabelStyle Label1Style">Loại CK</label>
					<div class="BoxSelect BoxSelect2" id ="typePromotion">
		                <select class="MySelectBoxClass" id="isTypePromotion" >
		                     <option value="-1" selected="selected">Tất cả</option>
		                     <option value="0">CK dòng hàng</option>
		                     <option value="1">CK đơn hàng</option>
		                </select>
	                </div>   --%>      
	              	<label class="LabelStyle Label1Style">Từ ngày</label>
	              	<input id="fromDate" type="text" class="InputTextStyle InputText6Style vinput-date"/>
	              	<label class="LabelStyle Label1Style">Đến ngày</label>
	              	<input id="toDate" type="text" class="InputTextStyle InputText6Style vinput-date"/>
	              	<div class="Clear"></div>
	               	<%-- <label class="LabelStyle Label1Style">Trạng thái</label>
					<div class="BoxSelect BoxSelect2">
						<select id="status" class="MySelectBoxClass">
	                       <option value="-2">-- Tất cả --</option>
	                       <option value="0">Tạm ngưng</option>
	                       <option value="1" selected="selected">Hoạt động</option>
	                       <option value="2">Dự thảo</option>
	                       <option value="8">Hết hạn</option>
	                   </select>
	               	</div> --%>
	               	<label class="LabelStyle Label1Style" id="labelDiscount">% Chiết khấu</label>
	              	<input id="discountPercent" type="text" class="InputTextStyle InputText1Style" maxlength="50"/>
	               	<div class="Clear"></div>	               	
	              <div class="BtnCenterSection">
	              	<button id="btnSearch" class="BtnGeneralStyle Sprite2" onclick="DefaultPromotionProgram.searchDefaultProgram();"><span class="Sprite2">Tìm kiếm</span></button><%--$('#grid').datagrid('load', VTUtilJS.getFormData('searchForm'));--%>
	              </div>
	              <p id="errorMsgSearch" class="ErrorMsgStyle" style="display: none"></p>
	           	</div>
	           	<div class="SearchInSection SProduct1Form">
	           	<h2 class="Title2Style">Danh sách CTCK mặc định</h2>
	           		<div class="GridSection" id="promotionGrid">
	                  	<div id="grid"></div>
	              	</div>
	           	</div>
           </div>
      	</div>
      	<div id="divDetailCommon" class="GeneralCntSection" style="display: none;">
        </div>
   </div>
</div>
<div class="SearchInSection SProduct1Form">
	<div class="GeneralForm GeneralNoTP1Form">
		<div class="Func1Section" style="width : 580px; font-size: 0.75em;"> 
			<div class="cmsiscontrol" id="group_insert_div_import">
		        <p class="DownloadSFileStyle DownloadSFile2Style">
		            <a id="downloadTemplate" href="javascript:void(0)" onclick="PromotionProgram.downloadImportTemplate();" class="Sprite1" >Tải mẫu file excel</a></p>
	            <div class="DivInputFile">
					<form action="/promotion/import" name="importFrm" id="importFrm"  method="post" enctype="multipart/form-data">
			            <input type="file" class="InputFileStyle" size="12" name="excelFile" id="excelFile" onchange="previewImportExcelFile(this,'importFrm');" style="width:240px !important;">
			            <div class="FakeInputFile">
			            <input id="fakefilepc" type="text" readonly="readonly" class="InputTextStyle" style="width:220px;" />
			            </div>
					</form>
		        </div>	     			                          
				<button id="btnImport" class="BtnGeneralStyle" onclick="PromotionProgram.importExcel()"><span class="Sprite2">Nhập từ Excel</span></button>
			</div>
			<button style="float:right" onclick="return PromotionProgram.exportExcel();" class="BtnGeneralStyle">Xuất Excel</button>
		    <div class="Clear"></div>
        </div>
	</div>		
</div>
<tiles:insertTemplate template="/WEB-INF/jsp/general/successMsg.jsp" />

<input type="hidden" id="proType" value="<s:property value='proType' />" />
<input type="hidden" id="isVNMAdmin" value="<s:property value='isVNMAdmin' />" /> 
<script type="text/javascript">
$(document).ready(function(){
	//$('#downloadTemplate').attr('href', excel_template_path + 'catalog/Bieu_mau_danh_muc_import_CTKM.xlsx');
	$('#typeCode').dropdownchecklist({
		emptyText:'Tất cả',
		firstItemChecksAll: true,
		maxDropHeight: 250,
		onItemClick:function(checkbox, selector){
		},
		textFormatFunction: function(options) {
	        var selectedOptions = options.filter(":selected");
		    var text = '';
	        if(selectedOptions.size() > 0 && parseInt(selectedOptions[0].value) == -1){
	        	text = 'Tất cả';
	        }else{
		        for(var i = 0;i<selectedOptions.size();i++){
			        if(text.length > 0){
			        	text += ',';
			        }
			        text += selectedOptions[i].text.split('-')[0];
		        }
	        }
	        return text;
		}
	});
	
	var tlb = $('#proType').val().trim() == 1 ? "CTCK" : "CTHTTM";
	$("#grid").datagrid({
		url: "/promotion-default/search",
		queryParams: {proType:$('#proType').val(), status:1},
		width: $("#promotionGrid").width()-10,
		height: 'auto',
		singleSelect: true,
		rownumbers: true,
		pagination: true,
		fitColumns: true,
		scrollbarSize: 0,
		columns: [[
			{field:'promotionProgramCode', title:"Mã " + tlb, width:150,align:'left',sortable:false,resizable:false, function(val, row, index) {
				return VTUtilJS.XSSEncode(val);
			}},
			{field:'promotionProgramName', title:"Tên "+tlb, width:270,align:'left',sortable:false,resizable:false, function(val, row, index) {
				return VTUtilJS.XSSEncode(val);
			}},
			{field:'type', title:"Loại "+tlb, width:200,align:'left',sortable:false,resizable:false,formatter:function(value,row,index){
				if(row.type != null ||row.type != undefined ){
					return Utils.XSSEncode(row.type + ' - ' + row.proFormat);
				} else {
					return '';
				}
			}},
			{field:'fromDate', title:"Từ ngày", width:80,align:'center',sortable:false,resizable:false,formatter:function(value,row,index){
				if(VTUtilJS.isNullOrEmpty(value)){
					return '';
				} else {
					return $.datepicker.formatDate('dd/mm/yy', new Date(value));
				}
			}},
			{field:'toDate', title:"Đến ngày", width:80,align:'center',sortable:false,resizable:false,formatter:function(value,row,index){
				if(VTUtilJS.isNullOrEmpty(value)){
					return '';
				} else {
					return $.datepicker.formatDate('dd/mm/yy', new Date(value));
				}
			}},
			{field:'calPriceVat', title:"Trước/Sau VAT", width:100,align:'center',sortable:false,resizable:false,formatter:function(value,row,index){
				if(!VTUtilJS.isNullOrEmpty(row.calPriceVat)){
					if(row.calPriceVat == 0){
						return 'Trước VAT';
					}else if(row.calPriceVat == 1){
						return 'Sau VAT';
					}else{
						return '';
					}
				}else{
					return '';
				}
			}},
			{field:'view', title: '<a class="cmsiscontrol" id="group_insert_btnadd_grid" href="/promotion/detail"><span style="cursor:pointer"><img title="Thêm mới" src="/resources/images/icon_add.png"/></span></a>',
				width:35, fixed:true, align:'center',sortable:false,resizable:false,formatter:PromotionProgram.gridRowIconFormatter}
		]],
		onLoadSuccess: function(data) {
			$('.datagrid-header-rownumber').html('STT');
			GridUtil.updateDefaultRowNumWidth("#promotionGrid", "grid");
			Utils.functionAccessFillControl('promotionGrid', function(data) {
				//Xu ly cac su kien lien quan den control duoc phan quyen
			});
		}
	});
	
	//load combobox don vi
	$('#shopTree').combotree({
		url: '/commons/search-unit-tree',
		lines: true,
		panelWidth: 206,
		formatter: function(node) {
			return Utils.XSSEncode(node.text);
		},
		onSelect: function(data){
        	if (data != null) {
        		if (data.id < 0 ) {
        			$('#shopCode').val("");
        		} else {
        			$('#shopCode').val(data.attributes.shop.shopCode);
        		}        		
        	}
	    },
	    onLoadSuccess: function(node, data) {
			$('#shopTree').combotree('setValue', data[0].id);
		}
	});
	
	$("#promotionCode").parent().unbind("keyup");
	$("#promotionCode").parent().bind("keyup", function(event) {
		if (event.keyCode == keyCodes.ENTER) {
			$("#btnSearch").click();
		}
	});
});
</script>
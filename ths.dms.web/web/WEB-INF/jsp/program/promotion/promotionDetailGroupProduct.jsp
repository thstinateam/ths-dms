<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<div id="groupProductTab">
    <div title="Khai báo nhóm">
    	<h2 class="Title2Style">Danh sách nhóm mua</h2>
    	<div class="GridSection" id="groupMuaGridContainer">
			<div id="groupMuaGrid"></div>
	    </div>
	    <h2 class="Title2Style">Danh sách nhóm khuyến mại</h2>
	    <div class="GridSection" id="groupKMGridContainer">
			<div id="groupKMGrid"></div>
	    </div>
    </div>
    <div title="Cơ cấu khuyến mại">
    	<div class="SearchInSection" style="display:none;">
	    	<div class="BoxSelect BoxSelect2">
			   	<select id="cbGroupMua">
					
		       	</select>
			</div>
			<div class="BoxSelect BoxSelect2">
			   	<select id="cbGroupKM">
					
		       	</select>
			</div>
			<button id="btnDistribution" class="BtnGeneralStyle Sprite2" onclick="return PromotionProgram.distributeLevel();"><span class="Sprite2">Tự động phân bổ</span></button>
		</div>
		<div class="Clear"></div>
		<p class="ErrorMsgStyle SpriteErr" id="errorMsgDistribute" style="display: none;"></p>
		<div class="Clear"></div>
    	<div class="GridSection" id="mappingGridContainer">
    		<h2 class="Title2Style">Danh sách cơ cấu khuyến mại</h2>
			<div id="mappingGrid"></div>
	    </div>
    </div>
</div>
<script type="text/javascript">
$(document).ready(function(){
	/* if($('#statusHidden').val() == 0 || $('#statusHidden').val() == 1) {
		$('#btnDistribution').hide();
	} */
    $('#groupProductTab').tabs({
    	title:'New Tab',
    	width:$('#divTab').width(),
    	onSelect : function(title,index) {
    		if(index == 0) {
    			$('#groupMuaGrid').datagrid({
    		    	url: "/promotion/detail-group-sale",
    				queryParams: {id:$('#id').val()},
    				width: $("#groupMuaGridContainer").width(),
    				height: 'auto',
    				singleSelect: true,
    				fitColumns: true,
    				scrollbarSize:0,
    				columns: [[
    					{field:'id', title:"Nhóm", width:20,align:'center',sortable:false,resizable:false,formatter:function(value,row,index) {
    						return index + 1;
    					}},
    					{field:'productGroupCode', title:"Mã nhóm", width:100,align:'left',sortable:false,resizable:false,formatter:CommonFormatter.formatNormalCell},
    					{field:'productGroupName', title:"Tên nhóm", width:210,align:'left',sortable:false,resizable:false,formatter:CommonFormatter.formatNormalCell},
    					{field:'minQuantity', title:"Số lượng tối thiểu", width:50,align:'right',sortable:false,resizable:false,formatter:function(value, row, index){
    						if(value == 0 || value == 'null' || VTUtilJS.isNullOrEmpty(value)) {
    							return '';
    						} else {
    							return CommonFormatter.formatNormalCell(value, row, index);
    						}
    					}},
    					{field:'minAmount', title:"Số tiền tối thiểu", width:50,align:'right',sortable:false,resizable:false,formatter:function(value, row, index){
    						if(value == 0 || value == 'null' || VTUtilJS.isNullOrEmpty(value)) {
    							return '';
    						} else {
    							return CommonFormatter.formatNormalCell(value, row, index);
    						}
    					}},
    					{field:'level', title:"", width:30,align:'center',sortable:false,resizable:false,formatter:function(value,row,index){
    						return '<a onclick="PromotionProgram.popupAddLevelMua('+row.id+', \''+row.productGroupCode+'\', \''+row.productGroupName+'\', '+(VTUtilJS.isNullOrEmpty(row.minQuantity) ?'\'\'':row.minQuantity)+', '+(VTUtilJS.isNullOrEmpty(row.minAmount)?'\'\'':row.minAmount)+', '+row.multiple+', '+row.recursive+', '+(VTUtilJS.isNullOrEmpty(row.order)?'\'\'':row.order)+')"><span style="cursor:pointer"><img title="Thêm mức bán" src="/resources/images/signal.png"/></span></a>';
    					}},
    					{field:'edit', title:"", width:30,align:'center',sortable:false,resizable:false,formatter:function(value,row,index){
    						return '<a '+(($('#statusHidden').val() == 0 || $('#statusHidden').val() == 1) ? 'style="display:none;"' : '')+' onclick="PromotionProgram.popupEditGroupMua('+row.id+', \''+row.productGroupCode+'\', \''+row.productGroupName+'\', '+(VTUtilJS.isNullOrEmpty(row.minQuantity) ?'\'\'': row.minQuantity)+', '+(VTUtilJS.isNullOrEmpty(row.minAmount) ?'\'\'':row.minAmount)+', '+row.multiple+', '+row.recursive+', '+(VTUtilJS.isNullOrEmpty(row.order)?'\'\'':row.order)+')"><span style="cursor:pointer"><img title="Sửa nhóm mua" src="/resources/images/icon_edit.png"/></span></a>';
    					}}
    				]],
    				onLoadSuccess: function(data) {
    				}
    		    });
    		    
    		    $('#groupKMGrid').datagrid({
    		    	url: "/promotion/detail-group-free",
    				queryParams: {id:$('#id').val()},
    				width: $("#groupKMGridContainer").width(),
    				height: 'auto',
    				singleSelect: true,
    				fitColumns: true,
    				scrollbarSize:0,
    				columns: [[
    					{field:'id', title:"Nhóm", width:20,align:'center',sortable:false,resizable:false,formatter:function(value,row,index) {
    						return index + 1;
    					}},
    					{field:'productGroupCode', title:"Mã nhóm", width:100,align:'left',sortable:false,resizable:false,formatter:CommonFormatter.formatNormalCell},
    					{field:'productGroupName', title:"Tên nhóm", width:210,align:'left',sortable:false,resizable:false,formatter:CommonFormatter.formatNormalCell},
    					{field:'maxQuantity', title:"Số lượng tối thiểu", width:50,align:'right',sortable:false,resizable:false,formatter:function(value, row, index){
    						if(value == 0 || value == 'null' || VTUtilJS.isNullOrEmpty(value)) {
    							return '';
    						} else {
    							return CommonFormatter.formatNormalCell(value, row, index);
    						}
    					}},
    					{field:'maxAmount', title:"Số tiền tối thiểu", width:50,align:'right',sortable:false,resizable:false,formatter:function(value, row, index){
    						if(value == 0 || value == 'null' || VTUtilJS.isNullOrEmpty(value)) {
    							return '';
    						} else {
    							return CommonFormatter.formatNormalCell(value, row, index);
    						}
    					}},
    					{field:'level', title:"", width:30,align:'center',sortable:false,resizable:false,formatter:function(value,row,index){
    						return '<a onclick="PromotionProgram.popupAddLevelKM('+row.id+', \''+row.productGroupCode+'\', \''+row.productGroupName+'\', '+(VTUtilJS.isNullOrEmpty(row.maxQuantity) ?'\'\'':row.maxQuantity)+', '+(VTUtilJS.isNullOrEmpty(row.maxAmount)?'\'\'':row.maxAmount)+')"><span style="cursor:pointer"><img title="Thêm mức KM" src="/resources/images/signal.png"/></span></a>';
    					}},
    					{field:'edit', title:"", width:30,align:'center',sortable:false,resizable:false,formatter:function(value,row,index){
    						return '<a '+(($('#statusHidden').val() == 0 || $('#statusHidden').val() == 1) ? 'style="display:none;"' : '')+' onclick="PromotionProgram.popupEditGroupKM('+row.id+', \''+row.productGroupCode+'\', \''+row.productGroupName+'\', '+row.maxQuantity+', '+row.maxAmount+')"><span style="cursor:pointer"><img title="Sửa nhóm KM" src="/resources/images/icon_edit.png"/></span></a>';
    					}}/* ,
    					{field:'delete', title:'<a '+(($('#statusHidden').val() == 0 || $('#statusHidden').val() == 1) ? 'style="display:none;"' : '')+' onclick="PromotionProgram.popupAddGroupKM()"><span style="cursor:pointer"><img title="Thêm nhóm KM" src="/resources/images/icon_add.png"/></span></a>', width:30,align:'center',sortable:false,resizable:false,formatter:function(value,row,index){
    						return '<a '+(($('#statusHidden').val() == 0 || $('#statusHidden').val() == 1) ? 'style="display:none;"' : '')+' onclick="PromotionProgram.deleteProductGroup(\'groupKMGrid\', '+row.id+')"><span style="cursor:pointer"><img title="Xóa nhóm KM" src="/resources/images/icon_delete.png"/></span></a>';
    					}}, */
    				]],
    				onLoadSuccess: function(data) {
    				}
    		    });
    		} else if(index == 1) {
    			$('#errorMsgDistribute').html('').hide();
    			VTUtilJS.getFormJson({id:$('#id').val()}, '/promotion/get-list-group', function(data) {
    				if(!data.error) {
    					var lstGroupSale = data.lstGroupSale;
        				var lstGroupFree = data.lstGroupFree;
        				PromotionProgram._mapGroupMua = new Map();
        				PromotionProgram._mapGroupKM = new Map();
        				PromotionProgram._mapLevelMua = new Map();
        				PromotionProgram._mapLevelKM = new Map();
        				if($.isArray(lstGroupSale) && lstGroupSale.length > 0) {
        					var optHtml = '';
        					for(var i = 0; i < lstGroupSale.length; i++) {
        						optHtml += '<option value="'+lstGroupSale[i].id+'">'+lstGroupSale[i].productGroupName+'</option>';
        						PromotionProgram._mapGroupMua.put(lstGroupSale[i].id, lstGroupSale[i]);
        						PromotionProgram._mapLevelMua.put(lstGroupSale[i].productGroupCode, lstGroupSale[i].listLevel);
        					}
        					$('#cbGroupMua').html('');
        					$('#cbGroupMua').append(optHtml);
        					$('#cbGroupMua').change();
        				}
        				if($.isArray(lstGroupFree) && lstGroupFree.length > 0) {
        					var optHtml = '';
        					for(var i = 0; i < lstGroupFree.length; i++) {
        						optHtml += '<option value="'+lstGroupFree[i].id+'">'+lstGroupFree[i].productGroupName+'</option>';
        						PromotionProgram._mapGroupKM.put(lstGroupFree[i].id, lstGroupFree[i]);
        						PromotionProgram._mapLevelKM.put(lstGroupFree[i].productGroupCode, lstGroupFree[i].listLevel);
        					}
        					$('#cbGroupKM').html('');
        					$('#cbGroupKM').append(optHtml);
        					$('#cbGroupKM').change();
        				}
        				var isFirstLoad = true;
        				$('#mappingGrid').datagrid({
            				url: "/promotion/list-mapping",
            				queryParams: {id:$('#id').val()},
            				width: $("#mappingGridContainer").width(),
            				height: 'auto',
            				singleSelect: true,
            				fitColumns: true,
            				scrollbarSize:0,
            				columns: [[
            					{field:'groupMuaCode', title:"Mã nhóm mua", width:80,align:'left',sortable:false,resizable:false,formatter:CommonFormatter.formatNormalCell, editor : {
            						type:'combobox',
	    							options:{
	   								 	valueField : 'productGroupCode',
	   								 	textField : 'productGroupCode',
	   								 	data : PromotionProgram._mapGroupMua.valArray,
	   								 	formatter:function(row){
	   			                            return row.productGroupCode;
	   			                        },
	   			                     	keyHandler: {
	   			                  			up: function(e){
	   			                  				console.log('up');
	   			                  			},
	   			                  			down: function(e){
	   			                  				console.log('down');
	   			                  			},
	   			                  			left: function(e){
	   			                  				console.log('left');
	   			                  			},
	   			                  			right: function(e){
	   			                  				console.log('right');
	   			                  			},
	   			                  			enter: function(e){
	   			                  				$('#mappingGrid').datagrid('acceptChanges');
	   			                  			},
	   			                  			query: function(q,e){
	   			                  				console.log(q);
	   			                  				console.log(e);
	   			                  			}
	   			                  		},
	   			                     	onChange: function(newVal, oldVal) {
   								 			var index = $(this).parent().parent().parent().parent().parent().parent().parent().attr('datagrid-row-index');
   								 			var orderLevelMuaEd = $('#mappingGrid').datagrid('getEditor', {index:index, field:'orderLevelMua'});
   								 			var listLevel = [];
   								 			if(newVal != oldVal) {
   								 				var groupMuaCodeVal = newVal;
	   								 			if(!VTUtilJS.isNullOrEmpty(groupMuaCodeVal)) {
	   			            						listLevel = PromotionProgram._mapLevelMua.get(groupMuaCodeVal);
	   			            						$(orderLevelMuaEd.target).combobox('loadData', listLevel);
	   			            					}
	   								 			if(!VTUtilJS.isNullOrEmpty(oldVal)) {
	   								 				$('#mappingGrid').datagrid('getRows')[index].mappingId = undefined;	   								 				
	   								 			}
		   								 		$('#mappingGrid').datagrid('getRows')[index].groupMuaCode = newVal;
		   								 		if($.isArray(listLevel) && listLevel.length > 0) {
			   								 		$(orderLevelMuaEd.target).combobox('setValue', listLevel[0].orderNumber);
			   								 		$('tr[datagrid-row-index='+index+'] td[field=groupMuaText] div').html(listLevel[0].groupText);
			   								 		$('tr[datagrid-row-index='+index+'] td[field=minQuantityMua] div').html(VTUtilJS.formatCurrency(listLevel[0].minQuantity));
		   			                     			$('tr[datagrid-row-index='+index+'] td[field=minAmountMua] div').html(VTUtilJS.formatCurrency(listLevel[0].minAmount));		   								 			
		   								 		} else {
		   								 			$('tr[datagrid-row-index='+index+'] td[field=groupMuaText] div').html('');
		   								 			$(orderLevelMuaEd.target).combobox('setValue', null);
			   								 		$('tr[datagrid-row-index='+index+'] td[field=minQuantityMua] div').html('');
		   			                     			$('tr[datagrid-row-index='+index+'] td[field=minAmountMua] div').html('');
		   								 		}
   								 			}
	   								 	}
	    							}
            					}},
            					{field:'groupMuaText', title:"Tên nhóm mua", width:160,align:'left',sortable:false,resizable:false,formatter:CommonFormatter.formatNormalCell},
            					{field:'orderLevelMua', title:"Mức mua", width:50,align:'center',sortable:false,resizable:false,formatter:CommonFormatter.formatNormalCell, editor : {
            						type:'combobox',
	    							options:{
	   								 	valueField : 'orderNumber',
	   								 	textField : 'orderNumber',
	   								 	data : [],
	   								 	formatter:function(row){
	   			                            return row.orderNumber;
	   			                        },
		   			                    keyHandler: {
	   			                  			up: function(e){
	   			                  				console.log('up');
	   			                  			},
	   			                  			down: function(e){
	   			                  				console.log('down');
	   			                  			},
	   			                  			left: function(e){
	   			                  				console.log('left');
	   			                  			},
	   			                  			right: function(e){
	   			                  				console.log('right');
	   			                  			},
	   			                  			enter: function(e){
	   			                  				$('#mappingGrid').datagrid('acceptChanges');
	   			                  			},
	   			                  			query: function(q,e){
	   			                  				console.log(q);
	   			                  				console.log(e);
	   			                  			}
		   			                  	},
	   			                     	onSelect: function(rec){
	   			                     		var index = $(this).parent().parent().parent().parent().parent().parent().parent().attr('datagrid-row-index');
	   			                     		var minQuantity = rec.minQuantity;
	   			                     		var minAmount = rec.minAmount;
	   			                     		var groupText = rec.groupText;
	   			                     		$('tr[datagrid-row-index='+index+'] td[field=minQuantityMua] div').html(groupText);
	   			                     		$('tr[datagrid-row-index='+index+'] td[field=minQuantityMua] div').html(VTUtilJS.formatCurrency(minQuantity));
	   			                     		$('tr[datagrid-row-index='+index+'] td[field=minAmountMua] div').html(VTUtilJS.formatCurrency(minAmount));
	   			                     		$('#mappingGrid').datagrid('getRows')[index].mappingId = undefined;
			   			            	}
	    							}
            					}},
            					{field:'minQuantityMua', title:"Số lượng tối thiểu", width:50,align:'right',sortable:false,resizable:false,formatter:CommonFormatter.formatNormalCell},
            					{field:'minAmountMua', title:"Số tiền tối thiểu", width:70,align:'right',sortable:false,resizable:false,formatter:CommonFormatter.formatNormalCell},
            					{field:'groupKMCode', title:"Mã nhóm KM", width:80,align:'left',sortable:false,resizable:false,formatter:CommonFormatter.formatNormalCell, editor : {
            						type:'combobox',
	    							options:{
	   								 	valueField : 'productGroupCode',
	   								 	textField : 'productGroupCode',
	   								 	data : PromotionProgram._mapGroupKM.valArray,
	   								 	formatter:function(row){
	   			                            return row.productGroupCode;
	   			                        },
		   			                    keyHandler: {
	   			                  			up: function(e){
	   			                  				console.log('up');
	   			                  			},
	   			                  			down: function(e){
	   			                  				console.log('down');
	   			                  			},
	   			                  			left: function(e){
	   			                  				console.log('left');
	   			                  			},
	   			                  			right: function(e){
	   			                  				console.log('right');
	   			                  			},
	   			                  			enter: function(e){
	   			                  				$('#mappingGrid').datagrid('acceptChanges');
	   			                  			},
	   			                  			query: function(q,e){
	   			                  				console.log(q);
	   			                  				console.log(e);
	   			                  			}
			   			               	},
	   			                     	onChange: function(newVal, oldVal) {
		   			                     	var index = $(this).parent().parent().parent().parent().parent().parent().parent().attr('datagrid-row-index');
								 			var orderLevelKMEd = $('#mappingGrid').datagrid('getEditor', {index:index, field:'orderLevelKM'});
								 			var listLevel = [];
								 			if(newVal != oldVal) {
								 				var groupKMCodeVal = newVal;
	   								 			if(!VTUtilJS.isNullOrEmpty(groupKMCodeVal)) { 
	   			            						listLevel = PromotionProgram._mapLevelKM.get(groupKMCodeVal);
	   			            						$(orderLevelKMEd.target).combobox('loadData', listLevel);
	   			            					}
		   								 		if(!VTUtilJS.isNullOrEmpty(oldVal)) {
	   								 				$('#mappingGrid').datagrid('getRows')[index].mappingId = undefined;	   								 				
	   								 			}
		   								 		$('#mappingGrid').datagrid('getRows')[index].groupKMCode = newVal;
		   								 		if($.isArray(listLevel) && listLevel.length > 0) {
			   								 		$(orderLevelKMEd.target).combobox('setValue', listLevel[0].orderNumber);
			   								 		$('tr[datagrid-row-index='+index+'] td[field=groupKMText] div').html(VTUtilJS.formatCurrency(listLevel[0].groupText));
			   								 		$('tr[datagrid-row-index='+index+'] td[field=maxQuantityKM] div').html(VTUtilJS.formatCurrency(listLevel[0].maxQuantityKM));
			   								 		$('tr[datagrid-row-index='+index+'] td[field=maxAmountKM] div').html(VTUtilJS.formatCurrency(listLevel[0].maxAmountKM));
		   			                     			$('tr[datagrid-row-index='+index+'] td[field=percentKM] div').html(VTUtilJS.formatCurrency(listLevel[0].percentKM));		   								 			
		   								 		} else {
		   								 			$(orderLevelKMEd.target).combobox('setValue', null);
			   								 		$('tr[datagrid-row-index='+index+'] td[field=maxQuantityKM] div').html('');
			   								 		$('tr[datagrid-row-index='+index+'] td[field=maxAmountKM] div').html('');
		   			                     			$('tr[datagrid-row-index='+index+'] td[field=percentKM] div').html('');
		   								 		}
								 			}
		   								 }
	    							}
            					}},
            					{field:'groupKMText', title:"Tên nhóm KM", width:160,align:'left',sortable:false,resizable:false,formatter:CommonFormatter.formatNormalCell},
            					{field:'orderLevelKM', title:"Mức mua", width:50,align:'center',sortable:false,resizable:false,formatter:CommonFormatter.formatNormalCell, editor : {
            						type:'combobox',
	    							options:{
	   								 	valueField : 'orderNumber',
	   								 	textField : 'orderNumber',
	   								 	data : [],
	   								 	formatter:function(row){
	   			                            return row.orderNumber;
	   			                        },
	   			                     	keyHandler: {
	   			                  			up: function(e){
	   			                  				console.log('up');
	   			                  			},
	   			                  			down: function(e){
	   			                  				console.log('down');
	   			                  			},
	   			                  			left: function(e){
	   			                  				console.log('left');
	   			                  			},
	   			                  			right: function(e){
	   			                  				console.log('right');
	   			                  			},
	   			                  			enter: function(e){
	   			                  				$('#mappingGrid').datagrid('acceptChanges');
	   			                  			},
	   			                  			query: function(q,e){
	   			                  				console.log(q);
	   			                  				console.log(e);
	   			                  			}
		   			                  	},
	   								 	onSelect: function(rec){
	   			                     		var index = $(this).parent().parent().parent().parent().parent().parent().parent().attr('datagrid-row-index');
	   			                     		var maxQuantityKM = rec.maxQuantityKM;
	   			                     		var maxAmountKM = rec.maxAmountKM;
	   			                     		var percentKM = rec.percentKM;
	   			                     		var groupText = rec.groupText;
	   			                     		$('#mappingGrid').datagrid('getRows')[index].mappingId = undefined;
	   			                     		$('tr[datagrid-row-index='+index+'] td[field=groupKMText] div').html(groupText);
	   			                     		$('tr[datagrid-row-index='+index+'] td[field=maxQuantityKM] div').html(VTUtilJS.formatCurrency(maxQuantityKM));
	   			                     		$('tr[datagrid-row-index='+index+'] td[field=maxAmountKM] div').html(VTUtilJS.formatCurrency(maxAmountKM));
	   			                     		$('tr[datagrid-row-index='+index+'] td[field=percentKM] div').html(VTUtilJS.formatCurrency(percentKM));
			   			            	}
	    							}
            					}},
            					{field:'maxQuantityKM', title:"Số lượng tối đa", width:50,align:'right',sortable:false,resizable:false,formatter:CommonFormatter.formatNormalCell},
            					{field:'maxAmountKM', title:"Số tiền tối đa", width:50,align:'right',sortable:false,resizable:false,formatter:CommonFormatter.formatNormalCell},
            					{field:'percentKM', title:"% KM", width:70,align:'right',sortable:false,resizable:false,formatter:CommonFormatter.formatNormalCell},
            					{field:'delete', hidden:true, title:'<a '+(($('#statusHidden').val() == 0 || $('#statusHidden').val() == 1) ? 'style="display:none;"' : '')+' onclick="PromotionProgram.addMapping()"><span style="cursor:pointer"><img title="Thêm mới" src="/resources/images/icon_add.png"/></span></a>', width:30,align:'center',sortable:false,resizable:false,formatter:function(value,row,index){
            						var key = row.groupMuaCode + '.' + row.orderLevelMua + '-' + row.groupKMCode + '.' + row.orderLevelKM;
            						if((PromotionProgram._listMapping != null && PromotionProgram._listMapping.get(key) != null) || !VTUtilJS.isNullOrEmpty(row.mappingId)) {
            							return '<a '+(($('#statusHidden').val() == 0 || $('#statusHidden').val() == 1) ? 'style="display:none;"' : '')+' onclick="PromotionProgram.deleteMapping('+index+')"><span style="cursor:pointer"><img title="Thêm mức bán" src="/resources/images/icon_delete.png"/></span></a>';
            						} else {
            							return '<a '+(($('#statusHidden').val() == 0 || $('#statusHidden').val() == 1) ? 'style="display:none;"' : '')+' onclick="PromotionProgram.saveMapping('+index+')"><span style="cursor:pointer"><img title="Thêm mức bán" src="/resources/images/icon-save.jpg"/></span></a>';            							
            						}
            					}},
            				]],
            				onDblClickRow : function(index, rowData) {
            					if($('#statusHidden').val() == 0 || $('#statusHidden').val() == 1) {
            						return;
            					}
            					$('#mappingGrid').datagrid('acceptChanges');
            					$('#mappingGrid').datagrid('selectRow', index).datagrid('beginEdit', index);
            					var groupMuaCodeEd = $('#mappingGrid').datagrid('getEditor', {index:index, field:'groupMuaCode'});
            					var orderLevelMuaEd = $('#mappingGrid').datagrid('getEditor', {index:index, field:'orderLevelMua'});
            					var groupKMCodeEd = $('#mappingGrid').datagrid('getEditor', {index:index, field:'groupKMCode'});
            					var orderLevelKMEd = $('#mappingGrid').datagrid('getEditor', {index:index, field:'orderLevelKM'});
            					var groupMuaCodeVal = $(groupMuaCodeEd.target).combobox('getValue');
            					if(!VTUtilJS.isNullOrEmpty(groupMuaCodeVal)) {
            						listLevel = PromotionProgram._mapLevelMua.get(groupMuaCodeVal);
            						$(orderLevelMuaEd.target).combobox('loadData', listLevel);
            					}
            					var groupKMCodeVal = $(groupKMCodeEd.target).combobox('getValue');
            					if(!VTUtilJS.isNullOrEmpty(groupKMCodeVal)) {
            						listLevel = PromotionProgram._mapLevelKM.get(groupKMCodeVal);
            						$(orderLevelKMEd.target).combobox('loadData', listLevel);
            					}
            				},
            				onLoadSuccess: function(data) {
            					PromotionProgram._listMapping = new Map();
            					if($.isArray(data)) {
            						for(var i = 0; i < data.length; i++) {
            							var __row = data[i];
            							if(!VTUtilJS.isNullOrEmpty(__row.mappingId)) {
            								var key = __row.groupMuaCode + '.' + __row.orderLevelMua + '-' + __row.groupKMCode + '.' + __row.orderLevelKM;
                							PromotionProgram._listMapping.put(key, __row);            								
            							}
            						}
            					} else if($.isArray(data.rows)) {
            						for(var i = 0; i < data.rows.length; i++) {
            							var __row = data.rows[i];
            							if(!VTUtilJS.isNullOrEmpty(__row.mappingId)) {
            								var key = __row.groupMuaCode + '.' + __row.orderLevelMua + '-' + __row.groupKMCode + '.' + __row.orderLevelKM;
                							PromotionProgram._listMapping.put(key, __row);            								
            							}
            						}
            					}
            				}
            			});
    				}
    			});
    		}
    	}
	});
});
</script>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<h2 class="Title2Style">Thông tin sản phẩm quy đổi</h2>
<div class="SearchInSection SProduct1Form">
	<div class="GridSection" id="promotionConvertGrid" style="width: 100%;">
		<table id="convertGrid"></table> 
	</div>
	<div class="Clear"></div>
	<s:if test="promotionProgram.getStatus().getValue() == 2">
		<div class="BtnCenterSection">
		<button class="BtnGeneralStyle Sprite2" onclick="PromotionProgram.saveProductConvert();"><span class="Sprite2">Cập nhập</span></button>
	</div>	
	</s:if>
	<p id="errMsgConvert" class="ErrorMsgStyle" style="display: none"></p>
	<p id="successMsgConvert" class="SuccessMsgStyle" style="display: none;"></p>
</div>
<script type="text/javascript">
$(document).ready(function(){
	PromotionProgram._listSaleProduct = new Map();
	<s:iterator value="listProduct">
		var productCode = '<s:property value="productCode"/>';
		PromotionProgram._listSaleProduct.put(productCode, productCode);
	</s:iterator>
	$('#convertGrid').datagrid({
		url: "/promotion/product-convert-load-list-group",
		queryParams: {promotionId : $('#id').val()},
		width: $("#promotionConvertGrid").width(),
		height:'auto',
		singleSelect: true,
		fitColumns: true,
		scrollbarSize:0,
		view: detailview,
        detailFormatter:function(idx,r){
        	return '<div style="padding:2px"><table class="ddv"></table></div>';
        },
        onExpandRow: function(index,row){
        	var ddv = $(this).datagrid('getRowDetail',index).find('table.ddv');
        	ddv.datagrid({
        		data:row.listDetail,
        		fitColumns:true,
        		singleSelect:true,
        		rownumbers:true,
        		scrollbarSize:0,
        		height:'auto',
        		onDblClickRow : function(idx,r) {
        			var __rows = ddv.datagrid('getRows');
        			if(__rows != null && $.isArray(__rows)) {
        				for(var ii = 0; ii < __rows.length; ii++) {
        					if(VTUtilJS.isNullOrEmpty(__rows[ii].productCode)) {
        						return;
        					}
        				}
        			}
        			ddv.datagrid('beginEdit', idx);
        		},
        		columns:[[
        			{field:'productCode', title:'Mã sản phẩm',width:200, align:'left', sortable:false, resizable:false, formatter:CommonFormatter.formatNormalCell, editor: {
						type:'combobox',
						 options:{
						 	valueField : 'productCode',
						 	textField : 'productCode',
						 	data : PromotionProgram._listProduct.valArray,
						 	formatter:function(row){
	                            return Utils.XSSEncode(row.productCode + ' - ' +row.productName);
	                        },
						 	onSelect: function(r) {
						 		var row = $(this).parent().parent().parent().parent().parent().parent().parent();
						 		row.find('td[field=productName] div').html(r.productName);
						 		var detail = ddv.datagrid('getRows')[Number(row.attr('datagrid-row-index'))];
						 		detail.productCode = r.productCode;
						 		detail.productName = r.productName;
						 		ddv.datagrid('getRows').splice(Number(row.attr('datagrid-row-index')), 1, detail);
						 	}
						 }
					}},
        		 	{field:'productName', title:'Tên sản phẩm',width:350, align:'left', sortable:false, resizable:false, formatter:CommonFormatter.formatNormalCell},
        		 	{field:'factor', title:'Hệ số quy đổi', width:50, align:'center', sortable:false, resizable:false, editor: {
						type:'currency', 
						options : {
							maxLength:13,
							blur : function(target) {
								var row = $(target).parent().parent().parent().parent().parent().parent().parent();
						 		var factor = $(target).val();
						 		var detail = ddv.datagrid('getRows')[Number(row.attr('datagrid-row-index'))];
						 		detail.factor = factor;
						 		ddv.datagrid('getRows').splice(Number(row.attr('datagrid-row-index')), 1, detail);
						 		
						 		ddv.datagrid('fixRowHeight', row.attr('datagrid-row-index'));
						 		$('#convertGrid').datagrid('fixRowHeight', index);
							}
						}
					}},
					{field:'isSourceProduct', title:'SP gốc', width:30, align:'center', sortable:false, resizable:false, editor:{
						type:'checkbox', options:{on:'1', off:'0', onCheck:function(target) {
							var row = $(target).parent().parent().parent().parent().parent().parent().parent();
					 		var isSourceProduct = $(target).is(":checked") ? 1 : 0;
					 		var detail = ddv.datagrid('getRows')[Number(row.attr('datagrid-row-index'))];
					 		detail.isSourceProduct = isSourceProduct;
					 		ddv.datagrid('getRows').splice(Number(row.attr('datagrid-row-index')), 1, detail);
					 		ddv.datagrid('fixRowHeight', row.attr('datagrid-row-index'));
					 		$('#convertGrid').datagrid('fixRowHeight', index);
						}
					}}},
					{field:'delete', title:'<a '+(($('#statusHidden').val() == 0 || $('#statusHidden').val() == 1) ? 'style="display:none;"' : '')+' onclick="PromotionProgram.addNewProductConvertDetail('+index+')"><span style="cursor:pointer"><img title="Thêm mới nhóm" src="/resources/images/icon_add.png"/></span></a>', width:30,align:'center',sortable:false,resizable:false,formatter:function(v,r,idx){
						if(!VTUtilJS.isNullOrEmpty(r.id)) {
							return '<a '+(($('#statusHidden').val() == 0 || $('#statusHidden').val() == 1) ? 'style="display:none;"' : '')+' onclick="PromotionProgram.deleteProductConvertDetail('+idx+', '+r.id+', '+index+')"><span style="cursor:pointer"><img title="Xóa nhóm" src="/resources/images/icon_delete.png"/></span></a>';
						} else {
							return '<a '+(($('#statusHidden').val() == 0 || $('#statusHidden').val() == 1) ? 'style="display:none;"' : '')+' onclick="PromotionProgram.deleteProductConvertDetail('+idx+',\'\', '+index+')"><span style="cursor:pointer"><img title="Xóa nhóm" src="/resources/images/icon_delete.png"/></span></a>';							
						}
					}}
        		]],
        		onLoadSuccess:function(){
        		 	setTimeout(function(){
        		 		$('#convertGrid').datagrid('fixDetailRowHeight',index);
        		 	},500);
        		}
        	});
        },
		columns: [[
			{field:'name', title:"Nhóm", width:500,align:'left',sortable:false,resizable:false,formatter:CommonFormatter.formatNormalCell},
			{field:'delete', title:'<a '+(($('#statusHidden').val() == 0 || $('#statusHidden').val() == 1) ? 'style="display:none;"' : '')+' onclick="PromotionProgram.addNewProductConvert()"><span style="cursor:pointer"><img title="Thêm mới nhóm" src="/resources/images/icon_add.png"/></span></a>', width:20,align:'center',sortable:false,resizable:false,formatter:function(value,row,index){
				if(!VTUtilJS.isNullOrEmpty(row.id)) {
					return '<a '+(($('#statusHidden').val() == 0 || $('#statusHidden').val() == 1) ? 'style="display:none;"' : '')+' onclick="PromotionProgram.deleteProductConvert('+index+', '+row.id+')"><span style="cursor:pointer"><img title="Xóa nhóm" src="/resources/images/icon_delete.png"/></span></a>';
				} else {
					return '<a '+(($('#statusHidden').val() == 0 || $('#statusHidden').val() == 1) ? 'style="display:none;"' : '')+' onclick="PromotionProgram.deleteProductConvert('+index+')"><span style="cursor:pointer"><img title="Xóa nhóm" src="/resources/images/icon_delete.png"/></span></a>';					
				}
			}}
		]],
		onLoadSuccess: function(data) {
		}
	});
});
</script>
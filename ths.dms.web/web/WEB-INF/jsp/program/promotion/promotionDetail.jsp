<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<div class="BreadcrumbSection">
    <ul class="ResetList FixFloat BreadcrumbList">
	    <li class="Sprite1"><a href="/promotion/info">Chương trình khuyến mãi</a></li><li><span id="tabTitle">Thông tin CTKM</span></li>	
    </ul>
</div>
<div class="CtnOneColSection">
   <div class="ContentSection">
   		<div class="ToolBarSection">
           <div class="SearchSection GeneralSSection">
	           	<h2 class="Title2Style"><s:if test="proType == 1">Thông tin chương trình khuyến mãi</s:if><s:else>Thông tin chương trình HTTM</s:else></h2>
	           	<div class="SearchInSection SProduct1Form" id="update-form">
	           		<label class="LabelStyle Label1Style" id="labelPromotionCode">Mã CTKM<span class="ReqiureStyle">(*)</span></label>
	           		<s:if test="promotionId != null && promotionId > 0">
	           			<input style="width: 160px;" id="promotionCode" disabled="disabled" type="text" class="InputTextStyle InputText1Style" call-back="VTValidateUtils.getMessageOfRequireCheck('promotionCode', 'Mã CTKM', false, true);VTValidateUtils.getMessageOfSpecialCharactersValidate('promotionCode', 'Mã CTKM', Utils._CODE)" value="<s:property value="promotionProgram.promotionProgramCode"/>" maxlength="50"/>
	           		</s:if>
	           		<s:else>
						<input style="width: 160px;" id="promotionCode" type="text" class="InputTextStyle InputText1Style" call-back="VTValidateUtils.getMessageOfRequireCheck('promotionCode', 'Mã CTKM', false, true);VTValidateUtils.getMessageOfSpecialCharactersValidate('promotionCode', 'Mã CTKM', Utils._CODE)" value="<s:property value="promotionProgram.promotionProgramCode"/>" maxlength="50"/>	           		
	           		</s:else>
					<label class="LabelStyle Label1Style" style="width: 117px;" id="labelPromotionName">Tên CTKM<span class="ReqiureStyle">(*)</span></label>
					<input id="promotionName" style="width: 326px;" type="text" class="InputTextStyle InputText7Style" call-back="VTValidateUtils.getMessageOfRequireCheck('promotionName', 'Tên CTKM', false, true);VTValidateUtils.getMessageOfSpecialCharactersValidate('promotionName', 'Tên CTKM', Utils._NAME)" value="<s:property value="promotionProgram.promotionProgramName"/>" maxlength="100"/>
					<div class="Clear"></div>
					<label class="LabelStyle Label1Style">Từ ngày<span class="ReqiureStyle">(*)</span></label>
					<div id="startDatePickerDiv" style="float: left;">
						<s:if test="promotionProgram.status.getValue() == 1 || promotionProgram.status.getValue() == 0">
							<input id="startDate" disabled="disabled" type="text" class="InputTextStyle InputText6Style vinput-date" call-back="VTValidateUtils.getMessageOfEmptyDate('startDate', 'Từ ngày');VTValidateUtils.getMessageOfInvalidFormatDate('startDate', 'Từ ngày')" style="width:138px;" value="<s:date name="promotionProgram.fromDate" format="dd/MM/yyyy"/>"/>
						</s:if>
						<s:else>
							<input id="startDate" type="text" class="InputTextStyle InputText6Style vinput-date" call-back="VTValidateUtils.getMessageOfEmptyDate('startDate', 'Từ ngày');VTValidateUtils.getMessageOfInvalidFormatDate('startDate', 'Từ ngày')" style="width:138px;" value="<s:date name="promotionProgram.fromDate" format="dd/MM/yyyy"/>"/>
						</s:else>
					</div>
					<div id="endDatePickerDiv">
						<label style="padding-right: 17px;" class="LabelStyle Label1Style" id="endDateClass">Đến ngày</label>
						<input id="endDate" style="width:138px;" type="text" class="InputTextStyle InputText6Style vinput-date" call-back="VTValidateUtils.getMessageOfInvalidFormatDate('endDate', 'Đến ngày');VTValidateUtils.getMessageCheckToDate('startDate', 'Từ ngày', 'endDate', 'Đến ngày');VTValidateUtils.getMessageCheckCurrentDate('endDate', 'Đến ngày')" value="<s:date name="promotionProgram.toDate" format="dd/MM/yyyy"/>"/>
					</div>
					<label class="LabelStyle Label6Style">Trạng thái<span class="ReqiureStyle">(*)</span></label>
					<div class="BoxSelect BoxSelect2" id="comboStatus">
						<s:if test="promotionProgram.status.getValue() == 2">
							<select id="status" class="MySelectBoxClass">
								<option value="1">Hoạt động</option>
								<option value="2" selected="selected">Dự thảo</option>
								<option value="0">Tạm ngưng</option>
							</select>
						</s:if>
						<s:elseif test="promotionProgram.status.getValue() == 1">
							<select id="status" class="MySelectBoxClass">
								<option value="1" selected="selected">Hoạt động</option>
								<option value="0">Tạm ngưng</option>
							</select>
						</s:elseif>
						<s:elseif test="promotionProgram.status.getValue() == 0">
							<select id="status" class="MySelectBoxClass">
								<option value="0" selected="selected">Tạm ngưng</option>
							</select>
						</s:elseif>
						<s:else>
							<select id="status" class="MySelectBoxClass">
								<option value="2" selected="selected">Dự thảo</option>
							</select>
						</s:else>
					</div>
					<div class="Clear"></div>
					<label class="LabelStyle Label1Style">Loại CTKM<span class="ReqiureStyle">(*)</span></label>
					<s:if test="promotionId != null && promotionId > 0">
						<input style="width: 160px;" id="typeCodeTmp" type="text" class="InputTextStyle InputText1Style" value="<s:property value="promotionProgram.type"/>-<s:property value="promotionProgram.proFormat"/>" maxlength="100" disabled="disabled"/>
						<input id="typeCode" type="hidden" value="<s:property value="promotionProgram.type"/>"/>
					</s:if>
					<s:else>
						<div class="BoxSelect BoxSelect2">
						   	<select id="typeCode">
								<option value="-1" selected="selected">---Chọn Loại CTKM---</option>
					       		<s:iterator value="lstTypeCode">
					       			<option value="<s:property value="apParamCode"/>"><s:property value="apParamCode"/>-<s:property value="value"/></option>
					       		</s:iterator>
					       	</select>
						</div>
					</s:else>
					<label class="LabelStyle Label1Style" style="width:100px;">&nbsp;&nbsp;&nbsp;&nbsp;</label>
					<s:if test="promotionProgram.status.getValue() == 1 || promotionProgram.status.getValue() == 0">
						<s:if test="promotionProgram.isEdited == 1">
							<input disabled="disabled" type="checkbox" id="canEdit" checked="checked" class="InputCbxStyle"/><label class="LabelStyle">Được sửa số lượng khuyến mại</label>
						</s:if>
						<s:else>
							<input disabled="disabled" type="checkbox" id="canEdit" class="InputCbxStyle"/><label class="LabelStyle">Được sửa số lượng khuyến mại</label>
						</s:else>
					</s:if>
					<s:else>
						<s:if test="promotionProgram.isEdited == 1">
							<input type="checkbox" id="canEdit" checked="checked" class="InputCbxStyle"/><label class="LabelStyle">Được sửa số lượng khuyến mại</label>						
						</s:if>
						<s:else>
							<input type="checkbox" id="canEdit" class="InputCbxStyle"/><label class="LabelStyle">Được sửa số lượng khuyến mại</label>
						</s:else>
					</s:else>
					<div class="Clear"></div>
					<label class="LabelStyle Label1Style">Mô tả<span class="ReqiureStyle">(*)</span></label>
					<input id="description" style="width: 636px;" type="text" class="InputTextStyle InputText1Style" value="<s:property value="promotionProgram.description"/>" maxlength="1024"/>
					<div class="Clear"></div>
					<div class="BtnCenterSection">
						<button id="btnSave" class="BtnGeneralStyle Sprite2" onclick="return PromotionProgram.update();"><span class="Sprite2">Cập nhật</span></button>
						<button id="btnCopy" class="BtnGeneralStyle Sprite2" onclick="return PromotionProgram.openCopyPromotionProgram();"><span class="Sprite2">Sao chép</span></button>
					</div>
					<div class="Clear"></div>
					<p class="ErrorMsgStyle SpriteErr" id="errorMsg" style="display: none;"></p>
					<p id="successMsg" class="SuccessMsgStyle" style="display: none"></p>
	           	</div>
	           	<s:if test="promotionProgram != null && promotionProgram.id != null && promotionProgram.id > 0">
					<div id="tabSectionDiv" class="TabSection">
						<ul class="ResetList TabSectionList">
							<li><a id="groupTab" href="javascript:void(0);" onclick="PromotionProgram.showTab('groupTab', '/promotion/detail-group-product');" class="Sprite1 Active"><span class="Sprite1">Nhóm sản phẩm</span></a></li>
					        <li><a id="shopTab" href="javascript:void(0);" onclick="PromotionProgram.showTab('shopTab', '/promotion/detail-shop');" class="Sprite1"><span class="Sprite1">Đơn vị tham gia</span></a></li>
					        <li><a id="attributeTab" href="javascript:void(0);" onclick="PromotionProgram.showTab('attributeTab', '/promotion/customer-attribute');" class="Sprite1"><span class="Sprite1">Thuộc tính khách hàng</span></a></li>
					        <li><a id="staffTab" href="javascript:void(0);" onclick="PromotionProgram.showTab('staffTab', '/promotion/saler-quantity');" class="Sprite1"><span class="Sprite1">Số suất NVBH</span></a></li>
					    </ul>
					    <div class="Clear"></div>
					</div>	           	
	           	</s:if>
           </div>
      	</div>
      	<div id="divTab" class="SearchSection GeneralSSection">
        </div>	
   </div>
</div>
<div id="masterDataArea">
	<input type="hidden" id="id" value='<s:property value="promotionProgram.id"/>'>
	<input type="hidden" id="promotionStatus" value="<s:property value='promotionProgram.status.value'/>" />
	<input type="hidden" id="promotionType" value="<s:property value="promotionProgram.type"/>">
	<input type="hidden" id="statusHidden" value="<s:property value="promotionProgram.status.getValue()"/>">
	<input type="hidden" id="isVNMAdmin" value="<s:property value='isVNMAdmin' />" />
</div>
<script type="text/javascript">
$(document).ready(function(){
	PromotionProgram._listProduct = new Map();
	<s:iterator value="listProduct">
		var productCode = '<s:property value="productCode"/>';
		var productName = '<s:property value="productName"/>';
		var obj = new Object();
		obj.productCode = productCode;
		obj.productName = productName;
		PromotionProgram._listProduct.put(obj.productCode, obj);
	</s:iterator>
	$.fn.focusEnd = function(){
        this.focus();
        var $thisVal = VTUtilJS.returnMoneyValue(this.val());
        if(this.hasClass('vinput-money')) {
        	this.val('').val(VTUtilJS.formatCurrency($thisVal));
        } else {
        	this.val('').val($thisVal);        	
        }
        return this;
    };
    var oldCheckbox = $.fn.datagrid.defaults.editors.checkbox;
	$.extend($.fn.datagrid.defaults.editors, {
		checkbox: {
			init: function(container, options){
				var __checkbox = oldCheckbox.init(container, options);
				if(!VTUtilJS.isNullOrEmpty(options) && options.isDisable) {
					__checkbox.attr('disabled' , 'disabled');
				}
				return __checkbox;
			},
			destroy: function(target){
				$(target).remove();
        	},
        	getValue: function(target){
        		return oldCheckbox.getValue(target);
        	},
        	setValue: function(target, value){
        		return oldCheckbox.setValue(target, value);
        	},
        	resize: function(target, width){
        		$(target)._outerWidth(width);
        	}
		},
        currency: {
        	init: function(container, options){
        		var disableText = (!VTUtilJS.isNullOrEmpty(options) && options.isDisable) ? 'disabled="disabled"' : '';
        		var maxLength = (!VTUtilJS.isNullOrEmpty(options) && !VTUtilJS.isNullOrEmpty(options.maxLength)) ? 'maxlength="'+options.maxLength+'"' : '';
       			var input = $('<input type="text" '+disableText+' '+maxLength+' class="datagrid-currency-input vinput-money">').appendTo(container);
       			var target = input;
       			var reg = /[^0-9]/;
           		var regAll = /[^0-9]/g;
           		$(target).bind('keyup', options, function(e) {
           			var options = e.data;
           			if(!VTUtilJS.isNullOrEmpty(options) && !VTUtilJS.isNullOrEmpty(options.blur)) {
           				var func = options.blur;
           				func.call(this, target);
           			}
           		});
           		$(target).bind('keyup', function(e) {
           			var valMoneyInput = $(target).val();
           			valMoneyInput = VTUtilJS.returnMoneyValue(valMoneyInput);
           			if(isNaN(valMoneyInput) || valMoneyInput == "" || valMoneyInput == '') {
           				return false;
           			}
           			var _valMoneyInput = VTUtilJS.formatCurrency(valMoneyInput);
           			$(target).val(_valMoneyInput);
           		});
           		$(target).bind('paste', function(e) {
           			var tm = setTimeout(function() {
           				var valMoneyInput = $(target).val();
               			valMoneyInput = VTUtilJS.returnMoneyValue(valMoneyInput);
               			if(isNaN(valMoneyInput) || valMoneyInput == "" || valMoneyInput == '') {
               				return false;
               			}
               			var _valMoneyInput = VTUtilJS.formatCurrency(valMoneyInput);
               			$(target).val(_valMoneyInput);
               			clearTimeout(tm);
           			}, 200);
           		});
           		$(target).bind('keyup', function(e){
           			var code;
           			if (!e) var e = window.event;
           			if (e.keyCode) code = e.keyCode;
           			else if (e.which) code = e.which;
           			if(code == keyCodes.CTRL){
           				Utils._CTRL_PRESS = false;
           			}
           			if(code == keyCodes.SHIFT) {
           				Utils._SHIFT_PRESS = false;
           			}
           		});
           		$(target).bind('keydown', function(e){
           			var code;
           			if (!e) var e = window.event;
           			if (e.keyCode) code = e.keyCode;
           			else if (e.which) code = e.which;
           			var character = fromKeyCode(code).split(' ')[0];
           			if ((!Utils._CTRL_PRESS && !Utils._SHIFT_PRESS && code >=96 && code <= 105) || (!Utils._CTRL_PRESS && !Utils._SHIFT_PRESS && code>=48 && code<=57) || code==null || code==0 || code== keyCodes.BACK_SPACE || 
           					code == keyCodes.TAB || code==keyCodes.ENTER || code==keyCodes.ESCAPE || code == keyCodes.DELETE ||
           					(Utils._SHIFT_PRESS && code == keyCodes.HOME) || code == keyCodes.SHIFT || code == keyCodes.HOME || code == keyCodes.END ||
           					code==keyCodes.CTRL || code == keyCodes.ARROW_LEFT || code == keyCodes.ARROW_RIGHT || code == keyCodes.ARROW_UP || code == keyCodes.ARROW_DOWN ||
           					(Utils._CTRL_PRESS && (character  == 'v' || character  == 'V'))){
           				if(code == keyCodes.CTRL){
           					Utils._CTRL_PRESS = true;
           				}
           				if(code == keyCodes.SHIFT) {
           					Utils._SHIFT_PRESS = true;
           				}
           				return true;
           			} else if (reg.test(character) || (Utils._SHIFT_PRESS && !/[^0-9]/.test(character))) {
           				return false;
           			}else{
           				return true;
           			}
           		});
           		$(target).bind('paste', function(){			
           			var tmAZ = setTimeout(function(){
           				$(target).val($(target).val().replace(regAll,''));
           				clearTimeout(tmAZ);
           			},200);
           		});
       			return input;
        	},
        	destroy: function(target){
        		$(target).remove();
        	},
        	getValue: function(target){
        		return VTUtilJS.returnMoneyValue($(target).val());
        	},
        	setValue: function(target, value){
        		if(value == 0 || value == null || VTUtilJS.isNullOrEmpty(value)) {
        			$(target).val('');
        		}else {
        			$(target).val(VTUtilJS.formatCurrency(value));        			
        		}
        	},
        	resize: function(target, width){
        		$(target)._outerWidth(width);
        	}
        },
        percent: {
        	init: function(container, options){
        		var disableText = (!VTUtilJS.isNullOrEmpty(options) && options.isDisable) ? 'disabled="disabled"' : '';
        		var maxLength = (!VTUtilJS.isNullOrEmpty(options) && !VTUtilJS.isNullOrEmpty(options.maxLength)) ? 'maxlength="'+options.maxLength+'"' : '';
       			var input = $('<input type="text" '+disableText+' '+maxLength+' class="datagrid-currency-input vinput-money">').appendTo(container);
       			var target = input;
       			reg = /[^0-9.]/;
    			regAll = /[^0-9.]/g; 
           		$(target).bind('keyup', options, function(e) {
           			var options = e.data;
           			if(!VTUtilJS.isNullOrEmpty(options) && !VTUtilJS.isNullOrEmpty(options.blur)) {
           				var func = options.blur;
           				func.call(this, target);
           			}
           		});
           		$(target).bind('keyup', function(e) {
           			var valMoneyInput = $(target).val();
           			if(valMoneyInput.length > 3) {
           				return false;
           			}
           			valMoneyInput = VTUtilJS.returnMoneyValue(valMoneyInput);
           			if(isNaN(valMoneyInput) || valMoneyInput == "" || valMoneyInput == '') {
           				return false;
           			}
           			var _valMoneyInput = VTUtilJS.formatCurrency(valMoneyInput);
           			$(target).val(_valMoneyInput);
           		});
           		$(target).bind('paste', function(e) {
           			var tm = setTimeout(function() {
           				var valMoneyInput = $(target).val();
           				if(valMoneyInput.length > 3) {
               				return false;
               			}
               			valMoneyInput = VTUtilJS.returnMoneyValue(valMoneyInput);
               			if(isNaN(valMoneyInput) || valMoneyInput == "" || valMoneyInput == '') {
               				return false;
               			}
               			var _valMoneyInput = VTUtilJS.formatCurrency(valMoneyInput);
               			$(target).val(_valMoneyInput);
               			clearTimeout(tm);
           			}, 200);
           		});
           		$(target).bind('keyup', function(e){
           			var code;
           			if (!e) var e = window.event;
           			if (e.keyCode) code = e.keyCode;
           			else if (e.which) code = e.which;
           			if(code == keyCodes.CTRL){
           				Utils._CTRL_PRESS = false;
           			}
           			if(code == keyCodes.SHIFT) {
           				Utils._SHIFT_PRESS = false;
           			}
           		});
           		$(target).bind('keydown', function(e){
           			var code;
           			if (!e) var e = window.event;
           			if (e.keyCode) code = e.keyCode;
           			else if (e.which) code = e.which;
           			var character = fromKeyCode(code).split(' ')[0];			
           			if ((!Utils._CTRL_PRESS && !Utils._SHIFT_PRESS && code >=96 && code <= 105) || (!Utils._CTRL_PRESS && !Utils._SHIFT_PRESS && code>=48 && code<=57) || code==null || code==0 || code== keyCodes.BACK_SPACE || 
           					code == keyCodes.TAB || code==keyCodes.ENTER || code==keyCodes.ESCAPE || code == keyCodes.DELETE ||
           					(Utils._SHIFT_PRESS && code == keyCodes.HOME) || code == keyCodes.SHIFT || code == keyCodes.HOME || code == keyCodes.END ||
           					code==keyCodes.CTRL || code == keyCodes.ARROW_LEFT || code == keyCodes.ARROW_RIGHT || code == keyCodes.ARROW_UP || code == keyCodes.ARROW_DOWN ||
           					(Utils._CTRL_PRESS && (character  == 'v' || character  == 'V'))){
           				if(code == keyCodes.CTRL){
           					Utils._CTRL_PRESS = true;
           				}
           				if(code == keyCodes.SHIFT) {
           					Utils._SHIFT_PRESS = true;
           				}
           				return true;
           			} else if (reg.test(character) || (Utils._SHIFT_PRESS && !/[^0-9]/.test(character))) {
           				return false;
           			}else{
           				return true;
           			}
           		});
           		$(target).bind('paste', function(){			
           			var tmAZ = setTimeout(function(){
           				$(target).val($(target).val().replace(regAll,''));
           				clearTimeout(tmAZ);
           			},200);
           		});
       			return input;
        	},
        	destroy: function(target){
        		$(target).remove();
        	},
        	getValue: function(target){
        		return VTUtilJS.returnMoneyValue($(target).val());
        	},
        	setValue: function(target, value){
        		if(value == 0 || value == null || VTUtilJS.isNullOrEmpty(value)) {
        			$(target).val('');
        		}else {
        			$(target).val(VTUtilJS.formatCurrency(value));        			
        		}
        	},
        	resize: function(target, width){
        		$(target)._outerWidth(width);
        	}
        }
    });
	var idt = '<s:property value="promotionProgram.id"/>';
	if (idt.length == 0 || Number(idt) <= 0) {
		$('#typeCode').dropdownchecklist({ 
			emptyText:'Tất cả',
			//firstItemChecksAll: true,
			maxDropHeight: 250,
			onItemClick:function(checkbox, selector){
			},
			textFormatFunction: function(options) {
		        var selectedOptions = options.filter(":selected");
			    var text = '';
		        if(selectedOptions.size() > 0 && parseInt(selectedOptions[0].value) == -1){
		        	text = 'Tất cả';
		        }else{
			        for(var i = 0;i<selectedOptions.size();i++){
				        if(text.length > 0){
				        	text += ',';
				        }
				        text += selectedOptions[i].text.split('-')[0];
			        }
		        }
		        return text;
			}
		});
	} else {
		VTUtilJS.getFormHtml('masterDataArea', '/promotion/detail-group-product', function(html) {$('#divTab').html(html);});
	}
});
</script>
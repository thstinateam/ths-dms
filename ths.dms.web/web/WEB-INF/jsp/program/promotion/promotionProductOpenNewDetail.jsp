<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<h2 class="Title2Style">Thông tin sản phẩm quy đổi</h2>
<div class="SearchInSection SProduct1Form">
	<div class="GridSection" id="promotionOpenGrid">
		<table id="openGrid"></table> 
	</div>
	<div class="Clear"></div>
	<s:if test="promotionProgram.getStatus().getValue() == 2">
		<div class="BtnCenterSection">
			<button class="BtnGeneralStyle Sprite2" onclick="PromotionProgram.saveProductOpenNew();"><span class="Sprite2">Cập nhập</span></button>
		</div>
	</s:if>
	<p id="errMsgOpen" class="ErrorMsgStyle" style="display: none"></p>
	<p id="successMsgOpen" class="SuccessMsgStyle" style="display: none;"></p>
</div>
<script type="text/javascript">
$(document).ready(function(){
	$('#openGrid').datagrid({
		url:'/promotion/product-open-new-load',
		queryParams: {promotionId : $('#id').val()},
		fitColumns:true,
		singleSelect:true,
		rownumbers:true,
		scrollbarSize:0,
		width: $('#promotionOpenGrid').width(),
		height:'auto',
		onDblClickRow : function(idx,r) {
			var __rows = $('#openGrid').datagrid('getRows');
			if(__rows != null && $.isArray(__rows)) {
				
			}
			$('#openGrid').datagrid('beginEdit', idx);
		},
		columns:[[
			{field:'productCode', title:'Mã sản phẩm',width:200, align:'left', sortable:false, resizable:false, formatter:CommonFormatter.formatNormalCell, editor: {
				type:'combobox',
				 options:{
				 	valueField : 'productCode',
				 	textField : 'productCode',
				 	data : PromotionProgram._listProduct.valArray,
				 	formatter:function(row){
                        return Utils.XSSEncode(row.productCode + ' - ' +row.productName);
                    },
				 	onSelect: function(r) {
				 		var row = $(this).parent().parent().parent().parent().parent().parent().parent();
				 		row.find('td[field=productName] div').html(r.productName);
				 		var detail = $('#openGrid').datagrid('getRows')[Number(row.attr('datagrid-row-index'))];
				 		detail.productCode = r.productCode;
				 		detail.productName = r.productName;
				 		$('#openGrid').datagrid('getRows').splice(Number(row.attr('datagrid-row-index')), 1, detail);
				 	}
				 }
			}},
		 	{field:'productName', title:'Tên sản phẩm',width:350, align:'left', sortable:false, resizable:false, formatter:CommonFormatter.formatNormalCell},
		 	{field:'quantity', title:'Số lượng', width:50, align:'center', sortable:false, resizable:false, formatter:CommonFormatter.formatNormalCell, editor: {
				type:'currency', 
				options : {
					maxLength:12,
					blur : function(target) {
						var row = $(target).parent().parent().parent().parent().parent().parent().parent();
				 		var quantity = $(target).val();
				 		var detail = $('#openGrid').datagrid('getRows')[Number(row.attr('datagrid-row-index'))];
				 		detail.quantity = quantity;
				 		$('#openGrid').datagrid('getRows').splice(Number(row.attr('datagrid-row-index')), 1, detail);
					}
				}
			}},
			{field:'amount', title:'Số tiền', width:50, align:'center', sortable:false, resizable:false, formatter:CommonFormatter.formatNormalCell, editor: {
				type:'currency', 
				options : {
					maxLength:24,
					blur : function(target) {
						var row = $(target).parent().parent().parent().parent().parent().parent().parent();
				 		var amount = $(target).val();
				 		var detail = $('#openGrid').datagrid('getRows')[Number(row.attr('datagrid-row-index'))];
				 		detail.amount = amount;
				 		$('#openGrid').datagrid('getRows').splice(Number(row.attr('datagrid-row-index')), 1, detail);
					}
				}
			}},
			{field:'delete', title:'<a '+(($('#statusHidden').val() == 0 || $('#statusHidden').val() == 1) ? 'style="display:none;"' : '')+' onclick="PromotionProgram.addProductOpenNew()"><span style="cursor:pointer"><img title="Thêm mới sản phẩm" src="/resources/images/icon_add.png"/></span></a>', width:30,align:'center',sortable:false,resizable:false,formatter:function(v,r,idx){
				if(!VTUtilJS.isNullOrEmpty(r.id)) {
					return '<a '+(($('#statusHidden').val() == 0 || $('#statusHidden').val() == 1) ? 'style="display:none;"' : '')+' onclick="PromotionProgram.deleteProductOpenNew('+idx+', '+r.id+')"><span style="cursor:pointer"><img title="Xóa sản phẩm" src="/resources/images/icon_delete.png"/></span></a>';
				} else {
					return '<a '+(($('#statusHidden').val() == 0 || $('#statusHidden').val() == 1) ? 'style="display:none;"' : '')+' onclick="PromotionProgram.deleteProductOpenNew('+idx+')"><span style="cursor:pointer"><img title="Xóa sản phẩm" src="/resources/images/icon_delete.png"/></span></a>';							
				}
			}}
		]],
		onLoadSuccess:function(){
		}
	});
});
</script>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>

<style type="text/css">
.qttClass {	text-align:right;
	border:1px solid #c4c4c4;
	height:20px;
	line-height:20px;
	padding:0 4px;
	width:110px;
}
</style>

<div id="boxSearch2">
	<h2 class="Title2Style">Danh sách nhân viên bán hàng tham gia CTKM</h2>
	<div class="SearchInSection SProduct1Form">
		<div class="GridSection" id="promotionStaffGrid">
			<table id="exGrid"></table> 
		</div>
		<div class="Clear"></div>
		<p id="errMsgShop" class="ErrorMsgStyle" style="display: none"></p>
		<p id="successMsgShop" class="SuccessMsgStyle" style="display: none;"></p>
	</div>
</div>

<div class="SearchInSection SProduct1Form">
	<div class="GeneralForm GeneralNoTP1Form">
		<div class="Func1Section cmsiscontrol" id="tab_nv_gr_change_import_staff">
			<p class="DownloadSFileStyle DownloadSFile2Style"><a id="downloadTemplateLevel" href="/resources/templates/catalog/Bieu_mau_danh_muc_CTKM_NVBH.xls" class="Sprite1">Tải file excel mẫu</a></p>
			<div class="DivInputFile">
				<form action="/promotion/import-promotion-staff" name="importFrm" id="importFrm"  method="post" enctype="multipart/form-data">
					<input type="hidden" name="token" id="tokenImport" value='<s:property value="token"/>'>										
			    	<input type="file" class="InputFileStyle" size="10" style="width:170px;" readonly="readonly" name="excelFile" id="excelFile" onchange="previewImportExcelFile(this,'importFrm');">
			    	<input type="hidden" id="promotionIdImport" name="promotionId" value="<s:property value="promotionId" />" />
					<div class="FakeInputFile">
						<input id="fakefilepc" type="text" class="InputTextStyle">
					</div>
				</form>
			</div>
			<button id="btnImport" class="BtnGeneralStyle" onclick="PromotionProgram.importPromotionStaff();">Nhập từ Excel</button>
		</div>
		<button id="btnExport" class="BtnGeneralStyle" onclick="PromotionProgram.exportPromotionStaff();">Xuất Excel</button>
	<div class="Clear"></div>
	</div>
	<div class="Clear"></div>
	<p id="errExcelMsgShop" class="ErrorMsgStyle" style="display: none;margin-top:10px;margin-left: 10px;padding: 2px 0 5px 9px;"></p>
</div>


<%-- script --%>
<script type="text/javascript">
$(document).ready(function(){
	var status = $("#masterDataArea #promotionStatus").val().trim();
	var pms = "true";
	
	var vtitle = "";
	if (status == 1) {
		vtitle = "<a href='javascript:void(0);' onclick='PromotionProgram.showSalerDlg();'><img title='Thêm mới' width='17' height='17' src='/resources/images/icon-add.png'></a>";
	}
	
	$("#exGrid").treegrid({
		url: "/promotion/search-saler-of-promotion",
		queryParams: {promotionId:$("#masterDataArea #id").val().trim()},
		rownumbers: false,
		width: $("#promotionStaffGrid").width() - 10,
		height: "auto",
		fitColumns: true,
		idField: "nodeId",
		treeField: "text",
		scrollbarSize:0,
		columns: [[
			{field:"no", title:"STT", sortable: false, resizable: false, width:45, fixed:true, align:"center", formatter:function(v, r) {
				return "";
			}},
			{field:"text", title:"Nhân viên bán hàng", sortable: false, resizable: false, width: 250, align:"left", formatter:CommonFormatter.formatNormalCell},
			{field:"quantity", title:"Số suất KM", sortable: false, resizable: false, width: 90, fixed:true, align:"right", formatter: function(v, r) {
				var val = "";
				v = r.attr.quantity;
				if (v != undefined && v != null) {
					val = formatCurrency(v);
				}
				if (status == 0) {
					return val;
				}
				return "<span id='qtt-"+r.nodeId+"'>"+val+"</span>" + "<input type='text' id='txt"+r.nodeId+"' value='"+val+"' style='display:none;; width:75px;' class='qttClass vinput-money' maxlength='11' />";
			}},
			{field:"receivedQtt", title:"Số suất đã KM", sortable: false, resizable: false, width: 105, fixed:true, align:"right", formatter: function(v, r) {
				var val = "0";
				v = r.attr.receivedQtt;
				if (v != undefined && v != null) {
					val = formatCurrency(v);
				}
				return val;
			}},
			{field:"amountMax", title:"Số tiền KM", sortable: false, resizable: false, width: 90, fixed:true, align:"right", formatter: function(v, r) {
				var val = "";
				v = r.attr.amountMax;
				if (v != undefined && v != null) {
					val = formatCurrency(v);
				}
				if (status == 0) {
					return val;
				}
				return "<span id='amt-"+r.nodeId+"'>"+val+"</span>" + "<input type='text' id='txtamt"+r.nodeId+"' value='"+val+"' style='display:none;; width:75px;' class='qttClass vinput-money' maxlength='11' />";
			}},
			{field:"receivedAmt", title:"Số tiền đã KM", sortable: false, resizable: false, width: 105, fixed:true, align:"right", formatter: function(v, r) {
				var val = "0";
				v = r.attr.receivedAmt;
				if (v != undefined && v != null) {
					val = formatCurrency(v);
				}
				return val;
			}},
			{field:"numMax", title:"Số lượng KM", sortable: false, resizable: false, width: 90, fixed:true, align:"right", formatter: function(v, r) {
				var val = "";
				v = r.attr.numMax;
				if (v != undefined && v != null) {
					val = formatCurrency(v);
				}
				if (status == 0) {
					return val;
				}
				return "<span id='num-"+r.nodeId+"'>"+val+"</span>" + "<input type='text' id='txtnum"+r.nodeId+"' value='"+val+"' style='display:none;; width:75px;' class='qttClass vinput-money' maxlength='11' />";
			}},
			{field:"receivedNum", title:"Số lượng đã KM", sortable: false, resizable: false, width: 105, fixed:true, align:"right", formatter: function(v, r) {
				var val = "0";
				v = r.attr.receivedNum;
				if (v != undefined && v != null) {
					val = formatCurrency(v);
				}
				return val;
			}},
			{field:"edit", title:vtitle, sortable: false, resizable: false, width: 30, fixed:true, align:"center", formatter: function(v, r) {
				if (r.attr.isSaler == 1) {
	        		if (status == 1 || status == 2) {
	        			return '<a href="javascript:void(0);" id="link'+r.nodeId+'" onclick="edit(\''+r.nodeId+'\','+r.attr.mapId+','+r.attr.quantity+', '+r.attr.amountMax+', '+r.attr.numMax+');"><img title="Sửa" width="17" height="17" src="/resources/images/icon-edit.png"></a>';
	        		}
	        	} else if (status == 2) {
        			return '<a href="javascript:void(0);" onclick="PromotionProgram.showSalerDlg('+r.attr.id+');"><img title="Thêm mới" width="17" height="17" src="/resources/images/icon-add.png"></a>';
        		}
			}},
			{field:"delete", title:"", sortable: false, resizable: false, width: 30, fixed:true, align:"center", formatter: function(v, r) {
				if(r.attr.isSaler == 1 && (status == 1|| status == 2)){
        			return '<a href="javascript:void(0);" id="linkesc'+r.nodeId+'" onclick="PromotionProgram.deleteStaffMap(\''+r.nodeId+'\');"><img title="Xóa" width="17" height="17" src="/resources/images/icon-delete.png"></a>';
        		}
				return '<a href="javascript:void(0);" id="linkesc'+r.nodeId+'"></a>';
			}}
		]],
		onLoadSuccess: function(data) {
			var i = 1;
			$("#promotionStaffGrid .datagrid-btable td[field=no] div").each(function() {
				$(this).html(i); // danh STT
				i++;
			});
			$('.vinput-money').each(function() {
				VTUtilJS.bindFormatOnTextfield($(this), VTUtilJS._TF_NUMBER_COMMA);
				VTUtilJS.formatCurrencyFor($(this).prop('id'));
			});
			$('#promotionStaffGrid .qttClass').each(function() {
	      		VTUtilJS.formatCurrencyFor($(this).attr('id'));
	      	});
			$('#promotionStaffGrid .amtClass').each(function() {
	      		VTUtilJS.formatCurrencyFor($(this).attr('id'));
	      	});
			$('#promotionStaffGrid .numClass').each(function() {
	      		VTUtilJS.formatCurrencyFor($(this).attr('id'));
	      	});
			var arrEdit =  $('#promotionStaffGrid td[field="edit"]');
			if (arrEdit != undefined && arrEdit != null && arrEdit.length > 0) {
			  for (var i = 0, size = arrEdit.length; i < size; i++) {
			  	$(arrEdit[i]).prop("id", "tab_nv_gr_change_gr_staff_td_edit_" + i);//Khai bao id danh cho phan quyen
				$(arrEdit[i]).addClass("cmsiscontrol");
			  }
			}
			var arrDelete =  $('#promotionStaffGrid td[field="delete"]');
			if (arrDelete != undefined && arrDelete != null && arrDelete.length > 0) {
			  for (var i = 0, size = arrDelete.length; i < size; i++) {
			    $(arrDelete[i]).prop("id", "tab_nv_gr_change_gr_staff_td_del_" + i);//Khai bao id danh cho phan quyen
				$(arrDelete[i]).addClass("cmsiscontrol");
			  }
			}
			Utils.functionAccessFillControl('promotionStaffGrid', function(data){
				//Xu ly cac su kien lien quan den cac control phan quyen
				var arrTmpLength =  $('#promotionStaffGrid td[id^="tab_nv_gr_change_gr_staff_td_del_"]').length;
				var invisibleLenght = $('.isCMSInvisible[id^="tab_nv_gr_change_gr_staff_td_del_"]').length;
				if (arrTmpLength > 0 && arrTmpLength != invisibleLenght) {
					$('#exGrid').datagrid("showColumn", "delete");
				} else {
					$('#exGrid').datagrid("hideColumn", "delete");
				}
				arrTmpLength =  $('#promotionStaffGrid td[id^="tab_nv_gr_change_gr_staff_td_edit_"]').length;
				invisibleLenght = $('.isCMSInvisible[id^="tab_nv_gr_change_gr_staff_td_edit_"]').length;
				if (arrTmpLength > 0 && arrTmpLength != invisibleLenght) {
					$('#exGrid').datagrid("showColumn", "edit");
				} else {
					$('#exGrid').datagrid("hideColumn", "edit");
				}
			});
		}
	});
	
	$("#shopCode").parent().unbind("keyup");
	$("#shopCode").parent().bind("keyup", function(event) {
		if (event.keyCode == keyCodes.ENTER) {
			$("#btnSearch").click();
		}
	});
});

function edit(nodeId, mapId, qtt, amt, num) {
	$('.ErrorMsgStyle').hide();
	$("#qtt-"+nodeId).hide();
	$("#amt-"+nodeId).hide();
	$("#num-"+nodeId).hide();
	$('#txt'+nodeId).show();
	$('#txtamt'+nodeId).show();
	$('#txtnum'+nodeId).show();
	Utils.bindFormatOnTextfieldEx1('txt'+nodeId,Utils._TF_NUMBER,null,'#errMsgShop','Số suất phải là số nguyên dương.');
	Utils.bindFormatOnTextfieldEx1('txtamt'+nodeId,Utils._TF_NUMBER,null,'#errMsgShop','Số tiền phải là số nguyên dương.');
	Utils.bindFormatOnTextfieldEx1('txtnum'+nodeId,Utils._TF_NUMBER,null,'#errMsgShop','Số lượng phải là số nguyên dương.');
	
	$('#txt'+nodeId).val(formatCurrency(qtt));
	$('#txtamt'+nodeId).val(formatCurrency(amt));
	$('#txtnum'+nodeId).val(formatCurrency(num));
	
	$('#link'+nodeId).attr('onclick','PromotionProgram.updateSalerQuantity(\''+nodeId+'\','+mapId+');');
	$('#link'+nodeId).html('<img width="17" height="17" src="/resources/images/icon-save.png" title="Lưu"/>');
	$('#linkesc'+nodeId).attr('onclick','esc(\''+nodeId+'\','+mapId+','+qtt+','+amt+','+num+');');
	$('#linkesc'+nodeId).html('<img width="17" height="17" title="Quay lại" src="/resources/images/icon_esc.png"/>');
}

function esc(nodeId,mapId,qtt,amt,num){
	$('.ErrorMsgStyle').hide();
	$('#txt'+nodeId).hide();
	$('#txtamt'+nodeId).hide();
	$('#txtnum'+nodeId).hide();
	$("#qtt-"+nodeId).show();
	$("#amt-"+nodeId).show();
	$("#num-"+nodeId).show();
   //$('#txt'+nodeId).val(formatCurrency(qtt));

   $('#link'+nodeId).attr('onclick','edit(\''+nodeId+'\','+mapId+','+qtt+','+amt+','+num+');');
   $('#link'+nodeId).html('<img width="17" height="17" src="/resources/images/icon-edit.png" title="Sửa"/>');
   var status = $("#masterDataArea #promotionStatus").val().trim();
   if (status == 2) {
	   $('#linkesc'+nodeId).attr('onclick','PromotionProgram.deleteStaffMap(\''+nodeId+'\');');
	   $('#linkesc'+nodeId).html('<img width="17" height="17" title="Xem chi tiết" src="/resources/images/icon-delete.png"/>');
   } else {
	   $('#linkesc'+nodeId).attr('onclick','');
	   $('#linkesc'+nodeId).html('');
   }

   $('#txt'+nodeId).unbind('change');
   $('#txtamt'+nodeId).unbind('change');
   $('#txtnum'+nodeId).unbind('change');
}

function _formatCurrencyFor(idInput) {
	$('#'+idInput).bind('keyup', function(e) {
		var valMoneyInput = $('#'+idInput).val();
		valMoneyInput = Utils.returnMoneyValue(valMoneyInput);
		if(isNaN(valMoneyInput) || valMoneyInput.length == 0) {
			$('#'+idInput).val("");
		} else {
			var _valMoneyInput = formatCurrency(valMoneyInput);
			$('#'+idInput).val(_valMoneyInput);
		}
	});
	$('#'+idInput).bind('paste', function(){
	    var tm = setTimeout(function(){
	    	var valMoneyInput = $('#'+idInput).val();
	        valMoneyInput = Utils.returnMoneyValue(valMoneyInput);
	        if(isNaN(valMoneyInput) || valMoneyInput.length == null) {
	        	$('#'+idInput).val("");
	        } else {
	        	var _valMoneyInput = formatCurrency(valMoneyInput);
	        	$('#'+idInput).val(_valMoneyInput);
	        }
	        clearTimeout(tm);
	    },200);
	});
}
</script>
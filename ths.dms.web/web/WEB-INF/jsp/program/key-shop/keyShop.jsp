<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@page import="ths.dms.helper.Configuration"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<input type="hidden" id="function_code" value="<s:property value="systimestamp"/>" />
<s:set id="downloadToken" value="%{generateReportToken()}"></s:set>
<input type="hidden" id="report_token" value="<s:property value="#downloadToken"/>" />
<s:set id="downloadToken" value="%{putSessionValue(systimestamp, #downloadToken)}"></s:set>
<div class="BreadcrumbSection">
    <ul class="ResetList FixFloat BreadcrumbList">
        <li class="Sprite1"><a href="#">Chương trình HTTM</a></li>
        <li><span>Chương trình hỗ trợ thương mại</span></li>
    </ul>
</div>
<div class="CtnOneColSection">
    <div class="ContentSection">
    	<div class="ToolBarSection">
            <div class="SearchSection GeneralSSection">
                <h2 class="Title2Style" onclick="$('#searchKS').toggle();">Thông tin tìm kiếm</h2>
                <div class="SearchInSection SProduct1Form">
                	<div id="searchKS" style="margin:auto; max-width:900px">
	                    <label class="LabelStyle Label1Style">Chương trình</label>
	                    <input id="code" style="width:317px;" type="text" class="InputTextStyle InputText1Style" maxlength="100" placeholder="Nhập mã hoặc tên chương trình" autocomplete="off"/>
	                    <label class="LabelStyle Label11Style">Đơn vị</label>
	                    <input type="text" id="shop" style="width:327px;" class="InputTextStyle InputText1Style" />
	                    <div class="Clear"></div>
	                    <label class="LabelStyle Label1Style">Năm</label>
	                    <div class="BoxSelect BoxSelect11">
					    	<select class="MySelectBoxClass" id="year" onchange="return KeyShop.yearChange('year', 'num');" style="width: 115px;" autocomplete="off">
								<s:iterator value="lstYear">
									<option value="<s:property/>"><s:property/></option>
								</s:iterator>
					    	</select>
						</div>
						<label class="LabelStyle Label11Style">Chu kỳ</label>
	                    <div class="BoxSelect BoxSelect11">
	                    	<select id="num" style="width: 115px;">
						    </select>
						</div>
						<label class="LabelStyle Label11Style">Loại</label>
	                    <div class="BoxSelect BoxSelect11">
					    	<select class="MySelectBoxClass" id="ksType" style="width: 115px;" autocomplete="off">
								<option value="" selected="selected">Tất cả</option>
								<option value="1">Tích lũy</option>
								<option value="2">Trưng bày</option>
					    	</select>
						</div>
						<label class="LabelStyle Label11Style">Trạng thái</label>
	                    <div class="BoxSelect BoxSelect11">
					    	<select class="MySelectBoxClass" id="status" style="width: 115px;" autocomplete="off">
								<option value="" selected="selected">Tất cả</option>
								<option value="1">Hoạt động</option>
								<option value="0">Tạm ngưng</option>
					    	</select>
						</div>
						<div class="Clear"></div>
	                    <div class="BtnCenterSection">
	                        <button id="btnSearch" onclick="return KeyShop.searchKS()" class="BtnGeneralStyle">Tìm kiếm</button>
	                    </div>
	                    <div class="Clear"></div>
                    </div>
                    <p style="display:none;" class="ErrorMsgStyle" id="errMsg"/>
                    <div class="Clear"></div>
                </div>
                <h2 class="Title2Style">Danh sách chương trình</h2>
                <div class="GridSection">
                  	<div class="GeneralTable Table34Section">
						<div class="BoxGeneralTTitle" id="dgGridContainer">
							<table id="gridKS"></table>
						</div>
					</div>
              	</div>
            </div>
            <div class="Clear"></div>
            <div class="SearchSection GeneralSSection" id="productDiv" style="display:none">
                <h2 class="Title2Style" onclick="$('#searchProduct').toggle();" id="productTitle">SẢN PHẨM CỦA CHƯƠNG TRÌNH</h2>
                <div class="SearchInSection SProduct1Form">
                	<div id="searchProduct" style="margin:auto; max-width:1200px">
	                    <label class="LabelStyle Label1Style">Sản phẩm</label>
	                    <input id="productCode" style="width:250px;" type="text" class="InputTextStyle InputText1Style" maxlength="100" placeholder="Nhập mã hoặc tên sản phẩm" autocomplete="off"/>
	                    <label class="LabelStyle Label2Style"><s:text name="catalog.categoryvn"/></label>
                        <div class="BoxSelect BoxSelect2" >
                            <select id="category" class="MySelectBoxClassNo" multiple="multiple" onchange="KeyShop.fillCategoryChildToMultiSelect();">
         					</select>
                       	</div>
                       	<label class="LabelStyle Label1Style"><s:text name="catalog.categoryvn.child"/></label>
                        <div class="BoxSelect BoxSelect2" >
                            <select id="categoryChild" class="MySelectBoxClassNo" multiple="multiple">
         					</select>
                       	</div>
	                    <div class="BtnCenterSection">
	                        <button id="btnSearch" onclick="return KeyShop.searchKSProduct()" class="BtnGeneralStyle">Tìm kiếm</button>
	                    </div>
	                    <div class="Clear"></div>
                    </div>
                    <div class="Clear"></div>
                </div>
                <h2 class="Title2Style">Danh sách sản phẩm</h2>
                <div class="GridSection">
                  	<div class="GeneralTable Table34Section">
						<div class="BoxGeneralTTitle" id="dgGridContainerProduct">
							<table id="gridProduct"></table>
						</div>
					</div>
              	</div>
            </div>
            <div class="Clear"></div>
            <div class="SearchSection GeneralSSection" id="levelDiv" style="display:none"">
                <h2 class="Title2Style" id="levelTitle">Các mức</h2>
                <div class="GridSection">
                  	<div class="GeneralTable Table34Section">
						<div class="BoxGeneralTTitle" id="dgGridContainerLevel">
							<table id="gridLevel"></table>
						</div>
					</div>
              	</div>
            </div>
            <div class="Clear"></div>
            
            <div class="SearchSection GeneralSSection" id="shopDiv" style="display:none">
            	<h2 class="Title2Style" onclick="$('#searchShop').toggle();" id="shopTitle">ĐƠN VỊ PHÂN BỔ</h2>
                <div class="SearchInSection SProduct1Form">
                	<div id="searchShop" style="margin:auto; max-width:1200px">
	                    <label class="LabelStyle">Hiển thị đầy đủ cây đơn vị</label>
                		<input id="checkAllTreeShop" type="checkbox" checked="checked" onchange="return KeyShop.loadShopGrid()" class="InputTextStyle InputText1Style" style="width: 30px;" />
                    </div>
                    <div class="Clear"></div>
                </div>
                <div class="GridSection">
                  	<div class="GeneralTable Table34Section">
						<div class="GridSection" id="shopContainerGrid">
			 				<table id="treeGridShop" class="easyui-treegrid"></table>
						</div>
					</div>					 
              	</div> 
              	<div class="SearchInSection SProduct1Form">
	            		<div class="BtnCenterSection">              		
							<button class="BtnGeneralStyle cmsiscontrol" id="group_edit_ks_btnUpdateOrgAccess" onclick="return KeyShop.updateChangeByCheckInTreeGridShop();">Cập nhật</button>
							<div class="Clear"></div>
						</div>
	            </div>
        	</div>

        	<div class="SearchSection GeneralSSection" id="customerDiv" style="display:none">
                <h2 class="Title2Style" id="customerTitle">KHÁCH HÀNG ĐĂNG KÝ THAM GIA</h2>
                	<div class="SearchInSection SProduct1Form">                	
	                    <label class="LabelStyle Label1Style">Khách hàng</label>
	                    <input id="customerMix" style="width:325px;" type="text" class="InputTextStyle InputText1Style" maxlength="100" placeholder="Nhập mã, tên, địa chỉ khách hàng" autocomplete="off"/>
	                    <label class="LabelStyle Label11Style">Đơn vị</label>
	                    <input type="text" id="shopBottom" style="width:325px;" class="InputTextStyle InputText1Style" />
	                    <div class="Clear"></div>
	                    <label class="LabelStyle Label1Style">Năm</label>
	                    <div class="BoxSelect BoxSelect11">
					    	<select class="MySelectBoxClass" id="yearBottom" onchange="return KeyShop.yearChange('yearBottom', 'numBottom');" style="width: 115px;" autocomplete="off">
								<s:iterator value="lstYear">
									<option value="<s:property/>"><s:property/></option>
								</s:iterator>
					    	</select>
						</div>
	                    <label class="LabelStyle Label1Style">Chu kỳ</label>
	                    <div class="BoxSelect BoxSelect11">
	                    	<select id="numBottom" style="width: 115px;">
					    	</select>
						</div>
						<label class="LabelStyle Label11Style">Trạng thái</label>
	                    <div class="BoxSelect BoxSelect11">
					    	<select class="MySelectBoxClass" id="statusBottom" style="width: 115px;" autocomplete="off">
								<option value="" selected="selected">Tất cả</option>
								<option value="1">Đã duyệt</option>
								<option value="2">Chưa duyệt</option>
								<option value="3">Từ chối</option>
					    	</select>
						</div>
						<div class="Clear"></div>
	                    <div class="BtnCenterSection">
	                        <button id="btnSearch" onclick="return KeyShop.searchKSCustomer()" class="BtnGeneralStyle">Tìm kiếm</button>
	                    </div>
	            		<div class="Clear"></div>
	            	</div>
	                <div class="GridSection">
	                  	<div class="GeneralTable Table34Section">
							<div class="BoxGeneralTTitle" id="dgGridContainerCustomer">
								<table id="gridCustomer"></table>
							</div>
						</div>
	              	</div>
	              	<div class="Clear"></div>      	        
	        		<div class="SearchInSection SProduct1Form">
	            		<div class="BtnCenterSection">              		
							<button class="BtnGeneralStyle" id="btnExportCustomer" onclick="return KeyShop.exportCustomer();">Export</button>
						<div class="Clear"></div>
	            	</div> 
            	</div> 
            	<div class="Clear"></div>	
            </div>   
            
            <div class="SearchSection GeneralSSection" id="rewardDiv" style="display:none">
                <h2 class="Title2Style" id="rewardTitle">THÔNG TIN TRẢ THƯỞNG KHÁCH HÀNG</h2>
                	<div class="SearchInSection SProduct1Form">                	
	                    <label class="LabelStyle Label1Style">Khách hàng</label>
	                    <input id="customerReward" style="width:325px;" type="text" class="InputTextStyle InputText1Style" maxlength="100" placeholder="Nhập mã, tên, địa chỉ khách hàng" autocomplete="off"/>
	                    <label class="LabelStyle Label11Style">Đơn vị</label>
	                    <input type="text" id="shopReward" style="width:325px;" class="InputTextStyle InputText1Style" />
	                    <div class="Clear"></div>
	                    <label class="LabelStyle Label1Style">Năm</label>
	                    <div class="BoxSelect BoxSelect11">
					    	<select class="MySelectBoxClass" id="yearReward" onchange="return KeyShop.yearChange('yearReward', 'numReward');" style="width: 115px;" autocomplete="off">
								<s:iterator value="lstYear">
									<option value="<s:property/>"><s:property/></option>
								</s:iterator>
					    	</select>
						</div>
	                    <label class="LabelStyle Label1Style">Chu kỳ</label>
	                    <div class="BoxSelect BoxSelect11">
	                    	<select id="numReward" style="width: 115px;">
					    	</select>
						</div>
						<label class="LabelStyle Label11Style">Kết quả</label>
	                    <div class="BoxSelect BoxSelect11">
					    	<select class="MySelectBoxClass" id="resultReward" style="width: 115px;" autocomplete="off">
								<option value="" selected="selected">Tất cả</option>
								<option value="5">Không đạt</option>
								<option value="6">Đạt</option>
					    	</select>
						</div>
						<label class="LabelStyle Label11Style">Trạng thái</label>
	                    <div class="BoxSelect BoxSelect11">
					    	<select class="MySelectBoxClass" id="statusReward" style="width: 115px;" autocomplete="off">
								<option value="" selected="selected">Tất cả</option>
								<option value="-1">Chưa import trả thưởng</option>
								<option value="0">Chuyển khoản</option>
								<option value="1">Chưa mở khóa trả thưởng</option>
								<option value="2">Đã mở khóa trả thưởng</option>
								<option value="3">Đã trả 1 phần</option>
								<option value="4">Đã trả hết</option>
					    	</select>
						</div>
						<div class="Clear"></div>
	                    <div class="BtnCenterSection">
	                        <button id="btnSearchReward" onclick="return KeyShop.searchKSReward()" class="BtnGeneralStyle">Tìm kiếm</button>
	                    </div>
	            		<div class="Clear"></div>
	            	</div>
	                <div class="GridSection">
	                  	<div class="GeneralTable Table34Section">
							<div class="BoxGeneralTTitle" id="dgGridContainerReward">
								<table id="gridReward"></table>
							</div>
						</div>
	              	</div>
	              	<div class="Clear"></div>      	        
	        		<div class="SearchInSection SProduct1Form">
	            		<div class="BtnCenterSection">              		
							<button class="BtnGeneralStyle cmsiscontrol" id="group_tt_k_mk_btnUnlockReward" onclick="return KeyShop.unlockReward();">Mở khóa trả thưởng</button>
							<button class="BtnGeneralStyle cmsiscontrol" id="group_tt_k_mk_btnLockReward" onclick="return KeyShop.lockReward();">Khóa trả thưởng</button>
							<button class="BtnGeneralStyle" id="btnExportReward" onclick="return KeyShop.exportReward();">Export</button>
						<div class="Clear"></div>
	            	</div> 
            	</div> 
            	<div class="Clear"></div>	
			</div>
	    </div>
		<p id="errMsgKeyShop" style="display: none;" class="ErrorMsgStyle"></p>
		<p id="successMsgKeyShop" class="SuccessMsgStyle" style="display:none;"></p>
		<input type="hidden" id="permissionIdByFormTemplate" />
	</div>
</div>
<div class="Clear"></div>
<div class="GeneralForm GeneralNoTP1Form GeneralCntSection" style="border: medium hidden;">
	<div class="Func1Section cmsiscontrol" id="group_import_insert_div_imp">
		<div class=" BoxSelect BoxSelect2">
	        <select class="MySelectBoxClass" name="excelType" id="excelType" onchange="KeyShop.excelTypeChanged();" autocomplete="off">
	            <option value="1">Chương trình</option>
	            <option value="2">Sản phẩm</option>
	            <option value="3">Đơn vị tham gia</option>
	            <option value="4">Khách hàng tham gia</option>
	            <option value="5">Trả thưởng khách hàng</option>
	        </select>
		</div>
		<p class="DownloadSFileStyle DownloadSFile2Style">
			<a href="javascript:void(0)" id="downloadTemplate" onclick="KeyShop.exportExcelTemplate();" class="Sprite1 "><s:text name="unit_tree.search_unit.search_grid.import.download_file"/></a>
		</p> 
		<form action="/key-shop/import-excel" name="importFrm" style="float: left;" id="importFrm"  method="post" enctype="multipart/form-data">
			<input type="hidden" name="token" id="tokenImport" value='<s:property value="token"/>'>
			<input type="hidden" id="isView" name="isView"/>
			<input type="file" class="InputFileStyle " size="22" name="excelFile" id="excelFile" onchange="previewImportExcelFile(this,'importFrm');">
			<div class="FakeInputFile">
				<input id="fakefilepc" style="margin: -3px 5px 0 0;" readonly="readonly" type="text" class=" InputTextStyle InputText1Style">
			</div>
		</form>
		<button id="btnImportExcel" class="BtnGeneralStyle " onclick="return KeyShop.openImportExcelType();"><s:text name="unit_tree.search_unit.search_grid.import_button_text"/></button>
	</div>
	<button id="btnExportExcel" onclick="return KeyShop.exportExcelKS();" class="BtnGeneralStyle"><s:text name="unit_tree.search_unit.search_grid.export_button_text"/></button>
	<div class="Clear"></div>
	<p id="errExcelMsg" class="ErrorMsgStyle" style="display: none; margin: 5px 0; padding: 2px 0;"></p>
	<div id="responseDiv" style="display: none"></div>
	<div class="Clear"></div>
</div>
<div class="Clear"></div>
<tiles:insertTemplate template="/WEB-INF/jsp/program/key-shop/keyShopPopup.jsp" />
<tiles:insertTemplate template="/WEB-INF/jsp/program/key-shop/keyShopPopupCustomer.jsp" />
<input type="hidden" id="ksIdCurrent" name="isView"/>
<s:hidden id="shopId" name="shopId"></s:hidden>
<s:hidden id="curShopId" name="shopId"></s:hidden>
<s:hidden id="currentCycleId" name="currentCycleId"></s:hidden>
<s:hidden id="lastCycleId" name="lastCycleId"></s:hidden>
<s:hidden id="currentYear" name="year"></s:hidden>
<s:hidden id="currentNum" name="num"></s:hidden>
<script type="text/javascript">
$(document).ready(function(){
	$('#year').val($('#currentYear').val()).change();
	$('#yearBottom').val($('#currentYear').val()).change();
	$('#yearReward').val($('#currentYear').val()).change();
	$('#fromCycleKS').val($('#currentCycleId').val()).change();
	$('#toCycleKS').val($('#lastCycleId').val()).change();
	KeyShop.initImport();
	ReportUtils.loadComboTree('shop', 'shopId', $('#curShopId').val(), function(shopId){
	});
	$('#productCode').bind('keyup', function(event){
		if(event.keyCode == keyCodes.ENTER){
			KeyShop.searchKSProduct();
		}
	});
	$('#customerMix').bind('keyup', function(event){
		if(event.keyCode == keyCodes.ENTER){
			KeyShop.searchKSCustomer();
		}
	});
	$('#customerReward').bind('keyup', function(event){
		if(event.keyCode == keyCodes.ENTER){
			KeyShop.searchKSReward();
		}
	});
	var editTitle = '<a class="cmsiscontrol" id="group_edit_ks_gr_main_inst" title="Thêm mới Keyshop" href="javascript:void(0)" onclick=\"return KeyShop.openPopupEditKS(0);\"><img src="/resources/images/icon_add.png"/></a>';
	$('#gridKS').datagrid({
		url: KeyShop.getUrlSearchKS(),
		width : $('#dgGridContainer').width() - 17,
		height: 'auto',
		singleSelect: true,
		rownumbers: true,
		fitColumns: true,
		scrollbarSize: 0,
		pagination: true,
		pageList  : [10,20,30],
		columns:[[  
		          {field:'ksCode',title:'Mã chương trình',width:70, sortable : true, resizable : false, align: 'left', formatter:function(value,row,index){
		        	  return Utils.XSSEncode(value);
		          }},
		          {field:'ksName',title:'Tên chương trình', width:150,sortable: true,resizable:false, align: 'left',formatter:function(value,row,index){
		        	  return Utils.XSSEncode(value);
		          }},
		          {field:'ksType',title:'Loại', width:50,sortable: true,resizable:false, align: 'left',formatter:function(value,row,index){
		        	  if (value == 1) {
		        		  return 'Tích lũy';
		        	  } else if (value == 2){
		        		  return 'Trưng bày';
		        	  }
		        	  return '';
		          }},
		          {field:'fromCycle',title:'Từ chu kỳ',width:45, sortable: true, align: 'center',resizable:false , formatter:function(value, row, index) {
		        	  return Utils.XSSEncode(value);
		          }},
		          {field:'toCycle',title:'Đến chu kỳ',width:45, sortable: true, align: 'center',resizable:false , formatter:function(value, row, index) {
		        	  return Utils.XSSEncode(value);
		          }},
		          {field:'description',title:'Mô tả',width:100, sortable: true, align: 'left',resizable:false , formatter:function(value, row, index) {
		        	  return Utils.XSSEncode(value);
		          }},
		          {field:'status',title:'Trạng thái',width:50, sortable: true, align: 'center',resizable:false , formatter:function(value, row, index) {
		        	  if (value == 1) {
		        		  return 'Hoạt động';
		        	  } else if (value == 0) {
		        		  return 'Tạm ngưng';
		        	  }
		        	  return '';
		          }},
		          {field:'product',title:'Sản phẩm', width: 35,sortable:false,resizable:false, align: 'center', formatter: function(value, row, index) {
		        	  return "<a title='Xem sản phẩm' href='javascript:void(0)' onclick=\"return KeyShop.showProductGrid('"+ row.ksId +"');\"><img height='17' src='/resources/images/icon-view.png'></a>";
		          }},
		          {field:'level',title:'Mức', width: 35,sortable:false,resizable:false, align: 'center', formatter: function(value, row, index) {
		        	  return "<a title='Xem mức' href='javascript:void(0)' onclick=\"return KeyShop.showLevelGrid('"+ row.ksId +"');\"><img height='17' src='/resources/images/icon-view.png'></a>";
		          }},
		          {field:'shop',title:'Đơn vị', width: 35,sortable:false,resizable:false, align: 'center', formatter: function(value, row, index) {
		        	  return "<a title='Xem đơn vị' href='javascript:void(0)' onclick=\"return KeyShop.showShopGrid('"+ row.ksId +"');\"><img height='17' src='/resources/images/icon-view.png'></a>";
		          }},
		          {field:'customer',title:'Khách hàng', width: 35,sortable:false,resizable:false, align: 'center', formatter: function(value, row, index) {
		        	  return "<a title='Xem khách hàng' href='javascript:void(0)' onclick=\"return KeyShop.showCustomerGrid('"+ row.ksId +"');\"><img height='17' src='/resources/images/icon-view.png'></a>";
		          }},
		          {field:'reward',title:'Trả thưởng', width: 35,sortable:false,resizable:false, align: 'center', formatter: function(value, row, index) {
		        	  return "<a title='Xem trả thưởng' href='javascript:void(0)' onclick=\"return KeyShop.showRewardGrid('"+ row.ksId +"');\"><img height='17' src='/resources/images/icon-view.png'></a>";
		          }},
		          {field:'edit',title:editTitle, width: 35,sortable:false,resizable:false, align: 'center', formatter: function(value, row, index) {
		        	  return "<a title='Xem trả thưởng' href='javascript:void(0)' onclick=\"return KeyShop.openPopupEditKS('"+ row.ksId +"');\"><img height='17' src='/resources/images/icon-edit.png'></a>";
		          }},
		]],
	  	onLoadSuccess :function(data){	    	
	    	$('.datagrid-header-rownumber').html('STT');
	    	KeyShop._mapKSSearch = new Map();
	    	if (data != undefined && data != null && data.rows.length > 0) {
	    		for (var i = 0 ; i < data.rows.length ; i++) {
		    		KeyShop._mapKSSearch.put(data.rows[i].ksId, data.rows[i]);
		    	}
	    		enable('btnExportExcel');
	    	} else {
	    		disabled('btnExportExcel');
	    	}
	    	Utils.functionAccessFillControl('dgGridContainer', function () {
	    		//Xu ly cho cac su kien lien quan den ctrl phan quyen
	    	});
	    },
	    method : 'GET'
	});
});
</script>
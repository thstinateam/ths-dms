<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@page import="ths.dms.helper.Configuration"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<style>
        .datagrid-header-rownumber{
            width:40px;
        }
        .datagrid-cell-rownumber{
            width:40px;
        }
    </style>

<div id ="addOrEditKSCustomerDiv" style="display: none" >
	<div id="addOrEditKSCustomer" class="easyui-dialog" title="Thêm & sửa mức" data-options="closed:true,modal:true">
		<div class="PopupContentMid">
			<div class="GeneralForm Search1Form" >
				<label class="LabelStyle Label1Style">Mã KH (F9)<span class="ReqiureStyle">*</span></label>
                    <input id="customerCode" name="customerCode" type="text" class="InputTextStyle InputText1Style" />
                    <label class="LabelStyle Label1Style">Năm</label>
	                    <div class="BoxSelect BoxSelect11">
					    	<select class="MySelectBoxClass" id="yearPopup" onchange="return KeyShop.yearChangePopup('yearPopup', 'numPopup');" style="width: 115px;" autocomplete="off">
								<s:iterator value="lstYear">
									<option value="<s:property/>"><s:property/></option>
								</s:iterator>
					    	</select>
						</div>
                    <label class="LabelStyle Label2Style">Chu kỳ</label>
	                    <div class="BoxSelect BoxSelect11">
	                    	<select id="numPopup" onchange="return KeyShop.validatePeriod();" style="width: 115px;">
					    	</select>
						</div>
                  	<div class="Clear"></div>
                  	<label class="LabelStyle Label1Style">Địa chỉ:</label>                  	
                  	<label id="addressPop"  class="LabelStyle Label1Style" style="text-align: left; width: 293px;"/></label>                  	
                  	<label id="lblTrangThai" class="LabelStyle Label2Style">Trạng Thái</label>
                  	<div class="BoxSelect BoxSelect11" id="statusPopupDiv">
					    <select class="MySelectBoxClass" id="statusPopup" style="width: 115px;" autocomplete="off">
							<option value="1">Đã duyệt</option>
							<option value="2">Chưa duyệt</option>
							<option value="3">Từ chối</option>
					    </select>
					</div> 
                  	<div class="Clear"></div>
		            <div class="SearchSection GeneralSSection" id="CustomerLevelDiv">
		                <div class="GridSection">
		                  	<div class="GeneralTable Table34Section">
								<div class="BoxGeneralTTitle" id="dgGridContainerCustomerLevel">
									<table id="gridCustomerLevel"></table>
								</div>
							</div>
		              	</div>
		            </div> 
		                	        
	        		<div class="SearchInSection SProduct1Form">
	            		<div class="BtnCenterSection">              		
							<button class="BtnGeneralStyle cmsiscontrol" id="group_edit_ks_btnUpdateCustomerLevel" onclick="return KeyShop.UpdateCustomerLevel();">Cập nhật</button>
							<button class="BtnGeneralStyle cmsiscontrol" id="group_dt_kh_d_tc_btnApproveCustomerLevel" onclick="return KeyShop.ApproveCustomerLevel(1);">Duyệt</button>
							<button class="BtnGeneralStyle cmsiscontrol" id="group_dt_kh_d_tc_btnDenyCustomerLevel" onclick="return KeyShop.ApproveCustomerLevel(3);">Từ chối</button>
						<div class="Clear"></div>
	            	</div>   
				<input type="hidden" id="customerId" value=""/>
				<p id="errMsgKSCustomerLevel" style="display: none;" class="ErrorMsgStyle SpriteErr"></p>
				<p id="successMsgKSCustomerLevel" class="SuccessMsgStyle" style="display: none"></p>
			</div>
		</div>
	</div>
</div>
<div id ="searchStyle1EasyUIDialogDiv" style="display: none" >
	<div id="searchStyle1EasyUIDialog" class="easyui-dialog" style="width: 600px; height: auto;" title="" data-options="closed:true,modal:true">
		<div class="PopupContentMid">
			<div class="GeneralForm Search1Form" >
				<label id="seachStyle1CodeLabel" class="LabelStyle Label1Style" style=" width: 100px;">Mã</label>
				<input id="seachStyle1Code" maxlength="50" tabindex="1" type="text" style=" width: 145px;" class="InputTextStyle InputText1Style" /> 
				<label id="seachStyle1NameLabel" class="LabelStyle Label1Style" style=" width: 100px;">Tên</label> 
				<input id="seachStyle1Name" type="text" maxlength="250" tabindex="2" style="width: 145px;"  class="InputTextStyle InputText1Style" />
				<div class="Clear"></div>
                <label id="seachStyle1AddressLabel" style="display: none;width: 100px;" class="LabelStyle Label1Style">Địa chỉ</label>
                <input id="seachStyle1Address" type="text" style="display: none;width: 409px;"  maxlength="250" tabindex="3" class="InputTextStyle InputText1Style" />
                <div class="Clear"></div>
				<button class="BtnGeneralStyle BtnSearchStyle BtnSearchOnDialog" tabindex="3" id="btnSearchStyle1">Tìm kiếm</button>
				<input type="hidden" name="" id="searchStyle1Url"/>
				<input type="hidden" name="" id="searchStyle1CodeText"/>
				<input type="hidden" name="" id="searchStyle1NameText"/>
				<input type="hidden" name="" id="searchStyle1IdText"/>
				<s:hidden id="searchStyle1AddressText"></s:hidden>
				<div class="Clear"></div>
				<div class="GridSection" id="searchStyle1ContainerGrid">					
					<table id="searchStyle1Grid" class="easyui-datagrid"></table>
					<div id="searchStyle1Pager"></div>
				</div>
				<div class="BtnCenterSection">
					<button id="__btnSave" style="display: none;" class="BtnGeneralStyle">Chọn</button>
					<button id="__btnClose" class="BtnGeneralStyle BtnGeneralMStyle" onclick="$('.easyui-dialog').dialog('close');">Đóng</button>
				</div>
				<p id="errMsgSearch" style="display: none;" class="ErrorMsgStyle SpriteErr"></p>
				<p id="successMsgCopy" class="SuccessMsgStyle" style="display: none"></p>
			</div>
		</div>
	</div>
</div>
</div>
<s:hidden id="ksCustomerId"></s:hidden>
<script type="text/javascript">
$(document).ready(function() {
	$('#yearPopup').val($('#currentYear').val()).change();
	$('#customerCode').bind('keyup', function(event) {	
		if ($('#numPopup').combobox('getValue')<10) numPopup='0'+$('#numPopup').combobox('getValue');
		else numPopup=$('#numPopup').combobox('getValue');
		var cycleIdPopup = $('#yearPopup').val() +''+ numPopup+''+numPopup;
		var obj = new Object();
		obj.shopId = $('#shopId').val().trim();
		obj.cycleId = cycleIdPopup;
		obj.ksId = $('#ksIdCurrent').val();
		var url = '/commons/customer-in-shop/filter?'+ $.param(obj, true);
		if (event.keyCode == keyCode_F9) {
			KeyShop.openCustomerOnDialog(
					url,function(data){					
				$('#customerCode').val(data.code);
				$('#customerCode').change();
			});						
		} 
	});
});

// function validatePeriod(){
// 	if ($('#yearPopup').val() != null && $('#numPopup').combobox('getValue') != null) {
// 		var curYearPeriod = Number($('#currentYear').val().trim());
// 		var curNumPeriod = Number($('#currentCycleId').val().substring(7, 8));
// 		var yearPeriodCreate = Number($('#yearPopup').val().trim());
// 		var numPeriodCreate = Number($('#numPopup').combobox('getValue').trim());
		
// 		if(yearPeriodCreate < curYearPeriod || (yearPeriodCreate == curYearPeriod && numPeriodCreate < curNumPeriod)){
// 			$('#group_dt_kh_d_tc_btnApproveCustomerLevel').hide();
// 			$('#group_dt_kh_d_tc_btnDenyCustomerLevel').hide();
// 			$('#group_edit_ks_btnUpdateCustomerLevel').hide();
// 		}else{
// 			$('#group_edit_ks_btnUpdateCustomerLevel').show();
// 		}
// 		var ksId = $('#ksIdCurrent').val().trim()
// 		KeyShop.loadGridCustomerLevel(ksId);
// 	}
// }
	
</script>


<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@page import="ths.dms.helper.Configuration"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<div style="display:none">
	<div id="popupImport" class="easyui-dialog" title="Import" data-options="closed:true,modal:true">
		<div class="PopupContentMid">
			<div class="GeneralForm" style="margin-left: 20px;padding-top:0px">
                <input type="radio" name="importRadio" value="1" id="rbKSImport" checked><label style="display:inline;" for="rbKSImport"> Chương trình</label><br>
				<input type="radio" name="importRadio" value="2" id="rbProductImport"><label style="display:inline;" for="rbProductImport"> Sản phẩm</label><br>
				<input type="radio" name="importRadio" value="3" id="rbShopImport"><label style="display:inline;" for="rbShopImport"> Đơn vị tham gia</label><br>
				<input type="radio" name="importRadio" value="4" id="rbCustomerImport"><label style="display:inline;" for="rbCustomerImport"> Khách hàng tham gia</label><br>
				<input type="radio" name="importRadio" value="5" id="rbRewardImport"><label style="display:inline;" for="rbRewardImport"> Trả thưởng khách hàng</label><br>
                <div class="Clear"></div>
			</div>
			<div class="GeneralForm BtnCenterSection" style="padding-top:0px" >
            	<button id="group_edit_ks_btnAcceptImport_ct" class="BtnGeneralStyle cmsiscontrol" onclick="KeyShop.importExcel();">Đồng ý</button>
            </div>
    	</div>
	</div>
</div>

<div style="display:none">
	<div id="popupImportUnit" class="easyui-dialog" title="Import" data-options="closed:true,modal:true">
		<div class="PopupContentMid">
			<div class="GeneralForm" style="margin-left: 20px;padding-top:0px">
                <input type="radio" name="importRadio" value="1" id="rbKSImport" checked><label style="display:inline;" for="rbKSImport"> Nhân viên</label><br>
				<input type="radio" name="importRadio" value="2" id="rbProductImport"><label style="display:inline;" for="rbProductImport"> Ngành hàng con</label><br>
                <div class="Clear"></div>
			</div>
			<div class="GeneralForm BtnCenterSection" style="padding-top:0px" >
            	<button id="group_edit_ks_btnAcceptImport_nv" class="BtnGeneralStyle" onclick="UnitTreeCatalog.importExcel();">Đồng ý</button>
            </div>
    	</div>
	</div>
</div>

<div id ="addOrEditKSDiv" style="display: none" >
	<div id="addOrEditKS" class="easyui-dialog" title="Import" data-options="closed:true,modal:true">
		<div class="PopupContentMid">
			<div class="GeneralForm Search1Form" >
				<label class="LabelStyle Label1Style">Mã CT <span class="ReqiureStyle">(*)</span></label>
				<input id="ksCodeKS" maxlength="50" type="text" class="InputTextStyle InputText2Style" style="width:136px;"/>
				<label class="LabelStyle Label1Style">Tên CT <span class="ReqiureStyle">(*)</span></label>
				<input id="ksNameKS" maxlength="250" type="text" class="InputTextStyle InputText2Style" style="width:136px;"/>
				<label class="LabelStyle Label1Style">Loại </label>
				<div class="BoxSelect BoxSelect6">
			    	<select class="MySelectBoxClass" id="ksTypeKS" autocomplete="off">
						<option value="1">Tích lũy</option>
						<option value="2">Trưng bày</option>
			    	</select>
				</div>
				<div class="Clear"></div>
				<label class="LabelStyle Label1Style">Từ chu kỳ <span class="ReqiureStyle">(*)</span></label>
				<div class="BoxSelect BoxSelect6">
			    	<select class="MySelectBoxClass" id="fromCycleKS">
						<s:iterator value="lstCycleVO">
							<option value='<s:property value="cycleId"/>'><s:property value="cycleName"/></option>
						</s:iterator>
			    	</select>
				</div>
				<label class="LabelStyle Label1Style">Đến chu kỳ <span class="ReqiureStyle">(*)</span></label>
				<div class="BoxSelect BoxSelect6">
			    	<select class="MySelectBoxClass" id="toCycleKS">
						<s:iterator value="lstCycleVO">
							<option value='<s:property value="cycleId"/>'><s:property value="cycleName"/></option>
						</s:iterator>
			    	</select>
				</div>
				<label class="LabelStyle Label1Style">Số ảnh / 1 lần ghé thăm</label>
				<input id="minPhotoKS" maxlength="3" type="text" class="InputTextStyle InputText2Style vinput-number" />
				<div class="Clear"></div>
				<label class="LabelStyle Label1Style">Mô tả</label>
				<textarea class="LabelStyle" id="descriptionKS" maxlength="500" rows="5" style="margin-right: 0; width: 380px;color: black;font-size: 12px;text-align: left;"></textarea>
				<label class="LabelStyle Label1Style">Trạng thái</label>
				<div class="BoxSelect BoxSelect6">
			    	<select class="MySelectBoxClass" id="statusKS">
						<option value="1" checked>Hoạt động</option>
						<option value="0">Tạm ngưng</option>
			    	</select>
				</div>
				<div class="Clear"></div>
				<div class="BtnCenterSection">
					<button id="group_edit_ks_change_ks" onclick="KeyShop.editKS();" class="BtnGeneralStyle cmsiscontrol">Cập nhật</button>
				</div>
				<input type="hidden" id="ksIdKS" value=""/>
				<p id="errMsgKS" style="display: none;" class="ErrorMsgStyle SpriteErr"></p>
				<p id="successMsgKS" class="SuccessMsgStyle" style="display: none"></p>
			</div>
		</div>
	</div>
</div>

<div id ="addOrEditKSLevelDiv" style="display: none" >
	<div id="addOrEditKSLevel" class="easyui-dialog" title="Thêm & sửa mức" data-options="closed:true,modal:true">
		<div class="PopupContentMid">
			<div class="GeneralForm Search1Form" >
				<label class="LabelStyle Label1Style">Mã mức <span class="ReqiureStyle">(*)</span></label>
				<input id="levelCode" maxlength="50" type="text" class="InputTextStyle InputText2Style" style="width:136px;"/>
				<label class="LabelStyle Label1Style">Tên mức <span class="ReqiureStyle">(*)</span></label>
				<input id="levelName" maxlength="250" type="text" class="InputTextStyle InputText2Style" style="width:136px;"/>
				<div class="Clear"></div>
				<label class="LabelStyle Label1Style">Số tiền <span class="ReqiureStyle">(*)</span></label>
				<input id="amountLevel" maxlength="20" type="text" class="InputTextStyle InputText2Style vinput-money" />
				<label class="LabelStyle Label1Style">Số lượng</label>
				<input id="quantityLevel" maxlength="11" type="text" class="InputTextStyle InputText2Style vinput-money" />
				<div class="Clear"></div>
				<div class="BtnCenterSection">
					<button id="group_edit_ks_change_ks_lv" onclick="KeyShop.editKSLevel();" class="BtnGeneralStyle cmsiscontrol">Cập nhật</button>
				</div>
				<input type="hidden" id="levelId" value=""/>
				<p id="errMsgKSLevel" style="display: none;" class="ErrorMsgStyle SpriteErr"></p>
				<p id="successMsgKSLevel" class="SuccessMsgStyle" style="display: none"></p>
			</div>
		</div>
	</div>
</div>

<div id ="searchStyle2EasyUIDialogDiv" style="display:none; width:630px; height: auto;" >
	<div id="searchStyle2EasyUIDialog" data-options="closed:true,modal:true">
		<div class="PopupContentMid2">
			<div class="GeneralForm Search1Form">				
				<label id="searchStyle2EasyUICodeLabel" class="LabelStyle Label2Style">Mã sản phẩm</label>
				<input id="searchStyle2EasyUICode" maxlength="50" tabindex="1" type="text" class="InputTextStyle InputText5Style" /> 
				<label id="searchStyle2EasyUINameLabel" class="LabelStyle Label2Style">Tên sản phẩm</label> 
				<input id="searchStyle2EasyUIName" type="text" maxlength="250" tabindex="2"  class="InputTextStyle InputText5Style" />
				<div class="Clear"></div>
				<div class="BtnCenterSection">
					<button class="BtnGeneralStyle BtnSearchOnDialog" tabindex="3" id="btnSearchStyle2EasyUI">Tìm kiếm</button>
				</div>
				<div class="Clear"></div>
				<s:hidden id="searchStyle2EasyUIUrl"></s:hidden>
				<s:hidden id="searchStyle2EasyUICodeText"></s:hidden>
				<s:hidden id="searchStyle2EasyUINameText"></s:hidden>
			</div>
			<div class="GeneralForm Search1Form">
				<h2 class="Title2Style Title2MTStyle" id="searchStyle2EasyUIContainerTitle">Kết quả tìm kiếm </h2>
				<div class="GridSection" id="searchStyle2EasyUIContainerGrid">
					<table id="searchStyle2EasyUIGrid"></table>
				</div>
				<div class="Clear"></div>
				<div class="BtnCenterSection">
					<button class="BtnGeneralStyle" style="display: none;" id="btnSearchStyle2EasyUIUpdate">Chọn</button>
					<button class="BtnGeneralStyle BtnGeneralMStyle" id="btnSearchStyle2EasyUIClose" onclick="$('#searchStyle2EasyUIDialog').dialog('destroy');">Đóng</button>
				</div>
			</div>
			<p id="errMsgSearchStyle2EasyUI" style="display: none;" class="ErrorMsgStyle SpriteErr"></p>
		</div>
	</div>
</div>
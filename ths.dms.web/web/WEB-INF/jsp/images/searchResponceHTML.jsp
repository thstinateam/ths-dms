<%@page import="ths.dms.web.utils.DateUtil"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@page import="ths.dms.helper.Configuration"%>
<div class="CtnOneColSection">
	<div class="ContentSection">
       	<div class="GeneralCntSection">
			<ul class="ResetList PhotoGroupList">
				<s:iterator id="lstAlbum" value="lstAlbum" var="obj" status="status">
					<li>
				       	<a class="fancybox PhotoThumbGroup" href="javascript:void(0)" onclick="return Images.showAlbumDetail(<s:property value="displayProgrameId"/>);">
					        <span class="BoxFrame"><span class="BoxMiddle">
								<img id="searchResponceHTML_<s:property value="id"/>" width="201" class="imSearch ImageFrame1" height="144" data-original="<%=Configuration.getImgServerSOPath()%><s:property value="urlThum"/>?v=<%=DateUtil.getCurrentGMTDate().getTime()%>" src="/resources/images/grey.jpg">
							</span></span>
						</a>
				    <p class="Text1Style"><a href="javascript:void(0)" onclick="return Images.showAlbumDetail(<s:property value="displayProgrameId"/>);"><s:property value="displayProgrameCode"/></a></p>
				    <p class="Text2Style"><s:property value="countImg"/> <s:text name="image_manager_image"/></p>
					</li>
				</s:iterator>
			</ul>
    	</div>
	</div>
    <div class="Clear"></div>
</div>
<input type="hidden"  id="errMsgDate"  value = "<s:property value="errMsg"/>" />
<script type="text/javascript" >
$(document).ready(function(){
	$(".imSearch").lazyload({
	    event: "scrollstop"
	});
});
</script>
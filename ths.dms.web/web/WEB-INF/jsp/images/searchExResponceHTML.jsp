<%@page import="ths.dms.web.utils.DateUtil"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@page import="ths.dms.helper.Configuration"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<div class="CtnTwoColsSection">
	<div class="ContentSection" style="width:100%">
		<div class="ReportCtnSection" id="albumContent">
			<tiles:insertTemplate template="/WEB-INF/jsp/images/imagesOfAlbumDetail.jsp"></tiles:insertTemplate>
	    </div>
	</div>
</div>
<input type="hidden"  id="errMsgDate"  value = "<s:property value="errMsg"/>" />
<script type="text/javascript" >
$(document).ready(function(){
	$(".imSearch").lazyload({
	    event: "scrollstop"
	});
});
</script>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<script type="text/javascript">
function makepage(src)
{
return "<html>\n" +
"<head>\n" +
"<title>Temporary Printing Window</title>\n" +
"<script>\n" +
"function step1() {\n" +
" setTimeout('step2()', 10);\n" +
"}\n" +
"function step2() {\n" +
" window.print();\n" +
" window.close();\n" +
"}\n" +
"</scr" + "ipt>\n" +
"</head>\n" +
"<body onLoad='step1()'>\n" +
"<img src='" + src + "'/>\n" +
"</body>\n" +
"</html>\n";
}
function printme(evt,type)
{
if (!evt) {
// Old IE
evt = window.event;
}
var image = null;
if (evt == undefined || evt == null) {
// 	src = $('.ImageFrame4').attr('src');
	if (type != undefined && type != null) {
		src = $('.ImagePrintingClassFull .DownloadPhotoLink ').attr('href');
	} else {
		src = $('.ImagePrintingClass .DownloadPhotoLink ').attr('href');
	}
	
} else {
	image = evt.target;
	if (!image) {
		// Old IE
		image = window.event.srcElement;
	}
	src = image.src;
}
var newWin = window.frames["printf"];
newWin.document.write(makepage(src));
window.frames["printf"].focus();
newWin.document.close();
setTimeout(function() {newWin.document.write('');},1000);
// link = "about:blank";
// var pw = window.open(link, "_new");
// pw.document.open();
// pw.document.write(makepage(src));
// pw.document.close();
}
</script>
<input type="hidden" id="function_code" value="<s:property value="systimestamp"/>" />
<s:set id="downloadToken" value="%{generateReportToken()}"></s:set>
<input type="hidden" id="report_token" value="<s:property value="#downloadToken"/>" />
<s:set id="downloadToken" value="%{putSessionValue(systimestamp, #downloadToken)}"></s:set>
<div class="BreadcrumbSection">
	<ul class="ResetList FixFloat BreadcrumbList">
		<li class="Sprite1"><a href="javascript:void(0)"><s:text name="catalog_sales_brand_catalogy"/></a>
		</li>
		<li><span id="title"><s:text name="jsp.images.info.title"/></span>
		</li>
	</ul>
</div>
<div class="ToolBarSection">
	<div class="SearchSection GeneralSSection">
		<div class="SearchInSection PhotoSearchSection">
			<label class="LabelStyle Label1Style"><s:text name="image_manager_unit"/></label>
			<div class="BoxSelect BoxSelect2">				
				<div class="Field2">
				<input id="shop" type="text" data-placeholder="Chọn đơn vị"  style="width:301px;height: auto;" class="InputTextStyle InputText1Style" />
				<%-- 	<input id="shop" type="text" data-placeholder="<s:text name="image_manager_choose_unit"/>"  style="width:301px;height: auto;" class="InputTextStyle InputText1Style" /> --%>
				</div>
			</div>
			<label class="LabelStyle Label2Style"><s:text name="image_manager_photographer"/></label>
		    <div class="BoxSelect BoxSelect2" id="divStaff">
				<input id="staff" type="text" data-placeholder="<s:text name="image_manager_choose_photographer"/>"  style="width:300px;height: auto;" class="InputTextStyle InputText1Style" />
		    </div>
		    <label class="LabelStyle Label2Style"><s:text name="image_manager_rout"/></label>
		    <div class="BoxSelect BoxSelect4" id = "divTuyen">
		        <select id="tuyen" class="MySelectBoxClass" name="LevelSchool">
		            <option value="-1" selected="selected"><s:text name="image_manager_all"/></option>
		            <option value="2"><s:text name="image_manager_monday"/></option>
		            <option value="3"><s:text name="image_manager_tuesday"/></option>
		            <option value="4"><s:text name="image_manager_wednesday"/></option>
		            <option value="5"><s:text name="image_manager_thursday"/></option>
		            <option value="6"><s:text name="image_manager_friday"/></option>
		            <option value="7"><s:text name="image_manager_saturday"/></option>
		            <option value="8"><s:text name="image_manager_sunday"/></option>
		       </select>
		    </div>
		    <div class="Clear"></div>
		    <label class="LabelStyle Label1Style"><s:text name="image_manager_customer"/></label>
		    <input type="text" id="customerCode" class="InputTextStyle InputText2Style" placeholder="<s:text name="image_manager_custcode"/>" maxlength="40"/>
		    <input type="text" id="customerAddress" class="InputTextStyle InputText8Style" placeholder="<s:text name="image_manager_nameoraddress"/>" maxlength="200"/>			    
		    <label class="LabelStyle Label2Style"  style="margin-left:10px">Chương trình thương mại</label>
		    <div class="BoxSelect BoxSelect2" id="divCttb">
				<input id="cttb" type="text" data-placeholder="Chọn chương trình"  style="width:300px;height: auto;" class="InputTextStyle InputText1Style" />
		    </div>
		    <input type="text" hidden="true" placeholder="<s:text name="image_manager_program_level"/>" id="level" class="InputTextStyle InputText2Style" maxlength="15" title="<s:text name="image_manager_program_input"/>" />
		    <label class="LabelStyle Label2Style" style="margin-left:9px"><s:text name="image_manager_capturedate"/></label>
		  	<input type="text" class="InputTextStyle " id="fromDate"	maxlength="10" alt="<s:text name="image_manager_choosedate"/>" value="<s:property value="fromDate"/>" style="width:70px;"/>
		    <span class="TempStyle">-</span>
		    <input type="text" class="InputTextStyle InputText2Style" id="toDate"	maxlength="10" alt="<s:text name="image_manager_choosedate"/>" value="<s:property value="toDate"/>" style="width:70px;"/>
		    <div class="Clear"></div>
		    <div class="SearchInSection SProduct1Form">
				<div class="BtnCenterSection">
		     		<button id="btnSearch" class="BtnGeneralStyle BtnGeneralMStyle" onClick="Images.btnSearchClick();"><s:text name="image_manager_search"/></button>
		    	</div>
		    </div>
		    <div class="Clear"></div>
		    <p id="errMsg" class="ErrorMsgStyle SpriteErr" style="display: none"></p>
	    </div>
	    <div class="Clear"></div>
	    <div class="TabSection" style="display:none;">
			<ul class="ResetList TabSectionList">
				<li><a class="Sprite1" onclick="Images.showTab4();" id="tab4" href="javascript:void(0);">
					<span class="Sprite1 Active">Trưng bày theo CTHTTM</span></a></li>
				<%-- 	<span class="Sprite1"><s:text name="image_manager_presentbyprogram"/></span></a></li> --%>
				<li><a class="Sprite1 Active" onclick="Images.showTab1();" id="tab1" href="javascript:void(0);">
					<span class="Sprite1"><s:text name="image_manager_shop_point"/></span></a></li>
				<li><a class="Sprite1" onclick="Images.showTab2();" id="tab2" href="javascript:void(0);">
					<span class="Sprite1"><s:text name="image_manager_shop_close"/></span></a></li>
				<li><a class="Sprite1" onclick="Images.showTab3();" id="tab3" href="javascript:void(0);">
					<span class="Sprite1"><s:text name="image_manager_present_general"/></span></a></li>
			</ul>
		
		</div>
		<div class="Clear"></div>
	</div>
</div>
<div id ="imageContent">
	<s:if test="staff_role != null && staff_role> 2">
		<tiles:insertTemplate template="/WEB-INF/jsp/images/searchResponceHTML.jsp"></tiles:insertTemplate>	
	</s:if>
	<s:else>
		<p style="font-family:Arial; font-weight:bold;text-align:center;"><s:text name="image_manager_search_condition"/></p>
	</s:else>
</div>
<div id="popup-images" style="display:none;">
	<div class="GeneralDialog General2Dialog">
      	<div class="DialogPhotoSection FixFloat">
        	<div class="PhotoCols1">
              	<div id="listImage" class="DScrollpane">
              		<ul id="listImageUL" class="ResetList PhotoCols2List">
              		</ul>
            	</div>
            </div>
            <div class="PhotoCols2">
            	<div class="PhotoCols1Info1">
                	<p onclick="Images.showImageNextPre(0);" class="LeftStyle Sprite1"></p>
                	<div class="PhotoLarge"><span class="BoxFrame BoxFrame2"><span class="BoxMiddle">
                		<img id="bigImages" class="ImageMap ImageFrame4" width="752" height="508"/>
                		<img style="display:none" id="loadingImages" width="50" height="50" src="/resources/images/loading.gif">
                	</span></span></div>
                    <p onclick="Images.showImageNextPre(1);" class="RightStyle Sprite1"></p>
                </div>
                <div class="PhotoCols1Info3">
                	<div id="divMapSection" class="Map2Section SmallMapSection">
                    	<p class="OnFucnStyle" onclick="Images.onOffMap(1);" style="display:none;"><span class="HideText Sprite1"><s:text name="image_manager_expand"/></span></p>
                    	<p class="OffFucnStyle" onclick="Images.onOffMap(0);" style="display:none;"><span class="HideText Sprite1"><s:text name="image_manager_minimize"/></span></p>
                        <div class="MapSection">
                    		<div id="bigMap" style="width: 140px; height: 120px; position: relative;"></div>
                        </div>
                    </div>
					<p class="Text0Style" id="shopImage"></p>
                	<p class="Text1Style" id="staffImage"></p>
                    <p style="padding-bottom: 5px;" class="Text2Style"><s:text name="image_manager_capturetime"/><span id="timeImage"></span></p>
                    <p class="Text3Style ImagePrintingClass">
	                    <a href="javascript:void(0);" class="Sprite1 DownloadPhotoLink"><s:text name="image_manager_downloadimage"/></a>
<!-- 	                    <a href="javascript:void(0)" onclick="Images.deleteImage(this)" class="Sprite1 DeletePhotoLink">Xóa hình ảnh</a> -->
						<a href="javascript:void(0)" style="background-image: url('/resources/images/icon-rotate.png'); background-repeat : no-repeat; padding-left:22px;" onclick="Images.rotateImage(this)"><s:text name="image_manager_rotate"/></a>
						<s:if test="isCTB!=null && isCTB==true">
							<input type="checkbox" id="result" onclick="Images.updateResult();" style="position: absolute; width: 15px;margin-left: 22px;" class="InputTextStyle">
	                    	<label class="LabelStyle" id="lblResult" style="margin-left:40px;color: rgb(0, 153, 204);"><s:text name="image_manager_setup_present"/></label>
                    	</s:if>
                    	<a href="javascript:void(0)" style="background-image: url('/resources/images/icon-printer.png'); background-repeat : no-repeat; padding-left:22px;margin-left: 12px;" onclick="printme()"><s:text name="image_manager_print"/></a>
						<input type="hidden" class="DeletePhotoLink" />
                    </p>
                    <p id="errMsgPopup" class="ErrorMsgStyle" style="display: none"></p>
                </div>
            </div>
            <div class="Clear"></div>
    	</div>
    </div>
</div>
<div id="popup-images-full" style="display:none;">
	<div class="GeneralDialog General2Dialog" id="fullGeneral2Dialog">
      	<div class="DialogPhotoSection FixFloat" id="fullDialogPhotoSection">
        	<div class="PhotoCols1" id="fullPhotoCols1">
              	<div id="listImage" class="DScrollpane">
              		<ul id="listImageUL" class="ResetList PhotoCols2List">
              		</ul>
            	</div>
            </div>
            <div class="PhotoCols2" id="fullPhotoCols2">
            	<div class="PhotoCols1Info1" id="fullPhotoCols1Info1">
                	<p onclick="Images.showImageNextPre(0);" class="LeftStyle Sprite1"></p>
                	<div class="PhotoLarge"><span class="BoxFrame BoxFrame2" id="fullBoxFrame2"><span class="BoxMiddle">
                		<img id="bigImages" class="ImageMapFull ImageFrame4Full" width="752" height="508"/>
                		<img style="display:none; top: 0px;" id="loadingImages" width="50" height="50" src="/resources/images/loading.gif">
                	</span></span></div>
                    <p onclick="Images.showImageNextPre(1);" class="RightStyle Sprite1"></p>
                </div>
                <div class="PhotoCols1Info3">
                	<div id="divMapSection" class="Map2Section SmallMapSection">
                    	<p class="OnFucnStyle" onclick="Images.onOffMap(1);" style="display:none;"><span class="HideText Sprite1"><s:text name="image_manager_expand"/></span></p>
                    	<p class="OffFucnStyle" style="top: 9px;" onclick="Images.onOffMap(0);"><span class="HideText Sprite1"><s:text name="image_manager_minimize"/></span></p>
                        <div class="MapSection">
                    		<div id="bigMap" style="width: 140px; height: 115px; position: relative;"></div>
                        </div>
                    </div>
					<p class="Text0Style" id="shopImage"></p>
                	<p class="Text1Style" id="staffImage"></p>
                    <p style="padding-bottom: 5px;" class="Text2Style"><s:text name="image_manager_capturetime"/><span id="timeImage"></span></p>
                    <p class="Text3Style ImagePrintingClassFull">
	                    <a href="javascript:void(0);" class="Sprite1 DownloadPhotoLink"><s:text name="image_manager_downloadimage"/></a>
<!-- 	                    <a href="javascript:void(0)" onclick="Images.deleteImage(this)" class="Sprite1 DeletePhotoLink">Xóa hình ảnh</a> -->
						<a href="javascript:void(0)" style="background-image: url('/resources/images/icon-rotate.png'); background-repeat : no-repeat; padding-left:22px;" onclick="Images.rotateImage(this)"><s:text name="image_manager_rotate"/></a>
						<s:if test="isCTB!=null && isCTB==true">
							<input type="checkbox" id="result" onclick="Images.updateResult();" style="position: absolute; width: 15px;margin-left: 22px;" class="InputTextStyle">
	                    	<label class="LabelStyle" id="lblResult" style="margin-left:40px;color: rgb(0, 153, 204);"><s:text name="image_manager_setup_present"/></label>
                    	</s:if>
                    	<a href="javascript:void(0)" style="background-image: url('/resources/images/icon-printer.png'); background-repeat : no-repeat; padding-left:22px;margin-left: 12px;" onclick="printme(null,1)"><s:text name="image_manager_print"/></a>
						<input type="hidden" class="DeletePhotoLink" />
                    </p>
                    <p id="errMsgPopup" class="ErrorMsgStyle" style="display: none"></p>
                </div>
            </div>
            <div class="Clear"></div>
    	</div>
    </div>
</div>
<div id ="orderImageDiv" style="visibility: hidden;width:616px" >
	<div id="orderImagePopup" class="easyui-dialog" title="Tải hình ảnh" data-options="closed:true,modal:true">
		<div class="UploadMediaSection">
			<div class="MediaBox ">
			    <label class="LabelStyle"><s:text name="image_manager_cttb"/></label>
			    <input type="text" id="cttbOrder" class="InputTextStyle InputText2Style" title="CTTB1,CTTB2,CTTB3"/>
				<label class="LabelStyle"><s:text name="image_manager_cust"/></label>
			    <input type="text" id="customerOrder" class="InputTextStyle InputText2Style" title="KH1,KH2,KH3"/>
			    <label class="LabelStyle Label2Style"><s:text name="image_manager_capturedate"/></label>
			  	<input type="text" class="InputTextStyle InputText2Style" id="fromDateOrder"	maxlength="10" alt="<s:text name="image_manager_choosedate"/>" value="<s:property value="fromDate"/>" style="width:70px;"/>
			    <span class="TempStyle">-</span>
			    <input type="text" class="InputTextStyle InputText2Style" id="toDateOrder"	maxlength="10" alt="<s:text name="image_manager_choosedate"/>" value="<s:property value="toDate"/>" style="width:70px;"/>
				<div class="Clear"></div>													
			</div>
			<div class="GeneralForm GeneralNoTPForm">
            	<div class="BtnCenterSection">
                	<button onclick="return Images.exportAlbumEx();" class="BtnGeneralStyle"><s:text name="image_manager_download_album"/></button>
            	</div>                        
            </div>
		</div>
    </div>
</div>
<div>
<iframe id="printf" width="0" height="0" style="display: none" name="printf"></iframe>
</div>
<s:hidden id="curShopId" name="shopId"></s:hidden>
<s:hidden id="shopId" name="shopId"></s:hidden>
<s:hidden id="isCTB" name="isCTB"></s:hidden>
<s:hidden id="staffType" name="staffType"></s:hidden>
<input type="hidden" id="staffId" value='<s:property value="staff.staffId"/>'/>
<input type="hidden" id="roleType" value='<s:property value="staff.channelType.objecType"/>'/>
<script type="text/javascript" src="http://viettelmap.vn/VTMapService/VTMapAPI?api=VTMap&type=main&k=fe8be5338b3467c2820dec3c4e00e3d8"></script>
<script type="text/javascript" >
$(document).ready(function(){ 
	var tmpTime = 0;
	var intervalTime = 0;
	Images.isFirstLoad = true;
	Utils.bindQuicklyAutoSearch();
	/* $('#tuyen').customStyle(); */
	setDateTimePicker('fromDate');
	setDateTimePicker('toDate');
// 	$('#lastSeq').removeAttr('checked');
// 	Images.cbLastSeqChange();
	Images.lstCTTB=new Array();
	Images.lstCTTB_temp=new Array();
	$.getJSON('/images/displayPrograme/getListCTTBbyShopId',function(list){
	/* 	$('#cttb').removeAttr('disabled');
		var html = ''; */
// 		if(list.length >= 2){
// 			html += '<option value="-1" selected>--Tất cả--</option>';
// 			for(var i=0; i < list.length; i++){
// 				Images.lstCTTB.push(Utils.XSSEncode(list[i].displayProgrameId));
// 				Images.lstCTTB_temp.push(Utils.XSSEncode(list[i].displayProgrameId));
// 				html += '<option value="'+ Utils.XSSEncode(list[i].displayProgrameId) +'">'+ Utils.XSSEncode(list[i].displayProgrameCode) +'</option>';  
// 			}
// 			$('#cttb').html(html);
// 			setSelectBoxValue('cttb',-1);
// 			$('#ddcl-cttb').remove();
// 			$('#ddcl-cttb-ddw').remove();
// 			Images.lstCTTB_temp = Images.lstCTTB;
// 			$('#cttb').dropdownchecklist({ 
// 				forceMultiple: true,
// 				firstItemChecksAll: true,
// 				maxDropHeight: 350,
// 				onItemClick:function(checkbox, selector){
// 					var value = Number(checkbox.context.defaultValue);
// 					if (checkbox.context.checked) {
// 						if ( value == -1){
// 							Images.lstCTTB = new Array();
// 							$('#ddcl-cttb-ddw input[type=checkbox]').each(function(){
// 								var valueTmp = Number($(this).val());
// 								if(valueTmp != -1) Images.lstCTTB.push(valueTmp);
// 							}); 
// 						}else{
// 							var index = Images.lstCTTB.indexOf(value);
// 							if (index==-1) Images.lstCTTB.push(value);									
// 						}
// 					}
// 					else{
// 						if (value == -1){
// 							Images.lstCTTB = new Array();
// 						}else{
// 							var index = Images.lstCTTB.indexOf(value);
// 							if (index!=-1) Images.lstCTTB.splice(index,1);	
// 						}
// 					}
// 				}
// 			});
// 		}  else if(list.length ==1 ) {
// 			Images.lstCTTB.push(Utils.XSSEncode(list[0].displayProgrameId));
// 			html += '<option value="'+ Utils.XSSEncode(list[0].displayProgrameId) +'">'+ Utils.XSSEncode(list[0].displayProgrameCode) +'</option>';  
// 			$('#cttb').html(html);
// 			setSelectBoxValue('cttb',Utils.XSSEncode(list[0].displayProgrameId));
// 		} else {
// 			html += '<option value="-2"></option>';
// 			$('#cttb').html(html);
// 			setSelectBoxValue('cttb',-2);
// 			$('#cttb').attr('disabled','disabled');
// 		}

		if($('#roleType').val() == 5) {
			Images.filterSearchAlbum = new Object();
			Images.filterSearchAlbum.shopId = $('#curShopId').val();
			Images.filterSearchAlbum.gsnppId = $('#staffId').val() ;
			//Images.filterSearchAlbum.nvbhId = $('#nvbh').combobox('getValue') ;		
			Images.filterSearchAlbum.tuyen = -1 ;
			Images.filterSearchAlbum.fromDate = $('#fromDate').val().trim();
			Images.filterSearchAlbum.toDate = $('#toDate').val().trim();
			//Images.filterSearchAlbum.customerCode = $('#customerCode').val().trim();
			//Images.filterSearchAlbum.customerNameOrAddress = $('#customerAddress').val().trim() ;
			Images.filterSearchAlbum.lstCTTB = Images.lstCTTB;
		}
		
		var staffType=$('#staffType').val().trim();
	    if(staffType=='5'){
	    	$('#btnSearch').click();
	    }
		
	});
    $("#shop").kendoMultiSelect({
        dataTextField: "shopCode",
        dataValueField: "shopId",
        filter: "contains",
		itemTemplate: function(data, e, s, h, q) {
			var level = data.isLevel;
			if(level == 1) {//VNM
				return '<div class="tree-vnm" node-id="'+Utils.XSSEncode(data.shopId)+'" style="width:280px"><span class="tree-vnm-node"><div style="width: 15px; display: inline-block;"></div></span><span class="tree-vnm-text">'+Utils.XSSEncode(data.shopCode) + ' - ' + Utils.XSSEncode(data.shopName)+'</span></div>';
			} else if(level == 2) {
					return '<div class="tree-vnm" node-id="'+Utils.XSSEncode(data.shopId)+'" style="width:280px"><span class="tree-vnm-node"><div style="width: 15px; display: inline-block;"></div></span><span class="tree-vnm-text">'+Utils.XSSEncode(data.shopCode) + ' - ' + Utils.XSSEncode(data.shopName)+'</span></div>';
			}else if(level == 3){
				return '<div class="tree-mien" node-id="'+Utils.XSSEncode(data.shopId)+'" style="width:280px;"><span class="tree-mien-node"><div style="width: 15px; display: inline-block;"></div></span><span class="tree-mien-text">'+Utils.XSSEncode(data.shopCode) + ' - ' + Utils.XSSEncode(data.shopName)+'</span></div>';
			}else if(level == 4){
				return '<div class="tree-vung" node-id="'+Utils.XSSEncode(data.shopId)+'" style="width:280px"><span class="tree-vung-node"><div style="width: 15px; display: inline-block;"></div></span><span class="tree-vung-text">'+Utils.XSSEncode(data.shopCode) + ' - ' + Utils.XSSEncode(data.shopName)+'</span></div>';
			}else if(level == 5){
				return '<div class="tree-npp" node-id="'+Utils.XSSEncode(data.shopId)+'" style="display:-moz-grid-group"><span class="tree-npp-node"><div style="width: 15px; display: inline-block;"></div></span><span class="tree-npp-text">'+Utils.XSSEncode(data.shopCode) + '-' + Utils.XSSEncode(data.shopName)+'</span></div>';
			} else {
				return '<div class="tree-npp" node-id="'+Utils.XSSEncode(data.shopId)+'" style="display:-moz-grid-group"><span class="tree-npp-node"><div style="width: 15px; display: inline-block;"></div></span><span class="tree-npp-text">'+Utils.XSSEncode(data.shopCode) + '-' + Utils.XSSEncode(data.shopName)+'</span></div>';
			}
		},
		tagTemplate:  '#: data.shopCode #',
        change: function(e) {
        	var lstShop=this.value();
        	var param='';
        	var param1='';
        	for(var i=0;i<lstShop.length;i++){
        		if(lstShop[i] != null && lstShop[i] != ""){
	        		if(i==0){
	        			param+='?lstShop='+lstShop[i];
	        			param1+='?lstShopId='+lstShop[i];
	        		}else{
	        			param+='&lstShop='+lstShop[i];
	        			param1+='&lstShopId='+lstShop[i];
	        		}
	        	}
        	}
        	if(param != "" && param1 != ""){
	        	$('#divStaff').html('<input id="staff" type="text" data-placeholder="<s:text name="image_manager_choose_photographer"/>"  style="width:300px;height: auto;" class="InputTextStyle InputText1Style" />');
	        	$('#divCTTB').html('<select id="cttb" name="LevelSchool" data-placeholder="<s:text name="image_manager_choose_photographer"/>" style="width:180px"></select>');
			/* 	$('#divCttb').html('<input id="cttb" type="text" data-placeholder="Chọn câu lạc bộ"  style="width:300px;height: auto;margin-left:0px" class="InputTextStyle InputText1Style" />'); */
	       		Images.loadComboStaff("/images/getListStaffForShop"+param);
	        //	Images.loadComboCTTB("/images/displayPrograme/getListCTTBbyListShop"+param1);
	        }
        },
        dataSource: {
            transport: {
                read: {
                    dataType: "json",
                    url: "/rest/report/shop/kendo-ui-combobox-ho.json"
                }
            }
        },
        value: [$('#curShopId').val()]
    });
    var shopKendo = $("#shop").data("kendoMultiSelect");
    shopKendo.wrapper.attr("id", "shop-wrapper");
    
    Images.loadComboStaff( "/images/getListStaffForShop");
    Images.loadComboCTTB("/images/displayPrograme/getListCTTBbyListShop");
    $(window).bind('keypress',function(event){
		if($('.fancybox-inner').length!=0){
			if(event.keyCode == keyCodes.ARROW_LEFT || event.keyCode == keyCodes.ARROW_UP){
				Images.showImageNextPre(0);
			}else if(event.keyCode == keyCodes.ARROW_RIGHT || event.keyCode == keyCodes.ARROW_DOWN){
				Images.showImageNextPre(1);
			}
			return false;
		}
		if(event.keyCode == keyCodes.ENTER){
			$("#btnSearch").click();
		}
		return true;
	});
    
    
    //set stype for full dialog
    $('#fullGeneral2Dialog').css('width', $(window).width() - 56);
    $('#fullDialogPhotoSection').css('height', $(window).height() - 74);
    $('#fullPhotoCols1 #listImage').css('height', $(window).height() - 74 - 15 - 11);
    $('#fullPhotoCols2').css('width', $('#fullGeneral2Dialog').width() - $('#fullPhotoCols1').width() - 1);
    $('#fullPhotoCols1Info1').css('width', $('#fullGeneral2Dialog').width() - $('#fullPhotoCols1').width() - 1 - 20);
    $('#fullBoxFrame2').css('width', $('#fullGeneral2Dialog').width() - $('#fullPhotoCols1').width() - 1 - 20);
    $('#fullBoxFrame2').css('width', $('#fullGeneral2Dialog').width() - $('#fullPhotoCols1').width() - 1 - 20);
    $('#fullBoxFrame2').css('height', $(window).height() - 74 - 15 - 122);
    $('#fullDialogPhotoSection #bigImages').css('height', $('#fullDialogPhotoSection').height() - 214 + 77);
    
    
//     $('.k-input').each(function(){
// 		if(!$(this).is(':hidden') && !$(this).is(':disabled')){
// 			$(this).bind('keyup', keyupEnter);
// 		}
// 	});
    
//     function keyupEnter(event){
//     	if(event.keyCode == keyCodes.ENTER){
// 			if(!$('#btnSearch').is(':hidden') && !$('btnSearch').is(':disabled')){
// 				$('#btnSearch').click();
// 			}
// 		}
//     };

 	$('#divTuyen').each(function(){
		if(!$(this).is(':hidden') && !$(this).is(':disabled')){
			$(this).bind('keyup', keyupEnter);
		}  
	});
 
	function keyupEnter(event){
		if(event.keyCode == keyCodes.ENTER){
			if(!$('#btnSearch').is(':hidden') && !$('btnSearch').is(':disabled')){
				$('#btnSearch').click();
			}
		}
	};


});
</script>
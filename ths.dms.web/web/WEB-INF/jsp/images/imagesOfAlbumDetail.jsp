<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@page import="ths.dms.helper.Configuration"%>
<%@page import="ths.dms.web.utils.DateUtil"%>
<h2 class="Title2Style" id="titleAlbum" ><s:property value="titleAlbum"/></h2>
<s:if test="isEmptyImage==false || isEmptyImage==null">
<div class="PhotoFuncSection" style="top: 132px;">
 	<a id="btnDownloadAlbum" href="javascript:void(0)" onclick="Images.exportAlbum(<s:property value="displayPrograme.displayProgrameId"/>)" class="Sprite1 DownloadPhotoLink" ><s:text name="image_manager_download_album"/></a>
 	<a href="javascript:void(0)" onclick="Images.showAlbumDetail();" style="<s:if test="typeGroup==null">border: 2px inset;</s:if>"><img src="/resources/images/icon_detail.png" style="height:17px;margin: 0 2px;"></a>
 	<a href="javascript:void(0)" onclick="Images.showAlbumSelect(2);" style="<s:if test="typeGroup!=null && typeGroup!=1">border: 2px inset;</s:if>"><img src="/resources/images/icon_distributor.png" style="height:17px;margin: 0 2px;"></a>
 	<a href="javascript:void(0)" onclick="Images.showAlbumSelect(1);" style="<s:if test="typeGroup!=null && typeGroup==1">border: 2px inset;</s:if>"><img src="/resources/images/icon_user.png" style="height:17px;margin: 0 2px;"></a>
</div>
</s:if>
<div class="PhotoListSection">
	<ul class="ResetList PhotoGroupList" id="albumContentDetail" >
	<s:iterator value="lstImageOfAlbum" status="status">
		<li>
		<a href="javascript:void(0)"  onclick="Images.showDialogFancy(<s:property value="id"/>)">
			<span class="BoxFrame">
				<span class="BoxMiddle"> 
					<img id="imagesOfAlbulmDetail_<s:property value="id"/>" class="ImageOfAlbum ImageFrame3" src="/resources/images/grey.jpg" data-original="<%=Configuration.getImgServerSOPath()%><s:property value="urlThum"/>?v=<%=DateUtil.getCurrentGMTDate().getTime()%>" width="201" height="144" />
<%-- 						<img id="imagesOfAlbulmDetail_<s:property value="id"/>" class="ImageOfAlbum ImageFrame3" src="<%=Configuration.getImgServerSOPath()%><s:property value="urlThum"/>?v=<%=DateUtil.getCurrentGMTDate().getTime()%>" data-original="<%=Configuration.getImgServerSOPath()%><s:property value="urlThum"/>?v=<%=DateUtil.getCurrentGMTDate().getTime()%>" width="201" height="144" /> --%>
				</span>
			</span>
		</a>
		<p class="Text1Style"><a href="javascript:void(0)" onclick="Images.showDialogFancy(<s:property value="id"/>)"><s:property value="shopCode"/> - <s:property value="customerCode"/> - <s:property value="customerName"/></a></p>
		<p class="Text2Style"><s:property value="createDateStr"/></p>
		</li>
	</s:iterator>
  	</ul>
  	<div class="Clear"></div>
  	<p class="BoxLoading" id="divOverlayAddImage" style="display:none;"><img src="/resources/images/loadingbig.gif" width="70" height="70" alt="Loading"/></p>
</div>
<input type="hidden"  id="displayProgrameCodeHid"  value = "<s:property value="displayPrograme.displayProgrameCode"/>" />

<script type="text/javascript" >
$(document).ready(function(){
	$(".ImageOfAlbum").lazyload({
	    event: "scrollstop"
	});
	Images.checkScrollImageOfAlbum = 1;
	if(!Images.filterImageOfAlbum.isEndImgOfAlbum && Images.checkScrollImageOfAlbum == 1 && $(window).scrollTop() + $(window).height() == $(document).height()){
		Images.checkScrollImageOfAlbum = 0;
		var typeGroup = $('#typeGroup').val();
		var id=$('#id').val();
		if(typeGroup!='' && id!='') Images.addImageOfAlbumSelect(typeGroup,id);
		else Images.addImageOfAlbumSelect();
	}
 	$(window).scroll(function(){
 		if(!Images.filterImageOfAlbum.isEndImgOfAlbum && Images.checkScrollImageOfAlbum == 1 && $(window).scrollTop() + $(window).height() == $(document).height()){
 			Images.checkScrollImageOfAlbum = 0;
 			var typeGroup = $('#typeGroup').val();
 			var id=$('#id').val();
 			if(typeGroup!='' && id!='') Images.addImageOfAlbumSelect(typeGroup,id);
 			else Images.addImageOfAlbumSelect();
 		}
 	});
 	if($('#typeGroup').length!=0){
 		$('#titleAlbum').html('<s:text name="image_manager_search_error_10"/>'+$('#titleAlbum').html());
 	}else{
 		$('#btnDownloadAlbum').css('visibility','hidden');
 	}
});
</script>
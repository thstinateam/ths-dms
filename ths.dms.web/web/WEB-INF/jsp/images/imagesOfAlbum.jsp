<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@page import="ths.dms.helper.Configuration"%>
<%@page import="ths.dms.web.utils.DateUtil"%>
<div class="CtnTwoColsSection">
	<div class="SidebarSection">
		<div class="SidebarInSection">
	        <div class="PhotoGroupSection" style="border-right: 1px inset;">
	            <ul class="ResetList PhotoGroupList" id="lstAlbumContent">
	                <s:iterator value="lstAlbum" status="status">
		                <li>
		                    <a href="javascript:void(0)" class="PhotoThumbGroup" onclick="Images.changeGroup('<s:property value="typeGroup"/>','<s:property value="customerId"/>')">
			                    <span class="BoxFrame">
			                    	<span class="BoxMiddle">
			                    	<img id="imagesOfAlbum_<s:property value="id"/>" width="142" height="102" class="ImageAlbum ImageFrame2"  src="/resources/images/grey.jpg" data-original="<%=Configuration.getImgServerSOPath()%><s:property value="urlThum"/>?v=<%=DateUtil.getCurrentGMTDate().getTime()%>">
			                    	</span>
			                    </span>
		                    </a>
		                    <p class="Text3Style"><a href="javascript:void(0)" onclick="Images.changeGroup('<s:property value="typeGroup"/>','<s:property value="customerId"/>')">
		                    	<s:property value="customerCode"/> - <s:property value="customerName"/> (<s:property value="countImg"/>)</a></p>
		                </li>
	                </s:iterator>
	        	</ul>
	        </div>
	    </div>
	</div>
	<div class="ContentSection">
		<div class="ReportCtnSection" id="albumContent">
			<tiles:insertTemplate template="/WEB-INF/jsp/images/imagesOfAlbumDetail.jsp"></tiles:insertTemplate>
			<s:hidden id="typeGroup" name="typeGroup"></s:hidden>
			<s:hidden id="id" name="id"></s:hidden>
	    </div>
	</div>
</div>
<script type="text/javascript" >
$(document).ready(function(){
	Images.lazyLoadImages(".ImageAlbum", "#content");
	Images.filterAlbum.checkScrollAlbum = 1;
	$('.PhotoGroupSection').scroll(function(){
		if (!Images.filterAlbum.isEndAlbum && Images.filterAlbum.checkScrollAlbum ==1 && $(this).outerHeight() == ($(this).get(0).scrollHeight - $(this).scrollTop())){
			Images.filterAlbum.checkScrollAlbum == 0;
			Images.addListAlbumSelect();
		}
	});
	Images.resize();
});
</script>
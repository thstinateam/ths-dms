<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<input type="hidden" id="function_code" value="<s:property value="systimestamp"/>" />
<s:set id="downloadToken" value="%{generateReportToken()}"></s:set>
<input type="hidden" id="report_token" value="<s:property value="#downloadToken"/>" />
<s:set id="downloadToken" value="%{putSessionValue(systimestamp, #downloadToken)}"></s:set>
<style>
	#selectImageType .ui-dropdownchecklist-text,
	#selectImageType .ui-dropdownchecklist-selector {
    	width: 196px !important;
	}
</style>
<dl class="Dl3Style">
	<dt class="ClearLeft LabelTMStyle" style="margin:0;">
		<label class="LabelStyle Label1Style">Mã đơn vị <span class="ReqiureStyle">*</span></label>
	</dt>
    <dd>
	 	<s:if test="Npp=='NPP'">
	 		<input id="shop" style="width:226px;" disabled="disabled" class="InputTextStyle InputText1Style" />
	 	</s:if>
	 	<s:else>
	 		<input id="shop" style="width:226px;" class="InputTextStyle InputText1Style" />
	 	</s:else>
    </dd>
    <dt><label class="LabelStyle Label1Style"></label></dt>
	<dd>
		<div class="BoxSelect BoxSelect1"></div>
	</dd>
<%-- 	<tiles:insertTemplate template="/WEB-INF/jsp/report/chuKyG.jsp" /> --%>
	<dt class="ClearLeft LabelTMStyle" style="margin:0;">
		<label class="LabelStyle Label1Style">Từ ngày <span class="ReqiureStyle">*</span></label>
	</dt>
	<dd>
		<input id="fromDate" type="text" class="InputTextStyle InputText2Style" value="<s:property value='fromDate' />" />
	</dd>
	<dt class="LabelTMStyle" style="margin:0;">
		<label class="LabelStyle Label1Style">Đến ngày <span class="ReqiureStyle">*</span></label>
	</dt>
	<dd>
		<input id="toDate" type="text" class="InputTextStyle InputText2Style" value="<s:property value='toDate' />" />
	</dd>
</dl>
<div class="Clear"></div>
<div class="BtnCenterSection">
	<button class="BtnGeneralStyle" id="btnReport" onclick="HoReport.report1D1DSPSTN();">Xuất báo cáo</button>
</div>
<p id="errMsg" class="ErrorMsgStyle SpriteErr" style="display: none"></p>
<s:hidden id="curShopId" name="curShopId"></s:hidden>
<script type="text/javascript">
	$(document).ready( function() {
// 		$('.MySelectBoxClass').each(function() {
// 			$(this).customStyle();
// 		});
		//Khoi tao cac control kieu ngay
		setDateTimePicker('fromDate');
		setDateTimePicker('toDate');
		//Khoi tao cac control easy ui
		ReportUtils.loadComboTree('shop', 'shopId', $('#curShopId').val(), function(shopId){
			//Xu ly load lai combobox khuyen mai
		});
		//Khoi tao cac control kendo ui
		//HoReport.changePromotionProgramByShop(StatusType.ACTIVE, $('#curShopId').val());
	});
</script>
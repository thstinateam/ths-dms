<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<input type="hidden" id="function_code" value="<s:property value="systimestamp"/>" />
<s:set id="downloadToken" value="%{generateReportToken()}"></s:set>
<input type="hidden" id="report_token" value="<s:property value="#downloadToken"/>" />
<s:set id="downloadToken" value="%{putSessionValue(systimestamp, #downloadToken)}"></s:set>
<style>
	#selectImageType .ui-dropdownchecklist-text,
	#selectImageType .ui-dropdownchecklist-selector {
    	width: 196px !important;
	}	
</style>
<dl class="Dl3Style">
	<dt class="ClearLeft LabelTMStyle" style="margin:0;">
		<label class="LabelStyle Label1Style">Mã đơn vị <span class="ReqiureStyle">*</span></label>
	</dt>
    <dd>
	 	<s:if test="Npp=='NPP'">
	 		<input id="shop" style="width:226px;" disabled="disabled" class="InputTextStyle InputText1Style" />
	 	</s:if>
	 	<s:else>
	 		<input id="shop" style="width:226px;" class="InputTextStyle InputText1Style" />
	 	</s:else>
    </dd>
	<tiles:insertTemplate template="/WEB-INF/jsp/report/chuKyG.jsp" />
	<dt class="LabelTMStyle" style="margin:0;">
		<label class="LabelStyle Label1Style"><s:text name="catalog.categoryvn"/></label>
	</dt>
	<dd>
		<input id="category" type="text" data-placeholder="Tất cả" style="width:215px; height: auto;" class="InputTextStyle InputText1Style" />
	</dd>
	<dt><label class="LabelStyle Label1Style"><s:text name="catalog.categoryvn.child"/> <span class="ReqiureStyle">*</span></label></dt>
	<dd id="ddSubcat">
		<input id="categoryChild" type="text" data-placeholder="Tất cả" style="width:215px; height: auto;" class="InputTextStyle InputText1Style" />
	</dd>
</dl>
<div class="Clear"></div>
<div class="BtnCenterSection">
	<button class="BtnGeneralStyle" id="btnReport" onclick="HoReport.reportASOForExcel();">Xuất báo cáo</button>
</div>
<p id="errMsg" class="ErrorMsgStyle SpriteErr" style="display: none"></p>
<s:hidden id="curShopId" name="curShopId"></s:hidden>
<script type="text/javascript">
	$(document).ready( function() {
		$('.MySelectBoxClass').each(function() {
			$(this).customStyle();
		});
		//Khoi tao cac control easy ui
		ReportUtils.loadComboTree('shop', 'shopId', $('#curShopId').val(), function(shopId) {
			//Xu ly load lai combobox khuyen mai
		});
		//Khoi tao cac control kendo ui
		$("#category").kendoMultiSelect({
			placeholder: "Chọn tất cả",
			dataTextField: "productInfoCode",
	        dataValueField: "id",
	        filter: "contains",
			itemTemplate: function (data, e, s, h, q) {
				return '<div node-id="'+Utils.XSSEncode(data.id)+'" style="width:230px"><span><div style="width: 15px; display: inline-block;"></div></span><span>'+Utils.XSSEncode(data.productInfoCode) + ' - ' + Utils.XSSEncode(data.productInfoName)+'</span></div>';
			},
			tagTemplate: '#: data.productInfoCode #',
	        change: function(e) {
	        	$('#ddSubcat').html('<input id="categoryChild" type="text" data-placeholder="Tất cả" style="width:215px; height: auto;" class="InputTextStyle InputText1Style" />').show();
	        	//Lay param
	        	var catIdStr = '-2';
	        	var lstCat = $('#category').data("kendoMultiSelect").dataItems();
	        	if (lstCat != null && lstCat != undefined && lstCat.length > 0) {
	        		for (var i = 0, size = lstCat.length; i < size; i++) {
	        			catIdStr = catIdStr + ',' + lstCat[i].id;
	        		}
	        	}
	        	catIdStr = catIdStr.replace(/-2,/g, '').replace(/-2/g, '');
	        	//Khoi tao cac control kendo ui
	    		$("#categoryChild").kendoMultiSelect({
	    			placeholder: "Chọn tất cả",
	    			dataTextField: "productInfoCode",
	    	        dataValueField: "id",
	    	        filter: "contains",
	    	        animation: false,
	    			itemTemplate: function (data, e, s, h, q) {
	    				return '<div node-id="'+Utils.XSSEncode(data.id)+'" style="width:230px"><span><div style="width: 15px; display: inline-block;"></div></span><span>' + Utils.XSSEncode(data.productInfoCode) + ' - ' + Utils.XSSEncode(data.productInfoName) + '</span></div>';
	    			},
	    			tagTemplate: '#: data.productInfoCode #',
	    	        change: function(e) {
	    	        },
	    	        dataSource: {
	    	            transport: {
	    	                read: {
	    	                    dataType: "json",
	    	                    url: '/rest/report/product-info-sub-cat/kendoui-combobox/' + activeType.RUNNING + '/' + catIdStr + '.json'
	    	                }
	    	            }
	    	        }
	    	    });
	        },
	        dataSource: {
	            transport: {
	                read: {
	                	dataType: "json",
	                    url: '/rest/report/product-info-cat/kendoui-combobox/' + activeType.RUNNING + '/' + Number($('#curShopId').val()) + '.json' 
	                }
	            }
	        }
	    });
		
		//Khoi tao cac control kendo ui
		$("#categoryChild").kendoMultiSelect({
			placeholder: "Chọn tất cả",
			dataTextField: "productInfoCode",
	        dataValueField: "id",
	        filter: "contains",
	        animation: false,
			itemTemplate: function (data, e, s, h, q) {
				return '<div node-id="'+Utils.XSSEncode(data.id)+'" style="width:230px"><span><div style="width: 15px; display: inline-block;"></div></span><span>' + Utils.XSSEncode(data.productInfoCode) + ' - ' + Utils.XSSEncode(data.productInfoName) + '</span></div>';
			},
			tagTemplate: '#: data.productInfoCode #',
	        change: function(e) {
	        },
	        dataSource: {
	            transport: {
	                read: {
	                    dataType: "json",
	                    url: '/rest/report/product-info-sub-cat/kendoui-combobox/' + activeType.RUNNING + '/.json'
	                }
	            }
	        }
	    });
	});
</script>
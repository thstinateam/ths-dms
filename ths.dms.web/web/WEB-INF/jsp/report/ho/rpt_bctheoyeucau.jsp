<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<input type="hidden" id="function_code" value="<s:property value="systimestamp"/>" />
<s:set id="downloadToken" value="%{generateReportToken()}"></s:set>
<input type="hidden" id="report_token" value="<s:property value="#downloadToken"/>" />
<s:set id="downloadToken" value="%{putSessionValue(systimestamp, #downloadToken)}"></s:set>
<div class="ModuleList3Form">
	<dl class="Dl3Style">
		<dt><label class="LabelStyle Label1Style">Mẫu báo cáo <span class="ReqiureStyle">*</span></label></dt>
		<dd>
            <div class="BoxSelect BoxSelect1" id="typeDiv"> 
            	<select id="type" class="MySelectBoxClass" >  
           			<option value="1">Báo cáo NVBH hoạt động</option>                                      	
           			<option value="2">Báo cáo đơn hàng cho KH</option>
         	   </select> 
            </div>
		</dd>
		<!-- <dt class="ClearLeft"> -->
		<dt><label class="LabelStyle Label1Style">Ngày <span class="ReqiureStyle">*</span></label></dt>
		<dd><input id="fDate" class="InputTextStyle InputText2Style"></dd>
	</dl>
	
 	<div class="Clear"></div> 
 	<div class="BtnCenterSection">                	
 		<button id="btnSearch" class="BtnGeneralStyle Sprite2" onclick="return HoReport.exportGeneral();"><span class="Sprite2">Xuất báo cáo</span></button>                	
 	</div>
 	<p id="errMsg" class="ErrorMsgStyle" style="display: none"></p>               
</div>            
<s:hidden id="shopId"></s:hidden>
<s:hidden id="curShopId" name="shopId"></s:hidden>
<script type="text/javascript">
$(document).ready(function(){	
	$('.MySelectBoxClass').customStyle();
	setDateTimePicker('fDate');	
	ReportUtils.setCurrentDateForCalendar('fDate');
});
</script>
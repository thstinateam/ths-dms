<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<dt><label class="LabelStyle Label1Style">Chu kỳ <span class="ReqiureStyle">*</span></label></dt>
<dd>
	<div class="BoxSelect BoxSelect1">
    	<select class="MySelectBoxClass" id="cycle">
			<s:iterator value="lstCycle">
				<option value='<s:property value="cycleId"/>'><s:property value="cycleName"/></option>
			</s:iterator>
    	</select>
	</div>
</dd>
<script type="text/javascript">
$(function(){
	General.mapCycle = new Map();
	var idDefault = -1;
	<s:iterator value="lstCycle">
		var obj = new Object();
		obj.cycleId = '<s:property value="cycleId"/>';
		obj.cycleCode = '<s:property value="cycleCode"/>';
		obj.cycleName = '<s:property value="cycleName"/>';
		obj.num = '<s:property value="num"/>';
		obj.year = '<s:property value="year"/>';
		obj.beginDate = '<s:property value="beginDateStr"/>';
		obj.endDate = '<s:property value="endDateStr"/>';
		SuperviseCustomer.mapCycle.put(obj.cycleId, obj);
		idDefault = obj.cycleId;
	</s:iterator>
	$('#cycle').val(idDefault).change();//setDefault là giá trị cuối cùng
});
function onChangeCycle() {
	var obj = General.mapCycle.get($('#cycle').val());
	if (obj != null) {
		$('#fDate').val(obj.beginDate);
		$('#tDate').val(obj.endDate);
	}
}
</script>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<input type="hidden" id="function_code" value="<s:property value="systimestamp"/>" />
<s:set id="downloadToken" value="%{generateReportToken()}"></s:set>
<input type="hidden" id="report_token" value="<s:property value="#downloadToken"/>" />
<s:set id="downloadToken" value="%{putSessionValue(systimestamp, #downloadToken)}"></s:set>
<div class="BreadcrumbSection">
       	<ul class="ResetList FixFloat BreadcrumbList">
           <li class="Sprite1"><a href="javascript:void(0);"><s:text name="catalog_sales_brand_catalogy"/></a></li>
           <li><span><s:text name="jsp.product.ds.price-manage"/></span></li>
    	</ul>
</div>
<div class="CtnOneColSection">
	<div class="ContentSection">
		<div class="ToolBarSection">
			<div class="SearchSection GeneralSSection">
				<h2 class="Title2Style"><s:text name="price.info.search"/></h2>
				<div class="SearchInSection SProduct1Form" id="divSearchForm">

				<label class="LabelStyle Label1Style"><s:text name="price.menu1"/></label>
				<input id="txtProductCode" type="text" class="InputTextStyle InputText1Style" maxlength="100" />

				<label class="LabelStyle Label1Style"><s:text name="price.menu2"/></label>
				<input id="txtProductName" type="text" class="InputTextStyle InputText1Style" maxlength="100"/>

				<label class="LabelStyle Label1Style"><s:text name="price.menu3"/></label>
				<div class="BoxSelect BoxSelect2" id="ddlProductInfoDiv">
					<select class="MySelectBoxClass" id="ddlProductInfo">
						<option value="-2" selected="selected"><s:text name="price_combo_all"/></option>
						<s:iterator value="lstProductInfo" var="obj" >
							<option value="<s:property value="#obj.id" />"><s:property value="#obj.productInfoName" /></option>
						</s:iterator>
					</select>                            
				</div>
                <div class="Clear"></div>
				<label class="LabelStyle Label1Style"><s:text name="price.menu4"/></label>
				<div class="BoxSelect BoxSelect2" id="ddlTypeShopDiv">
					<select class="MySelectBoxClass" id="ddlTypeShop">
					<option value="-2" selected="selected"><s:text name="price_combo_all"/></option>
						<s:iterator value="lstShopType"  var="objs" > 
						<option value="<s:property value="#objs.id" />"><s:property value="#objs.name" /></option> 
						</s:iterator> 
					</select>                            
				</div>
					
				<label class="LabelStyle Label1Style"><s:text name="price.menu5"/></label>
				<input id="txtShopCode" type="text" class="InputTextStyle InputText1Style" maxlength="50"/>

				<label class="LabelStyle Label1Style"><s:text name="price.menu6"/></label>
				<div class="BoxSelect BoxSelect2" id="ddlTypeCustomerDiv">
					<select class="MySelectBoxClass" id="ddlTypeCustomer">
						<option value="-2" selected="selected"><s:text name="price_combo_all"/></option>
						<s:iterator value="lstChannelType" var="obj" >
							<option value="<s:property value="#obj.id" />"><s:property value="#obj.channelTypeName" /></option>
						</s:iterator>
					</select>                            
				</div>

				 <div class="Clear"></div>

				<label class="LabelStyle Label1Style"><s:text name="price.menu7"/></label>
				<input id="txtCustomerCode" type="text" class="InputTextStyle InputText1Style" maxlength="100"/>
				
				<label class="LabelStyle Label1Style"><s:text name="price.menu8"/></label>
				<input id="txtFromDate" type="text" class="InputTextStyle InputText1Style vinput-date"/> 

				<label class="LabelStyle Label1Style"><s:text name="price.menu9"/></label>
				<input id="txtToDate" type="text" class="InputTextStyle InputText1Style vinput-date"/>
				<div class="Clear"></div>

				 <label class="LabelStyle Label1Style"><s:text name="price.menu10"/></label>
				<div class="BoxSelect BoxSelect2" id="ddlStatusDiv">
					<select class="MySelectBoxClass" id="ddlStatus">
						<option value="2"><s:text name="action.status.waiting"/></option>
						<option value="1" ><s:text name="action.status.name"/></option>
						<option value="0"><s:text name="pause.status.name"/></option>
						<option value="-2" selected="selected"><s:text name="price_combo_all"/></option>
					</select>                            
				</div> 
				<div class="Clear"></div>
				<div class="BtnCenterSection">
						<button id="btnSearchPrice" class="BtnGeneralStyle cmsdefault" onclick="return PriceManager.searchProductPrice();"><s:text name="price.btn.search"/></button>
					</div>
					<div class="Clear"></div>
				</div>
			</div>
			<p id="errMsgPriceManager" style="display: none;" class="ErrorMsgStyle"></p>
			<p id="successMsgPriceManager" class="SuccessMsgStyle" style="display:none;"></p>
			<div class="Clear"></div>
		</div>
		<div class="GeneralCntSection">
			<h2 class="Title2Style"><s:text name="price.list.price"/></h2>
			<div class="GridSection" id="productPriceDgDiv">
				 <table id="dgProductPrice"></table>
			</div>
			<div class="Clear"></div>
			<p id="errMsgImportPrice" style="display: none;" class="ErrorMsgStyle"></p>
			<p id="successMsgImportPrice" class="SuccessMsgStyle" style="display:none;"></p>
			<div class="Clear"></div>
			<div class="GeneralForm GeneralNoTP1Form" style="width:auto;">
				<button class="BtnGeneralStyle" id="btnExportPrice" style="float:right;" onclick="return PriceManager.exportBySearchPrice();"><s:text name="price_export_excel"/></button>
                <div id="group_edit_showBtnFooterGrid" class="Func1Section cmsiscontrol">
					<button class="BtnGeneralStyle changewarningPrdpr" style="float:right;" id="btnChangeDelete" onclick = "return PriceManager.updateChangeWaittingPrice(PriceManager._flagChangeWaitting.isdelete);"><s:text name="price_delete"/></button>
					<button class="BtnGeneralStyle changewarningPrdpr" style="float:right;" id="btnChangeRunning" onclick = "return PriceManager.updateChangeWaittingPrice(PriceManager._flagChangeWaitting.isrunning);"><s:text name="price_approve"/></button>
		       		<button class="BtnGeneralStyle" id="btnImportPrice" style="float:right;" onclick="return PriceManager.importPriceProduct();"><s:text name="price_import_excel"/></button>
		       		<form style="float:right; width: 345px;" action="/price-manage/importPriceProduct" name="importFrmPrice" id="importFrmPrice" method="post" enctype="multipart/form-data">
	                	<p class="DownloadSFileStyle DownloadSFile2Style">
				        	<a href="javascript:void(0)" id="downloadTemplate" class="Sprite1" onclick="PriceManager.downloadImportPriceTemplateFile();"><s:text name="price.dowload.exc.template"/></a>
				        </p>
						<input type="hidden" id="importTokenVal" name="token" value='<s:property value="token"/>'>
						<input type="file" class="InputFileStyle InputText1Style" style="width: 120px;" size="20" name="excelFile" id="excelFileFormPrice" onchange="previewImportExcelFile(this,'importFrmPrice');"> 
						<div class="FakeInputFile">
							<input type="text" id="fakefilepc" style="width: 195px;" class="InputTextStyle" />
						</div>
					</form>
                </div>
                <div class="Clear"></div>
            </div>
		</div>
		<div class="Clear" style="height: 20px;"></div>
	</div>
</div>
<div class="ContentSection" style="visibility: hidden;">
	<tiles:insertTemplate template="/WEB-INF/jsp/sale-product/price/dialogSearchShopByCMS.jsp" />
</div>
<div class="ContentSection" style="visibility: hidden;">
	<tiles:insertTemplate template="/WEB-INF/jsp/sale-product/price/dialogChangePriceManager.jsp" />
</div>
<script type="text/javascript">
	var firstLoadPage = true;
	$(document).ready(function() {
		$('#ddlTypeShopDiv, #ddlTypeCustomerDiv, #ddlStatusDiv, #ddlProductInfoDiv').bind('keyup',function(event){
			if (event.keyCode == keyCodes.ENTER) {
				$('#btnSearchPrice').click();
			}
		});
		$('#divSearchForm input').bind('keyup', function(event) {
			if (event.keyCode == keyCodes.ENTER) {
				$('#btnSearchPrice').click();
			}
		});
		//$('#downloadTemplatePrice').attr('href', excel_template_path + 'price/Bieu_Mau_Import_Gia_San_Pham.xls');
		$('#txtFromDate, #txtToDate').width($('#txtFromDate').width()-22);
		$('#txtProductCode').bind('keyup', function(event) {
			if (event.keyCode == keyCode_F9) {
				VCommonJS.showDialogSearch2({
					params : {
						status : 1,
						longType: $('#ddlProductInfo').val()
					},
					inputs : [
				        {id:'code', maxlength:50, label:'<s:text name="price_product_code"/>'},
				        {id:'name', maxlength:250, label:'<s:text name="price_product_name"/>'}
				    ],
				    url : '/commons/product-searchAllProduct',
				    columns : [[
				        {field:'productCode', title:'<s:text name="price_product_code"/>', align: 'left', width: 110, sortable: false, resizable: false, formatter: function(value, row, index) {
				        	return Utils.XSSEncode(value);         
				        }},
				        {field:'productName', title:'<s:text name="price_product_name"/>', align: 'left', width: 200, sortable: false, resizable: false, formatter: function(value, row, index) {
				        	return Utils.XSSEncode(value);         
				        }},
				        {field:'choose', title:'', align: 'center', width:40, sortable: false, resizable: false, formatter:function(value,row,index) {
				            return '<a href="javascript:void(0)" onclick="PriceManager.fillTxtProductCodeByF9(\''+Utils.XSSEncode(row.productCode)+'\',\''+Utils.XSSEncode(row.productName)+'\');"><s:text name="price_choose"/></a>';         
				        }}
				    ]]
				});
			} else if (event.keyCode == keyCode_DELETE) {
				$('#txtProductCode').val('');
			}
		});
		$('#txtShopCode').bind('keyup', function(event) {
			if (event.keyCode == keyCode_F9) {
				VCommonJS.showDialogSearch2({
					params : {
						status : 1
					},
					inputs : [
				        {id:'code', maxlength:50, label:'<s:text name="price_unit_code"/>'},
				        {id:'name', maxlength:250, label:'<s:text name="price_unit_name"/>'},
				    ],
				    url : '/commons/search-shop-show-list',
				    columns : [[
				        {field:'shopCode', title:'<s:text name="price_unit_code"/>', align: 'left', width: 110, sortable: false, resizable: false, formatter: function(value, row, index) {
							return Utils.XSSEncode(value);
						}},
				        {field:'shopName', title:'<s:text name="price_unit_name"/>', align: 'left', width: 200, sortable: false, resizable: false, formatter: function(value, row, index) {
							return Utils.XSSEncode(value);
						}},
				        {field:'choose', title:'', align: 'center', width:40, sortable: false, resizable: false, formatter:function(value,row,index) {
				            return '<a href="javascript:void(0)" onclick="PriceManager.fillTxtShopCodeByF9(\''+row.shopCode+'\');"><s:text name="price_choose"/></a>';         
				        }}
				    ]]
				});
			}
		});
		$('#dgProductPrice').datagrid({
			data: [],
			//pagination : true,
	        rownumbers : true,
	        //pageNumber : 1,
	        checkOnSelect: false,
			selectOnCheck: false,
	        scrollbarSize: 0,
	        autoWidth: true,
	        //pageList: [10, 20, 50],
	        autoRowHeight : true,
	        fitColumns : true,
			columns:[[
				{field: 'cbxWarningPrdPr', title: '<input type = "checkbox" id = "ckallPrdPr">', width: 40, align: 'center', formatter: function(value, row, index) {
					return '';
				}},
				{field: 'shopTypeName', title: price_grid_column1, width: 60, sortable: true, align: 'left', formatter: function (value, row, index) {
					return '';
				}},  
			    {field: 'shopText', title: price_grid_column2, width: 110, sortable: true, align: 'left', formatter: function(value, row, index) {
			    	return '';
			    }},
			    {field: 'customerTypeName', title: price_grid_column3, width: 60, sortable: true, align: 'left', formatter: function(value, row, index) {
			    	return '';
			    }},
			    {field: 'customerName', title: price_grid_column4, width: 110, sortable: true, align: 'left', formatter: function(value, row, index) {
			    	return '';
			    }},
			    {field: 'productCode', title: price_grid_column5, width: 50, sortable: true, align: 'left', formatter: function(value, row, index) {
			    	return '';
			    }},
			    {field: 'productName', title: price_grid_column6, width: 110, sortable: true, align: 'left', formatter: function(value, row, index) {
			    	return '';
			    }},
			    {field: 'fromDateStr', title: price_grid_column7, width: 50, sortable: true, align: 'center', formatter: function(value, row, index) {
			    	return '';
			    }},
			    {field: 'toDateStr', title: price_grid_column8, width: 50, sortable: true, align: 'center', formatter: function(value, row, index) {
			    	return '';
			    }},
			    {field: 'pricePackage', title: price_grid_column13, width: 60, sortable: true, align: 'right', formatter: function(value, row, index) {
			    	return '';
			    }},
			    {field: 'pricePackageNotVAT', title:price_grid_column14, width: 60, sortable: true, align: 'right', formatter: function(value, row, index) {
			    	return '';
			    }},
			    {field: 'priceNotVAT', title: price_grid_column9, width: 50, sortable: true, align: 'right', formatter: function(value, row, index) {
			    	return '';
			    }},
			    {field: 'price', title: price_grid_column10, width: 50, sortable: true, align: 'right', formatter: function(value, row, index) {
			    	return '';
			    }},
			    {field: 'vat', title: price_grid_column11, width: 30, sortable: true, align: 'right', formatter: function(value, row, index) {
			    	return '';
			    }},
			    {field: 'status', title: price_grid_column12, width:60, sortable: true, align: 'left', formatter: function(value, row, index){
			    	return '';
			    }},
			    {field: 'change', title: '', width:40, align: 'center', formatter: function(value, row, index) {
			    	return '';
			    }}
			]],
	        onLoadSuccess :function(data){
	        	$('.datagrid-header-rownumber').html('<s:text name="price_stt"/>');
				Utils.updateRownumWidthAndHeightForDataGrid('dgProductPrice');
				$('#dgProductPrice').datagrid("hideColumn", "cbxWarningPrdPr");
				$('#dgProductPrice').datagrid("hideColumn", "change");
				$('.changewarningPrdpr').hide();
	        }
		});
		//Forcus khi moi vao trang
		$('#txtProductCode').focus();
	});
</script>

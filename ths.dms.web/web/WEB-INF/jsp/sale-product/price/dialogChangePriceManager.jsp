<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<div id="changePriceDialog" class="easyui-dialog" title="" data-options="closed:true, modal:true" style="width:670px;height:auto;">
	<div class="PopupContentMid2">
       	<div class="GeneralForm Search1Form" style="padding-left: 20px;">
  			<div class="Clear" style="height: 20px;"></div>

			     <label class="LabelStyle Label2Style"><s:text name="price.menu1"/></label>
            <input id="txtProductCodeDl" readonly="readonly" disabled="disabled" type="text" class="InputTextStyle InputText1Style" style="width: 196px;" maxlength="50"/>
            <label class="LabelStyle Label2Style"><s:text name="price.menu2"/></label>
            <input id="txtProductNameDl" readonly="readonly" disabled="disabled" type="text" class="InputTextStyle InputText1Style" style="width: 196px;" maxlength="50"/>

            <div class="Clear"></div>

            <label class="LabelStyle Label2Style"><s:text name="price_grid_column1"/></label>
            <input id="txtShopTypeNameDl" readonly="readonly" disabled="disabled" type="text" class="InputTextStyle InputText1Style" style="width: 196px;" maxlength="100"/>
            <label class="LabelStyle Label2Style"><s:text name="price_grid_column2"/></label>
            <input id="txtShopCodeDl" readonly="readonly" disabled="disabled" type="text" class="InputTextStyle InputText1Style" style="width: 196px;" maxlength="100"/>

            <div class="Clear"></div>

            <label class="LabelStyle Label2Style"><s:text name="price_grid_column3"/></label>
            <input id="txtCustomerTypeNameDl" readonly="readonly" disabled="disabled" type="text" class="InputTextStyle InputText1Style" style="width: 196px;" maxlength="100"/>
             <label class="LabelStyle Label2Style"><s:text name="price_grid_column4"/></label>
            <input id="txtCustomerCodeDl" readonly="readonly" disabled="disabled" type="text" class="InputTextStyle InputText1Style" style="width: 196px;" maxlength="100"/>

            <div class="Clear"></div>    

            <label class="LabelStyle Label2Style"><s:text name="price_grid_column13"/><span class="ReqiureStyle">(*)</span></label>
            <input id="txtPricePackageNotVATDl" type="text" readonly="readonly" disabled="disabled" class="InputTextStyle InputText1Style vinput-money" style="width: 196px;" maxlength="22" />
            <label class="LabelStyle Label2Style"><s:text name="price_grid_column14"/><span class="ReqiureStyle">(*)</span></label>
            <input id="txtPricePackageDl" type="text" class="InputTextStyle InputText1Style vinput-money" style="width: 196px;" maxlength="15" onchange="PriceManager.updatePricePackageNotVAT(2);" />

             <div class="Clear"></div> 

            <label class="LabelStyle Label2Style"><s:text name="price_grid_column9"/><span class="ReqiureStyle">(*)</span></label>
            <input id="txtPriceNotVATDl" type="text" readonly="readonly" disabled="disabled" class="InputTextStyle InputText1Style vinput-money" style="width: 196px;" maxlength="22" />
            <label class="LabelStyle Label2Style"><s:text name="price_grid_column10"/><span class="ReqiureStyle">(*)</span></label>
            <input id="txtPriceDl" type="text" class="InputTextStyle InputText1Style vinput-money" style="width: 196px;" maxlength="15" onchange="PriceManager.updatePriceNotVAT(2);" />

             <div class="Clear"></div>  

            <label class="LabelStyle Label2Style"><s:text name="price_grid_column11"/><span class="ReqiureStyle">(*)</span></label>
            <input id="txtVATDl" type="text" class="InputTextStyle InputText1Style vinput-number-dot" style="width: 196px;" maxlength="5" onchange="PriceManager.updatePriceNotVAT(1);PriceManager.updatePricePackageNotVAT(1);" />

            <div class="Clear"></div>
            
            
          	<div class="BtnCenterSection">
          		<button class="BtnGeneralStyle Sprite2 BtnSearchOnDialog" id="btnSaveDialogPriceChange" onclick="return PriceManager.changePriceByDialog();">
          			<span class="Sprite2"><s:text name="jsp.common.luu"/></span>
          		</button>
          		<button class="BtnGeneralStyle Sprite2 BtnSearchOnDialog" onclick="$('#changePriceDialog').dialog('close');">
          			<span class="Sprite2"><s:text name="jsp.common.dong"/></span>
          		</button>
          	</div>
           	<p id="errMsgDialogChangePrice" style="display: none;" class="ErrorMsgStyle"></p>
           	<p id="successMsgDialogChangePrice" class="SuccessMsgStyle" style="display:none;"></p>
           	<input type="hidden" id="permissionIdByChangeDialog" />
			<div class="Clear"></div>
		</div>
     </div>
</div>
<script type="text/javascript">
	$(function(){
		$('#txtVATDl, #txtPriceDl, #txtPriceNotVATDl, #btnSaveDialogPriceChange').bind('keyup',function(event){
			if(event.keyCode == keyCodes.ENTER){
				$('#btnSaveDialogPriceChange').click();
			}
		});
	});
</script>

<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<div class="GeneralMilkBox">
	<div class="GeneralMilkTopBox">
		<div class="GeneralMilkBtmBox">
			<div class="GeneralMilkInBox">
				<div class="GeneralTable">
					<div id="orderDetailsScrollSelection" class="ScrollSection">
						<table id="saleProductGrid" class="easyui-datagrid" title="Danh sách sản phẩm bán">  
        					 
    					</table>
						<div class="BoxGeneralTFooter">
							<table width="100%" border="0" cellspacing="0" cellpadding="0" id="footer_saleProductDetails">
								<colgroup>
									<col style="width: 44px;" />
									<col style="width: 91px;" />
									<col style="width: 200px;" />
									<col style="width: 84px;" />									
									<col style="width: 73px;" />
									<col style="width: 102px;" />
									<col style="width: 126px;" />
									<col style="width: 150px;" />
									<col style="width: 94px;" />
									<col style="width: 50px;" />
								</colgroup>
								<tfoot>
									<tr>
										<td class="ColsTd1">&nbsp;</td>
										<td class="ColsTd2">&nbsp;</td>
										<td class="ColsTd3">&nbsp;</td>
										<td class="ColsTd4">Tổng:</td>										
										<td class="ColsTd6">&nbsp;</td>
										<td class="ColsTd7" style="font-weight: normal;"  id="amount_total"></td>
										<td class="ColsTd8" style="font-weight: normal;" id="_total"></td>
										<td class="ColsTd9">&nbsp;</td>
										<td class="ColsTd10">&nbsp;</td>
										<td class="ColsTd11 ColsTdEnd">&nbsp;</td>
									</tr>
								</tfoot>
							</table>
						</div>
					</div>
				</div>
				<div class="ButtonSection" style="text-align: right;">
					<p id="alertSuccess_successMsg" class="SuccessMsgStyle" style="display: none;" >Tính tiền thành công cho đơn hàng !</p>
					<button class="BtnGeneralStyle BtnGeneralMStyle Sprite2" id="payment" onclick="return SPCreateOrder.payment();">
						<span class="Sprite2">Tính tiền</span>
					</button>					
				</div>				
			</div>
		</div>
	</div>
</div>
<script type="text/javascript">
	$(document).ready(function(){		
		
	});
</script>
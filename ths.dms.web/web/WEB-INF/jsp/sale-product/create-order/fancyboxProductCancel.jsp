<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<div class="GeneralDialog General2Dialog">
	<div class="DialogProductSearch">
		<div class="GeneralMilkBox">
			<div class="GeneralMilkTopBox">
				<div class="GeneralMilkBtmBox">
					<h3 class="Sprite2">
						<span class="Sprite2">Thông tin lô</span>
					</h3>
					<div class="GeneralMilkInBox ResearchSection">
						<div class="Warehouse83Form">
							<input type="hidden" id="shopCodeCancel" value='<s:property value="shopCodeCancel"/>'>
							<label class="LabelStyle Label1Style">Mã sản phẩm</label>
							<p class="ValueStyle Value1Style"><s:property value="productCodeCancel"/></p>
							<input type="hidden" id="productCodeCancel" value="<s:property value="productCodeCancel"/>"/>
							<label class="LabelStyle Label2Style">Tên sản phẩm</label>
							<p class="ValueStyle Value2Style"><s:property value="productNameCancel"/></p>
							<input type="hidden" id="productNameCancel" value="<s:property value="productNameCancel"/>"/>
							<div class="Clear"></div>
							<label class="LabelStyle"
								style="padding-left: 8px; width: 110px;">Tổng số lượng
								hủy</label>
							<p class="ValueStyle Value1Style"><s:property value="totalCancel1Cancel"/></p>
							<input type="hidden" id="totalCancel1Cancel" value="<s:property value="totalCancel1Cancel"/>"/>
							<div class="Clear"></div>
							<label class="LabelStyle Label3Style">Số lô</label> <input
								type="text" class="InputTextStyle InputText1Style"
								id="lotNumberCancel"/> <label
								class="LabelStyle Label4Style">Số lượng</label> <input
								type="text" class="InputTextStyle InputText1Style"
								id="lotTotalCancel"/>
							<div class="Clear"></div>
							<div class="ButtonSection">
								<button class="BtnGeneralStyle Sprite2" onclick="return saveLotProductCancelData();">
									<span class="Sprite2">Lưu</span>
								</button>
							</div>
							<p style="display: none" class="ErrorMsgStyle Sprite1" id="errMsgSaveLotCancel"></p>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="GeneralMilkBox">
			<div class="GeneralMilkTopBox">
				<div class="GeneralMilkBtmBox">
					<h3 class="Sprite2">
						<span class="Sprite2">Danh sách lô tách</span>
					</h3>
					<div class="GeneralMilkInBox">
						<div class="GeneralTable Table31Section">
							<div class="BoxGeneralTTitle">
								<table width="100%" style="width: 479px;" border="0" cellspacing="0" cellpadding="0">
                                       <colgroup>
                                           <col style="width:80px;" />
                                           <col style="width:144px;" />
                                           <col style="width:150px;" />
                                           <col style="width:50px;" />
                                           <col style="width:50px;" />
                                       </colgroup>
                                       <thead>
                                           <tr>
                                               <th class="ColsThFirst">STT</th>
                                               <th>Số lô</th>
                                               <th>Số lượng</th>
                                               <th>&nbsp;</th>
                                               <th class="ColsThEnd">&nbsp;</th>
                                           </tr>
                                       </thead>
                                   </table>
							</div>
							<div class="BoxGeneralTBody">
                                       <div class="ScrollBodySection" style="height: 120px; width: 479px;" id="listLotProductCancel">
                                           <table width="100%" style="width: 479px;" border="0" cellspacing="0" cellpadding="0">
                                               <colgroup>
                                                   <col style="width:80px;" />
                                                   <col style="width:144px;" />
                                                   <col style="width:150px;" />
                                                   <col style="width:50px;" />
                                                   <col style="width:50px;" />
                                               </colgroup>
                                               <tbody>
                                                   <s:iterator value="listLotProductCancel" status="stt">
                                                   	<tr>
                                                   		<td class="ColsTd1"><s:property value="#stt.count"/></td>
                                                   		<td class="ColsTd2"><s:property value="lotNumber"/></td>
                                                   		<td class="ColsTd3"><s:property value="lotTotal"/></td>
                                                   		<td class="ColsTd4"><a href="#" onclick="return editLotProductCancelData('<s:property value="lotNumber"/>', <s:property value="lotTotal"/>);"><img src="/resources/images/icon-edit.png" width="16" height="17" /></a></td>
                                                   		<td class="ColsTd5 ColsTdEnd"><a href="#" onclick="return deleteLotProductCancelData('<s:property value="lotNumber"/>', <s:property value="lotTotal"/>);"><img src="/resources/images/icon-delete.png" width="19" height="20" /></a></td>
                                                   	</tr>
                                                   </s:iterator>
                                               </tbody>
                                           </table>
                                       </div>
                                   </div>
							<div class="BoxGeneralTFooter">
								<table width="100%" cellspacing="0" cellpadding="0" border="0">
									<colgroup>
										<col style="width: 80px;">
										<col style="width: 144px;">
										<col style="width: 250px;">
									</colgroup>
									<tfoot>
										<tr>
											<td colspan="2">Tổng:</td>
											<td class="ColsTd3" id="totalCancel2Cancel"><s:property value="totalCancel2Cancel"/></td>
											<td class="ColsTd4"></td>
										</tr>
									</tfoot>
								</table>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="BoxDialogBtm">
			<div class="ButtonSection">
				<button
					class="BtnGeneralStyle BtnGeneralCStyle BtnGeneralMStyle Sprite2"
					onclick="return checkTotalLotProductCancel();">
					<span class="Sprite2">Đóng</span>
				</button>
			</div>
			<p style="display: none" class="ErrorMsgStyle Sprite1" id="errMsgLotCancel"></p>
		</div>
	</div>
</div>
<script type="text/javascript">
checkTotalLotProductCancel = function() {
	var totalCancel1Cancel = $('#totalCancel1Cancel').val();
	var productCodeCancel = $('#productCodeCancel').val();
	var productNameCancel = $('#productNameCancel').val();
	var shopCodeCancel = $('#shopCodeCancel').val();
	var urlCheck = '/sale-product/create-order/check-product-cancel';
	var params = new Object();
	params.totalCancel1Cancel = totalCancel1Cancel;
	params.productCodeCancel = productCodeCancel;
	params.productNameCancel = productNameCancel;
	params.shopCodeCancel = shopCodeCancel;
	
	$.ajax({
		type : "POST",
		url : urlCheck,
		data :($.param(params, true)),
		dataType : "json",
		success : function(data) {
			if(data == false) {
				$('#errMsgLotCancel').html('Tổng số lượng danh sách lô tách không trùng với tổng số lượng hủy').show();
				var tm = setTimeout(function(){
					$('#errMsgLotCancel').html('').hide();
					clearTimeout(tm);
				}, 3000);
			} else {
				$.fancybox.close();
			}
		},
		error:function(XMLHttpRequest, textStatus, errorThrown) {
			$.ajax({
				type : "POST",
				url : '/check-session',
				dataType : "json",
				success : function(data) {
					$('#errMsgLotCancel').html('Xảy ra lỗi').show();
				},
				error: function(XMLHttpRequest, textStatus, errorThrown) {
					window.location.href = '/home';
				}
			});
		}
	});
};
saveLotProductCancelData = function() {
	var lotNumberCancel = $('#lotNumberCancel').val();
	var lotTotalCancel = $('#lotTotalCancel').val();
	var productCodeCancel = $('#productCodeCancel').val();
	var productNameCancel = $('#productNameCancel').val();
	var totalCancel1Cancel = $('#totalCancel1Cancel').val();
	var shopCodeCancel = $('#shopCodeCancel').val();
	
	var params = new Object();
	params.lotNumberCancel = lotNumberCancel;
	params.lotTotalCancel = lotTotalCancel;
	params.productCodeCancel = productCodeCancel;
	params.productNameCancel = productNameCancel;
	params.totalCancel1Cancel = totalCancel1Cancel;
	params.shopCodeCancel = shopCodeCancel;
	
	var url = '/sale-product/create-order/product-cancel-info';
	
	Utils.getHtmlDataByAjax(params, url, function(data){
		try {
			var _data = JSON.parse(data);
			if(_data.error == true && _data.errMessage) {
				$('#errMsgSaveLotCancel').html(_data.errMessage).show();
				var tm = setTimeout(function(){
					$('#errMsgSaveLotCancel').html('').hide();
					clearTimeout(tm);
				}, 3000);
			}
		} catch(e) {
			$.fancybox.close();
			$.fancybox(data, {
				modal: true,
				title: 'Nhập lô hủy',
				afterShow: function() {
					$('#listLotProductCancel').jScrollPane();
				},
				afterClose: function() {
				}
			});
		}
	}, null, 'POST');
};
deleteLotProductCancelData = function(lotNum, lotTotal) {
	var lotNumberCancel = lotNum;
	var lotTotalCancel = lotTotal;
	var productCodeCancel = $('#productCodeCancel').val();
	var productNameCancel = $('#productNameCancel').val();
	var totalCancel1Cancel = $('#totalCancel1Cancel').val();
	var shopCodeCancel = $('#shopCodeCancel').val();
	
	var params = new Object();
	params.lotNumberCancel = lotNumberCancel;
	params.lotTotalCancel = lotTotalCancel;
	params.productCodeCancel = productCodeCancel;
	params.productNameCancel = productNameCancel;
	params.totalCancel1Cancel = totalCancel1Cancel;
	
	var url = '/sale-product/create-order/del-product-cancel';
	
	Utils.getHtmlDataByAjax(params, url, function(data){
		$.fancybox.close();
		$.fancybox(data, {
			modal: true,
			title: 'Nhập lô hủy',
			afterShow: function() {
				$('#listLotProductCancel').jScrollPane();
			},
			afterClose: function() {
			}
		});
		
	}, null, 'POST');
};
editLotProductCancelData = function(lotNum, lotTotal) {
	$('#lotNumberCancel').val(lotNum);
	$('#lotTotalCancel').val(lotTotal);
};
</script>
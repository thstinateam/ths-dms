<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
	<%@ taglib prefix="s" uri="/struts-tags"%>
<div class="GeneralMilkBox">
	<div class="GeneralMilkTopBox">
		<div class="GeneralMilkBtmBox">		
				<h3 class="Sprite2"><span class="Sprite2">Thông tin tìm kiếm</span></h3>
				<div class="GeneralMilkInBox ResearchSection">
	          	<div class="ModuleList18Form">
              		<label class="LabelStyle Label1Style">Mã sản phẩm</label>
              		<input id="productId" type="text" class="InputTextStyle InputText1Style" /> 
              		<label class="LabelStyle Label2Style">Tên sản phẩm</label>
              		<input id="searchMonth" type="text" class="InputTextStyle InputText2Style" />
              		<div class="Clear"></div>              		
              		<label class="LabelStyle Label1Style">Mã CTKM</label>
              		<input id="promotionCode" type="text" class="InputTextStyle InputText1Style" />  
              		            		
		             <button id="btnSearch" style="padding-left: 210px" class="BtnGeneralStyle Sprite2" onclick="return QuotaMonthPlan.search();">
			         <span class="Sprite2">Tìm kiếm</span></button>	
            </div>
            
		</div>			
			<div class="GeneralMilkInBox">
				<div class="ResultSection" id="listProductResult">
					<p id="gridNoResult" style="display: none" class="WarningResultStyle">Không có kết quả</p>
						<table id="productGrids"></table>
						<div id="productPaper"></div>						
				</div>
				<div class="ButtonSection">
					<button class="BtnGeneralStyle Sprite2"
						onclick="return SPCreateOrder.selectListProducts();">
						<span class="Sprite2">&nbsp; Chọn hàng &nbsp;</span>
					</button>
					<button class="BtnGeneralStyle BtnGeneralMStyle Sprite2">
						<span class="Sprite2">&nbsp;&nbsp;Đóng&nbsp;&nbsp;</span>
					</button>					
				</div>
			</div>
		</div>
	</div>
	
	</div>
<script type="text/javascript">
	$(document).ready(function(){	
		$("#productGrids").jqGrid({			  
			  url:SPCreateOrder.gridURL(),			  
			  colModel:[		
			    {name:'productCode', label: 'Mã MH', width: 100, sortable:false,resizable:false , align: 'left', formatter:function(value, row, index) {
		        	  return Utils.XSSEncode(value);
		          }},
			    {name:'productName', label: 'Tên MH',width: 200, sortable:false,resizable:false , align: 'left', formatter:function(value, row, index) {
		        	  return Utils.XSSEncode(value);
		          }},
			    {name:'availableQuantity', label: 'Tồn kho', width: 100,sortable:false,resizable:false , align: 'right'},
			    {name:'price', label: 'Giá',width: 100, sortable:false,resizable:false , align: 'right',formatter: GridFormatterUtils.currencyCellFormatter},
			    {name:'isFocus', label: 'TT',width: 30, sortable:false,resizable:false , align: 'right',formatter:SaleProductGridFormatter.forcusFormatter},
			    {name:'promotionProgramCode', label: 'CTKM',width: 100, sortable:false,resizable:false , align: 'left', formatter:function(value, row, index) {
		        	  return Utils.XSSEncode(value);
		          }},
			    {name:'',width: 100, label: 'Số lượng', sortable:false,resizable:false , align: 'right',formatter:SaleProductGridFormatter.quantityFormatter},
			    {name:'commercial', label: 'CT HTTM (F9)',width: 100, sortable:false,resizable:false , align: 'left',formatter:SaleProductGridFormatter.commercialSupporFormatter},
			    {name: 'convfact', index:'convfact', hidden: true},
			    {name: 'productId', index:'productId', hidden: true}
			  ],	  
			  pager : '#productPaper',
			  height: 'auto',
			  rownumbers: true,	  
			  width: ($('#listProductResult').width()),
			  gridComplete: function(){	
				  $.fancybox.update();
				  updateRownumWidthForJqGrid();
			  }
			})
		.navGrid('#productPaper', {edit:false,add:false,del:false, search: false});
		$('#jqgh_productGrids_rn').prepend('STT');		
	});
</script>
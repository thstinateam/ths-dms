<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles" %>
<tiles:insertTemplate template='/WEB-INF/jsp/sale-product/common.jsp'/>

<div class="BreadcrumbSection">
    <ul class="ResetList FixFloat BreadcrumbList">
        <li class="Sprite1"><a id="btnShowCreateOrderPage20140919114109" href="/sale-product/create-order/info"><s:text name="create_order_ban_hang"/></a></li>
        <li><span><s:text name="create_order_tao_don"/></span></li>
    </ul>
</div>

<div class="CtnOneColSection">
    <div class="ContentSection">
    	<div class="ToolBarSection">
            <div class="SearchSection GeneralSSection">
            	<h2 class="Title2Style"> <s:text name="create_order_thong_tin_chung"/>
            		<img style="display: none;" src="/resources/scripts/plugins/jquery-easyui-1.3.2/themes/default/images/loading.gif" class="loadingTitle" >
            	</h2>
                <div class="SearchInSection SProduct1Form" id="generalInfoDiv">
                 <label class="LabelStyle Label2Style"><s:text name="unit_tree.search_unit.tab_name"/>
                            <span class="ReqiureStyle"><font color="red"> *</font></span>
                    </label>
                    <div class="BoxSelect BoxSelect2" id ="shopIdList" >
                        <select id="shopCodeCB" style="width: 200px;">
                        </select>
                    </div>
                    <label class="LabelStyle Label1Style"><s:text name="create_order_order_date"/><span class="ReqiureStyle"> *</span></label>
                    <input id="orderDate" name="orderDate" tabindex="-1" type="text" class="InputTextStyle InputText1Style" autocomplete="off"
                    		value="<s:property value='' />" />
                    <div class="Clear"></div>
                	<label class="LabelStyle Label2Style"><s:text name="create_order_so_don_hang"/></label>
                         <input type="text" id="orderNumber" tabindex="-1" class="InputTextStyle InputText1Style" readOnly="readOnly" autocomplete="off" style="background:#eee;"
                    		value="<s:property value='saleOrder.orderNumber' />" />
                    <label class="LabelStyle Label1Style"><s:text name="create_order_makh_F9"/><span class="ReqiureStyle"> *</span></label>
                    <input id="customerCode" name="customerCode" tabindex="0" type="text" class="InputTextStyle InputText1Style" autocomplete="off"
                    		value="<s:property value='saleOrder.customer.shortCode' />" />
                    <label class="LabelStyle Label1Style"><s:text name="create_order_ten_kh"/></label>
                    <p id="customerName" class="ValueStyle Value1Style"><s:property value="saleOrder.customer.customerName" /></p>
                    <div class="Clear"></div>
                    <label class="LabelStyle Label2Style"><s:text name="create_order_nvtt"/></label>
                    <div class="BoxSelect BoxSelect2"> 
                        <select id="cashierStaffCode" tabindex="1" style="width:210px">
                        	<option value="" ></option>
                        	<s:iterator value="lstNVTTVo"  var="obj" >
                        		<option value="<s:property value="#obj.staffCode" />" ><s:property value="#obj.staffName" /></option>
                        	</s:iterator>
                        </select>
                    </div>
                    <label class="LabelStyle Label1Style"><s:text name="create_order_nvgh"/></label>
                    <div class="BoxSelect BoxSelect2">
                        <select id="transferStaffCode" tabindex="2" style="width:210px">
                        	<option value="" ></option>
                        	<s:iterator value="lstNVGHVo"  var="obj" >
                        		<option value="<s:property value="#obj.staffCode" />" ><s:property value="#obj.staffName" /></option>
                        	</s:iterator>
                        </select>
                    </div>
                    <label class="LabelStyle Label1Style"><s:text name="create_order_dia_chi_kh"/></label>
                    <p class="ValueStyle Value1Style" id="address"><s:property value="saleOrder.customer.address" /></p>
                    <div class="Clear"></div>
                    <label class="LabelStyle Label2Style"><s:text name="create_order_nvbh"/><span class="ReqiureStyle"> *</span></label>
                    <div class="BoxSelect BoxSelect2" id="boxSelectNVBH">
                        <select id="staffSaleCode" tabindex="3" style="width:210px">
                        	<option value="0" >Chọn nhân viên</option>
                        	<s:iterator value="lstNVBHVo" var="obj" >
                        		<option value="<s:property value="#obj.staffCode" />" ><s:property value="#obj.staffName" /></option>
                        	</s:iterator>
                        </select>
                    </div>
                    <label class="LabelStyle Label1Style"><s:text name="create_order_chon_xe"/></label>
                    <div class="BoxSelect BoxSelect2">
                    	<select class="easyui-combobox" id="carId" tabindex="4">
                        </select>
                    </div>
                    <label class="LabelStyle Label1Style"><s:text name="create_order_so_dt"/></label>
                    <p class="ValueStyle Value1Style" id="phone"><s:property value="saleOrder.customer.phone" /> / <s:property value="saleOrder.customer.mobiphone" /></p>
                    <div class="Clear"></div>
                    <label class="LabelStyle Label2Style"><s:text name="create_order_ngay_giao"/></label>
                    <input type="text" id="deliveryDate" name="deliveryDate"tabindex="5"  class="InputTextStyle InputText6Style vinput-date" />
                    <label class="LabelStyle Label1Style"><s:text name="create_order_do_uu_tien"/></label>
                    <div class="BoxSelect BoxSelect2">
                        <s:select cssStyle="opacity: 0; height: 20px;" tabindex="6" cssClass="MySelectBoxClass" list="priority" id="priorityId"
								value="saleOrder.priority" listKey="id" listValue="apParamName" disabled="disabledButton" ></s:select>
                    </div>
                    <div class="Clear"></div> 
                    <div style=""> 
						<p style="height: 25px; display: none;" class="ErrorMsgStyle" id="GeneralInforErrors"><s:text name="create_order_co_loi"/></p>
						<p style="height: 25px; display: none;" class="ErrorMsgStyle" id="GeneralInforErrors2"><s:text name="create_order_co_loi"/></p>
						<p style="height: 25px; display: none;" class="ErrorMsgStyle" id="GeneralInforWarning"><s:text name="create_order_co_loi"/></p>
					</div>
                    <div class="Clear"></div>
                </div>
                <h2 class="Title2Style"><s:text name="create_order_ds_sp_ban"/>
                	<img style="display: none;" src="/resources/scripts/plugins/jquery-easyui-1.3.2/themes/default/images/loading.gif" id="loadingProduct" />
                </h2>
                <div class="SearchInSection SProduct1Form" id="spSaleProductInfor" style="">                	
                	<div class="GeneralMilkBox">
						<div class="GeneralMilkTopBox">
							<div class="GeneralMilkBtmBox">
								<div class="GeneralMilkInBox">
									<div id="gridSaleData"></div>
									<div class="Clear"></div>
									<p class="TotalInfo" style="margin-top:10px;display:none;">
	                                	<span class="Tem1Style"><s:text name="create_order_tong_trong_luong"/> <span style="color: #FF0000" id="totalWeightSale">0</span><span style="color: #FF0000">(Kg)</span></span>
	                                	<span id="sumPrice1">
	                                    	<span class="Tem1Style"> <s:text name="create_order_tong_tien"/> <span style="color: #FF0000" id="totalAmountSale">0</span><span style="color: #FF0000">(VNĐ)</span></span>
                                			<span class="Tem1Style"><s:text name="create_order_chiet_khau"/> <span style="color: #FF0000" id="totalDiscountSale">0</span><span style="color: #FF0000">(VNĐ)</span></span>
                                		</span>
                                	</p>
									<div class="ButtonSection" style="margin-top:20px;text-align: right;">										
										<button style="margin-right: 10px;" class="BtnGeneralStyle BtnGeneralMStyle Sprite2" id="payment" onclick="return SPCreateOrder.paymentPrepare();">
											<span class="Sprite2"><s:text name="create_order_tinh_tien"/></span>
										</button>
										<a href="javascript:void(0)" style="margin-right: 10px;" id="backBtn" title="<s:text name="create_order_tinh_tien_lai"/>">
											<img src="/resources/images/icon-back.png" width="20" height="20" />
										</a>	
										<div style="height: 15px;">
											<p id="alertSuccess_successMsg" class="SuccessMsgStyle" style="display: none;padding: 0;" >Tính tiền thành công cho đơn hàng !</p>
											<p style="display: none;" class="ErrorMsgStyle" id="saleInputErr"></p>
										</div>										
									</div>				
								</div>
							</div>
						</div>
					</div>                	
                </div>
                
                <h2 id="h2SPKM" class="Title2Style">Danh sách sản phẩm khuyến mãi</h2>
                <div class="SearchInSection SProduct1Form" id="spPromotionProductInfor" style="display: none;padding-bottom:0;">
                      <div class="GeneralMilkBox">
					  	<div class="GeneralMilkTopBox">
							<div class="GeneralMilkBtmBox">
								<div class="GeneralMilkInBox">
									<div class="GeneralTable">
										<div class="GridSection" style="padding: 0px;">
											<table id="gridPromotionProduct" class="easyui-datagrid" title="Danh sách sản phẩm khuyến mãi"></table>												
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
                    <div class="Clear"></div>
                </div>
                
                <h2 id="h2MM" class="Title2Style">Khuyến mãi mở mới</h2>
                <div class="SearchInSection SProduct1Form">
                    <div class="GeneralMilkBox">
					  	<div class="GeneralMilkTopBox">
							<div class="GeneralMilkBtmBox">
								<div class="GeneralMilkInBox">
									<div class="GeneralTable">
										<div class="GridSection" id="promotionGridContainer2" style="padding: 0px;">
											<div id="promotionGrid2"></div>	
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
                    <div class="Clear"></div>
                </div>
                
                <h2 id="h2KMDH" class="Title2Style">Khuyến mãi đơn hàng</h2>
                <div id="saleOrderPromotion" class="SearchInSection SProduct1Form">
                	<div class="GeneralMilkBox">
						<div class="GeneralMilkTopBox">
							<div class="GeneralMilkBtmBox">
								<div class="GeneralMilkInBox">
									<div class="GeneralTable">
										<div class="GridSection" style="padding: 0px;">
											<table id="gridSaleOrderPromotion" class="easyui-datagrid" title="Danh sách sản phẩm khuyến mãi"></table>												
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
                    <div class="Clear"></div>
                </div>
                
                <h2 id="h2KS" class="Title2Style">Trả thưởng chương trình hỗ trợ thương mại vào đơn hàng      <input id="chkKeyshop" type="checkbox" checked onchange="SPCreateOrder.chkKeyShopChange();" /></h2>
                <div id="keyShop" class="SearchInSection SProduct1Form">
                	<div class="GeneralMilkBox">
						<div class="GeneralMilkTopBox">
							<div class="GeneralMilkBtmBox">
								<div class="GeneralMilkInBox">
									<div class="GeneralTable">
										<div class="GridSection" style="padding: 0px;">
											<table id="gridKeyShop"></table>												
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
                    <div class="Clear"></div>
                </div>
                
                <h2 id="h2KMTL" class="Title2Style">Khuyến mãi tích lũy</h2>
                <div class="SearchInSection SProduct1Form">
                    <div class="GeneralMilkBox">
					  	<div class="GeneralMilkTopBox">
							<div class="GeneralMilkBtmBox">
								<div class="GeneralMilkInBox">
									<div class="GeneralTable">
										<div class="GridSection" id="promotionGridContainer4" style="padding: 0px;">
											<div id="accumulativePromotionGrid"></div>	
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
                    <div class="Clear"></div>
                 </div>
                 <div class="SearchInSection SProduct1Form">
                    <p class="TotalInfo">
                    	<span class="Tem1Style"><s:text name="create_order_tong_trong_luong"/> <span style="color: #FF0000" id="totalWeight">0</span><span style="color: #FF0000">(Kg)</span></span>
                    	<span id="sumPrice">
                        	<span class="Tem1Style"><s:text name="create_order_tong_tien"/> <span style="color: #FF0000" id="totalVAT">0</span><span style="color: #FF0000">(VNĐ)</span></span>
                    		<span class="Tem1Style"><s:text name="create_order_chiet_khau"/> <span style="color: #FF0000" id="totalDiscount">0</span><span style="color: #FF0000">(VNĐ)</span></span>
                    		<span class="Tem1Style"><s:text name="create_order_tong_tien_thanh_toan"/> <span style="color: #FF0000" id="total">0</span><span style="color: #FF0000">(VNĐ)</span></span>
                    	</span>
                    </p>
                    <div class="Clear"></div>
                    <p id="errExcelMsg" class="ErrorMsgStyle" style="display: none"></p>
					<p style="display: none;" class="ErrorMsgStyle" id="serverErrors"><s:text name="create_order_co_loi"/></p>
					<p id="errMsgOrder" class="ErrorMsgStyle" style="display:none;" />
					<p id="successMsg" class="SuccessMsgStyle" style="display: none"></p>  
                    <div class="ButtonSection" style="text-align: right;">
						<button class="BtnGeneralStyle" style="float: left;" id = "btnOrderCancel" onclick="SPCreateOrder.cancelWebOrder();"> <s:text name="create_order_huy"/></button>
                    	<button style="float: right; margin-right: 20px;" class="BtnGeneralStyle BtnGeneralMStyle Sprite2" id="btnOrderSave" onclick="SPCreateOrder.createOrder();">
							<span class="Sprite2"><s:text name="create_order_luu"/> </span>
						</button>
						<button style="float: right; margin-right: 10px;" class="BtnGeneralStyle BtnGeneralMStyle Sprite2" id="btnOrderReset" onclick="SPCreateOrder.resetOrder();">
							<span class="Sprite2"><s:text name="create_order_them_moi"/></span>
						</button>
						<div class="Clear"></div>
					</div>                   
                </div>
            </div>
            <div class="Clear"></div>
        </div>
    </div>
    <div class="Clear"></div>
</div>
<div id="responseDiv" style="display:none;"></div>
<div id="searchStyle5PPDialog" style="display: none;">
	<div id="searchStyle5" class="easyui-dialog" title="Danh sách sản phẩm khuyến mãi" style="width:616px;height:auto;" data-options="closed:true,modal:true">
        <div class="PopupContentMid2">
        	<div class="GeneralForm Search1Form">
                <div class="GridSection">
                    <table id="listPromotionProduct"></table>
                </div>
                <p style="display: none" id="errMultiChooseProduct" class="ErrorMsgStyle"></p>
                <div class="BtnCenterSection">
                	<button class="BtnGeneralStyle BtnGeneralMStyle" id="btnMultiChoose" style="display: none;"><span class="Sprite2"><s:text name="create_order_chon"/></span></button>
					<button class="BtnGeneralStyle BtnGeneralMStyle" onclick="$('#searchStyle5').dialog('close');"><span class="Sprite2"><s:text name="create_order_dong"/></span></button>
				</div>
            </div>            
		</div>
    </div>
</div>
<div id ="discountDetailDialogDiv" style="display: none" >
	<div id="discountDetailDialog" class="easyui-dialog" title="Danh sách chiết khấu theo chương trình" style="width:616px;height:auto;" data-options="closed:true,modal:true">
		<div class="PopupContentMid">
			<div class="GeneralForm Search1Form">
                <div class="GridSection" id="discountDetailDiv">
                    <table id="gridDiscountDetail"></table>
                </div>
                <div class="BtnCenterSection">
					<button class="BtnGeneralStyle BtnGeneralMStyle" onclick="$('#discountDetailDialog').dialog('close');"><span class="Sprite2"><s:text name="create_order_dong"/></span></button>
				</div>
            </div>
		</div>
	</div>
</div>
<input type="hidden" id="shopId" value="<s:property value="currentShop.id" />" />
<input type="hidden" id="shopCode" value="<s:property value="shopCode" />" />
<input type="hidden" id="currentLockDay" value="<s:property value="lockDate" />" />
<s:hidden id="canChangePromotionQuantity" name="canChangePromotionQuantity"></s:hidden>
<input type="hidden" id="saleOrderId" value="<s:property value='saleOrder.id' />" />

<script type="text/javascript">
$(document).ready(function() {	
	//Define cac su kien phim tat
	//F1: tinh tien
	//F2: tao don hang
	//F3: Huy don hang
	//F8: Them moi don hang
	//F6: Import Excel
	//F9: tim kiem don hang
    // $("#shopCode").change(function() {
    //     var shopCode = $('#shopCode').combobox('getValue');
    //     alert(shopCode);
    //     SPCreateOrder.getStaffByShopCode(shopCode);
    // });
    var chose = true;    
	$(document).keydown(function(event){
		if (event.which == keyCodes.F3) {
			event.preventDefault();
		    SPCreateOrder.cancelWebOrder();
		}
	});
	$(document).keyup(function(event){
	  if (event.which == keyCodes.F1) {
		  if ($('#btnOrderSave').prop('disabled')) {
			  SPCreateOrder.payment();
		  } 
	  }
	  if (event.which == keyCodes.F2) {
		  event.preventDefault();
		  if ($('#payment').prop('disabled')) {
			  SPCreateOrder.createOrder();
		  } 
	  } 
	  if (event.which == keyCodes.F6) {
		  event.preventDefault();
		  if (!$('#btnOrderReset').prop('disabled') && $('#btnOrderSave').prop('disabled')) {
			  SPCreateOrder.resetOrder();
		  }
	  } 
	});
	
	var wWidth = $(window).width();
	applyDateTimePicker("#orderDate", null, null, null, null, null, null, null, null, null, null, function() {
		SPCreateOrder.changeOrderDate();
	});
	SPCreateOrder.orderDate = $('#orderDate').val();
	$('#gridSaleContainer').css('overflow', 'scroll');
	$('#gridSaleContainer').css('width', wWidth - 40);
	var priority = $('#priorityId option').next().val();
	$('#priorityId').val(priority);
	$('#priorityId').change();
	SPCreateOrder._isDividualWarehouse = ('<s:property value="isDividualWarehouse"/>' == 'true');
	SPCreateOrder._sysConvertQuantityConfig = '<s:property value="sysConvertQuantityConfig"/>';
	SPCreateOrder.pageLoad();
	disabled('backBtn');
	$('#backBtn').click(function(e) {
		enable('payment');
		disabled('btnOrderSave');	
	});
	var dDate = '<s:date name="saleOrder.deliveryDate" format="dd/MM/yyyy" />';
	if (dDate.length == 0) {
		var lockDate = '<s:property value="lockDate"/>';
		$('#deliveryDate').val(lockDate);
	} else {
		$('#deliveryDate').val(dDate);
	}
	$('#downloadTemplate').attr('href',excel_template_path + 'sale-product/template_import_sale_oder_display.xls');
	$('#btnOrderSave').removeAttr('disabled');
	
	$("#orderNumber").bind("keyup", function (event) {
		if (event.keyCode == keyCodes.F9) {
			SPCreateOrder.openSearchWebOrderDlg();
		}
	});
	
	$('#shortCodeDlg').live('keyup', function(event){
		if(event.keyCode == keyCode_F9){
			var arrParam = new Array();
			var param = {};
			param.name = "shopCode";
            param.value = $('#shopCodeCB').combobox('getValue');
			arrParam.push(param);
			SPReturnOrder.searchCustomerEasyUIOnDialog(function(data) {
				$('#shortCodeDlg').val(data.code);
				$('#customerNameDlg').val(data.name);
			}, arrParam);
		}
	});
});
</script>

<div id="searchWebOrderDiv" style="display: none;">
	<div id="searchWebOrderPopup">
        <div class="PopupContentMid2">
        	<div class="GeneralForm Search1Form">
                <label class="LabelStyle Label2Style"><s:text name="create_order_so_dh"/></label>
                <input id ="orderNumberDlg" type="text" class="InputTextStyle InputText5Style" maxlength="20"/>
                <label class="LabelStyle Label2Style"><s:text name="create_order_thuc_hien"/></label>
                <p class="ValueStyle Value1Style" style="width:100px;"><s:text name="create_order_presale"/></p>
                <label class="LabelStyle Label2Style"><s:text name="create_order_trang_thai"/></label>
                <p class="ValueStyle Value1Style" style="width:100px;"><s:text name="create_order_chua_duyet"/></p>
                <label class="LabelStyle Label2Style" style="width:100px;"><s:text name="create_order_loai_don"/></label>
                <p class="ValueStyle Value1Style" style="width:100px;"><s:text name="create_order_don_ban"/></p>
                <div class="Clear"></div>
                
                <label class="LabelStyle Label2Style"><s:text name="create_order_makh"/></label>
                <input id ="shortCodeDlg" type="text" class="InputTextStyle InputText5Style" maxlength="20"/>
                <label class="LabelStyle Label2Style"><s:text name="create_order_tenkh"/></label>
                <input id="customerNameDlg" type="text" class="InputTextStyle InputText5Style" maxlength="250"/>
                
                <label class="LabelStyle Label2Style"><s:text name="create_order_nvbh"/></label>
                <div class="BoxSelect BoxSelect2">
                    <select id="staffCodeDlg"></select>
                </div>
                <div class="Clear"></div>
                
                <label class="LabelStyle Label2Style"><s:text name="create_order_tu_ngay"/></label>
                <input type="text" class="InputTextStyle InputText6Style" id="fromDateDlg" maxlength="10" value="<s:property value="lockDate"/>"/>
                <label class="LabelStyle Label2Style"><s:text name="create_order_den_ngay"/></label>
                <input type="text" class="InputTextStyle InputText6Style" id="toDateDlg" maxlength="10" value="<s:property value="lockDate"/>"/>
                
                <label class="LabelStyle Label2Style"><s:text name="create_order_nvgh"/></label>
                <div class="BoxSelect BoxSelect2" >
                	<select id="deliveryCodeDlg" ></select>
                </div>
                <div class="Clear"></div>
                <div class="Clear"></div>
                
                <div class="BtnCenterSection">
                    <button class="BtnGeneralStyle BtnSearchOnDialog cmsdefault" id ="btnSearchDlg" onclick="SPCreateOrder.searchWebSaleOrder();"><s:text name="create_order_tk"/></button>
                </div>
                <div class="Clear"></div>
            </div>
            <div class="GeneralForm Search1Form">
            	<h2 class="Title2Style Title2MTStyle"><s:text name="create_order_ds_don_hang"/></h2>
                <div class="GridSection">
                <table id="gridSearchBill"></table>
                </div>
                <div class="Clear"></div>
                <div class="BtnCenterSection">
                    <button class="BtnGeneralStyle cmsdefault" onclick="$('#searchWebOrderPopup').dialog('close');"><s:text name="create_order_dong"/></button>
                </div>
            </div>
            <div class="Clear"></div>
            <p class="ErrorMsgStyle" id="errorSearchMsg" style="display: none;"></p>
		</div>
    </div>
</div>
<div id ="searchCustomerEasyUIDialogDiv" style="display: none" >
	<div id="searchCustomerEasyUIDialog" style="width: 600px; height: auto;" title="" data-options="closed:true,modal:true">
		<div class="PopupContentMid">
			<div class="GeneralForm Search1Form" >
				<label id="seachStyle1CodeLabel" class="LabelStyle Label1Style" style=" width: 100px;">Mã</label>
				<input id="seachStyle1Code" maxlength="20" tabindex="1" type="text" style=" width: 145px;" class="InputTextStyle InputText1Style" /> 
				<label id="seachStyle1NameLabel" class="LabelStyle Label1Style" style=" width: 100px;">Tên</label> 
				<input id="seachStyle1Name" type="text" maxlength="20" tabindex="2" style="width: 145px;"  class="InputTextStyle InputText1Style" />
				<div class="Clear"></div>
                <label id="seachStyle1AddressLabel" style="display: none;width: 100px;" class="LabelStyle Label1Style">Địa chỉ</label>
                <input id="seachStyle1Address" type="text" style="display: none;width: 409px;"  maxlength="250" tabindex="3" class="InputTextStyle InputText1Style" />
                <div class="Clear"></div>
				<button class="BtnGeneralStyle BtnSearchStyle BtnSearchOnDialog" tabindex="3" id="btnsearchCustomer">Tìm kiếm</button>
				<input type="hidden" name="" id="searchCustomerUrl"/>
				<input type="hidden" name="" id="searchCustomerCodeText"/>
				<input type="hidden" name="" id="searchCustomerNameText"/>
				<input type="hidden" name="" id="searchCustomerIdText"/>
				<s:hidden id="searchCustomerAddressText"></s:hidden>
				<div class="Clear"></div>
				<div class="GridSection" id="searchCustomerContainerGrid">					
					<table id="searchCustomerGrid"></table>
					<div id="searchCustomerPager"></div>
				</div>
				<div class="BtnCenterSection">
					<button id="__btnSave" style="display: none;" class="BtnGeneralStyle cmsdefault">Chọn</button>
					<button class="BtnGeneralStyle BtnGeneralMStyle cmsdefault" onclick="$('#searchCustomerEasyUIDialog').dialog('close');">Đóng</button>
				</div>
				<p id="errMsgSearch" style="display: none;" class="ErrorMsgStyle SpriteErr"></p>
			</div>
		</div>
	</div>
</div>
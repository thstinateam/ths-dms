<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<input id="shopCode" value="<s:property value="shopCode"/>" type="hidden"/>
<label class="LabelStyle Label2Style">Số đơn trả</label>
<s:if test="saleOrder.type.value==2">
	<input type="text" class="InputTextStyle InputText1Style" disabled="disabled" id="returnOrderNumber" value="<s:property value='saleOrder.orderNumber' />" />
</s:if>
<s:else>
	<input type="text" class="InputTextStyle InputText1Style" disabled="disabled" id="returnOrderNumber" />
</s:else>
<label class="LabelStyle Label1Style">Số đơn hàng(F9)</label>
<input type="text" class="InputTextStyle InputText1Style" readonly="readonly" id="poCode" value="<s:property value="orderNumber" />" />
<div class="Clear"></div>
<label class="LabelStyle Label2Style">Mã KH</label>
<!-- <p class="ValueStyle Value2Style"></p> -->
<a href="javascript:void(0)" style="cursor: pointer;" class="ValueStyle Value2Style" id="customerCodeHeader"
		onclick="SPReturnOrder.showCustomerInfoEasyUI('<s:property value="customerId" />');" ><s:property value="customerCode" /></a>
<label class="LabelStyle Label1Style">Tên KH</label>
<p class="ValueStyle Value1Style" id="customerNameHeader"><s:property value="customerName" /></p>
<label class="LabelStyle Label1Style">Địa chỉ KH</label>
<p class="ValueStyle Value1Style" id="customerAddressHeader"><s:property value="customerAddress" /></p>
<div class="Clear"></div>
<label class="LabelStyle Label2Style">NVBH</label>
<p class="ValueStyle Value1Style" id="staffCodeHeader"><s:property value="staffCode" /></p>
<label class="LabelStyle Label1Style">NVGH</label>
<p class="ValueStyle Value1Style" id="deliveryCodeHeader"><s:property value="deliveryCode" /></p>
<label class="LabelStyle Label1Style">Số ĐT</label>
<p class="ValueStyle Value1Style" id="phoneHeader"><s:property value="phone"/> / <s:property value="mobiphone"/></p>
<div class="Clear"></div>
<label class="LabelStyle Label2Style">Ngày giao</label>
<p class="ValueStyle Value1Style" id="deliveryDateHeader"><s:property value="deliveryDate" /></p>
<label class="LabelStyle Label1Style">Xe</label>
<p class="ValueStyle Value1Style" id="carNumberHeader"><s:property value="carNumber" /></p>
<div class="Clear"></div>

<input type="hidden" id="returnOrderId" value="<s:property value='saleOrder.id' />" />

<script type="text/javascript">
var totalWeight = "<s:property value='saleOrder.totalWeight' />";
if (!isNaN(totalWeight) && Number(totalWeight)) {
	$("#pTotalWeight").text("<s:property value='saleOrder.totalWeight' /> kg" );
} else {
	$("#pTotalWeight").text("0.0 kg");
}
$("#pAmount").text("<s:property value='convertMoney(saleOrder.amount)' /> VNĐ" );
var discount = "<s:property value='saleOrder.discount' />";
if (!isNaN(discount) && Number(discount)) {
	$("#pDiscount").text("<s:property value='convertMoney(saleOrder.discount)' /> VNĐ" );
} else {
	$("#pDiscount").text("0 VNĐ");
}
$("#pTotal").text("<s:property value='convertMoney(saleOrder.total)' /> VNĐ" );
</script>
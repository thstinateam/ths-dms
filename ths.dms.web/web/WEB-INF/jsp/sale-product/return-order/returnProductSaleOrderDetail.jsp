<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>

<h2 class="Title2Style">Sản phẩm bán</h2>
<div class="SearchInSection SProduct1Form">
	<div class="GridSection">
		<div class="GeneralMilkInBox">
			<div class="GeneralTable">
				<div class="ScrollSection">
					<div class="BoxGeneralTTitle">
						<table width="100%" border="0" cellspacing="0" cellpadding="0">
							<colgroup>
								<col style="width: 30px;" />
								<col style="width: 95px;" />
								<col style="width: 134px;" />
								<col style="width: 90px;" />
								<col style="width: 90px;" />
								<!-- <col style="width: 90px;" /> -->
								<col style="width: 70px;" />
								<col style="width: 116px;" />
								<col style="width: 100px;" />
								<col style="width: 100px;" />
							</colgroup>
							<thead>
								<tr>
									<th class="ColsThFirst">STT</th>
									<th>Mã sản phẩm</th>
									<th>Tên sản phẩm</th>
									<th>Đơn giá</th>
									<th>Kho</th>
									<!-- <th>Lô</th> -->
									<th>Thực đặt</th>
									<th>Thành tiền</th>
									<th>CKMH</th>
									<th>CT HTTM</th>
								</tr>
							</thead>
						</table>
					</div>
					<div class="BoxGeneralTBody">
						<div class="ScrollBodySection" id="salesOrderDetailScroll">
							<table width="100%" border="0" cellspacing="0" cellpadding="0">
								<colgroup>
									<col style="width: 30px;" />
									<col style="width: 95px;" />
									<col style="width: 134px;" />
									<col style="width: 90px;" />
									<col style="width: 90px;" />
									<!-- <col style="width: 90px;" /> -->
									<col style="width: 70px;" />
									<col style="width: 116px;" />
									<col style="width: 100px;" />
									<col style="width: 100px;" />
								</colgroup>
								<tbody>
									<s:iterator value="listSaleOrderDetailVOEx" status="stt">
										<tr>
											<td class="ColsTd1" style="text-align:center;"><s:property value="#stt.index+1" /> </td>
											<td class="ColsTd2 ColsAlignLeft" style="text-align:left;padding-left:3px;">
												<a style="cursor: pointer;" class="cmsdefault" onclick="return SPReturnOrder.showProductInfo('<s:property value="productCode"/>', 
					'<s:property value="productName"/>', <s:property value="convfact"/>,'<s:property value="cellQuantityFormatter(availableQuantity,convfact)"/>', '<s:property value="convertMoney(price)" />', 
					'<s:property value="availableQuantity"/>','<s:property value="promotionProgramCode"/>');"><s:property value="productCode" /> </a>
											</td>
											<td class="ColsTd3 ColsAlignLeft" style="text-align:left;padding-left:3px;"><s:property value="productName" />
											</td>
											<td class="ColsTd4 ColsAlignRight" style="text-align:right;padding-right:3px;"><s:property value="convertMoney(price)" /></td>
											<td class="ColsTd3 ColsAlignLeft" style="text-align:left;padding-left:3px;"><s:property value="warehouseName" /></td>
											<!-- <td class="ColsTd3 ColsAlignLeft" style="text-align:left;padding-left:3px;"></td> -->
											<td class="ColsTd5 ColsAlignRight" style="text-align:right;padding-right:3px;"><s:property value="cellQuantityFormatter(quantity,convfact)" /></td>
											<td class="ColsTd6 ColsAlignRight" style="text-align:right;padding-right:3px;"><s:property value="convertMoney(price*quantity)" /></td>
											<td class="ColsTd3 ColsAlignLeft" style="text-align:left;padding-left:3px;"><s:property value="discountAmount" /></td>
											<td class="ColsTd7 ColsAlignLeft" style="text-align:left;padding-left:3px;"><a style="cursor:pointer;" class="cmsdefault" onclick="SPReturnOrder.choosePromotionProgram('<s:property value="programCode" />',<s:property value="ppType" />);"><s:property value="programCode" /></a></td>
										</tr>
									</s:iterator>
								</tbody>

								</tbody>
							</table>
						</div>
					</div>
					<div class="BoxGeneralTFooter">
						<table width="100%" border="0" cellspacing="0" cellpadding="0">
							<colgroup>
								<col style="width: 30px;" />
								<col style="width: 95px;" />
								<col style="width: 134px;" />
								<col style="width: 90px;" />
								<col style="width: 90px;" />
								<!-- <col style="width: 90px;" /> -->
								<col style="width: 70px;" />
								<col style="width: 116px;" />
								<col style="width: 100px;" />
								<col style="width: 100px;" />
							</colgroup>
							<tfoot>
								<tr>
									<td class="ColsTd1" colspan="5" style="text-align: right; padding-right: 29px;">Tổng :</td>
									<td class="ColsTd5" style="text-align: right; padding-right: 5px;">
										<s:if test="totalWeight!=null"> 
											<s:property value="totalWeight" />
										</s:if> 
										<s:else>
											<s:property value="0.0" /> 
										</s:else> kg
									</td>
									<td class="ColsTd6" style="text-align: right; padding-right: 5px;">
										<s:if test="total!=null"> 
											<s:property value="convertMoney(total)" />
										</s:if> 
										<s:else>
												<s:property value="0.0" />
										</s:else> VNĐ
									</td>
									<td class="ColsTd7" colspan="2"></td>

								</tr>
							</tfoot>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="Clear"></div>
</div>

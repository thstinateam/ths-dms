<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<tiles:insertTemplate template='/WEB-INF/jsp/sale-product/common.jsp' />

<div class="BreadcrumbSection">
	<ul class="ResetList FixFloat BreadcrumbList">
		<li class="Sprite1"><a href="javascript:void(0);" class="cmsdefault">Bán hàng</a> </li>
		<li><span>Trả hàng</span> </li>
	</ul>
</div>

<div class="CtnOneColSection">
	<div class="ContentSection">
		<div class="ToolBarSection">
			<div class="SearchSection GeneralSSection">
				<h2 class="Title2Style">Thông tin chung</h2>
				<div class="SearchInSection SProduct1Form">
					<label class="LabelStyle Label2Style">Đơn vị</label>
					<div class="BoxSelect BoxSelect2" id ="shopIdList" >
						<select id="shopCodeCB" style="width: 206px;">
						</select>
					</div>
					<label class="LabelStyle Label1Style">Ngày làm việc</label>
                	<p id="strLockDate" class="ValueStyle Value1Style"><s:property value="lockDate"/></p>
                	<div class="Clear"></div>
					<div id="customerDetail">
						<label class="LabelStyle Label2Style">Số đơn trả</label> 
						<input type="text" class="InputTextStyle InputText1Style" disabled="disabled"  id="returnOrderNumber" /> 
						<label class="LabelStyle Label1Style">Số đơn hàng (F9)</label> 
						<input type="text" class="InputTextStyle InputText1Style" id="poCode"/>
						<div class="Clear"></div>
						
						<label class="LabelStyle Label2Style">Mã KH</label>
						<p class="ValueStyle Value2Style" id="customerCodeHeader"></p>
						<label class="LabelStyle Label1Style">Tên KH</label>
						<p class="ValueStyle Value1Style"></p>
						<label class="LabelStyle Label1Style">Địa chỉ KH</label>
						<p class="ValueStyle Value1Style"></p>
						<div class="Clear"></div>
						
						<label class="LabelStyle Label2Style">NVBH</label>
						<p class="ValueStyle Value1Style" id="staffCodeHeader"></p>
						<label class="LabelStyle Label1Style">NVGH</label>
						<p class="ValueStyle Value1Style"></p>
						<label class="LabelStyle Label1Style">Số ĐT</label>
						<p class="ValueStyle Value1Style"></p>
						<div class="Clear"></div>
						
						<label class="LabelStyle Label2Style">Ngày giao</label>
						<p class="ValueStyle Value1Style"></p>
						<label class="LabelStyle Label1Style">Xe</label>
						<p class="ValueStyle Value1Style"></p>
						<div class="Clear"></div>
						<input id="shopCode" value="<s:property value="shopCode"/>" type="hidden"/>
					</div>
					<div class="Clear"></div>
	
					<h2 id="productH2" class="Title2Style">Danh sách sản phẩm bán
						<img style="display: none;" src="/resources/scripts/plugins/jquery-easyui-1.3.2/themes/default/images/loading.gif" id="loadingProduct">
					</h2>
					<div class="SearchInSection SProduct1Form">
						<div class="GridSection" id="gridContainer" style="padding: 0px;">
							<div id="productGrid"></div>
						</div>
					</div>
					<div class="Clear"></div>
					
					<h2 id="promotionH2" class="Title2Style" style="display:none;">Danh sách sản phẩm khuyến mãi</h2>
					<div class="SearchInSection SProduct1Form">
						<div class="GridSection" id="promotionContainer" style="padding: 0px;">
							<div id="promotionGrid"></div>
						</div>
					</div>
					<div class="Clear"></div>
					
					<h2 id="openPromoH2" class="Title2Style" style="display:none;">Khuyến mãi mở mới</h2>
					<div class="SearchInSection SProduct1Form">
						<div class="GridSection" id="openPromoContainer" style="padding: 0px;">
							<div id="openPromoGrid"></div>
						</div>
					</div>
					<div class="Clear"></div>
					
					<h2 id="orderPromoH2" class="Title2Style" style="display:none;">Khuyến mãi đơn hàng</h2>
					<div class="SearchInSection SProduct1Form">
						<div class="GridSection" id="orderPromoContainer" style="padding: 0px;">
							<div id="orderPromoGrid"></div>
						</div>
					</div>
					<div class="Clear"></div>
					
					<h2 id="accPromoH2" class="Title2Style" style="display:none;">Khuyến mãi đơn hàng</h2>
					<div class="SearchInSection SProduct1Form">
						<div class="GridSection" id="accPromoContainer" style="padding: 0px;">
							<div id="accPromoGrid"></div>
						</div>
					</div>
					<div class="Clear"></div>
					
					<h2 id="h2KS" class="Title2Style" style="display: none;">Trả thưởng chương trình hỗ trợ thương mại vào đơn hàng</h2>
					<div id="keyShop" class="SearchInSection SProduct1Form">
						<div class="GridSection" style="padding: 0px;">
							<div id="gridKeyShop"></div>
						</div>
					</div>
					<div class="Clear"></div>
				</div>
			</div>
			<div class="Clear"></div>
			
			<div class="SearchInSection SProduct1Form" style="font-size: 0.75em;">
				<p class="TotalInfo">
					<span class="Tem1Style">Tổng trọng lượng: 
					<span style="color:red" id="pTotalWeight">
						<s:if test="saleOrder.totalWeight!=null"> 
							<s:property value="saleOrder.totalWeight" />  
						</s:if>
						<s:else>
							<s:property value="0.0" /> 
						</s:else> kg</span> 
					</span>
					<span id="sumPrice">
						<span class="Tem1Style">Tổng tiền: 
						<span style="color:red" id="pAmount">
							<s:if test="saleOrder.amount!=null"> 
								 <s:property value="convertMoney(saleOrder.amount)" />
							 </s:if> 
							 <s:else>
								 <s:property value="0" />
							 </s:else> VNĐ
							 </span> 
						</span>
						<span class="Tem1Style">Chiết khấu: 
						<span style="color:red" id="pDiscount">
							<s:if test="saleOrder.discount!=null"> 
								 <s:property value="convertMoney(saleOrder.discount)" />
							 </s:if>
							 <s:else>
								 <s:property value="0" />
							 </s:else> VNĐ
							 </span> 
						</span>
						<span class="Tem1Style">Tổng tiền thanh toán: 
						<span style="color:red" id="pTotal">
							<s:if test="saleOrder.total!=null"> 
								 <s:property value="convertMoney(saleOrder.total)" />
							 </s:if> 
							 <s:else>
								 <s:property value="0" />
							 </s:else> VNĐ
							 </span> 
						</span>
					</span>
				</p>
				<div class="Clear"></div>
				<p id="errMsg" class="ErrorMsgStyle" style="display: none;">Có lỗi xảy ra khi cập nhật dữ liệu</p>
				<p id="successMsg" class="SuccessMsgStyle" style="display: none;"></p>
				<div class="BtnCenterSection" id="returnProductButton">
					<button id="btnCancel" class="BtnGeneralStyle BtnGeneralDStyle" disabled="disabled" onclick="SPReturnOrder.cancelReturnOrder(<s:property value="saleOrderId"/>);" style="float:left; margin-left:12px;">Hủy</button>
					<button id="btnReturn" class="BtnGeneralStyle BtnGeneralDStyle" disabled="disabled" onclick="SPReturnOrder.returnProductOrder(<s:property value="saleOrderId"/>);" style="float:right; margin-right:12px;">Trả hàng</button>
					<div class="Clear"></div>
				</div>
			</div>
		</div>
	</div>
	<div class="Clear"></div>
</div>

<div id="warningPopupDiv" style="display:none">
	<div id="warningPopup">
		<div class="PopupContentMid">
			<div class="GeneralForm" >
                <div class="icon-warning" style="width: 48px; height: 48px; float: left; margin-right: 20px;"></div>
                <%-- <p style="font-weight: bold; padding: 2px 0px; margin-left: 68px; margin-bottom: 10px;">Bạn có muốn hủy hóa đơn số <span id="invoiceNumber" style="color: #FF6600; font-weight: bold;"></span> không ?</p> --%>
                <p style="padding: 2px 0px; margin-left: 68px; margin-bottom: 10px;">Đơn hàng đã có hóa đơn số <span id="invoiceNumber" style="color: #FF6600; font-weight: bold;"></span>.<br/> Bạn có muốn hủy bỏ số hóa đơn?</p>
                <div class="Clear"></div>
                <div class="BtnCenterSection">
	                <button id="btnOK" class="BtnGeneralStyle cmsdefault">Có</button>
					<button class="BtnGeneralStyle cmsdefault" id="btnNOK">Không</button>
                </div>
			</div>
             </div>
	</div>
</div>
	
<div id="searchBillDialog" style="display: none;">
<div id="popup1">
        <div class="PopupContentMid2">
        	<div class="GeneralForm Search1Form">
                <label class="LabelStyle Label2Style">Số đơn hàng</label>
                <input id ="orderNumber" type="text" class="InputTextStyle InputText5Style" maxlength="20"/>
                <label class="LabelStyle Label7Style">Thực hiện</label>
<!--                 <p class="ValueStyle Value1Style">Presale</p> -->
                <div class="BoxSelect BoxSelect5" id="orderTypeDlgDiv">
                                   <select class="MySelectBoxClass" id="orderType">
									<option value="IN" selected="selected">Presale</option>
									<option value="SO">Vansale</option>
                                   </select>
                </div>
                <div class="Clear"></div>
                <label class="LabelStyle Label2Style">Mã KH (F9)</label>
                <input id ="shortCode" type="text" class="InputTextStyle InputText5Style" maxlength="20"/>
                <label class="LabelStyle Label7Style">Trạng thái</label>
                <p class="ValueStyle Value1Style">Đã duyệt</p>
                <div class="Clear"></div>
                <label class="LabelStyle Label2Style">Tên KH </label>
                <input id="customerName" type="text" class="InputTextStyle InputText5Style" maxlength="250"/>
                <label class="LabelStyle Label7Style">Loại đơn hàng</label>
                <p class="ValueStyle Value1Style">Đơn bán</p>
                <div class="Clear"></div>
                <label class="LabelStyle Label2Style">Từ ngày</label>
                <input type="text" class="InputTextStyle InputText6Style" id="fromDate" maxlength="10" value="<s:property value="displayDate(fromDate)"/>"/>
               
                <label class="LabelStyle Label7Style">Đến ngày</label>
                <input type="text" class="InputTextStyle InputText6Style" id="toDate" maxlength="10" value="<s:property value="displayDate(toDate)"/>"/>
                
                <div class="Clear"></div>
                <label class="LabelStyle Label2Style">NVBH</label>
                <input id="staffCode" type="text" class="InputTextStyle InputText5Style" maxlength="50"/>
                <label class="LabelStyle Label7Style">NVGH</label>
                <input id="deliveryCode" type="text" class="InputTextStyle InputText5Style" maxlength="50"/>
                <div class="Clear"></div>
                <div class="BtnCenterSection">
                    <button class="BtnGeneralStyle BtnSearchOnDialog cmsdefault" id ="btnSearch" onclick="return SPReturnOrder.searchSaleOrder();">Tìm kiếm</button>
                </div>
                <div class="Clear"></div>
            </div>
            <div class="GeneralForm Search1Form">
            	<h2 class="Title2Style Title2MTStyle">Danh sách đơn hàng</h2>
                <div class="GridSection">
                <table id="gridSearchBill"></table>
                </div>
                <div class="Clear"></div>
                <div class="BtnCenterSection">
                    <button class="BtnGeneralStyle cmsdefault" onclick="$('#popup1').dialog('close');">Đóng</button>
                </div>
            </div>
            <div class="Clear"></div>
            <p class="ErrorMsgStyle" id="errorSearchMsg" style="display: none;"></p>
		</div>
    </div>
</div>

<div id="PopupShowCustomerInfoDiv" style="display: none;">
<div id="PopupShowCustomerInfo">
        <div class="PopupContentMid2">
        	<div class="GeneralForm Search1Form">
                <label class="LabelStyle Label4Style">Mã khách hàng</label>
                <p class="ValueStyle Value1Style" id="shortCodep"><s:property value="customerVo.shortCode" /></p>
                <div class="Clear"></div>
                <label class="LabelStyle Label4Style">Tên khách hàng</label>
                <p class="ValueStyle Value1Style" id="customerNamep" style="width:auto;"><s:property value="customerVo.customerName" /></p>
                <div class="Clear"></div>
                <label class="LabelStyle Label4Style">Đia chỉ</label>
                <p class="ValueStyle Value1Style" id="addressp" style="width:auto;"><s:property value="customerVo.address" /></p>
                <div class="Clear"></div>
                <label class="LabelStyle Label4Style">Số điện thoại</label>
                <p class="ValueStyle Value1Style" id="phonep" style="width:auto;"><s:if test="customerVo.phone!=null"><s:property value="customerVo.phone" /></s:if>
                	<s:else><s:property value="" /></s:else></p>
                <div class="Clear"></div>
                <label class="LabelStyle Label4Style">Di động</label>
                <p class="ValueStyle Value1Style" id="mobiphonep" style="width:auto;"><s:if test="customerVo.mobiphone!=null"><s:property value="customerVo.mobiphone" /></s:if>
                	<s:else><s:property value="" /></s:else></p>
                <div class="Clear"></div>               
                <label class="LabelStyle Label4Style">Loại cửa hàng</label>
                <p class="ValueStyle Value1Style" id="shopTypeNamep" style="width:auto;"><s:property value="customerVo.shopTypeName" /></p>
                <div class="Clear"></div>
                <label class="LabelStyle Label4Style">Độ trung thành</label>
                <p class="ValueStyle Value1Style" id="loyaltyp" style="width:auto;"><s:property value="customerVo.loyalty" /></p>
                <div class="Clear"></div>
                <label class="LabelStyle Label4Style">Người liên hệ</label>
                <p class="ValueStyle Value1Style" id="contactNamep" style="width:auto;"><s:property value="customerVo.contactName" /></p>
                <div class="Clear"></div>
                <label class="LabelStyle Label4Style">Bán trong ngày</label>
                <p class="ValueStyle Value1Style" id="totalInDatep"><s:property value="convertMoney(customerVo.totalInDate)" /></p>
                <div class="Clear"></div>
            </div>
            <div class="GeneralForm Search1Form">
                <h2 class="Title2Style">Thông tin doanh số</h2>
                <label class="LabelStyle Label5Style">Số lần đã đặt hàng trong tháng</label>
                <p class="ValueStyle Value1Style" id="numOrderInMonthp"><s:property value="convertMoney(customerVo.numOrderInMonth)" /></p>
                <div class="Clear"></div>
                <label class="LabelStyle Label5Style">Bình quân doanh số 2 tháng trước</label>
                <p class="ValueStyle Value1Style" id="avgTotalInLastTwoMonthp"><s:property value="convertMoney(customerVo.avgTotalInLastTwoMonth)" /></p>
                <div class="Clear"></div>
                <label class="LabelStyle Label5Style">Doanh số đã thực hiện trong tháng</label>
                <p class="ValueStyle Value1Style" id="totalInMonthp"><s:property value="convertMoney(customerVo.totalInMonth)" /></p>
                <div class="Clear"></div>
            </div>
            <div class="GeneralForm Search1Form">
            	<h2 class="Title2Style Title2MTStyle">Danh sách đơn hàng gần nhất</h2>
                <div class="GridSection">
					<table id="gridCustomerInfo"></table>
					<!-- <div id="gridCustomerInfoPager"></div> -->
                </div>
                <div class="Clear"></div>
                <div class="BtnCenterSection">
                    <button class="BtnGeneralStyle cmsdefault" onclick="$('#PopupShowCustomerInfo').dialog('close');">Đóng</button>
                </div>
            </div>
		</div>
    </div>
</div>

<div id="showProductInfoContainerRT" style="display: none;">
<div id="showProductInfoRT">
      <div class="PopupContentMid2">
      	<div class="GeneralForm Search1Form">
			<label class="LabelStyle Label2Style">Mã SP</label>
            <p class="ValueStyle Value1Style" id="productCode"></p>
            <label class="LabelStyle Label2Style">Tên SP</label>
            <p class="ValueStyle Value1Style" id="productName"></p>
            <div class="Clear"></div>
            <label class="LabelStyle Label2Style">Quy cách</label>
            <p class="ValueStyle Value1Style" id="convfactNumber"></p>
            <label class="LabelStyle Label2Style">Tồn kho</label>
            <p class="ValueStyle Value1Style" id="stockNumber"></p>
            <div class="Clear"></div>
            <label class="LabelStyle Label2Style">Giá</label>
            <p class="ValueStyle Value1Style" id="priceValue"></p>
            <label class="LabelStyle Label2Style">Tổng số hàng</label>
            <p class="ValueStyle Value1Style" id="totalQuantity"></p>
            <div class="Clear"></div>
            <label class="LabelStyle Label2Style">Mã khuyến mãi</label>
            <p class="ValueStyle Value1Style" id="promotionProgram" style="width:auto;"></p>
            <div class="Clear"></div>
          </div>
	</div>
</div>
</div>

<div id="PopupShowPromotionProgramInfoDiv" style="display: none;">
<div id="PopupShowPromotionProgramInfo">
        <div class="PopupContentMid2">
        	<div class="GeneralForm Search1Form">            	
                <label class="LabelStyle Label4Style">Mã chương trình</label>
                <p class="ValueStyle Value1Style" id="programCode" style="width:auto;"></p>
                <div class="Clear"></div>
                <label class="LabelStyle Label4Style">Tên chương trình</label>
                <p class="ValueStyle Value1Style" id="programName" style="width:auto;"></p>
                <div class="Clear"></div>
                <label class="LabelStyle Label4Style">Loại khuyến mại</label>
                <p class="ValueStyle Value1Style" id="programType"></p>
                <div class="Clear"></div>
                <label class="LabelStyle Label4Style">Dạng chương trình</label>
                <p class="ValueStyle Value1Style" id="programFormat" style="width:auto;"></p>
                <div class="Clear"></div>
                <label class="LabelStyle Label4Style">Từ ngày</label>
                <p class="ValueStyle Value1Style" id="fromDatePP"></p>
                <div class="Clear"></div>               
                <label class="LabelStyle Label4Style">Đến ngày</label>
                <p class="ValueStyle Value1Style" id="toDatePP"></p>
                <div class="Clear"></div>
                <label class="LabelStyle Label4Style">Diễn giải chương trình</label>
                <p class="ValueStyle Value1Style" id="programDescription" style="width:auto;"></p>
                <div class="Clear"></div>
                <div class="BtnCenterSection">
                    <button class="BtnGeneralStyle cmsdefault" onclick="$('#PopupShowPromotionProgramInfo').dialog('close');">Đóng</button>
                </div>
                
            </div>
		</div>
    </div>
</div>

<div id ="searchCustomerEasyUIDialogDiv" style="display: none" >
	<div id="searchCustomerEasyUIDialog">
		<div class="PopupContentMid">
			<div class="GeneralForm Search1Form" >
				<label id="seachStyle1CodeLabel" class="LabelStyle Label1Style" style=" width: 100px;">Mã</label>
				<input id="seachStyle1Code" maxlength="20" tabindex="1" type="text" style=" width: 145px;" class="InputTextStyle InputText1Style" /> 
				<label id="seachStyle1NameLabel" class="LabelStyle Label1Style" style=" width: 100px;">Tên</label> 
				<input id="seachStyle1Name" type="text" maxlength="20" tabindex="2" style="width: 145px;"  class="InputTextStyle InputText1Style" />
				<div class="Clear"></div>
                <label id="seachStyle1AddressLabel" style="display: none;width: 100px;" class="LabelStyle Label1Style">Địa chỉ</label>
                <input id="seachStyle1Address" type="text" style="display: none;width: 409px;"  maxlength="250" tabindex="3" class="InputTextStyle InputText1Style" />
                <div class="Clear"></div>
				<button class="BtnGeneralStyle BtnSearchStyle BtnSearchOnDialog" tabindex="3" id="btnsearchCustomer">Tìm kiếm</button>
				<input type="hidden" name="" id="searchCustomerUrl"/>
				<input type="hidden" name="" id="searchCustomerCodeText"/>
				<input type="hidden" name="" id="searchCustomerNameText"/>
				<input type="hidden" name="" id="searchCustomerIdText"/>
				<s:hidden id="searchCustomerAddressText"></s:hidden>
				<div class="Clear"></div>
				<div class="GridSection" id="searchCustomerContainerGrid">					
					<table id="searchCustomerGrid"></table>
					<!-- <div id="searchCustomerPager"></div> -->
				</div>
				<div class="BtnCenterSection">
					<button id="__btnSave" style="display: none;" class="BtnGeneralStyle cmsdefault">Chọn</button>
					<button class="BtnGeneralStyle BtnGeneralMStyle cmsdefault" onclick="$('#searchCustomerEasyUIDialog').dialog('close');">Đóng</button>
				</div>
				<p id="errMsgSearch" style="display: none;" class="ErrorMsgStyle SpriteErr"></p>
			</div>
		</div>
	</div>
</div>

<s:hidden id="saleOrderReturnId" value="0"></s:hidden>
<s:hidden id="date" name="fromDate"></s:hidden>
<s:hidden id="isReturnKeyShop" name="isReturnKeyShop"></s:hidden>

<script type="text/javascript">
	$(document).ready(function() {
		if ($('#isReturnKeyShop').val() == 'false') {
			SPReturnOrder._isReturnKeyShop = false;
		}
		SPReturnOrder.initUnitCbx();
		$('#poCode').focus();
		
		$('#poCode').live('keyup', function(event) {
			if (event.keyCode == keyCode_F9) {				
			 	SPReturnOrder.openSearchBillDialog();
			}
			return false;
		});
		$('#shortCode').live('keyup', function(event){
			if(event.keyCode == keyCode_F9){
				var arrParam = new Array();
				var param = {};
				param.name = "shopCode";
				param.value = $('#shopCodeCB').combobox('getValue');
				arrParam.push(param);
				SPReturnOrder.searchCustomerEasyUIOnDialog(function(data) {
					$('#shortCode').val(data.code);
					$('#customerName').val(data.name);
				}, arrParam);
			}
		});

	});
</script>
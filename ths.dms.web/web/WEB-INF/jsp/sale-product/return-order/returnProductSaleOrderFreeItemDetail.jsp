<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>

<h2 class="Title2Style">Sản phẩm Khuyến mãi</h2>
<div class="SearchInSection SProduct1Form">
	<div class="GridSection">
		<div class="GeneralMilkInBox">
			<div class="GeneralTable">
				<div class="BoxGeneralTTitle">
					<table width="100%" border="0" cellspacing="0" cellpadding="0">
						<colgroup>
							<col style="width: 44px;" />
							<col style="width: 134px;" />
							<col style="width: 237px;" />
							<col style="width: 119px;" />
							<!-- <col style="width: 119px;" /> -->
							<col style="width: 113px;" />
							<col style="width: 144px;" />
						</colgroup>
						<thead>
							<tr>
								<th class="ColsThFirst">STT</th>
								<th>Mã sản phẩm</th>
								<th>Tên sản phẩm</th>
								<th>Kho</th>
								<!-- <th>Lô</th> -->
								<th>Tổng</th>
								<th class="ColsThEnd">Khuyến mãi</th>
							</tr>
						</thead>
					</table>
				</div>

				<div class="BoxGeneralTBody" id="saleOrderFreeItemDetail">
					<div class="ScrollBodySection">
						<table width="100%" border="0" cellspacing="0" cellpadding="0">
							<colgroup>
								<col style="width: 44px;" />
								<col style="width: 134px;" />
								<col style="width: 237px;" />
								<col style="width: 119px;" />
								<!-- <col style="width: 119px;" /> -->
								<col style="width: 113px;" />
								<col style="width: 144px;" />
							</colgroup>
							<tbody>
								<s:if test="listSaleOrderDetailVOEx != null">
								<s:iterator value="listSaleOrderDetailVOEx" status="stt">
									<tr>
										<td class="ColsTd1" style="text-align:center;"><s:property value="#stt.index+1" /></td>
										<td class="ColsTd2 ColsAlignLeft" style="text-align:left;padding-left:3px;">
											<a style="cursor:pointer;" class="cmsdefault" onclick="return SPReturnOrder.showProductInfo('<s:property value="productCode"/>', 
											'<s:property value="productName"/>', <s:property value="convfact"/>,'<s:property value="cellQuantityFormatter(stockQuantity,convfact)"/>', '<s:property value="convertMoney(price)" />', 
											'<s:property value="stockQuantity"/>','<s:property value="programCode"/>');"><s:property value="productCode" />
											</a>
										</td>
										<td class="ColsTd3 ColsAlignLeft" style="text-align:left;padding-left:3px;"><s:property value="productName"/></td>
										<td class="ColsTd3 ColsAlignLeft" style="text-align:left;padding-left:3px;"><s:property value="warehouseName"/></td>
										<!-- <td class="ColsTd3 ColsAlignLeft" style="text-align:left;padding-left:3px;"></td> -->
										<td class="ColsTd6 ColsAlignRight" style="text-align:right;padding-right:3px;"><s:property value="quantity" /></td>
										<td class="ColsTd9 ColsTdEnd ColsAlignLeft" style="text-align:left;padding-left:3px;"><a style="cursor:pointer;" class="cmsdefault" onclick="SPReturnOrder.choosePromotionProgram('<s:property value="programCode" />',<s:property value="ppType" />);"><s:property value="programCode" /></a></td>
									</tr>
								</s:iterator>
								</s:if>
								<s:else>
								<tr><td colspan="7">Không có dữ liệu trả về.</td> </tr>
								</s:else>
							</tbody>
						</table>
					</div>
					<div class="BoxGeneralTFooter">
						<table width="100%" border="0" cellspacing="0" cellpadding="0">
							<colgroup>
								<col style="width: 44px;" />
								<col style="width: 134px;" />
								<col style="width: 237px;" />
								<col style="width: 119px;" />
								<!-- <col style="width: 119px;" /> -->
								<col style="width: 113px;" />
								<col style="width: 144px;" />
							</colgroup>
							<tfoot>
								<tr>
									<td class="ColsTd1" colspan="4" style="text-align: right; padding-right: 20px;">Tổng :</td>
									<td class="ColsTd2" style="text-align: right; padding-right: 3px;">
									<s:if test="totalPrmWeight!=null"> 
											<s:property value="totalPrmWeight" />  
										</s:if> 
										<s:else>
											<s:property value="0.0" /> 
										</s:else> kg
									</td>
									<%-- <td class="ColsTd3" style="text-align: right; padding-right: 3px;">
										<s:if test="sumDiscountAmount!=null"> 
											<s:property value="convertMoney(saleOrder.total)" />
										</s:if> 
										<s:else>
												<s:property value="0.0" />
										</s:else> VNĐ
									</td> --%>
									<td class="ColsTd4"></td>
								</tr>
							</tfoot>
						</table>
					</div>
				</div>

			</div>
		</div>
	</div>
	<div class="Clear"></div>
	<p class="TotalInfo">
		<span class="Tem1Style">Tổng trọng lượng: 
		<span style="color:red">
			<s:if test="totalWeight!=null"> 
				<s:property value="totalWeight" />  
			</s:if> 
			<s:else>
				<s:property value="0.0" /> 
			</s:else> kg</span> 
		</span>
		<span class="Tem1Style">Tổng tiền: 
		<span style="color:red">
			<s:if test="totalAmount!=null"> 
				 <s:property value="convertMoney(totalAmount)" />
			 </s:if> 
			 <s:else>
				 <s:property value="0.0" />
			 </s:else> VNĐ
			 </span> 
		</span>
		<span class="Tem1Style">Chiết khấu: 
		<span style="color:red">
			<s:if test="sumDiscountAmount!=null"> 
				 <s:property value="convertMoney(sumDiscountAmount)" />
			 </s:if>
			 <s:else>
				 <s:property value="0.0" />
			 </s:else> VNĐ
			 </span> 
		</span>
		<span class="Tem1Style">Tổng tiền thanh toán: 
		<span style="color:red">
			<s:if test="total!=null"> 
				 <s:property value="convertMoney(total)" />
			 </s:if> 
			 <s:else>
				 <s:property value="0.0" />
			 </s:else> VNĐ
			 </span> 
		</span>
	</p>
	<div class="BtnCenterSection" id="returnProductButton">
		<button id="btnCancel" class="BtnGeneralStyle" onclick="SPReturnOrder.cancelReturnOrder(<s:property value="saleOrderId"/>);" style="float:left; margin-left:12px;">Hủy</button>
		<button id="btnReturn" class="BtnGeneralStyle" onclick="SPReturnOrder.returnProductOrder(<s:property value="saleOrderId"/>);" style="float:right; margin-right:12px;">Trả hàng</button>
		<div class="Clear"></div>
	<script type="text/javascript">
		/* if (!_isFullPrivilege) {
			if (_MapControl.get("btnReturn") == 3) {
				$("#btnReturn").attr("disabled", "disabled");
				$("#btnReturn").addClass("BtnGeneralDStyle");
				$("#btnReturn").attr("onclick", "");
			} else if (_MapControl.get("btnReturn") != 2) {
				$("#btnReturn").remove();
			}
			if (_MapControl.get("btnCancel") == 3) {
				$("#btnCancel").attr("disabled", "disabled");
				$("#btnCancel").addClass("BtnGeneralDStyle");
				$("#btnCancel").attr("onclick", "");
			} else if (_MapControl.get("btnCancel") != 2) {
				$("#btnCancel").remove();
			}
		} */
	</script>
	</div>
	<p id="errMsg" class="ErrorMsgStyle" style="display: none;">Có lỗi xảy ra khi cập nhật dữ liệu</p>
	<p id="successMsg" class="SuccessMsgStyle" style="display: none;"></p>
	<div class="Clear"></div>
</div>

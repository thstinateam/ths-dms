<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<div id="ScrollSection" class="ScrollSection">
	<div class="BoxGeneralTTitle">
		<table width="100%" border="0" cellspacing="0" cellpadding="0">
			<colgroup>
				<col style="width: 30px;" />
				<col style="width: 20px;" />
				<col style="width: 120px;" />
				<col style="width: 120px;" />
				<col style="width: 80px;" />				
				<col style="width: 198px;" />
				<col style="width: 80px;" />
				<col style="width: 70px;" />
				<col style="width: 135px;" />
				<col style="width: 70px;" />
			</colgroup>
			<thead>
				<tr>
					<th class="ColsThFirst">STT</th>
					<th><input type="checkbox" class="checkAll" />
					</th>
					<th>Hóa đơn nội bộ</th>
					<th>Hóa đơn đỏ</th>
					<th>Thanh toán</th>					
					<th>Khách hàng</th>
					<th>NVBH</th>
					<th>Ngày</th>
					<th>Thành tiền</th>
					<th>Thuế suất</th>
				</tr>
			</thead>
		</table>
	</div>
	<div class="BoxGeneralTBody">
		<div id="notExists" class="ScrollBodySection">
			<table width="100%" border="0" cellspacing="0" cellpadding="0">
				<colgroup>
					<col style="width: 30px;" />
					<col style="width: 20px;" />
					<col style="width: 120px;" />
					<col style="width: 120px;" />
					<col style="width: 80px;" />					
					<col style="width: 198px;" />
					<col style="width: 80px;" />
					<col style="width: 70px;" />
					<col style="width: 135px;" />
					<col style="width: 70px;" />
				</colgroup>
				<tbody>
					<s:iterator id="obj" value="listInvoice" status="stt">
						<tr>
							<td class="ColsTd1 AlignCenter"><s:property value="#stt.index+1" />
							</td>
							<td class="ColsTd2 AlignCenter"><input id="CheckBox_<s:property value="#obj.id" />" type="checkbox" class="checkboxList CheckBox_<s:property value="#obj.id" />" 
									name="<s:property value="#obj.id" />" />
							</td>
							<td class="ColsTd1"><s:property value="#obj.orderNumber" /></td>
							<td class="ColsTd1"><s:property value="#obj.invoiceNumber" /></td>
							<s:if test="#obj.custPayment.value==1">
								<td class="ColsTd1">Tiền mặt</td>
							</s:if><s:elseif test="#obj.custPayment.value==2">
								<td class="ColsTd1">Chuyển khoản</td>
							</s:elseif>
							<s:else><td class="ColsTd1"></td></s:else>							
							<td class="ColsTd1"><s:property value="#obj.customerCode" /> - <s:property value="#obj.custName" /></td>
							<td class="ColsTd1"><s:property value="#obj.staff.staffCode" /></td>
							<td class="ColsTd1 AlignCenter"><s:date name="#obj.orderDate" format="dd/MM/yyyy" /></td>
							<td class="ColsTd13 AlignRight"><s:property value="convertMoney(#obj.amount)" /></td>
							<td class="ColsTdEnd AlignRight" ><s:property value="#obj.vat" /> %</td>
						</tr>
					</s:iterator>
				</tbody>
			</table>
			<br />
		</div>
	</div>
</div>


<script type="text/javascript">
	$(function() {		
// 		$('#ScrollSection').jScrollPane();				//Scroll bar
// 		$('#notExists').jScrollPane();
// 		$('.checkAll').click(function() {
// 			$('.checkboxList').attr('checked', this.checked);
// 		});
// 		$('.checkboxList').click( function() {
// 	         if(this.checked==false){
// 	             $('.checkAll').attr('checked',false);
// 	         }
	        	 
// 	    });
	});
</script>
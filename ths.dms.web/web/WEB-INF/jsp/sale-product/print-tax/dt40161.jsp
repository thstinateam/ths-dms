<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<style>
table, tr, td {
	font-family: Times New Roman;
	font-size: 10px;
}
</style>
<s:set id="idx" value="0"></s:set>
<s:iterator value="listObjectBeans">
<div style="width: 210mm; height: 245mm; page-break-after: always; page-break-inside: avoid;">
	<table style="width: 100%; padding-top: 12px;">
		<tr style="visibility: hidden;"><!-- 1 -->
			<td rowspan="2" style="text-align: center; width: 248px; font-weight: bold;">CÔNG TY TNHH THƯƠNG MẠI VÀ THỰC PHẨM TÀI LỘC</td>
			<td rowspan="3" style=" font-size: 15px; font-weight: bold; text-align: center; width: 215px;">HÓA ĐƠN GIÁ TRỊ GIA TĂNG</td>
			<td>Mẫu số 013/001</td>
		</tr>
		<tr style="visibility: hidden;"><!-- 2 -->
			<td>Ký hiệu: TL/HP</td>
		</tr>
		<tr style="visibility: hidden;"><!-- 3 -->
			<td>Địa chỉ: 1283 Phạm Thế Hiển, Phường 5, Quận 8, TP.HCM</td>
			<td>Số: 0028154</td>
		</tr>
		<tr><!-- 7 -->
			<td></td>
			<td style="font-size: 13px;"><span style="visibility: hidden; padding-left: 78px;">Ngày....</span><span style="font-size: 15px;"><s:property value="ngay"/></span><span style="visibility: hidden;">......</span><span style="visibility: hidden;">tháng......</span><span style="font-size: 15px;"><s:property value="thang"/></span><span style="visibility: hidden;">.....</span><span style="visibility: hidden;">năm.....</span><span style="font-size: 15px;"><s:property value="nam"/></span><span style="visibility: hidden;">..</span></td>
			<td></td>
		</tr>
	</table>
	<table style="width: 50%; float: left; line-height: 13px; padding-top: 18px;">
		<tr style="line-height: 15px;"><!-- 1 -->
			<td><span style="visibility: hidden;">Đơn vị bán hàng: </span><span style="font-size: 15px; padding-left : 55px;"><s:property value="saleOrder.shop.shopName"/></span></td>
		</tr>
		<tr style="line-height: 15px;"><!-- 2 -->
			<td><span style="visibility: hidden;">Địa chỉ: </span><span style="font-size: 15px; padding-left : 30px;"><s:property value="saleOrder.shop.address"/></span></td>
		</tr>
		<tr style="line-height: 15px;"><!-- 3 -->
			<td><span style="visibility: hidden;">Số tài khoản: </span><span style="font-size: 15px; padding-left : 48px;"><s:property value="saleOrder.shop.invoiceNumberAccount"/></span>    <span style="float:right; padding-right:10px"><span style="visibility: hidden;">NH: </span><span style="font-size: 15px;"><s:property value="saleOrder.shop.invoiceBankName"/></span></span></td>
		</tr>
		<tr style="line-height: 15px;"><!-- 4 -->
			<td><span style="visibility: hidden;">Nhân viên giao hàng: </span><span style="font-size: 15px; padding-left : 80px;"><s:property value="saleOrder.delivery.staffName"/></span></td>
		</tr>
		<tr style="line-height: 15px;"><!-- 5 -->
			<td><span style="visibility: hidden;">Nhân viên bán hàng: </span><span style="font-size: 15px; padding-left : 75px;"><s:property value="saleOrder.staff.staffName"/></span></td>
		</tr>
		<tr style="line-height: 15px;"><!-- 6 -->
			<td><span style="visibility: hidden;">Số HĐ nội bộ: </span><span style="font-size: 15px; padding-left : 70px;"><s:property value="saleOrder.orderNumber"/></span></td>
		</tr>
		<tr style="line-height: 15px;"><!-- 7 -->
			<td><span style="visibility: hidden;">Ngày in: </span><span style="font-size: 15px; padding-left : 65px;"><s:property value="printDay"/></span></td>
		</tr>
	</table>
	<table style="width: 50%; line-height: 14px; padding-top: 18px;">
		<tr style="line-height: 15px;"><!-- 1 -->
			<td style="overflow-x: hidden; display: -moz-box;"><span style="visibility: hidden;">Họ tên người mua hàng: </span><span style="font-size: 15px; padding-left : 80px;"><s:property value="saleOrder.customer.customerName"/></span></td>
		</tr>
		<tr style="line-height: 15px;"><!-- 2 -->
			<td style="overflow-x: hidden; display: -moz-box;"><span style="visibility: hidden;">Tên đơn vị: </span><span style="font-size: 15px; padding-left : 50px;"><s:property value="saleOrder.customer.invoiceConpanyName"/></span></td>
		</tr>
		<tr style="line-height: 15px;"><!-- 3 -->
			<td><span style="visibility: hidden;">Mã số thuế: </span><span style="font-size: 15px; padding-left : 55px;"><s:property value="saleOrder.customer.invoiceTax"/></span>    <span style="float:right; padding-right: 50px;"><span style="visibility: hidden;">Mã KH: </span><span style="font-size: 15px;"><s:property value="saleOrder.customer.shortCode"/></span></span></td>
		</tr>
		<tr style="line-height: 15px;"><!-- 4 -->
			<td style="overflow-x: hidden; display: -moz-box;"><span style="visibility: hidden;">Địa chỉ: </span><span style="font-size: 15px; padding-left : 35px;"><s:property value="diachiHD"/></span></td>
		</tr>
		<tr style="line-height: 15px;"><!-- 5 -->
			<td style="overflow-x: hidden; display: -moz-box;"><span style="visibility: hidden;">Hình thức thanh toán: </span><span style="font-size: 15px; padding-left : 70px;"><s:property value="custPayment"/></span></td>
		</tr>
		<tr style="line-height: 15px;"><!-- 6 -->
			<%-- <s:if test="saleOrder.customer.deliveryAddress != null && saleOrder.customer.deliveryAddress.length() > 30">
				<s:if test="saleOrder.customer.deliveryAddress == null">
					<td><span style="visibility: hidden;">Địa chỉ giao hàng: </span><span style="font-size: 15px; padding-left : 15px;"><s:property value="saleOrder.customer.address"/></span></td>									
				</s:if>
				<s:else>
					<td><span style="visibility: hidden;">Địa chỉ giao hàng: </span><span style="font-size: 15px; padding-left : 15px;"><s:property value="saleOrder.customer.deliveryAddress"/></span></td>
				</s:else>
			</s:if>
			<s:else>
				<s:if test="saleOrder.customer.deliveryAddress == null">
					<td><span style="visibility: hidden;">Địa chỉ giao hàng: </span><span style="font-size: 15px; padding-left : 35px;"><s:property value="saleOrder.customer.address"/></span></td>
				</s:if>
				<s:else>
					<td><span style="visibility: hidden;">Địa chỉ giao hàng: </span><span style="font-size: 15px; padding-left : 35px;"><s:property value="saleOrder.customer.deliveryAddress"/></span></td>				
				</s:else>
			</s:else> --%>			
			<td style="overflow-x: hidden; display: -moz-box;"><span style="visibility: hidden;">Địa chỉ giao hàng: </span><span style="font-size: 15px; padding-left : 15px;"><s:property value="diachiKH"/></span></td>
		</tr>
		<tr style="line-height: 15px;"><!-- 7 -->
			<td><span style="visibility: hidden;">Số tài khoản: </span><span style="font-size: 15px; padding-left : 65px;"><s:property value="saleOrder.customer.invoiceNumberAccount"/></span>    <span style="float:right; padding-right:10px;"><span style="visibility: hidden;">NH: </span><span style="font-size: 15px;"><s:property value="saleOrder.customer.invoiceNameBank"/></span></span></td>
		</tr>
	</table>
	<table style="width: 100%;">
		<tr style="text-align: center; font-weight: bold; visibility: hidden; height: 47px;">
			<td style="width: 25px;">STT</td><!-- 1 -->
			<td style="width: 260px;">Tên hàng hóa, dịch vụ</td><!-- 2 -->
			<td style="width: 64px;">Đơn vị tính</td><!-- 3 -->
			<td style="width: 90px;">Số lượng</td><!-- 4 -->
			<td style="width: 90px;">Đơn giá</td><!-- 5 -->
			<td style="width: 150px;">Thành tiền</td><!-- 6 -->
			<td style="text-align: center;" rowspan="2">Ghi chú</td><!-- 7 -->
		</tr>
		<tr style="visibility: hidden;">
			<td style="text-align: center;">1</td>
			<td style="text-align: center;">2</td>
			<td style="text-align: center;">3</td>
			<td style="text-align: center;">4</td>
			<td style="text-align: center;">5</td>
			<td style="text-align: center;">6 = 4 x 5</td>
		</tr>
	</table>
	<div style="height: 480px;">
		<table style="width: 97%; margin-left: 10px;">
			<s:set id="loopIdx" value="1"></s:set>
			<s:iterator value="listDetail" status="stt">
				<tr style="line-height: 20px;">
					<td style="text-align: center;width: 25px;  font-size: 13px;"><s:property value="#loopIdx"/></td>
					<td style="width: 270px;  font-size: 13px; display: inline-block;"><div style="padding-left: 15px;"><s:property value="product.productCode"/> - <s:property value="product.productName"/></div></td>
					<td style="text-align: center;width: 54px;  font-size: 13px;"><div style="padding-left: 10px; float: left;"><s:property value="product.uom1"/></div></td>
					<td style="text-align: right;width: 80px;  font-size: 13px;"><div style="padding-left: 10px;"><s:property value="displayNumberVAT(quantity)"/></div></td>
					<td style="text-align: right;width: 80px;  font-size: 13px;"><div style="padding-left: 10px;"><s:property value="displayNumberVAT(priceNotVat)"/></div></td>
					<td style="text-align: right;width: 200px;  font-size: 13px;"><div style="padding-left: 10px;"><s:property value="displayNumberVAT(discountAmount)"/></div></td>
					<td style="  font-size: 13px; width: 80px;"><div style="padding-left: 25px;"><s:property value="createUser"/></div></td>
					<s:set id="loopIdx" value="#loopIdx + 1"></s:set>
				</tr>	
			</s:iterator>
			<tr>
				<td colspan="7" style="font-size: 15px; padding-left: 45px;">           </td>
			</tr>
			<s:if test="listDetailFree.size() > 0">
				<tr>
					<td colspan="7" style="font-size: 15px; padding-left: 45px;">Hàng quảng cáo, khuyến mãi, hàng mẫu không thu tiền</td>
				</tr>
			</s:if>
			<s:iterator value="listDetailFree" status="stt">
				<tr style="line-height: 20px;">
					<td style="text-align: center;width: 25px;  font-size: 13px;"><s:property value="#loopIdx"/></td>
					<td style="width: 270px;  font-size: 13px; display: inline-block;"><div style="padding-left: 15px;"><s:property value="product.productCode"/> - <s:property value="product.productName"/></div></td>
					<td style="text-align: center;width: 54px;  font-size: 13px;"><div style="padding-left: 10px; float: left;"><s:property value="product.uom1"/></div></td>
					<td style="text-align: right;width: 80px;  font-size: 13px;"><div style="padding-left: 10px;"><s:property value="displayNumberVAT(quantity)"/></div></td>
					<td style="text-align: right;width: 80px;  font-size: 13px;"></td>
					<td style="text-align: right;width: 200px;  font-size: 13px;"></td>
					<td style=" font-size: 13px; width: 80px;"><div style="padding-left: 25px;"><s:property value="createUser"/></div></td>
					<s:set id="loopIdx" value="#loopIdx + 1"></s:set>
				</tr>
			</s:iterator>
			<s:if test="invoice.discount > 0">
				<tr>
					<td colspan="5" style="font-size: 15px; padding-left: 45px;">Chiết khấu thương mại</td>
					<td style="text-align: right;width: 120px;  font-size: 15px;"><div style="padding-left: 10px;"><s:property value="displayNumberVAT(invoice.discount)"/></div></td>
				</tr>
			</s:if>
		</table>
	</div>
	<table style="width: 100%;">
		<tr style="height: 25px;">
			<td colspan="5" style="width: 389px; text-align: right;"><span style="visibility: hidden;">Cộng tiền hàng:</span></td>
			<td style="width: 125px; text-align: right; font-size: 15px;"><s:property value="displayNumberVAT(amount)"/> VNĐ</td>
			<td style="width: 63px;"></td>
		</tr>
		<tr style="height: 25px;">
			<td colspan="2" style="width: 214px; font-size: 15px;"><span style="visibility: hidden;">Thuế suất GTGT: ....</span><span style="padding-left:60px;"><s:number name="invoice.vat" type="integer"/></span><span style="visibility: hidden;">....%</span></td>
			<td colspan="3" style="width: 175px; text-align: right;"><span style="visibility: hidden;">Tiền thuế GTGT:</span></td>
			<td style="width: 125px; text-align: right; font-size: 15px;"><s:property value="displayNumberVAT(amountVAT)"/> VNĐ</td>
			<td></td>
		</tr>
		<tr style="height: 25px;">
			<td colspan="5" style="width: 389px; text-align: right;"><span style="visibility: hidden;">Tổng cộng tiền thanh toán:</span></td>
			<td style="width: 125px; text-align: right; font-size: 15px;"><s:property value="displayNumberVAT(totalAmount - invoice.discount)"/> VNĐ</td>
			<td></td>
		</tr>
		<tr style="height: 25px;">
			<td colspan="7" style="font-size: 15px;"><span style="visibility: hidden;padding-left: 100px;">Số tiền viết bằng chữ:...</span><s:property value="moneyString"/><span style="visibility: hidden;">....</span></td>
		</tr>
	</table>
	<table style="width: 100%; padding-top: 93px;">
		<tr>
			<td style="font-size: 17px;">Bán hàng qua ĐH, Điện Thoại</td>
			<td style="font-size: 17px;">Nguyễn Thị Thùy Quyên</td>
			<td style="font-size: 17px;">Đặng Thị Thùy Linh</td>
		</tr>
	</table>
</div>
</s:iterator>
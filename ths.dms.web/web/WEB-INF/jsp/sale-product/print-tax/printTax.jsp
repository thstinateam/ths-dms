<%@page import="ths.dms.helper.Configuration"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>

<input type="hidden" id="function_code" value="<s:property value="systimestamp"/>" />
<s:set id="downloadToken" value="%{generateReportToken()}"></s:set>
<input type="hidden" id="report_token" value="<s:property value="#downloadToken"/>" />
<s:set id="downloadToken" value="%{putSessionValue(systimestamp, #downloadToken)}"></s:set>
		
<input type="hidden" id="shopCode" value="<s:property value='shop.getShopCode()'/>" /> 
<div class="BreadcrumbSection">
	<ul class="ResetList FixFloat BreadcrumbList">
		<li class="Sprite1"><a href="/sale-product/print-tax/info">Bán hàng</a></li>
		<li><span>In hóa đơn thuế GTGT</span></li>
	</ul>
</div>
<div class="CtnOneColSection">
	<div class="ContentSection">
		<div class="ToolBarSection">
			<div class="SearchSection GeneralSSection">
				<h2 class="Title2Style">Thông tin tìm kiếm</h2>
				<div class="SearchInSection SProduct1Form">
					<label class="LabelStyle Label2Style">Mã NPP<span class="ReqiureStyle">*</span></label>
                	<div class="BoxSelect BoxSelect2">
                		<input  type="text" class="InputTextStyle InputText1Style" id="cbxShop" maxlength="50" />
                	</div>
                	<label class="LabelStyle Label7Style">Ngày làm việc:</label>
                	<p id="strLockDate" class="LabelStyle"><s:property value="lockDate"/></p>
                	<div class="Clear"></div>
					<label class="LabelStyle Label2Style">Số đơn hàng</label> 
					<input type="text" class="InputTextStyle InputText1Style" id="orderNumber" maxlength="50"/>
					 
					<label class="LabelStyle Label1Style">Mã NVBH</label> 
					<div class="BoxSelect BoxSelect2">
                        <select class="easyui-combobox" id="saleStaff">
                        </select>
                    </div>
				<%-- 	<div class="BoxSelect BoxSelect2">
						<select class="easyui-combobox" id="saleStaff">
							<option value=""></option>
							<s:iterator value="lstNVBHVo"  var="obj" >								
								<option value="<s:property value="#obj.staffCode" />"><s:property value="#obj.staffName" /></option>
							</s:iterator>	
						</select>	
					</div> --%>
					 
					<label class="LabelStyle Label6Style">Thuế suất</label> 
					<input type="text" class="InputTextStyle InputText1Style" id="taxValue" type="text" maxlength="7"/>
					<div class="Clear"></div>
					<label class="LabelStyle Label2Style">Từ ngày <span class="ReqiureStyle">*</span></label> 
					
					<input type="text" class="InputTextStyle InputText6Style vinput-date" id="datepickerFromDate" value="<s:property value="lockDate" />" maxlength="10"/> 
					
					<label class="LabelStyle Label1Style">Đến ngày <span class="ReqiureStyle">*</span></label> 
					<input type="text" class="InputTextStyle InputText6Style vinput-date" id="datepickerToDate" value="<s:property value="lockDate" />" maxlength="10"/> 
					
					<label class="LabelStyle Label6Style">Hình thức thanh toán</label>
					<div class="BoxSelect BoxSelect2">
						<select id="custPayment" class="MySelectBoxClass">
							<option value="0" selected="selected">Chọn</option>
							<option value="1">Tiền mặt</option>
							<option value="2">Chuyển khoản</option>
						</select>
					</div>
					<div class="Clear"></div>
					<label class="LabelStyle Label2Style">Mã KH (F9)</label> 
					<input type="text" class="InputTextStyle InputText1Style" id="shortCode" maxlength="50"/> 
					
					<label class="LabelStyle Label1Style">Địa chỉ</label> 
					<input type="text" class="InputTextStyle InputText1Style" id="addressCus" maxlength="200"/>
					
					<label class="LabelStyle Label6Style">Trạng thái</label>
					<div class="BoxSelect BoxSelect2">
						<select id="status" class="MySelectBoxClass">
							<option value="0" selected="selected">Tất cả</option>
							<option value="1">Đã in</option>
							<option value="2">Chưa in</option>
						</select>
					</div>
					<div class="Clear"></div>
                    <label class="LabelStyle Label2Style">Giá trị ĐH</label>
                    <div class="BoxSelect BoxSelect2">
	                    <select class="" id="isValueOrder" style="width:210px" tabindex="8">
	                         <option value="-1">Tất cả</option>
	                         <option value="1" selected="selected">Có giá trị</option>
	                         <option value="0">Không có giá trị</option>
	                    </select>
                    </div>
					<%-- <div class="BoxSelect BoxSelect2">
	                    <select class="MySelectBoxClass" id="isValueOrder" >
	                         <option value="-1">Tất cả</option>
	                         <option value="1" selected="selected">Có giá trị >=</option>
	                         <option value="0">Có giá trị <= </option>
	                    </select>
                    </div>
                    <label class="LabelStyle Label1Style"></label>     			
     				 	<input type="text" class="InputTextStyle InputText1Style" id="numberValueOrder" type="text" value='<s:property value="numberValue"/>' maxlength="50"/>                 
					 --%>
					<div class="Clear"></div>
					<div class="BtnCenterSection">
						<button class="BtnGeneralStyle" id="btnSearch" onclick="return SPPrintTax.getTaxSearch();">Tìm kiếm</button>
					</div>
					<p id="errMsg" class="ErrorMsgStyle" style="display: none"></p>
					
					<div class="Clear"></div>
				</div>
				<h2 class="Title2Style">Kết quả tìm kiếm</h2>
				<div class="SearchInSection SProduct1Form">
					<div class="GridSection" id="gridContainer">
						<table id="grid"></table></div>
						<div class="Clear"></div>
					<div class="BtnRSection">
						<div>
						<label class="LabelStyle Label7Style">Người mua hàng</label> 
						<input type="text" class="InputTextStyle" value='<s:property value="buyer"/>' id="buyer" maxlength="200" />
						<label class="LabelStyle Label7Style" style="padding-left: 54px; width: 57px;">Thủ kho</label> 
						<input type="text" class="InputTextStyle" id="stockStaff" value='<s:property value="stockStaff"/>' maxlength="200" />
						<label class="LabelStyle Label7Style" style="padding-left: 30px;">Người bán hàng</label> 
						<input type="text" class="InputTextStyle" id="selStaff" value='<s:property value="selStaff"/>' maxlength="200" />
						</div>
<!-- 						<button class="BtnGeneralStyle BtnMSection" onclick="return SPPrintTax.printTax();" >In hóa đơn</button> -->
<!-- 						<button id="btnInVAT" class="BtnGeneralStyle BtnMSection" onclick="return SPPrintTax.viewTax();" >In hóa đơn[1]</button> -->
						<button class="BtnGeneralStyle BtnMSection" onclick="return SPPrintTax.viewTaxNew();" >In hóa đơn</button>
						<button class="BtnGeneralStyle BtnMSection" onclick="return SPPrintTax.appletViewer();" >view Applet</button>
					</div>
					<div class="Clear"></div>
				</div>
			</div>
			<div class="Clear"></div>
		</div>
	</div>
	<div class="Clear"></div>
		<%-- <div class="SearchInSection SProduct1Form">
		<div class="GridSection"> 
			<div class="ButtonSection" style="text-align: left;">
		       	<div class="GeneralForm" style="float:left;">
					<div class="Func1Section">
						<p class="DownloadSFileStyle DownloadSFile2Style">
                        	<a class="Sprite1" href="javascript:void(0)" onclick="return SPPrintTax.downloadFile();">Tải template</a>
                        </p>
						<div class="DivInputFile">
							<form action="/sale-product/print-tax/upload-file" name="importFrm" id="importFrm"  method="post" enctype="multipart/form-data">
					            <input type="file" class="InputFileStyle" size="20" name="zipFile" id="zipFile" onchange="uploadZIPFile(this,'importFrm');">
					            <div class="FakeInputFile">
					                <input id="fakefilepc" name="fileName" type="text" class="InputTextStyle InputText1Style">
					            </div>
							</form>
				        </div>
			                <button style="float:right; font-size: 12px;" id="btnOrderDisplayImport" class="BtnGeneralStyle BtnGeneralMStyle Sprite2" onclick="SPPrintTax.uploadFile();">
		                  		<span class="Sprite2">Upload</span>
		                  	</button>
					</div>
					<div class="Func2Section">
						<p class="DownloadSFileStyle">
							<a href="javascript:void(0)" onclick="return SPPrintTax.printTemplate();">In template</a>
						</p>
					</div>
				    <div class="Clear"></div>
				</div>
			</div>
		</div>
	</div> --%>
</div>
<div class="Clear"></div>
<!-- <p class="ErrorMsgStyle SpriteErr" id="errMsg" style="display: none;">Có lỗi xảy ra khi cập nhật dữ liệu</p> -->
<div id="viewTaxDialogContainer" style="display: none;">
<div id="viewTaxContainer">
	<div style="height: 475px; overflow: scroll;">
		<div id="frameContainer" style="padding-left: 20px;"></div>
	</div>
	<button class="BtnGeneralStyle BtnGeneralCStyle BtnGeneralMStyle Sprite2" style="width: 50px; float: right; margin-right: 18px; margin-top: 3px;" onclick="return SPPrintTax.updatePrintDate();"><span class="Sprite2">In</span></button>
	<button class="BtnGeneralStyle BtnMSection" onclick="return SPPrintTax.printTax();" style="width: 80px; float: right; margin-top: 3px;" >Xuất file</button>
</div>
</div>
<%-- <tiles:insertTemplate template="/WEB-INF/jsp/general/successMsg.jsp" />						 --%>
<script type="text/javascript">
$(function() {
	var buyer = '<s:property value="buyer"/>';
	if(buyer != '' && buyer != "") {
		SPPrintTax.buyer = buyer;
	}
	var stockStaff = '<s:property value="stockStaff"/>';
	if(stockStaff != '' && stockStaff != "") {
		SPPrintTax.stockStaff = stockStaff;
	}
	var selStaff = '<s:property value="selStaff"/>';
	if(selStaff != '' && selStaff != "") {
		SPPrintTax.selStaff = selStaff;
	}
	SPPrintTax.initInfo(); 
	$('#shortCode').bind('keyup',function(event) {
		if ($('#cbxShop').combobox('getValue') != null&& $('#cbxShop').combobox('getValue') != ''&& $('#cbxShop').combobox('getValue') != undefined) {
			if (event.keyCode == keyCode_F9) {
				VCommonJS.showDialogSearch2({
                    inputs : [
             	 		{id:'code', maxlength:50, label:'Mã KH'},
               	 		{id:'name', maxlength:250, label:'Tên KH'},
                 		{id:'address', maxlength:250, label:'Địa chỉ', width:410},
                          ],
                    params : {shopCode: $('#cbxShop').combobox('getValue')},
                    url : '/commons/customer-in-shop/search',
                    columns : [[
                           {field:'shortCode', title:'Mã KH', align:'left', width: 50, sortable:false, resizable:false, formatter:function(value,row,index) {
                        	   return VTUtilJS.XSSEncode(value);        
                           }},
                           {field:'customerName', title:'Tên khách hàng', align:'left', width: 120, sortable:false, resizable:false, formatter:function(value,row,index) {
                        	   return VTUtilJS.XSSEncode(value);        
                           }},
                           {field:'address', title:'Địa chỉ', align:'left', width: 200, sortable:false, resizable:false, formatter:function(value,row,index) {
                        	   return VTUtilJS.XSSEncode(value);        
                           }},
                           {field:'choose', title:'', align:'center', width:40, sortable:false, resizable:false, formatter:function(value,row,index) {
                        	   return "<a href='javascript:void(0)' onclick='chooseCustomer(0,\""+ Utils.XSSEncode(row.shortCode) + "\");'>Chọn</a>";        
                           }}
                    ]]
              });
			}
		}
	});	
	$(".textEnter").keyup(function(event){
	    if(event.keyCode == 13){
	    	SPPrintTax.getTaxSearch();
	    }
	});
	$('#orderNumber').focus();
	Utils.bindComboboxStaffEasyUI('saleStaff');	
	setTimeout(function(){SPPrintTax.getTaxSearch();},3000);
	
	$('.combo-arrow').live('click',function(){$('.combo-panel').each(function(){if(!$(this).is(':hidden')){$(this).parent().css('width','250px');$(this).css('width','248px');}});});
	$(document).bind('keyup',function(event){
		if(event.keyCode == keyCodes.ENTER){		   				
			if($('#btnSearch')!= null && $('#btnSearch').html()!= null && $('#btnSearch').html().trim().length > 0 && !$('#btnSearch').is(':hidden')){
				$('#btnSearch').click();
			}			
		}
	});	
});
function chooseCustomer(type,code) {
	$('#shortCode').val(code);
	$('.easyui-dialog').dialog('close');
}
</script>

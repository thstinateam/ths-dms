<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles" %>
<tiles:insertTemplate template='/WEB-INF/jsp/sale-product/common.jsp'/>
<div class="BreadcrumbSection">
    <ul class="ResetList FixFloat BreadcrumbList">
        <li class="Sprite1"><a href="/sale-product/create-order/info">Bán hàng</a></li>
        <li><span>Tạo đơn bán hàng</span></li>
    </ul>
</div>
<div class="CtnOneColSection">
    <div class="ContentSection">
    	<div class="ToolBarSection">
            <div class="SearchSection GeneralSSection">
            	<h2 class="Title2Style">Thông tin chung
            		<img style="display: none;" src="/resources/scripts/plugins/jquery-easyui-1.3.2/themes/default/images/loading.gif" class="loadingTitle" >
            	</h2>
                <div class="SearchInSection SProduct1Form">
                	<label class="LabelStyle Label2Style">Số đơn hàng</label>
                    <input type="text" id="orderNumber" class="InputTextStyle InputText1Style" disabled="disabled"/>
                    <label class="LabelStyle Label1Style">Mã KH(F9)<span class="ReqiureStyle">*</span></label>
                    <input id="customerCode" name="customerCode" type="text" class="InputTextStyle InputText1Style" /> 
                    <label class="LabelStyle Label1Style">Tên KH</label>
                    <p id="customerName" class="ValueStyle Value1Style"></p>
                    <div class="Clear"></div>
                    <label class="LabelStyle Label2Style">NVTT</label>
                    <div class="BoxSelect BoxSelect2"> 
                        <select id="cashierStaffCode" class="easyui-combobox">
                        	<option value="" ></option>
                        	<s:iterator value="listNVTT"  var="obj" >
                        		<option value="<s:property value="#obj.staffCode" />" ><s:property value="#obj.staffName" /></option>
                        	</s:iterator>
                        </select>
                    </div>
                    <label class="LabelStyle Label1Style">NVGH</label>
                    <div class="BoxSelect BoxSelect2">
                        <select id="transferStaffCode" class="easyui-combobox">
                        	<option value="" ></option>
                        	<s:iterator value="listNVGH"  var="obj" >
                        		<option value="<s:property value="#obj.staffCode" />" ><s:property value="#obj.staffName" /></option>
                        	</s:iterator>
                        </select>
                    </div>
                    <label class="LabelStyle Label1Style">Địa chỉ KH</label>
                    <p class="ValueStyle Value1Style" id="address"></p>
                    <div class="Clear"></div>
                    <label class="LabelStyle Label2Style">NVBH<span class="ReqiureStyle">*</span></label>
                    <div class="BoxSelect BoxSelect2" id="boxSelectNVBH">
                        <select id="staffSaleCode" class="easyui-combobox">
                        	<option value="" ></option>
                        	<s:iterator value="listNVBH"  var="obj" >
                        		<option value="<s:property value="#obj.staffCode" />" ><s:property value="#obj.staffName" /></option>
                        	</s:iterator>
                        </select>
                    </div>
                    <label class="LabelStyle Label1Style">Chọn xe</label>
                    <div class="BoxSelect BoxSelect2">
                    	<select class="easyui-combobox" id="carId">
                            <option value=""></option>
							<s:iterator value="listCar"  var="obj" >
								<option value="<s:property value="#obj.id" />"><s:property value="#obj.carNumber" /></option>															
							</s:iterator>
                        </select>
                        <%-- <s:select cssStyle="opacity: 0; height: 20px;" tabindex="4" cssClass="MySelectBoxClass" list="listCar" id="carId"
							headerKey="" headerValue=" Chọn xe "  listKey="id" listValue="carNumber"></s:select> --%>
                    </div>
                    <label class="LabelStyle Label1Style">Số ĐT</label>
                    <p class="ValueStyle Value1Style" id="phone"></p>
                    <div class="Clear"></div>
                    <label class="LabelStyle Label2Style">Ngày giao</label>
                    <input type="text" id="deliveryDate" name="deliveryDate"  class="InputTextStyle InputText6Style" />
                    <label class="LabelStyle Label1Style">Độ ưu tiên</label>
                    <div class="BoxSelect BoxSelect2">
                        <s:select cssStyle="opacity: 0; height: 20px;" cssClass="MySelectBoxClass" list="listPriority" id="priorityId"
								value="saleOrder.priority" listKey="id" listValue="apParamName" disabled="disabledButton" ></s:select>
                    </div>
                    <div class="Clear"></div> 
                    <div style="height: 30px;"> 
						<p style="height: 25px; display: none;" class="ErrorMsgStyle" id="GeneralInforErrors">Có lỗi xảy ra khi cập nhật dữ liệu</p>
					</div>
                    <div class="Clear"></div>
                </div>
                <h2 class="Title2Style">Danh sách sản phẩm bán</h2>
                <div class="SearchInSection SProduct1Form" id="spSaleProductInfor">                	
                	<div class="GeneralMilkBox">
						<div class="GeneralMilkTopBox">
							<div class="GeneralMilkBtmBox">
								<div class="GeneralMilkInBox">
									<div class="GeneralTable" id="gridSaleContainer">
										<div id="gridSaleData"></div>
										<div class="Clear"></div>
	                    				<p class="TotalInfo">
	                                	<span class="Tem1Style">Tổng trọng lượng: <span style="color: #000000" id="totalWeightSale">0</span><span style="color: #000000">(Kg)</span></span>
	                                    <span class="Tem1Style">Tổng tiền : <span style="color: #000000" id="totalAmountSale">0</span><span style="color: #000000">(VNĐ)</span></span>
	                                	</p>
									</div>
									<div class="ButtonSection" style="text-align: right;">										
										<button style="margin-right: 10px;" class="BtnGeneralStyle BtnGeneralMStyle Sprite2" title="Nhấn Ctrl+Alt để thực hiện Tính tiền" id="payment" onclick="return CreateSaleOrder.pay();">
											<span class="Sprite2">Tính tiền</span>
										</button>	
										<div style="height: 15px;">
											<p id="alertSuccess_successMsg" class="SuccessMsgStyle" style="display: none;padding: 0;" >Tính tiền thành công cho đơn hàng !</p>
											<p style="display: none;" class="ErrorMsgStyle" id="saleInputErr"></p>
										</div>										
									</div>				
								</div>
							</div>
						</div>
					</div>                	
                </div>
                <h2 class="Title2Style">Danh sách sản phẩm khuyến mãi</h2>
                <div class="SearchInSection SProduct1Form" id="spPromotionProductInfor">
                       <div class="GeneralMilkBox">
							<div class="GeneralMilkTopBox">
								<div class="GeneralMilkBtmBox">
									<div class="GeneralMilkInBox">
										<div class="GeneralTable" id="gridFreeContainer">
											<div id="gridFreeData"></div>
											<div class="Clear"></div>
	                    				<p class="TotalInfo">
	                                	<span class="Tem1Style">Tổng trọng lượng: <span style="color: #000000" id="totalWeightFree">0</span><span style="color: #000000">(Kg)</span></span>
	                                    <span class="Tem1Style">Tổng tiền : <span style="color: #000000" id="totalAmountFree">0</span><span style="color: #000000">(VNĐ)</span></span>
	                                	</p>
										</div>
									</div>
								</div>
							</div>
						</div>
                    <div class="Clear"></div>
                    <p class="TotalInfo">
                                	<span class="Tem1Style">Tổng trọng lượng: <span style="color: #FF0000" id="totalWeight">0</span><span style="color: #FF0000">(Kg)</span></span>
                                    <span class="Tem1Style">Tổng tiền (bao gồm VAT): <span style="color: #FF0000" id="totalAmount">0</span><span style="color: #FF0000">(VNĐ)</span></span>
                                </p>
                    <div class="ButtonSection" style="text-align: right;">
                    	<button style="margin-right: 10px;" class="BtnGeneralStyle BtnGeneralMStyle Sprite2" title="Nhấn Ctrl+Alt để thực hiện Tạo đơn hàng sau khi Tính tiền" id="btnOrderSave" onclick="CreateSaleOrder.createOrder();">
							<span class="Sprite2">Tạo đơn hàng</span>
						</button>
					</div>                   
                    <div class="Clear"></div>
					<p style="display: none;" class="ErrorMsgStyle" id="serverErrors">Có lỗi xảy ra khi cập nhật dữ liệu</p>
					<p id="successMsg" class="SuccessMsgStyle" style="display: none"></p>                    
                </div>
            </div>
            <div class="Clear"></div>
        </div>
    </div>
    <div class="Clear"></div>
</div>

<input type="hidden" id="fsxn" value="${fsxn}" />
<input type="hidden" id="shopCode" value="<s:property value="shopCode" />" />
<input type="hidden" id="currentLockDay" value="<s:property value="lockDate" />" />
<s:hidden id="canChangePromotionQuantity" name="canChangePromotionQuantity"></s:hidden>
<script type="text/javascript">
	$(document).ready(function() {
		var wWidth = $(window).width();
		$('#gridSaleContainer').css('overflow', 'scroll');
		$('#gridSaleContainer').css('width', wWidth - 40);
		
		$('#gridFreeContainer').css('overflow', 'scroll');
		$('#gridFreeContainer').css('width', wWidth - 40);
		$("#customerCode").focus();
		var priority = $('#priorityId option').next().val();
		$('#priorityId').val(priority);
		$('#priorityId').change();
		CreateSaleOrder.loadPage();
		var lockDate = '<s:property value="lockDate"/>';
		$('#deliveryDate').val(lockDate);
		
	});
</script>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@page import="org.apache.struts2.ServletActionContext"%>
<%@page import="ths.dms.helper.Configuration"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<!-- trungtm6 comment -->
<%-- <s:hidden name="token" id="token"></s:hidden> --%>
<script type="text/javascript">
var excel_template_path ='<%=ServletActionContext.getServletContext().getContextPath() +Configuration.getExcelTemplatePath()%>';
var activeStatus = 'RUNNING';
var stoppedStatus = 'STOPPED';
var waitingStatus = 'WAITING';
var activeStatusText = 'Hoạt động';
$(document).ready(function(){
	$.extend($.jgrid.defaults, {
		datatype: 'json',
		jsonReader : {
			repeatitems:false,
			total: function(result) {
				//Total number of pages
				if(isNaN(result.total)){
					return 0;
				}
				return Math.ceil(result.total / result.max);
			},
			records: function(result) {
				//Total number of records
				return result.total;
			},
			page: function(result){
				if(result.total == 0 || isNaN(result.total)){
					return 0;
				}
				return result.page;
			}
		},
		prmNames: {rows: 'max', search: null},
		height: 'auto',
		viewrecords: true,
		rowNum: 10,
		rowList: [10,20,30],
		rownumbers: true,
		altRows: false,
		loadError: function(xhr, status, error) {
			alert(error);
		},
		gridComplete : function(){
			$('#jqgh_grid_rn').html('STT');
			$('.ui-widget-content loading').removeClass('ui-state-active');
			updateRownumWidthForJqGrid();
		}
	});	
	Utils.bindAutoSearch();	
});

</script>

<div id="searchStyle4CSContainer" style="display: none;">
<div id="searchStyle4CS" class="easyui-dialog" title="Danh sách CTHTTM" style="width:600px;height:auto;" data-options="closed:true">
      <div class="PopupContentMid2">
      	<div class="GeneralForm Search1Form">
              <div class="GridSection">
                  <table id="listCommercialSupport"></table>
              </div>
              <div class="Clear"></div>
              <div class="BtnCenterSection">
                  <button class="BtnGeneralStyle" onclick="$('#searchStyle4CS').dialog('close');">Đóng</button>
              </div>
              <div class="Clear"></div>
          </div>
	</div>
</div>
</div>
<div id="easyui-searchStyle4" class="easyui-window" data-options="closed:true,modal:true,title:'Chọn chương trình HTTM'">
		<div class="DialogProductSelection">
			<div id="UIdivCSScrollBodySection" style="height:auto;width:auto;" class="ScrollBodySection">
				<div class="ResultSection" id="easyui-listCSContainerGrid"	style="padding-right: 10px;width: 700px;">
					<table id="easyui-listCommercialSupport"></table>
					<div id="easyui-listCommercialSupportPager"></div>
				</div>
			</div>
		</div>
</div>
<div id="showProductInfoContainer" style="display: none;">
<div id="showProductInfo" class="easyui-dialog" title="Thông tin mặt hàng" data-options="closed:true,modal:true" style="width:616px;height:auto;">
      <div class="PopupContentMid2">
      	<div class="GeneralForm Search1Form">
			<label class="LabelStyle Label2Style">Mã SP</label>
            <p class="ValueStyle Value1Style" id="productCode"></p>
            <label class="LabelStyle Label2Style">Tên SP</label>
            <p class="ValueStyle Value1Style" id="productName"></p>
            <div class="Clear"></div>
            <label class="LabelStyle Label2Style">Quy cách</label>
            <p class="ValueStyle Value1Style" id="convfactNumber"></p>
            <label class="LabelStyle Label2Style">Tồn kho</label>
            <p class="ValueStyle Value1Style" id="stockNumber"></p>
            <div class="Clear"></div>
            <label id="lblPrice" class="LabelStyle Label2Style">Giá</label>
            <p class="ValueStyle Value1Style" id="priceValue"></p>
            <label class="LabelStyle Label2Style">Tổng số hàng</label>
            <p class="ValueStyle Value1Style" id="totalQuantity"></p>
            <div class="Clear"></div>
            <label class="LabelStyle Label2Style">Mã khuyến mãi</label>
            <p class="ValueStyle Value1Style" id="promotionProgram"></p>
            <div class="Clear"></div>
          </div>
	</div>
</div>
</div>
<div id="easyui-showCustomerInfoContainer" style="display: none;">
<div id="easyui-showCustomerInfo" class="easyui-dialog" style="width: 550px; height: 672px; padding: 10px;" data-options="closed:true,modal:true,title:'Thông tin khách hàng'"  >
	<div class="GeneralDialog General2Dialog">
		<div class="DialogProductSearch">
			<div class="GeneralMilkBox">
				<div class="GeneralMilkTopBox">
					<div class="GeneralMilkBtmBox">
						<h3 class="Sprite2">
							<span class="Sprite2">Thông tin khách hàng</span>
						</h3>
						<div class="GeneralMilkInBox">
							<div class="GeneralInfoSection">
								<label class="LabelStyle Label2Style">Mã khách hàng</label>
								<p class="ValueStyle Value2Style" id="info-customerCodeEasyUI"></p>
								<label class="LabelStyle Label3Style">Tên khách hàng</label>
								<p class="ValueStyle Value3Style" id="info-customerNameEasyUI"></p>
								<div class="Clear"></div>
								<label class="LabelStyle Label2Style">Địa chỉ</label>
								<p class="ValueStyle Value2Style" id="info-addressEasyUI"></p>
								<label class="LabelStyle Label3Style">Số điện thoại</label>
								<p class="ValueStyle Value3Style" id="info-phoneEasyUI"></p>
								<div class="Clear"></div>
								<label class="LabelStyle Label2Style">Di động</label>
								<p class="ValueStyle Value2Style" id="info-mobiphoneEasyUI"></p>
								<label class="LabelStyle Label3Style">Loại cửa hàng</label>
								<p class="ValueStyle Value3Style" id="info-shopTypeNameEasyUI"></p>
								<div class="Clear"></div>
								<label class="LabelStyle Label2Style">Độ trung thành</label>
								<p class="ValueStyle Value2Style" id="info-loyaltyEasyUI"></p>
								<label class="LabelStyle Label3Style">Người liên hệ</label>
								<p class="ValueStyle Value3Style" id="info-contactNameEasyUI"></p>
								<div class="Clear"></div>
								<label class="LabelStyle Label2Style">Bán trong ngày</label>
								<p class="ValueStyle Value2Style" id="info-totalInDateEasyUI"></p>
								<div class="Clear"></div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="GeneralMilkBox">
				<div class="GeneralMilkTopBox">
					<div class="GeneralMilkBtmBox">
						<h3 class="Sprite2">
							<span class="Sprite2">Thông tin doanh số</span>
						</h3>
						<div class="GeneralMilkInBox">
							<div class="GeneralInfoSection">
<!-- 								<label class="LabelStyle Label4Style">SKU</label>
								<p class="ValueStyle Value4Style" id="info-numSkuInDate"></p>
								<div class="Clear"></div>
 -->								<label class="LabelStyle Label4Style">Số lần đã đặt hàng
									trong tháng</label>
								<p class="ValueStyle Value4Style"
									id="info-numOrderInMonthEasyUI"></p>
								<div class="Clear"></div>
								<label class="LabelStyle Label4Style">Bình quân doanh số
									2 tháng trước</label>
								<p class="ValueStyle Value4Style"
									id="info-avgTotalInLastTwoMonthEasyUI"></p>
								<div class="Clear"></div>
								<label class="LabelStyle Label4Style">Doanh số đã thực
									hiện trong tháng</label>
								<p class="ValueStyle Value4Style" id="info-totalInMonthEasyUI"></p>
								<div class="Clear"></div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="GeneralMilkBox">
				<div class="GeneralMilkTopBox">
					<div class="GeneralMilkBtmBox">
						<h3 class="Sprite2">
							<span class="Sprite2">Danh sách đơn hàng gần nhất</span>
						</h3>
						<div class="GeneralMilkInBox">
							<div class="ResultSection" id="info-listSaleOrderContainerEasyUI">
								<table id="info-listCustomerOrderEasyUI"></table>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
</div>
<div style="display: none;" id="showPromotionProgramInfo">
	<div class="GeneralDialog General2Dialog">
		<div class="DialogProductSelection">
			<div class="GeneralInfoSection">
				<label class="LabelStyle Label2Style">Mã chương trình</label>
				<p class="ValueStyle Value2Style" id="programCode"></p>
				<label class="LabelStyle Label3Style">Tên chương trình</label>
				<p class="ValueStyle Value2Style " id="programName" ></p>
				<div class="Clear"></div>
				<label class="LabelStyle Label2Style">Loại khuyến mại</label>
				<p class="ValueStyle Value2Style" id="programType"></p>
				<label class="LabelStyle Label3Style">Dạng chương trình</label>
				<p class="ValueStyle Value3Style" id="programFormat"></p>
				<div class="Clear"></div>
				<label class="LabelStyle Label2Style">Từ ngày</label>
				<p class="ValueStyle Value2Style" id="fromDate1"></p>
				<label class="LabelStyle Label3Style">Đến ngày</label>
				<p class="ValueStyle Value3Style" id="toDate1"></p>
				<div class="Clear"></div>
				<label class="LabelStyle Label2Style">Diễn giải chương trình</label>
				<p class="ValueStyle Value2Style" id="programDescription"></p>
				<div class="Clear"></div>
			</div>
		</div>
		<div class="BoxDialogBtm">
			<div class="ButtonSection">
				<button
					class="BtnGeneralStyle BtnGeneralCStyle BtnGeneralMStyle Sprite2"
					onclick="$.fancybox.close();">
					<span class="Sprite2">Đóng</span>
				</button>
			</div>
		</div>
	</div>
</div>
<div style="display: none;" id="spAddProduct">
	<div class="DialogProductSelection">
		<div class="GeneralMilkBox">
			<div class="GeneralMilkTopBox">
				<div class="GeneralMilkBtmBox">
					<h3 class="Sprite2">
						<span class="Sprite2">Nội dung tìm kiếm</span>
					</h3>
					<div class="GeneralMilkInBox ResearchSection">
						<div class="SearchSales2Form">
							<div class="FormLeft">
								<label class="LabelStyle Label1Style">Mã MH sjafasfhkjds</label> <input
									class="InputTextStyle InputText1Style" type="text"> <label
									class="LabelStyle Label2Style">Tên MH</label> <input
									class="InputTextStyle InputText1Style" type="text"> <label
									class="LabelStyle Label3Style">Mã CTKM</label> <input
									class="InputTextStyle InputText1Style" type="text">
							</div>
							<div class="ButtonLSection">
								<button class="BtnGeneralStyle Sprite2">
									<span class="Sprite2">Tìm kiếm</span>
								</button>
							</div>
							<div class="Clear"></div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="GeneralMilkBox">
			<div class="GeneralMilkTopBox">
				<div class="GeneralMilkBtmBox">
					<h3 class="Sprite2">
						<span class="Sprite2">Kết quả tìm kiếm</span>
					</h3>
					<div class="GeneralMilkInBox">
						<div class="ResultSection">
							<p class="WarningResultStyle">Không có kết quả</p>
							<!--Đặt Grid ở đây-->
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="BoxDialogBtm">
			<div class="BoxDialogAlert">
				<p class="ErrorMsgStyle Sprite1">Vui lòng nhập đúng kết quả</p>
				<p class="SuccessMsgStyle">Bạn đã thêm thành công</p>
			</div>
			<div class="ButtonSection">
				<button class="BtnGeneralStyle Sprite2">
					<span class="Sprite2">Chọn</span>
				</button>
				<button
					class="BtnGeneralStyle BtnGeneralCStyle BtnGeneralMStyle Sprite2">
					<span class="Sprite2">Đóng</span>
				</button>
			</div>
		</div>
	</div>

</div>
<div id="searchPO" style="display: none;">
	<div class="GeneralDialog General1Dialog">
		<div class="DialogProductSelection">
			<div class="GeneralMilkBox">
				<div class="GeneralMilkTopBox">
					<div class="GeneralMilkBtmBox">
						<h3 class="Sprite2">
							<span class="Sprite2">Nội dung tìm kiếm</span>
						</h3>
						<div class="GeneralMilkInBox ResearchSection">
							<div class="SearchSales3Form">
								<label class="LabelStyle Label1Style">Số đơn hàng</label> 
								<input type="text" class="InputTextStyle InputText1Style" maxlength="14" id="ordernumber" /> 
								<label class="LabelStyle Label2Style">Thực hiện</label>
								<p class="ValueStyle Value1Style">Presale</p>
								<div class="Clear"></div>
								<label class="LabelStyle Label1Style">Mã khách hàng (F9)</label>
								<input type="text" class="InputTextStyle InputText1Style" maxlength="50" id="shortcode" onkeyup="return SPReturnOrder.searchCustomer(event);" /> 
								<label class="LabelStyle Label2Style">Trạng thái</label>
								<p class="ValueStyle Value1Style">Đã duyệt</p>
								<div class="Clear"></div>
								<label class="LabelStyle Label1Style">Tên khách hàng</label> 
								<input type="text" class="InputTextStyle InputText1Style" maxlength="250" id="customername"/> 
								<label class="LabelStyle Label2Style">Loại đơn hàng</label>
								<p class="ValueStyle Value1Style">Đơn bán</p>
								<div class="Clear"></div>
								<label class="LabelStyle Label1Style">Từ ngày</label> 
								<!-- <input type="text" class="InputTextStyle InputText2Style" id="fDate" disabled="disabled"/> -->
								<input type="text" class="InputTextStyle InputText2Style" id="fDate"/>
								<label class="LabelStyle Label2Style">Đến ngày</label> 
								<input type="text" class="InputTextStyle InputText2Style" id="tDate"/>
								<!-- <input type="text" class="InputTextStyle InputText2Style" id="tDate" disabled="disabled"/> -->									
								<div class="Clear"></div>
								<label class="LabelStyle Label1Style">NVBH</label> 
								<input type="text" class="InputTextStyle InputText1Style" maxlength="50" id="staffid"/> 
								<label class="LabelStyle Label2Style">NVGH</label> 
								<input type="text" class="InputTextStyle InputText1Style" maxlength="50" id="deliveryid" />
								<div class="Clear"></div>
								<div class="ButtonSection">
									<button class="BtnGeneralStyle Sprite2" onclick="return SPReturnOrder.searchSaleOrder();"> 
									<span class="Sprite2">Tìm kiếm</span>
										
								</button>
									<img src="/resources/images/loading.gif" style="visibility: hidden;" class="LoadingStyle"  id="loadingSearch" />
								</div>
								
			<p id="searcSaleOrderError" class="ErrorMsgStyle Sprite1" style="display:none;">Có
						lỗi xảy ra khi tìm kiếm dữ liệu</p>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="GeneralMilkBox">
				<div class="GeneralMilkTopBox">
					<div class="GeneralMilkBtmBox">
						<h3 class="Sprite2">
							<span class="Sprite2">Kết quả tìm kiếm</span>
						</h3>
						<div class="GeneralMilkInBox">
							<div class="GeneralTable Table36Section">
								<div class="BoxGeneralTTitle">
									<table width="100%" border="0" cellspacing="0" cellpadding="0">
										<colgroup>
											<col style="width: 48px;" />
											<col style="width: 80px;" />
											<col style="width: 150px;" />
											<col style="width: 150px;" />
											<col style="width: 150px;" />
											<col style="width: 100px;" />
											<col style="width: 100px;" />
											<col style="width: 58px;" />
											
										</colgroup>
										<thead>
											<tr>
												<th class="ColsThFirst">STT</th>
												<th>Ngày</th>
												<th>Khách hàng</th>
												<th>Địa chỉ</th>
												<th>Số đơn hàng</th>
												<th>NVBH</th>
												<th>NVGH</th>
												<th class="ColsThEnd">Chọn</th>
											</tr>
										</thead>
									</table>
								</div>
								<div class="BoxGeneralTBody">
									<div class="ScrollBodySection">
										<table width="100%" border="0" cellspacing="0" cellpadding="0">
											<colgroup>
												<col style="width: 48px;" />
												<col style="width: 80px;" />
												<col style="width: 150px;" />
												<col style="width: 150px;" />
												<col style="width: 150px;" />
												<col style="width: 100px;" />
												<col style="width: 100px;" />
												<col style="width: 58px;" />
											</colgroup>
											<tbody>
											</tbody>
										</table>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="BoxDialogBtm">
			<div class="BoxDialogAlert" id="searchSaleOrderError">
			
			</div>
				<div class="ButtonSection">
					<button onclick="$.fancybox.close();"
						class="BtnGeneralStyle BtnGeneralCStyle BtnGeneralMStyle Sprite2">
						<span class="Sprite2">Đóng</span>
					</button>
				</div>
			</div>
		</div>
	</div>
</div>
<div style="display: none;" id="showCustomerInfo">
	<div class="GeneralDialog General2Dialog">
		<div class="DialogProductSearch">
			<div class="GeneralMilkBox">
				<div class="GeneralMilkTopBox">
					<div class="GeneralMilkBtmBox">
						<h3 class="Sprite2">
							<span class="Sprite2">Thông tin khách hàng</span>
						</h3>
						<div class="GeneralMilkInBox">
							<div class="GeneralInfoSection">
								<label class="LabelStyle Label2Style">Mã khách hàng</label>
								<p class="ValueStyle Value2Style" id="info-customerCode"></p>
								<label class="LabelStyle Label3Style">Tên khách hàng</label>
								<p class="ValueStyle Value3Style" id="info-customerName"></p>
								<div class="Clear"></div>
								<label class="LabelStyle Label2Style">Địa chỉ</label>
								<p class="ValueStyle Value2Style" id="info-address"></p>
								<label class="LabelStyle Label3Style">Số điện thoại</label>
								<p class="ValueStyle Value3Style" id="info-phone"></p>
								<div class="Clear"></div>
								<label class="LabelStyle Label2Style">Di động</label>
								<p class="ValueStyle Value2Style" id="info-mobiphone"></p>
								<label class="LabelStyle Label3Style">Loại cửa hàng</label>
								<p class="ValueStyle Value3Style" id="info-shopTypeName"></p>
								<div class="Clear"></div>
								<label class="LabelStyle Label2Style">Độ trung thành</label>
								<p class="ValueStyle Value2Style" id="info-loyalty"></p>
								<label class="LabelStyle Label3Style">Người liên hệ</label>
								<p class="ValueStyle Value3Style" id="info-contactName"></p>
								<div class="Clear"></div>
								<label class="LabelStyle Label2Style">Bán trong ngày</label>
								<p class="ValueStyle Value2Style" id="info-totalInDate"></p>
								<div class="Clear"></div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="GeneralMilkBox">
				<div class="GeneralMilkTopBox">
					<div class="GeneralMilkBtmBox">
						<h3 class="Sprite2">
							<span class="Sprite2">Thông tin doanh số</span>
						</h3>
						<div class="GeneralMilkInBox">
							<div class="GeneralInfoSection">
<!-- 								<label class="LabelStyle Label4Style">SKU</label>
								<p class="ValueStyle Value4Style" id="info-numSkuInDate"></p>
								<div class="Clear"></div>
 -->								<label class="LabelStyle Label4Style">Số lần đã đặt hàng
									trong tháng</label>
								<p class="ValueStyle Value4Style"
									id="info-numOrderInMonth"></p>
								<div class="Clear"></div>
								<label class="LabelStyle Label4Style">Bình quân doanh số
									2 tháng trước</label>
								<p class="ValueStyle Value4Style"
									id="info-avgTotalInLastTwoMonth"></p>
								<div class="Clear"></div>
								<label class="LabelStyle Label4Style">Doanh số đã thực
									hiện trong tháng</label>
								<p class="ValueStyle Value4Style" id="info-totalInMonth"></p>
								<div class="Clear"></div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="GeneralMilkBox">
				<div class="GeneralMilkTopBox">
					<div class="GeneralMilkBtmBox">
						<h3 class="Sprite2">
							<span class="Sprite2">Danh sách đơn hàng gần nhất</span>
						</h3>
						<div class="GeneralMilkInBox">
							<div class="ResultSection"
								id="info-listSaleOrderContainer">
								<table id="info-listCustomerOrder"></table>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="BoxDialogBtm">
				<div class="ButtonSection">
					<button
						class="BtnGeneralStyle BtnGeneralCStyle BtnGeneralMStyle Sprite2"
						onclick="$.fancybox.close();">
						<span class="Sprite2">Đóng</span>
					</button>
				</div>
			</div>
		</div>
	</div>
</div>
<div style="display: none" id="showProductForSaleProduct">
<div class="GeneralDialog General1Dialog">
        	<div class="DialogProductSelection">
            	<div class="GeneralMilkBox">
                    <div class="GeneralMilkTopBox">
                        <div class="GeneralMilkBtmBox">
                            <h3 class="Sprite2"><span class="Sprite2">Nội dung tìm kiếm</span></h3>
                            <div class="GeneralMilkInBox ResearchSection">
                            	<div class="SearchSales2Form">
                                	<div class="FormLeft">
                                        <label class="LabelStyle Label1Style">Mã MH</label>
                                        <input type="text" class="InputTextStyle InputText1Style">
                                        <label class="LabelStyle Label2Style">Tên MH</label>
                                        <input type="text" class="InputTextStyle InputText1Style">
                                        <label class="LabelStyle Label3Style">Mã CTKM</label>
                                        <input type="text" class="InputTextStyle InputText1Style">
                                    </div>
                                    <div class="ButtonLSection">
                                        <button class="BtnGeneralStyle Sprite2"><span class="Sprite2">Tìm kiếm</span></button>
                                    </div>
                                    <div class="Clear"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="GeneralMilkBox">
                    <div class="GeneralMilkTopBox">
                        <div class="GeneralMilkBtmBox">
                            <h3 class="Sprite2"><span class="Sprite2">Kết quả tìm kiếm</span></h3>
                            <div class="GeneralMilkInBox">
                                <div class="ResultSection">
                                    <p class="WarningResultStyle">Không có kết quả</p>
                                    <!--Đặt Grid ở đây-->
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="BoxDialogBtm">
                	<div class="BoxDialogAlert">
                        <p class="ErrorMsgStyle Sprite1">Vui lòng nhập đúng kết quả</p>
                        <p class="SuccessMsgStyle">Bạn đã thêm thành công</p>
                    </div>
                    <div class="ButtonSection">
                        <button class="BtnGeneralStyle Sprite2"><span class="Sprite2">Chọn</span></button><button class="BtnGeneralStyle BtnGeneralCStyle BtnGeneralMStyle Sprite2"><span class="Sprite2">Đóng</span></button>
                    </div>
                </div>
        	</div>
        </div>
</div>
<!--  Lo san pham huy -->
<div id="adjustOrderLotDialogContainer" style="display: none;">
<div id="adjustOrderLotDialog" class="easyui-dialog" data-options="closed: true" title="Thông tin lô" style="width:616px;height:auto;">
    <div class="PopupContentMid2">
    	<div class="GeneralForm Search1Form">        	
            <label class="LabelStyle Label2Style">Mã SP</label><p class="ValueStyle Value1Style" id="saleProductLotCode"></p>
            <label class="LabelStyle Label2Style">Tên SP</label><p class="ValueStyle Value1Style" id="saleProductLotName"></p>
            <div class="Clear"></div>
            <label class="LabelStyle Label2Style">Tổng số lượng</label><p class="ValueStyle Value1Style" id="d_countLot"></p>
            <div class="Clear"></div>
        </div>
        <div class="GeneralForm Search1Form">
        	<h2 class="Title2Style Title2MTStyle">Danh sách lô tách</h2>
            <div class="GridSection">
					<div class="GeneralTable General1Table">
						<table width="100%" border="0" cellspacing="0" cellpadding="0">
							<colgroup>
								<col style="width: 55px;">
								<col style="width: 120px;">
								<col style="width: 90px;">
								<col style="width: 90px;">								
							</colgroup>
							<thead>
								<tr>
									<th class="ColsThFirst">STT</th>
									<th>Số lô</th>
									<th id="cancelLotStockTitle">Tồn kho đáp ứng</th>
									<th>Số lượng</th>									
								</tr>
							</thead>
							<tbody id="saleProductCancelLotTbody">
							</tbody>
							<tfoot>
								<tr>
									<td colspan="3">Tổng:</td>
									<td style="text-align: right;height: 24px; padding-right: 8px" id="totalCountTFoot"></td>
								</tr>
							</tfoot>
						</table>
					</div>
				</div>
              <div class="Clear"></div>
              <div class="BtnCenterSection">
              		<button class="BtnGeneralStyle BtnMSection" onclick="SPCreateOrder.onLotChange();">Chọn</button>
                  	<button class="BtnGeneralStyle" onclick="$('#adjustOrderLotDialog').dialog('close');">Đóng</button>
              </div>
              <div class="Clear"></div>
                <div style="height: 30px;"> 
					<p style="height: 25px; display: none;" class="ErrorMsgStyle" id="errMsg1">Có lỗi xảy ra khi cập nhật dữ liệu</p>
				</div>
          </div>
	</div>
</div>
</div>
<!--  Lo san pham ban -->
<div id="adjustSaleOrderLotDialogContainer" style="display: none;">
<div id="adjustSaleOrderLotDialog" class="easyui-dialog" data-options="closed: true" title="Thông tin lô" style="width:616px;height:auto;">
    <div class="PopupContentMid2">
    	<div class="GeneralForm Search1Form">        	
            <label class="LabelStyle Label2Style">Mã SP</label>
            <p class="ValueStyle Value1Style" id="saleProductLotCode"></p>
            <label class="LabelStyle Label2Style">Tên SP</label>
            <p class="ValueStyle Value1Style" id="saleProductLotName"></p>
            <div class="Clear"></div>
            <label class="LabelStyle Label2Style">Tổng số lượng</label>
            <p class="ValueStyle Value1Style" id="d_countLot"></p>
            <div class="Clear"></div>
        </div>
        <div class="GeneralForm Search1Form">
        	<h2 class="Title2Style Title2MTStyle">Danh sách lô tách</h2>
            <div class="GridSection">
                <div class="GeneralTable Table31Section">
                  <div class="BoxGeneralTTitle">
                      <table border="0" cellspacing="0" cellpadding="0" style="width: 100%;">
                          <colgroup>
                              <col style="width:55px;">
								<col style="width:120px;">
								<col style="width:90px;">
								<col style="width:90px;">								
                          </colgroup>
                          <thead>
                              <tr>
                                  <th class="ColsThFirst">STT</th>
                                  <th>Số lô</th>
                                  <th id="saleProductStockTitle">Tồn kho</th>
                                  <th>Số lượng</th>                                  
                              </tr>
                          </thead>
                          <tbody id="saleProductLotTbody">
								
							</tbody>
                          <tfoot>
							<tr>
								<td colspan="3">Tổng:</td>
								<td style="text-align: right;height: 24px; padding-right: 8px"	id="totalCountTFoot"></td>												
							</tr>
						</tfoot>
                      </table>
                  </div>
              </div>
              </div>
              <div class="Clear"></div>
              <div class="BtnCenterSection">
              	<button class="BtnGeneralStyle BtnMSection" id="btnSaleLot" onclick="SPCreateOrder.onProductSaleLotChange();">Chọn</button>
                  <button class="BtnGeneralStyle" onclick="$('#adjustSaleOrderLotDialog').dialog('close');">Đóng</button>
              </div>
              <div class="Clear"></div>
                <div style="height: 30px;"> 
					<p style="height: 25px; display: none;" class="ErrorMsgStyle" id="errMsg1">Có lỗi xảy ra khi cập nhật dữ liệu</p>
				</div>
          </div>
	</div>
</div>
</div>
<!--  Lo san pham km -->
<div id="adjustPromotionSaleOrderLotDialogContainer" style="display: none;">
<div id="adjustPromotionSaleOrderLotDialog" class="easyui-dialog" data-options="closed: true" title="Thông tin lô" style="width:616px;height:auto;">
	<div class="PopupContentMid2">
    	<div class="GeneralForm Search1Form">        	
            <label class="LabelStyle Label2Style">Mã SP</label>
            <p class="ValueStyle Value1Style" id="saleProductLotCode"></p>
            <label class="LabelStyle Label2Style">Tên SP</label>
            <p class="ValueStyle Value1Style" id="saleProductLotName"></p>
            <div class="Clear"></div>
            <label class="LabelStyle Label2Style">Tổng số lượng</label>
            <p class="ValueStyle Value1Style" id="d_countLot"></p>
            <div class="Clear"></div>
        </div>
        <div class="GeneralForm Search1Form">
        	<h2 class="Title2Style Title2MTStyle">Danh sách lô tách</h2>
            <div class="GridSection">
                <div class="GeneralTable Table31Section">
                  <div class="BoxGeneralTTitle">
                      <table border="0" cellspacing="0" cellpadding="0" style="width: 100%;" id="saleInputTableAdded">
                         <colgroup>
                              <col style="width:55px;">
								<col style="width:120px;">
								<col style="width:90px;">
								<col style="width:90px;">								
                          </colgroup>
                         <thead>
                              <tr>
                                  <th class="ColsThFirst">STT</th>
                                  <th>Số lô</th>
                                  <th id="ppStockTitle">Tồn kho</th>
                                  <th>Số lượng</th>                                  
                              </tr>
                          </thead>
                          <tbody id="saleProductLotTbody">
								
							</tbody>
                          <tfoot>
							<tr>
								<td colspan="3">Tổng:</td>
								<td style="text-align: right;height: 24px; padding-right: 8px"	id="totalCountTFoot"></td>												
							</tr>
						</tfoot>
                      </table>
                  </div>
              </div>
              </div>
              <div class="Clear"></div>
              <div class="BtnCenterSection">
              	<button id="btnPPLot" class="BtnGeneralStyle BtnMSection" onclick="SPCreateOrder.onPromotionProductSaleLotChange()">Chọn</button>
                <button class="BtnGeneralStyle" onclick="$('#adjustPromotionSaleOrderLotDialog').dialog('close');">Đóng</button>
              </div>
               <div class="Clear"></div>
                <div style="height: 30px;"> 
					<p style="height: 25px; display: none;" class="ErrorMsgStyle" id="errMsg1">Có lỗi xảy ra khi cập nhật dữ liệu</p>
				</div>
          </div>
	</div>
</div>
</div>
<div style="display:none;" id="customerInfo">
		<div class="GeneralDialog General4Dialog">
        	<div class="DialogPhotoSection">
            	<div class="PhotoCols1">
                	<div class="PhotoCols1Info1">
                    	<p class="LeftStyle Sprite1"></p>
                    	<div class="PhotoLarge"><span class="BoxFrame"><span class="BoxMiddle"><img width="580" height="426" src="../../../resources/images/pic-large1.jpg"></span></span></div>
                        <p class="RightStyle Sprite1"></p>
                        <div class="Map2Section"><img width="360" height="208" src="../../../resources/images/map5.jpg"></div>
                        <p class="OnFucnStyle"><span class="HideText Sprite1">M? r?ng</span></p>
                        <p class="OffFucnStyle"><span class="HideText Sprite1">Thu nh?</span></p>
                    </div>
                    <div class="PhotoCols1Info3">
                    	<p><strong>0000008613 &ndash; Phan Ðình Giót</strong> ch?p ngày <strong>03/08/2012 09:00</strong></p>
                    </div>
                </div>
                <div class="PhotoCols2">
                	<div class="DScrollpane">
                        <ul class="ResetList PhotoCols2List">
                            <li class="FocusStyle">
                                <div class="PhotoThumbnails"><span class="BoxFrame"><span class="BoxMiddle"><img width="192" height="126" src="../../../resources/images/pic-theme1.jpg"></span></span></div>
                                <p>04/07/2012 &ndash; 12:00</p>
                            </li>
                            <li>
                                <div class="PhotoThumbnails"><span class="BoxFrame"><span class="BoxMiddle"><img width="192" height="126" src="../../../resources/images/pic-theme2.jpg"></span></span></div>
                                <p>04/07/2012 &ndash; 12:00</p>
                            </li>
                            <li>
                                <div class="PhotoThumbnails"><span class="BoxFrame"><span class="BoxMiddle"><img width="192" height="126" src="../../../resources/images/pic-theme3.jpg"></span></span></div>
                                <p>04/07/2012 &ndash; 12:00</p>
                            </li>
                            <li>
                                <div class="PhotoThumbnails"><span class="BoxFrame"><span class="BoxMiddle"><img width="192" height="126" src="../../../resources/images/pic-theme1.jpg"></span></span></div>
                                <p>04/07/2012 &ndash; 12:00</p>
                            </li>
                        </ul>
                	</div>
                </div>
                <div class="Clear"></div>
        	</div>
        </div>
    </div>
<div id="productWarehouseDialogContainer" style="display: none;">
	<div id="productWarehouseDialog" class="easyui-dialog" title="Thông tin tìm kiếm" data-options="closed: true" style="width:750px;height:auto;">
		<div class="PopupContentMid2">
			<%--
			<div>
			 	<label class="LabelStyle" id="warningWarehouse" style="margin-left:20px;color:red;">Số lượng thực đặt bạn đã nhập vượt quá tồn kho ABC . Vui lòng nhập theo tồn kho bên dưới</label></div>
  			</div>
			--%>
			<div class="GeneralForm Search1Form">
				<%--
				<h2 class="Title2Style Title2MTStyle">Tồn kho đáp ứng của sản phẩm tương ứng các Kho</h2>
				 --%>
				<div class="GridSection">
					<table id="productWarehouseGrids"></table>
				</div>
				<div class="Clear"></div>
				<div class="BtnCenterSection">
					<button id="btnSelectWarehouse" class="BtnGeneralStyle BtnMSection" onclick="return SPCreateOrder.selectProductByWarehouse();">Chọn</button>
					<button class="BtnGeneralStyle" onclick="$('.easyui-dialog').dialog('close');">Đóng</button>
				</div>
				<div class="Clear"></div>
				<input type="hidden" id="indexTmpToAddRow">
				<p style="display: none;" class="ErrorMsgStyle" id="GeneralInforErrors"></p>
			</div>
		</div>
	</div> 
</div>
<div id="productDialogContainer" style="display: none;">
<div id="productDialog" class="easyui-dialog" title="Thông tin tìm kiếm" data-options="closed: true" style="width:750px;height:auto;">
	<div class="PopupContentMid2">
		<div class="GeneralForm Search1Form">			
			<label class="LabelStyle Label2Style">Mã SP</label>
			<input id="spCreateOrderSearchProductCode" type="text" class="InputTextStyle InputText5Style" />
			<label class="LabelStyle Label2Style">Tên SP</label>
			<input id="spCreateOrderSearchProductName" type="text" class="InputTextStyle InputText5Style" />
			<div class="Clear"></div>
			<label class="LabelStyle Label2Style">Mã CTKM</label>
			<input id="spCreateOrderSearchPPCode" type="text" class="InputTextStyle InputText5Style" />
			<button class="BtnGeneralStyle BtnSearchOnDialog" id="btnSearch" onclick="return SPCreateOrder.searchProduct();" style="margin-left: 105px;">Tìm kiếm</button>
			<div class="Clear"></div>
		</div>
		<div class="GeneralForm Search1Form">
			<h2 class="Title2Style Title2MTStyle">Kết quả tìm kiếm</h2>
			<div class="GridSection">
				<table id="productGrids"></table>
			</div>
			<div class="Clear"></div>
			<div class="BtnCenterSection">
				<button class="BtnGeneralStyle BtnMSection" onclick="return SPCreateOrder.selectListProducts();">Chọn</button>
				<button class="BtnGeneralStyle" onclick="$('.easyui-dialog').dialog('close');">Đóng</button>
			</div>
			<div class="Clear"></div>
			<p style="display: none;" class="ErrorMsgStyle" id="GeneralInforErrors"></p>
		</div>
	</div>
</div> 
</div>
<div id="createOrderPromotionProgramDetailContainer" style="display: none;">
<div id="createOrderPromotionProgramDetail" class="easyui-dialog" title="Thông tin CT HTTM" style="width:616px;height:auto;" data-options="closed:true,modal:true">
	<div class="PopupContentMid2">
		<div class="GeneralForm Search1Form">
	        <label class="LabelStyle Label4Style">Mã chương trình</label>
	        <p class="ValueStyle Value2Style" id="coPPDetailCode"></p>
	        <div class="Clear"></div>
	        <label class="LabelStyle Label4Style">Tên chương trình</label>
	        <p class="ValueStyle Value2Style" id="coPPDetailName"></p>
	        <div class="Clear"></div>
	        <label class="LabelStyle Label4Style">Loại khuyến mãi</label>
	        <p class="ValueStyle Value2Style" id="coPPDetailType"></p>
	        <div class="Clear"></div>
	        <label class="LabelStyle Label4Style">Dạng chương trình</label>
	        <p class="ValueStyle Value2Style" id="coPPDetialFormat"></p>
	        <div class="Clear"></div>
	        <label class="LabelStyle Label4Style">Từ ngày</label>
	        <p class="ValueStyle Value2Style" id="coPPDetailFromDate"></p>
	        <div class="Clear"></div>
	        <label class="LabelStyle Label4Style">Đến ngày</label>
	        <p class="ValueStyle Value2Style" id="coPPDetailToDate"></p>
	        <div class="Clear"></div>
	        <label class="LabelStyle Label4Style">Diễn giải chương trình</label>
	        <p class="ValueStyle Value2Style" id="coPPDetailDescription"></p>
	        <div class="Clear"></div>
		</div>
	</div>
</div>
</div>
<div id="createOrderKSDetailContainer" style="display: none;">
<div id="createOrderKSDetail" class="easyui-dialog" title="Thông tin CTHTTM" style="width:616px;height:auto;" data-options="closed:true,modal:true">
	<div class="PopupContentMid2">
		<div class="GeneralForm Search1Form">
	        <label class="LabelStyle Label1Style">Mã CT</label>
	        <p class="ValueStyle Value3Style" id="ksCodeKSDetail"></p>
	        <label class="LabelStyle Label1Style" style="margin-left:70px;">Tên CT</label>
	        <p class="ValueStyle Value1Style" id="ksNameKSDetail"></p>
	        <div class="Clear"></div>
	        <label class="LabelStyle Label1Style">Từ chu kỳ</label>
	        <p class="ValueStyle Value3Style" id="fromCycleKSDetail"></p>
	        <label class="LabelStyle Label1Style" style="margin-left:70px;">Đến chu kỳ</label>
	        <p class="ValueStyle Value1Style" id="toCycleKSDetail"></p>
	        <div class="Clear"></div>
	        <label class="LabelStyle Label1Style">SKU</label>
	        <p class="ValueStyle Value2Style" id="skuKSDetail"></p>
	        <div class="Clear"></div>
	        <label class="LabelStyle Label1Style">Mô tả</label>
	        <p class="ValueStyle Value2Style" id="descriptionKSDetail"></p>
	        <div class="Clear"></div>
	        <h2 class="Title2Style Title2MTStyle">Mức chương trình</h2>
	        <div class="GridSection">
            	<table id="gridKSLevelDetail"></table>
            </div>
		</div>
	</div>
</div>
</div> 
<div id="paymentChangePPContainer" style="display: none;">
<div id="paymentChangePP" class="easyui-dialog" title="Danh sách CTKM đơn hàng" style="width:616px;height:auto;" data-options="closed:true,modal:true">
        <div class="PopupContentMid2">        	
            <div class="GeneralForm Search1Form">            	
                <div class="GridSection">
                   <table id="paymentChangePPGrids"></table>
                </div>
                <div class="Clear"></div>
                <div class="BtnCenterSection">                	
                    <button class="BtnGeneralStyle" onclick="$('.easyui-dialog').dialog('close');">Đóng</button>
                </div>
            </div>
		</div>
    </div>   
    </div>   
    
    
<div id="openProductWarehouseDlgContainer" style="display: none;">
	<div id="openProductWarehouseDlg">
		<div class="PopupContentMid2">
			<div class="GeneralForm Search1Form">
				<div class="GridSection">
					<div id="openProductWarehouseGrid"></div>
				</div>
				<div class="Clear"></div>
				<div class="BtnCenterSection">
					<button id="btnSelectWarehouse" class="BtnGeneralStyle BtnMSection" onclick="OpenProductPromotion.selectProductByWarehouse();">Chọn</button>
					<button class="BtnGeneralStyle" onclick="$('.easyui-dialog').dialog('close');">Đóng</button>
				</div>
				<div class="Clear"></div>
				<input type="hidden" id="indexTmpToAddRow">
				<p style="display: none;" class="ErrorMsgStyle" id="dlgGeneralInforErrors2"></p>
			</div>
		</div>
	</div> 
</div>
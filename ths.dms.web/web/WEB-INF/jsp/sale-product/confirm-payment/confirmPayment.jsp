<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@page import="ths.dms.helper.Configuration"%>
<%@taglib prefix="k" uri="/kryptone"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<tiles:insertTemplate template='/WEB-INF/jsp/sale-product/common.jsp'/>
<%-- <style type="text/css">
.panel {z-index:100003 !important;}
</style> --%>
<div class="BreadcrumbSection">
	<ul class="ResetList FixFloat BreadcrumbList">
		<li class="Sprite1"><a  class="" ><s:text name="confirm_order_ban_hang"/></a></li>
		<li><span><s:text name="confirm_order_xan_nhan_thanh_toan"/></span></li>
	</ul>
</div>
<div class="CtnOneColSection">
	<div class="ContentSection">
		<div class="ToolBarSection">
			<div class="SearchSection GeneralSSection">
				<div id="payTabContainer">
					<h2 class="Title2Style"><s:text name="confirm_order_thong_tin_tim_kiem"/></h2>
					<div class="SearchInSection SProduct1Form">
						<label class="LabelStyle Label1Style"><s:text name="unit_tree.search_unit.tab_name"/>
                    	     <span class="ReqiureStyle"><font color="red">(*)</font></span>
                    	</label>
	                   	<div class="BoxSelect BoxSelect7">
	                        <select id="shopCode" style="width: 200px;"> 
	                        </select>
	                    </div>
                    	<div class="Clear"></div>
						<label class="LabelStyle Label1Style"><s:text name="confirm_order_nvbh"/></label> 
						<div class="BoxSelect BoxSelect7">
							<select id="nvbhIdPay" style="width: 200px;">
							</select>
						</div>
						<div class="Clear"></div>
						<div class="BtnCenterSection">
							<button style="margin-left:20px;margin-top:-4px;" class=" BtnGeneralStyle" id = "btnPaySearch" onclick="return SPConfirmOrder.searchPay();"><s:text name="create_order_tim_kiem"/></button>
						</div>
						<div class="Clear"></div>
					</div>
					<div class="Clear"></div>
					<h2 class="Title2Style"><s:text name="confirm_order_danh_sach_dh"/></h2>
					<div class="SearchInSection SProduct1Form">
                       <div class="GridSection">                                   
                            <div id="searchOrderResult" class="GeneralTable">
								<div id="payScrollSection">
									<div class="BoxGeneralTTitle" id="dgGridPayContainer">
										<table id="gridPay"></table>
										<div id="pager"></div>
									</div>
								</div>
							</div>
                            <div class="Clear"></div>
                            <div class="BtnRSection">
                            	<div style="float: left;">
                                	<button class=" BtnGeneralStyle" id = "btnPayCancel" onclick="return SPConfirmOrder.cancelPay();"><s:text name="confirm_order_huy"/></button>
                            	</div>
                            	<div style="float: right;">
                                	<button class=" BtnGeneralStyle BtnMSection" id="btnPayConfirm" onclick="return SPConfirmOrder.confirmPay();" style="float:right;"><s:text name="confirm_order_thanh_toan"/></button>
                              		<input type="text" class="InputTextStyle InputText8Style" id="description" maxlength="200" style="float:right; margin-right: 10px;" />
                              		<label class="LabelStyle Label1Style" style="float:right; margin-right:10px; width:auto; padding:0;">Mô tả</label>
	                            	<div class="Clear"></div>
                                </div>
                            	<div class="Clear"></div>
                            </div>
                            <div class="Clear"></div>
                            <p class="ErrorMsgStyle SpriteErr" id="errMsgPay" style="display: none;"><s:text name="confirm_order_co_loi"/> </p>
							<p id="successMsgPay" class="SuccessMsgStyle" style="display: none"></p>
                        </div>
                    </div>
				</div>
				<div class="Clear"></div>
			</div>
		</div>
		<div class="Clear"></div>
	</div>
</div>
<div class="Clear"></div>
<div id="salesCustomerDialog" style="display: none;">
	<div id="salesCustomerDialogUIDialog" class="easyui-dialog" title="<s:text name="sale_product_customer_thong_tin_khach_hang"/>" data-options="closed:true,modal:true">
	</div>
</div>
<div id ="errEasyUIDialogDiv" style="display: none" >
	<div id="errEasyUIDialog" class="easyui-dialog" style="width: 600px; height: auto;" title="" data-options="closed:true,modal:true">
		<div class="PopupContentMid">
			<div class="GeneralForm Search1Form" >
				<div class="Clear"></div>
				<div class="GridSection" id="searchStyle1ContainerGrid">					
					<table id="errGrid" class="easyui-datagrid"></table>
				</div>
				<div class="BtnCenterSection">
					<button id="btnAccept" class="BtnGeneralStyle" onclick="SPConfirmOrder.acceptErr();">Đồng ý</button>
					<button id="btnClose" class="BtnGeneralStyle BtnGeneralMStyle" onclick="$('.easyui-dialog').dialog('close');">Đóng</button>
				</div>
				<p id="successMsgErr" class="SuccessMsgStyle" style="display: none"></p>
			</div>
		</div>
	</div>
</div>
<input type="hidden" id="shopId" value="<s:property value="currentShop.id" />" />
<script type="text/javascript">
$(document).ready(function() {
	SPConfirmOrder.bindComboboxStaffEasyUI('nvbhIdPay');
	SPConfirmOrder.initUnitCbxPay();
});
</script>
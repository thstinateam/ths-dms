<%@ page language="java" contentType="text/html; charset=UTF-8"	pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>

        <div class="PopupContentMid2">
        	<div class="GeneralForm Search1Form">            	
                <label class="LabelStyle Label5Style"><s:text name="sale_product_customer_ma_khach_hang"/> </label>
                <p class="ValueStyle Value2Style"> <s:property value="customerVo.shortCode" /> </p>
                <div class="Clear"></div>
                <label class="LabelStyle Label5Style"><s:text name="sale_product_customer_ten_khach_hang"/> </label>
                <p class="ValueStyle Value2Style"><s:property value="customerVo.customerName" /></p>
                <div class="Clear"></div>
                <label class="LabelStyle Label5Style"><s:text name="sale_product_customer_dia_chi"/> </label>
                <p class="ValueStyle Value2Style"><s:property value="customerVo.address" /></p>
                <div class="Clear"></div>
                <label class="LabelStyle Label5Style"><s:text name="sale_product_customer_dtdd"/> </label>
                <p class="ValueStyle Value2Style" id="phoneShowValue">
                	
                <div class="Clear"></div>
                <label class="LabelStyle Label5Style"><s:text name="sale_product_customer_loai_cua_hang"/></label>
                <p class="ValueStyle Value2Style"><s:property value="customerVo.shopTypeName" /></p>
                <div class="Clear"></div>
                <label class="LabelStyle Label5Style"><s:text name="sale_product_customer_do_trung_thanh"/></label>
                <p class="ValueStyle Value2Style"><s:property value="customerVo.loyalty" /></p>
                <div class="Clear"></div>
                <label class="LabelStyle Label5Style"><s:text name="sale_product_customer_nguoi_lien_he"/></label>
                <p class="ValueStyle Value2Style"><s:property value="customerVo.contactName" /></p>
                <div class="Clear"></div>
                <s:if test="customerVo.valueShowPrice == 1"> 
	                <label class="LabelStyle Label5Style"><s:text name="sale_product_customer_ban_Trong_ngay"/></label>
	                <p class="ValueStyle Value2Style"><s:property value="convertMoney(customerVo.totalInDate)" /></p>
	                <div class="Clear"></div>
                </s:if>
                <label class="LabelStyle Label5Style"><s:text name="sale_product_customer_sl_ban_Trong_ngay"/></label>
                <p class="ValueStyle Value2Style"><s:property value="convertMoney(customerVo.quantityInDate)" /></p>
                <div class="Clear"></div>
                
            </div>
            <div class="GeneralForm Search1Form">
                <h2 class="Title2Style"><s:text name="sale_product_customer_thong_tin_doanh_so"/></h2>
                <label class="LabelStyle Label5Style"><s:text name="sale_product_customer_so_lan_dat_hang"/></label>
                <p class="ValueStyle Value1Style"><s:property value="convertMoney(customerVo.numOrderInMonth)" /></p>
                <div class="Clear"></div>
                <s:if test="customerVo.valueShowPrice == 1"> 
	                <label class="LabelStyle Label5Style"><s:text name="sale_product_customer_binh_quan_doanh_so"/></label>
	                <p class="ValueStyle Value1Style"><s:property value="convertMoney(customerVo.avgTotalInLastTwoMonth)" /></p>
	                <div class="Clear"></div>
	                <label class="LabelStyle Label5Style"><s:text name="sale_product_customer_doanh_so_thuc_hien_trong_thang"/></label>
	                <p class="ValueStyle Value1Style"><s:property value="convertMoney(customerVo.totalInMonth)" /></p>
	                <div class="Clear"></div>
				</s:if>
                <label class="LabelStyle Label5Style"><s:text name="sale_product_customer_sl_binh_quan"/></label>
                <p class="ValueStyle Value1Style"><s:property value="convertMoney(customerVo.avgQuantityInLastTwoMonth)" /></p>
                <div class="Clear"></div>
                <label class="LabelStyle Label5Style"><s:text name="sale_product_customer_sl_thuc_hien_trong_thang"/></label>
                <p class="ValueStyle Value1Style"><s:property value="convertMoney(customerVo.quantityInMonth)" /></p>
                <div class="Clear"></div>
                 
            </div>
            <div class="GeneralForm Search1Form">            	
                <div  id="gridContainer" style="padding-left: 5px;padding-right: 5px;">                   
					<table id="gridCustomerInfor" title="<s:text name="sale_product_customer_danh_sach_don_hang"/>" class="easyui-datagrid"></table>
					<div id="gridCustomerInforPager"></div>
                </div>
                <div class="Clear"></div>
                <div class="BtnCenterSection" style = "margin-top :10px">
                    <button id="btnCloseDlg" class="BtnGeneralStyle" onclick="$('.easyui-dialog').dialog('close');"><s:text name="sale_product_customer_dong"/></button>
                </div>
            </div>
		</div>
		

<%-- <input id="showGia" type="hidden" value="<s:property value="customerVo.isShowPrice" />" />  --%>
<script type="text/javascript">
var isShowPrice = '<s:property value="customerVo.isShowPrice" />';
var phone = '<s:property value="customerVo.phone" />';
var mobiphone = '<s:property value="customerVo.mobiphone" />';
var value = '';
if(!isNullOrEmpty(phone) && !isNullOrEmpty(mobiphone)){
	 value = mobiphone + ' / ' + phone;
}else if(!isNullOrEmpty(phone)){
	 value = phone;
}else {
	 value = mobiphone;
}			 
$('#phoneShowValue').html(value);
</script>

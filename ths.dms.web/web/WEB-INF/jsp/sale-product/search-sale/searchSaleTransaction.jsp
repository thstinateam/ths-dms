<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>

<style type="text/css">
.BoxSelect .ui-dropdownchecklist-text {
	width: 167px !important;
}
.BoxSelect .ui-dropdownchecklist-selector {
	width: 173px !important;
}
</style>

<div class="BreadcrumbSection">
	<ul class="ResetList FixFloat BreadcrumbList">
		<li class="Sprite1"><a href="/sale-product/search-sale/info">Bán hàng</a></li>
		<li><span>Danh sách đơn hàng</span></li>
	</ul>
</div>

<input type="hidden" id="shopCode" value="<s:property value='shop.shopCode'/>" />	
<div class="CtnOneColSection">
                <div class="ContentSection">
                	<div class="ToolBarSection">
                        <div class="SearchSection GeneralSSection">
                        	<h2 class="Title2Style">Thông tin tìm kiếm</h2>
                            <div class="SearchInSection SProduct1Form">
                            	<label class="LabelStyle Label1Style">Đơn vị</label>
								<div class="BoxSelect BoxSelect2" id ="shopIdList" >
			                        <select id="shopCodeCB" style="width: 206px;">
			                        </select>
			                    </div>
			                    <label class="LabelStyle Label9Style">Ngày làm việc</label>
                				<p id="strLockDate" class="LabelStyle" style="width: 196px; text-align: left;"><s:property value="lockDate"/></p>
                            	<label class="LabelStyle Label1Style">Loại ĐH</label>
                                <div class="BoxSelect BoxSelect2">
                                    <select class="MySelectBoxClass1" id="orderType" multiple="multiple">
                                        <option value="">Tất cả</option>
										<option value="IN" selected="selected">Đơn bán PreSale (IN)</option>
										<option value="CM">Đơn trả PreSale (CM)</option>
										<option value="SO">Đơn bán Vansale (SO)</option>
										<option value="CO">Đơn trả Vansale (CO)</option>
										<option value="GO">Đơn trả hàng Vansale (GO)</option>
										<option value="DP">Đơn bán hàng Vansale (DP)</option>
										<option value="DC">Điều chuyển (DC)</option>
										<option value="DCT">Điều chỉnh tăng kho (DCT)</option>
										<option value="DCG">Điều chỉnh giảm kho (DCG)</option>
                                    </select>
                                </div>
                                <div class="Clear"></div>
                            	<label class="LabelStyle Label1Style">Số đơn hàng</label>
                                <input type="text" class="InputTextStyle InputText1Style" id="orderNumber" maxlength="50" />
                                <label class="LabelStyle Label9Style">Số đơn hàng tham chiếu</label>
                                <input type="text" class="InputTextStyle InputText1Style" id="refOrderNumber" maxlength="50" />
	                            <label class="LabelStyle Label1Style">Độ ưu tiên</label>
                                <div class="BoxSelect BoxSelect2">
                                    <s:select cssStyle="opacity: 0; height: 20px;" tabindex="6" cssClass="MySelectBoxClass" list="priority" id="priorityId"
                                            value="saleOrder.priority" listKey="apParamCode" listValue="apParamName" disabled="disabledButton" ></s:select>
                                </div>
	                            <div class="Clear"></div>
                                
                                <label class="LabelStyle Label1Style">Từ ngày</label>
                                <input type="text" class="InputTextStyle InputText6Style" id="fromDate" maxlength="10"/>
                                <label class="LabelStyle Label9Style">Đến ngày</label>
                                <input type="text" class="InputTextStyle InputText6Style" id="toDate" maxlength="10"/>
                                <label class="LabelStyle Label1Style">Trạng thái</label>
                                <div class="BoxSelect BoxSelect2">
                                    <select class="MySelectBoxClass" id="orderStatus">
                                        <option value="-1" selected="selected">Tất cả</option>
                                        <option value="0-0" >Chưa xác nhận đơn hàng</option>
                                        <option value="0-1" >Đã xác nhận đơn hàng</option>
										<option value="1-2">Đã duyệt</option>
										<option value="1-3">Đã cập nhật kho</option>
										<option value="0">Đã trả</option>
										<option value="3-">Hủy</option>
										<option value="2-">Từ chối</option>
                                    </select>
                                </div>
                                <div class="Clear"></div>
                                
                                <label class="LabelStyle Label1Style">Mã KH(F9)</label>
                                <input type="text" class="InputTextStyle InputText1Style" id="customerCode" maxlength="50"/>
                                
                                <label class="LabelStyle Label9Style">Tên KH</label>
                                <input type="text" class="InputTextStyle InputText1Style" maxlength="250" id="customerName"/>
                                
                                <label class="LabelStyle Label1Style">Tạo trên</label>
                                <div class="BoxSelect BoxSelect2">
                                    <select class="MySelectBoxClass" id="orderSource">
                                        <option value="0" selected="selected">Tất cả</option>
										<option value="1" >Web</option>
										<option value="2">Tablet</option>
                                    </select>
                                </div>
                                <div class="Clear"></div>
                                
                                <label class="LabelStyle Label1Style">NVBH</label>
                                <div class="BoxSelect BoxSelect2">
									<select id="saleStaff" >
									</select>
								</div>								
                                <label class="LabelStyle Label9Style">NVGH</label>
                                <div class="BoxSelect BoxSelect2">
									<select id="deliveryStaff">
									</select>
								</div>
                                <label class="LabelStyle Label1Style">Ngày giao</label>
                                <input type="text" class="InputTextStyle InputText6Style" id="deliveryDate" maxlength="10"/>
                                <div class="Clear"></div>
                                
                                <div class="BtnCenterSection">
                                    <button class="BtnGeneralStyle" id="btnSearch" onclick="SPSearchSale.searchOrder();">Tìm kiếm</button>
                                </div>
                                <div class="Clear"></div>
                            </div>
                            <h2 class="Title2Style">Kết quả tìm kiếm
                            	<img style="display: none;" src="/resources/scripts/plugins/jquery-easyui-1.3.2/themes/default/images/loading.gif" class="loadingTitle" >
                            </h2>
                            <div class="SearchInSection SProduct1Form">
                            	<div class="GridSection">                                   
                                   <div id="searchResult" class="GeneralTable Table34Section">
										<div id="ScrollSection" class="ScrollSection">
											<p id="gridNoResult" style="display: none" class="WarningResultStyle">Không có kết quả</p>
											<div class="BoxGeneralTTitle" id="dgGridContainer">
												<table id="grid"></table>
												<div id="pager"></div>
											</div>
										</div>
									</div>
	                                <div class="Clear"></div>
                                <div class="Clear"></div>
                                <p class="ErrorMsgStyle SpriteErr" id="errMsg" style="display: none;">Có lỗi xảy ra khi cập nhật dữ liệu</p>
								<tiles:insertTemplate template="/WEB-INF/jsp/general/successMsg.jsp" />
                            	</div>
                        	</div>
                        	<div class="Clear"></div>
                    	</div>
             	</div>
      	<div class="Clear"></div>
  	</div>
</div>
<div id="salesCustomerDialog" style="display: none;">
	<div id="salesCustomerDialogUIDialog" class="easyui-dialog" title="<s:text name="sale_product_customer_thong_tin_khach_hang"/>" data-options="closed:true,modal:true">
	</div>
</div>
<!-- Popup xem chi tiet -->
<div id="billDetailPopupDiv" style="display:none;">
    <div id="billDetailPopup" class="PopupContentMid2">         
    </div>
</div>
<tiles:insertTemplate template='/WEB-INF/jsp/sale-product/common.jsp' />
<s:hidden id="lock" name="shopLocked"></s:hidden>
<input type="hidden" id="shopId" value="<s:property value="currentShop.id" />" />
<%-- <s:hidden id="isShowPrice" name="isShowPrice"></s:hidden> --%>
<script type="text/javascript">
$(function(){
	SPSearchSale.pageLoad();
	SPSearchSale.initUnitCbx();
	$('#orderType').dropdownchecklist({emptyText:'Tất cả',firstItemChecksAll: true,
		textFormatFunction: function(options) {
			if (options[0].selected) {
				return "Tất cả";
			}
			var s = null;
			for (var i = 1, sz = options.length; i < sz; i++) {
				if (options[i].selected) {
					if (s == null) {
						s = options[i].value;
					} else {
						s = s + "," + Utils.XSSEncode(options[i].value);
					}
				}
			}
			if (s == null) {
				return "Tất cả";
			}
			return s;
		}});
});
</script>

<%@ page language="java" contentType="text/html; charset=UTF-8"	pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>

<style type="text/css">
.datagrid-footer td[field=productCode], .datagrid-footer td[field=productName],
.datagrid-footer .datagrid-td-rownumber {
	border-right-color:transparent !important;
}
</style>

<!-- <div class="BreadcrumbSection">
	<ul class="ResetList FixFloat BreadcrumbList">
		<li class="Sprite1"><a href="javascript:void(0);" class="cmsdefault">Bán hàng</a></li>
		<li><span>Xem phiếu điều chỉnh kho</span></li>
	</ul>
</div>

<div class="CtnOneColSection">
	<div class="ContentSection">
		<div class="ToolBarSection">
			<div class="SearchSection GeneralSSection">
			 -->
				<h2 class="Title2Style">Thông tin tìm kiếm</h2>
				<div class="SearchInSection SProduct1Form">
					<label class="LabelStyle Label6Style" >Loại</label>
					<s:if test="stockTrans.transType == 'DCT'" >
						<p class="ValueStyle Value1Style">Nhập điều chỉnh</p>
					</s:if>
					<s:else>
						<p class="ValueStyle Value1Style">Xuất điều chỉnh</p>
					</s:else>
					<label class="LabelStyle Label1Style" >Mã phiếu</label>
					<p class="ValueStyle Value1Style" id ="lstDPView"><s:property value="stockTrans.stockTransCode" /></p>
					<label class="LabelStyle Label1Style">Ngày</label> 
					<p id="deliveryDate" class="ValueStyle Value1Style"><s:property value="displayDate(stockTrans.stockTransDate)" /></p>
					<div class="Clear"></div>
				</div>
				
				<h2 class="Title2Style">Danh sách sản phẩm
					<img style="display: none;" src="/resources/scripts/plugins/jquery-easyui-1.3.2/themes/default/images/loading.gif" id="loadingProduct">
				</h2>
				<div class="SearchInSection SProduct1Form">
					<div class="GridSection" id="gridContainer" style="padding:0;">
						<div id="dgrid"></div>
					</div>
					<div class="Clear"></div>
					<p class="ErrorMsgStyle" id="errMsg" style="display: none;">Có lỗi xảy ra khi cập nhật dữ liệu</p> 
					<p id="successMsg" class="SuccessMsgStyle" style="display: none;"></p>
					<div class="BtnCenterSection">
						<!-- <button class="BtnGeneralStyle Sprite2" id="btnClose" onclick="return SPSearchSale.closeOrderView('<s:property value="lnk" />');">Đóng</button> -->
						<button class="BtnGeneralStyle Sprite2" id="btnClose" onclick="$('#billDetailPopup').dialog('close');">Đóng</button>
                    </div>
				</div>
				<input type="hidden" id="lstDP" />
			<!-- </div>
		</div>
	</div>
</div> -->

<%-- --%>
<script type="text/javascript">
$(document).ready(function() {
	VansaleDP.loadPageView('<s:property value="orderId" />', 'DCTG', '<s:property value="currentShopCode" />');
});
</script>
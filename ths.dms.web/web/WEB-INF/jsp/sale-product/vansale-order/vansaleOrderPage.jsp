<%@ page language="java" contentType="text/html; charset=UTF-8"	pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>

<style type="text/css">
.datagrid-footer td[field=productCode], .datagrid-footer td[field=productName],
.datagrid-footer td[field=warehouseId], .datagrid-footer .datagrid-td-rownumber {
	border-right-color:transparent !important;
}
</style>

<div class="BreadcrumbSection">
	<ul class="ResetList FixFloat BreadcrumbList">
		<li class="Sprite1"><a href="javascript:void(0);" class="cmsdefault">Bán hàng</a></li>
		<li><span>Lập đơn bán hàng vansale (DP)</span></li>
	</ul>
</div>

<div class="CtnOneColSection">
	<div class="ContentSection">
		<div class="ToolBarSection">
			<div class="SearchSection GeneralSSection">
			
				<h2 class="Title2Style">Thông tin chung</h2>
				<div class="SearchInSection SProduct1Form">
					<label class="LabelStyle Label2Style"><s:text name="unit_tree.search_unit.tab_name"/>
                            <span class="ReqiureStyle"><font color="red">*</font></span>
                    </label>
                    <div class="BoxSelect BoxSelect2" id ="shopIdList" >
                        <select id="shopCodeCB" style="width: 206px;">
                        </select>
                    </div>
					<label class="LabelStyle Label1Style">Mã phiếu</label> 
					<input type="text" class="InputTextStyle InputText1Style" id="DPNumber" disabled="disabled" autocomplete="off" />
					
					<label class="LabelStyle Label1Style">Ngày</label> 
					<input type="text" class="InputTextStyle InputText1Style" id="fDate" value="<s:property value='lockDate' />" disabled="disabled" autocomplete="off" />
					<div class="Clear"></div>
					
					<label class="LabelStyle Label2Style">Nhân viên <span class="ReqiureStyle">*</span></label>
					<div class="BoxSelect BoxSelect2">
						<select class="MySelectBoxClas" id="saler" style="width: 206px;">
							<option value=""></option>
							<s:iterator value="lstStaff" var="obj">
								<option value="<s:property value='#obj.staffCode' />"><s:property value='#obj.staffName' /></option>
							</s:iterator>
						</select>
					</div>
					
					<label class="LabelStyle Label1Style">Chọn xe <span class="ReqiureStyle">*</span></label>
					<div class="BoxSelect BoxSelect2">
						<div class="BoxSelect BoxSelect2" id ="car" >
                        <select id="carBox" style="width: 206px;">
                        </select>
                    </div>
					</div>
					
					<label class="LabelStyle Label1Style">Người lập</label> 
					<input type="text" class="InputTextStyle InputText1Style" value="<s:property value='currentUser.userName' />-<s:property value='currentUser.fullName' />" disabled="disabled" autocomplete="off" />
					<div class="Clear"></div>
				</div>
				
				<h2 class="Title2Style">Danh sách sản phẩm
					<img style="display: none;" src="/resources/scripts/plugins/jquery-easyui-1.3.2/themes/default/images/loading.gif" id="loadingProduct">
				</h2>
				<div class="SearchInSection SProduct1Form">
					<div class="GridSection" id="gridContainer" style="padding:0;">
						<div id="dgrid"></div>
					</div>
					<div class="BtnCenterSection">
						<button class="BtnGeneralStyle" id="btnSave" onClick="VansaleDP.saveOrder();" >Lưu</button>
					</div>
					<div class="Clear"></div>	
					<p class="ErrorMsgStyle" id="errMsg" style="display: none;">Có lỗi xảy ra khi cập nhật dữ liệu</p> 
					<p id="successMsg" class="SuccessMsgStyle" style="display: none;"></p>
				</div>
				
			</div>
		</div>
	</div>
</div>

<%-- --%>
<script type="text/javascript">
$(document).ready(function() {
	VansaleDP.loadPage('<s:property value="lockPage" />', '<s:property value="errMsg" />');
});
</script>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>

<div class="BreadcrumbSection">
	<ul class="ResetList FixFloat BreadcrumbList">
		<li class="Sprite1"><a href="javascript:void(0)">Bán hàng</a></li>
		<li><span>Hủy hóa đơn thuế GTGT</span></li>
	</ul>
</div>
<div class="CtnOneColSection">
	<div class="ContentSection">
		<div class="ToolBarSection">
			<div class="SearchSection GeneralSSection">
			<input id="shopCode" type="hidden" value="<s:property value="shop.shopCode"/>"/>
			<input id="shopId" type="hidden" value="<s:property value="shop.id"/>"/>
				<h2 class="Title2Style">Thông tin tìm kiếm</h2>
				<div class="SearchInSection SProduct1Form">
					<label class="LabelStyle Label2Style">Đơn vị</label>
					<div class="BoxSelect BoxSelect2">
						<select id="cbxShop" style="width:206px;">
						</select>					
					</div>
					<label class="LabelStyle Label1Style">Ngày làm việc</label>
                	<p id="lockDate" class="ValueStyle Value1Style"></p>
                	<div class="Clear"></div>
                	
                	<label class="LabelStyle Label2Style">Mã KH (F9)</label>					 
					<input type="text" class="InputTextStyle InputText1Style" tabindex="8" maxlength="50" id="customerCode" />
					
					<label	class="LabelStyle Label1Style">Hình thức thanh toán</label>
					<div class="BoxSelect BoxSelect2">
						<s:select list="listPayment" listKey="value" id="choosePaymentMethod" tabindex="9" headerValue="Chọn" headerKey="-1"
							cssClass="MySelectBoxClass" listValue="name" value="-1">
						</s:select>
					</div>
					<div class="Clear"></div>
					
					<label class="LabelStyle Label2Style">Thuế suất</label> 
					<input	type="text" class="InputTextStyle InputText1Style" tabindex="3" maxlength="10" id="taxValue"/>  
					
					<label class="LabelStyle Label1Style">Số đơn hàng</label> 
					<input type="text" class="InputTextStyle InputText1Style" tabindex="4" maxlength="50" id="invoiceNumber"/>
					
					<label class="LabelStyle Label1Style">Số hóa đơn đỏ</label> 
					<input type="text" class="InputTextStyle InputText1Style" tabindex="4" maxlength="50" id="redInvoiceNumber"/>
					
					<div class="Clear"></div>
					<label class="LabelStyle Label2Style">Từ ngày <span class="ReqiureStyle">*</span></label> 
					<input	type="text" class="InputTextStyle InputText6Style vinput-date" tabindex="5" maxlength="10" id="createFromDate" value="<s:property value="createFromDate"/>"/> 
					
					<label class="LabelStyle Label1Style">Đến ngày <span class="ReqiureStyle">*</span></label> 
					<input	type="text" class="InputTextStyle InputText6Style vinput-date" tabindex="6" maxlength="10" value="<s:property value="createToDate"/>" id="createToDate" />

					<div class="Clear"></div>
					<label class="LabelStyle Label2Style">Mã NVBH</label> 
					<div class="BoxSelect BoxSelect2">
						<select tabindex="7" id="saleStaffCode" style="width:206px;">
						</select>					
					</div>
					 
					<label class="LabelStyle Label1Style">NVGH</label>
					<div class="BoxSelect BoxSelect2"> 
						<select tabindex="2" id="deliveryStaffCode" style="width:206px;">
						</select>
					</div>
					
					<label class="LabelStyle Label2Style" style="margin-left: 20px;">Giá trị ĐH</label>
					<div class="BoxSelect BoxSelect2">
	                    <select class="" id="isValueOrder" style="width:210px" tabindex="8">
	                         <option value="-1">Tất cả</option>
	                         <option value="1" selected="selected">Có giá trị</option>
	                         <option value="0">Không có giá trị</option>
	                    </select>
                    </div>
                    
					<div class="Clear"></div>
					<div class="BtnCenterSection">
						<button class="BtnGeneralStyle" tabindex="10" id="btnSearch" onclick="return SPRemoveTax.search();">Tìm kiếm</button>
					</div>

					<div class="Clear"></div>
				</div>
				<p class="ErrorMsgStyle SpriteErr" id="errMsg" style="display: none;">Có lỗi xảy ra khi cập nhật dữ liệu</p>
				<h2 class="Title2Style">Kết quả tìm kiếm</h2>
				<div class="SearchInSection SProduct1Form">
					<div class="GridSection">
						<div class="ResultSection" id="areaGrid">
								<table id="grid" style="overflow: scroll; height: 330px;"></table>
						</div>							
					</div>
					<div class="Clear"></div>
					<div class="BtnRSection">
						<button id="save" class="BtnGeneralStyle BtnMSection" >Hủy</button>
					</div>
					<div class="Clear"></div>
					<tiles:insertTemplate template="/WEB-INF/jsp/general/successMsg.jsp" />	
				</div>
			</div>
			<div class="Clear"></div>
		</div>
	</div>
	<div class="Clear"></div>
</div>
<script type="text/javascript">
$(document).ready( function() {	
	SPRemoveTax.initUnitCbx();
	<s:iterator value="listPayment">
		var obj = new Object();
		obj.value = '<s:property value="value"/>';
		obj.name = '<s:property value="name"/>';
		SPRemoveTax.listPayment.push(obj);		
	</s:iterator>
	$('#deliveryStaffCode').focus();	
	$('#chooseStatus').val(0);
	$('#chooseStatus').change();
	Utils.bindComboboxStaffEasyUI('deliveryStaffCode');
	Utils.bindComboboxStaffEasyUI('saleStaffCode');
	
   	$('#save').unbind('click');
  	$('#save').bind('click', function(e) {
		SPRemoveTax.save();
	});

	$('#customerCode').bind('keyup',function(event) {
		var shopCode = $('#cbxShop').combobox('getValue');
		if (shopCode != null && shopCode != undefined && shopCode.trim() != '') {
			$('#errMsg').html('').hide();
			if (event.keyCode == keyCode_F9) {
				VCommonJS.showDialogSearch2({
                    inputs : [
             	 		{id:'code', maxlength:50, label:'Mã KH'},
               	 		{id:'name', maxlength:250, label:'Tên KH'},
                 		{id:'address', maxlength:250, label:'Địa chỉ', width:410},
                          ],
                    params : {shopCode: shopCode.trim()},
                    url : '/commons/customer-in-shop/search',
                    columns : [[
                           {field:'shortCode', title:'Mã KH', align:'left', width: 50, sortable:false, resizable:false, formatter:function(value,row,index) {
                        	   return VTUtilJS.XSSEncode(value);        
                           }},
                           {field:'customerName', title:'Tên khách hàng', align:'left', width: 120, sortable:false, resizable:false, formatter:function(value,row,index) {
                        	   return VTUtilJS.XSSEncode(value);        
                           }},
                           {field:'address', title:'Địa chỉ', align:'left', width: 200, sortable:false, resizable:false, formatter:function(value,row,index) {
                        	   return VTUtilJS.XSSEncode(value);        
                           }},
                           {field:'choose', title:'', align:'center', width:40, sortable:false, resizable:false, formatter:function(value,row,index) {
                        	   return "<a href='javascript:void(0)' onclick='chooseCustomer(1,\""+ Utils.XSSEncode(row.shortCode) + "\");'>Chọn</a>";        
                           }}
                    ]]
              });
			}
		} else {
			$('#errMsg').html('Đơn vị không được để trống. Vui lòng nhập đơn vị').show();
		}
	});		
});
function chooseCustomer(type,code) {
	$('#customerCode').val(code);
	$('.easyui-dialog').dialog('close');
}
</script>
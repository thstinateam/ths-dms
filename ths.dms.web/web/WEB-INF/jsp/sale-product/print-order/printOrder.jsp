<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@page import="ths.dms.helper.Configuration"%>
<%@taglib prefix="k" uri="/kryptone"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%-- <style type="text/css">
.panel {z-index:100003 !important;}
</style> --%>
<div class="BreadcrumbSection">
	<ul class="ResetList FixFloat BreadcrumbList">
		<li class="Sprite1"><a href="javascript:void(0);">Đơn hàng</a></li>
		<li><span>Duyệt đơn hàng</span></li>
	</ul>
</div>
<div class="CtnOneColSection">
	<div class="ContentSection">
		<div class="ToolBarSection">
			<div class="SearchSection GeneralSSection">
				<h2 class="Title2Style">Thông tin tìm kiếm</h2>
				<div class="SearchInSection SProduct1Form" id="search-form">
					<label class="LabelStyle Label1Style">Đơn vị</label>
					<div class="BoxSelect BoxSelect2" id ="shopIdList" >
                        <select id="shopCodeCB" style="width: 206px;">
                        </select>
                    </div>
                    <label class="LabelStyle Label1Style">Ngày làm việc</label>
					<p id="strLockDate" class="LabelStyle" style="width: 196px; text-align: left;"><s:property value="lockDate"/></p>
                    <div class="Clear"></div>
					<label class="LabelStyle Label1Style">Đơn hàng tham chiếu</label>
                    <input type="text" id="refOrderNumber" class="InputTextStyle InputText1Style"/>
					<label class="LabelStyle Label1Style">Loại đơn</label> 
					<div class="BoxSelect BoxSelect2">
						<select id="orderType" class="MySelectBoxClass">
							<option value="IN">Đơn bán PreSale (IN)</option>
							<option value="SO">Đơn bán Vansale (SO)</option>
							<option value="CO">Đơn trả Vansale (CO)</option>
							<option value="CM">Đơn trả PreSale (CM)</option>
							<option value="GO">Đơn trả hàng Vansale (GO)</option>
							<option value="DP">Đơn bán hàng Vansale (DP)</option>
<!-- 							<option value="AI">Điều chỉnh doanh thu tăng (AI)</option> -->
<!-- 							<option value="AD">Điều chỉnh doanh thu giảm (AD)</option> -->
						</select>
					</div>
					<label class="LabelStyle Label1Style">Tạo trên</label> 
					<div class="BoxSelect BoxSelect2">
						<select id="orderSource" class="MySelectBoxClass">
							<option value="-1" selected="selected">Tất cả</option>
							<option value="1" >WEB</option>
							<option value="2" >TABLET</option>
						</select>
					</div>
					<div class="Clear"></div>
					<label class="LabelStyle Label1Style">Mã KH (F9)</label>					 
					<input type="text" class="InputTextStyle InputText1Style" tabindex="8" maxlength="50" id="customerCode" />
					<label class="LabelStyle Label1Style">Tên KH</label>					 
					<input type="text" class="InputTextStyle InputText1Style" tabindex="8" maxlength="50" id="customerName" />
					<label class="LabelStyle Label1Style">NVBH</label> 
					<div class="BoxSelect BoxSelect2">
						<select id="nvbhCode" >
						</select>
					</div>
					<div class="Clear"></div>
					<label class="LabelStyle Label1Style">NVGH</label> 
					<div class="BoxSelect BoxSelect2">
						<select id="nvghCode">
						</select>
					</div>
					<div id = "valueOrderDiv">
						<label class="LabelStyle Label1Style">Giá trị ĐH</label>
						<div class="BoxSelect BoxSelect2">
		                    <select class="MySelectBoxClass" id="isValueOrder" >
		                         <option value="-1" selected="selected">Tất cả</option>
		                         <option value="1">Có giá trị &gt;=</option>
		                         <option value="0">Có giá trị &lt; </option>
		                    </select>
	                    </div>		
	                    <label class="LabelStyle LabelStyle"></label>
	     				<input type="text" class="InputTextStyle InputText4Style vinput-money" disabled="disabled" id="numberValueOrder" type="text" maxlength="18"/>
     				</div>
     				<div class="Clear"></div>
					<div class="BtnCSection">
						<button class="BtnGeneralStyle" id="btnSearch" onclick="return SPPrintOrder.search();">Tìm kiếm</button>
					</div>
					<div class="Clear"></div>
					<p class="ErrorMsgStyle SpriteErr" id="errMsgSearch" style="display: none;">Có lỗi xảy ra khi cập nhật dữ liệu</p>
					<div class="Clear"></div>
				</div>
				<div class="Clear"></div>
				<h2 class="Title2Style">Danh sách đơn hàng</h2>
				<div class="SearchInSection SProduct1Form">
                    <div class="GridSection">
                		<div id="searchOrderResult" class="GeneralTable">
							<div class="BoxGeneralTTitle" style="width: 100%;" id="dgGridContainer">
								<table id="grid"></table>
								<div id="pager"></div>
							</div>
						</div>
		                <div class="Clear"></div>
		                <div class="BtnCenterSection">
		                	<button class="BtnGeneralStyle BtnMSection cmsiscontrol" style="float:left;" id="btnCancel" onclick="return SPPrintOrder.cancelOrder();">Hủy</button>
		                	<button class="BtnGeneralStyle BtnMSection cmsiscontrol" id="btnConfirmUpdateStock" onclick="return SPPrintOrder.confirmAndUpdateStock();" style="float:right; margin-right:20px;">Xác nhận & cập nhật kho</button>
		                    <button class="BtnGeneralStyle BtnMSection cmsiscontrol" id="btnConfirm_" onclick="return SPPrintOrder.confirm();" style="float:right; margin-right:20px;">Xác nhận</button>
		                    <div class="Clear"></div>
		                </div>
	                    <div class="Clear"></div>
	                    <p id="successMsg" class="SuccessMsgStyle" style="display: none; padding-left: 10px;"></p>
	                    <br>
	                    <p class="ErrorMsgStyle SpriteErr" id="errorMsg" style="display: none; margin-left: 5px;">Có lỗi xảy ra khi cập nhật dữ liệu</p>
                      </div>
                </div>
				<div class="Clear"></div>
			</div>
		</div>
		<div class="Clear"></div>
	</div>
</div>
<div class="Clear"></div>
<input id="shopId" value='<s:property value="shopId"/>' type="hidden">

<div id ="dialogStockOrder" style="display:none;" >
	<div id="dialogStockOrderEasyUIDialog">
		<div class="SearchInSection SProduct1Form">
			<div class="GridSection">	                 					
				<table id="dgError"></table>
			</div>
			<label class="LabelStyle LabelStyle" id="confirmMsg" style="display:none;width:auto !important;"></label>
			 <div class="BtnCenterSection">
			 	  <button class="BtnGeneralStyle" onclick="" id="btnConfirm" style="display:none;">Đồng ý</button>
                  <button class="BtnGeneralStyle" onclick="$('#dialogStockOrderEasyUIDialog').dialog('close');">Đóng</button>
              </div>                             
     	</div>    
	</div>
</div>
<div id="printOrderWarningPopupDiv" style="display:none">
	<div id="printOrderWarningPopup">
		<div class="PopupContentMid">
			<div class="GeneralForm" style="padding-top: 0; padding-bottom: 0;">
				<div class="icon-warning" style="width: 48px; height: 48px; float: left; margin-right: 20px; margin-top: -7px;"></div>
				<p style="padding: 2px 0px; margin-left: 68px; margin-bottom: 10px;">Đơn gốc của (những) đơn hàng sau đã có hóa đơn.<br/>Bạn có muốn hủy số hóa đơn đó?</p>
				<div class="Clear"></div>
				<div class="GeneralForm Search1Form">
                	<div id="ivGridCtn" class="GridSection" style="padding-top: 3px; padding-bottom: 3px;">
                		<div id="invoiceGrid"></div>
                	</div>
                	<div class="Clear"></div>
            	</div>
            	<div class="Clear"></div>
				<div class="BtnCenterSection">
					<button id="btnOK" class="BtnGeneralStyle cmsdefault">Đồng ý</button>
					<button class="BtnGeneralStyle cmsdefault" onclick="$('#printOrderWarningPopup').dialog('close');">Đóng</button>
				</div>
			</div>
		</div>
	</div>
</div>
<!-- Popup xem chi tiet -->
<div id="billDetailPopupDiv" style="display:none;">
	<div id="billDetailPopup" class="PopupContentMid2">			
	</div>
</div>
<script type="text/javascript">
$(document).ready(function() {
	$('#customerCode').bind('keyup', function(event) {					
		if (event.keyCode == keyCode_F9) {
			VCommonJS.showDialogSearch2({
			    inputs : [
			        {id:'code', maxlength:50, label:'Mã KH'},
			        {id:'name', maxlength:250, label:'Tên KH'}
			    ],
			    params:{shopId:$('#shopId').val(), shopCode:$("#shopCodeCB").combobox('getValue')},
			    url : '/commons/customer-in-shop/search',
			    columns : [[
			        {field:'shortCode', title:'Mã KH', align:'left', width: 110, sortable:false, resizable:false, formatter:function(value,row,index) {
                  	   return VTUtilJS.XSSEncode(value);        
                    }},
			        {field:'customerName', title:'Tên KH', align:'left', width: 150, sortable:false, resizable:false, formatter:function(value,row,index) {
                  	   return VTUtilJS.XSSEncode(value);        
                    }},
			        {field:'choose', title:'', align:'center', width:40, sortable:false, resizable:false, formatter:function(value,row,index) {
			        	return "<a href='javascript:void(0)' onclick=\"return SPPrintOrder.getResultF9Customer("+ index + ");\">Chọn</a>";        
			        }}
			    ]]
			});
		} 
	});
	Utils.bindComboboxStaffEasyUI('nvbhCode');
	Utils.bindComboboxStaffEasyUI('nvghCode');
	SPPrintOrder.initUnitCbx();
	SPPrintOrder._lstOrder=new Map();
	SPPrintOrder._params = {orderType : $('#orderType').val(), orderSource : $('#orderSource').val(), };
	var lnk = '&lnk=' + window.location.href;
	$('#orderType').change(function(data){
		var ORDER_TYPE_IN = "IN";
	    var selectedOrderType = $(this).val();
	    if (selectedOrderType && selectedOrderType == ORDER_TYPE_IN) {
	    	$('#btnCancel').show();
			enable('btnCancel');
	    } else {
	    	$('#btnCancel').hide();
	    	disabled('btnCancel');
	    }
	});
	
	//xu ly bind value money dau phay
	Utils.formatCurrencyFor('numberValueOrder');
	$('#isValueOrder').bind('change', function(event){
		var value = $(this).val();
		if($('#numberValueOrder').val().trim() != ''){
			SPAdjustmentTax._textChangeBySeach.avg = $('#numberValueOrder').val().replace(/,/g, '');
		}
		if (Number(value) ==  -1) {
			$('#numberValueOrder').val(Utils.XSSEncode(SPAdjustmentTax._textChangeBySeach.all));
			disabled('numberValueOrder');
		} else {
			enable('numberValueOrder');
			$('#numberValueOrder').val(formatCurrency(SPAdjustmentTax._textChangeBySeach.avg));
		}
	});
});
</script>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@page import="ths.dms.helper.Configuration"%>
<%@taglib prefix="k" uri="/kryptone"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<tiles:insertTemplate template='/WEB-INF/jsp/sale-product/common.jsp'/>
<%-- <style type="text/css">
.panel {z-index:100003 !important;}
</style> --%>
<div class="BreadcrumbSection">
	<ul class="ResetList FixFloat BreadcrumbList">
		<li class="Sprite1"><a  class="" ><s:text name="confirm_order_ban_hang"/></a></li>
		<li><span><s:text name="confirm_order_xac_nhan_dh"/></span></li>
	</ul>
</div>
<div class="CtnOneColSection">
	<div class="ContentSection">
		<div class="ToolBarSection">
			<div class="SearchSection GeneralSSection">
				<div id="orderTabContainer">
					<h2 class="Title2Style">Thông tin tìm kiếm</h2>
					<div class="SearchInSection SProduct1Form">
						<label class="LabelStyle Label1Style"><s:text name="unit_tree.search_unit.tab_name"/>
                            <span class="ReqiureStyle"><font color="red">(*)</font></span>
                    	</label>
                    	<div class="BoxSelect BoxSelect2" id ="shopIdList"   >
                        	<select id="shopCodeCB" style="width: 206px;">
                        	</select>
                    	</div>
                    	<label class="LabelStyle Label9Style"><s:text name="chot.ngay.title.ngay.lam.viec"/></label>
                    	<p class="ValueStyle Value1Style" id="lockDay"></p>
                    	<div class="Clear"></div>
						<label class="LabelStyle Label1Style">NVBH</label> 
						<div class="BoxSelect BoxSelect2">
							<select id="nvbhId" class="easyui-combobox" style="width: 204px;">
							</select>
						</div>
						
						<label class="LabelStyle Label9Style">NVGH</label>
                        <div class="BoxSelect BoxSelect2">
                            <select class="easyui-combobox" id="deliveryStaff" >
								<option value=""></option>
								<!-- <s:iterator value="listNVGH" var="obj" >								
									<option value="<s:property value="#obj.staffCode" />"><s:property value="#obj.staffCode" /> - <s:property value="#obj.staffName" /></option>
								</s:iterator>	 -->
							</select>		
                        </div>

						<label class="LabelStyle Label1Style">Loại đơn</label> 
						<div class="BoxSelect BoxSelect2" style="width: 209px;">
							<select id="orderType" class="MySelectBoxClass" style="width: 209px;">
								<option value="IN">Đơn bán PreSale (IN)</option>
								<option value="SO">Đơn bán Vansale (SO)</option>
								<option value="CO">Đơn trả Vansale (CO)</option>
							</select>
						</div>
						<div class="Clear"></div>

						<label class="LabelStyle Label1Style">Độ ưu tiên</label>
                        <div class="BoxSelect BoxSelect2">
                            <s:select cssStyle="opacity: 0; height: 20px;" tabindex="6" cssClass="MySelectBoxClass" list="priority" id="priorityId"
                                    value="saleOrder.priority" listKey="apParamCode" listValue="apParamName" disabled="disabledButton" ></s:select>
                        </div>

                        <label class="LabelStyle Label9Style">Mã KH(F9)</label>
                        <input type="text" class="InputTextStyle InputText1Style" id="customerCode" maxlength="50"/>
                        
                        <label class="LabelStyle Label1Style">Tên KH</label>
                        <input type="text" class="InputTextStyle InputText1Style" maxlength="250" id="customerName"/>

                        <div class="Clear"></div>
                        <label class="LabelStyle Label1Style" id="gtDH">Giá trị ĐH</label>
						<div class="BoxSelect BoxSelect2" id ="devValueOrder">
		                    <select class="MySelectBoxClass" id="isValueOrder" >
		                         <option value="-1" selected="selected">Tất cả</option>
		                         <option value="1">Có giá trị &gt;=</option>
		                         <option value="0">Có giá trị &lt; </option>
		                    </select>
	                    </div>		
	                    <label class="LabelStyle LabelStyle"></label>
	     				<input type="text" class="InputTextStyle InputText4Style vinput-money" disabled="disabled" id="numberValueOrder" type="text" maxlength="18"/>
	     				<div class="Clear"></div>
                        <div class="BtnCenterSection">
							<button style="margin-left:20px;margin-top:-4px;" class=" BtnGeneralStyle" id="btnSearch" onclick="return SPConfirmOrder.searchOrder();">Tìm kiếm</button>
						</div>
						<div class="Clear"></div>
					</div>
					<div class="Clear"></div>
					<h2 class="Title2Style">Danh sách đơn hàng</h2>
					<div class="SearchInSection SProduct1Form">
                       <div class="GridSection">                                   
                            <div id="searchOrderResult" class="GeneralTable">
								<div id="orderScrollSection" class="">
									<div class="BoxGeneralTTitle" id="dgGridOrderContainer">
										<table id="gridOrder"></table>
										<div id="pager"></div>
									</div>
								</div>
							</div>
                            <div class="Clear"></div>
                            <div class="BtnRSection">
                            	<div style="float: left;">
	                                <button class=" BtnGeneralStyle" id ="btnOrderCancel" onclick="return SPConfirmOrder.cancelOrder();">Hủy</button>
                            	</div>
                            	<div style="float: right; margin-right:20px;">
	                                <button style="float:right;" class=" BtnGeneralStyle BtnMSection" id="btnOrderApproved" onclick="return SPConfirmOrder.confirmOrder();">Chấp nhận</button>
	                                <button class=" BtnGeneralStyle BtnMSection" style="float:right; margin-right:30px;" id="btnOrderDeny" onclick="return SPConfirmOrder.denyOrder();">Từ chối</button>
	                              	<input type="text" class="InputTextStyle InputText8Style" id="description" maxlength="200" style="float:right; margin-right:10px;" />
	                            	<label class="LabelStyle Label1Style" style="float:right; margin-right:10px; width:auto; padding:0;">Mô tả</label>
	                            	<div class="Clear"></div>
                                </div>
                                <div class="Clear"></div>
                            </div>
                            <div class="Clear"></div>
                            <p class="ErrorMsgStyle SpriteErr" id="errMsgOrder" style="display: none;">Có lỗi xảy ra khi cập nhật dữ liệu</p>
                            <p class="ErrorMsgStyle" id="errMsgOrder2" style="display: none;"></p>
							<p id="successMsgOrder" class="SuccessMsgStyle" style="display: none"></p>
                        </div>
                    </div>
                    <div id="panelDetail" style="display:none;">
	                    <h2 class="Title2Style">Chi tiết đơn hàng <span id="orderNumberDetail" style="color:red;"></span></h2>
						<div class="SearchInSection SProduct1Form">
	                       <div class="GridSection">                                   
	                            <div id="searchDetailResult" class="GeneralTable">
									<div id="ScrollSection" class="">
										<div class="BoxGeneralTTitle" id="dgGridDetailContainer">
											<table id="gridDetail"></table>
											<div id="pager"></div>
										</div>
									</div>
								</div>
	                            <div class="Clear"></div>
	                        </div>
	                     </div>
                     </div>
				</div>
				<div class="Clear"></div>
			</div>
		</div>
		<div class="Clear"></div>
	</div>
</div>
<div class="Clear"></div>
<div style="display: none">
	<div id="divConfirmPrice" class="PopupContentMid">
		<div class="GeneralForm Search1Form">
			<label id="txtConfirmPrice" class="LabelStyle"></label>
			<div class="Clear"></div>
			<hr size="1">
			<div class="Clear"></div>
			<div class="BtnCenterSection">
				<button id="btnApplyPriceAll" onclick="SPConfirmOrder.applyPriceAll();"  class="BtnGeneralStyle BtnGeneralMStyle">Xác nhận tất cả</button>
				<button id="btnApplyPrice" onclick="SPConfirmOrder.applyPrice();" class="BtnGeneralStyle BtnGeneralMStyle">Xác nhận</button>
				<button id="btnDismissPrice" onclick="SPConfirmOrder.dismissPrice();" class="BtnGeneralStyle BtnGeneralMStyle" onclick="">Bỏ qua</button>
				<button id="btnDismissPriceAll" onclick="SPConfirmOrder.dismissPriceAll();" class="BtnGeneralStyle BtnGeneralMStyle" onclick="">Bỏ qua tất cả</button>
			</div>
		</div>
	</div>
	<div id="divConfirmDebit" class="PopupContentMid">
		<div class="GeneralForm Search1Form" >
			<label id="txtConfirmDebit" class="LabelStyle"></label>
			<div class="Clear"></div>
			<hr size="1">
			<div class="Clear"></div>
			<div class="BtnCenterSection">
				<button id="btnSkipDebitAll" onclick="SPConfirmOrder.skipDebitAll();" class="BtnGeneralStyle BtnGeneralMStyle">Xác nhận tất cả</button>
				<button id="btnSkipDebit" onclick="SPConfirmOrder.skipDebit();" class="BtnGeneralStyle BtnGeneralMStyle">Xác nhận</button>
				<button id="btnDismissDebit" onclick="SPConfirmOrder.dismissDebit();" class="BtnGeneralStyle BtnGeneralMStyle" onclick="">Không xác nhận</button>
				<button id="btnDismissDebitAll" onclick="SPConfirmOrder.dismissDebitAll();" class="BtnGeneralStyle BtnGeneralMStyle" onclick="">Không xác nhận tất cả</button>
			</div>
		</div>
	</div>
</div>
<div id="salesCustomerDialog" style="display: none;">
	<div id="salesCustomerDialogUIDialog" class="easyui-dialog" title="<s:text name="sale_product_customer_thong_tin_khach_hang"/>" data-options="closed:true,modal:true">
	</div>
</div>
<div id ="errEasyUIDialogDiv" style="display: none" >
	<div id="errEasyUIDialog" class="easyui-dialog" style="width: 600px; height: auto;" title="" data-options="closed:true,modal:true">
		<div class="PopupContentMid">
			<div class="GeneralForm Search1Form" >
				<div class="Clear"></div>
				<div class="GridSection" id="searchStyle1ContainerGrid">					
					<table id="errGrid" class="easyui-datagrid"></table>
				</div>
				<div class="BtnCenterSection">
					<button id="btnAccept" class="BtnGeneralStyle" onclick="SPConfirmOrder.acceptErr();">Đồng ý</button>
					<button id="btnClose" class="BtnGeneralStyle BtnGeneralMStyle" onclick="$('.easyui-dialog').dialog('close');">Đóng</button>
				</div>
				<p id="successMsgErr" class="SuccessMsgStyle" style="display: none"></p>
			</div>
		</div>
	</div>
</div>
<input type="hidden" id="shopId" value="<s:property value="currentShop.id" />" />
<script type="text/javascript">
$(document).ready(function() {
	SPConfirmOrder.bindComboboxStaffEasyUI('nvbhId');
	SPConfirmOrder.bindComboboxStaffEasyUI('nvbhIdPay');
	Utils.bindComboboxStaffEasyUI('deliveryStaff');	
	SPConfirmOrder.initUnitCbx();
	SPCreateOrder.loadProductForSale();
	var chose = true;
	var showPrice = '<s:property value="isShowPrice"/>';
	SPConfirmOrder.initPageControl();
	(function($){ function pagerFilter(data){
			if ($.isArray(data)){ // is array
				data = { total: data.length, rows: data }
			}
			var dg = $(this);
			var state = dg.data('datagrid');
			var opts = dg.datagrid('options');
			if (!state.allRows){
				state.allRows = (data.rows);
			}
			var start = (opts.pageNumber-1)*parseInt(opts.pageSize);
			var end = start + parseInt(opts.pageSize);
			data.rows = $.extend(true,[],state.allRows.slice(start, end));
			return data;
		}
		 
		var loadDataMethod = $.fn.datagrid.methods.loadData;
		$.extend($.fn.datagrid.methods, {
			clientPagingErr: function(jq){
				return jq.each(function(){
					var dg = $(this);
					var state = dg.data('datagrid');
					var opts = state.options;
					opts.loadFilter = pagerFilter;
					var onBeforeLoad = opts.onBeforeLoad;
					opts.onBeforeLoad = function(param){
						state.allRows = null;
						return onBeforeLoad.call(this, param);
					}
					dg.datagrid('getPager').pagination({
						onSelectPage:function(pageNum, pageSize){
							opts.pageNumber = pageNum;
							opts.pageSize = pageSize;
							$(this).pagination('refresh',{
								pageNumber:pageNum,
								pageSize:pageSize
							});
							dg.datagrid('loadData',state.allRows);
						}
					});
					$(this).datagrid('loadData', state.data);
					if (opts.url){
						$(this).datagrid('reload');
					}
				});
			},
			loadData: function(jq, data){
				jq.each(function(){
					$(this).data('datagrid').allRows = null;
				});
				return loadDataMethod.call($.fn.datagrid.methods, jq, data);
			},
			getAllRows: function(jq){
				return jq.data('datagrid').allRows;
			}
		});
	})(jQuery);
	setTimeout(function(){
		$('.easyui-dialog #errGrid').datagrid({
			data: [],
			autoRowHeight : true,
			checkOnSelect :true,
			pagination:true,
			pageSize : 10,
			pageList: [10,20,30,40,50],
			scrollbarSize : 0,
			singleSelect:true,
			pageNumber:1,
			fitColumns:true,
			width : ($('#errEasyUIDialog').width() - 40),
		    columns:[[  
				{field:'stt',title:'STT',align:'center', width:40, sortable : false,resizable : false},
		        {field:'orderNumber',title:'Số đơn hàng',align:'left', width:110, sortable : false,resizable : false,
		        	formatter: function(v, r, i) {
		        		return Utils.XSSEncode(r.orderNumber);
		        	}
		        },  
		        {field:'err',title:'Lỗi',align:'left', width:230, sortable : false,resizable : false,
		        	formatter: function(v, r, i) {
		        		var str='';
		        		var stt = 1;
		        		if (r.systemError == 'X') {
		        			str += (stt++) + '.  Lỗi hệ thống.<br>';
		        		}
		        		if (r.orderNotIntegrityErr == 'X') {
		        			str += (stt++) + '.  Lỗi toàn vẹn đơn hàng.<br>';
		        		}
		        		if (r.confirmedErr == 'X') {
		        			str += (stt++) + '.  Đơn hàng đã xác nhận.<br>';
		        		}
		        		if(r.debitMax == 'X') {
		        			str += (stt++) + '.  Số tiền của khách hàng vượt quá định mức.<br>';
		        		}
		        		if(r.debitMaxDate == 'X') {
		        			str += (stt++) + '.  Số ngày nợ của khách hàng vượt quá định mức.<br>';
		        		}
		        		if(r.quantityErr != null && r.quantityErr.length > 0) {
		        			str += (stt++) + '.  Hết tồn kho sản phẩm ' + r.quantityErr + '.<br>';
		        		}
		        		if(r.promotionErr == 'X') {
		        			str += (stt++) + '.  CTKM hết hạn.<br>';
		        		}
		        		if(r.amountErr == 'X') {
		        			str += (stt++) + '.  Mất chi tiết.<br>';
		        		}
		        		if(r.priceErr == 'X') {
		        			str += (stt++) + '.  Sai giá.<br>';
		        		}
		        		if(r.SOCOErr == 'X') {
		        			str += (stt++) + '.  Đơn bán SO chưa qua bước in hóa đơn.<br>';
		        		}
		        		if(r.openPromo == 'X') {
		        			str += (stt++) + '.  Khuyến mãi mở mới đã được trả.<br>';
		        		}
		        		if(r.accumulation == 'X') {
		        			str += (stt++) + '.  Trả thưởng khuyến mãi tích lũy không còn đúng đắn.<br>';
		        		}
		        		if(r.notEnoughPortion == 'X') {
		        			str += (stt++) + '.  Có CTKM không đủ số suất.<br>';
		        		}
		        		if(r.changeQuantity == 'X') {
		        			str += (stt++) + '.  Có SPKM bị đổi số lượng.<br>';
		        		}
		        		if(r.changeRation == 'X') {
		        			str += (stt++) + '.  Có CTKM bị đổi số suất.<br>';
		        		}
		        		if(r.keyShopErr == 'X') {
		        			str += (stt++) + '.  Trả thưởng chương trình hỗ trợ thương mại vượt quá định mức trả.<br>';
		        		}
		        		if(r.customerNotActive == 'X') {
		        			str += (stt++) + '.  Khách hàng này đã ngưng hoạt động, bạn không thể tiến hành xác nhận đơn hàng được.<br>';
		        		}
		        		return str;
		        	}
		        },				        
		        {field:'select', title:'Chấp nhận', width:65, align:'center',sortable : false,resizable : false,formatter : function(v,r,i){
		        		if (r.systemError != 'X' && r.orderNotIntegrityErr != 'X' && r.confirmedErr != 'X' && (r.quantityErr == null || r.quantityErr.length == 0) && r.promotionErr != 'X' && r.amountErr != 'X' && r.SOCOErr != 'X' && r.openPromo != 'X' && r.accumulation != 'X'
		        				&& r.notEnoughPortion != 'X' && r.keyShopErr != 'X') {
		        			var checked = '';
		        			if (SPConfirmOrder._mapOrderErr.get(r.id) != null ) {
		        				checked = 'checked=true';
		        			}
		        			return '<input '+checked+' class="cbErr"'+Utils.XSSEncode(r.orderNumber)+' type="checkbox" onclick="SPConfirmOrder.onClickCBErr(this,'+r.id+')" />';
		        		}
		        }},
		    ]],
		    onLoadSuccess :function(data){
		    	if(data != undefined && data != null && data.rows != undefined && data.rows != null && data.rows.length > 0){
		    		$('#btnAccept').hide();
		    		for(var i = 0; i < data.rows.length; i++){
		    			var r = data.rows[i];
		    			if (r.quantityErr != 'X' && r.promotionErr != 'X' && r.amountErr != 'X' && r.SOCOErr != 'X' && r.openPromo != 'X' && r.accumulation != 'X'
	        				&& r.notEnoughPortion != 'X') {
		    				$('#btnAccept').show();
		    				break;
		    			}
		    		}
		    	}
		    }		    
		}).datagrid('clientPagingErr');
	},1000);
	
	//bo sung text box gia tri DH
	var numberValueOrder = '<s:property value="numberValue"/>';
	if(numberValueOrder != '' && numberValueOrder != "") {
		SPAdjustmentTax.numberValueOrder = numberValueOrder;
		SPAdjustmentTax._textChangeBySeach.avg = numberValueOrder.replace(/,/g, '');
	}
		//xu ly bind value money dau phay
	Utils.formatCurrencyFor('numberValueOrder');
	$('#isValueOrder').bind('change', function(event){
		var value = $(this).val();
		if($('#numberValueOrder').val().trim() != ''){
			SPAdjustmentTax._textChangeBySeach.avg = $('#numberValueOrder').val().replace(/,/g, '');
		}
		if (Number(value) ==  -1) {
			$('#numberValueOrder').val(SPAdjustmentTax._textChangeBySeach.all);
			disabled('numberValueOrder');
		} else {
			enable('numberValueOrder');
			$('#numberValueOrder').val(formatCurrency(SPAdjustmentTax._textChangeBySeach.avg));
		}
	});	
});
</script>
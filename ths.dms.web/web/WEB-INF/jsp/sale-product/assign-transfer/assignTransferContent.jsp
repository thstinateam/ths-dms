<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<input type="hidden" id="function_code" value="<s:property value="systimestamp"/>" />
<s:set id="downloadToken" value="%{generateReportToken()}"></s:set>
<input type="hidden" id="report_token" value="<s:property value="#downloadToken"/>" />
<s:set id="downloadToken" value="%{putSessionValue(systimestamp, #downloadToken)}"></s:set>

<div class="BreadcrumbSection">
    <ul class="ResetList FixFloat BreadcrumbList">
        <li class="Sprite1"><a href="/sale-product/assign-transfer-staff/info">Bán hàng</a></li>
        <li><span>Chỉ định NVGH</span></li>
    </ul>
</div>
<div class="CtnOneColSection">
    <div class="ContentSection">
    	<div class="ToolBarSection">
            <div class="SearchSection GeneralSSection">
            	<h2 class="Title2Style">Thông tin chung</h2>
                <div class="SearchInSection SProduct1Form">
                	<label class="LabelStyle Label2Style">Mã NPP<span class="ReqiureStyle">*</span></label>
                	<div class="BoxSelect BoxSelect2">
                		<input  type="text" class="InputTextStyle InputText1Style" id="cbxShop" maxlength="50" />
                	</div>
                	<label class="LabelStyle Label2Style">Ngày làm việc:</label>
                	<p id="strLockDate" class="LabelStyle"><s:property value="lockDate"/></p>
                	<div class="Clear"></div>
                
                	<label class="LabelStyle Label2Style">Mã NVBH</label>
                    <div class="BoxSelect BoxSelect2">
                        <select class="easyui-combobox" id="saleStaffCode">
                        </select>
                    </div>
                    <label class="LabelStyle Label2Style">Mã NVGH</label>
                    <div class="BoxSelect BoxSelect2">
                        <select class="easyui-combobox" id="transferStaffCode">
                        </select>
                    </div>
                    <label class="LabelStyle Label2Style">Mã NVTT</label>
                     <div class="BoxSelect BoxSelect2">
                         <select class="easyui-combobox" id="cashierStaffCode">
                         </select>
                     </div>
                   
                    <div class="Clear"></div>
                    <label class="LabelStyle Label2Style">Chọn xe</label>
                     <div class="BoxSelect BoxSelect2">
                         <select class="easyui-combobox" id="carId">
                         </select>
                     </div>
                    <label class="LabelStyle Label2Style">Mã KH(F9)</label>
                    <input type="text" id="customerCode" maxlength="50" class="InputTextStyle InputText1Style" />
                    <label class="LabelStyle Label2Style">Số đơn hàng</label>
                    <input type="text" id="orderNumber" maxlength="50" class="InputTextStyle InputText1Style" />
                    <div class="Clear"></div>
                    <label class="LabelStyle Label2Style">Ngày <span class="ReqiureStyle">*</span></label>
                    <input type="text" id="date" value="<s:property value="displayDate(now)"/>" maxlength="10" class="InputTextStyle InputText6Style vinput-date" />
                    <label class="LabelStyle Label2Style">Độ ưu tiên</label>
                    <div class="BoxSelect BoxSelect2">
                        <s:select cssStyle="opacity: 0; height: 20px;" tabindex="6" cssClass="MySelectBoxClass" list="priority" id="priorityId"
                                value="saleOrder.priority" listKey="apParamCode" listValue="apParamName" disabled="disabledButton" ></s:select>
                    </div>
					<label class="LabelStyle Label2Style" >Giá trị ĐH</label>
					<div class="BoxSelect BoxSelect2">
						<select class="MySelectBoxClass" id="isValueOrder" >
							 <option value="-1" selected="selected">Tất cả</option>
							 <option value="1">Có giá trị &gt;=</option>
							 <option value="0">Có giá trị &lt; </option>
						</select>
					</div>
					
					<label class="LabelStyle LabelStyle"></label>
					<input type="text" class="InputTextStyle InputText4Style vinput-money" disabled="disabled" id="numberValueOrder" type="text" maxlength="15"/>
                    <div class="Clear"></div>
                    <label class="LabelStyle Label2Style">Loại ĐH</label>
                    <div class="BoxSelect BoxSelect2" style="">
                    	<select class="" id="orderType" multiple="multiple" style="">
                    		<option value="">Tất cả</option>
                    		<option value="IN" selected="selected">Đơn bán PreSale (IN)</option>
                    		<option value="SO">Đơn bán Vansale (SO)</option>
                    	</select>
                    </div>
                    <div class="Clear"></div>
                    <div class="BtnCenterSection">
                        <button id="btnSearch" class="BtnGeneralStyle" onclick="return SPAssignTransferStaff.searchSaleOrderForDelivery();">Tìm kiếm</button>
                    </div>
                    <div class="Clear"></div>
                    <p id="errMsgInfo" class="ErrorMsgStyle" style="display: none">Có
						lỗi xảy ra khi cập nhật dữ liệu</p>
                    <input type="hidden" id="shopCode" value="<s:property value="shop.shopCode"/>"/>
                    <input type="hidden" id="shopId" value="<s:property value="shop.Id"/>"/>
                </div>
                <h2 class="Title2Style">Kết quả tìm kiếm</h2> 
                <div class="SearchInSection SProduct1Form">
                	<div class="GridSection" id="gridContainer" style="padding:0;">
                		<table id="grid" ></table>
                	</div>                	
                    <div class="Clear"></div>
                    <div class="BtnRSection" style="padding:10px">
                    	<input style="margin-left: 70px;" type="checkbox" id="checkSelectTransferStaff" class="InputCbxStyle InputCbx5Style"/>
                        <label class="LabelStyle Label7Style">Mã NVGH mới</label>
                        <div class="BoxSelect BoxSelect2">
                            <select class="easyui-combobox" id="listStaff">
                            </select>
                        </div>
                        <input style="margin-left: 70px;" type="checkbox" id="checkSelectCashierStaff" class="InputCbxStyle InputCbx5Style"/>
                        <label class="LabelStyle Label7Style">Mã NVTT mới</label>
                        <div class="BoxSelect BoxSelect2">
                            <select class="easyui-combobox" id="listCashierStaff">
                            </select>
                        </div>
                        <input type="checkbox" class="InputCbxStyle InputCbx2Style" id="checkSelectCar" />
                        <label class="LabelStyle Label8Style">Chọn xe</label>
                        <div class="BoxSelect BoxSelect2">
                            <select class="easyui-combobox" id="listCar">
                            </select>
                        </div>
                        <button id="btnUpdate" class="BtnGeneralStyle" onclick = "return SPAssignTransferStaff.updateAssignTransfer();">Cập nhật</button>
                    </div>
                    <div class="SearchInSection SProduct1Form" style="float: right; padding: 0px; margin-right: 0px;">
						<div class="GeneralForm GeneralNoTP1Form" style="float: right; margin-right: 0px; padding: 0px;">
							<div class="Func1Section" style="width: 474px; float: right; margin-right: 0px; padding: 0px;">
								<div id="idImportDiv">
									<p class="DownloadSFileStyle DownloadSFile2Style">
										<a id="downloadTemplate1" href="/resources/templates/sale-product/Template_Import_Chi_Dinh_NVGH.xls" class="Sprite1">Tải file mẫu</a>
									</p>
									<div class="DivInputFile">
										<form action="/sale-product/assign-transfer-staff/import" name="importFrm" id="importFrm" method="post" enctype="multipart/form-data">
											<input type="file" class="InputFileStyle" size="14" name="excelFile" id="excelFile" onchange="previewImportExcelFile(this,'importFrm');">
											<input type="hidden" id="isView" name="isView">
											<div class="FakeInputFile">
												<input id="fakefilepc" type="text" readonly="readonly" class="InputTextStyle">
											</div>
										</form>
									</div>
									<button class="BtnGeneralStyle" onclick = "return SPAssignTransferStaff.importAssignTransfer(fakefilepc);">Nhập Excel</button>
									<button class="BtnGeneralStyle"  onclick = "return SPAssignTransferStaff.exportAssignTransfer();">Xuất Excel</button>
								</div>
								<div class="Clear"></div>
							</div>
						</div>
					</div>
					
					<p id="errorMsg" class="ErrorMsgStyle" style="display: none"></p>
                    <p id="errExcelMsg" class="ErrorMsgStyle" style="display: none"></p>
                    
                    <p id="errMsg" class="ErrorMsgStyle" style="display: none">Có lỗi xảy ra khi cập nhật dữ liệu</p>
					<p id="successMsg" class="SuccessMsgStyle" style="display: none"></p>
					<p id="successMsgProduct" class="SuccessMsgStyle" style="display: none"></p>
					
                </div>
            </div>
            <div class="Clear"></div>
        </div>
    </div>
    <div class="Clear"></div>
</div>

<script>
	$(document).ready(function() {
		$('.MySelectBoxClass').css('width','206');
		$('.CustomStyleSelectBox').css('width','173');
		SPAssignTransferStaff.initInfo();
		Utils.bindComboboxStaffEasyUI('saleStaffCode');
		Utils.bindComboboxStaffEasyUI('transferStaffCode');
		Utils.bindComboboxStaffEasyUI('cashierStaffCode');
		$('#orderType').dropdownchecklist({
			width: 260,
			emptyText:'Tất cả',
			firstItemChecksAll: true,
			textFormatFunction: function(options) {
				if (options[0].selected) {
					return "Tất cả";
				}
				var s = null;
				for (var i = 1, sz = options.length; i < sz; i++) {
					if (options[i].selected) {
						if (s == null) {
							s = options[i].value;
						} else {
							s = s + "," + options[i].value;
						}
					}
				}
				if (s == null) {
					return "Tất cả";
				}
				return s;
			}
		});

		SPAssignTransferStaff._lstSaleOrderSelect = new Map();
		$('#customerCode').bind('keyup', function(event) {
			if (event.keyCode == keyCode_F9) {
				VCommonJS.showDialogSearch2({
					inputs : [
		          	{id:'code', maxlength:50, label:'Mã KH'},
			        {id:'name', maxlength:250, label:'Tên KH'},
			        {id:'address', maxlength:250, label:'Địa chỉ', width:410},
					      ],
					params : {shopCode: $('#cbxShop').combobox('getValue')},
					url : '/commons/customer-in-shop/search',
					columns : [[
						{field:'shortCode', title:'Mã KH', align:'left', width: 50, sortable:false, resizable:false, formatter:function(value,row,index) {
                     	   return VTUtilJS.XSSEncode(value);        
                        }},
						{field:'customerName', title:'Tên khách hàng', align:'left', width: 120, sortable:false, resizable:false, formatter:function(value,row,index) {
                     	   return VTUtilJS.XSSEncode(value);        
                        }},
						 {field:'address', title:'Địa chỉ', align:'left', width: 200, sortable:false, resizable:false, formatter:function(value,row,index) {
                      	   return VTUtilJS.XSSEncode(value);        
                         }},
						{field:'choose', title:'', align:'center', width:40, sortable:false, resizable:false, formatter:function(value,row,index) {
							return '<a href="javascript:void(0)" onclick="return SPAssignTransferStaff.callSelectF9CustomerCode('+index+',\''+Utils.XSSEncode(row.shortCode)+'\',\''+Utils.XSSEncode(row.customerName)+'\');">Chọn</a>';         
						}}
					]]
				});
				
				
			}
		});
		
		$('.BoxSelect2').each(function(){
			if(!$(this).is(':hidden')){
				$(this).bind('keyup', function(event){
					if(event.keyCode == keyCodes.ENTER){
						$('#btnSearch').click();
					}
				}).focus();
			}
		});
		
		var numberValueOrder = '<s:property value="numberValue"/>';
		if(numberValueOrder != '' && numberValueOrder != "") {
			SPAdjustmentTax.numberValueOrder = numberValueOrder;
			SPAdjustmentTax._textChangeBySeach.avg = numberValueOrder.replace(/,/g, '');
		}
		Utils.bindFormatOnTextfield('numberValueOrder',Utils._TF_NUMBER); // bind khong cho nhap ky tu dac biet
		//xu ly bind value money dau phay
		Utils.formatCurrencyFor('numberValueOrder');
		$('#isValueOrder').bind('change', function(event){
			var value = $(this).val();
			if($('#numberValueOrder').val().trim() != ''){
				SPAdjustmentTax._textChangeBySeach.avg = $('#numberValueOrder').val().replace(/,/g, '');
			}
			if (Number(value) ==  -1) {
				$('#numberValueOrder').val(Utils.XSSEncode(SPAdjustmentTax._textChangeBySeach.all));
				disabled('numberValueOrder');
			} else {
				enable('numberValueOrder');
				$('#numberValueOrder').val(formatCurrency(SPAdjustmentTax._textChangeBySeach.avg));
			}
		});
	});
</script>


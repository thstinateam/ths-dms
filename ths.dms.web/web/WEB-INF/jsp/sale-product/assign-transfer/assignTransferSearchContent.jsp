<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<script>
	$(document).ready(function() {

		$('#Section1').jScrollPane();
		$('#Section2').jScrollPane();
	});
</script>


		<!-- ajax saleOrderDetail -->
		<div class="ScrollBodySection" id="Section2">
		
			<table width="100%" border="0" cellspacing="0" cellpadding="0">
				<colgroup>
					<col style="width: 54px;" />
					<col style="width: 54px;" />
					<col style="width: 134px;" />
					<col style="width: 102px;" />
					<col style="width: 126px;" />
					<col style="width: 126px;" />
					<col style="width: 126px;" />
					<col style="width: 104px;" />
					<col style="width: 104px;" />
					<col style="width: 104px;" />
				</colgroup>
				<tbody id="searchResult">
				<s:if test="listSaleOrder != null && listSaleOrder.size() >0">
					<s:iterator value="listSaleOrder" status="stt">
					<tr>
						<td class="ColsTd1"><s:property value="#stt.index+1" />
						</td>
						<td class="ColsTd2"><input type="checkbox" class="checkboxList" onchange="return Utils.onCheckboxChangeForCheckAll(this.checked, 'checkboxList', 'checkboxAll');" name="<s:property value="id" />" />
						</td>
						<td class="ColsTd3"><s:property value="orderNumber" />
						</td>
						<td class="ColsTd4"><s:property value="displayDate(orderDate)" />
						</td>
						<td class="ColsTd5" style="text-align:center;"><s:property value="customer.shortCode" />
						</td>
						<td class="ColsTd6"><s:property value="staff.staffCode" />
						</td>
						<td class="ColsTd7"><s:property value="delivery.staffCode" />
						</td>
						<td class="ColsTd8"><s:property value="car.carNumber" />
						</td>
						<td class="ColsTd9"><s:property value="convertMoney(amount)" />
						</td>
						<td class="ColsTd10"><s:property value="totalWeight" />
						</td>
						</tr>
					</s:iterator>
					<tr><td colspan="10"></td></tr>
					</s:if>
					<s:else>
					<tr><td colspan="10"><p>Không tìm thấy kết quả.</p></td></tr>
					</s:else>
				</tbody>
			</table>
			
		</div>



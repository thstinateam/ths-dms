<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>

<style>
@page{
	size:595px 420px portrait; 
	margin:5px 0 0px 15px;
}

h3{
	font-family:Tahoma, Geneva, sans-serif;
	font-size:13px;
	margin:2px 0 0 10px;
	font-weight:normal;
}

h2{
	font-family:Tahoma, Geneva, sans-serif;
	font-size:11px;
	margin:2px 0 0 10px;
	padding-top:-12px;
	font-weight:normal;
}

h1{
	font-family:Tahoma, Geneva, sans-serif;
	font-size:20px;
	font-weight:normal;
	text-align:center;
	vertical-align:middle;
	/*display:table-cell;*/
}

body{
	margin:0 auto;
	width:100%;
	height:100%;
}

.header{
	width:100%;
	float:left;
	/*height:70px;*/
}

img.logo{
	float:left;
	height:20px;
	width:70px;
	margin-left:8px;
	margin-top:10px;
	vertical-align:middle;
}

.content{
	float:left;
	width:100%;
	height:auto;
	margin-top:5px;
}

.info, .info1{
	width:100%;
	height:auto;
}

.info p{
	font-family:Tahoma, Geneva, sans-serif;
	font-size:13px;
	padding:0px;
	margin:0 0 0 10px;
}

.info1 p{
	font-family:Tahoma, Geneva, sans-serif;
	font-size:13px;
	padding:0px;
	margin:0 0 0 10px;
	font-weight:normal;
}

.info .col1{
	width:60%;
	float:left;
}

.info .col2{
	width:40%;
	float:left;
}

.data, .data1{
	margin:2px 0 0 10px;
	width:96%;
	margin-left: 2%;
/* 	border: 0.25px dotted black; */
	font-family:Tahoma, Geneva, sans-serif;
	font-size:13px;
	border-collapse:collapse;
}

.data .title{
	text-align:center;
	vertical-align:middle;
	border: 0.25px dotted black;
	font-weight:normal;
}

.left{
	font-weight:normal;
	text-align:left;
	vertical-align:middle;
	border: 0.25px dotted black;
}

.right{
	font-weight:normal;
	text-align:right;
	vertical-align:middle;
	border: 0.25px dotted black;
}

.data1{
	border:none;
}

.center{
	font-weight:normal;
	text-align:center;
	vertical-align:middle;
	border:none;
	font-weight:normal;
}

.title1{
	font-weight:normal;
	text-align:center;
	vertical-align:middle;
}

@media print {
	.break_page{
		page-break-after: always;
		page-break-inside: inherit;
	}
}

.Clear {
    clear: both;
    padding: 0 !important;
}

</style>
<s:set id="iRun" value="0"></s:set>
<s:iterator value="viewGopOrder">
	<div style="width: 100%; height: 420px;">
		<div class="header">
			<div>
	        	<h1>PHIẾU GIAO NHẬN VÀ THANH TOÁN</h1>
	        </div>
			<div>
	        	<h2>NPP: <s:property value="parametersReport.get('SHOP_NAME')"/></h2>
	        	<h2>SĐT: <span><s:property value="parametersReport.get('PHONE_BOOK')"/></span></h2>
	            <h2>Ngày in: <span><s:property value="parametersReport.get('PRINT_DATE')"/></span></h2>
	        </div>
		</div>
		
		<div class="content">
	    	<div class="info">
	    		<div class="col1">
			        <h3 style="margin-left: 10px;">Số P.GH: <span style="font-weight:bold;"><s:property value="lstOrderNumber"/></span></h3>
			        <s:if test="typePrint == 0">
	        			<h3 style="margin-left: 10px;">Khách hàng: <span style="font-weight:bold;"><s:property value="customerInfo"/></span></h3>
	        		</s:if>
	        		<s:if test="typePrint == 0">
	        			<h3 style="margin-left: 10px;">SĐT: <span style="font-weight:bold;"><s:property value="phone"/></span></h3>
	        		</s:if>
	            </div>
	            <div class="col2">
	            	<s:if test="typePrint == 0">
                		<h3>Ngày P.GH: <span style="font-weight:bold;"><s:property value="lstOrderDate"/></span></h3>
                	</s:if>
                	<s:if test="typePrint == 0 || typePrint == 1">
                		<h3>NVGH: <span style="font-weight:bold;"><s:property value="lstDelivery"/></span></h3>
                	</s:if>
	            </div>
	        </div>
	        <div class="Clear"></div>
	        <s:if test="typePrint == 0">
		        <h3 style="margin-left: 10px;">Địa chỉ giao hàng: <span style="font-weight:bold;"><s:property value="address"/></span></h3>
			</s:if>
			<div class="info1" style="margin-top:8px;">
	        	<p style="margin-left: 10px; font-weight:bold;">1. Hàng bán</p>
	        </div>
	        <div class="table" style="clear:both; padding-top:3px;">
	       	  <table class="data">
	              <tr style="font-weight: bold;">
	                <td class="title" style="font-weight:bold;" scope="col" rowspan="2">Mã hàng</td>
	                <th class="title" style="font-weight:bold;" scope="col" rowspan="2">Tên hàng</th>
	                <th class="title" style="font-weight:bold;" scope="col" rowspan="2">&nbsp;QC&nbsp;</th>
	                <th class="title" style="font-weight:bold;" scope="col" colspan="2">Số lượng</th>
	                <th class="title" style="font-weight:bold;" scope="col" rowspan="2">Đơn giá</th>
	                <th class="title" style="font-weight:bold;" scope="col" rowspan="2">Thành tiền</th>
	              </tr>
	              <tr style="font-weight: bold;">
	                <th class="title" style="font-weight:bold;" scope="col">Thùng</th>
	                <th class="title" style="font-weight:bold;" scope="col">&nbsp;Lẻ&nbsp;</th>
	              </tr>
	              <s:iterator value="lstSaleProduct">
	              		<s:iterator value="lstProductLot" status="rowstatus">
	              			<s:if test="#rowstatus.index == 0">
				              	<tr>
					                <td class="center" style="border: 0.25px dotted black" rowspan="<s:property value="lstProductLot.size"/>"><s:property value="productCode"/></td>
					                <td class="left" rowspan="<s:property value="lstProductLot.size"/>"><s:property value="productName"/></td>
					                <td class="center" style="border: 0.25px dotted black"><s:property value="convfact"/></td>
					                <s:if test="(thung) == 0">
					                	<td class="center" style="border: 0.25px dotted black">-</td>
					                </s:if>
					                <s:else>
					                	<td class="center" style="border: 0.25px dotted black;"><s:property value="thung"/></td>
					                </s:else>
					                <s:if test="(le) == 0">
					                	<td class="center" style="border: 0.25px dotted black">-</td>
					                </s:if>
					                <s:else>
					                	<td class="center" style="border: 0.25px dotted black;"><s:property value="le"/></td>
					                </s:else>
					                
					                <td class="right" style="border: 0.25px dotted black"><s:property value="convertMoney(packagePrice)"/>/<s:property value="convertMoney(price)"/></td>
					                <s:if test="(amount) == 0">
					                	<td class="right" style="border: 0.25px dotted black">-</td>
					                </s:if>
					                <s:else>
					                	<td class="right" style="border: 0.25px dotted black"><s:property value="convertMoney(amount)"/></td>
					                </s:else>
			              		</tr>
		              		</s:if>
		              		<s:else>
		              			<tr>
		              				<td class="center" style="border: 0.25px dotted black"><s:property value="convfact"/></td>
					                <s:if test="(thung) == 0">
					                	<td class="center" style="border: 0.25px dotted black">-</td>
					                </s:if>
					                <s:else>
					                	<td class="center" style="border: 0.25px dotted black;font-weight:bold;"><s:property value="thung"/></td>
					                </s:else>
					                <s:if test="(le) == 0">
					                	<td class="center" style="border: 0.25px dotted black">-</td>
					                </s:if>
					                <s:else>
					                	<td class="center" style="border: 0.25px dotted black;font-weight:bold;"><s:property value="le"/></td>
					                </s:else>
					                <td class="right"><s:property value="convertMoney(packagePrice)"/>/<s:property value="convertMoney(price)"/></td>
					                <s:if test="(amount) == 0">
					                	<td class="right" style="border: 0.25px dotted black">-</td>
					                </s:if>
					                <s:else>
					                	<td class="right" style="border: 0.25px dotted black"><s:property value="convertMoney(amount)"/></td>
					                </s:else>
					           	</tr>
		              		</s:else>
	              		</s:iterator>
	              </s:iterator>
					<tr style="border:none; border-collapse: collapse;">
		                <td class="left" style="border:none;"></td>
		                <td class="center" style="font-weight:bold; border:none; font-size: 13px;"></td>
		                <td class="right" style="border:none;"></td>
		                <td class="center" style="font-weight:bold; border:none; font-size: 13px;"></td>
		                <td class="center" style="font-weight:bold; border:none; font-size: 13px;"></td>
		                <td class="right" style="border:none;">Tổng tiền</td>
		                <td class="right" style="border:none; font-weight:bold;"><s:property value="convertMoney(amount)"/></td>
		           </tr>
		           <tr style="border:none; border-collapse: collapse;">
		                <td class="left" style="border:none;"></td>
		                <td class="center" style="font-weight:bold; border:none; font-size: 13px;"></td>
		                <td class="right" style="border:none;"></td>
		                <td class="center" style="font-weight:bold; border:none; font-size: 13px;"></td>
		                <td class="center" style="font-weight:bold; border:none; font-size: 13px;"></td>
		                <td class="right" style="border:none;">Chiết khấu</td>
		                <td class="right" style="border:none; font-weight:bold;"><s:property value="convertMoney(moneyDiscount)"/></td>
		           </tr>
		           <tr style="border:none; border-collapse: collapse;">
		                <td class="left" style="border:none;"></td>
		                <td class="center" style="font-weight:bold; border:none; font-size: 13px;"></td>
		                <td class="right" style="border:none;"></td>
		                <td class="center" style="font-weight:bold; border:none; font-size: 13px;"></td>
		                <td class="center" style="font-weight:bold; border:none; font-size: 13px;"></td>
		                <td class="right" style="border:none; font-weight:bold;">Tổng tiền thanh toán</td>
		                <td class="right" style="border:none; font-weight:bold;"><s:property value="convertMoney(total)"/></td>
		           </tr>
	            </table>
	        </div>
	        <div class="info" style="margin-top:3px;">
	        	<p style="margin-left: 25px;">Số tiền bằng chữ: <span style="font-weight:bold; font-size: 13px;"><s:property value="totalString"/></span></p>
	        </div>
	        
	        <s:if test="lstPromoProduct.size() > 0"> 
		        <div class="info1" style="margin-top:8px;">
		        	<p style="margin-left: 10px; font-weight:bold;">2. Hàng khuyến mãi/trả thưởng CT.HTTM</p>
		        </div>
		        <div class="table1">
		       	  <table class="data">
		              <tr style="font-weight: normal;">
		                <th class="title" scope="col" style="font-weight:bold;" rowspan="2">Mã hàng</th>
		                <th class="title" scope="col" style="font-weight:bold;" rowspan="2">Tên hàng</th>
		                <th class="title" scope="col" style="font-weight:bold;" rowspan="2">&nbsp;QC&nbsp;</th>
		                <th class="title" scope="col" style="font-weight:bold;" colspan="2">Số lượng</th>
		                <th class="title" scope="col" style="font-weight:bold;" rowspan="2">Đơn giá</th>
		                <th class="title" scope="col" style="font-weight:bold;" rowspan="2">Chương trình KM</th>
		              </tr>
		              <tr style="font-weight: normal;">
		                <th class="title" scope="col" style="font-weight:bold;">Thùng</th>
		                <th class="title" scope="col" style="font-weight:bold;">&nbsp;Lẻ&nbsp;</th>
		              </tr>
		              <s:iterator value="lstPromoProduct">
			              <s:iterator value="lstProductLot">
				              <tr>
				                <td class="center" style="font-weight:bold; border: 0.25px dotted black" rowspan="lstProductLot.size()"><s:property value="productCode"/></td>
				                <td class="left" style="font-weight:bold;" rowspan="lstProductLot.size()" style="font-size: 11px"><s:property value="productName"/></td>
				                <td class="center" style="border: 0.25px dotted black;"><s:property value="convfact"/></td>
				                <td class="center" style="border: 0.25px dotted black;"><s:property value="thung"/></td>
				                <td class="center" style="border: 0.25px dotted black;"><s:property value="le"/></td>
				                <td class="right" style="border: 0.25px dotted black;"><s:property value="convertMoney(price)"/>/<s:property value="convertMoney(price)"/></td>
				                <td class="left" style="font-size: 11px;"><s:property value="promotionName"/></td>				                
				              </tr>
			              </s:iterator>
		              </s:iterator>
		            </table>
		        </div>
	        </s:if>
	        
	        <div class="data2" style="margin-top: 10px; margin-bottom: 10px;">
	        	<table class="data1">
	              <tr>
	                <th class="center" scope="col">Kế toán</th>
	                <th class="center" scope="col">Thủ kho</th>
	                <th class="center" scope="col">NV Giao Hàng</th>
	                <th class="center" scope="col">KH ký nhận</th>
	                <th class="center" scope="col">Ghi chú</th>
	              </tr>
	            </table>
	        </div>
		</div>
			        <s:set id="iRun" value="#iRun + 1"></s:set>
	</div>
	<div style="position: relative; bottom: 0;">
		<p style="font-size: 11px; float: right; position: relative; right: 20;  margin:0px; padding:0px; display:inline;">Trang <s:property value="#iRun"/>/<s:property value="viewGopOrder.size()"/></p>
	</div>
	<p class="break_page" style=" margin:0px; padding:0px; display:inline; font-size: 1px;">&nbsp;</p>
</s:iterator>

<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>

<style>
@page{
	size:595px 420px portrait; 
	margin:5px 0 0px 15px;
}

h3{
	font-family:Tahoma, Geneva, sans-serif;
	font-size:13px;
	margin:2px 0 0 12px;
	font-weight:normal;
}

h2{
	font-family:Tahoma, Geneva, sans-serif;
	font-size:11px;
	margin:2px 0 0 10px;
	padding-top:-12px;
	font-weight:normal;
}

h1{
	font-family: Tahoma, Geneva, sans-serif;
	font-size:20px;
 	font-weight:normal;
	text-align:center;
	vertical-align:middle;
	/*display:table-cell;*/
}

body{
	margin:0 auto;
	width:100%;
	height:100%;
}

.header{
	width:100%;
	float:left;
	/*height:70px;*/
}

img.logo{
	float:left;
	height:18px;
	width:65px;
	margin-left:20px;
	margin-top:10px;
	vertical-align:middle;
}

.content{
	float:left;
	width:100%;
	height:auto;
	margin-top:3px;
}

.info, .info1{
	width:100%;
	height:auto;
}

.info p{
	font-family:Tahoma, Geneva, sans-serif;
	font-size:12px;
	padding:0px;
	margin:0 0 0 10px;
}

.info1 p{
	font-family:Tahoma, Geneva, sans-serif;
	font-size:13px;
	padding:0px;
	margin:0 0 0 10px;
	font-weight:normal;
}

.info .col1{
	width:60%;
	float:left;
}

.info .col2{
	width:40%;
	float:left;
}

.data, .data1{
	margin:3px 0 0 0;
	width:94%;
	margin-left: 2%;
/* 	border: 0.25px dotted black; */
	font-family:Tahoma, Geneva, sans-serif;
	font-size:13px;
	border-collapse:collapse;
}

.data .title{
	text-align:center;
	vertical-align:middle;
	border: 0.25px dotted black;
	font-weight:normal;
}

.left{
	font-weight:normal;
	text-align:left;
	vertical-align:middle;
	border: 0.25px dotted black;
}

.right{
	font-weight:normal;
	text-align:right;
	vertical-align:middle;
	border: 0.25px dotted black;
}

.data1{
	border:none;
}

.center{
	text-align:center;
	vertical-align:middle;
	border:none;
 	font-weight:normal;
}

.title1{
	font-weight:normal;
	text-align:center;
	vertical-align:middle;
}

@media print {
	.break_page{
		page-break-after: always;
		page-break-inside: inherit;
	}
}

.Clear {
    clear: both;
    padding: 0 !important;
}
</style>
<s:set id="iRun" value="0"></s:set>
<s:iterator value="listBeans">
	<div style="width: 100%; height: 420px;" id="<s:property value="saleOrder.id"/>">
		<div class="header">
			<div>
	        	<h1>PHIẾU GIAO NHẬN VÀ THANH TOÁN</h1>
	        </div>
			<div>
	        	<h3>NPP: <s:property value="saleOrder.shop.shopName"/></h3>
	        	<h3>SĐT: <span><s:property value="saleOrder.shop.phone"/></span></h3>
	            <h3>Ngày in: <span><s:property value="printDate"/></span></h3>
	        </div>
		</div>
		
		<div class="content">
	    	<div class="info">
	            <div class="col1">
	            	<h3>Số P.GH: <span style="font-weight:bold;"><s:property value="saleOrder.orderNumber"/></span></h3>
	            	<h3>Khách hàng: <span style="font-weight:bold;"><s:property value="saleOrder.customer.shortCode"/> - <s:property value="saleOrder.customer.customerName"/></span></h3>
	                <h3>SĐT: <span style="font-weight:bold;"><s:property value="saleOrder.customer.mobiphone"/></span></h3>
	            </div>
	            <div class="col2">
	                <h3>Ngày P.GH: <span style="font-weight:bold;"><s:property value="invoiceDate"/></span></h3>
<%-- 	                <s:if test="saleOrder.delivery.staffName.length() + saleOrder.staff.staffCode.length() > 30"> --%>
<%-- 	                	<h3>NVGH: <span><s:property value="(saleOrder.delivery.staffName + ' - ' + saleOrder.staff.staffCode).substring(0,30)"/></span></h3> --%>
<%-- 	                </s:if> --%>
<%-- 	                <s:else> --%>
<%-- 	                	<h3>NVGH: <span><s:property value="saleOrder.delivery.staffName"/> - <s:property value="saleOrder.delivery.staffCode"/></span></h3> --%>
<%-- 	                </s:else> --%>
	                <h3>NVGH: <span><s:property value="infoDeliveryStaff"/></span></h3>
	                <h3>NVBH: <span><s:property value="infoStaff"/></span></h3>
	            </div>
	        </div>
	        <div class="Clear"></div>
	        <s:set id="customerFullAddress" value="getCustomerFullAddress(saleOrder.customer)"></s:set>
	        <h3>Địa chỉ giao hàng: <span style="font-weight:bold;"><s:property value="customerFullAddress"/></span></h3>
	        <div class="info1" style="margin-top:8px;">
	        	<p style="font-weight:bold;">1. Hàng bán</p>
	        </div>
	        <div class="table" style="clear:both; padding-top:3px;">
	       	  <table class="data">
	              <tr style="font-weight: normal; border: 0.25px dotted black">
	                <th class="title" style="font-weight:bold;" scope="col" rowspan="2">Mã hàng</th>
	                <th class="title" style="font-weight:bold;" scope="col" rowspan="2">Tên hàng</th>
	                <th class="title" style="font-weight:bold;" scope="col" rowspan="2">&nbsp;QC&nbsp;</th>
	                <th class="title" style="font-weight:bold;" scope="col" colspan="2">Số lượng</th>
	                <th class="title" style="font-weight:bold;" scope="col" rowspan="2">Giá thùng</th>
	                <th class="title" style="font-weight:bold;" scope="col" rowspan="2">Giá lẻ</th>
	                <th class="title" style="font-weight:bold;" scope="col" rowspan="2">Thành tiền</th>
	              </tr>
	              <tr style="font-weight: normal; border: 0.25px dotted black">
	              	<th class="title" style="font-weight:bold;" scope="col">Thùng</th>
	                <th class="title" style="font-weight:bold;" scope="col">&nbsp;Lẻ&nbsp;</th>
	              </tr>
	              <s:iterator value="listSale">
		              <tr style="border: 0.25px dotted black">
		                <td class="center" style="border: 0.25px dotted black"><s:property value="product.productCode"/></td>
		                <td class="left" style="border: 0.25px dotted black"><s:property value="product.productName"/></td>
		                <td class="center" style="border: 0.25px dotted black"><s:property value="product.convfact"/></td>
		                
		                <s:if test="(quantity / product.convfact) == 0">
		                	<td class="center" style="border: 0.25px dotted black">-</td>
		                </s:if>
		                <s:else>
		                	<td class="center" style="border: 0.25px dotted black;"><s:property value="quantity / product.convfact"/></td>
		                </s:else>

						<s:if test="(quantity % product.convfact) == 0">
		                	<td class="center" style="border: 0.25px dotted black">-</td>
		                </s:if>
		                <s:else>
		                	<td class="center" style="border: 0.25px dotted black;"><s:property value="quantity % product.convfact"/></td>
		                </s:else>
		                
		                <td class="right" style="border: 0.25px dotted black"><s:property value="formatNumber(packagePrice)"/></td>
		                
		                <td class="right" style="border: 0.25px dotted black"><s:property value="convertMoney(priceValue)"/></td>
		                
		                <s:if test="(quantity * priceValue) == 0">
		                	<td class="right" style="border: 0.25px dotted black">-</td>
		                </s:if>
		                <s:else>
		                	<td class="right" style="border: 0.25px dotted black"><s:property value="convertMoney(quantity * priceValue)"/></td>
		                </s:else>
		              </tr>
	              </s:iterator>
					<tr style="border:none; border-collapse: collapse;">
		                <td class="left" style="border:none;"></td>
		                <td class="center" style="border:none; font-size: 13px;"></td>
		                <td class="right" style="border:none;"></td>
		                <td class="center" style="border:none; font-size: 13px;"></td>
		                <td class="center" style="border:none; font-size: 13px;"></td>
		                <td class="right" style="border:none;"></td>
		                <td class="right" style="border:none;">Tổng tiền</td>
		                <td class="right" style="border:none; font-weight:bold;"><s:property value="notDiscountAmount"/></td>
		           </tr>
		           <tr style="border:none; border-collapse: collapse;">
		                <td class="left" style="border:none;"></td>
		                <td class="center" style="border:none; font-size: 13px;"></td>
		                <td class="right" style="border:none;"></td>
		                <td class="center" style="border:none; font-size: 13px;"></td>
		                <td class="center" style="border:none; font-size: 13px;"></td>
		                <td class="right" style="border:none;"></td>
		                <td class="right" style="border:none;">Chiết khấu</td>
		                <td class="right" style="border:none; font-weight:bold;"><s:property value="discountAmount"/></td>
		           </tr>
		           <tr style="border:none; border-collapse: collapse;">
		                <td class="left" style="border:none;"></td>
		                <td class="center" style="border:none; font-size: 13px;"></td>
		                <td class="right" style="border:none;"></td>
		                <td class="center" style="border:none; font-size: 13px;"></td>
		                <td class="center" style="border:none; font-size: 13px;"></td>
		                <td class="right" style="border:none;"></td>
		                <td class="right" style="border:none; font-weight:bold;">Tổng tiền thanh toán</td>
		                <td class="right" style="border:none; font-weight:bold;"><s:property value="totalMoney"/></td>
		           </tr>
	            </table>
	        </div>
	        <div class="info" style="margin-top: 3px;">
	        	<p style="margin-left: 25px;">Số tiền bằng chữ: <span style="font-weight:bold; font-size: 14px;"><s:property value="moneyString"/></span></p>
	        </div>
	        
	        <s:if test="listFree.size() > 0">
		        <div class="info1" style="margin-top: 8px;">
		        	<p style="font-weight:bold;">2. Hàng khuyến mãi/trả thưởng CT.HTTM</p>
		        </div>
		        <div class="table1" style="margin-top: 3px;">
		       	  <table class="data" style="border: 0.25px dotted black">
		              <tr style="border: 0.25px dotted black">
		                <th class="title" style="font-weight:bold;" scope="col" rowspan="2">Mã hàng</th>
		                <th class="title" style="font-weight:bold;" scope="col" rowspan="2">Tên hàng</th>
		                <th class="title" style="font-weight:bold;" scope="col" rowspan="2">&nbsp;&nbsp;QC&nbsp;&nbsp;</th>
		                <th class="title" style="font-weight:bold;" scope="col" colspan="2">Số lượng</th>
		                <th class="title" style="font-weight:bold;" scope="col" rowspan="2">Chương trình KM</th>
		              </tr>
		              <tr style="border: 0.25px dotted black">
		                <th class="title" style="font-weight:bold;" scope="col">Thùng</th>
		                <th class="title" style="font-weight:bold;" scope="col">&nbsp;&nbsp;Lẻ&nbsp;&nbsp;</th>
		              </tr>
		              <s:iterator value="listFree">
			              <tr style="border: 0.25px dotted black">
			                <td class="center" style="border: 0.25px dotted black"><s:property value="product.productCode"/></td>
			                <td class="left" style="border: 0.25px dotted black"><s:property value="product.productName"/></td>
			                
			                <td class="center" style="border: 0.25px dotted black"><s:property value="product.convfact"/></td>
			                <td class="center" style="border: 0.25px dotted black;"><s:property value="quantity / product.convfact"/></td>
			                <td class="center" style="border: 0.25px dotted black;"><s:property value="quantity % product.convfact"/></td>
			                <td class="left" style="font-size: 11px"><s:property value="programCode"/></td>
			                
			              </tr>
		              </s:iterator>
		            </table>
		        </div>
	        </s:if>
	        <div class="data2" style="margin-top: 10px; margin-bottom: 10px;">
	        	<table class="data1">
	              <tr>
	                <td class="center" scope="col">Kế toán</td>
	                <th class="center" scope="col">NV giao hàng</th>
	                <th class="center" scope="col">KH ký nhận</th>
	                <th class="center" scope="col">Ghi chú</th>
	              </tr>
	            </table>
	        </div>
		</div>
		<s:set id="iRun" value="#iRun + 1"></s:set>
	</div>
	<div style="position: relative; top: 0;">
		<p style="font-size: 11px; float: right; position: relative; right: 30; margin:0px; padding:0px; display:inline;">Trang <s:property value="#iRun"/>/<s:property value="listBeans.size()"/></p>
	</div>
	<p class="break_page" style="margin:0px; padding:0px; display:inline; font-size: 1px;">&nbsp;</p>
</s:iterator>

<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
	<colgroup>
		<col style="width: 44px;" />
		<col style="width: 40px;" />
		<col style="width: 130px;" />
		<col style="width: 100px;" />
		<col style="width: 180px;" />
		<col style="width: 100px;" />
		<col style="width: 100px;" />
		<col style="width: 100px;" />
		<col style="width: 100px;" />
		<col style="width: 100px;" />
		<col style="width: 100px;" />
		<col style="width: 100px;" />
	</colgroup>
	<tbody>
		<s:if test="listSaleOrder != null && listSaleOrder.size > 0">
			<s:iterator value="listSaleOrder" status="stt">
				<tr>
					<td class="ColsTd1"><s:property value="#stt.index+1"/></td>
					<td class="ColsTd2"><input type="checkbox" name="approveStatus" onchange="return SPPrintReceiveOrder.checkThis(this.checked);" class="ApproveStatus" value="<s:property value="id"/>" /></td>
					<td class="ColsTd3 AlignLeft"><s:property value="orderNumber"/></td>
					<td class="ColsTd4 AlignLeft"><s:property value="customer.shortCode"/> - <s:property value="customer.customerName"/></td>
					<td class="ColsTd5 AlignLeft"><s:property value="customer.address"/></td>
					<td class="ColsTd6 AlignLeft"><s:property value="staff.staffCode"/></td>
					<td class="ColsTd7 AlignLeft"><s:property value="delivery.staffCode"/></td>
					<td class="ColsTd8 AlignLeft"><s:property value="car.carNumber"/></td>
					<s:if test="approved.value == 1">
						<td class="ColsTd9">Đã duyệt</td>
					</s:if>
					<s:if test="SaleOrder.timePrint!=null">
						<td class="ColsTd10">Đã in</td>
					</s:if>
					<s:else>
						<td class="ColsTd10">Chưa in</td>
					</s:else>
					
					<td class="ColsTd11"><s:property value="displayDate(orderDate)"/></td>
					<td class="ColsTd12 ColsTdEnd AlignRight"><s:property value="convertMoney(amount)"/></td>
				</tr>		
			</s:iterator>
				<tr><td colspan="11"></td></tr>
		</s:if>
		<s:else>
			<tr><td colspan="11">Không tìm thấy kết quả nào thỏa điều kiện tìm kiếm trên</td></tr>
		</s:else>
	</tbody>
</table>


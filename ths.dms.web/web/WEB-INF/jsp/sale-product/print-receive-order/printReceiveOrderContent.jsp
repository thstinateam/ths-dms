<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>

<style type="text/css">
#orTypeDiv.BoxSelect .ui-dropdownchecklist-selector {
    width: 174px !important;
}
</style>

<input type="hidden" id="function_code" value="<s:property value="systimestamp"/>" />
<s:set id="downloadToken" value="%{generateReportToken()}"></s:set>
<input type="hidden" id="report_token" value="<s:property value="#downloadToken"/>" />
<s:set id="downloadToken" value="%{putSessionValue(systimestamp, #downloadToken)}"></s:set>

<div class="BreadcrumbSection">
	<ul class="ResetList FixFloat BreadcrumbList">
		<li class="Sprite1"><a href="/sale-product/print-receive-order/info" class="cmsdefault">Bán hàng</a>
		</li><li><span>In phiếu giao hàng</span></li>
	</ul>
</div>

<div class="CtnOneColSection">
	<div class="ContentSection">
		<div class="ToolBarSection">
			<div class="SearchSection GeneralSSection">
				<h2 class="Title2Style">Thông tin tìm kiếm</h2>
				<div class="SearchInSection SProduct1Form">
					<label class="LabelStyle Label2Style">Mã NPP<span class="ReqiureStyle"> *</span></label>
                	<div class="BoxSelect BoxSelect2">
                		<input  type="text" class="InputTextStyle InputText1Style" id="cbxShop" maxlength="50" />
                	</div>
                	<label class="LabelStyle Label1Style">Ngày làm việc</label>
                	<p id="strLockDate" class="LabelStyle"><s:property value="lockDate"/></p>
                	<div class="Clear"></div>
				
				
					<label for="saleStaffCode" class="LabelStyle Label2Style">Mã NVBH</label> 
					<!-- <div class="BoxSelect BoxSelect2" id="divStaffCode"> -->
					<div class="BoxSelect BoxSelect2">
						<select class="easyui-combobox" id="saleStaffCode" style="width:175px">
<!-- 							<option value=""></option> -->
<%-- 							<s:iterator value="lstNVBHVo"  var="obj" >								 --%>
<%-- 								<option value="<s:property value="#obj.staffCode" />"><s:property value="#obj.staffName" /></option> --%>
<%-- 							</s:iterator> --%>
						</select>	
					</div>
					<label for="deliveryStaffCode" class="LabelStyle Label1Style">Mã NVGH</label> 
					<div class="BoxSelect BoxSelect2"> 
						<select class="easyui-combobox" id="deliveryStaffCode" style="width:175px">
<!-- 							<option value=""></option> -->
<%-- 							<s:iterator value="lstNVGHVo" var="obj" >								 --%>
<%-- 								<option value="<s:property value="#obj.staffCode" />"><s:property value="#obj.staffName" /></option> --%>
<%-- 							</s:iterator>	 --%>
						</select>
					</div>
					<label class="LabelStyle Label2Style">Mã KH(F9)</label>
                    <input type="text" class="InputTextStyle InputText1Style" id="customerCode" maxlength="50" />
					<input type="hidden" id="shopCode" value="<s:property value="shop.shopCode"/>">
					<div class="Clear"></div>
					
					<label for="createDateFrom" class="LabelStyle Label2Style">Từ ngày <span class="ReqiureStyle">*</span></label> 
					<input type="text" class="InputTextStyle InputText6Style" id="createDateFrom" value="<s:property value="createDateFrom"/>" maxlength="10" /> 
					<label for="createDateTo" class="LabelStyle Label1Style">Đến ngày <span class="ReqiureStyle">*</span></label> 
					<input type="text" class="InputTextStyle InputText6Style" id="createDateTo" value="<s:property value="createDateTo"/>" maxlength="10"/>
					<div class="Clear"></div>
					
					<label class="LabelStyle Label2Style">Lượt in</label>
					<input type="text" class="InputTextStyle InputText1Style" id="printBatch"  maxlength="8" />			
					
					<label  class="LabelStyle Label1Style">Ngày In</label> 
					<input type="text" class="InputTextStyle InputText6Style" id="printDate"  maxlength="10"/>					
					<label class="LabelStyle Label2Style">Trạng thái In</label>
					<div class="BoxSelect BoxSelect2">
	                    <select class="MySelectBoxClass" id="isNotPrint" >
	                         <option value="-2">Tất cả</option>
	                         <option value="0">Đã in</option>
	                         <option value="1" selected="selected">Chưa in</option>
	                    </select>
                    </div>
                    <%-- <label class="LabelStyle Label2Style">Tạo trên</label>
					<div class="BoxSelect BoxSelect2">
	                    <select class="MySelectBoxClass" id="placeCreaterOn" >
	                         <option value="0" selected="selected">Tất cả</option>
	                         <option value="1">Web</option>
	                         <option value="2">Tablet</option>
	                    </select>
                    </div> --%>
                    
                    <div class="Clear"></div>
<!--                     <label class="LabelStyle Label2Style">Trạng thái IN VAT</label> -->
<!-- 					<div class="BoxSelect BoxSelect2"> -->
<%-- 	                    <select class="MySelectBoxClass" id="printVATStatus" > --%>
<!-- 	                         <option value="-1" selected="selected">Tất cả</option> -->
<!-- 	                         <option value="0">Chưa có hóa đơn VAT</option> -->
<!-- 	                         <option value="1">Có hóa đơn VAT - Đã in</option> -->
<!-- 	                         <option value="2">Có hóa đơn VAT - Chưa in</option> -->
<%-- 	                    </select> --%>
<!--                     </div> -->
                    <label class="LabelStyle Label2Style">Loại ĐH</label>
                    <div class="BoxSelect BoxSelect2" id="orTypeDiv">
                    	<select class="MySelectBoxClass1" id="orderType" multiple="multiple">
                    		<option value="">Tất cả</option>
                    		<option value="IN" selected="selected">Đơn bán PreSale (IN)</option>
                    		<option value="SO">Đơn bán Vansale (SO)</option>
                    	</select>
                    </div>

                    <label class="LabelStyle Label1Style">Giá trị ĐH</label>
					<div class="BoxSelect BoxSelect2">
	                    <select class="MySelectBoxClass" id="isValueOrder" >
	                         <option value="-1" selected="selected">Tất cả</option>
	                         <option value="1">Có giá trị &gt;=</option>
	                         <option value="0">Có giá trị &lt; </option>
	                    </select>
                    </div>
		    
		    		<label class="LabelStyle LabelStyle"></label>
	     			<input type="text" class="InputTextStyle InputText4Style vinput-money" disabled="disabled" id="numberValueOrder" type="text" maxlength="15"/>
	     			
				    <%-- <label class="LabelStyle Label1Style">Trạng thái IN VAT</label>
					<div class="BoxSelect BoxSelect2">		
	                    <select class="MySelectBoxClass" id="printVATStatus" >
	                         <option value="-1" selected="selected">Tất cả</option>
	                         <option value="0">Chưa có hóa đơn VAT</option>
	                         <option value="1">Có hóa đơn VAT - Đã in</option>
	                         <option value="2">Có hóa đơn VAT - Chưa in</option>
	                    </select>
                    </div> --%>
                    
	     			<%-- <label class="LabelStyle Label1Style">Độ ưu tiên</label>
                         <div class="BoxSelect BoxSelect2">
                             <select class="MySelectBoxClass" id="priority">
                                 <option value="" selected="selected">Tất cả</option>
								<option value="A">Cần giao ngay</option>
								<option value="B">Giao trong ngày</option>
								<option value="C">Giao ngoài ngày</option>
                             </select>
                         </div> --%>
                         
                         
                    <%-- <label class="LabelStyle Label2Style">Loại ĐH</label>
                    <div class="BoxSelect BoxSelect2" id="orTypeDiv">
                    	<select class="MySelectBoxClass1" id="orderType" multiple="multiple">
                    		<option value="">Tất cả</option>
                    		<option value="IN" selected="selected">Đơn bán PreSale (IN)</option>
                    		<option value="SO">Đơn bán Vansale (SO)</option>
                    	</select>
                    </div> --%>
                    
                    <div class="Clear"></div>
					<div class="BtnCenterSection">
						<button id="btnSearch" class="BtnGeneralStyle cmsdefault" onclick="return SPPrintReceiveOrder.search();">Tìm kiếm</button>
						<img src="/resources/images/loading.gif" class="LoadingStyle" id="searchPOButton" style="display: none;" />
					</div>
					<div id="errorSearchMsg" class="ErrorMsgStyle"></div>
					<div class="Clear"></div>
				</div>
				<h2 class="Title2Style">Danh sách hóa đơn</h2>
				<div class="SearchInSection SProduct1Form">
					<div class="GridSection" id="gridContainer">
						<table title="Kết quả tìm kiếm" id="gridPrintReceiveOrder"></table>
					</div>
					<div class="Clear"></div>
					<div class="BtnCenterSection">
						<button id="btnPR_InGop" class="BtnGeneralStyle BtnMSection" onclick="return SPPrintReceiveOrder.showDialogPrint();">In đơn gộp</button>
						<button id="btnPR_InLeNVGH" class="BtnGeneralStyle" onclick="return SPPrintReceiveOrder.viewOddOrder(1);">In đơn lẻ (NVGH)</button>
						<button id="btnPR_InLeNVBH" class="BtnGeneralStyle" onclick="return SPPrintReceiveOrder.viewOddOrder(2);">In đơn lẻ (NVBH)</button>
						<!-- <button class="BtnGeneralStyle" onclick="SPPrintReceiveOrder.viewApplet();">View applet</button> -->
					</div>
					<div class="Clear"></div>
					<p id="errMsg" class="ErrorMsgStyle" style="display: none">Có lỗi xảy ra khi cập nhật dữ liệu</p>
					<tiles:insertTemplate template="/WEB-INF/jsp/general/successMsg.jsp" />
				</div>
			</div>
			<div class="Clear"></div>
		</div>
	</div>
	<div class="Clear"></div>
</div>
<div id ="showDialogPrintContainer" style="visibility:hidden;" >	
	<div id="showDialogPrintDialog" class="easyui-dialog" title="Chọn kiểu gộp đơn hàng" data-options="closed:true,modal:true" style="width:670px;height:140px;">
		<div class="PopupContentMid">
			<div class="GeneralForm Search1Form">	
				<div class="BoxSelect BoxSelect2">
					<select class="MySelectBoxClass" id="typePrint">
						<option value="0">Gộp theo khách hàng</option>
						<option value="1">Gộp theo NVGH</option>
						<option value="2">Gộp theo đơn hàng chọn</option>
					</select>
				</div>		
<!-- 			<button id="dlPrintEx" onclick="return SPPrintReceiveOrder.printWithType();"  class="BtnGeneralStyle" style="width: 80px;margin-left: 20px; margin-top: -3px;">In</button> -->
				<button id="dlPrintEx" onclick="return SPPrintReceiveOrder.viewWithType();"  class="BtnGeneralStyle cmsdefault" style="width: 80px;margin-left: 20px; margin-top: -3px;">In</button>				
				<div class="Clear"></div>
				<p id="errMsgDlgPrint" class="ErrorMsgStyle" style="display: none; padding: inherit"></p>			
			</div>
		</div>
	</div>
</div>

<div id="viewOddDialogContainer" style="display: none;">
	<div id="viewOddContainer">
		<div id="frameContainer" style="padding-left: 20px;"></div>
<%-- 		<button class="BtnGeneralStyle BtnGeneralCStyle BtnGeneralMStyle Sprite2" style="width: 50px; float: right; margin-right: 18px; margin-top: 3px;" onclick="window.frames[0].print();"><span class="Sprite2">In</span></button> --%>
<!-- 		onclick="return SPPrintReceiveOrder.printOddOrder();" -->
<!-- 		onclick="return SPPrintReceiveOrder.printOddOrderPdf();" -->
		<button class="BtnGeneralStyle BtnGeneralCStyle BtnGeneralMStyle Sprite2 cmsdefault" style="width: 50px; float: right; margin-right: 18px; margin-top: 3px;" onclick="return SPPrintReceiveOrder.print();"><span class="Sprite2">In</span></button>
		<button id="btnOddExcel" class="BtnGeneralStyle cmsdefault" style="width: 80px;margin-left: 10px; float: right; margin-top: 3px;">Xuất Excel</button> 
		<button id="btnOddPdf" page-size="A5" class="BtnGeneralStyle cmsdefault" style="width: 80px;margin-left:10px; float: right; margin-top: 3px;">Xuất Pdf A5</button>
		<button id="btnOddPdfA4" page-size="A4" class="BtnGeneralStyle cmsdefault" style="width: 80px;margin-left:10px; float: right; margin-top: 3px;">Xuất Pdf A4</button>
	</div>
</div>

<div id="viewGopDialogContainer" style="display: none;">
	<div id="viewGopContainer">
		<div id="frameContainer1" style="padding-left: 20px;"></div>
		<button class="BtnGeneralStyle BtnGeneralCStyle BtnGeneralMStyle Sprite2" style="width: 50px; float: right; margin-right: 18px; margin-top: 3px;" onclick="return SPPrintReceiveOrder.updatePrintWithType();"><span class="Sprite2">In</span></button>
				<button class="BtnGeneralStyle cmsdefault" style="width: 80px;margin-left: 10px; float: right; margin-top: 3px;"; onclick="return SPPrintReceiveOrder.printWithType();">Xuất File</button>
		
	</div>
</div>

<%-- <div id="phieuVCNoiBoContainer" style="display:none;">
	<div id="phieuVCNoiBoPopup">
		<div class="PopupContentMid">
			<div class="GeneralForm Search1Form" >
				<label class="LabelStyle Label1Style" style="width: 130px;">Tên lệnh điều động <span class="ReqiureStyle">*</span></label>
				<input id="nameTrans" maxlength="50" type="text" style="width:250px;" class="InputTextStyle InputText1Style" />
				<label class="LabelStyle Label1Style" style="width: 100px;">Ngày điều động <span class="ReqiureStyle">*</span></label> 
				<input id="dateTrans" type="text" maxlength="10" style="width:75px;"  class="InputTextStyle InputText1Style" />
				<div class="Clear"></div>
                <label class="LabelStyle Label1Style" style="width: 130px;">Nội dung <span class="ReqiureStyle">*</span></label>
                <textarea id="contentTrans" style="width:465px;height:70px;font-size:1em;"  maxlength="250" class="InputTextStyle InputText1Style">Vận chuyển hàng đi bán lưu động</textarea>
                <div class="Clear"></div>
                <div class="BtnCenterSection">
					<button class="BtnGeneralStyle BtnGeneralMStyle" onclick="SPPrintReceiveOrder.printTransport();">Xuất phiếu</button>
					<button class="BtnGeneralStyle BtnGeneralMStyle" onclick="$('#phieuVCNoiBoPopup').dialog('close');">Đóng</button>
				</div>
				<div class="Clear"></div>
				<p id="errMsgVCNB" style="display: none;" class="ErrorMsgStyle"></p>
			</div>
		</div>
	</div>
</div> --%>

<input type="hidden" id="shopCode" value="<s:property value='shopCode' />" />

<script type="text/javascript">
function changeIsNotPrint(id){
	if($('#' + id).val()==1){
		$('#printBatch').val('');
		disabled('printBatch');
		$('#printDate').val('');
		disabled('printDate');
		removeDateTimePicker('printDate');
		$('#printDate').removeClass('InputText6Style');
		$('#printDate').addClass('InputText1Style');
	}else{
		enable('printBatch');
		enable('printDate');
		VTUtilJS.applyDateTimePicker('printDate');
		$('#printDate').removeClass('InputText1Style');
		$('#printDate').addClass('InputText6Style');
	}
};
$(document).ready(function(){	
	VTUtilJS.applyDateTimePicker('createDateFrom');
	VTUtilJS.applyDateTimePicker('createDateTo');
	VTUtilJS.applyDateTimePicker('printDate');	
	//$('.MySelectBoxClass').customStyle();
	
	//trungtm6
	SPPrintReceiveOrder.initInfo();
	
	/* $('#saleStaffCode').focus();
	Utils.bindComboboxStaffEasyUI('deliveryStaffCode');
	Utils.bindComboboxStaffEasyUI('saleStaffCode'); */
	
	changeIsNotPrint('isNotPrint');
	$('#isNotPrint').change(function(){
		changeIsNotPrint('isNotPrint');
	});
	$('#customerCode').bind('keyup', function(event) {					
		if (event.keyCode == keyCode_F9) {
			VCommonJS.showDialogSearch2({
			    inputs : [
			        {id:'code', maxlength:50, label:'Mã KH'},
			        {id:'name', maxlength:250, label:'Tên KH'},
			        {id:'address', maxlength:250, label:'Địa chỉ', width:410}
			    ],
// 			    url : '/commons/customer-in-shop/search?shopCode='+$("#shopCode").val().trim(),
				url : '/commons/customer-in-shop/search?shopCode='+$('#cbxShop').combobox('getValue'),
			    onShow : function() {
		        	$('.easyui-dialog #code').focus();
			    },
			    columns : [[
			        {field:'shortCode', title:'Mã KH', align:'left', width: 110, sortable:false, resizable:false, formatter: function(v, r, i){
			        	if (v) {
			        		return Utils.XSSEncode(v);
			        	}
			        	return "";
			        }},
			        {field:'customerName', title:'Tên KH', align:'left', width: 150, sortable:false, resizable:false, formatter: function(v, r, i){
			        	if (v) {
			        		return Utils.XSSEncode(v);
			        	}
			        	return "";
			        }},
			        {field:'address', title:'Địa chỉ', align:'left', width: 170, sortable:false, resizable:false, formatter: function(v, r, i){
			        	if (v) {
			        		return Utils.XSSEncode(v);
			        	}
			        	return "";
			        }},
			        {field:'choose', title:'', align:'center', width:40, sortable:false, resizable:false, formatter:function(value,row,index) {
			        	return "<a href='javascript:void(0)' onclick='$(\"#customerCode\").val(\""+ Utils.XSSEncode(row.shortCode) + "\");$(\".easyui-dialog\").dialog(\"close\");'>Chọn</a>";        
			        }}
			    ]]
			});
		}
	});		
	Utils.bindFormatOnTextfield('printBatch',Utils._TF_NUMBER);

	$('#orderType').dropdownchecklist({
		emptyText:'Tất cả',
		firstItemChecksAll: true,
		textFormatFunction: function(options) {
			if (options[0].selected) {
				return "Tất cả";
			}
			var s = null;
			for (var i = 1, sz = options.length; i < sz; i++) {
				if (options[i].selected) {
					if (s == null) {
						s = options[i].value;
					} else {
						s = s + "," + options[i].value;
					}
				}
			}
			if (s == null) {
				return "Tất cả";
			}
			return s;
		}
	});

	Utils.bindAutoSearchUpdateBoxSelect();
	
	//bo sung text box gia tri DH
	var numberValueOrder = '<s:property value="orderValue"/>';
	if(numberValueOrder != '' && numberValueOrder != "") {
		SPAdjustmentTax.numberValueOrder = numberValueOrder;
		SPAdjustmentTax._textChangeBySeach.avg = numberValueOrder.replace(/,/g, '');
	}
	Utils.bindFormatOnTextfield('numberValueOrder',Utils._TF_NUMBER); // bind khong cho nhap ky tu dac biet
	//xu ly bind value money dau phay
	Utils.formatCurrencyFor('numberValueOrder');
	$('#isValueOrder').bind('change', function(event){
		var value = $(this).val();
		if($('#numberValueOrder').val().trim() != ''){
			SPAdjustmentTax._textChangeBySeach.avg = $('#numberValueOrder').val().replace(/,/g, '');
		}
		if (Number(value) ==  -1) {
			$('#numberValueOrder').val(SPAdjustmentTax._textChangeBySeach.all);
			disabled('numberValueOrder');
		} else {
			enable('numberValueOrder');
			$('#numberValueOrder').val(formatCurrency(SPAdjustmentTax._textChangeBySeach.avg));
		}
	});
});
</script>

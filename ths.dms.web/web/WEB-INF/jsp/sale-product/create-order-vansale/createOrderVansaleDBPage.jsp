<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles" %>
<tiles:insertTemplate template='/WEB-INF/jsp/sale-product/common.jsp'/>
<div class="BreadcrumbSection">
    <ul class="ResetList FixFloat BreadcrumbList">
        <li class="Sprite1"><a href="#">Bán hàng</a></li>
        <li><span>Lập đơn bán hàng vansale(DP)</span></li>
    </ul>
</div>
<div class="CtnOneColSection">
    <div class="ContentSection">
    	<div class="ToolBarSection">
            <div class="SearchSection GeneralSSection">
            	<h2 class="Title2Style">Thông tin chung
            		<img style="display: none;" src="/resources/scripts/plugins/jquery-easyui-1.3.2/themes/default/images/loading.gif" class="loadingTitle" >
            	</h2>
                <div class="SearchInSection SProduct1Form">
                	<label class="LabelStyle Label2Style">Mã phiếu</label>
                    <input type="text" id="orderNumber" class="InputTextStyle InputText1Style" disabled="disabled" value="<s:property value="code"/>"/>
                    <label class="LabelStyle Label1Style">Ngày xuất</label>
                    <input type="text" id="exportDate" name="exportDate"  class="InputTextStyle InputText1Style"/>
                    <div class="Clear"></div>
                    <label class="LabelStyle Label2Style">Mã NV<span class="ReqiureStyle">*</span></label>
                    <div class="BoxSelect BoxSelect2" id="boxSelectNVBH">
                        <select id="staffCode" class="easyui-combobox">
                        	<option value="" ></option>
                        	<s:iterator value="listStaff"  var="obj" >
                        		<option value="<s:property value="#obj.staffCode" />" ><s:property value="#obj.staffName" /></option>
                        	</s:iterator>
                        </select>
                    </div>
                    <label class="LabelStyle Label1Style">Chọn xe</label>
                    <div class="BoxSelect BoxSelect2">
                    	<select class="easyui-combobox" id="carId">
                            <option value=""></option>
							<s:iterator value="listCars"  var="obj" >
								<option value="<s:property value="#obj.id" />"><s:property value="#obj.carNumber" /></option>															
							</s:iterator>
                        </select>
                    </div>
                    <label class="LabelStyle Label1Style">Người lập</label>
                    <input type="text" id="createUser" value="<s:property value="userName"/>" class="InputTextStyle InputText1Style" disabled="disabled"/>
                    <div class="Clear"></div> 
                    <div style="height: 30px;"> 
						<p style="height: 25px; display: none;" class="ErrorMsgStyle" id="GeneralInforErrors">Có lỗi xảy ra khi cập nhật dữ liệu</p>
					</div>
                    <div class="Clear"></div>
                </div>
                <h2 class="Title2Style">Danh sách sản phẩm bán</h2>
                <div class="SearchInSection SProduct1Form" id="spSaleProductInfor">                	
                	<div class="GeneralMilkBox">
						<div class="GeneralMilkTopBox">
							<div class="GeneralMilkBtmBox">
								<div class="GeneralMilkInBox">
									<div class="GeneralTable">
										<div class="GridSection" id="gridSaleContainer" style="padding: 0px; overflow: scroll;">
											<table id="saleProductVanGrid"></table>
											<div class="Clear"></div>
		                    				<p class="TotalInfo">
			                                	<span class="Tem1Style">Tổng trọng lượng: <span style="color: #000000" id="totalWeightSale">0</span><span style="color: #000000">(Kg)</span></span>
			                                    <span class="Tem1Style">Tổng tiền : <span style="color: #000000" id="totalAmountSale">0</span><span style="color: #000000">(VNĐ)</span></span>
		                                	</p>											
										</div>
									</div>
									<div class="ButtonSection" style="text-align: right;">										
										<button style="margin-right: 10px;" class="BtnGeneralStyle BtnGeneralMStyle Sprite2" id="print" onclick="">
											<span class="Sprite2">In phiếu xuất kho kiêm vận chuyển nội bộ</span>
										</button>
										<button style="margin-right: 10px;" class="BtnGeneralStyle BtnGeneralMStyle Sprite2" id="save" onclick="">
											<span class="Sprite2">Lưu</span>
										</button>
										<div style="height: 15px;">
											<p id="alertSuccess_successMsg" class="SuccessMsgStyle" style="display: none;padding: 0;" >Tính tiền thành công cho đơn hàng !</p>
											<p style="display: none;" class="ErrorMsgStyle" id="saleInputErr"></p>
										</div>										
									</div>				
								</div>
							</div>
						</div>
					</div>                	
                </div>
            </div>
            <div class="Clear"></div>
        </div>
    </div>
    <div class="Clear"></div>
</div>
<div id="responseDiv" style="display:none;"></div>
<input type="hidden" id="fsxn" value="${fsxn}" />
<input type="hidden" id="shopId" value="<s:property value="shop.id" />" />
<input type="hidden" id="currentLockDay" value="<s:property value="lockDate" />" />
<input type="hidden" id="isShow" value="<s:property value="isShow"/>"/>
<script type="text/javascript">
	$(document).ready(function() {
		$('#gridSaleContainer').css('width', $(window).width() - 40);
		$('.MySelectBoxClass').customStyle();
		Utils.bindComboboxStaffEasyUI('staffCode');
		Utils.bindComboboxCarEasyUI('carId');
		applyDateTimePicker('#exportDate');
		var lockDate = '<s:property value="lockDate"/>';
		$('#exportDate').val(lockDate);
		$('#exportDate').datepicker('disable');
		disabled('print');
		disabled('save');
		CreateSaleOrderVansaleDB.initSaleProductGrid();
		if($('#isShow').val() != 0){
			//$('.CtnOneColSection').css("display", "none");
			$.messager.alert('Cảnh báo','Không thể thực hiện xuất kho nhân viên do ngày chốt khác ngày hiện tại!');
			setTimeout(function(){disableCombo('staffCode');disableCombo('carId');}, 2000);
		}
		$('#staffCode').combobox({
			onChange: function(newValue, oldValue){
				if(newValue != oldValue){
					var params = new Object();
					params.saleStaffCode = newValue;
					var data = $('#saleProductVanGrid').handsontable('getData');
					if(data != null && data.length >0 && data[0].productCode != null){
						$.messager.confirm('Xác nhận','Danh sách sản phẩm sẽ bị xóa khi chọn lại NVBH. <br /> <b>Bạn có muốn chọn nhân viên mới?</b>',function(r){  
						    if (r){ 					    	
						    	CreateSaleOrderVansaleDB.getProductStaffSale(params)
						    	CreateSaleOrderVansaleDB._currentSaleStaff = newValue;
						    }else{
						    	$('#staffCode').combobox('setValue',CreateSaleOrderVansaleDB._currentSaleStaff);
						    }
						});
					}else{
						CreateSaleOrderVansaleDB.getProductStaffSale(params);
					}
				}
				
			}
		});
	});
	
</script>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>

<div class="BreadcrumbSection">
	<ul class="ResetList FixFloat BreadcrumbList">
		<li class="Sprite1"><a href="javascript:void(0)">Bán hàng</a></li>
		<li><span>Cập nhật hóa đơn thuế GTGT</span></li>
	</ul>
</div>
<div class="CtnOneColSection">
	<div class="ContentSection">
		<div class="ToolBarSection">
			<div class="SearchSection GeneralSSection">
			<div class="TabSection">
					<ul class="ResetList TabSectionList">
						<li><a href="javascript:void(0);" onclick="SPAdjustmentTax.showTab1()" id="infoTab" class="Sprite1 Active"><span class="Sprite1">Hóa đơn VAT lẻ</span> </a></li>
						<li><a href="javascript:void(0);" onclick="SPAdjustmentTax.showTab2()" id="propertyTab" class="Sprite1"><span class="Sprite1">Hóa đơn VAT gộp</span> </a></li>
					</ul>
					<div class="Clear"></div>
			</div>
			<input id="shopCode" type="hidden" value="<s:property value="shop.shopCode"/>"/>
			<input id="shopId" type="hidden" value="<s:property value="shop.id"/>"/>
			<div id="infoTabContainer">
				<h2 class="Title2Style">Thông tin tìm kiếm</h2>
				<div class="SearchInSection SProduct1Form">
					<label class="LabelStyle Label2Style">Mã NPP<span class="ReqiureStyle">*</span></label>
                	<div class="BoxSelect BoxSelect2">
                		<input  type="text" class="InputTextStyle InputText1Style" id="cbxShop" maxlength="50" />
                	</div>
                	<label class="LabelStyle Label7Style">Ngày làm việc:</label>
                	<p id="strLockDate" class="LabelStyle"><s:property value="lockDate"/></p>
                	<div class="Clear"></div>
					<label class="LabelStyle Label2Style">Trạng thái</label>
					<div class="BoxSelect BoxSelect2">
						<s:select list="listStatus" listKey="value" id="chooseStatus" tabindex="1" cssClass="MySelectBoxClass" 
							listValue="name" value="0" onchange="return SPAdjustmentTax.onSelectStatusChange();">
						</s:select>
					</div>
					<label class="LabelStyle Label1Style">NVGH</label>
					<div class="BoxSelect BoxSelect2">
                        <select class="easyui-combobox" id="deliveryStaffCode">
                        </select>
                    </div>
					<label	class="LabelStyle Label6Style">Hình thức thanh toán</label>
					<div class="BoxSelect BoxSelect2">
						<s:select list="listPayment" listKey="value" id="choosePaymentMethod" disabled="true" tabindex="9" headerValue="Chọn" headerKey="-1"
							cssClass="MySelectBoxClass" listValue="name" value="-1">
						</s:select>
					</div>
					<div class="Clear"></div>
					<label class="LabelStyle Label2Style">Thuế suất</label> 
					<input	type="text" class="InputTextStyle InputText1Style" tabindex="3" maxlength="10" id="taxValue"/> 
					
					<label class="LabelStyle Label1Style">Số đơn hàng</label> 
					<input type="text" class="InputTextStyle InputText1Style" tabindex="4" maxlength="50" id="invoiceNumber"/>
					
					<label class="LabelStyle Label6Style">Số hóa đơn đỏ</label> 
					<input type="text" class="InputTextStyle InputText1Style" tabindex="4" maxlength="50" id="redInvoiceNumber"/>
					
					
					<div class="Clear"></div>
					<label class="LabelStyle Label2Style">Từ ngày <span class="ReqiureStyle">*</span></label> 
					<input	type="text" class="InputTextStyle InputText6Style vinput-date " tabindex="5" maxlength="10" id="createFromDate" value="<s:property value="lockDate"/>"/> 
					
					<label class="LabelStyle Label1Style">Đến ngày <span class="ReqiureStyle">*</span></label> 
					<input	type="text" class="InputTextStyle InputText6Style vinput-date " tabindex="6" maxlength="10" value="<s:property value="lockDate"/>" id="createToDate" />

					<div class="Clear"></div>
					<label class="LabelStyle Label2Style">Mã NVBH</label> 
					<div class="BoxSelect BoxSelect2">
                        <select class="easyui-combobox" id="saleStaffCode">
                        </select>
                    </div>
					<label class="LabelStyle Label1Style">Mã KH (F9)</label>					 
					<input type="text" class="InputTextStyle InputText1Style" tabindex="8" maxlength="50" id="customerCode" />
					
					<label class="LabelStyle Label2Style" style="margin-left: 20px;">Giá trị ĐH</label>
					<div class="BoxSelect BoxSelect2">
	                    <select class="MySelectBoxClass" id="isValueOrder" >
	                         <option value="-1">Tất cả</option>
	                         <option value="1" selected="selected">Có giá trị >=</option>
	                         <option value="0">Có giá trị < </option>
	                    </select>
                    </div>		
                    <label class="LabelStyle LabelStyle"></label>
     				<input type="text" class="InputTextStyle InputText4Style" id="numberValueOrder" type="text" value='<s:property value="numberValue"/>' maxlength="50"/>                 	
					<div class="Clear"></div>
					<div class="BtnCenterSection">
						<button class="BtnGeneralStyle" tabindex="10" id="btnSearch" onclick="return SPAdjustmentTax.searchVATle();">Tìm kiếm</button>
					</div>

					<div class="Clear"></div>
				</div>
				<p class="ErrorMsgStyle SpriteErr" id="errMsg" style="display: none;">Có lỗi xảy ra khi cập nhật dữ liệu</p>
				<h2 class="Title2Style">Kết quả tìm kiếm</h2>
				<div class="SearchInSection SProduct1Form">
					<div class="GridSection">
						<div class="ResultSection" id="areaGrid">
							<p id="gridNoResult" style="display: none"
								class="WarningResultStyle">Không có kết quả</p>
								<table id="grid" style="overflow: scroll; height: 330px;"></table>
							<div id="pager"></div>
						</div>							
					</div>
					<div class="Clear"></div>
					<div class="BtnRSection">
						<input type="checkbox" class="InputCbxStyle InputCbx4Style" id="chkTypePay"/> 
						<label class="LabelStyle Label9Style" id="chkTypePay1">Chọn loại thanh toán</label>
						<div class="BoxSelect BoxSelect2" id="chkTypePay2">
							<s:select list="listPayment" listKey="value" id="sigleSelectTypePay" cssClass="MySelectBoxClass" listValue="name"></s:select>
						</div>
						<button id="save" class="BtnGeneralStyle BtnMSection" >Lưu</button>
					</div>
					<div class="Clear"></div>
					<tiles:insertTemplate template="/WEB-INF/jsp/general/successMsg.jsp" />	
				</div>
			</div>
			<!--tab vat gop  -->
			<div id="propertyTabContainer" style="display: none">
			<h2 class="Title2Style">Thông tin tìm kiếm</h2>
				<div class="SearchInSection SProduct1Form">
					<label class="LabelStyle Label2Style">Mã NPP<span class="ReqiureStyle">*</span></label>
                	<div class="BoxSelect BoxSelect2">
                		<input  type="text" class="InputTextStyle InputText1Style" id="cbxShop1" maxlength="50" />
                	</div>
                	<label class="LabelStyle Label7Style">Ngày làm việc:</label>
                	<p id="strLockDate" class="LabelStyle"><s:property value="lockDate"/></p>
                	<div class="Clear"></div>
					<label class="LabelStyle Label2Style">Trạng thái</label>
					<div class="BoxSelect BoxSelect2">
						<s:select list="listStatus1" listKey="value" id="chooseStatus1" tabindex="1" cssClass="MySelectBoxClass" 
							listValue="name" value="0" onchange="return SPAdjustmentTax.onSelectStatusChange1();">
						</s:select>
					</div>
					<label class="LabelStyle Label1Style">NVGH</label>
					<div class="BoxSelect BoxSelect2">
                        <select class="easyui-combobox" id="deliveryStaffCode1">
                        </select>
                    </div>
					<label	class="LabelStyle Label6Style">Hình thức thanh toán</label>
					<div class="BoxSelect BoxSelect2">
						<s:select list="listPayment" listKey="value" id="choosePaymentMethod1" disabled="true" tabindex="9" headerValue="Chọn" headerKey="-1"
							cssClass="MySelectBoxClass" listValue="name" value="-1">
						</s:select>
					</div>
					<div class="Clear"></div>
					<label class="LabelStyle Label2Style">Thuế suất</label> 
					<input	type="text" class="InputTextStyle InputText1Style" tabindex="3" maxlength="10" id="taxValue1"/> 
					
					<label class="LabelStyle Label1Style">Số đơn hàng</label> 
					<input type="text" class="InputTextStyle InputText1Style" tabindex="4" maxlength="50" id="invoiceNumber1"/>
					
					<label class="LabelStyle Label1Style">Số hóa đơn đỏ</label> 
					<input type="text" class="InputTextStyle InputText1Style" tabindex="4" maxlength="50" id="redInvoiceNumber1"/>
					
					
					<div class="Clear"></div>
					<label class="LabelStyle Label2Style">Từ ngày <span class="ReqiureStyle">*</span></label> 
					<input	type="text" class="InputTextStyle InputText6Style vinput-date " tabindex="5" maxlength="10" id="createFromDate1" value="<s:property value="lockDate"/>"/> 
					
					<label class="LabelStyle Label1Style">Đến ngày <span class="ReqiureStyle">*</span></label> 
					<input	type="text" class="InputTextStyle InputText6Style vinput-date " tabindex="6" maxlength="10" value="<s:property value="lockDate"/>" id="createToDate1" />

					<div class="Clear"></div>
					<label class="LabelStyle Label2Style">Mã NVBH</label> 
					<div class="BoxSelect BoxSelect2">
                        <select class="easyui-combobox" id="saleStaffCode1">
                        </select>
                    </div>
					<label class="LabelStyle Label1Style">Mã KH (F9)</label>					 
					<input type="text" class="InputTextStyle InputText1Style" tabindex="8" maxlength="50" id="customerCode1" />
					
					<label class="LabelStyle Label2Style" style="margin-left: 20px;">Giá trị ĐH</label>
					<div class="BoxSelect BoxSelect2">
	                    <select class="MySelectBoxClass" id="isValueOrder1" >
	                         <option value="-1">Tất cả</option>
	                         <option value="1" selected="selected">Có giá trị >=</option>
	                         <option value="0">Có giá trị < </option>
	                    </select>
                    </div>	
                   	 <label class="LabelStyle LabelStyle"></label>
     				 <input type="text" class="InputTextStyle InputText4Style" id="numberValueOrder1" type="text" value='<s:property value="numberValue"/>' maxlength="50"/>                 
					<div class="Clear"></div>
					<div class="BtnCenterSection">
						<button class="BtnGeneralStyle BtnSearch" tabindex="10" id="btnSearch1">Tìm kiếm</button>
					</div>

					<div class="Clear"></div>
					<p class="ErrorMsgStyle SpriteErr" id="errMsg1" style="display: none;">Có lỗi xảy ra khi cập nhật dữ liệu</p>
				</div>
		<!--   Danh cho don gop co hoa don hay ko co hoa don -->
		<div id="divDonTong">
				<h2 class="Title2Style">Kết quả tìm kiếm</h2>
				<div class="SearchInSection SProduct1Form">
					<div class="GridSection">
						<div class="ResultSection" id="areaGrid1">
							<p id="gridNoResult" style="display: none" class="WarningResultStyle">Không có kết quả</p>
							<table id="grid1" style="overflow: scroll; height: 300px;"></table>
						</div>							
					</div>
					<div class="Clear"></div>
				</div>
	
				<h2 class="Title2Style">Đơn tổng</h2>
				<div class="SearchInSection SProduct1Form">
					<div class="GridSection">
						<div class="ResultSection" id="areaGrid2">
							<p id="gridNoResult" style="display: none" class="WarningResultStyle">Không có kết quả</p>
							<table id="grid2" style="overflow: scroll; height: 200px;"></table>
						</div>							
					</div>
					<div class="BtnCenterSection">
						<button id="btnCompound" class="BtnGeneralStyle BtnMSection" onclick="return SPAdjustmentTax.gopHoaDon();">Tạo hóa đơn</button>
					</div>
					<p class="ErrorMsgStyle SpriteErr" id="errMsg3" style="display: none;">Có lỗi xảy ra khi cập nhật dữ liệu</p>
					<div class="Clear"></div>
				</div>
				</div>
		<!--end   Danh cho don gop co hoa don hay ko co hoa don-->
				<h2 class="Title2Style">Kết quả tìm kiếm</h2>
				<div class="SearchInSection SProduct1Form">
					<div class="GridSection">
						<div class="ResultSection" id="areaGrid">
							<p id="gridNoResult" style="display: none"
								class="WarningResultStyle">Không có kết quả</p>
								<table id="grid3" style="overflow: scroll; height: 330px;"></table>
							<div id="pager"></div>
						</div>							
					</div>
					<div class="Clear"></div>
					<div class="BtnRSection">
						<input type="checkbox" class="InputCbxStyle InputCbx4Style" id="chkTypePayEX"/> 
						<label class="LabelStyle Label9Style" id="chkTypePayEX1">Chọn loại thanh toán</label>
						<div class="BoxSelect BoxSelect2" id="chkTypePayEX2">
							<s:select list="listPayment" listKey="value" id="sigleSelectTypePay1" cssClass="MySelectBoxClass" listValue="name"></s:select>
						</div>
						<button id="save1" class="BtnGeneralStyle BtnMSection" >Lưu</button>
<!-- 						<button id="cancel" class="BtnGeneralStyle" >Hủy</button> -->
					</div>
					<div class="Clear"></div>
					<div id="divGop"><tiles:insertTemplate template="/WEB-INF/jsp/general/successMsg.jsp" />	</div>
					<p class="ErrorMsgStyle SpriteErr" id="errMsg2" style="display: none;">Có lỗi xảy ra khi cập nhật dữ liệu</p>
				</div>
				</div>
				<!-- tab vat gop -->
			<div class="Clear"></div>
		</div>
		</div>
	</div>
	<div class="Clear"></div>
</div>
<script type="text/javascript">

$(document).ready( function(){
	<s:iterator value="listPayment">
		var obj = new Object();
		obj.value = '<s:property value="value"/>';
		obj.name = '<s:property value="name"/>';
		SPAdjustmentTax.listPayment.push(obj);		
	</s:iterator>
	$('#deliveryStaffCode').focus();	
	$('#chooseStatus').val(0);
	$('#chooseStatus').change();
	SPAdjustmentTax._textChangeBySeach.all = '';
	SPAdjustmentTax._textChangeBySeach.avg = '<s:property value="numberValue" />';
	SPAdjustmentTax.initInfo();
	Utils.bindComboboxStaffEasyUI('saleStaffCode');
	Utils.bindComboboxStaffEasyUI('deliveryStaffCode');	
	
	Utils.bindComboboxStaffEasyUI('deliveryStaffCode1');
	Utils.bindComboboxStaffEasyUI('saleStaffCode1');	
	var numberValueOrder1 = '<s:property value="numberValue"/>';
	if(numberValueOrder1 != '' && numberValueOrder1 != "") {
		SPAdjustmentTax.numberValueOrder1 = numberValueOrder1;
		// xu ly dau phay
		$('#numberValueOrder1').val(formatCurrency(numberValueOrder1));
	}
	var params = SPAdjustmentTax.getParams();		
	//bo sung text box gia tri DH
	var numberValueOrder = '<s:property value="numberValue"/>';
	if(numberValueOrder != '' && numberValueOrder != "") {
		SPAdjustmentTax.numberValueOrder = numberValueOrder;
		// xu ly dau phay
		$('#numberValueOrder').val(formatCurrency(numberValueOrder));
	}
		//xu ly bind value money dau phay
	Utils.formatCurrencyFor('numberValueOrder');
	$('#isValueOrder').bind('change', function(event){
		var value = $(this).val();
		if (Number(value) ==  -1) {
			$('#numberValueOrder').val(SPAdjustmentTax._textChangeBySeach.all);
			disabled('numberValueOrder');
		} else {
			enable('numberValueOrder');
			$('#numberValueOrder').val(formatCurrency(SPAdjustmentTax._textChangeBySeach.avg));
		}
	});	
	$('#isValueOrder1').bind('change', function(event){
		var value = $(this).val();
		if (Number(value) ==  -1) {
			$('#numberValueOrder1').val(formatCurrency(SPAdjustmentTax._textChangeBySeach.all));
			disabled('numberValueOrder1');
		} else {
			enable('numberValueOrder1');
			$('#numberValueOrder1').val(formatCurrency(SPAdjustmentTax._textChangeBySeach.avg));
		}
	});	
	//load datagrid
	//$('#divOverlay').show();
	function boldRenderer(instance, td, row, col, prop, value, cellProperties) {
	  Handsontable.renderers.TextRenderer.apply(this, arguments);
	  td.style.fontWeight = 'bold';
	
	}
	Utils.formatCurrencyFor('numberValueOrder1');
	Utils.formatCurrencyFor('numberValueOrder');
	function boldRedRenderer(instance, td, row, col, prop, value, cellProperties) {
	  Handsontable.renderers.TextRenderer.apply(this, arguments);
	  td.style.color = 'OrangeRed';
	}
	
	function brownRenderer(instance, td, row, col, prop, value, cellProperties) {
	  Handsontable.renderers.TextRenderer.apply(this, arguments);
	  //td.style.fontWeight = 'bold';
	  td.style.color = 'black';
	}
	
	var maxLengthValidate = function (value, callback) {
		if(value.length == 0 || value == null || value == ''){
			callback(true);
		}
	    if (value.length > 50) {
	      callback(false);
	    }else if(!isNullOrEmpty(value) && !/^[0-9a-zA-Z-_.]+$/.test(value)){//(!/^[^<|>|?|\\|\'|\"|&|~#|$|%|@|*|(|)|^|\[|\]|\{|\}|ư|ơ|Ơ|Ư|ô|Ô|ă|Ă|â|Â|`]+$/.test(value)){
	    	$('#errMsg').html('Số hóa đơn không được có ký tự đặc biệt. Vui lòng kiểm tra lại.').show();
	    	SPAdjustmentTax.tmpInvoice = value;
	    	callback(false);
		} else {
			SPAdjustmentTax.tmpInvoice = value;
			$('.ErrorMsgStyle').html('').hide();
	      	callback(true);
	    }
	};
	var $container = $("#grid");
	$container.handsontable({
	  rowHeaders: true,
	  colHeaders: true,
	  manualColumnResize: true,
	  colWidths: [130, 150, 120, 90, 90, 180, 180, 250, 100, 100, 80, 40],
	  colHeaders: ['Hóa đơn nội bộ', 'Hóa đơn đỏ', 'Thanh toán', 'Tổng tiền', 'Tiền thuế', 'Khách hàng', 'Tên đơn vị', 'Địa chỉ', 'NVBH', 'NVGH', 'Ngày', 'Thuế suất'],
	  minSpareRows: 1,
	  width: $(window).width() - 45,
	  columns: [
	    {data: "orderNumber", readOnly: true, renderer:boldRenderer},
	    {data: "invoiceNumber", renderer:boldRedRenderer, validator: maxLengthValidate, allowInvalid: false},
	    {data: "payment", type: 'dropdown', source: ["Tiền mặt", "Chuyển khoản"]},
	    {data: "total", readOnly: true, renderer:boldRenderer},
	    {data: "taxTotal", readOnly: true, renderer:boldRenderer},
	    {data: "customer", readOnly: true},
	    {data: "cpnyName", readOnly: true},
	    {data: "customerAddr", readOnly: true},
	    {data: "staffInfo", readOnly: true},
	    {data: "deliveryStaffInfo", readOnly: true},
	    {data: "date", readOnly: true},
	    {data: "stringVat", readOnly: true}
	  ],
	  afterChange: function(changes, source) {
		  if(changes != null){
			  if(changes[0][1] == 'invoiceNumber'){
				  $.each(changes, function (index, element) {
			            var change = element;
			            var rowIndex = change[0];
			            var oldValue = change[2];
			            var newValue = change[3];
			            var cellChange = {
			                'rowIndex': rowIndex,
			                'columnIndex': 1
			            };
			            if(oldValue != newValue){
			            	SPAdjustmentTax.cellChanges.push(cellChange);
			            	var td = $("#grid").handsontable('getCell', rowIndex, 1 );
			            	$(td).css('color','Maroon');
			            }
			       });
			  }
		  }
	  },
	  afterRender: function () {
		  $.each(SPAdjustmentTax.cellChanges, function (index, element) {
              var cellChange = element;
              var rowIndex = cellChange['rowIndex'];
              var columnIndex = cellChange['columnIndex'];
              var td = $("#grid").handsontable('getCell', rowIndex, columnIndex );
              $(td).css('color','Maroon');
          });
	  }
	});
	var handsontable = $container.data('handsontable');
	var params = SPAdjustmentTax.getParams();
	var kData = $.param(params, true);
   	$('#save').unbind('click');
  		$('#save').bind('click', function(e) {
		SPAdjustmentTax.save();
	});
	$('#customerCode').bind('keyup',function(event) {
		if ($('#cbxShop').combobox('getValue') != null&& $('#cbxShop').combobox('getValue') != ''&& $('#cbxShop').combobox('getValue') != undefined) {
			if (event.keyCode == keyCode_F9) {
				VCommonJS.showDialogSearch2({
                    inputs : [
             	 		{id:'code', maxlength:50, label:'Mã KH'},
               	 		{id:'name', maxlength:250, label:'Tên KH'},
                 		{id:'address', maxlength:250, label:'Địa chỉ', width:410},
                          ],
                    params : {shopCode: $('#cbxShop').combobox('getValue')},
                    url : '/commons/customer-in-shop/search',
                    columns : [[
                           {field:'shortCode', title:'Mã KH', align:'left', width: 50, sortable:false, resizable:false, formatter:function(value,row,index) {
                        	   return VTUtilJS.XSSEncode(value);        
                           }},
                           {field:'customerName', title:'Tên khách hàng', align:'left', width: 120, sortable:false, resizable:false, formatter:function(value,row,index) {
                        	   return VTUtilJS.XSSEncode(value);        
                           }},
                           {field:'address', title:'Địa chỉ', align:'left', width: 200, sortable:false, resizable:false, formatter:function(value,row,index) {
                        	   return VTUtilJS.XSSEncode(value);        
                           }},
                           {field:'choose', title:'', align:'center', width:40, sortable:false, resizable:false, formatter:function(value,row,index) {
                        	   return "<a href='javascript:void(0)' onclick='chooseCustomer(0,\""+ Utils.XSSEncode(row.shortCode) + "\");'>Chọn</a>";        
                           }}
                    ]]
              });
			}
		}
	});	
	$('#customerCode1').bind('keyup',function(event) {
		if ($('#cbxShop1').combobox('getValue') != null&& $('#cbxShop1').combobox('getValue') != ''&& $('#cbxShop1').combobox('getValue') != undefined) {
			if (event.keyCode == keyCode_F9) {
				VCommonJS.showDialogSearch2({
                    inputs : [
             	 		{id:'code', maxlength:50, label:'Mã KH'},
               	 		{id:'name', maxlength:250, label:'Tên KH'},
                 		{id:'address', maxlength:250, label:'Địa chỉ', width:410},
                          ],
                    params : {shopCode: $('#cbxShop').combobox('getValue')},
                    url : '/commons/customer-in-shop/search',
                    columns : [[
                           {field:'shortCode', title:'Mã KH', align:'left', width: 50, sortable:false, resizable:false, formatter:function(value,row,index) {
                        	   return VTUtilJS.XSSEncode(value);        
                           }},
                           {field:'customerName', title:'Tên khách hàng', align:'left', width: 120, sortable:false, resizable:false, formatter:function(value,row,index) {
                        	   return VTUtilJS.XSSEncode(value);        
                           }},
                           {field:'address', title:'Địa chỉ', align:'left', width: 200, sortable:false, resizable:false, formatter:function(value,row,index) {
                        	   return VTUtilJS.XSSEncode(value);        
                           }},
                           {field:'choose', title:'', align:'center', width:40, sortable:false, resizable:false, formatter:function(value,row,index) {
                        	   return "<a href='javascript:void(0)' onclick='chooseCustomer(1,\""+ Utils.XSSEncode(row.shortCode) + "\");'>Chọn</a>";        
                           }}
                    ]]
              });
			}
		}
	});	
	
});
function chooseCustomer(type,code) {
	if(type == 1){
		$('#customerCode1').val(code);
	}else{
		$('#customerCode').val(code);
	}
	$('.easyui-dialog').dialog('close');
}
</script>
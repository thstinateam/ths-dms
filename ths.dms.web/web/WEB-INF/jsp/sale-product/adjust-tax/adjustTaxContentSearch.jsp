<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
	<colgroup>
        <col style="width:44px;" />
        <col style="width:40px;" />
        <col style="width:130px;" />
        <col style="width:100px;" />
        <col style="width:150px;" />
        <col style="width:250px;" />
        <col style="width:250px;" /> 
        <col style="width:100px;" />
        <col style="width:100px;" />
        <col style="width:110px;" />
        <col style="width:100px;" />
        <col style="width:100px;" />
    </colgroup>
	<tbody>
		<s:if test="chooseStatus == 0">
			<s:if test="listSaleOrderVOEx != null && listSaleOrderVOEx.size() > 0">
			<s:iterator value="listSaleOrderVOEx" status="stt">
				<tr>
					<td class="ColsTd1"><s:property value="#stt.index + 1" />
					</td>
					<td class="ColsTd2">
						<input name="approveStatus" type="checkbox" id="approveStatus-<s:property value="rowId"/>" 
										onchange="return SPAdjustmentTax.checkThisBox(this.checked, '<s:property value="rowId"/>');"
										class="ApproveStatus <s:property value="saleOrder.id"/>" value="<s:property value="rowId"/>" />
					</td>
					<td class="ColsTd3 AlignLeft"><s:property value="saleOrder.orderNumber" /></td>
					<td class="ColsTd4">
							<input type="text" maxlength="20" onchange="return SPAdjustmentTax.onInvoiceNumberInput('<s:property value="rowId"/>');" id="vat-<s:property value="rowId"/>"
										class="InputTextStyle InputText1Style VAT AlignLeft" value="<s:property value="invoice.invoiceNumber"/>"/>
									<input type="hidden" id="hidden-<s:property value="rowId"/>" value="<s:property value="invoice.invoiceNumber"/>">
					</td>
					<td class="ColsTd5">
						<s:if test="invoice == null">
							<div class="BoxSelect BoxSelect7">
								<select id="pay-<s:property value="rowId"/>" class="BoxSelect BoxSelect7">
									<s:iterator value="listPayment">
										<option value="<s:property value="value"/>"><s:property value="name"/></option>
									</s:iterator>
								</select>					
							</div>
						</s:if>
						<s:else>
							<div class="BoxSelect BoxDisSelect BoxSelect7">
								<select disabled="disabled" id="pay-<s:property value="rowId"/>" class="BoxSelect  BoxSelect7">
									<s:iterator value="listPayment" var="pm">
										<s:if test="custPayment.value==#pm.value">
											<option value="<s:property value="#pm.value"/>" selected="selected"><s:property value="#pm.name"/></option>
										</s:if>
										<s:else>
											<option value="<s:property value="#pm.value"/>"><s:property value="#pm.name"/></option>
										</s:else>
									</s:iterator>
								</select>
							</div>	
						</s:else>
							
					</td>
					<td class="ColsTd6 AlignLeft"><s:property value="saleOrder.customer.shortCode" /> - <s:property value="saleOrder.customer.customerName" /></td>
					<td class="ColsTd7 AlignLeft"><div class="Wordwrap"><s:property value="saleOrder.customer.address" /></div></td>
					<td class="ColsTd8 AlignLeft"><s:property value="saleOrder.staff.staffCode" /></td>
					<td class="ColsTd9 AlignLeft"><s:property value="saleOrder.delivery.staffCode" /></td>
					<td class="ColsTd10"><s:property value="displayDate(saleOrder.orderDate)" /></td>
					<td class="ColsTd11 AlignRight"><s:property value="convertMoney(amount)" /></td>
					<td class="ColsTd12 ColsTdEnd" style="text-align: right; padding-right: 15px;">
						<s:if test="lstSaleOrderDetail != null && lstSaleOrderDetail.size() > 0"></s:if>
						<s:property value="displayNumber(lstSaleOrderDetail.get(0).getVat())" />
					</td>
				</tr>
			</s:iterator>
			</s:if>
			<s:else>
				<tr><td colspan="12" class="NotData">Không có dữ liệu nào</td>
				</tr>
			</s:else>
			<tr><td colspan="12"></td>
			</tr>
		</s:if>
		<s:else>
			<s:if test="listInvoice != null && listInvoice.size > 0">
				<s:iterator value="listInvoice"  status="stt">
					<tr>
						<td class="ColsTd1"><s:property value="#stt.index + 1" /></td>
						<td class="ColsTd2">
							<s:if test="invoiceDate != null">
								<input name="approveStatus" disabled="disabled" class="ApproveStatus" onchange="return SPAdjustmentTax.checkThisBox(this.checked);" type="checkbox" value='<s:property value="id"/>' />
							</s:if>
							<s:else>
								<input name="approveStatus" class="ApproveStatus" onchange="return SPAdjustmentTax.checkThisBox(this.checked);" type="checkbox" value='<s:property value="id"/>' />								
							</s:else>
						</td>
						<td class="ColsTd3 AlignLeft"><s:property value="orderNumber" /></td>
						<td class="ColsTd4">
							<s:if test="invoiceDate != null">
								<input type="text" disabled="disabled" maxlength="20" id="show-<s:property value="id"/>" class="InputTextStyle InputText1Style AlignLeft" value="<s:property value="invoiceNumber"/>"/>
								<input type="hidden" id="hidden-<s:property value="id"/>" value="<s:property value="invoiceNumber"/>">
							</s:if>
							<s:else>
								<input type="text" maxlength="20" id="show-<s:property value="id"/>" class="InputTextStyle InputText1Style AlignLeft" value="<s:property value="invoiceNumber"/>"/>
								<input type="hidden" id="hidden-<s:property value="id"/>" value="<s:property value="invoiceNumber"/>">							
							</s:else>
						</td>
						<td class="ColsTd5">
							<div class="BoxSelect BoxDisSelect BoxSelect7">
								<select disabled="disabled" class="BoxSelect  BoxSelect7">
									<s:iterator value="listPayment" var="pm">
										<s:if test="custPayment.value==#pm.value">
											<option value="<s:property value="#pm.value"/>" selected="selected"><s:property value="#pm.name"/></option>
										</s:if>
										<s:else>
											<option value="<s:property value="#pm.value"/>"><s:property value="#pm.name"/></option>
										</s:else>
									</s:iterator>
								</select>
							</div>
						</td>
						<td class="ColsTd6 AlignLeft"><s:property value="customerCode" /> - <s:property value="custName" /></td>
						<td class="ColsTd7 AlignLeft"><div class="Wordwrap"><s:property value="customerAddr" /></div></td>
						<td class="ColsTd8 AlignLeft"><s:property value="staff.staffCode" /></td>
						<td class="ColsTd9 AlignLeft"><s:property value="deliveryStaff.staffCode" /></td>
						<td class="ColsTd10"><s:property value="displayDate(orderDate)" /></td>
						<td class="ColsTd11 AlignRight"><s:property value="convertMoney(taxAmount)" /></td>
						<td class="ColsTd12 ColsTdEnd" style="text-align: right; padding-right: 15px;">
							<s:property value="displayNumber(vat)" />
						</td>
					</tr>
				</s:iterator>
			</s:if>
			<s:else>
				<tr><td colspan="12" class="NotData">Không có dữ liệu nào</td>
				</tr>
			</s:else>
			<tr><td colspan="12"></td>
			</tr>
		</s:else>
	</tbody>
</table>
<script type="text/javascript">
$(function(){
	
});
</script>
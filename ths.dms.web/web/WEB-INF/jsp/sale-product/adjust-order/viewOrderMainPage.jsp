<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles" %>
<style type="text/css">
.panel {z-index:100003 !important;}
</style>
<tiles:insertTemplate template='/WEB-INF/jsp/sale-product/common.jsp'/>

<!-- <div class="BreadcrumbSection">
    <ul class="ResetList FixFloat BreadcrumbList">
        <li class="Sprite1"><a href="javascript:void(0);">Bán hàng</a></li>
        <li><span id="title">Xem thông tin đơn hàng</span></li>
    </ul>
</div> -->
<!-- <div class="CtnOneColSection">
    <div class="ContentSection">
    	<div class="ToolBarSection"> 
            <div class="SearchSection GeneralSSection">-->
            	<h2 class="Title2Style">Thông tin chung</h2>
                <div class="SearchInSection SProduct1Form">
                	<label class="LabelStyle Label1Style">Số đơn hàng</label>
                    <p id="orderNumber" class="ValueStyle Value1Style"><s:property value="saleOrder.orderNumber" /></p>
                    <label class="LabelStyle Label1Style">Số tham chiếu</label>
                    <p id="refOrderNumber"  class="ValueStyle Value1Style"><s:property value="saleOrder.refOrderNumber" /></p>
                    <s:if test="saleOrder.orderType.value == 'CO' || saleOrder.orderType.value == 'CM'
                    		|| saleOrder.orderType.value == 'AI' || saleOrder.orderType.value == 'AD'">
	                    <label class="LabelStyle Label1Style">Số đơn gốc</label>
	                    <p id="fromOrderNumber"  class="ValueStyle Value1Style"><s:property value="fromSaleOrderNumber" /></p>
                    </s:if>
                    <div class="Clear"></div>
                    <label class="LabelStyle Label1Style">Mã KH</label>
                    <p id="customerCode" class="ValueStyle Value1Style"><a href="javascript:void(0)" id="customerCodeLink" onclick="return SPConfirmOrder.showCustomerInfor(<s:property value="saleOrder.customer.id" />)"><s:property value="saleOrder.customer.shortCode" /></a></p>
                    <label class="LabelStyle Label1Style">Tên KH</label>
                    <p id="customerName" class="ValueStyle Value1Style"><s:property value="saleOrder.customer.customerName" /></p>
                    <label class="LabelStyle Label1Style">Địa chỉ KH</label>
                    <p class="ValueStyle Value1Style" id="address"><s:property value="saleOrder.customer.address" /></p>
                    <div class="Clear"></div>
                    <label class="LabelStyle Label1Style">NVBH</label>
                    <p class="ValueStyle Value1Style" id="saleStaff"><s:property value="saleOrder.staff.staffCode" /></p>
                    <label class="LabelStyle Label1Style">NVGH</label>
                    <p class="ValueStyle Value1Style" id="deliveryStaff"><s:property value="saleOrder.delivery.staffCode" /></p>
                    <label class="LabelStyle Label1Style">Số ĐT</label>
                    <p class="ValueStyle Value1Style" id="phone">
                    	<s:property value="codeNameDisplayEx(saleOrder.customer.mobiphone,saleOrder.customer.phone)" /> 
                    </p>
                    
                    <div class="Clear"></div>
                    <label class="LabelStyle Label1Style">Ngày đơn hàng</label>
                    <p class="ValueStyle Value1Style" id="orderDate"><s:property value="displayDate(saleOrder.orderDate)" /></p>
                    <label class="LabelStyle Label1Style">Ngày giao</label>
                    <p id="deliveryDate" class="ValueStyle Value1Style"><s:property value="displayDate(saleOrder.deliveryDate)"/></p>
                    <label class="LabelStyle Label1Style">Xe</label>
                    <p id="deliveryDate" class="ValueStyle Value1Style"><s:property value="saleOrder.car.carNumber"/></p>
                    <div class="Clear"></div>
                    <s:if test="saleOrder.orderType.value == 'AI' || saleOrder.orderType.value == 'AD'">
	                    <label class="LabelStyle Label1Style">Lý do</label>
	                    <p id="description" class="ValueStyle Value1Style"><s:property value="saleOrder.description"/></p>
                    </s:if>
                    <div class="Clear"></div>
                </div>
                <h2 class="Title2Style">Sản phẩm bán
                	<img src="/resources/scripts/plugins/jquery-easyui-1.3.2/themes/default/images/loading.gif" id="loadingProduct" >
                </h2>
             	<div class="SearchInSection SProduct1Form" id="spSaleProductInfor" style="display: none;">                	
                	<div class="GeneralMilkBox">
						<div class="GeneralMilkTopBox">
							<div class="GeneralMilkBtmBox">
								<div class="GeneralMilkInBox">
									<div id="gridSaleData"></div>
									<div class="Clear"></div>
									<p style="display:none;" class="TotalInfo">
	                                	<span class="Tem1Style">Tổng trọng lượng: <span style="color: #FF0000" id="totalWeightSale">0</span><span style="color: #FF0000">(Kg)</span></span>
	                                	<span id = "sumPrice1">
	                                    	<span class="Tem1Style">Tổng tiền hàng : <span style="color: #FF0000" id="totalAmountSale">0</span><span style="color: #FF0000">(VNĐ)</span></span>
                                			<span class="Tem1Style">Chiết khấu: <span style="color: #FF0000" id="totalDiscountSale">0</span><span style="color: #FF0000">(VNĐ)</span></span>
                                		</span>
                                	</p>
								</div>
							</div>
						</div>
					</div>                	
                </div>
                <div style="margin-top:15px;"></div>
                 <h2 id="h2SPKM" class="Title2Style" style="display: none;">Sản phẩm khuyến mãi
                 	<img style="display: none;" src="/resources/scripts/plugins/jquery-easyui-1.3.2/themes/default/images/loading.gif" id="loadingPPromotion" >
                 </h2>
                 <div class="SearchInSection SProduct1Form" id="spPromotionProductInfor" style="display: none;">
                      <div class="GeneralMilkBox">
					  	<div class="GeneralMilkTopBox">
							<div class="GeneralMilkBtmBox">
								<div class="GeneralMilkInBox">
									<div class="GeneralTable">
										<div class="GridSection" style="padding: 0px;">
											<table id="gridPromotionProduct" class="easyui-datagrid" title="Danh sách sản phẩm khuyến mãi"></table>												
										</div>					
									</div>
								</div>
							</div>
						</div>
					</div>
                    <div class="Clear"></div>
                </div>
                <h2 id="h2MM" class="Title2Style" style="display: none;">Khuyến mãi mở mới</h2>
                <div class="SearchInSection SProduct1Form" style="display: none;">
                    <div class="GeneralMilkBox">
					  	<div class="GeneralMilkTopBox">
							<div class="GeneralMilkBtmBox">
								<div class="GeneralMilkInBox">
									<div class="GeneralTable">
										<div class="GridSection" id="promotionGridContainer2" style="padding: 0px;">
											<div id="promotionGrid2"></div>	
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
                    <div class="Clear"></div>
                </div>
                <h2 id="h2KMDH" class="Title2Style" style="display: none;">Khuyến mãi đơn hàng</h2>
               <div id="saleOrderPromotion" class="SearchInSection SProduct1Form" style="display: none;">
                	<div class="GeneralMilkBox">
						<div class="GeneralMilkTopBox">
							<div class="GeneralMilkBtmBox">
								<div class="GeneralMilkInBox">
									<div class="GeneralTable">
										<div class="GridSection" style="padding: 0px;">
											<table id="gridSaleOrderPromotion" class="easyui-datagrid" title="Danh sách sản phẩm khuyến mãi"></table>												
										</div>					
									</div>
								</div>
							</div>
						</div>
					</div>
                    <div class="Clear"></div>
                </div>
                <h2 id="h2KS" class="Title2Style" style="display: none;">Trả thưởng chương trình hỗ trợ thương mại vào đơn hàng</h2>
                <div id="keyShop" class="SearchInSection SProduct1Form" style="display: none;">
                	<div class="GeneralMilkBox">
						<div class="GeneralMilkTopBox">
							<div class="GeneralMilkBtmBox">
								<div class="GeneralMilkInBox">
									<div class="GeneralTable">
										<div class="GridSection" style="padding: 0px;">
											<table id="gridKeyShop"></table>												
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
                    <div class="Clear"></div>
                </div>
                <h2 id="h2KMTL" class="Title2Style" style="display: none;">Khuyến mãi tích lũy</h2>
                <div class="SearchInSection SProduct1Form" style="display: none;">
                    <div class="GeneralMilkBox">
					  	<div class="GeneralMilkTopBox">
							<div class="GeneralMilkBtmBox">
								<div class="GeneralMilkInBox">
									<div class="GeneralTable">
										<div class="GridSection" id="promotionGridContainer4" style="padding: 0px;">
											<div id="accumulativePromotionGrid"></div>	
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
                    <div class="Clear"></div>
                 </div>
                 <div class="SearchInSection SProduct1Form" style="padding-top:0;">
                    <p style="margin-top:0;" class="TotalInfo">
                    	<span class="Tem1Style">Tổng trọng lượng: <span style="color: #FF0000" id="totalWeight">0</span><span style="color: #FF0000">(Kg)</span></span>
                    	<span id = "sumPrice">
                        	<span class="Tem1Style">Tổng tiền: <span style="color: #FF0000" id="totalVAT">0</span><span style="color: #FF0000">(VNĐ)</span></span>
                    		<span class="Tem1Style">Chiết khấu: <span style="color: #FF0000" id="totalDiscount">0</span><span style="color: #FF0000">(VNĐ)</span></span>
                    		<span class="Tem1Style">Tổng tiền thanh toán: <span style="color: #FF0000" id="total">0</span><span style="color: #FF0000">(VNĐ)</span></span>
                    	</span>
                    </p>
                    <div class="BtnCenterSection">
						<!-- <button class="BtnGeneralStyle Sprite2" id="btnClose" onclick="return SPSearchSale.closeOrderView('<s:property value="lnk" />');">Đóng</button> -->
                        <button class="BtnGeneralStyle Sprite2" id="btnClose" onclick="$('#billDetailPopup').dialog('close');">Đóng</button>
                    </div>
                    <div class="Clear"></div>
                    <div style="height: 30px;"> 
						<p style="height: 25px; display: none;" class="ErrorMsgStyle" id="serverErrors">Có lỗi xảy ra khi cập nhật dữ liệu</p>
						<p id="successMsg" class="SuccessMsgStyle" style="display: none"></p>
					</div>
                </div>
            <!--</div>
            <div class="Clear"></div>
        </div>
    </div>
    <div class="Clear"></div>
</div> -->
<div id ="discountDetailDialogDiv" style="display: none" >
	<div id="discountDetailDialog" class="easyui-dialog" title="Danh sách chiết khấu theo chương trình" style="width:616px;height:auto;" data-options="closed:true,modal:true">
		<div class="PopupContentMid">
			<div class="GeneralForm Search1Form">
                <div class="GridSection" id="discountDetailDiv">
                    <table id="gridDiscountDetail"></table>
                </div>
                <div class="BtnCenterSection">
					<button class="BtnGeneralStyle BtnGeneralMStyle" onclick="$('#discountDetailDialog').dialog('close');"><span class="Sprite2"><s:text name="create_order_dong"/></span></button>
				</div>
            </div>
		</div>
	</div>
</div>
<input type="hidden" id="orderId" value="${orderId}" />
<input type="hidden" id="custCode" value="${saleOrder.customer.shortCode}" />
<input type="hidden" id="staffCode" value="${saleOrder.staff.staffCode}" />
<div id="salesCustomerDialog" style="display: none;">
	<div id="salesCustomerDialogUIDialog" class="easyui-dialog" title="Thông tin khách hàng" data-options="closed:true,modal:true">
</div>
</div>
<script type="text/javascript">
$(document).ready(function(){	
	if(SPCreateOrder._isAllowShowPrice){
		$('#sumPrice').show();
		$('#sumPrice1').show();
	} else {
		$('#sumPrice').hide();
		$('#sumPrice1').hide();
	}
	SPSearchSale.loadPageDetail('<s:property value="currentShopCode"/>');
    setTimeout(function(){
        if($("#gridSaleData").length>0){
            $('#gridSaleData').handsontable("updateSettings",{width:$("#billDetailPopup").width() - 27});                                                      
        }
    }, 4000);
});
</script>
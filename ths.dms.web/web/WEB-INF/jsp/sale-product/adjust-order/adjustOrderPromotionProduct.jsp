<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
	<%@ taglib prefix="s" uri="/struts-tags"%>
<div class="GeneralMilkBox">
	<div class="GeneralMilkTopBox">
		<div class="GeneralMilkBtmBox">
			<div class="GeneralMilkInBox">
				<div class="GeneralTable">
					<table id="gridPromotionProduct" class="easyui-datagrid" title="Danh sách sản phẩm khuyến mãi">
					</table>
					<div class="BoxGeneralTFooter">
							<table width="100%" border="0" cellspacing="0" cellpadding="0" id="footer_salePromotionProductDetails">
								<colgroup>
									<col style="width: 44px;" />
									<col style="width: 91px;" />
									<col style="width: 180px;" />								
									<col style="width: 83px;" />
									<col style="width: 83px;" />
									<col style="width: 120px;" />
									<col style="width: 100px;" />
									<col style="width: 100px;" />
									<col style="width: 100px" />
								</colgroup>
								<tfoot>
									<tr>
										<td class="ColsTd1">&nbsp;</td>
										<td class="ColsTd2">&nbsp;</td>
										<td class="ColsTd3">Tổng:</td>
										<td class="ColsTd4">&nbsp;</td>										
										<td class="ColsTd6" style="font-weight: normal;" id="grossWeightTotal"></td>
										<td class="ColsTd7 ColsTdEnd" colspan="4" style="font-weight: normal;text-align:left"  id="disPercentTotal"></td>
									</tr>
								</tfoot>
							</table>
						</div>
				</div>
			</div>
		</div>
	</div>
</div>
<div id="createOrderPromotionProgramDetail" style="display: none">
<div class="GeneralDialog General2Dialog">
        	<div class="DialogProductSelection">
            	<div class="GeneralInfoSection">
                	<label class="LabelStyle Label1Style">Mã chương trình</label>
                    <p class="ValueStyle Value1Style" style="font-weight: bold;" id="coPPDetailCode"></p>
                    <div class="Clear"></div>
                    <label class="LabelStyle Label1Style">Tên chương trình</label>
                    <p class="ValueStyle Value1Style" style="font-weight: bold;" id="coPPDetailName"></p>
                    <div class="Clear"></div>
                    <label class="LabelStyle Label1Style">Loại khuyến mại</label>
                    <p class="ValueStyle Value1Style" style="font-weight: bold;" id="coPPDetailType"></p>
                    <div class="Clear"></div>
                    <label class="LabelStyle Label1Style">Dạng chương trình</label>
                    <p class="ValueStyle Value1Style" style="font-weight: bold;" id="coPPDetialFormat"></p>
                    <div class="Clear"></div>
                    <label class="LabelStyle Label1Style">Từ ngày</label>
                    <p class="ValueStyle Value1Style" style="font-weight: bold;"id="coPPDetailFromDate"></p>
                    <div class="Clear"></div>
                    <label class="LabelStyle Label1Style">Đến ngày</label>
                    <p class="ValueStyle Value1Style" style="font-weight: bold;" id="coPPDetailToDate"></p>
                    <div class="Clear"></div>
                    <label class="LabelStyle Label1Style">Diễn giải chương trình</label>
                    <p class="ValueStyle Value1Style" style="font-weight: bold;" id="coPPDetailDescription"> </p>
                    <div class="Clear"></div>
                </div>
                <div class="BoxDialogBtm">                	
                    <div class="ButtonSection">
                        <button onclick="$.fancybox.close();" class="BtnGeneralStyle BtnGeneralCStyle BtnGeneralMStyle Sprite2"><span class="Sprite2">Đóng</span></button>
                    </div>
                </div>
        	</div>
        </div>
</div>
<script type="text/javascript">
	$(document).ready(function(){
		
		SPCreateOrder._lstPromotionProductIsAuto = new Map();
		
	});
</script>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles" %>
<style type="text/css">
.panel {z-index:100003 !important;}
</style>
<tiles:insertTemplate template='/WEB-INF/jsp/sale-product/common.jsp'/>

<div class="BreadcrumbSection">
    <ul class="ResetList FixFloat BreadcrumbList">
        <li class="Sprite1"><a href="javascript:void(0);">Bán hàng</a></li>
        <li><span id="title">Sửa đơn bán hàng</span></li>
    </ul>
</div>

<div class="CtnOneColSection">
    <div class="ContentSection">
    	<div class="ToolBarSection">
            <div class="SearchSection GeneralSSection">
            	<h2 class="Title2Style">Thông tin chung</h2>
                <div class="SearchInSection SProduct1Form" id="generalInfoDiv">
                	<label class="LabelStyle Label2Style">Đơn vị</label>
                    <div class="BoxSelect BoxSelect2">
                        <select class="easyui-combobox" id="shopCodeCB" disabled="disabled">
                            <option selected="selected" value="<s:property value="saleOrder.shop.shopCode" />"><s:property value="saleOrder.shop.shopName" /></option>							
                        </select>
                    </div>
                    <div class="Clear"></div>
                	<label class="LabelStyle Label2Style">Số đơn hàng</label>
                    <input type="text" id="orderNumber" class="InputTextStyle InputText1Style" disabled="disabled" value="<s:property value="saleOrder.orderNumber" />"/>
                    <label class="LabelStyle Label1Style">Mã KH</label>
                    <input id="customerCode" disabled="disabled" value="<s:property value="saleOrder.customer.shortCode" />" name="customerCode" type="text" class="InputTextStyle InputText1Style" />
                    <label class="LabelStyle Label1Style">Tên KH</label>
                    <p id="customerName" class="ValueStyle Value1Style"><s:property value="saleOrder.customer.customerName" /></p>
                    <div class="Clear"></div>
                    <label class="LabelStyle Label2Style">NVTT</label>
                    <div class="BoxSelect BoxSelect2">
                        <select <s:if test="disabledButton==1">disabled="disabled"</s:if> class="easyui-combobox" id="cashierStaffCode">
                            <option value=""></option>
							<s:iterator value="lstNVTTVo"  var="obj" >
								<s:if test="#obj.staffCode.equals(saleOrder.cashier.staffCode)">
									<option selected="selected" value="<s:property value="#obj.staffCode" />"><s:property value="#obj.staffName" /></option>
								</s:if>
								<s:else>
									<option value="<s:property value="#obj.staffCode" />"><s:property value="#obj.staffName" /></option>								
								</s:else>								
							</s:iterator>
                        </select>
                    </div>
                    <label class="LabelStyle Label1Style">NVGH</label>
                    <div class="BoxSelect BoxSelect2">
                        <select class="easyui-combobox" id="transferStaffCode" <s:if test="disabledButton==1">disabled="disabled"</s:if>>
                            <option value=""></option>
							<s:iterator value="lstNVGHVo" var="obj" >
								<s:if test="#obj.staffCode.equals(saleOrder.delivery.staffCode)">
									<option selected="selected" value="<s:property value="#obj.staffCode" />"><s:property value="#obj.staffName" /></option>
								</s:if>
								<s:else>
									<option value="<s:property value="#obj.staffCode" />"><s:property value="#obj.staffName" /></option>								
								</s:else>								
							</s:iterator>
                        </select>
                    </div>
                    <label class="LabelStyle Label1Style">Địa chỉ KH</label>
                    <p class="ValueStyle Value1Style" id="address"><s:property value="saleOrder.customer.address" /></p>
                    <div class="Clear"></div>
                    <label class="LabelStyle Label2Style">NVBH</label>
                    <div class="BoxSelect BoxSelect2">
                        <select class="easyui-combobox" id="staffSaleCode" disabled="disabled">
                            <option value=""></option>
							<s:iterator value="lstNVBHVo"  var="obj" >
								<s:if test="#obj.staffCode.equals(saleOrder.staff.staffCode)">
									<option selected="selected" value="<s:property value="#obj.staffCode" />"><s:property value="#obj.staffName" /></option>
								</s:if>
								<s:else>
									<option value="<s:property value="#obj.staffCode" />"><s:property value="#obj.staffName" /></option>								
								</s:else>								
							</s:iterator>
                        </select>
                    </div>
                    <label class="LabelStyle Label1Style">Chọn xe</label>
                    <div class="BoxSelect BoxSelect2">
                    	<select class="easyui-combobox" id="carId">
                            <option value=""></option>
							<s:iterator value="lstCars"  var="obj" >
								<s:if test="#obj.id.equals(carId)">
									<option selected="selected" value="<s:property value="#obj.id" />"><s:property value="#obj.carNumber" /></option>
								</s:if>
								<s:else>
									<option value="<s:property value="#obj.id" />"><s:property value="#obj.carNumber" /></option>								
								</s:else>								
							</s:iterator>
                        </select>
                    </div>
                    <label class="LabelStyle Label1Style">Số ĐT</label>
                    <p class="ValueStyle Value1Style" id="phone">
                    	<s:property value="codeNameDisplayEx(saleOrder.customer.mobiphone,saleOrder.customer.phone)" /> 
                    </p>
                    <div class="Clear"></div>
                    <label class="LabelStyle Label2Style">Ngày giao</label>
                    <input type="text" id="deliveryDate" value='<s:property value="deliveryDate"/>' name="deliveryDate" class="InputTextStyle InputText6Style vinput-date"  />
                    <label class="LabelStyle Label1Style">Độ ưu tiên</label>
                    <div class="BoxSelect BoxSelect2">
                        <s:select cssStyle="opacity: 0; height: 20px;" cssClass="MySelectBoxClass" list="priority" id="priorityId"
								value="saleOrder.priority" listKey="id" listValue="apParamName" disabled="disabledButton" ></s:select>
                    </div>
                    <div class="Clear"></div> 
                    <div style=""> 
						<p style="height: 25px; display: none;" class="ErrorMsgStyle " id="GeneralInforErrors">Có lỗi xảy ra khi cập nhật dữ liệu</p>
						<p style="height: 25px; display: none;" class="ErrorMsgStyle " id="GeneralInforErrorsWarning"></p>
					</div>
                    <div class="Clear"></div>
                </div>
                <h2 class="Title2Style">Danh sách sản phẩm bán
                	<img src="/resources/scripts/plugins/jquery-easyui-1.3.2/themes/default/images/loading.gif" id="loadingProduct" >
                </h2>
             	<div class="SearchInSection SProduct1Form" id="spSaleProductInfor" style="display: none;">                	
                	<div class="GeneralMilkBox">
						<div class="GeneralMilkTopBox">
							<div class="GeneralMilkBtmBox">
								<div class="GeneralMilkInBox">
									<div id="gridSaleData"></div>
									<div class="Clear"></div>
									<p class="TotalInfo" style="display:none;">
	                                	<span class="Tem1Style">Tổng trọng lượng: <span style="color: #FF0000" id="totalWeightSale">0</span><span style="color: #FF0000">(Kg)</span></span>
	                                	<span id="sumPrice1">
	                                    	<span class="Tem1Style">Tổng tiền hàng : <span style="color: #FF0000" id="totalAmountSale">0</span><span style="color: #FF0000">(VNĐ)</span></span>
                                			<span class="Tem1Style">Chiết khấu: <span style="color: #FF0000" id="totalDiscountSale">0</span><span style="color: #FF0000">(VNĐ)</span></span>
                                		</span>
                                	</p>
									<div class="ButtonSection" style="margin-top:20px;text-align: right;">			
										<s:if test="compareLockDay==0 && orderType=='IN' && approved==0 && approvedStep==0" >
											<button style="margin-right: 10px;" class="BtnGeneralStyle BtnGeneralMStyle Sprite2" id="payment" onclick="return SPCreateOrder.payment();">
												<span class="Sprite2">Tính tiền</span>
											</button>
											<a href="javascript:void(0)" style="margin-right: 10px;" id="backBtn" title="Tính tiền lại">
												<img src="/resources/images/icon-back.png" width="20" height="20" />
											</a>
										</s:if>	
										<div style="height: 15px;">
											<p id="alertSuccess_successMsg" class="SuccessMsgStyle" style="display: none;padding: 0;" >Tính tiền thành công cho đơn hàng !</p>
											<p style="display: none;" class="ErrorMsgStyle" id="saleInputErr"></p>
										</div>										
									</div>				
								</div>
							</div>
						</div>
					</div>                	
                </div>
                
                 <h2 id="h2SPKM" class="Title2Style">Danh sách sản phẩm khuyến mãi
                 	<img style="display: none;" src="/resources/scripts/plugins/jquery-easyui-1.3.2/themes/default/images/loading.gif" id="loadingPPromotion" >
                 </h2>
                 <div class="SearchInSection SProduct1Form" id="spPromotionProductInfor" style="display: none;">
                      <div class="GeneralMilkBox">
					  	<div class="GeneralMilkTopBox">
							<div class="GeneralMilkBtmBox">
								<div class="GeneralMilkInBox">
									<div class="GeneralTable">
										<div class="GridSection" style="padding: 0px;">
											<table id="gridPromotionProduct" class="easyui-datagrid" title="Danh sách sản phẩm khuyến mãi"></table>												
										</div>					
									</div>
								</div>
							</div>
						</div>
					</div>
                    <div class="Clear"></div>
                </div>
                
               <h2 id="h2MM" class="Title2Style">Khuyến mãi mở mới</h2>
                <div class="SearchInSection SProduct1Form">
                    <div class="GeneralMilkBox">
					  	<div class="GeneralMilkTopBox">
							<div class="GeneralMilkBtmBox">
								<div class="GeneralMilkInBox">
									<div class="GeneralTable">
										<div class="GridSection" id="promotionGridContainer2" style="padding: 0px;">
											<div id="promotionGrid2"></div>	
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
                    <div class="Clear"></div>
                </div>
                
                <h2 id="h2KMDH" class="Title2Style">Khuyến mãi đơn hàng</h2>
               <div id="saleOrderPromotion" class="SearchInSection SProduct1Form">
                	<div class="GeneralMilkBox">
						<div class="GeneralMilkTopBox">
							<div class="GeneralMilkBtmBox">
								<div class="GeneralMilkInBox">
									<div class="GeneralTable">
										<div class="GridSection" style="padding: 0px;">
											<table id="gridSaleOrderPromotion" class="easyui-datagrid" title="Danh sách sản phẩm khuyến mãi"></table>												
										</div>					
									</div>
								</div>
							</div>
						</div>
					</div>
                    <div class="Clear"></div>
                </div>
                
                <h2 id="h2KMTL" class="Title2Style">Khuyến mãi tích lũy</h2>
                <div id="saleOrderPromotion" class="SearchInSection SProduct1Form">
                	<div class="GeneralMilkBox">
					  	<div class="GeneralMilkTopBox">
							<div class="GeneralMilkBtmBox">
								<div class="GeneralMilkInBox">
									<div class="GeneralTable">
										<div class="GridSection" id="promotionGridContainer4" style="padding: 0px;">
											<div id="accumulativePromotionGrid"></div>	
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
                    <div class="Clear"></div>
                 </div>
                 <div class="SearchInSection SProduct1Form">
                   <p class="TotalInfo">
                    	<span class="Tem1Style">Tổng trọng lượng: <span style="color: #FF0000" id="totalWeight">0</span><span style="color: #FF0000">(Kg)</span></span>
                    	<span id="sumPrice">
	                        <span class="Tem1Style">Tổng tiền: <span style="color: #FF0000" id="totalVAT">0</span><span style="color: #FF0000">(VNĐ)</span></span>
                    		<span class="Tem1Style">Chiết khấu: <span style="color: #FF0000" id="totalDiscount">0</span><span style="color: #FF0000">(VNĐ)</span></span>
                    		<span class="Tem1Style">Tổng tiền thanh toán: <span style="color: #FF0000" id="total">0</span><span style="color: #FF0000">(VNĐ)</span></span>
                    	</span>
                    </p>
                    <div class="BtnCenterSection">
                        <s:if test="isVanSale!=1">
                        	<s:if test="disabledButton!=1">
                        		<%-- 
	 							<button id="removeButton"  class="BtnGeneralStyle Sprite2" title="Nhấn Ctrl+C để thực hiện Xoá đơn hàng" 
	 									onclick="return SPAdjustmentOrder.removeOrder();"><span class="Sprite2">&nbsp;&nbsp;&nbsp; Xóa&nbsp;&nbsp;&nbsp;</span>
	 							</button> 
                        		--%>
                			    <s:if test="approvedStep==0 && approved==0" >
                			    	<s:if test="compareLockDay!=-1 && orderType=='IN'" >
	                                	<button class="BtnGeneralStyle Sprite2" id = "btnOrderCancel" style="float: left;" onclick="return SPConfirmOrder.cancelOrder(<s:property value='orderId'/>);">&nbsp;&nbsp;Hủy&nbsp;&nbsp;</button>
	                                </s:if>
	                                
	                                <s:if test="compareLockDay==0 && orderType=='IN' && approved==0" >
			 							<button disabled="disabled" id="btnOrderSave"  class="BtnGeneralStyle BtnGeneralDStyle Sprite2" style="float:right; margin-right: 20px;" 
			 									onclick="return SPCreateOrder.createOrder();"><span class="Sprite2">&nbsp;&nbsp;&nbsp; Lưu&nbsp;&nbsp;&nbsp;</span>
			 							</button> 
		 							</s:if>
		 							
		 							<s:if test="compareLockDay==0" >
	                               		<button class="BtnGeneralStyle Sprite2" id="btnOrderApproved" style="float: right; margin-right: 10px;" onclick="return SPConfirmOrder.confirmOrder(<s:property value='orderId'/>,'serverErrors', false);">
	                               				Chấp nhận</button>
	                                </s:if>
	                                
	                                <button style="float: right; margin-right: 40px;" class="BtnGeneralStyle Sprite2" id="btnOrderDeny"
	                                		onclick="return SPConfirmOrder.denyOrder(<s:property value='orderId'/>);">Từ chối</button>
		 							
		 							<input type="text" class="InputTextStyle InputText8Style" id="description" maxlength="200" style="float:right; margin-right: 20px; margin-top:4px;" />
                			    	<label class="LabelStyle Label1Style" style="float:right; margin-right:10px; width:auto;padding:0; margin-top:8px;">Mô tả</label>
	 							</s:if>
                        	</s:if>
                        </s:if>
                        <s:if test="isVanSale==1">
                        	<s:if test="isApprovedVanSale!=1">
<%-- 	                        	<button disabled="disabled" id="approveButton" class="BtnGeneralStyle Sprite2" title="Nhấn Ctrl+X để thực hiện Duyệt đơn hàng" onclick="return SPAdjustmentOrder.saveOrder(3);"><span class="Sprite2">&nbsp;&nbsp;&nbsp; Duyệt&nbsp;&nbsp;&nbsp;</span></button>  --%>
								<s:if test="approvedStep==0 && approved==0 && lockedStock==1" >
		 							<s:if test="compareLockDay==0" >
	                               		<button class="BtnGeneralStyle Sprite2" id="btnOrderApproved" style="float: right; margin-right: 10px;"
	                               				onclick="return SPConfirmOrder.confirmOrder(<s:property value='orderId'/>,'serverErrors');">Chấp nhận</button>
	                                </s:if>
	                                
	                                <button style="float: right; margin-right: 40px;" class="BtnGeneralStyle Sprite2" id="btnOrderDeny"
	                                		onclick="return SPConfirmOrder.denyOrder(<s:property value='orderId'/>);">Từ chối</button>
		 							
		 							<input type="text" class="InputTextStyle InputText8Style" id="description" maxlength="200" style="float:right; margin-right: 20px; margin-top:4px;" />
                			    	<label class="LabelStyle Label1Style" style="float:right; margin-right:10px; width:auto;padding:0; margin-top:8px;">Mô tả</label>
                               	</s:if>
                        	</s:if>
                        </s:if>
                    </div>
                    <div class="Clear"></div>
                    <div style="height: 30px;"> 
						<p style="height: 25px; display: none;" class="ErrorMsgStyle" id="serverErrors">Có lỗi xảy ra khi cập nhật dữ liệu</p>
						<p id="successMsg" class="SuccessMsgStyle" style="display: none"></p>
					</div>
                </div>
            </div>
            <div class="Clear"></div>
        </div>
    </div>
    <div class="Clear"></div>
</div>
<div style="display:none">
	<div id="warningPopup" class="easyui-dialog" title="Xác nhận" data-options="closed:true,modal:true">
		<div class="PopupContentMid">
			<div class="GeneralForm" >
                <div class="icon-warning" style="width: 48px; height: 48px; float: left; margin-right: 20px;"></div>
               	<p>Đơn hàng có sản phẩm bán có giá không bằng giá hiện tại: <span id="productError" style="font-weight: bold; color: #FF6600;"></span></p>
                <p style="font-weight: bold; padding: 2px 0px; margin:10px 0 10px 50px;">Bạn có muốn tiếp tục duyệt đơn hàng này không?</p>
                <div class="Clear"></div>
                <div class="BtnCenterSection">
	                <button id="btnOk" class="BtnGeneralStyle">Duyệt</button>
					<button class="BtnGeneralStyle" id="btnCancel">Đóng</button>
                </div>
					<p id="errMsgSelectShop" class="ErrorMsgStyle" style="display: none"></p> 
			</div>
             </div>
	</div>
</div>
<div id="searchStyle5PPDialog" style="display: none;">
	<div id="searchStyle5" class="easyui-dialog" title="Danh sách sản phẩm khuyến mãi" style="width:616px;height:auto;" data-options="closed:true,modal:true">
        <div class="PopupContentMid2">
        	<div class="GeneralForm Search1Form">
                <div class="GridSection">
                    <table id="listPromotionProduct"></table>
                </div>
                <p style="display: none" id="errMultiChooseProduct" class="ErrorMsgStyle"></p>
                <div class="BtnCenterSection">
                	<button class="BtnGeneralStyle BtnGeneralMStyle" id="btnMultiChoose" style="display: none;"><span class="Sprite2">Chọn</span></button>
					<button class="BtnGeneralStyle BtnGeneralMStyle" onclick="$('#searchStyle5').dialog('close');"><span class="Sprite2">Đóng</span></button>
				</div>
            </div>            
		</div>
    </div>
</div>
<div style="display: none">
	<div id="divConfirmPrice" class="PopupContentMid">
		<div class="GeneralForm Search1Form">
			<label id="txtConfirmPrice" class="LabelStyle"></label>
			<div class="Clear"></div>
			<hr size="1">
			<div class="Clear"></div>
			<div class="BtnCenterSection">
				<button id="btnApplyPrice" onclick="SPConfirmOrder.applyPrice(<s:property value='orderId'/>);" class="BtnGeneralStyle BtnGeneralMStyle">Xác nhận</button>
				<button id="btnDismissPrice" onclick="SPConfirmOrder.dismissPrice();" class="BtnGeneralStyle BtnGeneralMStyle" onclick="">Bỏ qua</button>
			</div>
		</div>
	</div>
	<div id="divConfirmDebit" class="PopupContentMid">
		<div class="GeneralForm Search1Form" >
			<label id="txtConfirmDebit" class="LabelStyle"></label>
			<div class="Clear"></div>
			<hr size="1">
			<div class="Clear"></div>
			<div class="BtnCenterSection">
				<button id="btnSkipDebit" onclick="SPConfirmOrder.skipDebit(<s:property value='orderId'/>);" class="BtnGeneralStyle BtnGeneralMStyle">Xác nhận</button>
				<button id="btnDismissDebit" onclick="SPConfirmOrder.dismissDebit();" class="BtnGeneralStyle BtnGeneralMStyle" onclick="">Không xác nhận</button>
			</div>
		</div>
	</div>
</div>
<input type="hidden" id="shopCode" value="<s:property value='shopCode'/>" />
<input type="hidden" id="saleOrderStatus" value="<s:property value='saleOrder.approved.value'/>" />
<input type="hidden" id="saleOrderApprovedVan" value="<s:property value='saleOrder.approvedVan'/>" />
<input type="hidden" id="orderId" value="${orderId}" />
<input type="hidden" id="tablet" value="${saleOrder.orderSource.value}" />
<input type="hidden" id="currentLockDay" value="<s:property value="lockDate" />" />
<s:hidden id="disabledButton" name="disabledButton"></s:hidden>
<s:hidden id="canChangePromotionQuantity" name="canChangePromotionQuantity"></s:hidden>

<input type="hidden" id="isVansale" value="<s:property value='isVanSale'/>" />

<script type="text/javascript">
$(document).ready(function(){	
	//Define cac su kien phim tat
	//F1: tinh tien
	//F2: tao don hang
	//F3: Huy don hang
	//F4: Them moi don hang
	//F6: Import Excel
	//F9: tim kiem don hang
	$(document).keyup(function(event){
		if (event.which == keyCodes.F3) {
			event.stopImmediatePropagation();
		    SPCreateOrder.cancelWebOrder();
		}
	});
	$(document).keyup(function(event){
	  if (event.which == keyCodes.F1) {
		  if ($('#btnOrderSave').prop('disabled')) {
			  SPCreateOrder.payment();
		  } 
	  }
	  if (event.which == keyCodes.F2) {
		  event.stopImmediatePropagation();
		  if ($('#payment').prop('disabled')) {
			  SPCreateOrder.createOrder();
		  } 
	  } 
	  if (event.which == keyCodes.F4) {
		  event.stopImmediatePropagation();
		  $('')
	  } 
	  if (event.which == keyCodes.F6) {
		  event.stopImmediatePropagation();
		  $('#btnOrderDeny').click();
	  }
	  if (event.which == keyCodes.F9) {
		  event.stopImmediatePropagation();
		  SPCreateOrder.openSearchWebOrderDlg();
	  }
	});

	SPCreateOrder._isAllowShowPrice = <s:property value="isAllowShowPrice"/>;
	if(SPCreateOrder._isAllowShowPrice){
		$('#sumPrice').show();
		$('#sumPrice1').show();
	} else {
		$('#sumPrice').hide();
		$('#sumPrice1').hide();
	}
	SPAdjustmentOrder.orderId = '<s:property value="orderId"/>';
	var cashier = '<s:property value="saleOrder.cashier.staffCode"/>';
	var delivery = '<s:property value="saleOrder.delivery.staffCode"/>';
	$('#cashierStaffCode').val(cashier);
	$('#transferStaffCode').val(delivery);
	$('#cashierStaffCode').change();
	$('#transferStaffCode').change();
	var deliveryDate = '<s:property value="deliveryDate" />';
	if (!isNullOrEmpty(deliveryDate)) {
		$('#deliveryDate').val(deliveryDate);
	}
// 	var isWarningPrice = '<s:property value="isWarningPrice"/>';
	var isLostDetail = '<s:property value="isLostDetail"/>';
	SPAdjustmentOrder.pageLoad({
		priceWarning: 0
	});
	enable('payment');
	disabled('btnOrderSave');
	$('#backBtn').click(function(e) {
		enable('payment');
		disabled('btnOrderSave');
	});
	if($.cookie('saveOrder') != undefined && $.cookie('saveOrder') != null){
		$.cookie('saveOrder', null);
		$('#Save_successMsg').html('Chỉnh sửa đơn hàng thành công').show();
		setTimeout(function(){$('#Save_successMsg').hide();},5000);
	}
	if($.cookie('approvedOrder') != undefined && $.cookie('approvedOrder') != null){
		$.cookie('approvedOrder', null);
		$('#Save_successMsg').html('Duyệt đơn hàng thành công').show();
		setTimeout(function(){$('#Save_successMsg').hide();},5000);
	}	
	if(isLostDetail=='1'){
		$('#GeneralInforErrorsWarning').html('Đơn hàng rớt chi tiết không thể sửa').show();
		$('#backBtn').hide();
		$('#payment').remove();
		$('#btnOrderSave').remove();
		$('#btnOrderApproved').remove();
	}
// 	else if(isWarningPrice=='1' && '1' != '<s:property value="isVanSale"/>'){
// 		$('#GeneralInforErrorsWarning').html('Đơn hàng có sản phẩm sai giá, yêu cầu tính tiền lại.').show();

// 		setTimeout(function(){$('#GeneralInforErrorsWarning').hide();},5000);
// 	}
	<s:if test="approvedStep!=0 || approved!=0 || isLostDetail!=0" >
		setTimeout(function(){
			SPConfirmOrder.disabledGridEditOrderTablet();
		},3000);
	</s:if>
});
</script>
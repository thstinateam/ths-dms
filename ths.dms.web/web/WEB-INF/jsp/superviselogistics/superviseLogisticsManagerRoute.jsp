
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<style type="text/css">
.datagrid { margin:auto; }
</style>
<div class="BreadcrumbSection">
                <ul class="ResetList FixFloat BreadcrumbList">
                    <li class="Sprite1"><a href="/catalog/product">Giám sát vận tải</a></li>
                    <li><span>Quản lý tuyến giao hàng</span></li>
                </ul>
</div>

<div class="CtnOneColSection">
   	<div class="ContentSection">
       	<div class="ToolBarSection">
	      <div class="SearchSection GeneralSSection">
	      		<h2 class="Title2Style">Thông tin tìm kiếm</h2>
                <div class="SearchInSection SProduct1Form">
                    <label class="LabelStyle Label1Style">Xe</label>
                    <div class="BoxSelect BoxSelect2">
                            <select id="car" class="MySelectBoxClass" style="opacity: 0; height: 20px;">
                            	<s:iterator value="lstCar">
                                <option value="<s:property value="id"/>"><s:property value="carNumber"/></option>
                                </s:iterator>
                            </select>
                        </div>
                    <label class="LabelStyle Label1Style">Ngày</label>
                    <input type="text" id="date" maxLength="10" class="InputTextStyle InputText4Style" value="<s:property value="deliveryDate"/>" />
                    <input type="hidden" id="carSearch"  />
                    <input type="hidden" id="dateSearch"  />
                    <input type="hidden" id="nvghId"  />
                    <button onclick="return SupperviseLogisticsRoute.searchRoute();" style="margin-left:30px" class="BtnGeneralStyle" id="btSearch">Tìm kiếm</button>
                    <div class="Clear"></div>
                    <p id="errMsgSearch" class="ErrorMsgStyle" style="display: none"></p>
                </div>
	           	<div class="TabSection">
	            	<ul class="ResetList TabSectionList">
	                   	<li><a id="tab1" href="javascript:void(0);" onclick="SupperviseLogisticsRoute.loadTab1();" class="Sprite1"><span class="Sprite1">Danh sách khách hàng</span></a></li>
		            	<li><a id="tab2" href="javascript:void(0);" onclick="SupperviseLogisticsRoute.loadTab2();" class="Sprite1"><span class="Sprite1">Xem trên bản đồ</span></a></li>
		            	<li><a id="tab3" href="javascript:void(0);" onclick="SupperviseLogisticsRoute.loadTab3();" class="Sprite1"><span class="Sprite1">Chỉ định tài xế</span></a></li>
		            </ul>
	              	<div class="Clear"></div>
	           </div>
	           <div style="" id="container1">
		      		<div class="SearchInSection SProduct1Form">
					   	<div class="GridSection" id="searchContainerGrid">					
							<table id="grid"></table>
						</div>
                       <div class="Clear"></div>
	                </div>
			    	<div class="Clear"></div>
	            </div>
	            <div style="display:none" id="container2">
                     <div class="GeneralForm SearchInSection">
				       	<div id="divMapRoute" style="position:relative;">
							<div id="mapRoute" style="width:100%;height:565px;position: relative;" ></div>
			         	</div>
			         </div>
	            </div>
	            <div style="display:none" id="container3">
	            	<div class="SearchInSection SProduct1Form">
                		<label class="LabelStyle Label1Style">Tài xế</label>
                   		<div class="BoxSelect BoxSelect2">
                            <select id="nvgh" class="MySelectBoxClass" style="opacity: 0; height: 20px;">
                            	<option value="-1">-- Chọn nhân viên --</option>
	                            <s:iterator value="lstDeliveryStaff">
                                	<option value="<s:property value="id"/>"><s:property value="staffCode"/> - <s:property value="staffName"/></option>
                                </s:iterator>
                                <option selected="selected" value="1"></option>
                            </select>
                        </div>
	                </div>
	            </div>
	            <div class="Clear"></div>
	            <div class="SearchInSection SProduct1Form" style="text-align:center;">
	            <button onclick="return SupperviseLogisticsRoute.saveRoute();" style="display:none;" class="BtnGeneralStyle" id="btUpdate">Cập nhật</button>
	            </div>
	            <p id="errMsg" class="ErrorMsgStyle" style="display: none"></p>
            	<p id="successMsg" class="SuccessMsgStyle" style="display: none"></p>
	       </div>            
		</div>
    </div>
</div>
<script type="text/javascript">
$(document).ready(function(){
	setDateTimePicker('date');
	ReportUtils.setCurrentDateForCalendar('date');
	$('.MySelectBoxClass').customStyle();
	SupperviseLogisticsRoute._lstCustomer=new Map();
	SupperviseLogisticsRoute._lstCustomerStand=new Map();
	$('#carSearch').val($('#car').val());
	$('#dateSearch').val($('#date').val());
	SupperviseLogisticsRoute.loadAllCustomer();
	SupperviseLogisticsRoute.loadNVGH();
	$('#grid').datagrid({
		url : '/supervise/logistics/list-customer-for-routing',
		autoRowHeight : true,
		rownumbers : true, 
		pagination:true,
		rowNum : 10,
		pageSize:10,
		scrollbarSize : 0,
		singleSelect:true,
		pageNumber:1,
		fitColumns:true,
		queryParams:{
			page:1,
			carId:$('#carSearch').val().trim(),
			deliveryDate:$('#dateSearch').val().trim()
		},
		width : ($(window).width() - 40),
	    columns:[[  
	        {field:'shortCode',title:'Mã khách hàng',align:'left', width:60, sortable : false,resizable : false, formatter: function(v, r, i){
				return Utils.XSSEncode(v);
			}},
			{field:'customerName',title:'Tên khách hàng',align:'left', width:100, sortable : false,resizable : false, formatter: function(v, r, i){
				return Utils.XSSEncode(v);
			}},
			{field:'address',title:'Địa chỉ',align:'left', width:150, sortable : false,resizable : false, formatter: function(v, r, i){
				return Utils.XSSEncode(v);
			}},
			{field:'customerId',title:'Thứ tự ghé thăm',align:'center', width:30, sortable : false,resizable : false, formatter: function(v, r, i){
				var seq='';
				if(r.seq!=null && r.seq!=0) seq=Utils.XSSEncode(r.seq);
				return '<input id="customer'+r.customerId+'" onChange="SupperviseLogisticsRoute.inputChange('+r.customerId+')" type="text" style="width:100%" maxLength="10" class="InputTextStyle" value="'+seq+'" />';
			}}
	    ]],
	    onLoadSuccess :function(){
    		 $('.datagrid-header-rownumber').html('STT');	
    		 updateRownumWidthForDataGrid('.easyui-dialog');
	    }
	});
});
</script>
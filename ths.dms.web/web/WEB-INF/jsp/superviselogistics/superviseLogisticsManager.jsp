<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@page import="ths.dms.helper.Configuration"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<style>
.olPopup .olPopupContent {min-width:350px;}
.olPopupContent table td {border: 1px solid #CCCCCC; padding: 1px;}
.GeneralTable .AlignLCols, .GeneralTable .AlignCCols, .GeneralTable .AlignRCols {font-size: 14px;padding: 1px 1px;}
.GeneralTable th {font-size: 13px;}
.Dl1Style, .Dl2Style {font-size: 12px;}
.MPContent .Table1Section,.MapPopup1Section {width:350px;}
.Decoration {text-decoration:none;}
.Decoration:hover {text-decoration:underline;}
.StaffSelectSection {width:auto;}
.ToolSelectSection  {filter: alpha(opacity=70);z-index:9000;opacity: 0.7;}
.ToolSelectSection td div{font-weight: bold;}
</style>
<div class="BreadcrumbSection">
    <ul class="ResetList FixFloat BreadcrumbList">
        <li class="Sprite1"><a href="#">Giám sát vận tải</a></li>
        <li><span>Giám sát lộ trình trên bản đồ</span></li>
    </ul>
</div>
<div class="CtnOneColSection">
	<div class="ContentSection">
		<div class="ToolBarSection">
			<div class="GuideMapSection">
				<div class="GuideMapInSection">
					<label class="LabelStyle Label2Style"><input id="cbCust" onclick="SupperviseLogistics.cbCust();" disabled="disabled" type="checkbox" >Khách hàng</label>
					<label class="LabelStyle Label2Style"><input id="cbRoute" onclick="SupperviseLogistics.cbRoute();" type="checkbox" disabled="disabled">Lộ trình</label>
					<label class="LabelStyle Label1Style"><input id="cbCar" onclick="SupperviseLogistics.cbCar();" type="checkbox" disabled="disabled">Xe</label>
					<label class="LabelStyle Label2Style"><input id="cbOverlay" onclick="SupperviseLogistics.cbOverlay();" checked="checked" type="checkbox" >Vùng phủ</label>
					<div class="Clear"></div>
				</div>
			</div>
			<div class="TSSection SearchInSection SProduct1Form">
				<div class="BtnRSection">
					<button onclick="SupperviseLogistics.refresh();" style="height: 23px;padding: 3px 8px !important;width:120px" class="BtnGeneralStyle" id="btCapNhatViTri">Cập nhật vị trí</button>
				</div>
			</div>
			<div class="Clear"></div>
		</div>
		<div class="MapRouteSection" >
			<div class="ShadowBgSection"></div>
			<div id="bigMap" style="width: 100%; height: 500px; position: relative;"></div>
			<div class="NoteCustomersStatus" style="display:none;">
               	<ul class="ResetList NCSList FixFloat">
                   	<li id="item1">Bình thường</li>
                      <li id="item3">Cảnh báo</li>
                </ul>
                <a href="javascript:void(0)" onclick="SupperviseLogistics.reloadMarker();" class="CloseStyle"><img src="/resources/images/icon_close.png" width="12" height="12" /></a>
               </div>
			<div class="StaffSelectSection">
				<div class="StaffSelectBtmSection">
					<p id="titleStaff" onclick="SupperviseLogistics.toggleListShop();" class="StaffSelectText Sprite1 OffStyle">Đơn vị quản lý</p>
					<div id="listStaff" class="StaffTreeSection SearchSection GridSection">
						<input id="shopCode" placeholder="Mã đơn vị" type="text" class="InputTextStyle InputText1Style" style="width: 70px;padding-right:5px"/>
						<input id="shopName" placeholder="Tên đơn vị" type="text" class="InputTextStyle InputText2Style" style="width: 120px"/>
						<div class="BtnLSection">
							<button id="btSearchShop" onclick="SupperviseLogistics.loadTreeShop(1);" class="BtnGeneralStyle">Tìm kiếm</button>
						</div>
						<div class="Clear"></div>
						<div id="promotionShopGrid">
                             <table id="treeGrid" ></table> 
                   		</div>
					</div>
				</div>
			</div>
<!-- 			<div class="StaffSelectSection ToolSelectSection" style="right:2px;left:auto;top:0px;"> -->
<!-- 				<p id="titleTool" onclick="SupperviseLogistics.toggleListTool();" class="StaffSelectText Sprite1 OffStyle">&nbsp;</p> -->
<!-- 				<div id="listTool" style="max-height:200px" class="GridSection"> -->
<!-- 					<table id="toolGridEx" class="easyui-datagrid"></table> -->
<!-- 					<div id="searchStyle1PagerEx"></div> -->
<!-- 				</div> -->
<!-- 			</div> -->
		</div>
	</div>
	<div class="Clear"></div>
</div>
<div class="GeneralMilkBox">
	<div class="GeneralMilkTopBox">
		<div class="GeneralMilkBtmBox"></div>
	</div>
</div>
<div id="contextMenu" class="easyui-menu" style="width:150px;display:none">
		<div id="cmLanTuyen" >Xe lấn tuyến</div>
		<div id="cmLanVung" >Xe lấn vùng</div>
		<div id="cmKhongHetTuyen" >Xe không đi hết tuyến</div>
</div>

<div id="searchEasyUIDialog" class="easyui-dialog" title="Danh sách xe lấn tuyến" style="width: 850px; height: 450px;" data-options="closed:true,modal:true">
	<div class="PopupContentMid">
		<div class="GeneralForm Search1Form" >
				<input id="deliveryDate" maxlength="50" type="text" style=" width: 145px;" class="InputTextStyle InputText1Style" /> 
				<button class="BtnGeneralStyle BtnSearchStyle BtnSearchOnDialog" style="margin-left: 20px; " id="btnSearchCar">Tìm kiếm</button>
				<div class="Clear"></div>
				<p id="warningMsg" class="SuccessMsgStyle" style="display: none"></p>
				<s:hidden id="shopId"></s:hidden>
				<s:hidden id="isWarning"></s:hidden>
				<div class="Clear"></div>
				<div class="GridSection" id="searchContainerGrid">					
					<table id="grid" class="easyui-datagrid"></table>
					<div id="searchStyle1Pager"></div>
				</div>
				<div class="BtnCenterSection">
					<button class="BtnGeneralStyle BtnGeneralMStyle" onclick="$('#searchEasyUIDialog').dialog('close');">Đóng</button>
				</div>
				<p id="errMsgSearch" style="display: none;" class="ErrorMsgStyle SpriteErr"></p>
			</div>
	</div>
</div>
<div id="searchEasyRoutingUIDialog" class="easyui-dialog" title="Danh sách xe không đi hết tuyến" style="width: 850px; height: 450px;" data-options="closed:true,modal:true">
	<div class="PopupContentMid">
		<div class="GeneralForm Search1Form" >
				<input id="deliveryDateRouting" maxlength="50" type="text" style=" width: 145px;" class="InputTextStyle InputText1Style" /> 
				<button class="BtnGeneralStyle BtnSearchStyle BtnSearchOnDialog" style="margin-left: 20px; " id="btnSearchRouting">Tìm kiếm</button>
				<div class="Clear"></div>
				<p id="warningMsg" class="SuccessMsgStyle" style="display: none"></p>
				<s:hidden id="shopIdRouting"></s:hidden>
				<s:hidden id="isWarning"></s:hidden>
				<div class="Clear"></div>
				<div class="GridSection" id="searchRoutingContainerGrid">					
					<table id="gridRouting" class="easyui-datagrid"></table>
					<div id="searchStyle1Pager"></div>
				</div>
				<div class="BtnCenterSection">
					<button class="BtnGeneralStyle BtnGeneralMStyle" onclick="$('#searchEasyRoutingUIDialog').dialog('close');">Đóng</button>
				</div>
				<p id="errMsgSearch" style="display: none;" class="ErrorMsgStyle SpriteErr"></p>
			</div>
	</div>
</div>
<div id="searchOverlayEasyUIDialog" class="easyui-dialog" title="Danh sách xe lấn vùng" style="width: 850px; height: 450px;" data-options="closed:true,modal:true">
	<div class="PopupContentMid">
		<div class="GeneralForm Search1Form" >
<!-- 				<input id="carNumberOverlay" maxlength="50" type="text" style=" width: 145px;" class="InputTextStyle InputText1Style" />  -->
<!-- 				<button class="BtnGeneralStyle BtnSearchStyle BtnSearchOnDialog" style="margin-left: 20px; " id="btnSearchOverlay">Tìm kiếm</button> -->
<!-- 				<div class="Clear"></div> -->
				<p id="warningMsg" class="SuccessMsgStyle" style="display: none"></p>
				<s:hidden id="shopIdOverlay"></s:hidden>
				<div class="Clear"></div>
				<div class="GridSection" id="searchOverlayContainerGrid">					
					<table id="gridOverlay" class="easyui-datagrid"></table>
					<div id="searchStyle1Pager"></div>
				</div>
				<div class="BtnCenterSection">
					<button class="BtnGeneralStyle BtnGeneralMStyle" onclick="$('#searchOverlayEasyUIDialog').dialog('close');">Đóng</button>
				</div>
			</div>
	</div>
</div>
<s:hidden id="shopId" name="shopId"></s:hidden>
<s:hidden id="type" name="type"></s:hidden>
<script>
$(document).ready(function(){
	$('.MySelectBoxClass').customStyle();
	setDateTimePicker('deliveryDate');
	setDateTimePicker('deliveryDateRouting');
	$('#deliveryDate').val(getCurrentDate());
	$('#deliveryDateRouting').val(getCurrentDate());
	$('#bigMap').css('height',($(window).height()-172)+'px');
	$(window).resize(function() {
		$('#bigMap').css('height',($(window).height()-172)+'px');
		$('.datagrid-body').css('max-height',($(window).height()-300)+'px');
	});
	SupperviseLogistics._lstCarPosition=new Map();
	SupperviseLogistics.loadTreeShop();
	SupperviseLogistics.getListShop();
	$('.datagrid-body').css('max-height',($(window).height()-300)+'px');
	VTMapUtil.loadMapResource(function(){
		var map = ViettelMap.loadBigMap('bigMap',null,null,4,null);
		viettel.Events.addListener(map, 'zoom_changed', function(a,b,c) {
			if(SupperviseLogistics._currentShopId==null && SupperviseLogistics._denyZoom==null){
				var zoom=ViettelMap._map.getZoom();
				if(zoom==7 || zoom==8){
					SupperviseLogistics._notFitOverlay=1;
					SupperviseLogistics.reloadMarker(1);
				}
			}
	    });
	});
	$('#shopCode').bind('keyup',function(event){
		if(event.keyCode == keyCodes.ENTER){		   				
			SupperviseLogistics.loadTreeShop(1);
		}
	});
	$('#shopName').bind('keyup',function(event){
		if(event.keyCode == keyCodes.ENTER){		   				
			SupperviseLogistics.loadTreeShop(1);
		}
	});
});
</script>
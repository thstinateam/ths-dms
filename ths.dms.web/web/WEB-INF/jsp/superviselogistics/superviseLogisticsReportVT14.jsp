<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<dl class="Dl3Style" id="divHO_2x_to_3x">	
	<dt>
		<label class="LabelStyle Label1Style">Mã NPP<span class="ReqiureStyle">(*)</span>
		</label>
	</dt>
	<dd>
<!-- 		<input type="text" id="shop" class="InputTextStyle InputText1Style" /> -->
		<input type="text" id="shop" style="width:250px; height: auto;" class="InputTextStyle InputText1Style" />
	</dd>

	<dt class="">
		<label class="LabelStyle Label1Style">Số xe</label>
	</dt>
	<dd>
		<input id="carNumber" type="text" class="InputTextStyle InputText3Style"  />
	</dd>
</dl>
<div class="Clear"></div>
<div class="BtnCenterSection">
	<button id="btnSearch" class="BtnGeneralStyle" onclick="SupperviseLogistics.exportVT14();">Xuất báo cáo</button>
</div>	
<p id="errMsg" class="ErrorMsgStyle" style="display: none"></p>
<s:hidden id="shopId"></s:hidden>
<s:hidden id="curShopId" name="shopId"></s:hidden>
<script type="text/javascript">
$(document).ready(function(){			
// 	ReportUtils.loadComboTree('shop','shopId',$('#curShopId').val(),null,null,null,null,true);
	ReportUtils.loadKendoTreeMultipleChoice("shop", $('#shopId').val(), $('#curShopId').val());
});
</script>
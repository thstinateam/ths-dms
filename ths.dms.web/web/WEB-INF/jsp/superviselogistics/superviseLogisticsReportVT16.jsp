<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<dl class="Dl3Style" id="divHO_2x_to_3x">	
	<dt>
		<label class="LabelStyle Label1Style">Mã NPP<span class="ReqiureStyle">(*)</span>
		</label>
	</dt>
	<dd>
		<input type="text" id="shop" class="InputTextStyle InputText1Style" />
	</dd>

	<dt class="">
		<label class="LabelStyle Label1Style">Xe</label>
	</dt>
	<dd>
		<input id="carNumber" maxlength="20" type="text" class="InputTextStyle InputText3Style" />
	</dd>
	<dt class="ClearLeft LabelTMStyle">
		<label class="LabelStyle Label1Style">Từ ngày<br />(dd/MM/yyyy) <span class="ReqiureStyle">(*)</span>
		</label>
	</dt>
	<dd>
		<input id="fromDate" type="text" class="InputTextStyle InputText3Style" />
	</dd>
	<dt class="LabelTMStyle">
		<label class="LabelStyle Label1Style">Đến ngày<br />(dd/MM/yyyy) <span class="ReqiureStyle">(*)</span>
		</label>
	</dt>
	<dd>
		<input id="toDate" type="text" class="InputTextStyle InputText3Style" />
	</dd>
</dl>
<div class="Clear"></div>
<div class="BtnCenterSection">
	<button id="btnSearch" class="BtnGeneralStyle" onclick="SupperviseLogistics.exportVT16();">Xuất báo cáo</button>
</div>	
<p id="errMsg" class="ErrorMsgStyle" style="display: none"></p>
<s:hidden id="shopId"></s:hidden>
<s:hidden id="curShopId" name="shopId"></s:hidden>
<script type="text/javascript">
$(document).ready(function(){			
// 	ReportUtils.loadComboTree('shop','shopId',$('#curShopId').val(),null,null,null,null,true);
	ReportUtils.loadKendoTreeMultipleChoice("shop", $('#shopId').val(), $('#curShopId').val());
	//fix loi ngay gio hien tai
	setDateTimePicker('fromDate');
	setDateTimePicker('toDate');
	$('#fromDate').datepicker({ dateFormat: 'dd/mm/yyyy' });
	$('#fromDate').datepicker('setDate', new Date());
	$('#toDate').datepicker({ dateFormat: 'dd/mm/yyyy' });
	$('#toDate').datepicker('setDate', new Date());
});
</script>